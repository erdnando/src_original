package com.netro.fondojr;

public class FondoJrConciliacionBean implements java.io.Serializable  {
	
	private String fecVencIni = "";
	private String fecVencFin = "";
	private String fecOperIni = "";
	private String fecOperFin = "";
	private String fecFideIni = "";
	private String fecFideFin = "";
	private String prestamo = "";
	private String estatus = "";
	private String strDirectorioPublicacion = "";
	private String clavePrograma = "";
	private String diasDesde;	
	private String diasHasta;
	private String descProgramaFondoJR;
	
	/*------------------fecVencIni-------------------*/
	public String getFecVencIni(){ return fecVencIni; }	
	public void setFecVencIni(String fecVencIni)	{ this.fecVencIni = fecVencIni;}
	/*------------------fecVencFin-------------------*/	
	public String getFecVencFin(){ return fecVencFin; }
	public void setFecVencFin(String fecVencFin)	{ this.fecVencFin = fecVencFin;}
	/*------------------fecOperIni-------------------*/
	public String getFecOperIni(){ return fecOperIni; }	
	public void setFecOperIni(String fecOperIni)	{ this.fecOperIni = fecOperIni;}
	/*------------------fecOperFin-------------------*/
	public String getFecOperFin(){ return fecOperFin; }
	public void setFecOperFin(String fecOperFin)	{ this.fecOperFin = fecOperFin;}
	/*------------------fecFideIni-------------------*/
	public String getFecFideIni(){ return fecFideIni; }	
	public void setFecFideIni(String fecFideIni)	{ this.fecFideIni = fecFideIni;}
	/*------------------fecFideFin-------------------*/
	public String getFecFideFin(){ return fecFideFin; }	
	public void setFecFideFin(String fecFideFin)	{ this.fecFideFin = fecFideFin;}
	/*-----------strDirectorioPublicacion------------*/
	public String getStrDirectorioPublicacion(){ return strDirectorioPublicacion; }
	public void setStrDirectorioPublicacion(String strDirectorioPublicacion){this.strDirectorioPublicacion = strDirectorioPublicacion;}
	/*--------------*/
	public String getPrestamo(){ return prestamo; }
	public void setPrestamo(String prestamo) { this.prestamo = prestamo;}
	/*------------*/
	public String getEstatus(){ return estatus; }
	public void setEstatus(String estatus) {this.estatus = estatus;}
	
	public String getClavePrograma() {return clavePrograma;}
	public void setClavePrograma(String clavePrograma) {this.clavePrograma = clavePrograma;}
  
  	public String getDiasDesde(){	return this.diasDesde; }
	public void setDiasDesde(String diasDesde){	this.diasDesde = diasDesde; }
	
	public String getDiasHasta(){	return this.diasHasta;	}
	public void setDiasHasta(String diasHasta){ this.diasHasta = diasHasta;	}

	public String getDescProgramaFondoJR() {return descProgramaFondoJR; }
	public void setDescProgramaFondoJR(String descProgramaFondoJR) { this.descProgramaFondoJR = descProgramaFondoJR; }
	
}