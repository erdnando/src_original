package com.netro.fondojr;

public class FondoJrConsultaBean implements java.io.Serializable  {
	
	private String fecVencIni = "";
	private String fecVencFin = "";
	private String fecOperIni = "";
	private String fecOperFin = "";
	private String fecFideIni = "";
	private String fecFideFin = "";	
	private String estatus = "";
	private String estatusVal = "";
	private String prestamo = "";
	private String numSirac = "";
	private String nombrePyme = "";
	private String orden = "";
	private String subtotales = "";
	private String strDirectorioPublicacion = "";
	
	/*------------------fecVencIni-------------------*/
	public String getFecVencIni(){ return fecVencIni; }	
	public void setFecVencIni(String fecVencIni)	{ this.fecVencIni = fecVencIni;}
	/*------------------fecVencFin-------------------*/	
	public String getFecVencFin(){ return fecVencFin; }
	public void setFecVencFin(String fecVencFin)	{ this.fecVencFin = fecVencFin;}
	/*------------------fecOperIni-------------------*/
	public String getFecOperIni(){ return fecOperIni; }	
	public void setFecOperIni(String fecOperIni)	{ this.fecOperIni = fecOperIni;}
	/*------------------fecOperFin-------------------*/
	public String getFecOperFin(){ return fecOperFin; }
	public void setFecOperFin(String fecOperFin)	{ this.fecOperFin = fecOperFin;}
	/*------------------fecFideIni-------------------*/
	public String getFecFideIni(){ return fecFideIni; }	
	public void setFecFideIni(String fecFideIni)	{ this.fecFideIni = fecFideIni;}
	/*------------------fecFideFin-------------------*/
	public String getFecFideFin(){ return fecFideFin; }	
	public void setFecFideFin(String fecFideFin)	{ this.fecFideFin = fecFideFin;}
	/*-----------strDirectorioPublicacion------------*/
	public String getStrDirectorioPublicacion(){ return strDirectorioPublicacion; }
	public void setStrDirectorioPublicacion(String strDirectorioPublicacion){this.strDirectorioPublicacion = strDirectorioPublicacion;}
	/*--------------*/
	public String getPrestamo(){ return prestamo; }
	public void setPrestamo(String prestamo) { this.prestamo = prestamo;}
	/*------------*/
	public String getEstatus(){ return estatus; }
	public void setEstatus(String estatus) {this.estatus = estatus;}
	/*------------*/
	public String getEstatusVal(){ return estatusVal; }
	public void setEstatusVal(String estatusVal){	this.estatusVal = estatusVal;}
	/*------------*/
	public String getNumSirac(){ return numSirac; }
	public void setNumSirac(String numSirac){ this.numSirac = numSirac;}
	/*------------*/
	public String getNombrePyme(){ return nombrePyme; }
	public void setNombrePyme(String nombrePyme){	this.nombrePyme = nombrePyme;}
	/*------------*/
	public String getOrden(){ return orden; }
	public void setOrden(String orden){	this.orden = orden;}
	/*------------*/
	public String getSubtotales(){ return subtotales; }
	public void setSubtotales(String subtotales)	{this.subtotales = subtotales;}
}