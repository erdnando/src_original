package com.netro.fondojr;

import java.io.Serializable;
import com.netro.exception.*;
import java.util.HashMap;
import netropology.utilerias.*;
import java.sql.*;
import java.text.*;
import java.math.*;

/**
 * Clase que se encarga de invocar al metodo realizarCargaFondoJr del 
 * Bean de FondoJr
 *
 * @author  Fabian Valenzuela
 * @version 1.0.14
 */

public class CargaDatosFondoJrThread implements Runnable, Serializable {
	
	private int 				numberOfRegisters;
	private boolean 			started;
	private boolean 			running;
	private int 				processedRegisters;
	private FondoJuniorBean fondojr;
	private boolean			error;
	private String 			folio;
	private String				numProc;
	private String				fecha;
	private String				hora;
	private String				tipoMod;
	private String				cvePrograma;
	private String				iNoUsuario;
	private String				strNombreUsuario;
	

   public CargaDatosFondoJrThread() {
	numberOfRegisters 	= 0;
	started 			   = false;
	running 			   = false;
	processedRegisters = 0;
	fondojr			= new FondoJuniorBean();
	error					= false;
   }
	 
   protected void realizarCargaFondoJr() {
		
	try {

		HashMap map = new HashMap();
		map = fondojr.realizaAjusteFondoJr(numProc,tipoMod, cvePrograma, iNoUsuario, strNombreUsuario);
		folio = (String)map.get("RECIBO");		
		fecha = (String)map.get("FECHA");
		hora = (String)map.get("HORA");
		

	} catch(Exception e){
			setRunning(false);
			processedRegisters = numberOfRegisters ;
			setError(true);
			e.printStackTrace();
			System.out.println("Error en CargaDatosFondoJrThread::realizarCargaFondoJr al modificar vencimientos");
			// terminar el proceso si falla la funcion de validacion para algun registro
	}
		
   }
	
	public synchronized int getNumberOfRegisters() {
       return numberOfRegisters;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		return((processedRegisters == 0 && numberOfRegisters == 0)?false:(processedRegisters == numberOfRegisters));
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
	public synchronized void setError(boolean error) {
       this.error = error;
   }
	
	public synchronized boolean hasError() {
       return error;
   }
	
	public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }
	
	public synchronized void setNumProc(String numProc) {
		this.numProc = numProc;
	}
	
	public synchronized void setTipoMod(String tipoMod) {
		this.tipoMod = tipoMod;
	}
	
	public synchronized void setCvePrograma(String cvePrograma) {
		this.cvePrograma = cvePrograma;
	}
	
	public synchronized void setINoUsuario(String iNoUsuario) {
		this.iNoUsuario = iNoUsuario;
	}
	
	public synchronized void setStrNombreUsuario(String strNombreUsuario) {
		this.strNombreUsuario = strNombreUsuario;
	}
	

   public void run() {
		 
		// Obtener numero de registros
		//AccesoDB 	con 	=	new AccesoDB();
		boolean		lbOK	=  true;
		
		// Realizar la Validacion de los datos
      try {
			
			//if(lbOK == false) 
				setRunning(true);
			
				while (isRunning() && folio == null){
					if(lbOK && folio == null){
						lbOK = false;
						realizarCargaFondoJr();
					}
				}
		
      } finally {
			setRunning(false);
      }

    }

	 public String getFolio(){
		return folio;
	 }
 
	 public void setFolio(String folio){
		this.folio = folio;
	 }
	 
	 public String getFecha(){
		return fecha;
	 }
	 
	 public void setFecha(String fecha){
		this.fecha = fecha;
	 }
	 
	 public String getHora(){
		return hora;
	 }
	 
	 public void setHora(String hora){
		this.hora = hora;
	 }


}