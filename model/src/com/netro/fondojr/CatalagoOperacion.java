package com.netro.fondojr;

public class CatalagoOperacion implements java.io.Serializable{


	public String numero;
	public String descripcion;
	public String operacion;

	public CatalagoOperacion()
	{
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String Descripcion) {
		this.descripcion = Descripcion;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}


}