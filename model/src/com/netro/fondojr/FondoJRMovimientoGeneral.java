package com.netro.fondojr;

public class FondoJRMovimientoGeneral implements java.io.Serializable{

	public String numero;
	public String proceso;
	public String fechaMovimiento;
	public String fechaValor;
	public String importe;
	public String referencia;
	public String operacion;
	public String observaciones;
	public String pkcs7;
	public String textoFirmado;
	public String firmar;
	public String serial;
	public String termina;
	public String folio;
	public String validCert;
	

	public FondoJRMovimientoGeneral()
	{
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public String getFechaMovimiento() {
		return fechaMovimiento;
	}

	public void setFechaMovimiento(String fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}

	public String getFechaValor() {
		return fechaValor;
	}

	public void setFechaValor(String fechaValor) {
		this.fechaValor = fechaValor;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public String getPkcs7() {
		return pkcs7;
	}

	public void setPkcs7(String pkcs7) {
		this.pkcs7 = pkcs7;
	}

	public String getTextoFirmado() {
		return textoFirmado;
	}

	public void setTextoFirmado(String textoFirmado) {
		this.textoFirmado = textoFirmado;
	}

	public String getFirmar() {
		return firmar;
	}

	public void setFirmar(String firmar) {
		this.firmar = firmar;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}
	
public String toString()
	{
		String cadena = "";
		cadena = "No de Movimiento:"+numero+
					" Proceso:"+proceso+
					" Fecha de Movimiento:"+fechaMovimiento+
					" Fecha de Valor:"+fechaValor+
					" Importe:"+importe+
					" Referencia:"+referencia+
					" Tipo de Operacion:"+operacion+
					" pkcs7=" + pkcs7 + 
					" TextoFirmado=" + textoFirmado +
				   " serial=" + serial +
					" usuario=" + termina +
					" folio=" + folio +
					" Observaciones:"+observaciones;

		return cadena;
	}

	public String getTermina() {
		return termina;
	}

	public void setTermina(String termina) {
		this.termina = termina;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getValidCert() {
		return validCert;
	}

	public void setValidCert(String validCert) {
		this.validCert = validCert;
	}

	
}