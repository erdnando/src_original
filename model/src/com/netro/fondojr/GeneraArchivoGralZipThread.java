package com.netro.fondojr;

import java.io.Serializable;
import com.netro.exception.*;
import netropology.utilerias.*;
import java.sql.*;
import java.text.*;
import java.math.*;

/**
 * Clase que se encarga de invocar al metodo generarArchConciliacionZip del 
 * Bean de FondoJr
 *
 * @author  Jesus Salim Hernandez David
 * @version 1.0.14
 */

public class GeneraArchivoGralZipThread implements Runnable, Serializable {
	
	private int 				numberOfRegisters;
	private boolean 			started;
	private boolean 			running;
	private int 				processedRegisters;
	private FondoJuniorBean fondojr;
	private boolean			error;
	private String 			nombreArchivo;
	private String 			rutaFisica;
	private String 			rutaVirtual;
	private String 			clavePrograma;
	private String 			nombrePrograma;
	private String				pantalla;
	private String				folio_val;
	private String				estatus_val;
	private String				fecVencimiento;
	private String				chkDesembolso;
	
	private String				estatusDetVal;
	private String				estatus2DetVal;
	private String				procDetVal;

   public GeneraArchivoGralZipThread() {
	numberOfRegisters 	= 0;
	started 			   = false;
	running 			   = false;
	processedRegisters = 0;
	fondojr			= new FondoJuniorBean();
	error					= false;
   }
	 
   protected void generaArchivoZip() {
		
	try {

		//nombreArchivo = fondojr.generaArchivoGralZip(rutaFisica, rutaVirtual, clavePrograma, nombrePrograma, pantalla, estatus_val, folio_val, fecVencimiento, chkDesembolso);
		nombreArchivo = fondojr.generaArchivoGralZip(rutaFisica, rutaVirtual, clavePrograma, nombrePrograma, pantalla, estatus_val, folio_val, fecVencimiento, chkDesembolso, estatusDetVal, estatus2DetVal, procDetVal);
				

	} catch(Exception e){
			setRunning(false);
			processedRegisters = numberOfRegisters ;
			setError(true);
			e.printStackTrace();
			System.out.println("Error en ValidaDatosPymeMasivaThread::validateRegisters al extraer numero de registro de la base de datos");
			// terminar el proceso si falla la funcion de validacion para algun registro
	}
		
   }
	
	public synchronized int getNumberOfRegisters() {
       return numberOfRegisters;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		return((processedRegisters == 0 && numberOfRegisters == 0)?false:(processedRegisters == numberOfRegisters));
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
	public synchronized void setError(boolean error) {
       this.error = error;
   }
	
	public synchronized boolean hasError() {
       return error;
   }
	
	public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }
	

   public void run() {
		 
		// Obtener numero de registros
		//AccesoDB 	con 	=	new AccesoDB();
		boolean		lbOK	=  true;
		
		// Realizar la Validacion de los datos
      try {
			
			//if(lbOK == false) 
				setRunning(true);
			
				while (isRunning() && nombreArchivo == null){
					if(lbOK && nombreArchivo == null){
						lbOK = false;
						generaArchivoZip();
					}
				}
		
      } finally {
			setRunning(false);
      }

    }

	 public String getNombreArchivo(){ // Fodea 057 - 2010
		return nombreArchivo;
	 }
 
	 public void setNombreArchivo(String nombreArchivo){ // Fodea 057 - 2010
		this.nombreArchivo = nombreArchivo;
	 }


	public void setRutaFisica(String rutaFisica) {
		this.rutaFisica = rutaFisica;
	}


	public String getRutaFisica() {
		return rutaFisica;
	}


	public void setRutaVirtual(String rutaVirtual) {
		this.rutaVirtual = rutaVirtual;
	}


	public String getRutaVirtual() {
		return rutaVirtual;
	}


	public void setClavePrograma(String clavePrograma) {
		this.clavePrograma = clavePrograma;
	}


	public String getClavePrograma() {
		return clavePrograma;
	}


	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}


	public String getNombrePrograma() {
		return nombrePrograma;
	}


	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}


	public String getPantalla() {
		return pantalla;
	}


	public void setFolio_val(String folio_val) {
		this.folio_val = folio_val;
	}


	public String getFolio_val() {
		return folio_val;
	}



	public void setEstatus_val(String estatus_val) {
		this.estatus_val = estatus_val;
	}


	public String getEstatus_val() {
		return estatus_val;
	}


	public void setFecVencimiento(String fecVencimiento) {
		this.fecVencimiento = fecVencimiento;
	}


	public String getFecVencimiento() {
		return fecVencimiento;
	}


	public void setChkDesembolso(String chkDesembolso) {
		this.chkDesembolso = chkDesembolso;
	}


	public String getChkDesembolso() {
		return chkDesembolso;
	}


	public void setEstatusDetVal(String estatusDetVal) {
		this.estatusDetVal = estatusDetVal;
	}


	public String getEstatusDetVal() {
		return estatusDetVal;
	}


	public void setEstatus2DetVal(String estatus2DetVal) {
		this.estatus2DetVal = estatus2DetVal;
	}


	public String getEstatus2DetVal() {
		return estatus2DetVal;
	}


	public void setProcDetVal(String procDetVal) {
		this.procDetVal = procDetVal;
	}


	public String getProcDetVal() {
		return procDetVal;
	}

}