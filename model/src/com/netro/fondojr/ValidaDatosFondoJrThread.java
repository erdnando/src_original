package com.netro.fondojr;
import java.io.Serializable;
import com.netro.exception.*;
import netropology.utilerias.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class ValidaDatosFondoJrThread implements Runnable, Serializable {
	
	private static final long serialVersionUID = 20110120L;
	 
   private int 							numberOfRegisters;
   private float							percent;
   private boolean 						started;
   private boolean 						running;
	private int 							processID;
	private int 							processedRegisters;
	private boolean						error;
	private int								errorRecordsCount;
	private FondoJuniorBean				fondojr;
	private String							tipoModificacion;
	private String							cveProgramaFjr;
	 
	public ValidaDatosFondoJrThread() {
        numberOfRegisters 	= 0;
        percent 		   	= 0;
        started 			   = false;
        running 			   = false;
	     processID 	   	= 0;
		  processedRegisters = 0;
		  error					= false;
		  errorRecordsCount	= 0;
		  fondojr				= new FondoJuniorBean();
		  tipoModificacion	= "A";
		  cveProgramaFjr		= "1";
   }
	
	 protected void validateRegisters() {
		
	   try {
			
			
			fondojr.validaDatosTmpAjusteFondoJr(processID,processedRegisters+1, tipoModificacion, cveProgramaFjr);				
			
				
			processedRegisters++;
			percent = (processedRegisters/(float)numberOfRegisters)*100;
			
		} catch(Exception e){
			setRunning(false);
			processedRegisters = numberOfRegisters ;
			setError(true);
			e.printStackTrace();
			System.out.println("Error en ValidaDatosPymeMasivaThread::validateRegisters al extraer numero de registro de la base de datos");
			// terminar el proceso si falla la funcion de validacion para algun registro
		}
		
   }
	
	public synchronized float getPercent() {
       return percent;
   }
	
	public synchronized int getNumberOfRegisters() {
       return numberOfRegisters;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		//return((processedRegisters == 0 && numberOfRegisters == 0)?false:(processedRegisters == numberOfRegisters));
		return (processedRegisters < numberOfRegisters?false:true);
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
	public synchronized void setProcessID(int processID) {
       this.processID = processID;
   }
	
	public synchronized void setError(boolean error) {
       this.error = error;
   }
		
	public synchronized boolean hasError() {
       return error;
   }
	
	public synchronized int getProcessID() {
       return processID;
   }

	public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }
	
	public synchronized void setTipoModificacion(String tipoModificacion) {
       this.tipoModificacion = tipoModificacion;
   }
	
	public synchronized void setCveProgramaFjr(String cveProgramaFjr){
		this.cveProgramaFjr = cveProgramaFjr;
	}

	public void run() {
		 
		// Obtener numero de registros
		AccesoDB 			con 	= new AccesoDB();
		boolean				lbOK	= true;
		StringBuffer 		query = null; 
		ResultSet 			rs 	= null;
		PreparedStatement ps		= null;
		
		try {
			
			con.conexionDB();
			
			query = new StringBuffer(); 		
			query.append(
				"SELECT                    " +
				"	MAX(IC_NUMERO_LINEA)    " +
		 		"FROM                      " +
				"	COMTMP_AJUSTE_FONDOJR   " +
				"WHERE                     " +
				"	IC_NUMERO_PROCESO  = ?  " // processID
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,processID);
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()){	
				numberOfRegisters = rs.getInt(1);
			}
			
		}catch (Exception e){
			System.out.println("Error en ValidaDatosFondoJrThread::run() al extraer de la base de datos el numero de registros a procesar");
			lbOK = false;
			e.printStackTrace();
			setError(true);
			numberOfRegisters = 0;
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		// Verificar que no se hayan devuelto datos vacios
		if ( numberOfRegisters == 0){
			numberOfRegisters  	= 0;
			processedRegisters 	= 0;
			percent					= 100F;
			lbOK 						= false;
		}
		
		// Realizar la Validacion de los datos
      try {
			
			if(lbOK == false){ 
				setRunning(false);
			}else{
				setRunning(true);
			}
			
			//registroTemporal 			= new RegistroTmpCompranet();
			//validacionesEspeciales 	= new ValidacionesEspeciales();
			
			while (isRunning() && !isCompleted()){
					validateRegisters();
					// Debug info 
					if( (processedRegisters % 350) == 0){
						System.out.println("Validating Registers");
						System.out.println("numberOfRegisters:	" + numberOfRegisters);
						System.out.println("percent:    			" + percent);
						System.out.println("started:       		" + started);
						System.out.println("running:       		" + running);
						System.out.println("completed:     		" + isCompleted());
						System.out.println("processID: 			" + processID);
						System.out.println("processedRegisters:" + processedRegisters );
					}
			}
			
			// Debug info
			System.out.println("Validating Registers");
			System.out.println("numberOfRegisters:	" + numberOfRegisters);
			System.out.println("percent:    			" + percent);
			System.out.println("started:       		" + started);
			System.out.println("running:       		" + running);
			System.out.println("completed:     		" + isCompleted());
			System.out.println("processID: 			" + processID);
			System.out.println("processedRegisters:" + processedRegisters );
		
      } finally {
			setRunning(false);
      }

    }

}