package com.netro.fondojr;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.GenerarArchivoZip;
import netropology.utilerias.Seguridad;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

@Stateless(name = "FondoJuniorEJB", mappedName = "FondoJuniorEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class FondoJuniorBean implements FondoJunior {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(FondoJuniorBean.class);

	/**
	 * Obtiene Consulta general de registros cargados por el FIDE para Fondo Junior
	 * @throws com.netro.exception.NafinException
	 * @return List lstConsMonitor
	 * @param fec_fin - fecha de vencimiento final
	 * @param fec_ini - fecha de vencimiento inicial
	 */
	public List consultaMonitor(String fec_ini, String fec_fin, String estatusCarga, String chkDesembolsos,
										 String cvePrograma) throws NafinException {
		System.out.println("FondoJuniorBean::consultaMonitor(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		List lstConsMonitor = new ArrayList();
		List regMonitor = null;
		String qrySentence = "";
		String condFechas = "";
		String condFechas2 = ""; //FODEA 026-2010 FVR

		log.debug("fec_ini==" + fec_ini);
		log.debug("fec_fin==" + fec_fin);
		log.debug("estatusCarga==" + estatusCarga);

		try {
			con.conexionDB();

			/*---------------- SE OBTIENE REGISTROS DE BITACORA-------------------*/

			//if(fec_ini!=null && !fec_ini.equals("") && fec_fin!=null && !fec_fin.equals("")){
			if (fec_ini != null && !fec_ini.equals("") && fec_fin != null && !fec_fin.equals("")) {
				condFechas =
							  " AND  ((COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " +
							  " AND COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY')) OR (COM_FECHAPROBABLEPAGO IS NULL)) ";

				condFechas2 =
							  " AND PF.COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " +
							  " AND PF.COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY') ";
			}
			qrySentence =
						"SELECT  tmonitor.orden as morden, " + "		tmonitor.estatus as mestatus, " +
						"		tmonitor.fecha_venc as mfecha_venc, " + "        tmonitor.ultimo_reg as multimo_reg, " +
						"        tmonitor.total_reg as mtotal_reg, " + "        tmonitor.monto_tot as mmonto_tot, " +
						"        tmonitor.folio as mfolio, " + "        tmonitor.estatus_proc as mestatus_proc, " +
						"        tmonitor.tipo_reg as mtipo_reg, " +
						"			DECODE(tmonitor.orden,'1',tmonitor.tipo_reg,'2',tmonitor.fecha_venc,tmonitor.estatus) as mtipo_venc, " +
						"        tmonitor.usuario as musuario " + "FROM ( ";
			if ("V".equals(estatusCarga) || "B".equals(estatusCarga)) {
				qrySentence +=
							  " 			SELECT 1 orden, " + " 			DECODE(cg_tipo_registro,'Reembolsos al dia','BR', " +
							  "    									'Pagados y No Pagados','BP', " + "    									'Recuperaciones al dia','BRC', " +
							  "    									'Prepagos al dia','BT')estatus, " + "        DECODE(com_fechaprobablepago,null, " +
							  "               		TO_CHAR (DF_FECHA_BIT, 'DD/MM/YYYY'), " +
							  "               		TO_CHAR (com_fechaprobablepago, 'DD/MM/YYYY')) fecha_venc, " +
							  "         TO_CHAR (df_registro_fide, 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " +
							  "         ig_total_reg total_reg, " + "         fg_monto_total monto_tot, " + "         ig_folio folio,  " +
							  "         cg_estatus_carga estatus_proc, " + "         cg_tipo_registro tipo_reg,          " +
							  "         cg_usuario usuario  " + "    FROM bit_monitor_fondojr " + "		WHERE ic_programa_fondojr = ? " +
							  condFechas;
			}
			if ("N".equals(estatusCarga)) {
				//"UNION ALL " +
				qrySentence +=
							  "SELECT   2 orden, " + "		 'P' estatus, " +
							  "		 TO_CHAR (pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "         TO_CHAR (MAX (pf.df_registro_fide),'DD/MM/YYYY hh:mi:ss') ultimo_reg, " +
							  "         COUNT (pf.com_fechaprobablepago) total_reg, " +
							  "         SUM (pf.fg_totalvencimiento) monto_tot,  " + "         0  folio, " + "         'N' estatus_proc, " +
							  "         null tipo_reg, " + "         null usuario                  " + "    FROM com_pagos_fide pf " +
							  "   WHERE pf.cg_estatus IN ('P', 'NP') " + "	 	AND pf.ic_programa_fondojr = ? " + condFechas2 +
							  "GROUP BY pf.com_fechaprobablepago, pf.ig_folio " + "UNION ALL " + "SELECT   3 orden, " +
							  "		 decode(pf.cg_estatus,'R', 'Reembolsos al dia', 'T','Prepagos al dia' ) as estatus, " +
							  "		 TO_CHAR (SYSDATE, 'DD/MM/YYYY') fecha_venc, " +
							  "         TO_CHAR (MAX (pf.df_registro_fide), 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " +
							  "         COUNT (pf.cg_estatus) total_reg,  " + "         SUM (pf.fg_totalvencimiento) monto_tot, " +
							  "         0 folio, " + "         pf.cg_estatus_proc estatus_proc, " + "         null tipo_reg, " +
							  "         null usuario " + "    FROM com_pagos_fide pf " + "   WHERE pf.cg_estatus IN ('R', 'T') " +
							  "		AND pf.ic_programa_fondojr = ? " + "     AND 'RC' != " +
							  "            fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, " +
							  "                                        pf.ig_prestamo, " +
							  "                                        pf.ig_disposicion, " +
							  "                                        pf.fg_totalvencimiento " +
							  "                                       ) " + "GROUP BY pf.cg_estatus, pf.cg_estatus_proc  " + "UNION ALL " +
							  "SELECT   4 orden, " + "		 DECODE(DECODE(cp.cg_estatus ,'R','RC'),'RC','Recuperaciones al dia') estatus, " +
							  "		 TO_CHAR (SYSDATE, 'dd/mm/yyyy') fecha_venc, " +
							  "         TO_CHAR (MAX (cp.df_registro_fide),'dd/mm/yyyy hh:mi:ss') ultimo_reg, " +
							  "         COUNT (cp.cg_estatus) total_reg, " + "         SUM (cp.fg_totalvencimiento) monto_tot, " +
							  "         0 folio,           " + "         cp.cg_estatus_proc estatus_proc, " + "         null tipo_reg, " +
							  "         null usuario " + "    FROM com_pagos_fide cp " + "   WHERE cp.cg_estatus = 'R' " +
							  "		AND cp.ic_programa_fondojr = ? " + "     AND 'RC' = " +
							  "            fn_es_recuperacion_fondojr (cp.com_fechaprobablepago, " +
							  "                                        cp.ig_prestamo, " +
							  "                                        cp.ig_disposicion, " +
							  "                                        cp.fg_totalvencimiento " +
							  "                                       ) " + "GROUP BY cp.cg_estatus, cp.cg_estatus_proc ";
			}
			if ("NPR".equals(estatusCarga)) { //R:NP Rechazados -- F027-2011 FVR
				qrySentence +=
							  " SELECT   5 orden, " + "		 'NP Rechazados al dia' estatus, " +
							  "		 TO_CHAR (sysdate, 'DD/MM/YYYY') fecha_venc, " +
							  "         TO_CHAR (MAX (pf.df_registro_fide),'DD/MM/YYYY hh:mi:ss') ultimo_reg, " +
							  "         COUNT (pf.com_fechaprobablepago) total_reg, " +
							  "         SUM (pf.fg_totalvencimiento) monto_tot,  " + "         0  folio, " +
							  "         'NPR' estatus_proc, " + "         null tipo_reg, " + "         null usuario                  " +
							  "    FROM com_cred_np_rechazados pf " + "   WHERE pf.cg_estatus IN ('NPR') " +
							  "	 	AND pf.ic_programa_fondojr = ? " + "GROUP BY pf.cg_estatus ";
			} //if( "R".equals(estatusCarga) )
			qrySentence += ") tmonitor " + "order by tmonitor.orden, tmonitor.fecha_venc ";

			System.out.println("qrySentence>>>>>>" + qrySentence);
			ps = con.queryPrecompilado(qrySentence);
			int i = 0;

			if ("V".equals(estatusCarga) || "B".equals(estatusCarga)) {
				ps.setLong(++i, Long.parseLong(cvePrograma));
				if (!condFechas.equals("")) {
					ps.setString(++i, fec_ini);
					ps.setString(++i, fec_fin);
				}
			}

			if ("N".equals(estatusCarga)) {
				ps.setLong(++i, Long.parseLong(cvePrograma));
				if (!condFechas2.equals("")) {
					ps.setString(++i, fec_ini);
					ps.setString(++i, fec_fin);
				}

				ps.setLong(++i, Long.parseLong(cvePrograma));
				ps.setLong(++i, Long.parseLong(cvePrograma));
			}
			if ("NPR".equals(estatusCarga)) {
				ps.setLong(++i, Long.parseLong(cvePrograma));
			}

			rs = ps.executeQuery();
			while (rs != null && rs.next()) {
				regMonitor = new ArrayList();
				regMonitor.add(rs.getString("MESTATUS"));
				regMonitor.add(rs.getString("MTIPO_VENC"));
				/*if(  (rs.getString("MESTATUS").equals("BRC")) || (rs.getString("MESTATUS").equals("BT"))  )
					regMonitor.add(rs.getString("MTIPO_REG"));
				else
					regMonitor.add(rs.getString("MFECHA_VENC"));
				*/
				regMonitor.add(rs.getString("MULTIMO_REG"));
				regMonitor.add(rs.getString("MFOLIO"));
				regMonitor.add(rs.getString("MTOTAL_REG"));
				regMonitor.add(rs.getString("MMONTO_TOT"));
				regMonitor.add(rs.getString("MESTATUS_PROC"));
				lstConsMonitor.add(regMonitor);
			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			/* -------------------------------------------------------------------------------------------- */
			/* SE OBTIENE TODOS LOS VENCIMIENTOS(P,NP) QUE SE ENCUENTREN EN EL RANGO DE FECHAS ESTABLECIDOS */
			/* -------------------------------------------------------------------------------------------- */

			/* --------------------------------------------------------------------------------------------- */
			/* SE OBTIENE TODOS LOS VENCIMIENTOS(R,T) QUE SE ENCUENTREN EN LA TABLA(TMP) DE DEPOSITO DE FIDE */
			/* --------------------------------------------------------------------------------------------- */

			/* ------------------------------------------------------------------------- */
			/* SE DETERMINA CUALES DE LOS REEEMBOLOSOS SE CONSIDERAN COMO RECUPERACIONES */
			/* ------------------------------------------------------------------------- */


			System.out.println("FondoJuniorBean::consultaMonitor(S)");
			return lstConsMonitor;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("FondoJuniorBean::consultaMonitor(Error)");
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}

	/**
	 * Obtiene el detalle de cada registro arrojado en la consulta general de Monitor
	 * @throws com.netro.exception.NafinException
	 * @return List lstDetmonitor
	 * @param fec_vencimiento - fecha de vencimiento
	 */
	public List consDetMonitor(String cg_estatus, String fec_vencimiento, String cvePrograma) throws NafinException {
		System.out.println("FondoJuniorBean::consDetMonitor(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDetmonitor = new ArrayList();
		List lstRegDetMonitor = null;
		String qrySentence = "";
		boolean seValidaReg = false;
		boolean commit = true;

		try {
			con.conexionDB();
			if (!cg_estatus.equals("R") && !cg_estatus.equals("T") && !cg_estatus.equals("RT") &&
				 !cg_estatus.equals("NPR")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     decode(pf.cg_estatus_proc,'R','NPR',pf.cg_estatus) cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " + //F027-2011 FVR AJUSTE NPR
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM com_pagos_fide pf " +
							  "   WHERE pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') " + "   AND pf.cg_estatus in ('P','NP') " +
							  "   AND pf.cg_estatus_proc in ('N','R') " + "	 AND pf.ic_programa_fondojr = ? " +
							  "   ORDER BY pf.df_registro_fide ";
				seValidaReg = true;
			} else if (cg_estatus.equals("R")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM COM_PAGOS_FIDE PF  " +
							  "   WHERE PF.CG_ESTATUS = ?  ";
				if (!"".equals(fec_vencimiento)) {
					qrySentence += "  AND pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') ";
				}
				qrySentence +=
							  "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND CG_ESTATUS_PROC = 'N'  " +
							  "	 AND pf.ic_programa_fondojr = ? " + "   ORDER BY pf.df_registro_fide ";
			} else if (cg_estatus.equals("T")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM COM_PAGOS_FIDE PF  " +
							  "   WHERE PF.CG_ESTATUS = ?  " +
							  "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND pf.CG_ESTATUS_PROC = 'N'  " +
							  "	 AND pf.ic_programa_fondojr = ? " + "   ORDER BY pf.df_registro_fide ";
			} else if (cg_estatus.equals("RT")) {
				//cg_estatus = "R";
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "  FROM com_pagos_fide pf  " +
							  " WHERE pf.cg_estatus = ?  " + " AND pf.cg_estatus_proc = 'N' " +
							  " AND 'RC' = fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,  " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento)  " + " AND pf.ic_programa_fondojr = ? " +
							  "   ORDER BY pf.df_registro_fide ";

			} else if (cg_estatus.equals("NPR")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " + //F027-2011 FVR AJUSTE NPR
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide " +
							  "    FROM com_cred_np_rechazados pf " + "   WHERE pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') " +
							  "   AND pf.cg_estatus in ('NPR') " + "	 AND pf.ic_programa_fondojr = ? " +
							  "   ORDER BY pf.df_registro_fide ";
			}

			System.out.println("consDetMonitor:qrySentence>>" + qrySentence);
			ps = con.queryPrecompilado(qrySentence);
			int x = 0;
			if (!cg_estatus.equals("R") && !cg_estatus.equals("T") && !cg_estatus.equals("RT")) {
				ps.setString(++x, fec_vencimiento);
			} else if (cg_estatus.equals("R")) {
				ps.setString(++x, cg_estatus);
				if (!"".equals(fec_vencimiento)) {
					ps.setString(++x, fec_vencimiento);
				}
			} else if (cg_estatus.equals("T")) {
				ps.setString(++x, cg_estatus);
			} else if (cg_estatus.equals("RT")) {
				cg_estatus = "R";
				ps.setString(++x, cg_estatus);
			} else if (cg_estatus.equals("NPR")) {
				ps.setString(++x, fec_vencimiento);
			}
			ps.setLong(++x, Long.parseLong(cvePrograma));

			rs = ps.executeQuery();
			while (rs != null && rs.next()) {
				lstRegDetMonitor = new ArrayList();
				String estatus = rs.getString("cg_estatus") == null ? "" : rs.getString("cg_estatus");

				if ("NP".equals(rs.getString("cg_estatus"))) {
					if ("N".equals(rs.getString("estatus_proc")) && seValidaReg) {
						String statusProc =
											setAmortRechazada(con, rs.getString("ig_prestamo"), rs.getString("fecha_venc"),
																	rs.getString("ig_disposicion"), rs.getString("fg_totalvencimiento"),
																	cvePrograma);
						estatus = "R".equals(statusProc) ? "NPR" : estatus;
					}
				}

				/*0*/lstRegDetMonitor.add(rs.getString("fecha_venc") == null ? "" : rs.getString("fecha_venc"));
				/*1*/lstRegDetMonitor.add(rs.getString("ig_prestamo") == null ? "" : rs.getString("ig_prestamo"));
				/*2*/lstRegDetMonitor.add(rs.getString("ig_cliente") == null ? "" : rs.getString("ig_cliente"));
				/*3*/lstRegDetMonitor.add(rs.getString("ig_disposicion") == null ? "" : rs.getString("ig_disposicion"));
				/*4*/lstRegDetMonitor.add(rs.getString("fg_amortizacion") == null ? "" : rs.getString("fg_amortizacion"));
				/*5*/lstRegDetMonitor.add(rs.getString("fg_interes") == null ? "" : rs.getString("fg_interes"));
				/*6*/lstRegDetMonitor.add(rs.getString("fg_totalvencimiento") == null ? "" :
												  rs.getString("fg_totalvencimiento"));
				/*7*/lstRegDetMonitor.add(rs.getString("df_pago_cliente") == null ? "" : rs.getString("df_pago_cliente"));
				/*8*/lstRegDetMonitor.add(estatus);
				/*9*/lstRegDetMonitor.add(rs.getString("df_periodofin") == null ? "" : rs.getString("df_periodofin"));
				/*10*/lstRegDetMonitor.add(rs.getString("ig_numero_docto") == null ? "" : rs.getString("ig_numero_docto"));
				/*11*/lstRegDetMonitor.add(rs.getString("df_registro_fide") == null ? "" :
													rs.getString("df_registro_fide"));
				lstDetmonitor.add(lstRegDetMonitor);

			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			System.out.println("FondoJuniorBean::consDetMonitor(S)");
			return lstDetmonitor;
		} catch (Exception e) {
			commit = false;
			e.printStackTrace();
			System.out.println("FondoJuniorBean::consDetMonitor(Error)");
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Realiza validacion de los registros Pagados y No Pagados
	 * que son Enviados por el FIDE
	 * @throws com.netro.exception.NafinException
	 * @return Lista que contiene el resumen de la validacion de los rregisrtros
	 * @param fec_vencimiento
	 */
	public List validaMonitorPyNP(String fec_vencimiento, String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		ResultSet rs = null, rs2 = null;
		PreparedStatement ps = null, ps2 = null;

		List lstRegValidados = new ArrayList();
		List lstValidados = new ArrayList();
		StringBuffer txtError = new StringBuffer();
		StringBuffer qrySentence = new StringBuffer();
		String qryError = "";

		BigDecimal montoTotalVista = new BigDecimal("0.00");
		BigDecimal montoTotalPagados = new BigDecimal("0.00");
		BigDecimal montoTotalNoPagados = new BigDecimal("0.00");
		BigDecimal montoTotalNoPagadosR = new BigDecimal("0.00");
		BigDecimal montoTotalTMP = new BigDecimal("0.00");
		BigDecimal montoTotalReemb = new BigDecimal("0.00");
		BigDecimal montoTotalError = new BigDecimal("0.00");
		BigDecimal montoTotalRec = new BigDecimal("0.00");
		BigDecimal montoTotalNoEncon = new BigDecimal("0.00");
		BigDecimal montoTotalNoEnv = new BigDecimal("0.00");
		BigDecimal montoTotalPrepago = new BigDecimal("0.00");

		int sumaTotalVista = 0;
		int sumaTotalPagados = 0;
		int sumaTotalNoPagados = 0;
		int sumaTotalNoPagadosR = 0; //F027-2011 FVR
		int sumaTotalTMP = 0;
		int sumaTotalReemb = 0;
		int sumaTotalError = 0;
		int sumaTotalRec = 0;
		int sumaTotalNoEncon = 0;
		int sumaTotalNoEnv = 0;
		int sumaTotalPrepago =
				 0; //variable urilizada para los prestamos que ya fueron prepagados  mas no para registros con estatus de prepagados 'T'
		boolean realizaTransaccion = true;
		boolean regNoEncon = false;
		boolean regPrepagado = false;

		try {

			con.conexionDB();

			/*----------------------------------------------------------------------------------*/
			/*SE OBTIENE TOTAL DE REGISTROS DE LA VISTA DE VENCIMIENTOS POR FECHA DE VENCIMEINTO*/
			/*----------------------------------------------------------------------------------*/
			qrySentence.append("select count(1) total_reg, sum(fg_totalvencimiento) totalvencimiento from comvis_vencimiento_fide ");
			qrySentence.append("where com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
			qrySentence.append("and ic_programa_fondojr = ? "); //SE AGREGA POR CORRECCIONES DEL MES DEL 09/2010
			qrySentence.append("group by com_fechaprobablepago ");
			qrySentence.append("order by com_fechaprobablepago ");

			ps = con.queryPrecompilado(qrySentence.toString());
			ps.setString(1, fec_vencimiento);
			ps.setLong(2, Long.parseLong(cvePrograma)); //SE AGREGA POR CORRECCIONES DEL MES DEL 09/2010
			rs = ps.executeQuery();
			if (rs != null && rs.next()) {
				lstRegValidados = new ArrayList();
				lstRegValidados.add(rs.getString("total_reg"));
				lstRegValidados.add(rs.getString("totalvencimiento"));
				lstRegValidados.add("vista");
				lstValidados.add(lstRegValidados);

				sumaTotalVista = Integer.parseInt(rs.getString("total_reg"));
				montoTotalVista = montoTotalVista.add(new BigDecimal(rs.getString("totalvencimiento")));
			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			/*---------------------------------------------------------------------------------*/
			/*--------se establece query para actualizar registros que tengan errores----------*/
			/*---------------------------------------------------------------------------------*/
			qryError =
						"update com_pagos_fide set cg_error = ? " + "where com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
						"and ig_prestamo = ? " + "and ig_cliente = ? " + "and fg_totalvencimiento = ? " + "and ig_disposicion = ? " +
						"and ic_programa_fondojr = ? "; //FODEA 26-2010 FVR

			/*------------------------------------------------------------------------*/
			/*SE OBTIENE REGISTROS DE TABLA TEMPORAL POR FECHA DE VENCIMEINTO*/
			/*------------------------------------------------------------------------*/
			qrySentence.delete(0, qrySentence.length());
			qrySentence.append("select to_char(com_fechaprobablepago,'dd/mm/yyyy') fec_venc, fg_totalvencimiento totalvencimiento, ");
			qrySentence.append("ig_prestamo prestamo, ig_cliente cliente, cg_estatus estatus, ig_disposicion disposicion, cg_estatus_proc estatus_proc ");
			qrySentence.append("from COM_PAGOS_FIDE ");
			qrySentence.append("where com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') ");
			qrySentence.append("and cg_estatus IN ('P', 'NP') ");
			qrySentence.append("and cg_estatus_proc in ('N','R', 'P') "); //modificaicon F006-2014 req 1
			qrySentence.append("and ic_programa_fondojr = ? "); //FODEA 026-2010 FVR


			System.out.println("consDetMonitor:qrySentence>>" + qrySentence.toString());
			ps = con.queryPrecompilado(qrySentence.toString());
			ps.setString(1, fec_vencimiento);
			ps.setLong(2, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					regNoEncon = false;
					if (txtError.length() > 0)
						txtError.delete(0, txtError.length());
					/*---------------------------------*/
					/*SE REALIZA VALIDACIONES DE CAMPOS*/
					/*---------------------------------*/
					if (rs.getString("totalvencimiento") == null || !Comunes.esDecimal(rs.getString("totalvencimiento")))
						txtError.append("El campo: \"monto de Vencimiento\" no tiene un n�mero v�lido/");
					if (rs.getString("prestamo") == null || !Comunes.esNumero(rs.getString("prestamo")))
						txtError.append("El campo: \"prestamo\" no tiene un n�mero v�lido/");
					if (rs.getString("cliente") == null || !Comunes.esNumero(rs.getString("cliente")))
						txtError.append("El campo: \"cliente\" no tiene un n�mero v�lido/");
					if (rs.getString("estatus") == null ||
						 (!(rs.getString("estatus")).equals("P") && !(rs.getString("estatus")).equals("NP") &&
						  !(rs.getString("estatus")).equals("R") && !(rs.getString("estatus")).equals("T")))
						txtError.append("El campo: \"estatus\" es invalido/");

					//se incluye validacion para determinar prestamos prepagados
					regPrepagado =
									 validaPrestamoPrepagado(con, rs.getString("prestamo"), rs.getString("fec_venc"),
																	 rs.getString("disposicion"), rs.getString("totalvencimiento"),
																	 cvePrograma);
					if (regPrepagado) {
						sumaTotalPrepago++;
						montoTotalPrepago = montoTotalPrepago.add(new BigDecimal(rs.getString("totalvencimiento")));

					} else if (rs.getString("estatus").equals("P")) {
						sumaTotalPagados++;
						montoTotalPagados = montoTotalPagados.add(new BigDecimal(rs.getString("totalvencimiento")));

					} else if (rs.getString("estatus").equals("NP") && rs.getString("estatus_proc").equals("N")) {

						//Ini - logica para determinar si se cataloga al vencimiento como rechazado (posible NPR)
						String statusp =
											setAmortRechazada(con, rs.getString("prestamo"), rs.getString("fec_venc"),
																	rs.getString("disposicion"), rs.getString("totalvencimiento"),
																	cvePrograma);
						//Fin - logica para determinar si se cataloga al vencimiento como rechazado (posible NPR)

						if ("N".equals(statusp)) {
							sumaTotalNoPagados++;
							montoTotalNoPagados = montoTotalNoPagados.add(new BigDecimal(rs.getString("totalvencimiento")));
						} else if ("R".equals(statusp)) {
							sumaTotalNoPagadosR++;
							montoTotalNoPagadosR = montoTotalNoPagadosR.add(new BigDecimal(rs.getString("totalvencimiento")));
						}

					} else if (rs.getString("estatus").equals("NP") && rs.getString("estatus_proc").equals("R")) {
						sumaTotalNoPagadosR++;
						montoTotalNoPagadosR = montoTotalNoPagadosR.add(new BigDecimal(rs.getString("totalvencimiento")));
					}


					//SE INTEGRA VALIDACION DE AMORTIZACIONES YA EXISTENTES EN PANTALLA DE VALIDACION
					if (rs.getString("estatus").equals("P") || rs.getString("estatus").equals("NP")) {
						String qryExisteEnValidacion =
											"SELECT cs_validacion " + "  FROM com_pagos_fide_val " +
											" WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " + "   AND ig_prestamo = ? " +
											"   AND ig_disposicion = ? " + "   AND fg_totalvencimiento = ? " + "   AND cg_estatus = ? " +
											"   AND cs_validacion in (?, ?) " + " AND rownum = 1 ";

						ps2 = con.queryPrecompilado(qryExisteEnValidacion);
						ps2.setString(1, rs.getString("fec_venc"));
						ps2.setLong(2, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(3, Long.parseLong(rs.getString("disposicion")));
						ps2.setBigDecimal(4, new BigDecimal(rs.getString("totalvencimiento")));
						ps2.setString(5, rs.getString("estatus"));
						ps2.setString(6, "P");
						ps2.setString(7, "A");
						rs2 = ps2.executeQuery();
						if (rs2 != null && rs2.next()) {
							String csValidacion = rs2.getString("cs_validacion");
							if ("P".equals(csValidacion)) {
								txtError.append("El Numero de Prestamo " + rs.getString("prestamo") + " con Amortizacion " +
													 rs.getString("disposicion") +
													 " fue registrado anteriormente, se encuentra con estatus En Proceso/");
							}
							if ("A".equals(csValidacion)) {
								txtError.append("El Numero de Prestamo " + rs.getString("prestamo") + " con Amortizacion " +
													 rs.getString("disposicion") +
													 " fue registrado anteriormente, se encuentra con estatus Validado/");
							}
						}
						rs2.close();
						ps2.close();
					}


					if (txtError.length() > 0) {
						sumaTotalError++;
						montoTotalError =
											montoTotalError.add(new BigDecimal(rs.getString("totalvencimiento") != null ?
																						  rs.getString("totalvencimiento") : "0"));

						ps2 = con.queryPrecompilado(qryError);
						ps2.setString(1, txtError.toString());
						ps2.setString(2, rs.getString("fec_venc"));
						ps2.setLong(3, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(4, Long.parseLong(rs.getString("cliente")));
						ps2.setBigDecimal(5, new BigDecimal(rs.getString("totalvencimiento")));
						ps2.setLong(6, Long.parseLong(rs.getString("disposicion")));
						ps2.setLong(7, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR

						ps2.executeUpdate();
						if (rs2 != null)
							rs2.close();
						if (ps2 != null)
							ps2.close();
					}
					/*fin de validacion*/
				} //cierre while de validaciones
			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			/* SE OBTIENEN REGISTROS QUE REGRESO EL FIDE PERO NO SE ENCUENTRAN EN LA VISTA */
			String qryNoEncon =
						"SELECT TO_CHAR (pf.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc, " +
						"       pf.ig_prestamo prestamo, pf.ig_cliente cliente, " +
						"       pf.ig_disposicion disposicion, pf.fg_totalvencimiento totalvencimiento  " + "  FROM com_pagos_fide pf " +
						" WHERE NOT EXISTS ( " + "          SELECT vp.ig_prestamo " + "            FROM comvis_vencimiento_fide vp " +
						"           WHERE vp.com_fechaprobablepago = pf.com_fechaprobablepago " +
						"             AND vp.ig_prestamo = pf.ig_prestamo " + "             AND vp.ig_cliente = pf.ig_cliente " +
						"             AND vp.ig_disposicion = pf.ig_disposicion " +
						"             AND vp.fg_totalvencimiento = pf.fg_totalvencimiento) " +
						"   AND pf.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " + "   AND pf.cg_estatus_proc in ('N','R','P') " +
						"   AND pf.cg_estatus in ('P','NP') " + "	 AND ic_programa_fondojr = ? "; //FODEA 026-2010 FVR
			ps = con.queryPrecompilado(qryNoEncon);
			ps.setString(1, fec_vencimiento);
			ps.setLong(2, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					String txt_error = "Numero de Prestamo invalido, Registro no Encontrado/";
					ps2 = con.queryPrecompilado(qryError);
					ps2.setString(1, txt_error);
					ps2.setString(2, rs.getString("fec_venc"));
					ps2.setLong(3, Long.parseLong(rs.getString("prestamo")));
					ps2.setLong(4, Long.parseLong(rs.getString("cliente")));
					ps2.setBigDecimal(5, new BigDecimal(rs.getString("totalvencimiento")));
					ps2.setLong(6, Long.parseLong(rs.getString("disposicion")));
					ps2.setLong(7, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
					ps2.executeUpdate();
					if (ps2 != null)
						ps2.close();

					sumaTotalNoEncon++;
					montoTotalNoEncon = montoTotalNoEncon.add(new BigDecimal(rs.getString("totalvencimiento")));
					sumaTotalError++;
					montoTotalError = montoTotalError.add(new BigDecimal(rs.getString("totalvencimiento")));
				} //cierre while
			} //cierre if rs
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();


			/* SE OBTIENE REGISTROS QUE NO FUERON REGERSADOS POR EL FIDE PERO SI SE ENCUENTRAN EN LA VISTA */
			String qryNoEnviados =
						"SELECT /*+ index( v CP_COM_VENCIMIENTO_FIDE_PK) */ " +
						"       v.com_fechaprobablepago fec_venc, v.ig_prestamo prestamo, " +
						"       v.ig_cliente cliente, v.fg_totalvencimiento totalvencimiento " + "  FROM comvis_vencimiento_fide v " +
						" WHERE v.com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " + "   AND v.ic_programa_fondojr = ? " +
						"   AND NOT EXISTS ( " + "          SELECT 1 " + "            FROM com_pagos_fide c " +
						"           WHERE c.com_fechaprobablepago = v.com_fechaprobablepago " +
						"             AND c.ig_prestamo = v.ig_prestamo " + "             AND c.ig_cliente = v.ig_cliente " +
						"             AND c.ig_disposicion = v.ig_disposicion " +
						"             AND c.fg_totalvencimiento = v.fg_totalvencimiento " +
						"   			  AND c.cg_estatus_proc in ('N','R','P') " + "             AND c.cg_estatus IN ('P', 'NP') " +
						"             AND c.ic_programa_fondojr = ?) ";


			ps = con.queryPrecompilado(qryNoEnviados);
			ps.setString(1, fec_vencimiento);
			ps.setLong(2, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
			ps.setLong(3, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
			rs = ps.executeQuery();

			if (rs != null)
				while (rs.next()) {
					sumaTotalNoEnv++;
					montoTotalNoEnv = montoTotalNoEnv.add(new BigDecimal(rs.getString("totalvencimiento")));
					sumaTotalError++;
					montoTotalError = montoTotalError.add(new BigDecimal(rs.getString("totalvencimiento")));
				}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			sumaTotalTMP = sumaTotalPagados + sumaTotalNoPagados + sumaTotalNoPagadosR + sumaTotalReemb + sumaTotalPrepago;
			montoTotalTMP = montoTotalTMP.add(montoTotalPagados);
			montoTotalTMP = montoTotalTMP.add(montoTotalNoPagados);
			montoTotalTMP = montoTotalTMP.add(montoTotalReemb);
			montoTotalTMP = montoTotalTMP.add(montoTotalNoPagadosR);
			montoTotalTMP = montoTotalTMP.add(montoTotalPrepago);

			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalPagados));
			lstRegValidados.add(montoTotalPagados.toPlainString());
			lstRegValidados.add("pagados");
			lstValidados.add(lstRegValidados);
			/**/
			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalNoPagados));
			lstRegValidados.add(montoTotalNoPagados.toPlainString());
			lstRegValidados.add("no_pagados");
			lstValidados.add(lstRegValidados);
			/**/
			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalTMP));
			lstRegValidados.add(montoTotalTMP.toPlainString());
			lstRegValidados.add("tmp");
			lstValidados.add(lstRegValidados);
			/**/
			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalReemb));
			lstRegValidados.add(montoTotalReemb.toPlainString());
			lstRegValidados.add("reemb");
			lstValidados.add(lstRegValidados);
			/**/
			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalError));
			lstRegValidados.add(montoTotalError.toPlainString());
			lstRegValidados.add("error");
			lstValidados.add(lstRegValidados);
			/**/
			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalRec));
			lstRegValidados.add(montoTotalRec.toPlainString());
			lstRegValidados.add("recuperacion");
			lstValidados.add(lstRegValidados);
			/**/
			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalNoEncon));
			lstRegValidados.add(montoTotalNoEncon.toPlainString());
			lstRegValidados.add("no_encontrados");
			lstValidados.add(lstRegValidados);
			/**/
			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalNoEnv));
			lstRegValidados.add(montoTotalNoEnv.toPlainString());
			lstRegValidados.add("no_enviados");
			lstValidados.add(lstRegValidados);
			/**/
			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalPrepago));
			lstRegValidados.add(montoTotalPrepago.toPlainString());
			lstRegValidados.add("prepagos");
			lstValidados.add(lstRegValidados);
			/**/
			lstRegValidados = new ArrayList();
			lstRegValidados.add(String.valueOf(sumaTotalNoPagadosR));
			lstRegValidados.add(montoTotalNoPagadosR.toPlainString());
			lstRegValidados.add("no_pagados_rech");
			lstValidados.add(lstRegValidados);
			/**/

			return lstValidados;
		} catch (Exception e) {
			realizaTransaccion = false;
			System.out.println("FondoJuniorBean::validaMonitor(error)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("FondoJuniorBean::validaMonitorPyNP(S)");
			con.terminaTransaccion(realizaTransaccion);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}

	/**
	 * Realiza validacion de los registros sean Recuperaciones y Prepagos
	 * que son Enviados por el FIDE
	 * @throws com.netro.exception.NafinException
	 * @return Lista que contiene el resumen de la validacion de los regisrtros
	 * @param cg_estatus
	 */
	public List validaMonitorRCyT(String cg_estatus, String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;
		StringBuffer txtError = new StringBuffer();
		StringBuffer qrySentence = new StringBuffer();
		List lstRegValidados = new ArrayList();
		List lstValidados = new ArrayList();
		BigDecimal montoTotalPagados = new BigDecimal("0.00");
		BigDecimal montoTotalNoPagados = new BigDecimal("0.00");
		BigDecimal montoTotalTMP = new BigDecimal("0.00");
		BigDecimal montoTotalReemb = new BigDecimal("0.00");
		BigDecimal montoTotalError = new BigDecimal("0.00");
		BigDecimal montoTotalRec = new BigDecimal("0.00");
		BigDecimal montoTotalNoEncon = new BigDecimal("0.00");
		BigDecimal montoTotalNoEnv = new BigDecimal("0.00");
		BigDecimal montoTotalPrepago = new BigDecimal("0.00");
		int sumaTotalPagados = 0;
		int sumaTotalNoPagados = 0;
		int sumaTotalTMP = 0;
		int sumaTotalReemb = 0;
		int sumaTotalError = 0;
		int sumaTotalRec = 0;
		int sumaTotalNoEncon = 0;
		int sumaTotalNoEnv = 0;
		int sumaTotalPrepago = 0;
		boolean realizaTransaccion = true;
		boolean regNoEncon = false;

		try {

			con.conexionDB();

			cg_estatus = cg_estatus.equals("RC") ? "R" : cg_estatus;

			String qryError =
						"update com_pagos_fide set cg_error = ? " + "where com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
						"and ig_prestamo = ? " + "and ig_cliente = ? " + "and fg_totalvencimiento = ? " + "and ig_disposicion = ? " +
						"and ic_programa_fondojr = ? "; //FODEA 026-2010 FVR

			/*------------------------------------------------------------------------*/
			/*SE OBTIENE REGISTROS DE TABLA TEMPORAL POR FECHA DE VENCIMEINTO*/
			/*------------------------------------------------------------------------*/
			qrySentence.append("select to_char(com_fechaprobablepago,'dd/mm/yyyy') fec_venc, fg_totalvencimiento totalvencimiento, ");
			qrySentence.append("ig_prestamo prestamo, ig_cliente cliente, cg_estatus estatus, ig_disposicion disposicion ");
			qrySentence.append("from COM_PAGOS_FIDE ");
			qrySentence.append("where cg_estatus IN ( ? ) "); //MODIFICACION POR FODEA 057-2009 FVR
			qrySentence.append("and cg_estatus_proc = 'N' ");
			if ("T".equals(cg_estatus))
				qrySentence.append("and 'RC' != ");
			else
				qrySentence.append("and 'RC' = ");
			qrySentence.append("fn_es_recuperacion_fondojr (com_fechaprobablepago, ");
			qrySentence.append("ig_prestamo, ig_disposicion, fg_totalvencimiento) ");
			qrySentence.append("and ic_programa_fondojr = ? "); //FODEA 026-2010 FVR

			System.out.println("consDetMonitor:qrySentence>>" + qrySentence.toString());
			ps = con.queryPrecompilado(qrySentence.toString());
			ps.setString(1, cg_estatus); //MODIFICACION POR FODEA 057-2009 FVR
			ps.setLong(2, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					regNoEncon = false;
					if (txtError.length() > 0)
						txtError.delete(0, txtError.length());
					/*---------------------------------*/
					/*SE REALIZA VALIDACIONES DE CAMPOS*/
					/*---------------------------------*/
					if (rs.getString("totalvencimiento") == null || !Comunes.esDecimal(rs.getString("totalvencimiento")))
						txtError.append("El campo: \"monto de Vencimiento\" no tiene un n�mero v�lido/");
					if (rs.getString("prestamo") == null || !Comunes.esNumero(rs.getString("prestamo")))
						txtError.append("El campo: \"prestamo\" no tiene un n�mero v�lido/");
					if (rs.getString("cliente") == null || !Comunes.esNumero(rs.getString("cliente")))
						txtError.append("El campo: \"cliente\" no tiene un n�mero v�lido/");
					if (rs.getString("estatus") == null ||
						 (!(rs.getString("estatus")).equals("P") && !(rs.getString("estatus")).equals("NP") &&
						  !(rs.getString("estatus")).equals("R") && !(rs.getString("estatus")).equals("T")))
						txtError.append("El campo: \"estatus\" es invalido/");


					if (rs.getString("estatus").equals("T")) { //Prepagos Totales
						sumaTotalPrepago++;
						montoTotalPrepago = montoTotalPrepago.add(new BigDecimal(rs.getString("totalvencimiento")));
					}


					if (rs.getString("estatus").equals("R")) { //CRXF
						boolean exist = false;

						String qryCompruebaReemb =
											"select fn_es_recuperacion_fondojr (to_date(?,'dd/mm/yyyy'), ?, ?, ?) validaReemb " +
											"from dual ";
						ps2 = con.queryPrecompilado(qryCompruebaReemb.toString());
						ps2.setString(1, rs.getString("fec_venc"));
						ps2.setLong(2, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(3, Long.parseLong(rs.getString("disposicion")));
						//ps2.setLong(3, Long.parseLong(rs.getString("cliente")));
						ps2.setBigDecimal(4, new BigDecimal(rs.getString("totalvencimiento")));

						rs2 = ps2.executeQuery();
						if (rs2 != null) {
											//System.out.println("entra a estatus reembolso");
											while (rs2.next()) {
								if (rs2.getString("validaReemb").equals("RC")) {
									exist = true;
									sumaTotalRec++;
									montoTotalRec = montoTotalRec.add(new BigDecimal(rs.getString("totalvencimiento")));
								} else if (rs2.getString("validaReemb").equals("ERROR")) {
									//System.out.println("entra a error reembolso");
									txtError.append("No existe registro para aplicar Reembolso/");
								}
							} //cierre while rs2
						}
						if (!exist) {
							sumaTotalReemb++;
							montoTotalReemb = montoTotalReemb.add(new BigDecimal(rs.getString("totalvencimiento")));
						}

						if (rs2 != null)
							rs2.close();
						if (ps2 != null)
							ps2.close();
					} //cierre if (estatus R)

					if (txtError.length() > 0) {
									 //System.out.println("si entro a error>>>>"+txtError.toString());
									 sumaTotalError++;
						montoTotalError =
											montoTotalError.add(new BigDecimal(rs.getString("totalvencimiento") != null ?
																						  rs.getString("totalvencimiento") : "0"));
						//}
						ps2 = con.queryPrecompilado(qryError);
						ps2.setString(1, txtError.toString());
						ps2.setString(2, rs.getString("fec_venc"));
						ps2.setLong(3, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(4, Long.parseLong(rs.getString("cliente")));
						ps2.setBigDecimal(5, new BigDecimal(rs.getString("totalvencimiento")));
						ps2.setLong(6, Long.parseLong(rs.getString("disposicion")));
						ps2.setLong(7, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
						ps2.executeUpdate();
						if (rs2 != null)
							rs2.close();
						if (ps2 != null)
							ps2.close();
					}
					/*fin de validacion*/
				} //cierre while de validaciones


				sumaTotalTMP = sumaTotalRec + sumaTotalPrepago;
				montoTotalTMP = montoTotalTMP.add(montoTotalRec);
				montoTotalTMP = montoTotalTMP.add(montoTotalPrepago);

				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalPagados));
				lstRegValidados.add(montoTotalPagados.toPlainString());
				lstRegValidados.add("pagados");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalNoPagados));
				lstRegValidados.add(montoTotalNoPagados.toPlainString());
				lstRegValidados.add("no_pagados");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalTMP));
				lstRegValidados.add(montoTotalTMP.toPlainString());
				lstRegValidados.add("tmp");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalReemb));
				lstRegValidados.add(montoTotalReemb.toPlainString());
				lstRegValidados.add("reemb");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalError));
				lstRegValidados.add(montoTotalError.toPlainString());
				lstRegValidados.add("error");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalRec));
				lstRegValidados.add(montoTotalRec.toPlainString());
				lstRegValidados.add("recuperacion");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalNoEncon));
				lstRegValidados.add(montoTotalNoEncon.toPlainString());
				lstRegValidados.add("no_encontrados");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalNoEnv));
				lstRegValidados.add(montoTotalNoEnv.toPlainString());
				lstRegValidados.add("no_enviados");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalPrepago));
				lstRegValidados.add(montoTotalPrepago.toPlainString());
				lstRegValidados.add("prepagos");
				lstValidados.add(lstRegValidados);


			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			return lstValidados;
		} catch (Exception e) {
			realizaTransaccion = false;
			System.out.println("FondoJuniorBean::validaMonitor(error)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(realizaTransaccion);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}


	public List autorizaValidacionMonitor(FondoJrMonitorBean formaMonitor, boolean validCert,
													  String cvePrograma) throws NafinException {
		List lstAcuseGral = new ArrayList();
		try {

			lstAcuseGral = autorizaValidacionMonitor(formaMonitor, validCert, cvePrograma, "");
			return lstAcuseGral;

		} catch (Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}

	}

	/**
	 * LLeva a cabo la autorizacion de aquellso registro que ya fueron validados
	 * y no presentaron error, transpasandolos a la tabla de trabajo
	 * @throws com.netro.exception.NafinException
	 * @return List
	 * @param formaMonitor - Bean que contiene la informacion necesaria para realizar la autorizacion
	 */
	public List autorizaValidacionMonitor(FondoJrMonitorBean formaMonitor, boolean validCert, String cvePrograma,
													  String version) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null;
		List lstAcuseGral = new ArrayList();
		String tabla = "";
		String condicionGarl = "";
		String cmp_cgEstatus = "";
		String bit_group = "";
		String bit_campo = "";
		boolean transaccion = true;
		try {
			con.conexionDB();
			char getReceipt = 'Y';
			String qrySequence = "select seq_com_venc_fondo_jr.nextval folio from dual ";

			System.out.println("formaMonitor>>>>>>" + formaMonitor.toString());
			if ((!validCert) ||
				 (!formaMonitor.getSerial().equals("") && !formaMonitor.getTextoFirmado().equals("") &&
				  !formaMonitor.getPkcs7().equals(""))) {
				Seguridad s = new Seguridad();

				/*SE OBTIENE NUMERO DE FOLIO MEDIANTE UN SEQUENCE*/
				rs = con.queryDB(qrySequence);
				if (rs != null && rs.next())
					formaMonitor.setFolio(rs.getString("folio"));
				rs.close();

				if ((!validCert) ||
					 (s.autenticar(formaMonitor.getFolio(), formaMonitor.getSerial(), formaMonitor.getPkcs7(),
										formaMonitor.getTextoFirmado(), getReceipt))) {

					//FODEA 026-2010 FVR (SE CAMBIA CONDICION DE 'R' por 'RC')
					if ((formaMonitor.getFec_venc()).equals("T")) { //MODIFICACION POR FODDEA 057-2009 FVR
						tabla = "com_pagos_fide_val ";
						condicionGarl =
											"where cp.cg_estatus IN (?) " + "	and cp.cg_estatus_proc = ? " +
											"	and cp.ic_programa_fondojr = ? "; //se agrega por error en produccion
						cmp_cgEstatus =
											" DECODE(cp.cg_estatus,'R',fn_es_recuperacion_fondojr (cp.com_fechaprobablepago, cp.ig_prestamo, cp.ig_disposicion, cp.fg_totalvencimiento),cp.cg_estatus) estatus ";
						bit_group = " group by pf.ig_folio, pf.cg_estatus, pf.cg_usuario, pf.ic_programa_fondojr ";
						bit_campo = " pf.cg_estatus estatus, ";
					} else if ((formaMonitor.getFec_venc()).equals("RC")) {
						tabla = "com_pagos_fide_val ";
						condicionGarl =
											"where cp.cg_estatus IN (?) " + "	and cp.cg_estatus_proc = ? " +
											"	and cp.ic_programa_fondojr = ? " + //se agrega por error en produccion
											"	and 'RC' = " + "         fn_es_recuperacion_fondojr (cp.com_fechaprobablepago, " +
											"                                     cp.ig_prestamo, " +
											"                                     cp.ig_disposicion, " +
											"                                     cp.fg_totalvencimiento " +
											"                                    )   ";

						cmp_cgEstatus =
											" DECODE(cp.cg_estatus,'R',fn_es_recuperacion_fondojr (cp.com_fechaprobablepago, cp.ig_prestamo, cp.ig_disposicion, cp.fg_totalvencimiento),cp.cg_estatus) estatus ";
						bit_group = " group by pf.ig_folio, pf.cg_estatus, pf.cg_usuario, pf.ic_programa_fondojr ";
						bit_campo = " pf.cg_estatus estatus, ";
					} else if ("R".equals(formaMonitor.getEstatus_cred())) {
						tabla = "com_pagos_fide_val ";
						condicionGarl =
											"where cp.cg_estatus IN (?) " + "	and cp.cg_estatus_proc = ? " +
											"	and cp.ic_programa_fondojr = ? "; //se agrega por error en produccion
						if (!"R".equals(formaMonitor.getFec_venc())) {
							condicionGarl += "	and cp.com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') ";
						}
						condicionGarl +=
											"	and 'RC' != " + "         fn_es_recuperacion_fondojr (cp.com_fechaprobablepago, " +
											"                                     cp.ig_prestamo, " +
											"                                     cp.ig_disposicion, " +
											"                                     cp.fg_totalvencimiento " +
											"                                    )   ";

						cmp_cgEstatus =
											" DECODE(cp.cg_estatus,'R',fn_es_recuperacion_fondojr (cp.com_fechaprobablepago, cp.ig_prestamo, cp.ig_disposicion, cp.fg_totalvencimiento),cp.cg_estatus) estatus ";
						bit_group = " group by pf.ig_folio, pf.com_fechaprobablepago, pf.cg_usuario, pf.ic_programa_fondojr ";
						bit_campo = " to_char(pf.com_fechaprobablepago,'dd/mm/yyyy') fec_venc, ";
					} else {
						tabla = "comvis_vencimiento_fide ";
						condicionGarl =
											"where cp.cg_estatus IN ('P', 'NP') " + "	and cp.cg_estatus_proc = ?  " +
											"	and cp.com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
											"	and cp.ic_programa_fondojr = ? "; //se agrega por error en produccion
						cmp_cgEstatus = " cp.cg_estatus estatus ";
						bit_group = " group by pf.ig_folio, pf.com_fechaprobablepago, pf.cg_usuario, pf.ic_programa_fondojr ";
						bit_campo = " to_char(pf.com_fechaprobablepago,'dd/mm/yyyy') fec_venc, ";
					}
					/*SE ASIGNA NUMERO DE FOLIO A REGISTROS VALIDADOS POR FEXCHA DE VENCIMIENTO*/
					String qryUpdateVencTmp = "";

					if ((formaMonitor.getFec_venc()).equals("RC") || "R".equals(formaMonitor.getEstatus_cred())) {
						String queryConsUpd =
											"select to_char(cp.com_fechaprobablepago,'dd/mm/yyyy') as com_fechaprobablepago, cp.ig_prestamo, " +
											"		cp.ig_disposicion, cp.fg_totalvencimiento " + "from com_pagos_fide cp " +
											condicionGarl;

						ps = con.queryPrecompilado(queryConsUpd);
						if ((formaMonitor.getFec_venc()).equals("RC")) { //FODEA 026-2010 FVR - SE AGREGA CONDICION DE RECUPERADOS
							ps.setString(1, "R");
							ps.setString(2, "N");
							ps.setLong(3, Long.parseLong(cvePrograma));
						} else if ("R".equals(formaMonitor.getEstatus_cred())) {
							ps.setString(1, formaMonitor.getEstatus_cred());
							ps.setString(2, "N");
							ps.setLong(3, Long.parseLong(cvePrograma));
							if (!"R".equals(formaMonitor.getFec_venc())) {
								ps.setString(4, formaMonitor.getFec_venc());
							}
						}

						rs = ps.executeQuery();
						PreparedStatement psInt = null;
						while (rs.next()) {
							qryUpdateVencTmp =
												  " update com_pagos_fide cp set cp.ig_folio = ?, cp.cg_estatus_proc = ? " +
												  "	 WHERE cp.cg_estatus IN (?) " + "   AND cp.cg_estatus_proc = ? " +
												  "   AND cp.com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " + "   AND cp.ig_prestamo = ?  " +
												  "   AND cp.ig_disposicion = ? " + "   AND cp.fg_totalvencimiento = ? " +
												  "	 AND cp.ic_programa_fondojr = ? "; //se agrega por error en produccion

							psInt = con.queryPrecompilado(qryUpdateVencTmp);
							psInt.setLong(1, Long.parseLong(formaMonitor.getFolio()));
							psInt.setString(2, "S");
							psInt.setString(3, "R");
							psInt.setString(4, "N");
							psInt.setString(5, rs.getString("com_fechaprobablepago"));
							psInt.setString(6, rs.getString("ig_prestamo"));
							psInt.setString(7, rs.getString("ig_disposicion"));
							psInt.setString(8, rs.getString("fg_totalvencimiento"));
							psInt.setLong(9, Long.parseLong(cvePrograma));
							psInt.executeUpdate();
							psInt.close();
						}
						if (rs != null)
							rs.close();
						if (ps != null)
							ps.close();

						if ("R".equals(formaMonitor.getEstatus_cred())) {
							ps = con.queryPrecompilado(queryConsUpd);
							ps.setString(1, formaMonitor.getEstatus_cred());
							ps.setString(2, "P");
							ps.setLong(3, Long.parseLong(cvePrograma));
							if (!"R".equals(formaMonitor.getFec_venc())) {
								ps.setString(4, formaMonitor.getFec_venc());
							}

							rs = ps.executeQuery();
							psInt = null;
							while (rs.next()) {
								qryUpdateVencTmp = " update com_pagos_fide cp set cp.ig_folio = ?, cp.cg_estatus_proc = ? " +
														 "	 WHERE cp.cg_estatus IN (?) " + "   AND cp.cg_estatus_proc = ? " +
														 "   AND cp.com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " + "   AND cp.ig_prestamo = ?  " +
														 "   AND cp.ig_disposicion = ? " + "   AND cp.fg_totalvencimiento = ? " +
														 "	 AND cp.ic_programa_fondojr = ? "; //se agrega por error en produccion

								psInt = con.queryPrecompilado(qryUpdateVencTmp);
								psInt.setLong(1, Long.parseLong(formaMonitor.getFolio()));
								psInt.setString(2, "P");
								psInt.setString(3, "R");
								psInt.setString(4, "P");
								psInt.setString(5, rs.getString("com_fechaprobablepago"));
								psInt.setString(6, rs.getString("ig_prestamo"));
								psInt.setString(7, rs.getString("ig_disposicion"));
								psInt.setString(8, rs.getString("fg_totalvencimiento"));
								psInt.setLong(9, Long.parseLong(cvePrograma));
								psInt.executeUpdate();
								psInt.close();
							}
							if (rs != null)
								rs.close();
							if (ps != null)
								ps.close();

						}

					} else {
						qryUpdateVencTmp =
											"update com_pagos_fide cp set cp.ig_folio = ?, cp.cg_estatus_proc = ? " +
											condicionGarl;

						System.out.println("qryUpdateVencTmp>>>>>>" + qryUpdateVencTmp);
						ps = con.queryPrecompilado(qryUpdateVencTmp);
						ps.setLong(1, Long.parseLong(formaMonitor.getFolio()));
						ps.setString(2, "S");
						//MODIFICACION POR FODEA 057-2009 FVR - INI
						if ((formaMonitor.getFec_venc()).equals("T")) { //FODEA 026-2010 FVR - SE QUITA REEMBOLSO DE LA CONDICION
							ps.setString(3, formaMonitor.getFec_venc());
							ps.setString(4, "N");
							ps.setLong(5, Long.parseLong(cvePrograma)); //se agrega por error en produccion
						} /*else if((formaMonitor.getFec_venc()).equals("RC")){//FODEA 026-2010 FVR - SE AGREGA CONDICION DE RECUPERADOS
							ps.setString(3, "R");
							ps.setString(4, "N");
							*/
						else if ("R".equals(formaMonitor.getEstatus_cred())) {
							ps.setString(3, "N");
							ps.setLong(4, Long.parseLong(cvePrograma)); //se agrega por error en produccion
							if (!"R".equals(formaMonitor.getFec_venc())) {
								ps.setString(5, formaMonitor.getFec_venc());
							}
						} else { //MODIFICACION POR F027-2011 FVR
							//actualiza registros con estatus de no procesado
							ps.setString(3, "N");
							ps.setString(4, formaMonitor.getFec_venc());
							ps.setLong(5, Long.parseLong(cvePrograma)); //se agrega por error en produccion
							ps.executeUpdate();
							if (ps != null)
								ps.close();
							//actualiza registros con estatus de rechazado
							ps = con.queryPrecompilado(qryUpdateVencTmp);
							ps.setLong(1, Long.parseLong(formaMonitor.getFolio()));
							ps.setString(2, "R");
							ps.setString(3, "R"); //F027-2011 FVR
							ps.setString(4, formaMonitor.getFec_venc());
							ps.setLong(5, Long.parseLong(cvePrograma)); //se agrega por error en produccion

							ps.executeUpdate();
							if (ps != null)
								ps.close();
							//actualiza registros con estatus de prepago rechazado
							ps = con.queryPrecompilado(qryUpdateVencTmp);
							ps.setLong(1, Long.parseLong(formaMonitor.getFolio()));
							ps.setString(2, "P");
							ps.setString(3, "P");
							ps.setString(4, formaMonitor.getFec_venc());
							ps.setLong(5, Long.parseLong(cvePrograma));
						}
						//MODIFICACION POR FODEA 057-2009 FVR - FIN
						ps.executeUpdate();
						if (ps != null)
							ps.close();
					}

					/*SE TRANSPASAN REGISTROS VALIDADOS A TABLA FINAL DE TRABAJO*/
					String qrySelectVenc =
									 "SELECT TO_CHAR (cp.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc, " +
									 "       cp.ig_prestamo prestamo, cp.ig_cliente cliente, " +
									 "       cp.ig_disposicion disposicion, cp.fg_amortizacion amort, " +
									 "       cp.fg_interes interes, cp.fg_totalvencimiento monto_tot, " + cmp_cgEstatus + ", " +
									 "       cp.ig_origen origen, " + "       TO_CHAR (cp.df_periodofin, 'dd/mm/yyyy') fec_periodo, " +
									 "       TO_CHAR (cp.df_registro_fide, 'dd/mm/yyyy hh:mi:ss') fec_reg, " +
									 "       cp.ig_folio folio, cvf.cg_nombrecliente nombrecliente, cp.ic_programa_fondojr programa " +
									 "  FROM com_pagos_fide cp, " + tabla + " cvf " + condicionGarl + "	 AND cp.ig_folio = ? " +
									 "   AND cp.com_fechaprobablepago = cvf.com_fechaprobablepago(+) " +
									 "   AND cp.ig_cliente = cvf.ig_cliente(+) " + "   AND cp.ig_prestamo = cvf.ig_prestamo(+) " +
									 "   AND cp.ig_disposicion = cvf.ig_disposicion(+) " + "   AND cp.fg_amortizacion = cvf.fg_amortizacion(+) " +
									 "   AND cp.fg_interes = cvf.fg_interes(+) " +
									 "   AND cp.fg_totalvencimiento = cvf.fg_totalvencimiento(+) " +
									 //"	 AND cp.ic_programa_fondojr = ? " +
									 "   group by cp.com_fechaprobablepago,  " + "    	  cp.ig_prestamo , cp.ig_cliente ,  " +
									 "       cp.ig_disposicion , cp.fg_amortizacion ,  " + "       cp.fg_interes , cp.fg_totalvencimiento ,  " +
									 "       cp.ig_origen , cp.df_periodofin,  " + "       cp.df_registro_fide, cp.ig_folio ,  " +
									 "       cvf.cg_nombrecliente,cp.cg_estatus, cp.ic_programa_fondojr " +
									 " order by cp.com_fechaprobablepago ";
					System.out.println("qrySelectVenc>>>>" + qrySelectVenc);
					ps = con.queryPrecompilado(qrySelectVenc);
					int i = 0;
					if ((formaMonitor.getFec_venc()).equals("T")) {
						ps.setString(++i, formaMonitor.getFec_venc());
						ps.setString(++i, "S");
						ps.setLong(++i, Long.parseLong(cvePrograma)); //se agrega por error en produccion
					} else if ((formaMonitor.getFec_venc()).equals("RC")) {
						ps.setString(++i, "R");
						ps.setString(++i, "S");
						ps.setLong(++i, Long.parseLong(cvePrograma)); //se agrega por error en produccion
					} else if ("R".equals(formaMonitor.getEstatus_cred())) {


						//MODIFICACION X F006-2014 FVR
						//Si existen registros Rechazados por estar Prepagados se transpasan a la tabla com_cred_prepegados
						i = 0;
						ps = con.queryPrecompilado(qrySelectVenc);

						//ps.setString(++i, formaMonitor.getFec_venc());
						ps.setString(++i, "R");
						ps.setString(++i, "P");
						ps.setLong(++i, Long.parseLong(cvePrograma)); //se agrega por error en produccion
						if (!"R".equals(formaMonitor.getFec_venc())) {
							ps.setString(++i, formaMonitor.getFec_venc());
						}
						ps.setLong(++i, Long.parseLong(formaMonitor.getFolio()));
						rs = ps.executeQuery();

						if (rs != null) {
							String qryInsertVenc =
												  "insert into COM_CRED_PREPAGADOS_FJR " +
												  "   (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion,  " +
												  "   fg_amortizacion, fg_interes, fg_totalvencimiento, cg_estatus,  " +
												  "   ig_origen, df_periodofin, df_registro_fide, ic_programa_fondojr, ig_folio )values " +
												  "	(TO_DATE(?,'DD/MM/YYYY'),?,?,?, " + "	?,?,?,? " +
												  "	,?, TO_DATE(?,'DD/MM/YYYY'), TO_DATE(?,'DD/MM/YYYY hh:mi:ss'),?, ?) ";

							while (rs.next()) { //FODEA 026-2010 FVR - SE AGREGA IC_PROGRAMA_FONDOJR AL INSERT

								ps2 = con.queryPrecompilado(qryInsertVenc);

								ps2.setString(1, rs.getString("fec_venc"));
								if (rs.getString("prestamo") != null)
									ps2.setLong(2, Long.parseLong(rs.getString("prestamo")));
								else
									ps2.setNull(2, Types.INTEGER);
								if (rs.getString("cliente") != null)
									ps2.setLong(3, Long.parseLong(rs.getString("cliente")));
								else
									ps2.setNull(3, Types.INTEGER);
								if (rs.getString("disposicion") != null)
									ps2.setLong(4, Long.parseLong(rs.getString("disposicion")));
								else
									ps2.setNull(4, Types.INTEGER);
								if (rs.getString("amort") != null)
									ps2.setBigDecimal(5, new BigDecimal(rs.getString("amort")));
								else
									ps2.setNull(5, Types.DECIMAL);
								if (rs.getString("interes") != null)
									ps2.setBigDecimal(6, new BigDecimal(rs.getString("interes")));
								else
									ps2.setNull(6, Types.DECIMAL);
								if (rs.getString("monto_tot") != null)
									ps2.setBigDecimal(7, new BigDecimal(rs.getString("monto_tot")));
								else
									ps2.setNull(7, Types.DECIMAL);
								ps2.setString(8, rs.getString("estatus"));
								ps2.setString(9, rs.getString("origen"));
								ps2.setString(10, rs.getString("fec_periodo"));
								ps2.setString(11, rs.getString("fec_reg"));
								ps2.setLong(12, rs.getLong("programa"));
								ps2.setLong(13, Long.parseLong(formaMonitor.getFolio()));
								ps2.executeUpdate();
								if (ps2 != null)
									ps2.close();
							}
						}
						if (rs != null)
							rs.close();
						if (ps != null)
							ps.close();
						// FIN MODIFICACION F006-2014 FVR

						ps = con.queryPrecompilado(qrySelectVenc);
						i = 0;
						ps.setString(++i, formaMonitor.getEstatus_cred());
						ps.setString(++i, "S");
						ps.setLong(++i, Long.parseLong(cvePrograma)); //se agrega por error en produccion
						if (!"R".equals(formaMonitor.getFec_venc())) {
							ps.setString(++i, formaMonitor.getFec_venc());
						}
					} else {

						//MODIFICACION X F027-2011 FVR
						//Si existen registros NP Rechazados se transpasan a tabla 4
						ps.setString(++i, "R");
						ps.setString(++i, formaMonitor.getFec_venc());
						ps.setLong(++i, Long.parseLong(cvePrograma)); //se agrega por error en produccion
						ps.setLong(++i, Long.parseLong(formaMonitor.getFolio()));
						rs = ps.executeQuery();

						if (rs != null) {
							String qryInsertVenc =
												  "insert into COM_CRED_NP_RECHAZADOS " +
												  "   (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion,  " +
												  "   fg_amortizacion, fg_interes, fg_totalvencimiento, cg_estatus,  " +
												  "   ig_origen, df_periodofin, df_registro_fide, ic_programa_fondojr, ig_folio )values " +
												  "	(TO_DATE(?,'DD/MM/YYYY'),?,?,?, " + "	?,?,?,? " +
												  "	,?, TO_DATE(?,'DD/MM/YYYY'), TO_DATE(?,'DD/MM/YYYY hh:mi:ss'),?, ?) ";

							while (rs.next()) { //FODEA 026-2010 FVR - SE AGREGA IC_PROGRAMA_FONDOJR AL INSERT

								ps2 = con.queryPrecompilado(qryInsertVenc);

								ps2.setString(1, rs.getString("fec_venc"));
								if (rs.getString("prestamo") != null)
									ps2.setLong(2, Long.parseLong(rs.getString("prestamo")));
								else
									ps2.setNull(2, Types.INTEGER);
								if (rs.getString("cliente") != null)
									ps2.setLong(3, Long.parseLong(rs.getString("cliente")));
								else
									ps2.setNull(3, Types.INTEGER);
								if (rs.getString("disposicion") != null)
									ps2.setLong(4, Long.parseLong(rs.getString("disposicion")));
								else
									ps2.setNull(4, Types.INTEGER);
								if (rs.getString("amort") != null)
									ps2.setBigDecimal(5, new BigDecimal(rs.getString("amort")));
								else
									ps2.setNull(5, Types.DECIMAL);
								if (rs.getString("interes") != null)
									ps2.setBigDecimal(6, new BigDecimal(rs.getString("interes")));
								else
									ps2.setNull(6, Types.DECIMAL);
								if (rs.getString("monto_tot") != null)
									ps2.setBigDecimal(7, new BigDecimal(rs.getString("monto_tot")));
								else
									ps2.setNull(7, Types.DECIMAL);
								//ps2.setString(8,rs.getString("estatus"));
								ps2.setString(8, "NPR");
								ps2.setString(9, rs.getString("origen"));
								ps2.setString(10, rs.getString("fec_periodo"));
								ps2.setString(11, rs.getString("fec_reg"));
								ps2.setLong(12, rs.getLong("programa"));
								ps2.setLong(13, Long.parseLong(formaMonitor.getFolio()));
								ps2.executeUpdate();
								if (ps2 != null)
									ps2.close();
							}
						}
						if (rs != null)
							rs.close();
						if (ps != null)
							ps.close();
						// FIN MODIFICACION F027-2011

						//MODIFICACION X F006-2014 FVR
						//Si existen registros Rechazados por estar Prepagados se transpasan a la tabla com_cred_prepegados
						i = 0;
						ps = con.queryPrecompilado(qrySelectVenc);
						ps.setString(++i, "P");
						ps.setString(++i, formaMonitor.getFec_venc());
						ps.setLong(++i, Long.parseLong(cvePrograma)); //se agrega por error en produccion
						ps.setLong(++i, Long.parseLong(formaMonitor.getFolio()));
						rs = ps.executeQuery();

						if (rs != null) {
							String qryInsertVenc =
												  "insert into COM_CRED_PREPAGADOS_FJR " +
												  "   (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion,  " +
												  "   fg_amortizacion, fg_interes, fg_totalvencimiento, cg_estatus,  " +
												  "   ig_origen, df_periodofin, df_registro_fide, ic_programa_fondojr, ig_folio )values " +
												  "	(TO_DATE(?,'DD/MM/YYYY'),?,?,?, " + "	?,?,?,? " +
												  "	,?, TO_DATE(?,'DD/MM/YYYY'), TO_DATE(?,'DD/MM/YYYY hh:mi:ss'),?, ?) ";

							while (rs.next()) { //FODEA 026-2010 FVR - SE AGREGA IC_PROGRAMA_FONDOJR AL INSERT

								ps2 = con.queryPrecompilado(qryInsertVenc);

								ps2.setString(1, rs.getString("fec_venc"));
								if (rs.getString("prestamo") != null)
									ps2.setLong(2, Long.parseLong(rs.getString("prestamo")));
								else
									ps2.setNull(2, Types.INTEGER);
								if (rs.getString("cliente") != null)
									ps2.setLong(3, Long.parseLong(rs.getString("cliente")));
								else
									ps2.setNull(3, Types.INTEGER);
								if (rs.getString("disposicion") != null)
									ps2.setLong(4, Long.parseLong(rs.getString("disposicion")));
								else
									ps2.setNull(4, Types.INTEGER);
								if (rs.getString("amort") != null)
									ps2.setBigDecimal(5, new BigDecimal(rs.getString("amort")));
								else
									ps2.setNull(5, Types.DECIMAL);
								if (rs.getString("interes") != null)
									ps2.setBigDecimal(6, new BigDecimal(rs.getString("interes")));
								else
									ps2.setNull(6, Types.DECIMAL);
								if (rs.getString("monto_tot") != null)
									ps2.setBigDecimal(7, new BigDecimal(rs.getString("monto_tot")));
								else
									ps2.setNull(7, Types.DECIMAL);
								ps2.setString(8, rs.getString("estatus"));
								ps2.setString(9, rs.getString("origen"));
								ps2.setString(10, rs.getString("fec_periodo"));
								ps2.setString(11, rs.getString("fec_reg"));
								ps2.setLong(12, rs.getLong("programa"));
								ps2.setLong(13, Long.parseLong(formaMonitor.getFolio()));
								ps2.executeUpdate();
								if (ps2 != null)
									ps2.close();
							}
						}
						if (rs != null)
							rs.close();
						if (ps != null)
							ps.close();
						// FIN MODIFICACION F006-2014 FVR

						ps = con.queryPrecompilado(qrySelectVenc);
						i = 0;
						ps.setString(++i, "S");
						ps.setString(++i, formaMonitor.getFec_venc());
						ps.setLong(++i, Long.parseLong(cvePrograma)); //se agrega por error en produccion


					}
					ps.setLong(++i, Long.parseLong(formaMonitor.getFolio()));
					//ps.setLong(++i, Long.parseLong(cvePrograma));
					rs = ps.executeQuery();
					if (rs != null) {
						while (rs.next()) { //FODEA 026-2010 FVR - SE AGREGA IC_PROGRAMA_FONDOJR AL INSERT
							String qryInsertVenc =
												  "insert into com_pagos_fide_val " +
												  "   (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion,  " +
												  "   fg_amortizacion, fg_interes, fg_totalvencimiento, cg_estatus,  " +
												  "   ig_origen, df_periodofin, df_registro_fide, ig_folio, df_fecha_carga, cg_usuario, cg_nombrecliente, ic_programa_fondojr)values " +
												  "	(TO_DATE(?,'DD/MM/YYYY'),?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY hh:mi:ss'),?,TO_DATE(TO_CHAR(SYSDATE,'DD/MM/YYYY HH:MI:SS'),'DD/MM/YYYY HH:MI:SS'),?,?,?) ";
							System.out.println(rs.getString("prestamo"));
							System.out.println(rs.getString("cliente"));
							System.out.println(rs.getString("disposicion"));
							System.out.println(rs.getString("amort"));
							System.out.println(rs.getString("interes"));
							System.out.println(rs.getString("monto_tot"));
							System.out.println(rs.getString("estatus"));
							System.out.println(rs.getString("origen"));
							System.out.println(rs.getString("fec_periodo"));
							System.out.println(rs.getString("fec_reg"));
							System.out.println(rs.getString("folio"));
							System.out.println(rs.getString("nombrecliente"));
							System.out.println(formaMonitor.getUsuario());
							System.out.println(rs.getString("fec_venc"));
							ps2 = con.queryPrecompilado(qryInsertVenc);
							ps2.setString(1, rs.getString("fec_venc"));
							if (rs.getString("prestamo") != null)
								ps2.setLong(2, Long.parseLong(rs.getString("prestamo")));
							else
								ps2.setNull(2, Types.INTEGER);
							if (rs.getString("cliente") != null)
								ps2.setLong(3, Long.parseLong(rs.getString("cliente")));
							else
								ps2.setNull(3, Types.INTEGER);
							if (rs.getString("disposicion") != null)
								ps2.setLong(4, Long.parseLong(rs.getString("disposicion")));
							else
								ps2.setNull(4, Types.INTEGER);
							if (rs.getString("amort") != null)
								ps2.setBigDecimal(5, new BigDecimal(rs.getString("amort")));
							else
								ps2.setNull(5, Types.DECIMAL);
							if (rs.getString("interes") != null)
								ps2.setBigDecimal(6, new BigDecimal(rs.getString("interes")));
							else
								ps2.setNull(6, Types.DECIMAL);
							if (rs.getString("monto_tot") != null)
								ps2.setBigDecimal(7, new BigDecimal(rs.getString("monto_tot")));
							else
								ps2.setNull(7, Types.DECIMAL);
							ps2.setString(8, rs.getString("estatus"));
							ps2.setString(9, rs.getString("origen"));
							ps2.setString(10, rs.getString("fec_periodo"));
							ps2.setString(11, rs.getString("fec_reg"));
							if (rs.getString("folio") != null)
								ps2.setLong(12, Long.parseLong(rs.getString("folio")));
							else
								ps2.setNull(12, Types.INTEGER);
							ps2.setString(13, formaMonitor.getUsuario());
							ps2.setString(14, rs.getString("nombrecliente"));
							ps2.setLong(15, rs.getLong("programa"));
							ps2.executeUpdate();
							if (ps2 != null)
								ps2.close();
						}
					}
					if (rs != null)
						rs.close();
					if (ps != null)
						ps.close();


					/*---------------------------------------------------------------------*/
					/*-----------------SE CREA REGISTRO EN BITACORA DE MONITOR-------------*/
					/*---------------------------------------------------------------------*/

					qrySelectVenc =
									 "SELECT COUNT (PF.IG_FOLIO) TOTAL_REG, " + bit_campo +
									 "         TO_CHAR (MAX (PF.DF_REGISTRO_FIDE),'DD/MM/YYYY HH:MI:SS') ULTIMO_REG,  " +
									 "         SUM (PF.FG_TOTALVENCIMIENTO) MONTO_TOT, PF.IG_FOLIO FOLIO, PF.CG_USUARIO USUARIO, PF.IC_PROGRAMA_FONDOJR PROGRAMA  " +
									 "    FROM COM_PAGOS_FIDE_VAL PF  " + "   WHERE PF.IG_FOLIO = ?  " + bit_group;

					String qryInsertBit =
									 "insert into BIT_MONITOR_FONDOJR (IC_BIT_MONITOR, COM_FECHAPROBABLEPAGO, CG_TIPO_REGISTRO, IG_FOLIO, IG_TOTAL_REG, " +
									 " FG_MONTO_TOTAL, DF_REGISTRO_FIDE, CG_USUARIO, CG_ESTATUS_CARGA, IC_PROGRAMA_FONDOJR) values " +
									 " (seq_com_venc_fondo_jr.nextval, to_date(?,'dd/mm/yyyy'),?,?,?,?,to_date(?,'dd/mm/yyyy hh:mi:ss'),?,?,?) ";

					System.out.println("qrySelectVenc>>>>" + qrySelectVenc);
					ps = con.queryPrecompilado(qrySelectVenc);
					ps.setLong(1, Long.parseLong(formaMonitor.getFolio()));
					rs = ps.executeQuery();

					if (rs != null) {
						while (rs.next()) {
							ps2 = con.queryPrecompilado(qryInsertBit);
							if ((formaMonitor.getFec_venc()).equals("RC") || (formaMonitor.getFec_venc()).equals("T")) { //FODEA 026-2010 FVR - SE CAMBIA CONDICION, NO SE CONSIDERA REEMBOLSO
												  ps2.setString(1, "");
												  //if((rs.getString("estatus")).equals("R")) ps2.setString(2,"Reembolsos al dia");
												  if ((rs.getString("estatus")).equals("T"))
									ps2.setString(2, "Prepagos al dia");
								if ((rs.getString("estatus")).equals("RC"))
									ps2.setString(2, "Recuperaciones al dia");
							} else if (("R").equals(formaMonitor.getEstatus_cred())) {
								if (!"R".equals(formaMonitor.getFec_venc())) {
									ps2.setString(1, rs.getString("fec_venc"));
								} else {
									ps2.setString(1, rs.getString("fec_venc"));
								}
								ps2.setString(2, "Reembolsos al dia");
							} else {
								ps2.setString(1, rs.getString("fec_venc"));
								ps2.setString(2, "Pagados y No Pagados");
							}
							ps2.setLong(3, Long.parseLong(rs.getString("folio")));
							ps2.setLong(4, Long.parseLong(rs.getString("TOTAL_REG")));
							ps2.setBigDecimal(5, new BigDecimal(rs.getString("MONTO_TOT")));
							ps2.setString(6, rs.getString("ULTIMO_REG"));
							ps2.setString(7, rs.getString("usuario"));
							ps2.setString(8, "V");
							ps2.setLong(9, rs.getLong("PROGRAMA"));
							ps2.executeUpdate();
						} //cierre while(rs)
					} //cierre if(rs)
					if (rs != null)
						rs.close();
					if (ps != null)
						ps.close();

					/*SE REALIZA CONSULTA PARA OBRENER INFORMACION A MOSTRAR EN EL ACUSE*/
					con.terminaTransaccion(transaccion);
					eliminaRegMonitor(formaMonitor, false, cvePrograma);
					if ("JS".equals(version))
						lstAcuseGral = consAcuseMonitorValidadoJS(formaMonitor.getFolio(), formaMonitor.getUsuario(),
																				formaMonitor.getFec_venc());
					else
						lstAcuseGral = consAcuseMonitorValidado(formaMonitor.getFolio(), formaMonitor.getUsuario(),
																			 formaMonitor.getFec_venc());
				} else {

					lstAcuseGral.add(s.mostrarError());

				}
			}
			return lstAcuseGral;
		} catch (Exception e) {
			transaccion = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Metodo que genera un acuse de la informacion que ya fue autorizada
	 * @throws com.netro.exception.NafinException
	 * @return List
	 * @param fec_venc - fecha que indica el vencimiento de los registros autorizados
	 * @param usuario -  persona que realizo la autorizacion
	 * @param folio - numero generado pro la autorizacion
	 */
	public List consAcuseMonitorValidado(String folio, String usuario, String fec_venc) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstAcuseGral = new ArrayList();
		List lstRegAcuse = null;
		String ultimo_reg = "";

		BigDecimal montoTotalPyNP = new BigDecimal("0.00");
		BigDecimal montoTotalPrepago = new BigDecimal("0.00");
		BigDecimal montoTotalReemb = new BigDecimal("0.00");
		BigDecimal montoTotalRec = new BigDecimal("0.00");
		int sumaTotalPyNP = 0;
		int sumaTotalReemb = 0;
		int sumaTotalPrepago = 0;
		int sumaTotalRec = 0;
		try {
			con.conexionDB();
			/*SE REALIZA CONSULTA DE LOS REGISTROS INSERTADOS PARA MOSTRAR ACUSE*/
			String qryCons =
						"select to_char(max(pf.df_registro_fide),'dd/mm/yyy hh:mi:ss') ultimo_reg,  " +
						"		count(pf.cg_estatus) total_reg, sum(pf.fg_totalvencimiento) monto_tot,  " +
						"		pf.cg_estatus estatus, pf.ig_folio folio " + "from com_pagos_fide_val pf  " + "  where pf.ig_folio = ? " +
						"  group by pf.ig_folio, pf.cg_estatus " + " union all " +
						" select TO_CHAR (MAX (pf.df_registro_fide), 'dd/mm/yyy hh:mi:ss') ultimo_reg,   " +
						" 		count(pf.cg_estatus) total_reg, sum(pf.fg_totalvencimiento) monto_tot,   " +
						" 		pf.cg_estatus estatus, pf.ig_folio folio  " + " from COM_CRED_NP_RECHAZADOS pf   " +
						"  where pf.ig_folio = ? " + "  group by pf.ig_folio, pf.cg_estatus ";

			System.out.println("qryCons>>>>>>" + qryCons);
			ps = con.queryPrecompilado(qryCons);
			ps.setLong(1, Long.parseLong(folio));
			ps.setLong(2, Long.parseLong(folio));
			rs = ps.executeQuery();
			if (rs != null) {
				lstRegAcuse = new ArrayList();
				lstRegAcuse.add(folio);
				lstRegAcuse.add(usuario);
				lstRegAcuse.add("encabezado");
				lstAcuseGral.add(lstRegAcuse);
				while (rs.next()) {
					if (rs.getString("estatus").equals("P") || rs.getString("estatus").equals("NP") ||
						 rs.getString("estatus").equals("NPR")) {
						montoTotalPyNP = montoTotalPyNP.add(new BigDecimal(rs.getString("monto_tot")));
						sumaTotalPyNP += rs.getInt("total_reg");
						ultimo_reg = rs.getString("ultimo_reg");
					} else if (rs.getString("estatus").equals("R")) {
						montoTotalReemb = montoTotalReemb.add(new BigDecimal(rs.getString("monto_tot")));
						sumaTotalReemb += rs.getInt("total_reg");
						/**/
						lstRegAcuse = new ArrayList();
						lstRegAcuse.add(String.valueOf(sumaTotalReemb));
						lstRegAcuse.add(montoTotalReemb.toPlainString());
						lstRegAcuse.add("reembolsos");
						lstRegAcuse.add(rs.getString("ultimo_reg"));
						lstAcuseGral.add(lstRegAcuse);
					} else if (rs.getString("estatus").equals("T")) {
						montoTotalPrepago = montoTotalPrepago.add(new BigDecimal(rs.getString("monto_tot")));
						sumaTotalPrepago += rs.getInt("total_reg");
						/**/
						lstRegAcuse = new ArrayList();
						lstRegAcuse.add(String.valueOf(sumaTotalPrepago));
						lstRegAcuse.add(montoTotalPrepago.toPlainString());
						lstRegAcuse.add("prepagos");
						lstRegAcuse.add(rs.getString("ultimo_reg"));
						lstAcuseGral.add(lstRegAcuse);
					} else if (rs.getString("estatus").equals("RC")) {
						montoTotalRec = montoTotalRec.add(new BigDecimal(rs.getString("monto_tot")));
						sumaTotalRec += rs.getInt("total_reg");
						/**/
						lstRegAcuse = new ArrayList();
						lstRegAcuse.add(String.valueOf(sumaTotalRec));
						lstRegAcuse.add(montoTotalRec.toPlainString());
						lstRegAcuse.add("recuperacion");
						lstRegAcuse.add(rs.getString("ultimo_reg"));
						lstAcuseGral.add(lstRegAcuse);
					}
				} //cieere while (rs)
				/**/
				if (sumaTotalPyNP > 0) {
					lstRegAcuse = new ArrayList();
					lstRegAcuse.add(String.valueOf(sumaTotalPyNP));
					lstRegAcuse.add(montoTotalPyNP.toPlainString());
					lstRegAcuse.add("total");
					lstRegAcuse.add(ultimo_reg);
					lstRegAcuse.add(fec_venc);
					lstAcuseGral.add(lstRegAcuse);
				}
			} //cierre if(rs)
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			return lstAcuseGral;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Realiza la eliminacion de aquella informacion que fue enviada por el FIDE
	 * @throws com.netro.exception.NafinException
	 * @param bitacora - variable que indica si se quiere generar registro en bitacora
	 * @param usuario - persona que realiza la eliminacion
	 * @param fec_venc - fecha que indica los registros a borrar
	 */
	public void eliminaRegMonitor(FondoJrMonitorBean bean, boolean bitacora, String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null;
		String qrySentence = "";
		String qryReembPrep = "";
		boolean transaccion = true;
		try {
			con.conexionDB();
			String fec_venc = bean.getFec_venc();
			String usuario = bean.getUsuario();
			String estatus = bean.getEstatus_cred();

			System.out.println("estatus>>>>" + estatus);
			if (bitacora) { //FODEA 026-2010 FVR - SE MOFICAN CONDICIONES Y QUERYS
				System.out.println("entro en insertar en bitacora");
				if (!(fec_venc.equals("T") || fec_venc.equals("RC")) && !"R".equals(estatus)) { //MODIFICACION POR FODEA 057-2009 FVR
							  System.out.println("entro en 1");
					qrySentence =
									 "SELECT COUNT(PF.COM_FECHAPROBABLEPAGO) TOTAL_REG, TO_CHAR(PF.COM_FECHAPROBABLEPAGO,'DD/MM/YYYY') FECHA_VENC,  " +
									 "	TO_CHAR(MAX(PF.DF_REGISTRO_FIDE),'DD/MM/YYYY hh:mi:ss') ULTIMO_REG,  " +
									 "    SUM(PF.FG_TOTALVENCIMIENTO) MONTO_TOT " + " FROM COM_PAGOS_FIDE PF  " +
									 " WHERE PF.CG_ESTATUS IN ('P','NP')  " + " AND PF.COM_FECHAPROBABLEPAGO = TO_DATE(?,'DD/MM/YYYY') " +
									 " AND PF.IC_PROGRAMA_FONDOJR = ? " + //FODEA 026-2010 FVR
									 " GROUP BY PF.COM_FECHAPROBABLEPAGO ";
					ps = con.queryPrecompilado(qrySentence);
					ps.setString(1, fec_venc);
					ps.setLong(2, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
				} else if ("R".equals(estatus)) {
					System.out.println("entro en 2");
					int params = 0;
					qrySentence =
									 "SELECT COUNT(PF.COM_FECHAPROBABLEPAGO) TOTAL_REG, TO_CHAR(PF.COM_FECHAPROBABLEPAGO,'DD/MM/YYYY') FECHA_VENC,  " +
									 "	TO_CHAR(MAX(PF.DF_REGISTRO_FIDE),'DD/MM/YYYY hh:mi:ss') ULTIMO_REG,  " +
									 "		  SUM(PF.FG_TOTALVENCIMIENTO) MONTO_TOT, PF.CG_ESTATUS " + "  FROM COM_PAGOS_FIDE PF   " +
									 " WHERE PF.CG_ESTATUS = ?  " + //MODIFICACION FODEA 057-2009 FVR
									 " AND 'RC' != FN_ES_RECUPERACION_FONDOJR (PF.COM_FECHAPROBABLEPAGO, PF.IG_PRESTAMO,   " +
									 "                      PF.IG_DISPOSICION, PF.FG_TOTALVENCIMIENTO)   ";
					if (fec_venc != null && !"".equals(fec_venc) && !"R".equals(fec_venc)) {
						qrySentence += " AND PF.COM_FECHAPROBABLEPAGO = TO_DATE(?,'DD/MM/YYYY') ";
					}
					qrySentence += " AND PF.IC_PROGRAMA_FONDOJR = ? " + //FODEA 026-2010 FVR
									 " GROUP BY PF.CG_ESTATUS, PF.COM_FECHAPROBABLEPAGO, PF.CG_ESTATUS_PROC, PF.CG_ESTATUS  ";

					ps = con.queryPrecompilado(qrySentence);
					ps.setString(++params, estatus);
					if (fec_venc != null && !"".equals(fec_venc) && !"R".equals(fec_venc)) {
						ps.setString(++params, fec_venc);
					}
					ps.setLong(++params, Long.parseLong(cvePrograma)); //FODEA 012-2011 FVR
				} else {
					System.out.println("entro en 3");
					if (fec_venc.equals("T")) {
						qrySentence = "SELECT COUNT (PF.CG_ESTATUS) TOTAL_REG, PF.CG_ESTATUS ESTATUS,    " +
											"	TO_CHAR (SYSDATE, 'DD/MM/YYYY HH:MI:SS') ULTIMO_REG,              " +
											"     SUM (PF.FG_TOTALVENCIMIENTO) MONTO_TOT " + " FROM COM_PAGOS_FIDE PF   " +
											"WHERE PF.CG_ESTATUS IN (?) " + //MODIFICACION FODEA 057-2009 FVR
											"AND 'RC' != FN_ES_RECUPERACION_FONDOJR (PF.COM_FECHAPROBABLEPAGO, PF.IG_PRESTAMO,   " +
											"                   PF.IG_DISPOSICION, PF.FG_TOTALVENCIMIENTO)     " + " AND PF.IC_PROGRAMA_FONDOJR = ? " + //FODEA 026-2010 FVR
											"GROUP BY PF.CG_ESTATUS, PF.CG_ESTATUS_PROC   ";

					} else if (fec_venc.equals("RC")) {
						qrySentence = " SELECT 	COUNT(PF.CG_ESTATUS) TOTAL_REG, 'RC' ESTATUS, " +
											"		TO_CHAR(SYSDATE,'DD/MM/YYYY HH:MI:SS') ULTIMO_REG,   " + "		  SUM(PF.FG_TOTALVENCIMIENTO) MONTO_TOT " +
											"  FROM COM_PAGOS_FIDE PF   " + " WHERE PF.CG_ESTATUS = ?  " + //MODIFICACION FODEA 057-2009 FVR
											" AND 'RC' = FN_ES_RECUPERACION_FONDOJR (PF.COM_FECHAPROBABLEPAGO, PF.IG_PRESTAMO,   " +
											"                      PF.IG_DISPOSICION, PF.FG_TOTALVENCIMIENTO)   " + " AND PF.IC_PROGRAMA_FONDOJR = ? " + //FODEA 026-2010 FVR
											" GROUP BY PF.CG_ESTATUS,PF.CG_ESTATUS_PROC  ";
					}
					ps = con.queryPrecompilado(qrySentence);
					ps.setString(1, "RC".equals(fec_venc) ? "R" : fec_venc); //MODIFICACION FODEA 057-2009 FVR
					ps.setLong(2, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
				}

				String qryInsertBit =
							  "insert into BIT_MONITOR_FONDOJR (IC_BIT_MONITOR, COM_FECHAPROBABLEPAGO, CG_TIPO_REGISTRO, IG_FOLIO, IG_TOTAL_REG, " +
							  " FG_MONTO_TOTAL, DF_REGISTRO_FIDE, CG_USUARIO, CG_ESTATUS_CARGA, IC_PROGRAMA_FONDOJR) values " +
							  " (seq_com_venc_fondo_jr.nextval, to_date(?,'dd/mm/yyyy'),?,?,?,?,to_date(?,'dd/mm/yyyy hh:mi:ss'),?,?,?) ";

				rs = ps.executeQuery();

				if (rs != null) {
					while (rs.next()) {
						ps2 = con.queryPrecompilado(qryInsertBit);
						if (fec_venc.equals("RC") || fec_venc.equals("T")) { //MODIFICACION FODEA 057-2009 FVR
											ps2.setString(1, "");
											//if((rs.getString("estatus")).equals("R")) ps2.setString(2,"Remmbolsos al dia");
											if ((rs.getString("estatus")).equals("T"))
								ps2.setString(2, "Prepagos al dia");
							if ((rs.getString("estatus")).equals("RC"))
								ps2.setString(2, "Recuperaciones al dia");
						} else if (estatus.equals("R")) {
							ps2.setString(1, rs.getString("FECHA_VENC"));
							ps2.setString(2, "Reembolsos al dia");
						} else {
							ps2.setString(1, rs.getString("FECHA_VENC"));
							ps2.setString(2, "Pagados y No Pagados");
						}

						ps2.setNull(3, Types.INTEGER);
						ps2.setLong(4, Long.parseLong(rs.getString("TOTAL_REG")));
						ps2.setBigDecimal(5, new BigDecimal(rs.getString("MONTO_TOT")));
						ps2.setString(6, rs.getString("ULTIMO_REG"));
						ps2.setString(7, usuario);
						ps2.setString(8, "B");
						ps2.setLong(9, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
						ps2.executeUpdate();
						if (ps2 != null)
							ps2.close();
					} //cierre while(rs)
				} //cierre if(rs)
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			}

			if (!(fec_venc.equals("RC") || fec_venc.equals("T")) && !"R".equals(estatus)) {
				qryReembPrep =
							  "delete from COM_PAGOS_FIDE " + "where com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
							  "and cg_estatus IN ('P', 'NP') " + "and ic_programa_fondojr = ? "; //FODEA 026-2010 FVR

				ps = con.queryPrecompilado(qryReembPrep);
				ps.setString(1, "RC".equals(fec_venc) ? "R" : fec_venc);
				ps.setLong(2, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
				ps.executeUpdate();
				if (ps != null)
					ps.close();
			} else if (fec_venc.equals("T")) {
				qryReembPrep =
							  "delete from COM_PAGOS_FIDE " + "where cg_estatus IN (?) " +
							  "and ic_programa_fondojr = ? "; //FODEA 026-2010 FVR

				ps = con.queryPrecompilado(qryReembPrep);
				ps.setString(1, "RC".equals(fec_venc) ? "R" : fec_venc);
				ps.setLong(2, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
				ps.executeUpdate();
				if (ps != null)
					ps.close();
			} else if (fec_venc.equals("RC")) {
				if (!bitacora) {
					qryReembPrep = " delete from COM_PAGOS_FIDE " + " where cg_estatus IN (?) " + //MODIFICACION FODEA 057-2009 FVR
									 " and ic_programa_fondojr = ? " + //FODEA 026-2010 FVR
									 " and ig_folio = ? ";
					//" and 'RC' = fn_es_recuperacion_fondojr (com_fechaprobablepago, ig_prestamo,   " +
					//"                      ig_disposicion, fg_totalvencimiento)   ";
					ps = con.queryPrecompilado(qryReembPrep);
					ps.setString(1, "RC".equals(fec_venc) ? "R" : fec_venc);
					ps.setLong(2, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
					ps.setLong(3, Long.parseLong(bean.getFolio())); //FODEA 026-2010 FVR
					ps.executeUpdate();
					if (ps != null)
						ps.close();
				} else {

					qrySentence =
									 "select to_char(cp.com_fechaprobablepago,'dd/mm/yyyy') as com_fechaprobablepago, cp.ig_prestamo,  " +
									 "		cp.ig_disposicion, cp.fg_totalvencimiento  " + "from com_pagos_fide cp " + "where cp.cg_estatus IN (?)  " +
									 "	and cp.cg_estatus_proc = ?  " + "	and cp.ic_programa_fondojr = ?  " + "	and 'RC' =  " +
									 "         fn_es_recuperacion_fondojr (cp.com_fechaprobablepago,  " +
									 "                                     cp.ig_prestamo,  " +
									 "                                     cp.ig_disposicion,  " +
									 "                                     cp.fg_totalvencimiento  " +
									 "                                    ) ";
					ps2 = con.queryPrecompilado(qrySentence);
					ps2.setString(1, estatus);
					ps2.setString(2, "N");
					ps2.setLong(3, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
					rs = ps2.executeQuery();

					while (rs.next()) {

						qryReembPrep = " delete  com_pagos_fide cp " + "	 WHERE cp.cg_estatus IN (?) " +
											"   AND cp.cg_estatus_proc = ? " + "   AND cp.com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
											"   AND cp.ig_prestamo = ?  " + "   AND cp.ig_disposicion = ? " + "   AND cp.fg_totalvencimiento = ? " +
											"	 AND cp.ic_programa_fondojr = ? "; //se agrega por error en produccion

						ps = con.queryPrecompilado(qryReembPrep);
						ps.setString(1, "R");
						ps.setString(2, "N");
						ps.setString(3, rs.getString("com_fechaprobablepago"));
						ps.setString(4, rs.getString("ig_prestamo"));
						ps.setString(5, rs.getString("ig_disposicion"));
						ps.setString(6, rs.getString("fg_totalvencimiento"));
						ps.setLong(7, Long.parseLong(cvePrograma));
						ps.executeUpdate();
						if (ps != null)
							ps.close();
					}
					if (rs != null)
						rs.close();
					if (ps2 != null)
						ps2.close();
				}

			} else if (estatus.equals("R")) {
				int params = 0;
				if (!bitacora) {

					qryReembPrep = " delete from COM_PAGOS_FIDE " + " where cg_estatus IN ('R') ";
					if (!"R".equals(fec_venc)) {
						qryReembPrep += " and com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') ";
					}
					qryReembPrep += " and ic_programa_fondojr = ? " + //FODEA 026-2010 FVR
									 " and ig_folio = ? ";
					//"and 'RC' != fn_es_recuperacion_fondojr (com_fechaprobablepago, ig_prestamo,   " +
					//"                      ig_disposicion, fg_totalvencimiento)   "+
					ps = con.queryPrecompilado(qryReembPrep);
					if (!"R".equals(fec_venc)) {
						ps.setString(++params, "RC".equals(fec_venc) ? "R" : fec_venc);
					}
					ps.setLong(++params, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
					ps.setLong(++params, Long.parseLong(bean.getFolio())); //FODEA 026-2010 FVR
					ps.executeUpdate();
					if (ps != null)
						ps.close();

				} else {

					qrySentence =
									 "select to_char(cp.com_fechaprobablepago,'dd/mm/yyyy') as com_fechaprobablepago, cp.ig_prestamo,  " +
									 "		cp.ig_disposicion, cp.fg_totalvencimiento  " + "from com_pagos_fide cp  " +
									 "where cp.cg_estatus IN (?)  " + "	and cp.cg_estatus_proc = ?  ";
					if (!"R".equals(fec_venc)) {
						qrySentence += "	and cp.com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy')  ";
					}
					qrySentence +=
									 "	and cp.ic_programa_fondojr = ? " + "	and 'RC' !=  " +
									 "         fn_es_recuperacion_fondojr (cp.com_fechaprobablepago,  " +
									 "                                     cp.ig_prestamo,  " +
									 "                                     cp.ig_disposicion,  " +
									 "                                     cp.fg_totalvencimiento  " +
									 "                                    ) ";

					ps2 = con.queryPrecompilado(qrySentence);
					ps2.setString(++params, estatus);
					ps2.setString(++params, "N");
					if (!"R".equals(fec_venc)) {
						ps2.setString(++params, fec_venc);
					}
					ps2.setLong(++params, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
					rs = ps2.executeQuery();

					qryReembPrep =
									 " delete  com_pagos_fide cp " + "	 WHERE cp.cg_estatus IN (?) " +
									 "   AND cp.cg_estatus_proc = ? ";
					if (!"R".equals(fec_venc)) {
						qryReembPrep += "   AND cp.com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') ";
					}
					qryReembPrep +=
									 "   AND cp.ig_prestamo = ?  " + "   AND cp.ig_disposicion = ? " +
									 "   AND cp.fg_totalvencimiento = ? " +
									 "	 AND cp.ic_programa_fondojr = ? "; //se agrega por error en produccion


					while (rs.next()) {
						params = 0;
						ps = con.queryPrecompilado(qryReembPrep);
						ps.setString(++params, "R");
						ps.setString(++params, "N");
						if (!"R".equals(fec_venc)) {
							ps.setString(++params, rs.getString("com_fechaprobablepago"));
						}
						ps.setString(++params, rs.getString("ig_prestamo"));
						ps.setString(++params, rs.getString("ig_disposicion"));
						ps.setString(++params, rs.getString("fg_totalvencimiento"));
						ps.setLong(++params, Long.parseLong(cvePrograma));
						ps.executeUpdate();
						if (ps != null)
							ps.close();
					}
					if (rs != null)
						rs.close();
					if (ps2 != null)
						ps2.close();

				}
			}


		} catch (Exception e) {
			transaccion = false;
			e.printStackTrace();
			System.out.println("eliminaRegMonitor::(ERORR)");
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
		}
	}

	public void procValidaMonitorPyNP(String fec_vencimiento) throws NafinException {
		AccesoDB con = new AccesoDB();
		ResultSet rs = null, rs2 = null;
		PreparedStatement ps = null, ps2 = null;

		List lstRegValidados = new ArrayList();
		List lstValidados = new ArrayList();
		StringBuffer txtError = new StringBuffer();
		StringBuffer qrySentence = new StringBuffer();
		String qryError = "";
		FondoJrMonitorBean formaMonitor = new FondoJrMonitorBean();

		BigDecimal montoTotalVista = new BigDecimal("0.00");
		BigDecimal montoTotalPagados = new BigDecimal("0.00");
		BigDecimal montoTotalNoPagados = new BigDecimal("0.00");
		BigDecimal montoTotalTMP = new BigDecimal("0.00");
		BigDecimal montoTotalReemb = new BigDecimal("0.00");
		BigDecimal montoTotalError = new BigDecimal("0.00");
		BigDecimal montoTotalNoEncon = new BigDecimal("0.00");
		BigDecimal montoTotalNoEnv = new BigDecimal("0.00");

		int sumaTotalVista = 0;
		int sumaTotalPagados = 0;
		int sumaTotalNoPagados = 0;
		int sumaTotalTMP = 0;
		int sumaTotalReemb = 0;
		int sumaTotalError = 0;
		int sumaTotalNoEncon = 0;
		int sumaTotalNoEnv = 0;
		boolean realizaTransaccion = true;
		boolean regNoEncon = false;

		try {

			con.conexionDB();

			formaMonitor.setFec_venc(fec_vencimiento);
			formaMonitor.setUsuario("proc_auto");
			/*----------------------------------------------------------------------------------*/
			/*SE OBTIENE TOTAL DE REGISTROS DE LA VISTA DE VENCIMIENTOS POR FECHA DE VENCIMEINTO*/
			/*----------------------------------------------------------------------------------*/
			qrySentence.append("select count(1) total_reg, sum(fg_totalvencimiento) totalvencimiento from comvis_vencimiento_fide ");
			qrySentence.append("where com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
			qrySentence.append("group by com_fechaprobablepago ");
			qrySentence.append("order by com_fechaprobablepago ");

			ps = con.queryPrecompilado(qrySentence.toString());
			ps.setString(1, fec_vencimiento);
			rs = ps.executeQuery();
			if (rs != null && rs.next()) {
				lstRegValidados = new ArrayList();
				lstRegValidados.add(rs.getString("total_reg"));
				lstRegValidados.add(rs.getString("totalvencimiento"));
				lstRegValidados.add("vista");
				lstValidados.add(lstRegValidados);

				sumaTotalVista = Integer.parseInt(rs.getString("total_reg"));
				montoTotalVista = montoTotalVista.add(new BigDecimal(rs.getString("totalvencimiento")));
			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			/*---------------------------------------------------------------------------------*/
			/*--------se establece query para actualizar registros que tengan errores----------*/
			/*---------------------------------------------------------------------------------*/
			qryError =
						"update com_pagos_fide set cg_error = ? " + "where com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
						"and ig_prestamo = ? " + "and ig_cliente = ? " + "and fg_totalvencimiento = ? " +
						"and ig_disposicion = ? ";

			/*------------------------------------------------------------------------*/
			/*SE OBTIENE REGISTROS DE TABLA TEMPORAL POR FECHA DE VENCIMEINTO*/
			/*------------------------------------------------------------------------*/
			qrySentence.delete(0, qrySentence.length());
			qrySentence.append("select to_char(com_fechaprobablepago,'dd/mm/yyyy') fec_venc, fg_totalvencimiento totalvencimiento, ");
			qrySentence.append("ig_prestamo prestamo, ig_cliente cliente, cg_estatus estatus, ig_disposicion disposicion ");
			qrySentence.append("from COM_PAGOS_FIDE ");
			qrySentence.append("where com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') ");
			qrySentence.append("and cg_estatus IN ('P', 'NP') ");
			qrySentence.append("and cg_estatus_proc = 'N' ");

			System.out.println("consDetMonitor:qrySentence>>" + qrySentence.toString());
			ps = con.queryPrecompilado(qrySentence.toString());
			ps.setString(1, fec_vencimiento);
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					regNoEncon = false;
					if (txtError.length() > 0)
						txtError.delete(0, txtError.length());
					/*---------------------------------*/
					/*SE REALIZA VALIDACIONES DE CAMPOS*/
					/*---------------------------------*/
					if (rs.getString("totalvencimiento") == null || !Comunes.esDecimal(rs.getString("totalvencimiento")))
						txtError.append("El campo: \"monto de Vencimiento\" no tiene un n�mero v�lido/");
					if (rs.getString("prestamo") == null || !Comunes.esNumero(rs.getString("prestamo")))
						txtError.append("El campo: \"prestamo\" no tiene un n�mero v�lido/");
					if (rs.getString("cliente") == null || !Comunes.esNumero(rs.getString("cliente")))
						txtError.append("El campo: \"cliente\" no tiene un n�mero v�lido/");
					if (rs.getString("estatus") == null ||
						 (!(rs.getString("estatus")).equals("P") && !(rs.getString("estatus")).equals("NP") &&
						  !(rs.getString("estatus")).equals("R") && !(rs.getString("estatus")).equals("T")))
						txtError.append("El campo: \"estatus\" es invalido/");


					if (rs.getString("estatus").equals("P")) {
						sumaTotalPagados++;
						montoTotalPagados = montoTotalPagados.add(new BigDecimal(rs.getString("totalvencimiento")));

					} else if (rs.getString("estatus").equals("NP")) {
						sumaTotalNoPagados++;
						montoTotalNoPagados = montoTotalNoPagados.add(new BigDecimal(rs.getString("totalvencimiento")));
					}

					if (txtError.length() > 0) {
						sumaTotalError++;
						montoTotalError =
											montoTotalError.add(new BigDecimal(rs.getString("totalvencimiento") != null ?
																						  rs.getString("totalvencimiento") : "0"));

						ps2 = con.queryPrecompilado(qryError);
						ps2.setString(1, txtError.toString());
						ps2.setString(2, rs.getString("fec_venc"));
						ps2.setLong(3, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(4, Long.parseLong(rs.getString("cliente")));
						ps2.setBigDecimal(5, new BigDecimal(rs.getString("totalvencimiento")));
						ps2.setLong(6, Long.parseLong(rs.getString("disposicion")));
						ps2.executeUpdate();
						if (rs2 != null)
							rs2.close();
						if (ps2 != null)
							ps2.close();
					}
					/*fin de validacion*/
				} //cierre while de validaciones
			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			/* SE OBTIENEN REGISTROS QUE REGRESO EL FIDE PERO NO SE ENCUENTRAN EN LA VISTA */
			String qryNoEncon =
						"SELECT TO_CHAR (pf.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc, " +
						"       pf.ig_prestamo prestamo, pf.ig_cliente cliente, " +
						"       pf.ig_disposicion disposicion, pf.fg_totalvencimiento totalvencimiento  " + "  FROM com_pagos_fide pf " +
						" WHERE NOT EXISTS ( " + "          SELECT vp.ig_prestamo " + "            FROM comvis_vencimiento_fide vp " +
						"           WHERE vp.com_fechaprobablepago = pf.com_fechaprobablepago " +
						"             AND vp.ig_prestamo = pf.ig_prestamo " + "             AND vp.ig_cliente = pf.ig_cliente " +
						"             AND vp.ig_disposicion = pf.ig_disposicion " +
						"             AND vp.fg_totalvencimiento = pf.fg_totalvencimiento) " +
						"   AND pf.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " + "   AND pf.cg_estatus_proc = 'N' " +
						"   AND pf.cg_estatus in ('P','NP') ";
			ps = con.queryPrecompilado(qryNoEncon);
			ps.setString(1, fec_vencimiento);
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					String txt_error = "Numero de Prestamo invalido, Registro no Encontrado/";
					ps2 = con.queryPrecompilado(qryError);
					ps2.setString(1, txt_error);
					ps2.setString(2, rs.getString("fec_venc"));
					ps2.setLong(3, Long.parseLong(rs.getString("prestamo")));
					ps2.setLong(4, Long.parseLong(rs.getString("cliente")));
					ps2.setBigDecimal(5, new BigDecimal(rs.getString("totalvencimiento")));
					ps2.setLong(6, Long.parseLong(rs.getString("disposicion")));
					ps2.executeUpdate();
					if (ps2 != null)
						ps2.close();

					sumaTotalNoEncon++;
					montoTotalNoEncon = montoTotalNoEncon.add(new BigDecimal(rs.getString("totalvencimiento")));
					sumaTotalError++;
					montoTotalError = montoTotalError.add(new BigDecimal(rs.getString("totalvencimiento")));
				} //cierre while
			} //cierre if rs
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();


			/* SE OBTIENE REGISTROS QUE NO FUERON REGERSADOS POR EL FIDE PERO SI SE ENCUENTRAN EN LA VISTA */
			String qryNoEnviados =
						"SELECT v.com_fechaprobablepago fec_venc, v.ig_prestamo prestamo, " +
						"       v.ig_cliente cliente, v.fg_totalvencimiento totalvencimiento " + "  FROM comvis_vencimiento_fide v " +
						"	WHERE v.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " + " AND NOT EXISTS ( " + "          SELECT 1 " +
						"            FROM com_pagos_fide c " + "           WHERE c.com_fechaprobablepago = v.com_fechaprobablepago " +
						"             AND c.ig_prestamo = v.ig_prestamo " + "             AND c.ig_cliente = v.ig_cliente " +
						"             AND c.ig_disposicion = v.ig_disposicion " +
						"             AND c.fg_totalvencimiento = v.fg_totalvencimiento " +
						"				  AND c.cg_estatus in ('P','NP')) ";

			ps = con.queryPrecompilado(qryNoEnviados);
			ps.setString(1, fec_vencimiento);
			rs = ps.executeQuery();

			if (rs != null)
				while (rs.next()) {
					sumaTotalNoEnv++;
					montoTotalNoEnv = montoTotalNoEnv.add(new BigDecimal(rs.getString("totalvencimiento")));
					sumaTotalError++;
					montoTotalError = montoTotalError.add(new BigDecimal(rs.getString("totalvencimiento")));
				}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			sumaTotalTMP = sumaTotalPagados + sumaTotalNoPagados + sumaTotalReemb;
			montoTotalTMP = montoTotalTMP.add(montoTotalPagados);
			montoTotalTMP = montoTotalTMP.add(montoTotalNoPagados);
			montoTotalTMP = montoTotalTMP.add(montoTotalReemb);

			if (sumaTotalVista == sumaTotalTMP) {
				if (montoTotalVista.equals(montoTotalTMP)) {
					con.terminaTransaccion(realizaTransaccion);
					autorizaValidacionMonitor(formaMonitor, false, "");
				}
			}

		} catch (Exception e) {
			realizaTransaccion = false;
			System.out.println("FondoJuniorBean::validaMonitor(error)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("FondoJuniorBean::validaMonitorPyNP(S)");
			con.terminaTransaccion(realizaTransaccion);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}


	public void procValidaMonitorRyT() throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;

		StringBuffer txtError = new StringBuffer();
		StringBuffer qrySentence = new StringBuffer();
		FondoJrMonitorBean formaMonitor = new FondoJrMonitorBean();

		BigDecimal montoTotalTMP = new BigDecimal("0.00");
		BigDecimal montoTotalReemb = new BigDecimal("0.00");
		BigDecimal montoTotalError = new BigDecimal("0.00");
		BigDecimal montoTotalRec = new BigDecimal("0.00");
		BigDecimal montoTotalNoEnv = new BigDecimal("0.00");
		BigDecimal montoTotalPrepago = new BigDecimal("0.00");


		int sumaTotalTMP = 0;
		int sumaTotalReemb = 0;
		int sumaTotalError = 0;
		int sumaTotalRec = 0;
		int sumaTotalNoEncon = 0;
		int sumaTotalPrepago = 0;
		boolean realizaTransaccion = true;
		boolean regNoEncon = false;

		try {

			con.conexionDB();
			formaMonitor.setFec_venc("RT");
			formaMonitor.setUsuario("proc_auto");

			String qryError =
						"update com_pagos_fide set cg_error = ? " + "where com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
						"and ig_prestamo = ? " + "and ig_cliente = ? " + "and fg_totalvencimiento = ? " +
						"and ig_disposicion = ? ";

			/*------------------------------------------------------------------------*/
			/*SE OBTIENE REGISTROS DE TABLA TEMPORAL POR FECHA DE VENCIMEINTO*/
			/*------------------------------------------------------------------------*/
			qrySentence.append("select to_char(com_fechaprobablepago,'dd/mm/yyyy') fec_venc, fg_totalvencimiento totalvencimiento, ");
			qrySentence.append("ig_prestamo prestamo, ig_cliente cliente, cg_estatus estatus, ig_disposicion disposicion ");
			qrySentence.append("from COM_PAGOS_FIDE ");
			qrySentence.append("where cg_estatus IN ('R','T') ");
			qrySentence.append("and cg_estatus_proc = 'N' ");

			System.out.println("consDetMonitor:qrySentence>>" + qrySentence.toString());
			ps = con.queryPrecompilado(qrySentence.toString());
			//ps.setString(1,fec_vencimiento);
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					regNoEncon = false;
					if (txtError.length() > 0)
						txtError.delete(0, txtError.length());
					/*---------------------------------*/
					/*SE REALIZA VALIDACIONES DE CAMPOS*/
					/*---------------------------------*/
					if (rs.getString("totalvencimiento") == null || !Comunes.esDecimal(rs.getString("totalvencimiento")))
						txtError.append("El campo: \"monto de Vencimiento\" no tiene un n�mero v�lido/");
					if (rs.getString("prestamo") == null || !Comunes.esNumero(rs.getString("prestamo")))
						txtError.append("El campo: \"prestamo\" no tiene un n�mero v�lido/");
					if (rs.getString("cliente") == null || !Comunes.esNumero(rs.getString("cliente")))
						txtError.append("El campo: \"cliente\" no tiene un n�mero v�lido/");
					if (rs.getString("estatus") == null ||
						 (!(rs.getString("estatus")).equals("P") && !(rs.getString("estatus")).equals("NP") &&
						  !(rs.getString("estatus")).equals("R") && !(rs.getString("estatus")).equals("T")))
						txtError.append("El campo: \"estatus\" es invalido/");


					if (rs.getString("estatus").equals("T")) { //Prepagos Totales
						sumaTotalPrepago++;
						montoTotalPrepago = montoTotalPrepago.add(new BigDecimal(rs.getString("totalvencimiento")));
					}


					if (rs.getString("estatus").equals("R")) { //CRXF
						sumaTotalReemb++;
						montoTotalReemb = montoTotalReemb.add(new BigDecimal(rs.getString("totalvencimiento")));
						String qryCompruebaReemb =
											"select fn_es_recuperacion_fondojr (to_date(?,'dd/mm/yyyy'), ?, ?, ?) validaReemb " +
											"from dual ";
						ps2 = con.queryPrecompilado(qryCompruebaReemb.toString());
						ps2.setString(1, rs.getString("fec_venc"));
						ps2.setLong(2, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(3, Long.parseLong(rs.getString("disposicion")));
						//ps2.setLong(3, Long.parseLong(rs.getString("cliente")));
						ps2.setBigDecimal(4, new BigDecimal(rs.getString("totalvencimiento")));

						rs2 = ps2.executeQuery();
						if (rs2 != null) {
											//System.out.println("entra a estatus reembolso");
											while (rs2.next()) {
								if (rs2.getString("validaReemb").equals("RC")) {
									sumaTotalRec++;
									montoTotalRec = montoTotalRec.add(new BigDecimal(rs.getString("totalvencimiento")));
								} else if (rs2.getString("validaReemb").equals("ERROR")) {
									//System.out.println("entra a error reembolso");
									txtError.append("No existe registro para aplicar Reembolso/");
								}
							} //cierre while rs2
						}

						if (rs2 != null)
							rs2.close();
						if (ps2 != null)
							ps2.close();
					} //cierre if (estatus R)

					if (txtError.length() > 0) {
						sumaTotalError++;
						montoTotalError =
											montoTotalError.add(new BigDecimal(rs.getString("totalvencimiento") != null ?
																						  rs.getString("totalvencimiento") : "0"));
						//}
						ps2 = con.queryPrecompilado(qryError);
						ps2.setString(1, txtError.toString());
						ps2.setString(2, rs.getString("fec_venc"));
						ps2.setLong(3, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(4, Long.parseLong(rs.getString("cliente")));
						ps2.setBigDecimal(5, new BigDecimal(rs.getString("totalvencimiento")));
						ps2.setLong(6, Long.parseLong(rs.getString("cliente")));
						ps2.executeUpdate();
						if (rs2 != null)
							rs2.close();
						if (ps2 != null)
							ps2.close();
					}
					/*fin de validacion*/
				} //cierre while de validaciones

				/* SE OBTIENEN REGISTROS QUE REGRESO EL FIDE PERO NO SE ENCUENTRAN EN LA VISTA */
				String qryNoEncon =
							  "SELECT TO_CHAR (pf.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc, " +
							  "       pf.ig_prestamo prestamo, pf.ig_cliente cliente, " +
							  "       pf.ig_disposicion disposicion, pf.fg_totalvencimiento totalvencimiento  " +
							  "  FROM com_pagos_fide pf " + " WHERE  NOT EXISTS ( " + "          SELECT vp.ig_prestamo " +
							  "            FROM comvis_vencimiento_fide vp " +
							  "           WHERE vp.com_fechaprobablepago = pf.com_fechaprobablepago " +
							  "             AND vp.ig_prestamo = pf.ig_prestamo " + "             AND vp.ig_cliente = pf.ig_cliente " +
							  "             AND vp.ig_disposicion = pf.ig_disposicion " +
							  "             AND vp.fg_totalvencimiento = pf.fg_totalvencimiento) " + "   AND pf.cg_estatus in (?, ?) " +
							  "   AND pf.cg_estatus_proc = 'N' ";
				ps = con.queryPrecompilado(qryNoEncon);
				ps.setString(1, "R");
				ps.setString(2, "T");
				rs = ps.executeQuery();
				if (rs != null) {
					while (rs.next()) {
						String txt_error = "Numero de Prestamo invalido, Registro no Encontrado/";
						ps2 = con.queryPrecompilado(qryError);
						ps2.setString(1, txt_error);
						ps2.setString(2, rs.getString("fec_venc"));
						ps2.setLong(3, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(4, Long.parseLong(rs.getString("cliente")));
						ps2.setBigDecimal(5, new BigDecimal(rs.getString("totalvencimiento")));
						ps2.setLong(6, Long.parseLong(rs.getString("disposicion")));
						ps2.executeUpdate();
						if (ps2 != null)
							ps2.close();

						sumaTotalNoEncon++;
						montoTotalNoEnv = montoTotalNoEnv.add(new BigDecimal(rs2.getString("totalvencimiento")));
						sumaTotalError++;
						montoTotalError = montoTotalError.add(new BigDecimal(rs2.getString("totalvencimiento")));
					} //cierre while
				} //cierre if rs
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();

				sumaTotalTMP = sumaTotalReemb + sumaTotalPrepago;
				montoTotalTMP = montoTotalTMP.add(montoTotalReemb);
				montoTotalTMP = montoTotalTMP.add(montoTotalPrepago);


			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			if (sumaTotalError == 0 && sumaTotalTMP > 0) {
				con.terminaTransaccion(realizaTransaccion);
				autorizaValidacionMonitor(formaMonitor, false, "");
			}


		} catch (Exception e) {
			realizaTransaccion = false;
			System.out.println("FondoJuniorBean::validaMonitor(error)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(realizaTransaccion);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}

	/**
	 * M�todo que obtiene los datos de un prestamo para realizar un reembolso por finiquito.
	 * @param numero_prestamo N�mero de prestamo del que se obtiene la informaci�n
	 * @return datos_prestamo Lista con la informaci�n del prestamo.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public List obtenerDatosPrestamo(String numero_prestamo, String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List datos_prestamo = new ArrayList();
		System.out.println("..:: FondoJuniorBean : obtenerDatosPrestamo(I) ::..");
		try {
			con.conexionDB();


			strSQL = new StringBuffer();
			varBind = new ArrayList();
			strSQL.append(" SELECT TO_DATE(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_movimiento,");
			strSQL.append(" cpf.ig_cliente num_cliente_sirac,");
			strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
			strSQL.append(" cpf.fg_totalvencimiento monto_reembolso,");
			strSQL.append(" cop.cg_descripcion origen  ");
			strSQL.append(" FROM com_pagos_fide_val cpf, comcat_origen_programa cop  ");
			strSQL.append(" WHERE cpf.ig_origen = cop.ig_origen_programa  ");
			strSQL.append(" AND cpf.ig_prestamo  = ?  ");
			strSQL.append(" AND cpf.ic_programa_fondojr = ? ");
			strSQL.append(" and cpf.cg_estatus in('NP')  ");
			strSQL.append(" AND cpf.cs_validacion = 'A' ");
			strSQL.append(" and  ig_disposicion not in (  SELECT ig_disposicion  ");
			strSQL.append(" FROM com_pagos_fide_val cpf, comcat_origen_programa cop  ");
			strSQL.append(" WHERE cpf.ig_origen = cop.ig_origen_programa  ");
			strSQL.append(" AND cpf.ig_prestamo  = ? ");
			strSQL.append(" AND cpf.ic_programa_fondojr = ?  ");
			strSQL.append(" and  cg_estatus in('R','P','RF', 'T') ) ");


			varBind.add(new Integer(numero_prestamo));
			varBind.add(new Integer(cvePrograma)); //FODEA 026-2010 FVR
			varBind.add(new Integer(numero_prestamo));
			varBind.add(new Integer(cvePrograma)); //FODEA 026-2010 FVR
			System.out.println(strSQL.toString());
			System.out.println(varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();


			while (rst.next()) {
				datos_prestamo.add(rst.getString(1) == null ? "" : rst.getString(1));
				datos_prestamo.add(rst.getString(2) == null ? "" : rst.getString(2));
				datos_prestamo.add(rst.getString(3) == null ? "" : rst.getString(3));
				datos_prestamo.add(rst.getString(4) == null ? "" : rst.getString(4));
				datos_prestamo.add(rst.getString(5) == null ? "" : rst.getString(5));
			}
			pst.close();
			rst.close();


		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : obtenerDatosPrestamo(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : obtenerDatosPrestamo(F) ::..");
		}
		return datos_prestamo;
	}

	/**
	 * M�todo que registra en la tabla temporal los reembolosos por finiquito.
	 * @param datos_reembolso Lista con los parametros:
	 *        0) Numero de proceso de carga.
	 *        1) Numero de prestamo.
	 *        2) Observaciones.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public void agregaReembolso(List datos_reembolso) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;

		System.out.println("..:: FondoJuniorBean : agregaReembolso(I) ::..");
		try {
			con.conexionDB();

			String proceso_carga = (String) datos_reembolso.get(0);
			String numero_prestamo = (String) datos_reembolso.get(1);
			String monto_reembolso = (String) datos_reembolso.get(2);
			String observaciones = (String) datos_reembolso.get(3);

			strSQL.append(" INSERT INTO com_tmp_reemb_x_finiquito (ic_reemb_x_finiquito, ig_prestamo, fn_monto_reembolso, cg_observaciones)");
			strSQL.append(" VALUES (?, ?, ?, ?)");

			varBind.add(new Long(proceso_carga));
			varBind.add(new Long(numero_prestamo));
			varBind.add(new Double(monto_reembolso));
			varBind.add(observaciones);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();

			pst.close();
		} catch (Exception e) {
			trans_op = false;
			System.out.println("..:: FondoJuniorBean : agregaReembolso(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : agregaReembolso(F) ::..");
		}
	}

	/**
	 * M�todo que obtiene los datos los prestamos agregados en el proceso de carga
	 * en curso.
	 * @param proceso_carga N�mero de proceso de carga en curso.
	 * @return reembolsos_temp Lista con la informaci�n de los prestamos cargados.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public List obtenerReembolsosTemp(String proceso_carga, String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List reembolsos_temp = new ArrayList();

		System.out.println("..:: FondoJuniorBean : obtenerReembolsosTemp(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" SELECT   TO_CHAR (cpf.df_registro_fide, 'dd/mm/yyyy') fecha_movimiento, ");
			strSQL.append(" cpf.ig_prestamo numero_prestamo, cpf.ig_cliente num_cliente_sirac, ");
			strSQL.append(" cpf.cg_nombrecliente nombre_cliente, ");
			strSQL.append(" cpf.fg_totalvencimiento monto_reembolso, cop.cg_descripcion origen, ");
			strSQL.append(" crf.cg_observaciones observaciones, cpf.ig_disposicion disposicion ");
			strSQL.append(" FROM com_pagos_fide_val cpf, ");
			strSQL.append(" comcat_origen_programa cop, ");
			strSQL.append(" com_tmp_reemb_x_finiquito crf ");
			strSQL.append(" WHERE cpf.ig_origen = cop.ig_origen_programa ");
			strSQL.append(" AND cpf.ig_prestamo = crf.ig_prestamo ");
			strSQL.append(" AND crf.ic_reemb_x_finiquito = ? ");
			strSQL.append(" AND cpf.cg_estatus = 'NP' ");
			strSQL.append(" AND cpf.cs_validacion = 'A' ");
			strSQL.append(" AND cpf.ic_programa_fondojr = ? ");
			strSQL.append(" AND NOT EXISTS (     ");
			strSQL.append(" SELECT cpfi.ig_disposicion, cpfi.ig_prestamo");
			strSQL.append(" FROM com_pagos_fide_val cpfi, ");
			strSQL.append(" comcat_origen_programa copi,");
			strSQL.append(" com_tmp_reemb_x_finiquito crfi ");
			strSQL.append(" WHERE cpfi.ig_origen = copi.ig_origen_programa ");
			strSQL.append(" AND cpfi.ig_prestamo = crfi.ig_prestamo ");
			strSQL.append(" AND crfi.ic_reemb_x_finiquito = ? ");
			strSQL.append(" AND cpfi.ic_programa_fondojr = ? ");
			strSQL.append(" AND cg_estatus <> 'NP' ");
			strSQL.append(" AND cpf.cs_validacion <> 'R' ");
			strSQL.append(" AND cpfi.ig_prestamo = cpf.ig_prestamo ");
			strSQL.append(" AND cpfi.ig_disposicion = cpf.ig_disposicion  )");
			strSQL.append(" ORDER BY numero_prestamo, disposicion  ");

			varBind.add(new Integer(proceso_carga));
			varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
			varBind.add(new Integer(proceso_carga)); //Fodea 027-2011
			varBind.add(new Long(cvePrograma));
			log.debug("strSQL.toString() " + strSQL.toString());
			log.debug("varBind " + varBind);
			log.debug("proceso_carga " + proceso_carga);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();


			while (rst.next()) {
				List reembolso = new ArrayList();
				reembolso.add(rst.getString(1) == null ? "" : rst.getString(1));
				reembolso.add(rst.getString(2) == null ? "" : rst.getString(2));
				reembolso.add(rst.getString(3) == null ? "" : rst.getString(3));
				reembolso.add(rst.getString(4) == null ? "" : rst.getString(4));
				reembolso.add(rst.getString(5) == null ? "" : rst.getString(5));
				reembolso.add(rst.getString(6) == null ? "" : rst.getString(6));
				reembolso.add(rst.getString(7) == null ? "" : rst.getString(7));
				reembolso.add(rst.getString("disposicion") == null ? "" : rst.getString("disposicion"));
				reembolsos_temp.add(reembolso);
			}

			pst.close();
			rst.close();
		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : obtenerReembolsosTemp(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : obtenerReembolsosTemp(F) ::..");
		}
		return reembolsos_temp;
	}

	/**
	 * M�todo que obtiene los datos de un prestamo agregado en el proceso de carga
	 * en curso.
	 * @param proceso_carga N�mero de proceso de carga en curso.
	 * @param numero_prestamo N�mero de prestamos de que se obtiene la informaci�n.
	 * @return reembolsos_temp Lista con la informaci�n de los prestamos cargados.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public List obtenerDatosReembolso(String proceso_carga, String numero_prestamo,
												 String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List datos_reembolso = new ArrayList();

		System.out.println("..:: FondoJuniorBean : obtenerReembolsosTemp(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" SELECT TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_movimiento,");
			strSQL.append(" cpf.ig_prestamo numero_prestamo,");
			strSQL.append(" cpf.ig_cliente num_cliente_sirac,");
			//strSQL.append(" pyme.cg_razon_social nombre_cliente,");
			strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
			strSQL.append(" cpf.fg_totalvencimiento monto_reembolso,");
			strSQL.append(" cop.cg_descripcion origen,");
			strSQL.append(" crf.cg_observaciones observaciones");
			strSQL.append(" FROM com_pagos_fide_val cpf,");
			//strSQL.append(" comcat_pyme pyme,");
			strSQL.append(" comcat_origen_programa cop,");
			strSQL.append(" com_tmp_reemb_x_finiquito crf");
			//strSQL.append(" WHERE cpf.ig_cliente = pyme.in_numero_sirac");
			strSQL.append(" WHERE cpf.ig_origen = cop.ig_origen_programa");
			strSQL.append(" AND cpf.ig_prestamo = crf.ig_prestamo");
			strSQL.append(" AND crf.ic_reemb_x_finiquito = ?");
			strSQL.append(" AND crf.ig_prestamo = ?");
			strSQL.append(" AND cpf.ic_programa_fondojr = ?"); //FODEA 026-2010 FVR

			varBind.add(new Integer(proceso_carga));
			varBind.add(new Integer(numero_prestamo));
			varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				datos_reembolso.add(rst.getString(1) == null ? "" : rst.getString(1));
				datos_reembolso.add(rst.getString(2) == null ? "" : rst.getString(2));
				datos_reembolso.add(rst.getString(3) == null ? "" : rst.getString(3));
				datos_reembolso.add(rst.getString(4) == null ? "" : rst.getString(4));
				datos_reembolso.add(rst.getString(5) == null ? "" : rst.getString(5));
				datos_reembolso.add(rst.getString(6) == null ? "" : rst.getString(6));
				datos_reembolso.add(rst.getString(7) == null ? "" : rst.getString(7));
			}

			pst.close();
			rst.close();
		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : obtenerReembolsosTemp(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : obtenerReembolsosTemp(F) ::..");
		}
		return datos_reembolso;
	}

	/**
	 * M�todo que modifica un registro de reembolso en la tabla
	 * temporal dentro de un proceso de carga.
	 * @param proceso_carga Numero de proceso de carga en curso.
	 * @param numero_prestamo N�mero de prestamo del que se obtiene la informaci�n.
	 * @return existe_reembolso true si existe el numero de prestamo false en caso contrario.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public void modificaReembolsoTmp(String proceso_carga, String numero_prestamo,
												String observaciones) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;

		System.out.println("..:: FondoJuniorBean : modificaReembolsoTmp(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" UPDATE com_tmp_reemb_x_finiquito SET cg_observaciones = ?");
			strSQL.append(" WHERE ic_reemb_x_finiquito = ?");
			strSQL.append(" AND ig_prestamo = ?");

			varBind.add(observaciones);
			varBind.add(new Long(proceso_carga));
			varBind.add(new Long(numero_prestamo));

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();

			pst.close();
		} catch (Exception e) {
			trans_op = false;
			System.out.println("..:: FondoJuniorBean : modificaReembolsoTmp(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : modificaReembolsoTmp(F) ::..");
		}
	}

	/**
	 * M�todo que elimina registro del reembolso en la tabla
	 * temporal dentro de un proceso de carga.
	 * @param proceso_carga Numero de proceso de carga en curso.
	 * @param numero_prestamo N�mero de prestamo del que se obtiene la informaci�n.
	 * @return existe_reembolso true si existe el numero de prestamo false en caso contrario.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public void eliminaReembolsoTmp(String proceso_carga, String numero_prestamo) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;

		System.out.println("..:: FondoJuniorBean : eliminaReembolsoTmp(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" DELETE com_tmp_reemb_x_finiquito");
			strSQL.append(" WHERE ic_reemb_x_finiquito = ?");
			strSQL.append(" AND ig_prestamo = ?");

			varBind.add(new Long(proceso_carga));
			varBind.add(new Long(numero_prestamo));

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();

			pst.close();
		} catch (Exception e) {
			trans_op = false;
			System.out.println("..:: FondoJuniorBean : eliminaReembolsoTmp(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : eliminaReembolsoTmp(F) ::..");
		}
	}

	/**
	 * M�todo que registra en la tabla final los reembolosos por finiquito.
	 * @param proceso_carga Numero de proceso de carga.
	 * @param usuario Numero y nombre de usuario que realiza la carga.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public void guardarReembolsosPorFiniquito(String proceso_carga, String numero_prestamo, String monto_reembolso,
															String usuario, String tipo_carga, String programa) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;

		System.out.println("..:: FondoJuniorBean : guardarReembolsosPorFiniquito(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" INSERT INTO com_pagos_fide_val (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion,");
			strSQL.append(" fg_amortizacion, fg_interes, fg_totalvencimiento, df_pago_cliente,");
			strSQL.append(" cg_estatus, ig_origen, df_periodofin, ig_numero_docto,");
			strSQL.append(" df_registro_fide, cg_observaciones, cg_usuario, cg_nombrecliente, cs_validacion, df_validacion, ic_programa_fondojr, ig_proc_desembolso )");
			strSQL.append(" SELECT cpf.com_fechaprobablepago, cpf.ig_prestamo, cpf.ig_cliente, cpf.ig_disposicion,");
			strSQL.append(" cpf.fg_amortizacion, cpf.fg_interes, cpf.fg_totalvencimiento, cpf.df_pago_cliente,");
			strSQL.append(" ?, cpf.ig_origen, cpf.df_periodofin, cpf.ig_numero_docto,");
			strSQL.append(" cpf.df_registro_fide, crf.cg_observaciones, ?, cg_nombrecliente, ?, sysdate, ic_programa_fondojr, crf.ic_reemb_x_finiquito ");
			strSQL.append(" FROM com_pagos_fide_val cpf,");
			strSQL.append(" com_tmp_reemb_x_finiquito crf");
			strSQL.append(" WHERE cpf.ig_prestamo = crf.ig_prestamo");
			strSQL.append(" AND crf.ic_reemb_x_finiquito = ?");
			strSQL.append(" AND cpf.cg_estatus = ? ");
			strSQL.append(" AND cpf.ic_programa_fondojr = ?  ");
			strSQL.append(" AND cpf.cs_validacion = 'A' ");
			strSQL.append("	AND NOT EXISTS (      ");
			strSQL.append(" SELECT cpfi.ig_disposicion, cpfi.ig_prestamo ");
			strSQL.append(" FROM com_pagos_fide_val cpfi, ");
			strSQL.append(" comcat_origen_programa copi, ");
			strSQL.append("  com_tmp_reemb_x_finiquito crfi ");
			strSQL.append(" WHERE cpfi.ig_origen = copi.ig_origen_programa ");
			strSQL.append("  AND cpfi.ig_prestamo = crfi.ig_prestamo");
			strSQL.append(" AND crfi.ic_reemb_x_finiquito = ? ");
			strSQL.append("  AND cpfi.ic_programa_fondojr = ? ");
			strSQL.append(" AND cpfi.cg_estatus <> 'NP' ");
			strSQL.append(" AND cpfi.cs_validacion <> 'R' ");
			strSQL.append("  AND cpfi.ig_prestamo = cpf.ig_prestamo ");
			strSQL.append("  AND cpfi.ig_disposicion = cpf.ig_disposicion  )");
			strSQL.append("  ORDER BY cpf.ig_prestamo, cpf.ig_disposicion ");

			varBind.add("RF");
			varBind.add(usuario);
			varBind.add("A");
			varBind.add(new Long(proceso_carga));
			varBind.add("NP");
			varBind.add(programa);
			varBind.add(new Long(proceso_carga));
			varBind.add(programa);

			System.out.println("strSQL.toString() " + strSQL.toString());

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			if (tipo_carga.equals("INDIVIDUAL")) {
				strSQL.append(" INSERT INTO com_reembolsos_finiquito (ig_prestamo, fn_monto_reembolso)");
				strSQL.append(" VALUES (?, ?)");

				varBind.add(numero_prestamo);
				varBind.add(monto_reembolso);
			} else {
				strSQL.append(" INSERT INTO com_reembolsos_finiquito (ig_prestamo, fn_monto_reembolso, ig_proc_desembolso )");
				strSQL.append(" SELECT DISTINCT ig_prestamo, fn_monto_reembolso, ic_reemb_x_finiquito FROM com_tmp_reemb_x_finiquito");
				strSQL.append(" WHERE ic_reemb_x_finiquito = ?");

				varBind.add(new Long(proceso_carga));
			}

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" DELETE com_cred_pend_reembolso_fide");
			strSQL.append(" WHERE ig_prestamo IN (");
			strSQL.append(" SELECT ig_prestamo");
			strSQL.append(" FROM com_tmp_reemb_x_finiquito");
			strSQL.append(" WHERE ic_reemb_x_finiquito = ?)");

			varBind.add(new Long(proceso_carga));

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
		} catch (Exception e) {
			trans_op = false;
			System.out.println("..:: FondoJuniorBean : guardarReembolsosPorFiniquito(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : guardarReembolsosPorFiniquito(F) ::..");
		}
	}

	/**
	 * M�todo que obtiene el n�mero de proceso con el que se identificar� el proceso
	 * de carga individual de los reembolsos por finiquito.
	 * @return datos_prestamo Lista con la informaci�n del prestamo.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public String getProcesoReembolso() throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		String numero_proceso = "";

		System.out.println("..:: FondoJuniorBean : getProcesoReembolso(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" SELECT NVL(MAX(ic_reemb_x_finiquito), 0) + 1 numero_proceso FROM com_tmp_reemb_x_finiquito");

			pst = con.queryPrecompilado(strSQL.toString());
			rst = pst.executeQuery();

			while (rst.next()) {
				numero_proceso = rst.getString(1) == null ? "" : rst.getString(1);
			}

			pst.close();
			rst.close();
		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : getProcesoReembolso(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : getProcesoReembolso(F) ::..");
		}
		return numero_proceso;
	}

	/**
	 * M�todo que valida que no se duplique el registro del reembolso en la tabla
	 * temporal dentro de un proceso de carga.
	 * @param proceso_carga Numero de proceso de carga en curso.
	 * @param numero_prestamo N�mero de prestamo del que se obtiene la informaci�n.
	 * @return existe_reembolso true si existe el numero de prestamo false en caso contrario.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public boolean existeReembolsoTmp(String proceso_carga, String numero_prestamo) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean existe_reembolso = true;

		System.out.println("..:: FondoJuniorBean : existeReembolsoTmp(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" SELECT COUNT(ic_reemb_x_finiquito) num_registros");
			strSQL.append(" FROM com_tmp_reemb_x_finiquito");
			strSQL.append(" WHERE ic_reemb_x_finiquito = ?");
			strSQL.append(" AND ig_prestamo = ?");

			varBind.add(new Long(proceso_carga));
			varBind.add(new Long(numero_prestamo));

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				if (rst.getString(1).equals("0")) {
					existe_reembolso = false;
				} else {
					existe_reembolso = true;
				}
			}

			pst.close();
			rst.close();
		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : existeReembolsoTmp(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : existeReembolsoTmp(F) ::..");
		}
		return existe_reembolso;
	}

	/**
	 * M�todo que valida que no se duplique el registro del reembolso en la tabla
	 * temporal dentro de un proceso de carga.
	 * @param proceso_carga Numero de proceso de carga en curso.
	 * @param numero_prestamo N�mero de prestamo del que se obtiene la informaci�n.
	 * @return existe_reembolso true si existe el numero de prestamo false en caso contrario.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public boolean existeReembolsoFin(String numero_prestamo, String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean existe_reembolso = true;

		System.out.println("..:: FondoJuniorBean : existeReembolsoFin(I) ::..");
		try {
			con.conexionDB();


			strSQL = new StringBuffer();
			varBind = new ArrayList();
			strSQL.append(" SELECT COUNT(ig_prestamo) numero_registros");
			strSQL.append(" FROM com_pagos_fide_val");
			strSQL.append(" WHERE ig_prestamo = ?");
			strSQL.append(" AND ic_programa_fondojr = ? "); //FODEA 026-2010 FVR
			strSQL.append(" and  cg_estatus in('NP')  ");
			strSQL.append(" AND cs_validacion = 'A' ");
			strSQL.append(" and  ig_disposicion not in (   ");
			strSQL.append(" SELECT ig_disposicion   ");
			strSQL.append(" FROM com_pagos_fide_val ");
			strSQL.append(" WHERE ig_prestamo = ? ");
			strSQL.append(" and  cg_estatus in('R','P','RF', 'T')  ");
			strSQL.append(" AND cs_validacion <> 'R' ");
			strSQL.append(" AND ic_programa_fondojr = ? ) ");

			varBind.add(new Long(numero_prestamo));
			varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
			varBind.add(new Long(numero_prestamo));
			varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				if (!rst.getString(1).equals("0")) {
					existe_reembolso = false;
				} else {
					existe_reembolso = true;
				}
			}

			pst.close();
			rst.close();


		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : existeReembolsoFin(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : existeReembolsoFin(F) ::..");
		}
		return existe_reembolso;
	}

	/**
	 * metodo que inserta los datos a la tabla de catalogo de Operaciones JR
	 * @throws com.netro.exception.NafinException
	 * @param operacion
	 * @param nombre
	 */

	public void insertarCatalagoOperacion(String nombre, String operacion, String clavePrograma) throws NafinException {
		System.out.println("FondoJuniorBean::insertarCatalagoOperacion(E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		int seqNEXTVAL = 0;

		try {
			con.conexionDB();


			String qrySEQ = "SELECT MAX(ig_oper_fondo_jr)+1 FROM comcat_oper_fondo_jr";
			ps1 = con.queryPrecompilado(qrySEQ);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				seqNEXTVAL = rs1.getString(1) == null ? 1 : rs1.getInt(1);
			}

			ps1.close();

			System.out.println("FondoJuniorBean :: nombre :: " + nombre + "   operacion::" + operacion);

			String qry =
						" INSERT INTO comcat_oper_fondo_jr(ig_oper_fondo_jr, cg_descripcion, cg_operacion, IC_PROGRAMA_FONDOJR) " +
						" VALUES(?,?,?,?)";

			ps = con.queryPrecompilado(qry);
			ps.setInt(1, seqNEXTVAL);
			ps.setString(2, nombre);
			ps.setString(3, operacion);
			ps.setString(4, clavePrograma);
			ps.executeUpdate();
			ps.close();
			rs1.close();
			System.out.println("qry:::" + qry);
		} catch (Exception e) {
			commit = false;
			System.out.println("FondoJuniorBean::insertarCatalagoOperacion(Exception) " + e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(commit);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		System.out.println("FondoJuniorBean::insertarCatalagoOperacion(S)");
	}

	/**
	 * metodo que modifica el catalogo de Operaciones  JR
	 * */
	public void modifica(String numero, String nombre, String modificar, String operacion) throws NafinException {


		AccesoDB con = null;
		String qrySentencia = "";
		boolean ok = true;

		System.out.println("FondoJuniorBean :: nuemro :: " + numero + " modificar::" + modificar);
		System.out.println("FondoJuniorBean :: nombre :: " + nombre + " operacion::" + operacion);


		try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia =
						" UPDATE comcat_oper_fondo_jr SET ig_oper_fondo_jr='" + numero + "', cg_descripcion='" + nombre +
						"', " + " cg_operacion='" + operacion + "' " + " where ig_oper_fondo_jr=" + numero;

			con.ejecutaSQL(qrySentencia);
		} catch (SQLException sqle) {
			ok = false;
			System.out.println(qrySentencia);
			System.out.println("FondoJuniorBean::modifica SQLExcepcion: " + sqle);
			throw new NafinException("SIST0001");
		} catch (Exception e) {
			ok = false;
			System.out.println("FondoJuniorBean::modifica Excepcion: " + e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		}

	}


	/**
	 *Metodo que elimina  el catalogo de Operacion  por numero de operador fondo jr.
	 * @throws com.netro.exception.NafinException
	 * @param numero
	 */
	public void eliminar(String numero) throws NafinException {
		System.out.println("FondoJuniorBean.eliminar(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String qryDeleteDatos = "";

		System.out.println("FondoJuniorBean :: eliminar :: " + " numero ::" + numero);

		try {
			con.conectarDB();
			qryDeleteDatos = "DELETE comcat_oper_fondo_jr WHERE ig_oper_fondo_jr =" + numero;
			ps = con.queryPrecompilado(qryDeleteDatos);
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			System.out.println(":: Ocurrio un error en eliminar exportador. :: " + e.toString());
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			System.out.println("FondoJuniorBean.eliminar(S)");
		}
	}

	//***********MOVIMIENTOS GENERALES AL FONDO JR ***************************

	/**
	 *metodo para genera el numero de proceso en las captura de
	 * movimientos generales de Fondos JR
	 * @throws com.netro.exception.NafinException
	 * @return
	 */
	public String Numeroproceso() throws NafinException {

		System.out.println("FondoJuniorBean::proceso(E)");

		AccesoDB con = new AccesoDB();
		boolean commit = true;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;

		String proceso = "";

		try {
			con.conexionDB();


			//String qrySEQ = "SELECT MAX(IG_MOV_GRAL)+1 FROM COMTMP_MOV_GRAL_FONDO_JR";
			String qrySEQ = "SELECT SEQ_COMTMP_MOV_GRAL_FONDO_JR.nextval FROM dual";
			ps1 = con.queryPrecompilado(qrySEQ);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				/*seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
				proceso = new Integer(seqNEXTVAL);
				*/
				proceso = rs1.getString(1);
			}

			ps1.close();
			rs1.close();
		} catch (Exception e) {
			commit = false;
			System.out.println("FondoJuniorBean::proceso(Exception) " + e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(commit);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		System.out.println("FondoJuniorBean::proceso(S)");
		return proceso;
	}


	/**
	 * Metodo que inserta registros de movimientos Generales de Fondos JR
	 * @throws com.netro.exception.NafinException
	 * @param TipoOperacion
	 * @param Observaciones
	 * @param referencia
	 * @param importe
	 * @param fecha
	 */
	public void insertMovGeneralesJR(String proceso, String fecha, String importe, String referencia, String Observaciones,
												String TipoOperacion, String clavePrograma) throws NafinException {

		System.out.println("FondoJuniorBean::insertMovGeneralesJR(E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		int seqNEXTVAL = 0;

		try {
			con.conexionDB();


			String qrySEQ = "SELECT MAX(IG_MOV_GRAL)+1 FROM COMTMP_MOV_GRAL_FONDO_JR";
			ps1 = con.queryPrecompilado(qrySEQ);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				seqNEXTVAL = rs1.getString(1) == null ? 1 : rs1.getInt(1);
			}

			ps1.close();


			System.out.println("FondoJuniorBean :: fecha :: " + fecha + "   importe::" + importe);
			System.out.println("FondoJuniorBean :: referencia :: " + referencia + "   observaciones::" + Observaciones);
			System.out.println("FondoJuniorBean :: TipoOperacion :: " + TipoOperacion);


			String qry =
						" INSERT INTO COMTMP_MOV_GRAL_FONDO_JR ( IG_MOV_GRAL, DF_FEC_MOVIMIENTO, " +
						" DF_FEC_VALOR, FG_CAPITAL, CG_REFERENCIA, IG_OPER_FONDO_JR, CG_OBSERVACIONES, IG_NO_PROCESO, IC_PROGRAMA_FONDOJR)" +
						" VALUES(?,sysdate,TO_DATE(?,'dd/mm/yyyy'),?,?,?,?,?,?)";

			ps = con.queryPrecompilado(qry);
			ps.setInt(1, seqNEXTVAL);
			ps.setString(2, fecha);
			ps.setString(3, importe);
			ps.setString(4, referencia);
			ps.setString(5, TipoOperacion);
			ps.setString(6, Observaciones);
			ps.setString(7, proceso);
			ps.setString(8, clavePrograma);


			ps.executeUpdate();
			ps.close();
			rs1.close();
			System.out.println("Query al Insertar a la tabla temporal" + qry);
		} catch (Exception e) {
			commit = false;
			System.out.println("FondoJuniorBean::insertMovGeneralesJR(Exception) " + e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(commit);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		System.out.println("FondoJuniorBean::insertMovGeneralesJR(S)");

	}


	/**
	 *Metodo que elimina el Movimiento Generales de Fondo JR
	 * @throws com.netro.exception.NafinException
	 * @param numero
	 */

	public void eliminarMovGeneralesJR(String numero) throws NafinException {

		System.out.println("FondoJuniorBean.eliminarMovGeneralesJR(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String qryDeleteDatos = "";

		System.out.println("FondoJuniorBean :: eliminar :: " + " numero ::" + numero);

		try {
			con.conectarDB();
			qryDeleteDatos = "DELETE COMTMP_MOV_GRAL_FONDO_JR WHERE IG_MOV_GRAL=" + numero;
			ps = con.queryPrecompilado(qryDeleteDatos);
			ps.executeUpdate();
			ps.close();
		} catch (Exception e) {
			System.out.println(":: Ocurrio un error en eliminar exportador. :: " + e.toString());
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			System.out.println("FondoJuniorBean.eliminarMovGeneralesJR(S)");
		}

	}

	/**
	 *Metodo que Modifica  el Movimiento General al Fondo JR
	 * @throws com.netro.exception.NafinException
	 * @param numero
	 */

	public void ModificarMovGeneralesJR(String numero, String fecha, String importe, String referencia,
													String observaciones, String TipoOperacion, String modificar,
													String clavePrograma) throws NafinException {


		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String qrySentencia = "";
		boolean ok = true;
		try {
			con.conexionDB();
			qrySentencia =
						" UPDATE COMTMP_MOV_GRAL_FONDO_JR SET DF_FEC_VALOR = TO_DATE(?,'dd/mm/yyyy'), " +
						" FG_CAPITAL = ?, " + " CG_REFERENCIA = ?, " + " IG_OPER_FONDO_JR = ?, " + " CG_OBSERVACIONES =? ,  " +
						" IC_PROGRAMA_FONDOJR =? " + " where IG_MOV_GRAL = ? ";


			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, fecha);
			ps.setString(2, importe);
			ps.setString(3, referencia);
			ps.setString(4, TipoOperacion);
			ps.setString(5, observaciones);
			ps.setString(6, clavePrograma);
			ps.setString(7, numero);

			ps.executeUpdate();
			ps.close();


		} catch (Exception e) {
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::guardaObservaciones (S)");
		}

	}

	/**
	 *Metodo para generar el archivo PDF
	 * en la consulta de Movimientos Generales JR
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param operacion
	 * @param fecha
	 */
	public List getConsultaMovimientosJR(String fecha, String operacion) throws NafinException {


		AccesoDB con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lista = new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();
		List conditions = new ArrayList();

		System.out.println("fecha: " + fecha + " operacion::  " + operacion);

		try {

			con = new AccesoDB();
			con.conexionDB();


			qrySentencia.append("SELECT mov.IG_MOV_GRAL as MOVIMIENTO, " + " mov.IG_FOLIO AS FOLIO, " +
									  " TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') as FECHA_MOVIMIENTO, " +
									  " TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, " + " mov.FG_CAPITAL AS IMPORTE, " +
									  " mov.CG_REFERENCIA AS REFERENCIA,  " + " cat.cg_operacion AS OPERACION, " +
									  " mov.CG_OBSERVACIONES AS OBSERVACIONES " + " FROM  COM_MOV_GRAL_FONDO_JR  mov,  COMCAT_OPER_FONDO_JR cat " +
									  " WHERE  mov.IG_OPER_FONDO_JR = cat.IG_OPER_FONDO_JR ");


			if (!operacion.equals("")) {

				qrySentencia.append("AND mov.IG_OPER_FONDO_JR = ? ");
				conditions.add(operacion);
			}

			if (!fecha.equals("")) {
				qrySentencia.append(" AND TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') = ? ");
				conditions.add(fecha);
			}


			ps = con.queryPrecompilado(qrySentencia.toString(), conditions);
			rs = ps.executeQuery();

			System.out.println("Consulta--- : " + qrySentencia.toString());
			while (rs.next()) {

				FondoJRMovimientoGeneral prApo = new FondoJRMovimientoGeneral();

				prApo.setNumero(rs.getString("MOVIMIENTO") == null ? "" : rs.getString("MOVIMIENTO"));
				prApo.setFechaMovimiento(rs.getString("FECHA_MOVIMIENTO") == null ? "" : rs.getString("FECHA_MOVIMIENTO"));
				prApo.setFechaValor(rs.getString("FECHA_VALOR") == null ? "" : rs.getString("FECHA_VALOR"));
				prApo.setImporte(rs.getString("IMPORTE") == null ? "" : rs.getString("IMPORTE"));
				prApo.setReferencia(rs.getString("REFERENCIA") == null ? "" : rs.getString("REFERENCIA"));
				prApo.setOperacion(rs.getString("OPERACION") == null ? "" : rs.getString("OPERACION"));
				prApo.setObservaciones(rs.getString("OBSERVACIONES") == null ? "" : rs.getString("OBSERVACIONES"));

				lista.add(prApo);

			}

			System.out.println("lista: " + lista);

			rs.close();
			con.cierraStatement();
		} catch (Exception e) {
			System.out.println("Error en: " + e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return lista;
	} // fin


	/**
	 * metodo que termina la operacion  e inserta los datos de los movimientos
	 * Generales de Fondos JR a la tabla original COM_MOV_GRAL_FONDO_JR
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param validCert
	 * @param formaMovimiento
	 */

	public String TerminaOperacion(FondoJRMovimientoGeneral formaMovimiento, boolean validCert,
											 ArrayList ListaDatos) throws NafinException {
		System.out.println("FondoJuniorBean::TerminaOperacion(E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		PreparedStatement ps = null;
		StringBuffer qry = new StringBuffer();
		List conditions = new ArrayList();
		String Acuse = "";
		String mostrarError = "";
		try {
			con.conexionDB();
			char getReceipt = 'Y';
			if (ListaDatos.size() != 0) {
				System.out.println("TerminaOperacion :: formaMovimiento.getProceso() :: ");
				if ((!validCert) ||
					 (!ListaDatos.get(0).toString().equals("") && !ListaDatos.get(1).toString().equals("") &&
					  !ListaDatos.get(2).toString().equals(""))) {
					Seguridad s = new Seguridad();
					if ((!validCert) ||
						 (s.autenticar(ListaDatos.get(3).toString(), ListaDatos.get(0).toString(),
											ListaDatos.get(2).toString(), ListaDatos.get(1).toString(), getReceipt))) {
						Acuse = s.getAcuse();
						System.out.println("***************************");
						System.out.println("**Acuse***" + Acuse);
						System.out.println("***************************");
						if (Acuse != null) {
							qry.append("INSERT INTO COM_MOV_GRAL_FONDO_JR (IG_MOV_GRAL, IG_FOLIO, DF_FEC_MOVIMIENTO, " +
										  " DF_FEC_VALOR, FG_CAPITAL, CG_REFERENCIA, IG_OPER_FONDO_JR, CG_OBSERVACIONES, CG_NOUSUARIO, IG_NO_PROCESO, IC_PROGRAMA_FONDOJR)" +
										  " SELECT mov.IG_MOV_GRAL, mov.IG_FOLIO, mov.DF_FEC_MOVIMIENTO, mov.DF_FEC_VALOR, mov.FG_CAPITAL, " +
										  " mov.CG_REFERENCIA, mov.IG_OPER_FONDO_JR, mov.CG_OBSERVACIONES, ? , mov.IG_NO_PROCESO, mov.IC_PROGRAMA_FONDOJR" +
										  "  FROM COMTMP_MOV_GRAL_FONDO_JR mov, COMCAT_OPER_FONDO_JR cat" +
										  " WHERE  mov.IG_OPER_FONDO_JR = cat.IG_OPER_FONDO_JR ");
							conditions.add(ListaDatos.get(4).toString());


							if (!ListaDatos.get(5).toString().equals("")) {
								qry.append(" AND mov.IG_NO_PROCESO = ? ");
								conditions.add(ListaDatos.get(5).toString());
							}

							ps = con.queryPrecompilado(qry.toString(), conditions);
							ps.executeUpdate();
							ps.close();
							//rs.close();
						}
					} else {
						mostrarError = s.mostrarError();
					}
				}
			} else {
				System.out.println("**Version Original***");
				if ((!validCert) ||
					 (!formaMovimiento.getSerial().equals("") && !formaMovimiento.getTextoFirmado().equals("") &&
					  !formaMovimiento.getPkcs7().equals(""))) {
					Seguridad s = new Seguridad();
					if ((!validCert) ||
						 (s.autenticar(formaMovimiento.getFolio(), formaMovimiento.getSerial(), formaMovimiento.getPkcs7(),
											formaMovimiento.getTextoFirmado(), getReceipt))) {
						Acuse = s.getAcuse();
						System.out.println("***************************");
						System.out.println("**Acuse***" + Acuse);
						System.out.println("***************************");
						if (Acuse != null) {
							qry.append("INSERT INTO COM_MOV_GRAL_FONDO_JR (IG_MOV_GRAL, IG_FOLIO, DF_FEC_MOVIMIENTO, " +
										  " DF_FEC_VALOR, FG_CAPITAL, CG_REFERENCIA, IG_OPER_FONDO_JR, CG_OBSERVACIONES, CG_NOUSUARIO, IG_NO_PROCESO, IC_PROGRAMA_FONDOJR)" +
										  " SELECT mov.IG_MOV_GRAL, mov.IG_FOLIO, mov.DF_FEC_MOVIMIENTO, mov.DF_FEC_VALOR, mov.FG_CAPITAL, " +
										  " mov.CG_REFERENCIA, mov.IG_OPER_FONDO_JR, mov.CG_OBSERVACIONES, ? , mov.IG_NO_PROCESO, mov.IC_PROGRAMA_FONDOJR" +
										  "  FROM COMTMP_MOV_GRAL_FONDO_JR mov, COMCAT_OPER_FONDO_JR cat" +
										  " WHERE  mov.IG_OPER_FONDO_JR = cat.IG_OPER_FONDO_JR ");
							conditions.add(formaMovimiento.getTermina());
							if (!formaMovimiento.getProceso().equals("")) {
								qry.append(" AND mov.IG_NO_PROCESO = ? ");
								conditions.add(formaMovimiento.getProceso());
							}
							ps = con.queryPrecompilado(qry.toString(), conditions);
							ps.executeUpdate();
							ps.close();
							//rs.close();
						}
					} else {
						mostrarError = s.mostrarError();
					}
				}
			}
			System.out.println("FondoJuniorBean::Query Insert Original " + qry + conditions);
			System.out.println("FondoJuniorBean::mostrarError " + mostrarError);
		} catch (Exception e) {
			commit = false;
			System.out.println("FondoJuniorBean::TerminaOperacion(Exception) " + e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(commit);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		System.out.println("FondoJuniorBean::TerminaOperacion(S)");
		return Acuse;
	}


	/**
	 *Metodo que regresa el acuse de los movimientos generales de Fondos JR
	 * @throws com.netro.exception.NafinException
	 * @return
	 */
	public List AcuseMoviGenerales(String proceso) throws NafinException {

		System.out.println("FondoJuniorBean::AcuseMoviGenerales(E)");

		AccesoDB con = new AccesoDB();
		boolean commit = true;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List lista = new ArrayList();

		StringBuffer qryConsulta = new StringBuffer();
		List conditions = new ArrayList();

		System.out.println("AcuseMoviGenerales :: proceso :: " + proceso);

		try {
			con.conexionDB();

			qryConsulta.append(" SELECT mov.IG_MOV_GRAL as NUMERO, " +
									 " TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') AS FECHA_MOVIMIENTO, " +
									 " TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, " + " mov.FG_CAPITAL AS IMPORTE, " +
									 " mov.CG_REFERENCIA AS REFERENCIA, " + " cat.cg_operacion AS OPERACION, " +
									 " mov.CG_OBSERVACIONES AS OBSERVACIONES " + " FROM COM_MOV_GRAL_FONDO_JR mov , COMCAT_OPER_FONDO_JR cat " +
									 " WHERE  mov.IG_OPER_FONDO_JR = cat.IG_OPER_FONDO_JR  AND IG_NO_PROCESO = ?");

			conditions.add(proceso);

			System.out.println(" qryConsulta-AcuseMoviGenerales-- " + qryConsulta + "conditions----" + conditions);


			ps = con.queryPrecompilado(qryConsulta.toString(), conditions);
			rs = ps.executeQuery();

			while (rs.next()) {
				FondoJRMovimientoGeneral datos = new FondoJRMovimientoGeneral();

				datos.setNumero(rs.getString("NUMERO") == null ? "" : rs.getString("NUMERO"));
				datos.setFechaMovimiento(rs.getString("FECHA_MOVIMIENTO") == null ? "" : rs.getString("FECHA_MOVIMIENTO"));
				datos.setFechaValor(rs.getString("FECHA_VALOR") == null ? "" : rs.getString("FECHA_VALOR"));
				datos.setImporte(rs.getString("IMPORTE") == null ? "" : rs.getString("IMPORTE"));
				datos.setReferencia(rs.getString("REFERENCIA") == null ? "" : rs.getString("REFERENCIA"));
				datos.setOperacion(rs.getString("OPERACION") == null ? "" : rs.getString("OPERACION"));
				datos.setObservaciones(rs.getString("OBSERVACIONES") == null ? "" : rs.getString("OBSERVACIONES"));

				lista.add(datos);
			}

			//ps.close();
			rs.close();

			//System.out.println("lista  AcuseMoviGenerales " + lista);
		} catch (Exception e) {
			commit = false;
			System.out.println("FondoJuniorBean::AcuseMoviGenerales(Exception) " + e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(commit);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		System.out.println("FondoJuniorBean::AcuseMoviGenerales(S)");
		return lista;


	}

	/**
	 *Metodo para regregar el archivo pdf del acuse de Movimientos Generales
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param proceso
	 */

	public List AcuseMoviGeneralesPDF(String proceso) throws NafinException {

		System.out.println("FondoJuniorBean::AcuseMoviGeneralesPDF(E)");

		AccesoDB con = new AccesoDB();
		boolean commit = true;
		PreparedStatement ps = null;
		ResultSet rs = null;

		List lista = new ArrayList();

		StringBuffer qryConsulta = new StringBuffer();
		List conditions = new ArrayList();

		System.out.println("AcuseMoviGenerales :: proceso :: " + proceso);

		try {
			con.conexionDB();

			qryConsulta.append(" SELECT mov.IG_MOV_GRAL as NUMERO, " +
									 " TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') AS FECHA_MOVIMIENTO, " +
									 " TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, " + " mov.FG_CAPITAL AS IMPORTE, " +
									 " mov.CG_REFERENCIA AS REFERENCIA, " + " cat.cg_operacion AS OPERACION, " +
									 " mov.CG_OBSERVACIONES AS OBSERVACIONES " + " FROM COM_MOV_GRAL_FONDO_JR mov , COMCAT_OPER_FONDO_JR cat " +
									 " WHERE  mov.IG_OPER_FONDO_JR = cat.IG_OPER_FONDO_JR  AND IG_NO_PROCESO = ?");


			conditions.add(proceso);

			System.out.println(" qryConsulta-AcuseMoviGenerales-- " + qryConsulta + "conditions----" + conditions);


			ps = con.queryPrecompilado(qryConsulta.toString(), conditions);
			rs = ps.executeQuery();

			while (rs.next()) {
				FondoJRMovimientoGeneral datos = new FondoJRMovimientoGeneral();

				datos.setNumero(rs.getString("NUMERO") == null ? "" : rs.getString("NUMERO"));
				datos.setFechaMovimiento(rs.getString("FECHA_MOVIMIENTO") == null ? "" : rs.getString("FECHA_MOVIMIENTO"));
				datos.setFechaValor(rs.getString("FECHA_VALOR") == null ? "" : rs.getString("FECHA_VALOR"));
				datos.setImporte(rs.getString("IMPORTE") == null ? "" : rs.getString("IMPORTE"));
				datos.setReferencia(rs.getString("REFERENCIA") == null ? "" : rs.getString("REFERENCIA"));
				datos.setOperacion(rs.getString("OPERACION") == null ? "" : rs.getString("OPERACION"));
				datos.setObservaciones(rs.getString("OBSERVACIONES") == null ? "" : rs.getString("OBSERVACIONES"));

				lista.add(datos);
			}

			//ps.close();
			rs.close();

			System.out.println("lista  AcuseMoviGenerales " + lista);
		} catch (Exception e) {
			commit = false;
			System.out.println("FondoJuniorBean::AcuseMoviGenerales(Exception) " + e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(commit);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		System.out.println("FondoJuniorBean::AcuseMoviGeneralesPDF(S)");
		return lista;


	}


	public void CancelarMovGeneralesJR(String proceso) throws NafinException {

		System.out.println("FondoJuniorBean.eliminarMovGeneralesJR(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer qryDeleteDatos = new StringBuffer();
		List conditions = new ArrayList();


		try {
			con.conectarDB();
			qryDeleteDatos.append("DELETE COMTMP_MOV_GRAL_FONDO_JR  where IG_NO_PROCESO = ?");
			conditions.add(proceso);
			ps = con.queryPrecompilado(qryDeleteDatos.toString(), conditions);
			ps.executeUpdate();
			ps.close();

			System.out.println("Query de Cancelar Datos del a Tabla TMP  " + qryDeleteDatos + "Condiciones" + conditions);

		} catch (Exception e) {
			System.out.println(":: Ocurrio un error en eliminar exportador. :: " + e.toString());
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			System.out.println("FondoJuniorBean.eliminarMovGeneralesJR(S)");
		}

	}

	/**
	 * M�todo que realiza la insersion de los datos de Tabla 1(COMVIS_VENCIMIENTO_FIDE)
	 * a la Tabla 2 (com_pagos_fide_val)
	 * @param nombre_usuario Usuario de sesi�n.
	 * @param clavePrograma Clave interna del programa del fondo junior.
	 * @return distfondo, si la insersion se realizo con exito regresa un "1"
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 011 - 2009 Fondo Junior
	 * @author Ivan Almaguer
	 */
	public List DisposicionDeFondoInsert(String nombre_usuario, String clavePrograma) throws NafinException {
		log.debug("��� DisposicionDeFondoInsert(E) ���");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		boolean bcommit = true;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List distfondo = new ArrayList();

		try {
			con.conexionDB();

			strSQL.append("INSERT INTO com_pagos_fide_val  " +
							  "     ( COM_FECHAPROBABLEPAGO, IG_PRESTAMO, IG_CLIENTE, IG_DISPOSICION, FG_AMORTIZACION, " +
							  "       FG_INTERES, FG_TOTALVENCIMIENTO, CG_ESTATUS, IG_ORIGEN, " +
							  "       DF_PERIODOFIN, IG_NUMERO_DOCTO, CG_OBSERVACIONES, " + "       CG_USUARIO ) " + "SELECT " +
							  "       COM_FECHAPROBABLEPAGO, IG_PRESTAMO, IG_CLIENTE, IG_DISPOSICION, FG_AMORTIZACION, " +
							  "       FG_INTERES, FG_TOTALVENCIMIENTO,?,?,DF_PERIODOFIN, IG_NUMERO_DOCTO, " + // ? CG_ESTATUS = No pagadas, ? IG_ORIGEN = 'No Definido'
							  "       ?, " + //? = Disposicion de Fondo por Contingencia
							  "       ? " + // ? = USUARIO DE SESION
							  " FROM  COMVIS_VENCIMIENTO_FIDE " +
							  "WHERE  TO_CHAR (COM_FECHAPROBABLEPAGO,'dd/mm/yyyy') = TO_CHAR (SYSDATE ,'dd/mm/yyyy') " +
							  "AND IC_PROGRAMA_FONDOJR = ? "); //FODEA 026 - 2010 ACF

			varBind.add("NP");
			varBind.add("3");
			varBind.add("Disposicion de Fondo por Contingencia");
			varBind.add(nombre_usuario);
			varBind.add(new Integer(clavePrograma)); //FODEA 026 - 2010

			log.debug("..:: strSQL: " + strSQL.toString());
			log.debug("..:: varBind: " + varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
		} catch (Exception e) {
			bcommit = false;
			log.info("��� DisposicionDeFondoInsert -ERROR- ��� ");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bcommit);
				distfondo.add("1");
				con.cierraConexionDB();
			}
			log.debug("��� DisposicionDeFondoInsert(S) ���");
		}
		return distfondo;
	}

	/**
	 * M�todo que realiza la consulta para Disposicion de Fondo por Contingencia
	 * @param nombre_usuario Usuario de sesi�n.
	 * @param clavePrograma Clave interna del programa del fondo junior.
	 * @return distfondoTemp Lista con la informaci�n de la consulta.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 011 - 2009 Fondo Junior
	 * @author Ivan Almaguer
	 */
	public List DisposicionDeFondoConsulta(String nombre_usuario, String clavePrograma) throws NafinException {
		log.info("��� DisposicionDeFondoConsulta(E) ���");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List distfondo_temp = new ArrayList();

		try {
			con.conexionDB();

			strSQL.append("SELECT TO_CHAR (vista.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
							  "       TO_CHAR (SYSDATE ,'dd/mm/yyyy') AS fechaoperacion, " +
							  "       TO_CHAR (SYSDATE, 'dd/mm/yyyy') AS fechafide, " + "       vista.IG_PRESTAMO AS prestamo, " +
							  "       vista.IG_CLIENTE AS numsirac, " + "       vista.CG_NOMBRECLIENTE AS cliente, " +
							  "       vista.IG_DISPOSICION AS numcuota, " + "       vista.FG_AMORTIZACION AS capital, " +
							  "       vista.FG_INTERES AS interes, " + "       vista.FG_TOTALVENCIMIENTO AS totalpagar, " +
							  "       tots.registrostotal, " + "       tots.capitaltotal, " + "       tots.interestotal, " +
							  "       tots.sumatotalpagar " + "  FROM comvis_vencimiento_fide vista, " +
							  "  (SELECT COUNT (1) AS registrostotal,  " + "           SUM (FG_AMORTIZACION) AS capitaltotal, " +
							  "           SUM (FG_INTERES) AS interestotal, " + "           SUM (FG_TOTALVENCIMIENTO) AS sumatotalpagar " +
							  "     FROM comvis_vencimiento_fide " +
							  //"    WHERE TO_CHAR(COM_FECHAPROBABLEPAGO,'dd/mm/yyyy') = TO_CHAR (SYSDATE ,'dd/mm/yyyy')) tots " +//FODEA 026 - 2010 ACF
							  "    WHERE TO_CHAR(COM_FECHAPROBABLEPAGO,'dd/mm/yyyy') = TO_CHAR (SYSDATE ,'dd/mm/yyyy') " +
							  //FODEA 026 - 2010 ACF
				"    AND IC_PROGRAMA_FONDOJR = ?) tots " + //FODEA 026 - 2010 ACF
				"    WHERE TO_CHAR(COM_FECHAPROBABLEPAGO,'dd/mm/yyyy') = TO_CHAR (SYSDATE ,'dd/mm/yyyy') " +
						  "    AND IC_PROGRAMA_FONDOJR = ? "); //FODEA 026 - 2010 ACF

			log.debug("��� strSQL ��� " + strSQL);

			pst = con.queryPrecompilado(strSQL.toString());
			pst.setInt(1, Integer.parseInt(clavePrograma)); //FODEA 026 - 2010 ACF
			pst.setInt(2, Integer.parseInt(clavePrograma)); //FODEA 026 - 2010 ACF
			rst = pst.executeQuery();

			while (rst.next()) {
				List distfondo = new ArrayList();
				distfondo.add(rst.getString(1) == null ? "" : rst.getString(1));
				distfondo.add(rst.getString(2) == null ? "" : rst.getString(2));
				distfondo.add(rst.getString(3) == null ? "" : rst.getString(3));
				distfondo.add(rst.getString(4) == null ? "" : rst.getString(4));
				distfondo.add(rst.getString(5) == null ? "" : rst.getString(5));
				distfondo.add(rst.getString(6) == null ? "" : rst.getString(6));
				distfondo.add(rst.getString(7) == null ? "" : rst.getString(7));
				distfondo.add(rst.getString(8) == null ? "" : rst.getString(8));
				distfondo.add(rst.getString(9) == null ? "" : rst.getString(9));
				distfondo.add(rst.getString(10) == null ? "" : rst.getString(10));
				distfondo.add(rst.getString(11) == null ? "" : rst.getString(11));
				distfondo.add(rst.getString(12) == null ? "" : rst.getString(12));
				distfondo.add(rst.getString(13) == null ? "" : rst.getString(13));
				distfondo.add(rst.getString(14) == null ? "" : rst.getString(14));
				distfondo_temp.add(distfondo);
			}

			pst.close();
			rst.close();
		} catch (Exception e) {
			log.info("��� DisposicionDeFondoConsulta -ERROR- ���");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("��� DisposicionDeFondoConsulta(S) ���");
		}
		return distfondo_temp;
	}

	/**
	 * M�todo que realiza las consultas para obtener los Detalles de Credito a Validar
	 * @param ver_enlace tipo de detalle a mostrar
	 * @param estatus, proc, condiciones de consultas
	 * @return verdetalleTemp Lista con la informaci�n de la consulta.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 011 - 2009 Fondo Junior
	 * @author Ivan Almaguer
	 */
	public List ObtenerDetalleDeCreditos(String ver_enlace, String estatus, String estatus2, String proc, String fec_venc,
													 String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();

		List varBind = new ArrayList();
		List verdetalle_temp = new ArrayList();
		List titulo = new ArrayList();

		System.out.println("��� FondoJuniorBean �� ObtenerDetalleDeCreditos(I) ���");
		System.out.println("��� ver_enlace �� " + ver_enlace + " ���");
		System.out.println("��� estatus �� " + estatus + " ���");
		try {
			con.conexionDB();

			if ("Recuperaciones".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,   " +
								  "       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,   " +
								  "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,   " +
								  "       tmp.ig_prestamo AS prestamo,   " + "       tmp.ig_cliente AS numsirac,   " +
								  "       NVL(vista.cg_nombrecliente,'') AS cliente,   " + "       tmp.ig_disposicion AS numcuota,   " +
								  "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error   " +
								  "  FROM comvis_vencimiento_fide vista,    " + "       com_pagos_fide tmp    " +
								  " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago  " +
								  "	AND vista.ig_cliente(+)  = tmp.ig_cliente  " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion  " +
								  "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento  " +
								  "   AND vista.ig_prestamo(+) = tmp.ig_prestamo  " + " AND tmp.CG_ESTATUS = ?   " +
								  " AND 'RC' = FN_ES_RECUPERACION_FONDOJR (tmp.COM_FECHAPROBABLEPAGO, tmp.IG_PRESTAMO,    " +
								  "                      tmp.IG_DISPOSICION, tmp.FG_TOTALVENCIMIENTO)    " +
								  " AND tmp.ic_programa_fondojr = ? ");
				varBind.add(estatus);
				varBind.add(new Long(cvePrograma));
				titulo.add("Recuperaciones Recibidas");
				verdetalle_temp.add(titulo);
			} else if ("NoEncontrados".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (tmp.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
								  "'' AS fechaoperacion," + "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,   " +
								  "       tmp.IG_PRESTAMO AS prestamo, " + "       tmp.IG_CLIENTE AS numsirac, " + "'' AS cliente, " +
								  "       tmp.IG_DISPOSICION AS numcuota, " + "       tmp.fg_totalvencimiento AS capital_mas_interes " +
								  "       FROM com_pagos_fide tmp      " + "WHERE  tmp.cg_estatus in (?,?)  ");
				if ("RC".equals(estatus2)) {
					strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				}
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
				strSQL.append("  AND NOT EXISTS  " + "    ( " + "     SELECT vp.ig_prestamo  " +
								  "              FROM comvis_vencimiento_fide vp                   " +
								  "             WHERE vp.com_fechaprobablepago = tmp.com_fechaprobablepago " +
								  "               AND vp.ig_prestamo = tmp.ig_prestamo " +
								  "               AND vp.ig_cliente  = tmp.ig_cliente " +
								  "               AND vp.ig_disposicion = tmp.ig_disposicion " +
								  "               AND vp.fg_totalvencimiento = tmp.fg_totalvencimiento " + "     ) " +
								  "	  AND tmp.ic_programa_fondojr = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(estatus2);
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma));
				titulo.add("Registros No encontrados");
				verdetalle_temp.add(titulo);

			} else if ("NoEnviados".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (vista.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
								  "       TO_CHAR (vista.DF_FECHAOPERA, 'dd/mm/yyyy') AS fechaoperacion,   " + "'' AS fechapago, " +
								  "       vista.IG_PRESTAMO AS prestamo, " + "       vista.IG_CLIENTE AS numsirac, " + "'' AS cliente, " +
								  "       vista.IG_DISPOSICION AS numcuota, " + "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes " +
								  "       FROM comvis_vencimiento_fide vista     " + "WHERE NOT EXISTS  " + "    ( " +
								  "     SELECT tmp.ig_prestamo  " + "              FROM com_pagos_fide tmp                   " +
								  "             WHERE tmp.COM_FECHAPROBABLEPAGO = vista.COM_FECHAPROBABLEPAGO " +
								  "               AND tmp.ig_prestamo = vista.ig_prestamo " +
								  "               AND tmp.ig_cliente  = vista.ig_cliente " +
								  "               AND tmp.ig_disposicion = vista.ig_disposicion " +
								  "               AND tmp.fg_totalvencimiento = vista.fg_totalvencimiento " +
								  "               AND tmp.CG_ESTATUS in (?,?) " + "					 AND tmp.ic_programa_fondojr = ? " + //FODEA 026-2010 FVR
								  "     ) ");
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append(" AND vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");

				strSQL.append(" AND vista.ic_programa_fondojr = ? "); //FODEA 026-2010 FVR

				varBind.add(estatus);
				varBind.add(estatus2);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				titulo.add("Cr&eacute;ditos No enviados del d&iacute;a");
				verdetalle_temp.add(titulo);

			} else if ("Vencimientos".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (vista.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								  "       TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy') AS fechaoperacion, " + "       '' AS fechapago, " +
								  "       vista.ig_prestamo AS prestamo,  " + "       vista.ig_cliente AS numsirac,  " +
								  "       vista.cg_nombrecliente AS cliente,  " + "       vista.ig_disposicion AS numcuota,  " +
								  "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes  " + "  FROM comvis_vencimiento_fide vista " +
								  " WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
								  " AND vista.ic_programa_fondojr = ? "); //FODEA 026-2010 FVR);
				varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				titulo.add("Vencimientos del d&iacute;a");
				verdetalle_temp.add(titulo);
			} else {
				strSQL.append("SELECT DISTINCT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								  //FODEA 057 - 2009 ACF
					"       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,  " +
								 "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,  " +
								 "       tmp.ig_prestamo AS prestamo,  " + "       tmp.ig_cliente AS numsirac,  " +
								 "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       tmp.ig_disposicion AS numcuota,  " +
								 "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error  " +
								 "  FROM comvis_vencimiento_fide vista,   " + "       com_pagos_fide tmp  " +
								 " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
								 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
								 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
								 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "	 AND tmp.ic_programa_fondojr = ? ");
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
			}

			if ("Enviados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in ('P','NP') " +
								  " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
				varBind.add(fec_venc);
				titulo.add("Total de Registros Enviados");
				verdetalle_temp.add(titulo);
			}
			if ("RembolsosPrepagosTotales".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in (?) "); //FODEA 026-2010 FVR SE QUITA 'R'
				if ("R".equals(estatus)) {
					titulo.add("Total Reembolsos");
					varBind.add(estatus);
					if (!fec_venc.equals("") && fec_venc.length() > 2) {
						strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
						varBind.add(fec_venc);
					}
					strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("RC".equals(estatus)) {
					titulo.add("Total Recuperaciones");
					varBind.add(estatus2);
					strSQL.append(" AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("T".equals(estatus)) {
					titulo.add("Total Prepragos");
					varBind.add(estatus);
				}

				verdetalle_temp.add(titulo);
			}

			if ("Prepagos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? ");
				varBind.add(estatus);
				titulo.add("Prepagos Totales");
				verdetalle_temp.add(titulo);
			}
			if ("Pagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				titulo.add("Cr&eacute;ditos Pagados del D&iacute;a");
				verdetalle_temp.add(titulo);

			}
			if ("NoPagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);
				titulo.add("Cr&eacute;ditos No Pagados del D&iacute;a");
				verdetalle_temp.add(titulo);

			}
			if ("NoPagadosRech".equals(ver_enlace)) { //F027-2011 FVR
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);
				titulo.add("Cr&eacute;ditos No Pagados Rechazados del D&iacute;a");
				verdetalle_temp.add(titulo);

			}
			if ("Reembolsos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " +
								  " AND 'RC' != fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, tmp.ig_prestamo,   " +
								  "                      tmp.ig_disposicion, tmp.fg_totalvencimiento)  ");
				varBind.add(estatus);
				if (!fec_venc.equals("") && fec_venc.length() > 2) {
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
					varBind.add(fec_venc);
				}
				titulo.add("Reembolsos Totales");
				verdetalle_temp.add(titulo);
			}
			if ("ConError".equals(ver_enlace)) {
				if (!fec_venc.equals("") && fec_venc.length() > 2 && !"R".equals(estatus)) {
					strSQL.append(" AND tmp.cg_estatus in (?,?) " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
									  " AND tmp.cg_error is not null ");
					//varBind.add(proc);
					strSQL.append(" UNION ALL SELECT TO_CHAR(vista.com_fechaprobablepago,'dd/mm/yyyy') as fechaautorizacion, " +
									  "       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion, " + "			''  AS fechapago, " +
									  "		  vista.ig_prestamo as prestamo, " + "       vista.ig_cliente as numsirac, " +
									  "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       vista.ig_disposicion AS numcuota,  " +
									  "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes , " +
									  "       'Registro no Enviado por el FIDE' AS error  " + "  FROM comvis_vencimiento_fide vista " +
									  "	WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " + "	AND vista.ic_programa_fondojr = ? " +
									  "	AND NOT EXISTS ( " + "          SELECT 1 " + "            FROM com_pagos_fide c " +
									  "           WHERE c.com_fechaprobablepago = vista.com_fechaprobablepago " +
									  "             AND c.ig_prestamo = vista.ig_prestamo " + "             AND c.ig_cliente = vista.ig_cliente " +
									  "             AND c.ig_disposicion = vista.ig_disposicion " +
									  "             AND c.fg_totalvencimiento = vista.fg_totalvencimiento " + "				  AND c.cg_estatus in (?,?) " +
									  "				  AND c.ic_programa_fondojr = ?) ");
					varBind.add("P");
					varBind.add("NP");
					varBind.add(fec_venc);
					varBind.add(fec_venc);
					varBind.add(new Long(cvePrograma));
					varBind.add("P");
					varBind.add("NP");
					varBind.add(new Long(cvePrograma));
				} else {
					if ("R".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus);
						if (!fec_venc.equals("") && fec_venc.length() > 2) {
							strSQL.append(" AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
							varBind.add(fec_venc);
						}
						strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else if ("RC".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus2);

						strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add("T");
					}
				}


				titulo.add("Cr&eacute;ditos con ERROR");
				verdetalle_temp.add(titulo);
			}

			System.out.println("strSQL.toString()>>>>>>" + strSQL.toString());
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				List verdetalle = new ArrayList();
				verdetalle.add(rst.getString(1) == null ? "" : rst.getString(1));
				verdetalle.add(rst.getString(2) == null ? "" : rst.getString(2));
				verdetalle.add(rst.getString(3) == null ? "" : rst.getString(3));
				verdetalle.add(rst.getString(4) == null ? "" : rst.getString(4));
				verdetalle.add(rst.getString(5) == null ? "" : rst.getString(5));
				verdetalle.add(rst.getString(6) == null ? "" : rst.getString(6));
				verdetalle.add(rst.getString(7) == null ? "" : rst.getString(7));
				verdetalle.add(rst.getString(8) == null ? "" : rst.getString(8));
				if ("ConError".equals(ver_enlace))
					verdetalle.add(rst.getString(9) == null ? "" : rst.getString(9));
				verdetalle_temp.add(verdetalle);
			}

			rst.close();
			pst.close(); //se agrega por correccion a errore del mes del 09/2010


			if (verdetalle_temp != null && verdetalle_temp.size() > 1) {
						//System.out.println("��� verdetalle_temp �� "+verdetalle_temp+" ���");
						System.out.println("��� Registros encontrados ���");
			} else {
				//System.out.println("��� verdetalle �� "+verdetalle+" ���");
				System.out.println("��� No existen registros ���");
				verdetalle_temp.clear();
			}

		} catch (Exception e) {
			System.out.println("��� FondoJuniorBean �� ObtenerDetalleDeCreditos -ERROR- ���");
			e.printStackTrace();
		} finally {
			con.terminaTransaccion(true); //se agrega por correccion a errore del mes del 09/2010
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("��� strSQL ��� " + strSQL);
			System.out.println("��� varBind �� " + varBind + " ���");
			System.out.println("��� FondoJuniorBean �� ObtenerDetalleDeCreditos(F) ���");
		}
		return verdetalle_temp;
	}

	/**
	 * M�todo que realiza la consulta para la pantalla de Validaci�n.
	 * @param parametros_consulta Lista con los valores de los parametros de consulta.
	 * @return operaciones_validacion Lista con los resultados de la consulta.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public List obtenerOperacionesValidacion(List parametros_consulta) throws NafinException {
		log.info("obtenerOperacionesValidacion(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List operaciones_validacion = new ArrayList();

		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");

		try {
			con.conexionDB();

			String fecha_amort_nafin_ini = (String) parametros_consulta.get(0);
			String fecha_amort_nafin_fin = (String) parametros_consulta.get(1);
			String estatus_registro = (String) parametros_consulta.get(2);
			String numero_prestamo = (String) parametros_consulta.get(3);
			String clave_if = (String) parametros_consulta.get(4);
			String numero_sirac = (String) parametros_consulta.get(5);
			String fecha_pago_cliente_fide_ini = (String) parametros_consulta.get(6);
			String fecha_pago_cliente_fide_fin = (String) parametros_consulta.get(7);
			String fecha_registro_fide_ini = (String) parametros_consulta.get(8);
			String fecha_registro_fide_fin = (String) parametros_consulta.get(9);
			String cs_validacion = (String) parametros_consulta.get(10);
			String clavePrograma = (String) parametros_consulta.get(11); //FODEA 026 - 2010 ACF
			String folioValidacion = (String) parametros_consulta.get(12); //----------- ACF
			String fecha_amortizacion = "";

			if (fecha_amort_nafin_ini.equals("")) {
				strSQL = new StringBuffer();
				strSQL.append(" SELECT TO_CHAR(MIN(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
				strSQL.append(" FROM com_pagos_fide_val cpf");

				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();

				while (rst.next()) {
					fecha_amort_nafin_ini =
									 rst.getString("fecha_amort_nafin") == null ? "" : rst.getString("fecha_amort_nafin");
				}

				rst.close();
				pst.close();
			}

			if (fecha_amort_nafin_fin.equals("")) {
				strSQL = new StringBuffer();
				strSQL.append(" SELECT TO_CHAR(MAX(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
				strSQL.append(" FROM com_pagos_fide_val cpf");

				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();

				while (rst.next()) {
					fecha_amort_nafin_fin =
									 rst.getString("fecha_amort_nafin") == null ? "" : rst.getString("fecha_amort_nafin");
				}

				rst.close();
				pst.close();
			}

			fecha_amortizacion = fecha_amort_nafin_ini;

			while (Comunes.esComparacionFechaValida(fecha_amortizacion, "le", fecha_amort_nafin_fin, "dd/MM/yyyy")) {
				List operaciones_por_fecha = new ArrayList();
				List operaciones_pagados = new ArrayList();
				List operaciones_no_pagados = new ArrayList();
				List operaciones_reembolsos = new ArrayList();
				List operaciones_prepagos = new ArrayList();
				List operaciones_recuperaciones = new ArrayList();

				String dia = fecha_amortizacion.substring(0, fecha_amortizacion.indexOf("/"));
				String mes =
							  fecha_amortizacion.substring(fecha_amortizacion.indexOf("/") + 1,
																	 fecha_amortizacion.lastIndexOf("/"));
				String anio = fecha_amortizacion.substring(fecha_amortizacion.lastIndexOf("/") + 1);

				calendario.set(Integer.parseInt(anio), Integer.parseInt(mes) - 1, Integer.parseInt(dia));

				fecha_amortizacion = formato_fecha.format(calendario.getTime());

				if (!estatus_registro.equals("")) {
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add(estatus_registro);
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)

					log.debug("..:: strSQL : " + strSQL.toString());
					log.debug("..:: varBind : " + varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));

						if (estatus_registro.equals("P")) {
							operaciones_pagados.add(operacion_por_fecha);
						} else if (estatus_registro.equals("NP")) {
							operaciones_no_pagados.add(operacion_por_fecha);
						} else if (estatus_registro.equals("R")) {
							operaciones_reembolsos.add(operacion_por_fecha);
						} else if (estatus_registro.equals("T")) {
							operaciones_prepagos.add(operacion_por_fecha);
						} else if (estatus_registro.equals("RF")) {
							operaciones_recuperaciones.add(operacion_por_fecha);
						}
					}
					pst.close();
					rst.close();

					operaciones_por_fecha.add(fecha_amortizacion);
					operaciones_por_fecha.add(operaciones_pagados);
					operaciones_por_fecha.add(operaciones_no_pagados);
					operaciones_por_fecha.add(operaciones_reembolsos);
					operaciones_por_fecha.add(operaciones_prepagos);
					operaciones_por_fecha.add(operaciones_recuperaciones);
					operaciones_validacion.add(operaciones_por_fecha);
				} else {
					/*REGISTROS PAGADOS*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("P");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					log.debug("..:: strSQL : " + strSQL.toString());
					log.debug("..:: varBind : " + varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));
						operaciones_pagados.add(operacion_por_fecha);
					}

					pst.close();
					rst.close();

					/*REGISTROS NO PAGADOS*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("NP");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					log.debug("..:: strSQL : " + strSQL.toString());
					log.debug("..:: varBind : " + varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));
						operaciones_no_pagados.add(operacion_por_fecha);
					}

					pst.close();
					rst.close();

					/*REEMBOLSOS*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("R");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					log.debug("..:: strSQL : " + strSQL.toString());
					log.debug("..:: varBind : " + varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));
						operaciones_reembolsos.add(operacion_por_fecha);
					}

					pst.close();
					rst.close();

					/*PREPAGOS TOTALES*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("T");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					log.debug("..:: strSQL : " + strSQL.toString());
					log.debug("..:: varBind : " + varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));
						operaciones_prepagos.add(operacion_por_fecha);
					}

					pst.close();
					rst.close();

					/*RECUPERACION DE FONDO JR*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("RC");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					log.debug("..:: strSQL : " + strSQL.toString());
					log.debug("..:: varBind : " + varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));
						operaciones_recuperaciones.add(operacion_por_fecha);
					}

					pst.close();
					rst.close();

					operaciones_por_fecha.add(fecha_amortizacion);
					operaciones_por_fecha.add(operaciones_pagados);
					operaciones_por_fecha.add(operaciones_no_pagados);
					operaciones_por_fecha.add(operaciones_reembolsos);
					operaciones_por_fecha.add(operaciones_prepagos);
					operaciones_por_fecha.add(operaciones_recuperaciones);
					operaciones_validacion.add(operaciones_por_fecha);
				}

				calendario.add(Calendar.DATE, 1);
				fecha_amortizacion = formato_fecha.format(calendario.getTime());
			}
		} catch (Exception e) {
			log.info("obtenerOperacionesValidacion(ERROR) ::..");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerOperacionesValidacion(S) ::..");
		}
		return operaciones_validacion;
	}

	/**
	 * M�todo obtiene en n�mero SIRAC de una determinada pyme.
	 * @param clave_pyme Clave de la pyme
	 * @return numero_sirac Numero SIRAC de la pyme.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public List busquedaAvanzadaProveedor(String val_nombre, String val_rfc, String numero_sirac) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List informacion_proveedores = new ArrayList();

		System.out.println("..:: FondoJuniorBean : busquedaAvanzadaProveedor(I) ::..");

		try {
			con.conexionDB();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT pyme.in_numero_sirac numero_sirac, UPPER(pyme.cg_razon_social) nombre_pyme");
			strSQL.append(" FROM comcat_pyme pyme");
			strSQL.append(" WHERE pyme.cs_habilitado = ?");
			strSQL.append(" AND pyme.in_numero_sirac IS NOT NULL");
			if (!val_nombre.equals("")) {
				strSQL.append(" AND pyme.cg_razon_social LIKE NVL(REPLACE(UPPER(?), '*', '%'), '%')");
			}
			if (!val_rfc.equals("")) {
				strSQL.append(" AND pyme.cg_rfc LIKE NVL(REPLACE(UPPER(?), '*', '%'), '%')");
			}
			if (!numero_sirac.equals("")) {
				strSQL.append(" AND pyme.in_numero_sirac = ?");
			}
			strSQL.append(" ORDER BY pyme.cg_razon_social");

			varBind.add("S");
			if (!val_nombre.equals("")) {
				varBind.add(val_nombre);
			}
			if (!val_rfc.equals("")) {
				varBind.add(val_rfc);
			}
			if (!numero_sirac.equals("")) {
				varBind.add(new Long(numero_sirac));
			}

			System.out.println("..:: strSQL : " + strSQL.toString());
			System.out.println("..:: varBind : " + varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				List datos_pyme = new ArrayList();
				datos_pyme.add(rst.getString(1) == null ? "" : rst.getString(1));
				datos_pyme.add(rst.getString(2) == null ? "" : rst.getString(2));
				informacion_proveedores.add(datos_pyme);
			}

			pst.close();
			rst.close();
		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : busquedaAvanzadaProveedor(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : busquedaAvanzadaProveedor(F) ::..");
		}
		return informacion_proveedores;
	}

	/**
   * M�todo que guarda la validaci�n de los cr�ditos de la pantalla de Validaci�n.
   * @param parametros_consulta Lista con los valores de los parametros de consulta.
   * @throws NafinException Excepcion lanzada cuando ocurre un error.
   * @since FODEA 013 - 2009 Fondo Junior
   * @author Alberto Cruz Flores
   */ /*
   public void guardarValidacionCreditos(List parametros_consulta) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List creditos_validados = new ArrayList();
    boolean trans_op = true;

    Calendar calendario = Calendar.getInstance();
    SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");

    System.out.println("..:: FondoJuniorBean : guardarValidacionCreditos(I) ::..");
    try{
      con.conexionDB();

      String fecha_amort_nafin_ini = (String)parametros_consulta.get(0);
      String fecha_amort_nafin_fin = (String)parametros_consulta.get(1);
      String estatus_registro = (String)parametros_consulta.get(2);
      String numero_prestamo = (String)parametros_consulta.get(3);
      String clave_if = (String)parametros_consulta.get(4);
      String numero_sirac = (String)parametros_consulta.get(5);
      String fecha_pago_cliente_fide_ini = (String)parametros_consulta.get(6);
      String fecha_pago_cliente_fide_fin = (String)parametros_consulta.get(7);
      String fecha_registro_fide_ini = (String)parametros_consulta.get(8);
      String fecha_registro_fide_fin = (String)parametros_consulta.get(9);
      String tipo_validacion = (String)parametros_consulta.get(10);
      String seccion_estatus = (String)parametros_consulta.get(11);
      String fecha_amortizacion = "";

      if(fecha_amort_nafin_ini.equals("")){
        strSQL = new StringBuffer();
        strSQL.append(" SELECT TO_CHAR(MIN(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
        strSQL.append(" FROM com_pagos_fide_val cpf");

        pst = con.queryPrecompilado(strSQL.toString());
        rst = pst.executeQuery();

        while(rst.next()){fecha_amort_nafin_ini = rst.getString("fecha_amort_nafin")==null?"":rst.getString("fecha_amort_nafin");}

        rst.close();
        pst.close();
      }

      if(fecha_amort_nafin_fin.equals("")){
        strSQL = new StringBuffer();
        strSQL.append(" SELECT TO_CHAR(MAX(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
        strSQL.append(" FROM com_pagos_fide_val cpf");

        pst = con.queryPrecompilado(strSQL.toString());
        rst = pst.executeQuery();

        while(rst.next()){fecha_amort_nafin_fin = rst.getString("fecha_amort_nafin")==null?"":rst.getString("fecha_amort_nafin");}

        rst.close();
        pst.close();
      }

      fecha_amortizacion = fecha_amort_nafin_ini;

      while(Comunes.esComparacionFechaValida(fecha_amortizacion, "le", fecha_amort_nafin_fin, "dd/MM/yyyy")){
        List operaciones_por_fecha = new ArrayList();
        List operaciones_pagados = new ArrayList();
        List operaciones_no_pagados = new ArrayList();
        List operaciones_reembolsos = new ArrayList();
        List operaciones_prepagos = new ArrayList();
        List operaciones_recuperaciones = new ArrayList();

        String dia = fecha_amortizacion.substring(0, fecha_amortizacion.indexOf("/"));
        String mes = fecha_amortizacion.substring(fecha_amortizacion.indexOf("/") + 1, fecha_amortizacion.lastIndexOf("/"));
        String anio = fecha_amortizacion.substring(fecha_amortizacion.lastIndexOf("/") + 1);

        calendario.set(Integer.parseInt(anio), Integer.parseInt(mes) - 1, Integer.parseInt(dia));

        fecha_amortizacion = formato_fecha.format(calendario.getTime());

        strSQL = new StringBuffer();
        varBind = new ArrayList();

        strSQL.append(" SELECT cpf.ig_prestamo numero_prestamo");
        strSQL.append(" FROM com_pagos_fide_val cpf");
        //strSQL.append(", comcat_pyme pyme");
        strSQL.append(", comcat_origen_programa prg");
        if(!clave_if.equals("")){                                             //..:: intermediario financiero
          strSQL.append(", comcat_pyme pyme");
          strSQL.append(", comrel_cuenta_bancaria ccb");
          strSQL.append(", comrel_pyme_if cpi");
        }
        //strSQL.append(" WHERE cpf.ig_cliente = pyme.in_numero_sirac");
        strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
        strSQL.append(" AND cpf.cs_validacion = ?");                            //..:: estatus de la validaci�n
        strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin

        if(!numero_prestamo.equals("")){                                      //..:: numero de prestamo
          strSQL.append(" AND cpf.ig_prestamo = ?");
        }
        if(!clave_if.equals("")){                                             //..:: intermediario financiero
          strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
          strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
          strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
          strSQL.append(" AND pyme.cs_habilitado = ?");
          strSQL.append(" AND cpi.cs_vobo_if = ?");
          strSQL.append(" AND ccb.ic_moneda = ?");
          strSQL.append(" AND cpi.ic_if = ?");
        }
        if(!numero_sirac.equals("")){                                         //..:: numero sirac cliente
          strSQL.append(" AND cpf.ig_cliente = ?");
        }
        if(!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")){//..:: fecha pago cliente fide
          strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
        }
        if(!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")){//..:: fecha a consultar
          strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
        }

        varBind.add("P");                                      //..:: estatus de la validaci�n
        varBind.add(fecha_amortizacion);                                      //..:: fecha de amortizacion nafin
        if(!numero_prestamo.equals("")){                                      //..:: numero de prestamo
          varBind.add(new Long(numero_prestamo));
        }
        if(!clave_if.equals("")){                                             //..:: intermediario financiero
          varBind.add("S");
          varBind.add("S");
          varBind.add(new Integer(1));
          varBind.add(new Integer(clave_if));
        }
        if(!numero_sirac.equals("")){                                         //..:: numero sirac cliente
          varBind.add(new Long(numero_sirac));
        }
        if(!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")){//..:: fecha pago cliente fide
          varBind.add(fecha_pago_cliente_fide_ini);
          varBind.add(fecha_pago_cliente_fide_fin);
        }
        if(!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")){//..:: fecha a consultar
          varBind.add(fecha_registro_fide_ini);
          varBind.add(fecha_registro_fide_fin);
        }

        if(seccion_estatus.equals("PNP")){
          if(!estatus_registro.equals("")){
            strSQL.append(" AND cpf.cg_estatus = ?");                             //..:: estatus del registro
            varBind.add(estatus_registro);
          }else{
            strSQL.append(" AND cpf.cg_estatus IN (?, ?)");                             //..:: estatus del registro
            varBind.add("P");
            varBind.add("NP");
          }
        }else{
          if(!estatus_registro.equals("")){
            strSQL.append(" AND cpf.cg_estatus = ?");                             //..:: estatus del registro
            varBind.add(estatus_registro);
          }else{
            strSQL.append(" AND cpf.cg_estatus IN (?, ?, ?)");                             //..:: estatus del registro
            varBind.add("R");
            varBind.add("T");
            varBind.add("RC");
          }
        }

        //System.out.println("..:: strSQL : "+strSQL.toString());
        //System.out.println("..:: varBind : "+varBind);

        pst = con.queryPrecompilado(strSQL.toString(), varBind);
        rst = pst.executeQuery();

        while(rst.next()){
          creditos_validados.add(rst.getString(1)==null?"":rst.getString(1));
        }

        pst.close();
        rst.close();

        calendario.add(calendario.DATE, 1);
        fecha_amortizacion = formato_fecha.format(calendario.getTime());
      }

      if(!creditos_validados.isEmpty()){
        for(int i = 0; i < creditos_validados.size(); i++){
          strSQL = new StringBuffer();
          varBind = new ArrayList();

          strSQL.append(" UPDATE com_pagos_fide_val cpf");
          strSQL.append(" SET cpf.cs_validacion = ?,");
          strSQL.append(" cpf.df_validacion = SYSDATE");
          strSQL.append(" WHERE cpf.ig_prestamo = ?");

          varBind.add(tipo_validacion.equals("ACEPTAR")?"A":"R");
          varBind.add(new Long((String)creditos_validados.get(i)));

          //System.out.println("..:: strSQL : "+strSQL.toString());
          //System.out.println("..:: varBind : "+varBind);

          pst = con.queryPrecompilado(strSQL.toString(), varBind);
          pst.executeUpdate();
        }
      }
    }catch(Exception e){
      trans_op = false;
      System.out.println("..:: FondoJuniorBean : guardarValidacionCreditos(ERROR) :");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.terminaTransaccion(trans_op);
        con.cierraConexionDB();
      }
      System.out.println("..:: FondoJuniorBean : guardarValidacionCreditos(F) ::..");
    }
   }
*/

	/**
	 * Metodo para traer datos para el fondo concentrador.
	 * @param clavePrograma Clave de programa del Fondo Junior
	 * @return lstMontosConcentrado Lista con el concentrado de la Conciliaci�n.
	 * @throws NafinException Cuando ocurre un error en la consulta.
	 */
	public List consConciliacionConcentrado(String clavePrograma) throws NafinException {
		log.info("consConciliacionConcentrado(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String fec_ultimo_mov = "";
		String qrySentence = "";
		int iClavePrograma = 0;


		//**********************************Validaci�n de parametros:*****************************
		try {
			if ((clavePrograma == null || clavePrograma.equals(""))) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			iClavePrograma = Integer.parseInt(clavePrograma);
		} catch (Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		//***************************************************************************************

		List lstMontosGral = new ArrayList();
		List lstMontosConcentrado = new ArrayList();
		BigDecimal montoP = new BigDecimal("0.00");
		BigDecimal montoNP = new BigDecimal("0.00");
		BigDecimal montoR = new BigDecimal("0.00");
		BigDecimal montoRF = new BigDecimal("0.00");
		BigDecimal montoRC = new BigDecimal("0.00");
		BigDecimal montoT = new BigDecimal("0.00");
		BigDecimal montoSUM = new BigDecimal("0.00");
		BigDecimal montoRES = new BigDecimal("0.00");
		BigDecimal montoIni = new BigDecimal("0.00");
		try {
			con.conexionDB();
			//query para obtener fecha de ultimo movimiento
			qrySentence =
						" SELECT TO_CHAR (MAX (tablota.fec_mov), 'dd/mm/yyyy') fec_ultimo_mov " + " FROM (" +
						"    SELECT /*+ first_rows(10) */ " + "       MAX(cpv.df_registro_fide) fec_mov " +
						" FROM com_pagos_fide_val cpv " + " WHERE cpv.cg_estatus in('P','T','RC','R') " +
						"	AND cpv.cs_validacion = 'A' " + "    AND cpv.ic_programa_fondojr = ? " + //FODEA 026 - 2010 ACF
						" UNION ALL " +
						//"        /*QUERY PARA TOTAL DE REEMB. POR FINIQ*/ " +
						//se rescribe la consulta remplazando el not exists con un left join, esto porque sabemos que hay relativamente pocos registros con estatus RF
						//El query original se muestra m�s abajo, comentado...
						"    SELECT /*+ index (cpv CP_COM_PAGOS_FIDE_VAL_PK */   " + "       MAX(cpv.df_registro_fide) fec_mov  " +
						"    FROM com_pagos_fide_val cpv LEFT JOIN com_pagos_fide_val x ON  x.com_fechaprobablepago = cpv.com_fechaprobablepago  " +
						"        AND x.ig_prestamo = cpv.ig_prestamo  " + "        AND x.ig_cliente = cpv.ig_cliente  " +
						"        AND x.ig_disposicion = cpv.ig_disposicion  " +
						"        AND x.fg_totalvencimiento = cpv.fg_totalvencimiento " + "        AND x.cg_estatus = 'RC'  " +
						"        AND x.ic_programa_fondojr = cpv.ic_programa_fondojr " + " WHERE cpv.cg_estatus = 'RF' " +
						" AND cpv.cs_validacion = 'A' " + "    AND cpv.ic_programa_fondojr = ? " + "    AND x.ig_prestamo IS NULL " + //La comparaci�n se puede hacer con cualquier campo no nulo de la tabla x
						//						" SELECT /*+ index(cpv IN_COM_PAGOS_FIDE_VAL_01_NUK)*/ "+
						//						"	cpv.cg_estatus estatus, cpv.fg_totalvencimiento total, " +
						//						" cpv.df_registro_fide fec_mov " +
						//						" FROM com_pagos_fide_val cpv " +
						//						" WHERE cpv.cg_estatus = 'RF' " +
						//						" AND NOT EXISTS ( " +
						//						" SELECT 1 " +
						//						" FROM com_pagos_fide_val " +
						//						" WHERE cg_estatus = 'RC' " +
						//						" AND com_fechaprobablepago = cpv.com_fechaprobablepago " +
						//						" AND ig_prestamo = cpv.ig_prestamo " +
						//						" AND ig_cliente = cpv.ig_cliente " +
						//						" AND ig_disposicion = cpv.ig_disposicion " +
						//						" AND fg_totalvencimiento = cpv.fg_totalvencimiento " +//FODEA 026 - 2010 ACF
						//						" AND cpv.ic_programa_fondojr = "+ clavePrograma + ")" +//FODEA 026 - 2010 ACF
						//						" AND cpv.cs_validacion = 'A' " +
						//						" AND cpv.ic_programa_fondojr = "+ clavePrograma +//FODEA 026 - 2010 ACF
						" UNION ALL " +
						//"        /*QUERY PARA TOTAL DE REEMB. POR FINIQ*/ " +
						" SELECT MAX(cpv.df_registro_fide) fec_mov " +
						" FROM com_pagos_fide_val cpv " + " WHERE cpv.cg_estatus = 'NP' and cpv.df_registro_fide > sysdate-200 " + " AND NOT EXISTS ( " + " SELECT 1 " +
						" FROM com_pagos_fide_val " + " WHERE (   cg_estatus = 'R' " + " OR cg_estatus = 'RF' " +
						" OR cg_estatus = 'RC' " + " ) " + " AND com_fechaprobablepago = cpv.com_fechaprobablepago " +
						" AND ig_prestamo = cpv.ig_prestamo " + " AND ig_cliente = cpv.ig_cliente " +
						" AND ig_disposicion = cpv.ig_disposicion " + " AND fg_totalvencimiento = cpv.fg_totalvencimiento " + //FODEA 026 - 2010 ACF
						"       AND cpv.ic_programa_fondojr = ? )" + //FODEA 026 - 2010 ACF
						" AND cpv.cs_validacion = 'A' " + "    AND cpv.ic_programa_fondojr = ? " + //FODEA 026 - 2010 ACF
						" UNION ALL " +
						//"        /*QUERY PARA TOTAL DE OTROS MOVIMIENTOS*/ " +
						" SELECT /*+ index(cof CP_COMCAT_OPER_FONDO_JR_PK) index(cmv IN_COM_MOV_GRAL_FDO_JR_01_NUK)*/ " +
						"       MAX(cmv.df_fec_movimiento) fec_mov " + " FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv " +
						" WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr " +
						" AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
						"    AND cof.ic_programa_fondojr = ? " + //FODEA 026 - 2010 ACF
						" AND UPPER(cof.cg_descripcion) != 'SALDO INICIAL') tablota ";

			log.debug("..:: qrySentence1: " + qrySentence);
			ps = con.queryPrecompilado(qrySentence);
			ps.setInt(1, iClavePrograma);
			ps.setInt(2, iClavePrograma);
			ps.setInt(3, iClavePrograma);
			ps.setInt(4, iClavePrograma);
			ps.setInt(5, iClavePrograma);
			rs = ps.executeQuery();
			if (rs.next()) {
				fec_ultimo_mov = rs.getString("fec_ultimo_mov") != null ? rs.getString("fec_ultimo_mov") : "";
			}
			rs.close();
			ps.close();

			//query para obtener totales por estatus y por movimiento
			qrySentence = /*QUERY PARA TOTAL DE PAGADOS, PREPAGOS, REEMBOLSOS, REC DE FONDO*/
				" SELECT /*+ full (cpv) */ " +
						  "	cpv.cg_estatus estatus, sum(cpv.fg_totalvencimiento) total  " + " FROM com_pagos_fide_val cpv " +
						  " WHERE cpv.cg_estatus in('P','T','RC','R') " + " AND cpv.cs_validacion = 'A'       " +
						  " AND cpv.ic_programa_fondojr = ? " + //FODEA 026 - 2010 ACF
						  " GROUP BY cpv.cg_estatus " + "UNION ALL " +
						  /*QUERY PARA TOTAL DE REEMB. POR FINIQ*/
						  //Se rescribe el query remplazando el not exists por un left join y una condicion de x.ig_prestamo IS NULL
						  "SELECT   /*+ index(cpv IN_COM_PAGOS_FIDE_VAL_07_NUK)*/ " +
						  "   cpv.cg_estatus estatus, SUM(cpv.fg_totalvencimiento) total " +
						  "FROM com_pagos_fide_val cpv LEFT JOIN com_pagos_fide_val x " + "   ON x.cg_estatus = 'RC' " +
						  "   AND x.com_fechaprobablepago = cpv.com_fechaprobablepago " + "   AND x.ig_prestamo = cpv.ig_prestamo " +
						  "   AND x.ig_cliente = cpv.ig_cliente " + "   AND x.ig_disposicion = cpv.ig_disposicion " +
						  "   AND x.fg_totalvencimiento = cpv.fg_totalvencimiento " +
						  "   AND x.ic_programa_fondojr = cpv.ic_programa_fondojr " + "WHERE cpv.cg_estatus = 'RF' " +
						  "   AND cpv.cs_validacion = 'A' " + "   AND cpv.ic_programa_fondojr = ? " + "   AND x.ig_prestamo IS NULL " +
						  "GROUP BY cpv.cg_estatus " +
						  //					" SELECT /*+ index(cpv IN_COM_PAGOS_FIDE_VAL_01_NUK)*/ "+
						
				//					"	cpv.cg_estatus estatus, sum(cpv.fg_totalvencimiento) total  " +
				//					" FROM com_pagos_fide_val cpv " +
				//					" WHERE cpv.cg_estatus = 'RF' " +
				//					" AND not exists(select 1 from com_pagos_fide_val where cg_estatus='RC' " +
				//					" and com_fechaprobablepago = cpv.com_fechaprobablepago " +
				//					" and ig_prestamo = cpv.ig_prestamo " +
				//					" and ig_cliente = cpv.ig_cliente " +
				//					" and ig_disposicion = cpv.ig_disposicion " +
				//					" and fg_totalvencimiento = cpv.fg_totalvencimiento " +//FODEA 026 - 2010 ACF
				//					" AND cpv.ic_programa_fondojr = "+ clavePrograma + ")"+//FODEA 026 - 2010 ACF
				//					" and cpv.cs_validacion = 'A'       " +
				//					" AND cpv.ic_programa_fondojr = "+ clavePrograma +//FODEA 026 - 2010 ACF
				//					" GROUP BY cpv.cg_estatus " +
				"UNION ALL " +
				/*QUERY PARA TOTAL DE REEMB. POR FINIQ*/
				" SELECT /*+ index(cpv IN_COM_PAGOS_FIDE_VAL_07_NUK)*/ " +
						  "	cpv.cg_estatus estatus, sum(cpv.fg_totalvencimiento) total  " + " FROM com_pagos_fide_val cpv " +
						  " where cpv.cg_estatus = 'NP' " +
						  /*
					"    AND not exists(select 1 from com_pagos_fide_val where (cg_estatus='R' OR cg_estatus='RF' OR cg_estatus='RC' ) " +
					"    					and com_fechaprobablepago = cpv.com_fechaprobablepago " +
					"                        and ig_prestamo = cpv.ig_prestamo " +
					"                        and ig_cliente = cpv.ig_cliente " +
					"                        and ig_disposicion = cpv.ig_disposicion " +
					"                        and fg_totalvencimiento = cpv.fg_totalvencimiento) " +
					*/" and cpv.cs_validacion = 'A'       " + " AND cpv.ic_programa_fondojr = ? " + //FODEA 026 - 2010 ACF
							 " GROUP BY cpv.cg_estatus " + "UNION ALL " +
							 /*QUERY PARA TOTAL DE OTROS MOVIMIENTOS*/
							 " SELECT /*+ index(cof CP_COMCAT_OPER_FONDO_JR_PK) index(cmv IN_COM_MOV_GRAL_FDO_JR_01_NUK)*/ " +
							 "	cof.cg_operacion estatus, SUM(cmv.fg_capital) total  " +
							 " FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv  " +
							 " WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr " +
							 " AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
							 " AND cof.ic_programa_fondojr = ? " + //FODEA 026 - 2010 ACF
							 " AND UPPER(cof.cg_descripcion) != 'SALDO INICIAL'  " + "GROUP BY cof.cg_operacion " +
							 "UNION ALL " +
							 /*QUERY PARA OBTENER EL SALDO INICIAL*/
							 " SELECT /*+ index(cof CP_COMCAT_OPER_FONDO_JR_PK) index(cmv IN_COM_MOV_GRAL_FDO_JR_01_NUK)*/ " +
							 "	DECODE(cof.cg_operacion,'Suma','INICIAL','INICIAL') estatus, SUM(cmv.fg_capital) total  " +
							 " FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv  " +
							 " WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr " +
							 " AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
							 " AND cof.ic_programa_fondojr = ? " + //FODEA 026 - 2010 ACF
							 " AND UPPER(cof.cg_descripcion) = 'SALDO INICIAL' " + " AND ROWNUM =1  " +
							 " GROUP BY cof.cg_operacion ";

			log.debug("..:: qrySentence2: " + qrySentence);

			ps = con.queryPrecompilado(qrySentence);
			ps.setInt(1, iClavePrograma);
			ps.setInt(2, iClavePrograma);
			ps.setInt(3, iClavePrograma);
			ps.setInt(4, iClavePrograma);
			ps.setInt(5, iClavePrograma);
			rs = ps.executeQuery();
			while (rs.next()) {
				List lstRegMontos = new ArrayList();
				lstRegMontos.add(rs.getString("estatus"));
				lstRegMontos.add(rs.getString("total"));
				lstMontosGral.add(lstRegMontos);
			}
			rs.close();
			ps.close();

			if (lstMontosGral != null && lstMontosGral.size() > 0) {
				for (int x = 0; x < lstMontosGral.size(); x++) {
					List lstFondoConcentradoDet = (List) lstMontosGral.get(x);
					if (lstFondoConcentradoDet != null) {
						if (lstFondoConcentradoDet.get(0) != null &&
							 ((String) lstFondoConcentradoDet.get(0)).equals("INICIAL")) {
							montoIni = montoIni.add(new BigDecimal((String) lstFondoConcentradoDet.get(1)));
						}
						if (lstFondoConcentradoDet.get(0) != null && ((String) lstFondoConcentradoDet.get(0)).equals("P")) {
							montoP = montoP.add(new BigDecimal((String) lstFondoConcentradoDet.get(1)));
						}
						if (lstFondoConcentradoDet.get(0) != null && ((String) lstFondoConcentradoDet.get(0)).equals("NP")) {
							montoNP = montoNP.add(new BigDecimal((String) lstFondoConcentradoDet.get(1)));
						}
						if (lstFondoConcentradoDet.get(0) != null && ((String) lstFondoConcentradoDet.get(0)).equals("T")) {
							montoT = montoT.add(new BigDecimal((String) lstFondoConcentradoDet.get(1)));
						}
						if (lstFondoConcentradoDet.get(0) != null && ((String) lstFondoConcentradoDet.get(0)).equals("R")) {
							montoR = montoR.add(new BigDecimal((String) lstFondoConcentradoDet.get(1)));
						}
						if (lstFondoConcentradoDet.get(0) != null && ((String) lstFondoConcentradoDet.get(0)).equals("RF")) {
							montoRF = montoRF.add(new BigDecimal((String) lstFondoConcentradoDet.get(1)));
						}
						if (lstFondoConcentradoDet.get(0) != null && ((String) lstFondoConcentradoDet.get(0)).equals("RC")) {
							montoRC = montoRC.add(new BigDecimal((String) lstFondoConcentradoDet.get(1)));
						}
						if (lstFondoConcentradoDet.get(0) != null &&
							 (((String) lstFondoConcentradoDet.get(0)).toUpperCase()).equals("SUMA")) {
							montoSUM = montoSUM.add(new BigDecimal((String) lstFondoConcentradoDet.get(1)));
						}
						if (lstFondoConcentradoDet.get(0) != null &&
							 (((String) lstFondoConcentradoDet.get(0)).toUpperCase()).equals("RESTA")) {
							montoRES = montoRES.add(new BigDecimal((String) lstFondoConcentradoDet.get(1)));
						}
					} //cierre if interno
				} //cierre for interno
				/*0*/lstMontosConcentrado.add(montoP); /*1*/
				lstMontosConcentrado.add(montoNP);
				/*2*/lstMontosConcentrado.add(montoT); /*3*/
				lstMontosConcentrado.add(montoR);
				/*4*/lstMontosConcentrado.add(montoRF); /*5*/
				lstMontosConcentrado.add(montoRC);
				/*6*/lstMontosConcentrado.add(montoSUM); /*7*/
				lstMontosConcentrado.add(montoRES);
				/*8*/lstMontosConcentrado.add(montoIni); /*9*/
				lstMontosConcentrado.add(fec_ultimo_mov);
			} //cierre if principal (lstMontosGral)
			log.debug("montoSUM>>>>>" + montoSUM.toPlainString());
			log.debug("montoRES>>>>>" + montoRES.toPlainString());


			return lstMontosConcentrado;
		} catch (Exception e) {
			log.error("consConciliacionConcentrado(ERROR)", e);
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consConciliacionConcentrado(S)");
		}
	} //cierre metodo

	/**
	 * Obtiene el detalle de los moovimientos que conforman el FONDO CONCENTRADOR
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param form
	 */
	public List consConciliacionDetalle(FondoJrConciliacionBean form) throws NafinException {
		log.info("consConciliacionDetalle(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer qrySentence = new StringBuffer();
		String qrySaldoIni = "";
		List lstConciliaDetGral = new ArrayList();
		String Where_And = " WHERE ";
		int param = 0;

		try {
			con.conexionDB();
			qrySaldoIni =
						" SELECT  cmv.fg_capital saldo, DECODE(cof.cg_operacion,'Suma','INICIAL','INICIAL') estatus, " +
						"		cof.cg_descripcion observaciones, cmv.CG_NOUSUARIO usuario  " +
						"   FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv  " +
						"   WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr " + "   AND UPPER(cof.cg_descripcion) = 'SALDO INICIAL' " +
						" 	AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
						"   AND cof.ic_programa_fondojr = " + form.getClavePrograma() + //FODEA 026 - 2010 ACF
						"   AND ROWNUM = 1 ";
			log.debug("..:: qrySaldoIni: " + qrySaldoIni);
			rs = con.queryDB(qrySaldoIni);
			if (rs != null && rs.next()) {
				List lstConciliaDetReg = new ArrayList();
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add(rs.getString("saldo") != null ? rs.getString("saldo") : "");
				lstConciliaDetReg.add(rs.getString("estatus") != null ? rs.getString("estatus") : "");
				lstConciliaDetReg.add(rs.getString("observaciones") != null ? rs.getString("observaciones") : "");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add(rs.getString("usuario") != null ? rs.getString("usuario") : "");
				lstConciliaDetReg.add("");

				lstConciliaDetGral.add(lstConciliaDetReg);
			} else {
				List lstConciliaDetReg = new ArrayList();
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("0.00");
				lstConciliaDetReg.add("INICIAL");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("");
				lstConciliaDetReg.add("");

				lstConciliaDetGral.add(lstConciliaDetReg);
			}
			if (rs != null)
				rs.close();

			qrySentence.append(" SELECT  tablota.fec_venc fec_venc,   " + "         	tablota.fec_fide fec_fide,   " +
									 "		 	 	tablota.amort amort, tablota.interes interes, tablota.saldo saldo,   " +
									 "         	tablota.estatus estatus, tablota.observaciones observaciones,   " +
									 "         	tablota.fec_val fec_val, tablota.usuario usuario,   " +
									 "         	tablota.prestamo prestamo, tablota.num_cliente num_cliente,   " +
									 "         	tablota.fec_cliente fec_cliente, tablota.num_cuota num_cuota,   " +
									 "         	tablota.monto_reembolso,    " + "	 	    	tablota.ic_programa_fondojr, " + //FODEA 026 - 2010 ACF
									 "			  	tablota.DIAS_TRANSCURRIDOS_VENCIMIENTO,  " + "    			tablota.dias_limite " + "    FROM ( " +
									
									 " SELECT  tablota_tmp.fec_venc fec_venc,   " + "         tablota_tmp.fec_fide fec_fide,   " +
									 "		 	 tablota_tmp.amort amort, tablota_tmp.interes interes, tablota_tmp.saldo saldo,   " +
									 "         tablota_tmp.estatus estatus, tablota_tmp.observaciones observaciones,   " +
									 "         tablota_tmp.fec_val fec_val, tablota_tmp.usuario usuario,   " +
									 "         tablota_tmp.prestamo prestamo, tablota_tmp.num_cliente num_cliente,   " +
									 "         tablota_tmp.fec_cliente fec_cliente, tablota_tmp.num_cuota num_cuota,   " +
									 "         tablota_tmp.monto_reembolso,    " + "	 	    tablota_tmp.ic_programa_fondojr, " + //FODEA 026 - 2010 ACF
									 "			(  " + "      	SELECT  " + "         	DECODE(   " + "					tablota_tmp.estatus,   " + "					'NP',  " +
									 "					DECODE(    " + "						(  " + "							SELECT /*+ index(CPV2 CP_COM_PAGOS_FIDE_VAL_PK)*/  " +
									 "								COUNT(1)  " + "							FROM     " + "								COM_PAGOS_FIDE_VAL CPV2  " + "							WHERE    " +
									 "								CPV2.COM_FECHAPROBABLEPAGO     = to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')  " +
									 "								AND CPV2.IG_PRESTAMO           = tablota_tmp.prestamo  " +
									 "								AND CPV2.IG_DISPOSICION        = tablota_tmp.num_cuota  " +
									 "								AND CPV2.FG_TOTALVENCIMIENTO   = tablota_tmp.saldo   " +
									 "								AND CPV2.ic_programa_fondojr = tablota_tmp.ic_programa_fondojr ");

			if (!form.getDiasHasta().equals("") && !form.getDiasHasta().equals("")) { //Fodea 027-2011
						qrySentence.append(" AND CPV2.CG_ESTATUS IN ('RC','RF')  ");
			} else {
				qrySentence.append(" AND CPV2.CG_ESTATUS IN ('R','RC','RF')  ");
			}
			qrySentence.append("					),  " + "						0,  " + "						(  " + "							CASE  WHEN   " +
									 "								to_date(tablota_tmp.fec_venc,'dd/mm/yyyy') < trunc(sysdate+1)   " + "							then   " +
									 "								(trunc(sysdate) - to_date(tablota_tmp.fec_venc,'dd/mm/yyyy'))   " + "							else   " +
									 "								to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')-to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')   " +
									 "							end  " + "						),0  " + "             	 " + "					)  " + "         		,NULL  " + "				)   " +
									 "      	FROM  " + "         	COM_CRED_PARAM_DIAS_VENC CCP  " + "      	WHERE  " +
									 "				CCP.IC_PROGRAMA_FONDOJR 	=	tablota_tmp.ic_programa_fondojr 		AND 	 " +
									 "         	to_date(tablota_tmp.fec_venc,'dd/mm/yyyy') 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) 	AND 	 " +
									 "      		to_date(tablota_tmp.fec_venc,'dd/mm/yyyy') 	< 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) 		 " +
									 "   	) AS DIAS_TRANSCURRIDOS_VENCIMIENTO,  " + "    	(  " + "       	SELECT   " +
									 "         	CRED_PARAM.IG_DIAS_VENCIMIENTO   " + "       	FROM   " +
									 "         	COM_CRED_PARAM_DIAS_VENC CRED_PARAM  " + "       	WHERE   " +
									 "				CRED_PARAM.IC_PROGRAMA_FONDOJR 	=	tablota_tmp.ic_programa_fondojr					AND 	 " +
									 "         	to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')  		>= TRUNC(CRED_PARAM.DF_FECHA_VMTO_INI) 	AND  	 " +
									 "         	to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')			< 	TRUNC(CRED_PARAM.DF_FECHA_VMTO_FIN+1) 			 " +
									 "    	) AS dias_limite " + "    FROM (SELECT /*+ index(cpv IN_COM_PAGOS_FIDE_VAL_01_NUK)*/ " +
									 "    			TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,   " +
									 "                 TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,   " +
									 "                 cpv.fg_amortizacion amort, cpv.fg_interes interes,   " +
									 "                 cpv.fg_totalvencimiento saldo, cpv.cg_estatus estatus,   " +
									 "                 cpv.cg_observaciones observaciones,   " +
									 "                 TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val, cpv.cg_usuario usuario,   " +
									 "                 cpv.ig_prestamo prestamo, cpv.IG_CLIENTE num_cliente,   " +
									 "                 TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente, cpv.IG_DISPOSICION num_cuota,  " +
									 "                 TO_NUMBER (NULL) monto_reembolso   " + ", 	cpv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "            FROM com_pagos_fide_val cpv   " + "           WHERE (   cpv.cg_estatus = 'P'   " +
									 "                  OR cpv.cg_estatus = 'T'   " +
									 "                  OR cpv.cg_estatus = 'RC'   ");
			if (form.getDiasHasta().equals("") && form.getDiasHasta().equals("")) { //Fodea 027-2011
						qrySentence.append("   OR cpv.cg_estatus = 'R'   ");
			}
			qrySentence.append("        )   " + "             AND cpv.cs_validacion = 'A'   " + "          UNION ALL   " +
									 "          SELECT /*+ use_nl(cpv rxf) index(cpv IN_COM_PAGOS_FIDE_VAL_01_NUK) index(rxf CP_COM_REEMB_FINIQUITO_NUK)*/ " +
									 "          	   distinct TO_CHAR(rxf.df_reembolso, 'dd/mm/yyyy') fec_venc,   " +
									 "               null fec_fide,           " +
									 "               sum(cpv.fg_amortizacion) amort, sum(cpv.fg_interes) interes,  " +
									 "               sum(cpv.fg_totalvencimiento) saldo,   " + "               cpv.cg_estatus estatus,   " +
									 "               cpv.cg_observaciones observaciones,   " +
									 "               null fec_val, cpv.cg_usuario usuario,   " +
									 "               cpv.ig_prestamo prestamo, cpv.ig_cliente num_cliente,   " +
									 "               null fec_cliente, TO_NUMBER (NULL) num_cuota,  " +
									 "               rxf.fn_monto_reembolso monto_reembolso  " + ", 	cpv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "            FROM com_pagos_fide_val cpv, com_reembolsos_finiquito rxf  " +
									 "           WHERE cpv.ig_prestamo = rxf.ig_prestamo  " + "             AND cpv.cg_estatus = 'RF'   " +
									 "             AND cpv.cs_validacion = 'A'   " + "             AND NOT EXISTS (   " +
									 "                    SELECT 1   " + "                      FROM com_pagos_fide_val   " +
									 "                     WHERE cg_estatus = 'RC'   " +
									 "                       AND com_fechaprobablepago = cpv.com_fechaprobablepago   " +
									 "                       AND ig_prestamo = cpv.ig_prestamo   " +
									 "                       AND ig_cliente = cpv.ig_cliente   " +
									 "                       AND ig_disposicion = cpv.ig_disposicion   " +
									 "                       AND fg_totalvencimiento = cpv.fg_totalvencimiento)  " +
									 "            GROUP BY rxf.df_reembolso, rxf.fn_monto_reembolso , cpv.cg_estatus ,   " +
									 "       				cpv.cg_usuario, cpv.ig_prestamo, cpv.ig_cliente, cpv.cg_observaciones   " +
									 ", 	cpv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "          UNION ALL   " + "          SELECT /*+ index(cpv IN_COM_PAGOS_FIDE_VAL_01_NUK)*/ " +
									 "          		 TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,   " +
									 "                 TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,   " +
									 "                 cpv.fg_amortizacion amort, cpv.fg_interes interes,   " +
									 "                 cpv.fg_totalvencimiento saldo, cpv.cg_estatus estatus,   " +
									 "                 cpv.cg_observaciones observaciones,   " +
									 "                 TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val, cpv.cg_usuario usuario,   " +
									 "                 cpv.ig_prestamo prestamo, cpv.IG_CLIENTE num_cliente,   " +
									 "                 TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente, cpv.IG_DISPOSICION num_cuota,  " +
									 "                 TO_NUMBER (NULL) monto_reembolso   " + ", 	cpv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "            FROM com_pagos_fide_val cpv   " + "           WHERE cpv.cg_estatus = 'NP'   " +
									 "             AND cpv.cs_validacion = 'A'   " + "          UNION ALL   " +
									 "            SELECT /*+ index(cof CP_COMCAT_OPER_FONDO_JR_PK) index(cmv IN_COM_MOV_GRAL_FDO_JR_01_NUK)*/  " +
									 "            	  TO_CHAR (cmv.df_fec_movimiento, 'dd/mm/yyyy') AS fec_venc,  " +
									 "                   NULL AS fec_fide, TO_NUMBER (NULL) AS amort,  " +
									 "                   TO_NUMBER (NULL) AS interes, cmv.fg_capital AS saldo,  " +
									 "                   cof.cg_operacion AS estatus, cof.cg_descripcion AS observaciones,  " +
									 "                   NULL AS fec_val, cmv.cg_nousuario AS usuario,  " +
									 "                   TO_NUMBER (NULL) AS prestamo, TO_NUMBER (NULL) AS num_cliente,  " +
									 "                   NULL AS fec_cliente, TO_NUMBER (NULL) AS num_cuota,  " +
									 "                   TO_NUMBER (NULL) monto_reembolso   " + ", 	cof.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "              FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv  " +
									 "             WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr  " +
									 " 							AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "               AND UPPER (cof.cg_descripcion) != 'SALDO INICIAL') tablota_tmp " +
									 "      ) tablota "); //F012-2011


			/*qrySentence.append( "SELECT   tablota.fec_venc fec_venc,   " +
									"         tablota.fec_fide fec_fide,   " +
									"		 tablota.amort amort, tablota.interes interes, tablota.saldo saldo,   " +
									"         tablota.estatus estatus, tablota.observaciones observaciones,   " +
									"         tablota.fec_val fec_val, tablota.usuario usuario,   " +
									"         tablota.prestamo prestamo, tablota.num_cliente num_cliente,   " +
									"         tablota.fec_cliente fec_cliente, tablota.num_cuota num_cuota,   " +
									"         tablota.monto_reembolso    " +
									", 	      tablota.ic_programa_fondojr "+//FODEA 026 - 2010 ACF
									"    FROM VM_DET_CONCILIACION_FJR tablota ");*/

			if (form.getFecVencIni() != null && !form.getFecVencIni().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_venc,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				Where_And = " AND ";
			}
			if (form.getFecVencFin() != null && !form.getFecVencFin().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_venc,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
				Where_And = " AND ";
			}
			if (form.getFecOperIni() != null && !form.getFecOperIni().equals("")) {
				qrySentence.append(Where_And + " to_date(tablota.fec_val,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				Where_And = " AND ";
			}
			if (form.getFecOperFin() != null && !form.getFecOperFin().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_val,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
				Where_And = " AND ";
			}
			if (form.getFecFideIni() != null && !form.getFecFideIni().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_fide,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				Where_And = " AND ";
			}
			if (form.getFecFideFin() != null && !form.getFecFideFin().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_fide,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
				Where_And = " AND ";
			}
			//FODEA 026 - 2010 ACF (I)
			if (form.getClavePrograma() != null && !form.getClavePrograma().equals("")) {
				qrySentence.append(Where_And + " tablota.ic_programa_fondojr = ? ");
				Where_And = " AND ";
			}
			//FODEA 026 - 2010 ACF (F)
			//qrySentence.append(" order by tablota.fec_venc ");

			//F012-2011
			if (form.getDiasDesde() != null && !form.getDiasDesde().equals("")) {
				qrySentence.append(Where_And + " tablota.DIAS_TRANSCURRIDOS_VENCIMIENTO >= ? ");
				Where_And = " AND ";
			}
			if (form.getDiasHasta() != null && !form.getDiasHasta().equals("")) {
				qrySentence.append(Where_And + " tablota.DIAS_TRANSCURRIDOS_VENCIMIENTO <= ? ");
				Where_And = " AND ";
			}

			if ((form.getDiasDesde() != null && !form.getDiasDesde().equals("")) ||
				 (form.getDiasHasta() != null && !form.getDiasHasta().equals(""))) {
				qrySentence.append(Where_And + " tablota.estatus = 'NP' ");
				Where_And = " AND ";
			}

			log.debug("qrySentence>>>>>>>" + qrySentence + "\n" + form.getFecVencIni() + "-----" + form.getFecVencFin());
			ps = con.queryPrecompilado(qrySentence.toString());
			if (form.getFecVencIni() != null && !form.getFecVencIni().equals("")) {
				param++;
				ps.setString(param, form.getFecVencIni());
			}
			if (form.getFecVencFin() != null && !form.getFecVencFin().equals("")) {
				param++;
				ps.setString(param, form.getFecVencFin());
			}
			if (form.getFecOperIni() != null && !form.getFecOperIni().equals("")) {
				param++;
				ps.setString(param, form.getFecOperIni());
			}
			if (form.getFecOperFin() != null && !form.getFecOperFin().equals("")) {
				param++;
				ps.setString(param, form.getFecOperFin());
			}
			if (form.getFecFideIni() != null && !form.getFecFideIni().equals("")) {
				param++;
				ps.setString(param, form.getFecFideIni());
			}
			if (form.getFecFideFin() != null && !form.getFecFideFin().equals("")) {
				param++;
				ps.setString(param, form.getFecFideFin());
			}
			if (form.getClavePrograma() != null && !form.getClavePrograma().equals("")) {
				param++;
				ps.setInt(param, Integer.parseInt(form.getClavePrograma()));
			} //FODEA 026 - 2010 ACF
			if (form.getDiasDesde() != null && !form.getDiasDesde().equals("")) {
				param++;
				ps.setString(param, form.getDiasDesde());
			}
			if (form.getDiasHasta() != null && !form.getDiasHasta().equals("")) {
				param++;
				ps.setString(param, form.getDiasHasta());
			}

			rs = ps.executeQuery();

			if (rs != null) {
				while (rs.next()) {
					List lstConciliaDetReg = new ArrayList();
					lstConciliaDetReg.add(rs.getString("fec_venc") != null ? rs.getString("fec_venc") : "");
					lstConciliaDetReg.add(rs.getString("fec_fide") != null ? rs.getString("fec_fide") : "");
					lstConciliaDetReg.add(rs.getString("amort") != null ? rs.getString("amort") : "");
					lstConciliaDetReg.add(rs.getString("interes") != null ? rs.getString("interes") : "");
					lstConciliaDetReg.add(rs.getString("saldo") != null ? rs.getString("saldo") : "");
					lstConciliaDetReg.add(rs.getString("estatus") != null ? rs.getString("estatus") : "");
					lstConciliaDetReg.add(rs.getString("observaciones") != null ? rs.getString("observaciones") : "");
					lstConciliaDetReg.add(rs.getString("fec_val") != null ? rs.getString("fec_val") : "");
					lstConciliaDetReg.add(rs.getString("usuario") != null ? rs.getString("usuario") : "");
					lstConciliaDetReg.add(rs.getString("prestamo") != null ? rs.getString("prestamo") : "");
					lstConciliaDetReg.add(rs.getString("monto_reembolso") != null ? rs.getString("monto_reembolso") : "");
					lstConciliaDetReg.add(rs.getString("DIAS_TRANSCURRIDOS_VENCIMIENTO") != null ?
												 rs.getString("DIAS_TRANSCURRIDOS_VENCIMIENTO") : "");

					lstConciliaDetGral.add(lstConciliaDetReg);
				}
			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			return lstConciliaDetGral;
		} catch (Exception e) {
			log.info("consConciliacionDetalle(ERROR) ::..");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consConciliacionDetalle(S)");
		}
	} //cierre metodo


	/**
	 * Obtiene el detalle de los moovimientos que conforman el FONDO CONCENTRADOR
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param form
	 */
	private ResultSet consConciliacionDetalle(FondoJrConciliacionBean form, AccesoDB con, ResultSet rs,
															PreparedStatement ps) throws NafinException {
		log.info("consConciliacionDetalle_DB(E)");
		StringBuffer qrySentence = new StringBuffer();
		String Where_And = " WHERE ";
		int param = 0;
		try {
			qrySentence.append("SELECT tablota.fec_venc fec_venc,   " + "         tablota.fec_fide fec_fide,   " +
									 "		 	 tablota.amort amort, tablota.interes interes, tablota.saldo saldo,   " +
									 "         tablota.estatus estatus, tablota.observaciones observaciones,   " +
									 "         tablota.fec_val fec_val, tablota.usuario usuario,   " +
									 "         tablota.prestamo prestamo, tablota.num_cliente num_cliente,   " +
									 "         tablota.fec_cliente fec_cliente, tablota.num_cuota num_cuota,   " +
									 "         tablota.monto_reembolso,    " + " 	       tablota.ic_programa_fondojr, " + //FODEA 026 - 2010 ACF
									 "			 tablota.dias_transcurridos_vencimiento,  " + "    		 tablota.dias_limite " + "    FROM ( " +
									 "SELECT   tablota_tmp.fec_venc fec_venc,   " + "         tablota_tmp.fec_fide fec_fide,   " +
									 "		 	 tablota_tmp.amort amort, tablota_tmp.interes interes, tablota_tmp.saldo saldo,   " +
									 "         tablota_tmp.estatus estatus, tablota_tmp.observaciones observaciones,   " +
									 "         tablota_tmp.fec_val fec_val, tablota_tmp.usuario usuario,   " +
									 "         tablota_tmp.prestamo prestamo, tablota_tmp.num_cliente num_cliente,   " +
									 "         tablota_tmp.fec_cliente fec_cliente, tablota_tmp.num_cuota num_cuota,   " +
									 "         tablota_tmp.monto_reembolso,    " + " 	      tablota_tmp.ic_programa_fondojr, " + //FODEA 026 - 2010 ACF
									 "			(  " + "      	SELECT  " + "         	DECODE(   " + "					tablota_tmp.estatus,   " + "					'NP',  " +
									 "					DECODE(    " + "						(  " + "							SELECT /*+ index(cpv2 CP_COM_PAGOS_FIDE_VAL_PK)*/ " +
									 "								COUNT(1)  " + "							FROM     " + "								COM_PAGOS_FIDE_VAL CPV2  " + "							WHERE    " +
									 "								CPV2.COM_FECHAPROBABLEPAGO     = to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')  " +
									 "								AND CPV2.IG_PRESTAMO           = tablota_tmp.prestamo  " +
									 "								AND CPV2.IG_DISPOSICION        = tablota_tmp.num_cuota  " +
									 "								AND CPV2.FG_TOTALVENCIMIENTO   = tablota_tmp.saldo   " +
									 "								AND CPV2.ic_programa_fondojr = tablota_tmp.ic_programa_fondojr ");
			if (form.getDiasHasta() != null) { //Fodea 027-2011
						qrySentence.append("		AND CPV2.CG_ESTATUS IN ('RC','RF')  ");
			} else {
				qrySentence.append("	AND CPV2.CG_ESTATUS IN ('R','RC','RF')  ");
			}
			qrySentence.append("						),  " + "						0,  " + "						(  " + "							CASE  WHEN   " +
									 "								to_date(tablota_tmp.fec_venc,'dd/mm/yyyy') < trunc(sysdate+1)   " + "							then   " +
									 "								(trunc(sysdate) - to_date(tablota_tmp.fec_venc,'dd/mm/yyyy'))   " + "							else   " +
									 "								to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')-to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')   " +
									 "							end  " + "						),0  " + "             	 " + "					)  " + "         		,NULL  " + "				)   " +
									 "      	FROM  " + "         	COM_CRED_PARAM_DIAS_VENC CCP  " + "      	WHERE  " +
									 "				CCP.IC_PROGRAMA_FONDOJR 	=	tablota_tmp.ic_programa_fondojr 		AND 	 " +
									 "         	to_date(tablota_tmp.fec_venc,'dd/mm/yyyy') 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) 	AND 	 " +
									 "      		to_date(tablota_tmp.fec_venc,'dd/mm/yyyy') 	< 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) 		 " +
									 "   	) AS dias_transcurridos_vencimiento,  " + "    	(  " + "       	SELECT   " +
									 "         	CRED_PARAM.IG_DIAS_VENCIMIENTO   " + "       	FROM   " +
									 "         	COM_CRED_PARAM_DIAS_VENC CRED_PARAM  " + "       	WHERE   " +
									 "				CRED_PARAM.IC_PROGRAMA_FONDOJR 	=	tablota_tmp.ic_programa_fondojr					AND 	 " +
									 "         	to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')  		>= TRUNC(CRED_PARAM.DF_FECHA_VMTO_INI) 	AND  	 " +
									 "         	to_date(tablota_tmp.fec_venc,'dd/mm/yyyy')			< 	TRUNC(CRED_PARAM.DF_FECHA_VMTO_FIN+1) 			 " +
									 "    	) AS dias_limite " + "    FROM (SELECT /*+ index(cpv IN_COM_PAGOS_FIDE_VAL_01_NUK)*/ " +
									 "    			TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,   " +
									 "                 TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,   " +
									 "                 cpv.fg_amortizacion amort, cpv.fg_interes interes,   " +
									 "                 cpv.fg_totalvencimiento saldo, cpv.cg_estatus estatus,   " +
									 "                 cpv.cg_observaciones observaciones,   " +
									 "                 TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val, cpv.cg_usuario usuario,   " +
									 "                 cpv.ig_prestamo prestamo, cpv.IG_CLIENTE num_cliente,   " +
									 "                 TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente, cpv.IG_DISPOSICION num_cuota,  " +
									 "                 TO_NUMBER (NULL) monto_reembolso   " + ", 	cpv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "            FROM com_pagos_fide_val cpv   " + "           WHERE (   cpv.cg_estatus = 'P'   " +
									 "                  OR cpv.cg_estatus = 'T'   " +
									 "                  OR cpv.cg_estatus = 'RC'   ");
			if (form.getDiasHasta() == null) { //Fodea 027-2011
						qrySentence.append("  OR cpv.cg_estatus = 'R'   ");
			}
			qrySentence.append("             )   " + "             AND cpv.cs_validacion = 'A'   " + "          UNION ALL   " +
									 "          SELECT /*+ use_nl(cpv rxf) index(cpv IN_COM_PAGOS_FIDE_VAL_01_NUK) index(rxf CP_COM_REEMB_FINIQUITO_NUK)*/ " +
									 "          	   distinct TO_CHAR(rxf.df_reembolso, 'dd/mm/yyyy') fec_venc,   " +
									 "               null fec_fide,           " +
									 "               sum(cpv.fg_amortizacion) amort, sum(cpv.fg_interes) interes,  " +
									 "               sum(cpv.fg_totalvencimiento) saldo,   " + "               cpv.cg_estatus estatus,   " +
									 "               cpv.cg_observaciones observaciones,   " +
									 "               null fec_val, cpv.cg_usuario usuario,   " +
									 "               cpv.ig_prestamo prestamo, cpv.ig_cliente num_cliente,   " +
									 "               null fec_cliente, TO_NUMBER (NULL) num_cuota,  " +
									 "               rxf.fn_monto_reembolso monto_reembolso  " + ", 	cpv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "            FROM com_pagos_fide_val cpv, com_reembolsos_finiquito rxf  " +
									 "           WHERE cpv.ig_prestamo = rxf.ig_prestamo  " + "             AND cpv.cg_estatus = 'RF'   " +
									 "             AND cpv.cs_validacion = 'A'   " + "             AND NOT EXISTS (   " +
									 "                    SELECT 1   " + "                      FROM com_pagos_fide_val   " +
									 "                     WHERE cg_estatus = 'RC'   " +
									 "                       AND com_fechaprobablepago = cpv.com_fechaprobablepago   " +
									 "                       AND ig_prestamo = cpv.ig_prestamo   " +
									 "                       AND ig_cliente = cpv.ig_cliente   " +
									 "                       AND ig_disposicion = cpv.ig_disposicion   " +
									 "                       AND fg_totalvencimiento = cpv.fg_totalvencimiento)  " +
									 "            GROUP BY rxf.df_reembolso, rxf.fn_monto_reembolso , cpv.cg_estatus ,   " +
									 "       				cpv.cg_usuario, cpv.ig_prestamo, cpv.ig_cliente, cpv.cg_observaciones   " +
									 ", 	cpv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "          UNION ALL   " + "          SELECT /*+ index(cpv IN_COM_PAGOS_FIDE_VAL_01_NUK)*/ " +
									 "          		 TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,   " +
									 "                 TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,   " +
									 "                 cpv.fg_amortizacion amort, cpv.fg_interes interes,   " +
									 "                 cpv.fg_totalvencimiento saldo, cpv.cg_estatus estatus,   " +
									 "                 cpv.cg_observaciones observaciones,   " +
									 "                 TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val, cpv.cg_usuario usuario,   " +
									 "                 cpv.ig_prestamo prestamo, cpv.IG_CLIENTE num_cliente,   " +
									 "                 TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente, cpv.IG_DISPOSICION num_cuota,  " +
									 "                 TO_NUMBER (NULL) monto_reembolso   " + ", 	cpv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "            FROM com_pagos_fide_val cpv   " + "           WHERE cpv.cg_estatus = 'NP'   " +
									 "             AND cpv.cs_validacion = 'A'   " + "          UNION ALL   " +
									 "            SELECT /*+ index(cof CP_COMCAT_OPER_FONDO_JR_PK) index(cmv IN_COM_MOV_GRAL_FDO_JR_01_NUK)*/  " +
									 "            	  TO_CHAR (cmv.df_fec_movimiento, 'dd/mm/yyyy') AS fec_venc,  " +
									 "                   NULL AS fec_fide, TO_NUMBER (NULL) AS amort,  " +
									 "                   TO_NUMBER (NULL) AS interes, cmv.fg_capital AS saldo,  " +
									 "                   cof.cg_operacion AS estatus, cof.cg_descripcion AS observaciones,  " +
									 "                   NULL AS fec_val, cmv.cg_nousuario AS usuario,  " +
									 "                   TO_NUMBER (NULL) AS prestamo, TO_NUMBER (NULL) AS num_cliente,  " +
									 "                   NULL AS fec_cliente, TO_NUMBER (NULL) AS num_cuota,  " +
									 "                   TO_NUMBER (NULL) monto_reembolso   " + ", 	cof.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "              FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv  " +
									 "             WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr  " +
									 " 							AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr " + //FODEA 026 - 2010 ACF
									 "               AND UPPER (cof.cg_descripcion) != 'SALDO INICIAL') tablota_tmp " +
									 "               )tablota ");


			if (form.getFecVencIni() != null && !form.getFecVencIni().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_venc,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				Where_And = " AND ";
			}
			if (form.getFecVencFin() != null && !form.getFecVencFin().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_venc,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
				Where_And = " AND ";
			}
			if (form.getFecOperIni() != null && !form.getFecOperIni().equals("")) {
				qrySentence.append(Where_And + " to_date(tablota.fec_val,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				Where_And = " AND ";
			}
			if (form.getFecOperFin() != null && !form.getFecOperFin().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_val,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
				Where_And = " AND ";
			}
			if (form.getFecFideIni() != null && !form.getFecFideIni().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_fide,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				Where_And = " AND ";
			}
			if (form.getFecFideFin() != null && !form.getFecFideFin().equals("")) {
				qrySentence.append(Where_And +
										 " to_date(tablota.fec_fide,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
				Where_And = " AND ";
			}
			//FODEA 026 - 2010 ACF (I)
			if (form.getClavePrograma() != null && !form.getClavePrograma().equals("")) {
				System.out.println("form.getClavePrograma()=== " + form.getClavePrograma());
				qrySentence.append(Where_And + " tablota.ic_programa_fondojr = ? ");
				Where_And = " AND ";
			}
			//FODEA 026 - 2010 ACF (F)
			//qrySentence.append(" order by tablota.fec_venc ");
			//F012-2011 FVR
			if (form.getDiasDesde() != null && !form.getDiasDesde().equals("")) {
				qrySentence.append(Where_And + " tablota.DIAS_TRANSCURRIDOS_VENCIMIENTO >= ? ");
				Where_And = " AND ";
			}
			if (form.getDiasHasta() != null && !form.getDiasHasta().equals("")) {
				qrySentence.append(Where_And + " tablota.DIAS_TRANSCURRIDOS_VENCIMIENTO <= ? ");
				Where_And = " AND ";
			}

			if ((form.getDiasDesde() != null && !form.getDiasDesde().equals("")) ||
				 (form.getDiasHasta() != null && !form.getDiasHasta().equals(""))) {
				qrySentence.append(Where_And + " tablota.estatus = 'NP' ");
				Where_And = " AND ";
			}


			log.debug("qrySentence>>>>>>>" + qrySentence);
			ps = con.queryPrecompilado(qrySentence.toString());
			if (form.getFecVencIni() != null && !form.getFecVencIni().equals("")) {
				param++;
				ps.setString(param, form.getFecVencIni());
			}
			if (form.getFecVencFin() != null && !form.getFecVencFin().equals("")) {
				param++;
				ps.setString(param, form.getFecVencFin());
			}
			if (form.getFecOperIni() != null && !form.getFecOperIni().equals("")) {
				param++;
				ps.setString(param, form.getFecOperIni());
			}
			if (form.getFecOperFin() != null && !form.getFecOperFin().equals("")) {
				param++;
				ps.setString(param, form.getFecOperFin());
			}
			if (form.getFecFideIni() != null && !form.getFecFideIni().equals("")) {
				param++;
				ps.setString(param, form.getFecFideIni());
			}
			if (form.getFecFideFin() != null && !form.getFecFideFin().equals("")) {
				param++;
				ps.setString(param, form.getFecFideFin());
			}
			if (form.getClavePrograma() != null && !form.getClavePrograma().equals("")) {
				param++;
				ps.setInt(param, Integer.parseInt(form.getClavePrograma()));
			} //FODEA 026 - 2010 ACF
			if (form.getDiasDesde() != null && !form.getDiasDesde().equals("")) {
				param++;
				ps.setString(param, form.getDiasDesde());
			}
			if (form.getDiasHasta() != null && !form.getDiasHasta().equals("")) {
				param++;
				ps.setString(param, form.getDiasHasta());
			}

			rs = ps.executeQuery();

			if (rs != null)
				System.out.println("si tiene datos");

			return rs;
		} catch (Exception e) {
			log.info("consConciliacionDetalle(ERROR) ::..");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			/*if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}*/
			log.info("consConciliacionDetalle(S)");
		}
	} //cierre metodo

	/**
	 * Obtiene el detalle de los moovimientos que conforman el FONDO CONCENTRADOR
	 * @throws com.netro.exception.NafinException
	 * @return List
	 * @param prestamo
	 * @param estatus
	 */
	public List consConciliacionPop(String prestamo, String estatus, String clavePrograma) throws NafinException {
		log.info("consConciliacionPop(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer qrySentence = new StringBuffer();
		List lstConciliaDetGral = new ArrayList();
		try {
			con.conexionDB();


			qrySentence.append("SELECT   tablota.fec_venc fec_venc, tablota.fec_fide fec_fide, " +
									 "		 tablota.amort amort, tablota.interes interes, tablota.saldo saldo, " +
									 "         tablota.estatus estatus, tablota.observaciones observaciones, " +
									 "         tablota.fec_val fec_val, tablota.usuario usuario, " +
									 "         tablota.prestamo prestamo, tablota.num_cliente num_cliente, " +
									 "         tablota.fec_cliente fec_cliente, tablota.num_cuota num_cuota, tablota.origen origen, " +
									 "         tablota.nombrecliente nombrecliente, prg.cg_descripcion origen_desc, " + "			(  " +
									 "      	SELECT  " + "         	DECODE(   " + "					tablota.estatus,   " + "					'NP',  " +
									 "					DECODE(    " + "						(  " + "							SELECT   " + "								COUNT(1)  " + "							FROM     " +
									 "								COM_PAGOS_FIDE_VAL CPV2  " + "							WHERE    " +
									 "								CPV2.COM_FECHAPROBABLEPAGO     = to_date(tablota.fec_venc,'dd/mm/yyyy')  " +
									 "								AND CPV2.IG_PRESTAMO           = tablota.prestamo  " +
									 "								AND CPV2.IG_DISPOSICION        = tablota.num_cuota  " +
									 "								AND CPV2.FG_TOTALVENCIMIENTO   = tablota.saldo   " + "								AND CPV2.IC_PROGRAMA_FONDOJR 	=	" +
									 clavePrograma + "								AND CPV2.CG_ESTATUS IN ('R','RC','RF')  " + "						),  " + "						0,  " +
									 "						(  " + "							CASE  WHEN   " +
									 "								to_date(tablota.fec_venc,'dd/mm/yyyy') < trunc(sysdate+1)   " + "							then   " +
									 "								(trunc(sysdate) - to_date(tablota.fec_venc,'dd/mm/yyyy'))   " + "							else   " +
									 "								to_date(tablota.fec_venc,'dd/mm/yyyy')-to_date(tablota.fec_venc,'dd/mm/yyyy')   " + "							end  " +
									 "						),0  " + "             	 " + "					)  " + "         		,NULL  " + "				)   " + "      	FROM  " +
									 "         	COM_CRED_PARAM_DIAS_VENC CCP  " + "      	WHERE  " + "			CCP.IC_PROGRAMA_FONDOJR 	=	" +
									 clavePrograma + "		AND 	 " +
									 "         	to_date(tablota.fec_venc,'dd/mm/yyyy') 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) 	AND 	 " +
									 "      		to_date(tablota.fec_venc,'dd/mm/yyyy') 	< 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) 		 " +
									 "   	) AS DIAS_TRANSCURRIDOS_VENCIMIENTO,  " + "    	(  " + "       	SELECT   " +
									 "         	CRED_PARAM.IG_DIAS_VENCIMIENTO   " + "       	FROM   " +
									 "         	COM_CRED_PARAM_DIAS_VENC CRED_PARAM  " + "       	WHERE   " +
									 "				CRED_PARAM.IC_PROGRAMA_FONDOJR 	=	" + clavePrograma + "					AND 	 " +
									 "         	to_date(tablota.fec_venc,'dd/mm/yyyy')  		>= TRUNC(CRED_PARAM.DF_FECHA_VMTO_INI) 	AND  	 " +
									 "         	to_date(tablota.fec_venc,'dd/mm/yyyy')			< 	TRUNC(CRED_PARAM.DF_FECHA_VMTO_FIN+1) 			 " +
									 "    	) AS dias_limite " + "    FROM (SELECT TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc, " +
									 "                 TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide, " +
									 "                 cpv.fg_amortizacion amort, cpv.fg_interes interes, " +
									 "                 cpv.fg_totalvencimiento saldo, cpv.cg_estatus estatus, " +
									 "                 cpv.cg_observaciones observaciones, " +
									 "                 TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val, cpv.cg_usuario usuario, " +
									 "                 cpv.ig_prestamo prestamo, cpv.ig_cliente num_cliente, " +
									 "                 to_char (cpv.df_pago_cliente,'dd/mm/yyyy') fec_cliente, cpv.ig_disposicion num_cuota, " +
									 "                 cpv.ig_origen origen, cpv.cg_nombrecliente nombrecliente " +
									 "            FROM com_pagos_fide_val cpv " + "           WHERE (   cpv.cg_estatus = 'P' " +
									 "                  OR cpv.cg_estatus = 'T' " + "                  OR cpv.cg_estatus = 'RC' " +
									 "                  OR cpv.cg_estatus = 'R' " + "                 ) " +
									 "             AND cpv.cs_validacion = 'A' " + "             AND cpv.ic_programa_fondojr = " + clavePrograma + //FODEA 026 - 2010 ACF
									 "          UNION ALL " + "          SELECT TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc, " +
									 "                 TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide, " +
									 "                 cpv.fg_amortizacion amort, cpv.fg_interes interes, " +
									 "                 cpv.fg_totalvencimiento saldo, cpv.cg_estatus estatus, " +
									 "                 cpv.cg_observaciones observaciones, " +
									 "                 TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val, cpv.cg_usuario usuario, " +
									 "                 cpv.ig_prestamo prestamo, cpv.ig_cliente num_cliente, " +
									 "                 to_char (cpv.df_pago_cliente,'dd/mm/yyyy') fec_cliente, cpv.ig_disposicion num_cuota, " +
									 "                 cpv.ig_origen origen, cpv.cg_nombrecliente nombrecliente " +
									 "            FROM com_pagos_fide_val cpv " + "           WHERE cpv.cg_estatus = 'RF' " +
									 "             AND cpv.cs_validacion = 'A' " + "             AND cpv.ic_programa_fondojr = " + clavePrograma + //FODEA 026 - 2010 ACF
									 "             AND NOT EXISTS ( " + "                    SELECT 1 " +
									 "                      FROM com_pagos_fide_val " + "                     WHERE cg_estatus = 'RC' " +
									 "             					AND cpv.ic_programa_fondojr = " + clavePrograma + //FODEA 026 - 2010 ACF
									 "                       AND com_fechaprobablepago = cpv.com_fechaprobablepago " +
									 "                       AND ig_prestamo = cpv.ig_prestamo " +
									 "                       AND ig_cliente = cpv.ig_cliente " +
									 "                       AND ig_disposicion = cpv.ig_disposicion " +
									 "                       AND fg_totalvencimiento = cpv.fg_totalvencimiento) " + "          UNION ALL " +
									 "          SELECT TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc, " +
									 "                 TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide, " +
									 "                 cpv.fg_amortizacion amort, cpv.fg_interes interes, " +
									 "                 cpv.fg_totalvencimiento saldo, cpv.cg_estatus estatus, " +
									 "                 cpv.cg_observaciones observaciones, " +
									 "                 TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val, cpv.cg_usuario usuario, " +
									 "                 cpv.ig_prestamo prestamo, cpv.ig_cliente num_cliente, " +
									 "                 to_char (cpv.df_pago_cliente,'dd/mm/yyyy') fec_cliente, cpv.ig_disposicion num_cuota, " +
									 "                 cpv.ig_origen origen, cpv.cg_nombrecliente nombrecliente " +
									 "            FROM com_pagos_fide_val cpv " + "           WHERE cpv.cg_estatus = 'NP' " +
									 "             AND cpv.cs_validacion = 'A' " + "             AND cpv.ic_programa_fondojr = " + clavePrograma + //FODEA 026 - 2010 ACF
									 /*"             AND NOT EXISTS ( " +
						"                    SELECT 1 " +
						"                      FROM com_pagos_fide_val " +
						"                     WHERE (   cg_estatus = 'R' " +
						"                            OR cg_estatus = 'RF' " +
						"                            OR cg_estatus = 'RC' " +
						"                           ) " +
						"             					AND cpv.ic_programa_fondojr = " + clavePrograma +//FODEA 026 - 2010 ACF
						"                       AND com_fechaprobablepago = cpv.com_fechaprobablepago " +
						"                       AND ig_prestamo = cpv.ig_prestamo " +
						"                       AND ig_cliente = cpv.ig_cliente " +
						"                       AND ig_disposicion = cpv.ig_disposicion " +
						"                       AND fg_totalvencimiento = cpv.fg_totalvencimiento) " +*/
								"          ) tablota  " + "	, comcat_origen_programa prg " + "WHERE  tablota.origen = prg.ig_origen_programa " +
								"AND tablota.prestamo = ? " + "AND tablota.estatus = ? " + "ORDER BY tablota.fec_venc ");

			log.debug("qrySentence>>>>>>>" + qrySentence);
			ps = con.queryPrecompilado(qrySentence.toString());
			ps.setLong(1, Long.parseLong(prestamo));
			ps.setString(2, estatus);
			rs = ps.executeQuery();

			if (rs != null) {
				while (rs.next()) {
					List lstConciliaDetReg = new ArrayList();
					/*0*/lstConciliaDetReg.add(rs.getString("fec_venc") != null ? rs.getString("fec_venc") : "");
					/*1*/lstConciliaDetReg.add(rs.getString("fec_fide") != null ? rs.getString("fec_fide") : "");
					/*2*/lstConciliaDetReg.add(rs.getString("amort") != null ? rs.getString("amort") : "");
					/*3*/lstConciliaDetReg.add(rs.getString("interes") != null ? rs.getString("interes") : "");
					/*4*/lstConciliaDetReg.add(rs.getString("saldo") != null ? rs.getString("saldo") : "");
					/*5*/lstConciliaDetReg.add(rs.getString("estatus") != null ? rs.getString("estatus") : "");
					/*6*/lstConciliaDetReg.add(rs.getString("observaciones") != null ? rs.getString("observaciones") : "");
					/*7*/lstConciliaDetReg.add(rs.getString("fec_val") != null ? rs.getString("fec_val") : "");
					/*8*/lstConciliaDetReg.add(rs.getString("usuario") != null ? rs.getString("usuario") : "");
					/*9*/lstConciliaDetReg.add(rs.getString("prestamo") != null ? rs.getString("prestamo") : "");

					/*10*/lstConciliaDetReg.add(rs.getString("fec_cliente") != null ? rs.getString("fec_cliente") : "");
					/*11*/lstConciliaDetReg.add(rs.getString("num_cliente") != null ? rs.getString("num_cliente") : "");
					/*12*/lstConciliaDetReg.add(rs.getString("nombrecliente") != null ? rs.getString("nombrecliente") : "");
					/*13*/lstConciliaDetReg.add(rs.getString("origen_desc") != null ? rs.getString("origen_desc") : "");
					/*14*/lstConciliaDetReg.add(rs.getString("num_cuota") != null ? rs.getString("num_cuota") : "");
					/*15*/lstConciliaDetReg.add(rs.getString("DIAS_TRANSCURRIDOS_VENCIMIENTO") != null ?
														 rs.getString("DIAS_TRANSCURRIDOS_VENCIMIENTO") : "0");
					/*16*/lstConciliaDetReg.add(rs.getString("dias_limite") != null ? rs.getString("dias_limite") : "0");

					lstConciliaDetGral.add(lstConciliaDetReg);
				}
			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			return lstConciliaDetGral;
		} catch (Exception e) {
			log.info("consConciliacionPop(ERROR) ::.. ");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consConciliacionPop(S) ::..");
		}
	} //cierre metodo

	/*
	 * Obtiene el detalle de los prestamos regresados por el FIDE
	 * @throws com.netro.exception.NafinException
	 * @return List
	 * @param form
	 */
	/*Este m�todo ya no se emplea, la consulta fue movida a un paginador//FODEA 026 - 2010 ACF
	public List consConsultaDetalle(FondoJrConsultaBean form) throws NafinException {
		log.info("consConsultaDetalle(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer qrySentence = new StringBuffer();
		String Where_And = " AND ";
		List lstConsultaDetGral = new ArrayList();
		BigDecimal montoCap = new BigDecimal("0.00");
		BigDecimal montoInteres = new BigDecimal("0.00");
		BigDecimal montoSaldo = new BigDecimal("0.00");
		int param = 0;
		try{
			con.conexionDB();
			qrySentence.append( "SELECT TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc, " +
								"       TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide, " +
								"       cpv.fg_amortizacion amort, cpv.fg_interes interes, " +
								"       cpv.fg_totalvencimiento saldo,  " +
								"		  decode(cpv.cg_estatus,'P','Pagado','NP','No Pagado','R', " +
								"       		'Reembolso','RF','Reembolso por Finiquito', " +
								"            'RC','Recuperacion de Fondo','T','Prepagos Totales') estatus, "+
								"       cpv.cg_observaciones observaciones, " +
								"       TO_CHAR (cpv.df_validacion, 'dd/mm/yyyy') fec_val, " +
								"       cpv.cg_usuario usuario, cpv.ig_prestamo prestamo, " +
								"       cpv.ig_cliente num_cliente, " +
								"       TO_CHAR (cpv.df_pago_cliente, 'dd/mm/yyyy') fec_cliente, " +
								"       cpv.ig_disposicion num_cuota, "+
								"       decode(cpv.cs_validacion,'P','En Proceso','A','Validado','R','Rechazado','En Proceso') estatus_val, " +
								"       cpv.cg_nombrecliente nombrecliente, prg.cg_descripcion origen_desc " +
								"  FROM com_pagos_fide_val cpv, comcat_origen_programa prg " +
								" WHERE  cpv.ig_origen = prg.ig_origen_programa " +
								"   AND (   cpv.cg_estatus = 'P' " +
								"        OR cpv.cg_estatus = 'NP' " +
								"        OR cpv.cg_estatus = 'T' " +
								"        OR cpv.cg_estatus = 'RC' " +
								"        OR cpv.cg_estatus = 'RF' " +
								"        OR cpv.cg_estatus = 'R' " +
								"       ) ");
			if(form.getFecVencIni()!=null && !form.getFecVencIni().equals("")){
				qrySentence.append(Where_And+" cpv.com_fechaprobablepago >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				Where_And = " AND ";
			}
			if(form.getFecVencFin()!=null && !form.getFecVencFin().equals("")){
				qrySentence.append(Where_And+" cpv.com_fechaprobablepago < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
				Where_And = " AND ";
			}
			if(form.getFecOperIni()!=null && !form.getFecOperIni().equals("")){
				qrySentence.append(Where_And+" cpv.df_registro_fide >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				Where_And = " AND ";
			}
			if(form.getFecOperFin()!=null && !form.getFecOperFin().equals("")){
				qrySentence.append(Where_And+" cpv.df_registro_fide < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
				Where_And = " AND ";
			}
			if(form.getFecFideIni()!=null && !form.getFecFideIni().equals("")){
				qrySentence.append(Where_And+" cpv.df_pago_cliente >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				Where_And = " AND ";
			}
			if(form.getFecFideFin()!=null && !form.getFecFideFin().equals("")){
				qrySentence.append(Where_And+" cpv.df_pago_cliente < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
				Where_And = " AND ";
			}
			if(form.getEstatus()!=null && !form.getEstatus().equals("")){
				qrySentence.append(Where_And+" cpv.cg_estatus = ? ");
				Where_And = " AND ";
			}
			if(form.getEstatusVal()!=null && !form.getEstatusVal().equals("")){
				qrySentence.append(Where_And+" cpv.cs_validacion = ? ");
				Where_And = " AND ";
			}
			if(form.getNumSirac()!=null && !form.getNumSirac().equals("")){
				qrySentence.append(Where_And+" cpv.ig_cliente = ? ");
				Where_And = " AND ";
			}
			if(form.getPrestamo()!=null && !form.getPrestamo().equals("")){
				qrySentence.append(Where_And+" cpv.ig_prestamo = ? ");
				Where_And = " AND ";
			}

			if(form.getOrden()!=null && !form.getOrden().equals("")){
				if(form.getOrden().equals("E"))qrySentence.append(" order by cpv.cg_estatus ");
				else if(form.getOrden().equals("V"))qrySentence.append(" order by cpv.com_fechaprobablepago ");
				else if(form.getOrden().equals("F"))qrySentence.append(" order by cpv.df_registro_fide ");
			}

			log.debug("qrySentence>>>>>>>>>"+qrySentence.toString());
			ps = con.queryPrecompilado(qrySentence.toString());
			if(form.getFecVencIni()!=null && !form.getFecVencIni().equals("")){ param++; ps.setString(param,form.getFecVencIni());	}
			if(form.getFecVencFin()!=null && !form.getFecVencFin().equals("")){ param++; ps.setString(param,form.getFecVencFin());	}
			if(form.getFecOperIni()!=null && !form.getFecOperIni().equals("")){ param++; ps.setString(param,form.getFecOperIni());	}
			if(form.getFecOperFin()!=null && !form.getFecOperFin().equals("")){ param++; ps.setString(param,form.getFecOperFin());	}
			if(form.getFecFideIni()!=null && !form.getFecFideIni().equals("")){ param++; ps.setString(param,form.getFecFideIni());	}
			if(form.getFecFideFin()!=null && !form.getFecFideFin().equals("")){ param++; ps.setString(param,form.getFecFideFin());	}

			if(form.getEstatus()!=null && !form.getEstatus().equals("")){ param++; ps.setString(param,form.getEstatus());	}
			if(form.getEstatusVal()!=null && !form.getEstatusVal().equals("")){ param++; ps.setString(param,form.getEstatusVal());	}
			if(form.getNumSirac()!=null && !form.getNumSirac().equals("")){ param++; ps.setString(param,form.getNumSirac());	}
			if(form.getPrestamo()!=null && !form.getPrestamo().equals("")){ param++; ps.setString(param,form.getPrestamo());	}

			rs = ps.executeQuery();

			if(rs!=null){
				while(rs.next()){
					List lstConsultaDetReg = new ArrayList();
					/*0//lstConsultaDetReg.add(rs.getString("fec_venc")!=null?rs.getString("fec_venc"):"");
					/*1//lstConsultaDetReg.add(rs.getString("fec_fide")!=null?rs.getString("fec_fide"):"");
					/*2//lstConsultaDetReg.add(rs.getString("amort")!=null?rs.getString("amort"):"");
					/*3//lstConsultaDetReg.add(rs.getString("interes")!=null?rs.getString("interes"):"");
					/*4//lstConsultaDetReg.add(rs.getString("saldo")!=null?rs.getString("saldo"):"");
					/*5//lstConsultaDetReg.add(rs.getString("estatus")!=null?rs.getString("estatus"):"");
					/*6//lstConsultaDetReg.add(rs.getString("observaciones")!=null?rs.getString("observaciones"):"");
					/*7//lstConsultaDetReg.add(rs.getString("fec_val")!=null?rs.getString("fec_val"):"");
					/*8//lstConsultaDetReg.add(rs.getString("usuario")!=null?rs.getString("usuario"):"");
					/*9//lstConsultaDetReg.add(rs.getString("prestamo")!=null?rs.getString("prestamo"):"");

					/*10//lstConsultaDetReg.add(rs.getString("fec_cliente")!=null?rs.getString("fec_cliente"):"");
					/*11//lstConsultaDetReg.add(rs.getString("num_cliente")!=null?rs.getString("num_cliente"):"");
					/*12//lstConsultaDetReg.add(rs.getString("nombrecliente")!=null?rs.getString("nombrecliente"):"");
					/*13//lstConsultaDetReg.add(rs.getString("origen_desc")!=null?rs.getString("origen_desc"):"");
					/*14//lstConsultaDetReg.add(rs.getString("num_cuota")!=null?rs.getString("num_cuota"):"");
					/*15//lstConsultaDetReg.add(rs.getString("estatus_val")!=null?rs.getString("estatus_val"):"");

					montoCap = montoCap.add(new BigDecimal(rs.getString("amort")!=null?rs.getString("amort"):"0.00"));
					montoInteres = montoInteres.add(new BigDecimal(rs.getString("interes")!=null?rs.getString("interes"):"0.00"));
					montoSaldo = montoSaldo.add(new BigDecimal(rs.getString("saldo")!=null?rs.getString("saldo"):"0.00"));

					lstConsultaDetGral.add(lstConsultaDetReg);
				}
				if(form.getSubtotales()!=null && !form.getSubtotales().equals("")){
				List lstConsultaDetReg = new ArrayList();
				lstConsultaDetReg.add(montoCap.toString());
				lstConsultaDetReg.add(montoInteres.toString());
				lstConsultaDetReg.add(montoSaldo.toString());
				lstConsultaDetGral.add(lstConsultaDetReg);
				}
			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();
			return lstConsultaDetGral;
		}catch(Exception e){
			log.info("consConsultaDetalle(ERROR) ::..");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("consConsultaDetalle(S) ::..");
		}
	}//cierre metodo
	*/
	/****************************************************************************************************************************/
	/****************************************************************************************************************************/
	/********************************************************FODEA 057-2009**********************************************/
	/****************************************************************************************************************************/
	/****************************************************************************************************************************/

	/**
	 * M�todo que registra en la tabla temporal los reembolosos por finiquito.
	 * @param datos_reembolso Lista con los parametros:
	 *           0) Numero de proceso de carga.
	 *           1) Numero de prestamo.
	 *           2) Observaciones.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author
	 * @return
	 * @param datos_prepagos
	 */
	public String agregaPrepagosTmp(List datos_prepagos) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		boolean trans_op = true;
		String num_proceso = "";

		System.out.println("..:: FondoJuniorBean : agregaPrepagosTmp(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" INSERT INTO COM_TMP_PREPAGOS_TOTALES (IG_PRESTAMO, IG_CLIENTE, IG_DISPOSICION, DF_REAL_PAGO, ");
			strSQL.append(" FG_AMORTIZACION,FG_INTERES, FG_TOTALVENCIMIENTO, DF_PAGO_CLIENTE, CG_ESTATUS, IG_ORIGEN, IC_PROCESO, IC_PROGRAMA_FONDOJR)");
			strSQL.append(" VALUES (?, ?, ?, to_date(?,'dd/mm/yyyy'),?,?,?,to_date(?,'dd/mm/yyyy'),?,?,?,?)");

			if (datos_prepagos != null && datos_prepagos.size() > 0) {

				String qrySecuence = "select SEQ_com_tmp_prepagos_totales.nextval from dual ";

				rs = con.queryDB(qrySecuence);

				if (rs.next())
					num_proceso = rs.getString(1);

				for (int x = 0; x < datos_prepagos.size(); x++) {
					List lstDatosReg = (ArrayList) datos_prepagos.get(x);
					String fec_real_pago = (String) lstDatosReg.get(0);
					long num_prestamo = Long.parseLong((String) lstDatosReg.get(1));
					long codigo_cliente = Long.parseLong((String) lstDatosReg.get(2));
					long disposicion = Long.parseLong((String) lstDatosReg.get(3));
					double fn_amortizacion = Double.parseDouble((String) lstDatosReg.get(4));
					double fn_interes = Double.parseDouble((String) lstDatosReg.get(5));
					double fn_total_venc = Double.parseDouble((String) lstDatosReg.get(6));
					String fec_pago_cliente = (String) lstDatosReg.get(7);
					String estatus_registro = (String) lstDatosReg.get(8);
					String origen = (String) lstDatosReg.get(9);
					String clavePrograma = (String) lstDatosReg.get(10);

					pst = con.queryPrecompilado(strSQL.toString());
					pst.setLong(1, num_prestamo);
					pst.setLong(2, codigo_cliente);
					pst.setLong(3, disposicion);
					pst.setString(4, fec_real_pago);
					pst.setDouble(5, fn_amortizacion);
					pst.setDouble(6, fn_interes);
					pst.setDouble(7, fn_total_venc);
					pst.setString(8, fec_pago_cliente);
					pst.setString(9, estatus_registro);
					pst.setString(10, origen);
					pst.setLong(11, Long.parseLong(num_proceso));
					pst.setInt(12, Integer.parseInt(clavePrograma));

					pst.executeUpdate();

					pst.close();
				} //cierre for
			} //cierre if (datos_prepagos)

		} catch (Exception e) {
			trans_op = false;
			System.out.println("..:: FondoJuniorBean : agregaPrepagosTmp(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : agregaPrepagosTmp(F) ::..");
		}
		return num_proceso;
	}

	/**
	 *
	 */
	public List detallePrepagosTmp(String num_proceso) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List lstGralPrepagosTmp = new ArrayList();
		boolean trans_op = true;

		String fec_real_pago = "";
		String num_prestamo = "";
		String codigo_cliente = "";
		String disposicion = "";
		String fn_amortizacion = "";
		String fn_interes = "";
		String fn_total_venc = "";
		String fec_pago_cliente = "";
		String estatus_registro = "";
		String origen = "";

		System.out.println("..:: FondoJuniorBean : detallePrepagosTmp(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" SELECT IG_PRESTAMO IG_PRESTAMO, IG_CLIENTE IG_CLIENTE, IG_DISPOSICION IG_DISPOSICION, TO_CHAR(DF_REAL_PAGO,'DD/MM/YYYY') DF_REAL_PAGO, ");
			strSQL.append(" FG_AMORTIZACION FG_AMORTIZACION,FG_INTERES FG_INTERES, FG_TOTALVENCIMIENTO FG_TOTALVENCIMIENTO, TO_CHAR(DF_PAGO_CLIENTE,'DD/MM/YYYY') DF_PAGO_CLIENTE, CG_ESTATUS CG_ESTATUS, IG_ORIGEN IG_ORIGEN");
			strSQL.append(" FROM COM_TMP_PREPAGOS_TOTALES WHERE IC_PROCESO = ? ");

			pst = con.queryPrecompilado(strSQL.toString());
			pst.setLong(1, Long.parseLong(num_proceso));

			rs = pst.executeQuery();

			while (rs != null && rs.next()) {
				List lstPrepagosTmp = new ArrayList();

				fec_real_pago = rs.getString("DF_REAL_PAGO");
				num_prestamo = rs.getString("IG_PRESTAMO");
				codigo_cliente = rs.getString("IG_CLIENTE");
				disposicion = rs.getString("IG_DISPOSICION");
				fn_amortizacion = rs.getString("FG_AMORTIZACION");
				fn_interes = rs.getString("FG_INTERES");
				fn_total_venc = rs.getString("FG_TOTALVENCIMIENTO");
				fec_pago_cliente = rs.getString("DF_PAGO_CLIENTE");
				estatus_registro = rs.getString("CG_ESTATUS");
				origen = rs.getString("IG_ORIGEN");

				lstPrepagosTmp.add(fec_real_pago);
				lstPrepagosTmp.add(num_prestamo);
				lstPrepagosTmp.add(codigo_cliente);
				lstPrepagosTmp.add(disposicion);
				lstPrepagosTmp.add(fn_amortizacion);
				lstPrepagosTmp.add(fn_interes);
				lstPrepagosTmp.add(fn_total_venc);
				lstPrepagosTmp.add(fec_pago_cliente);
				lstPrepagosTmp.add(estatus_registro);
				lstPrepagosTmp.add(origen);

				lstGralPrepagosTmp.add(lstPrepagosTmp);

			} //cierre WHILE
			if (rs != null)
				rs.close();
			if (pst != null)
				pst.close();

		} catch (Exception e) {
			trans_op = false;
			System.out.println("..:: FondoJuniorBean : detallePrepagosTmp(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : detallePrepagosTmp(F) ::..");
		}
		return lstGralPrepagosTmp;
	}

	/**
	 *
	 */
	public HashMap terminaOperacionPrepagos(FondoJrPrepagoTotalBean fondoJrPrepagoTotalBean) throws NafinException {

		System.out.println("FondoJuniorBean::terminaOperacionPrepagos(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean commit = true;
		List lstGralPrepagos = new ArrayList();

		StringBuffer qry = new StringBuffer();
		StringBuffer strSQL = new StringBuffer();
		HashMap prepagos_transmitidos = new HashMap();
		String Acuse = "";
		String mostrarError = "";
		boolean validCert = false;


		try {
			con.conexionDB();


			char getReceipt = 'Y';

			if ((!validCert) ||
				 (!fondoJrPrepagoTotalBean.getSerial().equals("") &&
				  !fondoJrPrepagoTotalBean.getTextoFirmado().equals("") &&
				  !fondoJrPrepagoTotalBean.getPkcs7().equals(""))) {
				Seguridad s = new Seguridad();
				if ((!validCert) ||
					 (s.autenticar(fondoJrPrepagoTotalBean.getFolio(), fondoJrPrepagoTotalBean.getSerial(),
										fondoJrPrepagoTotalBean.getPkcs7(), fondoJrPrepagoTotalBean.getTextoFirmado(),
										getReceipt))) {

					Acuse = s.getAcuse();

					System.out.println("***************************");
					System.out.println("**Acuse***" + Acuse);
					System.out.println("***************************");


					//SE REALIZA EL TRASPASO DE LOS PREPAGOS TOTALES A LA TABLA COM_PAGOS_FIDE
					qry.append("INSERT INTO COM_PAGOS_FIDE (IG_PRESTAMO , IG_CLIENTE , IG_DISPOSICION, COM_FECHAPROBABLEPAGO, ");
					qry.append(" FG_AMORTIZACION ,FG_INTERES , FG_TOTALVENCIMIENTO, DF_PAGO_CLIENTE, CG_ESTATUS, IG_ORIGEN, IC_PROGRAMA_FONDOJR) ");
					qry.append("SELECT IG_PRESTAMO , IG_CLIENTE , IG_DISPOSICION , DF_REAL_PAGO,");
					qry.append("FG_AMORTIZACION, FG_INTERES, FG_TOTALVENCIMIENTO, DF_PAGO_CLIENTE, CG_ESTATUS, IG_ORIGEN, IC_PROGRAMA_FONDOJR ");
					qry.append("FROM COM_TMP_PREPAGOS_TOTALES WHERE IC_PROCESO = ? ");


					ps = con.queryPrecompilado(qry.toString());
					ps.setLong(1, Long.parseLong(fondoJrPrepagoTotalBean.getNum_proceso()));
					ps.executeUpdate();
					ps.close();

					//SE REALIZA CONSULTA DE LOS REGISTROS AFECTADOS PARA MOSTRARLOS EN EL ACUSE
					strSQL.append(" SELECT IG_PRESTAMO IG_PRESTAMO, IG_CLIENTE IG_CLIENTE, IG_DISPOSICION IG_DISPOSICION, TO_CHAR(DF_REAL_PAGO,'DD/MM/YYYY') DF_REAL_PAGO, ");
					strSQL.append(" FG_AMORTIZACION FG_AMORTIZACION,FG_INTERES FG_INTERES, FG_TOTALVENCIMIENTO FG_TOTALVENCIMIENTO, TO_CHAR(DF_PAGO_CLIENTE,'DD/MM/YYYY') DF_PAGO_CLIENTE, CG_ESTATUS CG_ESTATUS, IG_ORIGEN IG_ORIGEN");
					strSQL.append(" FROM COM_TMP_PREPAGOS_TOTALES WHERE IC_PROCESO = ? ");

					ps = con.queryPrecompilado(strSQL.toString());
					ps.setLong(1, Long.parseLong(fondoJrPrepagoTotalBean.getNum_proceso()));
					rs = ps.executeQuery();

					while (rs != null && rs.next()) {
						List lstPrepagos = new ArrayList();

						lstPrepagos.add(rs.getString("DF_REAL_PAGO"));
						lstPrepagos.add(rs.getString("IG_PRESTAMO"));
						lstPrepagos.add(rs.getString("IG_CLIENTE"));
						lstPrepagos.add(rs.getString("IG_DISPOSICION"));
						lstPrepagos.add(rs.getString("FG_AMORTIZACION"));
						lstPrepagos.add(rs.getString("FG_INTERES"));
						lstPrepagos.add(rs.getString("FG_TOTALVENCIMIENTO"));
						lstPrepagos.add(rs.getString("DF_PAGO_CLIENTE"));
						lstPrepagos.add(rs.getString("CG_ESTATUS"));
						lstPrepagos.add(rs.getString("IG_ORIGEN"));

						lstGralPrepagos.add(lstPrepagos);

					} //cierre WHILE
					if (rs != null)
						rs.close();
					if (ps != null)
						ps.close();

				} else {
					mostrarError = s.mostrarError();
				}
			}
			System.out.println("FondoJuniorBean::mostrarError " + mostrarError);

			prepagos_transmitidos.put("PrepagosTransmitidos", lstGralPrepagos);
			prepagos_transmitidos.put("Acuse", Acuse);


		} catch (Exception e) {
			commit = false;
			System.out.println("FondoJuniorBean::terminaOperacionPrepagos(Exception) " + e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(commit);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		System.out.println("FondoJuniorBean::terminaOperacionPrepagos(S)");
		return prepagos_transmitidos;
	}

	//==============================================================================>>

	/**
	 * M�todo que guarda la validaci�n de los cr�ditos de la pantalla de Validaci�n.
	 * @param parametros_consulta Lista con los valores de los parametros de consulta.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public String guardarValidacionCreditos(List parametros_consulta) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String folioValidacion = "";
		boolean trans_op = true;

		System.out.println("..:: FondoJuniorBean : guardarValidacionCreditos(I) ::..");
		try {
			con.conexionDB();

			String fecha_amort_nafin_ini = (String) parametros_consulta.get(0);
			String fecha_amort_nafin_fin = (String) parametros_consulta.get(1);
			String estatus_registro = (String) parametros_consulta.get(2);
			String numero_prestamo = (String) parametros_consulta.get(3);
			String clave_if = (String) parametros_consulta.get(4);
			String numero_sirac = (String) parametros_consulta.get(5);
			String fecha_pago_cliente_fide_ini = (String) parametros_consulta.get(6);
			String fecha_pago_cliente_fide_fin = (String) parametros_consulta.get(7);
			String fecha_registro_fide_ini = (String) parametros_consulta.get(8);
			String fecha_registro_fide_fin = (String) parametros_consulta.get(9);
			String tipo_validacion = (String) parametros_consulta.get(10);
			String seccion_estatus = (String) parametros_consulta.get(11);

			if (fecha_amort_nafin_ini.equals("")) {
				strSQL = new StringBuffer();
				strSQL.append(" SELECT TO_CHAR(MIN(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
				strSQL.append(" FROM com_pagos_fide_val cpf");

				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();

				while (rst.next()) {
					fecha_amort_nafin_ini =
									 rst.getString("fecha_amort_nafin") == null ? "" : rst.getString("fecha_amort_nafin");
				}

				rst.close();
				pst.close();
			}

			if (fecha_amort_nafin_fin.equals("")) {
				strSQL = new StringBuffer();
				strSQL.append(" SELECT TO_CHAR(MAX(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
				strSQL.append(" FROM com_pagos_fide_val cpf");

				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();

				while (rst.next()) {
					fecha_amort_nafin_fin =
									 rst.getString("fecha_amort_nafin") == null ? "" : rst.getString("fecha_amort_nafin");
				}

				rst.close();
				pst.close();
			}
			/*------------------------------------------------------------------------*/ //AJUSTE VALIDACION ACF
			strSQL = new StringBuffer();

			strSQL.append(" SELECT seq_com_pagos_ffolio_val.nextval AS folio_validacion FROM dual");

			pst = con.queryPrecompilado(strSQL.toString());
			rst = pst.executeQuery();

			while (rst.next()) {
				folioValidacion = rst.getString("folio_validacion") == null ? "" : rst.getString("folio_validacion");
			}

			rst.close();
			pst.close();
			/*------------------------------------------------------------------------*/
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" UPDATE com_pagos_fide_val");
			strSQL.append(" SET cs_validacion = ?,");
			strSQL.append(" ig_folio_val = ?,");
			strSQL.append(" df_validacion = SYSDATE");
			strSQL.append(" WHERE ig_prestamo IN (");
			strSQL.append(" SELECT /*+ leading(cpf) use_nl(cpf ccb prg cpi pyme)*/ cpf.ig_prestamo");
			strSQL.append(" FROM com_pagos_fide_val cpf");
			strSQL.append(", comcat_origen_programa prg");
			if (!clave_if.equals("")) { //..:: intermediario financiero
				strSQL.append(", comcat_pyme pyme");
				strSQL.append(", comrel_cuenta_bancaria ccb");
				strSQL.append(", comrel_pyme_if cpi");
			}
			strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
			strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
			strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
			strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
			if (!numero_prestamo.equals("")) { //..:: numero de prestamo
				strSQL.append(" AND cpf.ig_prestamo = ?");
			}
			if (!clave_if.equals("")) { //..:: intermediario financiero
				strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
				strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
				strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
				strSQL.append(" AND pyme.cs_habilitado = ?");
				strSQL.append(" AND cpi.cs_vobo_if = ?");
				strSQL.append(" AND ccb.ic_moneda = ?");
				strSQL.append(" AND cpi.ic_if = ?");
			}
			if (!numero_sirac.equals("")) { //..:: numero sirac cliente
				strSQL.append(" AND cpf.ig_cliente = ?");
			}
			if (!fecha_pago_cliente_fide_ini.equals("") &&
				 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
						strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
			}
			if (!fecha_registro_fide_ini.equals("") &&
				 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
						strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
			}

			varBind.add(tipo_validacion.equals("ACEPTAR") ? "A" : "R");
			varBind.add(new Long(folioValidacion)); //-----------------------------------AJUSTE VALIDACION ACF
			varBind.add("P"); //..:: estatus de la validaci�n
			varBind.add(fecha_amort_nafin_ini); //..:: fecha de amortizacion nafin
			varBind.add(fecha_amort_nafin_fin); //..:: fecha de amortizacion nafin
			if (!numero_prestamo.equals("")) { //..:: numero de prestamo
				varBind.add(new Long(numero_prestamo));
			}
			if (!clave_if.equals("")) { //..:: intermediario financiero
				varBind.add("S");
				varBind.add("S");
				varBind.add(new Integer(1));
				varBind.add(new Integer(clave_if));
			}
			if (!numero_sirac.equals("")) { //..:: numero sirac cliente
				varBind.add(new Long(numero_sirac));
			}
			if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
						varBind.add(fecha_pago_cliente_fide_ini);
				varBind.add(fecha_pago_cliente_fide_fin);
			}
			if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
						varBind.add(fecha_registro_fide_ini);
				varBind.add(fecha_registro_fide_fin);
			}
			if (seccion_estatus.equals("PNP")) {
				if (!estatus_registro.equals("")) {
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					varBind.add(estatus_registro);
				} else {
					strSQL.append(" AND cpf.cg_estatus IN (?, ?)"); //..:: estatus del registro
					varBind.add("P");
					varBind.add("NP");
				}
			} else {
				if (!estatus_registro.equals("")) {
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					varBind.add(estatus_registro);
				} else {
					strSQL.append(" AND cpf.cg_estatus IN (?, ?, ?)"); //..:: estatus del registro
					varBind.add("R");
					varBind.add("T");
					varBind.add("RC");
				}
			}
			strSQL.append(")");

			strSQL.append(" AND cs_validacion = ?"); //..:: estatus de la validaci�n
			varBind.add("P");

			strSQL.append(" AND com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
			strSQL.append(" AND com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
			varBind.add(fecha_amort_nafin_ini); //..:: fecha de amortizacion nafin
			varBind.add(fecha_amort_nafin_fin); //..:: fecha de amortizacion nafin

			if (seccion_estatus.equals("PNP")) {
				if (!estatus_registro.equals("")) {
					strSQL.append(" AND cg_estatus = ?"); //..:: estatus del registro
					varBind.add(estatus_registro);
				} else {
					strSQL.append(" AND cg_estatus IN (?, ?)"); //..:: estatus del registro
					varBind.add("P");
					varBind.add("NP");
				}
			} else {
				if (!estatus_registro.equals("")) {
					strSQL.append(" AND cg_estatus = ?"); //..:: estatus del registro
					varBind.add(estatus_registro);
				} else {
					strSQL.append(" AND cg_estatus IN (?, ?, ?)"); //..:: estatus del registro
					varBind.add("R");
					varBind.add("T");
					varBind.add("RC");
				}
			}

			System.out.println("..:: strSQL : " + strSQL.toString());
			System.out.println("..:: varBind : " + varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			//codigo para la actualizacion de campo prepago_validacion
			if (seccion_estatus.equals("RTRF")) {
				actualizaPrepagoValidacionCreditos(con, parametros_consulta, folioValidacion);
			}


		} catch (Exception e) {
			trans_op = false;
			System.out.println("..:: FondoJuniorBean : guardarValidacionCreditos(ERROR) :");
			e.printStackTrace();
			throw new AppException();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : guardarValidacionCreditos(F) ::..");
		}
		return folioValidacion;
	}

	/**
	 * Obtiene Consulta general de registros(Reembolsos) cargados por el FIDE para Fondo Junior
	 * @throws com.netro.exception.NafinException
	 * @return List lstConsMonitor
	 * @param fec_fin - fecha de vencimiento final
	 * @param fec_ini - fecha de vencimiento inicial
	 */
	public List consultaMonitorReemb(String fec_ini, String fec_fin, String cvePrograma) throws NafinException {
		System.out.println("FondoJuniorBean::consultaMonitor(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		List lstConsMonitor = new ArrayList();
		List regMonitor = null;
		String qrySentence = "";
		String condFechas2 = ""; //FODEA 026-2010 FVR

		try {
			con.conexionDB();

			/*---------------- SE OBTIENE REGISTROS DE BITACORA-------------------*/
			if (fec_ini != null && !fec_ini.equals("") && fec_fin != null && !fec_fin.equals("")) {

				condFechas2 =
							  " AND PF.COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " +
							  " AND PF.COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY') ";
			}
			qrySentence =
						"SELECT  tmonitor.orden as morden, " + "		tmonitor.estatus as mestatus, " +
						"		tmonitor.fecha_venc as mfecha_venc, " + "        tmonitor.ultimo_reg as multimo_reg, " +
						"        tmonitor.total_reg as mtotal_reg, " + "        tmonitor.monto_tot as mmonto_tot, " +
						"        tmonitor.folio as mfolio, " + "        tmonitor.estatus_proc as mestatus_proc, " +
						"        tmonitor.tipo_reg as mtipo_reg, " + "        tmonitor.usuario as musuario " + "FROM ( " +
						"SELECT   '1' orden, " + "		 pf.cg_estatus estatus, " +
						"		 TO_CHAR (pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
						"         TO_CHAR (MAX (pf.df_registro_fide), 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " +
						"         COUNT (pf.cg_estatus) total_reg,  " + "         SUM (pf.fg_totalvencimiento) monto_tot, " +
						"         null folio, " + "         pf.cg_estatus_proc estatus_proc, " + "         null tipo_reg, " +
						"         null usuario " + "    FROM com_pagos_fide pf " + "   WHERE pf.cg_estatus IN ('R') " +
						"		AND pf.ic_programa_fondojr = ? " + condFechas2 + "     AND 'RC' != " +
						"            fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, " +
						"                                        pf.ig_prestamo, " +
						"                                        pf.ig_disposicion, " +
						"                                        pf.fg_totalvencimiento " + "                                       ) " +
						"GROUP BY pf.com_fechaprobablepago, pf.cg_estatus, pf.cg_estatus_proc " + ") tmonitor " +
						"order by tmonitor.orden, tmonitor.fecha_venc ";

			System.out.println("qrySentence>>>>>>" + qrySentence);
			ps = con.queryPrecompilado(qrySentence);
			int i = 0;

			ps.setLong(++i, Long.parseLong(cvePrograma));

			if (!condFechas2.equals("")) {
				ps.setString(++i, fec_ini);
				ps.setString(++i, fec_fin);
			}


			rs = ps.executeQuery();
			while (rs != null && rs.next()) {
				regMonitor = new ArrayList();
				regMonitor.add(rs.getString("MESTATUS"));
				regMonitor.add(rs.getString("MFECHA_VENC"));
				regMonitor.add(rs.getString("MULTIMO_REG"));
				regMonitor.add(rs.getString("MFOLIO"));
				regMonitor.add(rs.getString("MTOTAL_REG"));
				regMonitor.add(rs.getString("MMONTO_TOT"));
				regMonitor.add(rs.getString("MESTATUS_PROC"));
				lstConsMonitor.add(regMonitor);
			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();


			System.out.println("FondoJuniorBean::consultaMonitor(S)");
			return lstConsMonitor;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("FondoJuniorBean::consultaMonitor(Error)");
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}

	/**
	 * Realiza validacion de los registros que son Reembolsos
	 * que son Enviados por el FIDE (F026-2010 FVR)
	 * @throws com.netro.exception.NafinException
	 * @return Lista que contiene el resumen de la validacion de los regisrtros
	 * @param cg_estatus
	 */
	public List validaMonitorR(String fec_venc, String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null, ps2 = null;
		ResultSet rs = null, rs2 = null;
		StringBuffer txtError = new StringBuffer();
		StringBuffer qrySentence = new StringBuffer();
		List lstRegValidados = new ArrayList();
		List lstValidados = new ArrayList();
		BigDecimal montoTotalPagados = new BigDecimal("0.00");
		BigDecimal montoTotalNoPagados = new BigDecimal("0.00");
		BigDecimal montoTotalTMP = new BigDecimal("0.00");
		BigDecimal montoTotalReemb = new BigDecimal("0.00");
		BigDecimal montoTotalError = new BigDecimal("0.00");
		BigDecimal montoTotalRec = new BigDecimal("0.00");
		BigDecimal montoTotalNoEncon = new BigDecimal("0.00");
		BigDecimal montoTotalNoEnv = new BigDecimal("0.00");
		BigDecimal montoTotalPrepago = new BigDecimal("0.00");
		int sumaTotalPagados = 0;
		int sumaTotalNoPagados = 0;
		int sumaTotalTMP = 0;
		int sumaTotalReemb = 0;
		int sumaTotalError = 0;
		int sumaTotalRec = 0;
		int sumaTotalNoEncon = 0;
		int sumaTotalNoEnv = 0;
		int sumaTotalPrepago = 0;
		boolean realizaTransaccion = true;
		boolean regNoEncon = false;
		boolean regPrepagado = false;
		int param = 0;

		try {

			con.conexionDB();


			String qryError =
						"update com_pagos_fide set cg_error = ? " + "where com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
						"and ig_prestamo = ? " + "and ig_cliente = ? " + "and fg_totalvencimiento = ? " + "and ig_disposicion = ? " +
						"and ic_programa_fondojr = ? ";

			/*------------------------------------------------------------------------*/
			/*SE OBTIENE REGISTROS DE TABLA TEMPORAL POR FECHA DE VENCIMEINTO*/
			/*------------------------------------------------------------------------*/
			qrySentence.append("select to_char(com_fechaprobablepago,'dd/mm/yyyy') fec_venc, fg_totalvencimiento totalvencimiento, ");
			qrySentence.append("ig_prestamo prestamo, ig_cliente cliente, cg_estatus estatus, ig_disposicion disposicion ");
			qrySentence.append("from COM_PAGOS_FIDE ");
			qrySentence.append("where cg_estatus IN ( ? ) ");
			qrySentence.append("and cg_estatus_proc in ('N','P') ");
			if (!"R".equals(fec_venc)) {
				qrySentence.append("AND com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
			}
			qrySentence.append("and 'RC' != ");
			qrySentence.append("fn_es_recuperacion_fondojr (com_fechaprobablepago, ");
			qrySentence.append("ig_prestamo, ig_disposicion, fg_totalvencimiento) ");
			qrySentence.append("and ic_programa_fondojr = ? "); //FODEA 026-2010 FVR

			System.out.println("consDetMonitor:qrySentence>>" + qrySentence.toString());
			ps = con.queryPrecompilado(qrySentence.toString());
			ps.setString(++param, "R"); //MODIFICACION //FODEA 026-2010 FVR
			if (!"R".equals(fec_venc)) {
				ps.setString(++param, fec_venc); //MODIFICACION //FODEA 026-2010 FVR
			}
			ps.setLong(++param, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					regNoEncon = false;
					if (txtError.length() > 0)
						txtError.delete(0, txtError.length());
					/*---------------------------------*/
					/*SE REALIZA VALIDACIONES DE CAMPOS*/
					/*---------------------------------*/
					if (rs.getString("totalvencimiento") == null || !Comunes.esDecimal(rs.getString("totalvencimiento")))
						txtError.append("El campo: \"monto de Vencimiento\" no tiene un n�mero v�lido/");
					if (rs.getString("prestamo") == null || !Comunes.esNumero(rs.getString("prestamo")))
						txtError.append("El campo: \"prestamo\" no tiene un n�mero v�lido/");
					if (rs.getString("cliente") == null || !Comunes.esNumero(rs.getString("cliente")))
						txtError.append("El campo: \"cliente\" no tiene un n�mero v�lido/");
					if (rs.getString("estatus") == null ||
						 (!(rs.getString("estatus")).equals("P") && !(rs.getString("estatus")).equals("NP") &&
						  !(rs.getString("estatus")).equals("R") && !(rs.getString("estatus")).equals("T")))
						txtError.append("El campo: \"estatus\" es invalido/");


					if (rs.getString("estatus").equals("R")) { //CRXF
						boolean exist = false;

						String qryCompruebaReemb =
											"select fn_es_recuperacion_fondojr (to_date(?,'dd/mm/yyyy'), ?, ?, ?) validaReemb " +
											"from dual ";
						ps2 = con.queryPrecompilado(qryCompruebaReemb.toString());
						ps2.setString(1, rs.getString("fec_venc"));
						ps2.setLong(2, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(3, Long.parseLong(rs.getString("disposicion")));
						//ps2.setLong(3, Long.parseLong(rs.getString("cliente")));
						ps2.setBigDecimal(4, new BigDecimal(rs.getString("totalvencimiento")));

						rs2 = ps2.executeQuery();
						if (rs2 != null) {
											//System.out.println("entra a estatus reembolso");
											while (rs2.next()) {
								if (rs2.getString("validaReemb").equals("RC")) {
									exist = true;
									//System.out.println("entra a error reembolso");
									txtError.append("Error es un Reembolso/");
								} else if (rs2.getString("validaReemb").equals("ERROR")) {
									//System.out.println("entra a error reembolso");
									txtError.append("No existe registro para aplicar Reembolso/");
								}
							} //cierre while rs2
						}
						if (rs2 != null)
							rs2.close();
						if (ps2 != null)
							ps2.close();

						regPrepagado = validaPrestamoPrepagado(con, rs.getString("prestamo"), rs.getString("fec_venc"),
																			rs.getString("disposicion"), rs.getString("totalvencimiento"),
																			cvePrograma);
						if (regPrepagado) {
							sumaTotalPrepago++;
							montoTotalPrepago = montoTotalPrepago.add(new BigDecimal(rs.getString("totalvencimiento")));

						} else if (!exist) {
							sumaTotalReemb++;
							montoTotalReemb = montoTotalReemb.add(new BigDecimal(rs.getString("totalvencimiento")));
						}


					} //cierre if (estatus R)

					if (txtError.length() > 0) {
									 //System.out.println("si entro a error>>>>"+txtError.toString());
									 sumaTotalError++;
						montoTotalError =
											montoTotalError.add(new BigDecimal(rs.getString("totalvencimiento") != null ?
																						  rs.getString("totalvencimiento") : "0"));
						//}
						ps2 = con.queryPrecompilado(qryError);
						ps2.setString(1, txtError.toString());
						ps2.setString(2, rs.getString("fec_venc"));
						ps2.setLong(3, Long.parseLong(rs.getString("prestamo")));
						ps2.setLong(4, Long.parseLong(rs.getString("cliente")));
						ps2.setBigDecimal(5, new BigDecimal(rs.getString("totalvencimiento")));
						ps2.setLong(6, Long.parseLong(rs.getString("disposicion")));
						ps2.setLong(7, Long.parseLong(cvePrograma)); //FODEA 026-2010 FVR
						ps2.executeUpdate();
						if (rs2 != null)
							rs2.close();
						if (ps2 != null)
							ps2.close();
					}
					/*fin de validacion*/
				} //cierre while de validaciones


				sumaTotalTMP = sumaTotalReemb + sumaTotalPrepago;
				montoTotalTMP = montoTotalTMP.add(montoTotalReemb);
				montoTotalTMP = montoTotalTMP.add(montoTotalPrepago);

				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalPagados));
				lstRegValidados.add(montoTotalPagados.toPlainString());
				lstRegValidados.add("pagados");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalNoPagados));
				lstRegValidados.add(montoTotalNoPagados.toPlainString());
				lstRegValidados.add("no_pagados");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalTMP));
				lstRegValidados.add(montoTotalTMP.toPlainString());
				lstRegValidados.add("tmp");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalReemb));
				lstRegValidados.add(montoTotalReemb.toPlainString());
				lstRegValidados.add("reemb");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalError));
				lstRegValidados.add(montoTotalError.toPlainString());
				lstRegValidados.add("error");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalRec));
				lstRegValidados.add(montoTotalRec.toPlainString());
				lstRegValidados.add("recuperacion");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalNoEncon));
				lstRegValidados.add(montoTotalNoEncon.toPlainString());
				lstRegValidados.add("no_encontrados");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalNoEnv));
				lstRegValidados.add(montoTotalNoEnv.toPlainString());
				lstRegValidados.add("no_enviados");
				lstValidados.add(lstRegValidados);
				/**/
				lstRegValidados = new ArrayList();
				lstRegValidados.add(String.valueOf(sumaTotalPrepago));
				lstRegValidados.add(montoTotalPrepago.toPlainString());
				lstRegValidados.add("prepagos");
				lstValidados.add(lstRegValidados);


			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			return lstValidados;
		} catch (Exception e) {
			realizaTransaccion = false;
			System.out.println("FondoJuniorBean::validaMonitor(error)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(realizaTransaccion);
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}

	public String totalConMovimientos(String operacion, String fechaValor, String clavePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		String totales = "";
		PreparedStatement ps = null;
		double suma = 0;
		double resta = 0;
		double total = 0;

		try {

			con.conexionDB();

			strSQL.append("SELECT " + " nvl(sum(case when cat.CG_OPERACION = 'Suma' then mov.FG_CAPITAL end),0) as suma, " +
							  " nvl(sum(case when cat.CG_OPERACION = 'Resta' then mov.FG_CAPITAL end),0) as resta " +
							  " FROM com_mov_gral_fondo_jr mov, comcat_oper_fondo_jr cat " +
							  " WHERE mov.ig_oper_fondo_jr = cat.ig_oper_fondo_jr ");

			if (!"".equals(clavePrograma) && clavePrograma != null) {
				strSQL.append(" AND mov.ic_programa_fondojr = " + clavePrograma);
			}

			if ((!"".equals(fechaValor) && fechaValor != null)) {
				strSQL.append(" AND mov.DF_FEC_VALOR = TO_DATE ('" + fechaValor + "', 'dd/mm/yyyy') ");
			}

			if ((!"".equals(operacion) && operacion != null)) {
				strSQL.append(" and  mov.IG_OPER_FONDO_JR =  " + operacion);
			}


			strSQL.append(" ORDER BY  mov.CG_OBSERVACIONES");

			log.debug("strSQL.toString()" + strSQL.toString());

			ps = con.queryPrecompilado(strSQL.toString());
			rs = ps.executeQuery();

			while (rs.next()) {

				suma = ((Double.parseDouble(rs.getString("suma"))));
				resta = ((Double.parseDouble(rs.getString("resta"))));
				total = suma - resta;
			}

			totales = Double.toString(total);

			System.out.println("totales" + totales);


			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : totalConMovimientos(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : totalConMovimientos(F) ::..");
		}
		return totales;

	}


	public String generaArchConciliacionZip(FondoJrConciliacionBean form, String rutaFisica, String rutaVirtual,
														 String cvePrograma, String nombreProg) throws NafinException {
		log.info("generaArchConciliacionZip ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String nombreArchivo = null;
		String nombreZip = null;
		CreaArchivo archivo = null;
		int x = 0;

		StringBuffer contenidotxt = new StringBuffer();
		String fec_ultimo_mov = "";
		BigDecimal montoP = new BigDecimal("0.00");
		BigDecimal montoNP = new BigDecimal("0.00");
		BigDecimal montoT = new BigDecimal("0.00");
		BigDecimal montoR = new BigDecimal("0.00");
		BigDecimal montoRF = new BigDecimal("0.00");
		BigDecimal montoRC = new BigDecimal("0.00");
		BigDecimal montoSUM = new BigDecimal("0.00");
		BigDecimal montoRES = new BigDecimal("0.00");
		BigDecimal montoIni = new BigDecimal("0.00");
		BigDecimal montoOtros = new BigDecimal("0.00");
		BigDecimal montoFinal = new BigDecimal("0.00");
		BigDecimal montoPenReem = new BigDecimal("0.00");
		BigDecimal montoCap = new BigDecimal("0.00");
		BigDecimal montoInteres = new BigDecimal("0.00");
		BigDecimal montoSaldo = new BigDecimal("0.00");
		String clavePrograma = cvePrograma;
		//String nombrePrograma = nombreProg;

		try {
			con.conexionDB();
			java.text.SimpleDateFormat fHora = new java.text.SimpleDateFormat("dd-MM-yyyy");
			String fec_Actual = fHora.format(new java.util.Date());

			log.debug("rutaFisica>>>>>" + rutaFisica);
			log.debug("rutaVirtual>>>>" + rutaVirtual);

			PrintWriter salida = null;
			FileWriter fw = new FileWriter(rutaFisica + "conciliacionfjr" + fec_Actual + ".txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			salida = new PrintWriter(bw);

			archivo = new CreaArchivo();
			nombreZip = "conciliacionfjr" + fec_Actual;
			nombreArchivo = nombreZip + ".txt";

			List lstFondoConcentrado = consConciliacionConcentrado(clavePrograma); //FODEA 026 - 2010 ACF
			rs = consConciliacionDetalle(form, con, rs, ps);

			if (lstFondoConcentrado != null && lstFondoConcentrado.size() > 0) {
				montoP = montoP.add((BigDecimal) lstFondoConcentrado.get(0));
				montoNP = montoNP.add((BigDecimal) lstFondoConcentrado.get(1));
				montoT = montoT.add((BigDecimal) lstFondoConcentrado.get(2));
				montoR = montoR.add((BigDecimal) lstFondoConcentrado.get(3));
				montoRF = montoRF.add((BigDecimal) lstFondoConcentrado.get(4));
				montoRC = montoRC.add((BigDecimal) lstFondoConcentrado.get(5));
				montoSUM = montoSUM.add((BigDecimal) lstFondoConcentrado.get(6));
				montoRES = montoRES.add((BigDecimal) lstFondoConcentrado.get(7));
				montoIni = montoIni.add((BigDecimal) lstFondoConcentrado.get(8));
				fec_ultimo_mov = (String) lstFondoConcentrado.get(9);
				montoOtros = montoOtros.add(montoSUM.subtract(montoRES));
				montoFinal =
							  montoFinal.add(((((montoIni.add(montoR)).add(montoRC)).add(montoOtros)).subtract(montoNP)).subtract(montoRF));
				montoPenReem = montoPenReem.add(montoIni.subtract(montoFinal));
			}

			/*----------PARTE DE FONDO CONCENTRADOR---------------*/
			contenidotxt.append("SALDO FONDO JUNIOR AL " + fec_Actual + "\n");
			contenidotxt.append("Fecha Ultimo Movimiento, ");
			contenidotxt.append("Saldo Inicial, ");
			contenidotxt.append("Total Retiros, ");
			contenidotxt.append("Total Desembolsos FIDE, ");
			contenidotxt.append("Desembolsos, ");
			contenidotxt.append("Prepagos Totales, ");
			contenidotxt.append("Otros Movimientos, ");
			contenidotxt.append("Saldo Final, ");
			contenidotxt.append("Retiros Pendientes de Desembolsos \n");

			contenidotxt.append(fec_ultimo_mov + "	");
			contenidotxt.append("$" + Comunes.formatoDecimal(montoIni.toPlainString(), 2, true) + "    ");
			contenidotxt.append("$" + Comunes.formatoDecimal(montoNP.toPlainString(), 2, true) + "    ");
			contenidotxt.append("$" + Comunes.formatoDecimal(montoR.toPlainString(), 2, true) + "    ");
			contenidotxt.append("$" + Comunes.formatoDecimal(montoRF.toPlainString(), 2, true) + "    ");
			contenidotxt.append("$" + Comunes.formatoDecimal(montoT.toPlainString(), 2, true) + "    ");
			contenidotxt.append("$" + Comunes.formatoDecimal(montoOtros.toPlainString(), 2, true) + "    ");
			contenidotxt.append("$" + Comunes.formatoDecimal(montoFinal.toPlainString(), 2, true) + "    ");
			contenidotxt.append("$" + Comunes.formatoDecimal(montoPenReem.toPlainString(), 2, true) + "\n\n");

			/*----------------PARTE DEL DETALLE DEL FONDO CONCENTRADOR--------*/

			//if(lstFondoConcentradoDet!=null && lstFondoConcentradoDet.size()>0){

			if (rs != null) {
				System.out.println("entra a  resulset");
				montoP = new BigDecimal("0.00");
				montoNP = new BigDecimal("0.00");
				montoT = new BigDecimal("0.00");
				montoR = new BigDecimal("0.00");
				montoRF = new BigDecimal("0.00");
				montoRC = new BigDecimal("0.00");
				montoSUM = new BigDecimal("0.00");
				montoRES = new BigDecimal("0.00");
				montoIni = new BigDecimal("0.00");
				montoOtros = new BigDecimal("0.00");
				montoFinal = new BigDecimal("0.00");
				montoPenReem = new BigDecimal("0.00");
				montoCap = new BigDecimal("0.00");
				montoInteres = new BigDecimal("0.00");
				montoSaldo = new BigDecimal("0.00");
				//for(int x=0; x<lstFondoConcentradoDet.size();x++){
				while (rs.next()) {

					List lstFondoConcentradoposicion = new ArrayList();
					lstFondoConcentradoposicion.add(rs.getString("fec_venc") != null ? rs.getString("fec_venc") : "");
					lstFondoConcentradoposicion.add(rs.getString("fec_fide") != null ? rs.getString("fec_fide") : "");
					lstFondoConcentradoposicion.add(rs.getString("amort") != null ? rs.getString("amort") : "");
					lstFondoConcentradoposicion.add(rs.getString("interes") != null ? rs.getString("interes") : "");
					lstFondoConcentradoposicion.add(rs.getString("saldo") != null ? rs.getString("saldo") : "");
					lstFondoConcentradoposicion.add(rs.getString("estatus") != null ? rs.getString("estatus") : "");
					lstFondoConcentradoposicion.add(rs.getString("observaciones") != null ? rs.getString("observaciones") :
															  "");
					lstFondoConcentradoposicion.add(rs.getString("fec_val") != null ? rs.getString("fec_val") : "");
					lstFondoConcentradoposicion.add(rs.getString("usuario") != null ? rs.getString("usuario") : "");
					lstFondoConcentradoposicion.add(rs.getString("prestamo") != null ? rs.getString("prestamo") : "");
					lstFondoConcentradoposicion.add(rs.getString("monto_reembolso") != null ? rs.getString("monto_reembolso") :
															  "");
					lstFondoConcentradoposicion.add(rs.getString("dias_transcurridos_vencimiento") != null ?
															  rs.getString("dias_transcurridos_vencimiento") : "N/A");
					if (x == 0) {
						if (!((String) lstFondoConcentradoposicion.get(4)).equals(""))
							montoIni = montoIni.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
						montoSaldo = montoSaldo.add(montoIni);

						contenidotxt.append("Fecha de Vencimiento, ");
						contenidotxt.append("Fecha Pago FIDE, ");
						contenidotxt.append("Capital, ");
						contenidotxt.append("Interes, ");
						contenidotxt.append("Saldo, ");
						contenidotxt.append("Pagado, ");
						contenidotxt.append("No Pagado, ");
						contenidotxt.append("Desembolsos FIDE, ");
						contenidotxt.append("Desembolsos, ");
						contenidotxt.append("Prepagos Totales, ");
						contenidotxt.append("Otros Movimientos, ");
						contenidotxt.append("Dias Transcurridos de Vencimiento, ");
						contenidotxt.append("Observaciones, ");
						contenidotxt.append("Fecha y Hora de Validaci�n, ");
						contenidotxt.append("Usuario \n");

						/*	contenidotxt.append("Saldo inicial del Fondo:	");
						contenidotxt.append("$"+Comunes.formatoDecimal(((String)lstFondoConcentradoposicion.get(4)),2,true)+"	");
						contenidotxt.append((String)lstFondoConcentradoposicion.get(6)+"\n");
					*/
						//salida.print(contenidotxt.toString());
						//contenidotxt = contenidotxt.delete(0,contenidotxt.length());
					} //else{

					if (!((String) lstFondoConcentradoposicion.get(2)).equals(""))
						montoCap = montoCap.add(new BigDecimal((String) lstFondoConcentradoposicion.get(2)));
					if (!((String) lstFondoConcentradoposicion.get(3)).equals(""))
						montoInteres = montoInteres.add(new BigDecimal((String) lstFondoConcentradoposicion.get(3)));
					if (!((String) lstFondoConcentradoposicion.get(4)).equals(""))
						montoSaldo = montoSaldo.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));

					if (((String) lstFondoConcentradoposicion.get(5)).equals("P"))
						montoP = montoP.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
					if (((String) lstFondoConcentradoposicion.get(5)).equals("NP"))
						montoNP = montoNP.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
					if (((String) lstFondoConcentradoposicion.get(5)).equals("R"))
						montoR = montoR.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
					if (((String) lstFondoConcentradoposicion.get(5)).equals("RF"))
						montoRF = montoRF.add(new BigDecimal((String) lstFondoConcentradoposicion.get(10)));
					if (((String) lstFondoConcentradoposicion.get(5)).equals("T"))
						montoT = montoT.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
					if ((((String) lstFondoConcentradoposicion.get(5)).toUpperCase()).equals("SUMA") ||
						 (((String) lstFondoConcentradoposicion.get(5)).toUpperCase()).equals("RESTA"))
						montoOtros = montoOtros.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));

					contenidotxt.append((String) lstFondoConcentradoposicion.get(0) + "	");
					contenidotxt.append((String) lstFondoConcentradoposicion.get(1) + "	");
					contenidotxt.append((!((String) lstFondoConcentradoposicion.get(2)).equals("") ?
												"$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(2)), 2, true) : "") +
											  "	");
					contenidotxt.append((!((String) lstFondoConcentradoposicion.get(3)).equals("") ?
												"$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(3)), 2, true) : "") +
											  "	");
					contenidotxt.append((!((String) lstFondoConcentradoposicion.get(4)).equals("") ?
												"$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, true) : "") +
											  "	");
					contenidotxt.append((((String) lstFondoConcentradoposicion.get(5)).equals("P") ?
												"$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, true) : "") +
											  "	");
					contenidotxt.append((((String) lstFondoConcentradoposicion.get(5)).equals("NP") ?
												"$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, true) : "") +
											  "	");
					contenidotxt.append((((String) lstFondoConcentradoposicion.get(5)).equals("R") ?
												"$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, true) : "") +
											  "	");
					contenidotxt.append((((String) lstFondoConcentradoposicion.get(5)).equals("RF") ?
												"$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(10)), 2, true) : "") +
											  "	");
					contenidotxt.append((((String) lstFondoConcentradoposicion.get(5)).equals("T") ?
												"$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, true) : "") +
											  "	");
					contenidotxt.append(((((String) lstFondoConcentradoposicion.get(5)).toUpperCase()).equals("SUMA") ||
												(((String) lstFondoConcentradoposicion.get(5)).toUpperCase()).equals("RESTA") ?
												"$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, true) : "") +
											  "	");
					contenidotxt.append((String) lstFondoConcentradoposicion.get(11) + "	");
					contenidotxt.append((String) lstFondoConcentradoposicion.get(6) + "	");
					contenidotxt.append((String) lstFondoConcentradoposicion.get(7) + "	");
					contenidotxt.append((String) lstFondoConcentradoposicion.get(8) + "\n");

					//}//cierre if(x)
					salida.print(contenidotxt.toString());
					contenidotxt = contenidotxt.delete(0, contenidotxt.length());
					x++;

				} //cierre for(detalle)


				contenidotxt.append("Totales:	");
				contenidotxt.append(Comunes.formatoDecimal(montoCap.toPlainString(), 2, true) + "	");
				contenidotxt.append(Comunes.formatoDecimal(montoInteres.toPlainString(), 2, true) + "	");
				contenidotxt.append(Comunes.formatoDecimal(montoSaldo.toPlainString(), 2, true) + "	");
				contenidotxt.append(Comunes.formatoDecimal(montoP.toPlainString(), 2, true) + "	");
				contenidotxt.append(Comunes.formatoDecimal(montoNP.toPlainString(), 2, true) + "	");
				contenidotxt.append(Comunes.formatoDecimal(montoR.toPlainString(), 2, true) + "	");
				contenidotxt.append(Comunes.formatoDecimal(montoRF.toPlainString(), 2, true) + "	");
				contenidotxt.append(Comunes.formatoDecimal(montoT.toPlainString(), 2, true) + "	");
				contenidotxt.append(Comunes.formatoDecimal(montoOtros.toPlainString(), 2, true) + "	");
				contenidotxt.append(" ");

				salida.print(contenidotxt.toString());
				contenidotxt = contenidotxt.delete(0, contenidotxt.length());

			} //cierre if(detalle)
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			salida.close();

			GenerarArchivoZip zip = new GenerarArchivoZip();
			zip.generandoZip(rutaFisica, rutaFisica, nombreZip);
			File f = new File(rutaFisica + nombreArchivo);
			if (f.exists())
				f.delete();

			return nombreZip + ".zip";
		} catch (Exception e) {
			log.info("generaArchConciliacionZip(ERROR) :: " + e.toString());
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("generaArchConciliacionZip(E) ::..");
		}
	} //generaPDF

	/**
	 *  Devuelve la descripcion del detalle de los Dias de Vencimiento en Base de la clave del programa <tt>clavePrograma</tt>
	 *  y la fecha de probable pago <tt>fechaProbablePago</tt>
	 *
	 * @param fechaProbablePago Fecha de Probable Pago con formato <tt>dd/mm/yyyy</tt>
	 * @param clavePrograma Clave del Programa de Fondo Jr <tt>dd/mm/yyyy</tt>
	 *
	 * @return <tt>HashMap</tt> con el detalle de la consulta.
	 *
	 */
	public HashMap getDetalleDiasVencimiento(String clavePrograma, String fechaProbablePago) throws AppException {

		log.info("getDetalleDiasVencimiento(E)");
		HashMap consulta = new HashMap();

		AccesoDB con = new AccesoDB();
		StringBuffer querySentencia = new StringBuffer();
		ResultSet rs = null;
		PreparedStatement ps = null;

		boolean hayRegistros = false;

		try {

			con.conexionDB();
			querySentencia.append(" SELECT " + "    IC_NUM_OPERACION,  " + "    CG_DETALLE         " + " FROM  " +
										 "    COM_CRED_PARAM_DIAS_VENC CCP " + " WHERE  " +
										 "    CCP.IC_PROGRAMA_FONDOJR   =  ?                                   AND   " +
										 "    TO_DATE(?,'DD/MM/YYYY')   >= TRUNC(CCP.DF_FECHA_VMTO_INI    )    AND   " +
										 "    TO_DATE(?,'DD/MM/YYYY')   <  TRUNC(CCP.DF_FECHA_VMTO_FIN+1  )          ");

			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, Integer.parseInt(clavePrograma));
			ps.setString(2, fechaProbablePago);
			ps.setString(3, fechaProbablePago);
			rs = ps.executeQuery();

			if (rs != null && rs.next()) {
				hayRegistros = true;
				consulta.put("NUMERO_OPERACION", rs.getString("IC_NUM_OPERACION"));

				String detalle = rs.getString("CG_DETALLE");
				detalle = detalle == null ? "" : detalle;
				consulta.put("DESCRIPCION", detalle);
			}

		} catch (Exception e) {

			hayRegistros = false;
			log.error("getDetalleDiasVencimiento(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener el detalle de los dias de vencimiento.");

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
				}
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			consulta.put("HAY_DETALLE", String.valueOf(hayRegistros));
			log.info("getDetalleDiasVencimiento(S)");
		}

		return consulta;
	}

	/**  Captura de par�metros para la aplicaci�n del l�mite de vencimiento
	 * Fodea 012-2011  DLHC
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param parametros
	 */
	public String paraDiasVencimiento(List parametros) throws AppException {
		log.info("paraDiasVencimiento(E)");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean ok = true;
		String fechaVenDesde = "", fechaVenA = "", diasVencimiento = "", detalle = "", programa = "", usuarioModifico =
				 "", respuesta = "";
		int seqNEXTVAL = 0;

		try {
			con.conexionDB();

			fechaVenDesde = (String) parametros.get(0);
			fechaVenA = (String) parametros.get(1);
			diasVencimiento = (String) parametros.get(2);
			detalle = (String) parametros.get(3);
			programa = (String) parametros.get(4);
			usuarioModifico = (String) parametros.get(5);


			query.append(" SELECT SEQ_COM_CRED_PARAM_DIAS_VENC.nextval FROM dual ");
			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				seqNEXTVAL = rs.getInt(1);
			}
			rs.close();
			ps.close();
			String operacion = "";

			int valor = String.valueOf(seqNEXTVAL).length();
			if (valor == 1) {
				operacion += "0000";
			}
			if (valor == 2) {
				operacion += "000";
			}
			if (valor == 3) {
				operacion += "00";
			}
			if (valor == 4) {
				operacion += "0";
			}

			operacion += seqNEXTVAL;

			query = new StringBuffer();
			query.append(" insert into  COM_CRED_PARAM_DIAS_VENC ");
			query.append(" ( IC_NUM_OPERACION , DF_FECHA_VMTO_INI , DF_FECHA_VMTO_FIN, IG_DIAS_VENCIMIENTO  ");
			query.append(" , CG_DETALLE , IC_PROGRAMA_FONDOJR, CG_USER_MODIFICA, DF_ULT_MODIFICA ) ");

			query.append(" values('" + operacion + "',TO_DATE('" + fechaVenDesde + "','DD/MM/YYYY'), " + " TO_DATE('" +
							 fechaVenA + "','DD/MM/YYYY') ," + diasVencimiento + "," + "'" + detalle + "'," + programa + ",'" +
							 usuarioModifico + "'," + "Sysdate)");


			System.out.println("query " + query.toString());

			ps = con.queryPrecompilado(query.toString());
			ps.executeUpdate();
			ps.close();

			respuesta = operacion;
			return respuesta;

		} catch (Exception e) {
			ok = false;
			e.printStackTrace();
			throw new AppException("Error al guardar la Parametrizacion de Dias de vencimiento ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("paraDiasVencimiento(S)");
		}
	}


	/**
	 * elimina  el registro seleccionado de la tabla  COM_CRED_PARAM_DIAS_VENC
	 * que corresponde a los par�metros para la aplicaci�n del l�mite de vencimiento
	 * Fodea 012-2011  DLHC
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param lista
	 */
	public String eliminarParaDiasVencidos(List lista) throws AppException {

		log.info("eliminarParaDiasVencidos(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean ok = true;
		String operacion = "", respuesta = "";
		try {
			con.conexionDB();

			for (int i = 0; i < lista.size(); i++) {
				operacion = (String) lista.get(i);

				if (operacion != null) {
					query = new StringBuffer();
					query.append(" DELETE FROM  COM_CRED_PARAM_DIAS_VENC ");
					query.append(" WHERE IC_NUM_OPERACION = ? ");

					ps = con.queryPrecompilado(query.toString());
					ps.setString(1, operacion);
					ps.executeUpdate();
					ps.close();
					respuesta = "E";
				}
			}

			return respuesta;

		} catch (Exception e) {
			ok = false;
			e.printStackTrace();
			throw new AppException("Error al eliminar el numero de operacionde los d�as de vencimiento ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("paraDiasVencimiento(S)");
		}
	}


	/**modifica  el registro seleccionado de la tabla  COM_CRED_PARAM_DIAS_VENC
	 * que corresponde a los par�metros para la aplicaci�n del l�mite de vencimiento
	 * Fodea 012-2011  DLHC
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param parametros
	 */
	public String modificarParaDiasVencidos(List parametros) throws AppException {

		log.info("modificarParaDiasVencidos(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean ok = true;
		String fechaVenDesde = "", fechaVenA = "", diasVencimiento = "", detalle = "", usuarioModifico = "", respuesta =
				 "MO", operacion = "";

		try {
			con.conexionDB();

			fechaVenDesde = (String) parametros.get(0);
			fechaVenA = (String) parametros.get(1);
			diasVencimiento = (String) parametros.get(2);
			detalle = (String) parametros.get(3);
			operacion = (String) parametros.get(4);
			usuarioModifico = (String) parametros.get(5);

			query = new StringBuffer();
			query.append(" UPDATE COM_CRED_PARAM_DIAS_VENC ");
			query.append(" SET  DF_FECHA_VMTO_INI  = TO_DATE('" + fechaVenDesde + "','DD/MM/YYYY') ,");
			query.append(" DF_FECHA_VMTO_FIN  = TO_DATE('" + fechaVenA + "','DD/MM/YYYY') ,");
			query.append(" IG_DIAS_VENCIMIENTO  = " + diasVencimiento + ",");
			query.append(" CG_DETALLE  = '" + detalle + "',");
			query.append(" CG_USER_MODIFICA  = '" + usuarioModifico + "',");
			query.append(" DF_ULT_MODIFICA  = Sysdate ");
			query.append(" WHERE IC_NUM_OPERACION  = '" + operacion + "'");

			ps = con.queryPrecompilado(query.toString());
			ps.executeUpdate();
			ps.close();

			return respuesta;

		} catch (Exception e) {
			ok = false;
			e.printStackTrace();
			throw new AppException("Error al modificar la Parametrizacion de Dias de vencimiento ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("modificarParaDiasVencidos(S)");
		}
	}

	/**
	 * metodo valida si existe conflicto con las fechas de alguna otra parametrizaci�n
	 * Fodea 012-2011  DLHC
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param fechaFin
	 * @param fechaIni
	 */
	public String validaRangoFechas(String fechaIni, String fechaFin, String programa,
											  String noOperacion) throws AppException {
		log.info("validaRangoFechas(E)");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs2 = null;
		PreparedStatement ps2 = null;

		StringBuffer query = new StringBuffer();
		boolean ok = true;
		String respuesta = "";
		List fechas = new ArrayList();
		List fechasxOp = new ArrayList();
		List fechasOX = new ArrayList();
		fechas.add(fechaIni);

		try {
			con.conexionDB();

			//obtengo las fechas dentro del rango de las fecha a capturar.
			query = new StringBuffer();
			query.append(" SELECT to_char(TO_date('" + fechaIni + "', 'DD/MM/YYYY') + ic_numero,'DD/MM/YYYY') AS fecha ");
			query.append(" FROM com_numero ");
			query.append(" WHERE TO_DATE ('" + fechaIni + "', 'DD/MM/YYYY') + ic_numero <= TO_DATE ('" + fechaFin +
							 "', 'DD/MM/YYYY') ");
			//log.debug("query.toString()  "+ query.toString());

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				fechas.add(rs.getString("fecha") == null ? "" : rs.getString("fecha"));
			}
			rs.close();
			ps.close();

			//if(fechas.size()>0){
			//for(int x=0; x<fechas.size(); x++) {
			//String fecha		= (String)fechas.get(x);
			//log.debug("fecha   "+fecha);
			//}
			//}

			//obtengo las fechas que estan insertadas en la tabla
			query = new StringBuffer();
			query.append(" SELECT  to_char (a.DF_FECHA_VMTO_INI, 'DD/MM/YYYY')  as ini,  ");
			query.append(" 				 to_char( a.DF_FECHA_VMTO_FIN, 'DD/MM/YYYY') as fin, ");
			query.append(" 				 IC_NUM_OPERACION AS IC_NUM_OPERACION  ");
			query.append(" FROM COM_CRED_PARAM_DIAS_VENC a ");
			query.append(" WHERE IC_PROGRAMA_FONDOJR =" + programa);
			if (!noOperacion.equals("")) {
				query.append(" AND IC_NUM_OPERACION !='" + noOperacion + "'");
			}

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				List fechasX = new ArrayList();
				fechasX.add(rs.getString("ini") == null ? "" : rs.getString("ini"));
				fechasX.add(rs.getString("fin") == null ? "" : rs.getString("fin"));
				fechasX.add(rs.getString("IC_NUM_OPERACION") == null ? "" : rs.getString("IC_NUM_OPERACION"));
				fechasxOp.add(fechasX);
			}
			rs.close();
			ps.close();


			if (fechasxOp.size() > 0) {
				for (int x = 0; x < fechasxOp.size(); x++) {
					List datos = (ArrayList) fechasxOp.get(x);
					List fechasO = new ArrayList();

					String fechaIniO = (String) datos.get(0);
					String fechaFinO = (String) datos.get(1);
					String operacion = (String) datos.get(2);

					StringBuffer query2 = new StringBuffer();
					query2.append(" SELECT to_char(TO_date('" + fechaIniO +
									  "', 'DD/MM/YYYY') + ic_numero,'DD/MM/YYYY') AS fecha2 ");
					query2.append(" FROM com_numero ");
					query2.append(" WHERE TO_DATE ('" + fechaIniO + "', 'DD/MM/YYYY') + ic_numero <= TO_DATE ('" + fechaFinO +
									  "', 'DD/MM/YYYY') ");

					ps2 = con.queryPrecompilado(query2.toString());
					rs2 = ps2.executeQuery();
					fechasO.add(operacion);
					fechasO.add(fechaIniO);
					while (rs2.next()) {
						fechasO.add(rs2.getString("fecha2") == null ? "" : rs2.getString("fecha2"));
					}
					rs2.close();
					ps2.close();

					fechasOX.add(fechasO);
				}

			}

			if (fechas.size() > 0) {
				for (int x = 0; x < fechas.size(); x++) {
					String fecha = (String) fechas.get(x);

					if (fechasOX.size() > 0) {

						for (int y = 0; y < fechasOX.size(); y++) {
							List datosO = (ArrayList) fechasOX.get(y);
							for (int z = 0; z < datosO.size(); z++) {
								String fechaValidar = (String) datosO.get(z);
								String operacion = (String) datosO.get(0);
								if (z >= 1) {
									if (fechaValidar.equals(fecha)) {
										respuesta += operacion + ",";
									}
								}
							} //for(int z=1; z<datosO.size(); z++) {
						}
					}
				} //for(int x=0; x<fechas.size(); x++) {
			} //if(fechas.size()>0  ){

			if (respuesta.equals("")) {
				respuesta = "N";
			}

			return respuesta;

		} catch (Exception e) {
			ok = false;
			e.printStackTrace();
			throw new AppException("Error al validar el rando fechas de vencimiento ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("validaRangoFechas(S)");
		}
	}

	/**
	 * Obtiene el numero de reembolsos a validar del Monitor o Validacion
	 * Fodea 012-2011  Fabo
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param pantalla  M = monitor  V = Validacion
	 * @param cvePrograma
	 */
	public int numReembTotales(String cvePrograma, String pantalla, String folio_val) throws AppException {

		log.info("numReembTotales(E)");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean ok = true;
		int numReemb = 0;

		try {
			con.conexionDB();

			query = new StringBuffer();

			if ("V".equals(pantalla)) {
				if (folio_val != null && !"".equals(folio_val)) {
					query.append("SELECT  COUNT (1) numreemb " + "  FROM com_pagos_fide_val cpf, comcat_origen_programa prg " +
									 " WHERE cpf.ig_origen = prg.ig_origen_programa " +
									 //"   AND cpf.cs_validacion = 'P' " +
									 "   AND cpf.cg_estatus = 'R' " + "   AND cpf.ic_programa_fondojr = ? " +
									 " AND cpf.ig_folio_val = ? ");
					ps = con.queryPrecompilado(query.toString());
					ps.setLong(1, Long.parseLong(cvePrograma));
					ps.setLong(2, Long.parseLong(folio_val));

				} else {
					query.append("SELECT  COUNT (1) numreemb " + "  FROM com_pagos_fide_val cpf, comcat_origen_programa prg " +
									 " WHERE cpf.ig_origen = prg.ig_origen_programa " + "   AND cpf.cs_validacion = 'P' " +
									 "   AND cpf.cg_estatus = 'R' " + "   AND cpf.ic_programa_fondojr = ? ");
					ps = con.queryPrecompilado(query.toString());
					ps.setLong(1, Long.parseLong(cvePrograma));
				}
			} else {
				if ("".equals(pantalla)) {
					query.append("SELECT  COUNT (1) numreemb " + "    FROM COM_PAGOS_FIDE PF  " + "   WHERE PF.CG_ESTATUS = ?  " +
									 "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
									 "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND pf.CG_ESTATUS_PROC = 'N'  " +
									 "	 AND pf.ic_programa_fondojr = ? " + "   ORDER BY pf.df_registro_fide ");

				} else if ("RembolsosPrepagosTotales".equals(pantalla)) {
					query.append("SELECT COUNT (1) numreemb  " + "  FROM comvis_vencimiento_fide vista,   " +
									 "       com_pagos_fide tmp  " + " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
									 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
									 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
									 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "   AND tmp.CG_ESTATUS_PROC = 'N'  " +
									 " 	 AND tmp.cg_estatus = ? " + "	 AND tmp.ic_programa_fondojr = ? " + "   AND 'RC' != " +
									 "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " + "     									tmp.ig_prestamo, " +
									 "                                tmp.ig_disposicion, " +
									 "                                tmp.fg_totalvencimiento " +
									 "                                )   ");
					//ps.setLong(2, Long.parseLong(folio_val));
				} else if ("Reembolsos".equals(pantalla)) {
					query.append(" SELECT COUNT (1) numreemb  " + "  FROM comvis_vencimiento_fide vista,   " +
									 "       com_pagos_fide tmp  " + " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
									 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
									 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
									 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "   AND tmp.CG_ESTATUS_PROC = 'N'  " +
									 " 	 AND tmp.cg_estatus = ? " + "	 AND tmp.ic_programa_fondojr = ? " +
									 "   AND 'RC' != fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, tmp.ig_prestamo,   " +
									 "                      tmp.ig_disposicion, tmp.fg_totalvencimiento)  ");

				} else if ("NoEncontrados".equals(pantalla)) {
					query.append("SELECT COUNT (1) numreemb " + "       FROM com_pagos_fide tmp      " +
									 "WHERE  tmp.cg_estatus in (?)  " + "   AND tmp.CG_ESTATUS_PROC = 'N'  " + "  AND NOT EXISTS  " + "    ( " +
									 "     SELECT vp.ig_prestamo  " + "              FROM comvis_vencimiento_fide vp                   " +
									 "             WHERE vp.com_fechaprobablepago = tmp.com_fechaprobablepago " +
									 "               AND vp.ig_prestamo = tmp.ig_prestamo " +
									 "               AND vp.ig_cliente  = tmp.ig_cliente " +
									 "               AND vp.ig_disposicion = tmp.ig_disposicion " +
									 "               AND vp.fg_totalvencimiento = tmp.fg_totalvencimiento " + "     ) " +
									 "	  AND tmp.ic_programa_fondojr = ? ");

				} else if ("ConError".equals(pantalla)) {
					query.append("SELECT COUNT (1) numreemb  " + "  FROM comvis_vencimiento_fide vista,   " +
									 "       com_pagos_fide tmp  " + " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
									 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
									 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
									 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "   AND tmp.CG_ESTATUS_PROC = 'N'  " +
									 " 	 AND tmp.cg_estatus in (?) " + "	 AND tmp.ic_programa_fondojr = ? " +
									 " AND tmp.cg_error is not null ");
				}


				ps = con.queryPrecompilado(query.toString());
				ps.setString(1, "R");
				ps.setLong(2, Long.parseLong(cvePrograma));
			}


			//ps = con.queryPrecompilado(query.toString());
			//ps.setLong(1, Long.parseLong(cvePrograma));
			rs = ps.executeQuery();

			if (rs.next()) {
				numReemb = rs.getInt("numreemb");
			}

			rs.close();
			ps.close();

			return numReemb;

		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException("Error al obtener numero de reembolsos a validar ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("numReembTotales(S)");
		}
	}


	/**
	 * Obtiene el totoal de reembolsos a validar o ya validados
	 * Fodea 012-2011  Fabo
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param folio_val
	 * @param estatus_valida
	 * @param cvePrograma
	 */
	public List getTotalesReembValida(String cvePrograma, String estatus_valida, String folio_val) throws AppException {
		log.info("getTotalesReembValida(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean ok = true;
		List lstTotalesReemb = new ArrayList();

		try {
			con.conexionDB();

			query = new StringBuffer();

			if ("P".equals(estatus_valida)) {
				query.append(" SELECT /*+ use_nl(cpf prg) */  " + " count(1) numreemb, " +
								 " sum(cpf.fg_amortizacion) monto_capital, " + " sum(cpf.fg_interes) monto_interes, " +
								 " sum(cpf.fg_totalvencimiento) monto_total " + " FROM com_pagos_fide_val cpf, comcat_origen_programa prg " +
								 " WHERE cpf.ig_origen = prg.ig_origen_programa " + " AND cpf.cs_validacion = 'P' " +
								 " AND cpf.cg_estatus = 'R' " + " AND cpf.ic_programa_fondojr = ? ");
				ps = con.queryPrecompilado(query.toString());
				ps.setLong(1, Long.parseLong(cvePrograma));
			} else { //IG_FOLIO_VAL
				query.append(" SELECT /*+ use_nl(cpf prg) */  " + " count(1) numreemb, " +
								 " sum(cpf.fg_amortizacion) monto_capital, " + " sum(cpf.fg_interes) monto_interes, " +
								 " sum(cpf.fg_totalvencimiento) monto_total " + " FROM com_pagos_fide_val cpf, comcat_origen_programa prg " +
								 " WHERE cpf.ig_origen = prg.ig_origen_programa " + " AND cpf.cg_estatus = 'R' " +
								 " AND cpf.ic_programa_fondojr = ? " + " AND cpf.ig_folio_val = ? ");
				ps = con.queryPrecompilado(query.toString());
				ps.setLong(1, Long.parseLong(cvePrograma));
				ps.setLong(2, Long.parseLong(folio_val));
			}


			rs = ps.executeQuery();

			if (rs.next()) {
				lstTotalesReemb.add(rs.getString("numreemb"));
				lstTotalesReemb.add(rs.getString("monto_capital"));
				lstTotalesReemb.add(rs.getString("monto_interes"));
				lstTotalesReemb.add(rs.getString("monto_total"));
			}

			rs.close();
			ps.close();

			return lstTotalesReemb;

		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException("Error al obtener totales de todos los reembolsos a validar ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("getTotalesReembValida(S)");
		}
	}

	/**
	 * Obtiene los reembolsos en la pantallade validacion para la generacion del zip
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param folio_val
	 * @param estatus_valida
	 * @param cvePrograma
	 */
	//public ResultSet obtenerInfoTxtValidacion(AccesoDB con, ResultSet rs, PreparedStatement ps, String cvePrograma, String estatus_valida, String folio_val) throws AppException{
	private HashMap obtenerInfoTxtValidacion(AccesoDB con, String cvePrograma, String estatus_valida,
														  String folio_val) throws AppException {
		log.info("obtenerInfoTxtValidacion(E)");
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		HashMap hmCursores = new HashMap();

		try {
			query = new StringBuffer();

			if ("P".equals(estatus_valida)) {
				query.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin, " +
								 " TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion, " +
								 " TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide, " + " cpf.ig_prestamo numero_prestamo, " +
								 " cpf.ig_cliente numero_sirac, " + " cpf.cg_nombrecliente nombre_cliente, " +
								 " cpf.ig_disposicion numero_cuota, " + " cpf.fg_amortizacion monto_capital, " +
								 " cpf.fg_interes monto_interes, " + " cpf.fg_totalvencimiento monto_total, " +
								 " DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo, " +
								 " prg.cg_descripcion origen_prestamo, " +
								 " DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion " +
								 " FROM com_pagos_fide_val cpf " + ", comcat_origen_programa prg " +
								 " WHERE cpf.ig_origen = prg.ig_origen_programa " + " AND cpf.cs_validacion = 'P' " +
								 " AND cpf.cg_estatus = 'R' " + " AND cpf.ic_programa_fondojr = ? ");
				ps = con.queryPrecompilado(query.toString());
				ps.setLong(1, Long.parseLong(cvePrograma));
			} else { //IG_FOLIO_VAL
				query.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin, " +
								 " TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion, " +
								 " TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide, " + " cpf.ig_prestamo numero_prestamo, " +
								 " cpf.ig_cliente numero_sirac, " + " cpf.cg_nombrecliente nombre_cliente, " +
								 " cpf.ig_disposicion numero_cuota, " + " cpf.fg_amortizacion monto_capital, " +
								 " cpf.fg_interes monto_interes, " + " cpf.fg_totalvencimiento monto_total, " +
								 " DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo, " +
								 " prg.cg_descripcion origen_prestamo, " +
								 " DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion " +
								 " FROM com_pagos_fide_val cpf " + ", comcat_origen_programa prg " +
								 " WHERE cpf.ig_origen = prg.ig_origen_programa " +
								 //" AND cpf.cs_validacion = '' " +
								 " AND cpf.cg_estatus = 'R' " + " AND cpf.ic_programa_fondojr = ? " +
								 " AND cpf.ig_folio_val = ? ");
				ps = con.queryPrecompilado(query.toString());
				ps.setLong(1, Long.parseLong(cvePrograma));
				ps.setLong(2, Long.parseLong(folio_val));
			}


			rs = ps.executeQuery();

			hmCursores.put("RESULTSET", rs);
			hmCursores.put("PREPAREDSTATEMENT", ps);

			//rs.close();
			//ps.close();

			return hmCursores;

		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException("Error al obtener totales de todos los reembolsos a validar ", e);
		} finally {

			log.info("obtenerInfoTxtValidacion(S)");
		}
	}

	/**
	 * Obtiene detalle del monitor para crear el zip con la info en un txt
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param tipoDetalle
	 * @param cvePrograma
	 * @param ps
	 * @param rs
	 * @param con
	 */
	private HashMap obtenerInfoTxtMonitor(AccesoDB con, String cvePrograma, String tipoDetalle, String fecVenc,
													  String chkDesembolso) throws AppException {
		log.info("obtenerInfoTxtMonitor(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer query = new StringBuffer();
		HashMap hmCursores = new HashMap();

		try {
			query = new StringBuffer();

			if ("".equals(tipoDetalle)) {
				query.append("SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
								 "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
								 "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
								 "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
								 "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
								 "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
								 "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM COM_PAGOS_FIDE PF  " +
								 "   WHERE PF.CG_ESTATUS = ?  " +
								 "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
								 "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND pf.CG_ESTATUS_PROC = 'N'  " +
								 "	 AND pf.ic_programa_fondojr = ? " + "   ORDER BY pf.df_registro_fide ");

			} else if ("NoPagadosRechazados".equals(tipoDetalle)) {
				query.append("SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
								 "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
								 "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
								 "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
								 "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
								 "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide " +
								 //"     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " +
								 "    FROM COM_CRED_NP_RECHAZADOS PF  " + "   WHERE PF.CG_ESTATUS = ?  " + "	 AND pf.ic_programa_fondojr = ? " +
								 "	 AND pf.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
				if (!"S".equals(chkDesembolso))
					query.append("		AND pf.cg_desembolso = ? ");

				query.append("	ORDER BY pf.df_registro_fide ");

			} else if ("RembolsosPrepagosTotales".equals(tipoDetalle)) {
				query.append("SELECT DISTINCT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								 //FODEA 057 - 2009 ACF
					"       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,  " +
								 "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,  " +
								 "       tmp.ig_prestamo AS prestamo,  " + "       tmp.ig_cliente AS numsirac,  " +
								 "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       tmp.ig_disposicion AS numcuota,  " +
								 "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error  " +
								 "  FROM comvis_vencimiento_fide vista,   " + "       com_pagos_fide tmp  " +
								 " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
								 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
								 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
								 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "   AND tmp.CG_ESTATUS_PROC = 'N'  " +
								 " 	 AND tmp.cg_estatus = ? " + "	 AND tmp.ic_programa_fondojr = ? " + "   AND 'RC' != " +
								 "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " + "     									tmp.ig_prestamo, " +
								 "                                tmp.ig_disposicion, " +
								 "                                tmp.fg_totalvencimiento " +
								 "                                )   ");
				//ps.setLong(2, Long.parseLong(folio_val));
			} else if ("Reembolsos".equals(tipoDetalle)) {
				query.append(" SELECT DISTINCT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								 //FODEA 057 - 2009 ACF
					"       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,  " +
								 "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,  " +
								 "       tmp.ig_prestamo AS prestamo,  " + "       tmp.ig_cliente AS numsirac,  " +
								 "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       tmp.ig_disposicion AS numcuota,  " +
								 "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error  " +
								 "  FROM comvis_vencimiento_fide vista,   " + "       com_pagos_fide tmp  " +
								 " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
								 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
								 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
								 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "   AND tmp.CG_ESTATUS_PROC = 'N'  " +
								 " 	 AND tmp.cg_estatus = ? " + "	 AND tmp.ic_programa_fondojr = ? " +
								 "   AND 'RC' != fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, tmp.ig_prestamo,   " +
								 "                      tmp.ig_disposicion, tmp.fg_totalvencimiento)  ");

			} else if ("NoEncontrados".equals(tipoDetalle)) {
				query.append("SELECT TO_CHAR (tmp.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
								 "'' AS fechaoperacion," + "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,   " +
								 "       tmp.IG_PRESTAMO AS prestamo, " + "       tmp.IG_CLIENTE AS numsirac, " + "'' AS cliente, " +
								 "       tmp.IG_DISPOSICION AS numcuota, " + "       tmp.fg_totalvencimiento AS capital_mas_interes " +
								 "       FROM com_pagos_fide tmp      " + "WHERE  tmp.cg_estatus in (?)  " +
								 "   AND tmp.CG_ESTATUS_PROC = 'N'  " + "  AND NOT EXISTS  " + "    ( " + "     SELECT vp.ig_prestamo  " +
								 "              FROM comvis_vencimiento_fide vp                   " +
								 "             WHERE vp.com_fechaprobablepago = tmp.com_fechaprobablepago " +
								 "               AND vp.ig_prestamo = tmp.ig_prestamo " +
								 "               AND vp.ig_cliente  = tmp.ig_cliente " +
								 "               AND vp.ig_disposicion = tmp.ig_disposicion " +
								 "               AND vp.fg_totalvencimiento = tmp.fg_totalvencimiento " + "     ) " +
								 "	  AND tmp.ic_programa_fondojr = ? ");

			} else if ("ConError".equals(tipoDetalle)) {
				query.append("SELECT DISTINCT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								 //FODEA 057 - 2009 ACF
					"       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,  " +
								 "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,  " +
								 "       tmp.ig_prestamo AS prestamo,  " + "       tmp.ig_cliente AS numsirac,  " +
								 "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       tmp.ig_disposicion AS numcuota,  " +
								 "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error  " +
								 "  FROM comvis_vencimiento_fide vista,   " + "       com_pagos_fide tmp  " +
								 " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
								 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
								 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
								 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "   AND tmp.CG_ESTATUS_PROC = 'N'  " +
								 " 	 AND tmp.cg_estatus in (?) " + "	 AND tmp.ic_programa_fondojr = ? " +
								 " AND tmp.cg_error is not null ");
			}


			ps = con.queryPrecompilado(query.toString());
			if ("NoPagadosRechazados".equals(tipoDetalle)) {
				ps.setString(1, "NPR");
				ps.setLong(2, Long.parseLong(cvePrograma));
				ps.setString(3, fecVenc);
				if (!"S".equals(chkDesembolso))
					ps.setString(4, "N");
			} else if ("".equals(tipoDetalle) || "ConError".equals(tipoDetalle) ||
						  "RembolsosPrepagosTotales".equals(tipoDetalle) || "Reembolsos".equals(tipoDetalle)) {
				ps.setString(1, "R");
				ps.setLong(2, Long.parseLong(cvePrograma));

			} else {
				ps.setString(1, "R");
				ps.setLong(2, Long.parseLong(cvePrograma));
				ps.setString(3, fecVenc);
			}
			rs = ps.executeQuery();


			hmCursores.put("RESULTSET", rs);
			hmCursores.put("PREPAREDSTATEMENT", ps);

			return hmCursores;

		} catch (Exception e) {
			e.printStackTrace();
			throw new AppException("Error al obtener totales de todos los reembolsos a validar ", e);
		} finally {

			log.info("obtenerInfoTxtMonitor(S)");
		}
	}

	/**
	 * genera archivo zip para la parte de la validacion cuando son mas de 500 registros
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param folio_val
	 * @param estatus_val
	 * @param nombreProg
	 * @param cvePrograma
	 * @param rutaVirtual
	 * @param rutaFisica
	 */
	public String generaArchivoGralZip(String rutaFisica, String rutaVirtual, String cvePrograma, String nombreProg,
												  String pantalla, String estatus_val, String folio_val, String fecVenc, String chkDesembolso,
												  String estatusDetVal, String estatus2DetVal, String procDetVal) throws NafinException,
																																				 NafinException {
		log.info("generaArchivoGralZip ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String nombreArchivo = null;
		String nombreZip = null;
		CreaArchivo archivo = null;
		int x = 0;

		StringBuffer contenidotxt = new StringBuffer();
		HashMap hmCursores = new HashMap();

		try {
			con.conexionDB();
			//Se crea la instancia del EJB para su utilizaci�n.
			//FondoJuniorHome fondoJuniorHome = (FondoJuniorHome)ServiceLocator.getInstance().getEJBHome("FondoJuniorEJB", FondoJuniorHome.class);
			//FondoJunior fondoJunior = fondoJuniorHome.create();
			java.text.SimpleDateFormat fHora = new java.text.SimpleDateFormat("dd-MM-yyyy");
			String fec_Actual = fHora.format(new java.util.Date());

			log.debug("rutaFisica>>>>>" + rutaFisica);
			log.debug("rutaVirtual>>>>" + rutaVirtual);

			PrintWriter salida = null;
			FileWriter fw = new FileWriter(rutaFisica + "validacionfjr" + fec_Actual + ".txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			salida = new PrintWriter(bw);

			archivo = new CreaArchivo();
			nombreZip = "validacionfjr" + fec_Actual;
			nombreArchivo = nombreZip + ".txt";

			if ("V".equals(pantalla)) {

				hmCursores = obtenerInfoTxtValidacion(con, cvePrograma, estatus_val, folio_val);

				rs = (ResultSet) hmCursores.get("RESULTSET");
				ps = (PreparedStatement) hmCursores.get("PREPAREDSTATEMENT");

				while (rs.next()) {
					if (x == 0) {
						contenidotxt.append("Fecha Amortizacion Nafin| ");
						contenidotxt.append("Fecha de Operacion| ");
						contenidotxt.append("Fecha Pago Cliente FIDE| ");
						contenidotxt.append("Prestamo| ");
						contenidotxt.append("Num. Cliente SIRAC|");
						contenidotxt.append("Cliente|");
						contenidotxt.append("Num. Cuota a Pagar|");
						contenidotxt.append("Capital| ");
						contenidotxt.append("Interes| ");
						contenidotxt.append("Total a Pagar| ");
						contenidotxt.append("Estatus del Registro| ");
						contenidotxt.append("Origen|");
						contenidotxt.append("Estatus de la Validacion| \n");
					}

					contenidotxt.append(rs.getString("fecha_amort_nafin") == null ? "|" :
											  rs.getString("fecha_amort_nafin") + "|");
					contenidotxt.append(rs.getString("fecha_operacion") == null ? "|" :
											  rs.getString("fecha_operacion") + "|");
					contenidotxt.append(rs.getString("fecha_pag_clfide") == null ? "|" :
											  rs.getString("fecha_pag_clfide") + "|");
					contenidotxt.append(rs.getString("numero_prestamo") == null ? "|" :
											  rs.getString("numero_prestamo") + "|");
					contenidotxt.append(rs.getString("numero_sirac") == null ? "|" : rs.getString("numero_sirac") + "|");
					contenidotxt.append(rs.getString("nombre_cliente") == null ? "|" : rs.getString("nombre_cliente") + "|");
					contenidotxt.append(rs.getString("numero_cuota") == null ? "|" : rs.getString("numero_cuota") + "|");
					contenidotxt.append(rs.getString("monto_capital") == null ? "|" : rs.getString("monto_capital") + "|");
					contenidotxt.append(rs.getString("monto_interes") == null ? "|" : rs.getString("monto_interes") + "|");
					contenidotxt.append(rs.getString("monto_total") == null ? "|" : rs.getString("monto_total") + "|");
					contenidotxt.append(rs.getString("estatus_prestamo") == null ? "|" :
											  rs.getString("estatus_prestamo") + "|");
					contenidotxt.append(rs.getString("origen_prestamo") == null ? "|" :
											  rs.getString("origen_prestamo") + "|");
					contenidotxt.append(rs.getString("estatus_validacion") == null ? "|" :
											  (rs.getString("estatus_validacion") + "|"));
					contenidotxt.append("\n");

					salida.print(contenidotxt.toString());
					contenidotxt = contenidotxt.delete(0, contenidotxt.length());
					x++;
				}
				rs.close();
				ps.close();
			} else {
				if ("M".equals(pantalla)) {
					hmCursores = obtenerInfoTxtMonitorIni(con, estatus_val, fecVenc, cvePrograma);
					rs = (ResultSet) hmCursores.get("RESULTSET");
					ps = (PreparedStatement) hmCursores.get("PREPAREDSTATEMENT");

				} else if (!"M".equals(pantalla) && !"".equals(pantalla)) {

					//rs = detalleDeCreditosTxtJS(con,rs,ps,cvePrograma,pantalla, fecVenc, chkDesembolso);
					hmCursores = detalleDeCreditosTxtJS(con, pantalla, estatusDetVal, estatus2DetVal, procDetVal, fecVenc,
																	cvePrograma);

					rs = (ResultSet) hmCursores.get("RESULTSET");
					ps = (PreparedStatement) hmCursores.get("PREPAREDSTATEMENT");

				} else {
					hmCursores = obtenerInfoTxtMonitor(con, cvePrograma, pantalla, fecVenc,
																  chkDesembolso); //FODEA 026 - 2010 ACF
					rs = (ResultSet) hmCursores.get("RESULTSET");
					ps = (PreparedStatement) hmCursores.get("PREPAREDSTATEMENT");
				}
				while (rs.next()) {
					if (x == 0) {
						if ("".equals(pantalla) || "NoPagadosRechazados".equals(pantalla) || "M".equals(pantalla)) {
							contenidotxt.append("Fecha de Vencimiento Nafin|");
							contenidotxt.append("No. Prestamo|");
							contenidotxt.append("No. Cliente|");
							contenidotxt.append("Num. Disposicion|");
							contenidotxt.append("Monto Amortizacion|");
							contenidotxt.append("Monto Interes|");
							contenidotxt.append("Total Venc|");
							contenidotxt.append("Fecha Pago Cliente|");
							contenidotxt.append("Estatus|");
							contenidotxt.append("Fecha Periodo Fin|");
							contenidotxt.append("Numero Docto|");
							contenidotxt.append("Fecha Registro FIDE|");
							contenidotxt.append("\n");

						} else {
							contenidotxt.append("Fecha Amortizacion Nafin| ");
							contenidotxt.append("Fecha de Operacion| ");
							contenidotxt.append("Fecha Pago Cliente FIDE| ");
							contenidotxt.append("Prestamo| ");
							contenidotxt.append("Num. Cliente SIRAC|");
							contenidotxt.append("Cliente|");
							contenidotxt.append("Num. Cuota a Pagar|");
							contenidotxt.append("Capital mas Interes| ");
							if ("ConError".equals(pantalla))
								contenidotxt.append("Error|");
							contenidotxt.append("\n");
						}
					}
					//List verdetalle = new ArrayList();
					if ("".equals(pantalla) || "NoPagadosRechazados".equals(pantalla) ||
						 "M".equals(pantalla)) {
									 /*0*/contenidotxt.append(rs.getString("fecha_venc") == null ? "|" :
																	  rs.getString("fecha_venc") + "|");
									 /*1*/contenidotxt.append(rs.getString("ig_prestamo") == null ? "|" :
																	  rs.getString("ig_prestamo") + "|");
						/*2*/contenidotxt.append(rs.getString("ig_cliente") == null ? "|" : rs.getString("ig_cliente") + "|");
						/*3*/contenidotxt.append(rs.getString("ig_disposicion") == null ? "|" :
														 rs.getString("ig_disposicion") + "|");
						/*4*/contenidotxt.append(rs.getString("fg_amortizacion") == null ? "|" :
														 rs.getString("fg_amortizacion") + "|");
						/*5*/contenidotxt.append(rs.getString("fg_interes") == null ? "|" : rs.getString("fg_interes") + "|");
						/*6*/contenidotxt.append(rs.getString("fg_totalvencimiento") == null ? "|" :
														 rs.getString("fg_totalvencimiento") + "|");
						/*7*/contenidotxt.append(rs.getString("df_pago_cliente") == null ? "|" :
														 rs.getString("df_pago_cliente") + "|");
						/*8*/contenidotxt.append(rs.getString("cg_estatus") == null ? "|" : rs.getString("cg_estatus") + "|");
						/*9*/contenidotxt.append(rs.getString("df_periodofin") == null ? "|" :
														 rs.getString("df_periodofin") + "|");
						/*10*/contenidotxt.append(rs.getString("ig_numero_docto") == null ? "|" :
														  rs.getString("ig_numero_docto") + "|");
						/*11*/contenidotxt.append(rs.getString("df_registro_fide") == null ? "|" :
														  rs.getString("df_registro_fide") + "|");
						contenidotxt.append("\n");
					} else {
						contenidotxt.append(rs.getString(1) == null ? "|" : rs.getString(1) + "|");
						contenidotxt.append(rs.getString(2) == null ? "|" : rs.getString(2) + "|");
						contenidotxt.append(rs.getString(3) == null ? "|" : rs.getString(3) + "|");
						contenidotxt.append(rs.getString(4) == null ? "|" : rs.getString(4) + "|");
						contenidotxt.append(rs.getString(5) == null ? "|" : rs.getString(5) + "|");
						contenidotxt.append(rs.getString(6) == null ? "|" : rs.getString(6) + "|");
						contenidotxt.append(rs.getString(7) == null ? "|" : rs.getString(7) + "|");
						contenidotxt.append(rs.getString(8) == null ? "|" : rs.getString(8) + "|");
						if ("ConError".equals(pantalla))
							contenidotxt.append(rs.getString(9) == null ? "|" : rs.getString(9) + "|");
						contenidotxt.append("\n");
					}


					salida.print(contenidotxt.toString());
					contenidotxt = contenidotxt.delete(0, contenidotxt.length());
					x++;
				}
				rs.close();
				ps.close();
			}

			salida.close();

			GenerarArchivoZip zip = new GenerarArchivoZip();
			zip.generandoZip(rutaFisica, rutaFisica, nombreZip);
			File f = new File(rutaFisica + nombreArchivo);
			if (f.exists())
				f.delete();

			return nombreZip + ".zip";
		} catch (Exception e) {
			log.info("generaArchivoGralZip(ERROR) :: " + e.toString());
			e.printStackTrace();
			throw new AppException("Error al generar zip de validacion ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("generaArchivoGralZip(E) ::..");
		}
	} //generaPDF
	//Fodea 012-2011 (S)

	/**
	 * Obtiene Consulta general de registros(No Pagados Rechazados) cargados por el FIDE para Fondo Junior (F027-2011)
	 * @throws com.netro.exception.NafinException
	 * @return List lstConsMonitor
	 * @param fec_fin - fecha de vencimiento final
	 * @param fec_ini - fecha de vencimiento inicial
	 */
	public List consultaMonitorNPR(String fec_ini, String fec_fin, String chkDesembolso,
											 String cvePrograma) throws AppException {
		log.info("FondoJuniorBean::consultaMonitorNPR(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		List lstConsMonitor = new ArrayList();
		List regMonitor = null;
		String qrySentence = "";
		String condFechas2 = ""; //FODEA 026-2010 FVR

		try {
			con.conexionDB();

			/*---------------- SE OBTIENE REGISTROS DE BITACORA-------------------*/
			if (fec_ini != null && !fec_ini.equals("") && fec_fin != null && !fec_fin.equals("")) {

				condFechas2 =
							  " AND PF.COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " +
							  " AND PF.COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY') ";
			}
			qrySentence =
						"SELECT  tmonitor.orden as morden, " + "		tmonitor.estatus as mestatus, " +
						"		tmonitor.fecha_venc as mfecha_venc, " + "        tmonitor.ultimo_reg as multimo_reg, " +
						"        tmonitor.total_reg as mtotal_reg, " + "        tmonitor.monto_tot as mmonto_tot, " +
						"        tmonitor.folio as mfolio " + "FROM ( " + "SELECT   '1' orden, " + "		 pf.cg_estatus estatus, " +
						"		 TO_CHAR (pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
						"         TO_CHAR (MAX (pf.df_registro_fide), 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " +
						"         COUNT (pf.cg_estatus) total_reg,  " + "         SUM (pf.fg_totalvencimiento) monto_tot, " +
						"         null folio " + "    FROM com_cred_np_rechazados pf " + "   WHERE pf.cg_estatus IN ('NPR') " +
						"		AND pf.ic_programa_fondojr = ? ";
			if (!"S".equals(chkDesembolso))
				qrySentence += "	AND pf.cg_desembolso = ? ";

			qrySentence +=
						condFechas2 + "GROUP BY pf.com_fechaprobablepago, pf.cg_estatus  " + ") tmonitor " +
						"order by tmonitor.orden, tmonitor.fecha_venc ";

			log.info("qrySentence>>>>>>" + qrySentence);
			ps = con.queryPrecompilado(qrySentence);
			int i = 0;

			ps.setLong(++i, Long.parseLong(cvePrograma));
			if (!"S".equals(chkDesembolso))
				ps.setString(++i, "N");

			if (!condFechas2.equals("")) {
				ps.setString(++i, fec_ini);
				ps.setString(++i, fec_fin);
			}


			rs = ps.executeQuery();
			while (rs != null && rs.next()) {
				regMonitor = new ArrayList();
				regMonitor.add(rs.getString("MESTATUS"));
				regMonitor.add(rs.getString("MFECHA_VENC"));
				regMonitor.add(rs.getString("MULTIMO_REG"));
				regMonitor.add(rs.getString("MFOLIO"));
				regMonitor.add(rs.getString("MTOTAL_REG"));
				regMonitor.add(rs.getString("MMONTO_TOT"));
				lstConsMonitor.add(regMonitor);
			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();


			log.info("FondoJuniorBean::consultaMonitorNPR(S)");
			return lstConsMonitor;
		} catch (Exception e) {
			log.info("FondoJuniorBean::consultaMonitorNPR(Error)");
			e.printStackTrace();
			throw new AppException("Error en consulta de venc No Pagados Rechazados", e);
		} finally {
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}


	/**
	 * Fodea 027-2011
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param clavePrograma
	 * @param fechaMovimiento
	 */
	public List ControlFondoJR(String fechaMovimiento, String clavePrograma) throws AppException {
		log.info("ControlFondoJR(E)");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean ok = true;
		List resultado = new ArrayList();
		List datos1 = new ArrayList();
		List datos2 = new ArrayList();
		List datos3 = new ArrayList();
		List datos4 = new ArrayList();
		List datos5 = new ArrayList();
		List datos6 = new ArrayList();
		List datos7 = new ArrayList();
		log.debug("fechaMovimiento---" + fechaMovimiento);

		try {
			con.conexionDB();

			//	Aportaciones
			query = new StringBuffer();
			query.append(" select sum(fg_capital) as suma,  count(*) as  numero  ");
			query.append(" from COM_MOV_GRAL_FONDO_JR m, ");
			query.append(" comcat_oper_fondo_jr c ");
			query.append(" where  m.IG_OPER_FONDO_JR  = c.IG_OPER_FONDO_JR ");
			query.append(" and c.CG_OPERACION IN('suma','Suma','SUMA') ");
			query.append(" and c.CG_DESCRIPCION not in ('intereses', 'INTERESES', 'Intereses') ");
			query.append(" and m.IC_PROGRAMA_FONDOJR = " + clavePrograma);

			if (!fechaMovimiento.equals("")) {
				query.append("AND m.DF_FEC_VALOR < TO_DATE('" + fechaMovimiento + "','dd/mm/yyyy') +1  ");
			}

			log.debug("Aportaciones  " + query.toString());

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				String suma = rs.getString("suma") == null ? "0" : rs.getString("suma");
				String numero = rs.getString("numero") == null ? "0" : rs.getString("numero");
				datos1.add("Aportaciones");
				datos1.add(suma);
				datos1.add(numero);
			}
			rs.close();
			ps.close();


			//Cancelaciones
			query = new StringBuffer();
			query.append(" select sum(fg_capital) as suma, count(*)  as numero  ");
			query.append(" from COM_MOV_GRAL_FONDO_JR m, ");
			query.append(" comcat_oper_fondo_jr c ");
			query.append(" where  m.IG_OPER_FONDO_JR  = c.IG_OPER_FONDO_JR ");
			query.append(" and c.CG_OPERACION in('resta','Resta', 'RESTA') ");
			query.append(" and m.IC_PROGRAMA_FONDOJR = " + clavePrograma);
			if (!fechaMovimiento.equals("")) {
				query.append(" and m.DF_FEC_VALOR < TO_DATE('" + fechaMovimiento + "','dd/mm/yyyy') +1 ");
			}

			log.debug("Cancelaciones  " + query.toString());

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				String suma = rs.getString("suma") == null ? "0" : rs.getString("suma");
				String numero = rs.getString("numero") == null ? "0" : rs.getString("numero");
				datos2.add("Cancelaciones");
				datos2.add(suma);
				datos2.add(numero);
			}
			rs.close();
			ps.close();


			//Intereses
			query = new StringBuffer();
			query.append(" select sum(fg_capital) as suma, count(*) as numero  ");
			query.append(" from COM_MOV_GRAL_FONDO_JR m, ");
			query.append(" comcat_oper_fondo_jr c ");
			query.append(" where  m.IG_OPER_FONDO_JR  = c.IG_OPER_FONDO_JR ");
			query.append(" and c.CG_OPERACION IN('suma','Suma', 'SUMA') ");
			query.append(" and c.CG_DESCRIPCION in ('intereses', 'INTERESES', 'Intereses') ");
			query.append(" and m.IC_PROGRAMA_FONDOJR = " + clavePrograma);
			if (!fechaMovimiento.equals("")) {
				query.append(" and m.DF_FEC_VALOR < TO_DATE('" + fechaMovimiento + "','dd/mm/yyyy') +1 ");
			}
			log.debug("intereses  " + query.toString());

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				String suma = rs.getString("suma") == null ? "0" : rs.getString("suma");
				String numero = rs.getString("numero") == null ? "0" : rs.getString("numero");
				datos3.add("Intereses");
				datos3.add(suma);
				datos3.add(numero);
			}
			rs.close();
			ps.close();


			//	Desembolsos
			query = new StringBuffer();
			query.append(" Select Sum(FG_TOTALVENCIMIENTO) as suma, count(*) as numero   ");
			query.append(" from COM_PAGOS_FIDE_VAL ");
			query.append(" where  CG_ESTATUS ='RF' ");
			query.append(" and CS_VALIDACION= 'A' ");
			query.append(" and IC_PROGRAMA_FONDOJR = " + clavePrograma);
			if (!fechaMovimiento.equals("")) {
				query.append(" and DF_VALIDACION < TO_DATE('" + fechaMovimiento + "','dd/mm/yyyy') +1 ");

			}
			log.debug("Desembolsos  " + query.toString());

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				String suma = rs.getString("suma") == null ? "0" : rs.getString("suma");
				String numero = rs.getString("numero") == null ? "0" : rs.getString("numero");
				datos4.add("Desembolsos");
				datos4.add(suma);
				datos4.add(numero);
			}
			rs.close();
			ps.close();


			//Reembolsos
			query = new StringBuffer();
			query.append(" Select Sum(FG_TOTALVENCIMIENTO) as suma , count(*) as numero ");
			query.append(" from COM_PAGOS_FIDE_VAL ");
			query.append("  where  CG_ESTATUS ='R' ");
			query.append(" and CS_VALIDACION= 'A' ");
			query.append(" and IC_PROGRAMA_FONDOJR = " + clavePrograma);
			if (!fechaMovimiento.equals("")) {
				query.append(" and DF_VALIDACION < TO_DATE('" + fechaMovimiento + "','dd/mm/yyyy') + 1 ");
			}
			log.debug("Reembolsos  " + query.toString());

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				String suma = rs.getString("suma") == null ? "0" : rs.getString("suma");
				String numero = rs.getString("numero") == null ? "0" : rs.getString("numero");
				datos5.add("Reembolsos");
				datos5.add(suma);
				datos5.add(numero);
			}
			rs.close();
			ps.close();


			//No Pagados
			query = new StringBuffer();
			query.append(" SELECT SUM(fg_totalvencimiento) AS suma ,  COUNT(*)  numero ");
			query.append(" FROM com_pagos_fide_val ");
			query.append("  WHERE  cg_estatus ='NP' ");
			query.append(" AND cs_validacion= 'A' ");
			query.append(" AND ic_programa_fondojr = " + clavePrograma);
			if (!fechaMovimiento.equals("")) {
				query.append(" AND com_fechaprobablepago < TO_DATE('" + fechaMovimiento + "','dd/mm/yyyy') + 1 ");
			}
			log.debug("No pagados  " + query.toString());

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				String suma = rs.getString("suma") == null ? "0" : rs.getString("suma");
				String numero = rs.getString("numero") == null ? "0" : rs.getString("numero");
				datos6.add("No Pagados");
				datos6.add(suma);
				datos6.add(numero);
			}
			rs.close();
			ps.close();

			//Prepagos
			query = new StringBuffer();
			query.append(" Select Sum(FG_TOTALVENCIMIENTO) as suma , count(*) as numero ");
			query.append(" from COM_PAGOS_FIDE_VAL ");
			query.append("  where  CG_ESTATUS ='T' ");
			query.append(" and CS_VALIDACION= 'A' ");
			query.append(" and IC_PROGRAMA_FONDOJR = " + clavePrograma);
			if (!fechaMovimiento.equals("")) {
				query.append(" and com_fechaprobablepago < TO_DATE('" + fechaMovimiento + "','dd/mm/yyyy') + 1 ");
			}
			log.debug("Prepagos  " + query.toString());

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				String suma = rs.getString("suma") == null ? "0" : rs.getString("suma");
				String numero = rs.getString("numero") == null ? "0" : rs.getString("numero");
				datos7.add("Prepagos");
				datos7.add(suma);
				datos7.add(numero);
			}
			rs.close();
			ps.close();


			resultado.add(datos1);
			resultado.add(datos2);
			resultado.add(datos3);
			resultado.add(datos4);
			resultado.add(datos5);
			resultado.add(datos6);
			resultado.add(datos7);

			int hayRegistros = 0;
			if (resultado.size() > 0) {
				for (int x = 0; x < resultado.size(); x++) {
					List datos = (ArrayList) resultado.get(x);
					String info = (String) datos.get(2);
					if (!info.equals("0")) {
						hayRegistros++;
					}
				}
			}
			if (hayRegistros == 0) {
				resultado = new ArrayList();
			}

			return resultado;

		} catch (Exception e) {
			ok = false;
			e.printStackTrace();
			throw new AppException("Error al ControlFondoJR ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("ControlFondoJR(S)");
		}
	}

	/**
	 * Devuelve un <tt>HashMap</tt> con las claves y descripciones de cada
	 * uno de los origenes de los programas registrados en la tabla
	 * COMCAT_ORIGEN_PROGRAMA
	 */
	public HashMap getCatalogoOrigenPrograma() throws AppException {

		log.info("getCatalogoOrigenPrograma(E)");

		AccesoDB con = new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer();
		ResultSet rs = null;
		PreparedStatement ps = null;

		HashMap catalogo = new HashMap();

		try {

			con.conexionDB();

			qrySentencia.append("SELECT                              " + "	IG_ORIGEN_PROGRAMA AS CLAVE,      " +
									  "	CG_DESCRIPCION     AS DESCRIPCION " + "FROM                                " +
									  "	COMCAT_ORIGEN_PROGRAMA            ");
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();

			while (rs != null && rs.next()) {

				String clave = rs.getString("CLAVE");
				String descripcion = rs.getString("DESCRIPCION");

				descripcion = (descripcion == null || descripcion.trim().equals("")) ? null : descripcion;

				catalogo.put(clave, descripcion);

			}

		} catch (Exception e) {
			log.error("getCatalogoOrigenPrograma(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener el Catalogo \"Origen Programa\"");
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception e) {
				}
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getCatalogoOrigenPrograma(S)");
		}

		return catalogo;

	}

	/**
	 *
	 * Devuelve la descripcion del origen del programa cuya clave se especifica en el
	 * parametro <tt>claveOrigenPrograma</tt>
	 *
	 * @param clavePrograma <tt>String</tt> con la clave del origen del programa.
	 * @param catalogo <tt>HashMap</tt> con el catalogo de todos los origenes
	 *                 de los programas.
	 * @return <tt>String</tt> con la descripcion de los programas.
	 *
	 */
	public String getDescripcionOrigenPrograma(String claveOrigenPrograma, HashMap catalogo) {

		log.info("getDescripcionOrigenPrograma(E)");
		if (claveOrigenPrograma == null || catalogo == null)
			return "";

		String descripcion = (String) catalogo.get(claveOrigenPrograma);
		descripcion = claveOrigenPrograma == null || descripcion.trim().equals("") ? "" : descripcion;

		log.info("getDescripcionOrigenPrograma(S)");
		return descripcion;

	}

	private String setAmortRechazada(AccesoDB con, String prestamo, String fecVenc, String disposicion,
												String totalvencimiento, String cvePrograma) throws Exception {
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		try {

			String statusp = "N";
			int rdisposicion = 0;
			String qry =
						" SELECT DECODE (COUNT (1), 0, 0, MIN (a.ig_disposicion)) disposicion " +
						"  FROM com_pagos_fide_val a " + " WHERE a.cg_estatus = 'NP' " + "   AND a.cs_validacion = 'A' " +
						"   AND a.ig_prestamo = ? " + "   AND a.ic_programa_fondojr = ? " + "   AND NOT EXISTS ( " +
						"          SELECT 1 " + "            FROM com_pagos_fide_val b " +
						"           WHERE b.com_fechaprobablepago = a.com_fechaprobablepago " +
						"             AND b.ig_disposicion = a.ig_disposicion " +
						"             AND b.fg_totalvencimiento = a.fg_totalvencimiento " +
						"             AND b.ig_prestamo = a.ig_prestamo " +
						"             AND b.ic_programa_fondojr = a.ic_programa_fondojr  " + "             AND b.cg_estatus = 'R' " +
						"             AND a.cs_validacion = 'A') ";

			ps2 = con.queryPrecompilado(qry);
			ps2.setLong(1, Long.parseLong(prestamo));
			ps2.setLong(2, Long.parseLong(cvePrograma));
			rs2 = ps2.executeQuery();
			if (rs2 != null && rs2.next()) {
				rdisposicion = rs2.getInt("disposicion");
			}
			if (rs2 != null)
				rs2.close();
			if (ps2 != null)
				ps2.close();

			int diasVencimiento = 0;
			int diasVencidos = 0;
			boolean siParametro = false;
			if (rdisposicion > 0) {
				qry = "SELECT a.ig_dias_vencimiento as dias_vencimiento , " +
							  "       (TRUNC (SYSDATE) - b.com_fechaprobablepago) as dias_vencidos " +
							  "  FROM com_cred_param_dias_venc a, com_pagos_fide_val b " + " WHERE b.ig_prestamo = ? " +
							  "   AND b.ig_disposicion = ? " + "   AND b.ic_programa_fondojr = ? " + "   AND b.cg_estatus = 'NP' " +
							  "   AND b.cs_validacion = 'A' " + "   AND b.com_fechaprobablepago >= a.df_fecha_vmto_ini " +
							  "   AND b.com_fechaprobablepago < a.df_fecha_vmto_fin + 1 ";
				ps2 = con.queryPrecompilado(qry);
				ps2.setLong(1, Long.parseLong(prestamo));
				ps2.setInt(2, rdisposicion);
				ps2.setLong(3, Long.parseLong(cvePrograma));
				rs2 = ps2.executeQuery();
				if (rs2 != null && rs2.next()) {
					siParametro = true;
					diasVencimiento = rs2.getInt("dias_vencimiento");
					diasVencidos = rs2.getInt("dias_vencidos");
				}
				if (rs2 != null)
					rs2.close();
				if (ps2 != null)
					ps2.close();

			} else {
				qry = "SELECT a.ig_dias_vencimiento as dias_vencimiento , " +
							  "       (TRUNC (SYSDATE) - to_date(?,'dd/mm/yyyy')) as dias_vencidos " + "  FROM com_cred_param_dias_venc a " +
							  " WHERE to_date(?,'dd/mm/yyyy') >= a.df_fecha_vmto_ini " +
							  "   AND to_date(?,'dd/mm/yyyy') < a.df_fecha_vmto_fin + 1 ";
				ps2 = con.queryPrecompilado(qry);
				ps2.setString(1, fecVenc);
				ps2.setString(2, fecVenc);
				ps2.setString(3, fecVenc);
				rs2 = ps2.executeQuery();
				if (rs2 != null && rs2.next()) {
					siParametro = true;
					diasVencimiento = rs2.getInt("dias_vencimiento");
					diasVencidos = rs2.getInt("dias_vencidos");
				}
				if (rs2 != null)
					rs2.close();
				if (ps2 != null)
					ps2.close();
			} //fin if(rdisposicion>0)

			if ((diasVencimiento - diasVencidos) < 0 && siParametro) {
				qry = " UPDATE com_pagos_fide " + "   SET cg_estatus_proc = ? " +
							  " WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " + "   AND ig_prestamo = ? " +
							  "   AND fg_totalvencimiento = ? " + "   AND ig_disposicion = ? " + "   AND cg_estatus_proc = ? " +
							  "   AND ic_programa_fondojr = ? ";
				ps2 = con.queryPrecompilado(qry);
				ps2.setString(1, "R");
				ps2.setString(2, fecVenc);
				ps2.setLong(3, Long.parseLong(prestamo));
				ps2.setDouble(4, Double.parseDouble(totalvencimiento));
				ps2.setLong(5, Long.parseLong(disposicion));
				ps2.setString(6, "N");
				ps2.setLong(7, Long.parseLong(cvePrograma));
				ps2.executeUpdate();
				ps2.close();

				statusp = "R";

			} //fin if( (diasVencimiento-diasVencidos)< 0 )

			return statusp;

		} catch (Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}

	}


	public List obtenerReembolsosCarga(String proceso_carga, String cvePrograma,
												  String disposiciones) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List reembolsos_temp = new ArrayList();

		System.out.println("..:: FondoJuniorBean : obtenerReembolsosCarga(I) ::..");
		try {
			con.conexionDB();

			strSQL.append(" SELECT TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_movimiento,");
			strSQL.append(" cpf.ig_prestamo numero_prestamo,");
			strSQL.append(" cpf.ig_cliente num_cliente_sirac,");
			strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
			strSQL.append(" cpf.fg_totalvencimiento monto_reembolso,");
			strSQL.append(" cop.cg_descripcion origen,");
			strSQL.append(" crf.cg_observaciones observaciones,  ");
			strSQL.append(" cpf.ig_disposicion disposicion   ");
			strSQL.append(" FROM com_pagos_fide_val cpf,");
			strSQL.append(" comcat_origen_programa cop, ");
			strSQL.append(" com_tmp_reemb_x_finiquito crf ");
			strSQL.append(" WHERE cpf.ig_origen = cop.ig_origen_programa");
			strSQL.append(" AND cpf.ig_prestamo = crf.ig_prestamo");
			strSQL.append(" AND cpf.ig_proc_desembolso = crf.ic_reemb_x_finiquito");
			strSQL.append(" AND crf.ic_reemb_x_finiquito = ?");
			strSQL.append(" AND cpf.cg_estatus = ?");
			strSQL.append(" AND cpf.ic_programa_fondojr = ? "); //FODEA 026-2010 FVR
			//strSQL.append(" and cpf.ig_disposicion  in ( "+disposiciones +")"); //Fodea 027-2011
			strSQL.append(" ORDER BY numero_prestamo, disposicion  ");

			//varBind.add(new Integer(proceso_carga));
			//varBind.add("RF");
			//varBind.add(new Long(cvePrograma));//FODEA 026-2010 FVR

			log.debug("strSQL.toString() " + strSQL.toString());
			log.debug("varBind " + varBind);
			log.debug("proceso_carga " + proceso_carga);

			pst = con.queryPrecompilado(strSQL.toString());
			pst.setLong(1, Long.parseLong(proceso_carga));
			pst.setString(2, "RF");
			pst.setLong(3, Long.parseLong(cvePrograma));
			//pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				List reembolso = new ArrayList();
				reembolso.add(rst.getString(1) == null ? "" : rst.getString(1));
				reembolso.add(rst.getString(2) == null ? "" : rst.getString(2));
				reembolso.add(rst.getString(3) == null ? "" : rst.getString(3));
				reembolso.add(rst.getString(4) == null ? "" : rst.getString(4));
				reembolso.add(rst.getString(5) == null ? "" : rst.getString(5));
				reembolso.add(rst.getString(6) == null ? "" : rst.getString(6));
				reembolso.add(rst.getString(7) == null ? "" : rst.getString(7));
				reembolso.add(rst.getString("disposicion") == null ? "" : rst.getString("disposicion"));
				reembolsos_temp.add(reembolso);
			}

			rst.close();
			pst.close();

		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : obtenerReembolsosCarga(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : obtenerReembolsosCarga(F) ::..");
		}
		return reembolsos_temp;
	}


	public boolean existeReembolso(String numero_prestamo, String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean existe_reembolso = true;
		String ig_prestamo_pf = "";
		System.out.println("..:: FondoJuniorBean : existeReembolso(I) ::..");

		try {
			con.conexionDB();

			strSQL = new StringBuffer();
			varBind = new ArrayList();
			strSQL.append(" SELECT DISTINCT ig_prestamo,");
			strSQL.append(" ig_cliente");
			strSQL.append(" FROM com_pagos_fide_val cpf");
			strSQL.append(" WHERE ig_prestamo = ?");
			strSQL.append(" AND ic_programa_fondojr =  ? ");

			varBind.add(new Long(numero_prestamo));
			varBind.add(new Long(cvePrograma));
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			System.out.println("strSQL.toString() " + strSQL.toString());
			System.out.println("varBind " + varBind);

			while (rst.next()) {
				ig_prestamo_pf = rst.getString(1) == null ? "" : rst.getString(1);
			}
			pst.close();
			rst.close();
			System.out.println("ig_prestamo_pf " + ig_prestamo_pf);
			if (ig_prestamo_pf.equals("")) {
				existe_reembolso = false;
			} else {
				existe_reembolso = true;
			}

			System.out.println("existe_reembolso " + existe_reembolso);


		} catch (Exception e) {
			System.out.println("..:: FondoJuniorBean : existeReembolso(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("..:: FondoJuniorBean : existeReembolso(F) ::..");
		}
		return existe_reembolso;
	}

	/**
	 * Metodo para la carga temporal del archivo a ser porcesado para la Modificacion(Ajuste)
	 * al Fondo Junior
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param rutaArchivo
	 */
	public String cargaDatosTmpAjusteFondoJr(String rutaArchivo, String tipoModif) throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		String lineaf = "";
		String numeroProceso = "";
		VectorTokenizer vtd = null;
		Vector vecdet = null;
		boolean commit = true;
		int contregistros = 0;
		int numLinea = 1;

		try {
			con.conexionDB();

			query = "SELECT SEQ_COMTMP_AJUSTE_FONDOJR.NEXTVAL numProc FROM DUAL";
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if (rs != null && rs.next()) {
				numeroProceso = rs.getString("numProc");
			}
			rs.close();
			ps.close();

			query = " INSERT INTO comtmp_ajuste_fondojr " +
						"            (ic_numero_proceso, ic_numero_linea, ig_prestamo, ig_disposicion, " +
				"             fg_totalvencimiento, com_fechaprobablepago, fg_amortizacion, fg_interes " +
				"            ) " +
				"     VALUES (?, ?, ?, ?, " +
				"             ?, ?, ?, ? " +
				"            ) ";


			java.io.File f = new java.io.File(rutaArchivo);
			BufferedReader br	= new BufferedReader(new InputStreamReader(new FileInputStream(f),"ISO-8859-1"));
			
			String prestamo	="";
			String cuota		="";
			String totalVenc	="";
			String fechaVenc	="";
			String montoCapital	="";
			String montoInteres	="";
			
			while ((lineaf=br.readLine())!=null){
				contregistros++;
				vtd		= new VectorTokenizer(lineaf,"|");
				vecdet = vtd.getValuesVector();

				prestamo	=(vecdet.size()>=1)?vecdet.get(0).toString().trim():"";
				if(!"F".equals(tipoModif)){
					cuota		=(vecdet.size()>=2)?vecdet.get(1).toString().trim().toUpperCase():"";
					totalVenc	=(vecdet.size()>=3)?vecdet.get(2).toString().trim().toUpperCase():"";
					fechaVenc	=(vecdet.size()>=4)?vecdet.get(3).toString().trim().toUpperCase():"";
				}else{
					totalVenc = "";
					montoCapital =(vecdet.size()>=2)?vecdet.get(1).toString().trim().toUpperCase():"";
					montoInteres =(vecdet.size()>=3)?vecdet.get(2).toString().trim().toUpperCase():"";
					cuota	=(vecdet.size()>=4)?vecdet.get(3).toString().trim().toUpperCase():"";
					fechaVenc =(vecdet.size()>=5)?vecdet.get(4).toString().trim().toUpperCase():"";
					
					if( Comunes.esDecimalPositivo(montoCapital)){
						totalVenc = montoCapital;
					}
					if( Comunes.esDecimalPositivo(montoInteres) ){
						if(!"".equals(totalVenc)){
							totalVenc = (new BigDecimal(totalVenc).add(new BigDecimal(montoInteres))).toString();
						}else{
							totalVenc = montoInteres;
						}
					}
				}
				

				prestamo  =Comunes.quitaComitasSimplesyDobles(prestamo);
				cuota		 =Comunes.quitaComitasSimplesyDobles(cuota);
				totalVenc =Comunes.quitaComitasSimplesyDobles(totalVenc);
				fechaVenc =Comunes.quitaComitasSimplesyDobles(fechaVenc);
				montoCapital =Comunes.quitaComitasSimplesyDobles(montoCapital);
				montoInteres =Comunes.quitaComitasSimplesyDobles(montoInteres);

				prestamo	 =((prestamo.length()>12)?prestamo.substring(0,13):prestamo);
				cuota		 =((cuota.length()>5)?cuota.substring(0,6):cuota);
				totalVenc =((totalVenc.length()>20)?totalVenc.substring(0,21):totalVenc);
				fechaVenc =((fechaVenc.length()>10)?fechaVenc.substring(0,11):fechaVenc);
				montoCapital =((montoCapital.length()>20)?montoCapital.substring(0,21):montoCapital);
				montoInteres =((montoInteres.length()>20)?montoInteres.substring(0,21):montoInteres);

				ps = con.queryPrecompilado(query);
				ps.setLong(1, Long.parseLong(numeroProceso));
				ps.setInt(2, numLinea++);
				ps.setString(3, prestamo);
				ps.setString(4, cuota);
				ps.setString(5, totalVenc);
				ps.setString(6, fechaVenc);
				ps.setString(7, montoCapital);
				ps.setString(8, montoInteres);

				ps.executeUpdate();
				ps.close();

			}

			return numeroProceso;

		} catch (Throwable t) {
			commit = false;
			throw new AppException("Error al cargar informacion temporal de venc a modificar en Fondo Jr. ", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}


	public void validaDatosTmpAjusteFondoJr(int numeroProceso, int numeroLinea, String tipoMod,
														 String cvePrograma) throws AppException {
		log.info("validaDatosTmpAjusteFondoJr(E)::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strbErrores = new StringBuffer();
		StringBuffer strSQL = new StringBuffer();
		boolean lbOK = true;

		String strPrestamo = "";
		String strCuota = "";
		String strTotalVenc = "";
		String strFecVenc = "";
		String strMontoCapital	="";
		String strMontoInteres	="";

		// LEER REGISTRO
		try {
			con.conexionDB();

			strSQL.append("SELECT cf.ig_prestamo prestamo, cf.ig_disposicion disposicion, cf.fg_totalvencimiento totalvenc,  ");
			strSQL.append("       cf.com_fechaprobablepago fechavenc, cf.fg_amortizacion capital, cf.fg_interes interes  ");
			strSQL.append("  FROM comtmp_ajuste_fondojr cf  ");
			strSQL.append(" WHERE cf.ic_numero_proceso = ? AND cf.ic_numero_linea = ?  ");

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setLong(1, Long.parseLong(String.valueOf(numeroProceso)));
			ps.setLong(2, Long.parseLong(String.valueOf(numeroLinea)));
			rs = ps.executeQuery();

			if (rs.next()) {
				strPrestamo = (rs.getString("prestamo") != null) ? rs.getString("prestamo") : "";
				strCuota = (rs.getString("disposicion") != null) ? rs.getString("disposicion") : "";
				strTotalVenc = (rs.getString("totalvenc") != null) ? rs.getString("totalvenc") : "";
				strFecVenc = (rs.getString("fechavenc") != null) ? rs.getString("fechavenc") : "";
				strMontoCapital		= (rs.getString("capital")  != null )?rs.getString("capital"):"";
				strMontoInteres		= (rs.getString("interes")  != null )?rs.getString("interes"):"";
			}
			rs.close();
			ps.close();

			// VALIDAR DATOS
			// 0. Verificar que la linea proporcionada no este vacia
			int numTotalCaracteres = 0;
			numTotalCaracteres += strPrestamo.length();
			numTotalCaracteres += strCuota.length();
			numTotalCaracteres += strTotalVenc.length();
			numTotalCaracteres += strMontoCapital.length();
			numTotalCaracteres += strMontoInteres.length();

			if (numTotalCaracteres == 0) {
				strbErrores.append("No hay datos,");
			} else { // La linea no esta vacia
				//log.debug("Linea no esta vacia");
				// 1. Verificar que los campos del registro no excedan la logitud m�xima permitida
				if(strPrestamo.length()>12)	strbErrores.append("El \"Numero de Prestamo\" excede la logitud m�xima permitida,");
				if(strCuota.length()>5)			strbErrores.append("El \"Numero de Cuota\" excede la logitud m�xima permitida,");
				if(strTotalVenc.length()>20)	strbErrores.append("El \"Monto Total del Venc.\" excede la logitud m�xima permitida,");
				if(strFecVenc.length()>10)		strbErrores.append("La \"Fecha de Venc.\" excede la logitud m�xima permitida,");
				if(strMontoCapital.length()>20)	strbErrores.append("El \"Monto de Capital\" excede la logitud m�xima permitida,");
				if(strMontoInteres.length()>20)	strbErrores.append("El \"Monto de Interes\" excede la logitud m�xima permitida,");

				// 3. Validar campos obligatorios
				if(strPrestamo.equals(""))	strbErrores.append("El \"Numero de Prestamo\" es requerido,");
				if(strCuota.equals("") && !"F".equals(tipoMod))			strbErrores.append("El \"Numero de Cuota\" es requerido,");
				if(strTotalVenc.equals("") && !"F".equals(tipoMod))	strbErrores.append("El \"Monto Total del Venc.\" es requerido,");
				if(strFecVenc.equals("") && !"F".equals(tipoMod))		strbErrores.append("La \"Fecha de Venc.\" es requerido,");
				if(strMontoCapital.equals("") && "F".equals(tipoMod))		strbErrores.append("El \"Monto de Capital\" es requerido,");
				if(strMontoInteres.equals("") && "F".equals(tipoMod))		strbErrores.append("El \"Monto de Interes\" es requerido,");

				// 6. Validar el tipo de caracteres en los campos
				//	if (!strApPaterno.equals("")    	  && !Comunes.esAlfabetico(strApPaterno," ������������.-"))	 	  	strbErrores.append("El \"Apellido Paterno\" tiene caracteres no permitidos,");

				// 7. Validar el tipo de valor en los campos (contenido)
				if (!strPrestamo.equals("") && !Comunes.esNumeroEnteroPositivo(strPrestamo) ) strbErrores.append("El campo: \"Prestamo\" no es numero entero positivo,");
				if (!strCuota.equals("") && !Comunes.esNumeroEnteroPositivo(strCuota) ) strbErrores.append("El campo: \"Numero de Cuota\" no es numero entero positivo,");
				if (!strTotalVenc.equals("") && !Comunes.esDecimalPositivo(strTotalVenc) ) strbErrores.append("El campo: \"Monto Total Venc.\" no es un dato numerico,");
				if (!strFecVenc.equals("") && !Comunes.esFechaValida(strFecVenc,"dd/mm/yyyy") ) strbErrores.append("El campo: \"Fecha de Venc.\" tiene un formato incorrecto,");
				if (!strMontoCapital.equals("") && !Comunes.esDecimalPositivo(strMontoCapital) ) strbErrores.append("El campo: \"Monto de Capital\" no es un dato numerico,");
				if (!strMontoInteres.equals("") && !Comunes.esDecimalPositivo(strMontoInteres) ) strbErrores.append("El campo: \"Monto de Interes\" no es un dato numerico,");

				// 7. Validar que el registro sea unico
				strSQL = new StringBuffer();
				if (strbErrores.length() <= 0) {
					boolean entroAlCicloPreviamente = false;

					strSQL.append("SELECT  ");
					strSQL.append("IC_NUMERO_LINEA  ");
					strSQL.append("FROM  ");
					strSQL.append(" COMTMP_AJUSTE_FONDOJR  ");
					strSQL.append("WHERE  ");
					strSQL.append("IG_PRESTAMO = ? AND  ");
					if(!"F".equals(tipoMod)){
						strSQL.append("IG_DISPOSICION = ? AND  ");
					}
					strSQL.append("IC_NUMERO_PROCESO = ? AND  ");
					strSQL.append("IC_NUMERO_LINEA != ? AND  ");
					strSQL.append("ROWNUM = 1 ");

					int p=0;
					ps = con.queryPrecompilado(strSQL.toString());
					ps.setString(++p,strPrestamo);
					if(!"F".equals(tipoMod)){
						ps.setString(++p,strCuota);
					}
					ps.setLong(++p,Long.parseLong(String.valueOf(numeroProceso)));
					ps.setLong(++p,Long.parseLong(String.valueOf(numeroLinea)));

					rs = ps.executeQuery();

					while (rs != null && rs.next()) {
						if (!entroAlCicloPreviamente) {
							strbErrores.append("\"El registro se encuentra repetido en la(s) linea(s): ");
							entroAlCicloPreviamente = true;
						}
						strbErrores.append(rs.getString("IC_NUMERO_LINEA") + ", ");
					}
					rs.close();
					ps.close();

					if (entroAlCicloPreviamente) {
						//log.debug("Regustro repetdio en el archivo");
						// Quitar la ultima coma y espacio agregados
						strbErrores.setLength(strbErrores.length() - 2);
						// Agregar delimitador de campo
						strbErrores.append("\",");
					}
				}
				//se realiza validacion dependiendo de la modificacion
				strSQL = new StringBuffer();
				int regEncontrados = 0;
				if (strbErrores.length() <= 0) {
					if ("A".equals(tipoMod)) { //OPCION DE NO PAGADO A NP RECHAZADO
						//log.debug("Tipo de Modificacion (A) ");
						strSQL.append("SELECT COUNT (*)  ");
						strSQL.append("  FROM com_pagos_fide_val  ");
						strSQL.append(" WHERE ig_prestamo = ?  ");
						strSQL.append("   AND ig_disposicion = ?  ");
						strSQL.append("   AND fg_totalvencimiento = ?  ");
						strSQL.append("   AND com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy')  ");
						strSQL.append("   AND cg_estatus = ?  ");
						strSQL.append("   AND cs_validacion != ?  ");
						strSQL.append("	 AND ic_programa_fondojr = ?  ");


						ps = con.queryPrecompilado(strSQL.toString());
						ps.setLong(1, Long.parseLong(strPrestamo));
						ps.setLong(2, Long.parseLong(strCuota));
						ps.setBigDecimal(3, new BigDecimal(strTotalVenc));
						ps.setString(4, strFecVenc);
						ps.setString(5, "NP");
						ps.setString(6, "R");
						ps.setLong(7, Long.parseLong(cvePrograma));
						rs = ps.executeQuery();

						if (rs != null && rs.next()) {
							regEncontrados = rs.getInt(1);
							if (regEncontrados < 1)
								strbErrores.append("El vencimiento no existe,");
						}
						rs.close();
						ps.close();

					} else if ("B".equals(tipoMod)) { //OPCION DE REEMBOLSO A DESEMBOLSO
						//log.debug("Tipo de Modificacion (B) ");
						strSQL.append(" SELECT COUNT (*)  ");
						strSQL.append("  FROM com_pagos_fide_val  ");
						strSQL.append(" WHERE ig_prestamo = ?  ");
						strSQL.append("   AND ig_disposicion = ?  ");
						strSQL.append("   AND fg_totalvencimiento = ?  ");
						strSQL.append("   AND com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy')  ");
						strSQL.append("   AND cg_estatus in (?, ?)  ");
						strSQL.append("   AND cs_validacion != ?  ");
						strSQL.append("	 AND ic_programa_fondojr = ?  ");


						ps = con.queryPrecompilado(strSQL.toString());
						ps.setLong(1, Long.parseLong(strPrestamo));
						ps.setLong(2, Long.parseLong(strCuota));
						ps.setBigDecimal(3, new BigDecimal(strTotalVenc));
						ps.setString(4, strFecVenc);
						ps.setString(5, "NP");
						ps.setString(6, "R");
						ps.setString(7, "R");
						ps.setLong(8, Long.parseLong(cvePrograma));
						rs = ps.executeQuery();

						if (rs != null && rs.next()) {
							regEncontrados = rs.getInt(1);
							if (regEncontrados < 1)
								strbErrores.append("El vencimiento no existe,");
							else if (regEncontrados != 2)
								strbErrores.append("El vencimiento no ha sido reembolsado,");
						}
						rs.close();
						ps.close();

					} else if ("C".equals(tipoMod)) { //OPCION DE DESEMBOLSO A REEMBOLSO
						//log.debug("Tipo de Modificacion (C) ");
						strSQL.append(" SELECT COUNT (*)  ");
						strSQL.append("  FROM com_pagos_fide_val  ");
						strSQL.append(" WHERE ig_prestamo = ?  ");
						strSQL.append("   AND ig_disposicion = ?  ");
						strSQL.append("   AND fg_totalvencimiento = ?  ");
						strSQL.append("   AND com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy')  ");
						strSQL.append("   AND cg_estatus in (?, ?)  ");
						strSQL.append("   AND cs_validacion != ?  ");
						strSQL.append("	 AND ic_programa_fondojr = ?  ");


						ps = con.queryPrecompilado(strSQL.toString());
						ps.setLong(1, Long.parseLong(strPrestamo));
						ps.setLong(2, Long.parseLong(strCuota));
						ps.setBigDecimal(3, new BigDecimal(strTotalVenc));
						ps.setString(4, strFecVenc);
						ps.setString(5, "NP");
						ps.setString(6, "RF");
						ps.setString(7, "R");
						ps.setLong(8, Long.parseLong(cvePrograma));
						rs = ps.executeQuery();

						if (rs != null && rs.next()) {
							regEncontrados = rs.getInt(1);
							if (regEncontrados < 1)
								strbErrores.append("El vencimiento no existe,");
							else if (regEncontrados != 2)
								strbErrores.append("El vencimiento no ha sido desembolsado,");
						}
						rs.close();
						ps.close();

					}else if("F".equals(tipoMod)){//OPCION DE NO PAGADO A NP RECHAZADO
						//log.debug("Tipo de Modificacion (A) ");
						strSQL.append("SELECT COUNT (*)  ");
						strSQL.append("  FROM com_pagos_fide_val  ");
						strSQL.append(" WHERE ig_prestamo = ?  ");
						strSQL.append("   AND cg_estatus = ?  ");
						strSQL.append("   AND cs_validacion != ?  ");
						strSQL.append("	 AND ic_programa_fondojr = ?  ");


						ps = con.queryPrecompilado(strSQL.toString());
						ps.setLong(1,Long.parseLong(strPrestamo));
						ps.setString(2,"T");
						ps.setString(3,"R");
						ps.setLong(4,Long.parseLong(cvePrograma));
						rs = ps.executeQuery();

						if(rs!=null && rs.next()){
							regEncontrados = rs.getInt(1);
							if(regEncontrados < 1 )		strbErrores.append("El vencimiento no existe,");
						}
						rs.close();
						ps.close();

					}
				}

			}

			// INSERTAR RESULTADO DE LA VALIDACION EN EL REGISTRO CORRESPONDIENTE
			if (strbErrores.length() > 0) {
						//log.debug("Tipo de Modificacion (A) ");
						
						// Construir cadena de errores
						StringBuffer stringBufferAux = new StringBuffer();
				stringBufferAux.append(strbErrores.toString());
				// Copiar contenido del StringBuffer Auxiliar
				strbErrores.setLength(0);
				strbErrores.append(stringBufferAux.toString());
				// Recortar el mensaje de error si excede los 4000 caracteres
				if (strbErrores.length() > 4000) {
					strbErrores.setLength(3996);
					strbErrores.append("...");
					// Agregar codigo que repare las lineas cortadas
				}

				strSQL = new StringBuffer();
				strSQL.append("UPDATE COMTMP_AJUSTE_FONDOJR ");
				strSQL.append("   SET CG_MENSAJES_ERROR = ?, ");
				strSQL.append("       CS_VALIDO = ? ");
				strSQL.append(" WHERE IC_NUMERO_PROCESO = ? AND IC_NUMERO_LINEA = ? ");


				ps = con.queryPrecompilado(strSQL.toString());
				ps.setString(1, strbErrores.toString());
				ps.setString(2, "N");
				ps.setLong(3, Long.parseLong(String.valueOf(numeroProceso)));
				ps.setLong(4, Long.parseLong(String.valueOf(numeroLinea)));
				ps.executeUpdate();
				ps.close();

				stringBufferAux.setLength(0);
			}


			strbErrores.setLength(0);

		} catch (Exception e) {
			lbOK = false;
			throw new AppException("Error en validaDatosTmpAjusteFondoJr()::..", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("validaDatosTmpAjusteFondoJr(S)::..");
		}

	}

	public List getResumenValidacion(String numProc) throws AppException {
		log.info("getResumenValidacion(E):..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstTotales = new ArrayList();
		String query = "";
		BigInteger bTotalRegC = new BigInteger("0"); //correctos
		BigInteger bTotalRegI = new BigInteger("0"); //incorrectos
		BigDecimal bMontoRegC = new BigDecimal("0.0"); //correctos
		BigDecimal bMontoRegI = new BigDecimal("0.0"); //incorrectos


		try {
			con.conectarDB();

			query = " SELECT   fg_totalvencimiento, cs_valido " + "  FROM comtmp_ajuste_fondojr " +
						"   WHERE ic_numero_proceso = ? ";
			//"	GROUP BY cs_valido ";

			ps = con.queryPrecompilado(query);
			ps.setLong(1, Long.parseLong(numProc));
			rs = ps.executeQuery();

			while (rs != null && rs.next()) {
				if ("N".equals(rs.getString("cs_valido"))) {
					bTotalRegI = bTotalRegI.add(new BigInteger("1"));
					if (Comunes.esDecimalPositivo(rs.getString("fg_totalvencimiento")))
						bMontoRegI = bMontoRegI.add(new BigDecimal(rs.getString("fg_totalvencimiento")));
				}
				if ("S".equals(rs.getString("cs_valido"))) {
					bTotalRegC = bTotalRegC.add(new BigInteger("1"));
					if (Comunes.esDecimalPositivo(rs.getString("fg_totalvencimiento")))
						bMontoRegC = bMontoRegC.add(new BigDecimal(rs.getString("fg_totalvencimiento")));
				}
			}
			rs.close();
			ps.close();

			HashMap mpTotales = new HashMap();
			mpTotales.put("CSVALIDO", "S");
			mpTotales.put("TOTALREG", bTotalRegC.toString());
			mpTotales.put("MONTOREG", bMontoRegC.toPlainString());
			mpTotales.put("NUMPROC", numProc);
			lstTotales.add(mpTotales);

			mpTotales = new HashMap();
			mpTotales.put("CSVALIDO", "N");
			mpTotales.put("TOTALREG", bTotalRegI.toString());
			mpTotales.put("MONTOREG", bMontoRegI.toPlainString());
			mpTotales.put("NUMPROC", numProc);
			lstTotales.add(mpTotales);

			return lstTotales;

		} catch (Throwable t) {
			log.info("getResumenValidacion(Error):..");
			t.printStackTrace();
			throw new AppException("Error al obtener resumen de validacion de carga de Venc...", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getResumenValidacion(S):..");
		}
	}

	public HashMap realizaAjusteFondoJr(String numProc, String tipoModifi, String cvePrograma, String iNoUsuario,
													String strNombreUsuario) throws AppException {
		log.info("realizaAjusteFondoJr(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		String query = "";
		String folio = "";
		int totalReg = 0;
		int totalRegC = 0;
		int totalRegI = 0;
		HashMap mpDataRes = new HashMap();
		boolean commit = true;
		try {
			con.conexionDB();

			String prestamo = "";
			String disposicion = "";
			String totalVenc = "";
			String fecVenc = "";
			String montoCapital = "";
			String montoInteres = "";

			String nombreCliente = "";


			query = " select seq_ajuste_fondojr.nextval folio from dual ";
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if (rs != null && rs.next()) {
				folio = rs.getString("folio");
			}
			rs.close();
			ps.close();

			query = " SELECT ig_prestamo prestamo, ig_disposicion disposicion,  " +
					"       fg_totalvencimiento totalvenc, com_fechaprobablepago fecvenc, " +
					"       fg_amortizacion capital, fg_interes interes " +
					"  FROM comtmp_ajuste_fondojr  " +
					" WHERE ic_numero_proceso = ? " +
					" AND cs_valido = ? ";

			ps = con.queryPrecompilado(query);
			ps.setLong(1, Long.parseLong(numProc));
			ps.setString(2, "S");
			rs = ps.executeQuery();

			while (rs != null && rs.next()) {
				prestamo = rs.getString("prestamo");
				disposicion = rs.getString("disposicion");
				totalVenc = rs.getString("totalvenc");
				fecVenc = rs.getString("fecvenc");
				montoCapital = rs.getString("capital");
				montoInteres = rs.getString("interes");

				log.debug("fecVenc==" + fecVenc);
				log.debug("prestamo==" + prestamo);
				log.debug("disposicion==" + disposicion);
				log.debug("totalVenc==" + totalVenc);
				log.debug("montoCapital=="+montoCapital);
				log.debug("montoInteres=="+montoInteres);

				/*query = "SELECT TO_CHAR (com_fechaprobablepago, 'dd/mm/yyyy') fecvenc, " +
					"       ig_prestamo prestamo, ig_cliente cliente, ig_disposicion disposicion, " +
					"       fg_amortizacion montoamort, fg_interes montointeres, " +
					"       fg_totalvencimiento totalvenc, df_pago_cliente fecpagocliente, " +
					"       cg_estatus estatus, ig_origen origen, df_periodofin fecperiodofin, " +
					"       ig_numero_docto numdocto, df_registro_fide fecregfide, " +
					"       cg_observaciones observaciones, df_fecha_carga feccarga, " +
					"       cg_usuario usuario, ig_folio folio " +
					"  FROM com_pagos_fide_val " +
					" WHERE ig_prestamo = ? " +
					"   AND ig_disposicion = ? " +
					"   AND fg_totalvencimento = ? " +
					"   AND com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') ";*/


				if ("A".equals(tipoModifi)) { //MODIFICACION DE NP o R o RF a NO PAGADOS RECHAZADOS

					query = " DELETE com_cred_np_rechazados " +
							"  WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
							"    AND ig_prestamo = ? " +
							"    AND ig_disposicion = ? " +
									 "    AND fg_totalvencimiento = ? ";

					ps2 = con.queryPrecompilado(query);
					ps2.setString(1, fecVenc);
					ps2.setLong(2, Long.parseLong(prestamo));
					ps2.setLong(3, Long.parseLong(disposicion));
					ps2.setBigDecimal(4, new BigDecimal(totalVenc));
					ps2.executeUpdate();
					ps2.close();


					query = "INSERT INTO com_cred_np_rechazados " +
									 "            (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion, " +
									 "             fg_amortizacion, fg_interes, fg_totalvencimiento, " +
									 "             df_pago_cliente, cg_estatus, ig_origen, df_periodofin, " +
									 "             ig_numero_docto, df_registro_fide, ic_programa_fondojr, ig_folio, ig_folio_ajuste) " +
									 "   (SELECT com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion, " +
									 "           fg_amortizacion, fg_interes, fg_totalvencimiento, df_pago_cliente, " +
									 "           'NPR', ig_origen, df_periodofin, ig_numero_docto, " +
					"           df_registro_fide, ic_programa_fondojr, ig_folio, ? " +
					"      FROM com_pagos_fide_val " +
					"     WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
					"       AND ig_prestamo = ? " +
					"       AND ig_disposicion = ? " +
					"       AND fg_totalvencimiento = ? " +
					"       AND cg_estatus = ? " +
					"       AND ic_programa_fondojr = ? " +
					"		  AND rownum = 1) ";

					log.debug("query==" + query);

					ps2 = con.queryPrecompilado(query);
					ps2.setLong(1, Long.parseLong(folio));
					ps2.setString(2, fecVenc);
					ps2.setLong(3, Long.parseLong(prestamo));
					ps2.setLong(4, Long.parseLong(disposicion));
					ps2.setBigDecimal(5, new BigDecimal(totalVenc));
					ps2.setString(6, "NP");
					ps2.setLong(7, Long.parseLong(cvePrograma));
					ps2.executeUpdate();
					ps2.close();


					query = "DELETE com_pagos_fide_val " +
					"	WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
					"  AND ig_prestamo = ? " +
					"  AND ig_disposicion = ? " +
					"  AND fg_totalvencimiento = ? " +
					"  AND ic_programa_fondojr = ? " +
					"	AND cg_estatus in (?, ?, ?) ";

					log.debug("query==" + query);

					ps2 = con.queryPrecompilado(query);
					ps2.setString(1, fecVenc);
					ps2.setLong(2, Long.parseLong(prestamo));
					ps2.setLong(3, Long.parseLong(disposicion));
					ps2.setBigDecimal(4, new BigDecimal(totalVenc));
					ps2.setLong(5, Long.parseLong(cvePrograma));
					ps2.setString(6, "NP");
					ps2.setString(7, "R");
					ps2.setString(8, "RF");
					ps2.executeUpdate();
					ps2.close();


				}
				if ("B".equals(tipoModifi)) { //MODIFICACION DE REEMBOLSO A DESEMBOLSO
					query = "UPDATE com_pagos_fide_val " +
					"   SET df_pago_cliente = NULL, " +
					"       ig_numero_docto = NULL, " +
					"       cg_observaciones = 'Modificacion Fondo Jr', " +
					"       cg_estatus = 'RF', " +
					"		  ig_folio_ajuste = ? " +
					" WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
					"   AND ig_prestamo = ? " +
					"   AND ig_disposicion = ? " +
					"   AND fg_totalvencimiento = ? " +
					"   AND cg_estatus = ? " +
					"   AND ic_programa_fondojr = ? ";

					log.debug("query==" + query);

					ps2 = con.queryPrecompilado(query);
					ps2.setLong(1, Long.parseLong(folio));
					ps2.setString(2, fecVenc);
					ps2.setLong(3, Long.parseLong(prestamo));
					ps2.setLong(4, Long.parseLong(disposicion));
					ps2.setBigDecimal(5, new BigDecimal(totalVenc));
					ps2.setString(6, "R");
					ps2.setLong(7, Long.parseLong(cvePrograma));
					ps2.executeUpdate();
					ps2.close();


				}
				if ("C".equals(tipoModifi)) { //MODIFICACION DE DESEMBOLSO A REEMBOLSO
					query = "UPDATE com_pagos_fide_val " +
					"   SET ig_proc_desembolso = NULL, " +
					"       cg_observaciones = 'Modificacion Fondo Jr ', " +
					"       cg_estatus = 'R', " +
					"		  ig_folio_ajuste = ? " +
					" WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
					"   AND ig_prestamo = ? " +
					"   AND ig_disposicion = ? " +
					"   AND fg_totalvencimiento = ? " +
					"   AND cg_estatus = ? " +
					"   AND ic_programa_fondojr = ? ";

					log.debug("query==" + query);

					ps2 = con.queryPrecompilado(query);
					ps2.setLong(1, Long.parseLong(folio));
					ps2.setString(2, fecVenc);
					ps2.setLong(3, Long.parseLong(prestamo));
					ps2.setLong(4, Long.parseLong(disposicion));
					ps2.setBigDecimal(5, new BigDecimal(totalVenc));
					ps2.setString(6, "RF");
					ps2.setLong(7, Long.parseLong(cvePrograma));
					ps2.executeUpdate();
					ps2.close();

				}
				if ("D".equals(tipoModifi)) { //MODIFICACION DE NO PAGADO RECHAZADO A NO PAGADO
					nombreCliente = "";
					query = "SELECT cg_nombrecliente " +
						"  FROM com_pagos_fide_val " +
						" WHERE ig_prestamo = ?  " +
									 " AND cg_nombrecliente IS NOT NULL AND ROWNUM = 1 ";

					ps2 = con.queryPrecompilado(query);
					ps2.setLong(1, Long.parseLong(prestamo));
					rs2 = ps2.executeQuery();

					if (rs2.next()) {
						nombreCliente = rs2.getString("cg_nombrecliente");
					}
					rs2.close();
					ps2.close();


					query = "INSERT INTO com_pagos_fide_val " +
									 "         (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion, " +
									 "          fg_amortizacion, fg_interes, fg_totalvencimiento, cg_estatus, " +
									 "          ig_origen, df_periodofin, df_registro_fide, df_fecha_carga, " +
									 "          cg_usuario, ig_folio, cs_validacion, df_validacion, " +
									 "          cg_nombrecliente, ic_programa_fondojr, ig_folio_val, cg_observaciones, ig_folio_ajuste) " +
									 "(SELECT com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion, " +
									 "       fg_amortizacion, fg_interes, fg_totalvencimiento, 'NP', " +
									 "       ig_origen, df_periodofin, df_registro_fide, SYSDATE, NULL, ig_folio, " +
									 "       'A', df_valida_rechazo, ?, ic_programa_fondojr, 0, 'Modificacion Fondo Jr', ? " +
						"  FROM com_cred_np_rechazados " +
						"     WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
						"       AND ig_prestamo = ? " +
						"       AND ig_disposicion = ? " +
						"       AND fg_totalvencimiento = ? " +
						"       AND cg_estatus = ? " +
						"       AND ic_programa_fondojr = ? " +
									 "		  AND rownum = 1) ";


					log.debug("query==" + query);

					ps2 = con.queryPrecompilado(query);
					ps2.setString(1, nombreCliente);
					ps2.setLong(2, Long.parseLong(folio));
					ps2.setString(3, fecVenc);
					ps2.setLong(4, Long.parseLong(prestamo));
					ps2.setLong(5, Long.parseLong(disposicion));
					ps2.setBigDecimal(6, new BigDecimal(totalVenc));
					ps2.setString(7, "NPR");
					ps2.setLong(8, Long.parseLong(cvePrograma));
					ps2.executeUpdate();
					ps2.close();


					query = "DELETE com_cred_np_rechazados " +
					"	WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
					"  AND ig_prestamo = ? " +
					"  AND ig_disposicion = ? " +
					"  AND fg_totalvencimiento = ? " +
					"  AND ic_programa_fondojr = ? " +
					"	AND cg_estatus = ? ";

					log.debug("query==" + query);

					ps2 = con.queryPrecompilado(query);
					ps2.setString(1, fecVenc);
					ps2.setLong(2, Long.parseLong(prestamo));
					ps2.setLong(3, Long.parseLong(disposicion));
					ps2.setBigDecimal(4, new BigDecimal(totalVenc));
					ps2.setLong(5, Long.parseLong(cvePrograma));
					ps2.setString(6, "NPR");
					ps2.executeUpdate();
					ps2.close();

				}
				if ("E".equals(tipoModifi)) { //MODIFICACION DE PAGADO A NO PAGADO
					query = "UPDATE com_pagos_fide_val " +
					"   SET df_pago_cliente = NULL, " +
									 //"       ig_numero_docto = NULL, " +
					"       cg_observaciones = 'Modificacion Fondo Jr', " +
					"       cg_estatus = 'NP', " +
					"		  ig_folio_ajuste = ? " +
					" WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " +
					"   AND ig_prestamo = ? " +
					"   AND ig_disposicion = ? " +
					"   AND fg_totalvencimiento = ? " +
					"   AND cg_estatus = ? " +
					"   AND ic_programa_fondojr = ? ";

					log.debug("query==" + query);

					ps2 = con.queryPrecompilado(query);
					ps2.setLong(1, Long.parseLong(folio));
					ps2.setString(2, fecVenc);
					ps2.setLong(3, Long.parseLong(prestamo));
					ps2.setLong(4, Long.parseLong(disposicion));
					ps2.setBigDecimal(5, new BigDecimal(totalVenc));
					ps2.setString(6, "P");
					ps2.setLong(7, Long.parseLong(cvePrograma));
					ps2.executeUpdate();
					ps2.close();


				}
				if("F".equals(tipoModifi)){//MODIFICACION MONTO TOTAL DE PREPAGOS TOTALES
					query = "UPDATE com_pagos_fide_val " +
					"   SET cg_observaciones = 'Modificacion Fondo Jr', " +
					"		  ig_folio_ajuste = ?, " +
					"		  fg_totalvencimiento = ?, " +
					"		  fg_amortizacion = ?, " +
					"		  fg_interes = ? " +
					" WHERE ig_prestamo = ? " +
					"   AND cg_estatus = ? " +
					"   AND cs_validacion != ? " +
					"   AND ic_programa_fondojr = ? ";

					log.debug("query=="+query);

					ps2 = con.queryPrecompilado(query);
					ps2.setLong(1, Long.parseLong(folio));
					ps2.setBigDecimal(2, new BigDecimal(totalVenc));
					ps2.setBigDecimal(3, new BigDecimal(montoCapital));
					ps2.setBigDecimal(4, new BigDecimal(montoInteres));
					ps2.setLong(5, Long.parseLong(prestamo));
					ps2.setString(6, "T");
					ps2.setString(7, "R");
					ps2.setLong(8, Long.parseLong(cvePrograma));
					ps2.executeUpdate();
					ps2.close();


				}

				totalRegC++;

			}
			rs.close();
			ps.close();

			//SE GUARADA RESUMEN DE LA CARGA

			query = " SELECT count(*) totalreg  " +
					"  FROM comtmp_ajuste_fondojr  " +
					" WHERE ic_numero_proceso = ? ";

			ps = con.queryPrecompilado(query);
			ps.setLong(1, Long.parseLong(numProc));
			rs = ps.executeQuery();
			if (rs != null && rs.next()) {
				totalReg = rs.getInt("totalreg");
			}
			rs.close();
			ps.close();

			java.util.Date date = new java.util.Date();
			String fecha = (new SimpleDateFormat("dd/MM/yyyy")).format(date);
			String hora = (new SimpleDateFormat("HH:mm:ss")).format(date);
			totalRegI = totalReg - totalRegC;

			query = "INSERT INTO com_carga_ajuste_fondojr " +
						"            (ic_folio_ajuste, df_fecha_carga, df_hora_carga, cs_tipo_mod, " +
			"             ic_usuario, cg_nombre_user, ig_total_reg, ig_total_reg_c, ig_total_reg_i " +
			"            ) " +
			"     VALUES (?, ?, ?, ?, " +
			"             ?, ?, ?, ?, ? " +
			"            ) ";

			ps = con.queryPrecompilado(query);
			ps.setLong(1, Long.parseLong(folio));
			ps.setString(2, fecha);
			ps.setString(3, hora);
			ps.setString(4, tipoModifi);
			ps.setString(5, iNoUsuario);
			ps.setString(6, strNombreUsuario);
			ps.setInt(7, totalReg);
			ps.setInt(8, totalRegC);
			ps.setInt(9, totalRegI);
			ps.executeUpdate();
			ps.close();

			mpDataRes.put("RECIBO", folio);
			mpDataRes.put("FECHA", fecha);
			mpDataRes.put("HORA", hora);
			mpDataRes.put("TOTALREG", String.valueOf(totalReg));
			mpDataRes.put("TOTALREGC", String.valueOf(totalRegC));
			mpDataRes.put("TOTALREGI", String.valueOf(totalRegI));

			return mpDataRes;

		} catch (Throwable t) {
			log.error("realizaAjusteFondoJr(Error)");
			commit = false;
			throw new AppException("Error al realizar Modificacion de Fondo Junior", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("realizaAjusteFondoJr(S)");
		}
	}


	/**
	 * genera archivo zip para la parte de la Modificacion al Fondo Junior
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param numProc
	 * @param csReg
	 * @param rutaVirtual
	 * @param rutaFisica
	 */
	public String generaArchivoCargaZip(String rutaFisica, String rutaVirtual, String numProc,
													String csReg) throws AppException {
		log.info("generaArchivoCargaZip ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String nombreArchivo = null;
		String nombreZip = null;
		String query = "";
		CreaArchivo archivo = null;
		int x = 0;

		StringBuffer contenidotxt = new StringBuffer();

		try {
			con.conexionDB();
			//Se crea la instancia del EJB para su utilizaci�n.
			//FondoJuniorHome fondoJuniorHome = (FondoJuniorHome)ServiceLocator.getInstance().getEJBHome("FondoJuniorEJB", FondoJuniorHome.class);
			//FondoJunior fondoJunior = fondoJuniorHome.create();
			java.text.SimpleDateFormat fHora = new java.text.SimpleDateFormat("dd-MM-yyyy");
			String fec_Actual = fHora.format(new java.util.Date());

			log.debug("rutaFisica>>>>>" + rutaFisica);
			log.debug("rutaVirtual>>>>" + rutaVirtual);

			PrintWriter salida = null;
			FileWriter fw = new FileWriter(rutaFisica + "modificacionfjr" + fec_Actual + ".txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			salida = new PrintWriter(bw);

			archivo = new CreaArchivo();
			nombreZip = "modificacionfjr" + fec_Actual;
			nombreArchivo = nombreZip + ".txt";


			query = "SELECT   ig_prestamo, ig_disposicion, fg_totalvencimiento, " +
				"         fg_amortizacion, fg_interes, " +
				"         com_fechaprobablepago, ic_numero_linea, cg_mensajes_error " +
				"    FROM comtmp_ajuste_fondojr " +
				"   WHERE ic_numero_proceso = ? AND cs_valido = ? " +
				"ORDER BY ic_numero_linea ";

			ps = con.queryPrecompilado(query);
			ps.setLong(1, Long.parseLong(numProc));
			ps.setString(2, csReg);
			rs = ps.executeQuery();

			while (rs.next()) {
				if(rs.getString("fg_amortizacion")!=null && !"".equals(rs.getString("fg_amortizacion"))
						&& Comunes.esNumeroEnteroPositivo(rs.getString("fg_amortizacion")) ){
					contenidotxt.append(rs.getString("ig_prestamo")==null?"|":rs.getString("ig_prestamo")+"|");
					contenidotxt.append(rs.getString("fg_amortizacion")==null?"|":rs.getString("fg_amortizacion")+"|");
					contenidotxt.append(rs.getString("fg_interes")==null?"|":rs.getString("fg_interes")+"|");
					contenidotxt.append(rs.getString("ic_numero_linea")==null?"|":("Linea "+rs.getString("ic_numero_linea"))+"|");	
				}else{
				contenidotxt.append(rs.getString("ig_prestamo") == null ? "|" : rs.getString("ig_prestamo") + "|");
				contenidotxt.append(rs.getString("ig_disposicion") == null ? "|" : rs.getString("ig_disposicion") + "|");
					contenidotxt.append(rs.getString("fg_totalvencimiento")==null?"|":rs.getString("fg_totalvencimiento")+"|");
					contenidotxt.append(rs.getString("com_fechaprobablepago")==null?"|":rs.getString("com_fechaprobablepago")+"|");
					contenidotxt.append(rs.getString("ic_numero_linea")==null?"|":("Linea "+rs.getString("ic_numero_linea"))+"|");	
				}
				
				
				if ("N".equals(csReg)) {
					contenidotxt.append(rs.getString("cg_mensajes_error")==null?"|":rs.getString("cg_mensajes_error")+"|");
				}
				contenidotxt.append("\n");

				salida.print(contenidotxt.toString());
				contenidotxt = contenidotxt.delete(0, contenidotxt.length());
				x++;
			}

			rs.close();
			ps.close();


			salida.close();

			GenerarArchivoZip zip = new GenerarArchivoZip();
			zip.generandoZip(rutaFisica, rutaFisica, nombreZip);
			File f = new File(rutaFisica + nombreArchivo);
			if (f.exists())
				f.delete();

			return nombreZip + ".zip";
		} catch (Throwable e) {
			log.info("generaArchivoCargaZip(ERROR) :: " + e.toString());
			e.printStackTrace();
			throw new AppException("Error al generar zip de modificacion ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("generaArchivoCargaZip(S) ::..");
		}
	} //generaPDF

	//*************************************************************************** F042-2011

	public void depuraInfoDesembolsosBatch(int dias) throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
		boolean commit = true;
		String query = "";
		String query2 = "";
		String query3 = "";

		try {
			con.conexionDB();

			query = "SELECT   ic_desembolsos_batch, ic_resumen_desem_batch " + "    FROM com_resumen_desembolsos_batch " +
						"   WHERE df_procesado < (trunc(SYSDATE) - ?)  " + "   AND cs_procesado = ? " +
						"ORDER BY ic_resumen_desem_batch ";

			query2 = "DELETE comtmp_desembolsos_batch " + "		WHERE ic_desembolsos_batch = ?  " +
						"		AND cg_estatus_reg = ? ";

			query3 = "UPDATE com_resumen_desembolsos_batch " + "   SET cs_depurado = ? " +
						" WHERE ic_resumen_desem_batch = ? ";

			ps = con.queryPrecompilado(query);
			ps.setInt(1, dias);
			ps.setString(2, "S");
			rs = ps.executeQuery();

			String icDesembolosB = "";
			String icResumenB = "";
			while (rs != null && rs.next()) {
				icDesembolosB = rs.getString("ic_desembolsos_batch");
				icResumenB = rs.getString("ic_resumen_desem_batch");

				log.debug("icDesembolosB==" + icDesembolosB);
				log.debug("icResumenB==" + icResumenB);

				ps2 = con.queryPrecompilado(query2);
				ps2.setLong(1, Long.parseLong(icDesembolosB));
				ps2.setString(2, "N");
				ps2.executeUpdate();
				ps2.close();

				ps2 = con.queryPrecompilado(query3);
				ps2.setString(1, "S");
				ps2.setLong(2, Long.parseLong(icResumenB));
				ps2.close();

				con.terminaTransaccion(commit);

			}


			rs.close();
			ps.close();

		} catch (Throwable t) {
			log.info("Error en la depuracion de Desembolsos Batch");
			commit = false;
			throw new AppException("Error en la depuracion de Desembolsos Batch", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}

	public void procesarCargaTmpDesembolsosBatch() throws AppException {
		log.info("ReembolsosPorFiniquitoCargaMasiva : validaCargaTmpDesembolsosBatch(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ResultSet rs2 = null;
		PreparedStatement ps2 = null;
		boolean commit = true;
		String query = "";
		String query2 = "";
		String query3 = "";

		try {
			con.conexionDB();
			String cveResumenB = "";
			String cveDesemB = "";
			String cveFolioB = "";
			String csDepuradoB = "";
			String cveProgramaB = "";
			String cgUsuarioB = "";

			query = "SELECT   rm.ic_resumen_desem_batch, rm.ic_desembolsos_batch, " +
						"         rm.cg_folio_batch, rm.cs_depurado, rm.ic_programa_fondojr, " + "         rm.cg_usuario " +
						"    FROM com_resumen_desembolsos_batch rm " + "   WHERE rm.cs_procesado = ? " +
						" ORDER BY rm.ic_resumen_desem_batch ";

			query2 = "SELECT  db.ic_desembolsos_batch, db.ig_linea, db.ig_prestamo, db.ig_cliente, db.fn_monto_reembolso, " +
						"         db.cg_observaciones " + "    FROM comtmp_desembolsos_batch db " +
						"   WHERE db.ic_desembolsos_batch = ? AND db.cg_estatus_reg = ? " + " ORDER BY db.ig_linea ";

			/*
			query3 = "SELECT   COUNT (1) numreg, cg_estatus_reg " +
					"    FROM comtmp_desembolsos_batch " +
					"  WHERE ic_desembolsos_batch = ?  " +
					"GROUP BY cg_estatus_reg  ";
			*/
			query3 = "UPDATE com_resumen_desembolsos_batch " + "   SET cs_procesado = ?, " +
						"       df_procesado = trunc(SYSDATE) " + " WHERE ic_resumen_desem_batch = ? AND cs_procesado = ? ";


			ps = con.queryPrecompilado(query);
			ps.setString(1, "N");
			rs = ps.executeQuery();
			while (rs != null && rs.next()) {
				int cont = 0;
				cveResumenB = rs.getString("ic_resumen_desem_batch") != null ? rs.getString("ic_resumen_desem_batch") : "";
				cveDesemB = rs.getString("ic_desembolsos_batch") != null ? rs.getString("ic_desembolsos_batch") : "";
				cveFolioB = rs.getString("cg_folio_batch") != null ? rs.getString("cg_folio_batch") : "";
				csDepuradoB = rs.getString("cs_depurado") != null ? rs.getString("cs_depurado") : "";
				cveProgramaB = rs.getString("ic_programa_fondojr") != null ? rs.getString("ic_programa_fondojr") : "";
				cgUsuarioB = rs.getString("cg_usuario") != null ? rs.getString("cg_usuario") : "";

				ps2 = con.queryPrecompilado(query2);
				ps2.setLong(1, Long.parseLong(cveDesemB));
				ps2.setString(2, "P");
				rs2 = ps2.executeQuery();

				while (rs2 != null && rs2.next()) {
					HashMap mpDesembolso = new HashMap();
					mpDesembolso.put("IC_DESEMBOLSO_BATCH", cveDesemB);
					mpDesembolso.put("IC_RESUMEN_DESEM_BATCH", cveResumenB);
					mpDesembolso.put("LINEA", rs2.getString("ig_linea") != null ? rs2.getString("ig_linea") : "");
					mpDesembolso.put("IC_PROGRAMA_FONDOJR", cveProgramaB);
					mpDesembolso.put("IG_PRESTAMO",
										  rs2.getString("ig_prestamo") != null ? rs2.getString("ig_prestamo") : "");
					mpDesembolso.put("IG_CLIENTE", rs2.getString("ig_cliente") != null ? rs2.getString("ig_cliente") : "");
					mpDesembolso.put("FG_TOTALVENCIMIENTO",
										  rs2.getString("fn_monto_reembolso") != null ? rs2.getString("fn_monto_reembolso") : "");
					mpDesembolso.put("OBSERVACIONES",
										  rs2.getString("cg_observaciones") != null ? rs2.getString("cg_observaciones") : "");
					mpDesembolso.put("CG_USUARIO", cgUsuarioB);

					this.validaDesembolsoBatch(mpDesembolso);
					cont++;

				}
				rs2.close();
				ps2.close();

				if (cont > 0) {
					ps2 = con.queryPrecompilado(query3);
					ps2.setString(1, "S");
					ps2.setLong(2, Long.parseLong(cveResumenB));
					ps2.setString(3, "N");
					ps2.executeUpdate();
					ps2.close();
				}

			}
			rs.close();
			ps.close();

		} catch (Throwable t) {
			log.info("ReembolsosPorFiniquitoCargaMasiva : validaCargaTmpDesembolsosBatch(Error)");
			commit = false;
			throw new AppException("Error al validar Desembolsos Tmp Batch", t);
		} finally {
			log.info("ReembolsosPorFiniquitoCargaMasiva : validaCargaTmpDesembolsosBatch(S)");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}

	private String validaDesembolsoBatch(HashMap mpDesembolso) throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		StringBuffer errores = new StringBuffer();
		boolean commit = true;

		try {
			con.conexionDB();
			String cveDesemB = (String) mpDesembolso.get("IC_DESEMBOLSO_BATCH");
			String cveResumenB = (String) mpDesembolso.get("IC_RESUMEN_DESEM_BATCH");
			String linea = (String) mpDesembolso.get("LINEA");
			String cveProgramaB = (String) mpDesembolso.get("IC_PROGRAMA_FONDOJR");
			String ig_prestamo = (String) mpDesembolso.get("IG_PRESTAMO");
			String ig_cliente = (String) mpDesembolso.get("IG_CLIENTE");
			String fg_totalvencimiento = (String) mpDesembolso.get("FG_TOTALVENCIMIENTO");
			//String observaciones = (String)mpDesembolso.get("OBSERVACIONES");

			String ig_prestamo_tmp = "";
			String ig_cliente_tmp = "";

			if (ig_prestamo == null || ig_prestamo.equals("")) {
				errores.append("L�nea " + linea + ": El n�mero de pr�stamo es requerido,");
			} else if (!ig_prestamo.equals("")) {
				if (!Comunes.esNumero(ig_prestamo) || ig_prestamo.length() > 12) {
					errores.append("L�nea " + linea + ": El n�mero de pr�stamo debe ser un entero de 12 d�gitos,");
				} else {
					//ig_prestamo_pf = "";
					strSQL.append(" SELECT DISTINCT ig_prestamo,");
					strSQL.append(" ig_cliente");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(" WHERE ig_prestamo = ? ");
					strSQL.append(" AND ic_programa_fondojr =  ? ");

					ps = con.queryPrecompilado(strSQL.toString());
					ps.setLong(1, Long.parseLong(ig_prestamo));
					ps.setLong(2, Long.parseLong(cveProgramaB));
					rs = ps.executeQuery();


					while (rs != null && rs.next()) {
						ig_prestamo_tmp = rs.getString("ig_prestamo") == null ? "" : rs.getString("ig_prestamo");
						ig_cliente_tmp = rs.getString("ig_cliente") == null ? "" : rs.getString("ig_cliente");
					}

					rs.close();
					ps.close();

					if (ig_prestamo_tmp.equals("")) {
						errores.append("Linea " + linea + ": El n�mero de pr�stamo no existe,");
					} else {

						strSQL = new StringBuffer();


						strSQL.append(" SELECT COUNT(ig_prestamo) numero_registros  ");
						strSQL.append(" FROM com_pagos_fide_val  cpf ");
						strSQL.append(" WHERE cpf.ig_prestamo = ?  ");
						strSQL.append(" AND cpf.ic_programa_fondojr = ? ");
						strSQL.append(" AND cpf.cg_estatus IN ('NP') ");
						strSQL.append(" and  ig_disposicion not in ( SELECT ig_disposicion  ");
						strSQL.append(" FROM com_pagos_fide_val cpf ");
						strSQL.append(" WHERE cpf.ig_prestamo  = ? ");
						strSQL.append(" AND cpf.ic_programa_fondojr = ?  ");
						strSQL.append(" and  cg_estatus in('R','P','RF','T')) ");

						ps = con.queryPrecompilado(strSQL.toString());
						ps.setLong(1, Long.parseLong(ig_prestamo));
						ps.setLong(2, Long.parseLong(cveProgramaB));
						ps.setLong(3, Long.parseLong(ig_prestamo));
						ps.setLong(4, Long.parseLong(cveProgramaB));
						rs = ps.executeQuery();

						while (rs.next()) {
							if (rs.getString("numero_registros").equals("0")) {
								errores.append("L�nea " + linea + ": El pr�stamo ya ha sido finiquitado,");
							}
						}

						rs.close();
						ps.close();
					}


					if (ig_cliente == null || ig_cliente.equals("")) {
						errores.append("L�nea " + linea + ": El c�digo de cliente es requerido,");
					} else if (!Comunes.esNumero(ig_cliente) || ig_cliente.length() > 12) {
						errores.append("L�nea " + linea + ": El c�digo de cliente debe ser un entero de m�ximo 12 d�gitos,");
					} else {
						if (!ig_cliente.equals(ig_cliente_tmp)) {
							errores.append("L�nea " + linea + ": El c�digo de cliente no corresponde al n�mero de pr�stamo,");
						}
					}

					if (!Comunes.esDecimal(fg_totalvencimiento) || fg_totalvencimiento.length() > 21) {
						errores.append("L�nea " + linea +
											": El monto total del vencimiento debe ser un n�mero de m�ximo 19 d�gitos y 2 decimales,");
					} else {
						//total_reembolsos = total_reembolsos.add(new BigDecimal(fg_totalvencimiento)).setScale(2, BigDecimal.ROUND_HALF_UP);
					}
				}
			} else {
				errores.append("Linea " + linea + ": El n�mero de pr�stamo no existe,");

			}

			strSQL = new StringBuffer();

			strSQL.append("UPDATE comtmp_desembolsos_batch ");
			strSQL.append("   SET cg_estatus_reg = ?, ");
			strSQL.append("       cg_errores = ? ");
			strSQL.append(" WHERE ic_desembolsos_batch = ? AND ig_linea = ? AND cg_estatus_reg = ? ");

			if (errores.length() > 0) {
				ps = con.queryPrecompilado(strSQL.toString());
				ps.setString(1, "N");
				ps.setString(2, errores.toString());
				ps.setLong(3, Long.parseLong(cveDesemB));
				ps.setLong(4, Long.parseLong(linea));
				ps.setString(5, "P");
				ps.executeUpdate();
				ps.close();
			} else {

				this.procesaDesembolsoBatch(mpDesembolso);

				ps = con.queryPrecompilado(strSQL.toString());
				ps.setString(1, "S");
				ps.setString(2, errores.toString());
				ps.setLong(3, Long.parseLong(cveDesemB));
				ps.setLong(4, Long.parseLong(linea));
				ps.setString(5, "P");
				ps.executeUpdate();
				ps.close();
			}

			String queryCont =
						"SELECT   COUNT (1) numreg, cg_estatus_reg " + "    FROM comtmp_desembolsos_batch " +
						"  WHERE ic_desembolsos_batch = ?  " + "GROUP BY cg_estatus_reg  ";

			String queryUpdate =
						"UPDATE com_resumen_desembolsos_batch " + "   SET ig_reg_validos = ?, " +
						"		  ig_reg_erroneos = ?, " + "		  ig_reg_pend = ? " +
						//		"	  	  cs_procesado = ?, " +
						//		"       df_procesado = trunc(SYSDATE) " +
						" WHERE ic_resumen_desem_batch = ? AND cs_procesado = ? ";

			int regError = 0;
			int regCorrectos = 0;
			int regPendientes = 0;

			ps = con.queryPrecompilado(queryCont);
			ps.setLong(1, Long.parseLong(cveDesemB));
			rs = ps.executeQuery();
			while (rs != null && rs.next()) {
				if ("P".equals(rs.getString("cg_estatus_reg"))) {
					regPendientes = rs.getInt("numreg");
				}
				if ("N".equals(rs.getString("cg_estatus_reg"))) {
					regError = rs.getInt("numreg");
				}
				if ("S".equals(rs.getString("cg_estatus_reg"))) {
					regCorrectos = rs.getInt("numreg");
				}
			}
			rs.close();
			ps.close();

			ps = con.queryPrecompilado(queryUpdate);
			ps.setInt(1, regCorrectos);
			ps.setInt(2, regError);
			ps.setInt(3, regPendientes);
			ps.setLong(4, Long.parseLong(cveResumenB));
			ps.setString(5, "N");
			ps.executeUpdate();
			ps.close();

		} catch (Throwable t) {
			log.info("ReembolsosPorFiniquitoCargaMasiva : validaDesembolsoBatch(Error)");
			commit = false;
			throw new AppException("Error en la validacion por registro de la carga tmp de desembolsos", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("ReembolsosPorFiniquitoCargaMasiva : validaDesembolsoBatch(S)");
		}
		return errores.toString();
	}

	private void procesaDesembolsoBatch(HashMap mpDesembolso) throws AppException {
		log.info("ReembolsosPorFiniquitoCargaMasiva : procesaCargaTmpDesembolsosBatch(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer();
		boolean commit = true;

		try {
			con.conexionDB();
			String cveDesemB = (String) mpDesembolso.get("IC_DESEMBOLSO_BATCH");
			//String cveResumenB = (String)mpDesembolso.get("IC_RESUMEN_DESEM_BATCH");
			String linea = (String) mpDesembolso.get("LINEA");
			String cveProgramaB = (String) mpDesembolso.get("IC_PROGRAMA_FONDOJR");
			String ig_prestamo = (String) mpDesembolso.get("IG_PRESTAMO");
			//String ig_cliente = (String)mpDesembolso.get("IG_CLIENTE");
			//String fg_totalvencimiento = (String)mpDesembolso.get("FG_TOTALVENCIMIENTO");
			//String observaciones = (String)mpDesembolso.get("OBSERVACIONES");
			String cgUsuario = (String) mpDesembolso.get("CG_USUARIO");

			strSQL.append("INSERT INTO com_pagos_fide_val ");
			strSQL.append("            (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion, ");
			strSQL.append("             fg_amortizacion, fg_interes, fg_totalvencimiento, ");
			strSQL.append("             df_pago_cliente, cg_estatus, ig_origen, df_periodofin, ");
			strSQL.append("             ig_numero_docto, df_registro_fide, cg_observaciones, cg_usuario, ");
			strSQL.append("             cg_nombrecliente, cs_validacion, df_validacion, ");
			strSQL.append("             ic_programa_fondojr, ic_desembolsos_batch) ");
			strSQL.append("   SELECT   cpf.com_fechaprobablepago, cpf.ig_prestamo, cpf.ig_cliente, ");
			strSQL.append("            cpf.ig_disposicion, cpf.fg_amortizacion, cpf.fg_interes, ");
			strSQL.append("            cpf.fg_totalvencimiento, cpf.df_pago_cliente, ?, cpf.ig_origen, ");
			strSQL.append("            cpf.df_periodofin, cpf.ig_numero_docto, cpf.df_registro_fide, ");
			strSQL.append("            cdb.cg_observaciones, ?, cg_nombrecliente, ?, SYSDATE, ");
			strSQL.append("            ic_programa_fondojr, cdb.ic_desembolsos_batch ");
			strSQL.append("       FROM com_pagos_fide_val cpf, comtmp_desembolsos_batch cdb ");
			strSQL.append("      WHERE cpf.ig_prestamo = cdb.ig_prestamo ");
			strSQL.append("        AND cdb.ic_desembolsos_batch = ? ");
			strSQL.append("        AND cpf.cg_estatus = ? ");
			strSQL.append("        AND cpf.ic_programa_fondojr = ? ");
			strSQL.append("        AND cdb.ig_prestamo = ? ");
			strSQL.append("        AND NOT EXISTS ( ");
			strSQL.append("               SELECT cpfi.ig_disposicion, cpfi.ig_prestamo ");
			strSQL.append("                 FROM com_pagos_fide_val cpfi, comtmp_desembolsos_batch cdbi ");
			strSQL.append("                WHERE cpfi.ig_prestamo = cdbi.ig_prestamo ");
			strSQL.append("                  AND cdbi.ic_desembolsos_batch = ? ");
			strSQL.append("                  AND cpfi.ic_programa_fondojr = ? ");
			strSQL.append("                  AND cdb.ig_prestamo = ? ");
			strSQL.append("                  AND cg_estatus <> 'NP' ");
			strSQL.append("                  AND cpfi.ig_prestamo = cpf.ig_prestamo ");
			strSQL.append("                  AND cpfi.ig_disposicion = cpf.ig_disposicion) ");
			strSQL.append("   ORDER BY cpf.ig_prestamo, cpf.ig_disposicion ");


			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1, "RF");
			ps.setString(2, cgUsuario);
			ps.setString(3, "A");
			ps.setLong(4, Long.parseLong(cveDesemB));
			ps.setString(5, "NP");
			ps.setLong(6, Long.parseLong(cveProgramaB));
			ps.setLong(7, Long.parseLong(ig_prestamo));
			ps.setLong(8, Long.parseLong(cveDesemB));
			ps.setLong(9, Long.parseLong(cveProgramaB));
			ps.setLong(10, Long.parseLong(ig_prestamo));

			log.debug("strSQL.toString() " + strSQL.toString());

			ps.executeUpdate();
			ps.close();

			strSQL = new StringBuffer();

			strSQL.append(" INSERT INTO com_reembolsos_finiquito (ig_prestamo, fn_monto_reembolso, ic_desembolsos_batch, cs_carga_batch )");
			strSQL.append(" SELECT ig_prestamo, fn_monto_reembolso, ic_desembolsos_batch, ? FROM comtmp_desembolsos_batch ");
			strSQL.append(" WHERE ic_desembolsos_batch = ? ");
			strSQL.append(" AND ig_linea = ? ");
			strSQL.append(" AND ig_prestamo = ? ");

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1, "S");
			ps.setLong(2, Long.parseLong(cveDesemB));
			ps.setLong(3, Long.parseLong(linea));
			ps.setLong(4, Long.parseLong(ig_prestamo));

			ps.executeUpdate();
			ps.close();

			strSQL = new StringBuffer();

			strSQL.append(" DELETE com_cred_pend_reembolso_fide");
			strSQL.append(" WHERE ig_prestamo = ? ");

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setLong(1, Long.parseLong(ig_prestamo));
			ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			commit = false;
			log.info("..:: ReembolsosPorFiniquitoCargaMasiva : procesaDesembolsoBatch(ERROR) :");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("..:: ReembolsosPorFiniquitoCargaMasiva : procesaDesembolsoBatch(F) ::..");
		}
	}

	public List consultaDesembolsosBatch(HashMap mpCriteriosBusq) throws AppException {
		log.info("consultaDesembolsosBatch(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer();
		List lstRegDesemb = new ArrayList();

		try {

			con.conexionDB();
			String numFolio =
						mpCriteriosBusq.get("FOLIO_BATCH") == null ? "" : (String) mpCriteriosBusq.get("FOLIO_BATCH");
			String cgUsuario = mpCriteriosBusq.get("CG_USUARIO") == null ? "" : (String) mpCriteriosBusq.get("CG_USUARIO");
			String fechaCargaIni =
						mpCriteriosBusq.get("FECHA_CARGA_INI") == null ? "" : (String) mpCriteriosBusq.get("FECHA_CARGA_INI");
			String fechaCargaFin =
						mpCriteriosBusq.get("FECHA_CARGA_FIN") == null ? "" : (String) mpCriteriosBusq.get("FECHA_CARGA_FIN");
			String WhereAnd = "";

			strSQL.append("SELECT ic_resumen_desem_batch cveResB, ic_desembolsos_batch cveDesemB, cg_folio_batch folioB, ");
			strSQL.append("       cg_nombre_archivo nombreArch, ");
			strSQL.append("       TO_CHAR (df_carga, 'dd/mm/yyyy HH24:MI:SS') fecha_carga, ");
			strSQL.append("       (cg_usuario || '-' || cg_nombre_usr) c_usuario, ig_reg_total, ");
			strSQL.append("       ig_reg_validos, ig_reg_erroneos, ig_reg_pend, cs_procesado, ");
			strSQL.append("       cs_depurado ");
			strSQL.append("  FROM com_resumen_desembolsos_batch ");
			if (!"".equals(numFolio)) {
				WhereAnd = !"".equals(WhereAnd) ? " AND " : " WHERE";
				strSQL.append(WhereAnd + " cg_folio_batch = ? ");
			}
			if (!"".equals(cgUsuario)) {
				WhereAnd = !"".equals(WhereAnd) ? " AND " : " WHERE";
				strSQL.append(WhereAnd + " cg_usuario = ? ");
			}
			if (!"".equals(fechaCargaIni)) {
				WhereAnd = !"".equals(WhereAnd) ? " AND " : " WHERE";
				strSQL.append(WhereAnd + " df_carga >= TO_DATE (?, 'dd/mm/yyyy') ");
			}
			if (!"".equals(fechaCargaFin)) {
				WhereAnd = !"".equals(WhereAnd) ? " AND " : " WHERE";
				strSQL.append(WhereAnd + " df_carga < (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
			}


			int i = 0;
			ps = con.queryPrecompilado(strSQL.toString());
			if (!"".equals(numFolio)) {
				ps.setString(++i, numFolio);
			}
			if (!"".equals(cgUsuario)) {
				ps.setString(++i, cgUsuario);
			}
			if (!"".equals(fechaCargaIni)) {
				ps.setString(++i, fechaCargaIni);
			}
			if (!"".equals(fechaCargaFin)) {
				ps.setString(++i, fechaCargaFin);
			}

			rs = ps.executeQuery();
			while (rs != null && rs.next()) {
				HashMap mpCargaB = new HashMap();
				mpCargaB.put("CVE_RESUMEN", rs.getString("cveResB") == null ? "" : rs.getString("cveResB"));
				mpCargaB.put("CVE_DESEMB", rs.getString("cveDesemB") == null ? "" : rs.getString("cveDesemB"));
				mpCargaB.put("FOLIO_BATCH", rs.getString("folioB") == null ? "" : rs.getString("folioB"));
				mpCargaB.put("NOMBRE_ARCHIVO", rs.getString("nombreArch") == null ? "" : rs.getString("nombreArch"));
				mpCargaB.put("FECHA_CARGA", rs.getString("fecha_carga") == null ? "" : rs.getString("fecha_carga"));
				mpCargaB.put("USUARIO", rs.getString("c_usuario") == null ? "" : rs.getString("c_usuario"));
				mpCargaB.put("REG_TOTAL", rs.getString("ig_reg_total") == null ? "0" : rs.getString("ig_reg_total"));
				mpCargaB.put("REG_CORRECTOS",
								 rs.getString("ig_reg_validos") == null ? "0" : rs.getString("ig_reg_validos"));
				mpCargaB.put("REG_INCORRECTOS",
								 rs.getString("ig_reg_erroneos") == null ? "0" : rs.getString("ig_reg_erroneos"));
				mpCargaB.put("REG_PENDIENTES", rs.getString("ig_reg_pend") == null ? "0" : rs.getString("ig_reg_pend"));
				mpCargaB.put("CS_PROCESADO", rs.getString("cs_procesado") == null ? "" : rs.getString("cs_procesado"));
				mpCargaB.put("CS_DEPURADO", rs.getString("cs_depurado") == null ? "" : rs.getString("cs_depurado"));

				lstRegDesemb.add(mpCargaB);
			}

			return lstRegDesemb;
		} catch (Throwable t) {
			log.info("consultaDesembolsosBatch(Error)");
			throw new AppException("Error al consultar cargas batch de desembolsos", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultaDesembolsosBatch(S)");
		}
	}


	/**
	 * genera archivo zip del detalle de las cargas batch de desembolsos.
	 * (Pantalla - Consulta Batch)
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param tipoReg
	 * @param icResBatch
	 * @param numFolio
	 * @param rutaVirtual
	 * @param rutaFisica
	 */
	public String generaArchivoCargaBatchZip(String rutaFisica, String rutaVirtual, String numFolio, String icResBatch,
														  String tipoReg) throws AppException {
		log.info("generaArchivoCargaBatchZip ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String nombreArchivo = null;
		String nombreZip = null;
		String query = "";
		CreaArchivo archivo = null;
		int x = 0;

		StringBuffer contenidotxt = new StringBuffer();

		try {
			con.conexionDB();

			log.debug("rutaFisica>>>>>" + rutaFisica);
			log.debug("rutaVirtual>>>>" + rutaVirtual);

			PrintWriter salida = null;
			FileWriter fw = new FileWriter(rutaFisica + "desembolsosfjr" + numFolio + ".txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			salida = new PrintWriter(bw);

			archivo = new CreaArchivo();
			nombreZip = "desembolsosfjr" + numFolio;
			nombreArchivo = nombreZip + ".txt";


			if ("N".equals(tipoReg) || "P".equals(tipoReg)) {
				query = "SELECT cd.ig_prestamo, cd.ig_cliente, cd.fn_monto_reembolso, " +
							  "       cd.cg_observaciones, cd.cg_errores " +
							  "  FROM com_resumen_desembolsos_batch cr, comtmp_desembolsos_batch cd " +
							  " WHERE cr.ic_desembolsos_batch = cd.ic_desembolsos_batch " + "   AND cr.ic_resumen_desem_batch = ? " +
							  "   AND cd.cg_estatus_reg = ? ";

				ps = con.queryPrecompilado(query);
				ps.setString(1, icResBatch);
				ps.setString(2, tipoReg);
				rs = ps.executeQuery();

				while (rs.next()) {

					contenidotxt.append(rs.getString("ig_prestamo") == null ? "|" : rs.getString("ig_prestamo") + "|");
					contenidotxt.append(rs.getString("ig_cliente") == null ? "|" : rs.getString("ig_cliente") + "|");
					contenidotxt.append(rs.getString("fn_monto_reembolso") == null ? "|" :
											  rs.getString("fn_monto_reembolso") + "|");
					contenidotxt.append(rs.getString("cg_observaciones") == null ? "|" :
											  rs.getString("cg_observaciones") + "|");
					if ("N".equals(tipoReg)) {
						contenidotxt.append(rs.getString("cg_errores") == null ? "|" : rs.getString("cg_errores") + "|");
					}
					contenidotxt.append("\n");

					salida.print(contenidotxt.toString());
					contenidotxt = contenidotxt.delete(0, contenidotxt.length());
					x++;
				}

				rs.close();
				ps.close();

			} else if ("S".equals(tipoReg)) {
				PreparedStatement ps2 = null;
				ResultSet rs2 = null;
				String query2 = "";

				query = "SELECT DISTINCT cd.ig_prestamo, cd.ig_cliente, cf.fn_monto_reembolso, " +
							  "                cd.cg_observaciones " + "           FROM com_resumen_desembolsos_batch cr, " +
							  "                com_reembolsos_finiquito cf, " + "                com_pagos_fide_val cd " +
							  "          WHERE cr.ic_desembolsos_batch = cf.ic_desembolsos_batch " +
							  "            AND cf.ic_desembolsos_batch = cd.ic_desembolsos_batch " +
							  "            AND cf.ig_prestamo = cd.ig_prestamo " + "            AND cr.ic_resumen_desem_batch = ? " +
							  "            AND cf.cs_carga_batch = ? ";

				query2 = "SELECT to_char(cd.com_fechaprobablepago,'dd/mm/yyyy') com_fechaprobablepago, cd.ig_prestamo, cd.ig_cliente, " +
							  "       cd.cg_nombrecliente, cd.fg_totalvencimiento, cd.ig_disposicion, " +
							  "       co.cg_descripcion, cd.cg_observaciones " + "  FROM com_resumen_desembolsos_batch cr, " +
							  "       com_reembolsos_finiquito cf, " + "       com_pagos_fide_val cd, " +
							  "       comcat_origen_programa co " + " WHERE cr.ic_desembolsos_batch = cf.ic_desembolsos_batch " +
							  "   AND cf.ic_desembolsos_batch = cd.ic_desembolsos_batch " + "   AND cf.ig_prestamo = cd.ig_prestamo " +
							  "   AND cd.ig_origen = co.ig_origen_programa " + "   AND cr.ic_resumen_desem_batch = ? " +
							  "	 AND cd.ig_prestamo= ? " + "   AND cf.cs_carga_batch = ? " + " ORDER BY cd.ig_disposicion ";

				ps = con.queryPrecompilado(query);
				ps.setLong(1, Long.parseLong(icResBatch));
				ps.setString(2, tipoReg);
				rs = ps.executeQuery();

				while (rs.next()) {
					String prestamo = rs.getString("ig_prestamo");
					contenidotxt.append(rs.getString("ig_prestamo") == null ? "|" : rs.getString("ig_prestamo") + "|");
					contenidotxt.append(rs.getString("ig_cliente") == null ? "|" : rs.getString("ig_cliente") + "|");
					contenidotxt.append(rs.getString("fn_monto_reembolso") == null ? "|" :
											  rs.getString("fn_monto_reembolso") + "|");
					contenidotxt.append(rs.getString("cg_observaciones") == null ? "|" :
											  rs.getString("cg_observaciones") + "|");
					contenidotxt.append("\n");

					ps2 = con.queryPrecompilado(query2);
					ps2.setLong(1, Long.parseLong(icResBatch));
					ps2.setLong(2, Long.parseLong(prestamo));
					ps2.setString(3, "S");
					rs2 = ps2.executeQuery();
					while (rs2.next()) {
						contenidotxt.append("\t");
						contenidotxt.append(rs2.getString("com_fechaprobablepago") == null ? "|" :
												  rs2.getString("com_fechaprobablepago") + "|");
						contenidotxt.append(rs2.getString("ig_prestamo") == null ? "|" : rs2.getString("ig_prestamo") + "|");
						contenidotxt.append(rs2.getString("ig_cliente") == null ? "|" : rs2.getString("ig_cliente") + "|");
						contenidotxt.append(rs2.getString("cg_nombrecliente") == null ? "|" :
												  rs2.getString("cg_nombrecliente") + "|");
						contenidotxt.append(rs2.getString("fg_totalvencimiento") == null ? "|" :
												  rs2.getString("fg_totalvencimiento") + "|");
						contenidotxt.append(rs2.getString("ig_disposicion") == null ? "|" :
												  rs2.getString("ig_disposicion") + "|");
						contenidotxt.append(rs2.getString("cg_descripcion") == null ? "|" :
												  rs2.getString("cg_descripcion") + "|");
						contenidotxt.append(rs2.getString("cg_observaciones") == null ? "|" :
												  rs2.getString("cg_observaciones") + "|");
						contenidotxt.append("\n");
					}
					rs2.close();
					ps2.close();

					salida.print(contenidotxt.toString());
					contenidotxt = contenidotxt.delete(0, contenidotxt.length());
					x++;
				}

				rs.close();
				ps.close();
			}

			salida.close();

			GenerarArchivoZip zip = new GenerarArchivoZip();
			zip.generandoZip(rutaFisica, rutaFisica, nombreZip);
			File f = new File(rutaFisica + nombreArchivo);
			if (f.exists())
				f.delete();

			return nombreZip + ".zip";
		} catch (Throwable e) {
			log.info("generaArchivoCargaBatchZip(ERROR) :: " + e.toString());
			e.printStackTrace();
			throw new AppException("Error al generar zip del resultad de la carga batch ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("generaArchivoCargaBatchZip(S) ::..");
		}
	} //generaPDF

	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param request
	 * @param rutaVirtual
	 * @param rutaFisica
	 * @param form
	 */


	public String generaCSV(FondoJrConciliacionBean form, String rutaFisica, String rutaVirtual) throws AppException {
		System.out.println("generaCSV e");

		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = "";
		BigDecimal montoP = new BigDecimal("0.00");
		BigDecimal montoNP = new BigDecimal("0.00");
		BigDecimal montoT = new BigDecimal("0.00");
		BigDecimal montoR = new BigDecimal("0.00");
		BigDecimal montoRF = new BigDecimal("0.00");
		BigDecimal montoIni = new BigDecimal("0.00");
		BigDecimal montoOtros = new BigDecimal("0.00");
		BigDecimal montoCap = new BigDecimal("0.00");
		BigDecimal montoInteres = new BigDecimal("0.00");
		BigDecimal montoSaldo = new BigDecimal("0.00");
		//String clavePrograma= form.getClavePrograma();
		String nombrePrograma = form.getDescProgramaFondoJR();
		try {

			//List lstFondoConcentrado = this.consConciliacionConcentrado(clavePrograma);//FODEA 026 - 2010 ACF
			form.setStrDirectorioPublicacion("pppp----");

			System.out.println(form.getStrDirectorioPublicacion());
			System.out.println(form.getClavePrograma());
			List lstFondoConcentradoDet = this.consConciliacionDetalle(form);

			StringBuffer contenidoArchivo = new StringBuffer();
			if (lstFondoConcentradoDet != null && lstFondoConcentradoDet.size() > 0) {
				for (int x = 0; x < lstFondoConcentradoDet.size(); x++) {
					List lstFondoConcentradoposicion = (List) lstFondoConcentradoDet.get(x);
					if (x == 0) {
						if (!((String) lstFondoConcentradoposicion.get(4)).equals(""))
							montoIni = montoIni.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
						montoSaldo = montoSaldo.add(montoIni);
						contenidoArchivo.append("Programa:, " + nombrePrograma + "\n");
						contenidoArchivo.append("Fecha de Vencimiento,Fecha Pago FIDE,Capital,Interes,Saldo,Pagado,No Pagado,Reembolso FIDE,Desembolsos,Prepagos Totales,Otros Movimientos,Dias Transcurridos de Venc,Observaciones,Fecha de Validacion,Usuario\n");
						contenidoArchivo.append("Saldo inicial del Fondo," +
														Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, false) + "," +
														(String) lstFondoConcentradoposicion.get(6) + "\n");
					} else {
						if (!((String) lstFondoConcentradoposicion.get(2)).equals(""))
							montoCap = montoCap.add(new BigDecimal((String) lstFondoConcentradoposicion.get(2)));
						if (!((String) lstFondoConcentradoposicion.get(3)).equals(""))
							montoInteres = montoInteres.add(new BigDecimal((String) lstFondoConcentradoposicion.get(3)));
						if (!((String) lstFondoConcentradoposicion.get(4)).equals(""))
							montoSaldo = montoSaldo.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
						if (((String) lstFondoConcentradoposicion.get(5)).equals("P"))
							montoP = montoP.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
						if (((String) lstFondoConcentradoposicion.get(5)).equals("NP"))
							montoNP = montoNP.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
						if (((String) lstFondoConcentradoposicion.get(5)).equals("R"))
							montoR = montoR.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
						if (((String) lstFondoConcentradoposicion.get(5)).equals("RF"))
							montoRF = montoRF.add(new BigDecimal((String) lstFondoConcentradoposicion.get(10)));
						if (((String) lstFondoConcentradoposicion.get(5)).equals("T"))
							montoT = montoT.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));
						if ((((String) lstFondoConcentradoposicion.get(5)).toUpperCase()).equals("SUMA") ||
							 (((String) lstFondoConcentradoposicion.get(5)).toUpperCase()).equals("RESTA"))
							montoOtros = montoOtros.add(new BigDecimal((String) lstFondoConcentradoposicion.get(4)));

						contenidoArchivo.append((String) lstFondoConcentradoposicion.get(0) + "," +
														(String) lstFondoConcentradoposicion.get(1) + "," +
														(!((String) lstFondoConcentradoposicion.get(2)).equals("") ?
														 "$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(2)), 2, false) : "") + "," +
														(!((String) lstFondoConcentradoposicion.get(3)).equals("") ?
														 "$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(3)), 2, false) : "") + "," +
														(!((String) lstFondoConcentradoposicion.get(4)).equals("") ?
														 "$" + Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, false) : "") + "," +
														(((String) lstFondoConcentradoposicion.get(5)).equals("P") ?
														 Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, false) : "") + "," +
														(((String) lstFondoConcentradoposicion.get(5)).equals("NP") ?
														 Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, false) : "") + "," +
														(((String) lstFondoConcentradoposicion.get(5)).equals("R") ?
														 Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, false) : "") + "," +
														(((String) lstFondoConcentradoposicion.get(5)).equals("RF") ?
														 Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(10)), 2, false) : "") + "," +
														(((String) lstFondoConcentradoposicion.get(5)).equals("T") ?
														 Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, false) : "") + "," +
														((((String) lstFondoConcentradoposicion.get(5)).toUpperCase()).equals("SUMA") ||
														 (((String) lstFondoConcentradoposicion.get(5)).toUpperCase()).equals("RESTA") ?
														 Comunes.formatoDecimal(((String) lstFondoConcentradoposicion.get(4)), 2, false) : "") + "," +
														(((String) lstFondoConcentradoposicion.get(11)).equals("") ? "N/A" :
														 (String) lstFondoConcentradoposicion.get(11)) + "," + (String) lstFondoConcentradoposicion.get(6) + "," +
														(String) lstFondoConcentradoposicion.get(7) + "," + (String) lstFondoConcentradoposicion.get(8) +
														"\n");
					} //CIERRE ELSE
				} //CIERRE FOR

				contenidoArchivo.append("Totales,," + Comunes.formatoDecimal(montoCap.toPlainString(), 2, false) + "," +
												Comunes.formatoDecimal(montoInteres.toPlainString(), 2, false) + "," +
												Comunes.formatoDecimal(montoSaldo.toPlainString(), 2, false) + "," +
												Comunes.formatoDecimal(montoP.toPlainString(), 2, false) + "," +
												Comunes.formatoDecimal(montoNP.toPlainString(), 2, false) + "," +
												Comunes.formatoDecimal(montoR.toPlainString(), 2, false) + "," +
												Comunes.formatoDecimal(montoRF.toPlainString(), 2, false) + "," +
												Comunes.formatoDecimal(montoT.toPlainString(), 2, false) + "," +
												Comunes.formatoDecimal(montoOtros.toPlainString(), 2, false) + "\n");
			}

			archivo.make(contenidoArchivo.toString(), rutaFisica, ".csv");
			nombreArchivo = archivo.nombre;

			return nombreArchivo;
			/*}catch(Exception e){

			e.printStackTrace();
			throw new AppException("Error al generar resultad de la carga batch ",e);
		}
		*/
		} catch (Throwable e) {
			log.info("generaCSV(ERROR) :: " + e.toString());
			e.printStackTrace();
			throw new AppException("Error al generar zip del resultad de la carga batch ", e);
		} finally {

			log.info("generaCSV(S) ::..");
		}
	} //generaCSV


	public String generaZIP(FondoJrConciliacionBean form, String rutaFisica, String rutaVirtual) throws AppException {

		String clavePrograma = form.getClavePrograma();
		String nombrePrograma = form.getDescProgramaFondoJR();

		try {
			return this.generaArchConciliacionZip(form, rutaFisica, rutaVirtual, clavePrograma, nombrePrograma);

		} catch (Throwable e) {
			log.info("generaCSV(ERROR) :: " + e.toString());
			e.printStackTrace();
			throw new AppException("Error al generar zip del resultad de la carga batch ", e);
		} finally {

			log.info("generaCSV(S) ::..");
		}
	} //generaZIP


	public String proceso() throws NafinException {
		System.out.println("Proceso (E)");

		PreparedStatement ps = null;
		ResultSet rs = null;
		String clavePrograma = "";
		String qrySentence = "";
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		AccesoDB con = new AccesoDB();
                AccesoDB conPs = new AccesoDB();
		BigDecimal montoP = new BigDecimal("0.00");
		BigDecimal montoNP = new BigDecimal("0.00");
		BigDecimal montoT = new BigDecimal("0.00");
		BigDecimal montoR = new BigDecimal("0.00");
		BigDecimal montoRF = new BigDecimal("0.00");
		BigDecimal montoRC = new BigDecimal("0.00");
		BigDecimal montoSUM = new BigDecimal("0.00");
		BigDecimal montoRES = new BigDecimal("0.00");
		BigDecimal montoIni = new BigDecimal("0.00");
		BigDecimal montoOtros = new BigDecimal("0.00");
		BigDecimal montoFinal = new BigDecimal("0.00");
		BigDecimal montoPenReem = new BigDecimal("0.00");
		String fec_ultimo_mov = "";

		try {

			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" DELETE FROM  TMP_CONCILIACION_FJR  ");
			ps = con.queryPrecompilado(strSQL.toString());
			ps.executeUpdate();
			ps.close();

			qrySentence = "  select IC_PROGRAMA_FONDOJR  from  COMCAT_PROGRAMA_FONDOJR ";
			rs = con.queryDB(qrySentence);
                        ArrayList<String> registrosIcProgramaFondoJR = new ArrayList<String>();
                        while (rs.next()) {
                                registrosIcProgramaFondoJR.add(rs.getString("IC_PROGRAMA_FONDOJR") != null ? rs.getString("IC_PROGRAMA_FONDOJR") : "");
                        }
                        rs.close();
                        Iterator<String> ItRegIcProgramaFondoJR = registrosIcProgramaFondoJR.iterator();
			while (ItRegIcProgramaFondoJR.hasNext()) {
				clavePrograma = ItRegIcProgramaFondoJR.next();

				List lstFondoConcentrado = this.consConciliacionConcentrado(clavePrograma);

				if (lstFondoConcentrado != null && lstFondoConcentrado.size() > 0) {
					montoP = montoP.add((BigDecimal) lstFondoConcentrado.get(0));
					montoNP = montoNP.add((BigDecimal) lstFondoConcentrado.get(1));
					montoT = montoT.add((BigDecimal) lstFondoConcentrado.get(2));
					montoR = montoR.add((BigDecimal) lstFondoConcentrado.get(3));
					montoRF = montoRF.add((BigDecimal) lstFondoConcentrado.get(4));
					montoRC = montoRC.add((BigDecimal) lstFondoConcentrado.get(5));
					montoSUM = montoSUM.add((BigDecimal) lstFondoConcentrado.get(6));
					montoRES = montoRES.add((BigDecimal) lstFondoConcentrado.get(7));
					montoIni = montoIni.add((BigDecimal) lstFondoConcentrado.get(8));
					fec_ultimo_mov = (String) lstFondoConcentrado.get(9);
					montoOtros = montoOtros.add(montoSUM.subtract(montoRES));
					//(SI + R +RC+OTROS)-(NP)

					montoFinal = montoFinal.add(((((montoIni.add(montoR)).add(montoRC)).add(montoOtros)).subtract(montoNP)));
					montoPenReem = montoPenReem.add(montoNP.subtract(montoR));

					strSQL = new StringBuffer();
					strSQL.append(" INSERT INTO TMP_CONCILIACION_FJR ( FECHA_MOVIMIENTO ,  SALDO_INICIAL ,  TOTAL_RETIROS , TOTAL_REMBOLOSOS ,  DESEM_PENDIENTES,  PREPAGOS_TOTALES ,  OTROS_MOVIMIENTOS ,  SALDO_FINAL ,  RETIROS_PRENDIENTES ,  IG_ORIGEN_PROGRAMA  )" +
									  " VALUES ( to_date(?,'dd/mm/yyyy') , ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");

					System.out.println("strSQL >>>>>>>>>" + strSQL);

                                        conPs.conexionDB();
                                        pst = conPs.queryPrecompilado(strSQL.toString());
					pst.setString(1, fec_ultimo_mov);
					pst.setBigDecimal(2, montoIni);
					pst.setBigDecimal(3, montoNP);
					pst.setBigDecimal(4, montoR);
					pst.setBigDecimal(5, montoRF);
					pst.setBigDecimal(6, montoT);
					pst.setBigDecimal(7, montoOtros);
					pst.setBigDecimal(8, montoFinal);
					pst.setBigDecimal(9, montoPenReem);
					pst.setString(10, clavePrograma);
					pst.executeUpdate();
					pst.close();
                                        if (conPs.hayConexionAbierta()) {
                                                conPs.terminaTransaccion(true);
                                                conPs.cierraConexionDB();
                                        }
				}
			}
			return "termino proceso";

		} catch (Exception e) {
			System.out.println("proceso(ERROR)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
                        if (conPs.hayConexionAbierta()) {
                                conPs.terminaTransaccion(true);
                                conPs.cierraConexionDB();
                        }

			System.out.println("proceso(S)");
		}

	}


	public List consConciliacion(String clavePrograma) throws NafinException {
		log.info("consConciliacion(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		String qrySentence = "";

		List lstRegMontos = new ArrayList();

		try {
			con.conexionDB();
			//query para obtener fecha de ultimo movimiento
			qrySentence =
						" SELECT  TO_CHAR(FECHA_MOVIMIENTO, 'dd/mm/yyyy')FECHA_MOVIMIENTO,SALDO_INICIAL,TOTAL_RETIROS,TOTAL_REMBOLOSOS," +
						" DESEM_PENDIENTES,PREPAGOS_TOTALES,OTROS_MOVIMIENTOS,SALDO_FINAL,RETIROS_PRENDIENTES " +
						" FROM TMP_CONCILIACION_FJR " + " WHERE IG_ORIGEN_PROGRAMA=" + clavePrograma + " ";


			log.debug("..:: qrySentence1: " + qrySentence);

			rs = con.queryDB(qrySentence);
			if (rs != null) {
				while (rs.next()) {

					lstRegMontos.add(rs.getString("FECHA_MOVIMIENTO"));
					lstRegMontos.add(rs.getString("SALDO_INICIAL"));
					lstRegMontos.add(rs.getString("TOTAL_RETIROS"));
					lstRegMontos.add(rs.getString("TOTAL_REMBOLOSOS"));
					lstRegMontos.add(rs.getString("DESEM_PENDIENTES"));
					lstRegMontos.add(rs.getString("PREPAGOS_TOTALES"));
					lstRegMontos.add(rs.getString("OTROS_MOVIMIENTOS"));
					lstRegMontos.add(rs.getString("SALDO_FINAL"));
					lstRegMontos.add(rs.getString("RETIROS_PRENDIENTES"));

				}
			}

			return lstRegMontos;

		} catch (Exception e) {
			log.info("consConciliacion(ERROR)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consConciliacion(S)");
		}
	} //cierre metodo


	public String generaCSVMonitor(String fec_ini, String fec_fin, String estatus, String cvePrograma, String descPrograma,
											 String chkDesembolso, String tipoArchivo, String rutaFisica,
											 String rutaVirtual) throws AppException {

		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		List lstConsMonitor = new ArrayList();
		List regConsMonitor = new ArrayList();


		try {

			//java.text.SimpleDateFormat fHora = new java.text.SimpleDateFormat ("dd/MM/yyyy hh:mm:ss");
			//String fec_Actual = fHora.format(new java.util.Date());

			if ("R".equals(tipoArchivo)) {

				lstConsMonitor = consultaMonitorReemb(fec_ini, fec_fin, cvePrograma);

				if (lstConsMonitor != null && lstConsMonitor.size() > 0) {
					for (int x = 0; x < lstConsMonitor.size(); x++) {
						regConsMonitor = (List) lstConsMonitor.get(x);
						if (x == 0) {
							contenidoArchivo.append("Programa:," + descPrograma + "\n");
							contenidoArchivo.append("Fecha de Vencimiento Nafin,");
							contenidoArchivo.append("Fecha y Hora del Ultimo Registro Enviado por FIDE,");
							contenidoArchivo.append("Folio de Operacion,");
							contenidoArchivo.append("Total de Registros,");
							contenidoArchivo.append("Monto");
							contenidoArchivo.append("\n");
						}

						contenidoArchivo.append((regConsMonitor.get(1) == null ? "" : (String) regConsMonitor.get(1)) + ",");
						contenidoArchivo.append((regConsMonitor.get(2) == null ? "" : (String) regConsMonitor.get(2)) + ",");
						contenidoArchivo.append((regConsMonitor.get(3) == null ? "" : (String) regConsMonitor.get(3)) + ",");
						contenidoArchivo.append((regConsMonitor.get(4) == null ? "" : (String) regConsMonitor.get(4)) + ",");
						contenidoArchivo.append((regConsMonitor.get(5) == null ? "" : (String) regConsMonitor.get(5)));
						contenidoArchivo.append("\n");
					}
				}

			} else if ("NPR".equals(tipoArchivo)) {
				lstConsMonitor = consultaMonitorNPR(fec_ini, fec_fin, chkDesembolso, cvePrograma);

				if (lstConsMonitor != null && lstConsMonitor.size() > 0) {
					for (int x = 0; x < lstConsMonitor.size(); x++) {
						regConsMonitor = (List) lstConsMonitor.get(x);
						if (x == 0) {
							contenidoArchivo.append("Programa:," + descPrograma + "\n");
							contenidoArchivo.append("Fecha de Vencimiento Nafin,");
							contenidoArchivo.append("NP Rechazados,");
							contenidoArchivo.append("Monto");
							contenidoArchivo.append("\n");
						}

						contenidoArchivo.append((regConsMonitor.get(1) == null ? "" : (String) regConsMonitor.get(1)) + ",");
						contenidoArchivo.append((regConsMonitor.get(4) == null ? "" : (String) regConsMonitor.get(4)) + ",");
						contenidoArchivo.append((regConsMonitor.get(5) == null ? "" : (String) regConsMonitor.get(5)));
						contenidoArchivo.append("\n");

					}
				}
			} else {

				lstConsMonitor = consultaMonitor(fec_ini, fec_fin, estatus, "", cvePrograma);

				if (lstConsMonitor != null && lstConsMonitor.size() > 0) {
					for (int x = 0; x < lstConsMonitor.size(); x++) {
						regConsMonitor = (List) lstConsMonitor.get(x);
						if (x == 0) {
							contenidoArchivo.append("Programa:," + descPrograma + "\n");
							contenidoArchivo.append("Fecha de Vencimiento Nafin,");
							contenidoArchivo.append("Fecha y Hora del Ultimo Registro Enviado por FIDE,");
							contenidoArchivo.append("Folio de Operacion,");
							contenidoArchivo.append("Total de Registros,");
							contenidoArchivo.append("Monto");
							contenidoArchivo.append("\n");
						}

						contenidoArchivo.append((regConsMonitor.get(1) == null ? "" : (String) regConsMonitor.get(1)) + ",");
						contenidoArchivo.append((regConsMonitor.get(2) == null ? "" : (String) regConsMonitor.get(2)) + ",");
						if ("NPR".equals(estatus))
							contenidoArchivo.append("N/A,");
						else
							contenidoArchivo.append((regConsMonitor.get(3) == null ? "" : (String) regConsMonitor.get(3)) +
															",");
						contenidoArchivo.append((regConsMonitor.get(4) == null ? "" : (String) regConsMonitor.get(4)) + ",");
						contenidoArchivo.append((regConsMonitor.get(5) == null ? "" : (String) regConsMonitor.get(5)));
						contenidoArchivo.append("\n");

					}
				}
			}

			if (!archivo.make(contenidoArchivo.toString(), rutaFisica, ".csv")) {
				throw new NafinException("SIST0001");
			} else {
				nombreArchivo = archivo.nombre;
			}
			return nombreArchivo;

		} catch (Throwable e) {
			log.info("generaCSV(ERROR) :: " + e.toString());
			e.printStackTrace();
			throw new AppException("Error al generar archivo csv del monitor de fondojr ", e);
		} finally {
			log.info("generaCSV(S) ::..");
		}
	} //generaCSVMonitor


	/**
	 * Obtiene el detalle de cada registro arrojado en la consulta general de Monitor
	 * @throws com.netro.exception.NafinException
	 * @return List lstDetmonitor
	 * @param fec_vencimiento - fecha de vencimiento
	 */
	public int numRegDetMonitorJS(String cg_estatus, String fec_vencimiento, String cvePrograma) throws NafinException {
		System.out.println("FondoJuniorBean::consDetMonitor(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qrySentence = "";
		int numReg = 0;
		boolean commit = true;

		try {
			con.conexionDB();
			if (!cg_estatus.equals("R") && !cg_estatus.equals("T") && !cg_estatus.equals("RT") &&
				 !cg_estatus.equals("NPR")) {
				qrySentence =
							  " SELECT count(1) numReg " + "    FROM com_pagos_fide pf " +
							  "   WHERE pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') " + "   AND pf.cg_estatus in ('P','NP') " +
							  "   AND pf.cg_estatus_proc in ('N','R') " + "	 AND pf.ic_programa_fondojr = ? " +
							  "   ORDER BY pf.df_registro_fide ";
			} else if (cg_estatus.equals("R")) {
				qrySentence = "SELECT count(1) numReg " + "    FROM COM_PAGOS_FIDE PF  " + "   WHERE PF.CG_ESTATUS = ?  ";
				if (!"".equals(fec_vencimiento)) {
					qrySentence += "  AND pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') ";
				}
				qrySentence +=
							  "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND CG_ESTATUS_PROC = 'N'  " +
							  "	 AND pf.ic_programa_fondojr = ? " + "   ORDER BY pf.df_registro_fide ";
			} else if (cg_estatus.equals("T")) {
				qrySentence =
							  "SELECT count(1) numReg " + "    FROM COM_PAGOS_FIDE PF  " + "   WHERE PF.CG_ESTATUS = ?  " +
							  "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND pf.CG_ESTATUS_PROC = 'N'  " +
							  "	 AND pf.ic_programa_fondojr = ? " + "   ORDER BY pf.df_registro_fide ";
			} else if (cg_estatus.equals("RT")) {
				//cg_estatus = "R";
				qrySentence =
							  "SELECT count(1) numReg " + "  FROM com_pagos_fide pf  " + " WHERE pf.cg_estatus = ?  " +
							  " AND pf.cg_estatus_proc = 'N' " +
							  " AND 'RC' = fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,  " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento)  " + " AND pf.ic_programa_fondojr = ? " +
							  "   ORDER BY pf.df_registro_fide ";

			} else if (cg_estatus.equals("NPR")) {
				qrySentence =
							  "SELECT count(1) numReg " + "    FROM com_cred_np_rechazados pf " +
							  "   WHERE pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') " + "   AND pf.cg_estatus in ('NPR') " +
							  "	 AND pf.ic_programa_fondojr = ? " + "   ORDER BY pf.df_registro_fide ";
			}

			System.out.println("consDetMonitor:qrySentence>>" + qrySentence);
			ps = con.queryPrecompilado(qrySentence);
			int x = 0;
			if (!cg_estatus.equals("R") && !cg_estatus.equals("T") && !cg_estatus.equals("RT")) {
				ps.setString(++x, fec_vencimiento);
			} else if (cg_estatus.equals("R")) {
				ps.setString(++x, cg_estatus);
				if (!"".equals(fec_vencimiento)) {
					ps.setString(++x, fec_vencimiento);
				}
			} else if (cg_estatus.equals("T")) {
				ps.setString(++x, cg_estatus);
			} else if (cg_estatus.equals("RT")) {
				cg_estatus = "R";
				ps.setString(++x, cg_estatus);
			} else if (cg_estatus.equals("NPR")) {
				ps.setString(++x, fec_vencimiento);
			}
			ps.setLong(++x, Long.parseLong(cvePrograma));

			rs = ps.executeQuery();

			if (rs != null && rs.next()) {
				numReg = rs.getInt("numReg");
			}

			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			System.out.println("FondoJuniorBean::consDetMonitor(S)");
			return numReg;
		} catch (Exception e) {
			commit = false;
			e.printStackTrace();
			System.out.println("FondoJuniorBean::consDetMonitor(Error)");
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Obtiene el detalle de cada registro arrojado en la consulta general de Monitor
	 * @throws com.netro.exception.NafinException
	 * @return List lstDetmonitor
	 * @param fec_vencimiento - fecha de vencimiento
	 */
	public List consDetMonitorJS(String cg_estatus, String fec_vencimiento, String cvePrograma,
										  int numRegistros) throws NafinException {
		System.out.println("FondoJuniorBean::consDetMonitor(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDetmonitor = new ArrayList();
		List lstRegDetMonitor = null;
		String qrySentence = "";
		String qryRownum = "";
		boolean seValidaReg = false;
		boolean commit = true;

		try {
			con.conexionDB();
			if (numRegistros > 1000)
				qryRownum = " AND rownum < 1001 ";

			if (!cg_estatus.equals("R") && !cg_estatus.equals("T") && !cg_estatus.equals("RT") &&
				 !cg_estatus.equals("NPR")) {
				qrySentence =
							  " SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     decode(pf.cg_estatus_proc,'R','NPR',pf.cg_estatus) cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " + //F027-2011 FVR AJUSTE NPR
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM com_pagos_fide pf " +
							  "   WHERE pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') " + "   AND pf.cg_estatus in ('P','NP') " +
							  "   AND pf.cg_estatus_proc in ('N','R','P') " + "	 AND pf.ic_programa_fondojr = ? " + qryRownum +
							  "   ORDER BY pf.df_registro_fide ";
				seValidaReg = true;
			} else if (cg_estatus.equals("R")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM COM_PAGOS_FIDE PF  " +
							  "   WHERE PF.CG_ESTATUS = ?  ";
				if (!"".equals(fec_vencimiento)) {
					qrySentence += "  AND pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') ";
				}
				qrySentence +=
							  "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND CG_ESTATUS_PROC in ('N','P')  " +
							  "	 AND pf.ic_programa_fondojr = ? " + qryRownum + "   ORDER BY pf.df_registro_fide ";
			} else if (cg_estatus.equals("T")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM COM_PAGOS_FIDE PF  " +
							  "   WHERE PF.CG_ESTATUS = ?  " +
							  "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND pf.CG_ESTATUS_PROC = 'N'  " +
							  "	 AND pf.ic_programa_fondojr = ? " + qryRownum + "   ORDER BY pf.df_registro_fide ";
			} else if (cg_estatus.equals("RT")) {
				//cg_estatus = "R";
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "  FROM com_pagos_fide pf  " +
							  " WHERE pf.cg_estatus = ?  " + " AND pf.cg_estatus_proc = 'N' " +
							  " AND 'RC' = fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,  " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento)  " + " AND pf.ic_programa_fondojr = ? " +
							  qryRownum + "   ORDER BY pf.df_registro_fide ";

			} else if (cg_estatus.equals("NPR")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " + //F027-2011 FVR AJUSTE NPR
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide " +
							  "    FROM com_cred_np_rechazados pf " + "   WHERE pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') " +
							  "   AND pf.cg_estatus in ('NPR') " + "	 AND pf.ic_programa_fondojr = ? " + qryRownum +
							  "   ORDER BY pf.df_registro_fide ";
			}

			System.out.println("consDetMonitor:qrySentence>>" + qrySentence);
			ps = con.queryPrecompilado(qrySentence);
			int x = 0;
			if (!cg_estatus.equals("R") && !cg_estatus.equals("T") && !cg_estatus.equals("RT")) {
				ps.setString(++x, fec_vencimiento);
			} else if (cg_estatus.equals("R")) {
				ps.setString(++x, cg_estatus);
				if (!"".equals(fec_vencimiento)) {
					ps.setString(++x, fec_vencimiento);
				}
			} else if (cg_estatus.equals("T")) {
				ps.setString(++x, cg_estatus);
			} else if (cg_estatus.equals("RT")) {
				cg_estatus = "R";
				ps.setString(++x, cg_estatus);
			} else if (cg_estatus.equals("NPR")) {
				ps.setString(++x, fec_vencimiento);
			}
			ps.setLong(++x, Long.parseLong(cvePrograma));

			rs = ps.executeQuery();
			while (rs != null && rs.next()) {
				lstRegDetMonitor = new ArrayList();
				HashMap hmDet = new HashMap();
				String estatus = rs.getString("cg_estatus") == null ? "" : rs.getString("cg_estatus");

				if ("NP".equals(rs.getString("cg_estatus"))) {
					if ("N".equals(rs.getString("estatus_proc")) && seValidaReg) {
						String statusProc =
											setAmortRechazada(con, rs.getString("ig_prestamo"), rs.getString("fecha_venc"),
																	rs.getString("ig_disposicion"), rs.getString("fg_totalvencimiento"),
																	cvePrograma);
						estatus = "R".equals(statusProc) ? "NPR" : estatus;
					}
				}

				/*0*/hmDet.put("FECHA_VENC", rs.getString("fecha_venc") == null ? "" : rs.getString("fecha_venc"));
				/*1*/hmDet.put("IG_PRESTAMO", rs.getString("ig_prestamo") == null ? "" : rs.getString("ig_prestamo"));
				/*2*/hmDet.put("IG_CLIENTE", rs.getString("ig_cliente") == null ? "" : rs.getString("ig_cliente"));
				/*3*/hmDet.put("IG_DISPOSICION",
									rs.getString("ig_disposicion") == null ? "" : rs.getString("ig_disposicion"));
				/*4*/hmDet.put("FG_AMORTIZACION",
									rs.getString("fg_amortizacion") == null ? "" : rs.getString("fg_amortizacion"));
				/*5*/hmDet.put("FG_INTERES", rs.getString("fg_interes") == null ? "" : rs.getString("fg_interes"));
				/*6*/hmDet.put("FG_TOTALVENCIMIENTO",
									rs.getString("fg_totalvencimiento") == null ? "" : rs.getString("fg_totalvencimiento"));
				/*7*/hmDet.put("DF_PAGO_CLIENTE",
									rs.getString("df_pago_cliente") == null ? "" : rs.getString("df_pago_cliente"));
				/*8*/hmDet.put("CG_ESTATUS", estatus);
				/*9*/hmDet.put("DF_PERIODOFIN", rs.getString("df_periodofin") == null ? "" : rs.getString("df_periodofin"));
				/*10*/hmDet.put("IG_NUMERO_DOCTO",
									 rs.getString("ig_numero_docto") == null ? "" : rs.getString("ig_numero_docto"));
				/*11*/hmDet.put("DF_REGISTRO_FIDE",
									 rs.getString("df_registro_fide") == null ? "" : rs.getString("df_registro_fide"));
				lstDetmonitor.add(hmDet);

			}
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			System.out.println("FondoJuniorBean::consDetMonitor(S)");
			return lstDetmonitor;
		} catch (Exception e) {
			commit = false;
			e.printStackTrace();
			System.out.println("FondoJuniorBean::consDetMonitor(Error)");
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Obtiene el detalle de cada registro arrojado en la consulta general de Monitor
	 * @throws com.netro.exception.NafinException
	 * @return List lstDetmonitor
	 * @param fec_vencimiento - fecha de vencimiento
	 */
	private HashMap obtenerInfoTxtMonitorIni(AccesoDB con, String cg_estatus, String fec_vencimiento,
														  String cvePrograma) throws NafinException {
		System.out.println("FondoJuniorBean::obtenerInfoTxtMonitorIni(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qrySentence = "";
		String qryRownum = "";
		boolean seValidaReg = false;
		boolean commit = true;
		HashMap hmCursores = new HashMap();

		try {
			if (!cg_estatus.equals("R") && !cg_estatus.equals("T") && !cg_estatus.equals("RT") &&
				 !cg_estatus.equals("NPR")) {
				qrySentence =
							  " SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     decode(pf.cg_estatus_proc,'R','NPR',pf.cg_estatus) cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " + //F027-2011 FVR AJUSTE NPR
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM com_pagos_fide pf " +
							  "   WHERE pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') " + "   AND pf.cg_estatus in ('P','NP') " +
							  "   AND pf.cg_estatus_proc in ('N','R') " + "	 AND pf.ic_programa_fondojr = ? " + qryRownum +
							  "   ORDER BY pf.df_registro_fide ";
				seValidaReg = true;
			} else if (cg_estatus.equals("R")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM COM_PAGOS_FIDE PF  " +
							  "   WHERE PF.CG_ESTATUS = ?  ";
				if (!"".equals(fec_vencimiento)) {
					qrySentence += "  AND pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') ";
				}
				qrySentence +=
							  "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND CG_ESTATUS_PROC = 'N'  " +
							  "	 AND pf.ic_programa_fondojr = ? " + qryRownum + "   ORDER BY pf.df_registro_fide ";
			} else if (cg_estatus.equals("T")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "    FROM COM_PAGOS_FIDE PF  " +
							  "   WHERE PF.CG_ESTATUS = ?  " +
							  "   AND 'RC' != fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,   " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento) " + "   AND pf.CG_ESTATUS_PROC = 'N'  " +
							  "	 AND pf.ic_programa_fondojr = ? " + qryRownum + "   ORDER BY pf.df_registro_fide ";
			} else if (cg_estatus.equals("RT")) {
				//cg_estatus = "R";
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " +
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide, " +
							  "     pf.ig_folio folio, pf.cg_estatus_proc estatus_proc " + "  FROM com_pagos_fide pf  " +
							  " WHERE pf.cg_estatus = ?  " + " AND pf.cg_estatus_proc = 'N' " +
							  " AND 'RC' = fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, pf.ig_prestamo,  " +
							  "                      pf.ig_disposicion, pf.fg_totalvencimiento)  " + " AND pf.ic_programa_fondojr = ? " +
							  qryRownum + "   ORDER BY pf.df_registro_fide ";

			} else if (cg_estatus.equals("NPR")) {
				qrySentence =
							  "SELECT TO_CHAR(pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " +
							  "	   pf.ig_prestamo, pf.ig_cliente, pf.ig_disposicion,  " +
							  "     pf.fg_amortizacion fg_amortizacion, pf.fg_interes fg_interes,  " +
							  "     pf.fg_totalvencimiento fg_totalvencimiento, to_char(pf.df_pago_cliente,'dd/mm/yyyy') df_pago_cliente,  " +
							  "     pf.cg_estatus cg_estatus, to_char(pf.df_periodofin,'dd/mm/yyyy') df_periodofin,  " + //F027-2011 FVR AJUSTE NPR
							  "		pf.ig_numero_docto ig_numero_docto, to_char(pf.df_registro_fide,'dd/mm/yyyy hh:mi:ss') df_registro_fide " +
							  "    FROM com_cred_np_rechazados pf " + "   WHERE pf.com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') " +
							  "   AND pf.cg_estatus in ('NPR') " + "	 AND pf.ic_programa_fondojr = ? " + qryRownum +
							  "   ORDER BY pf.df_registro_fide ";
			}

			System.out.println("consDetMonitor:qrySentence>>" + qrySentence);
			ps = con.queryPrecompilado(qrySentence);
			int x = 0;
			if (!cg_estatus.equals("R") && !cg_estatus.equals("T") && !cg_estatus.equals("RT")) {
				ps.setString(++x, fec_vencimiento);
			} else if (cg_estatus.equals("R")) {
				ps.setString(++x, cg_estatus);
				if (!"".equals(fec_vencimiento)) {
					ps.setString(++x, fec_vencimiento);
				}
			} else if (cg_estatus.equals("T")) {
				ps.setString(++x, cg_estatus);
			} else if (cg_estatus.equals("RT")) {
				cg_estatus = "R";
				ps.setString(++x, cg_estatus);
			} else if (cg_estatus.equals("NPR")) {
				ps.setString(++x, fec_vencimiento);
			}
			ps.setLong(++x, Long.parseLong(cvePrograma));

			rs = ps.executeQuery();
			/*while(rs!=null && rs.next()){
					lstRegDetMonitor = new ArrayList();
					HashMap hmDet = new HashMap();
					String estatus = rs.getString("cg_estatus")==null?"":rs.getString("cg_estatus");

					if("NP".equals(rs.getString("cg_estatus"))){
						if("N".equals(rs.getString("estatus_proc")) && seValidaReg){
							String statusProc = setAmortRechazada(con, rs.getString("ig_prestamo"), rs.getString("fecha_venc"), rs.getString("ig_disposicion"), rs.getString("fg_totalvencimiento"), cvePrograma);
							estatus = "R".equals(statusProc)? "NPR":estatus;
						}
					}

					hmDet.put("FECHA_VENC", rs.getString("fecha_venc")==null?"":rs.getString("fecha_venc"));
					hmDet.put("IG_PRESTAMO", rs.getString("ig_prestamo")==null?"":rs.getString("ig_prestamo"));
					hmDet.put("IG_CLIENTE", rs.getString("ig_cliente")==null?"":rs.getString("ig_cliente"));
					hmDet.put("IG_DISPOSICION", rs.getString("ig_disposicion")==null?"":rs.getString("ig_disposicion"));
					hmDet.put("FG_AMORTIZACION", rs.getString("fg_amortizacion")==null?"":rs.getString("fg_amortizacion"));
					hmDet.put("FG_INTERES", rs.getString("fg_interes")==null?"":rs.getString("fg_interes"));
					hmDet.put("FG_TOTALVENCIMIENTO", rs.getString("fg_totalvencimiento")==null?"":rs.getString("fg_totalvencimiento"));
					hmDet.put("DF_PAGO_CLIENTE", rs.getString("df_pago_cliente")==null?"":rs.getString("df_pago_cliente"));
					hmDet.put("CG_ESTATUS", estatus);
					hmDet.put("DF_PERIODOFIN", rs.getString("df_periodofin")==null?"":rs.getString("df_periodofin"));
					hmDet.put("IG_NUMERO_DOCTO", rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto"));
					hmDet.put("DF_REGISTRO_FIDE", rs.getString("df_registro_fide")==null?"":rs.getString("df_registro_fide"));
					lstDetmonitor.add(hmDet);

				}*/
			//if(rs!=null)rs.close();
			//if(ps!=null)ps.close();

			hmCursores.put("RESULTSET", rs);
			hmCursores.put("PREPAREDSTATEMENT", ps);
			return hmCursores;
		} catch (Exception e) {
			commit = false;
			e.printStackTrace();
			System.out.println("FondoJuniorBean::obtenerInfoTxtMonitorIni(Error)");
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				//con.cierraConexionDB();
			}
			System.out.println("FondoJuniorBean::obtenerInfoTxtMonitorIni(S)");
		}
	}


	/**
	 * Metodo que genera un acuse de la informacion que ya fue autorizada
	 * @throws com.netro.exception.NafinException
	 * @return List
	 * @param fec_venc - fecha que indica el vencimiento de los registros autorizados
	 * @param usuario -  persona que realizo la autorizacion
	 * @param folio - numero generado pro la autorizacion
	 */
	public List consAcuseMonitorValidadoJS(String folio, String usuario, String fec_venc) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstAcuseFinal = new ArrayList();
		List lstAcuseGral = new ArrayList();
		List lstCabecera = new ArrayList();
		HashMap hmRegAcuse = new HashMap();
		String ultimo_reg = "";

		BigDecimal montoTotalPyNP = new BigDecimal("0.00");
		BigDecimal montoTotalPrepago = new BigDecimal("0.00");
		BigDecimal montoTotalReemb = new BigDecimal("0.00");
		BigDecimal montoTotalRec = new BigDecimal("0.00");
		int sumaTotalPyNP = 0;
		int sumaTotalReemb = 0;
		int sumaTotalPrepago = 0;
		int sumaTotalRec = 0;
		try {
			con.conexionDB();
			/*SE REALIZA CONSULTA DE LOS REGISTROS INSERTADOS PARA MOSTRAR ACUSE*/
			String qryCons =
						"select to_char(max(pf.df_registro_fide),'dd/mm/yyy hh:mi:ss') ultimo_reg,  " +
						"		count(pf.cg_estatus) total_reg, sum(pf.fg_totalvencimiento) monto_tot,  " +
						"		pf.cg_estatus estatus, pf.ig_folio folio " + "from com_pagos_fide_val pf  " + "  where pf.ig_folio = ? " +
						"  group by pf.ig_folio, pf.cg_estatus " + " union all " +
						" select TO_CHAR (MAX (pf.df_registro_fide), 'dd/mm/yyy hh:mi:ss') ultimo_reg,   " +
						" 		count(pf.cg_estatus) total_reg, sum(pf.fg_totalvencimiento) monto_tot,   " +
						" 		pf.cg_estatus estatus, pf.ig_folio folio  " + " from COM_CRED_NP_RECHAZADOS pf   " +
						"  where pf.ig_folio = ? " + "  group by pf.ig_folio, pf.cg_estatus ";

			System.out.println("qryCons>>>>>>" + qryCons);
			ps = con.queryPrecompilado(qryCons);
			ps.setLong(1, Long.parseLong(folio));
			ps.setLong(2, Long.parseLong(folio));
			rs = ps.executeQuery();
			if (rs != null) {
				HashMap hmCabecera = new HashMap();
				hmCabecera.put("FOLIO", folio);
				hmCabecera.put("USUARIO", usuario);
				lstCabecera.add(hmCabecera);
				lstAcuseFinal.add(lstCabecera);
				while (rs.next()) {
					if (rs.getString("estatus").equals("P") || rs.getString("estatus").equals("NP") ||
						 rs.getString("estatus").equals("NPR")) {
						montoTotalPyNP = montoTotalPyNP.add(new BigDecimal(rs.getString("monto_tot")));
						sumaTotalPyNP += rs.getInt("total_reg");
						ultimo_reg = rs.getString("ultimo_reg");
					} else if (rs.getString("estatus").equals("R")) {
						montoTotalReemb = montoTotalReemb.add(new BigDecimal(rs.getString("monto_tot")));
						sumaTotalReemb += rs.getInt("total_reg");
						/**/
						hmRegAcuse = new HashMap();
						hmRegAcuse.put("TOTALREG", String.valueOf(sumaTotalReemb));
						hmRegAcuse.put("MONTO", montoTotalReemb.toPlainString());
						hmRegAcuse.put("TITULO", "reembolsos");
						hmRegAcuse.put("FECULTIMOREG", rs.getString("ultimo_reg"));
						hmRegAcuse.put("FECVENC", "");
						lstAcuseGral.add(hmRegAcuse);
					} else if (rs.getString("estatus").equals("T")) {
						montoTotalPrepago = montoTotalPrepago.add(new BigDecimal(rs.getString("monto_tot")));
						sumaTotalPrepago += rs.getInt("total_reg");
						/**/
						hmRegAcuse = new HashMap();
						hmRegAcuse.put("TOTALREG", String.valueOf(sumaTotalPrepago));
						hmRegAcuse.put("MONTO", montoTotalPrepago.toPlainString());
						hmRegAcuse.put("TITULO", "prepagos");
						hmRegAcuse.put("FECULTIMOREG", rs.getString("ultimo_reg"));
						hmRegAcuse.put("FECVENC", "");
						lstAcuseGral.add(hmRegAcuse);
					} else if (rs.getString("estatus").equals("RC")) {
						montoTotalRec = montoTotalRec.add(new BigDecimal(rs.getString("monto_tot")));
						sumaTotalRec += rs.getInt("total_reg");
						/**/
						hmRegAcuse = new HashMap();
						hmRegAcuse.put("TOTALREG", String.valueOf(sumaTotalRec));
						hmRegAcuse.put("MONTO", montoTotalRec.toPlainString());
						hmRegAcuse.put("TITULO", "recuperacion");
						hmRegAcuse.put("FECULTIMOREG", rs.getString("ultimo_reg"));
						hmRegAcuse.put("FECVENC", "");
						lstAcuseGral.add(hmRegAcuse);
					}
				} //cieere while (rs)
				/**/
				if (sumaTotalPyNP > 0) {
					hmRegAcuse = new HashMap();
					hmRegAcuse.put("TOTALREG", String.valueOf(sumaTotalPyNP));
					hmRegAcuse.put("MONTO", montoTotalPyNP.toPlainString());
					hmRegAcuse.put("TITULO", "total");
					hmRegAcuse.put("FECULTIMOREG", ultimo_reg);
					hmRegAcuse.put("FECVENC", fec_venc);
					lstAcuseGral.add(hmRegAcuse);
				}
				lstAcuseFinal.add(lstAcuseGral);
			} //cierre if(rs)
			if (rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			return lstAcuseFinal;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * M�todo que realiza las consultas para obtener los Detalles de Credito a Validar
	 * @param ver_enlace tipo de detalle a mostrar
	 * @param estatus, proc, condiciones de consultas
	 * @return verdetalleTemp Lista con la informaci�n de la consulta.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since Migracion EXTJSFondo Junior
	 * @author Fabian
	 */
	public int numRegDetalleDeCreditosJS(String ver_enlace, String estatus, String estatus2, String proc, String fec_venc,
													 String cvePrograma) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();

		List varBind = new ArrayList();

		int numRegistros = 0;
		double montoTotal = 0.0;

		System.out.println("��� FondoJuniorBean �� numRegDetalleDeCreditosJS(I) ���");
		System.out.println("��� ver_enlace �� " + ver_enlace + " ���");
		System.out.println("��� estatus �� " + estatus + " ���");
		try {
			con.conexionDB();

			if ("Recuperaciones".equals(ver_enlace)) {
				strSQL.append("SELECT count(1) numReg, sum(tmp.fg_totalvencimiento) AS capital_mas_interes " +
								  "  FROM comvis_vencimiento_fide vista,    " + "       com_pagos_fide tmp    " +
								  " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago  " +
								  "	AND vista.ig_cliente(+)  = tmp.ig_cliente  " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion  " +
								  "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento  " +
								  "   AND vista.ig_prestamo(+) = tmp.ig_prestamo  " + " AND tmp.CG_ESTATUS = ?   " +
								  " AND 'RC' = FN_ES_RECUPERACION_FONDOJR (tmp.COM_FECHAPROBABLEPAGO, tmp.IG_PRESTAMO,    " +
								  "                      tmp.IG_DISPOSICION, tmp.FG_TOTALVENCIMIENTO)    " +
								  " AND tmp.ic_programa_fondojr = ? ");
				varBind.add(estatus);
				varBind.add(new Long(cvePrograma));

				//verdetalle_temp.add(titulo);
			} else if ("NoEncontrados".equals(ver_enlace)) {
				strSQL.append("SELECT count(1) numReg, sum(tmp.fg_totalvencimiento) AS capital_mas_interes " +
								  "       FROM com_pagos_fide tmp      " + "WHERE  tmp.cg_estatus in (?,?)  ");
				if ("RC".equals(estatus2)) {
					strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				}
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
				strSQL.append("  AND NOT EXISTS  " + "    ( " + "     SELECT vp.ig_prestamo  " +
								  "              FROM comvis_vencimiento_fide vp                   " +
								  "             WHERE vp.com_fechaprobablepago = tmp.com_fechaprobablepago " +
								  "               AND vp.ig_prestamo = tmp.ig_prestamo " +
								  "               AND vp.ig_cliente  = tmp.ig_cliente " +
								  "               AND vp.ig_disposicion = tmp.ig_disposicion " +
								  "               AND vp.fg_totalvencimiento = tmp.fg_totalvencimiento " + "     ) " +
								  "	  AND tmp.ic_programa_fondojr = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(estatus2);
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma));


			} else if ("NoEnviados".equals(ver_enlace)) {
				strSQL.append("SELECT count(1) numReg, sum(vista.fg_totalvencimiento) AS capital_mas_interes " +
								  "       FROM comvis_vencimiento_fide vista     " + "WHERE NOT EXISTS  " + "    ( " +
								  "     SELECT tmp.ig_prestamo  " + "              FROM com_pagos_fide tmp                   " +
								  "             WHERE tmp.COM_FECHAPROBABLEPAGO = vista.COM_FECHAPROBABLEPAGO " +
								  "               AND tmp.ig_prestamo = vista.ig_prestamo " +
								  "               AND tmp.ig_cliente  = vista.ig_cliente " +
								  "               AND tmp.ig_disposicion = vista.ig_disposicion " +
								  "               AND tmp.fg_totalvencimiento = vista.fg_totalvencimiento " +
								  "               AND tmp.CG_ESTATUS in (?,?) " + "					 AND tmp.ic_programa_fondojr = ? " + //FODEA 026-2010 FVR
								  "     ) ");
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append(" AND vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");

				strSQL.append(" AND vista.ic_programa_fondojr = ? "); //FODEA 026-2010 FVR

				varBind.add(estatus);
				varBind.add(estatus2);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR


			} else if ("Vencimientos".equals(ver_enlace)) {
				strSQL.append("SELECT count(1) numReg, sum(vista.fg_totalvencimiento) AS capital_mas_interes " +
								  "  FROM comvis_vencimiento_fide vista " + " WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
								  " AND vista.ic_programa_fondojr = ? "); //FODEA 026-2010 FVR);
				varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR


			} else {
				strSQL.append("SELECT count(1) numReg, sum(tmp.fg_totalvencimiento) AS capital_mas_interes " +
								  "  FROM comvis_vencimiento_fide vista,   " + "       com_pagos_fide tmp  " +
								  " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
								  "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
								  "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
								  "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "	 AND tmp.ic_programa_fondojr = ? ");
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
			}

			if ("Enviados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in ('P','NP') " +
								  " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
				varBind.add(fec_venc);

			}
			if ("RembolsosPrepagosTotales".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in (?) "); //FODEA 026-2010 FVR SE QUITA 'R'
				if ("R".equals(estatus)) {
					varBind.add(estatus);
					if (!fec_venc.equals("") && fec_venc.length() > 2) {
						strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
						varBind.add(fec_venc);
					}
					strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("RC".equals(estatus)) {
					varBind.add(estatus2);
					strSQL.append(" AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("T".equals(estatus)) {
					varBind.add(estatus);
				}

			}

			if ("Prepagos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? ");
				varBind.add(estatus);
			}
			if ("Pagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);

			}
			if ("NoPagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);

			}
			if ("Prepagados".equals(ver_enlace)) { //MODIFICAION F006-2014 REQ 1
				strSQL.append(" AND tmp.cg_estatus in (?, ?)  ");
				varBind.add(estatus);
				varBind.add(estatus2);

				if (!fec_venc.equals("") && fec_venc.length() > 2) {
					strSQL.append(" AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
					varBind.add(fec_venc);
				}

				strSQL.append(" AND tmp.cg_estatus_proc = ? ");
				varBind.add(proc);

			}
			if ("NoPagadosRech".equals(ver_enlace)) { //F027-2011 FVR
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);

			}
			if ("Reembolsos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " +
								  " AND 'RC' != fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, tmp.ig_prestamo,   " +
								  "                      tmp.ig_disposicion, tmp.fg_totalvencimiento)  ");
				varBind.add(estatus);
				if (!fec_venc.equals("") && fec_venc.length() > 2) {
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
					varBind.add(fec_venc);
				}
			}
			if ("ConError".equals(ver_enlace)) {
				if (!fec_venc.equals("") && fec_venc.length() > 2 && !"R".equals(estatus)) {
					strSQL.append(" AND tmp.cg_estatus in (?,?) " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
									  " AND tmp.cg_error is not null ");
					//varBind.add(proc);
					strSQL.append(" UNION ALL SELECT count(1) numReg, sum(vista.fg_totalvencimiento) AS capital_mas_interes " +
									  "  FROM comvis_vencimiento_fide vista " + "	WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
									  "	AND vista.ic_programa_fondojr = ? " + "	AND NOT EXISTS ( " + "          SELECT 1 " +
									  "            FROM com_pagos_fide c " +
									  "           WHERE c.com_fechaprobablepago = vista.com_fechaprobablepago " +
									  "             AND c.ig_prestamo = vista.ig_prestamo " + "             AND c.ig_cliente = vista.ig_cliente " +
									  "             AND c.ig_disposicion = vista.ig_disposicion " +
									  "             AND c.fg_totalvencimiento = vista.fg_totalvencimiento " + "				  AND c.cg_estatus in (?,?) " +
									  "				  AND c.ic_programa_fondojr = ?) ");
					varBind.add("P");
					varBind.add("NP");
					varBind.add(fec_venc);
					varBind.add(fec_venc);
					varBind.add(new Long(cvePrograma));
					varBind.add("P");
					varBind.add("NP");
					varBind.add(new Long(cvePrograma));
				} else {
					if ("R".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus);
						if (!fec_venc.equals("") && fec_venc.length() > 2) {
							strSQL.append(" AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
							varBind.add(fec_venc);
						}
						strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else if ("RC".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus2);

						strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add("T");
					}
				}

			}

			System.out.println("strSQL.toString()>>>>>>" + strSQL.toString());
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {

				numRegistros += rst.getInt("numReg");
				montoTotal += rst.getDouble("capital_mas_interes");

			}

			rst.close();
			pst.close(); //se agrega por correccion a errore del mes del 09/2010

			return numRegistros;

		} catch (Exception e) {
			System.out.println("��� FondoJuniorBean �� numRegDetalleDeCreditosJS -ERROR- ���");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(true); //se agrega por correccion a errore del mes del 09/2010
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("��� strSQL ��� " + strSQL);
			System.out.println("��� varBind �� " + varBind + " ���");
			System.out.println("��� FondoJuniorBean �� numRegDetalleDeCreditosJS(F) ���");
		}
	}


	/**
	 * M�todo que realiza las consultas para obtener los Detalles de Credito a Validar
	 * @param ver_enlace tipo de detalle a mostrar
	 * @param estatus, proc, condiciones de consultas
	 * @return verdetalleTemp Lista con la informaci�n de la consulta.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since Migracion EXTJSFondo Junior
	 * @author Fabian
	 */
	public List ObtenerDetalleDeCreditosJS(String ver_enlace, String estatus, String estatus2, String proc,
														String fec_venc, String cvePrograma, int numRegistros) throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		String qryRownum = "";

		List varBind = new ArrayList();
		List verdetalle_temp = new ArrayList();
		List titulo = new ArrayList();

		System.out.println("��� FondoJuniorBean �� ObtenerDetalleDeCreditos(I) ���");
		System.out.println("��� ver_enlace �� " + ver_enlace + " ���");
		System.out.println("��� estatus �� " + estatus + " ���");
		try {
			con.conexionDB();
			if (numRegistros > 1000)
				qryRownum = " AND rownum < 1001 ";

			if ("Recuperaciones".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,   " +
								  "       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,   " +
								  "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,   " +
								  "       tmp.ig_prestamo AS prestamo,   " + "       tmp.ig_cliente AS numsirac,   " +
								  "       NVL(vista.cg_nombrecliente,'') AS cliente,   " + "       tmp.ig_disposicion AS numcuota,   " +
								  "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error,   " +
								  "		  tmp.cg_estatus as estatus " + "  FROM comvis_vencimiento_fide vista,    " +
								  "       com_pagos_fide tmp    " + " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago  " +
								  "	AND vista.ig_cliente(+)  = tmp.ig_cliente  " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion  " +
								  "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento  " +
								  "   AND vista.ig_prestamo(+) = tmp.ig_prestamo  " + " AND tmp.CG_ESTATUS = ?   " +
								  " AND 'RC' = FN_ES_RECUPERACION_FONDOJR (tmp.COM_FECHAPROBABLEPAGO, tmp.IG_PRESTAMO,    " +
								  "                      tmp.IG_DISPOSICION, tmp.FG_TOTALVENCIMIENTO)    " +
								  " AND tmp.ic_programa_fondojr = ? " + qryRownum);
				varBind.add(estatus);
				varBind.add(new Long(cvePrograma));
				titulo.add("Recuperaciones Recibidas");
				//verdetalle_temp.add(titulo);
			} else if ("NoEncontrados".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (tmp.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
								  "'' AS fechaoperacion," + "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,   " +
								  "       tmp.IG_PRESTAMO AS prestamo, " + "       tmp.IG_CLIENTE AS numsirac, " + "'' AS cliente, " +
								  "       tmp.IG_DISPOSICION AS numcuota, " + "       tmp.fg_totalvencimiento AS capital_mas_interes, " +
								  "		  tmp.cg_estatus as estatus " + "       FROM com_pagos_fide tmp      " +
								  "WHERE  tmp.cg_estatus in (?,?)  ");
				if ("RC".equals(estatus2)) {
					strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				}
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
				strSQL.append("  AND NOT EXISTS  " + "    ( " + "     SELECT vp.ig_prestamo  " +
								  "              FROM comvis_vencimiento_fide vp                   " +
								  "             WHERE vp.com_fechaprobablepago = tmp.com_fechaprobablepago " +
								  "               AND vp.ig_prestamo = tmp.ig_prestamo " +
								  "               AND vp.ig_cliente  = tmp.ig_cliente " +
								  "               AND vp.ig_disposicion = tmp.ig_disposicion " +
								  "               AND vp.fg_totalvencimiento = tmp.fg_totalvencimiento " + "     ) " +
								  "	  AND tmp.ic_programa_fondojr = ? " + qryRownum);
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(estatus2);
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma));
				titulo.add("Registros No encontrados");
				//verdetalle_temp.add(titulo);

			} else if ("NoEnviados".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (vista.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
								  "       TO_CHAR (vista.DF_FECHAOPERA, 'dd/mm/yyyy') AS fechaoperacion,   " + "'' AS fechapago, " +
								  "       vista.IG_PRESTAMO AS prestamo, " + "       vista.IG_CLIENTE AS numsirac, " + "'' AS cliente, " +
								  "       vista.IG_DISPOSICION AS numcuota, " + "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes, " +
								  "		  '' as estatus " + "       FROM comvis_vencimiento_fide vista     " + "WHERE NOT EXISTS  " + "    ( " +
								  "     SELECT tmp.ig_prestamo  " + "              FROM com_pagos_fide tmp                   " +
								  "             WHERE tmp.COM_FECHAPROBABLEPAGO = vista.COM_FECHAPROBABLEPAGO " +
								  "               AND tmp.ig_prestamo = vista.ig_prestamo " +
								  "               AND tmp.ig_cliente  = vista.ig_cliente " +
								  "               AND tmp.ig_disposicion = vista.ig_disposicion " +
								  "               AND tmp.fg_totalvencimiento = vista.fg_totalvencimiento " +
								  "               AND tmp.CG_ESTATUS in (?,?) " + "					 AND tmp.ic_programa_fondojr = ? " + //FODEA 026-2010 FVR
								  "     ) ");
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append(" AND vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");

				strSQL.append(" AND vista.ic_programa_fondojr = ? " + qryRownum); //FODEA 026-2010 FVR

				varBind.add(estatus);
				varBind.add(estatus2);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				titulo.add("Cr&eacute;ditos No enviados del d&iacute;a");
				//verdetalle_temp.add(titulo);

			} else if ("Vencimientos".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (vista.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								  "       TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy') AS fechaoperacion, " + "       '' AS fechapago, " +
								  "       vista.ig_prestamo AS prestamo,  " + "       vista.ig_cliente AS numsirac,  " +
								  "       vista.cg_nombrecliente AS cliente,  " + "       vista.ig_disposicion AS numcuota,  " +
								  "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes,  " + "		  '' as estatus " +
								  "  FROM comvis_vencimiento_fide vista " + " WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
								  " AND vista.ic_programa_fondojr = ? " + qryRownum); //FODEA 026-2010 FVR);
				varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				titulo.add("Vencimientos del d&iacute;a");
				//verdetalle_temp.add(titulo);
			} else {
				strSQL.append("SELECT DISTINCT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								  //FODEA 057 - 2009 ACF
					"       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,  " +
								 "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,  " +
								 "       tmp.ig_prestamo AS prestamo,  " + "       tmp.ig_cliente AS numsirac,  " +
								 "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       tmp.ig_disposicion AS numcuota,  " +
								 "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error,  " +
								 "		  tmp.cg_estatus as estatus " + "  FROM comvis_vencimiento_fide vista,   " + "       com_pagos_fide tmp  " +
								 " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
								 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
								 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
								 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "	 AND tmp.ic_programa_fondojr = ? " +
								 qryRownum);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
			}

			if ("Enviados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in ('P','NP') " +
								  " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
				varBind.add(fec_venc);
				titulo.add("Total de Registros Enviados");
				//verdetalle_temp.add(titulo);
			}
			if ("RembolsosPrepagosTotales".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in (?) "); //FODEA 026-2010 FVR SE QUITA 'R'
				if ("R".equals(estatus)) {
					titulo.add("Total Reembolsos");
					varBind.add(estatus);
					if (!fec_venc.equals("") && fec_venc.length() > 2) {
						strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
						varBind.add(fec_venc);
					}
					strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("RC".equals(estatus)) {
					titulo.add("Total Recuperaciones");
					varBind.add(estatus2);
					strSQL.append(" AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("T".equals(estatus)) {
					titulo.add("Total Prepragos");
					varBind.add(estatus);
				}

				//verdetalle_temp.add(titulo);
			}

			if ("Prepagos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? ");
				varBind.add(estatus);
				titulo.add("Prepagos Totales");
				//verdetalle_temp.add(titulo);
			}
			if ("Pagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);
				titulo.add("Cr&eacute;ditos Pagados del D&iacute;a");
				//verdetalle_temp.add(titulo);

			}
			if ("NoPagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);
				titulo.add("Cr&eacute;ditos No Pagados del D&iacute;a");
				//verdetalle_temp.add(titulo);

			}
			if ("Prepagados".equals(ver_enlace)) { //Modficacion F006-2014 req 1
				strSQL.append(" AND tmp.cg_estatus in (?, ?)  ");
				varBind.add(estatus);
				varBind.add(estatus2);

				if (!fec_venc.equals("") && fec_venc.length() > 2) {
					strSQL.append(" AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
					varBind.add(fec_venc);
				}

				strSQL.append(" AND tmp.cg_estatus_proc = ? ");
				varBind.add(proc);
				titulo.add("Registros Rechazados Cr&eacute;ditos Prepagados");
				//verdetalle_temp.add(titulo);

			}
			if ("NoPagadosRech".equals(ver_enlace)) { //F027-2011 FVR
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);
				titulo.add("Cr&eacute;ditos No Pagados Rechazados del D&iacute;a");
				//verdetalle_temp.add(titulo);

			}
			if ("Reembolsos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.cg_estatus_proc = ? " +
								  " AND 'RC' != fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, tmp.ig_prestamo,   " +
								  "                      tmp.ig_disposicion, tmp.fg_totalvencimiento)  ");
				varBind.add(estatus);
				varBind.add(proc);
				if (!fec_venc.equals("") && fec_venc.length() > 2) {
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
					varBind.add(fec_venc);
				}
				titulo.add("Reembolsos Totales");
				//verdetalle_temp.add(titulo);
			}
			if ("ConError".equals(ver_enlace)) {
				if (!fec_venc.equals("") && fec_venc.length() > 2 && !"R".equals(estatus)) {
					strSQL.append(" AND tmp.cg_estatus in (?,?) " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
									  " AND tmp.cg_error is not null ");
					//varBind.add(proc);
					strSQL.append(" UNION ALL SELECT TO_CHAR(vista.com_fechaprobablepago,'dd/mm/yyyy') as fechaautorizacion, " +
									  "       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion, " + "			''  AS fechapago, " +
									  "		  vista.ig_prestamo as prestamo, " + "       vista.ig_cliente as numsirac, " +
									  "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       vista.ig_disposicion AS numcuota,  " +
									  "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes , " +
									  "       'Registro no Enviado por el FIDE' AS error,  " + "		  '' as estatus " +
									  "  FROM comvis_vencimiento_fide vista " + "	WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
									  "	AND vista.ic_programa_fondojr = ? " + "	AND NOT EXISTS ( " + "          SELECT 1 " +
									  "            FROM com_pagos_fide c " +
									  "           WHERE c.com_fechaprobablepago = vista.com_fechaprobablepago " +
									  "             AND c.ig_prestamo = vista.ig_prestamo " + "             AND c.ig_cliente = vista.ig_cliente " +
									  "             AND c.ig_disposicion = vista.ig_disposicion " +
									  "             AND c.fg_totalvencimiento = vista.fg_totalvencimiento " + "				  AND c.cg_estatus in (?,?) " +
									  "				  AND c.ic_programa_fondojr = ?) " + qryRownum);
					varBind.add("P");
					varBind.add("NP");
					varBind.add(fec_venc);
					varBind.add(fec_venc);
					varBind.add(new Long(cvePrograma));
					varBind.add("P");
					varBind.add("NP");
					varBind.add(new Long(cvePrograma));
				} else {
					if ("R".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus);
						if (!fec_venc.equals("") && fec_venc.length() > 2) {
							strSQL.append(" AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
							varBind.add(fec_venc);
						}
						strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else if ("RC".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus2);

						strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add("T");
					}
				}


				titulo.add("Cr&eacute;ditos con ERROR");
				//verdetalle_temp.add(titulo);
			}

			System.out.println("strSQL.toString()>>>>>>" + strSQL.toString());
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				HashMap hmDetalle = new HashMap();
				hmDetalle.put("FECVENC", rst.getString(1) == null ? "" : rst.getString(1));
				hmDetalle.put("FECOPER", rst.getString(2) == null ? "" : rst.getString(2));
				hmDetalle.put("FECPAGO", rst.getString(3) == null ? "" : rst.getString(3));
				hmDetalle.put("PRESTAMO", rst.getString(4) == null ? "" : rst.getString(4));
				hmDetalle.put("NUMSIRAC", rst.getString(5) == null ? "" : rst.getString(5));
				hmDetalle.put("CLIENTE", rst.getString(6) == null ? "" : rst.getString(6));
				hmDetalle.put("NUMCUOTA", rst.getString(7) == null ? "" : rst.getString(7));
				hmDetalle.put("MONTOVENC", rst.getString(8) == null ? "" : rst.getString(8));
				if ("ConError".equals(ver_enlace))
					hmDetalle.put("ERROR", rst.getString(9) == null ? "" : rst.getString(9));
				else
					hmDetalle.put("ERROR", "");
				hmDetalle.put("ESTATUS", rst.getString("estatus") == null ? "" : rst.getString("estatus"));
				verdetalle_temp.add(hmDetalle);
			}

			rst.close();
			pst.close(); //se agrega por correccion a errore del mes del 09/2010


			if (verdetalle_temp != null && verdetalle_temp.size() > 0) {
						//System.out.println("��� verdetalle_temp �� "+verdetalle_temp+" ���");
						System.out.println("��� Registros encontrados ���");
			} else {
				//System.out.println("��� verdetalle �� "+verdetalle+" ���");
				System.out.println("��� No existen registros ���");
				verdetalle_temp.clear();
			}

		} catch (Exception e) {
			System.out.println("��� FondoJuniorBean �� ObtenerDetalleDeCreditos -ERROR- ���");
			e.printStackTrace();
		} finally {
			con.terminaTransaccion(true); //se agrega por correccion a errore del mes del 09/2010
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("��� strSQL ��� " + strSQL);
			System.out.println("��� varBind �� " + varBind + " ���");
			System.out.println("��� FondoJuniorBean �� ObtenerDetalleDeCreditos(F) ���");
		}
		return verdetalle_temp;
	}


	/**
	 * M�todo que realiza las consultas para obtener los Detalles de Credito a Validar
	 * @param ver_enlace tipo de detalle a mostrar
	 * @param estatus, proc, condiciones de consultas
	 * @return verdetalleTemp Lista con la informaci�n de la consulta.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since Migracion EXTJSFondo Junior
	 * @author Fabian
	 */
	private HashMap detalleDeCreditosTxtJS(AccesoDB con, String ver_enlace, String estatus, String estatus2, String proc,
														String fec_venc, String cvePrograma) throws NafinException {
		  //AccesoDB con = new AccesoDB();
		  PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		String qryRownum = "";

		List varBind = new ArrayList();
		List titulo = new ArrayList();
		HashMap hmCursores = new HashMap();

		System.out.println("��� FondoJuniorBean �� detalleDeCreditosTxtJS(I) ���");
		System.out.println("��� ver_enlace �� " + ver_enlace + " ���");
		System.out.println("��� estatus �� " + estatus + " ���");
		try {
			//con.conexionDB();

			if ("Recuperaciones".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,   " +
								  "       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,   " +
								  "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,   " +
								  "       tmp.ig_prestamo AS prestamo,   " + "       tmp.ig_cliente AS numsirac,   " +
								  "       NVL(vista.cg_nombrecliente,'') AS cliente,   " + "       tmp.ig_disposicion AS numcuota,   " +
								  "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error,   " +
								  "		  tmp.cg_estatus as estatus " + "  FROM comvis_vencimiento_fide vista,    " +
								  "       com_pagos_fide tmp    " + " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago  " +
								  "	AND vista.ig_cliente(+)  = tmp.ig_cliente  " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion  " +
								  "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento  " +
								  "   AND vista.ig_prestamo(+) = tmp.ig_prestamo  " + " AND tmp.CG_ESTATUS = ?   " +
								  " AND 'RC' = FN_ES_RECUPERACION_FONDOJR (tmp.COM_FECHAPROBABLEPAGO, tmp.IG_PRESTAMO,    " +
								  "                      tmp.IG_DISPOSICION, tmp.FG_TOTALVENCIMIENTO)    " +
								  " AND tmp.ic_programa_fondojr = ? " + qryRownum);
				varBind.add(estatus);
				varBind.add(new Long(cvePrograma));
				titulo.add("Recuperaciones Recibidas");
				//verdetalle_temp.add(titulo);
			} else if ("NoEncontrados".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (tmp.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
								  "'' AS fechaoperacion," + "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,   " +
								  "       tmp.IG_PRESTAMO AS prestamo, " + "       tmp.IG_CLIENTE AS numsirac, " + "'' AS cliente, " +
								  "       tmp.IG_DISPOSICION AS numcuota, " + "       tmp.fg_totalvencimiento AS capital_mas_interes, " +
								  "		  tmp.cg_estatus as estatus " + "       FROM com_pagos_fide tmp      " +
								  "WHERE  tmp.cg_estatus in (?,?)  ");
				if ("RC".equals(estatus2)) {
					strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				}
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
				strSQL.append("  AND NOT EXISTS  " + "    ( " + "     SELECT vp.ig_prestamo  " +
								  "              FROM comvis_vencimiento_fide vp                   " +
								  "             WHERE vp.com_fechaprobablepago = tmp.com_fechaprobablepago " +
								  "               AND vp.ig_prestamo = tmp.ig_prestamo " +
								  "               AND vp.ig_cliente  = tmp.ig_cliente " +
								  "               AND vp.ig_disposicion = tmp.ig_disposicion " +
								  "               AND vp.fg_totalvencimiento = tmp.fg_totalvencimiento " + "     ) " +
								  "	  AND tmp.ic_programa_fondojr = ? " + qryRownum);
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(estatus2);
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma));
				titulo.add("Registros No encontrados");
				//verdetalle_temp.add(titulo);

			} else if ("NoEnviados".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (vista.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
								  "       TO_CHAR (vista.DF_FECHAOPERA, 'dd/mm/yyyy') AS fechaoperacion,   " + "'' AS fechapago, " +
								  "       vista.IG_PRESTAMO AS prestamo, " + "       vista.IG_CLIENTE AS numsirac, " + "'' AS cliente, " +
								  "       vista.IG_DISPOSICION AS numcuota, " + "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes, " +
								  "		  '' as estatus " + "       FROM comvis_vencimiento_fide vista     " + "WHERE NOT EXISTS  " + "    ( " +
								  "     SELECT tmp.ig_prestamo  " + "              FROM com_pagos_fide tmp                   " +
								  "             WHERE tmp.COM_FECHAPROBABLEPAGO = vista.COM_FECHAPROBABLEPAGO " +
								  "               AND tmp.ig_prestamo = vista.ig_prestamo " +
								  "               AND tmp.ig_cliente  = vista.ig_cliente " +
								  "               AND tmp.ig_disposicion = vista.ig_disposicion " +
								  "               AND tmp.fg_totalvencimiento = vista.fg_totalvencimiento " +
								  "               AND tmp.CG_ESTATUS in (?,?) " + "					 AND tmp.ic_programa_fondojr = ? " + //FODEA 026-2010 FVR
								  "     ) ");
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append(" AND vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");

				strSQL.append(" AND vista.ic_programa_fondojr = ? " + qryRownum); //FODEA 026-2010 FVR

				varBind.add(estatus);
				varBind.add(estatus2);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				titulo.add("Cr&eacute;ditos No enviados del d&iacute;a");
				//verdetalle_temp.add(titulo);

			} else if ("Vencimientos".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (vista.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								  "       TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy') AS fechaoperacion, " + "       '' AS fechapago, " +
								  "       vista.ig_prestamo AS prestamo,  " + "       vista.ig_cliente AS numsirac,  " +
								  "       vista.cg_nombrecliente AS cliente,  " + "       vista.ig_disposicion AS numcuota,  " +
								  "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes,  " + "		  '' as estatus " +
								  "  FROM comvis_vencimiento_fide vista " + " WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
								  " AND vista.ic_programa_fondojr = ? " + qryRownum); //FODEA 026-2010 FVR);
				varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				titulo.add("Vencimientos del d&iacute;a");
				//verdetalle_temp.add(titulo);
			} else {
				strSQL.append("SELECT DISTINCT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								  //FODEA 057 - 2009 ACF
					"       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,  " +
								 "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,  " +
								 "       tmp.ig_prestamo AS prestamo,  " + "       tmp.ig_cliente AS numsirac,  " +
								 "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       tmp.ig_disposicion AS numcuota,  " +
								 "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error,  " +
								 "		  tmp.cg_estatus as estatus " + "  FROM comvis_vencimiento_fide vista,   " + "       com_pagos_fide tmp  " +
								 " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
								 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
								 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
								 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "	 AND tmp.ic_programa_fondojr = ? " +
								 qryRownum);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
			}

			if ("Enviados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in ('P','NP') " +
								  " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
				varBind.add(fec_venc);
				titulo.add("Total de Registros Enviados");
				//verdetalle_temp.add(titulo);
			}
			if ("RembolsosPrepagosTotales".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in (?) "); //FODEA 026-2010 FVR SE QUITA 'R'
				if ("R".equals(estatus)) {
					titulo.add("Total Reembolsos");
					varBind.add(estatus);
					if (!fec_venc.equals("") && fec_venc.length() > 2) {
						strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
						varBind.add(fec_venc);
					}
					strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("RC".equals(estatus)) {
					titulo.add("Total Recuperaciones");
					varBind.add(estatus2);
					strSQL.append(" AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("T".equals(estatus)) {
					titulo.add("Total Prepragos");
					varBind.add(estatus);
				}

				//verdetalle_temp.add(titulo);
			}

			if ("Prepagos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? ");
				varBind.add(estatus);
				titulo.add("Prepagos Totales");
				//verdetalle_temp.add(titulo);
			}
			if ("Pagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				titulo.add("Cr&eacute;ditos Pagados del D&iacute;a");
				//verdetalle_temp.add(titulo);

			}
			if ("NoPagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);
				titulo.add("Cr&eacute;ditos No Pagados del D&iacute;a");
				//verdetalle_temp.add(titulo);

			}
			if ("Prepagados".equals(ver_enlace)) { //Modificacion F006-2014 Req 1

				strSQL.append(" AND tmp.cg_estatus in (?, ?)  ");
				varBind.add(estatus);
				varBind.add(estatus2);

				if (!fec_venc.equals("") && fec_venc.length() > 2) {
					strSQL.append(" AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
					varBind.add(fec_venc);
				}

				strSQL.append(" AND tmp.cg_estatus_proc = ? ");
				varBind.add(proc);
				titulo.add("Registros Rechazados Cr&eacute;ditos Prepagados");
				//verdetalle_temp.add(titulo);

			}
			if ("NoPagadosRech".equals(ver_enlace)) { //F027-2011 FVR
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);
				titulo.add("Cr&eacute;ditos No Pagados Rechazados del D&iacute;a");
				//verdetalle_temp.add(titulo);

			}
			if ("Reembolsos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " +
								  " AND 'RC' != fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, tmp.ig_prestamo,   " +
								  "                      tmp.ig_disposicion, tmp.fg_totalvencimiento)  ");
				varBind.add(estatus);
				if (!fec_venc.equals("") && fec_venc.length() > 2) {
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
					varBind.add(fec_venc);
				}
				titulo.add("Reembolsos Totales");
				//verdetalle_temp.add(titulo);
			}
			if ("ConError".equals(ver_enlace)) {
				if (!fec_venc.equals("") && fec_venc.length() > 2 && !"R".equals(estatus)) {
					strSQL.append(" AND tmp.cg_estatus in (?,?) " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
									  " AND tmp.cg_error is not null ");
					//varBind.add(proc);
					strSQL.append(" UNION ALL SELECT TO_CHAR(vista.com_fechaprobablepago,'dd/mm/yyyy') as fechaautorizacion, " +
									  "       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion, " + "			''  AS fechapago, " +
									  "		  vista.ig_prestamo as prestamo, " + "       vista.ig_cliente as numsirac, " +
									  "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       vista.ig_disposicion AS numcuota,  " +
									  "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes , " +
									  "       'Registro no Enviado por el FIDE' AS error,  " + "		  '' as estatus " +
									  "  FROM comvis_vencimiento_fide vista " + "	WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
									  "	AND vista.ic_programa_fondojr = ? " + "	AND NOT EXISTS ( " + "          SELECT 1 " +
									  "            FROM com_pagos_fide c " +
									  "           WHERE c.com_fechaprobablepago = vista.com_fechaprobablepago " +
									  "             AND c.ig_prestamo = vista.ig_prestamo " + "             AND c.ig_cliente = vista.ig_cliente " +
									  "             AND c.ig_disposicion = vista.ig_disposicion " +
									  "             AND c.fg_totalvencimiento = vista.fg_totalvencimiento " + "				  AND c.cg_estatus in (?,?) " +
									  "				  AND c.ic_programa_fondojr = ?) " + qryRownum);
					varBind.add("P");
					varBind.add("NP");
					varBind.add(fec_venc);
					varBind.add(fec_venc);
					varBind.add(new Long(cvePrograma));
					varBind.add("P");
					varBind.add("NP");
					varBind.add(new Long(cvePrograma));
				} else {
					if ("R".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus);
						if (!fec_venc.equals("") && fec_venc.length() > 2) {
							strSQL.append(" AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
							varBind.add(fec_venc);
						}
						strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else if ("RC".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus2);

						strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add("T");
					}
				}


				titulo.add("Cr&eacute;ditos con ERROR");
				//verdetalle_temp.add(titulo);
			}

			System.out.println("strSQL.toString()>>>>>>" + strSQL.toString());
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			hmCursores.put("RESULTSET", rst);
			hmCursores.put("PREPAREDSTATEMENT", pst);

			/*
	   while(rst.next()){
		  List verdetalle = new ArrayList();
		  HashMap hmDetalle = new HashMap();
        hmDetalle.put("FECVENC",rst.getString(1)==null?"":rst.getString(1));
        hmDetalle.put("FECOPER",rst.getString(2)==null?"":rst.getString(2));
        hmDetalle.put("FECPAGO",rst.getString(3)==null?"":rst.getString(3));
        hmDetalle.put("PRESTAMO",rst.getString(4)==null?"":rst.getString(4));
        hmDetalle.put("NUMSIRAC",rst.getString(5)==null?"":rst.getString(5));
        hmDetalle.put("CLIENTE",rst.getString(6)==null?"":rst.getString(6));
        hmDetalle.put("NUMCUOTA",rst.getString(7)==null?"":rst.getString(7));
		  hmDetalle.put("MONTOVENC",rst.getString(8)==null?"":rst.getString(8));
		  if("ConError".equals(ver_enlace))
				hmDetalle.put("ERROR",rst.getString(9)==null?"":rst.getString(9));
			else
				hmDetalle.put("ERROR","");
		  hmDetalle.put("ESTATUS",rst.getString("estatus")==null?"":rst.getString("estatus"));
        verdetalle_temp.add(hmDetalle);
		}

      rst.close();
		pst.close();//se agrega por correccion a errore del mes del 09/2010


		if(verdetalle_temp!=null && verdetalle_temp.size()>0){
				//System.out.println("��� verdetalle_temp �� "+verdetalle_temp+" ���");
				System.out.println("��� Registros encontrados ���");
		}else{
			//System.out.println("��� verdetalle �� "+verdetalle+" ���");
			System.out.println("��� No existen registros ���");
			verdetalle_temp.clear();
		}
		*/
			return hmCursores;
		} catch (Exception e) {
			System.out.println("��� FondoJuniorBean �� detalleDeCreditosTxtJS -ERROR- ���");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {

			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //se agrega por correccion a errore del mes del 09/2010
				//con.cierraConexionDB();
			}
			System.out.println("��� strSQL ��� " + strSQL);
			System.out.println("��� varBind �� " + varBind + " ���");
			System.out.println("��� FondoJuniorBean �� detalleDeCreditosTxtJS(F) ���");
		}

	}


	/**
	 * Realiza eliminacion de registros no enviados y no encontrados en la validacion del monitor
	 * @throws com.netro.exception.AppException
	 * @param regEliminar
	 * @param cvePrograma
	 */
	public void eliminaRegMonitorValid(List lstRegEliminar, String csTipoInfo, String causas,
												  String usuario) throws AppException {
		log.info("FondoJuniorBean::eliminaRegMonitorValid(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String qrySentence = "";
		String qryDelete = "";
		boolean commit = true;


		try {
			con.conexionDB();

			/*---------------- SE OBTIENE REGISTROS DE BITACORA-------------------*/

			if ("FI".equals(csTipoInfo)) {
				qrySentence =
							  "INSERT INTO bit_monitor_fjr_eliminados " +
							  "            (ic_bit_monitor, com_fechaprobablepago, ig_prestamo, ig_cliente, " +
							  "             ig_disposicion, fg_amortizacion, fg_interes, fg_totalvencimiento, " +
							  "             df_pago_cliente, cg_estatus, ig_origen, df_periodofin, " +
							  "             ig_numero_docto, df_registro_fide, ig_folio, cg_estatus_proc, " +
							  "             cg_error, ic_programa_fondojr, cg_causas, cs_tipo_info, " + "             cg_usuario) " +
							  "   SELECT seq_bit_monitor_fjr_eliminados.NEXTVAL, com_fechaprobablepago, " +
							  "          ig_prestamo, ig_cliente, ig_disposicion, fg_amortizacion, " +
							  "          fg_interes, fg_totalvencimiento, df_pago_cliente, cg_estatus, " +
							  "          ig_origen, df_periodofin, ig_numero_docto, df_registro_fide, " +
							  "          ig_folio, cg_estatus_proc, cg_error, ic_programa_fondojr, " + "          ? , '" + csTipoInfo +
							  "', ? " + "     FROM com_pagos_fide " + "    WHERE com_fechaprobablepago = TO_DATE ( ? , 'DD/MM/YYYY') " +
							  "      AND ig_prestamo = ? " + "      AND ig_disposicion = ? " + "      AND fg_totalvencimiento = ? " +
							  "      AND cg_estatus = ? ";

				qryDelete =
							  "DELETE com_pagos_fide " + "    WHERE com_fechaprobablepago = TO_DATE ( ? , 'DD/MM/YYYY') " +
							  "      AND ig_prestamo = ? " + "      AND ig_disposicion = ? " + "      AND fg_totalvencimiento = ? " +
							  "      AND cg_estatus = ? ";


			} else if ("NA".equals(csTipoInfo)) {

				qrySentence =
							  "INSERT INTO bit_monitor_fjr_eliminados " + "            (ic_bit_monitor, com_fechaprobablepago,  " +
							  "             cg_modalidadpago, ic_moneda, cg_moneda,  " +
							  "             ic_financiera, cg_razon_social, ig_baseoperacion, cg_descbaseoper, " +
							  "             ig_cliente, cg_nombrecliente, ig_sucursal, ig_proyecto, ig_contrato, " +
							  "             ig_prestamo, ig_numelectronico, cg_bursatilizado, ig_disposicion,  " +
							  "             ig_subaplicacion, ig_cuotasemit, ig_cuotasporvenc, ig_totalcuotas, " +
							  "             cg_aniomodalidad, ig_tipoestrato, df_fechaopera, ig_sancionado, " +
							  "             ig_frecuenciacapital, ig_frecuenciainteres, ig_tasabase, " +
							  "             cg_esquematasas, cg_relmat_1, fg_spread_1, cg_relmat_2, fg_spread_2,  " +
							  "             cg_relmat_3, fg_spread_3, cg_relmat_4, fg_margen, ig_tipogarantia,  " +
							  "             fg_porcdescfop, fg_totdescfop, fg_porcdescfinape, fg_totdescfinape,  " +
							  "             fg_fcrecimiento, df_periodoinic, df_periodofin, ig_dias, fg_comision,  " +
							  "             fg_porcgarantia, fg_porccomision, fg_sdoinsoluto, fg_amortizacion, " +
							  "             fg_interes, fg_intcobradoxanticip, fg_totalvencimiento, fg_pagotrad,  " +
							  "             fg_subsidio, fg_totalexigible, fg_capitalvencido, fg_interesvencido,  " +
							  "             fg_totalcarven, fg_adeudototal, fg_finadicotorgado, fg_finadicrecup,  " +
							  "             fg_sdoinsnvo, fg_sdoinsbase, ig_numero_docto, ic_programa_fondojr, " +
							  "             cg_causas, cs_tipo_info, cg_usuario, cg_error " + "            )     " +
							  "SELECT seq_bit_monitor_fjr_eliminados.NEXTVAL, com_fechaprobablepago,  " +
							  "       cg_modalidadpago, ic_moneda, cg_moneda, " +
							  "       ic_financiera, cg_razon_social, ig_baseoperacion, cg_descbaseoper, " +
							  "       ig_cliente, cg_nombrecliente, ig_sucursal, ig_proyecto, ig_contrato, " +
							  "       ig_prestamo, ig_numelectronico, cg_bursatilizado, ig_disposicion, " +
							  "       ig_subaplicacion, ig_cuotasemit, ig_cuotasporvenc, ig_totalcuotas, " +
							  "       cg_aniomodalidad, ig_tipoestrato, df_fechaopera, ig_sancionado, " +
							  "       ig_frecuenciacapital, ig_frecuenciainteres, ig_tasabase, " +
							  "       cg_esquematasas, cg_relmat_1, fg_spread_1, cg_relmat_2, fg_spread_2, " +
							  "       cg_relmat_3, fg_spread_3, cg_relmat_4, fg_margen, ig_tipogarantia, " +
							  "       fg_porcdescfop, fg_totdescfop, fg_porcdescfinape, fg_totdescfinape, " +
							  "       fg_fcrecimiento, df_periodoinic, df_periodofin, ig_dias, fg_comision, " +
							  "       fg_porcgarantia, fg_porccomision, fg_sdoinsoluto, fg_amortizacion, " +
							  "       fg_interes, fg_intcobradoxanticip, fg_totalvencimiento, fg_pagotrad, " +
							  "       fg_subsidio, fg_totalexigible, fg_capitalvencido, fg_interesvencido, " +
							  "       fg_totalcarven, fg_adeudototal, fg_finadicotorgado, fg_finadicrecup, " +
							  "       fg_sdoinsnvo, fg_sdoinsbase, ig_numero_docto, ic_programa_fondojr, " + "       ?, '" + csTipoInfo +
							  "', ?, ? " + "  FROM comvis_vencimiento_fide " + "  WHERE com_fechaprobablepago = TO_DATE (?, 'DD/MM/YYYY') " +
							  "      AND ig_prestamo = ? " + "      AND ig_disposicion = ? " +
							  "      AND fg_totalvencimiento = ? ";

				qryDelete =
							  "DELETE comvis_vencimiento_fide " + "    WHERE com_fechaprobablepago = TO_DATE ( ? , 'DD/MM/YYYY') " +
							  "      AND ig_prestamo = ? " + "      AND ig_disposicion = ? " +
							  "      AND fg_totalvencimiento = ? ";


			}

			log.info("qrySentence>>>>>>" + qrySentence);
			if (lstRegEliminar != null && lstRegEliminar.size() > 0) {
				for (int x = 0; x < lstRegEliminar.size(); x++) {
					int p = 0;
					HashMap mp = new HashMap();
					mp = (HashMap) lstRegEliminar.get(x);

					ps = con.queryPrecompilado(qrySentence);
					ps.setString(++p, causas);
					ps.setString(++p, usuario);
					if ("NA".equals(csTipoInfo)) {
						ps.setString(++p, "Registro no Enviado por el FIDE");
					}
					ps.setString(++p, (String) mp.get("FECVENC"));
					ps.setLong(++p, Long.parseLong((String) mp.get("PRESTAMO")));
					ps.setLong(++p, Long.parseLong((String) mp.get("NUMCUOTA")));
					ps.setDouble(++p, Double.parseDouble((String) mp.get("MONTOVENC")));
					if ("FI".equals(csTipoInfo)) {
						ps.setString(++p, (String) mp.get("ESTATUS"));
					}

					ps.executeUpdate();
					ps.close();

					ps = con.queryPrecompilado(qryDelete);
					ps.setString(1, (String) mp.get("FECVENC"));
					ps.setLong(2, Long.parseLong((String) mp.get("PRESTAMO")));
					ps.setLong(3, Long.parseLong((String) mp.get("NUMCUOTA")));
					ps.setDouble(4, Double.parseDouble((String) mp.get("MONTOVENC")));
					if ("FI".equals(csTipoInfo)) {
						ps.setString(5, (String) mp.get("ESTATUS"));
					}

					ps.executeUpdate();
					ps.close();

				}
			}

			log.info("FondoJuniorBean::eliminaRegMonitorValid(S)");
		} catch (Exception e) {
			commit = false;
			log.info("FondoJuniorBean::eliminaRegMonitorValid(Error)");
			e.printStackTrace();
			throw new AppException("Error al aliminar registros no encontrados o no enviados del monitor", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}


	public void restableceRegMonitorValid(List lstRegEliminar) throws AppException {
		log.info("FondoJuniorBean::restableceRegMonitorValid(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String qrySentence = "";
		String qryDelete = "";
		String csTipoReg = "";
		boolean commit = true;

		try {
			con.conexionDB();

			qryDelete = "delete bit_monitor_fjr_eliminados " + " where ic_bit_monitor = ? ";

			if (lstRegEliminar != null && lstRegEliminar.size() > 0) {
				for (int x = 0; x < lstRegEliminar.size(); x++) {
					HashMap mp = new HashMap();
					mp = (HashMap) lstRegEliminar.get(x);
					csTipoReg = (String) mp.get("TIPOREG");
					if ("FI".equals(csTipoReg)) {
						qrySentence = "INSERT INTO com_pagos_fide " +
											"            (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion, " +
											"             fg_amortizacion, fg_interes, fg_totalvencimiento, " +
											"             df_pago_cliente, cg_estatus, ig_origen, df_periodofin, " +
											"             ig_numero_docto, df_registro_fide, ig_folio, cg_estatus_proc, " +
											"             cg_error, ic_programa_fondojr) " +
											"   SELECT com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion, " +
											"          fg_amortizacion, fg_interes, fg_totalvencimiento, df_pago_cliente, " +
											"          cg_estatus, ig_origen, df_periodofin, ig_numero_docto, " +
											"          df_registro_fide, ig_folio, cg_estatus_proc, cg_error, " + "          ic_programa_fondojr " +
											"     FROM bit_monitor_fjr_eliminados " + "    WHERE ic_bit_monitor = ? ";

					} else if ("NA".equals(csTipoReg)) {

						qrySentence = "insert into comvis_vencimiento_fide " + "( " + "	com_fechaprobablepago, " +
											"	cg_modalidadpago, ic_moneda, cg_moneda, " +
											"	ic_financiera, cg_razon_social, ig_baseoperacion, cg_descbaseoper, " +
											"	ig_cliente, cg_nombrecliente, ig_sucursal, ig_proyecto, ig_contrato, " +
											"	ig_prestamo, ig_numelectronico, cg_bursatilizado, ig_disposicion, " +
											"	ig_subaplicacion, ig_cuotasemit, ig_cuotasporvenc, ig_totalcuotas, " +
											"	cg_aniomodalidad, ig_tipoestrato, df_fechaopera, ig_sancionado, " +
											"	ig_frecuenciacapital, ig_frecuenciainteres, ig_tasabase, " +
											"	cg_esquematasas, cg_relmat_1, fg_spread_1, cg_relmat_2, fg_spread_2, " +
											"	cg_relmat_3, fg_spread_3, cg_relmat_4, fg_margen, ig_tipogarantia, " +
											"	fg_porcdescfop, fg_totdescfop, fg_porcdescfinape, fg_totdescfinape, " +
											"	fg_fcrecimiento, df_periodoinic, df_periodofin, ig_dias, fg_comision, " +
											"	fg_porcgarantia, fg_porccomision, fg_sdoinsoluto, fg_amortizacion, " +
											"	fg_interes, fg_intcobradoxanticip, fg_totalvencimiento, fg_pagotrad, " +
											"	fg_subsidio, fg_totalexigible, fg_capitalvencido, fg_interesvencido, " +
											"	fg_totalcarven, fg_adeudototal, fg_finadicotorgado, fg_finadicrecup, " +
											"	fg_sdoinsnvo, fg_sdoinsbase, ig_numero_docto, ic_programa_fondojr " + ") " +
											"SELECT com_fechaprobablepago, " + "      cg_modalidadpago, ic_moneda, cg_moneda, " +
											"      ic_financiera, cg_razon_social, ig_baseoperacion, cg_descbaseoper, " +
											"      ig_cliente, cg_nombrecliente, ig_sucursal, ig_proyecto, ig_contrato, " +
											"      ig_prestamo, ig_numelectronico, cg_bursatilizado, ig_disposicion, " +
											"      ig_subaplicacion, ig_cuotasemit, ig_cuotasporvenc, ig_totalcuotas, " +
											"      cg_aniomodalidad, ig_tipoestrato, df_fechaopera, ig_sancionado, " +
											"      ig_frecuenciacapital, ig_frecuenciainteres, ig_tasabase, " +
											"      cg_esquematasas, cg_relmat_1, fg_spread_1, cg_relmat_2, fg_spread_2, " +
											"      cg_relmat_3, fg_spread_3, cg_relmat_4, fg_margen, ig_tipogarantia, " +
											"      fg_porcdescfop, fg_totdescfop, fg_porcdescfinape, fg_totdescfinape, " +
											"      fg_fcrecimiento, df_periodoinic, df_periodofin, ig_dias, fg_comision, " +
											"      fg_porcgarantia, fg_porccomision, fg_sdoinsoluto, fg_amortizacion, " +
											"      fg_interes, fg_intcobradoxanticip, fg_totalvencimiento, fg_pagotrad, " +
											"      fg_subsidio, fg_totalexigible, fg_capitalvencido, fg_interesvencido, " +
											"      fg_totalcarven, fg_adeudototal, fg_finadicotorgado, fg_finadicrecup, " +
											"      fg_sdoinsnvo, fg_sdoinsbase, ig_numero_docto, ic_programa_fondojr " +
											" from bit_monitor_fjr_eliminados " + "where ic_bit_monitor = ? ";


					}


					ps = con.queryPrecompilado(qrySentence);
					ps.setLong(1, Long.parseLong((String) mp.get("CVEBIT")));
					ps.executeUpdate();
					ps.close();


					ps = con.queryPrecompilado(qryDelete);
					ps.setLong(1, Long.parseLong((String) mp.get("CVEBIT")));
					ps.executeUpdate();
					ps.close();
				}
			}

		} catch (Throwable t) {
			commit = false;
			throw new AppException("Error al restablecer el registro eliminado", t);

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * M�todo para generar un numero consecutivo de desembolso
	 * Fodea 07-2014  FONDO JR -Desembolsos FIDE
	 * @return
	 */
	public String getIc_Desembolso() {
		log.info("getIc_Desembolso (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String ic_desembolso = "";

		try {
			con.conexionDB();

			String query = "select SEQ_COM_DESEMBOLSO_FJR.NEXTVAL  from dual";

			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				ic_desembolso = rs.getString(1);
			}
			rs.close();
			ps.close();


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("getIc_Desembolso  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  getIc_Desembolso (S) ");
			}
		}
		return ic_desembolso;
	}

	/**
	 * Metodo para validar  los registros a cargar
	 * Fodea 07-2014  FONDO JR -Desembolsos FIDE
	 * @return
	 * @param ic_desembolso
	 * @param numDesembolsos
	 * @param nombreArchivo
	 * @param strDirectorioTemp
	 */
	public List getValidarArchivo(String strDirectorioTemp, String nombreArchivo, int numberOfRegisters,
											String ic_desembolso, String fechaHonrada) {
		log.info("getValidarArchivo (E)");

		int iNumLinea = 0;
		String numeroPrestamo = "", numeroAcreditado = "", fondeoNafinCapital = "", fondeoNafinInteres =
				 "", fondeoNafinTotal = "", fondeoJRCapital = "", fondeoJRInteres = "", fondeoJRTotal = "", totalAdeudoCapital =
				 "", totalAdeudoInteres = "", totalAdeudoTotal = "", totalDesembolsoCapital = "", totalDesembolsoInteres =
				 "", totalDesembolsoTotal = "", solicitadoFIDECapital = "", solicitadoFIDEInteres = "", solicitadoFIDETotal =
				 "";

		String fondeoNafinCapital_sf = "", fondeoNafinInteres_sf = "", fondeoNafinTotal_sf = "", fondeoJRCapital_sf =
				 "", fondeoJRInteres_sf = "", fondeoJRTotal_sf = "", totalAdeudoCapital_sf = "", totalAdeudoInteres_sf =
				 "", totalAdeudoTotal_sf = "", totalDesembolsoCapital_sf = "", totalDesembolsoInteres_sf =
				 "", totalDesembolsoTotal_sf = "", solicitadoFIDECapital_sf = "", solicitadoFIDEInteres_sf =
				 "", solicitadoFIDETotal_sf = "";

		List Errorlineas = new ArrayList();
		List lErrores = new ArrayList();
		List lineasBien = new ArrayList();
		List listas = new ArrayList();
		List cifras = new ArrayList();
		List cifrasControl = new ArrayList();

		BigDecimal totalFondoNafin = new BigDecimal(0.0);
		BigDecimal totalFondoJR = new BigDecimal(0.0);
		BigDecimal totalAdeudo = new BigDecimal(0.0);
		BigDecimal totalDesembolso = new BigDecimal(0.0);
		BigDecimal totalSolicFIDE = new BigDecimal(0.0);

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();


		try {

			con.conexionDB();

			log.debug("ic_desembolso   " + ic_desembolso);

			borraTemporales(ic_desembolso);

			strSQL = new StringBuffer();
			strSQL.append(" INSERT INTO  COMTMP_DESEMBOLSO_FJR " +
							  " ( IC_DESEMBOLSO   ,  	 DF_FECHAHONRADA  ,  	 IN_NUM_PRESTAMO , 			CG_NOM_ACREDITADO ,  " +
							  " FN_NAFIN_CAPITAL , 	 FN_NAFIN_INTERES  ,  FN_NAFIN_TOTAL  ,   " +
							  " FN_JR_CAPITAL  , 		 FN_JR_INTERES  ,  	 FN_JR_TOTAL ,  " +
							  " FN_ADEUDO_CAPITAL ,    FN_ADEUDO_INTERES ,  FN_ADEUDO_TOTAL  ,   " +
							  " FN_DESEMBOLSO_CAPITAL, FN_DESEMBOLSO_INTERES  ,  FN_DESEMBOLSO_TOTAL, " +
							  " FN_SOLICFIDE_CAPITAL ,  FN_SOLICFIDE_INTERES ,  FN_SOLICFIDE_TOTAL )  " +
							  " VALUES ( ? ,  TO_DATE(?, 'dd/mm/yyyy')  ,  ? ,  ? ,  ?,  ?,  ?,  ?,  ?,  ?,  ?,  ? , ?,  ?,   ?,  ?,  ?,  ?,  ?  )   ");


			String rutaNombreArchivo = strDirectorioTemp + nombreArchivo;

			ComunesXLS xls = new ComunesXLS();
			List lDatos = (ArrayList) xls.LeerXLS(rutaNombreArchivo, false);

			int ultimo = lDatos.size() - 1;
			if (lDatos.size() > 0) {

				for (int x = 6; x < lDatos.size(); x++) {

					List lineaArchivo = (List) lDatos.get(x);

					if (x != ultimo) { // esto es para no leer la ultima linea
									
									 iNumLinea++;

						if (lineaArchivo.size() > 0) {

							Errorlineas = new ArrayList();

							if (lineaArchivo.size() > 18) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " No cuenta con la cantidad de campos requeridos");
								lErrores.add(Errorlineas);
								continue;
							} else if (lineaArchivo.size() < 18) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " No cuenta con la cantidad de campos requeridos");
								lErrores.add(Errorlineas);
								continue;
							}

							numeroPrestamo = lineaArchivo.get(1).toString();
							numeroAcreditado = lineaArchivo.get(2).toString();

							fondeoNafinCapital_sf = lineaArchivo.get(3).toString();
							fondeoNafinInteres_sf = lineaArchivo.get(4).toString();
							fondeoNafinTotal_sf = lineaArchivo.get(5).toString();
							fondeoJRCapital_sf = lineaArchivo.get(6).toString();
							fondeoJRInteres_sf = lineaArchivo.get(7).toString();
							fondeoJRTotal_sf = lineaArchivo.get(8).toString();
							totalAdeudoCapital_sf = lineaArchivo.get(9).toString();
							totalAdeudoInteres_sf = lineaArchivo.get(10).toString();
							totalAdeudoTotal_sf = lineaArchivo.get(11).toString();
							totalDesembolsoCapital_sf = lineaArchivo.get(12).toString();
							totalDesembolsoInteres_sf = lineaArchivo.get(13).toString();
							totalDesembolsoTotal_sf = lineaArchivo.get(14).toString();
							solicitadoFIDECapital_sf = lineaArchivo.get(15).toString();
							solicitadoFIDEInteres_sf = lineaArchivo.get(16).toString();
							solicitadoFIDETotal_sf = lineaArchivo.get(17).toString();


							fondeoNafinCapital = Comunes.formatoDecimal(lineaArchivo.get(3).toString(), 2, false);
							fondeoNafinInteres = Comunes.formatoDecimal(lineaArchivo.get(4).toString(), 2, false);
							fondeoNafinTotal = Comunes.formatoDecimal(lineaArchivo.get(5).toString(), 2, false);
							fondeoJRCapital = Comunes.formatoDecimal(lineaArchivo.get(6).toString(), 2, false);
							fondeoJRInteres = Comunes.formatoDecimal(lineaArchivo.get(7).toString(), 2, false);
							fondeoJRTotal = Comunes.formatoDecimal(lineaArchivo.get(8).toString(), 2, false);
							totalAdeudoCapital = Comunes.formatoDecimal(lineaArchivo.get(9).toString(), 2, false);
							totalAdeudoInteres = Comunes.formatoDecimal(lineaArchivo.get(10).toString(), 2, false);
							totalAdeudoTotal = Comunes.formatoDecimal(lineaArchivo.get(11).toString(), 2, false);
							totalDesembolsoCapital = Comunes.formatoDecimal(lineaArchivo.get(12).toString(), 2, false);
							totalDesembolsoInteres = Comunes.formatoDecimal(lineaArchivo.get(13).toString(), 2, false);
							totalDesembolsoTotal = Comunes.formatoDecimal(lineaArchivo.get(14).toString(), 2, false);
							solicitadoFIDECapital = Comunes.formatoDecimal(lineaArchivo.get(15).toString(), 2, false);
							solicitadoFIDEInteres = Comunes.formatoDecimal(lineaArchivo.get(16).toString(), 2, false);
							solicitadoFIDETotal = Comunes.formatoDecimal(lineaArchivo.get(17).toString(), 2, false);


							// *************  1 .  N�mero de Pr�stamo  ********
							if (numeroPrestamo.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea + " El n�mero de Periodo es requerido.");

							} else if (numeroPrestamo.length() > 12) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo N�mero de Pr�stamo no cumple con la longitud m�xima permitida: 12. ");

							} else if (!Comunes.esNumero(numeroPrestamo)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea + " El N�mero de Pr�stamo es incorrecto.");
							}

							// *************2 . Nombre del Acreditado  ********
							if (numeroAcreditado.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Nombre del Acreditado es obligatorio.");

							} else if (numeroAcreditado.length() > 80) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Nombre del Acreditado no cumple con la longitud m�xima permitida: 80. ");
							}

							// ************** 3. Fondeo Nafin - Capital ***************+

							if (fondeoNafinCapital.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "El valor del campo Fondeo Nafin - Capital es obligatorio. ");
							}
							if (!Comunes.precisionValida(fondeoNafinCapital, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Fondeo Nafin - Capital no cumple con la longitud requerida. ");

							}
							if (fondeoNafinCapital_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Fondeo Nafin � Capital  no deber� ser f�rmula.  ");
							}

							// ************** 4. Fondeo Nafin � Inter�s  ***************+

							if (fondeoNafinInteres.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "	El campo Fondeo Nafin � Inter�s es obligatorio. ");

							}
							if (!Comunes.precisionValida(fondeoNafinInteres, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Fondeo Nafin � Inter�s no cumple con la longitud requerida. ");

							}
							if (fondeoNafinInteres_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Fondeo Nafin � Inter�s  no deber� ser f�rmula.  ");
							}

							// ************** 5. Fondeo Nafin - Total ***************+

							if (fondeoNafinTotal.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Fondeo Nafin - Total es obligatorio. ");

							}
							if (!Comunes.precisionValida(fondeoNafinTotal, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Fondeo Nafin - Total no cumple con la longitud requerida. ");

							}
							if (fondeoNafinTotal_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Fondeo Nafin � Total  no deber� ser f�rmula.  ");
							}


							// **************  6.  Fondeo JR- Capital ***************+

							if (fondeoJRCapital.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Fondeo JR- Capital es obligatorio.  ");

							}
							if (!Comunes.precisionValida(fondeoJRCapital, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Fondeo JR- Capital no cumple con la longitud requerida. ");

							}
							if (fondeoJRCapital_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Fondeo JR � Capital  no deber� ser f�rmula.  ");
							}

							// ************** 7 Fondeo JR- Inter�s ***************

							if (fondeoJRInteres.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Fondeo JR- Inter�s es obligatorio. ");

							}
							if (!Comunes.precisionValida(fondeoJRInteres, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Fondeo JR- Inter�s no cumple con la longitud requerida. ");

							}
							if (fondeoJRInteres_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Fondeo JR � Inter�s  no deber� ser f�rmula.  ");
							}

							// ************** 8 Fondeo JR- Total **************

							if (fondeoJRTotal.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Fondeo JR- Total es obligatorio. ");

							}
							if (!Comunes.precisionValida(fondeoJRTotal, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Fondeo JR- Total no cumple con la longitud requerida. ");

							}
							if (fondeoJRTotal_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Fondeo JR � Total  no deber� ser f�rmula.  ");
							}

							// ************** 9 Total Adeudo- Capital ***************+

							if (totalAdeudoCapital.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Total Adeudo- Capital es obligatorio.  ");

							}
							if (!Comunes.precisionValida(totalAdeudoCapital, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Total Adeudo- Capital no cumple con la longitud requerida. ");

							}
							if (totalAdeudoCapital_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Total Adeudo � Capital  no deber� ser f�rmula.  ");
							}

							// **************  10  Total Adeudo - Inter�s ***************

							if (totalAdeudoInteres.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Total Adeudo - Inter�s es obligatorio.  ");

							}
							if (!Comunes.precisionValida(totalAdeudoInteres, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Total Adeudo - Inter�s no cumple con la longitud requerida. ");

							}
							if (totalAdeudoInteres_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Total Adeudo � Inter�s  no deber� ser f�rmula.  ");
							}


							// ************** 11. Total Adeudo - Total***************+

							if (totalAdeudoTotal.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Total Adeudo - Total es obligatorio.  ");

							}
							if (!Comunes.precisionValida(totalAdeudoTotal, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Total Adeudo - Total no cumple con la longitud requerida. ");

							}
							if (totalAdeudoTotal_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Total Adeudo � Total  no deber� ser f�rmula.  ");
							}

							// ************** 12  Total Desembolso - Capital ***************

							if (totalDesembolsoCapital.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Total Desembolso - Capital es obligatorio. ");

							}
							if (!Comunes.precisionValida(totalDesembolsoCapital, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Total Desembolso - Capital no cumple con la longitud requerida. ");

							}
							if (totalDesembolsoCapital_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Total Desembolso � Capital  no deber� ser f�rmula.  ");
							}

							// ************** 13   Total Desembolso - Inter�s **************

							if (totalDesembolsoInteres.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Fondeo Total Desembolso - Inter�s es obligatorio. ");

							}
							if (!Comunes.precisionValida(totalDesembolsoInteres, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Total Desembolso - Inter�s no cumple con la longitud requerida. ");

							}
							if (totalDesembolsoInteres_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Total Desembolso � Inter�s  no deber� ser f�rmula.  ");
							}


							// ************** 14   Total Desembolso - Total ***************

							if (totalDesembolsoTotal.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Total Desembolso - Total es obligatorio.  ");

							}
							if (!Comunes.precisionValida(totalDesembolsoTotal, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Total Desembolso - Total no cumple con la longitud requerida. ");

							}
							if (totalDesembolsoTotal_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Total Desembolso � Total  no deber� ser f�rmula  ");
							}

							// ************** 15 Solicitado FIDE- Capital ***************+

							if (solicitadoFIDECapital.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Solicitado FIDE- Capital es obligatorio. ");

							}
							if (!Comunes.precisionValida(solicitadoFIDECapital, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Solicitado FIDE- Capital no cumple con la longitud requerida. ");

							}
							if (solicitadoFIDECapital_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Solicitado FIDE � Capital  no deber� ser f�rmula.  ");
							}


							// ************** 16 Solicitado FIDE- Interes ***************

							if (solicitadoFIDEInteres.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 "  El campo Solicitado FIDE- Inter�s  es obligatorio. ");

							}
							if (!Comunes.precisionValida(solicitadoFIDEInteres, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Solicitado FIDE- Inter�s  no cumple con la longitud requerida. ");

							}
							if (solicitadoFIDEInteres_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Solicitado FIDE � Inter�s   no deber� ser f�rmula.  ");
							}


							// ************** 17  Solicitado FIDE - Total ***************
							if (solicitadoFIDETotal.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Solicitado FIDE - Total es obligatorio. ");

							}
							if (!Comunes.precisionValida(solicitadoFIDETotal, 14, 2)) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El valor del campo Solicitado FIDE - Total no cumple con la longitud requerida. ");

							}
							if (solicitadoFIDETotal_sf.equals("")) {
								Errorlineas.add("Error en la Linea: " + iNumLinea +
													 " El campo Solicitado FIDE -  Total no deber� ser f�rmula ");
							}


							if (Errorlineas.size() > 0) { //cuando hay errores
												  lErrores.add(Errorlineas);
							} else if (Errorlineas.size() == 0) { //cuando no hay error
												
												  lineasBien.add(numeroPrestamo);
								totalFondoNafin = totalFondoNafin.add(new BigDecimal(fondeoNafinTotal));
								totalFondoJR = totalFondoJR.add(new BigDecimal(fondeoJRTotal));
								totalAdeudo = totalAdeudo.add(new BigDecimal(totalAdeudoTotal));
								totalDesembolso = totalDesembolso.add(new BigDecimal(totalDesembolsoTotal));
								totalSolicFIDE = totalSolicFIDE.add(new BigDecimal(solicitadoFIDETotal));


								lVarBind = new ArrayList();
								lVarBind.add(ic_desembolso);
								lVarBind.add(fechaHonrada);
								lVarBind.add(numeroPrestamo);
								lVarBind.add(numeroAcreditado);
								lVarBind.add(fondeoNafinCapital);
								lVarBind.add(fondeoNafinInteres);
								lVarBind.add(fondeoNafinTotal);
								lVarBind.add(fondeoJRCapital);
								lVarBind.add(fondeoJRInteres);
								lVarBind.add(fondeoJRTotal);
								lVarBind.add(totalAdeudoCapital);
								lVarBind.add(totalAdeudoInteres);
								lVarBind.add(totalAdeudoTotal);
								lVarBind.add(totalDesembolsoCapital);
								lVarBind.add(totalDesembolsoInteres);
								lVarBind.add(totalDesembolsoTotal);
								lVarBind.add(solicitadoFIDECapital);
								lVarBind.add(solicitadoFIDEInteres);
								lVarBind.add(solicitadoFIDETotal);

								//		log.debug("strSQL.toString()  "+strSQL.toString());
								//		log.debug("lVarBind  "+lVarBind);

								ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
								ps.executeUpdate();
								ps.close();
							}

						} //if(lineaArchivo.size()>0)

					} //if(x !=ultimo)  {

				} // for(int x=6; x<lDatos.size();  x++) {

			}

			if (iNumLinea != numberOfRegisters) {
				cifras = new ArrayList();
				cifras.add("El N�mero de Desembolsos no corresponde al contenido en el archivo "+iNumLinea);
			}


			if (lErrores.size() == 0) {
				HashMap mapa = new HashMap();
				mapa = new HashMap();
				mapa.put("CAMPO1", "Operaci�n ");
				mapa.put("CAMPO2", "Desembolsos FIDE ");
				mapa.put("CAMPO3", "TEXTO");
				cifrasControl.add(mapa);
				mapa = new HashMap();
				mapa.put("CAMPO1", "Fecha Honrada ");
				mapa.put("CAMPO2", String.valueOf(fechaHonrada));
				mapa.put("CAMPO3", "TEXTO");
				cifrasControl.add(mapa);
				mapa = new HashMap();
				mapa.put("CAMPO1", "No. Total de desembolsos ");
				mapa.put("CAMPO2", String.valueOf(lineasBien.size()));
				mapa.put("CAMPO3", "TEXTO");
				cifrasControl.add(mapa);
				mapa = new HashMap();
				mapa.put("CAMPO1", "Total Fondo Nafin ");
				mapa.put("CAMPO2", totalFondoNafin);
				mapa.put("CAMPO3", "MONTO");
				cifrasControl.add(mapa);
				mapa = new HashMap();
				mapa.put("CAMPO1", "Total Fondo JR ");
				mapa.put("CAMPO2", totalFondoJR);
				mapa.put("CAMPO3", "MONTO");
				cifrasControl.add(mapa);
				mapa = new HashMap();
				mapa.put("CAMPO1", "Total Adeudo ");
				mapa.put("CAMPO2", totalAdeudo);
				mapa.put("CAMPO3", "MONTO");
				cifrasControl.add(mapa);
				mapa = new HashMap();
				mapa.put("CAMPO1", "Total Desembolso ");
				mapa.put("CAMPO2", totalDesembolso);
				mapa.put("CAMPO3", "MONTO");
				cifrasControl.add(mapa);
				mapa = new HashMap();
				mapa.put("CAMPO1", "Total Solicitado FIDE ");
				mapa.put("CAMPO2", totalSolicFIDE);
				mapa.put("CAMPO3", "MONTO");
				cifrasControl.add(mapa);
			}

			listas.add(lErrores);
			listas.add(lineasBien);
			listas.add(cifras);
			listas.add(cifrasControl);

			if (lErrores.size() == 0 && cifras.size() == 0) {
				con.terminaTransaccion(true);
				log.debug("TRACE::commit()");
			} else {
				con.terminaTransaccion(false);
				log.debug("TRACE::rollback()");
			}


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("Error getValidarArchivo " + e);
			throw new AppException("Error al getValidarArchivo", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getValidarArchivo (S) ");
			}

		}
		return listas;

	}


	/**
	 *  Metodo para Generar el archivo con Errores
	 *  Fodea 07-2014  FONDO JR -Desembolsos FIDE
	 * @return
	 * @param path
	 * @param registros
	 */
	public String archivoErrores(List registros, List cifras, String path) {

		log.debug("archivoErrores (E)");
		String nombreArchivoTmp = "";

		try {
			CreaArchivo creaArchivo = new CreaArchivo();
			nombreArchivoTmp = creaArchivo.nombreArchivo() + ".xls";
			ComunesXLS xls = new ComunesXLS(path + nombreArchivoTmp);

			xls.setTabla(1);
			if (registros.size() > 0) {
				for (int x = 0; x < registros.size(); x++) {
					List infor = (ArrayList) registros.get(x);

					for (int y = 0; y < infor.size(); y++) {
						String linea = (String) infor.get(y);
						xls.setCelda(linea, "formas", ComunesXLS.LEFT, 1);
					}
				} //while(rs.next()){
			}

			if (cifras.size() > 0) {
				for (int x = 0; x < cifras.size(); x++) {
					String linea = (String) cifras.get(x);
					xls.setCelda(linea, "formas", ComunesXLS.LEFT, 1);
				}
			}

			xls.cierraTabla();
			xls.cierraXLS();


		} catch (Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			} catch (Exception e) {
			}
			log.debug("archivoErrores (S)");
		}
		return nombreArchivoTmp;
	}


	/**
	 *  Metodo para generar el archivo del Acuse
	 *  Fodea 07-2014  FONDO JR -Desembolsos FIDE
	 * @return
	 * @param parametros
	 */
	public String generarPDFAcuseDesembolso(List parametros) {
		log.info("  generarPDFAcuseDesembolso (E) ");
		String nombreArchivo = "";
		ComunesPDF pdfDoc = new ComunesPDF();

		try {

			String path = parametros.get(0).toString();
			//String noCliente  = parametros.get(1).toString();
			String strNombre = parametros.get(2).toString();
			String strNombreUsuario = parametros.get(3).toString();
			String strLogo = parametros.get(4).toString();
			String strDirectorioPublicacion = parametros.get(5).toString();
			//String strPerfil  =  parametros.get(6).toString();
			String num_folio = parametros.get(7).toString();
			String NoTotalDesembolsos = parametros.get(8).toString();
			String TotalFondoNafin = parametros.get(9).toString();
			String TotalFondoJR = parametros.get(10).toString();
			String TotalAdeudo = parametros.get(11).toString();
			String TotalDesembolso = parametros.get(12).toString();
			String TotalSolicitadoFIDE = parametros.get(13).toString();
			String FechaCarga = parametros.get(14).toString();
			String HoraCarga = parametros.get(15).toString();
			String Usuario = parametros.get(16).toString();
			String strPais = parametros.get(17).toString();
			String fechaHonrada = parametros.get(18).toString();


			String sesExterno = "";

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[] = {
						"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
						"Noviembre", "Diciembre"
			};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual = fechaActual.substring(0, 2);
			String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
			String anioActual = fechaActual.substring(6, 10);
			String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc, strPais, "", sesExterno, strNombre, strNombreUsuario, strLogo,
												  strDirectorioPublicacion);


			pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
								" ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);


			pdfDoc.addText(" Su Solicitud fue recibida con el n�mero de Folio " + num_folio, "formas", ComunesPDF.CENTER);

			//---------------------Acuse -------------------------
			pdfDoc.setTable(2, 50);
			pdfDoc.setCell("Cifras de Control ", "celda02", ComunesPDF.CENTER, 2);
			pdfDoc.setCell("Operaci�n ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell("Desembolsos FIDE ", "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Fecha Honrada ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell(fechaHonrada, "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("No. Total de Desembolsos ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell(NoTotalDesembolsos, "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Total Fondo Nafin  ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell("$" + Comunes.formatoDecimal(TotalFondoNafin, 2), "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Total Fondo JR  ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell("$" + Comunes.formatoDecimal(TotalFondoJR, 2), "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Total Adeudo  ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell("$" + Comunes.formatoDecimal(TotalAdeudo, 2), "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Total Desembolso  ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell("$" + Comunes.formatoDecimal(TotalDesembolso, 2), "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Total Solicitado FIDE  ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell("$" + Comunes.formatoDecimal(TotalSolicitadoFIDE, 2), "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Fecha de Carga ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell(FechaCarga, "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Hora de Carga ", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell(HoraCarga, "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell("Usuario", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell(Usuario, "formas", ComunesPDF.RIGHT);
			pdfDoc.addTable();


			pdfDoc.endDocument();

		} catch (Throwable e) {
			log.error(" Error  generarPDFAcuseDesembolso (S) " + e);
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			} catch (Exception e) {
			}
			log.info("  generarPDFAcuseDesembolso (S) ");
		}
		return nombreArchivo;
	}


	/**
	 *  Metodo para guardar el archivo
	 *  Fodea 07-2014  FONDO JR -Desembolsos FIDE
	 * @return
	 * @param parametros
	 */
	public boolean guardarAcuseyArchivo(List parametros) {
		log.info("guardarAcuseyArchivo (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
		boolean errorCorreo = true;

		try {
			con.conexionDB();

			String acuse = parametros.get(0).toString();
			String nombreUsuario = parametros.get(1).toString();
			String folio = parametros.get(2).toString();
			String noTotalDesembolsos = parametros.get(3).toString();
			String totalFondoNafin = parametros.get(4).toString();
			String totalFondoJR = parametros.get(5).toString();
			String totalAdeudo = parametros.get(6).toString();
			String totalDesembolso = parametros.get(7).toString();
			String totalSolicitadoFIDE = parametros.get(8).toString();
			//String fecha = parametros.get(9).toString();
			//String hora = parametros.get(10).toString();
			String rutaArchivoDesembolso = parametros.get(11).toString();
			String rutaArchivoAcuse = parametros.get(12).toString();
			String ic_desembolso = parametros.get(13).toString();
			String fechahonrada = parametros.get(14).toString();

			totalFondoNafin.replaceAll(",", " ");
			totalFondoJR.replaceAll(",", "");
			totalAdeudo.replaceAll(",", "");
			totalDesembolso.replaceAll(",", "");
			totalSolicitadoFIDE.replaceAll(",", "");


			strSQL = new StringBuffer();
			strSQL.append(" INSERT INTO  COM_DESEMBOLSO_FJR " +
							  " ( IC_DESEMBOLSO   ,  	   DF_FECHAHONRADA  ,  	 IN_NUM_PRESTAMO , 			CG_NOM_ACREDITADO ,  " +
							  " FN_NAFIN_CAPITAL , 	 FN_NAFIN_INTERES  ,  FN_NAFIN_TOTAL  ,   " +
							  " FN_JR_CAPITAL  , 		 FN_JR_INTERES  ,  	 FN_JR_TOTAL ,  " +
							  " FN_ADEUDO_CAPITAL ,    FN_ADEUDO_INTERES ,  FN_ADEUDO_TOTAL  ,   " +
							  " FN_DESEMBOLSO_CAPITAL, FN_DESEMBOLSO_INTERES  ,  FN_DESEMBOLSO_TOTAL, " + " FN_SOLICFIDE_CAPITAL ,  FN_SOLICFIDE_INTERES ,  FN_SOLICFIDE_TOTAL , CC_ACUSE   )  " +
							
							  " SELECT  IC_DESEMBOLSO   ,  	   DF_FECHAHONRADA  ,  	 IN_NUM_PRESTAMO , 			CG_NOM_ACREDITADO ,  " +
							  " FN_NAFIN_CAPITAL , 	 FN_NAFIN_INTERES  ,  FN_NAFIN_TOTAL  ,   " +
							  " FN_JR_CAPITAL  , 		 FN_JR_INTERES  ,  	 FN_JR_TOTAL ,  " +
							  " FN_ADEUDO_CAPITAL ,    FN_ADEUDO_INTERES ,  FN_ADEUDO_TOTAL  ,   " +
							  " FN_DESEMBOLSO_CAPITAL, FN_DESEMBOLSO_INTERES  ,  FN_DESEMBOLSO_TOTAL, " +
							  " FN_SOLICFIDE_CAPITAL ,  FN_SOLICFIDE_INTERES ,  FN_SOLICFIDE_TOTAL ,  '" + acuse + "'" +
							  " FROM COMTMP_DESEMBOLSO_FJR   " + " WHERE ic_desembolso = ?   ");


			lVarBind = new ArrayList();
			lVarBind.add(ic_desembolso);
			log.debug("strSQL " + strSQL.toString());
			log.debug("lVarBind " + lVarBind);
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();


			strSQL = new StringBuffer();
			strSQL.append(" INSERT INTO COM_ACUSE_DESEMBOLSO_FJR  " + " (  CG_OPERACION  , IN_TOTAL_DESEMBOLSO  ,  " +
							  "  FN_TOTAL_FNAFIN ,  FN_TOTAL_FJR   , FN_TOTAL_ADEUDO , FN_TOTAL_DESEMBOLSO , FN_TOTAL_SOLICFIDE ,  " +
							  " DF_FECHAHORA_CARGA ,  CG_USUARIO , CC_ACUSE  , CG_FOLIO , BI_DESEMBOLSOS  ,  BI_ACUSE   ) " +
							  " VALUES (  ? ,  ? , ? ,  ?, ?, ?, ? ,   Sysdate , ? ,  ?,  ? , '0', '0' )");

			lVarBind = new ArrayList();
			lVarBind.add("Desembolsos FIDE");
			lVarBind.add(noTotalDesembolsos);
			lVarBind.add(totalFondoNafin);
			lVarBind.add(totalFondoJR);
			lVarBind.add(totalAdeudo);
			lVarBind.add(totalDesembolso);
			lVarBind.add(totalSolicitadoFIDE);
			lVarBind.add(nombreUsuario);
			lVarBind.add(acuse);
			lVarBind.add(folio);
			log.debug("strSQL " + strSQL.toString());
			log.debug("lVarBind " + lVarBind);
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();


			File archivo1 = new File(rutaArchivoDesembolso);
			FileInputStream fileinputstream1 = new FileInputStream(archivo1);

			File archivo2 = new File(rutaArchivoAcuse);
			FileInputStream fileinputstream2 = new FileInputStream(archivo2);

			strSQL = new StringBuffer();
			strSQL.append(" UPDATE COM_ACUSE_DESEMBOLSO_FJR   " + " SET   BI_DESEMBOLSOS  = ? " + " , BI_ACUSE  = ? " +
							  " WHERE CC_ACUSE = ? ");

			log.debug("strSQL " + strSQL.toString());
			ps = con.queryPrecompilado(strSQL.toString());
			ps.setBinaryStream(1, fileinputstream1, (int) archivo1.length());
			ps.setBinaryStream(2, fileinputstream2, (int) archivo2.length());
			ps.setString(3, acuse);
			ps.executeUpdate();
			ps.close();

			borraTemporales(ic_desembolso); //Borra Temporales

			parametros = new ArrayList();
			parametros.add(fechahonrada);
			parametros.add(rutaArchivoAcuse);

			String mensaje = this.envioCorreoDesembolsoFJR(parametros); //envio de correo Automatico
			if (mensaje.equals("S")) {
				errorCorreo = false;
			}

		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("guardarAcuseyArchivo  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  guardarAcuseyArchivo (S) ");
			}
		}
		return errorCorreo;
	}


	/**
	 *  Metodo que borra los registros capturados en tabla temporal
	 *  Fodea 07-2014  FONDO JR -Desembolsos FIDE
	 * @param ic_desembolso
	 */

	public void borraTemporales(String ic_desembolso) {
		log.info("borraTemporales (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();

		try {
			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" DELETE  COMTMP_DESEMBOLSO_FJR   WHERE IC_DESEMBOLSO = ? ");
			lVarBind.add(ic_desembolso);
			log.debug("strSQL " + strSQL.toString());
			log.debug("lVarBind " + lVarBind);
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();

		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("borraTemporales  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  borraTemporales (S) ");
			}
		}
	}


	/** Metodo para enviar correo
	 *  Fodea 07-2014  FONDO JR -Desembolsos FIDE
	 * @param parametros
	 */
	public String envioCorreoDesembolsoFJR(List parametros) {
		log.info("envioCorreoDesembolsoFJR(E)");

		Correo correo = new Correo();
		StringBuffer contenido = new StringBuffer();
		String menError = "N";
		try {


			String fechaHonrada = parametros.get(0).toString();
			String nombreArchivo = parametros.get(1).toString();
			String destinatario = this.consEnvioCorreo("D");
			String destinatarioCC = this.consEnvioCorreo("C");
			String asunto = "Desembolsos FIDE ";


			contenido.append("<br> <table width='600' align='CENTER' border='0'  >" +
								  " <tr> <td align='left' width='200'  style='font-size:11pt;font-family: Calibri'> " +
								  " Por medio del presente se notifica que se han realizado los desembolsos con fecha honrada del " +
								  fechaHonrada + "</td>   <tr> " +
								  " <tr> <td align='left' width='200'  style='font-size:11pt;font-family: Calibri'>" +
								  "  Se anexa acuse de validaci�n. </td> </tr>" + " </table> <br>  ");


			contenido.append(" <BR>ATENTAMENTE" + " <BR>Nacional Financiera S.N.C. <BR>");
			contenido.append("</span></span>");

			ArrayList listaDeImagenes = null;
			ArrayList listaDeArchivos = new ArrayList();

			HashMap archivoAdjunto = new HashMap();
			archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo);
			listaDeArchivos.add(archivoAdjunto);

			if (!destinatario.equals("") || !destinatarioCC.equals("")) {
				correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", destinatario, destinatarioCC, asunto,
															  contenido.toString(), listaDeImagenes, listaDeArchivos);
			} else {
				menError = "S";
			}

		} catch (Throwable e) {
			log.error("Error al enviar correo  " + e);
			throw new AppException("Error al enviar correo ", e);
		} finally {
			log.info("envioCorreoDesembolsoFJR(S)");
		}
		return menError;
	}


	/**
	 * metodo para guardar los registros de la pantalla
	 * Desembolsos FIDE / Parametrizaci�n de Correo
	 * @param parametros
	 */

	public void guardaParamCorreo(List parametros) {
		log.info("guardaParamCorreo (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();

		try {
			con.conexionDB();

			String nombre = parametros.get(0).toString();
			String correo = parametros.get(1).toString();
			String tipo = parametros.get(2).toString();


			strSQL = new StringBuffer();
			strSQL.append(" INSERT INTO COM_PARAMCORREO_FJR (  IC_CORREO , CG_NOMBRE ,    CG_CORREO ,   CG_TIPO_DES   )    " +
							  " VALUES ( SEQ_COM_PARAMCORREO_FJR.nextval ,  ?, ?, ?     )    ");

			lVarBind.add(nombre);
			lVarBind.add(correo);
			lVarBind.add(tipo);

			log.debug("strSQL " + strSQL.toString());
			log.debug("lVarBind " + lVarBind);

			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();

		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("guardaParamCorreo  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  guardaParamCorreo (S) ");
			}
		}
	}

	/**
	 * metodo para elimiant los registros de la pantalla
	 * Desembolsos FIDE / Parametrizaci�n de Correo
	 * @param parametros
	 */

	/**
	 * metodo para guardar los registros de la pantalla ,   Desembolsos FIDE / Parametrizaci�n de Correo
	 * @param ic_correo
	 */

	public void eliminarParamCorreo(String ic_correo) {
		log.info("eliminarParamCorreo (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();

		try {
			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" DELETE  FROM   COM_PARAMCORREO_FJR WHERE   IC_CORREO =  ?    ");
			lVarBind.add(ic_correo);
			log.debug("strSQL " + strSQL.toString());
			log.debug("lVarBind " + lVarBind);
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();

		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("eliminarParamCorreo  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  eliminarParamCorreo (S) ");
			}
		}
	}

	/**
	 * metodo para la consulta de los registros de la pantalla ,   Desembolsos FIDE / Parametrizaci�n de Correo
	 * para la opc+om del Catalogo  Parametrizaci�n de Correo
	 * @return
	 */
	public List consParamCorreo() {
		log.info("consParamCorreo (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List registros = new ArrayList();
		HashMap datos = new HashMap();


		try {

			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" SELECT  IC_CORREO  ,   CG_NOMBRE  ,   CG_CORREO  ,    " +
							  " DECODE (CG_TIPO_DES, 'D','Destinatario', 'C', 'CC') AS TIPO  	" +
							  " FROM   COM_PARAMCORREO_FJR  ");

			ps = con.queryPrecompilado(strSQL.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				datos = new HashMap();
				String ic_correo = rs.getString("IC_CORREO") == null ? "" : rs.getString("IC_CORREO");
				String nombre = rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE");
				String correo = rs.getString("CG_CORREO") == null ? "" : rs.getString("CG_CORREO");
				String tipo = rs.getString("TIPO") == null ? "" : rs.getString("TIPO");

				datos.put("IC_CORREO", ic_correo);
				datos.put("CG_NOMBRE", nombre);
				datos.put("CG_CORREO", correo);
				datos.put("TIPO", tipo);
				registros.add(datos);
			}
			rs.close();
			ps.close();


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("consParamCorreo  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  consParamCorreo (S) ");
			}
		}
		return registros;
	}


	/**
	 * Metodo para obtener los correos para el envio del correo en la carga de Archivo
	 * Desembolsos FIDE
	 * @return
	 * @param tipoDestinatario
	 */
	private String consEnvioCorreo(String tipoDestinatario) {
		log.info("consEnvioCorreo (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		StringBuffer correos = new StringBuffer();
		List lVarBind = new ArrayList();

		try {

			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" SELECT  CG_CORREO   " + " FROM   COM_PARAMCORREO_FJR   " + " 	WHERE CG_TIPO_DES  =  ? ");
			lVarBind.add(tipoDestinatario);

			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			rs = ps.executeQuery();

			while (rs.next()) {
				correos.append(rs.getString("CG_CORREO") == null ? "" : rs.getString("CG_CORREO") + ",");
			}
			rs.close();
			ps.close();

			if (correos.length() > 0) {
				correos.deleteCharAt(correos.length() - 1);
			}


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("consEnvioCorreo  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  consEnvioCorreo (S) ");
			}
		}
		return correos.toString();
	}

	private boolean validaPrestamoPrepagado(AccesoDB con, String prestamo, String fecVenc, String disposicion,
														 String totalvencimiento, String cvePrograma) throws AppException {
		  //AccesoDB con = new AccesoDB();
		  PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		boolean existPrepago = false;
		try {
			//con.conexionDB();

			strSQL = "SELECT COUNT(1) conteo " + "  FROM com_pagos_fide_val " + " WHERE ig_prestamo = ? " +
						"   AND cg_estatus = 'T' " + "   AND cs_validacion = 'A' " + "   AND ic_programa_fondojr = ? " +
						"   AND ROWNUM = 1 ";

			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, Long.parseLong(prestamo));
			ps.setLong(2, Long.parseLong(cvePrograma));
			rs = ps.executeQuery();

			if (rs.next()) {
				if (rs.getInt("conteo") > 0) {
					existPrepago = true;
				}
			}

			rs.close();
			ps.close();

			if (existPrepago) {
				strSQL = " UPDATE com_pagos_fide " + "   SET cg_estatus_proc = ? " +
							  " WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') " + "   AND ig_prestamo = ? " +
							  "   AND fg_totalvencimiento = ? " + "   AND ig_disposicion = ? " + "   AND cg_estatus_proc = ? " +
							  "   AND ic_programa_fondojr = ? ";
				ps = con.queryPrecompilado(strSQL);
				ps.setString(1, "P");
				ps.setString(2, fecVenc);
				ps.setLong(3, Long.parseLong(prestamo));
				ps.setDouble(4, Double.parseDouble(totalvencimiento));
				ps.setLong(5, Long.parseLong(disposicion));
				ps.setString(6, "N");
				ps.setLong(7, Long.parseLong(cvePrograma));
				ps.executeUpdate();
				ps.close();
			}

			return existPrepago;
		} catch (Throwable t) {
			throw new AppException("Error al validar si el prestamo ya fue prepagado.", t);
		}
	}

	public boolean validaDesembolsoPrepagado(String prestamo, String cvePrograma) throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		boolean existPrepago = false;
		try {
			con.conexionDB();

			strSQL = "SELECT COUNT(1) conteo " + "  FROM com_pagos_fide_val " + " WHERE ig_prestamo = ? " +
						"   AND cg_estatus = 'T' " + "   AND cs_validacion = 'A' " + "   AND ic_programa_fondojr = ? " +
						"   AND ROWNUM = 1 ";

			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, Long.parseLong(prestamo));
			ps.setLong(2, Long.parseLong(cvePrograma));
			rs = ps.executeQuery();

			if (rs.next()) {
				if (rs.getInt("conteo") > 0) {
					existPrepago = true;
				}
			}

			rs.close();
			ps.close();

			return existPrepago;
		} catch (Throwable t) {
			throw new AppException("Error al validar si el prestamo ya fue prepagado.", t);
		} finally {
			if (con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}

	//public String crearXLSDetValidMonitor(Strind) throws AppException{
	public String crearXLSDetValidMonitor(String ver_enlace, String estatus, String estatus2, String proc, String fec_venc,
													  String cvePrograma, String strDirectorioTemp) throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		String qryRownum = "";

		List varBind = new ArrayList();
		List titulo = new ArrayList();

		String nombreArchivo = "";

		System.out.println("��� FondoJuniorBean �� ObtenerDetalleDeCreditos(I) ���");
		System.out.println("��� ver_enlace �� " + ver_enlace + " ���");
		System.out.println("��� estatus �� " + estatus + " ���");
		try {
			con.conexionDB();
			//if(numRegistros>1000)
			//qryRownum = " AND rownum < 1001 ";

			if ("Recuperaciones".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,   " +
								  "       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,   " +
								  "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,   " +
								  "       tmp.ig_prestamo AS prestamo,   " + "       tmp.ig_cliente AS numsirac,   " +
								  "       NVL(vista.cg_nombrecliente,'') AS cliente,   " + "       tmp.ig_disposicion AS numcuota,   " +
								  "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error,   " +
								  "		  tmp.cg_estatus as estatus " + "  FROM comvis_vencimiento_fide vista,    " +
								  "       com_pagos_fide tmp    " + " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago  " +
								  "	AND vista.ig_cliente(+)  = tmp.ig_cliente  " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion  " +
								  "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento  " +
								  "   AND vista.ig_prestamo(+) = tmp.ig_prestamo  " + " AND tmp.CG_ESTATUS = ?   " +
								  " AND 'RC' = FN_ES_RECUPERACION_FONDOJR (tmp.COM_FECHAPROBABLEPAGO, tmp.IG_PRESTAMO,    " +
								  "                      tmp.IG_DISPOSICION, tmp.FG_TOTALVENCIMIENTO)    " +
								  " AND tmp.ic_programa_fondojr = ? " + qryRownum);

				varBind.add(estatus);
				varBind.add(new Long(cvePrograma));
				titulo.add("Recuperaciones Recibidas");
				//verdetalle_temp.add(titulo);
			} else if ("NoEncontrados".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (tmp.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
								  "'' AS fechaoperacion," + "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,   " +
								  "       tmp.IG_PRESTAMO AS prestamo, " + "       tmp.IG_CLIENTE AS numsirac, " + "'' AS cliente, " +
								  "       tmp.IG_DISPOSICION AS numcuota, " + "       tmp.fg_totalvencimiento AS capital_mas_interes, " +
								  "		  tmp.cg_estatus as estatus " + "       FROM com_pagos_fide tmp      " +
								  "WHERE  tmp.cg_estatus in (?,?)  ");
				if ("RC".equals(estatus2)) {
					strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				}
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
				strSQL.append("  AND NOT EXISTS  " + "    ( " + "     SELECT vp.ig_prestamo  " +
								  "              FROM comvis_vencimiento_fide vp                   " +
								  "             WHERE vp.com_fechaprobablepago = tmp.com_fechaprobablepago " +
								  "               AND vp.ig_prestamo = tmp.ig_prestamo " +
								  "               AND vp.ig_cliente  = tmp.ig_cliente " +
								  "               AND vp.ig_disposicion = tmp.ig_disposicion " +
								  "               AND vp.fg_totalvencimiento = tmp.fg_totalvencimiento " + "     ) " +
								  "	  AND tmp.ic_programa_fondojr = ? " + qryRownum);
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(estatus2);

				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);

				varBind.add(new Long(cvePrograma));
				titulo.add("Registros No encontrados");
				//verdetalle_temp.add(titulo);

			} else if ("NoEnviados".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (vista.COM_FECHAPROBABLEPAGO, 'dd/mm/yyyy') AS fechaautorizacion, " +
								  "       TO_CHAR (vista.DF_FECHAOPERA, 'dd/mm/yyyy') AS fechaoperacion,   " + "'' AS fechapago, " +
								  "       vista.IG_PRESTAMO AS prestamo, " + "       vista.IG_CLIENTE AS numsirac, " + "'' AS cliente, " +
								  "       vista.IG_DISPOSICION AS numcuota, " + "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes, " +
								  "		  '' as estatus " + "       FROM comvis_vencimiento_fide vista     " + "WHERE NOT EXISTS  " + "    ( " +
								  "     SELECT tmp.ig_prestamo  " + "              FROM com_pagos_fide tmp                   " +
								  "             WHERE tmp.COM_FECHAPROBABLEPAGO = vista.COM_FECHAPROBABLEPAGO " +
								  "               AND tmp.ig_prestamo = vista.ig_prestamo " +
								  "               AND tmp.ig_cliente  = vista.ig_cliente " +
								  "               AND tmp.ig_disposicion = vista.ig_disposicion " +
								  "               AND tmp.fg_totalvencimiento = vista.fg_totalvencimiento " +
								  "               AND tmp.CG_ESTATUS in (?,?) " + "					 AND tmp.ic_programa_fondojr = ? " + //FODEA 026-2010 FVR
								  "     ) ");

				if (!fec_venc.equals("") && fec_venc.length() > 2)
					strSQL.append(" AND vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");

				strSQL.append(" AND vista.ic_programa_fondojr = ? " + qryRownum); //FODEA 026-2010 FVR

				varBind.add(estatus);
				varBind.add(estatus2);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				if (!fec_venc.equals("") && fec_venc.length() > 2)
					varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				titulo.add("Cr&eacute;ditos No enviados del d&iacute;a");
				//verdetalle_temp.add(titulo);

			} else if ("Vencimientos".equals(ver_enlace)) {
				strSQL.append("SELECT TO_CHAR (vista.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								  "       TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy') AS fechaoperacion, " + "       '' AS fechapago, " +
								  "       vista.ig_prestamo AS prestamo,  " + "       vista.ig_cliente AS numsirac,  " +
								  "       vista.cg_nombrecliente AS cliente,  " + "       vista.ig_disposicion AS numcuota,  " +
								  "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes,  " + "		  '' as estatus " +
								  "  FROM comvis_vencimiento_fide vista " + " WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
								  " AND vista.ic_programa_fondojr = ? " + qryRownum); //FODEA 026-2010 FVR);
				varBind.add(fec_venc);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
				titulo.add("Vencimientos del d&iacute;a");
				//verdetalle_temp.add(titulo);
			} else {
				strSQL.append("SELECT DISTINCT TO_CHAR (tmp.com_fechaprobablepago, 'dd/mm/yyyy') AS fechaautorizacion,  " +
								  //FODEA 057 - 2009 ACF
					"       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion,  " +
								 "       TO_CHAR (tmp.df_pago_cliente, 'dd/mm/yyyy') AS fechapago,  " +
								 "       tmp.ig_prestamo AS prestamo,  " + "       tmp.ig_cliente AS numsirac,  " +
								 "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       tmp.ig_disposicion AS numcuota,  " +
								 "       tmp.fg_totalvencimiento AS capital_mas_interes , " + "       tmp.cg_error AS error,  " +
								 "		  tmp.cg_estatus as estatus " + "  FROM comvis_vencimiento_fide vista,   " + "       com_pagos_fide tmp  " +
								 " WHERE vista.com_fechaprobablepago(+) = tmp.com_fechaprobablepago " +
								 "   AND vista.ig_cliente(+)  = tmp.ig_cliente " + "   AND vista.ig_disposicion(+) = tmp.ig_disposicion " +
								 "   AND vista.fg_totalvencimiento(+) = tmp.fg_totalvencimiento " +
								 "   AND vista.ig_prestamo(+) = tmp.ig_prestamo " + "	 AND tmp.ic_programa_fondojr = ? " +
								 qryRownum);
				varBind.add(new Long(cvePrograma)); //FODEA 026-2010 FVR
			}

			if ("Enviados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in ('P','NP') " +
								  " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
				varBind.add(fec_venc);
				titulo.add("Total de Registros Enviados");
				//verdetalle_temp.add(titulo);
			}

			if ("RembolsosPrepagosTotales".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus in (?) "); //FODEA 026-2010 FVR SE QUITA 'R'
				if ("R".equals(estatus)) {
					titulo.add("Total Reembolsos");
					varBind.add(estatus);
					if (!fec_venc.equals("") && fec_venc.length() > 2) {
						strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
						varBind.add(fec_venc);
					}

					strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("RC".equals(estatus)) {
					titulo.add("Total Recuperaciones");
					varBind.add(estatus2);
					strSQL.append(" AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
									  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
									  "                                tmp.fg_totalvencimiento " +
									  "                                )   ");
				} else if ("T".equals(estatus)) {
					titulo.add("Total Prepragos");
					varBind.add(estatus);
				}

				//verdetalle_temp.add(titulo);
			}

			if ("Prepagos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? ");
				varBind.add(estatus);
				titulo.add("Prepagos Totales");
				//verdetalle_temp.add(titulo);
			}
			if ("Pagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				titulo.add("Cr&eacute;ditos Pagados del D&iacute;a");
				//verdetalle_temp.add(titulo);

			}
			if ("NoPagados".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);
				titulo.add("Cr&eacute;ditos No Pagados del D&iacute;a");
				//verdetalle_temp.add(titulo);

			}
			if ("Prepagados".equals(ver_enlace)) { //Modficacion F006-2014 req 1

				strSQL.append(" AND tmp.cg_estatus in (?, ?)  ");
				varBind.add(estatus);
				varBind.add(estatus2);

				if (!fec_venc.equals("") && fec_venc.length() > 2) {
					strSQL.append(" AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
					varBind.add(fec_venc);
				}

				strSQL.append(" AND tmp.cg_estatus_proc = ? ");
				varBind.add(proc);
				titulo.add("Registros Rechazados Cr&eacute;ditos Prepagados");
				//verdetalle_temp.add(titulo);

			}
			if ("NoPagadosRech".equals(ver_enlace)) { //F027-2011 FVR
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
								  " AND tmp.cg_estatus_proc = ? ");
				//varBind.add(proc);
				varBind.add(estatus);
				varBind.add(fec_venc);
				varBind.add(proc);
				titulo.add("Cr&eacute;ditos No Pagados Rechazados del D&iacute;a");
				//verdetalle_temp.add(titulo);

			}
			if ("Reembolsos".equals(ver_enlace)) {
				strSQL.append(" AND tmp.cg_estatus = ? " + " AND tmp.cg_estatus_proc = ? " +
								  " AND 'RC' != fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, tmp.ig_prestamo,   " +
								  "                      tmp.ig_disposicion, tmp.fg_totalvencimiento)  ");
				varBind.add(estatus);
				varBind.add("N");
				if (!fec_venc.equals("") && fec_venc.length() > 2) {
					strSQL.append("AND tmp.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') ");
					varBind.add(fec_venc);
				}
				titulo.add("Reembolsos Totales");
				//verdetalle_temp.add(titulo);
			}
			if ("ConError".equals(ver_enlace)) {
				if (!fec_venc.equals("") && fec_venc.length() > 2 && !"R".equals(estatus)) {
					strSQL.append(" AND tmp.cg_estatus in (?,?) " + " AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') " +
									  " AND tmp.cg_error is not null ");
					//varBind.add(proc);
					strSQL.append(" UNION ALL SELECT TO_CHAR(vista.com_fechaprobablepago,'dd/mm/yyyy') as fechaautorizacion, " +
									  "       nvl(TO_CHAR (vista.df_fechaopera ,'dd/mm/yyyy'),'') AS fechaoperacion, " + "			''  AS fechapago, " +
									  "		  vista.ig_prestamo as prestamo, " + "       vista.ig_cliente as numsirac, " +
									  "       NVL(vista.cg_nombrecliente,'') AS cliente,  " + "       vista.ig_disposicion AS numcuota,  " +
									  "       vista.FG_TOTALVENCIMIENTO AS capital_mas_interes , " +
									  "       'Registro no Enviado por el FIDE' AS error,  " + "		  '' as estatus " +
									  "  FROM comvis_vencimiento_fide vista " + "	WHERE vista.com_fechaprobablepago = to_date(?,'dd/mm/yyyy') " +
									  "	AND vista.ic_programa_fondojr = ? " + "	AND NOT EXISTS ( " + "          SELECT 1 " +
									  "            FROM com_pagos_fide c " +
									  "           WHERE c.com_fechaprobablepago = vista.com_fechaprobablepago " +
									  "             AND c.ig_prestamo = vista.ig_prestamo " + "             AND c.ig_cliente = vista.ig_cliente " +
									  "             AND c.ig_disposicion = vista.ig_disposicion " +
									  "             AND c.fg_totalvencimiento = vista.fg_totalvencimiento " + "				  AND c.cg_estatus in (?,?) " +
									  "				  AND c.ic_programa_fondojr = ?) " + qryRownum);
					varBind.add("P");
					varBind.add("NP");
					varBind.add(fec_venc);
					varBind.add(fec_venc);
					varBind.add(new Long(cvePrograma));
					varBind.add("P");
					varBind.add("NP");
					varBind.add(new Long(cvePrograma));
				} else {
					if ("R".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus);
						if (!fec_venc.equals("") && fec_venc.length() > 2) {
							strSQL.append(" AND tmp.COM_FECHAPROBABLEPAGO = to_date(?,'dd/mm/yyyy') ");
							varBind.add(fec_venc);
						}
						strSQL.append(" AND 'RC' != " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else if ("RC".equals(estatus)) {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add(estatus2);

						strSQL.append("AND 'RC' = " + "		fn_es_recuperacion_fondojr (tmp.com_fechaprobablepago, " +
										  "     									tmp.ig_prestamo, " + "                                tmp.ig_disposicion, " +
										  "                                tmp.fg_totalvencimiento " +
										  "                                )   ");
					} else {
						strSQL.append(" AND tmp.cg_estatus in (?) " + " AND tmp.cg_error is not null ");
						varBind.add("T");
					}
				}


				titulo.add("Cr&eacute;ditos con ERROR");
				//verdetalle_temp.add(titulo);
			}

			ComunesXLS xlsDoc = null;
			CreaArchivo creaArchivo = new CreaArchivo();
			int cont = 0;
			System.out.println("strSQL.toString()>>>>>>" + strSQL.toString());
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				//List verdetalle = new ArrayList();
				//HashMap hmDetalle = new HashMap();
				//hmDetalle.put("ESTATUS",rst.getString("estatus")==null?"":rst.getString("estatus"));
				//verdetalle_temp.add(hmDetalle);

				if (cont == 0) {
					nombreArchivo = creaArchivo.nombreArchivo() + ".xls";
					xlsDoc = new ComunesXLS(strDirectorioTemp + nombreArchivo);
					xlsDoc.setTabla(8);
					xlsDoc.setCelda("FECHA AUTORIZACION NAFIN", "celda01", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda("FECHA OPERACION", "celda01", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda("FECHA OPERACION CLIENTE", "celda01", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda("PRESTAMO", "celda01", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda("NUM. CLIENTE SIRAC", "celda01", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda("CLIENTE", "celda01", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda("NUM. CUOTA A PAGAR", "celda01", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda("CAPITAL MAS INTERES", "celda01", ComunesXLS.CENTER, 1);
				}

				xlsDoc.setCelda(rst.getString(1) == null ? "" : rst.getString(1), "formas", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda(rst.getString(2) == null ? "" : rst.getString(2), "formas", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda(rst.getString(3) == null ? "" : rst.getString(3), "formas", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda(rst.getString(4) == null ? "" : rst.getString(4), "formas", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda(rst.getString(5) == null ? "" : rst.getString(5), "formas", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda(rst.getString(6) == null ? "" : rst.getString(6), "formas", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda(rst.getString(7) == null ? "" : rst.getString(7), "formas", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda(rst.getString(8) == null ? "" : rst.getString(8), "formas", ComunesXLS.CENTER, 1);
				cont++;

			}

			rst.close();
			pst.close(); //se agrega por correccion a errore del mes del 09/2010

			if (cont > 0) {
				xlsDoc.cierraTabla();
				xlsDoc.cierraXLS();
			}

			return nombreArchivo;

		} catch (Throwable t) {
			throw new AppException("Error al generar XLS del detallle de reg validados en monitor", t);
		} finally {
			con.terminaTransaccion(true); //se agrega por correccion a errore del mes del 09/2010
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			//System.out.println("��� strSQL ��� "+strSQL);
			//System.out.println("��� varBind �� " +varBind+ " ���");
			//System.out.println("��� FondoJuniorBean �� ObtenerDetalleDeCreditos(F) ���");
		}
	}

	/** Fodea 06-2014
	 * Actualiza
	 * @param numero_prestamo
	 */
	public void actPrepagoValidacion(String numero_prestamo) {
		log.info("actPrepagoValidacion (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();

		try {

			con.conexionDB();

			strSQL = new StringBuffer();
			lVarBind = new ArrayList();
			strSQL.append("  Update  COM_PAGOS_FIDE_VAL  " + " set   PREPAGO_VALIDACION  = ? " +
							  " where  IG_PRESTAMO in ( Select IG_PRESTAMO  from COM_PAGOS_FIDE_VAL where cg_estatus= ?  and  IG_PRESTAMO =   ?  and  cs_validacion  = ? )  " +
							  " and cg_estatus =  ?      " + " and  cs_validacion  = ?   ");

			lVarBind.add("S");
			lVarBind.add("T");
			lVarBind.add(numero_prestamo);
			lVarBind.add("A");
			lVarBind.add("NP");
			lVarBind.add("A");

			log.debug("strSQL.toString()  " + strSQL.toString());
			log.debug("lVarBind  " + lVarBind);

			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();

		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("actPrepagoValidacion  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  actPrepagoValidacion (S) ");
			}
		}
	}


	private void actualizaPrepagoValidacionCreditos(AccesoDB con, List parametros_consulta,
																	String folioValidacion) throws NafinException {
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		System.out.println("..:: FondoJuniorBean : actualizaPrepagoValidacionCreditos(I) ::..");
		try {
			String fecha_amort_nafin_ini = (String) parametros_consulta.get(0);
			String fecha_amort_nafin_fin = (String) parametros_consulta.get(1);
			//String estatus_registro = (String)parametros_consulta.get(2);
			String numero_prestamo = (String) parametros_consulta.get(3);
			String clave_if = (String) parametros_consulta.get(4);
			String numero_sirac = (String) parametros_consulta.get(5);
			String fecha_pago_cliente_fide_ini = (String) parametros_consulta.get(6);
			String fecha_pago_cliente_fide_fin = (String) parametros_consulta.get(7);
			String fecha_registro_fide_ini = (String) parametros_consulta.get(8);
			String fecha_registro_fide_fin = (String) parametros_consulta.get(9);
			// String tipo_validacion = (String)parametros_consulta.get(10);
			//String seccion_estatus = (String)parametros_consulta.get(11);
			//String fecha_amortizacion = "";

			if (fecha_amort_nafin_ini.equals("")) {
				strSQL = new StringBuffer();
				strSQL.append(" SELECT TO_CHAR(MIN(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
				strSQL.append(" FROM com_pagos_fide_val cpf");

				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();

				while (rst.next()) {
					fecha_amort_nafin_ini =
									 rst.getString("fecha_amort_nafin") == null ? "" : rst.getString("fecha_amort_nafin");
				}

				rst.close();
				pst.close();
			}

			if (fecha_amort_nafin_fin.equals("")) {
				strSQL = new StringBuffer();
				strSQL.append(" SELECT TO_CHAR(MAX(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
				strSQL.append(" FROM com_pagos_fide_val cpf");

				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();

				while (rst.next()) {
					fecha_amort_nafin_fin =
									 rst.getString("fecha_amort_nafin") == null ? "" : rst.getString("fecha_amort_nafin");
				}

				rst.close();
				pst.close();
			}

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" UPDATE com_pagos_fide_val");
			strSQL.append(" SET prepago_validacion = ?");
			strSQL.append(" WHERE ig_prestamo IN (");
			strSQL.append(" SELECT /*+ leading(cpf) use_nl(cpf ccb prg cpi pyme)*/ cpf.ig_prestamo");
			strSQL.append(" FROM com_pagos_fide_val cpf");
			strSQL.append(", comcat_origen_programa prg");
			if (!clave_if.equals("")) { //..:: intermediario financiero
				strSQL.append(", comcat_pyme pyme");
				strSQL.append(", comrel_cuenta_bancaria ccb");
				strSQL.append(", comrel_pyme_if cpi");
			}
			strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
			strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
			strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
			strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
			if (!numero_prestamo.equals("")) { //..:: numero de prestamo
				strSQL.append(" AND cpf.ig_prestamo = ?");
			}
			if (!clave_if.equals("")) { //..:: intermediario financiero
				strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
				strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
				strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
				strSQL.append(" AND pyme.cs_habilitado = ?");
				strSQL.append(" AND cpi.cs_vobo_if = ?");
				strSQL.append(" AND ccb.ic_moneda = ?");
				strSQL.append(" AND cpi.ic_if = ?");
			}
			if (!numero_sirac.equals("")) { //..:: numero sirac cliente
				strSQL.append(" AND cpf.ig_cliente = ?");
			}
			if (!fecha_pago_cliente_fide_ini.equals("") &&
				 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
						strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
			}
			if (!fecha_registro_fide_ini.equals("") &&
				 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
						strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
			}

			varBind.add("S");
			//varBind.add(new Long(folioValidacion));//-----------------------------------AJUSTE VALIDACION ACF
			varBind.add("A"); //..:: estatus de la validaci�n
			varBind.add(fecha_amort_nafin_ini); //..:: fecha de amortizacion nafin
			varBind.add(fecha_amort_nafin_fin); //..:: fecha de amortizacion nafin
			if (!numero_prestamo.equals("")) { //..:: numero de prestamo
				varBind.add(new Long(numero_prestamo));
			}
			if (!clave_if.equals("")) { //..:: intermediario financiero
				varBind.add("S");
				varBind.add("S");
				varBind.add(new Integer(1));
				varBind.add(new Integer(clave_if));
			}
			if (!numero_sirac.equals("")) { //..:: numero sirac cliente
				varBind.add(new Long(numero_sirac));
			}
			if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
						varBind.add(fecha_pago_cliente_fide_ini);
				varBind.add(fecha_pago_cliente_fide_fin);
			}
			if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
						varBind.add(fecha_registro_fide_ini);
				varBind.add(fecha_registro_fide_fin);
			}

			strSQL.append(" AND cpf.cg_estatus = ? "); //..:: estatus del registro
			varBind.add("T");
			strSQL.append(" AND cpf.ig_folio_val = ? "); //..:: estatus del registro
			varBind.add(new Long(folioValidacion));
			strSQL.append(")");
			strSQL.append(" AND cs_validacion = ?"); //..:: estatus de la validaci�n
			varBind.add("A");
			strSQL.append(" AND cg_estatus = ?"); //..:: estatus del registro
			varBind.add("NP");


			System.out.println("..:: strSQL : " + strSQL.toString());
			System.out.println("..:: varBind : " + varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			//codigo para la actualizacion de


		} catch (Exception e) {

			System.out.println("..:: FondoJuniorBean : actualizaPrepagoValidacionCreditos(ERROR) :");
			e.printStackTrace();
			throw new AppException();
		} finally {

			System.out.println("..:: FondoJuniorBean : actualizaPrepagoValidacionCreditos(F) ::..");
		}

	}
	//Fodea XX 2014

	 /**
	     * Funci�n que agrega un nuevo registro a la tabla COMCAT_PROGRAMA_FONDOJR
	     * @throws com.netro.exception.NafinException
	     * @return nos regresa un tipo de dato boolean true o false dependiendo si se agrego o no el registro.
	     * @param descripcion es la descripci�n del programa que se agregara.
	     * @param intermediario   es la clave del Intermediario Financiero. 
	     */
	    public boolean agregarPrograma(String intermediario , String descripcion){
	       log.debug("agregarPrograma(E)");
	       AccesoDB    con = new AccesoDB();
	       PreparedStatement ps = null;
	       ResultSet rs = null;
	       boolean     commit = true;
	       String cveCat = "";
	       try{
	          con.conexionDB();
	          String query  = "SELECT NVL(MAX(IC_PROGRAMA_FONDOJR), 0) + 1 AS ClaveCatalogo FROM COMCAT_PROGRAMA_FONDOJR ";
	          rs = con.queryDB(query);
	          if (rs.next()){
	             cveCat = rs.getString("CLAVECATALOGO");
	          }
	          rs.close();
	          con.cierraStatement();
	          String query1 = " insert into COMCAT_PROGRAMA_FONDOJR "+
	                        " (IC_PROGRAMA_FONDOJR, IC_IF, CG_DESCRIPCION) "+
	                        "  values('"+cveCat+"',"+intermediario+",'" + descripcion+"')";

	          log.debug("Catalogo Programa FJ  "+query1);
	          con.ejecutaSQL(query1);
	          con.terminaTransaccion(true);
	          
	       }catch(Exception e){
	          commit = false;
	          throw new AppException("Error al agregar  el programa ", e);

	       }finally{
	          try{
	             if(rs!=null)rs.close();
	             if(ps!=null)ps.close();
	          }catch(Exception t){
	             log.error("agregarPrograma():: Error al cerrar Recursos: " + t.getMessage());
	          }
	          if(con.hayConexionAbierta()){
	             con.terminaTransaccion(commit);
	             con.cierraConexionDB();
	          }
	       }
	       log.debug("agregarPrograma(S)");
	       return commit;
	    }

	/**
	 * Funci�n que elimina un programa de la tabla COMCAT_PROGRAMA_FONDOJR
	 * @throws java.lang.Exception
	 * @return regresa un String con el mensaje si se elimino o no el registro.
	 * @param programa  es la clave del programa.
	 */
	public String eliminarPrograma(String programa) {
		log.debug("eliminarPrograma(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean commit = true;
		String cveCat = "";
		String Mensaje = "";
		try {
			con.conexionDB();
			String SQLcve =
						" select /*+ index(IN_COM_PAGOS_FIDE_VAL_04_NUK)*/ IC_PROGRAMA_FONDOJR  from  com_pagos_fide_val " +
						" where IC_PROGRAMA_FONDOJR  in( " + programa + ") " + " union " +
						" select IC_PROGRAMA_FONDOJR  from  com_pagos_fide  " + " where IC_PROGRAMA_FONDOJR  in( " + programa + " )" +
						" union " + " select IC_PROGRAMA_FONDOJR  from  comcat_base_operacion  " + " where IC_PROGRAMA_FONDOJR  in (" +
						programa + ")";
			log.debug("SQLcve ::: " + SQLcve);
			rs = con.queryDB(SQLcve);
			if (rs.next()) {
				cveCat = rs.getString(1);
			}
			rs.close();
			con.cierraStatement();

			if (!cveCat.equals("")) {
				Mensaje = "No se puede Eliminar el Registro";
			} else {
				Mensaje = "El Registro fue Eliminado";
			}

			if (cveCat.equals("")) {
				String SQLUpdateCatalogo =
							  "DELETE  COMCAT_PROGRAMA_FONDOJR  WHERE IC_PROGRAMA_FONDOJR in( " + programa + ")";
				log.debug("SQLUpdateCatalogo ::   " + SQLUpdateCatalogo);
				con.ejecutaSQL(SQLUpdateCatalogo);
				con.terminaTransaccion(true);
			}

		} catch (Exception e) {
			commit = false;
			Mensaje = "No se puede Eliminar el Registro";
			throw new AppException("Error al eliminar el programa " + e);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("eliminarPrograma():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
		log.debug("eliminarPrograma(S)");
		return Mensaje;
	}


	/**
	 * M�todo que realiza la consulta para la pantalla de Validaci�n.
	 * @param parametros_consulta Lista con los valores de los parametros de consulta.
	 * @return operaciones_validacion Lista con los resultados de la consulta.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 013 - 2009 Fondo Junior
	 * @author Alberto Cruz Flores
	 */
	public List obtenerOperacionesValidacionEXTJS(List parametros_consulta) throws NafinException {
		log.info("obtenerOperacionesValidacionextjs(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List operaciones_validacion = new ArrayList();

		Calendar calendario = Calendar.getInstance();
		SimpleDateFormat formato_fecha = new SimpleDateFormat("dd/MM/yyyy");

		try {
			con.conexionDB();

			String fecha_amort_nafin_ini = (String) parametros_consulta.get(0);
			String fecha_amort_nafin_fin = (String) parametros_consulta.get(1);
			String estatus_registro = (String) parametros_consulta.get(2);
			String numero_prestamo = (String) parametros_consulta.get(3);
			String clave_if = (String) parametros_consulta.get(4);
			String numero_sirac = (String) parametros_consulta.get(5);
			String fecha_pago_cliente_fide_ini = (String) parametros_consulta.get(6);
			String fecha_pago_cliente_fide_fin = (String) parametros_consulta.get(7);
			String fecha_registro_fide_ini = (String) parametros_consulta.get(8);
			String fecha_registro_fide_fin = (String) parametros_consulta.get(9);
			String cs_validacion = (String) parametros_consulta.get(10);
			String clavePrograma = (String) parametros_consulta.get(11); //FODEA 026 - 2010 ACF
			String folioValidacion = (String) parametros_consulta.get(12); //----------- ACF
			String fecha_amortizacion = "";

			if (fecha_amort_nafin_ini.equals("")) {
				strSQL = new StringBuffer();
				strSQL.append(" SELECT TO_CHAR(MIN(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
				strSQL.append(" FROM com_pagos_fide_val cpf");
				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();

				while (rst.next()) {
					fecha_amort_nafin_ini =
									 rst.getString("fecha_amort_nafin") == null ? "" : rst.getString("fecha_amort_nafin");
				}

				rst.close();
				pst.close();
			}

			if (fecha_amort_nafin_fin.equals("")) {
				strSQL = new StringBuffer();
				strSQL.append(" SELECT TO_CHAR(MAX(cpf.com_fechaprobablepago), 'dd/mm/yyyy') fecha_amort_nafin");
				strSQL.append(" FROM com_pagos_fide_val cpf");
				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();

				while (rst.next()) {
					fecha_amort_nafin_fin =
									 rst.getString("fecha_amort_nafin") == null ? "" : rst.getString("fecha_amort_nafin");
				}

				rst.close();
				pst.close();
			}

			fecha_amortizacion = fecha_amort_nafin_ini;

			List operaciones_reembolsos = new ArrayList();
			List operaciones_prepagos = new ArrayList();
			List operaciones_recuperaciones = new ArrayList();
			while (Comunes.esComparacionFechaValida(fecha_amortizacion, "le", fecha_amort_nafin_fin, "dd/MM/yyyy")) {
				List operaciones_por_fecha = new ArrayList();
				List operaciones_pagados = new ArrayList();
				List operaciones_no_pagados = new ArrayList();


				String dia = fecha_amortizacion.substring(0, fecha_amortizacion.indexOf("/"));
				String mes =
							  fecha_amortizacion.substring(fecha_amortizacion.indexOf("/") + 1,
																	 fecha_amortizacion.lastIndexOf("/"));
				String anio = fecha_amortizacion.substring(fecha_amortizacion.lastIndexOf("/") + 1);

				calendario.set(Integer.parseInt(anio), Integer.parseInt(mes) - 1, Integer.parseInt(dia));
				fecha_amortizacion = formato_fecha.format(calendario.getTime());

				if (!estatus_registro.equals("")) {
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add(estatus_registro);
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)

					//log.debug("..:: strSQL : "+strSQL.toString());
					//log.debug("..:: varBind : "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						HashMap hmOperPorFecha = new HashMap();
						hmOperPorFecha.put("fecha_amort_nafin",
												 rst.getString("fecha_amort_nafin") == null ? "" :
												 rst.getString("fecha_amort_nafin"));
						hmOperPorFecha.put("fecha_operacion",
												 rst.getString("fecha_operacion") == null ? "" : rst.getString("fecha_operacion"));
						hmOperPorFecha.put("fecha_pag_clfide",
												 rst.getString("fecha_pag_clfide") == null ? "" :
												 rst.getString("fecha_pag_clfide"));
						hmOperPorFecha.put("numero_prestamo",
												 rst.getString("numero_prestamo") == null ? "" : rst.getString("numero_prestamo"));
						hmOperPorFecha.put("numero_sirac",
												 rst.getString("numero_sirac") == null ? "" : rst.getString("numero_sirac"));
						hmOperPorFecha.put("nombre_cliente",
												 rst.getString("nombre_cliente") == null ? "" : rst.getString("nombre_cliente"));
						hmOperPorFecha.put("numero_cuota",
												 rst.getString("numero_cuota") == null ? "" : rst.getString("numero_cuota"));
						hmOperPorFecha.put("monto_capital",
												 rst.getString("monto_capital") == null ? "" : rst.getString("monto_capital"));
						hmOperPorFecha.put("monto_interes",
												 rst.getString("monto_interes") == null ? "" : rst.getString("monto_interes"));
						hmOperPorFecha.put("monto_total",
												 rst.getString("monto_total") == null ? "" : rst.getString("monto_total"));
						hmOperPorFecha.put("estatus_prestamo",
												 rst.getString("estatus_prestamo") == null ? "" :
												 rst.getString("estatus_prestamo"));
						hmOperPorFecha.put("origen_prestamo",
												 rst.getString("origen_prestamo") == null ? "" : rst.getString("origen_prestamo"));
						hmOperPorFecha.put("estatus_validacion",
												 rst.getString("estatus_validacion") == null ? "" :
												 rst.getString("estatus_validacion"));

						if (estatus_registro.equals("P")) {
							operaciones_pagados.add(hmOperPorFecha);
						} else if (estatus_registro.equals("NP")) {
							operaciones_no_pagados.add(hmOperPorFecha);
						} else if (estatus_registro.equals("R")) {
							operaciones_reembolsos.add(hmOperPorFecha);
						} else if (estatus_registro.equals("T")) {
							operaciones_prepagos.add(hmOperPorFecha);
						} else if (estatus_registro.equals("RF")) {
							operaciones_recuperaciones.add(hmOperPorFecha);
						}
					}
					pst.close();
					rst.close();

					operaciones_por_fecha.add(fecha_amortizacion);
					operaciones_por_fecha.add(operaciones_pagados);
					operaciones_por_fecha.add(operaciones_no_pagados);
					//operaciones_por_fecha.add(operaciones_reembolsos);
					//operaciones_por_fecha.add(operaciones_prepagos);
					//operaciones_por_fecha.add(operaciones_recuperaciones);
					operaciones_validacion.add(operaciones_por_fecha);
				} else {
					/*REGISTROS PAGADOS*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("P");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					//log.debug("..:: strSQL : "+strSQL.toString());
					//log.debug("..:: varBind : "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));

						HashMap hmOperPorFecha = new HashMap();
						hmOperPorFecha.put("fecha_amort_nafin",
												 rst.getString("fecha_amort_nafin") == null ? "" :
												 rst.getString("fecha_amort_nafin"));
						hmOperPorFecha.put("fecha_operacion",
												 rst.getString("fecha_operacion") == null ? "" : rst.getString("fecha_operacion"));
						hmOperPorFecha.put("fecha_pag_clfide",
												 rst.getString("fecha_pag_clfide") == null ? "" :
												 rst.getString("fecha_pag_clfide"));
						hmOperPorFecha.put("numero_prestamo",
												 rst.getString("numero_prestamo") == null ? "" : rst.getString("numero_prestamo"));
						hmOperPorFecha.put("numero_sirac",
												 rst.getString("numero_sirac") == null ? "" : rst.getString("numero_sirac"));
						hmOperPorFecha.put("nombre_cliente",
												 rst.getString("nombre_cliente") == null ? "" : rst.getString("nombre_cliente"));
						hmOperPorFecha.put("numero_cuota",
												 rst.getString("numero_cuota") == null ? "" : rst.getString("numero_cuota"));
						hmOperPorFecha.put("monto_capital",
												 rst.getString("monto_capital") == null ? "" : rst.getString("monto_capital"));
						hmOperPorFecha.put("monto_interes",
												 rst.getString("monto_interes") == null ? "" : rst.getString("monto_interes"));
						hmOperPorFecha.put("monto_total",
												 rst.getString("monto_total") == null ? "" : rst.getString("monto_total"));
						hmOperPorFecha.put("estatus_prestamo",
												 rst.getString("estatus_prestamo") == null ? "" :
												 rst.getString("estatus_prestamo"));
						hmOperPorFecha.put("origen_prestamo",
												 rst.getString("origen_prestamo") == null ? "" : rst.getString("origen_prestamo"));
						hmOperPorFecha.put("estatus_validacion",
												 rst.getString("estatus_validacion") == null ? "" :
												 rst.getString("estatus_validacion"));

						operaciones_pagados.add(hmOperPorFecha);
					}

					pst.close();
					rst.close();

					/*REGISTROS NO PAGADOS*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("NP");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					//log.debug("..:: strSQL : "+strSQL.toString());
					//log.debug("..:: varBind : "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));

						HashMap hmOperPorFecha = new HashMap();
						hmOperPorFecha.put("fecha_amort_nafin",
												 rst.getString("fecha_amort_nafin") == null ? "" :
												 rst.getString("fecha_amort_nafin"));
						hmOperPorFecha.put("fecha_operacion",
												 rst.getString("fecha_operacion") == null ? "" : rst.getString("fecha_operacion"));
						hmOperPorFecha.put("fecha_pag_clfide",
												 rst.getString("fecha_pag_clfide") == null ? "" :
												 rst.getString("fecha_pag_clfide"));
						hmOperPorFecha.put("numero_prestamo",
												 rst.getString("numero_prestamo") == null ? "" : rst.getString("numero_prestamo"));
						hmOperPorFecha.put("numero_sirac",
												 rst.getString("numero_sirac") == null ? "" : rst.getString("numero_sirac"));
						hmOperPorFecha.put("nombre_cliente",
												 rst.getString("nombre_cliente") == null ? "" : rst.getString("nombre_cliente"));
						hmOperPorFecha.put("numero_cuota",
												 rst.getString("numero_cuota") == null ? "" : rst.getString("numero_cuota"));
						hmOperPorFecha.put("monto_capital",
												 rst.getString("monto_capital") == null ? "" : rst.getString("monto_capital"));
						hmOperPorFecha.put("monto_interes",
												 rst.getString("monto_interes") == null ? "" : rst.getString("monto_interes"));
						hmOperPorFecha.put("monto_total",
												 rst.getString("monto_total") == null ? "" : rst.getString("monto_total"));
						hmOperPorFecha.put("estatus_prestamo",
												 rst.getString("estatus_prestamo") == null ? "" :
												 rst.getString("estatus_prestamo"));
						hmOperPorFecha.put("origen_prestamo",
												 rst.getString("origen_prestamo") == null ? "" : rst.getString("origen_prestamo"));
						hmOperPorFecha.put("estatus_validacion",
												 rst.getString("estatus_validacion") == null ? "" :
												 rst.getString("estatus_validacion"));

						operaciones_no_pagados.add(hmOperPorFecha);
					}

					pst.close();
					rst.close();

					/*REEMBOLSOS*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("R");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					//log.debug("..:: strSQL : "+strSQL.toString());
					//log.debug("..:: varBind : "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));

						HashMap hmOperPorFecha = new HashMap();
						hmOperPorFecha.put("fecha_amort_nafin",
												 rst.getString("fecha_amort_nafin") == null ? "" :
												 rst.getString("fecha_amort_nafin"));
						hmOperPorFecha.put("fecha_operacion",
												 rst.getString("fecha_operacion") == null ? "" : rst.getString("fecha_operacion"));
						hmOperPorFecha.put("fecha_pag_clfide",
												 rst.getString("fecha_pag_clfide") == null ? "" :
												 rst.getString("fecha_pag_clfide"));
						hmOperPorFecha.put("numero_prestamo",
												 rst.getString("numero_prestamo") == null ? "" : rst.getString("numero_prestamo"));
						hmOperPorFecha.put("numero_sirac",
												 rst.getString("numero_sirac") == null ? "" : rst.getString("numero_sirac"));
						hmOperPorFecha.put("nombre_cliente",
												 rst.getString("nombre_cliente") == null ? "" : rst.getString("nombre_cliente"));
						hmOperPorFecha.put("numero_cuota",
												 rst.getString("numero_cuota") == null ? "" : rst.getString("numero_cuota"));
						hmOperPorFecha.put("monto_capital",
												 rst.getString("monto_capital") == null ? "" : rst.getString("monto_capital"));
						hmOperPorFecha.put("monto_interes",
												 rst.getString("monto_interes") == null ? "" : rst.getString("monto_interes"));
						hmOperPorFecha.put("monto_total",
												 rst.getString("monto_total") == null ? "" : rst.getString("monto_total"));
						hmOperPorFecha.put("estatus_prestamo",
												 rst.getString("estatus_prestamo") == null ? "" :
												 rst.getString("estatus_prestamo"));
						hmOperPorFecha.put("origen_prestamo",
												 rst.getString("origen_prestamo") == null ? "" : rst.getString("origen_prestamo"));
						hmOperPorFecha.put("estatus_validacion",
												 rst.getString("estatus_validacion") == null ? "" :
												 rst.getString("estatus_validacion"));
						operaciones_reembolsos.add(hmOperPorFecha);
					}

					pst.close();
					rst.close();

					/*PREPAGOS TOTALES*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ DISTINCT TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("T");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					//log.debug("..:: strSQL : "+strSQL.toString());
					//log.debug("..:: varBind : "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));

						HashMap hmOperPorFecha = new HashMap();
						hmOperPorFecha.put("fecha_amort_nafin",
												 rst.getString("fecha_amort_nafin") == null ? "" :
												 rst.getString("fecha_amort_nafin"));
						hmOperPorFecha.put("fecha_operacion",
												 rst.getString("fecha_operacion") == null ? "" : rst.getString("fecha_operacion"));
						hmOperPorFecha.put("fecha_pag_clfide",
												 rst.getString("fecha_pag_clfide") == null ? "" :
												 rst.getString("fecha_pag_clfide"));
						hmOperPorFecha.put("numero_prestamo",
												 rst.getString("numero_prestamo") == null ? "" : rst.getString("numero_prestamo"));
						hmOperPorFecha.put("numero_sirac",
												 rst.getString("numero_sirac") == null ? "" : rst.getString("numero_sirac"));
						hmOperPorFecha.put("nombre_cliente",
												 rst.getString("nombre_cliente") == null ? "" : rst.getString("nombre_cliente"));
						hmOperPorFecha.put("numero_cuota",
												 rst.getString("numero_cuota") == null ? "" : rst.getString("numero_cuota"));
						hmOperPorFecha.put("monto_capital",
												 rst.getString("monto_capital") == null ? "" : rst.getString("monto_capital"));
						hmOperPorFecha.put("monto_interes",
												 rst.getString("monto_interes") == null ? "" : rst.getString("monto_interes"));
						hmOperPorFecha.put("monto_total",
												 rst.getString("monto_total") == null ? "" : rst.getString("monto_total"));
						hmOperPorFecha.put("estatus_prestamo",
												 rst.getString("estatus_prestamo") == null ? "" :
												 rst.getString("estatus_prestamo"));
						hmOperPorFecha.put("origen_prestamo",
												 rst.getString("origen_prestamo") == null ? "" : rst.getString("origen_prestamo"));
						hmOperPorFecha.put("estatus_validacion",
												 rst.getString("estatus_validacion") == null ? "" :
												 rst.getString("estatus_validacion"));

						operaciones_prepagos.add(hmOperPorFecha);
					}

					pst.close();
					rst.close();

					/*RECUPERACION DE FONDO JR*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT /*+ use_nl(cpf prg) index(cpf cp_com_pagos_fide_val_pk) ordered*/ TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_amort_nafin,");
					strSQL.append(" TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_operacion,");
					strSQL.append(" TO_CHAR(cpf.df_pago_cliente, 'dd/mm/yyyy') fecha_pag_clfide,");
					strSQL.append(" cpf.ig_prestamo numero_prestamo,");
					strSQL.append(" cpf.ig_cliente numero_sirac,");
					strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
					strSQL.append(" cpf.ig_disposicion numero_cuota,");
					strSQL.append(" cpf.fg_amortizacion monto_capital,");
					strSQL.append(" cpf.fg_interes monto_interes,");
					strSQL.append(" cpf.fg_totalvencimiento monto_total,");
					strSQL.append(" DECODE(cpf.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'T', 'Prepago Total', 'RC', 'Recuperaci�n de Fondo Jr') estatus_prestamo,");
					strSQL.append(" prg.cg_descripcion origen_prestamo,");
					strSQL.append(" DECODE(cpf.cs_validacion, 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') estatus_validacion");
					strSQL.append(" FROM com_pagos_fide_val cpf");
					strSQL.append(", comcat_origen_programa prg");
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(", comcat_pyme pyme");
						strSQL.append(", comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
					}
					strSQL.append(" WHERE cpf.ig_origen = prg.ig_origen_programa");
					if (!cs_validacion.equals("ACUSE")) {
						strSQL.append(" AND cpf.cs_validacion = ?"); //..:: estatus de la validaci�n
					} else {
						strSQL.append(" AND cpf.ig_folio_val = ?");
					}
					//strSQL.append(" AND cpf.com_fechaprobablepago = TO_DATE(?, 'dd/mm/yyyy')");//..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago >= TO_DATE(?, 'dd/mm/yyyy')"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.com_fechaprobablepago < TO_DATE(?, 'dd/mm/yyyy') + 1"); //..:: fecha de amortizacion nafin
					strSQL.append(" AND cpf.cg_estatus = ?"); //..:: estatus del registro
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						strSQL.append(" AND cpf.ig_prestamo = ?");
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						strSQL.append(" AND cpf.ig_cliente = pyme.in_numero_sirac");
						strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
						strSQL.append(" AND ccb.ic_cuenta_bancaria = cpi.ic_cuenta_bancaria");
						strSQL.append(" AND pyme.cs_habilitado = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.ic_if = ?");
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						strSQL.append(" AND cpf.ig_cliente = ?");
					}
					if (!fecha_pago_cliente_fide_ini.equals("") &&
						 !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 strSQL.append(" AND cpf.df_pago_cliente BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}
					if (!fecha_registro_fide_ini.equals("") &&
						 !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 strSQL.append(" AND cpf.df_registro_fide BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
					}

					if (!cs_validacion.equals("ACUSE")) { //..:: estatus de la validaci�n
						varBind.add("P");
					} else {
						varBind.add(new Long(folioValidacion));
					}
					varBind.add(fecha_amortizacion);
					varBind.add(fecha_amortizacion);
					varBind.add("RC");
					if (!numero_prestamo.equals("")) { //..:: numero de prestamo
						varBind.add(new Long(numero_prestamo));
					}
					if (!clave_if.equals("")) { //..:: intermediario financiero
						varBind.add("S");
						varBind.add("S");
						varBind.add(new Integer(1));
						varBind.add(new Integer(clave_if));
					}
					if (!numero_sirac.equals("")) { //..:: numero sirac cliente
						varBind.add(new Long(numero_sirac));
					}
					if (!fecha_pago_cliente_fide_ini.equals("") && !fecha_pago_cliente_fide_fin.equals("")) { //..:: fecha pago cliente fide
									 varBind.add(fecha_pago_cliente_fide_ini);
						varBind.add(fecha_pago_cliente_fide_fin);
					}
					if (!fecha_registro_fide_ini.equals("") && !fecha_registro_fide_fin.equals("")) { //..:: fecha a consultar
									 varBind.add(fecha_registro_fide_ini);
						varBind.add(fecha_registro_fide_fin);
					}
					//FODEA 026 - 2010 ACF (I)
					if (!clavePrograma.equals("")) {
						strSQL.append(" AND cpf.ic_programa_fondojr = ?");
						varBind.add(new Integer(clavePrograma));
					}
					//FODEA 026 - 2010 ACF (F)
					//log.debug("..:: strSQL : "+strSQL.toString());
					//log.debug("..:: varBind : "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					rst = pst.executeQuery();

					while (rst.next()) {
						List operacion_por_fecha = new ArrayList();
						operacion_por_fecha.add(rst.getString(1) == null ? "" : rst.getString(1));
						operacion_por_fecha.add(rst.getString(2) == null ? "" : rst.getString(2));
						operacion_por_fecha.add(rst.getString(3) == null ? "" : rst.getString(3));
						operacion_por_fecha.add(rst.getString(4) == null ? "" : rst.getString(4));
						operacion_por_fecha.add(rst.getString(5) == null ? "" : rst.getString(5));
						operacion_por_fecha.add(rst.getString(6) == null ? "" : rst.getString(6));
						operacion_por_fecha.add(rst.getString(7) == null ? "" : rst.getString(7));
						operacion_por_fecha.add(rst.getString(8) == null ? "" : rst.getString(8));
						operacion_por_fecha.add(rst.getString(9) == null ? "" : rst.getString(9));
						operacion_por_fecha.add(rst.getString(10) == null ? "" : rst.getString(10));
						operacion_por_fecha.add(rst.getString(11) == null ? "" : rst.getString(11));
						operacion_por_fecha.add(rst.getString(12) == null ? "" : rst.getString(12));
						operacion_por_fecha.add(rst.getString("estatus_validacion") == null ? "" :
														rst.getString("estatus_validacion"));

						HashMap hmOperPorFecha = new HashMap();
						hmOperPorFecha.put("fecha_amort_nafin",
												 rst.getString("fecha_amort_nafin") == null ? "" :
												 rst.getString("fecha_amort_nafin"));
						hmOperPorFecha.put("fecha_operacion",
												 rst.getString("fecha_operacion") == null ? "" : rst.getString("fecha_operacion"));
						hmOperPorFecha.put("fecha_pag_clfide",
												 rst.getString("fecha_pag_clfide") == null ? "" :
												 rst.getString("fecha_pag_clfide"));
						hmOperPorFecha.put("numero_prestamo",
												 rst.getString("numero_prestamo") == null ? "" : rst.getString("numero_prestamo"));
						hmOperPorFecha.put("numero_sirac",
												 rst.getString("numero_sirac") == null ? "" : rst.getString("numero_sirac"));
						hmOperPorFecha.put("nombre_cliente",
												 rst.getString("nombre_cliente") == null ? "" : rst.getString("nombre_cliente"));
						hmOperPorFecha.put("numero_cuota",
												 rst.getString("numero_cuota") == null ? "" : rst.getString("numero_cuota"));
						hmOperPorFecha.put("monto_capital",
												 rst.getString("monto_capital") == null ? "" : rst.getString("monto_capital"));
						hmOperPorFecha.put("monto_interes",
												 rst.getString("monto_interes") == null ? "" : rst.getString("monto_interes"));
						hmOperPorFecha.put("monto_total",
												 rst.getString("monto_total") == null ? "" : rst.getString("monto_total"));
						hmOperPorFecha.put("estatus_prestamo",
												 rst.getString("estatus_prestamo") == null ? "" :
												 rst.getString("estatus_prestamo"));
						hmOperPorFecha.put("origen_prestamo",
												 rst.getString("origen_prestamo") == null ? "" : rst.getString("origen_prestamo"));
						hmOperPorFecha.put("estatus_validacion",
												 rst.getString("estatus_validacion") == null ? "" :
												 rst.getString("estatus_validacion"));
						operaciones_recuperaciones.add(hmOperPorFecha);
					}

					pst.close();
					rst.close();

					operaciones_por_fecha.add(fecha_amortizacion);
					operaciones_por_fecha.add(operaciones_pagados);
					operaciones_por_fecha.add(operaciones_no_pagados);
					//operaciones_por_fecha.add(operaciones_reembolsos);
					//operaciones_por_fecha.add(operaciones_prepagos);
					//operaciones_por_fecha.add(operaciones_recuperaciones);
					operaciones_validacion.add(operaciones_por_fecha);
				}

				calendario.add(Calendar.DATE, 1);
				fecha_amortizacion = formato_fecha.format(calendario.getTime());
			}

			operaciones_validacion.add(0, operaciones_reembolsos);
			operaciones_validacion.add(1, operaciones_prepagos);
			operaciones_validacion.add(2, operaciones_recuperaciones);
		} catch (Exception e) {
			log.info("obtenerOperacionesValidacionextjs(ERROR) ::..");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerOperacionesValidacionextjs(S) ::..");
		}
		return operaciones_validacion;
	}

/**
	 * Obtiene amortizaciones No Pagadas del Prestamo que se le indique
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param cvePrograma
	 * @param numero_prestamo
	 */
	public List obtenerNPaReembolsarXprestamo(String numero_prestamo, String cvePrograma) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List regNpAreembolsar = new ArrayList();
		int hayRegistros = 0; //variable para saber si existen registros con otro estatus que no sea 'NP'
		System.out.println("..:: FondoJuniorBean : obtenerNPaReembolsarXprestamo(E) ::..");
		try{
			con.conexionDB();
			
			strSQL = new StringBuffer();
			
			strSQL.append(" SELECT TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_movimiento,");
			strSQL.append(" TO_CHAR(cpf.com_fechaprobablepago, 'dd/mm/yyyy') fecha_probablepago,");
			strSQL.append(" cpf.ig_prestamo prestamo,");
			strSQL.append(" cpf.ig_disposicion disposicion,");
			strSQL.append(" cpf.ig_cliente num_cliente_sirac,");
			strSQL.append(" cpf.cg_nombrecliente nombre_cliente,");
			strSQL.append(" cpf.fg_totalvencimiento monto_amortizacion,");
			strSQL.append(" cop.cg_descripcion origen  ");
			strSQL.append(" FROM com_pagos_fide_val cpf, comcat_origen_programa cop  ");
			strSQL.append(" WHERE cpf.ig_origen = cop.ig_origen_programa  ");
			strSQL.append(" AND cpf.ig_prestamo  = ?  ");
			strSQL.append(" AND cpf.ic_programa_fondojr = ? ");
			strSQL.append(" and cpf.cg_estatus in('NP')  ");
			strSQL.append(" AND cpf.cs_validacion = 'A' ");
			strSQL.append(" and  ig_disposicion not in (  SELECT ig_disposicion  ");
			strSQL.append(" FROM com_pagos_fide_val cpf, comcat_origen_programa cop  ");
			strSQL.append(" WHERE cpf.ig_origen = cop.ig_origen_programa  ");
			strSQL.append(" AND cpf.ig_prestamo  = ? ");
			strSQL.append(" AND cpf.ic_programa_fondojr = ?  ");
			strSQL.append(" and  cg_estatus in('R','P','RF','T') ) ");
			
			
			pst = con.queryPrecompilado(strSQL.toString());
			pst.setLong(1, Long.parseLong(numero_prestamo));
			pst.setInt(2, Integer.parseInt(cvePrograma));
			pst.setLong(3, Long.parseLong(numero_prestamo));
			pst.setInt(4, Integer.parseInt(cvePrograma));
			
			rst = pst.executeQuery();
			
			HashMap hmReg = new HashMap();
			while(rst.next()){
				hmReg = new HashMap();
				hmReg.put("FECHA_MOVIMIENTO",rst.getString("fecha_movimiento")==null?"":rst.getString("fecha_movimiento"));
				hmReg.put("FECHA_PROBABLEPAGO",rst.getString("fecha_probablepago")==null?"":rst.getString("fecha_probablepago"));
				hmReg.put("PRESTAMO",rst.getString("prestamo")==null?"":rst.getString("prestamo"));
				hmReg.put("AMORTIZACION",rst.getString("disposicion")==null?"":rst.getString("disposicion"));
				hmReg.put("NUM_CLIENTE_SIRAC",rst.getString("num_cliente_sirac")==null?"":rst.getString("num_cliente_sirac"));
				hmReg.put("NOMBRE_CLIENTE",rst.getString("nombre_cliente")==null?"":rst.getString("nombre_cliente"));
				hmReg.put("MONTO_AMORTIZACION",rst.getString("monto_amortizacion")==null?"":rst.getString("monto_amortizacion"));
				hmReg.put("ORIGEN",rst.getString("origen")==null?"":rst.getString("origen"));
				regNpAreembolsar.add(hmReg);
				
			}
			rst.close();
			pst.close();
			
			return regNpAreembolsar;
		
		}catch(Exception e){
			log.info("..:: FondoJuniorBean : obtenerDatosPrestamo(ERROR) :");
			e.printStackTrace();
			throw new AppException("Error al consultar amoritzaciones NP por prestamo", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("..:: FondoJuniorBean : obtenerDatosPrestamo(S) ::..");
		}
   }
	
	/**
	 *
	 * @param lstRegistros
	 * @param cvePrograma
	 * @throws AppException
	 */
	public void generarReembolsos(List lstRegistros, String cvePrograma) throws AppException{
		log.info("..:: FondoJuniorBean : generarReembolsos(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		boolean commit = true;
		try{
			con.conexionDB();
			
			strSQL = new StringBuffer();
			
			strSQL.append("INSERT INTO com_pagos_fide_val ");
			strSQL.append("            (com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion, ");
			strSQL.append("             fg_amortizacion, fg_interes, fg_totalvencimiento, cg_estatus, ");
			strSQL.append("             ig_origen, df_periodofin, df_registro_fide, df_fecha_carga, ");
			strSQL.append("             cg_usuario, ig_folio, cs_validacion, df_validacion, ");
			strSQL.append("             cg_nombrecliente, ic_programa_fondojr, ig_folio_val, ");
			strSQL.append("             cg_observaciones) ");
			strSQL.append("   (SELECT com_fechaprobablepago, ig_prestamo, ig_cliente, ig_disposicion, ");
			strSQL.append("           fg_amortizacion, fg_interes, fg_totalvencimiento, 'R', ig_origen, ");
			strSQL.append("           df_periodofin, df_registro_fide, SYSDATE, NULL, ig_folio, 'A', ");
			strSQL.append("           df_validacion, cg_nombrecliente, ic_programa_fondojr, 0, ");
			strSQL.append("           'Inserta Reembolso Fondo Jr' ");
			strSQL.append("      FROM com_pagos_fide_val ");
			strSQL.append("     WHERE com_fechaprobablepago = TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("       AND ig_prestamo = ? ");
			strSQL.append("       AND ig_disposicion = ? ");
			strSQL.append("       AND fg_totalvencimiento = ? ");
			strSQL.append("       AND cg_estatus = ? ");
			strSQL.append("       AND ic_programa_fondojr = ? ) ");
			
			HashMap hmRegistro = new HashMap();
			for(int i=0; i<lstRegistros.size();i++){
				hmRegistro = (HashMap)lstRegistros.get(i);
				String fecha = hmRegistro.get("FECHA_PROBABLEPAGO")==null?"":(String)hmRegistro.get("FECHA_PROBABLEPAGO");
				String prestamo = hmRegistro.get("PRESTAMO")==null?"":(String)hmRegistro.get("PRESTAMO");
				String disposicion = hmRegistro.get("AMORTIZACION")==null?"":(String)hmRegistro.get("AMORTIZACION");
				String monto = hmRegistro.get("MONTO_AMORTIZACION")==null?"":(String)hmRegistro.get("MONTO_AMORTIZACION");
				
				
				ps = con.queryPrecompilado(strSQL.toString());
				ps.setString(1, fecha);
				ps.setLong(2, Long.parseLong(prestamo));
				ps.setInt(3, Integer.parseInt(disposicion));
				ps.setDouble(4, Double.parseDouble(monto));
				ps.setString(5, "NP");
				ps.setInt(6,Integer.parseInt(cvePrograma));
				
				ps.executeUpdate();
			}
		
		}catch(Exception e){
			log.info("..:: FondoJuniorBean : generarReembolsos(ERROR) :");
			commit = false;
			e.printStackTrace();
			throw new AppException("Error al generar los Reembolsos ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("..:: FondoJuniorBean : generarReembolsos(S) ::..");
		}
   }


}
