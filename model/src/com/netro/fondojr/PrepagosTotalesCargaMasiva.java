package com.netro.fondojr;

import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.sql.*;
import java.math.BigDecimal;
import netropology.utilerias.*;
import com.netro.exception.*;

public class PrepagosTotalesCargaMasiva  implements Serializable{

	private String numeroProceso;
	private String numeroPrestamo;
	private String montoTotalReembolso;
	

	/**
	 * Lee el archivo de carga de documentos y almacena la informaci�n contenida en una lista.
	 * @param inputStream Stream con los datos del archivo origen.
	 * @return List con los datos que contiene el archivo
	 */
	/*
	public HashMap leeArchivoPrepagosTotales(InputStream inputStream, FondoJrPrepagoTotalBean fondoJrPrepagoTotalBean) throws NafinException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstPrepagosTotales = new ArrayList();
		HashMap validacion_prepagos = new HashMap();
		String linea = "";    
		BigDecimal total_prepagos = new BigDecimal("0.00");
		BigDecimal total_prepagos_erroneos = new BigDecimal("0.00");
		
		
		String fec_real_pago		= "";
		String num_prestamo		= "";
		String codigo_cliente	= "";
		String disposicion		= "";
		String fn_amortizacion	= "";
		String fn_interes			= "";
		String fn_total_venc		= "";
		String fec_pago_cliente = "";
		String estatus_registro = "";
		String origen 				= "";
		String clavePrograma		= "";
		
		String query = "";
		String existePrepago = "";
		
		int i=0;
		
		StringBuffer errores = new StringBuffer();
		StringBuffer correctos = new StringBuffer();
		StringBuffer registro_erroneo = new StringBuffer();
		StringBuffer erroresCifrasControl = new StringBuffer();
		ArrayList registro_correcto = new ArrayList();
		ArrayList validacion_registro = new ArrayList();
		ArrayList resultado_validacion = new ArrayList();
		ArrayList lstNumeroPrestamo = new ArrayList();
		int num_reg_error = 0;
		
		boolean errorMontoMaxMn = false;
		boolean errorMontoMinMn = false;


		System.out.println("..:: PrepagosTotalesCargaMasiva : leeArchivoPrepagosTotales(I) ::..");

		try{
			con.conexionDB();
			BufferedReader breader = new BufferedReader(new InputStreamReader(inputStream));
			
			double montoMaxMn = Double.parseDouble(fondoJrPrepagoTotalBean.getHidCtrlMontoMaxMN());
			double montoMinMn = Double.parseDouble(fondoJrPrepagoTotalBean.getHidCtrlMontoMinMN());
			double montoTotalMn = Double.parseDouble(fondoJrPrepagoTotalBean.getHidCtrlMontoTotalMN());
			int totalDoctosMn = Integer.parseInt(fondoJrPrepagoTotalBean.getHidCtrlNumDoctosMN());
			
			
			query = "SELECT /*+ index(cpv IN_COM_PAGOS_FIDE_VAL_01_NUK)*/ //COUNT(1) SINREMBOLSO " +
	/*
						"  FROM com_pagos_fide_val cpv " +
						" WHERE cg_estatus = ? " +
						"   AND ig_prestamo = ? " +
						"   AND NOT EXISTS ( " +
						"          SELECT 1 " +
						"            FROM com_pagos_fide_val cpv2 " +
						"           WHERE cpv2.ig_prestamo = ? " +
						"             AND cpv.ig_prestamo = cpv2.ig_prestamo " +
						"             AND cpv.ig_cliente = cpv2.ig_cliente " +
						"             AND cpv.fg_totalvencimiento = cpv2.fg_totalvencimiento " +
						"             AND cpv.ig_disposicion = cpv2.ig_disposicion " +
						"             AND cpv2.cg_estatus = ?) ";
			
			
			while((linea = breader.readLine()) != null){
				StringTokenizer tokens = new StringTokenizer(linea, "|");
			
				validacion_registro = new ArrayList();
				registro_correcto = new ArrayList();
				errores = new StringBuffer();
				correctos = new StringBuffer();
				registro_erroneo = new StringBuffer();
				if(tokens.countTokens()==11){
					//LAYOUT PREPAGOS TOTALES

					fec_real_pago		 = tokens.nextToken("|");
					num_prestamo		 = tokens.nextToken("|");
					codigo_cliente		 = tokens.nextToken("|");
					disposicion			 = tokens.nextToken("|");
					fn_amortizacion	 = tokens.nextToken("|");
					fn_interes			 = tokens.nextToken("|");
					fn_total_venc		 = tokens.nextToken("|");
					fec_pago_cliente	 = tokens.nextToken("|");
					estatus_registro	 = tokens.nextToken("|");
					origen				 = tokens.nextToken("|");
					clavePrograma		 = tokens.nextToken("|");
					
					//SE REALIZA VALIDACION DE CAMPOS
					
					if(!Comunes.esNumero(num_prestamo) || num_prestamo.length() > 12){
						errores.append("L�nea " + (i + 1) + ": El n�mero de prestamo debe ser un entero no mayor a 12 d�gitos.\n");
					}else{
						//validacion - verifica si el preprago se repite en el archivo de carga
						if(lstNumeroPrestamo!=null && lstNumeroPrestamo.size()>0){
							for(int z = 0; z<lstNumeroPrestamo.size();z++){
								if(num_prestamo.equals((String)lstNumeroPrestamo.get(z))){
									errores.append("L�nea " + (i + 1) + ": N�mero de prestamo repetido en el archivo de carga\n");
									break;
								}
							}
						}
						
						//valida que el prepago no exista en la tabla de com_pagos_fide
						existePrepago = "select count(1) from com_pagos_fide where ig_prestamo = ? and cg_estatus = ? ";
						ps = con.queryPrecompilado(existePrepago);
						ps.setLong(1,Long.parseLong(num_prestamo));
						ps.setString(2,"T");
						
						rs = ps.executeQuery();
						if(rs!=null && rs.next()){
							if(!"0".equals(rs.getString(1)))
								errores.append("L�nea " + (i + 1) + ": No se puede aceptar el prepago debido a que ya existe\n");
						}
						if(rs!=null)rs.close();
						if(ps!=null)ps.close();
						
						//valida que el prepago no exista en la tabla de com_pagos_fide_val
						existePrepago = "select count(1) from com_pagos_fide_val where ig_prestamo = ? and cg_estatus = ? ";
						ps = con.queryPrecompilado(existePrepago);
						ps.setLong(1,Long.parseLong(num_prestamo));
						ps.setString(2,"T");
						
						rs = ps.executeQuery();
						if(rs!=null && rs.next()){
							if(!"0".equals(rs.getString(1)))
								errores.append("L�nea " + (i + 1) + ": No se puede aceptar el prepago debido a que ya existe\n");
						}
						if(rs!=null)rs.close();
						if(ps!=null)ps.close();
						
						//validacion de np y reembolsos
						ps = con.queryPrecompilado(query);
						ps.setString(1,"NP");
						ps.setLong(2,Long.parseLong(num_prestamo));
						ps.setLong(3,Long.parseLong(num_prestamo));
						ps.setString(4,"R");
						
						rs = ps.executeQuery();
						
						if(rs!=null && rs.next()){
							String contRegSinReembolso = rs.getString("SINREMBOLSO");
							if(!"0".equals(contRegSinReembolso))
								errores.append("L�nea " + (i + 1) + ": No se puede aceptar el prepago ya que existen amortizaciones no pagadas sin reembolso.\n");
						}
						if(rs!=null)rs.close();
						if(ps!=null)ps.close();
					}
					
					if(!Comunes.esNumero(codigo_cliente) || codigo_cliente.length() > 12){
						errores.append("L�nea " + (i + 1) + ": El c�digo de cliente debe ser un entero no mayor a 12 d�gitos.\n");
					}
					
					if(!Comunes.esNumero(disposicion) || disposicion.length() > 12){
						errores.append("L�nea " + (i + 1) + ": El n�mero de disposici�n debe ser un entero no mayor a 5 d�gitos.\n");
					}
					
					if(!Comunes.esDecimal(fn_amortizacion)  || fn_amortizacion.length() > 20){
						errores.append("L�nea " + (i + 1) + ": El monto de la amortizaci�n debe ser un n�mero de m�ximo 17 d�gitos y 2 decimales.\n");
					}
					
					if(!Comunes.esDecimal(fn_interes)  || fn_interes.length() > 20){
						errores.append("L�nea " + (i + 1) + ": El monto del Interes debe ser un n�mero de m�ximo 17 d�gitos y 2 decimales.\n");
					}
				
					if( "".equals(fec_real_pago) || !Comunes.esFechaValida(fec_real_pago,"dd/mm/yyyy") || (fec_real_pago.length()!=10)  ){
						errores.append("L�nea " + (i + 1) + ": El formato de la fecha real del pago es inv�lido. Debe ser dd/mm/aaaa.\n");
					}
					
					if( "".equals(fec_pago_cliente) || !Comunes.esFechaValida(fec_pago_cliente,"dd/mm/yyyy") || (fec_pago_cliente.length()!=10)  ){
						errores.append("L�nea " + (i + 1) + ": El formato de la fecha del pago del cliente es inv�lido. Debe ser dd/mm/aaaa.\n");
					}
					
					if( "".equals(estatus_registro) || !"T".equals(estatus_registro)){
						errores.append("L�nea " + (i + 1) + ": El estatus del prestamo debe de ser T (Prepagos).\n");
					}
					
					if( "".equals(origen) || (!"1".equals(origen) && !"2".equals(origen))){
						errores.append("L�nea " + (i + 1) + ": El origen del programa de apoyo debe ser 1 (CFE) � 2 (Luz y Fuerza).\n");
					}
					
					if(!Comunes.esDecimal(fn_total_venc)  || fn_total_venc.length() > 20){
						errores.append("L�nea " + (i + 1) + ": El monto total del vencimiento debe ser un n�mero de m�ximo 17 d�gitos y 2 decimales.\n");
					}else{
						
							if(Double.parseDouble(fn_total_venc) > montoMaxMn){
								errorMontoMaxMn = true;
							}
							if(Double.parseDouble(fn_total_venc) < montoMinMn){
								errorMontoMinMn = true;
							}
							
							if(!"".equals(errores.toString()) ){
								total_prepagos_erroneos = total_prepagos_erroneos.add(new BigDecimal(fn_total_venc)).setScale(2, BigDecimal.ROUND_HALF_UP);
							}else{
								total_prepagos = total_prepagos.add(new BigDecimal(fn_total_venc)).setScale(2, BigDecimal.ROUND_HALF_UP);
							}
						
					}
					// Fodea 026 - 2010
					System.err.println("L�nea " + (i + 1) + ": Clave del Programa: " + clavePrograma);
					if(!Comunes.esNumero(clavePrograma) || clavePrograma.length() > 3){
						errores.append("L�nea " + (i + 1) + ": El N�mero de Programa debe ser un entero no mayor a 3 d�gitos.\n");
					}else{
						int numClaveProgramaSesion = Integer.parseInt(fondoJrPrepagoTotalBean.getClavePrograma());
						int numClavePrograma			= Integer.parseInt(clavePrograma);
						if(numClaveProgramaSesion != numClavePrograma){
							errores.append("L�nea " + (i + 1) + ": El N�mero de Programa no coincide con el programa seleccionado.\n");
						}
					}
					
					if(!"".equals(errores.toString()) ){
						//registro_erroneo.append(com_fechaprobablepago + "|");
						num_reg_error++;
						registro_erroneo.append(num_prestamo + "|");
						registro_erroneo.append(codigo_cliente + "|");
						registro_erroneo.append(fn_total_venc + "|\n");
					}else{
						correctos.append("Linea " + (i + 1) + ": Registro correcto.\n");
											
						registro_correcto.add(fec_real_pago);
						registro_correcto.add(num_prestamo);
						registro_correcto.add(codigo_cliente);
						registro_correcto.add(disposicion);
						registro_correcto.add(fn_amortizacion);
						registro_correcto.add(fn_interes);
						registro_correcto.add(fn_total_venc);
						registro_correcto.add(fec_pago_cliente);
						registro_correcto.add(estatus_registro);
						registro_correcto.add(origen);
						registro_correcto.add(clavePrograma);
					
						this.setNumeroPrestamo(num_prestamo);
						this.setMontoTotalReembolso(fn_total_venc);
					}
					
				}else{
						num_reg_error++;
						errores.append("L�nea " + (i + 1) + ": El layout del registro viene incompleto.\n");
				}
				
				validacion_registro.add(correctos);
				validacion_registro.add(registro_correcto);
				validacion_registro.add(errores);
				validacion_registro.add(registro_erroneo);
				
				resultado_validacion.add(validacion_registro);
				i++;
			}//cerre while
			
			total_prepagos.add(total_prepagos_erroneos);
			//VALIDACIOPN DE CIFRAS DE CONTROL

			if(totalDoctosMn != i)
				erroresCifrasControl.append("El n�mero de Documentos es incorrecto\n");
			if(errorMontoMaxMn)
				erroresCifrasControl.append("El monto por documento no puede ser mayor a "+montoMaxMn+"\n");
			if(errorMontoMinMn)
				erroresCifrasControl.append("El monto por documento no puede ser menor a "+montoMinMn+"\n");
			if(montoTotalMn != (total_prepagos.add(total_prepagos_erroneos)).doubleValue())
				erroresCifrasControl.append("El monto total de los documentos cargados no coincide con el establecido de "+montoTotalMn+"\n");
			
			
			
			
			validacion_prepagos.put("resultado_validacion", resultado_validacion);
			validacion_prepagos.put("total_reembolsos", total_prepagos.toPlainString());
			validacion_prepagos.put("total_prepagos_erroneos", total_prepagos_erroneos.toPlainString());
			validacion_prepagos.put("num_reg_error", String.valueOf(num_reg_error));
			validacion_prepagos.put("erroresCifrasControl", erroresCifrasControl.toString());
			
			
			inputStream.close();
		}catch(Exception e){
		System.out.println("..:: PrepagosTotalesCargaMasiva : leeArchivoPrepagosTotales(ERROR) :: ");
		e.printStackTrace();
		}finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("..:: PrepagosTotalesCargaMasiva : leeArchivoPrepagosTotales(F) ::..");
		}
		return validacion_prepagos;
  }

*/

	public String getNumeroProceso(){return numeroProceso;}
  public String getNumeroPrestamo(){return numeroPrestamo;}
  public String getMontoTotalReembolso(){return montoTotalReembolso;}
  
	public void setNumeroProceso(String numeroProceso){this.numeroProceso = numeroProceso;}
  public void setNumeroPrestamo(String numeroPrestamo){this.numeroPrestamo = numeroPrestamo;}
	public void setMontoTotalReembolso(String montoTotalReembolso){this.montoTotalReembolso = montoTotalReembolso;}
 }