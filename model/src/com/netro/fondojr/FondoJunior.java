package com.netro.fondojr;

import java.io.File;
import java.util.*;
import com.netro.exception.*;
import java.sql.*;
import javax.naming.NamingException;
import netropology.utilerias.*;
import javax.servlet.http.HttpServletRequest;
import javax.ejb.Remote;

@Remote
public interface FondoJunior {

/*---MONITOR DE CREEDITOS---*/
public List  consultaMonitor(String fec_ini, String fec_fin, String estatusCarga, String chkDesembolsos, String cvePrograma) throws NafinException;//FODEA 026-2010 FVR
public List consDetMonitor(String cg_estatus, String fec_vencimiento, String cvePrograma) throws NafinException;
public List validaMonitorPyNP(String fec_vencimiento, String cvePrograma) throws NafinException;//FODEA 026-2010 FVR
public List validaMonitorRCyT(String fec_vencimiento, String cvePrograma) throws NafinException;//FODEA 026-2010 FVR
public List autorizaValidacionMonitor(FondoJrMonitorBean formaMonitor,boolean validCert, String cvePrograma) throws NafinException;
public List autorizaValidacionMonitor(FondoJrMonitorBean formaMonitor, boolean validCert, String cvePrograma, String version) throws NafinException;
public List consAcuseMonitorValidado(String folio, String usuario, String fec_venc)throws NafinException;
public void eliminaRegMonitor(FondoJrMonitorBean bean, boolean bitacora, String cvePrograma)throws NafinException;
public void procValidaMonitorPyNP(String fec_vencimiento)throws NafinException;
public void procValidaMonitorRyT()throws NafinException;

public  String generaCSVMonitor(String fec_ini, String fec_fin, String estatus, String cvePrograma, String descPrograma, 
											  String chkDesembolso, String tipoArchivo, String rutaFisica, String rutaVirtual) throws AppException;

public List consDetMonitorJS(String cg_estatus, String fec_vencimiento, String cvePrograma, int numRegistros) throws NafinException;
public List consAcuseMonitorValidadoJS(String folio, String usuario, String fec_venc)throws NafinException;
public List ObtenerDetalleDeCreditosJS(String ver_enlace, String estatus, String estatus2, String proc, String fec_venc, String cvePrograma, int numRegistros) throws NafinException;

// Catalogo de Operaciones JR
public void insertarCatalagoOperacion(String nombre, String operacion, String clavePrograma)  throws NafinException;
public void modifica(String numero, String nombre, String modificar, String operacion)  throws NafinException;
public void eliminar(String numero)	 throws NafinException;

// Movimientos Generales Al Fondo JR
public void insertMovGeneralesJR(String proceso,String fecha, String importe, String referencia, String Observaciones, String TipoOperacion, String clavePrograma )  throws NafinException;
public void eliminarMovGeneralesJR(String numero) throws NafinException;
public void ModificarMovGeneralesJR( String numero, String fecha, String importe, String referencia, String observaciones, String TipoOperacion,  String modificar,String clavePrograma ) throws NafinException;
public String TerminaOperacion(FondoJRMovimientoGeneral formaMovimiento, boolean validCert, ArrayList ListaDatos)throws NafinException;
public List AcuseMoviGenerales(String proceso) throws NafinException;

  
public List AcuseMoviGeneralesPDF (String proceso)  throws NafinException;

public void CancelarMovGeneralesJR(String proceso) throws NafinException;
// Consulta Movimientos Generales Al Fondo JR

public List getConsultaMovimientosJR(String fecha, String operacion) throws NafinException;

public String Numeroproceso() throws NafinException;

  public List obtenerDatosPrestamo(String numero_prestamo, String cvePrograma) throws NafinException; //FODEA 011 - 2009 ACF
  public void agregaReembolso(List datos_reembolso) throws NafinException; //FODEA 011 - 2009 ACF
  public String getProcesoReembolso()  throws NafinException; //FODEA 011 - 2009 ACF
  public List obtenerReembolsosTemp(String proceso_carga, String cvePrograma) throws NafinException; //FODEA 011 - 2009 ACF //FODEA 026-2010 FVR
  public boolean existeReembolsoTmp(String proceso_carga, String numero_prestamo) throws NafinException; //FODEA 011 - 2009 ACF
  public boolean existeReembolsoFin(String numero_prestamo, String cvePrograma) throws NafinException; //FODEA 011 - 2009 ACF //FODEA 026-2010 FVR
  public void eliminaReembolsoTmp(String proceso_carga, String numero_prestamo) throws NafinException; //FODEA 011 - 2009 ACF
  public void modificaReembolsoTmp(String proceso_carga, String numero_prestamo, String observaciones) throws NafinException; //FODEA 011 - 2009 ACF
  public List obtenerDatosReembolso(String proceso_carga, String numero_prestamo, String cvePrograma) throws NafinException; //FODEA 011 - 2009 ACF //FODEA 026-2010 FVR
  public void guardarReembolsosPorFiniquito(String proceso_carga, String prestamo, String monto_reembolso, String usuario, String tipo_carga, String programa) throws NafinException; //FODEA 011 - 2009 ACF
  public List obtenerOperacionesValidacion(List parametros_consulta) throws NafinException; //FODEA 011 - 2009 ACF
  public List busquedaAvanzadaProveedor(String val_nombre, String val_rfc, String numero_sirac) throws NafinException; //FODEA 011 - 2009 ACF
  //public void guardarValidacionCreditos(List parametros_consulta) throws NafinException; //FODEA 011 - 2009 ACF
	public String guardarValidacionCreditos(List parametros_consulta) throws NafinException; //FODEA 011 - 2009 ACF
  
  public List DisposicionDeFondoInsert(String nombre_usuario, String clavePrograma) throws NafinException; // ��� -IA- �� FODEA 011-2009 ���//FODEA 026 - 2010 ACF
  public List DisposicionDeFondoConsulta(String nombre_usuario, String clavePrograma) throws NafinException; // ��� -IA- �� FODEA 011-2009 ���//FODEA 026 - 2010 ACF
  public List ObtenerDetalleDeCreditos(String ver_enlace, String estatus, String estatus2, String proc, String fec_venc, String cvePrograma) throws NafinException; // ��� -IA- �� FODEA 011-2009 ���
  
  //metodos para conciliacion
	public List consConciliacionConcentrado(String clavePrograma) throws NafinException;//FODEA 026 - 2010 ACF
	public List consConciliacionDetalle(FondoJrConciliacionBean form) throws NafinException;
  //public List consConciliacionPop(String prestamo, String estatus) throws NafinException;
	public List consConciliacionPop(String prestamo, String estatus, String clavePrograma) throws NafinException;//FODEA 026 - 2010 ACF
  //metodos para Consulta
  //public List consConsultaDetalle(FondoJrConsultaBean form) throws NafinException;
  //FODEA 057-2009
  public String agregaPrepagosTmp(List datos_prepagos) throws NafinException;
  public List detallePrepagosTmp(String num_proceso) throws NafinException;
  public HashMap terminaOperacionPrepagos(FondoJrPrepagoTotalBean fondoJrPrepagoTotalBean) throws NafinException;
  public String totalConMovimientos(String operacion, String fechaValor,  String clavePrograma) throws NafinException;
 
  //FODEA 026-2010 FVR
  public List consultaMonitorReemb(String fec_ini, String fec_fin, String cvePrograma) throws NafinException;
  public List validaMonitorR(String fec_venc, String cvePrograma)throws NafinException;
  //AJUSTES F026-2010
  public String generaArchConciliacionZip(FondoJrConciliacionBean form, String rutaFisica, String rutaVirtual, String cvePrograma, String nombreProg) throws NafinException;
  //FODEA 012 - 2012
  public HashMap getDetalleDiasVencimiento(String clavePrograma, String fechaProbablePago) throws AppException;

  //AJUSTES F012-2011
  public String paraDiasVencimiento(List parametros) throws NafinException;
  public String eliminarParaDiasVencidos(List lista) throws NafinException;
  public String modificarParaDiasVencidos(List parametros) throws NafinException;
  public String validaRangoFechas( String fechaIni , String fechaFin, String programa, String noOperacion)  throws NafinException;
  
  public int numReembTotales(String cvePrograma, String pantalla, String folio_val) throws AppException;//012-2011 FVR
  public List getTotalesReembValida(String cvePrograma, String estatus_valida, String folio_val) throws AppException;//012-2011 FVR
  public String generaArchivoGralZip(String rutaFisica, String rutaVirtual, String cvePrograma, String nombreProg, String pantalla, String estatus_val, 
												 String folio_val, String fecVenc, String chkDesembolso, String estatusDetVal, String estatus2DetVal, String procDetVal) throws AppException, NafinException;//012-2011 FVR
  
  public List consultaMonitorNPR(String fec_ini, String fec_fin, String chkDesembolso, String cvePrograma) throws AppException; //F027-2011 FVR
	
	public List ControlFondoJR( String fechaMovimiento, String clavePrograma) throws NafinException; //Fodea 027-2011
	
	public HashMap getCatalogoOrigenPrograma() throws AppException;
	public String  getDescripcionOrigenPrograma(String claveOrigenPrograma,HashMap catalogo);
	
	public List obtenerReembolsosCarga(String proceso_carga, String cvePrograma, String disposiciones) throws NafinException;
	public boolean existeReembolso(String numero_prestamo, String cvePrograma) throws NafinException;
	
	public String cargaDatosTmpAjusteFondoJr (String rutaArchivo, String tipoModif) throws AppException;
	public void validaDatosTmpAjusteFondoJr(int  numeroProceso, int numeroLinea, String tipoMod, String cvePrograma)  throws AppException;
	public List getResumenValidacion(String numProc)throws AppException;
	public HashMap realizaAjusteFondoJr(String numProc, String tipoModifi, String cvePrograma, String iNoUsuario, String strNombreUsuario) throws AppException;
	public String generaArchivoCargaZip(String rutaFisica, String rutaVirtual, String numProc, String csReg) throws AppException;
	
	public void depuraInfoDesembolsosBatch(int dias) throws AppException;
	public void procesarCargaTmpDesembolsosBatch() throws AppException;
	public String generaArchivoCargaBatchZip(String rutaFisica, String rutaVirtual, String numFolio, String icResBatch, String tipoReg) throws AppException;
	public List consultaDesembolsosBatch(HashMap mpCriteriosBusq) throws AppException;
	
	public String generaCSV(FondoJrConciliacionBean form, String rutaFisica, String rutaVirtual) 	throws AppException;
	
	public String generaZIP(FondoJrConciliacionBean form, String rutaFisica, String rutaVirtual) throws AppException;
	
	 public String  proceso() throws NafinException;  
	 
	 public List consConciliacion(String clavePrograma) throws NafinException; 
	 
	 public void eliminaRegMonitorValid(List lstRegEliminar,  String csTipoInfo, String causas, String usuario) throws AppException;
	 public void restableceRegMonitorValid(List lstRegBitacora) throws AppException;
	 public int numRegDetMonitorJS(String cg_estatus, String fec_vencimiento, String cvePrograma) throws NafinException;
	 public int numRegDetalleDeCreditosJS(String ver_enlace, String estatus, String estatus2, String proc, String fec_venc, String cvePrograma) throws NafinException;

	//Fodea 07-2014  FONDO JR -Desembolsos FIDE
	public String getIc_Desembolso() throws NafinException;
	public List   getValidarArchivo (String strDirectorioTemp,  String  nombreArchivo, int numberOfRegisters, String ic_desembolso, String fechaHonrada ) throws NafinException;
	public String archivoErrores(List registros, List cifras, String  path ) throws NafinException;
	public String generarPDFAcuseDesembolso(List parametros) throws NafinException;
	public boolean guardarAcuseyArchivo( List parametros ) throws NafinException;
	public void borraTemporales( String ic_desembolso   )	throws NafinException;
	public  String envioCorreoDesembolsoFJR( List parametros ) throws NafinException;
		
	public void guardaParamCorreo( List parametros ) throws NafinException;
	public void eliminarParamCorreo( String ic_correo ) throws NafinException;
	public List  consParamCorreo() throws NafinException;
	
	public String crearXLSDetValidMonitor(String ver_enlace, String estatus, String estatus2, String proc, String fec_venc, String cvePrograma, String strDirectorioTemp) throws AppException;
	public boolean validaDesembolsoPrepagado (String prestamo, String cvePrograma)	throws AppException;
	 
	 //Fodea 06-2014
	public void  actPrepagoValidacion(String numero_prestamo) throws NafinException;
	public List obtenerOperacionesValidacionEXTJS(List parametros_consulta) throws NafinException;
	//Fodea XX 2014
	public boolean agregarPrograma(String intermediario , String descripcion) throws NafinException;
	public String  eliminarPrograma(String programa) throws NafinException;
	
	public List obtenerNPaReembolsarXprestamo(String numero_prestamo, String cvePrograma)  throws NafinException;
	public void generarReembolsos(List lstRegistros, String cvePrograma) throws NafinException;
	
}