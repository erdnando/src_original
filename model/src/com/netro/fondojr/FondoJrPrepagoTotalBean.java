package com.netro.fondojr;

public class FondoJrPrepagoTotalBean  implements java.io.Serializable  {

	private String hidCtrlNumDoctosMN				= "";
	private String hidCtrlMontoTotalMN 				= "";
	private String hidCtrlMontoTotalMNDespliegue = "";
	private String hidCtrlMontoMinMN					= "";
	private String hidCtrlMontoMinMNDespliegue	= "";
	private String hidCtrlMontoMaxMN					= "";
	private String hidCtrlMontoMaxMNDespliegue	= "";
	private String hidCtrlNumDoctosDL				= "";
	private String hidCtrlMontoTotalDL				= "";
	private String hidCtrlMontoTotalDLDespliegue = "";
	private String hidCtrlMontoMinDL					= "";
	private String hidCtrlMontoMinDLDespliegue	= "";
	private String hidCtrlMontoMaxDL					= "";
	private String hidCtrlMontoMaxDLDespliegue	= "";
	
	private String cboIf = "";	
	
	private String folio								= "";
	private String texto_plano						= "";
	private String Pkcs7								= "";
	private String TextoFirmado					= "";
	private String firmar							= "";
	private String serial							= "";
	private String usuario							= "";
	private String directorio_publicacion		= "";
	private String reg_correctos					= "";
	private String reg_errores						= "";
	private String accion							= "";
	private String num_proceso						= "";
	private String observaciones					= "";
	private String clavePrograma					= "";


	public void setHidCtrlNumDoctosMN(String hidCtrlNumDoctosMN) {
		this.hidCtrlNumDoctosMN = hidCtrlNumDoctosMN;
	}


	public String getHidCtrlNumDoctosMN() {
		return hidCtrlNumDoctosMN;
	}


	public void setHidCtrlMontoTotalMN(String hidCtrlMontoTotalMN) {
		this.hidCtrlMontoTotalMN = hidCtrlMontoTotalMN;
	}


	public String getHidCtrlMontoTotalMN() {
		return hidCtrlMontoTotalMN;
	}


	public void setHidCtrlMontoTotalMNDespliegue(String hidCtrlMontoTotalMNDespliegue) {
		this.hidCtrlMontoTotalMNDespliegue = hidCtrlMontoTotalMNDespliegue;
	}


	public String getHidCtrlMontoTotalMNDespliegue() {
		return hidCtrlMontoTotalMNDespliegue;
	}


	public void setHidCtrlMontoMinMN(String hidCtrlMontoMinMN) {
		this.hidCtrlMontoMinMN = hidCtrlMontoMinMN;
	}


	public String getHidCtrlMontoMinMN() {
		return hidCtrlMontoMinMN;
	}


	public void setHidCtrlMontoMinMNDespliegue(String hidCtrlMontoMinMNDespliegue) {
		this.hidCtrlMontoMinMNDespliegue = hidCtrlMontoMinMNDespliegue;
	}


	public String getHidCtrlMontoMinMNDespliegue() {
		return hidCtrlMontoMinMNDespliegue;
	}


	public void setHidCtrlMontoMaxMN(String hidCtrlMontoMaxMN) {
		this.hidCtrlMontoMaxMN = hidCtrlMontoMaxMN;
	}


	public String getHidCtrlMontoMaxMN() {
		return hidCtrlMontoMaxMN;
	}


	public void setHidCtrlMontoMaxMNDespliegue(String hidCtrlMontoMaxMNDespliegue) {
		this.hidCtrlMontoMaxMNDespliegue = hidCtrlMontoMaxMNDespliegue;
	}


	public String getHidCtrlMontoMaxMNDespliegue() {
		return hidCtrlMontoMaxMNDespliegue;
	}


	public void setHidCtrlNumDoctosDL(String hidCtrlNumDoctosDL) {
		this.hidCtrlNumDoctosDL = hidCtrlNumDoctosDL;
	}


	public String getHidCtrlNumDoctosDL() {
		return hidCtrlNumDoctosDL;
	}


	public void setHidCtrlMontoTotalDL(String hidCtrlMontoTotalDL) {
		this.hidCtrlMontoTotalDL = hidCtrlMontoTotalDL;
	}


	public String getHidCtrlMontoTotalDL() {
		return hidCtrlMontoTotalDL;
	}


	public void setHidCtrlMontoTotalDLDespliegue(String hidCtrlMontoTotalDLDespliegue) {
		this.hidCtrlMontoTotalDLDespliegue = hidCtrlMontoTotalDLDespliegue;
	}


	public String getHidCtrlMontoTotalDLDespliegue() {
		return hidCtrlMontoTotalDLDespliegue;
	}


	public void setHidCtrlMontoMinDL(String hidCtrlMontoMinDL) {
		this.hidCtrlMontoMinDL = hidCtrlMontoMinDL;
	}


	public String getHidCtrlMontoMinDL() {
		return hidCtrlMontoMinDL;
	}


	public void setHidCtrlMontoMinDLDespliegue(String hidCtrlMontoMinDLDespliegue) {
		this.hidCtrlMontoMinDLDespliegue = hidCtrlMontoMinDLDespliegue;
	}


	public String getHidCtrlMontoMinDLDespliegue() {
		return hidCtrlMontoMinDLDespliegue;
	}


	public void setHidCtrlMontoMaxDL(String hidCtrlMontoMaxDL) {
		this.hidCtrlMontoMaxDL = hidCtrlMontoMaxDL;
	}


	public String getHidCtrlMontoMaxDL() {
		return hidCtrlMontoMaxDL;
	}


	public void setHidCtrlMontoMaxDLDespliegue(String hidCtrlMontoMaxDLDespliegue) {
		this.hidCtrlMontoMaxDLDespliegue = hidCtrlMontoMaxDLDespliegue;
	}


	public String getHidCtrlMontoMaxDLDespliegue() {
		return hidCtrlMontoMaxDLDespliegue;
	}


	public void setCboIf(String cboIf) {
		this.cboIf = cboIf;
	}


	public String getCboIf() {
		return cboIf;
	}


	public void setTexto_plano(String texto_plano) {
		this.texto_plano = texto_plano;
	}


	public String getTexto_plano() {
		return texto_plano;
	}


	public void setPkcs7(String Pkcs7) {
		this.Pkcs7 = Pkcs7;
	}


	public String getPkcs7() {
		return Pkcs7;
	}


	public void setTextoFirmado(String TextoFirmado) {
		this.TextoFirmado = TextoFirmado;
	}


	public String getTextoFirmado() {
		return TextoFirmado;
	}


	public void setFirmar(String firmar) {
		this.firmar = firmar;
	}


	public String getFirmar() {
		return firmar;
	}


	public void setSerial(String serial) {
		this.serial = serial;
	}


	public String getSerial() {
		return serial;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setDirectorio_publicacion(String directorio_publicacion) {
		this.directorio_publicacion = directorio_publicacion;
	}


	public String getDirectorio_publicacion() {
		return directorio_publicacion;
	}


	public void setReg_correctos(String reg_correctos) {
		this.reg_correctos = reg_correctos;
	}


	public String getReg_correctos() {
		return reg_correctos;
	}


	public void setReg_errores(String reg_errores) {
		this.reg_errores = reg_errores;
	}


	public String getReg_errores() {
		return reg_errores;
	}


	public void setAccion(String accion) {
		this.accion = accion;
	}


	public String getAccion() {
		return accion;
	}


	public void setNum_proceso(String num_proceso) {
		this.num_proceso = num_proceso;
	}


	public String getNum_proceso() {
		return num_proceso;
	}


	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}


	public String getObservaciones() {
		return observaciones;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getFolio() {
		return folio;
	}
	
	public void setClavePrograma(String clavePrograma) {
		this.clavePrograma = clavePrograma;
	}
 
	public String getClavePrograma() {
		return this.clavePrograma;
	}
 
}