package com.netro.fondojr;

import java.io.Serializable;
import com.netro.exception.*;
import netropology.utilerias.*;
import java.sql.*;
import java.text.*;
import java.math.*;

/**
 * Clase que se encarga de invocar al metodo generaArchivoCargaBathZip del 
 * Bean de FondoJr
 *
 * @author  Fabian Valenzuela
 * @version 1.0.14
 */

public class GeneraArchivoCargaBatchZipThread implements Runnable, Serializable {
	
	private int 				numberOfRegisters;
	private boolean 			started;
	private boolean 			running;
	private int 				processedRegisters;
	private FondoJuniorBean fondojr;
	private boolean			error;
	private String 			nombreArchivo;
	private String 			rutaFisica;
	private String 			rutaVirtual;
	private String				numFolio;
	private String				icResBatch;
	private String				tipoReg;
	

   public GeneraArchivoCargaBatchZipThread() {
	numberOfRegisters 	= 0;
	started 			   = false;
	running 			   = false;
	processedRegisters = 0;
	fondojr			= new FondoJuniorBean();
	error					= false;
   }
	 
   protected void generaArchivoZip() {
		
	try {

		nombreArchivo = fondojr.generaArchivoCargaBatchZip(rutaFisica, rutaVirtual, numFolio, icResBatch, tipoReg);
				

	} catch(Exception e){
			setRunning(false);
			processedRegisters = numberOfRegisters ;
			setError(true);
			e.printStackTrace();
			System.out.println("Error en ValidaDatosPymeMasivaThread::validateRegisters al extraer numero de registro de la base de datos");
			// terminar el proceso si falla la funcion de validacion para algun registro
	}
		
   }
	
	public synchronized int getNumberOfRegisters() {
       return numberOfRegisters;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		return((processedRegisters == 0 && numberOfRegisters == 0)?false:(processedRegisters == numberOfRegisters));
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
	public synchronized void setError(boolean error) {
       this.error = error;
   }
	
	public synchronized boolean hasError() {
       return error;
   }
	
	public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }
	
	public synchronized void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}
	
	public synchronized void setTipoReg(String tipoReg) {
		this.tipoReg = tipoReg;
	}
	
	public synchronized void setIcResBatch(String icResBatch) {
		this.icResBatch = icResBatch;
	}

   public void run() {
		 
		// Obtener numero de registros
		//AccesoDB 	con 	=	new AccesoDB();
		boolean		lbOK	=  true;
		
		// Realizar la Validacion de los datos
      try {
			
			//if(lbOK == false) 
				setRunning(true);
			
				while (isRunning() && nombreArchivo == null){
					if(lbOK && nombreArchivo == null){
						lbOK = false;
						generaArchivoZip();
					}
				}
		
      } finally {
			setRunning(false);
      }

    }

	 public String getNombreArchivo(){ // Fodea 057 - 2010
		return nombreArchivo;
	 }
 
	 public void setNombreArchivo(String nombreArchivo){ // Fodea 057 - 2010
		this.nombreArchivo = nombreArchivo;
	 }


	public void setRutaFisica(String rutaFisica) {
		this.rutaFisica = rutaFisica;
	}


	public String getRutaFisica() {
		return rutaFisica;
	}


	public void setRutaVirtual(String rutaVirtual) {
		this.rutaVirtual = rutaVirtual;
	}


	public String getRutaVirtual() {
		return rutaVirtual;
	}

}