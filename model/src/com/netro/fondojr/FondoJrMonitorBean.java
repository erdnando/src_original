package com.netro.fondojr;

public class FondoJrMonitorBean implements java.io.Serializable  {
	private String ic_if					= "";
	private String fec_venc				= "";
	private String fec_venc_ini		= "";
	private String fec_venc_fin		= "";
	private String fec_Actual			= "";
	private String form_accion			= "";
	private String Pkcs7					= "";
	private String TextoFirmado		= "";
	private String firmar				= "";
	private String serial				= "";
	private String usuario				= "";
	private String folio					= "";
	private String sumaTotalPyNP		= "";
	private String sumaTotalReemb		= "";
	private String sumaTotalPrepago	= "";
	private String sumaTotalRec		= "";
	private String montoTotalPyNP		= "";
	private String montoTotalReemb	= "";
	private String montoTotalPrepago	= "";
	private String montoTotalRec		= "";
	private String tipo_credito		= "";
	private String estatus_proc		= "";
	private String estatus_cred		= "";
	private String chkDesembolso		= "";
	
	private String strDirectorioPublicacion	 = "";

	/*public FondoJrMonitorBean() {
		ic_if					= "";
		fec_venc				= "";
		fec_venc_ini		= "";
		fec_venc_fin		= "";
		form_accion			= "";
		Pkcs7					= "";
		TextoFirmado		= "";
		firmar				= "";
		serial				= "";
		usuario				= "";
		folio					= "";
		sumaTotalPyNP		= "";
		sumaTotalReemb		= "";
		sumaTotalPrepago	= "";
		sumaTotalRec		= "";
		montoTotalPyNP		= "";
		montoTotalReemb	= "";
		montoTotalPrepago	= "";
		montoTotalRec		= "";
		fec_Actual			= "";
		strDirectorioPublicacion = "";

	}*/
	

	//	GETS PARA LA FORMA DE LA PANTALLA MONITOR 
	public String getEstatus_proc(){ return estatus_proc; }

	public void setEstatus_proc(String estatus_proc)
	{
		this.estatus_proc = estatus_proc;
	}
	public String getEstatus_cred(){ return estatus_cred; }
	
	public void setEstatus_cred(String estatus_cred)
	{
		this.estatus_cred = estatus_cred;
	}

	public String getTipo_credito(){ return tipo_credito; }

	public void setTipo_credito(String tipo_credito)
	{
		this.tipo_credito = tipo_credito;
	}
	public String getMontoTotalRec(){ return montoTotalRec; }

	public void setMontoTotalRec(String montoTotalRec)
	{
		this.montoTotalRec = montoTotalRec;
	}
	public String getSumaTotalRec(){ return sumaTotalRec; }
	
	public void setSumaTotalRec(String sumaTotalRec)
	{
		this.sumaTotalRec = sumaTotalRec;
	}
	public String getFec_Actual(){ return fec_Actual; }

	public void setFec_Actual(String fec_Actual)
	{
		this.fec_Actual = fec_Actual;
	}
	public String getSumaTotalPyNP(){ return sumaTotalPyNP; }

	public void setSumaTotalPyNP(String sumaTotalPyNP)
	{
		this.sumaTotalPyNP = sumaTotalPyNP;
	}
	public String getSumaTotalReemb(){ return sumaTotalReemb; }
	
	public void setSumaTotalReemb(String sumaTotalReemb)
	{
		this.sumaTotalReemb = sumaTotalReemb;
	}
	public String getSumaTotalPrepago(){ return sumaTotalPrepago; }
	
	public void setSumaTotalPrepago(String sumaTotalPrepago)
	{
		this.sumaTotalPrepago = sumaTotalPrepago;
	}
	public String getMontoTotalPyNP(){ return montoTotalPyNP; }
	
	public void setMontoTotalPyNP(String montoTotalPyNP)
	{
		this.montoTotalPyNP = montoTotalPyNP;
	}
	public String getMontoTotalReemb(){ return montoTotalReemb; }
	
	public void setMontoTotalReemb(String montoTotalReemb)
	{
		this.montoTotalReemb = montoTotalReemb;
	}
	public String getMontoTotalPrepago(){ return montoTotalPrepago; }
	
	public void setMontoTotalPrepago(String montoTotalPrepago)
	{
		this.montoTotalPrepago = montoTotalPrepago;
	}
	public String getFolio(){ return folio; }

	public void setFolio(String folio)
	{
		this.folio = folio;
	}
	public String getUsuario(){ return usuario; }

	public void setUsuario(String usuario)
	{
		this.usuario = usuario;
	}
	public String getSerial(){ return serial; }

	public void setSerial(String serial)
	{
		this.serial = serial;
	}
	public String getFirmar(){ return firmar; }
	
	public void setFirmar(String firmar)
	{
		this.firmar = firmar;
	}
	public String getPkcs7(){ return Pkcs7; }
	public String getTextoFirmado(){ return TextoFirmado; }
	public String getFec_venc(){ return fec_venc; }
	public String getIc_if(){ return ic_if; }
	public String getFec_venc_ini(){ return fec_venc_ini; }
	public String getFec_venc_fin(){ return fec_venc_fin; }
	public String getForm_accion(){ return form_accion; }
	public String getStrDirectorioPublicacion(){ return strDirectorioPublicacion; }
	// SETS PARA LA FORMA DE LA PANTALLA MONITOR 
	public void setFec_venc(String fec_venc){
		this.fec_venc = fec_venc;
	}	
	public void setIc_if(String ic_if){
		this.ic_if = ic_if;
	}
	public void setFec_venc_ini(String fec_venc_ini){
		this.fec_venc_ini = fec_venc_ini;
	}
	public void setFec_venc_fin(String fec_venc_fin){
		this.fec_venc_fin = fec_venc_fin;
	}
	public void setForm_accion(String form_accion){
		this.form_accion = form_accion;
	}
	public void setStrDirectorioPublicacion(String strDirectorioPublicacion){
		this.strDirectorioPublicacion = strDirectorioPublicacion;
	}
	public void setPkcs7(String Pkcs7){
		this.Pkcs7 = Pkcs7;
	}
	public void setTextoFirmado(String TextoFirmado){
		this.TextoFirmado = TextoFirmado;
	}

	
	public String toString() {
		String cadena = "";
		cadena = "[" +
				"ic_if=" + ic_if + "\n" +
				"usuario=" + usuario + "\n" +
				"Pkcs7=" + Pkcs7 + "\n" +
				"TextoFirmado=" + TextoFirmado + "\n" +
				"serial=" + serial + "\n" +
				"fec_venc=" + fec_venc + "\n" +
				"fec_venc_ini=" + fec_venc_ini + "\n" +
				"fec_venc_fin=" + fec_venc_fin + "\n" +
				"form_accion=" + form_accion + "\n" +
				"firmar=" + firmar + "\n" +
				"strDirectorioPublicacion=" + strDirectorioPublicacion + "\n" +
				"folio=" + folio + "\n" +"]";
		
		return cadena;
   }


	public void setChkDesembolso(String chkDesembolso) {
		this.chkDesembolso = chkDesembolso;
	}


	public String getChkDesembolso() {
		return chkDesembolso;
	}
}