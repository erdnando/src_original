package com.netro.fondojr;

import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.sql.*;
import java.math.BigDecimal;
import netropology.utilerias.*;
import com.netro.exception.*;
import java.text.SimpleDateFormat;
import org.apache.commons.logging.Log;

/**
 * Esta clase proporciona mecanismos para realizar el proceso de carga masiva de 
 * reembolsos por finiquito de la Operaci�n de Fondo Junior.
 * @author Alberto Cruz Flores
 * @since FODEA 013 - 2009 Fondo Junior
 */
 
 public class ReembolsosPorFiniquitoCargaMasiva implements Serializable{
	private String numeroProceso;
	private String numeroPrestamo;
	private String montoTotalReembolso;
  
  //Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(FondoJuniorBean.class);
	 
	public ReembolsosPorFiniquitoCargaMasiva(){}

	/**
	 * Lee el archivo de carga de documentos y almacena la informaci�n contenida en una lista.
	 * @param inputStream Stream con los datos del archivo origen.
	 * @return List con los datos que contiene el archivo
	 */
	public List leeArchivoReembolsosPorFiniquito(InputStream inputStream) throws NafinException{
		List reembolsos_cargados = new ArrayList();
		String linea = "";
    //String com_fechaprobablepago = "";
    String ig_prestamo = "";
    String ig_cliente = "";
    //String ig_disposicion = "";
    //String fg_amortizacion = "";
    //String fg_interes = "";
    String fg_totalvencimiento = "";
    //String df_pago_cliente = "";
    //String cg_estatus = "";
    //String ig_origen = "";
    //String df_periodofin = "";
    //String ig_numero_docto = "";
    String observaciones = "";

		System.out.println("..:: ReembolsosPorFiniquitoCargaMasiva : leeArchivoReembolsosPorFiniquito(I) ::..");

		try{
			BufferedReader breader = new BufferedReader(new InputStreamReader(inputStream));
		
			while((linea = breader.readLine()) != null){
				if(linea!=null && !"".equals(linea)){
					StringTokenizer tokens = new StringTokenizer(linea, "|");
					
					ig_prestamo = tokens.hasMoreTokens()?tokens.nextToken("|"):"";
					ig_cliente = tokens.hasMoreTokens()?tokens.nextToken("|"):"";
					fg_totalvencimiento = tokens.hasMoreTokens()?tokens.nextToken("|"):"";
					observaciones = tokens.hasMoreTokens()?tokens.nextToken("|"):"";
					//com_fechaprobablepago = tokens.nextToken("|");
					//ig_disposicion = tokens.nextToken("|");
					//fg_amortizacion = tokens.nextToken("|");
					//fg_interes = tokens.nextToken("|");
					//df_pago_cliente = tokens.nextToken("|");
					//cg_estatus = tokens.nextToken("|");
					//ig_origen = tokens.nextToken("|");
					//df_periodofin = tokens.nextToken("|");
					//ig_numero_docto = tokens.nextToken("|");
					
					
					
					List reembolso = new ArrayList();
					reembolso.add(ig_prestamo);
					reembolso.add(ig_cliente);
					reembolso.add(fg_totalvencimiento);
					reembolso.add(observaciones);
					//reembolso.add(com_fechaprobablepago);
					//reembolso.add(ig_disposicion);
					//reembolso.add(fg_amortizacion);
					//reembolso.add(fg_interes);
					//reembolso.add(df_pago_cliente);
					//reembolso.add(cg_estatus);
					//reembolso.add(ig_origen);
					//reembolso.add(df_periodofin);
					//reembolso.add(ig_numero_docto);
					
					
					reembolsos_cargados.add(reembolso);
				}
			}
      inputStream.close();
    }catch(Exception e){
      System.out.println("..:: ReembolsosPorFiniquitoCargaMasiva : leeArchivoReembolsosPorFiniquito(ERROR) :: ");
      e.printStackTrace();
    }finally {
      System.out.println("..:: ReembolsosPorFiniquitoCargaMasiva : leeArchivoReembolsosPorFiniquito(F) ::..");
    }
    return reembolsos_cargados;
  }

	/**
	 * Lee el archivo de carga de documentos y almacena la informaci�n contenida en una lista.
	 * @param inputStream Stream con los datos del archivo origen.
	 * @return List con los datos que contiene el archivo
	 */
	public HashMap validaReembolsosPorFiniquito(List reembolsos_cargados, String clavePrograma) throws NafinException{
    AccesoDB con = new AccesoDB();
	 HashMap validacion_reembolsos = new HashMap();
    List resultado_validacion = new ArrayList();
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    PreparedStatement pst = null;
    ResultSet rst = null;
	 BigDecimal total_reembolsos = new BigDecimal("0.00");
	 String linea = "";
    
    //String com_fechaprobablepago = "";
    String ig_prestamo = "";
    String ig_cliente = "";
    //String ig_disposicion = "";
    //String fg_amortizacion = "";
    //String fg_interes = "";
    String fg_totalvencimiento = "";
    //String df_pago_cliente = "";
    //String cg_estatus = "";
    //String ig_origen = "";
    //String df_periodofin = "";
    //String ig_numero_docto = "";
    String observaciones = "";
    String ig_prestamo_aux = "";
    //String com_fechaprobablepago_pf = "";
    String ig_prestamo_pf = "";
    String ig_cliente_pf = "";
    //String ig_disposicion_pf = "";
    //String fg_amortizacion_pf = "";
    //String fg_interes_pf = "";
    //String fg_totalvencimiento_pf = "";
    //String df_pago_cliente_pf = "";
    //String cg_estatus_pf = "";
    //String ig_origen_pf = "";
    //String df_periodofin_pf = "";
    //String ig_numero_docto_pf = "";
		System.out.println("..:: ReembolsosPorFiniquitoCargaMasiva : validaReembolsosPorFiniquito(I) ::..");

		try{
			con.conexionDB();
      
			System.out.println("reembolsos  "+reembolsos_cargados.size());
			System.out.println("reembolsos  "+reembolsos_cargados  );
			
      for(int i = 0; i < reembolsos_cargados.size(); i++){
        List validacion_registro =  new ArrayList();
        List registro_correcto = new ArrayList();
        StringBuffer registro_erroneo = new StringBuffer();
        StringBuffer errores = new StringBuffer();
        StringBuffer correctos = new StringBuffer();
		  StringBuffer erroresArchivo = new StringBuffer();
        
        List reembolso = (List)reembolsos_cargados.get(i);        
        ig_prestamo = (String)reembolso.get(0);
        ig_cliente = (String)reembolso.get(1);
        fg_totalvencimiento = (String)reembolso.get(2);
        observaciones = (String)reembolso.get(3); 

        strSQL = new StringBuffer();
        varBind =  new ArrayList();
	      if(!ig_prestamo.equals("")){
	
					if(!Comunes.esNumero(ig_prestamo) || ig_prestamo.length() > 12){
            errores.append("L�nea " + (i + 1) + ": El n�mero de pr�stamo debe ser un entero de 12 d�gitos.\n");
				erroresArchivo.append("El n�mero de pr�stamo debe ser un entero de 12 d�gitos, ");
          }else{
					
							ig_prestamo_pf = "";
							strSQL.append(" SELECT DISTINCT ig_prestamo,");
							strSQL.append(" ig_cliente");
							strSQL.append(" FROM com_pagos_fide_val cpf");
							strSQL.append(" WHERE ig_prestamo = ? ");					
							strSQL.append(" AND ic_programa_fondojr =  ? ");

							varBind.add(new Long(ig_prestamo));
							varBind.add(new Long(clavePrograma));
							pst = con.queryPrecompilado(strSQL.toString(), varBind);
							rst = pst.executeQuery();
        
							while(rst.next()){							
								ig_prestamo_pf = rst.getString(1)==null?"":rst.getString(1);
								ig_cliente_pf = rst.getString(2)==null?"":rst.getString(2);          
							}
							pst.close();
							rst.close();
							System.out.println("ig_prestamo_pf   "+ig_prestamo_pf);
				
							if(ig_prestamo_pf.equals("")) {
								errores.append("Linea " + (i + 1) +": El n�mero de pr�stamo no existe.\n");
								erroresArchivo.append("El n�mero de pr�stamo no existe, ");
							}else{
									
								strSQL = new StringBuffer();
								varBind =  new ArrayList();
            
								strSQL.append(" SELECT COUNT(ig_prestamo) numero_registros  ");
								strSQL.append(" FROM com_pagos_fide_val  cpf ");
								strSQL.append(" WHERE cpf.ig_prestamo = ?  ");
								strSQL.append(" AND cpf.ic_programa_fondojr = ? ");
								strSQL.append(" AND cpf.cg_estatus IN ('NP') ");
								strSQL.append(" and  ig_disposicion not in ( SELECT ig_disposicion  ");
								strSQL.append(" FROM com_pagos_fide_val cpf ");
								strSQL.append(" WHERE cpf.ig_prestamo  = ? ");
								strSQL.append(" AND cpf.ic_programa_fondojr = ?  ");
								strSQL.append(" and  cg_estatus in('R','P','RF', 'T') ) ");
 
								varBind.add(new Long(ig_prestamo));
								varBind.add(new Long(clavePrograma));
								varBind.add(new Long(ig_prestamo));
								varBind.add(new Long(clavePrograma));
								pst = con.queryPrecompilado(strSQL.toString(), varBind);
								rst = pst.executeQuery();
								
								while(rst.next()){
									if(rst.getString(1).equals("0")){
										errores.append("L�nea " + (i + 1) + ": El pr�stamo ya ha sido finiquitado.\n");
										erroresArchivo.append("El pr�stamo ya ha sido finiquitado, ");
									}
								}
								
								pst.close();
								rst.close();
								
								FondoJuniorBean fondoJunior = new FondoJuniorBean();
								if(fondoJunior.validaDesembolsoPrepagado(ig_prestamo,clavePrograma)){
									errores.append("L�nea " + (i + 1) + ": El pr�stamo se encuentra Prepagado.\n");
									erroresArchivo.append("El pr�stamo se encuentra Prepagado, ");
								}
							}
          
							if(!Comunes.esNumero(ig_cliente) || ig_cliente.length() > 12){
								errores.append("L�nea " + (i + 1) + ": El c�digo de cliente debe ser un entero de 12 d�gitos.\n");
								erroresArchivo.append("El c�digo de cliente debe ser un entero de 12 d�gitos, ");
							}else{
								if(!ig_cliente.equals(ig_cliente_pf)){
									errores.append("L�nea " + (i + 1) + ": El c�digo de cliente no corresponde al n�mero de pr�stamo.\n");
									erroresArchivo.append("El c�digo de cliente no corresponde al n�mero de pr�stamo, ");
								}
							}

							if(!Comunes.esDecimal(fg_totalvencimiento)  || fg_totalvencimiento.length() > 21){
								errores.append("L�nea " + (i + 1) + ": El monto total del vencimiento debe ser un n�mero de m�ximo 19 d�gitos y 2 decimales.\n");
								erroresArchivo.append("El monto total del vencimiento debe ser un n�mero de m�ximo 19 d�gitos y 2 decimales, ");
							}else{
								total_reembolsos = total_reembolsos.add(new BigDecimal(fg_totalvencimiento)).setScale(2, BigDecimal.ROUND_HALF_UP);
						}

             ig_prestamo_aux = ig_prestamo;
					
					}
        } else{
          errores.append("Linea " + (i + 1) +": El n�mero de pr�stamo no existe.\n");
			 erroresArchivo.append("El n�mero de pr�stamo no existe, ");
          
        }
				
			if(!errores.toString().equals("")){
            registro_erroneo.append(ig_prestamo + "|");
            registro_erroneo.append(ig_cliente + "|");
            registro_erroneo.append(fg_totalvencimiento + "|");      
            registro_erroneo.append(observaciones + "|");
				registro_erroneo.append(erroresArchivo.substring(0, (erroresArchivo.length()-2)).toString() + "|\n");
						
          }else{
            correctos.append("Linea " + (i + 1) + ": Registro correcto.\n");
            registro_correcto.add(ig_prestamo);
            registro_correcto.add(ig_cliente);
            registro_correcto.add(fg_totalvencimiento);
            registro_correcto.add(observaciones);
            this.setNumeroPrestamo(ig_prestamo);
            this.setMontoTotalReembolso(fg_totalvencimiento);
          }
					

        validacion_registro.add(correctos);
        validacion_registro.add(registro_correcto);
        validacion_registro.add(errores);
        validacion_registro.add(registro_erroneo);
        
        resultado_validacion.add(validacion_registro);
      }
		
		validacion_reembolsos.put("resultado_validacion", resultado_validacion);
		validacion_reembolsos.put("total_reembolsos", total_reembolsos.toPlainString());

    }catch(Exception e){
      System.out.println("..:: ReembolsosPorFiniquitoCargaMasiva : validaReembolsosPorFiniquito(ERROR) :: ");
      e.printStackTrace();
    }finally {
      System.out.println("..:: ReembolsosPorFiniquitoCargaMasiva : validaReembolsosPorFiniquito(F) ::..");
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    return validacion_reembolsos;
  }
  
  /**
	 * Lee el archivo de carga de documentos y almacena la informaci�n contenida en una lista.
	 * @param inputStream Stream con los datos del archivo origen.
	 * @return List con los datos que contiene el archivo
	 */
	public String cargaTmpDesembolsosBatch(InputStream inputStream, String nombreArchivo, String cgUsuario, String nombreUsr, String cvePrograma) throws NafinException{
		log.info("ReembolsosPorFiniquitoCargaMasiva : cargaTmpDesembolsosBatch(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		boolean commit = true;
		
		VectorTokenizer vtd = null;
		Vector vecdet = null;
		String linea = "";
		String numProcResumen = "";
		String folioBatch = "";
		
		String ig_prestamo = "";
		String ig_cliente = "";
		String fg_totalvencimiento = "";
		String observaciones = "";
		int contReg = 0;

		try{
			con.conexionDB();
			BufferedReader breader = new BufferedReader(new InputStreamReader(inputStream));
		
			query = "SELECT SEQ_COMTMP_DESEMBOLSOS_BATCH.NEXTVAL numProc FROM DUAL";
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				numeroProceso = rs.getString("numProc");
			}
			rs.close();
			ps.close();
			
			query = "INSERT INTO comtmp_desembolsos_batch " +
				"            (ic_desembolsos_batch, ig_linea, ig_prestamo, ig_cliente, fn_monto_reembolso, cg_observaciones " +
				"            ) " +
				"     VALUES (?, ?, ?, ?, ?, ?) ";

			
			while ((linea=breader.readLine())!=null){
				if(linea!=null && !"".equals(linea)){
					contReg++;
					vtd		= new VectorTokenizer(linea,"|");
					vecdet	= vtd.getValuesVector();
					
					ig_prestamo	=(vecdet.size()>=1)?vecdet.get(0).toString().trim():"";
					ig_cliente		=(vecdet.size()>=2)?vecdet.get(1).toString().trim().toUpperCase():"";
					fg_totalvencimiento	=(vecdet.size()>=3)?vecdet.get(2).toString().trim().toUpperCase():"";
					observaciones	=(vecdet.size()>=4)?vecdet.get(3).toString().trim().toUpperCase():"";
					
					ig_prestamo =Comunes.quitaComitasSimplesyDobles(ig_prestamo);
					ig_cliente =Comunes.quitaComitasSimplesyDobles(ig_cliente);
					fg_totalvencimiento =Comunes.quitaComitasSimplesyDobles(fg_totalvencimiento);
					observaciones =Comunes.quitaComitasSimplesyDobles(observaciones);
					
					ig_prestamo	 =((ig_prestamo.length()>12)?ig_prestamo.substring(0,13):ig_prestamo);
					ig_cliente		 =((ig_cliente.length()>12)?ig_cliente.substring(0,13):ig_cliente);
					fg_totalvencimiento =((fg_totalvencimiento.length()>20)?fg_totalvencimiento.substring(0,21):fg_totalvencimiento);
					observaciones =((observaciones.length()>200)?observaciones.substring(0,201):observaciones);
					
					ps = con.queryPrecompilado(query);
					ps.setLong(1, Long.parseLong(numeroProceso));
					ps.setInt(2, contReg);
					ps.setString(3, ig_prestamo);
					ps.setString(4, ig_cliente);
					ps.setString(5, fg_totalvencimiento);
					ps.setString(6, observaciones);
					
					ps.executeUpdate();
					ps.close();
				}
				
			}
			inputStream.close();
			
			if(contReg>0){
				
				query = "SELECT seq_com_resumen_desem_batch.NEXTVAL numProc FROM DUAL";
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					numProcResumen = rs.getString("numProc");
				}
				rs.close();
				ps.close();
				
				//Se genera folio de la carga batch
				java.util.Date date = new java.util.Date();
				String fechaFolio = (new SimpleDateFormat ("ddMMyyyy")).format(date);
				folioBatch = fechaFolio+numProcResumen;
				
				//Se inserta resumen de la carga batch antes de que sea procesada
				query = "INSERT INTO com_resumen_desembolsos_batch " +
					"            (ic_resumen_desem_batch, ic_desembolsos_batch, cg_folio_batch, " +
					"             cg_nombre_archivo, ig_reg_total, ig_reg_pend, cg_usuario, " +
					"             cg_nombre_usr, ic_programa_fondojr " +
					"            ) " +
					"     VALUES (?, ?, ?, " +
					"             ?, ?, ?, ?, " +
					"             ?, ? " +
					"            ) ";
					
				ps = con.queryPrecompilado(query);
				ps.setLong(1, Long.parseLong(numProcResumen));
				ps.setLong(2, Long.parseLong(numeroProceso));
				ps.setString(3, folioBatch);
				ps.setString(4, nombreArchivo);
				ps.setInt(5, contReg);
				ps.setInt(6, contReg);
				ps.setString(7, cgUsuario);
				ps.setString(8, nombreUsr);
				ps.setLong(9, Long.parseLong(cvePrograma));
				
				ps.executeUpdate();
				ps.close();
			}
			
			return folioBatch;
		}catch(Throwable e){
			log.info("ReembolsosPorFiniquitoCargaMasiva : cargaTmpDesembolsosBatch(Error)");
			commit = false;
			throw new AppException("Error al realizar carga temporal de desembolsos batch",e);
		}finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("ReembolsosPorFiniquitoCargaMasiva : cargaTmpDesembolsosBatch(S)");
		}
   
	}
	
	

 
	public String getNumeroProceso(){return numeroProceso;}
	public String getNumeroPrestamo(){return numeroPrestamo;}
	public String getMontoTotalReembolso(){return montoTotalReembolso;}
  
	public void setNumeroProceso(String numeroProceso){this.numeroProceso = numeroProceso;}
	public void setNumeroPrestamo(String numeroPrestamo){this.numeroPrestamo = numeroPrestamo;}
	public void setMontoTotalReembolso(String montoTotalReembolso){this.montoTotalReembolso = montoTotalReembolso;}
 }