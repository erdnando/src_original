package com.netro.catalogos;

import com.netro.catalogos.mantenimiento.CatalogoMantenimiento;
import com.netro.catalogos.mantenimiento.CatalogoMantenimientoEstado;
import com.netro.exception.NafinException;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;

@Stateless(name = "ActualizacionCatalogosEJB" , mappedName = "ActualizacionCatalogosEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ActualizacionCatalogosBean implements ActualizacionCatalogos {

	// Metodos para Administracion - Parametrizaci�n - "Actualizacion Cat�logos".

	public String menu(String esNombreTabla) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsError = "Error al Actualizar el Cat�logo ";
	String lsOK = "Se Realiz� con �xito la Actualizaci�n del Cat�logo ";
	StringBuffer lsbMsg = new StringBuffer();
	PreparedStatement ps = null;

	try{
		lobdConexion.conexionDB();
		/* Actualizaci�n de Cat�logos Fijos. */
		// SectorEconomico - SubSector - Rama - Clase
		if(esNombreTabla.equalsIgnoreCase("Sector Econ")) {
			lsbMsg.append((sectorEconomico(lobdConexion)==false?lsError+"Sector Econ�mico. \n":lsOK+"Sector Econ�mico. \n"));
		}
		if(esNombreTabla.equalsIgnoreCase("Subsector")) {
			lsbMsg.append((sectorEconomico(lobdConexion)==false?lsError+"Sector Econ�mico. \n":lsOK+"Sector Econ�mico. \n"));
			lsbMsg.append((subSector(lobdConexion)==false?lsError+"SubSector. \n":lsOK+"SubSector. \n"));
		}
		if(esNombreTabla.equalsIgnoreCase("Rama")) {
			lsbMsg.append((sectorEconomico(lobdConexion)==false?lsError+"Sector Econ�mico. \n":lsOK+"Sector Econ�mico. \n"));
			lsbMsg.append((subSector(lobdConexion)==false?lsError+"SubSector. \n":lsOK+"SubSector. \n"));
			lsbMsg.append((rama(lobdConexion)==false?lsError+"Rama. \n":lsOK+"Rama. \n"));
		}
		if(esNombreTabla.equalsIgnoreCase("Clase")) {
			lsbMsg.append((sectorEconomico(lobdConexion)==false?lsError+"Sector Econ�mico. \n":lsOK+"Sector Econ�mico. \n"));
			lsbMsg.append((subSector(lobdConexion)==false?lsError+"SubSector. \n":lsOK+"SubSector. \n"));
			lsbMsg.append((rama(lobdConexion)==false?lsError+"Rama. \n":lsOK+"Rama. \n"));
			lsbMsg.append((clase(lobdConexion)==false?lsError+"Clase. \n":lsOK+"Clase. \n"));
		}
		// Pais 	-	Estado	-	Municipio
		if(esNombreTabla.equalsIgnoreCase("Pais")) {
			lsbMsg.append((pais(lobdConexion)==false?lsError+"Pais. \n":lsOK+"Pais. \n"));
		}
		if(esNombreTabla.equalsIgnoreCase("Estado")) {
			lsbMsg.append((pais(lobdConexion)==false?lsError+"Pais. \n":lsOK+"Pais. \n"));
			lsbMsg.append((estado(lobdConexion)==false?lsError+"Estado. \n":lsOK+"Estado \n."));
		}
		if(esNombreTabla.equalsIgnoreCase("Municipio")) {
			lsbMsg.append((pais(lobdConexion)==false?lsError+"Pais. \n":lsOK+"Pais. \n"));
			lsbMsg.append((estado(lobdConexion)==false?lsError+"Estado. \n":lsOK+"Estado. \n"));
			lsbMsg.append((municipio(lobdConexion)==false?lsError+"Municipio. \n":lsOK+"Municipio. \n"));
		}
		// Tipo Financiera 	-	Financiera
		if(esNombreTabla.equalsIgnoreCase("Tipo Financiera"))
			lsbMsg.append((tipoFinanciera(lobdConexion)==false?lsError+"Tipo Financiera. \n":lsOK+"Tipo Financiera. \n"));
		if(esNombreTabla.equalsIgnoreCase("Financiera")) {
			lsbMsg.append((tipoFinanciera(lobdConexion)==false?lsError+"Tipo Financiera. \n":lsOK+"Tipo Financiera. \n"));
			lsbMsg.append((financiera(lobdConexion)==false?lsError+"Financiera. \n":lsOK+"Financiera. \n"));
		}
		/* Actualizaci�n de Cat�logos Individuales. */
		if(esNombreTabla.equalsIgnoreCase("Base Operacion"))
			lsbMsg.append((baseOperacion(lobdConexion)==false?lsError+"Bases de Operaci�n.":lsOK+"Bases de Operaci�n."));
		if(esNombreTabla.equalsIgnoreCase("Causa Rechazo"))
			lsbMsg.append((causaRechazo(lobdConexion)==false?lsError+"Causa de Rechazo.":lsOK+"Causa de Rechazo."));
		if(esNombreTabla.equalsIgnoreCase("Error Procesos"))
			lsbMsg.append((errorProcesos(lobdConexion)==false?lsError+"Error de Procesos.":lsOK+"Error de Procesos."));
		if(esNombreTabla.equalsIgnoreCase("Estrato"))
			lsbMsg.append((estrato(lobdConexion)==false?lsError+"Estrato.":lsOK+"Estrato."));
		if(esNombreTabla.equalsIgnoreCase("Identificacion"))
			lsbMsg.append((identificacion(lobdConexion)==false?lsError+"Identificaci�n.":lsOK+"Identificaci�n."));
		if(esNombreTabla.equalsIgnoreCase("Moneda"))
			lsbMsg.append((moneda(lobdConexion)==false?lsError+"Moneda.":lsOK+"Moneda."));
		if(esNombreTabla.equalsIgnoreCase("Oficina Estatal"))
			lsbMsg.append((oficinaEstatal(lobdConexion)==false?lsError+"Oficina Estatal.":lsOK+"Oficina Estatal."));
		if(esNombreTabla.equalsIgnoreCase("Tabla Amort"))
			lsbMsg.append((amortizacion(lobdConexion)==false?lsError+"Amortizaci�n.":lsOK+"Amortizaci�n."));
		if(esNombreTabla.equalsIgnoreCase("Tasa"))
			lsbMsg.append((tasa(lobdConexion)==false?lsError+"Tasa.":lsOK+"Tasa."));
    if(esNombreTabla.equalsIgnoreCase("Ciudad")){//Fodea campos PLD 2010
			lsbMsg.append((pais(lobdConexion)==false?lsError+"Pais. \n":lsOK+"Pais. \n"));
			lsbMsg.append((estado(lobdConexion)==false?lsError+"Estado. \n":lsOK+"Estado. \n"));
    	lsbMsg.append((ciudad(lobdConexion)==false?lsError+"Ciudad.":lsOK+"Ciudad."));
    }
		/* Este Proceso se Ejecuta cuando se da el Cierre de Servicio. */
		if(esNombreTabla.equalsIgnoreCase("Todos")) {
			String query = "select cs_cierre_servicio_nafin, 'ActualizacionCatalogosEJB::menu()' from comcat_producto_nafin "+
							" where ic_producto_nafin=1";

			ps = lobdConexion.queryPrecompilado(query);
			ResultSet rs = ps.executeQuery();

			//ResultSet rs = lobdConexion.queryDB(query);
			String todos = "";
			while(rs.next()) {
				todos = (rs.getString(1)==null)?"":rs.getString(1).trim();
			}
			if(ps != null) ps.close();

			if(todos.equals("S")) {
				lsbMsg.append((sectorEconomico(lobdConexion)==false?lsError+"Sector Econ�mico. \n":lsOK+"Sector Econ�mico. \n"));
				lsbMsg.append((subSector(lobdConexion)==false?lsError+"SubSector. \n":lsOK+"SubSector. \n"));
				lsbMsg.append((rama(lobdConexion)==false?lsError+"Rama. \n":lsOK+"Rama. \n"));
				lsbMsg.append((clase(lobdConexion)==false?lsError+"Clase. \n":lsOK+"Clase. \n"));
				lsbMsg.append((pais(lobdConexion)==false?lsError+"Pais. \n":lsOK+"Pais. \n"));
				lsbMsg.append((estado(lobdConexion)==false?lsError+"Estado. \n":lsOK+"Estado. \n"));
				lsbMsg.append((municipio(lobdConexion)==false?lsError+"Municipio. \n":lsOK+"Municipio. \n"));
				lsbMsg.append((tipoFinanciera(lobdConexion)==false?lsError+"Tipo Financiera. \n":lsOK+"Tipo Financiera. \n"));
				lsbMsg.append((financiera(lobdConexion)==false?lsError+"Financiera. \n":lsOK+"Financiera. \n"));
				/* ------- */
				lsbMsg.append((baseOperacion(lobdConexion)==false?lsError+"Bases de Operaci�n. \n":lsOK+"Bases de Operaci�n. \n"));
				lsbMsg.append((causaRechazo(lobdConexion)==false?lsError+"Causa de Rechazo. \n":lsOK+"Causa de Rechazo. \n"));
				lsbMsg.append((errorProcesos(lobdConexion)==false?lsError+"Error de Procesos. \n":lsOK+"Error de Procesos. \n"));
				lsbMsg.append((estrato(lobdConexion)==false?lsError+"Estrato. \n":lsOK+"Estrato. \n"));
				lsbMsg.append((identificacion(lobdConexion)==false?lsError+"Identificaci�n. \n":lsOK+"Identificaci�n. \n"));
				lsbMsg.append((moneda(lobdConexion)==false?lsError+"Moneda. \n":lsOK+"Moneda. \n"));
				lsbMsg.append((oficinaEstatal(lobdConexion)==false?lsError+"Oficina Estatal. \n":lsOK+"Oficina Estatal. \n"));
				lsbMsg.append((amortizacion(lobdConexion)==false?lsError+"Amortizaci�n. \n":lsOK+"Amortizaci�n. \n"));
				lsbMsg.append((tasa(lobdConexion)==false?lsError+"Tasa. \n":lsOK+"Tasa. \n"));
        lsbMsg.append((ciudad(lobdConexion)==false?lsError+"Ciudad. \n":lsOK+"Ciudad. \n"));//Fodea campos PLD 2010
			}
		}//Todos
	}//try
	//catch(SQLException sqle) { msg="Error "+sqle.getMessage(); }
	catch(Exception e) { System.out.println("Error menu. "+e.getMessage()); }
	finally{
		if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
	}
	return lsbMsg.toString();
	}


	private boolean sectorEconomico(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_sector_econ (ic_sector_econ, cd_nombre) "+
					"(select codigo_sector, descripcion from mg_tipo_sector "+
					"where codigo_sector not in (select ic_sector_econ from comcat_sector_econ) )";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Sector Econ�mico. "+sqle.getMessage());
			}

			lsQry = "update comcat_sector_econ E "+
					"set E.cd_nombre = (select S.descripcion from mg_tipo_sector S where E.ic_sector_econ = S.codigo_sector) "+
					"where E.ic_sector_econ = (select S1.codigo_sector from mg_tipo_sector S1 where E.ic_sector_econ = S1.codigo_sector)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Sector Econ�mico. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Sector Econ�mico. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //sectorEconomico


	private boolean subSector(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_subsector (ic_sector_econ, ic_subsector, cd_nombre) "+
					"(select TS.codigo_sector, TS.codigo_subsector, TS.descripcion from mg_tipo_subsector TS "+
					"where TS.codigo_sector not in (select S.ic_sector_econ from comcat_subsector S where S.ic_sector_econ=TS.codigo_sector and S.ic_subsector=TS.codigo_subsector) "+
					"and TS.codigo_subsector not in (select S.ic_subsector from comcat_subsector S where S.ic_sector_econ=TS.codigo_sector and S.ic_subsector=TS.codigo_subsector))";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo SubSector. "+sqle.getMessage());
			}

			lsQry = "update comcat_subsector S "+
					"set S.cd_nombre = (select TS.descripcion FROM mg_tipo_subsector TS where S.ic_sector_econ = TS.codigo_sector and S.ic_subsector = TS.codigo_subsector) "+
					"where S.ic_sector_econ = (select TS1.codigo_sector FROM mg_tipo_subsector TS1 where S.ic_sector_econ = TS1.codigo_sector and S.ic_subsector = TS1.codigo_subsector) "+
					"and S.ic_subsector = (select TS2.codigo_subsector FROM mg_tipo_subsector TS2 where S.ic_sector_econ = TS2.codigo_sector and S.ic_subsector = TS2.codigo_subsector)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo SubSector. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo SubSector. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //subSector


	private boolean rama(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_rama (ic_sector_econ, ic_subsector, ic_rama, cd_nombre) "+
					"(select SR.codigo_sector, SR.codigo_subsector, SR.codigo_rama, SR.descripcion from mg_tipo_rama SR "+
					"where SR.codigo_sector not in (select R.ic_sector_econ from comcat_rama R where R.ic_sector_econ=SR.codigo_sector and R.ic_subsector=SR.codigo_subsector and R.ic_rama=SR.codigo_rama) "+
					"and SR.codigo_subsector not in (select R.ic_subsector from comcat_rama R where R.ic_sector_econ=SR.codigo_sector and R.ic_subsector=SR.codigo_subsector and R.ic_rama=SR.codigo_rama) "+
					"and SR.codigo_rama not in (select R.ic_rama from comcat_rama R where R.ic_sector_econ=SR.codigo_sector and R.ic_subsector=SR.codigo_subsector and R.ic_rama=SR.codigo_rama) )";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Rama. "+sqle.getMessage());
			}

			lsQry = "update comcat_rama R "+
					"set R.cd_nombre = (select SR.descripcion FROM mg_tipo_rama SR where R.ic_sector_econ = SR.codigo_sector and R.ic_subsector = SR.codigo_subsector and R.ic_rama = SR.codigo_rama) "+
					"where R.ic_sector_econ = (select SR1.codigo_sector FROM mg_tipo_rama SR1 where R.ic_sector_econ = SR1.codigo_sector and R.ic_subsector = SR1.codigo_subsector and R.ic_rama = SR1.codigo_rama) "+
					"and R.ic_subsector = (select SR2.codigo_subsector FROM mg_tipo_rama SR2 where R.ic_sector_econ = SR2.codigo_sector and R.ic_subsector = SR2.codigo_subsector and R.ic_rama = SR2.codigo_rama) "+
					"and R.ic_rama = (select SR3.codigo_rama FROM mg_tipo_rama SR3 where R.ic_sector_econ = SR3.codigo_sector and R.ic_subsector = SR3.codigo_subsector and R.ic_rama = SR3.codigo_rama)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Rama. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Rama. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //rama


	private boolean clase(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_clase (ic_sector_econ, ic_subsector, ic_rama, ic_clase, cd_nombre) "+
					"(select SC.codigo_sector, SC.codigo_subsector, SC.codigo_rama, SC.codigo_clase, SC.descripcion from mg_tipo_clase SC "+
					"where SC.codigo_sector not in (select C.ic_sector_econ from comcat_clase C where C.ic_sector_econ = SC.codigo_sector and C.ic_subsector = SC.codigo_subsector and C.ic_rama = SC.codigo_rama and C.ic_clase = SC.codigo_clase) "+
					"and SC.codigo_subsector not in (select C.ic_subsector from comcat_clase C where C.ic_sector_econ = SC.codigo_sector and C.ic_subsector = SC.codigo_subsector and C.ic_rama = SC.codigo_rama and C.ic_clase = SC.codigo_clase) "+
					"and SC.codigo_rama not in (select C.ic_rama from comcat_clase C where C.ic_sector_econ = SC.codigo_sector and C.ic_subsector = SC.codigo_subsector and C.ic_rama = SC.codigo_rama and C.ic_clase = SC.codigo_clase) "+
					"and SC.codigo_clase not in (select C.ic_clase from comcat_clase C where C.ic_sector_econ = SC.codigo_sector and C.ic_subsector = SC.codigo_subsector and C.ic_rama = SC.codigo_rama and C.ic_clase = SC.codigo_clase) )";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Clase. "+sqle.getMessage());
			}

			lsQry = "update comcat_clase C "+
					"set C.cd_nombre = (select SC.descripcion from mg_tipo_clase SC where C.ic_sector_econ = SC.codigo_sector and C.ic_subsector = SC.codigo_subsector and C.ic_rama = SC.codigo_rama and C.ic_clase = SC.codigo_clase) "+
					"where C.ic_sector_econ = (select SC1.codigo_sector from mg_tipo_clase SC1 where C.ic_sector_econ = SC1.codigo_sector and C.ic_subsector = SC1.codigo_subsector and C.ic_rama = SC1.codigo_rama and C.ic_clase = SC1.codigo_clase) "+
					"and C.ic_subsector = (select SC2.codigo_subsector from mg_tipo_clase SC2 where C.ic_sector_econ = SC2.codigo_sector and C.ic_subsector = SC2.codigo_subsector and C.ic_rama = SC2.codigo_rama and C.ic_clase = SC2.codigo_clase) "+
					"and C.ic_rama = (select SC3.codigo_rama from mg_tipo_clase SC3 where C.ic_sector_econ = SC3.codigo_sector and C.ic_subsector = SC3.codigo_subsector and C.ic_rama = SC3.codigo_rama and C.ic_clase = SC3.codigo_clase) "+
					"and C.ic_clase = (select SC4.codigo_clase from mg_tipo_clase SC4 where C.ic_sector_econ = SC4.codigo_sector and C.ic_subsector = SC4.codigo_subsector and C.ic_rama = SC4.codigo_rama and C.ic_clase = SC4.codigo_clase)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Clase. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Clase. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //clase


  /**
   *
   * @throws com.netro.exception.NafinException
   * @return
   * @param lobdConexion
   */
	private boolean pais(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_pais (ic_pais, cd_descripcion) "+
					"(select codigo_pais, nombre from mg_paises "+
					"where codigo_pais not in (select ic_pais from comcat_pais) )";
			System.out.println("ActualizacionCatalogosBean::pais::lsQry:"+lsQry+"*");
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Pais. "+sqle.getMessage());
			}

			lsQry = "update comcat_pais P "+
					"set P.cd_descripcion = (select SP.nombre FROM mg_paises SP where P.ic_pais = SP.codigo_pais) "+
					"where P.ic_pais = (select SP1.codigo_pais FROM mg_paises SP1 where P.ic_pais = SP1.codigo_pais)";
			System.out.println("ActualizacionCatalogosBean::pais::lsQry:"+lsQry+"*");
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Pais. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Pais. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //pais


	private boolean estado(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			/*
			lsQry = "insert into comcat_estado (ic_estado, cd_nombre) "+
					"(select SD.codigo_departamento, SD.nombre from mg_departamentos SD "+
					"where SD.codigo_departamento not in (select E1.ic_estado from comcat_estado E1 where E1.ic_estado=SD.codigo_departamento) "+
					"and SD.codigo_pais = 24)";
			*/
			//FODA 36-2005
			lsQry =
					" INSERT INTO comcat_estado e "+
					"            (ic_estado, cd_nombre, ic_pais, codigo_departamento) "+
					"   		 (SELECT ( rownum + (select max(ic_estado) from comcat_estado) ), sd.nombre, sd.codigo_pais, sd.codigo_departamento "+
					"     		    FROM mg_departamentos sd "+
					"     		   WHERE (sd.codigo_pais ||'-'|| sd.codigo_departamento) NOT IN ( "+
					"                                   SELECT (e1.ic_pais ||'-'|| e1.codigo_departamento)  "+
					"                                     FROM comcat_estado e1 "+
					"                                    WHERE e1.codigo_departamento = sd.codigo_departamento) "+
					"				   	  and sd.codigo_departamento !=99   "+
					"			 )  ";

			System.out.println("ActualizacionCatalogosBean::estado::lsQry:"+lsQry+"*");
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Estado. "+sqle.getMessage());
			}
			/*
			lsQry = "update comcat_estado E "+
					"set E.cd_nombre = (select SE.nombre FROM mg_departamentos SE where E.ic_estado = SE.codigo_departamento and SE.codigo_pais = 24) "+
					"where E.ic_estado = (select SE1.codigo_departamento FROM mg_departamentos SE1 where E.ic_estado = SE1.codigo_departamento and SE1.codigo_pais = 24)";
			*/
			//FODA 36-2005
			lsQry =
					" UPDATE comcat_estado e "+
					"   SET e.cd_nombre =  "+
					"          (SELECT se.nombre "+
					"             FROM mg_departamentos se "+
					"            WHERE e.codigo_departamento = se.codigo_departamento and se.codigo_pais = e.ic_pais) "+
					" WHERE e.codigo_departamento = "+
					"          (SELECT se1.codigo_departamento "+
					"             FROM mg_departamentos se1 "+
					"            WHERE e.codigo_departamento = se1.codigo_departamento and se1.codigo_pais = e.ic_pais ) ";

			System.out.println("ActualizacionCatalogosBean::estado::lsQry:"+lsQry+"*");
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Estado. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Estado. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //estado


	private boolean municipio(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			/*
			lsQry = "insert into comcat_municipio (ic_pais, ic_estado, ic_municipio, cd_nombre) "+
					"(select SM.codigo_pais, SM.codigo_departamento, SM.codigo_municipio, SM.nombre from mg_municipios SM "+
					"where SM.codigo_pais not in (select M.ic_pais from comcat_municipio M where M.ic_pais = SM.codigo_pais and M.ic_estado = SM.codigo_departamento and M.ic_municipio = SM.codigo_municipio) "+
					"and SM.codigo_departamento not in (select M.ic_estado from comcat_municipio M where M.ic_pais = SM.codigo_pais and M.ic_estado = SM.codigo_departamento and M.ic_municipio = SM.codigo_municipio) "+
					"and SM.codigo_municipio not in (select M.ic_municipio from comcat_municipio M where M.ic_pais = SM.codigo_pais and M.ic_estado = SM.codigo_departamento and M.ic_municipio = SM.codigo_municipio) "+
					"and SM.codigo_pais = 24)";
			*/
			//FODA 36-2005
			lsQry =
					" INSERT INTO comcat_municipio (ic_pais, ic_estado, ic_municipio, cd_nombre) "+
					"     ( "+
					" 	     select  sm.codigo_pais, ce.IC_ESTADO, sm.codigo_municipio,sm.nombre  "+
					"	       from  mg_municipios sm,comcat_estado ce "+
					"	      where "+
					"                 sm.codigo_pais !=99 "+
					"             and "+
					"	   		    (sm.codigo_pais || '-' || sm.codigo_departamento || '-' || sm.codigo_municipio) "+
					"			     not in "+
					"			    ( "+
					"			    select /*+ index(ce2 CP_COMCAT_ESTADO_PK)*/ (cm2.IC_PAIS||'-'||ce2.CODIGO_DEPARTAMENTO||'-'||cm2.IC_MUNICIPIO)  "+
					"			      from comcat_municipio cm2,comcat_estado ce2 "+
					"			     where cm2.IC_ESTADO = ce2.IC_ESTADO "+
					"			       and cm2.IC_PAIS = ce2.IC_PAIS  "+
					"			    )				 "+
					"			 and sm.CODIGO_DEPARTAMENTO = ce.CODIGO_DEPARTAMENTO "+
					"			 and sm.codigo_pais = ce.IC_PAIS				 "+
					"     ) ";

			System.out.println("ActualizacionCatalogosBean::municipio::lsQry:"+lsQry+"*");
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Municipio. "+sqle.getMessage());
			}
			/*
			lsQry = "update comcat_municipio M "+
					"set M.cd_nombre = (select SM.nombre from mg_municipios SM where M.ic_pais = SM.codigo_pais and M.ic_estado = SM.codigo_departamento and M.ic_municipio = SM.codigo_municipio and SM.codigo_pais = 24) "+
					"where M.ic_pais = (select SM.codigo_pais from mg_municipios SM where M.ic_pais = SM.codigo_pais and M.ic_estado = SM.codigo_departamento and M.ic_municipio = SM.codigo_municipio and SM.codigo_pais = 24) "+
					"and M.ic_estado = (select SM.codigo_departamento from mg_municipios SM where M.ic_pais = SM.codigo_pais and M.ic_estado = SM.codigo_departamento and M.ic_municipio = SM.codigo_municipio and SM.codigo_pais = 24) "+
					"and M.ic_municipio = (select SM.codigo_municipio from mg_municipios SM where M.ic_pais = SM.codigo_pais and M.ic_estado = SM.codigo_departamento and M.ic_municipio = SM.codigo_municipio and SM.codigo_pais = 24)";
			*/
			//FODA 36-2005

			lsQry =
					"			UPDATE comcat_municipio m "+
					"   SET m.cd_nombre =       "+
					"          (SELECT sm.nombre "+
					"             FROM mg_municipios sm,comcat_estado e1 "+
					"            WHERE m.ic_pais = sm.codigo_pais "+
					"              	  AND m.ic_municipio = sm.codigo_municipio "+
					"				  and e1.ic_estado = m.ic_estado "+
					"				  and e1.ic_pais = m.ic_pais "+
					"				  AND e1.codigo_departamento = sm.codigo_departamento  			   "+
					"		  ) "+
					" WHERE m.ic_pais = "+
					"          (SELECT sm.codigo_pais "+
					"             FROM mg_municipios sm,comcat_estado e2 "+
					"            WHERE m.ic_pais = sm.codigo_pais "+
					"              	  AND m.ic_municipio = sm.codigo_municipio "+
					"				  and e2.ic_estado = m.ic_estado "+
					"				  and e2.ic_pais = m.ic_pais "+
					"				  AND e2.codigo_departamento = sm.codigo_departamento  			   "+
					"		  ) "+
					"   AND m.ic_municipio = "+
					"          (SELECT sm.codigo_municipio "+
					"             FROM mg_municipios sm,comcat_estado e3 "+
					"            WHERE m.ic_pais = sm.codigo_pais "+
					"              	  AND m.ic_municipio = sm.codigo_municipio "+
					"				  and e3.ic_estado = m.ic_estado "+
					"				  and e3.ic_pais = m.ic_pais "+
					"				  AND e3.codigo_departamento = sm.codigo_departamento  			   "+
					"		  ) ";

			System.out.println("ActualizacionCatalogosBean::municipio::lsQry:"+lsQry+"*");
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Municipio. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Municipio. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //municipio


	private boolean tipoFinanciera(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_tipo_financiera (ic_tipo_financiera, cd_nombre, in_dias_limite_entrega_rep, in_ente_contable, in_cod, ic_tipo_empresa) "+
					"(select tipo_financiera, descripcion, dias_limite_entrega_rep, ente_contable, codigo_linea_financiera, codigo_empresa from mg_tipos_de_financiera "+
					"where tipo_financiera not in (select ic_tipo_financiera from comcat_tipo_financiera))";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Tipo Financiera. "+sqle.getMessage());
			}

			lsQry = "update comcat_tipo_financiera F "+
					"set F.cd_nombre = (select FS.descripcion FROM mg_tipos_de_financiera FS where F.ic_tipo_financiera = FS.tipo_financiera), "+
					"F.in_dias_limite_entrega_rep = (select FS1.dias_limite_entrega_rep FROM mg_tipos_de_financiera FS1 where F.ic_tipo_financiera = FS1.tipo_financiera), "+
					"F.in_ente_contable = (select FS2.ente_contable FROM mg_tipos_de_financiera FS2 where F.ic_tipo_financiera = FS2.tipo_financiera), "+
					"F.in_cod = (select FS3.codigo_linea_financiera FROM mg_tipos_de_financiera FS3 where F.ic_tipo_financiera = FS3.tipo_financiera), "+
					"F.ic_tipo_empresa = (select FS4.codigo_empresa FROM mg_tipos_de_financiera FS4 where F.ic_tipo_financiera = FS4.tipo_financiera) "+
					"where F.ic_tipo_financiera = (select FS5.codigo_empresa FROM mg_tipos_de_financiera FS5 where F.ic_tipo_financiera = FS5.tipo_financiera)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Tipo Financiera. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Tipo Financiera. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //tipoFinanciera


	private boolean financiera(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_financiera (ic_financiera, ic_tipo_financiera, cd_nombre, cg_nombre_largo, ic_estado) "+
					"(select codigo_financiera, tipo_financiera, nombre, nombre_largo, codigo_departamento from mg_financieras "+
					"where codigo_financiera not in (select ic_financiera from comcat_financiera))";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Financiera. "+sqle.getMessage());
			}

			lsQry = "update comcat_financiera F "+
					"set F.cd_nombre = (select FS.nombre FROM mg_financieras FS where F.ic_financiera = FS.codigo_financiera and F.ic_tipo_financiera = FS.tipo_financiera), "+
					"F.cg_nombre_largo = (select FS1.nombre_largo FROM mg_financieras FS1 where F.ic_financiera = FS1.codigo_financiera and F.ic_tipo_financiera = FS1.tipo_financiera), "+
					"F.ic_estado = (select FS2.codigo_departamento FROM mg_financieras FS2 where F.ic_financiera = FS2.codigo_financiera and F.ic_tipo_financiera = FS2.tipo_financiera) "+
					"where F.ic_financiera = (select FS3.codigo_financiera FROM mg_financieras FS3 where F.ic_financiera = FS3.codigo_financiera and F.ic_tipo_financiera = FS3.tipo_financiera)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Financiera. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Financiera. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //financiera


	private boolean baseOperacion(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_base_operacion (ig_codigo_base, cg_descripcion) "+
					"(select SB.codigo_base_operacion, SB.descripcion from no_bases_de_operacion SB "+
					"where SB.codigo_base_operacion not in (select B.ig_codigo_base from comcat_base_operacion B where B.ig_codigo_base=SB.codigo_base_operacion) )";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Bases de Operaci�n. "+sqle.getMessage());
			}

			lsQry = "update comcat_base_operacion B "+
					"set B.cg_descripcion = (select SB.descripcion FROM no_bases_de_operacion SB where B.ig_codigo_base = SB.codigo_base_operacion) "+
					"where B.ig_codigo_base = (select SB1.codigo_base_operacion FROM no_bases_de_operacion SB1 where B.ig_codigo_base = SB1.codigo_base_operacion)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Bases de Operaci�n. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Bases de Operaci�n. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //baseOperacion


	private boolean causaRechazo(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_causa_rechazo (ic_causa_rechazo, cd_descripcion) "+
					"(select codigo_bloqueo, descripcion_bloqueo from mg_tipos_de_bloqueos "+
					"where codigo_bloqueo not in (select ic_causa_rechazo from comcat_causa_rechazo))";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Causa de Rechazo. "+sqle.getMessage());
			}

			lsQry = "update comcat_causa_rechazo R "+
					"set R.cd_descripcion = (select BS.descripcion_bloqueo FROM mg_tipos_de_bloqueos BS where R.ic_causa_rechazo = BS.codigo_bloqueo) "+
					"where R.ic_causa_rechazo = (select BS2.codigo_bloqueo FROM mg_tipos_de_bloqueos BS2 where R.ic_causa_rechazo = BS2.codigo_bloqueo)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Causa de Rechazo. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Causa de Rechazo. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //causaRechazo


	private boolean errorProcesos(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_error_procesos (ic_error_proceso, cg_descripcion, cc_codigo_aplicacion) "+
					"(select SE.codigo_estatus, SE.descripcion_estatus, SE.codigo_aplic_envia from mg_estatus_transferencia SE "+
					"where SE.codigo_estatus not in (select EP.ic_error_proceso from comcat_error_procesos EP where EP.ic_error_proceso=SE.codigo_estatus and EP.cc_codigo_aplicacion=SE.codigo_aplic_envia) "+
					"and SE.codigo_aplic_envia not in (select EP.cc_codigo_aplicacion from comcat_error_procesos EP where EP.ic_error_proceso=SE.codigo_estatus and EP.cc_codigo_aplicacion=SE.codigo_aplic_envia) )";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Error de Procesos. "+sqle.getMessage());
			}

			lsQry = "update comcat_error_procesos EP "+
					"set EP.cg_descripcion = (select SE.descripcion_estatus from mg_estatus_transferencia SE where EP.ic_error_proceso = SE.codigo_estatus and EP.cc_codigo_aplicacion=SE.codigo_aplic_envia) "+
					"where EP.ic_error_proceso = (select SE1.codigo_estatus from mg_estatus_transferencia SE1 where EP.ic_error_proceso = SE1.codigo_estatus and EP.cc_codigo_aplicacion=SE1.codigo_aplic_envia) "+
					"and EP.cc_codigo_aplicacion = (select SE2.codigo_aplic_envia from mg_estatus_transferencia SE2 where EP.ic_error_proceso = SE2.codigo_estatus and EP.cc_codigo_aplicacion=SE2.codigo_aplic_envia)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Error de Procesos. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Error de Procesos. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //errorProcesos


	private boolean estrato(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_estrato (ic_estrato, cd_nombre) "+
					"(select codigo_estrato, descripcion from mg_estrato "+
					"where codigo_estrato not in (select ic_estrato from comcat_estrato) )";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Estrato. "+sqle.getMessage());
			}

			lsQry = "update comcat_estrato E "+
					"set E.cd_nombre = (select ES.descripcion from mg_estrato ES where E.ic_estrato = ES.codigo_estrato) "+
					"where E.ic_estrato = (select ES2.codigo_estrato from mg_estrato ES2 where E.ic_estrato = ES2.codigo_estrato)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Estrato. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Estrato. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //estrato


	private boolean identificacion(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_identificacion (ic_identificacion, cd_nombre) "+
					"(select codigo_tipo_identificacion, descripcion from mg_tipos_de_identificacion "+
					"where codigo_tipo_identificacion not in (select ic_identificacion from comcat_identificacion))";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Identificaci�n. "+sqle.getMessage());
			}

			lsQry = "update comcat_identificacion I "+
					"set I.cd_nombre = (select SI.descripcion FROM mg_tipos_de_identificacion SI where I.ic_identificacion = SI.codigo_tipo_identificacion) "+
					"where I.ic_identificacion = (select S2.codigo_tipo_identificacion FROM mg_tipos_de_identificacion S2 where I.ic_identificacion = S2.codigo_tipo_identificacion)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Identificaci�n. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Identificaci�n. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //identificacion


	private boolean moneda(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_moneda (ic_moneda, cd_nombre) "+
					"(select codigo_moneda, descripcion from mg_monedas "+
					"where codigo_moneda not in (select ic_moneda from comcat_moneda))";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Moneda. "+sqle.getMessage());
			}

			lsQry = "update comcat_moneda M "+
					"set M.cd_nombre = (select MS.descripcion FROM mg_monedas MS where M.ic_moneda = MS.codigo_moneda) "+
					"where M.ic_moneda = (select MS2.codigo_moneda FROM mg_monedas MS2 where M.ic_moneda = MS2.codigo_moneda)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Moneda. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Moneda. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //moneda


	private boolean oficinaEstatal(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_oficina_estatal (ic_oficina, cd_descripcion) "+
					"(select codigo_agencia, nombre_agencia from mg_agencias_generales "+
					"where codigo_agencia not in (select ic_oficina from comcat_oficina_estatal) )";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Oficina Estatal. "+sqle.getMessage());
			}

			lsQry = "update comcat_oficina_estatal OE "+
					"set OE.cd_descripcion = (select SA.nombre_agencia from mg_agencias_generales SA where OE.ic_oficina = SA.codigo_agencia) "+
					"where OE.ic_oficina = (select SA1.codigo_agencia from mg_agencias_generales SA1 where OE.ic_oficina = SA1.codigo_agencia)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Oficina Estatal. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Oficina Estatal. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //oficinaEstatal


	private boolean amortizacion(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_tabla_amort (ic_tabla_amort, cd_descripcion) "+
					"(select codigo_tipo_amortizacion, descripcion from pr_tipos_amortizacion "+
					"where codigo_tipo_amortizacion not in (select ic_tabla_amort from comcat_tabla_amort) )";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Amortizaci�n. "+sqle.getMessage());
			}

			lsQry = "update comcat_tabla_amort A "+
					"set A.cd_descripcion = (select SA.descripcion from pr_tipos_amortizacion SA where A.ic_tabla_amort = SA.codigo_tipo_amortizacion) "+
					"where A.ic_tabla_amort = (select SA1.codigo_tipo_amortizacion from pr_tipos_amortizacion SA1 where A.ic_tabla_amort = SA1.codigo_tipo_amortizacion)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Amortizaci�n. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Amortizaci�n. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //amortizacion


	private boolean tasa(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
		try {
			lsQry = "insert into comcat_tasa (ic_tasa, cd_nombre) "+
					"(select codigo_valor_tasa_cartera, descripcion from mg_grupos_tasa "+
					"where codigo_valor_tasa_cartera not in (select ic_tasa from comcat_tasa) )";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo Tasa. "+sqle.getMessage());
			}

			lsQry = "update comcat_tasa T "+
					"set T.cd_nombre = (select ST.descripcion from mg_grupos_tasa ST where T.ic_tasa = ST.codigo_valor_tasa_cartera) "+
					"where T.ic_tasa = (select ST1.codigo_valor_tasa_cartera from mg_grupos_tasa ST1 where T.ic_tasa = ST1.codigo_valor_tasa_cartera)";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Tasa. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo Tasa. "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //tasa


	/**
	 * Actualiza los datos del ajuste anual.
	 * @param ic_if Clave del if del cual se estableceran los par�metros
	 * 		del ajuste anual
	 * @param ig_mes_ajuste_de Mes inferior del rango en el que se establece
	 * 		el ajuste anual
	 * @param ig_mes_ajuste_a Mes superior del rango en el que se establece
	 * 		el ajuste anual
	 * @param ig_anio_ajuste A�o para el que se especifica el ajuste anual
	 *	@param perfil es el usuario que va ejecutar el metodo ("ADMIN NAFIN"|"ADMIN BANCOMEXT") con el fin de
    * establecer una restriccion diferente para la query
	 * @exception NafinException si existe un error al grabar la informaci�n.
	 */
	public void guardarAjusteAnual(String ic_if,
		String ig_mes_ajuste_de, String ig_mes_ajuste_a,
		String ig_anio_ajuste, String perfil) throws NafinException {
		AccesoDB	con			= null;
		String 		strSQL		= "";
	    boolean		resultado	= true;
		String bancoSegunPerfil = "";
		String campo1 = "";
		String campo2 = "";
		String campo3 = "";

		/*Verificar que perfil es*/
		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			campo1 = " ig_mes_ajuste_de ";
			campo2 = " ig_mes_ajuste_a ";
			campo3 = " ig_anio_ajuste ";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			campo1 = " ig_mes_ajuste_de_bmx ";
			campo2 = " ig_mes_ajuste_a_bmx ";
			campo3 = " ig_anio_ajuste_bmx ";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}

		try{
			con = new AccesoDB();
			con.conexionDB();

			/*Consulta anterior
			 * strSQL =	" UPDATE comcat_if " +
						" SET ig_mes_ajuste_de = " + ig_mes_ajuste_de + "," +
						" ig_mes_ajuste_a = " + ig_mes_ajuste_a + "," +
						" ig_anio_ajuste = " + ig_anio_ajuste +
						" WHERE ic_if = " + ic_if;*/

			strSQL = "UPDATE comcat_if SET " + campo1 + "=" + ig_mes_ajuste_de + ", " + campo2 + "=" + ig_mes_ajuste_a + ", " + campo3 + "=" + ig_anio_ajuste +
						" WHERE ic_if = " + ic_if +
						"AND ic_if in (select ic_if " +
						"  				from comrel_if_epo_x_producto " +
						"					where ic_epo in(select ic_epo " +
                  "										from comcat_epo " +
                  "										where ic_banco_fondeo in(	select ic_banco_fondeo " +
                  "																			from comcat_banco_fondeo " +
                  "																			where ic_banco_fondeo =" + bancoSegunPerfil + ")))";
				System.out.println(".::guardarAjusteAnual::strSQL::. " + strSQL);

			con.ejecutaSQL(strSQL);
    	}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
			    con.cierraConexionDB();
			}
		}
	}



	// Metodos para Administracion - Parametrizaci�n - nafin - "Comision Recursos Propios"
	public boolean guardaDatosComision(String cs_tipo_fondeo, String ic_if, String perfil) throws NafinException{
		AccesoDB	con			= null;
		String 		strSQL		= "";
	   boolean		resultado	= true;
		String bancoSegunPerfil = "";
		System.out.println("*************Guargar Datos*******");
		/*Verificar que perfil es*/
		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}

		try{
			con = new AccesoDB();
			con.conexionDB();

			strSQL =	"Update comrel_if_epo_x_producto " +
						"Set cs_tipo_fondeo = '" + cs_tipo_fondeo + "' " +
						"Where ic_if = " + ic_if  +
						" And comrel_if_epo_x_producto.ic_epo in (Select ic_epo " +
						"														From comcat_epo " +
						"														Where comcat_epo.ic_banco_fondeo = " + bancoSegunPerfil + ")";

						System.out.println(".::guardaDatosComision - strSQL::. " + strSQL);

			con.ejecutaSQL(strSQL);
    	}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
			    con.cierraConexionDB();
			}
		}
	    return true;
	}

	/**
	 *Funcion que inserta elementos a la tabla com_param_if
	 * si ya existe el registro lo elimina
	 **/
	public boolean guardaDatosComisionParamIf(String ic_if, String cg_financiamiento, String cf_comision, String fg_por_comision_de, String fg_porc_comision_dm, String fg_porc_comision_vc, String fg_porc_comision_m2) throws NafinException{
		AccesoDB	con			= null;
		String 		strSQL		= "";
		String 		strSQLD		= "";
		String 		strSQLCondicion		= "";
	   boolean		resultado	= true;
		String resul  = "";
		System.out.println("::guardaDatosComisionParamIf(E)..");
		ResultSet existe = null;
		try{
			con = new AccesoDB();
			con.conexionDB();
			strSQLCondicion="SELECT IC_IF FROM com_param_if WHERE IC_IF= "+ic_if;
			existe = con.queryDB(strSQLCondicion);
			if(existe.next()){
				resul = existe.getString(1);
				strSQLD=" DELETE FROM  com_param_if  WHERE IC_IF = "+resul;
				con.ejecutaSQL(strSQLD);
			}
			strSQL ="INSERT INTO com_param_if (IC_IF,CG_FINANCIAMIENTO,CG_COMISION,FG_PORC_COMISION_DE,FG_PORC_COMISION_DM,FG_PORC_COMISION_VC,FG_PORC_COMISION_M2)"+
				" values("+ic_if+",'"+cg_financiamiento+"','"+cf_comision+"','"+fg_por_comision_de+"','"+fg_porc_comision_dm+"','"+fg_porc_comision_vc+"','"+fg_porc_comision_m2+"')";
			System.out.println("::::strSQL:::"+strSQL);
			con.ejecutaSQL(strSQL);
    	}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
			    con.cierraConexionDB();
			}
		}
		System.out.println("::guardaDatosComisionParamIf(S)..");
	    return resultado;
	}

/**
 * Guarda el porcentaje de comisi�n.
 * Actualmente se maneja el mismo porcentaje para todos los productos.
 * Y el producto 3 no aplica este porcentaje de comisi�n.
 * @param perfil es el usuario que va ejecutar el metodo ("ADMIN NAFIN"|"ADMIN BANCOMEXT") con el fin de
 * establecer una restriccion diferente para la query
 * @param fg_porc_comision_fondeo	Porcentaje de comisi�n
 * @exception	NafinException Si no es posible actualizar en la BD el
 * 		porcentaje de comision
 */
	public boolean guardarPorcentajeComision(String fg_porc_comision_fondeo, String perfil)
												throws NafinException{
		AccesoDB	con			= null;
		String 		strSQL		= "";
	   boolean		resultado	= true;
		String campoSegunPerfil = "";

		/*Verificar que perfil es*/
		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			campoSegunPerfil = "fg_porc_comision_fondeo";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			campoSegunPerfil = "fg_porc_comision_fondeo_bmx";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}

		try{
			con = new AccesoDB();
			con.conexionDB();

			strSQL =	"Update comcat_producto_nafin " +
						"	Set " + campoSegunPerfil + " = " + fg_porc_comision_fondeo +
						" Where ic_producto_nafin not in (3) ";

			//System.out.println(strSQL);
			con.ejecutaSQL(strSQL);

			System.out.println(".::guardarPorcentajeComision::" + strSQL + "::.");

    	}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
			    con.cierraConexionDB();
			}
		}
	    return true;
	}


/**
 * Guarda los porcentajes de comisi�n por rangos.
 * Actualmente se maneja el mismo porcentaje para todos los productos,
 * por lo cual el mismo rango ser� guardado para cada uno de los productos
 * Nafin registrados.
 * (Excepto para el producto 3 que no aplica este porcentaje de comisi�n.)
 * Este metodo asume que los rangos ya fueron validados previamente.
 *
 * @param comisiones	Listado de objetos ComisionPorRango, los cuales
 * 		contienen los datos de los rangos y la comision que aplica.
 *	@param perfil es el usuario que va ejecutar el metodo ("ADMIN NAFIN"|"ADMIN BANCOMEXT") con el fin de
 * establecer una restriccion diferente para la query
 *
 * @exception	NafinException Si no es posible actualizar en la BD el
 * 		porcentaje de comision
 */
	public void guardarComisionPorRangos(List comisiones, String perfil)
			throws NafinException{
		AccesoDB	con			= null;
		String 		strSQL		= "";
		ResultSet	rs			= null;
	   boolean		resultado	= true;
	   List productosNafin = new ArrayList();
		String bancoSegunPerfil = "";

		/*Verificar que perfil es*/
		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}

		try{
			con = new AccesoDB();
			con.conexionDB();

			strSQL = " delete from comrel_producto_comision " +
						" where ic_banco_fondeo =" + bancoSegunPerfil;
			con.ejecutaSQL(strSQL);

			strSQL = " SELECT ic_producto_nafin FROM comcat_producto_nafin " +
						" WHERE ic_producto_nafin != 3 ";
			rs = con.queryDB(strSQL);

			while (rs.next()) {
				int productoNafin = rs.getInt("ic_producto_nafin");
				productosNafin.add(new Integer(productoNafin));
			}
			rs.close();
			con.cierraStatement();

			strSQL = " insert into comrel_producto_comision " +
					" (ic_producto_nafin, ic_rango, fn_saldo_de, " +
					" fn_saldo_a, fn_porcentaje, ic_banco_fondeo) " +
					" values (?, ?, ?, ?, ?, ?)";

			PreparedStatement ps = con.queryPrecompilado(strSQL);

			Iterator itRangos = comisiones.iterator();
			int numRango = 0;

			while(itRangos.hasNext()) {	//Recorre los rangos
				ComisionPorRango comisionPorRango = (ComisionPorRango) itRangos.next();
				numRango++;

				Iterator itProductosNafin = productosNafin.iterator();
				while (itProductosNafin.hasNext()) {	//Recorre los productos
					int productoNafin = ((Integer) itProductosNafin.next()).intValue();
					ps.clearParameters();
					ps.setInt(1,productoNafin);
					ps.setInt(2,numRango);
					ps.setBigDecimal(3,comisionPorRango.getSaldoDe());
					ps.setBigDecimal(4,comisionPorRango.getSaldoA());
					ps.setBigDecimal(5,comisionPorRango.getComision());
					ps.setInt(6, Integer.parseInt(bancoSegunPerfil)); /*Aqui se debe definir para que perfil es(nafin|bancomext)*/

					ps.executeUpdate();
				}
			}

		} catch(SQLException e) {
			e.printStackTrace();
			resultado=false;
			throw new NafinException("SIST0001");
    	} catch(Exception e) {
    		e.printStackTrace();
			resultado = false;
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
			    con.cierraConexionDB();
			}
		}
	}

	/**
	 * Permite realizar la validaci�n de que no existan el registro dentro del cat�logo,
	 * utilizando el query regresado por el m�todo getQueryExistencia y
	 * getParametrosBind del objeto catalogo.
	 *
	 * @param catalogo Objeto que implementa la interfaz CatalogoMantenimiento
	 * @return true si es valida la inserci�n o false de lo contrario.
	 * 	Si getQueryExistencia() regresa vacio o nulo, entonces el m�todo
	 * 	siempre regresa true.

	 * @throws com.netro.exception.NafinException Si existe un error durante la
	 * 		validaci�n
	 */
	public boolean validarInexistenciaEnCatalogo(CatalogoMantenimiento catalogo) throws NafinException {
		System.out.println("ActualizacionCatalogosBean::validarInsercionEnCatalogo(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		String qrySentencia = "";
		List lVarBind = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (catalogo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			qrySentencia = catalogo.getQueryExistencia();
			if ( qrySentencia == null || qrySentencia.trim().equals("")) {
				return true;
			}
			lVarBind = catalogo.getParametrosBind();
			if ( lVarBind == null || lVarBind.size() == 0) {
				throw new Exception("Los valores para las variables bind son requeridos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"catalogo=" + "\n" + catalogo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			int numRegistros = con.existeDB(qrySentencia, lVarBind);

			//Si ya existe se regresa false, de lo contrario true
			return (numRegistros>0)?false:true;

		} catch(Exception e) {
			System.out.println("ActualizacionCatalogosBean::validarInsercionEnCatalogo(Exception) " + e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("ActualizacionCatalogosBean::validarInsercionEnCatalogo(S)");
		}
	}


	/**
	 * Permite realizar la inserci�n de un registro dentro del cat�logo,
	 * utilizando el query regresado por el m�todo getQueryInsertar y
	 * getParametrosBind del objeto catalogo.
	 * @param catalogo Objeto que implementa la interfaz CatalogoMantenimiento
	 * @throws com.netro.exception.NafinException Si existe un error durante la
	 * 		inserci�n
	 */
	public void insertarEnCatalogo(CatalogoMantenimiento catalogo) throws NafinException {
		System.out.println("ActualizacionCatalogosBean::insertarEnCatalogo(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		String qrySentencia = "";
		List lVarBind = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (catalogo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			qrySentencia = catalogo.getQueryInsertar();
			if ( qrySentencia == null || qrySentencia.trim().equals("")) {
				throw new Exception("El query de inserci�n es requerido para esta operaci�n");
			}
			lVarBind = catalogo.getParametrosBind();
			if ( lVarBind == null || lVarBind.size() == 0) {
				throw new Exception("Los valores para las variables bind son requeridos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"catalogo=" + "\n" + catalogo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			System.out.println("ActualizacionCatalogosBean::insertarEnCatalogo(Exception) " + e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("ActualizacionCatalogosBean::insertarEnCatalogo(S)");
		}
	}

  /**
   * Este metodo permite la insercion de el municipio con descripcion
   * "OTRO" para el estado nuevo insertado
   * @throws com.netro.exception.NafinException
   * @param catalogo
   */
 public void insertaMunicipio(CatalogoMantenimientoEstado catalogo)throws NafinException{
	System.out.println("ActualizacionCatalogosBean::insertarEnCatalogoMunic(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
    PreparedStatement ps = null;
    ResultSet rs = null;

    try {
			con.conexionDB();
			String clavePais  = catalogo.getClavePais();
      String nombreEstado = catalogo.getNombreEstado();
      String claveEstado = "";
      String sql = "Select ic_estado from comcat_estado where cd_nombre='"+nombreEstado+"' and ic_pais="+clavePais;
      ps = con.queryPrecompilado(sql);
      rs = ps.executeQuery();
      if(rs.next()){claveEstado = rs.getString(1);}


      String insert = " INSERT INTO comcat_municipio(ic_pais, ic_estado, ic_municipio, cd_nombre) " +
				" VALUES("+clavePais+","+claveEstado+",0,'OTRO')";
      ps = con.queryPrecompilado(insert);
      ps.executeUpdate();
      ps.close();rs.close();

		} catch(Exception e) {
			System.out.println("ActualizacionCatalogosBean::insertarEnCatalogo(Exception) " + e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("ActualizacionCatalogosBean::insertarEnCatalogoMunic(S)");
		}
	}

	/**
	 * Permite realizar la actualizaci�n de un registro dentro del cat�logo,
	 * utilizando el query regresado por el m�todo getQueryInsertar y
	 * getParametrosBind del objeto catalogo.
	 * @param catalogo Objeto que implementa la interfaz CatalogoMantenimiento
	 * @throws com.netro.exception.NafinException Si existe un error durante la
	 * 		inserci�n
	 */
	public void actualizarCatalogo(CatalogoMantenimiento catalogo) throws NafinException {
		System.out.println("ActualizacionCatalogosBean::actualizarCatalogo(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		String qrySentencia = "";
		List lVarBind = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (catalogo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			qrySentencia = catalogo.getQueryActualizar();
			if ( qrySentencia == null || qrySentencia.trim().equals("")) {
				throw new Exception("El query de actualizacion es requerido para esta operaci�n");
			}
			lVarBind = catalogo.getParametrosBind();
			if ( lVarBind == null || lVarBind.size() == 0) {
				throw new Exception("Los valores para las variables bind son requeridos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"catalogo=" + "\n" + catalogo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			System.out.println("ActualizacionCatalogosBean::actualizarCatalogo(Exception) " + e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("ActualizacionCatalogosBean::actualizarCatalogo(S)");
		}
	}


	/**
	 * Permite la obtenci�n de los datos de un registro dentro del cat�logo,
	 * por ejemplo, para la actualizaci�n de datos utilizando el m�todo
	 * getQueryRegistro y getParametrosBind del objeto catalogo.
	 * @param catalogo Objeto que implementa la interfaz CatalogoMantenimiento
	 * @return Objeto Registros con los datos del registro del catalogo
	 * @throws com.netro.exception.NafinException Si existe un error durante la
	 * 		inserci�n
	 */
	public Registros getRegistroCatalogo(CatalogoMantenimiento catalogo) throws NafinException {
		System.out.println("ActualizacionCatalogosBean::getRegistroCatalogo(E)");
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		List lVarBind = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (catalogo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			qrySentencia = catalogo.getQueryRegistro();
			if ( qrySentencia == null || qrySentencia.trim().equals("")) {
				throw new Exception("El query de registro es requerido para esta operaci�n");
			}
			lVarBind = catalogo.getParametrosBind();
			if ( lVarBind == null || lVarBind.size() == 0) {
				throw new Exception("Los valores para las variables bind son requeridos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"catalogo=" + "\n" + catalogo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			Registros reg = con.consultarDB(qrySentencia, lVarBind);
			return reg;
		} catch(Exception e) {
			System.out.println("ActualizacionCatalogosBean::getRegistroCatalogo(Exception) " + e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("ActualizacionCatalogosBean::getRegistroCatalogo(S)");
		}
	}


	/**
	 * Permite realizar la eliminaci�n de un registro dentro del cat�logo,
	 * utilizando el query regresado por el m�todo getQueryInsertar y
	 * getParametrosBind del objeto catalogo.
	 * @param catalogo Objeto que implementa la interfaz CatalogoMantenimiento
	 * @return Entero con el numero de registros modificados. Si es -1, significa
	 * 		que hubo un problema de SQL, siendo lo m�s com�n, un error de integridad
	 *       referencial
	 * @throws com.netro.exception.NafinException Si existe un error durante la
	 * 		inserci�n
	 */
	public int eliminarDelCatalogo(CatalogoMantenimiento catalogo) throws NafinException {
				System.out.println("ActualizacionCatalogosBean::eliminarDelCatalogo(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		String qrySentencia = "";
		List lVarBind = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (catalogo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			qrySentencia = catalogo.getQueryEliminar();
			if ( qrySentencia == null || qrySentencia.trim().equals("")) {
				throw new Exception("El query de eliminaci�n es requerido para esta operaci�n");
			}
			lVarBind = catalogo.getParametrosBind();
			if ( lVarBind == null || lVarBind.size() == 0) {
				throw new Exception("Los valores para las variables bind son requeridos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"catalogo=" + "\n" + catalogo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			int regAfectados = con.ejecutaUpdateDB(qrySentencia, lVarBind);
			return regAfectados;
		} catch(SQLException e) {
			System.out.println("ActualizacionCatalogosBean::eliminarDelCatalogo(Exception) " + e);
			bOk = false;
			e.printStackTrace();
			return -1;
		} catch(Exception e) {
			System.out.println("ActualizacionCatalogosBean::eliminarDelCatalogo(Exception) " + e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("ActualizacionCatalogosBean::eliminarDelCatalogo(S)");
		}
	}

/**
 * Obtiene la definici�n del ajuste anual para el IF especificada
 * param ic_if Clave de la IF de la que se desea obtener el parametro de
 * 		ajuste anual
 *	@param perfil es el usuario que va ejecutar el metodo ("ADMIN NAFIN"|"ADMIN BANCOMEXT") con el fin de
 * establecer una restriccion diferente para la query
 * @return Listado con los sig. datos (Los valores son String y nunca son nulos)
 * 		Indice 0.- ig_mes_ajuste_de. (Mes de ajuste. Limite inferior del rango)
 * 		Indice 1.- ig_mes_ajuste_a. (Mes de ajuste. Limite superior del rango)
 * 		Indice 2.- ig_anio_ajuste. (A�o del ajuste)
 * @exception	NafinException Si hay un error en la BD
 */
	public List consultarAjusteAnual(String ic_if, String perfil)
			throws NafinException{

		AccesoDB	con			= null;
		String 		strSQL		= "";
		ResultSet	rs			= null;
	   boolean		resultado	= true;
	   List ajusteAnual = new ArrayList();
		String bancoSegunPerfil = "";
		String campo1 = "";
		String campo2 = "";
		String campo3 = "";

		/*Verificar que perfil es*/
		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			campo1 = " ci.ig_mes_ajuste_de, ";
			campo2 = " ci.ig_mes_ajuste_a, ";
			campo3 = " ci.ig_anio_ajuste ";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			campo1 = " ci.ig_mes_ajuste_de_bmx, ";
			campo2 = " ci.ig_mes_ajuste_a_bmx, ";
			campo3 = " ci.ig_anio_ajuste_bmx ";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}


		try{
			con = new AccesoDB();
			con.conexionDB();

		/*Consulta anterior
		 * strSQL = " SELECT ig_mes_ajuste_de, ig_mes_ajuste_a, " +
					" ig_anio_ajuste " +
					" FROM comcat_if " +
					" WHERE ic_if = " + ic_if;*/

		strSQL = "SELECT distinct " + campo1 + campo2 + campo3 +
					"From comcat_if ci , comrel_if_epo_x_producto rie, comcat_epo epo, comcat_banco_fondeo cbf " +
					"WHERE ci.ic_if = " + ic_if +
					" And rie.ic_if = ci.ic_if " +
					"And rie.ic_epo = epo.ic_epo " +
					"And epo.ic_banco_fondeo = cbf.ic_banco_fondeo " +
					"And cbf.ic_banco_fondeo = " + bancoSegunPerfil;

		//System.out.println(".::consultarAjusteAnual::strSQL" + strSQL + "::.");

			rs = con.queryDB(strSQL);

			String ig_mes_ajuste_de = "";
			String ig_mes_ajuste_a = "";
			String ig_anio_ajuste = "";

			if(rs.next()) {
				ig_mes_ajuste_de =
						(rs.getString(1) == null)?"":
						rs.getString(1);

				ig_mes_ajuste_a =
						(rs.getString(2) == null)?"":
						rs.getString(2);

				ig_anio_ajuste =
						(rs.getString(3) == null)?"":
						rs.getString(3);
			}
			ajusteAnual.add(0, ig_mes_ajuste_de);
			ajusteAnual.add(1, ig_mes_ajuste_a);
			ajusteAnual.add(2, ig_anio_ajuste);

			rs.close();
			con.cierraStatement();
		} catch(SQLException e) {
			e.printStackTrace();
			resultado=false;
			throw new NafinException("SIST0001");
    	} catch(Exception e) {
    		e.printStackTrace();
			resultado = false;
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
			    con.cierraConexionDB();
			}
		}
		return ajusteAnual;
	}

/**
 * Obtiene los porcentajes de comisi�n por rangos.
 * Actualmente se maneja el mismo porcentaje para todos los productos,
 * por lo cual la comision es la misma para cualquiera de los productos
 * Nafin registrados.
 * (Excepto para el producto 3 que no aplica este porcentaje de comisi�n.)
 *
 *	 @param perfil es el usuario que va ejecutar el metodo ("ADMIN NAFIN"|"ADMIN BANCOMEXT") con el fin de
 *  establecer una restriccion diferente para la query
 *
 * @return Listado de objetos ComisionPorRango, los cuales
 * 		contienen los datos de los rangos y la comision que aplica.
 * 		Nunca regresa null.
 * @exception	NafinException Si hay un error en la BD
  */
	public List consultarComisionPorRangos(String perfil)
			throws NafinException{
		AccesoDB	con			= null;
		String 		strSQL		= "";
		ResultSet	rs			= null;
	   boolean		resultado	= true;
	   List comisiones = new ArrayList();
		String bancoSegunPerfil = "";

		/*Verificar que perfil es*/
		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}

		try{
			con = new AccesoDB();
			con.conexionDB();

			strSQL = "SELECT ic_producto_nafin, fn_saldo_de, fn_saldo_a, fn_porcentaje " +
						"FROM comrel_producto_comision " +
						"WHERE ic_producto_nafin = 1 " +
						"AND ic_banco_fondeo = " + bancoSegunPerfil +
						"ORDER BY ic_rango ";

			rs = con.queryDB(strSQL);

			while (rs.next()) {
				ComisionPorRango comisionPorRango = new ComisionPorRango();
				comisionPorRango.setSaldoDe(rs.getString("fn_saldo_de"));
				comisionPorRango.setSaldoA(rs.getString("fn_saldo_a"));
				comisionPorRango.setComision(rs.getString("fn_porcentaje"));

				comisiones.add(comisionPorRango);
			}
			rs.close();
			con.cierraStatement();

		} catch(SQLException e) {
			e.printStackTrace();
			resultado=false;
			throw new NafinException("SIST0001");
    	} catch(Exception e) {
    		e.printStackTrace();
			resultado = false;
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
			    con.cierraConexionDB();
			}
		}
		return comisiones;
	}





	public boolean guardaDetalleComision(String cs_tipo_fondeo,
			String cg_tipo_comision,
			String cs_recorte_solic,
			String ic_if,
			String ic_epo,
			String ic_producto_nafin,
			String perfil) throws NafinException {
		AccesoDB	con			= null;
		String 		strSQL		= "";
	   boolean		resultado	= true;
		String bancoSegunPerfil = "";

		/*Verificar que perfil es*/
		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}

		try{
			con = new AccesoDB();
			con.conexionDB();

			/*Consulta anterior
			 * strSQL =	" UPDATE comrel_if_epo_x_producto " +
						" SET cs_tipo_fondeo = '" + cs_tipo_fondeo + "', " +
						" cg_tipo_comision = '" + cg_tipo_comision + "', "+
						" cs_recorte_solic = '" + cs_recorte_solic + "' "+
						" WHERE ic_if = " + ic_if +
						"	And ic_epo = " + ic_epo +
						"	And ic_producto_nafin = " + ic_producto_nafin;*/

				strSQL = "	UPDATE comrel_if_epo_x_producto SET cs_tipo_fondeo = '" +  cs_tipo_fondeo + "', cg_tipo_comision = '" + cg_tipo_comision + "', cs_recorte_solic = '" + cs_recorte_solic + "' "+
							" 	WHERE ic_if = " + ic_if +
							" 	AND ic_if IN(SELECT ic_if FROM comcat_if) " +
							"	AND ic_epo IN(SELECT ic_epo " +
							"	              FROM comcat_epo " +
							"	              WHERE ic_banco_fondeo IN(SELECT ic_banco_fondeo " +
							"	                                       FROM comcat_banco_fondeo " +
							"  	                                     WHERE ic_banco_fondeo = " + bancoSegunPerfil + ")) " +
							"	AND ic_epo = " + ic_epo +
							"	AND ic_producto_nafin = " + ic_producto_nafin;

							System.out.println(".::guardaDetalleComision::strSQL" + strSQL + "::.");

			con.ejecutaSQL(strSQL);

    	}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
			    con.cierraConexionDB();
			}
		}
	    return true;
	}


	/**
	 * Consulta de los datos correspondientes a las comisiones.
	 * @param perfil es el usuario que va ejecutar el metodo ("ADMIN NAFIN"|"ADMIN BANCOMEXT") con el fin de
	 * establecer una restriccion diferente para la query
	 * @return Regresa un Vector con los sig datos:
	 * 		(todos los valores son de tipo String)
	 * 		0.- Razon Social del IF
	 * 		1.- Clave del IF
	 * 		2.- Tipo de Fondeo
	 * 		3.- Mes ajuste de
	 *		4.- Mes ajuste a
	 *		5.- A�o de ajuste
	 */
	public Vector consultaComision(String perfil)
		throws NafinException{

		AccesoDB	con 			= null;
		String		strSQL			= "";
		String		cg_razon_social	= "";
		String		ic_if			= "";
		String		cs_tipo_fondeo	= "";
		String		strFondeo		= "";
		ResultSet	rs				= null;
		ResultSet	rs2				= null;
		Vector		vecFilas		= new Vector();
		String bancoSegunPerfil = "";
		String condicion = " And cbf.ic_banco_fondeo = ";

		/*Verificar que perfil es*/
		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}

		try {
			con = new AccesoDB();
			con.conexionDB();

			strSQL =	"Select distinct ci.ic_if, ci.cg_razon_social " +
						"From comcat_if ci , comrel_if_epo_x_producto rie, comcat_epo epo, comcat_banco_fondeo cbf " +
						"Where ci.cs_habilitado = 'S' " +
						"And rie.ic_if = ci.ic_if " +
						"And rie.ic_epo = epo.ic_epo " +
						"And epo.ic_banco_fondeo = cbf.ic_banco_fondeo " +
							condicion + bancoSegunPerfil +
						"Order by cg_razon_social ";
			rs = con.queryDB(strSQL);

			while (rs.next()){
				cg_razon_social = rs.getString("cg_razon_social");
				ic_if = rs.getString("ic_if");

				strFondeo = "SinValor";
				cs_tipo_fondeo = "N";

				Vector	vecColumnas	= new Vector();

				strSQL =	"Select rie.cs_tipo_fondeo " +
							"  From comcat_if ci, " +
							"       comrel_if_epo_x_producto rie, comcat_epo epo, comcat_banco_fondeo cbf " +
							" Where ci.ic_if = " + ic_if +
							"   And rie.ic_if = ci.ic_if " +
							"   And rie.cs_habilitado = 'S' " +
							"   And rie.ic_producto_nafin not in (3) "+
							"	 And rie.ic_epo = epo.ic_epo " +
							"	 And cbf.ic_banco_fondeo = epo.ic_banco_fondeo " +
								condicion + bancoSegunPerfil;

				//System.out.println(".::consultaComision::." + strSQL);

				rs2 = con.queryDB(strSQL);

				while (rs2.next()){
					cs_tipo_fondeo = (rs2.getString("cs_tipo_fondeo") == null) ? "" : rs2.getString("cs_tipo_fondeo");

					if (strFondeo.equals("SinValor"))
						strFondeo = cs_tipo_fondeo;
					else{
						if (!strFondeo.equals(cs_tipo_fondeo)){
							cs_tipo_fondeo = "A";
							break;
						}
					}
				}

				rs2.close();
				con.cierraStatement();

				vecColumnas.add(0, cg_razon_social);
				vecColumnas.add(1, ic_if);
				vecColumnas.add(2, cs_tipo_fondeo);

				List ajuste = consultarAjusteAnual(ic_if, perfil);
				//Agrega 3 columnas mas:
				//3.-mes ajuste de
				//4.-mes ajuste a
				//5.-anio de ajuste
				vecColumnas.addAll(3, ajuste);
				vecFilas.addElement(vecColumnas);
			}

			rs.close();
			con.cierraStatement();

		}catch(Exception e){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

		return vecFilas;
	}

	/**
	 *
	 */
	public String consultarPorcentajeComision(String perfil)
		throws NafinException{

		AccesoDB	con 					= null;
		String		strSQL					= "";
		String		fg_porc_comision_fondeo	= "";
		ResultSet	rs						= null;
		String campoPorcComSegunPer = "";

		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			campoPorcComSegunPer = "fg_porc_comision_fondeo";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			campoPorcComSegunPer = "fg_porc_comision_fondeo_bmx";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}


		try {
			con = new AccesoDB();
			con.conexionDB();

			strSQL =	"Select " + campoPorcComSegunPer +
						" From comcat_producto_nafin ";

			rs = con.queryDB(strSQL);

			System.out.println(".::consultarPorcentajeComision::" + strSQL + "::.");

			if(rs.next())
				fg_porc_comision_fondeo = rs.getString(campoPorcComSegunPer);

			rs.close();
			con.cierraStatement();

		}catch(Exception e){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

		return fg_porc_comision_fondeo;
	}

	public Vector consultaDetalleComision(String ic_if,
										  String ic_producto_nafin, String perfil)
											throws NafinException{

		AccesoDB	con 			= null;
		String		strSQL			= "";
		String		cg_razon_social	= "";
		String		ic_epo			= "";
		String		cs_tipo_fondeo	= "";
		String		cg_tipo_comision= "";
		String		cs_recorte_solic = "";
		ResultSet	rs				= null;
		Vector		vecFilas		= new Vector();
		String bancoSegunPerfil = "";

		/*Verificar que perfil es*/
		if(perfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			System.out.println(".::Peril del usuario admin nafin::.");
		}
		if(perfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			System.out.println(".::Peril del usuario admin bancomext::.");
		}

		try {
			con = new AccesoDB();
			con.conexionDB();

			/*Consulta Anterior
			 * strSQL =	"Select riexp.ic_epo, " +
						"       ce.cg_razon_social, " +
						"		riexp.cs_tipo_fondeo, " +
						"		riexp.cg_tipo_comision, " +
						"		riexp.cs_recorte_solic " +
						"  From comcat_epo ce, " +
						"		comrel_if_epo_x_producto riexp " +
						" Where riexp.ic_if = " + ic_if +
						"	And riexp.cs_habilitado = 'S' " +
						"	And riexp.ic_producto_nafin = " + ic_producto_nafin +
						"	And ce.ic_epo = riexp.ic_epo " +
						" Order by " +
						"		ce.cg_razon_social";*/

						strSQL = "Select rie.ic_epo, epo.cg_razon_social, rie.cs_tipo_fondeo, rie.cg_tipo_comision, rie.cs_recorte_solic " +
									"From comcat_epo epo, comrel_if_epo_x_producto rie, comcat_if ci, comcat_banco_fondeo cbf " +
									"Where rie.ic_if = " + ic_if +
									" And rie.cs_habilitado = 'S' " +
									"And rie.ic_producto_nafin = "  + ic_producto_nafin +
									" And epo.ic_epo = rie.ic_epo " +
									"And rie.ic_if = ci.ic_if " +
									"And epo.ic_banco_fondeo = cbf.ic_banco_fondeo " +
									"And cbf.ic_banco_fondeo = " + bancoSegunPerfil +
									" Order by epo.cg_razon_social";

									System.out.println(".::consultaDetalleComision::strSQL:" + strSQL + "::.");

			rs = con.queryDB(strSQL);

			while(rs.next()){
				Vector	vecColumnas	= new Vector();

				cg_razon_social = rs.getString("cg_razon_social");
				ic_epo = rs.getString("ic_epo");
				cs_tipo_fondeo = rs.getString("cs_tipo_fondeo");
				cg_tipo_comision = rs.getString("cg_tipo_comision");
				cs_recorte_solic = rs.getString("cs_recorte_solic");

				vecColumnas.addElement(cg_razon_social);
				vecColumnas.addElement(ic_epo);
				vecColumnas.addElement(cs_tipo_fondeo);
				vecColumnas.addElement(cg_tipo_comision);
				vecColumnas.addElement(cs_recorte_solic);

				vecFilas.addElement(vecColumnas);
			}

			rs.close();
			con.cierraStatement();

		}catch(Exception e){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

		return vecFilas;
	}


 /**
 * Realiza actualizacion de datos en el catalogo Tipo de cambio (peso/dolar).
 * sobre la tabla com_tipo_cambio
 * @param no requiere parametros
 * @exception	NafinException (Si hay un error en la BD)
 */
	 public void execProcActualizaTipoCambio()throws NafinException
	  {

	    AccesoDB lobdConexion = new AccesoDB();

	    try{
	      lobdConexion.conexionDB();
	      CallableStatement cstm = null;

	      String sqlExecute = "sp_sincroniza_tipo_cambio";

	      System.out.println("Ejecucion (E) :sp_sincroniza_tipo_cambio :..");
        cstm = lobdConexion.ejecutaSP(sqlExecute);
        cstm.executeUpdate();

        System.out.println("Ejecucion (S) :sp_sincroniza_tipo_cambio ::");
	      cstm.close();

	    }
	    catch(Exception e) {
				throw new NafinException("SIST0001");
			}
	    finally{
	      if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
	    }

	}
/**
 *@ Realiza actualizacion de datos del catalogo de ciudades.
 *@ sobre la tabla comcat_ciudad la cual se actualiza apartir de la
 *@ tabla de mg_ciudades1 que procede del sistema de Sirac
 *@exception	NafinException
 *@autor Deysi Laura Hern�ndez Contreras
 */
	private boolean ciudad(AccesoDB lobdConexion) throws NafinException {
	String lsQry = "";
	boolean lbOK = true;
  System.out.println("catalogo de  ciudad (E) ");

		try {
			lsQry = "DELETE FROM  comcat_ciudad ";

			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Insertar en el Cat�logo ciudad. "+sqle.getMessage());
			}

				lsQry = "INSERT INTO "+
              " comcat_ciudad (CODIGO_PAIS, CODIGO_DEPARTAMENTO, CODIGO_CIUDAD, NOMBRE) "+
              "( SELECT CODIGO_PAIS, CODIGO_DEPARTAMENTO, CODIGO_CIUDAD, NOMBRE FROM MG_CIUDADES1) ";

			System.out.println("ActualizacionCatalogosBean::Ciudad::lsQry:"+lsQry+"*");
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				System.out.println("Error al Actualizar el Cat�logo Ciudad. "+sqle.getMessage());
			}

		} catch(Exception e){
			lbOK = false;
			System.out.println("Error al Actualizar el Cat�logo ciudad. "+e);
			throw new NafinException("SIST0001");
		}

		finally	{
    System.out.println(" catalogo de  ciudad (S) ");
			lobdConexion.terminaTransaccion(lbOK);
		}
	return lbOK;
	} //pais


}// Fin del Bean
