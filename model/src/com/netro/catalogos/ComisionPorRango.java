package com.netro.catalogos;

import java.io.*;
import java.math.*;

/**
 * Esta clase es un Bean que permite manejar una comisión por
 * rango epescificado.
 * Dado que la implementación de este bean requiere de gran precisión
 * no se emplean ni float ni double y se optó por recibir los parametros
 * como String, sin embargo los valores de retorno son BigDecimal
 * @author Gilberto Aparicio
 */
public class ComisionPorRango implements Serializable {
	public ComisionPorRango() {}
	
	/**
	 * Permite establecer el saldo inferior que conforma el rango.
	 */
	public void setSaldoDe(String saldoDe) {
		this.saldoDe = new BigDecimal(saldoDe);
	}
	
	/**
	 * Permite establecer el saldo superior que conforma el rango.
	 */
	public void setSaldoA(String saldoA) {
		this.saldoA = new BigDecimal(saldoA);
	}
	
	/**
	 * Permite establecer la comisión que corresponde al rango especificado.
	 */
	public void setComision(String comision) {
		this.comision = new BigDecimal(comision);
	}
	
	/**
	 * Obtiene el saldo inferior del rango
	 */
	public BigDecimal getSaldoDe() {
		return this.saldoDe;
	}

	/**
	 * Obtiene el saldo superior del rango
	 */
	public BigDecimal getSaldoA() {
		return this.saldoA;
	}
	
	/**
	 * Obtiene la comisión correspondiente al rango especificado
	 */
	public BigDecimal getComision() {
		return this.comision;
	}
	
	
	public String toString() {
		String str = "[" + this.saldoDe.toPlainString() + " - " + this.saldoA.toPlainString() + "] -> " + 
				comision.toPlainString() + "%";
		return str;
	}
	
	
	private BigDecimal saldoDe;
	private BigDecimal saldoA;
	private BigDecimal comision;
}