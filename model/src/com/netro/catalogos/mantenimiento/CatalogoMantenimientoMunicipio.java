package com.netro.catalogos.mantenimiento;
import java.util.ArrayList;
import java.util.List;

/**
 * Mantenimiento del cat�logo de Municipios
 * @author Gilberto Aparicio
 */

public class CatalogoMantenimientoMunicipio 
		implements java.io.Serializable, CatalogoMantenimiento {

	private String clavePais;
	private String claveEstado;
	private String claveMunicipio;
	private String nombreMunicipio;
	
	private List parametrosBind = new ArrayList();

	public CatalogoMantenimientoMunicipio(){}
	
	/**
	 * M�todo para obtener el query que permite la inserci�n de un 
	 * registro en el cat�logo. 
	 * 
	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * @return Cadena con la instruccion SQL, para insertar
	 */
	public String getQueryInsertar() {
		String qrySentencia = 
				" INSERT INTO comcat_municipio(ic_pais, ic_estado, ic_municipio, cd_nombre) " +
				" VALUES(?,?,(SELECT NVL(MAX(ic_municipio)+1,1) FROM comcat_municipio WHERE ic_pais = ? AND ic_estado = ?), UPPER(?))";
		
		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(new Integer(this.getClaveEstado()));
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(new Integer(this.getClaveEstado()));
		parametrosBind.add(this.getNombreMunicipio());
		return qrySentencia;
	}

	/**
	 * M�todo para obtener el query que permite determinar si el registro
	 * que se desea insertar/actualizar es valido
	 * 
	 * La instrucci�n de SQL debe ser una consulta que indique el numero 
	 * de registros que existen (count). Adem�s debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo
	 * {@link #getParametrosBind() getParametrosBind}
	 * @return Cadena con la instrucci�n SQL, para validar existencia
	 */
	public String getQueryExistencia() {
		StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append( 
				" SELECT  count(*) " +
				" FROM comcat_municipio " +
				" WHERE ic_pais = ? AND ic_estado = ? AND cd_nombre = UPPER(?) ");

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(new Integer(this.getClaveEstado()));
		parametrosBind.add(this.getNombreMunicipio());

		//Condiciones especiales
		if (this.getClaveMunicipio() != null && !this.getClaveMunicipio().equals("")) {
			//Esta condici�n aplica para las actualizaciones
			qrySentencia.append( " AND ic_municipio <> ? ");
			parametrosBind.add(new Integer(this.getClaveMunicipio()));
		}


		return qrySentencia.toString();
	}

	/**
	 * M�todo para obtener el query que permite la actualizaci�n de un 
	 * registro en el cat�logo
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL, para actualizar
	 */
	public String getQueryActualizar() {
		String qrySentencia = 
				" UPDATE comcat_municipio " +
				" SET cd_nombre = UPPER(?) " + 
				" WHERE ic_pais = ? AND ic_estado = ? AND ic_municipio = ? ";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(this.getNombreMunicipio());
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(new Integer(this.getClaveEstado()));
		parametrosBind.add(new Integer(this.getClaveMunicipio()));

		return qrySentencia;
	}

	/**
	 * M�todo para obtener el query que permite la eliminaci�n de un 
	 * registro en el cat�logo
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL, para eliminar
	 */
	public String getQueryEliminar() {
		String qrySentencia = 
				" DELETE FROM comcat_municipio " +
				" WHERE ic_pais = ? AND ic_estado = ? AND ic_municipio = ? ";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(new Integer(this.getClaveEstado()));
		parametrosBind.add(new Integer(this.getClaveMunicipio()));

		return qrySentencia;
	}
	
	/**
	 * M�todo para obtener el query que permite mostrar los datos
	 * de un registro a modificar.
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL.
	 */
	public String getQueryRegistro(){
		String qrySentencia = 
				" SELECT m.ic_pais, m.ic_estado, m.ic_municipio, " +
				" p.cd_descripcion as nombrePais, e.cd_nombre as nombreEstado, " +
				" m.cd_nombre as nombreMunicipio " + 
				" FROM comcat_pais p, comcat_estado e, comcat_municipio m " +
				" WHERE " +
				" m.ic_pais = p.ic_pais AND m.ic_estado = e.ic_estado " +
				" AND m.ic_pais = ? AND m.ic_estado = ? AND m.ic_municipio = ? ";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(new Integer(this.getClaveEstado()));
		parametrosBind.add(new Integer(this.getClaveMunicipio()));


		return qrySentencia;
	}

	
	/**
	 * M�todo para obtener los valores para cada uno de las variables bind
	 * de la instrucci�n de SQL.
	 * Los valores deberan de estar acorde a la operaci�n que se realice
	 * (inserci�n, actualizacion, eliminaci�n)
	 * @return Lista con los valores de las variables bind.
	 */
	public List getParametrosBind() {
		return this.parametrosBind;
	}

	/**
	 * Obtiene la clave del Pais al que pertenece el Municipio
	 * @return Cadena con la clave del pais
	 */
	public String getClavePais()
	{
		return clavePais;
	}

	/**
	 * Establece la clave del Pais al que pertenece el Municipio
	 * @param clavePais Cadena con la clave del pais
	 */
	public void setClavePais(String clavePais)
	{
		this.clavePais = clavePais;
	}
	/**
	 * Obtiene la clave del Estado al que pertenece el Municipio
	 * @return Cadena con la clave del estado
	 */
	public String getClaveEstado()
	{
		return claveEstado;
	}

	/**
	 * Establece la clave del Estado al que pertenece el Municipio
	 * @param claveEstado Cadena con la clave del estado
	 */
	public void setClaveEstado(String claveEstado)
	{
		this.claveEstado = claveEstado;
	}
	/**
	 * Obtiene la clave del Municipio
	 * @return Cadena con la clave del municipio
	 */
	public String getClaveMunicipio()
	{
		return claveMunicipio;
	}

	/**
	 * Establece la clave del Municipio
	 * @param claveMunicipio Cadena con la clave del municipio
	 */
	public void setClaveMunicipio(String claveMunicipio)
	{
		this.claveMunicipio = claveMunicipio;
	}

	/**
	 * Obtiene el nombre del Municipio
	 * @return Cadena con el nombre del municipio
	 */
	public String getNombreMunicipio()
	{
		return nombreMunicipio;
	}

	/**
	 * Establece el nombre del Municipio
	 * @param  nombreMunicipio Cadena con el nombre del municipio
	 */
	public void setNombreMunicipio(String nombreMunicipio)
	{
		this.nombreMunicipio = nombreMunicipio;
	}

}