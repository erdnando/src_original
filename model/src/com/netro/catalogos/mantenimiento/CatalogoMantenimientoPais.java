package com.netro.catalogos.mantenimiento;
import java.util.ArrayList;
import java.util.List;

/**
 * Mantenimiento del cat�logo de Pais
 * @author Gilberto Aparicio
 */
public class CatalogoMantenimientoPais implements java.io.Serializable, CatalogoMantenimiento {

	private String clavePais;
	private String nombrePais;
	private List parametrosBind = new ArrayList();

	public CatalogoMantenimientoPais() { }
	
	/**
	 * M�todo para obtener el query que permite la inserci�n de un 
	 * registro en el cat�logo. 
	 * 
	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * @return Cadena con la instruccion SQL, para insertar
	 */
	public String getQueryInsertar() {
		String qrySentencia = 
				" INSERT INTO comcat_pais(ic_pais, cd_descripcion) " +
				" VALUES((SELECT NVL(MAX(ic_pais)+1,1) FROM comcat_pais), ?)";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(this.getNombrePais());
		return qrySentencia;
	}

	/**
	 * M�todo para obtener el query que permite determinar si el registro
	 * que se desea insertar/actualizar es valido
	 * 
	 * La instrucci�n de SQL debe ser una consulta que indique el numero 
	 * de registros que existen (count). Adem�s debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo
	 * {@link #getParametrosBind() getParametrosBind}
	 * @return Cadena con la instrucci�n SQL, para validar existencia
	 */
	public String getQueryExistencia() {
		StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
				" SELECT  count(*) " +
				" FROM comcat_pais " +
				" WHERE cd_descripcion = UPPER(?) ");

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(this.getNombrePais());
		
		//Condiciones especiales
		if (this.getClavePais() != null && !this.getClavePais().equals("")) {
			//Esta condici�n aplica para las actualizaciones
			qrySentencia.append( " AND ic_pais <> ? ");
			parametrosBind.add(new Integer(this.getClavePais()));
		}

		
		return qrySentencia.toString();
	}


	/**
	 * M�todo para obtener el query que permite la actualizaci�n de un 
	 * registro en el cat�logo
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL, para actualizar
	 */
	public String getQueryActualizar() {
		String qrySentencia = 
				" UPDATE comcat_pais " +
				" SET cd_descripcion = ? " +
				" WHERE ic_pais = ? ";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(this.getNombrePais());
		parametrosBind.add(new Integer(this.getClavePais()));
		return qrySentencia;
	}

	/**
	 * M�todo para obtener el query que permite la eliminaci�n de un 
	 * registro en el cat�logo
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL, para eliminar
	 */
	public String getQueryEliminar() {
		String qrySentencia = 
				" DELETE FROM comcat_pais " +
				" WHERE ic_pais = ? ";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));
		return qrySentencia;
	}


	/**
	 * M�todo para obtener el query que permite mostrar los datos
	 * de un registro a modificar.
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL.
	 */
	public String getQueryRegistro(){
		String qrySentencia = 
				" SELECT p.ic_pais, p.cd_descripcion as nombrePais " + 
				" FROM comcat_pais p " +
				" WHERE p.ic_pais = ? ";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));

		return qrySentencia;
	}

	
	/**
	 * M�todo para obtener los valores para cada uno de las variables bind
	 * de la instrucci�n de SQL.
	 * Los valores deberan de estar acorde a la operaci�n que se realice
	 * (inserci�n, actualizacion, eliminaci�n)
	 * @return Lista con los valores de las variables bind.
	 */
	public List getParametrosBind() {
		return this.parametrosBind;
	}
	/**
	 * Obtiene la clave del Pais al que pertenece el Municipio
	 * @return Cadena con la clave del pais
	 */
	public String getClavePais()
	{
		return clavePais;
	}
	/**
	 * Establece la clave del Pais al que pertenece el Municipio
	 * @param clavePais Cadena con la clave del pais
	 */
	public void setClavePais(String clavePais)
	{
		this.clavePais = clavePais;
	}
	/**
	 * Obtiene el nombre del Pais
	 * @return Cadena con el nombre del pais
	 */
	public String getNombrePais()
	{
		return nombrePais;
	}
	/**
	 * Establece el nombre del Pais
	 * @param  nombrePais Cadena con el nombre del pais
	 */
	public void setNombrePais(String nombrePais)
	{
		this.nombrePais = nombrePais;
	}
}