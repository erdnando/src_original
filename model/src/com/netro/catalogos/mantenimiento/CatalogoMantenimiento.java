package com.netro.catalogos.mantenimiento;

import java.util.List;

/**
 * Interfaz para definir la estructura de las clases que se utilicen
 * para el mantenimiento de Catalogos (Inserci�n, Actualizaci�n, Eliminaci�n)
 * Las clases que implementen esta interfaz, ser�n utilizadas como parametro
 * de los m�todos en el EJB ActualizacionCatalogosEJB:
 * {@link com.netro.catalogos.ActualizacionCatalogosBean#insertarEnCatalogo(CatalogoMantenimiento) insertarEnCatalogo},
 * {@link com.netro.catalogos.ActualizacionCatalogosBean#actualizarCatalogo(CatalogoMantenimiento) actualizarCatalogo},
 * {@link com.netro.catalogos.ActualizacionCatalogosBean#eliminarCatalogo(CatalogoMantenimiento) eliminarDelCatalogo}
 *
 * IMPORTANTE: Las clases que implementen esta clasa deben marcarse como
 * Serializable para que sea posible su uso como par�metro del EJB.
 *
 * Los queries que permiten la inserci�n, actualizaci�n y eliminaci�n, deberan
 * manejar variables bind. De manera que sea posible su ejecuci�n a trav�s
 * de el m�todo {@link netropology.utilerias.AccesoDB#ejecutaUpdateDB(String, List) ejecutaUpdateDB}
 *
 * @author Gilberto Aparicio
 * @since Fodea 018-2008
 */
public interface CatalogoMantenimiento extends java.io.Serializable{

	/**
	 * M�todo para obtener el query que permite la inserci�n de un 
	 * registro en el cat�logo. 
	 * 
	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * @return Cadena con la instruccion SQL, para insertar
	 */
	public String getQueryInsertar();
	
	
	/**
	 * M�todo para obtener el query que permite determinar si el registro
	 * que se desea insertar/actualizar es valido
	 * 
	 * La instrucci�n de SQL debe ser una consulta que indique el numero 
	 * de registros que existen (count). Adem�s debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo
	 * {@link #getParametrosBind() getParametrosBind}
	 * @return Cadena con la instrucci�n SQL, para validar existencia
	 */
	public String getQueryExistencia();

	/**
	 * M�todo para obtener el query que permite la actualizaci�n de un 
	 * registro en el cat�logo
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL, para actualizar
	 */
	public String getQueryActualizar();

	/**
	 * M�todo para obtener el query que permite la eliminaci�n de un 
	 * registro en el cat�logo
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL, para eliminar
	 */
	public String getQueryEliminar();


	/**
	 * M�todo para obtener el query que permite mostrar los datos
	 * de un registro a modificar.
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL.
	 */
	public String getQueryRegistro();

	
	/**
	 * M�todo para obtener los valores para cada uno de las variables bind
	 * de la instrucci�n de SQL.
	 * Los valores deberan de estar acorde a la operaci�n que se realice
	 * (inserci�n, actualizacion, eliminaci�n)
	 * @return Lista con los valores de las variables bind.
	 */
	public List getParametrosBind();
}