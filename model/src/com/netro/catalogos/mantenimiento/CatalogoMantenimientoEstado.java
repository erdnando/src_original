package com.netro.catalogos.mantenimiento;
import java.util.ArrayList;
import java.util.List;

public class CatalogoMantenimientoEstado 
		implements java.io.Serializable, CatalogoMantenimiento {
	
	public CatalogoMantenimientoEstado(){}

	private String clavePais;
	private String claveEstado;
	private String nombreEstado;
	
	private List parametrosBind = new ArrayList();

	/**
	 * M�todo para obtener el query que permite la inserci�n de un 
	 * registro en el cat�logo. 
	 * 
	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * @return Cadena con la instruccion SQL, para insertar
	 */
	public String getQueryInsertar() {
		//Para el caso de estado, la clave es un consecutivo general, no se reinicia por pais.
		String qrySentencia = 
				" INSERT INTO comcat_estado(ic_pais, ic_estado, cd_nombre) " +
				" VALUES(?, (SELECT NVL(MAX(ic_estado)+1,1) FROM comcat_estado), ?)";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(this.getNombreEstado());
		return qrySentencia;
	}

	/**
	 * M�todo para obtener el query que permite determinar si el registro
	 * que se desea insertar/actualizar es valido
	 * 
	 * La instrucci�n de SQL debe ser una consulta que indique el numero 
	 * de registros que existen (count). Adem�s debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo
	 * {@link #getParametrosBind() getParametrosBind}
	 * @return Cadena con la instrucci�n SQL, para validar existencia
	 */
	public String getQueryExistencia() {
		StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
				" SELECT  count(*) " +
				" FROM comcat_estado " +
				" WHERE ic_pais = ? AND cd_nombre = UPPER(?) ");

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(this.getNombreEstado());
		
		
		//Condiciones especiales
		if (this.getClaveEstado() != null && !this.getClaveEstado().equals("")) {
			//Esta condici�n aplica para las actualizaciones
			qrySentencia.append( " AND ic_estado <> ? ");
			parametrosBind.add(new Integer(this.getClaveEstado()));
		}

		return qrySentencia.toString();
	}

	/**
	 * M�todo para obtener el query que permite la actualizaci�n de un 
	 * registro en el cat�logo
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL, para actualizar
	 */
	public String getQueryActualizar() {
		String qrySentencia = 
				" UPDATE comcat_estado " +
				" SET cd_nombre = ? " +
				" WHERE ic_pais = ? AND ic_estado = ? ";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(this.getNombreEstado());
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(new Integer(this.getClaveEstado()));
		return qrySentencia;
	}

	/**
	 * M�todo para obtener el query que permite la eliminaci�n de un 
	 * registro en el cat�logo
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL, para eliminar
	 */
	public String getQueryEliminar() {
		String qrySentencia = 
				" DELETE FROM comcat_estado " +
				" WHERE ic_pais = ? AND ic_estado = ? ";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(new Integer(this.getClaveEstado()));

		return qrySentencia;
	}
	
	
	/**
	 * M�todo para obtener el query que permite mostrar los datos
	 * de un registro a modificar.
	 * 
 	 * La instrucci�n de SQL debe manejar variables bind, cuyos
	 * valores deben ser regresados por el m�todo 
	 * {@link #getParametrosBind() getParametrosBind}
	 * 
	 * @return Cadena con la instruccion SQL.
	 */
	public String getQueryRegistro(){
		String qrySentencia = 
				" SELECT e.ic_pais, e.ic_estado, e.cd_nombre as nombrePais,  " +
				" p.cd_descripcion as nombrePais " + 
				" FROM comcat_estado e, comcat_pais p " +
				" WHERE e.ic_pais = p.ic_pais " +
				" AND e.ic_pais = ? AND e.ic_estado = ? ";

		//Es necesario establecer el tipo adecuado de dato segun 
		//la definici�n de la tabla, a partir de la cadena que contiene el valor.
		parametrosBind.add(new Integer(this.getClavePais()));
		parametrosBind.add(new Integer(this.getClaveEstado()));

		return qrySentencia;
	}

	
	/**
	 * M�todo para obtener los valores para cada uno de las variables bind
	 * de la instrucci�n de SQL.
	 * Los valores deberan de estar acorde a la operaci�n que se realice
	 * (inserci�n, actualizacion, eliminaci�n)
	 * @return Lista con los valores de las variables bind.
	 */
	public List getParametrosBind() {
		return parametrosBind;
	}
	/**
	 * Obtiene la clave del Pais al que pertenece el Municipio
	 * @return Cadena con la clave del pais
	 */
	public String getClavePais()
	{
		return clavePais;
	}
	/**
	 * Establece la clave del Pais al que pertenece el Municipio
	 * @param clavePais Cadena con la clave del pais
	 */
	public void setClavePais(String clavePais)
	{
		this.clavePais = clavePais;
	}
	/**
	 * Obtiene el nombre del Estado
	 * @return Cadena con el nombre del estado
	 */
	public String getNombreEstado()
	{
		return nombreEstado;
	}
	/**
	 * Establece el nombre del Estado
	 * @param  nombreEstado Cadena con el nombre del estado
	 */
	public void setNombreEstado(String nombreEstado)
	{
		this.nombreEstado = nombreEstado;
	}
	
	/**
	 * Obtiene la clave del Estado al que pertenece el Municipio
	 * @return Cadena con la clave del estado
	 */
	public String getClaveEstado()
	{
		return claveEstado;
	}

	/**
	 * Establece la clave del Estado al que pertenece el Municipio
	 * @param claveEstado Cadena con la clave del estado
	 */
	public void setClaveEstado(String claveEstado)
	{
		this.claveEstado = claveEstado;
	}

}