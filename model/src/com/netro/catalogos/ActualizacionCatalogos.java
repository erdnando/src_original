package com.netro.catalogos;

import com.netro.catalogos.mantenimiento.*;
import java.util.*;
import netropology.utilerias.*;
import com.netro.exception.*;
import javax.ejb.Remote;

@Remote
public interface ActualizacionCatalogos{

    // Metodos para Administracion - Parametrización - "Actualizacion Catálogos".
    public String menu(String esNombreTabla) throws NafinException;

    // Metodos para Administracion - Parametrización - nafin - "Comision Recursos Propios"
    public abstract boolean guardaDatosComision(String cs_tipo_fondeo, String ic_if, String perfil) throws NafinException;

    public abstract boolean guardaDatosComisionParamIf(
	String ic_if,
	String cg_financiamiento,
	String cf_comision,
	String fg_por_comision_de,
	String fg_porc_comision_dm,
	String fg_porc_comision_vc,
	String fg_porc_comision_m2
    ) throws NafinException;

    public abstract boolean guardarPorcentajeComision(String fg_porc_comision_fondeo, String perfil) throws NafinException;

    public abstract void guardarComisionPorRangos(List comisiones, String perfil) throws NafinException;

    public abstract void guardarAjusteAnual(
	String ic_if,
	String ig_mes_ajuste_de,
	String ig_mes_ajuste_a,
	String ig_anio_ajuste,
	String perfil
    ) throws NafinException;

    public void insertarEnCatalogo(CatalogoMantenimiento catalogo) throws NafinException;

    public void insertaMunicipio(CatalogoMantenimientoEstado catalogo) throws NafinException;

    public void actualizarCatalogo(CatalogoMantenimiento catalogo) throws NafinException;

    public int eliminarDelCatalogo(CatalogoMantenimiento catalogo) throws NafinException;

    public boolean validarInexistenciaEnCatalogo(CatalogoMantenimiento catalogo) throws NafinException;

    public Registros getRegistroCatalogo(CatalogoMantenimiento catalogo) throws NafinException;

    public abstract List consultarComisionPorRangos(String perfil) throws NafinException;

    public abstract List consultarAjusteAnual(String ic_if, String perfil) throws NafinException;

    public abstract boolean guardaDetalleComision(
	String cs_tipo_fondeo,
	String cg_tipo_comision,
	String cs_recorte_solic,
	String ic_if,
	String ic_epo,
	String ic_producto_nafin,
	String perfil
    ) throws NafinException;

    public Vector consultaComision(String perfil) throws NafinException;

    public String consultarPorcentajeComision(String perfil) throws NafinException;

    public Vector consultaDetalleComision(String ic_if, String ic_producto_nafin, String perfil) throws NafinException;

    public void execProcActualizaTipoCambio() throws NafinException;

}