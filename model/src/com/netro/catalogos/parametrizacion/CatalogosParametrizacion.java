package com.netro.catalogos.parametrizacion;

import java.util.HashMap;
import java.util.ArrayList;
import netropology.utilerias.AppException;
import javax.ejb.Remote;

@Remote
public interface CatalogosParametrizacion {

	public HashMap getParametrosTipoCatalogo(String claveCatalogo) throws AppException;

	public boolean esCatalogoExterno(String claveCatalogo, String perfilCatalogo) throws AppException;

	public String getNombreArchivoCatalogo(String claveCatalogo, String perfilCatalogo) throws AppException;

	public boolean hayPermisoAgregarRegistro(String claveCatalogo, String perfilCatalogo) throws AppException;

	public boolean hayPermisoModificarRegistro(String claveCatalogo, String perfilCatalogo) throws AppException;

	public boolean hayPermisoBorrarRegistro(String claveCatalogo, String perfilCatalogo) throws AppException;

	// METODO_TEMPORAL
	public ArrayList getListaResponsables(String claveEpoCesion) throws AppException; 

}