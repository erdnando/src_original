package com.netro.catalogos.parametrizacion;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "CatalogosParametrizacionEJB" , mappedName = "CatalogosParametrizacionEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class CatalogosParametrizacionBean implements CatalogosParametrizacion {

	private final static Log log = ServiceLocator.getInstance().getLog(CatalogosParametrizacionBean.class);

	public HashMap getParametrosTipoCatalogo(String claveCatalogo)
		throws AppException{

		log.info("getParametrosTipoCatalogo(E)");

		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		StringBuffer		query			= new StringBuffer();

		HashMap 				resultado 	= new HashMap();

		try {

			con.conexionDB();

			String esCatalogoExterno 	= null;
			String nombreArchivo			= null;

			query.append(
				"SELECT                                  "  +
				"  catalogo.cs_externo,                  "  +
				"  catalogo.cg_file                      "  +
				"FROM                                    "  +
				"  comcat_catalogos_param catalogo       "  +
				"WHERE                                   "  +
				"  catalogo.cs_activo          = 'S' and "  +
				"  catalogo.ic_catalogos_param = ?       "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, Integer.parseInt(claveCatalogo));

			if(rs.next()){
				esCatalogoExterno = "S".equals( rs.getString("cs_externo") )?"true":"false";
				nombreArchivo		= rs.getString("cg_file");
			}

			if( esCatalogoExterno 		== null || esCatalogoExterno.trim().equals("") 	){
				throw new AppException(" El atributo: \"externo\" no es v�lido.");
			} else if( nombreArchivo 	== null || nombreArchivo.trim().equals("") 		){
				throw new AppException(" El atributo: \"file\" no es v�lido.");
			}

			resultado.put("esCatalogoExterno", 	new Boolean(esCatalogoExterno) 	);
			resultado.put("nombreArchivo",		nombreArchivo 							);

		} catch (Exception e) {

			log.error("getParametrosTipoCatalogo(Exception)");
			log.error("getParametrosTipoCatalogo.claveCatalogo = <" + claveCatalogo+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al consultar el cat�logo: " + e.getMessage() );

		} finally {

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("getParametrosTipoCatalogo(S)");

		}

		return resultado;

	}

	public boolean esCatalogoExterno(String claveCatalogo, String perfilCatalogo)
		throws AppException{

		log.info("getParametrosTipoCatalogo(E)");

		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		StringBuffer		query			= new StringBuffer();

		boolean 				esExterno 	= false;

		try {

			con.conexionDB();

			String csExterno 	= null;

			query.append(
				"SELECT                                  "  +
				"  catalogo.cs_externo                   "  +
				"FROM                                    "  +
				"  comcat_catalogos_param catalogo,      "  +
				"  comrel_catalogos_param rel      	  	  "  +
				"WHERE                                   "  +
				"	catalogo.IC_CATALOGOS_PARAM 	= rel.ic_catalogos_param and "  +
				"  catalogo.cs_activo          	= 'S'                    and "  +
				"  catalogo.ic_catalogos_param 	= ?       					 and "  +
				"  rel.cs_activo          			= 'S'                    and "  +
				"  rel.c_perfil_catalogos_param	= ?     "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, 		Integer.parseInt(claveCatalogo));
			ps.setString(2, 	perfilCatalogo);

			rs = ps.executeQuery();

			if(rs.next()){
				csExterno = rs.getString("cs_externo");
			} else {
				throw new AppException("Cat�logo inv�lido");
			}

			if( csExterno 		== null || csExterno.trim().equals("") 	){
				throw new AppException(" El atributo: \"externo\" no es v�lido.");
			}

			esExterno = "S".equals(csExterno)?true:false;

		} catch (Exception e) {

			log.error("getParametrosTipoCatalogo(Exception)");
			log.error("getParametrosTipoCatalogo.claveCatalogo  = <" + claveCatalogo+ ">");
			log.error("getParametrosTipoCatalogo.perfilCatalogo = <" + perfilCatalogo+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al consultar el cat�logo: " + e.getMessage() );

		} finally {

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("getParametrosTipoCatalogo(S)");

		}

		return esExterno;

	}

	public String getNombreArchivoCatalogo(String claveCatalogo, String perfilCatalogo)
		throws AppException{

		log.info("getNombreArchivoCatalogo(E)");

		AccesoDB 			con 				= new AccesoDB();
		PreparedStatement	ps					= null;
		ResultSet			rs					= null;
		StringBuffer		query				= new StringBuffer();

		String 				nombreArchivo 	= null;

		try {

			con.conexionDB();

			query.append(
				"SELECT                                  "  +
				"  catalogo.cg_file                      "  +
				"FROM                                    "  +
				"  comcat_catalogos_param catalogo,      "  +
				"  comrel_catalogos_param rel      	  	  "  +
				"WHERE                                   "  +
				"	catalogo.IC_CATALOGOS_PARAM 	= rel.ic_catalogos_param and "  +
				"  catalogo.cs_activo          	= 'S'                    and "  +
				"  catalogo.ic_catalogos_param 	= ?       					 and "  +
				"  rel.cs_activo          			= 'S'                    and "  +
				"  rel.c_perfil_catalogos_param	= ?     "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, 		Integer.parseInt(claveCatalogo));
			ps.setString(2, 	perfilCatalogo);

			rs = ps.executeQuery();

			if(rs.next()){
				nombreArchivo = rs.getString("cg_file");
			} else {
				throw new AppException("Cat�logo inv�lido");
			}

			if( nombreArchivo 		== null || nombreArchivo.trim().equals("") 	){
				throw new AppException(" El atributo: \"archivo\" no es v�lido.");
			}

		} catch (Exception e) {

			log.error("getNombreArchivoCatalogo(Exception)");
			log.error("getNombreArchivoCatalogo.claveCatalogo  = <" + claveCatalogo+ ">");
			log.error("getNombreArchivoCatalogo.perfilCatalogo = <" + perfilCatalogo+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al consultar el cat�logo: " + e.getMessage() );

		} finally {

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("getNombreArchivoCatalogo(S)");

		}

		return nombreArchivo;

	}

   public boolean hayPermisoAgregarRegistro(String claveCatalogo, String perfilCatalogo)
		throws AppException{

		log.info("hayPermisoAgregarRegistro(E)");

		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		StringBuffer		query			= new StringBuffer();

		boolean 				agregar 		= false;

		try {

			con.conexionDB();

			String csAgregar 	= null;

			query.append(
				"SELECT                                  "  +
				"  rel.cs_agregar                        "  +
				"FROM                                    "  +
				"  comcat_catalogos_param catalogo,      "  +
				"  comrel_catalogos_param rel      	  	  "  +
				"WHERE                                   "  +
				"	catalogo.IC_CATALOGOS_PARAM 	= rel.ic_catalogos_param and "  +
				"  catalogo.cs_activo          	= 'S'                    and "  +
				"  catalogo.ic_catalogos_param 	= ?       					 and "  +
				"  rel.cs_activo          			= 'S'                    and "  +
				"  rel.c_perfil_catalogos_param	= ?     "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, 		Integer.parseInt(claveCatalogo));
			ps.setString(2, 	perfilCatalogo);

			rs = ps.executeQuery();

			if(rs.next()){
				csAgregar = rs.getString("cs_agregar");
			} else {
				throw new AppException("Cat�logo inv�lido");
			}

			if( csAgregar 		== null || csAgregar.trim().equals("") 	){
				throw new AppException(" El atributo: \"agregar\" no es v�lido.");
			}

			agregar = "S".equals(csAgregar)?true:false;

		} catch (Exception e) {

			log.error("hayPermisoAgregarRegistro(Exception)");
			log.error("hayPermisoAgregarRegistro.claveCatalogo  = <" + claveCatalogo+ ">");
			log.error("hayPermisoAgregarRegistro.perfilCatalogo = <" + perfilCatalogo+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al consultar el cat�logo: " + e.getMessage() );

		} finally {

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("hayPermisoAgregarRegistro(S)");

		}

		return agregar;

	}

   public boolean hayPermisoModificarRegistro(String claveCatalogo, String perfilCatalogo)
		throws AppException{

		log.info("hayPermisoModificarRegistro(E)");

		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		StringBuffer		query			= new StringBuffer();

		boolean 				modificar 	= false;

		try {

			con.conexionDB();

			String csModificar 	= null;

			query.append(
				"SELECT                                  "  +
				"  rel.cs_modificar                      "  +
				"FROM                                    "  +
				"  comcat_catalogos_param catalogo,      "  +
				"  comrel_catalogos_param rel      	  	  "  +
				"WHERE                                   "  +
				"	catalogo.IC_CATALOGOS_PARAM 	= rel.ic_catalogos_param and "  +
				"  catalogo.cs_activo          	= 'S'                    and "  +
				"  catalogo.ic_catalogos_param 	= ?       					 and "  +
				"  rel.cs_activo          			= 'S'                    and "  +
				"  rel.c_perfil_catalogos_param	= ?     "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, 		Integer.parseInt(claveCatalogo));
			ps.setString(2, 	perfilCatalogo);

			rs = ps.executeQuery();

			if(rs.next()){
				csModificar = rs.getString("cs_modificar");
			} else {
				throw new AppException("Cat�logo inv�lido");
			}

			if( csModificar 		== null || csModificar.trim().equals("") 	){
				throw new AppException(" El atributo: \"modificar\" no es v�lido.");
			}

			modificar = "S".equals(csModificar)?true:false;

		} catch (Exception e) {

			log.error("hayPermisoModificarRegistro(Exception)");
			log.error("hayPermisoModificarRegistro.claveCatalogo  = <" + claveCatalogo+ ">");
			log.error("hayPermisoModificarRegistro.perfilCatalogo = <" + perfilCatalogo+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al consultar el cat�logo: " + e.getMessage() );

		} finally {

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("hayPermisoModificarRegistro(S)");

		}

		return modificar;

	}

   public boolean hayPermisoBorrarRegistro(String claveCatalogo, String perfilCatalogo)
		throws AppException{

		log.info("hayPermisoBorrarRegistro(E)");

		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		StringBuffer		query			= new StringBuffer();

		boolean 				borrar 		= false;

		try {

			con.conexionDB();

			String csBorrar 	= null;

			query.append(
				"SELECT                                  "  +
				"  rel.cs_borrar                         "  +
				"FROM                                    "  +
				"  comcat_catalogos_param catalogo,      "  +
				"  comrel_catalogos_param rel      	  	  "  +
				"WHERE                                   "  +
				"	catalogo.IC_CATALOGOS_PARAM 	= rel.ic_catalogos_param and "  +
				"  catalogo.cs_activo          	= 'S'                    and "  +
				"  catalogo.ic_catalogos_param 	= ?       					 and "  +
				"  rel.cs_activo          			= 'S'                    and "  +
				"  rel.c_perfil_catalogos_param	= ?     "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, 		Integer.parseInt(claveCatalogo));
			ps.setString(2, 	perfilCatalogo);

			rs = ps.executeQuery();

			if(rs.next()){
				csBorrar = rs.getString("cs_borrar");
			} else {
				throw new AppException("Cat�logo inv�lido");
			}

			if( csBorrar 		== null || csBorrar.trim().equals("") 	){
				throw new AppException(" El atributo: \"borrar\" no es v�lido.");
			}

			borrar = "S".equals(csBorrar)?true:false;

		} catch (Exception e) {

			log.error("hayPermisoBorrarRegistro(Exception)");
			log.error("hayPermisoBorrarRegistro.claveCatalogo  = <" + claveCatalogo+ ">");
			log.error("hayPermisoBorrarRegistro.perfilCatalogo = <" + perfilCatalogo+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al consultar el cat�logo: " + e.getMessage() );

		} finally {

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("hayPermisoBorrarRegistro(S)");

		}

		return borrar;

	}

	/**
	 * Devuelve la lista de usuarios del OID.
	 * Nota: en un futuro cuando la implementaci&oacute;n de los catalogos "hard coded" se vuelva m&aacute;s gen&eacute;rica,
	 * mover la implementaci&oacute;n de este metodo a otra clase.
	 * Este m&eacute;todo s&oacute;lo se utiliza por el momento para tipoAfiliado = "E" ( EPO )
	 *
	 * @throws AppException
	 * @param tipoAfiliado <tt>String</tt> con el tipo de afiliado.
	 * @param claveAfiliado <tt>String</tt> con la clave de afiliado, por ejemplo <tt>COMCAT_EPO.IC_EPO</tt>
	 * @param perfil <tt>String</tt> con el perfil del afiliado.
	 *
	 * @return <tt>ArrayList</tt> de Usuarios ( <tt>netropology.utilerias.usuarios.Usuario</tt> )
	 *
	 */
	// METODO_TEMPORAL
	private ArrayList getListaUsuarios(String tipoAfiliado, String claveAfiliado, String perfil)
		throws AppException {

		log.info("getListaUsuarios(E)");

		ArrayList 	listaUsuarios 	= new ArrayList();
		UtilUsr 		oUtilUsr 		= null;
		UtilUsr 		utilUsr 			= null;

		try {

			oUtilUsr = new UtilUsr();
			utilUsr  = new UtilUsr();

			List 		cuentas 		= utilUsr.getUsuariosxAfiliado( claveAfiliado, tipoAfiliado);
			Iterator itCuentas 	= cuentas.iterator();
			while (itCuentas.hasNext()) {

				netropology.utilerias.usuarios.Usuario oUsuario = null;

				String login 	= (String) itCuentas.next();
				oUsuario 		= oUtilUsr.getUsuario(login);
				if( "".equals(perfil)) {
					listaUsuarios.add(oUsuario);
				} else {
					if(perfil.equals(oUsuario.getPerfil())){
						listaUsuarios.add(oUsuario);
					}
				}

			}

			Collections.sort(listaUsuarios);

		} catch(Exception e){

			log.error("getListaUsuarios(Exception)");
			log.error("getListaUsuarios.tipoAfiliado  = <" + tipoAfiliado  + ">");
			log.error("getListaUsuarios.claveAfiliado = <" + claveAfiliado + ">");
			log.error("getListaUsuarios.perfil        = <" + perfil        + ">");
			e.printStackTrace();

			throw new AppException("Fall� la consulta de usuarios: " + e.getMessage() );

		} finally {

			log.info("getListaUsuarios(S)");

		}
		return listaUsuarios;

	}

	/**
	 *
	 * Devuelve un <tt>ArrayList</tt> con la lista de responsables para una EPO en especifico.
	 * Nota: en un futuro cuando la implementaci&oacute;n de los catalogos "hard coded" se vuelva m&aacute;s gen&eacute;rica,
	 * mover la implementaci&oacute;n de este metodo a otra clase.
	 *
	 * @throws AppException
	 *
	 * @param claveEpoCesion <tt>String</tt> con la clave de la EPO ( como aperece en el campo <tt>COMCAT_EPO.IC_EPO</tt> ).
	 *
	 * @return <tt>ArrayList</tt> de <tt>HashMap</tt> con los datos de los usuarios responsables.
	 *
	 */
	// METODO_TEMPORAL
	public ArrayList getListaResponsables(String claveEpoCesion)
		throws AppException {

		log.info("getListaResponsables(E)");
		ArrayList listaResponsables = new ArrayList();

		try {

			List 		listaUsuarios	= getListaUsuarios("E", claveEpoCesion, "ADMIN EPO");
			String 	responsable 	= null;
			Usuario 	oUsuario 		= null;
			HashMap	registro			= null;
			for (int indice=0; indice < listaUsuarios.size(); indice++){

				oUsuario 	= (Usuario) listaUsuarios.get(indice);
				responsable = oUsuario.getLogin()+" - "+oUsuario.getNombre()+" "+oUsuario.getApellidoPaterno()+" "+oUsuario.getApellidoMaterno();

				registro		= new HashMap();
				registro.put("CLAVE",			responsable);
				registro.put("DESCRIPCION",	responsable);

				listaResponsables.add(registro);

			}

		} catch( AppException appE ){

			log.error("getListaResponsables(AppException)");
			log.error("getListaResponsables.claveEpoCesion = <" + claveEpoCesion+ ">");
			appE.printStackTrace();

			throw appE;

		} catch( Exception e ){

			log.error("getListaResponsables(Exception)");
			log.error("getListaResponsables.claveEpoCesion = <" + claveEpoCesion+ ">");
			e.printStackTrace();

			throw new AppException("Se presentaron problemas al consultar la lista de responsables: "+ e.getMessage() );

		} finally {

			listaResponsables = listaResponsables == null?new ArrayList():listaResponsables;
			log.info("getListaResponsables(S)");

		}

		return listaResponsables;

	}
}