/*************************************************************************************
*
* Nombre de Clase o de Archivo: Dispersion.java
*
* Versi�n: 		  1.0
*
* Fecha Creaci�n: Febrero 2mil3
*
* Autor:          Ricardo Arzate Ramirez
*
*************************************************************************************/

package com.netro.dispersion;

import com.netro.exception.NafinException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.AppException;
import netropology.utilerias.Registros;

@Remote
public interface Dispersion {
  
	public void ovactualizarCuenta( 	
										String ic_cuenta, 			String ic_nafin_electronico,
										String ic_producto_nafin,	String ic_moneda,
										String ic_bancos_tef,		String cg_cuenta,
										String ic_usuario,			String ic_tipo_cuenta, 
										HashMap parametrosAdicionales ) throws NafinException;
 
	public String ovinsertarCuenta(  String ic_nafin_electronico,	String ic_producto_nafin,
										String ic_moneda, 				String ic_bancos_tef,
										String cg_cuenta,				String ic_usuario,
										String ic_tipo_cuenta,			String ic_epo,
										String cg_tipo_afiliado,		String rfc, 
										HashMap parametrosAdicionales) throws NafinException; 

	public String sgetNumaxDispersion(	String sCuenta) throws NafinException;
	
	public Hashtable ohprocesarDispersionTEF(String esDocumentos, String lsProcSirac) throws NafinException;

	public Hashtable ohprocesarDispersionCECOBAN(String esDocumentos, String lsIcProceso) throws NafinException;
	
	public Vector ovgetDispersionesProcesadasTEF(String lsIcProceso) throws NafinException;	
	
	public Vector ovgetDispersionesProcesadasCECOBAN(String lsIcProceso) throws NafinException;	

	public boolean bborrarDispersionesTmp(String lsProcSirac, String lsCuenta) throws NafinException;

	public String sgetNumaxAsignacion() throws  NafinException;
	
	public Hashtable ohprocesarAsignacion(String esDocumentos, String lsIcProceso, String ic_epo) throws  NafinException;

	public Vector ovgetAsignacionProcesadas(String lsIcProceso, String ic_epo) throws NafinException;
	
	public boolean bborrarAsignacionesTmp(String lsIcProceso) throws NafinException;

	public Vector bEsDiaInhabil(String esFechaAplicacion, Calendar cFechaSigHabil) throws NafinException;
	
	public String getFechaSiguienteHabil() throws NafinException;
	
	public String getFechaSiguienteHabilPS() throws NafinException;
	
	public String getFechaSiguienteHabil(String sFecha) throws NafinException;
	
	public boolean tieneOperaciones(String sNoIf, String sNoEPO, String sNoMoneda, 
									String sFechaReg, String sTipo) throws NafinException;
	
	public Hashtable cambiaEstatusDeOperaciones(String fechaReg, String sLineasDoctos, String sTipo, String sOrigen) throws NafinException;
	
	public boolean esUsuarioCorrecto(String sLoginSesion, String sLoginEntrada, String sPassEntrada) throws NafinException;
	
	public boolean existenOperaciones(String sNoEPO, String sNoMoneda, String sFechaVenc, String sNoIF, String sNoEstatus, String sFechaOperacion) throws NafinException;
		
	public boolean existenOperaciones(String sNoEPO, String sNoMoneda, String sFechaVenc, String sNoIF, String sNoEstatus, String sFechaOperacion, String sNoIFFondeo) throws  NafinException;
		
	public void llenaEncabezadoFlujoFondos(String sNoEPO, String sNoMoneda, String sFechaVenc, String sNoIF, String sOrigen, String sNoEstatus, String sFechaOperacion) throws NafinException;
	
	public void llenaEncabezadoFlujoFondos(String sNoEPO, String sNoMoneda, String sFechaVenc, String sNoIF, String sOrigen, String sNoEstatus, String sFechaOperacion, boolean actualizarTablero ) throws NafinException;
	
	public void llenaEncabezadoFlujoFondos( String sNoEPO, String sNoMoneda, String sFechaVenc, String sNoIF, String sOrigen, String sNoEstatus, String sFechaOperacion, boolean actualizarTablero, String sNoIfFondeo ) throws NafinException; 
		
	public Hashtable getResumOperacErrorFF(String sTipo, String sTipoDispersion, String sFechaRegIni, 
											String sFechaRegFin, String sViaLiq, String ic_epo, String ic_if) throws NafinException;
											
	public Hashtable getResumOperacErrorFF(String sTipo, String sTipoDispersion, String sFechaRegIni, 
											String sFechaRegFin, String sViaLiq, String ic_epo, String ic_if, String sEstatus) throws NafinException;
	
	public Hashtable getResumOperacErrorFF(String sTipo, String sTipoDispersion, String sFechaRegIni, 
											String sFechaRegFin, String sViaLiq, String ic_epo, String ic_if, String sEstatus, String sEstatusFF) throws NafinException;
	
	public void reprocesaOperacionFF(String sNoFlujoFondos[]) throws NafinException;
	
	public void cancelarOperacionFF(String sNoFlujoFondos[], String usuario, String fcancelacion) throws NafinException;  

	public void getOperacionesAceptadas() throws NafinException;
	
	public int getEpoInfonavit() throws NafinException;
	
	public String getIfNafin() throws NafinException;
	
	public Vector getOperacionesDispersadasFisos(String sNoIf, String sNoEpo, String sFechaIni, 
												String sFechaFin) throws NafinException;
	
   public Vector getOperacionesDispersadasFisos(String sNoIf, String sNoEpo, String sFechaIni, 
												String sFechaFin, String sIcMoneda) throws NafinException;
																						
	public Hashtable getOperacionesDispersadasEpos(String sNoEpo, String sFechaIni, String sFechaFin) 
		throws NafinException;
	
	public Hashtable getOperacionesDispersadasEpos(String sNoEpo, String sFechaIni, String sFechaFin, String inicio, String fin) 
		throws NafinException;
		
	public Hashtable getOperacionesDispersadasEposUSD(String sNoEpo, String sFechaIni, String sFechaFin, String inicio, String fin)
		throws NafinException;
	
	public int setParamGralesFF(String rad_tipo_epo, String rad_via_liq_epo, String rad_tipo_if, String rad_via_liq_if, String rad_tipo_info, String rad_via_liq_info) 
		throws NafinException;
		
	public int setParamGralesDetalle(String tipo, String cmb_if, String cmb_epo, String rad_via_liq) 
		throws NafinException;

	public int setParamDetalleEliminar(String tipo, String cmb_if, String epo, String rad_via_liq) 
		throws NafinException;

	public Vector getComboEposxIF(String tipoAfiliado, String ic_producto_nafin, String ic_nafin_electronico)		
		throws NafinException;
		
		
	public Vector getDatosPyme(String tipo_factura,
				String ic_nafin_electronico,
				String mesIni,
				String anyoIni,
				String mesFin,
				String anyoFin,
				String ic_producto_nafin, 
				String ic_epo, 
				String ic_if)
		throws NafinException;
		
	public Vector getDatosFactura(String ic_pyme,String fecha_seleccion_de,String fecha_seleccion_a, String ic_producto_nafin, String ic_epo, String ic_if,String tipo_factura)
		throws NafinException;

	public void setParamFacturaxProducto(String ic_producto_nafin, String rad_epo, String rad_tipo_int)
		throws NafinException;	

	public Vector getParamFacturaxProducto(String ic_producto_nafin) 
		throws NafinException;
		
	public void setParamFacturaxProductoEpo(String ic_producto_nafin, String ic_epo, String rad_tipo_int) 
		throws NafinException;

	public String getParamFacturaxProductoEpo(String ic_producto_nafin, String ic_epo) 
		throws NafinException;
		
	public HashMap getDetalleOperacion(String ic_flujo_fondos, String tabla)
		throws NafinException;
		
	public Vector getDetalleDoctos(
					String ic_flujo_fondos, 	String tipo_disp, 		String estatus, 
					String ic_epo, 				String ic_pyme, 		String ic_if, 
					String ic_estatus_docto, 	String df_fecha_venc, 	String df_operacion)
		throws NafinException;
		
	public String crearResumenFactura(Vector DatosFactura, String ic_producto_nafin, String ic_epo, String ic_if)
		throws NafinException;
		
	public int getExisteDetalleHist(String ic_flujo_fondos, String tabla)
		throws NafinException;
		
		
	public void eliminarCuenta(String ic_cuenta, String ic_usuario, HashMap parametrosAdicionales )
		throws NafinException;
		
	public String getQueryDispINFONAVIT(String ic_folio_ff, String sNoEPO, String sNoMoneda, String sNoEstatus, String sFechaOperacion)
		throws NafinException;
	
	public String getQueryDispEPOS(String ic_folio_ff, String sNoEPO, String sNoMoneda, String sNoEstatus, String sFechaVenc)
		throws NafinException;	
	
	public String getQueryDispFISOS(String ic_folio_ff, String sNoEPO, String sNoMoneda, String sNoIF)
		throws NafinException;
	
	public String getQueryDispPEMEX(String ic_folio_ff, String sNoEPO, String sNoMoneda, String sNoEstatus, String sFechaVenc, String sFechaOperacion)	
		throws NafinException;
		
	public int getExisteCuenta(String ic_cuenta)
		throws NafinException;
		
	public void reasignarCuenta(String ic_cuenta, String ic_epo, String ic_usuario, HashMap parametrosAdicionales)
		throws NafinException;

	public int getExistenDoctosRelacionados(String ic_flujo_fondos)
		throws NafinException;
		
	public List getTarifaEpo(String ic_epo)
		throws NafinException;
		
	public List getTarifaEpoUSD(String ic_epo)
		throws NafinException;
		
	public void setTarifaEpo(String ic_epo, String txt_dia[], String txt_tarifa[])
		throws NafinException;
		
	public void setTarifaEpoUSD(String ic_epo, String txt_dia[], String txt_tarifa[])
		throws NafinException;
		
	public void delTarifaEpo(String ic_epo)
		throws NafinException;
		
	public void delTarifaEpoUSD(String ic_epo)
		throws NafinException;
		
	public void getSuceptibleDispersion(String ic_epo)
		throws NafinException;
		
	public String setDatosTmpCLABE(String ic_proc_carga, List lRegistros) 
		throws NafinException;
    
  public String setDatosTmpCLABEbncmxt(String ic_proc_carga, List lRegistros) 
		throws NafinException;
    
  public String setDatosTmpValidaCLABEbncmxt(String ic_proc_carga, List lRegistros) 
  //public String setDatosTmpVCLABEbxt(String ic_proc_carga, List lRegistros) 
		throws NafinException;
    
  public Registros getEstatusCargaCLABEbncmxt(String ic_proc_carga) 
		throws NafinException;
    
  public Registros getEstatusCargaVerificaCLABEbncmxt(String ic_proc_carga) 
		throws NafinException;    
    
  public Registros getRegistrosValidadosbncmxt(String ic_proc_carga, String cg_estatus)
		throws NafinException;    
    
  public Registros getRegistrosValidadosVerifbncmxt(String ic_proc_carga, String cg_estatus)
		throws NafinException;        
    
  public void setCuentasClaveMasivabncmxt(Registros registros, String ic_epo, String ic_usuario) 
		throws NafinException;    
    
  public void setCuentasVerifClaveMasivabncmxt(Registros registros, String ic_epo, String ic_usuario) 
		throws NafinException;        

	public Registros getEstatusCargaCLABE(String ic_proc_carga) 
		throws NafinException;   
	
	public Registros getRegistrosValidados(String ic_proc_carga, String cg_estatus)
		throws NafinException;
		
	public void setCuentasClaveMasiva(Registros registros, String ic_epo, String ic_usuario) 
		throws NafinException;
		
	public HashMap DispConsBloqServs(String sIdEpo) 
		throws NafinException;			
		
	public HashMap autenticaFirmaDigital(String pkcs7, String textoFirmado, String serial, String numeroCliente) 
		throws NafinException;	
		
	public String DispActualizacionBloqueos(List causa, List checkbloq, List Epo, List ProdNafin, 
																					String NombreUsuario, String ClaveUsuario, String Folio, String Recibo) 
		throws NafinException;	
		
	public HashMap DispCaptBloqServsFin(List causa, List Epo) 
		throws NafinException;	

	public boolean existeRegistroDeDispersion(String claveEPO)
		throws NafinException; 
		
	public void setParametrosDeDispersion(String claveEPO,String clavePYME,String cuentasDeCorreoNAFIN, String cuentasDeCorreoEPO) 
		throws NafinException;		
	
	public HashMap getParametrosDeDispersion(String claveEPO) 
		throws NafinException;
		
	public String getMensajeDedispersion()
		throws NafinException;
		
	public void setMensajeDedispersion(String mensaje)
		throws NafinException;
		
	public boolean	estaAutorizadoDocumentoParaPagoDelServicioDeDispersion(String numeroDocumento, String claveEPO)
		throws NafinException;
		
	public boolean hayPYMEParametrizadaParaElServicioDeDispersion(String claveEPO)
		throws NafinException;
		
	public boolean existeClavePYMENAFIN()
		throws NafinException;
		
	public String getNombreMoneda(String claveMoneda)
		throws NafinException;
		
	public void generaDocumentoParaCobroDelServicioDeDispersion(HashMap documento, HashMap acuse, String clavePYME)
		throws NafinException;
		
	public String getClavePYMENAFIN()
		throws NafinException;
	
	public String getClaveDelProveedorDeDispersion(String claveEPO)
		throws NafinException;
		
	public boolean existeRelacionEPOconPYMENAFIN(String claveEPO, String clavePYME)
		throws NafinException;
		
	public  String getNumeroMaximoDocto() 
		throws NafinException;
	
	public HashMap autenticaFirmaDigitalv2(String pkcs7, String textoFirmado, String serial, String numeroCliente) 
		throws NafinException;
		
	public List getDetalleFlujoFondosConError(String claveFlujoFondos) 
		throws NafinException; 
		
	public List getDetalleFlujoFondos(String claveFlujoFondos) 
		throws NafinException;
		
	public String getQueryClaveDispersionNoNegociables();
		
	public String getQueryConteoDispersion();
	
	public String getQueryNumeroFolioSiguiente();
	
	public String getQueryMonitoreoDispersionPorEPO(String claveEpo, String sufijoFechaVencimientoPyme, String fechaVencimientoInicio, String fechaVencimientoFin );
		
	 public String getQueryCatalogoEposMonitoreoPEMEXREF();
		
	 public List getDetalleCatalogoEposMonitoreoPEMEXREF()
	 	throws AppException;
	 	
	 public String getQueryDetalleMonitoreoPemexRef(String claveEpo);
		
	 public String getQueryDetalleImpresionMonitoreoDispersionPorEPO(String claveEpo, String sufijoFechaVencimientoPyme, String fechaVencimientoInicio, String fechaVencimientoFin );
		
	 public String getQueryDetalleMonitoreoDispersionPorIF(String claveIF, String claveEpo, String radHab, String chkCuenta);
		
	public String getQueryDetalleMonitoreoDispersionPorInfonavit(String claveIf, String claveEpo, String radHab, String chkCuenta);
		
	public String getQueryDetallePemexGasolineros(String estatus);
		
	public String getQueryDetalleArchivoTEFPemexGasolineros(String estatus, String fechaSiguienteDiaHabil);
		
	public boolean hayServicioDeDispersionHabilitado(String claveEPO)
		throws NafinException;
		
	public String getQueryDetalleDocumentosDispersados(String claveEpo, String fechaVencimientoInicio, String fechaVencimientoFin);
		
	public String getQueryDetalleDocumentosNoDispersados(String claveEpo, String fechaVencimientoInicio, String fechaVencimientoFin);
		
	public String getQueryDetalleDocumentosADispersar(int numPymes);
		
	public boolean hasEstatusDispersionOperada(String ic_epo)
		throws AppException; 
		
	public void setEstatusDispersionOperada(String ic_epo, boolean estatus)
		throws AppException;
		
	public boolean hasEstatusDispersionVencidoSinOperar(String ic_epo)
		throws AppException;
	
	public void setEstatusDispersionVencidoSinOperar(String ic_epo, boolean estatus)
		throws AppException;
		
	public boolean hasEstatusDispersionOperadaPagada(String ic_epo)
		throws AppException;
		
	public void setEstatusDispersionOperadaPagada(String ic_epo, boolean estatus)
		throws AppException;
		
	public boolean hasEstatusDispersionPagadoSinOperar(String ic_epo)
		throws AppException;
		
	public void setEstatusDispersionPagadoSinOperar(String ic_epo, boolean estatus)
		throws AppException;
		
	public boolean hasTarifaDispersionMonedaNacional(String ic_epo)
		throws AppException;
		
	public void setEstatusTarifaDispersionMonedaNacional(String ic_epo, boolean estatus)
		throws AppException;
		
	public boolean hasTarifaDispersionDolaresAmericanos(String ic_epo)
		throws AppException;
		
	public void setEstatusTarifaDispersionDolaresAmericanos(String ic_epo, boolean estatus)
		throws AppException;
		
	public String getTipoArchivoAEnviar(String ic_epo)
		throws AppException;
		
	public void setTipoArchivoAEnviar(String ic_epo, String tipoArchivo)
		throws AppException;
		
	public String getNombreProveedor(String ic_epo)
		throws AppException;
		
	public String getClaveProveedor(String ic_epo)
		throws AppException;
		
	public String getClaveProveedorByName(String nombreProveedor, String ic_epo)
		throws AppException;
		
	public String getDescripcionMoneda(String clave)
		throws AppException;
		
	public void validarCuenta(String icCuenta, HashMap parametrosAdicionales)
		throws AppException;
		
	public void invalidarCuenta(String icCuenta, HashMap parametrosAdicionales)
		throws AppException;
		
	public  String getNumeroAcusePdfFlujoFondos() 
		throws AppException;
		
	public  List consultaBitacoraCan( String TipoDispersion, String noEpo, String noIf, String fechaRegIni, String fechaRegFin, 
		String fechaCancelacionIni, String fechaCancelacionFin) throws AppException;
		
	public void realizaDispersionAutomaticaDoctosCadenaNafin() 
		throws AppException;
		
	public void realizaDispersionAutomaticaDoctosVencidosCadenaNafin()
		throws AppException;
		
	public String setDatosTmpCuentas(String ic_proc_carga, List lRegistros) 
		throws NafinException; 
		
	public Registros getEstatusCargaCuentas(String ic_proc_carga) 
		throws NafinException;
		
	public Registros getRegistrosConCuentasValidadas(String ic_proc_carga, String cg_estatus) 
		throws NafinException;
		
	public void agregaCuentas(Registros registros, String ic_epo, String ic_usuario, HashMap parametrosAdicionales ) 
		throws NafinException;
		
	public List getEPOSQuetTrabajanConIF(String claveIF)
		throws AppException;
		
	public void generaEstadoCuentatxt(String cveEpo, String prodNafinm, String rutaApp)
		throws AppException;
		
	public void setClaveEpoEnFFON(String ic_epo, String clave_epo_ffon)
		throws AppException;
		
	public String getClaveEpoEnFFON(String ic_epo)
		throws AppException;
		
	public HashMap getDatosClienteOrdenante(String claveEPO)
		throws AppException;

	public String getNombrePyme(String RFC)
		throws AppException;
		
	public HashMap getCatalogoMonedas()
		throws AppException;
		
	public String getNombreMoneda(String cveMoneda, HashMap catalogoMoneda)
		throws AppException;
	
	public ArrayList ordenaRegistros(Vector registrosMN, Vector registrosUSD)
		throws AppException;
		
	public List getOperDispEposDoctosNoOperJS(Vector documentosNoOperados, double tarifaA, double tarifaB, double tarifaC, double valorIva, double valorIvaT, String pantalla )
		throws AppException;
		
	public List getOperDispEposDoctosNoOperUSDJS(Vector documentosNoOperadosUSD, double tarifaAUSD, double tarifaBUSD, double tarifaCUSD, double valorIva, double valorIvaT, String pantalla )
		throws AppException;
		
	public List getDetalleDocumentosDispersados(String claveEpo, String fechaVencimientoInicio, String fechaVencimientoFin)
		throws AppException;
	
	public List getDetalleDocumentosNoDispersados(String claveEpo, String fechaVencimientoInicio, String fechaVencimientoFin)
		throws AppException;
		public void enviaSMSDispersionesPendientes()throws AppException;
		
	public boolean seEnvioAFlujoFondos( String claveEpo, String claveMoneda, String fechaVencimiento, String tipoDocumento )
		throws AppException;
		
	public String getNombreEPO(String claveEpo)
		throws AppException;
		
	public boolean existenParametrosDeDispersion(String claveEPO)
		throws AppException;

	public HashMap calcularDoctosYMontosTotalesDispersionEPO(Registros registros, String claveMoneda, String fechaVencimiento )
		throws AppException; 
		
	public List getDetalleDispersionesEnlistadas(String parametroFechaVencimiento)
		throws AppException;
	
	public void actualizaObservacionesTableroDisperionEPO( List observacionesList, String loginDelUsuario, String nombreDelUsuario ) 
		throws AppException;
	
	public HashMap enviarFlujoFondosDocumentosEPO( String recordID, String icEPO, String icMoneda, String dcVencimiento, String tipoDocumento, String totalDocumentos, String monto );
		
	public HashMap consultaDispersionFISOS( String noEpo, String noMoneda, String noIf, String fechaReg )
		throws  AppException;
	public HashMap getCuenta(String icCuenta) throws  AppException;
		
	public HashMap getEPOQueOperaFideicomiso(String claveIfFideicomiso)
		throws  AppException; 
		
	public String getQueryDispIfFideicomiso( String icFolioFF, String claveIfFideicomiso, String sNoEPO, String sNoIF, String sNoMoneda ) 
		throws  NafinException;
		
	public HashMap consultaDispersionIfFideicomiso( String claveIfFideicomiso, String noEpo, String noIf, String noMoneda, String fechaReg )
		throws  AppException; 
		
	public HashMap getQueryDetalleErrorDispersionIfFideicomiso( String claveIfFideicomiso, String claveEPO, String claveIF, String claveMoneda, String fechaRegistro )
		throws  AppException; 
		
	public String getQueryDetalleMonitoreoDispersionPorIfFideicomiso( String claveIfFideicomiso, String claveEPO, String claveIF, String pymeSinCuentaClabe, String radioHabilitadas );
	
	public String sLlaveSiguiente() throws  NafinException;    
	
	public boolean generarArchivosCuenta(String iNoUsuario, String strNombreUsuario,String ruta, String nombreArchivo, String tipoArchivo)
		throws  AppException;
    
  public List getCtasPymeCLABESwift(String tipoAfiliado, String cveProducto, String nafinElectronico, String cvePyme, String cveMoneda ) throws  AppException;
  
   public String ovinsertarCuentaProveedor(  	String ic_nafin_electronico,	String ic_producto_nafin,
										String ic_moneda, 				String ic_bancos_tef,
										String cg_cuenta,				String ic_usuario,
										String ic_tipo_cuenta,			List lstCtasEpo,
										String cg_tipo_afiliado,		String rfc, HashMap parametrosAdicionales)
   	throws  NafinException;    
}