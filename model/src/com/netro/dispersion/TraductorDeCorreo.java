package com.netro.dispersion;

import java.util.regex.*;
import java.util.*;

public class TraductorDeCorreo {
	
	public String 			texto 	= new String();
	public StringBuffer 	salida	= new StringBuffer();

	public String 		razonSocialEPO;
	public ArrayList 	listaDeIntermediariosConOperacion;
	public String 		montoDocumentosNoOperados;
	public String 		totalADispersar;
	public String 		fechaDeVencimiento;
	public String 		fechaADepositar;
	public String 		totalNoDispersado;
	public String 		logo;
	public String 		totalIntermediariosConOperacion;

	
	public ArrayList	listaDeIntermediariosConOperacionUSD;
	public String 		montoDocumentosNoOperadosUSD; 
	public String 		totalADispersarUSD;
	public String 		totalNoDispersadoUSD;
	public String 		totalIntermediariosConOperacionUSD;

	
	public final String	etiquetaUNO  			=	"<span style='text-align:justify;font-family:helvetica;font-weight:bolder;font-size:12px;color:#808080;'>";
	public final String	etiquetaDOSbanco 		=	"<td   style='padding-left:0px;word-wrap:break-word;text-align:left;font-family:helvetica;font-weight:bolder;font-size:12px;color:#535353;'>";
	public final String	etiquetaDOSmonto 		=	"<td   style='text-align:right;font-family:helvetica;font-weight:bolder;font-size:12px;color:#535353;'>";
	public final String  etiquetaTRESbanco		=	etiquetaDOSbanco;
	public final String 	etiquetaTRESmonto		=	etiquetaDOSmonto;
	public final String	etiquetaCUATRObanco	=	etiquetaDOSbanco;
	public final String	etiquetaCUATROmonto	= 	etiquetaDOSmonto;
	public final String 	etiquetaCINCObanco	=	etiquetaDOSbanco;
	public final String 	etiquetaCINCOmonto	=	etiquetaDOSmonto;
	public final String	etiquetaSEIS  			=	"<span style='text-align:justify;font-family:helvetica;font-weight:bolder;font-size:14px;color:black;'>";
	public final String	etiquetaSIETE  		=	"<span style='text-align:justify;font-family:helvetica;font-weight:bolder;font-size:14px;color:black;'>";
	public final String	etiquetaOCHO  			=	"<span style='text-align:justify;font-family:helvetica;font-weight:bolder;font-size:18px;color:red;'>";
	public final String	etiquetaNUEVE 			=	"<span style='text-align:justify;font-family:helvetica;font-weight:bolder;font-size:18px;color:red;'>";
	public final String	etiquetaDIEZ 			=	"<span style='text-align:justify;font-family:helvetica;font-weight:normal;font-size:12px;color:black;'>";
	public final String	etiquetaONCE 			=	"<span style='text-align:justify;font-family:helvetica;font-weight:normal;font-size:12px;color:black;'>";
	public final String	etiquetaDOCE  			=	"<span style='text-align:justify;font-family:helvetica;font-weight:bolder;font-size:14px;color:black;'>";
	public final String	etiquetaTRECE  		=	"<span style='text-align:justify;font-family:helvetica;font-weight:bolder;font-size:14px;color:black;'>"; 
	
	public final String	etiquetaNO				= "<span style='text-align:justify;font-family:helvetica;font-weight:bolder;font-size:14px;color:black;'>";
	
	public final String cabeceraContenido 	= "<div style='width:550px;text-align:justify;font-family:helvetica;font-weigth:bolder;font-size:12px;padding-left:11px;'>";
	public final String cabeceraDireccion 	= "<div style='font-family:\"Courier New\";font-size:14px;font-weight: bolder;padding-left:11px;'>";
	public final String etiquetaLogo	  		= "<img src=\"/nafin/00archivos/15cadenas/15archcadenas/logos/nafinsa.gif\" height=\"65\" width=\"175\" alt=\"\" border=\"0\">";			
	
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}	
	
	public String getRazonSocialEPO() {
		return razonSocialEPO;
	}
	public void setRazonSocialEPO(String razonSocialEPO) {
		this.razonSocialEPO = razonSocialEPO;
	}

	public ArrayList getListaDeIntermediariosConOperacion() {
		return listaDeIntermediariosConOperacion;
	}
	public void setListaDeIntermediariosConOperacion(ArrayList listaDeIntermediariosConOperacion) {
		this.listaDeIntermediariosConOperacion = listaDeIntermediariosConOperacion;
	}

	public String getMontoDocumentosNoOperados() {
		return montoDocumentosNoOperados;
	}
	public void setMontoDocumentosNoOperados(String montoDocumentosNoOperados) {
		this.montoDocumentosNoOperados = montoDocumentosNoOperados;
	}

	public String getTotalADispersar() {
		return totalADispersar;
	}
	public void setTotalADispersar(String totalADispersar) {
		this.totalADispersar = totalADispersar;
	}

	public String getFechaDeVencimiento() {
		return fechaDeVencimiento;
	}
	public void setFechaDeVencimiento(String fechaDeVencimiento) {
		this.fechaDeVencimiento = fechaDeVencimiento;
	}

	public String getFechaADepositar() {
		return fechaADepositar;
	}
	public void setFechaADepositar(String fechaADepositar) {
		this.fechaADepositar = fechaADepositar;
	}

	public String getTotalNoDispersado() {
		return totalNoDispersado;
	}
	public void setTotalNoDispersado(String totalNoDispersado) {
		this.totalNoDispersado = totalNoDispersado;
	}

	/**
	 * Devuelve un <tt>ArrayList</tt> con la lista de los los bancos con los que se
	 * registraron operaciones en dolares americanos. Cada elemento de la lista es un HashMap
	 * que posee las llaves INTERMEDIARIO y MONTO. En intermediario se almacena el nombre del
	 * Banco y en MONTO se almacena la cantidad en dolares, esta debe llevar el signo monetario $,
	 * estar formateada con comas y con dos decimales de precision.
	 * @return ArrayList con la lista de los bancos y los montos.
	 */
	public ArrayList getListaDeIntermediariosConOperacionUSD() {
		return listaDeIntermediariosConOperacionUSD;
	}
	/**
	 * Asigna el argumento <tt>listaDeIntermediariosConOperacionUSD</tt> al atributo <tt>listaDeIntermediariosConOperacionUSD</tt>
	 * de la clase. 
	 * @param listaDeIntermediariosConOperacionUSD Es un <tt>ArrayList</tt> donde cada elemento de esta lista 
	 * es un HashMap que posee las llaves INTERMEDIARIO y MONTO. En INTERMEDIARIO se almacena el nombre del
	 * Banco y en MONTO se almacena la cantidad en dolares, esta debe llevar el signo monetario $,
	 * estar formateado con comas y usar dos decimales de precision.
	 */
	public void setListaDeIntermediariosConOperacionUSD(ArrayList listaDeIntermediariosConOperacionUSD) {
		this.listaDeIntermediariosConOperacionUSD = listaDeIntermediariosConOperacionUSD;
	}
	/**
	 * Devuelve el monto de los documentos no operados en dolares americanos.
	 * @return Cadena de texto con el monto de los documentos.
	 */
	public String getMontoDocumentosNoOperadosUSD() {
		return montoDocumentosNoOperadosUSD;
	}
	/**
	 * Asigna el argumento <tt>montoDocumentosNoOperadosUSD</tt> al atributo de la clase <tt>montoDocumentosNoOperadosUSD</tt>
	 * @param montoDocumentosNoOperadosUSD Cadena de texto con el monto de los documentos no operados en dolares americanos.
	 * Este debe llevar el signo monetario $, estar formateado con comas y usar dos decimales de precision.
	 */
	public void setMontoDocumentosNoOperadosUSD(String montoDocumentosNoOperadosUSD) {
		this.montoDocumentosNoOperadosUSD = montoDocumentosNoOperadosUSD;
	}
	/**
	 * Devuelve el monto total a dispersar en Dolares Americanos.
	 * @return Cadena de texto con el monto a dispersar.
	 */
	public String getTotalADispersarUSD() {
		return totalADispersarUSD;
	}
	/**
	 * Asigna el argumento <tt>totalADispersarUSD</tt> al atributo de la clase <tt>totalADispersarUSD</tt>
	 * @param totalADispersarUSD Cadena de texto con el monto total a dispersar en Dolares Americanos.
	 * Este debe llevar el signo monetario $, estar formateado con comas y usar dos decimales de precision.
	 */
	public void setTotalADispersarUSD(String totalADispersarUSD) {
		this.totalADispersarUSD = totalADispersarUSD;
	}
	/**
	 * Devuelve el monto total no dispersado en Dolares Americanos.
	 * @return Cadena de texto con el monto total no dispersado.
	 */
	public String getTotalNoDispersadoUSD() {
		return totalNoDispersadoUSD;
	}
	/**
	 * Asigna el argumento <tt>totalNoDispersadoUSD</tt> al atributo de la clase <tt>totalNoDispersadoUSD</tt>
	 * @param totalADispersarUSD Cadena de texto con el monto total no dispersado en Dolares Americanos.
	 * Este debe llevar el signo monetario $, estar formateado con comas y usar dos decimales de precision.
	 */
	public void setTotalNoDispersadoUSD(String totalNoDispersadoUSD) {
		this.totalNoDispersadoUSD = totalNoDispersadoUSD;
	}
 
	/**
	 * Devuelve la suma de los montos de todos los bancos con los que se registraron operaciones en moneda nacional.
	 * @return Cadena de texto con la suma total.Esta suma lleva antepuesto el signo monetario $, esta formateada con comas 
	 * y usa dos decimales de precision.
	 */
	public String getTotalIntermediariosConOperacion(){
		return totalIntermediariosConOperacion;
	}
	/**
	 * Asigna el argumento <tt>totalIntermediariosConOperacion</tt> al atributo de la clase <tt>totalIntermediariosConOperacion</tt>
	 * @param totalIntermediariosConOperacion Cadena de texto con la suma de los montos de todos los bancos con los que se registraron operaciones en moneda nacional.
	 * Esta debe llevar el signo monetario $, estar formateada con comas y usar dos decimales de precision.
	 */
	public void setTotalIntermediariosConOperacion(String totalIntermediariosConOperacion) {
		this.totalIntermediariosConOperacion = totalIntermediariosConOperacion;
	}
 
	/**
	 * Devuelve la suma de los montos de todos los bancos con los que se registraron operaciones en dolares americanos.
	 * @return Cadena de texto con la suma total. Esta suma lleva antepuesto el signo monetario $, esta formateada con comas 
	 * y usa dos decimales de precision.
	 */
	public String getTotalIntermediariosConOperacionUSD(){
		return totalIntermediariosConOperacionUSD;
	}
	/**
	 * Asigna el argumento <tt>totalIntermediariosConOperacionUSD</tt> al atributo de la clase <tt>totalIntermediariosConOperacionUSD</tt>.
	 * @param totalIntermediariosConOperacionUSD Cadena de texto con la suma de los montos de todos los bancos con los que se registraron operaciones en dolares americanos.
	 * Esta debe llevar el signo monetario $, estar formateada con comas y usar dos decimales de precision.
	 */
	public void setTotalIntermediariosConOperacionUSD(String totalIntermediariosConOperacionUSD) {
		this.totalIntermediariosConOperacionUSD = totalIntermediariosConOperacionUSD;
	}
	
	public String convierteEnCodigoHTML(String cadena){
		
		String 			[]contenido 	= null;
		String 			[]direccion 	= null;
		String 			[]lineas			= null;
		StringBuffer 	resultado 		= new StringBuffer();
		int posicionUltimaLineaVacia 	= 0;
		
		String 			sContenido		= null;
		String			sDireccion		= null;
			
		
		// Borrar las ultimas lineas que esten vacias
		lineas						= cadena.split("\n");
		lineas 						= suprimeLineasVaciasAlFinal(lineas);
		
		// Extraer todos los parrafos de contenido y direccion
		posicionUltimaLineaVacia 	= getPosicionUltimaLineaVacia(lineas);
		
		// Extraer los parrafos correspondientes 		
		if(posicionUltimaLineaVacia != -1 ){
			contenido = getSubArray(lineas, 0, posicionUltimaLineaVacia);
			direccion = getSubArray(lineas, posicionUltimaLineaVacia, lineas.length-1);
		}else{
			contenido = lineas;
		}
		// Verificar que no haya datos nulos
		if(contenido == null){
			return "";
		}
		
		// Agregar salto de linea para codigo HTML
		for(int i=0;i<contenido.length;i++){
			contenido[i] = contenido[i] + "<br>";
		}
		if(direccion != null){
			for(int i=0;i<direccion.length;i++){
				direccion[i] = direccion[i] + "<br>";
			}
		}
		
		// Convertir array de contenido a cadena de texto
		sContenido = arrayToString(contenido,"\n");
		if(direccion != null) sDireccion = arrayToString(direccion,"\n"); // Para que pueda ser leido por seres humanos
		
		// Respetar los Espacios ingresados por el usuario
		//sContenido = conservaEspacios(sContenido);
		//if(direccion != null) sDireccion = conservaEspacios(sDireccion);
		
		// Traducir codificacion [numero] a etiquetas HTML con su correcta representacion
		sContenido =  reemplazaCorchetesConEtiquetasHTML(sContenido);
		
		// Reemplazar caracteres con acentos por sus respectivos equivalentes en codigo HTML
		sContenido = convierteAcentosACodigoHTML(sContenido);
		if(direccion != null) sDireccion = convierteAcentosACodigoHTML(sDireccion);
		
		// Cambiar de logo
		if(logo == null || logo.equals("")){
			logo = etiquetaLogo;
		}
		
		// Contruir bloque HTML para generar la vista preeliminar
		resultado.append("&nbsp;<br>"+ "\n");
		resultado.append(logo + "<br>"+ "\n");
		resultado.append(cabeceraContenido + "\n");
		resultado.append(sContenido);
		resultado.append("</div>\n");
		if(direccion != null){
			resultado.append(cabeceraDireccion);
			resultado.append(sDireccion);
			resultado.append("</div>\n");
		}
		
		return  resultado.toString();  
		
	}
	
	public String convierteAcentosACodigoHTML(String cadena){
		String resultado = cadena;
		resultado = resultado.replaceAll("�","&aacute;");
		resultado = resultado.replaceAll("�","&eacute;");
		resultado = resultado.replaceAll("�","&iacute;");
		resultado = resultado.replaceAll("�","&oacute;");
		resultado = resultado.replaceAll("�","&uacute;");
		resultado = resultado.replaceAll("�","&Aacute;");
		resultado = resultado.replaceAll("�","&Eacute;");
		resultado = resultado.replaceAll("�","&Iacute;");
		resultado = resultado.replaceAll("�","&Oacute;");
		resultado = resultado.replaceAll("�","&Uacute;");
		resultado = resultado.replaceAll("�","&ntilde;");
		resultado = resultado.replaceAll("�","&Ntilde;");
		resultado = resultado.replaceAll("�","&uuml;");
		resultado = resultado.replaceAll("�","&Uuml;");
		return resultado;
	}
	
	public String conservaEspacios(String cadena){
		String resultado = cadena;
		resultado = resultado.replaceAll(" ","&nbsp;");
		return resultado;
	}
	
	public boolean esLineaVacia(String linea){
		
		String espacios="\r\t\n ";
		for(int i=0;i<linea.length();i++){
			char caracter = linea.charAt(i);
			if( espacios.indexOf(caracter) == -1 ){
				return false;
			}
		}
		return true;
	}
	
	public  String[] suprimeLineasVaciasAlFinal(String []linea){
		String			[]resultado	= null;
		
		// Detectar indice de la ultima linea con contenido
		int indice=linea.length-1;
		for(indice=linea.length-1;indice>=0;indice--){
			if(!esLineaVacia(linea[indice]))
				break;
		}
		int ultima_linea_con_contenido = indice;
		
		resultado = new String[ultima_linea_con_contenido+1];
		// Recortar lineas
		for(indice=0;indice <= ultima_linea_con_contenido;indice++){
			resultado[indice] = linea[indice];
		}
		return resultado;
	}
	
	
	public int getPosicionUltimaLineaVacia(String []linea){
		int posicion	= -1;
		
		for(int indice=0;indice<linea.length;indice++){
			if(esLineaVacia(linea[indice])){
				posicion = indice;
			}
		}
		return posicion;
	}
	 		
	public String arrayToString(String []lineas, String delimitador){
		StringBuffer buffer = new StringBuffer();
		
		if(lineas 		== null) return null;
		if(delimitador == null) delimitador = "";
		
		for(int i=0;i<lineas.length;i++){
			buffer.append(lineas[i]);
			if(i<(lineas.length-1)){
				buffer.append(delimitador);
			}
		}
		
		return buffer.toString();
	}
	
	public String[] getSubArray(String []lineas, int inicio, int fin){
		
		if(lineas == null) return null;
		
		int indiceMaximo = lineas.length -1;
		
		if(inicio 	< 0) 				inicio 	= 0;
		if(fin 		< 0) 				fin		= 0;
		if(fin		> indiceMaximo ) 	fin 	= indiceMaximo;
		if(inicio   > fin		) 		return null;
		
		// Calcular la Longitud del Nuevo Array
		int 	newArraySize 	= fin - inicio + 1;
		String 	[]resultado 	= new String[newArraySize];	 
		
		// Extraer los elementos correspondientes
		for(int i=inicio;i<=fin;i++){
			resultado[i-inicio]=lineas[i];
		}

		return resultado;
		
	}
	
	/**
	 *	Reemplaza en la cadena de texto proporcionada en el argumento <tt>cadena</tt> 
	 * cada una de las ocurrencias de las cadenas: "[1]", "[2]", "[3]", "[4]", "[5]", "[6]", "[7]", "[8]", "[9]",
	 * "[10]", "[11]", "[12]" y "[13]" por los valores almacenados en los atributos de clase.
	 * Significado de las claves:
	 * 	[1] Razon Social EPO (Atributo <tt>razonSocialEPO</tt>)
	 * 	[2] Lista de Intermediarios con Operacion M.N. (Atributo <tt>listaDeIntermediariosConOperacion</tt>)
	 * 	[3] Total Intermediarios con Operacion M.N. (Atributo <tt>totalIntermediariosConOperacion</tt>)
	 *    [4] Lista de Intermediarios con Operacion USD (Atributo <tt>listaDeIntermediariosConOperacionUSD</tt>)
	 *    [5] Total Intermediarios con Operacion USD (Atributo <tt>totalIntermediariosConOperacion</tt>)
	 * 	[6] Monto Documentos No Operados M.N. (Atributo <tt>montoDocumentosNoOperados</tt>)
	 * 	[7] Monto Documentos No Operados USD (Atributo <tt>montoDocumentosNoOperadosUSD</tt>)
	 * 	[8] Total a Dispersar M.N. (Atributo <tt>totalADispersar</tt>)
	 * 	[9] Total a Dispersar USD (Atributo <tt>totalADispersarUSD</tt>)
	 * 	[10] Fecha de Vencimiento (Atributo <tt>fechaDeVencimiento</tt>)
	 * 	[11] Fecha a Depositar (Atributo <tt>fechaADepositar</tt>)
	 * 	[12] Total No Dispersado M.N. (Atributo <tt>totalNoDispersado</tt>)
	 * 	[13] Total No Dispersado USD (Atributo <tt>totalNoDispersadoUSD</tt>)
	 * @param Cadena Argumento de tipo <tt>java.lang.String</tt>. 
	 * @return Cadena de texto con los corchetes reemplazados.
	 */
	public String reemplazaCorchetesConEtiquetasHTML(String cadena){
		String 		 resultado  = cadena;
		String 		 reemplazo  = null;
		String 		 []lineas	= null;
		StringBuffer buffer		= null;
		
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaNO  		+ "NO" 										+ "</span>");
		resultado = resultado.replaceAll("NO", 		reemplazo);
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaUNO 		+ razonSocialEPO 							+ "</span>");
		resultado = resultado.replaceAll("\\[1\\]",	reemplazo);
 
		// Reemplazar [2] con lista de elementos
		lineas 	= 	resultado.split("\n");
		buffer	=	new StringBuffer();
		for(int i=0;i<lineas.length;i++){
			String linea = (String) lineas[i];
			if(linea.indexOf("[2]") != -1 ){ // Se encontro [2]
				buffer.append("<table align=\"center\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\" style=\"padding-left:0px;border-collapse:collapse;\">\n");
				for(int j=0;j<listaDeIntermediariosConOperacion.size();j++){
					HashMap registro = (HashMap) listaDeIntermediariosConOperacion.get(j);
					
					buffer.append("<tr>");
					buffer.append(etiquetaDOSbanco);
					buffer.append((String) registro.get("INTERMEDIARIO"));
					buffer.append("</td>");
					buffer.append(etiquetaDOSmonto);
					buffer.append((String) registro.get("MONTO"));
					buffer.append("</td>");
					buffer.append("</tr>");
					buffer.append("\n");
				}
				buffer.append("</table>\n");
			}else{
				buffer.append(linea);
				buffer.append("\n");
			}
		}
		resultado = buffer.toString();
		
		// Reemplazar [3] 
		lineas 	= 	resultado.split("\n");
		buffer	=	new StringBuffer();
		for(int i=0;i<lineas.length;i++){
			String linea = (String) lineas[i];
			if(linea.indexOf("[3]") != -1 ){ // Se encontro [3]
				buffer.append("<table align=\"center\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\" style=\"padding-left:0px;border-collapse:collapse;\">\n");	
				buffer.append("<tr>");
				buffer.append(etiquetaTRESbanco);
				buffer.append("TOTAL");
				buffer.append("</td>");
				buffer.append(etiquetaTRESmonto);
				buffer.append(totalIntermediariosConOperacion);
				buffer.append("</td>");
				buffer.append("</tr>");
				buffer.append("\n");
				buffer.append("</table>\n");
			}else{
				buffer.append(linea);
				buffer.append("\n");
			}
		}
		resultado = buffer.toString();
		
		// Reemplazar [4] con lista de elementos
		lineas 	= 	resultado.split("\n");
		buffer	=	new StringBuffer();
		for(int i=0;i<lineas.length;i++){
			String linea = (String) lineas[i];
			if(linea.indexOf("[4]") != -1 ){ // Se encontro [3]
				buffer.append("<table align=\"center\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\" style=\"padding-left:0px;border-collapse:collapse;\">\n");
				for(int j=0;j<listaDeIntermediariosConOperacionUSD.size();j++){
					HashMap registro = (HashMap) listaDeIntermediariosConOperacionUSD.get(j);
					
					buffer.append("<tr>");
					buffer.append(etiquetaCUATRObanco);
					buffer.append((String) registro.get("INTERMEDIARIO"));
					buffer.append("</td>");
					buffer.append(etiquetaCUATROmonto);
					buffer.append((String) registro.get("MONTO"));
					buffer.append("</td>");
					buffer.append("</tr>");
					buffer.append("\n");
				}
				buffer.append("</table>\n");
			}else{
				buffer.append(linea);
				buffer.append("\n");
			}
		}
		resultado = buffer.toString();

		// Reemplazar [5] 
		lineas 	= 	resultado.split("\n");
		buffer	=	new StringBuffer();
		for(int i=0;i<lineas.length;i++){
			String linea = (String) lineas[i];
			if(linea.indexOf("[5]") != -1 ){ // Se encontro [5]
				buffer.append("<table align=\"center\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\" style=\"padding-left:0px;border-collapse:collapse;\">\n");	
				buffer.append("<tr>");
				buffer.append(etiquetaCINCObanco);
				buffer.append("TOTAL");
				buffer.append("</td>");
				buffer.append(etiquetaCINCOmonto);
				buffer.append(totalIntermediariosConOperacionUSD);
				buffer.append("</td>");
				buffer.append("</tr>");
				buffer.append("\n");
				buffer.append("</table>\n");
			}else{
				buffer.append(linea);
				buffer.append("\n");
			}
		}
		resultado = buffer.toString();
		
		// MONTO DOCUMENTOS NO OPERADOS (M.N. Y USD)
		lineas 	= 	resultado.split("\n");
		buffer	=	new StringBuffer();
		for(int i=0;i<lineas.length;i++){
			String linea = (String) lineas[i];
			if(linea.indexOf("[6]") != -1 || linea.indexOf("[7]") != -1){ // Se encontro [3]
				buffer.append("<center>" + linea + "</center>");
				buffer.append("\n");
			}else{
				buffer.append(linea);
				buffer.append("\n");
			}	
		}
		resultado = buffer.toString();
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaSEIS 		+ montoDocumentosNoOperados 			+ "</span>");
		resultado = resultado.replaceAll("\\[6\\]",	reemplazo);	
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaSIETE 	+ montoDocumentosNoOperadosUSD 		+ "</span>");
		resultado = resultado.replaceAll("\\[7\\]",	reemplazo);
		
		// TOTAL A DISPERSAR (M.N. Y USD)
		lineas 	= 	resultado.split("\n");
		buffer	=	new StringBuffer();
		for(int i=0;i<lineas.length;i++){
			String linea = (String) lineas[i];
			if(linea.indexOf("[8]") != -1 || linea.indexOf("[9]") != -1){ // Se encontro [3]
				buffer.append("<center>" + linea + "</center>");
				buffer.append("\n");
			}else{
				buffer.append(linea);
				buffer.append("\n");
			}	
		}
		resultado = buffer.toString();
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaOCHO 	+ totalADispersar							+ "</span>");
		resultado = resultado.replaceAll("\\[8\\]",	reemplazo);
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaNUEVE	+ totalADispersarUSD						+ "</span>");
		resultado = resultado.replaceAll("\\[9\\]",	reemplazo);
		
		// FECHA DE VENCIMIENTO
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaDIEZ 	+ fechaDeVencimiento						+ "</span>");
		resultado = resultado.replaceAll("\\[10\\]",	reemplazo);
		
		// FECHA A DEPOSITAR
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaONCE	+ fechaADepositar							+ "</span>");
		resultado = resultado.replaceAll("\\[11\\]",	reemplazo);
		
		// TOTAL NO DISPERSADO (M.N. Y USD)
		lineas 	= 	resultado.split("\n");
		buffer	=	new StringBuffer();
		for(int i=0;i<lineas.length;i++){
			String linea = (String) lineas[i];
			if(linea.indexOf("[12]") != -1 || linea.indexOf("[13]") != -1){ // Se encontro [3]
				buffer.append("<center>" + linea + "</center>");
				buffer.append("\n");
			}else{
				buffer.append(linea);
				buffer.append("\n");
			}	
		}
		resultado = buffer.toString();
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaDOCE 	+ totalNoDispersado						+ "</span>");
		resultado = resultado.replaceAll("\\[12\\]",	reemplazo);
		reemplazo = deshabilitarSignificadoDeCaracteresEspeciales(etiquetaTRECE	+ totalNoDispersadoUSD					+ "</span>");
		resultado = resultado.replaceAll("\\[13\\]",	reemplazo);
		
		return resultado;
	}
	
	public String deshabilitarSignificadoDeCaracteresEspeciales(String cadena) {
	
		
        if((cadena.indexOf('$') == -1) && (cadena.indexOf('\\') == -1)){
            return cadena;
		  }
		  
		  StringBuffer buffer = new StringBuffer();  
		  
        for (int i=0; i<cadena.length(); i++) {
            char car = cadena.charAt(i);
				if (car == '$') {
                 buffer.append('\\');  
					  buffer.append('$');
            } else if (car == '\\') {
                 buffer.append('\\');  
					  buffer.append('\\');
            } else {
                 buffer.append(car);
            }
        }
		  
        return  buffer.toString();
    }

}