package com.netro.dispersion.notificaciones;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.MessageFormat;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.Fecha;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;

/**
 *
 * Permite realizar notificaciones v�a correo electr�nico
 * a los usuarios NAFIN y Clientes NAFIN de L�nea Cr�dito que su Estado de
 * Cuenta fue generado y que se puede consultar en la aplicaci�n
 * NAFINET.
 *
 * @since	F001 - 2015 -- PAGOS - Vencimientos 1er Piso.
 *				Proceso: Notificar Estados de Cuenta generados.
 * @author	jshernandez
 * @date		24/04/2015 01:00:28 p.m.
 *
 */
@Stateless(name = "EdoCuentaLineaCreditoEJB" , mappedName = "EdoCuentaLineaCreditoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class EdoCuentaLineaCreditoBean implements EdoCuentaLineaCredito  {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(EdoCuentaLineaCreditoBean.class);
	
	/**
	 * Verifica si hay Estados de Cuenta Generados.
	 * @throws AppException
	 * 
	 * @return <tt>true</tt> si hay estados de cuenta generados y <tt>false</tt> en caso contrario.
	 * 
	 * @param fechaActual				   String con la fecha de d�a en curso, con formato dd/MM/yyyy.
	 * @param fechaUltimoDiaMesAnterior String con la fecha del �ltimo d�a del mes anterior, 
	 * 											con formato dd/MM/yyyy.
	 *
	 * @since	F001 - 2015 -- PAGOS - Vencimientos 1er Piso.
	 *				Proceso: Notificar Estados de Cuenta generados.
	 * @author	jshernandez
	 * @date		24/04/2015 01:00:28 p.m.
	 *
	 */
	private boolean hayEdosCuentaGenerados(String fechaActual,String fechaUltimoDiaMesAnterior)
		throws AppException {

		log.info("hayEdosCuentaGenerados(E)");
		
		AccesoDB 			con 				= new AccesoDB();
		StringBuffer 		query 			= new StringBuffer(126);
		ResultSet			rs					= null;
		PreparedStatement ps					= null;
		
		boolean				hayEdosCuenta 	= false;
		
		try {

			con.conexionDB();
			
			query.append(
				"SELECT                                                      			"  +
				"  DECODE(COUNT(1),0,'false','true') AS HAY_ESTADOS_CUENTA           "  +
				"FROM                                                                "  +
				"  COMHIS_PROC_EDO_CUENTA                                            "  +
				"WHERE                                                               "  +
				"  DF_FECHAFINMES              >= TO_DATE(?,'DD/MM/YYYY')     AND  "  +
				"  DF_FECHAFINMES              <  TO_DATE(?,'DD/MM/YYYY') + 1 AND  "  +
				"  DF_FECHA_HORA               >= TO_DATE(?,'DD/MM/YYYY')     AND  "  +
				"  DF_FECHA_HORA               <  TO_DATE(?,'DD/MM/YYYY') + 1      " 
			);
		 
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,fechaUltimoDiaMesAnterior	);
			ps.setString(2,fechaUltimoDiaMesAnterior	);
			ps.setString(3,fechaActual						);
			ps.setString(4,fechaActual						);
			rs = ps.executeQuery();
			if( rs.next() ){
				hayEdosCuenta = "true".equals(rs.getString("HAY_ESTADOS_CUENTA"))?true:false;
			}

		} catch(Exception e) {
			
			log.error("hayEdosCuentaGenerados(Exception)");
			log.error("hayEdosCuentaGenerados.fechaActual               = <" + fechaActual               + ">");
			log.error("hayEdosCuentaGenerados.fechaUltimoDiaMesAnterior = <" + fechaUltimoDiaMesAnterior + ">");
			log.error("hayEdosCuentaGenerados.query 							= <" + query 							+ ">");
			e.printStackTrace();
			
			throw new AppException("Fall� la consulta para determinar si hay Estados de Cuenta Generados:",e);
			
		} finally {
			
			if( rs  != null ){ try{ rs.close();  }catch(Exception e){} }
			if( ps  != null ){ try{ ps.close();  }catch(Exception e){} }
					
			if( con.hayConexionAbierta() ){
				con.cierraConexionDB();
			}
					
			log.info("hayEdosCuentaGenerados(S)");
			
		}
		
		return hayEdosCuenta;
		
	}
	
	/**
	 * Registra en bit�cora COMHIS_PROC_EDO_CUENTA el env�o del correo de notificaci�n de el Estado
	 * de cuenta generado.
	 *
	 * @throws AppException
	 * 
	 * @return 
	 * 
	 * @param fechaActual				   String con la fecha de d�a en curso, con formato dd/MM/yyyy.
	 * @param fechaUltimoDiaMesAnterior String con la fecha del �ltimo d�a del mes anterior, 
	 * 											con formato dd/MM/yyyy.
	 * @param campoUpdate				   String con el nombre del campo de la tabla COMHIS_PROC_EDO_CUENTA 
	 *												que ser� actualizado a 'S', para indicar que la notificaci�n 
	 *                                  fue realizada.
	 *
	 * @since	F001 - 2015 -- PAGOS - Vencimientos 1er Piso.
	 *				Proceso: Notificar Estados de Cuenta generados.
	 * @author	jshernandez
	 * @date		24/04/2015 01:00:28 p.m.
	 *
	 */
	private void registraNotificacion(String fechaUltimoDiaMesAnterior,List clavesProcEdoCuenta,String campoUpdate)
		throws AppException {
		
		log.info("registraNotificacion(E)");
		
		AccesoDB 			con 				= new AccesoDB();
		StringBuffer 		sentence			= new StringBuffer(48);
		PreparedStatement	ps					= null;
		
		boolean				exito 			= true;
		
		try {

			con.conexionDB();
			
			StringBuffer variablesBind = new StringBuffer();
			for(int i=0;i<clavesProcEdoCuenta.size();i++){
				if( i>0 ){ variablesBind.append(","); }
				variablesBind.append("?");
			}
			
			sentence.append(
				"UPDATE "  +
				"	COMHIS_PROC_EDO_CUENTA "  +
				"SET "  +
				"	");
			sentence.append(
				campoUpdate
			);
			sentence.append(
				" = 'S' "  +
				"WHERE "  +
				"   DF_FECHAFINMES  			>= TO_DATE(?,'DD/MM/YYYY')     AND "  +
				"   DF_FECHAFINMES  			<  TO_DATE(?,'DD/MM/YYYY') + 1 AND "  +
				"   IC_PROC_EDO_CUENTA IN ( "
			);
			sentence.append(
				variablesBind
			);
			sentence.append(
				" )  "
			);
		 
			ps = con.queryPrecompilado(sentence.toString());
			int indice = 1;
			ps.setString(indice++,fechaUltimoDiaMesAnterior);
			ps.setString(indice++,fechaUltimoDiaMesAnterior);
			for(int i=0;i<clavesProcEdoCuenta.size();i++){
				ps.setString(indice++,(String)clavesProcEdoCuenta.get(i));
			}
			ps.executeUpdate();	

		} catch(Exception e) {
			
			exito = false;
			
			log.error("registraNotificacion(Exception)");
			log.error("registraNotificacion.fechaUltimoDiaMesAnterior = <" + fechaUltimoDiaMesAnterior + ">");
			log.error("registraNotificacion.clavesProcEdoCuenta       = <" + clavesProcEdoCuenta       + ">");
			log.error("registraNotificacion.sentence 					    = <" + sentence 					    + ">");
			e.printStackTrace();
			
			throw new AppException("No se pudo registrar la notificaci�n exitosa.",e);
			
		} finally {
			
			if( ps  != null ){ try{ ps.close();  }catch(Exception e){} }
					
			if( con.hayConexionAbierta() ){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
					
			log.info("registraNotificacion(S)");
			
		}

	}
	
	/** 
	 * Notificar Estados de Cuenta generados a Clientes NAFIN de L�nea de 
	 * Cr�dito y Usuarios NAFIN.
	 * @throws Exception
	 *
	 * @since	F001 - 2015 -- PAGOS - Vencimientos 1er Piso.
	 *				Proceso: Notificar Estados de Cuenta generados.
	 * @author	jshernandez
	 * @date		24/04/2015 01:00:28 p.m.
	 *
	 */
	public void notificarEdoCuentaGeneradoNafin(String fechaActual, String fechaUltimoDiaMesAnterior) 
		throws Exception { 

		log.info("notificarEdoCuentaGeneradoNafin(E)");

		try {
			
			if( fechaActual == null ){
				fechaActual 					= Fecha.getFechaActual();
			}
			if( fechaUltimoDiaMesAnterior == null ){
				fechaUltimoDiaMesAnterior	= Fecha.calculaUltimoDiaDelMes(fechaActual, "dd/MM/yyyy", -1); // -1 => MES ANTERIOR
			}
	
			// Determinar si se generaron Estados de Cuenta en el d�a en curso		
			if( hayEdosCuentaGenerados(fechaActual,fechaUltimoDiaMesAnterior) ){
			
				/*
					1.- Proceso que notificar� v�a correo electr�nico a los usuarios NAFIN 
					el listado de Clientes que ya cuentan con estado de cuenta generado.
				*/
				enviaListaClientesUsuariosNafin(fechaActual,fechaUltimoDiaMesAnterior);
			
			} else {
			
				/*
					2.- Proceso que notificar� v�a correo electr�nico a los clientes de 
					L�nea Cr�dito (clientes externos, Intermediarios Financieros e 
					Intermediarios Financieros No Bancarios) que su �ltimo Estado de 
					Cuenta ya fue generado y registrado en NAFINET para su consulta.
				*/
				notificaEdoCuentaClientesNafin(fechaActual,fechaUltimoDiaMesAnterior);
				
			}
		
		} catch(Exception e){
			
			log.error("notificarEdoCuentaGeneradoNafin(Exception)");
			log.error("notificarEdoCuentaGeneradoNafin.fechaActual               = <" + fechaActual               + ">");
			log.error("notificarEdoCuentaGeneradoNafin.fechaUltimoDiaMesAnterior = <" + fechaUltimoDiaMesAnterior + ">");
			e.printStackTrace();
			
			throw e;
			
		} finally {
			log.info("notificarEdoCuentaGeneradoNafin(S)");
		}
		
	}

	/**
	 * 
	 * Notificar v�a correo electr�nico a los usuarios NAFIN el listado 
	 * de clientes que ya cuentan con estado de cuenta generado.
	 * @throws Exception
	 * 
	 * @param fechaActual				   String con la fecha de d�a en curso, con formato dd/MM/yyyy.
	 * @param fechaUltimoDiaMesAnterior String con la fecha del �ltimo d�a del mes anterior, 
	 * 											con formato dd/MM/yyyy.
	 *
	 * @since	F001 - 2015 -- PAGOS - Vencimientos 1er Piso.
	 *				Proceso: Notificar Estados de Cuenta generados.
	 * @author	jshernandez
	 * @date		24/04/2015 01:00:28 p.m.
	 *
	 */
	public void enviaListaClientesUsuariosNafin(String fechaActual, String fechaUltimoDiaMesAnterior) 
		throws Exception {

		log.info("enviaListaClientesUsuariosNafin(E)");
		log.info("Inicio notificaci�n a Usuarios NAFIN del Estado de cuenta generado.");
		
		AccesoDB 			con 						= new AccesoDB();
		StringBuffer 		query						= new StringBuffer(126);
		ResultSet			rs							= null;
		PreparedStatement ps							= null;
			
		boolean 				exito 					= true;
			
		StringBuffer 		detalleClientes 		= new StringBuffer(4096);
		String 				correosUsuariosNafin = "";
		List 					clavesProcEdoCuenta 	= new ArrayList();
		
		try {

			// NOTIFICAR V�A CORREO ELECTR�NICO A LOS USUARIOS NAFIN EL LISTADO 
			// DE CLIENTES QUE YA CUENTAN CON ESTADO DE CUENTA GENERADO.
			// ..........................................................................................
				
			// 1. Obtener los clientes que operan con L�NEA DE CR�DITO que cuentan 
			// con N�mero de Cliente SIRAC registrados en el sistema NAFINET.
				
			// Conectarse a la Base de Datos
			con.conexionDB();
			
			// Consultar lista de reportes generados durante el d�a.
			query.setLength(0);
			query.append(
				"SELECT DISTINCT                                                     "  +
				"  IC_PROC_EDO_CUENTA                                                "  +
				"FROM                                                                "  +
				"  COMHIS_PROC_EDO_CUENTA                                            "  +
				"WHERE                                                               "  +
				"  DF_FECHAFINMES              >= TO_DATE(?,'DD/MM/YYYY')     AND  "  +
				"  DF_FECHAFINMES              <  TO_DATE(?,'DD/MM/YYYY') + 1 AND  "  +
				"  CS_CORREO_LINCRED_NAFIN     =  'N'                           AND  "  +
				"  DF_FECHA_HORA               >= TO_DATE(?,'DD/MM/YYYY')     AND  "  +
				"  DF_FECHA_HORA               <  TO_DATE(?,'DD/MM/YYYY') + 1      " 
			);
		 
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,fechaUltimoDiaMesAnterior	);
			ps.setString(2,fechaUltimoDiaMesAnterior	);
			ps.setString(3,fechaActual						);
			ps.setString(4,fechaActual						);
			rs = ps.executeQuery();
			while( rs.next() ){
				clavesProcEdoCuenta.add(rs.getString("IC_PROC_EDO_CUENTA"));
			}
			rs.close();
			ps.close();
				
			// Verificar que estados de cuenta se encuentran generados
			if(clavesProcEdoCuenta.size() == 0){
				log.info("No hay ning�n nuevo estado de cuenta generado durante el d�a, se cancela la notificaci�n a Usuarios NAFIN.");
				log.info("enviaListaClientesUsuariosNafin(S)");
				return;
			}
	
			// 2. Obtener lista de direcciones de correo de Usuarios NAFIN a los que se les enviar� la notificaci�n
					
			query.setLength(0);
			query.append(
				"SELECT                               "  +
				"  CG_NOTIF_USUARIOS_NAFIN AS CORREOS "  +
				"FROM                                 "  +
				"  COM_PARAM_GRAL                     "  +
				"WHERE                                "  +
				"  IC_PARAM_GRAL = 1                  "
			);
						
			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				correosUsuariosNafin = rs.getString("CORREOS");
			}
			rs.close();
			ps.close();
			
			if( Comunes.esVacio(correosUsuariosNafin) ){
				throw new AppException("Favor de parametrizar al menos una direcci�n de correo en COM_PARAM_GRAL.CG_NOTIF_USUARIOS_NAFIN tal que IC_PARAM_GRAL = 1.");
			}
							
			// Validar que las direcciones de correo proporcionadas sean correctas.
			String[] correosUsuariosNafin01 = correosUsuariosNafin.split(",",-1);
			for(int i=0;i<correosUsuariosNafin01.length;i++){
				correosUsuariosNafin01[i] = correosUsuariosNafin01[i].trim();
				if(!Comunes.validaEmail(correosUsuariosNafin01[i])){
					log.info(
						"La direcci�n de correo: '" + correosUsuariosNafin01[i] + "' del usuario NAFIN no es correcta, por lo que no podr� ser notificado."
					);
					correosUsuariosNafin01[i] = null;
				}
			}
							
			// Reconstruir lista de correos
			correosUsuariosNafin = "";
			for( int i=0,ctaCorreos=0; i<correosUsuariosNafin01.length; i++ ){
				if( correosUsuariosNafin01[i] != null ){
					if(ctaCorreos>0) correosUsuariosNafin += ",";
					correosUsuariosNafin += correosUsuariosNafin01[i];
					ctaCorreos++;
				}
			}
			if( Comunes.esVacio(correosUsuariosNafin) ){
				throw new AppException("Ninguna de la cuentas de correo parametrizadas en COM_PARAM_GRAL.CG_NOTIF_USUARIOS_NAFIN tal que IC_PARAM_GRAL = 1 es v�lida.");
			}
							
			// 3. Construir tabla con el detalle de los Clientes cuyo estados de cuenta 
			//    fueron generados durante el d�a.

			detalleClientes.append(
				"		<table class=\"center\" style=\"border-collapse: collapse;\" > "  +
				"			<tr> "  +
				"				<td class=\"cabecera\" style=\"width:70%;\">Nombre Cliente</td> "  +
				"				<td class=\"cabecera\" style=\"width:30%;font-weight:bold;text-align:left;border:1pt solid black;padding:4pt;\">No. Cliente SIRAC</td> "  +
				"			</tr> " 
			);
			
			// Preparar consulta
			StringBuffer variablesBind = new StringBuffer();
			for(int i=0;i<clavesProcEdoCuenta.size();i++){
				if( i>0 ){ variablesBind.append(","); }
				variablesBind.append("?");
			}
						
			/* 
			 *  Nota de dise�o by jshernandez (29/04/2015 06:48:53 p.m.):
			 *  En el correo de notificaci�n se excluyen los clientes externos e intermediarios
			 *  financieros inactivos.
			 */
			query.setLength(0);
			query.append(
				"SELECT                                                                 "  +
				"  CG_NOMBRECLIENTE,                                                    "  +
				"  IG_CLIENTE                                                           "  +
				"FROM                                                                   "  +
				"  (                                                                    "  +
				"    SELECT /*+ USE_NL(EC CE) */ DISTINCT                               "  +
				"      EC.CG_NOMBRECLIENTE,                                             "  +
				"      EC.IG_CLIENTE                                                    "  +
				"    FROM                                                               "  +
				"      COM_ESTADO_CUENTA       EC,                                      "  +
				"      COMCAT_CLI_EXTERNO      CE                                       "  +
				"    WHERE                                                              "  +
				"        EC.DF_FECHAFINMES   >=  TO_DATE(?,'DD/MM/YYYY')                "  +
				"    AND EC.DF_FECHAFINMES   <   TO_DATE(?,'DD/MM/YYYY') + 1            "  +
				"    AND EC.IC_PROC_EDO_CUENTA IN ("
			);
			query.append(
				variablesBind
			);
			query.append(	
				")                                                                      "  +
				"    AND EC.IC_IF            =   12                                     "  +
				"    AND EC.IG_CLIENTE       =   CE.IN_NUMERO_SIRAC                     "  +
				"    AND CE.CS_HABILITADO    =   'S'                                    "  +
				"    UNION                                                              "  +
				"    SELECT /*+ use_nl(EC OPEIF I) */ DISTINCT                          "  +
				"      EC.CG_NOMBRECLIENTE,                                             "  +
				"      EC.IG_CLIENTE                                                    "  +
				"    FROM                                                               "  +
				"      COM_ESTADO_CUENTA EC,                                            "  +
				"      OPE_DATOS_IF       OPEIF,                                        "  +
				"      COMCAT_IF          I                                             "  +
				"    WHERE                                                              "  +
				"        EC.DF_FECHAFINMES   >=  TO_DATE(?,'DD/MM/YYYY')                "  +
				"    AND EC.DF_FECHAFINMES   <   TO_DATE(?,'DD/MM/YYYY') + 1            "  +
				"    AND EC.IC_PROC_EDO_CUENTA IN ("
			);
			query.append(
				variablesBind
			);
			query.append(	
				")                                                                      "  +
				"    AND EC.IC_IF           = 12                                        "  +
				"    AND OPEIF.IC_IF        = I.IC_IF                                   "  +
				"    AND I.CS_HABILITADO    = 'S'                                       "  +
				"    AND EC.IG_CLIENTE      = I.IN_NUMERO_SIRAC                         "  +
				"  )                                                                    "  +
				"  A                                                                    "  +
				"ORDER BY                                                               "  +
				"  CG_NOMBRECLIENTE                                                     "
			);
			
			ps = con.queryPrecompilado(query.toString());
			
			int indice = 1;
			ps.setString(indice++,fechaUltimoDiaMesAnterior);
			ps.setString(indice++,fechaUltimoDiaMesAnterior);
			for(int i=0;i<clavesProcEdoCuenta.size();i++){
				ps.setString(indice++,(String)clavesProcEdoCuenta.get(i));
			}
			ps.setString(indice++,fechaUltimoDiaMesAnterior);
			ps.setString(indice++,fechaUltimoDiaMesAnterior);
			for(int i=0;i<clavesProcEdoCuenta.size();i++){
				ps.setString(indice++,(String)clavesProcEdoCuenta.get(i));
			}
			rs = ps.executeQuery();

			boolean hayRegistros = false;
			while( rs.next() ){
				
				String cgNombrecliente 	= ( rs.getString("CG_NOMBRECLIENTE")== null)?"":rs.getString("CG_NOMBRECLIENTE");
				String igCliente 			= ( rs.getString("IG_CLIENTE")		== null)?"":rs.getString("IG_CLIENTE");
				
				hayRegistros				= true;
				
				// Agregar detalle de los clientes
				detalleClientes.append(
					"			<tr> "  +
					"				<td class=\"celda\" >"
				);
				detalleClientes.append(
					StringEscapeUtils.escapeHtml(cgNombrecliente)
				);
				detalleClientes.append(
					"</td> "  +
					"				<td class=\"celda\" >"
				);
				detalleClientes.append(
					StringEscapeUtils.escapeHtml(igCliente)
				);
				detalleClientes.append(
					"</td> "  +
					"			</tr> "
				);
			
			}
			rs.close();
			ps.close();
			
			// En dado caso que no haya registros, indicarlo en el mensaje
			if(!hayRegistros){
				detalleClientes.append(
					"			<tr> "  +
					"				<td class=\"celda\" colspan=\"2\" > " +
					"					No se encontraron registros. "  +
					"				</td> "  +
					"			</tr> " 
				);
			}
			
			detalleClientes.append(
					"		</table> " 
			);
						
		} catch(Exception e){
			
			log.error("enviaListaClientesUsuariosNafin(Exception)");
			log.error("enviaListaClientesUsuariosNafin.fechaActual               = <" + fechaActual               + ">");
			log.error("enviaListaClientesUsuariosNafin.fechaUltimoDiaMesAnterior = <" + fechaUltimoDiaMesAnterior + ">");
			log.error("enviaListaClientesUsuariosNafin.query                     = <" + query                     + ">");
			e.printStackTrace();
			
			log.info("enviaListaClientesUsuariosNafin(S)");
			
			throw e;
			
		} finally {
			
			if( rs  != null ){ try{ rs.close();  }catch(Exception e){} }
			if( ps  != null ){ try{ ps.close();  }catch(Exception e){} }
					
			if( con.hayConexionAbierta() ){
				con.cierraConexionDB();
			}
			
		}
		
		try {
			
			// 3. Construir mensaje completo de correo
			String mensaje = MessageFormat.format(
				"<html> "  +
				"	<head> "  +
				"		<title>Estados de Cuenta Generados</title> "  +
				"		<style> "  +
				"			td.cabecera '{ "  +
				"				 font-weight: bold;text-align:left;border:1pt solid black;padding:4pt; "  +
				"			}' "  +
				"			td.celda '{ "  +
				"				 text-align:left;border:1pt solid black;padding:4pt; "  +
				"			}' "  +
				"			table.center '{ "  +
				"				width:90%;  "  +
				"				margin-left:5%;  "  +
				"				margin-right:5%; "  +
				"		   }' "  +
				"		</style> "  +
				"	</head> "  +
				"	<body style=\"font-family:Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;font-size:11.0pt;color:black;\" >	 "  +
				"		<br> "  +
				"		<div style=\"font-style:italic;\" >Por medio del presente se le notifica que los siguientes Estados de Cuenta a la fecha de corte {0} se han generado.</div> "  +
				"		<br> "  +
				"		{1}  "  +
				"		<br> "  +
				"		<br> "  +
				"		<div style=\"\">ATENTAMENTE.</div> "  +
				"		<div style=\"font-style:italic;font-weight:bold;\" > "  +
				"			Nacional Financiera, S.C.N. "  +
				"		</div> "  +
				"		<br> "  +
				"		<div style=\"font-style:italic;\" > "  +
				"			<span style=\"font-weight:bold;\">Nota:</span> Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr&oacute;nicos de entrada.<br> "  +
				"			Por favor no responda a este mensaje. "  +
				"		</div> "  +
				"	</body> "  +
				"</html> ",
				new Object[]{
					StringEscapeUtils.escapeHtml(fechaUltimoDiaMesAnterior),
					detalleClientes
				}
			); 
			
			// 4. Enviar Correo de Notificaci�n.
			
			// Agregar remitente
			String 			remitente 				= "no_response@nafin.gob.mx";
			// Agregar los siguientes destinatarios
			String 			destinatarios			= correosUsuariosNafin;
			// No se agregar� destinatarios de copia de carbon
			String 			ccDestinatarios		= null;
			// Agregar titulo del mensaje
			String 			tituloMensaje			= "Estados de Cuenta Generados" ;
			// Agregar mensaje
			String			mensajeCorreo 			= mensaje.toString();
			
			// Agregar Archivos de Imagenes 
			ArrayList 		listaDeImagenes		= null;
			// Agregar archivo adjunto
			ArrayList 		listaDeArchivos		= null;
			
			// Enviar correo
			Correo correo = new Correo();
			correo.disableDebug();
			correo.enviaCorreoConDatosAdjuntos(
				remitente,
				destinatarios,
				ccDestinatarios,
				tituloMensaje,
				mensajeCorreo,
				listaDeImagenes,
				listaDeArchivos
			);

			//5. Registrar el env�o de los correos.
			registraNotificacion(fechaUltimoDiaMesAnterior,clavesProcEdoCuenta,"CS_CORREO_LINCRED_NAFIN");	
						
			log.info("Finalizo notificaci�n a Usuarios NAFIN del Estado de cuenta generado.");
						
		} catch(Exception e){
			
			log.error("enviaListaClientesUsuariosNafin(Exception)");
			log.error("enviaListaClientesUsuariosNafin.fechaActual               = <" + fechaActual               + ">");
			log.error("enviaListaClientesUsuariosNafin.fechaUltimoDiaMesAnterior = <" + fechaUltimoDiaMesAnterior + ">");
			e.printStackTrace();
					
			throw e;
			
		} finally {
					
			log.info("enviaListaClientesUsuariosNafin(S)");
					
		}
				
	}

	/**
	 * 
	 * Notifica v�a correo electr�nico a los Clientes de Nafin de la Linea de Cr�dito (1er piso) 
	 * que su �ltimo Estado de Cuenta ya fue generado y registrado en NAFINET para su consulta.
	 * @throws Exception
	 * 
	 * @param fechaActual				   String con la fecha de d�a en curso, con formato dd/MM/yyyy.
	 * @param fechaUltimoDiaMesAnterior String con la fecha del �ltimo d�a del mes anterior, 
	 * 											con formato dd/MM/yyyy.
	 *
	 * @since	F001 - 2015 -- PAGOS - Vencimientos 1er Piso.
	 *				Proceso: Notificar Estados de Cuenta generados.
	 * @author	jshernandez
	 * @date		24/04/2015 01:00:28 p.m.
	 *
	 */
	public void notificaEdoCuentaClientesNafin(String fechaActual, String fechaUltimoDiaMesAnterior) 
		throws Exception { 
			
			log.info("notificaEdoCuentaClientesNafin(E)");
			
			AccesoDB 			con 							= new AccesoDB();
			StringBuffer 		query							= new StringBuffer(126);
			ResultSet			rs								= null;
			PreparedStatement ps								= null;

			List 					clavesProcEdoCuenta		= new ArrayList();
			String 				fechaGeneracionReporte 	= null; 
			String 				diasInhabiles 				= null;
			
			// Generar Id de Proceso - �til para detectar si hubo errores en la notificaci�n 
			// individual de clientes.
			final long 			ID_PROCESO					= System.currentTimeMillis();
			
			try {
				
				con.conexionDB();
				
				// NOTIFICAR V�A CORREO ELECTR�NICO A LOS CLIENTES DE L�NEA CR�DITO (CLIENTES EXTERNOS, 
				// INTERMEDIARIOS FINANCIEROS E INTERMEDIARIOS FINANCIEROS NO BANCARIOS) QUE SU �LTIMO 
				// ESTADO DE CUENTA YA FUE GENERADO Y REGISTRADO EN NAFINET PARA SU CONSULTA
				// ..........................................................................................
				
				// 1. Definir Par�metros Iniciales
				
				// Definir el n�mero de d�as h�biles transcurridos para que se pueda notificar
				// la generaci�n de Estados de Cuenta a Clientes NAFIN
				final int				DIAS_HABILES_TRANSCURRIDOS	= 4;		
				
				// Dependiendo del Tipo de Cliente, definir los perfiles de los usuarios que
				// ser�n notificados.
				
				final String 			CLIENTE_EXTERNO 				= "C";
				final String 			INTERMEDIARIO_FINANCIERO	= "I";
				
				// Perfiles para Clientes Externos
				final ArrayList 		PERFILES_CLIENTES_EXTERNOS	= new ArrayList();
				PERFILES_CLIENTES_EXTERNOS.add("LINEA CREDITO");
				
				// Perfiles para Intermediarios Financieros (Bancarios y No Bancarios)
				final ArrayList 		PERFILES_IF 					= new ArrayList();
				PERFILES_IF.add("IF LI");
				PERFILES_IF.add("IF 4CP");
				PERFILES_IF.add("IF 5CP");
				PERFILES_IF.add("IF 4MIC");
				PERFILES_IF.add("IF 5MIC");

				// Definir plantilla del mensaje que se utilizar� para notificar a los clientes NAFIN
				final MessageFormat  MENSAJE_CLIENTES 				= new MessageFormat(
					"<html> "  +
					"	<head> "  +
					"		<title>Notificaci&oacute;n Estado de Cuenta</title> "  +
					"		<style> "  +
					"			td.cabecera '{ "  +
					"				 font-weight: bold;text-align:left;border:1pt solid black;padding:4pt; "  +
					"			}' "  +
					"			td.celda '{ "  +
					"				 text-align:left;border:1pt solid black;padding:4pt; "  +
					"			}' "  +
					"		</style> "  +
					"	</head> "  +
					"	<body style=\"font-family:Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif;font-size:11.0pt;color:black;\" >	 "  +
					"		<br> "  +
					"		<div style=\"font-style:italic;\">{0}</div> "  +
					"		<br> "  +
					"		<div style=\"font-style:italic;\" >Por medio del presente se le notifica que su Estado de Cuenta a la fecha de corte {1} fue generado.</div> "  +
					"		<br> "  +
					"		<div style=\"font-style:italic;\" >Para consultar la informaci&oacute;n deber&aacute; ingresar al portal NAFINET (<a href=\"http://cadenas.nafin.com.mx/\">http://cadenas.nafin.com.mx/</a>) en el m&oacute;dulo Pagos de Cartera / Informaci&oacute;n Operativa / Estados de Cuenta.</div> "  +
					"		<br> "  +
					"		<br> "  +
					"		<div style=\"\">ATENTAMENTE.</div> "  +
					"		<div style=\"font-style:italic;font-weight:bold;\" > "  +
					"			Nacional Financiera, S.C.N. "  +
					"		</div> "  +
					"		<br> "  +
					"		<div style=\"font-style:italic;\" > "  +
					"			<span style=\"font-weight:bold;\">Nota:</span> Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr&oacute;nicos de entrada.<br> "  +
					"			Por favor no responda a este mensaje. "  +
					"		</div> "  +
					"	</body> "  +
					"</html> "
				);

				// Parametros invariantes del mensaje de correo
				
				// Agregar remitente
				final String 		REMITENTE 				= "no_response@nafin.gob.mx";
				// No se agregar� destinatarios de copia de carbon
				final String 		CC_DESTINATARIOS		= null;
				// Agregar titulo del mensaje
				final String 		TITULO_MENSAJE			= "Notificaci\u00F3n Estado de Cuenta" ;
				// Agregar Archivos de Imagenes 
				final ArrayList 	LISTA_DE_IMAGENES		= null;
				// Agregar archivo adjunto
				final ArrayList 	LISTA_DE_ARCHIVOS		= null;

				// 2. Determinar si hay Estados de Cuenta cuya fecha de generaci�n haya sido hace 4 d�as h�biles
				
				// Obtener lista de d�as inhabiles especiales (los que caen en d�as festivos)
				StringBuffer diaMes = new StringBuffer(48);
				query.setLength(0);
				query.append(
					"SELECT                                         "  +
					"   CG_DIA_INHABIL AS DIA_MES                   "  +
					"FROM                                           "  +
					"   COMCAT_DIA_INHABIL                          "  +
					"WHERE                                          "  +
					"   CG_DIA_INHABIL IS NOT NULL                  "  +
					"UNION                                          "  +
					"SELECT                                         "  +
					"   TO_CHAR(DF_DIA_INHABIL,'DD/MM') AS DIA_MES  "  +
					"FROM                                           "  +
					"   COMCAT_DIA_INHABIL                          "  +
					"WHERE                                          "  +
					"   DF_DIA_INHABIL IS NOT NULL                  "
				);
				
				ps = con.queryPrecompilado(query.toString());
				rs = ps.executeQuery();
				while(rs.next()){
					
					String dia = rs.getString("DIA_MES");
					
					diaMes.append(dia);
					diaMes.append(";");
				}
				rs.close();
				ps.close();
				
				diasInhabiles = diaMes.toString();
				
				// Buscar fecha de generaci�n de reportes que haya sido hace 4 d�as h�biles
				query.setLength(0);
				query.append(
					"SELECT                                                                 "  +
					"  DISTINCT                                                             "  +
					"    TO_CHAR(DF_FECHA_HORA,'DD/MM/YYYY') AS FECHA_GENERACION_EDO_CTA    "  +
					"FROM                                                                   "  +
					"    COMHIS_PROC_EDO_CUENTA                                             "  +
					"WHERE                                                                  "  +
					"    DF_FECHAFINMES                 >= TO_DATE(?,'DD/MM/YYYY')     AND  "  +
					"    DF_FECHAFINMES                 <  TO_DATE(?,'DD/MM/YYYY') + 1 AND  "  +
					"    CS_CORREO_LINCRED_CLIENTES_NAF =  'N'                         AND  "  +
					"    DF_FECHA_HORA		            IS NOT NULL                         "
				);
    
				ps = con.queryPrecompilado(query.toString());
				ps.setString(1,fechaUltimoDiaMesAnterior);
				ps.setString(2,fechaUltimoDiaMesAnterior);
				
				rs = ps.executeQuery();
				while(rs.next()){
					
					String fechaGeneracion 			= rs.getString("FECHA_GENERACION_EDO_CTA");
					
					// Determinar si el reporte fue generado hace 4 d�as h�biles
					String fechaDiaHabilPosterior = Fecha.calculaDiaHabil(fechaGeneracion, "dd/MM/yyyy", DIAS_HABILES_TRANSCURRIDOS, diasInhabiles);
						
					// Revisar si ya han transcurrido 4 d�as h�biles desde que se gener� el reporte
					if ( Comunes.comparaFechas( fechaActual, fechaDiaHabilPosterior, "dd/MM/yyyy" ) == 0 ){
						fechaGeneracionReporte = fechaGeneracion;
						break;
					}
					
				}
				rs.close();
				ps.close();
				
				// Si no se encontr� ninguna fecha de generaci�n de Estados de Cuenta, correspondiente
				// al mes anterior y que haya sido hace 4 d�as h�biles, cancelar proceso.
				if ( fechaGeneracionReporte == null ){
					log.info("notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): No hay ning�n Estado de Cuenta v�lido que haya sido generado hace " + DIAS_HABILES_TRANSCURRIDOS + " d�as h�biles.");
					log.info("notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): Finalizo notificaci�n a Clientes NAFIN de L�nea de Cr�dito del Estado de cuenta generado.");
					log.info("notificaEdoCuentaClientesNafin(S)");
					return;
				}
				
				// 3. Consultar grupos de estados de cuenta generados hace 4 d�as h�biles.
				query.setLength(0);
				query.append(
					"SELECT                                                                 "  +
					"  DISTINCT                                                             "  +
					"    IC_PROC_EDO_CUENTA                               						"  +
					"FROM                                                                   "  +
					"    COMHIS_PROC_EDO_CUENTA                                             "  +
					"WHERE                                                                  "  +
					"    DF_FECHAFINMES                 >= TO_DATE(?,'DD/MM/YYYY')     AND  "  +
					"    DF_FECHAFINMES                 <  TO_DATE(?,'DD/MM/YYYY') + 1 AND  "  +
					"    CS_CORREO_LINCRED_CLIENTES_NAF =  'N'                         AND  "  +
					"    DF_FECHA_HORA                 >=  TO_DATE(?,'DD/MM/YYYY')     AND  "  +
					"    DF_FECHA_HORA                 <   TO_DATE(?,'DD/MM/YYYY') + 1      "
				);
    
				ps = con.queryPrecompilado(query.toString());
				ps.setString(1,fechaUltimoDiaMesAnterior);
				ps.setString(2,fechaUltimoDiaMesAnterior);
				ps.setString(3,fechaGeneracionReporte);
				ps.setString(4,fechaGeneracionReporte);
				
				rs = ps.executeQuery();
				while(rs.next()){
					
					String icProcEdoCuenta = rs.getString("IC_PROC_EDO_CUENTA");
					clavesProcEdoCuenta.add(icProcEdoCuenta);
					
				}
				rs.close();
				ps.close();
				
				// No se encontr� ning�n grupo de Estados de Cuenta del mes anterior y que haya sido 
				// hace 4 d�as h�biles, cancelar el proceso.
				if ( clavesProcEdoCuenta.size() == 0 ){
					log.info("notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): No se encontr� ning�n grupo v�lido (CS_CORREO_LINCRED_CLIENTES_NAF = 'N') de Estados de Cuenta del mes anterior y que haya sido generado hace " + DIAS_HABILES_TRANSCURRIDOS + " d�as h�biles.");
					log.info("notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): Finalizo notificaci�n a Clientes NAFIN de L�nea de Cr�dito del Estado de cuenta generado.");
					log.info("notificaEdoCuentaClientesNafin(S)");
					return;
				}
					
				log.info("notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): Inicio notificaci�n a Clientes NAFIN de L�nea de Cr�dito del Estado de cuenta generado.");
				
				// 4. Enviar notificaciones a los clientes NAFIN
					
				// 4.1 Consultar lista de clientes cuyo estado de cuenta fue generado hace 4 d�as h�biles.
				
				StringBuffer variablesBind = new StringBuffer();
				for(int i=0;i<clavesProcEdoCuenta.size();i++){
					if( i>0 ){ variablesBind.append(","); }
					variablesBind.append("?");
				}
						
				/*
				
					Nota de Dise�o by jshernandez(29/04/2015 04:29:13 p.m.): 
					
					Debido a que el env�o de correos tarda, de 5 a 15 segundos por mensaje (m�s la consulta al OID, etc), 
					el cursor a la base de datos queda abierto mucho tiempo (ya que se tienen aproximadamente 800 clientes), 
					lo que un futuro podr�a causar problemas, si COM_ESTADO_CUENTA es actualizado constantemente, por lo que se 
					recomendar�a crear una vista materializada del siguiente query, tabla temporal, etc. Por lo pronto 
					y debido a que COM_ESTADO_CUENTA es actualizado una vez al mes se decidi� no tomar en cuenta este problema; 
					no se pusieron los datos en un ArrayList con el prop�sito de optimizar el uso de memoria y pensando 
					que en un futuro el n�mero de clientes podr�a incrementarse.
					
				*/
				query.setLength(0);
				query.append(
					"SELECT                                                                 "  +
					"  NOMBRECLIENTE,                                                       "  +
					"  CLAVE_AFILIADO,                                                      "  +
					"  TIPO_AFILIADO                                                        "  +
					"FROM                                                                   "  +
					"  (                                                                    "  +
					"    SELECT /*+ use_nl(EC CE REL) */ DISTINCT                           "  +
					"      EC.CG_NOMBRECLIENTE     AS NOMBRECLIENTE,                        "  +
					"      REL.IC_EPO_PYME_IF      AS CLAVE_AFILIADO,                       "  +
					"      REL.CG_TIPO             AS TIPO_AFILIADO                         "  +
					"    FROM                                                               "  +
					"      COM_ESTADO_CUENTA       EC,                                      "  +
					"      COMCAT_CLI_EXTERNO      CE,                                      "  +
					"      COMREL_NAFIN            REL                                      "  +
					"    WHERE                                                              "  +
					"        EC.DF_FECHAFINMES   >= TO_DATE(?,'DD/MM/YYYY')                 "  +
					"    AND EC.DF_FECHAFINMES   <  TO_DATE(?,'DD/MM/YYYY') + 1             "  +
					"    AND EC.IC_PROC_EDO_CUENTA IN (                                     "
				);
				query.append(
					variablesBind
				);
				query.append(
					" )                                                                     "  +
					"    AND EC.IC_IF            =  12                                      "  +
					"    AND EC.IG_CLIENTE       =  CE.IN_NUMERO_SIRAC                      "  +
					"    AND CE.CS_HABILITADO    =  'S'                                     "  +
					"    AND CE.IC_NAFIN_ELECTRONICO = REL.IC_NAFIN_ELECTRONICO             "  +
					"    AND REL.CG_TIPO          = 'C'                                     "  +
					"    UNION                                                              "  +
					"    SELECT  /*+ use_nl(EC OPEIF I) */ DISTINCT                         "  +
					"      EC.CG_NOMBRECLIENTE AS NOMBRECLIENTE,                            "  +
					"      I.IC_IF             AS CLAVE_AFILIADO,                           "  +
					"      'I'                 AS TIPO_AFILIADO                             "  +
					"    FROM                                                               "  +
					"      COM_ESTADO_CUENTA EC,                                            "  +
					"      OPE_DATOS_IF OPEIF,                                              "  +
					"      COMCAT_IF I                                                      "  +
					"    WHERE                                                              "  +
					"        EC.DF_FECHAFINMES   >=  TO_DATE(?,'DD/MM/YYYY')                "  +
					"    AND EC.DF_FECHAFINMES   <   TO_DATE(?,'DD/MM/YYYY') + 1            "  +
					"    AND EC.IC_PROC_EDO_CUENTA IN (                                     "
				);
				query.append(
					variablesBind
				);
				query.append(
					" )                                "  +
					"    AND EC.IC_IF           = 12                                        "  +
					"    AND OPEIF.IC_IF        = I.IC_IF                                   "  +
					"    AND I.CS_HABILITADO    = 'S'                                       "  +
					"    AND EC.IG_CLIENTE      = I.IN_NUMERO_SIRAC                         "  +
					"  )                                                                    "  +
					"  A                                                                    "  +
					"ORDER BY                                                               "  +
					"  NOMBRECLIENTE                                                        "
				);
					
				ps = con.queryPrecompilado(query.toString());
						
				int indice = 1;
				ps.setString(indice++,fechaUltimoDiaMesAnterior);
				ps.setString(indice++,fechaUltimoDiaMesAnterior);
				for(int i=0;i<clavesProcEdoCuenta.size();i++){
					ps.setString(indice++,(String)clavesProcEdoCuenta.get(i));
				}
				ps.setString(indice++,fechaUltimoDiaMesAnterior);
				ps.setString(indice++,fechaUltimoDiaMesAnterior);
				for(int i=0;i<clavesProcEdoCuenta.size();i++){
					ps.setString(indice++,(String)clavesProcEdoCuenta.get(i));
				}
						
				// 4.2 Por cada cliente, notificar a sus usuarios la generaci�n del Estado de cuenta.
				List 				correos 			= new ArrayList(1000);
				StringBuffer 	listaDeCorreos = new StringBuffer(1024);
				// Inicializar instancia de la clase que realizar� el env�o de correos
				Correo 			correo 			= new Correo();
				correo.disableDebug();
				// Obtener instancia de Clase UtilUsr que se utilizar� para consultar informacion de los usuarios 
				UtilUsr 			utilUsr 			= new UtilUsr();
				
				rs = ps.executeQuery();
				while( rs.next() ){
						
					// 4.2.1 Obtener Datos del Cliente.
					String 			nombrecliente 	= (rs.getString("NOMBRECLIENTE") == null)?"":rs.getString("NOMBRECLIENTE");
					String 			claveAfiliado 	= rs.getString("CLAVE_AFILIADO");
					String 			tipoAfiliado 	= rs.getString("TIPO_AFILIADO");
					
					// Resetear arreglo que guardar� la lista de correos.
					correos.clear();
						
					try {
							
						// 4.2.2 Obtener direcciones de correo de los usuarios del Cliente NAFIN 
						
						// Obtener lista de usuarios del Cliente NAFIN
						List listaLogin = utilUsr.getUsuariosxAfiliado(claveAfiliado,tipoAfiliado);
						if( listaLogin == null || listaLogin.size() == 0 ){
							log.info(
								"notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): Se gener� estado de cuenta para: " + nombrecliente + ", pero no tiene ning�n " +
								"usuario que cumpla con los perfiles requeridos, por lo que no podr� ser notificado."
							);
							continue;
						}
	
						// Obtener direcciones de los correos de los usuarios que con los perfiles de notificaci�n
						// Cliente Externo: LINEA CREDITO
						// Intermediario Financiero (No Bancario): IF LI, IF 4CP, IF 5CP, IF 4MIC e IF 5MIC
						for(int i=0;i<listaLogin.size();i++){
								
							String  login 		= (String) listaLogin.get(i);
							Usuario usuario 	= utilUsr.getUsuario(login);
							String  perfil		= usuario.getPerfil();
								
							if( 			!CLIENTE_EXTERNO.equals(tipoAfiliado) 				&& !INTERMEDIARIO_FINANCIERO.equals(tipoAfiliado) 	){
								continue;
							} else if(	CLIENTE_EXTERNO.equals(tipoAfiliado) 				&& !PERFILES_CLIENTES_EXTERNOS.contains(perfil) 	){
								continue;
							} else if( 	INTERMEDIARIO_FINANCIERO.equals(tipoAfiliado )	&& !PERFILES_IF.contains(perfil) 						){
								continue;
							}
								
							// Obtener correo del usuario
							String emailUsuario = usuario.getEmail();
							if( emailUsuario != null ){
								emailUsuario = emailUsuario.trim();
							}
							if( Comunes.validaEmail(emailUsuario) ){
								if(emailUsuario.length() > 1000 ){
									log.info(
										"notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): La direcci�n de correo del usuario: " + login +", es muy grande, por lo que no podr� ser notificado debido a restricciones en el servidor de correos."
									);
								} else {
									correos.add(emailUsuario);
								}
							} else {
								log.debug(
									"notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): La direcci�n de correo del usuario: " + login +", no es v�lida: " + emailUsuario + ", por lo que se excluir� del correo de notificaci�n."
								);
							}
								
						}
							
						// Verificar que al menos haya un correo v�lido para hacer el env�o.
						if( correos.size() == 0 ){
							log.info(
								"notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): Se gener� estado de cuenta para: " + nombrecliente + ", pero ninguno de usuarios " +
								"tiene el perfil requerido o una direcci�n de correo v�lida, por lo que no se enviar� correo de notificaci�n."
							);
							continue;
						}
					
						// 4.2.3 Preparar mensaje
						String mensajeCorreo = MENSAJE_CLIENTES.format(
							new Object[]{
								StringEscapeUtils.escapeHtml(nombrecliente),
								fechaUltimoDiaMesAnterior
							}
						);
								
						// 4.2.4 Enviar Correo de Notificaci�n a los usuarios del Cliente NAFIN
						while(correos.size() > 0 ){
								
							// Resetear direcciones de correo
							listaDeCorreos.setLength(0);
							
							// Generar lista de direcciones de correo a notificar de m�ximo 1000 caracteres de longitud.
							for(int cta=0;correos.size()>0;cta++){
									
								String direccionCorreo = (String) correos.get(0);
									
								if( listaDeCorreos.length() + 1 + direccionCorreo.length() > 1000 + 1 ){ // +1 por la coma
									break;
								} else {
									correos.remove(0);
									if( cta > 0 ) listaDeCorreos.append(",");
									listaDeCorreos.append(direccionCorreo);
								}
								
							}
								
							// Agregar los siguientes destinatarios
							String 			destinatarios			= listaDeCorreos.toString();
								
							// Enviar correo
							correo.enviaCorreoConDatosAdjuntos(
								REMITENTE,
								destinatarios,
								CC_DESTINATARIOS,
								TITULO_MENSAJE,
								mensajeCorreo,
								LISTA_DE_IMAGENES,
								LISTA_DE_ARCHIVOS
							);
	
						}
	
					} catch(Exception e){
							
						log.error("notificaEdoCuentaClientesNafin(" + ID_PROCESO + "):No se pudo notificar la genereaci�n del estado de cuenta al siguiente cliente: " + nombrecliente);
						log.error("notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): "+e.toString() + ": " + e.getMessage());
						e.printStackTrace();
							
						log.error("NOTIFICA_EDO_CUENTA_CLIENTES_NAFIN_ERROR:ID_PROCESO:CLAVES_PROC_EDO_CUENTA:FECHA_ULTIMO_DIA_MES_ANTERIOR:FECHA_GENERACION_REPORTE:CLAVE_AFILIADO:TIPO_AFILIADO:NOMBRE_CLIENTE");
						log.error("NOTIFICA_EDO_CUENTA_CLIENTES_NAFIN_ERROR:" + ID_PROCESO + ":" + clavesProcEdoCuenta + ":" + fechaUltimoDiaMesAnterior + ":" + fechaGeneracionReporte + ":" + claveAfiliado + ":" + tipoAfiliado + ":" + nombrecliente);

					}
						
				}
				rs.close();
				ps.close();

				log.info("notificaEdoCuentaClientesNafin(" + ID_PROCESO + "): Finalizo notificaci�n a Clientes NAFIN de L�nea de Cr�dito del Estado de cuenta generado.");
				
			} catch(Exception e){
				
				log.error("notificaEdoCuentaClientesNafin(" + ID_PROCESO + ")(Exception)");
				log.error("notificaEdoCuentaClientesNafin(" + ID_PROCESO + ").fechaActual               = <" + fechaActual               + ">");
				log.error("notificaEdoCuentaClientesNafin(" + ID_PROCESO + ").fechaUltimoDiaMesAnterior = <" + fechaUltimoDiaMesAnterior + ">");
				e.printStackTrace();

				log.info("notificaEdoCuentaClientesNafin(S)");
				
				throw e;
				
			} finally {
				
				if(rs  != null)  { try{ rs.close();  }catch(Exception e){} }
				if(ps  != null)  { try{ ps.close();  }catch(Exception e){} }

				if( con.hayConexionAbierta() ){
					con.cierraConexionDB();
				}
				
			}
			
			// 5. Registrar el env�o de los correos.
			try {
				
				registraNotificacion(fechaUltimoDiaMesAnterior,clavesProcEdoCuenta,"CS_CORREO_LINCRED_CLIENTES_NAF");
				
			} catch(Exception e){
				
				log.error("notificaEdoCuentaClientesNafin(" + ID_PROCESO + ")(Exception)");
				log.error("notificaEdoCuentaClientesNafin(" + ID_PROCESO + ").fechaActual               = <" + fechaActual               + ">");
				log.error("notificaEdoCuentaClientesNafin(" + ID_PROCESO + ").fechaUltimoDiaMesAnterior = <" + fechaUltimoDiaMesAnterior + ">");
				e.printStackTrace();
				
				throw e;
				
			} finally {
				
				log.info("notificaEdoCuentaClientesNafin(S)");
				
			}

	}

}