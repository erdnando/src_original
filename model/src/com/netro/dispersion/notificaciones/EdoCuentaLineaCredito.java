package com.netro.dispersion.notificaciones;

import javax.ejb.Remote;

@Remote
public interface EdoCuentaLineaCredito {

	public void notificarEdoCuentaGeneradoNafin(String fechaActual, String fechaUltimoDiaMesAnterior) 
		throws Exception; 
	
	public void enviaListaClientesUsuariosNafin(String fechaActual, String fechaUltimoDiaMesAnterior)
		throws Exception; 
	
	public void notificaEdoCuentaClientesNafin(String fechaActual, String fechaUltimoDiaMesAnterior) 
		throws Exception; 
	
}