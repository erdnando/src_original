package com.netro.dispersion;

import com.nafin.descuento.ws.CuentasPymeIFWS;

import com.netro.descuento.AutorizacionDescuento;
import com.netro.descuento.ISeleccionDocumento;
import com.netro.descuento.ParametrosDescuentoBean;
import com.netro.exception.NafinException;
import com.netro.xls.ComunesXLS;

import java.io.File;
import java.io.FileInputStream;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.Bitacora;
import netropology.utilerias.Comunes;
import netropology.utilerias.ConvierteCadena;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.SMS;
import netropology.utilerias.Seguridad;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "DispersionEJB" , mappedName = "DispersionEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class DispersionBean implements Dispersion {

	private final static String claveCuentaSwift = "50";
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(DispersionBean.class);

	//Variable para determinar los tipos de factoraje para manejar en la dispersion.
	private final 	static String 	dispersionTradicional = "'N', 'V', 'M'";
	private final 	static String 	dispersionDistribuido = "'D', 'I'";
	private 			static HashMap monedas 					= null;


	public void ovactualizarCuenta(
										String ic_cuenta, 			String ic_nafin_electronico,
										String ic_producto_nafin,	String ic_moneda,
										String ic_bancos_tef,		String cg_cuenta,
										String ic_usuario,			String ic_tipo_cuenta,
										HashMap parametrosAdicionales )
   	throws NafinException {

		AccesoDB		con 			= null;
		ResultSet		rs				= null;
		String			qrySentencia	= "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		boolean			resultado		= true;
		PreparedStatement ps = null;

		StringBuffer   queryConsulta = new StringBuffer();
		parametrosAdicionales 	= parametrosAdicionales == null?new HashMap():parametrosAdicionales;

		try {
			con = new AccesoDB();
			con.conexionDB();

			// Consultar valores anteriores
			queryConsulta.append(
				"SELECT                  "  +
				"	CG_CUENTA,            "  +
				"	IC_PRODUCTO_NAFIN,    "  +
				"	IC_MONEDA,            "  +
				"	IC_BANCOS_TEF,        "  +
				"	IC_TIPO_CUENTA,       "  +
				"	CG_PLAZA_SWIFT,       "  +
				"	CG_SUCURSAL_SWIFT,    "  +
				"	IC_NAFIN_ELECTRONICO, "  +
				"	CG_TIPO_AFILIADO,      "  +
        "	IC_SWIFT,      "  +
        "	IC_ABA      "  +
				"FROM                    "  +
				"  COM_CUENTAS           "  +
				"WHERE                   "  +
				"	IC_CUENTA = ?         "
			);
			ps = con.queryPrecompilado(queryConsulta.toString());
			ps.setBigDecimal(1,new BigDecimal(ic_cuenta));
			rs = ps.executeQuery();

			String cg_tipo_afiliado_ant		= null;
			String cg_cuenta_ant					= null;
			String ic_producto_nafin_ant		= null;
			String ic_moneda_ant					= null;
			String ic_bancos_tef_ant			= null;
			String ic_tipo_cuenta_ant			= null;
			String cg_plaza_swift_ant			= null;
			String cg_sucursal_swift_ant		= null;
			String ic_nafin_electronico_ant	= null;
      String ic_swift_ant	= null;
      String ic_aba_ant	= null;

			if(rs.next()){

				cg_tipo_afiliado_ant			= rs.getString("cg_tipo_afiliado"		);
				cg_cuenta_ant					= rs.getString("cg_cuenta"					);
				ic_producto_nafin_ant		= rs.getString("ic_producto_nafin"		);
				ic_moneda_ant					= rs.getString("ic_moneda"					);
				ic_bancos_tef_ant				= rs.getString("ic_bancos_tef"			);
				ic_tipo_cuenta_ant			= rs.getString("ic_tipo_cuenta"			);
				cg_plaza_swift_ant			= rs.getString("cg_plaza_swift"			);
				cg_sucursal_swift_ant		= rs.getString("cg_sucursal_swift"		);
				ic_nafin_electronico_ant	= rs.getString("ic_nafin_electronico"	);
        ic_swift_ant	= rs.getString("ic_swift"	);
        ic_aba_ant	= rs.getString("ic_aba"	);

			}

			cg_tipo_afiliado_ant			= cg_tipo_afiliado_ant     == null?"":cg_tipo_afiliado_ant;
			cg_cuenta_ant					= cg_cuenta_ant            == null?"":cg_cuenta_ant;
			ic_producto_nafin_ant		= ic_producto_nafin_ant    == null?"":ic_producto_nafin_ant;
			ic_moneda_ant					= ic_moneda_ant            == null?"":ic_moneda_ant;
			ic_bancos_tef_ant				= ic_bancos_tef_ant        == null?"":ic_bancos_tef_ant;
			ic_tipo_cuenta_ant			= ic_tipo_cuenta_ant       == null?"":ic_tipo_cuenta_ant;
			cg_plaza_swift_ant			= cg_plaza_swift_ant       == null?"":cg_plaza_swift_ant;
			cg_sucursal_swift_ant		= cg_sucursal_swift_ant    == null?"":cg_sucursal_swift_ant;
			ic_nafin_electronico_ant	= ic_nafin_electronico_ant == null?"":ic_nafin_electronico_ant;
      ic_swift_ant	= ic_swift_ant == null?"":ic_swift_ant;
      ic_aba_ant	= ic_aba_ant == null?"":ic_aba_ant;

			rs.close();
			ps.close();

			// Realizar modificaciones
			String cg_tipo_afiliado = "";
			String ic_epo = "";
			String ic_estatus_cecoban = "";
			qrySentencia =
				" SELECT cg_tipo_afiliado, ic_epo, ic_estatus_cecoban"   +
				"   FROM com_cuentas"   +
				"  WHERE ic_cuenta = ? "  ;
			System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_cuenta));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				cg_tipo_afiliado	= rs.getString(1)==null?"":rs.getString(1);
				ic_epo				= rs.getString(2)==null?"":rs.getString(2);
				ic_estatus_cecoban	= rs.getString(3)==null?"":rs.getString(3);
			}//while
			if(ps != null) ps.close();

			String condicionIF = "";
			String condicionIF2 = "";

			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado) || "P".equals(cg_tipo_afiliado)) {
				condicionIF = "    AND ic_epo = ?"  ;
				condicionIF2 = "    AND ic_epo != ?"  ;
			}

			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM com_cuentas c"   +
				"  WHERE c.cg_cuenta = ? "   +
				"    AND ic_nafin_electronico != ? "+condicionIF2  ;
			System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, cg_cuenta);
			ps.setInt(2, Integer.parseInt(ic_nafin_electronico));
			int cont = 2;
			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado) || "P".equals(cg_tipo_afiliado)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
			}
			rs = ps.executeQuery();
			ps.clearParameters();
			int existeCuenta = 0;
			if(rs.next()){
				existeCuenta = rs.getInt(1);
			}//while
			if(ps != null) ps.close();
			if(existeCuenta>0)
				throw new NafinException("DISP0018");

/*			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado)) {
				qrySentencia =
					" SELECT COUNT (1)"   +
					"   FROM com_cuentas c"   +
					"  WHERE c.cg_cuenta = ?"   +
					"    AND ic_nafin_electronico = ? "   +
					"    AND ic_epo != ? "  ;
				System.out.println("\n qrySentencia: "+qrySentencia);
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1, cg_cuenta);
				ps.setInt(2, Integer.parseInt(ic_nafin_electronico));
				ps.setInt(3, Integer.parseInt(ic_epo));
				rs = ps.executeQuery();
				ps.clearParameters();
				existeCuenta = 0;
				if(rs.next()){
					existeCuenta = rs.getInt(1);
				}//while
				if(ps != null) ps.close();
				if(existeCuenta>0)
					throw new NafinException("DISP0019");
			}*/

			// VALIDAR SI LA CUENTA A REGISTRAR ES EN DOLARES, REVISAR QUE VENGAN ESPECIFICADAS LA PLAZA SWIFT
			// Y LA SUCURSAL SWIFT
			String 	plazaSwift 		= null;
			String 	sucursalSwift 	= null;
      String cgSwift = null;
      String cgAba = null;
			if(ic_moneda.equals("54") && ic_tipo_cuenta.equals("50")){ // Cuenta en Dolares y de Tipo Swift

				plazaSwift 					= (String) parametrosAdicionales.get("PLAZA_SWIFT");
				plazaSwift					= plazaSwift    == null || plazaSwift.trim().equals("")   ?null:plazaSwift.trim();

				sucursalSwift				= (String) parametrosAdicionales.get("SUCURSAL_SWIFT");
				sucursalSwift				= sucursalSwift == null || sucursalSwift.trim().equals("")?null:sucursalSwift.trim();

        cgSwift				= (String) parametrosAdicionales.get("CG_SWIFT");
				cgSwift				= cgSwift == null || cgSwift.trim().equals("")?null:cgSwift.trim();

        cgAba				= (String) parametrosAdicionales.get("CG_ABA");
				cgAba				= cgAba == null || cgAba.trim().equals("")?null:cgAba.trim();

				if(plazaSwift == null){
					throw new NafinException("DISP0021");
				}else if(sucursalSwift == null){
					throw new NafinException("DISP0022");
				}

			}

			Calendar cFechaSigHabil = new GregorianCalendar();
			cFechaSigHabil.setTime(new java.util.Date());
			cFechaSigHabil.add(Calendar.DATE, 1);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
				cFechaSigHabil.add(Calendar.DATE, 2);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
				cFechaSigHabil.add(Calendar.DATE, 1);
			Vector vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
			boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
			cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			while(bInhabil) {
				vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
				bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
				if(!bInhabil)
					break;
				cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			}
			String sFechaSigHabil = sdf.format(cFechaSigHabil.getTime());
			System.out.println("Fecha siguiente: "+sFechaSigHabil);


	        qrySentencia =	"Update com_cuentas ";
	        qrySentencia += "   Set ic_nafin_electronico = " + ic_nafin_electronico + " ,";
	        qrySentencia += "   ic_producto_nafin = " + ic_producto_nafin + ", ";
	        qrySentencia += "   ic_moneda = " + ic_moneda + ", ";
	        qrySentencia += "   ic_bancos_tef = " + ic_bancos_tef + ", ";
	        qrySentencia += "   cg_cuenta = '" + cg_cuenta + "', ";
	        qrySentencia += "   ic_usuario = '" + ic_usuario + "', ";
	        qrySentencia += "   df_ultima_mod = SYSDATE, ";
	        qrySentencia += "   df_transferencia = to_date('" + sFechaSigHabil + "','dd/mm/yyyy'), ";
	        qrySentencia += "   df_aplicacion = to_date('" + sFechaSigHabil + "','dd/mm/yyyy'), ";
	        qrySentencia += "   ic_tipo_cuenta = " + ic_tipo_cuenta + ", ";
			  if(!ic_moneda.equals("54")){
				  qrySentencia += "   ic_estatus_cecoban = null, ";
			  }
	        qrySentencia += "   ic_estatus_tef    = null, ";
			  qrySentencia += "	 cg_plaza_swift    = "+(plazaSwift 	== null?"null":"'"+plazaSwift   +"'")+", ";
			  qrySentencia += "	 cg_sucursal_swift = "+(sucursalSwift == null?"null":"'"+sucursalSwift+"'")+", ";
        qrySentencia += "	 ic_swift = "+(cgSwift == null?"null":"'"+cgSwift+"'")+", ";
        qrySentencia += "	 ic_aba = "+(cgAba == null?"null":"'"+cgAba+"'")+" ";
	        qrySentencia += " Where ic_cuenta = " + ic_cuenta;

			System.out.print("DispersionBean :: ovactualizarCuenta :: Query "+qrySentencia);
			con.ejecutaSQL(qrySentencia);

			if ("2".equals(ic_producto_nafin)){
				qrySentencia =	" SELECT ic_linea_credito FROM com_cuentas WHERE ic_cuenta = " + ic_cuenta;

				System.out.print("DispersionBean :: ovactualizarCuenta :: Query "+qrySentencia);
				con.ejecutaSQL(qrySentencia);

				rs = con.queryDB(qrySentencia);

				if(rs.next()){
			        qrySentencia =	"Update com_linea_credito ";

			        qrySentencia += "  Set ic_producto_nafin = " + ic_producto_nafin + ", ";
			        qrySentencia += "   ic_moneda = " + ic_moneda + ", ";

					if ("1".equals(ic_tipo_cuenta))
						qrySentencia += "   cg_numero_cuenta = '" + cg_cuenta + "' ";
					else
						qrySentencia += "   cg_cuenta_clabe = '" + cg_cuenta + "' ";

			        qrySentencia += " Where ic_linea_credito = " + rs.getString("ic_linea_credito");

					System.out.print("DispersionBean :: ovactualizarCuenta :: Query "+qrySentencia);
					con.ejecutaSQL(qrySentencia);
				}
			}

			// Consultar los campos Actuales
			queryConsulta.setLength(0);
			queryConsulta.append(
				"SELECT                  "  +
				"	CG_CUENTA,            "  +
				"	IC_PRODUCTO_NAFIN,    "  +
				"	IC_MONEDA,            "  +
				"	IC_BANCOS_TEF,        "  +
				"	IC_TIPO_CUENTA,       "  +
				"	CG_PLAZA_SWIFT,       "  +
				"	CG_SUCURSAL_SWIFT,    "  +
				"	IC_NAFIN_ELECTRONICO, "  +
				"	CG_TIPO_AFILIADO,      "  +
        "	IC_SWIFT,     "  +
        "	IC_ABA      "  +
				"FROM                    "  +
				"  COM_CUENTAS           "  +
				"WHERE                   "  +
				"	IC_CUENTA = ?         "
			);
			ps = con.queryPrecompilado(queryConsulta.toString());
			ps.setBigDecimal(1,new BigDecimal(ic_cuenta));
			rs = ps.executeQuery();

			String cg_tipo_afiliado_act		= null;
			String cg_cuenta_act					= null;
			String ic_producto_nafin_act		= null;
			String ic_moneda_act					= null;
			String ic_bancos_tef_act			= null;
			String ic_tipo_cuenta_act			= null;
			String cg_plaza_swift_act			= null;
			String cg_sucursal_swift_act		= null;
			String ic_nafin_electronico_act	= null;
      String ic_swift_act = null;
      String ic_aba_act = null;

			if(rs.next()){

				cg_tipo_afiliado_act			= rs.getString("cg_tipo_afiliado"		);
				cg_cuenta_act					= rs.getString("cg_cuenta"					);
				ic_producto_nafin_act		= rs.getString("ic_producto_nafin"		);
				ic_moneda_act					= rs.getString("ic_moneda"					);
				ic_bancos_tef_act				= rs.getString("ic_bancos_tef"			);
				ic_tipo_cuenta_act			= rs.getString("ic_tipo_cuenta"			);
				cg_plaza_swift_act			= rs.getString("cg_plaza_swift"			);
				cg_sucursal_swift_act		= rs.getString("cg_sucursal_swift"		);
				ic_nafin_electronico_act	= rs.getString("ic_nafin_electronico"	);
        ic_swift_act	= rs.getString("ic_swift"	);
        ic_aba_act = rs.getString("ic_aba"	);
			}

			cg_tipo_afiliado_act			= cg_tipo_afiliado_act     == null?"":cg_tipo_afiliado_act;
			cg_cuenta_act					= cg_cuenta_act            == null?"":cg_cuenta_act;
			ic_producto_nafin_act		= ic_producto_nafin_act    == null?"":ic_producto_nafin_act;
			ic_moneda_ant					= ic_moneda_act            == null?"":ic_moneda_act;
			ic_bancos_tef_act				= ic_bancos_tef_act        == null?"":ic_bancos_tef_act;
			ic_tipo_cuenta_act			= ic_tipo_cuenta_act       == null?"":ic_tipo_cuenta_act;
			cg_plaza_swift_act			= cg_plaza_swift_act       == null?"":cg_plaza_swift_act;
			cg_sucursal_swift_act		= cg_sucursal_swift_act    == null?"":cg_sucursal_swift_act;
			ic_swift_act	= ic_swift_act == null?"":ic_swift_act;
      ic_aba_act	= ic_aba_act == null?"":ic_aba_act;

			rs.close();
			ps.close();

			// Registrar en BITACORA el LA MODIFICACION DE LA CUENTA para cuando la Clave del Afiliado al que se le di�
			// de alta la cuenta sea: P(PYME), D(DISTRIBUIDOR),I(IF Bancario/No Bancario) y B(IF/Beneficiario)
			if(
				"P".equals(cg_tipo_afiliado_ant) ||
				"D".equals(cg_tipo_afiliado_ant) ||
				"I".equals(cg_tipo_afiliado_ant) ||
				"B".equals(cg_tipo_afiliado_ant)
			){

				// Registrar los cambios en bitacora
				String decripcionModificacion 	= (String) parametrosAdicionales.get("DESCRIPCION_MODIFICACION");
				decripcionModificacion 				= decripcionModificacion == null?"REGISTRO MODIFICADO\nNO DISPONIBLE":decripcionModificacion;
				String clavePantalla  				= (String) parametrosAdicionales.get("PANTALLA_ORIGEN");
				clavePantalla 							= clavePantalla  == null?"CONSCTAS"						  :clavePantalla;

				//boolean esCuentaCLABE = "40".equals(ic_tipo_cuenta_ant)?true:false;
				boolean esCuentaSWIFT = "50".equals(ic_tipo_cuenta_ant)?true:false;

				String valoresAnteriores =
					"cg_estatus=\n" +
					"cg_cuenta="+cg_cuenta_ant+"\n" +
					"ic_producto_nafin="+ic_producto_nafin_ant+"\n" +
					"ic_moneda="+ic_moneda_ant+"\n" +
					"ic_bancos_tef="+ic_bancos_tef_ant+"\n" +
					"ic_tipo_cuenta="+ic_tipo_cuenta_ant+"\n" +
					"cg_plaza_swift="+(esCuentaSWIFT?cg_plaza_swift_ant:"N/A")+"\n" +
					"cg_sucursal_swift="+(esCuentaSWIFT?cg_sucursal_swift_ant:"N/A")+"\n" +
          "cg_swift="+(esCuentaSWIFT?ic_swift_ant:"N/A")+"\n" +
          "cg_aba="+(esCuentaSWIFT?ic_aba_ant:"N/A");

				String valoresActuales =
					decripcionModificacion + "\n" +
					"cg_cuenta="+cg_cuenta_act+"\n"  +
					"ic_producto_nafin="+ic_producto_nafin_act+"\n"  +
					"ic_moneda ="+ic_moneda_act+"\n"  +
					"ic_bancos_tef="+ic_bancos_tef_act+"\n"  +
					"ic_tipo_cuenta="+ic_tipo_cuenta_act+"\n"  +
					"cg_plaza_swift="+(esCuentaSWIFT?cg_plaza_swift_act:"N/A")+"\n"  +
					"cg_sucursal_swift="+(esCuentaSWIFT?cg_sucursal_swift_act:"N/A")+"\n"  +
          "ig_swift="+(esCuentaSWIFT?ic_swift_act:"N/A")+"\n" +
          "ig_aba="+(esCuentaSWIFT?ic_aba_act:"N/A");

				Bitacora.grabarEnBitacora(
						con,
						clavePantalla,																//clavePantalla,
						"B".equals(cg_tipo_afiliado_ant)?"I":cg_tipo_afiliado_ant,	//tipoAfiliado; Si el tipo es "B" usar "I"
						ic_nafin_electronico_ant, 												//claveNafinElectronico,
						ic_usuario,																	//claveUsuario,
						valoresAnteriores,														//datoAnterior,
						valoresActuales                                          	//datoActual
				);

			}


		}catch(NafinException ne){
			resultado = false;
			throw ne;
		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{

			if(rs != null ) try{ rs.close(); }catch(Exception e){}
			if(ps != null ) try{ ps.close(); }catch(Exception e){}

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	} /*Fin del M�todo ovactualizarProducto*/

	//-------------------------------------------------------------------------------------------
	//	void ovinsertarCuenta()
	//
	//-------------------------------------------------------------------------------------------
	public String ovinsertarCuenta(  	String ic_nafin_electronico,	String ic_producto_nafin,
										String ic_moneda, 				String ic_bancos_tef,
										String cg_cuenta,				String ic_usuario,
										String ic_tipo_cuenta,			String ic_epo,
										String cg_tipo_afiliado,		String rfc, HashMap parametrosAdicionales)
   	throws NafinException {
		System.out.println("\nDispersionEJB::ovinsertarCuenta (E)");
		AccesoDB		con 			= null;
		ResultSet		rs				= null;
		String			qrySentencia	= "";
		String 			sSigLlave		= sLlaveSiguiente();
		String 			sResBusqueda	= "0";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		PreparedStatement ps = null;
		boolean resultado = true;

		parametrosAdicionales 	= parametrosAdicionales == null?new HashMap():parametrosAdicionales;

		try {
			con = new AccesoDB();
			con.conexionDB();

			//VALIDA LA PERTENENCIA DEL AFILIADO AL PRODUCTO DADO
			if ("E".equals(cg_tipo_afiliado)) {
				qrySentencia =
					" SELECT COUNT (1)"   +
					"   FROM comrel_producto_epo cpe, comrel_nafin rna"   +
					"  WHERE cpe.ic_epo = rna.ic_epo_pyme_if "+
					"	AND rna.cg_tipo = 'E' " +
					"	AND rna.ic_nafin_electronico = ? "+
					"    AND cpe.ic_producto_nafin = ? ";
			} else {
				if("P".equals(cg_tipo_afiliado) || "D".equals(cg_tipo_afiliado)) {
					if("P".equals(cg_tipo_afiliado) && "1".equals(ic_producto_nafin)) {
						qrySentencia =
							" SELECT COUNT (1)"   +
							"   FROM comrel_pyme_epo pep, comrel_nafin rna "   +
							"    WHERE pep.ic_pyme = rna.ic_epo_pyme_if "+
							"	 AND rna.cg_tipo= 'P' "+
							"	 AND rna.ic_nafin_electronico = ? ";
					} else {
						qrySentencia =
							" SELECT COUNT (1)"   +
							"   FROM comrel_pyme_epo_x_producto pep, comrel_nafin rna "   +
							"    WHERE pep.ic_pyme = rna.ic_epo_pyme_if "+
							"	 AND rna.cg_tipo= 'P' "+
							"	AND rna.ic_nafin_electronico = ? "+
							"    AND pep.ic_producto_nafin = ? ";
					}
				} else {
					if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado)) {
						qrySentencia =
							" SELECT COUNT (1)"   +
							"   FROM comrel_producto_if cpi,comrel_nafin rna"   +
							"    WHERE cpi.ic_if = rna.ic_epo_pyme_if "+
							"	 AND rna.cg_tipo= 'I' "+
							"	AND rna.ic_nafin_electronico = ? "+
							"    AND cpi.ic_producto_nafin = ? ";
					}
				}
			}

			System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_nafin_electronico));
			if(!("P".equals(cg_tipo_afiliado) && "1".equals(ic_producto_nafin)))
				ps.setInt(2, Integer.parseInt(ic_producto_nafin));
			rs = ps.executeQuery();
			ps.clearParameters();
			int perteneceProd = 0;
			if(rs.next())
				perteneceProd = rs.getInt(1);
			rs.close();ps.close();

			if(perteneceProd==0)
				throw new NafinException("DISP0020");

			// Validar que el proveedor maneje Descuento Electronico
			/*if("P".equals(cg_tipo_afiliado) && "1".equals(ic_producto_nafin)){

				qrySentencia =
					"SELECT                                                        "  +
					"	DECODE(COUNT(1),0,'false','true') AS MANEJA_DESCUENTO       "  +
					"FROM                                                          "  +
					"	COMREL_NAFIN REL, COMREL_PYME_EPO PE                        "  +
					"WHERE                                                         "  +
					"	REL.IC_NAFIN_ELECTRONICO 		= ?                      AND  "  +
					"	REL.CG_TIPO          			= ?                      AND  "  +
					"	PE.IC_PYME							= REL.IC_EPO_PYME_IF 	 AND  "  +
					"	PE.IC_EPO							= ?					       AND  "  +
					"	PE.cs_aceptacion              = ?                      AND  "  +
					"	PE.cs_habilitado              = ?                           ";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,    Integer.parseInt(ic_nafin_electronico));
				ps.setString(2, "P");
				ps.setInt(3,    Integer.parseInt(ic_epo));
				ps.setString(4, "H");
				ps.setString(5, "S");

				rs = ps.executeQuery();
				ps.clearParameters();

				boolean manejaDescuento = false;
				if(rs != null && rs.next()){
					manejaDescuento = (rs.getStrng("MANEJA_DESCUENTO") != null && "true".equals(rs.getStrng("MANEJA_DESCUENTO")))?true:false;
				}
				rs.close();ps.close();

				if(!manejaDescuento)
					throw new NafinException("DISP0020");

			}*/

			String condicionIFB = "";
			String condicionIF = "";
			String condicionIF2 = "";

			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado) || "P".equals(cg_tipo_afiliado)) {
				condicionIF 	= " AND ic_epo = ?"  ;
				condicionIF2 	= " AND ic_epo != ?"  ;
				condicionIFB 	= " AND cg_tipo_afiliado = '"+cg_tipo_afiliado+"' ";
			}

			//VALIDA QUE NO TENGA MAS DE UNA CUENTA POR EPO, PROVEEDOR, MONEDA Y PRODUCTO.
			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM com_cuentas"   +
				"  WHERE ic_nafin_electronico = ?"   +
				"    AND ic_producto_nafin    = ?"   +
				"    AND ic_tipo_cuenta       = ?"   +
				condicionIF+condicionIFB  ;
			//System.out.println("\n qrySentencia: "+qrySentencia);

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_nafin_electronico));
			ps.setInt(2, Integer.parseInt(ic_producto_nafin));
			ps.setInt(3, Integer.parseInt(ic_tipo_cuenta));
			int cont = 3;
			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado) || "P".equals(cg_tipo_afiliado)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
			}
			rs = ps.executeQuery();
			ps.clearParameters();
			int existeCuenta = 0;
			if(rs.next())
				existeCuenta = rs.getInt(1);
			rs.close(); ps.close();

			if(existeCuenta>0)
				throw new NafinException("DISP0017");

			//VALIDA QUE LA CUENTA NO ESTE ASIGNADA A OTRO USUARIO
			qrySentencia =
				" SELECT   /*+index(c CP_COM_CUENTAS_PK) use_nl(crn c)*/"   +
				"        crn.cg_tipo, crn.ic_epo_pyme_if"   +
				"   FROM com_cuentas c, comrel_nafin crn"   +
				"  WHERE c.ic_nafin_electronico = crn.ic_nafin_electronico "+
				"    AND c.cg_cuenta = ?"   +
				"    AND c.ic_nafin_electronico != ? "   +condicionIF2  ;
				// NOTA: SE USARA LA CONDICION: condicionIF2 DEBIDO A QUE NO PERMITE DETECTAR CORRECTAMENTE
				//  PROVEEDORES EN UNA MISMA EPO QUE TENGAN LA CUENTA REPETIDA:  AND ic_epo != ?
			//System.out.println("\n qrySentencia: "+qrySentencia);

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, cg_cuenta);
			ps.setInt(2, Integer.parseInt(ic_nafin_electronico));
			cont = 2;
			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado) || "P".equals(cg_tipo_afiliado)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
			}
			rs = ps.executeQuery();
			ps.clearParameters();
			existeCuenta = 0;
			while(rs.next()){
				ResultSet			rs1				= null;
				String				qrySentencia1	= "";
				PreparedStatement	ps1				= null;

				String rs_tipo = rs.getString(1)==null?"":rs.getString(1);
				String rs_epo_pyme_if = rs.getString(2)==null?"":rs.getString(2);

				if("E".equals(rs_tipo)) {
					qrySentencia1 =
						" SELECT COUNT (1)"   +
						"   FROM comcat_epo"   +
						"  WHERE ic_epo = ?"   +
						"    AND cg_rfc != ?"  ;
				} else if("P".equals(rs_tipo)) {
					qrySentencia1 =
						" SELECT COUNT (1)"   +
						"   FROM comcat_pyme"   +
						"  WHERE ic_pyme = ?"   +
						"    AND cg_rfc != ?"  ;
				} else if("I".equals(rs_tipo)) {
					qrySentencia1 =
						" SELECT COUNT (1)"   +
						"   FROM comcat_if"   +
						"  WHERE ic_if = ?"   +
						"    AND cg_rfc != ?"  ;
				}
				//System.out.println("\nqrySentencia1: "+qrySentencia1);
				ps1 = con.queryPrecompilado(qrySentencia1);
				ps1.setInt(1, Integer.parseInt(rs_epo_pyme_if));
				ps1.setString(2, rfc);
				rs1 = ps1.executeQuery();
				ps1.clearParameters();
				if(rs1.next())
					existeCuenta += rs1.getInt(1);
				rs1.close();ps1.close();
			}//while
			rs.close();ps.close();
			if(existeCuenta>0)
				throw new NafinException("DISP0018");

			//VALIDA QUE NO SE ASIGNE UNA CUENTA A UN MISMO USUARIO CON OTRA EPO
/*			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado)) {
				qrySentencia =
					" SELECT COUNT (1)"   +
					"   FROM com_cuentas c"   +
					"  WHERE c.cg_cuenta = ?"   +
					"    AND ic_nafin_electronico = ? "   +
					"    AND ic_epo != ? "  ;
				//System.out.println("\n qrySentencia: "+qrySentencia);
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1, cg_cuenta);
				ps.setInt(2, Integer.parseInt(ic_nafin_electronico));
				ps.setInt(3, Integer.parseInt(ic_epo));
				rs = ps.executeQuery();
				ps.clearParameters();
				existeCuenta = 0;
				if(rs.next()){
					existeCuenta = rs.getInt(1);
				}//while
				rs.close();ps.close();
				if(existeCuenta>0)
					throw new NafinException("DISP0019");
			}*/

			// VALIDAR SI LA CUENTA A REGISTRAR ES EN DOLARES, REVISAR QUE VENGAN ESPECIFICADAS LA PLAZA SWIFT
			// Y LA SUCURSAL SWIFT
			String plazaSwift 	= null;
			String sucursalSwift = null;

			if( ic_moneda.equals("54") && ic_tipo_cuenta.equals("50")){ // Cuenta en Dolares y de Tipo Swift

				plazaSwift 					= (String) parametrosAdicionales.get("PLAZA_SWIFT");
				plazaSwift					= plazaSwift    == null || plazaSwift.trim().equals("")   ?null:plazaSwift.trim();

				sucursalSwift				= (String) parametrosAdicionales.get("SUCURSAL_SWIFT");
				sucursalSwift				= sucursalSwift == null || sucursalSwift.trim().equals("")?null:sucursalSwift.trim();

				if(plazaSwift == null){
					throw new NafinException("DISP0021");
				}else if(sucursalSwift == null){
					throw new NafinException("DISP0022");
				}

			}

			// Debido a que si se registra una cuenta en dolares, esta se almacena con
			// estatus 99, entonces si esta misma cuenta se registra en pesos, pasaria por alto
			// la validacion que se hace para las cuentas en monedas nacional.
			// Por eso se excluye de esta consulta, las cuentas en dolares.
			qrySentencia =
				" SELECT ic_estatus_cecoban"   +
				"   FROM com_cuentas c"   +
				"  WHERE c.cg_cuenta = ? "   +
				"	  AND ic_moneda != ? "  +// 54; Excluir Dolares Americanos
				"    AND ic_nafin_electronico = ?"+condicionIF  ;

			//System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, cg_cuenta);
			ps.setInt(2, Integer.parseInt("54")); // Excluir Dolares Americanos
			ps.setInt(3, Integer.parseInt(ic_nafin_electronico));
			cont = 3;
			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado)  || "P".equals(cg_tipo_afiliado)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
			}
			rs = ps.executeQuery();
			ps.clearParameters();
			String estatusCuenta = "";
			String estatusCampo = "";
			if(rs.next()){
				estatusCuenta = rs.getString(1)==null?"":","+rs.getString(1);
				if(!"".equals(estatusCuenta))
					estatusCampo = ",ic_estatus_cecoban";

			}//while
			rs.close();ps.close();

			// Si la cuenta a registrar es en dolares, autom�ticamente se almacenar�n con el estatus 99 (Cuenta Correcta).
			if(ic_moneda.equals("54")){
				estatusCampo 	= ",ic_estatus_cecoban";
				estatusCuenta 	= ",99";
			}

			qrySentencia =  " SELECT nvl(max(ic_cuenta),0) as total ";
			qrySentencia += " FROM com_cuentas ";
			qrySentencia += " WHERE ic_nafin_electronico = " + ic_nafin_electronico;
			qrySentencia += " AND ic_tipo_cuenta = " + ic_tipo_cuenta;
			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado)) {
				qrySentencia += " AND ic_epo = " + ic_epo;
			}

			System.out.print("DispersionBean :: ovinsertarCuenta :: Query "+qrySentencia);

			ResultSet rsBusqueda = con.queryDB(qrySentencia);

			if (rsBusqueda.next()){
				System.out.print("\nCampo"+rsBusqueda.getString("total")+" \n");
				sResBusqueda = (rsBusqueda.getString("total") == null)? "0" : rsBusqueda.getString("total");
			}

			Calendar cFechaSigHabil = new GregorianCalendar();
			cFechaSigHabil.setTime(new java.util.Date());
			cFechaSigHabil.add(Calendar.DATE, 1);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
				cFechaSigHabil.add(Calendar.DATE, 2);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
				cFechaSigHabil.add(Calendar.DATE, 1);
			Vector vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
			boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
			cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			while(bInhabil) {
				vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
				bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
				if(!bInhabil)
					break;
				cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			}
			String sFechaSigHabil = sdf.format(cFechaSigHabil.getTime());
			System.out.println("Fecha siguiente: "+sFechaSigHabil);

			String condicioncampo = "";
			String condicioncampojsp = "";

			if(!"".equals(ic_epo)) {
				condicioncampo = ",ic_epo";
				condicioncampojsp = ","+ic_epo;
			}


			qrySentencia = " INSERT INTO com_cuentas  ";
			qrySentencia += " ( ";
			qrySentencia += " ic_cuenta, ic_nafin_electronico, ic_producto_nafin, ic_moneda, ";
			qrySentencia += " ic_bancos_tef, cg_cuenta, ic_usuario, df_ultima_mod,  ";
			qrySentencia += " df_registro, df_transferencia, df_aplicacion, ic_tipo_cuenta, cg_tipo_afiliado "+condicioncampo+estatusCampo + ",";
			qrySentencia += " cg_plaza_swift, ";
			qrySentencia += " cg_sucursal_swift ";
			qrySentencia += " ) ";
			qrySentencia += " VALUES ";
			qrySentencia += " ( ";
			qrySentencia += " " + sSigLlave + ", " + ic_nafin_electronico + " , " + ic_producto_nafin + ", " + ic_moneda + ", ";
			qrySentencia += " " + ic_bancos_tef + ", '" + cg_cuenta + "', '" + ic_usuario + "', SYSDATE, ";
			qrySentencia += " SYSDATE, to_date('" + sFechaSigHabil + "','dd/mm/yyyy'), to_date('" + sFechaSigHabil +"','dd/mm/yyyy'), " + ic_tipo_cuenta + " , '"+cg_tipo_afiliado+"' "+condicioncampojsp+estatusCuenta + ",";
			qrySentencia += (plazaSwift    == null?"NULL":"'"+plazaSwift    +"'")+",";
			qrySentencia += (sucursalSwift == null?"NULL":"'"+sucursalSwift +"'");
			qrySentencia += " ) ";

			System.out.print("DispersionBean :: ovinsertarCuenta :: Query "+qrySentencia);

			con.ejecutaSQL(qrySentencia);

			// Registrar en BITACORA el ALTA DE LA CUENTA para cuando la Clave del Afiliado al que se le di�
			// de alta la cuenta sea: P(PYME), D(DISTRIBUIDOR),I(IF Bancario/No Bancario) y B(IF/Beneficiario)
			if(
				"P".equals(cg_tipo_afiliado) ||
				"D".equals(cg_tipo_afiliado) ||
				"I".equals(cg_tipo_afiliado) ||
				"B".equals(cg_tipo_afiliado)
			){

				// Registrar la informacion registrada para la cuenta
				String decripcionAlta = (String) parametrosAdicionales.get("DESCRIPCION_ALTA");
				decripcionAlta = decripcionAlta == null?"ALTA\nNO DISPONIBLE":decripcionAlta;
				String clavePantalla  = (String) parametrosAdicionales.get("PANTALLA_ORIGEN");
				clavePantalla 	= clavePantalla  == null?"REGINDCTAS"    :clavePantalla;

				//boolean esCuentaCLABE = "40".equals(ic_tipo_cuenta)?true:false;
				boolean esCuentaSWIFT = "50".equals(ic_tipo_cuenta)?true:false;

				String valoresActuales =
					decripcionAlta + "\n" +
					"cg_cuenta="+cg_cuenta+"\n"+
					"ic_producto_nafin="+ic_producto_nafin+"\n"+
					"ic_moneda="+ic_moneda+"\n"+
					"ic_bancos_tef="+ic_bancos_tef+"\n"+
					"ic_tipo_cuenta="+ic_tipo_cuenta+"\n"+
					"cg_plaza_swift="+   (esCuentaSWIFT?(plazaSwift    == null?"":plazaSwift)   :"N/A")+"\n"+
					"cg_sucursal_swift="+(esCuentaSWIFT?(sucursalSwift == null?"":sucursalSwift):"N/A")+"\n"+
					"ic_epo="+ic_epo;

				Bitacora.grabarEnBitacora(
						con,
						clavePantalla,															//clavePantalla,
						"B".equals(cg_tipo_afiliado)?"I":cg_tipo_afiliado,			//tipoAfiliado; Si el tipo es "B" usar "I"
						ic_nafin_electronico, 												//claveNafinElectronico,
						ic_usuario,																//claveUsuario,
						"",																		//datoAnterior,
						valoresActuales                                          //datoActual
				);

			}

			return sSigLlave;
		}catch(NafinException ne){
			System.out.println("ovinsertarCuenta::NafinException");
			ne.printStackTrace();
			resultado = false;
			throw ne;
		}catch(Exception e){
			System.out.println("ovinsertarCuenta::Exception");
			e.printStackTrace();
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			System.out.println("\nDispersionEJB::ovinsertarCuenta (S)");
		}
	} /*Fin del M�todo ovinsertarCuenta*/


	//-------------------------------------------------------------------------------------------
	//	String sLlaveSiguiente()
	//
	//-------------------------------------------------------------------------------------------
		public String sLlaveSiguiente() throws NafinException{
		AccesoDB		con 			= null;
		ResultSet		rs				= null;
		String			qrySentencia	= "";
		String 			sResultado		= "";

		try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia = " SELECT  NVL(MAX(ic_cuenta),0)+1 FROM com_cuentas ";

			System.out.print("DispersionBean :: sLlaveSiguiente :: Query "+qrySentencia);

			rs = con.queryDB(qrySentencia);

			while(rs.next()){
				sResultado = rs.getString(1);
			}

			return sResultado;

		}catch(Exception e){

			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){

				con.cierraConexionDB();
			}
		}
	}/*Fin del M�todo sLlaveSiguiente*/

	//-------------------------------------------------------------------------------------------
	//	boolean bEsDiaInhabil()
	//
	//-------------------------------------------------------------------------------------------
	public Vector bEsDiaInhabil(String esFechaAplicacion, Calendar cFechaSigHabil, AccesoDB lodbConexion)
		throws NafinException{
		boolean lbOK = true;
		String lsCodError = "OK";
		boolean lbExisteDiaInhabil = false;
		ResultSet lrsSel = null;
		Vector vFecha = new Vector();

		System.out.println("bEsDiaInhabil(E)");
		try {
			String lsCadenaSQL = "select * from comcat_dia_inhabil"+
								" where cg_dia_inhabil = '"+esFechaAplicacion.substring(0,5)+"'"+
								" and cg_dia_inhabil is not null ";
			try {
				lrsSel = lodbConexion.queryDB(lsCadenaSQL);
				if(lrsSel.next()) {
					lbExisteDiaInhabil = true;
					cFechaSigHabil.add(Calendar.DATE, 1);
					if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
						cFechaSigHabil.add(Calendar.DATE, 2);
					if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
						cFechaSigHabil.add(Calendar.DATE, 1);
				} else
					lbExisteDiaInhabil = false;

				lrsSel.close();
				lodbConexion.cierraStatement();

				vFecha.addElement(new Boolean(lbExisteDiaInhabil));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.YEAR)));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.MONTH)));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.DAY_OF_MONTH)));

			} catch (Exception error){
				throw new NafinException("DESC0041");
			}

			return vFecha;
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			if(!lbOK) throw new NafinException(lsCodError);
			System.out.println("bEsDiaInhabil(S)");
		}
	}/*Fin del M�todo bEsDiaInhabil*/


	public Vector bEsDiaInhabil(String esFechaAplicacion, Calendar cFechaSigHabil)
		throws NafinException{
		boolean lbOK = true;
		String lsCodError = "OK";
		boolean lbExisteDiaInhabil = false;
		ResultSet lrsSel = null;
		Vector vFecha = new Vector();
	    AccesoDB conn = new AccesoDB();
	    

		System.out.println("bEsDiaInhabil(E)");
		try {
			conn.conexionDB();
			String lsCadenaSQL = 
					" SELECT * FROM comcat_dia_inhabil "+
					" WHERE cg_dia_inhabil = ? "+
					" AND cg_dia_inhabil IS NOT NULL ";
			PreparedStatement ps = conn.queryPrecompilado(lsCadenaSQL);
			ps.setString(1, esFechaAplicacion.substring(0,5));
			
			lrsSel = ps.executeQuery();
			if(lrsSel.next()) {
				lbExisteDiaInhabil = true;
				cFechaSigHabil.add(Calendar.DATE, 1);
				if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) { // S�bado
						cFechaSigHabil.add(Calendar.DATE, 2);
				}
				if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) {// Domingo
						cFechaSigHabil.add(Calendar.DATE, 1);
				}
			} else {
				lbExisteDiaInhabil = false;
			}

			lrsSel.close();
			ps.close();

			vFecha.add(Boolean.valueOf(lbExisteDiaInhabil));
			vFecha.add(new Integer(cFechaSigHabil.get(Calendar.YEAR)));
			vFecha.add(new Integer(cFechaSigHabil.get(Calendar.MONTH)));
			vFecha.add(new Integer(cFechaSigHabil.get(Calendar.DAY_OF_MONTH)));

			return vFecha;
		} catch (Exception epError){
			throw new AppException("Error al determinar dias inhabiles", epError);
		} finally {
		   log.info("bEsDiaInhabil(S)");
			if (conn.hayConexionAbierta()) {
				conn.cierraConexionDB();
			}
		}
	}/*Fin del M�todo bEsDiaInhabil*/




	//-------------------------------------------------------------------------------------------
	//	boolean sgetNumaxDispersion()
	//
	//-------------------------------------------------------------------------------------------
	public String sgetNumaxDispersion(String sCuenta) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsSiguiente="0";
	String sTabla = "";

		if ("TEF".equalsIgnoreCase(sCuenta)){
			sTabla = " comtmp_cuentas_tef ";
		} else if ("CECOBAN".equalsIgnoreCase(sCuenta)){
			sTabla = " comtmp_cuentas_cecoban ";
		}

		try{
			lobdConexion.conexionDB();
			String lsQry = "select decode(max(ic_proceso),null,1,max(ic_proceso)+1) as CVEPROC "+
							"from " + sTabla;
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next())
				lsSiguiente = lrsQry.getString("CVEPROC");
			else
				throw new NafinException("ANTI0020");
			lrsQry.close();
			lobdConexion.cierraStatement();
		} catch (NafinException ne) {
			throw ne;
		} catch(Exception e){
			System.out.println("Exception ocurrida en sgetNumaxDispersion(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lsSiguiente;
	}/*Fin del M�todo sgetNumaxDispersion*/

	//-------------------------------------------------------------------------------------------
	//	boolean checaFecha(String formato)
	//
	//-------------------------------------------------------------------------------------------
    private boolean checaFecha(String formato) {
        String date=formato;
        StringTokenizer st=new StringTokenizer(date,"-");
        int dia=0, mes=0, ano=0;    boolean isFecha = false;
        if(st.countTokens()==3){
            try {
                dia = Integer.parseInt(st.nextToken());
                mes = Integer.parseInt(st.nextToken())-1;
                ano = Integer.parseInt(st.nextToken());       //System.out.println(dia+"/"+mes+"/"+ano);
                Calendar fecha = new GregorianCalendar(ano, mes, dia);
                int year = fecha.get(Calendar.YEAR);
                int month = fecha.get(Calendar.MONTH);
                int day = fecha.get(Calendar.DAY_OF_MONTH);
                //System.out.println(year+" "+month+" "+day);
                if((dia!=day) || (mes!=month) || (ano!=year))
                    isFecha = false;
                else
                    isFecha = true;
            }
            catch(NoSuchElementException nse) { isFecha = false; }
            catch(NumberFormatException nf) { isFecha = false; }
        } //if
    return isFecha;
    } /*Fin del M�todo checaFecha*/

	//-------------------------------------------------------------------------------------------
	//	Hashtable ohprocesarDispersion(String esDocumentos, String lsProcSirac)
	//
	//-------------------------------------------------------------------------------------------
	public Hashtable ohprocesarDispersionTEF(String esDocumentos, String lsIcProceso) throws NafinException {
    String lsMsgError = "", lsMsgOk = "", lsLinea = "";
  	int liNumLinea = 1, liLineaArchivo = 0;
  	boolean bOk = true;	//verifica individualmente cada registro
  	boolean bTodosOk = true; //revisa que todos los registros sean correctos
  	VectorTokenizer lvtValores = null;
  	Vector lvDatos = null;
	StringBuffer lsbError = new StringBuffer();
  	StringBuffer lsbCorrecto = new StringBuffer();
  	StringBuffer lsbContArchivo = new StringBuffer();

  	String lsDSN = "";
	lsDSN = "anticipoDS";

	String lsFechaRegistro = "", lsFechaTransferencia = "", lsFechaAplicacion = "", lsRFC = "", lsNombre = "";
	String lsAdscripcion = "", lsClaveBanco = "", lsTipoCuenta = "", lsNumeroCuenta = "", lsReferenciaRastreo = "";
	String lsLeyendaRastreo = "", lsMotivoRechazo = "";

	Vector lovFechaRegistro 		= new Vector();
  	Vector lovFechaTransferencia	= new Vector();
  	Vector lovFechaAplicacion 		= new Vector();
	Vector lovRFC 					= new Vector();
  	Vector lovNombre				= new Vector();
  	Vector lovAdscripcion 			= new Vector();
	Vector lovClaveBanco 			= new Vector();
  	Vector lovTipoCuenta			= new Vector();
  	Vector lovNumeroCuenta 			= new Vector();
	Vector lovReferenciaRastreo 	= new Vector();
  	Vector lovLeyendaRastreo		= new Vector();
  	Vector lovMotivoRechazo 		= new Vector();

	boolean lbFechaRegistro = false, lbFechaTransferencia = false, lbFechaAplicacion = false;

  	String lsQry = "", lsColumnas = "", lsValores = "";

  	Hashtable lohResultados = new Hashtable();
  	ResultSet lorsBusca = null;

System.out.println("DispersionBean::ohprocesarDispersionTEF");

  	AccesoDB lobdConexion = new AccesoDB();
  		try	{
  			lobdConexion.conexionDB();
  			StringTokenizer lstDocto = new StringTokenizer(esDocumentos,"\n");
  			while(lstDocto.hasMoreElements()) {
  				lsLinea = lstDocto.nextToken();
  				lsMsgError = "|Error en la linea: "+ liNumLinea + ", ";
  				lsMsgOk = "Linea: "+ liNumLinea ;
	  			bOk = true;
	  			if(lsLinea.equals(""))	//si la linea es vacia nos brincamos a la siguente iteraci�n
	  				continue;
	  			lvtValores = new VectorTokenizer(lsLinea,";");
	  			lvDatos = lvtValores.getValuesVector();
	             System.out.println("\nlvDatos.size "+lvDatos.size());
  				if (lvDatos.size() != 13) {
  					bTodosOk = bOk = false;
  					lsbError.append(lsMsgError +" no coincide con el layout. Por favor verifiquelo.\n\n");
					if(liLineaArchivo != liNumLinea) {
						liLineaArchivo = liNumLinea;
						lsbContArchivo.append(lsLinea+"\n");
					}
  				}// if lvDatos
  				else {  //Obtiene informaci�n
  					lsFechaRegistro = (lvDatos.get(0)==null) ? "" : lvDatos.get(0).toString();
  					lsFechaTransferencia = (lvDatos.get(1)==null) ? "" : lvDatos.get(1).toString();
  					lsFechaAplicacion = (lvDatos.get(2)==null) ? "" : lvDatos.get(2).toString();
  					lsRFC = (lvDatos.get(3)==null) ? "" : lvDatos.get(3).toString();
  					lsNombre = (lvDatos.get(4)==null) ? "" : lvDatos.get(4).toString();
   					lsAdscripcion = (lvDatos.get(5)==null) ? "" : lvDatos.get(5).toString();
  					lsClaveBanco = (lvDatos.get(6)==null) ? "" : lvDatos.get(6).toString();
  					lsTipoCuenta = (lvDatos.get(7)==null) ? "" : lvDatos.get(7).toString();
  					lsNumeroCuenta = (lvDatos.get(8)==null) ? "" : lvDatos.get(8).toString();
  					lsReferenciaRastreo = (lvDatos.get(9)==null) ? "" : lvDatos.get(9).toString();
   					lsLeyendaRastreo = (lvDatos.get(10)==null) ? "" : lvDatos.get(10).toString();
  					lsMotivoRechazo = (lvDatos.get(11)==null) ? "" : lvDatos.get(11).toString();

  					//validar que sea Fecha de Registro y con formato DD-MM-YYYY
					lbFechaRegistro = checaFecha(lsFechaRegistro);
					if (lbFechaRegistro == false) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El formato de la Fecha de Registro es incorrecto.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lbFechaRegistro == false)*/

  					//validar que sea Fecha de Transferencia y con formato DD-MM-YYYY
					lbFechaTransferencia = checaFecha(lsFechaTransferencia);
					if (lbFechaTransferencia == false) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El formato de la Fecha de Transferencia es incorrecto.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lbFechaTransferencia == false)*/

  					//validar que sea Fecha de Aplicaci�n y con formato DD-MM-YYYY
					lbFechaAplicacion = checaFecha(lsFechaAplicacion);
					if (lbFechaAplicacion == false) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El formato de la Fecha de Aplicaci�n es incorrecto.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lbFechaAplicacion == false)*/

  					//validar que la longitud sea correcta
					if (lsRFC.length() != 13 && lsRFC.length() != 12) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" La longitud del formato del RFC es incorrecto.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lsRFC.length() != 13)*/

					//validar que la longitud sea correcta
					if (lsNombre.length() > 30 || "".equals(lsNombre)) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" La longitud del formato del Nombre es incorrecto.\n\n");
   						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lsNombre.length() > 30)*/

					//validar que la informacion sea correcta
					if (!"HDA".equals(lsAdscripcion)) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El campo Adscripci�n es incorrecto.\n\n");
   						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (!"HDA".equalsIgnoreCase(lsAdscripcion))*/

  					//validar que sea Numero
					boolean bEsNumero = false;
					bEsNumero = Comunes.esNumero(lsClaveBanco);
					if (!bEsNumero) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" La Clave de Banco debe ser Num�rica.\n\n");
						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (bNumero == false)*/

					//validar que la Clave de Banco sea correcta
					if(!lsClaveBanco.equals("")) {
		  				lsQry = "SELECT ic_bancos_tef FROM comcat_bancos_tef WHERE ic_bancos_tef = " + lsClaveBanco;

			  			System.out.println("Query 1 "+lsQry);
			   			lorsBusca = lobdConexion.queryDB(lsQry);

				      	if(!lorsBusca.next()) {
				      		bTodosOk = bOk = false;
			  				lsbError.append(lsMsgError +" no existe la Clave de Banco: "+lsClaveBanco+".\n\n");
 			  				if(liLineaArchivo != liNumLinea) {
			  					liLineaArchivo = liNumLinea;
			  					lsbContArchivo.append(lsLinea+"\n");
			  				}
				  		}
		                lobdConexion.cierraStatement();
					} else {
				      		bTodosOk = bOk = false;
			  				lsbError.append(lsMsgError +" La Clave de Banco no es v�lida: "+lsClaveBanco+".\n\n");
 			  				if(liLineaArchivo != liNumLinea) {
			  					liLineaArchivo = liNumLinea;
			  					lsbContArchivo.append(lsLinea+"\n");
			  				}
					}

					 /*if(!lsClaveBanco.equals(""))*/

					//validar que la informacion de Tipo de Cuenta sea correcta
					if (!"1".equals(lsTipoCuenta) && !"40".equals(lsTipoCuenta)) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El campo Tipo de Cuenta es incorrecto.\n\n");
   						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (!"1".equals(lsTipoCuenta) || !"40".equals(lsTipoCuenta))*/

					//validar que la informacion de Numero de Cuenta sea correcta
					if (!"1".equals(lsTipoCuenta) && !"40".equals(lsTipoCuenta)) {
							bTodosOk = bOk = false;
	  						lsbError.append(lsMsgError +" El Tipo de Cuenta es incorrecto.\n\n");
 	  						if(liLineaArchivo != liNumLinea) {
	  							liLineaArchivo = liNumLinea;
	  							lsbContArchivo.append(lsLinea+"\n");
	  						}
  					}

					//validar que la informacion de Numero de Cuenta sea correcta
					if ("1".equals(lsTipoCuenta)) {
  						if (lsNumeroCuenta.length() > 11 || lsNumeroCuenta.length() < 11) {
							bTodosOk = bOk = false;
	  						lsbError.append(lsMsgError +" La longitud de Numero de Cuenta es incorrecto.\n\n");
 	  						if(liLineaArchivo != liNumLinea) {
	  							liLineaArchivo = liNumLinea;
	  							lsbContArchivo.append(lsLinea+"\n");
	  						}
						}
  					} else  if ("40".equals(lsTipoCuenta)) {
		  						if (lsNumeroCuenta.length() > 18 || lsNumeroCuenta.length() < 18) {
									bTodosOk = bOk = false;
			  						lsbError.append(lsMsgError +" La longitud de Numero de Cuenta es incorrecto.\n\n");
									if(liLineaArchivo != liNumLinea) {
			  							liLineaArchivo = liNumLinea;
			  							lsbContArchivo.append(lsLinea+"\n");
			  						}
								}
		  					}
							  					//validar que sea Numero
					/*bEsNumero = false;
					bEsNumero = Comunes.esNumero(lsNumeroCuenta);
					if (!bEsNumero) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El N�mero de Cuenta debe ser Num�rico.\n\n");
						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					}*/ /*if (bNumero == false)*/

					//validar que la longitud de Leyenda de Rastreo sea correcta
					if (lsReferenciaRastreo.length() > 30 || "".equals(lsReferenciaRastreo) ) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" La longitud de Leyenda de Rastreo es incorrecta.\n\n");
   						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lsReferenciaRastreo.length() > 40)*/

					//validar que la longitud de Leyenda de Rastreo sea correcta
					if (lsLeyendaRastreo.length() > 40 || "".equals(lsLeyendaRastreo)) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" La longitud de Leyenda de Rastreo es incorrecta.\n\n");
   						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lsLeyendaRastreo.length() > 40)*/
					//validar que el Motivo del Rechazo sea correcto
					if(!lsMotivoRechazo.equals("")) {
						bEsNumero = false;
						bEsNumero = Comunes.esNumero(lsMotivoRechazo);
						if (!bEsNumero) {
	  						bTodosOk = bOk = false;
	  						lsbError.append(lsMsgError +" El Motivo del Rechazo debe ser Num�rico.\n\n");
							if(liLineaArchivo != liNumLinea) {
	  							liLineaArchivo = liNumLinea;
	  							lsbContArchivo.append(lsLinea+"\n");
	  						}
	  					} else {
			  				lsQry = "SELECT ic_estatus_tef FROM comcat_estatus_tef WHERE ic_estatus_tef = " + lsMotivoRechazo;

				  			System.out.println("Query lsMotivoRechazo "+lsQry);
				   			lorsBusca = lobdConexion.queryDB(lsQry);

					      	if(!lorsBusca.next()) {
					      		bTodosOk = bOk = false;
				  				lsbError.append(lsMsgError +" No existe el Motivo de Rechazo: "+lsMotivoRechazo+".\n\n");
	 			  				if(liLineaArchivo != liNumLinea) {
				  					liLineaArchivo = liNumLinea;
				  					lsbContArchivo.append(lsLinea+"\n");
				  				}
					  		}
			                lobdConexion.cierraStatement();
						}
					}else {
				      		bTodosOk = bOk = false;
			  				lsbError.append(lsMsgError +" El Motivo de Rechazo no es v�lido: "+lsMotivoRechazo+".\n\n");
 			  				if(liLineaArchivo != liNumLinea) {
			  					liLineaArchivo = liNumLinea;
			  					lsbContArchivo.append(lsLinea+"\n");
			  				}
					} /*if(!lsMotivoRechazo.equals(""))*/

					if (bOk) {
						lsColumnas = "(ic_proceso,df_registro,df_transferencia,df_aplicacion,cg_rfc,cg_nombre,cg_adscripcion,";
						lsColumnas += "ic_clave_banco,ic_tipo_cuenta,cg_numero_cuenta,cg_referencia_rastreo,cg_leyenda_rastreo,ic_motivo_rechazo)";
						lsValores = "('"+lsIcProceso+"','"+lsFechaRegistro+"','"+lsFechaTransferencia+"','"+lsFechaAplicacion+"','"+lsRFC+"','";
						lsValores += lsNombre+"','"+lsAdscripcion+"','"+lsClaveBanco+"','"+lsTipoCuenta+"','"+lsNumeroCuenta+"','";
						lsValores += lsReferenciaRastreo+"','"+lsLeyendaRastreo+"','"+lsMotivoRechazo+"')";

						lsQry = "insert into comtmp_cuentas_tef "+lsColumnas+
								"values "+lsValores;
						System.out.println("\nQuery 6 "+lsQry);
						lobdConexion.ejecutaSQL(lsQry);

						lsbCorrecto.append(lsMsgOk+"  Cuenta: "+lsNumeroCuenta+"|\n\n");
						lovFechaRegistro.add(lsFechaRegistro);
					  	lovFechaTransferencia.add(lsFechaTransferencia);
					  	lovFechaAplicacion.add(lsFechaAplicacion);
						lovRFC.add(lsRFC);
					  	lovNombre.add(lsNombre);
					  	lovAdscripcion.add(lsAdscripcion);
						lovClaveBanco.add(lsClaveBanco);
					  	lovTipoCuenta.add(lsTipoCuenta);
					  	lovNumeroCuenta.add(lsNumeroCuenta);
						lovReferenciaRastreo.add(lsReferenciaRastreo);
					  	lovLeyendaRastreo.add(lsLeyendaRastreo);
					  	lovMotivoRechazo.add(lsMotivoRechazo);
					} //if Ok
				} // else lvDatos
				// Vaciar los vectores
				lvDatos.removeAllElements();
				liNumLinea++;
			}//while lsLinea

		} //try
		catch(Exception e) {
			bTodosOk = false;
			lsbError.append(lsMsgError + " El Layout es incorrecto \n\n");
			if(liLineaArchivo != liNumLinea) {
				liLineaArchivo = liNumLinea;
				lsbContArchivo.append(lsLinea+"\n");
			}
			System.out.println(lobdConexion.mostrarError());
			System.out.println("Exception ocurrida en hprocesarAnticipos(). Sobrecargado "+e.getMessage());
			throw new NafinException("ANTI0020");
		}


		finally {
			lobdConexion.terminaTransaccion(true);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();

			lohResultados.put("bTodosOk",new Boolean(bTodosOk));
			lohResultados.put("lovFechaRegistro",lovFechaRegistro);
			lohResultados.put("lovFechaTransferencia",lovFechaTransferencia);
			lohResultados.put("lovFechaAplicacion",lovFechaAplicacion);
			lohResultados.put("lovRFC",lovRFC);
			lohResultados.put("lovNombre",lovNombre);
			lohResultados.put("lovAdscripcion",lovAdscripcion);
			lohResultados.put("lovClaveBanco",lovClaveBanco);
			lohResultados.put("lovTipoCuenta",lovTipoCuenta);
			lohResultados.put("lovNumeroCuenta",lovNumeroCuenta);
			lohResultados.put("lovReferenciaRastreo",lovReferenciaRastreo);
			lohResultados.put("lovLeyendaRastreo",lovLeyendaRastreo);
			lohResultados.put("lovMotivoRechazo",lovMotivoRechazo);
			lohResultados.put("lsbError",lsbError.toString());
			lohResultados.put("lsbCorrecto",lsbCorrecto.toString());
			lohResultados.put("lsbContArchivo",lsbContArchivo.toString());
		}
	return lohResultados;
	}/*Fin del M�todo ohprocesarDispersion*/



	//-------------------------------------------------------------------------------------------
	//	Hashtable ohprocesarDispersion(String esDocumentos, String lsProcSirac)
	//
	//-------------------------------------------------------------------------------------------
	public Hashtable ohprocesarDispersionCECOBAN(String esDocumentos, String lsIcProceso) throws NafinException {
    String lsMsgError = "", lsMsgOk = "", lsLinea = "";
  	int liNumLinea = 1, liLineaArchivo = 0;
  	boolean bOk = true;	//verifica individualmente cada registro
  	boolean bTodosOk = true; //revisa que todos los registros sean correctos
  	VectorTokenizer lvtValores = null;
  	Vector lvDatos = null;
	StringBuffer lsbError = new StringBuffer();
  	StringBuffer lsbCorrecto = new StringBuffer();
  	StringBuffer lsbContArchivo = new StringBuffer();

  	String lsDSN = "";
	lsDSN = "anticipoDS";

	String lsRastreo = "",  lsFechaRegistro = "", lsFechaTransferencia = "", lsFechaAplicacion = "", lsRFC = "";
	String lsTipoCuenta = "", lsNumeroCuenta = "", lsReferenciaRastreo = "";
	String lsLeyendaRastreo = "", lsEstatus = "", lsNafElec = "";

	Vector lovRastreo		 		= new Vector();
	Vector lovFechaRegistro 		= new Vector();
  	Vector lovFechaTransferencia	= new Vector();
  	Vector lovFechaAplicacion 		= new Vector();
	Vector lovRFC 					= new Vector();
  	Vector lovTipoCuenta			= new Vector();
  	Vector lovNumeroCuenta 			= new Vector();
	Vector lovReferenciaRastreo 	= new Vector();
  	Vector lovLeyendaRastreo		= new Vector();
  	Vector lovEstatus		 		= new Vector();

	boolean bEsFecha = false;
	boolean bEsNumero = false;

  	String lsQry = "", lsColumnas = "", lsValores = "";

  	Hashtable lohResultados = new Hashtable();
  	ResultSet lorsBusca = null;

System.out.println("DispersionBean::ohprocesarDispersionCECOBAN");

  	AccesoDB lobdConexion = new AccesoDB();
  		try	{
  			lobdConexion.conexionDB();
  			StringTokenizer lstDocto = new StringTokenizer(esDocumentos,"\n");
  			while(lstDocto.hasMoreElements()) {
  				lsLinea = lstDocto.nextToken();
  				lsMsgError = "|Error en la linea: "+ liNumLinea + ", ";
  				lsMsgOk = "Linea: "+ liNumLinea ;
	  			bOk = true;
	  			if(lsLinea.equals(""))	//si la linea es vacia nos brincamos a la siguente iteraci�n
	  				continue;
	  			lvtValores = new VectorTokenizer(lsLinea,";");
	  			lvDatos = lvtValores.getValuesVector();
				System.out.println("\nlvDatos.size "+lvDatos.size());
				System.out.println("\nlvDatos.size "+lvDatos);
  				if (lvDatos.size() < 10 || lvDatos.size() > 12) {
  					bTodosOk = bOk = false;
  					lsbError.append(lsMsgError +" no coincide con el layout. Por favor verifiquelo.\n\n");
					if(liLineaArchivo != liNumLinea) {
						liLineaArchivo = liNumLinea;
						lsbContArchivo.append(lsLinea+"\n");
					}
  				}// if lvDatos
  				else {  //Obtiene informaci�n
  					lsRastreo 				= (lvDatos.get( 0)==null) ? "" : lvDatos.get( 0).toString();
  					lsFechaRegistro 		= (lvDatos.get( 1)==null) ? "" : lvDatos.get( 1).toString();
  					lsFechaTransferencia	= (lvDatos.get( 2)==null) ? "" : lvDatos.get( 2).toString();
  					lsFechaAplicacion 		= (lvDatos.get( 3)==null) ? "" : lvDatos.get( 3).toString();
  					lsRFC 					= (lvDatos.get( 4)==null) ? "" : lvDatos.get( 4).toString();
  					lsTipoCuenta 			= (lvDatos.get( 5)==null) ? "" : lvDatos.get( 5).toString();
  					lsNumeroCuenta 			= (lvDatos.get( 6)==null) ? "" : lvDatos.get( 6).toString();
  					lsReferenciaRastreo 	= (lvDatos.get( 7)==null) ? "" : lvDatos.get( 7).toString();
   					lsLeyendaRastreo 		= (lvDatos.get( 8)==null) ? "" : lvDatos.get( 8).toString();
  					lsEstatus 				= (lvDatos.get( 9)==null) ? "" : lvDatos.get( 9).toString();
  					lsNafElec 				= "";
  					if(lvDatos.size()>11)
  						lsNafElec			= (lvDatos.get(11)==null) ? "" : lvDatos.get(11).toString();

  					//validar que sea Numero y longiruf 7
					bEsNumero = Comunes.esNumero(lsRastreo);
					if (!bEsNumero) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El Rastreo es incorrecto.\n\n");
						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (bNumero == false)*/

  					//validar que sea Fecha de Registro y con formato DD-MM-YYYY
					bEsFecha = checaFecha(lsFechaRegistro);
					if (!bEsFecha) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El formato de la Fecha de Registro es incorrecto.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lbFechaRegistro == false)*/

  					//validar que sea Fecha de Transferencia y con formato DD-MM-YYYY
					bEsFecha = false;
					bEsFecha = checaFecha(lsFechaTransferencia);
					if (!bEsFecha) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El formato de la Fecha de Transferencia es incorrecto.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lbFechaTransferencia == false)*/

  					//validar que sea Fecha de Aplicaci�n y con formato DD-MM-YYYY
					bEsFecha = false;
					bEsFecha = checaFecha(lsFechaAplicacion);
					if (!bEsFecha) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El formato de la Fecha de Aplicaci�n es incorrecto.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lbFechaAplicacion == false)*/

  					//validar que la longitud sea correcta
					if (lsRFC.length() != 13 && lsRFC.length() != 12) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" La longitud del formato del RFC es incorrecto.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lsRFC.length() != 13)*/

					//validar que la informacion de Tipo de Cuenta sea correcta
					if (!"1".equals(lsTipoCuenta) && !"40".equals(lsTipoCuenta)) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El campo Tipo de Cuenta es incorrecto.\n\n");
   						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (!"1".equals(lsTipoCuenta) || !"40".equals(lsTipoCuenta))*/

					//validar que la informacion de Numero de Cuenta sea correcta
					if ("1".equals(lsTipoCuenta)) {
  						if (lsNumeroCuenta.length() != 11) {
							bTodosOk = bOk = false;
	  						lsbError.append(lsMsgError +" La longitud de N�mero de Cuenta es incorrecto.\n\n");
 	  						if(liLineaArchivo != liNumLinea) {
	  							liLineaArchivo = liNumLinea;
	  							lsbContArchivo.append(lsLinea+"\n");
	  						}
						}
  					} else  if ("40".equals(lsTipoCuenta)) {
		  						if (lsNumeroCuenta.length() != 18) {
									bTodosOk = bOk = false;
			  						lsbError.append(lsMsgError +" La longitud de N�mero de Cuenta es incorrecto.\n\n");
									if(liLineaArchivo != liNumLinea) {
			  							liLineaArchivo = liNumLinea;
			  							lsbContArchivo.append(lsLinea+"\n");
			  						}
								}
		  			  }

  					//validar que sea Numero
					bEsNumero = false;
					bEsNumero = Comunes.esNumero(lsNumeroCuenta);
					if (!bEsNumero) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El N�mero de Cuenta debe ser Numerico.\n\n");
						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (bNumero == false)*/


					//validar que la longitud de Referencia de Rastreo sea correcta
					if (lsReferenciaRastreo.length() > 30 || "".equals(lsReferenciaRastreo) ) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" La longitud del Nombre es incorrecta.\n\n");
   						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lsReferenciaRastreo.length() > 30)*/

					//validar que la longitud de Leyenda de Rastreo sea correcta
					if (lsLeyendaRastreo.length() > 40 || "".equals(lsLeyendaRastreo)) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" La longitud de Leyenda de Rastreo es incorrecta.\n\n");
   						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (lsReferenciaRastreo.length() > 40)*/

  					//validar que sea Numero

					//validar que el Estatus sea correcto
					if(!lsEstatus.equals("")) {

						bEsNumero = false;
						bEsNumero = Comunes.esNumero(lsEstatus);
						if (!bEsNumero) {
	  						bTodosOk = bOk = false;
	  						lsbError.append(lsMsgError +" El Estatus debe ser Num�rico.\n\n");
							if(liLineaArchivo != liNumLinea) {
	  							liLineaArchivo = liNumLinea;
	  							lsbContArchivo.append(lsLinea+"\n");
	  						}
	  					} else {
							lsQry = "SELECT ic_estatus_cecoban FROM comcat_estatus_cecoban WHERE ic_estatus_cecoban = " + lsEstatus;

				  			System.out.println("Query lsEstatus "+lsQry);
				   			lorsBusca = lobdConexion.queryDB(lsQry);

					      	if(!lorsBusca.next()) {
					      		bTodosOk = bOk = false;
				  				lsbError.append(lsMsgError +" no existe el Estatus: "+lsEstatus+".\n\n");
	 			  				if(liLineaArchivo != liNumLinea) {
				  					liLineaArchivo = liNumLinea;
				  					lsbContArchivo.append(lsLinea+"\n");
								}
				  			}
				  			lobdConexion.cierraStatement();
					  	 }

					} else {
	  						bTodosOk = bOk = false;
	  						lsbError.append(lsMsgError +" El Estatus debe ser v�lido.\n\n");
							if(liLineaArchivo != liNumLinea) {
	  							liLineaArchivo = liNumLinea;
	  							lsbContArchivo.append(lsLinea+"\n");
	  						}
					}

					//} /*if(!lsMotivoRechazo.equals(""))*/

  					//validar que sea Numero y longiruf 7
  					if(!"".equals(lsNafElec)) {
						bEsNumero = Comunes.esNumero(lsNafElec);
						if (!bEsNumero) {
	  						bTodosOk = bOk = false;
	  						lsbError.append(lsMsgError +" El Numero de N@E es incorrecto.\n\n");
							if(liLineaArchivo != liNumLinea) {
	  							liLineaArchivo = liNumLinea;
	  							lsbContArchivo.append(lsLinea+"\n");
	  						}
	  					} /*if (bNumero == false)*/
  					}


  					String lsNumNafinElec = validaRFCUnico(lsRFC, lsNafElec, lobdConexion);

  					//validar la relacion 1 - 1 entre el RFC y un tipo de Usuario
					if ("".equals(lsNumNafinElec)) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +lsRFC+" RFC asignado a distintos usuarios, verifiquelo. \n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  						lsNumNafinElec = "NULL";
  					}//if ("".equals(tabla))

  					if("-1".equals(lsNumNafinElec)) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El RFC no es valido, verifiquelo. \n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  						lsNumNafinElec = "NULL";
  					}


					if (bOk) {
						lsColumnas = "(ic_proceso,ig_rastreo,df_registro,df_transferencia,df_aplicacion,cg_rfc,";
						lsColumnas += "ic_tipo_cuenta,cg_numero_cuenta,cg_referencia_rastreo,cg_leyenda_rastreo,cg_estatus, ic_nafin_electronico)";
						lsValores = "('"+lsIcProceso+"','"+lsRastreo+"','"+lsFechaRegistro+"','"+lsFechaTransferencia+"','"+lsFechaAplicacion+"','"+lsRFC+"','";
						lsValores += lsTipoCuenta+"','"+lsNumeroCuenta+"','"+lsReferenciaRastreo+"','";
						lsValores += lsLeyendaRastreo+"','"+lsEstatus+"',"+lsNumNafinElec+")";

						lsQry = "insert into comtmp_cuentas_cecoban "+lsColumnas+
								"values "+lsValores;
						System.out.println("\nQuery 6 "+lsQry);
						lobdConexion.ejecutaSQL(lsQry);

						lsbCorrecto.append(lsMsgOk+"  Cuenta: "+lsNumeroCuenta+"|\n\n");
						lovRastreo.add(lsRastreo);
						lovFechaRegistro.add(lsFechaRegistro);
					  	lovFechaTransferencia.add(lsFechaTransferencia);
					  	lovFechaAplicacion.add(lsFechaAplicacion);
						lovRFC.add(lsRFC);
					  	lovTipoCuenta.add(lsTipoCuenta);
					  	lovNumeroCuenta.add(lsNumeroCuenta);
						lovReferenciaRastreo.add(lsReferenciaRastreo);
					  	lovLeyendaRastreo.add(lsLeyendaRastreo);
					  	lovEstatus.add(lsEstatus);

					} //if Ok
				} // else lvDatos
				// Vaciar los vectores
				lvDatos.removeAllElements();
				liNumLinea++;
			}//while lsLinea

		} //try
		catch(Exception e) {
			bTodosOk = false;
			lsbError.append(lsMsgError + " El Layout es incorrecto \n\n");
			if(liLineaArchivo != liNumLinea) {
				liLineaArchivo = liNumLinea;
				lsbContArchivo.append(lsLinea+"\n");
			}
			System.out.println(lobdConexion.mostrarError());
			System.out.println("Exception ocurrida en hprocesarAnticipos(). Sobrecargado "+e.getMessage());
			throw new NafinException("ANTI0020");
		} finally {
			lobdConexion.terminaTransaccion(true);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();

			lohResultados.put("bTodosOk",new Boolean(bTodosOk));
			lohResultados.put("lovRastreo",lovRastreo);
			lohResultados.put("lovFechaRegistro",lovFechaRegistro);
			lohResultados.put("lovFechaTransferencia",lovFechaTransferencia);
			lohResultados.put("lovFechaAplicacion",lovFechaAplicacion);
			lohResultados.put("lovRFC",lovRFC);
			lohResultados.put("lovTipoCuenta",lovTipoCuenta);
			lohResultados.put("lovNumeroCuenta",lovNumeroCuenta);
			lohResultados.put("lovReferenciaRastreo",lovReferenciaRastreo);
			lohResultados.put("lovLeyendaRastreo",lovLeyendaRastreo);
			lohResultados.put("lovEstatus",lovEstatus);
			lohResultados.put("lsbError",lsbError.toString());
			lohResultados.put("lsbCorrecto",lsbCorrecto.toString());
			lohResultados.put("lsbContArchivo",lsbContArchivo.toString());
		}
		return lohResultados;
	}/*Fin del M�todo ohprocesarDispersion*/

	private String validaRFCUnico(String esRFC, String lsNafElec, AccesoDB lobdConexion)
			throws NafinException {
		System.out.println("DispersionEJB::validaRFCUnico(E)");

		System.out.println("esRFC: "+esRFC);
		System.out.println("lsNafElec: "+lsNafElec);

		String 				cadAux 			= "";
		String 				sTabla			= "";
		String 				tipoUsuario		= "";
		StringBuffer 		lsConsulta 		= new StringBuffer();
		ResultSet			rs				= null;
		PreparedStatement 	ps 				= null;
		int					cont			= 0;
		String 				sId 			= "";
		String 				nafelec			= "";
		try{
			//prepara el rfc
			cadAux = esRFC.substring(0,4);
			cadAux = cadAux.trim() + "-" + esRFC.substring(4,10);
			cadAux = cadAux.trim() + "-" + esRFC.substring(10);
			System.out.print("\nAntes "+esRFC);
			esRFC = cadAux;
			System.out.print("\nDespues "+cadAux);

			if(!"".equals(lsNafElec)) {
				lsConsulta.append(" select cg_tipo from comrel_nafin where ic_nafin_electronico = ? ");
				System.out.print("\nConsultaN@E "+lsConsulta.toString());
				ps = lobdConexion.queryPrecompilado(lsConsulta.toString());
				ps.setString(1, lsNafElec);
				rs = ps.executeQuery();
				ps.clearParameters();
				if (rs.next())
					tipoUsuario =(rs.getString(1) == null) ? "" : rs.getString(1);
				rs.close();
				ps.close();
			}

			if("".equals(tipoUsuario) || "E".equals(tipoUsuario)) {
				//Busca en la tabla de comcat_epo si existe el rfc
				lsConsulta.delete(0,lsConsulta.length());
				lsConsulta.append(" SELECT ic_epo FROM  comcat_epo WHERE  cg_rfc = ? ");
				System.out.print("\nConsultaEpo "+lsConsulta.toString());
				ps = lobdConexion.queryPrecompilado(lsConsulta.toString());
				ps.setString(1, esRFC);
				rs = ps.executeQuery();
				ps.clearParameters();
				if (rs.next()) {
					sId=(rs.getString(1) == null) ? "" : rs.getString(1);
					sTabla = "E";
					cont++;
				}
				rs.close();
				ps.close();
			}

			if("".equals(tipoUsuario) || "P".equals(tipoUsuario)) {
				//Busca en la tabla de comcat_pyme si existe el rfc
				lsConsulta.delete(0,lsConsulta.length());
				lsConsulta.append(" SELECT ic_pyme FROM  comcat_pyme WHERE  cg_rfc = ? ");
				System.out.print("\nConsultaPyme "+lsConsulta.toString());
				ps = lobdConexion.queryPrecompilado(lsConsulta.toString());
				ps.setString(1, esRFC);
				rs = ps.executeQuery();
				ps.clearParameters();
				if (rs.next()) {
					sId=(rs.getString(1) == null) ? "" : rs.getString(1);
					sTabla = "P";
					cont++;
				}
				rs.close();
				ps.close();
			}

			if("".equals(tipoUsuario) || "I".equals(tipoUsuario)) {
				//Busca en la tabla de comcat_if si existe el rfc
				lsConsulta.delete(0,lsConsulta.length());
				lsConsulta.append(" SELECT ic_if FROM  comcat_if WHERE  cg_rfc = ? ");
				System.out.print("\nConsultaIf "+lsConsulta.toString());
				ps = lobdConexion.queryPrecompilado(lsConsulta.toString());
				ps.setString(1, esRFC);
				rs = ps.executeQuery();
				ps.clearParameters();
				if (rs.next()) {
					sId=(rs.getString(1) == null) ? "" : rs.getString(1);
					sTabla = "I";
					cont++;
				}
				rs.close();
				ps.close();
			}

			if(cont>1)
				sTabla = "";

System.out.print("\n sId "+sId);
System.out.print("\n sTabla "+sTabla);
			if(!"".equals(sId)) {
				lsConsulta.delete(0,lsConsulta.length());
				lsConsulta.append(" SELECT ic_nafin_electronico, ic_epo_pyme_if, cg_tipo FROM comrel_nafin WHERE ic_epo_pyme_if = ? and cg_tipo = ?");
				System.out.print("\nConsultaIf "+lsConsulta.toString());
				ps = lobdConexion.queryPrecompilado(lsConsulta.toString());
				ps.setInt(1, Integer.parseInt(sId));
				ps.setString(2, sTabla);
				rs = ps.executeQuery();
				ps.clearParameters();
				if (rs.next()) {
					nafelec = rs.getString("ic_nafin_electronico")==null?"":rs.getString("ic_nafin_electronico");
				}
				rs.close();
				ps.close();
			} else
				nafelec = "-1";

		} catch (Exception e){
			nafelec = "";
			e.printStackTrace();
			System.out.println("DispersionEJB::validaRFCUnico(Exception) "+e);
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("\nDispersionEJB::validaRFCUnico(S)");
		}
		return nafelec;
	}

	public boolean bactualizarDispersion( String esTipoCuenta, String esRFC, String esEstatus, String esCuenta)
			throws NafinException {
		boolean bExiste = false;
		try {
			bExiste = bactualizarDispersion(esTipoCuenta, esRFC, esEstatus, esCuenta, "");
		} catch(Exception e) {
			e.printStackTrace();
		}
		return bExiste;
	}

	private boolean bactualizarDispersion( String esTipoCuenta, String esRFC, String esEstatus, String esCuenta, String esNafElec)
			throws NafinException {
		String lsDSN = "anticipoDS";

	AccesoDB lobdConexion = new AccesoDB();

	String sTabla = "";
	String sCampo = "";
	StringBuffer lsConsulta = new StringBuffer();
	ResultSet rs = null;
	boolean bExiste = false;
	String sId = "";
	String cadAux = "";

	boolean lbOk = true;

		if ("TEF".equals(esTipoCuenta))
			sCampo = " ic_estatus_tef ";

		if ("CECOBAN".equals(esTipoCuenta))
			sCampo = " ic_estatus_cecoban ";

		try{
			lobdConexion.conexionDB();

			if("".equals(esNafElec)) {
				//prepara el rfc
				cadAux = esRFC.substring(0,4);
				cadAux = cadAux.trim() + "-" + esRFC.substring(4,10);
				cadAux = cadAux.trim() + "-" + esRFC.substring(10);
				System.out.print("\nAntes "+esRFC);
				esRFC = cadAux;
				System.out.print("\nDespues "+cadAux);

				//Busca en la tabla de comcat_epo si existe el rfc
				lsConsulta.append(" SELECT ic_epo FROM  comcat_epo WHERE  cg_rfc = '" + esRFC + "'");
				System.out.print("\nConsultaEpo "+lsConsulta.toString());
				rs= lobdConexion.queryDB(lsConsulta.toString());
				if (rs.next()) {
					sId=(rs.getString(1) == null) ? "" : rs.getString(1);
					sTabla = "E";
					bExiste = true;
				}
				rs.close();
				lobdConexion.cierraStatement();

				//Busca en la tabla de comcat_pyme si existe el rfc
				lsConsulta.delete(0,lsConsulta.length());
				lsConsulta.append(" SELECT ic_pyme FROM  comcat_pyme WHERE  cg_rfc = '" + esRFC + "'");
				System.out.print("\nConsultaPyme "+lsConsulta.toString());
				if (bExiste == false) {
					rs= lobdConexion.queryDB(lsConsulta.toString());
					if (rs.next()) {
						sId=(rs.getString(1) == null) ? "" : rs.getString(1);
						sTabla = "P";
						bExiste = true;
					}
					rs.close();
					lobdConexion.cierraStatement();
				}

				//Busca en la tabla de comcat_if si existe el rfc
				lsConsulta.delete(0,lsConsulta.length());
				lsConsulta.append(" SELECT ic_if FROM  comcat_if WHERE  cg_rfc = '" + esRFC + "'");
				System.out.print("\nConsultaIf "+lsConsulta.toString());
				if (bExiste == false) {
					rs= lobdConexion.queryDB(lsConsulta.toString());
					if (rs.next()) {
						sId=(rs.getString(1) == null) ? "" : rs.getString(1);
						sTabla = "I";
						bExiste = true;
					}
					rs.close();
					lobdConexion.cierraStatement();
				}
			}//if("".equals(esTipoUsuario))
			else {
				bExiste = true;
			}

			if (bExiste) {
				if("".equals(esNafElec)) {
					lsConsulta.delete(0,lsConsulta.length());
					lsConsulta.append(" SELECT ic_nafin_electronico, ic_epo_pyme_if, cg_tipo FROM comrel_nafin WHERE ic_epo_pyme_if = " + sId + " and cg_tipo = '" + sTabla + "'");
					System.out.print("\nConsultaNE "+lsConsulta.toString());
					rs= lobdConexion.queryDB(lsConsulta.toString());
					if (rs.next()) {
						esNafElec = rs.getString("IC_NAFIN_ELECTRONICO");
					}
					rs.close();
					lobdConexion.cierraStatement();
			 	}

				String lsQry =
					" update com_cuentas " +
					" set " + sCampo + " = " + esEstatus + ", " +
					" df_ultima_mod = SYSDATE ";

				if ("CECOBAN".equals(esTipoCuenta))
					lsQry += " , ic_estatus_tef = null ";

				lsQry +=  " where ic_nafin_electronico = " + esNafElec + " and cg_cuenta = '" + esCuenta + "'";

				try {

					System.out.println("bactualizarAnticipo::lsQry"+lsQry);
					lobdConexion.ejecutaSQL(lsQry);
				} catch (SQLException sqle){
				lbOk = false;
				throw new NafinException("ANTI0020");
				}
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e){
			lbOk = false;
			System.out.println("Exception ocurrida en actualizarAnticipo(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(lbOk);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOk;
	}




	public Vector ovgetDispersionesProcesadasTEF(String lsIcProceso) throws NafinException {


	AccesoDB lobdConexion = new AccesoDB();
	Vector lovAnticipos = new Vector();
	Vector lovRegistro = null;
	boolean bOkActualiza = false;
	//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	String lsFechaRegistro = "", lsFechaTransferencia = "", lsFechaAplicacion = "", lsRFC = "", lsNombre = "";
	String lsAdscripcion = "", lsClaveBanco = "", lsTipoCuenta = "", lsNumeroCuenta = "", lsReferenciaRastreo = "";
	String lsLeyendaRastreo = "", lsMotivoRechazo = "";

		try{
			lobdConexion.conexionDB();

			String lsQry = 	"select ic_proceso,df_registro,df_transferencia,df_aplicacion,cg_rfc,cg_nombre,cg_adscripcion,"+
							" ic_clave_banco,ic_tipo_cuenta,cg_numero_cuenta,cg_referencia_rastreo,cg_leyenda_rastreo,ic_motivo_rechazo "+
							" from comtmp_cuentas_tef where ic_proceso = "+lsIcProceso+
							" order by 10";

			System.out.println("------------------------------------------------");
			System.out.println("lsQry"+lsQry);

			ResultSet lrsDatos = lobdConexion.queryDB(lsQry);
			while(lrsDatos.next()) {
				lovRegistro = new Vector();

				lsFechaRegistro = (lrsDatos.getString("df_registro")==null)?"":lrsDatos.getString("df_registro");
				lsFechaTransferencia = (lrsDatos.getString("df_transferencia")==null)?"":lrsDatos.getString("df_transferencia");
				lsFechaAplicacion = (lrsDatos.getString("df_aplicacion")==null)?"":lrsDatos.getString("df_aplicacion");
				lsRFC = (lrsDatos.getString("cg_rfc")==null)?"":lrsDatos.getString("cg_rfc");
				lsNombre = (lrsDatos.getString("cg_nombre")==null)?"":lrsDatos.getString("cg_nombre");
				lsAdscripcion = (lrsDatos.getString("cg_adscripcion")==null)?"":lrsDatos.getString("cg_adscripcion");
				lsClaveBanco = (lrsDatos.getString("ic_clave_banco")==null)?"":lrsDatos.getString("ic_clave_banco");
				lsTipoCuenta = (lrsDatos.getString("ic_tipo_cuenta")==null)?"":lrsDatos.getString("ic_tipo_cuenta");
				lsNumeroCuenta = (lrsDatos.getString("cg_numero_cuenta")==null)?"":lrsDatos.getString("cg_numero_cuenta");
				lsReferenciaRastreo = (lrsDatos.getString("cg_referencia_rastreo")==null)?"":lrsDatos.getString("cg_referencia_rastreo");
				lsLeyendaRastreo = (lrsDatos.getString("cg_leyenda_rastreo")==null)?"":lrsDatos.getString("cg_leyenda_rastreo");
				lsMotivoRechazo = (lrsDatos.getString("ic_motivo_rechazo")==null)?"":lrsDatos.getString("ic_motivo_rechazo");

				System.out.println("lsFechaRegistro"+lsFechaRegistro);
				System.out.println("lsFechaTransferencia"+lsFechaTransferencia);
				System.out.println("lsFechaAplicacion"+lsFechaAplicacion);
				System.out.println("lsRFC"+lsRFC);
				System.out.println("lsNombre"+lsNombre);
				System.out.println("lsAdscripcion"+lsAdscripcion);
				System.out.println("lsClaveBanco"+lsClaveBanco);
				System.out.println("lsTipoCuenta"+lsTipoCuenta);
				System.out.println("lsNumeroCuenta"+lsNumeroCuenta);
				System.out.println("lsReferenciaRastreo"+lsReferenciaRastreo);
				System.out.println("lsLeyendaRastreo"+lsLeyendaRastreo);
				System.out.println("lsMotivoRechazo"+lsMotivoRechazo);

				bOkActualiza = bactualizarDispersion("TEF",lsRFC, lsMotivoRechazo, lsNumeroCuenta);

				lovRegistro.add(lsFechaRegistro);
				lovRegistro.add(lsFechaTransferencia);
				lovRegistro.add(lsFechaAplicacion);
				lovRegistro.add(lsRFC);
				lovRegistro.add(lsNombre);
				lovRegistro.add(lsAdscripcion);
				lovRegistro.add(lsClaveBanco);
				lovRegistro.add(lsTipoCuenta);
				lovRegistro.add(lsNumeroCuenta);
				lovRegistro.add(lsReferenciaRastreo);
				lovRegistro.add(lsLeyendaRastreo);
				lovRegistro.add(lsMotivoRechazo);

				lovAnticipos.addElement(lovRegistro);
			}
			lobdConexion.cierraStatement();

		} /*catch (NafinException ne) {
			throw ne;
		}*/ catch (Exception e){
			System.out.println("Exception ocurrida en getAnticiposProcesados(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}

	return lovAnticipos;
	}

	public Vector ovgetDispersionesProcesadasCECOBAN(String lsIcProceso) throws NafinException {

	AccesoDB lobdConexion = new AccesoDB();
	Vector lovAnticipos = new Vector();
	Vector lovRegistro = null;
	boolean bOkActualiza = false;

	String lsRastreo = "",lsFechaRegistro = "", lsFechaTransferencia = "", lsFechaAplicacion = "", lsRFC = "";
	String lsTipoCuenta = "", lsNumeroCuenta = "", lsReferenciaRastreo = "";
	String lsLeyendaRastreo = "", lsEstatus = "", lsNafElec = "";

		try{
			lobdConexion.conexionDB();

			String lsQry = 	"select ic_proceso,ig_rastreo,df_registro,df_transferencia,df_aplicacion,cg_rfc,"+
							" ic_tipo_cuenta,cg_numero_cuenta,cg_referencia_rastreo,cg_leyenda_rastreo,cg_estatus "+
							" ,ic_nafin_electronico "+
							" from comtmp_cuentas_cecoban where ic_proceso = "+lsIcProceso+
							" order by 8";

			System.out.println("------------------------------------------------");
			System.out.println("lsQry"+lsQry);

			ResultSet lrsDatos = lobdConexion.queryDB(lsQry);
			while(lrsDatos.next()) {
				lovRegistro = new Vector();

				lsRastreo = (lrsDatos.getString("ig_rastreo")==null)?"":lrsDatos.getString("ig_rastreo");
				lsFechaRegistro = (lrsDatos.getString("df_registro")==null)?"":lrsDatos.getString("df_registro");
				lsFechaTransferencia = (lrsDatos.getString("df_transferencia")==null)?"":lrsDatos.getString("df_transferencia");
				lsFechaAplicacion = (lrsDatos.getString("df_aplicacion")==null)?"":lrsDatos.getString("df_aplicacion");
				lsRFC = (lrsDatos.getString("cg_rfc")==null)?"":lrsDatos.getString("cg_rfc");
				lsTipoCuenta = (lrsDatos.getString("ic_tipo_cuenta")==null)?"":lrsDatos.getString("ic_tipo_cuenta");
				lsNumeroCuenta = (lrsDatos.getString("cg_numero_cuenta")==null)?"":lrsDatos.getString("cg_numero_cuenta");
				lsReferenciaRastreo = (lrsDatos.getString("cg_referencia_rastreo")==null)?"":lrsDatos.getString("cg_referencia_rastreo");
				lsLeyendaRastreo = (lrsDatos.getString("cg_leyenda_rastreo")==null)?"":lrsDatos.getString("cg_leyenda_rastreo");
				lsEstatus = (lrsDatos.getString("cg_estatus")==null)?"":lrsDatos.getString("cg_estatus");
				lsNafElec = (lrsDatos.getString("ic_nafin_electronico")==null)?"":lrsDatos.getString("ic_nafin_electronico");

				System.out.println("lsRastreo"+lsRastreo);
				System.out.println("lsFechaRegistro"+lsFechaRegistro);
				System.out.println("lsFechaTransferencia"+lsFechaTransferencia);
				System.out.println("lsFechaAplicacion"+lsFechaAplicacion);
				System.out.println("lsRFC"+lsRFC);
				System.out.println("lsTipoCuenta"+lsTipoCuenta);
				System.out.println("lsNumeroCuenta"+lsNumeroCuenta);
				System.out.println("lsReferenciaRastreo"+lsReferenciaRastreo);
				System.out.println("lsLeyendaRastreo"+lsLeyendaRastreo);
				System.out.println("lsEstatus"+lsEstatus);
				System.out.println("lsNafElec"+lsNafElec);

				bOkActualiza = bactualizarDispersion("CECOBAN",lsRFC, lsEstatus, lsNumeroCuenta, lsNafElec);

				lovRegistro.add(lsEstatus);
				lovRegistro.add(lsFechaRegistro);
				lovRegistro.add(lsFechaTransferencia);
				lovRegistro.add(lsFechaAplicacion);
				lovRegistro.add(lsRFC);
				lovRegistro.add(lsTipoCuenta);
				lovRegistro.add(lsNumeroCuenta);
				lovRegistro.add(lsReferenciaRastreo);
				lovRegistro.add(lsLeyendaRastreo);
				lovRegistro.add(lsEstatus);

				lovAnticipos.addElement(lovRegistro);
			}
			lobdConexion.cierraStatement();

		} /*catch (NafinException ne) {
			throw ne;
		}*/ catch (Exception e){
			System.out.println("Exception ocurrida en getAnticiposProcesados(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lovAnticipos;
	}

	public boolean bborrarDispersionesTmp(String lsProcSirac, String lsCuenta) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	boolean lbOk = true;
	String sTabla = "";


		try{
			lobdConexion.conexionDB();

			if ("TEF".equalsIgnoreCase(lsCuenta))
				sTabla = "comtmp_cuentas_tef";

			if ("CECOBAN".equalsIgnoreCase(lsCuenta))
				sTabla = "comtmp_cuentas_cecoban";

			String lsQry = " delete " + sTabla + " where ic_proceso = " +lsProcSirac;
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOk = false;
				throw new NafinException("ANTI0020");
			}

		} catch (Exception e){
			lbOk = false;
			System.out.println("Exception ocurrida en borrar SiracTmp(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(lbOk);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOk;
	}







	//-------------------------------------------------------------------------------------------
	//	boolean sgetNumaxDispersion()
	//
	//-------------------------------------------------------------------------------------------
	public String sgetNumaxAsignacion() throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsSiguiente="0";

		try{
			lobdConexion.conexionDB();
			String lsQry = " select decode(max(ic_session),null,1,max(ic_session)+1) as CVEPROC "+
							" from comtmp_asignaprod_epo_pyme ";
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next())
				lsSiguiente = lrsQry.getString("CVEPROC");
			else
				throw new NafinException("ANTI0020");
			lrsQry.close();
			lobdConexion.cierraStatement();
		} catch (NafinException ne) {
			throw ne;
		} catch(Exception e){
			System.out.println("Exception ocurrida en sgetNumaxAsignacion(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lsSiguiente;
	}/*Fin del M�todo sgetNumaxDispersion*/

	//-------------------------------------------------------------------------------------------
	//	Hashtable ohprocesarDispersion(String esDocumentos, String lsProcSirac)
	//
	//-------------------------------------------------------------------------------------------
	public Hashtable ohprocesarAsignacion(String esDocumentos, String lsIcProceso, String ic_epo) throws NafinException {
    String lsMsgError = "", lsMsgOk = "", lsLinea = "";
  	int liNumLinea = 1, liLineaArchivo = 0;
  	boolean bOk = true;	//verifica individualmente cada registro
  	boolean bTodosOk = true; //revisa que todos los registros sean correctos
  	VectorTokenizer lvtValores = null;
  	Vector lvDatos = null;
	StringBuffer lsbError = new StringBuffer();
  	StringBuffer lsbCorrecto = new StringBuffer();
  	StringBuffer lsbContArchivo = new StringBuffer();

  	String lsDSN = "";
	lsDSN = "anticipoDS";

	String lsNumero = "", lsClave = "", lsTipoMovimiento= "";

	Vector lovNumero		 		= new Vector();
  	Vector lovClave					= new Vector();
  	Vector lovTipoMovimiento 		= new Vector();

  	String lsQry = "", lsColumnas = "", lsValores = "";

  	Hashtable lohResultados = new Hashtable();
  	ResultSet lorsBusca = null;

	boolean bEsNumero = false;


System.out.println("DispersionBean::ohprocesarAsignacion");

  	AccesoDB lobdConexion = new AccesoDB();
  		try	{
  			lobdConexion.conexionDB();
  			StringTokenizer lstDocto = new StringTokenizer(esDocumentos,"\n");
  			while(lstDocto.hasMoreElements()) {
  				lsLinea = lstDocto.nextToken();
  				lsMsgError = "Error en la linea: "+ liNumLinea + ", ";
  				lsMsgOk = "Linea: "+ liNumLinea ;
	  			bOk = true;
	  			if(lsLinea.equals(""))	//si la linea es vacia nos brincamos a la siguente iteraci�n
	  				continue;
	  			lvtValores = new VectorTokenizer(lsLinea,"|");
	  			lvDatos = lvtValores.getValuesVector();
	             System.out.println("\nlvDatos.size "+lvDatos.size());
  				if (lvDatos.size() != 3) {
  					bTodosOk = bOk = false;
  					lsbError.append(lsMsgError +" no coincide con el layout. Por favor verifiquelo.\n\n");
					if(liLineaArchivo != liNumLinea) {
						liLineaArchivo = liNumLinea;
						lsbContArchivo.append(lsLinea+"\n");
					}
  				}// if lvDatos
  				else {  //Obtiene informaci�n
  					lsNumero = (lvDatos.get(0)==null) ? "" : lvDatos.get(0).toString();
  					lsClave = (lvDatos.get(1)==null) ? "" : lvDatos.get(1).toString();
  					lsTipoMovimiento = (lvDatos.get(2)==null) ? "" : lvDatos.get(2).toString();

  					//validar que sea Numero
					bEsNumero = false;
					bEsNumero = Comunes.esNumero(lsNumero);
					if (!bEsNumero) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El N�mero de Proveedor PYME debe ser Num�rico.\n\n");
						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (bNumero == false)*/

					//validar que exista
					if(!"".equals(lsNumero)) {
		  				lsQry = "SELECT ic_pyme, cs_aceptacion FROM comrel_pyme_epo WHERE ic_epo = " + ic_epo + " AND cg_pyme_epo_interno = '" + lsNumero +"'";

			  			System.out.println("Query lsNumero "+lsQry);
			   			lorsBusca = lobdConexion.queryDB(lsQry);

				      	if(lorsBusca.next()) {
							if (!"S".equals(lorsBusca.getString("cs_aceptacion")) && !"R".equals(lorsBusca.getString("cs_aceptacion")) && !"H".equals(lorsBusca.getString("cs_aceptacion"))) {
					      		bTodosOk = bOk = false;
				  				lsbError.append(lsMsgError +" No se encuentre afiliado a cadenas como susceptible de descuento. el Num. Interno  "+lsNumero+"  y Num. EPO "  + ic_epo + ".\n\n");
	 			  				if(liLineaArchivo != liNumLinea) {
				  					liLineaArchivo = liNumLinea;
				  					lsbContArchivo.append(lsLinea+"\n");
				  				}
							}
				  		} else {
				      		bTodosOk = bOk = false;
			  				lsbError.append(lsMsgError +" no existe la PYME con Num. Interno  "+lsNumero+"  y Num. EPO "  + ic_epo + ".\n\n");
 			  				if(liLineaArchivo != liNumLinea) {
			  					liLineaArchivo = liNumLinea;
			  					lsbContArchivo.append(lsLinea+"\n");
			  				}
						}
		                lobdConexion.cierraStatement();
					} /*if(!"".equals(lsNumero))*/

  					//validar que sea Numero
					bEsNumero = false;
					bEsNumero = Comunes.esNumero(lsClave);
					if (!bEsNumero) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" La Clave de Producto debe ser Num�rico.\n\n");
						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (bNumero == false)*/

  					//validar que sea Numero
					if ( !"A".equals(lsTipoMovimiento) && !"B".equals(lsTipoMovimiento) ) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" El Tipo de Movimiento no es v�lido.\n\n");
						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					} /*if (bNumero == false)*/

					if (bOk) {
						lsColumnas = "(ic_session,cg_pyme_epo_interno,ic_producto_nafin,cs_tipo_mov)";
						lsValores = "('"+lsIcProceso+"','"+lsNumero+"','"+lsClave+"','"+lsTipoMovimiento+"')";

						lsQry = "insert into comtmp_asignaprod_epo_pyme "+lsColumnas+
								"values "+lsValores;
						System.out.println("\nQuery 6 "+lsQry);
						lobdConexion.ejecutaSQL(lsQry);

						lsbCorrecto.append(lsMsgOk+"  Cuenta: "+lsNumero+"\n\n");
						lovNumero.add(lsNumero);
					  	lovClave.add(lsClave);
					  	lovTipoMovimiento.add(lsTipoMovimiento);
					} //if Ok
				} // else lvDatos
				// Vaciar los vectores
				lvDatos.removeAllElements();
				liNumLinea++;
			}//while lsLinea

		} //try
		catch(Exception e) {
			bTodosOk = false;
			lsbError.append(lsMsgError + " El Layout es incorrecto \n\n");
			if(liLineaArchivo != liNumLinea) {
				liLineaArchivo = liNumLinea;
				lsbContArchivo.append(lsLinea+"\n");
			}
			System.out.println(lobdConexion.mostrarError());
			System.out.println("Exception ocurrida en ohprocesarAsignacion(). "+e.getMessage());
			throw new NafinException("ANTI0020");
		}


		finally {
			lobdConexion.terminaTransaccion(true);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();

			lohResultados.put("bTodosOk",new Boolean(bTodosOk));
			lohResultados.put("lovNumero",lovNumero);
			lohResultados.put("lovClave",lovClave);
			lohResultados.put("lovTipoMovimiento",lovTipoMovimiento);
			lohResultados.put("lsbError",lsbError.toString());
			lohResultados.put("lsbCorrecto",lsbCorrecto.toString());
			lohResultados.put("lsbContArchivo",lsbContArchivo.toString());
		}
	return lohResultados;
	}/*Fin del M�todo ohprocesarDispersion*/

	//-------------------------------------------------------------------------------------------
	//	Vector ovgetAsignacionProcesadas(String lsIcProceso, String ic_epo)
	//
	//-------------------------------------------------------------------------------------------
	public Vector ovgetAsignacionProcesadas(String lsIcProceso, String ic_epo) throws NafinException {

	AccesoDB lobdConexion = new AccesoDB();
	Vector lovAnticipos = new Vector();
	Vector lovRegistro = null;
	boolean bOkActualiza = false;

	String lsNumero = "", lsCuenta = "", lsTipoMovimiento = "";

		try{
			lobdConexion.conexionDB();

			String lsQry = 	"select ic_session,cg_pyme_epo_interno,ic_producto_nafin,cs_tipo_mov"+
							" from comtmp_asignaprod_epo_pyme where ic_session = "+lsIcProceso;

			System.out.println("lsQry"+lsQry);

			ResultSet lrsDatos = lobdConexion.queryDB(lsQry);
			while(lrsDatos.next()) {
				lovRegistro = new Vector();

				lsNumero = (lrsDatos.getString("cg_pyme_epo_interno")==null)?"":lrsDatos.getString("cg_pyme_epo_interno");
				lsCuenta = (lrsDatos.getString("ic_producto_nafin")==null)?"":lrsDatos.getString("ic_producto_nafin");
				lsTipoMovimiento = (lrsDatos.getString("cs_tipo_mov")==null)?"":lrsDatos.getString("cs_tipo_mov");

				System.out.println("\nlsFechaRegistro"+lsNumero);
				System.out.println("\nlsFechaTransferencia"+lsCuenta);
				System.out.println("\nlsFechaAplicacion"+lsTipoMovimiento);

				bOkActualiza = bactualizarAsignacion(lsNumero, lsCuenta, lsTipoMovimiento, ic_epo);

				lovRegistro.add(lsNumero);
				lovRegistro.add(lsCuenta);
				lovRegistro.add(lsTipoMovimiento);

				lovAnticipos.addElement(lovRegistro);
			}
			lobdConexion.cierraStatement();

		} /*catch (NafinException ne) {
			throw ne;
		}*/ catch (Exception e){
			System.out.println("Exception ocurrida en getAnticiposProcesados(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lovAnticipos;
	}/*Fin del M�todo ovgetAsignacionProcesadas*/

	//-------------------------------------------------------------------------------------------
	//	boolean bactualizarAsignacion(String esNumero, String esCuenta, String esTipoMovimiento, String ic_epo)
	//
	//-------------------------------------------------------------------------------------------
	public boolean bactualizarAsignacion(String esNumero, String esCuenta, String esTipoMovimiento, String ic_epo) throws NafinException {
	String lsDSN = "anticipoDS";

	AccesoDB lobdConexion = new AccesoDB();

	String lsQry = "";
	boolean lbOk = true;

		try{
			lobdConexion.conexionDB();
			lsQry =	" update comrel_pyme_epo set ";
			lsQry += " cs_aceptacion = '" + esTipoMovimiento + "' ";
			lsQry += " where ic_epo  = " + ic_epo + " ";
			lsQry += " and cg_pyme_epo_interno = '" + esNumero + "' ";

			System.out.println("bactualizarAsignacion::lsQry"+lsQry);
			lobdConexion.ejecutaSQL(lsQry);

			if (!"2".equals(esCuenta)){
				lsQry =	 " select ic_epo, ic_pyme, cg_pyme_epo_interno ";
				lsQry += " from comrel_pyme_epo ";
				lsQry += " where ic_epo = "+ ic_epo + " ";
				lsQry += " and cg_pyme_epo_interno = '" + esNumero + "' ";

				System.out.println("bactualizarAsignacion::lsQry"+lsQry);
				ResultSet lrsDatos = lobdConexion.queryDB(lsQry);

				if(lrsDatos.next()) {

					lsQry =	 " select ic_epo, ic_pyme, ic_producto_nafin  ";
					lsQry += " from comrel_pyme_epo_x_producto ";
					lsQry += " where ic_epo = "+ ic_epo + " ";
					lsQry += " and ic_pyme = " + lrsDatos.getString("ic_pyme");
					lsQry += " and ic_producto_nafin = " + esCuenta;

					System.out.println("bactualizarAsignacion::lsQry"+lsQry);
					ResultSet lrs_x_prod = lobdConexion.queryDB(lsQry);

					if(lrs_x_prod.next()) {

						lsQry =	" update comrel_pyme_epo_x_producto set ";
						lsQry += " cs_habilitado = 'S' ";
						lsQry += " df_habilitado = SYSDATE ";
						lsQry += " where ic_epo  = " + ic_epo + " ";
						lsQry += " and ic_pyme = " + lrsDatos.getString("ic_pyme");
						lsQry += " and ic_producto_nafin = " + esCuenta;

					} else {

						String ic_producto = lrsDatos.getString("ic_pyme");

						lsQry =	" insert into  comrel_pyme_epo_x_producto (ic_epo, ic_pyme, ic_producto_nafin, cs_habilitado, df_habilitado) values (" + ic_epo +"," + ic_producto + "," + esCuenta + ", 'S', SYSDATE) ";

					}

					System.out.println("bactualizarAsignacion::lsQry"+lsQry);
					lobdConexion.ejecutaSQL(lsQry);

				}
			}

		} catch (Exception e){
			lbOk = false;
			System.out.println("Exception ocurrida en actualizarAsignacion(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(lbOk);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOk;
	}/*Fin del M�todo bactualizarAsignacion*/

	//-------------------------------------------------------------------------------------------
	//	boolean bborrarAsignacionesTmp(String lsIcProceso)
	//
	//-------------------------------------------------------------------------------------------
	public boolean bborrarAsignacionesTmp(String lsIcProceso) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	boolean lbOk = true;
		try{
			lobdConexion.conexionDB();


			String lsQry = " delete comtmp_asignaprod_epo_pyme where ic_session = " +lsIcProceso;
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOk = false;
				throw new NafinException("ANTI0020");
			}

		} catch (Exception e){
			lbOk = false;
			System.out.println("Exception ocurrida en borrar SiracTmp(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(lbOk);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOk;
	}/*Fin del M�todo bborrarAsignacionesTmp*/

	public String getFechaSiguienteHabil() throws NafinException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar cFechaSigHabil = new GregorianCalendar();
			cFechaSigHabil.setTime(new java.util.Date());
			cFechaSigHabil.add(Calendar.DATE, 1);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
				cFechaSigHabil.add(Calendar.DATE, 2);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
				cFechaSigHabil.add(Calendar.DATE, 1);
			Vector vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
			boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
			cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			while(bInhabil) {
				vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
				bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
				if(!bInhabil)
					break;
				cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			}
			return sdf.format(cFechaSigHabil.getTime());
		}catch(Exception e) {
			System.out.println("Excepcion ocurrida en getFechaSiguienteHabil(): "+e.getMessage());
			e.printStackTrace();
			throw new NafinException("DISP0002");
		}
	}

	//	String getFechaSiguienteHabilPS() -- realizado por HDG -- 30/06/2003 --

	public String getFechaSiguienteHabilPS() throws NafinException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cFechaSigHabil = new GregorianCalendar();
			cFechaSigHabil.setTime(new java.util.Date());
			cFechaSigHabil.add(Calendar.DATE, 1);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
				cFechaSigHabil.add(Calendar.DATE, 2);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
				cFechaSigHabil.add(Calendar.DATE, 1);
			Vector vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
			boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
			cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			while(bInhabil) {
				vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
				bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
				if(!bInhabil)
					break;
				cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			}
			return sdf.format(cFechaSigHabil.getTime());
		}catch(Exception e) {
			System.out.println("Excepcion ocurrida en getFechaSiguienteHabilPS(): "+e.getMessage());
			e.printStackTrace();
			throw new NafinException("DISP0002");
		}
	}

	//	String getFechaSiguienteHabil(String sFecha) -- realizado por HDG -- 22/09/2003 --

	public String getFechaSiguienteHabil(String sFecha) throws NafinException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar cFechaSigHabil = new GregorianCalendar();
			cFechaSigHabil.setTime(Comunes.parseDate(sFecha));
			cFechaSigHabil.add(Calendar.DATE, 1);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
				cFechaSigHabil.add(Calendar.DATE, 2);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
				cFechaSigHabil.add(Calendar.DATE, 1);
			Vector vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
			boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
			cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			while(bInhabil) {
				vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil);
				bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
				if(!bInhabil)
					break;
				cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			}
			return sdf.format(cFechaSigHabil.getTime());
		}catch(Exception e) {
			System.out.println("Excepcion ocurrida en getFechaSiguienteHabil(sFecha): "+e.getMessage());
			e.printStackTrace();
			throw new NafinException("DISP0002");
		}
	}


	//	boolean tieneOperaciones() -- realizado por HDG -- 28/05/2003 --

	public boolean tieneOperaciones(String sNoIf, String sNoEPO, String sNoMoneda,
									String sFechaReg, String sTipo) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bOperaciones = false;
		try{
			con.conexionDB();
			String query =  "select A.totReg + B.totReg from "+
							"(select count(D.ic_folio) as totReg from "+(sTipo.equals("FISOS")?"com_dispersion D, ":"com_dispersion_epo D, ")+
							"com_cuentas C, comcat_moneda M "+
							"where D.ic_epo = "+sNoEPO+""
							+(sTipo.equals("FISOS")?" and D.ic_if = "+sNoIf:"")+
							" and D.ic_cuenta = C.ic_cuenta "+
							"and C.ic_moneda = M.ic_moneda "+
							"and C.ic_moneda = "+sNoMoneda+
							" and trunc(D.df_registro) = trunc(TO_DATE('"+sFechaReg+"','dd/mm/yyyy'))) A, "+
							"(select count(D.ic_folio) as totReg from "+(sTipo.equals("FISOS")?"com_dispersion D ":"com_dispersion_epo D ")+
							"where D.ic_epo = "+sNoEPO+""
							+(sTipo.equals("FISOS")?" and D.ic_if = "+sNoIf:"")+
							" and NOT exists (select 1 from com_cuentas c "+
							"				where D.ic_cuenta = c.ic_cuenta) "+
							"and trunc(D.df_registro) = trunc(TO_DATE('"+sFechaReg+"','dd/mm/yyyy'))) B";
			try {
				ResultSet rs = con.queryDB(query);
				if (rs.next())
					bOperaciones=(rs.getInt(1)>0)?true:false;
				con.cierraStatement();
			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DISP0003");
			}
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			System.out.println("Exception en tieneOperaciones(). "+e.getMessage());
		}
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB();	}
	return bOperaciones;
	}

	//	Hashtable cambiaEstatusDeOperaciones() -- realizado por HDG -- 29/05/2003 --

	public Hashtable cambiaEstatusDeOperaciones(String fechaReg, String sLineasDoctos,
												String sTipo, String sOrigen) throws NafinException {
	AccesoDB con = new AccesoDB();
	Hashtable hResumen = new Hashtable();
	try{
		con.conexionDB();
		int iNoLinea = 0, iRegBien = 0, iRegError = 0;
		int iNoElementos = (sTipo.equals("TEF"))?11:12;
		String sNomTabla = (sOrigen.equals("FISOS"))?"com_dispersion D":"com_dispersion_epo D";
		boolean bOk = true;
		ResultSet rs = null;
		String sLinea = "", sMsgError = "", sQuery = "", sRastreo = "";
		String sFechaReg = "", sFechaTrans = "", sFechaAplic = "", sImporte = "", sRFC = "", sBancoRep = "", sTipoCta = "", sNoCta = "", sRefRas = "", sLeyRas = "", sMotivo = "";
		StringBuffer sbError = new StringBuffer();
		VectorTokenizer vtValores=null; Vector vDatos=null;	StringTokenizer st = null;
		StringTokenizer stOperac = new StringTokenizer(sLineasDoctos,"\n");
		while(stOperac.hasMoreElements()) {
			sLinea = stOperac.nextToken().trim();
			iNoLinea++;
 			sMsgError = "Error en la linea: "+ iNoLinea + ", ";
  			bOk = true;
  			if(sLinea.equals(""))	//si la linea es vacia nos brincamos a la siguente iteraci�n
  				continue;
			st = new StringTokenizer(sLinea,";");
			if(st.countTokens() != iNoElementos) {
 				bOk = false;
 				sbError.append(sMsgError +" el layout es incorrecto revise sus separadores, deben ser todos por (;) su delimitador.\n");
			} else {
	  			vtValores = new VectorTokenizer(sLinea,";");
	  			vDatos = vtValores.getValuesVector();
	 			if (vDatos.size()!=iNoElementos) {
	 				bOk = false;
	 				sbError.append(sMsgError +" no coincide con el layout. Por favor verifiquelo.\n");
				} else {  //Obtiene informaci�n
					if(sTipo.equals("TEF")) {
		 				sFechaReg = (vDatos.size()>=1)?vDatos.get(0).toString().replace('-','/').trim():"";
						sFechaTrans = (vDatos.size()>=2)?vDatos.get(1).toString().replace('-','/').trim():"";
						sFechaAplic = (vDatos.size()>=3)?vDatos.get(2).toString().replace('-','/').trim():"";
						sImporte = (vDatos.size()>=4)?vDatos.get(3).toString().trim():"";
						sRFC = (vDatos.size()>=5)?vDatos.get(4).toString().trim():"";
						sBancoRep = (vDatos.size()>=6)?vDatos.get(5).toString().trim():"";
						sTipoCta = (vDatos.size()>=7)?vDatos.get(6).toString().trim():"";
						sNoCta = (vDatos.size()>=8)?vDatos.get(7).toString().trim():"";
						sRefRas = (vDatos.size()>=9)?vDatos.get(8).toString().trim():"";
						sLeyRas = (vDatos.size()>=10)?vDatos.get(9).toString().trim():"";
						sMotivo = (vDatos.size()>=11)?vDatos.get(10).toString().trim():"";
					} else if(sTipo.equals("CECOBAN")) {
						sRastreo = (vDatos.size()>=1)?vDatos.get(0).toString().trim():"";
		 				sFechaReg = (vDatos.size()>=2)?vDatos.get(1).toString().replace('-','/').trim():"";
						sFechaTrans = (vDatos.size()>=3)?vDatos.get(2).toString().replace('-','/').trim():"";
						sFechaAplic = (vDatos.size()>=4)?vDatos.get(3).toString().replace('-','/').trim():"";
						sImporte = (vDatos.size()>=5)?vDatos.get(4).toString().trim():"";
						sRFC = (vDatos.size()>=6)?vDatos.get(5).toString().trim():"";
						sBancoRep = (vDatos.size()>=7)?vDatos.get(6).toString().trim():"";
						sTipoCta = (vDatos.size()>=8)?vDatos.get(7).toString().trim():"";
						sNoCta = (vDatos.size()>=9)?vDatos.get(8).toString().trim():"";
						sRefRas = (vDatos.size()>=10)?vDatos.get(9).toString().trim():"";
						sLeyRas = (vDatos.size()>=11)?vDatos.get(10).toString().trim():"";
						sMotivo = (vDatos.size()>=12)?vDatos.get(11).toString().trim():"";
					}

					if(sTipo.equals("CECOBAN")) {
						//Validaci�n para el N�mero de Rastreo.
						if(sRastreo.length() > 7) {
							bOk = false;
							sbError.append(sMsgError +" el N�mero de Rastreo excede la longitud permitida.\n");
						} else if(!Comunes.esNumero(sRastreo)) {
							bOk = false;
							sbError.append(sMsgError +" el N�mero de Rastreo no es un valor num�rico.\n");
						}
					}
					//Validaci�n de la Fecha de Registro con formato DD-MM-YYYY.
					if (sFechaReg.equals("")) {
						bOk = false;
						sbError.append(sMsgError+", la Fecha del Registro viene vac�a.\n");
					} else if(sFechaReg.length()>10) {
						bOk = false;
						sbError.append(sMsgError +" la Fecha de Registro excede la longitud permitida.\n");
					} else if(!Comunes.checaFecha(sFechaReg)) {
						bOk = false;
						sbError.append(sMsgError +" la Fecha de Registro es incorrecta.\n");
					} else if(!sFechaReg.equals(fechaReg)) {
						bOk = false;
						sbError.append(sMsgError +" la Fecha de Registro del archivo no es la misma a la proporcionada.\n");
					} else {
						sQuery= "select count(*) from "+sNomTabla+
								" where TO_CHAR(df_registro,'dd/mm/yyyy') = '"+sFechaReg+"'";
						try {
				   			rs = con.queryDB(sQuery);
							rs.next();
					    	if(rs.getInt(1)==0) {
					      		bOk = false;
				  				sbError.append(sMsgError +" no existe el registro de la Operaci�n.\n");
			  				}
						} catch(SQLException sqle) {
							bOk = false;
							sqle.printStackTrace();
							throw new NafinException("DISP0004");
						}
					}
					//Validaci�n de la Fecha de Tranferencia con formato DD-MM-YYYY.
					if (sFechaTrans.equals("")) {
						bOk = false;
						sbError.append(sMsgError+", la Fecha de Tranferencia viene vac�a.\n");
					} else if(sFechaTrans.length()>10) {
						bOk = false;
						sbError.append(sMsgError +" la Fecha de Tranferencia excede la longitud permitida.\n");
					} else if (!Comunes.checaFecha(sFechaTrans)) {
						bOk = false;
						sbError.append(sMsgError +" la Fecha de Tranferencia es incorrecta.\n");
					}
					//Validaci�n de la Fecha de Aplicaci�n con formato DD-MM-YYYY.
					if (sFechaAplic.equals("")) {
						bOk = false;
						sbError.append(sMsgError+", la Fecha de Aplicaci�n viene vac�a.\n");
					} else if(sFechaAplic.length()>10) {
						bOk = false;
						sbError.append(sMsgError +" la Fecha de Aplicaci�n excede la longitud permitida.\n");
					} else if (!Comunes.checaFecha(sFechaAplic)) {
	 					bOk = false;
	 					sbError.append(sMsgError +" la Fecha de Aplicaci�n es incorrecto.\n");
					}
					//Validaci�n del Importe.
					if (sImporte.equals("")) {
						bOk = false;
						sbError.append(sMsgError+", el valor del Importe viene vac�o.\n");
					} else if(!Comunes.esDecimal(sImporte)) {
						bOk = false;
	 					sbError.append(sMsgError +" el importe de la operaci�n no es un valor num�rico correcto.\n");
					}
					//Validaci�n del RFC.
					String sFormatoRFC = "";
					if (sRFC.length()!=13 && sRFC.length()!=12) {
	 					bOk = false;
	 					sbError.append(sMsgError +" la longitud del formato del RFC es incorrecto.\n\n");
					} else {
						boolean bIniciales = true;
						String sIniciales = sRFC.substring(0,4).trim();
						for(int i=0; i<sIniciales.length(); i++) {
							//Character c = new Character(sIniciales.charAt(i));
							if(!Character.isLetter(sIniciales.charAt(i)) )
								bIniciales = false;
						}
						if(!bIniciales) {
							bOk = false;
							sbError.append(sMsgError +" las Iniciales del RFC son incorrectas \"Solo Letras\", 4 para Pna. F�sica y 3 para Pna. Moral.\n");
						}

						boolean bFecha = true;
						String sFecha = sRFC.substring(4,10);
						for(int i=0; i<sFecha.length(); i++) {
							//Character c = new Character(sFecha.charAt(i));
							if(!Character.isDigit(sFecha.charAt(i)) )
								bFecha = false;
						}
						if(!bFecha) {
							bOk = false;
							sbError.append(sMsgError +" la Fecha del RFC no es valida \"Solo Valores N�meros\".\n");
						} else if(sFecha.length()==6) {
							int ano = Integer.parseInt(sFecha.substring(0,2));
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
							int finAnoActual = Integer.parseInt(sdf.format(new java.util.Date()).substring(2,4));
							int Ano = (ano>finAnoActual && ano<99)?Integer.parseInt("19"+sFecha.substring(0,2)):Integer.parseInt("20"+sFecha.substring(0,2));
							int Mes = Integer.parseInt(sFecha.substring(2,4));
							int Dia = Integer.parseInt(sFecha.substring(4,6));
							if(!Comunes.checaFecha(Dia+"/"+Mes+"/"+Ano)) {
								bOk = false;
								sbError.append(sMsgError +" la fecha es inexistente, por favor revisela.");
							}
						}

						boolean bHomo = true;
						String sHomoclave = sRFC.substring(10,13);
						for(int i=0; i<sHomoclave.length(); i++) {
							//Character c = new Character(sHomoclave.charAt(i));
							if(!Character.isLetterOrDigit(sHomoclave.charAt(i)) )
								bHomo = false;
						}
						if(!bHomo) {
							bOk = false;
							sbError.append(sMsgError + " la Homoclave del RFC no es valida.\n");
						}
						sFormatoRFC = sIniciales+"-"+sFecha+"-"+sHomoclave;
					} // else RFC
					//Validaci�n del N�mero de Banco.
					if(sBancoRep.equals("")) {
						bOk = false;
						sbError.append(sMsgError + " el Banco Receptor viene vac�o.\n");
					} else if(sBancoRep.length()>3) {
						bOk = false;
						sbError.append(sMsgError + " el Banco Receptor excede la longitud permitida.\n");
					} else if(!Comunes.esNumero(sBancoRep)) {
						bOk = false;
						sbError.append(sMsgError + " el Banco Receptor no es un valor num�rico.\n");
					}
					//Validaci�n del Tipo de Cuenta del Receptor.
					if(!"40".equals(sTipoCta) && !"0".equals(sTipoCta)) {
						bOk = false;
						sbError.append(sMsgError + " el tipo de cuenta no es V�lido.\n");
					}
					//Validaci�n del N�mero Cuenta Receptor.
					if(sNoCta.equals("")) {
						bOk = false;
						sbError.append(sMsgError + " el N�mero de Cuenta viene vac�o.\n");
					} else if(sNoCta.length()>20) {
						bOk = false;
						sbError.append(sMsgError + " el N�mero de Cuenta no es v�lido.\n");
					}
					//Validaci�n de la Referencia Rastreo.
					if(!sRefRas.equals("TEF-NE")) {
						bOk = false;
						sbError.append(sMsgError + " la Referencia de Rastreo no es v�lida.\n");
					}
					//Validaci�n de la Leyenda Rastreo.
					if(sLeyRas.equals("")) {
						bOk = false;
						sbError.append(sMsgError + " la Leyenda de Rastreo viene vacia.\n");
					} else if (sLeyRas.length()>40) {
						if((!sLeyRas.startsWith("FISO AAA PH DP") && !sLeyRas.startsWith("DISPEPO")) || !sLeyRas.endsWith(Comunes.strtr(sFechaReg,"/"," ").trim()) ) {
		 					bOk = false;
		 					sbError.append(sMsgError +" la Leyenda de Rastreo es incorrecta.\n");
						}
					}
					//validar que el Estatus sea correcto
					if(sMotivo.equals("")) {
						bOk = false;
						sbError.append(sMsgError +" el Estatus viene vac�o.\n");
					} else if(sMotivo.length() > 2){
						bOk = false;
						sbError.append(sMsgError +" el Estatus excede la longitud permitida.\n");
					} else if(!Comunes.esNumero(sMotivo)) {
						bOk = false;
						sbError.append(sMsgError +" el Estatus debe ser num�rico.\n");
	 				} else {
						if(sTipo.equals("TEF"))
							sQuery = "SELECT ic_estatus_tef FROM comcat_estatus_tef WHERE ic_estatus_tef = " + sMotivo;
						else if(sTipo.equals("CECOBAN"))
							sQuery = "SELECT ic_estatus_cecoban_op FROM comcat_estatus_cecoban_op WHERE ic_estatus_cecoban_op = " + sMotivo;
						try {
			   				rs = con.queryDB(sQuery);
				      		if(!rs.next()) {
				      			bOk = false;
			  					sbError.append(sMsgError +" no existe el Estatus: "+sMotivo+".\n");
			  				}
						} catch(SQLException sqle) {
							bOk = false;
							sqle.printStackTrace();
							if(sTipo.equals("TEF"))
								throw new NafinException("DISP0005");
							else if(sTipo.equals("CECOBAN"))
								throw new NafinException("DISP0007");
						}
				  	 }

					// Revisi�n de la existencia de la operaci�n en dispersi�n.
					if(bOk) {
						sQuery = "select count(D.ic_folio), D.ic_pyme from "+sNomTabla+", comcat_pyme P "+
								((!sBancoRep.equals("0") && !sTipoCta.equals("0") && !sNoCta.equals("0"))?", com_cuentas C ":"")+
								" where TO_CHAR(D.df_registro,'dd/mm/yyyy') = '"+sFechaReg+"'"+
								" and TO_CHAR(D.df_transferencia,'dd/mm/yyyy') = '"+sFechaTrans+"'"+
								" and TO_CHAR(D.df_aplicacion,'dd/mm/yyyy') = '"+sFechaAplic+"'"+
								" and D.fn_importe = "+sImporte+
								" and D.ic_pyme = P.ic_pyme "+
								" and P.cg_rfc = '"+sFormatoRFC+"'"+
								" and D.cg_referencia_rastreo = '"+sRefRas+"'"+
								" and D.cg_leyenda_rastreo = '"+sLeyRas+"'";
						if(!sBancoRep.equals("0") && !sTipoCta.equals("0") && !sNoCta.equals("0")) {
							sQuery+=" and D.ic_cuenta = C.ic_cuenta "+
									" and C.ic_bancos_tef = "+sBancoRep+
									" and C.ic_tipo_cuenta = "+sTipoCta+
									" and C.cg_cuenta = '"+sNoCta+"'";
						}
						sQuery += " group by D.ic_pyme";
						//System.out.println(sQuery);
						try {
			   				rs = con.queryDB(sQuery);
				      		rs.next();
							if(rs.getInt(1) == 1) {
				      			sQuery = "update "+sNomTabla;
								sQuery+=(sTipo.equals("TEF"))?" set ic_estatus_tef = "+sMotivo:" set ic_estatus_cecoban_op = "+sMotivo;
								sQuery+=" where TO_CHAR(df_registro,'dd/mm/yyyy') = '"+sFechaReg+"'";
								sQuery+=" and ic_pyme = "+rs.getString(2).trim();
								try {
									con.ejecutaSQL(sQuery);
								} catch(SQLException sqle) {
									bOk = false;
									sqle.printStackTrace();
									if(sTipo.equals("TEF"))
										throw new NafinException("DISP0006");
									else if(sTipo.equals("CECOBAN"))
										throw new NafinException("DISP0008");
								}
								if(bOk && sTipo.equals("CECOBAN") && sMotivo.equals("1")) {
									sQuery = "update "+sNomTabla+" set ic_estatus_tef = null "+
											" where TO_CHAR(df_registro,'dd/mm/yyyy') = '"+sFechaReg+"'";
									try {
										con.ejecutaSQL(sQuery);
									} catch(SQLException sqle) {
										bOk = false;
										sqle.printStackTrace();
										throw new NafinException("DISP0008");
									}
								}
			  				} else if(rs.getInt(1)==0) {
								bOk = false;
	 							sbError.append(sMsgError +" no existe el registro de la Operaci�n.\n");
							}
						} catch(SQLException sqle) {
							bOk = false;
							sqle.printStackTrace();
							throw new NafinException("DISP0004");
						} finally {
							con.terminaTransaccion(bOk);
						}
					} // bOk.
				} // else
			} // else st.countTokens()
			if(bOk)	iRegBien++;
			else iRegError++;
		} // while
		// Resumen del Procesamiento.
		hResumen.put("RegLeidos", new Integer(iNoLinea));
		hResumen.put("RegProcesados", new Integer(iRegBien));
		hResumen.put("RegConError", new Integer(iRegError));
		hResumen.put("DescErrores", sbError.toString());

	} catch(NafinException ne) {
		throw ne;
	} catch(Exception e) {
		e.printStackTrace();
		System.out.println("Exception en cambiaEstatusDeOperaciones(). "+e.getMessage());
		throw new NafinException("SIST0001");
	} finally {	if(con.hayConexionAbierta()) con.cierraConexionDB(); }
	return hResumen;
	} // m�todo

	//	boolean esUsuarioCorrecto() -- realizado por HDG -- 20/08/2003 --

	public boolean esUsuarioCorrecto(String sLoginSesion, String sLoginEntrada,
									String sPassEntrada) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean bEsElUsuario = true;
		try{
			con.conexionDB();
			try {

				UtilUsr utils = new UtilUsr();
				boolean usaurioOK = utils.esUsuarioValido(sLoginEntrada, sPassEntrada);
				if(!usaurioOK)
					throw new NafinException("DSCT0009");
			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DISP0009");
			}
/*
			String sPassBase = "";
			try {
				ps = con.queryPrecompilado("select desencripta_pwd('"+sLoginSesion+"') from dual");
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					sPassBase = rs.getString(1);
					bEsElUsuario = (sLoginSesion.equals(setCerosLogPass(sLoginEntrada)) && sPassBase.equals(setCerosLogPass(sPassEntrada)) )?true:false;
				} else
					throw new NafinException("DISP0009");

				ps.clearParameters();
				if(ps != null) ps.close();
			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DISP0009");
			}
*/
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			System.out.println("Exception en revisaUsuario(). "+e.getMessage());
		}
		finally { if(con.hayConexionAbierta()) con.cierraConexionDB();	}
	return bEsElUsuario;
	}

	public boolean existenOperaciones(String sNoEPO, String sNoMoneda, String sFechaVenc,
									String sNoIF, String sNoEstatus, String sFechaOperacion )
		throws NafinException {
			return existenOperaciones( sNoEPO, sNoMoneda, sFechaVenc, sNoIF, sNoEstatus, sFechaOperacion, "");
	}

	public boolean existenOperaciones(String sNoEPO, String sNoMoneda, String sFechaVenc,
									String sNoIF, String sNoEstatus, String sFechaOperacion, String sNoIFFondeo) throws NafinException {
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	String query = "";
	boolean bExistenOperaciones = false;
		try {
			con.conexionDB();
			try {
				if(!"PA".equals(sNoEstatus)) {
					query =
						"select count(1) from int_flujo_fondos "+
						"where ic_epo = "+sNoEPO+
						" and ic_moneda = "+sNoMoneda+
						(sNoIF.equals("")?"":" and (ic_if = "+sNoIF+" and df_fecha_venc is null) ")+
						(sNoIFFondeo.equals("")?"":" and (ic_if_fondeo = "+sNoIFFondeo+" and df_fecha_venc is null) ")+
						(sFechaVenc.equals("")?"":" and trunc(df_fecha_venc) = TO_DATE('"+sFechaVenc+"','dd/mm/yyyy') ")+
						(sNoEstatus.equals("")?"":" and ic_estatus_docto = "+sNoEstatus)+
						(sFechaOperacion.equals("")?"":" and trunc(df_operacion) = TO_DATE('"+sFechaOperacion+"','dd/mm/yyyy') ")+
						"and trunc(df_registro) = trunc(SYSDATE) ";
				} else {
					query =
						"select count(1) from int_flujo_fondos "+
						"where ic_epo = "+sNoEPO+
						" and ic_moneda = "+sNoMoneda+
						" and df_fecha_venc is null "+
						(sFechaOperacion.equals("")?"":" and trunc(df_operacion) = TO_DATE('"+sFechaOperacion+"','dd/mm/yyyy') ")+
						"and trunc(df_registro) = trunc(SYSDATE) ";
				}
//System.out.println("\n query: "+query);
				ps = con.queryPrecompilado(query);

				ResultSet rs = ps.executeQuery();
				if(rs.next())
					bExistenOperaciones = (rs.getInt(1)>0)?true:false;
				ps.clearParameters();
				if(ps != null) ps.close();
			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DISP0011");
			}
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en existenOperaciones(). "+e.getMessage());
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return bExistenOperaciones;
	}

	private String getRazonSocial(String sOrigen, String sNoEPO, String sNoIF, AccesoDB con) throws NafinException {
		System.out.println("DispersionEJB::getRazonSocial (E)");
		String				query = "";
		String				condicion = "";
		String 				sRazonSocial = "";
		PreparedStatement 	ps = null;
		ResultSet 			rs = null;
		try {
			if("EPOS".equals(sOrigen)||"INFONAVIT".equals(sOrigen)||"PEMEX".equals(sOrigen))
				condicion = " comcat_epo where ic_epo = ?";
			else
				condicion = " comcat_if where ic_if = ? ";

			query = "select cg_razon_social from "+condicion;
			ps = con.queryPrecompilado(query);
			if("EPOS".equals(sOrigen)||"INFONAVIT".equals(sOrigen)||"PEMEX".equals(sOrigen))
				ps.setInt(1, Integer.parseInt(sNoEPO));
			else
				ps.setInt(1, Integer.parseInt(sNoIF));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next())
				sRazonSocial = rs.getString(1);
			rs.close();ps.close();
		} catch(Exception e) {
			System.out.println("DispersionEJB::getRazonSocial (Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("DispersionEJB::getRazonSocial (S)");
		}
		return sRazonSocial;
	}//getRazonSocial

	public String getQueryDispINFONAVIT(String ic_folio_ff, String sNoEPO, String sNoMoneda, String sNoEstatus, String sFechaOperacion) throws NafinException {
		System.out.println("DispersionEJB::getQueryDispINFONAVIT (E)");
		String query = "";
		String sFechaSigHab = getFechaSiguienteHabil().replace('/','-');
		try {
			query =
				" SELECT   /*+index(d IN_COM_DOCUMENTO_02_NUK) use_nl(d ds s n p i c m)*/"   +
				"           '"+ic_folio_ff+"' AS ic_flujo_fondos,"   +
				"           SUM (ds.in_importe_recibir"   +
				"              - NVL (ds.fn_importe_recibir_benef, 0)) AS fn_importe,"   +
				"           COUNT (ds.ic_documento) AS ig_total_documentos,"   +
				"           d.ic_pyme AS desarrollador, TO_NUMBER (NULL) AS beneficiario,"   +
				"           p.cg_rfc, p.cg_razon_social, m.cd_nombre, c.ic_cuenta,"   +
				"           c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, c.ic_estatus_tef,"   +
				"           c.ic_estatus_cecoban,"   +
				"           TO_CHAR (s.df_operacion, 'dd/mm/yyyy') AS df_operacion,"   +
				"           DECODE (c.ic_estatus_cecoban, 99, 0, -1) AS estatusoper,"   +
				"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
				"           '"+sFechaSigHab+"' AS fechatrasf, '"+sFechaSigHab+"' AS fechaapli,"   +
				"           'TEF-NE' AS refrastreo,"   +
				"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo,"   +
				"           'DispersionEJB::getQueryDispINFONAVIT'"   +
				"     FROM com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          com_solicitud s,"   +
				"          comrel_nafin n,"   +
				"          comcat_pyme p,"   +
				"          comcat_if i,"   +
				"          com_cuentas c,"   +
				"          comcat_moneda m"   +
				"    WHERE ds.ic_documento = d.ic_documento"   +
				"      AND ds.ic_documento = s.ic_documento"   +
				"      AND d.ic_pyme = p.ic_pyme"   +
				"      AND d.ic_epo = ds.ic_epo"   +
				"      AND ds.ic_if = i.ic_if"   +
				"      AND c.ic_moneda = m.ic_moneda"   +
				"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
				"      AND c.ic_epo = "+sNoEPO+
				"      AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"      AND m.ic_moneda = "+sNoMoneda+
				"      AND d.ic_moneda = m.ic_moneda"+
				"      AND ds.ic_epo = "+sNoEPO+
				"      AND n.cg_tipo = 'P'"   +
				"      AND d.ic_estatus_docto = "+sNoEstatus+
				"      AND c.ic_producto_nafin = 1"   +
				"      AND c.ic_tipo_cuenta = 40"   +
				"      AND i.ig_tipo_piso = 1"   +
				"      AND s.df_operacion >= TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy')"   +
				"      AND s.df_operacion < (TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy') + 1)"   +
				"      AND ( ("   +
				"                   d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
				"               AND ds.in_importe_recibir - ds.fn_importe_recibir_benef > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+") )"   +
				" GROUP BY c.ic_estatus_cecoban,"   +
				"          c.ic_cuenta,"   +
				"          c.ic_bancos_tef,"   +
				"          c.ic_tipo_cuenta,"   +
				"          c.cg_cuenta,"   +
				"          c.ic_estatus_tef,"   +
				"          c.ic_estatus_cecoban,"   +
				"          d.ic_pyme,"   +
				"          p.cg_rfc,"   +
				"          p.cg_razon_social,"   +
				"          m.cd_nombre,"   +
				"          TO_CHAR (s.df_operacion, 'dd/mm/yyyy')"   +
				" UNION ALL"   +
				" SELECT   /*+index(d IN_COM_DOCUMENTO_02_NUK) use_nl(d ds s n p i m)*/"   +
				"           '"+ic_folio_ff+"' AS ic_flujo_fondos,"   +
				"           SUM (ds.in_importe_recibir"   +
				"              - NVL (ds.fn_importe_recibir_benef, 0)) AS fn_importe,"   +
				"           COUNT (ds.ic_documento) AS ig_total_documentos,"   +
				"           d.ic_pyme AS desarrollador, TO_NUMBER (NULL) AS beneficiario,"   +
				"           p.cg_rfc, p.cg_razon_social, m.cd_nombre, 0 AS ic_cuenta,"   +
				"           0 AS ic_bancos_tef, 0 AS ic_tipo_cuenta, '0' AS cg_cuenta,"   +
				"           0 AS ic_estatus_tef, 0 AS ic_estatus_cecoban,"   +
				"           TO_CHAR (s.df_operacion, 'dd/mm/yyyy') AS df_operacion,"   +
				"           -1 AS estatusoper, TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
				"           '"+sFechaSigHab+"' AS fechatrasf, '"+sFechaSigHab+"' AS fechaapli,"   +
				"           'TEF-NE' AS refrastreo,"   +
				"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo,"   +
				"           'DispersionEJB::getQueryDispINFONAVIT'"   +
				"     FROM com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          com_solicitud s,"   +
				"          comrel_nafin n,"   +
				"          comcat_pyme p,"   +
				"          comcat_if i,"   +
				"          comcat_moneda m"   +
				"    WHERE d.ic_documento = ds.ic_documento"   +
				"      AND d.ic_pyme = n.ic_epo_pyme_if"   +
				"      AND d.ic_epo = ds.ic_epo"   +
				"      AND ds.ic_documento = s.ic_documento"   +
				"      AND d.ic_documento = s.ic_documento"   +
				"      AND ds.ic_if = i.ic_if"   +
				"      AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"      AND NOT EXISTS ("   +
				"             SELECT 1"   +
				"               FROM com_cuentas c"   +
				"              WHERE c.ic_moneda = m.ic_moneda"   +
				"                AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                AND c.ic_producto_nafin = 1"   +
				"                AND c.ic_tipo_cuenta = 40)"   +
				"      AND m.ic_moneda = "+sNoMoneda+
				"      AND d.ic_moneda = m.ic_moneda"+
				"      AND ds.ic_epo = "+sNoEPO+
				"      AND n.cg_tipo = 'P'"   +
				"      AND d.ic_estatus_docto = "+sNoEstatus+
				"      AND i.ig_tipo_piso = 1"   +
				"      AND s.df_operacion >= TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy')"   +
				"      AND s.df_operacion < (TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy') + 1)"   +
				"      AND ( ("   +
				"                   d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
				"               AND ds.in_importe_recibir - ds.fn_importe_recibir_benef > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+") )"   +
				" GROUP BY d.ic_pyme,"   +
				"          p.cg_rfc,"   +
				"          p.cg_razon_social,"   +
				"          m.cd_nombre,"   +
				"          TO_CHAR (s.df_operacion, 'dd/mm/yyyy')"   +
				" UNION ALL"   +
				" SELECT   /*+index(d IN_COM_DOCUMENTO_02_NUK) index(ds) index(c) use_nl(d ds s n c i2 c m i)*/"   +
				"           '"+ic_folio_ff+"' AS ic_flujo_fondos,"   +
				"           SUM (ds.fn_importe_recibir_benef) AS fn_importe,"   +
				"           COUNT (ds.ic_documento) AS ig_total_documentos,"   +
				"           TO_NUMBER (NULL) AS desarrollador, d.ic_beneficiario AS beneficiario,"   +
				"           i2.cg_rfc, i2.cg_razon_social, m.cd_nombre, c.ic_cuenta,"   +
				"           c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, c.ic_estatus_tef,"   +
				"           c.ic_estatus_cecoban,"   +
				"           TO_CHAR (s.df_operacion, 'dd/mm/yyyy') AS df_operacion,"   +
				"           DECODE (c.ic_estatus_cecoban, 99, 0, -1) AS estatusoper,"   +
				"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
				"           '"+sFechaSigHab+"' AS fechatrasf, '"+sFechaSigHab+"' AS fechaapli,"   +
				"           'TEF-NE' AS refrastreo,"   +
				"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo,"   +
				"           'DispersionEJB::getQueryDispINFONAVIT'"   +
				"     FROM com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          com_solicitud s,"   +
				"          comrel_nafin n,"   +
				"          comcat_if i2,"   +
				"          com_cuentas c,"   +
				"          comcat_moneda m,"   +
				"          comcat_if i"   +
				"    WHERE ds.ic_documento = d.ic_documento"   +
				"      AND ds.ic_documento = s.ic_documento"   +
				"      AND d.ic_beneficiario = i2.ic_if"   +
				"      AND d.ic_epo = ds.ic_epo"   +
				"      AND ds.ic_if = i.ic_if"   +
				"      AND c.ic_moneda = m.ic_moneda"   +
				"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
				"      AND n.ic_epo_pyme_if = i2.ic_if"   +
				"      AND m.ic_moneda = "+sNoMoneda+
				"      AND d.ic_moneda = m.ic_moneda"+
				"      AND ds.ic_epo = "+sNoEPO+
				"      AND n.cg_tipo = 'I'"   +
				"      AND d.ic_estatus_docto = "+sNoEstatus+
				"      AND c.ic_producto_nafin = 1"   +
				"      AND c.ic_tipo_cuenta = 40"   +
				"      AND c.ic_epo = "+sNoEPO+" "+
				"      AND c.cg_tipo_afiliado = 'B'"   +
				"      AND i.ig_tipo_piso = 1"   +
				"      AND s.df_operacion >= TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy')"   +
				"      AND s.df_operacion < (TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy') + 1)"   +
				"      AND d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
				"      AND ds.fn_importe_recibir_benef > 0"   +
				" GROUP BY c.ic_estatus_cecoban,"   +
				"          c.ic_cuenta,"   +
				"          c.ic_bancos_tef,"   +
				"          c.ic_tipo_cuenta,"   +
				"          c.cg_cuenta,"   +
				"          c.ic_estatus_tef,"   +
				"          c.ic_estatus_cecoban,"   +
				"          d.ic_beneficiario,"   +
				"          i2.cg_rfc,"   +
				"          i2.cg_razon_social,"   +
				"          m.cd_nombre,"   +
				"          TO_CHAR (s.df_operacion, 'dd/mm/yyyy')"   +
				" UNION ALL"   +
				" SELECT   /*+index(d IN_COM_DOCUMENTO_02_NUK) index(ds) use_nl(d ds s n i2 m i)*/"   +
				"           '"+ic_folio_ff+"' AS ic_flujo_fondos,"   +
				"           SUM (ds.fn_importe_recibir_benef) AS fn_importe,"   +
				"           COUNT (ds.ic_documento) AS ig_total_documentos,"   +
				"           TO_NUMBER (NULL) AS desarrollador, d.ic_beneficiario AS beneficiario,"   +
				"           i2.cg_rfc, i2.cg_razon_social, m.cd_nombre, 0 ic_cuenta,"   +
				"           0 AS ic_bancos_tef, 0 AS ic_tipo_cuenta, '0' AS cg_cuenta,"   +
				"           0 AS ic_estatus_tef, 0 AS ic_estatus_cecoban,"   +
				"           TO_CHAR (s.df_operacion, 'dd/mm/yyyy') AS df_operacion,"   +
				"           -1 AS estatusoper, TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
				"           '"+sFechaSigHab+"' AS fechatrasf, '"+sFechaSigHab+"' AS fechaapli,"   +
				"           'TEF-NE' AS refrastreo,"   +
				"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo,"   +
				"           'DispersionEJB::getQueryDispINFONAVIT'"   +
				"     FROM com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          com_solicitud s,"   +
				"          comrel_nafin n,"   +
				"          comcat_if i2,"   +
				"          comcat_moneda m,"   +
				"          comcat_if i"   +
				"    WHERE d.ic_documento = ds.ic_documento"   +
				"      AND d.ic_beneficiario = i2.ic_if"   +
				"      AND d.ic_epo = ds.ic_epo"   +
				"      AND ds.ic_documento = s.ic_documento"   +
				"      AND ds.ic_if = i.ic_if"   +
				"      AND n.ic_epo_pyme_if = i2.ic_if"   +
				"      AND NOT EXISTS ("   +
				"             SELECT 1"   +
				"               FROM com_cuentas c"   +
				"              WHERE c.ic_moneda = m.ic_moneda"   +
				"                AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                AND c.cg_tipo_afiliado = 'B'"   +
				"                AND c.ic_producto_nafin = 1"   +
				"                AND c.ic_tipo_cuenta = 40)"   +
				"      AND m.ic_moneda = "+sNoMoneda+
				"      AND d.ic_moneda = m.ic_moneda"+
				"      AND ds.ic_epo = "+sNoEPO+
				"      AND n.cg_tipo = 'I'"   +
				"      AND d.ic_estatus_docto = "+sNoEstatus+
				"      AND i.ig_tipo_piso = 1"   +
				"      AND s.df_operacion >= TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy')"   +
				"      AND s.df_operacion < (TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy') + 1)"   +
				"      AND d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
				"      AND ds.fn_importe_recibir_benef > 0"   +
				" GROUP BY d.ic_beneficiario,"   +
				"          i2.cg_rfc,"   +
				"          i2.cg_razon_social,"   +
				"          m.cd_nombre,"   +
				"          TO_CHAR (s.df_operacion, 'dd/mm/yyyy')"  ;
		} catch(Exception e) {
			System.out.println("DispersionEJB::getQueryDispINFONAVIT (Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("DispersionEJB::getQueryDispINFONAVIT (S)");
		}
		return query;
	}//getQueryDispINFONAVIT


	public String getQueryDispEPOS(String ic_folio_ff, String sNoEPO, String sNoMoneda, String sNoEstatus, String sFechaVenc) throws NafinException {
		System.out.println("DispersionEJB::getQueryDispEPOS (E)");
		String query = "";
		String sFechaSigHab = getFechaSiguienteHabil().replace('/','-');
		try {
			ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

			String sFechaVencPyme = "";
			if("9".equals(sNoEstatus)) {
				sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(sNoEPO);
			}

			String claveTipoCuenta 	= (sNoMoneda != null && sNoMoneda.equals("54"))?claveCuentaSwift:"40";// Cuenta Swift: // Codigo cuenta cliente

			if(sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1")) {

				String importeDisp = "d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)";
				String numDocDisp = "COUNT (d.ic_documento)";
				String condicionDsctoEspecial = dispersionTradicional;

				if("9".equals(sNoEstatus)||sNoEstatus.equals("10")) {
					importeDisp = "DECODE (d.cs_dscto_especial, 'C', -1, 1) * ("+importeDisp+")";
					condicionDsctoEspecial += ", 'C'";
					numDocDisp = "SUM (DECODE (d.cs_dscto_especial, 'C', 0, 1))";
				}

				query =
					" SELECT   /*+index(d) use_nl(d n p c m)*/"   +
					"           '"+ic_folio_ff+"' AS ic_flujo_fondos, p.cg_rfc, p.cg_razon_social,"   +
					"           SUM ("+importeDisp+") fn_importe,"   +
					"           "+numDocDisp+" ig_total_documentos,"   +
					"           d.ic_pyme AS desarrollador, TO_NUMBER (NULL) AS beneficiario,"   +
					"           c.ic_cuenta, TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc,"   +
					"           c.ic_estatus_tef, c.ic_estatus_cecoban, m.cd_nombre, c.ic_bancos_tef,"   +
					"           c.ic_tipo_cuenta, c.cg_cuenta,"   +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
					"           '"+sFechaSigHab+"' AS fechatrasf, "   +
					"           '"+sFechaSigHab+"' AS fechaapli,"   +
					"           'TEF-NE' AS refrastreo,"   +
					"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo,"   +
					"           DECODE (c.ic_estatus_cecoban, 99, 0, -1) AS estatusoper,"   +
					"           'DispersionEJB::getQueryDispEPOS'"   +
					"     FROM com_documento d,"   +
					"          comrel_nafin n,"   +
					"          comcat_pyme p,"   +
					"          com_cuentas c,"   +
					"          comcat_moneda m"   +
					"    WHERE d.ic_pyme = p.ic_pyme"   +
					"      AND c.ic_moneda = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND c.ic_epo = "+sNoEPO+
					"      AND n.ic_epo_pyme_if = p.ic_pyme"   +
					"      AND n.cg_tipo = 'P'"   +
					"      AND d.ic_epo = "+sNoEPO+
					"      AND m.ic_moneda = "+sNoMoneda+
					"      AND d.ic_moneda = m.ic_moneda"+
					"      AND d.ic_estatus_docto = "+sNoEstatus+
					"      AND c.ic_producto_nafin = 1"   +
					"      AND c.ic_tipo_cuenta = "+ claveTipoCuenta +
					"      AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')"   +
					"      AND d.df_fecha_venc"+sFechaVencPyme+" < (TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy') + 1)"   +
					"      AND ( ( d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
					"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
					"           OR d.cs_dscto_especial IN ("+condicionDsctoEspecial+") )"   +
					" GROUP BY d.ic_pyme,"   +
					"          m.cd_nombre,"   +
					"          c.ic_cuenta,"   +
					"          TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy'),"   +
					"          p.cg_rfc,"   +
					"          p.cg_razon_social,"   +
					"          c.ic_estatus_tef,"   +
					"          c.ic_estatus_cecoban,"   +
					"          m.cd_nombre,"   +
					"          c.ic_bancos_tef,"   +
					"          c.ic_tipo_cuenta,"   +
					"          c.cg_cuenta"   +
					" UNION ALL"   +
					" SELECT   /*+index(d) use_nl(d n p m)*/"   +
					"           '"+ic_folio_ff+"' AS ic_flujo_fondos, p.cg_rfc, p.cg_razon_social,"   +
					"           SUM ("+importeDisp+") fn_importe,"   +
					"           "+numDocDisp+" ig_total_documentos,"   +
					"           d.ic_pyme AS desarrollador, TO_NUMBER (NULL) AS beneficiario, 0 AS ic_cuenta ,"   +
					"           TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc,"   +
					"           0 AS ic_estatus_tef,"   +
					"           0 AS ic_estatus_cecoban, m.cd_nombre,"   +
					"           0 AS ic_bancos_tef, 0 AS ic_tipo_cuenta, '0' AS cg_cuenta,"   +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
					"           '"+sFechaSigHab+"' AS fechatrasf, "   +
					"           '"+sFechaSigHab+"' AS fechaapli,"   +
					"           'TEF-NE' AS refrastreo,"   +
					"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo,"   +
					"           -1 AS estatusoper,"   +
					"           'DispersionEJB::getQueryDispEPOS'"   +
					"     FROM com_documento d, comrel_nafin n, comcat_pyme p, comcat_moneda m"   +
					"    WHERE d.ic_pyme = p.ic_pyme"   +
					"      AND n.ic_epo_pyme_if = p.ic_pyme"   +
					"      AND NOT EXISTS ("   +
					"             SELECT 1"   +
					"               FROM com_cuentas c"   +
					"              WHERE c.ic_moneda = m.ic_moneda"   +
					"                AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
					"					  AND c.ic_epo = d.ic_epo "   +
					"                AND c.ic_producto_nafin = 1"   +
					"                AND c.ic_tipo_cuenta = "+ claveTipoCuenta +")"   +
					"      AND n.cg_tipo = 'P'"   +
					"      AND d.ic_epo = "+sNoEPO+
					"			 AND d.ic_moneda = m.ic_moneda " +
					"      AND m.ic_moneda = "+sNoMoneda+
					"      AND d.ic_estatus_docto = "+sNoEstatus+
					"      AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')"   +
					"      AND d.df_fecha_venc"+sFechaVencPyme+" < (TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy') + 1)"   +
					"      AND ( ( d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
					"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
					"           OR d.cs_dscto_especial IN ("+condicionDsctoEspecial+") )"   +
					" GROUP BY d.ic_pyme,"   +
					"          m.cd_nombre,"   +
					"          TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy'),"   +
					"          p.cg_rfc,"   +
					"          p.cg_razon_social,"   +
					"          m.cd_nombre"   +
					" UNION ALL"   +
					" SELECT   /*+index(d) use_nl(d n i2 c m)*/"   +
					"           '"+ic_folio_ff+"' AS ic_flujo_fondos, i2.cg_rfc, i2.cg_razon_social,"   +
					"           SUM (( d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100 )) AS fn_importe,"   +
					"           COUNT (d.ic_documento) AS ig_total_documentos,"   +
					"           TO_NUMBER (NULL) AS desarrollador, d.ic_beneficiario AS beneficiario,"   +
					"           c.ic_cuenta, TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc,"   +
					"           c.ic_estatus_tef, c.ic_estatus_cecoban, m.cd_nombre, c.ic_bancos_tef,"   +
					"           c.ic_tipo_cuenta, c.cg_cuenta,"   +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
					"           '"+sFechaSigHab+"' AS fechatrasf, "   +
					"           '"+sFechaSigHab+"' AS fechaapli,"   +
					"           'TEF-NE' AS refrastreo,"   +
					"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo,"   +
					"           DECODE (c.ic_estatus_cecoban, 99, 0, -1) AS estatusoper,"   +
					"           'DispersionEJB::getQueryDispEPOS'"   +
					"     FROM com_documento d,"   +
					"          comrel_nafin n,"   +
					"          comcat_if i2,"   +
					"          com_cuentas c,"   +
					"          comcat_moneda m"   +
					"    WHERE d.ic_beneficiario = i2.ic_if"   +
					"      AND c.ic_moneda = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND n.ic_epo_pyme_if = i2.ic_if"   +
					"      AND n.cg_tipo = 'I'"   +
					"      AND d.ic_epo = "+sNoEPO+
					"      AND d.ic_moneda = "+sNoMoneda+
					"      AND m.ic_moneda = d.ic_moneda "+
					"      AND d.ic_estatus_docto = "+sNoEstatus+
					"      AND c.ic_producto_nafin = 1"   +
					"      AND c.ic_tipo_cuenta = "+ claveTipoCuenta +
					"      AND c.ic_epo = "+sNoEPO+
					"      AND c.cg_tipo_afiliado = 'B'"   +
					"      AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')"   +
					"      AND d.df_fecha_venc"+sFechaVencPyme+" < (TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy') + 1)"   +
					"      AND d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
					"      AND (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0"   +
					" GROUP BY d.ic_beneficiario,"   +
					"          m.cd_nombre,"   +
					"          c.ic_cuenta,"   +
					"          TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy'),"   +
					"          i2.cg_rfc,"   +
					"          i2.cg_razon_social,"   +
					"          c.ic_estatus_tef,"   +
					"          c.ic_estatus_cecoban,"   +
					"          m.cd_nombre,"   +
					"          c.ic_bancos_tef,"   +
					"          c.ic_tipo_cuenta,"   +
					"          c.cg_cuenta"   +
					" UNION ALL"   +
					" SELECT   /*+use_nl(d n i2 m)*/"   +
					"           '"+ic_folio_ff+"' AS ic_flujo_fondos, i2.cg_rfc, i2.cg_razon_social,"   +
					"           SUM (( d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100 )) AS fn_importe,"   +
					"           COUNT (d.ic_documento) AS ig_total_documentos,"   +
					"           TO_NUMBER (NULL) AS desarrollador, d.ic_beneficiario AS beneficiario,"   +
					"           0 AS ic_cuenta, TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc,"   +
					"           0 AS ic_estatus_tef,"   +
					"           0 AS ic_estatus_cecoban, m.cd_nombre,"   +
					"           0 AS ic_bancos_tef, 0 AS ic_tipo_cuenta, '0' AS cg_cuenta,"   +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
					"           '"+sFechaSigHab+"' AS fechatrasf, "   +
					"           '"+sFechaSigHab+"' AS fechaapli,"   +
					"           'TEF-NE' AS refrastreo,"   +
					"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo,"   +
					"           -1 AS estatusoper,"   +
					"           'DispersionEJB::getQueryDispEPOS'"   +
					"     FROM com_documento d, comrel_nafin n, comcat_if i2, comcat_moneda m"   +
					"    WHERE d.ic_beneficiario = i2.ic_if"   +
					"      AND n.ic_epo_pyme_if = i2.ic_if"   +
					"      AND NOT EXISTS ("   +
					"             SELECT 1"   +
					"               FROM com_cuentas c"   +
					"              WHERE c.ic_moneda = m.ic_moneda"   +
					"                AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
					"					  AND c.ic_epo = d.ic_epo "   +
					"                AND c.ic_producto_nafin = 1"   +
					"                AND c.ic_tipo_cuenta = "+ claveTipoCuenta +")"   +
					"      AND n.cg_tipo = 'I'"   +
					"      AND d.ic_epo = "+sNoEPO+
					"      AND d.ic_moneda = m.ic_moneda " +
					"      AND m.ic_moneda = "+sNoMoneda+
					"      AND d.ic_estatus_docto = "+sNoEstatus+
					"      AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')"   +
					"      AND d.df_fecha_venc"+sFechaVencPyme+" < (TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy') + 1)"   +
					"      AND d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
					"      AND (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0"   +
					" GROUP BY d.ic_beneficiario,"   +
					"          m.cd_nombre,"   +
					"          TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy'),"   +
					"          i2.cg_rfc,"   +
					"          i2.cg_razon_social,"   +
					"          m.cd_nombre"  ;
			} else if(sNoEstatus.equals("4") || sNoEstatus.equals("11")) {
				query =
					" SELECT   /*+index(c) use_nl(d n i c m)*/"   +
					"           '"+ic_folio_ff+"' AS ic_flujo_fondos, SUM (d.fn_monto) AS fn_importe,"   +
					"           COUNT (d.ic_documento) AS ig_total_documentos, NULL AS ic_pyme,"   +
					"           c.ic_cuenta, TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc,"   +
					"           i.ic_if, DECODE (c.ic_estatus_cecoban, 99, 0, -1) AS estatusoper,"   +
					"           i.cg_rfc, i.cg_razon_social, m.cd_nombre, c.ic_bancos_tef,"   +
					"           c.ic_tipo_cuenta, c.cg_cuenta, c.ic_estatus_tef, c.ic_estatus_cecoban,"   +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
					"           '"+sFechaSigHab+"' AS fechatrasf, "   +
					"           '"+sFechaSigHab+"' AS fechaapli,"   +
					"           'TEF-NE' AS refrastreo,"   +
					"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo,"   +
					"           'DispersionEJB::getQueryDispEPOS'"   +
					"     FROM com_documento d,"   +
					"          comrel_nafin n,"   +
					"          comcat_if i,"   +
					"          com_cuentas c,"   +
					"          comcat_moneda m"   +
					"    WHERE d.ic_if = i.ic_if"   +
					"      AND c.ic_moneda = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND n.ic_epo_pyme_if = i.ic_if"   +
					"      AND n.cg_tipo = 'I'"   +
					"      AND d.ic_epo = "+sNoEPO+
					"      AND m.ic_moneda = "+sNoMoneda+
					"      AND d.ic_moneda = m.ic_moneda"+
					"      AND d.ic_estatus_docto = "+sNoEstatus+
					"      AND c.ic_producto_nafin = 1"   +
					"      AND c.ic_tipo_cuenta = "+ claveTipoCuenta +
					"      AND c.ic_epo = "+sNoEPO+
					"      AND c.cg_tipo_afiliado = 'I'"   +
					"      AND d.df_fecha_venc >= TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')"   +
					"      AND d.df_fecha_venc < (TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy') + 1)"   +
					"      AND d.cs_dscto_especial != 'C' "   +
					" GROUP BY c.ic_cuenta,"   +
					"          TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy'),"   +
					"          i.ic_if,"   +
					"          DECODE (c.ic_estatus_cecoban, 99, 0, -1),"   +
					"          i.cg_rfc,"   +
					"          i.cg_razon_social,"   +
					"          m.cd_nombre,"   +
					"          c.ic_bancos_tef,"   +
					"          c.ic_tipo_cuenta,"   +
					"          c.cg_cuenta,"   +
					"          c.ic_estatus_tef,"   +
					"          c.ic_estatus_cecoban"  ;
			}
		} catch(Exception e) {
			System.out.println("DispersionEJB::getQueryDispEPOS (Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("DispersionEJB::getQueryDispEPOS (S)");
		}
		return query;
	}//getQueryDispEPOS

	public String getQueryDispFISOS(String ic_folio_ff, String sNoEPO, String sNoMoneda, String sNoIF) throws NafinException {
		System.out.println("DispersionEJB::getQueryDispFISOS (E)");
		String query = "";
		String sFechaSigHab = getFechaSiguienteHabil().replace('/','-');

		String claveTipoCuenta 	= (sNoMoneda != null && sNoMoneda.equals("54"))?claveCuentaSwift:"40";// Cuenta Swift: // Codigo cuenta cliente

		try {
			query =
				" SELECT   /*+use_nl(d ds s n p c m)*/"   +
				"           '"+ic_folio_ff+"' AS ic_flujo_fondos, SUM (ds.in_importe_recibir) AS fn_importe,"   +
				"           COUNT (ds.ic_documento) AS ig_total_documentos, m.cd_nombre,"   +
				"           d.ic_pyme, p.cg_rfc, p.cg_razon_social, c.ic_cuenta,"   +
				"           DECODE (c.ic_estatus_cecoban, 99, 0, -1) AS estatusoper,"   +
				"           c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, c.ic_estatus_tef,"   +
				"           c.ic_estatus_cecoban, TO_DATE(SYSDATE,'dd/mm/yyyy') as fechaReg, '"+sFechaSigHab+"' as fechaTrasf, '"+sFechaSigHab+"' as fechaApli, 'TEF-NE' as refRastreo, 'FISO AAA PH DP  '||TO_CHAR(sysdate,'dd mm yyyy') as leyendaRastreo"   +
				"           ,'DispersionEJB::getQueryDispFISOS'"   +
				"     FROM com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          com_solicitud s,"   +
				"          comrel_nafin n,"   +
				"          comcat_pyme p,"   +
				"          com_cuentas c,"   +
				"          comcat_moneda m"   +
				"    WHERE ds.ic_documento = d.ic_documento"   +
				"      AND ds.ic_documento = s.ic_documento"   +
				"      AND d.ic_pyme = p.ic_pyme"   +
				"      AND d.ic_epo = ds.ic_epo"   +
				"      AND c.ic_moneda = m.ic_moneda"   +
				"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
				"      AND c.ic_epo = d.ic_epo"   +
				"      AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"      AND m.ic_moneda = "+sNoMoneda+
				"      AND d.ic_moneda = m.ic_moneda"+
				"      AND ds.ic_epo = "+sNoEPO+
				"      AND n.cg_tipo = 'P'"   +
				"      AND ds.ic_if = "+sNoIF+
				"      AND d.ic_estatus_docto = 4"   +
				"      AND c.ic_producto_nafin = 1"   +
				"      AND c.ic_tipo_cuenta = " + claveTipoCuenta +
				"      AND s.df_operacion >= TRUNC (SYSDATE)"   +
				"      AND s.df_operacion < (TRUNC (SYSDATE) + 1)"   +
				"      AND d.cs_dscto_especial != 'C' "   +
				"      AND ds.cs_opera_fiso = 'N' "  + // Documentos registrados bajo la modalidad normal (sin Fideicomiso para Desarrollo de Proveedores).
				" GROUP BY c.ic_estatus_cecoban,"   +
				"          c.ic_cuenta,"   +
				"          d.ic_pyme,"   +
				"          p.cg_rfc,"   +
				"          p.cg_razon_social,"   +
				"          m.cd_nombre,"   +
				"          c.ic_bancos_tef,"   +
				"          c.ic_tipo_cuenta,"   +
				"          c.cg_cuenta,"   +
				"          c.ic_estatus_tef,"   +
				"          c.ic_estatus_cecoban"   +
				" UNION ALL"   +
				" SELECT   /*+use_nl(d ds s n p m)*/"   +
				"           '"+ic_folio_ff+"' AS ic_flujo_fondos, SUM (ds.in_importe_recibir) AS fn_importe,"   +
				"           COUNT (ds.ic_documento) AS ig_total_documentos, m.cd_nombre,"   +
				"           d.ic_pyme, p.cg_rfc, p.cg_razon_social, 0, -1 AS estatusoper,"   +
				"           0 AS ic_bancos_tef, 0 AS ic_tipo_cuenta, '0' AS cg_cuenta,"   +
				"           0 AS ic_estatus_tef, 0 AS ic_estatus_cecoban, TO_DATE(SYSDATE,'dd/mm/yyyy') as fechaReg, '"+sFechaSigHab+"' as fechaTrasf, '"+sFechaSigHab+"' as fechaApli, 'TEF-NE' as refRastreo, 'FISO AAA PH DP  '||TO_CHAR(sysdate,'dd mm yyyy') as leyendaRastreo"   +
				"           ,'DispersionEJB::getQueryDispFISOS'"   +
				"     FROM com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          com_solicitud s,"   +
				"          comrel_nafin n,"   +
				"          comcat_pyme p,"   +
				"          comcat_moneda m"   +
				"    WHERE d.ic_documento = ds.ic_documento"   +
				"      AND d.ic_pyme = p.ic_pyme"   +
				"      AND d.ic_epo = ds.ic_epo"   +
				"      AND ds.ic_documento = s.ic_documento"   +
				"      AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"      AND NOT EXISTS ("   +
				"             SELECT 1"   +
				"               FROM com_cuentas c"   +
				"              WHERE c.ic_moneda = m.ic_moneda"   +
				"                AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                AND c.ic_producto_nafin = 1"   +
				"                AND c.ic_tipo_cuenta = " + claveTipoCuenta + ")"   +
				"      AND m.ic_moneda = "+sNoMoneda+
				"      AND d.ic_moneda = m.ic_moneda"+
				"      AND ds.ic_epo = "+sNoEPO+
				"      AND n.cg_tipo = 'P'"   +
				"      AND ds.ic_if = "+sNoIF+
				"      AND d.ic_estatus_docto = 4"   +
				"      AND s.df_operacion >= TRUNC (SYSDATE)"   +
				"      AND s.df_operacion < (TRUNC (SYSDATE) + 1)"   +
				"      AND d.cs_dscto_especial != 'C' "   +
				"      AND ds.cs_opera_fiso = 'N' "  + // Documentos registrados bajo la modalidad normal (sin Fideicomiso para Desarrollo de Proveedores).
				" GROUP BY 0, d.ic_pyme, p.cg_rfc, p.cg_razon_social, m.cd_nombre"  ;
		} catch(Exception e) {
			System.out.println("DispersionEJB::getQueryDispFISOS (Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("DispersionEJB::getQueryDispFISOS (S)");
		}

		return query;
	}//getQueryDispFISOS

	public String getQueryDispPEMEX(String ic_folio_ff, String sNoEPO, String sNoMoneda, String sNoEstatus, String sFechaVenc, String sFechaOperacion) throws NafinException {
		System.out.println("DispersionEJB::getQueryDispPEMEX (E)");
		String query = "";
		try {
			if("2".equals(sNoEstatus)) {
				query =
					" SELECT   /*+index(d) use_nl(d n e c m)*/ DECODE (SUBSTR (u.consecutivo, 1, 8),"   +
					"        '"+ic_folio_ff+"' AS ic_flujo_fondos,"   +
					"          SUM (d.fn_monto) AS fn_importe,"   +
					"          COUNT (d.ic_documento) AS ig_total_documentos,"   +
					"          e.ic_epo, "   +
					"          d.ic_pyme,"   +
					"          c.ic_cuenta, "   +
					"          TO_CHAR (d.df_fecha_venc, 'yyyy-mm-dd') AS df_fecha_venc,"   +
					"          DECODE (c.ic_estatus_cecoban, 99, 0, -1) AS estatusoper"   +
					"     FROM dis_documento d,"   +
					"          comrel_nafin n,"   +
					"          comcat_epo e,"   +
					"          com_cuentas c,"   +
					"          comcat_moneda m"   +
					"    WHERE d.ic_epo = e.ic_epo"   +
					"      AND c.ic_moneda = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND n.ic_epo_pyme_if = e.ic_epo"   +
					"      AND n.cg_tipo = 'E'"   +
					"      AND c.ic_producto_nafin = 4"   +
					"      AND c.ic_tipo_cuenta = 40"   +
					"      AND c.cg_tipo_afiliado = 'E'"   +
					"      AND d.ic_producto_nafin = 4"   +
					"      AND m.ic_moneda = "+sNoMoneda+
					"      AND d.ic_moneda = m.ic_moneda"+
					"      AND d.ic_epo = "+sNoEPO+
					"      AND d.ic_estatus_docto = "+sNoEstatus+
					"      AND d.df_fecha_venc >= TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')"   +
					"      AND d.df_fecha_venc < (TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')+1)"   +
					" GROUP BY c.ic_estatus_cecoban, c.ic_cuenta, e.ic_epo, d.ic_pyme,"   +
					"          m.cd_nombre, TO_CHAR (d.df_fecha_venc, 'yyyy-mm-dd') "  ;
			} else if("PA".equals(sNoEstatus)) {
				query =
					" SELECT   /*+index(d) use_nl(d n e c m)*/ DECODE (SUBSTR (u.consecutivo, 1, 8),"   +
					"        '"+ic_folio_ff+"' AS ic_flujo_fondos,"   +
					"          SUM (d.FN_IMPORTE) AS fn_importe,"   +
					"          COUNT (1) AS ig_total_documentos,"   +
					"          e.ic_epo, "   +
					"          d.ic_pyme,"   +
					"          c.ic_cuenta, "   +
					"          TO_CHAR (d.df_fecha_aplicacion, 'yyyy-mm-dd') AS df_operacion,"   +
					"          DECODE (c.ic_estatus_cecoban, 99, 0, -1) AS estatusoper"   +
					"     FROM dis_pagos_ant d,"   +
					"          comrel_nafin n,"   +
					"          comcat_epo e,"   +
					"          com_cuentas c,"   +
					"          comcat_moneda m"   +
					"    WHERE d.ic_epo = e.ic_epo"   +
					"      AND c.ic_moneda = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND n.ic_epo_pyme_if = e.ic_epo"   +
					"      AND n.cg_tipo = 'E'"   +
					"      AND c.ic_producto_nafin = 4"   +
					"      AND c.ic_tipo_cuenta = 40"   +
					"      AND c.cg_tipo_afiliado = 'E'"   +
					"      AND d.CS_DISPERSION != 'S'"+
					"      AND m.ic_moneda = "+sNoMoneda+
					"      AND d.ic_moneda = m.ic_moneda"+
					"      AND d.ic_epo = "+sNoEPO+
					"      AND d.df_fecha_aplicacion >= TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy')"   +
					"      AND d.df_fecha_aplicacion < (TO_DATE ('"+sFechaOperacion+"', 'dd/mm/yyyy')+1)"   +
					" GROUP BY c.ic_estatus_cecoban, c.ic_cuenta, e.ic_epo, d.ic_pyme,"   +
					"          m.cd_nombre, TO_CHAR (d.df_fecha_aplicacion, 'yyyy-mm-dd')"  ;
			}
		} catch(Exception e) {
			System.out.println("DispersionEJB::getQueryDispPEMEX (Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("DispersionEJB::getQueryDispPEMEX (S)");
		}
		return query;
	}//getQueryDispFISOS

	private synchronized long getFolioFF() throws NafinException {

		System.out.println("DispersionEJB::getFolioFF(E)");

		AccesoDB 			con				= new AccesoDB();
		String 				qrySentencia	= "";
		PreparedStatement ps 				= null;
		ResultSet			rs					= null;
		String 				fechaHoy 		= new java.text.SimpleDateFormat("yyyyMMdd").format(new java.util.Date());//
		String				folioHoy			= fechaHoy+"00000";
		long					folioFF			= 0L;
		boolean				bOk				= true;
		System.out.println("----------------> fechaHoy = <"+fechaHoy+">");//Debug info
		try {
			con.conexionDB();

			// OBTENER FOLIO NUEVO
			qrySentencia =
				" SELECT IC_FLUJO_FONDOS AS CONSECUTIVO FROM COM_ULTIMO_FOLIO_FFON WHERE IC_REGISTRO = 1";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				folioFF = (rs.getString(1)==null)?Long.parseLong(folioHoy):rs.getLong(1);
				folioFF = folioFF+1L;
			}//while
			rs.close();
			if(ps != null) ps.close();

			// SI EL FOLIO NUEVO PERTENECE AL DIA ANTERIOR, OBTENER EL PRIMERO DEL NUEVO DIA
			if(!fechaHoy.equals(Long.toString(folioFF).substring(0,8)))
				folioFF = Long.parseLong(folioHoy);

			// ACTUALIZAR COM_ULTIMO_FOLIO_FFON CON EL ULTIMO FOLIO GENERADO
			qrySentencia =
				" UPDATE COM_ULTIMO_FOLIO_FFON  "  +
				"    SET                        "  +
				"      IC_FLUJO_FONDOS = ?      "  +
				"  WHERE                        "  +
				"      IC_REGISTRO = ?          " ;

			ArrayList lVarBind = new ArrayList();
			lVarBind.add(new Long(folioFF));
			lVarBind.add(new Integer("1"));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			bOk = false;
			System.out.println("DispersionEJB::getFolioFF (Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
		   con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("DispersionEJB::getFolioFF (S)");
		}

		//return Long.toString(folioFF);
		return folioFF;
	}
	/*
	private String getFolioFF(AccesoDB con) throws NafinException {
		System.out.println("DispersionEJB::getFolioFF (E)");
		String 				qrySentencia	= "";
		PreparedStatement 	ps 				= null;
		ResultSet			rs				= null;
		String 				fechaHoy 		= new java.text.SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
		String				folioHoy		= fechaHoy+"00000";
		String				folioFF			= "";
		try {
			qrySentencia =
				" SELECT NVL (MAX (ic_flujo_fondos),"   +
				"             TO_NUMBER (TO_CHAR (SYSDATE, 'yyyymmdd') || '00000')"   +
				"            ) AS consecutivo"   +
				"   FROM (SELECT ic_flujo_fondos"   +
				"           FROM int_flujo_fondos"   +
				"         UNION"   +
				"         SELECT ic_flujo_fondos"   +
				"           FROM int_flujo_fondos_err)"  ;
			System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				folioFF = (rs.getString(1)==null)?folioHoy:rs.getString(1);
			}//while
			rs.close();
			if(ps != null) ps.close();

			if(!fechaHoy.equals(folioFF.substring(0,8)))
				folioFF = folioHoy;


		} catch(Exception e) {
			System.out.println("DispersionEJB::getFolioFF (Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("DispersionEJB::getFolioFF (S)");
		}
		return folioFF;
	}*/

	public void llenaEncabezadoFlujoFondos(
										String sNoEPO, 	String sNoMoneda, 	String sFechaVenc,
										String sNoIF,	String sOrigen, 	String sNoEstatus,
										String sFechaOperacion) throws NafinException {
		llenaEncabezadoFlujoFondos(
			sNoEPO,
			sNoMoneda,
			sFechaVenc,
			sNoIF,
			sOrigen,
			sNoEstatus,
			sFechaOperacion,
			false
		);

	}

		//	void llenaEncabezadoFlujoFondos() -- realizado por HDG -- 22/08/2003 --
	public void llenaEncabezadoFlujoFondos(
										String sNoEPO, 	String sNoMoneda, 	String sFechaVenc,
										String sNoIF,	String sOrigen, 	String sNoEstatus,
										String sFechaOperacion, boolean actualizarTablero ) throws NafinException {

		llenaEncabezadoFlujoFondos(
			sNoEPO,
			sNoMoneda,
			sFechaVenc,
			sNoIF,
			sOrigen,
			sNoEstatus,
			sFechaOperacion,
			actualizarTablero,
			""
   	);

	}
	//	void llenaEncabezadoFlujoFondos() -- realizado por HDG -- 22/08/2003 --
	public void llenaEncabezadoFlujoFondos(
										String sNoEPO, 	String sNoMoneda, 	String sFechaVenc,
										String sNoIF,	String sOrigen, 	String sNoEstatus,
										String sFechaOperacion, boolean actualizarTablero, String sNoIfFondeo ) throws NafinException { // Actualizar tablero actualmente solo funciona para EPOs.

		System.out.println("DispersionEJB::llenaEncabezadoFlujoFondos (E)");
		System.out.println("\n sNoEPO:			   " + sNoEPO);
		System.out.println("\n sNoMoneda:		   " + sNoMoneda);
		System.out.println("\n sFechaVenc:		   " + sFechaVenc);
		System.out.println("\n sNoIF:			      " + sNoIF);
		System.out.println("\n sOrigen:			   " + sOrigen);
		System.out.println("\n sNoEstatus:		   " + sNoEstatus);
		System.out.println("\n sFechaOperacion:	" + sFechaOperacion);
		System.out.println("\n actualizarTablero: " + actualizarTablero );
		System.out.println("\n sNoIfFondeo:       " + sNoIfFondeo );

		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement 	ps 				= null;
		ResultSet 			rs 				= null;
		String 				query 			= "",
							queri 			= "",
							sRazonSocial 	= "";
		boolean 			bOk 			= true;

		try {
			con.conexionDB();

			//Obtenci�n de la Razon Social que realiza la Dispersi�n de acuerdo al Tipo.
			sRazonSocial = getRazonSocial(sOrigen, sNoEPO, sNoIF, con);

			System.out.println("\n ***********************************************");
			System.out.println("\n sOrigen: "+sOrigen);
			String folio_ff = "";//getFolioFF(con);
			if(sOrigen.equals("INFONAVIT")) {		// Dispersion INFONAVIT "EPOS"
				query = getQueryDispINFONAVIT(folio_ff, sNoEPO, sNoMoneda, sNoEstatus, sFechaOperacion);
			} else if(sOrigen.equals("EPOS")) {		// Dispersion EPOS
				query = getQueryDispEPOS(folio_ff, sNoEPO, sNoMoneda, sNoEstatus, sFechaVenc);
			} else if(sOrigen.equals("FISOS")) {	// Dispersion FISOS
				query = getQueryDispFISOS(folio_ff, sNoEPO, sNoMoneda, sNoIF);
			} else if(sOrigen.equals("IFFIDEICOMISO")) {	// Dispersion FISOS ( IF FIDEICOMISO )
				query = getQueryDispIfFideicomiso( folio_ff, sNoIF, sNoEPO, sNoIfFondeo, sNoMoneda );
			} else if(sOrigen.equals("PEMEX")) {	// Dispersion PEMEX
				query = getQueryDispPEMEX(folio_ff, sNoEPO, sNoMoneda, sNoEstatus, sFechaVenc, sFechaOperacion);
			}

			try {
				int 	iNoAfecFF = 0, iNoAfecEnca = 0;
				long 	iNoReg = 0l;
				System.out.println("\n ****************** \n Registros a Insertar en FF: "+query);
				ps = con.queryPrecompilado(query);
				ps.clearParameters();
				rs = ps.executeQuery();

				boolean esEstatusOperada 				= sNoEstatus.equals("4")	?true:false;
				boolean esEstatusOperadaPagada 		= sNoEstatus.equals("11")	?true:false;
				boolean esEstatusVencidoSinOperar 	= sNoEstatus.equals("9")	?true:false;
				boolean esEstatusPagadoSinOperar 	= sNoEstatus.equals("10")	?true:false;

				while(rs.next()) {
					iNoReg++;

					long 		folioFF		= getFolioFF();//rs.getLong("ic_flujo_fondos") + iNoReg;
					double 	importe		= rs.getDouble("fn_importe");
					int 		totDoctos	= rs.getInt("ig_total_documentos");
					int 		estatusoper	= rs.getInt("estatusoper");

					String desarrolador 	= "";
					String beneficiario 	= "";
					String ic_pyme 		= "";
					String ic_if 			= "";

					if(sOrigen.equals("INFONAVIT")) {
						desarrolador = rs.getString("desarrollador")==null?"0":rs.getString("desarrollador");
						beneficiario = rs.getString("beneficiario")==null?"0":rs.getString("beneficiario");
					} else if(sOrigen.equals("EPOS") && (sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1"))) {
						desarrolador = rs.getString("desarrollador")==null?"0":rs.getString("desarrollador");
						beneficiario = rs.getString("beneficiario")==null?"0":rs.getString("beneficiario");
					} else if(sOrigen.equals("EPOS") && (sNoEstatus.equals("4") || sNoEstatus.equals("11"))) {
						ic_if = rs.getString("ic_if")==null?"0":rs.getString("ic_if");
					} else if( sOrigen.equals("FISOS")         ) {
						ic_pyme = rs.getString("ic_pyme")==null?"0":rs.getString("ic_pyme");
					} else if( sOrigen.equals("IFFIDEICOMISO") ) { // Dispersion FISOS ( IF FIDEICOMISO )
						ic_pyme = rs.getString("ic_pyme")==null?"0":rs.getString("ic_pyme");
					} else if(sOrigen.equals("PEMEX")) {
						ic_pyme = rs.getString("ic_pyme")==null?"0":rs.getString("ic_pyme");
					}

					queri =
						" INSERT INTO int_flujo_fondos"   +
						"             (ic_flujo_fondos, fn_importe, ig_total_documentos, df_fecha_venc,"   +
						"              ic_epo, ic_moneda, ic_if, ic_pyme, ic_cuenta, ic_estatus_docto,"   +
						"              df_operacion, ic_if_fondeo )"   +
						"      VALUES (?, ?, ?, TO_DATE (?, 'dd/mm/yyyy'),"   +
						"              ?, ?, ?, ?, ?, ?,"   +
						"              TO_DATE (?, 'dd/mm/yyyy'), ? )"  ;
					ps = con.queryPrecompilado(queri);

					ps.setLong(		1, folioFF);
					ps.setDouble(	2, importe);
					ps.setInt(		3, totDoctos);

					if(sFechaVenc.equals(""))
						ps.setNull(4, Types.VARCHAR);
					else
						ps.setString(4, rs.getString("df_fecha_venc"));
					ps.setInt(5, Integer.parseInt(sNoEPO.trim()));
					ps.setInt(6, Integer.parseInt(sNoMoneda.trim()));
					if ( sOrigen.equals("INFONAVIT") ||
						(sOrigen.equals("EPOS") && (sNoEstatus.equals("9") || sNoEstatus.equals("10")|| sNoEstatus.equals("1")))) {

						if(rs.getInt("desarrollador")!=0)
							ps.setInt(8, rs.getInt("desarrollador"));
						else
							ps.setNull(8, Types.INTEGER);
						if(rs.getInt("beneficiario")!=0)
							ps.setInt(7,rs.getInt("beneficiario"));
						else
							ps.setNull(7, Types.INTEGER);
					} else {
						if(sOrigen.equals("EPOS") && (sNoEstatus.equals("4") || sNoEstatus.equals("11")))
							sNoIF = rs.getString("ic_if");

						if(sNoIF.equals(""))
							ps.setNull(7, Types.INTEGER);
						else
							ps.setInt(7, Integer.parseInt(sNoIF.trim()));

						if(rs.getString("ic_pyme")==null)
							ps.setNull(8, Types.INTEGER);
						else
							ps.setInt(8, rs.getInt("ic_pyme"));
					}
					if(rs.getString("ic_cuenta")==null)
						ps.setNull(9, Types.INTEGER);
					else
						ps.setLong(9, rs.getLong("ic_cuenta"));


					if(sNoEstatus.equals("")||sNoEstatus.equals("PA"))
						ps.setNull(10, Types.INTEGER);
					else
						ps.setInt(10, Integer.parseInt(sNoEstatus.trim()));

					if(sFechaOperacion.equals(""))
						ps.setNull(11, Types.VARCHAR);
					else
						ps.setString(11, rs.getString("df_operacion"));

					if( sOrigen.equals("IFFIDEICOMISO") && !sNoIfFondeo.equals("") ){ // Dispersion FISOS ( IF FIDEICOMISO )
						ps.setInt(12,  Integer.parseInt(sNoIfFondeo.trim()));
					} else {
						ps.setNull(12, Types.INTEGER);
					}

					iNoAfecFF += ps.executeUpdate();
					ps.clearParameters();

					queri =
						" INSERT INTO cfe_m_encabezado "   +
						"             (idoperacion_cen_i, idsistema_cen_i, status_cen_i, error_cen_s,"   +
						"              historia_cen_s, idoperef_cen_i)"   +
						"      VALUES (?, ?, ?, ?,"   +
						"              ?, ?)"  ;

					// NOTA: PARA LA CUENTA EN DOLARES, PONER 9998, CUANDO LA CUENTA ESTE VALIDADA Y LOS DOCTOS NO TENGAN
					// ESTATUS: 4 (OPERADA), 11 (OPERADA PAGADA), 9 (VENCIDO SIN OPERAR) O 10 (PAGADO SIN OPERAR), CON ESTO SE
					// EVITA QUE LOS DOCTOS SEAN PROCESADOS POR SP_DETALLES_FF
					if( sNoMoneda.equals("54") && !"EPOS".equals(sOrigen) ){
						estatusoper = 9998;
					}else	if( sNoMoneda.equals("54") && estatusoper == 0 ){
						if( !(esEstatusOperada || esEstatusOperadaPagada || esEstatusVencidoSinOperar || esEstatusPagadoSinOperar) ){
								estatusoper = 9998;
						}
					}

					ps = con.queryPrecompilado(queri);
					ps.setLong(	1, folioFF);
					ps.setInt(	2, 200);
					ps.setInt(	3, estatusoper); // 0 cuenta valida, -1 cuenta invalida
					ps.setNull(	4, Types.VARCHAR);
					ps.setNull(	5, Types.VARCHAR);
					ps.setNull(	6, Types.INTEGER);
					iNoAfecEnca += ps.executeUpdate();
					ps.clearParameters();

					if(sOrigen.equals("PEMEX"))
						setRelacionDoctosFFDist(sNoEPO, sNoEstatus, sFechaVenc, sFechaOperacion, ic_pyme, folioFF, importe, totDoctos, con);
					else
						setRelacionDoctosFF(sOrigen, sNoEPO, sNoEstatus, sFechaOperacion, sFechaVenc, sNoIF, desarrolador, beneficiario, ic_if, ic_pyme, folioFF, importe, totDoctos, sNoMoneda, con, null, sNoIfFondeo );

				}//while(rs.next()) insert FF
				rs.close();
				if(ps != null) ps.close();

				if( "PEMEX".equals(sOrigen) ){
					sOrigen = "EPOS";
				} else if( "IFFIDEICOMISO".equals(sOrigen) ){ // Dispersion FISOS ( IF FIDEICOMISO )
					sOrigen = "FISOS";
				}

				String param2IdSistema = "200";
				String param3AreaCaptura = "EF15";
				//Modificado EGB Foda 051 - 2004
				if (sOrigen.equals("INFONAVIT")) {
					param3AreaCaptura = "EF02";
				} else if (sOrigen.equals("FISOS")){
					param3AreaCaptura = "EF04";
				}

            String param5TipoOper 			= "";
				String param8CveCteOrden 		= "";
				String param9RFCCteOrden 		= "";
				String param10NomCteOrden 		= "";
				String param11DomCteOrden 		= "";
				String param12Banco 				= "";
				String param13Plaza 				= "";
				String param14Sucursal 			= "";
				String param15NoCta 				= "";
				String param16y27TipoCta 		= "";
				String param17y28TipoDivisa 	= "";

				// Documento(s) en Moneda Nacional
				if("1".equals(sNoMoneda)){

					param5TipoOper 			= "TEF";
					param8CveCteOrden 		= "37135";
					param9RFCCteOrden 		= "NFI3406305TO";
					param10NomCteOrden 		= "NACIONAL FINANCIERA, S.N.C.";
					param11DomCteOrden 		= "INSURGENTES SUR 1971 COL. GUADALUPE INN DEL. ALVARO OBREGON C.P. 01020";
					param12Banco 				= "37135";
					param13Plaza 				= "1001";
					param14Sucursal 			= "25";
					param15NoCta 				= "TEF";
					param16y27TipoCta 		= "CH";
					param17y28TipoDivisa 	= "MXN";

				// Documento(s) en Dolares Americanos con estatus: Operada y Operada Pagada
				}else if( "54".equals(sNoMoneda) && (esEstatusOperada || esEstatusOperadaPagada) ){

					param5TipoOper 			= "MT202"; // Dispersion a Intermediario
					param8CveCteOrden 		= "37135";
					param9RFCCteOrden 		= "NFI3406305TO";
					param10NomCteOrden 		= "NACIONAL FINANCIERA, S.N.C.";
					param11DomCteOrden 		= "INSURGENTES SUR 1971 COL. GUADALUPE INN DEL. ALVARO OBREGON C.P. 01020";
					param12Banco 				= "BKTRUS33";
					param13Plaza 				= "US-306";
					param14Sucursal 			= "1";
					param15NoCta 				= "BK01";
					param16y27TipoCta 		= "CH";
					param17y28TipoDivisa 	= "USD";

				// Documento(s) en Dolares Americanos con estatus: Vencido Sin Operar y Pagado Sin Operar
				}else if(  "54".equals(sNoMoneda) &&  (esEstatusVencidoSinOperar || esEstatusPagadoSinOperar) ){

					// Obtener la Clave EPO FFON:
					String 	claveEpoFFON 	= getClaveEpoEnFFON(sNoEPO);
					HashMap 	datosEPO			= getDatosClienteOrdenante(sNoEPO);

					String   domicilio		= (String) datosEPO.get("DOMICILIO_EPO");
					domicilio					= domicilio.length() > 255?domicilio.substring(0,255):domicilio;

					param5TipoOper 			= "MT103"; 			// Dispersion a Proveedores
					param8CveCteOrden 		= claveEpoFFON;
					param9RFCCteOrden 		= (String) datosEPO.get("RFC_EPO");
					param10NomCteOrden 		= (String) datosEPO.get("RAZON_SOCIAL");
					param11DomCteOrden 		= domicilio;
					param12Banco 				= "BKTRUS33";
					param13Plaza 				= "US-306";
					param14Sucursal 			= "1";
					param15NoCta 				= "BK01";
					param16y27TipoCta 		= "CH";
					param17y28TipoDivisa 	= "USD";

				}

				// Invocar SP_DETALLES_FF
				CallableStatement cs = con.ejecutaSP("SP_DETALLES_FF(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				cs.setString( 1, getFechaSiguienteHabil());
				cs.setString( 2, param2IdSistema);
				cs.setString( 3, param3AreaCaptura);
				cs.setString( 4, param5TipoOper);
				cs.setString( 5, param8CveCteOrden);
				cs.setString( 6, param9RFCCteOrden);
				cs.setString( 7, param10NomCteOrden);
				cs.setString( 8, param11DomCteOrden);
				cs.setString( 9, param12Banco);
				cs.setString(10, param13Plaza);
				cs.setString(11, param14Sucursal);
				cs.setString(12, param15NoCta);
				cs.setString(13, param16y27TipoCta);
				cs.setString(14, param17y28TipoDivisa);
				cs.setString(15, sOrigen);
				cs.setString(16, sRazonSocial);
				cs.execute();
				cs.close();

				// Actualizar tablero de dispersion EPOs
				if( actualizarTablero && "EPOS".equals(sOrigen) ){

					actualizaFechaEnvioFFONTableroDispersionEPO(
						sNoEPO,
						sNoMoneda,
						sFechaVenc,
						sNoEstatus,
						con
					);

				}

			} catch(SQLException sqle) {
				bOk = false;
				sqle.printStackTrace();
				throw new NafinException("DISP0010");
			}

		} catch(NafinException ne) {
			bOk = false;
			throw ne;
		} catch(Exception e) {
			bOk = false;
			System.out.println("Exception en llenaEncabezadoFlujoFondos(). "+e.getMessage());
			e.printStackTrace();
		}
		finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			System.out.println("DispersionEJB::llenaEncabezadoFlujoFondos (S)");
		}
	}//llenaEncabezadoFlujoFondos

	private void setRelacionDoctosFFDist(String sNoEPO, String sNoEstatus, String sFechaVenc, String sFechaOperacion, String ic_pyme,
								long folioFF, double importe, int totDoctos, AccesoDB con) throws NafinException {
		System.out.println("Dispersion::setRelacionDoctosFFDist(E)");

		System.out.println("sNoEPO: "+sNoEPO);
		System.out.println("sNoEstatus: "+sNoEstatus);
		System.out.println("sFechaVenc: "+sFechaVenc);
		System.out.println("ic_pyme: "+ic_pyme);
		System.out.println("folioFF: "+folioFF);
		System.out.println("importe: "+importe);
		System.out.println("totDoctos: "+totDoctos);

		String qrySentenciaTotales = "";
		String qrySentenciaFolios = "";
		String qrySentenciaInsert = "";
		PreparedStatement psT = null;
		PreparedStatement psF = null;
		PreparedStatement psI = null;
		ResultSet rsT = null;
		ResultSet rsF = null;
		boolean doctosOK = false;

		try {
			if("2".equals(sNoEstatus)) {
				qrySentenciaTotales =
					" SELECT COUNT (1) AS totdocs, NVL (SUM (doc.fn_monto), 0) AS totmonto"   +
					"   FROM dis_documento doc"   +
					"  WHERE NOT EXISTS (SELECT 1"   +
					"                      FROM comrel_documento_ff dff"   +
						"                    WHERE doc.ic_documento = dff.ic_documento "+
						"                     AND dff.ic_producto_nafin = 4) "+
					"    AND doc.ic_epo = ?"   +
					"    AND doc.ic_pyme = ?"   +
					"    AND doc.df_fecha_venc >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND doc.df_fecha_venc < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  +
					"    AND doc.ic_estatus_docto = ?"   ;

				qrySentenciaFolios =
					" SELECT "+folioFF+" as folio, doc.ic_documento "   +
					"   FROM dis_documento doc"   +
					"  WHERE NOT EXISTS (SELECT 1"   +
					"                      FROM comrel_documento_ff dff"   +
						"                    WHERE doc.ic_documento = dff.ic_documento "+
						"                     AND dff.ic_producto_nafin = 4) "+
					"    AND doc.ic_epo = ?"   +
					"    AND doc.ic_estatus_docto = ?"   +
					"    AND doc.ic_pyme = ?"   +
					"    AND doc.df_fecha_venc >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND doc.df_fecha_venc < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  ;
			} else if("PA".equals(sNoEstatus)) {
				qrySentenciaTotales =
					" SELECT COUNT (1) AS totdocs, NVL (SUM (d.FN_IMPORTE), 0) AS totmonto"   +
					"   FROM dis_pagos_ant d"   +
					"  WHERE NOT EXISTS (SELECT 1"   +
					"                      FROM comrel_documento_ff dff"   +
						"                    WHERE d.ic_pago = dff.ic_pago "+
						"                     AND dff.ic_producto_nafin = 4) "+
					"    AND d.ic_epo = ?"   +
					"    AND d.ic_pyme = ?"   +
					"    AND d.df_fecha_aplicacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND d.df_fecha_aplicacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  ;

				qrySentenciaFolios =
					" SELECT "+folioFF+" as folio, d.ic_pago "   +
					"   FROM dis_pagos_ant d"   +
					"  WHERE NOT EXISTS (SELECT 1"   +
					"                      FROM comrel_documento_ff dff"   +
						"                    WHERE d.ic_pago = dff.ic_pago "+
						"                     AND dff.ic_producto_nafin = 4) "+
					"    AND d.ic_epo = ?"   +
					"    AND d.ic_pyme = ?"   +
					"    AND d.df_fecha_aplicacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND d.df_fecha_aplicacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  ;
			}

			//System.out.println("qrySentencia: "+qrySentencia);
			psT = con.queryPrecompilado(qrySentenciaTotales);
			psT.setInt(1, Integer.parseInt(sNoEPO));
			psT.setInt(2, Integer.parseInt(ic_pyme));
			if("2".equals(sNoEstatus)) {
				psT.setString(3, sFechaVenc);
				psT.setString(4, sFechaVenc);
				psT.setInt(5, Integer.parseInt(sNoEstatus));
			} else {
				psT.setString(3, sFechaOperacion);
				psT.setString(4, sFechaOperacion);
			}


			rsT = psT.executeQuery();
			psT.clearParameters();
			if(rsT.next()){
				//System.out.println("totDoctos: "+totDoctos);
				//System.out.println("rsT.getInt(totDocs): "+rsT.getInt("totDocs"));
				//if(rst.getInt("totDocs")==totDoctos && rst.getDouble("totMonto")==importe)
				if(rsT.getInt("totDocs")==totDoctos)
					doctosOK = true;
			}
			if(psT != null) psT.close();
			rsT.close();
			if(doctosOK) {
				//System.out.println("qrySentencia: "+qrySentencia);
				psF = con.queryPrecompilado(qrySentenciaFolios);
				psF.setInt(1, Integer.parseInt(sNoEPO));
				psF.setInt(2, Integer.parseInt(ic_pyme));
				if("2".equals(sNoEstatus)) {
					psF.setString(3, sFechaVenc);
					psF.setString(4, sFechaVenc);
					psF.setInt(5, Integer.parseInt(sNoEstatus));
			 	} else {
					psF.setString(3, sFechaOperacion);
					psF.setString(4, sFechaOperacion);
			 	}

				System.out.println("qrySentenciaFolios: "+qrySentenciaFolios);
				rsF = psF.executeQuery();
				psF.clearParameters();
				while(rsF.next()) {
					qrySentenciaInsert =
						" INSERT INTO comrel_documento_ff"   +
						"             (ic_documento_ff, ic_flujo_fondos, ic_documento, ic_producto_nafin, ic_pago) "   +
						"  VALUES "  +
						"				  (SEQ_COMREL_DOCUMENTO_FF.NEXTVAL, ?, ?, 4, ?) ";
						//"         SELECT SEQ_COMREL_DOCUMENTO_FF.NEXTVAL, ?, ?, 4, ?"   +
						//"           FROM comrel_documento_ff"  ;
					psI = con.queryPrecompilado(qrySentenciaInsert);
					psI.setLong(1, rsF.getLong("folio"));
					if("2".equals(sNoEstatus))
						psI.setLong(2, rsF.getInt("ic_documento"));
					else
						psI.setNull(2, Types.INTEGER);

					if("PA".equals(sNoEstatus))
						psI.setLong(3, rsF.getInt("ic_pago"));
					else
						psI.setNull(3, Types.INTEGER);

					psI.executeUpdate();
					psI.close();

					if("PA".equals(sNoEstatus)) {
						qrySentenciaInsert =
							" UPDATE dis_pagos_ant"   +
							"    SET cs_dispersion = 'S'"   +
							"  WHERE ic_pago = ?"  ;
						psI = con.queryPrecompilado(qrySentenciaInsert);
						psI.setLong(1, rsF.getInt("ic_pago"));
						psI.executeUpdate();
						psI.close();
					}//if("PA".equals(sNoEstatus))
				}//while(rsF.next())
				if(psF != null) psF.close();
				rsF.close();
			}//if(doctosOK)

		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Dispersion::setRelacionDoctosFFDist(Exception) "+e);
			throw new NafinException("DISP0010");
		} finally {
			System.out.println("Dispersion::setRelacionDoctosFFDist(S)");
		}
	}//setRelacionDoctosFF

	private void setRelacionDoctosFF(String sOrigen, String sNoEPO, String sNoEstatus, String sFechaOperacion,
								String sFechaVenc, String sNoIF,
								String desarrollador, String beneficiario, String ic_if, String ic_pyme,
								long folioFF, double importe, int totDoctos, String sMoneda,
								AccesoDB con, String icDocumentoCadenaNafin, String sNoIfFondeo ) throws NafinException {
		System.out.println("Dispersion::setRelacionDoctosFF(E)");

		System.out.println("sOrigen:_________ "+sOrigen);
		System.out.println("sNoEPO:__________ "+sNoEPO);
		System.out.println("sNoEstatus:______ "+sNoEstatus);
		System.out.println("sFechaOperacion:_ "+sFechaOperacion);
		System.out.println("sFechaVenc:______ "+sFechaVenc);
		System.out.println("sNoIF:___________ "+sNoIF);
		System.out.println("desarrollador:___ "+desarrollador);
		System.out.println("beneficiario:____ "+beneficiario);
		System.out.println("ic_if:___________ "+ic_if);
		System.out.println("ic_pyme:_________ "+ic_pyme);
		System.out.println("folioFF:_________ "+folioFF);
		System.out.println("importe:_________ "+importe);
		System.out.println("totDoctos:_______ "+totDoctos);
		System.out.println("sMoneda:_______ "+sMoneda);
		System.out.println("icDocumentoCadenaNafin:_______ "+icDocumentoCadenaNafin);
		System.out.println("sNoIfFondeo:_______ "+sNoIfFondeo);


		String qrySentenciaTotales = "";
		String qrySentenciaFolios = "";
		String qrySentenciaInsert = "";
		PreparedStatement psT = null;
		PreparedStatement psF = null;
		PreparedStatement psI = null;
		ResultSet rsT = null;
		ResultSet rsF = null;
		int cont = 0;
		boolean doctosOK = false;

		try {
			if(sOrigen.equals("INFONAVIT")) {
				qrySentenciaTotales =
					" SELECT COUNT (1) as totDocs, NVL (SUM (doc.fn_monto), 0) as totMonto"   +
					"   FROM com_documento doc, com_docto_seleccionado sel, com_solicitud sol"   +
					"  WHERE doc.ic_documento = sel.ic_documento"   +
					"    AND sel.ic_documento = sol.ic_documento"   +
					"    AND NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"    AND doc.ic_epo = ?"   +
					"    AND doc.ic_estatus_docto = ?"   +
					"    AND sol.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND sol.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  ;
				if(Integer.parseInt(desarrollador)!=0)
					qrySentenciaTotales += "    AND doc.ic_pyme = ?";
				else if(Integer.parseInt(beneficiario)!=0)
					qrySentenciaTotales += "    AND doc.ic_beneficiario = ? ";
				//System.out.println("qrySentencia: "+qrySentencia);

				psT = con.queryPrecompilado(qrySentenciaTotales);
				psT.setLong(	1, folioFF);
				psT.setInt(		2, Integer.parseInt(sNoEPO));
				psT.setInt(		3, Integer.parseInt(sNoEstatus));
				psT.setString(	4, sFechaOperacion);
				psT.setString(	5, sFechaOperacion);
				cont = 5;
				if(Integer.parseInt(desarrollador)!=0) {
					cont++;psT.setInt(cont, Integer.parseInt(desarrollador));
				} else if(Integer.parseInt(beneficiario)!=0) {
					cont++;psT.setInt(cont, Integer.parseInt(beneficiario));
				}

				qrySentenciaFolios =
					" SELECT "+folioFF+" as folio, doc.ic_documento "   +
					"   FROM com_documento doc, com_docto_seleccionado sel, com_solicitud sol"   +
					"  WHERE doc.ic_documento = sel.ic_documento"   +
					"    AND sel.ic_documento = sol.ic_documento"   +
					"    AND NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"    AND doc.ic_epo = ?"   +
					"    AND doc.ic_estatus_docto = ?"   +
					"    AND sol.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND sol.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  ;
				if(Integer.parseInt(desarrollador)!=0)
					qrySentenciaFolios += "    AND doc.ic_pyme = ?";
				else if(Integer.parseInt(beneficiario)!=0)
					qrySentenciaFolios += "    AND doc.ic_beneficiario = ? ";
				//System.out.println("qrySentencia: "+qrySentencia);
				psF = con.queryPrecompilado(qrySentenciaFolios);
				psF.setLong(	1, folioFF);
				psF.setInt(		2, Integer.parseInt(sNoEPO));
				psF.setInt(		3, Integer.parseInt(sNoEstatus));
				psF.setString(	4, sFechaOperacion);
				psF.setString(	5, sFechaOperacion);
				cont = 5;
				if(Integer.parseInt(desarrollador)!=0) {
					cont++;psF.setInt(cont, Integer.parseInt(desarrollador));
				} else if(Integer.parseInt(beneficiario)!=0) {
					cont++;psF.setInt(cont, Integer.parseInt(beneficiario));
				}
			} else if(sOrigen.equals("EPOS")) {

				ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

				String sFechaVencPyme = "";
				if("9".equals(sNoEstatus)) {
					sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(sNoEPO);
				}

				String numDocDisp = "COUNT (1)";
				String importeDisp = "d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)";


				if("9".equals(sNoEstatus)) {
					numDocDisp = "SUM (DECODE (doc.cs_dscto_especial, 'C', 0, 1))";
					importeDisp = "DECODE (doc.cs_dscto_especial, 'C', -1, 1) * ("+importeDisp+")";
				}


				qrySentenciaTotales =
					" SELECT "+numDocDisp+" totDocs, NVL (SUM (doc.fn_monto), 0) as totMonto"   +
					"   FROM com_documento doc"   +
					"  WHERE NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"	 AND doc.ic_epo = ?"   +
					"    AND doc.ic_estatus_docto = ?"   +
					"    AND doc.df_fecha_venc"+sFechaVencPyme+" >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND doc.df_fecha_venc"+sFechaVencPyme+" < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)" +
					"	 AND doc.ic_moneda = ?"   ;

				if(sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1")) {
					if(Integer.parseInt(desarrollador)!=0)
						qrySentenciaTotales += "    AND doc.ic_pyme = ?";
					else if(Integer.parseInt(beneficiario)!=0)
						qrySentenciaTotales += "    AND doc.ic_beneficiario = ? ";
				} else if(sNoEstatus.equals("4") || sNoEstatus.equals("11")) {
					if(Integer.parseInt(ic_if)!=0)
						qrySentenciaTotales += "    AND doc.ic_if = ? ";
          qrySentenciaTotales += "    AND doc.cs_dscto_especial != 'C'";
				}
				// Debug info: start
				System.out.println(" folioFF 		  = <"+folioFF+">");
				System.out.println(" sNoEPO 		  = <"+sNoEPO+">");
				System.out.println(" sNoEstatus 	  = <"+sNoEstatus+">");
				System.out.println(" sFechaVenc 	  = <"+sFechaVenc+">");
				System.out.println(" sFechaVenc 	  = <"+sFechaVenc+">");
				System.out.println(" sMoneda 		  = <"+sMoneda+">");
				// Debug info: end


				psT = con.queryPrecompilado(qrySentenciaTotales);
				psT.setLong(	1, folioFF);
				psT.setInt(		2, Integer.parseInt(sNoEPO));
				psT.setInt(		3, Integer.parseInt(sNoEstatus));
				psT.setString(	4, sFechaVenc);
				psT.setString(	5, sFechaVenc);
				psT.setInt(	6, Integer.parseInt(sMoneda));
				cont = 6;
				if(sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1")) {
					if(Integer.parseInt(desarrollador)!=0) {
						cont++;psT.setInt(cont, Integer.parseInt(desarrollador));
						System.out.println(" desarrollador = <"+desarrollador+">");// Debug info
					} else if(Integer.parseInt(beneficiario)!=0) {
						cont++;psT.setInt(cont, Integer.parseInt(beneficiario));
						System.out.println(" beneficiario  = <"+beneficiario+">");// Debug info
					}
				} else if(sNoEstatus.equals("4") || sNoEstatus.equals("11")) {
					if(Integer.parseInt(ic_if)!=0) {
						cont++;psT.setInt(cont, Integer.parseInt(ic_if));
						System.out.println(" ic_if         = <"+ic_if+">");// Debug info
					}
				}

				qrySentenciaFolios =
					" SELECT "+folioFF+" as folio, doc.ic_documento "   +
					"   FROM com_documento doc"   +
					"  WHERE NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"	 AND doc.ic_epo = ?"   +
					"    AND doc.ic_estatus_docto = ?"   +
					"    AND doc.df_fecha_venc"+sFechaVencPyme+" >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND doc.df_fecha_venc"+sFechaVencPyme+" < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)" +
					"    AND doc.ic_moneda = ?"   ;

				if(sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1")) {
					if(Integer.parseInt(desarrollador)!=0)
						qrySentenciaFolios += "    AND doc.ic_pyme = ?";
					else if(Integer.parseInt(beneficiario)!=0)
						qrySentenciaFolios += "    AND doc.ic_beneficiario = ? ";
				} else if(sNoEstatus.equals("4") || sNoEstatus.equals("11")) {
					if(Integer.parseInt(ic_if)!=0)
						qrySentenciaFolios += "    AND doc.ic_if = ? ";
          qrySentenciaFolios += "    AND doc.cs_dscto_especial != 'C'";
				}
				psF = con.queryPrecompilado(qrySentenciaFolios);
				psF.setLong(	1, folioFF);
				psF.setInt(		2, Integer.parseInt(sNoEPO));
				psF.setInt(		3, Integer.parseInt(sNoEstatus));
				psF.setString(	4, sFechaVenc);
				psF.setString(	5, sFechaVenc);
				psF.setInt(	6, Integer.parseInt(sMoneda));
				cont = 6;
				if(sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1")) {
					if(Integer.parseInt(desarrollador)!=0) {
						cont++;psF.setInt(cont, Integer.parseInt(desarrollador));
					} else if(Integer.parseInt(beneficiario)!=0) {
						cont++;psF.setInt(cont, Integer.parseInt(beneficiario));
					}
				} else if(sNoEstatus.equals("4") || sNoEstatus.equals("11")) {
					if(Integer.parseInt(ic_if)!=0) {
						cont++;psF.setInt(cont, Integer.parseInt(ic_if));
					}
				}

			} else if(sOrigen.equals("EPOS_CADENA_NAFIN")) {
				//ISeleccionDocumento BeanSeleccionDocumento = ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

				String sFechaVencPyme = "";
				//if("9".equals(sNoEstatus)) {
				//	sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(sNoEPO);
				//}

				String numDocDisp = "COUNT (1)";
				String importeDisp = "d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)";


				if("9".equals(sNoEstatus)) {
					numDocDisp = "SUM (DECODE (doc.cs_dscto_especial, 'C', 0, 1))";
					importeDisp = "DECODE (doc.cs_dscto_especial, 'C', -1, 1) * ("+importeDisp+")";
				}


				qrySentenciaTotales =
					" SELECT "+numDocDisp+" totDocs, NVL (SUM (doc.fn_monto), 0) as totMonto"   +
					"   FROM com_documento doc"   +
					"  WHERE NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"	 AND doc.ic_epo = ?"   +
					"    AND doc.ic_estatus_docto = ?"   +
					"    AND doc.df_fecha_venc"+sFechaVencPyme+" >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND doc.df_fecha_venc"+sFechaVencPyme+" < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)" +
					"	  AND doc.ic_moneda = ? "  +
					"    AND doc.ic_documento = ? " ;

				if(sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1")) {
					if(Integer.parseInt(desarrollador)!=0)
						qrySentenciaTotales += "    AND doc.ic_pyme = ?";
					else if(Integer.parseInt(beneficiario)!=0)
						qrySentenciaTotales += "    AND doc.ic_beneficiario = ? ";
				} else if(sNoEstatus.equals("4") || sNoEstatus.equals("11")) {
					if(Integer.parseInt(ic_if)!=0)
						qrySentenciaTotales += "    AND doc.ic_if = ? ";
          qrySentenciaTotales += "    AND doc.cs_dscto_especial = 'N'";
				}
				// Debug info: start
				System.out.println(" folioFF 		  = <"+folioFF+">");
				System.out.println(" sNoEPO 		  = <"+sNoEPO+">");
				System.out.println(" sNoEstatus 	  = <"+sNoEstatus+">");
				System.out.println(" sFechaVenc 	  = <"+sFechaVenc+">");
				System.out.println(" sFechaVenc 	  = <"+sFechaVenc+">");
				System.out.println(" sMoneda 		  = <"+sMoneda+">");
				// Debug info: end


				psT = con.queryPrecompilado(qrySentenciaTotales);
				psT.setLong(	1, folioFF);
				psT.setInt(		2, Integer.parseInt(sNoEPO));
				psT.setInt(		3, Integer.parseInt(sNoEstatus));
				psT.setString(	4, sFechaVenc);
				psT.setString(	5, sFechaVenc);
				psT.setInt(	   6, Integer.parseInt(sMoneda));
				psT.setInt(    7, Integer.parseInt(icDocumentoCadenaNafin));
				cont = 7;
				if(sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1")) {
					if(Integer.parseInt(desarrollador)!=0) {
						cont++;psT.setInt(cont, Integer.parseInt(desarrollador));
						System.out.println(" desarrollador = <"+desarrollador+">");// Debug info
					} else if(Integer.parseInt(beneficiario)!=0) {
						cont++;psT.setInt(cont, Integer.parseInt(beneficiario));
						System.out.println(" beneficiario  = <"+beneficiario+">");// Debug info
					}
				} else if(sNoEstatus.equals("4") || sNoEstatus.equals("11")) {
					if(Integer.parseInt(ic_if)!=0) {
						cont++;psT.setInt(cont, Integer.parseInt(ic_if));
						System.out.println(" ic_if         = <"+ic_if+">");// Debug info
					}
				}

				qrySentenciaFolios =
					" SELECT "+folioFF+" as folio, doc.ic_documento "   +
					"   FROM com_documento doc"   +
					"  WHERE NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"	 AND doc.ic_epo = ?"   +
					"    AND doc.ic_estatus_docto = ?"   +
					"    AND doc.df_fecha_venc"+sFechaVencPyme+" >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND doc.df_fecha_venc"+sFechaVencPyme+" < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)" +
					"    AND doc.ic_moneda = ? "  +
					"	  AND doc.ic_documento = ? ";

				if(sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1")) {
					if(Integer.parseInt(desarrollador)!=0)
						qrySentenciaFolios += "    AND doc.ic_pyme = ?";
					else if(Integer.parseInt(beneficiario)!=0)
						qrySentenciaFolios += "    AND doc.ic_beneficiario = ? ";
				} else if(sNoEstatus.equals("4") || sNoEstatus.equals("11")) {
					if(Integer.parseInt(ic_if)!=0)
						qrySentenciaFolios += "    AND doc.ic_if = ? ";
          qrySentenciaFolios += "    AND doc.cs_dscto_especial = 'N'";
				}
				psF = con.queryPrecompilado(qrySentenciaFolios);
				psF.setLong(	1, folioFF);
				psF.setInt(		2, Integer.parseInt(sNoEPO));
				psF.setInt(		3, Integer.parseInt(sNoEstatus));
				psF.setString(	4, sFechaVenc);
				psF.setString(	5, sFechaVenc);
				psF.setInt(	6, Integer.parseInt(sMoneda));
				psF.setInt(	7, Integer.parseInt(icDocumentoCadenaNafin));
				cont = 7;
				if(sNoEstatus.equals("9")||sNoEstatus.equals("10")||sNoEstatus.equals("1")) {
					if(Integer.parseInt(desarrollador)!=0) {
						cont++;psF.setInt(cont, Integer.parseInt(desarrollador));
					} else if(Integer.parseInt(beneficiario)!=0) {
						cont++;psF.setInt(cont, Integer.parseInt(beneficiario));
					}
				} else if(sNoEstatus.equals("4") || sNoEstatus.equals("11")) {
					if(Integer.parseInt(ic_if)!=0) {
						cont++;psF.setInt(cont, Integer.parseInt(ic_if));
					}
				}

			} else if( sOrigen.equals("FISOS") ){
				qrySentenciaTotales =
					" SELECT COUNT (1) as totDocs, NVL (SUM (doc.fn_monto), 0) as totMonto"   +
					"   FROM com_documento doc, com_docto_seleccionado sel, com_solicitud sol"   +
					"  WHERE doc.ic_documento = sel.ic_documento"   +
					"    AND sel.ic_documento = sol.ic_documento"   +
					"    AND NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"    AND doc.ic_estatus_docto = 4"   +
					"    AND doc.ic_epo = ?"   +
					"    AND doc.ic_if = ?"   +
					"    AND sel.cs_opera_fiso = 'N' "  + // Documentos registrados bajo la modalidad normal (sin Fideicomiso para Desarrollo de Proveedores).
					"    AND sol.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND sol.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  +
					"    AND doc.ic_moneda = ?"   ;
				if(Integer.parseInt(ic_pyme)!=0)
					qrySentenciaTotales += "    AND doc.ic_pyme = ?";
				psT = con.queryPrecompilado(qrySentenciaTotales);
				psT.setLong(	1, folioFF);
				psT.setInt(		2, Integer.parseInt(sNoEPO));
				psT.setInt(		3, Integer.parseInt(sNoIF));
				psT.setString(	4, sFechaOperacion);
				psT.setString(	5, sFechaOperacion);
				psT.setInt(	6, Integer.parseInt(sMoneda));
				if(Integer.parseInt(ic_pyme)!=0)
					psT.setInt(7, Integer.parseInt(ic_pyme));

				qrySentenciaFolios =
					" SELECT "+folioFF+" as folio, doc.ic_documento "   +
					"   FROM com_documento doc, com_docto_seleccionado sel, com_solicitud sol"   +
					"  WHERE doc.ic_documento = sel.ic_documento"   +
					"    AND sel.ic_documento = sol.ic_documento"   +
					"    AND NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"    AND doc.ic_estatus_docto = 4"   +
					"    AND doc.ic_epo = ?"   +
					"    AND doc.ic_if = ?"   +
					"    AND sel.cs_opera_fiso = 'N' "  + // Documentos registrados bajo la modalidad normal (sin Fideicomiso para Desarrollo de Proveedores).
					"    AND sol.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND sol.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  +
					"    AND doc.ic_moneda = ?"   ;
				if(Integer.parseInt(ic_pyme)!=0)
					qrySentenciaFolios += "    AND doc.ic_pyme = ?";
				psF = con.queryPrecompilado(qrySentenciaFolios);
				psF.setLong(	1, folioFF);
				psF.setInt(		2, Integer.parseInt(sNoEPO));
				psF.setInt(		3, Integer.parseInt(sNoIF));
				psF.setString(	4, sFechaOperacion);
				psF.setString(	5, sFechaOperacion);
				psF.setInt(	6, Integer.parseInt(sMoneda));
				if(Integer.parseInt(ic_pyme)!=0)
					psF.setInt(7, Integer.parseInt(ic_pyme));
			} else if( sOrigen.equals("IFFIDEICOMISO") ){ // Dispersion FISOS ( IF FIDEICOMISO )
				qrySentenciaTotales =
					" SELECT COUNT (1) as totDocs, NVL (SUM (doc.fn_monto), 0) as totMonto"   +
					"   FROM com_documento doc, com_docto_seleccionado sel, com_solicitud sol"   +
					"  WHERE doc.ic_documento = sel.ic_documento"   +
					"    AND sel.ic_documento = sol.ic_documento"   +
					"    AND NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"    AND doc.ic_estatus_docto = 4"   +
					"    AND doc.ic_epo = ?"   +
					"    AND sel.ic_if = ?"   + // Intermediario Financiero que la PYME selecciono para cederle los documentos
					"    AND doc.ic_if = ?"   + // IF con quien se aplic� el Redescuento
					"    AND sel.cs_opera_fiso = 'S' "  + // Documentos registrados bajo la modalidad de Fideicomiso para Desarrollo de Proveedores
					"    AND sol.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND sol.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  +
					"    AND doc.ic_moneda = ?"   ;
				if(Integer.parseInt(ic_pyme)!=0)
					qrySentenciaTotales += "    AND doc.ic_pyme = ?";
				psT = con.queryPrecompilado(qrySentenciaTotales);
				psT.setLong(	1, folioFF);
				psT.setInt(		2, Integer.parseInt(sNoEPO));
				psT.setInt(		3, Integer.parseInt(sNoIF));
				psT.setInt(		4, Integer.parseInt(sNoIfFondeo));
				psT.setString(	5, sFechaOperacion);
				psT.setString(	6, sFechaOperacion);
				psT.setInt(	7, Integer.parseInt(sMoneda));
				if(Integer.parseInt(ic_pyme)!=0)
					psT.setInt(8, Integer.parseInt(ic_pyme));

				qrySentenciaFolios =
					" SELECT "+folioFF+" as folio, doc.ic_documento "   +
					"   FROM com_documento doc, com_docto_seleccionado sel, com_solicitud sol"   +
					"  WHERE doc.ic_documento = sel.ic_documento"   +
					"    AND sel.ic_documento = sol.ic_documento"   +
					"    AND NOT EXISTS(SELECT 1 "+
					"                     FROM comrel_documento_ff dff "+
					"                    WHERE doc.ic_documento = dff.ic_documento "+
					"                     AND dff.ic_producto_nafin = 1 "+
					"                     AND dff.ic_flujo_fondos = ?) "+
					"    AND doc.ic_estatus_docto = 4"   +
					"    AND doc.ic_epo = ?"   +
					"    AND sel.ic_if = ?"   + // Intermediario Financiero que la PYME selecciono para cederle los documentos
					"    AND doc.ic_if = ?"   + // IF con quien se aplic� el Redescuento
					"    AND sel.cs_opera_fiso = 'S' "  + // Documentos registrados bajo la modalidad de Fideicomiso para Desarrollo de Proveedores
					"    AND sol.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
					"    AND sol.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)"  +
					"    AND doc.ic_moneda = ?"   ;
				if(Integer.parseInt(ic_pyme)!=0)
					qrySentenciaFolios += "    AND doc.ic_pyme = ?";
				psF = con.queryPrecompilado(qrySentenciaFolios);
				psF.setLong(	1, folioFF);
				psF.setInt(		2, Integer.parseInt(sNoEPO));
				psF.setInt(		3, Integer.parseInt(sNoIF));
				psF.setInt(		4, Integer.parseInt(sNoIfFondeo));
				psF.setString(	5, sFechaOperacion);
				psF.setString(	6, sFechaOperacion);
				psF.setInt(	7, Integer.parseInt(sMoneda));
				if(Integer.parseInt(ic_pyme)!=0)
					psF.setInt(8, Integer.parseInt(ic_pyme));
			} //sOrigen

			System.out.println("sOrigen: "+sOrigen);
			System.out.println("qrySentenciaTotales: "+qrySentenciaTotales);
			rsT = psT.executeQuery();
			psT.clearParameters();
			if(rsT.next()){
				//System.out.println("totDoctos: "+totDoctos);
				//System.out.println("rsT.getInt(totDocs): "+rsT.getInt("totDocs"));
				//if(rst.getInt("totDocs")==totDoctos && rst.getDouble("totMonto")==importe)
				if(rsT.getInt("totDocs")==totDoctos)
					doctosOK = true;
			}
			if(psT != null) psT.close();
			rsT.close();

			if(doctosOK) {
				System.out.println("qrySentenciaFolios: "+qrySentenciaFolios);
				rsF = psF.executeQuery();
				psF.clearParameters();
				while(rsF.next()) {
					qrySentenciaInsert =
						" INSERT INTO comrel_documento_ff"   +
						"             (ic_documento_ff, ic_flujo_fondos, ic_documento, ic_producto_nafin)"   +
						" VALUES "  +
						"				  (SEQ_COMREL_DOCUMENTO_FF.NEXTVAL, ?, ?, 1) ";
						//"         SELECT SEQ_COMREL_DOCUMENTO_FF.NEXTVAL, ?, ?, 1"   +
						//"           FROM comrel_documento_ff"  ;
					psI = con.queryPrecompilado(qrySentenciaInsert);
					psI.setLong(1, rsF.getLong("folio"));
					psI.setLong(2, rsF.getInt("ic_documento"));
					psI.executeUpdate();
					psI.close();
				}
				if(psF != null) psF.close();
				rsF.close();
			}//if(doctosOK)

		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Dispersion::setRelacionDoctosFF(Exception) "+e);
			throw new NafinException("DISP0010");
		} finally {
			System.out.println("Dispersion::setRelacionDoctosFF(S)");
		}
	}//setRelacionDoctosFF


	//	Hashtable getResumOperacErrorFF() -- realizado por HDG -- 28/08/2003 --

	public Hashtable getResumOperacErrorFF(String sTipo, String sTipoDispersion,
											String sFechaRegIni, String sFechaRegFin,
											String sViaLiq, String ic_epo,
											String ic_if) throws NafinException {

		System.out.println("\n sTipo: "+sTipo);
		System.out.println("\n sTipoDispersion: "+sTipoDispersion);
		System.out.println("\n sFechaRegIni: "+sFechaRegIni);
		System.out.println("\n sFechaRegFin: "+sFechaRegFin);
		System.out.println("\n sViaLiq: "+sViaLiq);
		System.out.println("\n ic_epo: "+ic_epo);
		System.out.println("\n ic_if: "+ic_if);

		Hashtable hOperaciones = new Hashtable();
		try {
			hOperaciones = getResumOperacErrorFF(sTipo, sTipoDispersion, sFechaRegIni, sFechaRegFin, sViaLiq, ic_epo, ic_if, "");
		} catch(NafinException ne) {
			 throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getResumOperacErrorFF(). "+e.getMessage());
		}
		return hOperaciones;
	}

  public Hashtable getResumOperacErrorFF(String sTipo, String sTipoDispersion,
											String sFechaRegIni, String sFechaRegFin,
											String sViaLiq, String ic_epo,
											String ic_if, String sEstatus) throws NafinException {
    return getResumOperacErrorFF(sTipo, sTipoDispersion, sFechaRegIni, sFechaRegFin, sViaLiq, ic_epo, ic_if, sEstatus, "");
  }

   /*
    * @note:   Los cambios realizados al query de este m�todo deber�n verse reflejados en la clase:
    *            clases_individuales/src/com/netro/dispersion/ConsultaResumenOperacionErrorFFON.java, que contiene
    *          la versi�n paginada de esta consulta.
    *
    * @since   F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 25/09/2013 04:46:55 p.m.
	 * @author  jshernandez
    *
    */
	public Hashtable getResumOperacErrorFF(String sTipo, String sTipoDispersion,
											String sFechaRegIni, String sFechaRegFin,
											String sViaLiq, String ic_epo,
											String ic_if, String sEstatus, String sEstatusFF) throws NafinException {

		System.out.println("\n sTipo: "+sTipo);
		System.out.println("\n sTipoDispersion: "+sTipoDispersion);
		System.out.println("\n sFechaRegIni: "+sFechaRegIni);
		System.out.println("\n sFechaRegFin: "+sFechaRegFin);
		System.out.println("\n sViaLiq: "+sViaLiq);
		System.out.println("\n ic_epo: "+ic_epo);
		System.out.println("\n ic_if: "+ic_if);

		AccesoDB con = new AccesoDB();
		Hashtable hOperaciones = new Hashtable();
		Vector vOperError = new Vector();
		Vector vTotales = new Vector();
		Vector vDatos = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		try {
			con.conexionDB();

			String cond = "";
			if(sTipoDispersion.equals("E")) {
				cond =
					"    AND (ff.df_fecha_venc IS NOT NULL OR ff.df_operacion IS NOT NULL)"   +
					"    AND ff.ic_estatus_docto IN (1, 4, 9, 10, 11)"  ;
			} else if(sTipoDispersion.equals("F")) {
				cond =
					"    AND (ff.ic_if IS NOT NULL AND ff.df_fecha_venc IS NULL)"  ;
			} else if(sTipoDispersion.equals("P") && "2".equals(sEstatus)) {
				cond =
					"    AND (    ff.ic_if IS NULL"   +
					"         AND ff.df_fecha_venc IS NOT NULL"   +
					"         AND ff.ic_estatus_docto = 2"   +
					"        )"  ;
			} else if(sTipoDispersion.equals("P") && "PA".equals(sEstatus)) {
				cond =
					"    AND (    ff.ic_if IS NULL"   +
					"         AND ff.df_operacion IS NOT NULL"   +
					"         AND ff.ic_estatus_docto IS NULL"   +
					"        )"  ;
			}

			if(!sFechaRegIni.equals("") && !sFechaRegFin.equals("")) {
				cond +=
					" and FF.df_registro >= TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') "+
					" and FF.df_registro < (TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') + 1) ";
			}
/*
			String cond = (sTipoDispersion.equals("E")?"and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?"and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
							((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");
*/

			String epoEspecifica = "";
			String ifEspecifica = "";

			if(!"".equals(ic_epo))
				epoEspecifica = " AND FF.ic_epo = "+ic_epo;
			else if(!"".equals(ic_if))
					ifEspecifica = " AND FF.ic_if = "+ic_if;

			String condEPOs =
   				"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   				"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   				"AND c.ic_producto_nafin  = 1 ";
			String condEPOsErr =
   				"AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
   				"AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') "+
   				"AND c.ic_tipo_cuenta (+) = 40 "+
   				"AND c.ic_producto_nafin (+) = 1 "+
   				"AND c.ic_moneda (+) = 1 ";

			String condIFs =
   				"AND crn.ic_nafin_electronico = c.ic_nafin_electronico "+
//   				"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   				"AND c.ic_producto_nafin  = 1 ";
			String condIFsErr =
   				"AND crn.ic_nafin_electronico(+) = c.ic_nafin_electronico "+
//   				"AND crn.ic_epo_pyme_if = ff.ic_pyme "+
   				"AND c.ic_tipo_cuenta (+) = 40 "+
   				"AND c.ic_producto_nafin (+) = 1 "+
   				"AND c.ic_moneda (+) = 1 ";

			String condPEMEXs =
   				"AND crn.ic_epo_pyme_if = ff.ic_epo "+
   				"AND crn.cg_tipo = 'E' "+
   				"AND c.ic_tipo_cuenta (+) = 40 "+
   				"AND c.ic_producto_nafin (+) = 4 "+
   				"AND c.ic_moneda (+) = 1 ";


			String cond2 = "";
			String cond2Err = "";
			if(sTipoDispersion.equals("E")){
				cond2 = " "+condEPOs+" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) ";
				cond2Err = " "+condEPOsErr+" and (FF.df_fecha_venc is not null or FF.df_operacion is not null) and ff.ic_estatus_docto in (1, 4, 9,10, 11) ";
			} else if(sTipoDispersion.equals("F")){
				cond2 = " "+condIFs+" and (FF.ic_if is not null and FF.df_fecha_venc is null) ";
				cond2Err = " "+condIFsErr+" and (FF.ic_if is not null and FF.df_fecha_venc is null) ";
			}else if(sTipoDispersion.equals("P") && "2".equals(sEstatus))
				cond2 = " "+condPEMEXs+" AND (ff.ic_if IS NULL AND ff.df_fecha_venc IS NOT NULL AND ff.ic_estatus_docto = 2) ";
			else if(sTipoDispersion.equals("P") && "PA".equals(sEstatus))
				cond2 = " "+condPEMEXs+" AND (ff.ic_if IS NULL AND ff.df_operacion IS NOT NULL AND ff.ic_estatus_docto IS NULL) ";

			if(!sFechaRegIni.equals("") && !sFechaRegFin.equals("")) {
				cond2 +=
					" and FF.df_registro >= TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') "+
					" and FF.df_registro < (TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy')+1) ";
				cond2Err +=
					" and FF.df_registro >= TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') "+
					" and FF.df_registro < (TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy')+1) ";
			}
/*
			String cond2 = (sTipoDispersion.equals("E")?condEPOs+" and (FF.df_fecha_venc is not null or FF.df_operacion is not null)":(sTipoDispersion.equals("F")?condIFs+" and (FF.ic_if is not null and FF.df_fecha_venc is null) ":""))+
							((!sFechaRegIni.equals("") && !sFechaRegFin.equals(""))?"and trunc(FF.df_registro) between TO_DATE('"+sFechaRegIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaRegFin+"','dd/mm/yyyy') ":"");

*/

			String condvia = "";
			if(!"".equals(sViaLiq)) {
				if("T".equals(sViaLiq))
					condvia += " and FF.cg_via_liquidacion = 'TEF' ";
				else if("S".equals(sViaLiq))
						condvia += " and FF.cg_via_liquidacion = 'SPEUA' ";
				else if("I".equals(sViaLiq))
						condvia += " and FF.cg_via_liquidacion = 'SPEI' ";
			}


			if(sTipo.equals("Monitor")) {
				query =
					" SELECT ffa.enproceso, ffb.aceptados + ffb2.aceptados, ffc.conerror"   +
					"   FROM (SELECT COUNT (1) AS enproceso"   +
					"           FROM int_flujo_fondos ff, cfe_m_encabezado e"   +
					"          WHERE ff.ic_flujo_fondos = e.idoperacion_cen_i"   +
					"            AND e.status_cen_i = 0"   +
					"            AND ff.cs_reproceso = 'N' "+cond+epoEspecifica+ifEspecifica+" ) ffa,"   +
					"        (SELECT COUNT (1) AS aceptados"   +
					"           FROM int_flujo_fondos ff, cfe_m_encabezado e"   +
					"          WHERE ff.ic_flujo_fondos = e.idoperacion_cen_i"   +
					"            AND e.status_cen_i >= 1"   +
					"            AND ff.cs_reproceso = 'N' "+cond+epoEspecifica+ifEspecifica+" ) ffb,"   +
					"        (SELECT COUNT (1) AS aceptados"   +
					"           FROM int_flujo_fondos ff"   +
					"          WHERE ff.cs_reproceso = 'N'"   +
					"            AND ff.cs_concluido = 'S' " +cond+epoEspecifica+ifEspecifica+" ) ffb2,"   +
					"        (SELECT SUM (conerror) conerror"   +
					"           FROM (SELECT COUNT (1) AS conerror"   +
					"                   FROM int_flujo_fondos ff, cfe_m_encabezado e"   +
					"                  WHERE ff.ic_flujo_fondos = e.idoperacion_cen_i"   +
					"                    AND e.status_cen_i = -1"   +
					"                    AND ff.cs_reproceso = 'N' "+cond+epoEspecifica+ifEspecifica+" "   +
					"                 UNION ALL"   +
					"                 SELECT COUNT (1) AS conerror"   +
					"                   FROM int_flujo_fondos_err ff, cfe_m_encabezado_err e"   +
					"                  WHERE ff.ic_flujo_fondos = e.idoperacion_cen_i"   +
					"                    AND e.status_cen_i IN (-20, -1, 42, 44) "   +
					"                    AND ff.cs_reproceso = 'N' "+cond+epoEspecifica+ifEspecifica+" )) ffc"  ;
				try {
					System.out.println("\n queryError: Monitor ::   "+query);
					ps = con.queryPrecompilado(query);
					rs = ps.executeQuery();
					if(rs.next()) {
						vTotales.addElement(rs.getString(1));
						vTotales.addElement(rs.getString(2));
						vTotales.addElement(rs.getString(3));
					}
					rs.close();
					ps.clearParameters();
				} catch(SQLException sqle) { sqle.printStackTrace(); throw new NafinException("DISP0012"); }
			}

			String condMonitor = "", condConsultaEpo = "";
			if(sTipo.equals("Monitor")) {
				condMonitor = "    AND ME.status_cen_i IN (-20, -1, 42, 44, 60, 9999) ";
			}
      if (!"".equals(sEstatusFF)){
				condConsultaEpo = "    AND ME.status_cen_i >= "+sEstatusFF+" ";
      }

			query =
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff btt)*/"   +
				"         e.cg_razon_social, i.cg_razon_social, p.cg_razon_social, p.cg_rfc,"   +
				"         c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, ff.fn_importe,"   +
				"         me.error_cen_s, ff.ic_flujo_fondos,"   +
				"         me.status_cen_i || ' ' || eff.cd_descripcion,"   +
				"         TO_CHAR (ff.df_registro, 'dd/mm/yyyy'), ff.cg_via_liquidacion,"   +
				"         c.ic_estatus_cecoban, c.ic_cuenta, ff.cs_reproceso, e.ic_epo,"   +
				"         ff.ic_pyme, ff.ic_if, ff.ic_estatus_docto, TO_CHAR (ff.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc,"   +
				"         TO_CHAR (ff.df_operacion, 'dd/mm/yyyy') as df_operacion, 'FFON' origen, me.status_cen_i, "   +
				"			 btef.cc_clave_dcat_s || ' - ' || btef.cd_descripcion as inst_cuenta_benef"	+
				"   FROM int_flujo_fondos ff,"   +
				"        cfe_m_encabezado me,"   +
				"        comrel_nafin crn,"   +
				"        comcat_epo e,"   +
				"        comcat_if i,"   +
				"        comcat_pyme p,"   +
				"        com_cuentas c,"   +
				"        comcat_estatus_ff eff,"   +
				"        comcat_bancos_tef btef"   +
				"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
				"    AND ff.ic_epo = e.ic_epo"   +
				"    AND ff.ic_if = i.ic_if(+)"   +
				"    AND ff.ic_pyme = p.ic_pyme(+)"   +
				"    AND ff.ic_cuenta = c.ic_cuenta"   +
				"    AND c.ic_bancos_tef = btef.ic_bancos_tef"   +
				"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
				//"    AND ff.cs_reproceso IN ('N', 'D')"   +
				"    AND ff.cs_reproceso ='N' "+
				"    AND ff.cs_concluido = 'N'"  +
				cond2 + condvia + epoEspecifica + ifEspecifica+condMonitor+condConsultaEpo+
				" UNION ALL "+
				" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/"   +
				"         e.cg_razon_social, i.cg_razon_social, p.cg_razon_social, p.cg_rfc,"   +
				"         c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, ff.fn_importe,"   +
				"         me.error_cen_s, ff.ic_flujo_fondos,"   +
				"         me.status_cen_i || ' ' || eff.cd_descripcion,"   +
				"         TO_CHAR (ff.df_registro, 'dd/mm/yyyy'), ff.cg_via_liquidacion,"   +
				"         c.ic_estatus_cecoban, c.ic_cuenta, ff.cs_reproceso, e.ic_epo,"   +
				"         ff.ic_pyme, ff.ic_if, ff.ic_estatus_docto, TO_CHAR (ff.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc,"   +
				"         TO_CHAR (ff.df_operacion, 'dd/mm/yyyy') as df_operacion, 'ERR' origen, me.status_cen_i, "   +
				"			 ' '  as inst_cuenta_benef"	+
				"   FROM int_flujo_fondos_err ff,"   +
				"        cfe_m_encabezado_err me,"   +
				"        comrel_nafin crn,"   +
				"        comcat_epo e,"   +
				"        comcat_if i,"   +
				"        comcat_pyme p,"   +
				"        com_cuentas c,"   +
				"        comcat_estatus_ff eff"   +
				"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
				"    AND ff.ic_epo = e.ic_epo"   +
				"    AND ff.ic_if = i.ic_if(+)"   +
				"    AND ff.ic_pyme = p.ic_pyme(+)"   +
				"    AND ff.ic_cuenta = c.ic_cuenta(+)"   +
				"    AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
				//"    AND ff.cs_reproceso IN ('N', 'D')"   +
				"    AND ff.cs_reproceso ='N' "+
				"    AND ff.cs_concluido = 'N'"  +
				cond2Err + condvia + epoEspecifica + ifEspecifica+condMonitor+condConsultaEpo;

			if(sTipo.equals("Consulta")) {
				query +=
					" UNION ALL"   +
					" SELECT /*+index(ff) use_nl(ff me e i p c eff)*/"   +
					" 		  e.cg_razon_social, i.cg_razon_social, p.cg_razon_social, p.cg_rfc,"   +
					" 		  c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, ff.fn_importe, NULL,"   +
					" 		  ff.ic_flujo_fondos, me.status_cen_i || ' ' || eff.cd_descripcion,"   +
					" 		  TO_CHAR (ff.df_registro, 'dd/mm/yyyy'), ff.cg_via_liquidacion,"   +
					" 		  c.ic_estatus_cecoban, c.ic_cuenta, ff.cs_reproceso, e.ic_epo,"   +
					" 		  ff.ic_pyme, ff.ic_if, ff.ic_estatus_docto, TO_CHAR (ff.df_fecha_venc, 'dd/mm/yyyy') as df_fecha_venc,"   +
					" 		  TO_CHAR (ff.df_operacion, 'dd/mm/yyyy') as df_operacion, 'FFON' origen, me.status_cen_i, "   +
					"			 ' '  as inst_cuenta_benef"	+
					"   FROM int_flujo_fondos ff,"   +
					" 		 cfe_m_encabezado me,"   +
					" 		 comcat_epo e,"   +
					" 		 comcat_if i,"   +
					" 		 comcat_pyme p,"   +
					" 		 com_cuentas c,"   +
					" 		 comcat_estatus_ff eff"   +
					"  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i"   +
					" 	AND ff.ic_epo = e.ic_epo"   +
					" 	AND ff.ic_if = i.ic_if(+)"   +
					" 	AND ff.ic_pyme = p.ic_pyme(+)"   +
					" 	AND ff.ic_cuenta = c.ic_cuenta(+)"   +
					" 	AND me.status_cen_i = eff.ic_estatus_ff(+)"   +
					" 	AND ff.cs_reproceso IN ('N', 'D')"   +
					" 	AND ff.cs_concluido = 'S'"  +
					cond + condvia + epoEspecifica + ifEspecifica + condConsultaEpo;
			}

			try {
				System.out.println("\n ***** \nquery: "+query);
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				while (rs.next()) {
					vDatos = new Vector();
/* 0*/				vDatos.add((rs.getString(1)==null?"":rs.getString(1)));
/* 1*/				vDatos.add((rs.getString(2)==null?"":rs.getString(2)));
/* 2*/				vDatos.add((rs.getString(3)==null?"":rs.getString(3)));
/* 3*/				vDatos.add((rs.getString(4)==null?"":rs.getString(4)));
/* 4*/				vDatos.add((rs.getString(5)==null?"":rs.getString(5)));
/* 5*/				vDatos.add((rs.getString(6)==null?"":rs.getString(6)));
/* 6*/				vDatos.add((rs.getString(7)==null?"":rs.getString(7)));
/* 7*/				vDatos.add((rs.getString(8)==null?"":rs.getString(8)));
/* 8*/				vDatos.add((rs.getString(9)==null?"":rs.getString(9)));
/* 9*/				vDatos.add((rs.getString(10)==null?"":rs.getString(10)));
/*10*/				vDatos.add((rs.getString(11)==null?"":rs.getString(11)));
/*11*/				vDatos.add((rs.getString(12)==null?"":rs.getString(12)));
/*12*/				vDatos.add((rs.getString(13)==null?"":rs.getString(13)));
/*13*/				vDatos.add((rs.getString(14)==null?"":rs.getString(14)));
/*14*/				vDatos.add((rs.getString(15)==null?"":rs.getString(15)));
/*15*/				vDatos.add((rs.getString(16)==null?"":rs.getString(16)));
/*16*/				vDatos.add((rs.getString("ic_epo")==null?"":rs.getString("ic_epo")));
/*17*/				vDatos.add((rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme")));
/*18*/				vDatos.add((rs.getString("ic_if")==null?"":rs.getString("ic_if")));
/*19*/				vDatos.add((rs.getString("ic_estatus_docto")==null?"":rs.getString("ic_estatus_docto")));
/*20*/				vDatos.add((rs.getString("df_fecha_venc")==null?"":rs.getString("df_fecha_venc")));
/*21*/				vDatos.add((rs.getString("df_operacion")==null?"":rs.getString("df_operacion")));
/*22*/				vDatos.add((rs.getString("origen")==null?"":rs.getString("origen")));
/*23*/				vDatos.add((rs.getString("status_cen_i")==null?"":rs.getString("status_cen_i")));
/*24*/				vDatos.add((rs.getString("inst_cuenta_benef")==null?"":rs.getString("inst_cuenta_benef")));
					vOperError.addElement(vDatos);
				}
				rs.close();
			} catch(SQLException sqle) { sqle.printStackTrace(); throw new NafinException("DISP0013"); }

			if(ps != null) ps.close();

			hOperaciones.put("OperError", vOperError);

		System.out.println("hOperaciones. "+hOperaciones);

			if(sTipo.equals("Monitor"))
				hOperaciones.put("Totales", vTotales);
		}
		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getResumOperacErrorFF(). "+e.getMessage());
		}
		finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return hOperaciones;
	}


	//	void actulizaEstatusFF() -- realizado por HDG -- 29/08/2003 --

	public void reprocesaOperacionFF(String sNoFlujoFondos[]) throws NafinException {
		System.out.println("DispersionEJB::reprocesaOperacionFF(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		boolean bOk = true;
		int iAfectados = 0;
		ResultSet rs = null;
		String query = "";
		try {
			con.conexionDB();
			try {
				for(int i=0; i<sNoFlujoFondos.length; i++){
					StringTokenizer st = new StringTokenizer(sNoFlujoFondos[i],"|");
					st.nextToken();
					String s_ic_flujo_fondos 	= st.nextToken();
					String s_ic_cuenta 			= st.nextToken();
					String s_tabla 				= st.nextToken();
					if("ERR".equals(s_tabla)) {
						s_tabla = "_ERR";
					}
					else {
						s_tabla = "";
					}

					ps = con.queryPrecompilado(
						" UPDATE int_flujo_fondos"+s_tabla+" "   +
						"    SET cs_reproceso = ?"   +
						"  WHERE ic_flujo_fondos = ?");
					ps.setString(1, "S");
					ps.setString(2, s_ic_flujo_fondos);
					iAfectados = ps.executeUpdate();
					ps.clearParameters();

          long 		folioFF		= getFolioFF();//rs.getLong("ic_flujo_fondos") + iNoReg;
          query =
            " INSERT INTO int_flujo_fondos"   +
            "             (ic_flujo_fondos, fn_importe, ig_total_documentos, df_fecha_venc,"   +
            "              ic_epo, ic_moneda, ic_pyme, ic_cuenta, ic_if, ic_estatus_docto,"   +
            "              df_operacion, ic_if_fondeo )"   +
            "    SELECT "+folioFF+", fn_importe, ig_total_documentos, df_fecha_venc, ic_epo,"   +
            "           ic_moneda, ic_pyme, "+s_ic_cuenta+", ic_if, ic_estatus_docto, df_operacion, "   +
            "           ic_if_fondeo "   +
            "      FROM int_flujo_fondos"+s_tabla+" "   +
            "     WHERE ic_flujo_fondos = "+s_ic_flujo_fondos;
          ps = con.queryPrecompilado(query);
          iAfectados = ps.executeUpdate();
          ps.clearParameters();

          query =
            " INSERT INTO cfe_m_encabezado"   +
            "             (idoperacion_cen_i, idsistema_cen_i, status_cen_i, error_cen_s,"   +
            "              historia_cen_s, idoperef_cen_i)"   +
            "    SELECT "+folioFF+", idsistema_cen_i, 0, NULL, historia_cen_s, "+s_ic_flujo_fondos+" "   +
            "      FROM cfe_m_encabezado"+s_tabla+" "   +
            "     WHERE idoperacion_cen_i = "+s_ic_flujo_fondos;
          ps = con.queryPrecompilado(query);
          iAfectados = ps.executeUpdate();
          ps.clearParameters();

					String sOrigen = "", sRazonSocial = "";
          String sNoEstatus = "", sNoMoneda = "", sNoEPO = "", sNoIF = "";
          boolean esEstatusOperada 				= false;
          boolean esEstatusOperadaPagada 		= false;
          boolean esEstatusVencidoSinOperar 	= false;
          boolean esEstatusPagadoSinOperar 	= false;

					query =
						" SELECT df_fecha_venc, df_operacion, ic_if, IC_ESTATUS_DOCTO, IC_MONEDA, IC_EPO, IC_IF"   +
						"   FROM int_flujo_fondos"   +
						"  WHERE ic_flujo_fondos = ?"  ;
					ps = con.queryPrecompilado(query);
					ps.setString(1, s_ic_flujo_fondos);
					rs = ps.executeQuery();
					if(rs.next()) {
						sOrigen = (rs.getString(1)!=null)?"EPOS":(rs.getString(2)!=null?"INFONAVIT":(rs.getString(3)!=null?"FISOS":""));
            sNoEstatus = (rs.getString(4)==null)?"":rs.getString(4);
            sNoMoneda = (rs.getString(5)==null)?"":rs.getString(5);
            sNoEPO = (rs.getString(6)==null)?"":rs.getString(6);
            sNoIF = (rs.getString(7)==null)?"":rs.getString(7);
            esEstatusOperada 				= sNoEstatus.equals("4")	?true:false;
            esEstatusOperadaPagada 		= sNoEstatus.equals("11")	?true:false;
            esEstatusVencidoSinOperar 	= sNoEstatus.equals("9")	?true:false;
            esEstatusPagadoSinOperar 	= sNoEstatus.equals("10")	?true:false;
          }

					rs.close();
					ps.clearParameters();

          String param2IdSistema 			= "200";
          String param3AreaCaptura 		= "";
          String param5TipoOper 			= "";
          String param8CveCteOrden 		= "";
          String param9RFCCteOrden 		= "";
          String param10NomCteOrden 		= "";
          String param11DomCteOrden 		= "";
          String param12Banco 			= "";
          String param13Plaza 			= "";
          String param14Sucursal 			= "";
          String param15NoCta 			= "";
          String param16y27TipoCta 		= "";
          String param17y28TipoDivisa 	= "";
          param3AreaCaptura 		= "EF15";

          if("1".equals(sNoMoneda)){

            param5TipoOper 			= "TEF";
            param8CveCteOrden 		= "37135";
            param9RFCCteOrden 		= "NFI3406305TO";
            param10NomCteOrden 		= "NACIONAL FINANCIERA, S.N.C.";
            param11DomCteOrden 		= "INSURGENTES SUR 1971 COL. GUADALUPE INN DEL. ALVARO OBREGON C.P. 01020";
            param12Banco 			= "37135";
            param13Plaza 			= "1001";
            param14Sucursal 			= "25";
            param15NoCta 			= "TEF";
            param16y27TipoCta 		= "CH";
            param17y28TipoDivisa 	= "MXN";
            //Modificado EGB Foda 051 - 2004
            if (sOrigen.equals("INFONAVIT")) {
              param3AreaCaptura = "EF02";
            } else if (sOrigen.equals("FISOS")){
              param3AreaCaptura = "EF04";
            }
				// Documento(s) en Dolares Americanos con estatus: Operada y Operada Pagada
          }else if( "54".equals(sNoMoneda) && (esEstatusOperada || esEstatusOperadaPagada) ){

            param5TipoOper 			= "MT202"; // Dispersion a Intermediario
            param8CveCteOrden 		= "37135";
            param9RFCCteOrden 		= "NFI3406305TO";
            param10NomCteOrden 		= "NACIONAL FINANCIERA, S.N.C.";
            param11DomCteOrden 		= "INSURGENTES SUR 1971 COL. GUADALUPE INN DEL. ALVARO OBREGON C.P. 01020";
            param12Banco 				= "BKTRUS33";
            param13Plaza 				= "US-306";
            param14Sucursal 			= "1";
            param15NoCta 				= "BK01";
            param16y27TipoCta 		= "CH";
            param17y28TipoDivisa 	= "USD";
          // Documento(s) en Dolares Americanos con estatus: Vencido Sin Operar y Pagado Sin Operar
          }else if(  "54".equals(sNoMoneda) &&  (esEstatusVencidoSinOperar || esEstatusPagadoSinOperar) ){
            // Obtener la Clave EPO FFON:
            String 	claveEpoFFON 	= getClaveEpoEnFFON(sNoEPO);
            HashMap 	datosEPO			= getDatosClienteOrdenante(sNoEPO);

            String   domicilio		= (String) datosEPO.get("DOMICILIO_EPO");
            domicilio					= domicilio.length() > 255?domicilio.substring(0,255):domicilio;

            param5TipoOper 			= "MT103"; 			// Dispersion a Proveedores
            param8CveCteOrden 		= claveEpoFFON;
            param9RFCCteOrden 		= (String) datosEPO.get("RFC_EPO");
            param10NomCteOrden 		= (String) datosEPO.get("RAZON_SOCIAL");
            param11DomCteOrden 		= domicilio;
            param12Banco 				= "BKTRUS33";
            param13Plaza 				= "US-306";
            param14Sucursal 			= "1";
            param15NoCta 				= "BK01";
            param16y27TipoCta 		= "CH";
            param17y28TipoDivisa 	= "USD";

          }

          sRazonSocial = getRazonSocial(sOrigen, sNoEPO, sNoIF, con);

					/*String campo = (sOrigen.equals("EPOS") || sOrigen.equals("INFONAVIT"))?"E.cg_razon_social ":"I.cg_razon_social ";
					String tabla = (sOrigen.equals("EPOS") || sOrigen.equals("INFONAVIT"))?"comcat_epo E ":"comcat_if I ";
					String condi = (sOrigen.equals("EPOS") || sOrigen.equals("INFONAVIT"))?"FF.ic_epo = E.ic_epo ":"FF.ic_if = I.ic_if ";
					query = "select "+campo+" from int_flujo_fondos FF, "+tabla+
							"where "+condi+
							"and FF.ic_flujo_fondos = ?";
					ps = con.queryPrecompilado(query);
					ps.setString(1, s_ic_flujo_fondos);
					rs = ps.executeQuery();
					if(rs.next())
						sRazonSocial = rs.getString(1);
					rs.close();
					ps.clearParameters();*/

					CallableStatement cs = con.ejecutaSP("SP_DETALLES_FF(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
					cs.setString( 1, getFechaSiguienteHabil());
					cs.setString( 2, param2IdSistema);
					cs.setString( 3, param3AreaCaptura);
					cs.setString( 4, param5TipoOper);
					cs.setString( 5, param8CveCteOrden);
					cs.setString( 6, param9RFCCteOrden);
					cs.setString( 7, param10NomCteOrden);
					cs.setString( 8, param11DomCteOrden);
					cs.setString( 9, param12Banco);
					cs.setString(10, param13Plaza);
					cs.setString(11, param14Sucursal);
					cs.setString(12, param15NoCta);
					cs.setString(13, param16y27TipoCta);
					cs.setString(14, param17y28TipoDivisa);
					cs.setString(15, sOrigen);
					cs.setString(16, sRazonSocial);

					cs.execute();
					cs.close();
				} // for
				if(ps != null) ps.close();
			} catch(SQLException sqle) {
				bOk = false;
				sqle.printStackTrace();
				throw new NafinException("DISP0014");
			}
		}
//		catch(NafinException ne) { throw ne; }
		catch(Exception e) {
			System.out.println("DispersionEJB::reprocesaOperacionFF(Exception ) "+e);
			bOk = false;
			e.printStackTrace();
			System.out.println("Exception en actulizaEstatusFF(). "+e.getMessage());
		}
		finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionEJB::reprocesaOperacionFF(S)");
		}
	}

	public void cancelarOperacionFF(String sNoFlujoFondos[],String usuario, String fcancelacion) throws NafinException {
	System.out.println("DispersionEJB::cancelarOperacionFF(E)");
	AccesoDB con = new AccesoDB();
	boolean bOk = true;
	int iAfectados = 0;
	String query = "";
	List lVarBind = new ArrayList();

	log.debug("usuario::    "+usuario);
	log.debug("fcancelacion::    "+fcancelacion);


		try {
			con.conexionDB();
			for(int i=0; i<sNoFlujoFondos.length; i++) {
				System.out.println("\n sNoFlujoFondos["+i+"]: "+sNoFlujoFondos[i]);
				StringTokenizer st = new StringTokenizer(sNoFlujoFondos[i],"|");
				String s_estat_cuenta 		= st.nextToken();
				String s_ic_flujo_fondos 	= st.nextToken();
				String s_ic_cuenta 			= st.nextToken();
				String s_tabla 				= st.nextToken();
				String s_fecha_reg			= st.nextToken();
				String s_causa 				= st.nextToken();

				log.debug("s_estat_cuenta::    "+s_estat_cuenta);
				log.debug("s_ic_flujo_fondos::    "+s_ic_flujo_fondos);
				log.debug("s_ic_cuenta::    "+s_ic_cuenta);
				log.debug("s_tabla::    "+s_tabla);
				log.debug("s_fecha_reg::    "+s_fecha_reg);
				log.debug("s_causa::    "+s_causa);


				if("ERR".equals(s_tabla)) {
					s_tabla = "_ERR";
				} else {
					s_tabla = "";
				}

				if("".equals(s_tabla)) {
					query =
						" DELETE "   +
						"   FROM cfe_d_edetalle"   +
						"  WHERE idoperacion_cen_i = ? ";
					lVarBind = new ArrayList();
					lVarBind.add(new Long(s_ic_flujo_fondos));
					iAfectados = con.ejecutaUpdateDB(query, lVarBind);
				}

				query =
					" UPDATE cfe_m_encabezado"+s_tabla+" "   +
					"    SET status_cen_i = ?,"   +
					"        error_cen_s = ?"   +
					"  WHERE idoperacion_cen_i = ?"  ;
				lVarBind = new ArrayList();
				lVarBind.add(new Integer(60));
				lVarBind.add(s_causa);
				lVarBind.add(new Long(s_ic_flujo_fondos));
				iAfectados = con.ejecutaUpdateDB(query, lVarBind);

				query =
					" UPDATE int_flujo_fondos"+s_tabla+" "   +
					"   SET CS_REPROCESO = ? "   +
					"      , cg_Usuario = ? "+
				   "     , df_cancelacion = SYSDATE "+
					"  WHERE ic_flujo_fondos = ? ";
				lVarBind = new ArrayList();
				lVarBind.add("D");
				lVarBind.add(usuario);
				lVarBind.add(new Long(s_ic_flujo_fondos));
				iAfectados = con.ejecutaUpdateDB(query, lVarBind);

			}//for(int i=0; i<sNoFlujoFondos.length; i++)

			log.debug("query::    "+query);

		} catch(Exception e) {
			System.out.println("DispersionEJB::cancelarOperacionFF(Exception) "+e.getMessage());
			e.printStackTrace();
			bOk = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionEJB::cancelarOperacionFF(S)");
		}
	}

	public void getOperacionesAceptadas() throws NafinException {
		AccesoDB con = new AccesoDB();
		String query = "";
		PreparedStatement ps = null;
		int iAfectados = 0;
		boolean bOk = true;
		try {
			con.conexionDB();
			query = "select ME.idoperacion_cen_i from cfe_m_encabezado ME, int_flujo_fondos FF "+
					"where ME.idoperacion_cen_i = FF.ic_flujo_fondos "+
					"and ME.status_cen_i >= 1 "+
					"and FF.cs_concluido = 'N' "+
					"and FF.cs_reproceso = 'N' ";
			try {
				ps = con.queryPrecompilado(query);
				ResultSet rs = ps.executeQuery();
				ps.clearParameters();

				while(rs.next()) {
					query = "update int_flujo_fondos set cs_concluido = 'S' "+
							"where ic_flujo_fondos = ? ";
					ps = con.queryPrecompilado(query);
					ps.setLong(1, rs.getLong(1));
					iAfectados += ps.executeUpdate();
				}
				if(ps != null) ps.close();
			} catch(SQLException sqle) {
				bOk = false;
				sqle.printStackTrace();
				System.out.println("Exception en getOperacionesAceptadas(). "+sqle.getMessage());
				throw new NafinException("SIST0001");
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getOperacionesAceptadas(). "+e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	}

	public int getEpoInfonavit() throws NafinException {
	AccesoDB con = new AccesoDB();
	PreparedStatement ps = null;
	ResultSet rs = null;
	int iEpoInfonavit = 0;
		try {
			con.conexionDB();
			String query = "select ic_epo, cg_razon_social, cg_nombre_comercial "+
							"from comcat_epo "+
							"where cg_razon_social like '%INSTITUTO%' "+
							"and cg_razon_social like '%FONDO%' "+
							"and cg_razon_social like '%NACIONAL%' "+
							"and cg_razon_social like '%VIVIENDA%' ";
			try {
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				if(rs.next()) {
					iEpoInfonavit = rs.getInt("ic_epo");
				} else {
					query = "select ic_epo, cg_razon_social from comcat_epo "+
							"where cg_nombre_comercial like 'INFONAVIT'";
					ps = con.queryPrecompilado(query);
					rs = ps.executeQuery();
					if(rs.next())
						iEpoInfonavit = rs.getInt("ic_epo");
					else
						iEpoInfonavit = 16;

					rs.close();
				}
				rs.close();
				if(ps!=null) ps.close();
			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DISP0015");
			}
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getEpoInfonavit(). "+e.getMessage());
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return iEpoInfonavit;
	}

	public String getIfNafin() throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String in = "";
		try {
			con.conexionDB();
			String query =
				" SELECT ic_if"   +
				"   FROM comcat_if"   +
				"  WHERE cg_razon_social LIKE '%NACIONAL%'"   +
				"    AND cg_razon_social LIKE '%FINANCIERA%'"   +
				"    AND ig_tipo_piso = 1"  ;
			try {
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				while(rs.next()) {
					if(!"".equals(in))
						in+=",";
					in += rs.getString("ic_if");
				}
				rs.close();
				if(ps!=null) ps.close();
			} catch(SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("DISP0015");
			}
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getIfNafin(). "+e.getMessage());
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return in;
	}

   //	Vector getOperacionesDispersadasFisos() -- realizado por HDG -- 24/11/2003 --
	public Vector getOperacionesDispersadasFisos(String sNoIf, String sNoEpo,
												String sFechaIni, String sFechaFin) throws NafinException{
    return getOperacionesDispersadasFisos(sNoIf, sNoEpo, sFechaIni, sFechaFin, "1");
  }

  //FODEA 043-2009 INCORPORACION DEL MANEJO EN DOLARES
	public Vector getOperacionesDispersadasFisos(String sNoIf, String sNoEpo,
												String sFechaIni, String sFechaFin, String sIcMoneda) throws NafinException{
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	Vector vOperDisp = new Vector();
	Vector vDatos = null;
		try {
			con.conexionDB();
			String lsQuery= "select /*+ use_nl(fac) */ FAC.df_registro, sum(totalOper), sum(montoOper), count(FAC.PYME), sum(ig_total_documentos), sum(fn_importe) "+
							"from ( "+
								"select /*+ use_nl(ff) */ TO_CHAR(OPER.df_registro,'dd/mm/yyyy') as df_registro, OPER.totalOper, OPER.montoOper, "+
								"FF.ig_total_documentos, FF.fn_importe, decode(FF.ig_total_documentos,null,null,OPER.ic_pyme) as PYME "+
								"from int_flujo_fondos FF, "+
									"(select /*+ USE_NL(P)*/ trunc(S.df_fecha_solicitud) as df_registro, count(DS.ic_documento) as totalOper, "+
									"sum(DS.in_importe_recibir) as montoOper, D.ic_epo, D.ic_if, D.ic_pyme "+
									"from com_documento D, com_docto_seleccionado DS, com_solicitud S, comcat_pyme P "+
									"where DS.ic_documento = D.ic_documento "+
									"and DS.ic_documento = S.ic_documento "+
									"and D.ic_epo = DS.ic_epo "+
									"and D.ic_pyme = P.ic_pyme "+
									"and DS.ic_if = "+sNoIf+
									" and DS.ic_epo = "+sNoEpo+
									" and D.ic_moneda =  "+ sIcMoneda +
									"and D.ic_estatus_docto in (4, 11) "+
									"and trunc(S.df_fecha_solicitud) between TO_DATE('"+sFechaIni+"','dd/mm/yyyy') and TO_DATE('"+sFechaFin+"','dd/mm/yyyy') "+
									"group by trunc(S.df_fecha_solicitud), D.ic_epo, D.ic_if, D.ic_pyme "+
									"order by 1) OPER "+
								"where FF.ic_epo(+) = OPER.ic_epo "+
								"and FF.ic_if(+) = OPER.ic_if "+
								"and FF.ic_pyme(+) = OPER.ic_pyme "+
								"and trunc(FF.df_registro(+)) = trunc(OPER.df_registro) "+
								"and FF.ic_moneda(+) =  "+ sIcMoneda +
								"order by OPER.df_registro "+
							") FAC "+
							"group by FAC.df_registro ";
			try {
				rs = con.queryDB(lsQuery);
				while(rs.next()) {
					vDatos = new Vector();
					vDatos.addElement((rs.getString(1)==null?"":rs.getString(1)));
					vDatos.addElement((rs.getString(2)==null?"0":rs.getString(2)));
					vDatos.addElement((rs.getString(3)==null?"0":rs.getString(3)));
					vDatos.addElement((rs.getString(4)==null?"0":rs.getString(4)));
					vDatos.addElement((rs.getString(5)==null?"0":rs.getString(5)));
					vDatos.addElement((rs.getString(6)==null?"0":rs.getString(6)));
					vOperDisp.addElement(vDatos);
				}
				rs.close();
				con.cierraStatement();
			} catch (SQLException sqle){
				sqle.printStackTrace();
				throw new NafinException("DISP0016");
			}

		} catch (NafinException ne){
			throw ne;
		} catch (Exception e){
			e.printStackTrace();
			System.out.println("Exception en getOperacionesDispersadasFisos(). "+e.getMessage());
		} finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return vOperDisp;
	}

	public Hashtable getOperacionesDispersadasEpos(String sNoEpo, String sFechaIni, String sFechaFin) throws NafinException {
		Hashtable hDatosFactura = new Hashtable();
		try {
			hDatosFactura = getOperacionesDispersadasEpos(sNoEpo, sFechaIni, sFechaFin, "", "");
		} catch (NafinException ne){
			throw ne;
		} catch (Exception e){
			e.printStackTrace();
			System.out.println("Exception en getOperacionesDispersadasEpos(). "+e.getMessage());
		}
		return hDatosFactura;
	}

	// Hashtable getOperacionesDispersadasEpos() -- realizado por HDG -- 28/11/2003 --
	public Hashtable getOperacionesDispersadasEpos(String sNoEpo, String sFechaIni, String sFechaFin, String inicio, String fin) throws NafinException{
		AccesoDB con = new AccesoDB();
		String lsQuery = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		Hashtable hDatosFactura = new Hashtable();
		Vector vOperyOperPag = new Vector();
		Vector vVencSinOperSumRes = new Vector();
		Vector vVencSinOperSum = new Vector();
		Vector vDatos = null;
		try {
			con.conexionDB();
			// Obtiene los Documentos Operados y Operados Pagados Dispersados para la EPO.
			if("".equals(inicio))
				inicio = "15";
			if("".equals(fin))
				fin = "29";

			lsQuery =
				" SELECT   v.cg_razon_social, SUM (v.ig_total_documentos), SUM (v.fn_importe)"   +
				"     FROM (SELECT i.cg_razon_social, ff.ig_total_documentos, ff.fn_importe"   +
				"             FROM int_flujo_fondos ff, comcat_if i"   +
				"            WHERE ff.ic_if = i.ic_if"   +
				"              AND ff.ic_epo = ?"   +
				"              AND ff.ic_estatus_docto IN (4, 11)"   +
				"					AND ff.ic_moneda = 1 "  +
				"              AND ff.cs_reproceso = 'N'"   +
				"              AND ff.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"              AND ff.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + 1)) v "   +
				" GROUP BY v.cg_razon_social"   +
				" ORDER BY v.cg_razon_social"  ;

				System.out.println("getOperacionesDispersadasEpos:=== "+lsQuery);
				System.out.println("sNoEpo= "+sNoEpo+"  sFechaIni= "+sFechaIni+"  sFechaFin= "+sFechaFin);



			try {
				ps = con.queryPrecompilado(lsQuery);
				ps.setInt(1, Integer.parseInt(sNoEpo));
				ps.setString(2, sFechaIni);
				ps.setString(3, sFechaFin);
				rs = ps.executeQuery();
				ps.clearParameters();
				while(rs.next()) {
					vDatos = new Vector();
					vDatos.addElement((rs.getString(1)==null?"":rs.getString(1)));
					vDatos.addElement((rs.getString(2)==null?"0":rs.getString(2)));
					vDatos.addElement((rs.getString(3)==null?"0":rs.getString(3)));
					vOperyOperPag.addElement(vDatos);
				}
				rs.close();
				ps.close();
			} catch (SQLException sqle){
				System.out.println("Problemas al generar la factura EPOS, Doctos Operados: "+sqle.getMessage());
				sqle.printStackTrace();
				throw new NafinException("DISP0016");
			}

			// Obtiene los Documentos Vencidos sin Operar Dispersados para la EPO.
			/* Obtenemos la sumatoria de acuerdo a la misma Fecha de Vencimiento, Pyme y Periodo de Vigencia
			 	El Grupo 1 va de (1 a 15 d�as de Vigencia)
			 	El Grupo 2 va de (16 a 29 d�as de Vigencia)
			 	El Grupo 3 va de (30 en adelante)			 	*/
			 lsQuery =
				" SELECT   /*+leading(d)*/"   +
				"           doctos.df_fecha_venc,"   +
				"           'P' || doctos.ic_pyme prov_benef, SUM (total_importe) total_importe,"   +
				"           grupo "   +
				"     FROM (SELECT  "   +
				"                     TRUNC (d.df_fecha_venc) df_fecha_venc, d.ic_pyme,"   +
				"                     SUM (DECODE (d.cs_dscto_especial, 'C', -1, 1) * (d.fn_monto"   +
				"                        - NVL (("   +
				"                              d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100"   +
				"                             ),"   +
				"                             0))) total_importe,"   +
				"                     d.ic_epo, d.ic_moneda, d.ig_numero_docto,"   +
				"                     CASE"   +
				"                        WHEN ((d.df_fecha_venc - d.df_alta) < ?)"   +
				"                           THEN 1"   +
				"                        ELSE CASE"   +
				"                        WHEN     ((d.df_fecha_venc - d.df_alta) >= ?)"   +
				"                             AND ((d.df_fecha_venc - d.df_alta) < ?)"   +
				"                           THEN 2"   +
				"                        ELSE 3"   +
				"                     END"   +
				"                     END AS grupo"   +
				"               FROM com_documento d"   +
				"              WHERE d.ic_moneda = 1"   +
				"                AND d.ic_epo = ?"   +
				"                AND d.ic_estatus_docto IN (9, 10)"   +
				"                AND d.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"                AND d.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
				"                AND ("   +
				"                        ("   +
				"                             d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
				"                         AND   d.fn_monto"   +
				"                             - ("   +
				"                                d.fn_monto * NVL (d.fn_porc_beneficiario, 0)"   +
				"                                / 100"   +
				"                               ) > 0"   +
				"                        )"   +
				"                     OR d.cs_dscto_especial IN ("+dispersionTradicional+",'C')"   +
				"                    )"   +
				"           GROUP BY d.df_fecha_venc,"   +
				"                    d.ic_pyme,"   +
				"                     d.df_fecha_venc - d.df_alta,"   +
				"                    d.ic_epo,"   +
				"                    d.ic_moneda,"   +
				"                    d.ig_numero_docto) doctos,"   +
				"          int_flujo_fondos ff,"   +
				"          comrel_nafin n,"   +
				"          com_cuentas c,"   +
				"          cfe_m_encabezado cfe"   +
				"    WHERE doctos.ic_pyme = n.ic_epo_pyme_if"   +
				"      AND doctos.ic_epo = ff.ic_epo"   +
				"      AND doctos.ic_moneda = ff.ic_moneda"   +
				"      AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"      AND ff.df_fecha_venc >= doctos.df_fecha_venc"   +
				"      AND ff.df_fecha_venc < (doctos.df_fecha_venc + 1)"   +
				"      AND ff.ic_flujo_fondos = cfe.idoperacion_cen_i"   +
				"      AND c.ic_cuenta = ff.ic_cuenta"   +
				"      AND n.cg_tipo = 'P'"   +
				"      AND c.ic_tipo_cuenta = 40"   +
				"      AND c.ic_producto_nafin = 1"   +
				"      AND cfe.status_cen_i > 0"   +
				" GROUP BY doctos.df_fecha_venc, 'P' || doctos.ic_pyme, doctos.grupo  , doctos.grupo "   +
				" UNION ALL"   +
				" SELECT   /*+leading(d)*/"   +
				"           doctos.df_fecha_venc,"   +
				"           'I' || doctos.ic_beneficiario prov_benef,"   +
				"           SUM (total_importe) total_importe, doctos.grupo "   +
				"     FROM (SELECT   "   +
				"                     TRUNC (d.df_fecha_venc) df_fecha_venc, d.ic_beneficiario,"   +
				"                     SUM (d.fn_monto"   +
				"                        * NVL (d.fn_porc_beneficiario, 0)"   +
				"                        / 100) total_importe,"   +
				"                     d.ic_epo, d.ic_moneda, d.ig_numero_docto,"   +
				"                     CASE"   +
				"                        WHEN ((d.df_fecha_venc - d.df_alta) < ?)"   +
				"                           THEN 1"   +
				"                        ELSE CASE"   +
				"                        WHEN     ((d.df_fecha_venc - d.df_alta) >= ?)"   +
				"                             AND ((d.df_fecha_venc - d.df_alta) < ?)"   +
				"                           THEN 2"   +
				"                        ELSE 3"   +
				"                     END"   +
				"                     END AS grupo"   +
				"               FROM com_documento d"   +
				"              WHERE d.ic_moneda = 1"   +
				"                AND d.ic_epo = ?"   +
				"                AND d.ic_estatus_docto IN (9, 10)"   +
				"                AND d.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"                AND d.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
				"                AND d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
				"                AND (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0"   +
				"           GROUP BY d.df_fecha_venc,"   +
				"                    d.ic_beneficiario,"   +
				"                     d.df_fecha_venc - d.df_alta,"   +
				"                    d.ic_epo,"   +
				"                    d.ic_moneda,"   +
				"                    d.ig_numero_docto ) doctos,"   +
				"          int_flujo_fondos ff,"   +
				"          comrel_nafin n,"   +
				"          com_cuentas c,"   +
				"          cfe_m_encabezado cfe"   +
				"    WHERE doctos.ic_beneficiario = n.ic_epo_pyme_if"   +
				"      AND doctos.ic_epo = ff.ic_epo"   +
				"      AND doctos.ic_moneda = ff.ic_moneda"   +
				"      AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"      AND ff.df_fecha_venc >= doctos.df_fecha_venc"   +
				"      AND ff.df_fecha_venc < (doctos.df_fecha_venc + 1)"   +
				"      AND ff.ic_flujo_fondos = cfe.idoperacion_cen_i"   +
				"      AND c.ic_cuenta = ff.ic_cuenta"   +
				"      AND n.cg_tipo = 'I'"   +
				"      AND c.ic_tipo_cuenta = 40"   +
				"      AND c.ic_producto_nafin = 1"   +
				"      AND cfe.status_cen_i > 0"   +
				" GROUP BY doctos.df_fecha_venc, 'I' || doctos.ic_beneficiario, doctos.grupo  "  ;

			 lsQuery =
				" SELECT   TO_CHAR (df_fecha_venc, 'dd/mm/yyyy'), COUNT (DISTINCT (prov_benef)), SUM (total_importe), grupo"   +
				"     FROM ("+lsQuery+")"   +

				"  where grupo < = 2  "+
				" GROUP BY TO_CHAR (df_fecha_venc, 'dd/mm/yyyy'), grupo "  ;

			try {
				int iReg = 0;
				String sFechaAnt = "", sFechaAct = "";
				System.out.println("================== ");
				System.out.println("lsQuery: "+lsQuery);
				System.out.println("inicio: "+inicio);
				System.out.println("fin: "+fin);
				System.out.println("sNoEpo: "+sNoEpo);
				System.out.println("sFechaIni: "+sFechaIni);
				System.out.println("sFechaFin: "+sFechaFin);
				System.out.println("================== ");
				ps = con.queryPrecompilado(lsQuery);
				ps.setInt(1, Integer.parseInt(inicio));
				ps.setInt(2, Integer.parseInt(inicio));
				ps.setInt(3, Integer.parseInt(fin));
				ps.setInt(4, Integer.parseInt(sNoEpo));
				ps.setString(5, sFechaIni);
				ps.setString(6, sFechaFin);
				ps.setInt(7, Integer.parseInt(inicio));
				ps.setInt(8, Integer.parseInt(inicio));
				ps.setInt(9, Integer.parseInt(fin));
				ps.setInt(10, Integer.parseInt(sNoEpo));
				ps.setString(11, sFechaIni);
				ps.setString(12, sFechaFin);

				rs = ps.executeQuery();
				ps.clearParameters();
				while(rs.next()) {
					sFechaAct = rs.getString(1);
					if(sFechaAnt.equals(""))
						sFechaAnt = rs.getString(1);

					if(sFechaAct.equals(sFechaAnt)) {
						vDatos = new Vector();
						vDatos.addElement((rs.getString(2)==null?"0":rs.getString(2)));
						vDatos.addElement((rs.getString(3)==null?"0":rs.getString(3)));
						vDatos.addElement((rs.getString(4)==null?"":rs.getString(4)));
						vVencSinOperSum.addElement(vDatos);
					} else {
						vVencSinOperSum.add(0, sFechaAnt);
						vVencSinOperSumRes.addElement(vVencSinOperSum.clone());
						vVencSinOperSum.clear();
						// Cuando cambia el registro se guarda el nuevo.
						vDatos = new Vector();
						vDatos.addElement((rs.getString(2)==null?"0":rs.getString(2)));
						vDatos.addElement((rs.getString(3)==null?"0":rs.getString(3)));
						vDatos.addElement((rs.getString(4)==null?"":rs.getString(4)));
						vVencSinOperSum.addElement(vDatos);
					}
					sFechaAnt = rs.getString(1);
					iReg++;
				}
				// Guarda el ultimo registro.
				if(iReg>0) {
					vVencSinOperSum.add(0, sFechaAnt);
					vVencSinOperSumRes.addElement(vVencSinOperSum);
				}
				rs.close();
				ps.close();
			} catch (SQLException sqle){
				System.out.println("Problemas al generar la factura EPOS, Vencidos sin Operar Sumatoria: "+sqle.getMessage());
				sqle.printStackTrace();
				throw new NafinException("DISP0016");
			}

		} catch (NafinException ne){
			throw ne;
		} catch (Exception e){
			e.printStackTrace();
			System.out.println("Exception en getOperacionesDispersadasEpos(). "+e.getMessage());
		} finally {
			hDatosFactura.put("vOperyOperPag",vOperyOperPag);
			hDatosFactura.put("vVencSinOperSumRes",vVencSinOperSumRes);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return hDatosFactura;
	}

	// Hashtable getOperacionesDispersadasEposUSD() -- 26/10/2009 --
	public Hashtable getOperacionesDispersadasEposUSD(String sNoEpo, String sFechaIni, String sFechaFin, String inicio, String fin) throws NafinException{

		AccesoDB con = new AccesoDB();

		String 				lsQuery 			= "";
		PreparedStatement ps 				= null;
		ResultSet 			rs 				= null;
		Hashtable 			hDatosFactura 	= new Hashtable();

		Vector vOperyOperPagUSD 		= new Vector();
		Vector vVencSinOperSumResUSD 	= new Vector();
		Vector vVencSinOperSumUSD 		= new Vector();

		Vector vDatos 						= null;

		try {
			con.conexionDB();
			// Obtiene los Documentos Operados y Operados Pagados Dispersados para la EPO.
			if("".equals(inicio))
				inicio = "15";
			if("".equals(fin))
				fin = "29";

			// Obtener Detalle de Documentos Operados en USD
			lsQuery =
				" SELECT   v.cg_razon_social, SUM (v.ig_total_documentos), SUM (v.fn_importe)"   +
				"     FROM (SELECT i.cg_razon_social, ff.ig_total_documentos, ff.fn_importe"   +
				"             FROM int_flujo_fondos ff, comcat_if i"   +
				"            WHERE ff.ic_if = i.ic_if"   +
				"              AND ff.ic_epo = ?"   +
				"              AND ff.ic_estatus_docto IN (4, 11)"   +
				"					AND ff.ic_moneda = 54 "  +
				"              AND ff.cs_reproceso = 'N'"   +
				"              AND ff.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"              AND ff.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + 1)) v "   +
				" GROUP BY v.cg_razon_social"   +
				" ORDER BY v.cg_razon_social"  ;
			try {

				System.out.println("getOperacionesDispersadasEposUSD=== "+lsQuery);
				System.out.println("sNoEpo= "+sNoEpo+"  sFechaIni= "+sFechaIni+"  sFechaFin= "+sFechaFin);

				ps = con.queryPrecompilado(lsQuery);
				ps.setInt(1, Integer.parseInt(sNoEpo));
				ps.setString(2, sFechaIni);
				ps.setString(3, sFechaFin);
				rs = ps.executeQuery();
				ps.clearParameters();
				while(rs.next()) {
					vDatos = new Vector();
					vDatos.addElement((rs.getString(1)==null?"":rs.getString(1)));
					vDatos.addElement((rs.getString(2)==null?"0":rs.getString(2)));
					vDatos.addElement((rs.getString(3)==null?"0":rs.getString(3)));
					vOperyOperPagUSD.addElement(vDatos);
				}
				rs.close();
				ps.close();
			} catch (SQLException sqle){
				System.out.println("Problemas al generar la factura EPOS, Doctos Operados USD: "+sqle.getMessage());
				sqle.printStackTrace();
				throw new NafinException("DISP0016");
			}

			// Obtener los Documentos Vencidos sin Operar en Dolares Americanos Dispersados para la EPO.
			/* Obtenemos la sumatoria de acuerdo a la misma Fecha de Vencimiento, Pyme y Periodo de Vigencia
			 	El Grupo 1 va de (1 a 15 d�as de Vigencia)
			 	El Grupo 2 va de (16 a 29 d�as de Vigencia)
			 	El Grupo 3 va de (30 en adelante)			 	*/
			 lsQuery =
				" SELECT   /*+leading(d)*/"   +
				"           doctos.df_fecha_venc,"   +
				"           'P' || doctos.ic_pyme prov_benef, SUM (total_importe) total_importe,"   +
				"           grupo "   +
				"     FROM (SELECT  "   +
				"                     TRUNC (d.df_fecha_venc) df_fecha_venc, d.ic_pyme,"   +
				"                     SUM (DECODE (d.cs_dscto_especial, 'C', -1, 1) * (d.fn_monto"   +
				"                        - NVL (("   +
				"                              d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100"   +
				"                             ),"   +
				"                             0))) total_importe,"   +
				"                     d.ic_epo, d.ic_moneda, d.ig_numero_docto,"   +
				"                     CASE"   +
				"                        WHEN ((d.df_fecha_venc - d.df_alta) < ?)"   +
				"                           THEN 1"   +
				"                        ELSE CASE"   +
				"                        WHEN     ((d.df_fecha_venc - d.df_alta) >= ?)"   +
				"                             AND ((d.df_fecha_venc - d.df_alta) < ?)"   +
				"                           THEN 2"   +
				"                        ELSE 3"   +
				"                     END"   +
				"                     END AS grupo"   +
				"               FROM com_documento d"   +
				"              WHERE d.ic_moneda = 54"   +
				"                AND d.ic_epo = ?"   +
				"                AND d.ic_estatus_docto IN (9, 10)"   +
				"                AND d.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"                AND d.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
				"                AND ("   +
				"                        ("   +
				"                             d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
				"                         AND   d.fn_monto"   +
				"                             - ("   +
				"                                d.fn_monto * NVL (d.fn_porc_beneficiario, 0)"   +
				"                                / 100"   +
				"                               ) > 0"   +
				"                        )"   +
				"                     OR d.cs_dscto_especial IN ("+dispersionTradicional+",'C')"   +
				"                    )"   +
				"           GROUP BY d.df_fecha_venc,"   +
				"                    d.ic_pyme,"   +
				"                     d.df_fecha_venc - d.df_alta,"   +
				"                    d.ic_epo,"   +
				"                    d.ic_moneda,"   +
				"                    d.ig_numero_docto) doctos,"   +
				"          int_flujo_fondos ff,"   +
				"          comrel_nafin n,"   +
				"          com_cuentas c,"   +
				"          cfe_m_encabezado cfe"   +
				"    WHERE doctos.ic_pyme = n.ic_epo_pyme_if"   +
				"      AND doctos.ic_epo = ff.ic_epo"   +
				"      AND doctos.ic_moneda = ff.ic_moneda"   +
				"      AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"      AND ff.df_fecha_venc >= doctos.df_fecha_venc"   +
				"      AND ff.df_fecha_venc < (doctos.df_fecha_venc + 1)"   +
				"      AND ff.ic_flujo_fondos = cfe.idoperacion_cen_i"   +
				"      AND c.ic_cuenta = ff.ic_cuenta"   +
				"      AND n.cg_tipo = 'P'"   +
				"      AND c.ic_tipo_cuenta = " + claveCuentaSwift +
				"      AND c.ic_producto_nafin = 1"   +
				"      AND cfe.status_cen_i > 0"   +
				" GROUP BY doctos.df_fecha_venc, 'P' || doctos.ic_pyme, doctos.grupo "   +
				" UNION ALL"   +
				" SELECT   /*+leading(d)*/"   +
				"           doctos.df_fecha_venc,"   +
				"           'I' || doctos.ic_beneficiario prov_benef,"   +
				"           SUM (total_importe) total_importe,  doctos.grupo"   +
				"     FROM (SELECT   "   +
				"                     TRUNC (d.df_fecha_venc) df_fecha_venc, d.ic_beneficiario,"   +
				"                     SUM (d.fn_monto"   +
				"                        * NVL (d.fn_porc_beneficiario, 0)"   +
				"                        / 100) total_importe,"   +
				"                     d.ic_epo, d.ic_moneda, d.ig_numero_docto,"   +
				"                     CASE"   +
				"                        WHEN ((d.df_fecha_venc - d.df_alta) < ?)"   +
				"                           THEN 1"   +
				"                        ELSE CASE"   +
				"                        WHEN     ((d.df_fecha_venc - d.df_alta) >= ?)"   +
				"                             AND ((d.df_fecha_venc - d.df_alta) < ?)"   +
				"                           THEN 2"   +
				"                        ELSE 3"   +
				"                     END"   +
				"                     END AS grupo"   +
				"               FROM com_documento d"   +
				"              WHERE d.ic_moneda = 54"   +
				"                AND d.ic_epo = ?"   +
				"                AND d.ic_estatus_docto IN (9, 10)"   +
				"                AND d.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"                AND d.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
				"                AND d.cs_dscto_especial IN ("+dispersionDistribuido+")"   +
				"                AND (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0"   +
				"           GROUP BY d.df_fecha_venc,"   +
				"                    d.ic_beneficiario,"   +
				"                     d.df_fecha_venc - d.df_alta,"   +
				"                    d.ic_epo,"   +
				"                    d.ic_moneda,"   +
				"                    d.ig_numero_docto) doctos,"   +
				"          int_flujo_fondos ff,"   +
				"          comrel_nafin n,"   +
				"          com_cuentas c,"   +
				"          cfe_m_encabezado cfe"   +
				"    WHERE doctos.ic_beneficiario = n.ic_epo_pyme_if"   +
				"      AND doctos.ic_epo = ff.ic_epo"   +
				"      AND doctos.ic_moneda = ff.ic_moneda"   +
				"      AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"      AND ff.df_fecha_venc >= doctos.df_fecha_venc"   +
				"      AND ff.df_fecha_venc < (doctos.df_fecha_venc + 1)"   +
				"      AND ff.ic_flujo_fondos = cfe.idoperacion_cen_i"   +
				"      AND c.ic_cuenta = ff.ic_cuenta"   +
				"      AND n.cg_tipo = 'I'"   +
				"      AND c.ic_tipo_cuenta = "+ claveCuentaSwift +
				"      AND c.ic_producto_nafin = 1"   +
				"      AND cfe.status_cen_i > 0"   +
				" GROUP BY doctos.df_fecha_venc, 'I' || doctos.ic_beneficiario,  doctos.grupo "  ;


			 lsQuery =
				" SELECT   TO_CHAR (df_fecha_venc, 'dd/mm/yyyy'), COUNT (DISTINCT (prov_benef)), SUM (total_importe), grupo"   +
				"     FROM ("+lsQuery+")"   +
				"  where grupo < = 2  "+
				" GROUP BY TO_CHAR (df_fecha_venc, 'dd/mm/yyyy'), grupo "  ;

			try {
				int iReg = 0;
				String sFechaAnt = "", sFechaAct = "";
				System.out.println("lsQuery: "+lsQuery);
				System.out.println("inicio: "+inicio);
				System.out.println("fin: "+fin);
				System.out.println("sNoEpo: "+sNoEpo);
				System.out.println("sFechaIni: "+sFechaIni);
				System.out.println("sFechaFin: "+sFechaFin);
				ps = con.queryPrecompilado(lsQuery);
				ps.setInt(1, Integer.parseInt(inicio));
				ps.setInt(2, Integer.parseInt(inicio));
				ps.setInt(3, Integer.parseInt(fin));
				ps.setInt(4, Integer.parseInt(sNoEpo));
				ps.setString(5, sFechaIni);
				ps.setString(6, sFechaFin);
				ps.setInt(7, Integer.parseInt(inicio));
				ps.setInt(8, Integer.parseInt(inicio));
				ps.setInt(9, Integer.parseInt(fin));
				ps.setInt(10, Integer.parseInt(sNoEpo));
				ps.setString(11, sFechaIni);
				ps.setString(12, sFechaFin);

				rs = ps.executeQuery();
				ps.clearParameters();
				while(rs.next()) {
					sFechaAct = rs.getString(1);
					if(sFechaAnt.equals(""))
						sFechaAnt = rs.getString(1);

					if(sFechaAct.equals(sFechaAnt)) {
						vDatos = new Vector();
						vDatos.addElement((rs.getString(2)==null?"0":rs.getString(2)));
						vDatos.addElement((rs.getString(3)==null?"0":rs.getString(3)));
						vDatos.addElement((rs.getString(4)==null?"":rs.getString(4)));
						vVencSinOperSumUSD.addElement(vDatos);
					} else {
						vVencSinOperSumUSD.add(0, sFechaAnt);
						vVencSinOperSumResUSD.addElement(vVencSinOperSumUSD.clone());
						vVencSinOperSumUSD.clear();
						// Cuando cambia el registro se guarda el nuevo.
						vDatos = new Vector();
						vDatos.addElement((rs.getString(2)==null?"0":rs.getString(2)));
						vDatos.addElement((rs.getString(3)==null?"0":rs.getString(3)));
						vDatos.addElement((rs.getString(4)==null?"":rs.getString(4)));
						vVencSinOperSumUSD.addElement(vDatos);
					}
					sFechaAnt = rs.getString(1);
					iReg++;
				}
				// Guarda el ultimo registro.
				if(iReg>0) {
					vVencSinOperSumUSD.add(0, sFechaAnt);
					vVencSinOperSumResUSD.addElement(vVencSinOperSumUSD);
				}
				rs.close();
				ps.close();
			} catch (SQLException sqle){
				System.out.println("Problemas al generar la factura EPOS, Vencidos sin Operar USD Sumatoria: "+sqle.getMessage());
				sqle.printStackTrace();
				throw new NafinException("DISP0016");
			}

		} catch (NafinException ne){
			throw ne;
		} catch (Exception e){
			e.printStackTrace();
			System.out.println("Exception en getOperacionesDispersadasEposUSD(). "+e.getMessage());
		} finally {
			hDatosFactura.put("vOperyOperPagUSD",vOperyOperPagUSD);
			hDatosFactura.put("vVencSinOperSumResUSD",vVencSinOperSumResUSD);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return hDatosFactura;
	}


	/*******************IHJ***************/
	public int setParamGralesFF(String rad_tipo_epo, String rad_via_liq_epo, String rad_tipo_if, String rad_via_liq_if, String rad_tipo_info, String rad_via_liq_info)
			throws NafinException {
		System.out.println("Dispersion BEAN::setParamGralesFF (E)");
		AccesoDB con = new AccesoDB();
		String					qrySentencia	= "";
		PreparedStatement		pst				= null;
		boolean			resultado		= true;
		int registros = 0;
		try{
			con.conexionDB();
			for(int i=0;i<3;i++){
				registros = 0;
				qrySentencia =
					"UPDATE com_param_ff SET cg_tipo = ?, "+
					"    cg_via_liquidacion = ? "+
					"WHERE ic_param_ff = ? ";
				System.out.println("\nqrySentencia: "+qrySentencia);
				pst = con.queryPrecompilado(qrySentencia);
				switch(i){
					case 0:
						pst.setString(1,rad_tipo_epo);
						pst.setString(2,rad_via_liq_epo);
					break;
					case 1:
						pst.setString(1,rad_tipo_if);
						pst.setString(2,rad_via_liq_if);
					break;
					case 2:
						pst.setString(1,rad_tipo_info);
						pst.setString(2,rad_via_liq_info);
					break;
				}
				pst.setInt(3,i+1);
				registros = pst.executeUpdate();
				pst.close();
			}
		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		return registros;
	}


	public int setParamGralesDetalle(String tipo, String cmb_if, String cmb_epo, String rad_via_liq)
			throws NafinException {
		System.out.println("Dispersion BEAN::setParamGralesDetalle (E)");
		AccesoDB con = new AccesoDB();
		String					qrySentencia	= "";
		PreparedStatement		pst				= null;
		ResultSet				rs				= null;
		boolean			resultado		= true;
		int registros = 0;
		try{
			con.conexionDB();
			if("IF".equals(tipo) && !"".equals(cmb_if) && !"".equals(cmb_epo)) {
				qrySentencia =
					" SELECT COUNT (1)"   +
					"   FROM comrel_if_epo_x_producto"   +
					"  WHERE ic_producto_nafin = 1"   +
					"    AND ic_epo = ?"   +
					"    AND ic_if = ?"  ;
				System.out.println("\nqrySentencia: "+qrySentencia);
				pst = con.queryPrecompilado(qrySentencia);
				pst.setInt(1,Integer.parseInt(cmb_epo));
				pst.setInt(2,Integer.parseInt(cmb_if));
				rs = pst.executeQuery();
				if(rs.next())
					registros = rs.getInt(1);
				rs.close();pst.close();
				if(registros==0){
					qrySentencia =
						" INSERT INTO comrel_if_epo_x_producto"   +
						"             (ic_producto_nafin, ic_epo, ic_if, cg_via_liquidacion)"   +
						"      VALUES(1, ?, ?, ?)"  ;
					System.out.println("\nqrySentencia: "+qrySentencia);
					pst = con.queryPrecompilado(qrySentencia);
					pst.setInt(1,Integer.parseInt(cmb_epo));
					pst.setInt(2,Integer.parseInt(cmb_if));
					pst.setString(3,rad_via_liq);
					registros = pst.executeUpdate();
					pst.close();
				}
				else {
					qrySentencia =
						" UPDATE comrel_if_epo_x_producto"   +
						"    SET cg_via_liquidacion = ? "   +
						"  WHERE ic_producto_nafin = 1"   +
						"    AND ic_epo = ? "   +
						"    AND ic_if = ? "  ;
					System.out.println("\nqrySentencia: "+qrySentencia);
					pst = con.queryPrecompilado(qrySentencia);
					pst.setString(1,rad_via_liq);
					pst.setInt(2,Integer.parseInt(cmb_epo));
					pst.setInt(3,Integer.parseInt(cmb_if));
					registros = pst.executeUpdate();
					pst.close();
				}
			}
			else {
				if("EPO".equals(tipo) && !"".equals(cmb_epo)) {
					qrySentencia =
						"SELECT COUNT (1) "+
						"  FROM comrel_producto_epo "+
						" WHERE ic_producto_nafin = 1 "+
						"   AND ic_epo = ? ";
					System.out.println("\nqrySentencia: "+qrySentencia);
					pst = con.queryPrecompilado(qrySentencia);
					pst.setInt(1,Integer.parseInt(cmb_epo));
					rs = pst.executeQuery();
					if(rs.next())
						registros = rs.getInt(1);
					rs.close();pst.close();
					if(registros==0){
						qrySentencia =
							" INSERT INTO comrel_producto_epo"   +
							"             (ic_producto_nafin, ic_epo, cg_via_liquidacion)"   +
							"      VALUES(1, ?, ?)"  ;
						pst = con.queryPrecompilado(qrySentencia);
						pst.setInt(1,Integer.parseInt(cmb_epo));
						pst.setString(2,rad_via_liq);
						registros = pst.executeUpdate();
						pst.close();
					}
					else {
						qrySentencia =
							" UPDATE comrel_producto_epo"   +
							"    SET cg_via_liquidacion = ? "   +
							"  WHERE ic_producto_nafin = 1"   +
							"    AND ic_epo = ? "  ;
						System.out.println("\nqrySentencia: "+qrySentencia);
						pst = con.queryPrecompilado(qrySentencia);
						pst.setString(1,rad_via_liq);
						pst.setInt(2,Integer.parseInt(cmb_epo));
						registros = pst.executeUpdate();
						pst.close();
					}
				}
			}
		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		return registros;
	}

public int setParamDetalleEliminar(String tipo, String cmb_if, String epo, String rad_via_liq)
			throws NafinException {
		System.out.println("Dispersion BEAN::setParamDetalleEliminar (E)");
		AccesoDB con = new AccesoDB();
		String					qrySentencia	= "";
		int						ic_if			= 0;
		int						ic_epo			= 0;
		PreparedStatement		pst				= null;
		ResultSet				rs				= null;
		boolean			resultado		= true;
		int registros = 0;
		try{
			con.conexionDB();

			System.out.println("desc_if: " + cmb_if);
			System.out.println("desc_epo: " + epo);
			System.out.println("via: " + rad_via_liq);

			if("IF".equals(tipo)) {
			qrySentencia =  " SELECT IC_IF " +
							" FROM COMCAT_IF " +
							" WHERE CG_RAZON_SOCIAL = ? ";

					System.out.println("\nqrySentencia: "+qrySentencia);
					pst = con.queryPrecompilado(qrySentencia);
					pst.setString(1,cmb_if);
					rs = pst.executeQuery();
					if(rs.next())
						ic_if = rs.getInt(1);
					rs.close();pst.close();
			}

			qrySentencia =  " SELECT IC_EPO " +
							" FROM COMCAT_EPO " +
							" WHERE CG_RAZON_SOCIAL = ? ";

					System.out.println("\nqrySentencia: "+qrySentencia);
					pst = con.queryPrecompilado(qrySentencia);
					pst.setString(1,epo);
					rs = pst.executeQuery();
					if(rs.next())
						ic_epo = rs.getInt(1);
					rs.close();pst.close();

			System.out.println("ic_if: " + ic_if);
			System.out.println("ic_epo: " + ic_epo);

			if("IF".equals(tipo)) {
					qrySentencia =
						" UPDATE comrel_if_epo_x_producto"   +
						"    SET cg_via_liquidacion = null "   +
						"  WHERE ic_producto_nafin = 1"   +
						"    AND ic_epo = ? "   +
						"    AND ic_if = ? "  +
						"    AND cg_via_liquidacion = ? "  ;
					System.out.println("\nqrySentencia: "+qrySentencia);
					pst = con.queryPrecompilado(qrySentencia);
					pst.setInt(1,ic_epo);
					pst.setInt(2,ic_if);
					pst.setString(3,rad_via_liq);
					registros = pst.executeUpdate();
					pst.close();
			}
			else {
				if("EPO".equals(tipo)) {
						qrySentencia =
							" UPDATE comrel_producto_epo"   +
							"    SET cg_via_liquidacion = null "   +
							"  WHERE ic_producto_nafin = 1"   +
							"    AND ic_epo = ? "  +
							"    AND cg_via_liquidacion = ? "  ;
						System.out.println("\nqrySentencia: "+qrySentencia);
						pst = con.queryPrecompilado(qrySentencia);
					pst.setInt(1,ic_epo);
					pst.setString(2,rad_via_liq);
						registros = pst.executeUpdate();
						pst.close();
					}
			}
		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		return registros;
	}

	public Vector getComboEposxIF(String tipoAfiliado, String ic_producto_nafin, String ic_nafin_electronico)
			throws NafinException{
		System.out.println("Dispersion BEAN::getComboEposxIF (E)");
		AccesoDB con = new AccesoDB();
		String					condicion		= "";
		String					qrySentencia	= "";
		ResultSet				rs				= null;
		Vector 					renglones = new Vector();
		Vector 					columnas = null;

		try {
			con.conexionDB();
			String tablas = "";

			if("1".equals(ic_producto_nafin)) {
				if("I".equals(tipoAfiliado)) {
					condicion = "    AND rie.cs_vobo_nafin = 'S'";
				}
				else {
					if("B".equals(tipoAfiliado))
						condicion = "    AND rie.cs_beneficiario = 'S'";
				}
				if(!"".equals(ic_nafin_electronico)) {
					tablas = " ,comrel_nafin rna ";
					condicion +=
						"    AND rie.ic_if = rna.ic_epo_pyme_if "+
						"    AND rna.cg_tipo = 'I' "+
						"    AND rna.ic_nafin_electronico = "+ic_nafin_electronico+" ";
				}

				qrySentencia =
					" SELECT DISTINCT epo.ic_epo, epo.cg_razon_social"   +
					"   FROM comcat_epo epo, comrel_if_epo rie"   +tablas+
					"  WHERE epo.ic_epo = rie.ic_epo"   +condicion+
					"  ORDER BY 2"  ;
			}
			else {
				if(!"".equals(ic_nafin_electronico)) {
					tablas = ",comrel_nafin rna";
					condicion +=
						"    AND iep.ic_if = rna.ic_epo_pyme_if "+
						"    AND rna.cg_tipo = 'I' "+
						"    AND rna.ic_nafin_electronico = "+ic_nafin_electronico+" ";
				}
				qrySentencia =
					" SELECT DISTINCT epo.ic_epo, epo.cg_razon_social"   +
					"   FROM comcat_epo epo, comrel_if_epo_x_producto iep"+tablas+
					"  WHERE epo.ic_epo = iep.ic_epo"   +
					"	 AND iep.cs_habilitado = 'S' "+
					"    AND ic_producto_nafin = "+ic_producto_nafin+" "+condicion+
					"  ORDER BY 2"  ;
			}
			rs = con.queryDB(qrySentencia);
			while(rs.next()) {
				columnas = new Vector();
				columnas.addElement((rs.getString(1)==null?"":rs.getString(1)));
				columnas.addElement((rs.getString(2)==null?"":rs.getString(2)));
				renglones.addElement(columnas);
			}
			rs.close(); con.cierraStatement();
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion BEAN::getComboEposxIF (S)");
		}
		return renglones;
	}

	public Vector getDatosPyme(
				String tipo_factura,
				String ic_nafin_electronico,
				String mesIni,
				String anyoIni,
				String mesFin,
				String anyoFin,
				String ic_producto_nafin,
				String ic_epo,
				String ic_if)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosPyme (E)");
		AccesoDB con = new AccesoDB();
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector 				renglones		= new Vector();
		Vector 				columnas		= null;

		Calendar 			mesAnyoFinal 	= new GregorianCalendar();
		Calendar 			mesAnyoEval		= new GregorianCalendar();
		String 				fecha, fechaEval;
		try {
			con.conexionDB();

			if("1".equals(ic_producto_nafin))
				qrySentencia = getDatosPymeDescuento(tipo_factura,ic_nafin_electronico);
			else if("2".equals(ic_producto_nafin))
				qrySentencia = getDatosPymePedidos(tipo_factura,ic_nafin_electronico);
			else if("4".equals(ic_producto_nafin))
				qrySentencia = getDatosPymeDistribuidores(tipo_factura,ic_nafin_electronico);
			else if("5".equals(ic_producto_nafin))
				qrySentencia = getDatosPymeInventarios(tipo_factura,ic_nafin_electronico);


			mesAnyoFinal.set(Integer.parseInt(anyoFin), Integer.parseInt(mesFin)-1, 1);
			fecha = new java.text.SimpleDateFormat("dd/MM/yyyy").format(mesAnyoFinal.getTime());

			mesAnyoFinal.add(Calendar.MONTH, 1);
			fecha = new java.text.SimpleDateFormat("dd/MM/yyyy").format(mesAnyoFinal.getTime());

			mesFin	= fecha.substring(3,5);
			anyoFin	= fecha.substring(6,10);

			mesAnyoEval.set(Integer.parseInt(anyoIni), Integer.parseInt(mesIni)-1, 1);
			fechaEval = new java.text.SimpleDateFormat("dd/MM/yyyy").format(mesAnyoEval.getTime());

			String _mes = fechaEval.substring(3,5);
			String _anyo = fechaEval.substring(6,10);
			do {
				int diaMaxMes = mesAnyoEval.getActualMaximum(Calendar.DAY_OF_MONTH);
				ps = con.queryPrecompilado(qrySentencia);
				String fecha_inicio = "01/"+_mes+"/"+_anyo;
				String fecha_fin = diaMaxMes+"/"+_mes+"/"+_anyo;
				ps.setString(1,fecha_inicio);
				ps.setString(2,fecha_fin);
				ps.setInt(3,Integer.parseInt(ic_epo));
				ps.setInt(4,Integer.parseInt(ic_if));
				if(!"T".equals(tipo_factura) && !"".equals(ic_nafin_electronico))
					ps.setInt(5,Integer.parseInt(ic_nafin_electronico));
				rs = ps.executeQuery();
				ps.clearParameters();
				while(rs.next()) {
					columnas = new Vector();
					columnas.addElement((rs.getString(1)==null?"":rs.getString(1))); // 0 ic_pyme
					columnas.addElement((rs.getString(2)==null?"":rs.getString(2))); // 1 razon social
					columnas.addElement((rs.getString(3)==null?"":rs.getString(3))); // 2 RFC
					columnas.addElement((rs.getString(4)==null?"":rs.getString(4))); // 3 Domicilio
					columnas.addElement(fecha_inicio);//4 inicio periodo
					columnas.addElement(fecha_fin);//5 fin periodo
					columnas.addElement((rs.getString(5)==null?"":rs.getString(5))); // 6 Nafin Electr�nico
					//FODEA 017-2010 FVR -INI
					columnas.addElement((rs.getString("calle_dom")==null?"":rs.getString("calle_dom"))); // 7
					columnas.addElement((rs.getString("numext_dom")==null?"":rs.getString("numext_dom"))); // 8
					columnas.addElement((rs.getString("numint_dom")==null?"":rs.getString("numint_dom"))); // 9
					columnas.addElement((rs.getString("colonia_dom")==null?"":rs.getString("colonia_dom"))); // 10
					columnas.addElement((rs.getString("cp_dom")==null?"":rs.getString("cp_dom"))); // 11
					columnas.addElement((rs.getString("municipio")==null?"":rs.getString("municipio"))); // 12
					columnas.addElement((rs.getString("estado")==null?"":rs.getString("estado"))); // 13
					//FODEA 017-2010 FVR -FIN
					renglones.addElement(columnas);
				}
				rs.close();
				if(ps!=null) ps.close();
				mesAnyoEval.add(Calendar.MONTH, 1);
				fechaEval = new java.text.SimpleDateFormat("dd/MM/yyyy").format(mesAnyoEval.getTime());
				_mes = fechaEval.substring(3,5);
				_anyo = fechaEval.substring(6,10);
			} while(!mesFin.equals(_mes) || !anyoFin.equals(_anyo));
		}catch(Exception e){
			System.out.println("Dispersion BEAN::getDatosPyme (Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion BEAN::getDatosPyme (S)");
		}
		return renglones;
	}

	private String getDatosPymeDescuento(String tipo_factura, String ic_nafin_electronico)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosPymeDescuento (E)");
		String				tablas			= "";
		String				condicion		= "";
		String				qrySentencia	= "";
		String				indice			= "";
		if(!"T".equals(tipo_factura) && !"".equals(ic_nafin_electronico)) {
			indice = "index(rel CP_COMREL_NAFIN_PK) index(mun CP_COMCAT_MUNICIPIO_PK) ";
			tablas =  " comrel_nafin rel, ";
			condicion =
				"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
				"    AND rel.cg_tipo = 'P' "   +
				"    AND rel.ic_nafin_electronico = ? ";

		}
		qrySentencia =
			" SELECT   /*+"+indice+"use_nl(dom edo)*/"   +
			"        doc.ic_pyme, pym.cg_razon_social,pym.cg_rfc,"   +
			"        dom.cg_calle || ' ' ||"   +
			"        dom.cg_numero_ext || ' ' ||"   +
			"        dom.cg_numero_int || ' ' ||"   +
			"        'COL. ' || dom.cg_colonia || ', ' ||"   +
			"        'C.P. ' || dom.cn_cp || ', ' ||" +
			"         mun.cd_nombre || ', ' || "   +
			"        edo.cd_nombre AS domicilio,"  +
			"		 r.ic_nafin_electronico,"  +
			//MOFICICACION F017-2010 FVR ini
			"        dom.cg_calle as calle_dom, " +
			"        dom.cg_numero_ext as numext_dom, " +
			"        dom.cg_numero_int as numint_dom, " +
			"        dom.cg_colonia as colonia_dom, " +
			"        dom.cn_cp as cp_dom, " +
			"        mun.cd_nombre as municipio,  " +
			"        edo.cd_nombre as estado " +
			//MODIFICACION F017-2010 FVR fin
			"   FROM com_documento doc,"   +
			"        com_docto_seleccionado sel,"   +
			"        com_solicitud s,"   +
			"        comcat_pyme pym,"+tablas+" "+
			"        com_domicilio dom, "   +
			"        comcat_estado edo, "   +
			"        comcat_municipio mun, "+
			"		 comrel_nafin r"+
			"  WHERE doc.ic_documento = sel.ic_documento"   +
			"    AND sel.ic_documento = s.ic_documento"   +
			"    AND doc.ic_pyme = pym.ic_pyme"   +
			"    AND pym.ic_pyme = dom.ic_pyme"   +
			"    AND dom.ic_estado = edo.ic_estado"   +
			"    AND edo.ic_estado = mun.ic_estado"   +
			"    AND dom.ic_municipio = mun.ic_municipio "+
			"    AND s.ig_numero_prestamo IS NOT NULL"   +
			"    AND TRUNC (s.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
			"    AND doc.ic_epo = ?"   +
			"    AND doc.ic_if = ?"   +
			"    AND dom.cs_fiscal = 'S' "+condicion+" "+
			"	AND pym.ic_pyme = r.ic_epo_pyme_if "+
			"    AND r.cg_tipo = 'P' " +
			"  GROUP BY doc.ic_pyme,"   +
			"        pym.cg_razon_social,pym.cg_rfc,"   +
			"        dom.cg_calle || ' ' ||"   +
			"        dom.cg_numero_ext || ' ' ||"   +
			"        dom.cg_numero_int || ' ' ||"   +
			"        'COL. ' || dom.cg_colonia || ', ' ||"   +
			"        'C.P. ' || dom.cn_cp || ', ' ||" +
			"         mun.cd_nombre || ', ' || "   +
			"         edo.cd_nombre,"  +
			"		 r.ic_nafin_electronico,"+
			//FODEA 017-2010 FVR ini
			"			dom.cg_calle , " +
			"        dom.cg_numero_ext, " +
			"        dom.cg_numero_int, " +
			"        dom.cg_colonia, " +
			"        dom.cn_cp, " +
			"        mun.cd_nombre,  " +
			"        edo.cd_nombre ";
			//FODEA 017-2010 FVR fin


		System.out.println("Dispersion BEAN::getDatosPymeDescuento (S)");
		return qrySentencia;
	}

	private String getDatosPymePedidos(String tipo_factura, String ic_nafin_electronico)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosPymePedidos (E)");
		String				tablas			= "";
		String				condicion		= "";
		String				qrySentencia	= "";
		if(!"T".equals(tipo_factura) && !"".equals(ic_nafin_electronico)) {
			tablas =  " comrel_nafin rel, ";
			condicion =
				"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
				"    AND rel.cg_tipo = 'P' "   +
				"    AND rel.ic_nafin_electronico = ? ";

		}
		qrySentencia =
			qrySentencia =
			" SELECT   /*+index(sel CP_COM_PEDIDO_SELECCIONADO_PK) index(ant CP_COM_ANTICIPO_PK) index(mun CP_COMCAT_MUNICIPIO_PK)*/"   +
			"        ped.ic_pyme, pym.cg_razon_social, pym.cg_rfc,"   +
			"        dom.cg_calle || ' ' ||"   +
			"        dom.cg_numero_ext || ' ' ||"   +
			"        dom.cg_numero_int || ' ' ||"   +
			"        'COL. ' || dom.cg_colonia || ', ' ||"   +
			"        'C.P. ' || dom.cn_cp || ', ' ||" +
			"         mun.cd_nombre || ', ' || "   +
			"         edo.cd_nombre AS domicilio,"  +
			"		 r.ic_nafin_electronico,"  +
			"        dom.cg_calle as calle_dom, " +
			"        dom.cg_numero_ext as numext_dom, " +
			"        dom.cg_numero_int as numint_dom, " +
			"        dom.cg_colonia as colonia_dom, " +
			"        dom.cn_cp as cp_dom, " +
			"        mun.cd_nombre as municipio,  " +
			"        edo.cd_nombre as estado " +
			"   FROM com_pedido ped,"   +
			"        com_pedido_seleccionado sel,"   +
			"        com_anticipo ant,"   +
			"        com_linea_credito lc,"   +
			"        comcat_pyme pym,"+tablas+" "+
			"        com_domicilio dom, "   +
			"       comcat_estado edo, "   +
			"        comcat_municipio mun,"   +
			"		 comrel_nafin r"+
			"  WHERE ped.ic_pedido = sel.ic_pedido"   +
			"    AND sel.ic_pedido = ant.ic_pedido"   +
			"    AND sel.ic_linea_credito = lc.ic_linea_credito"   +
			"    AND ped.ic_pyme = pym.ic_pyme"   +
			"    AND pym.ic_pyme = dom.ic_pyme"   +
			"    AND dom.ic_estado = edo.ic_estado"   +
			"    AND edo.ic_estado = mun.ic_estado"   +
			"    AND dom.ic_municipio = mun.ic_municipio"   +
			"    AND ant.ig_numero_prestamo IS NOT NULL"   +
			"    AND TRUNC (ant.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
			"    AND ped.ic_epo = ?"   +
			"    AND lc.ic_if = ?"   +
			"    AND dom.cs_fiscal = 'S' "+condicion+" "+
			"	AND pym.ic_pyme = r.ic_epo_pyme_if "+
			"    AND r.cg_tipo = 'P' " +
			"  GROUP BY ped.ic_pyme,"   +
			"        pym.cg_razon_social, pym.cg_rfc,"   +
			"        dom.cg_calle || ' ' ||"   +
			"        dom.cg_numero_ext || ' ' ||"   +
			"        dom.cg_numero_int || ' ' ||"   +
			"        'COL. ' || dom.cg_colonia || ', ' ||"   +
			"        'C.P. ' || dom.cn_cp || ', ' ||" +
			"         mun.cd_nombre || ', ' || "   +
			"         edo.cd_nombre AS domicilio,"  +
			"		 r.ic_nafin_electronico,"+
			//FODEA 017-2010 FVR ini
			"			dom.cg_calle , " +
			"        dom.cg_numero_ext, " +
			"        dom.cg_numero_int, " +
			"        dom.cg_colonia, " +
			"        dom.cn_cp, " +
			"        mun.cd_nombre,  " +
			"        edo.cd_nombre ";
			//FODEA 017-2010 FVR fin
		System.out.println("Dispersion BEAN::getDatosPymePedidos (S)");
		return qrySentencia;
	}

	private String getDatosPymeDistribuidores(String tipo_factura, String ic_nafin_electronico)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosPymeDistribuidores (E)");
		String				tablas			= "";
		String				condicion		= "";
		String				qrySentencia	= "";
		if(!"T".equals(tipo_factura) && !"".equals(ic_nafin_electronico)) {
			tablas =  " comrel_nafin rel, ";
			condicion =
				"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
				"    AND rel.cg_tipo = 'P' "   +
				"    AND rel.ic_nafin_electronico = ? ";

		}
		qrySentencia =
			" SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(sol CP_DIS_SOLICITUD_PK) index(mun CP_COMCAT_MUNICIPIO_PK) use_nl(doc sel sol lc pym dom edo mun)*/"   +
			"        doc.ic_pyme, pym.cg_razon_social, pym.cg_rfc,"   +
			"        dom.cg_calle || ' ' ||"   +
			"        dom.cg_numero_ext || ' ' ||"   +
			"        dom.cg_numero_int || ' ' ||"   +
			"        'COL. ' || dom.cg_colonia || ', ' ||"   +
			"        'C.P. ' || dom.cn_cp || ', ' ||  " +
			"        mun.cd_nombre || ', ' || "   +
			"        edo.cd_nombre AS domicilio,"  +
			"		 r.ic_nafin_electronico,"  +
			//FODEA 017-2010 VFR ini
			"        dom.cg_calle as calle_dom, " +
			"        dom.cg_numero_ext as numext_dom, " +
			"        dom.cg_numero_int as numint_dom, " +
			"        dom.cg_colonia as colonia_dom, " +
			"        dom.cn_cp as cp_dom, " +
			"        mun.cd_nombre as municipio,  " +
			"        edo.cd_nombre as estado " +
			//FODEA 017-2010 VFR fin
			"   FROM dis_documento doc,"   +
			"        dis_docto_seleccionado sel,"   +
			"        dis_solicitud sol,"   +
			"        dis_linea_credito_dm lc,"   +
			"        comcat_pyme pym,"+tablas+" "+
			"        com_domicilio dom,"   +
			"        comcat_estado edo,"   +
			"        comcat_municipio mun,"   +
			"		 comrel_nafin r"+
			"  WHERE doc.ic_documento = sel.ic_documento"   +
			"    AND sel.ic_documento = sol.ic_documento"   +
			"    AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm"   +
			"    AND doc.ic_pyme = pym.ic_pyme"   +
			"    AND pym.ic_pyme = dom.ic_pyme"   +
			"    AND dom.ic_estado = edo.ic_estado"   +
			"    AND edo.ic_estado = mun.ic_estado"   +
			"    AND dom.ic_municipio = mun.ic_municipio"   +
			"    AND sol.ig_numero_prestamo IS NOT NULL"   +
			"    AND TRUNC (sol.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
			"    AND doc.ic_epo = ?"   +
			"    AND lc.ic_if = ?"   +
			"    AND dom.cs_fiscal = 'S' "+condicion+" "+
			"	AND pym.ic_pyme = r.ic_epo_pyme_if "+
			"    AND r.cg_tipo = 'P' " +
			"  GROUP BY doc.ic_pyme,"   +
			"        pym.cg_razon_social, pym.cg_rfc,"   +
			"        dom.cg_calle || ' ' ||"   +
			"        dom.cg_numero_ext || ' ' ||"   +
			"        dom.cg_numero_int || ' ' ||"   +
			"        'COL. ' || dom.cg_colonia || ', ' ||"   +
			"        'C.P. ' || dom.cn_cp || ', ' ||  " +
			"        mun.cd_nombre || ', ' || "   +
			"        edo.cd_nombre,"  +
			"		 r.ic_nafin_electronico,"+
			//FODEA 017-2010 FVR ini
			"			dom.cg_calle , " +
			"        dom.cg_numero_ext, " +
			"        dom.cg_numero_int, " +
			"        dom.cg_colonia, " +
			"        dom.cn_cp, " +
			"        mun.cd_nombre,  " +
			"        edo.cd_nombre ";
			//FODEA 017-2010 FVR fin
		System.out.println("Dispersion BEAN::getDatosPymeDistribuidores (S)");
		return qrySentencia;
	}

	private String getDatosPymeInventarios(String tipo_factura, String ic_nafin_electronico)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosPymeInventarios (E)");
		String				tablas			= "";
		String				condicion		= "";
		String				qrySentencia	= "";
		if(!"T".equals(tipo_factura) && !"".equals(ic_nafin_electronico)) {
			tablas =  " comrel_nafin rel, ";
			condicion =
				"    AND pym.ic_pyme = rel.ic_epo_pyme_if "   +
				"    AND rel.cg_tipo = 'P' "   +
				"    AND rel.ic_nafin_electronico = ? ";

		}
		qrySentencia =
				qrySentencia =
			" SELECT   /*+index(sol CP_INV_SOLICITUD_PK) index(mun CP_COMCAT_MUNICIPIO_PK)*/"   +
			"        dis.ic_pyme, pym.cg_razon_social, pym.cg_rfc,"   +
			"        dom.cg_calle || ' ' ||"   +
			"        dom.cg_numero_ext || ' ' ||"   +
			"        dom.cg_numero_int || ' ' ||"   +
			"        'COL. ' || dom.cg_colonia || ', ' ||"   +
			"        'C.P. ' || dom.cn_cp  || ', ' || " +
			"        mun.cd_nombre || ', ' || "   +
			"        edo.cd_nombre AS domicilio,"  +
			"		 r.ic_nafin_electronico,"  +
			//FODEA 017-2010 VFR ini
			"        dom.cg_calle as calle_dom, " +
			"        dom.cg_numero_ext as numext_dom, " +
			"        dom.cg_numero_int as numint_dom, " +
			"        dom.cg_colonia as colonia_dom, " +
			"        dom.cn_cp as cp_dom, " +
			"        mun.cd_nombre as municipio,  " +
			"        edo.cd_nombre as estado " +
			//FODEA 017-2010 VFR fin
			"   FROM inv_disposicion dis,"   +
			"        inv_solicitud sol,"   +
			"        com_linea_credito lc,"   +
			"        comcat_pyme pym,"+tablas+" "+
			"        com_domicilio dom,"   +
			"        comcat_estado edo,"   +
			"        comcat_municipio mun,"   +
			"		 comrel_nafin r"+
			"  WHERE dis.cc_disposicion = sol.cc_disposicion"  +
			"    AND dis.ic_linea_credito = lc.ic_linea_credito"   +
			"    AND dis.ic_pyme = pym.ic_pyme"   +
			"    AND pym.ic_pyme = dom.ic_pyme"   +
			"    AND dom.ic_estado = edo.ic_estado"   +
			"    AND edo.ic_estado = mun.ic_estado"   +
			"    AND dom.ic_municipio = mun.ic_municipio"   +
			"    AND sol.ig_numero_prestamo IS NOT NULL"   +
			"    AND TRUNC (sol.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
			"    AND dis.ic_epo = ?"   +
			"    AND lc.ic_if = ?"   +
			"    AND dom.cs_fiscal = 'S' "+condicion+" "+
			"	 AND pym.ic_pyme = r.ic_epo_pyme_if "+
			"    AND r.cg_tipo = 'P' " +
			"  GROUP BY dis.ic_pyme,"   +
			"	     pym.cg_razon_social, pym.cg_rfc,"   +
			"        dom.cg_calle || ' ' ||"   +
			"        dom.cg_numero_ext || ' ' ||"   +
			"        dom.cg_numero_int || ' ' ||"   +
			"        'COL. ' || dom.cg_colonia || ', ' ||"   +
			"        'C.P. ' || dom.cn_cp  || ', ' || " +
			"        mun.cd_nombre || ', ' || "   +
			"        edo.cd_nombre,"  +
			"		 r.ic_nafin_electronico,"+
			//FODEA 017-2010 FVR ini
			"			dom.cg_calle , " +
			"        dom.cg_numero_ext, " +
			"        dom.cg_numero_int, " +
			"        dom.cg_colonia, " +
			"        dom.cn_cp, " +
			"        mun.cd_nombre,  " +
			"        edo.cd_nombre ";
			//FODEA 017-2010 FVR fin
		System.out.println("Dispersion BEAN::getDatosPymeInventarios (S)");
		return qrySentencia;
	}

	public String crearResumenFactura(Vector DatosFactura, String ic_producto_nafin, String ic_epo, String ic_if)
			throws NafinException {
		System.out.println("Dispersion BEAN::crearResumenFactura (E)");
		AccesoDB con = new AccesoDB();
		String				meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String 				contenido		= "";
		String 				fechaFinAux		= "";
		int					cont			= 0;

		try {
			Vector nombres = getNombres(ic_producto_nafin, ic_epo);
			String nombreProducto 	= nombres.get(0).toString().replace(',',' ');
			String nombreEpo 		= nombres.get(1).toString().replace(',',' ');
			for(int fac=0;fac<DatosFactura.size();fac++) {
				Vector DatosPyme			= (Vector)DatosFactura.get(fac);
				String ic_pyme		    	= (String)DatosPyme.get(0);
				String nombrePyme			= (String)DatosPyme.get(1);
				//String rfcPyme      		= (String)DatosPyme.get(2);
				//String domicilioPyme		= (String)DatosPyme.get(3);
				String fechaInicio			= (String)DatosPyme.get(4);
				String fechaFin				= (String)DatosPyme.get(5);
				String ic_nafin_electronico	= (String)DatosPyme.get(6);

				//String diaFactura    = fechaFin.substring(0,2);
				String mesFactura    = fechaFin.substring(3,5);
				String anioFactura   = fechaFin.substring(6,10);

				Vector Registros = getDatosFactura(ic_pyme,fechaInicio,fechaFin,ic_producto_nafin,ic_epo,ic_if,"R");

				if(!"0".equals(Registros.get(0).toString())) {
					if(!fechaFin.equals(fechaFinAux)) {
						cont = 0;
						if(!"".equals(contenido))
							contenido += "\n \n";
						fechaFinAux = fechaFin;
						String encabezado = "FACTURAS "+nombreEpo+" ("+meses[Integer.parseInt(mesFactura)-1]+" "+anioFactura+") DE "+nombreProducto;
						contenido += encabezado.toUpperCase();
						contenido += "\n Num.,Nombre / Raz�n Social,Num. Cliente\n";
					}
					cont++;
					contenido += cont+","+nombrePyme.replace(',',' ')+","+ic_nafin_electronico+"\n";
				}

			}//for(int fac=0;fac<DatosFactura.size();fac++)
		}catch(Exception e){
			System.out.println("Dispersion BEAN::crearResumenFactura (Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion BEAN::crearResumenFactura (S)");
		}
		return contenido;
	}


	public Vector getDatosFactura(
				String ic_pyme,
				String fecha_seleccion_de,
				String fecha_seleccion_a,
				String ic_producto_nafin,
				String ic_epo,
				String ic_if,
				String tipo_factura)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosFactura (E)");
		AccesoDB con = new AccesoDB();
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector 				renglones		= new Vector();
		Vector 				columnas		= null;
		try {
			con.conexionDB();
			String tipo_interes = getTipoInteresFacura(ic_producto_nafin,ic_epo,con);
/*
System.out.println("\n --------------------------------------------------------");
System.out.println("\n --------------------------------------------------------");
System.out.println("\n ic_pyme: "+ic_pyme);
System.out.println("\n fecha_seleccion_de: "+fecha_seleccion_de);
System.out.println("\n fecha_seleccion_a: "+fecha_seleccion_a);
System.out.println("\n ic_epo: "+ic_epo);
System.out.println("\n ic_if: "+ic_if);
System.out.println("\n tipo_factura: "+tipo_factura);
System.out.println("\n tipo_interes: "+tipo_interes);*/
			if("1".equals(ic_producto_nafin))
				qrySentencia = getDatosFacturaDescuento(tipo_factura,tipo_interes);
			else if("2".equals(ic_producto_nafin))
				qrySentencia = getDatosFacturaPedidos(tipo_factura,tipo_interes);
			else if("4".equals(ic_producto_nafin))
				qrySentencia = getDatosFacturaDistribuidores(tipo_factura,tipo_interes);
			else if("5".equals(ic_producto_nafin))
				qrySentencia = getDatosFacturaInventarios(tipo_factura,tipo_interes);

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,fecha_seleccion_de);
			ps.setString(2,fecha_seleccion_a);
			ps.setInt(3,Integer.parseInt(ic_epo));
			ps.setInt(4,Integer.parseInt(ic_pyme));
			ps.setInt(5,Integer.parseInt(ic_if));
			if("S".equals(tipo_factura)) {
				ps.setString(6,fecha_seleccion_de);
				ps.setString(7,fecha_seleccion_a);
			}
			rs = ps.executeQuery();
			while(rs.next()) {
				if("R".equals(tipo_factura))
					renglones.addElement((rs.getString(1)==null?"":rs.getString(1)));
				else {
					columnas = new Vector();
					columnas.addElement((rs.getString(1)==null?"":rs.getString(1)));
					columnas.addElement((rs.getString(2)==null?"":rs.getString(2)));
					columnas.addElement((rs.getString(3)==null?"":rs.getString(3)));
					renglones.addElement(columnas);
				}
			}//while(rs.next())
			rs.close();
			if(ps!=null) ps.close();

		}catch(Exception e){
			System.out.println("Dispersion BEAN::getDatosFactura (Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if("S".equals(tipo_factura))
				con.terminaTransaccion(true);

			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion BEAN::getDatosFactura (S)");
		}
		return renglones;
	}

	private Vector getNombres(String ic_producto_nafin, String ic_epo)
			throws NafinException {
		System.out.println("Dispersion BEAN::getNombres (E)");
		AccesoDB con = new AccesoDB();
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector 				renglones		= new Vector();
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT cpn.ic_nombre, epo.cg_razon_social"   +
				"   FROM comcat_producto_nafin cpn, comcat_epo epo"   +
				"  WHERE cpn.ic_producto_nafin = ? AND ic_epo = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_producto_nafin));
			ps.setInt(2,Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			while(rs.next()) {
				renglones.addElement((rs.getString(1)==null?"":rs.getString(1)));
				renglones.addElement((rs.getString(2)==null?"":rs.getString(2)));
			}//while(rs.next())
			rs.close();
			if(ps!=null) ps.close();
		}catch(Exception e){
			System.out.println("Dispersion BEAN::getNombres (Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion BEAN::getNombres (S)");
		}
		return renglones;
	}

	private String getDatosFacturaDescuento(String tipo_factura, String tipo_interes)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosFacturaDescuento (E)");
		String				condicion		= "";
		String				campos			= "";
		String				tablas			= "";
		String				groupby			= "";
		String				qrySentencia	= "";
		if("N".equals(tipo_factura) || "R".equals(tipo_factura) || "A".equals(tipo_interes)) {
			if("R".equals(tipo_factura)) {
				campos = "COUNT(1)";
			} else if("N".equals(tipo_factura)) {
				//datos de N@E
				campos = " SUM (doc.FN_MONTO_DSCTO), ROUND (sel.in_tasa_aceptada, 4), SUM (sel.in_importe_interes) ";
				groupby = " GROUP BY sel.in_tasa_aceptada ";
			} else if("S".equals(tipo_factura) && "A".equals(tipo_interes)) {
				//datos de SIRAC tipos de cobro de interes anticipado
				campos =
					" SUM (pres.monto_inicial) AS monto, "+
					" ROUND (pres.tasa_total, 4) AS tasa, "+
					" SUM (ROUND (ROUND ((pres.monto_inicial * pres.tasa_total) / 360, 2) * pres.plazo, 2)) AS montointereses ";
				tablas = " ,pr_prestamos pres ";
				condicion = " AND s.ig_numero_prestamo = pres.numero_prestamo ";
				groupby = " GROUP BY pres.tasa_total ";
			}
			qrySentencia =
				" SELECT "+campos+" "+
				"   FROM com_docto_seleccionado sel, com_documento doc, com_solicitud s "+tablas+" "+
				"  WHERE doc.ic_documento = sel.ic_documento"   +
				"    AND sel.ic_documento = s.ic_documento"   +
				"    AND s.ig_numero_prestamo IS NOT NULL "+condicion+" "+
				"	  AND s.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))   " +
				"    AND s.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy')+1) " +
				"    AND doc.ic_epo = ?"   +
				"    AND doc.ic_pyme = ?"   +
				"    AND doc.ic_if = ?"+
				groupby;
		} else {
			//datos de SIRAC tipos de cobro de interes al vencimiento
			qrySentencia =
				" SELECT   SUM (pres.monto_inicial), pres.tasa_total, SUM (sal.valor) AS intereses"   +
				"     FROM pr_prestamos pres, pr_pagos pag, pr_saldos_pagos sal, pr_tipos_saldo ps"   +
				"    WHERE pag.codigo_empresa = pres.codigo_empresa"   +
				"      AND pag.codigo_agencia = pres.codigo_agencia"   +
				"      AND pag.codigo_sub_aplicacion = pres.codigo_sub_aplicacion"   +
				"      AND pag.numero_prestamo = pres.numero_prestamo"   +
				"      AND sal.codigo_empresa = pag.codigo_empresa"   +
				"      AND sal.codigo_agencia = pag.codigo_agencia"   +
				"      AND sal.codigo_sub_aplicacion = pag.codigo_sub_aplicacion"   +
				"      AND sal.numero_prestamo = pag.numero_prestamo"   +
				"      AND sal.numero_cuota = pag.numero_cuota"   +
				"      AND sal.secuencia = pag.secuencia_pago"   +
				"      AND ps.codigo_tipo_saldo = sal.codigo_tipo_saldo"   +
				"      AND ps.tipo_abono = 'I'"   +
				"      AND pres.numero_prestamo IN ("   +
				"             SELECT   s.ig_numero_prestamo"   +
				"                 FROM com_docto_seleccionado sel, com_documento doc, com_solicitud s"   +
				"                WHERE doc.ic_documento = sel.ic_documento"   +
				"                  AND sel.ic_documento = s.ic_documento"   +
				"                  AND s.ig_numero_prestamo IS NOT NULL"   +
				"                  AND TRUNC (s.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"                  AND doc.ic_epo = ?"   +
				"                  AND doc.ic_pyme = ?"   +
				"                  AND doc.ic_if = ?"   +
				"             GROUP BY s.ig_numero_prestamo)"   +
				"      AND TRUNC (pag.fecha_valida) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				" GROUP BY pres.tasa_total"  ;
		}//else
		System.out.println("Dispersion BEAN::getDatosFacturaDescuento (S)");
		return qrySentencia;
	}

	private String getDatosFacturaPedidos(String tipo_factura, String tipo_interes)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosFacturaPedidos (E)");
		String				condicion		= "";
		String				campos			= "";
		String				tablas			= "";
		String				groupby			= "";
		String				qrySentencia	= "";

		if("N".equals(tipo_factura) || "A".equals(tipo_interes)) {
			if("R".equals(tipo_factura)) {
				campos = "COUNT(1)";
			} else if("N".equals(tipo_factura)) {
				campos =
					" /*+index(sel CP_COM_PEDIDO_SELECCIONADO_PK) index(ant CP_COM_ANTICIPO_PK)*/ "+
					" SUM (sel.fn_credito_recibir), ROUND (sel.fn_tasa_aceptada, 4),SUM (sel.fn_interes_pagado) ";
				groupby = " GROUP BY sel.fn_tasa_aceptada ";
			} else if("S".equals(tipo_factura) && "A".equals(tipo_interes)) {
				campos =
					" /*+index(sel CP_COM_PEDIDO_SELECCIONADO_PK) index(ant CP_COM_ANTICIPO_PK)*/ "+
					" SUM (pres.monto_inicial) AS monto,  "+
					" ROUND (pres.tasa_total, 4) AS tasa,  "+
					" SUM (ROUND (ROUND ((pres.monto_inicial * pres.tasa_total) / 360, 2) * pres.plazo, 2)) AS montointereses  ";
				tablas = " ,pr_prestamos pres ";
				condicion = " AND ant.ig_numero_prestamo = pres.numero_prestamo ";
				groupby = " GROUP BY pres.tasa_total ";
			}
			qrySentencia =
				" SELECT "+campos+" "+
				"   FROM com_pedido ped, com_pedido_seleccionado sel, com_anticipo ant, com_linea_credito lc"+tablas+" "+
				"  WHERE ped.ic_pedido = sel.ic_pedido"   +
				"    AND sel.ic_pedido = ant.ic_pedido"   +
				"    AND sel.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND ant.ig_numero_prestamo IS NOT NULL "+condicion+" "+
				"    AND TRUNC (ant.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"    AND ped.ic_epo = ?"   +
				"    AND ped.ic_pyme = ?"   +
				"    AND lc.ic_if = ?"   +
				groupby+" ";
		} else {
			qrySentencia =
				" SELECT   SUM (pres.monto_inicial), pres.tasa_total, SUM (sal.valor) AS intereses"   +
				"     FROM pr_prestamos pres, pr_pagos pag, pr_saldos_pagos sal, pr_tipos_saldo ps"   +
				"    WHERE pag.codigo_empresa = pres.codigo_empresa"   +
				"      AND pag.codigo_agencia = pres.codigo_agencia"   +
				"      AND pag.codigo_sub_aplicacion = pres.codigo_sub_aplicacion"   +
				"      AND pag.numero_prestamo = pres.numero_prestamo"   +
				"      AND sal.codigo_empresa = pag.codigo_empresa"   +
				"      AND sal.codigo_agencia = pag.codigo_agencia"   +
				"      AND sal.codigo_sub_aplicacion = pag.codigo_sub_aplicacion"   +
				"      AND sal.numero_prestamo = pag.numero_prestamo"   +
				"      AND sal.numero_cuota = pag.numero_cuota"   +
				"      AND sal.secuencia = pag.secuencia_pago"   +
				"      AND ps.codigo_tipo_saldo = sal.codigo_tipo_saldo"   +
				"      AND ps.tipo_abono = 'I'"   +
				"      AND pres.numero_prestamo IN ("   +
				"             SELECT   /*+index(sel CP_COM_PEDIDO_SELECCIONADO_PK) index(ant CP_COM_ANTICIPO_PK)*/"   +
				"                       ant.ig_numero_prestamo"   +
				"                 FROM com_pedido ped, com_pedido_seleccionado sel, com_anticipo ant, com_linea_credito lc"   +
				"                WHERE ped.ic_pedido = sel.ic_pedido"   +
				"                  AND sel.ic_pedido = ant.ic_pedido"   +
				"                  AND sel.ic_linea_credito = lc.ic_linea_credito"   +
				"                  AND ant.ig_numero_prestamo IS NOT NULL"   +
				"                  AND TRUNC (ant.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"                  AND ped.ic_epo = ?"   +
				"                  AND ped.ic_pyme = ?"   +
				"                  AND lc.ic_if = ?"   +
				"             GROUP BY ant.ig_numero_prestamo)"   +
				"      AND TRUNC (pag.fecha_valida) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))			"   +
				" GROUP BY pres.tasa_total"  ;
		}
		System.out.println("Dispersion BEAN::getDatosFacturaPedidos (S)");
		return qrySentencia;
	}

	private String getDatosFacturaDistribuidores(String tipo_factura, String tipo_interes)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosFacturaDistribuidores (E)");
		String				condicion		= "";
		String				campos			= "";
		String				tablas			= "";
		String				groupby			= "";
		String				qrySentencia	= "";

		if("N".equals(tipo_factura) || "A".equals(tipo_interes)) {
			if("R".equals(tipo_factura)) {
				campos = "COUNT(1)";
			} else	if("N".equals(tipo_factura)) {
				campos =
					" /*+index(sol CP_DIS_SOLICITUD_PK) index(doc CP_DIS_DOCUMENTO_PK)*/ "+
					" SUM (sel.fn_importe_recibir), ROUND (sel.fn_valor_tasa, 4), SUM (sel.fn_importe_interes) ";
				groupby = " GROUP BY sel.fn_valor_tasa ";
			} else if("S".equals(tipo_factura) && "A".equals(tipo_interes)) {
				campos =
					" /*+index(sol CP_DIS_SOLICITUD_PK) index(doc CP_DIS_DOCUMENTO_PK)*/ "+
					" SUM (pres.monto_inicial) AS monto, "+
					" ROUND (pres.tasa_total, 4) AS tasa, "+
					" SUM (ROUND (ROUND ((pres.monto_inicial * pres.tasa_total) / 360, 2) * pres.plazo, 2)) AS montointereses ";
				tablas = " ,pr_prestamos pres ";
				condicion = " AND sol.ig_numero_prestamo = pres.numero_prestamo ";
				groupby = " GROUP BY pres.tasa_total ";
			}
			qrySentencia =
				" SELECT "+campos+" "+
				"   FROM dis_documento doc, dis_docto_seleccionado sel, dis_solicitud sol, dis_linea_credito_dm lc"+tablas+" "+
				"  WHERE doc.ic_documento = sel.ic_documento"   +
				"    AND sel.ic_documento = sol.ic_documento"   +
				"    AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm"   +
				"    AND sol.ig_numero_prestamo IS NOT NULL "+condicion+" "+
				"    AND TRUNC (sol.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"    AND doc.ic_epo = ?"   +
				"    AND doc.ic_pyme = ?"   +
				"    AND lc.ic_if = ?"   +
				groupby+" ";

		} else {
			qrySentencia =
				" SELECT   SUM (pres.monto_inicial), pres.tasa_total, SUM (sal.valor) AS intereses"   +
				"     FROM pr_prestamos pres, pr_pagos pag, pr_saldos_pagos sal, pr_tipos_saldo ps"   +
				"    WHERE pag.codigo_empresa = pres.codigo_empresa"   +
				"      AND pag.codigo_agencia = pres.codigo_agencia"   +
				"      AND pag.codigo_sub_aplicacion = pres.codigo_sub_aplicacion"   +
				"      AND pag.numero_prestamo = pres.numero_prestamo"   +
				"      AND sal.codigo_empresa = pag.codigo_empresa"   +
				"      AND sal.codigo_agencia = pag.codigo_agencia"   +
				"      AND sal.codigo_sub_aplicacion = pag.codigo_sub_aplicacion"   +
				"      AND sal.numero_prestamo = pag.numero_prestamo"   +
				"      AND sal.numero_cuota = pag.numero_cuota"   +
				"      AND sal.secuencia = pag.secuencia_pago"   +
				"      AND ps.codigo_tipo_saldo = sal.codigo_tipo_saldo"   +
				"      AND ps.tipo_abono = 'I'"   +
				"      AND pres.numero_prestamo IN ("   +
				"             SELECT   /*+index(sol CP_DIS_SOLICITUD_PK) index(doc CP_DIS_DOCUMENTO_PK)*/"   +
				"                       sol.ig_numero_prestamo"   +
				"                 FROM dis_documento doc, dis_docto_seleccionado sel, dis_solicitud sol, dis_linea_credito_dm lc"   +
				"                WHERE doc.ic_documento = sel.ic_documento"   +
				"                  AND sel.ic_documento = sol.ic_documento"   +
				"                  AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm"   +
				"                  AND sol.ig_numero_prestamo IS NOT NULL"   +
				"                  AND TRUNC (sol.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"                  AND doc.ic_epo = ?"   +
				"                  AND doc.ic_pyme = ?"   +
				"                  AND lc.ic_if = ?"   +
				"             GROUP BY sol.ig_numero_prestamo)"   +
				"      AND TRUNC (pag.fecha_valida) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				" GROUP BY pres.tasa_total"  ;
		}
		System.out.println("Dispersion BEAN::getDatosFacturaDistribuidores (S)");
		return qrySentencia;
	}

	private String getDatosFacturaInventarios(String tipo_factura, String tipo_interes)
			throws NafinException {
		System.out.println("Dispersion BEAN::getDatosFacturaInventarios (E)");
		String				condicion		= "";
		String				campos			= "";
		String				tablas			= "";
		String				groupby			= "";
		String				qrySentencia	= "";

		if("N".equals(tipo_factura) || "A".equals(tipo_interes)) {
			if("R".equals(tipo_factura)) {
				campos = "COUNT(1)";
			} else if("N".equals(tipo_factura)) {
				campos =
					" /*+index(dis CP_INV_DISPOSICION_PK) index(sol CP_INV_SOLICITUD_PK)*/ "+
					" SUM (dis.fn_monto_credito), ROUND (dis.fn_tasa_interes, 4), SUM (dis.fn_interes_generado) ";
				groupby = " GROUP BY dis.fn_tasa_interes ";
			} else if("S".equals(tipo_factura) && "A".equals(tipo_interes)) {
				campos =
					" /*+index(dis CP_INV_DISPOSICION_PK) index(sol CP_INV_SOLICITUD_PK)*/ "+
					" SUM (pres.monto_inicial) AS monto,  "+
					" ROUND (pres.tasa_total, 4) AS tasa, "+
					" SUM (ROUND (ROUND ((pres.monto_inicial * pres.tasa_total) / 360, 2) * pres.plazo, 2)) AS montointereses ";
				tablas = " ,pr_prestamos pres ";
				condicion = " AND sol.ig_numero_prestamo = pres.numero_prestamo ";
				groupby = " GROUP BY pres.tasa_total ";
			}
			qrySentencia =
				" SELECT "+campos+" "+
				"   FROM inv_disposicion dis, inv_solicitud sol, com_linea_credito lc"+tablas+" "+
				"  WHERE dis.cc_disposicion = sol.cc_disposicion"   +
				"    AND dis.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND sol.ig_numero_prestamo IS NOT NULL"   +condicion+ " "+
				"    AND TRUNC (sol.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"    AND dis.ic_epo = ?"   +
				"    AND dis.ic_pyme = ?"   +
				"    AND lc.ic_if = ?"   +
				groupby+" ";

		} else {
			qrySentencia =
				" SELECT   SUM (pres.monto_inicial), pres.tasa_total, SUM (sal.valor) AS intereses"   +
				"     FROM pr_prestamos pres, pr_pagos pag, pr_saldos_pagos sal, pr_tipos_saldo ps"   +
				"    WHERE pag.codigo_empresa = pres.codigo_empresa"   +
				"      AND pag.codigo_agencia = pres.codigo_agencia"   +
				"      AND pag.codigo_sub_aplicacion = pres.codigo_sub_aplicacion"   +
				"      AND pag.numero_prestamo = pres.numero_prestamo"   +
				"      AND sal.codigo_empresa = pag.codigo_empresa"   +
				"      AND sal.codigo_agencia = pag.codigo_agencia"   +
				"      AND sal.codigo_sub_aplicacion = pag.codigo_sub_aplicacion"   +
				"      AND sal.numero_prestamo = pag.numero_prestamo"   +
				"      AND sal.numero_cuota = pag.numero_cuota"   +
				"      AND sal.secuencia = pag.secuencia_pago"   +
				"      AND ps.codigo_tipo_saldo = sal.codigo_tipo_saldo"   +
				"      AND ps.tipo_abono = 'I'"   +
				"      AND pres.numero_prestamo IN ("   +
				"             SELECT   /*+index(dis CP_INV_DISPOSICION_PK) index(sol CP_INV_SOLICITUD_PK)*/"   +
				"                       sol.ig_numero_prestamo"   +
				"                 FROM inv_disposicion dis, inv_solicitud sol, com_linea_credito lc"   +
				"                WHERE dis.cc_disposicion = sol.cc_disposicion"   +
				"                  AND dis.ic_linea_credito = lc.ic_linea_credito"   +
				"                  AND sol.ig_numero_prestamo IS NOT NULL"   +
				"                  AND TRUNC (sol.df_operacion) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				"                  AND dis.ic_epo = ?"   +
				"                  AND dis.ic_pyme = ?"   +
				"                  AND lc.ic_if = ?"   +
				"             GROUP BY sol.ig_numero_prestamo)"   +
				"      AND TRUNC (pag.fecha_valida) BETWEEN TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND TRUNC (TO_DATE (?, 'dd/mm/yyyy'))"   +
				" GROUP BY pres.tasa_total"  ;
		}
		System.out.println("Dispersion BEAN::getDatosFacturaInventarios (S)");
		return qrySentencia;
	}

	public void setParamFacturaxProducto(String ic_producto_nafin, String rad_epo, String rad_tipo_int)
	 		throws NafinException {
		System.out.println("Dispersion::setParamFacturaxProducto(E) ");
		String				qrySentencia	= "";
		AccesoDB			con				= null;
		PreparedStatement	ps				= null;
		boolean 			resultado		= false;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia  =
				" UPDATE comcat_producto_nafin"   +
				"    SET cs_fact_epo = ?,"   +
				"        cs_fact_tipo_int = ?"   +
				"  WHERE ic_producto_nafin = ? "  ;
			System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,rad_epo);
			ps.setString(2,rad_tipo_int);
			ps.setInt(3,Integer.parseInt(ic_producto_nafin));
			ps.executeUpdate();
			ps.close();
			resultado = true;
		} catch(Exception e){
			System.out.println("Dispersion::setParamFacturaxProducto Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(resultado);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::setParamFacturaxProducto(S)");
		}
	}//setParamFacturaxProducto

	public Vector getParamFacturaxProducto(String ic_producto_nafin)
			throws NafinException {
		System.out.println("Dispersion::getParamFacturaxProducto(E) ");
		String				qrySentencia	= "";
		AccesoDB			con				= null;
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		Vector				columnas		= null;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia  =
				" SELECT cs_fact_epo, cs_fact_tipo_int"   +
				"   FROM comcat_producto_nafin"   +
				"  WHERE ic_producto_nafin = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_producto_nafin));
			//System.out.println(qrySentencia);
			rs = ps.executeQuery();
			while(rs.next()) {
				columnas = new Vector();
				columnas.addElement((rs.getString(1)==null?"":rs.getString(1)));
				columnas.addElement((rs.getString(2)==null?"":rs.getString(2)));
			}
			rs.close();
			if(ps!=null) ps.close();
		} catch(Exception e){
			System.out.println("Dispersion::getParamFacturaxProducto Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getParamFacturaxProducto(S)");
		}
		return columnas;
	}//getParamFacturaxProducto

	public void setParamFacturaxProductoEpo(String ic_producto_nafin, String ic_epo, String rad_tipo_int)
			throws NafinException {
		System.out.println("Dispersion::setParamFacturaxProductoEpo(E) ");
		String				qrySentencia	= "";
		AccesoDB			con				= null;
		PreparedStatement	ps				= null;
		boolean 			resultado		= false;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia  =
				" UPDATE comrel_producto_epo"   +
				"    SET cs_fact_tipo_int = ?"   +
				"  WHERE ic_producto_nafin = ? "  +
				"   AND  ic_epo = ? "  ;
//System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,rad_tipo_int);
			ps.setInt(2,Integer.parseInt(ic_producto_nafin));
			ps.setInt(3,Integer.parseInt(ic_epo));
			ps.executeUpdate();
			ps.close();
			resultado = true;
		} catch(Exception e){
			System.out.println("Dispersion::setParamFacturaxProductoEpo Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(resultado);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::setParamFacturaxProductoEpo(S)");
		}
	}//setParamFacturaxProductoEpo

	public String getParamFacturaxProductoEpo(String ic_producto_nafin, String ic_epo)
			throws NafinException {
		System.out.println("Dispersion::getParamFacturaxProductoEpo(E) ");
		String				qrySentencia	= "";
		AccesoDB			con				= null;
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		String				fact_tipo_int	= "";
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia  =
				" SELECT cs_fact_tipo_int"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = ?"  +
				"   AND  ic_epo = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_producto_nafin));
			ps.setInt(2,Integer.parseInt(ic_epo));
			//System.out.println(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next())
				fact_tipo_int = rs.getString(1)==null?"":rs.getString(1);
			rs.close();
			if(ps!=null) ps.close();
		} catch(Exception e){
			System.out.println("Dispersion::getParamFacturaxProductoEpo Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getParamFacturaxProductoEpo(S)");
		}
		return fact_tipo_int;
	}//getParamFacturaxProductoEpo


	private String getTipoInteresFacura(String ic_producto_nafin, String ic_epo, AccesoDB con)
			throws NafinException {
		System.out.println("Dispersion::getTipoInteresFacura(E) ");
		String				qrySentencia	= "";
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		String				fact_tipo_int	= "";
		try {
			qrySentencia  =
				" SELECT /*+index(cpe CP_COMREL_PRODUCTO_EPO_PK)*/"   +
				"         DECODE (cpn.cs_fact_epo, 'E', NVL (cpe.cs_fact_tipo_int, cpn.cs_fact_tipo_int), cpn.cs_fact_tipo_int) AS tipointeres"   +
				"   FROM comcat_producto_nafin cpn, "   +
				"   	   comrel_producto_epo cpe"   +
				"  WHERE cpn.ic_producto_nafin = cpe.ic_producto_nafin "   +
				"   AND cpn.ic_producto_nafin = ? "   +
				"   AND cpe.ic_epo = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_producto_nafin));
			ps.setInt(2,Integer.parseInt(ic_epo));
			//System.out.println(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next())
				fact_tipo_int = rs.getString(1)==null?"":rs.getString(1);
			rs.close();
			if(ps!=null) ps.close();
		} catch(Exception e){
			System.out.println("Dispersion::getTipoInteresFacura Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("Dispersion::getTipoInteresFacura(S)");
		}
		return fact_tipo_int;
	}//getTipoInteresFacura

	public HashMap getDetalleOperacion(String ic_flujo_fondos, String tabla)
			throws NafinException {
		System.out.println("Dispersion::getDetalleOperacion(E)");
		AccesoDB			con 			= null;
		ResultSet			rs				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps 				= null;
		HashMap 			detalle			= new HashMap();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT idcampo_cen_i, valor_edt_s"   +
				"   FROM cfe"+tabla+"_d_edetalle"   +
				"  WHERE idoperacion_cen_i = ?"  ;
//System.out.println("<br> qrySentencia: "+qrySentencia+ " "+ic_flujo_fondos );
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong(1, Long.parseLong(ic_flujo_fondos));
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()){
				String rs_campo = rs.getString("idcampo_cen_i")==null?"":rs.getString("idcampo_cen_i");
				String rs_valor = rs.getString("valor_edt_s")==null?"":rs.getString("valor_edt_s");
				detalle.put(rs_campo, rs_valor);
			}//while
			rs.close();
			if(ps != null) ps.close();
		} catch (Exception e) {
			System.out.println("Dispersion::getDetalleOperacion(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("Dispersion::getDetalleOperacion(S)");
		}
		return detalle;
	}

	public Vector getDetalleDoctos(
					String ic_flujo_fondos, 	String tipo_disp, 		String estatus,
					String ic_epo, 				String ic_pyme, 		String ic_if,
					String ic_estatus_docto, 	String df_fecha_venc, 	String df_operacion)
			throws NafinException {
		System.out.println("Dispersion::getDetalleDoctos(E)");
		AccesoDB			con 			= null;
		ResultSet			rs				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps 				= null;
		Vector	 			renglones			= new Vector();
		Vector	 			columnas			= new Vector();
		try {
			con = new AccesoDB();
			con.conexionDB();
			if("P".equals(tipo_disp) && "2".equals(estatus)) {
				qrySentencia =
					" SELECT doc.ic_documento, doc.ig_numero_docto,"   +
					"        pym.cg_razon_social AS pyme, doc.fn_monto AS Monto,"   +
					"        '' AS Importe, doc.ic_moneda"   +
					"   FROM dis_documento doc, comcat_pyme pym, comrel_documento_ff dff"   +
					"  WHERE doc.ic_documento = dff.ic_documento"   +
					"    AND doc.ic_pyme = pym.ic_pyme"   +
					"    AND dff.ic_producto_nafin = 4"   +
					"    AND dff.ic_documento is not null"   +
					"    AND dff.ic_flujo_fondos = ?"  ;
			} else if("P".equals(tipo_disp) && "PA".equals(estatus)) {
				qrySentencia =
					" SELECT pag.ic_pago, TO_CHAR (pag.df_fecha_aplicacion, 'dd/mm/yyyy'),"   +
					"        pym.cg_razon_social AS pyme, pag.fn_importe AS monto, '' AS importe,"   +
					"        pag.ic_moneda"   +
					"   FROM dis_pagos_ant pag, comcat_pyme pym, comrel_documento_ff dff"   +
					"  WHERE pag.ic_pago = dff.ic_pago"   +
					"    AND pag.ic_pyme = pym.ic_pyme"   +
					"    AND dff.ic_producto_nafin = 4"   +
					"    AND dff.ic_documento is null"   +
					"    AND dff.ic_flujo_fondos = ?"  ;
			} else {
				String montos = "doc.fn_monto as Monto, sel.in_importe_recibir as Importe";

				if("16".equals(ic_epo)&&"4".equals(ic_estatus_docto)&&!"".equals(ic_pyme))
					montos = "doc.fn_monto Monto, sel.in_importe_recibir - NVL (sel.fn_importe_recibir_benef, 0) Importe";
				else if("16".equals(ic_epo)&&"4".equals(ic_estatus_docto)&&!"".equals(ic_if))
					montos = "doc.fn_monto Monto, NVL (sel.fn_importe_recibir_benef, 0) Importe";
				else if(("1".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"10".equals(ic_estatus_docto))&&!"".equals(ic_pyme)&&!"".equals(df_fecha_venc))
					montos = "doc.fn_monto - NVL ((doc.fn_monto * NVL (doc.fn_porc_beneficiario, 0) / 100), 0) Monto, doc.fn_monto - NVL ((doc.fn_monto * NVL (doc.fn_porc_beneficiario, 0) / 100), 0) Importe";
				else if(("1".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"10".equals(ic_estatus_docto))&&!"".equals(ic_if)&&!"".equals(df_fecha_venc))
					montos = "( doc.fn_monto * NVL (doc.fn_porc_beneficiario, 0) / 100 ) Monto, ( doc.fn_monto * NVL (doc.fn_porc_beneficiario, 0) / 100 ) Importe";

				qrySentencia =
					" SELECT doc.ic_documento, doc.ig_numero_docto,"   +
					"        pym.cg_razon_social AS pyme, doc.ic_moneda, "   +
					montos+
					"        ,doc.cs_dscto_especial"+
					"   FROM com_documento doc,"   +
					"        com_docto_seleccionado sel,"   +
					"        comcat_pyme pym,"   +
					"        comrel_documento_ff dff"   +
					"  WHERE doc.ic_documento = dff.ic_documento"   +
					"    AND doc.ic_documento = sel.ic_documento (+)"   +
					"    AND doc.ic_pyme = pym.ic_pyme "  +
					"    AND dff.ic_producto_nafin = 1"  +
					"    AND dff.ic_flujo_fondos = ?"  +
					"  ORDER BY doc.cs_dscto_especial DESC, doc.ic_documento"  ;
			}
			System.out.println("<br> qrySentencia: "+qrySentencia+ " "+ic_flujo_fondos );
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong(1, Long.parseLong(ic_flujo_fondos));
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()){
				String rs_documento		= rs.getString("ic_documento")==null?"":rs.getString("ic_documento");
				String rs_numero_docto 	= rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto");
				String rs_pyme 			= rs.getString("pyme")==null?"":rs.getString("pyme");
				String rs_monto 		= rs.getString("Monto")==null?"0":rs.getString("Monto");
				String rs_importe 		= rs.getString("Importe")==null?"0":rs.getString("Importe");
				String rs_moneda 		= rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
				String rs_dscto_especial= rs.getString("cs_dscto_especial")==null?"":rs.getString("cs_dscto_especial");
				columnas = new Vector();
				columnas.addElement(rs_documento);
				columnas.addElement(rs_numero_docto);
				columnas.addElement(rs_pyme);
				columnas.addElement(rs_monto);
				columnas.addElement(rs_importe);
				columnas.addElement(rs_moneda);
				columnas.addElement(rs_dscto_especial);
				renglones.addElement(columnas);
			}//while
			rs.close();
			if(ps != null) ps.close();
		} catch (Exception e) {
			System.out.println("Dispersion::getDetalleDoctos(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("Dispersion::getDetalleDoctos(S)");
		}
		return renglones;
	}

	public int getExistenDoctosRelacionados(String ic_flujo_fondos)
			throws NafinException {
		System.out.println("Dispersion::getExisteDetalleHist(E)");
		AccesoDB			con 			= null;
		ResultSet			rs				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps 				= null;
		int					existe			= 0;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM comrel_documento_ff"   +
				"  WHERE ic_flujo_fondos = ?"  ;
			//System.out.println("<br> qrySentencia: "+qrySentencia+ " "+ic_flujo_fondos );
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong(1, Long.parseLong(ic_flujo_fondos));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				existe = rs.getInt(1);
			}//while
			rs.close();
			if(ps != null) ps.close();
		} catch (Exception e) {
			System.out.println("Dispersion::getExisteDetalleHist(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getExisteDetalleHist(S)");
		}
		return existe;
	}

	public int getExisteDetalleHist(String ic_flujo_fondos, String tabla)
			throws NafinException {
		System.out.println("Dispersion::getExisteDetalleHist(E)");
		AccesoDB			con 			= null;
		ResultSet			rs				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps 				= null;
		int					existe			= 0;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM cfe"+tabla+"_d_edetalle"   +
				"  WHERE idoperacion_cen_i = ?"  ;
			//System.out.println("<br> qrySentencia: "+qrySentencia+ " "+ic_flujo_fondos );
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong(1, Long.parseLong(ic_flujo_fondos));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				existe = rs.getInt(1);
			}//while
			rs.close();
			if(ps != null) ps.close();
		} catch (Exception e) {
			System.out.println("Dispersion::getExisteDetalleHist(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getExisteDetalleHist(S)");
		}
		return existe;
	}

	public void eliminarCuenta(String ic_cuenta, String ic_usuario, HashMap parametrosAdicionales ) throws NafinException {
		System.out.println("DispersionEJB::eliminarCuenta(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		boolean				bOk				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();

			String rs_nafin_electronico = "";
			String rs_cuenta = "";
			String rs_producto_nafin = "";
			String rs_moneda = "";
			String rs_bancos_tef = "";
			String rs_estatus_tef = "";
			String rs_estatus_cecoban = "";
			String rs_tipo_cuenta = "";
			String rs_linea_credito = "";
			String rs_epo = "";
			String rs_plaza_swift = "";
			String rs_sucursal_swift = "";
			String rs_tipo_afiliado = "";

			String valoresActuales = "";

			qrySentencia =
				" SELECT ic_nafin_electronico, cg_cuenta, ic_producto_nafin, ic_moneda,"   +
				"        ic_bancos_tef, ic_estatus_tef, ic_estatus_cecoban, ic_tipo_cuenta,"   +
				"        ic_linea_credito, ic_epo, cg_plaza_swift, cg_sucursal_swift, cg_tipo_afiliado "   +
				"   FROM com_cuentas"   +
				"  WHERE ic_cuenta = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString( 1, ic_cuenta);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){

				rs_nafin_electronico = rs.getString(1)==null?"":rs.getString(1);
				rs_cuenta 				= rs.getString(2)==null?"":rs.getString(2);
				rs_producto_nafin 	= rs.getString(3)==null?"":rs.getString(3);
				rs_moneda 				= rs.getString(4)==null?"":rs.getString(4);
				rs_bancos_tef 			= rs.getString(5)==null?"":rs.getString(5);
				rs_estatus_tef 		= rs.getString(6)==null?"":rs.getString(6);
				rs_estatus_cecoban 	= rs.getString(7)==null?"":rs.getString(7);
				rs_tipo_cuenta 		= rs.getString(8)==null?"":rs.getString(8);
				rs_linea_credito 		= rs.getString(9)==null?"":rs.getString(9);
				rs_epo 					= rs.getString(10)==null?"":rs.getString(10);
				rs_plaza_swift			= rs.getString("cg_plaza_swift")    == null?"":rs.getString("cg_plaza_swift");
				rs_sucursal_swift		= rs.getString("cg_sucursal_swift") == null?"":rs.getString("cg_sucursal_swift");
				rs_tipo_afiliado		= rs.getString("cg_tipo_afiliado")  == null?"":rs.getString("cg_tipo_afiliado");

				//boolean esCuentaCLABE = "40".equals(rs_tipo_cuenta)?true:false;
				boolean esCuentaSWIFT = "50".equals(rs_tipo_cuenta)?true:false;

				valoresActuales =
					"cg_cuenta="+rs_cuenta+"\n"+
					"ic_producto_nafin="+rs_producto_nafin+"\n"+
					"ic_moneda="+rs_moneda+"\n"+
					"ic_bancos_tef="+rs_bancos_tef+"\n"+
					"ic_estatus_tef="+rs_estatus_tef+"\n"+
					"ic_estatus_cecoban="+rs_estatus_cecoban+"\n"+
					"ic_tipo_cuenta="+rs_tipo_cuenta+"\n"+
					"ic_linea_credito="+rs_linea_credito+"\n"+
					"cg_plaza_swift="+(esCuentaSWIFT?rs_plaza_swift:"N/A")+"\n"+
					"cg_sucursal_swift="+(esCuentaSWIFT?rs_sucursal_swift:"N/A")+"\n"+
					"ic_epo="+rs_epo+"\n";

			}
			rs.close();
			if(ps != null) ps.close();


			qrySentencia =
				" DELETE com_cuentas"   +
				"  WHERE ic_cuenta = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString( 1, ic_cuenta);
			ps.executeUpdate();
			ps.close();

			parametrosAdicionales 	= parametrosAdicionales == null?new HashMap():parametrosAdicionales;
			String claveEliminar  	= (String) parametrosAdicionales.get("DESCRIPCION_ELIMINAR");
			claveEliminar 				= claveEliminar == null?"Registro Eliminado\nNO DISPONIBLE":claveEliminar;

			Bitacora.grabarEnBitacora(con, "CONSCTAS",
					"B".equals(rs_tipo_afiliado)?"I":rs_tipo_afiliado,			//tipoAfiliado; Si el tipo es "B" usar "I"
					rs_nafin_electronico, ic_usuario,
					valoresActuales, claveEliminar);

		} catch(Exception e) {
			bOk = false;
			System.out.println("DispersionEJB::eliminarCuenta(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionEJB::eliminarCuenta(S)");
		}
	}//eliminarCuenta

	public void reasignarCuenta(String ic_cuenta, String ic_epo, String ic_usuario, HashMap parametrosAdicionales) throws NafinException {
		System.out.println("DispersionEJB::reasignarCuenta(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		boolean				bOk				= true;

		parametrosAdicionales 	= parametrosAdicionales == null?new HashMap():parametrosAdicionales;

		try {
			con = new AccesoDB();
			con.conexionDB();

			String rs_nafin_electronico = "";
			String rs_cuenta = "";
			String rs_producto_nafin = "";
			String rs_moneda = "";
			String rs_epo = "";
			String rs_tipo_afiliado = "";

			String valoresActuales = "";
			String valoresNuevos = "";



			qrySentencia =
				" SELECT ic_nafin_electronico, cg_cuenta, ic_producto_nafin, ic_moneda, ic_epo,"   +
				"        cg_tipo_afiliado"   +
				"   FROM com_cuentas"   +
				"  WHERE ic_cuenta = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString( 1, ic_cuenta);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				rs_nafin_electronico 	= rs.getString(1)==null?"":rs.getString(1);
				rs_cuenta 				= rs.getString(2)==null?"":rs.getString(2);
				rs_producto_nafin 		= rs.getString(3)==null?"":rs.getString(3);
				rs_moneda 				= rs.getString(4)==null?"":rs.getString(4);
				rs_epo 					= rs.getString(5)==null?"":rs.getString(5);
				rs_tipo_afiliado		= rs.getString(6)==null?"":rs.getString(6);

				valoresActuales =
					"cg_cuenta="+rs_cuenta+"\n"+
					"ic_producto_nafin="+rs_producto_nafin+"\n"+
					"ic_moneda="+rs_moneda+"\n"+
					"ic_epo="+rs_epo;

				String claveReasignacion  	= (String) parametrosAdicionales.get("DESCRIPCION_REASIGNACION");
				claveReasignacion 			= claveReasignacion == null?"REASIGNACION\nNO DISPONIBLE":claveReasignacion;

				valoresNuevos =
					claveReasignacion+"\n" +
					"cg_cuenta="+rs_cuenta+"\n"+
					"ic_producto_nafin="+rs_producto_nafin+"\n"+
					"ic_moneda="+rs_moneda+"\n"+
					"ic_epo="+ic_epo;

			}
			rs.close();
			if(ps != null) ps.close();

			int existe = 0;
			qrySentencia =
				" SELECT COUNT (1) existe"   +
				"   FROM com_cuentas"   +
				"  WHERE ic_nafin_electronico = ? "   +
				"    AND ic_epo = ?"  +
				"    AND ic_producto_nafin = ? "  +
				"    AND cg_tipo_afiliado = ? "  +
				"	  AND ic_moneda = ? ";
System.out.println("\n qrySentencia: "+qrySentencia);
System.out.println("\n rs_nafin_electronico: "+rs_nafin_electronico);
System.out.println("\n rs_epo: "+ic_epo);
System.out.println("\n rs_producto_nafin: "+rs_producto_nafin);
System.out.println("\n rs_tipo_afiliado: "+rs_tipo_afiliado);
System.out.println("\n rs_moneda: "+rs_moneda);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(rs_nafin_electronico));
			ps.setInt( 2, Integer.parseInt(ic_epo));
			ps.setInt( 3, Integer.parseInt(rs_producto_nafin));
			ps.setString( 4, rs_tipo_afiliado);
			ps.setInt( 5, Integer.parseInt(rs_moneda));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				existe = rs.getInt(1);
			}
			rs.close();ps.close();
			if(existe>0)
				throw new NafinException("SIST0001"); //SOLO PUEDE HABER UNA CUENTA POR EPO PARA CADA PYME


			qrySentencia =
				" UPDATE com_cuentas"   +
				"    SET ic_epo = ?"   +
				"  WHERE ic_cuenta = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString( 1, ic_epo);
			ps.setString( 2, ic_cuenta);
			ps.executeUpdate();
			ps.close();


			Bitacora.grabarEnBitacora(con, "CONSCTAS", "P",
					rs_nafin_electronico, ic_usuario,
					valoresActuales, valoresNuevos);

		} catch(Exception e) {
			bOk = false;
			System.out.println("DispersionEJB::reasignarCuenta(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionEJB::reasignarCuenta(S)");
		}
	}//eliminarCuenta

	public int getExisteCuenta(String ic_cuenta)
			throws NafinException {
		System.out.println("Dispersion::getExisteCuenta(E)");
		AccesoDB			con 			= null;
		ResultSet			rs				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps 				= null;
		int					existe			= 0;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM com_dispersion"   +
				"  WHERE ic_cuenta = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_cuenta));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				existe += rs.getInt(1);
			}//while
			rs.close(); ps.close();

			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM com_dispersion_epo"   +
				"  WHERE ic_cuenta = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_cuenta));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				existe += rs.getInt(1);
			}//while
			rs.close(); ps.close();

			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM int_flujo_fondos"   +
				"  WHERE ic_cuenta = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_cuenta));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				existe += rs.getInt(1);
			}//while
			rs.close(); ps.close();
		} catch (Exception e) {
			System.out.println("Dispersion::getExisteCuenta(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getExisteCuenta(S)");
		}
		return existe;
	}

	/**
	 *	Devuelve las Tarifas de la EPO en Moneda Nacional
	 * @param ic_epo Clave de la EPO
	 * @return Lista con las tarifas de la EPO para cada uno de los dias configurados
	 */
	public List getTarifaEpo(String ic_epo) throws NafinException {
		System.out.println("Dispersion::getTarifaEpo(E)");
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		List 		lDatos			= new ArrayList();
		try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia =
				" SELECT   ig_dias, fg_tarifa, 'Dispersion::getTarifaEpo()'"   +
				"     FROM comrel_epo_tarifa"   +
				"    WHERE 	ic_epo 		= ? "   +
				"		AND 	ic_moneda 	= ? "  +
				" ORDER BY ig_dias"  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("1")); // Moneda Nacional
			lDatos = con.consultaDB(qrySentencia, lVarBind, false);

		} catch(Exception e) {
			System.out.println("Dispersion::getTarifaEpo(Exception) "+e);
			e.printStackTrace();
			lDatos = new ArrayList();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getTarifaEpo(S)");
		}
		return lDatos;
	}//getDisposicionesPorAutorizar

	/**
	 *	Devuelve las Tarifas de la EPO en Dolares Americanos
	 * @param ic_epo Clave de la EPO
	 * @return Lista con las tarifas de la EPO para cada uno de los dias configurados
	 */
	public List getTarifaEpoUSD(String ic_epo) throws NafinException {
		log.info("getTarifaEpoUSD(E)");
		AccesoDB	con				= null;
      String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		List 		lDatos			= new ArrayList();
		try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia =
				" SELECT   ig_dias, fg_tarifa, 'Dispersion::getTarifaEpo()'"   +
				"     FROM comrel_epo_tarifa"   +
				"    WHERE 	ic_epo 		= ? "   +
				"		AND 	ic_moneda 	= ? "  +
				" ORDER BY ig_dias"  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("54")); // Dolares Americanos
			lDatos = con.consultaDB(qrySentencia, lVarBind, false);

		} catch(Exception e) {
			log.debug("getTarifaEpoUSD(Exception) "+e);
			log.debug("getTarifaEpoUSD.ic_epo = "+ic_epo);
			e.printStackTrace();
			lDatos = new ArrayList();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getTarifaEpoUSD(S)");
		}
		return lDatos;
	}

	/**
	 * Asigna las tarificas especificadas (en Moneda Nacional) en el arreglo <tt>txt_tarifa</tt> correspondiente
	 * a los dias especificados en el arreglo <tt>txt_dia</tt> para la epo cuya clave se especifica
	 * en el parametro <tt>ic_epo</tt>.
	 */
	public void setTarifaEpo(String ic_epo, String txt_dia[], String txt_tarifa[]) throws NafinException {
		System.out.println("Dispersion::setTarifaEpo(E)");
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		int			registros		= 0;
		boolean		bOK				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();

			for(int i=0;i<txt_dia.length;i++) {
				qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setTarifaEpo()'"   +
					"   FROM comrel_epo_tarifa"   +
					"  WHERE ic_epo = ? AND ig_dias = ? AND ic_moneda = ? "  ;
				lVarBind = new ArrayList();
				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(new Integer(txt_dia[i]));
				lVarBind.add(new Integer("1")); // Moneda Nacional
				registros = con.existeDB(qrySentencia, lVarBind);
				if(registros==0) {
					qrySentencia =
						" INSERT INTO comrel_epo_tarifa"   +
						"             (ic_epo, ig_dias, fg_tarifa, ic_moneda )"   +
						"      VALUES (?, ?, ?, ?)"  ;
					lVarBind = new ArrayList();
					lVarBind.add(new Integer(ic_epo));
					lVarBind.add(new Integer(txt_dia[i]));
					lVarBind.add(new Double(txt_tarifa[i]));
					lVarBind.add(new Integer("1")); // Moneda Nacional
					registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
				} else {
					qrySentencia =
						" UPDATE comrel_epo_tarifa"   +
						"    SET fg_tarifa = ?"   +
						"  WHERE ic_epo = ? AND ig_dias = ? AND ic_moneda = ? "  ;
					lVarBind = new ArrayList();
					lVarBind.add(new Double(txt_tarifa[i]));
					lVarBind.add(new Integer(ic_epo));
					lVarBind.add(new Integer(txt_dia[i]));
					lVarBind.add(new Integer("1")); // Moneda Nacional
					registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
				}
			}//for(int i=0;i<txt_dia.length;i++)

		} catch(Exception e) {
			bOK = false;
			System.out.println("Dispersion::setTarifaEpo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::setTarifaEpo(S)");
		}
	}//getDisposicionesPorAutorizar

	/**
	 * Asigna las tarificas especificadas (en Dolares Americanos) en el arreglo <tt>txt_tarifa</tt> correspondiente
	 * a los dias especificados en el arreglo <tt>txt_dia</tt> para la epo cuya clave se especifica
	 * en el parametro <tt>ic_epo</tt>.
	 */
	public void setTarifaEpoUSD(String ic_epo, String txt_dia[], String txt_tarifa[]) throws NafinException {
		log.info("setTarifaEpoUSD(E)");
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		int			registros		= 0;
		boolean		bOK				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();

			for(int i=0;i<txt_dia.length;i++) {
				qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setTarifaEpo()'"   +
					"   FROM comrel_epo_tarifa"   +
					"  WHERE ic_epo = ? AND ig_dias = ? AND ic_moneda = ? "  ;
				lVarBind = new ArrayList();
				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(new Integer(txt_dia[i]));
				lVarBind.add(new Integer("54")); // Dolares Americanos
				registros = con.existeDB(qrySentencia, lVarBind);
				if(registros==0) {
					qrySentencia =
						" INSERT INTO comrel_epo_tarifa"   +
						"             (ic_epo, ig_dias, fg_tarifa, ic_moneda )"   +
						"      VALUES (?, ?, ?, ?)"  ;
					lVarBind = new ArrayList();
					lVarBind.add(new Integer(ic_epo));
					lVarBind.add(new Integer(txt_dia[i]));
					lVarBind.add(new Double(txt_tarifa[i]));
					lVarBind.add(new Integer("54")); // Dolares Americanos
					registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
				} else {
					qrySentencia =
						" UPDATE comrel_epo_tarifa"   +
						"    SET fg_tarifa = ?"   +
						"  WHERE ic_epo = ? AND ig_dias = ? AND ic_moneda = ? "  ;
					lVarBind = new ArrayList();
					lVarBind.add(new Double(txt_tarifa[i]));
					lVarBind.add(new Integer(ic_epo));
					lVarBind.add(new Integer(txt_dia[i]));
					lVarBind.add(new Integer("54")); // Dolares Americanos
					registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
				}
			}//for(int i=0;i<txt_dia.length;i++)

		} catch(Exception e) {
			bOK = false;
			log.debug("setTarifaEpoUSD(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setTarifaEpoUSD(S)");
		}
	}//getDisposicionesPorAutorizar

	/**
	 * Borra la tarifa configurada en Moneda Nacional que tenga el dia mas grande.
	 * @param ic_epo Clave de la Epo cuya tarifa sera borrada.
	 */
	public void delTarifaEpo(String ic_epo) throws NafinException {
		System.out.println("Dispersion::delTarifaEpo(E)");
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		int			registros		= 0;
		boolean		bOK				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT MAX (ig_dias) as existe, 'Dispersion::delTarifaEpo()'"   +
				"   FROM comrel_epo_tarifa"   +
				"  WHERE ic_epo = ? AND ic_moneda = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("1")); // Moneda Nacional
			registros = con.existeDB(qrySentencia, lVarBind);

			qrySentencia =
				" DELETE      comrel_epo_tarifa"   +
				"       WHERE ic_epo = ? AND ig_dias = ? AND ic_moneda = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer(registros));
			lVarBind.add(new Integer("1")); // Moneda Nacinal
			registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			bOK = false;
			System.out.println("Dispersion::delTarifaEpo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::delTarifaEpo(S)");
		}
	}//getDisposicionesPorAutorizar

	/**
	 * Borra la tarifa configurada en Dolares Americanos que tenga el dia mas grande.
	 * @param ic_epo Clave de la Epo cuya tarifa sera borrada.
	 */
	public void delTarifaEpoUSD(String ic_epo) throws NafinException {
		log.info("delTarifaEpoUSD(E)");
		AccesoDB	con				= null;
        String		qrySentencia    = "";
		List 		lVarBind		= new ArrayList();
		int			registros		= 0;
		boolean		bOK				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT MAX (ig_dias) as existe, 'Dispersion::delTarifaEpo()'"   +
				"   FROM comrel_epo_tarifa"   +
				"  WHERE ic_epo = ? AND ic_moneda = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("54")); // Moneda Nacional
			registros = con.existeDB(qrySentencia, lVarBind);

			qrySentencia =
				" DELETE      comrel_epo_tarifa"   +
				"       WHERE ic_epo = ? AND ig_dias = ? AND ic_moneda = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer(registros));
			lVarBind.add(new Integer("54")); // Moneda Nacinal
			registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			bOK = false;
			log.debug("delTarifaEpoUSD(Exception) "+e);
			log.debug("delTarifaEpoUSD.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("delTarifaEpoUSD(S)");
		}
	}

	public void getSuceptibleDispersion(String ic_epo) throws NafinException {
		System.out.println("Dispersion::getSuceptibleDispersion(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT COUNT (1) existe, 'Dispersion::getSuceptibleDispersion()'"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_producto_nafin = ?"   +
				"    AND ic_epo = ?"   +
				"    AND cg_dispersion = ?"  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(1));
			lVarBind.add(new Long(ic_epo));
			lVarBind.add("S");
			registros = con.consultarDB(qrySentencia, lVarBind);
			if(registros.next()) {
				int total = Integer.parseInt(registros.getString("existe"));
				if (total==0) {
					throw new NafinException("DSCT0018");
				}
			}
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			System.out.println("Dispersion::getSuceptibleDispersion(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getSuceptibleDispersion(S)");
		}
	}//getSuceptibleDispersion

  /****************J.Angel.Haro.Juarez**********************/
  // AHJ 26/11/2007  Lee los datos del archivo txt de la carga masiva para bancomext y los
  // almacenara en la tabla tmp.

  public String setDatosTmpCLABEbncmxt(String ic_proc_carga, List lRegistros) throws NafinException {
		System.out.println("Dispersion::setDatosTmpCLABEbncmxt(E)");
		AccesoDB 			con				= new AccesoDB();
		boolean				bOk				= true;
		Registros			registros;
		String				qrySentencia;
		List 				lVarBind;
   // System.out.println("IC PROCESO DE CARGA::::::"+ic_proc_carga);
    System.out.println("LISTA DE REGISTROS::::::"+lRegistros);
		try {
			con.conexionDB();

			if("0".equals(ic_proc_carga)) {
				qrySentencia =
					" SELECT SEQ_COMTMP_CUENTAS_BXT.NEXTVAL ic_proc_solic, 'Dispersion::setDatosTmpCLABEbncmxt()' "   +
					"   FROM DUAL"  ;
          //System.out.println("#####################setDatosTmpCLABEbncmxt 01###############################");
         // System.out.println("PRIMER QUERY::::"+qrySentencia);
        //  System.out.println("####################setDatosTmpCLABEbncmxt 01################################");
				registros = con.consultarDB(qrySentencia);
				if(registros.next()) {
					ic_proc_carga = registros.getString("ic_proc_solic");
				}
			}

			String ic_cuenta = "1";
			qrySentencia =
				" SELECT NVL (MAX (ic_cuenta) + 1, 1) ic_cuenta, 'Dispersion::setDatosTmpCLABEbncmxt()'"   +
				"   FROM COMTMP_CUENTAS_BXT"   +
				"  WHERE ic_proc_carga = ?"  ;
          //System.out.println("#####################setDatosTmpCLABEbncmxt 02###############################");
          //System.out.println("SEGUNDO QUERY::::"+qrySentencia);
        ///  System.out.println("####################setDatosTmpCLABEbncmxt 02################################");
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia, lVarBind);
			if(registros.next()) {
				ic_cuenta = registros.getString("ic_cuenta");
			}
			//System.out.println("NUMERO DE CUENTA::::::"+ic_cuenta);
			qrySentencia =
                    "INSERT INTO COMTMP_CUENTAS_BXT ( " +
                    "     IC_PROC_CARGA,  " +         //01
                    "     IC_CUENTA,  " +             //02
                    "     IC_EPO,  " +                //03
                    "     CG_RAZON_SOCIAL,  " +       //04
                    "     IC_NAFIN_ELECTRONICO,  " +  //05
                    "     CG_TIPO_AFILIADO,  " +      //06
                    "     CG_CUENTA,  " +             //07
                    "     IC_BANCOS_TEF,  " +         //08
                    "     IC_MONEDA,  " +             //09
                    "     CG_ESTATUS,  " +            //10
                    "     CG_ERROR)  " +              //11
                    "   VALUES ( ? ,? ,? , " +
                    "         ? ,? ,? , " +
                    "         ? ,? ,? , " +
                    "         ? ,? )" ;
          //System.out.println("#####################setDatosTmpCLABEbncmxt 03###############################");
         // System.out.println("**************************TERCER QUERY INSERT::::"+qrySentencia);
       //   System.out.println("####################setDatosTmpCLABEbncmxt 03################################");
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));//01
			lVarBind.add(new Long(ic_cuenta));		//02
			String 	campo;
			String 	layout[]		= {"EPO", "Nombre o Raz�n Social", "Numero Electronico", "Tipo de Afiliado", "Cuenta CLABE", "Banco de Servicio", "Moneda"};
			int 	longcampo[] 	= {3,100, 38, 1, 18, 3, 2};
			String 	cg_estatus		= "S",
					cg_error		= "";
			if(lRegistros.size()!=layout.length) {
				cg_estatus 	= "N";
				cg_error 	= "El registro no coincide con el layout. ";
			}

			for(int i=0;i<layout.length;i++) {
				campo = (lRegistros.size()>=(i+1))?lRegistros.get(i).toString().trim():"";
				if(campo.length()>longcampo[i]) {
					cg_estatus	= "N";
					cg_error	+= "El campo "+layout[i]+" excede la longitud permitida ("+longcampo[i]+"). ";
					campo = campo.substring(0, longcampo[i]);
				}
				lVarBind.add(campo);                //03 - 09
			}//for(int i=0;i<=layout.size();i++)
			lVarBind.add(cg_estatus);             //10
			lVarBind.add(cg_error);               //11
   //   System.out.println("##############___________________############### VARIABLES BIND ################__________________##########"+lVarBind);
			con.ejecutaUpdateDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("Dispersion::setDatosTmpCLABE(Exception) "+e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::setDatosTmpCLABE(S)");
		}
		return ic_proc_carga;
	}//setDatosTmpCLABEbncmxt



  /****************J.Angel.Haro.Juarez**********************/
  // J.Angel.Haro.Juarez 27/12/2007  Lee los datos del archivo txt de la carga masiva de verificacion CLABE para bancomext y los
  // almacenara en la tabla tmp.

  public String setDatosTmpValidaCLABEbncmxt(String ic_proc_carga, List lRegistros) throws NafinException {
		System.out.println("Dispersion::setDatosTmpValidaCLABEbncmxt(E)");
		AccesoDB 			con				= new AccesoDB();
		boolean				bOk				= true;
		Registros			registros;
		String				qrySentencia;
		List 				lVarBind;
    //System.out.println("___________________________IC PROCESO DE CARGA::::::"+ic_proc_carga);
    //System.out.println("___________________________LISTA DE REGISTROS::::::"+lRegistros);
		try {
			con.conexionDB();

			if("0".equals(ic_proc_carga)) {
				qrySentencia =
					" SELECT SEQ_COMTMP_CUENTAS_BXT.NEXTVAL ic_proc_solic, 'Dispersion::setDatosTmpValidaCLABEbncmxt()' "   +
					"   FROM DUAL"  ;
        //  System.out.println("#####################setDatosTmpValidaCLABEbncmxt 01###############################");
        //  System.out.println("PRIMER QUERY::::"+qrySentencia);
        //  System.out.println("####################setDatosTmpValidaCLABEbncmxt 01################################");
				registros = con.consultarDB(qrySentencia);
				if(registros.next()) {
					ic_proc_carga = registros.getString("ic_proc_solic");
				}
			}
			/*// SE OBTIENE EL IDENTIFICADOR DE LA CUENTA //*/
			String ic_cuenta = "1";
			qrySentencia =
				" SELECT NVL (MAX (ic_cuenta) + 1, 1) ic_cuenta, 'Dispersion::setDatosTmpValidaCLABEbncmxt()'"   +
				"   FROM COMTMP_CUENTAS_BXT"   +
				"  WHERE ic_proc_carga = ?"  ;
         // System.out.println("#####################setDatosTmpValidaCLABEbncmxt 02###############################");
         // System.out.println("SEGUNDO QUERY::::"+qrySentencia);
         // System.out.println("#####################setDatosTmpValidaCLABEbncmxt 02###############################");
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia, lVarBind);
			if(registros.next()) {
				ic_cuenta = registros.getString("ic_cuenta");
			}
			System.out.println("NUMERO DE CUENTA::::::"+ic_cuenta);
      qrySentencia =
                    "INSERT INTO COMTMP_CUENTAS_BXT ( " +
                    "     IC_PROC_CARGA,  " +         //01
                    "     IC_CUENTA,  " +             //02
                    "     CG_RAZON_SOCIAL,  " +       //04
                    "     CG_RFC, " +                 //10
                    "     CG_CUENTA,  " +             //07
                    "     IC_BANCOS_TEF,  " +         //08
                    "     IC_MONEDA,  " +             //09
                    "     IC_NAFIN_ELECTRONICO,  " +  //05
                    "     CG_TIPO_AFILIADO,  " +      //06
                    "     CG_ESTATUS,  " +            //11
                    "     CG_ERROR)  " +              //12
                    "   VALUES ( ? ,? , " +
                    "            ? ,? ,? , " +
                    "            ? ,? ,? , " +
                    "            ? ,? ,?  )" ;
        //  System.out.println("#####################setDatosTmpValidaCLABEbncmxt 03###############################");
        //  System.out.println("TERCER QUERY INSERT::::"+qrySentencia);
        //  System.out.println("####################setDatosTmpValidaCLABEbncmxt 03################################");
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));//01
			lVarBind.add(new Long(ic_cuenta));		//02
			String 	campo;
			String 	layout[]		= {"Nombre o Raz�n Social","RFC","Cuenta CLABE","Banco de Servicio","Moneda","Numero Electronico","Tipo de Afiliado"};
			int 	longcampo[] 	= {100,15,18,3,2,38,1};
			String 	cg_estatus		= "S",
					cg_error		= "";
			if(lRegistros.size()!=layout.length) {
				cg_estatus 	= "N";
				cg_error 	= "El registro no coincide con el layout. ";
			}
			//System.out.println("##################################################TAMA�O DEL LAYOT:::::::"+layout.length);
			for(int i=0;i<layout.length;i++) {
				campo = (lRegistros.size()>=(i+1))?lRegistros.get(i).toString().trim():"";
				if(campo.length()>longcampo[i]) {
					cg_estatus	= "N";
					cg_error	+= "El campo "+layout[i]+" excede la longitud permitida ("+longcampo[i]+"). ";
					campo = campo.substring(0, longcampo[i]);
				}
      //  System.out.println("##################################################CAMPOS A AGREGAR:::::::"+campo+",,,"+i);
				lVarBind.add(campo);                //03 - 10
			}//for(int i=0;i<=layout.size();i++)
			lVarBind.add(cg_estatus);             //11
			lVarBind.add(cg_error);               //12
        //System.out.println("##################################################QUERY:::::::"+qrySentencia);
        //System.out.println("##################################################VARIABLES:::::::"+lVarBind);
			con.ejecutaUpdateDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("Dispersion::setDatosTmpValidaCLABE(Exception) "+e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::setDatosTmpValidaCLABE(S)");
		}
		return ic_proc_carga;
	}//setDatosTmpValidaCLABEbncmxt



  /****************J.Angel.Haro.Juarez**********************/
  // J.Angel.Haro.Juarez 26/11/2007  Extrae las estadisticas que son mostradas en el avance del proceso

  public Registros getEstatusCargaCLABEbncmxt(String ic_proc_carga) throws NafinException {
		System.out.println("Dispersion::getEstatusCargaCLABEbncmxt(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT cg_estatus_proc, NVL (ig_num_procesados, 0) ig_num_procesados, 'Dispersion::getEstatusCargaCLABEbncmxt()'"   +
				"   FROM BIT_TMP_CUENTAS_BXT"   +
				"  WHERE ic_proc_carga = ?"  ;
        //System.out.println("#####################getEstatusCargaCLABEbncmxt 01###############################");
        //System.out.println("PRIMER QUERY:::"+qrySentencia);
        //System.out.println("#####################getEstatusCargaCLABEbncmxt 01###############################");
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("Dispersion::getEstatusCargaCLABEbncmxt(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getEstatusCargaCLABEbncmxt(S)");
		}
		return registros;
	}//getEstatusCargaCLABEbncmxt



  /****************J.Angel.Haro.Juarez**********************/
  // J.Angel.Haro.Juarez 27/12/2007  Extrae las estadisticas que son mostradas en el avance del proceso

  public Registros getEstatusCargaVerificaCLABEbncmxt(String ic_proc_carga) throws NafinException {
		System.out.println("Dispersion::getEstatusCargaVerificaCLABEbncmxt(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT cg_estatus_proc, NVL (ig_num_procesados, 0) ig_num_procesados, 'Dispersion::getEstatusCargaVerificaCLABEbncmxt()'"   +
				"   FROM BIT_TMP_CUENTAS_BXT"   +
				"  WHERE ic_proc_carga = ?"  ;
        //System.out.println("#####################getEstatusCargaVerificaCLABEbncmxt 01###############################");
        //System.out.println("PRIMER QUERY:::"+qrySentencia);
        //System.out.println("#####################getEstatusCargaVerificaCLABEbncmxt 01###############################");
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("Dispersion::getEstatusCargaVerificaCLABEbncmxt(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getEstatusCargaVerificaCLABEbncmxt(S)");
		}
		return registros;
	}//getEstatusCargaVerificaCLABEbncmxt


  /****************J.Angel.Haro.Juarez**********************/
  // J.Angel.Haro.Juarez 26/11/2007  Seleccion de los registros que ya fueron leidos y validados

  public Registros getRegistrosValidadosbncmxt(String ic_proc_carga, String cg_estatus) throws NafinException {
		System.out.println("Dispersion::getRegistrosValidadosbncmxt(E)");
    //LEE LOS REGISTROS QUE FUERON VALIDAODS CORRECTAMENTE
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();
			qrySentencia =
                    "  SELECT  " +
                    "    CTA.IC_PROC_CARGA,  " +
                    "    CTA.IC_CUENTA,  " +
                    "    CTA.CG_RAZON_SOCIAL,  " +
                    "    CTA.IC_NAFIN_ELECTRONICO,  " +
                    "    CTA.CG_TIPO_AFILIADO,  " +
                    "    CTA.CG_CUENTA,  " +
                    "    CTA.IC_BANCOS_TEF,  " +
                    "    CTA.IC_MONEDA, " +
                    "    MON.CD_NOMBRE NOMBREMONEDA,  " +
                    "    CTA.IC_EPO,  " +
                    "    CTA.CG_ESTATUS,  " +
                    "    CTA.CG_ERROR, " +
                    "    'DISPERSION::GETREGISTROSVALIDADOSBNCMXT()' " +
                    "   FROM COMTMP_CUENTAS_BXT CTA,  " +
                    "        COMCAT_MONEDA MON  " +
                    "  WHERE CTA.IC_MONEDA = MON.IC_MONEDA (+)  " +
                    "    AND CTA.IC_PROC_CARGA = ?  " +
                    "    AND CTA.CG_ESTATUS = ? " +
                    "  ORDER BY CTA.IC_CUENTA ";
        System.out.println("#####################getRegistrosValidadosbncmxt 01###############################");
        System.out.println("PRIMER QUERY .. :::"+qrySentencia);
        System.out.println("#####################getRegistrosValidadosbncmxt 01###############################");
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			lVarBind.add(cg_estatus);
			registros = con.consultarDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("Dispersion::getRegistrosValidadosbncmxt(Exception) "+e);
      System.out.println("####################################################");
			e.printStackTrace();
      System.out.println("####################################################");
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getRegistrosValidadosbncmxt(S)");
		}
		return registros;
	}//getRegistrosValidadosbncmxt




/****************J.Angel.Haro.Juarez**********************/
  // J.Angel.Haro.Juarez 27/12/2007  Seleccion de los registros que ya fueron leidos y validados

  public Registros getRegistrosValidadosVerifbncmxt(String ic_proc_carga, String cg_estatus) throws NafinException {
		System.out.println("Dispersion::getRegistrosValidadosVerifbncmxt(E)");
    //LEE LOS REGISTROS QUE FUERON VALIDAODS CORRECTAMENTE
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();
			qrySentencia =
                    "  SELECT  " +
                    "    CTA.IC_PROC_CARGA,  " +
                    "    CTA.IC_CUENTA,  " +
                    "    CTA.CG_RAZON_SOCIAL,  " +
                    "    CTA.IC_NAFIN_ELECTRONICO,  " +
                    "    CTA.CG_TIPO_AFILIADO,  " +
                    "    CTA.CG_CUENTA,  " +
                    "    CTA.IC_BANCOS_TEF,  " +
                    "    CTA.IC_MONEDA, " +
                    "    CTA.CG_RFC, " +
                    "    MON.CD_NOMBRE NOMBREMONEDA,  " +
                    "    CTA.IC_EPO,  " +
                    "    CTA.CG_ESTATUS,  " +
                    "    CTA.CG_ERROR, " +
                    "    'DISPERSION::GETREGISTROSVALIDADOSBNCMXT()' " +
                    "   FROM COMTMP_CUENTAS_BXT CTA,  " +
                    "        COMCAT_MONEDA MON  " +
                    "  WHERE CTA.IC_MONEDA = MON.IC_MONEDA (+)  " +
                    "    AND CTA.IC_PROC_CARGA = ?  " +
                    "    AND CTA.CG_ESTATUS = ? " +
                    "  ORDER BY CTA.IC_CUENTA ";
        //System.out.println("#####################getRegistrosValidadosVerifbncmxt 01###############################");
        //System.out.println("PRIMER QUERY .. :::"+qrySentencia);
        //System.out.println("#####################getRegistrosValidadosVerifbncmxt 01###############################");
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			lVarBind.add(cg_estatus);
			registros = con.consultarDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("Dispersion::getRegistrosValidadosVerifbncmxt(Exception) "+e);
      //System.out.println("####################################################");
			e.printStackTrace();
      //System.out.println("####################################################");
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getRegistrosValidadosVerifbncmxt(S)");
		}
		return registros;
	}//getRegistrosValidadosVerifbncmxt


  /****************J.Angel.Haro.Juarez**********************/
  // J.Angel.Haro.Juarez 26/11/2007  Se insertan en la tabla definitiva los datos deidos del TXT despues de pasar por el proceso de validacion

  public void setCuentasClaveMasivabncmxt(Registros registros, String ic_epo, String ic_usuario) throws NafinException {
		System.out.println("Dispersion::setCuentasClaveMasivabncmxt(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			datos 			= new Registros();
		String				qrySentencia;
		List 				lVarBind;
    System.out.println("__________________________________ic_epo::::::::"+ic_epo+"__________________________________");
		boolean				bOk				= true;
		try {
			con.conexionDB();
			while(registros.next()) {
				qrySentencia =
					" SELECT NVL (MAX (ic_cuenta), 0) + 1 ic_cuenta,"   +
					"        'Dispersion::setCuentasClaveMasivabncmxt()'"   +
					"   FROM COM_CUENTAS_BXT"  ;
				datos = con.consultarDB(qrySentencia);
				if(datos.next()) {
					qrySentencia =
            " INSERT INTO COM_CUENTAS_BXT ( " +
            "   IC_CUENTA,  " +                                     //01
            "   IC_NAFIN_ELECTRONICO,  " +                          //02
            "   IC_PRODUCTO_NAFIN,  " +                             //03
            "   IC_MONEDA,  " +                                     //04
            "   IC_BANCOS_TEF,  " +                                 //05
            "   CG_CUENTA,  " +                                     //06
            "   IC_USUARIO,  " +                                    //07
            "   CG_ESTATUS,  " +                                    //08
            "   DF_ULTIMA_MOD,  " +                                 //09 sysdate
            "   DF_REGISTRO,  " +                                   //10 sysdate
            "   IC_EPO,  " +                                        //11
            "   CG_TIPO_AFILIADO)  " +                              //12
            "VALUES ( ?, ?, ?, " +
            "         ?, ?, ?, " +
            "         ?,?,SYSDATE, SYSDATE, " +
            "            ?, ? ) ";
        System.out.println("#####################setCuentasClaveMasivabncmxt 01###############################");
        System.out.println("PRIMER QUERY .. :::"+qrySentencia);
        System.out.println("variables para ejecutar el query:::");
        System.out.println("IC_CUENTA"+registros.getString("IC_CUENTA"));
        System.out.println("IC_NAFIN_ELECTRONICO"+registros.getString("IC_NAFIN_ELECTRONICO"));
        System.out.println("ic_moneda"+registros.getString("ic_moneda"));
        System.out.println("ic_bancos_tef"+registros.getString("ic_bancos_tef"));
        System.out.println("cg_cuenta"+registros.getString("cg_cuenta"));

        //System.out.println("IC_ESTATUS"+registros.getString("IC_ESTATUS"));

        //System.out.println("CG_TIPO_AFILIADO"+registros.getString("CG_TIPO_AFILIADO"));
        //System.out.println("#####################setCuentasClaveMasivabncmxt 01###############################");
        String estatus = "N";
					lVarBind = new ArrayList();
					lVarBind.add(new Long(datos.getString("ic_cuenta")));                   //1
					lVarBind.add(new Long(registros.getString("IC_NAFIN_ELECTRONICO")));    //2
					lVarBind.add(new Integer(1));                                           //3
					lVarBind.add(new Integer(registros.getString("ic_moneda")));            //4
					lVarBind.add(new Integer(registros.getString("ic_bancos_tef")));        //5
					lVarBind.add(registros.getString("cg_cuenta"));                         //6
					lVarBind.add(ic_usuario);                                               //7
          //lVarBind.add(registros.getString("cg_ESTATUS"));           //8
          lVarBind.add(estatus);           //8
					lVarBind.add(new Integer(registros.getString("ic_epo")));                                      //11
					lVarBind.add(registros.getString("CG_TIPO_AFILIADO"));                  //12
					con.ejecutaUpdateDB(qrySentencia, lVarBind);
				}
			}//while(registros.next())
		} catch(Exception e) {
			bOk = false;
			System.out.println("Dispersion::setCuentasClaveMasivabncmxt(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("Dispersion::setCuentasClaveMasivabncmxt(S)");
		}
	}//setCuentasClaveMasiva


/****************J.Angel.Haro.Juarez**********************/
  // J.Angel.Haro.Juarez 27/12/2007  Se insertan en la tabla definitiva los datos deidos del TXT despues de pasar por el proceso de validacion

  public void setCuentasVerifClaveMasivabncmxt(Registros registros, String ic_epo, String ic_usuario) throws NafinException {
		System.out.println("Dispersion::setCuentasVerifClaveMasivabncmxt(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			datos 			= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		boolean				bOk				= true;
		try {
			con.conexionDB();
			while(registros.next()) {
				qrySentencia =
					" SELECT NVL (MAX (ic_cuenta), 0) + 1 ic_cuenta,"   +
					"        'Dispersion::setCuentasVerifClaveMasivabncmxt()'"   +
					"   FROM COM_CUENTAS_BXT"  ;
				datos = con.consultarDB(qrySentencia);

				if(datos.next()) {
					qrySentencia =
            " INSERT INTO COM_CUENTAS_BXT ( " +
            "   IC_CUENTA,  " +                                     //01
            "   IC_NAFIN_ELECTRONICO,  " +                          //02
            "   IC_PRODUCTO_NAFIN,  " +                             //03
            "   IC_MONEDA,  " +                                     //04
            "   IC_BANCOS_TEF,  " +                                 //05
            "   CG_CUENTA,  " +                                     //06
            "   IC_USUARIO,  " +                                    //07
            "   CG_ESTATUS,  " +                                    //08
            "   DF_ULTIMA_MOD,  " +                                 //09 sysdate
            "   DF_REGISTRO,  " +                                   //10 sysdate
            "   IC_EPO,  " +                                        //11
            "   CG_TIPO_AFILIADO)  " +                              //12
            "VALUES ( ?, ?, ?, " +
            "         ?, ?, ?, " +
            "         ?,?,SYSDATE, SYSDATE, " +
            "            ?, ? ) ";
        System.out.println("#####################setCuentasVerifClaveMasivabncmxt 01###############################");
        System.out.println("PRIMER QUERY .. :::"+qrySentencia);
        System.out.println("variables para ejecutar el query:::");
        System.out.println("IC_CUENTA"+registros.getString("IC_CUENTA"));
        System.out.println("IC_NAFIN_ELECTRONICO"+registros.getString("IC_NAFIN_ELECTRONICO"));
        System.out.println("ic_moneda"+registros.getString("ic_moneda"));
        System.out.println("ic_bancos_tef"+registros.getString("ic_bancos_tef"));
        System.out.println("cg_cuenta"+registros.getString("cg_cuenta"));

        //System.out.println("IC_ESTATUS"+registros.getString("IC_ESTATUS"));

        System.out.println("CG_TIPO_AFILIADO"+registros.getString("CG_TIPO_AFILIADO"));
        System.out.println("#####################setCuentasVerifClaveMasivabncmxt 01###############################");
					lVarBind = new ArrayList();
					lVarBind.add(new Long(datos.getString("ic_cuenta")));                   //1
					lVarBind.add(new Long(registros.getString("IC_NAFIN_ELECTRONICO")));    //2
					lVarBind.add(new Integer(1));                                           //3
					lVarBind.add(new Integer(registros.getString("ic_moneda")));            //4
					lVarBind.add(new Integer(registros.getString("ic_bancos_tef")));        //5
					lVarBind.add(registros.getString("cg_cuenta"));                         //6
					lVarBind.add(ic_usuario);                                               //7
          lVarBind.add(registros.getString("cg_ESTATUS"));                        //8
					lVarBind.add(new Integer(ic_epo));                                      //11
					lVarBind.add(registros.getString("CG_TIPO_AFILIADO"));                  //12
					con.ejecutaUpdateDB(qrySentencia, lVarBind);
				}
			}//while(registros.next())
		} catch(Exception e) {
			bOk = false;
			System.out.println("Dispersion::setCuentasVerifClaveMasivabncmxt(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("Dispersion::setCuentasVerifClaveMasivabncmxt(S)");
		}
	}//setCuentasClaveMasiva




  /**************************************/

	public String setDatosTmpCLABE(String ic_proc_carga, List lRegistros) throws NafinException {
		System.out.println("Dispersion::setDatosTmpCLABE(E)");
		AccesoDB 			con				= new AccesoDB();
		boolean				bOk				= true;
		Registros			registros;
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();

			if("0".equals(ic_proc_carga)) {
				qrySentencia =
					" SELECT seq_comtmp_cuentas.NEXTVAL ic_proc_solic, 'Dispersion::setDatosTmpCLABE()' "   +
					"   FROM DUAL"  ;
				registros = con.consultarDB(qrySentencia);
				if(registros.next()) {
					ic_proc_carga = registros.getString("ic_proc_solic");
				}
			}

			String ic_cuenta = "1";
			qrySentencia =
				" SELECT NVL (MAX (ic_cuenta) + 1, 1) ic_cuenta, 'Dispersion::setDatosTmpCLABE()'"   +
				"   FROM comtmp_cuentas"   +
				"  WHERE ic_proc_carga = ?"  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia, lVarBind);
			if(registros.next()) {
				ic_cuenta = registros.getString("ic_cuenta");
			}

			qrySentencia =
				" INSERT INTO comtmp_cuentas"   +
				"             (ic_proc_carga, ic_cuenta, cg_razon_social, cg_rfc, cg_cuenta,"   +
				"              ic_bancos_tef, ic_moneda, cg_estatus, cg_error)"   +
				"      VALUES (?, ?, ?, ?, ?,"   +
				"              ?, ?, ?, ?)"  ;

			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			lVarBind.add(new Long(ic_cuenta));

			String 	campo;
			String 	layout[]		= {"Nombre o Raz�n Social", "RFC", "CLABE", "Banco de Servicio", "Moneda"};
			int 	longcampo[] 	= {100, 15, 18, 3, 2};
			String 	cg_estatus		= "S",
					cg_error		= "";
			if(lRegistros.size()!=layout.length) {
				cg_estatus 	= "N";
				cg_error 	= "El registro no coincide con el layout. ";
			}

			for(int i=0;i<layout.length;i++) {
				campo = (lRegistros.size()>=(i+1))?lRegistros.get(i).toString().trim():"";
				if(campo.length()>longcampo[i]) {
					cg_estatus	= "N";
					cg_error	+= "El campo "+layout[i]+" excede la longitud permitida ("+longcampo[i]+"). ";
					campo = campo.substring(0, longcampo[i]);
				}
				lVarBind.add(campo);
			}//for(int i=0;i<=layout.size();i++)

			lVarBind.add(cg_estatus);
			lVarBind.add(cg_error);
			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			System.out.println("Dispersion::setDatosTmpCLABE(Exception) "+e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::setDatosTmpCLABE(S)");
		}
		return ic_proc_carga;
	}//setDatosTmpCLABE

	public String setDatosTmpCuentas(String ic_proc_carga, List lRegistros) throws NafinException {
		log.info("Dispersion::setDatosTmpCuentas(E)");
		AccesoDB 			con				= new AccesoDB();
		boolean				bOk				= true;
		Registros			registros;
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();

			if("0".equals(ic_proc_carga)) {
				qrySentencia =
					" SELECT seq_comtmp_cuentas.NEXTVAL ic_proc_solic, 'Dispersion::setDatosTmpCuentas()' "   +
					"   FROM DUAL"  ;
				registros = con.consultarDB(qrySentencia);
				if(registros.next()) {
					ic_proc_carga = registros.getString("ic_proc_solic");
				}
			}

			String ic_cuenta = "1";
			qrySentencia =
				" SELECT NVL (MAX (ic_cuenta) + 1, 1) ic_cuenta, 'Dispersion::setDatosTmpCuentas()'"   +
				"   FROM comtmp_cuentas"   +
				"  WHERE ic_proc_carga = ?"  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia, lVarBind);
			if(registros.next()) {
				ic_cuenta = registros.getString("ic_cuenta");
			}

			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			lVarBind.add(new Long(ic_cuenta));

			String 	campo;
			String 	layout[]				= null;
			int 		longcampoCLABE[] 	= {15, 2, 2, 18, 3};
			int 		longcampoSwift[] 	= {15, 2, 2, 20, 3, 20, 20};
			String 	cg_estatus			= "S";
			String	cg_error				= "";

			// Determinar el Tipo de Cuenta
			String 	tipoCuenta 		= (lRegistros.size()>=(2+1))?lRegistros.get(2).toString().trim():"";
			boolean 	esCuentaSwift 	= (tipoCuenta != null && tipoCuenta.equals("50"))?true:false;

			if(esCuentaSwift){
				layout = new String[]{"RFC", "Moneda", "Tipo Cuenta", "Cuenta", "Banco de Servicio", "Plaza Swift", "Sucursal Swift"};
			}else{
				layout = new String[]{"RFC", "Moneda", "Tipo Cuenta", "Cuenta", "Banco de Servicio"};
			}

			if(lRegistros.size()!=layout.length) {
				cg_estatus 	= "N";
				cg_error 	= "El registro no coincide con el layout para el tipo de cuenta especificado. ";
			}

			for(int i=0;i<layout.length;i++) {
				campo = (lRegistros.size()>=(i+1))?lRegistros.get(i).toString().trim():"";

				if(esCuentaSwift){ // Validar longitud de los Campos de Una Cuenta Swift
					if(campo.length()>longcampoSwift[i]) {
						cg_estatus	= "N";
						cg_error	+= "El campo "+layout[i]+" excede la longitud permitida ("+longcampoSwift[i]+"). ";
						campo = campo.substring(0, longcampoSwift[i]);
					}
				}else{				// Validar longitud de los Campos de Una Cuenta Clabe
					if(campo.length()>longcampoCLABE[i]) {
						cg_estatus	= "N";
						cg_error	+= "El campo "+layout[i]+" excede la longitud permitida ("+longcampoCLABE[i]+"). ";
						campo = campo.substring(0, longcampoCLABE[i]);
					}
				}
				lVarBind.add(campo);
			}//for(int i=0;i<=layout.size();i++)

			lVarBind.add(cg_estatus);
			lVarBind.add(cg_error);

			qrySentencia =
				" INSERT INTO comtmp_cuentas "   +
				"             (ic_proc_carga, ic_cuenta, cg_rfc, ic_moneda, ic_tipo_cuenta, "  +
				"              cg_cuenta, ic_bancos_tef,"+(esCuentaSwift?" cg_plaza_swift, cg_sucursal_swift,":"")+" cg_estatus, cg_error )"   +
				"      VALUES (?, ?, ?, ?, ?,"   +
				"              ?, ?,"+(esCuentaSwift?" ?, ?,":"")+" ?, ?)"  ;

			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			log.error("Dispersion::setDatosTmpCuentas(Exception) "+e);
			bOk = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("Dispersion::setDatosTmpCuentas(S)");
		}
		return ic_proc_carga;
	}

	public Registros getEstatusCargaCLABE(String ic_proc_carga) throws NafinException {
		System.out.println("Dispersion::getEstatusCargaCLABE(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT cg_estatus_proc, NVL (ig_num_procesados, 0) ig_num_procesados, 'Dispersion::getEstatusCargaCLABE()'"   +
				"   FROM bit_tmp_cuentas"   +
				"  WHERE ic_proc_carga = ?"  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("Dispersion::getEstatusCargaCLABE(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getEstatusCargaCLABE(S)");
		}
		return registros;
	}//getEstatusCargaCLABE

	/**
	 *  Devuelve el numero de proceso ademas del numero de registros procesados
	 *  por procedimiento de carga masiva de cuentas: SP_CARGA_MASIVA_CUENTAS.
	 *
	 *  @PARAM ic_proc_carga ID del Proceso de Carga.
	 *
	 *  @return objeto de tipo <tt>Registros</tt> con el estatus del avance del
	 *          proceso.
	 */
	public Registros getEstatusCargaCuentas(String ic_proc_carga) throws NafinException {
		log.info("Dispersion::getEstatusCargaCuentas(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		StringBuffer		qrySentencia	= new StringBuffer();
		List 					lVarBind;
		try {
			con.conexionDB();
			qrySentencia.append(
				" SELECT cg_estatus_proc, NVL (ig_num_procesados, 0) ig_num_procesados, 'Dispersion::getEstatusCargaCuentas()' "  +
				"   FROM bit_tmp_cuentas "  +
				"  WHERE ic_proc_carga = ?" );
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			registros = con.consultarDB(qrySentencia.toString(), lVarBind);
		} catch(Exception e) {
			log.error("Dispersion::getEstatusCargaCuentas(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("Dispersion::getEstatusCargaCuentas(S)");
		}
		return registros;
	}//getEstatusCargaCuentas

	public Registros getRegistrosValidados(String ic_proc_carga, String cg_estatus) throws NafinException {
		System.out.println("Dispersion::getRegistrosValidados(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT cta.ic_cuenta, cta.cg_razon_social, cta.cg_rfc, cta.cg_cuenta,"   +
				"        cta.ic_bancos_tef, cta.ic_moneda, cta.ic_estatus_cecoban,"   +
				"        mon.cd_nombre nombremoneda, cta.ic_nafin_electronico, cta.cg_error, 'Dispersion::getRegistrosValidados()'"   +
				"   FROM comtmp_cuentas cta, comcat_moneda mon"   +
				"  WHERE cta.ic_moneda = mon.ic_moneda (+)"   +
				"    AND cta.ic_proc_carga = ?"   +
				"    AND cta.cg_estatus = ?"   +
				"  ORDER BY cta.ic_cuenta"  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			lVarBind.add(cg_estatus);
			registros = con.consultarDB(qrySentencia, lVarBind);
		} catch(Exception e) {
			System.out.println("Dispersion::getRegistrosValidados(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("Dispersion::getRegistrosValidados(S)");
		}
		return registros;
	}//getRegistrosValidados

	/**
	 *	Devuelve los Datos de los Registros que fueron validados por
	 * el procedimiento de Carga Masiva de Cuentas.
	 *
	 * @param ic_prog_carga ID del Proceso de Carga de las Cuentas
	 * @param cg_estatus Estatus de los Registros que traera la consulta.
	 *                   Si el estatus es "S", traera los datos de los
	 *                   registros que pasaron la validacion; si el estatus es
	 *                   "N" traera los registros con errores.
	 *
	 * @return Objeto de tipo <tt>Registros</tt> con la informacion solicitada.
	 *
	 */
	public Registros getRegistrosConCuentasValidadas(String ic_proc_carga, String cg_estatus) throws NafinException {
		log.info("Dispersion::getRegistrosConCuentasValidadas(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			registros 		= new Registros();
		StringBuffer		qrySentencia	= new StringBuffer();
		List 					lVarBind;
		try {
			con.conexionDB();
			/*
			" SELECT "  +
				"        cta.ic_cuenta, "  +
				"        cta.cg_rfc, "  +
				"        cta.ic_moneda, "  +
				"        cta.ic_tipo_cuenta, "  +
				"        cta.cg_cuenta, "   +
				"        cta.ic_bancos_tef, "  +
				"        cta.ic_estatus_cecoban, "   + // Pendiente Revisar para que se ocupa
				"        mon.cd_nombre nombremoneda, "  +
				"			cta.ic_nafin_electronico, "  +
				"			cta.cg_error, "  +
				"       'Dispersion::getRegistrosConCuentasValidadas()' "  +
				"   FROM comtmp_cuentas cta, comcat_moneda mon"   +
				"  WHERE cta.ic_moneda = mon.ic_moneda (+)"   +
				"    AND cta.ic_proc_carga = ?"   +
				"    AND cta.cg_estatus = ?"   +
				"  ORDER BY cta.ic_cuenta"
			*/
			qrySentencia.append(
				" SELECT "  +
				"        cta.ic_cuenta,                "  +
				"        cta.cg_rfc,                   "  +
				"        cta.ic_moneda,                "  +
				"        cta.ic_tipo_cuenta,           "  +
				"        cta.cg_cuenta,                "  +
				"        cta.ic_bancos_tef,            "  +
				"        cta.ic_estatus_cecoban,       "  + // Pendiente Revisar para que se ocupa
				"        '' as nombremoneda,           "  +
				"			cta.ic_nafin_electronico,        "  +
				"			cta.cg_error,                    "  +
				"			cta.cg_plaza_swift,           "  +
				"			cta.cg_sucursal_swift,        "  +
				"       'Dispersion::getRegistrosConCuentasValidadas()' "  +
				"   FROM comtmp_cuentas cta            "   +
				"  WHERE                               "   +
				"        cta.ic_proc_carga = ? AND     "   +
				"        cta.cg_estatus    = ?         "   +
				"  ORDER BY cta.ic_cuenta              "  );
			lVarBind = new ArrayList();
			lVarBind.add(new Long(ic_proc_carga));
			lVarBind.add(cg_estatus);
			registros = con.consultarDB(qrySentencia.toString(), lVarBind);
		} catch(Exception e) {
			log.error("Dispersion::getRegistrosConCuentasValidadas(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("Dispersion::getRegistrosConCuentasValidadas(S)");
		}
		return registros;
	}

	public void setCuentasClaveMasiva(Registros registros, String ic_epo, String ic_usuario) throws NafinException {
		System.out.println("Dispersion::setCuentasClaveMasiva(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			datos 			= new Registros();
		String				qrySentencia;
		List 				lVarBind;
		boolean				bOk				= true;
		try {
			con.conexionDB();
			while(registros.next()) {
				qrySentencia =
					" SELECT NVL (MAX (ic_cuenta), 0) + 1 ic_cuenta,"   +
					"        TO_CHAR (sigfechahabil (SYSDATE, 1, '+'), 'dd/mm/yyyy') fechasighabil, 'Dispersion::setCuentasClaveMasiva()'"   +
					"   FROM com_cuentas"  ;
				datos = con.consultarDB(qrySentencia);
				if(datos.next()) {
					qrySentencia =
						" INSERT INTO com_cuentas"   +
						"             (ic_cuenta, ic_nafin_electronico, ic_producto_nafin, ic_moneda,"   +
						"              ic_bancos_tef, cg_cuenta, ic_usuario, df_ultima_mod, df_registro,"   +
						"              df_transferencia, df_aplicacion, ic_tipo_cuenta, cg_tipo_afiliado,"   +
						"              ic_estatus_cecoban, ic_epo)"   +
						"      VALUES (?, ?, ?, ?,"   +
						"              ?, ?, ?, SYSDATE, SYSDATE,"   +
						"              TO_DATE(?, 'dd/mm/yyyy'), TO_DATE(?, 'dd/mm/yyyy'), ?, ?,"   +
						"              ?, ?)"  ;
					lVarBind = new ArrayList();
					lVarBind.add(new Long(datos.getString("ic_cuenta")));
					lVarBind.add(new Long(registros.getString("ic_nafin_electronico")));
					lVarBind.add(new Integer(1));
					lVarBind.add(new Integer(registros.getString("ic_moneda")));
					lVarBind.add(new Integer(registros.getString("ic_bancos_tef")));
					lVarBind.add(registros.getString("cg_cuenta"));
					lVarBind.add(ic_usuario);
					lVarBind.add(datos.getString("fechasighabil"));
					lVarBind.add(datos.getString("fechasighabil"));
					lVarBind.add(new Integer(40));
					lVarBind.add("P");
					if(!"".equals(registros.getString("ic_estatus_cecoban")))
						lVarBind.add(new Integer(registros.getString("ic_estatus_cecoban")));
					else
						lVarBind.add(Integer.TYPE);
					lVarBind.add(new Integer(ic_epo));
					con.ejecutaUpdateDB(qrySentencia, lVarBind);
				}
			}//while(registros.next())
		} catch(Exception e) {
			bOk = false;
			System.out.println("Dispersion::setCuentasClaveMasiva(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			System.out.println("Dispersion::setCuentasClaveMasiva(S)");
		}
	}//setCuentasClaveMasiva

	public void agregaCuentas(Registros registros, String ic_epo, String ic_usuario, HashMap parametrosAdicionales ) throws NafinException {
		log.info("Dispersion::agregaCuentas(E)");
		AccesoDB 			con				= new AccesoDB();
		Registros 			datos 			= new Registros();
		StringBuffer		qrySentencia	= new StringBuffer();
		List 				lVarBind;
		boolean				bOk				= true;

		parametrosAdicionales 	= parametrosAdicionales == null?new HashMap():parametrosAdicionales;
		boolean esCuentaCLABE 	= false;
		boolean esCuentaSWIFT 	= false;

		try {
			con.conexionDB();
                        int registrosInsertados = 0;
			while(registros.next()) {
				qrySentencia = new StringBuffer();
				qrySentencia.append(
					" SELECT NVL (MAX (ic_cuenta), 0) + 1 ic_cuenta,"   +
					"        TO_CHAR (sigfechahabil (SYSDATE, 1, '+'), 'dd/mm/yyyy') fechasighabil, 'Dispersion::setCuentasClaveMasiva()'"   +
					"   FROM com_cuentas");
				datos = con.consultarDB(qrySentencia.toString());
				if(datos.next()) {
					qrySentencia = new StringBuffer();
                                        qrySentencia.append("INSERT INTO com_cuentas   \n" + 
                                            "	 (ic_cuenta, ic_nafin_electronico, ic_producto_nafin, ic_moneda, ic_bancos_tef, cg_cuenta, \n" + 
                                            "	 ic_usuario, df_ultima_mod, df_registro, df_transferencia, df_aplicacion, ic_tipo_cuenta, \n" + 
                                            "	 cg_tipo_afiliado, ic_estatus_cecoban, ic_epo, cg_plaza_swift, cg_sucursal_swift)    \n" + 
                                            "SELECT ?, ?, ?, ?, ?, ?, ?, SYSDATE, SYSDATE,   \n" + 
                                            "	  TO_DATE(?, 'dd/mm/yyyy'), TO_DATE(?, 'dd/mm/yyyy'), ?, ?, ?, ?, ?, ? FROM DUAL\n" + 
                                            "WHERE NOT EXISTS (SELECT * FROM com_cuentas WHERE cg_cuenta = ? AND ic_nafin_electronico = ? AND ic_epo = ?) ");
                                        lVarBind = new ArrayList();
					lVarBind.add(new Long(datos.getString("ic_cuenta")));
                                        Long numeroNE = new Long(registros.getString("ic_nafin_electronico"));
					lVarBind.add(numeroNE);
					lVarBind.add(new Integer(1));
					lVarBind.add(new Integer(registros.getString("ic_moneda")));
					lVarBind.add(new Integer(registros.getString("ic_bancos_tef")));
					lVarBind.add(registros.getString("cg_cuenta"));
					lVarBind.add(ic_usuario);
					lVarBind.add(datos.getString("fechasighabil"));
					lVarBind.add(datos.getString("fechasighabil"));
					lVarBind.add(registros.getString("ic_tipo_cuenta"));
					lVarBind.add("P");
					if(!"".equals(registros.getString("ic_estatus_cecoban")))
						lVarBind.add(new Integer(registros.getString("ic_estatus_cecoban")));
					else
						lVarBind.add(Integer.TYPE);
                                        Integer icEPO = new Integer(ic_epo);
                                        lVarBind.add(icEPO);
					lVarBind.add(registros.getString("cg_plaza_swift"));
					lVarBind.add(registros.getString("cg_sucursal_swift"));
                                        
                                        lVarBind.add(registros.getString("cg_cuenta"));
                                        lVarBind.add(numeroNE);
                                        lVarBind.add(icEPO);
                                        
					registrosInsertados = con.ejecutaUpdateDB(qrySentencia.toString(), lVarBind);
                                        
                                        if (registrosInsertados > 0){
                                            // Registrar en BITACORA el ALTA DE LA CUENTA
                                            String decripcionAlta = (String) parametrosAdicionales.get("DESCRIPCION_ALTA");
                                            decripcionAlta = decripcionAlta == null?"ALTA\nNO DISPONIBLE":decripcionAlta;
                                            String clavePantalla  = (String) parametrosAdicionales.get("PANTALLA_ORIGEN");
                                            clavePantalla   = clavePantalla  == null?"REGMASCTAS"    :clavePantalla;

                                            esCuentaCLABE = "40".equals(registros.getString("ic_tipo_cuenta"))?true:false;
                                            esCuentaSWIFT = "50".equals(registros.getString("ic_tipo_cuenta"))?true:false;

                                            String valoresActuales =
                                                    decripcionAlta + "\n" +
                                                    "cg_cuenta="+registros.getString("cg_cuenta")+"\n"+
                                                    "ic_producto_nafin=1\n"+
                                                    "ic_moneda="+registros.getString("ic_moneda")+"\n"+
                                                    "ic_bancos_tef="+registros.getString("ic_bancos_tef")+"\n"+
                                                    "ic_tipo_cuenta="+registros.getString("ic_tipo_cuenta")+"\n"+
                                                    "cg_plaza_swift="+   (esCuentaSWIFT? registros.getString("cg_plaza_swift")   :"N/A")+"\n"+
                                                    "cg_sucursal_swift="+(esCuentaSWIFT? registros.getString("cg_sucursal_swift"):"N/A")+"\n"+
                                                    "ic_epo="+ic_epo;

                                            Bitacora.grabarEnBitacora(
                                                    con, clavePantalla,                         //clavePantalla,
                                                    "P",                                        //tipoAfiliado = P
                                                    registros.getString("ic_nafin_electronico"),//claveNafinElectronico,
                                                    ic_usuario,                                 //claveUsuario,
                                                    "",                                         //datoAnterior,
                                                    valoresActuales                             //datoActual
                                            );                                            
                                        }
                                        else{
                                            log.info("La cuenta "+ registros.getString("cg_cuenta")+" ya existe para la EPO["+ icEPO+"] y el NE["+numeroNE+"]");
                                        }
                                        registrosInsertados = 0;
                                        con.getConnection().commit();
				}
			} //while(registros.next())
		} catch(Exception e) {
			log.error("Dispersion::agregaCuentas(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
                            con.cierraConexionDB();
			}
			log.info("Dispersion::agregaCuentas(S)");
		}
	}//setCuentasClaveMasiva

	/*******************IHJ***************/



/**
	 * @param sIdEpo contiene el ID de la Epo, se usa para realizar la consulta
	 * si viene vacia entonces se realiza la consulta
	 * @return HashMap de EPOS con dispersion
	 * @exception NafinException  excepcion para mostrar msg de error
	 * @author Erik Barba Alatriste
	 */
	 public HashMap DispConsBloqServs(String sIdEpo) throws NafinException{

		AccesoDB 	con 						= new AccesoDB();
		String 		qrySentencia 		= "";
		ResultSet rs 							= null;
		HashMap   map 						= new HashMap();

		System.out.println("DispersionBean::DispConsBloqServs (E)");

		try{
			con.conexionDB();

			qrySentencia 	 = "  SELECT /*use_nl(cpe,ce,cn,xxx)*/ CPE.IC_PRODUCTO_NAFIN, CPE.IC_EPO, CN.IC_NAFIN_ELECTRONICO,   ";
			qrySentencia 	+= "          CE.CG_RAZON_SOCIAL, CE.CG_RFC, CPE.CS_BLOQUEADO  ";
			qrySentencia 	+= "          , bb3.df_fecha, bb3.ic_usuario, bb3.cg_nombre_usuario, bb3.cg_causa  ";
			qrySentencia 	+= "    FROM COMREL_PRODUCTO_EPO CPE, COMCAT_EPO CE, COMREL_NAFIN CN,  ";
			qrySentencia 	+= "    (SELECT bbd.ic_epo, TO_CHAR(BBD.DF_FECHA,'DD/MM/YYYY HH24:MM:SS') DF_FECHA, bbd.ic_usuario, bbd.cg_nombre_usuario, bbd.cg_causa  ";
			qrySentencia 	+= "            FROM bit_bloqueo_disp bbd,   ";
			qrySentencia 	+= "            (select max(ic_bloqueo_disp) ic_bloqueo_disp, ic_epo  ";
			qrySentencia 	+= "              FROM bit_bloqueo_disp bbd  ";
			qrySentencia 	+= "              where ic_producto_nafin = 1  ";
			qrySentencia 	+= "             group by ic_epo) bd2  ";
			qrySentencia 	+= "           WHERE bbd.ic_bloqueo_disp = bd2.ic_bloqueo_disp   ";
			qrySentencia 	+= "             AND bbd.ic_epo =  bd2.ic_epo  ";
			qrySentencia 	+= "             AND bbd.ic_producto_nafin = 1) bb3  ";
			qrySentencia 	+= "   WHERE     CPE.IC_EPO = CE.IC_EPO          ";
			qrySentencia 	+= "         AND CPE.IC_EPO = CN.IC_EPO_PYME_IF  ";
			qrySentencia 	+= "         and cpe.IC_EPO = bb3.ic_epo(+)                ";
			if(!sIdEpo.equals("") && sIdEpo!=null )
				qrySentencia 	+= "         AND CPE.IC_EPO = ?  ";
			qrySentencia 	+= "         AND CPE.IC_PRODUCTO_NAFIN = 1  ";
			qrySentencia 	+= "         AND CPE.CG_DISPERSION = 'S'          ";
			qrySentencia 	+= "         AND CN.CG_TIPO = 'E'  ";

			qrySentencia 	+= "         ORDER BY CE.CG_RAZON_SOCIAL  ";

			System.out.println("qrySentencia::"+qrySentencia.toString());
			System.out.println("sIdEpo::"+sIdEpo);

			PreparedStatement ps = con.queryPrecompilado(qrySentencia);
			if(!sIdEpo.equals("") && sIdEpo!=null )
				ps.setString(1, sIdEpo);
			rs = ps.executeQuery();

			int i = 0;
			while(rs.next()) {
				map.put("ic_prod_nafin"+i,rs.getString(1));
				map.put("ic_epo"+i,rs.getString(2));
				map.put("ic_nafin_electronico"+i,rs.getString(3));
				map.put("razon_social"+i,rs.getString(4));
				map.put("rfc"+i,rs.getString(5));
				map.put("bloqueado"+i,rs.getString(6));
				map.put("fecha"+i,rs.getString(7));
				map.put("usuario"+i,rs.getString(8));
				map.put("nombre_usuario"+i,rs.getString(9));
				map.put("causa"+i,rs.getString(10));

				i++;
			}

			map.put("registros", Integer.toString(i));

			if(ps!=null)
				ps.close();

			rs.close();

		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		System.out.println("DispersionBean::DispConsBloqServs (S)");
		return map;
	}


	public HashMap autenticaFirmaDigital(String pkcs7, String textoFirmado, String serial, String numeroCliente)
		throws NafinException{

		System.out.println("DispersionBean::autenticaFirmaDigital(E)");

		pkcs7						= (pkcs7 					== null?"":pkcs7);
		textoFirmado		= (textoFirmado 	== null?"":textoFirmado);
		numeroCliente		= (numeroCliente 	== null?"":numeroCliente);
		serial					= (serial 				== null?"":serial);

		String 		  folioCertificado 	= null;
		netropology.utilerias.Seguridad	seguridad			= null;
		char 			  getReceipt 			  = 'Y';
		HashMap  	  resultado			    = new HashMap();

		if ( !serial.equals("") && !textoFirmado.equals("") && !pkcs7.equals("") && !numeroCliente.equals("") ) {
			folioCertificado 	= "01CC"+numeroCliente+new SimpleDateFormat("ddMMyyHHmm").format(new java.util.Date());
			seguridad 			= new Seguridad();

			if (!seguridad.autenticar(folioCertificado, serial, pkcs7, textoFirmado, getReceipt)) {
				resultado.put("VERIFICACION_EXITOSA","false");
				resultado.put("MENSAJE_ERROR","La autentificacion no se llevo a cabo: \n"+seguridad.mostrarError());
			}else{
				resultado.put("VERIFICACION_EXITOSA","true");
				resultado.put("RECIBO",seguridad.getAcuse());
				resultado.put("FOLIO",folioCertificado);
			}

		}else{
			System.out.println("ParametrosDescuentoBean::autenticaFirmaDigital(Exception)");
			System.out.println("Uno o mas parametros vienen vacios: ");
			System.out.println("      textoFirmado   = " + textoFirmado);
			System.out.println("      serial         = " + serial);
			System.out.println("      numeroCliente  = " + numeroCliente);
			System.out.println("      pkcs7          = " + pkcs7);
			throw new NafinException("GRAL0021");
		}

		System.out.println("DispersionBean::autenticaFirmaDigital(S)");
		return resultado;
	}

	public String DispActualizacionBloqueos(List causa, List checkbloq, List Epo, List ProdNafin,
																					String NombreUsuario, String ClaveUsuario, String Folio, String Recibo)
		throws NafinException{
			System.out.println("DispersionBean::DispActualizacionBloqueos(E)");

			String 				nuevoStatusBloqueo 			= "";
			String 				strSQL             			= "";
			AccesoDB con 							= new AccesoDB();
			PreparedStatement ps 			= null;
			PreparedStatement ps2 		= null;
			try{

			con.conexionDB();

			int i = 0;
			while(i < causa.size()){

				if( causa.get(i)!=null && !((String)causa.get(i)).equals("") ) {

					if(checkbloq!=null){
						if( i < checkbloq.size() ){
							if(checkbloq.get(i)==null){
								nuevoStatusBloqueo = "N";
								//System.out.println(" ---- checkbloq.get("+i+")	::N:: N "  );
							}
							else{
								nuevoStatusBloqueo = "S";
								//System.out.println(" ---- checkbloq.get("+i+")	::S:: " + checkbloq.get(i) );
							}
						} else{
							nuevoStatusBloqueo = "N";
							//System.out.println(" ---- checkbloq.get("+i+")	::N:: N "  );
						}
					}	else{
						nuevoStatusBloqueo = "N";
						//System.out.println(" ---- checkbloq.get("+i+")	::N:: N "  );
					}

					strSQL = 	" INSERT INTO BIT_BLOQUEO_DISP " +
										" (IC_BLOQUEO_DISP, IC_PRODUCTO_NAFIN, IC_EPO, CS_BLOQUEADO, DF_FECHA, " +
										" IC_USUARIO, CG_NOMBRE_USUARIO, CG_CAUSA, CG_FOLIO, CG_RECIBO) " +
										"	VALUES (SEQ_BIT_BLOQUEO_DISP.NEXTVAL, ?, ?, ?, SYSDATE, "+
										" ?, ?, ?, ?, ?)";
					System.out.println("..:: strSQL : "+strSQL);
					ps = con.queryPrecompilado(strSQL);
					ps.setInt(1, Integer.parseInt((String)ProdNafin.get(i)));
					ps.setInt(2, Integer.parseInt((String)Epo.get(i)));
					ps.setString(3, nuevoStatusBloqueo);
					ps.setString(4, ClaveUsuario);
					ps.setString(5, NombreUsuario);
					ps.setString(6, (String)causa.get(i));
					ps.setString(7, Folio);
					ps.setString(8, Recibo);
					ps.executeUpdate();
					ps.close();

					strSQL = " UPDATE COMREL_PRODUCTO_EPO SET CS_BLOQUEADO = ? WHERE  IC_EPO = ? AND IC_PRODUCTO_NAFIN  = ? ";
					System.out.println("..:: strSQL : "+strSQL);
					ps2 = con.queryPrecompilado(strSQL);
					ps2.setString(1, nuevoStatusBloqueo);
					ps2.setInt(2, Integer.parseInt((String)Epo.get(i)));
					ps2.setInt(3, Integer.parseInt((String)ProdNafin.get(i)));
					ps2.executeUpdate();

					ps2.close();

				} //fin de if
				i++;
			} // fin de while

			con.terminaTransaccion(true);
			nuevoStatusBloqueo = "TransaccionAplicada";

		} catch(Exception e){
			con.terminaTransaccion(false);
			nuevoStatusBloqueo = "Error";
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			if(con.hayConexionAbierta()){ con.cierraConexionDB();	}
		}

		System.out.println("DispersionBean::DispActualizacionBloqueos(S)");
		return nuevoStatusBloqueo;
	}


	 public HashMap DispCaptBloqServsFin(List causa, List Epo) throws NafinException{

		AccesoDB 	con 						= new AccesoDB();
		String 		qrySentencia 		= "";
		ResultSet rs 							= null;
		HashMap   map 						= new HashMap();
		String    sEpos						= "";

		System.out.println("DispersionBean::DispConsBloqServs (E)");

		try{
			con.conexionDB();

//AKI ME QUEDE, estoy sacando las epos y las voy a concatenar con comas para hacer select con un in

			qrySentencia 	 = "  SELECT /*use_nl(cpe,ce,cn,xxx)*/ CN.IC_NAFIN_ELECTRONICO,   ";
			qrySentencia 	+= "          CE.CG_RAZON_SOCIAL, CE.CG_RFC, DECODE(CPE.CS_BLOQUEADO,'N','No','Si') CS_BLOQUEADO  ";
			qrySentencia 	+= "          , bb3.df_fecha, bb3.ic_usuario, bb3.cg_nombre_usuario, bb3.cg_causa  ";
			qrySentencia 	+= "    FROM COMREL_PRODUCTO_EPO CPE, COMCAT_EPO CE, COMREL_NAFIN CN,  ";
			qrySentencia 	+= "    (SELECT bbd.ic_epo, TO_CHAR(BBD.DF_FECHA,'DD/MM/YYYY HH24:MM:SS') DF_FECHA, bbd.ic_usuario, bbd.cg_nombre_usuario, bbd.cg_causa  ";
			qrySentencia 	+= "            FROM bit_bloqueo_disp bbd,   ";
			qrySentencia 	+= "            (select max(ic_bloqueo_disp) ic_bloqueo_disp, ic_epo  ";
			qrySentencia 	+= "              FROM bit_bloqueo_disp bbd  ";
			qrySentencia 	+= "              where ic_producto_nafin = 1  ";
			qrySentencia 	+= "             group by ic_epo) bd2  ";
			qrySentencia 	+= "           WHERE bbd.ic_bloqueo_disp = bd2.ic_bloqueo_disp   ";
			qrySentencia 	+= "             AND bbd.ic_epo =  bd2.ic_epo  ";
			qrySentencia 	+= "             AND bbd.ic_producto_nafin = 1) bb3  ";
			qrySentencia 	+= "   WHERE     CPE.IC_EPO = CE.IC_EPO          ";
			qrySentencia 	+= "         AND CPE.IC_EPO = CN.IC_EPO_PYME_IF  ";
			qrySentencia 	+= "         and cpe.IC_EPO = bb3.ic_epo(+)                ";

			//concatenando ids de epos
			int i = 0;
			while(i < causa.size()){
				if( causa.get(i)!=null && !((String)causa.get(i)).equals("") ) {
					sEpos += (String)Epo.get(i) + ",";
				} //fin de if
				i++;
			} //fin de while

			sEpos = sEpos.substring(0,sEpos.length()-1);
			sEpos = "(" + sEpos + ")";
			qrySentencia 	+= "         AND CPE.IC_EPO IN " + sEpos;

			qrySentencia 	+= "         AND CPE.IC_PRODUCTO_NAFIN = 1  ";
			qrySentencia 	+= "         AND CPE.CG_DISPERSION = 'S'          ";
			qrySentencia 	+= "         AND CN.CG_TIPO = 'E'  ";

			qrySentencia 	+= "         ORDER BY CE.CG_RAZON_SOCIAL  ";

			System.out.println("qrySentencia::"+qrySentencia.toString());

			PreparedStatement ps = con.queryPrecompilado(qrySentencia);

			rs = ps.executeQuery();

			i = 0;
			while(rs.next()) {
				map.put("ic_nafin_electronico"+i,rs.getString(1));
				map.put("razon_social"+i,rs.getString(2));
				map.put("rfc"+i,rs.getString(3));
				map.put("bloqueado"+i,rs.getString(4));
				map.put("fecha"+i,rs.getString(5));
				map.put("usuario"+i,rs.getString(6));
				map.put("nombre_usuario"+i,rs.getString(7));
				map.put("causa"+i,rs.getString(8));

				i++;
			}

			map.put("registros", Integer.toString(i));

			if(ps!=null)
				ps.close();

			rs.close();

		}catch (Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		System.out.println("DispersionBean::DispCaptBloqServsFin (S)");
		return map;
	}

	/**
	 * Este metodo se encarga de revisar si ya se habian parametrizado datos
	 * dispersion para la epo correspondiente.
	 */
	public boolean existeRegistroDeDispersion(String claveEPO)
		throws NafinException {

		System.out.println("DispersionBean::existeRegistroDeDispersion(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		boolean			existeRegistro	= false;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT                                                           " +
					"		DECODE(COUNT(1),0,'false','true') AS EXISTE_PARAMETRIZACION " +
					"FROM                                                             " +
					"	COM_PARAMETROS_DISPERSION                                      " +
					"WHERE                                                            " +
					"	IC_EPO 	= ?                                                   ";

				lVarBind.add(claveEPO);

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					existeRegistro = registros.getString("EXISTE_PARAMETRIZACION").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("DispersionBean::existeRegistroDeDispersion(Exception)");
			System.out.println("claveEPO  = " + claveEPO);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::existeRegistroDeDispersion(S)");
		}

		return existeRegistro;
	}

	/**
	 * Este metodo inserta en la tabla COM_PARAMETRIZACION_DISPERSION las cuentas de correo NAFIN y EPO asi como
	 * de la PYME para el servicio de Dispersion
	 */
	public void setParametrosDeDispersion(String claveEPO,String clavePYME,String cuentasDeCorreoNAFIN, String cuentasDeCorreoEPO)
		throws NafinException {

		System.out.println("DispersionBean::setParametrosDeDispersion(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		boolean			lbOK				= true;

		try {

				con.conexionDB();

				String cuentasEPO		= (cuentasDeCorreoEPO	== null)?"":cuentasDeCorreoEPO;
				String cuentasNAFIN	= (cuentasDeCorreoNAFIN	== null)?"":cuentasDeCorreoNAFIN;

				if(!existeRegistroDeDispersion(claveEPO)){

					qrySentencia =

						"INSERT INTO " +
						"	COM_PARAMETROS_DISPERSION " +
						"		( " +
						"			IC_EPO, " +
						"			IC_PYME, " +
						"			CG_MAIL_CUENTAS_NAFIN, " +
						"			CG_MAIL_CUENTAS_EPO " +
						"		) " +
						"VALUES " +
						"		( " +
						"			?, " +
						"			?, " +
						"			?, " +
						"			?  " +
						"		) ";

					lVarBind.add(new Integer(claveEPO));
					lVarBind.add(new Integer(clavePYME));
					lVarBind.add(cuentasNAFIN);
					lVarBind.add(cuentasEPO);

				} else {

					qrySentencia =

						"UPDATE 									"  +
						"	COM_PARAMETROS_DISPERSION 		"  +
						"SET 										"  +
						"	IC_PYME 						= ?, 	"  +
						"	CG_MAIL_CUENTAS_NAFIN 	= ?, 	"  +
						"	CG_MAIL_CUENTAS_EPO 		= ? 	"  +
						"WHERE 									"  +
						"	IC_EPO    = ? 				      ";

					lVarBind.add(new Integer(clavePYME));
					lVarBind.add(cuentasNAFIN);
					lVarBind.add(cuentasEPO);
					lVarBind.add(new Integer(claveEPO));
				}

				con.ejecutaUpdateDB(qrySentencia, lVarBind);

			} catch(Exception e) {
				System.out.println("DispersionBean::setParametrosDeDispersion(Exception)");
				System.out.println("claveEPO  = " + claveEPO);
				System.out.println("clavePYME = " + clavePYME);
				System.out.println("cuentasDeCorreoNAFIN = " + cuentasDeCorreoNAFIN );
				System.out.println("cuentasDeCorreoEPO   = " + cuentasDeCorreoEPO  );
				e.printStackTrace();
				lbOK = false;
				throw new NafinException("SIST0001");
			} finally {
				con.terminaTransaccion(lbOK);
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
				System.out.println("DispersionBean::setParametrosDeDispersion(S)");
			}
	}

	/**
	 * Este metodo devuelve los parametros de Dispersion: Clave de la PYME, Cuentas de Correo NAFIN y
	 * Cuentas para una EPO en particular.
	 */
	public HashMap getParametrosDeDispersion(String claveEPO)
		throws NafinException {

		System.out.println("DispersionBean::getParametrosDeDispersion(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		HashMap 			resultado					= new HashMap();
		String			cuentas						= null;
		String			clavePYME					= "";
		String		   []cuentasDeCorreoNAFIN	= null;
		String			[]cuentasDeCorreoEPO		= null;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT                                   	" +
					"	IC_PYME 						AS CLAVE_PYME,		" +
					"	CG_MAIL_CUENTAS_NAFIN 	AS CUENTAS_NAFIN,	" +
					"	CG_MAIL_CUENTAS_EPO		AS CUENTAS_EPO		" +
					"FROM                                        " +
					"	COM_PARAMETROS_DISPERSION                 " +
					"WHERE                                       " +
					"	IC_EPO 	= ? 										";
				lVarBind.add(claveEPO);
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next() ){

					clavePYME 	=  registros.getString("CLAVE_PYME");

					cuentas 					= registros.getString("CUENTAS_NAFIN");
					cuentasDeCorreoNAFIN = cuentas.equals("")?null:cuentas.split(",");

					cuentas					= registros.getString("CUENTAS_EPO");
					cuentasDeCorreoEPO   = cuentas.equals("")?null:cuentas.split(",");
				}

				resultado.put("CLAVE_PYME",		clavePYME            );
				resultado.put("CUENTAS_NAFIN",	cuentasDeCorreoNAFIN );
				resultado.put("CUENTAS_EPO",		cuentasDeCorreoEPO   );

		} catch(Exception e) {
			System.out.println("DispersionBean::getParametrosDeDispersion(Exception)");
			System.out.println("claveEPO  = " + claveEPO);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::getParametrosDeDispersion(S)");
		}

		return resultado;
	}

	/**
	 * Este metodo devuelve una cadena de texto con el contenido del mensaje que
	 * se enviara a los clientes del servicio de Dispersion de Cadenas
	 * para el cobro de este servicio.
	 */
	public String getMensajeDedispersion()
		throws NafinException{

		System.out.println("DispersionBean::getMensajeDedispersion(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String 			mensaje			= "";
		try {
				con.conexionDB();
				qrySentencia =
					"SELECT " +
					"	CG_MENSAJE_DISPERSION AS MENSAJE " +
					"FROM   " +
					"	COM_PARAM_GRAL " +
					"WHERE  " +
					"	IC_PARAM_GRAL = ? ";

				lVarBind.add(new Integer(1));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next() ){
					mensaje = registros.getString("MENSAJE");
				}

		} catch(Exception e) {
			System.out.println("DispersionBean::getMensajeDedispersion(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::getMensajeDedispersion(S)");
		}

		return mensaje;
	}

	/**
	 * Este metodo guarda el contenido del mensaje que se enviara a los clientes
	 * para el cobro por el servicio de Dispersion.
	 */
	public void setMensajeDedispersion(String mensaje)
		throws NafinException{

		System.out.println("DispersionBean::setMensajeDedispersion(E)");

		if(mensaje != null && mensaje.length() > 3072){
			mensaje = mensaje.substring(0,3072);
		}

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		boolean			lbOK				= true;

		try {
				con.conexionDB();
				qrySentencia =
						"UPDATE 									" +
						"	COM_PARAM_GRAL 					" +
						"SET 										" +
						"	CG_MENSAJE_DISPERSION  = ? 	" +
						"WHERE  									" +
						"	IC_PARAM_GRAL = ? 				";

				lVarBind.add(mensaje);
				lVarBind.add(new Integer(1));

				con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e) {
			System.out.println("DispersionBean::setMensajeDedispersion(Exception)");
			System.out.println(" mensaje = " + mensaje);
			e.printStackTrace();
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(lbOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::setMensajeDedispersion(S)");
		}
	}

	public boolean	estaAutorizadoDocumentoParaPagoDelServicioDeDispersion(String numeroDocumento, String claveEPO)
		throws NafinException{
		System.out.println("DispersionBean::estaAutorizadoDocumentoParaPagoDelServicioDeDispersion(E)");

		AccesoDB 		con 					= new AccesoDB();
		String 			qrySentencia		= null;
		List 				lVarBind				= new ArrayList();
		Registros		registros			= null;
		boolean			hayAutorizacion	= false;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 																				" +
					"	DECODE(COUNT(1),0,'false','true') AS HAY_DOCUMENTO_AUTORIZADO 	" +
					"FROM 										 										" +
					"	COM_DOCUMENTO 							 										" +
					"WHERE 										 										" +
					"	IC_EPO              = ? 		AND 										" +
					"	IG_NUMERO_DOCTO     = ?				 										" ;

				lVarBind.add(new Integer(claveEPO));
				lVarBind.add(numeroDocumento);

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					hayAutorizacion = registros.getString("HAY_DOCUMENTO_AUTORIZADO").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("DispersionBean::estaAutorizadoDocumentoParaPagoDelServicioDeDispersion(Exception)");
			System.out.println("claveEPO  		= " + claveEPO);
			System.out.println("numeroDocumento = " + numeroDocumento);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::estaAutorizadoDocumentoParaPagoDelServicioDeDispersion(S)");
		}

		return hayAutorizacion;
	}

	public boolean hayPYMEParametrizadaParaElServicioDeDispersion(String claveEPO)
		throws NafinException{

		System.out.println("DispersionBean::hayPYMEParametrizadaParaElServicioDeDispersion(E)");

		AccesoDB 		con 					= new AccesoDB();
		String 			qrySentencia		= null;
		List 				lVarBind				= new ArrayList();
		Registros		registros			= null;
		boolean			existeClave			= false;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 																			" +
					"	DECODE(COUNT(1),0,'false','true') AS EXISTE_PARAMETRIZACION " +
					"FROM 																			" +
					"	COM_PARAMETROS_DISPERSION 												" +
					"WHERE 																			" +
					"	IC_EPO  = ? 																";

				lVarBind.add(new Integer(claveEPO));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					existeClave = registros.getString("EXISTE_PARAMETRIZACION").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("DispersionBean::hayPYMEParametrizadaParaElServicioDeDispersion(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::hayPYMEParametrizadaParaElServicioDeDispersion(S)");
		}

		return existeClave;
	}

	public boolean existeClavePYMENAFIN()
		throws NafinException{

		System.out.println("DispersionBean::existeClavePYMENAFIN(E)");

		AccesoDB 		con 					= new AccesoDB();
		String 			qrySentencia		= null;
		List 				lVarBind				= new ArrayList();
		Registros		registros			= null;
		boolean			existeClave			= false;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 																			" +
					"	DECODE(IC_PYME_NAFIN,null,'false','true') AS EXISTE_CLAVE 	" +
					"FROM 																			" +
					"	COM_PARAM_GRAL 															" +
					"WHERE 																			" +
					"	IC_PARAM_GRAL  = ?														";

				lVarBind.add(new Integer(1));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					existeClave = registros.getString("EXISTE_CLAVE").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("DispersionBean::existeClavePYMENAFIN(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::existeClavePYMENAFIN(S)");
		}

		return existeClave;
	}

	public String getNombreMoneda(String claveMoneda)
		throws NafinException{

		System.out.println("DispersionBean::getNombreMoneda(E)");

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String			resultado				= "";

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 									" +
					"	CD_NOMBRE AS NOMBRE_MONEDA		" +
					"FROM 									" +
					"	COMCAT_MONEDA 						" +
					"WHERE 									" +
					"	IC_MONEDA  = ?						" ;

				lVarBind.add(new Integer(claveMoneda));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					resultado = registros.getString("NOMBRE_MONEDA");
				}

		}catch(Exception e){
			System.out.println("DispersionBean::getNombreMoneda(Exception)");
			System.out.println(" claveMoneda = " + claveMoneda);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::getNombreMoneda(S)");
		}
		return resultado;
	 }

	 public void generaDocumentoParaCobroDelServicioDeDispersion(HashMap documento, HashMap acuse, String clavePYME)
		throws NafinException{

		System.out.println("DispersionBean::generaDocumentoParaCobroDelServicioDeDispersion(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		boolean			lbOK				= true;

		try {

				con.conexionDB();

				qrySentencia =
					"INSERT INTO 							" +
					"	COM_ACUSE1							" +
					"		( 									" +
					"			CC_ACUSE, 					" +
					"			IN_TOTAL_DOCTO_MN, 		" +
					"			FN_TOTAL_MONTO_MN, 		" +
					"			DF_FECHAHORA_CARGA, 		" +
					"			IN_TOTAL_DOCTO_DL, 		" +
					"			FN_TOTAL_MONTO_DL, 		" +
					"			IC_USUARIO, 				" +
					"			CG_RECIBO_ELECTRONICO 	" +
					"		) 									" +
					"VALUES 									" +
					"		( 									" +
					"			?,								" + //NUMERO_DE_ACUSE
					"			?,								" + //NUMERO_DOCTOS_MN
					"			?,								" + //MONTO_TOTAL_MN
					"			SYSDATE,						" +
					"			?,								" + //NUMERO_DOCTOS_DL
					"			?,								" + //MONTO_TOTAL_DL
					"			?,								" + //CLAVE_USUARIO
					"			?								" + //NUMERO_DE_RECIBO
					"	  ) ";

				lVarBind.add((String)acuse.get("NUMERO_DE_ACUSE"));
				lVarBind.add((String)acuse.get("NUMERO_DOCTOS_MN"));
				lVarBind.add((String)acuse.get("MONTO_TOTAL_MN"));
				lVarBind.add((String)acuse.get("NUMERO_DOCTOS_DL"));
				lVarBind.add((String)acuse.get("MONTO_TOTAL_DL"));
				lVarBind.add((String)acuse.get("CLAVE_USUARIO"));
				lVarBind.add((String)acuse.get("NUMERO_DE_RECIBO"));

				con.ejecutaUpdateDB(qrySentencia, lVarBind);

				boolean insertarDocumentoMN 	= (documento.get("INSERTA_DOCTO_MN")  != null &&  "true".equals((String)documento.get("INSERTA_DOCTO_MN"))) ?true:false;
				boolean insertarDocumentoUSD 	= (documento.get("INSERTA_DOCTO_USD") != null &&  "true".equals((String)documento.get("INSERTA_DOCTO_USD")))?true:false;

				if(insertarDocumentoMN){
					qrySentencia =
						"INSERT INTO 												" +
						"	COM_DOCUMENTO 											" +
						"		( 														" +
						"			IC_DOCUMENTO,									" +
						"			IC_PYME,											" +
						"			IC_EPO,											" +
						"			CC_ACUSE,										" +
						"			IG_NUMERO_DOCTO,								" +
						"			DF_FECHA_DOCTO,								" +
						"			DF_FECHA_VENC,									" +
						"			IC_MONEDA,										" +
						"			FN_MONTO,										" +
						"			CS_DSCTO_ESPECIAL,							" +
						"			IC_ESTATUS_DOCTO,								" +
						"			CT_REFERENCIA,									" +
						"			DF_ALTA,											" +
						"			CS_CAMBIO_IMPORTE							   " +
						"		) 														" +
						"VALUES 														" +
						"		( 														" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			TO_DATE(?,'DD/MM/YYYY HH24:MI:SS' ),  	" +
						"			TO_DATE(?,'DD/MM/YYYY' ), 					" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			TO_DATE(?,'DD/MM/YYYY HH24:MI:SS' ),	" +
						"			? 												   " +
						"		) 														";

					lVarBind.clear();
					lVarBind.add(getNumeroMaximoDocto());
					lVarBind.add(new Integer(clavePYME));
					lVarBind.add(new Integer((String)documento.get("IC_EPO")));
					lVarBind.add((String)documento.get("CC_ACUSE"));
					lVarBind.add((String)documento.get("IG_NUMERO_DOCTO"));
					lVarBind.add((String)documento.get("DF_FECHA_DOCTO"));
					lVarBind.add((String)documento.get("DF_FECHA_VENC"));
					lVarBind.add(new Integer((String)documento.get("IC_MONEDA")));
					lVarBind.add((String)documento.get("FN_MONTO"));
					lVarBind.add((String)documento.get("CS_DSCTO_ESPECIAL"));
					lVarBind.add(new Integer((String)documento.get("IC_ESTATUS_DOCTO")));
					lVarBind.add((String)documento.get("CT_REFERENCIA"));
					lVarBind.add((String)documento.get("DF_ALTA"));
					lVarBind.add((String)documento.get("CS_CAMBIO_IMPORTE"));

					con.ejecutaUpdateDB(qrySentencia, lVarBind);
				}

				if(insertarDocumentoUSD){
					qrySentencia =
						"INSERT INTO 												" +
						"	COM_DOCUMENTO 											" +
						"		( 														" +
						"			IC_DOCUMENTO,									" +
						"			IC_PYME,											" +
						"			IC_EPO,											" +
						"			CC_ACUSE,										" +
						"			IG_NUMERO_DOCTO,								" +
						"			DF_FECHA_DOCTO,								" +
						"			DF_FECHA_VENC,									" +
						"			IC_MONEDA,										" +
						"			FN_MONTO,										" +
						"			CS_DSCTO_ESPECIAL,							" +
						"			IC_ESTATUS_DOCTO,								" +
						"			CT_REFERENCIA,									" +
						"			DF_ALTA,											" +
						"			CS_CAMBIO_IMPORTE							   " +
						"		) 														" +
						"VALUES 														" +
						"		( 														" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			TO_DATE(?,'DD/MM/YYYY HH24:MI:SS' ),  	" +
						"			TO_DATE(?,'DD/MM/YYYY' ), 					" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			?, 												" +
						"			TO_DATE(?,'DD/MM/YYYY HH24:MI:SS' ),	" +
						"			? 												   " +
						"		) 														";

					lVarBind.clear();
					lVarBind.add(getNumeroMaximoDocto());
					lVarBind.add(new Integer(clavePYME));
					lVarBind.add(new Integer((String)documento.get("IC_EPO")));
					lVarBind.add((String)documento.get("CC_ACUSE"));
					lVarBind.add((String)documento.get("IG_NUMERO_DOCTO"));
					lVarBind.add((String)documento.get("DF_FECHA_DOCTO"));
					lVarBind.add((String)documento.get("DF_FECHA_VENC"));
					lVarBind.add(new Integer((String)documento.get("IC_MONEDA_USD")));
					lVarBind.add((String)documento.get("FN_MONTO_USD"));
					lVarBind.add((String)documento.get("CS_DSCTO_ESPECIAL"));
					lVarBind.add(new Integer((String)documento.get("IC_ESTATUS_DOCTO")));
					lVarBind.add((String)documento.get("CT_REFERENCIA_USD"));
					lVarBind.add((String)documento.get("DF_ALTA"));
					lVarBind.add((String)documento.get("CS_CAMBIO_IMPORTE"));

					con.ejecutaUpdateDB(qrySentencia, lVarBind);
				}

			} catch(Exception e) {
				System.out.println("DispersionBean::generaDocumentoParaCobroDelServicioDeDispersion(Exception)");
				System.out.println("documento  = " + documento );
				System.out.println("clavePYME  = " + clavePYME );
				e.printStackTrace();
				lbOK = false;
				throw new NafinException("SIST0001");
			} finally {
				con.terminaTransaccion(lbOK);
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
				System.out.println("DispersionBean::generaDocumentoParaCobroDelServicioDeDispersion(S)");
			}
	}

	public String getClavePYMENAFIN()
		throws NafinException{

		System.out.println("DispersionBean::getClavePYMENAFIN(E)");

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String			resultado				= null;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 											" +
					"	IC_PYME_NAFIN AS CLAVE_PYME_NAFIN 	" +
					"FROM 											" +
					"	COM_PARAM_GRAL 							" +
					"WHERE 											" +
					"	IC_PARAM_GRAL  = ?						" ;

				lVarBind.add(new Integer(1));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					resultado = registros.getString("CLAVE_PYME_NAFIN");
				}

		}catch(Exception e){
			System.out.println("DispersionBean::getClavePYMENAFIN(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::getClavePYMENAFIN(S)");
		}

		return resultado;
	}

	public String getClaveDelProveedorDeDispersion(String claveEPO)
		throws NafinException{

		System.out.println("DispersionBean::getClavePYMENAFIN(E)");

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String			resultado				= null;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 									" +
					"	IC_PYME AS CLAVE_PYME		 	" +
					"FROM 									" +
					"	COM_PARAMETROS_DISPERSION 		" +
					"WHERE 									" +
					"	IC_EPO  = ?							" ;

				lVarBind.add(new Integer(claveEPO));

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					resultado = registros.getString("CLAVE_PYME");
				}

		}catch(Exception e){
			System.out.println("DispersionBean::getClaveDelProveedorDeDispersion(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::getClaveDelProveedorDeDispersion(S)");
		}

		return resultado;
	}

	public boolean existeRelacionEPOconPYMENAFIN(String claveEPO, String clavePYME)
		throws NafinException{

		System.out.println("DispersionBean::existeRelacionEPOconPYMENAFIN(E)");

		AccesoDB 		con 					= new AccesoDB();
		String 			qrySentencia		= null;
		List 				lVarBind				= new ArrayList();
		Registros		registros			= null;
		boolean			hayRelacion			= false;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 																	" +
					"	DECODE(COUNT(1),0,'false','true') AS EXISTE_RELACION 	" +
					"FROM 																	" +
					"	 COMREL_PYME_EPO 													" +
					"WHERE 																	" +
					"	IC_EPO    = ? AND 												" +
					"	IC_PYME   = ?														";

				lVarBind.add(new Integer(claveEPO));
				lVarBind.add(new Integer(clavePYME));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					hayRelacion = registros.getString("EXISTE_RELACION").equals("true")?true:false;
				}

		} catch(Exception e) {
			System.out.println("DispersionBean::existeRelacionEPOconPYMENAFIN(Exception)");
			System.out.println("	claveEPO  = " + claveEPO	);
			System.out.println("	clavePYME = " + clavePYME	);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::existeRelacionEPOconPYMENAFIN(S)");
		}

		return hayRelacion;
	}

	public  String getNumeroMaximoDocto()
		throws NafinException {

		System.out.println("DispersionBean::getNumeroMaximoDocto(E)");

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String			ic_documento			= null;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 	" +
					"	SEQ_COM_DOCUMENTO.NEXTVAL AS NUEVO_IC_DOCUMENTO " +
					"FROM 	" +
					"	DUAL 	";

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					ic_documento = registros.getString("NUEVO_IC_DOCUMENTO");
				}

		}catch(Exception e){
			System.out.println("DispersionBean::getNumeroMaximoDocto(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::getNumeroMaximoDocto(S)");
		}

		return ic_documento;
	}

	public String getMontoDocumentosNoOperados(HttpSession session){

		System.out.println("DispersionBean::getMontoDocumentosNoOperados(E)");

		List 		lReportes 	= (ArrayList)session.getAttribute("lReportes");
		String 	montoTotal 	= "0.00";

		if(lReportes != null && lReportes.size() > 0) {

			int 			i				= 0;	// REPORTE DE PYMES CON DISPERSION
			RepDispEpo 	repDispEpo 	= (RepDispEpo)lReportes.get(i);
			Registros 	reporte 		= repDispEpo.getRepPymesConDisp();
			double		rs_monto		= 0;
			double		totMonto 	= 0;

			while(reporte.next()) {
				rs_monto 	= "".equals(reporte.getString("Monto Total"))?0:Double.parseDouble(reporte.getString("Monto Total"));
				totMonto		+= rs_monto;
			}

			montoTotal = Comunes.formatoDecimal(totMonto, 2,false);
		}

		System.out.println("DispersionBean::getMontoDocumentosNoOperados(S)");
		return montoTotal;
	}

	public String getTotalNoDispersado(HttpSession session){

		System.out.println("DispersionBean::getTotalNoDispersado(E)");

		List 		lReportes 	= (ArrayList)session.getAttribute("lReportes");
		String 	montoTotal 	= "0.00";

		if(lReportes != null && lReportes.size() > 0) {

			int 			i				= 0;	// REPORTE DE PYMES CON DISPERSION
			RepDispEpo 	repDispEpo 	= (RepDispEpo)lReportes.get(i);
			Registros 	reporte 		= repDispEpo.getRepPymesSinDisp();
			double		rs_monto		= 0;
			double		totMonto 	= 0;

			while(reporte.next()) {
				rs_monto 	= "".equals(reporte.getString("Monto Total"))?0:Double.parseDouble(reporte.getString("Monto Total"));
				totMonto		+= rs_monto;
			}

			montoTotal = Comunes.formatoDecimal(totMonto, 2,false);
		}

		System.out.println("DispersionBean::getTotalNoDispersado(S)");

		return montoTotal;
	}

	public HashMap getListaDeIntermediariosConOperacion(HttpSession session){

		System.out.println("DispersionBean::getListaDeIntermediariosConOperacion(E)");

		List 			lReportes 			= (ArrayList)session.getAttribute("lReportes");
		String 		montoTotal 			= "0.00";
		HashMap		datos					= new HashMap();
		ArrayList	registros			= new ArrayList();
		HashMap		registro				= null;

		if(lReportes != null && lReportes.size() > 0) {

			int 			i					= 0;	// REPORTE DE PYMES CON DISPERSION
			RepDispEpo 	repDispEpo 		= (RepDispEpo)lReportes.get(i);
			Registros 	reporte 			= repDispEpo.getRepPagoIf();
			String 		intermediario	= "";
			double		rs_monto			= 0;
			double		totMonto 		= 0;

			while(reporte.next()) {
				registro			= new HashMap();

				intermediario 	= 	reporte.getString("IF");
				rs_monto 		= 	"".equals(reporte.getString("Monto Total"))?0:Double.parseDouble(reporte.getString("Monto Total"));

				totMonto			+= rs_monto;
				registro.put("INTERMEDIARIO",	intermediario );
				registro.put("MONTO",			"$ " + Comunes.formatoDecimal(rs_monto, 2));

				registros.add(registro);
			}

			datos.put("LISTA_BANCOS",registros);

			montoTotal = Comunes.formatoDecimal(totMonto, 2,false);
			System.out.println("MONTO_TOTAL = " + montoTotal);
			datos.put("MONTO_TOTAL",montoTotal);
		}

		System.out.println("DispersionBean::getListaDeIntermediariosConOperacion(S)");

		return datos;
	}

	public HashMap autenticaFirmaDigitalv2(String pkcs7, String textoFirmado, String serial, String numeroCliente)
		throws NafinException{

		System.out.println("DispersionBean::autenticaFirmaDigital(E)");

		pkcs7					= (pkcs7 			== null?"":pkcs7);
		textoFirmado		= (textoFirmado 	== null?"":textoFirmado);
		numeroCliente		= (numeroCliente 	== null?"":numeroCliente);
		serial				= (serial 			== null?"":serial);

		final String 							PRODUCTO_DESCUENTO	= "1";
		String 									folioCertificado 		= null;
		netropology.utilerias.Seguridad	seguridad				= null;
		char 										getReceipt 				= 'Y';
		HashMap  								resultado				= new HashMap();

		if ( !serial.equals("") && !textoFirmado.equals("") && !pkcs7.equals("") && !numeroCliente.equals("") ) {

			//folioCertificado 	= "01CC"+numeroCliente+new SimpleDateFormat("ddMMyyHHmm").format(new java.util.Date());

			Acuse 	acuse 	= new Acuse(Acuse.ACUSE_EPO, PRODUCTO_DESCUENTO);
			folioCertificado	= acuse.toString();
			seguridad 			= new netropology.utilerias.Seguridad();

			if (!seguridad.autenticar(folioCertificado, serial, pkcs7, textoFirmado, getReceipt)) {
				resultado.put("VERIFICACION_EXITOSA","false");
				resultado.put("MENSAJE_ERROR","La autentificacion no se llevo a cabo: <br>"+seguridad.mostrarError());
			}else{
				resultado.put("VERIFICACION_EXITOSA","true");
				resultado.put("RECIBO",seguridad.getAcuse());
				resultado.put("FOLIO",folioCertificado);
			}

		}else{
			System.out.println("DispersionBean::autenticaFirmaDigital(Exception)");
			System.out.println("Uno o mas parametros vienen vacios: ");
			System.out.println("      textoFirmado   = " + textoFirmado);
			System.out.println("      serial         = " + serial);
			System.out.println("      numeroCliente  = " + numeroCliente);
			System.out.println("      pkcs7          = " + pkcs7);
			throw new NafinException("GRAL0021");
		}

		System.out.println("DispersionBean::autenticaFirmaDigital(S)");
		return resultado;
	}

	public List getDetalleFlujoFondos(String claveFlujoFondos)
		throws NafinException {

		System.out.println("DispersionBean::getDetalleFlujoFondos(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		List				resultado					= new ArrayList();

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT															 						" +
					"	D.IG_NUMERO_DOCTO        						AS NUMERO_DE_DOCUMENTO, " +
					"	P.CG_RAZON_SOCIAL        						AS PYME,					 	" +
					"	NVL(D.FN_MONTO,0)        						AS IMPORTE_DOCUMENTO,	" +
					"	NVL(D.FN_MONTO_DSCTO,NVL(D.FN_MONTO,0))  	AS IMPORTE_DESCUENTO    " +
					"FROM 															 						" +
					"	COMREL_DOCUMENTO_FF DFF, 								 						" +
					"	COM_DOCUMENTO       D,									 						" +
					"	COMCAT_PYME         P 									 						" +
					"WHERE 															 						" +
					"	DFF.IC_DOCUMENTO 	= D.IC_DOCUMENTO  AND			 						" +
					"	D.IC_PYME        	= P.IC_PYME       AND			 						" +
					"	IC_FLUJO_FONDOS 	= ?															";

				lVarBind.add(claveFlujoFondos);
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				while(registros != null && registros.next() ){
					HashMap 	registro		= new HashMap();

					registro.put("NUMERO_DE_DOCUMENTO", registros.getString("NUMERO_DE_DOCUMENTO"));
					registro.put("PYME", 					registros.getString("PYME"));
					registro.put("IMPORTE_DOCUMENTO", 	"$" + Comunes.formatoDecimal(registros.getString("IMPORTE_DOCUMENTO"),2,true));
					registro.put("IMPORTE_DESCUENTO", 	"$" + Comunes.formatoDecimal(registros.getString("IMPORTE_DESCUENTO"),2,true));

					resultado.add(registro);
				}

		} catch(Exception e) {
			System.out.println("DispersionBean::getDetalleFlujoFondos(Exception)");
			System.out.println("claveFlujoFondos  = " + claveFlujoFondos);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::getDetalleFlujoFondos(S)");
		}

		return resultado;
	}

	public List getDetalleFlujoFondosConError(String claveFlujoFondos)
		throws NafinException {

		System.out.println("DispersionBean::getDetalleFlujoFondosConError(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;

		List				resultado					= new ArrayList();

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT															 " +
					"	D.IG_NUMERO_DOCTO        AS NUMERO_DE_DOCUMENTO, " +
					"	P.CG_RAZON_SOCIAL        AS PYME,					 " +
					"	NVL(D.FN_MONTO,0)        AS IMPORTE_DOCUMENTO,	 " +
					"	NVL(D.FN_MONTO_DSCTO,0)  AS IMPORTE_DESCUENTO    " +
					"FROM 															 " +
					"	COMREL_DOCUMENTO_FF_ERR EDFF, 						 " +
					"	COM_DOCUMENTO       D,									 " +
					"	COMCAT_PYME         P 									 " +
					"WHERE 															 " +
					"	EDFF.IC_DOCUMENTO 		= D.IC_DOCUMENTO  AND	 " +
					"	D.IC_PYME        			= P.IC_PYME       AND	 " +
					"	EDFF.IC_FLUJO_FONDOS 	= ?";

				lVarBind.add(claveFlujoFondos);
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				while(registros != null && registros.next() ){
					HashMap 	registro		= new HashMap();

					registro.put("NUMERO_DE_DOCUMENTO", registros.getString("NUMERO_DE_DOCUMENTO"));
					registro.put("PYME", 					registros.getString("PYME"));
					registro.put("IMPORTE_DOCUMENTO", 	"$" + Comunes.formatoDecimal(registros.getString("IMPORTE_DOCUMENTO"),2,true));
					registro.put("IMPORTE_DESCUENTO", 	"$" + Comunes.formatoDecimal(registros.getString("IMPORTE_DESCUENTO"),2,true));

					resultado.add(registro);
				}

		} catch(Exception e) {
			System.out.println("DispersionBean::getDetalleFlujoFondosConError(Exception)");
			System.out.println("claveFlujoFondos  = " + claveFlujoFondos);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("DispersionBean::getDetalleFlujoFondosConError(S)");
		}

		return resultado;
	}

	/**
	 *   Este metodo devuelve el query que se usa para consultar el valor
	 *   del campo: cs_dispersion_no_negociables de la tabla
	 *   comrel_producto_epo.
	 *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - EPOs
	 *
	 *   @return El query para hacer la consulta y adaptado para usuar variables bind.
	 *
	 */
	public String getQueryClaveDispersionNoNegociables(){

			log.info("getQueryClaveDispersionNoNegociables(E)");

			StringBuffer query = new StringBuffer();
			query.append(	" SELECT cs_dispersion_no_negociables "+
								"   FROM comrel_producto_epo "+
								"  WHERE ic_epo = ? "+
								"    AND ic_producto_nafin = ? ");

			log.debug("getQueryClaveDispersionNoNegociables.query = <"+query.toString()+">");
			log.info("getQueryClaveDispersionNoNegociables(S)");

			return query.toString();
	}

	/**
	 *   Este metodo devuelve el query que se usa para consultar el numero
	 *   de documentos la tabla com_dispersion por epo e if.
	 *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - IFs
	 *
	 *   @return El query para hacer la consulta y adaptado para usuar variables bind.
	 *
	 */
	public String getQueryConteoDispersion(){

			log.info("getQueryConteoDispersion(E)");

			StringBuffer query = new StringBuffer();
			query.append(	"select count(*) as num_registros from com_dispersion "+
								"where ic_if = ? "+ // noIf
								" and ic_epo = ? "+ // noEpo
								" and trunc(df_registro) = TO_DATE(?,'dd/mm/yyyy')"); // fechaReg

			log.debug("getQueryConteoDispersion.query = <"+query.toString()+">");
			log.info("getQueryConteoDispersion(S)");

			return query.toString();
	}

	/**
	 *   Este metodo devuelve el query utilizado para obtener el numero de folio siguiente
	 *   correspondiente a la tabla com_dispersion.
	 *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - IFs
	 *
	 *   @return El query para hacer la consulta.
	 *
	 */
	public String getQueryNumeroFolioSiguiente(){

			log.info("getQueryNumeroFolioSiguiente(E)");

			StringBuffer query = new StringBuffer();
			query.append("select nvl(max(ic_folio),0)+1 from com_dispersion"); // fechaReg

			log.debug("getQueryNumeroFolioSiguiente.query = <"+query.toString()+">");
			log.info("getQueryNumeroFolioSiguiente(S)");

			return query.toString();
	}

	/**
	 *   Este metodo devuelve el query que se utiliza para consultar el detalle de la dispersion
	 *	  de "documentos" por EPO.
	 *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - MONITOR
	 *
	 *   @param  claveEpo							Cadena de Texto con la clave de la Epo a consultar.
	 *   @param  sufijoFechaVencimientoPyme	Sufijo que sirve para indicar de que tabla se reportara la fecha de vencimiento de la pyme.
	 *			                        			Este parametro se obtiene del metodo: operaFechaVencPyme(ic_epo) del bean de SeleccionDocumento.
	 *   @param  fechaVencimientoInicio 		Cadena de texto con la fecha de Vencimiento Inicial del Documento. Formato DD/MM/YYYY.
	 *   @param  fechaVencimientoFin				Cadena de texto con la Fecha de Vencimiento Final del Documento. Formato DD/MM/YYYY.
	 *   @return El query para hacer la consulta.
	 *
	 */
	 public String getQueryMonitoreoDispersionPorEPO(String claveEpo, String sufijoFechaVencimientoPyme, String fechaVencimientoInicio, String fechaVencimientoFin ){

			log.info("getQueryMonitoreoDispersionPorEPO(E)");

			StringBuffer 	query 			= new StringBuffer();
			StringBuffer 	qrySentencia	= new StringBuffer();
			String 			condicion 		= "";
         String         sFechaVencPyme = "";

			String ic_epo				= (claveEpo 					!= null)?claveEpo						:"";
			String txt_fecha_ven_de = (fechaVencimientoInicio 	!= null)?fechaVencimientoInicio	:"";
			String txt_fecha_ven_a 	= (fechaVencimientoFin 		!= null)?fechaVencimientoFin		:"";

			if(!"".equals(ic_epo)) {
				condicion = "    AND d.ic_epo = "+ic_epo+" ";
				sFechaVencPyme = (sufijoFechaVencimientoPyme != null)?sufijoFechaVencimientoPyme:"";
			}

			query.append(
				" SELECT /*+index(c) index(cpe) index(est) use_nl(est*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)) AS total_importe, c.ic_bancos_tef,"   +
				"        c.cg_cuenta, c.ic_estatus_cecoban AS cecoban, cec.cd_descripcion AS desccec,epo.ic_epo AS epo, d.ic_pyme "   +
				"			, c.ic_moneda			 AS ic_moneda	"  +
				"        , d.ic_estatus_docto  AS ic_estatus_docto "  +
				"		   , '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comrel_producto_epo cpe,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        com_cuentas c,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"        ,comcat_estatus_cecoban cec"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND cpe.ic_producto_nafin = c.ic_producto_nafin"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND c.ic_moneda in (1,54) "   +
				"    AND d.ic_moneda = c.ic_moneda " +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND c.ic_tipo_cuenta in (40,"+claveCuentaSwift+") "   +
				"    AND c.ic_epo = d.ic_epo"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND d.ic_estatus_docto IN (2, 1)"   +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+") )"   +
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,"   +
				"           c.ic_bancos_tef,"   +
				"           c.cg_cuenta,"   +
				"           cec.cd_descripcion, c.ic_estatus_cecoban,epo.ic_epo,"+
				"           d.ic_pyme, "  +
				"    			c.ic_moneda, "  +
				"           d.ic_estatus_docto, "  +
				"				c.ic_tipo_cuenta "  +
				" UNION"   +
				" SELECT /*+index(est) index(cpe) index(est) use_nl(est)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)) AS total_importe, 0, '0', 0  AS cecoban, '0' AS desccec,epo.ic_epo AS epo, d.ic_pyme "   +
				"		,	d.ic_moneda AS ic_moneda "  +
				"     ,  d.ic_estatus_docto  AS ic_estatus_docto "  +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        comrel_producto_epo cpe,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND cpe.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto IN (2, 1)"   +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 1 "   +
				"                      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_epo = epo.ic_epo"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = 40 ) "   +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+") )"   +
				"    AND d.ic_moneda = 1 " +
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,epo.ic_epo,"+
				"           d.ic_pyme, "+
				"				d.ic_moneda, " +
				"           d.ic_estatus_docto "  +
				" UNION"   +
				" SELECT /*+index(est) index(cpe) index(est) use_nl(est)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)) AS total_importe, 0, '0', 0  AS cecoban, '0' AS desccec,epo.ic_epo AS epo, d.ic_pyme "   +
				"		,	d.ic_moneda AS ic_moneda "  +
				"     ,  d.ic_estatus_docto  AS ic_estatus_docto "  +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        comrel_producto_epo cpe,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND cpe.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto IN (2, 1)"   +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 54 "   +
				"                      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_epo = epo.ic_epo"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = " + claveCuentaSwift + " ) " +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+") )"   +
				"    AND d.ic_moneda = 54 " +
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,epo.ic_epo,"+
				"           d.ic_pyme, "+
				"				d.ic_moneda, " +
				"           d.ic_estatus_docto "+
				" UNION"   +
				" SELECT /*+index(c) index(cpe) index(est)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd/mm/yyyy') AS fechaVenc,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', 0, 1)) AS num_doctos,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', -1, 1) * (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0))) AS total_importe, "+
				"        c.ic_bancos_tef,"   +
				"        c.cg_cuenta, c.ic_estatus_cecoban AS cecoban, cec.cd_descripcion AS desccec,epo.ic_epo AS epo, d.ic_pyme "   +
				"		,	c.ic_moneda	       AS ic_moneda "  +
				"     ,  d.ic_estatus_docto  AS ic_estatus_docto "  +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comrel_producto_epo cpe,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        com_cuentas c,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"        ,comcat_estatus_cecoban cec"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND cpe.ic_producto_nafin = c.ic_producto_nafin"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND c.ic_moneda in (1,54) "   +
				"    AND d.ic_moneda = c.ic_moneda " +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND c.ic_tipo_cuenta in (40,"+claveCuentaSwift+") "   +
				"    AND c.ic_epo = d.ic_epo"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND d.ic_estatus_docto IN (9,10) "   +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+", 'C') )"   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc"+sFechaVencPyme+","   +
				"           c.ic_bancos_tef,"   +
				"           c.cg_cuenta,"   +
				"           cec.cd_descripcion, c.ic_estatus_cecoban,epo.ic_epo,"+
				"           d.ic_pyme, "+
				"				c.ic_moneda, "  +
				"           d.ic_estatus_docto, "  +
				"				c.ic_tipo_cuenta "  +
				" UNION"   +
				" SELECT /*+index(d) use_nl(d pym epo cpe crn est) index(cpe) index(est) */ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd/mm/yyyy') AS fechaVenc,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', 0, 1)) AS num_doctos,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', -1, 1) * (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0))) AS total_importe, "+
				"        0, '0', 0  AS cecoban, '0' AS desccec,epo.ic_epo AS epo, d.ic_pyme "   +
				"		,	d.ic_moneda     AS ic_moneda "  +
				"     ,  d.ic_estatus_docto  AS ic_estatus_docto "  +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        comrel_producto_epo cpe,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND cpe.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto IN (9,10) "   +
				"    AND d.ic_moneda = 1 " +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 1 "   +
				"                      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_epo = epo.ic_epo"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = 40 ) "   +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+", 'C') )"   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc"+sFechaVencPyme+",epo.ic_epo,"+
				"           d.ic_pyme,"+
				"				d.ic_moneda,"  +
				"           d.ic_estatus_docto "  +
				" UNION"   +
				" SELECT /*+index(d) use_nl(d pym epo cpe crn est) index(cpe) index(est) */ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd/mm/yyyy') AS fechaVenc,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', 0, 1)) AS num_doctos,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', -1, 1) * (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0))) AS total_importe, "+
				"        0, '0', 0  AS cecoban, '0' AS desccec,epo.ic_epo AS epo, d.ic_pyme "   +
				"		,	d.ic_moneda     AS ic_moneda "  +
				"     ,  d.ic_estatus_docto  AS ic_estatus_docto "  +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        comrel_producto_epo cpe,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND cpe.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto IN (9,10) "   +
				"    AND d.ic_moneda = 54 " +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 54 "   +
				"                      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_epo = epo.ic_epo"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = "+claveCuentaSwift+" ) "   +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+", 'C') )"   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc"+sFechaVencPyme+",epo.ic_epo,"+
				"           d.ic_pyme,"+
				"				d.ic_moneda, "  +
				"           d.ic_estatus_docto "  +
				" UNION"   +
				" SELECT /*+index(d) index(c) use_nl(d epo cif cpe c crn est cec) index(cpe) index(est)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico, '' AS nompyme,"   +
				"        cif.cg_razon_social AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto) AS total_importe, c.ic_bancos_tef,"   +
				"        c.cg_cuenta, c.ic_estatus_cecoban  AS cecoban, cec.cd_descripcion  AS desccec,epo.ic_epo AS epo, TO_NUMBER(NULL) ic_pyme "   +
				"		,	c.ic_moneda			 AS ic_moneda "  +
				"     ,  d.ic_estatus_docto  AS ic_estatus_docto "  +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif,"   +
				"        comrel_producto_epo cpe,"   +
				"        com_cuentas c,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"        ,comcat_estatus_cecoban cec"   +
				"  WHERE d.ic_if = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_if = cif.ic_if"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND cpe.ic_producto_nafin = c.ic_producto_nafin"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"    AND crn.cg_tipo = 'I'"   +
				"    AND c.ic_moneda in (1,54) "   +
				"    AND d.ic_moneda = c.ic_moneda " +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND c.ic_tipo_cuenta in (40,"+claveCuentaSwift+") "   +
				"    AND c.cg_tipo_afiliado = 'I'"   +
				"    AND c.ic_epo = epo.ic_epo"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND d.ic_estatus_docto IN (4,11) "   +
				"	  AND cif.ic_if not in(12) "+
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
        "    AND d.cs_dscto_especial  !='C' " +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           cif.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,"   +
				"           c.ic_bancos_tef,"   +
				"           c.cg_cuenta,"   +
				"           cec.cd_descripcion,c.ic_estatus_cecoban,epo.ic_epo, "+
				"				c.ic_moneda,"  +
				"           d.ic_estatus_docto, " +
				"				c.ic_tipo_cuenta"  +
				" UNION"   +
				" SELECT /*+index(d) use_nl(d epo cif cpe crn est) index(cpe) index(est)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico, '' AS nompyme,"   +
				"        cif.cg_razon_social AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto) AS total_importe, 0, '0', 0  AS cecoban, '0' AS desccec,epo.ic_epo AS epo, TO_NUMBER(NULL) ic_pyme "   +
				"		,	d.ic_moneda         AS ic_moneda  "  +
				"     ,  d.ic_estatus_docto  AS ic_estatus_docto "  +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif,"   +
				"        comrel_producto_epo cpe,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"  WHERE d.ic_if = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_if = cif.ic_if"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND crn.cg_tipo = 'I'"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND cpe.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto IN (4,11) "   +
				"    AND d.ic_moneda = 1 " +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 1 "   +
				"                      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_epo = epo.ic_epo"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.cg_tipo_afiliado = 'I'"   +
				"                      AND c.ic_tipo_cuenta = 40 ) "   +
				"	  AND cif.ic_if not in(12) "+
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				"    AND d.cs_dscto_especial  !='C' " +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           cif.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,"   +
				"           epo.ic_epo,"  +
				"				d.ic_moneda,"  +
				"           d.ic_estatus_docto" +
				" UNION"   +
				" SELECT /*+index(d) use_nl(d epo cif cpe crn est) index(cpe) index(est)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico, '' AS nompyme,"   +
				"        cif.cg_razon_social AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto) AS total_importe, 0, '0', 0  AS cecoban, '0' AS desccec,epo.ic_epo AS epo, TO_NUMBER(NULL) ic_pyme "   +
				"		,	d.ic_moneda         AS ic_moneda  "  +
				"     ,  d.ic_estatus_docto  AS ic_estatus_docto "  +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif,"   +
				"        comrel_producto_epo cpe,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"  WHERE d.ic_if = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_if = cif.ic_if"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND crn.cg_tipo = 'I'"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND cpe.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto IN (4,11) "   +
				"    AND d.ic_moneda = 54 " +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 54 "   +
				"                      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_epo = epo.ic_epo"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.cg_tipo_afiliado = 'I'"   +
				"                      AND c.ic_tipo_cuenta = " + claveCuentaSwift + " ) " +
				"	  AND cif.ic_if not in(12) "+
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				"    AND d.cs_dscto_especial  !='C' " +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           cif.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,"   +
				"           epo.ic_epo,"  +
				"				d.ic_moneda,"  +
				"           d.ic_estatus_docto");

			qrySentencia.append( "select * from (" + query.toString() + ") order by nomepo, fechaVenc, nompyme, nomif " );

			log.debug("getQueryMonitoreoDispersionPorEPO.qrySentencia 					= <"+qrySentencia.toString()		+">");
			log.debug("getQueryMonitoreoDispersionPorEPO.claveEpo							= <"+claveEpo							+">");
			log.debug("getQueryMonitoreoDispersionPorEPO.sufijoFechaVencimientoPyme = <"+sufijoFechaVencimientoPyme	+">");
			log.debug("getQueryMonitoreoDispersionPorEPO.fechaVencimientoInicio		= <"+fechaVencimientoInicio		+">");
			log.debug("getQueryMonitoreoDispersionPorEPO.fechaVencimientoFin			= <"+fechaVencimientoFin			+">");

			log.info("getQueryMonitoreoDispersionPorEPO(S)");

			return qrySentencia.toString();
	}

	/*
	 *  Generar Archivos Adjuntos del Reporte de Dispersion a ser enviado por correo.
	 *  Se crea en el archivo en dos versiones: una en formato XLS y la otra en formato PDF.
	 *
	 * @param 	session Sesion
	 * @param	strDirectorioTemp	Directorio Temporal donde se guardaran los archivos creados
	 * @param	strDirectorioPublicacion	Directorio de Publicacion
	 * @param 	fileName Nombre del Archivo a Generar
	 */

	 /**
	 *   Devuelve el query para traer el catalogo de epos correspondiente al
	 *   Tipo de Monitoreo: PEMEX-REF
	 *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - MONITOR
	 *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - DISPERSION PEMEX - Gasolineros
	 *
	 *   @return El query para hacer la consulta y adaptado para usuar variables bind.
	 *
	 */
	 public String getQueryCatalogoEposMonitoreoPEMEXREF(){

			log.info("getQueryCatalogoEposMonitoreoPEMEXREF(E)");

			StringBuffer query = new StringBuffer();
			query.append(	" SELECT   /*+index(epo CP_COMCAT_EPO_PK)*/"   +
								"        epo.ic_epo, epo.cg_razon_social"   +
								"   FROM comcat_epo epo, comrel_epo_tipo_financiamiento tf"   +
								"  WHERE epo.ic_epo = tf.ic_epo"   +
								"    AND tf.ic_producto_nafin = ? "   + // 4
								"    AND tf.ic_tipo_financiamiento = ? " // 5
							); // fechaReg

			log.debug("getQueryCatalogoEposMonitoreoPEMEXREF.query = <"+query.toString()+">");
			log.info("getQueryCatalogoEposMonitoreoPEMEXREF(S)");

			return query.toString();
	 }

	 /**
	  *
	  *   Devuelve el detalle del Catalogo de EPOS correspondiente al Tipo de Monitoreo: PEMEX-REF
	  *
	  *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - MONITOR
	  *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - DISPERSION PEMEX - Gasolineros
	  *	@throws AppException
	  *
	  *   @return <tt>List</tt> con del detalle de las EPO.
	  *
	  *   @author jshernandez
	  *   @since  F###-2013 ADMIN NAFIN - Migracion Monitor; 24/06/2013 06:37:18 p.m.
	  *
	  */
	 public List getDetalleCatalogoEposMonitoreoPEMEXREF(){

	 	log.info("getDetalleCatalogoEposMonitoreoPEMEXREF(E)");

	 	AccesoDB 			con 		= new AccesoDB();
	 	PreparedStatement ps  		= null;
	 	ResultSet 			rs  		= null;
	 	String 				query 	= "";

	 	List					catalogo = new ArrayList();

	 	try {

	 		// Conectarse a la base de datos
	 	 	con.conexionDB();

	 	 	// Query
	 	 	query = getQueryCatalogoEposMonitoreoPEMEXREF();
			ps 	= con.queryPrecompilado(query);
			ps.setInt(1,4);
			ps.setInt(2,5);
			rs = ps.executeQuery();

			// Construir catalogo
			while(rs.next()) {

				String  claveEPO 	= (rs.getString("ic_epo")				== null)?"":rs.getString("ic_epo");
				String  nombreEPO = (rs.getString("cg_razon_social")	== null)?"":rs.getString("cg_razon_social");

				HashMap registro  = new HashMap();
				registro.put("clave",       claveEPO  );
				registro.put("descripcion", nombreEPO );

				catalogo.add(registro);

			}

		} catch( Exception e){

			log.error("getDetalleCatalogoEposMonitoreoPEMEXREF(Exception)");
			log.error("getDetalleCatalogoEposMonitoreoPEMEXREF.query = <" + query+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al obtener el Catalogo de EPO");

		} finally {

			if(rs != null) try { rs.close(); }catch(Exception e){}
			if(ps != null) try { ps.close(); }catch(Exception e){}

			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}

			log.info("getDetalleCatalogoEposMonitoreoPEMEXREF(S)");

		}

		return catalogo;

	 }

	 /**
	  *  Este metodo devuelve el query que se utilza para el monitoreo de la dispersion en PEMEX-REF.
	  *  PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - MONITOR
	  *
	  *  @param	claveEpo	Clave de La Epo a monitorear
	  *  @return Cadena de Texto con el query en question y adaptado para hacer uso de variables bind.
	  *
	  */
	 public String getQueryDetalleMonitoreoPemexRef(String claveEpo){
		 log.info("getQueryDetalleMonitoreoPemexRef(E)");

		 	StringBuffer 	query 			= new StringBuffer();
			StringBuffer 	qrySentencia 	= new StringBuffer();

			String 			ic_epo 			= (claveEpo == null)?"":claveEpo;
			String 			condicion		= "";

		 	if(!"".equals(ic_epo)) {
				condicion = "    AND d.ic_epo = ? ";
			}

			query.append(
					" SELECT   /*+index(d) use_nl(d crn pym c epo est etf cec)*/ "   +
					"          epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
					"          pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
					"          TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavenc,"   +
					"          COUNT (d.ic_documento) AS num_doctos,"   +
					"          SUM (d.fn_monto) AS total_importe, c.ic_bancos_tef, c.cg_cuenta,"   +
					"          c.ic_estatus_cecoban AS cecoban, cec.cd_descripcion AS desccec,"   +
					"          epo.ic_epo AS epo"   +
					"     FROM dis_documento d,"   +
					"          comrel_nafin crn,"   +
					"          comcat_pyme pym,"   +
					"          com_cuentas c,"   +
					"          comcat_epo epo,"   +
					"          comcat_estatus_docto est,"   +
					"          comrel_epo_tipo_financiamiento etf,"   +
					"          comcat_estatus_cecoban cec"   +
					"    WHERE d.ic_epo = crn.ic_epo_pyme_if"   +
					"      AND d.ic_epo = epo.ic_epo"   +
					"      AND d.ic_pyme = pym.ic_pyme"   +
					"      AND d.ic_estatus_docto = est.ic_estatus_docto"   +
					"      AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban(+)"   +
					"      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
					"      AND etf.ic_epo = d.ic_epo"   +
					"      AND d.ic_tipo_financiamiento = etf.ic_tipo_financiamiento "   +
					"      AND d.ic_producto_nafin = ?"   + // 4
					"      AND etf.ic_producto_nafin = ?"   + // 4
					"      AND etf.ic_tipo_financiamiento = ?"   + // 5
					"      AND crn.cg_tipo = ? "   + // 'E'
					"      AND c.ic_moneda = ? "   + // 1
					"      AND d.ic_moneda = ? "   + // 1
					"      AND c.ic_producto_nafin = ? "   + // 4
					"      AND c.ic_tipo_cuenta = ? "   + // 40
					"      AND d.ic_estatus_docto = ? "   + // 2
					"      AND d.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')"   + // fechaVencimientoInicial
					"      AND d.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + ?)"   + // fechaVencimientoFinal,1
					condicion +
					" GROUP BY epo.cg_razon_social,"   +
					"          crn.ic_nafin_electronico,"   +
					"          pym.cg_razon_social,"   +
					"          est.cd_descripcion,"   +
					"          d.df_fecha_venc,"   +
					"          c.ic_bancos_tef,"   +
					"          c.cg_cuenta,"   +
					"          cec.cd_descripcion,"   +
					"          c.ic_estatus_cecoban,"   +
					"          epo.ic_epo"
				) ;

				qrySentencia.append("select * from ("+query.toString()+") order by nomepo,fechaVenc,nompyme,nomif ");

		 log.debug("getQueryDetalleMonitoreoPemexRef.qrySentencia = < " + qrySentencia.toString() + " >");
		 log.info("getQueryDetalleMonitoreoPemexRef(S)");

		 return qrySentencia.toString();

	 }

	 /**
	 *	  Este metodo devuelve el query que se utilza para imprimir el detalle del monitoreo de la dispersion por EPO.
	 *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - MONITOR
	 *
	 *   @param  claveEpo							Cadena de Texto con la clave de la Epo a consultar.
	 *   @param  sufijoFechaVencimientoPyme	Sufijo que sirve para indicar de que tabla se reportara la fecha de vencimiento de la pyme.
	 *			                        			Este parametro se obtiene del metodo: operaFechaVencPyme(ic_epo) del bean de SeleccionDocumento.
	 *   @param  fechaVencimientoInicio 		Cadena de texto con la fecha de Vencimiento Inicial del Documento. Formato DD/MM/YYYY.
	 *   @param  fechaVencimientoFin				Cadena de texto con la Fecha de Vencimiento Final del Documento. Formato DD/MM/YYYY.
	 *   @return El query para hacer la consulta.
	 *
	 */
	public String getQueryDetalleImpresionMonitoreoDispersionPorEPO(String claveEpo, String sufijoFechaVencimientoPyme, String fechaVencimientoInicio, String fechaVencimientoFin ){

			log.info("getQueryDetalleImpresionMonitoreoDispersionPorEPO(E)");

			StringBuffer query 			= new StringBuffer();
			StringBuffer qrySentencia 	= new StringBuffer();

			String	condicion 		= "";
         String	sFechaVencPyme = "";

			String ic_epo				= (claveEpo 					!= null)?claveEpo						:"";
			String txt_fecha_ven_de = (fechaVencimientoInicio 	!= null)?fechaVencimientoInicio	:"";
			String txt_fecha_ven_a 	= (fechaVencimientoFin 		!= null)?fechaVencimientoFin		:"";

			if(!"".equals(ic_epo)) {
				condicion = "    AND d.ic_epo = "+ic_epo+" ";
				sFechaVencPyme = (sufijoFechaVencimientoPyme != null)?sufijoFechaVencimientoPyme:"";
			}

			query.append(
				" SELECT /*+index(c)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)) AS total_importe, c.ic_bancos_tef,"   +
				"        c.cg_cuenta, c.ic_estatus_cecoban AS cecoban, cec.cd_descripcion AS desccec,epo.ic_epo AS epo "   +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comrel_producto_epo cpe,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        com_cuentas c,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"        ,comcat_estatus_cecoban cec"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND cpe.ic_producto_nafin = c.ic_producto_nafin"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND c.ic_moneda = 1"   +
				"    AND d.ic_moneda = c.ic_moneda"   +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND c.ic_tipo_cuenta = 40"   +
				"    AND c.ic_epo = d.ic_epo"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND d.ic_estatus_docto IN (2, 1)"   +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+") )"   +
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,"   +
				"           c.ic_bancos_tef,"   +
				"           c.cg_cuenta,"   +
				"           cec.cd_descripcion, c.ic_estatus_cecoban,epo.ic_epo"   +
				" UNION"   +
				" SELECT /*+index(est)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)) AS total_importe, 0, '0', 0  AS cecoban, '0' AS desccec,epo.ic_epo AS epo "   +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        comrel_producto_epo cpe,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND cpe.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto IN (2, 1)"   +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 1"   +
				"                      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_epo = epo.ic_epo"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = 40)"   +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+") )"   +
				"    AND d.ic_moneda = 1"   +
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,epo.ic_epo"   +
				" UNION"   +
				" SELECT /*+index(c)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd/mm/yyyy') AS fechaVenc,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', 0, 1)) AS num_doctos,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', -1, 1) * (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0))) AS total_importe, "+
				"        c.ic_bancos_tef,"   +
				"        c.cg_cuenta, c.ic_estatus_cecoban AS cecoban, cec.cd_descripcion AS desccec,epo.ic_epo AS epo "   +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comrel_producto_epo cpe,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        com_cuentas c,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"        ,comcat_estatus_cecoban cec"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND cpe.ic_producto_nafin = c.ic_producto_nafin"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND c.ic_moneda = 1"   +
				"    AND d.ic_moneda = c.ic_moneda"   +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND c.ic_tipo_cuenta = 40"   +
				"    AND c.ic_epo = d.ic_epo"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND d.ic_estatus_docto IN (9)"   +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+", 'C') )"   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc"+sFechaVencPyme+","   +
				"           c.ic_bancos_tef,"   +
				"           c.cg_cuenta,"   +
				"           cec.cd_descripcion, c.ic_estatus_cecoban,epo.ic_epo"   +
				" UNION"   +
				" SELECT /*+index(d) use_nl(d pym epo cpe crn est)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico,"   +
				"        pym.cg_razon_social AS nompyme, '' AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc"+sFechaVencPyme+",'dd/mm/yyyy') AS fechaVenc,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', 0, 1)) AS num_doctos,"   +
				"        SUM (DECODE (d.cs_dscto_especial, 'C', -1, 1) * (d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0))) AS total_importe, "+
				"        0, '0', 0  AS cecoban, '0' AS desccec,epo.ic_epo AS epo "   +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        comrel_producto_epo cpe,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"  WHERE d.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_pyme = pym.ic_pyme"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND cpe.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto IN (9)"   +
				"    AND d.ic_moneda = 1"   +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 1"   +
				"                      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_epo = epo.ic_epo"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = 40)"   +
				"      AND ( ( d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"               AND   d.fn_monto - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > 0 )"   +
				"           OR d.cs_dscto_especial IN ("+dispersionTradicional+", 'C') )"   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc"+sFechaVencPyme+" <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           pym.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc"+sFechaVencPyme+",epo.ic_epo"   +
				" UNION"   +
				" SELECT /*+index(d) index(c) use_nl(d epo cif cpe c crn est cec)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico, '' AS nompyme,"   +
				"        cif.cg_razon_social AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto) AS total_importe, c.ic_bancos_tef,"   +
				"        c.cg_cuenta, c.ic_estatus_cecoban  AS cecoban, cec.cd_descripcion  AS desccec,epo.ic_epo AS epo "   +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif,"   +
				"        comrel_producto_epo cpe,"   +
				"        com_cuentas c,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"        ,comcat_estatus_cecoban cec"   +
				"  WHERE d.ic_if = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_if = cif.ic_if"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND cpe.ic_producto_nafin = c.ic_producto_nafin"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"    AND crn.cg_tipo = 'I'"   +
				"    AND c.ic_moneda = 1"   +
				"    AND d.ic_moneda = c.ic_moneda"   +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND c.ic_tipo_cuenta = 40"   +
				"    AND c.cg_tipo_afiliado = 'I'"   +
				"    AND c.ic_epo = epo.ic_epo"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND d.ic_estatus_docto = 4"   +
				"	  AND cif.ic_if not in(12) "+
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
        "    AND d.cs_dscto_especial  !='C' " +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           cif.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,"   +
				"           c.ic_bancos_tef,"   +
				"           c.cg_cuenta,"   +
				"           cec.cd_descripcion,c.ic_estatus_cecoban,epo.ic_epo"   +
				" UNION"   +
				" SELECT /*+index(d) use_nl(d epo cif cpe crn est)*/ "+
				"        epo.cg_razon_social AS nomepo, crn.ic_nafin_electronico, '' AS nompyme,"   +
				"        cif.cg_razon_social AS nomif, est.cd_descripcion,"   +
				"        TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS fechaVenc,"   +
				"        COUNT (d.ic_documento) AS num_doctos,"   +
				"        SUM (d.fn_monto) AS total_importe, 0, '0', 0  AS cecoban, '0' AS desccec,epo.ic_epo AS epo "   +
				"		, '15cadenas/15mantenimiento/15dispersion/15monitordispe.jsp' as pantalla" +
				"   FROM com_documento d,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif,"   +
				"        comrel_producto_epo cpe,"   +
				"        comrel_nafin crn,"   +
				"        comcat_estatus_docto est"   +
				"  WHERE d.ic_if = crn.ic_epo_pyme_if"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND d.ic_if = cif.ic_if"   +
				"    AND d.ic_estatus_docto = est.ic_estatus_docto"   +
				"    AND cpe.ic_epo = d.ic_epo"   +
				"    AND crn.cg_tipo = 'I'"   +
				"    AND cpe.cg_dispersion = 'S'"   +
				"    AND cpe.ic_producto_nafin = 1"   +
				"    AND d.ic_estatus_docto = 4"   +
				"    AND d.ic_moneda = 1"   +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 1"   +
				"                      AND crn.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_epo = epo.ic_epo"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.cg_tipo_afiliado = 'I'"   +
				"                      AND c.ic_tipo_cuenta = 40)"   +
				"	  AND cif.ic_if not in(12) "+
				"    AND d.df_fecha_venc >= TO_DATE ('"+txt_fecha_ven_de+"', 'dd/mm/yyyy') "   +
				"    AND d.df_fecha_venc <  TO_DATE ('"+txt_fecha_ven_a+"', 'dd/mm/yyyy') + 1 "   +
				"    AND d.cs_dscto_especial  !='C' " +
				condicion+
				"  GROUP BY epo.cg_razon_social,"   +
				"           crn.ic_nafin_electronico,"   +
				"           cif.cg_razon_social,"   +
				"           est.cd_descripcion,"   +
				"           d.df_fecha_venc,"   +
				"           epo.ic_epo" );

		qrySentencia.append("select * from ("+ query.toString()+") order by nomepo,fechaVenc,nompyme,nomif ");

		log.debug("getQueryDetalleImpresionMonitoreoDispersionPorEPO.qrySentencia 						= <"+qrySentencia.toString()+">");
		log.debug("getQueryDetalleImpresionMonitoreoDispersionPorEPO..claveEpo							= <"+claveEpo							+">");
		log.debug("getQueryDetalleImpresionMonitoreoDispersionPorEPO.sufijoFechaVencimientoPyme   = <"+sufijoFechaVencimientoPyme	+">");
		log.debug("getQueryDetalleImpresionMonitoreoDispersionPorEPO.fechaVencimientoInicio		  	= <"+fechaVencimientoInicio		+">");
		log.debug("getQueryDetalleImpresionMonitoreoDispersionPorEPO.fechaVencimientoFin			  	= <"+fechaVencimientoFin			+">");

		log.info("getQueryDetalleImpresionMonitoreoDispersionPorEPO(S)");

		return qrySentencia.toString();
	}

	/**
	 *	  Este metodo devuelve el query que se utilza para obtener el detalle del monitoreo de
	 *   la dispersion por IF.
	 *   PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - MONITOR
	 *
	 *   @param	claveIF		Cadena de texto con la clave del IF a consultar.
	 *   @param	claveEpo		Cadena de texto con la clave de la Epo a consultar.
	 *   @param	radHab		Cadena de texto con el parametro cs_aceptacion de la tabla comrel_pyme_epo.
	 *   @param	chkCuenta	Cadena de texto donde se especifica si la Cuenta no es CLABE
	 *
	 *   @return El query para hacer la consulta.
	 *
	 */
	 public String getQueryDetalleMonitoreoDispersionPorIF(String claveIF, String claveEpo, String radHab, String chkCuenta){
			log.info("getQueryDetalleMonitoreoDispersionPorIF(E)");

			StringBuffer condicion = new StringBuffer(
														"      AND s.df_operacion >= TRUNC (SYSDATE)"   +
														"      AND s.df_operacion < (TRUNC (SYSDATE) + 1)" +
														"      AND ds.cs_opera_fiso = 'N' "   // Documentos registrados bajo la modalidad normal (sin Fideicomiso para Desarrollo de Proveedores).
													);

			String ic_if 		= (claveIF 		== null)?"":claveIF;
			String ic_epo 		= (claveEpo 	== null)?"":claveEpo;
			String rad_hab 	= (radHab 		== null)?"":radHab;
			String chk_cuenta = (chkCuenta 	== null)?"":chkCuenta;

			if(!"".equals(ic_if)) {
				condicion.append("    AND ds.ic_if =  "+ic_if+" ");
			}
			if(!"".equals(ic_epo)) {
				condicion.append("    AND ds.ic_epo = "+ic_epo+" ");
			}
			if("H".equals(rad_hab)) {
				condicion.append("    AND rpe.cs_aceptacion = '"+rad_hab+"' ");
			}
			else {
				if("N".equals(rad_hab)) {
					condicion.append("    AND rpe.cs_aceptacion != 'H' ");
				}
			}

			StringBuffer qrySentenciaConCuenta 		= new StringBuffer();
			StringBuffer qrySentenciaSinCuenta 		= new StringBuffer();
			StringBuffer qrySentenciaConCuentaUSD 	= new StringBuffer();
			StringBuffer qrySentenciaSinCuentaUSD 	= new StringBuffer();
			StringBuffer qrySentencia 					= new StringBuffer();

			qrySentenciaConCuenta.append(
				" SELECT /*+ INDEX (CIF CP_COMCAT_IF_PK) USE_NL(CIF) index(c IN_COM_CUENTAS_01_NUK) use_nl(n) */ ds.ic_if AS nomif, cif.cg_razon_social, epo.cg_razon_social,"   +
				"        p.cg_razon_social AS nompyme, n.ic_nafin_electronico,"   +
				"        rpe.cs_aceptacion, COUNT (ds.ic_documento) AS num_doctos,"   +
				"        SUM (ds.in_importe_recibir) AS total_importe, c.ic_bancos_tef,"   +
				"        c.cg_cuenta, cec.ic_estatus_cecoban, cec.cd_descripcion"   +
				"			,d.ic_moneda as ic_moneda "  +
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_solicitud s,"   +
				"        comrel_nafin n,"   +
				"        comrel_pyme_epo rpe,"   +
				"        comcat_epo epo,"   +
				"        comcat_pyme p,"   +
				"        comcat_if cif,"   +
				"        com_cuentas c,"   +
				"        comcat_estatus_cecoban cec"   +
				"  WHERE ds.ic_documento = d.ic_documento"   +
				"    AND ds.ic_documento = s.ic_documento"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND ds.ic_if = cif.ic_if"   +
				"    AND rpe.ic_pyme = d.ic_pyme"   +
				"    AND rpe.ic_epo = d.ic_epo"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
				"    AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND n.cg_tipo = 'P'"   +
				"    AND d.ic_estatus_docto = 4"   +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND c.ic_tipo_cuenta = 40"   +
				"    AND c.ic_epo = d.ic_epo"   +
				"    AND d.ic_moneda=c.ic_moneda   "  +
				"    AND c.ic_moneda = 1"   +condicion.toString()+
				"  GROUP BY ds.ic_if, cif.cg_razon_social, epo.cg_razon_social, p.cg_razon_social, n.ic_nafin_electronico,"   +
				"           rpe.cs_aceptacion, c.ic_bancos_tef, c.cg_cuenta, cec.cd_descripcion,cec.ic_estatus_cecoban, d.ic_moneda");

			qrySentenciaSinCuenta.append(
				" SELECT /*+ use_nl(n) */ ds.ic_if AS nomif, cif.cg_razon_social, epo.cg_razon_social,"   +
				"        p.cg_razon_social AS nompyme, n.ic_nafin_electronico,"   +
				"        rpe.cs_aceptacion, COUNT (ds.ic_documento) AS num_doctos,"   +
				"        SUM (ds.in_importe_recibir) AS total_importe, 0, '0', 0, '0'"   +
				"			,d.ic_moneda as ic_moneda "  +
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_solicitud s,"   +
				"        comrel_nafin n,"   +
				"        comrel_pyme_epo rpe,"   +
				"        comcat_pyme p,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif"   +
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND ds.ic_if = cif.ic_if"   +
				"    AND rpe.ic_pyme = d.ic_pyme"   +
				"    AND rpe.ic_epo = d.ic_epo"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND ds.ic_documento = s.ic_documento"   +
				"    AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND n.cg_tipo = 'P'"   +
				"    AND d.ic_moneda=1   "  +
				"    AND d.ic_estatus_docto = 4"   +condicion.toString()+
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 1"   +
				"                      AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = 40"   +
				"                      AND c.ic_epo = d.ic_epo)"   +
				"  GROUP BY ds.ic_if, cif.cg_razon_social, epo.cg_razon_social,"   +
				"           p.cg_razon_social, n.ic_nafin_electronico, rpe.cs_aceptacion, d.ic_moneda");

			qrySentenciaConCuentaUSD.append(
				" SELECT  /*+ INDEX (CIF CP_COMCAT_IF_PK) USE_NL(CIF)  index(c IN_COM_CUENTAS_01_NUK) use_nl(n) */ ds.ic_if AS nomif, cif.cg_razon_social, epo.cg_razon_social,"   +
				"        p.cg_razon_social AS nompyme, n.ic_nafin_electronico,"   +
				"        rpe.cs_aceptacion, COUNT (ds.ic_documento) AS num_doctos,"   +
				"        SUM (ds.in_importe_recibir) AS total_importe, c.ic_bancos_tef,"   +
				"        c.cg_cuenta, cec.ic_estatus_cecoban, cec.cd_descripcion"   +
				"			,d.ic_moneda as ic_moneda "  +
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_solicitud s,"   +
				"        comrel_nafin n,"   +
				"        comrel_pyme_epo rpe,"   +
				"        comcat_epo epo,"   +
				"        comcat_pyme p,"   +
				"        comcat_if cif,"   +
				"        com_cuentas c,"   +
				"        comcat_estatus_cecoban cec"   +
				"  WHERE ds.ic_documento = d.ic_documento"   +
				"    AND ds.ic_documento = s.ic_documento"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND ds.ic_if = cif.ic_if"   +
				"    AND rpe.ic_pyme = d.ic_pyme"   +
				"    AND rpe.ic_epo = d.ic_epo"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
				"    AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND n.cg_tipo = 'P'"   +
				"    AND d.ic_estatus_docto = 4"   +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND c.ic_tipo_cuenta = " + claveCuentaSwift  +
				"    AND c.ic_epo = d.ic_epo"   +
				"    AND d.ic_moneda=c.ic_moneda   "  +
				"    AND c.ic_moneda = 54"   +condicion.toString()+
				"  GROUP BY ds.ic_if, cif.cg_razon_social, epo.cg_razon_social, p.cg_razon_social, n.ic_nafin_electronico,"   +
				"           rpe.cs_aceptacion, c.ic_bancos_tef, c.cg_cuenta, cec.cd_descripcion,cec.ic_estatus_cecoban, d.ic_moneda");

			qrySentenciaSinCuentaUSD.append(
				" SELECT /*+ use_nl(n) */ ds.ic_if AS nomif, cif.cg_razon_social, epo.cg_razon_social,"   +
				"        p.cg_razon_social AS nompyme, n.ic_nafin_electronico,"   +
				"        rpe.cs_aceptacion, COUNT (ds.ic_documento) AS num_doctos,"   +
				"        SUM (ds.in_importe_recibir) AS total_importe, 0, '0', 0, '0'"   +
				"			,d.ic_moneda as ic_moneda "  +
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_solicitud s,"   +
				"        comrel_nafin n,"   +
				"        comrel_pyme_epo rpe,"   +
				"        comcat_pyme p,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif"   +
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND ds.ic_if = cif.ic_if"   +
				"    AND rpe.ic_pyme = d.ic_pyme"   +
				"    AND rpe.ic_epo = d.ic_epo"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND ds.ic_documento = s.ic_documento"   +
				"    AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND n.cg_tipo = 'P'"   +
				"    AND d.ic_moneda=54   "  +
				"    AND d.ic_estatus_docto = 4"   +condicion.toString()+
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 54"   +
				"                      AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = " + claveCuentaSwift +
				"                      AND c.ic_epo = d.ic_epo)"   +
				"  GROUP BY ds.ic_if, cif.cg_razon_social, epo.cg_razon_social,"   +
				"           p.cg_razon_social, n.ic_nafin_electronico, rpe.cs_aceptacion, d.ic_moneda");

			// Mostrar Cuenta clabe(M.N.) si el checkbox "Sin cuenta clabe" esta "unchecado"
			if("".equals(chk_cuenta)) {
				qrySentencia.append(qrySentenciaConCuenta);
			}

			// Mostrar siempre Registros en USD con Cuenta Swift
			if(!"".equals(qrySentencia.toString())){
				qrySentencia.append(" UNION ALL ");
	 		}
			qrySentencia.append(qrySentenciaConCuentaUSD);


			if(!"".equals(qrySentencia.toString()))
				qrySentencia.append(" UNION ALL ");

			qrySentencia.append(
								qrySentenciaSinCuenta.toString()+
								" UNION ALL "+
								qrySentenciaSinCuentaUSD.toString()+
								"  ORDER BY nomif, nompyme, ic_moneda "
							 );

			log.debug("getQueryDetalleMonitoreoDispersionPorIF.qrySentencia = <"+qrySentencia.toString()+">");
			log.debug("getQueryDetalleMonitoreoDispersionPorIF.claveIF 		 = <"+claveIF+">");
			log.debug("getQueryDetalleMonitoreoDispersionPorIF.claveEpo		 = <"+claveEpo+">");
			log.debug("getQueryDetalleMonitoreoDispersionPorIF.radHab 		 = <"+radHab+">");
			log.debug("getQueryDetalleMonitoreoDispersionPorIF.chkCuenta 	 = <"+chkCuenta+">");

			log.info("getQueryDetalleMonitoreoDispersionPorIF(S)");

			return qrySentencia.toString();
		}

	/**
	 *  Este metodo devuelve el query que se utilza para obtener el detalle del Monitoreo de
	 *  Tipo Infonavit.
	 *  PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - MONITOR
	 *
	 *  @param	claveIF		Cadena de texto con la clave del IF a consultar.
	 *  @param	claveEpo		Cadena de texto con la clave de la Epo a consultar.
	 *  @param	radHab		Cadena de texto con el parametro cs_aceptacion de la tabla comrel_pyme_epo.
	 *  @param	chkCuenta	Cadena de texto donde se especifica si la Cuenta no es CLABE
	 *
	 *  @return	El query para hacer la consulta.
	 */
	 public String getQueryDetalleMonitoreoDispersionPorInfonavit(String claveIf, String claveEpo, String radHab, String chkCuenta){

		 	log.info("getQueryDetalleMonitoreoDispersionPorInfonavit(E)");

			StringBuffer condicion 		= new StringBuffer();
			StringBuffer condicionHab 	= new StringBuffer();

			String ic_if 		= (claveIf 		== null)?"":claveIf;
			String ic_epo 		= (claveEpo 	== null)?"":claveEpo;
			String rad_hab 	= (radHab 		== null)?"":radHab;
			String chk_cuenta	= (chkCuenta 	== null)?"":chkCuenta;

			if(!"".equals(ic_if)) {
				condicion.append("    AND ds.ic_if =  "+ic_if+" ");
			}
			if(!"".equals(ic_epo)) {
				condicion.append("    AND ds.ic_epo = "+ic_epo+" ");
			}
			if("H".equals(rad_hab)) {
				condicionHab.append("    AND rpe.cs_aceptacion = '"+rad_hab+"' ");
			}
			else {
				if("N".equals(rad_hab)) {
					condicionHab.append("    AND rpe.cs_aceptacion != 'H' ");
				}
			}

			StringBuffer qrySentencia				= new StringBuffer();
			StringBuffer qrySentenciaConCuentaP = new StringBuffer();
			StringBuffer qrySentenciaSinCuentaP = new StringBuffer();
			StringBuffer qrySentenciaConCuentaB = new StringBuffer();
			StringBuffer qrySentenciaSinCuentaB = new StringBuffer();

			qrySentenciaConCuentaP.append(
				" SELECT ds.ic_if AS nomif, i.cg_razon_social,epo.cg_razon_social, n.ic_nafin_electronico,"   +
				"        p.cg_razon_social AS nompyme, '' AS beneficiario,"   +
				"        rpe.cs_aceptacion AS habilitada,"   +
				"        COUNT (ds.ic_documento) AS num_doctos,"   +
				"        SUM (ds.in_importe_recibir - NVL (ds.fn_importe_recibir_benef, 0))"   +
				"              AS total_importe,"   +
				"        c.ic_bancos_tef, c.cg_cuenta, c.ic_estatus_cecoban,"   +
				"        cec.cd_descripcion"   +
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_solicitud s,"   +
				"        comrel_nafin n,"   +
				"        comrel_pyme_epo rpe,"   +
				"        comcat_pyme p,"   +
				"        comcat_epo epo,"   +
				"        comcat_if i,"   +
				"        com_cuentas c,"   +
				"        comcat_estatus_cecoban cec"   +
				"  WHERE ds.ic_documento = d.ic_documento"   +
				"    AND ds.ic_documento = s.ic_documento"   +
				"    AND d.ic_pyme = p.ic_pyme"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND ds.ic_epo = epo.ic_epo"   +
				"    AND d.ic_epo = rpe.ic_epo"   +
				"    AND d.ic_pyme = rpe.ic_pyme"   +
				"    AND ds.ic_if = i.ic_if"   +
				"    AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
				"    AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND c.ic_epo = d.ic_epo "   +
				"    AND c.ic_moneda = 1   "   +
				"    AND d.ic_moneda = c.ic_moneda   "  +
				"    AND n.cg_tipo = 'P'"   +
				"    AND d.ic_estatus_docto = 4"   +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND c.ic_tipo_cuenta = 40"   +
				"    AND i.ig_tipo_piso = 1"   +
				"    AND ((    d.cs_dscto_especial in  ("+dispersionDistribuido+")"   +
				"          AND ds.in_importe_recibir - ds.fn_importe_recibir_benef > 0)"   +
				"         OR d.cs_dscto_especial IN ("+dispersionTradicional+"))"+condicion.toString()+condicionHab.toString()+
				"  GROUP BY ds.ic_if, i.cg_razon_social,epo.cg_razon_social,"   +
				"           n.ic_nafin_electronico,"   +
				"           p.cg_razon_social,"   +
				"           rpe.cs_aceptacion,"   +
				"           c.ic_bancos_tef,"   +
				"           c.cg_cuenta,"   +
				"           c.ic_estatus_cecoban,"   +
				"           cec.cd_descripcion");

			qrySentenciaSinCuentaP.append(
				" SELECT ds.ic_if AS nomif, i.cg_razon_social,epo.cg_razon_social, n.ic_nafin_electronico,"   +
				"        p.cg_razon_social AS nompyme, '' AS beneficiario,"   +
				"        rpe.cs_aceptacion AS habilitada,"   +
				"        COUNT (ds.ic_documento) AS num_doctos,"   +
				"        SUM (ds.in_importe_recibir - NVL (ds.fn_importe_recibir_benef, 0))"   +
				"              AS total_importe,"   +
				"        0, '0', 0, '0'"   +
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_solicitud s,"   +
				"        comrel_nafin n,"   +
				"        comrel_pyme_epo rpe,"   +
				"        comcat_pyme p,"   +
				"        comcat_epo epo,"   +
				"        comcat_if i"   +
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"    AND d.ic_pyme = n.ic_epo_pyme_if"   +
				"    AND d.ic_pyme = rpe.ic_pyme"   +
				"    AND d.ic_epo = rpe.ic_epo"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND ds.ic_epo = epo.ic_epo"   +
				"    AND ds.ic_documento = s.ic_documento"   +
				"    AND d.ic_documento = s.ic_documento"   +
				"    AND ds.ic_if = i.ic_if"   +
				"    AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = 1"   +
				"                      AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = 40"   +
				"                      AND c.ic_epo = d.ic_epo)"   +
				"    AND n.cg_tipo = 'P'"   +
				"    AND d.ic_estatus_docto = 4"   +
				"    AND d.ic_moneda=1   "  +
				"    AND i.ig_tipo_piso = 1"   +
				"    AND ((    d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"          AND ds.in_importe_recibir - ds.fn_importe_recibir_benef > 0)"   +
				"         OR d.cs_dscto_especial IN ("+dispersionTradicional+"))"+condicion.toString()+condicionHab.toString()+
				"  GROUP BY ds.ic_if, i.cg_razon_social,epo.cg_razon_social,"   +
				"           n.ic_nafin_electronico,"   +
				"           p.cg_razon_social,"   +
				"           rpe.cs_aceptacion");

			qrySentenciaConCuentaB.append(
				" SELECT ds.ic_if AS nomif, i.cg_razon_social,epo.cg_razon_social, n.ic_nafin_electronico, '' AS nompyme,"   +
				"        i2.cg_razon_social AS beneficiario, '' AS habilitada,"   +
				"        COUNT (ds.ic_documento) AS num_doctos,"   +
				"        SUM (ds.fn_importe_recibir_benef) AS total_importe,"   +
				"        c.ic_bancos_tef, c.cg_cuenta, c.ic_estatus_cecoban,"   +
				"        cec.cd_descripcion"   +
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_solicitud s,"   +
				"        comrel_nafin n,"   +
				"        comcat_epo epo,"   +
				"        comcat_if i2,"   +
				"        com_cuentas c,"   +
				"        comcat_moneda m,"   +
				"        comcat_if i,"   +
				"        comcat_estatus_cecoban cec"   +
				"  WHERE ds.ic_documento = d.ic_documento"   +
				"    AND ds.ic_documento = s.ic_documento"   +
				"    AND d.ic_beneficiario = i2.ic_if"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND ds.ic_if = i.ic_if"   +
				"    AND c.ic_moneda = m.ic_moneda"   +
				"    AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
				"    AND n.ic_epo_pyme_if = i2.ic_if"   +
				"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+) "   +
				"    AND n.cg_tipo = 'I'"   +
				"    AND d.ic_estatus_docto = 4"   +
				"    AND c.ic_producto_nafin = 1"   +
				"    AND d.ic_moneda=1   "  +
				"    AND d.ic_moneda = c.ic_moneda   "  +
				"    AND d.ic_epo = c.ic_epo  " +
				"    AND c.cg_tipo_afiliado = 'B'"   +
				"    AND c.ic_tipo_cuenta = 40"   +
				"    AND i.ig_tipo_piso = 1"   +
				"    AND d.cs_dscto_especial in ("+dispersionDistribuido+")"   +
				"    AND ds.fn_importe_recibir_benef > 0"+condicion.toString()+
				"  GROUP BY ds.ic_if, i.cg_razon_social,epo.cg_razon_social,"   +
				"           n.ic_nafin_electronico,"   +
				"           i2.cg_razon_social,"   +
				"           c.ic_bancos_tef,"   +
				"           c.cg_cuenta,"   +
				"           c.ic_estatus_cecoban,"   +
				"           cec.cd_descripcion");

			qrySentenciaSinCuentaB.append(
				" SELECT ds.ic_if AS nomif, i.cg_razon_social,epo.cg_razon_social, n.ic_nafin_electronico, '' AS nompyme,"   +
				"        i2.cg_razon_social AS beneficiario, '' AS habilitada,"   +
				"        COUNT (ds.ic_documento) AS num_doctos,"   +
				"        SUM (ds.fn_importe_recibir_benef) AS total_importe, 0, '0', 0,"   +
				"        '0'"   +
				"   FROM com_documento d,"   +
				"        com_docto_seleccionado ds,"   +
				"        com_solicitud s,"   +
				"        comrel_nafin n,"   +
				"        comcat_epo epo,"   +
				"        comcat_if i2,"   +
				"        comcat_if i"   +
				"  WHERE d.ic_documento = ds.ic_documento"   +
				"    AND d.ic_beneficiario = i2.ic_if"   +
				"    AND d.ic_epo = ds.ic_epo"   +
				"    AND d.ic_epo = epo.ic_epo"   +
				"    AND ds.ic_documento = s.ic_documento"   +
				"    AND ds.ic_if = i.ic_if"   +
				"    AND n.ic_epo_pyme_if = i2.ic_if"   +
				"    AND NOT EXISTS(SELECT 1"   +
				"                     FROM com_cuentas c"   +
				"                    WHERE c.ic_moneda = d.ic_moneda"   +
				"                      AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                      AND c.ic_producto_nafin = 1"   +
				"                      AND c.ic_tipo_cuenta = 40"   +
				"                      AND c.ic_epo = d.ic_epo)"   +
				"    AND n.cg_tipo = 'I'"   +
				"    AND d.ic_estatus_docto = 4"   +
				"    AND i.ig_tipo_piso = 1"   +
				"    AND d.ic_moneda=1   "  +
				"    AND d.cs_dscto_especial in  ("+dispersionDistribuido+")"   +
				"    AND ds.fn_importe_recibir_benef > 0"+condicion.toString()+
				"  GROUP BY ds.ic_if, i.cg_razon_social,epo.cg_razon_social,  n.ic_nafin_electronico,  i2.cg_razon_social");

				if("".equals(chk_cuenta)) {
					qrySentencia.append(qrySentenciaConCuentaP);
				}
			 	if(!"".equals(qrySentencia.toString()))
					qrySentencia.append(" UNION ALL");
				qrySentencia.append(qrySentenciaSinCuentaP);
				if("".equals(chk_cuenta)) {
				 	if(!"".equals(qrySentencia.toString()))
						qrySentencia.append(" UNION ALL");
					qrySentencia.append(qrySentenciaConCuentaB);
				}
			 	if(!"".equals(qrySentencia.toString()))
					qrySentencia.append(" UNION ALL");
				qrySentencia.append(qrySentenciaSinCuentaB);

				log.debug("getQueryDetalleMonitoreoDispersionPorInfonavit.qrySentencia = <"+ qrySentencia.toString()+">");
				log.debug("getQueryDetalleMonitoreoDispersionPorInfonavit.claveIf		= <" + claveIf		+">");
				log.debug("getQueryDetalleMonitoreoDispersionPorInfonavit.claveEpo	= <" + claveEpo	+">");
				log.debug("getQueryDetalleMonitoreoDispersionPorInfonavit.radHab		= <" + radHab		+">");
				log.debug("getQueryDetalleMonitoreoDispersionPorInfonavit.chkCuenta	= <" + chkCuenta	+">");
				log.info("getQueryDetalleMonitoreoDispersionPorInfonavit(S)");

				return qrySentencia.toString();

	 }

	/**
	 *
	 * Devuelve el query que se usa para traer el detalle de DISPERSION PEMEX Gasolineros.
	 * PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - DISPERSION PEMEX - Gasolineros
	 *
	 * @param estatus Cadena de texto con dos posibles valores: NEGOCIABLES, para documentos negociables
	 *                y PAGO_ANTICIPADO para documentos con pago anticipado.
	 *
	 * @return Cadena de texto con el query solicitado.
	 *
	 */
	public String getQueryDetallePemexGasolineros(String estatus){

		log.info("getQueryDetallePemexGasolineros(E)");

		StringBuffer 	query 							= new StringBuffer();

		if(estatus == null) return query.toString();

		boolean 			esEstatusNegociable 			= (estatus.equals("NEGOCIABLES"))		?true:false;
		boolean 			esEstatusPagoAnticipado 	= (estatus.equals("PAGO_ANTICIPADO"))	?true:false;

		if(esEstatusNegociable){
			query.append(
					" SELECT   /*+index(d) use_nl(d n c epo)*/"   +
					"           c.ic_cuenta, epo.ic_epo, epo.cg_rfc, epo.cg_razon_social,"   +
					"           COUNT (d.ic_documento) AS num_doctos, m.cd_nombre,"   +
					"           SUM (d.fn_monto) AS total_importe, c.ic_bancos_tef,"   +
					"           c.ic_tipo_cuenta, c.cg_cuenta,"   +
					"           TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fecha,"   +
					"           c.ic_estatus_tef, c.ic_estatus_cecoban"   +
					"     FROM dis_documento d,"   +
					"          comrel_nafin n,"   +
					"          com_cuentas c,"   +
					"          comcat_epo epo,"   +
					"          comcat_moneda m"   +
					"    WHERE d.ic_epo = epo.ic_epo"   +
					"      AND n.ic_epo_pyme_if = epo.ic_epo"   +
					"      AND c.ic_moneda = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND c.ic_producto_nafin = 4"   +
					"      AND c.ic_tipo_cuenta = 40"   +
					"      AND c.cg_tipo_afiliado = 'E'"   +
					"      AND n.cg_tipo = 'E'"   +
					"      AND m.ic_moneda = d.ic_moneda "   +
					"      AND d.ic_moneda = ?"   +
					"      AND d.ic_epo = ?"   +
					"      AND TRUNC (d.df_fecha_venc) = TO_DATE (?, 'dd/mm/yyyy')"   +
					"      AND d.ic_estatus_docto = ?"   +
					"      AND d.ic_producto_nafin = 4"   +
					" GROUP BY c.ic_cuenta,"   +
					"          epo.ic_epo,"   +
					"          epo.cg_rfc,"   +
					"          epo.cg_razon_social,"   +
					"          m.cd_nombre,"   +
					"          c.ic_bancos_tef,"   +
					"          c.ic_tipo_cuenta,"   +
					"          c.cg_cuenta,"   +
					"          TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy'),"   +
					"          c.ic_estatus_tef,"   +
					"          c.ic_estatus_cecoban"   +
					" ORDER BY epo.cg_razon_social"
				);
		}
		if(esEstatusPagoAnticipado){
			query.append(
					" SELECT   c.ic_cuenta, epo.ic_epo, epo.cg_rfc, epo.cg_razon_social,"   +
					"          COUNT (1) AS num_doctos, m.cd_nombre,"   +
					"          SUM (d.fn_importe) AS total_importe, c.ic_bancos_tef,"   +
					"          c.ic_tipo_cuenta, c.cg_cuenta,"   +
					"          TO_CHAR (d.df_fecha_aplicacion, 'dd/mm/yyyy') AS fecha,"   +
					"          c.ic_estatus_tef, c.ic_estatus_cecoban"   +
					"     FROM dis_pagos_ant d,"   +
					"          comrel_nafin n,"   +
					"          com_cuentas c,"   +
					"          comcat_epo epo,"   +
					"          comcat_moneda m"   +
					"    WHERE d.ic_epo = epo.ic_epo"   +
					"      AND n.ic_epo_pyme_if = epo.ic_epo"   +
					"      AND c.ic_moneda = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND c.ic_producto_nafin = 4"   +
					"      AND c.ic_tipo_cuenta = 40"   +
					"      AND c.cg_tipo_afiliado = 'E'"   +
					"      AND n.cg_tipo = 'E'"   +
					"      AND m.ic_moneda = d.ic_moneda "   +
					"      AND d.ic_moneda = ?"   +
					"      AND d.ic_epo = ?"   +
					"      AND TRUNC (d.df_fecha_aplicacion) = TO_DATE (?, 'dd/mm/yyyy')"   +
					"      AND d.cs_dispersion = 'N'"   +
					" GROUP BY c.ic_cuenta,"   +
					"          epo.ic_epo,"   +
					"          epo.cg_rfc,"   +
					"          epo.cg_razon_social,"   +
					"          m.cd_nombre,"   +
					"          c.ic_bancos_tef,"   +
					"          c.ic_tipo_cuenta,"   +
					"          c.cg_cuenta,"   +
					"          TO_CHAR (d.df_fecha_aplicacion, 'dd/mm/yyyy'),"   +
					"          c.ic_estatus_tef,"   +
					"          c.ic_estatus_cecoban"   +
					" ORDER BY epo.cg_razon_social"
				);
		}

		log.debug("getQueryDetallePemexGasolineros.query 	= <"+query.toString()+">");
		log.debug("getQueryDetallePemexGasolineros.estatus = <"+estatus+">");
		log.info("getQueryDetallePemexGasolineros(S)");

		return query.toString();

	}

	/**
	 * Devuelve el query que se usa para traer el detalle que se guardara en el Archivo de
	 * Transferencia Electronica de Fondos del modulo: DISPERSION PEMEX Gasolineros.
	 * PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - DISPERSION PEMEX - Gasolineros.
	 *
	 * @param estatus Cadena de texto con dos posibles valores: NEGOCIABLES, para documentos negociables
	 *                y PAGO_ANTICIPADO para documentos con pago anticipado.
	 * @param fechaSiguienteDiaHabil Cadena de texto con la fecha del dia habil siguiente. Formato DD-MM-YYYY
	 *
	 * @return Cadena de texto con el query solicitado.
	 *
	 */
	public String getQueryDetalleArchivoTEFPemexGasolineros(String estatus, String fechaSiguienteDiaHabil){

		log.info("getQueryDetalleArchivoTEFPemexGasolineros(E)");

		StringBuffer 	query 							= new StringBuffer();

		if(estatus == null) return query.toString();

		String 			sFechaSigHab 					= (fechaSiguienteDiaHabil == null)?"":fechaSiguienteDiaHabil;
		boolean 			esEstatusNegociable 			= (estatus.equals("NEGOCIABLES"))		?true:false;
		boolean 			esEstatusPagoAnticipado 	= (estatus.equals("PAGO_ANTICIPADO"))	?true:false;

		if(esEstatusNegociable){
			query.append(
					" SELECT   /*+index(d) use_nl(d n e c m)*/"   +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
					"           '"+sFechaSigHab+"' AS fechatrasf, '"+sFechaSigHab+"' AS fechaapli,"   +
					"           SUM (d.fn_monto) AS total_importe, e.cg_rfc, c.ic_bancos_tef,"   +
					"           c.ic_tipo_cuenta, c.cg_cuenta, 'TEF-NE' AS refrastreo,"   +
					"           'DISPEPO  ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo"   +
					"     FROM dis_documento d,"   +
					"          comrel_nafin n,"   +
					"          comcat_epo e,"   +
					"          com_cuentas c,"   +
					"          comcat_moneda m"   +
					"    WHERE d.ic_epo = e.ic_epo"   +
					"      AND c.ic_moneda = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND n.ic_epo_pyme_if = e.ic_epo"   +
					"      AND n.cg_tipo = 'E'"   +
					"      AND c.ic_tipo_cuenta = 40"   +
					"      AND c.cg_tipo_afiliado = 'E'"   +
					"      AND c.ic_producto_nafin = 4"   +
					"      AND d.ic_producto_nafin = 4"   +
					"      AND m.ic_moneda = d.ic_moneda "   +
					"      AND d.ic_moneda = ?"   +
					"      AND d.ic_epo = ?"   +
					"      AND TRUNC (d.df_fecha_venc) = TO_DATE (?, 'dd/mm/yyyy')"   +
					"      AND d.ic_estatus_docto = ?"   +
					" GROUP BY c.ic_cuenta,"   +
					"          e.ic_epo,"   +
					"          e.cg_rfc,"   +
					"          e.cg_razon_social,"   +
					"          m.cd_nombre,"   +
					"          c.ic_bancos_tef,"   +
					"          c.ic_tipo_cuenta,"   +
					"          c.cg_cuenta,"   +
					"          TO_CHAR (d.df_fecha_venc, 'yyyy-mm-dd')"
				);
		}
		if(esEstatusPagoAnticipado){
			query.append(
					" SELECT   /*+index(d) use_nl(d n e c m)*/"   +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,"   +
					"           '"+sFechaSigHab+"' AS fechatrasf, '"+sFechaSigHab+"' AS fechaapli,"   +
					"           SUM (d.fn_importe) AS total_importe, e.cg_rfc, c.ic_bancos_tef,"   +
					"           c.ic_tipo_cuenta, c.cg_cuenta, 'TEF-NE' AS refrastreo,"   +
					"           'DISPEPO  ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo"   +
					"     FROM dis_pagos_ant d,"   +
					"          comrel_nafin n,"   +
					"          comcat_epo e,"   +
					"          com_cuentas c,"   +
					"          comcat_moneda m"   +
					"    WHERE d.ic_epo = e.ic_epo"   +
					"      AND c.ic_moneda = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND n.ic_epo_pyme_if = e.ic_epo"   +
					"      AND n.cg_tipo = 'E'"   +
					"      AND c.ic_tipo_cuenta = 40"   +
					"      AND c.cg_tipo_afiliado = 'E'"   +
					"      AND c.ic_producto_nafin = 4"   +
					"      AND m.ic_moneda = d.ic_moneda "   +
					"      AND d.ic_moneda = ?"   +
					"      AND d.ic_epo = ?"   +
					"      AND TRUNC (d.df_fecha_aplicacion) = TO_DATE (?, 'dd/mm/yyyy')"   +
					" GROUP BY c.ic_cuenta,"   +
					"          e.ic_epo,"   +
					"          e.cg_rfc,"   +
					"          e.cg_razon_social,"   +
					"          m.cd_nombre,"   +
					"          c.ic_bancos_tef,"   +
					"          c.ic_tipo_cuenta,"   +
					"          c.cg_cuenta"
				);
		}

		log.debug("getQueryDetalleArchivoTEFPemexGasolineros.query 	 = <"+query.toString()+">");
		log.debug("getQueryDetalleArchivoTEFPemexGasolineros.estatus = <"+estatus+">");
		log.info("getQueryDetalleArchivoTEFPemexGasolineros(S)");

		return query.toString();

	}

	/**
	 *  Este este metodo se utiliza para consultar si una epo en particular posee el
	 *  servicio de dispersion habilitado
	 *  PANTALLA: 	EPO - ADMINISTRACION - DISPERSION EPOS - CONSULTA DISPERSION EPOS
	 *	 NOTA: 		NO UTILIZAR POR EL MOMENTO, YA QUE FALTAN CORRECCIONES AL DETALLADO ...
	 *
	 *  @param claveEpo Cadena de Texto con la clave de la Epo a Consultar
	 *
	 *  @return Un boolean, con valor <tt>true</tt> si la Epo en cuestion tiene habilitado
	 *          el servicio, <tt>false</tt> es caso contrario.
	 */
	public boolean hayServicioDeDispersionHabilitado(String claveEPO)
		throws NafinException {

		log.info("hayServicioDeDispersionHabilitado(E)");

		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		boolean			hayServicio		= false;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 																		 " +
					"	DECODE(CG_DISPERSION,'S','true','false')  AS HAY_SERVICIO " +
					"FROM 																		 " +
					"	COMREL_PRODUCTO_EPO 													 " +
					"WHERE 																		 " +
					"	IC_PRODUCTO_NAFIN   = ? AND 										 " +
					"	IC_EPO              = ? 											 ";
				lVarBind.add(new Integer(1));
				lVarBind.add(new Integer(claveEPO));
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					hayServicio = registros.getString("HAY_SERVICIO").equals("true")?true:false;
				}

		} catch(Exception e) {
			log.info("hayServicioDeDispersionHabilitado(Exception)");
			log.info("hayServicioDeDispersionHabilitado.qrySentencia  = <" + qrySentencia + ">");
			log.info("hayServicioDeDispersionHabilitado.claveEPO      = <" + claveEPO     + ">");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("hayServicioDeDispersionHabilitado(S)");
		}

		return hayServicio;
	}

	/**
	 * Este metodo devuelve el Query que trae el detalle de los documentos que fueron dispersados.
	 * PANTALLA: EPO - ADMINISTRACION - DISPERSION EPOS - CONSULTA DISPERSION EPOS
	 *
	 * @param claveEpo Cadena de texto con la clave de la Epo
	 * @param fechaVencimientoInicio Fecha de Vencimiento Inicial. Formato DD/MM/YYYY
	 * @param fechaVencimientoFinal Fecha de Vencimiento Final. Formato DD/MM/YYYY
	 *
	 * @return Cadena de texto con el query para realizar la consulta
	 */
	public String getQueryDetalleDocumentosDispersados(String claveEpo, String fechaVencimientoInicio, String fechaVencimientoFin){

		log.info("getQueryDetalleDocumentosDispersados(E)");

		StringBuffer	query	= new StringBuffer();
		StringBuffer 	cond 	= new StringBuffer();

		String 			icEPO	= (claveEpo 					== null)?"":claveEpo;
		String 	fechaVencDe	= (fechaVencimientoInicio 	== null)?"":fechaVencimientoInicio;
		String 	fechaVencA	= (fechaVencimientoFin 		== null)?"":fechaVencimientoFin;

		if(!fechaVencDe.equals("") && !fechaVencA.equals("")){
			cond.append(" and trunc(ff.df_fecha_venc) between trunc(to_date('"+fechaVencDe+"','dd/mm/yyyy')) and trunc(to_date('"+fechaVencA+"','dd/mm/yyyy')) ");
		}

		if(fechaVencDe.equals("") 	&& fechaVencA.equals("")){
			cond.append(" and trunc(ff.df_fecha_venc) = trunc(sysdate)");
		}

		/*	query = "select TO_CHAR(D.df_registro,'dd/mm/yyyy') as fechaVenc, P.cg_rfc, "+
		"P.cg_razon_social as nombrePyme, D.ig_total_documentos, M.cd_nombre, D.fn_importe, "+
		"C.ic_bancos_tef, C.ic_tipo_cuenta, C.cg_cuenta, ED.cd_descripcion as descDispersion, "+
		"ET.ic_estatus_tef, ET.cd_descripcion as descTEF, "+
		"EC.ic_estatus_cecoban_op, EC.cd_descripcion as descCecoban "+
		"from com_dispersion_epo D, comcat_epo E, comcat_pyme P, com_cuentas C, comcat_moneda M, "+
		"comcat_estatus_dispersion ED, comcat_estatus_tef ET, comcat_estatus_cecoban_op EC "+
		"where D.ic_epo = E.ic_epo "+
		"and D.ic_pyme = P.ic_pyme "+
		"and D.ic_cuenta = C.ic_cuenta "+
		"and C.ic_moneda = M.ic_moneda "+
		"and D.ic_estatus_dispersion = ED.ic_estatus_dispersion "+
		"and D.ic_estatus_tef = ET.ic_estatus_tef(+) "+
		"and D.ic_estatus_cecoban_op = EC.ic_estatus_cecoban_op(+) "+
		cond;*/
		/*
		"union all "+
		"select TO_CHAR(D.df_registro,'dd/mm/yyyy') as fechaVenc, P.cg_rfc, "+
		"P.cg_razon_social as nombrePyme, D.ig_total_documentos, 'MONEDA NACIONAL', D.fn_importe, "+
		"0 as ic_bancos_tef, 0 as ic_tipo_cuenta, '0' as cg_cuenta, ED.cd_descripcion as descDispersion, "+
		"ET.ic_estatus_tef, ET.cd_descripcion as descTEF, "+
		"EC.ic_estatus_cecoban_op, EC.cd_descripcion as descCecoban "+
		"from com_dispersion_epo D, comcat_epo E, comcat_pyme P, "+
		"comcat_estatus_dispersion ED, comcat_estatus_tef ET, comcat_estatus_cecoban_op EC "+
		"where D.ic_epo = E.ic_epo "+
		"and D.ic_pyme = P.ic_pyme "+
		"and NOT exists (select 1 from com_cuentas c "+
     	"				where D.ic_cuenta = c.ic_cuenta) "+
		"and D.ic_estatus_dispersion = ED.ic_estatus_dispersion "+
		"and D.ic_estatus_tef = ET.ic_estatus_tef(+) "+
		"and D.ic_estatus_cecoban_op = EC.ic_estatus_cecoban_op(+) "+
		cond;*/
		query.append(
			"SELECT  																			" +
			"      /*+ 																			" +
			"        INDEX (P CP_COMCAT_PYME_PK) 										" +
			"        INDEX (E CP_COMCAT_EPO_PK) 										" +
			"        INDEX (C CP_COM_CUENTAS_PK) 										" +
			"        INDEX (B CP_COMCAT_BANCOS_TEF_PK) 								" +
			"        INDEX (M CP_COMCAT_MONEDA_PK) 									" +
			"        INDEX (FF CP_INT_FLUJO_FONDOS_PK) 								" +
			"        INDEX (CFE CP_CFE_M_ENCABEZADO_PK) 								" +
			"        USE_NL (P E C B M FF) 												" +
			"       */  										   							" +
			"       	TO_CHAR (ff.df_fecha_venc, 'dd/mm/yyyy') AS fechavenc, 	" +
			"			p.cg_rfc, 																" +
			"       	p.cg_razon_social AS nombrepyme, 								" +
			"			ff.ig_total_documentos, 											" +
			"       	m.cd_nombre, 															" +
			"			ff.fn_importe, 														" +
			"			b.cd_descripcion as banco, 										" +
			"       	c.ic_tipo_cuenta, 													" +
			"			c.cg_cuenta, 															" +
			//"       	cfe.status_cen_i AS estatusflujo, 								" +
			"			cat.cd_descripcion AS estatusflujo,								" +
			"			ff.ic_flujo_fondos AS folio, 										" +
			"			ff.df_fecha_venc as fecha_venc, 									" +
			"			cif.cg_razon_social as nomif 										" +
			"FROM   																				" +
			"			comcat_pyme 		p, 												" +
			"       	comcat_epo 			e, 												" +
			"       	comcat_if 			cif, 												" +
			"       	com_cuentas 		c, 												" +
			"       	comcat_bancos_tef b, 												" +
			"       	comcat_moneda 		m, 												" +
			"       	int_flujo_fondos 	ff, 												" +
			"       	cfe_m_encabezado 	cfe, 												" +
			"			comcat_estatus_ff cat												" +
			"WHERE 																				" +
			"		e.ic_epo 				= ff.ic_epo 				AND				" +
			"   	p.ic_pyme(+) 			= ff.ic_pyme 				AND				" +
			"   	cif.ic_if(+) 			= ff.ic_if 					AND				" +
			"   	c.ic_cuenta 			= ff.ic_cuenta 			AND				" +
			"   	c.ic_bancos_tef 		= b.ic_bancos_tef 		AND				" +
			"   	c.ic_moneda 			= m.ic_moneda 				AND				" +
			"   	ff.ic_flujo_fondos 	= cfe.idoperacion_cen_i AND				" +
			"		cat.ic_estatus_ff		= cfe.status_cen_i		AND				" +
			"		ff.ic_epo				="+icEPO+" "+cond.toString() +" 			" +
			" ORDER BY fecha_venc, nombrepyme 											");

			log.debug("getQueryDetalleDocumentosDispersados.query = <"+query.toString()+">");
			log.debug("getQueryDetalleDocumentosDispersados.claveEpo						= <"+claveEpo+">");
			log.debug("getQueryDetalleDocumentosDispersados.fechaVencimientoInicio	= <"+fechaVencimientoInicio+">");
			log.debug("getQueryDetalleDocumentosDispersados.fechaVencimientoFin		= <"+fechaVencimientoFin+">");

			log.info("getQueryDetalleDocumentosDispersados(S)");
			return query.toString();
	}

	/**
	 * Este metodo devuelve el Query que trae el detalle de los documentos que no fueron dispersados.
	 * PANTALLA: EPO - ADMINISTRACION - DISPERSION EPOS - CONSULTA DISPERSION EPOS
	 *
	 * @param claveEpo Cadena de texto con la clave de la Epo
	 * @param fechaVencimientoInicio Fecha de Vencimiento Inicial. Formato DD/MM/YYYY
	 * @param fechaVencimientoFinal Fecha de Vencimiento Final. Formato DD/MM/YYYY
	 *
	 * @return Cadena de texto con el query para realizar la consulta
	 */
	public String getQueryDetalleDocumentosNoDispersados(String claveEpo, String fechaVencimientoInicio, String fechaVencimientoFin){

		log.info("getQueryDetalleDocumentosNoDispersados(E)");

		StringBuffer	query	= new StringBuffer();
		StringBuffer 	cond 	= new StringBuffer();

		String 			icEPO	= (claveEpo 					== null)?"":claveEpo;
		String 	fechaVencDe	= (fechaVencimientoInicio 	== null)?"":fechaVencimientoInicio;
		String 	fechaVencA	= (fechaVencimientoFin 		== null)?"":fechaVencimientoFin;

		if(!fechaVencDe.equals("") 	&& !fechaVencA.equals("")){
			cond.append(" and trunc(eff.df_fecha_venc) between trunc(to_date('"+fechaVencDe+"','dd/mm/yyyy')) and trunc(to_date('"+fechaVencA+"','dd/mm/yyyy'))");
		}
		if(fechaVencDe.equals("") 	&& fechaVencA.equals("")){
			cond.append(" and trunc(eff.df_fecha_venc) = trunc(sysdate)");
		}

		query.append(
				"SELECT  																			" +
				"      /*+ 																			" +
				"        INDEX (P CP_COMCAT_PYME_PK) 										" +
				"        INDEX (E CP_COMCAT_EPO_PK) 										" +
				"        INDEX (C CP_COM_CUENTAS_PK) 										" +
				"        INDEX (B CP_COMCAT_BANCOS_TEF_PK) 								" +
				"        INDEX (M CP_COMCAT_MONEDA_PK) 									" +
				"        INDEX (FF CP_INT_FLUJO_FONDOS_PK) 								" +
				"        INDEX (CFE CP_CFE_M_ENCABEZADO_PK) 								" +
				"        USE_NL (P E C B M FF) 												" +
				"       */  										   							" +
				"       	TO_CHAR (eff.df_fecha_venc, 'dd/mm/yyyy') AS fechavenc, 	" +
				"			p.cg_rfc, 																" +
				"       	p.cg_razon_social AS nombrepyme, 								" +
				"			eff.ig_total_documentos, 											" +
				"       	m.cd_nombre, 															" +
				"			eff.fn_importe, 														" +
				"			b.cd_descripcion as banco, 										" +
				"       	c.ic_tipo_cuenta, 													" +
				"			c.cg_cuenta, 															" +
				"			eff.df_fecha_venc as fecha_venc, 								" +
				"			cif.cg_razon_social as nomif 										" +
				"FROM   																				" +
				"			comcat_pyme 			p, 											" +
				"       	comcat_epo 				e, 											" +
				"       	comcat_if 				cif, 											" +
				"       	com_cuentas 			c, 											" +
				"       	comcat_bancos_tef 	b, 											" +
				"       	comcat_moneda 			m, 											" +
				"       	int_flujo_fondos_err eff, 											" +
				"       	cfe_m_encabezado		cfe 											" +
				"WHERE 																				" +
				"		e.ic_epo 				= eff.ic_epo 				AND				" +
				"   	p.ic_pyme(+) 			= eff.ic_pyme 				AND				" +
				"   	cif.ic_if(+) 			= eff.ic_if 				AND				" +
				"   	c.ic_cuenta 			= eff.ic_cuenta 			AND				" +
				"   	c.ic_bancos_tef 		= b.ic_bancos_tef 		AND				" +
				"   	c.ic_moneda 			= m.ic_moneda 				AND				" +
				"   	eff.ic_flujo_fondos 	= cfe.idoperacion_cen_i AND				" +
				"		eff.ic_epo				="+icEPO+" "+cond.toString() +" 			" +
				" ORDER BY fecha_venc, nombrepyme 											");

		log.debug("getQueryDetalleDocumentosNoDispersados.query = <"+query.toString()+">");
		log.debug("getQueryDetalleDocumentosNoDispersados.claveEpo						= <"+claveEpo+">");
		log.debug("getQueryDetalleDocumentosNoDispersados.fechaVencimientoInicio	= <"+fechaVencimientoInicio+">");
		log.debug("getQueryDetalleDocumentosNoDispersados.fechaVencimientoFin		= <"+fechaVencimientoFin+">");

		log.info("getQueryDetalleDocumentosNoDispersados(S)");
		return query.toString();

	}

	/**
	 * Devuelve el query que trae el detalle con los documentos que se dispersaran
	 * PANTALLA: NAFIN - ADMINISTRACION - DISPERSION - MONITOR
	 *
	 * @param numPymes Numero entero con el numero de pymes de la consulta.
	 * @return Cadena de texto con el query para realizar la consulta y adaptado para
	 *         usar variables bind.
	 */
	public String getQueryDetalleDocumentosADispersar(int numPymes){

		log.info("getQueryDetalleDocumentosADispersar(E)");
		StringBuffer qrySentencia = new StringBuffer();

		if( numPymes > 0 ) {

			qrySentencia.append(
				" SELECT /*+index(d) index(pym) use_nl(d pym)*/"   +
				"         pym.cg_razon_social, d.ig_numero_docto,"   +
				"         TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') df_fecha_docto,"   +
				"         TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') df_fecha_venc,"   +
				"         d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0) fn_monto, "   +
				"			 d.ic_moneda "  +
				"   FROM com_documento d, comcat_pyme pym"   +
				"  WHERE d.ic_pyme = pym.ic_pyme"   +
				"    AND (   (    d.cs_dscto_especial IN (?, ?)"   +
				"             AND d.fn_monto"   +
				"                 - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > ?"   +
				"            )"   +
				"         OR d.cs_dscto_especial IN (?, ?, ?)"   +
				"        )"   +
				"    AND d.ic_epo = ?"   +
				"    AND d.ic_estatus_docto IN (?, ?)"   +
				"    AND d.ic_moneda in (?,?) "   + // 1
				"    AND d.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"    AND d.df_fecha_venc < TO_DATE (?, 'dd/mm/yyyy') + ?"   +
				"    AND d.ic_pyme in (");

			for(int p=0;p<numPymes;p++) {
				qrySentencia.append("?,");
			}
			// Borrar ultima coma agregada
			qrySentencia.deleteCharAt(qrySentencia.length()-1);

			qrySentencia.append(
				" )"+
				" UNION"   +
				" SELECT /*+index(d) index(pym) use_nl(d pym)*/"   +
				"         pym.cg_razon_social, d.ig_numero_docto,"   +
				"         TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') df_fecha_docto,"   +
				"         TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') df_fecha_venc,"   +
				"           DECODE (d.cs_dscto_especial, 'C', -1, 1)"   +
				"         * (  d.fn_monto"   +
				"            - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)"   +
				"           ) fn_monto, "   +
				"			d.ic_moneda "  +
				"   FROM com_documento d, comcat_pyme pym"   +
				"  WHERE d.ic_pyme = pym.ic_pyme"  +
				"    AND (   (    d.cs_dscto_especial IN (?, ?)"   +
				"             AND d.fn_monto"   +
				"                 - (d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100) > ?"   +
				"            )"   +
				"         OR d.cs_dscto_especial IN (?, ?, ?, ?)"   +
				"        )"   +
				"    AND d.ic_epo = ?"  +
				"    AND d.ic_estatus_docto = ?"   +
				"    AND d.ic_moneda in (?,?) "   + // 1
				"    AND d.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"    AND d.df_fecha_venc < TO_DATE (?, 'dd/mm/yyyy') + ?"   +
				"    AND d.ic_pyme in (");

			for(int p=0;p<numPymes;p++) {
				qrySentencia.append("?,");
			}
			// Borrar ultima coma agregada
			qrySentencia.deleteCharAt(qrySentencia.length()-1);
			qrySentencia.append(")");

			log.debug("getQueryDetalleDocumentosADispersar.qrySentencia = <"+qrySentencia.toString()+">");
			log.debug("getQueryDetalleDocumentosADispersar.numPymes    = <"+numPymes+">");

		}

		log.info("getQueryDetalleDocumentosADispersar(S)");
		return qrySentencia.toString();

	}

	/**
	 *	Devuelve El Estatus de Dispersion: Operada para la EPO cuya clave se proporciona en
	 * el parametro <tt>ic_epo</tt>
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si esta habilitado. <tt>false</tt> en caso contrario.
	 */
	public boolean hasEstatusDispersionOperada(String ic_epo)
		throws AppException{

		log.info("hasEstatusDispersionOperada(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"		DECODE(CS_OPERADA,'S','true','false') AS ESTATUS_PARAMETRO, "  +
					"		'Dispersion::hasEstatusDispersionOperada()' "   +
					"FROM "  +
					"		com_parametros_dispersion "   +
					"WHERE 	ic_epo = ? ";

				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("ESTATUS_PARAMETRO").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("hasEstatusDispersionOperada(Exception)");
			log.debug("hasEstatusDispersionOperada.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al consultar estatus de dispersion operada.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("hasEstatusDispersionOperada(S)");
		}

		return resultado;
	}

	/**
	 * Habilita o deshabilita el Estatus de Dispersion: Operada
	 * @param ic_epo Clave de la Epo.
	 * @param estatus <tt>true</tt> para habilitar estatus. <tt>false</tt> para deshabilitar el estatus.
	 */
	public void setEstatusDispersionOperada(String ic_epo, boolean estatus)
			throws AppException {

		log.info("setEstatusDispersionOperada(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;
		int 					registros		= 0;

		try {

			con.conexionDB();

			qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setEstatusDispersionOperada()' "   +
					"   FROM com_parametros_dispersion "   +
					"  WHERE ic_epo = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			registros = con.existeDB(qrySentencia, lVarBind);
			if(registros==0) {
				qrySentencia =
						" INSERT INTO com_parametros_dispersion "   +
						"             (ic_epo, ic_pyme, cs_operada )"   +
						"      VALUES (?, ?, ?) "  ;
				lVarBind = new ArrayList();
				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(new Integer("-1"));
				lVarBind.add((estatus?"S":"N"));
				registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
			} else {
				qrySentencia =
					" UPDATE com_parametros_dispersion "   +
					"    SET CS_OPERADA = ?"   +
					"  WHERE  "   +
					"    ic_epo = ?" ;

				lVarBind = new ArrayList();
				lVarBind.add((estatus?"S":"N"));
				lVarBind.add(new Integer(ic_epo));

				con.ejecutaUpdateDB(qrySentencia, lVarBind);
			}
		} catch(Exception e){
			bOK = false;
			log.debug("setEstatusDispersionOperada(Exception)"+e);
			log.debug("setEstatusDispersionOperada.ic_epo  = <"+ic_epo+">");
			log.debug("setEstatusDispersionOperada.estatus = <"+estatus+">");
			e.printStackTrace();
			throw new AppException("Error al modificar Estatus de Dispersion: Operada");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setEstatusDispersionOperada(S)");
		}
	}

	/**
	 *	Devuelve El Estatus de Dispersion: Vencido Sin Operar para la EPO cuya clave se proporciona en
	 * el parametro <tt>ic_epo</tt>
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si esta habilitado. <tt>false</tt> en caso contrario.
	 */
	public boolean hasEstatusDispersionVencidoSinOperar(String ic_epo)
		throws AppException{

		log.info("hasEstatusDispersionVencidoSinOperar(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"		DECODE(CS_VENCIDO_SIN_OPERAR,'S','true','false') AS ESTATUS_PARAMETRO, "  +
					"		'Dispersion::hasEstatusDispersionVencidoSinOperar()' "   +
					"FROM "  +
					"		com_parametros_dispersion "   +
					"WHERE 	ic_epo = ? ";

				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("ESTATUS_PARAMETRO").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("hasEstatusDispersionVencidoSinOperar(Exception)");
			log.debug("hasEstatusDispersionVencidoSinOperar.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al consultar Estatus de Dispersion: Vencido sin Operar .");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("hasEstatusDispersionVencidoSinOperar(S)");
		}

		return resultado;
	}

	/**
	 * Habilita o deshabilita el Estatus de Dispersion: Vencido sin Operar
	 * @param ic_epo Clave de la Epo.
	 * @param estatus <tt>true</tt> para habilitar estatus. <tt>false</tt> para deshabilitar el estatus.
	 */
	public void setEstatusDispersionVencidoSinOperar(String ic_epo, boolean estatus)
			throws AppException {

		log.info("setEstatusDispersionVencidoSinOperar(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;
		int 					registros		= 0;

		try {

			con.conexionDB();

			qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setEstatusDispersionVencidoSinOperar()' "   +
					"   FROM com_parametros_dispersion "   +
					"  WHERE ic_epo = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			registros = con.existeDB(qrySentencia, lVarBind);
			if(registros==0) {
				qrySentencia =
						" INSERT INTO com_parametros_dispersion "   +
						"             (ic_epo, ic_pyme, cs_vencido_sin_operar )"   +
						"      VALUES (?, ?, ?) "  ;
				lVarBind = new ArrayList();
				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(new Integer("-1"));
				lVarBind.add((estatus?"S":"N"));
				registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
			} else {
				qrySentencia =
					" UPDATE com_parametros_dispersion "   +
					"    SET CS_VENCIDO_SIN_OPERAR = ?"   +
					"  WHERE  "   +
					"    ic_epo = ?" ;

				lVarBind = new ArrayList();
				lVarBind.add((estatus?"S":"N"));
				lVarBind.add(new Integer(ic_epo));

				con.ejecutaUpdateDB(qrySentencia, lVarBind);
			}
		} catch(Exception e){
			bOK = false;
			log.debug("setEstatusDispersionVencidoSinOperar(Exception)"+e);
			log.debug("setEstatusDispersionVencidoSinOperar.ic_epo  = <"+ic_epo+">");
			log.debug("setEstatusDispersionVencidoSinOperar.estatus = <"+estatus+">");
			e.printStackTrace();
			throw new AppException("Error al modificar Estatus de Dispersion: Vencido Sin Operar");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setEstatusDispersionVencidoSinOperar(S)");
		}
	}

	/**
	 *	Devuelve El Estatus de Dispersion: Operada Pagada para la EPO cuya clave se proporciona en
	 * el parametro <tt>ic_epo</tt>
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si esta habilitado. <tt>false</tt> en caso contrario.
	 */
	public boolean hasEstatusDispersionOperadaPagada(String ic_epo)
		throws AppException{

		log.info("hasEstatusDispersionOperadaPagada(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"		DECODE(CS_OPERADA_PAGADA,'S','true','false') AS ESTATUS_PARAMETRO, "  +
					"		'Dispersion::hasEstatusDispersionOperadaPagada()' "   +
					"FROM "  +
					"		com_parametros_dispersion "   +
					"WHERE 	ic_epo = ? ";

				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("ESTATUS_PARAMETRO").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("hasEstatusDispersionOperadaPagada(Exception)");
			log.debug("hasEstatusDispersionOperadaPagada.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al consultar Estatus de Dispersion: Operada Pagada.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("hasEstatusDispersionOperadaPagada(S)");
		}

		return resultado;
	}

	/**
	 * Habilita o deshabilita el Estatus de Dispersion: Operada Pagada
	 * @param ic_epo Clave de la Epo.
	 * @param estatus <tt>true</tt> para habilitar estatus. <tt>false</tt> para deshabilitar el estatus.
	 */
	public void setEstatusDispersionOperadaPagada(String ic_epo, boolean estatus)
			throws AppException {

		log.info("setEstatusDispersionOperadaPagada(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;
		int 					registros		= 0;

		try {

			con.conexionDB();

			qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setEstatusDispersionOperadaPagada()' "   +
					"   FROM com_parametros_dispersion "   +
					"  WHERE ic_epo = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			registros = con.existeDB(qrySentencia, lVarBind);
			if(registros==0) {
				qrySentencia =
						" INSERT INTO com_parametros_dispersion "   +
						"             (ic_epo, ic_pyme, cs_operada_pagada )"   +
						"      VALUES (?, ?, ?) "  ;
				lVarBind = new ArrayList();
				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(new Integer("-1"));
				lVarBind.add((estatus?"S":"N"));
				registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
			} else {
				qrySentencia =
					" UPDATE com_parametros_dispersion "   +
					"    SET CS_OPERADA_PAGADA = ?"   +
					"  WHERE  "   +
					"    ic_epo = ?" ;

				lVarBind = new ArrayList();
				lVarBind.add((estatus?"S":"N"));
				lVarBind.add(new Integer(ic_epo));

				con.ejecutaUpdateDB(qrySentencia, lVarBind);
			}

		} catch(Exception e){
			bOK = false;
			log.debug("setEstatusDispersionOperadaPagada(Exception)"+e);
			log.debug("setEstatusDispersionOperadaPagada.ic_epo  = <"+ic_epo+">");
			log.debug("setEstatusDispersionOperadaPagada.estatus = <"+estatus+">");
			e.printStackTrace();
			throw new AppException("Error al modificar Estatus de Dispersion: Operada Pagada");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setEstatusDispersionOperadaPagada(S)");
		}
	}

	/**
	 *	Devuelve El Estatus de Dispersion: Pagado Sin Operar para la EPO cuya clave se proporciona en
	 * el parametro <tt>ic_epo</tt>
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si esta habilitado. <tt>false</tt> en caso contrario.
	 */
	public boolean hasEstatusDispersionPagadoSinOperar(String ic_epo)
		throws AppException{

		log.info("hasEstatusDispersionPagadoSinOperar(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"		DECODE(CS_PAGADO_SIN_OPERAR,'S','true','false') AS ESTATUS_PARAMETRO, "  +
					"		'Dispersion::hasEstatusDispersionPagadoSinOperar()' "   +
					"FROM "  +
					"		com_parametros_dispersion "   +
					"WHERE 	ic_epo = ? ";

				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("ESTATUS_PARAMETRO").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("hasEstatusDispersionPagadoSinOperar(Exception)");
			log.debug("hasEstatusDispersionPagadoSinOperar.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al consultar Estatus de Dispersion:  Pagado Sin Operar.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("hasEstatusDispersionPagadoSinOperar(S)");
		}

		return resultado;
	}

	/**
	 * Habilita o deshabilita el Estatus de Dispersion: Pagado Sin Operar
	 * @param ic_epo Clave de la Epo.
	 * @param estatus <tt>true</tt> para habilitar estatus. <tt>false</tt> para deshabilitar el estatus.
	 */
	public void setEstatusDispersionPagadoSinOperar(String ic_epo, boolean estatus)
			throws AppException {

		log.info("setEstatusDispersionPagadoSinOperar(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;
		int 					registros		= 0;

		try {

			con.conexionDB();

			qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setEstatusDispersionPagadoSinOperar()' "   +
					"   FROM com_parametros_dispersion "   +
					"  WHERE ic_epo = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			registros = con.existeDB(qrySentencia, lVarBind);
			if(registros==0) {
				qrySentencia =
						" INSERT INTO com_parametros_dispersion "   +
						"             (ic_epo, ic_pyme, cs_pagado_sin_operar )"   +
						"      VALUES (?, ?, ?) "  ;
				lVarBind = new ArrayList();
				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(new Integer("-1"));
				lVarBind.add((estatus?"S":"N"));
				registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
			} else {
				qrySentencia =
					" UPDATE com_parametros_dispersion "   +
					"    SET CS_PAGADO_SIN_OPERAR = ?"   +
					"  WHERE  "   +
					"    ic_epo = ?" ;

				lVarBind = new ArrayList();
				lVarBind.add((estatus?"S":"N"));
				lVarBind.add(new Integer(ic_epo));

				con.ejecutaUpdateDB(qrySentencia, lVarBind);
			}
		} catch(Exception e){
			bOK = false;
			log.debug("setEstatusDispersionPagadoSinOperar(Exception)"+e);
			log.debug("setEstatusDispersionPagadoSinOperar.ic_epo  = <"+ic_epo+">");
			log.debug("setEstatusDispersionPagadoSinOperar.estatus = <"+estatus+">");
			e.printStackTrace();
			throw new AppException("Error al modificar Estatus de Dispersion: Pagado Sin Operar.");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setEstatusDispersionPagadoSinOperar(S)");
		}
	}

	/**
	 *	Devuelve si esta habilitado o no la Tarifa de Dispersion en Moneda Nacional para la EPO cuya clave se proporciona en
	 * el parametro <tt>ic_epo</tt>
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si esta habilitado. <tt>false</tt> en caso contrario.
	 */
	public boolean hasTarifaDispersionMonedaNacional(String ic_epo)
		throws AppException{

		log.info("hasTarifaDispersionMonedaNacional(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"		DECODE(CS_DISPERSION_MN,'S','true','false') AS ESTATUS_PARAMETRO, "  +
					"		'Dispersion::hasTarifaDispersionMonedaNacional()' "   +
					"FROM "  +
					"		com_parametros_dispersion "   +
					"WHERE 	ic_epo = ? ";

				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("ESTATUS_PARAMETRO").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("hasTarifaDispersionMonedaNacional(Exception)");
			log.debug("hasTarifaDispersionMonedaNacional.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al consultar si las Tarifas de Dispersion en Moneda Nacional se encuentran habilitadas");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("hasTarifaDispersionMonedaNacional(S)");
		}

		return resultado;
	}

	/**
	 * Habilita o deshabilita las Tarifas de Dispersion en Moneda Nacional.
	 * @param ic_epo Clave de la Epo.
	 * @param estatus <tt>true</tt> para habilitar estatus. <tt>false</tt> para deshabilitar el estatus.
	 */
	public void setEstatusTarifaDispersionMonedaNacional(String ic_epo, boolean estatus)
			throws AppException {

		log.info("setEstatusTarifaDispersionMonedaNacional(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;
		int 					registros		= 0;

		try {

			con.conexionDB();

			qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setEstatusTarifaDispersionMonedaNacional()' "   +
					"   FROM com_parametros_dispersion "   +
					"  WHERE ic_epo = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			registros = con.existeDB(qrySentencia, lVarBind);
			if(registros==0) {
					qrySentencia =
						" INSERT INTO com_parametros_dispersion "   +
						"             (ic_epo, ic_pyme, CS_DISPERSION_MN )"   +
						"      VALUES (?, ?, ?) "  ;
					lVarBind = new ArrayList();
					lVarBind.add(new Integer(ic_epo));
					lVarBind.add(new Integer("-1"));
					lVarBind.add((estatus?"S":"N"));
					registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
			} else {

			qrySentencia =
				" UPDATE com_parametros_dispersion "   +
				"    SET CS_DISPERSION_MN = ?"   +
				"  WHERE  "   +
				"    ic_epo = ?" ;

			lVarBind = new ArrayList();
			lVarBind.add((estatus?"S":"N"));
			lVarBind.add(new Integer(ic_epo));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);
			}
		} catch(Exception e){
			bOK = false;
			log.debug("setEstatusTarifaDispersionMonedaNacional(Exception)"+e);
			log.debug("setEstatusTarifaDispersionMonedaNacional.ic_epo  = <"+ic_epo+">");
			log.debug("setEstatusTarifaDispersionMonedaNacional.estatus = <"+estatus+">");
			e.printStackTrace();
			throw new AppException("Error al modificar Estatus de las Tarifas de Dispersion en Moneda Nacional");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setEstatusTarifaDispersionMonedaNacional(S)");
		}
	}

	/**
	 *	Devuelve si esta habilitado o no la Tarifa de Dispersion en Dolares Americanos para la EPO cuya clave se proporciona en
	 * el parametro <tt>ic_epo</tt>
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>boolean</tt>. <tt>true</tt> si esta habilitado. <tt>false</tt> en caso contrario.
	 */
	public boolean hasTarifaDispersionDolaresAmericanos(String ic_epo)
		throws AppException{

		log.info("hasTarifaDispersionDolaresAmericanos(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"		DECODE(CS_DISPERSION_USD,'S','true','false') AS ESTATUS_PARAMETRO, "  +
					"		'Dispersion::hasTarifaDispersionDolaresAmericanos()' "   +
					"FROM "  +
					"		com_parametros_dispersion "   +
					"WHERE 	ic_epo = ? ";

				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("ESTATUS_PARAMETRO").equals("true")?true:false;
				}

		}catch(Exception e){
			log.debug("hasTarifaDispersionDolaresAmericanos(Exception)");
			log.debug("hasTarifaDispersionDolaresAmericanos.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al consultar si las Tarifas de Dispersion en Dolares Americanos se encuentran habilitadas");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("hasTarifaDispersionDolaresAmericanos(S)");
		}

		return resultado;
	}

	/**
	 * Habilita o deshabilita las Tarifas de Dispersion en Dolares Americanos.
	 * @param ic_epo Clave de la Epo.
	 * @param estatus <tt>true</tt> para habilitar estatus. <tt>false</tt> para deshabilitar el estatus.
	 */
	public void setEstatusTarifaDispersionDolaresAmericanos(String ic_epo, boolean estatus)
			throws AppException {

		log.info("setEstatusTarifaDispersionDolaresAmericanos(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;
		int 					registros		= 0;

		try {

			con.conexionDB();

			qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setEstatusTarifaDispersionDolaresAmericanos()' "   +
					"   FROM com_parametros_dispersion "   +
					"  WHERE ic_epo = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			registros = con.existeDB(qrySentencia, lVarBind);
			if(registros==0) {
					qrySentencia =
						" INSERT INTO com_parametros_dispersion "   +
						"             (ic_epo, ic_pyme, cs_dispersion_usd )"   +
						"      VALUES (?, ?, ?) "  ;
					lVarBind = new ArrayList();
					lVarBind.add(new Integer(ic_epo));
					lVarBind.add(new Integer("-1"));
					lVarBind.add((estatus?"S":"N"));
					registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
			} else {

			qrySentencia =
				" UPDATE com_parametros_dispersion "   +
				"    SET CS_DISPERSION_USD = ?"   +
				"  WHERE  "   +
				"    ic_epo = ?" ;

			lVarBind = new ArrayList();
			lVarBind.add((estatus?"S":"N"));
			lVarBind.add(new Integer(ic_epo));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);
			}
		} catch(Exception e){
			bOK = false;
			log.debug("setEstatusTarifaDispersionDolaresAmericanos(Exception)"+e);
			log.debug("setEstatusTarifaDispersionDolaresAmericanos.ic_epo  = <"+ic_epo+">");
			log.debug("setEstatusTarifaDispersionDolaresAmericanos.estatus = <"+estatus+">");
			e.printStackTrace();
			throw new AppException("Error al modificar Estatus de las Tarifas de Dispersion en Dolares Americanos.");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setEstatusTarifaDispersionDolaresAmericanos(S)");
		}
	}

	/**
	 *	Devuelve el tipo de archivo que se enviara el en correo de disperssion.
	 * @param ic_epo Clave de la EPO.
	 * @return String con la clave del tipo de archivo. Valores posibles: "A" = Ambos,
	 *         "P" = PDF y "E" = Excel.
	 */
	public String getTipoArchivoAEnviar(String ic_epo)
		throws AppException{

		log.info("getTipoArchivoAEnviar(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"		CG_ARCHIVO_ENVIAR AS ARCHIVO_A_ENVIAR, "  +
					"		'Dispersion::getTipoArchivoAEnviar()' "   +
					"FROM "  +
					"		com_parametros_dispersion "   +
					"WHERE 	ic_epo = ? ";

				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("ARCHIVO_A_ENVIAR");
				}

		}catch(Exception e){
			log.debug("getTipoArchivoAEnviar(Exception)");
			log.debug("getTipoArchivoAEnviar.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al obtener el tipo de archivo a enviar.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getTipoArchivoAEnviar(S)");
		}

		return resultado;
	}

	/**
	 * Especifica el Tipo de Archivo que se enviara en el correo de dispersion.
	 * @param ic_epo Clave de la Epo.
	 * @param tipoArchivo Cadena de texto de 1 caracter donde se especifica el tipo de archivo que se enviara.
	 * 						 Valores posibles: "A" = Ambos, "P" = PDF y "E" = Excel.
	 */
	public void setTipoArchivoAEnviar(String ic_epo, String tipoArchivo)
			throws AppException {

		log.info("setTipoArchivoAEnviar(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;
		int 					registros		= 0;

		try {

			con.conexionDB();

			qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setTipoArchivoAEnviar()' "   +
					"   FROM com_parametros_dispersion "   +
					"  WHERE ic_epo = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			registros = con.existeDB(qrySentencia, lVarBind);
			if(registros==0) {
					qrySentencia =
						" INSERT INTO com_parametros_dispersion "   +
						"             (ic_epo, ic_pyme, cg_archivo_enviar )"   +
						"      VALUES (?, ?, ?) "  ;
					lVarBind = new ArrayList();
					lVarBind.add(new Integer(ic_epo));
					lVarBind.add(new Integer("-1"));
					lVarBind.add(tipoArchivo);
					registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
			} else {

			qrySentencia =
				" UPDATE com_parametros_dispersion "   +
				"    SET CG_ARCHIVO_ENVIAR = ?"   +
				"  WHERE  "   +
				"    ic_epo = ?" ;

			lVarBind = new ArrayList();
			lVarBind.add(tipoArchivo);
			lVarBind.add(new Integer(ic_epo));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);
			}
		} catch(Exception e){
			bOK = false;
			log.debug("setTipoArchivoAEnviar(Exception)"+e);
			log.debug("setTipoArchivoAEnviar.ic_epo      = <"+ic_epo+">");
			log.debug("setTipoArchivoAEnviar.tipoArchivo = <"+tipoArchivo+">");
			e.printStackTrace();
			throw new AppException("Error al especificar el Tipo de Archivo a enviar");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setTipoArchivoAEnviar(S)");
		}
	}


	/**
	 *	Devuelve el Nombre del Proveedor. - Pantalla de NAFIN - Administracion - Dispersion - Parametrizacion
	 * @param ic_epo Clave de la EPO.
	 * @return String con el Nombre del Proveedor
	 */
	public String getNombreProveedor(String ic_epo)
		throws AppException{

		log.info("getNombreProveedor(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"  /*+ index(P CP_COMCAT_PYME_PK) */ "  +
					"  P.cg_razon_social as NOMBRE_PROVEEDOR "  +
					"FROM  "  +
					"  comcat_pyme P,  "  +
					"  com_parametros_dispersion param "  +
					"WHERE  "  +
					"  P.ic_pyme             = param.ic_pyme "  +
					"  and param.ic_epo      =   ? "  +
					"  AND P.cs_habilitado   =   ? "  +
					"  AND P.cs_invalido     !=  ? "  +
					"GROUP BY P.ic_pyme, P.cg_razon_social ";

				lVarBind.add(new Integer(ic_epo));
				lVarBind.add("S");
				lVarBind.add("S");

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("NOMBRE_PROVEEDOR");
				}

		}catch(Exception e){
			log.debug("getNombreProveedor(Exception)");
			log.debug("getNombreProveedor.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al obtener el Nombre del Proveedor.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getNombreProveedor(S)");
		}

		return resultado;
	}

	/**
	 *	Devuelve la Clave del Proveedor. - Pantalla de NAFIN - Administracion - Dispersion - Parametrizacion
	 * @param ic_epo Clave de la EPO.
	 * @return String con la Clave del Proveedor(ic_pyme)
	 */
	public String getClaveProveedor(String ic_epo)
		throws AppException{

		log.info("getClaveProveedor(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"  param.ic_pyme as CLAVE "  +
					"FROM "  +
					"  com_parametros_dispersion param "  +
					"WHERE "  +
					"  param.ic_epo   =   ? ";

				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("CLAVE");
				}

		}catch(Exception e){
			log.debug("getClaveProveedor(Exception)");
			log.debug("getClaveProveedor.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al obtener la Clave del Proveedor.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getClaveProveedor(S)");
		}

		return resultado;
	}

	/**
	 *	Devuelve la Clave del Proveedor usando el Nombre Proporcionado por el Usuario -
	 * Pantalla de NAFIN - Administracion - Dispersion - Parametrizacion
	 * @param nombreProveedor El Nombre del Proveedor.
	 * @param ic_epo La Clave de la EPO.
	 * @return String con la Clave del Proveedor(ic_pyme)
	 */
	public String getClaveProveedorByName(String nombreProveedor, String ic_epo)
		throws AppException{

		log.info("getClaveProveedorByName(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";

			if(nombreProveedor == null || nombreProveedor.trim().equals("")){
				log.info("getClaveProveedorByName(S)");
				return "";
			}
			if(ic_epo == null || ic_epo.trim().equals("")){
				log.info("getClaveProveedorByName(S)");
				return "";
			}

			try {
				con.conexionDB();
				qrySentencia =
					" SELECT "  +
					"  /*+ ordered use_nl(P) */ "  +
					"  P.ic_pyme as CLAVE"  +
					"   FROM comrel_pyme_epo PE, "  +
					"  comcat_pyme P "  +
					"  WHERE P.ic_pyme    =PE.ic_pyme "  +
					"AND PE.ic_epo        = ? "  +
					"AND PE.cs_habilitado = ? "  +
					"AND P.cs_habilitado  = ? "  +
					"AND P.cs_invalido   != ? "  +
					"AND P.cg_razon_social = ? "  +
					"GROUP BY P.ic_pyme, "  +
					"  P.cg_razon_social "  +
					"ORDER BY P.cg_razon_social " ;

				lVarBind.add(new Integer(ic_epo));
				lVarBind.add("S");
				lVarBind.add("S");
				lVarBind.add("S");
				lVarBind.add(nombreProveedor);

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("CLAVE");
				}

		}catch(Exception e){
			log.debug("getClaveProveedorByName(Exception)");
			log.debug("getClaveProveedorByName.ic_epo          = <"+ic_epo+">");
			log.debug("getClaveProveedorByName.nombreProveedor = <"+nombreProveedor+">");
			e.printStackTrace();
			throw new AppException("Error al obtener la Clave del Proveedor.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getClaveProveedorByName(S)");
		}

		return resultado;
	}

	/**
	 * Devuelve la descripcion de la moneda cuya clave se proporciona en la variable <tt>clave</tt>.
	 * @param clave Clave de la Moneda.
	 * @return String con el Nombre de la Moneda.
	 */
	public String getDescripcionMoneda(String clave)
		throws AppException{

		log.info("getDescripcionMoneda(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer 	qrySentencia	= null;
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();

		if(clave == null || clave.trim().equals("")){
			log.info("getDescripcionMoneda(S)");
			return "";
		}

		if(monedas == null ) monedas = new HashMap();

		String descripcion = (String)monedas.get(clave);

		if(descripcion == null){
			try {
					con.conexionDB();
					qrySentencia = new StringBuffer();
					qrySentencia.append(
						" SELECT "  +
						"	 /*+ index (C CP_COMCAT_MONEDA_PK)*/ "  +
						"   C.CD_NOMBRE AS DESCRIPCION "  +
						" FROM "  +
						"	 COMCAT_MONEDA C " +
						" WHERE "  +
						"	IC_MONEDA = ? "
						);

					lVarBind.add(new Integer(clave));

					registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
					if(registros != null && registros.next()){
						descripcion = registros.getString("DESCRIPCION");
						descripcion = (descripcion != null && descripcion.trim().equals(""))?"":descripcion;
					}

			}catch(Exception e){
				log.debug("getDescripcionMoneda(Exception)");
				log.debug("getDescripcionMoneda.clave = <"+clave+">");
				e.printStackTrace();
				throw new AppException("Error al obtener la Descripcion de la Moneda.");
			}finally{
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
			}

			if(!descripcion.equals("")){
				monedas.put(clave,descripcion);
			}

		}

		log.info("getDescripcionMoneda(S)");
		return (String) monedas.get(clave);

	}

	/**
	 *  Cambia el estatus de la cuenta a 99 (Cuenta Correcta)
	 *  @param icCuenta Clave de la Cuenta a Validar.
	 *  @param parametrosAdicionales Parametros adicionales para registro en bitacora
	 *                               de la operacion.
	 */
	 public void validarCuenta(String icCuenta, HashMap parametrosAdicionales)
	 	throws AppException{

		log.info("validarCuenta(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;

		StringBuffer		queryConsulta 	= new StringBuffer();
		PreparedStatement ps				  	= null;
		ResultSet			rs					= null;
		String strLogin					= (String) parametrosAdicionales.get("STR_LOGIN");

		try {
			AutorizacionDescuento BeanAutDescuento = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			//getOperaFideicomisoEpo
			String epo="";
			String icIF="";
			String icMoneda="";
			String rfcPyme="";
			String ic_pyme ="";
			con.conexionDB();

			lVarBind = new ArrayList();
			lVarBind.add(icCuenta);

			Registros miReg = con.consultarDB("SELECT ic_epo,ic_moneda FROM com_cuentas WHERE ic_cuenta = ? ",lVarBind,false);


			if(miReg.next()){
				epo = miReg.getString("IC_EPO");
				icMoneda = miReg.getString("IC_MONEDA");
				//BeanParamDscto
			}
			String qryIf = "SELECT   I.ic_if " +
								"    FROM comrel_if_epo relifepo, " +
								"         comrel_producto_epo cpe, " +
								"         comcat_epo epo, " +
								"         comcat_if I, " +
								"         comcat_producto_nafin pn " +
								"   WHERE epo.ic_epo = relifepo.ic_epo " +
								"     AND epo.ic_epo = cpe.ic_epo " +
								"     AND cpe.ic_producto_nafin = pn.ic_producto_nafin " +
								"     AND I.ic_if = relifepo.ic_if " +
								"     AND epo.cs_habilitado = ? " +
								"     AND pn.ic_producto_nafin = ? " +
								"     AND epo.ic_epo = ? " +
								"    AND i.CS_OPERA_FIDEICOMISO = ? ";
			lVarBind = new ArrayList();
			lVarBind.add("S");
			lVarBind.add("1");
			lVarBind.add(epo);
			lVarBind.add("S");

			if(!epo.equals("")){
				 miReg = con.consultarDB(qryIf,lVarBind,false);
				if(miReg.next()){
					icIF = miReg.getString("IC_IF");
				}
			}


			ParametrosDescuentoBean param  = new ParametrosDescuentoBean();

			if(!icIF.equals("")&&!epo.equals("")&&icMoneda.equals("1")  )   {
				if(BeanAutDescuento.getOperaIFEPOFISO(icIF,epo) ){
					String qryRfcPyme="select cp.cg_rfc,  cp.ic_pyme   from comrel_nafin cn, comcat_pyme cp, com_cuentas cc " +
											"    where ic_cuenta = ? " +
											"    and cn.IC_NAFIN_ELECTRONICO = cc.IC_NAFIN_ELECTRONICO " +
											"    and cn.CG_TIPO = 'P' " +
											"    and cn.IC_EPO_PYME_IF= cp.IC_PYME ";
					lVarBind = new ArrayList();
					lVarBind.add(icCuenta);
					 miReg = con.consultarDB(qryRfcPyme,lVarBind,false);
					 if(miReg.next()){
						rfcPyme = miReg.getString("CG_RFC");
						ic_pyme = miReg.getString("ic_pyme");

					 }
					 String operaFide_Pyme =   param.getOperaFideicomisoPYME( ic_pyme ); //Fodea 019-2014

					 if(operaFide_Pyme.equals("S") )  {

						CuentasPymeIFWS  cuentaConfirmar = new CuentasPymeIFWS();
						cuentaConfirmar.setRfcPyme(rfcPyme);
						cuentaConfirmar.setClaveEpo(epo);
						cuentaConfirmar.setClaveMoneda(icMoneda);
						BeanAutDescuento.confirmacionCuentasIFWS ( icIF, strLogin,new CuentasPymeIFWS[]{ cuentaConfirmar});

					 }
				}
			}

			qrySentencia =
				" UPDATE com_cuentas "   +
				"    SET ic_estatus_cecoban = ?, "   +
				"			df_ultima_mod 		 = SYSDATE "  +
				"  WHERE  "   +
				"    ic_cuenta = ?" ;

			lVarBind = new ArrayList();
			lVarBind.add(new Integer("99"));
			lVarBind.add(new Integer(icCuenta));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);

			// Consultar campos adicionales
			queryConsulta.append(
				"SELECT                  "  +
				"	CG_CUENTA,            "  +
				"	IC_PRODUCTO_NAFIN,    "  +
				"	IC_MONEDA,            "  +
				"	IC_BANCOS_TEF,        "  +
				"	IC_TIPO_CUENTA,       "  +
				"	CG_PLAZA_SWIFT,       "  +
				"	CG_SUCURSAL_SWIFT,    "  +
				"	IC_NAFIN_ELECTRONICO, "  +
				"	CG_TIPO_AFILIADO      "  +
				"FROM                    "  +
				"  COM_CUENTAS           "  +
				"WHERE                   "  +
				"	IC_CUENTA = ?         "
			);
			ps = con.queryPrecompilado(queryConsulta.toString());
			ps.setBigDecimal(1,new BigDecimal(icCuenta));
			rs = ps.executeQuery();

			String cg_tipo_afiliado			= null;
			String cg_cuenta					= null;
			String ic_producto_nafin		= null;
			String ic_moneda					= null;
			String ic_bancos_tef				= null;
			String ic_tipo_cuenta			= null;
			String cg_plaza_swift			= null;
			String cg_sucursal_swift		= null;
			String ic_nafin_electronico	= null;

			if(rs.next()){

				cg_tipo_afiliado		= rs.getString("cg_tipo_afiliado"		);
				cg_cuenta				= rs.getString("cg_cuenta"					);
				ic_producto_nafin		= rs.getString("ic_producto_nafin"		);
				ic_moneda				= rs.getString("ic_moneda"					);
				ic_bancos_tef			= rs.getString("ic_bancos_tef"			);
				ic_tipo_cuenta			= rs.getString("ic_tipo_cuenta"			);
				cg_plaza_swift			= rs.getString("cg_plaza_swift"			);
				cg_sucursal_swift		= rs.getString("cg_sucursal_swift"		);
				ic_nafin_electronico	= rs.getString("ic_nafin_electronico"	);

			}

			cg_tipo_afiliado		= cg_tipo_afiliado     == null?"":cg_tipo_afiliado;
			cg_cuenta				= cg_cuenta            == null?"":cg_cuenta;
			ic_producto_nafin		= ic_producto_nafin    == null?"":ic_producto_nafin;
			ic_moneda				= ic_moneda            == null?"":ic_moneda;
			ic_bancos_tef			= ic_bancos_tef        == null?"":ic_bancos_tef;
			ic_tipo_cuenta			= ic_tipo_cuenta       == null?"":ic_tipo_cuenta;
			cg_plaza_swift			= cg_plaza_swift       == null?"":cg_plaza_swift;
			cg_sucursal_swift		= cg_sucursal_swift    == null?"":cg_sucursal_swift;
			ic_nafin_electronico	= ic_nafin_electronico == null?"":ic_nafin_electronico;

			// Registrar en BITACORA la VALIDACION para cuando la Clave del Afiliado al que se le di�
			// de alta la cuenta sea: P(PYME), D(DISTRIBUIDOR),I(IF Bancario/No Bancario) y B(IF/Beneficiario)
			if(
				"P".equals(cg_tipo_afiliado) ||
				"D".equals(cg_tipo_afiliado) ||
				"I".equals(cg_tipo_afiliado) ||
				"B".equals(cg_tipo_afiliado)
			){

				parametrosAdicionales 	= parametrosAdicionales == null?new HashMap():parametrosAdicionales;

				// Registrar la informacion registrada para la cuenta
				String decripcionValidacion 	= (String) parametrosAdicionales.get("DESCRIPCION_VALIDACION");
				decripcionValidacion 			= decripcionValidacion == null?"CUENTA CORRECTA\nNO DISPONIBLE":decripcionValidacion;
				String clavePantalla  			= (String) parametrosAdicionales.get("PANTALLA_ORIGEN");
				clavePantalla 						= clavePantalla  == null?"CONSCTAS"						  :clavePantalla;
				String ic_usuario					= (String) parametrosAdicionales.get("IC_USUARIO");

				//boolean esCuentaCLABE = "40".equals(ic_tipo_cuenta)?true:false;
				boolean esCuentaSWIFT = "50".equals(ic_tipo_cuenta)?true:false;

				String valoresAnteriores =
					"cg_estatus=SIN VALIDAR\n" +
					"cg_cuenta="+cg_cuenta+"\n" +
					"ic_producto_nafin="+ic_producto_nafin+"\n" +
					"ic_moneda="+ic_moneda+"\n" +
					"ic_bancos_tef="+ic_bancos_tef+"\n" +
					"ic_tipo_cuenta="+ic_tipo_cuenta+"\n" +
					"cg_plaza_swift="+(esCuentaSWIFT?cg_plaza_swift:"N/A")+"\n" +
					"cg_sucursal_swift="+(esCuentaSWIFT?cg_sucursal_swift:"N/A");

				String valoresActuales =
					decripcionValidacion + "\n" +
					"cg_cuenta="+cg_cuenta+"\n"  +
					"ic_producto_nafin="+ic_producto_nafin+"\n"  +
					"ic_moneda ="+ic_moneda+"\n"  +
					"ic_bancos_tef="+ic_bancos_tef+"\n"  +
					"ic_tipo_cuenta="+ic_tipo_cuenta+"\n"  +
					"cg_plaza_swift="+(esCuentaSWIFT?cg_plaza_swift:"N/A")+"\n"  +
					"cg_sucursal_swift="+(esCuentaSWIFT?cg_sucursal_swift:"N/A");

				Bitacora.grabarEnBitacora(
						con,
						clavePantalla,															//clavePantalla,
						"B".equals(cg_tipo_afiliado)?"I":cg_tipo_afiliado,			//tipoAfiliado; Si el tipo es "B" usar "I"
						ic_nafin_electronico, 												//claveNafinElectronico,
						ic_usuario,																//claveUsuario,
						valoresAnteriores,													//datoAnterior,
						valoresActuales                                          //datoActual
				);

			}

		} catch(Exception e){
			bOK = false;
			log.debug("validarCuenta(Exception)"+e);
			log.debug("validarCuenta.icCuenta  = <"+icCuenta+">");
			e.printStackTrace();
			throw new AppException("Error al validar cuenta.");
		} finally {

			if(rs != null) try { rs.close(); }catch(Exception e){}
			if(ps != null) try { ps.close(); }catch(Exception e){}

			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("validarCuenta(S)");
		}

	 }


	/**
	 *  Cambia el estatus de la cuenta a <tt>null</tt> (Cuenta Invalida)
	 *  @param icCuenta Clave de la Cuenta a Validar.
	 *  @param parametrosAdicionales Parametros adicionales para registro en bitacora
	 *                               de la operacion.
	 */
	 public void invalidarCuenta(String icCuenta, HashMap parametrosAdicionales)
	 	throws AppException{

		log.info("invalidarCuenta(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;

		StringBuffer		queryConsulta 	= new StringBuffer();
		PreparedStatement ps				  	= null;
		ResultSet			rs					= null;

		try {
			con.conexionDB();
			qrySentencia =
				" UPDATE com_cuentas "   +
				"    SET ic_estatus_cecoban = null, "   +
				"			df_ultima_mod 		 = SYSDATE "  +
				"  WHERE  "   +
				"    ic_cuenta = ?" ;

			lVarBind = new ArrayList();
			lVarBind.add(new Integer(icCuenta));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);

			// Consultar campos adicionales
			queryConsulta.append(
				"SELECT                  "  +
				"	CG_CUENTA,            "  +
				"	IC_PRODUCTO_NAFIN,    "  +
				"	IC_MONEDA,            "  +
				"	IC_BANCOS_TEF,        "  +
				"	IC_TIPO_CUENTA,       "  +
				"	CG_PLAZA_SWIFT,       "  +
				"	CG_SUCURSAL_SWIFT,    "  +
				"	IC_NAFIN_ELECTRONICO, "  +
				"	CG_TIPO_AFILIADO      "  +
				"FROM                    "  +
				"  COM_CUENTAS           "  +
				"WHERE                   "  +
				"	IC_CUENTA = ?         "
			);
			ps = con.queryPrecompilado(queryConsulta.toString());
			ps.setBigDecimal(1,new BigDecimal(icCuenta));
			rs = ps.executeQuery();

			String cg_tipo_afiliado			= null;
			String cg_cuenta					= null;
			String ic_producto_nafin		= null;
			String ic_moneda					= null;
			String ic_bancos_tef				= null;
			String ic_tipo_cuenta			= null;
			String cg_plaza_swift			= null;
			String cg_sucursal_swift		= null;
			String ic_nafin_electronico	= null;

			if(rs.next()){

				cg_tipo_afiliado		= rs.getString("cg_tipo_afiliado"		);
				cg_cuenta				= rs.getString("cg_cuenta"					);
				ic_producto_nafin		= rs.getString("ic_producto_nafin"		);
				ic_moneda				= rs.getString("ic_moneda"					);
				ic_bancos_tef			= rs.getString("ic_bancos_tef"			);
				ic_tipo_cuenta			= rs.getString("ic_tipo_cuenta"			);
				cg_plaza_swift			= rs.getString("cg_plaza_swift"			);
				cg_sucursal_swift		= rs.getString("cg_sucursal_swift"		);
				ic_nafin_electronico	= rs.getString("ic_nafin_electronico"	);

			}

			cg_tipo_afiliado		= cg_tipo_afiliado     == null?"":cg_tipo_afiliado;
			cg_cuenta				= cg_cuenta            == null?"":cg_cuenta;
			ic_producto_nafin		= ic_producto_nafin    == null?"":ic_producto_nafin;
			ic_moneda				= ic_moneda            == null?"":ic_moneda;
			ic_bancos_tef			= ic_bancos_tef        == null?"":ic_bancos_tef;
			ic_tipo_cuenta			= ic_tipo_cuenta       == null?"":ic_tipo_cuenta;
			cg_plaza_swift			= cg_plaza_swift       == null?"":cg_plaza_swift;
			cg_sucursal_swift		= cg_sucursal_swift    == null?"":cg_sucursal_swift;
			ic_nafin_electronico	= ic_nafin_electronico == null?"":ic_nafin_electronico;

			// Registrar en BITACORA la VALIDACION para cuando la Clave del Afiliado al que se le di�
			// de alta la cuenta sea: P(PYME), D(DISTRIBUIDOR),I(IF Bancario/No Bancario) y B(IF/Beneficiario)
			if(
				"P".equals(cg_tipo_afiliado) ||
				"D".equals(cg_tipo_afiliado) ||
				"I".equals(cg_tipo_afiliado) ||
				"B".equals(cg_tipo_afiliado)
			){

				parametrosAdicionales 	= parametrosAdicionales == null?new HashMap():parametrosAdicionales;

				// Registrar la informacion registrada para la cuenta
				String decripcionSinValidacion 	= (String) parametrosAdicionales.get("DESCRIPCION_SIN_VALIDACION");
				decripcionSinValidacion 			= decripcionSinValidacion == null?"SIN VALIDAR\nNO DISPONIBLE":decripcionSinValidacion;
				String clavePantalla  				= (String) parametrosAdicionales.get("PANTALLA_ORIGEN");
				clavePantalla 							= clavePantalla  == null?"CONSCTAS"						  :clavePantalla;
				String ic_usuario						= (String) parametrosAdicionales.get("IC_USUARIO");

				//boolean esCuentaCLABE = "40".equals(ic_tipo_cuenta)?true:false;
				boolean esCuentaSWIFT = "50".equals(ic_tipo_cuenta)?true:false;

				String valoresAnteriores =
					"cg_estatus=CUENTA CORRECTA\n" +
					"cg_cuenta="+cg_cuenta+"\n" +
					"ic_producto_nafin="+ic_producto_nafin+"\n" +
					"ic_moneda="+ic_moneda+"\n" +
					"ic_bancos_tef="+ic_bancos_tef+"\n" +
					"ic_tipo_cuenta="+ic_tipo_cuenta+"\n" +
					"cg_plaza_swift="+(esCuentaSWIFT?cg_plaza_swift:"N/A")+"\n" +
					"cg_sucursal_swift="+(esCuentaSWIFT?cg_sucursal_swift:"N/A");

				String valoresActuales =
					decripcionSinValidacion + "\n" +
					"cg_cuenta="+cg_cuenta+"\n"  +
					"ic_producto_nafin="+ic_producto_nafin+"\n"  +
					"ic_moneda ="+ic_moneda+"\n"  +
					"ic_bancos_tef="+ic_bancos_tef+"\n"  +
					"ic_tipo_cuenta="+ic_tipo_cuenta+"\n"  +
					"cg_plaza_swift="+(esCuentaSWIFT?cg_plaza_swift:"N/A")+"\n"  +
					"cg_sucursal_swift="+(esCuentaSWIFT?cg_sucursal_swift:"N/A");

				Bitacora.grabarEnBitacora(
						con,
						clavePantalla,															//clavePantalla,
						"B".equals(cg_tipo_afiliado)?"I":cg_tipo_afiliado,			//tipoAfiliado; Si el tipo es "B" usar "I"
						ic_nafin_electronico, 												//claveNafinElectronico,
						ic_usuario,																//claveUsuario,
						valoresAnteriores,													//datoAnterior,
						valoresActuales                                          //datoActual
				);

			}

		} catch(Exception e){
			bOK = false;
			log.debug("invalidarCuenta(Exception)"+e);
			log.debug("invalidarCuenta.icCuenta  = <"+icCuenta+">");
			e.printStackTrace();
			throw new AppException("Error al invalidar cuenta.");
		} finally {

			if(rs != null) try { rs.close(); }catch(Exception e){}
			if(ps != null) try { ps.close(); }catch(Exception e){}

			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("invalidarCuenta(S)");
		}
	 }

	 public  String getNumeroAcusePdfFlujoFondos()
		throws AppException {

		log.info("DispersionBean::getNumeroAcusePdfFlujoFondos(E)");

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		String 			qrySentencia			= null;
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();
		String			numeroAcuse				= null;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT 	" +
					"	SEQ_PDF_FLUJO_FONDOS.NEXTVAL AS NUMERO_ACUSE " +
					"FROM 	" +
					"	DUAL 	";

				registros = con.consultarDB(qrySentencia, lVarBind, false);

				if(registros != null && registros.next()){
					numeroAcuse = registros.getString("NUMERO_ACUSE");
				}

		}catch(Exception e){
			log.debug("DispersionBean::getNumeroAcusePdfFlujoFondos(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener el numero consecutivo para usarse en el PDF de Flujo Fondos (Comprobante de Transferencia Electronica)");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("DispersionBean::getNumeroAcusePdfFlujoFondos(S)");
		}

		return numeroAcuse;
	}
/**
 *
 */

 public  List  consultaBitacoraCan( String TipoDispersion, String noEpo, String noIf, String fechaRegIni, String fechaRegFin,
		String fechaCancelacionIni, String fechaCancelacionFin) throws AppException {
		log.debug("consultaBitacoraCan :: (E) ");

		AccesoDB con = new AccesoDB();

		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer query = new StringBuffer();

		List voperaciones = new ArrayList();

		log.debug("TipoDispersion :: "+TipoDispersion);
		log.debug("noEpo :: "+noEpo);
		log.debug("noIf :: "+noIf);
		log.debug("fechaRegIni :: "+fechaRegIni);
		log.debug("fechaRegFin :: "+fechaRegFin);
		log.debug("fechaCancelacionIni :: "+fechaCancelacionIni);
		log.debug("fechaCancelacionFin :: "+fechaCancelacionFin);


		try {
			con.conexionDB();


					query.append(" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/ "+
									 " e.cg_razon_social as nombreepo,  "+
									 " i.cg_razon_social as nombreIf,  "+
									 " p.cg_razon_social as nombrePyme, "+
									 " p.cg_rfc as rfc, "+
									 " c.ic_bancos_tef as banco, "+
									 " c.ic_tipo_cuenta as tipocuenta, "+
									 "  c.cg_cuenta as nocuenta, "+
									 " ff.fn_importe as importe,"+
									 " me.error_cen_s as causa, "+
									 " ff.ic_flujo_fondos as NoFlujoFondo,  "+
									 " ff.cg_Usuario as usuario,   "+
									 " TO_CHAR (ff.df_cancelacion, 'DD/MM/YYYY HH24:Mi:SS') as  fcancelacion,   "+
									 " ff.cs_reproceso as reproceso, "+
									 " me.status_cen_i || ' ' || eff.cd_descripcion as MotivoRechazo" );

				query.append("  FROM int_flujo_fondos_err ff, "+
									 " cfe_m_encabezado_err me, "+
									 " comrel_nafin crn, "+
									 " comcat_epo e, "+
									 " comcat_if i,"+
									 " comcat_pyme p, "+
									 " com_cuentas c, "+
									 " comcat_estatus_ff eff ");

			 query.append("  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i "+
									 " AND ff.ic_epo = e.ic_epo "+
									 " AND ff.ic_if = i.ic_if(+)"+
									 " AND ff.ic_pyme = p.ic_pyme(+) "+
									 "	AND ff.ic_cuenta = c.ic_cuenta(+) "+
									 " AND me.status_cen_i = eff.ic_estatus_ff(+) "+
									 " AND ff.cs_reproceso = 'D'"+
									 " AND ff.cs_concluido = 'N' "+
									" AND ff.df_cancelacion IS NOT NULL "+
									 " AND me.status_cen_i = 60 " );



 			if (TipoDispersion.equals("E")) {
	   		   query.append(" AND (ff.df_fecha_venc IS NOT NULL OR ff.df_operacion IS NOT NULL)"   +
					    		    " AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
								    " AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') ");
			}

			if(TipoDispersion.equals("F")) {

					query.append(" AND me.status_cen_i = eff.ic_estatus_ff(+) "+
									 " AND (ff.ic_if IS NOT NULL AND ff.df_fecha_venc IS NOT NULL)" +
									 " AND crn.ic_nafin_electronico(+) = c.ic_nafin_electronico ");
			}

			if(!"".equals(noEpo)){
			   	query.append(" AND FF.ic_epo = "+noEpo);
			}else if(!"".equals(noIf)) {
				   query.append(" AND FF.ic_if = "+noIf);
			}


			 if(!fechaRegIni.equals("") && !fechaRegFin.equals("")) {
					query.append(" AND FF.df_registro >= TO_DATE('"+fechaRegIni+"','dd/mm/yyyy') "+
							   	 " AND FF.df_registro < (TO_DATE('"+fechaRegFin+"','dd/mm/yyyy') + 1) ");
			 }

			if(!fechaCancelacionIni.equals("") && !fechaCancelacionFin.equals("")) {
				  query.append(" AND FF.df_cancelacion >= TO_DATE('"+fechaCancelacionIni+"','dd/mm/yyyy') "+
							   	" AND FF.df_cancelacion < (TO_DATE('"+fechaCancelacionFin+"','dd/mm/yyyy') + 1) ");

			 }

        query.append(" UNION ALL ");

        query.append(" SELECT /*+index(ff) index(me) use_nl(ff me crn e i p c eff)*/ "+
									 " e.cg_razon_social as nombreepo,  "+
									 " i.cg_razon_social as nombreIf,  "+
									 " p.cg_razon_social as nombrePyme, "+
									 " p.cg_rfc as rfc, "+
									 " c.ic_bancos_tef as banco, "+
									 " c.ic_tipo_cuenta as tipocuenta, "+
									 "  c.cg_cuenta as nocuenta, "+
									 " ff.fn_importe as importe,"+
									 " me.error_cen_s as causa, "+
									 " ff.ic_flujo_fondos as NoFlujoFondo,  "+
									 " ff.cg_Usuario as usuario,   "+
									 " TO_CHAR (ff.df_cancelacion, 'DD/MM/YYYY HH24:Mi:SS') as  fcancelacion,   "+
									 " ff.cs_reproceso as reproceso, "+
									 " me.status_cen_i || ' ' || eff.cd_descripcion as MotivoRechazo" );

				query.append("  FROM int_flujo_fondos ff, "+
									 " cfe_m_encabezado me, "+
									 " comrel_nafin crn, "+
									 " comcat_epo e, "+
									 " comcat_if i,"+
									 " comcat_pyme p, "+
									 " com_cuentas c, "+
									 " comcat_estatus_ff eff ");

			 query.append("  WHERE ff.ic_flujo_fondos = me.idoperacion_cen_i "+
									 " AND ff.ic_epo = e.ic_epo "+
									 " AND ff.ic_if = i.ic_if(+)"+
									 " AND ff.ic_pyme = p.ic_pyme(+) "+
									 "	AND ff.ic_cuenta = c.ic_cuenta(+) "+
									 " AND me.status_cen_i = eff.ic_estatus_ff(+) "+
									 " AND ff.cs_reproceso = 'D'"+
									 " AND ff.cs_concluido = 'N' "+
									" AND ff.df_cancelacion IS NOT NULL "+
									 " AND me.status_cen_i = 60 " );



 			if (TipoDispersion.equals("E")) {
	   		   query.append(" AND (ff.df_fecha_venc IS NOT NULL OR ff.df_operacion IS NOT NULL)"   +
					    		    " AND crn.ic_epo_pyme_if = NVL (ff.ic_pyme, ff.ic_if) "+
								    " AND crn.cg_tipo = DECODE (ff.ic_pyme, NULL, 'I', 'P') ");
			}

			if(TipoDispersion.equals("F")) {

					query.append(" AND me.status_cen_i = eff.ic_estatus_ff(+) "+
									 " AND (ff.ic_if IS NOT NULL AND ff.df_fecha_venc IS NOT NULL)" +
									 " AND crn.ic_nafin_electronico(+) = c.ic_nafin_electronico ");
			}

			if(!"".equals(noEpo)){
			   	query.append(" AND FF.ic_epo = "+noEpo);
			}else if(!"".equals(noIf)) {
				   query.append(" AND FF.ic_if = "+noIf);
			}


			 if(!fechaRegIni.equals("") && !fechaRegFin.equals("")) {
					query.append(" AND FF.df_registro >= TO_DATE('"+fechaRegIni+"','dd/mm/yyyy') "+
							   	 " AND FF.df_registro < (TO_DATE('"+fechaRegFin+"','dd/mm/yyyy') + 1) ");
			 }

			if(!fechaCancelacionIni.equals("") && !fechaCancelacionFin.equals("")) {
				  query.append(" AND FF.df_cancelacion >= TO_DATE('"+fechaCancelacionIni+"','dd/mm/yyyy') "+
							   	" AND FF.df_cancelacion < (TO_DATE('"+fechaCancelacionFin+"','dd/mm/yyyy') + 1) ");

			 }






				ps = con.queryPrecompilado(query.toString());
				rs = ps.executeQuery();
				if(rs!=null){
					while (rs.next()) {
						List datos = new ArrayList();
	/*0*/					datos.add((rs.getString("nombreepo")==null?"":rs.getString("nombreepo")));
	/*1*/					datos.add((rs.getString("nombreIf")==null?"":rs.getString("nombreIf")));
	/*2*/ 				datos.add((rs.getString("nombrePyme")==null?"":rs.getString("nombrePyme")));
	/*3*/ 				datos.add((rs.getString("rfc")==null?"":rs.getString("rfc")));
	/*4*/					datos.add((rs.getString("banco")==null?"":rs.getString("banco")));
	/*5*/					datos.add((rs.getString("tipocuenta")==null?"":rs.getString("tipocuenta")));
	/*6*/					datos.add((rs.getString("nocuenta")==null?"":rs.getString("nocuenta")));
	/*7*/					datos.add((rs.getString("importe")==null?"":rs.getString("importe")));
	/*8*/					datos.add((rs.getString("causa")==null?"":rs.getString("causa")));
	/*9*/				   datos.add((rs.getString("usuario")==null?"":rs.getString("usuario")));
	/*10*/				datos.add((rs.getString("fcancelacion")==null?"":rs.getString("fcancelacion")));
	/*11*/				datos.add((rs.getString("NoFlujoFondo")==null?"":rs.getString("NoFlujoFondo")));
	/*12*/				datos.add((rs.getString("reproceso")==null?"":rs.getString("reproceso")));
	/*13*/				datos.add((rs.getString("MotivoRechazo")==null?"":rs.getString("MotivoRechazo")));
							voperaciones.add(datos);
					}
				}
				if(rs!=null)rs.close();
				if(ps!=null)ps.close();



			log.debug("TipoDispersion::    "+TipoDispersion+"     query::  "+query);

			log.debug("voperaciones"+voperaciones);

		} catch(Exception e) {
			e.printStackTrace();
			log.info("Exception en consultaBitacoraCan(). "+e.getMessage());

		}finally {
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}

			return voperaciones;
		}

	/**
	 *	 Invoca al procedimiento almacenado: SP_BITACORA_PROCESO, el cual graba en BIT_EJECUTA_PROCESO
	 *  el nombre del proceso que se ejecuta y el cual se proporciona en el parametro <tt>nombreProceso</tt>.
	 *
	 *  @param nombreProceso Cadena de Texto con el nombre del proceso a registrar su ejecion.
	 *
	 */
	private void registraEnBitacoraDeProcesos(String nombreProceso)
		throws AppException {

		log.info("registraEnBitacoraDeProcesos(E)");

		AccesoDB 				con 	= new AccesoDB();

		boolean 					bOk 	= true;

		try {

			con.conexionDB();

			CallableStatement cs = con.ejecutaSP("SP_BITACORA_PROCESO(?)");
			cs.setString(1, nombreProceso);
			cs.execute();
			cs.close();

		} catch(Exception e) {

			bOk = false;
			log.debug("registraEnBitacoraDeProcesos(Exception)");
			log.debug("registraEnBitacoraDeProcesos.nombreProceso = <"+nombreProceso+">");
			e.printStackTrace();
			throw new AppException("Error al Registrar en Bitacora de Procesos");
		}finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			log.info("registraEnBitacoraDeProcesos(S)");
		}
	}

	/**
	 *	Borra de la tabla BIT_EJECUTA PROCESO, el registro correspondiente al dia de doy cuyo
	 * nombre se proporciona en el parametro: <tt>nombreProceso</tt>
	 *
	 * @param nombreProceso Nombre del Proceso a borrar de la bitacora.
	 *
	 */
	private void borraNombreProcesoDeLaBitacora(String nombreProceso)
		throws AppException{

		log.info("borraNombreProcesoDeLaBitacora(E)");

		AccesoDB 			con 					= new AccesoDB();
		PreparedStatement ps      				= null;
		StringBuffer 		qryDeleteDatos 	= new StringBuffer();
		List 					conditions 			= new ArrayList();
		boolean				bOk					= true;

		try {
			con.conectarDB();

			qryDeleteDatos.append(
				"DELETE FROM 									"  +
				"	BIT_EJECUTA_PROCESO 						"  +
				"WHERE 											"  +
				"	CG_NOMBRE_PROCESO = ? AND 				"  + // ?
				"	TRUNC(DF_FECHA)   = TRUNC(SYSDATE) 	"
			);
			conditions.add(nombreProceso);

			ps = con.queryPrecompilado(qryDeleteDatos.toString(),conditions);
			ps.executeUpdate();
			ps.close();

		}catch(Exception e){

			bOk = false;
			log.debug("borraNombreProcesoDeLaBitacora(Exception)");
			log.debug("borraNombreProcesoDeLaBitacora.nombreProceso = <"+nombreProceso+">");
			e.printStackTrace();
			throw new AppException("Error al borrar registro de ejecucion el proceso: " + nombreProceso );

		}finally{

			con.terminaTransaccion(bOk);
			if (con.hayConexionAbierta())	con.cierraConexionDB();
			log.info("borraNombreProcesoDeLaBitacora(S)");

		}

	}

	/**
	 *	Este metodo revisa en la bitacora de procesos: BIT_EJECUTA_PROCESO, si se ha ejecutado
	 * el dia de hoy el proceso cuyo nombre se proporciona en el parametro: <tt>nombreProceso</tt>.
	 *
	 * @param nombreProceso Nombre del Proceso a consultar.
	 * @return <tt>boolean</tt> <tt>true</tt> si se ha ejecutado. <tt>false</tt> en caso contrario.
	 */
	private boolean seHaEjecutatoProcesoHoy(String nombreProceso)
		throws AppException{

			log.info("seHaEjecutatoProcesoHoy(E)");

			AccesoDB 		con 				= new AccesoDB();
			StringBuffer	qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			boolean			resultado		= false;

			try {
				con.conexionDB();
				qrySentencia.append(
					"SELECT 								"  +
					"	DECODE( 							"  +
					"		trunc(MAX(DF_FECHA)), 	"  +
					"		trunc(sysdate),'true', 	"  +
					"		'false' 						"  +
					"	) as PROCESO_EJECUTADO 		"  +
					"FROM 								"  +
					"	BIT_EJECUTA_PROCESO 			"  +
					"WHERE 								"  +
					"	CG_NOMBRE_PROCESO = ?  		");

				lVarBind.add(nombreProceso);

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("PROCESO_EJECUTADO").equals("true")?true:false;
				}

		}catch(Exception e){

			log.debug("seHaEjecutatoProcesoHoy(Exception)");
			log.debug("seHaEjecutatoProcesoHoy.nombreProceso = <"+nombreProceso+">");
			e.printStackTrace();
			throw new AppException("Error al consultar estatus del proceso " + nombreProceso + " en la bitacora de procesos: BIT_EJECUTA_PROCESO ");

		}finally{

			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("seHaEjecutatoProcesoHoy(S)");
		}

		return resultado;
	}

	/**
	 * Devuelve el query que trae los documentos que vencen el dia actual(SPEI) o el dia
	 * siguiente habil(TEF) de la Cadena Nafin.
	 *
	 * @param ic_folio_ff ID del Folio de Flujo de Fondos
	 * @param sNoEstatus  Clave del Estatus, solo acepta dos valores posibles: estatus 9 para
	 *                    documentos "Vencido sin Operar" y estatus 4 para documentos "Operados"
	 * @param sFechaVenc	 Fecha de Vencimiento del documento.
	 *
	 */
	private String getQueryDispersionEpoNafin(
			String ic_folio_ff,
			String sNoEstatus,
			String sFechaVenc
		) throws AppException {

		log.info("getQueryDispersionEpoNafin(E)");

		String 			sNoMoneda			= "1"; 		// Moneda Nacional
		String 			sNoEPO				= "658"; 	// CADENA NAFIN: Epo Nacional Financiera S.N.C.

		StringBuffer 	query 				= new StringBuffer();
		String 			fechaSigDiaHabil 	= "";
		String 			claveTipoCuenta 	= "";

		try {
			//ISeleccionDocumento 		BeanSeleccionDocumento 	= ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

			String sFechaVencPyme = "";
			// Debido a que la cadena nafin no opera la fecha de vencimiento pyme
			//if("9".equals(sNoEstatus)) {
			//	sFechaVencPyme = BeanSeleccionDocumento.operaFechaVencPyme(sNoEPO);
			//}

			fechaSigDiaHabil 	= Fecha.getProximoDiaHabilPorEPO(sFechaVenc,"DD/MM/YYYY",1,sNoEPO);
			claveTipoCuenta 	= (sNoMoneda != null && sNoMoneda.equals("54"))?claveCuentaSwift:"40";// Cuenta Swift: // Codigo cuenta cliente

			// Estatus 9: Vencido sin Operar - Solo con medio de pago TEF
			if(sNoEstatus.equals("9")) {

				String importeDisp = "d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)";

				query.append(
					" SELECT   /*+index(d) use_nl(d n p c m)*/        "  +
					"           '"+ic_folio_ff+"' AS ic_flujo_fondos, "  +
					"           p.cg_rfc,                             "  +
					"           p.cg_razon_social,                    "  +
					"           ("+importeDisp+") as fn_importe,      "  +
					"           1 as ig_total_documentos,             "  +
					"           d.ic_pyme AS desarrollador,           "  +
					"           TO_NUMBER (NULL) AS beneficiario,     "  +
					"           c.ic_cuenta,                          "  +
					"           TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc, "  +
					"           c.ic_estatus_tef,                     "  +
					"           c.ic_estatus_cecoban,                 "  +
					"           m.cd_nombre,                          "  +
					"           c.ic_bancos_tef,                      "  +
					"           c.ic_tipo_cuenta,                     "  +
					"           c.cg_cuenta,                          "  +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg, "  +
					"				TO_CHAR (SIGFECHAHABILXEPO("+sNoEPO+",d.df_fecha_venc"+sFechaVencPyme+",1,'+'),'DD-MM-YYYY') AS fechatrasf, "  +
					"				TO_CHAR (SIGFECHAHABILXEPO("+sNoEPO+",d.df_fecha_venc"+sFechaVencPyme+",1,'+'),'DD-MM-YYYY') AS fechaapli,  "  +
					"           dc.cg_via_liq || '-NE'           AS refrastreo,     "  +
					"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo, "  +
					"           DECODE (c.ic_estatus_cecoban, 99, 0, -1)      AS estatusoper,    "  +
					"				d.ig_numero_docto,                                               "  +
					"				pe.cg_pyme_epo_interno,                                          "  +
					"				d.ct_referencia,                                                 "  +
					"           TO_CHAR (d.df_fecha_docto, 'dd-mm-yyyy') AS df_fecha_docto,      "  +
					"				dc.cg_via_liq, 																  "  +
					"				d.ic_documento, 																  "  +
					"           'DispersionEJB::getQueryDispersionEpoNafin',                     "  +
					"				d.cg_campo2                                   AS numero_factura  "  +
					"     FROM com_documento d, "  +
					"          comrel_nafin  n, "  +
					"          comcat_pyme   p, "  +
					"          com_cuentas   c, "  +
					"          comcat_moneda m, "  +
					"			  comrel_docto_cuenta dc, "  +
					"			  comrel_pyme_epo pe "  +
					"    WHERE d.ic_pyme              = p.ic_pyme"   +
					"      AND c.ic_moneda            = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND c.ic_epo               = "+sNoEPO+
					"      AND n.ic_epo_pyme_if       = p.ic_pyme"   +
					"      AND n.cg_tipo              = 'P' "  +
					"		 AND c.ic_estatus_cecoban	 = 99  "  +
					"      AND d.ic_epo               = "+sNoEPO+
					"      AND m.ic_moneda            = "+sNoMoneda+
					"      AND d.ic_moneda            = m.ic_moneda"+
					"      AND d.ic_estatus_docto     = "+sNoEstatus+
					"      AND c.ic_producto_nafin    = 1"   +
					"      AND c.ic_tipo_cuenta       = "+ claveTipoCuenta +
					"		 AND dc.ic_documento        = d.ic_documento          "  +
					"		 AND dc.ic_cuenta				 = c.ic_cuenta             "  +
					"		 AND pe.ic_pyme				 = d.ic_pyme					"  +
					"		 AND pe.ic_epo					 = d.ic_epo						"  +
					"		 AND (                                                "  +
					"				 (                                              "  +
					"					d.df_fecha_venc"+sFechaVencPyme+" >= TO_DATE('"+fechaSigDiaHabil+"', 'DD/MM/YYYY')    AND	"  + // Siguiente Dia Habil
					"					d.df_fecha_venc"+sFechaVencPyme+" <  TO_DATE('"+fechaSigDiaHabil+"', 'DD/MM/YYYY') +1 AND "  +
					"					dc.cg_via_liq = 'TEF'  																						"  +
					"				 )                                              "  +
					"			  )                                                "  +
					"      AND d.cs_dscto_especial     = 'N'                    "  + // Solo Factoraje Normal
					"      AND NOT EXISTS(  SELECT 1                            "  +
					"                         FROM										"  +
					"									comrel_documento_ff dff      		"  +
					"                    	 WHERE 										"  +
					"									d.ic_documento   			= dff.ic_documento AND "  +
					"                     		dff.ic_producto_nafin 	= 1 )    "

				);

			// Estatus 10: Pagado sin Operar, solo con medio de pago SPEI
			} else if(sNoEstatus.equals("10")) {

				String importeDisp = "d.fn_monto - NVL ((d.fn_monto * NVL (d.fn_porc_beneficiario, 0) / 100), 0)";

				query.append(
					" SELECT   /*+index(d) use_nl(d n p c m)*/        "  +
					"           '"+ic_folio_ff+"' AS ic_flujo_fondos, "  +
					"           p.cg_rfc,                             "  +
					"           p.cg_razon_social,                    "  +
					"           ("+importeDisp+") as fn_importe,      "  +
					"           1 as ig_total_documentos,             "  +
					"           d.ic_pyme AS desarrollador,           "  +
					"           TO_NUMBER (NULL) AS beneficiario,     "  +
					"           c.ic_cuenta,                          "  +
					"           TO_CHAR (d.df_fecha_venc"+sFechaVencPyme+", 'dd/mm/yyyy') AS df_fecha_venc, "  +
					"           c.ic_estatus_tef,                     "  +
					"           c.ic_estatus_cecoban,                 "  +
					"           m.cd_nombre,                          "  +
					"           c.ic_bancos_tef,                      "  +
					"           c.ic_tipo_cuenta,                     "  +
					"           c.cg_cuenta,                          "  +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg, "  +
					"				TO_CHAR (SIGFECHAHABILXEPO("+sNoEPO+",d.df_fecha_venc"+sFechaVencPyme+",1,'+'),'DD-MM-YYYY') AS fechatrasf, "  +
					"				TO_CHAR (SIGFECHAHABILXEPO("+sNoEPO+",d.df_fecha_venc"+sFechaVencPyme+",1,'+'),'DD-MM-YYYY') AS fechaapli,  "  +
					"           dc.cg_via_liq || '-NE'           AS refrastreo,     "  +
					"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy') AS leyendarastreo, "  +
					"           DECODE (c.ic_estatus_cecoban, 99, 0, -1)      AS estatusoper,    "  +
					"				d.ig_numero_docto,                                               "  +
					"				pe.cg_pyme_epo_interno,                                          "  +
					"				d.ct_referencia,                                                 "  +
					"           TO_CHAR (d.df_fecha_docto, 'dd-mm-yyyy') AS df_fecha_docto,      "  +
					"				dc.cg_via_liq, 																  "  +
					"				d.ic_documento, 																  "  +
					"           'DispersionEJB::getQueryDispersionEpoNafin',                     "  +
					"				d.cg_campo2                                   AS numero_factura  "  +
					"     FROM com_documento d, "  +
					"          comrel_nafin  n, "  +
					"          comcat_pyme   p, "  +
					"          com_cuentas   c, "  +
					"          comcat_moneda m, "  +
					"			  comrel_docto_cuenta dc, "  +
					"			  comrel_pyme_epo pe "  +
					"    WHERE d.ic_pyme              = p.ic_pyme"   +
					"      AND c.ic_moneda            = m.ic_moneda"   +
					"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
					"      AND c.ic_epo               = "+sNoEPO+
					"      AND n.ic_epo_pyme_if       = p.ic_pyme"   +
					"      AND n.cg_tipo              = 'P' "  +
					"		 AND c.ic_estatus_cecoban	 = 99  "  +
					"      AND d.ic_epo               = "+sNoEPO+
					"      AND m.ic_moneda            = "+sNoMoneda+
					"      AND d.ic_moneda            = m.ic_moneda"+
					"      AND d.ic_estatus_docto     = "+sNoEstatus+
					"      AND c.ic_producto_nafin    = 1"   +
					"      AND c.ic_tipo_cuenta       = "+ claveTipoCuenta +
					"		 AND dc.ic_documento        = d.ic_documento          "  +
					"		 AND dc.ic_cuenta				 = c.ic_cuenta             "  +
					"		 AND pe.ic_pyme				 = d.ic_pyme					"  +
					"		 AND pe.ic_epo					 = d.ic_epo						"  +
					"		 AND (                                                "  +
					"				 (                                              "  + // Dia Actual
					"					d.df_fecha_venc"+sFechaVencPyme+" >=  TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')    AND  "  +
					"					d.df_fecha_venc"+sFechaVencPyme+" <  (TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')+1) AND  "  +
					"              dc.cg_via_liq = 'SPEI'                                                                 "  +
					"				 )                                              "  +
					"			  )                                                "  +
					"      AND d.cs_dscto_especial     = 'N'                    " + // Solo Factoraje Normal
					"      AND NOT EXISTS(  SELECT 1                            "  +
					"                         FROM										"  +
					"									comrel_documento_ff dff      		"  +
					"                    	 WHERE 										"  +
					"									d.ic_documento   			= dff.ic_documento AND "  +
					"                     		dff.ic_producto_nafin 	= 1 )    "

				);

			// Estatus 4: Operada
			} else if(sNoEstatus.equals("4")){
				query.append(
					" SELECT   /*+index(c) use_nl(d n i c m)*/             "  +
					"           '"+ic_folio_ff+"' AS ic_flujo_fondos,      "  +
					"				d.fn_monto        AS fn_importe,           "  +
					"           1                 AS ig_total_documentos,  "  +
					"				NULL              AS ic_pyme,              "  +
					"           c.ic_cuenta,                               "  +
					"				TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy')   AS df_fecha_venc, "   +
					"           i.ic_if,                                   "  +
					"				DECODE (c.ic_estatus_cecoban, 99, 0, -1)  AS estatusoper,   "   +
					"           i.cg_rfc,          "  +
					"				i.cg_razon_social, "  +
					"				m.cd_nombre,       "  +
					"				c.ic_bancos_tef,   "  +
					"           c.ic_tipo_cuenta,  "  +
					"				c.cg_cuenta,       "  +
					"				c.ic_estatus_tef,  "  +
					"				c.ic_estatus_cecoban,                            "  +
					"           TO_CHAR (SYSDATE, 'dd-mm-yyyy') AS fechareg,     "  +
					"				TO_CHAR (SIGFECHAHABILXEPO("+sNoEPO+",d.df_fecha_venc"+sFechaVencPyme+",1,'+'),'DD-MM-YYYY') AS fechatrasf, "  +
					"				TO_CHAR (SIGFECHAHABILXEPO("+sNoEPO+",d.df_fecha_venc"+sFechaVencPyme+",1,'+'),'DD-MM-YYYY') AS fechaapli,  "  +
					"           'SPEI' || '-NE'                        AS refrastreo,              "  +
					"           'DISPEPO ' || TO_CHAR (SYSDATE, 'dd mm yyyy')  AS leyendarastreo, "  +
					"				d.ig_numero_docto,                                                "  +
					"				'' as cg_pyme_epo_interno,                                        "  +
					"				d.ct_referencia,                                                	"  +
					"           TO_CHAR (d.df_fecha_docto, 'dd-mm-yyyy') AS df_fecha_docto,       "  +
					"				'SPEI' as cg_via_liq, 														   "  +
					"				d.ic_documento, 																   "  +
					"           'DispersionEJB::getQueryDispersionEpoNafin',                      "  +
					"				d.cg_campo2                                   AS numero_factura   "  +
					"     FROM com_documento d, "  +
					"          comrel_nafin  n, "  +
					"          comcat_if     i, "  +
					"          com_cuentas   c, "  +
					"          comcat_moneda m  "  +
					"    WHERE d.ic_if                 = i.ic_if                "  +
					"      AND c.ic_moneda             = m.ic_moneda            "  +
					"      AND c.ic_nafin_electronico  = n.ic_nafin_electronico "  +
					"      AND n.ic_epo_pyme_if        = i.ic_if                "  +
					"      AND n.cg_tipo               = 'I'                    "  +
					"      AND d.ic_epo                = "+sNoEPO+
					"      AND m.ic_moneda             = "+sNoMoneda+
					"      AND d.ic_moneda             = m.ic_moneda            "  +
					"      AND d.ic_estatus_docto      = "+sNoEstatus+
					"      AND c.ic_producto_nafin     = 1                      "  +
					"      AND c.ic_tipo_cuenta        = "+ claveTipoCuenta +
					"		 AND c.ic_estatus_cecoban	  = 99                     "  +
					"      AND c.ic_epo                = "+sNoEPO+
					"      AND c.cg_tipo_afiliado      = 'I'                    "  +
					"		 AND (                                                "  +
					"				 (                                              "  + // Dia Actual
					"					d.df_fecha_venc     >=  TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')    AND  "  +
					"					d.df_fecha_venc     <  (TO_DATE ('"+sFechaVenc+"', 'dd/mm/yyyy')+1)      "  +
					"				 ) "  +
					"			  )   "  +
					"      AND d.cs_dscto_especial     = 'N'                    "  + // Solo Factoraje Normal
					"      AND exists ( select * from comrel_docto_cuenta rel where d.ic_documento =  rel.ic_documento and rel.cg_via_liq in ('SPEI','TEF') ) "  +
					"      AND NOT EXISTS(  SELECT 1                            "  +
					"                         FROM										"  +
					"									comrel_documento_ff dff      		"  +
					"                    	 WHERE 										"  +
					"									d.ic_documento   			= dff.ic_documento AND "  +
					"                     		dff.ic_producto_nafin 	= 1 )    "

				);
			}

		} catch(Exception e) {

			log.debug("getQueryDispersionEpoNafin(Exception)");
			log.debug("getQueryDispersionEpoNafin.ic_folio_ff 	= <"+ic_folio_ff+">");
			log.debug("getQueryDispersionEpoNafin.sNoEstatus 	= <"+sNoEstatus+">");
			log.debug("getQueryDispersionEpoNafin.sFechaVenc 	= <"+sFechaVenc+">");

			e.printStackTrace();
			throw new AppException("Error al obtener el query de detalle de dispersion de la EPO NAFIN");

		} finally {
			log.info("getQueryDispersionEpoNafin(S)");
		}

		return query.toString();
	}

	private String getDigitoIdentificador(String cuenta){
		log.info("getDigitoIdentificador(E)");
		String digitoIdentificador = Character.toString(cuenta.charAt(cuenta.length()-1));
		log.info("getDigitoIdentificador(S)");
		return digitoIdentificador;
	}

	/**
	 * Proceso que realiza la Dispersion Automatica de la Cadena Nafin para el dia Actual
	 */
	public void realizaDispersionAutomaticaDoctosCadenaNafin() throws AppException{

		System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosCadenaNafin(E)");

		String fechaDeHoy 			= null;
		String estatus 				= null;
		String fechaDeOperacion 	= "";

		try{

			// Obtener Fecha del Dia Actual
			fechaDeHoy 		= Fecha.getFechaActual();
			estatus 			= "";

			// Si es dia inhabil no realizar ninguna dispersion
			boolean esDiaInhabilPorEpo = Fecha.esDiaInhabilPorEpo("658",fechaDeHoy,"dd/MM/yyyy");
			if(esDiaInhabilPorEpo){
				System.out.println("El proceso Automatico de Dispersion de la Cadena Nafin no realizara ninguna dispersion, ya que la fecha de hoy "+fechaDeHoy+" corresponde a un dia inhabil");
				System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosCadenaNafin(S)");
				return;
			}

			System.out.println("realizaDispersionAutomaticaDoctosCadenaNafin.fechaDeHoy = <"+fechaDeHoy+">");// Debug info
			// Verificar si no se ha ejecutado el proceso
			if(seHaEjecutatoProcesoHoy("disp_auto_cadena_nafin(E)")){
				System.out.println("El proceso Automatico de Dispersion de la Cadena Nafin para el dia "+fechaDeHoy+", ya ha sido ejecutado");
				System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosCadenaNafin(S)");
				return;
			}
			// Registrar en bitacora inicio del proceso
			registraEnBitacoraDeProcesos("disp_auto_cadena_nafin(E)");
			// Borrar tabla
			borrarInformacionDeComDoctosDispSIOR();
			// Realizar Dispersion de Documentos Vencidos sin Operar (Estatus 9) con tipo de pago: TEF
			System.out.println("Realizando Dispersion de Documentos Vencidos sin Operar para la Cadena Nafin");
			estatus 	= "9";
			llenaEncabezadoFlujoFondosCadenaNafin(fechaDeHoy,estatus,fechaDeOperacion);
			// Realizar Dispersion de Documentos Pagado sin Operar (Estatus 10) con tipo de pago: SPEI
			System.out.println("Realizando Dispersion de Documentos Pagados sin Operar para la Cadena Nafin");
			estatus 	= "10";
			llenaEncabezadoFlujoFondosCadenaNafin(fechaDeHoy,estatus,fechaDeOperacion);
			// Realizar Dispersion Automatica de Documentos con estatus Operada ( Estatus 4 )
			System.out.println("Realizando Dispersion Automatica de Documentos con estatus Operada para la Cadena Nafin");
			estatus 	= "4";
			llenaEncabezadoFlujoFondosCadenaNafin(fechaDeHoy,estatus,fechaDeOperacion);
			// Guardar Fecha y Hora de Termino
			registraEnBitacoraDeProcesos("disp_auto_cadena_nafin(S)");

		}catch(Exception e){

			System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosCadenaNafin(Exception)");
			System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosCadenaNafin.fechaDeHoy 			 = <"+fechaDeHoy+">");
			System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosCadenaNafin.estatus            = <"+estatus+">");
			System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosCadenaNafin.fechaDeOperacion   = <"+fechaDeOperacion+">");

			// Si hay algun fallo borrar registro de inicio del proceso correspondiente al dia de hoy
			try { borraNombreProcesoDeLaBitacora("disp_auto_cadena_nafin(E)"); }catch(Exception a){}
			e.printStackTrace();
			throw new AppException("Ocurrio un Error al Realizar la Dispersion Automatica de Documentos de la Cadena Nafin");

		}

		System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosCadenaNafin(S)");
	}

	/**
	 * Proceso que realiza la Dispersion Automatica De Documentos Vencidos de la Cadena Nafin
	 * para el dia Actual
	 */
	public void realizaDispersionAutomaticaDoctosVencidosCadenaNafin() throws AppException{

		System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosVencidosCadenaNafin(E)");

		String fechaDeHoy 			= null;
		String estatus 				= null;
		String fechaDeOperacion 	= "";

		try{

			// Obtener Fecha del Dia Actual
			fechaDeHoy 		= Fecha.getFechaActual();
			estatus 			= "";

			// Si es dia inhabil no realizar ninguna dispersion
			boolean esDiaInhabilPorEpo = Fecha.esDiaInhabilPorEpo("658",fechaDeHoy,"dd/MM/yyyy");
			if(esDiaInhabilPorEpo){
				System.out.println("El proceso Automatico de Dispersion Vespertina de Documentos Vencidos Sin Operar de la Cadena Nafin no realizara ninguna dispersion, ya que la fecha de hoy "+fechaDeHoy+" corresponde a un dia inhabil");
				System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosVencidosCadenaNafin(S)");
				return;
			}

			System.out.println("realizaDispersionAutomaticaDoctosVencidosCadenaNafin.fechaDeHoy = <"+fechaDeHoy+">");// Debug info
			// Verificar si no se ha ejecutado el proceso
			if(seHaEjecutatoProcesoHoy("disp_auto_venc_cadena_nafin(E)")){
				System.out.println("El proceso Automatico de Dispersion Vespertina de Documentos Vencidos Sin Operar de la Cadena Nafin para el dia "+fechaDeHoy+", ya ha sido ejecutado");
				System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosVencidosCadenaNafin(S)");
				return;
			}
			// Registrar en bitacora inicio del proceso
			registraEnBitacoraDeProcesos("disp_auto_venc_cadena_nafin(E)");
			// Realizar Dispersion Vespertina de Documentos Vencidos sin Operar (Estatus 9) con tipo de pago: TEF
			System.out.println("Realizando Dispersion Vespertina de Documentos Vencidos Sin Operar para la Cadena Nafin");
			estatus 	= "9";
			llenaEncabezadoFlujoFondosCadenaNafin(fechaDeHoy,estatus,fechaDeOperacion);
			// Guardar Fecha y Hora de Termino
			registraEnBitacoraDeProcesos("disp_auto_venc_cadena_nafin(S)");

		}catch(Throwable e){

			System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosVencidosCadenaNafin(Exception)");
			System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosVencidosCadenaNafin.fechaDeHoy 			= <"+fechaDeHoy+">");
			System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosVencidosCadenaNafin.estatus            = <"+estatus+">");
			System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosVencidosCadenaNafin.fechaDeOperacion   = <"+fechaDeOperacion+">");

			// Si hay algun fallo borrar registro de inicio del proceso correspondiente al dia de hoy
			try { borraNombreProcesoDeLaBitacora("disp_auto_venc_cadena_nafin(E)"); }catch(Exception a){}
			e.printStackTrace();
			throw new AppException("Ocurrio un Error al Realizar la Dispersion Automatica Vespertina de Documentos Vencidos Sin Operar de la Cadena Nafin");

		}

		System.out.println("DispersionBean::realizaDispersionAutomaticaDoctosVencidosCadenaNafin(S)");
	}

	/**
	 * Llena los encabezados de flujo fondos para la cadena Nafin
	 *
	 */
	private void llenaEncabezadoFlujoFondosCadenaNafin(
										String sFechaVenc,
										String sNoEstatus,
										String sFechaOperacion
		) throws Exception {

		log.info("llenaEncabezadoFlujoFondosCadenaNafin(E)");

		AccesoDB 			con 				= new AccesoDB();
		PreparedStatement ps 				= null;
		ResultSet 			rs 				= null;
		ResultSet 			rs1 				= null;
		CallableStatement cs					= null;
		String 				query 			= "",
								queri 			= "",
								sRazonSocial 	= "";
		boolean 				bOk 				= true;

		String sNoEPO				= "658";
		String sNoMoneda 			= "1";
		String sOrigen				= "EPOS";
		String fechaSigDiaHabil = Fecha.getProximoDiaHabilPorEPO(sFechaVenc,"DD/MM/YYYY",1,sNoEPO);
		String sNoIF				= "";

		log.debug("\n sNoEPO:			 "+sNoEPO);
		log.debug("\n sNoMoneda:		 "+sNoMoneda);
		log.debug("\n sFechaVenc:		 "+sFechaVenc);
		log.debug("\n sNoIF:			    "+sNoIF);
		log.debug("\n sOrigen:			 "+sOrigen);
		log.debug("\n sNoEstatus:		 "+sNoEstatus);
		log.debug("\n sFechaOperacion: "+sFechaOperacion);

		try {
			con.conexionDB();

			//Obtencion de la Razon Social que realiza la Dispersion de acuerdo al Tipo.
			sRazonSocial 		= getRazonSocial(sOrigen, sNoEPO, sNoIF, con);
			fechaSigDiaHabil 	= Fecha.getProximoDiaHabilPorEPO(sFechaVenc,"DD/MM/YYYY",1,sNoEPO);

			log.debug("\n ***********************************************");
			log.debug("\n sOrigen: "+sOrigen);

			String folio_ff = "";//getFolioFF(con);
			query = getQueryDispersionEpoNafin(folio_ff,sNoEstatus,sFechaVenc);

			boolean	esDocumentoOperado	=((sNoEstatus != null) && sNoEstatus.equals("4"))?true:false;

			try {

				int 	iNoAfecFF 	= 0;
				int	iNoAfecEnca = 0;
				//long 	iNoReg 		= 0l; <--- deprecated

				log.debug("\n ****************** \n Registros a Insertar en FF: "+query);
				ps = con.queryPrecompilado(query);
				ps.clearParameters();
				rs = ps.executeQuery();

				while(rs.next()) {

					//iNoReg++;

					long 		folioFF		= getFolioFF();
					double 	importe		= rs.getDouble("fn_importe");
					int 		totDoctos	= rs.getInt("ig_total_documentos");
					int 		estatusoper	= rs.getInt("estatusoper");

					String desarrolador 				= "";
					String beneficiario 				= "";
					String ic_pyme		  				= "";
					String ic_if 						= "";
					String cgPymeEpoInterno 		= "";
					String cgRazonSocialIF			= "";
					String cgRfcIF						= "";
					String igNumeroDocto				= "";
					String ctReferencia				= "";
					String cgCuenta 					= "";
					String cgDigitoIdentificador 	= "";
					String cgViaLiq					= "";
					String dfFechaDocto       		= "";
					String dfFechaVenc        		= "";
					String icDocumento				= "";
					String numeroFactura				= "";

					if( sOrigen.equals("EPOS") && ( sNoEstatus.equals("9") || sNoEstatus.equals("10") ) ){
						desarrolador 		= rs.getString("desarrollador")			== null?"0"	:rs.getString("desarrollador");
						beneficiario 		= rs.getString("beneficiario")			== null?"0"	:rs.getString("beneficiario");
						cgPymeEpoInterno 	= rs.getString("cg_pyme_epo_interno")	== null?""	:rs.getString("cg_pyme_epo_interno");
					} else if(sOrigen.equals("EPOS") && sNoEstatus.equals("4")) {
						ic_if 				= rs.getString("ic_if")					== null?"0":rs.getString("ic_if");
						cgRazonSocialIF	= rs.getString("cg_razon_social")	== null?"0":rs.getString("cg_razon_social");
						cgRfcIF				= rs.getString("cg_rfc")				== null?"0":rs.getString("cg_rfc");
					}

					igNumeroDocto				= rs.getString("ig_numero_docto")	== null?"":rs.getString("ig_numero_docto");
					ctReferencia				= rs.getString("ct_referencia")		== null?"":rs.getString("ct_referencia");
					dfFechaDocto				= rs.getString("df_fecha_docto")		== null?"":rs.getString("df_fecha_docto");
					cgCuenta						= rs.getString("cg_cuenta")			== null?"":rs.getString("cg_cuenta");
					cgDigitoIdentificador	= getDigitoIdentificador(cgCuenta);
					dfFechaVenc					= rs.getString("df_fecha_venc")		== null?"":rs.getString("df_fecha_venc");
					cgViaLiq						= rs.getString("cg_via_liq")			== null?"":rs.getString("cg_via_liq");
					icDocumento					= rs.getString("ic_documento")		== null?"":rs.getString("ic_documento");
					numeroFactura 				= rs.getString("numero_factura")		== null?"":rs.getString("numero_factura");

					queri =
						" INSERT INTO int_flujo_fondos"   +
						"             (ic_flujo_fondos, fn_importe, ig_total_documentos, df_fecha_venc,"   +
						"              ic_epo, ic_moneda, ic_if, ic_pyme, ic_cuenta, ic_estatus_docto,"   +
						"              df_operacion)"   +
						"      VALUES (?, ?, ?, TO_DATE (?, 'dd/mm/yyyy'),"   +
						"              ?, ?, ?, ?, ?, ?,"   +
						"              TO_DATE (?, 'dd/mm/yyyy'))"  ;
					ps = con.queryPrecompilado(queri);
					System.out.println("---> folioFF = <"+folioFF+">");//Debug info
					ps.setLong(		1, folioFF);
					ps.setDouble(	2, importe);
					ps.setInt(		3, totDoctos);

					if(sFechaVenc.equals(""))
						ps.setNull(4, Types.VARCHAR);
					else
						ps.setString(4, rs.getString("df_fecha_venc"));

					ps.setInt(5, Integer.parseInt(sNoEPO.trim()));
					ps.setInt(6, Integer.parseInt(sNoMoneda.trim()));

					if( sOrigen.equals("EPOS") && ( sNoEstatus.equals("9") || sNoEstatus.equals("10") ) ) {

						if(rs.getInt("desarrollador")!=0)
							ps.setInt(8, rs.getInt("desarrollador"));
						else
							ps.setNull(8, Types.INTEGER);

						if(rs.getInt("beneficiario")!=0)
							ps.setInt(7,rs.getInt("beneficiario"));
						else
							ps.setNull(7, Types.INTEGER);

					} else {

						if(sOrigen.equals("EPOS") && (sNoEstatus.equals("4")))
							sNoIF = rs.getString("ic_if");

						if(sNoIF.equals(""))
							ps.setNull(7, Types.INTEGER);
						else
							ps.setInt(7, Integer.parseInt(sNoIF.trim()));

						if(rs.getString("ic_pyme")==null)
							ps.setNull(8, Types.INTEGER);
						else
							ps.setInt(8, rs.getInt("ic_pyme"));
					}

					if(rs.getString("ic_cuenta")==null)
						ps.setNull(9, Types.INTEGER);
					else
						ps.setLong(9, rs.getLong("ic_cuenta"));


					if(sNoEstatus.equals("")||sNoEstatus.equals("PA"))
						ps.setNull(10, Types.INTEGER);
					else
						ps.setInt(10, Integer.parseInt(sNoEstatus.trim()));

					if(sFechaOperacion.equals(""))
						ps.setNull(11, Types.VARCHAR);
					else
						ps.setString(11, rs.getString("df_operacion"));

					iNoAfecFF += ps.executeUpdate();
					ps.clearParameters();

					queri =
						" INSERT INTO cfe_m_encabezado "   +
						"             (idoperacion_cen_i, idsistema_cen_i, status_cen_i, error_cen_s,"   +
						"              historia_cen_s, idoperef_cen_i)"   +
						"      VALUES (?, ?, ?, ?,"   +
						"              ?, ?)"  ;

					ps = con.queryPrecompilado(queri);
					ps.setLong(	1, folioFF);
					ps.setInt(	2, 200);
					ps.setInt(	3, estatusoper); // 0 cuenta valida, -1 cuenta invalida
					ps.setNull(	4, Types.VARCHAR);
					ps.setNull(	5, Types.VARCHAR);
					ps.setNull(	6, Types.INTEGER);
					iNoAfecEnca += ps.executeUpdate();
					ps.clearParameters();

					//setRelacionDoctosFF(sOrigen, sNoEPO, sNoEstatus, sFechaOperacion, sFechaVenc, sNoIF, desarrolador, beneficiario, ic_if, ic_pyme, folioFF, importe, totDoctos, sNoMoneda, con);
					setRelacionDoctosFF("EPOS_CADENA_NAFIN", sNoEPO, sNoEstatus, sFechaOperacion, dfFechaVenc, sNoIF, desarrolador, beneficiario, ic_if, ic_pyme, folioFF, importe, totDoctos, sNoMoneda, con, icDocumento, "");

					String param2IdSistema 			= "200";
					String param3AreaCaptura 		= "EF15";
					String param5TipoOper 			= (esDocumentoOperado?"SPEI-05":("SPEI".equals(cgViaLiq)?"SPEI-05":cgViaLiq));
					String param8CveCteOrden 		= "37135";
					String param9RFCCteOrden 		= "NFI3406305TO";
					String param10NomCteOrden 		= "NACIONAL FINANCIERA, S.N.C.";
					String param11DomCteOrden 		= "INSURGENTES SUR 1971 COL. GUADALUPE INN DEL. ALVARO OBREGON C.P. 01020";
					String param12Banco 				= "37135";
					String param13Plaza 				= "1001";
					String param14Sucursal 			= "25";
					String param15NoCta 				= (esDocumentoOperado?"SPEI":cgViaLiq);
					String param16y27TipoCta 		= "CH";
					String param17y28TipoDivisa 	= "MXN";

					cs = con.ejecutaSP("SP_DETALLES_FF(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
					cs.setString( 1, fechaSigDiaHabil);
					cs.setString( 2, param2IdSistema);
					cs.setString( 3, param3AreaCaptura);
					cs.setString( 4, param5TipoOper);
					cs.setString( 5, param8CveCteOrden);
					cs.setString( 6, param9RFCCteOrden);
					cs.setString( 7, param10NomCteOrden);
					cs.setString( 8, param11DomCteOrden);
					cs.setString( 9, param12Banco);
					cs.setString(10, param13Plaza);
					cs.setString(11, param14Sucursal);
					cs.setString(12, param15NoCta);
					cs.setString(13, param16y27TipoCta);
					cs.setString(14, param17y28TipoDivisa);
					//cs.setString(15, sOrigen);
					cs.setString(15, (esDocumentoOperado?("EPOS_CADENA_NAFIN_OPERADO"+"|"+ctReferencia+"@"+numeroFactura):("EPOS_CADENA_NAFIN"+"|"+ctReferencia+"@"+numeroFactura)));
					cs.setString(16, sRazonSocial);
					cs.execute();
					cs.close();

					// Insertar documentos en COMTMP_DOCTOS_PAG_SIOR
					queri = " INSERT INTO COM_DOCTOS_DISP_SIOR "  +
							  "	( "  +
							  "		IC_FLUJO_FONDOS, "  +
							  "		CG_PYME_EPO_INTERNO, "  +
							  "		CG_RAZON_SOCIAL_IF, "  +
							  "		CG_RFC_IF, "  +
							  "		IG_NUMERO_DOCTO, "  +
							  "		IC_ESTATUS_DOCTO, "  +
							  "		DF_FECHA_DOCTO, "  +
							  "		DF_FECHA_VENC, "  +
							  "		IC_MONEDA, "  +
							  "		FN_MONTO, "  +
							  "		CS_DSCTO_ESPECIAL, "  +
							  "		CT_REFERENCIA, "  +
							  "		CG_DIGITO_IDENTIFICADOR "  +
							  "	)     "  +
							  " VALUES  "  +
							  "	(	   "  +
							  "		?, "  + // IC_FLUJO_FONDOS
							  "		?, "  + // CG_PYME_EPO_INTERNO
							  "		?, "  + // CG_RAZON_SOCIAL_IF
							  "		?, "  + // CG_RFC_IF
							  "		?, "  + // IG_NUMERO_DOCTO
							  "		?, "  + // IC_ESTATUS_DOCTO
							  "		to_date(?,'DD/MM/YYYY'), "  + // DF_FECHA_DOCTO
							  "		to_date(?,'DD/MM/YYYY'), "  + // DF_FECHA_VENC
							  "		?, "  + // IC_MONEDA
							  "		?, "  + // FN_MONTO
							  "		?, "  + // CS_DSCTO_ESPECIAL
							  "		?, "  + // CT_REFERENCIA
							  "		? "  + // CG_DIGITO_IDENTIFICADOR
							  "	)     ";
						ps = con.queryPrecompilado(queri);

						ps.setLong(		1, folioFF);

						if(cgPymeEpoInterno == null || cgPymeEpoInterno.equals(""))
							ps.setNull(2, Types.VARCHAR);
						else
							ps.setString(	2, cgPymeEpoInterno);

						if(cgRazonSocialIF == null || cgRazonSocialIF.equals(""))
							ps.setNull(3, Types.VARCHAR);
						else
							ps.setString(	3, cgRazonSocialIF);

						if(cgRfcIF == null || cgRfcIF.equals(""))
							ps.setNull(4, Types.VARCHAR);
						else
							ps.setString(	4, cgRfcIF);

						if(igNumeroDocto == null || igNumeroDocto.equals(""))
							ps.setNull(5, Types.VARCHAR);
						else
							ps.setString(	5, igNumeroDocto);

						if(sNoEstatus == null || sNoEstatus.trim().equals("") || sNoEstatus.equals("PA"))
							ps.setNull(6, Types.INTEGER);
						else
							ps.setInt(		6, Integer.parseInt(sNoEstatus.trim()));

						if(dfFechaDocto == null || dfFechaDocto.equals(""))
							ps.setNull(7, Types.VARCHAR);
						else
							ps.setString(	7, dfFechaDocto);

						if(dfFechaVenc == null || dfFechaVenc.equals(""))
							ps.setNull(8, Types.VARCHAR);
						else
							ps.setString(	8, dfFechaVenc);

						ps.setString(	9, sNoMoneda);

						ps.setDouble( 10, importe);

						ps.setString( 11, "N");

						if(ctReferencia == null || ctReferencia.equals(""))
							ps.setNull(12, Types.VARCHAR);
						else
							ps.setString( 12, ctReferencia);

						if(cgDigitoIdentificador == null || cgDigitoIdentificador.equals(""))
							ps.setNull(13, Types.VARCHAR);
						else
							ps.setString( 13, cgDigitoIdentificador);

						ps.executeUpdate();
						ps.clearParameters();

				}//while(rs.next()) insert FF


			} catch(SQLException sqle) {
				bOk = false;
				sqle.printStackTrace();
				throw new Exception("DISP0010");
			} catch(Exception e) {
				bOk = false;
				e.printStackTrace();
				throw e;
			}finally{
				if(rs  != null) rs.close();
				if(rs1 != null) rs1.close();
				if(ps  != null) ps.close();
				if(cs  != null) cs.close();
			}

		} catch(AppException ap) {
			bOk = false;
			throw ap;
		} catch(NafinException ne) {
			bOk = false;
			throw ne;
		} catch(Throwable e) {
			bOk = false;
			log.error("Exception en llenaEncabezadoFlujoFondosCadenaNafin(). "+e.getMessage());
			e.printStackTrace();
			throw new AppException("Error en llenaEncabezadoFlujoFondosCadenaNafin()", e);
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) con.cierraConexionDB();
			log.info("DispersionEJB::llenaEncabezadoFlujoFondosCadenaNafin(S)");
		}

	}

	/**
	 * Elimina los datos del d�a anterior de la tabla COM_DOCTOS_DISP_SIOR
	 */
	private void borrarInformacionDeComDoctosDispSIOR()
		throws AppException{

		log.info("borrarInformacionDeComDoctosDispSIOR(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		boolean			bOk				= true;

		try {

			// Obtener Fecha del dia habil Anterior
			String claveEpoCadenaNafin		= "658";
			String fechaHoy 					= Fecha.getFechaActual();
			String fechaDiaHabilAnterior 	= Fecha.getDiaHabilAnteriorPorEPO( fechaHoy, "DD/MM/YYYY", 1, claveEpoCadenaNafin );

			con.conexionDB();

			//qrySentencia.append("DELETE FROM COM_DOCTOS_DISP_SIOR");
			/*"DELETE FROM COM_DOCTOS_DISP_SIOR WHERE IC_FLUJO_FONDOS NOT IN "  +
				"	(SELECT                                "  +
				"	  ic_flujo_fondos                      "  +
				"	FROM                                   "  +
				"	  int_flujo_fondos                     "  +
				"	WHERE                                  "  +
				"	  ic_estatus_docto IN (0,18,20,30,40)  "  +
				"	UNION ALL                              "  +
				"	SELECT                                 "  +
				"	  ic_flujo_fondos                      "  +
				"	FROM                                   "  +
				"	  int_flujo_fondos_err                 "  +
				"	WHERE                                  "  +
				"	  ic_estatus_docto IN (0,18,20,30,40)) "*/

			qrySentencia.append(
				"DELETE FROM COM_DOCTOS_DISP_SIOR WHERE IC_FLUJO_FONDOS NOT IN  "  +
				"(SELECT                                                        "  +
				"	ic_flujo_fondos 															 "  +
				"FROM 																			 "  +
				"	int_flujo_fondos 															 "  +
				"WHERE 																			 "  +
				"	ic_epo = "+claveEpoCadenaNafin+" and df_registro >= to_date('"+fechaDiaHabilAnterior+"','dd/mm/yyyy') and cg_via_liquidacion = 'TEF' "  +
				"UNION ALL 																		 "  +
				" SELECT 																		 "  +
				"	ic_flujo_fondos 															 "  +
				"FROM 																			 "  +
				"	int_flujo_fondos_err                                         "  +
				"WHERE                                                          "  +
				"	ic_epo = "+claveEpoCadenaNafin+" and df_registro >= to_date('"+fechaDiaHabilAnterior+"','dd/mm/yyyy') and cg_via_liquidacion = 'TEF' )"
			);
			con.ejecutaSQL(qrySentencia.toString());

		}catch(Exception e){

			bOk = false;
			log.debug("borrarInformacionDeComDoctosDispSIOR(Exception)");
			e.printStackTrace();
			throw new AppException("Error al eliminar Informacion de la Tabla: COM_DOCTOS_DISP_SIOR ");

		}finally{
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("borrarInformacionDeComDoctosDispSIOR(S)");
		}

	}

	/**
	 *
	 * Devuelve los IDs de las EPOs que estan autorizadas para trabajar con el IF cuyo ID se
	 * proporciona en el parametro <tt>claveIF</tt>.
	 *
	 * @param claveIF Clave del IF
	 * @return <tt>List</tt> con los ID de las EPOs.
	 */
	public List getEPOSQuetTrabajanConIF(String claveIF)
		throws AppException{

			log.info("getEPOSQuetTrabajanConIF(E)");

			AccesoDB 		con 				= new AccesoDB();
			StringBuffer	qrySentencia	= new StringBuffer();
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			List				resultado		= new ArrayList();

			try {
				con.conexionDB();
				qrySentencia.append(
					 "SELECT                   "  +
					 "	ic_epo AS CLAVE_EPO     "  +
					 "FROM                     "  +
					 "	comrel_if_epo           "  +
					 "WHERE                    "  +
					 "	cs_vobo_nafin = ?   AND "  + //'S'
					 " cs_bloqueo    = ?   AND "  + //'N'
					 " ic_if         = ?       "
				);

				lVarBind.add("S");
				lVarBind.add("N");
				lVarBind.add(claveIF);

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				while(registros != null && registros.next()){
					resultado.add(registros.getString("CLAVE_EPO"));
				}

		}catch(Exception e){

			log.debug("getEPOSQuetTrabajanConIF(Exception)");
			log.debug("getEPOSQuetTrabajanConIF.claveIF = <"+claveIF+">");
			e.printStackTrace();
			throw new AppException("Error al consultar las EPOs que estan autorizadas para trabajar con el IF.");

		}finally{

			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getEPOSQuetTrabajanConIF(S)");
		}

		return resultado;
	}

	/**
	 * Genera el Estado de Cuenta de la EPO en formato txt
	 * Fabian Valenzuela R.
	 * @throws com.netro.exception.NafinException
	 * @param rutaApp
	 * @param prodNafin
	 * @param cveEpo
	 */
	public void generaEstadoCuentatxt(String cveEpo, String prodNafin, String rutaApp)throws AppException{

		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = "";
		StringBuffer contenido = new StringBuffer();

		DispersionBean dispersion = new DispersionBean();
		ArrayList lstDatosPymes = new ArrayList();
		Calendar fechaConsulta = new GregorianCalendar();
		String diaFinal = "";
		String fechaInicio = "";
		String fechaFinal = "";
		String fechaActual = "";
		String horaActual = "";
		String periodoEdoCta = "";
		double totalMontoOperado = 0;
		double totalInteresCobrado = 0;

		String rfcPyme = "";
		String nombrePyme = "";
		String calleDom = "";
		String numextDom = "";
		String numintDom = "";
		String coloniaDom = "";
		String municipio = "";
		String estado = "";
		String cpDom = "";
		String nafElec = "";

		TimeZone.setDefault(TimeZone.getTimeZone("CST"));
		String nombreMes[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

		try{


			fechaActual = new java.text.SimpleDateFormat("yyyy-MM-dd").format(fechaConsulta.getTime());
			horaActual =new java.text.SimpleDateFormat("hh:mm:ss").format(fechaConsulta.getTime());

			fechaActual = fechaActual+"T"+horaActual;
			fechaConsulta.add(Calendar.MONTH,-1);
			diaFinal = String.valueOf(fechaConsulta.getActualMaximum(Calendar.DATE));
			fechaInicio = new java.text.SimpleDateFormat("01/MM/yyyy").format(fechaConsulta.getTime());
			fechaFinal = diaFinal+"/"+ new java.text.SimpleDateFormat("MM/yyyy").format(fechaConsulta.getTime());

			nombreArchivo = ("EdoCuentaINFONAVIT_"+fechaActual).replaceAll(":","");
			periodoEdoCta = "Del 01 al "+diaFinal+" de "+nombreMes[fechaConsulta.get(Calendar.MONTH)]+" de "+fechaConsulta.get(Calendar.YEAR);

			log.info("Epo = "+cveEpo);
			log.info("Fecha inicial = "+fechaInicio);
			log.info("Fecha final = "+fechaFinal);
			lstDatosPymes = getDatosPymeProc(cveEpo,prodNafin,fechaInicio,fechaFinal);

			if(lstDatosPymes!=null && lstDatosPymes.size()>0){
				for(int i=0;i<lstDatosPymes.size();i++){
					ArrayList lstDatosPyme = (ArrayList)lstDatosPymes.get(i);
					StringBuffer contenidoDet = new StringBuffer();

					rfcPyme = (String)lstDatosPyme.get(4);
					nombrePyme = (String)lstDatosPyme.get(3);

					calleDom = (String)lstDatosPyme.get(5);
					numextDom = (String)lstDatosPyme.get(6);
					numintDom = (String)lstDatosPyme.get(7);
					coloniaDom = (String)lstDatosPyme.get(8);
					municipio = (String)lstDatosPyme.get(10);
					estado = (String)lstDatosPyme.get(11);
					cpDom = (String)lstDatosPyme.get(9);
					nafElec = (String)lstDatosPyme.get(12);


					rfcPyme = rfcPyme.replaceAll("-","");

					Vector  Registros = dispersion.getDatosFactura((String)lstDatosPyme.get(0),(String)lstDatosPyme.get(13),(String)lstDatosPyme.get(14),"1",(String)lstDatosPyme.get(2),(String)lstDatosPyme.get(1),"N");



					for(int x=0;x<Registros.size();x++){
						Vector  renglones = (Vector)Registros.get(x);
						String rs_montoOperado		= (String)renglones.get(0);
						String rs_tasa				= (String)renglones.get(1);
						String rs_interesCobrado	= (String)renglones.get(2);


						System.out.println(Comunes.formatoDecimal(rs_montoOperado, 2, false));
						System.out.println(rs_tasa);
						System.out.println(Comunes.formatoDecimal(rs_interesCobrado, 2, false));
						totalMontoOperado += Double.parseDouble(rs_montoOperado);
						totalInteresCobrado += Double.parseDouble(rs_interesCobrado);

						contenidoDet.append("07|"+Comunes.formatoDecimal(rs_tasa, 4, false)+"%|"+Comunes.formatoDecimal(rs_interesCobrado, 2, false)+"|"+Comunes.formatoDecimal(rs_montoOperado, 2, false)+"\n");
					}//for(int x=0;x<Registros.size();x++)

					String numeroEnLetra = ConvierteCadena.numeroALetra(totalInteresCobrado);
					String centavos = Comunes.formatoDecimal(new Double(totalInteresCobrado).toString(),2,false);
					int indexOfDot = centavos.indexOf('.');
					centavos = centavos.substring(indexOfDot+1,centavos.length());
					numeroEnLetra = numeroEnLetra+" PESOS "+centavos+"/100 M.N.";

					//TRAMA 1
					contenido.append("01|Estado de Cuenta|3.0|||"+fechaActual+"||||Pago en una sola exhibici�n||||"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|||"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"||ingreso|MXN||Emisor|NFI3406305T0|");
					contenido.append("NACIONAL FINANCIERA, S.N.C.|DomicilioFiscal|Avenida Insurgentes Sur|1971||Col. Guadalupe Inn|M�xico||Del. Alavaro Obregon|M�xico D.F.|");
					contenido.append("M�xico|01020|Gln|ExpedidoEn|||||||||M�xico|||Receptor|"+rfcPyme+"|"+nombrePyme+"|Domicilio|"+calleDom+"|"+numextDom+"|"+numintDom+"|");
					contenido.append(coloniaDom+"|||"+municipio+"|"+estado+"|M�xico|"+cpDom+"|\n");

					//TRAMA 2
					contenido.append("02||1|||Interes Cobrado|"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"\n");

					//Trama 3
					contenido.append("03||0.00|03.1|03.1.1|03.2|03.2.1|IVA|16.00|0.00\n");

					//Trama4
					contenido.append("04|3|"+periodoEdoCta+"|"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|"+Comunes.formatoDecimal(totalMontoOperado, 2, false)+"|Pesos||||||"+nafElec+"|||||");
					contenido.append("I.INSURGENTES SUR 1971(F�BRICA DE CR�DITO), COL.GUADALUPE INN M�XICO 01020, D.F. TEL�FONOS: 01-800-6234672 PARA EL INTERIOR DE LA REP�BLICA Y 50-89-61-07"+
						" PARA D.F. CORREO ELECTR�NICO:INFO@NAFIN.GOB.MX P�GINA DE INTERNET:WWW.NAFIN.GOB.MX;CONDUSEF:TEL�FONO 01800 999 80 80,P�GINA DE INTERNET:WWW.CONDUSEF.GOB.MX!");
					contenido.append("II.SI SU CR�DITO ES DE TASA VARIABLE, LOS INTERESES MENSUALES, A SU CARGO, QUE SE GENEREN PUEDEN CAMBIAR Y, EN SU CASO, AUMENTAR ANTE UN INCREMENTO EN LAS"+
						" TASAS DE INTER�S DE MERCADO.!");
					contenido.append("III.SI SU CR�DITO ESTA DENOMINADO EN [MONEDA EXTRANJERA] / [UDIS] SU SALDO PODR�A AUMENTAR ANTE UN INCREMENTO EN"+
						" [EL TIPO DE CAMBIO] / [LA INFLACI�N].!");
					contenido.append("IV.SI SU CR�DITO ESTA SUJETO A UNA VARIABLE DE AJUSTE A LA SUERTE PRINCIPAL, SU SALDO PUDIERA AUMENTAR ANTE"+
						" UN INCREMENTO EN EL [SALARIO M�NIMO NACIONAL] / [�NDICE NACIONAL DE PRECIOS AL CONSUMIDOR].|"+numeroEnLetra+"|\n");

					//TRAMA 5
					contenido.append("05|||||\n");

					//TRAMA 6
					contenido.append("06||||||||\n");

					//TRAMA 7
					contenido.append(contenidoDet.toString());
					contenido.append("07||"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|"+Comunes.formatoDecimal(totalMontoOperado, 2, false)+"||||||||||||||||TOTAL OPERADO\n");
					contenido.append("07||0.00|0.00||||||||||||||||IVA\n");
					contenido.append("07||"+Comunes.formatoDecimal(totalInteresCobrado, 2, false)+"|"+Comunes.formatoDecimal(totalMontoOperado, 2, false)+"||||||||||||||||GRAN TOTAL\n");

				}//cierre for(int x=0;x<lstDatosPymes.size();x++)
			}//cierre if(lstDatosPymes!=null && lstDatosPymes.size()>0)

			if(!archivo.make(contenido.toString(), rutaApp,nombreArchivo,".txt"))
				log.info("Errror al generar el archivo txt Estado de Cuenta");
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error al generar el Estado de Cuenta ");
		}
	}


	/**
	 * Se obtienen todas aquellas pymes que hayan operado documentos en un periodo de tiempo establecido
	 * Fabian Valenzuela R.
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param fechaFinal
	 * @param fechaInicio
	 * @param prodNafin
	 * @param cveEpo
	 */
	private ArrayList getDatosPymeProc(String cveEpo, String prodNafin, String fechaInicio, String fechaFinal)throws NafinException {
		AccesoDB con = new AccesoDB();
		String query = "";
		ArrayList lstDatosPymes = new ArrayList();
		ArrayList lstDatosPyme = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try{
			con.conexionDB();

			query = " SELECT   /*+ use_nl(dom edo doc sel s pym mun r cpi ci)*/ " +
					"        doc.ic_epo as cve_epo,  " +
					"        doc.ic_if as cve_if, " +
					"        doc.ic_pyme as cve_pyme,  " +
					"        pym.cg_razon_social as pyme_nombre, " +
					"        pym.cg_rfc as pyme_rfc, " +
					"        dom.cg_calle as calle_dom, " +
					"        dom.cg_numero_ext as numext_dom, " +
					"        dom.cg_numero_int as numint_dom, " +
					"        dom.cg_colonia as colonia_dom, " +
					"        dom.cn_cp as cp_dom, " +
					"        mun.cd_nombre as municipio,  " +
					"        edo.cd_nombre as estado, " +
					"		r.ic_nafin_electronico as nfe " +
					"   FROM com_documento doc, " +
					"        com_docto_seleccionado sel, " +
					"        com_solicitud s, " +
					"        comcat_pyme pym,  " +
					"        com_domicilio dom,  " +
					"        comcat_estado edo,  " +
					"        comcat_municipio mun,  " +
					"		comrel_nafin r, " +
					"        comrel_producto_if cpi, " +
					"        comcat_if ci " +
					"  WHERE doc.ic_documento = sel.ic_documento " +
					"    AND sel.ic_documento = s.ic_documento " +
					"    AND doc.ic_pyme = pym.ic_pyme " +
					"    AND pym.ic_pyme = dom.ic_pyme " +
					"    AND dom.ic_estado = edo.ic_estado " +
					"    AND edo.ic_estado = mun.ic_estado " +
					"    AND dom.ic_municipio = mun.ic_municipio " +
					"    AND doc.ic_if = cpi.ic_if  " +
					"    AND cpi.ic_if = ci.ic_if " +
					"    AND ci.ig_tipo_piso = ? " +//1
					"    AND cpi.ic_producto_nafin = ?    " +
					"    AND s.ig_numero_prestamo IS NOT NULL " +
					"    AND s.df_operacion >= TRUNC (TO_DATE (?, 'dd/mm/yyyy'))  " +
					"    AND s.df_operacion < TRUNC (TO_DATE (?, 'dd/mm/yyyy')+1) " +
					"    AND doc.ic_epo = ? " +
					//"    --AND doc.ic_if = 56 " +
					"    AND dom.cs_fiscal = ?  " +//'S'
					"	AND pym.ic_pyme = r.ic_epo_pyme_if  " +
					"    AND r.cg_tipo = ?  " +//'P'
					"  GROUP BY doc.ic_epo,  " +
					"        doc.ic_if, " +
					"        doc.ic_pyme,  " +
					"        pym.cg_razon_social, " +
					"        pym.cg_rfc, " +
					"        dom.cg_calle , " +
					"        dom.cg_numero_ext, " +
					"        dom.cg_numero_int, " +
					"        dom.cg_colonia, " +
					"        dom.cn_cp, " +
					"        mun.cd_nombre,  " +
					"        edo.cd_nombre, " +
					"		r.ic_nafin_electronico ";

			ps = con.queryPrecompilado(query);
			ps.setInt(1,1);
			ps.setInt(2,Integer.parseInt(prodNafin));
			ps.setString(3,fechaInicio);
			ps.setString(4,fechaFinal);
			ps.setInt(5,Integer.parseInt(cveEpo));
			ps.setString(6,"S");
			ps.setString(7,"P");

			rs = ps.executeQuery();

			while(rs.next()) {
				lstDatosPyme = new ArrayList();
				/*00*/lstDatosPyme.add((rs.getString("cve_pyme")==null?"":rs.getString("cve_pyme")));
				/*01*/lstDatosPyme.add((rs.getString("cve_if")==null?"":rs.getString("cve_if")));
				/*02*/lstDatosPyme.add((rs.getString("cve_epo")==null?"":rs.getString("cve_epo")));
				/*03*/lstDatosPyme.add((rs.getString("pyme_nombre")==null?"":rs.getString("pyme_nombre")));
				/*04*/lstDatosPyme.add((rs.getString("pyme_rfc")==null?"":rs.getString("pyme_rfc")));

				/*05*/lstDatosPyme.add((rs.getString("calle_dom")==null?"":rs.getString("calle_dom")));
				/*06*/lstDatosPyme.add((rs.getString("numext_dom")==null?"":rs.getString("numext_dom")));
				/*07*/lstDatosPyme.add((rs.getString("numint_dom")==null?"":rs.getString("numint_dom")));
				/*08*/lstDatosPyme.add((rs.getString("colonia_dom")==null?"":rs.getString("colonia_dom")));
				/*09*/lstDatosPyme.add((rs.getString("cp_dom")==null?"":rs.getString("cp_dom")));
				/*10*/lstDatosPyme.add((rs.getString("municipio")==null?"":rs.getString("municipio")));
				/*11*/lstDatosPyme.add((rs.getString("estado")==null?"":rs.getString("estado")));
				/*12*/lstDatosPyme.add((rs.getString("nfe")==null?"":rs.getString("nfe")));

				/*13*/lstDatosPyme.add(fechaInicio);//4 inicio periodo
				/*14*/lstDatosPyme.add(fechaFinal);//5 fin periodo

				lstDatosPymes.add(lstDatosPyme);
			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();

		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return lstDatosPymes;
	}

	/**
	 *
	 * Especifica la Clave de la EPO en el Sistema Flujo de Fondos.
	 *	La informaci�n capturada en el nuevo campo ser� la que se tomar� para el env�o de operaciones
	 * en D�lares al sistema FFON para los mensajes SWIFT MT202 y MT103 en los campos 32 y 8.
	 *
	 * @since Fodea 001 - 2011
	 * @author jshernandez
	 *
	 * @param ic_epo 				Clave de la Epo.
	 * @param claveEpoEnFFON 	<tt>String</tt> alfanum�rico con la clave de la EPO
	 */
	public void setClaveEpoEnFFON(String ic_epo, String clave_epo_ffon)
			throws AppException {

		log.info("setClaveEpoEnFFON(E)");

		AccesoDB 			con 				= new AccesoDB();
		String 				qrySentencia 	= "";
		List 					lVarBind 		= new ArrayList();
		boolean				bOK				= true;
		int 					registros		= 0;

		try {

			con.conexionDB();

			qrySentencia =
					" SELECT COUNT (1), 'Dispersion::setClaveEpoEnFFON()' "   +
					"   FROM com_parametros_dispersion "   +
					"  WHERE ic_epo = ? "  ;
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_epo));
			registros = con.existeDB(qrySentencia, lVarBind);
			if(registros==0) {
					qrySentencia =
						" INSERT INTO com_parametros_dispersion "   +
						"             (ic_epo, ic_pyme, cg_clave_epo_ffon )"   +
						"      VALUES (?, ?, ?) "  ;
					lVarBind = new ArrayList();
					lVarBind.add(new Integer(ic_epo));
					lVarBind.add(new Integer("-1"));
					lVarBind.add(clave_epo_ffon);
					registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
			} else {

			qrySentencia =
				" UPDATE com_parametros_dispersion "   +
				"    SET CG_CLAVE_EPO_FFON = ?"   +
				"  WHERE  "   +
				"    ic_epo = ?" ;

			lVarBind = new ArrayList();
			lVarBind.add(clave_epo_ffon);
			lVarBind.add(new Integer(ic_epo));

			con.ejecutaUpdateDB(qrySentencia, lVarBind);
			}
		} catch(Exception e){

			bOK = false;
			log.debug("setClaveEpoEnFFON(Exception)"+e);
			log.debug("setClaveEpoEnFFON.ic_epo         = <"+ic_epo+">");
			log.debug("setClaveEpoEnFFON.clave_epo_ffon = <"+clave_epo_ffon+">");
			e.printStackTrace();
			throw new AppException("Error al especificar la Clave de la EPO en el Sistema de Flujo de Fondos.");

		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("setClaveEpoEnFFON(S)");
		}
	}

	/**
	 *
	 * Devuelve la Clave de la EPO en el Sistema Flujo de Fondos.
	 *	La informaci�n capturada en este campo ser� la que se tomar� para el env�o de operaciones
	 * en D�lares al sistema FFON para los mensajes SWIFT MT202 y MT103 en los campos 32 y 8.
	 *
	 * @since Fodea 001 - 2011
	 * @author jshernandez
	 *
	 * @param ic_epo Clave de la EPO.
	 * @return <tt>String</tt> con la clave de la EPO en FFON.
	 *
	 */
	public String getClaveEpoEnFFON(String ic_epo)
		throws AppException{

		log.info("getClaveEpoEnFFON(E)");

			AccesoDB 		con 				= new AccesoDB();
			String 			qrySentencia	= null;
			Registros		registros		= null;
			List 				lVarBind			= new ArrayList();
			String			resultado		= "";

			try {
				con.conexionDB();
				qrySentencia =
					"SELECT "  +
					"		CG_CLAVE_EPO_FFON AS CLAVE_EPO_FFON, "  +
					"		'Dispersion::getClaveEpoEnFFON()' "   +
					"FROM "  +
					"		com_parametros_dispersion "   +
					"WHERE 	ic_epo = ? ";

				lVarBind.add(new Integer(ic_epo));

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					resultado = registros.getString("CLAVE_EPO_FFON");
				}

		}catch(Exception e){
			log.debug("getClaveEpoEnFFON(Exception)");
			log.debug("getClaveEpoEnFFON.ic_epo = <"+ic_epo+">");
			e.printStackTrace();
			throw new AppException("Error al obtener la Clave de la EPO en el Sistema de Flujo de Fondos.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getClaveEpoEnFFON(S)");
		}

		return resultado;
	}

	/**
	 * Devuelve un HashMap con el RFC(sin guiones), Nombre y Domicilio de la EPO.
	 *
	 * @since Fodea 001 - 2011
	 * @author jshernandez
	 *
	 * @param claveEPO <tt>String</tt> con la Clave de la EPO.
	 * @return <tt>HashMap</tt> con los datos del cliente
	 *
	 */
	public HashMap getDatosClienteOrdenante(String claveEPO)
		throws AppException{

		log.info("getDatosClienteOrdenante(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();

		HashMap			resultado		= new HashMap();
		String 			rfcEpo 		   = "";
		String  			razonSocial		= "";
		String  			domicilioEpo	= "";

		try {
				con.conexionDB();
				qrySentencia.append(
					"SELECT 															"  +
					"	EPO.CG_RFC                    AS RFC_EPO, 		"  +
					"	EPO.CG_RAZON_SOCIAL           AS RAZON_SOCIAL, 	"  +
					"	DOMICILIO.CG_CALLE   || ' ' || 						"  +
					"	DOMICILIO.CG_NUMERO_EXT || ', ' || 					"  +
					"	'COL. ' || DOMICILIO.CG_COLONIA || ', ' || 		"  +
					"	ESTADO.CD_NOMBRE  || ', ' || 							"  +
					"	'C.P. ' || DOMICILIO.CN_CP    AS DOMICILIO_EPO 	"  +
					"FROM  															"  +
					"	COMCAT_EPO EPO, COM_DOMICILIO DOMICILIO, COMCAT_ESTADO ESTADO "  +
					"WHERE  															"  +
					"	EPO.IC_EPO = DOMICILIO.IC_EPO AND 					"  +
					"	DOMICILIO.IC_ESTADO = ESTADO.IC_ESTADO AND 		"  +
					"	EPO.IC_EPO = ?												"
				);

				lVarBind.add(new Integer(claveEPO));

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					rfcEpo 			= registros.getString("RFC_EPO").trim();
					razonSocial 	= registros.getString("RAZON_SOCIAL").trim();
					domicilioEpo 	= registros.getString("DOMICILIO_EPO").trim();

					// Suprimir guiones al RFC
					rfcEpo 			= rfcEpo.replaceAll("-","");
				}

				resultado.put("RFC_EPO",			rfcEpo);
				resultado.put("RAZON_SOCIAL",		razonSocial);
				resultado.put("DOMICILIO_EPO",	domicilioEpo);

		}catch(Exception e){
			log.debug("getDatosClienteOrdenante(Exception)");
			log.debug("getDatosClienteOrdenante.claveEPO = <"+claveEPO+">");
			e.printStackTrace();
			throw new AppException("Error al obtener los datos del Cliente Ordenante");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getDatosClienteOrdenante(S)");
		}

		return resultado;

	}

	public String getNombrePyme(String RFC)
		throws AppException{

		log.info("getNombrePyme(E)");

		String nombrePyme = "";
		if(RFC == null || RFC.trim().equals("")){
			log.info("getNombrePyme(S)");
			return nombrePyme;
		}

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();

		try {
				con.conexionDB();
				qrySentencia.append(
					"SELECT 										"  +
					"	CG_RAZON_SOCIAL AS NOMBRE_PYME 	"  +
					"FROM 										"  +
					"	COMCAT_PYME 							"  +
					"WHERE 										"  +
					"	CG_RFC = ?								"
				);

				lVarBind.add(RFC);

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				if(registros != null && registros.next()){
					nombrePyme = registros.getString("NOMBRE_PYME");
				}

		}catch(Exception e){

			log.error("getNombrePyme(Exception)");
			log.error("getNombrePyme.RFC = <"+RFC+">");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al consultar PYME: "+e.getMessage() );

		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getNombrePyme(S)");
		}
		return nombrePyme;
	}

	public HashMap getCatalogoMonedas()
		throws AppException{

		log.info("getCatalogoMonedas(E)");

		AccesoDB 		con 				= new AccesoDB();
		StringBuffer	qrySentencia	= new StringBuffer();
		Registros		registros		= null;
		List 				lVarBind			= new ArrayList();
		HashMap			catalogo			= new HashMap();

		try {
				con.conexionDB();
				qrySentencia.append(
					"SELECT                      "  +
					"	IC_MONEDA AS CLAVE,       "  +
					"	CD_NOMBRE AS DESCRIPCION  "  +
					"FROM                        "  +
					"	COMCAT_MONEDA             "  +
					"WHERE                       "  +
					"	IC_MONEDA IN (?,?)        "
				);

				lVarBind.add("1");
				lVarBind.add("54");

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);
				while(registros != null && registros.next()){
					String clave        = registros.getString("CLAVE");
					String descripcion  = registros.getString("DESCRIPCION");
					catalogo.put(clave,descripcion);
				}

		}catch(Exception e){

			log.error("getCatalogoMonedas(Exception)");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al consultar catalogo de monedas: " + e.getMessage() );

		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getCatalogoMonedas(S)");
		}
		return catalogo;
	}

	public String getNombreMoneda(String cveMoneda,HashMap catalogoMoneda)
		throws AppException {

		log.info("getNombreMoneda(E)");

		String nombreMoneda = "";
		if(cveMoneda == null || cveMoneda.trim().equals("")){
			log.info("getNombreMoneda(S)");
			return nombreMoneda;
		}

		if(catalogoMoneda == null){
			log.info("getNombreMoneda(S)");
			return nombreMoneda;
		}

		nombreMoneda = (String) catalogoMoneda.get(cveMoneda);
		nombreMoneda = nombreMoneda == null?"":nombreMoneda;

		log.info("getNombreMoneda(S)");

		return nombreMoneda;

	}


	/**
	 * Se encarga de ordenar la informaci�n obtenida en la Consulta de Movimientos(Consulta Dispersion EPOs)
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param registrosUSD
	 * @param registrosMN
	 */
	public ArrayList ordenaRegistros(Vector registrosMN, Vector registrosUSD) throws AppException{
		log.info("DispersionBean::ordenaRegistros(E)");

		ArrayList lista = new ArrayList();
		boolean hayRegistrosMN 	= (registrosMN 	== null)?false:true;
		boolean hayRegistrosUSD = (registrosUSD 	== null)?false:true;

		HashMap documentosOperadosMN 	= new HashMap();
		HashMap documentosOperadosUSD = new HashMap();

		// Crear arreglo con lista de los proveedores
		TreeSet intermediarios = new TreeSet();

		try{

			if(hayRegistrosMN){
				for(int i=0;i<registrosMN.size();i++){
					Vector registro = (Vector) registrosMN.get(i);
					intermediarios.add((String) registro.get(0));
				}
			}
			if(hayRegistrosUSD){
				for(int i=0;i<registrosUSD.size();i++){
					Vector registro = (Vector) registrosUSD.get(i);
					intermediarios.add((String) registro.get(0));
				}
			}

			// Construir HashMap de Registros de Moneda  Nacional
			if(hayRegistrosMN){
				for(int i=0;i<registrosMN.size();i++){
					Vector registro = (Vector) registrosMN.get(i);
					String nombre_intermediario 	= (String) registro.get(0);
					String numero 						= (String) registro.get(1);
					String monto						= (String) registro.get(2);

					HashMap registroDocto = new HashMap();
					registroDocto.put("NUMERO",numero);
					registroDocto.put("MONTO",monto);

					documentosOperadosMN.put(nombre_intermediario,registroDocto);
				}
			}
			// Construir HashMap de Registros de Dolares Americanos
			if(hayRegistrosUSD){
				for(int i=0;i<registrosUSD.size();i++){
					Vector registro = (Vector) registrosUSD.get(i);
					String nombre_intermediario 	= (String) registro.get(0);
					String numero 						= (String) registro.get(1);
					String monto						= (String) registro.get(2);

					HashMap registroDocto = new HashMap();
					registroDocto.put("NUMERO",numero);
					registroDocto.put("MONTO",	monto);

					documentosOperadosUSD.put(nombre_intermediario,registroDocto);
				}
			}

			// Construir list ordenada con los HashMaps anteriores
			Iterator iterator 				= intermediarios.iterator();
			String 	nombre_intermediario	= "";
			HashMap 	registro					= null;
			String   monto_mn 				= "";
			String   numero_mn 				= "";
			String 	monto_usd 				= "";
			String   numero_usd 				= "";
			HashMap  montos					= null;
			String   hayDocumentosMN		= "";
			String   hayDocumentosUSD		= "";
			while (iterator.hasNext()){
				nombre_intermediario = (String) iterator.next();

				monto_mn 			= null;
				numero_mn 			= null;
				hayDocumentosMN 	= "false";
				if(hayRegistrosMN){
					montos 		= (HashMap) documentosOperadosMN.get(nombre_intermediario);
					if(montos != null){
						hayDocumentosMN = "true";
						monto_mn 	= (String) montos.get("MONTO");
						numero_mn 	= (String) montos.get("NUMERO");

						registro 	= new HashMap();
						registro.put("INTERMEDIARIO",			nombre_intermediario);
						registro.put("MONTO",				monto_mn);
						registro.put("NUMERO",				numero_mn);
						registro.put("MONEDA",				"1");
						/*registro.put("HAY_DOCUMENTOS_MN",	hayDocumentosMN);
						registro.put("MONTO_USD",				monto_usd);
						registro.put("NUMERO_USD",				numero_usd);
						registro.put("HAY_DOCUMENTOS_USD",	hayDocumentosUSD);*/

						lista.add(registro);
					}


				}

				monto_usd 			= null;
				numero_usd 			= null;
				hayDocumentosUSD	= "false";
				if(hayRegistrosUSD){
					montos 		= (HashMap) documentosOperadosUSD.get(nombre_intermediario);
					if(montos != null){
						hayDocumentosUSD	= "true";
						monto_usd 			= (String) montos.get("MONTO");
						numero_usd 			= (String) montos.get("NUMERO");

						registro 	= new HashMap();
						registro.put("INTERMEDIARIO",			nombre_intermediario);
						registro.put("MONTO",				monto_usd);
						registro.put("NUMERO",				numero_usd);
						registro.put("MONEDA",				"54");
						/*registro.put("HAY_DOCUMENTOS_MN",	hayDocumentosMN);
						registro.put("MONTO_USD",				monto_usd);
						registro.put("NUMERO_USD",				numero_usd);
						registro.put("HAY_DOCUMENTOS_USD",	hayDocumentosUSD);*/
					}

					lista.add(registro);
				}


			}

			return lista;
		}catch(Throwable t){
			throw new AppException("Error al ordenar la informacion de la consulta de Movimientos",t);
		}finally{
			log.info("DispersionBean::ordenaRegistros(S)");
		}
	}


	/**
	 * Metodo que obtiene los totales de Doctos No Operados en MN.
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param valorIvaT
	 * @param tarifaC
	 * @param tarifaB
	 * @param tarifaA
	 * @param documentosNoOperados
	 */
	public List getOperDispEposDoctosNoOperJS(Vector documentosNoOperados, double tarifaA, double tarifaB, double tarifaC, double valorIva, double valorIvaT, String pantalla ) throws AppException{
		log.info("getOperDispEposDoctosNoOperJS(E)");
		double  		txt_tarifa[] 		= new double[3];
		List lstInfoTotal = new ArrayList();
		List lstDoctoNoOper = new ArrayList();

		txt_tarifa[0] = tarifaA;
		txt_tarifa[1] = tarifaB;
		txt_tarifa[2] = tarifaC;

		try{
			if(documentosNoOperados != null && documentosNoOperados.size()>0) {

				String 		fechaVencimiento	= "";
				int 			iNoPymePV1			= 0;
				int			iNoPymePV2			= 0;
				int			iNoPymePV3			= 0;
				int			iTotPymePV1 		= 0;
				int			iTotPymePV2 		= 0;
				int			iTotPymePV3 		= 0;
				int			iSumTotPymes 		= 0;
				BigDecimal 	bMtoPV1 				= new BigDecimal(0);
				BigDecimal 	bMtoTotPV1 			= new BigDecimal(0);
				BigDecimal 	bMtoPV2 				= new BigDecimal(0);
				BigDecimal 	bMtoTotPV2 			= new BigDecimal(0);
				BigDecimal 	bMtoPV3 				= new BigDecimal(0);
				BigDecimal 	bMtoTotPV3 			= new BigDecimal(0);
				BigDecimal 	bSumTotMtoH 		= new BigDecimal("0");

				//out.println(vVencSinOperSumRes);
				for (int i=0; i<documentosNoOperados.size(); i++) {
					HashMap mpDoctoNoOper = new HashMap();
					Vector registros = (Vector)documentosNoOperados.get(i);
					fechaVencimiento = registros.get(0).toString();

					iNoPymePV1 	= 0;
					iNoPymePV2 	= 0;
					iNoPymePV3 	= 0;
					bMtoPV1 		= new BigDecimal(0);
					bMtoPV2 		= new BigDecimal(0);
					bMtoPV3 		= new BigDecimal(0);

					for(int n=1;n<registros.size(); n++) {

						int iGrupo = Integer.parseInt(((Vector)registros.get(n)).get(2).toString());

						if(iGrupo == 1) {
							iNoPymePV1 	= Integer.parseInt(((Vector)registros.get(n)).get(0).toString());
							bMtoPV1 		= new BigDecimal(((Vector)registros.get(n)).get(1).toString());
							iTotPymePV1 += iNoPymePV1;
							bMtoTotPV1 	= bMtoTotPV1.add(bMtoPV1);
						}
						if(iGrupo == 2) {
							iNoPymePV2	= Integer.parseInt(((Vector)registros.get(n)).get(0).toString());
							bMtoPV2 		= new BigDecimal(((Vector)registros.get(n)).get(1).toString());
							iTotPymePV2 += iNoPymePV2;
							bMtoTotPV2 	= bMtoTotPV2.add(bMtoPV2);
						}
						if(iGrupo == 3) {
							iNoPymePV3	= Integer.parseInt(((Vector)registros.get(n)).get(0).toString());
							bMtoPV3 		= new BigDecimal(((Vector)registros.get(n)).get(1).toString());
							iTotPymePV3 += iNoPymePV3;
							bMtoTotPV3 	= bMtoTotPV3.add(bMtoPV3);
						}
					}

					int iTotPymes 	= iNoPymePV1 + iNoPymePV2 + iNoPymePV3;
					BigDecimal 	bSumTotMtoV = new BigDecimal("0");
					bSumTotMtoV = bSumTotMtoV.add(bMtoPV1).add(bMtoPV2).add(bMtoPV3);

					//texto de tablas de despliegue
					mpDoctoNoOper.put("fechaVencimiento", fechaVencimiento);
					mpDoctoNoOper.put("iNoPymePV1", String.valueOf(iNoPymePV1));
					mpDoctoNoOper.put("bMtoPV1", bMtoPV1.toPlainString());

					mpDoctoNoOper.put("iNoPymePV2", String.valueOf(iNoPymePV2));
					mpDoctoNoOper.put("bMtoPV2", bMtoPV2.toPlainString());

					mpDoctoNoOper.put("iNoPymePV3", String.valueOf(iNoPymePV3));
					mpDoctoNoOper.put("bMtoPV3", bMtoPV3.toPlainString());

					mpDoctoNoOper.put("iTotPymes", String.valueOf(iTotPymes));
					mpDoctoNoOper.put("bSumTotMtoV", bSumTotMtoV.toPlainString());

					lstDoctoNoOper.add(mpDoctoNoOper);
				}

				lstInfoTotal.add(lstDoctoNoOper);

				iSumTotPymes = iTotPymePV1 + iTotPymePV2 + iTotPymePV3;
				bSumTotMtoH = bSumTotMtoH.add(bMtoTotPV1).add(bMtoTotPV2).add(bMtoTotPV3);

				List lstTotalesMN = new ArrayList();
				HashMap mpBaseTotales = new HashMap();
				mpBaseTotales.put("titulo","");
				mpBaseTotales.put("iTotPymePV1","");
				mpBaseTotales.put("bMtoTotPV1","");
				mpBaseTotales.put("iTotPymePV2", "");
				mpBaseTotales.put("bMtoTotPV2", "");
				mpBaseTotales.put("iTotPymePV3", "");
				mpBaseTotales.put("bMtoTotPV3", "");
				mpBaseTotales.put("iSumTotPymes", "");
				mpBaseTotales.put("bSumTotMtoH", "");


				HashMap mpTotales = (HashMap)mpBaseTotales.clone();
				mpTotales.put("titulo","Totales:");
				mpTotales.put("iTotPymePV1",String.valueOf(iTotPymePV1));
				mpTotales.put("bMtoTotPV1",bMtoTotPV1.toPlainString());

				mpTotales.put("iTotPymePV2", String.valueOf(iTotPymePV2));
				mpTotales.put("bMtoTotPV2", bMtoTotPV2.toPlainString());

				mpTotales.put("iTotPymePV3", String.valueOf(iTotPymePV3));
				mpTotales.put("bMtoTotPV3", bMtoTotPV3.toPlainString());

				mpTotales.put("iSumTotPymes", String.valueOf(iSumTotPymes));
				mpTotales.put("bSumTotMtoH", bSumTotMtoH.toPlainString());

				lstTotalesMN.add(mpTotales);

				HashMap mpDataExtra = (HashMap)mpBaseTotales.clone();
				txt_tarifa[0] *= iTotPymePV1;
				txt_tarifa[1] *= iTotPymePV2;
				txt_tarifa[2] *= iTotPymePV3;

				mpDataExtra.put("titulo","Tarifas:");
				mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal(txt_tarifa[0], 2));
				mpDataExtra.put("iTotPymePV2",Comunes.formatoDecimal(txt_tarifa[1], 2));
				mpDataExtra.put("iTotPymePV3",Comunes.formatoDecimal(txt_tarifa[2], 2));
				lstTotalesMN.add(mpDataExtra);

				mpDataExtra = (HashMap)mpBaseTotales.clone();
				mpDataExtra.put("titulo","Sub Total:");
				mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal((txt_tarifa[0]+txt_tarifa[1]+txt_tarifa[2])*1, 2));
				lstTotalesMN.add(mpDataExtra);

				mpDataExtra = (HashMap)mpBaseTotales.clone();
				mpDataExtra.put("titulo","I.V.A.:");

				if("consulta".equals(pantalla)){
					mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal((txt_tarifa[0]+txt_tarifa[1]+txt_tarifa[2])*0.15, 2));
				}else if("autorizacion".equals(pantalla)){
					mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal((txt_tarifa[0]+txt_tarifa[1]+txt_tarifa[2])*valorIva, 2));
				}


				lstTotalesMN.add(mpDataExtra);

				mpDataExtra = (HashMap)mpBaseTotales.clone();
				mpDataExtra.put("titulo","Monto Comisi�n:");
				mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal((txt_tarifa[0]+txt_tarifa[1]+txt_tarifa[2])*valorIvaT, 2));
				lstTotalesMN.add(mpDataExtra);

				lstInfoTotal.add(lstTotalesMN);
			}

			return lstInfoTotal;
		}catch(Throwable t){
			throw new AppException("Error en getOperDispEposDoctosNoOperJS()", t);
		}finally{
			log.info("getOperDispEposDoctosNoOperJS(S)");
		}
	}

	/**
	 * Metodo que obtiene los totales de Doctos No Operados en MN.
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param valorIvaT
	 * @param tarifaC
	 * @param tarifaB
	 * @param tarifaA
	 * @param documentosNoOperados
	 */
	public List getOperDispEposDoctosNoOperUSDJS(Vector documentosNoOperadosUSD, double tarifaAUSD, double tarifaBUSD, double tarifaCUSD,double valorIva, double valorIvaT, String pantalla ) throws AppException{
		log.info("getOperDispEposDoctosNoOperUSDJS(E)");
		List lstInfoTotal = new ArrayList();
		List lstDoctoNoOperUSD = new ArrayList();
		double  		txt_tarifaUSD[] 		= new double[3];

		txt_tarifaUSD[0] = tarifaAUSD;
		txt_tarifaUSD[1] = tarifaBUSD;
		txt_tarifaUSD[2] = tarifaCUSD;

		try{

			if(documentosNoOperadosUSD != null && documentosNoOperadosUSD.size()>0) {

				String 		fechaVencimiento	= "";
				int 			iNoPymePV1			= 0;
				int			iNoPymePV2			= 0;
				int			iNoPymePV3			= 0;
				int			iTotPymePV1 		= 0;
				int			iTotPymePV2 		= 0;
				int			iTotPymePV3 		= 0;
				int			iSumTotPymes 		= 0;
				BigDecimal 	bMtoPV1 				= new BigDecimal(0);
				BigDecimal 	bMtoTotPV1 			= new BigDecimal(0);
				BigDecimal 	bMtoPV2 				= new BigDecimal(0);
				BigDecimal 	bMtoTotPV2 			= new BigDecimal(0);
				BigDecimal 	bMtoPV3 				= new BigDecimal(0);
				BigDecimal 	bMtoTotPV3 			= new BigDecimal(0);
				BigDecimal 	bSumTotMtoH 		= new BigDecimal("0");


				//out.println(vVencSinOperSumRes);
				for (int i=0; i<documentosNoOperadosUSD.size(); i++) {
					HashMap mpDoctoNoOperUSD = new HashMap();
					Vector registros = (Vector)documentosNoOperadosUSD.get(i);
					fechaVencimiento = registros.get(0).toString();

					iNoPymePV1 	= 0;
					iNoPymePV2 	= 0;
					iNoPymePV3 	= 0;
					bMtoPV1 		= new BigDecimal(0);
					bMtoPV2 		= new BigDecimal(0);
					bMtoPV3 		= new BigDecimal(0);

					for(int n=1;n<registros.size(); n++) {

						int iGrupo = Integer.parseInt(((Vector)registros.get(n)).get(2).toString());

						if(iGrupo == 1) {
							iNoPymePV1 	= Integer.parseInt(((Vector)registros.get(n)).get(0).toString());
							bMtoPV1 		= new BigDecimal(((Vector)registros.get(n)).get(1).toString());
							iTotPymePV1 += iNoPymePV1;
							bMtoTotPV1 	= bMtoTotPV1.add(bMtoPV1);
						}
						if(iGrupo == 2) {
							iNoPymePV2	= Integer.parseInt(((Vector)registros.get(n)).get(0).toString());
							bMtoPV2 		= new BigDecimal(((Vector)registros.get(n)).get(1).toString());
							iTotPymePV2 += iNoPymePV2;
							bMtoTotPV2 	= bMtoTotPV2.add(bMtoPV2);
						}
						if(iGrupo == 3) {
							iNoPymePV3	= Integer.parseInt(((Vector)registros.get(n)).get(0).toString());
							bMtoPV3 		= new BigDecimal(((Vector)registros.get(n)).get(1).toString());
							iTotPymePV3 += iNoPymePV3;
							bMtoTotPV3 	= bMtoTotPV3.add(bMtoPV3);
						}
					}



					int 			iTotPymes 	= iNoPymePV1 + iNoPymePV2 + iNoPymePV3;
					BigDecimal 	bSumTotMtoV = new BigDecimal("0");
					bSumTotMtoV = bSumTotMtoV.add(bMtoPV1).add(bMtoPV2).add(bMtoPV3);

					//DATOS DE LA TABLA DE DOCTOS NO OPERADOS USD
					mpDoctoNoOperUSD.put("fechaVencimiento", fechaVencimiento);
					mpDoctoNoOperUSD.put("iNoPymePV1", String.valueOf(iNoPymePV1));
					mpDoctoNoOperUSD.put("bMtoPV1", bMtoPV1.toPlainString());

					mpDoctoNoOperUSD.put("iNoPymePV2", String.valueOf(iNoPymePV2));
					mpDoctoNoOperUSD.put("bMtoPV2", bMtoPV2.toPlainString());

					mpDoctoNoOperUSD.put("iNoPymePV3", String.valueOf(iNoPymePV3));
					mpDoctoNoOperUSD.put("bMtoPV3", bMtoPV3.toPlainString());

					mpDoctoNoOperUSD.put("iTotPymes", String.valueOf(iTotPymes));
					mpDoctoNoOperUSD.put("bSumTotMtoV", bSumTotMtoV.toPlainString());

					lstDoctoNoOperUSD.add(mpDoctoNoOperUSD);

				}

				lstInfoTotal.add(lstDoctoNoOperUSD);
				//INFO DESPLEGADA EN LAS TABLAS (GRID) - INI

				iSumTotPymes = iTotPymePV1 + iTotPymePV2 + iTotPymePV3;
				bSumTotMtoH = bSumTotMtoH.add(bMtoTotPV1).add(bMtoTotPV2).add(bMtoTotPV3);

				List lstTotalesUSD = new ArrayList();
				HashMap mpBaseTotales = new HashMap();
				mpBaseTotales.put("titulo","");
				mpBaseTotales.put("iTotPymePV1","");
				mpBaseTotales.put("bMtoTotPV1","");
				mpBaseTotales.put("iTotPymePV2", "");
				mpBaseTotales.put("bMtoTotPV2", "");
				mpBaseTotales.put("iTotPymePV3", "");
				mpBaseTotales.put("bMtoTotPV3", "");
				mpBaseTotales.put("iSumTotPymes", "");
				mpBaseTotales.put("bSumTotMtoH", "");

				//TOTALES DE LA TABLA DE DOCTOS NO OPERADOS USD
				HashMap mpTotales = (HashMap)mpBaseTotales.clone();
				mpTotales.put("titulo","Totales:");
				mpTotales.put("iTotPymePV1",String.valueOf(iTotPymePV1));
				mpTotales.put("bMtoTotPV1",bMtoTotPV1.toPlainString());

				mpTotales.put("iTotPymePV2", String.valueOf(iTotPymePV2));
				mpTotales.put("bMtoTotPV2", bMtoTotPV2.toPlainString());

				mpTotales.put("iTotPymePV3", String.valueOf(iTotPymePV3));
				mpTotales.put("bMtoTotPV3", bMtoTotPV3.toPlainString());

				mpTotales.put("iSumTotPymes", String.valueOf(iSumTotPymes));
				mpTotales.put("bSumTotMtoH", bSumTotMtoH.toPlainString());

				lstTotalesUSD.add(mpTotales);

				//DATOS EXTRA DE LA TABLA DE DOCTOS NO OPERADOS USD
				HashMap mpDataExtra = (HashMap)mpBaseTotales.clone();
				txt_tarifaUSD[0] *= iTotPymePV1;
				txt_tarifaUSD[1] *= iTotPymePV2;
				txt_tarifaUSD[2] *= iTotPymePV3;

				mpDataExtra.put("titulo","Tarifas:");
				mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal(txt_tarifaUSD[0], 2));
				mpDataExtra.put("iTotPymePV2",Comunes.formatoDecimal(txt_tarifaUSD[1], 2));
				mpDataExtra.put("iTotPymePV3",Comunes.formatoDecimal(txt_tarifaUSD[2], 2));
				lstTotalesUSD.add(mpDataExtra);

				mpDataExtra = (HashMap)mpBaseTotales.clone();
				mpDataExtra.put("titulo","Sub Total:");
				mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal((txt_tarifaUSD[0]+txt_tarifaUSD[1]+txt_tarifaUSD[2])*1, 2));
				lstTotalesUSD.add(mpDataExtra);

				mpDataExtra = (HashMap)mpBaseTotales.clone();
				mpDataExtra.put("titulo","I.V.A.:");

				if("consulta".equals(pantalla)){
					mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal((txt_tarifaUSD[0]+txt_tarifaUSD[1]+txt_tarifaUSD[2])*0.15, 2));
				}else if("autorizacion".equals(pantalla)){
					mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal((txt_tarifaUSD[0]+txt_tarifaUSD[1]+txt_tarifaUSD[2])*valorIva, 2));
				}

				lstTotalesUSD.add(mpDataExtra);

				mpDataExtra = (HashMap)mpBaseTotales.clone();
				mpDataExtra.put("titulo","Monto Comisi�n:");
				mpDataExtra.put("iTotPymePV1",Comunes.formatoDecimal((txt_tarifaUSD[0]+txt_tarifaUSD[1]+txt_tarifaUSD[2])*valorIvaT, 2));
				lstTotalesUSD.add(mpDataExtra);

				lstInfoTotal.add(lstTotalesUSD);
				//INFO DESPLEGADA EN LAS TABLAS (GRID) - FIN

			}

			return lstInfoTotal;
		}catch(Throwable t){
			throw new AppException("Error en getOperDispEposDoctosNoOperUSDJS()", t);
		}finally{
			log.info("getOperDispEposDoctosNoOperUSDJS(S)");
		}
	}


	/**
	 * Se encarga de obtener el detalle de los documentos dispersados
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param fechaVencimientoFin
	 * @param fechaVencimientoInicio
	 * @param claveEpo
	 */
	public List getDetalleDocumentosDispersados(String claveEpo, String fechaVencimientoInicio, String fechaVencimientoFin) throws AppException{
		log.info("DispersionBean.getDetalleDocumentosDispersados(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDoctoDisp = new ArrayList();
		String query = "";

		try{
			con.conexionDB();

			query = this.getQueryDetalleDocumentosDispersados(claveEpo, fechaVencimientoInicio, fechaVencimientoFin);
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();

			while(rs!=null && rs.next()) {
				HashMap mpData = new HashMap();
				mpData.put("NOMIF",rs.getString("nomif")==null?"N/A":rs.getString("nomif"));
				mpData.put("NOMPYME",rs.getString("nombrePyme")==null?"N/A":rs.getString("nombrePyme"));
				mpData.put("RFC",rs.getString("cg_rfc")==null?"N/A":rs.getString("cg_rfc"));
				mpData.put("FECVENC",rs.getString("fechaVenc"));
				mpData.put("TOTALDOCTOS",rs.getString("ig_total_documentos"));
				mpData.put("NOMMONEDA",rs.getString("cd_nombre"));
				mpData.put("FNIMPORTE",rs.getString("fn_importe"));
				mpData.put("NOMBANCO",rs.getString("banco"));
				mpData.put("TIPOCUENTA",rs.getString("ic_tipo_cuenta"));
				mpData.put("CUENTA",rs.getString("cg_cuenta"));
				mpData.put("ESTATUSFLUJO",rs.getString("estatusflujo"));
				mpData.put("FOLIO",rs.getString("folio"));

				lstDoctoDisp.add(mpData);
			}

			return lstDoctoDisp;

		}catch(Throwable t){
			log.info("DispersionBean.getDetalleDocumentosDispersados(Error...)");
			throw new AppException("Error al consultar el detalle de doctos dispersados", t);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();

			log.info("DispersionBean.getDetalleDocumentosDispersados(S)");
		}
	}


	/**
	 * Se encarga de obtener el detalle de los documentos no dispersados para la dispersion de EPOs
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param fechaVencimientoFin
	 * @param fechaVencimientoInicio
	 * @param claveEpo
	 */
	public List getDetalleDocumentosNoDispersados(String claveEpo, String fechaVencimientoInicio, String fechaVencimientoFin) throws AppException{
		log.info("DispersionBean.getDetalleDocumentosNoDispersados(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDoctoNoDisp = new ArrayList();
		String query = "";

		try{
			con.conexionDB();

			query = this.getQueryDetalleDocumentosNoDispersados(claveEpo, fechaVencimientoInicio, fechaVencimientoFin);
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();

			while(rs!=null && rs.next()) {
				HashMap mpData = new HashMap();
				mpData.put("NOMIF",rs.getString("nomif")==null?"N/A":rs.getString("nomif"));
				mpData.put("NOMPYME",rs.getString("nombrePyme")==null?"N/A":rs.getString("nombrePyme"));
				mpData.put("RFC",rs.getString("cg_rfc")==null?"N/A":rs.getString("cg_rfc"));
				mpData.put("FECVENC",rs.getString("fechaVenc"));
				mpData.put("TOTALDOCTOS",rs.getString("ig_total_documentos"));
				mpData.put("NOMMONEDA",rs.getString("cd_nombre"));
				mpData.put("FNIMPORTE",rs.getString("fn_importe"));
				mpData.put("NOMBANCO",rs.getString("banco"));
				mpData.put("TIPOCUENTA",rs.getString("ic_tipo_cuenta"));
				mpData.put("CUENTA",rs.getString("cg_cuenta"));


				lstDoctoNoDisp.add(mpData);
			}

			return lstDoctoNoDisp;

		}catch(Throwable t){
			log.info("DispersionBean.getDetalleDocumentosNoDispersados(Error...)");
			throw new AppException("Error al consultar el detalle de doctos dispersados", t);
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();

			log.info("DispersionBean.getDetalleDocumentosNoDispersados(S)");
		}
	}

/**
 * Proceso mediante el cual se enviar� un mensaje SMS
 * para indicar el total de Cadenas Productivas para las
 * cuales no se ha ejecutado la Interface de Flujo de Fondos.
 *
 * @throws AppException cuando ocurre un error en la consulta.
 * @author gperez
 * @author jshernandez; 04/02/2015 11:17:24 a.m. (Correcciones)
 */
	public void enviaSMSDispersionesPendientes() throws AppException{

		log.info("enviaSMSDispersionesPendientes(I) ::..");

		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		SMS sms = new SMS();
		PreparedStatement	pst1 = null;
		ResultSet rst1	= null;
		StringTokenizer tokens=null;
		String fechaVencimiento="";

		SimpleDateFormat 	sdf 				= null;
		Calendar 			cal 				= null;
		final int 			UN_DIA_HABIL 	= 1;

		String 				fechaActual 					= null;
		StringBuffer 		diasInhabilesAdicionales 	= null;

		PreparedStatement pst2 				= null;
		ResultSet 			rst2 				= null;

		String 				numeroCelular 	= null;

		boolean				notificaciones = false;

		try{

			con.conexionDB();

			// 1. Obtener fecha actual
			fechaActual 										= Fecha.getFechaActual();

			// 2. Obtener lista de d�as inhabiles del a�o que no corresponde a los fines de semana
			diasInhabilesAdicionales 						= new StringBuffer(128);
			strSQL 												= new StringBuffer();
			strSQL.append(
				"SELECT                                        "  +
				"   CG_DIA_INHABIL AS DIA_MES                  "  +
				"FROM                                          "  +
				"   COMCAT_DIA_INHABIL                         "  +
				"WHERE                                         "  +
				"   CG_DIA_INHABIL IS NOT NULL                 "  +
				"UNION                                         "  +
				"SELECT                                        "  +
				"   TO_CHAR(DF_DIA_INHABIL,'DD/MM') AS DIA_MES "  +
				"FROM                                          "  +
				"   COMCAT_DIA_INHABIL                         "  +
				"WHERE                                         "  +
				"   DF_DIA_INHABIL IS NOT NULL                 "
			);

			log.debug("..:: strSQL  : "+strSQL.toString());

			pst1 = con.queryPrecompilado(strSQL.toString());
			rst1 = pst1.executeQuery();
			while(rst1.next()){

				String diaMes = rst1.getString("DIA_MES");

				diasInhabilesAdicionales.append(diaMes);
				diasInhabilesAdicionales.append(";");

			}
			rst1.close();
			pst1.close();

			String   listaDiasInhabilesAdicionales = diasInhabilesAdicionales.toString();

			// 3. En caso de que la fecha actual corresponda a un d�a inh�bil, abortar la operaci�n
			if( !Fecha.esDiaHabil(fechaActual,"dd/MM/yyyy",listaDiasInhabilesAdicionales) ){
				log.debug("enviaSMSDispersionesPendientes: La fecha actual: " + fechaActual + " corresponde a un d�a inh�bil; se cancela el env�o de mensajes SMS.");
				return;
			}

			// 4. Obtener siguiente d�a h�bil
			sdf = new SimpleDateFormat("dd/MM/yyyy");
			sdf.setLenient(false);
			sdf.parse(fechaActual);

			cal = sdf.getCalendar();

			int 		contadorDias 						= 0;
			while( contadorDias < UN_DIA_HABIL ){
				cal.add(Calendar.DATE, 1);
				if(Fecha.esDiaHabil(sdf.format(cal.getTime()), "dd/MM/yyyy", listaDiasInhabilesAdicionales)){
					contadorDias++;
				}
			}

			fechaVencimiento 								= sdf.format(cal.getTime()); // Siguiente D�a H�bil

			// 5. Obtener n�mero de cadenas productivas que tienen pendiente una dispersi�n.
			strSQL 											= new StringBuffer();
			strSQL.append(
				"SELECT                                                               "  +
				"   COUNT(1) AS NUMERO_CADENAS                                        "  +
				"FROM (                                                               "  +
				"   SELECT                                                            "  +
				"      COUNT(1)                                                       "  +
				"   FROM                                                              "  +
				"      COM_DISPERSION_ENLISTADA_X_EPO                                 "  +
				"   WHERE                                                             "  +
				"      (                                                              "  +
				"          (                                                          "  +
				"            DF_ENVIO_FLUJO_FONDOS_OPR IS NULL                        "  +
				"          AND IG_TOTAL_DOCUMENTOS_OPR  > 0                           "  +
				"          )                                                          "  +
				"        OR                                                           "  +
				"          (                                                          "  +
				"            DF_ENVIO_FLUJO_FONDOS_SNOPR IS NULL                      "  +
				"          AND IG_TOTAL_DOCUMENTOS_SNOPR  > 0                         "  +
				"          )                                                          "  +
				"      )                                               AND            "  +
				"      DC_VENCIMIENTO >= TO_DATE(?,'DD/MM/YYYY')       AND            "  +
				"      DC_VENCIMIENTO <  TO_DATE(?,'DD/MM/YYYY') + 1                  "  +
				"   GROUP BY                                                          "  +
				"      IC_EPO                                                         "  +
				") EPOS                                                               "
			);
			log.debug("..:: strSQL  : "+strSQL.toString());
			log.debug("..:: pst1[1] : "+fechaVencimiento);
			log.debug("..:: pst1[2] : "+fechaVencimiento);

			pst1 = con.queryPrecompilado(strSQL.toString());
			pst1.setString(1,fechaVencimiento);
			pst1.setString(2,fechaVencimiento);
			rst1 = pst1.executeQuery();

			// 6. Enviar SMS si hay al menos una cadena productiva con dispersiones pendientes
			if(rst1.next()){
				Integer cuenta=new Integer(rst1.getString(1));
						if(cuenta.intValue()>0){

							/*
							 * Se obtiene los numeros a mandar en el mensaje
							 *
							 * */

							strSQL = new StringBuffer();
							varBind = new ArrayList();

							strSQL.append(" SELECT cg_valor FROM com_parametros_sms WHERE cc_param_sms = ? ");


							varBind.add("TEL_ENVIO_SMS_DISPERSION");

							log.debug("..:: strSQL : "+strSQL.toString());
							log.debug("..:: varBind : "+varBind);

							pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

							rst2 = pst2.executeQuery();

							if(rst2.next()){
								tokens = rst2.getString(1) == null?null:new StringTokenizer(rst2.getString(1),",");
							}

							rst2.close();
							pst2.close();

							/*
							 * Enviar SMS con el n�mero de cadenas productivas con dispersiones pendientes
							 */

							while(tokens != null && tokens.hasMoreTokens() ){
								numeroCelular 	= tokens.nextToken();
								sms.enviarDispersionesPendientes(numeroCelular,cuenta.toString(),fechaVencimiento);
								notificaciones = true;
							}

							if( !notificaciones ){
								log.info("enviaSMSDispersionesPendientes: No se pudo notificar v�a SMS, debido a que no hay ning�n usuario registrado.");
								log.info("enviaSMSDispersionesPendientes.fechaActual              = <" + fechaActual       + ">");
								log.info("enviaSMSDispersionesPendientes.fechaVencimiento         = <" + fechaVencimiento  + ">");
								log.info("enviaSMSDispersionesPendientes.numeroCadenasProductivas = <" + cuenta.toString() + ">");
							}

						}

			}

			rst1.close();
			pst1.close();

		}catch(Exception e){

			log.error("enviaSMSDispersionesPendientes(ERROR) ::..");
			log.error("enviaSMSDispersionesPendientes.fechaActual              = <" + fechaActual              + ">");
			log.error("enviaSMSDispersionesPendientes.diasInhabilesAdicionales = <" + diasInhabilesAdicionales + ">");
			log.error("enviaSMSDispersionesPendientes.fechaVencimiento         = <" + fechaVencimiento         + ">");
			log.error("enviaSMSDispersionesPendientes.strSQL                   = <" + strSQL                   + ">");
			log.error("enviaSMSDispersionesPendientes.numeroCelular            = <" + numeroCelular            + ">");

			e.printStackTrace();
			throw new AppException("Error al enviar SMS cadenas productivas");

		}finally{

			if(rst1 != null ) try{ rst1.close(); }catch(Exception e){}
			if(pst1 != null ) try{ pst1.close(); }catch(Exception e){}

			if(rst2 != null ) try{ rst2.close(); }catch(Exception e){}
			if(pst2 != null ) try{ pst2.close(); }catch(Exception e){}

			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}

			log.info("enviaSMSDispersionesPendientes(F) ::..");

		}

	}

	/**
	 *
	 * Revisa si el grupo de documentos identificado por <tt>claveEpo</tt>, <tt>claveMoneda</tt>,
	 * <tt>fechaVencimiento</tt> y <tt>tipoDocumento</tt>, ya fue enviado a flujo de fondos; para
	 * ello consulta el campo de fecha de envio a flujo fondos de la tabla
	 * COM_DISPERSION_ENLISTADA_X_EPO.
	 * @throws AppException
	 *
	 * @param claveEpo         <tt>String</tt> con el <tt>ID(IC_EPO)</tt> de la EPO.
	 * @param claveMoneda      <tt>String</tt> con el <tt>ID(IC_MONEDA)</tt> de la moneda.
	 * @param fechaVencimiento <tt>String</tt> con la fecha de vencimiento en formato <tt>DD/MM/AAAA</tt>.
	 * @param tipoDocumento    <tt>String</tt> con la clave de los tipos de documentos, actualmente
	 *                         solo se tiene soporte para dos grupos de documentos posibles:
	 *									<tt>'OPR'</tt> ( Operada / Operada Pagada ) y <tt>'SNOPR'</tt>
	 *                         ( Vencido sin Operar/Pagado sin Operar )
	 *
	 * @return <tt>true</tt> si el grupo de documentos ya fue enviado a flujo de fondos
	 *         y <tt>false</tt> en caso contrario.
	 *
	 * @author jshernandez
	 * @since  F012-2013; 24/05/2013 11:44:16 a.m.
	 *
	 */
	public boolean seEnvioAFlujoFondos( String claveEpo, String claveMoneda, String fechaVencimiento, String tipoDocumento )
		throws AppException {

		log.info("seEnvioAFlujoFondos(E)");

		AccesoDB 			con 				= new AccesoDB();
		boolean				enviado			= false;

		try {

			con.conexionDB();
			enviado = seEnvioAFlujoFondos( claveEpo, claveMoneda, fechaVencimiento, tipoDocumento, con );

		} catch(Exception e) {

			log.error("seEnvioAFlujoFondos(Exception)");
			log.error("seEnvioAFlujoFondos.claveEpo         = <" + claveEpo          + ">");
			log.error("seEnvioAFlujoFondos.claveMoneda      = <" + claveMoneda       + ">");
			log.error("seEnvioAFlujoFondos.fechaVencimiento = <" + fechaVencimiento  + ">");
			log.error("seEnvioAFlujoFondos.tipoDocumento    = <" + tipoDocumento     + ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al revisar si los documentos ya hab�an sido enviados a flujo de fondos.");

		} finally {

			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("seEnvioAFlujoFondos(S)");

		}

		return enviado;

	}

	/**
	 *
	 * Revisa si el grupo de documentos identificado por <tt>claveEpo</tt>, <tt>claveMoneda</tt>,
	 * <tt>fechaVencimiento</tt> y <tt>tipoDocumento</tt>, ya fue enviado a flujo de fondos; para
	 * ello consulta el campo de fecha de envio a flujo fondos de la tabla
	 * COM_DISPERSION_ENLISTADA_X_EPO.
	 * @throws AppException
	 *
	 * @param claveEpo         <tt>String</tt> con el <tt>ID(IC_EPO)</tt> de la EPO.
	 * @param claveMoneda      <tt>String</tt> con el <tt>ID(IC_MONEDA)</tt> de la moneda.
	 * @param fechaVencimiento <tt>String</tt> con la fecha de vencimiento en formato <tt>DD/MM/AAAA</tt>.
	 * @param tipoDocumento    <tt>String</tt> con la clave de los tipos de documentos, actualmente
	 *                         solo se tiene soporte para dos grupos de documentos posibles:
	 *									<tt>'OPR'</tt> ( Operada / Operada Pagada ) y <tt>'SNOPR'</tt>
	 *                         ( Vencido sin Operar/Pagado sin Operar )
	 *
	 * @return <tt>true</tt> si el grupo de documentos ya fue enviado a flujo de fondos
	 *         y <tt>false</tt> en caso contrario.
	 *
	 * @author jshernandez
	 * @since  F012-2013; 30/05/2013 01:15:03 p.m.
	 *
	 */
	private boolean seEnvioAFlujoFondos( String claveEpo, String claveMoneda, String fechaVencimiento, String tipoDocumento, AccesoDB con )
		throws AppException {

		log.info("seEnvioAFlujoFondos(E)");

		StringBuffer		query				= new StringBuffer();
		PreparedStatement ps					= null;
		ResultSet			rs					= null;
		boolean				enviado			= false;

		try {

				query.append(
					"SELECT 																	"
				);
				if( "OPR".equals(tipoDocumento) ){
					query.append(
						"	DECODE(DF_ENVIO_FLUJO_FONDOS_OPR,NULL,'false','true')   AS ENVIADO_A_FFON 	"
					);
				} else if( "SNOPR".equals(tipoDocumento) ){
					query.append(
						"	DECODE(DF_ENVIO_FLUJO_FONDOS_SNOPR,NULL,'false','true') AS ENVIADO_A_FFON 	"
					);
				}
				query.append(
					"FROM 																	" +
					"  COM_DISPERSION_ENLISTADA_X_EPO								" +
					"WHERE 																	" +
					"	IC_EPO         =  ? AND 										" +
					"	IC_MONEDA      =  ? AND											" +
					"	DC_VENCIMIENTO >= TO_DATE(?,'DD/MM/YYYY')   AND       " +
					"	DC_VENCIMIENTO <  TO_DATE(?,'DD/MM/YYYY')+1           "
				);

				ps = con.queryPrecompilado(query.toString());

				ps.setInt(1, 	 Integer.parseInt(claveEpo)    );
				ps.setInt(2, 	 Integer.parseInt(claveMoneda) );
				ps.setString(3, fechaVencimiento              );
				ps.setString(4, fechaVencimiento              );

				rs = ps.executeQuery();
				if(rs.next()){
					enviado = "true".equals( rs.getString("ENVIADO_A_FFON") )?true:false;
				}

		} catch(Exception e) {

			log.error("seEnvioAFlujoFondos(Exception)");
			log.error("seEnvioAFlujoFondos.claveEpo         = <" + claveEpo          + ">");
			log.error("seEnvioAFlujoFondos.claveMoneda      = <" + claveMoneda       + ">");
			log.error("seEnvioAFlujoFondos.fechaVencimiento = <" + fechaVencimiento  + ">");
			log.error("seEnvioAFlujoFondos.tipoDocumento    = <" + tipoDocumento     + ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al revisar si los documentos ya hab�an sido enviados a flujo de fondos.");

		} finally {

			if(rs != null) try { rs.close(); }catch(Exception e){}
			if(ps != null) try { ps.close(); }catch(Exception e){}

			log.info("seEnvioAFlujoFondos(S)");

		}

		return enviado;

	}

	/**
	 *
	 * Devuelve el Nombre de la EPO.
	 * @throws AppException
	 *
	 * @param claveMoneda ID(<tt>comcat_epo.ic_epo</tt>) de la EPO. Numero entero mayor a cero.
	 *
	 * @return <tt>String</tt> con el nombre de la EPO.
	 *
	 * @author jshernandez
	 * @since  F012-2013; 27/05/2013 03:46:31 p.m.
	 *
	 */
	public String getNombreEPO(String claveEpo)
		throws AppException{

		log.info("getNombreEPO(E)");
		String 					nombreEpo 		= null;

		AccesoDB 				con				= new AccesoDB();
		PreparedStatement		ps					= null;
		ResultSet				rs					= null;
		StringBuffer			query 			= null;

		try {

			con.conexionDB();

			query = new StringBuffer();
			query.append(
				"SELECT          	              		"  +
				"   CG_RAZON_SOCIAL AS NOMBRE_EPO  	"  +
				"FROM                           		"  +
				"   COMCAT_EPO               			"  +
				"WHERE           	              		"  +
				"   IC_EPO = ?               			"
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,Integer.parseInt(claveEpo));
			rs = ps.executeQuery();

			if(rs.next()){
				nombreEpo = rs.getString("NOMBRE_EPO");
			}
			nombreEpo = nombreEpo == null || nombreEpo.trim().equals("")?"":nombreEpo.trim();

		}catch(Exception e){

			log.error("getNombreEPO(Exception)");
			log.error("getNombreEPO.claveEpo = <" + claveEpo + ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al consultar la descripci�n de la EPO.");

		}finally{

			if(rs != null){ try { rs.close(); }catch(Exception e){} }
			if(ps != null){ try { ps.close(); }catch(Exception e){} }

			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}

			log.info("getNombreEPO(S)");

		}

		return nombreEpo;

	}


	/**
	 * Este m�todo se encarga de revisar que se hayan parametrizado los datos
	 * de la dispersion para la EPO correspondiente; tambi�n verifica si se han capturado
	 * los correos de la EPO y de NAFIN.
	 * @throws AppException
	 *
	 * @param claveEPO <tt>String</tt> con la clave de la EPO (IC_EPO) a revisar su parametrizaci�n.
	 *
	 * @return Retorna <tt>true</tt> si ya se realiz� la parametrizaci�n y <tt>false</tt>
	 *         en caso contrario.
	 *
	 * @author jshernandez
	 * @mod F012-2013; 27/05/2013 06:39:54 p.m.
	 *
	 */
	public boolean existenParametrosDeDispersion(String claveEPO)
		throws AppException {

		log.info("existenParametrosDeDispersion(E)");

		AccesoDB 		con 							= new AccesoDB();
		String 			qrySentencia				= null;
		List 				lVarBind						= new ArrayList();
		Registros		registros					= null;

		boolean			existeParametrizacion	= false;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT                                                           " +
					"		DECODE(COUNT(1),0,'false','true') AS EXISTE_PARAMETRIZACION " +
					"FROM                                                             " +
					"	COM_PARAMETROS_DISPERSION                                      " +
					"WHERE                                                            " +
					"	IC_EPO 	             = ? AND                                  " +
					"  CG_MAIL_CUENTAS_NAFIN IS NOT NULL AND                          " +
					"  CG_MAIL_CUENTAS_EPO   IS NOT NULL                              ";

				lVarBind.add(claveEPO);

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					existeParametrizacion = registros.getString("EXISTE_PARAMETRIZACION").equals("true")?true:false;
				}

		} catch(Exception e) {
			log.error("existeRegistroDeDispersion(Exception)");
			log.error("existeRegistroDeDispersion.claveEPO  = <" + claveEPO + ">");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al consultar los par�metros de dispersi�n.");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("DispersionBean::existeRegistroDeDispersion(S)");
		}

		return existeParametrizacion;
	}

	/**
	 *
	 * Revisa si en la tabla COM_DISPERSION_ENLISTADA_X_EPO, hay un registro de dispersion para la EPO,
	 * Moneda y Fecha de Vencimiento especificos.
	 * @throws AppException
	 *
	 * @param claveEpo         <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param claveMoneda      <tt>String</tt> con la clave de la Moneda (COMCAT_MONEDA.IC_MONEDA).
	 * @param fechaVencimiento <tt>String</tt> con la fecha de vencimiento del grupo de documentos.
	 * @param con				   <tt>AccesoDB</tt> con la conexion a la base de datos.
	 *
	 * @return <tt>true</tt> si el registro de dispersion ya existe y <tt>false</tt> en caso contrario.
	 *
	 * @author jshernandez
	 * @since  F012-2013; 30/05/2013 03:51:57 p.m.
	 *
	 */
	public boolean existeDispersionEnlistadaPorEPO(
		String 	claveEpo,
		String 	claveMoneda,
		String 	fechaVencimiento,
		AccesoDB con
		) throws AppException {

			log.info("existeDispersionEnlistadaPorEPO(E)");

			StringBuffer	  	query			= new StringBuffer(64);
			PreparedStatement ps 			= null;
			ResultSet		  	rs 			= null;

			boolean				existeRegistro = false;

			try {

				// Revisar si ya se tiene la dispersion enlistada en COM_DISPERSION_ENLISTADA_X_EPO
				query.append(
					"SELECT                                                    "  +
					"   DECODE( count(1),0,'false','true') AS EXISTE_REGISTRO  "  +
					"FROM                                                      "  +
					"   COM_DISPERSION_ENLISTADA_X_EPO                         "  +
					"WHERE                                                     "  +
					"   IC_EPO         =  ? AND                                "  +
					"   IC_MONEDA      =  ? AND                                "  +
					"   DC_VENCIMIENTO >= TO_DATE(?,'DD/MM/YYYY') AND          "  +
					"   DC_VENCIMIENTO <  TO_DATE(?,'DD/MM/YYYY') + 1          "
				);

				ps = con.queryPrecompilado(query.toString());
				ps.setInt(1,	 Integer.parseInt(claveEpo)		);
				ps.setInt(2,	 Integer.parseInt(claveMoneda)	);
				ps.setString(3, fechaVencimiento						);
				ps.setString(4, fechaVencimiento						);

				rs = ps.executeQuery();
				if(rs.next()){
					existeRegistro = "true".equals( rs.getString("EXISTE_REGISTRO") )?true:false;
				}

			} catch(Exception e){

				log.error("existeDispersionEnlistadaPorEPO(Exception)");
				log.error("existeDispersionEnlistadaPorEPO.claveEpo         = <" + claveEpo         + ">");
				log.error("existeDispersionEnlistadaPorEPO.claveMoneda      = <" + claveMoneda      + ">");
				log.error("existeDispersionEnlistadaPorEPO.fechaVencimiento = <" + fechaVencimiento + ">");
				log.error("existeDispersionEnlistadaPorEPO.con              = <" + con              + ">");
				log.error("existeDispersionEnlistadaPorEPO.query            = <" + query            + ">");
				e.printStackTrace();

				throw new AppException("Ocurri� un error al realizar b�squeda de registros de dispersi�n.");

			} finally {

				if(rs != null ) try{ rs.close(); }catch(Exception e){}
				if(ps != null ) try{ ps.close(); }catch(Exception e){}

				log.info("existeDispersionEnlistadaPorEPO(S)");

			}

			return existeRegistro;

	}

	/**
	 *
	 * Crea nuevo registro de dispersion en la tabla COM_DISPERSION_ENLISTADA_X_EPO caso de que no haya registro previo para
	 * el grupo IC_EPO, IC_MONEDA y DC_VENCIMIENTO.
	 * @throws AppException
	 *
	 * @param claveEpo         <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param claveMoneda      <tt>String</tt> con la clave de la Moneda (COMCAT_MONEDA.IC_MONEDA).
	 * @param fechaVencimiento <tt>String</tt> con la fecha de vencimiento del grupo de documentos.
	 * @param loginDelUsuario  <tt>String</tt> con el login del usuario.
	 * @param nombreDelUsuario <tt>String</tt> con el nombre del usuario.
	 * @param con				   <tt>AccesoDB</tt> con la conexion a la base de datos.
	 *
	 * @author jshernandez
	 * @since  F012-2013; 30/05/2013 11:47:48 a.m.
	 *
	 */
	public void registrarNuevaDispersionEnlistadaPorEPO(
		String 	claveEpo,
		String 	claveMoneda,
		String 	fechaVencimiento,
		String	loginDelUsuario,
		String	nombreDelUsuario,
		AccesoDB con
		) throws AppException {

			log.info("registrarNuevaDispersionEnlistadaPorEPO(E)");

			StringBuffer	  	sentence			= new StringBuffer(256);
			PreparedStatement 	ps 				= null;

			try {

				sentence.setLength(0);
				sentence.append(
					"INSERT INTO                                   "  +
					"   COM_DISPERSION_ENLISTADA_X_EPO             "  +
					"      (                                       "  +
					"         IC_EPO,                              "  +
					"         IC_MONEDA,                           "  +
					"         DC_VENCIMIENTO,                      "  +
					"         DF_ENVIO_CLIENTE,                    "  +
					"         IG_TOTAL_DOCUMENTOS_OPR,             "  +
					"         FN_MONTO_OPR,                        "  +
					"         CG_OBSERVACIONES_OPR,                "  +
					"         DF_ENVIO_FLUJO_FONDOS_OPR,           "  +
					"         IG_TOTAL_DOCUMENTOS_SNOPR,           "  +
					"         FN_MONTO_SNOPR,                      "  +
					"         CG_OBSERVACIONES_SNOPR,              "  +
					"         DF_ENVIO_FLUJO_FONDOS_SNOPR,         "  +
					"         IC_USUARIO,                          "  +
					"         CG_NOMBRE_USUARIO                    "  +
					"      )                                       "  +
					"   VALUES                                     "  +
					"      (                                       "  +
					"         ?,                                   "  + // IC_EPO
					"         ?,                                   "  + // IC_MONEDA
					"         TO_DATE(?,'DD/MM/YYYY'),             "  + // DC_VENCIMIENTO
					"         NULL,                                "  + // DF_ENVIO_CLIENTE
					"         0,                                   "  + // IG_TOTAL_DOCUMENTOS_OPR
					"         0,                                   "  + // FN_MONTO_OPR
					"         NULL,                                "  + // CG_OBSERVACIONES_OPR
					"         NULL,                                "  + // DF_ENVIO_FLUJO_FONDOS_OPR
					"         0,                                   "  + // IG_TOTAL_DOCUMENTOS_SNOPR
					"         0,                                   "  + // FN_MONTO_SNOPR
					"         NULL,                                "  + // CG_OBSERVACIONES_SNOPR
					"         NULL,                                "  + // DF_ENVIO_FLUJO_FONDOS_SNOPR
					"         ?,                                   "  + // IC_USUARIO
					"         ?                                    "  + // CG_NOMBRE_USUARIO
					"      )                                       "
				);

				ps = con.queryPrecompilado( sentence.toString() );

				ps.setInt(1,	 Integer.parseInt(claveEpo)		);
				ps.setInt(2,	 Integer.parseInt(claveMoneda)	);
				ps.setString(3, fechaVencimiento						);
				ps.setString(4, loginDelUsuario						);
				ps.setString(5, nombreDelUsuario						);

				ps.executeUpdate();

			} catch(Exception e){

				log.error("registrarNuevaDispersionEnlistadaPorEPO(Exception)");
				log.error("registrarNuevaDispersionEnlistadaPorEPO.claveEpo         = <" + claveEpo         + ">");
				log.error("registrarNuevaDispersionEnlistadaPorEPO.claveMoneda      = <" + claveMoneda      + ">");
				log.error("registrarNuevaDispersionEnlistadaPorEPO.fechaVencimiento = <" + fechaVencimiento + ">");
				log.error("registrarNuevaDispersionEnlistadaPorEPO.loginDelUsuario  = <" + loginDelUsuario  + ">");
				log.error("registrarNuevaDispersionEnlistadaPorEPO.nombreDelUsuario = <" + nombreDelUsuario + ">");
				log.error("registrarNuevaDispersionEnlistadaPorEPO.con              = <" + con              + ">");
				log.error("registrarNuevaDispersionEnlistadaPorEPO.sentence         = <" + sentence         + ">");
				e.printStackTrace();

				throw new AppException("Ocurri� un error al insertar registro de dispersi�n.");

			} finally {

				if(ps != null ) try{ ps.close(); }catch(Exception e){}

				log.info("registrarNuevaDispersionEnlistadaPorEPO(S)");

			}

	}

	/**
	 *
	 * Borra registro de dispersion en la tabla COM_DISPERSION_ENLISTADA_X_EPO para el grupo de documentos
	 * identificado por EPO, Moneda y Fecha de Vencimiento. Se debe cumplir que las fechas de envio a
	 * flujo de fondos sean null.
	 * @throws AppException
	 *
	 * @param claveEpo         <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param claveMoneda      <tt>String</tt> con la clave de la Moneda (COMCAT_MONEDA.IC_MONEDA).
	 * @param fechaVencimiento <tt>String</tt> con la fecha de vencimiento del grupo de documentos.
	 * @param con				   <tt>AccesoDB</tt> con la conexion a la base de datos.
	 *
	 * @author jshernandez
	 * @since  F012-2013; 30/05/2013 04:15:39 p.m.
	 *
	 */
	public void borraDispersionEnlistadaPorEPO(
		String 	claveEpo,
		String 	claveMoneda,
		String 	fechaVencimiento,
		AccesoDB con
		) throws AppException {

			log.info("borraDispersionEnlistadaPorEPO(E)");

			StringBuffer	  	sentence			= new StringBuffer(256);
			PreparedStatement ps 				= null;

			try {

				sentence.setLength(0);
				sentence.append(
					"DELETE FROM                                                                      "  +
					"   COM_DISPERSION_ENLISTADA_X_EPO                                                "  +
					"WHERE                                                                            "  +
					"   IC_EPO                      =  ?                                       AND    "  +
					"   IC_MONEDA                   =  ?                                       AND    "  +
					"   DC_VENCIMIENTO              >= TO_DATE(?,'DD/MM/YYYY')                 AND    "  +
					"   DC_VENCIMIENTO              <  TO_DATE(?,'DD/MM/YYYY') + 1             AND    "  +
					"   DF_ENVIO_FLUJO_FONDOS_OPR   IS NULL                                    AND    "  +
					"   DF_ENVIO_FLUJO_FONDOS_SNOPR IS NULL                                           "
				);

				ps = con.queryPrecompilado( sentence.toString() );

				ps.setInt(1,	 Integer.parseInt(claveEpo)		);
				ps.setInt(2,	 Integer.parseInt(claveMoneda)	);
				ps.setString(3, fechaVencimiento						);
				ps.setString(4, fechaVencimiento						);

				ps.executeUpdate();

			} catch(Exception e){

				log.error("borraDispersionEnlistadaPorEPO(Exception)");
				log.error("borraDispersionEnlistadaPorEPO.claveEpo         = <" + claveEpo         + ">");
				log.error("borraDispersionEnlistadaPorEPO.claveMoneda      = <" + claveMoneda      + ">");
				log.error("borraDispersionEnlistadaPorEPO.fechaVencimiento = <" + fechaVencimiento + ">");
				log.error("borraDispersionEnlistadaPorEPO.con              = <" + con              + ">");
				log.error("borraDispersionEnlistadaPorEPO.sentence         = <" + sentence         + ">");
				e.printStackTrace();

				throw new AppException("Ocurri� un error al borrar registro de dispersi�n.");

			} finally {

				if(ps != null ) try{ ps.close(); }catch(Exception e){}

				log.info("borraDispersionEnlistadaPorEPO(S)");

			}

	}

	/**
	 *
	 * Registra el detalle del monto y numero total de documentos para un grupo
	 * de documentos identificado por <tt>tipoDocumentos</tt> y para una moneda,
	 * epo y fecha de vencimiento en especifico. En caso de que los documentos
	 * ya hayan sido enviados a flujo fondos, la actualizacion no se realiza.
	 * @throws AppException
	 *
	 * @param totalDocumentos  <tt>String</tt> con el numero total de documentos.
	 * @param monto            <tt>String</tt> con el monto total de documentos.
	 * @param tipoDocumentos   <tt>String</tt> con el tipo de los documentos: <tt>"OPR" </tt>para el grupo
	 *                         (Operada, Operada Pagada) y "SNOPR" para el grupo (Vencido sin Operar,
	 *                         Pagado sin Operar), si se especifica otra clave se lanza un AppException.
	 * @param claveEpo         <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param claveMoneda      <tt>String</tt> con la clave de la Moneda (COMCAT_MONEDA.IC_MONEDA).
	 * @param fechaVencimiento <tt>String</tt> con la fecha de vencimiento del grupo de documentos.
	 * @param loginDelUsuario  <tt>String</tt> con el login del usuario.
	 * @param nombreDelUsuario <tt>String</tt> con el nombre del usuario.
	 * @param con				   <tt>AccesoDB</tt> con la conexion a la base de datos.
	 *
	 * @author jshernandez
	 * @since  F012-2013; 30/05/2013 01:09:16 p.m.
	 *
	 */
	public boolean enlistarDispersionEPOPorTipoDocumento(
			String    totalDocumentos,
			String    monto,
			String    tipoDocumentos,
			String    claveEpo,
			String    claveMoneda,
			String    fechaVencimiento,
			String    loginDelUsuario,
			String    nombreDelUsuario,
			AccesoDB con
		) throws AppException {

			boolean 				dispersionEnlistada 	= false;

			StringBuffer		sentencia 				= new StringBuffer(128);
			PreparedStatement ps 						= null;

			try {

				// 1.4.7.1 Ver si es posible actualizar el registro
				boolean enviadoFlujoFondos = seEnvioAFlujoFondos( claveEpo, claveMoneda, fechaVencimiento, tipoDocumentos, con );

				// 1.4.7.2 Si se cumple que seEnvioAFlujoFondos == true, se cancela la operaci�n,
				// se retorna 'false' indicando que no se pudo realizar el registro.
				if( enviadoFlujoFondos ){

					dispersionEnlistada = false;

				} else {

					// 1.4.7.3 Actualizar los siguientes campos COM_DISPERSION_ENLISTADA_X_EPO
					sentencia.append(
						"UPDATE                            "  +
						"   COM_DISPERSION_ENLISTADA_X_EPO "  +
						"SET                               "
					);
					sentencia.append("   IG_TOTAL_DOCUMENTOS_");	sentencia.append(tipoDocumentos); sentencia.append(" = ?, ");
					sentencia.append("   FN_MONTO_");				sentencia.append(tipoDocumentos); sentencia.append(" = ?, ");
					//Se conservan los comentarios actuales
					  //sentencia.append("   CG_OBSERVACIONES_")		  sentencia.append(tipoDocumentos); sentencia.append(" = ?, ");
					//Se asume que envio a flujo fondos es NULL
					  //sentencia.append("   DF_ENVIO_FLUJO_FONDOS_") sentencia.append(tipoDocumentos); sentencia.append(" = ?, ");
					sentencia.append(
						"   IC_USUARIO          = ?,                      "  +
						"   CG_NOMBRE_USUARIO   = ?                       "  +
						"WHERE                                            "  +
						"   IC_EPO         =  ? AND                       "  +
						"   IC_MONEDA      =  ? AND                       "  +
						"   DC_VENCIMIENTO >= TO_DATE(?,'DD/MM/YYYY') AND "  +
						"   DC_VENCIMIENTO <  TO_DATE(?,'DD/MM/YYYY') + 1 "
					);

					ps = con.queryPrecompilado(sentencia.toString());

					ps.setInt(1, 			Integer.parseInt(totalDocumentos) );
					ps.setBigDecimal(2,	new BigDecimal(monto)				 );
					ps.setString(3, 		loginDelUsuario				       );
					ps.setString(4, 		nombreDelUsuario				       );
					ps.setInt(5,	 		Integer.parseInt(claveEpo)			 );
					ps.setInt(6,			Integer.parseInt(claveMoneda)		 );
					ps.setString(7, 		fechaVencimiento						 );
					ps.setString(8, 		fechaVencimiento						 );

					ps.executeUpdate();

					// 1.4.7.4 Retornar true, indicando que la actualizacion fue exitosa.
					dispersionEnlistada = true;

				}

			} catch(Exception e){

				log.error("enlistarDispersionEPOPorTipoDocumento(Exception)");
				log.error("enlistarDispersionEPOPorTipoDocumento.totalDocumentos  = <" + totalDocumentos      + ">");
				log.error("enlistarDispersionEPOPorTipoDocumento.monto            = <" + monto                + ">");
				log.error("enlistarDispersionEPOPorTipoDocumento.tipoDocumentos   = <" + tipoDocumentos       + ">");
				log.error("enlistarDispersionEPOPorTipoDocumento.claveEpo         = <" + claveEpo             + ">");
				log.error("enlistarDispersionEPOPorTipoDocumento.claveMoneda      = <" + claveMoneda          + ">");
				log.error("enlistarDispersionEPOPorTipoDocumento.fechaVencimiento = <" + fechaVencimiento     + ">");
				log.error("enlistarDispersionEPOPorTipoDocumento.loginDelUsuario  = <" + loginDelUsuario      + ">");
				log.error("enlistarDispersionEPOPorTipoDocumento.nombreDelUsuario = <" + nombreDelUsuario     + ">");
				log.error("enlistarDispersionEPOPorTipoDocumento.con              = <" + con                  + ">");
				log.error("enlistarDispersionEPOPorTipoDocumento.sentencia        = <" + sentencia.toString() + ">");
				e.printStackTrace();

				throw new AppException("Ocurri� un error al enlistar dispersi�n");

			} finally {

				if(ps != null ) try{ ps.close(); }catch(Exception e){}

				log.info("enlistarDispersionEPOPorTipoDocumento(S)");

			}

			return dispersionEnlistada;

	}

	/**
	 * Registra en la tabla COM_DISPERSION_ENLISTADA_X_EPO el env�o del correo de notificaci�n
	 * @throws AppException
	 *
	 * @param claveEpo         <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param fechaVencimiento <tt>String</tt> con la fecha de vencimiento del grupo de documentos.
	 * @param loginDelUsuario  <tt>String</tt> con el login del usuario.
	 * @param nombreDelUsuario <tt>String</tt> con el nombre del usuario.
	 * @param con				   <tt>AccesoDB</tt> con la conexion a la base de datos.
	 *
	 * @author jshernandez
	 * @since  F012-2013; 30/05/2013 05:48:30 p.m.
	 *
	 */
	public void registrarEnvioCorreoDispersionEPO(
		String 		claveEpo,
		String 		fechaVencimiento,
		String 		loginDelUsuario,
		String 		nombreDelUsuario,
		AccesoDB 	con
	 ) throws AppException {

		log.info("registrarEnvioCorreoDispersionEPO(E)");

		StringBuffer	  	sentence			= new StringBuffer(256);
		PreparedStatement ps 				= null;

		try {

			sentence.setLength(0);
			sentence.append(
				"UPDATE                                             "  +
				"   COM_DISPERSION_ENLISTADA_X_EPO                  "  +
				"SET                                                "  +
				"   DF_ENVIO_CLIENTE    = SYSDATE,                  "  +
				"   IC_USUARIO          = ?,                        "  +
				"   CG_NOMBRE_USUARIO   = ?                         "  +
				"WHERE                                              "  +
				"   IC_EPO         =  ?                       AND   "  +
				"   DC_VENCIMIENTO >= TO_DATE(?,'DD/MM/YYYY') AND   "  +
				"   DC_VENCIMIENTO <  TO_DATE(?,'DD/MM/YYYY') + 1   "
			);

			ps = con.queryPrecompilado( sentence.toString() );

			ps.setString(1, loginDelUsuario				      );
			ps.setString(2, nombreDelUsuario				      );
			ps.setInt(3,	 Integer.parseInt(claveEpo)		);
			ps.setString(4, fechaVencimiento						);
			ps.setString(5, fechaVencimiento						);

			ps.executeUpdate();

		} catch(Exception e){

			log.error("registrarEnvioCorreoDispersionEPO(Exception)");
			log.error("registrarEnvioCorreoDispersionEPO.claveEpo         = <" + claveEpo         + ">");
			log.error("registrarEnvioCorreoDispersionEPO.fechaVencimiento = <" + fechaVencimiento + ">");
			log.error("registrarEnvioCorreoDispersionEPO.loginDelUsuario  = <" + loginDelUsuario  + ">");
			log.error("registrarEnvioCorreoDispersionEPO.nombreDelUsuario = <" + nombreDelUsuario + ">");
			log.error("registrarEnvioCorreoDispersionEPO.con         	  = <" + con              + ">");
			log.error("registrarEnvioCorreoDispersionEPO.sentence         = <" + sentence         + ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al fecha de env�o del correo de notificaci�n.");

		} finally {

			if(ps != null ) try{ ps.close(); }catch(Exception e){}
			log.info("registrarEnvioCorreoDispersionEPO(S)");

		}

	}

	/**
	 *
	 * Calcula el monto total y total doctos para los grupos de estatus 'OPR' y 'SNOPR' corespondientes a una
	 * moneda en especifico; se debe cumplir ademas que la cuenta asociada a cada uno de los registros est�
	 * autorizada ( ic_estatus_cecoban = 99 ).
	 * @throws AppException
	 *
	 * @param registros        Objeto de tipo <tt>Registros</tt>, con los datos de los documentos cuyos totales ser�n
	 *                         calculados.
	 * @param claveMoneda      <tt>String</tt> con la clave de la moneda ( COMCAT_MONEDA.IC_MONEDA ) con respecto a la cual
	 *                         se calcular�n los totales.
	 * @param fechaVencimiento <tt>String</tt> con la fecha de vencimiento del grupo de documentos.
	 *
	 * @return HashMap con los totales de montos y documentos para los grupos 'OPR' (Operada/Operada Pagada) y 'SNROP'
	 *
	 * @author jshernandez
	 * @since  F012-2013; 31/05/2013 01:37:25 p.m.
	 *
	 */
	public HashMap calcularDoctosYMontosTotalesDispersionEPO(Registros registros, String claveMoneda, String fechaVencimiento )
		throws AppException {

		log.info("calcularDoctosYMontosTotalesDispersionEPO(E)");

		// Constantes de estatus de documento
		final String OPERADA 				= "4";
		final String OPERADA_PAGADA 		= "11";
		final String VENCIDO_SIN_OPERAR 	= "9";
		final String PAGADO_SIN_OPERAR 	= "10";
		// Constantes estatus cuenta
		final String CUENTA_AUTORIZADA	= "99";

		// Inicializar Variables auxiliares
		BigDecimal 	montoTotal  			= new BigDecimal(0);
		int 	 		numTotalDoctos			= 0;
		String 		grupoDoctos 			= "";
		// Variables para el grupo 'OPR'
		BigDecimal	totalMontoOpr			= new BigDecimal(0);
		int			totalDocumentosOpr	= 0;
		// Variables para el grupo 'SNOPR'
		BigDecimal	totalMontoSnOpr		= new BigDecimal(0);
		int 			totalDocumentosSnOpr = 0;

		HashMap  resultados					= new HashMap();

		try {

			//
			//   Calcular total monto y total doctos por grupo de estatus, moneda y estatus_cecoban.
			//
			// 	Grupo Operada
			//
			// 		Operada ( IC_ESTATUS_DOCTO = 4  )            � Operada Pagada 		( IC_ESTATUS_DOCTO = 11 )
			//
			// 	Grupo Sin Operar
			//
			// 		Vencido sin Operar ( IC_ESTATUS_DOCTO = 9  ) � Pagado sin Operar 	( IC_ESTATUS_DOCTO = 10 )
			//
			// 	   Se calcular�n 2 totales dependiendo de las siguientes combinaciones
			//
			// 	ic_estatus_docto			( Grupo Operadas 'OPR' , Grupo Sin Operar 'SNOPR' )
			// 	ic_estatus_cecoban   	( 99 )
			// 	ic_moneda					<MONEDA SELECCIONADA>
			//
			//   ( 23/09/2013 06:27:00 p.m. ) Si el documento aun no ha vencido y corresponde al grupo 'OPR' solo
			//   se tomaran en cuenta los documentos con estatus Operada ( IC_ESTATUS_DOCTO = 4  ).
			//

			// DETERMINAR SI LOS DOCUMENTOS "YA VENCIERON"

			SimpleDateFormat dateFormat 	= new SimpleDateFormat("dd/MM/yyyy");
			dateFormat.setLenient(false);
			Calendar aux 						= dateFormat.getCalendar();

			dateFormat.parse(Fecha.getFechaActual());
			Calendar fechaActual01 			= new GregorianCalendar(aux.get(Calendar.YEAR),aux.get(Calendar.MONTH),aux.get(Calendar.DAY_OF_MONTH));

			dateFormat.parse(fechaVencimiento);
			Calendar fechaVencimiento01  	= new GregorianCalendar(aux.get(Calendar.YEAR),aux.get(Calendar.MONTH),aux.get(Calendar.DAY_OF_MONTH));

			// ( 17/10/2013 04:49:52 p.m. ) Se asume que los documento vencen cuando llegan a su fecha de vencimiento.
			boolean sinVencer = fechaActual01.before(fechaVencimiento01)?true:false;

			// LEER LOS REGISTROS
			while( registros != null && registros.next() ){

				String icEstatusDocto   = registros.getString("ic_estatus_docto");
				String icEstatusCecoban = registros.getString("rs_cecoban");
				String icMoneda			= registros.getString("rs_ic_moneda");

				// Determinar Grupo
				if(        OPERADA.equals(icEstatusDocto)            || OPERADA_PAGADA.equals(icEstatusDocto)    ){
					grupoDoctos = "OPR";
				} else if( VENCIDO_SIN_OPERAR.equals(icEstatusDocto) || PAGADO_SIN_OPERAR.equals(icEstatusDocto) ){
					grupoDoctos = "SNOPR";
				} else {
					grupoDoctos = "";
				}

				// ( 23/09/2013 06:00:46 p.m. ) Si los documentos del grupo "OPR" todav�a est�n en estatus "Operada" ( sinVencer )
				// solo se deber�n considerar documentos con ese estatus.
				if( "OPR".equals(grupoDoctos) && sinVencer &&  !OPERADA.equals(icEstatusDocto) ){
					continue;
				}

				// Usar moneda seleccionada
				if( !claveMoneda.equals(icMoneda) ){
					continue;
				}
				// La cuenta debe estar autorizada
				if( !CUENTA_AUTORIZADA.equals(icEstatusCecoban)){ // icEstatusCecoban == 99
					continue;
				}
				// El grupo de documentos debe pertener 'OPR' � 'SNOPR'
				if( !"OPR".equals(grupoDoctos) && !"SNOPR".equals(grupoDoctos) ){
					continue;
				}

				// Extraer monto total
				montoTotal 	 	= "".equals(registros.getString("Monto Total"))      ?
					new BigDecimal(0.0): new BigDecimal(registros.getString("Monto Total"));
				// Extraer numero total de documentos
				numTotalDoctos = "".equals(registros.getString("Num. Total Doctos"))?
					0  					 : Integer.parseInt(registros.getString("Num. Total Doctos"));

				if(        "OPR".equals(grupoDoctos)   ){
					totalMontoOpr			= totalMontoOpr.add( montoTotal );
					totalDocumentosOpr  	+= numTotalDoctos;
				} else if( "SNOPR".equals(grupoDoctos) ){
					totalMontoSnOpr		= totalMontoSnOpr.add( montoTotal );
					totalDocumentosSnOpr	+= numTotalDoctos;
				}

			}

			// Enviar resultados

			// Variables para el grupo 'OPR'
			resultados.put("totalMontoOpr",			totalMontoOpr							);
			resultados.put("totalDocumentosOpr", 	new Integer(totalDocumentosOpr)	);
			// Variables para el grupo 'SNOPR'
			resultados.put("totalMontoSnOpr",		totalMontoSnOpr						);
			resultados.put("totalDocumentosSnOpr",	new Integer(totalDocumentosSnOpr));

		} catch(Exception e){

			log.error("calcularDoctosYMontosTotalesDispersionEPO(Exception)");
			log.error("calcularDoctosYMontosTotalesDispersionEPO.registros        = <" + registros        + ">");
			log.error("calcularDoctosYMontosTotalesDispersionEPO.claveMoneda      = <" + claveMoneda      + ">");
			log.error("calcularDoctosYMontosTotalesDispersionEPO.fechaVencimiento = <" + fechaVencimiento + ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al calcular totales de documentos y montos: " + e.getMessage() );

		} finally {
			log.info("calcularDoctosYMontosTotalesDispersionEPO(S)");
		}

		return resultados;

	}

	/**
	 *
	 * Devuelve el detalle de las dispersiones enlistadas con una fecha de vencimiento espec�fico.
	 * @throws AppException
	 *
	 * @param parametroFechaVencimiento <tt>String</tt> con la fecha de vencimiento,con formato <tt>DD/MM/AAAA</tt>.
	 *
	 * @return <tt>List</tt> con del detalle de las dispersiones enlistadas.
	 *
	 * @author jshernandez
	 * @since  F012-2013; 05/06/2013 04:08:01 p.m.
	 *
	 */
	public List getDetalleDispersionesEnlistadas(String parametroFechaVencimiento)
		throws AppException {

		log.info("getDetalleDispersionesEnlistadas(E)");

		AccesoDB 			con 			= new AccesoDB();
		StringBuffer 		query 		= new StringBuffer();
		PreparedStatement ps 			= null;
		ResultSet			rs				= null;

		List					registros 	= new ArrayList();

		try {

			con.conexionDB();

			query.append(
				"SELECT                                                                                                "  +
				"   LISTA.IC_EPO                                                  AS IC_EPO,                           "  +
				"   LISTA.IC_MONEDA                                               AS IC_MONEDA,                        "  +
				"   EPO.CG_RAZON_SOCIAL                                           AS NOMBRE_EPO,                       "  +
				"   TO_CHAR(LISTA.DF_ENVIO_CLIENTE,'DD/MM/YYYY')                  AS FECHA_ENVIO_CLIENTE,              "  +
				"   TO_CHAR(LISTA.DC_VENCIMIENTO,  'DD/MM/YYYY')                  AS FECHA_VENCIMIENTO,                "  +
				"   MONEDA.CD_NOMBRE                                              AS NOMBRE_MONEDA,                    "  +
				"   LISTA.IG_TOTAL_DOCUMENTOS_OPR                                 AS TOTAL_DOCUMENTOS_OPR,             "  +
				"   LISTA.FN_MONTO_OPR                                            AS MONTO_OPR,                        "  +
				"   LISTA.CG_OBSERVACIONES_OPR                                    AS OBSERVACIONES_OPR,                "  +
				"   LISTA.CG_OBSERVACIONES_OPR                                    AS OBSERVACIONES_ORIG_OPR,           "  +
				"   DECODE(LISTA.DF_ENVIO_FLUJO_FONDOS_OPR,NULL,'false','true')   AS ENVIO_FFON_OPR,                   "  +
				"   CASE                                                                                               "  +
				"      WHEN LISTA.IG_TOTAL_DOCUMENTOS_OPR = 0           THEN                                           "  +
				"         'READONLY'                                                                                   "  +
				"      WHEN LISTA.DF_ENVIO_FLUJO_FONDOS_OPR IS NOT NULL THEN                                           "  +
				"         'READONLY'                                                                                   "  +
				"      ELSE                                                                                            "  +
				"         'NORMAL'                                                                                     "  +
				"   END                                                           AS ENVIO_FFON_OPR_STATUS,            "  +
				"   TO_CHAR(LISTA.DF_ENVIO_FLUJO_FONDOS_OPR,'DD/MM/YYYY')         AS FECHA_ENVIO_FLUJO_FONDOS_OPR,     "  +
				"   LISTA.IG_TOTAL_DOCUMENTOS_SNOPR                               AS TOTAL_DOCUMENTOS_SNOPR,           "  +
				"   LISTA.FN_MONTO_SNOPR                                          AS MONTO_SNOPR,                      "  +
				"   LISTA.CG_OBSERVACIONES_SNOPR                                  AS OBSERVACIONES_SNOPR,              "  +
				"   LISTA.CG_OBSERVACIONES_SNOPR                                  AS OBSERVACIONES_ORIG_SNOPR,         "  +
				"   DECODE(LISTA.DF_ENVIO_FLUJO_FONDOS_SNOPR,NULL,'false','true') AS ENVIO_FFON_SNOPR,                 "  +
				"   CASE                                                                                               "  +
				"      WHEN LISTA.IG_TOTAL_DOCUMENTOS_SNOPR = 0           THEN                                         "  +
				"         'READONLY'                                                                                   "  +
				"      WHEN LISTA.DF_ENVIO_FLUJO_FONDOS_SNOPR IS NOT NULL THEN                                         "  +
				"         'READONLY'                                                                                   "  +
				"      ELSE                                                                                            "  +
				"         'NORMAL'                                                                                     "  +
				"   END                                                           AS ENVIO_FFON_SNOPR_STATUS,          "  +
				"   TO_CHAR(LISTA.DF_ENVIO_FLUJO_FONDOS_SNOPR,'DD/MM/YYYY')       AS FECHA_ENVIO_FLUJO_FONDOS_SNOPR,   "  +
				"   CASE                                                                                               "  +
				"      WHEN LISTA.DF_ENVIO_FLUJO_FONDOS_OPR   IS NOT NULL THEN                                         "  +
				"         'true'                                                                                       "  +
				"      WHEN LISTA.DF_ENVIO_FLUJO_FONDOS_SNOPR IS NOT NULL THEN                                         "  +
				"         'true'                                                                                       "  +
				"      ELSE                                                                                            "  +
				"         'false'                                                                                      "  +
				"   END                                                           AS VER_DETALLE_DISPERSION,           "  +
				"   LISTA.IC_USUARIO                                              AS LOGIN_USUARIO,                    "  +
				"   LISTA.CG_NOMBRE_USUARIO                                       AS NOMBRE_USUARIO                    "  +
				"FROM                                                                                                  "  +
				"   COM_DISPERSION_ENLISTADA_X_EPO LISTA,                                                              "  +
				"   COMCAT_EPO                     EPO,                                                                "  +
				"   COMCAT_MONEDA                  MONEDA                                                              "  +
				"WHERE                                                                                                 "  +
				"   LISTA.DC_VENCIMIENTO >= TO_DATE(?,'DD/MM/YYYY')     AND                                            "  +
				"   LISTA.DC_VENCIMIENTO <  TO_DATE(?,'DD/MM/YYYY') + 1 AND                                            "  +
				"   LISTA.IC_EPO         =  EPO.IC_EPO                  AND                                            "  +
				"   LISTA.IC_MONEDA      =  MONEDA.IC_MONEDA                                                           "  +
				"ORDER BY EPO.CG_RAZON_SOCIAL                                                                          "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,parametroFechaVencimiento);
			ps.setString(2,parametroFechaVencimiento);
            log.trace("query:"+query.toString());
		    log.trace("parametroFechaVencimiento[1]:"+parametroFechaVencimiento);
		    log.trace("parametroFechaVencimiento[2]:"+parametroFechaVencimiento);

			rs = ps.executeQuery();

			while(rs.next()){

				String icEpo 								= (rs.getString("IC_EPO")									== null)?""				:rs.getString("IC_EPO");
				String icMoneda 							= (rs.getString("IC_MONEDA")								== null)?""				:rs.getString("IC_MONEDA");
				String nombreEpo 							= (rs.getString("NOMBRE_EPO")								== null)?""				:rs.getString("NOMBRE_EPO");
				String fechaEnvioCliente 				= (rs.getString("FECHA_ENVIO_CLIENTE")					== null)?""				:rs.getString("FECHA_ENVIO_CLIENTE");
				String fechaVencimiento 				= (rs.getString("FECHA_VENCIMIENTO")					== null)?""				:rs.getString("FECHA_VENCIMIENTO");
				String nombreMoneda 						= (rs.getString("NOMBRE_MONEDA")							== null)?""				:rs.getString("NOMBRE_MONEDA");
				String totalDocumentosOpr 				= (rs.getString("TOTAL_DOCUMENTOS_OPR")				== null)?"0"			:rs.getString("TOTAL_DOCUMENTOS_OPR");
				String montoOpr 							= (rs.getString("MONTO_OPR")								== null)?"0"			:rs.getString("MONTO_OPR");
				String observacionesOpr 				= (rs.getString("OBSERVACIONES_OPR")					== null)?""				:rs.getString("OBSERVACIONES_OPR");
				String observacionesOrigOpr 			= (rs.getString("OBSERVACIONES_ORIG_OPR")				== null)?""				:rs.getString("OBSERVACIONES_ORIG_OPR");
				String envioFfonOpr 						= (rs.getString("ENVIO_FFON_OPR")						== null)?"false"		:rs.getString("ENVIO_FFON_OPR");
				String envioFfonOprStatus 				= (rs.getString("ENVIO_FFON_OPR_STATUS")				== null)?"READONLY"	:rs.getString("ENVIO_FFON_OPR_STATUS");
				String fechaEnvioFlujoFondosOpr 		= (rs.getString("FECHA_ENVIO_FLUJO_FONDOS_OPR")		== null)?""				:rs.getString("FECHA_ENVIO_FLUJO_FONDOS_OPR");
				String totalDocumentosSnOpr 			= (rs.getString("TOTAL_DOCUMENTOS_SNOPR")				== null)?"0"			:rs.getString("TOTAL_DOCUMENTOS_SNOPR");
				String montoSnOpr 						= (rs.getString("MONTO_SNOPR")							== null)?"0"			:rs.getString("MONTO_SNOPR");
				String observacionesSnOpr 				= (rs.getString("OBSERVACIONES_SNOPR")					== null)?""				:rs.getString("OBSERVACIONES_SNOPR");
				String observacionesOrigSnOpr 		= (rs.getString("OBSERVACIONES_ORIG_SNOPR")			== null)?""				:rs.getString("OBSERVACIONES_ORIG_SNOPR");
				String envioFfonSnOpr 					= (rs.getString("ENVIO_FFON_SNOPR")						== null)?"false"		:rs.getString("ENVIO_FFON_SNOPR");
				String envioFfonSnOprStatus 			= (rs.getString("ENVIO_FFON_SNOPR_STATUS")			== null)?"READONLY"	:rs.getString("ENVIO_FFON_SNOPR_STATUS");
				String fechaEnvioFlujoFondosSnOpr 	= (rs.getString("FECHA_ENVIO_FLUJO_FONDOS_SNOPR")	== null)?""				:rs.getString("FECHA_ENVIO_FLUJO_FONDOS_SNOPR");
				String verDetalleDispersion 			= (rs.getString("VER_DETALLE_DISPERSION")				== null)?""				:rs.getString("VER_DETALLE_DISPERSION");
				String loginUsuario 						= (rs.getString("LOGIN_USUARIO")							== null)?""				:rs.getString("LOGIN_USUARIO");
				String nombreUsuario 					= (rs.getString("NOMBRE_USUARIO")						== null)?""				:rs.getString("NOMBRE_USUARIO");

				//totalDocumentosOpr	= Comunes.formatoDecimal(totalDocumentosOpr,0,true);
				montoOpr					= "$" + Comunes.formatoDecimal(montoOpr,2,true);

				//totalDocumentosSnOpr	= Comunes.formatoDecimal(totalDocumentosSnOpr,0,true);
				montoSnOpr				= "$" + Comunes.formatoDecimal(montoSnOpr,2,true);

				HashMap registro = new HashMap();
				registro.put("IC_EPO",									icEpo								);
				registro.put("IC_MONEDA",								icMoneda							);
				registro.put("NOMBRE_EPO",								nombreEpo						);
				registro.put("FECHA_ENVIO_CLIENTE",					fechaEnvioCliente				);
				registro.put("FECHA_VENCIMIENTO",					fechaVencimiento				);
				registro.put("NOMBRE_MONEDA",							nombreMoneda					);
				registro.put("TOTAL_DOCUMENTOS_OPR",				totalDocumentosOpr			);
				registro.put("MONTO_OPR",								montoOpr							);
				registro.put("OBSERVACIONES_OPR",					observacionesOpr				);
				registro.put("OBSERVACIONES_ORIG_OPR",				observacionesOrigOpr			);
				registro.put("ENVIO_FFON_OPR",						envioFfonOpr					);
				registro.put("ENVIO_FFON_OPR_STATUS",				envioFfonOprStatus			);
				registro.put("FECHA_ENVIO_FLUJO_FONDOS_OPR",		fechaEnvioFlujoFondosOpr	);
				registro.put("TOTAL_DOCUMENTOS_SNOPR",				totalDocumentosSnOpr			);
				registro.put("MONTO_SNOPR",							montoSnOpr						);
				registro.put("OBSERVACIONES_SNOPR",					observacionesSnOpr			);
				registro.put("OBSERVACIONES_ORIG_SNOPR",			observacionesOrigSnOpr		);
				registro.put("ENVIO_FFON_SNOPR",						envioFfonSnOpr					);
				registro.put("ENVIO_FFON_SNOPR_STATUS",			envioFfonSnOprStatus			);
				registro.put("FECHA_ENVIO_FLUJO_FONDOS_SNOPR",	fechaEnvioFlujoFondosSnOpr	);
				registro.put("VER_DETALLE_DISPERSION",				verDetalleDispersion			);
				registro.put("LOGIN_USUARIO",							loginUsuario					);
				registro.put("NOMBRE_USUARIO",						nombreUsuario					);

				registros.add(registro);

			}

		} catch(Exception e){

			log.error("getDetalleDispersionesEnlistadas(Exception)");
			log.error("getDetalleDispersionesEnlistadas.parametroFechaVencimiento = <" + parametroFechaVencimiento+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al consultar el detalle de las dispersiones por EPO.");

		} finally {

			if(rs != null ) try{ rs.close(); }catch(Exception e){}
			if(ps != null ) try{ ps.close(); }catch(Exception e){}

			if( con.hayConexionAbierta() ){
				con.cierraConexionDB();
			}
			log.info("getDetalleDispersionesEnlistadas(S)");

		}

		return registros;

	}


	private void actualizaFechaEnvioFFONTableroDispersionEPO(
		String 	claveEPO,
		String 	claveMoneda,
		String 	fechaVencimiento,
		String 	estatusDocumentos,
		AccesoDB con )
	throws Exception {

		log.info("actualizaFechaEnvioFFONTableroDispersionEPO(E)");

		StringBuffer 		sentencia = new StringBuffer(512);
		PreparedStatement	ps			 = null;

		// Constantes de estatus de documento
		final String OPERADA 				= "4";
		final String OPERADA_PAGADA 		= "11";
		final String VENCIDO_SIN_OPERAR 	= "9";
		final String PAGADO_SIN_OPERAR 	= "10";

		try {

			// Determinar tipo de documento
			String tipoDocumento = null;

			if( OPERADA.equals(estatusDocumentos) || OPERADA_PAGADA.equals(estatusDocumentos) ){
				tipoDocumento = "OPR";
			} else if( VENCIDO_SIN_OPERAR.equals(estatusDocumentos) || PAGADO_SIN_OPERAR.equals(estatusDocumentos) ){
				tipoDocumento = "SNOPR";
			} else {
				return;
			}

			sentencia.setLength(0);
			sentencia.append(
				"UPDATE                                            "  +
				"   COM_DISPERSION_ENLISTADA_X_EPO                 "  +
				"SET                                               "
			);
			if(       "OPR".equals(tipoDocumento)   ){
				sentencia.append("   DF_ENVIO_FLUJO_FONDOS_OPR   = SYSDATE ");
			}else if( "SNOPR".equals(tipoDocumento) ){
				sentencia.append("   DF_ENVIO_FLUJO_FONDOS_SNOPR = SYSDATE ");
			}
			sentencia.append(
				"WHERE                                             "  +
				"   IC_EPO          =  ? AND                       "  +
				"   IC_MONEDA       =  ? AND                       "  +
				"   DC_VENCIMIENTO  >= TO_DATE(?,'DD/MM/YYYY') AND "  +
				"   DC_VENCIMIENTO  <  TO_DATE(?,'DD/MM/YYYY') + 1 "
			);

			ps = con.queryPrecompilado(sentencia.toString());
			ps.setInt(   1, Integer.parseInt(claveEPO)     );
			ps.setInt(   2, Integer.parseInt(claveMoneda)  );
			ps.setString(3, fechaVencimiento               );
			ps.setString(4, fechaVencimiento               );

			ps.executeUpdate();

		} catch(Exception e){

			log.error("actualizaFechaEnvioFFONTableroDispersionEPO(Exception)");
			log.error("actualizaFechaEnvioFFONTableroDispersionEPO.claveEPO          = <" + claveEPO          + ">");
			log.error("actualizaFechaEnvioFFONTableroDispersionEPO.claveMoneda       = <" + claveMoneda       + ">");
			log.error("actualizaFechaEnvioFFONTableroDispersionEPO.fechaVencimiento  = <" + fechaVencimiento  + ">");
			log.error("actualizaFechaEnvioFFONTableroDispersionEPO.estatusDocumentos = <" + estatusDocumentos + ">");
			e.printStackTrace();

			throw e;

		} finally {

			if( ps != null ) try{ ps.close(); }catch(Exception e){}
			log.info("actualizaFechaEnvioFFONTableroDispersionEPO(S)");

		}

	}

	public void actualizaObservacionesTableroDisperionEPO(
		List 	 observacionesList,
		String loginDelUsuario,
		String nombreDelUsuario
	) throws AppException {

			log.info("actualizaObservacionesTableroDisperionEPO(E)");

			AccesoDB				con  			= new AccesoDB();
			StringBuffer 		sentencia 	= new StringBuffer(128);
			PreparedStatement ps01 			= null;
			PreparedStatement ps02 			= null;

			boolean 				exito 		= true;
			try {

				// Conectarse a la Base de Datos
				con.conexionDB();

				// Preparar statement para actualizar observaciones correspondientes al grupo de doctos: Operada / Operada Pagada
				sentencia.setLength(0);
				sentencia.append(
					"UPDATE                                           "  +
					"   COM_DISPERSION_ENLISTADA_X_EPO                "  +
					"SET                                              "  +
					"   CG_OBSERVACIONES_OPR = ?,                     "  +
					"   IC_USUARIO           = ?,                     "  +
					"   CG_NOMBRE_USUARIO    = ?                      "  +
					"WHERE                                            "  +
					"   IC_EPO          =  ? AND                      "  +
					"   IC_MONEDA       =  ? AND                      "  +
					"   DC_VENCIMIENTO >= TO_DATE(?,'DD/MM/YYYY') AND "  +
					"   DC_VENCIMIENTO <  TO_DATE(?,'DD/MM/YYYY') + 1 "
				);
				ps01 = con.queryPrecompilado(sentencia.toString());

				// Preparar statement para actualizar observaciones correspondientes al grupo de doctos: Vencido sin Operar / Pagado sin Operar
				sentencia.setLength(0);
				sentencia.append(
					"UPDATE                                           "  +
					"   COM_DISPERSION_ENLISTADA_X_EPO                "  +
					"SET                                              "  +
					"   CG_OBSERVACIONES_SNOPR = ?,                   "  +
					"   IC_USUARIO             = ?,                   "  +
					"   CG_NOMBRE_USUARIO      = ?                    "  +
					"WHERE                                            "  +
					"   IC_EPO          =  ? AND                      "  +
					"   IC_MONEDA       =  ? AND                      "  +
					"   DC_VENCIMIENTO >= TO_DATE(?,'DD/MM/YYYY') AND "  +
					"   DC_VENCIMIENTO <  TO_DATE(?,'DD/MM/YYYY') + 1 "
				);
				ps02 = con.queryPrecompilado(sentencia.toString());

				// Realizar actualizaci�n de las observaciones
				for(int i=0;i<observacionesList.size();i++){

					HashMap 	registro 		= (HashMap) observacionesList.get(i);
					String 	tipoDocumento 	= (String)	registro.get("TIPO_DOCUMENTO");

					if( 		  "OPR".equals(tipoDocumento)   ){

						ps01.clearParameters();
						ps01.setString( 1, (String)	registro.get("OBSERVACIONES") 			);
						ps01.setString( 2, (String)	loginDelUsuario 								);
						ps01.setString( 3, (String)	nombreDelUsuario 								);
						ps01.setString( 4, (String)	registro.get("IC_EPO") 						);
						ps01.setString( 5, (String)	registro.get("IC_MONEDA") 					);
						ps01.setString( 6, (String)	registro.get("DC_VENCIMIENTO")			);
						ps01.setString( 7, (String)	registro.get("DC_VENCIMIENTO") 			);

						ps01.executeUpdate();

					} else if( "SNOPR".equals(tipoDocumento) ){

						ps02.clearParameters();
						ps02.setString( 1, (String)	registro.get("OBSERVACIONES") 			);
						ps02.setString( 2, (String)	loginDelUsuario 								);
						ps02.setString( 3, (String)	nombreDelUsuario 								);
						ps02.setString( 4, (String)	registro.get("IC_EPO") 						);
						ps02.setString( 5, (String)	registro.get("IC_MONEDA") 					);
						ps02.setString( 6, (String)	registro.get("DC_VENCIMIENTO") 			);
						ps02.setString( 7, (String)	registro.get("DC_VENCIMIENTO") 			);

						ps02.executeUpdate();

					}

				}

			}catch(Exception e){

				exito = false;

				log.error("actualizaObservacionesTableroDisperionEPO(Exception)");
				log.error("actualizaObservacionesTableroDisperionEPO.observacionesList = <" + observacionesList + ">");
				log.error("actualizaObservacionesTableroDisperionEPO.loginDelUsuario   = <" + loginDelUsuario   + ">");
				log.error("actualizaObservacionesTableroDisperionEPO.nombreDelUsuario  = <" + nombreDelUsuario  + ">");
				e.printStackTrace();

				throw new AppException("Ocurri� un error inesperado al actualizar las observaciones.");

			} finally {

				if( ps01 != null ) try{ ps01.close(); }catch(Exception e){}
				if( ps02 != null ) try{ ps02.close(); }catch(Exception e){}

				if( con.hayConexionAbierta() ){
					con.terminaTransaccion(exito);
					con.cierraConexionDB();
				}
				log.info("actualizaObservacionesTableroDisperionEPO(S)");

			}

	}

	public HashMap enviarFlujoFondosDocumentosEPO(
		String 	 recordID,
		String 	 icEPO,
		String 	 icMoneda,
		String 	 dcVencimiento,
		String 	 tipoDocumento,
		String 	 totalDocumentos,
		String	 monto ) {

			log.info("enviarFlujoFondosDocumentosEPO(E)");

			AccesoDB 			con			= new AccesoDB();
			PreparedStatement ps				= null;
			ResultSet			rs				= null;

			boolean 				exito 		= true;
			HashMap				resultado 	= new HashMap();
			try {

				String fechaEnvioFlujoFondos = Fecha.getFechaActual();

				resultado.put("recordId",					recordID);
				resultado.put("tipoDocumento",			tipoDocumento);
				resultado.put("fechaEnvioFlujoFondos",	fechaEnvioFlujoFondos);

				// Conectarse a la base de datos
				con.conexionDB();

				// 1. Determinar el estatus de los documentos a partir de la fecha de vencimiento.
				String estatusDocumento 		= null;

				SimpleDateFormat dateFormat 	= new SimpleDateFormat("dd/MM/yyyy");
				dateFormat.setLenient(false);
				Calendar aux 						= dateFormat.getCalendar();

				dateFormat.parse(fechaEnvioFlujoFondos);
				Calendar fechaActual 			= new GregorianCalendar(aux.get(Calendar.YEAR),aux.get(Calendar.MONTH),aux.get(Calendar.DAY_OF_MONTH));

				dateFormat.parse(dcVencimiento);
				Calendar fechaVencimiento  	= new GregorianCalendar(aux.get(Calendar.YEAR),aux.get(Calendar.MONTH),aux.get(Calendar.DAY_OF_MONTH));

				// ( 17/10/2013 04:49:52 p.m. ) Se asume que los documento vencen cuando llegan a su fecha de vencimiento.
				//  Operada -> Operada Pagado y Vencido sin Operar -> Pagado sin Operar
				// * Si la fecha de consulta es menor a la fecha de vencimiento, el estatus del
				//		documento es: Operada o Vencido sin Operar seg�n sea el caso.
				if( fechaActual.before(fechaVencimiento) ){

					if(        "OPR".equals(tipoDocumento)   ){
						estatusDocumento = "4"; // Operada
					} else if( "SNOPR".equals(tipoDocumento) ){
						estatusDocumento = "9"; // Vencido sin Operar
					}

				// * Si la fecha de consulta es mayor o igual a la fecha de vencimiento, el estatus del documento
				//   es: Operada Pagado o Pagado sin Operar seg�n sea el caso.
				} else {

					if(        "OPR".equals(tipoDocumento)   ){
						estatusDocumento = "11"; // Operada Pagado
					} else if( "SNOPR".equals(tipoDocumento) ){
						estatusDocumento = "10"; // Pagado sin Operar
					}

				}

				// 2. Calcular el total de documentos y el monto a dispersar
				int 	 		totalDocumentosActual 	= 0;
				BigDecimal 	montoTotalActual 			= new BigDecimal(0);

				String queryDispersionEpos = getQueryDispEPOS( "", icEPO, icMoneda, estatusDocumento, dcVencimiento );

				ps = con.queryPrecompilado(queryDispersionEpos);
				ps.clearParameters();
				rs = ps.executeQuery();

				while(rs.next()) {

					int    		igTotalDocumentos	= rs.getInt("ig_total_documentos");
					String 		fnImporte 			= (rs.getString("fn_importe")			  == null)?"":rs.getString("fn_importe");
					String 		icEstatusCecoban 	= (rs.getString("ic_estatus_cecoban") == null)?"":rs.getString("ic_estatus_cecoban");

					fnImporte = fnImporte.matches("\\s*")?"0.00":fnImporte;

					if( "99".equals(icEstatusCecoban) ) {
						totalDocumentosActual 	+= igTotalDocumentos;
						montoTotalActual 			=  montoTotalActual.add( new BigDecimal(fnImporte) );
					}

				}

				rs.close();
				ps.close();

				// En caso de que para alguna EPO no coincidan "Total de Documentos" y/o "Monto" se marcar�n en la pantalla Dispersi�n EPOS con
				//	el siguiente icono: ICONO WARNING, al posicionarse sobre �l se despliega el siguiente tool-tip:
				//
				//		La Interface de Flujo de Fondos reporta # Documentos con un Monto de $ ###,###.## favor de
				//		verificar con el reporte actual
				//
				int 	 		totalDocumentosOriginal 	= Integer.parseInt(totalDocumentos);
				BigDecimal 	montoTotalOriginal			= new BigDecimal(monto);

				if(
						totalDocumentosOriginal != totalDocumentosActual
							||
						montoTotalOriginal.compareTo(montoTotalActual) != 0
				){
					exito = false;
					resultado.put("hayMensaje",new Boolean(true));
					resultado.put("iconoMensaje","icoWarning");
					resultado.put(
						"textoMensaje",
						" La Interface de Flujo de Fondos reporta "+Comunes.formatoDecimal(totalDocumentosActual,0,true)+
						" documento(s) con un Monto de $"+Comunes.formatoDecimal(montoTotalActual,2,true)+
						" favor de	verificar con el reporte actual"
					);
					return resultado;
					//return exito;
				}

				// Cancelar la operacion si ya existen operaciones
				String sFechaOperacion 	= ""; // No se ocupa
				String noIf				 	= ""; // No se ocupa
				if( 	existenOperaciones(icEPO, icMoneda, dcVencimiento, noIf, estatusDocumento, sFechaOperacion ) ){
					exito = false;
					resultado.put("hayMensaje",new Boolean(true));
					resultado.put("iconoMensaje","icoWarning");
					resultado.put("textoMensaje","Ya fueron registradas estas operaciones. ");
					return resultado;
					//return exito;
				}

				// Realizar env�o a flujo de fondos
				try {

					String sOrigen         	= "EPOS";
					llenaEncabezadoFlujoFondos(icEPO, icMoneda, dcVencimiento, noIf, sOrigen, estatusDocumento, sFechaOperacion, true);

				} catch(Exception e){

					exito = false;
					resultado.put("hayMensaje",new Boolean(true));
					resultado.put("iconoMensaje","icoError");
					resultado.put("textoMensaje","Fall� el env�o a FFON: " + e.getMessage());

					e.printStackTrace();

				}

				if(exito){
					resultado.put("success",new Boolean(exito));
				}

			} catch(Exception e){

				exito = false;

				log.error("enviarFlujoFondosDocumentosEPO(Exception)");
				log.error("enviarFlujoFondosDocumentosEPO.recordID        = <" + recordID 			+ ">");
				log.error("enviarFlujoFondosDocumentosEPO.icEPO           = <" + icEPO 				+ ">");
				log.error("enviarFlujoFondosDocumentosEPO.icMoneda        = <" + icMoneda 			+ ">");
				log.error("enviarFlujoFondosDocumentosEPO.dcVencimiento   = <" + dcVencimiento 	+ ">");
				log.error("enviarFlujoFondosDocumentosEPO.tipoDocumento   = <" + tipoDocumento 	+ ">");
				log.error("enviarFlujoFondosDocumentosEPO.totalDocumentos = <" + totalDocumentos + ">");
				log.error("enviarFlujoFondosDocumentosEPO.monto           = <" + monto 				+ ">");
				e.printStackTrace();

				resultado.put("hayMensaje",new Boolean(true));
				resultado.put("iconoMensaje","icoError");
				resultado.put("textoMensaje","Fall� el env�o a FFON: " + e.getMessage());

			} finally {

				if(rs != null ) try{ rs.close(); }catch(Exception e){}
				if(ps != null ) try{ ps.close(); }catch(Exception e){}

				if( con.hayConexionAbierta() ){
					con.cierraConexionDB();
				}
				log.info("enviarFlujoFondosDocumentosEPO(S)");

			}

			return resultado;
			//return exito;

	}

	/**
	 * Devuelve el detalle de los documentos a dispersar para IF.
	 *   PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - IF's
	 * @throws AppException
	 *
	 * @param noEpo	 <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param noMoneda <tt>String</tt> con la clave de la Moneda (COMCAT_MONEDA.IC_MONEDA).
	 * @param noIf		 <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF).
	 * @param fechaReg <tt>String</tt> con la fecha de registro,con formato <tt>DD/MM/AAAA</tt>.
	 *
	 * @return <tt>HashMap</tt> con del detalle de la consulta.
	 *
	 * @author jshernandez
	 * @since  F006-2013; 29/07/2013 03:35:17 p.m.
	 *
	 */
	public HashMap consultaDispersionFISOS( String noEpo, String noMoneda, String noIf, String fechaReg )
		throws AppException {

		log.info("consultaDispersionFISOS(E)");

		HashMap 				consulta				= new HashMap();
		List					registros 			= new ArrayList();
		List					registrosTotales	= new ArrayList();
		HashMap				detalleTotales		= new HashMap();

		HashMap				registro				= null;

		AccesoDB				con					= new AccesoDB();
		PreparedStatement	ps						= null;
		ResultSet			rs						= null;
		String 				query 				= null;

		try {

			// Conectarse a la Base de Datos
			con.conexionDB();

			int 			iReg 					= 0;
			int 			iNoDoctos 			= 0;
			int 			iNoDoctosDisp 		= 0;
			int 			iNoDoctosError 	= 0;

			double 		dImpTotal 			= 0;
			double 		dImpTotalDisp 		= 0;
			double 		dImpTotalError 	= 0;

			query 								= getQueryDispFISOS("", noEpo, noMoneda, noIf);
			log.info("consultaDispersionFISOS.query = <" + query+ ">");

			ps 				 = con.queryPrecompilado(query);
			rs 				 = ps.executeQuery();
			int CtaNoValida = 0;
			while( rs.next() ){

				iReg++;
				if(iReg == 1) {
					/*
					RFC
					NOMBRE_PROVEEDOR
					TOTA_DOCUMENTOS
					MONEDA
					MONTO
					BANCO
					TIPO_CUENTA
					NUMERO_CUENTA
					ESTATUS_CECOBAN
					*/
				}

				String estatus_tef	= rs.getString("ic_estatus_tef")		 == null?"":rs.getString("ic_estatus_tef").trim();
				String estatus_cec	= rs.getString("ic_estatus_cecoban") == null?"":rs.getString("ic_estatus_cecoban").trim();
				String tipoColor 		= "formas";
				if("".equals(estatus_tef) && "".equals(estatus_cec)){
					tipoColor = "cuenta";
					CtaNoValida++;
				} else {
					if(!"99".equals(estatus_cec)){
						tipoColor = "cuenta";
						CtaNoValida++;
					}
				}

				registro			= new HashMap();
				registro.put("RFC",					rs.getString("cg_rfc")								);
				registro.put("NOMBRE_PROVEEDOR",	rs.getString("cg_razon_social")					);
				registro.put("TOTAL_DOCUMENTOS",	rs.getString("ig_total_documentos")				);
				registro.put("MONEDA",				rs.getString("cd_nombre")							);
				registro.put("MONTO",				Comunes.formatoMN(rs.getString("fn_importe")));
				registro.put("BANCO",				rs.getString("ic_bancos_tef")						);
				registro.put("TIPO_CUENTA",		rs.getString("ic_tipo_cuenta")					);
				registro.put("NUMERO_CUENTA",		rs.getString("cg_cuenta")							);
				registro.put("ESTATUS_CECOBAN",	estatus_cec												);
				registros.add(registro);

				int 		rs_total_documento 	= rs.getInt("ig_total_documentos");
				double 	rs_importe				= rs.getDouble("fn_importe");
				iNoDoctos += rs_total_documento;
				dImpTotal += rs_importe;
				if("99".equals(estatus_cec)) {
					iNoDoctosDisp  += rs_total_documento;
					dImpTotalDisp  += rs_importe;
				} else {
					iNoDoctosError += rs_total_documento;
					dImpTotalError += rs_importe;
				}

			}

			// Agregar totales
			if( iReg > 0 ) {

				if( iNoDoctosDisp > 0 ) {

					registro = new HashMap();
					registro.put("RFC",					"Total por Dispersar:"									);
					registro.put("NOMBRE_PROVEEDOR",	""																);
					registro.put("TOTAL_DOCUMENTOS",	String.valueOf(iNoDoctosDisp)							);
					registro.put("MONEDA",				""																);
					registro.put("MONTO",				"$ " + Comunes.formatoDecimal(dImpTotalDisp, 2)	);
					registro.put("BANCO",				""																);
					registro.put("TIPO_CUENTA",		""																);
					registro.put("NUMERO_CUENTA",		""																);
					registro.put("ESTATUS_CECOBAN",	""																);
					registrosTotales.add(registro);

				}

				if( iNoDoctosError > 0 ) {

					registro = new HashMap();
					registro.put("RFC",					"Total con Error:"										);
					registro.put("NOMBRE_PROVEEDOR",	""																);
					registro.put("TOTAL_DOCUMENTOS",	String.valueOf(iNoDoctosError)						);
					registro.put("MONEDA",				""																);
					registro.put("MONTO",				"$ " + Comunes.formatoDecimal(dImpTotalError, 2));
					registro.put("BANCO",				""																);
					registro.put("TIPO_CUENTA",		""																);
					registro.put("NUMERO_CUENTA",		""																);
					registro.put("ESTATUS_CECOBAN",	""																);
					registrosTotales.add(registro);

				}

				if( iNoDoctosDisp > 0 && iNoDoctosError > 0 ) {

					registro = new HashMap();
					registro.put("RFC",					"Suma Total:"												);
					registro.put("NOMBRE_PROVEEDOR",	""																);
					registro.put("TOTAL_DOCUMENTOS",	String.valueOf(iNoDoctos)								);
					registro.put("MONEDA",				""																);
					registro.put("MONTO",				"$ " + Comunes.formatoDecimal(dImpTotal, 2)		);
					registro.put("BANCO",				""																);
					registro.put("TIPO_CUENTA",		""																);
					registro.put("NUMERO_CUENTA",		""																);
					registro.put("ESTATUS_CECOBAN",	""																);
					registrosTotales.add(registro);

				}

			}

			detalleTotales.put( "numeroDocumentosDispersion",	new Integer(iNoDoctosDisp)	);
			detalleTotales.put( "importeTotalDispersion", 		new Double(dImpTotalDisp)	);
			detalleTotales.put( "numeroDocumentosError",			new Integer(iNoDoctosError));
			detalleTotales.put( "importeTotalError", 				new Double(dImpTotalError)	);
			detalleTotales.put( "origen", 							"FISOS"							);
			detalleTotales.put( "numeroRegistros", 				new Integer(iReg)				);

			/*

				// Nota: Parametros que se enviaban en la version original

				impresion.put("noIf",			noIf				);
				impresion.put("noEpo", 			noEpo				);
				impresion.put("noMoneda", 		noMoneda			);
				impresion.put("fechaReg", 		fechaReg			);
				impresion.put("query", 			query				);
				impresion.put("tipoQuery",		""					);
				detalleTotales.put("CtaNoValida", 	CtaNoValida		);

			*/

		} catch( Exception e ){

			log.error("consultaDispersionFISOS(Exception)");
			log.error("consultaDispersionFISOS.noEpo    = <" + noEpo    + ">");
			log.error("consultaDispersionFISOS.noMoneda = <" + noMoneda + ">");
			log.error("consultaDispersionFISOS.noIf     = <" + noIf     + ">");
			log.error("consultaDispersionFISOS.fechaReg = <" + fechaReg + ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al realizar la consulta");

		} finally {

			if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
			if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}

			log.info("consultaDispersionFISOS(S)");

		}

		consulta.put( "registros", 			registros			);
		consulta.put( "registrosTotales", 	registrosTotales 	);
		consulta.put( "detalleTotales", 		detalleTotales	 	);

		return consulta;

	}

	/**
	 * Devuelve la clave y la descripcion de la EPO que esta afiliada al IF FIDEICOMISO.
	 * PANTALLA: ADMIN_NAFIN - ADMINISTRACI�N � DISPERSI�N � IF FIDEICOMISO
	 * @throws AppException
	 *
	 * @param claveIfFideicomiso <tt>String</tt> con la clave del Intermediario Financiero
	 *                           que opera Fideicomiso para el Desarrollo de Proveedores.
	 *
	 * @return <tt>HashMap</tt> con la clave (<tt>COMCAT_EPO.IC_EPO</tt>) y la descripcion
	 * (<tt>COMCAT_EPO.CG_RAZON_SOCIAL</tt>) de la EPO.
	 *
	 * @since F017 - 2013; 11/09/2013 12:16:45 p.m.
	 * @author jshernandez
	 *
	 */
	public HashMap getEPOQueOperaFideicomiso(String claveIfFideicomiso)
		throws AppException {

		log.info("getEPOQueOperaFideicomiso(E)");

		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement ps 			= null;
		ResultSet 			rs 			= null;
		StringBuffer		query 		= new StringBuffer();

		HashMap				resultado 	= null;

		try {

			// Conectarse a la base de datos
			con.conexionDB();

			// Preparar Query
			query.append(
				"SELECT                                                      "  +
				"   CE.IC_EPO            AS CLAVE,                           "  +
				"   CE.CG_RAZON_SOCIAL   AS DESCRIPCION                      "  +
				"FROM                                                        "  +
				"   COMCAT_EPO 					CE,                            "  +
				"   COMREL_IF_EPO 				IE,                            "  +
				"   COMCAT_IF 						I,                             "  +
				"   COM_PARAMETRIZACION_EPO 	PARAM_EPO                      "  +
				"WHERE                                                       "  +
				"       CE.IC_EPO                   = IE.IC_EPO              "  +
				"   and I.IC_IF                     = IE.IC_IF               "  +
				"   and I.IC_IF                     = ?                      "  +
				"   and IE.CS_VOBO_NAFIN            = 'S'                    "  +
				"   and CE.CS_HABILITADO            = 'S'                    "  +
				"   and IE.CS_BLOQUEO               = 'N'                    "  +
				"   and CE.IC_EPO                   = PARAM_EPO.IC_EPO       "  +
				"   and PARAM_EPO.CC_PARAMETRO_EPO  = 'CS_OPERA_FIDEICOMISO' "  +
				"   and PARAM_EPO.CG_VALOR          = 'S'                    "  +
				"ORDER BY CE.CG_RAZON_SOCIAL                                 "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,Integer.parseInt(claveIfFideicomiso));
			rs = ps.executeQuery();

			if( rs.next() ){

				String clave 			= (rs.getString("CLAVE")			== null)?"":rs.getString("CLAVE");
				String descripcion	= (rs.getString("DESCRIPCION")	== null)?"":rs.getString("DESCRIPCION");

				resultado 				= new HashMap();
				resultado.put( "clave",			clave       );
				resultado.put( "descripcion", descripcion );

			}

			if( rs.next() ){
				throw new AppException("El IF Fideicomiso est� afiliado a m�s de una EPO.");
			}

		} catch( AppException a ){

			log.error("getEPOQueOperaFideicomiso(Exception)");
			log.error("getEPOQueOperaFideicomiso.claveIfFideicomiso = <" + claveIfFideicomiso+ ">");
			a.printStackTrace();

			throw a;

		} catch(Exception e){

			log.error("getEPOQueOperaFideicomiso(Exception)");
			log.error("getEPOQueOperaFideicomiso.claveIfFideicomiso = <" + claveIfFideicomiso+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al obtener la EPO afiliada al IF Fideicomiso: " + e.getMessage() );

		} finally {

			if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
			if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}

			log.info("getEPOQueOperaFideicomiso(S)");

		}

		return resultado;

	}

	/**
	 *
	 * Este m�todo devuelve el Query que trae el detalle de los documentos a dispersar para
	 * IF FIDEICOMISO seleccionado.
	 *   PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - IFs FIDEICOMISO
	 *
	 * @throws AppException
	 *
	 * @param icFolioFF          ID del Folio de Flujo de Fondos.
	 * @param claveIfFideicomiso <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF) que opera
	 *                           fideicomiso para el desarrollo de proveedores (COMCAT_IF.CS_OPERA_FIDEICOMSO).
	 * @param sNoEPO	           <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param sNoIF		        <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF).
	 * @param sNoMoneda          <tt>String</tt> con la clave de la Moneda (COMCAT_MONEDA.IC_MONEDA).
	 * @param fechaReg	        <tt>String</tt> con la fecha de registro,con formato <tt>DD/MM/AAAA</tt>.
	 *
	 * @return <tt>String</tt> con el query para realizar la consulta.
	 *
	 * @author jshernandez
	 * @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 13/09/2013 01:28:11 p.m.
	 *
	 */
	public String getQueryDispIfFideicomiso( String icFolioFF, String claveIfFideicomiso, String sNoEPO, String sNoIF, String sNoMoneda )
		throws NafinException {

		log.info("getQueryDispIfFideicomiso(E)");

		String query 			= "";
		String sFechaSigHab	= getFechaSiguienteHabil().replace('/','-');

		String claveTipoCuenta 	= (sNoMoneda != null && sNoMoneda.equals("54"))?claveCuentaSwift:"40";// Cuenta Swift: // Codigo cuenta cliente

		try {
			query =
				" SELECT   /*+use_nl(d ds s n p c m)*/"   +
				"           '"+icFolioFF+"' AS ic_flujo_fondos, SUM (ds.in_importe_recibir) AS fn_importe,"   +
				"           COUNT (ds.ic_documento) AS ig_total_documentos, m.cd_nombre,"   +
				"           d.ic_pyme, p.cg_rfc, p.cg_razon_social, c.ic_cuenta,"   +
				"           DECODE (c.ic_estatus_cecoban, 99, 0, -1) AS estatusoper,"   +
				"           c.ic_bancos_tef, c.ic_tipo_cuenta, c.cg_cuenta, c.ic_estatus_tef,"   +
				"           c.ic_estatus_cecoban, TO_CHAR(SYSDATE,'dd/mm/yyyy') as fechaReg, '"+sFechaSigHab+"' as fechaTrasf, '"+sFechaSigHab+"' as fechaApli, 'TEF-NE' as refRastreo, 'FISO AAA PH DP  '||TO_CHAR(sysdate,'dd mm yyyy') as leyendaRastreo"   +
				"           ,'DispersionEJB::getQueryDispIfFideicomiso'"   +
				"     FROM com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          com_solicitud s,"   +
				"          comrel_nafin n,"   +
				"          comcat_pyme p,"   +
				"          com_cuentas c,"   +
				"          comcat_moneda m"   +
				"    WHERE ds.ic_documento = d.ic_documento"   +
				"      AND ds.ic_documento = s.ic_documento"   +
				"      AND d.ic_pyme = p.ic_pyme"   +
				"      AND d.ic_epo = ds.ic_epo"   +
				"      AND c.ic_moneda = m.ic_moneda"   +
				"      AND c.ic_nafin_electronico = n.ic_nafin_electronico"   +
				"      AND c.ic_epo = d.ic_epo"   +
				"      AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"      AND m.ic_moneda = "+sNoMoneda+
				"      AND d.ic_moneda = m.ic_moneda"+
				"      AND ds.ic_epo = "+sNoEPO+
				"      AND n.cg_tipo = 'P'"   +
				"      AND ds.ic_if = "+claveIfFideicomiso+ // Intermediario Financiero que la PYME selecciono para cederle los documentos
				"      AND d.ic_if = "+sNoIF+ // IF con quien se aplic� el Redescuento
				"      AND d.ic_estatus_docto = 4"   +
				"      AND c.ic_producto_nafin = 1"   +
				"      AND c.ic_tipo_cuenta = " + claveTipoCuenta +
				"      AND s.df_operacion >= TRUNC (SYSDATE)"   +
				"      AND s.df_operacion < (TRUNC (SYSDATE) + 1)"   +
				"      AND d.cs_dscto_especial != 'C' "   +
				"      AND ds.cs_opera_fiso = 'S' "  + // Documentos registrados bajo la modalidad de Fideicomiso para Desarrollo de Proveedores
				" GROUP BY c.ic_estatus_cecoban,"   +
				"          c.ic_cuenta,"   +
				"          d.ic_pyme,"   +
				"          p.cg_rfc,"   +
				"          p.cg_razon_social,"   +
				"          m.cd_nombre,"   +
				"          c.ic_bancos_tef,"   +
				"          c.ic_tipo_cuenta,"   +
				"          c.cg_cuenta,"   +
				"          c.ic_estatus_tef,"   +
				"          c.ic_estatus_cecoban"   +
				" UNION ALL"   +
				" SELECT   /*+use_nl(d ds s n p m)*/"   +
				"           '"+icFolioFF+"' AS ic_flujo_fondos, SUM (ds.in_importe_recibir) AS fn_importe,"   +
				"           COUNT (ds.ic_documento) AS ig_total_documentos, m.cd_nombre,"   +
				"           d.ic_pyme, p.cg_rfc, p.cg_razon_social, 0, -1 AS estatusoper,"   +
				"           0 AS ic_bancos_tef, 0 AS ic_tipo_cuenta, '0' AS cg_cuenta,"   +
				"           0 AS ic_estatus_tef, 0 AS ic_estatus_cecoban, TO_CHAR(SYSDATE,'dd/mm/yyyy') as fechaReg, '"+sFechaSigHab+"' as fechaTrasf, '"+sFechaSigHab+"' as fechaApli, 'TEF-NE' as refRastreo, 'FISO AAA PH DP  '||TO_CHAR(sysdate,'dd mm yyyy') as leyendaRastreo"   +
				"           ,'DispersionEJB::getQueryDispIfFideicomiso'"   +
				"     FROM com_documento d,"   +
				"          com_docto_seleccionado ds,"   +
				"          com_solicitud s,"   +
				"          comrel_nafin n,"   +
				"          comcat_pyme p,"   +
				"          comcat_moneda m"   +
				"    WHERE d.ic_documento = ds.ic_documento"   +
				"      AND d.ic_pyme = p.ic_pyme"   +
				"      AND d.ic_epo = ds.ic_epo"   +
				"      AND ds.ic_documento = s.ic_documento"   +
				"      AND n.ic_epo_pyme_if = p.ic_pyme"   +
				"      AND NOT EXISTS ("   +
				"             SELECT 1"   +
				"               FROM com_cuentas c"   +
				"              WHERE c.ic_moneda = m.ic_moneda"   +
				"                AND n.ic_nafin_electronico = c.ic_nafin_electronico"   +
				"                AND c.ic_producto_nafin = 1"   +
				"                AND c.ic_tipo_cuenta = " + claveTipoCuenta + ")"   +
				"      AND m.ic_moneda = "+sNoMoneda+
				"      AND d.ic_moneda = m.ic_moneda"+
				"      AND ds.ic_epo = "+sNoEPO+
				"      AND n.cg_tipo = 'P'"   +
				"      AND ds.ic_if = "+claveIfFideicomiso+ // Intermediario Financiero que la PYME selecciono para cederle los documentos
				"      AND d.ic_if = "+sNoIF+ // IF con quien se aplic� el Redescuento
				"      AND d.ic_estatus_docto = 4"   +
				"      AND s.df_operacion >= TRUNC (SYSDATE)"   +
				"      AND s.df_operacion < (TRUNC (SYSDATE) + 1)"   +
				"      AND d.cs_dscto_especial != 'C' "   +
				"      AND ds.cs_opera_fiso = 'S' "  + // Documentos registrados bajo la modalidad de Fideicomiso para Desarrollo de Proveedores
				" GROUP BY 0, d.ic_pyme, p.cg_rfc, p.cg_razon_social, m.cd_nombre"  ;

		} catch(Exception e) {

			log.error("getQueryDispIfFideicomiso(Exception)");
			log.error("getQueryDispIfFideicomiso.icFolioFF          = <" + icFolioFF          + ">");
			log.error("getQueryDispIfFideicomiso.claveIfFideicomiso = <" + claveIfFideicomiso + ">");
			log.error("getQueryDispIfFideicomiso.sNoEPO             = <" + sNoEPO             + ">");
			log.error("getQueryDispIfFideicomiso.sNoIF              = <" + sNoIF              + ">");
			log.error("getQueryDispIfFideicomiso.sNoMoneda          = <" + sNoMoneda          + ">");
			log.error("getQueryDispIfFideicomiso.query              = <" + query              + ">");
			e.printStackTrace();

			throw new NafinException("SIST0001");

		} finally {

			log.error("getQueryDispIfFideicomiso(S)");

		}

		return query;

	}//getQueryDispIfFideicomiso

	/**
	 * Devuelve el detalle de los documentos a dispersar para IF FIDEICOMISO seleccionado.
	 *   PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - IFs FIDEICOMISO
	 *
	 * @throws AppException
	 *
	 * @param claveIfFideicomiso <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF) que opera
	 *                           fideicomiso para el desarrollo de proveedores  (COMCAT_IF.CS_OPERA_FIDEICOMSO).
	 * @param noEpo	 <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param noIf		 <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF).
	 * @param noMoneda <tt>String</tt> con la clave de la Moneda (COMCAT_MONEDA.IC_MONEDA).
	 * @param fechaReg <tt>String</tt> con la fecha de registro,con formato <tt>DD/MM/AAAA</tt>.
	 *
	 * @return <tt>HashMap</tt> con del detalle de la consulta.
	 *
	 * @author jshernandez
	 * @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 13/09/2013 11:40:11 a.m.
	 *
	 */
	public HashMap consultaDispersionIfFideicomiso( String claveIfFideicomiso, String noEpo, String noIf, String noMoneda, String fechaReg )
		throws AppException {

		log.info("consultaDispersionIfFideicomiso(E)");

		HashMap 				consulta				= new HashMap();
		List					registros 			= new ArrayList();
		List					registrosTotales	= new ArrayList();
		HashMap				detalleTotales		= new HashMap();

		HashMap				registro				= null;

		AccesoDB				con					= new AccesoDB();
		PreparedStatement	ps						= null;
		ResultSet			rs						= null;
		String 				query 				= null;

		try {

			// Conectarse a la Base de Datos
			con.conexionDB();

			int 			iReg 					= 0;
			int 			iNoDoctos 			= 0;
			int 			iNoDoctosDisp 		= 0;
			int 			iNoDoctosError 	= 0;

			double 		dImpTotal 			= 0;
			double 		dImpTotalDisp 		= 0;
			double 		dImpTotalError 	= 0;

			query 								= getQueryDispIfFideicomiso("", claveIfFideicomiso, noEpo, noIf, noMoneda );
			log.info("consultaDispersionIfFideicomiso.query = <" + query+ ">");

			ps 				 = con.queryPrecompilado(query);
			rs 				 = ps.executeQuery();
			int CtaNoValida = 0;
			while( rs.next() ){

				iReg++;
				if(iReg == 1) {
					/*
					RFC
					NOMBRE_PROVEEDOR
					TOTA_DOCUMENTOS
					MONEDA
					MONTO
					BANCO
					TIPO_CUENTA
					NUMERO_CUENTA
					ERROR
					ESTATUS_CECOBAN
					*/
				}

				//String estatus_tef	= rs.getString("ic_estatus_tef")		 == null?"":rs.getString("ic_estatus_tef").trim();
				String estatus_cec	= rs.getString("ic_estatus_cecoban") == null?"":rs.getString("ic_estatus_cecoban").trim();
				String cg_cuenta 		= rs.getString("cg_cuenta")			 == null?"":rs.getString("cg_cuenta");
				String ic_cuenta 		= rs.getString("ic_cuenta")			 == null?"":rs.getString("ic_cuenta");
				String tipoColor 		= "formas";

				// Sin cuenta en com_cuentas
				if( "0".equals( ic_cuenta ) || "0".equals( cg_cuenta ) || "".equals( cg_cuenta )  ){
					tipoColor = "cuenta";
					CtaNoValida++;
				// Cuando en ic_estatus_cecoban diferente de 99 en com_cuentas
				} else if( !"99".equals(estatus_cec) ){
					tipoColor = "cuenta";
					CtaNoValida++;
				}

				// Sin cuenta en com_cuentas
				String error = null;
				if( "0".equals( ic_cuenta ) || "0".equals( cg_cuenta ) || "".equals( cg_cuenta )  ){
					error = "Sin N�mero de Cuenta";
				// Cuando en ic_estatus_cecoban diferente de 99 en com_cuentas
				} else if( !"99".equals(estatus_cec) ){
					error = "Sin Cuenta Autorizada";
				// No se encontr� ning�n error
				} else {
					error	= "N/A";
				}

				registro			= new HashMap();
				registro.put("RFC",					rs.getString("cg_rfc")								);
				registro.put("NOMBRE_PROVEEDOR",	rs.getString("cg_razon_social")					);
				registro.put("TOTAL_DOCUMENTOS",	rs.getString("ig_total_documentos")				);
				registro.put("MONEDA",				rs.getString("cd_nombre")							);
				registro.put("MONTO",				Comunes.formatoMN(rs.getString("fn_importe")));
				registro.put("BANCO",				rs.getString("ic_bancos_tef")						);
				registro.put("TIPO_CUENTA",		rs.getString("ic_tipo_cuenta")					);
				registro.put("NUMERO_CUENTA",		cg_cuenta												);
				registro.put("ERROR",				error 													);
				registro.put("ESTATUS_CECOBAN",	estatus_cec												);
				registros.add(registro);

				int 		rs_total_documento 	= rs.getInt("ig_total_documentos");
				double 	rs_importe				= rs.getDouble("fn_importe");
				iNoDoctos += rs_total_documento;
				dImpTotal += rs_importe;
				if("99".equals(estatus_cec)) {
					iNoDoctosDisp  += rs_total_documento;
					dImpTotalDisp  += rs_importe;
				} else {
					iNoDoctosError += rs_total_documento;
					dImpTotalError += rs_importe;
				}

			}

			// Agregar totales
			if( iReg > 0 ) {

				if( iNoDoctosDisp > 0 ) {

					registro = new HashMap();
					registro.put("RFC",					"Total por Dispersar:"									);
					registro.put("NOMBRE_PROVEEDOR",	""																);
					registro.put("TOTAL_DOCUMENTOS",	String.valueOf(iNoDoctosDisp)							);
					registro.put("MONEDA",				""																);
					registro.put("MONTO",				"$ " + Comunes.formatoDecimal(dImpTotalDisp, 2)	);
					registro.put("BANCO",				""																);
					registro.put("TIPO_CUENTA",		""																);
					registro.put("NUMERO_CUENTA",		""																);
					registro.put("ERROR",				""																);
					registro.put("ESTATUS_CECOBAN",	""																);
					registrosTotales.add(registro);

				}

				if( iNoDoctosError > 0 ) {

					registro = new HashMap();
					registro.put("RFC",					"Total con Error:"										);
					registro.put("NOMBRE_PROVEEDOR",	""																);
					registro.put("TOTAL_DOCUMENTOS",	String.valueOf(iNoDoctosError)						);
					registro.put("MONEDA",				""																);
					registro.put("MONTO",				"$ " + Comunes.formatoDecimal(dImpTotalError, 2));
					registro.put("BANCO",				""																);
					registro.put("TIPO_CUENTA",		""																);
					registro.put("NUMERO_CUENTA",		""																);
					registro.put("ERROR",				""																);
					registro.put("ESTATUS_CECOBAN",	""																);
					registrosTotales.add(registro);

				}

				if( iNoDoctosDisp > 0 && iNoDoctosError > 0 ) {

					registro = new HashMap();
					registro.put("RFC",					"Suma Total:"												);
					registro.put("NOMBRE_PROVEEDOR",	""																);
					registro.put("TOTAL_DOCUMENTOS",	String.valueOf(iNoDoctos)								);
					registro.put("MONEDA",				""																);
					registro.put("MONTO",				"$ " + Comunes.formatoDecimal(dImpTotal, 2)		);
					registro.put("BANCO",				""																);
					registro.put("TIPO_CUENTA",		""																);
					registro.put("NUMERO_CUENTA",		""																);
					registro.put("ERROR",				""																);
					registro.put("ESTATUS_CECOBAN",	""																);
					registrosTotales.add(registro);

				}

			}

			detalleTotales.put( "numeroDocumentosDispersion",	new Integer(iNoDoctosDisp)	);
			detalleTotales.put( "importeTotalDispersion", 		new Double(dImpTotalDisp)	);
			detalleTotales.put( "numeroDocumentosError",			new Integer(iNoDoctosError));
			detalleTotales.put( "importeTotalError", 				new Double(dImpTotalError)	);
			detalleTotales.put( "origen", 							"IFFIDEICOMISO"				); // Dispersion FISOS ( IF FIDEICOMISO )
			detalleTotales.put( "numeroRegistros", 				new Integer(iReg)				);

			/*

				// Nota: Parametros que se enviaban en la version original

				impresion.put("noIf",			noIf				);
				impresion.put("noEpo", 			noEpo				);
				impresion.put("noMoneda", 		noMoneda			);
				impresion.put("fechaReg", 		fechaReg			);
				impresion.put("query", 			query				);
				impresion.put("tipoQuery",		""					);
				detalleTotales.put("CtaNoValida", 	CtaNoValida		);

			*/

		} catch( Exception e ){

			log.error("consultaDispersionIfFideicomiso(Exception)");
			log.error("consultaDispersionIfFideicomiso.claveIfFideicomiso	= <" + claveIfFideicomiso	+ ">");
			log.error("consultaDispersionIfFideicomiso.noEpo    				= <" + noEpo    				+ ">");
			log.error("consultaDispersionIfFideicomiso.noIf     				= <" + noIf     				+ ">");
			log.error("consultaDispersionIfFideicomiso.noMoneda 				= <" + noMoneda 				+ ">");
			log.error("consultaDispersionIfFideicomiso.fechaReg 				= <" + fechaReg 				+ ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al realizar la consulta");

		} finally {

			if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
			if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}

			log.info("consultaDispersionIfFideicomiso(S)");

		}

		consulta.put( "registros", 			registros			);
		consulta.put( "registrosTotales", 	registrosTotales 	);
		consulta.put( "detalleTotales", 		detalleTotales	 	);

		return consulta;

	}

	/**
	 * Devuelve el query se utilizar� para consultar el detalle por documento de la dispersion para un IF FIDEICOMISO
	 * en espec�fico.
	 *   PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - IFs FIDEICOMISO
	 *
	 * @throws AppException
	 *
	 * @param claveIfFideicomiso  <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF) que opera
	 *                            fideicomiso para el desarrollo de proveedores  (COMCAT_IF.CS_OPERA_FIDEICOMSO).
	 * @param claveEPO	         <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param claveIF		         <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF).
	 * @param claveMoneda         <tt>String</tt> con la clave de la Moneda (COMCAT_MONEDA.IC_MONEDA).
	 * @param fechaRegistro       <tt>String</tt> con la fecha de registro,con formato <tt>DD/MM/AAAA</tt>.
	 *
	 * @return                    <tt>HashMap</tt> con el query de consulta en el atributo: <tt>text</tt> y con
	 *                            los parametros de consulta en el atributo: <tt>parameters</tt>.
	 *
	 *
	 * @since             F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 17/09/2013 12:44:02 p.m.
	 * @author            jshernandez
	 *
	 */
	public HashMap getQueryDetalleErrorDispersionIfFideicomiso( String claveIfFideicomiso, String claveEPO, String claveIF, String claveMoneda, String fechaRegistro )
		throws AppException {

		log.info("getQueryDetalleErrorDispersionIfFideicomiso(E)");

		HashMap 			query 		= new HashMap();
		StringBuffer	text			= new StringBuffer();
		List				parameters	= new ArrayList();

		try {

			String claveTipoCuenta 	= (claveMoneda != null && claveMoneda.equals("54"))?claveCuentaSwift:"40";// Cuenta Swift: // Codigo cuenta cliente

			text.append(
				" SELECT  /*+use_nl(d ds s n p c m)*/ "  +
				"			  p.cg_rfc,                  "  + // RFC
				"          p.cg_razon_social,         "  + // Nombre del Proveedor
				"			  d.ig_numero_docto,         "  + // N�mero de Documento
				"          ds.in_importe_recibir,     "  + // Importe del Documento
				"          c.ic_bancos_tef,           "  + // Clave del banco de la PYME
				"          c.ic_tipo_cuenta,          "  + // Tipo de Cuenta
				"          c.cg_cuenta,               "  + // Cuenta CLABE / SWIFT
				"          c.ic_estatus_cecoban,      "  + // Estatus Cecoban
				"          'DispersionEJB::getQueryDetalleErrorDispersionIfFideicomiso'"  +
				"     FROM com_documento          d, "  +
				"          com_docto_seleccionado ds,"  +
				"          com_solicitud          s, "  +
				"          comrel_nafin           n, "  +
				"          comcat_pyme            p, "  +
				"          com_cuentas            c, "  +
				"          comcat_moneda          m  "  +
				"    WHERE ds.ic_documento        =  d.ic_documento         "  +
				"      AND ds.ic_documento        =  s.ic_documento         "  +
				"      AND d.ic_pyme              =  p.ic_pyme              "  +
				"      AND d.ic_epo               =  ds.ic_epo              "  +
				"      AND c.ic_moneda            =  m.ic_moneda            "  +
				"      AND c.ic_nafin_electronico =  n.ic_nafin_electronico "  +
				"      AND c.ic_epo               =  d.ic_epo               "  +
				"      AND n.ic_epo_pyme_if       =  p.ic_pyme              "  +
				"      AND m.ic_moneda            =  ?                      "  + // claveMoneda
				"      AND d.ic_moneda            =  m.ic_moneda            "  +
				"      AND ds.ic_epo              =  ?                      "  + // claveEPO
				"      AND n.cg_tipo              =  'P'"   +
				"      AND ds.ic_if               =  ?                      "  + // claveIfFideicomiso // Intermediario Financiero que la PYME selecciono para cederle los documentos
				"      AND d.ic_if                =  ?                      "  + // claveIF            // IF con quien se aplic� el Redescuento
				"      AND d.ic_estatus_docto     =  4                      "  +
				"      AND c.ic_producto_nafin    =  1                      "  +
				"      AND c.ic_tipo_cuenta       =  ?                      "  + // claveTipoCuenta
				"      AND s.df_operacion         >= TRUNC (SYSDATE)"   +
				"      AND s.df_operacion         <  (TRUNC (SYSDATE) + 1)"   +
				"      AND d.cs_dscto_especial    != 'C' "  +
				"      AND ds.cs_opera_fiso       =  'S' "  + // Documentos registrados bajo la modalidad de Fideicomiso para Desarrollo de Proveedores
				"      AND ( c.ic_estatus_cecoban !=  99 OR c.ic_estatus_cecoban IS NULL ) "  + // Estatus Cecoban
				" UNION ALL"   +
				" SELECT   /*+use_nl(d ds s n p m)*/  "  +
				"			  p.cg_rfc,                  "  + // RFC
				"          p.cg_razon_social,         "  + // Nombre del Proveedor
				"			  d.ig_numero_docto,         "  + // N�mero de Documento
				"          ds.in_importe_recibir,     "  + // Importe del Documento
				"          0   AS ic_bancos_tef,      "  + // Clave del banco de la PYME
				"          0   AS ic_tipo_cuenta,     "  + // Tipo de Cuenta
				"          '0' AS cg_cuenta,          "  + // Cuenta CLABE / SWIFT
				"          0   AS ic_estatus_cecoban, "  + // Estatus Cecoban
				"          'DispersionEJB::getQueryDetalleErrorDispersionIfFideicomiso'"  +
				"     FROM com_documento          d, "  +
				"          com_docto_seleccionado ds,"  +
				"          com_solicitud          s, "  +
				"          comrel_nafin           n, "  +
				"          comcat_pyme            p, "  +
				"          comcat_moneda          m  "  +
				"    WHERE d.ic_documento      = ds.ic_documento"  +
				"      AND d.ic_pyme           = p.ic_pyme"  +
				"      AND d.ic_epo            = ds.ic_epo"  +
				"      AND ds.ic_documento     = s.ic_documento"  +
				"      AND n.ic_epo_pyme_if    = p.ic_pyme"  +
				"      AND NOT EXISTS (        "  +
				"             SELECT 1"  +
				"               FROM com_cuentas c"  +
				"              WHERE c.ic_moneda            = m.ic_moneda            "  +
				"                AND n.ic_nafin_electronico = c.ic_nafin_electronico "  +
				"                AND c.ic_producto_nafin    = 1                      "  +
				"                AND c.ic_tipo_cuenta       = ? )                    "  + // claveTipoCuenta
				"      AND m.ic_moneda         = ?           "  + // claveMoneda
				"      AND d.ic_moneda         = m.ic_moneda "  +
				"      AND ds.ic_epo           = ?           "  + // claveEPO
				"      AND n.cg_tipo           = 'P'         "  +
				"      AND ds.ic_if            = ?           "  + // claveIfFideicomiso // Intermediario Financiero que la PYME selecciono para cederle los documentos
				"      AND d.ic_if             = ?           "  + // claveIF            // IF con quien se aplic� el Redescuento
				"      AND d.ic_estatus_docto  = 4           "  +
				"      AND s.df_operacion      >= TRUNC (SYSDATE)       "  +
				"      AND s.df_operacion      <  (TRUNC (SYSDATE) + 1) "  +
				"      AND d.cs_dscto_especial != 'C'                   "  +
				"      AND ds.cs_opera_fiso    =  'S'                   " // Documentos registrados bajo la modalidad de Fideicomiso para Desarrollo de Proveedores
			);

			parameters.add( new Integer( claveMoneda )        );
			parameters.add( new Integer( claveEPO )           );
			parameters.add( new Integer( claveIfFideicomiso ) );
			parameters.add( new Integer( claveIF )            );
			parameters.add( new Integer( claveTipoCuenta )    );
			parameters.add( new Integer( claveTipoCuenta )    );
			parameters.add( new Integer( claveMoneda )        );
			parameters.add( new Integer( claveEPO )           );
			parameters.add( new Integer( claveIfFideicomiso ) );
			parameters.add( new Integer( claveIF )            );

			query.put("text",       text.toString()  );
			query.put("parameters", parameters       );

		}catch(Exception e){

			log.error("getQueryDetalleDispersionIfFideicomiso(Exception)");
			log.error("getQueryDetalleDispersionIfFideicomiso.claveIfFideicomiso = <" + claveIfFideicomiso + ">");
			log.error("getQueryDetalleDispersionIfFideicomiso.claveEPO           = <" + claveEPO           + ">");
			log.error("getQueryDetalleDispersionIfFideicomiso.claveIF            = <" + claveIF            + ">");
			log.error("getQueryDetalleDispersionIfFideicomiso.claveMoneda        = <" + claveMoneda        + ">");
			log.error("getQueryDetalleDispersionIfFideicomiso.fechaRegistro      = <" + fechaRegistro      + ">");
			e.printStackTrace();

			throw new AppException("Ocurri� un error al preparar la consulta del detalle de los documentos.");

		}finally{

			log.info("getQueryDetalleDispersionIfFideicomiso(S)");

		}

		return query;

	}

	/**
	 *
	 *	Este metodo devuelve el query que se utilza para obtener el detalle del monitoreo de la dispersion
	 *    por IF FIDEICOMISO.
	 *
	 *    	PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - MONITOR ( Tipo de Monitoreo: IF's FIDEICOMISO )
	 *
	 * @param claveIfFideicomiso  <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF) que opera
	 *                            fideicomiso para el desarrollo de proveedores  (COMCAT_IF.CS_OPERA_FIDEICOMSO).
	 * @param claveEPO	         <tt>String</tt> con la clave de la EPO (COMCAT_EPO.IC_EPO).
	 * @param claveIF		         <tt>String</tt> con la clave del IF (COMCAT_IF.IC_IF).
	 * @param pymeSinCuentaClabe 	<tt>String</tt> con donde se especifica si la Cuenta no es CLABE.
	 * @param radioHabilitadas  	<tt>String</tt> con el parametro cs_aceptacion de la tabla comrel_pyme_epo.
	 *
	 * @return El query para hacer la consulta.
	 *
	 * @since             F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 19/09/2013 05:06:36 p.m.
	 * @author            jshernandez
	 *
	 */
	 public String getQueryDetalleMonitoreoDispersionPorIfFideicomiso(
	 	 String claveIfFideicomiso,
	 	 String claveEPO,
	 	 String claveIF,
	 	 String pymeSinCuentaClabe,
	 	 String radioHabilitadas
	 ){

	 	log.info("getQueryDetalleMonitoreoDispersionPorIfFideicomiso(E)");

		StringBuffer condicion = new StringBuffer(
			"      AND s.df_operacion   >=  TRUNC (SYSDATE)       "   +
			"      AND s.df_operacion   <   (TRUNC (SYSDATE) + 1) "   +
			"      AND ds.cs_opera_fiso =   'S'                   "    // Documentos registrados bajo la modalidad de Fideicomiso para Desarrollo de Proveedores
		);

		claveIfFideicomiso	= ( claveIfFideicomiso 	== null )?"":claveIfFideicomiso;
		claveEPO 				= ( claveEPO 				== null )?"":claveEPO;
		claveIF					= ( claveIF 				== null )?"":claveIF;
		pymeSinCuentaClabe	= ( pymeSinCuentaClabe 	== null )?"":pymeSinCuentaClabe;
		radioHabilitadas		= ( radioHabilitadas  	== null )?"":radioHabilitadas;

		if(        !"".equals(claveIfFideicomiso) ) {
			condicion.append("    AND ds.ic_if  = "+claveIfFideicomiso + " "); // claveIfFideicomiso // Intermediario Financiero que la PYME selecciono para cederle los documentos
		}
		if(        !"".equals(claveEPO)           ) {
			condicion.append("    AND ds.ic_epo = "+claveEPO           + " ");
		}
		if(        !"".equals(claveIF)            ) {
			condicion.append("    AND d.ic_if   = "+claveIF            + " "); // claveIF            // IF con quien se aplic� el Redescuento
		}
		if(        "H".equals(radioHabilitadas)   ) {
			condicion.append("    AND rpe.cs_aceptacion = '"+radioHabilitadas+"' ");
		} else if( "N".equals(radioHabilitadas)   ) {
			condicion.append("    AND rpe.cs_aceptacion != 'H' ");
		}

		StringBuffer qrySentenciaConCuenta 		= new StringBuffer();
		StringBuffer qrySentenciaSinCuenta 		= new StringBuffer();
		StringBuffer qrySentenciaConCuentaUSD 	= new StringBuffer();
		StringBuffer qrySentenciaSinCuentaUSD 	= new StringBuffer();
		StringBuffer qrySentencia 					= new StringBuffer();

		qrySentenciaConCuenta.append(
				" SELECT "   +
				"        /*+ INDEX (CIF CP_COMCAT_IF_PK) USE_NL(CIF) index(c IN_COM_CUENTAS_01_NUK) use_nl(n) */ "   +
				"        ds.ic_if                   AS nomif,         "   +
				"        cif.cg_razon_social,        "   +
				"        epo.cg_razon_social,        "   +
				"        p.cg_razon_social          AS nompyme,       "   +
				"        n.ic_nafin_electronico,     "   +
				"        rpe.cs_aceptacion,          "   +
				"        COUNT(ds.ic_documento)     AS num_doctos,    "   +
				"        SUM(ds.in_importe_recibir) AS total_importe, "   +
				"        c.ic_bancos_tef,            "   +
				"        c.cg_cuenta,                "   +
				"        cec.ic_estatus_cecoban,     "   +
				"        cec.cd_descripcion,         "   +
				"			d.ic_moneda                AS ic_moneda,     "   +
				"        ciffondeo.cg_razon_social  AS nomIfFondeo    "   +
				"   FROM                             "   +
				"        com_documento          d,   "   +
				"        com_docto_seleccionado ds,  "   +
				"        com_solicitud          s,   "   +
				"        comrel_nafin           n,   "   +
				"        comrel_pyme_epo        rpe, "   +
				"        comcat_epo             epo, "   +
				"        comcat_pyme            p,   "   +
				"        comcat_if              cif, "   +
				"        comcat_if              ciffondeo, "   +
				"        com_cuentas            c,   "   +
				"        comcat_estatus_cecoban cec  "   +
				"  WHERE                             "   +
				"        ds.ic_documento        = d.ic_documento  "   +
				"    AND ds.ic_documento        = s.ic_documento  "   +
				"    AND d.ic_pyme              = p.ic_pyme       "   +
				"    AND d.ic_epo               = ds.ic_epo       "   +
				"    AND d.ic_epo               = epo.ic_epo      "   +
				"    AND ds.ic_if               = cif.ic_if       "   +
				"    AND d.ic_if					  = ciffondeo.ic_if "   +
				"    AND rpe.ic_pyme            = d.ic_pyme       "   +
				"    AND rpe.ic_epo             = d.ic_epo        "   +
				"    AND c.ic_estatus_cecoban   = cec.ic_estatus_cecoban (+) "   +
				"    AND c.ic_nafin_electronico = n.ic_nafin_electronico     "   +
				"    AND n.ic_epo_pyme_if       = p.ic_pyme       "   +
				"    AND n.cg_tipo              = 'P'             "   +
				"    AND d.ic_estatus_docto     = 4               "   +
				"    AND c.ic_producto_nafin    = 1               "   +
				"    AND c.ic_tipo_cuenta       = 40              "   +
				"    AND c.ic_epo               = d.ic_epo        "   +
				"    AND d.ic_moneda            = c.ic_moneda     "   +
				"    AND c.ic_moneda            = 1               "   +
				condicion.toString()  +
				"  GROUP BY                   "   +
				"   ds.ic_if,                 "   +
				"   cif.cg_razon_social,      "   +
				"   epo.cg_razon_social,      "   +
				"   p.cg_razon_social,        "   +
				"   n.ic_nafin_electronico,   "   +
				"   rpe.cs_aceptacion,        "   +
				"   c.ic_bancos_tef,          "   +
				"   c.cg_cuenta,              "   +
				"   cec.cd_descripcion,       "   +
				"   cec.ic_estatus_cecoban,   "   +
				"   d.ic_moneda,              "   +
				"   ciffondeo.cg_razon_social "
			);

			qrySentenciaSinCuenta.append(
				"   SELECT                                             "   +
				"        /*+ use_nl(n) */                              "   +
				"        ds.ic_if                    AS nomif,         "   +
				"        cif.cg_razon_social,                          "   +
				"        epo.cg_razon_social,                          "   +
				"        p.cg_razon_social           AS nompyme,       "   +
				"        n.ic_nafin_electronico,                       "   +
				"        rpe.cs_aceptacion,                            "   +
				"        COUNT (ds.ic_documento)     AS num_doctos,    "   +
				"        SUM (ds.in_importe_recibir) AS total_importe, "   +
				"        0,                                            "   +
				"        '0',                                          "   +
				"        0,                                            "   +
				"        '0',                                          "   +
				"			d.ic_moneda                 AS ic_moneda,     "   +
				"        ciffondeo.cg_razon_social   AS nomIfFondeo    "   +
				"   FROM                                               "   +
				"        com_documento          d,   "   +
				"        com_docto_seleccionado ds,  "   +
				"        com_solicitud          s,   "   +
				"        comrel_nafin           n,   "   +
				"        comrel_pyme_epo        rpe, "   +
				"        comcat_pyme            p,   "   +
				"        comcat_epo             epo, "   +
				"        comcat_if              cif, "   +
				"        comcat_if              ciffondeo "   +
				"  WHERE "   +
				"        d.ic_documento     = ds.ic_documento "   +
				"    AND d.ic_pyme          = p.ic_pyme       "   +
				"    AND d.ic_epo           = ds.ic_epo       "   +
				"    AND ds.ic_if           = cif.ic_if       "   +
				"    AND d.ic_if				 = ciffondeo.ic_if "   +
				"    AND rpe.ic_pyme        = d.ic_pyme       "   +
				"    AND rpe.ic_epo         = d.ic_epo        "   +
				"    AND d.ic_epo           = epo.ic_epo      "   +
				"    AND ds.ic_documento    = s.ic_documento  "   +
				"    AND n.ic_epo_pyme_if   = p.ic_pyme       "   +
				"    AND n.cg_tipo          = 'P'             "   +
				"    AND d.ic_moneda        = 1               "   +
				"    AND d.ic_estatus_docto = 4               "   +
				condicion.toString()  +
				"    AND NOT EXISTS(                  "   +
				"                    SELECT           "   +
				"                       1             "   +
				"                    FROM             "   +
				"                       com_cuentas c "   +
				"                    WHERE            "   +
				"                          c.ic_moneda            = 1                      "   +
				"                      AND n.ic_nafin_electronico = c.ic_nafin_electronico "   +
				"                      AND c.ic_producto_nafin    = 1                      "   +
				"                      AND c.ic_tipo_cuenta       = 40                     "   +
				"                      AND c.ic_epo               = d.ic_epo               "   +
				"        )                                                                 "   +
				"  GROUP BY                   "   +
				"     ds.ic_if,               "   +
				"     cif.cg_razon_social,    "   +
				"     epo.cg_razon_social,    "   +
				"     p.cg_razon_social,      "   +
				"     n.ic_nafin_electronico, "   +
				"     rpe.cs_aceptacion,      "   +
				"     d.ic_moneda,            "   +
				"     ciffondeo.cg_razon_social "
			);

			qrySentenciaConCuentaUSD.append(
				" SELECT "   +
				"        /*+ INDEX (CIF CP_COMCAT_IF_PK) USE_NL(CIF)  index(c IN_COM_CUENTAS_01_NUK) use_nl(n) */ "   +
				"        ds.ic_if                    AS nomif,         "   +
				"        cif.cg_razon_social,                          "   +
				"        epo.cg_razon_social,                          "   +
				"        p.cg_razon_social           AS nompyme,       "   +
				"        n.ic_nafin_electronico,                       "   +
				"        rpe.cs_aceptacion,                            "   +
				"        COUNT (ds.ic_documento)     AS num_doctos,    "   +
				"        SUM (ds.in_importe_recibir) AS total_importe, "   +
				"        c.ic_bancos_tef,                              "   +
				"        c.cg_cuenta,                                  "   +
				"        cec.ic_estatus_cecoban,                       "   +
				"        cec.cd_descripcion,                           "   +
				"			d.ic_moneda                 AS ic_moneda,     "   +
				"        ciffondeo.cg_razon_social   AS nomIfFondeo    "   +
				"   FROM                                               "   +
				"        com_documento          d,      "   +
				"        com_docto_seleccionado ds,     "   +
				"        com_solicitud          s,      "   +
				"        comrel_nafin           n,      "   +
				"        comrel_pyme_epo        rpe,    "   +
				"        comcat_epo             epo,    "   +
				"        comcat_pyme            p,      "   +
				"        comcat_if              cif,    "   +
				"        comcat_if              ciffondeo, "   +
				"        com_cuentas            c,      "   +
				"        comcat_estatus_cecoban cec     "   +
				"  WHERE "   +
				"        ds.ic_documento        = d.ic_documento             "   +
				"    AND ds.ic_documento        = s.ic_documento             "   +
				"    AND d.ic_pyme              = p.ic_pyme                  "   +
				"    AND d.ic_epo               = ds.ic_epo                  "   +
				"    AND d.ic_epo               = epo.ic_epo                 "   +
				"    AND ds.ic_if               = cif.ic_if                  "   +
				"    AND d.ic_if					  = ciffondeo.ic_if            "   +
				"    AND rpe.ic_pyme            = d.ic_pyme                  "   +
				"    AND rpe.ic_epo             = d.ic_epo                   "   +
				"    AND c.ic_estatus_cecoban   = cec.ic_estatus_cecoban (+) "   +
				"    AND c.ic_nafin_electronico = n.ic_nafin_electronico     "   +
				"    AND n.ic_epo_pyme_if       = p.ic_pyme                  "   +
				"    AND n.cg_tipo              = 'P'                        "   +
				"    AND d.ic_estatus_docto     = 4                          "   +
				"    AND c.ic_producto_nafin    = 1                          "   +
				"    AND c.ic_tipo_cuenta       = " + claveCuentaSwift  +
				"    AND c.ic_epo               = d.ic_epo                   "   +
				"    AND d.ic_moneda            = c.ic_moneda                "   +
				"    AND c.ic_moneda            = 54                         "   +
				condicion.toString()  +
				"  GROUP BY                        "   +
				"        ds.ic_if,                 "   +
				"        cif.cg_razon_social,      "   +
				"        epo.cg_razon_social,      "   +
				"        p.cg_razon_social,        "   +
				"        n.ic_nafin_electronico,   "   +
				"        rpe.cs_aceptacion,        "   +
				"        c.ic_bancos_tef,          "   +
				"        c.cg_cuenta,              "   +
				"        cec.cd_descripcion,       "   +
				"        cec.ic_estatus_cecoban,   "   +
				"        d.ic_moneda,              "   +
				"        ciffondeo.cg_razon_social "
			);

			qrySentenciaSinCuentaUSD.append(
				" SELECT                                                "   +
				"        /*+ use_nl(n) */                               "   +
				"        ds.ic_if                    AS nomif,          "   +
				"        cif.cg_razon_social,                           "   +
				"        epo.cg_razon_social,                           "   +
				"        p.cg_razon_social           AS nompyme,        "   +
				"        n.ic_nafin_electronico,                        "   +
				"        rpe.cs_aceptacion,                             "   +
				"        COUNT (ds.ic_documento)     AS num_doctos,     "   +
				"        SUM (ds.in_importe_recibir) AS total_importe,  "   +
				"        0,                                             "   +
				"        '0',                                           "   +
				"        0,                                             "   +
				"        '0',                                           "   +
				"			d.ic_moneda                 AS ic_moneda,      "   +
				"        ciffondeo.cg_razon_social   AS nomIfFondeo     "   +
				"   FROM                                                "   +
				"        com_documento          d,   "   +
				"        com_docto_seleccionado ds,  "   +
				"        com_solicitud          s,   "   +
				"        comrel_nafin           n,   "   +
				"        comrel_pyme_epo        rpe, "   +
				"        comcat_pyme            p,   "   +
				"        comcat_epo             epo, "   +
				"        comcat_if              cif, "   +
				"        comcat_if              ciffondeo "   +
				"  WHERE                                      "   +
				"        d.ic_documento     = ds.ic_documento "   +
				"    AND d.ic_pyme          = p.ic_pyme       "   +
				"    AND d.ic_epo           = ds.ic_epo       "   +
				"    AND ds.ic_if           = cif.ic_if       "   +
				"    AND d.ic_if				 = ciffondeo.ic_if "   +
				"    AND rpe.ic_pyme        = d.ic_pyme       "   +
				"    AND rpe.ic_epo         = d.ic_epo        "   +
				"    AND d.ic_epo           = epo.ic_epo      "   +
				"    AND ds.ic_documento    = s.ic_documento  "   +
				"    AND n.ic_epo_pyme_if   = p.ic_pyme       "   +
				"    AND n.cg_tipo          = 'P'             "   +
				"    AND d.ic_moneda        = 54              "   +
				"    AND d.ic_estatus_docto = 4               "   +
				condicion.toString()  +
				"    AND NOT EXISTS(                        "   +
				"                    SELECT                 "   +
				"                       1                   "   +
				"                    FROM                   "   +
				"                       com_cuentas c       "   +
				"                    WHERE                  "   +
				"                          c.ic_moneda            = 54                     "   +
				"                      AND n.ic_nafin_electronico = c.ic_nafin_electronico "   +
				"                      AND c.ic_producto_nafin    = 1                      "   +
				"                      AND c.ic_tipo_cuenta       = " + claveCuentaSwift       +
				"                      AND c.ic_epo               = d.ic_epo)              "   +
				"  GROUP BY                        "   +
				"        ds.ic_if,                 "   +
				"        cif.cg_razon_social,      "   +
				"        epo.cg_razon_social,      "   +
				"        p.cg_razon_social,        "   +
				"        n.ic_nafin_electronico,   "   +
				"        rpe.cs_aceptacion,        "   +
				"        d.ic_moneda,              "   +
				"        ciffondeo.cg_razon_social "
			);

			// Mostrar Cuenta clabe(M.N.) si el checkbox "PYME Sin Cuenta Clabe" esta "unchecado"
			if("".equals(pymeSinCuentaClabe)) {
				qrySentencia.append(qrySentenciaConCuenta);
			}

			// MOSTRAR SIEMPRE REGISTROS EN USD CON CUENTA SWIFT

			if(!"".equals(qrySentencia.toString())){
				qrySentencia.append(" UNION ALL ");
	 		}
			qrySentencia.append(qrySentenciaConCuentaUSD);


			if(!"".equals(qrySentencia.toString())){
				qrySentencia.append(" UNION ALL ");
			}
			qrySentencia.append(
				qrySentenciaSinCuenta.toString()+
				" UNION ALL "+
				qrySentenciaSinCuentaUSD.toString()+
				"  ORDER BY nomIfFondeo, nompyme, ic_moneda "
			);

			log.debug("getQueryDetalleMonitoreoDispersionPorIfFideicomiso.qrySentencia       = <" + qrySentencia.toString() + ">");
			log.debug("getQueryDetalleMonitoreoDispersionPorIfFideicomiso.claveIfFideicomiso = <" + claveIfFideicomiso      + ">");
			log.debug("getQueryDetalleMonitoreoDispersionPorIfFideicomiso.claveEPO           = <" + claveEPO                + ">");
			log.debug("getQueryDetalleMonitoreoDispersionPorIfFideicomiso.claveIF            = <" + claveIF                 + ">");
			log.debug("getQueryDetalleMonitoreoDispersionPorIfFideicomiso.pymeSinCuentaClabe = <" + pymeSinCuentaClabe      + ">");
			log.debug("getQueryDetalleMonitoreoDispersionPorIfFideicomiso.radioHabilitadas   = <" + radioHabilitadas        + ">");

			log.info("getQueryDetalleMonitoreoDispersionPorIfFideicomiso(S)");

			return qrySentencia.toString();

		}

	/**
	 *
	 * Devuelve el detalle de las dispersiones enlistadas con una fecha de vencimiento espec�fico.
	 * @throws AppException
	 *
	 * @param el ic_cuenta de la cuenta
	 *
	 * @return <tt>HashMap</tt> con las cuentas bancarias
	 *
	 * @author gperez
	 * @since Migracion Nafin
	 *
	 */
	public HashMap getCuenta(String icCuenta)
		throws AppException {

		log.info("getCuenta(E)");
		HashMap datos=new HashMap();
		AccesoDB 			con 			= new AccesoDB();
		StringBuffer 		query 		= new StringBuffer();
		PreparedStatement ps 			= null;
		ResultSet			rs				= null;
		ResultSet rsEPI = null;
		ResultSet rsEmpresa = null;

		String no_nafin ;
		String nombre="" ;
		String rfc="" ;
		String no_producto ;
		String no_moneda ;
		String no_banco ;
		String cuenta ;
		String tipo_cuenta ;
		String plaza_swift 	;
		String sucursal_swift 	;
		String nom_producto = "";
		String nom_moneda = "";
		String clave = "";
		String sTabla = "";
		String sCampo = "";
		String cgSwift = "";
		String cgAba = "";
		try {

				con.conexionDB();

				query.append(
					" SELECT cc.ic_producto_nafin AS no_producto, cc.ic_nafin_electronico AS no_nafin,"   +
				"        cc.ic_moneda AS no_moneda, cc.ic_bancos_tef AS no_banco,"   +
				"        cc.cg_cuenta AS cuenta, cc.ic_tipo_cuenta AS tipo_cuenta,"   +
				"        cpn.ic_nombre nomprod, mon.cd_nombre moneda,"   +
				"			cc.cg_plaza_swift    as plaza_swift,     "  +
				"			cc.cg_sucursal_swift as sucursal_swift,   "  +
        "			cc.ic_swift as cgswift,   "  +
        "			cc.ic_aba as cgaba   "  +
				"   FROM com_cuentas cc, comcat_producto_nafin cpn, comcat_moneda mon"   +
				"  WHERE cc.ic_producto_nafin = cpn.ic_producto_nafin"   +
				"    AND cc.ic_moneda = mon.ic_moneda"   +
				"    AND cc.ic_cuenta = ? "
				);

				ps = con.queryPrecompilado(query.toString());
				ps.setLong(1,Long.parseLong(icCuenta));

				rs = ps.executeQuery();

				if (rs.next()) {
				no_producto 	= (rs.getString("no_producto") == null) ? "" : rs.getString("no_producto");
				no_nafin 		= (rs.getString("no_nafin") == null) ? "" : rs.getString("no_nafin");
				no_moneda 		= (rs.getString("no_moneda") == null) ? "" : rs.getString("no_moneda");
				no_banco 		= (rs.getString("no_banco") == null) ? "" : rs.getString("no_banco");
				cuenta 			= (rs.getString("cuenta") == null) ? "" : rs.getString("cuenta");
				tipo_cuenta 	= (rs.getString("tipo_cuenta") == null) ? "" : rs.getString("tipo_cuenta");
				nom_producto	= (rs.getString("nomprod") == null) ? "" : rs.getString("nomprod");
				nom_moneda		= (rs.getString("moneda") == null) ? "" : rs.getString("moneda");
				plaza_swift		= (rs.getString("plaza_swift") 		== null)?"": rs.getString("plaza_swift");
				sucursal_swift	= (rs.getString("sucursal_swift")	== null)?"": rs.getString("sucursal_swift");
        cgSwift =  (rs.getString("cgswift")	== null)?"": rs.getString("cgswift");
        cgAba = (rs.getString("cgaba")	== null)?"": rs.getString("cgaba");

				 boolean esCuentaSwift  = ("54".equals(no_moneda) && "50".equals(tipo_cuenta))?true:false;

				/*Obtiene Id de EPO, PYME o IF*/
				query.delete(0,query.length());
				query.append(
					" SELECT n.ic_nafin_electronico, n.ic_epo_pyme_if, n.cg_tipo"   +
					"   FROM comrel_nafin n"   +
					"  WHERE n.ic_nafin_electronico = "+no_nafin);
				rsEPI = con.queryDB(query.toString());
				if (rsEPI.next()){
					if ("E".equals(rsEPI.getString("CG_TIPO"))){
						sTabla = " comcat_epo ";
						sCampo = " ic_epo ";
					} else if ("P".equals(rsEPI.getString("CG_TIPO"))){
						sTabla = " comcat_pyme ";
						sCampo = " ic_pyme ";
					} else if ("I".equals(rsEPI.getString("CG_TIPO"))){
						sTabla = " comcat_if ";
						sCampo = " ic_if ";
					}
					query.delete(0,query.length());
					query.append(" SELECT ").append(sCampo).append(" as id , cg_razon_Social as nombre, cg_rfc as rfc FROM ").append(sTabla);
					query.append(" WHERE ").append(sCampo).append(" = ").append(rsEPI.getString("ic_epo_pyme_if"));
					rsEmpresa = con.queryDB(query.toString());
					if (rsEmpresa.next()) {
						clave 	= (rsEmpresa.getString(1) == null) ? "" : rsEmpresa.getString(1);
						nombre 	= (rsEmpresa.getString("nombre") == null) ? "" : rsEmpresa.getString("nombre");
						rfc 	= (rsEmpresa.getString("rfc") == null) ? "" : rsEmpresa.getString("rfc");
					}
				}
				datos.put("Producto",no_producto);
				datos.put("numElectronico",no_nafin);
				datos.put("nombre",nombre);
				datos.put("R_F_C",rfc);
				datos.put("cbMoneda",no_moneda);
				datos.put("Hbanco",no_banco);
				datos.put("cuenta",cuenta);
        datos.put("cuentaDL",cuenta);
				datos.put("HicEpo","");
				datos.put("nom_producto",nom_producto);
				datos.put("nom_moneda",nom_moneda);
				datos.put("plaza_swift",plaza_swift);
				datos.put("sucursal_swift",sucursal_swift);
				datos.put("esCuentaSwift",new Boolean(esCuentaSwift));
				datos.put("clave",clave);
				datos.put("tipo_cuenta",tipo_cuenta);
        datos.put("cgswift",cgSwift);
        datos.put("cgaba",cgAba);
			}
			rs.close();
			rsEPI.close();
			rsEmpresa.close();
			con.cierraStatement();



		} catch(Exception e){


			e.printStackTrace();

			throw new AppException("Ocurri� un error al obtener la cuenta");

		} finally {

			if(rs != null ) try{ rs.close(); }catch(Exception e){}
			if(ps != null ) try{ ps.close(); }catch(Exception e){}
			if(rsEPI != null ) try{ rsEPI.close(); }catch(Exception e){}
			if(rsEmpresa != null ) try{ rsEmpresa.close(); }catch(Exception e){}
			if( con.hayConexionAbierta() ){
				con.cierraConexionDB();
			}
			log.info("getCuenta(S)");

		}

		return datos;

	}
	/**
	 * Metodo que almacena en bitacora las descargas de los archivos con detalles de
	 * las CUENTAS CLABE modificadas o registradas el dia en curso
	 *
	 * @throws netropology.utilerias.AppException
	 * @return <tt>true</tt> si todo el metodo se ejecuta con exito <tt>false</tt> en caso contrario
	 * @param tipoArchivo
	 * @param nombreArchivo
	 * @param ruta
	 * @param strNombreUsuario
	 * @param iNoUsuario
	 */
	public boolean generarArchivosCuenta(String iNoUsuario, String strNombreUsuario,String ruta, String nombreArchivo, String tipoArchivo) throws AppException{
		log.info("generarArchivosCuenta(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		StringBuffer qrySentencia 	= new StringBuffer();
		boolean exito = true;
		try{
			con.conexionDB();
			//Consulta para el contenido del archivo
			qrySentencia.append(	" SELECT	\n"+
										"	c.ic_cuenta,	\n"+
										"	pym.cg_rfc AS rfc,	\n"+
										"	pym.cg_razon_social AS nombre,	\n"+
										"	bt.cd_descripcion banco,	\n"+
										"	bt.ic_bancos_tef ic_banco,	\n"+
										"	DECODE (c.ic_tipo_cuenta, 40, c.cg_cuenta) AS cuentaclabe,	\n"+
										"	pym.CS_TIPO_PERSONA as tipoPersona	\n"+
										" FROM	"+
										"  com_cuentas c,  \n"+
										"  comrel_nafin crn,  \n"+
										"  comcat_pyme pym,  \n"+
										"  comcat_epo epo,  \n"+
										"  comcat_bancos_tef bt,  \n"+
										"  comcat_estatus_cecoban cec,  \n"+
										"  comcat_estatus_tef tef,  \n"+
										"  comcat_producto_nafin cpn,  \n"+
										"  comcat_moneda cm  \n"+
										" WHERE	\n"+
										"  c.ic_nafin_electronico = crn.ic_nafin_electronico \n"+
										"  AND c.ic_bancos_tef = bt.ic_bancos_tef \n"+
										"  AND c.ic_estatus_tef = tef.ic_estatus_tef(+) \n"+
										"  AND c.ic_moneda = cm.ic_moneda \n"+
										"  AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban(+) \n"+
										"  AND c.ic_producto_nafin = cpn.ic_producto_nafin \n"+
										"  AND c.ic_epo = epo.ic_epo \n"+
										"  AND crn.ic_epo_pyme_if = pym.ic_pyme \n"+
										"  AND c.ic_tipo_cuenta != 1 \n"+
										"  AND crn.cg_tipo = 'P' \n"+
										"  AND c.ic_producto_nafin = 1 \n"+
										"  AND cm.ic_moneda = 1 \n"+
										"  AND c.cg_tipo_afiliado = 'P' \n"+
										"  AND (   TRUNC(c.df_transferencia) = TRUNC(SYSDATE+1) OR TRUNC (c.df_registro) =  TRUNC (SYSDATE)  OR TRUNC (c.df_transferencia) = TRUNC (SYSDATE + 3) ) \n"+
										" ORDER BY nombre	");
				ps = con.queryPrecompilado(qrySentencia.toString(),new ArrayList());
				rs = ps.executeQuery();
				//Se arma el contenido del archivo
				System.out.println("*********  qrySentencia :: "+qrySentencia.toString());
				int nReg = 0;
				while(rs.next()){
					String rfc = rs.getString("RFC")==null?"":rs.getString("RFC");
					String nombre = rs.getString("NOMBRE")==null?"":rs.getString("NOMBRE");
					String banco = rs.getString("IC_BANCO")==null?"":rs.getString("IC_BANCO");
					String cuentaClabe = rs.getString("CUENTACLABE")==null?"":rs.getString("CUENTACLABE");
					String tipoPersona = rs.getString("TIPOPERSONA")==null?"":rs.getString("TIPOPERSONA");
					StringBuffer aux = new StringBuffer("");
					//Validacion para el RFC segun tipo de persona Moral (M) Fisica (F)
					if("M".equals(tipoPersona)){
						rfc = rfc.replaceAll("-","");
						aux.append(rfc);
						aux.insert(3," ");
						rfc = aux.toString();
					}
					if("F".equals(tipoPersona)){
						rfc = rfc.replaceAll("-","");
					}
					if(banco.length()<3){
						StringBuffer auxBanco = new StringBuffer("");
						auxBanco.append(banco);
						for(int i = 0;i<3-banco.length();i++){
							auxBanco.insert(i,"0");
						}
						banco = auxBanco.toString();
					}
					nReg++;//Se increment ael contador
					contenidoArchivo.append(rfc.replaceAll(",",""));contenidoArchivo.append(";");
					contenidoArchivo.append(nombre.replaceAll(",",""));contenidoArchivo.append(";");
					contenidoArchivo.append(banco.replaceAll(",",""));contenidoArchivo.append(";");
					contenidoArchivo.append(cuentaClabe.replaceAll(",",""));contenidoArchivo.append("\n");
				}
				rs.close();
				ps.close();
				System.out.println("*********  nReg :: "+nReg);
				if(nReg>0){
//------------------------------------------------------------------------------
//	Segmento de codigo pensado para cuando los archivos se generan por proceso,
//
				tipoArchivo = (tipoArchivo==null)?"":tipoArchivo;
				qrySentencia.setLength(0);//limpiamos para reutilizar
				if("".equals(tipoArchivo)){//Si no hay tipo especificado, se crean ambos archivo (Sistema)
					//Consultamos si ya se descargo el archivo txt para el dia actual
					qrySentencia.append(" SELECT TO_CHAR (b.df_descarga, 'dd/mm/yyyy') AS fecha_carga, decode(dbms_lob.GETLENGTH(b.bi_archivo_txt),0,0,dbms_lob.GETLENGTH(b.bi_archivo_txt),1) as txt, ");
					qrySentencia.append(" decode(dbms_lob.GETLENGTH(b.bi_archivo_xls),0,0,dbms_lob.GETLENGTH(b.bi_archivo_xls),1  ) as xls FROM bit_descarga_cuentas_clabe b WHERE TRUNC (b.df_descarga) = TRUNC (SYSDATE)");
					ps = con.queryPrecompilado(qrySentencia.toString());
					rs = ps.executeQuery();
					boolean txt_ = true;
					boolean xls_ = true;
					while(rs.next()){
						String archivioTXT = (rs.getString("TXT")==null)?"":rs.getString("TXT");
						String archivioXLS = (rs.getString("XLS")==null)?"":rs.getString("XLS");
						txt_ = (archivioTXT.equals("0"))?true:false;
						xls_ = (archivioXLS.equals("0"))?true:false;
					}
					rs.close();
					ps.close();
					if(txt_ && xls_){//Se cran ambos archivos
						CreaArchivo archivoTXT = new CreaArchivo();
						CreaArchivo archivoXLS = new CreaArchivo();
						if(archivoTXT.make(contenidoArchivo.toString(), ruta,nombreArchivo, ".txt") && archivoXLS.make(contenidoArchivo.toString().replaceAll(";",","), ruta,nombreArchivo, ".csv")){//Si se crea el txt
							//Guardamos en Bitacora
							qrySentencia.setLength(0);//limpiamos para reutilizar
							qrySentencia.append(" INSERT INTO bit_descarga_cuentas_clabe ( IC_BIT_DESCARGA ,  CG_NOMBRE_USUARIO  , BI_ARCHIVO_TXT  , BI_ARCHIVO_XLS) ");
							qrySentencia.append("VALUES(SEQ_BIT_DESCARGA_CUENTAS.NEXTVAL,  ?,  ?,  ?)");
							ps = con.queryPrecompilado(qrySentencia.toString());
							ps.setString(1,"Sistema");
							//SE GUARDA ARCHIVO TXT
							File txt = new File(ruta+archivoTXT.getNombre());
							FileInputStream fileinputstreamTXT = new FileInputStream(txt);
							ps.setBinaryStream(2,fileinputstreamTXT,(int)txt.length());
							//SE GUARDA ARCHIVO XLS
							File xls = new File(ruta+archivoXLS.getNombre());
							FileInputStream fileinputstreamXLS = new FileInputStream(xls);
							ps.setBinaryStream(3,fileinputstreamXLS,(int)xls.length());
							//Ejecutamos el insert
							ps.executeUpdate();
							ps.close();
							fileinputstreamTXT.close();
							fileinputstreamXLS.close();
						}else{
							exito = false;
							throw new AppException("Error al general los archivos");
						}
					}
/**
					else if(txt_){//Ya exixte el xls, solo se crea el txt
						CreaArchivo archivoTXT = new CreaArchivo();
						if(archivoTXT.make(contenidoArchivo.toString(), ruta, nombreArchivo,".txt")){
							//Guardamos en Bitacora
							qrySentencia.setLength(0);//limpiamos para reutilizar
							qrySentencia.append(" INSERT INTO bit_descarga_cuentas_clabe ( IC_BIT_DESCARGA ,  CG_NOMBRE_USUARIO  , BI_ARCHIVO_TXT  , BI_ARCHIVO_XLS) ");
							qrySentencia.append("VALUES(SEQ_BIT_DESCARGA_CUENTAS.NEXTVAL,  ?,  ?,  empty_blob())");
							ps = con.queryPrecompilado(qrySentencia.toString());
							ps.setString(1,"Sistema");
							//SE GUARDA ARCHIVO TXT
							File txt = new File(ruta+archivoTXT.getNombre());
							FileInputStream fileinputstreamTXT = new FileInputStream(txt);
							ps.setBinaryStream(2,fileinputstreamTXT,(int)txt.length());
							//Ejecutamos el insert
							ps.executeUpdate();
							ps.close();
							fileinputstreamTXT.close();
						}else{
							exito = false;
							throw new AppException("Error al general el archivo TXT");
						}
					}else if(xls_){
						CreaArchivo archivoXLS = new CreaArchivo();
						contenidoArchivo.insert(0,"RFC,TITULAR,BANCO,CLABE\n");
						if(archivoXLS.make(contenidoArchivo.toString().replaceAll(";",","), ruta,nombreArchivo, ".csv")){
							qrySentencia.append(" INSERT INTO bit_descarga_cuentas_clabe ( IC_BIT_DESCARGA ,  CG_NOMBRE_USUARIO  , BI_ARCHIVO_TXT  , BI_ARCHIVO_XLS) ");
							qrySentencia.append("VALUES(SEQ_BIT_DESCARGA_CUENTAS.NEXTVAL,  ?,  empty_blob(),  ?)");
							ps = con.queryPrecompilado(qrySentencia.toString());
							ps.setString(1,"Sistema");
							//SE GUARDA ARCHIVO XLS
							File xls = new File(ruta+archivoXLS.getNombre());
							FileInputStream fileinputstreamXLS = new FileInputStream(xls);
							ps.setBinaryStream(2,fileinputstreamXLS,(int)xls.length());
							//Ejecutamos el insert
							ps.executeUpdate();
							ps.close();
							fileinputstreamXLS.close();
						}else{
							exito = false;
							throw new AppException("Error al general el archivo XLS");
						}

					}
*/
				}//FIN SYSTEMA
//------------------------------------------------------------------------------

				if("TXT".equals(tipoArchivo)){
					CreaArchivo archivoTXT = new CreaArchivo();
					if(archivoTXT.make(contenidoArchivo.toString(), ruta, nombreArchivo,".txt")){
						qrySentencia.setLength(0);//limpiamos para reutilizar
						qrySentencia.append(" INSERT INTO bit_descarga_cuentas_clabe ( IC_BIT_DESCARGA ,  CG_NOMBRE_USUARIO  , BI_ARCHIVO_TXT  , BI_ARCHIVO_XLS) ");
						qrySentencia.append("VALUES(SEQ_BIT_DESCARGA_CUENTAS.NEXTVAL,  ?,  ?,  empty_blob())");
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.setString(1,iNoUsuario+" - "+strNombreUsuario);
						//ps.setString(1,"Sistema");
						//SE GUARDA ARCHIVO TXT
						File txt = new File(ruta+archivoTXT.getNombre());
						FileInputStream fileinputstreamTXT = new FileInputStream(txt);
						ps.setBinaryStream(2,fileinputstreamTXT,(int)txt.length());
						//Ejecutamos el insert
						ps.executeUpdate();
						ps.close();
						fileinputstreamTXT.close();
					}else{
						exito = false;
						throw new AppException("Error al general el archivo TXT");
					}
				}
				if("XLS".equals(tipoArchivo)){
					//Creamos el XLS
					CreaArchivo archivoXLS = new CreaArchivo();
						archivoXLS.setNombre(nombreArchivo+".xls");
						String nombreArchivoExcel	=	archivoXLS.getNombre();
						ComunesXLS xlsDoc	= new ComunesXLS(ruta+nombreArchivoExcel,"CUENTAS CLABE");
						//xlsDoc.creaFila();
						xlsDoc.setTabla(5);
						xlsDoc.setCelda("", "celda01", ComunesXLS.CENTER, 1);
						xlsDoc.setCelda("RFC", "celda01", ComunesXLS.CENTER, 1);
						xlsDoc.setCelda("TITULAR", "celda01", ComunesXLS.CENTER, 1);
						xlsDoc.setCelda("BANCO", "celda01", ComunesXLS.CENTER, 1);
						xlsDoc.setCelda("CLABE", "celda01", ComunesXLS.CENTER, 1);
						String []contenido = contenidoArchivo.toString().split("\n");
						for(int i = 0; i < contenido.length; ++ i){
							String []contenidoFinal = new String[20];
							contenidoFinal = contenido[i].split(";");
							xlsDoc.setCelda((i+1+""), "formas", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda(contenidoFinal[0], "formas", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda(contenidoFinal[1], "formas", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda(contenidoFinal[2], "formas", ComunesXLS.CENTER, 1);
							xlsDoc.setCelda(contenidoFinal[3], "formas", ComunesXLS.CENTER, 1);
						}
						xlsDoc.cierraTabla();
						xlsDoc.cierraXLS();
						//Se almacema en bitacora
						qrySentencia.setLength(0);//limpiamos para reutilizar
						qrySentencia.append(" INSERT INTO bit_descarga_cuentas_clabe ( IC_BIT_DESCARGA ,  CG_NOMBRE_USUARIO  , BI_ARCHIVO_TXT  , BI_ARCHIVO_XLS) ");
						qrySentencia.append("VALUES(SEQ_BIT_DESCARGA_CUENTAS.NEXTVAL,  ?,  empty_blob(),  ?)");
						ps = con.queryPrecompilado(qrySentencia.toString());
						ps.setString(1,iNoUsuario+" - "+strNombreUsuario);
						//ps.setString(1,"Sistema");
						//SE GUARDA ARCHIVO XLS
						File xls = new File(ruta+archivoXLS.getNombre());
						FileInputStream fileinputstreamXLS = new FileInputStream(xls);
						ps.setBinaryStream(2,fileinputstreamXLS,(int)xls.length());
						//Ejecutamos el insert
						ps.executeUpdate();
						ps.close();
						fileinputstreamXLS.close();
				}

			}else{
				exito = false;
			   if("TXT".equals(tipoArchivo)){
					CreaArchivo archivoTXT = new CreaArchivo();
			      if(archivoTXT.make(contenidoArchivo.toString(), ruta, nombreArchivo,".txt")){
			          File txt = new File(ruta+archivoTXT.getNombre());
			          FileInputStream fileinputstreamTXT = new FileInputStream(txt);
			          fileinputstreamTXT.close();
			        }else{
			           exito = false;
			            throw new AppException("Error al general el archivo TXT");
			         }
			      }
			      if("XLS".equals(tipoArchivo)){
			        //Creamos el XLS
			         CreaArchivo archivoXLS = new CreaArchivo();
			         archivoXLS.setNombre(nombreArchivo+".xls");
			         String nombreArchivoExcel  =  archivoXLS.getNombre();
			         ComunesXLS xlsDoc = new ComunesXLS(ruta+nombreArchivoExcel,"CUENTAS CLABE");
			         //xlsDoc.creaFila();
						xlsDoc.cierraTabla();
			         xlsDoc.cierraXLS();
			         File xls = new File(ruta+archivoXLS.getNombre());
			         FileInputStream fileinputstreamXLS = new FileInputStream(xls);
			         fileinputstreamXLS.close();
					}
			}
		}catch(Exception e){
			exito = false;
			e.printStackTrace();
			throw new AppException("generarArchivosCuenta(Error...) "+ e.getMessage());
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		return exito;
	}

  public List getCtasPymeCLABESwift(String tipoAfiliado, String cveProducto, String nafinElectronico, String cvePyme, String cveMoneda )
  {
    AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
    String strSQL = "";
    try
    {
      con.conexionDB();

      strSQL = "SELECT  c.ic_cuenta, " +
            "         DECODE (c.ic_tipo_cuenta, 40, c.cg_cuenta, 'N/A') AS clabe, " +
            "         DECODE (c.ic_tipo_cuenta, 1, c.cg_cuenta, 'N/A') AS speua, " +
            "         c.ic_moneda, m.cd_nombre AS moneda, c.ic_bancos_tef, " +
            "         bt.cd_descripcion AS banco, c.cg_tipo_afiliado, " +
            "         epo.cg_razon_social AS nombre_epo, c.ic_tipo_cuenta, " +
            "         DECODE (c.ic_tipo_cuenta, 50, c.cg_cuenta, 'N/A') AS cuentaswift, " +
            "         DECODE (c.ic_tipo_cuenta, " +
            "                 50, c.cg_plaza_swift, " +
            "                 'N/A' " +
            "                ) AS plaza_swift, " +
            "         DECODE (c.ic_tipo_cuenta, " +
            "                 50, c.cg_sucursal_swift, " +
            "                 'N/A' " +
            "                ) AS sucursal_swift, " +
            "         DECODE (c.cg_tipo_afiliado, " +
            "                 'E', 'EPO', " +
            "                 'P', 'Proveedor', " +
            "                 'D', 'Distribuidor', " +
            "                 'I', 'IF Bancario/No Bancario', " +
            "                 'B', 'IF/Beneficiario', " +
            "                 '' " +
            "                ) AS tipo_afiliado, epo.ic_epo cveepo, pe.ic_pyme cvepyme, c.ic_aba aba, c.ic_swift swift " +
            "    FROM comcat_epo epo, comrel_pyme_epo pe, comrel_producto_epo cpe,  " +
            "         com_cuentas c, comcat_bancos_tef bt, comcat_moneda m " +
            "   WHERE pe.ic_epo = epo.ic_epo " +
            "    AND pe.ic_epo = cpe.ic_epo " +
            "    AND epo.cs_habilitado = 'S' " +
            "    AND pe.cs_habilitado = 'S' " +
            "    AND cpe.cg_dispersion = 'S' " +
            "    AND pe.ic_pyme = ? " +
            "    AND epo.ic_epo = c.ic_epo(+) " +
            "    AND c.ic_moneda = m.ic_moneda(+) " +
            "    AND c.ic_bancos_tef = bt.ic_bancos_tef(+) " +
            "    AND c.ic_nafin_electronico(+) = ? " +
            "    AND c.ic_producto_nafin(+) = ? " +//1
            "    AND c.ic_tipo_cuenta(+) != ? " +//1
            "    AND c.ic_moneda(+) = ? ";//1

      ps = con.queryPrecompilado(strSQL);
      ps.setLong(1, Long.parseLong(cvePyme));
      ps.setLong(2, Long.parseLong(nafinElectronico));
      ps.setLong(3, Long.parseLong(cveProducto));
      ps.setLong(4, Long.parseLong("1"));
      ps.setLong(5, Long.parseLong(cveMoneda));

      rs = ps.executeQuery();

      List lstCtas = new ArrayList();
      HashMap hmCtas = new HashMap();
      while (rs.next())
      {
        hmCtas = new HashMap();
        hmCtas.put("CVECUENTA", rs.getString("ic_cuenta")==null?"":rs.getString("ic_cuenta"));
        hmCtas.put("CLABE", rs.getString("clabe")==null?"":rs.getString("clabe"));
        hmCtas.put("SPEUA", rs.getString("speua")==null?"":rs.getString("speua"));
        hmCtas.put("CVEMONEDA", rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda"));
        hmCtas.put("MONEDA", rs.getString("moneda")==null?"":rs.getString("moneda"));
        hmCtas.put("CVEBANCOTEF", rs.getString("ic_bancos_tef")==null?"":rs.getString("ic_bancos_tef"));
        hmCtas.put("BANCOTEF", rs.getString("banco")==null?"":rs.getString("banco"));
        hmCtas.put("CVETIPOAFILIADO", rs.getString("cg_tipo_afiliado")==null?"":rs.getString("cg_tipo_afiliado"));
        hmCtas.put("EPO", rs.getString("nombre_epo")==null?"":rs.getString("nombre_epo"));
        hmCtas.put("TIPOCUENTA", rs.getString("ic_tipo_cuenta")==null?"":rs.getString("ic_tipo_cuenta"));
        hmCtas.put("CUENTASWIFT", rs.getString("cuentaswift")==null?"":rs.getString("cuentaswift"));
        hmCtas.put("PLAZASWIFT", rs.getString("plaza_swift")==null?"":rs.getString("plaza_swift"));
        hmCtas.put("SUCSWIFT", rs.getString("sucursal_swift")==null?"":rs.getString("sucursal_swift"));
        hmCtas.put("TIPOAFILIADO", rs.getString("tipo_afiliado")==null?"":rs.getString("tipo_afiliado"));
        hmCtas.put("CVEEPO", rs.getString("cveepo")==null?"":rs.getString("cveepo"));
        hmCtas.put("CVEPYME", rs.getString("cvepyme")==null?"":rs.getString("cvepyme"));
        hmCtas.put("SWIFT", rs.getString("swift")==null?"":rs.getString("swift"));
        hmCtas.put("ABA", rs.getString("aba")==null?"":rs.getString("aba"));

        lstCtas.add(hmCtas);
      }
      rs.close();
      ps.close();

      return lstCtas;
    }catch(Throwable t )
    {
      throw new AppException("Error al consultar ctas de proveedores para su registro de cuentas CLABE y SWIFT", t);
    }finally
    {
      if(con.hayConexionAbierta())
      {
        con.cierraConexionDB();
      }
    }
  }


  //---------------------------------
  public String ovinsertarCuentaProveedor(  	String ic_nafin_electronico,	String ic_producto_nafin,
										String ic_moneda, 				String ic_bancos_tef,
										String cg_cuenta,				String ic_usuario,
										String ic_tipo_cuenta,			List lstCtasEpo,
										String cg_tipo_afiliado,		String rfc, HashMap parametrosAdicionales)
   	throws NafinException {
		System.out.println("\nDispersionEJB::ovinsertarCuenta (E)");
		AccesoDB		con 			= null;
		ResultSet		rs				= null;
		String			qrySentencia	= "";
		String 			sSigLlave		= "";//sLlaveSiguiente();
		String 			sResBusqueda	= "0";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		PreparedStatement ps = null;
		boolean resultado = true;

		parametrosAdicionales 	= parametrosAdicionales == null?new HashMap():parametrosAdicionales;

		try {
			con = new AccesoDB();
			con.conexionDB();

			//VALIDA LA PERTENENCIA DEL AFILIADO AL PRODUCTO DADO
			if ("E".equals(cg_tipo_afiliado)) {
				qrySentencia =
					" SELECT COUNT (1)"   +
					"   FROM comrel_producto_epo cpe, comrel_nafin rna"   +
					"  WHERE cpe.ic_epo = rna.ic_epo_pyme_if "+
					"	AND rna.cg_tipo = 'E' " +
					"	AND rna.ic_nafin_electronico = ? "+
					"    AND cpe.ic_producto_nafin = ? ";
			} else {
				if("P".equals(cg_tipo_afiliado) || "D".equals(cg_tipo_afiliado)) {
					if("P".equals(cg_tipo_afiliado) && "1".equals(ic_producto_nafin)) {
						qrySentencia =
							" SELECT COUNT (1)"   +
							"   FROM comrel_pyme_epo pep, comrel_nafin rna "   +
							"    WHERE pep.ic_pyme = rna.ic_epo_pyme_if "+
							"	 AND rna.cg_tipo= 'P' "+
							"	 AND rna.ic_nafin_electronico = ? ";
					} else {
						qrySentencia =
							" SELECT COUNT (1)"   +
							"   FROM comrel_pyme_epo_x_producto pep, comrel_nafin rna "   +
							"    WHERE pep.ic_pyme = rna.ic_epo_pyme_if "+
							"	 AND rna.cg_tipo= 'P' "+
							"	AND rna.ic_nafin_electronico = ? "+
							"    AND pep.ic_producto_nafin = ? ";
					}
				} else {
					if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado)) {
						qrySentencia =
							" SELECT COUNT (1)"   +
							"   FROM comrel_producto_if cpi,comrel_nafin rna"   +
							"    WHERE cpi.ic_if = rna.ic_epo_pyme_if "+
							"	 AND rna.cg_tipo= 'I' "+
							"	AND rna.ic_nafin_electronico = ? "+
							"    AND cpi.ic_producto_nafin = ? ";
					}
				}
			}

			System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_nafin_electronico));
			if(!("P".equals(cg_tipo_afiliado) && "1".equals(ic_producto_nafin)))
				ps.setInt(2, Integer.parseInt(ic_producto_nafin));
			rs = ps.executeQuery();
			ps.clearParameters();
			int perteneceProd = 0;
			if(rs.next())
				perteneceProd = rs.getInt(1);
			rs.close();ps.close();

			if(perteneceProd==0)
				throw new NafinException("DISP0020");



			String condicionIFB = "";
			String condicionIF = "";
			String condicionIF2 = "";

			if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado) || "P".equals(cg_tipo_afiliado)) {
				condicionIF 	= " AND ic_epo = ?"  ;
				condicionIF2 	= " AND ic_epo != ?"  ;
				condicionIFB 	= " AND cg_tipo_afiliado = '"+cg_tipo_afiliado+"' ";
			}

			//VALIDA QUE NO TENGA MAS DE UNA CUENTA POR EPO, PROVEEDOR, MONEDA Y PRODUCTO.
			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM com_cuentas"   +
				"  WHERE ic_nafin_electronico = ?"   +
				"    AND ic_producto_nafin    = ?"   +
				"    AND ic_tipo_cuenta       = ?"   +
				condicionIF+condicionIFB  ;
			//System.out.println("\n qrySentencia: "+qrySentencia);

      //INICIO DE CICLO DE GUARDADO
      String vCveCta = "";
      String ic_epo = "";

      HashMap hmCtas = new HashMap();
      for(int x = 0; x<lstCtasEpo.size();x++){
          hmCtas = (HashMap)lstCtasEpo.get(x);
          vCveCta = (String)hmCtas.get("CVECTA");
          ic_epo = (String)hmCtas.get("CVEEPO");

          qrySentencia =
              " SELECT COUNT (1)"   +
              "   FROM com_cuentas"   +
              "  WHERE ic_nafin_electronico = ?"   +
              "    AND ic_producto_nafin    = ?"   +
              "    AND ic_tipo_cuenta       = ?"   +
              condicionIF+condicionIFB  ;

          ps = con.queryPrecompilado(qrySentencia);
          ps.setInt(1, Integer.parseInt(ic_nafin_electronico));
          ps.setInt(2, Integer.parseInt(ic_producto_nafin));
          ps.setInt(3, Integer.parseInt(ic_tipo_cuenta));
          int cont = 3;
          if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado) || "P".equals(cg_tipo_afiliado)) {
            cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
          }
          rs = ps.executeQuery();
          ps.clearParameters();
          int existeCuenta = 0;
          if(rs.next())
            existeCuenta = rs.getInt(1);
          rs.close(); ps.close();

          if(existeCuenta>0)
            System.out.println("Ya existe cuenta pero se omite porque se actualizara...");
            //throw new NafinException("DISP0017");

          //VALIDA QUE LA CUENTA NO ESTE ASIGNADA A OTRO USUARIO
          qrySentencia =
            " SELECT   /*+index(c CP_COM_CUENTAS_PK) use_nl(crn c)*/"   +
            "        crn.cg_tipo, crn.ic_epo_pyme_if"   +
            "   FROM com_cuentas c, comrel_nafin crn"   +
            "  WHERE c.ic_nafin_electronico = crn.ic_nafin_electronico "+
            "    AND c.cg_cuenta = ?"   +
            "    AND c.ic_nafin_electronico != ? "   +condicionIF2  ;
            // NOTA: SE USARA LA CONDICION: condicionIF2 DEBIDO A QUE NO PERMITE DETECTAR CORRECTAMENTE
            //  PROVEEDORES EN UNA MISMA EPO QUE TENGAN LA CUENTA REPETIDA:  AND ic_epo != ?
          //System.out.println("\n qrySentencia: "+qrySentencia);

          ps = con.queryPrecompilado(qrySentencia);
          ps.setString(1, cg_cuenta);
          ps.setInt(2, Integer.parseInt(ic_nafin_electronico));
          cont = 2;
          if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado) || "P".equals(cg_tipo_afiliado)) {
            cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
          }
          rs = ps.executeQuery();
          ps.clearParameters();
          existeCuenta = 0;
          while(rs.next()){
            ResultSet			rs1				= null;
            String				qrySentencia1	= "";
            PreparedStatement	ps1				= null;

            String rs_tipo = rs.getString(1)==null?"":rs.getString(1);
            String rs_epo_pyme_if = rs.getString(2)==null?"":rs.getString(2);

            if("E".equals(rs_tipo)) {
              qrySentencia1 =
                " SELECT COUNT (1)"   +
                "   FROM comcat_epo"   +
                "  WHERE ic_epo = ?"   +
                "    AND cg_rfc != ?"  ;
            } else if("P".equals(rs_tipo)) {
              qrySentencia1 =
                " SELECT COUNT (1)"   +
                "   FROM comcat_pyme"   +
                "  WHERE ic_pyme = ?"   +
                "    AND cg_rfc != ?"  ;
            } else if("I".equals(rs_tipo)) {
              qrySentencia1 =
                " SELECT COUNT (1)"   +
                "   FROM comcat_if"   +
                "  WHERE ic_if = ?"   +
                "    AND cg_rfc != ?"  ;
            }
            //System.out.println("\nqrySentencia1: "+qrySentencia1);
            ps1 = con.queryPrecompilado(qrySentencia1);
            ps1.setInt(1, Integer.parseInt(rs_epo_pyme_if));
            ps1.setString(2, rfc);
            rs1 = ps1.executeQuery();
            ps1.clearParameters();
            if(rs1.next())
              existeCuenta += rs1.getInt(1);
            rs1.close();ps1.close();
          }//while
          rs.close();ps.close();
          if(existeCuenta>0)
            throw new NafinException("DISP0018");

          // VALIDAR SI LA CUENTA A REGISTRAR ES EN DOLARES, REVISAR QUE VENGAN ESPECIFICADAS LA PLAZA SWIFT
          // Y LA SUCURSAL SWIFT
          String plazaSwift 	= null;
          String sucursalSwift = null;
          String vSwift = null;
          String vAba = null;

          if( ic_moneda.equals("54") && ic_tipo_cuenta.equals("50")){ // Cuenta en Dolares y de Tipo Swift

            plazaSwift 					= (String) parametrosAdicionales.get("PLAZA_SWIFT");
            plazaSwift					= plazaSwift    == null || plazaSwift.trim().equals("")   ?null:plazaSwift.trim();

            sucursalSwift				= (String) parametrosAdicionales.get("SUCURSAL_SWIFT");
            sucursalSwift				= sucursalSwift == null || sucursalSwift.trim().equals("")?null:sucursalSwift.trim();

            vSwift				= (String) parametrosAdicionales.get("SWIFT");
            vSwift				= vSwift == null || vSwift.trim().equals("")?null:vSwift.trim();

            vAba				= (String) parametrosAdicionales.get("ABA");
            vAba				= vAba == null || vAba.trim().equals("")?null:vAba.trim();

            if(plazaSwift == null){
              throw new NafinException("DISP0021");
            }else if(sucursalSwift == null){
              throw new NafinException("DISP0022");
            }

          }

          // Debido a que si se registra una cuenta en dolares, esta se almacena con
          // estatus 99, entonces si esta misma cuenta se registra en pesos, pasaria por alto
          // la validacion que se hace para las cuentas en monedas nacional.
          // Por eso se excluye de esta consulta, las cuentas en dolares.
          qrySentencia =
            " SELECT ic_estatus_cecoban"   +
            "   FROM com_cuentas c"   +
            "  WHERE c.cg_cuenta = ? "   +
            "	  AND ic_moneda != ? "  +// 54; Excluir Dolares Americanos
            "    AND ic_nafin_electronico = ?"+condicionIF  ;

          //System.out.println("\n qrySentencia: "+qrySentencia);
          ps = con.queryPrecompilado(qrySentencia);
          ps.setString(1, cg_cuenta);
          ps.setInt(2, Integer.parseInt("54")); // Excluir Dolares Americanos
          ps.setInt(3, Integer.parseInt(ic_nafin_electronico));
          cont = 3;
          if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado)  || "P".equals(cg_tipo_afiliado)) {
            cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
          }
          rs = ps.executeQuery();
          ps.clearParameters();
          String estatusCuenta = "";
          String estatusCampo = "";
          if(rs.next()){
            estatusCuenta = rs.getString(1)==null?"":","+rs.getString(1);
            if(!"".equals(estatusCuenta))
              estatusCampo = ",ic_estatus_cecoban";

          }//while
          rs.close();ps.close();

          // Si la cuenta a registrar es en dolares, autom�ticamente se almacenar�n con el estatus 99 (Cuenta Correcta).
          if(ic_moneda.equals("54")){
            estatusCampo 	= ",ic_estatus_cecoban";
            estatusCuenta 	= ",99";
          }

          qrySentencia =  " SELECT nvl(max(ic_cuenta),0) as total ";
          qrySentencia += " FROM com_cuentas ";
          qrySentencia += " WHERE ic_nafin_electronico = " + ic_nafin_electronico;
          qrySentencia += " AND ic_tipo_cuenta = " + ic_tipo_cuenta;
          if("I".equals(cg_tipo_afiliado) || "B".equals(cg_tipo_afiliado)) {
            qrySentencia += " AND ic_epo = " + ic_epo;
          }

          System.out.print("DispersionBean :: ovinsertarCuenta :: Query "+qrySentencia);

          ResultSet rsBusqueda = con.queryDB(qrySentencia);

          if (rsBusqueda.next()){
            System.out.print("\nCampo"+rsBusqueda.getString("total")+" \n");
            sResBusqueda = (rsBusqueda.getString("total") == null)? "0" : rsBusqueda.getString("total");
          }

          Calendar cFechaSigHabil = new GregorianCalendar();
          cFechaSigHabil.setTime(new java.util.Date());
          cFechaSigHabil.add(Calendar.DATE, 1);
          if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
            cFechaSigHabil.add(Calendar.DATE, 2);
          if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
            cFechaSigHabil.add(Calendar.DATE, 1);
          Vector vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
          boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
          cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
          while(bInhabil) {
            vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
            bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
            if(!bInhabil)
              break;
            cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
          }
          String sFechaSigHabil = sdf.format(cFechaSigHabil.getTime());
          System.out.println("Fecha siguiente: "+sFechaSigHabil);

          String condicioncampo = "";
          String condicioncampojsp = "";

          if(!"".equals(ic_epo)) {
            condicioncampo = ",ic_epo";
            condicioncampojsp = ","+ic_epo;
          }

          if(vCveCta==null || "".equals(vCveCta)){
            sSigLlave = sLlaveSiguienteProvBaja(con);
            qrySentencia = " INSERT INTO com_cuentas  ";
            qrySentencia += " ( ";
            qrySentencia += " ic_cuenta, ic_nafin_electronico, ic_producto_nafin, ic_moneda, ";
            qrySentencia += " ic_bancos_tef, cg_cuenta, ic_usuario, df_ultima_mod,  ";
            qrySentencia += " df_registro, df_transferencia, df_aplicacion, ic_tipo_cuenta, cg_tipo_afiliado "+condicioncampo+estatusCampo + ",";
            qrySentencia += " cg_plaza_swift, ";
            qrySentencia += " cg_sucursal_swift, ";
            qrySentencia += " ic_aba, ";
            qrySentencia += " ic_swift ";
            qrySentencia += " ) ";
            qrySentencia += " VALUES ";
            qrySentencia += " ( ";
            qrySentencia += " " + sSigLlave + ", " + ic_nafin_electronico + " , " + ic_producto_nafin + ", " + ic_moneda + ", ";
            qrySentencia += " " + ic_bancos_tef + ", '" + cg_cuenta + "', '" + ic_usuario + "', SYSDATE, ";
            qrySentencia += " SYSDATE, to_date('" + sFechaSigHabil + "','dd/mm/yyyy'), to_date('" + sFechaSigHabil +"','dd/mm/yyyy'), " + ic_tipo_cuenta + " , '"+cg_tipo_afiliado+"' "+condicioncampojsp+estatusCuenta + ",";
            qrySentencia += (plazaSwift    == null?"NULL":"'"+plazaSwift    +"'")+",";
            qrySentencia += (sucursalSwift == null?"NULL":"'"+sucursalSwift +"'")+",";
            qrySentencia += (vAba  == null?"NULL":"'"+vAba    +"'")+",";
            qrySentencia += (vSwift  == null?"NULL":"'"+vSwift    +"'");
            qrySentencia += " ) ";

            System.out.print("DispersionBean :: ovinsertarCuenta :: Query "+qrySentencia);

            con.ejecutaSQL(qrySentencia);
          }else
          {
            qrySentencia = "UPDATE com_cuentas " +
            "   SET ic_bancos_tef = "+ic_bancos_tef+", " +
            "       cg_cuenta = '"+cg_cuenta+"', " +
            ((ic_moneda.equals("54"))?"":"       ic_estatus_cecoban = null, ")+
            //"       ic_estatus_cecoban = null, " +
            "			  df_ultima_mod 		 = SYSDATE, "  +
            "       ic_tipo_cuenta = "+ic_tipo_cuenta + ", " +
            "       cg_sucursal_swift = "+(sucursalSwift    == null?"NULL":"'"+sucursalSwift    +"'")+", " +
            "       cg_plaza_swift = "+(plazaSwift    == null?"NULL":"'"+plazaSwift    +"'")+", " +
            "       ic_aba = "+(vAba    == null?"NULL":"'"+vAba    +"'")+", " +
            "       ic_swift = "+(vSwift    == null?"NULL":"'"+vSwift    +"'")+
            " WHERE ic_cuenta = "+vCveCta;

            System.out.print("DispersionBean :: ovinsertarCuenta :: Query "+qrySentencia);

            con.ejecutaSQL(qrySentencia);

          }

          // Registrar en BITACORA el ALTA DE LA CUENTA para cuando la Clave del Afiliado al que se le di�
          // de alta la cuenta sea: P(PYME), D(DISTRIBUIDOR),I(IF Bancario/No Bancario) y B(IF/Beneficiario)
          if(
            "P".equals(cg_tipo_afiliado) ||
            "D".equals(cg_tipo_afiliado) ||
            "I".equals(cg_tipo_afiliado) ||
            "B".equals(cg_tipo_afiliado)
          ){

            // Registrar la informacion registrada para la cuenta
            String decripcionAlta = (String) parametrosAdicionales.get("DESCRIPCION_ALTA");
            decripcionAlta = decripcionAlta == null?"ALTA\nNO DISPONIBLE":decripcionAlta;
            String clavePantalla  = (String) parametrosAdicionales.get("PANTALLA_ORIGEN");
            clavePantalla 	= clavePantalla  == null?"REGINDCTAS"    :clavePantalla;

            //boolean esCuentaCLABE = "40".equals(ic_tipo_cuenta)?true:false;
            boolean esCuentaSWIFT = "50".equals(ic_tipo_cuenta)?true:false;

            String valoresActuales =
              decripcionAlta + "\n" +
              "cg_cuenta="+cg_cuenta+"\n"+
              "ic_producto_nafin="+ic_producto_nafin+"\n"+
              "ic_moneda="+ic_moneda+"\n"+
              "ic_bancos_tef="+ic_bancos_tef+"\n"+
              "ic_tipo_cuenta="+ic_tipo_cuenta+"\n"+
              "cg_plaza_swift="+   (esCuentaSWIFT?(plazaSwift    == null?"":plazaSwift)   :"N/A")+"\n"+
              "cg_sucursal_swift="+(esCuentaSWIFT?(sucursalSwift == null?"":sucursalSwift):"N/A")+"\n"+
              "ic_epo="+ic_epo;

            Bitacora.grabarEnBitacora(
                con,
                clavePantalla,															//clavePantalla,
                "B".equals(cg_tipo_afiliado)?"I":cg_tipo_afiliado,			//tipoAfiliado; Si el tipo es "B" usar "I"
                ic_nafin_electronico, 												//claveNafinElectronico,
                ic_usuario,																//claveUsuario,
                "",																		//datoAnterior,
                valoresActuales                                          //datoActual
            );

          }
      }

			return sSigLlave;
		}catch(NafinException ne){
			System.out.println("ovinsertarCuenta::NafinException");
			ne.printStackTrace();
			resultado = false;
			throw ne;
		}catch(Exception e){
			System.out.println("ovinsertarCuenta::Exception");
			e.printStackTrace();
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			System.out.println("\nDispersionEJB::ovinsertarCuenta (S)");
		}
	} /*Fin del M�todo ovinsertarCuenta*/

  private String sLlaveSiguienteProvBaja(AccesoDB con) throws NafinException{
		//AccesoDB		con 			= null;
		ResultSet		rs				= null;
		String			qrySentencia	= "";
		String 			sResultado		= "";

		try {
			//con = new AccesoDB();
			//con.conexionDB();

			qrySentencia = " SELECT  NVL(MAX(ic_cuenta),0)+1 FROM com_cuentas ";

			System.out.print("DispersionBean :: sLlaveSiguiente :: Query "+qrySentencia);

			rs = con.queryDB(qrySentencia);

			while(rs.next()){
				sResultado = rs.getString(1);
			}
      rs.close();

			return sResultado;

		}catch(Exception e){
			throw new NafinException("SIST0001");
		}finally{

		}
	}/*Fin del M�todo sLlaveSiguiente*/
}/* Fin de la Clase */
