package com.netro.dispersion;

import java.util.List;
import netropology.utilerias.Registros;

public class RepDispEpo {
	
	private String 		ic_epo;
	private String 		nom_epo;
	private String		df_fecha_venc;
	private String		rs_cecoban;
	private Registros 	repPymesConDisp;
	private Registros 	repPymesSinDisp;
	private Registros	repPagoIf;
	
	public RepDispEpo() { }
	
	public RepDispEpo(String ic_epo, String nom_epo, String df_fecha_venc, String rs_cecoban) {
		this.ic_epo 			= ic_epo;
		this.nom_epo 			= nom_epo;
		this.df_fecha_venc 		= df_fecha_venc;
		this.rs_cecoban   = rs_cecoban;
		this.repPymesConDisp	= new Registros();
		this.repPymesSinDisp	= new Registros();
		this.repPagoIf			= new Registros();
	}//RepDispEpo
	
	public String getIc_epo() {
		return this.ic_epo;
	}
	
	public String getNom_epo() {
		return this.nom_epo;
	}	
		
	public String getDf_fecha_venc() {
		return this.df_fecha_venc;
	}
	
	public String getRs_cecoban() {
		return this.rs_cecoban;
	}	
	
	public void setNomRepPymesConDisp(List nombres) {
		this.repPymesConDisp.setNombreColumnas(nombres);
	}//setRepPymesConDisp
	
	public void addRepPymesConDisp(List datarow) {
		this.repPymesConDisp.addDataRow(datarow);
	}//setRepPymesConDisp
	
	public Registros getRepPymesConDisp() {
		return this.repPymesConDisp;
	}
	
	public void setNomRepPymesSinDisp(List nombres) {
		this.repPymesSinDisp.setNombreColumnas(nombres);
	}//setRepPymesConDisp
	
	public void addRepPymesSinDisp(List datarow) {
		this.repPymesSinDisp.addDataRow(datarow);
	}//setRepPymesConDisp
	
	public Registros getRepPymesSinDisp() {
		return this.repPymesSinDisp;
	}
	
	public void setNomRepPagoIf(List nombres) {
		this.repPagoIf.setNombreColumnas(nombres);
	}//setRepPymesConDisp
	
	public void addRepPagoIf(List datarow) {
		this.repPagoIf.addDataRow(datarow);
	}//setRepPymesConDisp
	
	public Registros getRepPagoIf() {
		return this.repPagoIf;
	}
	
}//RepDispEpo
