/*************************************************************************************
*
* Nombre de Clase o Archivo: ITOperativasTCambio.java
*
* Versi�n: 		  1.0
*
* Fecha Creaci�n: 01/marzo/2002
*
* Autor:          Ricardo Jimenez
*
* Fecha Ult. Modificaci�n:
*
* Descripci�n de Clase: Interface remota del EJB de Tasas Operativas y Tipos de Cambio
*
*************************************************************************************/

package com.netro.toperativastcambio;

import com.netro.exception.*;
import java.util.List;
import netropology.utilerias.*;
import javax.ejb.EJBObject;
import javax.ejb.Remote;

@Remote
public interface ITOperativasTCambio {
    public void vborrarTasa(String esFechaAplicacion, String esCveTasa) throws NafinException, Exception;

    public void vactualizarTasa(String esFechaAplicacion, String esCveTasa, String esValor) throws NafinException;

    public void vborrarTCambio(String esFechaAplicacion, String esCveMoneda) throws NafinException, Exception;

    public void vactualizarTCambio(String esFechaAplicacion, String esCveMoneda, String esValor, String FechaAplicfin) throws NafinException, Exception;

    public List tasasPrimerPiso(List list, String fecha) throws NafinException, Exception;

    public void tasasSegundoPiso(List list,String fecha) throws NafinException, Exception;

    public List  getCatalogoTasa(String pantalla ) throws NafinException, Exception;

    public List  getConsCapturaTasas(String cboTasa , String txt_fecha_app ) throws NafinException, Exception;

    public String  getConsTasas(String cboTasa , String txt_fecha_app_de, String txt_fecha_app_a) throws NafinException, Exception;
}

