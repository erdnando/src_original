package com.netro.toperativastcambio;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import net.sf.json.JSONObject;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;

/*************************************************************************************
 *
 * Nombre de Clase o de Archivo: CTOperativasTCambioBean.java
 *
 * Versi�n: 		  1.0
 *
 * Fecha Creaci�n: 01/marzo/2002
 *
 * Autor:          Ricardo Jimenez
 *
 * Fecha Ult. Modificaci�n:
 *
 * Descripci�n de Clase: Clase que implementa los m�todos para el EJB de las Tasas Operativas y Tipos de Cambio.
 *
 *************************************************************************************/
@Stateless(name = "TOperativasTCambioEJB" , mappedName = "TOperativasTCambioEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class CTOperativasTCambioBean implements ITOperativasTCambio {
	private String lsDS = "seguridadData";

	/*********************************************************************************************
	*
	*	    void vborrarTasa()
	*
	*********************************************************************************************/
	public void vborrarTasa(String esFechaAplicacion, String esCveTasa)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";
		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TOperativasTCambioEJB::vborrarTasa(E)");
		try {
			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				lsCodError = "SIST0001";
				throw new NafinException("SIST0001");
			}

			lsCadenaSQL = "DELETE com_mant_tasa WHERE TO_CHAR(DC_FECHA,'DD/MM/YYYY') = '" + esFechaAplicacion + "' "+
			" AND ic_tasa = " + esCveTasa;

			try{
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}
			catch(SQLException sqle){
				lbOK = false;
				lsCodError = "TOTC0001";
				throw new NafinException("TOTC0001");  //Error al tratar de borrar la tasa
			}

		}catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" TOperativasTCambioEJB::vborrarTasa(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    int iexisteTasa()
	*
	*********************************************************************************************/
	private int iexisteTasa(String esFechaAplicacion, String esCveTasa)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";
		String lsCadenaSQL = "";
		int liExisteTasa = 0;

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TOperativasTCambioEJB::iexisteTasa(E)");

		try {
			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				lsCodError = "SIST0001";
				throw new NafinException("SIST0001");
			}

			lsCadenaSQL = "SELECT count(*) FROM com_mant_tasa "+
				" WHERE TO_CHAR(DC_FECHA,'DD/MM/YYYY') = '" + esFechaAplicacion + "' " +
				" AND IC_TASA = " + esCveTasa;

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				liExisteTasa = lrsSel.getInt(1);
			else{
				lsCodError = "TOTC0002";
				throw new NafinException("TOTC0002");  //Error al verificar si existe la tasa
			}
			lrsSel.close();
			lodbConexion.cierraStatement();

			return liExisteTasa;
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" TablasDinamicasEJB::iexisteTasa(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    boolean bvalidarDiaHabil()
	*
	*********************************************************************************************/
	private boolean bvalidarDiaHabil(String esFechaAplicacion)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";
		String lsCadenaSQL = "";
		int liExisteDiaHabil = 0;
		boolean lbExisteDiaHabil = true;

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TOperativasTCambioEJB::ivalidarDiaHabil(E)");

		try {
			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				lsCodError = "SIST0001";
				throw new NafinException("SIST0001");
			}

			lsCadenaSQL = "SELECT count(*) FROM comcat_dia_inhabil"+
				" WHERE cg_dia_inhabil = '" + esFechaAplicacion.substring(0,5) + "'";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if(lrsSel.next()){
				liExisteDiaHabil = lrsSel.getInt(1);
				if(liExisteDiaHabil == 0){
					Calendar lcalDiaHabil = Calendar.getInstance();
					int liDiaSemana = lcalDiaHabil.get(Calendar.DAY_OF_WEEK);
					if(liDiaSemana == Calendar.SUNDAY || liDiaSemana == Calendar.SATURDAY)
						lbExisteDiaHabil = false;
				}
				else
					lbExisteDiaHabil = false;
			}
			else{
				lbExisteDiaHabil = false;
			}
			lrsSel.close();
			lodbConexion.cierraStatement();

			return lbExisteDiaHabil;
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" TOperativasTCambioEJB::ivalidarDiaHabil(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    boolean bEsDiaInhabil()
	*
	*********************************************************************************************/
	private Vector bEsDiaInhabil(String esFechaAplicacion, Calendar cFechaSigHabil, AccesoDB lodbConexion)
		throws NafinException{
		boolean lbOK = true;
		String lsCodError = "OK";
		boolean lbExisteDiaInhabil = false;
		ResultSet lrsSel = null;
		Vector vFecha = new Vector();

		System.out.println("TOperativasTCambioEJB::bEsDiaInhabil(E)");
		try {
			String lsCadenaSQL = "select * from comcat_dia_inhabil"+
								" where cg_dia_inhabil = '"+esFechaAplicacion.substring(0,5)+"'";
			try {
				lrsSel = lodbConexion.queryDB(lsCadenaSQL);
				if(lrsSel.next()) {
					lbExisteDiaInhabil = true;
					cFechaSigHabil.add(Calendar.DATE, 1);
					if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
						cFechaSigHabil.add(Calendar.DATE, 2);
					if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
						cFechaSigHabil.add(Calendar.DATE, 1);
				} else
					lbExisteDiaInhabil = false;

				lrsSel.close();
				lodbConexion.cierraStatement();

				vFecha.addElement(new Boolean(lbExisteDiaInhabil));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.YEAR)));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.MONTH)));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.DAY_OF_MONTH)));

			} catch (Exception error){
				throw new NafinException("DESC0041");
			}

			return vFecha;
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			if(!lbOK) throw new NafinException(lsCodError);
			System.out.println(" TOperativasTCambioEJB::bEsDiaInhabil(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    void vactualizarTasa()
	*
	*********************************************************************************************/
	public void vactualizarTasa(String esFechaAplicacion, String esCveTasa, String esValor)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";
		String lsCadenaSQL = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TOperativasTCambioEJB::vactualizarTasa(E)");
		try {
			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				lsCodError = "SIST0001";
				throw new NafinException("SIST0001");
			}

			String sFechaActual = sdf.format(new java.util.Date());

			Calendar cFechaSigHabil = new GregorianCalendar();
			cFechaSigHabil.setTime(new java.util.Date());
			cFechaSigHabil.add(Calendar.DATE, 1);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
				cFechaSigHabil.add(Calendar.DATE, 2);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
				cFechaSigHabil.add(Calendar.DATE, 1);
			Vector vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, lodbConexion);
			boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
			cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			//System.out.println("Valor del nuevo Calendar: "+sdf.format(cFechaSigHabil.getTime()));
			while(bInhabil) {
				vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, lodbConexion);
				bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
				if(!bInhabil)
					break;
				cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			}
			String sFechaSigHabil = sdf.format(cFechaSigHabil.getTime());
			System.out.println("Fecha siguiente: "+sFechaSigHabil);

			boolean bFechaInhabil = false;
			Calendar cDia = new GregorianCalendar();
			cDia.setTime(Comunes.parseDate(esFechaAplicacion));
			int iDiaSemana = cDia.get(Calendar.DAY_OF_WEEK);
			if(iDiaSemana==7 || iDiaSemana==1) // 7 Sabado y 1 Domingo.
				throw new NafinException("TOTC0011");
//				bFechaInhabil = true;

			if(bvalidarDiaHabil(esFechaAplicacion) && (esFechaAplicacion.equals(sFechaActual) || esFechaAplicacion.equals(sFechaSigHabil)) && !bFechaInhabil){
				if(iexisteTasa(esFechaAplicacion, esCveTasa) == 0){
					lsCadenaSQL = "INSERT INTO com_mant_tasa(DC_FECHA, IC_TASA, FN_VALOR)" +
						" VALUES(TO_DATE('" + esFechaAplicacion + "','DD/MM/YYYY')" +
						" , " + esCveTasa + "," + esValor + ")";
				}
				else{
					lsCadenaSQL = "UPDATE com_mant_tasa SET  FN_VALOR = " + esValor +
					" WHERE IC_TASA = " + esCveTasa +
					" AND TO_CHAR(DC_FECHA,'DD/MM/YYYY') = '" + esFechaAplicacion + "'";
				}

				try{
					lodbConexion.ejecutaSQL(lsCadenaSQL);
				}
				catch(SQLException sqle){
					lbOK = false;
					lsCodError = "TOTC0004";
					throw new NafinException("TOTC0004");  //Error al actualizar el valor de la tasa
				}
			}
			else{
				if((Comunes.parseDate(esFechaAplicacion)).compareTo(Comunes.parseDate(sFechaSigHabil)) > 0 ){
					lbOK = false;
					lsCodError = "TOTC0009";
					throw new NafinException("TOTC0009");  //La Fecha es posterior a la del d�a siguiente h�bil.
				} if((Comunes.parseDate(sFechaActual)).compareTo(Comunes.parseDate(esFechaAplicacion)) > 0 ){
					lbOK = false;
					lsCodError = "TOTC0010";
					throw new NafinException("TOTC0010");  //La Fecha es anterior a la fecha actual, as� como a la fecha siguiente h�bil.
				} else {
					lbOK = false;
					lsCodError = "TOTC0005";
					throw new NafinException("TOTC0005");  //Los datos no han sido agregados o actualizados \n debido a que la fecha de aplicaci�n es un d�a inh�bil.
				}
			}

		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" TOperativasTCambioEJB::vactualizarTasa(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    void vborrarTCambio()
	*
	*********************************************************************************************/
	public void vborrarTCambio(String esFechaAplicacion, String esCveMoneda)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";
		String lsCadenaSQL = "";
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TOperativasTCambioEJB::vborrarTCambio(E)");
		try {
			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				lsCodError = "SIST0001";
				throw new NafinException("SIST0001");
			}

			lsCadenaSQL = "DELETE com_tipo_cambio WHERE TO_CHAR(DC_FECHA,'DD/MM/YYYY') = '" + esFechaAplicacion + "' "+
			" AND ic_moneda = " + esCveMoneda;

			try{
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}
			catch(SQLException sqle){
				lbOK = false;
				lsCodError = "TOTC0006";
				throw new NafinException("TOTC0006");  //Error al tratar de borrar el tipo de cambio
			}

		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" TOperativasTCambioEJB::vborrarTCambio(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    int iexisteTCambio()
	*
	*********************************************************************************************/
	private int iexisteTCambio(String esFechaAplicacion, String esCveMoneda)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";
		String lsCadenaSQL = "";
		int liExisteTasa = 0;

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();
		System.out.println(" TOperativasTCambioEJB::iexisteTCambio(E)");

		try {
			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				lsCodError = "SIST0001";
				throw new NafinException("SIST0001");
			}

			lsCadenaSQL = "SELECT count(*) FROM com_tipo_cambio "+
				" WHERE TO_CHAR(DC_FECHA,'DD/MM/YYYY') = '" + esFechaAplicacion + "' " +
				" AND ic_moneda = " + esCveMoneda;

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				liExisteTasa = lrsSel.getInt(1);
			else{
				lsCodError = "TOTC0007";
				throw new NafinException("TOTC0007");  //Error al verificar si existe el tipo de cambio
			}
			lrsSel.close();
			lodbConexion.cierraStatement();

			return liExisteTasa;
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" TablasDinamicasEJB::esCveMoneda(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    void vactualizarTCambio()
	*
	*********************************************************************************************/
	public void vactualizarTCambio(String esFechaAplicacion, String esCveMoneda, String esValor, String FechaAplicfin)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";
		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TOperativasTCambioEJB::vactualizarTCambio(E)");
		try {
			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				lsCodError = "SIST0001";
				throw new NafinException("SIST0001");
			}

			if(bvalidarDiaHabil(esFechaAplicacion)){
				if("40".equals(esCveMoneda) && !"".equals(FechaAplicfin)){
					System.out.println("esFechaAplicacion :: "+esFechaAplicacion);
					System.out.println("FechaAplicfin :: "+FechaAplicfin);
					DateFormat myDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
					java.util.Date fecha_ini	= myDateFormat1.parse(esFechaAplicacion);

					DateFormat myDateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
					java.util.Date fecha_fin	= myDateFormat2.parse(FechaAplicfin);

					long tot = fecha_fin.getTime() - fecha_ini.getTime();
					long days = tot / 86400000;
					System.out.println("days :: * "+days);


					for(int i=0; i<=days; i++){//inicio for
						System.out.println("i :: * "+i);


						Calendar calendar = new GregorianCalendar();
        				calendar.setTime(fecha_ini);
        				calendar.add(Calendar.DATE, i);
						java.util.Date FECHACAL = new java.util.Date(calendar.getTime().getTime());

						String fecha_temp2 = new java.text.SimpleDateFormat("dd/MM/yyyy").format(FECHACAL);


						System.out.println("fecha_temp2 :: ::"+fecha_temp2);

						System.out.println("esFechaAplicacion :: ::"+esFechaAplicacion);

						if(iexisteTCambio(fecha_temp2, esCveMoneda) == 0){
							lsCadenaSQL = "INSERT INTO com_tipo_cambio(DC_FECHA, IC_MONEDA, FN_VALOR_COMPRA)" +
								" VALUES(TO_DATE('" + fecha_temp2 + "','DD/MM/YYYY')" +
								" , " + esCveMoneda + "," + esValor + ")";
						}
						else{

							lsCadenaSQL = "UPDATE com_tipo_cambio SET  FN_VALOR_COMPRA = " + esValor +
							" WHERE ic_moneda = " + esCveMoneda +
							" AND TO_CHAR(DC_FECHA,'DD/MM/YYYY') = '" +fecha_temp2 + "' ";
						}

						try{
							System.out.println("lsCadenaSQL :: "+lsCadenaSQL);
							lodbConexion.ejecutaSQL(lsCadenaSQL);
						}
						catch(SQLException sqle){
							lbOK = false;
							lsCodError = "TOTC0008";
							throw new NafinException("TOTC0008");  //Error al actualizar el valor del tipo de cambio
						}
					}//Fin for
				}else{
					if(iexisteTCambio(esFechaAplicacion, esCveMoneda) == 0){
						lsCadenaSQL = "INSERT INTO com_tipo_cambio(DC_FECHA, IC_MONEDA, FN_VALOR_COMPRA)" +
							" VALUES(TO_DATE('" + esFechaAplicacion + "','DD/MM/YYYY')" +
							" , " + esCveMoneda + "," + esValor + ")";
					}
					else{

						lsCadenaSQL = "UPDATE com_tipo_cambio SET  FN_VALOR_COMPRA = " + esValor +
						" WHERE ic_moneda = " + esCveMoneda +
						" AND TO_CHAR(DC_FECHA,'DD/MM/YYYY') = '" + esFechaAplicacion + "'";
					}

					try{
						lodbConexion.ejecutaSQL(lsCadenaSQL);
					}
					catch(SQLException sqle){
						lbOK = false;
						lsCodError = "TOTC0008";
						throw new NafinException("TOTC0008");  //Error al actualizar el valor del tipo de cambio
					}
				}
			} //fin d�a h�bil
			else{
				lbOK = false;
				lsCodError = "TOTC0005";
				throw new NafinException("TOTC0005");  //Los datos no han sido agregados o actualizados \n debido a que la fecha de aplicaci�n es un d�a inh�bil.
			}

		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" TOperativasTCambioEJB::vactualizarTasa(S)");
		}
	}

  /*************************************************
   * METODOS PARA LA CAPTURA MASIVA DE TASAS BASE
   * @MOISES DIAZ
   * ***********************************************/

  /**
   * Se encarga de realizar las actualizaciones e inserciones de las tasas del archivol
   * @throws com.netro.exception.NafinException
   * @return lista de diferencias.
   * @param list
   */
  public List tasasPrimerPiso(List list,String fecha)
		throws NafinException{
    System.out.println(" TOperativasTCambioEJB::tasasPrimerPiso(E)");
    AccesoDB con 					    = new AccesoDB();
    PreparedStatement ps      = null;
    ResultSet rs              = null;
    ArrayList diferencias     = new ArrayList();

    try
    {
    List _1erPlazo  = (ArrayList)list.get(27); //1-60 d�as
    List _2doPlazo  = (ArrayList)list.get(28); //61-120 d�as
    List _3erPlazo  = (ArrayList)list.get(29); //121-180 d�as
    List _4toPlazo  = (ArrayList)list.get(30); //181-366 d�as
    List _5toPlazo  = (ArrayList)list.get(31); //367-540 d�as
    List _6toPlazo  = (ArrayList)list.get(32); //541-720 d�as

    List _t1erPlazo  = (ArrayList)list.get(43); //1-60 d�as
    List _t2doPlazo  = (ArrayList)list.get(44); //61-120 d�as
    List _t3erPlazo  = (ArrayList)list.get(45); //121-180 d�as
    List _t4toPlazo  = (ArrayList)list.get(46); //181-366 d�as
    List _t5toPlazo  = (ArrayList)list.get(47); //367-540 d�as
    List _t6toPlazo  = (ArrayList)list.get(48); //541-720 d�as

    int y = 0;
    con.conexionDB();

    for(int i=1;i<=15;i++)// PLAZO 1
    {

      int z = 1;
      String qryConsultaSobretasa = "SELECT CD_DESCRIPCION DES,FG_SOBRE_TASA ST FROM COMCAT_PRODUCTO_PISO1 WHERE IC_PRODUCTO_PISO1="+i;
      ps=con.queryPrecompilado(qryConsultaSobretasa);
      rs=ps.executeQuery();
      if(rs.next())
      {
        insertaTasa(i,1,60,Double.parseDouble(_1erPlazo.get(3).toString()),rs.getDouble("ST"),(Double.parseDouble(_1erPlazo.get(3).toString())+rs.getDouble("ST")),"H",con,fecha);
        if(i<=6){
            if(i==1){z=1;}if(i==2){z=3;}if(i==3){z=5;}if(i==4){z=6;}if(i==5){z=8;}if(i==6){z=9;}
            if(!_1erPlazo.get(5+z).toString().equals("") && !_1erPlazo.get(5+z).toString().equals("n.a.")){
              if(!Comunes.formatoDecimal(_1erPlazo.get(5+z),4).equals(Comunes.formatoDecimal((Double.parseDouble(_1erPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 1 - 60 d�as");
              }
            }
        }
        else{
          if(!_t1erPlazo.get(6+y).toString().equals("") && !_t1erPlazo.get(6+y).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_t1erPlazo.get(6+y),4).equals(Comunes.formatoDecimal((Double.parseDouble(_1erPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 1 - 60 d�as");

            }
          }
          y++;
        }
      }
      ps.close();
      rs.close();
      if(actualizaTasaPiso1(Double.parseDouble(_1erPlazo.get(3).toString()),i,1,60,con,fecha)==0){
        insertaTasa(i,1,60,Double.parseDouble(_1erPlazo.get(3).toString()),0.0,0.0,"",con,fecha);
      }
    }
    y=0;
    for(int i=1;i<=15;i++)// PLAZO 2
    {
      int z = 1;
      String qryConsultaSobretasa = "SELECT CD_DESCRIPCION DES,FG_SOBRE_TASA ST FROM COMCAT_PRODUCTO_PISO1 WHERE IC_PRODUCTO_PISO1="+i;
      ps=con.queryPrecompilado(qryConsultaSobretasa);
      rs=ps.executeQuery();
      if(rs.next()){
        insertaTasa(i,61,120,Double.parseDouble(_2doPlazo.get(3).toString()),rs.getDouble("ST"),(Double.parseDouble(_2doPlazo.get(3).toString())+rs.getDouble("ST")),"H",con,fecha);
        if(i<=6){
          if(i==1){z=1;}if(i==2){z=3;}if(i==3){z=5;}if(i==4){z=6;}if(i==5){z=8;}if(i==6){z=9;}
          if(!_2doPlazo.get(5+z).toString().equals("") && !_2doPlazo.get(5+z).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_2doPlazo.get(5+z),4).equals(Comunes.formatoDecimal((Double.parseDouble(_2doPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 61 - 120 d�as");
            }
          }
        }else{
          if(!_t2doPlazo.get(6+y).toString().equals("") && !_t2doPlazo.get(6+y).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_t2doPlazo.get(6+y),4).equals(Comunes.formatoDecimal((Double.parseDouble(_2doPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 61 - 120 d�as");

            }
          }
          y++;
        }
      }
      if(actualizaTasaPiso1(Double.parseDouble(_2doPlazo.get(3).toString()),i,61,120,con,fecha)==0){
        insertaTasa(i,61,120,Double.parseDouble(_2doPlazo.get(3).toString()),0.0,0.0,"",con,fecha);
      }
    }
    y=0;
    for(int i=1;i<=15;i++)// PLAZO 3
    {
      int z = 1;
      String qryConsultaSobretasa = "SELECT CD_DESCRIPCION DES,FG_SOBRE_TASA ST FROM COMCAT_PRODUCTO_PISO1 WHERE IC_PRODUCTO_PISO1="+i;
      ps=con.queryPrecompilado(qryConsultaSobretasa);
      rs=ps.executeQuery();
      if(rs.next()){
        insertaTasa(i,121,180,Double.parseDouble(_3erPlazo.get(3).toString()),rs.getDouble("ST"),(Double.parseDouble(_3erPlazo.get(3).toString())+rs.getDouble("ST")),"H",con,fecha);
        if(i<=6){
          if(i==1){z=1;}if(i==2){z=3;}if(i==3){z=5;}if(i==4){z=6;}if(i==5){z=8;}if(i==6){z=9;}
          if(!_3erPlazo.get(5+z).toString().equals("") && !_3erPlazo.get(5+z).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_3erPlazo.get(5+z),4).equals(Comunes.formatoDecimal((Double.parseDouble(_3erPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 121 - 180 d�as");
            }
          }
      }else{
          if(!_t3erPlazo.get(6+y).toString().equals("") && !_t3erPlazo.get(6+y).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_t3erPlazo.get(6+y),4).equals(Comunes.formatoDecimal((Double.parseDouble(_3erPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 121 - 180 d�as");

            }
          }
          y++;
        }
      }
      if(actualizaTasaPiso1(Double.parseDouble(_3erPlazo.get(3).toString()),i,121,180,con,fecha)==0){
        insertaTasa(i,121,180,Double.parseDouble(_3erPlazo.get(3).toString()),0.0,0.0,"",con,fecha);
      }
    }
    y=0;
    for(int i=1;i<=15;i++)//PLAZO 4
    {
      int z = 1;
      String qryConsultaSobretasa = "SELECT CD_DESCRIPCION DES,FG_SOBRE_TASA ST FROM COMCAT_PRODUCTO_PISO1 WHERE IC_PRODUCTO_PISO1="+i;
      ps=con.queryPrecompilado(qryConsultaSobretasa);
      rs=ps.executeQuery();
      if(rs.next()){
        insertaTasa(i,181,366,Double.parseDouble(_4toPlazo.get(3).toString()),rs.getDouble("ST"),(Double.parseDouble(_4toPlazo.get(3).toString())+rs.getDouble("ST")),"H",con,fecha);
        if(i<=6){
          if(i==1){z=1;}if(i==2){z=3;}if(i==3){z=5;}if(i==4){z=6;}if(i==5){z=8;}if(i==6){z=9;}
          if(!_4toPlazo.get(5+z).toString().equals("") && !_4toPlazo.get(5+z).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_4toPlazo.get(5+z),4).equals(Comunes.formatoDecimal((Double.parseDouble(_4toPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 181 - 366 d�as");
            }
          }
        }else{
          if(!_t4toPlazo.get(6+y).toString().equals("") && !_t4toPlazo.get(6+y).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_t4toPlazo.get(6+y),4).equals(Comunes.formatoDecimal((Double.parseDouble(_4toPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 181 - 366 d�as");

            }
          }
          y++;
        }
      }
    if(actualizaTasaPiso1(Double.parseDouble(_4toPlazo.get(3).toString()),i,181,366,con,fecha)==0){
        insertaTasa(i,181,366,Double.parseDouble(_4toPlazo.get(3).toString()),0.0,0.0,"",con,fecha);
      }
    }
    y=0;
    for(int i=1;i<=15;i++)// PLAZO 5
    {
      int z = 1;
      String qryConsultaSobretasa = "SELECT CD_DESCRIPCION DES,FG_SOBRE_TASA ST FROM COMCAT_PRODUCTO_PISO1 WHERE IC_PRODUCTO_PISO1="+i;
      ps=con.queryPrecompilado(qryConsultaSobretasa);
      rs=ps.executeQuery();
      if(rs.next()){
        insertaTasa(i,367,540,Double.parseDouble(_5toPlazo.get(3).toString()),rs.getDouble("ST"),(Double.parseDouble(_5toPlazo.get(3).toString())+rs.getDouble("ST")),"H",con,fecha);
        if(i<=6){
          if(i==1){z=1;}if(i==2){z=3;}if(i==3){z=5;}if(i==4){z=6;}if(i==5){z=8;}if(i==6){z=9;}
          if(!_5toPlazo.get(5+z).toString().equals("") && !_5toPlazo.get(5+z).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_5toPlazo.get(5+z),4).equals(Comunes.formatoDecimal((Double.parseDouble(_5toPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 367 - 540 d�as");
            }
          }
        }else{
          if(!_t5toPlazo.get(6+y).toString().equals("") && !_t5toPlazo.get(6+y).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_t5toPlazo.get(6+y),4).equals(Comunes.formatoDecimal((Double.parseDouble(_5toPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 367 - 540 d�as");

            }
          }
          y++;
        }
      }
      if(actualizaTasaPiso1(Double.parseDouble(_5toPlazo.get(3).toString()),i,367,540,con,fecha)==0){
        insertaTasa(i,367,540,Double.parseDouble(_5toPlazo.get(3).toString()),0.0,0.0,"",con,fecha);
      }
    }
    y=0;
    for(int i=1;i<=15;i++)// PLAZO 6
    {
      int z = 1;
      String qryConsultaSobretasa = "SELECT CD_DESCRIPCION DES,FG_SOBRE_TASA ST FROM COMCAT_PRODUCTO_PISO1 WHERE IC_PRODUCTO_PISO1="+i;
      ps=con.queryPrecompilado(qryConsultaSobretasa);
      rs=ps.executeQuery();
      if(rs.next()){
        insertaTasa(i,541,720,Double.parseDouble(_6toPlazo.get(3).toString()),rs.getDouble("ST"),(Double.parseDouble(_6toPlazo.get(3).toString())+rs.getDouble("ST")),"H",con,fecha);
        if(i<=6){
          if(i==1){z=1;}if(i==2){z=3;}if(i==3){z=5;}if(i==4){z=6;}if(i==5){z=8;}if(i==6){z=9;}
          if(!_6toPlazo.get(5+z).toString().equals ("") && !_6toPlazo.get(5+z).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_6toPlazo.get(5+z),4).equals(Comunes.formatoDecimal((Double.parseDouble(_6toPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 541 - 720 d�as");
            }
          }
      }else{
          if(!_t6toPlazo.get(6+y).toString().equals("") && !_t6toPlazo.get(6+y).toString().equals("n.a.")){
            if(!Comunes.formatoDecimal(_t6toPlazo.get(6+y),4).equals(Comunes.formatoDecimal((Double.parseDouble(_6toPlazo.get(3).toString())+rs.getDouble("ST")),4))){
              diferencias.add("Producto: "+rs.getString("DES")+" Plazo: 541 - 720 d�as");

            }
          }
          y++;
        }
      }
      if(actualizaTasaPiso1(Double.parseDouble(_6toPlazo.get(3).toString()),i,541,720,con,fecha)==0){
        insertaTasa(i,541,720,Double.parseDouble(_6toPlazo.get(3).toString()),0.0,0.0,"",con,fecha);
      }
    }
    //con.terminaTransaccion(true);
    rs.close();
    ps.close();

    //if(!diferencias.isEmpty()){for(int i=0;i<diferencias.size();i++){System.out.println("datos: "+diferencias.get(i));}}
    }
    catch(Exception e) {
			System.out.println("Error al leer propiedades");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
      if (con.hayConexionAbierta())
				con.cierraConexionDB();
      System.out.println(" TOperativasTCambioEJB::tasasPrimerPiso(S)");
		}
    return diferencias;
  }

  /**
   * Actualiza la tasa base del producto.
   * @return 1 si se realizo la consulta de lo contrario 0
   * @param fin
   * @param inicio
   * @param producto
   * @param newTasa
   */
  public int actualizaTasaPiso1(double newTasa,int producto,int inicio,int fin,AccesoDB con,String fecha)
  {
    String qryActualizaTasa  = "";
    PreparedStatement ps     = null;
    int i=0;
    try
    {
      qryActualizaTasa = "UPDATE COM_TASA_PISO1 SET DC_FECHA=TO_DATE('"+fecha+"','DD/MM/YYYY'), FN_TASA_BASE=? WHERE IC_PRODUCTO_PISO1=? AND IG_PLAZO_INI=? AND IG_PLAZO_FIN=?";
      ps = con.queryPrecompilado(qryActualizaTasa);
      ps.setDouble(1,newTasa);
      ps.setInt(2,producto);
      ps.setInt(3,inicio);
      ps.setInt(4,fin);
      i=ps.executeUpdate();
      ps.close();

    }
    catch(Exception e)
    {
      System.out.println("Error al actualizar tasa.");
			e.printStackTrace();
    }finally {
      con.terminaTransaccion(true);
		}
    return i;
  }

  /**
   * Inserta tasa del producto
   * @param tipo
   * @param tasaTotal
   * @param sobreTasa
   * @param tasa
   * @param fin
   * @param inicio
   * @param prod
   */
  public void insertaTasa(int prod,int inicio,int fin,double tasa,double sobreTasa,double tasaTotal,String tipo,AccesoDB con,String fecha)
  {
    String qryInsertTasa    = "";
    PreparedStatement ps2   = null;
    try
    {
      if(tipo.equals(""))
      {
        qryInsertTasa = "INSERT INTO COM_TASA_PISO1 VALUES(TO_DATE('"+fecha+"','DD/MM/YYYY'),?,?,?,?)";
        ps2=con.queryPrecompilado(qryInsertTasa);
        ps2.setInt(1,prod);
        ps2.setInt(2,inicio);
        ps2.setInt(3,fin);
        ps2.setDouble(4,tasa);
      }
      else
      {
        qryInsertTasa = "INSERT INTO COMHIS_TASA_PISO1 VALUES(TO_DATE('"+fecha+"','DD/MM/YYYY'),?,?,?,?,?,?)";
        ps2=con.queryPrecompilado(qryInsertTasa);
        ps2.setInt(1,prod);
        ps2.setInt(2,inicio);
        ps2.setInt(3,fin);
        ps2.setDouble(4,tasa);
        ps2.setDouble(5,sobreTasa);
        ps2.setDouble(6,tasaTotal);
      }
      ps2.executeUpdate();
      ps2.close();
    }
    catch(Exception e)
    {
      System.out.println("Error al insertar");
			e.printStackTrace();
		}finally {
      con.terminaTransaccion(true);
		}
  }

  /**
   * Actualiza las tasas del segundo piso del dia si existen si no inserta.
   * @throws com.netro.exception.NafinException
   * @param list
   */
  public void tasasSegundoPiso(List list, String fecha)
		throws NafinException{
    System.out.println(" TOperativasTCambioEJB::tasasPrimerPiso(E)");
    AccesoDB con 					    = new AccesoDB();
    PreparedStatement ps      = null;
    ResultSet rs              = null;
    ArrayList tasas           = new ArrayList();
    ArrayList exist           = new ArrayList();

    try
    {
    List _1erPlazo  = (ArrayList)list.get(11); //1-60 d�as
    List _2doPlazo  = (ArrayList)list.get(12); //61-120 d�as
    List _3erPlazo  = (ArrayList)list.get(13); //121-180 d�as
    List _4toPlazo  = (ArrayList)list.get(14); //181-366 d�as
    List _5toPlazo  = (ArrayList)list.get(17); //367-540 d�as
    con.conexionDB();

    String buscaTasas = "SELECT ic_tasa FROM comcat_tasa WHERE cs_disponible = 'S' AND ic_tasa IN (20, 74, 135, 136, 137) ORDER BY 1";
    ps = con.queryPrecompilado(buscaTasas);
    rs = ps.executeQuery();
    while(rs.next())
    {
      tasas.add(rs.getString("ic_tasa"));
    }
    ps.close();rs.close();

    for(int i=0;i<tasas.size();i++)
    {
      String qryBuscaValor = " SELECT COUNT (1) X FROM com_mant_tasa WHERE ic_tasa = "+tasas.get(i)+" AND dc_fecha >= TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY')) AND dc_fecha < TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY') + 1)";
      ps = con.queryPrecompilado(qryBuscaValor);
      rs = ps.executeQuery();
      if(rs.next())
      {
        exist.add(rs.getString("X"));
      }
      ps.close();rs.close();
    }

    if(exist.get(0).equals("1"))
    {
      String qryUpdateTasam1 = "UPDATE COM_MANT_TASA SET FN_VALOR="+_1erPlazo.get(3)+"WHERE IC_TASA=20 AND dc_fecha >= TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY')) AND dc_fecha < TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY') + 1)";
      ps=con.queryPrecompilado(qryUpdateTasam1);
      ps.executeUpdate();
    }else
    {
      String qryInsertaTasam1 = "INSERT INTO COM_MANT_TASA (dc_fecha,ic_tasa,fn_valor) VALUES(TO_DATE('"+fecha+"','DD/MM/YYYY'),20,"+_1erPlazo.get(3)+")";
      ps=con.queryPrecompilado(qryInsertaTasam1);
      ps.executeUpdate();
    }
    if(exist.get(1).equals("1"))
    {
      String qryUpdateTasam1 = "UPDATE COM_MANT_TASA SET FN_VALOR="+_2doPlazo.get(3)+"WHERE IC_TASA=74 AND dc_fecha >= TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY')) AND dc_fecha < TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY') + 1)";
      ps=con.queryPrecompilado(qryUpdateTasam1);
      ps.executeUpdate();
    }else
    {
      String qryInsertaTasam1 = "INSERT INTO COM_MANT_TASA (dc_fecha,ic_tasa,fn_valor) VALUES(TO_DATE('"+fecha+"','DD/MM/YYYY'),74,"+_2doPlazo.get(3)+")";
      ps=con.queryPrecompilado(qryInsertaTasam1);
      ps.executeUpdate();
    }
    if(exist.get(2).equals("1"))
    {
      String qryUpdateTasam1 = "UPDATE COM_MANT_TASA SET FN_VALOR="+_3erPlazo.get(3)+"WHERE IC_TASA=135 AND dc_fecha >= TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY')) AND dc_fecha < TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY') + 1)";
      ps=con.queryPrecompilado(qryUpdateTasam1);
      ps.executeUpdate();
    }else
    {
      String qryInsertaTasam1 = "INSERT INTO COM_MANT_TASA (dc_fecha,ic_tasa,fn_valor) VALUES(TO_DATE('"+fecha+"','DD/MM/YYYY'),135,"+_3erPlazo.get(3)+")";
      ps=con.queryPrecompilado(qryInsertaTasam1);
      ps.executeUpdate();
    }
    if(exist.get(3).equals("1"))
    {
      String qryUpdateTasam1 = "UPDATE COM_MANT_TASA SET FN_VALOR="+_4toPlazo.get(3)+"WHERE IC_TASA=136 AND dc_fecha >= TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY')) AND dc_fecha < TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY') + 1)";
      ps=con.queryPrecompilado(qryUpdateTasam1);
      ps.executeUpdate();
    }else
    {
      String qryInsertaTasam1 = "INSERT INTO COM_MANT_TASA (dc_fecha,ic_tasa,fn_valor) VALUES(TO_DATE('"+fecha+"','DD/MM/YYYY'),136,"+_4toPlazo.get(3)+")";
      ps=con.queryPrecompilado(qryInsertaTasam1);
      ps.executeUpdate();
    }
    if(exist.get(4).equals("1"))
    {
      String qryUpdateTasam1 = "UPDATE COM_MANT_TASA SET FN_VALOR="+_5toPlazo.get(3)+"WHERE IC_TASA=137 AND dc_fecha >= TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY')) AND dc_fecha < TRUNC (TO_DATE('"+fecha+"','DD/MM/YYYY') + 1)";
      ps=con.queryPrecompilado(qryUpdateTasam1);
      ps.executeUpdate();
    }else
    {
      String qryInsertaTasam1 = "INSERT INTO COM_MANT_TASA (dc_fecha,ic_tasa,fn_valor) VALUES(TO_DATE('"+fecha+"','DD/MM/YYYY'),137,"+_5toPlazo.get(3)+")";
      ps=con.queryPrecompilado(qryInsertaTasam1);
      ps.executeUpdate();
    }

    ps.close();
    }
    catch(Exception e)
    {
      System.out.println("Error al insertar en COM_MANT_TASA");
			e.printStackTrace();
		}finally{
      con.terminaTransaccion(true);
      if (con.hayConexionAbierta())
				con.cierraConexionDB();

		}
    }

	 /**
	 *metodo para mostrar las tasas en el combo de la pantalla
	 * tasa operativas
	 * @return
	 * @param pantalla
	 */
public List  getCatalogoTasa(String pantalla ){
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;

	HashMap datos =  new HashMap();
	List registros  = new ArrayList();

	try{
		con.conexionDB();

		String SQL= " SELECT ic_tasa as clave, "+
						" cd_nombre as descripcion "+
						" FROM comcat_tasa "+
						" WHERE cs_disponible = 'S' " +
						" ORDER BY 2";
		rs = con.queryDB(SQL);

		if(pantalla.equals("Captura")) {
			datos = new HashMap();
			datos.put("clave", "TODAS");   
			datos.put("descripcion", "Todas");
			registros.add(datos);
		} else if(pantalla.equals("Consulta")){ // F021 - 2015 by jshernandez
			datos = new HashMap();
			datos.put("clave", "TODAS");   
			datos.put("descripcion", "Todas");
			registros.add(datos);
		}
		while(rs.next()) {
			datos = new HashMap();
			String clave = rs.getString(1);
			String descripcion = rs.getString(2);
			datos.put("clave", clave);
			datos.put("descripcion", descripcion);
			registros.add(datos);
		}
		rs.close();
		con.cierraStatement();

	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return registros;
}

/**
	 *consulta para mostrar el grid de las tasas operativas en la
	 * pantalla de captura
	 * @return
	 * @param txt_fecha_app
	 * @param cboTasa
	 */

public List  getConsCapturaTasas(String cboTasa , String txt_fecha_app ){
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	PreparedStatement ps = null;
	HashMap datos =  new HashMap();
	List registros  = new ArrayList();
	List varBind  = new ArrayList();
	try{
		con.conexionDB();

		String qrySentencia= 	" SELECT  cat.ic_tasa as IC_TASA, "+
								"  cat.cd_nombre as NOMBRE_TASA , "+
								"  v.valor as VALOR "   +
								"   FROM comcat_tasa cat,"   +
								"        (SELECT man.ic_tasa, man.fn_valor AS valor"   +
								"           FROM com_mant_tasa man"   +
								"          WHERE TRUNC (man.dc_fecha) = TRUNC (TO_DATE (? , 'dd/mm/yyyy'))) v"   +
								"  WHERE cat.ic_tasa = v.ic_tasa (+)"   +
								"    AND cat.cs_disponible = 'S'"  ;

			varBind.add(txt_fecha_app);

			if(!"TODAS".equals(cboTasa)) {
				qrySentencia += " AND cat.ic_tasa =  ?";
				varBind.add(cboTasa);
			}
			qrySentencia += " ORDER BY 1 ";


		 ps = con.queryPrecompilado(qrySentencia,varBind );
		 rs = ps.executeQuery();
		while(rs.next()) {

			String ic_tasa 	= rs.getString("IC_TASA")==null?"":rs.getString("IC_TASA");
			String nombre_tasa 			= rs.getString("NOMBRE_TASA")==null?"":rs.getString("NOMBRE_TASA");
			String valor_tasa			= rs.getString(3)==null?"":rs.getString(3);

			datos = new HashMap();
			datos.put("IC_TASA", ic_tasa);
			datos.put("NOMBRE_TASA", nombre_tasa);
			datos.put("VALOR", valor_tasa);
			datos.put("ELIMINAR", "false");

			registros.add(datos);
		}
		rs.close();
		con.cierraStatement();

	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return registros;
}

/**
	 * consulta para mostrar el grid de las tasas operativas en la
	 * pantalla de Consulta
	 * @return
	 * @param txt_fecha_app_a
	 * @param txt_fecha_app_de
	 * @param cboTasa
	 */
public String  getConsTasas(String cboTasa , String txt_fecha_app_de, String txt_fecha_app_a  ){
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	PreparedStatement ps = null;
	HashMap datos =  new HashMap();
	List registros  = new ArrayList();
	List varBind  = new ArrayList();

	String infoRegresar  ="", nombre_tasa = "", valor_tasa="", fecha="";
	JSONObject   jsonObj = new JSONObject();

	try{
		con.conexionDB();

		String qrySentencia =
			" SELECT "+
			"  cat.cd_nombre  as  NOMBRE_TASA , " +
			" v.valor as VALOR_TASA,    "   +
			"  TO_CHAR (fecha, 'dd/mm/yyyy')  as FECHA "+
			"   FROM comcat_tasa cat,"   +
			"        (SELECT man.ic_tasa, man.fn_valor AS valor, man.dc_fecha AS fecha"   +
			"           FROM com_mant_tasa man"   +
			"          WHERE TRUNC (man.dc_fecha) BETWEEN TRUNC (TO_DATE (? , 'dd/mm/yyyy')) AND TRUNC (TO_DATE (? , 'dd/mm/yyyy'))) v"   +
			"  WHERE cat.ic_tasa = v.ic_tasa "   +
			"    AND cat.cs_disponible = 'S'";
			
			
			varBind.add(txt_fecha_app_de);
			varBind.add(txt_fecha_app_a);
			
			if("TODAS".equals(cboTasa)){ // F021 - 2015 by jshernandez
				qrySentencia += "    ORDER by NOMBRE_TASA, FECHA";
			} else {
				qrySentencia += "    AND cat.ic_tasa = ?  ORDER by 3";
				varBind.add(cboTasa);
			}
			
			System.out.println("qrySentencia  "+qrySentencia);
			System.out.println("varBind  "+varBind);


		 ps = con.queryPrecompilado(qrySentencia,varBind );
		 rs = ps.executeQuery();


		while(rs.next()) {

			valor_tasa 	= rs.getString("VALOR_TASA")==null?"":rs.getString("VALOR_TASA");
			nombre_tasa 			= rs.getString("NOMBRE_TASA")==null?"":rs.getString("NOMBRE_TASA");
			fecha 			= rs.getString("FECHA")==null?"":rs.getString("FECHA");

			datos = new HashMap();
			datos.put("FECHA", fecha);
			datos.put("NOMBRE_TASA",nombre_tasa);
			datos.put("VALOR", valor_tasa);
			registros.add(datos);
		}
		rs.close();
		con.cierraStatement();


		jsonObj.put("registros", registros);
		jsonObj.put("nombre_tasa", nombre_tasa);
		jsonObj.put("success",  new Boolean(true));
		infoRegresar = jsonObj.toString();

	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		
		if(rs != null) { try { rs.close();}catch(Exception e){} }
		if(ps != null) { try { ps.close();}catch(Exception e){} }
	
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return infoRegresar;
}


}// Fin de la Clase


