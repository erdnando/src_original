package com.netro.anticipos;

import com.netro.exception.NafinException;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.Combos;

@Stateless(name = "LineaCreditoEJB" , mappedName = "LineaCreditoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class LineaCreditoBean implements LineaCredito {
	
	private String          numeroSirac    		= "";
	private String          folioArmado    		= "";
	private String          tipoSolicitud  		= "";
	private String          consecutivo   		= "";
	private String			lineasIF			= "";
	private	String			defLineaAut			= "";
	private String			defLineaInd			= "";
	private String			defLineaMax			= "";
	private int 			pymesAutorizadas	= 0;

  public String getFolioArmado(String ic_linea,String ic_pyme,String tipo_solicitud)
  throws NafinException
  {
  String qrySentencia = "";
  ResultSet rs = null;
  AccesoDB  con = null;
    try {
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia = "select in_numero_sirac from comcat_pyme where ic_pyme = "+ic_pyme;
      rs = con.queryDB(qrySentencia);
      if(rs.next())
        numeroSirac = rs.getString(1);
      con.cierraStatement();
      while(numeroSirac.length()<12)
        numeroSirac = "0"+numeroSirac;
        folioArmado = tipo_solicitud+numeroSirac+ic_linea;
    } catch(Exception e){
        folioArmado = "";
        System.out.println("Error en LineaCreditoBean::getFolioArmado(1,1,1) " +e.getMessage());
        throw new NafinException("SIST0001");
    } finally {
      if(con.hayConexionAbierta())
        con.cierraConexionDB();
    }
  return folioArmado;
  } //fin getFolioArmado


    //devuelve el folio armado cuando la solicitud ya tiene el tipo y la pyme ya esta asociada
  public String getFolioArmado(String ic_linea)
  throws NafinException
    {
    String    qrySentencia = "";
    AccesoDB  con          = null;
    ResultSet rs           = null;
    try {
        con = new AccesoDB();
        con.conexionDB();
        qrySentencia =  "select LC.cg_tipo_solicitud,PY.in_numero_sirac" +
                          " from com_linea_credito LC,comcat_pyme PY"+
                          " where LC.ic_pyme = PY.ic_pyme "+
                          " and LC.ic_linea_credito ="+ic_linea;
        rs = con.queryDB(qrySentencia);
        if(rs.next())
          {
          tipoSolicitud = rs.getString(1);
          numeroSirac   = rs.getString(2);
          }
        con.cierraStatement();
        while(numeroSirac.length()<7)
          numeroSirac = "0"+numeroSirac;
        folioArmado = tipoSolicitud+numeroSirac+ic_linea;
    } catch(Exception e) {
        folioArmado = "";
        System.out.println("Error en LineaCreditoBean::getFolioArmado(1)");
        throw new NafinException("SIST0001");
    } finally {
        if(con.hayConexionAbierta())
          con.cierraConexionDB();
    }
      return folioArmado;
  }     //fin getFolioArmado


  public void setFolioArmado(String folio)
    {
    folioArmado = folio;
    }


  public String getTipoSolicitud() throws NafinException{
    try{
      if(!"".equals(folioArmado)){
      	tipoSolicitud = folioArmado.substring(0,1);
      	System.out.println("**** getTipoSolicitud **** tipoSolicitud:::: "+tipoSolicitud);
      }       
    }catch(Exception e){
      tipoSolicitud = "-1";
      System.out.println("**** Error:: getTipoSolicitud ****");
      e.printStackTrace();
      throw new NafinException("SIST0001");
      }
    return tipoSolicitud;
    }   //fin getTipoSolicitud


	public String getNumeroSirac() throws NafinException{
    	long num = 0;
	    try{
	    	if(!"".equals(folioArmado))
	        	numeroSirac = folioArmado.substring(1,13);
	      	num = Long.parseLong(numeroSirac);
	      	numeroSirac = String.valueOf(num);
	      	System.out.println(" ***** getNumeroSirac ::: numeroSirac:::: "+numeroSirac);
		}catch(Exception e){
	      numeroSirac = "-1";
	      System.out.println(" ***** Error getNumeroSirac ***** ");
	      e.printStackTrace();
	      throw new NafinException("SIST0001");
		}
	    return numeroSirac;
	}      //fin getNumeroSirac


  public String getConsecutivo(){
    try{
      if(!"".equals(folioArmado))
        consecutivo = folioArmado.substring(8);
      System.out.println(" ***** getConsecutivo ::: consecutivo:::: "+consecutivo);
     }catch(Exception e){
      System.out.println("LineasCredito.getConsecutivo Exception "+e);
      e.printStackTrace();
      consecutivo = "-1";
     }
    return consecutivo;
  }    // fin getConsecutivo

  public void borraSolicitud(String ic_linea_credito) throws NafinException
    {
    AccesoDB con = null;
    String qrySentencia = "";
    boolean resultado = true;
      try {
        con = new AccesoDB();
        con.conexionDB();
  			qrySentencia = "delete com_linea_credito where ic_linea_credito = "+ic_linea_credito;
  			con.ejecutaSQL(qrySentencia);
      } catch(Exception e) {
          resultado = false;
          throw new NafinException("SIST0001");
      } finally {
          con.terminaTransaccion(resultado);
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
      }
    }

  public void generaSolicitud(String ic_linea_credito,String ic_epo,String ic_pyme,String ic_if,String cg_tipo_solicitud,String ic_linea_padre) throws NafinException
    {
	System.out.println("LineaCreditoBean::generaSolicitud(E)");
    AccesoDB  con           = null;
    String    qrySentencia  = "";
    boolean   resultado     = true;
    boolean   continuar     = false;
    ResultSet rs            = null;
      try {
        con = new AccesoDB();
        con.conexionDB();
  			qrySentencia = 	"select ic_linea_credito from com_linea_credito"+
							" where ic_linea_credito ="+ic_linea_credito+
							" and ic_estatus_linea is null";
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				continuar = true;
      con.cierraStatement();
      if("I".equals(cg_tipo_solicitud)&&continuar)
        {
				qrySentencia =	"update com_linea_credito " +
				  				"set ic_pyme = "+ ic_pyme +
					  			", ic_epo = "+ic_epo+
						  		", ic_producto_nafin = 2"+
							  	", ic_if = "+ic_if+
  								", cg_tipo_solicitud = 'I'"+
	  							", ic_estatus_linea = 1"+
		  						",df_captura = sysdate"+
			  					" where ic_linea_credito = "+ic_linea_credito;
        }
      else if(continuar)
        {
				qrySentencia = 	" UPDATE COM_LINEA_CREDITO "+
								" SET(IC_PYME,IC_IF,CG_TIPO_SOLICITUD"+
								" ,IC_ESTATUS_LINEA,IC_LINEA_CREDITO_PADRE"+
								" ,IC_FINANCIERA,CG_NUMERO_CUENTA,IC_EPO,IC_PRODUCTO_NAFIN,DF_CAPTURA)"+
								" = (SELECT '"+ic_pyme+"',ic_if,'"+cg_tipo_solicitud+"'"+
								" ,1,IC_LINEA_CREDITO,IC_FINANCIERA,CG_NUMERO_CUENTA,IC_EPO,IC_PRODUCTO_NAFIN,SYSDATE"+
								" FROM COM_LINEA_CREDITO WHERE IC_LINEA_CREDITO ="+ic_linea_padre+")"+
								" WHERE IC_LINEA_CREDITO = "+ic_linea_credito;
        }
      else
        throw new NafinException("ANTI0019");
	  System.out.println("El query de la linea de credito = "+qrySentencia);
      con.ejecutaSQL(qrySentencia);
      }catch(NafinException ne){
          resultado = false;
          throw ne;
      } catch(Exception e) {
		System.out.println("LineaCreditoBean::generaSolicitud Exception "+e);
          resultado = false;
          throw new NafinException("SIST0001");
      } finally {
          con.terminaTransaccion(resultado);
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
		System.out.println("LineaCreditoBean::generaSolicitud(S)");
      }
    }

    public String getNombreEpo(String ic_epo)
      throws NafinException
      {
      String    qrySentencia  = "";
      ResultSet rs            = null;
      AccesoDB  con           = null;
      String    nombreEpo     = "";
        try {
            con = new AccesoDB();
            con.conexionDB();
  					qrySentencia = "SELECT CG_RAZON_SOCIAL FROM COMCAT_EPO WHERE IC_EPO ="+ic_epo;
	  				rs = con.queryDB(qrySentencia);
		  			if(rs.next())
  						nombreEpo = rs.getString("CG_RAZON_SOCIAL");
					  con.cierraStatement();
        }catch(Exception e){
          throw new NafinException("SIST0001");
        }finally {
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
        }
      return nombreEpo;
      }


  public String reservaLinea(String ic_pyme)
    throws NafinException
    {
    String    ic_linea_credito  = "";
    String    qrySentencia      = "";
    ResultSet rs                = null;
    AccesoDB  con               = null;
    boolean		tieneLinea				= false;
      try {
				int maxLineaCredito = 0;
				con = new AccesoDB();
				con.conexionDB();
				qrySentencia = 	"select max(ic_linea_credito) as maxLin from com_linea_credito"+
							" where ic_pyme ="+ic_pyme+
  						" and ic_estatus_linea is null";
      	rs = con.queryDB(qrySentencia);
				if(rs.next())
					{
					maxLineaCredito = rs.getInt("MAXLIN");
					if(maxLineaCredito>0)
						tieneLinea=true;
					}
				con.cierraStatement();
				if(!tieneLinea)
							{
							qrySentencia = "select max(ic_linea_credito) as maxLin from com_linea_credito";
							rs = con.queryDB(qrySentencia);
							if(rs.next())
								maxLineaCredito = rs.getInt("MAXLIN") +1;
							qrySentencia =	"insert into com_linea_credito"+
											"(ic_linea_credito,ic_pyme) values ("+maxLineaCredito+","+ic_pyme+")";
							con.ejecutaSQL(qrySentencia);
							con.terminaTransaccion(true);
							}
							ic_linea_credito = new Integer(maxLineaCredito).toString();
							qrySentencia = "";
      }catch(Exception e){
        System.out.println("Error en LineaCreditoBean::reservaLinea(1)");
        throw new NafinException("SIST0001");
      } finally {
        if(con.hayConexionAbierta())
          con.cierraConexionDB();
      }
    return ic_linea_credito;
    } //reserva linea credito

	public String getNombreIF(String ic_if)
  	throws NafinException
  	{
    String		nombreIF			= "";
    String		qrySentencia	= "";
    ResultSet	rs						= null;
    AccesoDB 	con						= null;
    	try {
      		con = new AccesoDB();
          con.conexionDB();
          qrySentencia = 	" SELECT CG_RAZON_SOCIAL FROM COMCAT_IF"+
                          " WHERE IC_IF = "+ic_if;
          rs = con.queryDB(qrySentencia);
          if(rs.next())
            nombreIF = rs.getString("CG_RAZON_SOCIAL");
          con.cierraStatement();
      }catch(Exception e){
      		throw new NafinException("SIST0001");
      } finally {
      		if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return nombreIF;
    }


	public String getTipoPersona(String ic_pyme)
  	throws NafinException
  	{
    String		tipoPersona			= "";
    String		qrySentencia	= "";
    ResultSet	rs						= null;
    AccesoDB 	con						= null;
    	try {
      		con = new AccesoDB();
          con.conexionDB();
						qrySentencia =	"select cs_tipo_persona from comcat_pyme"+
										" where ic_pyme = "+ ic_pyme;
          rs = con.queryDB(qrySentencia);
          if(rs.next())
            tipoPersona = rs.getString("CS_TIPO_PERSONA");
          con.cierraStatement();
      }catch(Exception e){
      		throw new NafinException("SIST0001");
      } finally {
      		if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return tipoPersona;
    }

	public void getCuentaCorriente(String ic_pyme, String ic_epo)
  	throws NafinException {
    String    tipoCredito   = "";
    String		qrySentencia	= "";
    ResultSet	rs						= null;
    AccesoDB 	con						= null;
    try {
      con = new AccesoDB();
      con.conexionDB();
			qrySentencia =
        "SELECT cg_tipo_credito"+
        " FROM comrel_pyme_epo_x_producto PEXP"+
        " WHERE PEXP.cg_tipo_credito = 'C'"+
        " AND PEXP.ic_producto_nafin = 4"+
        " AND PEXP.ic_pyme = "+ic_pyme+
        " AND PEXP.ic_epo = "+ic_epo;
//      System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next())
        tipoCredito = rs.getString("cg_tipo_credito");
        con.cierraStatement();
        if (!("C".equals(tipoCredito)))
          throw new NafinException("DIST0001");
      } catch(NafinException ne){
       throw ne;
      } catch(Exception e){
      		throw new NafinException("SIST0001");
      } finally {
      		if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
  }

  	public Vector getDireccion(String ic_if)
	  	throws NafinException
  	{
		Vector vecDireccion = new Vector();
    String		qrySentencia	= "";
    ResultSet	rs						= null;
    AccesoDB 	con						= null;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
				qrySentencia =	"select cg_calle,decode(cg_numero_ext,null,'S/N',cg_numero_ext) as cg_numero_ext ,decode(cg_numero_int,null,' ','-'||cg_numero_int) as cg_numero_int,cg_colonia,cg_municipio,cn_cp "+
										" from com_domicilio where cs_fiscal='S' and ic_if = "+ic_if;
						rs = con.queryDB(qrySentencia);
						if(rs.next())
							{
              vecDireccion.addElement(rs.getString("CG_CALLE")+" "+rs.getString("CG_NUMERO_EXT")+rs.getString("CG_NUMERO_INT"));
							vecDireccion.addElement(rs.getString("CG_COLONIA"));
              vecDireccion.addElement(rs.getString("CG_MUNICIPIO"));
							vecDireccion.addElement(rs.getString("CN_CP"));
              }
            else
            	{
              vecDireccion.addElement("");
							vecDireccion.addElement("");
              vecDireccion.addElement("");
							vecDireccion.addElement("");
              }
          con.cierraStatement();
      }catch(Exception e){
      		throw new NafinException("SIST0001");
      } finally {
      		if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecDireccion;
    }

  public void autorizaPyme(String [] lineas,int iElementos)
  	throws NafinException
  	{
    String		lineasCredito	= "";
    String		qrySentencia	= "";
    AccesoDB	con 					= null;
    boolean		resultado 		= true;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
				for(int i=0;i<iElementos;i++)
					{
					if(!"".equals(lineasCredito))
							lineasCredito += ",";
					lineasCredito += lineas[i];
		      }
				qrySentencia = "update com_linea_credito set cs_aceptacion_pyme='S' where ic_linea_credito in("+lineasCredito+")";
		    con.ejecutaSQL(qrySentencia);
			} catch(Exception e) {
        	resultado = false;
      		throw new NafinException("SIST0001");
      }	finally {
      	if(con.hayConexionAbierta())
        	{
					con.terminaTransaccion(resultado);
          con.cierraConexionDB();
          }
      }
    }

	public Vector consultaPyme(String lineas[],int elementos)
  	throws NafinException
    {
    String 		lineasCredito	= "";
    String		qrySentencia	= "";
    ResultSet	rs 						= null;
    AccesoDB	con						= null;
    Vector		vecColumnas		= new Vector();
    Vector		vecFilas			= new Vector();
    int i=0;

    try {
			for(i=0;i<elementos;i++)
				{
				if(!"".equals(lineasCredito))
					lineasCredito += ",";
				lineasCredito += lineas[i];
				}
      con = new AccesoDB();
      con.conexionDB();

      	qrySentencia =	" select LC.ic_linea_credito "+
					",I.cg_razon_social"+
					",LC.fn_monto_autorizado "+
					", to_char(df_autorizacion_nafin,'dd/mm/yyyy') as autorizacion"+
					", to_char(df_vencimiento,'dd/mm/yyyy') as vencimiento"+
					", LC.cg_numero_cuenta NUM_CTA " +
					" ,F.cd_nombre as BANCO_SERVICIO"+
          			" ,to_char(LC.df_limite_disposicion,'dd/mm/yyyy')  as LIMITE" +
					" from com_linea_credito LC,comcat_if I"+
					"	,comcat_financiera F"+
					" where LC.ic_if = I.ic_if"+
					" and LC.ic_linea_credito in ("+lineasCredito+")"+
					" and LC.ic_financiera = F.ic_financiera";
			rs = con.queryDB(qrySentencia);
			i=0;
			while(rs.next())
				{
        vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO")==null)?"-1":rs.getString("IC_LINEA_CREDITO"));
				vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
				vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?"0.00":rs.getString("FN_MONTO_AUTORIZADO"));
				vecColumnas.addElement((rs.getString("AUTORIZACION")==null)?"":rs.getString("AUTORIZACION"));
				vecColumnas.addElement((rs.getString("VENCIMIENTO")==null)?"":rs.getString("VENCIMIENTO"));
        vecColumnas.addElement((rs.getString("NUM_CTA")==null)?"":rs.getString("NUM_CTA"));
        vecColumnas.addElement((rs.getString("BANCO_SERVICIO")==null)?"":rs.getString("BANCO_SERVICIO"));
        vecColumnas.addElement((rs.getString("LIMITE")==null)?"":rs.getString("LIMITE"));
      	vecFilas.addElement(vecColumnas);
		    }
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    } finally {
    	if(con.hayConexionAbierta())
      	con.cierraConexionDB();
    }
  return vecFilas;
	}	//consulta Pyme


	public int existeDocto(String ic_epo, String ic_pyme)
  	throws NafinException
    {
    String		qrySentencia	= "";
    ResultSet	rs 						= null;
    AccesoDB	con						= null;
    int existe = 0;
    try {
      con = new AccesoDB();
      con.conexionDB();
     	qrySentencia =
        "SELECT COUNT (*) "+
        "  FROM comrel_pyme_epo "+
        " WHERE cs_aceptacion IN ('R',  'H') "+
        "   AND ic_epo = "+ic_epo+" "+
        "   AND ic_pyme = "+ic_pyme+" ";
			rs = con.queryDB(qrySentencia);
			if(rs.next())
        existe = rs.getInt(1);
      con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    } finally {
    	if(con.hayConexionAbierta())
      	con.cierraConexionDB();
    }
  return existe;
	}	//Existe Doc


	public int existeDocto(String ic_pyme)
  	throws NafinException
    {
    String		qrySentencia	= "";
    ResultSet	rs 						= null;
    AccesoDB	con						= null;
    int existe = 0;
    try {
      con = new AccesoDB();
      con.conexionDB();
     	qrySentencia =
        "SELECT COUNT (*) "+
        "  FROM com_linea_credito "+
        " WHERE TRUNC (df_vencimiento) >= TRUNC (SYSDATE) "+
        "   AND ic_producto_nafin = 2 "+
        "   AND cs_aceptacion_pyme = 'S' "+
        "   AND ic_pyme = "+ic_pyme+" ";
			rs = con.queryDB(qrySentencia);
			if(rs.next())
        existe = rs.getInt(1);
      con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    } finally {
    	if(con.hayConexionAbierta())
      	con.cierraConexionDB();
    }
  return existe;
	}	//Existe Doc


  public Vector consultaPyme(String ic_pyme,String fecha_sol, String ic_producto_nafin)
  	throws NafinException
  	{
    String		condicion 		= "";
    String		qrySentencia 	= "";
		Vector		vecFilas 			= new Vector();
    Vector		vecColumnas 	= new Vector();
    ResultSet	rs						= null;
		AccesoDB	con 					= null;
			try {
				con = new AccesoDB();
				con.conexionDB();
				if(!"".equals(fecha_sol))
					condicion += " AND to_date(to_char(LC.df_captura,'dd/mm/yyyy'),'dd/mm/yyyy') <= to_date('"+fecha_sol+"','dd/mm/yyyy')";
				else
					condicion += " AND LC.df_captura between add_months(sysdate,-3) and sysdate";

				qrySentencia =  " select decode(LC.cg_tipo_solicitud,'I','Inicial','R','Renovacion','Ampliacion') as TIPO_SOLICITUD"+
										" ,LC.ic_linea_credito,to_char(LC.df_captura,'dd/mm/yyyy') as FECHA_CAPTURA,EL.cd_descripcion as ESTATUS,LC.ic_estatus_linea"+
										" ,LC.fn_monto_autorizado,P.in_plazo_dias as PLAZO,to_char(LC.df_vencimiento,'dd/mm/yyyy') as FECHA_VEN "+
					                    " ,F.cd_nombre as BANCO_SERVICIO, LC.cg_numero_cuenta, LC.cg_cuenta_clabe, LC.cs_aceptacion_pyme,LC.cg_causa_rechazo"+
                    					" ,I.cg_razon_social as NOMBRE_IF,PY.in_numero_sirac, VE.epo_rel, E.cg_razon_social as NOMBRE_EPO "+
										" ,to_char(LC.df_autorizacion_if,'dd/mm/yyyy') as df_autorizacion_if, LC.fn_saldo_linea_total, LC.fn_monto_autorizado_total "+
										" ,VE.ic_if as IC_IF, LC.fn_saldo_linea "+
										" from COM_LINEA_CREDITO LC,COMCAT_ESTATUS_LINEA EL,COMCAT_IF I "+										" ,COMREL_PYME_EPO_X_PRODUCTO PP,COMCAT_FINANCIERA F,COMCAT_PLAZO P,COMCAT_EPO E"+
										" ,COMREL_PRODUCTO_EPO PE,COMREL_IF_EPO_X_PRODUCTO IEP,COMCAT_PYME PY"+
										" , (select count(pexp.ic_epo) as EPO_REL "+
										"		,pexp.ic_pyme "+
										"		, iexp.ic_if  as IC_IF"+
										"		from comrel_pyme_epo_x_producto pexp "+
										"		, comrel_if_epo_x_producto iexp "+
										"		where pexp.ic_producto_nafin =   "+ ic_producto_nafin +
										"		and pexp.cs_habilitado in('S','H') "+
										"		and  pexp.ic_epo = iexp.ic_epo "+
										"		and  iexp.ic_producto_nafin =  "+ ic_producto_nafin +
										"		and iexp.cs_habilitado in('S') "+
										"		and  iexp.ic_producto_nafin = pexp.ic_producto_nafin "+
										"		group by pexp.ic_pyme, iexp.ic_if ) VE"+
                    					" where LC.ic_pyme = "+ic_pyme+
										" and PP.ic_pyme = LC.ic_pyme "+
										" and PP.ic_epo = LC.ic_epo "+
										" and PY.ic_pyme = LC.ic_pyme"+
										" and PP.ic_producto_nafin = "+ ic_producto_nafin +
										" and EL.ic_estatus_linea = LC.ic_estatus_linea "+
					                    " and LC.ic_financiera = F.ic_financiera(+)"+
										" and LC.ic_plazo = P.ic_plazo(+)"+
//									" and LC.cs_aceptacion_pyme='N'"+
										" and LC.ic_epo = E.ic_epo"+
										" and E.cs_habilitado = 'S'"+
										" and E.ic_epo = PE.ic_epo"+
										" and PE.ic_producto_nafin = "+ ic_producto_nafin +
										" and PE.cs_habilitado = 'S'"+
										" and I.cs_habilitado = 'S'"+
										" and I.ic_if = IEP.ic_if"+
                    					" and E.ic_epo = IEP.ic_epo"+
										" and IEP.ic_producto_nafin = "+ ic_producto_nafin +
										" and IEP.cs_habilitado = 'S'"+
					                    " and I.ic_if = LC.ic_if"+
										" and LC.ic_pyme = VE.ic_pyme(+)"+
										" and LC.ic_if = VE.ic_if(+)"+

										condicion+
										" order by LC.ic_linea_credito";
                    rs = con.queryDB(qrySentencia);
				while(rs.next())
					{
          String numeroSirac 		= "";
          String tipoSolicitud	= "";
          String folioSolicitud	= "";
          String estatus 				= "";
          String aceptacion			= "";

					vecColumnas = new Vector();
						folioSolicitud = (rs.getString("IC_LINEA_CREDITO")==null)?"":rs.getString("IC_LINEA_CREDITO");
						tipoSolicitud = (rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD");
					vecColumnas.addElement(folioSolicitud);
          			vecColumnas.addElement(tipoSolicitud);
					vecColumnas.addElement((rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF"));
					vecColumnas.addElement((rs.getString("FECHA_CAPTURA")==null)?"":rs.getString("FECHA_CAPTURA"));
					vecColumnas.addElement((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
						estatus = (rs.getString("IC_ESTATUS_LINEA")==null)?"":rs.getString("IC_ESTATUS_LINEA");
			        vecColumnas.addElement(estatus);
					vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?"":rs.getString("FN_MONTO_AUTORIZADO"));
					vecColumnas.addElement((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
					vecColumnas.addElement((rs.getString("FECHA_VEN")==null)?"":rs.getString("FECHA_VEN"));
					vecColumnas.addElement((rs.getString("BANCO_SERVICIO")==null)?"":rs.getString("BANCO_SERVICIO"));
					vecColumnas.addElement((rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA"));
						aceptacion = (rs.getString("CS_ACEPTACION_PYME")==null)?"":rs.getString("CS_ACEPTACION_PYME");
					vecColumnas.addElement(aceptacion);
					vecColumnas.addElement((rs.getString("CG_CAUSA_RECHAZO")==null)?"":rs.getString("CG_CAUSA_RECHAZO"));
						numeroSirac = (rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
					while(numeroSirac.length()<7)
								numeroSirac = "0"+numeroSirac;
					vecColumnas.addElement(tipoSolicitud.substring(0,1)+numeroSirac+folioSolicitud);
					vecColumnas.addElement((rs.getString("EPO_REL")==null)?"":rs.getString("EPO_REL"));
					vecColumnas.addElement((rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO"));
					vecColumnas.addElement((rs.getString("DF_AUTORIZACION_IF")==null)?"":rs.getString("DF_AUTORIZACION_IF"));
					vecColumnas.addElement((rs.getString("FN_SALDO_LINEA_TOTAL")==null)?"":rs.getString("FN_SALDO_LINEA_TOTAL"));
					vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
					vecColumnas.addElement((rs.getString("IC_IF")==null)?"":rs.getString("IC_IF"));
					vecColumnas.addElement((rs.getString("FN_SALDO_LINEA")==null)?"":rs.getString("FN_SALDO_LINEA"));
					vecColumnas.addElement((rs.getString("CG_CUENTA_CLABE")==null)?"":rs.getString("CG_CUENTA_CLABE"));

          vecFilas.addElement(vecColumnas);
          if("5".equals(estatus)&&"N".equals(aceptacion))
        		pymesAutorizadas++;
		    	}
        }catch(Exception e) {
        	throw new NafinException("SIST0001");
        }finally {
        	if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
	return vecFilas;
	}

    public Vector getEpoRelacionadaPyme(String ic_pyme,String ic_if,String ic_producto_nafin)
    	throws NafinException
    	{
		String		qrySentencia	= "";
      	AccesoDB	con				= null;
		ResultSet	rs				= null;
      	Vector 		vecFilas		= new Vector();
      	Vector		vecColumnas		= null;
				try {
						con = new AccesoDB();
            con.conexionDB();
       			qrySentencia =  " Select distinct CPE.ic_epo, CE.cg_razon_social "+
								" From comrel_pyme_epo CPE, comrel_pyme_epo_x_producto PEXP, "+
								" comcat_epo CE, comrel_if_epo_x_producto IEXP "+
								" Where CPE.ic_pyme= "+ ic_pyme +
								" And IEXP.ic_if ="+ ic_if +
								" And PEXP.ic_producto_nafin = "+ ic_producto_nafin +
								" And PEXP.cs_habilitado in('S','H') "+
								" And CPE.ic_pyme = PEXP.ic_pyme "+
								" And CPE.ic_epo = PEXP.ic_epo "+
								" And CPE.ic_epo = CE.ic_epo "+
								" And PEXP.ic_epo = IEXP.ic_epo "+
								" And IEXP.ic_producto_nafin =  "+ ic_producto_nafin +
								" And IEXP.cs_habilitado in('S') "+
								" And IEXP.ic_producto_nafin = PEXP.ic_producto_nafin ";

						rs = con.queryDB(qrySentencia);
						while(rs.next())
							{
              vecColumnas = new Vector();
							vecColumnas.addElement((rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO"));
							vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
							vecFilas.addElement(vecColumnas);
              }
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return vecFilas;
      } // getEpoRelacionadaPyme

  public int getLineasAutorizadas()
  	{
    return pymesAutorizadas;
    }
  public int getLineasAutorizadas(String ic_pyme)
    throws NafinException
    {
		AccesoDB con 		= null;
    int 		 lineas = 0;
			try {
	      con = new AccesoDB();
 				ResultSet rs = null;
				con.conexionDB();
				String 	qrySentencia =  "select count(*) "+
                      " from COM_LINEA_CREDITO LC,COMCAT_ESTATUS_LINEA EL "+
                      " ,COMREL_PYME_EPO_X_PRODUCTO PP,COMCAT_FINANCIERA F,COMCAT_PLAZO P "+
                      " where LC.ic_pyme = "+ic_pyme+
                      " and PP.ic_pyme = LC.ic_pyme "+
                      " and PP.ic_epo = LC.ic_epo "+
                      " and PP.ic_producto_nafin = 2"+
                      " and EL.ic_estatus_linea = LC.ic_estatus_linea "+
                      " and F.ic_financiera = LC.ic_financiera"+
                      " and P.ic_plazo = LC.ic_plazo"+
                      " and LC.cs_aceptacion_pyme='N'"+
                      " and LC.ic_estatus_linea = 5";
      	rs = con.queryDB(qrySentencia);
        	if(rs.next()){
						lineas = rs.getInt(1);
					}
        con.cierraStatement();
      }catch(Exception e){
      	throw new NafinException("SIST0001");
      }finally{
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
    return lineas;
    }
    
    
public int getLineasAutorizadas2(String ic_pyme, int ic_producto_nafin) throws NafinException {
	System.out.println("LineaCreditoEJB::getLineasAutorizadas2(E)");
	int lineas = 0;
	try {
		lineas = getLineasAutorizadas2(ic_pyme, ic_producto_nafin, "");
	} catch(Exception e) {
		System.out.println("LineaCreditoEJB::getLineasAutorizadas2(Exception) "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		System.out.println("LineaCreditoEJB::getLineasAutorizadas2(S)");
	}
	return lineas;
}


public int getLineasAutorizadas2(String ic_pyme, int ic_producto_nafin, String cg_linea_tipo) throws NafinException {
	System.out.println("LineaCreditoEJB::getLineasAutorizadas2(E)");
	AccesoDB			con 			= null;
	ResultSet			rs				= null;
	String				qrySentencia	= "";
	PreparedStatement 	ps 				= null;
	int 				lineas			= 0;
	try {
		con = new AccesoDB();
		con.conexionDB();
		if("".equals(cg_linea_tipo))
			cg_linea_tipo = "C";
		qrySentencia =  
			" SELECT COUNT (1)"   +
			"   FROM com_linea_credito lc,"   +
			"        comrel_pyme_epo_x_producto pp,"   +
			"        comcat_financiera f,"   +
			"        comcat_plazo p,"   +
			"        comcat_estatus_linea el"   +
			"  WHERE lc.ic_pyme = pp.ic_pyme"   +
			"    AND lc.ic_epo = pp.ic_epo"   +
			"    AND lc.ic_financiera = f.ic_financiera"   +
			"    AND lc.ic_plazo = p.ic_plazo"   +
			"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
			"    AND lc.cs_aceptacion_pyme = 'N'"   +
			"    AND lc.ic_estatus_linea = 5"   +
			"    AND lc.ic_pyme = ?"   +
			"    AND lc.cg_linea_tipo = ?"  +
			"    AND pp.ic_producto_nafin = ?";
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1, Integer.parseInt(ic_pyme));
		ps.setString(2, cg_linea_tipo);
		ps.setInt(3, ic_producto_nafin);
		rs = ps.executeQuery();
		ps.clearParameters();
		if(rs.next()) {
			lineas = rs.getInt(1);
		}
		rs.close(); ps.close();
	} catch(Exception e) {
		System.out.println("LineaCreditoEJB::getLineasAutorizadas2(Exception) "+e);
		e.printStackTrace();
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
		System.out.println("LineaCreditoEJB::getLineasAutorizadas2(S)");
	}
	return lineas;
}
/*
public List getDetLiberacion	(String ic_linea_credito) throws NafinException {
	String		linea_credito  	  = "";
	String		cg_razon_social   = "";
	String		ic_liberacion     = "";
	String		df_liberacion     = "";
	String		fg_monto_liberar  = "";
	String		df_lim_liberacion = "";
	String		ic_estatus_lib	  = "";
	String		cs_aplicada		  = "";
	String		qrySentencia	  = "";
	ResultSet	rs				  = null;
	AccesoDB 	con				  = null;
	List		columnas		  = null;
	List		renglones		  = new ArrayList();
	try {
		con = new AccesoDB();
		con.conexionDB();				
		qrySentencia = 	
			" SELECT lb.ic_linea_credito, lb.ic_liberacion,"   +
			"        TO_CHAR(lb.df_liberacion , 'dd/mm/yyyy')df_liberacion, lb.fg_monto_liberar, TO_CHAR (lb.df_lim_liberacion, 'dd/mm/yyyy')df_lim_liberacion,"   +
			"        lb.ic_estatus_liberacion, lb.cs_aplicada"   +
			"   FROM com_liberacion lb,com_linea_credito lc"   +
			"  WHERE lc.ic_linea_credito = lb.ic_linea_credito"   +
			"    AND lc.ic_linea_credito = "+ic_linea_credito +
			"  Order by lb.ic_liberacion	"; 
		rs = con.queryDB(qrySentencia);
        System.out.println ("El query del detalle de lineas" + qrySentencia);
		while(rs.next()) {
			linea_credito 	 = rs.getString("IC_LINEA_CREDITO"); 
			ic_liberacion 	 = rs.getString("IC_LIBERACION");	 
			df_liberacion 	 = rs.getString("df_liberacion");	 
			fg_monto_liberar = rs.getString("FG_MONTO_LIBERAR"); 
			df_lim_liberacion= rs.getString("df_lim_liberacion");
			ic_estatus_lib	 = rs.getString("IC_ESTATUS_LIBERACION"); 
			cs_aplicada		 = rs.getString("CS_APLICADA"); 
			columnas = new ArrayList();
				columnas.add(linea_credito);
				columnas.add(cg_razon_social);
				columnas.add(ic_liberacion);
				columnas.add(df_liberacion);
				columnas.add(fg_monto_liberar);
				columnas.add(df_lim_liberacion);
				columnas.add(ic_estatus_lib);
				columnas.add(cs_aplicada);
			renglones.add(columnas);
		}
		con.cierraStatement();
  }catch(Exception e){
  		throw new NafinException("SIST0001");
  } finally {
  		if(con.hayConexionAbierta())
    	con.cierraConexionDB();
  }
	return renglones;
}
*/



  private void getDatosComunes(String ic_if,String ic_epo,AccesoDB con)
  	throws NafinException
  	{
		String		qrySentencia	= "";
		ResultSet	rs						= null;
    String		consecutivo		= "";
    	try {
      	consecutivo = getConsecutivo();
    		qrySentencia =	"select sum(fn_monto_autorizado_total)"+
							" from com_linea_credito"+
							" where cg_tipo_solicitud <> 'A'"+
							" and df_vencimiento_adicional > sysdate"+
							" and ic_if not in("+ic_if+")";
							if(!"".equals(ic_epo))
								qrySentencia += " and ic_epo = "+ic_epo;
							else if(!"".equals(consecutivo))
								qrySentencia += " and ic_linea_credito ="+consecutivo;

					rs = con.queryDB(qrySentencia);
					if(rs.next())
						lineasIF = rs.getString(1);
					if("".equals(lineasIF)||lineasIF==null)
						lineasIF = "0.00";
					con.cierraStatement();
				//OBTENEMOS LAS LINEAS INDIVIDUAL Y AUTOMATICA
				qrySentencia = 	"select fn_linea_automatica,cs_linea_individual,NVL(fn_maximo_linea,0) as fn_maximo_linea "+
							" from comcat_producto_nafin " +
							" where ic_producto_nafin = 2";
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					{
					defLineaAut		= (rs.getString("FN_LINEA_AUTOMATICA")==null)?"0":rs.getString("FN_LINEA_AUTOMATICA");
					defLineaInd		= (rs.getString("CS_LINEA_INDIVIDUAL")==null)?"N":rs.getString("CS_LINEA_INDIVIDUAL");
					defLineaMax		= (rs.getString("FN_MAXIMO_LINEA")==null)?"0":rs.getString("FN_MAXIMO_LINEA");
					}
			con.cierraStatement();
      } catch(Exception e) {
      	throw new NafinException("SIST0001");
      }
    }

	public Combos getEstatus(String tipoUsuario)
  	throws NafinException
		{
		System.out.println("LineaCredito::getEstatus (tipoUsuario) Deprecated usar:getVecEstatus(tipoUsuario)");
    // tipoUsuario: Posibles valores = "I" para IF y "N" para Nafin
    AccesoDB con = null;
    Combos comboEstatus = null;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
	    	String qrySentencia = "SELECT IC_ESTATUS_LINEA,CD_DESCRIPCION"+
													" FROM COMCAT_ESTATUS_LINEA";
        if("I".equals(tipoUsuario))
        	qrySentencia += " WHERE IC_ESTATUS_LINEA IN(2,3,4)";
        else if("IBC".equals(tipoUsuario)) // Obtiene el estatus validos para IF
        	qrySentencia = "SELECT IC_ESTATUS_LINEA,DECODE(ic_estatus_linea,3,'Desbloqueada',5,'Desbloqueada', cd_descripcion)"+
							" FROM COMCAT_ESTATUS_LINEA" +
							" WHERE IC_ESTATUS_LINEA IN(3,8)";
        else if("NBC".equals(tipoUsuario)) // Obtiene el estatus validos para NAFIN
        	qrySentencia = "SELECT IC_ESTATUS_LINEA,DECODE(ic_estatus_linea,3,'Desbloqueada',5,'Desbloqueada', cd_descripcion)"+
							" FROM COMCAT_ESTATUS_LINEA" +
        					" WHERE IC_ESTATUS_LINEA IN(5,8)";
        else if("BC".equals(tipoUsuario)) // Obtiene el estatus para bloquear y cancelar l�neas para IF
        	qrySentencia += " WHERE IC_ESTATUS_LINEA IN(7,8)";
        else if("BCN".equals(tipoUsuario)) // Obtiene el estatus para bloquear y cancelar l�neas para Nafin
        	qrySentencia += " WHERE IC_ESTATUS_LINEA IN(9,10)";
        else
          qrySentencia += " WHERE IC_ESTATUS_LINEA IN(5,6)";
//System.out.println(qrySentencia);
				comboEstatus = new Combos(qrySentencia,con);
      }	catch(Exception e){
      	throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return comboEstatus;
    }

/**************/
  	public Vector getVecEstatus(String tipoUsuario)
  	throws NafinException{
		System.out.println("LineaCredito::getVecEstatus (tipoUsuario)");
	    AccesoDB 	con = null;
	    Vector 		vecFilas = new Vector();
		Vector 		vecColumnas = null;
	    String    	qrySentencia = "";
		ResultSet	rs = null;
    	try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia = "SELECT IC_ESTATUS_LINEA,CD_DESCRIPCION "+
			" FROM COMCAT_ESTATUS_LINEA ";
			if("I".equals(tipoUsuario))
				qrySentencia += " WHERE IC_ESTATUS_LINEA IN(2,3,4)";
			else if("IBC".equals(tipoUsuario)) // Obtiene el estatus validos para IF
				qrySentencia = "SELECT IC_ESTATUS_LINEA,DECODE(ic_estatus_linea,3,'Desbloqueada',5,'Desbloqueada', cd_descripcion) as CD_DESCRIPCION"+
					" FROM COMCAT_ESTATUS_LINEA" +
					" WHERE IC_ESTATUS_LINEA IN(3,8)";
			else if("NBC".equals(tipoUsuario)) // Obtiene el estatus validos para NAFIN
				qrySentencia = "SELECT IC_ESTATUS_LINEA,DECODE(ic_estatus_linea,3,'Desbloqueada',5,'Desbloqueada', cd_descripcion) AS CD_DESCRIPCION"+
					" FROM COMCAT_ESTATUS_LINEA" +
					" WHERE IC_ESTATUS_LINEA IN (5,10)";
			else if("BC".equals(tipoUsuario)) // Obtiene el estatus para bloquear y cancelar l�neas para IF
				qrySentencia += " WHERE IC_ESTATUS_LINEA IN(7,8)";
			else if("BCN".equals(tipoUsuario)) // Obtiene el estatus para bloquear y cancelar l�neas para Nafin
				qrySentencia += " WHERE IC_ESTATUS_LINEA IN(9,10)";
			else
				qrySentencia += " WHERE IC_ESTATUS_LINEA IN(12,6)";
			//System.out.println(qrySentencia);
	    	/*qrySentencia =
	    		"SELECT IC_ESTATUS_LINEA,CD_DESCRIPCION"+
				" FROM COMCAT_ESTATUS_LINEA";
        	if("I".equals(tipoUsuario))
        		qrySentencia += " WHERE IC_ESTATUS_LINEA IN(2,3,4)";
        	else
          		qrySentencia += " WHERE IC_ESTATUS_LINEA IN(5,6)";*/
			rs = con.queryDB(qrySentencia);
			while(rs.next()){
			  	vecColumnas = new Vector();
				vecColumnas.add((rs.getString("IC_ESTATUS_LINEA")==null)?"":rs.getString("IC_ESTATUS_LINEA"));
				vecColumnas.add((rs.getString("CD_DESCRIPCION")==null)?"":rs.getString("CD_DESCRIPCION"));
				vecFilas.add(vecColumnas);
			}
			con.cierraStatement();
      }	catch(Exception e){
		System.out.println("LineaCredito::getVecEstatus Exception "+e);
      	throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
		System.out.println("LineaCredito::getVecEstatus (tipoUsuario)");
      }
	return vecFilas;
    }
/**************/

	public String getEstatus(String tipoUsuario,String selected) //Deprecated: usar getVecEstatus
  	throws NafinException{
    // tipoUsuario: Posibles valores = "I" para IF y "N" para Nafin
    AccesoDB con = null;
    Combos comboEstatus = null;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
	    	String qrySentencia = "SELECT IC_ESTATUS_LINEA,CD_DESCRIPCION"+
													" FROM COMCAT_ESTATUS_LINEA";
        if("I".equals(tipoUsuario))
        	qrySentencia += " WHERE IC_ESTATUS_LINEA IN(2,3,4)";
        else
          qrySentencia += " WHERE IC_ESTATUS_LINEA IN(5,6)";
//System.out.println(qrySentencia);
				comboEstatus = new Combos(qrySentencia,con);
      }	catch(Exception e){
      	throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return comboEstatus.getCombo(selected);
    }


	public Combos getBancos()	//Deprecated: usar getVecBancos
  	throws NafinException{
		System.out.println("LineaCredito::getBancos (E) Deprecated usar:getVecBancos");
		AccesoDB 	con = null;
		Combos 		comboBancos = null;
		String    qrySentencia = "";
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =	" SELECT IC_FINANCIERA,CD_NOMBRE FROM COMCAT_FINANCIERA WHERE IC_TIPO_FINANCIERA = 1";
			comboBancos = new Combos(qrySentencia,con);
		}	catch(Exception e){
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
			con.cierraConexionDB();
			System.out.println("LineaCredito::getBancos (S) Deprecated usar:getVecBancos");
		}
		return comboBancos;
    }

  	public Vector getVecBancos()
  	throws NafinException{
		System.out.println("LineaCredito::getVecBancos (E)");
	    AccesoDB 	con = null;
	    Vector 		vecFilas = new Vector();
		Vector 		vecColumnas = null;
	    String    	qrySentencia = "";
		ResultSet	rs = null;

    	try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =	" SELECT IC_FINANCIERA,CD_NOMBRE FROM COMCAT_FINANCIERA WHERE IC_TIPO_FINANCIERA = 1";
			rs = con.queryDB(qrySentencia);
			while(rs.next()){
			  	vecColumnas = new Vector();
				vecColumnas.add((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
				vecColumnas.add((rs.getString("CD_NOMBRE")==null)?"":rs.getString("CD_NOMBRE"));
				vecFilas.add(vecColumnas);
			}
			con.cierraStatement();
      }	catch(Exception e){
		System.out.println("LineaCredito::getVecBancos Exception "+e);
      	throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
		System.out.println("LineaCredito::getVecBancos (S)");
      }
	return vecFilas;
    }


  	public Combos getPlazos()		//DEPRECATED USAR getVecPlazos
  	throws NafinException{
	System.out.println("LineaCredito::getPlazos (E) Deprecated");
    AccesoDB 	con = null;
    Combos 		comboPlazos = null;
    String    qrySentencia = "";
    	try {
      		con = new AccesoDB();
          con.conexionDB();
          qrySentencia =	" SELECT IC_PLAZO,IN_PLAZO_DIAS FROM COMCAT_PLAZO WHERE ic_plazo in (11)";
//System.out.println(qrySentencia);
					comboPlazos = new Combos(qrySentencia,con);
      }	catch(Exception e){
      	throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
		System.out.println("LineaCredito::getPlazos (E) Deprecated");
      }
		return comboPlazos;
    }


  	public Vector getVecPlazos()
  	throws NafinException{
		System.out.println("LineaCredito::getVecPlazos (E)");
	    AccesoDB 	con = null;
	    Vector 		vecFilas = new Vector();
		Vector 		vecColumnas = null;
	    String    	qrySentencia = "";
		ResultSet	rs = null;

    	try {
      	  con = new AccesoDB();
          con.conexionDB();
          qrySentencia =	" SELECT IC_PLAZO,IN_PLAZO_DIAS FROM COMCAT_PLAZO WHERE ic_plazo in (11)";
		  rs = con.queryDB(qrySentencia);
		  while(rs.next()){
		  	vecColumnas = new Vector();
			vecColumnas.add((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
			vecColumnas.add((rs.getString("IN_PLAZO_DIAS")==null)?"":rs.getString("IN_PLAZO_DIAS"));
			vecFilas.add(vecColumnas);
		  }
		  con.cierraStatement();
      }	catch(Exception e){
		System.out.println("LineaCredito::getVecPlazos Exception "+e);
      	throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
		System.out.println("LineaCredito::getVecPlazos (S)");
      }
	return vecFilas;
    }


	
  public Vector consultaIF(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin, String cc_acuse)
  	throws NafinException{
		try{
			return consultaIF(ic_epo,ic_if,ic_pyme,ic_linea,ic_producto_nafin,cc_acuse,"");
		}catch(NafinException ne){
			throw ne;
		}
	}

  public Vector consultaIF(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin)
  	throws NafinException{
		try{
			return consultaIF(ic_epo,ic_if,ic_pyme,ic_linea,ic_producto_nafin,"");
		}catch(NafinException ne){
			throw ne;
		}
	}

//Modificacion:27/02/2003 Arlette Espinosa
//Modificacion:17/11/2003 Patricia Camarillo
//Modificacion:04/10/2004 Ricardo Fuentes
	public Vector consultaIF( String ic_epo, String ic_if, String ic_pyme, String ic_linea,  String ic_producto_nafin, String cc_acuse, String ic_modalidad) throws NafinException {
		String		condicion 				= "";
		String		qrySentencia 			= "";
		Vector		vecFilas 				= new Vector();
		Vector		vecColumnas 			= new Vector();
		ResultSet	rs						= null;
		AccesoDB	con 					= null;
		String 		conTipoSolicitud		= "";
		String 		conNumeroSirac			= "";
		String		conConsecutivo			= "";
		String		strAux					= "";
		String     tipo_liq = "";
		try {
			con = new AccesoDB();
			con.conexionDB();

			if("4".equals(ic_producto_nafin)) {
				qrySentencia =
					"SELECT CG_TIPO_LIQUIDACION"+
					" FROM COMREL_PRODUCTO_IF"+
					" WHERE IC_IF = "+ic_if+" "+
					" AND IC_PRODUCTO_NAFIN=4";
				System.out.println(qrySentencia);
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					tipo_liq = (rs.getString(1)==null)?"":rs.getString(1);
				rs.close();con.cierraStatement();
			}
		
			getDatosComunes(ic_if,ic_epo,con);
			
			if(!"".equals(ic_linea)) {
				if(!"4".equals(ic_producto_nafin)) {
					setFolioArmado(ic_linea);
					conTipoSolicitud = getTipoSolicitud();
					conNumeroSirac   = getNumeroSirac();
					conConsecutivo   = getConsecutivo().trim();
					condicion +=	" AND LC.ic_linea_credito = "+conConsecutivo+
									" AND LC.cg_tipo_solicitud = '"+conTipoSolicitud+"'"+
									" AND PY.in_numero_sirac = "+conNumeroSirac;
				}
				else {
					condicion +=	" AND LC.ic_linea_credito = "+ic_linea;
				}
			}
			
			if(!"".equals(ic_epo)){
				condicion += " AND LC.ic_epo = "+ ic_epo;
				if(!"TODOS".equals(ic_pyme)&&!"".equals(ic_pyme))
					condicion += " AND LC.ic_pyme = "+ic_pyme;
			}
			if(!"".equals(cc_acuse)&&cc_acuse!=null){
				condicion += " AND LC.cc_acuse = '"+cc_acuse+"'";
			
			}else{
				condicion += " and LC.ic_estatus_linea in(1,2)";
			}
		
			if("ANTIC".equals(ic_modalidad))
				condicion += "and PRE.ic_modalidad = 1";
			else if("PRELI".equals(ic_modalidad))
				condicion += "and PRE.ic_modalidad = 2";
			
			condicion += " Order by PRODUCTO_NAFIN";

			qrySentencia =	
				" select LC.ic_linea_credito, PE.cg_pyme_epo_interno,PY.cg_razon_social, ES.cd_nombre as ESTADO "+
				" ,D.cg_calle||' '||decode(D.cg_numero_ext,null,'',D.cg_numero_ext)||' '||decode(D.cg_numero_ext,null,'',D.cg_numero_ext) as DIRECCION "+
				" ,D.cg_telefono1, decode(LC.cg_tipo_solicitud,'I','Inicial','R','Renovaci&oacute;n','A','Ampliaci&oacute;n') as TIPO_SOLICITUD "+
				" ,to_char(LC.df_captura,'dd/mm/yyyy') as FECHA_SOLICITUD, EL.cd_descripcion as ESTATUS"+
				" ,PRE.fn_linea_automatica,LC.ic_estatus_linea,decode(LC.df_vencimiento,null,to_char(sysdate,'dd/mm/yyyy'),to_char(LC.df_vencimiento,'dd/mm/yyyy')) as FECHA_HOY"+
				" ,NVL(PRE.cs_linea_individual,'"+defLineaInd+"') as cs_linea_individual,LC.cg_tipo_solicitud"+
				" ,LC.fn_monto_solicitado,LC.ic_plazo,LC.ic_financiera ";
				
            if("I".equals(tipo_liq))
            	qrySentencia += " ,LC.cg_numero_cuenta_if as CUENTA";
            else {
				if("A".equals(tipo_liq))
					qrySentencia += " ,LC.cg_numero_cuenta as CUENTA, LC.cg_cuenta_clabe as CUENTA_CLABE ";
				else
					qrySentencia += " ,LC.cg_numero_cuenta as CUENTA, LC.cg_cuenta_clabe as CUENTA_CLABE ";
            }
			qrySentencia +=
				" ,LC.fn_linea_opcional,LC.fn_linea_individual,LC.fn_monto_autorizado"+
				" ,PY.in_numero_sirac,VE.epo_rel,LC.ic_pyme,LC.ic_epo"+
				" ,(select sum(fn_monto_autorizado_total)"+
				" 		from com_linea_credito lc2"+
				" 		where cg_tipo_solicitud <> 'A'"+
				" 		and df_vencimiento_adicional > sysdate"+
				" 		and ic_if not in("+ic_if+") and lc.ic_pyme = lc2.ic_pyme) as lineas_otros"+
				" , to_char(sysdate,'dd/mm/yyyy') as FECHA_HOY, CPN.ic_nombre as PRODUCTO_NAFIN, CM.ic_moneda as MONEDA"+
				" , CPN.ic_producto_nafin as PRODUCTO_NAFIN_CVE, to_char(LC.df_captura,'DD-MON-YYYY') as FECHA_SOLICITUD_INV "+
				" ,lc.ic_linea_credito_padre, CP.in_plazo_dias, LC.cg_cuenta_clabe, CM.cd_nombre as NOMBRE_MONEDA "+
				" ,LC.cg_cuenta_clabe,CPN.fn_minimo_financiamiento "+
				" ,to_char(lc.df_vencimiento_adicional,'dd/mm/yyyy') as df_vencimiento "+
				" ,LC.ig_numero_solic_troya SOLICT , PY.IN_NUMERO_TROYA CLIENTET "+
//				", to_char(nvl(LC.DF_AUTORIZACION_IF,sysdate),'dd/mm/yyyy') DF_AUTORIZACION_IF "+
				", to_char(LC.DF_AUTORIZACION_IF,'dd/mm/yyyy') DF_AUTORIZACION_IF "+
				", to_char(LC.DF_AUTORIZACION,'dd/mm/yyyy') DF_AUTORIZACION "+
				" , E.cg_razon_social as nombre_epo"+
				" , fin.cd_nombre as nombre_banco"+
				" , lc.cg_causa_rechazo"+
				" , to_char(lc.df_limite_disposicion,'dd/mm/yyyy')as fecha_limite"+
				" , decode(LC.cg_linea_tipo, 'E', 'Credito de Exportacion','Tradicional') linea_tipo "+
				" , tam.cd_descripcion tabla_amort "+
				" from COM_LINEA_CREDITO LC, COMREL_PYME_EPO PE,COMCAT_PYME PY,COM_DOMICILIO D,COMCAT_ESTATUS_LINEA EL "+
				" ,COMREL_PRODUCTO_EPO PRE,COMREL_PYME_EPO_X_PRODUCTO PP,COMCAT_EPO E,COMCAT_ESTADO ES, DUAL "+
				" , (select count(pexp.ic_epo) as EPO_REL "+
				"		, pexp.ic_pyme "+
				"		, iexp.ic_if  "+
				"		, pexp.ic_producto_nafin AS PROD_REL"+
				"		from comrel_pyme_epo_x_producto pexp "+
				"		, comrel_if_epo_x_producto iexp "+
				"		where pexp.ic_producto_nafin in  ("+ ic_producto_nafin +")"+
				"		and pexp.cs_habilitado in('S','H') "+
				"		and  pexp.ic_epo = iexp.ic_epo "+
				"		and  iexp.ic_producto_nafin in ("+ ic_producto_nafin +")"+
				"		and iexp.cs_habilitado in('S') "+
				"		and  iexp.ic_producto_nafin = pexp.ic_producto_nafin ";
			if(!"".equals(ic_pyme)&&!"TODOS".equals(ic_pyme))
				qrySentencia += " 		AND pexp.ic_pyme = "+ic_pyme;
			qrySentencia +=
				"		group by pexp.ic_pyme, iexp.ic_if, pexp.ic_producto_nafin ) VE"+
				" ,COMCAT_PRODUCTO_NAFIN CPN, COMCAT_MONEDA CM, COMCAT_PLAZO CP, COMCAT_FINANCIERA FIN "+
				" ,comcat_tabla_amort tam "+
				" where PY.ic_pyme = PE.ic_pyme "+
				" and PY.ic_pyme = D.ic_pyme "+
				" and PE.ic_pyme = PP.ic_pyme "+
				" and PE.ic_epo = PP.ic_epo "+
				" and PP.ic_pyme = LC.ic_pyme "+
				" and PP.ic_epo = LC.ic_epo "+
				" and PE.cs_habilitado = 'S'"+
				" and PP.cs_habilitado in('N','S','H')"+
//				" and PP.ic_producto_nafin = "+ ic_producto_nafin +
				" and PE.ic_epo = E.ic_epo"+
				" and E.cs_habilitado = 'S'"+
				" and E.ic_epo = PRE.ic_epo"+
//				" and PRE.ic_producto_nafin = "+ ic_producto_nafin +
				" and PRE.cs_habilitado = 'S'"+
				" and EL.ic_estatus_linea = LC.ic_estatus_linea "+
				" and D.ic_estado = ES.ic_estado"+
				" and LC.ic_if ="+ic_if+
				" and LC.ic_pyme = VE.ic_pyme(+)"+
				" and LC.ic_if = VE.ic_if(+)"+
				" and PRE.ic_producto_nafin = PP.ic_producto_nafin  "+
				" and LC.ic_producto_nafin IN ("+ic_producto_nafin+")  "+
				" and LC.ic_producto_nafin = PP.ic_producto_nafin  "+
				" and LC.ic_producto_nafin = VE.prod_rel(+) "+
				" and PP.ic_producto_nafin = CPN.ic_producto_nafin "+
				" and CM.ic_moneda = LC.ic_moneda"+
				" and LC.ic_financiera = fin.ic_financiera(+)"+
				" and LC.ic_plazo = CP.ic_plazo(+) "+
				" and d.cs_fiscal = 'S'"+
				" AND LC.ic_tabla_amort = tam.ic_tabla_amort "+
				condicion;
			System.out.println("consultaIF{");
			System.out.println(qrySentencia);
			System.out.println("**********************************************");
			rs = con.queryDB(qrySentencia);
			while(rs.next()) {
					vecColumnas = new Vector();
/*0*/			vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO")==null)?"":rs.getString("IC_LINEA_CREDITO"));
/*1*/			vecColumnas.addElement((rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO"));
/*2*/			vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
/*3*/			vecColumnas.addElement((rs.getString("ESTADO")==null)?"":rs.getString("ESTADO"));
/*4*/			vecColumnas.addElement((rs.getString("DIRECCION")==null)?"":rs.getString("DIRECCION"));
/*5*/			vecColumnas.addElement((rs.getString("CG_TELEFONO1")==null)?"":rs.getString("CG_TELEFONO1"));
/*6*/			vecColumnas.addElement((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*7*/			vecColumnas.addElement((rs.getString("FECHA_SOLICITUD")==null)?"":rs.getString("FECHA_SOLICITUD"));
/*8*/			vecColumnas.addElement((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*9*/			vecColumnas.addElement((rs.getString("FECHA_HOY")==null)?"":rs.getString("FECHA_HOY"));
/*10*/			vecColumnas.addElement((rs.getString("FN_LINEA_AUTOMATICA")==null||"0".equals(rs.getString("FN_LINEA_AUTOMATICA")))?defLineaAut:rs.getString("FN_LINEA_AUTOMATICA"));
/*11*/			vecColumnas.addElement((rs.getString("IC_ESTATUS_LINEA")==null)?"":rs.getString("IC_ESTATUS_LINEA"));
/*12*/			vecColumnas.addElement((rs.getString("CG_TIPO_SOLICITUD")==null)?"":rs.getString("CG_TIPO_SOLICITUD"));
/*13*/			vecColumnas.addElement((rs.getString("FN_MONTO_SOLICITADO")==null)?"0.00":rs.getString("FN_MONTO_SOLICITADO"));
/*14*/			vecColumnas.addElement((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
/*15*/			vecColumnas.addElement((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
/*16*/			vecColumnas.addElement((rs.getString("CUENTA")==null)?"":rs.getString("CUENTA"));
/*17*/			vecColumnas.addElement((rs.getString("FN_LINEA_OPCIONAL")==null)?"0.00":rs.getString("FN_LINEA_OPCIONAL"));
/*18*/			vecColumnas.addElement((rs.getString("FN_LINEA_INDIVIDUAL")==null)?"0.00":rs.getString("FN_LINEA_INDIVIDUAL"));
/*19*/			vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?(String)vecColumnas.get(10):rs.getString("FN_MONTO_AUTORIZADO"));
/*20*/			vecColumnas.addElement((rs.getString("EPO_REL")==null)?"0":rs.getString("EPO_REL"));
/*21*/			vecColumnas.addElement((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*22*/			vecColumnas.addElement((rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO"));
				if(!"4".equals(ic_producto_nafin)){
					strAux = (rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
			       	while(strAux.length()<7)
								strAux = "0"+strAux;
			        strAux = (String)vecColumnas.get(12) +strAux +	(String)vecColumnas.get(0);
				} else
					strAux = (String)vecColumnas.get(0);

/*23*/			vecColumnas.addElement(strAux);     		//folioArmado
/*24*/			vecColumnas.addElement((rs.getString("CS_LINEA_INDIVIDUAL")==null)?"":rs.getString("CS_LINEA_INDIVIDUAL"));
/*25*/			vecColumnas.addElement(defLineaAut);
/*26*/ 			vecColumnas.addElement(defLineaInd);
/*27*/ 			vecColumnas.addElement(defLineaMax);
/*28*/			vecColumnas.addElement((rs.getString("LINEAS_OTROS")==null)?"0.00":rs.getString("LINEAS_OTROS"));
/*29*/			vecColumnas.addElement((rs.getString("FECHA_HOY")==null)?"":rs.getString("FECHA_HOY"));
/*30*/			vecColumnas.addElement((rs.getString("PRODUCTO_NAFIN")==null)?"":rs.getString("PRODUCTO_NAFIN"));
/*31*/			vecColumnas.addElement((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*32*/			vecColumnas.addElement((rs.getString("PRODUCTO_NAFIN_CVE")==null)?"":rs.getString("PRODUCTO_NAFIN_CVE"));
/*33*/			vecColumnas.addElement((rs.getString("FECHA_SOLICITUD_INV")==null)?"":rs.getString("FECHA_SOLICITUD_INV"));
/*34*/			vecColumnas.addElement((rs.getString("ic_linea_credito_padre")==null)?"":rs.getString("ic_linea_credito_padre"));
/*35*/			vecColumnas.addElement((rs.getString("IN_PLAZO_DIAS")==null)?"":rs.getString("IN_PLAZO_DIAS"));
/*36*/			vecColumnas.addElement((rs.getString("CG_CUENTA_CLABE")==null)?"":rs.getString("CG_CUENTA_CLABE"));
/*37*/			vecColumnas.addElement((rs.getString("NOMBRE_MONEDA")==null)?"":rs.getString("NOMBRE_MONEDA"));
/*38*/			vecColumnas.addElement((rs.getString("CG_CUENTA_CLABE")==null)?"":rs.getString("CG_CUENTA_CLABE"));
/*39*/			vecColumnas.addElement((rs.getString("FN_MINIMO_FINANCIAMIENTO")==null)?"":rs.getString("FN_MINIMO_FINANCIAMIENTO"));
/*40*/			vecColumnas.addElement((rs.getString("DF_VENCIMIENTO")==null)?"":rs.getString("DF_VENCIMIENTO"));
/*41*/			vecColumnas.addElement((rs.getString("CLIENTET")==null)?"":rs.getString("CLIENTET"));
/*42*/			vecColumnas.addElement((rs.getString("SOLICT")==null)?"":rs.getString("SOLICT"));
/*43*/			vecColumnas.addElement((rs.getString("DF_AUTORIZACION_IF")==null)?"":rs.getString("DF_AUTORIZACION_IF"));
/*44*/			vecColumnas.addElement((rs.getString("DF_AUTORIZACION")==null)?"":rs.getString("DF_AUTORIZACION"));
/*45*/			vecColumnas.addElement((rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO"));
/*46*/			vecColumnas.addElement((rs.getString("NOMBRE_BANCO")==null)?"":rs.getString("NOMBRE_BANCO"));
/*47*/			vecColumnas.addElement((rs.getString("CG_CAUSA_RECHAZO")==null)?"":rs.getString("CG_CAUSA_RECHAZO"));
/*48*/			vecColumnas.addElement((rs.getString("FECHA_LIMITE")==null)?"":rs.getString("FECHA_LIMITE"));
/*49*/			vecColumnas.addElement((rs.getString("linea_tipo")==null)?"":rs.getString("linea_tipo"));
/*50*/			vecColumnas.addElement((rs.getString("tabla_amort")==null)?"":rs.getString("tabla_amort"));
				vecFilas.addElement(vecColumnas);
		   	}
        } catch(Exception e) {
          System.out.println("Consulta IF Exception "+e.getMessage());
        	throw new NafinException("SIST0001");
        } finally {
        	if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
		return vecFilas;
	}

public void AutorizaNafin(String folio [],String estatus [],String [] huboCambio,String sElementos)
		throws NafinException {
    int i=0;
    String 		qrySentencia	= "";
    ResultSet	rs						= null;
    AccesoDB	con						= null;
    String 		lineaPadre		= "-1";
    String 		montoTotal		= "0.00";
    String 		saldoTotal		= "0.00";
    String 		fechaVen  		= "";
    String 		tipoSolicitud = "";
    float  		hayCambio 		= 0.0f;
    int			iElementos		= Integer.parseInt(sElementos);
    boolean		resultado 		= true;
	String		lsPyme			= "";
	String		lsEpo			= "";
	String		lsProducto		= "";
	String		lsTipoPiso		= "";
	String		lsFechaLimite	= "";	

    try	{
      con = new AccesoDB();
      con.conexionDB();
		for (i=0;i<iElementos&&resultado;i++) {
			if(huboCambio[i].equals("S")) {
				qrySentencia =  
					" update com_linea_credito "+
					" set ic_estatus_linea = "+estatus[i]+
					" ,df_autorizacion_nafin = sysdate"+
					" where ic_linea_credito = "+folio[i];
				con.ejecutaSQL(qrySentencia);
				
				if(estatus[i].equals("5")) {        //si es autorizada, sumar los saldos,montos y fechas
					qrySentencia =  
						" SELECT lc.ic_linea_credito_padre, lc.fn_monto_autorizado,"   +
						"        lc.fn_saldo_linea, TO_CHAR (lc.df_vencimiento, 'dd/mm/yyyy'),"   +
						"        lc.cg_tipo_solicitud, lc.ic_pyme, lc.ic_epo, lc.ic_producto_nafin,"   +
						"        i.ig_tipo_piso, TO_CHAR (df_limite_disposicion, 'dd/mm/yyyy')"   +
						"   FROM com_linea_credito lc, comcat_if i"   +
						"  WHERE i.ic_if(+) = lc.ic_if AND ic_linea_credito = "+folio[i];
					rs = con.queryDB(qrySentencia);
					System.out.println(qrySentencia);
					if(rs.next()) {
				        lineaPadre  	= (rs.getString( 1)==null)?"":rs.getString( 1);
				        montoTotal  	= (rs.getString( 2)==null)?"":rs.getString( 2);
				        saldoTotal  	= (rs.getString( 3)==null)?"":rs.getString( 3);
				        fechaVen    	= (rs.getString( 4)==null)?"":rs.getString( 4);
				        tipoSolicitud 	= (rs.getString( 5)==null)?"":rs.getString( 5);
						lsPyme			= (rs.getString( 6)==null)?"":rs.getString( 6);
						lsEpo			= (rs.getString( 7)==null)?"":rs.getString( 7);
						lsProducto		= (rs.getString( 8)==null)?"":rs.getString( 8);
						lsTipoPiso		= (rs.getString( 9)==null)?"":rs.getString( 9);
						lsFechaLimite	= (rs.getString(10)==null)?"":rs.getString(10);
				    }
					con.cierraStatement();
					if("A".equals(tipoSolicitud)) {
						qrySentencia =  
							"select to_date('"+fechaVen+"','dd/mm/yyyy')-df_vencimiento_adicional"+
							" from com_linea_credito where ic_linea_credito = "+lineaPadre;
						rs = con.queryDB(qrySentencia);
						if(rs.next())
							hayCambio = rs.getFloat(1);
						con.cierraStatement();
						qrySentencia =  
							" update com_linea_credito set fn_monto_autorizado_total = fn_monto_autorizado_total +"+montoTotal+
							" ,fn_saldo_linea_total = fn_saldo_linea_total +"+saldoTotal;
						if(hayCambio>0)
							qrySentencia += " ,df_vencimiento_adicional = to_date('"+fechaVen+"','dd/mm/yyyy')";
							
						if("2".equals(lsProducto)) {
							qrySentencia += 							
								" ,df_limite_disposicion = to_date('"+lsFechaLimite+"','dd/mm/yyyy')";
						}
						qrySentencia += " where ic_linea_credito = "+lineaPadre;
							con.ejecutaSQL(qrySentencia);
					} //fin if "A".equals
					if(lsTipoPiso.equals("1")){
						qrySentencia =  
							" update COMREL_PYME_EPO_X_PRODUCTO set cs_habilitado = 'H' "+
							" where ic_pyme ="+ lsPyme+
							" and ic_epo ="+lsEpo+
							" and ic_producto_nafin ="+lsProducto;
						con.ejecutaSQL(qrySentencia);
					}
				}
			}       //fin if huboCambio
		}     //fin for
	} catch(Exception e) {
      System.out.println("AutorizaLineas.AutorizaNafin Exception"+e);
      resultado = false;
      throw new NafinException("ANTI0020");
		} finally {
      if(con.hayConexionAbierta())
      	{
	      con.terminaTransaccion(resultado);
	      con.cierraConexionDB();
        }
     }
 }         //fin AutorizaNafin

//Ultima Modificacion:13/05/2003 Israel Herrera
 public Vector consultaNafin(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin)
  	throws NafinException
  	{
    String		condicion 				= "";
    String		qrySentencia 			= "";
		Vector		vecFilas 					= new Vector();
    Vector		vecColumnas 			= new Vector();
    ResultSet	rs								= null;
		AccesoDB	con 							= null;
    String 		conTipoSolicitud	= "";
    String 		conNumeroSirac		= "";
    String		conConsecutivo		= "";
    String		strAux						= "";

			try {
				con = new AccesoDB();
				con.conexionDB();


//        getDatosComunes(ic_if,ic_epo,con);

        if(!"".equals(ic_linea))
					{
			        	if(!"4".equals(ic_producto_nafin)) {
							setFolioArmado(ic_linea);
							conTipoSolicitud = getTipoSolicitud();
							conNumeroSirac   = getNumeroSirac();
							conConsecutivo   = getConsecutivo().trim();
							condicion +=	" AND LC.ic_linea_credito = "+conConsecutivo+
											" AND LC.cg_tipo_solicitud = '"+conTipoSolicitud+"'"+
											" AND PY.in_numero_sirac = "+conNumeroSirac;
			        	}
			        	else {
			        		condicion +=	" AND LC.ic_linea_credito = "+ic_linea;
			        	}

					}

        if(!"".equals(ic_epo))
					{
					condicion += " AND LC.ic_epo = "+ ic_epo;
					if(!"".equals(ic_pyme)&&!"TODOS".equals(ic_pyme))
						condicion += " AND LC.ic_pyme = "+ic_pyme;
					if(!"".equals(ic_if)&&!"TODOS".equals(ic_if))
						condicion += " AND  LC.ic_if = "+ic_if;
					}

			qrySentencia =	" select LC.ic_linea_credito,E.cg_razon_social as NOMBRE_EPO ,I.cg_razon_social as NOMBRE_IF ,PY.cg_razon_social AS NOMBRE_PYME"+
							" ,decode(LC.cg_tipo_solicitud,'I','Inicial','R','Renovacion','Ampliacion') as TIPO_SOLICITUD,to_char(LC.df_captura,'dd/mm/yyyy') as FECHA_SOL,LC.fn_monto_solicitado,LC.fn_monto_autorizado"+
							" ,F.cd_nombre as BANCO_SERVICIO "+
              				" ,LC.cg_numero_cuenta, LC.cg_cuenta_clabe "+
							" ,PY.in_numero_sirac"+
							" ,CPN.ic_nombre as PRODUCTO_NAFIN, CPN.ic_producto_nafin as PRODUCTO_NAFIN_CVE"+
							" ,to_char(LC.df_autorizacion_if,'dd/mm/yyyy') as F_AUTORIZACION_IF"+
              				" ,LC.cg_numero_cuenta_if as CUENTA_IF "+
							" ,(SELECT SUM(fn_monto_autorizado_total) "+
							" 		FROM com_linea_credito lc2 "+
							" 		WHERE cg_tipo_solicitud <> 'A' "+
							" 		AND df_vencimiento_adicional > SYSDATE "+
							" 		AND ic_if NOT IN(lc.ic_if) AND lc.ic_pyme = lc2.ic_pyme) AS lineas_otros "+
							" , LC.fn_monto_autorizado - LC.fn_saldo_linea AS monto_ejercido "+
							" , LC. fn_saldo_linea	AS monto_disponible, LC.ic_moneda, LC.cg_cuenta_clabe	"+
							" , PY.in_numero_troya clientetroya, lc.ig_numero_solic_troya solictroya, i.ig_tipo_piso tipopisoif"+
							" ,to_char(LC.df_autorizacion,'dd/mm/yyyy') as F_AUTORIZACION"+
							" ,LC.CG_LINEA_TIPO as TIPO_LINEA "+
							" ,LC.IC_TABLA_AMORT "+
							" ,TA.CD_DESCRIPCION as TIPO_AMORT"+
							" ,LC.FN_MONTO_LIBERADO as MONTO_LIBERADO"+
							" from COM_LINEA_CREDITO LC, COMREL_PYME_EPO PE,COMCAT_PYME PY "+
							" ,COMREL_PYME_EPO_X_PRODUCTO PP,COMCAT_EPO E,COMCAT_IF I,COMCAT_FINANCIERA F"+
							" ,COMREL_IF_EPO IE,COMREL_IF_EPO_X_PRODUCTO IEP"+
							" ,COMCAT_PRODUCTO_NAFIN CPN, comcat_tabla_amort TA "+
							" where PY.ic_pyme = PE.ic_pyme "+
							" and PE.ic_pyme = PP.ic_pyme "+
							" and PE.ic_epo = PP.ic_epo "+
							" and PP.ic_pyme = LC.ic_pyme "+
							" and PP.ic_epo = LC.ic_epo "+
							" and LC.ic_estatus_linea in(3)"+	//autorizada if
							" and PE.cs_habilitado = 'S'"+
							//" and PP.cs_habilitado in('S','H')"+//modificacion del 29-04-2008
							" and PP.ic_producto_nafin in ("+ ic_producto_nafin +")"+
							" and PE.ic_epo = E.ic_epo"+
							" and E.cs_habilitado = 'S'"+
							" and LC.ic_if = I.ic_if"+
							" and F.ic_financiera(+) = LC.ic_financiera"+ //quitar el join
							" and I.ic_if = IE.ic_if"+
							" and E.ic_epo = IE.ic_epo"+
							" and PP.ic_producto_nafin = LC.ic_producto_nafin "+ //arlette
							" and IEP.ic_producto_nafin = LC.ic_producto_nafin "; //arlette
        					if (!"4".equals(ic_producto_nafin)) {
								qrySentencia +=
									" and IE.cs_aceptacion = 'S'"+
									" and IE.cs_vobo_nafin = 'S'";
        					}
        					else
								qrySentencia += " and IE.cs_distribuidores = 'S'";
							qrySentencia +=
      						" and IE.ic_if = IEP.ic_if"+
							" and IE.ic_epo = IEP.ic_epo"+
							" and IEP.ic_producto_nafin in ("+ ic_producto_nafin + ")" +
							" and IEP.cs_habilitado = 'S'"+
							//" and LC.cs_aceptacion_pyme='N'"+//modificacion del 29-04-2008
							" and PP.ic_producto_nafin = CPN.ic_producto_nafin "+
							" and LC.IC_TABLA_AMORT = TA.IC_TABLA_AMORT(+) "+
							condicion+
							" Order by PRODUCTO_NAFIN, NOMBRE_IF, NOMBRE_PYME";
							System.out.println(qrySentencia);
              rs = con.queryDB(qrySentencia);
				while(rs.next())
					{
					vecColumnas = new Vector();

/*0*/					vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO")==null)?"":rs.getString("IC_LINEA_CREDITO"));
/*1*/					vecColumnas.addElement((rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO"));
/*2*/					vecColumnas.addElement((rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF"));
/*3*/					vecColumnas.addElement((rs.getString("NOMBRE_PYME")==null)?"":rs.getString("NOMBRE_PYME"));
/*4*/					vecColumnas.addElement((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*5*/					vecColumnas.addElement((rs.getString("FECHA_SOL")==null)?"":rs.getString("FECHA_SOL"));
/*6*/					vecColumnas.addElement((rs.getString("FN_MONTO_SOLICITADO")==null)?"0":rs.getString("FN_MONTO_SOLICITADO"));
/*7*/					vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?"0":rs.getString("FN_MONTO_AUTORIZADO"));
/*8*/					vecColumnas.addElement((rs.getString("BANCO_SERVICIO")==null)?"":rs.getString("BANCO_SERVICIO"));
/*9*/					vecColumnas.addElement((rs.getString("cg_numero_cuenta")==null)?"":rs.getString("cg_numero_cuenta"));

							strAux = (rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
			       	while(strAux.length()<7)
								strAux = "0"+strAux;
			         strAux = rs.getString("TIPO_SOLICITUD").substring(0,1) +strAux + (String)vecColumnas.get(0);
/*10*/				vecColumnas.addElement(strAux);     		//folioArmado
/*11*/					vecColumnas.addElement((rs.getString("PRODUCTO_NAFIN")==null)?"":rs.getString("PRODUCTO_NAFIN"));
/*12*/					vecColumnas.addElement((rs.getString("PRODUCTO_NAFIN_CVE")==null)?"":rs.getString("PRODUCTO_NAFIN_CVE"));
/*13*/					vecColumnas.addElement((rs.getString("F_AUTORIZACION_IF")==null)?"":rs.getString("F_AUTORIZACION_IF"));
/*14*/					vecColumnas.addElement((rs.getString("CUENTA_IF")==null)?"":rs.getString("CUENTA_IF"));
/*15*/					vecColumnas.addElement((rs.getString("cg_cuenta_clabe")==null)?"":rs.getString("cg_cuenta_clabe"));
/*16*/					vecColumnas.addElement((rs.getString("LINEAS_OTROS")==null)?"":rs.getString("LINEAS_OTROS"));
/*17*/					vecColumnas.addElement((rs.getString("MONTO_EJERCIDO")==null)?"":rs.getString("MONTO_EJERCIDO"));
/*18*/					vecColumnas.addElement((rs.getString("MONTO_DISPONIBLE")==null)?"":rs.getString("MONTO_DISPONIBLE"));
/*19*/					vecColumnas.addElement((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*20*/					vecColumnas.addElement((rs.getString("CLIENTETROYA")==null)?"":rs.getString("CLIENTETROYA"));
/*21*/					vecColumnas.addElement((rs.getString("SOLICTROYA")==null)?"":rs.getString("SOLICTROYA"));
/*22*/					vecColumnas.addElement((rs.getString("TIPOPISOIF")==null)?"":rs.getString("TIPOPISOIF"));
/*23*/					vecColumnas.addElement((rs.getString("F_AUTORIZACION")==null)?"":rs.getString("F_AUTORIZACION"));
/*24*/					vecColumnas.addElement((rs.getString("TIPO_LINEA")==null)?"":rs.getString("TIPO_LINEA"));
/*25*/					vecColumnas.addElement((rs.getString("IC_TABLA_AMORT")==null)?"":rs.getString("IC_TABLA_AMORT"));
/*26*/					vecColumnas.addElement((rs.getString("TIPO_AMORT")==null)?"":rs.getString("TIPO_AMORT"));
/*27*/					vecColumnas.addElement((rs.getString("MONTO_LIBERADO")==null)?"":rs.getString("MONTO_LIBERADO"));

vecFilas.addElement(vecColumnas);
		    	}
        }catch(Exception e) {
        	System.out.println("ConsultaNafin Exception "+e);
        	throw new NafinException("SIST0001");
        }finally {
        	if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
	return vecFilas;
	} // Fin del metodo ConsultaNafin

 public Vector getComboLineas(String ic_pyme,String ic_epo,String tipoSolicitud)
 throws NafinException
 {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        if("I".equals(tipoSolicitud)) {
          qrySentencia  =	"select a.ic_if, a.ic_if||' '||a.cg_razon_social,c.ic_epo,'' "+
				" from comcat_if a,"+
				" (select ic_if"+
				" from comcat_if"+
				" minus"+
				" select ic_if from "+
				" com_linea_credito"+
				" where ic_estatus_linea"+
				" in(1,2,3,5)"+
				" and ic_pyme= "+ic_pyme+" )  b,"+
                " comrel_if_epo_x_producto c "+
				" where a.ic_if = b.ic_if"+
				" and a.cs_habilitado = 'S'"+
				" and c.ic_producto_nafin = 2"+
				" and c.ic_if = a.ic_if"+
				" and c.cs_habilitado = 'S'"+
				" and c.ic_epo = "+ic_epo+
				" and a.ic_if in("+
				"		select distinct B.ic_if"+
				"		from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C, comcat_moneda D, comrel_pyme_if E "+
				"		where A.ic_epo = E.ic_epo and B.ic_if = E.ic_if and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria and "+
				"		C.ic_moneda = D.ic_moneda and E.cs_borrado = 'N'"+
				"		and E.cs_vobo_if = 'S' "+
				" 		and E.ic_epo = "+ic_epo+
				" 		and C.ic_pyme = "+ic_pyme+
				")";
	} else if("A".equals(tipoSolicitud)) {
        	qrySentencia  =	"select a.ic_linea_credito, a.ic_linea_credito||' '||b.cg_razon_social,b.ic_if" +
				" from com_linea_credito a,comcat_if b" +
				" where a.ic_if = b.ic_if" +
				" and a.ic_pyme = " +ic_pyme +				//para la pyne
//				" and a.ic_epo = "+ses_ic_epo					//&iquest;para cualquier EPO?
				" and a.cg_tipo_solicitud in('I','R')" +				//debe ser inicial
				" and df_vencimiento_adicional > sysdate" +		//vigente
				" and a.cs_aceptacion_pyme = 'S'" +				//autorizada pyme
				" and b.cs_habilitado = 'S'"+
				" and  ic_estatus_linea = 5 ";					//autorizada nafin
        } else if("R".equals(tipoSolicitud)) {
        	qrySentencia  =	"select a.ic_linea_credito, b.ic_if||' '||b.cg_razon_social,b.ic_if" +
				" from com_linea_credito a,comcat_if b" +
				" where a.ic_if = b.ic_if" +
				" and a.ic_pyme = " +ic_pyme +				//para la pyne
//				" and a.ic_epo = "+ses_ic_epo					//&iquest;para cualquier EPO?
				" and a.cg_tipo_solicitud in('I','R')" +		//debe ser inicial
				" and df_vencimiento_adicional < sysdate" +		//vencida
				" and a.cs_aceptacion_pyme = 'S'" +				//autorizada pyme
				" and  ic_estatus_linea = 5 "+					//autorizada nafin
				" and b.cs_habilitado = 'S' "+
				" and a.ic_linea_credito not in "+
				" ("+
				"	SELECT IC_LINEA_CREDITO "+
				"	FROM COM_LINEA_CREDITO"+
				"	WHERE "+
				"	(CG_TIPO_SOLICITUD = 'R' AND DF_VENCIMIENTO_ADICIONAL > sysdate)"+
				"	or"+
				"	(cg_tipo_solicitud = 'R' and ic_estatus_linea = 1)"+
				"	or ic_linea_credito in"+
				"		(select ic_linea_credito_padre from com_linea_credito"+
				"	where cg_tipo_solicitud = 'R')"+
				" )";
	}
  	rs = con.queryDB(qrySentencia);
  	
  	System.out.println(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
	        vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
	        vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
	        vecFilas.addElement(vecColumnas);
	      }
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
    }
/********************************************************************************************/
 public Vector getComboLineasDist(String ic_pyme,String ic_epo,String tipoSolicitud)
 throws NafinException
 {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        if("I".equals(tipoSolicitud)) {
          qrySentencia  =	"select a.ic_if, a.ic_if||' '||a.cg_razon_social,c.ic_epo,'' "+
				" from comcat_if a,"+
				" (select ic_if"+
				" from comcat_if"+
				" minus"+
				" select ic_if from "+
				" com_linea_credito"+
				" where ic_estatus_linea"+
				" in(1,2,3,5)"+
				" and ic_pyme= "+ic_pyme+" and ic_producto_nafin = 4)  b,"+
        " comrel_if_epo_x_producto c,"+
        " comrel_if_epo d "+
				" where a.ic_if = b.ic_if"+
				" and a.cs_habilitado = 'S'"+
				" and c.ic_producto_nafin = 4"+
				" and c.ic_if = d.ic_if"+
				" and c.ic_epo = d.ic_epo"+
        " and d.cs_distribuidores = 'S'"+
				" and c.ic_if = a.ic_if"+
				" and c.cs_habilitado = 'S'"+
				" and c.ic_epo = "+ic_epo+
				" and a.ic_if in("+
				"		select distinct B.ic_if"+
				"		from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C, comcat_moneda D, comrel_pyme_if E "+
				"		where A.ic_epo = E.ic_epo and B.ic_if = E.ic_if and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria and "+
				"		C.ic_moneda = D.ic_moneda and E.cs_borrado = 'N'"+
				"		and E.cs_vobo_if = 'S' "+
				" 		and E.ic_epo = "+ic_epo+
				" 		and C.ic_pyme = "+ic_pyme+
				")";
	} else if("A".equals(tipoSolicitud)) {
        	qrySentencia  =	"select a.ic_linea_credito, a.ic_linea_credito||' '||b.cg_razon_social,b.ic_if" +
				" from com_linea_credito a,comcat_if b" +
				" where a.ic_if = b.ic_if" +
				" and a.ic_pyme = " +ic_pyme +				//para la pyne
//				" and a.ic_epo = "+ses_ic_epo					//&iquest;para cualquier EPO?
				" and a.cg_tipo_solicitud in('I','R')" +				//debe ser inicial
				" and df_vencimiento_adicional > sysdate" +		//vigente
				" and a.cs_aceptacion_pyme = 'S'" +				//autorizada pyme
				" and b.cs_habilitado = 'S'"+
				" and  ic_estatus_linea = 5 ";					//autorizada nafin

        } else if("R".equals(tipoSolicitud)) {
        	qrySentencia  =	"select a.ic_linea_credito, b.ic_if||' '||b.cg_razon_social,b.ic_if" +
				" from com_linea_credito a,comcat_if b" +
				" where a.ic_if = b.ic_if" +
				" and a.ic_pyme = " +ic_pyme +				//para la pyne
//				" and a.ic_epo = "+ses_ic_epo					//&iquest;para cualquier EPO?
				" and a.cg_tipo_solicitud in('I','R')" +		//debe ser inicial
				" and df_vencimiento_adicional < sysdate" +		//vencida
				" and a.cs_aceptacion_pyme = 'S'" +				//autorizada pyme
				" and  ic_estatus_linea = 5 "+					//autorizada nafin
				" and b.cs_habilitado = 'S' "+
				" and a.ic_linea_credito not in "+
				" ("+
				"	SELECT IC_LINEA_CREDITO "+
				"	FROM COM_LINEA_CREDITO"+
				"	WHERE "+
				"	(CG_TIPO_SOLICITUD = 'R' AND DF_VENCIMIENTO_ADICIONAL > sysdate)"+
				"	or"+
				"	(cg_tipo_solicitud = 'R' and ic_estatus_linea = 1)"+
				"	or ic_linea_credito in"+
				"		(select ic_linea_credito_padre from com_linea_credito"+
				"	where cg_tipo_solicitud = 'R')"+
				" )";
	}
//System.out.println(qrySentencia);
  rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
	        vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
	        vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
	        vecFilas.addElement(vecColumnas);
	      }
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
    }
/********************************************************************************************/


 public Vector getComboIfDescto(String ic_pyme,String ic_epo) throws NafinException {
    String		qrySentencia 	= "";
    String		qrySentencia1 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
          qrySentencia1 =
            "SELECT CI.IC_IF"+
            " FROM  COMREL_CUENTA_BANCARIA  CB,"+
            " COMREL_PYME_IF                PI,"+
            " COMREL_PYME_EPO               PE,"+
            " COMCAT_IF                     CI"+
            " WHERE CB.IC_CUENTA_BANCARIA = PI.IC_CUENTA_BANCARIA"+
            " AND   CB.IC_PYME            = PE.IC_PYME"+
            " AND   PI.IC_IF              = CI.IC_IF"+
            " AND   PI.IC_EPO             = PE.IC_EPO"+
            " AND   CB.IC_PYME            = "+ic_pyme+" "+
            " AND   PE.IC_EPO             = "+ic_epo+" "+
            " AND   CB.CS_BORRADO         = 'N'"+
            " AND   PE.CS_HABILITADO      = 'S'"+
            " AND   PI.CS_BORRADO         = 'N'";

          qrySentencia  =
            "SELECT IE.IC_IF,CI.CG_RAZON_SOCIAL "+
            " FROM COMREL_IF_EPO IE, COMCAT_IF CI "+
            " WHERE  IE.IC_EPO = "+ic_epo+" "+
            " AND IE.IC_IF = CI.IC_IF "+
            " AND IE.IC_IF NOT IN("+qrySentencia1+")";

  	rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
	        vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
	        vecFilas.addElement(vecColumnas);
	      }
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
   }

public Vector getLineasProceso(String ic_pyme,String ic_epo,String tipoSolicitud)
 throws NafinException
 {
    String	qrySentencia          = "";
    AccesoDB	con                   = null;
    ResultSet	rs 		      = null;
    Vector	vecFilas	      = new Vector();
    Vector	vecColumnas	      = new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
          qrySentencia  =	" select distinct i.cg_razon_social,decode(lc.ic_estatus_linea,5,decode(pep.cs_habilitado,'H','Afiliado','En proceso'),'En proceso')"+
				" from comcat_if i,"+
        " com_linea_credito lc,"+
        " comrel_pyme_epo_x_producto pep"+
				" where lc.ic_estatus_linea"+
				" in(1,2,3,5)"+
				" and lc.ic_pyme= "+ic_pyme+
				" and lc.ic_if = i.ic_if"+
				" and i.cs_habilitado = 'S'"+
				" and lc.ic_producto_nafin = 2"+
				" and lc.ic_epo = "+ic_epo+
        " and pep.ic_epo = lc.ic_epo"+
        " and pep.ic_pyme = lc.ic_pyme"+
        " and lc.cg_tipo_solicitud = '"+tipoSolicitud+"'"+
        " and pep.ic_producto_nafin = lc.ic_producto_nafin"+
				" and lc.ic_if in("+
				"		select distinct B.ic_if"+
				"		from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C, comcat_moneda D, comrel_pyme_if E "+
				"		where A.ic_epo = E.ic_epo and B.ic_if = E.ic_if and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria and "+
				"		C.ic_moneda = D.ic_moneda and E.cs_borrado = 'N'"+
				"		and E.cs_vobo_if = 'S' "+
				" 		and E.ic_epo = "+ic_epo+
				" 		and C.ic_pyme = "+ic_pyme+
				")";

  	rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
	        vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
	        vecFilas.addElement(vecColumnas);
	      }
      } catch(Exception e){
           throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
     return vecFilas;
    }

public Vector getIfAfilDescto(String ic_pyme,String ic_epo)
 throws NafinException
 {
    String    qrySentencia  = "";
    AccesoDB	con           = null;
    ResultSet	rs            = null;
    Vector    vecFilas      = new Vector();
    Vector    vecColumnas   = new Vector();
    int       icIf          = 0;

    	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
          "SELECT distinct CI.IC_IF,CI.CG_RAZON_SOCIAL, PI.CS_VOBO_IF "+
          " FROM  COMREL_CUENTA_BANCARIA CB, "+
          " COMREL_PYME_IF 	PI, "+
          " COMREL_PYME_EPO 	PE, "+
          " COMCAT_IF        CI "+
          " WHERE CB.IC_CUENTA_BANCARIA = PI.IC_CUENTA_BANCARIA "+
          " AND   CB.IC_PYME            = PE.IC_PYME "+
          " AND   PI.IC_IF              = CI.IC_IF "+
          " AND   PI.IC_EPO             = PE.IC_EPO "+
          " AND   CB.IC_PYME            = "+ic_pyme+" "+
          " AND   PE.IC_EPO             = "+ic_epo+" "+
          " AND   CB.CS_BORRADO         = 'N' "+
          " AND   PE.CS_HABILITADO      = 'S' "+
          " AND   PI.CS_BORRADO         = 'N' "+
          " ORDER BY 2 asc,3 desc";
        rs = con.queryDB(qrySentencia);
        while(rs.next()) {
          if (icIf==0 || icIf!=rs.getInt(1)) {
            icIf=rs.getInt(1);
            vecColumnas = new Vector();
            vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
            vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
            vecFilas.addElement(vecColumnas);
          }
	      }
      } catch(Exception e){
           throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
     return vecFilas;
    }

    public Vector getEpoRelacionada(String ic_pyme,String ic_epo)
    	throws NafinException
    	{
			String		qrySentencia	= "";
      AccesoDB	con						= null;
			ResultSet	rs						= null;
      Vector 		vecFilas			= new Vector();
      Vector		vecColumnas		= null;
				try {
						con = new AccesoDB();
            con.conexionDB();
       			qrySentencia = 	"select distinct E.ic_epo,E.cg_razon_social"+
								" from comcat_epo E,com_linea_credito LC"+
								" where E.ic_epo = LC.ic_epo "+
								" and LC.ic_pyme = "+ic_pyme+
								" and LC.ic_epo not in("+ic_epo+")";
						rs = con.queryDB(qrySentencia);
						while(rs.next())
							{
              vecColumnas = new Vector();
							vecColumnas.addElement((rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO"));
							vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
							vecFilas.addElement(vecColumnas);
              }
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return vecFilas;
      }

 public void AutorizaNafin(String folio [],String estatus [],String [] huboCambio,String sElementos,String causa [])
		throws NafinException
    {
    int i=0;
    String 		qrySentencia	= "";
    ResultSet	rs				= null;
    AccesoDB	con				= null;
    String 		lineaPadre		= "-1";
    String 		montoTotal		= "0.00";
    String 		saldoTotal		= "0.00";
    String 		fechaVen  		= "";
    String 		tipoSolicitud 	= "";
	String 		fechaLimDisp	= "";
    float  		hayCambio 		= 0.0f;
	float		hayCambioDisp	= 0.0f;
    int				iElementos	= Integer.parseInt(sElementos);
    boolean		resultado 		= true;
	String		lsPyme			= "";
	String		lsEpo			= "";
	String		lsProducto		= "";
	String		lsTipoPiso		= "";
    try	{
      con = new AccesoDB();
      con.conexionDB();
      for(i=0;i<iElementos&&resultado;i++)
        {
        if(huboCambio[i].equals("S"))
          {
          qrySentencia =  " update com_linea_credito "+
                          " set ic_estatus_linea = "+estatus[i]+
                          " ,df_autorizacion_nafin = sysdate"+
                          " ,cg_causa_rechazo = '"+ causa[i] +"'"+
                          " where ic_linea_credito = "+folio[i];
//			  System.out.println(qrySentencia);
          con.ejecutaSQL(qrySentencia);
          if(estatus[i].equals("5"))        //si es autorizada, sumar los saldos,montos y fechas
              {
              qrySentencia =  
								"select ic_linea_credito_padre,fn_monto_autorizado"+
								" ,fn_saldo_linea,to_char(df_vencimiento,'dd/mm/yyyy')"+
								" ,cg_tipo_solicitud, to_char(df_limite_disposicion,'dd/mm/yyyy')"+
								" , LC.ic_pyme, LC.ic_epo, LC.ic_producto_nafin"+
								" , I.ig_tipo_piso"+
								" from com_linea_credito lc"+
								"      ,comcat_if i "+
								" where i.ic_if(+)=lc.ic_if and ic_linea_credito ="+folio[i];
              rs = con.queryDB(qrySentencia);
              if(rs.next())
                {
									lineaPadre  = rs.getString(1);
									montoTotal  = rs.getString(2);
									saldoTotal  = rs.getString(3);
									fechaVen    = rs.getString(4);
									tipoSolicitud = rs.getString(5);
									fechaLimDisp = rs.getString(6);
									lsPyme			= rs.getString(7);
									lsEpo			= rs.getString(8);
									lsProducto		= rs.getString(9);
									lsTipoPiso		= (rs.getString(10)==null)?"":rs.getString(10);
                }
              con.cierraStatement();
              if("A".equals(tipoSolicitud))
                  {
                  qrySentencia =  "select to_date('"+fechaVen+"','dd/mm/yyyy')-df_vencimiento_adicional"+
				 	 			  ", to_date('"+fechaLimDisp+"','dd/mm/yyyy')-df_limite_disposicion_adic"+
                                  " from com_linea_credito where ic_linea_credito = "+lineaPadre;
                  rs = con.queryDB(qrySentencia);
                  if(rs.next())
									{
                       hayCambio = rs.getFloat(1);
                       hayCambioDisp = rs.getFloat(2);
									}
                  con.cierraStatement();
                  qrySentencia =  " update com_linea_credito set fn_monto_autorizado_total = fn_monto_autorizado_total +"+montoTotal+
                                  " ,fn_saldo_linea_total = fn_saldo_linea_total +"+saldoTotal;
                  if(hayCambio>0)
                      qrySentencia += " ,df_vencimiento_adicional = to_date('"+fechaVen+"','dd/mm/yyyy')";
                  if(hayCambioDisp>0)
                      qrySentencia += " ,df_limite_disposicion_adic = to_date('"+fechaLimDisp+"','dd/mm/yyyy')";
                  qrySentencia += " ,cg_causa_rechazo = '"+ causa[i] +"'";
                  qrySentencia += " where ic_linea_credito = "+lineaPadre;
//				  System.out.println(qrySentencia);
                  con.ejecutaSQL(qrySentencia);
                  } //fin if "A".equals
									if(lsTipoPiso.equals("1")){
														qrySentencia =  " update COMREL_PYME_EPO_X_PRODUCTO set cs_habilitado = 'H' "+
																						" where ic_pyme ="+ lsPyme+
														" and ic_epo ="+lsEpo+
														" and ic_producto_nafin ="+lsProducto;
														con.ejecutaSQL(qrySentencia);
									}
									
              }	// fin estatus = 5
          }       //fin if huboCambio
        }     //fin for
    } catch(Exception e) {
      System.out.println("AutorizaLineas.AutorizaNafin Exception"+e);
      resultado = false;
      throw new NafinException("ANTI0020");
		} finally {
      if(con.hayConexionAbierta())
      	{
	      con.terminaTransaccion(resultado);
	      con.cierraConexionDB();
        }
     }
 }         //fin AutorizaNafin
 
 public Vector getPlazoIF(String ic_producto_nafin, String ic_if) throws NafinException {
	Vector vecFilas = new Vector(); 	
	try {
		vecFilas = getPlazoIF(ic_producto_nafin, ic_if, "");
	} catch(Exception e) {
		throw new NafinException("SIST0001");
	}
	return vecFilas;
 } // getPlazoIF

 public Vector getPlazoIF(String ic_producto_nafin, String ic_if, String credExp) throws NafinException {
	String 				qrySentencia 	= "";
	PreparedStatement 	ps				= null;
	ResultSet 			rs 				= null;
	AccesoDB  			con 			= null;

	Vector  vecFilas		= new Vector();
	Vector  vecColumnas	= null;

	try {
		con = new AccesoDB();
		con.conexionDB();
		if("".equals(credExp)) {
			qrySentencia = 
				" SELECT   cp.ic_plazo AS plazo, cp.in_plazo_dias AS dias"   +
				"     FROM comcat_plazo cp"   +
				"    WHERE cp.in_plazo_meses <="   +
				"             (SELECT cp1.in_plazo_meses"   +
				"                FROM comcat_plazo cp1"   +
				"               WHERE cp1.ic_plazo ="   +
				"                        NVL ((SELECT cpi.ic_periodo_max_vig"   +
				"                                FROM comrel_producto_if cpi"   +
				"                               WHERE cpi.ic_producto_nafin = ?"   +
				"                                 AND cpi.ic_if = ?),"   +
				"                           (SELECT cpn.ic_periodo_max_vig"   +
				"                              FROM comcat_producto_nafin cpn"   +
				"                             WHERE cpn.ic_producto_nafin = ?)))"   +
				"      AND cp.ic_producto_nafin IS NULL"   +
				" ORDER BY 2"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_producto_nafin));
			ps.setInt(2,Integer.parseInt(ic_if));
			ps.setInt(3,Integer.parseInt(ic_producto_nafin));
		} else {
			qrySentencia = 
				" SELECT   cp.ic_plazo AS plazo, cp.in_plazo_dias AS dias"   +
				"     FROM comcat_plazo cp"   +
				"    WHERE cp.in_plazo_meses ="   +
				"                        (SELECT cp1.in_plazo_meses"   +
				"                           FROM comcat_plazo cp1"   +
				"                          WHERE cp1.ic_plazo = (SELECT cpn.ic_periodo_max_vig"   +
				"                                                  FROM comcat_producto_nafin cpn"   +
				"                                                 WHERE cpn.ic_producto_nafin = ?))"   +
				"      AND cp.ic_producto_nafin IS NULL"   +
				" ORDER BY 2"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_producto_nafin));
		}
		System.out.println("\n qrySentencia: "+qrySentencia);
		System.out.println("\n qrySentencia: "+ic_producto_nafin);
		System.out.println("\n qrySentencia: "+ic_if);
		
		rs = ps.executeQuery();
		while(rs.next()) {
			vecColumnas = new Vector();
			vecColumnas.addElement((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
			vecColumnas.addElement((rs.getString("DIAS")==null)?"":rs.getString("DIAS"));
			vecFilas.addElement(vecColumnas);
		}
		rs.close(); ps.close();
	} catch(Exception e) {
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
	}
	return vecFilas;
 } // getPlazoIF


	public Vector getDispVig(String ic_producto_nafin, String ic_if) throws NafinException {
		String qrySentencia = "";
		ResultSet rs = null;
		AccesoDB  con = null;
		
		Vector  vecFilas		= new Vector();
		Vector  vecColumnas	= null;

		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
				" SELECT  nvl(CPI.ic_periodo_max_disp,CPN.ic_periodo_max_disp) as DISP,  nvl(CPI.ic_periodo_max_vig,CPN.ic_periodo_max_vig) as VIG "+
				" FROM COMREL_PRODUCTO_IF CPI, COMCAT_PRODUCTO_NAFIN CPN "+
				" WHERE CPI.ic_producto_nafin = "+ ic_producto_nafin +
				" AND CPI.ic_producto_nafin = CPN.ic_producto_nafin "+
				" AND CPI.ic_if = "+ ic_if+" ";
//	  System.out.println(qrySentencia);
			rs = con.queryDB(qrySentencia);
			if(!rs.next()) {
				qrySentencia = 
					" SELECT  CPN.ic_periodo_max_disp as DISP,  CPN.ic_periodo_max_vig as VIG "+
					" FROM COMCAT_PRODUCTO_NAFIN CPN "+
					" WHERE CPN.ic_producto_nafin = "+ ic_producto_nafin +"";
			}
			rs = con.queryDB(qrySentencia);
//	  System.out.println(qrySentencia);
			while(rs.next()) {
				vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString("DISP")==null)?"0":rs.getString("DISP"));
				vecColumnas.addElement((rs.getString("VIG")==null)?"0":rs.getString("VIG"));
//			  System.out.println("Disp "+rs.getString("DISP"));
				vecFilas.addElement(vecColumnas);
			}
			con.cierraStatement();
		} catch(Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return vecFilas;
	} // getDispVig


  public String getMontoMaxLinea(String ic_producto_nafin, String ic_moneda)
  throws NafinException
  {
  String qrySentencia 	= "";
  ResultSet rs 			= null;
  AccesoDB  con 		= null;

  String MontoMax 		= null;

    try {
      con = new AccesoDB();
      con.conexionDB();
	  if(ic_producto_nafin.equals("5"))
	  {
	      qrySentencia = " SELECT nvl(fn_monto_maximo,0) "+
						" FROM COMREL_PARAM_X_MONEDA "+
						" WHERE ic_producto_nafin = "+ ic_producto_nafin +
						" AND ic_moneda = "+ ic_moneda +"";
	  }
	  else
	  {
	      qrySentencia =  " SELECT fn_maximo_linea "+
						  " FROM comcat_producto_nafin "+
						  " WHERE ic_producto_nafin = "+ ic_producto_nafin;

	  }

//	  System.out.println("["+qrySentencia+"]");
      rs = con.queryDB(qrySentencia);
      if(rs.next())
		{
			 MontoMax = (rs.getString(1)==null)?"":rs.getString(1);
        }
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return MontoMax;
      } // getMontoMaxLinea

  public String validaCtaMoneda(String ic_if, String folio, String ic_moneda)
  throws NafinException
  {
  String qrySentencia 	= "";
  ResultSet rs 			= null;
  AccesoDB  con 		= null;

  String Cuenta 	= null;

    try {
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia = " SELECT CB.ic_cuenta_bancaria "+
					" FROM COMREL_CUENTA_BANCARIA CB, COMREL_PYME_IF PI, COM_LINEA_CREDITO LP "+
					" WHERE CB.ic_moneda = "+ ic_moneda +
					" AND LP.ic_linea_credito = "+ folio +
					" AND CB.ic_pyme = LP.ic_pyme "+
					" AND PI.ic_if = "+ ic_if +
					" AND PI.ic_epo = LP.ic_epo "+
					" AND CB.ic_cuenta_bancaria =  PI.ic_cuenta_bancaria "+
					" AND CB.cs_borrado = 'N' "+
					" AND PI.cs_borrado = 'N' ";

//	  System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next())
			 Cuenta = (rs.getString(1)==null)?"0":rs.getString(1);
	  else
	   		 Cuenta = "0";

            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return Cuenta;
      } // validaCtaDolar

  public String validaLineaCredito(String ic_epo, String ic_if, String ic_pyme, String producto)
  throws NafinException
  {
  String qrySentencia 	= "";
  ResultSet rs 			= null;
  AccesoDB  con 		= null;

  String linCredito		= null;

    try {
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia = " SELECT ic_linea_credito, df_vencimiento_adicional, ic_estatus_linea, cg_tipo_solicitud "+
					" FROM COM_LINEA_CREDITO "+
					" WHERE ic_pyme = "+ ic_pyme +
					" AND ic_epo = "+ ic_epo +
					" AND ic_if = "+ ic_if +
					" AND TRUNC(df_vencimiento_adicional) > TRUNC(SYSDATE) "+
					" AND ic_estatus_linea in (5,7,9) "+
					" AND cs_aceptacion_pyme ='S' "+
					" AND cg_tipo_solicitud IN ('I','R') ";
					if(producto.equals("5"))
						qrySentencia += " AND ic_producto_nafin = 2 ";
					else if(producto.equals("2"))
						qrySentencia += " AND ic_producto_nafin = 5 ";

			System.out.println("---------"+qrySentencia);
			
			
      rs = con.queryDB(qrySentencia);
      if(rs.next())
			 linCredito = (rs.getString(1)==null)?"0":rs.getString(1);
	  else
	   		 linCredito = "0";

            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return linCredito;
      } // validaLineaCredito


  public Vector getEstatus_INV(String tipo)
  throws NafinException
  {
  String qrySentencia = "";
  ResultSet rs = null;
  AccesoDB  con = null;

  Vector  vecFilas		= new Vector();
  Vector  vecColumnas	= null;

    try {
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia = " SELECT ic_estatus_disposicion AS CLAVE, cd_descripcion AS DESCRIPCION "+
					 " FROM INVCAT_ESTATUS_DISPOSICION ";
					 if (tipo.equals("EPO")||tipo.equals("EDO_ADEUDOS"))
						 qrySentencia += " WHERE ic_estatus_disposicion IN (3,4,5,6,7,8) ";
					 qrySentencia += " ORDER BY 1";

//	  System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      while(rs.next())
		{
              vecColumnas = new Vector();
		  	  vecColumnas.addElement((rs.getString("CLAVE")==null)?"":rs.getString("CLAVE"));
		  	  vecColumnas.addElement((rs.getString("DESCRIPCION")==null)?"":rs.getString("DESCRIPCION"));
 			  vecFilas.addElement(vecColumnas);
              }
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return vecFilas;
      } // getEstatusEPO

/* Cambios Febrero 2003*/
  public Vector getConsultaLC_Inv(String ic_pyme, String ic_if, String ic_moneda, String num_disposicion, String f_operacion_ini, String f_operacion_fin, String ic_estatus, String monto_credito_ini, String monto_credito_fin, String ic_epo, String tipo, String numero_prestamo, String numero_solicitud)
  throws NafinException
  {
  String condicion		= "";
  String qrySentencia 	= "";
  ResultSet rs 			= null;
  AccesoDB  con 		= null;

  Vector  vecFilas		= new Vector();
  Vector  vecColumnas	= null;

  if(!"".equals(ic_pyme)&&!"TODOS".equals(ic_pyme))
	condicion += " AND ID.ic_pyme = "+ic_pyme;
  if(!"".equals(ic_if)&&!"TODOS".equals(ic_if))
	condicion += " AND LC.ic_if = "+ic_if;
  if(!"".equals(ic_moneda)&&!"TODOS".equals(ic_moneda))
	condicion += " AND LC.ic_moneda = "+ic_moneda;
  if(!"".equals(num_disposicion)&&!"TODOS".equals(num_disposicion))
	condicion += " AND ID.cc_disposicion = '"+num_disposicion+"' ";
  if(!"".equals(f_operacion_ini)&&!"".equals(f_operacion_fin))
  {
  	if("PYME".equals(tipo))
	condicion += " AND TO_CHAR(S.df_autorizacion,'DD/MM/YYYY') BETWEEN TO_DATE('"+f_operacion_ini+"','DD/MM/YYYY') AND TO_DATE('"+f_operacion_fin+"','DD/MM/YYYY')";
	else
	condicion += " AND TO_CHAR(ID.df_disposicion,'DD/MM/YYYY') BETWEEN TO_DATE('"+f_operacion_ini+"','DD/MM/YYYY') AND TO_DATE('"+f_operacion_fin+"','DD/MM/YYYY')";
  }
  if(!"".equals(f_operacion_ini)&&"".equals(f_operacion_fin))
	condicion += " AND TO_CHAR(ID.df_disposicion,'DD/MM/YYYY') >= TO_DATE('"+f_operacion_ini+"','DD/MM/YYYY')";
  if("".equals(f_operacion_ini)&&!"".equals(f_operacion_fin))
	condicion += " AND TO_CHAR(ID.df_disposicion,'DD/MM/YYYY') <= TO_DATE('"+f_operacion_fin+"','DD/MM/YYYY')";
  if(!"".equals(ic_estatus)&&!"TODOS".equals(ic_estatus))
	condicion += " AND ID.ic_estatus_disposicion = "+ic_estatus;
  if(!"".equals(monto_credito_ini)&&!"".equals(monto_credito_fin))
	condicion += " AND ID.fn_monto_credito BETWEEN "+monto_credito_ini+" AND "+monto_credito_fin+" ";
  if(!"".equals(monto_credito_ini)&&"".equals(monto_credito_fin))
	condicion += " AND ID.fn_monto_credito >= "+monto_credito_ini+" ";
  if("".equals(monto_credito_ini)&&!"".equals(monto_credito_fin))
	condicion += " AND ID.fn_monto_credito <= "+monto_credito_fin+" ";
  if(!"".equals(ic_epo)&&!"TODOS".equals(ic_epo))
	condicion += " AND ID.ic_epo = "+ic_epo;
  if("EPO".equals(tipo))
  	condicion += " AND ID.ic_estatus_disposicion in (3,4,5,6,7,8) ";
  if(!"".equals(numero_prestamo)&&!"TODOS".equals(numero_prestamo))
	condicion += " AND S.ig_numero_prestamo = "+numero_prestamo;
  if(!"".equals(numero_solicitud)&&!"TODOS".equals(numero_solicitud))
	condicion += " AND S.ig_numero_solic = "+numero_solicitud;


    try {
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia = " SELECT DISTINCT P.cg_razon_social AS NOMBRE_PYME, I.cg_razon_social AS NOMBRE_IF, "+
					" 	   ID.cc_disposicion, TO_CHAR(ID.df_disposicion,'DD-MON-YYYY') AS FECHA_OPERACION, "+
					"	   ED.cd_descripcion AS ESTATUS, M.cd_nombre AS MONEDA, "+
					"	   ID.fn_monto_credito, ID.ic_plazo, "+
					"	   ID.fn_tasa_interes, ID.fn_interes_generado, "+
					" 	   ID.fn_interesm_generado, TO_CHAR(df_inicio_cobro,'DD-MON-YYYY') AS FECHA_INI_COBRO, "+
					"	   TO_CHAR(ID.df_proximo_pago,'DD-MON-YYYY') AS FECHA_PROX_PAGO, TO_CHAR(ID.df_vencimiento,'DD-MON-YYYY') AS FECHA_ULT_PAGO, "+
					"	   NVL(PA.PAGOS,'0')  AS PAGOS_REALIZADOS, ID.fn_capital_pagado, "+
					"	   ID.fn_interes_pagado, ID.fn_interesm_pagado, "+
					"	   CE.cg_razon_social AS NOMBRE_EPO, ID.cc_acuse, S.ig_numero_prestamo,  "+
					"	   TO_CHAR(S.df_autorizacion,'DD-MON-YYYY') AS FECHA_OPERACION_PYME, ID.fn_monto_prox_pago, ID.ic_estatus_disposicion, "+
					"	   LC.ic_moneda as CVE_MONEDA, TO_CHAR(S.df_autorizacion,'DD/MM/YYYY') AS FECHA_OPERACION_PYME_IMP, TO_CHAR(ID.df_disposicion,'DD/MM/YYYY') AS FECHA_OPERACION_IMP, "+
					" 	   TO_CHAR(df_inicio_cobro,'DD/MM/YYYY') AS FECHA_INI_COBRO_IMP, TO_CHAR(ID.df_proximo_pago,'DD/MM/YYYY') AS FECHA_PROX_PAGO_IMP, TO_CHAR(ID.df_vencimiento,'DD/MM/YYYY') AS FECHA_ULT_PAGO_IMP, "+
					"	   P.in_numero_sirac AS PYME_SIRAC, S.cc_acuse AS ACUSE_IF, S.ig_numero_solic, TO_CHAR(sysdate,'DD-MON-YYYY') AS FECHA_HOY "+
					" FROM INV_DISPOSICION ID, COM_LINEA_CREDITO LC, COMCAT_IF I, "+
					"	 COMCAT_PYME P, INVCAT_ESTATUS_DISPOSICION ED, COMCAT_MONEDA M, "+
					"	 INV_MENSUALIDAD IM, INV_SOLICITUD S, COMCAT_EPO CE, "+
					"		(SELECT COUNT(IM1.ic_mensualidad) AS PAGOS, IM1.cc_disposicion AS IC_DISP "+
					"		 FROM INV_MENSUALIDAD IM1 "+
					"		 WHERE IM1.ic_estatus_mensualidad = 6 "+
					"		 GROUP BY IM1.cc_disposicion ) PA "+
					" WHERE ID.ic_linea_credito = LC.ic_linea_credito "+
					" AND I.ic_if = LC.ic_if "+
					" AND P.ic_pyme = ID.ic_pyme "+
					" AND ED.ic_estatus_disposicion = ID.ic_estatus_disposicion "+
					" AND M.ic_moneda = LC.ic_moneda "+
					" AND ID.cc_disposicion = IM.cc_disposicion "+
					" AND ID.cc_disposicion = S.cc_disposicion(+) "+
					" AND ID.ic_epo (+)= CE.ic_epo "+
					"  AND PA.IC_DISP (+)= ID.cc_disposicion "+
					condicion +" ";


	  System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      while(rs.next())
		{
              vecColumnas = new Vector();
/*0*/		  	  vecColumnas.addElement((rs.getString("NOMBRE_PYME")==null)?"":rs.getString("NOMBRE_PYME"));
/*1*/		  	  vecColumnas.addElement((rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF"));
/*2*/		  	  vecColumnas.addElement((rs.getString("CC_DISPOSICION")==null)?"":rs.getString("CC_DISPOSICION"));
/*3*/		  	  vecColumnas.addElement((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*4*/		  	  vecColumnas.addElement((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*5*/		  	  vecColumnas.addElement((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*6*/		  	  vecColumnas.addElement((rs.getString("FN_MONTO_CREDITO")==null)?"":rs.getString("FN_MONTO_CREDITO"));
/*7*/		  	  vecColumnas.addElement((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
/*8*/		  	  vecColumnas.addElement((rs.getString("FN_TASA_INTERES")==null)?"":rs.getString("FN_TASA_INTERES"));
/*9*/		  	  vecColumnas.addElement((rs.getString("FN_INTERES_GENERADO")==null)?"":rs.getString("FN_INTERES_GENERADO"));
/*10*/		  	  vecColumnas.addElement((rs.getString("FN_INTERESM_GENERADO")==null)?"":rs.getString("FN_INTERES_GENERADO"));
/*11*/		  	  vecColumnas.addElement((rs.getString("FECHA_INI_COBRO")==null)?"":rs.getString("FECHA_INI_COBRO"));
/*12*/		  	  vecColumnas.addElement((rs.getString("FECHA_PROX_PAGO")==null)?"":rs.getString("FECHA_PROX_PAGO"));
/*13*/		  	  vecColumnas.addElement((rs.getString("FECHA_ULT_PAGO")==null)?"":rs.getString("FECHA_ULT_PAGO"));
/*14*/		  	  vecColumnas.addElement((rs.getString("PAGOS_REALIZADOS")==null)?"":rs.getString("PAGOS_REALIZADOS"));
/*15*/		  	  vecColumnas.addElement((rs.getString("FN_CAPITAL_PAGADO")==null)?"":rs.getString("FN_CAPITAL_PAGADO"));
/*16*/		  	  vecColumnas.addElement((rs.getString("FN_INTERES_PAGADO")==null)?"":rs.getString("FN_INTERES_PAGADO"));
/*17*/		  	  vecColumnas.addElement((rs.getString("FN_INTERESM_PAGADO")==null)?"":rs.getString("FN_INTERESM_PAGADO"));
/*18*/		  	  vecColumnas.addElement((rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO"));
/*19*/		  	  vecColumnas.addElement((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*20*/		  	  vecColumnas.addElement((rs.getString("IG_NUMERO_PRESTAMO")==null)?"":rs.getString("IG_NUMERO_PRESTAMO"));
/*21*/		  	  vecColumnas.addElement((rs.getString("FECHA_OPERACION_PYME")==null)?"":rs.getString("FECHA_OPERACION_PYME"));
/*22*/		  	  vecColumnas.addElement((rs.getString("FN_MONTO_PROX_PAGO")==null)?"":rs.getString("FN_MONTO_PROX_PAGO"));
/*23*/		  	  vecColumnas.addElement((rs.getString("IC_ESTATUS_DISPOSICION")==null)?"":rs.getString("IC_ESTATUS_DISPOSICION"));
/*24*/		  	  vecColumnas.addElement((rs.getString("CVE_MONEDA")==null)?"":rs.getString("CVE_MONEDA"));
/*25*/		  	  vecColumnas.addElement((rs.getString("FECHA_OPERACION_PYME_IMP")==null)?"":rs.getString("FECHA_OPERACION_PYME_IMP"));
/*26*/		  	  vecColumnas.addElement((rs.getString("FECHA_OPERACION_IMP")==null)?"":rs.getString("FECHA_OPERACION_IMP"));
/*27*/		  	  vecColumnas.addElement((rs.getString("FECHA_INI_COBRO_IMP")==null)?"":rs.getString("FECHA_INI_COBRO_IMP"));
/*28*/		  	  vecColumnas.addElement((rs.getString("FECHA_PROX_PAGO_IMP")==null)?"":rs.getString("FECHA_PROX_PAGO_IMP"));
/*29*/		  	  vecColumnas.addElement((rs.getString("FECHA_ULT_PAGO_IMP")==null)?"":rs.getString("FECHA_ULT_PAGO_IMP"));
/*30*/		  	  vecColumnas.addElement((rs.getString("PYME_SIRAC")==null)?"":rs.getString("PYME_SIRAC"));
/*31*/		  	  vecColumnas.addElement((rs.getString("ACUSE_IF")==null)?"":rs.getString("ACUSE_IF"));
/*32*/		  	  vecColumnas.addElement((rs.getString("IG_NUMERO_SOLIC")==null)?"":rs.getString("IG_NUMERO_SOLIC"));
/*33*/		  	  vecColumnas.addElement((rs.getString("FECHA_HOY")==null)?"":rs.getString("FECHA_HOY"));

 			  vecFilas.addElement(vecColumnas);
              }
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return vecFilas;
      } // getConsultaEPO_LC_Inv


  public Vector getDeatalleMensualidad_INV(String cc_disposicion, String condicion)
  throws NafinException
  {
  String qrySentencia = "";
  ResultSet rs = null;
  AccesoDB  con = null;

  Vector  vecFilas		= new Vector();
  Vector  vecColumnas	= null;

    try {
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia = " SELECT TO_CHAR(M.df_pago,'DD-MON-YYYY') AS FECHA_PAGO, M.ic_mensualidad, M.fn_monto_mensualidad, "+
					" 	   M.fn_interes, M.fn_interesm, M.fn_interes_pagado, "+
					"	   M.fn_interesm_pagado, M.fn_capital_pagado, ED.cd_descripcion, "+
					"	   TO_CHAR(M.df_pago,'DD/MM/YYYY') AS FECHA_PAGO2, M.fn_saldo_inicial "+
					" FROM inv_mensualidad M, invcat_estatus_disposicion ED "+
					" WHERE cc_disposicion= '"+cc_disposicion+"' "+
					" AND M.ic_estatus_mensualidad = ED.ic_estatus_disposicion(+) ";
//					if(condicion.equals("PAGOS_ANTERIORES"))
//						qrySentencia += " AND M.df_pago <= sysdate ";
					/*else*/ if (condicion.equals("ULTIMO_PAGO"))
						qrySentencia += " AND TO_DATE(TO_CHAR(M.df_pago,'MM/YYYY'),'MM/YYYY') >= TO_DATE(TO_CHAR(ADD_MONTHS(SYSDATE,1),'MM/YYYY'),'MM/YYYY') ";
					qrySentencia += " ORDER BY 2 ";

	  System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      while(rs.next())
		{
              vecColumnas = new Vector();
/*0*/		  	  vecColumnas.addElement((rs.getString("FECHA_PAGO")==null)?"":rs.getString("FECHA_PAGO"));
/*1*/		  	  vecColumnas.addElement((rs.getString("IC_MENSUALIDAD")==null)?"":rs.getString("IC_MENSUALIDAD"));
/*2*/		  	  vecColumnas.addElement((rs.getString("FN_MONTO_MENSUALIDAD")==null)?"":rs.getString("FN_MONTO_MENSUALIDAD"));
/*3*/		  	  vecColumnas.addElement((rs.getString("FN_INTERES")==null)?"":rs.getString("FN_INTERES"));
/*4*/		  	  vecColumnas.addElement((rs.getString("FN_INTERESM")==null)?"":rs.getString("FN_INTERESM"));
/*5*/		  	  vecColumnas.addElement((rs.getString("FN_INTERES_PAGADO")==null)?"":rs.getString("FN_INTERES_PAGADO"));
/*6*/		  	  vecColumnas.addElement((rs.getString("FN_INTERESM_PAGADO")==null)?"":rs.getString("FN_INTERESM_PAGADO"));
/*7*/		  	  vecColumnas.addElement((rs.getString("FN_CAPITAL_PAGADO")==null)?"":rs.getString("FN_CAPITAL_PAGADO"));
/*8*/		  	  vecColumnas.addElement((rs.getString("CD_DESCRIPCION")==null)?"":rs.getString("CD_DESCRIPCION"));
/*9*/		  	  vecColumnas.addElement((rs.getString("FECHA_PAGO2")==null)?"":rs.getString("FECHA_PAGO2"));
/*10*/		  	  vecColumnas.addElement((rs.getString("FN_SALDO_INICIAL")==null)?"":rs.getString("FN_SALDO_INICIAL"));
	 			  vecFilas.addElement(vecColumnas);
              }
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return vecFilas;
      } // getDeatalleMensualidad_INV

  public Vector getDeatallePago_INV(String cc_disposicion, String ic_mensualidad)
  throws NafinException
  {
  String qrySentencia = "";
  ResultSet rs = null;
  AccesoDB  con = null;

  Vector  vecFilas		= new Vector();
  Vector  vecColumnas	= null;

    try {
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia = " SELECT NVL('Pago con Doc. '||D.ig_numero_docto,'Pago externo') as CONCEPTO, TO_CHAR(P.df_aplicacion,'DD-MON-YYYY') AS FECHA_APLICACION, "+
					"	   P.fn_monto_pago, P.fn_saldom_x_pagar, P.fn_interesm_generado, P.fn_interes_generado, "+
					"	   P.fn_saldo_capitald "+
					" FROM inv_pago P, com_documento D "+
					" WHERE P.ic_mensualidad = "+ ic_mensualidad+
					" AND P.cc_disposicion = "+ cc_disposicion+
					" AND D.ic_documento (+)= P.ic_documento"+
					" ORDER BY ic_pago ";

	  System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      while(rs.next())
		{
              vecColumnas = new Vector();
/*0*/		  	  vecColumnas.addElement((rs.getString("CONCEPTO")==null)?"":rs.getString("CONCEPTO"));
/*1*/		  	  vecColumnas.addElement((rs.getString("FECHA_APLICACION")==null)?"":rs.getString("FECHA_APLICACION"));
/*2*/		  	  vecColumnas.addElement((rs.getString("FN_MONTO_PAGO")==null)?"":rs.getString("FN_MONTO_PAGO"));
/*3*/		  	  vecColumnas.addElement((rs.getString("FN_SALDOM_X_PAGAR")==null)?"":rs.getString("FN_SALDOM_X_PAGAR"));
/*4*/		  	  vecColumnas.addElement((rs.getString("FN_INTERESM_GENERADO")==null)?"":rs.getString("FN_INTERESM_GENERADO"));
/*5*/		  	  vecColumnas.addElement((rs.getString("FN_INTERES_GENERADO")==null)?"":rs.getString("FN_INTERES_GENERADO"));
/*6*/		  	  vecColumnas.addElement((rs.getString("FN_SALDO_CAPITALD")==null)?"":rs.getString("FN_SALDO_CAPITALD"));
	 			  vecFilas.addElement(vecColumnas);
              }
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return vecFilas;
      } // getDeatallePago_INV

	public Vector consultaLC(String ic_linea_credito, String ic_producto, String ic_epo)
  	throws NafinException
    {
    String		qrySentencia	= "";
    ResultSet	rs 						= null;
    AccesoDB	con						= null;
    Vector		vecColumnas		= new Vector();
    Vector		vecFilas			= new Vector();
    try {

      con = new AccesoDB();
      con.conexionDB();
	  System.out.println("consultaLC");
	  if(!ic_linea_credito.equals(""))
	  {
	      	qrySentencia =	"  SELECT F.cd_nombre, LC.cg_numero_cuenta, LC.ic_plazo, LC.ic_estatus_linea,"   +
							"  	   LC.fn_monto_solicitado, LC.fn_monto_autorizado, LC.fn_monto_autorizado_total, "   +
							"		LC.ic_financiera, DECODE(PRE.cs_linea_individual, NULL, pn.fn_linea_automatica, PRE.fn_linea_automatica ) as fn_linea_automatica, "+
							"	 	NVL(PRE.cs_linea_individual, pn.cs_linea_individual) AS cs_linea_individual, "+
							"		LC.fn_linea_opcional,LC.fn_linea_individual, LC.ic_moneda, LC.cg_cuenta_clabe "+
							"  FROM com_linea_credito LC, comcat_financiera F, comrel_producto_epo PRE, comcat_producto_nafin pn "   +
							"  WHERE ic_linea_credito = "   +ic_linea_credito+
							"  AND LC.ic_financiera = F.ic_financiera"+
							"  AND PRE.ic_producto_nafin = "   + ic_producto +
							"  AND LC.ic_epo = PRE.ic_epo"  +
							"  AND pre.ic_producto_nafin = pn.ic_producto_nafin " ;

//				System.out.println(qrySentencia);
				rs = con.queryDB(qrySentencia);
				while(rs.next())
					{
			        vecColumnas = new Vector();
	/*0*/				vecColumnas.addElement((rs.getString("CD_NOMBRE")==null)?"":rs.getString("CD_NOMBRE"));
	/*1*/				vecColumnas.addElement((rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA"));
	/*2*/				vecColumnas.addElement((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
	/*3*/				vecColumnas.addElement((rs.getString("IC_ESTATUS_LINEA")==null)?"":rs.getString("IC_ESTATUS_LINEA"));
	/*4*/				vecColumnas.addElement((rs.getString("FN_MONTO_SOLICITADO")==null)?"":rs.getString("FN_MONTO_SOLICITADO"));
	/*5*/        		vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?"":rs.getString("FN_MONTO_AUTORIZADO"));
	/*6*/		        vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
	/*7*/				vecColumnas.addElement((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
	/*8*/		        vecColumnas.addElement((rs.getString("FN_LINEA_AUTOMATICA")==null)?"":rs.getString("FN_LINEA_AUTOMATICA"));
	/*9*/		        vecColumnas.addElement((rs.getString("CS_LINEA_INDIVIDUAL")==null)?"":rs.getString("CS_LINEA_INDIVIDUAL"));
	/*10*/ 				vecColumnas.addElement(defLineaMax);
	/*11*/		        vecColumnas.addElement((rs.getString("FN_LINEA_OPCIONAL")==null)?"":rs.getString("FN_LINEA_OPCIONAL"));
	/*12*/		        vecColumnas.addElement((rs.getString("FN_LINEA_INDIVIDUAL")==null)?"":rs.getString("FN_LINEA_INDIVIDUAL"));
	/*13*/		        vecColumnas.addElement((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
	/*14*/				vecColumnas.addElement((rs.getString("CG_CUENTA_CLABE")==null)?"":rs.getString("CG_NUMERO_CUENTA"));

	      			vecFilas.addElement(vecColumnas);
			    }
		}
		else
		{
	      	qrySentencia =	"  SELECT DECODE(PRE.cs_linea_individual, NULL, pn.fn_linea_automatica, PRE.fn_linea_automatica ) as fn_linea_automatica,  "+
							"	NVL(PRE.cs_linea_individual, pn.cs_linea_individual) AS cs_linea_individual " +
							"  FROM comrel_producto_epo PRE, comcat_producto_nafin pn "   +
							"  WHERE PRE.ic_producto_nafin = "   + ic_producto +
							"  AND PRE.ic_epo = "+ic_epo  +
							"  AND pre.ic_producto_nafin = pn.ic_producto_nafin " ;

				rs = con.queryDB(qrySentencia);
				while(rs.next())
					{
			        vecColumnas = new Vector();
	/*0*/		        vecColumnas.addElement((rs.getString("FN_LINEA_AUTOMATICA")==null)?"":rs.getString("FN_LINEA_AUTOMATICA"));
	/*1*/		        vecColumnas.addElement((rs.getString("CS_LINEA_INDIVIDUAL")==null)?"":rs.getString("CS_LINEA_INDIVIDUAL"));
	/*2*/ 				vecColumnas.addElement(defLineaMax);

	      			vecFilas.addElement(vecColumnas);
			    }
		}
//	System.out.println(qrySentencia);

    }catch(Exception e){
		System.out.println("Exception en consultaLC::"+e.toString());
    	throw new NafinException("SIST0001");
    } finally {
    	if(con.hayConexionAbierta())
      	con.cierraConexionDB();
		System.out.println("LineaCreditoBean::consultaLC(S)");
    }
  return vecFilas;
	}	//consultaLC


public Vector setLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					   String banco, String cuenta, String plazo, String estatus,
					   String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					   String monto_autorizado, String linea_automatica, String linea_individual,
					   String credito_adicional, String linea_total, String ic_linea,
					   String ic_producto, String ic_moneda, String cuenta_clabe)
  	throws NafinException
    {
    String		qrySentencia	= "";
    ResultSet	rs 				= null;
    AccesoDB	con				= null;
	String      proceso			= "";
	String      linCredito		= "";
	String      mensaje			= "";
	String      Dias			= "0";
	String 		Disposicion     = "0";
    Vector		vecColumnas		= new Vector();
    Vector		vecFilasP		= null;
    Vector		vecColumnasP	= null;
    boolean		resultado		= true;

    try {

      con = new AccesoDB();
      con.conexionDB();
	  System.out.println("setLCtmp");

//		System.out.println("ic_proceso["+ic_proceso+"]");

	  if(ic_proceso.equals(""))
	  {
              qrySentencia = "  SELECT NVL(MAX(ic_proceso),0)+1 "+
							 " FROM comtmp_linea_credito ";

			rs = con.queryDB(qrySentencia);
			if(rs.next())
				proceso = rs.getString(1);
		}
		else
			proceso = ic_proceso;

   			vecColumnas.addElement(proceso);

		System.out.println("proceso["+proceso+"]");

		// validar q no se haya dado de alta una linea
		if(!tipo_solicitud.equals("I"))
		{
	        qrySentencia = " SELECT ic_tmp_linea_credito"   +
						" FROM comtmp_linea_credito"   +
						" WHERE ic_proceso = "+ proceso ;
			if(!ic_linea.equals(""))
			 qrySentencia += " AND ic_linea_credito_padre = " + ic_linea  ;

//			System.out.println(qrySentencia);
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				mensaje = "SI";
			else
				mensaje = "NO";
		}
		else // Valida que el proveedor no tenga otra l�nea inicial
		{
	        qrySentencia = " SELECT ic_tmp_linea_credito"   +
						" FROM comtmp_linea_credito"   +
						" WHERE ic_proceso = "   + proceso +
						" AND ic_pyme = "   + ic_pyme +
						" AND cg_tipo_solicitud = 'I'"  ;
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				mensaje = "SII";
			else
				mensaje = "NO";

		}

		if(mensaje.equals("NO"))
		{

	        qrySentencia = " SELECT NVL(MAX(ic_tmp_linea_credito),0)+1"   +
							" FROM comtmp_linea_credito" ;
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				linCredito = rs.getString(1);

//			System.out.println("linCredito["+linCredito+"]");


			if(plazo.equals("")) 			 plazo = "null";
			else{
	        qrySentencia = " SELECT  in_plazo_dias "+
							" FROM comcat_plazo "+
							" WHERE ic_plazo = "+plazo;
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				Dias = rs.getString(1);
			}

			if(monto_solicitado.equals(""))  monto_solicitado = "null";
			if(linea_automatica.equals(""))  linea_automatica = "null";
			if(linea_individual.equals(""))  linea_individual = "null";
			if(monto_autorizado.equals(""))  monto_autorizado = "null";
			if(credito_adicional.equals("")) credito_adicional = "null";
			if(linea_total.equals(""))  	 linea_total = "null";
			if(ic_linea.equals(""))  	 	 ic_linea = "null";
			if(banco.equals(""))  	 	 	 banco = "null";
			if(ic_moneda.equals(""))  	 	 ic_moneda = "null";

			if(ic_producto.equals("5"))
			{
				vecFilasP = getDispVig(ic_producto,ic_if);

			    for(int i=0;i<vecFilasP.size();i++)
				{
					vecColumnasP 			= (Vector)vecFilasP.get(i);
					Disposicion				= (String)vecColumnasP.get(0);
				}
			}

              qrySentencia =  " insert into comtmp_linea_credito "+
							" (ic_tmp_linea_credito, ic_proceso, ic_pyme, ic_if, ic_epo, ic_financiera, ic_plazo, "+
							" ic_estatus_linea, cg_tipo_solicitud, df_captura, cg_causa_rechazo, fn_monto_solicitado, "+
							" fn_linea_automatica, fn_linea_individual, fn_monto_autorizado, fn_linea_opcional, " +
							" fn_monto_autorizado_total, ic_linea_credito_padre, ic_producto_nafin, cg_numero_cuenta, "+
							" cg_cuenta_clabe, df_vencimiento, ic_moneda ";
							if(ic_producto.equals("5"))
								qrySentencia += ", df_limite_disposicion ";
							qrySentencia += " )"+
							" values "+
	                        " ("+ linCredito +
						  	" ,"+ proceso +
						  	" ,"+ ic_pyme +
						  	" ,"+ ic_if +
						  	" ,"+ ic_epo +
						  	" ,"+ banco +
						  	" ,"+ plazo +
						  	" ,"+ estatus +
						  	" ,'" + tipo_solicitud + "'" +
						  	" , sysdate " +
						  	" ,'"+ causa_rechazo + "'" +
						  	" ,"+ monto_solicitado +
						  	" ," + linea_automatica +
						  	" ," + linea_individual +
						  	" ," + linea_total +
						  	" ," + credito_adicional +
						  	" ," + linea_total +
							" ," + ic_linea +
							" ," + ic_producto +
							" ,'" + cuenta +"'"+
							" ,'" + cuenta_clabe +"'"+
							" , ADD_MONTHS(SYSDATE," +Disposicion+ ")+" + Dias +
							" , "+ ic_moneda ;
							if(ic_producto.equals("5"))
								qrySentencia += " , ADD_MONTHS(SYSDATE," +Disposicion+ ")" ;
							qrySentencia += ")";
//					System.out.println(qrySentencia);
							con.ejecutaSQL(qrySentencia);

		}
//		System.out.println("mensaje["+mensaje+"]");
		vecColumnas.addElement(mensaje);

    } catch(Exception e) {
      System.out.println("LineaCredito.setLCtmp Exception"+e);
	  resultado = false;
      throw new NafinException("ANTI0020");
    } finally {
			if(con.hayConexionAbierta())
      	{
	      con.terminaTransaccion(resultado);
	      con.cierraConexionDB();
        }
     }
	 return vecColumnas;
  }         //setLCtmp

/*	OJO: Se dejo de usar por el FODA 99 (MPCS)

public Vector consultaLCtmp(String ic_producto, String ic_proceso, String ic_if)
  	throws NafinException
    {
    String 		lineasCredito	= "";
    String		qrySentencia	= "";
	String 		strAux			= "";
    ResultSet	rs 				= null;
    AccesoDB	con				= null;
    Vector		vecColumnas		= new Vector();
    Vector		vecFilas		= new Vector();
    int i=0;

    try {

      con = new AccesoDB();
      con.conexionDB();
	  System.out.println("consultaLCtmp");

      	qrySentencia =	" SELECT PN.ic_nombre, LCT.ic_linea_credito_padre, PE.cg_pyme_epo_interno, P.cg_razon_social, E.cd_nombre,"   +
						" 	   D.cg_calle ||' '|| D.cg_numero_ext ||' '|| D.cg_numero_int ||' '|| D.cg_colonia AS DIRECCION, D.cg_telefono1,"   +
						" 	   DECODE(LCT.cg_tipo_solicitud,'A', 'AMPLIACION','R','RENOVACION','I','INICIAL') as TIPO_SOLICITUD, " +
						"	   TO_CHAR(LCT.df_captura,'dd/mm/yyyy') AS FECHA_SOLICITUD,"   +
						" 	   TO_CHAR(SYSDATE,'dd/mm/yyyy') AS FECHA_OPERACION, LCT.fn_monto_solicitado, VE.epo_rel,"   +
						" 	   (SELECT SUM(fn_monto_autorizado_total)"   +
						" 	   	  FROM COM_LINEA_CREDITO lc2"   +
						" 	   	  WHERE cg_tipo_solicitud <> 'A'"   +
						" 	   	  AND df_vencimiento_adicional > SYSDATE"   +
						" 	   	  AND ic_if NOT IN("+ic_if+") AND LCT.ic_pyme = lc2.ic_pyme) AS LINEAS_OTROS,"   +
						" 	   EL.cd_descripcion, LCT.cg_causa_rechazo, CP.in_plazo_dias as PLAZO, TO_CHAR(LCT.df_vencimiento,'dd/mm/yyyy') AS FECHA_VENCIMIENTO,"   +
						" 	   F.cd_nombre AS BANCO, LCT.cg_numero_cuenta, DECODE(PRE.cs_linea_individual,null,decode(pn.cs_linea_individual,'S',null,pn.fn_linea_automatica),'N',PRE.fn_linea_automatica,null) fn_linea_automatica,"   +
						" 	   LCT.fn_linea_opcional,LCT.fn_linea_individual,LCT.fn_monto_autorizado_total, "   +
						"	   LCT.ic_tmp_linea_credito, LCT.ic_proceso, LCT.ic_producto_nafin, LCT.ic_epo, LCT.ic_pyme, "+
						"	   LCT.ic_financiera, LCT.cg_tipo_solicitud, LCT.ic_estatus_linea, LCT.fn_monto_autorizado, "+
						"	   LCT.ic_plazo,NVL(PRE.cs_linea_individual,'"+defLineaInd+"') as cs_linea_individual, LCT.ic_moneda, "+
						"	   to_char(LCT.DF_LIMITE_DISPOSICION,'DD/MM/YYYY') as DF_LIMITE_DISPOSICION, M.cd_nombre as MONEDA, "+
						"	   P.in_numero_sirac, LCT.cg_cuenta_clabe "+
						" FROM COMTMP_LINEA_CREDITO LCT, COMCAT_PRODUCTO_NAFIN PN, COMCAT_PYME P, COM_DOMICILIO D, COMCAT_ESTADO E,"   +
						"     (SELECT COUNT(pexp.ic_epo) AS EPO_REL"   +
						"     , pexp.ic_pyme"   +
						"     , iexp.ic_if"   +
						"     FROM COMREL_PYME_EPO_X_PRODUCTO pexp,"   +
						"  	  COMREL_IF_EPO_X_PRODUCTO iexp"   +
						"     WHERE pexp.cs_habilitado IN('S','H')"   +
//						"	   AND pexp.ic_producto_nafin =   "   + ic_producto +"
						" 	   AND  pexp.ic_epo = iexp.ic_epo"   +
//						" 	   AND  iexp.ic_producto_nafin =  "   + ic_producto +
						" 	   AND iexp.cs_habilitado IN('S')"   +
						" 	   AND  iexp.ic_producto_nafin = pexp.ic_producto_nafin"   +
						" 	   GROUP BY pexp.ic_pyme, iexp.ic_if ) VE,"   +
						" 	  COMCAT_ESTATUS_LINEA EL, COMCAT_FINANCIERA F, COMREL_PRODUCTO_EPO PRE, "   +
						" 	  COMREL_PYME_EPO PE, COMREL_PYME_EPO_X_PRODUCTO PP, COMCAT_PLAZO CP, COMCAT_MONEDA M"   +
						" WHERE LCT.ic_proceso = "   + ic_proceso +
						" AND LCT.ic_producto_nafin = PN.ic_producto_nafin"   +
						" AND LCT.ic_pyme = P.ic_pyme"   +
						" AND LCT.ic_pyme = D.ic_pyme"   +
						" AND D.cs_fiscal = 'S'"   +
						" AND D.ic_estado = E.ic_estado"   +
						" AND LCT.ic_pyme = VE.ic_pyme(+)"   +
						" AND LCT.ic_if = VE.ic_if(+)"   +
						" AND LCT.ic_estatus_linea = EL.ic_estatus_linea"   +
						" AND LCT.ic_financiera = F.ic_financiera(+) "   +
//						" AND PRE.ic_producto_nafin = "   + ic_producto +
						" AND PRE.cs_habilitado = 'S'"   +
						" AND LCT.ic_plazo = CP.ic_plazo(+) "+
						" AND LCT.ic_epo = PRE.ic_epo"   +
						" AND PE.ic_pyme = PP.ic_pyme"   +
						" AND PE.ic_epo = PP.ic_epo"   +
						" AND PP.ic_pyme = LCT.ic_pyme"   +
						" AND PP.ic_epo = LCT.ic_epo"   +
						" AND PE.cs_habilitado = 'S'"   +
						" AND PP.cs_habilitado IN('S','H')"   +
//						" AND PP.ic_producto_nafin = "+ ic_producto +
						" AND LCT.ic_moneda = M.ic_moneda(+) " +
						" AND LCT.ic_producto_nafin = PP.ic_producto_nafin " +
						" AND LCT.ic_producto_nafin = PRE.ic_producto_nafin";

//			System.out.println(qrySentencia);
			rs = con.queryDB(qrySentencia);
			while(rs.next())
				{
		        vecColumnas = new Vector();
/*0*//*				vecColumnas.addElement((rs.getString("IC_NOMBRE")==null)?"":rs.getString("IC_NOMBRE"));
/*1*//*				vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO_PADRE")==null)?"":rs.getString("IC_LINEA_CREDITO_PADRE"));
/*2*//*				vecColumnas.addElement((rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO"));
/*3*//*				vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
/*4*//*				vecColumnas.addElement((rs.getString("CD_NOMBRE")==null)?"":rs.getString("CD_NOMBRE"));
/*5*//*				vecColumnas.addElement((rs.getString("DIRECCION")==null)?"":rs.getString("DIRECCION"));
/*6*//*				vecColumnas.addElement((rs.getString("CG_TELEFONO1")==null)?"":rs.getString("CG_TELEFONO1"));
/*7*//*				vecColumnas.addElement((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*8*//*				vecColumnas.addElement((rs.getString("FECHA_SOLICITUD")==null)?"":rs.getString("FECHA_SOLICITUD"));
/*9*//*				vecColumnas.addElement((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*10*//*				vecColumnas.addElement((rs.getString("FN_MONTO_SOLICITADO")==null)?"":rs.getString("FN_MONTO_SOLICITADO"));
/*11*//*				vecColumnas.addElement((rs.getString("EPO_REL")==null)?"":rs.getString("EPO_REL"));
/*12*//*				vecColumnas.addElement((rs.getString("LINEAS_OTROS")==null)?"":rs.getString("LINEAS_OTROS"));
/*13*//*				vecColumnas.addElement((rs.getString("CD_DESCRIPCION")==null)?"":rs.getString("CD_DESCRIPCION"));
/*14*//*				vecColumnas.addElement((rs.getString("CG_CAUSA_RECHAZO")==null)?"":rs.getString("CG_CAUSA_RECHAZO"));
/*15*//*				vecColumnas.addElement((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*16*//*				vecColumnas.addElement((rs.getString("FECHA_VENCIMIENTO")==null)?"":rs.getString("FECHA_VENCIMIENTO"));
/*17*//*				vecColumnas.addElement((rs.getString("BANCO")==null)?"":rs.getString("BANCO"));
/*18*//*				vecColumnas.addElement((rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA"));
/*19*//*				vecColumnas.addElement((rs.getString("FN_LINEA_AUTOMATICA")==null)?"":rs.getString("FN_LINEA_AUTOMATICA"));
/*20*//*				vecColumnas.addElement((rs.getString("FN_LINEA_OPCIONAL")==null)?"":rs.getString("FN_LINEA_OPCIONAL"));
/*21*//*				vecColumnas.addElement((rs.getString("FN_LINEA_INDIVIDUAL")==null)?"":rs.getString("FN_LINEA_INDIVIDUAL"));
/*22*//*				vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
/*23*//*				vecColumnas.addElement((rs.getString("IC_TMP_LINEA_CREDITO")==null)?"":rs.getString("IC_TMP_LINEA_CREDITO"));
/*24*//*				vecColumnas.addElement((rs.getString("IC_PROCESO")==null)?"":rs.getString("IC_PROCESO"));
/*25*//*				vecColumnas.addElement((rs.getString("IC_PRODUCTO_NAFIN")==null)?"":rs.getString("IC_PRODUCTO_NAFIN"));
/*26*//*				vecColumnas.addElement((rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO"));
/*27*//*				vecColumnas.addElement((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*28*//*				vecColumnas.addElement((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
/*29*//*				vecColumnas.addElement((rs.getString("CG_TIPO_SOLICITUD")==null)?"":rs.getString("CG_TIPO_SOLICITUD"));
/*30*//*				vecColumnas.addElement((rs.getString("IC_ESTATUS_LINEA")==null)?"":rs.getString("IC_ESTATUS_LINEA"));
/*31*//*				vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?"":rs.getString("FN_MONTO_AUTORIZADO"));
/*32*//*				vecColumnas.addElement((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
/*33*//*				vecColumnas.addElement((rs.getString("CS_LINEA_INDIVIDUAL")==null)?"":rs.getString("CS_LINEA_INDIVIDUAL"));
/*34*//*				vecColumnas.addElement((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*35*//*				vecColumnas.addElement((rs.getString("DF_LIMITE_DISPOSICION")==null)?"":rs.getString("DF_LIMITE_DISPOSICION"));
/*36*//*				vecColumnas.addElement((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
					strAux =	(rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
			       	while(strAux.length()<7)
								strAux = "0"+strAux;
			         strAux = (String)vecColumnas.get(29) +strAux +
					(String)vecColumnas.get(1);
/*37*//*				vecColumnas.addElement(strAux);     		//folioArmado
/*38*//*				vecColumnas.addElement((rs.getString("CG_CUENTA_CLABE")==null)?"":rs.getString("CG_CUENTA_CLABE"));
//			System.out.println("["+rs.getString("CG_CUENTA_CLABE")+"]");
      			vecFilas.addElement(vecColumnas);
		    }
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    } finally {
    	if(con.hayConexionAbierta())
      	con.cierraConexionDB();
    }
  return vecFilas;
	}	//consultaLCtmp

*/
 public Vector consultaLCtmp(String ic_producto, String ic_proceso, String ic_if)
		throws NafinException {
	System.out.println("consultaLCtmp");			
	String		qrySentencia	= "";
	String 		strAux			= "";
	ResultSet	rs 				= null;
	AccesoDB	con				= null;
	Vector		vecColumnas		= new Vector();
	Vector		vecFilas		= new Vector();

	try {
		con = new AccesoDB();
		con.conexionDB();

      	qrySentencia =	
			" SELECT PN.ic_nombre, LCT.ic_linea_credito_padre, PE.cg_pyme_epo_interno, P.cg_razon_social, E.cd_nombre,"   +
			" 	   D.cg_calle ||' '|| D.cg_numero_ext ||' '|| D.cg_numero_int ||' '|| D.cg_colonia AS DIRECCION, D.cg_telefono1,"   +
			" 	   DECODE(LCT.cg_tipo_solicitud,'A', 'AMPLIACION','R','RENOVACION','I','INICIAL') as TIPO_SOLICITUD, " +
			"	   TO_CHAR(LCT.df_captura,'dd/mm/yyyy') AS FECHA_SOLICITUD,"   +
			" 	   TO_CHAR(SYSDATE,'dd/mm/yyyy') AS FECHA_OPERACION, LCT.fn_monto_solicitado, VE.epo_rel,"   +
			" 	   (SELECT SUM(fn_monto_autorizado_total)"   +
			" 	   	  FROM COM_LINEA_CREDITO lc2"   +
			" 	   	  WHERE cg_tipo_solicitud <> 'A'"   +
			" 	   	  AND df_vencimiento_adicional > SYSDATE"   +
			" 	   	  AND ic_if NOT IN("+ic_if+") AND LCT.ic_pyme = lc2.ic_pyme) AS LINEAS_OTROS,"   +
			" 	   EL.cd_descripcion, LCT.cg_causa_rechazo, CP.in_plazo_dias as PLAZO, TO_CHAR(LCT.df_vencimiento,'dd/mm/yyyy') AS FECHA_VENCIMIENTO,"   +
			" 	   F.cd_nombre AS BANCO, LCT.cg_numero_cuenta, DECODE(PRE.cs_linea_individual,null,decode(pn.cs_linea_individual,'S',null,pn.fn_linea_automatica),'N',PRE.fn_linea_automatica,null) fn_linea_automatica,"   +
			" 	   LCT.fn_linea_opcional,LCT.fn_linea_individual,LCT.fn_monto_autorizado_total, "   +
			"	   LCT.ic_tmp_linea_credito, LCT.ic_proceso, LCT.ic_producto_nafin, LCT.ic_epo, LCT.ic_pyme, "+
			"	   LCT.ic_financiera, LCT.cg_tipo_solicitud, LCT.ic_estatus_linea, LCT.fn_monto_autorizado, "+
			"	   LCT.ic_plazo,NVL(PRE.cs_linea_individual,'"+defLineaInd+"') as cs_linea_individual, LCT.ic_moneda, "+
			"	   to_char(LCT.DF_LIMITE_DISPOSICION,'DD/MM/YYYY') as DF_LIMITE_DISPOSICION, M.cd_nombre as MONEDA, "+
			"	   P.in_numero_sirac, LCT.cg_cuenta_clabe, P.in_numero_troya "+
			"	   , LCT.ig_numero_solic_troya, to_char(LCT.df_autorizacion,'dd/mm/yyyy') df_autorizacion "+
			"	   , decode(LCT.cg_linea_tipo, 'E', 'Credito de Exportacion','Tradicional') linea_tipo "+
			"	   , tam.cd_descripcion tabla_amort, LCT.ig_numero_max_amort_exp  "+
			"	   , LCT.cg_linea_tipo, tam.ic_tabla_amort"+
			" FROM COMTMP_LINEA_CREDITO LCT, COMCAT_PRODUCTO_NAFIN PN, COMCAT_PYME P, COM_DOMICILIO D, COMCAT_ESTADO E,"   +
			"     (SELECT COUNT(pexp.ic_epo) AS EPO_REL"   +
			"     , pexp.ic_pyme"   +
			"     , iexp.ic_if"   +
			"     FROM COMREL_PYME_EPO_X_PRODUCTO pexp,"   +
			"  	  COMREL_IF_EPO_X_PRODUCTO iexp"   +
			"     WHERE pexp.cs_habilitado IN('S','H')"   +
//			"	   AND pexp.ic_producto_nafin =   "   + ic_producto +"
			" 	   AND  pexp.ic_epo = iexp.ic_epo"   +
//			" 	   AND  iexp.ic_producto_nafin =  "   + ic_producto +
			" 	   AND iexp.cs_habilitado IN('S')"   +
			" 	   AND  iexp.ic_producto_nafin = pexp.ic_producto_nafin"   +
			" 	   GROUP BY pexp.ic_pyme, iexp.ic_if ) VE,"   +
			" 	  COMCAT_ESTATUS_LINEA EL, COMCAT_FINANCIERA F, COMREL_PRODUCTO_EPO PRE, "   +
			" 	  COMREL_PYME_EPO PE, COMREL_PYME_EPO_X_PRODUCTO PP, COMCAT_PLAZO CP, COMCAT_MONEDA M"   +
			"	  ,comcat_tabla_amort tam"+
			" WHERE LCT.ic_proceso = "   + ic_proceso +
			" AND LCT.ic_producto_nafin = PN.ic_producto_nafin"   +
			" AND LCT.ic_pyme = P.ic_pyme"   +
			" AND LCT.ic_pyme = D.ic_pyme"   +
			" AND D.cs_fiscal = 'S'"   +
			" AND D.ic_estado = E.ic_estado"   +
			" AND LCT.ic_pyme = VE.ic_pyme(+)"   +
			" AND LCT.ic_if = VE.ic_if(+)"   +
			" AND LCT.ic_estatus_linea = EL.ic_estatus_linea"   +
			" AND LCT.ic_financiera = F.ic_financiera(+) "   +
//			" AND PRE.ic_producto_nafin = "   + ic_producto +
			" AND PRE.cs_habilitado = 'S'"   +
			" AND LCT.ic_plazo = CP.ic_plazo(+) "+
			" AND LCT.ic_epo = PRE.ic_epo"   +
			" AND PE.ic_pyme = PP.ic_pyme"   +
			" AND PE.ic_epo = PP.ic_epo"   +
			" AND PP.ic_pyme = LCT.ic_pyme"   +
			" AND PP.ic_epo = LCT.ic_epo"   +
			" AND PE.cs_habilitado = 'S'"   +
			" AND PP.cs_habilitado IN('S','H')"   +
//			" AND PP.ic_producto_nafin = "+ ic_producto +
			" AND LCT.ic_moneda = M.ic_moneda(+) " +
			" AND LCT.ic_producto_nafin = PP.ic_producto_nafin " +
			" AND LCT.ic_producto_nafin = PRE.ic_producto_nafin"+
			" AND LCT.ic_tabla_amort = tam.ic_tabla_amort";

//			System.out.println(qrySentencia);
			rs = con.queryDB(qrySentencia);
			while(rs.next())
				{
		        vecColumnas = new Vector();
/*0*/				vecColumnas.addElement((rs.getString("IC_NOMBRE")==null)?"":rs.getString("IC_NOMBRE"));
/*1*/				vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO_PADRE")==null)?"":rs.getString("IC_LINEA_CREDITO_PADRE"));
/*2*/				vecColumnas.addElement((rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO"));
/*3*/				vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
/*4*/				vecColumnas.addElement((rs.getString("CD_NOMBRE")==null)?"":rs.getString("CD_NOMBRE"));
/*5*/				vecColumnas.addElement((rs.getString("DIRECCION")==null)?"":rs.getString("DIRECCION"));
/*6*/				vecColumnas.addElement((rs.getString("CG_TELEFONO1")==null)?"":rs.getString("CG_TELEFONO1"));
/*7*/				vecColumnas.addElement((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*8*/				vecColumnas.addElement((rs.getString("FECHA_SOLICITUD")==null)?"":rs.getString("FECHA_SOLICITUD"));
/*9*/				vecColumnas.addElement((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*10*/				vecColumnas.addElement((rs.getString("FN_MONTO_SOLICITADO")==null)?"":rs.getString("FN_MONTO_SOLICITADO"));
/*11*/				vecColumnas.addElement((rs.getString("EPO_REL")==null)?"":rs.getString("EPO_REL"));
/*12*/				vecColumnas.addElement((rs.getString("LINEAS_OTROS")==null)?"":rs.getString("LINEAS_OTROS"));
/*13*/				vecColumnas.addElement((rs.getString("CD_DESCRIPCION")==null)?"":rs.getString("CD_DESCRIPCION"));
/*14*/				vecColumnas.addElement((rs.getString("CG_CAUSA_RECHAZO")==null)?"":rs.getString("CG_CAUSA_RECHAZO"));
/*15*/				vecColumnas.addElement((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*16*/				vecColumnas.addElement((rs.getString("FECHA_VENCIMIENTO")==null)?"":rs.getString("FECHA_VENCIMIENTO"));
/*17*/				vecColumnas.addElement((rs.getString("BANCO")==null)?"":rs.getString("BANCO"));
/*18*/				vecColumnas.addElement((rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA"));
/*19*/				vecColumnas.addElement((rs.getString("FN_LINEA_AUTOMATICA")==null)?"":rs.getString("FN_LINEA_AUTOMATICA"));
/*20*/				vecColumnas.addElement((rs.getString("FN_LINEA_OPCIONAL")==null)?"":rs.getString("FN_LINEA_OPCIONAL"));
/*21*/				vecColumnas.addElement((rs.getString("FN_LINEA_INDIVIDUAL")==null)?"":rs.getString("FN_LINEA_INDIVIDUAL"));
/*22*/				vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
/*23*/				vecColumnas.addElement((rs.getString("IC_TMP_LINEA_CREDITO")==null)?"":rs.getString("IC_TMP_LINEA_CREDITO"));
/*24*/				vecColumnas.addElement((rs.getString("IC_PROCESO")==null)?"":rs.getString("IC_PROCESO"));
/*25*/				vecColumnas.addElement((rs.getString("IC_PRODUCTO_NAFIN")==null)?"":rs.getString("IC_PRODUCTO_NAFIN"));
/*26*/				vecColumnas.addElement((rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO"));
/*27*/				vecColumnas.addElement((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*28*/				vecColumnas.addElement((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
/*29*/				vecColumnas.addElement((rs.getString("CG_TIPO_SOLICITUD")==null)?"":rs.getString("CG_TIPO_SOLICITUD"));
/*30*/				vecColumnas.addElement((rs.getString("IC_ESTATUS_LINEA")==null)?"":rs.getString("IC_ESTATUS_LINEA"));
/*31*/				vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?"":rs.getString("FN_MONTO_AUTORIZADO"));
/*32*/				vecColumnas.addElement((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
/*33*/				vecColumnas.addElement((rs.getString("CS_LINEA_INDIVIDUAL")==null)?"":rs.getString("CS_LINEA_INDIVIDUAL"));
/*34*/				vecColumnas.addElement((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*35*/				vecColumnas.addElement((rs.getString("DF_LIMITE_DISPOSICION")==null)?"":rs.getString("DF_LIMITE_DISPOSICION"));
/*36*/				vecColumnas.addElement((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
					strAux =	(rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
			       	while(strAux.length()<7)
								strAux = "0"+strAux;
			         strAux = (String)vecColumnas.get(29) +strAux +
					(String)vecColumnas.get(1);
/*37*/				vecColumnas.addElement(strAux);     		//folioArmado
/*38*/				vecColumnas.addElement((rs.getString("CG_CUENTA_CLABE")==null)?"":rs.getString("CG_CUENTA_CLABE"));
/*39*/				vecColumnas.addElement((rs.getString("IG_NUMERO_SOLIC_TROYA")==null)?"":rs.getString("IG_NUMERO_SOLIC_TROYA"));
/*40*/				vecColumnas.addElement((rs.getString("DF_AUTORIZACION")==null)?"":rs.getString("DF_AUTORIZACION"));
/*41*/				vecColumnas.addElement((rs.getString("IN_NUMERO_TROYA")==null)?"":rs.getString("IN_NUMERO_TROYA"));
/*42*/				vecColumnas.addElement((rs.getString("linea_tipo")==null)?"":rs.getString("linea_tipo"));
/*43*/				vecColumnas.addElement((rs.getString("tabla_amort")==null)?"":rs.getString("tabla_amort"));
/*44*/				vecColumnas.addElement((rs.getString("ig_numero_max_amort_exp")==null)?"":rs.getString("ig_numero_max_amort_exp"));
/*45*/				vecColumnas.addElement((rs.getString("cg_linea_tipo")==null)?"":rs.getString("cg_linea_tipo"));
/*46*/				vecColumnas.addElement((rs.getString("ic_tabla_amort")==null)?"":rs.getString("ic_tabla_amort"));
//			System.out.println("["+rs.getString("CG_CUENTA_CLABE")+"]");
      			vecFilas.addElement(vecColumnas);
		    }
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    } finally {
    	if(con.hayConexionAbierta())
      	con.cierraConexionDB();
    }
  return vecFilas;
	}	//consultaLCtmp




public void delLCtmp(String ic_proceso, String ic_linea)
  	throws NafinException
    {
    String		qrySentencia	= "";
    AccesoDB	con				= null;
    boolean		resultado		= true;

    try {

      con = new AccesoDB();
      con.conexionDB();
	  System.out.println("delLCtmp");

      qrySentencia = "  DELETE comtmp_linea_credito"   +
					"  WHERE ic_tmp_linea_credito = "   + ic_linea +
					"  AND ic_proceso = " + ic_proceso ;


//		System.out.println(qrySentencia);
		con.ejecutaSQL(qrySentencia);


    } catch(Exception e) {
      System.out.println("LineaCredito.delLCtmp Exception"+e);
	  resultado = false;
      throw new NafinException("ANTI0020");
    } finally {
			if(con.hayConexionAbierta())
      	{
	      con.terminaTransaccion(resultado);
	      con.cierraConexionDB();
        }
     }
  }         //delLCtmp

public void updtLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					  String banco, String cuenta, String plazo, String estatus,
					  String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					  String monto_autorizado, String linea_automatica, String linea_individual,
					  String credito_adicional, String linea_total, String ic_linea,
					  String ic_producto, String linCredito, String ic_moneda, String cuenta_clabe)
  	throws NafinException
    {
    String		qrySentencia	= "";
    ResultSet	rs 				= null;
    AccesoDB	con				= null;
	String 		Dias			= "0";
	String 		Disposicion     = "0";
    Vector		vecFilasP		= null;
    Vector		vecColumnasP	= null;
    boolean		resultado		= true;

    try {

      con = new AccesoDB();
      con.conexionDB();
	  System.out.println("updtLCtmp");

  			if(plazo.equals("")) 			 plazo = "null";
			else{
	        qrySentencia = " SELECT  in_plazo_dias "+
							" FROM comcat_plazo "+
							" WHERE ic_plazo = "+plazo;
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				Dias = rs.getString(1);
			}

			if(plazo.equals("")) 			 plazo = "null";
			if(monto_solicitado.equals(""))  monto_solicitado = "null";
			if(linea_automatica.equals(""))  linea_automatica = "null";
			if(linea_individual.equals(""))  linea_individual = "null";
			if(monto_autorizado.equals(""))  monto_autorizado = "null";
			if(credito_adicional.equals("")) credito_adicional = "null";
			if(linea_total.equals(""))  	 linea_total = "null";
			if(ic_linea.equals(""))  	 	 ic_linea = "null";
			if(banco.equals(""))  	 	 	 banco = "null";
			if(ic_moneda.equals(""))  	 	 ic_moneda = "null";

			if(ic_producto.equals("5"))
			{
				vecFilasP = getDispVig(ic_producto,ic_if);

			    for(int i=0;i<vecFilasP.size();i++)
				{
					vecColumnasP 			= (Vector)vecFilasP.get(i);
					Disposicion				= (String)vecColumnasP.get(0);
				}
			}

              qrySentencia =  " update comtmp_linea_credito "+
							" set ic_pyme="+ ic_pyme +
							", ic_if="+ ic_if +
							", ic_epo="+ ic_epo +
							", ic_financiera="+ banco +
							", ic_plazo="+ plazo +
							", ic_estatus_linea="+ estatus +
							", cg_tipo_solicitud='" + tipo_solicitud + "'" +
							", df_captura = sysdate "+
							", cg_causa_rechazo='"+ causa_rechazo + "'" +
							", fn_monto_solicitado="+ monto_solicitado +
							", fn_linea_automatica=" + linea_automatica +
							", fn_linea_individual=" + linea_individual +
							", fn_monto_autorizado=" + linea_total +
							", fn_linea_opcional=" + credito_adicional +
							", fn_monto_autorizado_total=" + linea_total +
							", ic_linea_credito_padre=" + ic_linea +
							", ic_producto_nafin=" + ic_producto +
							", cg_numero_cuenta ='" + cuenta +"'"+
							", cg_cuenta_clabe ='" + cuenta_clabe +"'"+
							", df_vencimiento = ADD_MONTHS(SYSDATE," +Disposicion+ ")+" + Dias +
							", ic_moneda = "+ ic_moneda ;
							if(ic_producto.equals("5"))
								qrySentencia += ", df_limite_disposicion = ADD_MONTHS(SYSDATE," +Disposicion+ ")" ;
							qrySentencia += " where ic_tmp_linea_credito="+ linCredito +
							" and ic_proceso="+ ic_proceso ;

//					System.out.println(qrySentencia);
							con.ejecutaSQL(qrySentencia);


    } catch(Exception e) {
      System.out.println("LineaCredito.updtLCtmp Exception"+e);
	  resultado = false;
      throw new NafinException("ANTI0020");
    } finally {
			if(con.hayConexionAbierta())
      	{
	      con.terminaTransaccion(resultado);
	      con.cierraConexionDB();
        }
     }
  }         //updtLCtmp


    public Vector getIcPyme_NafinElec(String ic_epo, String ic_if, String ic_producto)
    	throws NafinException
    	{
		String		qrySentencia	= "";
      	AccesoDB	con				= null;
		ResultSet	rs				= null;
      	Vector 		vecFilas		= new Vector();
      	Vector		vecColumnas		= null;
		try {
						con = new AccesoDB();
            con.conexionDB();
       			qrySentencia =  "  SELECT P.ic_pyme, P.cg_razon_social, CN.ic_nafin_electronico"   +
								"  FROM  comcat_pyme P, comrel_pyme_epo PE , comrel_pyme_epo_x_producto  PEP ,"   +
								"  comcat_epo E, comrel_if_epo IE, comrel_if_epo_x_producto IEP, comrel_nafin CN"   +
								"  WHERE PE.ic_pyme=P.ic_pyme " 	 +
								"  AND PE.ic_epo= "   + ic_epo +
								"  AND PE.ic_pyme=PEP.ic_pyme"   +
								"  AND PE.ic_epo=PEP.ic_epo "   +
								"  AND PE.cs_habilitado='S' "   +
								"  AND P.cs_habilitado = 'S'"   +
								"  AND PEP.ic_producto_nafin=  "   + ic_producto +
								"  AND IE.ic_epo = E.ic_epo  "   +
								"  AND E.CS_HABILITADO = 'S'"   +
								"  AND IE.ic_if = "   + ic_if +
								"  AND IE.ic_if = IEP.ic_if  "   +
								"  AND IE.ic_epo = IEP.ic_epo"   +
								"  AND IEP.CS_HABILITADO IN ('S') "   +
								"  AND IEP.ic_producto_nafin =  "   + ic_producto +
								"  AND IE.cs_vobo_nafin = 'S'"   +
								"  AND IE.ic_epo = PE.ic_epo  "   +
								"  AND IEP.ic_producto_nafin = PEP.ic_producto_nafin"   +
								"  AND IEP.ic_epo = PEP.ic_epo "   +
								"  AND PEP.cs_habilitado IN ('S','H' )"   +
								"  AND CN.cg_tipo = 'P'"   +
								"  AND CN.ic_epo_pyme_if = P.ic_pyme"   ;
						rs = con.queryDB(qrySentencia);
						while(rs.next())
						{
			              vecColumnas = new Vector();
							vecColumnas.addElement((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
							vecColumnas.addElement((rs.getString("IC_NAFIN_ELECTRONICO")==null)?"":rs.getString("IC_NAFIN_ELECTRONICO"));
							vecFilas.addElement(vecColumnas);
						}
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return vecFilas;
      } // getIcPyme_NafinElec



public Vector getComboLineas(String ic_pyme,String ic_epo,String tipoSolicitud, String ic_producto_nafin, String ic_if)
throws NafinException
{
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        if("I".equals(tipoSolicitud)) {
          qrySentencia  =	"select a.ic_if, a.ic_if||' '||a.cg_razon_social,c.ic_epo,'' "+
				" from comcat_if a,"+
				" (select ic_if"+
				" from comcat_if"+
				" minus"+
				" select ic_if from "+
				" com_linea_credito"+
				" where ic_producto_nafin = " + ic_producto_nafin;
				if("5".equals(ic_producto_nafin))
					qrySentencia += " and  ic_estatus_linea in(1,2,3,7,9) ";
				else
					qrySentencia += " and  ic_estatus_linea in(1,2,3,5,7,9) ";
			qrySentencia +=
				" and ic_epo = " + ic_epo +
				" and ic_pyme= "+ic_pyme+" )  b,"+
                                " comrel_if_epo_x_producto c "+
				" where a.ic_if = b.ic_if"+
				" and a.cs_habilitado = 'S'"+
				" and c.ic_producto_nafin = "+ ic_producto_nafin +
				" and c.ic_if = a.ic_if"+
				" and c.cs_habilitado = 'S'"+
				" and c.ic_epo = "+ic_epo+
				" and a.ic_if ="+ic_if +
				" and a.ic_if in("+
				"		select distinct B.ic_if"+
				"		from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C,comcat_moneda D, comrel_pyme_if E "+
				"		where A.ic_epo = E.ic_epo and B.ic_if = E.ic_if and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria and "+
				"		C.ic_moneda = D.ic_moneda and E.cs_borrado = 'N'"+
				"		and E.cs_vobo_if = 'S' "+
				" 		and E.ic_epo = "+ic_epo+
				" 		and C.ic_pyme = "+ic_pyme+
				")";
	} else if("A".equals(tipoSolicitud)) {
        	qrySentencia  =	"select a.ic_linea_credito, a.ic_linea_credito||''||b.cg_razon_social,b.ic_if" +
				" from com_linea_credito a,comcat_if b" +
				" where a.ic_if = b.ic_if" +
				" and a.ic_pyme = " +ic_pyme +					//para la pyne
				" and a.ic_epo = "+ic_epo	+					//&iquest;para cualquier EPO?
				" and a.ic_if ="+ic_if +
				" and a.cg_tipo_solicitud in('I','R')" +		//debe ser inicial
				" and df_vencimiento_adicional > sysdate" +		//vigente
				" and a.cs_aceptacion_pyme = 'S'" +				//autorizada pyme
				" and b.cs_habilitado = 'S'"+
				" and a.ic_producto_nafin = "+ ic_producto_nafin +
				" and  ic_estatus_linea = 5 ";					//autorizada nafin
        } else if("R".equals(tipoSolicitud)) {
        	qrySentencia  =	"select a.ic_linea_credito, b.ic_if||''||b.cg_razon_social,b.ic_if" +
				" from com_linea_credito a,comcat_if b" +
				" where a.ic_if = b.ic_if" +
				" and a.ic_pyme = " +ic_pyme +				//para la pyne
				" and a.ic_epo = "+ic_epo	+				//&iquest;para cualquier EPO?
				" and a.ic_if ="+ic_if +
				" and a.cg_tipo_solicitud in('I','R')" +		//debe ser inicial
				" and df_vencimiento_adicional < sysdate" +		//vencida
				" and a.cs_aceptacion_pyme = 'S'" +				//autorizada pyme
				" and  ic_estatus_linea = 5 "+					//autorizada nafin
				" and b.cs_habilitado = 'S' "+
				" and a.ic_producto_nafin = "+ ic_producto_nafin +
				" and a.ic_linea_credito not in "+
				" ("+
				"	SELECT IC_LINEA_CREDITO "+
				"	FROM COM_LINEA_CREDITO"+
				"	WHERE "+
				"	(CG_TIPO_SOLICITUD = 'R' AND DF_VENCIMIENTO_ADICIONAL > sysdate)"+
				"	or"+
				"	(cg_tipo_solicitud = 'R' and ic_estatus_linea = 1)"+
				"	or ic_linea_credito in"+
				"		(select ic_linea_credito_padre from com_linea_credito"+
				"	where cg_tipo_solicitud = 'R')"+
				" )";
	}
//	System.out.println(qrySentencia);
  	rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
	        vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
	        vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
	        vecFilas.addElement(vecColumnas);
	      }
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
    }


  public String getProductoIF(String ic_if)
  throws NafinException
  {
  String qrySentencia 	= "";
  ResultSet rs 			= null;
  AccesoDB  con 		= null;

  String Productos 		= "";

    try {
      con = new AccesoDB();
      con.conexionDB();
	      qrySentencia = " SELECT DISTINCT ic_producto_nafin "+
						" FROM comrel_if_epo_x_producto "+
						" WHERE ic_if = "+ ic_if +
						" AND cs_habilitado = 'S' "+
						" AND ic_producto_nafin IN (2,5) ";


//	  System.out.println("["+qrySentencia+"]");
      rs = con.queryDB(qrySentencia);
	  int cont = 0;
      while(rs.next())
		{
			 if(cont>0)
			 	Productos += ",";
			 Productos += (rs.getString(1)==null)?"":rs.getString(1);
			 cont++;
        }

            con.cierraStatement();
        } catch(Exception e) {
			System.out.println("Exception en getProductoIF:"+e.toString());
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return Productos;
      } // getProductoIF


  public String getLinInicialXMoneda(String ic_pyme, String ic_epo, String ic_if, String ic_producto_nafin, String ic_moneda)
  throws NafinException
  {
  String qrySentencia 	= "";
  ResultSet rs 			= null;
  AccesoDB  con 		= null;

  String Intermediario	= "";

    try {
		con = new AccesoDB();
		con.conexionDB();
		qrySentencia = 
			" SELECT ic_linea_credito"   +
			" FROM com_linea_credito"   +
			" WHERE ic_pyme = "   + ic_pyme +
			" AND ic_epo = "   + ic_epo +
			" AND ic_if = "   + ic_if +
			" AND ic_producto_nafin =  "   + ic_producto_nafin +
			" AND cg_tipo_solicitud = 'I'"   +
			" AND ic_estatus_linea IN (1,2,3,5,7,9)"   +
			" AND ic_moneda = " + ic_moneda  ;


	  System.out.println("["+qrySentencia+"]");
      rs = con.queryDB(qrySentencia);
      while(rs.next())
		{
			 Intermediario = (rs.getString(1)==null)?"":rs.getString(1);
        }

            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return Intermediario;
      } // getLinInicialXMoneda

// 09/04/03
    public Vector getNafinElec_Prod(String ic_linea_credito)
    	throws NafinException
    	{
		String		qrySentencia	= "";
      	AccesoDB	con				= null;
		ResultSet	rs				= null;
      	Vector 		vecFilas		= new Vector();
		try {
						con = new AccesoDB();
            con.conexionDB();
       			qrySentencia =  " SELECT CN.ic_nafin_electronico, LC.ic_producto_nafin"   +
								" FROM COM_LINEA_CREDITO LC, COMREL_NAFIN CN"   +
								" WHERE LC.ic_linea_credito = "   + ic_linea_credito +
								" AND CN.cg_tipo = 'P'"   +
								" AND CN.ic_epo_pyme_if = LC.ic_pyme"  ;
					rs = con.queryDB(qrySentencia);
					System.out.println("["+qrySentencia+"]");
						while(rs.next())
						{
							vecFilas.addElement((rs.getString("IC_NAFIN_ELECTRONICO")==null)?"":rs.getString("IC_NAFIN_ELECTRONICO"));
							vecFilas.addElement((rs.getString("IC_PRODUCTO_NAFIN")==null)?"":rs.getString("IC_PRODUCTO_NAFIN"));
						}
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return vecFilas;
      } // getNafinElec_Prod

  public String validaLineaCreditoIni(String ic_if, String ic_pyme, String tipo_solicitud)
  throws NafinException
  {
  String qrySentencia 	= "";
  ResultSet rs 			= null;
  AccesoDB  con 		= null;

  String slmensaje		= "";

    try {
      con = new AccesoDB();
      con.conexionDB();
	  if(tipo_solicitud.equals("I"))
	  {
	      qrySentencia = " SELECT ic_linea_credito "+
						" FROM COM_LINEA_CREDITO "+
						" WHERE ic_pyme = "+ ic_pyme +
						" AND ic_if = "+ ic_if +
						" AND ic_estatus_linea in (3,5,7,9) ";

	  System.out.println("validaLineaCreditoIni["+qrySentencia+"]");
    	  rs = con.queryDB(qrySentencia);
	      if(rs.next())
				 slmensaje = "Existe una linea de credito inicial con el mismo intermediario y proveedor";
		}
		else if(tipo_solicitud.equals("R"))
		{
	      qrySentencia = " SELECT ic_linea_credito "+
						" FROM COM_LINEA_CREDITO "+
						" WHERE ic_pyme = "+ ic_pyme +
						" AND ic_if = "+ ic_if +
						" AND TRUNC(df_vencimiento_adicional) > TRUNC(SYSDATE) "+
						" AND ic_estatus_linea in (3,5,7,9) "+
						" AND cg_tipo_solicitud IN ('I','R') ";
		  System.out.println("validaLineaCreditoRenovacion["+qrySentencia+"]");
    	  rs = con.queryDB(qrySentencia);
	      if(rs.next())
				 slmensaje = "Existe una renovacion de linea de credito con el mismo intermediario y proveedor";
		}
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return slmensaje;
      } // validaLineaCreditoIni

//METODO SOBRECARGADO, ELIMINAR EL ANTERIOR SI YA NO SE UTILIZA  -- EGB 11/02/2004
// AGREGADO PARA PERMITIR GENERAR LINEAS DE CREDITO CON EL MISMO IF EN DISTINTAS EPOS

public String validaLineaCreditoIni(String ic_if, String ic_pyme, String tipo_solicitud, String ic_epo)
  throws NafinException{
  try{
  	return validaLineaCreditoIni(ic_if,ic_pyme,tipo_solicitud,ic_epo,null);
  }catch(NafinException ne){
	throw ne;
  }
}


public String validaLineaCreditoIni(String ic_if, String ic_pyme, String tipo_solicitud, String ic_epo,String ic_producto_nafin)
  throws NafinException
  {
  String qrySentencia 	= "";
  ResultSet rs 			= null;
  AccesoDB  con 		= null;

  String slmensaje		= "";

    try {
      con = new AccesoDB();
      con.conexionDB();
	  if(tipo_solicitud.equals("I"))
	  {
	      qrySentencia = " SELECT ic_linea_credito "+
						" FROM COM_LINEA_CREDITO "+
						" WHERE ic_pyme = "+ ic_pyme +
						" AND ic_if = "+ ic_if +
						" AND ic_epo = " + ic_epo;
		  if("5".equals(ic_producto_nafin)){
		  	qrySentencia += " AND ic_estatus_linea in (3,7,9) ";
		  }else{
		  	qrySentencia += " AND ic_estatus_linea in (3,5,7,9) ";
		  }

		  if(ic_producto_nafin!=null)
		  	qrySentencia += " AND ic_producto_nafin = "+ic_producto_nafin;

	  System.out.println("validaLineaCreditoIni["+qrySentencia+"]");
    	  rs = con.queryDB(qrySentencia);
	      if(rs.next())
				 slmensaje = "Existe una linea de credito inicial con el mismo intermediario y proveedor";
		}
		else if(tipo_solicitud.equals("R"))
		{
	      qrySentencia = " SELECT ic_linea_credito "+
						" FROM COM_LINEA_CREDITO "+
						" WHERE ic_pyme = "+ ic_pyme +
						" AND ic_if = "+ ic_if +
						" AND TRUNC(df_vencimiento_adicional) > TRUNC(SYSDATE) "+
						" AND ic_estatus_linea in (3,5,7,9) "+
						" AND cg_tipo_solicitud IN ('I','R') ";
		  if(ic_producto_nafin!=null)
		  	qrySentencia += " AND ic_producto_nafin = "+ic_producto_nafin;

		  System.out.println("validaLineaCreditoRenovacion["+qrySentencia+"]");
    	  rs = con.queryDB(qrySentencia);
	      if(rs.next())
				 slmensaje = "Existe una renovacion de linea de credito con el mismo intermediario y proveedor";
		}
            con.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
					if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
      return slmensaje;
      } // validaLineaCreditoIni

  private void cmpCuentas(String cuentaSpeua, String cuentaClabe, String ic_financiera, String ic_linea_credito, String ic_estatus, AccesoDB  con)
  throws NafinException
  {
		String		qrySentencia	= "";
		ResultSet	rs				= null;
		String 		slcuentaSpeua	= "";
		String      slcuentaClabe	= "";
		String		slicFinanciera	= "";
	    boolean   resultado     = true;

		try {
       			qrySentencia = " SELECT cg_numero_cuenta, cg_cuenta_clabe, ic_financiera "+
								" FROM com_linea_credito "+
								" WHERE ic_linea_credito = "+ ic_linea_credito;

					System.out.println("1["+qrySentencia+"]");
					rs = con.queryDB(qrySentencia);
						if(rs.next())
						{
							slcuentaSpeua 	= (rs.getString(1)==null)?"":rs.getString(1);
							slcuentaClabe 	= (rs.getString(2)==null)?"":rs.getString(2);
							slicFinanciera 	= (rs.getString(3)==null)?"":rs.getString(3);

							if((ic_estatus.equals("7")|| ic_estatus.equals("9")) && (!cuentaSpeua.equals(slcuentaSpeua) || !cuentaClabe.equals(slcuentaClabe) || !ic_financiera.equals(slicFinanciera)))
							{
					              qrySentencia =  " update com_linea_credito "+
								  				" set cs_aceptacion_pyme = 'C'" +
												" where ic_linea_credito="+ ic_linea_credito ;

								System.out.println("Cambio de Estatus");
								System.out.println("2["+qrySentencia+"]");
								con.ejecutaSQL(qrySentencia);
							}
							else
					            con.cierraStatement();
						}
    } catch(Exception e) {
      System.out.println("LineaCredito.updtLCtmp Exception"+e);
	  resultado = false;
      throw new NafinException("ANTI0020");
     }
  } //fin cmpCuentas

  private String reservaLinea(String ic_pyme, AccesoDB  con)
    throws NafinException
    {
    String    ic_linea_credito  = "";
    String    qrySentencia      = "";
    ResultSet rs                = null;
    boolean		tieneLinea				= false;
      try {
				int maxLineaCredito = 0;
				qrySentencia = 	"select max(ic_linea_credito) as maxLin from com_linea_credito"+
							" where ic_pyme ="+ic_pyme+
	  						" and ic_estatus_linea is null";
    		  	rs = con.queryDB(qrySentencia);
				if(rs.next())
					{
						maxLineaCredito = rs.getInt("MAXLIN");
						if(maxLineaCredito>0)
						tieneLinea=true;
					}
//				con.cierraStatement();
				if(!tieneLinea)
							{
							qrySentencia = "select max(ic_linea_credito) as maxLin from com_linea_credito";
							rs = con.queryDB(qrySentencia);
							if(rs.next())
								maxLineaCredito = rs.getInt("MAXLIN") +1;
							qrySentencia =	"insert into com_linea_credito"+
											"(ic_linea_credito,ic_pyme) values ("+maxLineaCredito+","+ic_pyme+")";
							con.ejecutaSQL(qrySentencia);
//							con.terminaTransaccion(true);
							}
							ic_linea_credito = new Integer(maxLineaCredito).toString();
							System.out.println("reservaLinea["+qrySentencia+"]");
							qrySentencia = "";
      }catch(Exception e){
        System.out.println("Error en LineaCreditoBean::reservaLinea(1,1)"+e);
        throw new NafinException("SIST0001");
      } /*finally {
        if(con.hayConexionAbierta())
          con.cierraConexionDB();
      }*/
    return ic_linea_credito;
    } //reserva linea credito

private Vector getNafinElec_Prod(String ic_linea_credito, AccesoDB  con)
throws NafinException
{
	String		qrySentencia	= "";
	ResultSet	rs				= null;
	Vector 		vecFilas		= new Vector();
	try {
		qrySentencia =  " SELECT CN.ic_nafin_electronico, LC.ic_producto_nafin"   +
						" FROM COM_LINEA_CREDITO LC, COMREL_NAFIN CN"   +
						" WHERE LC.ic_linea_credito = "   + ic_linea_credito +
						" AND CN.cg_tipo = 'P'"   +
						" AND CN.ic_epo_pyme_if = LC.ic_pyme"  ;
		rs = con.queryDB(qrySentencia);
		System.out.println("getNafinElec_Prod["+qrySentencia+"]");
		while(rs.next())
		{
			vecFilas.addElement((rs.getString("IC_NAFIN_ELECTRONICO")==null)?"":rs.getString("IC_NAFIN_ELECTRONICO"));
			vecFilas.addElement((rs.getString("IC_PRODUCTO_NAFIN")==null)?"":rs.getString("IC_PRODUCTO_NAFIN"));
		}
		 con.cierraStatement();
	} catch(Exception e) {
	throw new NafinException("SIST0001");
	}
	return vecFilas;
} // getNafinElec_Prod


public Vector InsertaLC(String producto, String proceso, String ic_if, String iNoUsuario)
throws NafinException{
	try{
		return InsertaLC(producto,proceso,ic_if,iNoUsuario,null);
	}catch(NafinException ne){
		throw ne;
	}
}


public Vector InsertaLC(String producto, String proceso, String ic_if, String iNoUsuario,String ccAcuse)
throws NafinException
  {
  System.out.println("LineaCreditoBean::InsertaLC (E)");
  String qrySentencia 		= "";
  boolean   resultado  	 	= true;
  PreparedStatement ps		= null;
  AccesoDB  con 			= null;
  Vector   vecFilas			= new Vector();
  Vector   vecColumnas 		= new Vector();
  Vector   vecMsg		 	= new Vector();
  int		i				= 0;
  String sfolio				= "";
  String smontoSolicitado	= "";
  String scausaRechazo		= "";
  String sfechaVto			= "";
  String scuenta			= "";
  String slineaAut			= "";
  String screditoAdic		= "";
  String slineaInd			= "";
  String slineaTot			= "";
  String sproductoNafin		= "";
  String sicEpo				= "";
  String sicPyme			= "";
  String sicFinanciera		= "";
  String sicTipoSolicitud	= "";
  String sicEstatus			= "";
  String splazo				= "";
  String sicMoneda			= "";
  String sfechaLimite		= "";
  String scuentaClabe		= "";
  String sSolicTroya		= "";
  String sFechaAuto			= "";
  String sTablaAmort		= "";
  String sNumMaxAmort		= "";
  String sClaveLineaTipo	= "";
  String slmensaje			= "";
  String hidIcLineaCredito	= "";
  String folioArmado		= "";
  String mensaje			= "";

	int		numLineasMN		= 0;
	int		numLineasUSD	= 0;
	int		numRechMN		= 0;
	int		numRechUSD		= 0;
	double	montoLineasMN	= 0;
	double	montoLineasUSD	= 0;
	double	montoRechMN		= 0;
	double	montoRechUSD	= 0;


    try {

      con = new AccesoDB();
      con.conexionDB();

//	-- 1 -- Obtener las l&iacute;neas de cr&eacute;dito dadas de alta em la tabla comtmp_linea_credito


		if(!"".equals(ccAcuse)&&ccAcuse!=null){
			qrySentencia =
				" insert into dis_acuse4"+
				" (cc_acuse,df_acuse,ic_usuario)"+
				" values(?,sysdate,?)";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ccAcuse);
			ps.setString(2,iNoUsuario);
			ps.execute();
			ps.close();
		}


		vecFilas = consultaLCtmp(producto, proceso, ic_if);
		String contador = new Integer(vecFilas.size()).toString();
        vecMsg.addElement(contador);
		for(i=0;i<vecFilas.size();i++) {
			vecColumnas = (Vector)vecFilas.get(i);
			sfolio				= (String)vecColumnas.get(1);
			smontoSolicitado	= (String)vecColumnas.get(10);
			scausaRechazo		= (String)vecColumnas.get(14);
			sfechaVto			= (String)vecColumnas.get(16);
			scuenta				= (String)vecColumnas.get(18);
			slineaAut			= (String)vecColumnas.get(19);
			screditoAdic		= (String)vecColumnas.get(20);
			slineaInd			= (String)vecColumnas.get(21);
			slineaTot			= (String)vecColumnas.get(22);
			sproductoNafin		= (String)vecColumnas.get(25);
			sicEpo				= (String)vecColumnas.get(26);
			sicPyme				= (String)vecColumnas.get(27);
			sicFinanciera		= (String)vecColumnas.get(28);
			sicTipoSolicitud	= (String)vecColumnas.get(29);
			sicEstatus			= (String)vecColumnas.get(30);
			splazo				= (String)vecColumnas.get(32);
			sicMoneda			= (String)vecColumnas.get(34);
			sfechaLimite		= (String)vecColumnas.get(35);
			scuentaClabe		= (String)vecColumnas.get(38);
			sSolicTroya			= (String)vecColumnas.get(39);
			sFechaAuto			= (String)vecColumnas.get(40);
			sTablaAmort			= (String)vecColumnas.get(46);
			sNumMaxAmort		= (String)vecColumnas.get(44);
			sClaveLineaTipo		= (String)vecColumnas.get(45);

			if("1".equals(sicMoneda)){
				if("1".equals(sicEstatus)){
					numLineasMN++;
					montoLineasMN += Double.parseDouble(slineaTot);
				}else{
					numRechMN++;
					montoRechMN += Double.parseDouble(slineaTot);
				}
			}else{
				if("1".equals(sicEstatus)){
					numLineasUSD++;
					montoLineasUSD += Double.parseDouble(slineaTot);
				}else{
					numRechUSD++;
					montoRechUSD += Double.parseDouble(slineaTot);
				}
			}

			if(sicTipoSolicitud.equals("I") || sicTipoSolicitud.equals("R")) {
				//slmensaje  = validaLineaCreditoIni(ic_if, sicPyme, sicTipoSolicitud);
				slmensaje  = validaLineaCreditoIni(ic_if, sicPyme, sicTipoSolicitud, sicEpo,sproductoNafin);

			} else {
				slmensaje  = "";
			}

			System.out.println("EL MENSAJE = "+slmensaje);
	        vecMsg.addElement(slmensaje);

			if(slmensaje.equals(""))
			{
				try
				{
					//	-- 2 -- Genera solicitud
					System.out.println("hidIcLineaCredito1["+hidIcLineaCredito+"]");
				          if("".equals(hidIcLineaCredito))                //generamos el n&uacute;mero de l&iacute;nea de cr&eacute;dito
					          hidIcLineaCredito = reservaLinea(sicPyme, con);
							  System.out.println("hidIcLineaCredito["+hidIcLineaCredito+"]");
			    	      if("".equals(folioArmado))
						      folioArmado = getFolioArmado(hidIcLineaCredito,sicPyme,sicTipoSolicitud);
							  System.out.println("folioArmado["+folioArmado+"]");
					//	-- 3 -- Actualiza solicitud
								generaSolicitud(
									hidIcLineaCredito, sicEpo, sicPyme, ic_if,
									sicTipoSolicitud, sfolio, sproductoNafin, smontoSolicitado, 
									sicEstatus, scausaRechazo, splazo, sicFinanciera, 
									scuenta, sfechaVto, slineaAut, screditoAdic,
									slineaInd, slineaTot, sicMoneda, sfechaLimite, 
									scuentaClabe, iNoUsuario, con, sSolicTroya, 
									sFechaAuto, ccAcuse, 
									
									sTablaAmort, sNumMaxAmort, 
									sClaveLineaTipo);
					//	-- 4 -- Guarda mensajes de las solicitudes generadas
					String msg = "";
					if(sicTipoSolicitud.equals("I"))
						msg = "de L�nea de Cr�dito Inicial";
					else if(sicTipoSolicitud.equals("R"))
						msg = "de Renovaci�n de L�nea de Cr�dito";
					else if(sicTipoSolicitud.equals("A"))
						msg = "de Ampliaci�n de L�nea de Cr�dito";
					System.out.println("mensaje["+msg+"]");

		           mensaje = "Solicitud "+ msg +" generada con folio:"+folioArmado;
					hidIcLineaCredito = "";
					folioArmado = "";
			        vecMsg.addElement(mensaje);
					System.out.println("------------------------------------");
				} catch(Exception e){
						System.out.println("Excepcion = " + e +"Lanzando nafin exception");
				       resultado = false;
				       throw new NafinException("SIST0001");
				}
			}//if
			else
			{
				mensaje = "";
		        vecMsg.addElement(mensaje);
			}
		}//for
		if(!"".equals(ccAcuse)&&ccAcuse!=null){
			qrySentencia =
				" INSERT INTO DISREL_ACUSE4_MONEDA"+
				"(CC_ACUSE, IC_MONEDA, IN_TOTAL_LINEAS, FN_TOTAL_MONTO)"+
				" VALUES(?,?,?,?)";
			ps = con.queryPrecompilado(qrySentencia);
			if(numLineasMN>0){
				ps.setString(1,ccAcuse);
				ps.setInt(2,1);
				ps.setInt(3,numLineasMN);
				ps.setDouble(4,montoLineasMN);
				System.out.println("Insertando acuse mN");
				ps.execute();
			}
			if(numLineasUSD>0){
				ps.setString(1,ccAcuse);
				ps.setInt(2,54);
				ps.setInt(3,numLineasUSD);
				ps.setDouble(4,montoLineasUSD);
				System.out.println("Insertando acuse USD");
				ps.execute();
			}
		}

	} catch (NafinException ne) {
		resultado = false;
		throw ne;
    } catch(Exception e) {
      System.out.println("LineaCredito.InsertaLC Exception"+e);
      resultado = false;
      throw new NafinException("SIST001");
    } finally {
      con.terminaTransaccion(resultado);
      if(con.hayConexionAbierta())
        con.cierraConexionDB();
	  System.out.println("LineaCreditoBean::InsertaLC (S)");
    }
	return vecMsg;
  } //fin InsertaLC

public Vector BloqueoIF(
				String folio [], String estatus_ant [], String estatus_act [], String estatus_nuevo [],
				String causa [], String banco_ant [], String banco [], String cuenta [], 
				String cuentaClabe [], String cuenta_ant[], String cuentaClabe_ant [], String tipo[], 
				String sElementos, String moneda[], String iNoUsuario, String monto[], 
				String ccAcuse, String fecha_limite[])
		throws NafinException {
	int			i					= 0;
	String		qrySentencia		= "";
	AccesoDB	con					= null;
	PreparedStatement ps			= null;
	boolean		resultado			= true;
	String 		slnafinElec			= "";
	String 		slproducto			= "";
	String		sCta				= "";
	String  	sCta1				= "";
	String      Nestatus			= "";
	Vector		vecFilas			= new Vector();
	String 		in					= "";
	int			numLinMN			= 0;
	int			numLinUSD			= 0;
	double		totalMN				= 0;
	double		totalUSD			= 0;
	Vector 		vecRetorno			= new Vector();

    try {
		System.out.println("LineaCreditoBean:: BloqueoIF(E)");
		con = new AccesoDB();
		con.conexionDB();
		if(!"".equals(ccAcuse)&&ccAcuse!=null) {
		  qrySentencia =
		  	" insert into dis_acuse4"+
			" (cc_acuse,df_acuse,ic_usuario)"+
			" values(?,sysdate,?)";
		  ps = con.queryPrecompilado(qrySentencia);
		  ps.setString(1,ccAcuse);
		  ps.setString(2,iNoUsuario);
		  ps.executeUpdate();
		  ps.close();
		}
	  
		for(i=0;i<folio.length;i++) {
			if("".equals(estatus_nuevo[i])||"null".equals(estatus_nuevo[i])) {
				Nestatus = estatus_act[i];
				estatus_act[i] = "";
			} else {
				if (estatus_act[i].equals("7")&&(!estatus_ant[i].equals("")&&(estatus_ant[i]!=null)))
					Nestatus=estatus_ant[i];
				else
					Nestatus=estatus_nuevo[i];
			}
			qrySentencia =
				" UPDATE com_linea_credito"   +
				"    SET ic_estatus_linea = ?,"   +
				"        cg_causa_rechazo = ?,"   +
				"        cg_numero_cuenta = ?,"   +
				"        cg_cuenta_clabe = ?,"   +
				"        ic_estatus_linea_anterior = ?,"   +
				"        df_limite_disposicion = TO_DATE (?, 'dd/mm/yyyy'),"   +
				"        cc_acuse = ?"   +
				"  WHERE ic_linea_credito = ?"  ;
				
			System.out.println("AutorizacionIF::BloqueoyCambios["+qrySentencia+"]");
			System.out.println("Nestatus: "+Nestatus);
			System.out.println("causa[i]: "+causa[i]);
			System.out.println("cuenta[i]: "+cuenta[i]);
			System.out.println("cuentaClabe[i]: "+cuentaClabe[i]);
			System.out.println("estatus_act[i]: "+estatus_act[i]);
			System.out.println("fecha_limite[i]: "+fecha_limite[i]);
			System.out.println("ccAcuse: "+ccAcuse);
			System.out.println("folio[i]: "+folio[i]);
				
		  	ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(Nestatus));
			ps.setString(2, causa[i]);
			ps.setString(3, cuenta[i]);
			ps.setString(4, cuentaClabe[i]);
			if(estatus_act[i]==null||"".equals(estatus_act[i]))
				ps.setNull(5, Types.INTEGER);
			else
				ps.setInt(5, Integer.parseInt(estatus_act[i]));
			ps.setString(6, fecha_limite[i]);			
			if(ccAcuse==null||"".equals(ccAcuse))
				ps.setNull(7, Types.VARCHAR);
			else
				ps.setString(7, ccAcuse);
			ps.setInt(8, Integer.parseInt(folio[i]));

			try {
		  		if (!qrySentencia.equals("")) {
					in += ((in.equals(""))?"":",")+folio[i];
					if(monto!=null) {
						if("1".equals(moneda[i])) {
							numLinMN++;
							totalMN += Double.parseDouble(monto[i]);
						} else {
							numLinUSD++;
							totalUSD += Double.parseDouble(monto[i]);
						}
					}
					ps.executeUpdate();
					ps.close();
				}
			} catch (SQLException sqle) {
				sqle.printStackTrace();
				throw new NafinException("ANTI0020");
			} //Fin de la ejecucion del update
			
			if ((!cuentaClabe[i].equals(cuentaClabe_ant[i]))||(!cuenta[i].equals(cuenta_ant[i]))) {	
				try {
					vecFilas = getNafinElec_Prod(folio[i]);
					if(vecFilas.size()>0) {
						slnafinElec	= (String)vecFilas.get(0);
						slproducto	= (String)vecFilas.get(1);
						
						if(!cuenta[i].equals(""))
							sCta  = ovinsertarCuenta(slnafinElec, slproducto, moneda[i], "1", cuenta[i], iNoUsuario, "1", con);
						
						if(!cuentaClabe[i].equals(""))
						    sCta1 = ovinsertarCuenta(slnafinElec, slproducto, moneda[i], "1", cuentaClabe[i], iNoUsuario, "40", con);

						qrySentencia =  
							" update com_linea_credito "+
							" set cs_aceptacion_pyme = 'C'" +
							" , cc_acuse = ";
						
						qrySentencia +=  (ccAcuse == null)? ccAcuse:"'"+ ccAcuse +"'";
						qrySentencia +=  " where ic_linea_credito="+ folio[i] ;

						try {
							con.ejecutaSQL(qrySentencia);
							in += ((in.equals(""))?"":",")+folio[i];
							if(monto!=null){
								if("1".equals(moneda[i])){
									numLinMN++;
									totalMN += Double.parseDouble(monto[i]);
								}else{
									numLinUSD++;
									totalUSD += Double.parseDouble(monto[i]);
								}
							}
						} catch (SQLException sqle) {
						throw new NafinException("ANTI0020");
						} //Fin de la ejecucion del update
						 //cmpCuentas(cuenta[i], cuentaClabe[i], banco[i], folio[i], estatus[i], con);
						System.out.println("AutorizacionIF::Cambio a aceptacion["+qrySentencia+"]");
					}
				} catch (NafinException ne) {
					resultado = false;
					throw ne;
				}
			}       //fin if cambio en cuentas
		}//for(i=0;i<folio.length;i++)
		if(!"".equals(ccAcuse)&&ccAcuse!=null){
			qrySentencia =
				" insert into disrel_acuse4_moneda"+
				" (CC_ACUSE, IC_MONEDA, IN_TOTAL_LINEAS, FN_TOTAL_MONTO)"+
				" values(?,?,?,?)";
			ps = con.queryPrecompilado(qrySentencia);
			if(numLinMN>0){
				ps.setString(1,ccAcuse);
				ps.setInt(2,1);
				ps.setInt(3,numLinMN);
				ps.setDouble(4,totalMN);
				ps.execute();
			}
			if(numLinUSD>0){
				ps.setString(1,ccAcuse);
				ps.setInt(2,54);
				ps.setInt(3,numLinUSD);
				ps.setDouble(4,totalUSD);
				ps.execute();
			}
		}
	} catch (NafinException ne) {
		ne.printStackTrace();
		resultado = false;
		throw ne;
    } catch(Exception e) {
      e.printStackTrace();
      System.out.println("LineaCredito::BloqueoIF Exception"+e);
      resultado = false;
      throw new NafinException("SIST0001");
    } finally {
		vecRetorno.add(in);
		vecRetorno.add(""+numLinMN);
		vecRetorno.add(""+numLinUSD);
		vecRetorno.add(""+totalMN);
		vecRetorno.add(""+totalUSD);
		if(con.hayConexionAbierta()) {
	      con.terminaTransaccion(resultado);
	      System.out.println("LineaCreditoBean:: BloqueoIF(E)");
	      con.cierraConexionDB();
        }
     }
	 return vecRetorno;
  }         //fin BloqueoIF


/**
 * El m�todo se utiliza tanto en pantallas de credicadenas como de pedidos
 */
public Vector BloqueoIF(
		String folio []
		,String estatus_ant []
		,String estatus_act []
		,String estatus_nuevo []
		,String causa []
		,String banco_ant []
		,String banco []
		,String cuenta []
		,String cuentaClabe []
		,String cuenta_ant[]
		,String cuentaClabe_ant []
		,String [] huboCambio
		,String tipo[]
		,String sElementos
		,String moneda[]
		,String iNoUsuario)
  	throws NafinException{
	try{
		return BloqueoIF(folio,estatus_ant,estatus_act,estatus_nuevo,causa,banco_ant
			,banco,cuenta,cuentaClabe,cuenta_ant,cuentaClabe_ant,huboCambio,tipo
			,sElementos,moneda,iNoUsuario,null,
			null, null, //F011-2006
			null);
	}catch(NafinException ne){
		throw ne;
	}
}

/**
 * Realiza cambio de estatus de lineas de cr�dito y cambio en otros datos
 * como cuentas y tipo de pago (s�lo para lineas de credito de credicadenas)
 * Metodo Creado para F011-2006
 * 
 * @param folio ???
 * @param estatus_ant ???
 * @param estatus_act ???
 * @param estatus_nuevo ???
 * @param causa ???
 * @param banco_ant ???
 * @param banco ???
 * @param cuenta ???
 * @param cuentaClabe ???
 * @param cuenta_ant ???
 * @param cuentaClabe_ant ???
 * @param huboCambio ???
 * @param tipo ???
 * @param sElementos ???
 * @param moneda ???
 * @param iNoUsuario ???
 * @param tipoPagoNuevo Arreglo con los nuevos Tipos de Pago.
 * 		S�lo aplica para credicadenas, de lo contrario es null.
 * 		Si los elementos del arreglo son cadenas vacias significa que no son
 * 		de credicadenas. Esto se presenta porque en la pantalla de credicadenas
 * 		se pueden ver ambos tanto lineas de credito de credicadenas como de pedidos.
 * @param tipoPagoAnterior Arreglo con los Tipos de Pago originales.
 * 		S�lo aplica para credicadenas, de lo contrario es null
 * 		Si los elementos del arreglo son cadenas vacias significa que no son
 * 		de credicadenas. Esto se presenta porque en la pantalla de credicadenas
 * 		se pueden ver ambos tanto lineas de credito de credicadenas como de pedidos.
 *
 */
public Vector BloqueoIF(
		String folio []
		,String estatus_ant []
		,String estatus_act []
		,String estatus_nuevo []
		,String causa []
		,String banco_ant []
		,String banco []
		,String cuenta []
		,String cuentaClabe []
		,String cuenta_ant[]
		,String cuentaClabe_ant []
		,String huboCambio []
		,String tipo[]
		,String sElementos
		,String moneda[]
		,String iNoUsuario
		,String tipoPagoNuevo []
		,String tipoPagoAnterior [] )
  	throws NafinException{
	try{
		return BloqueoIF(folio,estatus_ant,estatus_act,estatus_nuevo,causa,banco_ant
			,banco,cuenta,cuentaClabe,cuenta_ant,cuentaClabe_ant,huboCambio,tipo
			,sElementos,moneda,iNoUsuario,null,
			tipoPagoNuevo, tipoPagoAnterior,
			null);
	}catch(NafinException ne){
		throw ne;
	}
}


/**
 * Realiza cambio de estatus de lineas de cr�dito y cambio en otros datos
 * como cuentas y tipo de pago (s�lo para lineas de credito de credicadenas)
 *
 * Este metodo cambia el estatus y los datos de la cuenta bancaria en la pantalla de Bloueso y Cambios de Estatus
 * Solo se ejecuta sobre lineas iniciales o de renovacion, por lo que ninguna amplicacion es afectada
 * asi como no se afecta ningun monto de la linea
 *
 *
 * El m�todo es invocado tanto por pantallas 
 * de CREDICADENAS como de PEDIDOS (indirectamente).
 * Tener cuidado al modificarlo, para no afectar el funcionamiento para alguno
 * de los productos.
 *
 * @param folio ???
 * @param estatus_ant ???
 * @param estatus_act ???
 * @param estatus_nuevo ???
 * @param causa ???
 * @param banco_ant ???
 * @param banco ???
 * @param cuenta ???
 * @param cuentaClabe ???
 * @param cuenta_ant ???
 * @param cuentaClabe_ant ???
 * @param huboCambio ???
 * @param tipo ???
 * @param sElementos ???
 * @param moneda ???
 * @param iNoUsuario ???
 * @param monto ???
 * @param tipoPagoNuevo Arreglo con los nuevos Tipos de Pago.
 * 		S�lo aplica para credicadenas, de lo contrario es null.
 * 		Si los elementos del arreglo son cadenas vacias significa que no son
 * 		de credicadenas. Esto se presenta porque en la pantalla de credicadenas
 * 		se pueden ver ambos tanto lineas de credito de credicadenas como de pedidos.
 * @param tipoPagoAnterior Arreglo con los Tipos de Pago originales.
 * 		S�lo aplica para credicadenas, de lo contrario es null
 * 		Si los elementos del arreglo son cadenas vacias significa que no son
 * 		de credicadenas. Esto se presenta porque en la pantalla de credicadenas
 * 		se pueden ver ambos tanto lineas de credito de credicadenas como de pedidos.
 * @param ccAcuse Acuse 
 * 		(puede ser nulo si el cambio no es realizado por el IF sino por Nafin)
 *
 */

public Vector BloqueoIF(
		String folio []
		,String estatus_ant []
		,String estatus_act []
		,String estatus_nuevo []
		,String causa []
		,String banco_ant []
		,String banco []
		,String cuenta []
		,String cuentaClabe []
		,String cuenta_ant[]
		,String cuentaClabe_ant []
		,String huboCambio[]
		,String tipo[]
		,String sElementos
		,String moneda[]
		,String iNoUsuario
		,String monto[]
		,String tipoPagoNuevo[]	//F011-2006
		,String tipoPagoAnterior[]	//F011-2006
		,String ccAcuse)
		throws NafinException {
	int	i = 0;
	int	iElementos = Integer.parseInt(sElementos);
	StringBuffer qrySentencia = new StringBuffer();
	AccesoDB con = null;
	PreparedStatement ps = null;
	boolean resultado = true;
	String slnafinElec = "";
	String slproducto = "";
	String sCta = "";
	String sCta1 = "";
	String Nestatus = "";
	Vector vecFilas = new Vector();
	StringBuffer in = new StringBuffer();
	int numLinMN = 0;
	int numLinUSD = 0;
	double totalMN = 0;
	double totalUSD = 0;
	Vector vecRetorno = new Vector();
	
	boolean registroModificado = false;

	try {
		System.out.println("LineaCreditoBean:: BloqueoIF(E)");
		con = new AccesoDB();
		con.conexionDB();
		if(!"".equals(ccAcuse)&&ccAcuse!=null) {
			String strSQL =
					" INSERT INTO dis_acuse4"+
					" (cc_acuse,df_acuse,ic_usuario)"+
					" VALUES(?,sysdate,?)";
			ps = con.queryPrecompilado(strSQL);
			ps.setString(1,ccAcuse);
			ps.setString(2,iNoUsuario);
			ps.execute();
			ps.close();
		}

		for(i=0;i<iElementos&&resultado;i++) {
			String sTipoPagoNuevo = "";
			String sTipoPagoAnterior = "";
			
			registroModificado = false;

			if(tipoPagoNuevo != null && tipoPagoAnterior != null) {
				sTipoPagoNuevo = 
						(tipoPagoNuevo[i] == null || tipoPagoNuevo[i].equals(""))?
      							"null":tipoPagoNuevo[i];
				sTipoPagoAnterior = 
      					(tipoPagoAnterior[i] == null || tipoPagoAnterior[i].equals(""))?
								"null":tipoPagoAnterior[i];
			} else {
				sTipoPagoNuevo = "null";
				sTipoPagoAnterior = "null";
			}
      	
			//Borra el query anterior
			qrySentencia.delete(0,qrySentencia.length());
			
			// Detectar si hubo cambios en el estatus
			if (!estatus_nuevo[i].equals("")&&(estatus_nuevo[i]!=null)&&
					!estatus_nuevo[i].equals("null")) {   
        	
				System.out.println("Cambio de estatus(1)");

				registroModificado = true;
        	
				if (estatus_act[i].equals("7")&&(!estatus_ant[i].equals("")&&(estatus_ant[i]!=null))) {
					Nestatus=estatus_ant[i];
				} else {
					Nestatus=estatus_nuevo[i];
				}
				qrySentencia.append(
						" UPDATE com_linea_credito "+
						" SET ic_estatus_linea = "+Nestatus+
						" ,cg_causa_rechazo = '"+causa[i]+"'"+
						" ,cg_numero_cuenta = '"+cuenta[i]+"'"+
						" ,cg_cuenta_clabe = '"+cuentaClabe[i]+"'"+
						" ,ic_financiera = " + banco[i] +
						((!sTipoPagoNuevo.equals("null"))?" ,ic_tabla_amort = " + sTipoPagoNuevo : "") +
						" ,ic_estatus_linea_anterior="+estatus_act[i] +
						" ,cc_acuse = " + ((ccAcuse == null)? "null":"'"+ ccAcuse +"'") +
						" WHERE ic_linea_credito = "+folio[i]);
				} else if ( !cuentaClabe[i].equals(cuentaClabe_ant[i]) || 
						!cuenta[i].equals(cuenta_ant[i]) || 
						!banco[i].equals(banco_ant[i]) || 
						(!sTipoPagoNuevo.equals("null") && !sTipoPagoNuevo.equals(sTipoPagoAnterior)) ) {  
					System.out.println("Cambio detectado linea credito");
        	
					registroModificado = true;
        	
					qrySentencia.append(
							" UPDATE com_linea_credito "+
							" SET cg_numero_cuenta = '"+cuenta[i]+"'"+
							" 	,cg_cuenta_clabe = '"+cuentaClabe[i]+"'"+
							"	,cc_acuse = " + ((ccAcuse == null)? "null":"'"+ ccAcuse +"'") +
							" 	,ic_financiera = " + banco[i] +
							((!sTipoPagoNuevo.equals("null"))?" ,ic_tabla_amort = " + sTipoPagoNuevo : "") +
							" WHERE ic_linea_credito = "+folio[i]);
				}// Fin del if-else para cambio de estatus
				System.out.println("LineaCreditoBean::BloqueoIF["+qrySentencia+"]");
  	    
				try {
					if (registroModificado){
						in.append( ((in.length() == 0)?"":",")+folio[i]);
						if(monto!=null) {
							if("1".equals(moneda[i])){
								numLinMN++;
								totalMN += Double.parseDouble(monto[i]);
							} else {
								numLinUSD++;
								totalUSD += Double.parseDouble(monto[i]);
							}
						}
						con.ejecutaSQL(qrySentencia.toString());
					}
				} catch (SQLException sqle) {
					throw new NafinException("ANTI0020");
				} //Fin de la ejecucion del update
		
				if (!cuentaClabe[i].equals(cuentaClabe_ant[i])||
						!cuenta[i].equals(cuenta_ant[i]) ) {
					try {
						//Borra el query anterior
						qrySentencia.delete(0,qrySentencia.length());

						vecFilas = getNafinElec_Prod(folio[i]);
						if(vecFilas.size()>0) {
							slnafinElec	= (String)vecFilas.get(0);
							slproducto	= (String)vecFilas.get(1);
					
							if(!cuenta[i].equals("")) {
								sCta  = ovinsertarCuenta(slnafinElec, slproducto, moneda[i], "1", cuenta[i], iNoUsuario, "1", con);
							}
					
							if(!cuentaClabe[i].equals("")) {
								sCta1 = ovinsertarCuenta(slnafinElec, slproducto, moneda[i], "1", cuentaClabe[i], iNoUsuario, "40", con);
							}

							qrySentencia.append(
									" UPDATE com_linea_credito "+
									" SET cs_aceptacion_pyme = 'C'" +
									" 	, cc_acuse = " + ((ccAcuse == null)? "null":"'"+ ccAcuse +"'") +
									" WHERE ic_linea_credito="+ folio[i]);

							try {
								con.ejecutaSQL(qrySentencia.toString());
								in.append(((in.equals(""))?"":",")+folio[i]);
								if(monto!=null){
									if("1".equals(moneda[i])){
										numLinMN++;
										totalMN += Double.parseDouble(monto[i]);
									}else{
										numLinUSD++;
										totalUSD += Double.parseDouble(monto[i]);
									}
								}
							} catch (SQLException sqle) {
								throw new NafinException("ANTI0020");
							} //Fin de la ejecucion del update
							//cmpCuentas(cuenta[i], cuentaClabe[i], banco[i], folio[i], estatus[i], con);
							System.out.println("LineaCreditoBean::BloqueoIF::Cambio a aceptacion["+qrySentencia+"]");
						}
					} catch (NafinException ne) {
						resultado = false;
						throw ne;
				}
			}       //fin if cambio en cuentas
		}     //fin for

		if(!"".equals(ccAcuse)&&ccAcuse!=null){
			String strSQL =
					" INSERT INTO disrel_acuse4_moneda"+
					" (CC_ACUSE, IC_MONEDA, IN_TOTAL_LINEAS, FN_TOTAL_MONTO)"+
					" VALUES(?,?,?,?)";
			ps = con.queryPrecompilado(strSQL);
			if(numLinMN>0){
				ps.setString(1,ccAcuse);
				ps.setInt(2,1);
				ps.setInt(3,numLinMN);
				ps.setDouble(4,totalMN);
				ps.execute();
			}
			if(numLinUSD>0){
				ps.setString(1,ccAcuse);
				ps.setInt(2,54);
				ps.setInt(3,numLinUSD);
				ps.setDouble(4,totalUSD);
				ps.execute();
			}
		}

	} catch (NafinException ne) {
		resultado = false;
		throw ne;
	} catch(Exception e) {
		System.out.println("LineaCredito::BloqueoIF Exception"+e);
		resultado = false;
		throw new NafinException("SIST0001");
	} finally {
		vecRetorno.add(in.toString());
		vecRetorno.add(""+numLinMN);
		vecRetorno.add(""+numLinUSD);
		vecRetorno.add(""+totalMN);
		vecRetorno.add(""+totalUSD);
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(resultado);
			System.out.println("LineaCreditoBean:: BloqueoIF(S)");
			con.cierraConexionDB();
		}
	}
	return vecRetorno;
} //fin BloqueoIF

/*-------------------- M�todos para bloqueo -----------------------------------*/
/**
 * Consulta ???
 * @param ic_epo Clave de la EPO
 * @param ic_if Clave del IF
 * @param ic_pyme Clave de la Pyme
 * @param ic_linea Clave de la linea
 * @param ic_producto_nafin Clave del producto nafin
 * 		2 Pedidos, 5 Credicadenas 2,5 Ambos
 * @param tipo ???
 *
 */
public Vector consultaIFBloqueo(String ic_epo,String ic_if,String ic_pyme,
		String ic_linea, String ic_producto_nafin, String tipo)
		throws NafinException {
	try {
		return consultaIFBloqueo(ic_epo,ic_if,ic_pyme,ic_linea,ic_producto_nafin,tipo,"");
	} catch(NafinException ne) {
		throw ne;
	}
}


/**
 * Consulta ???
 * @param ic_epo Clave de la EPO
 * @param ic_if Clave del IF
 * @param ic_pyme Clave de la Pyme
 * @param ic_linea Clave de la linea
 * @param ic_producto_nafin Clave del producto nafin
 * 		2 Pedidos, 5 Credicadenas 2,5 Ambos
 * @param tipo ???
 * @param in Claves de folios a obtener
 *
 */
public Vector consultaIFBloqueo(String ic_epo,String ic_if,String ic_pyme,
		String ic_linea, String ic_producto_nafin, String tipo,String in)
		throws NafinException {
	StringBuffer condicion = new StringBuffer();
	StringBuffer qrySentencia = new StringBuffer();
	Vector vecFilas = new Vector();
	Vector vecColumnas = new Vector();
	ResultSet rs = null;
	AccesoDB con = null;
	String conTipoSolicitud = "";
	String conNumeroSirac = "";
	String conConsecutivo = "";
	String strAux = "";
	String tipo_liq = "";
	try {
		con = new AccesoDB();
		con.conexionDB();

		String strSQL =
				" SELECT CG_TIPO_LIQUIDACION"+
				" FROM COMREL_PRODUCTO_IF"+
				" WHERE IC_IF = "+ic_if+" "+
				" AND IC_PRODUCTO_NAFIN=4";
		//System.out.println(qrySentencia);
		rs = con.queryDB(strSQL);
		if(rs.next()) {
			tipo_liq = (rs.getString(1)==null)?"":rs.getString(1);
		}
		rs.close();
		con.cierraStatement();

		getDatosComunes(ic_if, ic_epo, con);

		if(!"".equals(ic_linea)) {
			setFolioArmado(ic_linea);
			conTipoSolicitud = getTipoSolicitud();
			conNumeroSirac   = getNumeroSirac();
			conConsecutivo   = getConsecutivo().trim();
			condicion.append(
					" AND LC.ic_linea_credito = "+conConsecutivo+
					" AND LC.cg_tipo_solicitud = '"+conTipoSolicitud+"'"+
					" AND PY.in_numero_sirac = "+conNumeroSirac);
		}
		if(!"".equals(in)){
			condicion.append(" AND LC.ic_linea_credito in("+in+")");
		
		}
		
		if(!"".equals(ic_epo)){
			condicion.append(" AND LC.ic_epo = "+ ic_epo);
			if(!"TODOS".equals(ic_pyme)&&!"".equals(ic_pyme))
				condicion.append(" AND LC.ic_pyme = "+ic_pyme);
		}
		
		condicion.append(" Order by PRODUCTO_NAFIN");
		
		qrySentencia.append(
				" select LC.ic_linea_credito, PE.cg_pyme_epo_interno,PY.cg_razon_social, ES.cd_nombre as ESTADO "+
				" ,D.cg_calle||' '||decode(D.cg_numero_ext,null,'',D.cg_numero_ext)||' '||decode(D.cg_numero_ext,null,'',D.cg_numero_ext) as DIRECCION "+
				" ,D.cg_telefono1, decode(LC.cg_tipo_solicitud,'I','Inicial','R','Renovaci&oacute;n','A','Ampliaci&oacute;n') as TIPO_SOLICITUD "+
				" ,to_char(LC.df_captura,'dd/mm/yyyy') as FECHA_SOLICITUD, EL.cd_descripcion as ESTATUS"+
				" ,PRE.fn_linea_automatica,LC.ic_estatus_linea,decode(LC.df_vencimiento,null,to_char(sysdate,'dd/mm/yyyy'),to_char(LC.df_vencimiento,'dd/mm/yyyy')) as FECHA_HOY"+
				" ,NVL(PRE.cs_linea_individual,'"+defLineaInd+"') as cs_linea_individual,LC.cg_tipo_solicitud"+
				" ,LC.fn_monto_solicitado,LC.ic_plazo,LC.ic_financiera ");
		if("I".equals(tipo_liq)) {
			qrySentencia.append(" ,LC.cg_numero_cuenta_if as CUENTA");
		} else if("A".equals(tipo_liq)) {
			qrySentencia.append(" ,LC.cg_numero_cuenta as CUENTA");
		} else {
			qrySentencia.append(" ,LC.cg_numero_cuenta as CUENTA");
		}
		qrySentencia.append(
				" ,LC.fn_linea_opcional,LC.fn_linea_individual,LC.fn_monto_autorizado"+
				" ,PY.in_numero_sirac,VE.epo_rel,LC.ic_pyme,LC.ic_epo"+
				" ,(select sum(fn_monto_autorizado_total)"+
				" 		from com_linea_credito lc2"+
				" 		where cg_tipo_solicitud <> 'A'"+
				" 		and df_vencimiento_adicional > sysdate"+
				" 		and ic_if not in("+ic_if+") and lc.ic_pyme = lc2.ic_pyme) as lineas_otros"+
				" , to_char(sysdate,'dd/mm/yyyy') as FECHA_HOY2, CPN.ic_nombre as PRODUCTO_NAFIN, CM.ic_moneda as MONEDA"+
				" , CPN.ic_producto_nafin as PRODUCTO_NAFIN_CVE, to_char(LC.df_captura,'DD-MON-YYYY') as FECHA_SOLICITUD_INV "+
				" ,lc.ic_linea_credito_padre, CP.in_plazo_dias, null as cuenta_clabe, LC.cg_cuenta_clabe, CM.cd_nombre as NOMBRE_MONEDA"+
				" ,to_char(LC.df_vencimiento,'dd/mm/yyyy') as FECHA_VENCIMIENTO,to_char(LC.df_limite_disposicion,'dd/mm/yyyy') as FECHA_LIMITE_DISP"+
				" ,PY.in_numero_sirac , LC.ic_estatus_linea_anterior ESTATUS_ANT"+
				" ,to_char(LC.df_autorizacion,'dd/mm/yyyy') as FECHA_AUT"+
				" ,PY.in_numero_troya"+
				" ,LC.ig_numero_solic_troya"+
				" ,LC.cg_causa_rechazo"+
				" ,EL2.cd_descripcion as DESC_EST_ANT "+
				" ,LC.ic_tabla_amort " +
				" ,LC.cg_linea_tipo " +
				" ,ta.cd_descripcion as nombreTipoPago " +
				" from COM_LINEA_CREDITO LC, COMREL_PYME_EPO PE,COMCAT_PYME PY,COM_DOMICILIO D,COMCAT_ESTATUS_LINEA EL "+
				" ,COMREL_PRODUCTO_EPO PRE,COMREL_PYME_EPO_X_PRODUCTO PP,COMCAT_EPO E,COMCAT_ESTADO ES, COMCAT_ESTATUS_LINEA EL2 "+
				" ,comcat_tabla_amort ta " +
				" , (select count(pexp.ic_epo) as EPO_REL "+
				"		,pexp.ic_pyme "+
				"		, iexp.ic_if  "+
				"		, pexp.ic_producto_nafin AS PROD_REL"+
				"		from comrel_pyme_epo_x_producto pexp "+
				"		, comrel_if_epo_x_producto iexp "+
				"		where pexp.ic_producto_nafin in  ("+ ic_producto_nafin +")"+
				"		and pexp.cs_habilitado in('S','H') "+
				"		and  pexp.ic_epo = iexp.ic_epo "+
				"		and  iexp.ic_producto_nafin in ("+ ic_producto_nafin +")"+
				"		and iexp.cs_habilitado in('S') "+
				"		and  iexp.ic_producto_nafin = pexp.ic_producto_nafin "+
				"		group by pexp.ic_pyme, iexp.ic_if, pexp.ic_producto_nafin ) VE"+
				" ,COMCAT_PRODUCTO_NAFIN CPN, COMCAT_MONEDA CM, COMCAT_PLAZO CP "+
				" where PY.ic_pyme = PE.ic_pyme "+
				" and PY.ic_pyme = D.ic_pyme "+
				" and PE.ic_pyme = PP.ic_pyme "+
				" and PE.ic_epo = PP.ic_epo "+
				" and PP.ic_pyme = LC.ic_pyme "+
				" and PP.ic_epo = LC.ic_epo " + 
				" and LC.ic_tabla_amort = ta.ic_tabla_amort" );
		if(tipo.equals("bloqueo")) {
			qrySentencia.append(" and LC.ic_estatus_linea in(3,7,5) and LC.cg_tipo_solicitud in ('I','R') ");
		} else if(tipo.equals("cambio_estatus")) {
			qrySentencia.append(" and LC.ic_estatus_linea in(1)");
		}
		qrySentencia.append(
			" and PE.cs_habilitado = 'S'"+
			" and PP.cs_habilitado in('S','H')"+
			" and PE.ic_epo = E.ic_epo"+
			" and E.cs_habilitado = 'S'"+
			" and E.ic_epo = PRE.ic_epo"+
			" and PRE.cs_habilitado = 'S'"+
			" and EL.ic_estatus_linea = LC.ic_estatus_linea "+
			" and EL2.ic_estatus_linea (+) = LC.ic_estatus_linea_anterior "+
			" and D.ic_estado = ES.ic_estado"+
			" AND d.cs_fiscal = 'S'"+
			" and LC.ic_if ="+ic_if+
			" and LC.ic_pyme = VE.ic_pyme(+)"+
			" and LC.ic_if = VE.ic_if(+)"+
			" and PRE.ic_producto_nafin = PP.ic_producto_nafin  "+
			" and LC.ic_producto_nafin IN ("+ic_producto_nafin+")  "+
			" and LC.ic_producto_nafin = PP.ic_producto_nafin  "+
			" and LC.ic_producto_nafin = VE.prod_rel (+) "+
			" and LC.df_vencimiento_adicional > sysdate"+				
			" and PRE.ic_producto_nafin = LC.ic_producto_nafin "+
			" and PP.ic_producto_nafin = CPN.ic_producto_nafin "+
			" and CM.ic_moneda = LC.ic_moneda"+
			" and LC.ic_plazo = CP.ic_plazo(+) "+
			condicion);
		
		System.out.println("consultaIFBloqueo["+qrySentencia+"]");
		rs = con.queryDB(qrySentencia.toString());
		while(rs.next()) {
			vecColumnas = new Vector();
/*0*/		vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO")==null)?"":rs.getString("IC_LINEA_CREDITO"));
/*1*/		vecColumnas.addElement((rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO"));
/*2*/		vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
/*3*/		vecColumnas.addElement((rs.getString("ESTADO")==null)?"":rs.getString("ESTADO"));
/*4*/		vecColumnas.addElement((rs.getString("DIRECCION")==null)?"":rs.getString("DIRECCION"));
/*5*/		vecColumnas.addElement((rs.getString("CG_TELEFONO1")==null)?"":rs.getString("CG_TELEFONO1"));
/*6*/		vecColumnas.addElement((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*7*/		vecColumnas.addElement((rs.getString("FECHA_SOLICITUD")==null)?"":rs.getString("FECHA_SOLICITUD"));
/*8*/		vecColumnas.addElement((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*9*/		vecColumnas.addElement((rs.getString("FECHA_HOY")==null)?"":rs.getString("FECHA_HOY"));
/*10*/		vecColumnas.addElement((rs.getString("FN_LINEA_AUTOMATICA")==null||"0".equals(rs.getString("FN_LINEA_AUTOMATICA")))?defLineaAut:rs.getString("FN_LINEA_AUTOMATICA"));
/*11*/		vecColumnas.addElement((rs.getString("IC_ESTATUS_LINEA")==null)?"":rs.getString("IC_ESTATUS_LINEA"));
/*12*/		vecColumnas.addElement((rs.getString("CG_TIPO_SOLICITUD")==null)?"":rs.getString("CG_TIPO_SOLICITUD"));
/*13*/		vecColumnas.addElement((rs.getString("FN_MONTO_SOLICITADO")==null)?"0.00":rs.getString("FN_MONTO_SOLICITADO"));
/*14*/		vecColumnas.addElement((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
/*15*/		vecColumnas.addElement((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
/*16*/		vecColumnas.addElement((rs.getString("CUENTA")==null)?"":rs.getString("CUENTA"));
/*17*/		vecColumnas.addElement((rs.getString("FN_LINEA_OPCIONAL")==null)?"0.00":rs.getString("FN_LINEA_OPCIONAL"));
/*18*/		vecColumnas.addElement((rs.getString("FN_LINEA_INDIVIDUAL")==null)?"0.00":rs.getString("FN_LINEA_INDIVIDUAL"));
/*19*/		vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?(String)vecColumnas.get(10):rs.getString("FN_MONTO_AUTORIZADO"));
/*20*/		vecColumnas.addElement((rs.getString("EPO_REL")==null)?"0":rs.getString("EPO_REL"));
/*21*/		vecColumnas.addElement((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*22*/		vecColumnas.addElement((rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO"));

			strAux =	(rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
		    while(strAux.length()<7) {
				strAux = "0"+strAux;
			}
			strAux = (String)vecColumnas.get(12) +strAux + (String)vecColumnas.get(0);
				
/*23*/		vecColumnas.addElement(strAux);     		//folioArmado
/*24*/		vecColumnas.addElement((rs.getString("CS_LINEA_INDIVIDUAL")==null)?"":rs.getString("CS_LINEA_INDIVIDUAL"));
/*25*/ 	  	vecColumnas.addElement(defLineaAut);
/*26*/ 		vecColumnas.addElement(defLineaInd);
/*27*/ 		vecColumnas.addElement(defLineaMax);
/*28*/		vecColumnas.addElement((rs.getString("LINEAS_OTROS")==null)?"0.00":rs.getString("LINEAS_OTROS"));
/*29*/		vecColumnas.addElement((rs.getString("FECHA_HOY2")==null)?"":rs.getString("FECHA_HOY2"));
/*30*/		vecColumnas.addElement((rs.getString("PRODUCTO_NAFIN")==null)?"":rs.getString("PRODUCTO_NAFIN"));
/*31*/		vecColumnas.addElement((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*32*/		vecColumnas.addElement((rs.getString("PRODUCTO_NAFIN_CVE")==null)?"":rs.getString("PRODUCTO_NAFIN_CVE"));
/*33*/		vecColumnas.addElement((rs.getString("FECHA_SOLICITUD_INV")==null)?"":rs.getString("FECHA_SOLICITUD_INV"));
/*34*/		vecColumnas.addElement((rs.getString("ic_linea_credito_padre")==null)?"":rs.getString("ic_linea_credito_padre"));
/*35*/		vecColumnas.addElement((rs.getString("IN_PLAZO_DIAS")==null)?"":rs.getString("IN_PLAZO_DIAS"));
/*36*/		vecColumnas.addElement((rs.getString("CG_CUENTA_CLABE")==null)?"":rs.getString("CG_CUENTA_CLABE"));
/*37*/		vecColumnas.addElement((rs.getString("NOMBRE_MONEDA")==null)?"":rs.getString("NOMBRE_MONEDA"));
/*38*/		vecColumnas.addElement((rs.getString("FECHA_VENCIMIENTO")==null)?"":rs.getString("FECHA_VENCIMIENTO"));
/*39*/		vecColumnas.addElement((rs.getString("FECHA_LIMITE_DISP")==null)?"":rs.getString("FECHA_LIMITE_DISP"));
/*40*/		vecColumnas.addElement((rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC"));
/*41*/		vecColumnas.addElement((rs.getString("ESTATUS_ANT")==null)?"":rs.getString("ESTATUS_ANT"));
/*42*/		vecColumnas.addElement((rs.getString("FECHA_AUT")==null)?"":rs.getString("FECHA_AUT"));
/*43*/		vecColumnas.addElement((rs.getString("IN_NUMERO_TROYA")==null)?"":rs.getString("IN_NUMERO_TROYA"));
/*44*/		vecColumnas.addElement((rs.getString("IG_NUMERO_SOLIC_TROYA")==null)?"":rs.getString("IG_NUMERO_SOLIC_TROYA"));
/*45*/		vecColumnas.addElement((rs.getString("CG_CAUSA_RECHAZO")==null)?"":rs.getString("CG_CAUSA_RECHAZO"));
/*46*/		vecColumnas.addElement((rs.getString("DESC_EST_ANT")==null)?"":rs.getString("DESC_EST_ANT"));
/*47*/		vecColumnas.addElement((rs.getString("CG_CAUSA_RECHAZO")==null)?"":rs.getString("CG_CAUSA_RECHAZO"));


/*48*/		vecColumnas.addElement((rs.getString("ic_tabla_amort")==null)?"":rs.getString("ic_tabla_amort")); //F011-2006
/*49*/		vecColumnas.addElement((rs.getString("cg_linea_tipo")==null)?"":rs.getString("cg_linea_tipo"));	//F011-2006
/*50*/		vecColumnas.addElement((rs.getString("nombreTipoPago")==null)?"":rs.getString("nombreTipoPago"));	//F011-2006

			vecFilas.addElement(vecColumnas);
		}
	} catch(Exception e) {
		System.out.println("Consulta IF Bloqueo Exception" + e.getMessage());
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return vecFilas;
}


 
/**
 * Consulta de ???
 * @param ic_epo Clave epo
 * @param ic_if Clave IF
 * @param ic_pyme Clave Pyme
 * @param ic_linea Clave de la linea
 * @param ic_producto_nafin Clave del producto
 */
  
public Vector consultaNafinBloqueo(String ic_epo, String ic_if,
		String ic_pyme,String ic_linea, String ic_producto_nafin)
		throws NafinException {
	StringBuffer condicion = new StringBuffer();
	StringBuffer qrySentencia = new StringBuffer();
	Vector		vecFilas 				= new Vector();
	Vector		vecColumnas 			= new Vector();
	ResultSet	rs						= null;
	AccesoDB	con 					= null;
	String 		conTipoSolicitud		= "";
	String 		conNumeroSirac			= "";
	String		conConsecutivo			= "";
	String		strAux					= "";

	try {
		System.out.println("****** consultaNafinBloqueo (1) ******");
		con = new AccesoDB();
		con.conexionDB();
//        getDatosComunes(ic_if,ic_epo,con);
		if(!"".equals(ic_linea)) {
			System.out.println("****** consultaNafinBloqueo (2) ******");
			setFolioArmado(ic_linea);
			conTipoSolicitud = getTipoSolicitud();
			conNumeroSirac   = getNumeroSirac();
			conConsecutivo   		 = getConsecutivo().trim();
			condicion.append(" AND LC.ic_linea_credito = "+conConsecutivo+
					" AND LC.cg_tipo_solicitud = '"+conTipoSolicitud+"'"+
					" AND PY.in_numero_sirac = "+conNumeroSirac);
		}
		System.out.println("****** consultaNafinBloqueo (3) ******");
		if(!"".equals(ic_epo)) {
			condicion.append(" AND LC.ic_epo = "+ ic_epo);
			if(!"".equals(ic_pyme)&&!"TODOS".equals(ic_pyme)) {
				condicion.append(" AND LC.ic_pyme = "+ic_pyme);
			}
			if(!"".equals(ic_if)&&!"TODOS".equals(ic_if)) {
				condicion.append(" AND  LC.ic_if = "+ic_if);
			}
		}
		System.out.println("****** consultaNafinBloqueo (4) ******");
		qrySentencia.append(" SELECT LC.ic_linea_credito,E.cg_razon_social as NOMBRE_EPO ,I.cg_razon_social as NOMBRE_IF ,PY.cg_razon_social AS NOMBRE_PYME"+
				" ,decode(LC.cg_tipo_solicitud,'I','Inicial','R','Renovacion','Ampliacion') as TIPO_SOLICITUD,to_char(LC.df_captura,'dd/mm/yyyy') as FECHA_SOL,LC.fn_monto_solicitado,LC.fn_monto_autorizado"+
				" ,F.cd_nombre as BANCO_SERVICIO "+
				" ,LC.cg_numero_cuenta "+ 
				" ,PY.in_numero_sirac"+
				" ,CPN.ic_nombre as PRODUCTO_NAFIN, CPN.ic_producto_nafin as PRODUCTO_NAFIN_CVE"+
				" ,to_char(LC.df_autorizacion_if,'dd/mm/yyyy') as F_AUTORIZACION_IF"+
				" ,(SELECT SUM(fn_monto_autorizado_total) "+
				" 		FROM com_linea_credito lc2 "+
				"		WHERE cg_tipo_solicitud <> 'A' "+
				" 		AND df_vencimiento_adicional > SYSDATE "+
				" 		AND ic_if NOT IN(lc.ic_if) AND lc.ic_pyme = lc2.ic_pyme) AS lineas_otros "+
				" , LC.fn_monto_autorizado - LC.fn_saldo_linea AS monto_ejercido "+
				" , LC. fn_saldo_linea	AS monto_disponible, LC.ic_moneda, LC.cg_cuenta_clabe	"+
				" ,VE.epo_rel, to_char(sysdate,'dd/mm/yyyy') as FECHA_HOY, CM.cd_nombre as NOMBRE_MONEDA"+
				" ,EL.cd_descripcion as ESTATUS, CP.in_plazo_dias, LC.ic_plazo,to_char(LC.df_vencimiento,'dd/mm/yyyy') as FECHA_VENCIMIENTO"+
				" ,to_char(LC.df_limite_disposicion,'dd/mm/yyyy') as FECHA_LIMITE_DISP, LC.ic_financiera "+
				" ,PRE.fn_linea_automatica"+
				" ,LC.fn_linea_opcional,LC.fn_linea_individual,LC.fn_monto_autorizado"+
				" ,LC.cg_tipo_solicitud, LC.ic_epo, LC.ic_if, LC.ic_pyme, LC.ic_estatus_linea, LC.ic_estatus_linea_anterior ESTATUS_ANT "+
				" ,LC.ic_tabla_amort " +
				" ,LC.cg_linea_tipo " +
				" ,ta.cd_descripcion as nombreTipoPago " +
				" FROM COM_LINEA_CREDITO LC, COMREL_PYME_EPO PE,COMCAT_PYME PY "+
				" ,COMREL_PYME_EPO_X_PRODUCTO PP,COMCAT_EPO E,COMCAT_IF I,COMCAT_FINANCIERA F"+
				" ,COMREL_IF_EPO IE,COMREL_IF_EPO_X_PRODUCTO IEP"+
				" ,COMCAT_PRODUCTO_NAFIN CPN "+
				" ,comcat_tabla_amort ta "+
				" , (select count(pexp.ic_epo) as EPO_REL "+
				"		,pexp.ic_pyme "+
				"		, iexp.ic_if  "+
				"		, pexp.ic_producto_nafin AS PROD_REL"+
				"		from comrel_pyme_epo_x_producto pexp "+
				"		, comrel_if_epo_x_producto iexp "+
				"		where pexp.ic_producto_nafin in  ("+ ic_producto_nafin +")"+
				"		and pexp.cs_habilitado in('S','H') "+
				"		and  pexp.ic_epo = iexp.ic_epo "+
				"		and  iexp.ic_producto_nafin in ("+ ic_producto_nafin +")"+
				"		and iexp.cs_habilitado in('S') "+
				"		and  iexp.ic_producto_nafin = pexp.ic_producto_nafin "+
				"		group by pexp.ic_pyme, iexp.ic_if, pexp.ic_producto_nafin ) VE"+
				" , COMCAT_MONEDA CM, COMCAT_ESTATUS_LINEA EL, COMCAT_PLAZO CP, COMREL_PRODUCTO_EPO PRE "+
				" WHERE PY.ic_pyme = PE.ic_pyme "+
				" and PE.ic_pyme = PP.ic_pyme "+
				" and PE.ic_epo = PP.ic_epo "+
				" and PP.ic_pyme = LC.ic_pyme "+
				" and PP.ic_epo = LC.ic_epo "+
				" and LC.ic_tabla_amort = ta.ic_tabla_amort " +
				" and LC.ic_estatus_linea in(5,9)"+
				" and PE.cs_habilitado = 'S'"+
				" and PP.cs_habilitado in('S','H')"+
				" and PP.ic_producto_nafin in ("+ ic_producto_nafin +")"+
				" and PE.ic_epo = E.ic_epo"+
				" and E.cs_habilitado = 'S'"+
				" and LC.ic_if = I.ic_if"+
				" and F.ic_financiera = LC.ic_financiera"+ //quitar el join
				" and I.ic_if = IE.ic_if"+
				" and E.ic_epo = IE.ic_epo"+
				" and PP.ic_producto_nafin = LC.ic_producto_nafin "+ //arlette
				" and IEP.ic_producto_nafin = LC.ic_producto_nafin "+ //arlette
				" and LC.cg_tipo_solicitud in ('I','R') "+ //MPCS .- Solo se hacen cambios sobre las lineas iniciales y de renovacion
				" and IE.cs_vobo_nafin = 'S'");
		if (!"4".equals(ic_producto_nafin)) {
			qrySentencia.append(" and IE.cs_aceptacion = 'S'");
		}
		qrySentencia.append(
				" and IE.ic_if = IEP.ic_if"+
				" and IE.ic_epo = IEP.ic_epo"+
				" and IEP.ic_producto_nafin in ("+ ic_producto_nafin + ")" +
				" and IEP.cs_habilitado = 'S'"+
				" and PP.ic_producto_nafin = CPN.ic_producto_nafin "+
				" and LC.ic_pyme = VE.ic_pyme(+)"+
				" and LC.ic_if = VE.ic_if(+)"+
				" and CM.ic_moneda = LC.ic_moneda"+
				" and EL.ic_estatus_linea = LC.ic_estatus_linea "+
				" and E.ic_epo = PRE.ic_epo"+
				" and PRE.cs_habilitado = 'S'"+
				" and PRE.ic_producto_nafin = PP.ic_producto_nafin  "+
				" and PRE.ic_producto_nafin = VE.prod_rel "+
				" and LC.ic_plazo = CP.ic_plazo(+) "+
				condicion+
				" Order by PRODUCTO_NAFIN, NOMBRE_IF, NOMBRE_PYME");
		System.out.println("consultaNafinBloqueo["+qrySentencia+"]");
		rs = con.queryDB(qrySentencia.toString());
		while(rs.next()) {
				vecColumnas = new Vector();
/*0*/		vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO")==null)?"":rs.getString("IC_LINEA_CREDITO"));
/*1*/		vecColumnas.addElement((rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO"));
/*2*/		vecColumnas.addElement((rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF"));
/*3*/		vecColumnas.addElement((rs.getString("NOMBRE_PYME")==null)?"":rs.getString("NOMBRE_PYME"));
/*4*/		vecColumnas.addElement((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*5*/		vecColumnas.addElement((rs.getString("FECHA_SOL")==null)?"":rs.getString("FECHA_SOL"));
/*6*/		vecColumnas.addElement((rs.getString("FN_MONTO_SOLICITADO")==null)?"0":rs.getString("FN_MONTO_SOLICITADO"));
/*7*/		vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?"0":rs.getString("FN_MONTO_AUTORIZADO"));
/*8*/		vecColumnas.addElement((rs.getString("BANCO_SERVICIO")==null)?"":rs.getString("BANCO_SERVICIO"));
/*9*/		vecColumnas.addElement((rs.getString("cg_numero_cuenta")==null)?"":rs.getString("cg_numero_cuenta"));

			strAux = (rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
			while(strAux.length()<7)
				strAux = "0"+strAux;
			strAux = rs.getString("TIPO_SOLICITUD").substring(0,1) +strAux + (String)vecColumnas.get(0);
/*10*/		vecColumnas.addElement(strAux);     		//folioArmado
/*11*/		vecColumnas.addElement((rs.getString("PRODUCTO_NAFIN")==null)?"":rs.getString("PRODUCTO_NAFIN"));
/*12*/		vecColumnas.addElement((rs.getString("PRODUCTO_NAFIN_CVE")==null)?"":rs.getString("PRODUCTO_NAFIN_CVE"));
/*13*/		vecColumnas.addElement((rs.getString("F_AUTORIZACION_IF")==null)?"":rs.getString("F_AUTORIZACION_IF"));
/*14*/		vecColumnas.addElement((rs.getString("LINEAS_OTROS")==null)?"":rs.getString("LINEAS_OTROS"));
/*15*/		vecColumnas.addElement((rs.getString("MONTO_EJERCIDO")==null)?"":rs.getString("MONTO_EJERCIDO"));
/*16*/		vecColumnas.addElement((rs.getString("MONTO_DISPONIBLE")==null)?"":rs.getString("MONTO_DISPONIBLE"));
/*17*/		vecColumnas.addElement((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*18*/		vecColumnas.addElement((rs.getString("cg_cuenta_clabe")==null)?"":rs.getString("cg_cuenta_clabe"));

/*19*/		vecColumnas.addElement((rs.getString("EPO_REL")==null)?"0":rs.getString("EPO_REL"));
/*20*/		vecColumnas.addElement((rs.getString("FECHA_HOY")==null)?"":rs.getString("FECHA_HOY"));
/*21*/		vecColumnas.addElement((rs.getString("NOMBRE_MONEDA")==null)?"":rs.getString("NOMBRE_MONEDA"));
/*22*/		vecColumnas.addElement((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*23*/		vecColumnas.addElement((rs.getString("IN_PLAZO_DIAS")==null)?"":rs.getString("IN_PLAZO_DIAS"));
/*24*/		vecColumnas.addElement((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
/*25*/		vecColumnas.addElement((rs.getString("FECHA_VENCIMIENTO")==null)?"":rs.getString("FECHA_VENCIMIENTO"));
/*26*/		vecColumnas.addElement((rs.getString("FECHA_LIMITE_DISP")==null)?"":rs.getString("FECHA_LIMITE_DISP"));
/*27*/		vecColumnas.addElement((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
/*28*/		vecColumnas.addElement((rs.getString("FN_LINEA_AUTOMATICA")==null)?"":rs.getString("FN_LINEA_AUTOMATICA"));
/*29*/		vecColumnas.addElement((rs.getString("FN_LINEA_OPCIONAL")==null)?"0.00":rs.getString("FN_LINEA_OPCIONAL"));
/*30*/		vecColumnas.addElement((rs.getString("FN_LINEA_INDIVIDUAL")==null)?"0.00":rs.getString("FN_LINEA_INDIVIDUAL"));
/*31*/		vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?(String)vecColumnas.get(10):rs.getString("FN_MONTO_AUTORIZADO"));
/*32*/		vecColumnas.addElement((rs.getString("CG_TIPO_SOLICITUD")==null)?"":rs.getString("CG_TIPO_SOLICITUD"));
/*33*/		vecColumnas.addElement((rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO"));
/*34*/		vecColumnas.addElement((rs.getString("IC_IF")==null)?"":rs.getString("IC_IF"));
/*35*/		vecColumnas.addElement((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*36*/		vecColumnas.addElement((rs.getString("IC_ESTATUS_LINEA")==null)?"":rs.getString("IC_ESTATUS_LINEA"));
/*37*/		vecColumnas.addElement((rs.getString("ESTATUS_ANT")==null)?"":rs.getString("ESTATUS_ANT"));

/*38*/		vecColumnas.addElement((rs.getString("ic_tabla_amort")==null)?"":rs.getString("ic_tabla_amort")); //F011-2006
/*39*/		vecColumnas.addElement((rs.getString("cg_linea_tipo")==null)?"":rs.getString("cg_linea_tipo"));	//F011-2006
/*40*/		vecColumnas.addElement((rs.getString("nombreTipoPago")==null)?"":rs.getString("nombreTipoPago"));	//F011-2006

			vecFilas.addElement(vecColumnas);
			System.out.println("****** consultaNafinBloqueo (5) ******");
		}
	}catch(Exception e) {
		System.out.println("ConsultaNafin Exception "+e);
		throw new NafinException("SIST0001");
	}finally {
		if(con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return vecFilas;
}

// AEC

/****************************************************************************/


//Omar Torres

public void generaSolicitud(String ic_linea_credito,String ic_epo,String ic_pyme,String ic_if,
	String cg_tipo_solicitud,String ic_linea_padre, String ic_producto_nafin) throws NafinException {
    AccesoDB  con           = null;
    String    qrySentencia  = "";
    boolean   resultado     = true;
    boolean   continuar     = false;
    ResultSet rs            = null;
      try {
        con = new AccesoDB();
        con.conexionDB();
  			qrySentencia = 	"select ic_linea_credito from com_linea_credito"+
							" where ic_linea_credito ="+ic_linea_credito+
							" and ic_estatus_linea is null";
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				continuar = true;
      con.cierraStatement();
      if("I".equals(cg_tipo_solicitud)&&continuar)
        {
				qrySentencia =	"update com_linea_credito " +
				  				"set ic_pyme = "+ ic_pyme +
					  			", ic_epo = "+ic_epo+
						  		", ic_producto_nafin = "+ ic_producto_nafin +
							  	", ic_if = "+ic_if+
  								", cg_tipo_solicitud = 'I'"+
	  							", ic_estatus_linea = 1"+
		  						",df_captura = sysdate"+
			  					" where ic_linea_credito = "+ic_linea_credito;
        }
      else if(continuar)
        {
				qrySentencia = 	" UPDATE COM_LINEA_CREDITO "+
								" SET(IC_PYME,IC_IF,CG_TIPO_SOLICITUD"+
								" ,IC_ESTATUS_LINEA,IC_LINEA_CREDITO_PADRE"+
								" ,IC_FINANCIERA,CG_NUMERO_CUENTA,cg_numero_cuenta_if,IC_EPO,IC_PRODUCTO_NAFIN,DF_CAPTURA,IC_MONEDA)"+
								" = (SELECT '"+ic_pyme+"',ic_if,'"+cg_tipo_solicitud+"'"+
								" ,1,IC_LINEA_CREDITO,IC_FINANCIERA,CG_NUMERO_CUENTA,cg_numero_cuenta_if,IC_EPO,IC_PRODUCTO_NAFIN,SYSDATE,IC_MONEDA"+
								" FROM COM_LINEA_CREDITO WHERE IC_LINEA_CREDITO ="+ic_linea_padre+")"+
								" WHERE IC_LINEA_CREDITO = "+ic_linea_credito;
        }
      else
        throw new NafinException("ANTI0019");
      con.ejecutaSQL(qrySentencia);
      }catch(NafinException ne){
          resultado = false;
          throw ne;
      } catch(Exception e) {
          resultado = false;
          throw new NafinException("SIST0001");
      } finally {
          con.terminaTransaccion(resultado);
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
      }
    }

    public void autorizaPyme(String [] lineas,int iElementos, String ic_producto_nafin)
  	throws NafinException
  	{
    String		lineasCredito	= "";
    String		qrySentencia	= "";
    AccesoDB	con 					= null;
    boolean		resultado 		= true;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
				for(int i=0;i<iElementos;i++)
					{
					if(!"".equals(lineasCredito))
							lineasCredito += ",";
					lineasCredito += lineas[i];
		      }
				qrySentencia = "update com_linea_credito set cs_aceptacion_pyme='S' where ic_linea_credito in("+lineasCredito+") " +
                        " and ic_producto_nafin = " + ic_producto_nafin;
         System.out.println(qrySentencia);
		    con.ejecutaSQL(qrySentencia);
			} catch(Exception e) {
        	resultado = false;
      		throw new NafinException("SIST0001");
      }	finally {
      	if(con.hayConexionAbierta())
        	{
					con.terminaTransaccion(resultado);
          con.cierraConexionDB();
          }
      }
    }




public Vector getComboLineas(String ic_pyme,String ic_epo,String tipoSolicitud, String ic_producto_nafin)
throws NafinException
{
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        if("I".equals(tipoSolicitud)) {
          qrySentencia  =	"select a.ic_if, a.ic_if||' '||a.cg_razon_social,c.ic_epo,'' "+
				" from comcat_if a,"+
			//	" (select ic_if"+
			//	" from comcat_if"+
			//	" minus"+
			//	" select ic_if from "+
			//	" com_linea_credito"+
			//	" where ic_estatus_linea in(1,2,3,5,7,9,12) "+
			//	" and ic_producto_nafin = " + ic_producto_nafin +
			//	" and ic_pyme= "+ic_pyme+" )  b,"+
        " comrel_if_epo_x_producto c "+
				" where a.cs_habilitado = 'S'"+
				" and c.ic_producto_nafin = "+ ic_producto_nafin +
				" and c.ic_if = a.ic_if"+
				" and c.cs_habilitado = 'S'"+
				" and c.ic_epo = "+ic_epo+
				" AND NOT EXISTS(SELECT * "+//modificacion (6/05/2008) limita a solo una linea de credito
				" FROM com_linea_credito "+//modificacion (6/05/2008) limita a solo una linea de credito
				"	WHERE ic_estatus_linea IN(1, 2, 3, 5, 7, 9, 12) "+//modificacion (6/05/2008) limita a solo una linea de credito
				" AND ic_producto_nafin = " + ic_producto_nafin +//modificacion (6/05/2008) limita a solo una linea de credito
				" AND ic_pyme = "+ic_pyme+") ";//modificacion (6/05/2008) limita a solo una linea de credito
        if (!"4".equals(ic_producto_nafin)) {
	        qrySentencia  +=
				" and a.ic_if in("+
				"		select distinct B.ic_if"+
				"		from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C,comcat_moneda D, comrel_pyme_if E "+
				"		where A.ic_epo = E.ic_epo and B.ic_if = E.ic_if and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria and "+
				"		C.ic_moneda = D.ic_moneda and E.cs_borrado = 'N'"+
				"		and E.cs_vobo_if = 'S' "+
				" 		and E.ic_epo = "+ic_epo+
				" 		and C.ic_pyme = "+ic_pyme+
				")";
        }
	} else if("A".equals(tipoSolicitud)) {
        	qrySentencia  =	"select a.ic_linea_credito, a.ic_linea_credito||' '||b.cg_razon_social,b.ic_if" +
				" from com_linea_credito a,comcat_if b" +
				" where a.ic_if = b.ic_if" +
				" and a.ic_pyme = " +ic_pyme +				//para la pyne
				" and a.ic_epo = "+ic_epo+					//&iquest;para cualquier EPO?
				" and a.cs_aceptacion_pyme = 'S'" +				//autorizada pyme
				" and df_vencimiento_adicional > (sysdate)" +		//vigente
				" and b.cs_habilitado = 'S'";
				if(!"4".equals(ic_producto_nafin)){
					qrySentencia += " and a.cg_tipo_solicitud in('I','R')" +		//debe ser inicial
						" and  ic_estatus_linea = 5 ";					//autorizada nafin
				}else{
					qrySentencia += " and a.cg_tipo_solicitud =('I')" +		//debe ser inicial
						" and  ic_estatus_linea = 12 ";						//vigente
				}
        } else if("R".equals(tipoSolicitud)) {
        	qrySentencia  =	"select a.ic_linea_credito, b.ic_if||' '||b.cg_razon_social,b.ic_if" +
				" from com_linea_credito a,comcat_if b" +
				" where a.ic_if = b.ic_if" +
				" and a.ic_pyme = " +ic_pyme +				//para la pyne
				" and a.ic_epo = "+ic_epo;					//&iquest;para cualquier EPO?
				if(!"4".equals(ic_producto_nafin)){
					qrySentencia += " and df_vencimiento_adicional < sysdate" +		//vencida
						" and  ic_estatus_linea in (5,8,10) "+					//autorizada nafin y canceladas if y nafin
						" and a.cg_tipo_solicitud in('I','R')"+						//debe ser inicial
						" and a.ic_linea_credito not in "+
						" ("+
						"	SELECT IC_LINEA_CREDITO "+
						"	FROM COM_LINEA_CREDITO"+
						"	WHERE "+
						"	(CG_TIPO_SOLICITUD = 'R' AND DF_VENCIMIENTO_ADICIONAL > sysdate)"+
						"	or"+
						"	(cg_tipo_solicitud = 'R' and ic_estatus_linea = 1)"+
						"	or ic_linea_credito in"+
						"		(select ic_linea_credito_padre from com_linea_credito"+
						"	where cg_tipo_solicitud = 'R'  and ic_estatus_linea!=4 )"+
						" )";
				}else{
					qrySentencia += " and trunc(df_vencimiento_adicional) < trunc(sysdate+90) "+	//vencida o a noventa dias de vencer
						" and  ic_estatus_linea in (11,12) "+										//vigentes y vencidas
						" and a.cg_tipo_solicitud in('I')";											//debe ser inicial
				}
			qrySentencia +=
				" and a.cs_aceptacion_pyme = 'S'" +				//autorizada pyme
				" and b.cs_habilitado = 'S' ";
	}
	System.out.println(qrySentencia);
  	rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
	        vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
	        vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
	        vecFilas.addElement(vecColumnas);
	      }
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
    }

public Vector getLineasProceso(String ic_pyme,String ic_epo,String tipoSolicitud,String ic_producto_nafin)
throws NafinException
{
    String	qrySentencia          = "";
    AccesoDB	con                   = null;
    ResultSet	rs 		      = null;
    Vector	vecFilas	      = new Vector();
    Vector	vecColumnas	      = new Vector();
    	try {
//      System.out.println("********llega con producto: " +ic_producto_nafin);
      	con = new AccesoDB();
        con.conexionDB();
          qrySentencia  =	" select distinct i.cg_razon_social,decode(lc.ic_estatus_linea,5,decode(pep.cs_habilitado,'H','Afiliado','En proceso'),'En proceso')"+
				" from comcat_if i,"+
        " com_linea_credito lc,"+
        " comrel_pyme_epo_x_producto pep"+
				" where lc.ic_estatus_linea"+
				" in(1,2,3,5,7,9)"+
				" and lc.ic_pyme= "+ic_pyme+
				" and lc.ic_if = i.ic_if"+
				" and i.cs_habilitado = 'S'"+
				" and lc.ic_producto_nafin = "+ ic_producto_nafin +
				" and lc.ic_epo = "+ic_epo+
        " and pep.ic_epo = lc.ic_epo"+
        " and pep.ic_pyme = lc.ic_pyme"+
        " and lc.cg_tipo_solicitud = '"+tipoSolicitud+"'"+
        " and pep.ic_producto_nafin = lc.ic_producto_nafin"+
				" and lc.ic_if in("+
				"		select distinct B.ic_if"+
				"		from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C,comcat_moneda D, comrel_pyme_if E "+
				"		where A.ic_epo = E.ic_epo and B.ic_if = E.ic_if and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria and "+
				"		C.ic_moneda = D.ic_moneda and E.cs_borrado = 'N'"+
				"		and E.cs_vobo_if = 'S' "+
				" 		and E.ic_epo = "+ic_epo+
				" 		and C.ic_pyme = "+ic_pyme+
				")";

  	rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
	        vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
	        vecFilas.addElement(vecColumnas);
	      }
      } catch(Exception e){
           throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
     return vecFilas;
    }

	public Vector consultaPymeXProd(String lineas[],int elementos, String ic_producto_nafin)
  	throws NafinException
    {
    String 		lineasCredito	= "";
    String		qrySentencia	= "";
    ResultSet	rs 						= null;
    AccesoDB	con						= null;
    Vector		vecColumnas		= new Vector();
    Vector		vecFilas			= new Vector();
    int i=0;

    try {
			for(i=0;i<elementos;i++)
				{
				if(!"".equals(lineasCredito))
					lineasCredito += ",";
				lineasCredito += lineas[i];
				}
      con = new AccesoDB();
      con.conexionDB();

      	qrySentencia =	
			" SELECT lc.ic_linea_credito, i.cg_razon_social, lc.fn_monto_autorizado,"   +
			"        TO_CHAR (df_autorizacion_nafin, 'dd/mm/yyyy') AS autorizacion,"   +
			"        TO_CHAR (df_vencimiento, 'dd/mm/yyyy') AS vencimiento,"   +
			"        lc.cg_numero_cuenta num_cta, f.cd_nombre AS banco_servicio,"   +
			"        TO_CHAR (lc.df_limite_disposicion, 'dd/mm/yyyy') AS limite,"   +
			"        TO_CHAR (df_autorizacion_nafin, 'DD-MON-YY') AS autorizacionc,"   +
			"        TO_CHAR (df_vencimiento, 'DD-MON-YY') AS vencimientoc,"   +
			"        TO_CHAR (lc.df_limite_disposicion, 'DD-MON-YY') AS limitec,"   +
			"        m.cd_nombre AS moneda,"   +
			"        DECODE ("   +
			"           lc.cg_tipo_solicitud,"   +
			"           'I', 'Inicial',"   +
			"           'R', 'Renovacion',"   +
			"           'Ampliacion'"   +
			"        ) AS tipo_solicitud,"   +
			"        py.in_numero_sirac,"   +
			"        lc.cg_linea_tipo"   +
			"   FROM com_linea_credito lc,"   +
			"        comcat_if i,"   +
			"        comcat_financiera f,"   +
			"        comcat_moneda m,"   +
			"        comcat_pyme py"   +
			"  WHERE lc.ic_if = i.ic_if"   +
			"    AND lc.ic_linea_credito IN ("+lineasCredito+")"   +
			"    AND lc.ic_financiera = f.ic_financiera(+)"   +
			"    AND lc.ic_moneda = m.ic_moneda"   +
			"    AND lc.ic_pyme = py.ic_pyme"  ;
          if(!ic_producto_nafin.equals("")){
            qrySentencia +=	" and LC.ic_producto_nafin = " + ic_producto_nafin;
          }
          System.out.println("************** " + qrySentencia + " **********");
			rs = con.queryDB(qrySentencia);
			i=0;
			while(rs.next())
				{
        String numeroSirac 		= "";
        String tipoSolicitud	= "";
        String folioSolicitud	= "";

        vecColumnas = new Vector();

				vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO")==null)?"-1":rs.getString("IC_LINEA_CREDITO"));
				vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
				vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?"0.00":rs.getString("FN_MONTO_AUTORIZADO"));
				vecColumnas.addElement((rs.getString("AUTORIZACION")==null)?"":rs.getString("AUTORIZACION"));
				vecColumnas.addElement((rs.getString("VENCIMIENTO")==null)?"":rs.getString("VENCIMIENTO"));
				vecColumnas.addElement((rs.getString("NUM_CTA")==null)?"":rs.getString("NUM_CTA"));
				vecColumnas.addElement((rs.getString("BANCO_SERVICIO")==null)?"":rs.getString("BANCO_SERVICIO"));
				vecColumnas.addElement((rs.getString("LIMITE")==null)?"":rs.getString("LIMITE"));
				vecColumnas.addElement((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));

        folioSolicitud = (rs.getString("IC_LINEA_CREDITO")==null)?"":rs.getString("IC_LINEA_CREDITO");
  		numeroSirac = (rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
		tipoSolicitud = (rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD");

					while(numeroSirac.length()<7)
								numeroSirac = "0"+numeroSirac;

vecColumnas.addElement(tipoSolicitud.substring(0,1)+numeroSirac+folioSolicitud);

				vecColumnas.addElement((rs.getString("AUTORIZACIONC")==null)?"":rs.getString("AUTORIZACIONC"));
				vecColumnas.addElement((rs.getString("VENCIMIENTOC")==null)?"":rs.getString("VENCIMIENTOC"));
				vecColumnas.addElement((rs.getString("LIMITEC")==null)?"":rs.getString("LIMITEC"));

      	vecFilas.addElement(vecColumnas);
		    }
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    } finally {
    	if(con.hayConexionAbierta())
      	con.cierraConexionDB();
    }
  return vecFilas;
	}	//consulta Pyme

// Modificado  28/02/2003		--CARP
// Hacer la consulta Multiproducto (Inventarios, Pedidos y Distribuidores)
	public Vector consultaPymeInv(String ic_pyme,String fecha_sol,
    							  String ic_producto_nafin, String ic_epo,
                                  String tipoSolic,String Eestatus,
                                  String ic_moneda, String ic_if)
  	throws NafinException
  	{
    String		condicion 		= "";
    String		condicion2 		= "";
    String		qrySentencia 	= "";
		Vector		vecFilas 			= new Vector();
    Vector		vecColumnas 	= new Vector();
    ResultSet	rs						= null;
		AccesoDB	con 					= null;
			try {
				con = new AccesoDB();
				con.conexionDB();
				if(!"".equals(fecha_sol))
					condicion += " AND to_date(to_char(LC.df_captura,'dd/mm/yyyy'),'dd/mm/yyyy') <= to_date('"+fecha_sol+"','dd/mm/yyyy')";
				else
					condicion += " AND LC.df_captura between add_months(sysdate,-3) and sysdate";

				if(!"".equals(ic_epo))
					condicion += " AND PP.ic_epo = " + ic_epo;

				if(!"".equals(tipoSolic))
					condicion += "  AND LC.cg_tipo_solicitud ='" + tipoSolic + "'" ;

				if(!"".equals(Eestatus))
					condicion += "  AND LC.ic_estatus_linea =" + Eestatus  ;

				if(!"".equals(ic_moneda))
					condicion += "   AND  LC.ic_moneda = " + ic_moneda  ;

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
// Modificado 03/03/2003		-- CARP
/* ANT	        if(!"0".equals(ic_producto_nafin)){
				condicion += "   AND LC.ic_producto_nafin = " + ic_producto_nafin ;
        		condicion2 += " AND pexp.ic_producto_nafin =   "+ ic_producto_nafin;
    	     }
             */
	        if(!"0".equals(ic_producto_nafin)){
				condicion += "   AND LC.ic_producto_nafin = " + ic_producto_nafin ;
        		condicion2 += " AND pexp.ic_producto_nafin =   "+ ic_producto_nafin;
			} else {
				condicion += "   AND LC.ic_producto_nafin in (2,5)";
        		condicion2 += " AND pexp.ic_producto_nafin in (2,5)  ";
            }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

        if(!"".equals(ic_if))
					condicion += "   AND LC.ic_if = " + ic_if ;

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
// Agregado 10/03/2003  -- CARP   Ordena dependiendo del producto que se busque
			String lsOrden = "";
			if (ic_producto_nafin.equals("5"))
    			lsOrden = " order by LC.IC_PRODUCTO_NAFIN DESC,LC.ic_linea_credito";
		    else
				lsOrden = " order by LC.IC_PRODUCTO_NAFIN,LC.ic_linea_credito";
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

				qrySentencia =
					" SELECT   DECODE (lc.cg_tipo_solicitud, 'I', 'Inicial', 'R', 'Renovacion', 'Ampliacion') AS tipo_solicitud,"   +
					"          lc.ic_linea_credito,"   +
					"          TO_CHAR (lc.df_captura, 'dd/mm/yyyy') AS fecha_captura,"   +
					"          el.cd_descripcion AS estatus, lc.ic_estatus_linea,"   +
					"          lc.fn_monto_autorizado, p.in_plazo_dias AS plazo,"   +
					"          TO_CHAR (lc.df_vencimiento, 'dd/mm/yyyy') AS fecha_ven,"   +
					"          TO_CHAR (lc.df_vencimiento, 'DD-MON-YY') AS fecha_venc,"   +
					"          f.cd_nombre AS banco_servicio, lc.cg_numero_cuenta,"   +
					"          lc.cs_aceptacion_pyme, lc.cg_causa_rechazo,"   +
					"          i.cg_razon_social AS nombre_if, py.in_numero_sirac, ve.epo_rel,"   +
					"          e.cg_razon_social AS nombre_epo,"   +
					"          TO_CHAR (lc.df_autorizacion_if, 'dd/mm/yyyy') AS df_autorizacion_if,"   +
					"          TO_CHAR (lc.df_autorizacion_if, 'DD-MON-YY') AS df_autorizacion_ifc,"   +
					"          lc.fn_saldo_linea_total, lc.fn_monto_autorizado_total,"   +
					"          ve.ic_if AS ic_if, pn.ic_nombre AS producto,"   +
					"          TO_CHAR (lc.df_captura, 'dd/mm/yyyy') AS df_captura,"   +
					"          TO_CHAR (lc.df_limite_disposicion, 'dd/mm/yyyy') AS df_disposicion,"   +
					"          TO_CHAR (lc.df_captura, 'DD-MON-YY') AS df_capturac,"   +
					"          TO_CHAR (lc.df_limite_disposicion, 'DD-MON-YY') AS df_disposicionc,"   +
					"          cm.cd_nombre AS moneda, NVL (lc.fn_saldo_linea, 0) AS remanente,"   +
					"          NVL (lc.fn_monto_autorizado - lc.fn_saldo_linea, 0) AS ejercido,"   +
					"          lc.cg_numero_cuenta_if, tci.cd_descripcion AS tipo_cobro,"   +
					"          lc.ic_producto_nafin AS cve_producto, lc.cg_cuenta_clabe,"   +
					"          (SELECT SUM (fn_monto_autorizado_total)"   +
					"             FROM com_linea_credito lc2"   +
					"            WHERE cg_tipo_solicitud <> 'A'"   +
					"              AND df_vencimiento_adicional > SYSDATE"   +
					"              AND ic_if NOT IN (lc.ic_if)"   +
					"              AND lc.ic_pyme = lc2.ic_pyme) AS lineas_otros,"   +
					"          lc.ic_moneda, TO_CHAR (lc.df_cambio, 'dd/mm/yyyy') AS fecha_cambio,"   +
					"          i.ig_tipo_piso AS tipo_piso, ta.cd_descripcion AS tipo_amort,"   +
					"          lc.fn_monto_liberado AS monto_liberado, lc.cg_linea_tipo AS tipo_linea,"   +
					"          lc.ic_tabla_amort, ta.ic_tabla_amort,"   +
					"          TO_CHAR (lc.df_autorizacion, 'dd/mm/yyyy') AS df_autorizacion,"   +
					"          TO_CHAR (lc.df_autorizacion, 'DD-MON-YY') AS df_autorizacionc"   +
					"     FROM com_linea_credito lc,"   +
					"          comcat_estatus_linea el,"   +
					"          comcat_if i,"   +
					"          comcat_tipo_cobro_interes tci,"   +
					"          comrel_pyme_epo_x_producto pp,"   +
					"          comcat_financiera f,"   +
					"          comcat_plazo p,"   +
					"          comcat_epo e,"   +
					"          comrel_producto_epo pe,"   +
					"          comrel_if_epo_x_producto iep,"   +
					"          comcat_pyme py,"   +
					"          comcat_producto_nafin pn,"   +
					"          comcat_tabla_amort ta,"   +
					"          comcat_moneda cm,"   +
					"          (SELECT   COUNT (pexp.ic_epo) AS epo_rel, pexp.ic_pyme,"   +
					"                    iexp.ic_if AS ic_if, pexp.ic_producto_nafin AS producto"   +
					"               FROM comrel_pyme_epo_x_producto pexp,"   +
					"                    comrel_if_epo_x_producto iexp"   +
					"              WHERE pexp.cs_habilitado IN ('S', 'H')"   +
					"                AND pexp.ic_epo = iexp.ic_epo"   +
					condicion2 +
					"                AND iexp.cs_habilitado IN ('S')"   +
					"                AND iexp.ic_producto_nafin = pexp.ic_producto_nafin"   +
					"           GROUP BY pexp.ic_pyme, iexp.ic_if, pexp.ic_producto_nafin) ve"   +
					"    WHERE lc.ic_pyme = "+ic_pyme+
					"      AND pp.ic_pyme = lc.ic_pyme"   +
					"      AND pp.ic_epo = lc.ic_epo"   +
					"      AND py.ic_pyme = lc.ic_pyme"   +
					"      AND el.ic_estatus_linea = lc.ic_estatus_linea"   +
					"      AND lc.ic_financiera = f.ic_financiera(+)"   +
					"      AND lc.ic_plazo = p.ic_plazo(+)"   +
					"      AND lc.ic_epo = e.ic_epo"   +
					"      AND e.cs_habilitado = 'S'"   +
					"      AND e.ic_epo = pe.ic_epo"   +
					"      AND lc.ic_producto_nafin = pp.ic_producto_nafin"   +
					"      AND pp.ic_producto_nafin = pn.ic_producto_nafin"   +
					"      AND pp.ic_producto_nafin = pe.ic_producto_nafin"   +
					"      AND pp.ic_producto_nafin = iep.ic_producto_nafin"   +
					"      AND lc.ic_moneda = cm.ic_moneda"   +
					"      AND pe.cs_habilitado = 'S'"   +
					"      AND i.cs_habilitado = 'S'"   +
					"      AND i.ic_if = iep.ic_if"   +
					"      AND e.ic_epo = iep.ic_epo"   +
					"      AND iep.cs_habilitado = 'S'"   +
					"      AND i.ic_if = lc.ic_if"   +
					"      AND lc.ic_pyme = ve.ic_pyme(+)"   +
					"      AND lc.ic_if = ve.ic_if(+)"   +
					"      AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes(+)"   +
					"      AND lc.ic_producto_nafin = ve.producto"   +
					"      AND lc.ic_tabla_amort = ta.ic_tabla_amort(+)"   +
					condicion  + lsOrden;
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    System.out.println("Query: " + qrySentencia);
                    rs = con.queryDB(qrySentencia);
				while(rs.next()) {
					String numeroSirac 		= "";
					String tipoSolicitud	= "";
					String folioSolicitud	= "";
					String estatus 			= "";
					String aceptacion		= "";
					
					vecColumnas = new Vector();
					folioSolicitud = (rs.getString("IC_LINEA_CREDITO")==null)?"":rs.getString("IC_LINEA_CREDITO");
					tipoSolicitud =  (rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD");

					vecColumnas.addElement(folioSolicitud);
					vecColumnas.addElement(tipoSolicitud);
					vecColumnas.addElement((rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF"));
					vecColumnas.addElement((rs.getString("FECHA_CAPTURA")==null)?"":rs.getString("FECHA_CAPTURA"));
					vecColumnas.addElement((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));		//5

					estatus = (rs.getString("IC_ESTATUS_LINEA")==null)?"":rs.getString("IC_ESTATUS_LINEA");
					vecColumnas.addElement(estatus);
					vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO")==null)?"0":rs.getString("FN_MONTO_AUTORIZADO"));
					vecColumnas.addElement((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
					vecColumnas.addElement((rs.getString("FECHA_VEN")==null)?"":rs.getString("FECHA_VEN"));
					vecColumnas.addElement((rs.getString("BANCO_SERVICIO")==null)?"":rs.getString("BANCO_SERVICIO"));     //10
					vecColumnas.addElement((rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA"));
					
					aceptacion = (rs.getString("CS_ACEPTACION_PYME")==null)?"":rs.getString("CS_ACEPTACION_PYME");
					vecColumnas.addElement(aceptacion);
					vecColumnas.addElement((rs.getString("CG_CAUSA_RECHAZO")==null)?"":rs.getString("CG_CAUSA_RECHAZO"));

					numeroSirac =(rs.getString("IN_NUMERO_SIRAC")==null)?"":rs.getString("IN_NUMERO_SIRAC");
					while(numeroSirac.length()<7)
						numeroSirac = "0"+numeroSirac;

					vecColumnas.addElement(tipoSolicitud.substring(0,1)+numeroSirac+folioSolicitud);	//14
					vecColumnas.addElement((rs.getString("EPO_REL")==null)?"0":rs.getString("EPO_REL"));
					vecColumnas.addElement((rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO"));
					vecColumnas.addElement((rs.getString("DF_AUTORIZACION_IF")==null)?"":rs.getString("DF_AUTORIZACION_IF"));
					vecColumnas.addElement((rs.getString("FN_SALDO_LINEA_TOTAL")==null)?"0":rs.getString("FN_SALDO_LINEA_TOTAL"));
					vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"0":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
					vecColumnas.addElement((rs.getString("IC_IF")==null)?"":rs.getString("IC_IF"));      //20
					
					vecColumnas.addElement((rs.getString("producto")==null)?"":rs.getString("producto"));
					vecColumnas.addElement((rs.getString("df_disposicion")==null)?"":rs.getString("df_disposicion"));
					vecColumnas.addElement((rs.getString("moneda")==null)?"":rs.getString("moneda"));
					
					vecColumnas.addElement((rs.getString("ejercido")==null)?"0":rs.getString("ejercido"));
					vecColumnas.addElement((rs.getString("remanente")==null)?"0":rs.getString("remanente"));   //25
					
					vecColumnas.addElement((rs.getString("df_capturaC")==null)?"":rs.getString("df_capturaC"));
					vecColumnas.addElement((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
					vecColumnas.addElement((rs.getString("df_autorizacion_ifC")==null)?"":rs.getString("df_autorizacion_ifC"));
					vecColumnas.addElement((rs.getString("df_disposicionC")==null)?"":rs.getString("df_disposicionC"));  //29
					vecColumnas.addElement((rs.getString("cg_numero_cuenta_if")==null)?"":rs.getString("cg_numero_cuenta_if"));
					vecColumnas.addElement((rs.getString("tipo_cobro")==null)?"":rs.getString("tipo_cobro"));

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
					vecColumnas.addElement((rs.getString("CVE_PRODUCTO")==null)?"":rs.getString("CVE_PRODUCTO"));	//Agregado 04/03/2003	--CARP
					vecColumnas.addElement((rs.getString("lineas_otros")==null)?"":rs.getString("lineas_otros"));	//Agregado 04/03/2003	--CARP
					vecColumnas.addElement((rs.getString("cg_cuenta_clabe")==null)?"":rs.getString("cg_cuenta_clabe"));	//Agregado 10/03/2003	--CARP
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
					vecColumnas.addElement((rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda"));
					vecColumnas.addElement((rs.getString("fecha_cambio")==null)?"":rs.getString("fecha_cambio"));
					vecColumnas.addElement((rs.getString("TIPO_PISO")==null)?"":rs.getString("TIPO_PISO"));
					vecColumnas.addElement((rs.getString("tipo_amort")==null)?"":rs.getString("tipo_amort"));
					vecColumnas.addElement((rs.getString("monto_liberado")==null)?"":rs.getString("monto_liberado"));
					vecColumnas.addElement((rs.getString("tipo_linea")==null)?"":rs.getString("tipo_linea"));
					
					vecColumnas.addElement((rs.getString("df_autorizacion")==null)?"":rs.getString("df_autorizacion"));
					vecColumnas.addElement((rs.getString("df_autorizacionc")==null)?"":rs.getString("df_autorizacionc"));
					vecFilas.addElement(vecColumnas);
          if("5".equals(estatus)&&"N".equals(aceptacion))
        		pymesAutorizadas++;
		    	}
        }catch(Exception e) {
        	throw new NafinException("SIST0001");
        }finally {
        	if(con.hayConexionAbierta())
          	con.cierraConexionDB();
        }
	return vecFilas;
	}




//M&Eacute;TODOS DE RAFAEL FLORES

    public void VerificaLineaCredito(String NumCliente) throws NafinException {
		ResultSet			sqlrs  = null;
		int					Lineas = 0;
		int					Pedidos = 0;
    AccesoDB		conn = new AccesoDB();
        try {
			conn.conexionDB();
			String query = " select /*+ RULE */ count(1) "+
						   "   from com_linea_credito lc"+
						   "   where lc.ic_pyme = "+NumCliente+
						   "     and lc.ic_estatus_linea = 5"+
						   "     and lc.cs_aceptacion_pyme = 'S'"+
						   "     and (lc.CG_TIPO_SOLICITUD = 'I' or lc.CG_TIPO_SOLICITUD = 'R')"+
						   "     and lc.DF_VENCIMIENTO_ADICIONAL > sysdate"+
						   "     and lc.fn_saldo_linea_total > 0";
			sqlrs = conn.queryDB(query);
	        if (sqlrs.next()) {
				Lineas = sqlrs.getInt(1);
			}
			if (Lineas==0) throw new NafinException("ANTI0006");
			sqlrs.close();
			conn.cierraStatement();
			//REVISA QUE NO TENGA PEDIDOS CON ESTATUS CREDITO VENCIDO
			query = " select count(1) "+
					" from com_pedido "+
					" where ic_estatus_pedido = 17"+
					" and ic_pyme = "+NumCliente;
			sqlrs = conn.queryDB(query);
	        if (sqlrs.next()) {
				Pedidos = sqlrs.getInt(1);
			}
			if (Pedidos>0) throw new NafinException("ANTI0026");
			sqlrs.close();
			conn.cierraStatement();
		} catch (NafinException ne) {
			throw ne;
		} catch(Exception e) {
			System.out.println("SeleccionPymeEJB Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (conn.hayConexionAbierta()) conn.cierraConexionDB();
		}
	}

    public Vector LineaCredito(String NumCliente, String NumEpo,String NumMoneda) throws NafinException {
		ResultSet			sqlrs  = null;
		Vector 				afuera1 = null,
							afuera2 = null,
							adentro= null;
		int 				diferencia=0,
							nueva=0,
							usada=0,
							datos = 0;
		Vector afuera = new Vector();
    AccesoDB	conn = new AccesoDB();
        try {
			conn.conexionDB();
			afuera1 = new Vector();
			afuera2 = new Vector();

			String query = 
				"select "+
				"       if.cg_razon_social||'-'||fi.cd_nombre||'-'||lc.cg_numero_cuenta,"+
				"       lc.fn_saldo_linea_total,"+
				"       lc.ic_linea_credito,"+
				"       lc.fn_monto_autorizado_total-lc.fn_saldo_linea_total,"+
				"		nvl(pi.cs_plazo_maximo,'N') as EXISTE_PLAZO,"+
				"		pn.ig_plazo_maximo,"+
				"	    nvl(rpi.fn_aforo,pn.fn_aforo) as FN_AFORO,"+
				"		decode(rpi.fn_aforo,null,'N','I') as ORIGEN_PARAMETRO,"+
				"		if.ic_if as CVE_IF, "+
				"		to_char(df_limite_disposicion, 'dd/mm/yyyy') df_limite_disposicion"+				
				"  from com_linea_credito lc,"+
				"       comcat_financiera fi,"+
				"       comrel_pyme_epo pe,"+
				"       comcat_if if,"+
				"       comrel_pyme_epo_x_producto pep,"+
				"				comcat_producto_nafin pn,"+
				"				(select rpi.cs_plazo_maximo,rpi.ic_if from comrel_producto_if rpi"+
				"					where rpi.ic_producto_nafin = 2) pi,"+
				"	    comrel_producto_if rpi"+
				" where lc.ic_pyme = " +NumCliente +
				"   and lc.ic_estatus_linea = 5"+
				"	and lc.ic_producto_nafin = 2 " +
				"   and lc.cs_aceptacion_pyme = 'S'"+
				"   and fi.ic_financiera = lc.ic_financiera"+
				"   and if.ic_if = rpi.ic_if(+)"+
				"	and rpi.ic_producto_nafin(+) = 2"+
				"   and pe.ic_epo = " + NumEpo+
				"   and pe.ic_pyme = lc.ic_pyme"+
				"   and pe.CS_HABILITADO = 'S'"+
				"   and pep.ic_epo = pe.ic_epo"+
				"   and pep.ic_pyme = pe.ic_pyme"+
				"   and pep.IC_PRODUCTO_NAFIN = 2"+
				"   and pep.CS_HABILITADO in ('H')"+
				"   and if.ic_if = lc.ic_if"+
				"   and if.CS_HABILITADO = 'S'"+
				"   and (lc.CG_TIPO_SOLICITUD = 'I' or"+
				"        lc.CG_TIPO_SOLICITUD = 'R')"+
				"   and lc.DF_VENCIMIENTO_ADICIONAL >= sysdate"+
				"   and lc.fn_saldo_linea_total > 0"+
				"					 and lc.ic_if in("+
				"						select distinct B.ic_if"+
				"						from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C, comcat_moneda D, comrel_pyme_if E "+
				"						where A.ic_epo = E.ic_epo and B.ic_if = E.ic_if and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria"+
				"							and C.ic_moneda = D.ic_moneda and E.cs_borrado = 'N' "+
				"							and E.ic_epo = "+NumEpo+
				" 						and C.ic_pyme = "+NumCliente+
				"							and D.ic_moneda = "+NumMoneda+
				"						)"+
				"					and pn.ic_producto_nafin = 2"+
				"					and if.ic_if = pi.ic_if(+)";
			//System.out.println(query);
			sqlrs = conn.queryDB(query);

	        while (sqlrs.next()) {
				diferencia = sqlrs.getInt(4);
				if (diferencia<=0) {
					adentro = new Vector();
					adentro.addElement(sqlrs.getString(1).trim());
					adentro.addElement(sqlrs.getString(2).trim());
					adentro.addElement(sqlrs.getString(3).trim());
					adentro.addElement(sqlrs.getString("EXISTE_PLAZO").trim());
					adentro.addElement(sqlrs.getString("IG_PLAZO_MAXIMO").trim());
					adentro.addElement(sqlrs.getString("FN_AFORO"));
					adentro.addElement(sqlrs.getString("ORIGEN_PARAMETRO"));
					adentro.addElement(sqlrs.getString("CVE_IF"));
					adentro.addElement(sqlrs.getString("df_limite_disposicion"));
					afuera1.addElement(adentro);
					nueva++;
				 } else {
					adentro = new Vector();
					adentro.addElement(sqlrs.getString(1).trim());
					adentro.addElement(sqlrs.getString(2).trim());
					adentro.addElement(sqlrs.getString(3).trim());
					adentro.addElement(sqlrs.getString("EXISTE_PLAZO").trim());
					adentro.addElement(sqlrs.getString("IG_PLAZO_MAXIMO").trim());
					adentro.addElement(sqlrs.getString("FN_AFORO"));
					adentro.addElement(sqlrs.getString("ORIGEN_PARAMETRO"));
					adentro.addElement(sqlrs.getString("CVE_IF"));
					adentro.addElement(sqlrs.getString("df_limite_disposicion"));
					afuera2.addElement(adentro);
					usada++;
				 }
			  	 datos ++;
	        }
			sqlrs.close();
			conn.cierraStatement();
			if (datos > 0) {
				if (usada > 0) {
					afuera = afuera2;
				} else {
					afuera = afuera1;
				}
			} else {
				throw new NafinException("ANTI0006");
			}
		} catch (NafinException ne) {
			throw ne;
        } catch(Exception e) {
			System.out.println("SeleccionPymeEJB Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			if (conn.hayConexionAbierta()) conn.cierraConexionDB();
		}
		return(afuera);
    }

	public Vector consultaDoctos(String ic_pyme,String ic_if,String ic_linea,String cg_tipo_solicitud,String ic_producto_nafin)
  throws NafinException{
  	AccesoDB	con						= null;
    ResultSet rs						= null;
    String		qrySentencia	= "";
    String		condicion 	= "";
    Vector		vecFilas 	= new Vector();
    Vector		vecColumnas 	= null;
    String 		tipoPersona	= "";
    try {
        con = new AccesoDB();
        con.conexionDB();
	tipoPersona = getTipoPersona(ic_pyme,con);
	if("F".equals(tipoPersona))
                                   condicion = " AND (D.cg_tipo_persona in('F') or D.cg_tipo_persona is null)";
        if("M".equals(tipoPersona))
                                   condicion = " AND (D.cg_tipo_persona  in('M') or D.cg_tipo_persona is null)";
        if(cg_tipo_solicitud!=null)
                                   condicion += " AND D.cg_tipo_solicitud = '"+cg_tipo_solicitud+"'";

					qrySentencia =
              "SELECT * FROM ("+
              " SELECT D.ic_documento"+
              " ,D.cg_nombre_corto"+
							" ,D.cd_documento"+
							" ,nvl(D.cg_nombre_archivo,IPD.cg_nombre_archivo) as cg_nombre_archivo"+
							" ,I.cg_razon_social"+
							" ,cg_calle"+
							" ,decode(cg_numero_ext,null,'S/N',cg_numero_ext) as cg_numero_ext "+
							" ,decode(cg_numero_int,null,' ','-'||cg_numero_int) as cg_numero_int"+
							" ,cg_colonia"+
							" ,cg_municipio"+
							" ,cn_cp,decode(D.cg_nombre_archivo,null,I.ic_if,0) as IC_IF"+
              " ,nvl(D.cs_obligatorio,IPD.cs_obligatorio) as cs_obligatorio";
          qrySentencia += (ic_linea==null)?" ,'' as cg_tipo_solicitud":" 	,LC.cg_tipo_solicitud";
          qrySentencia += " FROM comcat_documentos D"+
							" 	,comrel_if_producto_docto IPD"+
							" 	,comcat_if I"+
							" 	,com_domicilio DOM";
          qrySentencia += (ic_linea==null)?"":" 	,com_linea_credito LC";
					qrySentencia += " WHERE I.ic_if = IPD.ic_if(+)";
          qrySentencia += (ic_linea==null)?" 	AND I.ic_if ="+ic_if:" 	AND LC.ic_linea_credito ="+ic_linea+" AND I.ic_if = LC.ic_if AND LC.ic_producto_nafin = "+ic_producto_nafin;
          qrySentencia += " AND IPD.ic_producto_nafin = "+ic_producto_nafin+
              " AND IPD.ic_producto_nafin = D.ic_producto_nafin"+
							" AND D.ic_documento = IPD.ic_documento"+
							" AND DOM.cs_fiscal = 'S'"+
							" AND DOM.ic_if = I.ic_if"+
							condicion+
                                                        " UNION ALL"   +
                                                        " SELECT d.ic_documento, d.cg_nombre_corto, d.cd_documento,"   +
                                                        "        d.cg_nombre_archivo AS cg_nombre_archivo,"   +
                                                        "        '' as cg_razon_social, '' as cg_calle,"   +
                                                        "        '' AS cg_numero_ext,"   +
                                                        "        '' AS cg_numero_int,"   +
                                                        "        '' AS cg_colonia,'' AS  cg_municipio,'' AS cn_cp,"   +
                                                        "        0 AS ic_if,"   +
                                                        "        d.cs_obligatorio,"   +
                                                        "        '' AS cg_tipo_solicitud"   +
                                                        "  FROM comcat_documentos d"   +
                                                        " WHERE d.ic_documento not in("   +
                                                        "  	   select ic_documento"   +
                                                        " 	   from comrel_if_producto_docto IPD";
                                        qrySentencia += (ic_linea==null)?"":" 	,com_linea_credito LC";
                                        qrySentencia += " 	   where IPD.ic_producto_nafin = " +ic_producto_nafin;
                                        qrySentencia += (ic_linea==null)?" 	AND IPD.ic_if ="+ic_if:" 	AND LC.ic_linea_credito ="+ic_linea+" AND IPD.ic_if = LC.ic_if AND LC.ic_producto_nafin = "+ic_producto_nafin;
                                        qrySentencia += " 	   ) "   +
                                                        condicion +
                                                        " AND D.ic_producto_nafin = "+ic_producto_nafin+
                                                        " AND D.cg_nombre_archivo IS NOT NULL"+
							" ) ORDER BY ic_documento";
                                        System.out.println(qrySentencia);
					rs = con.queryDB(qrySentencia);
						while(rs.next()){
            	vecColumnas = new Vector();
/*0*/								vecColumnas.addElement((rs.getString("CD_DOCUMENTO")==null)?"":rs.getString("CD_DOCUMENTO"));
/*1*/								vecColumnas.addElement((rs.getString("CG_NOMBRE_ARCHIVO")==null)?"":rs.getString("CG_NOMBRE_ARCHIVO"));
/*2*/								vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
/*3*/								vecColumnas.addElement((rs.getString("CG_CALLE")==null)?"":rs.getString("CG_CALLE"));
/*4*/								vecColumnas.addElement((rs.getString("CG_NUMERO_EXT")==null)?"":rs.getString("CG_NUMERO_EXT"));
/*5*/								vecColumnas.addElement((rs.getString("CG_NUMERO_INT")==null)?"":rs.getString("CG_NUMERO_INT"));
/*6*/								vecColumnas.addElement((rs.getString("CG_COLONIA")==null)?"":rs.getString("CG_COLONIA"));
/*7*/								vecColumnas.addElement((rs.getString("CG_MUNICIPIO")==null)?"":rs.getString("CG_MUNICIPIO"));
/*8*/								vecColumnas.addElement((rs.getString("CN_CP")==null)?"":rs.getString("CN_CP"));
/*9*/								vecColumnas.addElement((rs.getString("IC_IF")==null)?"":rs.getString("IC_IF"));
/*10*/			        vecColumnas.addElement((rs.getString("CG_TIPO_SOLICITUD")==null)?"":rs.getString("CG_TIPO_SOLICITUD"));
/*11*/			        vecColumnas.addElement((rs.getString("CS_OBLIGATORIO")==null)?"":rs.getString("CS_OBLIGATORIO"));
/*12*/			        vecColumnas.addElement((rs.getString("CG_NOMBRE_CORTO")==null)?"":rs.getString("CG_NOMBRE_CORTO"));
              vecFilas.addElement(vecColumnas);
						}
          	con.cierraStatement();
      }catch(Exception e){
      	throw new NafinException("SIST0001");
      }finally{
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
	return vecFilas;
  }

  private String getTipoPersona(String ic_pyme,AccesoDB con)
 	throws NafinException{
	String		tipoPersona		= "";
  String		qrySentencia	= "";
  ResultSet	rs 						= null;
  	try{
				qrySentencia =	"select cs_tipo_persona from comcat_pyme"+
								" where ic_pyme = "+ ic_pyme;
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					tipoPersona = rs.getString("CS_TIPO_PERSONA");
				con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }
  return tipoPersona;
  }
  public void chkSuceptibilidad(String ic_pyme,String ic_epo) 	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    String      suceptible  = "";
    try{
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia =
        "SELECT cs_habilitado "+
        "  FROM comrel_pyme_epo_x_producto"+
        " WHERE ic_pyme = "+ic_pyme+" "+
        "   AND ic_epo = "+ic_epo+" "+
        "   AND ic_producto_nafin = 2 ";
      System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next())
        suceptible=rs.getString("cs_habilitado")==null?"":rs.getString("cs_habilitado");
      con.cierraStatement();
      if (!("S".equals(suceptible) || "H".equals(suceptible)))
        throw new NafinException("ANTI27");
    }catch(NafinException ne){
    	throw ne;
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
  }//chkSuceptibilidad

public void AutorizaNafin(String folio [],String estatus [],String [] huboCambio,String sElementos,String causa [],String ic_producto_nafin)
		throws NafinException
    {
    int i=0;
    String 		qrySentencia	= "";
    ResultSet	rs				= null;
    AccesoDB	con				= null;
    String 		lineaPadre		= "-1";
    String 		montoTotal		= "0.00";
    String 		saldoTotal		= "0.00";
    String 		fechaVen  		= "";
    String 		tipoSolicitud 	= "";
		String		rsPyme	=	"";
		String		rsEpo	=	"";
		String		rsTipoProducto	=	"";
    float  		hayCambio 		= 0.0f;
    int				iElementos	= Integer.parseInt(sElementos);
    boolean		resultado 		= true;
    try	{
      con = new AccesoDB();
      con.conexionDB();
      for(i=0;i<iElementos&&resultado;i++)
        {
        if(huboCambio[i].equals("S"))
          {

          	if ("4".equals(ic_producto_nafin)) {
          		qrySentencia =
					"SELECT ic_linea_credito_padre, cg_tipo_solicitud "+
					"  FROM com_linea_credito "+
					" WHERE ic_linea_credito = "+folio[i]+" ";
				System.out.println(qrySentencia);
				rs = con.queryDB(qrySentencia);
              	if(rs.next()) {
                	lineaPadre  	= (rs.getString(1)==null)?"":rs.getString(1);
	                tipoSolicitud	= (rs.getString(2)==null)?"":rs.getString(2);
				}
				con.cierraStatement();
				if ("R".equals(tipoSolicitud)) {
					qrySentencia =
						"SELECT to_char(df_vencimiento_adicional,'dd/mm/yyyy') "+
						"  FROM com_linea_credito "+
						" WHERE ic_linea_credito = "+lineaPadre+" ";
					System.out.println(qrySentencia);
					rs = con.queryDB(qrySentencia);
	              	if(rs.next())
	                	fechaVen = (rs.getString(1)==null)?"":rs.getString(1);
					con.cierraStatement();

					String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

					Calendar f1 = Calendar.getInstance();
					Calendar f2 = Calendar.getInstance();
					f1.set(Integer.parseInt(fechaVen.substring(6,10)),Integer.parseInt(fechaVen.substring(3,5))-1,Integer.parseInt(fechaVen.substring(0,2)));
					java.util.Date dFechaVenc1 = f1.getTime();
					f2.set(Integer.parseInt(fechaHoy.substring(6,10)),Integer.parseInt(fechaHoy.substring(3,5))-1,Integer.parseInt(fechaHoy.substring(0,2)));
					java.util.Date dFechaVenc2 = f2.getTime();
					int res = dFechaVenc1.compareTo(dFechaVenc2);
					//                System.out.println("\n******"+dFechaVenc1+" "+dFechaVenc2+" "+res);
					if (res>0)
					fechaVen = fechaHoy;
					qrySentencia  =
					"UPDATE com_linea_credito "+
					" SET DF_VENCIMIENTO_ADICIONAL = '"+fechaVen+"' "+
					" WHERE IC_LINEA_CREDITO = "+lineaPadre+" ";
					System.out.println(qrySentencia);
					con.ejecutaSQL(qrySentencia);

				}
				lineaPadre = tipoSolicitud = fechaVen = "";
			}

          qrySentencia =  " update com_linea_credito "+
                          " set ic_estatus_linea = "+estatus[i]+
                          " ,df_autorizacion_nafin = sysdate"+
                          " ,cg_causa_rechazo = '"+ causa[i] +"'";
			if("4".equals(ic_producto_nafin))
				qrySentencia += " ,cs_aceptacion_pyme='S' ";
				qrySentencia += " where ic_linea_credito = "+folio[i];
				System.out.println(qrySentencia);
              con.ejecutaSQL(qrySentencia);
           if(estatus[i].equals("12"))        //si es autorizada, sumar los saldos,montos y fechas
              {
              qrySentencia =  "select ic_linea_credito_padre,fn_monto_autorizado"+
                              " ,fn_saldo_linea_total,to_char(df_vencimiento,'dd/mm/yyyy')"+
                              " ,cg_tipo_solicitud,ic_epo,ic_pyme,ic_producto_nafin"+
                              " from com_linea_credito where ic_linea_credito ="+folio[i];
							System.out.println(qrySentencia);
              rs = con.queryDB(qrySentencia);
              if(rs.next())
                {
                lineaPadre  	= rs.getString(1);
                montoTotal  	= rs.getString(2);
                saldoTotal  	= rs.getString(3);
                fechaVen    	= rs.getString(4);
                tipoSolicitud = rs.getString(5);
								rsEpo					= rs.getString(6);//MODIFICAION 29/04/2008
								rsPyme				= rs.getString(7);//MODIFICAION 29/04/2008
								rsTipoProducto= rs.getString(8);//MODIFICAION 29/04/2008
                }
              con.cierraStatement();
              if("A".equals(tipoSolicitud))
                  {
                  qrySentencia =  "select to_date('"+fechaVen+"','dd/mm/yyyy')-df_vencimiento_adicional"+
                                  " from com_linea_credito where ic_linea_credito = "+lineaPadre;
                  rs = con.queryDB(qrySentencia);
                  if(rs.next())
                       hayCambio = rs.getFloat(1);
                  con.cierraStatement();
                  qrySentencia =  " update com_linea_credito set fn_monto_autorizado_total = fn_monto_autorizado_total +"+montoTotal+
                                  " ,fn_saldo_linea_total = fn_saldo_linea_total +"+saldoTotal;
                  if(hayCambio>0)
                      qrySentencia += " ,df_vencimiento_adicional = to_date('"+fechaVen+"','dd/mm/yyyy')";
                  qrySentencia += " ,cg_causa_rechazo = '"+ causa[i] +"'";
                  qrySentencia += " where ic_linea_credito = "+lineaPadre;
									System.out.println(qrySentencia);
                  con.ejecutaSQL(qrySentencia);
                  } //fin if "A".equals
							
							//SE HABILITA PRODUCTO NAFIN 4--MODIFICACION DEL (29-04-2008)
							if("I".equals(tipoSolicitud) && "4".equals(rsTipoProducto))
							{
								qrySentencia =  
									" update COMREL_PYME_EPO_X_PRODUCTO set cs_habilitado = 'S', "+
									" df_habilitado = sysdate "+
									" where ic_pyme ="+ rsPyme+
									" and ic_epo ="+rsEpo+
									" and ic_producto_nafin ="+rsTipoProducto;
								con.ejecutaSQL(qrySentencia);
							}
            }
          }       //fin if huboCambio
        }     //fin for
    } catch(Exception e) {
      System.out.println("AutorizaLineas.AutorizaNafin Exception"+e.getMessage());
      e.printStackTrace();
      resultado = false;
      throw new NafinException("ANTI0020");
		} finally {
      if(con.hayConexionAbierta())
      	{
	      con.terminaTransaccion(resultado);
	      con.cierraConexionDB();
        }
     }
 }         //fin AutorizaNafin

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ISRAEL HERRERA */
public Vector AutorizacionIF(String iNoUsuario,int numRegistrosMN,double montoAutoMN,int numRegistrosDL,double montoAutoDL,String aTipo[],String tipo_liq,String aFolio[], String aMoneda[], String aMonto [],String aEstatus[],String aTipoCobro[],String aCausa [],String aMontoAuto [],String aFechaVto [],String aBanco [],String aCuenta [],int solMN,double montoMN,int solDL, double montoDL)
  	throws NafinException
  {
    int i = 0;
    String		qrySentencia		= "";
    AccesoDB	con					= null;
    Vector		  vecColumnas		= new Vector();
    Vector		  vecFilas			= new Vector();
    ResultSet   rs            = null;
    boolean		resultado			= true;
    try {
      con = new AccesoDB();
      con.conexionDB();
      Acuse acuse = new Acuse(4,"4","dis",con);
      String cc_acuse=acuse.toString();
      qrySentencia  =
        "INSERT INTO dis_acuse4 (cc_acuse,df_acuse,ic_usuario)"+
        "VALUES('"+cc_acuse+"',sysdate,'"+iNoUsuario+"')";
      System.out.println(qrySentencia);
      con.ejecutaSQL(qrySentencia);
      System.out.println("\nAcuse creado...\n");
        qrySentencia  =
          "INSERT INTO disrel_acuse4_moneda (CC_ACUSE,IC_MONEDA,IN_TOTAL_LINEAS,FN_TOTAL_MONTO) "+
          "VALUES('"+cc_acuse+"',1,"+numRegistrosMN+","+montoAutoMN+")";
        System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
          System.out.println("\n...agregarMN\n");
        qrySentencia  =
          "INSERT INTO disrel_acuse4_moneda (CC_ACUSE,IC_MONEDA,IN_TOTAL_LINEAS,FN_TOTAL_MONTO) "+
          "VALUES('"+cc_acuse+"',54,"+numRegistrosDL+","+montoAutoDL+")";
        System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
        System.out.println("\n...agregarDL\n");
      for (i=0;i<aFolio.length;i++){
        if(!"".equals(aEstatus[i])){
            qrySentencia =
	            "UPDATE com_linea_credito "+
    	        "   SET ic_estatus_linea = "+aEstatus[i]+" "+
        	    "      , df_autorizacion_if = SYSDATE ";
            if (!"".equals(aMoneda[i]))
				qrySentencia += " , ic_moneda = '"+aMoneda[i]+"' ";
            if (!"".equals(aMonto[i]))
				qrySentencia += " , fn_monto_solicitado = '"+aMonto[i]+"' ";
            if (!"".equals(aTipoCobro[i]))
				qrySentencia += " , ic_tipo_cobro_interes = '"+aTipoCobro[i]+"' ";
            if (!"".equals(aCausa[i]))
				qrySentencia += " , cg_causa_rechazo = '"+aCausa[i]+"' ";
            if (!"".equals(aMontoAuto[i])) {
				qrySentencia +=
		            " , fn_monto_autorizado = "+aMontoAuto[i]+" "+
        		    " , fn_monto_autorizado_total = "+aMontoAuto[i]+" "+
        		    " , FN_SALDO_LINEA = "+aMontoAuto[i]+" "+
        		    " , FN_SALDO_LINEA_TOTAL = "+aMontoAuto[i]+" ";
			}
            if (!"".equals(aFechaVto[i])) {
				qrySentencia +=
		            " , df_vencimiento = TO_DATE ('"+aFechaVto[i]+"', 'dd/mm/yyyy') "+
        		    " , df_vencimiento_adicional = TO_DATE ('"+aFechaVto[i]+"', 'dd/mm/yyyy') ";
			}
            if (!"".equals(aBanco[i]))
				qrySentencia += " , ic_financiera = "+aBanco[i]+" ";
            if (!"".equals(aCuenta[i])){
	            if("I".equals(tipo_liq))
    		        qrySentencia += " , cg_numero_cuenta_if = '"+aCuenta[i]+"' ";
            	else
            		if("A".equals(tipo_liq))
              			qrySentencia += " , cg_numero_cuenta = '"+aCuenta[i]+"' ";
            }
            qrySentencia += " WHERE ic_linea_credito = "+aFolio[i]+" ";
		System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
        qrySentencia =
          "SELECT to_char(sysdate,'dd/mm/yyyy') as fecha, to_char(sysdate,'HH:MI') as hora"+
          " FROM dual";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        if(rs.next()){
          vecColumnas = new Vector();
	  			vecColumnas.addElement(cc_acuse);
	  			vecColumnas.addElement(acuse.formatear());
  				vecColumnas.addElement((rs.getString("fecha")==null)?"":rs.getString("fecha"));
	  			vecColumnas.addElement((rs.getString("hora")==null)?"":rs.getString("hora"));
        	vecFilas.addElement(vecColumnas);
        }
      con.cierraStatement();

        }//if(!"".equals(aEstatus))
      }//for (i=0;i<aFolio.length;i++)
    } catch(Exception e) {
      System.out.println("LineaCredito.AutorizacionIF Exception: "+e.getMessage());
      resultado = false;
      throw new NafinException("SIST0001");
    } finally {
			if(con.hayConexionAbierta())
      	{
	      con.terminaTransaccion(resultado);
	      con.cierraConexionDB();
        }
     }
   		return vecFilas;
  }         //fin AutorizacionIF


public Vector getDescripcion(String ic_moneda,String ic_estatus,String tipo_cobro)
  	throws NafinException
  {
    String		qrySentencia		= "";
    AccesoDB	con					= null;
    Vector		  vecColumnas		= new Vector();
    Vector		  vecFilas			= new Vector();
    ResultSet   rs            = null;
    try {
      con = new AccesoDB();
      con.conexionDB();
        qrySentencia =
          "SELECT cel.cd_descripcion, mon.cd_nombre, tci.cd_descripcion as tipo_cobro"+
          "  FROM comcat_estatus_linea cel, comcat_moneda mon, "+
          "       comcat_tipo_cobro_interes tci "+
          " WHERE cel.ic_estatus_linea = "+ic_estatus+" "+
          "   AND tci.ic_tipo_cobro_interes = "+tipo_cobro+" "+
          "   AND mon.ic_moneda = "+ic_moneda+" ";
//        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        if(rs.next()){
          vecColumnas = new Vector();
	  			vecColumnas.addElement((rs.getString("cd_nombre")==null)?"":rs.getString("cd_nombre"));
	  			vecColumnas.addElement((rs.getString("cd_descripcion")==null)?"":rs.getString("cd_descripcion"));
	  			vecColumnas.addElement((rs.getString("tipo_cobro")==null)?"":rs.getString("tipo_cobro"));
        	vecFilas.addElement(vecColumnas);
        }
      con.cierraStatement();

    } catch(Exception e) {
      System.out.println("LineaCredito.getDescripcion Exception"+e);
      throw new NafinException("SIST0001");
    } finally {
			if(con.hayConexionAbierta())
      	{
	      con.cierraConexionDB();
        }
     }
   		return vecFilas;
  }         //fin AutorizacionIF


  public boolean getDiaHabil()
   throws NafinException{
	AccesoDB	con				= null;
    ResultSet	rs				= null;
    String		qrySentencia	= "";
	boolean		bFechaInhabil	= false;
	String		cadfechahab		= "";
	int			diahab			= 1;
	boolean 	diaHabilUno 	= false;
    try{
		con = new AccesoDB();
		con.conexionDB();
		String	fechaHoy	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	mes			= fechaHoy.substring(3, 5);
		String	anyo		= fechaHoy.substring(6, 10);
		Calendar cal = new GregorianCalendar();
		cal.set(Integer.parseInt(anyo), Integer.parseInt(mes)-1, diahab);
		int iDiaSemana = cal.get(Calendar.DAY_OF_WEEK);
		if(iDiaSemana==7)
			cal.set(Integer.parseInt(anyo), Integer.parseInt(mes)-1, diahab+2);
		if(iDiaSemana==1) // 7 Sabado y 1 Domingo.
			cal.set(Integer.parseInt(anyo), Integer.parseInt(mes)-1, diahab+1);
		qrySentencia = "select cg_dia_inhabil from comcat_dia_inhabil";
    	do {
			bFechaInhabil = false;
			cadfechahab = new java.text.SimpleDateFormat("dd/MM/yyyy").format(cal.getTime());
			String diauno = cadfechahab.substring(0, 2);
			rs = con.queryDB(qrySentencia);
			while (rs.next()){
				String aux1 = (rs.getString(1))==null?"":rs.getString(1).trim();
				String aux2 = diauno+"/"+mes;
				if (aux1.equals(aux2)){
					bFechaInhabil = true;
					break;
				}
			}
			rs.close();con.cierraStatement();
			if(bFechaInhabil){
				cal.set(Integer.parseInt(anyo), Integer.parseInt(mes)-1, diahab++);
				iDiaSemana = cal.get(Calendar.DAY_OF_WEEK);
				if(iDiaSemana==7)
					cal.set(Integer.parseInt(anyo), Integer.parseInt(mes)-1, diahab+2);
				if(iDiaSemana==1) // 7 Sabado y 1 Domingo.
					cal.set(Integer.parseInt(anyo), Integer.parseInt(mes)-1, diahab+1);
			}
		} while(bFechaInhabil);
		if(fechaHoy.equals(cadfechahab))
			diaHabilUno = true;

    }catch(Exception e){
		System.out.println("Exception "+e+"  Message"+e.getMessage());
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
   return diaHabilUno;
  }//getDiaHabil()


 public Vector getComboSolicitudRelacionada(String ic_if,String ic_epo, String ic_pyme, String solicitud,String moneda,String ic_producto_nafin)
 throws NafinException
 {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        if (solicitud.equals("A")) {
			qrySentencia  =
				"SELECT lc.ic_linea_credito, lc.ic_producto_nafin,ic_if "+
				"  FROM com_linea_credito lc "+
				" WHERE lc.ic_if = "+ic_if+" "+
				"   AND lc.ic_epo = "+ic_epo+" "+
				"   AND lc.ic_pyme = "+ic_pyme+" "+
				"   AND lc.ic_producto_nafin = "+ic_producto_nafin+" "+
				"   AND lc.ic_estatus_linea = 12 "+
				"   AND lc.cg_tipo_solicitud = 'I' "+
				"   AND TRUNC (df_vencimiento_adicional) > TRUNC (SYSDATE) "+
				"   AND (SELECT COUNT (*) "+
				"          FROM dis_documento "+
				"         WHERE ic_linea_credito = lc.ic_linea_credito "+
				"           AND ic_estatus_docto = 22) = 0 "+
				"   AND (SELECT COUNT (*) "+
				"          FROM com_linea_credito "+
				"         WHERE cg_tipo_solicitud IN ('A') "+
				"           AND ic_estatus_linea IN (1,  2,  3) "+
				"           AND ic_if = "+ic_if+" "+
				"           AND ic_epo = "+ic_epo+" "+
				"           AND ic_pyme = "+ic_pyme+" "+
				"           AND lc.ic_linea_credito = ic_linea_credito_padre) = 0 ";
        }
        else {
          if (solicitud.equals("R")){

            qrySentencia  =
              "SELECT lc.ic_linea_credito "+
              "  FROM com_linea_credito lc "+
              " WHERE ic_if = "+ic_if+" "+
              "   AND ic_epo = "+ic_epo+" "+
              "   AND ic_pyme = "+ic_pyme+" "+
			  "   AND ic_producto_nafin  = "+ic_producto_nafin+" "+
			  "   AND ic_estatus_linea  in(11,12)"+
              "   AND cg_tipo_solicitud = 'I' "+
              "   AND TRUNC (df_vencimiento_adicional) < TRUNC (SYSDATE+90) "+
/*              "   AND ic_linea_credito NOT IN (SELECT ic_linea_credito_padre "+
              "                                     FROM com_linea_credito "+
              "                                    WHERE cg_tipo_solicitud IN ('I',  'R') "+
              "			                             AND ic_linea_credito_padre IS NOT NULL) "+*/
			  "   AND (SELECT COUNT (*) "+
			  "          FROM dis_documento "+
			  "         WHERE ic_linea_credito = lc.ic_linea_credito "+
			  "           AND ic_estatus_docto = 22) = 0 "+
			  "   AND (SELECT COUNT (*) "+
			  "          FROM com_linea_credito "+
			  "         WHERE cg_tipo_solicitud IN ('R') "+
			  "           AND ic_estatus_linea IN (1,  2,  3) "+
			  "           AND ic_if = "+ic_if+" "+
			  "           AND ic_epo = "+ic_epo+" "+
			  "           AND ic_pyme = "+ic_pyme+" "+
			  "           AND lc.ic_linea_credito = ic_linea_credito_padre) = 0 ";
          }
        }
        qrySentencia += " AND ic_moneda = "+moneda+" ";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString("ic_linea_credito")==null)?"":rs.getString("ic_linea_credito"));
	        vecFilas.addElement(vecColumnas);
	      }
      con.cierraStatement();
      } catch(Exception e){
      		System.out.println("Error Bean Linea de Credito.getComboSolicitudRelacionada ");
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
    }

 public String getMontoAutorizado(String ic_if, String ic_epo,String ic_pyme, String folio)
  	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    String aux="";
    try{
      con = new AccesoDB();
      con.conexionDB();
      //CONSULTA
      qrySentencia =
        "SELECT FN_MONTO_AUTORIZADO_TOTAL"+
        " FROM com_linea_credito "+
        " WHERE ic_epo = "+ic_epo+" "+
        " AND ic_if = "+ic_if+" "+
        " AND ic_pyme = "+ic_pyme+" "+
        " AND IC_LINEA_CREDITO = "+folio+" ";
        System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
        aux = (rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL");
      }
      con.cierraStatement();
    }catch(Exception e){
      		System.out.println("Error Bean Linea de Credito.getMontoAutorizado ");
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
		return aux;
  }//getMontoAutorizado



 public Vector getMontoAutFechaVto(String ic_if, String ic_epo,String ic_pyme, String folio)
  	throws NafinException{
	System.out.println("LineaCredito::getMontoAutFechaVto (E)");
    AccesoDB    con				= null;
    ResultSet   rs				= null;
    String      qrySentencia	= "";
    Vector		aux				= new Vector();
    try{
      con = new AccesoDB();
      con.conexionDB();
      //CONSULTA
      qrySentencia =
        "SELECT FN_MONTO_AUTORIZADO_TOTAL"+
        " ,TO_CHAR(DF_VENCIMIENTO_ADICIONAL,'dd/mm/yyyy') as DF_VENCIMIENTO_ADICIONAL "+
        " FROM com_linea_credito "+
        " WHERE ic_epo = "+ic_epo+" "+
        " AND ic_if = "+ic_if+" "+
        " AND ic_pyme = "+ic_pyme+" "+
        " AND IC_LINEA_CREDITO = "+folio+" ";
        System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
        aux.add((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
		aux.add((rs.getString("DF_VENCIMIENTO_ADICIONAL")==null)?"":rs.getString("DF_VENCIMIENTO_ADICIONAL"));
      }
      con.cierraStatement();
    }catch(Exception e){
      		System.out.println("LineaCredito::getMontoAutFechaVto Exception "+e);
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
	System.out.println("LineaCredito::getMontoAutFechaVto (S)");
    }
		return aux;
  }//getMontoAutorizado


 public Vector getDetalleInicial(String ic_linea_credito)
  	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    try{
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia =
        "SELECT a.cg_numero_cuenta_if, a.ic_financiera, b.cd_nombre,a.cg_numero_cuenta "+
        "  FROM com_linea_credito a, comcat_financiera b "+
        " WHERE b.ic_financiera (+) = a.ic_financiera "+
        "   AND a.ic_linea_credito = "+ic_linea_credito+" ";
      System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString("CG_NUMERO_CUENTA_IF")==null)?"":rs.getString("CG_NUMERO_CUENTA_IF"));
	        vecColumnas.addElement((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
	        vecColumnas.addElement((rs.getString("CD_NOMBRE")==null)?"":rs.getString("CD_NOMBRE"));
	        vecColumnas.addElement((rs.getString("cg_numero_cuenta")==null)?"":rs.getString("cg_numero_cuenta"));
	        vecFilas.addElement(vecColumnas);
      }
      con.cierraStatement();
    }catch(Exception e){
      		System.out.println("Error Bean Linea de Credito.getDetalleInicial ");
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
		return vecFilas;
  }//getDetalleInicial

 public boolean getLineaExistente(String moneda,String ic_if,String ic_epo, String ic_pyme)  //DEPRECATED
 throws NafinException
 {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    boolean bandera=false;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
          "SELECT ic_linea_credito"+
          " FROM com_linea_credito"+
          " WHERE cg_tipo_solicitud='I'"+
          " AND ic_if = "+ic_if+" "+
          " AND ic_epo = "+ic_epo+" "+
          " AND ic_pyme = "+ic_pyme+" "+
          " AND ic_moneda = "+moneda+" ";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        if (rs.next())
          bandera = true;
        con.cierraStatement();
      } catch(Exception e){
      		System.out.println("Error Bean Linea de Credito.getLineaExistente ");
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return bandera;
    }//getLineaExistente


 //SUSTITUYE A GETLINEAEXISTENTE: Verifica si ya existe una linea inicial para una epo y una moneda, y valida que la fecha de vencimiento sea mayor al parametro de dias minimos
 public void validaLinea(String moneda,String ic_if,String ic_epo,String ic_pyme,String fechaVenc,String tipoSolic)
  throws NafinException{
	System.out.println("LineaCreditoBean::validaLinea(E) ");
	String				qrySentencia	= "";
	AccesoDB			con				= null;
	ResultSet			rs				= null;
	PreparedStatement	ps				= null;
	int					diferencia 		= 0;
	try {
		con = new AccesoDB();
		con.conexionDB();
		if("I".equals(tipoSolic)){
			qrySentencia  =
				"SELECT ic_linea_credito"+
				" FROM COM_LINEA_CREDITO"+
				" WHERE cg_tipo_solicitud='I'"+				
				" AND ic_epo = ? "+
				" AND ic_pyme = ? "+
				" AND ic_moneda = ? ";
				if(!ic_if.equals(""))
					qrySentencia += " AND ic_if = ? ";
			ps = con.queryPrecompilado(qrySentencia);		
			ps.setString(1,ic_epo);
			ps.setString(2,ic_pyme);
			ps.setString(3,moneda);
			if(!ic_if.equals("")){
				ps.setString(4,ic_if);
			}
			
				System.out.println(qrySentencia);
			System.out.println(ic_epo);
			System.out.println(ic_pyme);
			System.out.println(moneda);				
			System.out.println(ic_if);
			
			
			rs = ps.executeQuery();
			if (rs.next()){
				throw new NafinException("DIST0028");
			}
			ps.close();
			rs.close();
		}
		qrySentencia = "SELECT TO_DATE('"+fechaVenc+"','dd/mm/yyyy')-(sysdate+fn_minimo_financiamiento)"+
			" FROM comcat_producto_nafin "+
			" WHERE ic_producto_nafin = 4";
		rs = con.queryDB(qrySentencia);
		if(rs.next()){
			diferencia = rs.getInt(1);
		}
		con.cierraStatement();
		if(diferencia <= 0)
			throw new NafinException("DIST0029");
	} catch(NafinException ne){
		throw ne;
	} catch(Exception e){
		System.out.println("LineaCreditoBean::validaLinea Exception "+e);
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())
		con.cierraConexionDB();
	System.out.println("LineaCreditoBean::validaLinea(S)");
	}
 }


  public Vector getValores(String ic_epo, String ic_pyme, String solicitud, String ic_moneda, String ic_tipo_cobro_interes, String ic_financiera)
  	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    String      campos  = "";
    String      tablas  = "";
    String      condiciones  = "";
    Vector		  vecColumnas		= new Vector();
    Vector		  vecFilas			= new Vector();

    try{
      con = new AccesoDB();
      con.conexionDB();
      //CONSULTA

      campos =
		"	   epo.cg_razon_social as nomEpo, crn.ic_nafin_electronico as noDist,"+
		"       py.cg_razon_social as nomDist, mon.cd_nombre as moneda, tci.cd_descripcion as tipCobInt";

		if (!"".equals(ic_financiera))
			campos += " , fin.cd_nombre as banco ";

      tablas =
		"  	    comcat_epo epo, "+
		"       comrel_nafin crn, "+
		"       comcat_pyme py, "+
		"       comcat_moneda mon, "+
		"       comcat_tipo_cobro_interes tci ";
      if (!"".equals(ic_financiera))
      	tablas += " , comcat_financiera fin ";

	  condiciones =
		" 	   epo.ic_epo = "+ic_epo+" "+
		"   AND py.ic_pyme = "+ic_pyme+" "+
		"   AND crn.ic_epo_pyme_if = py.ic_pyme "+
		"   AND crn.cg_tipo = 'P' "+
		"   AND mon.ic_moneda = "+ic_moneda+" "+
		"   AND tci.ic_tipo_cobro_interes = "+ic_tipo_cobro_interes+" ";

      if (!"".equals(ic_financiera))
      	condiciones +=  "   AND fin.ic_financiera = "+ic_financiera+" ";

      qrySentencia = "SELECT "+campos+" FROM "+tablas+"WHERE"+condiciones;

      System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
        vecColumnas = new Vector();
		vecColumnas.addElement((rs.getString("nomEpo")==null)?"":rs.getString("nomEpo"));
		vecColumnas.addElement((rs.getString("noDist")==null)?"":rs.getString("noDist"));
		vecColumnas.addElement((rs.getString("nomDist")==null)?"":rs.getString("nomDist"));
        if ("I".equals(solicitud))
          solicitud="INICIAL";
        else
          if ("A".equals(solicitud))
            solicitud="AMPLIACI�N/REDUCCI�N";
          else
            solicitud="RENOVACI�N";
		vecColumnas.addElement(solicitud);
		vecColumnas.addElement((rs.getString("moneda")==null)?"":rs.getString("moneda"));
		vecColumnas.addElement((rs.getString("tipCobInt")==null)?"":rs.getString("tipCobInt"));
		if ("".equals(ic_financiera))
          vecColumnas.addElement("");
        else
          vecColumnas.addElement(rs.getString("banco"));
      	vecFilas.addElement(vecColumnas);
      }
      con.cierraStatement();
    }catch(Exception e){
      	System.out.println("Error Bean Linea de Credito.getValores "+e.getMessage());
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
   return vecFilas;
  }//getValores


public Vector setLineasCredito(String iNoUsuario, String ic_if,String totDocs,String totMtoAuto,String totDocsDol,String totMtoAutoDol,String ic_epos_aux[],String ic_pymes_aux[],String solics_aux[],String folios[],String monedas_aux[],String montoAutos[],String vencimientos[],String tipo_cobro_ints_aux[],String nCtaDists[],String ifBancos[],String nCtaIfs[])
 throws NafinException {
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    Vector		  vecColumnas		= new Vector();
    Vector		  vecFilas			= new Vector();
    String cc_acuse;
    boolean     ok = true;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        Acuse acuse = new Acuse(4,"4","dis",con);
        cc_acuse=acuse.toString();
        qrySentencia  =
          "INSERT INTO dis_acuse4 (cc_acuse,df_acuse,ic_usuario) "+
          "VALUES('"+cc_acuse+"',sysdate,'"+iNoUsuario+"') ";
        System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
        System.out.println("\nAcuse creado...\n");
        setDisRelAcuse4(cc_acuse,totDocs,totMtoAuto,totDocsDol,totMtoAutoDol,con);
        setDisLineasCreditoCCC(cc_acuse,ic_epos_aux,ic_pymes_aux,solics_aux,folios,monedas_aux,montoAutos,vencimientos,tipo_cobro_ints_aux,nCtaDists,ifBancos,nCtaIfs,ic_if,con);
        qrySentencia =
          "SELECT to_char(sysdate,'dd/mm/yyyy') as fecha, to_char(sysdate,'HH:MI') as hora"+
          " FROM dual";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        if(rs.next()){
          vecColumnas = new Vector();
	  			vecColumnas.addElement(cc_acuse);
	  			vecColumnas.addElement(acuse.formatear());
  				vecColumnas.addElement((rs.getString("fecha")==null)?"":rs.getString("fecha"));
	  			vecColumnas.addElement((rs.getString("hora")==null)?"":rs.getString("hora"));
        	vecFilas.addElement(vecColumnas);
        }
      con.cierraStatement();
      } catch(Exception e){
      	System.out.println("Error Bean Linea de Credito.setLineasCredito "+e.getMessage());
          ok = false;
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
          con.terminaTransaccion(ok);
        	con.cierraConexionDB();
      }
		return vecFilas;
    }

/////////// - - -  Modific Hugo Vargas, sobrecarga de metodo para el nuevo tipo de credito Fac con recurso
/************************************************************************************************/
/************************FODEA 019 Factoraje con recurso*****************************************/
/**Modific Hugo Vargas, sobrecarga de metodo para el nuevo tipo de credito Fac con recurso*******/
/************************************************************************************************/
public Vector setLineasCredito(String iNoUsuario, String nombreUsuario, String ic_if,String totDocs,String totMtoAuto,String totDocsDol,String totMtoAutoDol,String ic_epos_aux[],
										String ic_pymes_aux[],String solics_aux[],String monedas_aux[],String montoAutos[],String vencimientos[],
										String tipo_cobro_ints_aux[],String nPlazoMax[], String nCtaBancaria[])
 throws AppException {
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    Vector		  vecColumnas	= new Vector();
    Vector		  vecFilas		= new Vector();
    String cc_acuse;
    boolean     ok = true;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        Acuse acuse = new Acuse(4,"4","dis",con);
        cc_acuse=acuse.toString();
        qrySentencia  =
          "INSERT INTO dis_acuse4 (cc_acuse,df_acuse,ic_usuario) "+
          "VALUES('"+cc_acuse+"',sysdate,'"+iNoUsuario+"') ";
        System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
        System.out.println("\nAcuse creado...\n");
		  String usuario = iNoUsuario+"-"+nombreUsuario;	//Para guardar el usuario
        setDisRelAcuse4(cc_acuse,totDocs,totMtoAuto,totDocsDol,totMtoAutoDol,con);
		  setDisLineasCreditoCCC(cc_acuse,ic_epos_aux,ic_pymes_aux,solics_aux,monedas_aux,montoAutos,vencimientos,tipo_cobro_ints_aux,ic_if, con,nPlazoMax, nCtaBancaria, usuario);
        qrySentencia =
          "SELECT to_char(sysdate,'dd/mm/yyyy') as fecha, to_char(sysdate,'HH:MI') as hora"+
          " FROM dual";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        if(rs.next()){
          vecColumnas = new Vector();
	  			vecColumnas.addElement(cc_acuse);
	  			vecColumnas.addElement(acuse.formatear());
  				vecColumnas.addElement((rs.getString("fecha")==null)?"":rs.getString("fecha"));
	  			vecColumnas.addElement((rs.getString("hora")==null)?"":rs.getString("hora"));
        	vecFilas.addElement(vecColumnas);
        }
      con.cierraStatement();
      } catch(Exception e){
      	System.out.println("Error Bean Linea de Credito.setLineasCredito "+e.getMessage());
          ok = false;
					throw new AppException("Error Bean Linea de Credito.setLineasCredito "+e);
      } finally {
      	if(con.hayConexionAbierta())
          con.terminaTransaccion(ok);
        	con.cierraConexionDB();
      }
		return vecFilas;
    }
/////////// - - - Termina sobrecarga de metodo

private void setDisRelAcuse4(String cc_acuse, String in_total_lineas, String in_total_monto,String in_total_lineasDol, String in_total_montoDol, AccesoDB con)
 throws NafinException
  {
    String      qrySentencia  = "";
    boolean     ok = true;
    	try {
        qrySentencia  =
          "INSERT INTO disrel_acuse4_moneda (CC_ACUSE,IC_MONEDA,IN_TOTAL_LINEAS,FN_TOTAL_MONTO) "+
          "VALUES('"+cc_acuse+"',1,"+Integer.parseInt(in_total_lineas)+","+Double.parseDouble(in_total_monto)+")";
        System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
          System.out.println("\n...agregar\n");
        qrySentencia  =
          "INSERT INTO disrel_acuse4_moneda (CC_ACUSE,IC_MONEDA,IN_TOTAL_LINEAS,FN_TOTAL_MONTO) "+
          "VALUES('"+cc_acuse+"',54,"+Integer.parseInt(in_total_lineasDol)+","+Double.parseDouble(in_total_montoDol)+")";
        System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
          System.out.println("\n...agregar\n");
      } catch(Exception e){
      	      	System.out.println("Error Bean Linea de Credito.setDisRelAcuse4 "+e.getMessage());
          ok = false;
					throw new NafinException("SIST0001");
      }
  }

 private void setDisLineasCreditoCCC(String cc_acuse, String ic_epos_aux[],String ic_pymes_aux[],String solics_aux[],String folios[],String monedas_aux[],String montoAutos[],String vencimientos[],String tipo_cobro_ints_aux[],String nCtaDists[],String ifBancos[],String nCtaIfs[],String ic_if, AccesoDB con)
 throws NafinException {
    String      qrySentencia  = "";
    Vector		  vecColumnas		= new Vector();
    Vector		  vecFilas			= new Vector();
    boolean     ok = true;
    	try {
        for (int i=0;i<ic_epos_aux.length;i++){
          if ("I".equals(solics_aux[i])){
            qrySentencia  =
              "INSERT INTO com_linea_credito (IC_LINEA_CREDITO,CC_ACUSE,CG_TIPO_SOLICITUD,IC_MONEDA,FN_MONTO_AUTORIZADO,FN_MONTO_AUTORIZADO_TOTAL,DF_VENCIMIENTO,DF_VENCIMIENTO_ADICIONAL,FN_SALDO_LINEA,fn_saldo_linea_total,IC_TIPO_COBRO_INTERES,IC_PRODUCTO_NAFIN,IC_IF,IC_EPO,IC_PYME,cg_numero_cuenta,cg_numero_cuenta_if,IC_FINANCIERA,IC_ESTATUS_LINEA,CS_ACEPTACION_PYME,IC_LINEA_CREDITO_PADRE,DF_AUTORIZACION_IF) "+
              "select nvl(max(ic_linea_credito),0)+1,'"+
              cc_acuse+"','"+solics_aux[i]+"',"+
              monedas_aux[i]+","+
              montoAutos[i]+","+montoAutos[i]+","+
              "to_date('"+vencimientos[i]+"','dd/mm/yyyy'),to_date('"+
              vencimientos[i]+"','dd/mm/yyyy'),"+montoAutos[i]+","+montoAutos[i]+","+
              tipo_cobro_ints_aux[i]+",4,"+ic_if+","+
              ic_epos_aux[i]+","+ic_pymes_aux[i]+",'"+nCtaDists[i]+"','"+
              nCtaDists[i]+"',";
              if ("".equals(ifBancos[i]))
                qrySentencia = qrySentencia+"null";
              else
                qrySentencia = qrySentencia+""+ifBancos[i]+"";
              qrySentencia = qrySentencia+",12,'S','',sysdate "+
              " from com_linea_credito";
			  con.ejecutaSQL(qrySentencia);
          }else if ("A".equals(solics_aux[i])){
              vecFilas=getSolicRelac(folios[i]);
              vecColumnas = (Vector)vecFilas.get(0);
              //String montAuto = (String)vecColumnas.get(0);
              //String saldtot = (String)vecColumnas.get(1);
              String fechaVenc = (String)vecColumnas.get(2);

              Calendar f1 = Calendar.getInstance();
              Calendar f2 = Calendar.getInstance();
              f1.set(Integer.parseInt(fechaVenc.substring(6,10)),Integer.parseInt(fechaVenc.substring(3,5))-1,Integer.parseInt(fechaVenc.substring(0,2)));
              java.util.Date dFechaVenc1 = f1.getTime();
              f2.set(Integer.parseInt(vencimientos[i].substring(6,10)),Integer.parseInt(vencimientos[i].substring(3,5))-1,Integer.parseInt(vencimientos[i].substring(0,2)));
              java.util.Date dFechaVenc2 = f2.getTime();
              int res = dFechaVenc1.compareTo(dFechaVenc2);
//                System.out.println("\n******"+dFechaVenc1+" "+dFechaVenc2+" "+res);
              if (res==-1)
                fechaVenc = vencimientos[i];
				//Historico
				qrySentencia  =
					" INSERT INTO com_linea_credito (IC_LINEA_CREDITO,CC_ACUSE,CG_TIPO_SOLICITUD,IC_MONEDA,FN_MONTO_AUTORIZADO,FN_MONTO_AUTORIZADO_TOTAL,IC_PLAZO,DF_VENCIMIENTO,DF_VENCIMIENTO_ADICIONAL,FN_SALDO_LINEA_TOTAL,IC_TIPO_COBRO_INTERES,IC_PRODUCTO_NAFIN,IC_IF,IC_EPO,CG_NUMERO_CUENTA,CG_NUMERO_CUENTA_IF,IC_FINANCIERA,IC_LINEA_CREDITO_PADRE,IC_ESTATUS_LINEA) "+
					" select nvl(max(lc1.ic_linea_credito),0)+1 "+
					" ,'"+cc_acuse+"','A',lc2.IC_MONEDA"+
					" ,lc2.FN_MONTO_AUTORIZADO,lc2.FN_MONTO_AUTORIZADO_TOTAL,lc2.IC_PLAZO,lc2.DF_VENCIMIENTO,lc2.DF_VENCIMIENTO_ADICIONAL,lc2.FN_SALDO_LINEA_TOTAL,lc2.IC_TIPO_COBRO_INTERES,lc2.IC_PRODUCTO_NAFIN"+
					" ,lc2.IC_IF,lc2.IC_EPO,lc2.CG_NUMERO_CUENTA,lc2.CG_NUMERO_CUENTA_IF,lc2.IC_FINANCIERA,lc2.IC_LINEA_CREDITO,lc2.IC_ESTATUS_LINEA"+
					" from com_linea_credito lc1,com_linea_credito lc2"+
					" where lc2.ic_linea_credito = "+folios[i]+
					" group by lc2.IC_MONEDA,lc2.FN_MONTO_AUTORIZADO,lc2.FN_MONTO_AUTORIZADO_TOTAL,lc2.IC_PLAZO,lc2.DF_VENCIMIENTO,lc2.DF_VENCIMIENTO_ADICIONAL,lc2.FN_SALDO_LINEA_TOTAL,lc2.IC_TIPO_COBRO_INTERES,lc2.IC_PRODUCTO_NAFIN"+
					" ,lc2.IC_IF,lc2.IC_EPO,lc2.CG_NUMERO_CUENTA,lc2.CG_NUMERO_CUENTA_IF,lc2.IC_FINANCIERA,lc2.IC_LINEA_CREDITO,lc2.IC_ESTATUS_LINEA";
				con.ejecutaSQL(qrySentencia);
				qrySentencia =
					" UPDATE com_linea_credito  SET "+
					" FN_MONTO_AUTORIZADO = "+montoAutos[i]+
					" ,FN_MONTO_AUTORIZADO_TOTAL = "+montoAutos[i]+
					" ,FN_SALDO_LINEA_TOTAL = "+montoAutos[i]+"-(FN_MONTO_AUTORIZADO_TOTAL-FN_SALDO_LINEA_TOTAL)"+
					" ,DF_VENCIMIENTO_ADICIONAL = TO_DATE('"+vencimientos[i]+"','dd/mm/yyyy') "+
					" WHERE ic_linea_credito = "+folios[i];
				con.ejecutaSQL(qrySentencia);
            }	else if ("R".equals(solics_aux[i])){

	              vecFilas=getSolicRelac(folios[i]);
	              vecColumnas = (Vector)vecFilas.get(0);
	              String fechaVenc = (String)vecColumnas.get(2);
	              String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

	              Calendar f1 = Calendar.getInstance();
	              Calendar f2 = Calendar.getInstance();
	              f1.set(Integer.parseInt(fechaVenc.substring(6,10)),Integer.parseInt(fechaVenc.substring(3,5))-1,Integer.parseInt(fechaVenc.substring(0,2)));
	              java.util.Date dFechaVenc1 = f1.getTime();
	              f2.set(Integer.parseInt(fechaHoy.substring(6,10)),Integer.parseInt(fechaHoy.substring(3,5))-1,Integer.parseInt(fechaHoy.substring(0,2)));
	              java.util.Date dFechaVenc2 = f2.getTime();
	              int res = dFechaVenc1.compareTo(dFechaVenc2);
	//                System.out.println("\n******"+dFechaVenc1+" "+dFechaVenc2+" "+res);
	              if (res>0)
	                fechaVenc = fechaHoy;
				qrySentencia  =
					" INSERT INTO com_linea_credito (IC_LINEA_CREDITO,CC_ACUSE,CG_TIPO_SOLICITUD,IC_MONEDA,FN_MONTO_AUTORIZADO,FN_MONTO_AUTORIZADO_TOTAL,IC_PLAZO,DF_VENCIMIENTO,DF_VENCIMIENTO_ADICIONAL,FN_SALDO_LINEA_TOTAL,IC_TIPO_COBRO_INTERES,IC_PRODUCTO_NAFIN,IC_IF,IC_EPO,CG_NUMERO_CUENTA,CG_NUMERO_CUENTA_IF,IC_FINANCIERA,IC_LINEA_CREDITO_PADRE,IC_ESTATUS_LINEA) "+
					" select nvl(max(lc1.ic_linea_credito),0)+1 "+
					" ,'"+cc_acuse+"','R',lc2.IC_MONEDA"+
					" ,lc2.FN_MONTO_AUTORIZADO,lc2.FN_MONTO_AUTORIZADO_TOTAL,lc2.IC_PLAZO,lc2.DF_VENCIMIENTO,lc2.DF_VENCIMIENTO_ADICIONAL,lc2.FN_SALDO_LINEA_TOTAL,lc2.IC_TIPO_COBRO_INTERES,lc2.IC_PRODUCTO_NAFIN"+
					" ,lc2.IC_IF,lc2.IC_EPO,lc2.CG_NUMERO_CUENTA,lc2.CG_NUMERO_CUENTA_IF,lc2.IC_FINANCIERA,lc2.IC_LINEA_CREDITO,lc2.IC_ESTATUS_LINEA"+
					" from com_linea_credito lc1,com_linea_credito lc2"+
					" where lc2.ic_linea_credito = "+folios[i]+
					" group by lc2.IC_MONEDA,lc2.FN_MONTO_AUTORIZADO,lc2.FN_MONTO_AUTORIZADO_TOTAL,lc2.IC_PLAZO,lc2.DF_VENCIMIENTO,lc2.DF_VENCIMIENTO_ADICIONAL,lc2.FN_SALDO_LINEA_TOTAL,lc2.IC_TIPO_COBRO_INTERES,lc2.IC_PRODUCTO_NAFIN"+
					" ,lc2.IC_IF,lc2.IC_EPO,lc2.CG_NUMERO_CUENTA,lc2.CG_NUMERO_CUENTA_IF,lc2.IC_FINANCIERA,lc2.IC_LINEA_CREDITO,lc2.IC_ESTATUS_LINEA";
				con.ejecutaSQL(qrySentencia);
				qrySentencia =
					" UPDATE com_linea_credito  SET "+
					" FN_MONTO_AUTORIZADO = "+montoAutos[i]+
					" ,FN_MONTO_AUTORIZADO_TOTAL = "+montoAutos[i]+
					" ,FN_SALDO_LINEA_TOTAL = "+montoAutos[i]+"-(FN_MONTO_AUTORIZADO_TOTAL-FN_SALDO_LINEA_TOTAL)"+
					" ,DF_VENCIMIENTO_ADICIONAL = TO_DATE('"+vencimientos[i]+"','dd/mm/yyyy') "+
					" WHERE ic_linea_credito = "+folios[i];
				con.ejecutaSQL(qrySentencia);
          }//else Inicial
		  System.out.println("\n...agregar\n"  +qrySentencia);
        }//for
      } catch(Exception e){
      	      	System.out.println("Error Bean Linea de Credito.setDisLineasCreditoCCC "+e.getMessage());
          ok = false;
					throw new NafinException("SIST0001");
      }
 }

	/*****************************************************************************************/
	/************************FODEA 019 Factoraje con recurso**********************************/
	/****Sobrecarga de metodo para insertar el nuevo tipo de credito: FACTOAJE CON REECURSO***/
	/*****************************************************************************************/
	private void setDisLineasCreditoCCC(String cc_acuse, String ic_epos_aux[],String ic_pymes_aux[],String solics_aux[],String monedas_aux[],String montoAutos[],String vencimientos[],String tipo_cobro_ints_aux[],String ic_if, AccesoDB con, String nPlazoMax[], String nCtaBancaria[], String usuario)
		throws AppException {
	 String      qrySentencia  = "";
	 boolean     ok = true;
		try {
		  for (int i=0;i<ic_epos_aux.length;i++){
			 if ("I".equals(solics_aux[i])){
				qrySentencia  =
				  "INSERT INTO com_linea_credito (IC_LINEA_CREDITO,CC_ACUSE,CG_TIPO_SOLICITUD,IC_MONEDA,FN_MONTO_AUTORIZADO,FN_MONTO_AUTORIZADO_TOTAL,DF_VENCIMIENTO,"+
				  "DF_VENCIMIENTO_ADICIONAL,FN_SALDO_LINEA,fn_saldo_linea_total,IC_TIPO_COBRO_INTERES,IC_PRODUCTO_NAFIN,IC_IF,IC_EPO,IC_PYME,"+
				  "IC_ESTATUS_LINEA,CS_ACEPTACION_PYME,IC_LINEA_CREDITO_PADRE,DF_AUTORIZACION_IF,IG_PLAZO_MAX,IG_CUENTA_BANCARIA,CG_USUARIO_CAMBIO,CS_FACTORAJE_CON_REC) "+
				  "SELECT nvl(max(ic_linea_credito),0)+1,'"+
				  cc_acuse+"','"+solics_aux[i]+"',"+
				  monedas_aux[i]+","+
				  montoAutos[i]+","+montoAutos[i]+","+
				  "to_date('"+vencimientos[i]+"','dd/mm/yyyy'),to_date('"+
				  vencimientos[i]+"','dd/mm/yyyy'),"+montoAutos[i]+","+montoAutos[i]+","+
				  tipo_cobro_ints_aux[i]+",4,"+ic_if+","+
				  ic_epos_aux[i]+","+ic_pymes_aux[i]+
				  ",12,'S','',sysdate,"+
				  nPlazoMax[i]+","+nCtaBancaria[i]+",'"+usuario+"','S' FROM com_linea_credito";
			  con.ejecutaSQL(qrySentencia);
			 }
		System.out.println("\n...agregar\n");
		  }//for
		} catch(Exception e){
						System.out.println("Error Bean Linea de Credito.setDisLineasCreditoCCC "+e.getMessage());
			 ok = false;
					throw new AppException("Error Bean Linea de Credito.setDisLineasCreditoCCC "+e);
		}
	}

 public Vector getSolicRelac(String ic_linea_credito)
 throws NafinException
 {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
          "select IC_LINEA_CREDITO,"+
          " FN_MONTO_AUTORIZADO_TOTAL,"+
          " FN_SALDO_LINEA_TOTAL,"+
          " to_char(DF_VENCIMIENTO_ADICIONAL,'dd/mm/yyyy') AS VENCIMIENTO"+
          " from com_linea_credito"+
          " where IC_LINEA_CREDITO="+ic_linea_credito+" ";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
	        vecColumnas.addElement((rs.getString("FN_SALDO_LINEA_TOTAL")==null)?"":rs.getString("FN_SALDO_LINEA_TOTAL"));
	        vecColumnas.addElement((rs.getString("VENCIMIENTO")==null)?"":rs.getString("VENCIMIENTO"));
	        vecFilas.addElement(vecColumnas);
	      }
      con.cierraStatement();
      } catch(Exception e){
      	      	System.out.println("Error Bean Linea de Credito.getSolicRelac "+e.getMessage());
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
    }

/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ISRAEL HERRERA */

// 						-- CARP --
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Agregado 28/02/2003	--CARP
	public Vector getLineasProceso(String esCvePyme,	String esCveEPO,
    								String esTipoSolicitud,String esProducto,
							   		String esCveMoneda)
	throws NafinException
	{
	    StringBuffer lsQuery = new StringBuffer();
    	AccesoDB lobdCon 	 = null;
	    ResultSet lrsResult  = null;
    	Vector	vecFilas	 = new Vector();
	    Vector	vecColumnas	 = new Vector();
    	try {
	      	lobdCon = new AccesoDB();
    	    lobdCon.conexionDB();
        	lsQuery.append("  select distinct i.cg_razon_social,");
        	lsQuery.append("  				 decode(lc.ic_estatus_linea,5,decode(pep.cs_habilitado,'H','Afiliado','En proceso'),'En proceso')");
        	lsQuery.append("  from comcat_if i,");
        	lsQuery.append("   	  com_linea_credito lc,");
        	lsQuery.append(" 	  comrel_pyme_epo_x_producto pep");
        	lsQuery.append("  where lc.ic_estatus_linea in(1,2,3,5,7,9)");
        	lsQuery.append("  and lc.ic_pyme= "+ esCvePyme);
        	lsQuery.append("  and lc.ic_if = i.ic_if");
        	lsQuery.append("  and i.cs_habilitado = 'S'");
        	lsQuery.append("  and lc.ic_producto_nafin = "+ esProducto);
        	lsQuery.append("  and lc.ic_epo = "+ esCveEPO);
        	lsQuery.append("  and pep.ic_epo = lc.ic_epo");
        	lsQuery.append("  and pep.ic_pyme = lc.ic_pyme");
        	lsQuery.append("  and lc.cg_tipo_solicitud = '"+ esTipoSolicitud +"'");
        	lsQuery.append("  and pep.ic_producto_nafin = lc.ic_producto_nafin");
        	lsQuery.append("  AND lc.ic_moneda = " + esCveMoneda);
        	lsQuery.append("  and lc.ic_if in(");
        	lsQuery.append(" 		select distinct B.ic_if");
        	lsQuery.append(" 		from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C,comcat_moneda D, comrel_pyme_if E");
        	lsQuery.append(" 		where A.ic_epo = E.ic_epo and B.ic_if = E.ic_if and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria ");
        	lsQuery.append(" 		and	C.ic_moneda = D.ic_moneda ");
        	lsQuery.append(" 		and E.cs_borrado = 'N'	and E.cs_vobo_if = 'S'");
        	lsQuery.append("  		and E.ic_epo = "+ esCveEPO);
        	lsQuery.append("  		and C.ic_pyme = "+ esCvePyme);
        	lsQuery.append(" 		AND c.ic_moneda = "+ esCveMoneda);
        	lsQuery.append(" )"  );
		  	lrsResult = lobdCon.queryDB(lsQuery.toString());
			 while(lrsResult.next()) {
  	    		vecColumnas = new Vector();
	        	vecColumnas.addElement((lrsResult.getString(1)==null)?"":lrsResult.getString(1));
		        vecColumnas.addElement((lrsResult.getString(2)==null)?"":lrsResult.getString(2));
		        vecFilas.addElement(vecColumnas);
			}
            lrsResult.close();
            lobdCon.cierraStatement();
        } catch(Exception e){
        	throw new NafinException("SIST0001");
		} finally {
        	lsQuery = null;
			if(lobdCon.hayConexionAbierta())
            	lobdCon.cierraConexionDB();
		}
		return vecFilas;
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Agregado 28/02/2003	--CARP
	public Vector getComboLineas(String esCvePyme, String esCveEPO,
    							 String esTipoSolicitud, int eiProducto,
                                 String esCveMoneda)
			throws NafinException
	{
    	StringBuffer qrySentencia 	= new StringBuffer();
	    AccesoDB	lobdCon         = null;
    	ResultSet	lrsResult	 	= null;
	    Vector 		vecFilas 		= new Vector();
    	Vector		vecColumnas		= new Vector();
    	try {
	      	lobdCon = new AccesoDB();
    	    lobdCon.conexionDB();
	        if("I".equals(esTipoSolicitud)) {
				qrySentencia.append(
					" select a.ic_if, a.ic_if||' '||a.cg_razon_social,c.ic_epo,'' "+
					" from comcat_if a,"+
					"  (select ic_if"+
					"  	from comcat_if"+
					"  	minus"+
					"   select ic_if from com_linea_credito");
					if(eiProducto!=5)
						qrySentencia.append(" 	where ic_estatus_linea in(1,2,3,5,7,9) ");
					else
						qrySentencia.append(" 	where ic_estatus_linea in(1,2,3,7,9) ");
				qrySentencia.append(
					"	and ic_producto_nafin = " + eiProducto+
					"	AND ic_moneda = "+ esCveMoneda+
					"	and cg_tipo_solicitud = 'I' "+
					" 	and ic_pyme= "+ esCvePyme +" )  b,"+
					" comrel_if_epo_x_producto c "+
					" where a.ic_if = b.ic_if"+
					" and a.cs_habilitado = 'S'"+
					" and c.ic_producto_nafin = "+ eiProducto+
					" and c.ic_if = a.ic_if"+
					" and c.cs_habilitado = 'S'"+
					" and c.ic_epo = "+ esCveEPO
				);
				if (eiProducto != 4) {
					qrySentencia.append(
						" and a.ic_if in("+
						" 		select distinct B.ic_if"+
						" 		from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C,comcat_moneda D, comrel_pyme_if E "+
						" 		where A.ic_epo = E.ic_epo and B.ic_if = E.ic_if and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria and "+
						" 		C.ic_moneda = D.ic_moneda and E.cs_borrado = 'N'"+
						" 		and E.cs_vobo_if = 'S' "+
						"  		and E.ic_epo = "+ esCveEPO +
						"  		and C.ic_pyme = "+ esCvePyme+
						"  		and C.ic_moneda = "+ esCveMoneda+
						" )"
					);
		        }
			} else if("A".equals(esTipoSolicitud)) {
                qrySentencia.append(" select a.ic_linea_credito, a.ic_linea_credito||''||b.cg_razon_social,b.ic_if" );
                qrySentencia.append(" from com_linea_credito a,comcat_if b");
                qrySentencia.append(" where a.ic_if = b.ic_if");
                qrySentencia.append(" and a.ic_pyme = "+ esCvePyme );				//para la pyne
                qrySentencia.append(" and a.ic_epo = "+ esCveEPO);
                qrySentencia.append(" and A.IC_MONEDA = "+ esCveMoneda);
				qrySentencia.append(" AND a.ic_producto_nafin = "+ eiProducto);
                qrySentencia.append(" and a.cg_tipo_solicitud in('I','R')");				//debe ser inicial
                qrySentencia.append(" and df_vencimiento_adicional > sysdate");		//vigente
                qrySentencia.append(" and a.cs_aceptacion_pyme = 'S'");				//autorizada pyme
                qrySentencia.append(" and b.cs_habilitado = 'S'");
                qrySentencia.append(" and  ic_estatus_linea = 5 ");					//autorizada nafin
			} else if("R".equals(esTipoSolicitud)) {
                qrySentencia.append(" select a.ic_linea_credito, b.ic_if||''||b.cg_razon_social,b.ic_if");
                qrySentencia.append(" from com_linea_credito a,comcat_if b");
                qrySentencia.append(" where a.ic_if = b.ic_if");
                qrySentencia.append(" and a.ic_pyme = "+ esCvePyme);				//para la pyne
                qrySentencia.append(" and a.ic_epo = "+ esCveEPO);				//&iquest;para cualquier EPO?
                qrySentencia.append(" AND a.IC_MONEDA = "+ esCveMoneda);
   				qrySentencia.append(" AND a.ic_producto_nafin = "+ eiProducto);
                qrySentencia.append(" and a.cg_tipo_solicitud in('I','R')");		//debe ser inicial
                qrySentencia.append(" and df_vencimiento_adicional < sysdate");		//vencida
                qrySentencia.append(" and a.cs_aceptacion_pyme = 'S'"); 			//autorizada pyme
                qrySentencia.append(" and  ic_estatus_linea = 5 ");					//autorizada nafin
                qrySentencia.append(" and b.cs_habilitado = 'S' ");
                qrySentencia.append(" and a.ic_linea_credito not in ");
                qrySentencia.append(" (");
                qrySentencia.append(" 	SELECT IC_LINEA_CREDITO ");
                qrySentencia.append(" 	FROM COM_LINEA_CREDITO");
                qrySentencia.append(" 	WHERE ");
                qrySentencia.append(" 		(CG_TIPO_SOLICITUD = 'R' AND DF_VENCIMIENTO_ADICIONAL > sysdate)");
                qrySentencia.append(" 		or");
                qrySentencia.append(" 		(cg_tipo_solicitud = 'R' and ic_estatus_linea = 1)");
                qrySentencia.append(" 		or ic_linea_credito in");
                qrySentencia.append(" 			(select ic_linea_credito_padre from com_linea_credito");
                qrySentencia.append(" 			where cg_tipo_solicitud = 'R')");
                qrySentencia.append(" 		and IC_MONEDA = "+ esCveMoneda);
				qrySentencia.append(" 		AND ic_producto_nafin = "+ eiProducto);
                qrySentencia.append("  )");
			}
//    System.out.println("Query ="+ qrySentencia.toString());
			lrsResult = lobdCon.queryDB(qrySentencia.toString());
            while (lrsResult.next()) {
	  	    	vecColumnas = new Vector();
		        vecColumnas.addElement((lrsResult.getString(1)==null)?"":lrsResult.getString(1));
	    	    vecColumnas.addElement((lrsResult.getString(2)==null)?"":lrsResult.getString(2));
	        	vecColumnas.addElement((lrsResult.getString(3)==null)?"":lrsResult.getString(3));
		        vecFilas.addElement(vecColumnas);
		    }
          	lrsResult.close();
            lobdCon.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
        	if(lobdCon.hayConexionAbierta())
            	lobdCon.cierraConexionDB();
		}
		return vecFilas;
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Agregado 28/02/2003	--CARP
	public void generaSolicitud(String esLineaCredito, String esCveEPO, String esCvePyme,
    							String esCveIF, String esTipoSolicitud, String ic_linea_padre,
                                String esProducto, String esCveMoneda)
			 throws NafinException
    {
    	AccesoDB  lobdCon       = null;
	    StringBuffer qrySentencia  = new StringBuffer();
    	boolean   resultado     = true;
	    boolean   continuar     = false;
    	ResultSet rs            = null;
        try {
			lobdCon = new AccesoDB();
	        lobdCon.conexionDB();
  			qrySentencia.append(" select ic_linea_credito from com_linea_credito");
  			qrySentencia.append(" where ic_linea_credito ="+ esLineaCredito);
  			qrySentencia.append(" and ic_estatus_linea is null");
			rs = lobdCon.queryDB(qrySentencia.toString());
			if (rs.next())
				continuar = true;
			lobdCon.cierraStatement();
			if ("I".equals(esTipoSolicitud) && continuar) {
            	qrySentencia.delete(0,qrySentencia.length());
	  			qrySentencia.append(" update com_linea_credito ");
	  			qrySentencia.append(" set ic_pyme = "+ esCvePyme );
	  			qrySentencia.append(" , ic_epo = "+ esCveEPO);
	  			qrySentencia.append(" , ic_producto_nafin = "+ esProducto );
	  			qrySentencia.append(" , ic_if = "+ esCveIF);
				qrySentencia.append(" , ic_moneda = "+ esCveMoneda);
	  			qrySentencia.append(" , cg_tipo_solicitud = 'I'");
	  			qrySentencia.append(" , ic_estatus_linea = 1");
	  			qrySentencia.append(" ,df_captura = sysdate");
	  			qrySentencia.append(" where ic_linea_credito = "+ esLineaCredito);
			} else if(continuar) {
            	qrySentencia.delete(0,qrySentencia.length());
	  			qrySentencia.append(" UPDATE COM_LINEA_CREDITO ");
	  			qrySentencia.append(" SET(IC_PYME,IC_IF,CG_TIPO_SOLICITUD");
	  			qrySentencia.append(" ,IC_ESTATUS_LINEA,IC_LINEA_CREDITO_PADRE");
	  			qrySentencia.append(" ,IC_FINANCIERA,CG_NUMERO_CUENTA,IC_EPO,IC_PRODUCTO_NAFIN,DF_CAPTURA)");
	  			qrySentencia.append(" = (SELECT '"+esCvePyme+"',ic_if,'"+ esTipoSolicitud +"'");
	  			qrySentencia.append(" ,1,IC_LINEA_CREDITO,IC_FINANCIERA,CG_NUMERO_CUENTA,IC_EPO,IC_PRODUCTO_NAFIN,SYSDATE");
	  			qrySentencia.append(" FROM COM_LINEA_CREDITO WHERE IC_LINEA_CREDITO ="+ic_linea_padre+")");
	  			qrySentencia.append(" WHERE IC_LINEA_CREDITO = "+ esLineaCredito);
			} else
				throw new NafinException("ANTI0019");
			lobdCon.ejecutaSQL(qrySentencia.toString());
		} catch(NafinException ne){
        	resultado = false;
          	throw ne;
		} catch(Exception e) {
        	resultado = false;
            throw new NafinException("SIST0001");
		} finally {
        	lobdCon.terminaTransaccion(resultado);
			if(lobdCon.hayConexionAbierta())
            	lobdCon.cierraConexionDB();
		}
	}

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// Agregado 10/03/2003	--CARP
    public Vector getEpoRelacionadaPyme(String esPyme,String esCveIf,
    									String esProducto, String esCveLinea)
    	throws NafinException
	{
		String		qrySentencia	= "";
      	AccesoDB	lodbCon			= null;
		ResultSet	rs				= null;
        ResultSet   lrsDato 		= null;
      	Vector 		vecFilas		= new Vector();
      	Vector		vecColumnas 	= null;
        String 		lsCveEpo 	   	= "";
        String 		lsCveLinea 		= "";
        String 		lsOperable 		= "";
        boolean lbComparteLinea = false;
        boolean lbLineaVigente  = true;
		try {
			lodbCon = new AccesoDB();
            lodbCon.conexionDB();

            // Verifica si el if permite compartir linea
            lbComparteLinea = comparteLineaIF(esCveIf, esProducto, lodbCon );
            lbLineaVigente = lineaVigente(esCveLinea,lodbCon);

       		qrySentencia =  " Select distinct CPE.ic_epo, CE.cg_razon_social "+
								" From comrel_pyme_epo CPE, comrel_pyme_epo_x_producto PEXP, "+
								" comcat_epo CE, comrel_if_epo_x_producto IEXP "+
								" Where CPE.ic_pyme= "+ esPyme +
								" And IEXP.ic_if ="+ esCveIf +
								" And PEXP.ic_producto_nafin = "+ esProducto +
								" And PEXP.cs_habilitado in('S','H') "+
								" And CPE.ic_pyme = PEXP.ic_pyme "+
								" And CPE.ic_epo = PEXP.ic_epo "+
								" And CPE.ic_epo = CE.ic_epo "+
								" And PEXP.ic_epo = IEXP.ic_epo "+
								" And IEXP.ic_producto_nafin =  "+ esProducto +
								" And IEXP.cs_habilitado in('S') "+
								" And IEXP.ic_producto_nafin = PEXP.ic_producto_nafin ";
			rs = lodbCon.queryDB(qrySentencia);

			while (rs.next()) {

            	lsCveEpo = (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO");

				if ( lbComparteLinea ){
                	if ( lbLineaVigente ) {
		                qrySentencia =  " SELECT distinct ic_linea_credito"+
									" FROM inv_disposicion"+
									" WHERE ic_pyme = "+ esPyme +
									" AND ic_epo = "+ lsCveEpo +
                	                " AND ic_producto_nafin = "+ esProducto +
									" AND ic_estatus_disposicion in (5,7)";
		                lrsDato = lodbCon.queryDB(qrySentencia);

        		        lsOperable = "OPERABLE";
            		    while ( lrsDato.next()){
                			lsCveLinea = lrsDato.getString(1);

                    		if (!lsCveLinea.equals(esCveLinea))
                    			lsOperable = "NO OPERABLE";
		                }
                    } else
						lsOperable = "NO OPERABLE";
                } else if (esProducto.equals("5"))
					lsOperable = "NO OPERABLE";
				vecColumnas = new Vector();
				vecColumnas.addElement(lsCveEpo);
				vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
                vecColumnas.addElement(lsOperable);
				vecFilas.addElement(vecColumnas);
			}
            rs.close();
            lodbCon.cierraStatement();
        } catch(Exception e) {
        	throw new NafinException("SIST0001");
        } finally {
        	if(lodbCon.hayConexionAbierta())
				lodbCon.cierraConexionDB();
        }
		return vecFilas;
	} // getEpoRelacionadaPyme


/*********************************************************************************************
*	    boolean comparteLineaIF()
* *********************************************************************************************/
// Agregado 10/03/2003		--CARP
    private boolean comparteLineaIF(String esCveIF, String esProducto, AccesoDB eobdCon)
		throws NafinException
    {
    	StringBuffer lsQuery = new StringBuffer();
        ResultSet lrsResultado = null;
		boolean lbComparteLinea = false;

        System.out.println("LineaCreditoBean::comparteLineaIF(E) ");

        try {
	   		lsQuery.append(" SELECT nvl(cs_compartir_linea,'N')");
	   		lsQuery.append(" FROM comrel_producto_if");
	   		lsQuery.append(" WHERE ic_if = " + esCveIF);
            lsQuery.append(" AND ic_producto_nafin = " + esProducto);

			lrsResultado = eobdCon.queryDB(lsQuery.toString());

			if (lrsResultado.next()) {
				lbComparteLinea = lrsResultado.getString(1).equals("S")? true: false;
		   }
		   lrsResultado.close();
	   	   eobdCon.cierraStatement();

		} catch (Exception e) {
			System.out.println("Error en comparteLineaIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
        	lsQuery = null;
		}
      return lbComparteLinea;
    }
/*********************************************************************************************
*	    boolean lineaVigente()
* *********************************************************************************************/
// Agregado 10/03/2003		--CARP
    private boolean lineaVigente(String esCveLinea, AccesoDB eobdCon)
		throws NafinException
    {
    	StringBuffer lsQuery = new StringBuffer();
        ResultSet lrsResultado = null;
		boolean lbVigente = true;

        System.out.println("LineaCreditoBean::lineaVigente(E) ");

        try {
	   		lsQuery.append("SELECT CASE WHEN trunc(df_vencimiento) < trunc(sysdate) THEN 'VENCIDA'");
	   		lsQuery.append(" 	   ELSE 'OPERABLE' END,");
	   		lsQuery.append(" 	   CASE WHEN trunc(df_limite_disposicion) < trunc(sysdate) THEN 'NO DISPONE'");
	   		lsQuery.append(" 	   ELSE 'OPERABLE' END");
            lsQuery.append(" FROM com_linea_credito");
            lsQuery.append(" WHERE ic_linea_credito = "+ esCveLinea);
            lsQuery.append(" ");
            lsQuery.append(" ");

			lrsResultado = eobdCon.queryDB(lsQuery.toString());

			if (lrsResultado.next()) {
				lbVigente = lrsResultado.getString(1).equals("OPERABLE")? true: false;
                if (lbVigente)
	                lbVigente = lrsResultado.getString(2).equals("OPERABLE")? true: false;
		   }
		   lrsResultado.close();
	   	   eobdCon.cierraStatement();

		} catch (Exception e) {
			System.out.println("Error en comparteLineaIF: "+e);
			throw new NafinException("SIST0001");
		} finally {
        	lsQuery = null;
		}
      return lbVigente;
    }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*/////////////////////////////////////////////////////////////////////////////////////////*/
/*/////////////////////////////////////////////////////////////////////////////////////////*/
/* ----------------- M�todos del EJB de Dispersion  ------------------------------- */
	//-------------------------------------------------------------------------------------------
	//	void ovinsertarProducto() -- sobrecargado AEC -- 04/04/03 --
	//
	//-------------------------------------------------------------------------------------------
	private String ovinsertarCuenta(  	String ic_nafin_electronico,
										String ic_producto_nafin,	String ic_moneda,
										String ic_bancos_tef,		String cg_cuenta,
										String ic_usuario,			String ic_tipo_cuenta,
										AccesoDB		con)
   	throws NafinException {

		String			qrySentencia	= "";
		String 			sSigLlave		= sLlaveSiguiente(con);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		boolean resultado = true;

		try {

			//String sFechaActual = sdf.format(new java.util.Date());

			Calendar cFechaSigHabil = new GregorianCalendar();
			cFechaSigHabil.setTime(new java.util.Date());
			cFechaSigHabil.add(Calendar.DATE, 1);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
				cFechaSigHabil.add(Calendar.DATE, 2);
			if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
				cFechaSigHabil.add(Calendar.DATE, 1);
			Vector vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
			boolean bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
			cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			while(bInhabil) {
				vFecha = bEsDiaInhabil(sdf.format(cFechaSigHabil.getTime()), cFechaSigHabil, con);
				bInhabil = new Boolean(vFecha.get(0).toString()).booleanValue();
				if(!bInhabil)
					break;
				cFechaSigHabil = new GregorianCalendar(Integer.parseInt(vFecha.get(1).toString()), Integer.parseInt(vFecha.get(2).toString()), Integer.parseInt(vFecha.get(3).toString()));
			}
			String sFechaSigHabil = sdf.format(cFechaSigHabil.getTime());
			System.out.println("Fecha siguiente: "+sFechaSigHabil);

			qrySentencia = " INSERT INTO com_cuentas  ";
			qrySentencia += " ( ";
			qrySentencia += " ic_cuenta, ic_nafin_electronico, ic_producto_nafin, ic_moneda, ";
			qrySentencia += " ic_bancos_tef, cg_cuenta, ic_usuario, df_ultima_mod,  ";
			qrySentencia += " df_registro, df_transferencia, df_aplicacion, ic_tipo_cuenta ";
			qrySentencia += " ) ";
			qrySentencia += " VALUES ";
			qrySentencia += " ( ";
			qrySentencia += " " + sSigLlave + ", " + ic_nafin_electronico + " , " + ic_producto_nafin + ", " + ic_moneda + ", ";
			qrySentencia += " " + ic_bancos_tef + ", '" + cg_cuenta + "', '" + ic_usuario + "', SYSDATE, ";
			qrySentencia += " SYSDATE, to_date('" + sFechaSigHabil + "','dd/mm/yyyy'), to_date('" + sFechaSigHabil +"','dd/mm/yyyy'), " + ic_tipo_cuenta + "  ";
			qrySentencia += " ) ";

			System.out.print("DispersionBean :: ovinsertarCuenta :: Query "+qrySentencia);

			try{
				con.ejecutaSQL(qrySentencia);
			} catch(SQLException e){
				resultado = false;
				throw new NafinException("DISP0001");
			}


			return sSigLlave;

		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}
	}/*Fin del M�todo ovinsertarProducto*/


	//-------------------------------------------------------------------------------------------
	//	String sLlaveSiguiente(AccesoDB		con) -- sobrecargado AEC -- 08/04/03 --
	//
	//-------------------------------------------------------------------------------------------
		private String sLlaveSiguiente(AccesoDB		con) throws NafinException{
		ResultSet		rs				= null;
		String			qrySentencia	= "";
		String 			sResultado		= "";

		try {
			qrySentencia = " SELECT  NVL(MAX(ic_cuenta),0)+1 FROM com_cuentas ";

			System.out.print("DispersionBean :: sLlaveSiguiente :: Query "+qrySentencia);

			rs = con.queryDB(qrySentencia);

			while(rs.next()){
				sResultado = rs.getString(1);
			}

			return sResultado;

		}catch(Exception e){

			throw new NafinException("SIST0001");
		}
	}/*Fin del M�todo sLlaveSiguiente*/

		//-------------------------------------------------------------------------------------------
	//	boolean bEsDiaInhabil()
	//
	//-------------------------------------------------------------------------------------------
	private Vector bEsDiaInhabil(String esFechaAplicacion, Calendar cFechaSigHabil, AccesoDB lodbConexion)
		throws NafinException{
		boolean lbOK = true;
		String lsCodError = "OK";
		boolean lbExisteDiaInhabil = false;
		ResultSet lrsSel = null;
		Vector vFecha = new Vector();

		System.out.println("TOperativasTCambioEJB::bEsDiaInhabil(E)");
		try {
			String lsCadenaSQL = "select * from comcat_dia_inhabil"+
								" where cg_dia_inhabil = '"+esFechaAplicacion.substring(0,5)+"'";
			try {
				lrsSel = lodbConexion.queryDB(lsCadenaSQL);
				if(lrsSel.next()) {
					lbExisteDiaInhabil = true;
					cFechaSigHabil.add(Calendar.DATE, 1);
					if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==7) // S�bado
						cFechaSigHabil.add(Calendar.DATE, 2);
					if(cFechaSigHabil.get(Calendar.DAY_OF_WEEK)==1) // Domingo
						cFechaSigHabil.add(Calendar.DATE, 1);
				} else
					lbExisteDiaInhabil = false;

				lrsSel.close();
				lodbConexion.cierraStatement();

				vFecha.addElement(new Boolean(lbExisteDiaInhabil));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.YEAR)));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.MONTH)));
				vFecha.addElement(new Integer(cFechaSigHabil.get(Calendar.DAY_OF_MONTH)));

			} catch (Exception error){
				throw new NafinException("DESC0041");
			}

			return vFecha;
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "SIST0001";
			throw new NafinException("SIST0001");    //El sistema esta experimentando dificultades tecnicas. Intente mas tarde
		} finally {
			if(!lbOK) throw new NafinException(lsCodError);
			System.out.println(" TOperativasTCambioEJB::bEsDiaInhabil(S)");
		}
	}/*Fin del M�todo bEsDiaInhabil*/


 public Vector getEstatusXAsignar(String tipoUsuario) throws NafinException {
	System.out.println("Lineacredito::getEstatusXAsignar (E)");
    String		qrySentencia	= "";
    AccesoDB	con				= null;
    ResultSet	rs				= null;
    Vector 		vecFilas		= new Vector();
    Vector		vecColumnas		= new Vector();
	try {
		con = new AccesoDB();
		con.conexionDB();
		qrySentencia  = " select ic_estatus_linea,cd_descripcion"   +
			" from comcat_estatus_linea";
		if("I".equals(tipoUsuario))
			qrySentencia +=	" where ic_estatus_linea in(7,8,9,12)"  ;
		else {
			if("N".equals(tipoUsuario))
				qrySentencia +=	" where ic_estatus_linea in(9,10,12)"  ;
		}

		//System.out.println(qrySentencia);
		rs = con.queryDB(qrySentencia);
		while(rs.next()) {
			vecColumnas = new Vector();
/*0*/		vecColumnas.addElement((rs.getString("ic_estatus_linea")==null)?"":rs.getString("ic_estatus_linea"));
/*1*/		vecColumnas.addElement((rs.getString("cd_descripcion")==null)?"":rs.getString("cd_descripcion"));
			vecFilas.addElement(vecColumnas);
		}
		con.cierraStatement();
	} catch(Exception e){
		System.out.println("LineaCredito::getEstatusXAsignar Exception " +e);
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())
		con.cierraConexionDB();
		System.out.println("LineaCredito::getEstatusXAsignar (E)");
	}
	return vecFilas;
 }//getEstatusXAsignar


 public Vector cambiaEstatusLinea(String folios[],String estatus[],String tiposCredito[]) throws NafinException{
	System.out.println("LineaCreditoBean::cambiaEstatusLinea (E)");
	AccesoDB con 		= null;
	String qrySentencia	= "";
	boolean resultado	= true;
	PreparedStatement	ps = null;
	ResultSet			rs = null;
	String				inDM = "";
	String				inCCC= "";
	int i = 0;
	Vector	vecFilas	= new Vector();
	Vector	vecColumnas	= null;
	try {
		con = new AccesoDB();
		con.conexionDB();
		for(i=0;i<tiposCredito.length;i++){
			qrySentencia = "";
			if("C".equals(tiposCredito[i])&&!"".equals(estatus[i])){
				qrySentencia = " update com_linea_credito set ic_estatus_linea = ?"+
					" ,df_cambio = SYSDATE"+
					" where ic_linea_credito = ?";
				if(!"".equals(inCCC))
					inCCC += ",";
				inCCC += folios[i];
			}else if("D".equals(tiposCredito[i])&&!"".equals(estatus[i])){
				qrySentencia = " update dis_linea_credito_dm set ic_estatus_linea = ?"+
					" ,df_cambio = SYSDATE"+
					" where ic_linea_credito_dm = ?";
				if(!"".equals(inDM))
					inDM += ",";
				inDM += folios[i];
			}
			if(!"".equals(qrySentencia)){
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,estatus[i]);
				ps.setString(2,folios[i]);
				ps.execute();
			}
		}
		ps.close();
		qrySentencia = "";
		if(!"".equals(inDM)){
			qrySentencia = "SELECT lc.ic_linea_credito_dm AS folio,  'Descuento' as tipoCredito, "+
				"       DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol, "+
				"       TO_CHAR (cn.ic_nafin_electronico) as nEpo, ce.cg_razon_social as EPO, 'N/A' as NPyme, 'N/A' as Pyme, ic_linea_credito_dm_padre as lineaPadre, "+
				"       'N/A' as FechaSolic, TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_auto, cm.cd_nombre, "+
				"       el.cd_descripcion, '' AS causas, lc.ig_plazo as plazo, "+
				"       TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc, "+
				"       cf.cd_nombre, lc.cg_num_cuenta_epo as cuenta_epo, lc.cg_num_cuenta_if as cuenta_if, lc.fn_monto_autorizado_total,lc.FN_SALDO_TOTAL as saldo_total, lc.ic_moneda "+
				"		,el.ic_estatus_linea,cif.cg_razon_social "+
				"  FROM dis_linea_credito_dm lc, "+
				"       comrel_nafin cn, "+
				"       comcat_epo ce, "+
				"       comcat_if cif, "+
				"       dis_acuse4 ac4, "+
				"       comcat_moneda cm, "+
				"       comcat_estatus_linea el, "+
				"       comcat_financiera cf "+
				" WHERE lc.ic_epo = cn.ic_epo_pyme_if "+
				"   AND cn.cg_tipo = 'E' "+
				"   AND lc.ic_epo = ce.ic_epo "+
				"   AND lc.cc_acuse = ac4.cc_acuse "+
				"   AND lc.ic_moneda = cm.ic_moneda "+
				"   AND lc.ic_estatus_linea = el.ic_estatus_linea "+
				"   AND lc.ic_financiera = cf.ic_financiera (+) "+
				"   AND lc.ic_if = cif.ic_if "+
				"   AND lc.ic_linea_credito_dm in("+inDM+")";
		}
		if(!"".equals(inCCC)){
			if(!"".equals(qrySentencia))
				qrySentencia += " union all ";
			qrySentencia += 	" SELECT lc.ic_linea_credito AS folio, 'Credito en Cuenta Corriente' as tipoCredito, "+
				"       DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol, "+
				"       'N/A' as nEpo, 'N/A' as EPO, TO_CHAR (cn.ic_nafin_electronico) as NPyme, cp.cg_razon_social as Pyme, ic_linea_credito_padre as lineaPadre, "+
				"       TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic, "+
				"       TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto, "+
				"       cm.cd_nombre, el.cd_descripcion, lc.cg_causa_rechazo as causas, cpl.in_plazo_dias as plazo, "+
				"       TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc, "+
				"       cf.cd_nombre, lc.cg_numero_cuenta as cuenta_epo, lc.cg_numero_cuenta_if as cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL as saldo_total, lc.ic_moneda "+
				"		,el.ic_estatus_linea,cif.cg_razon_social "+
				"  FROM com_linea_credito lc, "+
				"       comrel_nafin cn, "+
				"       comcat_pyme cp, "+
				"       comcat_if cif, "+
				"       comcat_moneda cm, "+
				"       comcat_estatus_linea el, "+
				"       comcat_plazo cpl, "+
				"       comcat_financiera cf "+
				" WHERE lc.ic_pyme = cn.ic_epo_pyme_if "+
				"   AND cn.cg_tipo = 'P' "+
				"   AND lc.ic_pyme = cp.ic_pyme "+
				"   AND lc.ic_moneda = cm.ic_moneda "+
				"   AND lc.ic_estatus_linea = el.ic_estatus_linea "+
				"   AND lc.ic_plazo = cpl.ic_plazo(+)"+
				"   AND lc.ic_financiera = cf.ic_financiera (+) "+
				"   AND lc.ic_if = cif.ic_if "+
				"	AND lc.ic_linea_credito in("+inCCC+")";
		}
		rs = con.queryDB(qrySentencia);
		while(rs.next()){
			vecColumnas = new Vector();
/*0 folio	*/	vecColumnas.add((rs.getString(1)==null)?"":rs.getString(1));
/*1 tipoCre	*/	vecColumnas.add((rs.getString(2)==null)?"":rs.getString(2));
/*2 tipoSol	*/	vecColumnas.add((rs.getString(3)==null)?"":rs.getString(3));
/*3 NE epo	*/	vecColumnas.add((rs.getString(4)==null)?"":rs.getString(4));
/*4 epo		*/	vecColumnas.add((rs.getString(5)==null)?"":rs.getString(5));
/*5 NE pyme	*/	vecColumnas.add((rs.getString(6)==null)?"":rs.getString(6));
/*6 pyme	*/	vecColumnas.add((rs.getString(7)==null)?"":rs.getString(7));
/*7 folioRel*/	vecColumnas.add((rs.getString(8)==null)?"":rs.getString(8));
/*8 fechSol	*/	vecColumnas.add((rs.getString(9)==null)?"":rs.getString(9));
/*9	fechAut	*/	vecColumnas.add((rs.getString(10)==null)?"":rs.getString(10));
/*10 moneda	*/	vecColumnas.add((rs.getString(11)==null)?"":rs.getString(11));
/*11 estatus*/	vecColumnas.add((rs.getString(12)==null)?"":rs.getString(12));
/*12 causa	*/	vecColumnas.add((rs.getString(13)==null)?"":rs.getString(13));
/*13 plazo	*/	vecColumnas.add((rs.getString(14)==null)?"":rs.getString(14));
/*14 fechaV	*/	vecColumnas.add((rs.getString(15)==null)?"":rs.getString(15));
/*15 banco	*/	vecColumnas.add((rs.getString(16)==null)?"":rs.getString(16));
/*16 bcoEpo	*/	vecColumnas.add((rs.getString(17)==null)?"":rs.getString(17));
/*17 bcoIF	*/	vecColumnas.add((rs.getString(18)==null)?"":rs.getString(18));
/*18 montoAu*/	vecColumnas.add((rs.getString(19)==null)?"0":rs.getString(19));
/*19 montoSo*/	vecColumnas.add((rs.getString(20)==null)?"0":rs.getString(20));
/*20 icMoned*/	vecColumnas.add((rs.getString(21)==null)?"":rs.getString(21));
/*21 icEst	*/	vecColumnas.add((rs.getString(22)==null)?"":rs.getString(22));
/*23 IF	*/		vecColumnas.add((rs.getString(23)==null)?"":rs.getString(23));
				vecFilas.add(vecColumnas);
		}
		con.cierraStatement();
	} catch(Exception e) {
		resultado = false;
		throw new NafinException("SIST0001");
	} finally {
		con.terminaTransaccion(resultado);
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
		System.out.println("LineaCreditoBean::cambiaEstatusLinea (S)");
	}
	return vecFilas;
 }

/****************************************************/
/**Sobrecarga de m�todo para Factoraje con recursos**/
/****************************************************/
	public Vector cambiaEstatusLinea(String folios[],String estatus[],String tiposCredito[], String montosAut[], String saldoTot[],	String fechaVence[]) throws AppException{
		System.out.println("LineaCreditoBean::cambiaEstatusLinea (E)");
		AccesoDB con 		= null;
		StringBuffer qrySentencia;
		boolean resultado	= true;
		PreparedStatement	ps = null;
		ResultSet			rs = null;
		String				inFF= "";
		int i = 0;
		Vector	vecFilas	= new Vector();
		Vector	vecColumnas	= null;
		try {
			con = new AccesoDB();
			con.conexionDB();
			for(i=0;i<tiposCredito.length;i++){
				qrySentencia	= new StringBuffer();

				if( "F".equals(tiposCredito[i]) ){
					
					qrySentencia.append(" ");
			
					if (!"".equals(estatus[i]) || !"".equals(montosAut[i]) || !"".equals(fechaVence[i]) ){
						qrySentencia.append("UPDATE com_linea_credito SET df_cambio = SYSDATE ");
						if (!"".equals(estatus[i]) ){
							qrySentencia.append(",ic_estatus_linea = ? ");
						}
						if (!"".equals(montosAut[i]) ){
							qrySentencia.append(",fn_monto_autorizado_total = ? ");
						}
						if (!"".equals(saldoTot[i]) ){
							qrySentencia.append(",fn_saldo_linea_total = ? ");	//fn_saldo_linea = ? = este campo tambi�s es de l�nea ?????? hvargas
						}
						if (!"".equals(fechaVence[i]) ){
							qrySentencia.append(",df_vencimiento = to_date(?,'dd/mm/yyyy'), df_vencimiento_adicional = to_date(?,'dd/mm/yyyy') ");
						}
						qrySentencia.append(" WHERE ic_linea_credito = ?");
					}

					if(!"".equals(inFF))
						inFF += ",";
					inFF += folios[i];
				}
				if(!"".equals(qrySentencia.toString())){
					if (!"".equals(estatus[i]) || !"".equals(montosAut[i]) || !"".equals(fechaVence[i]) ){
						ps = con.queryPrecompilado(qrySentencia.toString());
						int inx = 1;
						if (!"".equals(estatus[i]) ){
							ps.setString(inx++,estatus[i]);
						}
						if (!"".equals(montosAut[i]) ){
							ps.setBigDecimal(inx++,new BigDecimal(montosAut[i]));
						}
						if (!"".equals(saldoTot[i]) ){
							ps.setBigDecimal(inx++,new BigDecimal(saldoTot[i]));
						}
						if (!"".equals(fechaVence[i]) ){
							ps.setString(inx++,fechaVence[i]);
							ps.setString(inx++,fechaVence[i]);
						}
						ps.setString(inx,folios[i]);
						//System.out.println("el update queda - - - - "+ qrySentencia.toString());
						ps.execute();
					}
				}
			}
			ps.close();
			qrySentencia = new StringBuffer();
			if(!"".equals(inFF)){
				qrySentencia.append(" SELECT lc.ic_linea_credito AS folio, 'Factoraje con recurso' as tipoCredito, "+
					"       DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol, "+
					"       'N/A' as nEpo, 'N/A' as EPO, TO_CHAR (cn.ic_nafin_electronico) as NPyme, cp.cg_razon_social as Pyme, ic_linea_credito_padre as lineaPadre, "+
					"       TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic, "+
					//"       TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto, "+
					"       TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS fecha_auto, "+
					"       cm.cd_nombre, el.cd_descripcion, lc.cg_causa_rechazo as causas, cpl.in_plazo_dias as plazo, "+
					"       TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc, "+
					"       cf.cd_nombre, lc.cg_numero_cuenta as cuenta_epo, lc.cg_numero_cuenta_if as cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL as saldo_total, lc.ic_moneda "+
					"		,el.ic_estatus_linea,cif.cg_razon_social, lc.ig_plazo_max, TO_CHAR(lc.df_cambio,'dd/mm/yyyy hh24:mm:ss') AS fecha_cambio, "+
					"		lc.ig_cuenta_bancaria, lc.cg_usuario_cambio, lc.cs_factoraje_con_rec "+
					"  FROM com_linea_credito lc, "+
					"       comrel_nafin cn, "+
					"       comcat_pyme cp, "+
					"       comcat_if cif, "+
					"       comcat_moneda cm, "+
					"       comcat_estatus_linea el, "+
					"       comcat_plazo cpl, "+
					"       comcat_financiera cf "+
					" WHERE lc.ic_pyme = cn.ic_epo_pyme_if "+
					"   AND cn.cg_tipo = 'P' "+
					"   AND lc.ic_pyme = cp.ic_pyme "+
					"   AND lc.ic_moneda = cm.ic_moneda "+
					"   AND lc.ic_estatus_linea = el.ic_estatus_linea "+
					"   AND lc.ic_plazo = cpl.ic_plazo(+)"+
					"   AND lc.ic_financiera = cf.ic_financiera (+) "+
					"   AND lc.ic_if = cif.ic_if "+
					"	AND lc.ic_linea_credito in("+inFF+")");
			}
			rs = con.queryDB(qrySentencia.toString());
			while(rs.next()){
				vecColumnas = new Vector();
		/*0 folio	*/	vecColumnas.add((rs.getString(1)==null)?"":rs.getString(1));
		/*1 tipoCre	*/	vecColumnas.add((rs.getString(2)==null)?"":rs.getString(2));
		/*2 tipoSol	*/	vecColumnas.add((rs.getString(3)==null)?"":rs.getString(3));
		/*3 NE epo	*/	vecColumnas.add((rs.getString(4)==null)?"":rs.getString(4));
		/*4 epo		*/	vecColumnas.add((rs.getString(5)==null)?"":rs.getString(5));
		/*5 NE pyme	*/	vecColumnas.add((rs.getString(6)==null)?"":rs.getString(6));
		/*6 pyme	*/	vecColumnas.add((rs.getString(7)==null)?"":rs.getString(7));
		/*7 folioRel*/	vecColumnas.add((rs.getString(8)==null)?"":rs.getString(8));
		/*8 fechSol	*/	vecColumnas.add((rs.getString(9)==null)?"":rs.getString(9));
		/*9	fechAut	*/	vecColumnas.add((rs.getString(10)==null)?"":rs.getString(10));
		/*10 moneda	*/	vecColumnas.add((rs.getString(11)==null)?"":rs.getString(11));
		/*11 estatus*/	vecColumnas.add((rs.getString(12)==null)?"":rs.getString(12));
		/*12 causa	*/	vecColumnas.add((rs.getString(13)==null)?"":rs.getString(13));
		/*13 plazo	*/	vecColumnas.add((rs.getString(14)==null)?"":rs.getString(14));
		/*14 fechaV	*/	vecColumnas.add((rs.getString(15)==null)?"":rs.getString(15));
		/*15 banco	*/	vecColumnas.add((rs.getString(16)==null)?"":rs.getString(16));
		/*16 bcoEpo	*/	vecColumnas.add((rs.getString(17)==null)?"":rs.getString(17));
		/*17 bcoIF	*/	vecColumnas.add((rs.getString(18)==null)?"":rs.getString(18));
		/*18 montoAu*/	vecColumnas.add((rs.getString(19)==null)?"0":rs.getString(19));
		/*19 montoSo*/	vecColumnas.add((rs.getString(20)==null)?"0":rs.getString(20));
		/*20 icMoned*/	vecColumnas.add((rs.getString(21)==null)?"":rs.getString(21));
		/*21 icEst	*/	vecColumnas.add((rs.getString(22)==null)?"":rs.getString(22));
		/*22 IF	*/		vecColumnas.add((rs.getString(23)==null)?"":rs.getString(23));
		/*23 ctaBanca*/vecColumnas.add((rs.getString("ig_cuenta_bancaria")==null)?"":rs.getString("ig_cuenta_bancaria"));
		/*24 usuario */vecColumnas.add((rs.getString("cg_usuario_cambio")==null)?"":rs.getString("cg_usuario_cambio"));
		/*25 fec_camb*/vecColumnas.add((rs.getString("fecha_cambio")==null)?"":rs.getString("fecha_cambio"));
				vecFilas.add(vecColumnas);
			}
			con.cierraStatement();
		} catch(Exception e) {
			resultado = false;
			throw new AppException("Error al actualizar la informaci�n"+e);
		} finally {
			con.terminaTransaccion(resultado);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("LineaCreditoBean::cambiaEstatusLinea (S)");
		}
		return vecFilas;
	}

/*******************************************************/
/* FODA 099: Autorizacion de numero de solicitud troya */
/*******************************************************/

public String ActualizaSolicTroya(String ic_linea,String iClienteTroya, String iSolicTroya)
throws NafinException
{ boolean resultado=true;
  String  query=null,mensaje=null;
  AccesoDB con=null;
  PreparedStatement ps= null;
  try
  { System.out.println("LineaCreditoBean::ActualizaSolicTroya(E)");
    con = new AccesoDB();
    con.conexionDB();
    mensaje=VerificaSolicTroya(ic_linea,iClienteTroya,iSolicTroya,con);
    if (mensaje.equals(""))
    {  mensaje="Error en la conexion con Troya, favor de verificarla!";
	   resultado=false;
    } else if (mensaje.equals("0"))
	{
    	query=" UPDATE COM_LINEA_CREDITO "+
   			  " SET IG_NUMERO_SOLIC_TROYA= ? "+
    		  " WHERE IC_LINEA_CREDITO= ? ";
  		ps = con.queryPrecompilado(query);
    	ps.setString(1,iSolicTroya);
    	ps.setInt(2,Integer.parseInt(ic_linea));
    	ps.execute();
    	if (ps!=null) ps.close();
		mensaje="Los cambios se almacenaron exitosamente";
		resultado=true;
    } else if (mensaje.equals("1"))
	{
		mensaje="El Numero de Solicitud Troya no es valido, favor de verificarlo";
        resultado=false;
    } else if (mensaje.equals("3"))
	{
		mensaje="Este Numero de Solicitud Troya ya tiene linea de credito asociada";
        resultado=false;
    } else if (mensaje.equals("5"))
	{
		mensaje="El Numero de Cliente Troya no es valido, favor de verificarlo";
        resultado=false;
	} else if (mensaje.equals("2"))
	{ mensaje="Error en la conexion con Troya, favor de verificarla!";
      resultado=false;
	} else
	{ mensaje="Existen dificultades en el sistema";
      resultado=false;
	}
  } catch (Exception e)
  {   System.out.println(e.toString());
	  resultado=false;
	  mensaje="Error en la conexion con Troya, favor de verificarla!";
	  throw new NafinException("ANTI0034");
  } finally
  {  if (con.hayConexionAbierta())
     {  con.terminaTransaccion(resultado);
        con.cierraConexionDB();
 	 }
 	 System.out.println("LineaCreditoBean::ActualizaSolicTroya(S)");
  }
  return(mensaje);
} // Fin del metodo ActualizaSolicTroya()


public String obtenTipoPiso (String cif)
throws NafinException
{ AccesoDB con=null;
  ResultSet rs=null;
  PreparedStatement ps=null;
  String query=" select ig_tipo_piso TP from comcat_if where ic_if = ?";
  String tipoPiso=null;
  try
  { System.out.println("LineaCreditoBean::obtenTipoPiso(E)");
    con=new AccesoDB();
    con.conexionDB();
    ps = con.queryPrecompilado(query);
    ps.setInt(1,Integer.parseInt(cif));
    rs=ps.executeQuery();
    if (rs.next())
    {  tipoPiso=(rs.getString("TP")==null)?"":rs.getString("TP");
	}
	if (rs!=null) rs.close();
	if (ps!=null) ps.close();
  } catch (Exception e)
  {   System.out.println(e.toString());
      tipoPiso="";
      throw new NafinException("SIST0001");
  } finally
  {  if (con.hayConexionAbierta())
     {
        con.cierraConexionDB();
 	 }
   	 System.out.println("LineaCreditoBean::obtenTipoPiso(S)");
  }
  return(tipoPiso);
}

public String obtenTipoTroya (String ic_producto_nafin) throws NafinException {
		AccesoDB con=null;
		ResultSet rs=null;
		PreparedStatement ps=null;
		String query =
			" SELECT cs_troya"   +
			"   FROM comcat_producto_nafin"   +
			"  WHERE ic_producto_nafin = ? "  ;
		String cs_troya = "";
		try {
			System.out.println("LineaCreditoBean::obtenTroyaObliga(E)");
			con=new AccesoDB();
			con.conexionDB();
			ps = con.queryPrecompilado(query);
			ps.setInt(1,Integer.parseInt(ic_producto_nafin));
			rs=ps.executeQuery();
			if (rs.next()) {
    			cs_troya = rs.getString("cs_troya")==null?"":rs.getString("cs_troya");
			}
			if (rs!=null) rs.close();
			if (ps!=null) ps.close();
  		} catch (Exception e) {
  			System.out.println(e.toString());
			cs_troya="";
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("LineaCreditoBean::obtenTroyaObliga(S)");
		}
		return(cs_troya);
}


public String obtenClienteTroya (String icPyme)
throws NafinException
{ AccesoDB con=null;
  ResultSet rs=null;
  PreparedStatement ps=null;
  String query=" select in_numero_troya TP from comcat_pyme where ic_pyme = ?";
  String ClienteT=null;
  try
  { System.out.println("LineaCreditoBean::obtenClienteTroya(E)");
    con=new AccesoDB();
    con.conexionDB();
    ps = con.queryPrecompilado(query);
    ps.setInt(1,Integer.parseInt(icPyme));
    rs=ps.executeQuery();
    if (rs.next())
    {  ClienteT=(rs.getString("TP")==null)?"":rs.getString("TP");
	}
	if (rs!=null) rs.close();
	if (ps!=null) ps.close();
  } catch (Exception e)
  {   System.out.println(e.toString());
      ClienteT="";
      throw new NafinException("SIST0001");
  } finally
  {  if (con.hayConexionAbierta())
     {
        con.cierraConexionDB();
 	 }
   	 System.out.println("LineaCreditoBean::obtenClienteTroya(S)");
  }
  return(ClienteT);
}

private String VerificaSolicTroya(String ic_linea,String iClienteTroya, String iSolicTroya, AccesoDB con)
throws NafinException
{
  String  query=null,mensaje=null;
  ResultSet rs=null;
  try
  { System.out.println("LineaCreditoBean::VerificaSolicTroya2(E)");
    query=" select VERIFICA_SOLICITUD_TROYA("+ic_linea+",'"+iSolicTroya+"',"+iClienteTroya+") mensaje from dual ";
    rs=con.queryDB(query);
    if (rs.next())
    {  mensaje=(rs.getString("MENSAJE")==null)?"":rs.getString("MENSAJE");
       if (mensaje.equals(""))
       {  mensaje="2";
	   }
	} else
	{ mensaje="2";
	}
	if (rs!=null) rs.close();
	con.cierraStatement();
  } catch (Exception e)
  {   System.out.println(e.toString());
	  mensaje="2";
  }
  System.out.println("LineaCreditoBean::VerificaSolicTroya2(S)");
	return(mensaje);
} // Fin del metodo  VerificaSolicTroya()


public String VerificaSolicTroya(String ic_linea,String iClienteTroya, String iSolicTroya)
throws NafinException
{
  String  query=null,mensaje=null;
  AccesoDB con=null;
  ResultSet rs=null;
  try
  { System.out.println("LineaCreditoBean::VerificaSolicTroya(E)::MetodoPublico");
    con = new AccesoDB();
    con.conexionDB();
    query=" select VERIFICA_SOLICITUD_TROYA("+ic_linea+",'"+iSolicTroya+"',"+iClienteTroya+") mensaje from dual ";
    System.out.println("************QUERY"+query);
    rs=con.queryDB(query);
    if (rs.next())
    {  mensaje=(rs.getString("MENSAJE")==null)?"":rs.getString("MENSAJE");
       if (mensaje.equals(""))
       {  mensaje="2";
	   }
	} else
	{ mensaje="2";
	}
	if (rs!=null) rs.close();
	con.cierraStatement();
  } catch (Exception e)
  {   System.out.println(e.toString());
	  mensaje="2";
	  throw new NafinException("ANTI0034");
  } finally
  {  con.terminaTransaccion(true);
	 if (con.hayConexionAbierta())
     {
        con.cierraConexionDB();
 	 }
  }
 System.out.println("LineaCreditoBean::VerificaSolicTroya(S)");
 return(mensaje);

} // Fin del metodo VerificaSolicTroya()

public Vector setLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					   String banco, String cuenta, String plazo, String estatus,
					   String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					   String monto_autorizado, String linea_automatica, String linea_individual,
					   String credito_adicional, String linea_total, String ic_linea,
					   String ic_producto, String ic_moneda, String cuenta_clabe, 
					   String fecha_autorizacion, String solic_troya)
		throws NafinException {
	System.out.println("LineaCredito::setLCtmp(E)");
    Vector		vecColumnas		= new Vector();			
	try {
		vecColumnas = setLCtmp(ic_proceso, ic_pyme, ic_if, ic_epo,
					   banco, cuenta, plazo, estatus,
					   tipo_solicitud, causa_rechazo, monto_solicitado,
					   monto_autorizado, linea_automatica, linea_individual,
					   credito_adicional, linea_total, ic_linea,
					   ic_producto, ic_moneda, cuenta_clabe,
					   fecha_autorizacion, solic_troya, "");
		
    } catch(Exception e) {
      System.out.println("LineaCredito.setLCtmp Exception"+e);
      throw new NafinException("ANTI0020");
    } finally {
		System.out.println("LineaCredito::setLCtmp(S)");
	}
	 return vecColumnas;
}//setLCtmp()

public Vector setLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					   String banco, String cuenta, String plazo, String estatus,
					   String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					   String monto_autorizado, String linea_automatica, String linea_individual,
					   String credito_adicional, String linea_total, String ic_linea,
					   String ic_producto, String ic_moneda, String cuenta_clabe, 
					   String fecha_autorizacion, String solic_troya, String fecha_limite)
		throws NafinException {
	System.out.println("LineaCredito::setLCtmp(E)");
    Vector		vecColumnas		= new Vector();			
	try {
		vecColumnas = setLCtmp(ic_proceso, ic_pyme, ic_if, ic_epo,
					   banco, cuenta, plazo, estatus,
					   tipo_solicitud, causa_rechazo, monto_solicitado,
					   monto_autorizado, linea_automatica, linea_individual,
					   credito_adicional, linea_total, ic_linea,
					   ic_producto, ic_moneda, cuenta_clabe,
					   fecha_autorizacion, solic_troya, fecha_limite, "", "");
		
    } catch(Exception e) {
      System.out.println("LineaCredito.setLCtmp Exception"+e);
      throw new NafinException("ANTI0020");
    } finally {
		System.out.println("LineaCredito::setLCtmp(S)");
	}
	 return vecColumnas;
}//setLCtmp()


public Vector setLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					   String banco, String cuenta, String plazo, String estatus,
					   String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					   String monto_autorizado, String linea_automatica, String linea_individual,
					   String credito_adicional, String linea_total, String ic_linea,
					   String ic_producto, String ic_moneda, String cuenta_clabe, 
					   String fecha_autorizacion, String solic_troya, String fecha_limite, 
					   String cred_exp, String num_max_amort)
		throws NafinException {
	System.out.println("LineaCredito::setLCtmp(S)");
    String		qrySentencia	= "";
    ResultSet	rs 				= null;
    AccesoDB	con				= null;
	String      proceso			= "";
	String      linCredito		= "";
	String      mensaje			= "";
	String      Dias			= "0";
	String 		Disposicion     = "0";
    Vector		vecColumnas		= new Vector();
    Vector		vecFilasP		= null;
    Vector		vecColumnasP	= null;
    boolean		resultado		= true;
    try {
		con = new AccesoDB();
		con.conexionDB();
		if(ic_proceso.equals("")) {
			qrySentencia = 
				"  SELECT NVL(MAX(ic_proceso),0)+1 "+
				" FROM comtmp_linea_credito ";
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				proceso = rs.getString(1);
			rs.close();
			con.cierraStatement();
				
		} else
			proceso = ic_proceso;
   		vecColumnas.addElement(proceso);
		System.out.println("proceso["+proceso+"]");

		// validar q no se haya dado de alta una linea
		if(!tipo_solicitud.equals("I")) {
			qrySentencia = 
				" SELECT ic_tmp_linea_credito"   +
				" FROM comtmp_linea_credito"   +
				" WHERE ic_proceso = "+ proceso ;
			if(!ic_linea.equals(""))
				qrySentencia += " AND ic_linea_credito_padre = " + ic_linea  ;
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				mensaje = "SI";
			else
				mensaje = "NO";
			rs.close();
			con.cierraStatement();				
		} else { // Valida que el proveedor no tenga otra l�nea inicial
				qrySentencia = 
					" SELECT ic_tmp_linea_credito"   +
					" FROM comtmp_linea_credito"   +
					" WHERE ic_proceso = "   + proceso +
					" AND ic_pyme = "   + ic_pyme +
					" AND cg_tipo_solicitud = 'I'"  ;
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					mensaje = "SII";
				else
					mensaje = "NO";
				rs.close();
				con.cierraStatement();
		} 
		if(mensaje.equals("NO")) {
			qrySentencia = 
				" SELECT NVL(MAX(ic_tmp_linea_credito),0)+1"   +
				" FROM comtmp_linea_credito" ;
			rs = con.queryDB(qrySentencia);
			if(rs.next())
				linCredito = rs.getString(1);
			rs.close();
			con.cierraStatement();
			if(plazo.equals("")) 			 
				plazo = "null";
			else {
				qrySentencia = 
					" SELECT  in_plazo_dias "+
					" FROM comcat_plazo "+
					" WHERE ic_plazo = "+plazo;
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					Dias = rs.getString(1);
				rs.close();
				con.cierraStatement();
			}

			if(monto_solicitado.equals(""))  	monto_solicitado = "null";
			if(linea_automatica.equals(""))  	linea_automatica = "null";
			if(linea_individual.equals(""))  	linea_individual = "null";
			if(monto_autorizado.equals("")) 	monto_autorizado = "null";
			if(credito_adicional.equals(""))	credito_adicional = "null";
			if(linea_total.equals(""))  		linea_total = "null";
			if(ic_linea.equals(""))  	 		ic_linea = "null";
			if(banco.equals(""))  	 	 		banco = "null";
			if(ic_moneda.equals(""))  	 		ic_moneda = "null";
			if(solic_troya.equals(""))		 	solic_troya = " null ";
			else 								solic_troya = "'"+solic_troya+"'" ;
			if(fecha_autorizacion.equals("")) 	fecha_autorizacion=" null ";
			else								fecha_autorizacion=" to_date('"+fecha_autorizacion+"','dd/mm/yyyy') ";
			
			String ic_tabla_amort = "";
			if("S".equals(cred_exp)) {
				cred_exp = "'E'";
				qrySentencia = 
					" SELECT ic_tabla_amort_exp"   +
					"   FROM comcat_producto_nafin"   +
					"  WHERE ic_producto_nafin = 5"  ;
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					ic_tabla_amort = rs.getString(1);
				rs.close();
				con.cierraStatement();
			} else {
				cred_exp = "'C'";
				qrySentencia = 
					" SELECT ic_tabla_amort"   +
					"   FROM comcat_producto_nafin"   +
					"  WHERE ic_producto_nafin = 5"  ;
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					ic_tabla_amort = rs.getString(1);
				rs.close();
				con.cierraStatement();
				num_max_amort = "NULL";
			}

			if(ic_producto.equals("5")) {
				vecFilasP = getDispVig(ic_producto,ic_if);
				for(int i=0;i<vecFilasP.size();i++) {
					vecColumnasP 			= (Vector)vecFilasP.get(i);
					Disposicion				= (String)vecColumnasP.get(0);
				}
			}

			qrySentencia =  
				" insert into comtmp_linea_credito "+
				" (ic_tmp_linea_credito, ic_proceso, ic_pyme, ic_if, ic_epo, ic_financiera, ic_plazo, "+
				" ic_estatus_linea, cg_tipo_solicitud, df_captura, cg_causa_rechazo, fn_monto_solicitado, "+
				" fn_linea_automatica, fn_linea_individual, fn_monto_autorizado, fn_linea_opcional, " +
				" fn_monto_autorizado_total, ic_linea_credito_padre, ic_producto_nafin, cg_numero_cuenta, "+
				" cg_cuenta_clabe, df_vencimiento, ic_moneda , ig_numero_solic_troya, df_autorizacion, "+
				" df_limite_disposicion, cg_linea_tipo, ic_tabla_amort, ig_numero_max_amort_exp ";

			qrySentencia += 
				" )"+
				" values "+
				" ("+ linCredito +
				" ,"+ proceso +
				" ,"+ ic_pyme +
				" ,"+ ic_if +
				" ,"+ ic_epo +
				" ,"+ banco +
				" ,"+ plazo +
				" ,"+ estatus +
				" ,'" + tipo_solicitud + "'" +
				" , sysdate " +
				" ,'"+ causa_rechazo + "'" +
				" ,"+ monto_solicitado +
				" ," + linea_automatica +
				" ," + linea_individual +
				" ," + linea_total +
				" ," + credito_adicional +
				" ," + linea_total +
				" ," + ic_linea +
				" ," + ic_producto +
				" ,'" + cuenta +"'"+
				" ,'" + cuenta_clabe +"'"+
				" , ADD_MONTHS("+fecha_autorizacion+"," +Disposicion+ ")+" + Dias +
				" , "+ ic_moneda +
				" , "+ solic_troya +
				" , "+ fecha_autorizacion;

			if(ic_producto.equals("5")) {
				qrySentencia += 
					" , ADD_MONTHS("+fecha_autorizacion+"," +Disposicion+ ")" ;
			} else {
				qrySentencia += 
					" , to_date('"+fecha_limite+"', 'dd/mm/yyyy')" ;
				
			}
			qrySentencia += 			
				" , "+ cred_exp+
				" , "+ ic_tabla_amort+
				" , "+ num_max_amort;								

			qrySentencia += ")";
			con.ejecutaSQL(qrySentencia);
		}//if(mensaje.equals("NO"))
		vecColumnas.addElement(mensaje);
    } catch(Exception e) {
		System.out.println("LineaCredito::setLCtmp(Exception) "+e);
		e.printStackTrace();
		resultado = false;
		throw new NafinException("ANTI0020");
	} finally {
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(resultado);
			con.cierraConexionDB();
		}
		System.out.println("LineaCredito::setLCtmp(S) ");
	}
	return vecColumnas;
}         //setLCtmp
  
public void updtLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					  String banco, String cuenta, String plazo, String estatus,
					  String tipo_solicitud, String causa_rechazo, String monto_solicitado, String monto_autorizado, 
					  String linea_automatica, String linea_individual, String credito_adicional, String linea_total, 
					  String ic_linea, String ic_producto, String linCredito, String ic_moneda, 
					  String cuenta_clabe , String fecha_autorizacion, String ic_solic_troya)
			throws NafinException {
    try {
		System.out.println("LineaCreditoBean::updtLCtmp(E)");
		updtLCtmp(ic_proceso, ic_pyme, ic_if, ic_epo,
					banco, cuenta, plazo, estatus,
					tipo_solicitud, causa_rechazo, monto_solicitado, monto_autorizado, 
					linea_automatica, linea_individual, credito_adicional, linea_total, 
					ic_linea, ic_producto, linCredito, ic_moneda, 
					cuenta_clabe , fecha_autorizacion, ic_solic_troya, "");
    } catch(Exception e) {
		System.out.println("LineaCredito.updtLCtmp Exception"+e);
		throw new NafinException("ANTI0020");
    } finally {
		System.out.println("LineaCreditoBean::updtLCtmp(S)");
	}
}//updtLCtmp

public void updtLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					  String banco, String cuenta, String plazo, String estatus,
					  String tipo_solicitud, String causa_rechazo, String monto_solicitado, String monto_autorizado, 
					  String linea_automatica, String linea_individual, String credito_adicional, String linea_total, 
					  String ic_linea, String ic_producto, String linCredito, String ic_moneda, 
					  String cuenta_clabe , String fecha_autorizacion, String ic_solic_troya, String fecha_limite)
			throws NafinException {
    try {
		System.out.println("LineaCreditoBean::updtLCtmp(E)");
		updtLCtmp(ic_proceso, ic_pyme, ic_if, ic_epo,
					banco, cuenta, plazo, estatus,
					tipo_solicitud, causa_rechazo, monto_solicitado, monto_autorizado, 
					linea_automatica, linea_individual, credito_adicional, linea_total, 
					ic_linea, ic_producto, linCredito, ic_moneda, 
					cuenta_clabe , fecha_autorizacion, ic_solic_troya, fecha_limite, "", "");
    } catch(Exception e) {
		System.out.println("LineaCredito.updtLCtmp Exception"+e);
		throw new NafinException("ANTI0020");
    } finally {
		System.out.println("LineaCreditoBean::updtLCtmp(S)");
	}
}//updtLCtmp

public void updtLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					  String banco, String cuenta, String plazo, String estatus,
					  String tipo_solicitud, String causa_rechazo, String monto_solicitado, String monto_autorizado, 
					  String linea_automatica, String linea_individual, String credito_adicional, String linea_total, 
					  String ic_linea, String ic_producto, String linCredito, String ic_moneda, 
					  String cuenta_clabe , String fecha_autorizacion, String ic_solic_troya, String fecha_limite,
					  String cred_exp, String num_max_amort)
  		throws NafinException {
    String		qrySentencia	= "";
    ResultSet	rs 				= null;
    AccesoDB	con				= null;
	String 		Dias			= "0";
	String 		Disposicion     = "0";
    Vector		vecFilasP		= null;
    Vector		vecColumnasP	= null;
    boolean		resultado		= true;

    try {
		con = new AccesoDB();
		con.conexionDB();
		System.out.println("LineaCreditoBean::updtLCtmp(E)");
		
		if(plazo.equals(""))
			plazo = "null";
		else {
			qrySentencia = 
				" SELECT  in_plazo_dias "+
				" FROM comcat_plazo "+
				" WHERE ic_plazo = "+plazo;
			rs = con.queryDB(qrySentencia);
			if(rs.next())
			Dias = rs.getString(1);
		}

		if(plazo.equals("")) 			 	plazo = "null";
		if(monto_solicitado.equals(""))  	monto_solicitado = "null";
		if(linea_automatica.equals(""))  	linea_automatica = "null";
		if(linea_individual.equals(""))  	linea_individual = "null";
		if(monto_autorizado.equals(""))  	monto_autorizado = "null";
		if(credito_adicional.equals("")) 	credito_adicional = "null";
		if(linea_total.equals(""))  	 	linea_total = "null";
		if(ic_linea.equals(""))  	 	 	ic_linea = "null";
		if(banco.equals(""))  	 	 	 	banco = "null";
		if(ic_moneda.equals(""))  	 	 	ic_moneda = "null";
		if(ic_solic_troya.equals(""))	 	ic_solic_troya = " null ";
		else 								ic_solic_troya = "'"+ic_solic_troya+"'";
		
		if(fecha_autorizacion.equals("")) 	fecha_autorizacion=" null ";
		else 								fecha_autorizacion = " to_date('"+fecha_autorizacion+"','dd/mm/yyyy') ";
		
		if(fecha_limite.equals("")) 		fecha_limite=" null ";
		else 								fecha_limite = " to_date('"+fecha_limite+"','dd/mm/yyyy') ";		

		if("S".equals(cred_exp)) {
			cred_exp = "'E'";
		} else {
			cred_exp = "'C'";
			num_max_amort = "NULL";
		}

		if(ic_producto.equals("5")) {
			vecFilasP = getDispVig(ic_producto,ic_if);
			for(int i=0;i<vecFilasP.size();i++) {
				vecColumnasP 			= (Vector)vecFilasP.get(i);
				Disposicion				= (String)vecColumnasP.get(0);
			}
		}

		qrySentencia =  
			" update comtmp_linea_credito "+
			" set ic_pyme="+ ic_pyme +
			", ic_if="+ ic_if +
			", ic_epo="+ ic_epo +
			", ic_financiera="+ banco +
			", ic_plazo="+ plazo +
			", ic_estatus_linea="+ estatus +
			", cg_tipo_solicitud='" + tipo_solicitud + "'" +
			", df_captura = sysdate "+
			", cg_causa_rechazo='"+ causa_rechazo + "'" +
			", fn_monto_solicitado="+ monto_solicitado +
			", fn_linea_automatica=" + linea_automatica +
			", fn_linea_individual=" + linea_individual +
			", fn_monto_autorizado=" + linea_total +
			", fn_linea_opcional=" + credito_adicional +
			", fn_monto_autorizado_total=" + linea_total +
			", ic_linea_credito_padre=" + ic_linea +
			", ic_producto_nafin=" + ic_producto +
			", cg_numero_cuenta ='" + cuenta +"'"+
			", cg_cuenta_clabe ='" + cuenta_clabe +"'"+
			", df_vencimiento = ADD_MONTHS("+fecha_autorizacion+"," +Disposicion+ ")+" + Dias +
			", ic_moneda = "+ ic_moneda +
			", cg_linea_tipo =" + cred_exp +""+
			", ig_numero_max_amort_exp =" + num_max_amort +"";
			
					
		if (!ic_solic_troya.equals(""))
			qrySentencia +=
				", ig_numero_solic_troya ="+ic_solic_troya;
				
		qrySentencia +=	
			", df_autorizacion = "+fecha_autorizacion;
			
		if(ic_producto.equals("2"))			
			qrySentencia +=	", df_limite_disposicion = "+fecha_limite;			
			
		if(ic_producto.equals("5"))
			qrySentencia +=  ", df_limite_disposicion = ADD_MONTHS("+fecha_autorizacion+"," +Disposicion+ ")" ;
			
		qrySentencia += 
			" where ic_tmp_linea_credito="+ linCredito +
			" and ic_proceso="+ ic_proceso ;
		
		//					System.out.println(qrySentencia);
		con.ejecutaSQL(qrySentencia);
    } catch(Exception e) {
      System.out.println("LineaCredito.updtLCtmp (Exception) "+e);
      e.printStackTrace();
	  resultado = false;
      throw new NafinException("ANTI0020");
    } finally {
		if(con.hayConexionAbierta()) {
	      con.terminaTransaccion(resultado);
	      con.cierraConexionDB();
        }
        System.out.println("LineaCreditoBean::updtLCtmp(S)");
	}
}//updtLCtmp

private void generaSolicitud(
				String ic_linea_credito, String ic_epo, String ic_pyme, String ic_if,
				String cg_tipo_solicitud, String ic_linea_padre, String ic_producto_nafin, String monto, 
				String estatus, String causa ,String plazo,	String banco, 
				String cuenta, String fechaVto, String auto,String opcion, 
				String ind, String total, String ic_moneda, String fechaLim, 
				String cuenta_clabe, String iNoUsuario, AccesoDB  con, String solicTroya, 
				String fechaAuto, String ccAcuse, String tablaAmort, String numMaxAmort, 
				String claveLineaTipo)
		throws NafinException {
    System.out.println("LineaCreditoBean::genraSolicitud(E)");			
    String    qrySentencia  = "";
    String    clienteT = "";
    String mensaje = "";
    boolean   resultado     = true;
    boolean   continuar     = false;
    ResultSet rs            = null;
    try {
  		qrySentencia = 	
			"select ic_linea_credito from com_linea_credito"+
			" where ic_linea_credito ="+ic_linea_credito+
			" and ic_estatus_linea is null";
		rs = con.queryDB(qrySentencia);
		if(rs.next())
			continuar = true;
		con.cierraStatement();
		if (!solicTroya.equals("")&&estatus.equals("3")) {
			/* Ultima validacion antes de ingresar el numero de solicitud troya */
			qrySentencia=" select IN_NUMERO_TROYA CLIENTE from comcat_pyme where ic_pyme="+ic_pyme;
			rs=con.queryDB(qrySentencia);
			if (rs.next())
				clienteT=(rs.getString("CLIENTE")==null)?"":rs.getString("CLIENTE");
			else
				clienteT="";
			rs.close();
			con.cierraStatement();
			if (clienteT.equals("")) {   
				solicTroya="";
				System.out.println("No hay cliente troya valido: No se puede almacenar el numero de solicitud troya");
			} else {
				qrySentencia=
					" select VERIFICA_SOLICITUD_TROYA("+ic_linea_credito+",'"+solicTroya+"',"+clienteT+") mensaje from dual ";
				rs=con.queryDB(qrySentencia);
				if (rs.next()) {  
					mensaje=(rs.getString("MENSAJE")==null)?"":rs.getString("MENSAJE");
					if (!mensaje.equals("0")) {
						System.out.println("La solicitud Troya es incorrecta o no se puede determinar. No se almacenara");
						solicTroya="";
					}
				} else { 
					solicTroya="";
					System.out.println("La solicitud Troya es incorrecta o no se puede determinar. No se almacenara");
				}
				if (rs!=null) rs.close();
				con.cierraStatement();
			} //Fin del if (clienteT)
		} //Fin del if(!solic..)

		if("I".equals(cg_tipo_solicitud)&&continuar) {
			qrySentencia =	
				"update com_linea_credito " +
				"set ic_pyme = "+ ic_pyme +
				", ic_epo = "+ic_epo+
				", ic_producto_nafin = "+ ic_producto_nafin +
				", ic_if = "+ic_if+
				", cg_tipo_solicitud = 'I'"+
				", ic_estatus_linea = 1"+
				",df_captura = sysdate"+
				",cc_acuse = '"+ccAcuse+"'";
		  		
			if (!solicTroya.equals(""))
				qrySentencia+=	" ,ig_numero_solic_troya = '"+solicTroya+"' ";
			qrySentencia+=	" where ic_linea_credito = "+ic_linea_credito;
		} else if(continuar) {
			if (solicTroya.equals("")) {
				qrySentencia = 	
					" UPDATE COM_LINEA_CREDITO "+
					" SET(IC_PYME,IC_IF,CG_TIPO_SOLICITUD"+
					" ,IC_ESTATUS_LINEA,IC_LINEA_CREDITO_PADRE"+
					" ,IC_FINANCIERA,CG_NUMERO_CUENTA,IC_EPO,IC_PRODUCTO_NAFIN,DF_CAPTURA,CC_ACUSE)"+
					" = (SELECT '"+ic_pyme+"',ic_if,'"+cg_tipo_solicitud+"'"+
					" ,1,IC_LINEA_CREDITO,IC_FINANCIERA,CG_NUMERO_CUENTA,IC_EPO,IC_PRODUCTO_NAFIN,SYSDATE,'"+ccAcuse+"'"+
					" FROM COM_LINEA_CREDITO WHERE IC_LINEA_CREDITO ="+ic_linea_padre+")"+
					" WHERE IC_LINEA_CREDITO = "+ic_linea_credito;
			} else {
				qrySentencia = 	
					" UPDATE COM_LINEA_CREDITO "+
					" SET(IC_PYME,IC_IF,CG_TIPO_SOLICITUD"+
					" ,IC_ESTATUS_LINEA,IC_LINEA_CREDITO_PADRE"+
					" ,IC_FINANCIERA,CG_NUMERO_CUENTA,IC_EPO,IC_PRODUCTO_NAFIN,DF_CAPTURA,IG_NUMERO_SOLIC_TROYA,CC_ACUSE)"+
					" = (SELECT '"+ic_pyme+"',ic_if,'"+cg_tipo_solicitud+"'"+
					" ,1,IC_LINEA_CREDITO,IC_FINANCIERA,CG_NUMERO_CUENTA,IC_EPO,IC_PRODUCTO_NAFIN,SYSDATE,'"+solicTroya+"','"+ccAcuse+"'  "+
					" FROM COM_LINEA_CREDITO WHERE IC_LINEA_CREDITO ="+ic_linea_padre+")"+
					" WHERE IC_LINEA_CREDITO = "+ic_linea_credito;
			}
		} else
			throw new NafinException("ANTI0019");
		System.out.println("generaSolicitud["+qrySentencia+"]");
		try {
			con.ejecutaSQL(qrySentencia);
		} catch (SQLException sqle) {
			throw new NafinException("ANTI0020");
		}
		try {
			if(ic_producto_nafin.equals(""))
				ic_producto_nafin = "null";
			AutorizacionIF(
				ic_linea_credito, monto, estatus, causa, 
				plazo, banco, cuenta, fechaVto, 
				auto, opcion, ind, total, 
				cg_tipo_solicitud, ic_moneda, ic_producto_nafin, fechaLim, 
				cuenta_clabe, iNoUsuario, con, solicTroya, 
				fechaAuto, tablaAmort, numMaxAmort, claveLineaTipo);
		} catch (NafinException ne) {
			resultado = false;
			throw ne;
		}
	} catch(NafinException ne){
		resultado = false;
		throw ne;
	} catch(Exception e) {
		resultado = false;
		throw new NafinException("SIST0001");
	}
} //Fin de generaSolicitud

private void AutorizacionIF(
				String folio, String monto, String estatus, String causa, 
				String plazo ,String banco, String cuenta, String fechaVto,
				String auto, String opcion, String ind, String total, 
				String tipo, String ic_moneda, String ic_producto, String fechaLim, 
				String cuenta_clabe, String iNoUsuario, AccesoDB con, String solicTroya, 
				String fechaAuto, String tablaAmort, String numMaxAmort, String claveLineaTipo)
		throws NafinException {
    String		qrySentencia		= "";
    boolean		resultado			= true;
	String 		slnafinElec			= "";
	String		sCta					= "";
	String  	sCta1				= "";
	Vector		vecFilas			= new Vector();

    try {
		if(opcion.equals("")) 		opcion 		= "null";
		if(ind.equals("")) 			ind			= "null";
		if(monto.equals("")) 		monto		= "null";
		if(total.equals("")) 		total		= "0";
		if(auto.equals("")) 		auto		= "null";
		if(ic_moneda.equals(""))	ic_moneda	= "1";
		if(fechaLim.equals(""))		fechaLim	= "null";
		if(plazo.equals(""))		plazo		= "null";
		if(banco.equals(""))		banco		= "null";
		if(solicTroya.equals(""))	solicTroya	= " null ";
		else 						solicTroya	= "'"+solicTroya+"'";
		
		System.out.println("producto["+ic_producto+"]");
	 
		if(tipo.equals("A")) {
			qrySentencia =  
				" update com_linea_credito "+
				" set fn_monto_autorizado ="+total+
				" ,df_vencimiento = to_date('"+fechaVto+"','dd/mm/yyyy')"+
				" ,df_vencimiento_adicional = to_date('"+fechaVto+"','dd/mm/yyyy')"+
				" ,ic_estatus_linea = "+estatus+
				" ,ic_plazo ="+ plazo+
				" ,cg_causa_rechazo = '"+causa+"'"+
				" ,cg_numero_cuenta = '"+cuenta+"'"+
				" ,cg_cuenta_clabe = '"+cuenta_clabe+"'"+
				" ,fn_monto_solicitado = "+monto+
				" ,fn_saldo_linea = "+total+
				" ,fn_saldo_linea_total = "+total+
				" ,ic_financiera = "+banco+
				" ,fn_linea_automatica = "+auto+
				" ,df_autorizacion = to_date('"+fechaAuto+"','dd/mm/yyyy')"+
				" ,df_autorizacion_if = sysdate "+
				" ,ig_numero_solic_troya = "+solicTroya+
				" ,ic_tabla_amort = "+tablaAmort;

			if(ic_producto.equals("2"))
				qrySentencia += 
					" ,fn_linea_opcional = "+opcion+
					" ,fn_linea_individual = "+ind;
			else
				qrySentencia +=	
					" ,ic_moneda = "+ ic_moneda ;
					
			if("E".equals(claveLineaTipo)) {
				qrySentencia +=	
					" ,cg_linea_tipo = 'E'"+
					" ,ig_numero_max_amort_exp = "+numMaxAmort;
			}
			
			qrySentencia += 
				" ,df_limite_disposicion = to_date('"+fechaLim+"','dd/mm/yyyy')"+
				" ,df_limite_disposicion_adic = to_date('"+fechaLim+"','dd/mm/yyyy')"+
				" where ic_linea_credito = "+folio;
		}//fin igual A
		else {
			qrySentencia =  
				" update com_linea_credito "+
				" set fn_monto_autorizado = "+ total+
				" ,fn_monto_autorizado_total = "+ total+
				" ,fn_saldo_linea = "+ total+
				" ,fn_saldo_linea_total = "+ total+
				" ,df_vencimiento_adicional = to_date('"+fechaVto+"','dd/mm/yyyy')"+
				" ,df_vencimiento = to_date('"+fechaVto+"','dd/mm/yyyy')"+
				" ,ic_estatus_linea = "+ estatus+
				" ,ic_plazo = "+ plazo +
				" ,cg_causa_rechazo = '"+ causa +"'"+
				" ,cg_numero_cuenta = '"+ cuenta +"'"+
				" ,cg_cuenta_clabe = '"+cuenta_clabe+"'"+
				" ,fn_monto_solicitado = "+ monto +
				" ,ic_financiera = "+ banco+
				" ,fn_linea_automatica = "+auto +
				" ,df_autorizacion = to_date('"+fechaAuto+"','dd/mm/yyyy')"+
				" ,df_autorizacion_if = sysdate "+
				" ,ig_numero_solic_troya = "+solicTroya+
				" ,ic_tabla_amort = "+tablaAmort;
				
			if(ic_producto.equals("2"))
				qrySentencia += 
					" ,fn_linea_opcional = "+opcion+
					" ,fn_linea_individual = "+ind;
			else
				qrySentencia +=	
					" ,ic_moneda = "+ ic_moneda ;
					
			if("E".equals(claveLineaTipo)) {
				qrySentencia +=	
					" ,cg_linea_tipo = 'E'"+
					" ,ig_numero_max_amort_exp = "+numMaxAmort;
			}
			
			qrySentencia +=	
				" ,df_limite_disposicion = to_date('"+fechaLim+"','dd/mm/yyyy')"+
				" ,df_limite_disposicion_adic = to_date('"+fechaLim+"','dd/mm/yyyy')"+
				" where ic_linea_credito = "+folio;

		}//fin else
		System.out.println("AutorizacionIF["+qrySentencia+"]");
		
		try {
			con.ejecutaSQL(qrySentencia);
		} catch (SQLException sqle) {
			throw new NafinException("ANTI0020");
		}

		try {
			vecFilas = getNafinElec_Prod(folio, con);
			if(vecFilas.size()>0) {
				slnafinElec	= (String)vecFilas.get(0);
				
				System.out.println("slnafinElec["+slnafinElec+"] slproducto["+ic_producto+"]");
				if(!cuenta.equals(""))
					sCta  = ovinsertarCuenta(slnafinElec, ic_producto, ic_moneda, "1", cuenta, iNoUsuario, "1", con);
				//							sCta  = objDispersion.ovinsertarCuenta(slnafinElec, ic_producto, ic_moneda, "1", cuenta, iNoUsuario, "1", con);
				if(!cuenta_clabe.equals(""))
					sCta1 = ovinsertarCuenta(slnafinElec, ic_producto, ic_moneda, "1", cuenta_clabe, iNoUsuario, "40", con);
				//						    sCta1 = objDispersion.ovinsertarCuenta(slnafinElec, ic_producto, ic_moneda, "1", cuenta_clabe, iNoUsuario, "40", con);
				// si el valor de la cuenta speua o de la cuenta clabe cabio
				// cambia el valor del campo cs_aceptacion_pyme a 'C'
				cmpCuentas(cuenta, cuenta_clabe, banco, folio, estatus, con);
			}

		} catch (NafinException ne) {
			throw ne;
		}

	} catch (NafinException ne) {
		resultado = false;
		throw ne;
    } catch(Exception e) {
      System.out.println("LineaCredito.AutorizacionIF Exception"+e);
      resultado = false;
      throw new NafinException("SIST001");
	}
}         //fin AutorizacionIF

	/*********************************************************************************************
	*	ArrayList getComboPymes()
    * 	Agregado 02/08/2004	por HDG
	*********************************************************************************************/
    public ArrayList getComboPymes(String sNoIf, String sNoEPO, String sNoMoneda,
    						  		int iNoProducto) throws NafinException {
	String sQuery = "";
	ResultSet rsResultado = null;
	AccesoDB con = new AccesoDB();
	ArrayList alRegistro = null;
	ArrayList alDatos = new ArrayList();
	boolean bOk = false;
        try{
        	con.conexionDB();

		   	sQuery =" SELECT distinct P.ic_pyme, P.cg_razon_social "   +
					" FROM comcat_pyme P, comrel_pyme_epo PE, "   +
					" comrel_pyme_epo_x_producto PEP, com_linea_credito LC "   +
					" WHERE PE.ic_pyme = P.ic_pyme "   +
					" AND PE.ic_pyme = PEP.ic_pyme "   +
					" AND PE.ic_epo = PEP.ic_epo "   +
					" AND LC.ic_pyme = P.ic_pyme "   +
					" AND PE.ic_epo IN ("+sNoEPO+")"   +
					" AND PE.cs_habilitado = 'S' "   +
					" AND P.cs_habilitado = 'S' "   +
					" AND PEP.ic_producto_nafin = "   +iNoProducto+
					" AND LC.ic_producto_nafin = "   +iNoProducto+
					" AND LC.ic_if = "+sNoIf+
					" AND TRUNC(LC.df_vencimiento_adicional) > TRUNC(SYSDATE) "   +
					" AND LC.cg_tipo_solicitud IN ('I', 'R') "   +
					" AND LC.ic_estatus_linea IN (3, 5) "   +
					" AND LC.ic_moneda = "+sNoMoneda;
			System.out.println(sQuery);
			try {
				rsResultado = con.queryDB(sQuery);
				while (rsResultado.next()) {
					alRegistro = new ArrayList();
					bOk = alRegistro.add(rsResultado.getString(1));
					bOk = alRegistro.add(rsResultado.getString(2));
					bOk = alDatos.add(alRegistro);
				}
				rsResultado.close();
				con.cierraStatement();
			} catch (SQLException sqle) {
				throw new NafinException("ANTI0035");
			}

		} catch (Exception e) {
			System.out.println("Error en getComboPymes: "+e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
    return alDatos;
	}
	
    public ArrayList getComboEpos(String sNoIf, int iNoProducto) throws NafinException {
        try{
        	return getComboEpos(sNoIf, iNoProducto,"");
		} catch (Exception e) {
			System.out.println("Error en getComboEpos: "+e.getMessage());
			throw new NafinException("SIST0001");
		}
	}


	/*********************************************************************************************
	*	ArrayList getComboEpos()
    * 	Agregado 06/08/2004	por HDG
	*********************************************************************************************/
    public ArrayList getComboEpos(String sNoIf, int iNoProducto,String ic_modalidad) throws NafinException {
	String sQuery = "";
	String condicion = "";
	ResultSet rsResultado = null;
	AccesoDB con = new AccesoDB();
	ArrayList alRegistro = null;
	ArrayList alDatos = new ArrayList();
	boolean bOk = false;
        try{
        	con.conexionDB();
        	if("ANTIC".equals(ic_modalidad))        	
        		condicion += " and PRE.ic_modalidad = 1";
        	else if("PRELI".equals(ic_modalidad))        	
        		condicion += " and PRE.ic_modalidad = 2";

		   	sQuery =" select distinct E.ic_epo, E.cg_razon_social"   +
					" from comcat_epo E, comrel_if_epo IE, "+
					" comrel_if_epo_x_producto IEP, comrel_producto_epo PRE "   +
					" where IE.ic_epo = E.ic_epo "   +
					" AND IE.ic_if = IEP.ic_if "   +
					" AND IE.ic_epo = IEP.ic_epo"   +
					" AND PRE.ic_epo = E.ic_epo  "   +
					" AND E.cs_habilitado = 'S' "   +
					" AND IEP.cs_habilitado = 'S' "   +
					" AND IEP.ic_producto_nafin = "   +iNoProducto+
					" AND IEP.cs_habilitado = 'S'"   +
					" AND IE.cs_vobo_nafin = 'S'"   +
					" AND IE.ic_if = "   +sNoIf+
					" AND PRE.ic_producto_nafin = "   +iNoProducto+
					condicion+
					" AND PRE.cs_habilitado = 'S'";
			System.out.println(sQuery);
			rsResultado = con.queryDB(sQuery);
			while (rsResultado.next()) {
				alRegistro = new ArrayList();
				bOk = alRegistro.add(rsResultado.getString(1));
				bOk = alRegistro.add(rsResultado.getString(2));
				bOk = alDatos.add(alRegistro);
			}
			rsResultado.close();
			con.cierraStatement();

		} catch (Exception e) {
			System.out.println("Error en getComboEpos: "+e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
    return alDatos;
	}

/*/////////////////////////////////////////////////////////////////////////////////////////*/
/*/////////////////////////////////////////////////////////////////////////////////////////*/

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>IHJ*/

	public int getNumDisposiciones(String ic_linea_credito)
			throws NafinException{
		System.out.println("LineaCredito::getNumDisposiciones(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		int					existe			= 0;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
				" SELECT COUNT (1)"   +
				"   FROM com_pedido_seleccionado"   +
				"  WHERE ic_linea_credito = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_linea_credito));
			rs = ps.executeQuery();
			if(rs.next()){
				existe = rs.getInt(1);
			}
			ps.close();
		} catch(Exception e) { 
			System.out.println("LineaCredito::getNumDisposiciones(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally { 
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB(); 
			System.out.println("LineaCredito::getNumDisposiciones(S)");
		}
		return existe;
	}	

	public List getLineasExp(String ic_linea_credito[]) throws NafinException {
		System.out.println("LineaCredito::getLineasExp(E)");
		List renglones		= new ArrayList();
		try {
			renglones = getLineasExp("", "", "", ic_linea_credito);
		} catch(Exception e) { 
			System.out.println("LineaCredito::getLineasExp(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally { 
			System.out.println("LineaCredito::getLineasExp(S)");
		}
		return renglones;
	}

	public List getLineasExp(String folio, String ic_epo, String ic_pyme, String ic_linea_credito[]) throws NafinException {
		System.out.println("LineaCredito::getLineasExp(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		String				condicion	 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		List				columnas		= null;
		List				renglones		= new ArrayList();
		try {
			con = new AccesoDB();
			con.conexionDB();
			
			if(!"".equals(folio))
				condicion += "    AND (lc.cg_tipo_solicitud || pym.in_numero_sirac || lc.ic_linea_credito) = ?"  ;
			if(!"".equals(ic_epo))
				condicion += "    AND lc.ic_epo = ?"  ;
			if(!"".equals(ic_pyme))
				condicion += "    AND lc.ic_pyme = ?"  ;
			if(ic_linea_credito!=null){
				condicion = "";
				for(int i=0;i<ic_linea_credito.length;i++) {
					if(!"".equals(condicion))
						condicion += ",";
					condicion += "?";
				}
				condicion = "    AND lc.ic_linea_credito in ("+condicion+")"  ;
			}//if(ic_linea_credito!=null)
			
			qrySentencia = 
				" SELECT lc.ic_linea_credito,"   +
				"        lc.cg_tipo_solicitud || pym.in_numero_sirac || lc.ic_linea_credito folio,"   +
				"        epo.cg_razon_social nomepo, pym.cg_razon_social nompyme,"   +
				"        DECODE ("   +
				"           lc.cg_tipo_solicitud,"   +
				"           'I', 'Inicial',"   +
				"           'R', 'Renovacion',"   +
				"           'Ampliacion'"   +
				"        ) AS tipo_solicitud,"   +
				"        TO_CHAR (lc.df_captura, 'dd/mm/yyyy') fecha_captura,"   +
				"        TO_CHAR (lc.df_autorizacion_if, 'dd/mm/yyyy') fecha_auto_if,"   +
				"        TO_CHAR (lc.df_vencimiento, 'dd/mm/yyyy') fecha_venc,"   +
				"        TO_CHAR (lc.df_limite_disposicion, 'dd/mm/yyyy') fecha_lim_disp,"   +
				"        mon.cd_nombre moneda, lc.fn_monto_solicitado, lc.fn_monto_autorizado,"   +
				"        NVL (lc.fn_monto_autorizado - lc.fn_saldo_linea, 0) monto_ejercido,"   +
				"        NVL (lc.fn_saldo_linea, 0) disponible, lc.fn_monto_liberado,"   +
				"        TO_CHAR (SYSDATE, 'dd/mm/yyyy') fecha_liberacion,"   +
				"        TO_CHAR (SYSDATE + cpn.ig_dias_lib_exp, 'dd/mm/yyyy') fecha_lim_lib,"   +
				"        mon.ic_moneda"   +
				"   FROM com_linea_credito lc,"   +
				"        comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        comcat_moneda mon,"   +
				"        comcat_producto_nafin cpn"   +
				"  WHERE lc.ic_pyme = pym.ic_pyme"   +
				"    AND lc.ic_epo = epo.ic_epo"   +
				"    AND lc.ic_moneda = mon.ic_moneda"   +
				"    AND lc.ic_producto_nafin = cpn.ic_producto_nafin"   +
				"    AND lc.df_vencimiento_adicional > TRUNC (SYSDATE)"   +
				"    AND lc.df_limite_disposicion_adic > TRUNC (SYSDATE)"   +
				"    AND lc.ic_producto_nafin = 5"   +
				"    AND lc.ic_estatus_linea = 5"   +
				"    AND lc.cg_linea_tipo = 'E'"  +
				condicion;
			//System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			int cont = 0;
			if(!"".equals(folio)) {
				cont++;ps.setString(cont, folio);
			}
			if(!"".equals(ic_epo)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_epo));
			}
			if(!"".equals(ic_pyme)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_pyme));
			}
			if(ic_linea_credito!=null){
				for(int i=0;i<ic_linea_credito.length;i++) {
					cont++;ps.setInt(cont, Integer.parseInt(ic_linea_credito[i]));
				}
			}//if(ic_linea_credito!=null)
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()){
				String rs_linea_credito		= rs.getString("ic_linea_credito")==null?"":rs.getString("ic_linea_credito");
				String rs_folio				= rs.getString("folio")==null?"":rs.getString("folio");
				String rs_nom_epo			= rs.getString("nomepo")==null?"":rs.getString("nomepo");
				String rs_nom_pyme			= rs.getString("nompyme")==null?"":rs.getString("nompyme");
				String rs_tipo_solicitud	= rs.getString("tipo_solicitud")==null?"":rs.getString("tipo_solicitud");
				String rs_fecha_captura		= rs.getString("fecha_captura")==null?"":rs.getString("fecha_captura");
				String rs_fecha_auto_if		= rs.getString("fecha_auto_if")==null?"":rs.getString("fecha_auto_if");
				String rs_fecha_venc		= rs.getString("fecha_venc")==null?"":rs.getString("fecha_venc");
				String rs_fecha_lim_disp	= rs.getString("fecha_lim_disp")==null?"":rs.getString("fecha_lim_disp");
				String rs_moneda			= rs.getString("moneda")==null?"":rs.getString("moneda");
				String rs_monto_solicitado	= rs.getString("fn_monto_solicitado")==null?"0":rs.getString("fn_monto_solicitado");
				String rs_monto_autorizado	= rs.getString("fn_monto_autorizado")==null?"0":rs.getString("fn_monto_autorizado");
				String rs_monto_ejercido	= rs.getString("monto_ejercido")==null?"0":rs.getString("monto_ejercido");
				String rs_disponible		= rs.getString("disponible")==null?"0":rs.getString("disponible");
				String rs_monto_liberado	= rs.getString("fn_monto_liberado")==null?"0":rs.getString("fn_monto_liberado");
				String rs_fecha_liberacion	= rs.getString("fecha_liberacion")==null?"":rs.getString("fecha_liberacion");
				String rs_fecha_lim_lib		= rs.getString("fecha_lim_lib")==null?"":rs.getString("fecha_lim_lib");
				String rs_ic_moneda			= rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
				
				columnas = new ArrayList();
				columnas.add(rs_linea_credito);
				columnas.add(rs_folio);
				columnas.add(rs_nom_epo);
				columnas.add(rs_nom_pyme);
				columnas.add(rs_tipo_solicitud);
				columnas.add(rs_fecha_captura);
				columnas.add(rs_fecha_auto_if);
				columnas.add(rs_fecha_venc);
				columnas.add(rs_fecha_lim_disp);
				columnas.add(rs_moneda);
				columnas.add(rs_monto_solicitado);
				columnas.add(rs_monto_autorizado);
				columnas.add(rs_monto_ejercido);
				columnas.add(rs_disponible);
				columnas.add(rs_monto_liberado);
				columnas.add(rs_fecha_liberacion);
				columnas.add(rs_fecha_lim_lib);
				columnas.add(rs_ic_moneda);
				renglones.add(columnas);
			}
			rs.close();ps.close();
		} catch(Exception e) { 
			System.out.println("LineaCredito::getLineasExp(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally { 
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB(); 
			System.out.println("LineaCredito::getLineasExp(S)");
		}
		return renglones;
	}

	public Acuse setLiberacion(String ic_linea_credito[], String fn_monto_liberar[], String df_liberacion[], String df_lim_liberacion[], String ic_usuario, String cg_recibo_electronico) throws NafinException {
		System.out.println("LineaCredito::setLiberacion(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		Acuse 				acuse 			= new Acuse(3, "5");
		boolean				bOk				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			String cc_acuse = "C"+acuse.toString();
			qrySentencia = 
				" INSERT INTO com_acuse3"   +
				"             (cc_acuse, df_fecha_hora, ic_usuario, cg_recibo_electronico, ic_producto_nafin)"   +
				"      VALUES (?, SYSDATE, ?, ?, 5)"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, cc_acuse);
			ps.setString(2, ic_usuario);
			ps.setString(3, cg_recibo_electronico);
			ps.executeUpdate();
			ps.close();
			
			for(int i=0;i<ic_linea_credito.length;i++) {
				qrySentencia = 
					" INSERT INTO com_liberacion"   +
					"             (ic_liberacion, ic_linea_credito, fg_monto_liberar, df_liberacion,"   +
					"              df_lim_liberacion, ic_estatus_liberacion, cc_acuse)"   +
					"    SELECT NVL (MAX (ic_liberacion), 0) + 1, ?, ?,"   +
					"           TO_DATE(?, 'dd/mm/yyyy'), TO_DATE(?, 'dd/mm/yyyy'), 12, ?"   +
					"      FROM com_liberacion"   +
					"     WHERE ic_linea_credito = ?"  ;
				//System.out.println("qrySentencia: "+qrySentencia);
				ps = con.queryPrecompilado(qrySentencia);
				ps.setLong(1, Long.parseLong(ic_linea_credito[i]));
				ps.setDouble(2, Double.parseDouble(fn_monto_liberar[i]));
				ps.setString(3, df_liberacion[i]);
				ps.setString(4, df_lim_liberacion[i]);
				ps.setString(5, cc_acuse);
				ps.setLong(6, Long.parseLong(ic_linea_credito[i]));
				ps.executeUpdate();
				ps.close();
				
				qrySentencia = 
					" UPDATE com_linea_credito"   +
					"    SET fn_monto_liberado = NVL (fn_monto_liberado, 0) + ?"   +
					"  WHERE ic_linea_credito = ?"  ;
				//System.out.println("qrySentencia: "+qrySentencia);
				ps = con.queryPrecompilado(qrySentencia);
				ps.setDouble(1, Double.parseDouble(fn_monto_liberar[i]));
				ps.setLong(2, Long.parseLong(ic_linea_credito[i]));
				ps.executeUpdate();
				ps.close();
			}//for(int i=0;i<ic_linea_credito.length;i++)
		} catch(Exception e) {
			bOk = false;
			System.out.println("LineaCredito::setLiberacion(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB(); 
			System.out.println("LineaCredito::setLiberacion(S)");
		}
		return acuse;
	}
	
	//AGREGO ESTE METODO PARA EL DETALLE DE LAS LIBERACIONES DE LINEAS CREDICADENAS 19/04/2006 SMJ
	public List getDetLiberacion	(String ic_linea_credito) throws NafinException {
    	String		linea_credito  	  = "";
    	String		cg_razon_social   = "";
    	String		ic_liberacion     = "";
    	String		df_liberacion     = "";
    	String		fg_monto_liberar  = "";
    	String		df_lim_liberacion = "";
    	String		estatus			  = "";
    	String		cs_aplicada		  = "";
    	String		qrySentencia	  = "";
    	ResultSet	rs				  = null;
    	AccesoDB 	con				  = null;
    	List		columnas		  = null;
    	List		renglones		  = new ArrayList();
    	try {
			con = new AccesoDB();
			con.conexionDB();				
			qrySentencia = 	
				" SELECT   lb.ic_linea_credito, lb.ic_liberacion,"   +
				"          TO_CHAR (lb.df_liberacion, 'dd/mm/yyyy') df_liberacion,"   +
				"          lb.fg_monto_liberar,"   +
				"          TO_CHAR (lb.df_lim_liberacion, 'dd/mm/yyyy') df_lim_liberacion,"   +
				"          est.cd_descripcion estatus, lb.cs_aplicada"   +
				"     FROM com_liberacion lb, com_linea_credito lc, comcat_estatus_linea est"   +
				"    WHERE lc.ic_linea_credito = lb.ic_linea_credito"   +
				"      AND lb.ic_estatus_liberacion = est.ic_estatus_linea"   +
				"      AND lc.ic_linea_credito = "+ic_linea_credito +
				" ORDER BY lb.ic_liberacion"  ;
			rs = con.queryDB(qrySentencia);
            System.out.println ("El query del detalle de lineas" + qrySentencia);
			while(rs.next()) {
				linea_credito 	 = rs.getString("IC_LINEA_CREDITO"); 
				ic_liberacion 	 = rs.getString("IC_LIBERACION");	 
				df_liberacion 	 = rs.getString("df_liberacion");	 
				fg_monto_liberar = rs.getString("FG_MONTO_LIBERAR"); 
				df_lim_liberacion= rs.getString("df_lim_liberacion");
				estatus			 = rs.getString("estatus"); 
				cs_aplicada		 = rs.getString("CS_APLICADA"); 
				columnas = new ArrayList();
					columnas.add(linea_credito);
					columnas.add(cg_razon_social);
					columnas.add(ic_liberacion);
					columnas.add(df_liberacion);
					columnas.add(fg_monto_liberar);
					columnas.add(df_lim_liberacion);
					columnas.add(estatus);
					columnas.add(cs_aplicada);
				renglones.add(columnas);
			}
			con.cierraStatement();
      }catch(Exception e){
      		throw new NafinException("SIST0001");
      } finally {
      		if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return renglones;
    }

}
