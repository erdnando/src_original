package com.netro.anticipos;

import com.netro.exception.NafinException;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.VectorTokenizer;

@Stateless(name = "ProcesoSNAnticipoEJB" , mappedName = "ProcesoSNAnticipoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ProcesoSNAnticipoBean implements ProcesoSNAnticipo {

	// Metodos para Anticipos "Sirac-Nafinet".
	
	public String sgetNumaxProcesoSirac() throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsProcSirac="0";
		try{
			lobdConexion.conexionDB();
			String lsQry = "select decode(max(ic_proc_sirac),null,1,max(ic_proc_sirac)+1) as CVEPROC "+
							"from comtmp_sirac";
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next())
				lsProcSirac = lrsQry.getString("CVEPROC");
			else
				throw new NafinException("ANTI0020");
				
			lobdConexion.cierraStatement();
		} catch (NafinException ne) {
			throw ne;						
		} catch(Exception e){
			System.out.println("Exception ocurrida en getNumaxProcesoSirac(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lsProcSirac;
	}

	public Hashtable ohprocesarAnticipos(String esDocumentos, String lsProcSirac) throws NafinException {
    String lsMsgError = "", lsMsgOk = "", lsLinea = "";
  	int liNumLinea = 1, liLineaArchivo = 0;
  	boolean bOk = true;	//verifica individualmente cada registro
  	boolean bTodosOk = true; //revisa que todos los registros sean correctos
  	VectorTokenizer lvtValores = null;
  	Vector lvDatos = null;
	  StringBuffer lsbError = new StringBuffer();
  	StringBuffer lsbCorrecto = new StringBuffer();
  	StringBuffer lsbContArchivo = new StringBuffer();
  	String lsPedido="", lsNumPrestamo="", lsFechaOperacion="", lsTipoMotivo="", lsCausaRechazo="";
  	String lsEstatusAnticipo = "";
  	String lsQry = "", lsColumnas = "", lsValores = "";
  	Vector lovFolio = new Vector();
  	Vector lovNumeroPrestamo = new Vector();
  	Vector lovFecha = new Vector();
  	Vector lovTipo	= new Vector();
  	Vector lovCausa = new Vector();
  	Hashtable lohResultados = new Hashtable();
  	ResultSet lorsBusca = null;
  	AccesoDB lobdConexion = new AccesoDB();
  		try	{
  			lobdConexion.conexionDB();
  			StringTokenizer lstDocto = new StringTokenizer(esDocumentos,"\n");
  			while(lstDocto.hasMoreElements()) {
  				lsLinea = lstDocto.nextToken();
  				lsMsgError = "Error en la linea: "+ liNumLinea + ", ";
  				lsMsgOk = "Linea: "+ liNumLinea ;
	  			bOk = true;
	  			if(lsLinea.equals(""))	//si la linea es vacia nos brincamos a la siguente iteraci�n
	  				continue;
	  			lvtValores = new VectorTokenizer(lsLinea,"|");
	  			lvDatos = lvtValores.getValuesVector();
	             //System.out.println(lvDatos.size());
  				if (lvDatos.size() != 5) {
  					bTodosOk = bOk = false;
  					lsbError.append(lsMsgError +" no coincide con el layout. Por favor verifiquelo.\n\n");
  					liLineaArchivo = liNumLinea;
  					lsbContArchivo.append(lsLinea+"\n");
  				}// if lvDatos
  				else {
  					lsPedido = (lvDatos.get(0)==null)?"":lvDatos.get(0).toString();
  					lsNumPrestamo = (lvDatos.get(1)==null)?"":lvDatos.get(1).toString();
  					lsFechaOperacion = (lvDatos.get(2)==null)?"":lvDatos.get(2).toString();
  					lsTipoMotivo = (lvDatos.get(3)==null)?"":lvDatos.get(3).toString();
  					lsCausaRechazo = (lvDatos.get(4)==null)?"":lvDatos.get(4).toString();
  					if(lsTipoMotivo.equalsIgnoreCase("O"))	// O = Operada.
  						lsEstatusAnticipo = "3";	// El estatus del anticipo en la insersi�n
  					if(lsTipoMotivo.equalsIgnoreCase("R"))	// R = Rechazada.
  						lsEstatusAnticipo = "4";
  					//validamos que si el campo es obligatorio, existan los campos num prestamo y fecha operacion
  					if(lsTipoMotivo.equalsIgnoreCase("O") && (lsPedido.equals("") || lsNumPrestamo.equals("") || lsFechaOperacion.equals("")) ) {
  						bTodosOk = bOk = false;
  						lsbError.append(lsMsgError +" los campos No. Anticipo, No. Prestamo y Fecha Operacion son obligatorios.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					}
	  				if(lsTipoMotivo.equalsIgnoreCase("R") && (lsPedido.equals("") || lsCausaRechazo.equals("")) ) {
	  					bTodosOk = bOk = false;
	  					lsbError.append(lsMsgError +" los campos No. Anticipo y Causa de Rechazo son obligatorios.\n\n");
	  					if(liLineaArchivo != liNumLinea) {
	  						liLineaArchivo = liNumLinea;
	  						lsbContArchivo.append(lsLinea+"\n");
	  					}
	  				}
	  				if(!lsPedido.equals("")) {
		  				lsQry = "select ic_pedido from com_anticipo where ic_pedido = '"+lsPedido+"'";
			  			//System.out.println(lsQry);
			   			lorsBusca = lobdConexion.queryDB(lsQry);
			  			if(! lorsBusca.next()) {
			  				bTodosOk = bOk = false;
			  				lsbError.append(lsMsgError +" no existe el Numero de Anticipo: "+lsPedido+".\n\n");
			  				if(liLineaArchivo != liNumLinea) {
			  					liLineaArchivo = liNumLinea;
			  					lsbContArchivo.append(lsLinea+"\n");
			  				}
				  		}// if lorsBusca
		                  lobdConexion.cierraStatement();
						//buscamos la solicitud que sea En Proceso
						lsQry = "select ic_pedido from com_anticipo where ic_pedido = '"+lsPedido+"' AND ic_estatus_antic = 2";
						//System.out.println(lsQry);
						lorsBusca = lobdConexion.queryDB(lsQry);
						if(! lorsBusca.next()) {
							bTodosOk = bOk = false;
							lsbError.append(lsMsgError +" el Numero de Anticipo: "+lsPedido+" no se encuentra en estatus de En Proceso.\n\n");
							if(liLineaArchivo != liNumLinea) {
								liLineaArchivo = liNumLinea;
								lsbContArchivo.append(lsLinea+"\n");
							}
						}// if lorsBusca
		                lobdConexion.cierraStatement();
					}
					if(lsTipoMotivo.equalsIgnoreCase("O")) {
						if(!lsNumPrestamo.equals(""))	{
							//buscamos que no exista el n�mero de prestamo en la base de datos
							lsQry = "select * from com_anticipo where ig_numero_prestamo = " + lsNumPrestamo;
							//System.out.println(lsQry);
							lorsBusca = lobdConexion.queryDB(lsQry);
							if(lorsBusca.next()) {
								bTodosOk = bOk = false;
								lsbError.append(lsMsgError +" el numero de prestamo: " + lsNumPrestamo + ", ya existe en la base de datos.\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}// if lorsBusca
							lobdConexion.cierraStatement();
							if (! Comunes.esNumero(lsNumPrestamo)) {
					            bTodosOk = bOk = false;
						        lsbError.append(lsMsgError + " el campo Numero de Prestamo no es un numero.\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}// if esNumero
							if (lsNumPrestamo.length() > 8) {
								bTodosOk = bOk = false;
								lsbError.append(lsMsgError + " el campo Numero de Prestamo sobrepasa la longitud permitida.\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}// if lsNumPrestamo.lenght()
						} //if lsNumPrestamo
						
						if(!lsFechaOperacion.equals("")) {
							if (! Comunes.checaFecha(lsFechaOperacion)) {
					            bTodosOk = bOk = false;
						        lsbError.append(lsMsgError + " la fecha de operacion (" + lsFechaOperacion + ") no es valida.\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}
						} //if lsFechaOperacion
					} //lsTipoMotivo.equalsIgnoreCase("O")
					if(!lsPedido.equals("")) {
						lsQry =	"select a.CUENTA tmpPedidos from "+
								"(select count(*) as CUENTA from COMTMP_SIRAC where ic_pedido = "+lsPedido+" and ic_proc_sirac = "+lsProcSirac+") a";
						lorsBusca = lobdConexion.queryDB(lsQry);
						if(lorsBusca.next()) {
							if(lorsBusca.getInt("tmpPedidos")>0) {
								bTodosOk = bOk = false;
								lsbError.append(lsMsgError + " El N�mero de Anticipo: "+lsPedido+" esta repetido en la carga\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}
						}
						lobdConexion.cierraStatement();
					}
					if(lsTipoMotivo.equalsIgnoreCase("O") && !lsNumPrestamo.equals("")) {
						lsQry =	"select c.CUENTA numPrestamos from "+
								"(select count(*) as CUENTA from COMTMP_SIRAC where ig_numero_prestamo = "+lsNumPrestamo+" and ic_proc_sirac = "+lsProcSirac+") c";
						lorsBusca = lobdConexion.queryDB(lsQry);
						if(lorsBusca.next()) {
							if(lorsBusca.getInt("numPrestamos")>0) {
								bTodosOk = bOk = false;
								lsbError.append(lsMsgError + " El N�mero de Prestamo: "+lsNumPrestamo+" esta repetido en la carga \n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}
						}
						lobdConexion.cierraStatement();
					}
					if (bOk) {
						lsColumnas = "(ic_proc_sirac,ic_pedido,ic_estatus_antic";
						lsValores = "("+lsProcSirac+","+lsPedido+","+lsEstatusAnticipo;
						if(lsEstatusAnticipo.equals("3")) { //operado
							lsColumnas +=" ,ig_numero_prestamo,df_operacion";
							lsValores +=" ,"+lsNumPrestamo+",TO_DATE('"+lsFechaOperacion+"','DD/MM/YYYY')";
						}
						else if(lsEstatusAnticipo.equals("4")) {	//rechazado
							lsColumnas +=",cg_causa_rechazo";
							lsValores +=",'"+lsCausaRechazo+"'";
							if(!lsFechaOperacion.equals("") && Comunes.checaFecha(lsFechaOperacion)) {
								lsColumnas +=",df_operacion";
								lsValores +=",TO_DATE('"+lsFechaOperacion+"','DD/MM/YYYY')";
							} else
								lsFechaOperacion = "";

							if(!lsNumPrestamo.equals("") && Comunes.esNumero(lsNumPrestamo)) {
								lsColumnas +=",ig_numero_prestamo";
								lsValores +=","+lsNumPrestamo;
							} else
								lsNumPrestamo = "";
						}
						lsColumnas += ")";
						lsValores += ")";

						lsQry = "insert into comtmp_sirac "+lsColumnas+
								"values "+lsValores;
						//System.out.println(lsQry);
						lobdConexion.ejecutaSQL(lsQry);

						lsbCorrecto.append(lsMsgOk+"  Pedido: "+lsPedido+" OK\n\n");

						lovFolio.add(lsPedido);
						lovNumeroPrestamo.add(lsNumPrestamo);
						lovFecha.add(lsFechaOperacion);
						lovTipo.add(lsTipoMotivo);
						lovCausa.add(lsCausaRechazo);
					} //if Ok

				} // else lvDatos
				// Vaciar los vectores
				lvDatos.removeAllElements();
				liNumLinea++;
			}//while lsLinea

		} //try
		catch(Exception e) {
			bTodosOk = false;
			lsbError.append(lsMsgError + " El Layout es incorrecto \n\n");
			if(liLineaArchivo != liNumLinea) {
				liLineaArchivo = liNumLinea;
				lsbContArchivo.append(lsLinea+"\n");
			}
			//System.out.println(lobdConexion.mostrarError());
			System.out.println("Exception ocurrida en hprocesarAnticipos(). "+e);
			throw new NafinException("ANTI0020");
		}
		finally {
			lobdConexion.terminaTransaccion(true);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();

			lohResultados.put("bTodosOk",new Boolean(bTodosOk));
			lohResultados.put("lovFolio",lovFolio);
			lohResultados.put("lovNumeroPrestamo",lovNumeroPrestamo);
			lohResultados.put("lovFecha",lovFecha);
			lohResultados.put("lovTipo",lovTipo);
			lohResultados.put("lovCausa",lovCausa);
			lohResultados.put("lsbError",lsbError.toString());
			lohResultados.put("lsbCorrecto",lsbCorrecto.toString());
			lohResultados.put("lsbContArchivo",lsbContArchivo.toString());
		}
	return lohResultados;
	}


	public boolean bborrarAnticiposSiracTmp(String lsProcSirac) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	boolean lbOk = true;
		try{
			lobdConexion.conexionDB();
			String lsQry = " delete comtmp_sirac "+
							"where ic_proc_sirac = " +lsProcSirac;
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOk = false;
				throw new NafinException("ANTI0020");
			}
			
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e){
			lbOk = false;
			System.out.println("Exception ocurrida en borrar SiracTmp(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(lbOk);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOk;
	}
	
	
	public boolean bactualizarAnticipo(String esEstatusAntic, String esFechaOperacion,
										String esNumPrestamo, String esCausaRechazo,
										String esPedido) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String condicion = "";
	boolean lbOk = true;
		try{
			lobdConexion.conexionDB();
			if(esEstatusAntic.equals("3") && Comunes.checaFecha(esFechaOperacion)) {
				condicion =	",df_operacion = TO_DATE('" + esFechaOperacion + "','DD/MM/YYYY')"+
							",ig_numero_prestamo = " + esNumPrestamo ;
			}
			else if(esEstatusAntic.equals("4")) {
				if(!"".equals(esFechaOperacion) && esFechaOperacion!=null && Comunes.checaFecha(esFechaOperacion))
					condicion += ",df_operacion = TO_DATE('" + esFechaOperacion + "','DD/MM/YYYY')";
				if(!"".equals(esNumPrestamo) && esNumPrestamo!=null && Comunes.esNumero(esNumPrestamo))
					condicion += ",ig_numero_prestamo = " + esNumPrestamo ;
					
				condicion += ",cg_causa_rechazo = '"+esCausaRechazo+"'";
			}
			String lsQry =	" update com_anticipo "+
							" set ic_estatus_antic = "+esEstatusAntic+
							" "+condicion +
							" where ic_pedido = " + esPedido + " and ic_estatus_antic = 2";
			try {
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOk = false;
				throw new NafinException("ANTI0020");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e){
			lbOk = false;
			System.out.println("Exception ocurrida en actualizarAnticipo(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(lbOk);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOk;
	}
	
	
	public Vector ovgetAnticiposProcesados(String lsProcSirac) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector lovAnticipos = new Vector();
	Vector lovRegistro = null;
	boolean bOkActualiza = false;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String esPedido = "", esNumPrestamo = "", esEstatusAntic = "", esFechaOperacion = "", esCausaRechazo = "";
		try{
			lobdConexion.conexionDB();

			String lsQry = 	"select ic_pedido, ig_numero_prestamo, df_operacion, ic_estatus_antic, cg_causa_rechazo"+
							" from comtmp_sirac where ic_proc_sirac = "+lsProcSirac+
							" and ic_pedido is not null "+
							" order by ic_pedido";
			ResultSet lrsDatos = lobdConexion.queryDB(lsQry);
			while(lrsDatos.next()) {
				lovRegistro = new Vector();
				
				esPedido = (lrsDatos.getString("ic_pedido")==null)?"":lrsDatos.getString("ic_pedido");
				esNumPrestamo = (lrsDatos.getString("ig_numero_prestamo")==null)?"":lrsDatos.getString("ig_numero_prestamo");
				esFechaOperacion = (lrsDatos.getString("df_operacion")==null)?"":sdf.format(lrsDatos.getDate("df_operacion"));
				esEstatusAntic = (lrsDatos.getString("ic_estatus_antic")==null)?"":lrsDatos.getString("ic_estatus_antic");
				esCausaRechazo = (lrsDatos.getString("cg_causa_rechazo")==null)?"":lrsDatos.getString("cg_causa_rechazo");
				
				bOkActualiza = bactualizarAnticipo(esEstatusAntic, esFechaOperacion, esNumPrestamo, esCausaRechazo, esPedido);
				
				lovRegistro.add(esPedido);
				lovRegistro.add(esNumPrestamo);
				lovRegistro.add(esFechaOperacion);
				lovRegistro.add(esEstatusAntic);
				lovRegistro.add(esCausaRechazo);
				
				lovAnticipos.addElement(lovRegistro);
			}
			lobdConexion.cierraStatement();

		} catch (NafinException ne) {
			throw ne;			
		} catch (Exception e){
			System.out.println("Exception ocurrida en getAnticiposProcesados(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lovAnticipos;	
	}

/***************************************************/

	public Hashtable ohprocesarCreditos(String esDocumentos, String lsProcSirac) throws NafinException {
    String lsMsgError = "", lsMsgOk = "", lsLinea = "";
  	int liNumLinea = 1, liLineaArchivo = 0;
  	boolean bOk = true;	//verifica individualmente cada registro
  	boolean bTodosOk = true; //revisa que todos los registros sean correctos
  	VectorTokenizer lvtValores = null;
  	Vector lvDatos = null;
	Vector lsbError = null;
  	Vector lsbCorrecto = null;
	Vector Error = new Vector();
  	Vector Correcto = new Vector();

  	StringBuffer lsbContArchivo = new StringBuffer();
  	String lsCredito="", lsNumPrestamo="", lsFechaOperacion="", lsTipoMotivo="", lsCausaRechazo="";
  	String lsEstatusAnticipo = "";
  	String lsQry = "", lsColumnas = "", lsValores = "";
  	Vector lovFolio = new Vector();
  	Vector lovNumeroPrestamo = new Vector();
  	Vector lovFecha = new Vector();
  	Vector lovTipo	= new Vector();
  	Vector lovCausa = new Vector();
  	Hashtable lohResultados = new Hashtable();
  	ResultSet lorsBusca = null;
  	AccesoDB lobdConexion = new AccesoDB();
  		try	{
  			lobdConexion.conexionDB();
  			StringTokenizer lstDocto = new StringTokenizer(esDocumentos,"\n");
  			while(lstDocto.hasMoreElements()) {
  				lsLinea = lstDocto.nextToken();
  				lsMsgError = "Error en la linea: "+ liNumLinea + " ";
  				lsMsgOk = "Linea: "+ liNumLinea ;
	  			bOk = true;
	  			if(lsLinea.equals(""))	//si la linea es vacia nos brincamos a la siguente iteraci�n
	  				continue;
	  			lvtValores = new VectorTokenizer(lsLinea,"|");
	  			lvDatos = lvtValores.getValuesVector();
	             //System.out.println(lvDatos.size());
  				if (lvDatos.size() != 5) {
  					bTodosOk = bOk = false;
            		lsbError = new Vector();
            		lsbError.addElement(lsMsgError);
            		lsbError.addElement("No coincide con el layout.");
            		Error.addElement(lsbError);
  					liLineaArchivo = liNumLinea;
  					lsbContArchivo.append(lsLinea+"\n");
  				}// if lvDatos
  				else {
  					lsCredito 			= (lvDatos.get(0)==null)?"":lvDatos.get(0).toString();
  					lsNumPrestamo 		= (lvDatos.get(1)==null)?"":lvDatos.get(1).toString();
  					lsFechaOperacion 	= (lvDatos.get(2)==null)?"":lvDatos.get(2).toString();
  					lsTipoMotivo 		= (lvDatos.get(3)==null)?"":lvDatos.get(3).toString();
  					lsCausaRechazo 		= (lvDatos.get(4)==null)?"":lvDatos.get(4).toString();
  					if(lsTipoMotivo.equalsIgnoreCase("O"))	// O = Operada.
  						lsEstatusAnticipo = "3";	// El estatus del anticipo en la insersi�n
  					if(lsTipoMotivo.equalsIgnoreCase("R"))	// R = Rechazada.
  						lsEstatusAnticipo = "4";
  					//validamos que si el campo es obligatorio, existan los campos num prestamo y fecha operacion
  					if(lsTipoMotivo.equalsIgnoreCase("O") && (lsCredito.equals("") || lsNumPrestamo.equals("") || lsFechaOperacion.equals("")) ) {
  						bTodosOk = bOk = false;
              			lsbError = new Vector();
//  						lsbError.addElement(lsMsgError +" los campos No. Credito, No. Prestamo y Fecha Operacion son obligatorios.\n\n");
              			lsbError.addElement(lsMsgError);
              			lsbError.addElement("Los campos No. de Documento Final, No. Prestamo y Fecha Operacion son obligatorios.");
              			Error.addElement(lsbError);
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					}
	  				if(lsTipoMotivo.equalsIgnoreCase("R") && (lsCredito.equals("") || lsCausaRechazo.equals("")) ) {
	  					bTodosOk = bOk = false;
              			lsbError = new Vector();
              			lsbError.addElement(lsMsgError);
              			lsbError.addElement("Los campos No. de Documento Final y Causa de Rechazo son obligatorios.");
              			Error.addElement(lsbError);
	  					if(liLineaArchivo != liNumLinea) {
	  						liLineaArchivo = liNumLinea;
	  						lsbContArchivo.append(lsLinea+"\n");
	  					}
	  				}
	  				if(!lsCredito.equals("")) {
		  				lsQry =
                			"SELECT ic_documento "+
                			"  FROM dis_solicitud "+
                			" WHERE ic_documento = "+lsCredito+" ";
						//System.out.println(lsQry);
			   			lorsBusca = lobdConexion.queryDB(lsQry);
			  			if(! lorsBusca.next()) {
			  				bTodosOk = bOk = false;
                			lsbError = new Vector();
//			  				lsbError.addElement(lsMsgError +" no existe el N�mero de Credito: "+lsCredito+".\n\n");
                			lsbError.addElement(lsCredito);
                			lsbError.addElement("No existe el N�mero de Credito");
                			Error.addElement(lsbError);
			  				if(liLineaArchivo != liNumLinea) {
			  					liLineaArchivo = liNumLinea;
			  					lsbContArchivo.append(lsLinea+"\n");
			  				}
				  		}// if lorsBusca
		                  lobdConexion.cierraStatement();
						//buscamos la solicitud que sea En Proceso
						lsQry =
              				"SELECT ic_documento "+
              				"  FROM dis_solicitud "+
              				" WHERE ic_documento = "+lsCredito+" "+
              				"   AND ic_estatus_solic = 2 ";
						//System.out.println(lsQry);
						lorsBusca = lobdConexion.queryDB(lsQry);
						if(! lorsBusca.next()) {
							bTodosOk = bOk = false;
              			lsbError = new Vector();
						//							lsbError.addElement(lsMsgError +" el Numero de Credito: "+lsCredito+" no se encuentra en estatus de En Proceso.\n\n");
              			lsbError.addElement(lsCredito);
              			lsbError.addElement("No se encuentra en estatus de: En Proceso.");
              			Error.addElement(lsbError);
							if(liLineaArchivo != liNumLinea) {
								liLineaArchivo = liNumLinea;
								lsbContArchivo.append(lsLinea+"\n");
							}
						}// if lorsBusca
		                lobdConexion.cierraStatement();
					}
					if(lsTipoMotivo.equalsIgnoreCase("O")) {
						if(!lsNumPrestamo.equals(""))	{
							//buscamos que no exista el n�mero de prestamo en la base de datos
							lsQry =
                				"SELECT * "+
                				"  FROM dis_solicitud "+
                				" WHERE ig_numero_prestamo = "+lsNumPrestamo+" ";
							//System.out.println(lsQry);
							lorsBusca = lobdConexion.queryDB(lsQry);
							if(lorsBusca.next()) {
								bTodosOk = bOk = false;
                				lsbError = new Vector();
//								lsbError.addElement(lsMsgError +" el numero de prestamo: " + lsNumPrestamo + ", ya existe en la base de datos.\n\n");
				                lsbError.addElement(lsCredito);
                				lsbError.addElement("El numero de prestamo: " + lsNumPrestamo + ", ya existe en la base de datos.");
				                Error.addElement(lsbError);
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}// if lorsBusca
							lobdConexion.cierraStatement();
							if (! Comunes.esNumero(lsNumPrestamo)) {
					            bTodosOk = bOk = false;
                			    lsbError = new Vector();
//						        lsbError.addElement(lsMsgError + " el campo Numero de Prestamo no es un numero.\n\n");
                    			lsbError.addElement(lsCredito);
                    			lsbError.addElement("El campo Numero de Prestamo no es un numero.");
                    			Error.addElement(lsbError);
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}// if esNumero
							if (lsNumPrestamo.length() > 8) {
								bTodosOk = bOk = false;
				                lsbError = new Vector();
//								lsbError.addElement(lsMsgError + " el campo Numero de Prestamo sobrepasa la longitud permitida.\n\n");
				                lsbError.addElement(lsCredito);
				                lsbError.addElement("El campo Numero de Prestamo sobrepasa la longitud permitida.");
				                Error.addElement(lsbError);
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}// if lsNumPrestamo.lenght()
						} //if lsNumPrestamo
						
						if(!lsFechaOperacion.equals("")) {
							if (! Comunes.checaFecha(lsFechaOperacion)) {
					            bTodosOk = bOk = false;
                    			lsbError = new Vector();
//						        lsbError.addElement(lsMsgError + " la fecha de operacion (" + lsFechaOperacion + ") no es valida.\n\n");
			                    lsbError.addElement(lsCredito);
                    			lsbError.addElement("La fecha de operacion (" + lsFechaOperacion + ") no es valida.");
                    			Error.addElement(lsbError);
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}
						} //if lsFechaOperacion
					} //lsTipoMotivo.equalsIgnoreCase("O")
					if(!lsCredito.equals("")) {
						lsQry =
							"SELECT a.cuenta tmpPedidos "+
							"  FROM (SELECT COUNT (*) AS cuenta "+
							"          FROM comtmp_sirac "+
							"         WHERE ic_pedido = "+lsCredito+" "+
							"           AND ic_proc_sirac = "+lsProcSirac+") a ";
//System.out.println(lsQry);
						lorsBusca = lobdConexion.queryDB(lsQry);
						if(lorsBusca.next()) {
							if(lorsBusca.getInt("tmpPedidos")>0) {
								bTodosOk = bOk = false;
                				lsbError = new Vector();
//								lsbError.addElement(lsMsgError + " El N�mero de Credito: "+lsCredito+" esta repetido en la carga\n\n");
                				lsbError.addElement(lsCredito);
                				lsbError.addElement("El N�mero de Credito esta repetido en la carga.");
                				Error.addElement(lsbError);
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}
						}
						lobdConexion.cierraStatement();
					}
					if(lsTipoMotivo.equalsIgnoreCase("O") && !lsNumPrestamo.equals("")) {
						lsQry =
							"SELECT c.cuenta numprestamos "+
				              "  FROM (SELECT COUNT (*) AS cuenta "+
				              "          FROM comtmp_sirac "+
				              "         WHERE ig_numero_prestamo = "+lsNumPrestamo+" "+
				              "           AND ic_proc_sirac = "+lsProcSirac+") c ";
//System.out.println(lsQry);
						lorsBusca = lobdConexion.queryDB(lsQry);
						if(lorsBusca.next()) {
							if(lorsBusca.getInt("numPrestamos")>0) {
								bTodosOk = bOk = false;
                				lsbError = new Vector();
//								lsbError.addElement(lsMsgError + " El N�mero de Prestamo: "+lsNumPrestamo+" esta repetido en la carga \n\n");
				                lsbError.addElement(lsCredito);
				                lsbError.addElement("El N�mero de Prestamo: "+lsNumPrestamo+" esta repetido en la carga (1)");
				                Error.addElement(lsbError);
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}
						}
						lobdConexion.cierraStatement();
					}
					if (bOk) {
						lsColumnas = "(ic_proc_sirac,ic_pedido,ic_estatus_antic";
						lsValores = "("+lsProcSirac+","+lsCredito+","+lsEstatusAnticipo;
						if(lsEstatusAnticipo.equals("3")) { //operado
							lsColumnas +=",ig_numero_prestamo,df_operacion";
							lsValores +=","+lsNumPrestamo+",TO_DATE('"+lsFechaOperacion+"','DD/MM/YYYY')";
						}
						else if(lsEstatusAnticipo.equals("4")) {	//rechazado
							lsColumnas +=",cg_causa_rechazo";
							lsValores +=",'"+lsCausaRechazo+"'";
							if(!lsFechaOperacion.equals("") && Comunes.checaFecha(lsFechaOperacion)) {
								lsColumnas +=",df_operacion";
								lsValores +=",TO_DATE('"+lsFechaOperacion+"','DD/MM/YYYY')";
							} else
								lsFechaOperacion = "";

							if(!lsNumPrestamo.equals("") && Comunes.esNumero(lsNumPrestamo)) {
								lsColumnas +=",ig_numero_prestamo";
								lsValores +=","+lsNumPrestamo;
							} else
								lsNumPrestamo = "";
						}
						lsColumnas += ")";
						lsValores += ")";
						lsQry =
			              "INSERT INTO comtmp_sirac "+
			              "            "+lsColumnas+" "+
			              "     VALUES"+lsValores+" ";
//System.out.println(lsQry);
						lobdConexion.ejecutaSQL(lsQry);
//						lsbCorrecto.append(lsMsgOk+"  Pedido: "+lsPedido+" OK\n\n");
			            lsbCorrecto = new Vector();
			            lsbCorrecto.addElement(lsCredito);
			            lsbCorrecto.addElement(lsNumPrestamo);
			            lsbCorrecto.addElement(lsFechaOperacion);
			            Correcto.addElement(lsbCorrecto);
						lovFolio.add(lsCredito);
						lovNumeroPrestamo.add(lsNumPrestamo);
						lovFecha.add(lsFechaOperacion);
						lovTipo.add(lsTipoMotivo);
						lovCausa.add(lsCausaRechazo);
					} //if Ok

				} // else lvDatos
				// Vaciar los vectores
				lvDatos.removeAllElements();
				liNumLinea++;
			}//while lsLinea
		} //try
		catch(Exception e) {
			bTodosOk = false;
      		lsbError = new Vector();
//			lsbError.addElement(lsMsgError + " El Layout es incorrecto \n\n");
      		lsbError.addElement(lsMsgError);
      		lsbError.addElement("El Layout es incorrecto.");
      		Error.addElement(lsbError);
			if(liLineaArchivo != liNumLinea) {
				liLineaArchivo = liNumLinea;
				lsbContArchivo.append(lsLinea+"\n");
			}
			//System.out.println(lobdConexion.mostrarError());
			System.out.println("Exception ocurrida en ohprocesarAnticipos(). "+e);
			throw new NafinException("ANTI0020");
		}
		finally {
			lobdConexion.terminaTransaccion(true);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();

			lohResultados.put("bTodosOk",new Boolean(bTodosOk));
			lohResultados.put("lovFolio",lovFolio);
			lohResultados.put("lovNumeroPrestamo",lovNumeroPrestamo);
			lohResultados.put("lovFecha",lovFecha);
			lohResultados.put("lovTipo",lovTipo);
			lohResultados.put("lovCausa",lovCausa);
			lohResultados.put("lsbError",Error);
			lohResultados.put("lsbCorrecto",Correcto);
			lohResultados.put("lsbContArchivo",lsbContArchivo.toString());
		}
		return lohResultados;
	}

	public boolean bactualizarCredito(	String esEstatusAntic, 
										String esFechaOperacion,
										String esNumPrestamo, 
										String esCausaRechazo,
										String esPedido) throws NafinException {
  			AccesoDB lobdConexion = new AccesoDB();
	  		String condicion = "";
  			boolean lbOk = true;
		try {
			lobdConexion.conexionDB();
			if(esEstatusAntic.equals("3") && Comunes.checaFecha(esFechaOperacion)) {
				condicion =	",df_operacion = TO_DATE('" + esFechaOperacion + "','DD/MM/YYYY')"+
							",ig_numero_prestamo = " + esNumPrestamo ;
			}
			else if(esEstatusAntic.equals("4")) {
				if(!"".equals(esFechaOperacion) && esFechaOperacion!=null && Comunes.checaFecha(esFechaOperacion))
					condicion += ",df_operacion = TO_DATE('" + esFechaOperacion + "','DD/MM/YYYY')";
				if(!"".equals(esNumPrestamo) && esNumPrestamo!=null && Comunes.esNumero(esNumPrestamo))
					condicion += ",ig_numero_prestamo = " + esNumPrestamo ;
				condicion += ",cg_causa_rechazo = '"+esCausaRechazo+"'";
			}
			String lsQry =
        		" update dis_solicitud "+
				" set ic_estatus_solic = "+esEstatusAntic+
				" "+condicion +
				" where ic_documento = " + esPedido + " and ic_estatus_solic = 2";

			String lsQry2 =
		        " update dis_documento "+
				" set ic_estatus_docto = 4 "+
				" where ic_documento = " + esPedido + " ";

			try {
				lobdConexion.ejecutaSQL(lsQry);
				lobdConexion.ejecutaSQL(lsQry2);        
			} catch (SQLException sqle){
				lbOk = false;
				throw new NafinException("ANTI0020");
			}

		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e){
			lbOk = false;
			System.out.println("Exception ocurrida en actualizarCredito(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(lbOk);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOk;
	}

	public Vector ovgetCreditosProcesados(String lsProcSirac) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector lovAnticipos = new Vector();
	Vector lovRegistro = null;
	boolean bOkActualiza = false;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String esPedido = "", esNumPrestamo = "", esEstatusAntic = "", esFechaOperacion = "", esCausaRechazo = "";
		try{
			lobdConexion.conexionDB();

			String lsQry = 	"select ic_pedido, ig_numero_prestamo, df_operacion, ic_estatus_antic, cg_causa_rechazo"+
							" from comtmp_sirac where ic_proc_sirac = "+lsProcSirac+
							" and ic_pedido is not null "+
							" order by ic_pedido";
			ResultSet lrsDatos = lobdConexion.queryDB(lsQry);
			while(lrsDatos.next()) {
				lovRegistro = new Vector();
				
				esPedido = (lrsDatos.getString("ic_pedido")==null)?"":lrsDatos.getString("ic_pedido");
				esNumPrestamo = (lrsDatos.getString("ig_numero_prestamo")==null)?"":lrsDatos.getString("ig_numero_prestamo");
				esFechaOperacion = (lrsDatos.getString("df_operacion")==null)?"":sdf.format(lrsDatos.getDate("df_operacion"));
				esEstatusAntic = (lrsDatos.getString("ic_estatus_antic")==null)?"":lrsDatos.getString("ic_estatus_antic");
				esCausaRechazo = (lrsDatos.getString("cg_causa_rechazo")==null)?"":lrsDatos.getString("cg_causa_rechazo");
				
				bOkActualiza = bactualizarCredito(esEstatusAntic, esFechaOperacion, esNumPrestamo, esCausaRechazo, esPedido);
				
				lovRegistro.add(esPedido);
				lovRegistro.add(esNumPrestamo);
				lovRegistro.add(esFechaOperacion);
				lovRegistro.add(esEstatusAntic);
				lovRegistro.add(esCausaRechazo);
				
				lovAnticipos.addElement(lovRegistro);
			}
			lobdConexion.cierraStatement();

		} catch (NafinException ne) {
			throw ne;			
		} catch (Exception e){
			System.out.println("Exception ocurrida en getCreditosProcesados(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lovAnticipos;	
	}

	public Hashtable ohprocesarAnticipos(String esDocumentos, String lsProcSirac, String lsProducto) throws NafinException {
    String lsMsgError = "", lsMsgOk = "", lsLinea = "";
  	int liNumLinea = 1, liLineaArchivo = 0;
  	boolean bOk = true;	//verifica individualmente cada registro
  	boolean bTodosOk = true; //revisa que todos los registros sean correctos
  	VectorTokenizer lvtValores = null;
  	Vector lvDatos = null;
	StringBuffer lsbError = new StringBuffer();
  	StringBuffer lsbCorrecto = new StringBuffer();
  	StringBuffer lsbContArchivo = new StringBuffer();
  	
  	String lsTabla = "", lsCampoClave = "", lsNumeroPrestamo = "", lsCampoEstatus = "", lsDSN = "";
 
  	int liParametro = 0;
  	// Variables del  producto
	if (lsProducto.equalsIgnoreCase("INVENTARIOS")){
	    lsDSN = "anticipoDS";
	    lsTabla = "inv_solicitud";
	    lsCampoClave = "ig_numero_solic";
	    lsNumeroPrestamo = "ig_numero_prestamo";
	    lsCampoEstatus = "ic_estatus_solic";
	    liParametro=5;
	}
	
  	String lsPedido="", lsNumPrestamo="", lsFechaOperacion="", lsTipoMotivo="", lsCausaRechazo="";
  	String lsEstatusAnticipo = "";
  	
  	String lsQry = "", lsColumnas = "", lsValores = "";
  	Vector lovFolio = new Vector();
  	Vector lovNumeroPrestamo = new Vector();
  	Vector lovFecha = new Vector();
  	Vector lovTipo	= new Vector();
  	Vector lovCausa = new Vector();
  	Hashtable lohResultados = new Hashtable();
  	ResultSet lorsBusca = null;
  	
  	AccesoDB lobdConexion = new AccesoDB();
  		try	{
  			lobdConexion.conexionDB();
  			StringTokenizer lstDocto = new StringTokenizer(esDocumentos,"\n");
  			while(lstDocto.hasMoreElements()) {
  				
				lsLinea = lstDocto.nextToken();
  				lsMsgError = "|Error en la linea: "+ liNumLinea + ", ";
  				lsMsgOk = "Linea: "+ liNumLinea ;
				
	  			bOk = true;
	  			if(lsLinea.equals(""))	//si la linea es vacia nos brincamos a la siguente iteraci�n
	  				continue;
	  			lvtValores = new VectorTokenizer(lsLinea,"|");
	  			lvDatos = lvtValores.getValuesVector();
	             //System.out.println(lvDatos.size());
  				if (lvDatos.size() != 5) {
  					bTodosOk = bOk = false;
  					lsbError.append("|   " + lsMsgError  +" no coincide con el layout. Por favor verifiquelo.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
/*  					liLineaArchivo = liNumLinea;
  					lsbContArchivo.append(lsLinea+"\n");*/
					//continue;
  				}// if lvDatos
  				else {
  					lsPedido = (lvDatos.get(0)==null)?"":lvDatos.get(0).toString();
  					lsNumPrestamo = (lvDatos.get(1)==null)?"":lvDatos.get(1).toString();
  					lsFechaOperacion = (lvDatos.get(2)==null)?"":lvDatos.get(2).toString();
  					lsTipoMotivo = (lvDatos.get(3)==null)?"":lvDatos.get(3).toString();
  					lsCausaRechazo = (lvDatos.get(4)==null)?"":lvDatos.get(4).toString();
					
					if(lsTipoMotivo.equalsIgnoreCase("O"))	// O = Operada.
  						lsEstatusAnticipo = "3";	// El estatus del anticipo en la insersi�n
  					if(lsTipoMotivo.equalsIgnoreCase("R"))	// R = Rechazada.
  						lsEstatusAnticipo = "4";
  					//validamos que si el campo es obligatorio, existan los campos num prestamo y fecha operacion
  					if(lsTipoMotivo.equalsIgnoreCase("O") && (lsPedido.equals("") || lsNumPrestamo.equals("") || lsFechaOperacion.equals("")) ) {
  						bTodosOk = bOk = false;
  						lsbError.append("|" + lsPedido + lsMsgError +" los campos No. Anticipo, No. Prestamo y Fecha Operacion son obligatorios.\n\n");
  						if(liLineaArchivo != liNumLinea) {
  							liLineaArchivo = liNumLinea;
  							lsbContArchivo.append(lsLinea+"\n");
  						}
  					}
	  				if(lsTipoMotivo.equalsIgnoreCase("R") && (lsPedido.equals("") || lsCausaRechazo.equals("")) ) {
	  					bTodosOk = bOk = false;
	  					lsbError.append("|" + lsPedido + lsMsgError +" los campos No. Anticipo y Causa de Rechazo son obligatorios.\n\n");
	  					if(liLineaArchivo != liNumLinea) {
	  						liLineaArchivo = liNumLinea;
	  						lsbContArchivo.append(lsLinea+"\n");
	  					}
	  				}
	  				if(!lsPedido.equals("")) {
		  				lsQry = "select " + lsCampoClave + ", " + lsCampoEstatus + " from inv_solicitud where " + lsCampoClave + " = '"+lsPedido+"'";

			  			System.out.println("Query 1 "+lsQry);
			   			lorsBusca = lobdConexion.queryDB(lsQry);
			  			
				      	if(!lorsBusca.next()) {
				      		bTodosOk = bOk = false;
			  				lsbError.append("|" + lsPedido + lsMsgError +" no existe el Numero de Solicitud: "+lsPedido+".\n\n");
			  				if(liLineaArchivo != liNumLinea) {
			  					liLineaArchivo = liNumLinea;
			  					lsbContArchivo.append(lsLinea+"\n");
			  				}
				  		} else {// if lorsBusca
				  		
					  		if (!"2".equals(lorsBusca.getString("ic_estatus_solic"))){
					    		bTodosOk = bOk = false;
								lsbError.append("|" + lsPedido + lsMsgError +" el Numero de Solicitud: "+lsPedido+" no se encuentra en estatus de En Proceso.\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
					  			}
				  			}
				  		}
		                lobdConexion.cierraStatement();
					}

					if(lsTipoMotivo.equalsIgnoreCase("O")) {
						if(!lsNumPrestamo.equals(""))	{
							//buscamos que no exista el n�mero de prestamo en la base de datos
							lsQry = "select * from " + lsTabla + " where " + lsNumeroPrestamo + " = " + lsNumPrestamo;
							System.out.println("Query 3 "+lsQry);
							lorsBusca = lobdConexion.queryDB(lsQry);
							if(lorsBusca.next()) {
								bTodosOk = bOk = false;
								lsbError.append("|" + lsPedido + lsMsgError +" el numero de prestamo: " + lsNumPrestamo + ", ya existe en la base de datos.\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}// if lorsBusca
							lobdConexion.cierraStatement();
							if (! Comunes.esNumero(lsNumPrestamo)) {
					            bTodosOk = bOk = false;
						        lsbError.append("|" + lsPedido + lsMsgError + " el campo Numero de Prestamo no es un numero.\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}// if esNumero
							if (lsNumPrestamo.length() > 8) {
								bTodosOk = bOk = false;
								lsbError.append("|" + lsPedido + lsMsgError + " el campo Numero de Prestamo sobrepasa la longitud permitida.\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}// if lsNumPrestamo.lenght()
						} //if lsNumPrestamo
						
						if(!lsFechaOperacion.equals("")) {
							if (! Comunes.checaFecha(lsFechaOperacion)) {
					            bTodosOk = bOk = false;
						        lsbError.append("|" + lsPedido + lsMsgError + " la fecha de operacion (" + lsFechaOperacion + ") no es valida.\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}
						} //if lsFechaOperacion
					} //lsTipoMotivo.equalsIgnoreCase("O")
					if(!lsPedido.equals("")) {
						lsQry =	"select a.CUENTA tmpPedidos from "+
								"(select count(*) as CUENTA from COMTMP_SIRAC where ic_pedido = "+lsPedido+" and ic_proc_sirac = "+lsProcSirac+") a";
						System.out.println("Query 4 "+lsQry);
						lorsBusca = lobdConexion.queryDB(lsQry);
						if(lorsBusca.next()) {
							if(lorsBusca.getInt("tmpPedidos")>0) {
								bTodosOk = bOk = false;
								lsbError.append("|" + lsPedido + lsMsgError + " El N�mero de Anticipo: "+lsPedido+" esta repetido en la carga\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}
						}
						lobdConexion.cierraStatement();
					
					}
					if(lsTipoMotivo.equalsIgnoreCase("O") && !lsNumPrestamo.equals("")) {
						lsQry =	"select c.CUENTA numPrestamos from "+
								"(select count(*) as CUENTA from COMTMP_SIRAC where ig_numero_prestamo = "+lsNumPrestamo+" and ic_proc_sirac = "+lsProcSirac+") c";

						System.out.println("Query 5 "+lsQry);
						lorsBusca = lobdConexion.queryDB(lsQry);
						if(lorsBusca.next()) {
							if(lorsBusca.getInt("numPrestamos")>0) {
								bTodosOk = bOk = false;
								lsbError.append("|" + lsPedido + lsMsgError + " El N�mero de Prestamo: "+lsNumPrestamo+" esta repetido en la carga\n\n");
								if(liLineaArchivo != liNumLinea) {
									liLineaArchivo = liNumLinea;
									lsbContArchivo.append(lsLinea+"\n");
								}
							}
						}
						lobdConexion.cierraStatement();
						System.out.println("Sali Query 5 ");
						
					}
					if (bOk) {
						lsColumnas = "(ic_proc_sirac,ic_pedido,ic_estatus_antic";
						lsValores = "("+lsProcSirac+","+lsPedido+","+lsEstatusAnticipo;
						if(lsEstatusAnticipo.equals("3")) { //operado
							lsColumnas +=" ,ig_numero_prestamo,df_operacion";
							lsValores +=" ,"+lsNumPrestamo+",TO_DATE('"+lsFechaOperacion+"','DD/MM/YYYY')";
						}
						else if(lsEstatusAnticipo.equals("4")) {	//rechazado
							lsColumnas +=",cg_causa_rechazo";
							lsValores +=",'"+lsCausaRechazo+"'";
							if(!lsFechaOperacion.equals("") && Comunes.checaFecha(lsFechaOperacion)) {
								lsColumnas +=",df_operacion";
								lsValores +=",TO_DATE('"+lsFechaOperacion+"','DD/MM/YYYY')";
							} else
								lsFechaOperacion = "";

							if(!lsNumPrestamo.equals("") && Comunes.esNumero(lsNumPrestamo)) {
								lsColumnas +=",ig_numero_prestamo";
								lsValores +=","+lsNumPrestamo;
							} else
								lsNumPrestamo = "";
						}
						lsColumnas += ")";
						lsValores += ")";

						lsQry = "insert into comtmp_sirac "+lsColumnas+
								"values "+lsValores;
						System.out.println("Query 6 "+lsQry);
						lobdConexion.ejecutaSQL(lsQry);

						lsbCorrecto.append(lsMsgOk+"  Pedido: "+lsPedido+" OK|\n\n");

						lovFolio.add(lsPedido);
						lovNumeroPrestamo.add(lsNumPrestamo);
						lovFecha.add(lsFechaOperacion);
						lovTipo.add(lsTipoMotivo);
						lovCausa.add(lsCausaRechazo);
					} //if Ok

				} // else lvDatos
				// Vaciar los vectores
				lvDatos.removeAllElements();
				liNumLinea++;
			}//while lsLinea

		} //try
		catch(Exception e) {
			bTodosOk = false;
			lsbError.append(lsMsgError + " El Layout es incorrecto \n\n");
			if(liLineaArchivo != liNumLinea) {
				liLineaArchivo = liNumLinea;
				lsbContArchivo.append(lsLinea+"\n");
			}
			System.out.println(lobdConexion.mostrarError());
			System.out.println("Exception ocurrida en hprocesarAnticipos(). Sobrecargado "+e);
			throw new NafinException("ANTI0020");
		}
		

		finally {
			lobdConexion.terminaTransaccion(true);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();

			lohResultados.put("bTodosOk",new Boolean(bTodosOk));
			lohResultados.put("lovFolio",lovFolio);
			lohResultados.put("lovNumeroPrestamo",lovNumeroPrestamo);
			lohResultados.put("lovFecha",lovFecha);
			lohResultados.put("lovTipo",lovTipo);
			lohResultados.put("lovCausa",lovCausa);
			lohResultados.put("lsbError",lsbError.toString());
			lohResultados.put("lsbCorrecto",lsbCorrecto.toString());
			lohResultados.put("lsbContArchivo",lsbContArchivo.toString());
		}
	return lohResultados;
	}


//###############################################
// MODIFICADO 07/03/2003		--CARP
//###############################################
	public boolean bactualizarAnticipo(String esEstatusAntic, String esFechaOperacion,
										String esNumPrestamo, String esCausaRechazo,
										String esPedido, String lsProducto) throws NafinException {
	
		String lsTabla = "";
  	String lsCampoClave = "";
  	String lsCampoFechaOperacion = "";
  	String lsCampoNumeroPrestamo = ""; 
  	String lsCampoEstatus = ""; 
  	String lsCampoCausaRechazo = "";
  	String lsDSN = "";
  	// Variables del  producto
	if (lsProducto.equalsIgnoreCase("INVENTARIOS")){
	    lsDSN="anticipoDS";
	    lsTabla = "inv_solicitud";
	    lsCampoClave = "ig_numero_solic";
	    lsCampoFechaOperacion = "df_operacion";
	    lsCampoNumeroPrestamo = "ig_numero_prestamo";
	    lsCampoEstatus = "ic_estatus_solic";
		lsCampoCausaRechazo = "cg_causa_rechazo";    
	    
	}
	
	AccesoDB lobdConexion = new AccesoDB();
	String condicion = "";
	boolean lbOk = true;

		try{
			lobdConexion.conexionDB();
			if(esEstatusAntic.equals("3") && Comunes.checaFecha(esFechaOperacion)) {
				condicion =	"," + lsCampoFechaOperacion + " = TO_DATE('" + esFechaOperacion + "','DD/MM/YYYY')"+
							"," + lsCampoNumeroPrestamo + " = " + esNumPrestamo ;
			}
			else if(esEstatusAntic.equals("4")) {
				if(!"".equals(esFechaOperacion) && esFechaOperacion!=null && Comunes.checaFecha(esFechaOperacion))
					condicion += "," + lsCampoFechaOperacion + " = TO_DATE('" + esFechaOperacion + "','DD/MM/YYYY')";
				if(!"".equals(esNumPrestamo) && esNumPrestamo!=null && Comunes.esNumero(esNumPrestamo))
					condicion += "," + lsCampoNumeroPrestamo + " = " + esNumPrestamo;
				condicion += "," + lsCampoCausaRechazo + " = '"+esCausaRechazo+"'";
			}
			String lsQry =	" update " + lsTabla + " "+
							" set " + lsCampoEstatus + " = "+esEstatusAntic+
							" "+condicion +
							" where TO_NUMBER(" + lsCampoClave + ") = '" + esPedido + "' and " + lsCampoEstatus + " = 2";
			try {
				
				System.out.println("bactualizarAnticipo::lsQry"+lsQry);
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOk = false;
				throw new NafinException("ANTI0020");
			}

//###############################################
// AGREGADO 07/03/2003		--CARP
//
  			if ( esPrimerPiso (esPedido, esNumPrestamo, lobdConexion) && esEstatusAntic.equals("4"))
            	actualizaEstatusDisp(esPedido, esNumPrestamo, lobdConexion);
//###############################################
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e){
			lbOk = false;
			System.out.println("Exception ocurrida en actualizarAnticipo(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			lobdConexion.terminaTransaccion(lbOk);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOk;
	}
	
	public Vector ovgetAnticiposProcesados(String lsProcSirac, String lsProducto) throws NafinException {
	
	String lsDSN = "";

  	// Variables del  producto
	if (lsProducto.equalsIgnoreCase("INVENTARIOS")){
	    lsDSN = "anticipoDS";
	}	
	
	AccesoDB lobdConexion = new AccesoDB();
	Vector lovAnticipos = new Vector();
	Vector lovRegistro = null;
	boolean bOkActualiza = false;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	String esPedido = "", esNumPrestamo = "", esEstatusAntic = "", esFechaOperacion = "", esCausaRechazo = "";


		try{
			lobdConexion.conexionDB();

			String lsQry = 	"select to_char(ic_pedido,'09999999999') as ic_pedido, ig_numero_prestamo, df_operacion, ic_estatus_antic, cg_causa_rechazo"+
							" from comtmp_sirac where ic_proc_sirac = "+lsProcSirac+
							" and ic_pedido is not null "+
							" order by ic_pedido";
			
			System.out.println("------------------------------------------------");
			System.out.println("lsQry"+lsQry);
			
			ResultSet lrsDatos = lobdConexion.queryDB(lsQry);
			while(lrsDatos.next()) {
				lovRegistro = new Vector();
				
				esPedido = (lrsDatos.getString("ic_pedido")==null)?"":lrsDatos.getString("ic_pedido");
				esNumPrestamo = (lrsDatos.getString("ig_numero_prestamo")==null)?"":lrsDatos.getString("ig_numero_prestamo");
				esFechaOperacion = (lrsDatos.getString("df_operacion")==null)?"":sdf.format(lrsDatos.getDate("df_operacion"));
				esEstatusAntic = (lrsDatos.getString("ic_estatus_antic")==null)?"":lrsDatos.getString("ic_estatus_antic");
				esCausaRechazo = (lrsDatos.getString("cg_causa_rechazo")==null)?"":lrsDatos.getString("cg_causa_rechazo");
				
				System.out.println("esPedido"+esPedido);
				System.out.println("esNumPrestamo"+esNumPrestamo);
				System.out.println("esFechaOperacion"+esFechaOperacion);
				System.out.println("esEstatusAntic"+esEstatusAntic);
				System.out.println("esCausaRechazo"+esCausaRechazo);
				
				
				bOkActualiza = bactualizarAnticipo(esEstatusAntic, esFechaOperacion, esNumPrestamo, esCausaRechazo, esPedido, "Inventarios");
				
				lovRegistro.add(esPedido);
				lovRegistro.add(esNumPrestamo);
				lovRegistro.add(esFechaOperacion);
				lovRegistro.add(esEstatusAntic);
				lovRegistro.add(esCausaRechazo);
				
				lovAnticipos.addElement(lovRegistro);
			}
			lobdConexion.cierraStatement();

		} catch (NafinException ne) {
			throw ne;			
		} catch (Exception e){
			System.out.println("Exception ocurrida en getAnticiposProcesados(). "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lovAnticipos;	
	}

//###############################################
// AGREGADO 07/03/2003		--CARP
//###############################################
	public boolean esPrimerPiso (String esPedido, String esNumPrestamo, AccesoDB lobdConexion)
    {
    	boolean lbPrimerPiso = false;
        
    	String lsQuery = " SELECT ig_tipo_piso"   +
					" FROM inv_solicitud s, inv_disposicion d, "   +
					" 	 com_linea_credito lc, comcat_if i"   +
					" WHERE s.cc_disposicion = d.cc_disposicion"   +
					" AND d.ic_linea_credito = lc.ic_linea_credito"   +
					" AND lc.ic_if = i.ic_if  "   +
					" AND ig_numero_solic = '"+ esPedido +"'"   +
					" AND ig_numero_prestamo = '"+ esNumPrestamo +"'"  ;

		try {
	        ResultSet lorsDatos = lobdConexion.queryDB(lsQuery);

    	    if (lorsDatos.next())
        		lbPrimerPiso = lorsDatos.getInt(1) == 1 ? true: false;
        } catch (SQLException error){
         	System.out.println("Error en ProcesosSNAnticipoBean::esPrimerPiso() "+ error.getMessage());
        }
        return lbPrimerPiso;
    }

//###############################################
// AGREGADO 07/03/2003		--CARP
//###############################################
	public void actualizaEstatusDisp (String esPedido, String esNumPrestamo, AccesoDB eobdConexion)
    {
		StringBuffer lsQuery = new StringBuffer();
		String lsCveDisposicion = "";
        
		try {
        	lsQuery.append(" SELECT cc_disposicion");
        	lsQuery.append(" FROM inv_solicitud");
        	lsQuery.append(" WHERE ig_numero_solic = '"+ esPedido +"'");
        	lsQuery.append(" AND ig_numero_prestamo = '"+ esNumPrestamo +"'");

	        ResultSet lorsDato = eobdConexion.queryDB(lsQuery.toString());

    	    if (lorsDato.next())
        		lsCveDisposicion = lorsDato.getString(1);

            lorsDato.close();
            eobdConexion.cierraStatement();
            
            lsQuery.delete(0, lsQuery.length());
        	lsQuery.append(" UPDATE inv_disposicion");
        	lsQuery.append(" SET ic_estatus_disposicion = 2");
            lsQuery.append(" WHERE cc_disposicion = '"+ lsCveDisposicion +"'" );

            eobdConexion.ejecutaSQL(lsQuery.toString());
        } catch (SQLException error){
         	System.out.println("Error en ProcesosSNAnticipoBean::actualizaEstatusDisp() "+ error.getMessage());
        }
    }


}// Fin del Bean