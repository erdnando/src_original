package com.netro.anticipos;

import netropology.utilerias.*;
import java.util.*;
import java.math.*;
import java.io.*;
import com.netro.exception.*;
import javax.ejb.Remote;

@Remote
public interface Parametro {


  public abstract Vector consultaEPO(String ic_epo)
   throws NafinException;

	public abstract String getMsgError();


	public abstract boolean guardaAnticipoIF(String ic_producto_nafin, String ic_if, String cs_plazo_maximo,
											String fn_aforo, String cs_cobro_n_creditos,
											String cs_cobranza_automatica,String cg_tipo_cobranza,
											String cg_tipo_modalidad, String cs_fecha_lim, String in_meses_adic)
		throws NafinException;

	public Vector consultaNafin()
		throws NafinException;

	public abstract String getDiasMaximos();

  public Vector consultaAnticipoIF(String ic_if)
   throws NafinException;

/*------- Para contratos abiertos AEC -------*/
	public void guardaDatosEPO(String ig_dias_minimo, String ig_dias_maximo, String ig_dias_admin,
								String fn_linea_automatica, String cs_linea_individual, String ic_epo,
								String cs_automatico_susceptible, String cs_sustitucion_pedido,
								String cs_carga_pyme, String cs_porcentaje_anticipo,
								String cs_contratos_abiertos, String fn_porcentaje_vida,
								String ig_dias_inicio_cobro, String fn_porcentaje_cobro_docto,String cs_cobranza_automatica,
								String ig_dias_venc_fecha_pub, String fn_aforo_epo, String in_dias_fecha_entrega_d, String cg_tipo_contrato,
								String cs_cobranza_referencial, String cs_obligatorio, String ig_campo_cob_ref)
   throws NafinException;



   public boolean guardaDatosNafin(String ig_dias_minimo,
     								String ig_dias_maximo,
     								String fn_maximo_linea,
   								String fn_linea_automatica,
   								String fn_aforo,
   								String fn_minimo_financiamiento,
   								String in_dias_fecha_entrega,
   								String ig_dias_admin,
   								String cs_linea_individual,
   								String ig_plazo_maximo,
   								String cs_automatico_susceptible,
   								String cs_sustitucion_pedido,
   								String cs_carga_pyme,
   								String cs_porcentaje_anticipo,
   								String cs_contratos_abiertos,
   								String fn_porcentaje_vida,
   								String ig_dias_inicio_cobro,
   								String fn_porcentaje_cobro_docto,
   								String cs_cobro_n_creditos,
   								String cs_cobranza_automatica,
   								String cg_tipo_cobranza,
   								String cg_tipo_modalidad,
   								String cs_troya,
   								String ig_dias_venc_fecha_pub,
   								String fn_porc_cobro_c,
   								String ig_div_inicio,
   								String cg_tipo_contrato)
   throws NafinException;

   public ArrayList getNombreCamposDinamicos(String claveEpo)
     throws NafinException;

/*------- AEC -------*/
//Inicia Fodea 062-2010
public String guardaParametrosCajones(String ic_epo, String moneda, String noCajones)  throws AppException;
public List getParametrosCajones(String ic_epo, String moneda) throws AppException;
public  List IntermeyEpoRepresentante(String noEpo, String intemediario, String moneda) throws AppException;
public  List FecchaEjecucionProceso() throws AppException;
//Termina Fodea 062-2010

// Fodea 039 - 2011 By JSHD
public String getNombreIF(String claveIF)	throws AppException;

public String getMontoPublicacionVigente(String intermediario, String noEpo, String moneda) throws AppException;
public String getCajonesIguales(String moneda) throws AppException;
}
