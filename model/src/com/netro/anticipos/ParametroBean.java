package com.netro.anticipos;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "ParametroEJB" , mappedName = "ParametroEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ParametroBean implements Parametro {

	private String msgError = "";
	private String strDiasMaximos = "";
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ParametroBean.class);

	private void validaDiasEPO(String ig_dias_minimo,String ig_dias_maximo,AccesoDB con)
   throws NafinException
   {
   System.out.println("ParametroEJB:validaDiasEPO(E)");
   ResultSet rs						= null;
   String		 qrySentencia	= "";
	   try {
			qrySentencia =	"select in_dias_minimo, " +
							"	    in_dias_maximo " +
							"  from comcat_producto_nafin " +
							" where ic_producto_nafin = 2";
			rs = con.queryDB(qrySentencia);
			while (rs.next()){
				if (ig_dias_minimo != null && !ig_dias_minimo.equals("")) {
					if (Double.parseDouble(ig_dias_minimo) < rs.getDouble("in_dias_minimo")) {
						throw new NafinException("ANTI0023");
					}
				}
				if (ig_dias_maximo != null && !ig_dias_maximo.equals("")) {
					if (Double.parseDouble(ig_dias_maximo) > rs.getDouble("in_dias_maximo")) {
						throw new NafinException("ANTI0024");
					}
				}
			}
	   }catch(NafinException ne){
		   System.out.println("ParametroEJB:validaDiasEPO(NafinException) " + ne);
     		throw ne;
	   }catch(Exception e){
		   System.out.println("ParametroEJB:validaDiasEPO(Exception) " + e);
		   throw new NafinException("SIST0001");
	   }finally {
		   System.out.println("ParametroEJB:validaDiasEPO(S)");
	   }

   }

	private boolean validaDiasNafin(String ig_dias_minimo,String ig_dias_maximo,String fn_maximo_linea,String fn_linea_automatica,AccesoDB con)
   throws NafinException
   {
   ResultSet 	rs						= null;
   String		  qrySentencia	= "";
   boolean    resultado = true;
	   try {
				qrySentencia =	"select MIN(ig_dias_minimo), " +
							"       NVL(MAX(ig_dias_maximo),0) " +
							"  from comrel_producto_epo"+
                                                        "  where ic_producto_nafin = 2";
				rs = con.queryDB(qrySentencia);

				while (rs.next()){
					if (!(rs.getString(1) == null)) {
						if (new Double(ig_dias_minimo).doubleValue() > rs.getDouble(1)) {
							msgError =  "Los dias minimos de anticipo carga, no puede ser mayor a " + rs.getString(1);
    		      resultado = false;
						}
					}
					if (new Double(ig_dias_maximo).doubleValue() < rs.getDouble(2)) {
						msgError = "Los dias maximos de anticipo carga, no puede ser menor a " + rs.getString(2);
	          resultado = false;
        	}
					if(resultado) {
						if(Double.parseDouble(fn_maximo_linea)< Double.parseDouble(fn_linea_automatica))
							throw new NafinException("ANTI0025");
					}
				}
     }catch(NafinException ne){
     		resultado = false;
				msgError = ne.getMsgError();
	   }catch(Exception e){
        resultado = false;
     		throw new NafinException("SIST0001");
	   }
   return resultado;
   }



  public String getMsgError() {
  	return msgError;
  }



  public Vector consultaEPO(String ic_epo)
   throws NafinException
   {
		AccesoDB		con 			= null;
		ResultSet		rs				= null;
		String			qrySentencia	= "";
		Vector			vecFilas 		= new Vector();

		try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia =	"select rpe.ig_dias_minimo, " +
							"       rpe.ig_dias_maximo, " +
							"       rpe.ig_dias_admin, " +
							"       rpe.fn_linea_automatica, " +
							"       rpe.cs_linea_individual, " +
							"       cpn.fn_aforo, " +
							"       cpn.fn_minimo_financiamiento, " +
							"       cpn.in_dias_fecha_entrega, " +
							"		rpe.ic_producto_nafin, " +
							"		rpe.cs_sustitucion_pedido, " +
							"		rpe.cs_automatico_susceptible, " +
							"		rpe.cs_carga_pyme, " +
							"		rpe.cs_porcentaje_anticipo, "+
							"		rpe.cs_contratos_abiertos, "+
							"		rpe.fn_porcentaje_vida, "+
							"		rpe.ig_dias_inicio_cobro, "+
							"		rpe.fn_porcentaje_cobro_docto, "+
							"		rpe.cs_cobranza_automatica,"+
							"		rpe.ig_dias_venc_fecha_pub, "+
							"		rpe.fn_aforo as fn_aforo_epo, "+
							"		rpe.in_dias_fecha_entrega as diasEPO, "+
							"		rpe.ic_modalidad, "+
							"		rpe.cs_cobranza_referencial, "+
							"		rpe.ig_campo_cob_ref, "+
							"       NVL(rpe.cg_tipo_contrato, cpn.cg_tipo_contrato) as cg_tipo_contrato "+
							"  from comcat_epo ce, " +
							"       comrel_producto_epo rpe, " +
							"       comcat_producto_nafin cpn " +
							" where ce.ic_epo = " + ic_epo +
							"   and ce.cs_habilitado = 'S' " +
							"   and rpe.ic_epo = ce.ic_epo " +
							"   and rpe.cs_habilitado = 'S' " +
							"	and cpn.ic_producto_nafin = 2 " +
							"   and cpn.ic_producto_nafin = rpe.ic_producto_nafin";

			rs = con.queryDB(qrySentencia);

			int campoRef=0;
			String cobroRef=null;
			while (rs.next()){
/*0*/			vecFilas.addElement((rs.getString("ig_dias_minimo")==null)?"":rs.getString("ig_dias_minimo"));
/*1*/			vecFilas.addElement((rs.getString("ig_dias_maximo")==null)?"":rs.getString("ig_dias_maximo"));
/*2*/			vecFilas.addElement((rs.getString("ig_dias_admin")==null)?"":rs.getString("ig_dias_admin"));
/*3*/			vecFilas.addElement((rs.getString("fn_linea_automatica")==null)?"":rs.getString("fn_linea_automatica"));
/*4*/			vecFilas.addElement((rs.getString("cs_linea_individual")==null)?"":rs.getString("cs_linea_individual"));
/*5*/			vecFilas.addElement((rs.getString("fn_aforo")==null)?"":rs.getString("fn_aforo"));
/*6*/			vecFilas.addElement((rs.getString("fn_minimo_financiamiento")==null)?"":rs.getString("fn_minimo_financiamiento"));
/*7*/			vecFilas.addElement((rs.getString("in_dias_fecha_entrega")==null)?"":rs.getString("in_dias_fecha_entrega"));
/*8*/			vecFilas.addElement((rs.getString("cs_automatico_susceptible")==null)?"":rs.getString("cs_automatico_susceptible"));
/*9*/			vecFilas.addElement((rs.getString("cs_sustitucion_pedido")==null)?"":rs.getString("cs_sustitucion_pedido"));
/*10*/			vecFilas.addElement((rs.getString("cs_carga_pyme")==null)?"":rs.getString("cs_carga_pyme"));
/*11*/			vecFilas.addElement((rs.getString("cs_porcentaje_anticipo")==null)?"":rs.getString("cs_porcentaje_anticipo"));
/*12*/			vecFilas.addElement((rs.getString("cs_contratos_abiertos")==null)?"":rs.getString("cs_contratos_abiertos"));
/*13*/			vecFilas.addElement((rs.getString("fn_porcentaje_vida")==null)?"":rs.getString("fn_porcentaje_vida"));
/*14*/			vecFilas.addElement((rs.getString("ig_dias_inicio_cobro")==null)?"":rs.getString("ig_dias_inicio_cobro"));
/*15*/			vecFilas.addElement((rs.getString("fn_porcentaje_cobro_docto")==null)?"":rs.getString("fn_porcentaje_cobro_docto"));
/*16*/			vecFilas.addElement((rs.getString("cs_cobranza_automatica")==null)?"":rs.getString("cs_cobranza_automatica"));
/*17*/			vecFilas.addElement((rs.getString("ig_dias_venc_fecha_pub")==null)?"":rs.getString("ig_dias_venc_fecha_pub"));
/*18*/			vecFilas.addElement((rs.getString("fn_aforo_epo")==null)?"":rs.getString("fn_aforo_epo"));
/*19*/			vecFilas.addElement((rs.getString("diasEPO")==null)?"":rs.getString("diasEPO"));
/*20*/			vecFilas.addElement((rs.getString("ic_modalidad")==null)?"":rs.getString("ic_modalidad"));
/*21*/			vecFilas.addElement((rs.getString("cg_tipo_contrato")==null)?"":rs.getString("cg_tipo_contrato"));
/*22*/			vecFilas.addElement((rs.getString("cs_cobranza_referencial")==null)?"":rs.getString("cs_cobranza_referencial"));
/*23*/			vecFilas.addElement((rs.getString("ig_campo_cob_ref")==null)?"":rs.getString("ig_campo_cob_ref"));
				
				campoRef=rs.getInt("ig_campo_cob_ref");
				cobroRef=(rs.getString("cs_cobranza_referencial")==null)?"":rs.getString("cs_cobranza_referencial");
			} // fin while
			rs.close();
			con.cierraStatement();

			
			String obligatorio=null;
			String tipo_dato = "";
			String nombre_campo = "";
			String no_campo = "";
			String longitud_campo = "";
			
			if(cobroRef.equals("S")){
				qrySentencia =	"select cs_obligatorio," + 
						"       cg_tipo_dato," +
						" 		cg_nombre_campo," + 
						"		ic_no_campo," + 
						"		ig_longitud " +
						"  from comrel_visor " +
						" where ic_producto_nafin = 1 " +
						"	AND ic_epo = "+ ic_epo +
						"	AND IC_NO_CAMPO = " + campoRef;

				
				
				rs = con.queryDB(qrySentencia);
				
				if(rs.next()){
					obligatorio 	= (rs.getString("cs_obligatorio") == null)?"N":rs.getString("cs_obligatorio");
					tipo_dato 		= (rs.getString("cg_tipo_dato") == null)?"":rs.getString("cg_tipo_dato");
					nombre_campo 	= (rs.getString("cg_nombre_campo") == null)?"":rs.getString("cg_nombre_campo");
					no_campo 		= (rs.getString("ic_no_campo") == null)?"":rs.getString("ic_no_campo");
					longitud_campo 	= (rs.getString("ig_longitud") == null)?"":rs.getString("ig_longitud");
				}
				rs.close();
				con.cierraStatement();
				
				
				
			}else{
				obligatorio="N";
				tipo_dato = "";
			}

/*24*/		vecFilas.addElement(obligatorio);
/*25*/		vecFilas.addElement(tipo_dato);
/*26*/		vecFilas.addElement(nombre_campo);
/*27*/		vecFilas.addElement(no_campo);
/*28*/		vecFilas.addElement(longitud_campo);

		
		}catch(Exception e){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

		return vecFilas;
   }

	public boolean guardaAnticipoIF(String ic_producto_nafin, String ic_if, String cs_plazo_maximo,
									String fn_aforo, String cs_cobro_n_creditos,
									String cs_cobranza_automatica,String cg_tipo_cobranza,
									String cg_tipo_modalidad, String cs_fecha_lim, String in_meses_adic)
										throws NafinException{

		AccesoDB	con			= null;
		String 		strSQL		= "";
		ResultSet	rs			= null;
	    boolean		resultado	= true;

		try {
			con = new AccesoDB();
			con.conexionDB();

			if("".equals(in_meses_adic)) {
				in_meses_adic = "NULL";
			}

			strSQL = "Select * " +
					 "  From comrel_producto_if " +
					 " Where ic_producto_nafin = " + ic_producto_nafin +
					 "	 And ic_if = " + ic_if;

			rs = con.queryDB(strSQL);

			// Revisa si ya existe el registro en comrel_producto_if
			if (!rs.next()){
				strSQL =	"Insert Into comrel_producto_if " +
							"		(ic_producto_nafin" +
							"		 , ic_if " +
							"		 , cs_plazo_maximo "+
							"		 , cs_cobro_n_creditos "+
							"		 , cs_cobranza_automatica "+
							"		 , cg_tipo_cobranza "+
							"		 , cg_tipo_modalidad "+
							"		 , fn_aforo "+
							"		 , cs_fecha_lim "+
							"		 , in_meses_adic) " +
							"Values (" + ic_producto_nafin +
							"	  	 , " + ic_if +
							"		 , '" + cs_plazo_maximo + "' "+
							"		 , '" + cs_cobro_n_creditos + "' "+
							"		 , '" + cs_cobranza_automatica + "' "+
							"		 , '" + cg_tipo_cobranza + "' "+
							"		 , '" + cg_tipo_modalidad + "' "+
							"		 , " + fn_aforo + " "+
							"		 , '" + cs_fecha_lim + "' "+
							"		 , " + in_meses_adic + " )";
			}
			else{
				strSQL =	"Update comrel_producto_if " +
							"   Set cs_plazo_maximo = '" + cs_plazo_maximo + "' " +
							"   , fn_aforo = " + fn_aforo +
							"   , cs_cobro_n_creditos = '" + cs_cobro_n_creditos +"' "+
							"   , cs_cobranza_automatica = '" + cs_cobranza_automatica +"' "+
							"   , cg_tipo_cobranza = '" + cg_tipo_cobranza +"' "+
							"   , cg_tipo_modalidad = '" + cg_tipo_modalidad +"' "+
							"   , cs_fecha_lim = '" + cs_fecha_lim +"' "+
							"   , in_meses_adic = " + in_meses_adic +" "+
							" Where ic_if = " + ic_if +
							"	And ic_producto_nafin = " + ic_producto_nafin;
			}
			rs.close();
			con.cierraStatement();
			//System.out.println(strSQL);
			con.ejecutaSQL(strSQL);

    	}catch(SQLException e){
			resultado = false;
			throw new NafinException("ANTI0020");
    	}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
			    con.cierraConexionDB();
			}
		}
	    return true;
	}

	public Vector consultaAnticipoIF(String pic_if) throws NafinException{

		AccesoDB	con						= null;
		String 		strSQL					= "";
		String		ic_producto_nafin		= "";
		String		ic_if_rel				= "";
		String		cs_plazo_maximo			= "";
		String		fn_aforo				= "";
		String		ic_if					= "";
		String		cg_razon_social			= "";
		String		cambiar					= "";
		String		cs_cobro_n_creditos		= "";
		String		cs_cobranza_automatica	= "";
		String		cg_tipo_cobranza		= "";
		String		cg_tipo_modalidad		= "";

		String		cs_fecha_lim			= "";
		String		in_meses_adic			= "";

		ResultSet	rs						= null;
	    Vector		vecFilas				= new Vector();

		try{
			con = new AccesoDB();
			con.conexionDB();

			strSQL = 	"Select ig_plazo_maximo " +
						"  From comcat_producto_nafin " +
						" Where ic_producto_nafin = 2";

			rs = con.queryDB(strSQL);

			if (rs.next()){
				strDiasMaximos = rs.getString("ig_plazo_maximo");
			}
			rs.close();

			// Busca nuevamente el IF que se selecciono.
			strSQL =	"Select ic_if, " +
						"		cg_razon_social " +
						"  From comcat_if " +
						" Where cs_habilitado = 'S'";

			if (!pic_if.equals("") && !pic_if.equals("Todos"))
				strSQL +=	"	And ic_if = " + pic_if;

			strSQL +=	" Order by ic_if";

			rs = con.queryDB(strSQL);

			while (rs.next()){

				ResultSet	rs2	= null;
				System.out.println("Pasa con VecFilas: " + vecFilas.size());

				ic_if = rs.getString("ic_if");
				cg_razon_social = rs.getString("cg_razon_social");

				ic_producto_nafin = "";
				ic_if_rel = "";
				cs_plazo_maximo = "";

				strSQL =	"Select ic_producto_nafin " +
							"		, ic_if " +
							"		, cs_plazo_maximo " +
							"		, fn_aforo " +
							"		, cs_cobro_n_creditos " +
							"		, cs_cobranza_automatica " +
							"		, cg_tipo_cobranza " +
							"		, cg_tipo_modalidad " +
							"       , cs_fecha_lim "+
							"       , in_meses_adic "+
							"  From comrel_producto_if " +
							"  Where ic_producto_nafin = 2" +
							" 	and ic_if = " + ic_if;

				System.out.println("Query: " + strSQL);

				rs2 = con.queryDB(strSQL);

				if (rs2.next()){

					Vector	vecColumnas	= new Vector();

					ic_producto_nafin 		= rs2.getString("ic_producto_nafin");
					ic_if_rel 				= rs2.getString("ic_if");
					cs_plazo_maximo 		= rs2.getString("cs_plazo_maximo");
					fn_aforo 				= (rs2.getString("fn_aforo")==null)? "" : rs2.getString("fn_aforo");
					cs_cobro_n_creditos 	= (rs2.getString("cs_cobro_n_creditos")==null)?"N":rs2.getString("cs_cobro_n_creditos");
					cs_cobranza_automatica 	= (rs2.getString("cs_cobranza_automatica")==null)?"":rs2.getString("cs_cobranza_automatica");
					cg_tipo_cobranza 		= (rs2.getString("cg_tipo_cobranza")==null)?"":rs2.getString("cg_tipo_cobranza");
					cg_tipo_modalidad 		= (rs2.getString("cg_tipo_modalidad")==null)?"":rs2.getString("cg_tipo_modalidad");

					cs_fecha_lim			= (rs2.getString("cs_fecha_lim")==null)?"":rs2.getString("cs_fecha_lim");
					in_meses_adic			= (rs2.getString("in_meses_adic")==null)?"":rs2.getString("in_meses_adic");

					if (cs_plazo_maximo.equals(""))
						cambiar = "S";
					else
						cambiar = "N";

					ic_producto_nafin = (ic_producto_nafin.equals("")) ? "2" : ic_producto_nafin;
					ic_if_rel = (ic_if_rel.equals("")) ? rs.getString("ic_if") : ic_if_rel;
					cs_plazo_maximo = (cs_plazo_maximo.equals("")) ? "N" : cs_plazo_maximo;

					vecColumnas.addElement(cg_razon_social);
					vecColumnas.addElement(ic_producto_nafin);
					vecColumnas.addElement(ic_if_rel);
					vecColumnas.addElement(cs_plazo_maximo);
					vecColumnas.addElement(cambiar);
					vecColumnas.addElement(fn_aforo);
					vecColumnas.addElement(cs_cobro_n_creditos);
					vecColumnas.addElement(cs_cobranza_automatica);
					vecColumnas.addElement(cg_tipo_cobranza);
					vecColumnas.addElement(cg_tipo_modalidad);

					vecColumnas.addElement(cs_fecha_lim);
					vecColumnas.addElement(in_meses_adic);

					vecFilas.addElement(vecColumnas);

				}
				rs2.close();
				con.cierraStatement();
			}

			rs.close();
    	}catch(Exception e){
    		e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
			    con.cierraConexionDB();
		}
	    return vecFilas;
	}

	public String getDiasMaximos(){
		return strDiasMaximos;
	}

   public Vector consultaNafin()
   throws NafinException
   {
		AccesoDB			con 					= null;
		ResultSet			rs						= null;
		PreparedStatement	ps						= null;
		String			qrySentencia	= "";
    Vector			vecFilas 			= new Vector();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =	"select fn_aforo, " +
						"       fn_minimo_financiamiento, " +
						"		in_dias_fecha_entrega, " +
						"       in_dias_minimo, " +
						"       in_dias_maximo, " +
						"       ig_dias_admin, " +
						"       fn_linea_automatica, " +
						"       NVL(fn_maximo_linea,0) as fn_maximo_linea, " +
						"       cs_linea_individual, " +
						"		ig_plazo_maximo, " +
						"		cs_automatico_susceptible, " +
						"		cs_sustitucion_pedido, " +
						"		cs_carga_pyme, " +
						"		cs_porcentaje_anticipo, "+
						"		cs_contratos_abiertos, "+
						"		fn_porcentaje_vida, "+
						"		ig_dias_inicio_cobro, "+
						"		fn_porcentaje_cobro_docto, "+
						"		cs_cobro_n_creditos, "+
						"		cs_cobranza_automatica, "+
						"		cg_tipo_cobranza, "+
						"		cg_tipo_modalidad "+
						"		,cs_troya "+
						"		,ig_dias_venc_fecha_pub"+
						"		,fn_porc_cobro_c"+
						"		,ig_div_inicio"+
						"		, NVL(cg_tipo_contrato,'A') as cg_tipo_contrato"+
						"  from comcat_producto_nafin " +
						" where ic_producto_nafin = 2";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();

			while (rs.next()){
				vecFilas.addElement((rs.getString("fn_aforo")==null)?"":rs.getString("fn_aforo"));
				vecFilas.addElement((rs.getString("fn_minimo_financiamiento")==null)?"":rs.getString("fn_minimo_financiamiento"));
				vecFilas.addElement((rs.getString("in_dias_fecha_entrega")==null)?"":rs.getString("in_dias_fecha_entrega"));
				vecFilas.addElement((rs.getString("in_dias_minimo")==null)?"":rs.getString("in_dias_minimo"));
				vecFilas.addElement((rs.getString("in_dias_maximo")==null)?"":rs.getString("in_dias_maximo"));
				vecFilas.addElement((rs.getString("ig_dias_admin")==null)?"":rs.getString("ig_dias_admin"));
				vecFilas.addElement((rs.getString("fn_linea_automatica")==null)?"":rs.getString("fn_linea_automatica"));
				vecFilas.addElement((rs.getString("fn_maximo_linea")==null)?"":rs.getString("fn_maximo_linea"));
				vecFilas.addElement((rs.getString("cs_linea_individual")==null)?"":rs.getString("cs_linea_individual"));
				vecFilas.addElement((rs.getString("ig_plazo_maximo")==null)?"":rs.getString("ig_plazo_maximo"));
				vecFilas.addElement((rs.getString("cs_automatico_susceptible")==null)?"":rs.getString("cs_automatico_susceptible"));
				vecFilas.addElement((rs.getString("cs_sustitucion_pedido")==null)?"":rs.getString("cs_sustitucion_pedido"));
				vecFilas.addElement((rs.getString("cs_carga_pyme")==null)?"":rs.getString("cs_carga_pyme"));
				vecFilas.addElement((rs.getString("cs_porcentaje_anticipo")==null)?"":rs.getString("cs_porcentaje_anticipo"));
				vecFilas.addElement((rs.getString("cs_contratos_abiertos")==null)?"":rs.getString("cs_contratos_abiertos"));
				vecFilas.addElement((rs.getString("fn_porcentaje_vida")==null)?"":rs.getString("fn_porcentaje_vida"));
				vecFilas.addElement((rs.getString("ig_dias_inicio_cobro")==null)?"":rs.getString("ig_dias_inicio_cobro"));
				vecFilas.addElement((rs.getString("fn_porcentaje_cobro_docto")==null)?"":rs.getString("fn_porcentaje_cobro_docto"));
				vecFilas.addElement((rs.getString("cs_cobro_n_creditos")==null)?"":rs.getString("cs_cobro_n_creditos"));
				vecFilas.addElement((rs.getString("cs_cobranza_automatica")==null)?"":rs.getString("cs_cobranza_automatica"));
				vecFilas.addElement((rs.getString("cg_tipo_cobranza")==null)?"":rs.getString("cg_tipo_cobranza"));
				vecFilas.addElement((rs.getString("cg_tipo_modalidad")==null)?"":rs.getString("cg_tipo_modalidad"));
				vecFilas.addElement((rs.getString("cs_troya")==null)?"":rs.getString("cs_troya"));
				vecFilas.addElement((rs.getString("ig_dias_venc_fecha_pub")==null)?"":rs.getString("ig_dias_venc_fecha_pub"));
/*24*/			vecFilas.addElement((rs.getString("fn_porc_cobro_c")==null)?"":rs.getString("fn_porc_cobro_c"));
/*25*/			vecFilas.addElement((rs.getString("ig_div_inicio")==null)?"":rs.getString("ig_div_inicio"));
                vecFilas.addElement((rs.getString("cg_tipo_contrato")==null)?"":rs.getString("cg_tipo_contrato"));
		}
		rs.close();
		ps.close();

    }catch(Exception e){
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
		    con.cierraConexionDB();
		}
    return vecFilas;
   }


/*------- Para contratos abiertos AEC -------*/
	public void guardaDatosEPO (String ig_dias_minimo, String ig_dias_maximo, String ig_dias_admin,
								String fn_linea_automatica, String cs_linea_individual, String ic_epo,
								String cs_automatico_susceptible, String cs_sustitucion_pedido,
								String cs_carga_pyme, String cs_porcentaje_anticipo,
								String cs_contratos_abiertos, String fn_porcentaje_vida,
								String ig_dias_inicio_cobro, String fn_porcentaje_cobro_docto,String cs_cobranza_automatica,
								String ig_dias_venc_fecha_pub,String fn_aforo_epo, String in_dias_fecha_entrega_d, String cg_tipo_contrato,
								String cs_cobranza_referencial,String cs_obligatorio,String ig_campo_cob_ref)
   throws NafinException
   {
		AccesoDB		con 			= null;
		String			qrySentencia	= "";
		boolean			resultado		= true;

		try {
			con = new AccesoDB();
			con.conexionDB();

	        validaDiasEPO(ig_dias_minimo,ig_dias_maximo,con);

			if (ig_dias_minimo.equals("")) {
				ig_dias_minimo="null";
			}
	        
	        if (ig_dias_maximo.equals("")) {
				ig_dias_maximo="null";
			}
			
			if (ig_dias_admin.equals("")) {
				ig_dias_admin="null";
			}
			
			if (fn_linea_automatica.equals("")) {
				fn_linea_automatica="null";
			}

			if (ig_dias_venc_fecha_pub.equals("")) {
				ig_dias_venc_fecha_pub="null";
			}
			
			if (ig_dias_inicio_cobro.equals("")) {
				ig_dias_inicio_cobro="null";
			}
			
			if (fn_porcentaje_vida.equals("")) {
				fn_porcentaje_vida="null";
			}
			
			if (fn_porcentaje_cobro_docto.equals("")) {
				fn_porcentaje_cobro_docto="null";
			}
			
			if (cs_cobranza_referencial.equals("")) {
				cs_cobranza_referencial="null";
			}else{
				cs_cobranza_referencial="'" + cs_cobranza_referencial + "'";
			}
			
	        if (cs_obligatorio.equals("")) {
				cs_obligatorio="null";
			}else{
				cs_obligatorio="'" + cs_obligatorio + "'";
			}
	        
       		if (ig_campo_cob_ref.equals("")) {
				ig_campo_cob_ref="null";
			}
	        
	        

			cs_linea_individual = (cs_linea_individual.equals(""))?"null":"'" + cs_linea_individual + "'";
			cs_automatico_susceptible = (cs_automatico_susceptible.equals(""))?"null":"'" + cs_automatico_susceptible + "'";
			cs_sustitucion_pedido = (cs_sustitucion_pedido.equals(""))?"null":"'" + cs_sustitucion_pedido + "'";
			cs_carga_pyme = (cs_carga_pyme.equals(""))?"null":"'" + cs_carga_pyme + "'";
			cs_porcentaje_anticipo = (cs_porcentaje_anticipo.equals(""))?"null":"'" + cs_porcentaje_anticipo + "'";
			cs_contratos_abiertos = (cs_contratos_abiertos.equals(""))?"null":"'" + cs_contratos_abiertos + "'";
			cs_cobranza_automatica =(cs_cobranza_automatica==null)?"null":"'" + cs_cobranza_automatica + "'";

			if (fn_aforo_epo.equals(""))
				fn_aforo_epo="null";
			if (in_dias_fecha_entrega_d.equals(""))
			   in_dias_fecha_entrega_d="null";


			qrySentencia =	"update comrel_producto_epo " +
							"   set ig_dias_minimo = " + ig_dias_minimo +
							"		, ig_dias_maximo = " + ig_dias_maximo +
							"		, fn_linea_automatica = " + fn_linea_automatica +
							"		, cs_linea_individual = " + cs_linea_individual +
							"		, cs_automatico_susceptible = " + cs_automatico_susceptible +
							"		, cs_sustitucion_pedido = " + cs_sustitucion_pedido +
							"		, cs_carga_pyme = " + cs_carga_pyme +
							"		, cs_porcentaje_anticipo = " + cs_porcentaje_anticipo +
							"		, cs_contratos_abiertos = " + cs_contratos_abiertos +
							"		, fn_porcentaje_vida = " + fn_porcentaje_vida +
							"		, ig_dias_inicio_cobro = " + ig_dias_inicio_cobro +
							"		, fn_porcentaje_cobro_docto = " + fn_porcentaje_cobro_docto +
							"		, ig_dias_admin = " + ig_dias_admin +
							"		, cs_cobranza_automatica = "+cs_cobranza_automatica+
							"		, ig_dias_venc_fecha_pub = "+ig_dias_venc_fecha_pub+
							"		, fn_aforo= "+fn_aforo_epo+
							"		, in_dias_fecha_entrega = "+in_dias_fecha_entrega_d+
							"       , cg_tipo_contrato= '"+cg_tipo_contrato+"'"+
							"		, cs_cobranza_referencial = " + cs_cobranza_referencial +
							"		, ig_campo_cob_ref = " + ig_campo_cob_ref +
							" where ic_epo = " + ic_epo+
							" 	AND ic_producto_nafin = 2 ";
			
			
			System.out.println(qrySentencia);

			con.ejecutaSQL(qrySentencia);
			

			qrySentencia =	"update comrel_visor "+
							"	set cs_obligatorio = " + cs_obligatorio + 
							" where ic_producto_nafin = 1" +
							" 	AND ic_epo = " + ic_epo +
							" 	AND ic_no_campo = " + ig_campo_cob_ref;
							if ("S".equals(cs_cobranza_referencial)) {
							qrySentencia +=	"	AND IC_NO_CAMPO = " + ig_campo_cob_ref;
						}
			
			
			
			System.out.println(qrySentencia);

			con.ejecutaSQL(qrySentencia);
			
			
			

		}catch(NafinException ne){
	      	resultado = false;
	      	throw ne;
		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
   }

// Sobrecargado

  public boolean guardaDatosNafin(String ig_dias_minimo,
  					String ig_dias_maximo,
  					String fn_maximo_linea,
					String fn_linea_automatica,
					String fn_aforo,
					String fn_minimo_financiamiento,
					String in_dias_fecha_entrega,
					String ig_dias_admin,
					String cs_linea_individual,
					String ig_plazo_maximo,
					String cs_automatico_susceptible,
					String cs_sustitucion_pedido,
					String cs_carga_pyme,
					String cs_porcentaje_anticipo,
					String cs_contratos_abiertos,
					String fn_porcentaje_vida,
					String ig_dias_inicio_cobro,
					String fn_porcentaje_cobro_docto,
					String cs_cobro_n_creditos,
					String cs_cobranza_automatica,
					String cg_tipo_cobranza,
					String cg_tipo_modalidad,
					String cs_troya,
					String ig_dias_venc_fecha_pub,
					String fn_porc_cobro_c,
					String ig_div_inicio,
					String cg_tipo_contrato)
   throws NafinException
   {
    AccesoDB		con 					= null;
		String			qrySentencia	= "";
    boolean			resultado			= true;
			try {
				con = new AccesoDB();
			  con.conexionDB();
        if(!validaDiasNafin(ig_dias_minimo,ig_dias_maximo,fn_maximo_linea,fn_linea_automatica,con))
						resultado = false;
        if(resultado) {
						qrySentencia =	"update comcat_producto_nafin " +
								"   set fn_aforo = " + fn_aforo + ", " +
								"		fn_minimo_financiamiento = " + fn_minimo_financiamiento + ", " +
								"		in_dias_fecha_entrega = " + in_dias_fecha_entrega + ", " +
								"       in_dias_minimo = " + ig_dias_minimo + ", " +
								"       in_dias_maximo = " + ig_dias_maximo + ", " +
								"       ig_dias_admin = " + ig_dias_admin + ", " +
								"       fn_linea_automatica = " + fn_linea_automatica + ", " +
								"       fn_maximo_linea = " + fn_maximo_linea + ", " +
								"       cs_linea_individual = '" + cs_linea_individual + "', " +
								"		ig_plazo_maximo = " + ig_plazo_maximo + "," +
								"		cs_automatico_susceptible = '" + cs_automatico_susceptible + "', " +
								"		cs_sustitucion_pedido = '" + cs_sustitucion_pedido + "'," +
								"		cs_carga_pyme = '" + cs_carga_pyme + "', " +
								"		cs_porcentaje_anticipo = '" + cs_porcentaje_anticipo + "', " +
								"		cs_contratos_abiertos = '" + cs_contratos_abiertos + "', " +
								"		fn_porcentaje_vida = " + fn_porcentaje_vida + ", " +
								"		ig_dias_inicio_cobro = " + ig_dias_inicio_cobro + ", " +
								"		fn_porcentaje_cobro_docto = " + fn_porcentaje_cobro_docto + ", " +
								"		cs_cobro_n_creditos = '" + cs_cobro_n_creditos + "',"+
								"		cs_cobranza_automatica = '" + cs_cobranza_automatica + "',"+
								"		cg_tipo_cobranza = '" + cg_tipo_cobranza + "',"+
								"		cg_tipo_modalidad = '" + cg_tipo_modalidad + "'"+
								"		,cs_troya = '" + cs_troya + "'"+
								"		,ig_dias_venc_fecha_pub = "+ig_dias_venc_fecha_pub+
								"		,fn_porc_cobro_c = " + fn_porc_cobro_c +
								"		,ig_div_inicio = "+ig_div_inicio +
								"		,cg_tipo_contrato = '" + cg_tipo_contrato + "'"+
								" where ic_producto_nafin = 2";
						con.ejecutaSQL(qrySentencia);
        }
			}catch(NafinException ne){
      	resultado = false;
				msgError = ne.getMsgError();
			}catch(Exception e){
      	resultado = false;
				throw new NafinException("SIST0001");
			}finally{
      	if(con.hayConexionAbierta()){
					con.terminaTransaccion(resultado);
        	con.cierraConexionDB();
        }
      }
    return resultado;
   }
   
	public ArrayList getNombreCamposDinamicos(String claveEpo)
			throws NafinException {
	
		ArrayList campos = new ArrayList();
		Vector nombres= new Vector();
		AccesoDB con = null;
		ResultSet			rs						= null;
		PreparedStatement	ps						= null;
	
		try {   
			con = new AccesoDB();
			con.conexionDB();
	
			String query = "SELECT INITCAP(cg_nombre_campo) as nombreCampoDinamico, " +
					" decode(cg_tipo_dato,'A','Alfanumerico','N','Numerico') as tipodato, ig_longitud " +
					" FROM comrel_visor " +
					" WHERE ic_epo = ? " +
					" 	AND ic_producto_nafin = 2 "+
					" 	AND ic_no_campo BETWEEN 1 AND 5 ORDER BY ic_no_campo";
	
			ps = con.queryPrecompilado(query);
			ps.setInt(1, Integer.parseInt(claveEpo));
			rs = ps.executeQuery();
	
			while (rs.next()) {
				nombres=new Vector();	
				nombres.add(0,(rs.getString("nombreCampoDinamico")==null)?"":rs.getString("nombreCampoDinamico"));
				nombres.add(1,(rs.getString("tipodato")==null)?"":rs.getString("tipodato"));
				nombres.add(2,(rs.getString("ig_longitud")==null)?"":rs.getString("ig_longitud"));
				campos.add(nombres);
			} // while
			rs.close();
			if(ps!=null) ps.close();
		} catch(Exception e) {
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return campos;
	}
	
	
	//Fodea 062-2010
	/**
	 * metodo que gauda y modifica los parametros de rango
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param norango
	 * @param accion
	 * @param noCajones
	 * @param moneda
	 * @param ic_epo
	 */
	public String guardaParametrosCajones(String ic_epo, String moneda, String noCajones) throws AppException{
		log.info("guardaParametrosCajones(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs2 = null;
		PreparedStatement ps2= null;
		List lvarbind2 = new ArrayList();
		String guarda ="N";		
		StringBuffer queryExiste = new StringBuffer();
		StringBuffer queryInsert = new StringBuffer();
		StringBuffer queryUpdate = new StringBuffer();	
		String accion ="";
		boolean			resultado			= true;		
		List Epos = new ArrayList();
		
		try{
			con.conexionDB();
		
		log.debug("ic_epo "+ic_epo);
		log.debug("moneda "+moneda);
		log.debug("noCajones "+noCajones);
	
		//verifica si existe alguna registro ya en la tabla
		queryExiste = new StringBuffer();
		queryExiste.append(" Select  count(*)  as valor  ");
		queryExiste.append(" from COM_RANGOS_PUB_PROVEEDORES r  ");
		queryExiste.append(" where r.ic_epo = ? ");
		queryExiste.append(" and r.ic_moneda = ? ");
		
		// inserta los registros
		queryInsert = new StringBuffer();
		queryInsert.append(" insert into  COM_RANGOS_PUB_PROVEEDORES ");
		queryInsert.append(" (  IC_RANGO, IC_EPO, IC_MONEDA, IG_NUM_CAJONES)");
		queryInsert.append(" values(SEQ_COM_RANGOS_PUB_PROVEEDORES.NEXTVAL, ?,?,?)");
							
		//modifica
		queryUpdate = new StringBuffer();
		queryUpdate.append(" UPDATE COM_RANGOS_PUB_PROVEEDORES ");
		queryUpdate.append(" SET IG_NUM_CAJONES = ? ");
		queryUpdate.append(" WHERE IC_EPO  = ? ");
		queryUpdate.append(" AND IC_MONEDA = ? ");			
								
		
			if (ic_epo.equals("T") ){ //cuando son elegidas todas las Epo			
				log.debug("cuando son elegidas todas las Epos ");
				Epos =	catalogoEpo();
				
				for(int j=0;j<Epos.size();j++){
				
					ic_epo 	= (String) Epos.get(j);
					
					//verifica si existe o no el registro
					lvarbind2 = new ArrayList();
					lvarbind2.add(ic_epo);
					lvarbind2.add(moneda);					
					log.debug("queryExiste "+queryExiste);
					log.debug("lvarbind2 "+lvarbind2);
					ps2= con.queryPrecompilado(queryExiste.toString(),lvarbind2);			
					rs2 = ps2.executeQuery();				
									
					if(rs2.next()){				
						accion = rs2.getString("valor")==null?"":rs2.getString("valor");	
					}
					rs2.close();
					ps2.close();		
							
					if(accion.equals("0")){ 
					
					// se inserta en caso de no existir 	
						  log.debug("queryInsert "+queryInsert);					
							ps = con.queryPrecompilado(queryInsert.toString());
							ps.setString(1, ic_epo);
							ps.setString(2, moneda);
							ps.setString(3, noCajones);
							ps.executeUpdate();
							ps.close();							
							guarda ="G";
					
					}else if(!accion.equals("0") ){
						
							// se Modifica en caso de existir		
							log.debug("queryUpdate "+queryUpdate);	
							ps = con.queryPrecompilado(queryUpdate.toString());
							ps.setString(1, noCajones);
							ps.setString(2, ic_epo);
							ps.setString(3, moneda);				
							ps.executeUpdate();
							ps.close();
						
						 guarda ="M";
					}
					
			  }//termina for
			
		}else if (!ic_epo.equals("T") ) {  // cuando es solo elegida una EPo 
		log.debug("cuando es solo elegida una EPo ");
			//para saber si existe el registro 
						lvarbind2 = new ArrayList();
						lvarbind2.add(ic_epo);
						lvarbind2.add(moneda);					
						ps2= con.queryPrecompilado(queryExiste.toString(),lvarbind2);			
						rs2 = ps2.executeQuery();				
					
					
						if(rs2.next()){				
									accion = accion = rs2.getString("valor")==null?"":rs2.getString("valor");	
						}
						rs2.close();
						ps2.close();		
							
					if(accion.equals("0")){ 
					
					// se inserta en caso de no existir 																										
							ps = con.queryPrecompilado(queryInsert.toString());
							ps.setString(1, ic_epo);
							ps.setString(2, moneda);
							ps.setString(3, noCajones);
							ps.executeUpdate();
							ps.close();							
							guarda ="G";
					
					}else if(!accion.equals("0") ){
						
							// se Modifica en caso de existir								
							ps = con.queryPrecompilado(queryUpdate.toString());
							ps.setString(1, noCajones);
							ps.setString(2, ic_epo);
							ps.setString(3, moneda);				
							ps.executeUpdate();
							ps.close();
						
						 guarda ="M";
					}
			}
		return guarda;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error guardaParametrosCajones",e);
		}finally{
			if(con.hayConexionAbierta()){
			con.terminaTransaccion(resultado);
			con.cierraConexionDB();
			}
			log.info("guardaParametrosCajones(S)");
		}
	}
	 
	 /**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param moneda
	 * @param ic_epo
	 */
	 
  public List getParametrosCajones(String ic_epo, String moneda) throws AppException{
		log.info("getParametrosCajones(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		List lvarbind = new ArrayList();
		ArrayList registros = new ArrayList();
		StringBuffer query = new StringBuffer();
		boolean			resultado			= true;
		try{
			con.conexionDB();

		query.append(" select r.IC_RANGO as IC_RANGO, e.cg_razon_social as cg_razon_social, ");
		query.append(" m.cd_nombre as cd_nombre , ");
		query.append(" r.IG_NUM_CAJONES as  IG_NUM_CAJONES ");
		query.append(" from COM_RANGOS_PUB_PROVEEDORES r,  comcat_epo  e,  comcat_moneda m");
		query.append(" where r.ic_epo = e.ic_epo");
		query.append(" and r.ic_moneda = m.ic_moneda");
		
			if (!ic_epo.equals("") && !ic_epo.equals("T")){
				query.append(" and r.ic_epo = ? ");
				lvarbind.add(ic_epo);
			}
			if (!moneda.equals("")){
				query.append(" and r.ic_moneda = ? ");
					lvarbind.add(moneda);
			}
	
			ps = con.queryPrecompilado(query.toString(),lvarbind);			
			rs = ps.executeQuery();

			log.debug("query  "+query);
			log.debug("lvarbind  "+lvarbind);
		
			while(rs.next()){					  
				registros.add(rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social"));					
				registros.add(rs.getString("cd_nombre")==null?"":rs.getString("cd_nombre"));					
				registros.add(rs.getString("IG_NUM_CAJONES")==null?"":rs.getString("IG_NUM_CAJONES"));
				registros.add(rs.getString("IC_RANGO")==null?"":rs.getString("IC_RANGO"));					
				
			}
			rs.close();
			ps.close();		

			return registros;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error getParametrosCajones",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			log.info("getParametrosCajones(S)");
		}
	}
	 
   
	 private List catalogoEpo() throws AppException{
		log.info("getParametrosCajones(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		List lvarbind = new ArrayList();
		ArrayList catalogoEpo = new ArrayList();
		StringBuffer query = new StringBuffer();
		boolean			resultado			= true;
		try{
			con.conexionDB();

		query.append(" select ic_epo  from comcat_epo  ");
		ps = con.queryPrecompilado(query.toString(),lvarbind);			
		rs = ps.executeQuery();
		
		while(rs.next()){					  
			catalogoEpo.add(rs.getString("ic_epo")==null?"":rs.getString("ic_epo"));					
		}
		rs.close();
		ps.close();		

			return catalogoEpo;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error getParametrosCajones",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			log.info("getParametrosCajones(S)");
		}
	}
	
	/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return regresa el IF, EPO Representantes y el valor de los cajones
	 * @param intemediario
	 * @param noEpo
	 */
	public  List IntermeyEpoRepresentante(String noEpo, String intemediario, String moneda) throws AppException{
		log.info("IntermeyEpoRepresentante(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		List lvarbind = new ArrayList();
		ArrayList catalogoEpo = new ArrayList();
		StringBuffer query = new StringBuffer();
		String IfRepresentante ="";
		String EPORepresentante ="";
		String cajones ="";
		 boolean			resultado			= true;

		try{
			con.conexionDB();

		//Obtiene el IF 
		query.append(" select cg_razon_social from comcat_if  ");			
		query.append(" where ic_if = "+intemediario);			
		ps = con.queryPrecompilado(query.toString(),lvarbind);			
		rs = ps.executeQuery();
		
		while(rs.next()){					  
		IfRepresentante	 = rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social");					
		}
		rs.close();
		ps.close();	
		
		//obtiene la Epo
		query = new StringBuffer();
		query.append(" select cg_razon_social from comcat_epo ");	
		query.append(" where ic_epo = "+noEpo);	
		ps = con.queryPrecompilado(query.toString(),lvarbind);			
		rs = ps.executeQuery();
		
		while(rs.next()){					  
			EPORepresentante	 = rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social");					
		}
		rs.close();
		ps.close();		


	//obtiene los Cajones
		query = new StringBuffer();
		query.append("Select  IG_NUM_CAJONES from COM_RANGOS_PUB_PROVEEDORES  ");	
		query.append(" where ic_epo = "+noEpo);			
		query.append(" and  ic_moneda = " +moneda);
		
		ps = con.queryPrecompilado(query.toString(),lvarbind);			
		rs = ps.executeQuery();
		
		while(rs.next()){					  
			cajones	 = rs.getString("IG_NUM_CAJONES")==null?"":rs.getString("IG_NUM_CAJONES");					
		}
		rs.close();
		ps.close();		

		catalogoEpo.add(IfRepresentante);
		catalogoEpo.add(EPORepresentante);
		catalogoEpo.add(cajones);
		
		return catalogoEpo;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error getParametrosCajones",e);
		}finally{
			if(con.hayConexionAbierta()){
			  con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			log.info("getParametrosCajones(S)");
		}
	}
	
/**metodo que regresa la ultima fecha en  la que le proceso 
 * de SP_ACT_REP_PROV_PUB fue Ejecutado 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 */
public  List FecchaEjecucionProceso() throws AppException{
		log.info("FechaEjecucionProceso(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		List lvarbind = new ArrayList();		
		StringBuffer query = new StringBuffer();
	    ArrayList fechaProceso = new ArrayList();
	    String mes ="";
		String desMes ="";
		boolean			resultado			= true;
		try{
			con.conexionDB();

		

	//obtiene los Cajones
		query = new StringBuffer();
		query.append("select ");
		query.append(" TO_CHAR(df_fecha,'dd')as dia, ");
		query.append(" TO_CHAR(df_fecha,'mm') as mes, ");
		query.append(" TO_CHAR(df_fecha,'yyyy')as anio, ");
		query.append(" TO_CHAR(df_fecha,'hh:mm:ss AM')as hora ");
		query.append("FROM BIT_EJECUTA_PROCESO ");
		query.append("where cg_nombre_proceso= 'SP_ACT_REP_PROV_PUB(S)' ");	
		query.append(" order by IC_EJECUTA_PROCESO desc ");
		
		ps = con.queryPrecompilado(query.toString(),lvarbind);			
		rs = ps.executeQuery();
		
		if(rs.next()){					  
			fechaProceso.add(rs.getString("dia")==null?"":rs.getString("dia"));	
			mes  = rs.getString("mes")==null?"":rs.getString("mes");
			if(mes.equals("01")){ desMes="Enero";	     }
			if(mes.equals("02")){ desMes="Febrero";	   }
			if(mes.equals("03")){ desMes="Marzo";	     }
			if(mes.equals("04")){ desMes="Abril";	     }
			if(mes.equals("05")){ desMes="Mayo";		   }
			if(mes.equals("06")){ desMes="Junio";	     }
			if(mes.equals("07")){ desMes="Julio";	     }
			if(mes.equals("08")){ desMes="Agosto";	   }
			if(mes.equals("09")){ desMes="Septiembre"; }
			if(mes.equals("10")){ desMes="Octubre";	   }
			if(mes.equals("11")){ desMes="Noviembre";  }
			if(mes.equals("12")){ desMes="Diciembre";  }			
			fechaProceso.add(desMes);
			fechaProceso.add(rs.getString("anio")==null?"":rs.getString("anio"));
			fechaProceso.add(rs.getString("hora")==null?"":rs.getString("hora"));	
		}
		rs.close();
		ps.close();		

		
		
		return fechaProceso;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error FechaEjecucionProceso",e);
		}finally{
			if(con.hayConexionAbierta()){
			 con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			log.info("FechaEjecucionProceso(S)");
		}
	}

	// Fodea 039 - 2011 By JSHD
	/**
	 * Este metodo devuelve el nombre del IF cuya clave se especifica en <tt>claveIF</tt>
	 *
	 * @throws AppException
	 *
	 * @since 	F039-2011
	 * @param 	claveIF Clave del IF.
	 * @return 	String con el nombre del intermediario financiero.
	 *
	 */
	public String getNombreIF(String claveIF)
		throws AppException {

		log.info("getNombreIF(E)");
		String nombreIF = "";

		if(claveIF == null || claveIF.equals("")){
			log.info("getNombreIF(S)");
			return nombreIF;
		}

		// Realizar consulta
		AccesoDB 		con 						= new AccesoDB();
		StringBuffer	qrySentencia			= new StringBuffer();
		Registros		registros				= null;
		List 				lVarBind					= new ArrayList();

		try {
			
				con.conexionDB();
				qrySentencia.append(
						"SELECT " +
						"	CG_RAZON_SOCIAL AS NOMBRE_IF " +
						"FROM " +
						"	COMCAT_IF " +
						"WHERE " +
						"	IC_IF           = ? " // ic_if
				); 

				lVarBind.add(new Integer(claveIF));

				registros = con.consultarDB(qrySentencia.toString(), lVarBind, false);

				if(registros != null && registros.next()){
					nombreIF = registros.getString("NOMBRE_IF");
				}

		}catch(Exception e){
			log.error("getNombreIF(Exception)");
			log.error("claveIF = <"+claveIF+">");
			e.printStackTrace();
			throw new AppException("Error al consultar el Nombre del Intermediario.");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getNombreIF(S)");
		}
		
		return nombreIF;
		
	}
	
/**
	* M�todo que obtiene el monto de la publicaci�n Vigente.
	* 
	* @param intermediario   Contiene la clave de el IF.
	* @param noEpo           Contiene la clave de la Epo .
	* @param moneda          Contiene la clave de la Moneda.
	* @return String con el valor del monto Vigente.
*/		

	public String getMontoPublicacionVigente(String intermediario, String noEpo, String moneda){
		log.info("getMontoPublicacionVigente(E)");
		//***********Validaci�n de parametros:*****
		try {
			if (intermediario == null || noEpo == null || moneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." + "\n" +
					" intermediario =" + intermediario + " noEpo ="+ noEpo+" moneda ="+moneda);
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//******************************************	
		AccesoDB con = new AccesoDB();		
		String montoPublicacionVigente = null;
		try {
			con.conexionDB();
			StringBuffer	qrySentencia 	= new StringBuffer();
			List conditions 		= new ArrayList();
				
			qrySentencia.append(
				" select /*+  use_nl(v rn p m c ) */                    "  +
				"		sum(v.fn_monto_publicacion) publicacion_vigente   "  +		 
				" from                        "  + 
				"    vm_prov_publicacion 	v,	"  +
				"    comrel_nafin 			rn,"  +
				"    comcat_pyme 				p, "  +
				"    comcat_moneda 			m, "  +
				"    com_contacto 			c 	"  +
				" where                       "  +
				"    v.ic_pyme                = rn.ic_epo_pyme_if "  +
				"    and p.ic_pyme            = rn.ic_epo_pyme_if "  +
				"    and v.ic_moneda          = m.ic_moneda       "  +
				"    and c.ic_pyme            = v.ic_pyme         "  +
				"    and c.cs_primer_contacto = 'S'               "  +
				"    and rn.cg_tipo           = 'P'               "
			);
		
			qrySentencia.append(" and v.ic_if = ? ");
			conditions.add(intermediario);
		
			qrySentencia.append(" and v.ic_epo = ? ");
			conditions.add(noEpo);
		
			qrySentencia.append(" and v.ic_moneda = ? ");
			conditions.add(moneda);
			
			String query = "" + qrySentencia;
			Registros registros = con.consultarDB(query,conditions);
			
			registros.next();
			montoPublicacionVigente = registros.getString("publicacion_vigente");
			montoPublicacionVigente = "$ "+Comunes.formatoDecimal(montoPublicacionVigente,2);
						
		}catch(Exception e){
			log.error("Error al obtener el monto de la publicaci�n Vigente ",e);
			montoPublicacionVigente = "�ERROR!";
		}finally{
			log.info("getMontoPublicacionVigente(S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}	
	
		return montoPublicacionVigente;
	}// fin metodo


/**
	* M�todo que obtiene el monto de la publicaci�n Vigente.
	* 
	* @param moneda          Contiene la clave de la Moneda.
	* @return String con el n�mero de Cajones iguales.
*/	
	public String getCajonesIguales(String moneda){
		log.info("getCajonesIguales(E)");
		//***********Validaci�n de parametros:*****************************
		try {
			if (moneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." + "\n" +
					" moneda =" + moneda);
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//*****************************************************************	
		AccesoDB con = new AccesoDB();
		StringBuffer queryTodas;
		List CajIguales2 = new ArrayList();
		List lvarbind2 = new ArrayList();
		PreparedStatement ps2= null;
		ResultSet rs2 = null;
		String cajIguales ="";
		try{
			con.conexionDB();
			//valida que los numeros de cajones sean iguales
			queryTodas = new StringBuffer();
			queryTodas.append(" Select  distinct ig_num_cajones  ");
			queryTodas.append(" from COM_RANGOS_PUB_PROVEEDORES r  ");		
			queryTodas.append(" where r.ic_moneda = ? ");
			lvarbind2.add(moneda);	
				
			ps2= con.queryPrecompilado(queryTodas.toString(),lvarbind2);			
			rs2 = ps2.executeQuery();		
				
			while(rs2.next()){				
				CajIguales2.add(rs2.getString("ig_num_cajones")==null?"":rs2.getString("ig_num_cajones"));	
				cajIguales = String.valueOf(CajIguales2.size());
			}
			rs2.close();
			ps2.close();					
								
		}catch(Exception e){
			log.error("Error al obtener el n�mero de Cajones Iguales ",e);
			throw new AppException("Error guardaParametrosCajones",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		log.info("getCajonesIguales(S)");
		}	
		
		return cajIguales;
	}//fin metodo


}//Fin Clase
