package com.netro.anticipos;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.BitacoraException;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "CapturaTasasEJB" , mappedName = "CapturaTasasEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class CapturaTasasBean implements CapturaTasas {

	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CapturaTasasBean.class);

 public void actualizaParamTasas(String ic_epo,String ic_producto_nafin)
 	throws NafinException{
	AccesoDB 	con  = null;
        ResultSet 	rs = null;
        String          qrySentencia = "";
        boolean		ok = true;
        Vector 		vecIcIf 	= new Vector();
        Vector		vecFnPuntos	= new Vector();
        Vector		vecCgRelMat	= new Vector();
        Vector		vecIcMoneda	= new Vector();
        String		ic_if		= "";
        String		fn_puntos	= "";
        String		cg_rel_mat	= "";
        String		claves 		= "";
		String		ic_moneda	= "";
        int 		i,j;
        try{
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =  "  select distinct ic_if,fn_puntos,cg_rel_mat,t.ic_moneda"   +
				"  from com_tasa_if_epo tie,comcat_tasa t"   +
				"  where tie.ic_tasa = t.ic_tasa"   +
				"  and tie.ic_epo = "+ic_epo+
				"  and tie.ic_producto_nafin = "+ic_producto_nafin;
			rs = con.queryDB(qrySentencia);
			while(rs.next()){
				vecIcIf.add(rs.getString("IC_IF"));
				vecFnPuntos.add(rs.getString("FN_PUNTOS"));
				vecCgRelMat.add(rs.getString("CG_REL_MAT"));
				vecIcMoneda.add(rs.getString("IC_MONEDA"));
			}
			con.cierraStatement();
			con.cierraConexionDB();
			for(j=0;j<vecIcIf.size();j++){
				ic_if		= (String)vecIcIf.get(j);
				fn_puntos	= (String)vecFnPuntos.get(j);
				cg_rel_mat	= (String)vecCgRelMat.get(j);
				ic_moneda	= (String)vecIcMoneda.get(j);
				System.out.println("\n\n\n*** PRODUCTO="+ic_producto_nafin+" EPO="+ic_epo+" IF="+ic_if+" MONEDA="+ic_moneda+"\n");
				Vector vecFilas = getTasasDispxEpo(ic_producto_nafin,ic_epo, ic_if,ic_moneda);
				Vector vecColumnas = null;
				claves = "";
				for(i=0;i<vecFilas.size();i++){
					vecColumnas = (Vector)vecFilas.get(i);
					if(!"".equals(claves))
						claves+= ",";
					claves += (String)vecColumnas.get(2);
				}
				System.out.println("\n\n\n*** EPO="+ic_epo+" IF="+ic_if+" CLAVES="+claves+" PUNTOS="+fn_puntos +"\n");
				if(!"".equals(claves)&&claves!=null)
					setTasasxEpo(ic_epo,ic_if,"GA",4,claves,cg_rel_mat,fn_puntos);
			}
        }catch(NafinException ne){
        	ok = false;
        	throw ne;
        }catch(Exception e){
        	ok = false;
        	throw new NafinException("SIST0001");
        }
 }

/////////////////// Metodos para Anticipos "Capturas-Tasas".
	public boolean binsertaTasa(String esCveTasa, String esRelMat, String esPuntos) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	boolean lbOK = true;
	PreparedStatement 	pst				= null;
	ResultSet			rs				= null;
	String 				lsQry 			= "";
	int					existe			= 0;
		try {
			lobdConexion.conexionDB();
			lsQry =
				" SELECT COUNT (1) AS total"   +
				"   FROM com_tasa_general"   +
				"  WHERE ic_producto_nafin = 2"   +
				"    AND ic_tasa = ? "  ;
			pst = lobdConexion.queryPrecompilado(lsQry);
			pst.setInt(1,Integer.parseInt(esCveTasa));
			rs = pst.executeQuery();
			if(rs.next())
				existe = rs.getInt("total");
			rs.close();pst.close();
			if(existe==0){
				lsQry =
					" INSERT INTO com_tasa_general"   +
					"             (ic_tasa_general,"   +
					"              ic_producto_nafin,"   +
					"              ic_tasa,"   +
					"              cg_rel_mat,"   +
					"              fn_puntos,"   +
					"              df_captura"   +
					"             )"   +
					"         SELECT NVL (MAX (ic_tasa_general), 0) + 1 AS nuevo, 2, " +esCveTasa+ ", '" +esRelMat+ "',"   +
					"                " +esPuntos+ ", SYSDATE"   +
					"           FROM com_tasa_general"  ;
				//System.out.println(lsQry);
				try{
					lobdConexion.ejecutaSQL(lsQry);
				} catch (SQLException sqle){
					lbOK = false;
					throw new NafinException("ANTI0022");
				}
			}
			else {
				if(existe>0) {
					lsQry =
						" UPDATE com_tasa_general"   +
						"    SET cg_rel_mat = '" +esRelMat+ "',"   +
						"        fn_puntos = " +esPuntos+ ","   +
						"        df_captura = SYSDATE"   +
						"  WHERE ic_producto_nafin = 2"   +
						"    AND ic_tasa = " +esCveTasa+ " "   ;
					//System.out.println(lsQry);
					try{
						lobdConexion.ejecutaSQL(lsQry);
					} catch (SQLException sqle){
						lbOK = false;
						throw new NafinException("SIST0001");
					}
				}
			}
	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en binsertaTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOK;
	}

	public boolean binsertaTasa(String esCveTasa, String esRelMat, String esPuntos, String ic_producto_nafin) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	boolean lbOK = true;
	PreparedStatement 	pst				= null;
	ResultSet			rs				= null;
	String 				lsQry 			= "";
	int					existe			= 0;
		try {
			lobdConexion.conexionDB();
			lsQry =
				" SELECT COUNT (1) AS total"   +
				"   FROM com_tasa_general"   +
				"  WHERE ic_producto_nafin = ? "   +
				"    AND ic_tasa = ? "  ;
			pst = lobdConexion.queryPrecompilado(lsQry);
			pst.setInt(1,Integer.parseInt(ic_producto_nafin));
			pst.setInt(2,Integer.parseInt(esCveTasa));
			rs = pst.executeQuery();
			if(rs.next())
				existe = rs.getInt("total");
			rs.close();pst.close();
			if(existe==0){
				lsQry =
					" INSERT INTO com_tasa_general"   +
					"             (ic_tasa_general,"   +
					"              ic_producto_nafin,"   +
					"              ic_tasa,"   +
					"              cg_rel_mat,"   +
					"              fn_puntos,"   +
					"              df_captura"   +
					"             )"   +
					"         SELECT NVL (MAX (ic_tasa_general), 0) + 1 AS nuevo, "+ic_producto_nafin+", " +esCveTasa+ ", '" +esRelMat+ "',"   +
					"                " +esPuntos+ ", SYSDATE"   +
					"           FROM com_tasa_general"  ;
				//System.out.println(lsQry);
				try{
					lobdConexion.ejecutaSQL(lsQry);
				} catch (SQLException sqle){
					lbOK = false;
					throw new NafinException("ANTI0022");
				}
			}
			else {
				if(existe>0) {
					lsQry =
						" UPDATE com_tasa_general"   +
						"    SET cg_rel_mat = '" +esRelMat+ "',"   +
						"        fn_puntos = " +esPuntos+ ","   +
						"        df_captura = SYSDATE"   +
						"  WHERE ic_producto_nafin = "+ic_producto_nafin+" "   +
						"    AND ic_tasa = " +esCveTasa+ " " ;
					//System.out.println(lsQry);
					try{
						lobdConexion.ejecutaSQL(lsQry);
					} catch (SQLException sqle){
						lbOK = false;
						throw new NafinException("SIST0001");
					}
				}
			}
			try{
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				throw new NafinException("ANTI0022");
			}
	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en binsertaTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOK;
	}

	public Vector ovgetDatosTasa(String esCveTasa) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = new Vector();
		try {
			lobdConexion.conexionDB();
 			String lsQry =
        "select ct.ic_tasa, cm.cd_nombre, cp.cg_descripcion, "+
				" TO_CHAR(SYSDATE,'dd/mm/yyyy') as fecha, "+
				" DECODE (tg.cg_rel_mat, "+
				"   '+', (mt.fn_valor + tg.fn_puntos), "+
				"	  '-', (mt.fn_valor - tg.fn_puntos), "+
				"		'*', (mt.fn_valor * tg.fn_puntos), "+
				"		'/', (mt.fn_valor / tg.fn_puntos), "+
				"		0) as tasapiso "+
        "from comcat_tasa ct, comcat_plazo cp, comcat_moneda cm, "+
        "com_tasa_general tg, com_mant_tasa mt, "+
        "comrel_tasa_producto tp "+
				"where ct.ic_tasa = " + esCveTasa +" "+
        " and mt.ic_tasa = ct.ic_tasa "+
        " and tg.ic_tasa = ct.ic_tasa "+
				"	and cm.ic_moneda = ct.ic_moneda "+
				"	and cp.ic_plazo = ct.ic_plazo "+
				"	and ct.ic_tasa = tp.ic_tasa "+
        " and tp.ic_producto_nafin = 2"+
				"	and mt.dc_fecha = (select max(mi.dc_fecha) "+
				"	                   from com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
				"			               where mi.ic_tasa = mt.ic_tasa "+
				"	                   and ti.ic_tasa = ctp.ic_tasa "+
        "                    and ctp.ic_producto_nafin = 2"+
				"			               and ti.ic_tasa = mi.ic_tasa)";
      System.out.println(lsQry);

			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next()) {
				ovDatosTasa.add(0, lrsQry.getString("CD_NOMBRE"));
				ovDatosTasa.add(1, lrsQry.getString("CG_DESCRIPCION"));
				ovDatosTasa.add(2, lrsQry.getString("FECHA"));
				ovDatosTasa.add(3, lrsQry.getString("tasapiso"));
			} else
				throw new NafinException("ANTI0022");

	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetDatosTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovDatosTasa;
	}

	public Vector ovgetDatosTasaCap(String esCveTasa) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = new Vector();
		try {
			lobdConexion.conexionDB();
 			String lsQry =
        "SELECT ct.ic_tasa, cm.cd_nombre, cp.cg_descripcion, "+
        "       TO_CHAR (SYSDATE, 'dd/mm/yyyy') AS fecha "+
        "  FROM comcat_tasa ct, "+
        "       comcat_plazo cp, "+
        "       comcat_moneda cm, "+
        "       com_mant_tasa mt, "+
        "       comrel_tasa_producto tp "+
        " WHERE ct.ic_tasa = "+esCveTasa+" "+
        "   AND mt.ic_tasa = ct.ic_tasa "+
        "   AND cm.ic_moneda = ct.ic_moneda "+
        "   AND cp.ic_plazo = ct.ic_plazo "+
        "   AND ct.ic_tasa = tp.ic_tasa "+
        "   AND tp.ic_producto_nafin = 2 "+
        "   AND mt.dc_fecha = "+
        "          (SELECT MAX (mi.dc_fecha) "+
        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
        "            WHERE mi.ic_tasa = mt.ic_tasa "+
        "              AND ti.ic_tasa = ctp.ic_tasa "+
        "              AND ctp.ic_producto_nafin = 2 "+
        "              AND ti.ic_tasa = mi.ic_tasa) ";
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next()) {
				ovDatosTasa.add(0, lrsQry.getString("CD_NOMBRE"));
				ovDatosTasa.add(1, lrsQry.getString("CG_DESCRIPCION"));
				ovDatosTasa.add(2, lrsQry.getString("FECHA"));
			} else
				throw new NafinException("ANTI0022");

	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetDatosTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovDatosTasa;
	}
/////////////////// Metodos para Anticipos "Capturas-Tasas".

	public Vector ovgetTasas(int ic_producto_nafin) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovRegistrosTasas = new Vector();
		try{
			lobdConexion.conexionDB();
			String lsQry =
				"select cm.cd_nombre as moneda, ct.cd_nombre as tipotasa, " +
				" ct.ic_tasa as cvetasa, cp.cg_descripcion as plazo, " +
				" mt.fn_valor as valor, tg.cg_rel_mat as relmat, " +
				" tg.fn_puntos as puntos, " +
				" DECODE (tg.cg_rel_mat," +
				"			'+', (mt.fn_valor + tg.fn_puntos)," +
				"			'-', (mt.fn_valor - tg.fn_puntos)," +
				"			'*', (mt.fn_valor * tg.fn_puntos)," +
				"			'/', (mt.fn_valor / tg.fn_puntos)," +
				"			0) as tasapiso, " +
				" TO_CHAR(tg.df_captura,'dd/mm/yyyy') as fecha," +
				" cp.ic_plazo, cm.ic_moneda, cp.in_plazo_meses"+
				",cp.in_plazo_dias "+
		      " ,cp.IN_PLAZO_INICIO  "+
				
				" from com_tasa_general tg, com_mant_tasa mt, " +
				" comcat_plazo cp, comcat_tasa ct, comcat_moneda cm, comrel_tasa_producto tp " +
				" where ct.ic_tasa = tg.ic_tasa" +
				" and ct.ic_plazo = cp.ic_plazo" +
				" and ct.ic_tasa = mt.ic_tasa" +
				" and mt.ic_tasa = tp.ic_tasa" +
				" and ct.ic_tasa = tp.ic_tasa" +
				" and tg.ic_producto_nafin = tp.ic_producto_nafin "+
				" and tp.ic_producto_nafin = "+ic_producto_nafin+" "+
				" and cm.ic_moneda = ct.ic_moneda" +
				" and mt.dc_fecha = (select max(mi.dc_fecha)" +
				"                    from com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp" +
				"                    where mi.ic_tasa = mt.ic_tasa" +
				"                    and ti.ic_tasa = mi.ic_tasa " +
				"                    and ti.ic_tasa = ctp.ic_tasa " +
				"                    and ctp.ic_producto_nafin = "+ic_producto_nafin+")"+
				" and tg.ic_tasa_general = (select max(ti.ic_tasa_general)" +
				"                           from com_tasa_general ti " +
				"                           where ti.ic_tasa = tg.ic_tasa "+
				"                           and ti.ic_producto_nafin = "+ic_producto_nafin+")"+
				" order by cm.ic_moneda, cp.in_plazo_meses";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
/*0*/				ovDatosTasa.add(lrsQry.getString("moneda"));
/*1*/				ovDatosTasa.add(lrsQry.getString("tipotasa"));
/*2*/				ovDatosTasa.add(lrsQry.getString("cvetasa"));
/*3*/				ovDatosTasa.add(lrsQry.getString("plazo"));
/*4*/				ovDatosTasa.add(lrsQry.getString("valor"));
/*5*/				ovDatosTasa.add(lrsQry.getString("relmat"));
/*6*/				ovDatosTasa.add(lrsQry.getString("puntos"));
/*7*/				ovDatosTasa.add(lrsQry.getString("tasapiso"));
/*8*/				ovDatosTasa.add(lrsQry.getString("fecha"));
/*9*/				ovDatosTasa.add(lrsQry.getString("in_plazo_meses"));
/*10*/				ovDatosTasa.add(lrsQry.getString("ic_moneda"));
/*11*/				ovDatosTasa.add(lrsQry.getString("in_plazo_dias"));
/*12*/            ovDatosTasa.add(lrsQry.getString("IN_PLAZO_INICIO"));
				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovRegistrosTasas;
	}

	public Vector ovgetTasasInvent(int ic_producto_nafin) throws NafinException {
		System.out.println("CapturaTasasEJB::ovgetTasasInvent(E)");
		Vector ovRegistrosTasas = new Vector();
		try{
			ovRegistrosTasas = ovgetTasasInvent(ic_producto_nafin, "");
		} catch(Exception e){
			System.out.println("CapturaTasasEJB::ovgetTasasInvent(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally	{
			System.out.println("CapturaTasasEJB::ovgetTasasInvent(S)");
		}
		return ovRegistrosTasas;
	}

	public Vector ovgetTasasInvent(int ic_producto_nafin, String ic_moneda) throws NafinException {
		System.out.println("CapturaTasasEJB::ovgetTasasInvent(E)");
		AccesoDB 			con				= new AccesoDB();
		String				qrySentencia 	= "";
		String				condicion	 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;

		Vector ovDatosTasa = null;
		Vector ovRegistrosTasas = new Vector();
		try{
			con.conexionDB();
			if(!"".equals(ic_moneda))
				condicion += " AND ct.ic_moneda = ? ";
			qrySentencia =
				" SELECT   cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa,"   +
				"          ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo,"   +
				"          mt.fn_valor AS valor, tg.cg_rel_mat AS relmat, tg.fn_puntos AS puntos,"   +
				"          DECODE ("   +
				"             tg.cg_rel_mat,"   +
				"             '+', (mt.fn_valor + tg.fn_puntos),"   +
				"             '-', (mt.fn_valor - tg.fn_puntos),"   +
				"             '*', (mt.fn_valor * tg.fn_puntos),"   +
				"             '/', (mt.fn_valor / tg.fn_puntos),"   +
				"             0 ) AS tasapiso,"   +
				"          TO_CHAR (tg.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo,"   +
				"          cm.ic_moneda, cp.in_plazo_meses, cp.in_plazo_dias, tg.ic_tasa_general"   +
				"			, tg.fn_puntos_emp AS puntos_emp,"   +
				"		   DECODE ("   +
				"		      tg.cg_rel_mat,"   +
				"		      '+', (mt.fn_valor + tg.fn_puntos_emp),"   +
				"		      '-', (mt.fn_valor - tg.fn_puntos_emp),"   +
				"		      '*', (mt.fn_valor * tg.fn_puntos_emp),"   +
				"		      '/', (mt.fn_valor / tg.fn_puntos_emp),"   +
				"		      0 ) AS tasapiso_emp"   +
				"     FROM com_tasa_general tg,"   +
				"          com_mant_tasa mt,"   +
				"          comcat_plazo cp,"   +
				"          comcat_tasa ct,"   +
				"          comcat_moneda cm,"   +
				"          comrel_tasa_producto tp,"   +
				"          (SELECT cp.in_plazo_meses"   +
				"             FROM comcat_producto_nafin cpn, comcat_plazo cp"   +
				"            WHERE cp.ic_plazo = cpn.ig_numero_min_amort"   +
				"              AND cpn.ic_producto_nafin = ?) t1,"   +
				"          (SELECT cp.in_plazo_meses"   +
				"             FROM comcat_producto_nafin cpn, comcat_plazo cp"   +
				"            WHERE cp.ic_plazo = cpn.ic_numero_max_amort"   +
				"              AND cpn.ic_producto_nafin = ?) t2"   +
				"    WHERE ct.ic_tasa = tg.ic_tasa"   +
				"      AND tg.ic_plazo = cp.ic_plazo"   +
				"      AND ct.ic_tasa = mt.ic_tasa"   +
				"      AND mt.ic_tasa = tp.ic_tasa"   +
				"      AND ct.ic_tasa = tp.ic_tasa"   +
				"      AND tg.ic_producto_nafin = tp.ic_producto_nafin"   +
				"      AND tp.ic_producto_nafin = ?"   +
				"      AND cm.ic_moneda = ct.ic_moneda"   +
				"      AND mt.dc_fecha ="   +
				"             (SELECT MAX (mi.dc_fecha)"   +
				"                FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp"   +
				"               WHERE mi.ic_tasa = mt.ic_tasa"   +
				"                 AND ti.ic_tasa = mi.ic_tasa"   +
				"                 AND ti.ic_tasa = ctp.ic_tasa"   +
				"                 AND ctp.ic_producto_nafin = ?)"   +
				"      AND cp.in_plazo_meses BETWEEN t1.in_plazo_meses AND t2.in_plazo_meses"   +
/*				" and tg.ic_tasa_general = (select max(ti.ic_tasa_general)" +
				"                           from com_tasa_general ti " +
				"                           where ti.ic_tasa = tg.ic_tasa "+
				"                           and ti.ic_producto_nafin = "+ic_producto_nafin+")"+*/
				condicion+
				" ORDER BY cm.ic_moneda, cp.in_plazo_meses"  ;
			//System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, ic_producto_nafin);
			ps.setInt( 2, ic_producto_nafin);
			ps.setInt( 3, ic_producto_nafin);
			ps.setInt( 4, ic_producto_nafin);
			if(!"".equals(ic_moneda))
				ps.setInt( 5, Integer.parseInt(ic_moneda));
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				ovDatosTasa = new Vector();
/*0*/			ovDatosTasa.add(rs.getString("moneda"));
/*1*/			ovDatosTasa.add(rs.getString("tipotasa"));
/*2*/			ovDatosTasa.add(rs.getString("cvetasa"));
/*3*/			ovDatosTasa.add(rs.getString("plazo"));
/*4*/			ovDatosTasa.add(rs.getString("valor"));
/*5*/			ovDatosTasa.add(rs.getString("relmat"));
/*6*/			ovDatosTasa.add(rs.getString("puntos"));
/*7*/			ovDatosTasa.add(rs.getString("tasapiso"));
/*8*/			ovDatosTasa.add(rs.getString("fecha"));
/*9*/			ovDatosTasa.add(rs.getString("in_plazo_meses"));
/*10*/			ovDatosTasa.add(rs.getString("ic_moneda"));
/*11*/			ovDatosTasa.add(rs.getString("in_plazo_dias"));
/*12*/			ovDatosTasa.add(rs.getString("ic_tasa_general"));
/*13*/			ovDatosTasa.add(rs.getString("puntos_emp"));
/*14*/			ovDatosTasa.add(rs.getString("tasapiso_emp"));
				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			rs.close(); ps.close();
		} catch(Exception e){
			System.out.println("CapturaTasasEJB::ovgetTasasInvent(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally	{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("CapturaTasasEJB::ovgetTasasInvent(S)");
		}
		return ovRegistrosTasas;
	}

	public Vector ovgetTasas(int ic_producto_nafin,String ic_monedas) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovRegistrosTasas = new Vector();
		try{
			lobdConexion.conexionDB();
			String lsQry =
        "select cm.cd_nombre as moneda, ct.cd_nombre as tipotasa, " +
				" ct.ic_tasa as cvetasa, cp.cg_descripcion as plazo, " +
				" mt.fn_valor as valor, tg.cg_rel_mat as relmat, " +
				" tg.fn_puntos as puntos, " +
				" DECODE (tg.cg_rel_mat," +
				"			'+', (mt.fn_valor + tg.fn_puntos)," +
				"			'-', (mt.fn_valor - tg.fn_puntos)," +
				"			'*', (mt.fn_valor * tg.fn_puntos)," +
				"			'/', (mt.fn_valor / tg.fn_puntos)," +
				"			0) as tasapiso, " +
				" TO_CHAR(tg.df_captura,'dd/mm/yyyy') as fecha," +
				" cp.ic_plazo, cm.ic_moneda, cp.in_plazo_meses"+
				",cp.in_plazo_dias "+
				" from com_tasa_general tg, com_mant_tasa mt, " +
				" comcat_plazo cp, comcat_tasa ct, comcat_moneda cm, comrel_tasa_producto tp " +
				" where ct.ic_tasa = tg.ic_tasa" +
				" and ct.ic_plazo = cp.ic_plazo" +
				" and ct.ic_tasa = mt.ic_tasa" +
				" and mt.ic_tasa = tp.ic_tasa" +
				" and ct.ic_tasa = tp.ic_tasa" +
        " and tg.ic_producto_nafin = tp.ic_producto_nafin "+
				" and tp.ic_producto_nafin = "+ic_producto_nafin+" "+
				" and cm.ic_moneda = ct.ic_moneda" +
				" and ct.ic_moneda in("+ic_monedas+")"+
	  		" and mt.dc_fecha = (select max(mi.dc_fecha)" +
				"                    from com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp" +
				"                    where mi.ic_tasa = mt.ic_tasa" +
				"                    and ti.ic_tasa = mi.ic_tasa " +
				"                    and ti.ic_tasa = ctp.ic_tasa " +
        "                    and ctp.ic_producto_nafin = "+ic_producto_nafin+")"+
				" and tg.ic_tasa_general = (select max(ti.ic_tasa_general)" +
				"                           from com_tasa_general ti " +
				"                           where ti.ic_tasa = tg.ic_tasa "+
        "                           and ti.ic_producto_nafin = "+ic_producto_nafin+")"+
				" order by cm.ic_moneda, cp.ic_plazo";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
/*0*/				ovDatosTasa.add(lrsQry.getString("moneda"));
/*1*/				ovDatosTasa.add(lrsQry.getString("tipotasa"));
/*2*/				ovDatosTasa.add(lrsQry.getString("cvetasa"));
/*3*/				ovDatosTasa.add(lrsQry.getString("plazo"));
/*4*/				ovDatosTasa.add(lrsQry.getString("valor"));
/*5*/				ovDatosTasa.add(lrsQry.getString("relmat"));
/*6*/				ovDatosTasa.add(lrsQry.getString("puntos"));
/*7*/				ovDatosTasa.add(lrsQry.getString("tasapiso"));
/*8*/				ovDatosTasa.add(lrsQry.getString("fecha"));
/*9*/				ovDatosTasa.add(lrsQry.getString("in_plazo_meses"));
/*10*/				ovDatosTasa.add(lrsQry.getString("ic_moneda"));
/*11*/				ovDatosTasa.add(lrsQry.getString("in_plazo_dias"));
				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovRegistrosTasas;
	}

	public Vector ovgetTasasxEpo(int ic_producto_nafin, String ic_epo,String ic_if)
    throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovRegistrosTasas = new Vector();
		try{
			lobdConexion.conexionDB();
			String lsQry =
        "SELECT cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
        "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
        "       mt.fn_valor AS valor, tie.cg_rel_mat AS relmat, "+
        "       tie.fn_puntos AS puntos, "+
        "       DECODE ( "+
        "          tie.cg_rel_mat, "+
        "          '+', (mt.fn_valor + tie.fn_puntos), "+
        "          '-', (mt.fn_valor - tie.fn_puntos), "+
        "         '*', (mt.fn_valor * tie.fn_puntos), "+
        "          '/', (mt.fn_valor / tie.fn_puntos), "+
        "          0 "+
        "       ) "+
        "             AS tasapiso, "+
        "       TO_CHAR (tie.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo, "+
        "       cm.ic_moneda "+
		"		,cp.in_plazo_dias "+
        "  FROM com_tasa_if_epo tie, "+
        "       com_mant_tasa mt, "+
        "       comcat_plazo cp, "+
        "       comcat_tasa ct, "+
        "       comcat_moneda cm, "+
        "       comrel_tasa_producto tp "+
        " WHERE ct.ic_tasa = tie.ic_tasa "+
        "   AND ct.ic_plazo = cp.ic_plazo "+
        "   AND ct.ic_tasa = mt.ic_tasa "+
        "   AND mt.ic_tasa = tp.ic_tasa "+
        "   AND ct.ic_tasa = tp.ic_tasa "+
        "   AND tie.ic_epo = "+ic_epo+" "+
        "   AND tie.ic_if = "+ic_if+" "+
        "   AND tie.ic_producto_nafin = tp.ic_producto_nafin "+
        "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
        "   AND cm.ic_moneda = ct.ic_moneda "+
        "   AND mt.dc_fecha = "+
        "          (SELECT MAX (mi.dc_fecha) "+
        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
        "            WHERE mi.ic_tasa = mt.ic_tasa "+
        "              AND ti.ic_tasa = mi.ic_tasa "+
        "              AND ti.ic_tasa = ctp.ic_tasa "+
        "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+") "+
/*        "   AND tie.ic_tasa_if_epo = (SELECT MAX (ti.ic_tasa_if_epo) "+
        "                               FROM com_tasa_if_epo ti "+
        "                              WHERE ti.ic_tasa = tie.ic_tasa "+
        "                                AND ti.ic_producto_nafin = "+ic_producto_nafin+") "+*/
        " ORDER BY cm.ic_moneda, cp.ic_plazo ";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
/*0*/				ovDatosTasa.add(lrsQry.getString("moneda"));
/*1*/				ovDatosTasa.add(lrsQry.getString("tipotasa"));
/*2*/				ovDatosTasa.add(lrsQry.getString("cvetasa"));
/*3*/				ovDatosTasa.add(lrsQry.getString("plazo"));
/*4*/				ovDatosTasa.add(lrsQry.getString("valor"));
/*5*/				ovDatosTasa.add(lrsQry.getString("relmat"));
/*6*/				ovDatosTasa.add(lrsQry.getString("puntos"));
/*7*/				ovDatosTasa.add(lrsQry.getString("tasapiso"));
/*8*/				ovDatosTasa.add(lrsQry.getString("fecha"));
/*9*/				ovDatosTasa.add(lrsQry.getString("ic_moneda"));
/*10*/				ovDatosTasa.add(lrsQry.getString("in_plazo_dias"));
/*11*/				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovRegistrosTasas;
	}

	public Vector ovgetTasasDisponibles(int ic_producto_nafin) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovTasas = new Vector();
		try {
			lobdConexion.conexionDB();
			String lsQry =
				" select ct.ic_tasa, ct.cd_nombre "+
				" from comcat_tasa ct, comrel_tasa_producto tp "+
				" where ct.ic_tasa not in (select distinct tg.ic_tasa "+
				"                         from com_tasa_general tg "+
				"                         where tg.ic_producto_nafin = "+ic_producto_nafin+")"+
				" and ct.cs_disponible = 'S' "+
				" and ct.ic_tasa = tp.ic_tasa "+
				" and tp.ic_producto_nafin = "+ic_producto_nafin+" "+
				" and ct.ic_tasa in (select mt.ic_tasa "+
				"                   from com_mant_tasa mt "+
				"                   where mt.dc_fecha = (select MAX(mi.dc_fecha) "+
				"                                        from com_mant_tasa mi "+
				"                                        where mi.ic_tasa = mt.ic_tasa)) ";
//      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
				ovDatosTasa.add(lrsQry.getString(1));
				ovDatosTasa.add(lrsQry.getString(2));
				ovTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasasDisponibles(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovTasas;
	}

	public Vector ovgetTasasDisponiblesInvet(String ic_producto_nafin) throws NafinException {
		AccesoDB lobdConexion = new AccesoDB();
		Vector ovDatosTasa = null;
		Vector ovTasas = new Vector();
		try {
			lobdConexion.conexionDB();
			String lsQry =
				" select ct.ic_tasa, ct.cd_nombre "+
				" from comcat_tasa ct, comrel_tasa_producto tp "+
				" where ct.cs_disponible = 'S' "+
				" and ct.ic_tasa = tp.ic_tasa "+
				" and tp.ic_producto_nafin = "+ic_producto_nafin+" "+
				" and ct.ic_tasa in (select mt.ic_tasa "+
				"                   from com_mant_tasa mt "+
				"                   where mt.dc_fecha = (select MAX(mi.dc_fecha) "+
				"                                        from com_mant_tasa mi "+
				"                                        where mi.ic_tasa = mt.ic_tasa)) ";
//      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
				ovDatosTasa.add(lrsQry.getString(1));
				ovDatosTasa.add(lrsQry.getString(2));
				ovTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasasDisponiblesInvet(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return ovTasas;
	}

	public Vector ovgetTasasDisponiblesxEpo(int ic_producto_nafin,String ic_epo,String ic_if) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovTasas = new Vector();
		try {
			lobdConexion.conexionDB();
			String lsQry =
      "SELECT ct.ic_tasa, ct.cd_nombre "+
      "  FROM comcat_tasa ct, comrel_tasa_producto tp "+
      " WHERE ct.ic_tasa NOT IN (SELECT DISTINCT tie.ic_tasa "+
      "                            FROM com_tasa_if_epo tie "+
      "                           WHERE tie.ic_producto_nafin = "+ic_producto_nafin+" "+
      "                             AND tie.ic_epo = "+ic_epo+" "+
      "                             AND tie.ic_if = "+ic_if+") "+
      "   AND ct.cs_disponible = 'S' "+
      "   AND ct.ic_tasa = tp.ic_tasa "+
      "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
      "   AND ct.ic_tasa IN (SELECT mt.ic_tasa "+
      "                        FROM com_mant_tasa mt "+
      "                       WHERE mt.dc_fecha = (SELECT MAX (mi.dc_fecha) "+
      "                                              FROM com_mant_tasa mi "+
      "                                             WHERE mi.ic_tasa = mt.ic_tasa))";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
				ovDatosTasa.add(lrsQry.getString(1));
				ovDatosTasa.add(lrsQry.getString(2));
				ovTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasasDisponibles(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovTasas;
	}

	public boolean binsertaTasaDis(String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsQry ="";
	boolean lbOK = true;
		try {
			lobdConexion.conexionDB();
      if ("GA".equals(pAccion)) {
        lsQry =
           "insert into com_tasa_general tg" +
		  		 " (tg.ic_tasa_general, tg.ic_producto_nafin, tg.ic_tasa, " +
			  	 " tg.cg_rel_mat, tg.fn_puntos, tg.df_captura)" +
				   " select NVL(MAX(ic_tasa_general),0) + 1 as Nuevo,"+ic_producto_nafin+", " +esCveTasa+ "," +
				   " '" +esRelMat+ "'," +esPuntos+ ", SYSDATE " +
				   " from com_tasa_general";
      }
      if ("GM".equals(pAccion)) {
        lsQry =
          "UPDATE com_tasa_general"+
          "   SET cg_rel_mat = '"+esRelMat+"', "+
          "       fn_puntos = "+esPuntos+", "+
          "       df_captura = SYSDATE"+
          " WHERE ic_tasa = "+esCveTasa+" "+
          "   AND ic_producto_nafin = "+ic_producto_nafin+" ";
      }

      System.out.println(lsQry);
			try{
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				throw new NafinException("ANTI0022");
			}
	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en binsertaTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOK;
	}

	public boolean binsertaTasaInvent(String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos, String ic_plazo, String ic_tasa_general, String esPuntosEmp) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsQry ="";
	boolean lbOK = true;
		try {
			lobdConexion.conexionDB();
      if ("GA".equals(pAccion)) {
        lsQry =
           "insert into com_tasa_general tg" +
		  		 " (tg.ic_tasa_general, tg.ic_producto_nafin, tg.ic_tasa, " +
			  	 " tg.cg_rel_mat, tg.fn_puntos, tg.df_captura, tg.ic_plazo, tg.fn_puntos_emp)" +
				   " select NVL(MAX(ic_tasa_general),0) + 1 as Nuevo,"+ic_producto_nafin+", " +esCveTasa+ "," +
				   " '" +esRelMat+ "'," +esPuntos+ ", SYSDATE,"+ic_plazo+","+esPuntosEmp+" " +
				   " from com_tasa_general";
      }
      if ("GM".equals(pAccion)) {
        lsQry =
          "UPDATE com_tasa_general"+
          "   SET cg_rel_mat = '"+esRelMat+"', "+
          "       fn_puntos = "+esPuntos+", "+
          "       fn_puntos_emp = "+esPuntosEmp+", "+
          "       df_captura = SYSDATE ,"+
          "		  ic_tasa = "+esCveTasa+
          " WHERE ic_tasa_general = "+ic_tasa_general+" "+
          "   AND ic_producto_nafin = "+ic_producto_nafin+" ";
      }

      System.out.println(lsQry);
			try{
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				throw new NafinException("ANTI0022");
			}
	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en binsertaTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOK;
	}

	public boolean binsertaTasaDisxEpo(String ic_epo,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsQry ="";
	boolean lbOK = true;
		try {
			lobdConexion.conexionDB();
      if ("GA".equals(pAccion)) {
        lsQry =
          "INSERT INTO com_tasa_if_epo tie "+
          "            (tie.ic_tasa_if_epo, "+
          "             tie.ic_producto_nafin, "+
          "             tie.ic_tasa, "+
          "             tie.cg_rel_mat, "+
          "             tie.fn_puntos, "+
          "             tie.df_captura, "+
          "             tie.ic_if, "+
          "             tie.ic_epo "+
          "            ) "+
          "        SELECT NVL (MAX (ic_tasa_if_epo), 0) + 1 AS nuevo, "+ic_producto_nafin+", "+esCveTasa+", "+
          "               '"+esRelMat+"', "+esPuntos+", SYSDATE, "+ic_if+", "+ic_epo+" "+
          "          FROM com_tasa_if_epo";
      }
      if ("GM".equals(pAccion)) {
        lsQry =
          "UPDATE com_tasa_if_epo "+
          "   SET cg_rel_mat = '"+esRelMat+"', "+
          "       fn_puntos = "+esPuntos+", "+
          "       df_captura = SYSDATE "+
          " WHERE ic_tasa = "+esCveTasa+" "+
          "   AND ic_producto_nafin = "+ic_producto_nafin+" "+
          "   AND ic_epo = "+ic_epo+" "+
          "   AND ic_if = "+ic_if+" ";
      }
      System.out.println(lsQry);
			try{
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				throw new NafinException("ANTI0022");
			}
	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en binsertaTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOK;
	}

	public boolean eliminarTasasDis(String esCveTasa) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsQry ="";
	boolean lbOK = true;
		try {
			lobdConexion.conexionDB();
      lsQry =
        "DELETE "+
        "  FROM com_tasa_general "+
        " WHERE ic_tasa = "+esCveTasa+" "+
        "   AND ic_producto_nafin = 4 ";
      System.out.println(lsQry);
			try{
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				throw new NafinException("ANTI0022");
			}
	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en binsertaTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOK;
	}

	public boolean eliminarTasasDisxEpo(String ic_epo,String ic_if,String esCveTasa) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsQry ="";
	boolean lbOK = true;
		try {
			lobdConexion.conexionDB();
      lsQry =
        "DELETE "+
        "  FROM com_tasa_if_epo "+
        " WHERE ic_tasa = "+esCveTasa+" "+
        "   AND ic_epo = "+ic_epo+" "+
        "   AND ic_if = "+ic_if+" "+
        "   AND ic_producto_nafin = 4 ";
      System.out.println(lsQry);
			try{
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				throw new NafinException("ANTI0022");
			}
	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en binsertaTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOK;
	}

	public Vector ovgetDatosTasaDis(String pAccion,int ic_producto_nafin,String esCveTasa) throws NafinException {
		AccesoDB lobdConexion = new AccesoDB();
		Vector ovDatosTasa = new Vector();
		String lsQry = "";
		try {
			lobdConexion.conexionDB();
			if ("M".equals(pAccion)) {
				lsQry =
					"select ct.ic_tasa, cm.cd_nombre, cp.cg_descripcion, "+
					" TO_CHAR(SYSDATE,'dd/mm/yyyy') as fecha, "+
					" DECODE (tg.cg_rel_mat, "+
					"   '+', (mt.fn_valor + tg.fn_puntos), "+
					"	  '-', (mt.fn_valor - tg.fn_puntos), "+
					"		'*', (mt.fn_valor * tg.fn_puntos), "+
					"		'/', (mt.fn_valor / tg.fn_puntos), "+
					"		0) as tasapiso, "+
					" cp.in_plazo_meses as meses, cm.ic_moneda"+
					" from comcat_tasa ct, comcat_plazo cp, comcat_moneda cm, "+
					" com_tasa_general tg, com_mant_tasa mt, "+
					" comrel_tasa_producto tp "+
					" where ct.ic_tasa = " + esCveTasa +" "+
					" and mt.ic_tasa = ct.ic_tasa "+
					" and tg.ic_tasa = ct.ic_tasa "+
					"	and cm.ic_moneda = ct.ic_moneda "+
					"	and cp.ic_plazo = ct.ic_plazo "+
					"	and ct.ic_tasa = tp.ic_tasa "+
					" and tp.ic_producto_nafin = "+ic_producto_nafin+" "+
					"	and mt.dc_fecha = (select max(mi.dc_fecha) "+
					"	                   from com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
					"			               where mi.ic_tasa = mt.ic_tasa "+
					"	                   and ti.ic_tasa = ctp.ic_tasa "+
					"                    and ctp.ic_producto_nafin = "+ic_producto_nafin+" "+
					"			               and ti.ic_tasa = mi.ic_tasa)";
      		} else if ("A".equals(pAccion)) {
				lsQry =
					"SELECT ct.ic_tasa, cm.cd_nombre, cp.cg_descripcion, "+
					"       TO_CHAR (SYSDATE, 'dd/mm/yyyy') AS fecha, "+
					"       mt.fn_valor AS tasapiso, "+
					" cp.in_plazo_meses as meses, cm.ic_moneda "+
					"  FROM comcat_tasa ct, "+
					"       comcat_plazo cp, "+
					"       comcat_moneda cm, "+
					"       com_mant_tasa mt, "+
					"       comrel_tasa_producto tp "+
					" WHERE ct.ic_tasa = " + esCveTasa +" "+
					"   AND mt.ic_tasa = ct.ic_tasa "+
					"   AND cm.ic_moneda = ct.ic_moneda "+
					"   AND cp.ic_plazo = ct.ic_plazo "+
					"   AND ct.ic_tasa = tp.ic_tasa "+
					"   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
					"   AND mt.dc_fecha = (SELECT MAX (mi.dc_fecha) "+
					"                        FROM com_mant_tasa mi, comcat_tasa ti "+
					"                       WHERE mi.ic_tasa = mt.ic_tasa "+
					"                         AND ti.ic_tasa = mi.ic_tasa)";
      		}
			System.out.println("*** "+lsQry+" ***");
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);

			if(lrsQry.next()) {
				ovDatosTasa.add(0, lrsQry.getString("CD_NOMBRE"));
				ovDatosTasa.add(1, lrsQry.getString("CG_DESCRIPCION"));
				ovDatosTasa.add(2, lrsQry.getString("FECHA"));
				ovDatosTasa.add(3, lrsQry.getString("tasapiso"));
				ovDatosTasa.add(4, lrsQry.getString("meses"));
				ovDatosTasa.add(5, lrsQry.getString("ic_tasa"));
				ovDatosTasa.add(6, lrsQry.getString("ic_moneda"));
			}
			lrsQry.close();

		} catch(Exception e){
			e.printStackTrace();
			System.out.println("Exception ocurrida en ovgetDatosTasaDis(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return ovDatosTasa;
	}

	public Vector ovgetDatosTasaDisxEpo(String ic_epo,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = new Vector();
  String lsQry = "";
		try {
			lobdConexion.conexionDB();
      if ("M".equals(pAccion)) {
  			lsQry =
          "SELECT ct.ic_tasa, cm.cd_nombre, cp.cg_descripcion, "+
          "       TO_CHAR (SYSDATE, 'dd/mm/yyyy') AS fecha, "+
          "       DECODE ( "+
          "          tie.cg_rel_mat, "+
          "          '+', (mt.fn_valor + tie.fn_puntos), "+
          "          '-', (mt.fn_valor - tie.fn_puntos), "+
          "          '*', (mt.fn_valor * tie.fn_puntos), "+
          "          '/', (mt.fn_valor / tie.fn_puntos), "+
          "          0 "+
          "       ) "+
          "             AS tasapiso "+
          "  FROM comcat_tasa ct, "+
          "       comcat_plazo cp, "+
          "       comcat_moneda cm, "+
          "       com_tasa_if_epo tie, "+
          "       com_mant_tasa mt, "+
          "       comrel_tasa_producto tp "+
          " WHERE ct.ic_tasa = " + esCveTasa +" "+
          "   AND mt.ic_tasa = ct.ic_tasa "+
          "   AND tie.ic_tasa = ct.ic_tasa "+
          "   AND tie.ic_epo = " + ic_epo +" "+
          "   AND tie.ic_if = " + ic_if +" "+
          "   AND cm.ic_moneda = ct.ic_moneda "+
          "   AND cp.ic_plazo = ct.ic_plazo "+
          "   AND ct.ic_tasa = tp.ic_tasa "+
          "   AND tp.ic_producto_nafin = 4 "+
          "   AND mt.dc_fecha = "+
          "          (SELECT MAX (mi.dc_fecha) "+
          "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
          "            WHERE mi.ic_tasa = mt.ic_tasa "+
          "              AND ti.ic_tasa = ctp.ic_tasa "+
          "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+" "+
          "              AND ti.ic_tasa = mi.ic_tasa)";
      }
      if ("A".equals(pAccion)) {
  			lsQry =
          "SELECT ct.ic_tasa, cm.cd_nombre, cp.cg_descripcion, "+
          "       TO_CHAR (SYSDATE, 'dd/mm/yyyy') AS fecha, "+
          "       mt.fn_valor AS tasapiso "+
          "  FROM comcat_tasa ct, "+
          "       comcat_plazo cp, "+
          "       comcat_moneda cm, "+
          "       com_mant_tasa mt, "+
          "       comrel_tasa_producto tp "+
          " WHERE ct.ic_tasa = " + esCveTasa +" "+
          "   AND mt.ic_tasa = ct.ic_tasa "+
          "   AND cm.ic_moneda = ct.ic_moneda "+
          "   AND cp.ic_plazo = ct.ic_plazo "+
          "   AND ct.ic_tasa = tp.ic_tasa "+
          "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
          "   AND mt.dc_fecha = (SELECT MAX (mi.dc_fecha) "+
          "                        FROM com_mant_tasa mi, comcat_tasa ti "+
          "                       WHERE mi.ic_tasa = mt.ic_tasa "+
          "                         AND ti.ic_tasa = mi.ic_tasa)";
      }
      System.out.println("*** "+lsQry+" ***");
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next()) {
				ovDatosTasa.add(0, lrsQry.getString("CD_NOMBRE"));
				ovDatosTasa.add(1, lrsQry.getString("CG_DESCRIPCION"));
				ovDatosTasa.add(2, lrsQry.getString("FECHA"));
				ovDatosTasa.add(3, lrsQry.getString("tasapiso"));
			} else
				throw new NafinException("ANTI0022");

	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetDatosTasaDisxEpo(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovDatosTasa;
	}



  public String getAuto(String ic_if,String ic_epo) 	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "",auto="";
    try{
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia =
        "SELECT cs_autorizado_tasa "+
        "  FROM comrel_if_epo_x_producto "+
        " WHERE ic_producto_nafin = 4 "+
        "   AND ic_if = "+ic_if+" "+
        "   AND ic_epo = "+ic_epo+" ";
      System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
        auto=rs.getString("cs_autorizado_tasa")==null?"":rs.getString("cs_autorizado_tasa");
      }
      con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
   return auto;
  }//getAuto



/****** C O N S U L T A S *******/
	public Vector disOpera(String ic_epo,int ic_producto_nafin, String ic_pyme) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
  Vector		vecFilas 	= new Vector();
  Vector		vecColumnas 	= null;
		try{
			lobdConexion.conexionDB();
			String lsQry =
          "SELECT COUNT (*) as total, cg_tipo_credito "+
          "  FROM comrel_pyme_epo_x_producto ";
      if (!("".equals(ic_epo))&&"".equals(ic_pyme))
        lsQry += " WHERE ic_epo = "+ic_epo+" ";
      if (!("".equals(ic_pyme))&&"".equals(ic_epo))
        lsQry += " WHERE ic_pyme = "+ic_pyme+" ";
        lsQry +=
          "   AND ic_producto_nafin = "+ic_producto_nafin+" "+
          " GROUP BY cg_tipo_credito ";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
      	vecColumnas = new Vector();
				vecColumnas.add(lrsQry.getString("total"));
				vecColumnas.add(lrsQry.getString("cg_tipo_credito"));
        vecFilas.addElement(vecColumnas);
			}
			lobdConexion.cierraStatement();
		} catch(Exception e){
			System.out.println("Exception ocurrida en disOpera(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return vecFilas;
	}

	public String paramPagoInteres(String ic_epo,int ic_producto_nafin) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
  String respInt = "";
		try{
			lobdConexion.conexionDB();
			String lsQry =
        "SELECT NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as responsable "+
        "  FROM comrel_producto_epo pe, comcat_producto_nafin pn "+
        " WHERE pn.ic_producto_nafin = pe.ic_producto_nafin "+
        "   AND pe.ic_epo = "+ic_epo+" "+
        "   AND pe.ic_producto_nafin = "+ic_producto_nafin+" ";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if (lrsQry.next()) {
        respInt = lrsQry.getString("responsable");
			}
			lobdConexion.cierraStatement();
		} catch(Exception e){
			System.out.println("Exception ocurrida en paramPagoInteres(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return respInt;
	}

//OBTIENE EL ESQUEMA PARTICULAR DE ACUERDO AL TIPO DE CREDITO
public Vector esquemaParticular(String ic_epo,int ic_producto_nafin,String autorizado,String ic_pyme,String cg_tipo_credito) throws NafinException {
	AccesoDB  lobdConexion  = new AccesoDB();
	Vector		vecFilas 	= new Vector();
	Vector		vecColumnas 	= null;
		try{
			lobdConexion.conexionDB();
			String lsQry =
		        "SELECT DISTINCT ie.ic_if, cif.cg_razon_social ";
			if (!("".equals(ic_pyme))||"C".equals(cg_tipo_credito)) {
				lsQry += ", ce.cg_razon_social as EPO";
			}
			lsQry +=
				"  FROM comrel_if_epo ie "+
				"       ,comrel_if_epo_x_producto iexp "+
				"       ,comcat_if cif ";
			if (!("".equals(ic_pyme))||"C".equals(cg_tipo_credito)) {
				lsQry += ", comrel_pyme_epo_x_producto pexp "+
					" ,comcat_epo ce ";
			}
			if("C".equals(cg_tipo_credito)){
				lsQry +=
					"  ,com_linea_credito lc "+
					"  WHERE pexp.ic_pyme = lc.ic_pyme";
			}else {
				lsQry +=
					"  ,dis_linea_credito_dm lc "+
					"  WHERE iexp.ic_epo = lc.ic_epo ";
			}
			lsQry +=
				"   AND ie.ic_epo = iexp.ic_epo "+
				"   AND ie.ic_epo = "+ic_epo+" "+
				"   AND ie.ic_if = lc.ic_if "+
				"   AND iexp.ic_if = ie.ic_if "+
				"   AND TRUNC (lc.df_vencimiento_adicional) > TRUNC (SYSDATE) "+
				"   AND lc.cg_tipo_solicitud IN ('I',  'R') "+
				"   AND iexp.ic_producto_nafin = lc.ic_producto_nafin "+
				"   AND lc.ic_producto_nafin = "+ic_producto_nafin+" ";
			if (!"".equals(autorizado)) {
				lsQry += "   AND iexp.cs_autorizado_tasa = '"+autorizado+"' ";
			}
			lsQry +=	"   AND cif.ic_if = ie.ic_if ";
			if (!("".equals(ic_pyme))) {
				lsQry +=
					"   AND ie.ic_epo = pexp.ic_epo "+
					"   AND ce.ic_epo = pexp.ic_epo "+
					"   AND pexp.ic_producto_nafin = iexp.ic_producto_nafin "+
					"   AND pexp.ic_pyme = "+ic_pyme+" ";
			}
			System.out.println("esquemaParticular "+lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				vecColumnas = new Vector();
				vecColumnas.add(lrsQry.getString("ic_if"));
				vecColumnas.add(lrsQry.getString("cg_razon_social"));
				if (!("".equals(ic_pyme)))
					vecColumnas.add(lrsQry.getString("EPO"));
		        vecFilas.addElement(vecColumnas);
			}
			lobdConexion.cierraStatement();
		} catch(Exception e){
			System.out.println("Exception ocurrida en esquemaParticular(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return vecFilas;
}


public Vector esquemaParticular(String ic_epo,int ic_producto_nafin,String autorizado,String ic_pyme) throws NafinException {
	AccesoDB  lobdConexion  = new AccesoDB();
	Vector		vecFilas 	= new Vector();
	Vector		vecColumnas 	= null;
		try{
			lobdConexion.conexionDB();
			String lsQry =
		        "SELECT DISTINCT ie.ic_if, cif.cg_razon_social ";
			if (!("".equals(ic_pyme))) {
				lsQry += ", ce.cg_razon_social as EPO";
			}
			lsQry +=
				"  FROM comrel_if_epo ie, "+
				"       dis_linea_credito_dm lc, "+
				"       comrel_if_epo_x_producto iexp, "+
				"       comcat_if cif ";
			if (!("".equals(ic_pyme))) {
				lsQry += ", comrel_pyme_epo_x_producto pexp, "+
					" comcat_epo ce ";
			}
			lsQry +=
				" WHERE iexp.ic_epo = lc.ic_epo "+
				"   AND ie.ic_epo = iexp.ic_epo "+
				"   AND ie.ic_epo = "+ic_epo+" "+
				"   AND ie.ic_if = lc.ic_if "+
				"   AND iexp.ic_if = ie.ic_if "+
				"   AND TRUNC (lc.df_vencimiento_adicional) > TRUNC (SYSDATE) "+
				"   AND lc.cg_tipo_solicitud IN ('I',  'R') "+
				"   AND iexp.ic_producto_nafin = lc.ic_producto_nafin "+
				"   AND lc.ic_producto_nafin = "+ic_producto_nafin+" ";
			if (!"".equals(autorizado)) {
				lsQry += "   AND iexp.cs_autorizado_tasa = '"+autorizado+"' ";
			}
			lsQry +=	"   AND cif.ic_if = ie.ic_if ";
			if (!("".equals(ic_pyme))) {
				lsQry +=
					"   AND ie.ic_epo = pexp.ic_epo "+
					"   AND ce.ic_epo = pexp.ic_epo "+
					"   AND pexp.ic_producto_nafin = iexp.ic_producto_nafin "+
					"   AND pexp.ic_pyme = "+ic_pyme+" ";
			}
			System.out.println("esquemaParticular "+lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				vecColumnas = new Vector();
				vecColumnas.add(lrsQry.getString("ic_if"));
				vecColumnas.add(lrsQry.getString("cg_razon_social"));
				if (!("".equals(ic_pyme)))
					vecColumnas.add(lrsQry.getString("EPO"));
		        vecFilas.addElement(vecColumnas);
			}
			lobdConexion.cierraStatement();
		} catch(Exception e){
			System.out.println("Exception ocurrida en esquemaParticular(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return vecFilas;
}


/****** C O N S U L T A S *******/
//>>>Israel Herrera
public String getPuntosMaximosxIf(String ic_producto_nafin, String ic_if)
  throws NafinException {
    String puntos ="";
    AccesoDB lobdConexion = new AccesoDB();
		try {
			lobdConexion.conexionDB();
			String lsQry =
        "SELECT fn_limite_puntos "+
        "  FROM comrel_producto_if "+
        " WHERE ic_producto_nafin = "+ic_producto_nafin+" "+
        "   AND ic_if = "+ic_if+" ";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next())
        puntos = (lrsQry.getString(1)==null)?"":lrsQry.getString(1);
      lrsQry.close();
			lobdConexion.cierraStatement();
      if("".equals(puntos))
        throw new NafinException("DIST0019");
    }catch(NafinException ne){
    	throw ne;
    }catch(Exception e){
    	throw new NafinException("SIST0001");
		}finally	{
			if(lobdConexion.hayConexionAbierta())
        lobdConexion.cierraConexionDB();
		}
  return puntos;
}

public int getTotalLineas(String ic_producto_nafin,String ic_epo,String ic_if,String ic_moneda)
  throws NafinException {
    int TotalLineas = 0;
    AccesoDB lobdConexion = new AccesoDB();
		try {
			lobdConexion.conexionDB();
			String lsQry =
        "SELECT COUNT (*) "+
        "  FROM dis_linea_credito_dm "+
        " WHERE ic_epo = "+ic_epo+" "+
        "   AND ic_if = "+ic_if+" "+
        "   AND ic_moneda = "+ic_moneda+" "+
        "   AND ic_producto_nafin = "+ic_producto_nafin+" "+
        "   AND TRUNC (df_vencimiento_adicional) > TRUNC (SYSDATE) ";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next())
        TotalLineas = lrsQry.getInt(1);
      lrsQry.close();
			lobdConexion.cierraStatement();
		}catch(Exception e){
		  System.out.println("Exception ocurrida en getTotalLineas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta())
        lobdConexion.cierraConexionDB();
		}
  return TotalLineas;
}
  public Vector getTasasDispxEpo(String ic_producto_nafin,String ic_epo,String ic_if,String ic_moneda)
   throws NafinException {
    AccesoDB lobdConexion = new AccesoDB();
    Vector		vecFilas 	= new Vector();
    Vector		vecColumnas 	= null;
    String 		condicion = "";
    try{
		  lobdConexion.conexionDB();
	if(ic_moneda!=null&&!"".equals(ic_moneda))
         condicion += " AND ct.ic_moneda = "+ic_moneda+" ";

      String lsQry =
        "SELECT ct.ic_tasa, cm.cd_nombre, ct.cd_nombre, cp.cg_descripcion AS plazo, mt.fn_valor AS valor "+
        "  FROM comcat_tasa ct "+
        "       , comrel_tasa_producto tp "+
        "       , comrel_producto_epo pe "+
        "       , comcat_plazo cp "+
        "       , com_mant_tasa mt "+
        "       , comcat_moneda cm "+
        "       , comcat_producto_nafin pn "+
        " WHERE tp.ic_producto_nafin = "+ic_producto_nafin+" "+
        "   AND pe.ic_epo = "+ic_epo+" "+
        "   AND pe.ic_producto_nafin=pn.ic_producto_nafin "+
        "   AND pn.ic_producto_nafin ="+ic_producto_nafin+" "+
        "   AND pe.ic_producto_nafin="+ic_producto_nafin+" "+
        "   AND cm.ic_moneda = ct.ic_moneda "+
        "   AND ct.ic_tasa NOT IN (SELECT DISTINCT tie.ic_tasa "+
        "                            FROM com_tasa_if_epo tie "+
        "                           WHERE tie.ic_producto_nafin = tp.ic_producto_nafin "+
        "                             AND tie.ic_epo = pe.ic_epo "+
        "                             AND tie.ic_if = "+ic_if+") "+
        "   AND ct.cs_disponible = 'S' "+
        "   AND ct.ic_tasa = tp.ic_tasa "+
        "   AND ct.ic_tasa IN (SELECT mt.ic_tasa "+
        "                        FROM com_mant_tasa mt "+
        "                       WHERE mt.dc_fecha = (SELECT MAX (mi.dc_fecha) "+
        "                                              FROM com_mant_tasa mi "+
        "                                             WHERE mi.ic_tasa = mt.ic_tasa)) "+
        "   AND pe.ic_producto_nafin = tp.ic_producto_nafin "+
        //"   AND cp.in_plazo_dias <= nvl(pe.ig_dias_maximo,pn.in_dias_maximo) "+
        "   AND ct.ic_plazo = cp.ic_plazo "+
        "   AND ct.ic_tasa = mt.ic_tasa "+
        "   AND mt.dc_fecha = "+
        "          (SELECT MAX (mi.dc_fecha) "+
        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
        "            WHERE mi.ic_tasa = mt.ic_tasa "+
        "              AND ti.ic_tasa = mi.ic_tasa "+
        "              AND ti.ic_tasa = ctp.ic_tasa "+
        "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+") "+
        condicion;


      System.out.println("getTasasDispxEpo "+lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
      while(lrsQry.next()) {
        vecColumnas = new Vector();
				vecColumnas.addElement((lrsQry.getString(2)==null)?"":lrsQry.getString(2));//moneda
				vecColumnas.addElement((lrsQry.getString(3)==null)?"":lrsQry.getString(3));//tipo tasa
				vecColumnas.addElement((lrsQry.getString(1)==null)?"":lrsQry.getString(1));//clave tasa
				vecColumnas.addElement((lrsQry.getString(4)==null)?"":lrsQry.getString(4));//plazo
				vecColumnas.addElement((lrsQry.getString(5)==null)?"":lrsQry.getString(5));//valor
        vecFilas.addElement(vecColumnas);
			}
			lobdConexion.cierraStatement();
    } catch(Exception e){
		  System.out.println("Exception ocurrida en getTasasDispxEpo(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta())
        lobdConexion.cierraConexionDB();
    }
  	return vecFilas;
  }

	public Vector ovgetTasasxEpo(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda,String ic_plazo)
    throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovRegistrosTasas = new Vector();
		try{
			lobdConexion.conexionDB();
			String lsQry =
	        " SELECT distinct cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
	        "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
	        "       mt.fn_valor AS valor, tie.cg_rel_mat AS relmat, "+
	        "       tie.fn_puntos AS puntos, "+
	        "       DECODE ( "+
	        "          tie.cg_rel_mat, "+
	        "          '+', (mt.fn_valor + tie.fn_puntos), "+
	        "          '-', (mt.fn_valor - tie.fn_puntos), "+
	        "         '*', (mt.fn_valor * tie.fn_puntos), "+
	        "          '/', (mt.fn_valor / tie.fn_puntos), "+
	        "          0 "+
	        "       ) "+
	        "             AS tasapiso, "+
	        "       TO_CHAR (tie.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo as icplazo, "+
	        "       cm.ic_moneda as icmoneda  "+
			//  "        CPY.CG_RAZON_SOCIAL AS NOMBRE_PYME "+//MOD +(line)
	        "  FROM com_tasa_if_epo tie, "+
	        "       com_mant_tasa mt, "+
	        "       comcat_plazo cp, "+
	        "       comcat_tasa ct, "+
	        "       comcat_moneda cm, "+
	        "       comrel_tasa_producto tp, "+
			  "       COMCAT_PYME CPY, "+
			  "       COMREL_PYME_EPO CPE, "+
			  "       COMREL_PYME_EPO_X_PRODUCTO PEXP "+
	        " WHERE ct.ic_tasa = tie.ic_tasa "+
	        "   AND ct.ic_plazo = cp.ic_plazo "+
	        "   AND ct.ic_tasa = mt.ic_tasa "+
	        "   AND mt.ic_tasa = tp.ic_tasa "+
	        "   AND ct.ic_tasa = tp.ic_tasa "+
			  "	AND CPY.IC_PYME = CPE.IC_PYME "+
			  "	AND CPY.IC_PYME = PEXP.IC_PYME "+
			  "	AND CPE.IC_EPO = PEXP.IC_EPO "+
			  "	AND PEXP.IC_EPO = TIE.IC_EPO  "+
			  "	AND PEXP.CG_TIPO_CREDITO IN( 'D') "+//??????????????????????????????????????????????????????????????????????????????????????????
	        "   AND tie.ic_epo = "+ic_epo+" "+
	        "   AND tie.ic_if = "+ic_if+" "+
	        "   AND tie.ic_producto_nafin = tp.ic_producto_nafin "+
	        "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
	        "   AND ct.ic_moneda in ("+ic_moneda+") "+
	        "   AND cm.ic_moneda = ct.ic_moneda "+
	        "   AND mt.dc_fecha = trunc(  "+
	        "          (SELECT MAX (mi.dc_fecha) "+
	        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
	        "            WHERE mi.ic_tasa = mt.ic_tasa "+
	        "              AND ti.ic_tasa = mi.ic_tasa "+
	        "              AND ti.ic_tasa = ctp.ic_tasa "+
	        "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+") ) ";
	        if(!ic_plazo.equals(""))
	       		lsQry += " and cp.ic_plazo = "+ ic_plazo;
	        lsQry += " ORDER BY cm.ic_moneda, cp.ic_plazo";
	      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
				ovDatosTasa.add(lrsQry.getString("moneda"));
				ovDatosTasa.add(lrsQry.getString("tipotasa"));
				ovDatosTasa.add(lrsQry.getString("cvetasa"));
				ovDatosTasa.add(lrsQry.getString("plazo"));
				ovDatosTasa.add(lrsQry.getString("valor"));
				ovDatosTasa.add(lrsQry.getString("relmat"));
				ovDatosTasa.add(lrsQry.getString("puntos"));
				ovDatosTasa.add(lrsQry.getString("tasapiso"));
				ovDatosTasa.add(lrsQry.getString("fecha"));
				ovDatosTasa.add(lrsQry.getString("icplazo"));
				ovDatosTasa.add(lrsQry.getString("icmoneda"));
				//ovDatosTasa.add(lrsQry.getString("NOMBRE_PYME"));
				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovRegistrosTasas;
	}

	/**
	 * FODEA-005-2014
	 * 	Metodo que genera la consulta del grid para tipo de  credito C (modalidad 2  Preferencial)
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_p�me
	 * @param ic_plazo
	 * @param ic_moneda
	 * @param ic_if
	 * @param ic_epo
	 * @param ic_producto_nafin
	 */
	public Vector ovgetTasasxEpo1(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda,String ic_plazo,String ic_pyme,String tipoCredito)
    throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovRegistrosTasas = new Vector();
		try{
			lobdConexion.conexionDB();
			String lsQry="";
			if("C".equals(tipoCredito)){//modalidad 2
				lsQry =
				  " SELECT cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
				  "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
				  "       mt.fn_valor AS valor, tie.cg_rel_mat AS relmat, "+
				  "       tie.fn_puntos AS puntos, "+
				  "       DECODE ( "+
				  "          tie.cg_rel_mat, "+
				  "          '+', (mt.fn_valor + tie.fn_puntos), "+
				  "          '-', (mt.fn_valor - tie.fn_puntos), "+
				  "         '*', (mt.fn_valor * tie.fn_puntos), "+
				  "          '/', (mt.fn_valor / tie.fn_puntos), "+
				  "          0 "+
				  "       ) "+
				  "             AS tasapiso, "+
				  "       TO_CHAR (tie.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo as icplazo, "+
				  "       cm.ic_moneda as icmoneda, "+
				  "        CPY.CG_RAZON_SOCIAL AS NOMBRE_PYME,  "+//MOD +(line)
				  "    pre.fn_puntos AS puntospref  "+

				  "  FROM com_tasa_if_pyme tie, "+
				  "       com_mant_tasa mt, "+
				  "       comcat_plazo cp, "+
				  "       comcat_tasa ct, "+
				  "       comcat_moneda cm, "+
				  "       comrel_tasa_producto tp, "+
				  "       COMCAT_PYME CPY, "+
				  "       COMREL_PYME_EPO CPE, "+
				  "       COMREL_PYME_EPO_X_PRODUCTO PEXP , "+

				  "	(SELECT fn_puntos, ic_moneda, ic_plazo, cg_tipos_credito  "+
				  " FROM dis_tasa_prefer_nego   "+
				  " WHERE ic_pyme = "+ic_pyme+
				  " AND ic_producto_nafin = "+ic_producto_nafin+
				  " AND ic_epo = "+ic_epo+
				  " AND ic_if = "+ic_if+
				  " AND ic_moneda in ("+ic_moneda+") "+
				  " AND cg_tipos_credito = 'C' "+
				  " AND cg_tipo = 'P') pre   "+

				  " WHERE ct.ic_tasa = tie.ic_tasa "+
				  "   AND ct.ic_plazo = cp.ic_plazo "+
				  "   AND ct.ic_tasa = mt.ic_tasa "+
				  "   AND mt.ic_tasa = tp.ic_tasa "+
				  "   AND ct.ic_tasa = tp.ic_tasa "+
				  "	AND CPY.IC_PYME = CPE.IC_PYME "+
				  "	AND CPY.IC_PYME = PEXP.IC_PYME "+
				  "	AND CPE.IC_EPO = PEXP.IC_EPO "+
				  "	AND PEXP.IC_PYME = TIE.IC_PYME  "+
				  "	AND PEXP.CG_TIPO_CREDITO IN( 'C') "+//??????????????????????????????????????????????????????????????????????????????????????????
				  "   AND tie.ic_pyme = "+ic_pyme+" "+
				  "   AND tie.ic_if = "+ic_if+" "+
				  "   AND tie.ic_producto_nafin = tp.ic_producto_nafin "+
				  "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
				  "   AND ct.ic_moneda in ("+ic_moneda+") "+
				  "   AND cm.ic_moneda = ct.ic_moneda "+
				  "   AND mt.dc_fecha = "+
				  "          (SELECT MAX (mi.dc_fecha) "+
				  "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
				  "            WHERE mi.ic_tasa = mt.ic_tasa "+
				  "              AND ti.ic_tasa = mi.ic_tasa "+
				  "              AND ti.ic_tasa = ctp.ic_tasa "+
				  "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+") ";
				  if(!ic_plazo.equals(""))
						lsQry += " and cp.ic_plazo = "+ ic_plazo;

				lsQry += " AND cpy.IC_PYME= "+ic_pyme +" ";


				lsQry += " AND pre.fn_puntos(+) > 0  "+
				" AND pre.ic_moneda(+) = ct.ic_moneda  "+
				" AND pre.ic_plazo(+) = ct.ic_plazo    ";

				  lsQry += " ORDER BY cm.ic_moneda, cp.ic_plazo";
			}else{
				lsQry =
				  " SELECT cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
				  "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
				  "       mt.fn_valor AS valor, tie.cg_rel_mat AS relmat, "+
				  "       tie.fn_puntos AS puntos, "+
				  "       DECODE ( "+
				  "          tie.cg_rel_mat, "+
				  "          '+', (mt.fn_valor + tie.fn_puntos), "+
				  "          '-', (mt.fn_valor - tie.fn_puntos), "+
				  "         '*', (mt.fn_valor * tie.fn_puntos), "+
				  "          '/', (mt.fn_valor / tie.fn_puntos), "+
				  "          0 "+
				  "       ) "+
				  "             AS tasapiso, "+
				  "       TO_CHAR (tie.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo as icplazo, "+
				  "       cm.ic_moneda as icmoneda, "+
				  "        CPY.CG_RAZON_SOCIAL AS NOMBRE_PYME ,  "+//MOD +(line)
				   "    pre.fn_puntos AS puntospref  "+

				  "  FROM com_tasa_if_epo tie, "+
				  "       com_mant_tasa mt, "+
				  "       comcat_plazo cp, "+
				  "       comcat_tasa ct, "+
				  "       comcat_moneda cm, "+
				  "       comrel_tasa_producto tp, "+
				  "       COMCAT_PYME CPY, "+
				  "       COMREL_PYME_EPO CPE, "+
				  "       COMREL_PYME_EPO_X_PRODUCTO PEXP ,  "+

				  "	(SELECT fn_puntos, ic_moneda, ic_plazo, cg_tipos_credito  "+
				  " FROM dis_tasa_prefer_nego   "+
				  " WHERE ic_pyme = "+ic_pyme+
				  " AND ic_producto_nafin = "+ic_producto_nafin+
				  " AND ic_epo = "+ic_epo+
				  " AND ic_if = "+ic_if+
				  " AND ic_moneda in ("+ic_moneda+") "+
				  " AND cg_tipos_credito = 'D' "+
				  " AND cg_tipo = 'P') pre   "+


				  " WHERE ct.ic_tasa = tie.ic_tasa "+
				  "   AND ct.ic_plazo = cp.ic_plazo "+
				  "   AND ct.ic_tasa = mt.ic_tasa "+
				  "   AND mt.ic_tasa = tp.ic_tasa "+
				  "   AND ct.ic_tasa = tp.ic_tasa "+
				  "	AND CPY.IC_PYME = CPE.IC_PYME "+
				  "	AND CPY.IC_PYME = PEXP.IC_PYME "+
				  "	AND CPE.IC_EPO = PEXP.IC_EPO "+
				  "	AND PEXP.IC_EPO = TIE.IC_EPO  "+
				  "	AND PEXP.CG_TIPO_CREDITO IN( 'D') "+//??????????????????????????????????????????????????????????????????????????????????????????
				  "   AND tie.ic_epo = "+ic_epo+" "+
				  "   AND tie.ic_if = "+ic_if+" "+
				  "   AND tie.ic_producto_nafin = tp.ic_producto_nafin "+
				  "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
				  "   AND ct.ic_moneda in ("+ic_moneda+") "+
				  "   AND cm.ic_moneda = ct.ic_moneda "+
				  "   AND mt.dc_fecha = "+
				  "          (SELECT MAX (mi.dc_fecha) "+
				  "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
				  "            WHERE mi.ic_tasa = mt.ic_tasa "+
				  "              AND ti.ic_tasa = mi.ic_tasa "+
				  "              AND ti.ic_tasa = ctp.ic_tasa "+
				  "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+") ";
				  if(!ic_plazo.equals(""))
						lsQry += " and cp.ic_plazo = "+ ic_plazo;

				lsQry += " AND cpy.IC_PYME= "+ic_pyme +" ";

				lsQry += " AND pre.fn_puntos(+) > 0  "+
				" AND pre.ic_moneda(+) = ct.ic_moneda  "+
				" AND pre.ic_plazo(+) = ct.ic_plazo    ";

				  lsQry += " ORDER BY cm.ic_moneda, cp.ic_plazo";
			}

	      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
				ovDatosTasa.add(lrsQry.getString("moneda"));
				ovDatosTasa.add(lrsQry.getString("tipotasa"));
				ovDatosTasa.add(lrsQry.getString("cvetasa"));
				ovDatosTasa.add(lrsQry.getString("plazo"));
				ovDatosTasa.add(lrsQry.getString("valor"));
				ovDatosTasa.add(lrsQry.getString("relmat"));
				ovDatosTasa.add(lrsQry.getString("puntos"));
				ovDatosTasa.add(lrsQry.getString("tasapiso"));
				ovDatosTasa.add(lrsQry.getString("fecha"));
				ovDatosTasa.add(lrsQry.getString("icplazo"));
				ovDatosTasa.add(lrsQry.getString("icmoneda"));
				ovDatosTasa.add(lrsQry.getString("NOMBRE_PYME"));
				ovDatosTasa.add( (lrsQry.getString("puntospref")==null)?"0":lrsQry.getString("puntospref") );

				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovRegistrosTasas;
	}

	public Vector ovgetTasasxEpo(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda,String ic_plazo,String ic_pyme,String tipoCredito)
    throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovRegistrosTasas = new Vector();
	String lsQry = "";
	ResultSet lrsQry = null;
	String	valor = "";
		try{
			lobdConexion.conexionDB();
			lsQry =
			" SELECT tpn.fn_valor"   +
			" FROM dis_tasa_prefer_nego tpn"   +
			" ,comcat_tasa ct"   +
			" WHERE ct.ic_tasa = tpn.ic_tasa "   +
			" AND tpn.ic_epo = "  +ic_epo+
			" AND tpn.ic_pyme = "   +ic_pyme+
			" AND tpn.ic_if = "   +ic_if+
			" AND tpn.ic_producto_nafin = "   +ic_producto_nafin+
			" AND tpn.cg_tipo = 'N'"   +
			" AND ct.ic_plazo = "+ic_plazo  ;

			lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next()){
				valor = lrsQry.getString("FN_VALOR");
			}
			lobdConexion.cierraStatement();



			lsQry =
	        " SELECT cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
	        "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
	        "       mt.fn_valor AS valor, tie.cg_rel_mat AS relmat, "+
	        "       tie.fn_puntos AS puntos, "+
	        "       DECODE ( "+
	        "          tie.cg_rel_mat, "+
	        "          '+', (mt.fn_valor + tie.fn_puntos), "+
	        "          '-', (mt.fn_valor - tie.fn_puntos), "+
	        "         '*', (mt.fn_valor * tie.fn_puntos), "+
	        "          '/', (mt.fn_valor / tie.fn_puntos), "+
	        "          0 "+
	        "       ) "+
	        "             AS tasapiso, "+
	        "       TO_CHAR (tie.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo, "+
	        "       cm.ic_moneda, "+
			  "        cpy.CG_RAZON_SOCIAL AS NOMBRE_PYME "+//MOD +(line)

			  " , pre.fn_valor as puntospref "+

	        "  FROM "+((tipoCredito.equals("C")?" com_tasa_if_pyme ":" com_tasa_if_epo "))+"  tie, "+
	        "       com_mant_tasa mt, "+
	        "       comcat_plazo cp, "+
	        "       comcat_tasa ct, "+
	        "       comcat_moneda cm, "+
	        "       comrel_tasa_producto tp, comcat_pyme cpy  "+

			  " , (SELECT fn_valor   "+
           "  FROM dis_tasa_prefer_nego      "+
           " WHERE ic_pyme = 2951852  "+
           "  AND ic_producto_nafin ="+ic_producto_nafin+
           "  AND ic_epo = "+ic_epo+
           "  AND ic_if = "+ic_if+
           "  AND ic_moneda in ("+ic_moneda+") "+
           "  AND cg_tipos_credito = 'D'  "+
           "  AND cg_tipo = 'N') pre  "+

	        " WHERE ct.ic_tasa = tie.ic_tasa "+
	        "   AND ct.ic_plazo = cp.ic_plazo "+
	        "   AND ct.ic_tasa = mt.ic_tasa "+
	        "   AND mt.ic_tasa = tp.ic_tasa "+
	        "   AND ct.ic_tasa = tp.ic_tasa "+
	        "   AND "+((tipoCredito.equals("C")?" tie.ic_pyme ="+ic_pyme+" ":" tie.ic_epo ="+ic_epo+" "))+//"tie.ic_epo = "+ic_epo+" "+
	        "   AND tie.ic_if = "+ic_if+" "+
	        "   AND tie.ic_producto_nafin = tp.ic_producto_nafin "+
	        "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
	        "   AND ct.ic_moneda in ("+ic_moneda+") "+
	        "   AND cm.ic_moneda = ct.ic_moneda "+
			"	AND cp.ic_plazo = "+ic_plazo+
	        "   AND mt.dc_fecha = "+
	        "          (SELECT MAX (mi.dc_fecha) "+
	        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
	        "            WHERE mi.ic_tasa = mt.ic_tasa "+
	        "              AND ti.ic_tasa = mi.ic_tasa "+
	        "              AND ti.ic_tasa = ctp.ic_tasa "+
	        "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+") "+
	        " AND cpy.IC_PYME="+ic_pyme+// MOD +()
			  " ORDER BY cm.ic_moneda, cp.ic_plazo ";
	      System.out.println(lsQry);
			lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
					ovDatosTasa = new Vector();
/*0*/				ovDatosTasa.add(lrsQry.getString("moneda"));
/*1*/				ovDatosTasa.add(lrsQry.getString("tipotasa"));
/*2*/				ovDatosTasa.add(lrsQry.getString("cvetasa"));
/*3*/				ovDatosTasa.add(lrsQry.getString("plazo"));
/*4*/				ovDatosTasa.add(lrsQry.getString("valor"));
/*5*/				ovDatosTasa.add(lrsQry.getString("relmat"));
/*6*/				ovDatosTasa.add(lrsQry.getString("puntos"));
/*7*/				ovDatosTasa.add(lrsQry.getString("tasapiso"));
/*8*/				ovDatosTasa.add(lrsQry.getString("fecha"));
/*9*/				ovDatosTasa.add(valor);
/*10*/			ovDatosTasa.add(lrsQry.getString("NOMBRE_PYME"));// MOD +(all)
/*11*/         ovDatosTasa.add( (lrsQry.getString("puntospref")==null)?"0":lrsQry.getString("puntospref") );

				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovRegistrosTasas;
	}

	public void guardaTasaPreferencial(String ic_epo,String ic_pyme,String ic_if,String ic_moneda,String tipo_tasa,String puntos)
		throws NafinException{
		AccesoDB			con 			= null;
		PreparedStatement	ps				= null;
		ResultSet			rs 				= null;
		String				qrySentencia	= "";
		boolean				resultado		= true;
		int					icTasaPrefer	= 0;
		try{
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" select ic_tasa_prefer_nego"+
				" from dis_tasa_prefer_nego"+
				" where ic_epo = ?"+
				" and ic_pyme = ?"+
				" and ic_if = ?"+
				" and cg_tipo = ?"+
				" and ic_moneda = ?"+
				" and ic_producto_nafin = 4";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_epo);
			ps.setString(2,ic_pyme);
			ps.setString(3,ic_if);
			ps.setString(4,tipo_tasa);
			ps.setString(5,ic_moneda);
			rs = ps.executeQuery();
			if(rs.next()){
				icTasaPrefer = rs.getInt(1);
			}
			ps.close();
			if(icTasaPrefer>0){
				qrySentencia =
					" update dis_tasa_prefer_nego"+
					" set fn_puntos = ?"+
					" ,cg_tipo = ?"+
					" where ic_tasa_prefer_nego = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setDouble(1,Double.parseDouble(puntos));
				ps.setString(2,tipo_tasa);
				ps.setInt(3,icTasaPrefer);
				ps.execute();
			}else{
				qrySentencia =
					" insert into dis_tasa_prefer_nego"+
					" (ic_tasa_prefer_nego,ic_producto_nafin,ic_epo,ic_pyme,ic_if,fn_puntos,cg_tipo,ic_moneda)"+
					" SELECT NVL(MAX(ic_tasa_prefer_nego),0)+1,4,?,?,?,?,?,? "+
					" FROM dis_tasa_prefer_nego";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_epo);
				ps.setString(2,ic_pyme);
				ps.setString(3,ic_if);
				ps.setDouble(4,Double.parseDouble(puntos));
				ps.setString(5,tipo_tasa);
				ps.setString(6,ic_moneda);
				ps.execute();
			}
		}catch(Exception e){
			System.out.println("Error = "+e);
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	}
	private String valoresAnteriores;//almacena los valores anteriores para almacenarlos en bitacora cuando se modifica el registro
	//UTILIZADO
	public void guardaTasaPreferencial(String ic_epo, String ic_pyme, String ic_if, String ic_moneda,
									   String tipo_tasa, String puntos, int ic_producto_nafin
									   ,String cc_acuse,String ic_usuario,String _acuse, String icplazo,String tipoCredito)
		throws NafinException{
		AccesoDB			con 			= null;
		PreparedStatement	ps				= null;
		ResultSet			rs 				= null;
		String				qrySentencia	= "";
		boolean				resultado		= true;
		int					icTasaPrefer	= 0;
		try{
			con = new AccesoDB();
			con.conexionDB();
			//FODEA-005-2014
			String datosAnteriores="";
			String datosNuevos="";
			//obtenemos datos del usuario para cuando se tenga que registrar los cambios en bitacora
			String nombreCompletoUsuario = "";
			try {
				UtilUsr utilUsr = new UtilUsr();
				Usuario usuario = utilUsr.getUsuario(ic_usuario);
				nombreCompletoUsuario = usuario.getApellidoPaterno() + " " +
						usuario.getApellidoMaterno() + " " + usuario.getNombre();
			} catch (Exception e) {
				throw new BitacoraException("Error al obtener los datos del usuario");
			}
			//

			qrySentencia =
				" select ic_tasa_prefer_nego"+
				" from dis_tasa_prefer_nego"+
				" where ic_epo = ?"+
				" and ic_pyme = ?"+
				" and ic_if = ?"+
				" and cg_tipo = ?"+
				" and ic_moneda = ?"+
				" and ic_producto_nafin = ?"+
				" and ic_plazo = ? "+
				" and cg_tipos_credito = ?	";/**MOD FO-005-2014*/
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_epo);
			ps.setString(2,ic_pyme);
			ps.setString(3,ic_if);
			ps.setString(4,tipo_tasa);
			ps.setString(5,ic_moneda);
			ps.setInt(6,ic_producto_nafin);
			ps.setInt(7,Integer.parseInt(icplazo));
			ps.setString(8,tipoCredito);/**MOD FO-005-2014*/
			rs = ps.executeQuery();
			if(rs.next()){
				icTasaPrefer = rs.getInt(1);
			}
			ps.close();
			rs.close();/**MOD FO-005-2014*/

			qrySentencia =
				" INSERT INTO COM_ACUSE3"+
				"(CC_ACUSE,DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN)"+
				" VALUES(?,sysdate,?,?,?)";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,cc_acuse);
			ps.setString(2,ic_usuario);
			ps.setString(3,_acuse);
			ps.setInt(4,ic_producto_nafin);
			ps.execute();
			ps.close();

			if(icTasaPrefer>0){
				/**FODEA-005-2014**/
				//Se obtienen los datos que xisten Y SE ALMACENA EN DATOS ANTERIORES
				qrySentencia="select  \n"+
					"	'TIPO='||decode(cg_tipo,'P','PREFERENCIAL','N','NEGOCIADA') AS TIPO,  \n"+
					"	'IC_EPO='||IC_EPO  AS IC_EPO,    \n"+
					"	'IC_PYME='||IC_pYME AS IC_PYME,  \n"+
					"	'TASA NEGOCIADA='||FN_PUNTOS AS TASA_NEGO     \n"+
					"	from   \n"+
					"	dis_tasa_prefer_nego    \n"+
					"	where ic_tasa_prefer_nego = ?  \n"+
					"	and ic_plazo = ? "+
					" and cg_tipos_credito = ?	";/**MOD FO-005-2014*/
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,icTasaPrefer);
				ps.setInt(2,Integer.parseInt(icplazo));
				ps.setString(3,tipoCredito);
				rs = ps.executeQuery();

				if(rs.next()){
					datosAnteriores=rs.getString("TIPO")+","+rs.getString("IC_EPO")+","+rs.getString("IC_PYME")+","+rs.getString("TASA_NEGO");
				}
				ps.close();
				//SE ARMA LA CADENA  DE LOS DATOS NUEVOS
					String tasa= "";
					if(tipo_tasa.equals("P")){
						tasa= "PREFERENCIAL";
					}else{
						tasa= "NEGOCIADA";
					}
					datosNuevos="TIPO="+tasa+",IC_EPO="+ic_epo+",IC_PYME="+ic_pyme+",TASA NEGOCIADA ="+puntos;
				/**------------------**/

				qrySentencia =
					" update dis_tasa_prefer_nego"+
					" set fn_puntos = ?"+
					" ,cg_tipo = ?"+
					" ,cc_acuse = ?"+//", cg_tipos_credito = ? "+
					" where ic_tasa_prefer_nego = ?"+
					" and ic_plazo = ?"+
					" and cg_tipos_credito = ?	";/**MOD FO-005-2014*/
				ps = con.queryPrecompilado(qrySentencia);
				ps.setDouble(1,Double.parseDouble(puntos));
				ps.setString(2,tipo_tasa);
				ps.setString(3,cc_acuse);
				ps.setInt(4,icTasaPrefer);
				ps.setInt(5,Integer.parseInt(icplazo));
				ps.setString(6,tipoCredito); /**MOD FO-005-2014*/
				ps.execute();
				ps.close();/**MOD FO-005-2014*/

			}else{
				/**FODEA-005-2014**/
				//Guardar en bitacora datos Nuevos="Tipo= PREFERENCIAL, IC_EPO= Clave EPO, IC_PYME= Calve Pyme, Tasa Negociada=10"
				//SE ARMA LA CADENA  DE LOS DATOS NUEVOS
					String tasa= "";
					if(tipo_tasa.equals("P")){
						tasa= "PREFERENCIAL";
					}else{
						tasa= "NEGOCIADA";
					}
					datosNuevos="Tipo="+tasa+", IC_EPO="+ic_epo+", IC_PYME="+ic_pyme+" , Tasa Negociada="+puntos;
				/**--------**/
				qrySentencia =
					" insert into dis_tasa_prefer_nego"+
					" (ic_tasa_prefer_nego,ic_producto_nafin,ic_epo,ic_pyme,ic_if,fn_puntos,cg_tipo,ic_moneda,cc_acuse, ic_plazo,cg_tipos_credito)"+/**MOD FO-005-2014 +(cg_tipos_credito)*/
					" SELECT NVL(MAX(ic_tasa_prefer_nego),0)+1,?,?,?,?,?,?,?,?,?,? "+/**MOD FO-005-2014 +(?)*/
					" FROM dis_tasa_prefer_nego";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,ic_producto_nafin);
				ps.setString(2,ic_epo);
				ps.setString(3,ic_pyme);
				ps.setString(4,ic_if);
				ps.setDouble(5,Double.parseDouble(puntos));
				ps.setString(6,tipo_tasa);
				ps.setString(7,ic_moneda);
				ps.setString(8,cc_acuse);
				ps.setInt(9,Integer.parseInt(icplazo));
				ps.setString(10,tipoCredito);/**MOD FO-005-2014 */
				ps.execute();
			}

			/**FODEA-005-2014  REGISTRAR CAMBIO EN BITACORA ------------------------**/
			qrySentencia =
				" INSERT INTO bit_cambios_gral ( ic_cambio, cc_pantalla_bitacora, cg_clave_afiliado, ic_nafin_electronico, " +
				" ic_usuario, cg_nombre_usuario, " +
				" cg_anterior, cg_actual) " +
				" VALUES(SEQ_BIT_CAMBIOS_GRAL.nextval, " +
				" '" + "TASAS_PREF_NEG" + "', " +
				" '" + "I" + "', " +
				" " + "(SELECT IC_NAFIN_ELECTRONICO FROM COMREL_NAFIN WHERE IC_EPO_PYME_IF ="+ic_if+" AND CG_TIPO ='I')" + ", " +
				" '" + ic_usuario + "','" + nombreCompletoUsuario + "'," +
				" '"+datosAnteriores+"','"+datosNuevos+"' )";
			try {
				if(tipoCredito.equals("C")){
					con.ejecutaSQL(qrySentencia);
					System.out.println("************Se ha agregado un nuevo registro a la bitacora general**********\n " + qrySentencia);
				}else{
					System.out.println("************ "+tipoCredito+" **********\n " + qrySentencia);
				}


			} catch(SQLException e) {
				throw new BitacoraException("Error al guardar datos en bitacora");
			}
			/**---**/

		}catch(Exception e){
			System.out.println("Error = "+e);
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	}

	public void guardaTasaNegociada(String ic_epo,String ic_pyme,String ic_if,String ic_moneda,String tipo_tasa,String valor_tasa_neg,String ic_tasa)
		throws NafinException{
		AccesoDB			con			= new AccesoDB();
		PreparedStatement	ps			= null;
		ResultSet			rs			= null;
		String				qrySentencia= "";
		boolean				resultado	= true;
		int					icTasaPrefer = 0;
		try{
			con.conexionDB();
			qrySentencia =
				" select ic_tasa_prefer_nego"+
				" from dis_tasa_prefer_nego"+
				" where ic_epo = ?"+
				" and ic_pyme = ?"+
				" and ic_if = ?"+
				" and cg_tipo = ?"+
				" and ic_moneda = ?"+
				" and ic_tasa = ?"+
				" and ic_producto_nafin = 4";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_epo);
			ps.setString(2,ic_pyme);
			ps.setString(3,ic_if);
			ps.setString(4,tipo_tasa);
			ps.setString(5,ic_moneda);
			ps.setString(6,ic_tasa);
			rs = ps.executeQuery();
			if(rs.next()){
				icTasaPrefer = rs.getInt(1);
			}
			ps.close();
			if(icTasaPrefer>0){
				qrySentencia =
					" update dis_tasa_prefer_nego"+
					" set fn_valor = ?"+
					" ,df_fecha = sysdate"+
					" where ic_tasa_prefer_nego = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setDouble(1,Double.parseDouble(valor_tasa_neg));
				ps.setInt(2,icTasaPrefer);
				ps.execute();
			}else{
				qrySentencia =
					" insert into dis_tasa_prefer_nego"+
					" (ic_tasa_prefer_nego,ic_producto_nafin,ic_epo,ic_pyme,ic_if,fn_valor,cg_tipo,ic_moneda,ic_tasa,df_fecha)"+
					"SELECT NVL(MAX(ic_tasa_prefer_nego),0)+1,4,?,?,?,?,?,?,?,sysdate "+
					"FROM dis_tasa_prefer_nego";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_epo);
				ps.setString(2,ic_pyme);
				ps.setString(3,ic_if);
				ps.setDouble(4,Double.parseDouble(valor_tasa_neg));
				ps.setString(5,tipo_tasa);
				ps.setString(6,ic_moneda);
				ps.setString(7,ic_tasa);
				ps.execute();
			}
		}catch(Exception e){
			resultado = false;
			System.out.println("Excepcion "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	}
	//UTILIZADO
	public void guardaTasaNegociada(String ic_epo, String ic_pyme, String ic_if, String ic_moneda,
									String tipo_tasa, String valor_tasa_neg, String ic_tasa,
									int ic_producto_nafin
									,String cc_acuse,String ic_usuario,String _acuse,String tipoCredito)
		throws NafinException{
		AccesoDB			con			= new AccesoDB();
		PreparedStatement	ps			= null;
		ResultSet			rs			= null;
		String				qrySentencia= "";
		boolean				resultado	= true;
		int					icTasaPrefer = 0;
		try{
			con.conexionDB();/** TODO: VERIFICAR CAMBIOS EN BITACORA**/
			/** FODEA-005-2014 **/
			String datosAnteriores="";
			String datosNuevos="";
			//obtenemos datos del usuario para cuando se tenga que registrar los cambios en bitacora
			String nombreCompletoUsuario = "";
			try {
				UtilUsr utilUsr = new UtilUsr();
				Usuario usuario = utilUsr.getUsuario(ic_usuario);
				nombreCompletoUsuario = usuario.getApellidoPaterno() + " " +
						usuario.getApellidoMaterno() + " " + usuario.getNombre();
			} catch (Exception e) {
				throw new BitacoraException("Error al obtener los datos del usuario");
			}
			/** -------- **/

			qrySentencia =
				" select ic_tasa_prefer_nego"+
				" from dis_tasa_prefer_nego"+
				" where ic_epo = ?"+
				" and ic_pyme = ?"+
				" and ic_if = ?"+
				" and cg_tipo = ?"+
				" and ic_moneda = ?"+
				" and ic_tasa = ?"+
				" and ic_producto_nafin = ?"+
				" and cg_tipos_credito = ?	";/**MOD FO-005-2014*/
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_epo);
			ps.setString(2,ic_pyme);
			ps.setString(3,ic_if);
			ps.setString(4,tipo_tasa);
			ps.setString(5,ic_moneda);
			ps.setString(6,ic_tasa);
			ps.setInt(7,ic_producto_nafin);
			ps.setString(8,tipoCredito);/**MOD FO-005-2014*/
			rs = ps.executeQuery();
			if(rs.next()){
				icTasaPrefer = rs.getInt(1);
			}
			ps.close();
			rs.close();/**MOD FO-005-2014*/

			qrySentencia =
				" INSERT INTO COM_ACUSE3"+
				"(CC_ACUSE,DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN)"+
				" VALUES(?,sysdate,?,?,?)";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,cc_acuse);
			ps.setString(2,ic_usuario);
			ps.setString(3,_acuse);
			ps.setInt(4,ic_producto_nafin);
			ps.execute();
			ps.close();

			if(icTasaPrefer>0){
				/**FODEA-005-2014**/
				//Se obtienen los datos que xisten Y SE ALMACENA EN DATOS ANTERIORES
				qrySentencia="select  \n"+
					"	'TIPO='||decode(cg_tipo,'P','PREFERENCIAL','N','NEGOCIADA') AS TIPO,  \n"+
					"	'IC_EPO='||IC_EPO  AS IC_EPO,    \n"+
					"	'IC_PYME='||IC_pYME AS IC_PYME,  \n"+
					"	'TASA NEGOCIADA='||FN_PUNTOS AS TASA_NEGO     \n"+
					"	from   \n"+
					"	dis_tasa_prefer_nego    \n"+
					"	where ic_tasa_prefer_nego = ?  \n"+
					" and cg_tipos_credito = ?	";/**MOD FO-005-2014*/
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,icTasaPrefer);
				ps.setString(2,tipoCredito);
				rs = ps.executeQuery();

				if(rs.next()){
					datosAnteriores=rs.getString("TIPO")+","+rs.getString("IC_EPO")+","+rs.getString("IC_PYME")+","+rs.getString("TASA_NEGO");
				}
				ps.close();
				//SE ARMA LA CADENA  DE LOS DATOS NUEVOS
					String tasa= "";
					if(tipo_tasa.equals("P")){
						tasa= "PREFERENCIAL";
					}else{
						tasa= "NEGOCIADA";
					}
					datosNuevos="TIPO="+tasa+",IC_EPO="+ic_epo+",IC_PYME="+ic_pyme+",TASA NEGOCIADA ="+valor_tasa_neg;
				/**------------------**/
				qrySentencia =
					" update dis_tasa_prefer_nego"+
					" set fn_valor = ?"+
					" ,df_fecha = sysdate"+
					" ,cc_acuse = ?"+
					" where ic_tasa_prefer_nego = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setDouble(1,Double.parseDouble(valor_tasa_neg));
				ps.setString(2,cc_acuse);
				ps.setInt(3,icTasaPrefer);
				ps.execute();
			}else{
				/**FODEA-005-2014**/
				//Guardar en bitacora datos Nuevos="Tipo= PREFERENCIAL, IC_EPO= Clave EPO, IC_PYME= Calve Pyme, Tasa Negociada=10"
				//SE ARMA LA CADENA  DE LOS DATOS NUEVOS
					String tasa= "";
					if(tipo_tasa.equals("P")){
						tasa= "PREFERENCIAL";
					}else{
						tasa= "NEGOCIADA";
					}
					datosNuevos="Tipo="+tasa+", IC_EPO="+ic_epo+", IC_PYME="+ic_pyme+" , Tasa Negociada="+valor_tasa_neg;
				/**--------**/
				qrySentencia =
					" insert into dis_tasa_prefer_nego"+
					" (ic_tasa_prefer_nego,ic_producto_nafin,ic_epo,ic_pyme,ic_if,fn_valor,cg_tipo,ic_moneda,ic_tasa,df_fecha,cc_acuse,cg_tipos_credito)"+/**MOD FO-005-2014 +(cg_tipos_credito)*/
					"SELECT NVL(MAX(ic_tasa_prefer_nego),0)+1,?,?,?,?,?,?,?,?,sysdate,?,? "+/**MOD FO-005-2014 +(?)*/
					"FROM dis_tasa_prefer_nego";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,ic_producto_nafin);
				ps.setString(2,ic_epo);
				ps.setString(3,ic_pyme);
				ps.setString(4,ic_if);
				ps.setDouble(5,Double.parseDouble(valor_tasa_neg));
				ps.setString(6,tipo_tasa);
				ps.setString(7,ic_moneda);
				ps.setString(8,ic_tasa);
				ps.setString(9,cc_acuse);
				ps.setString(10,tipoCredito);/**MOD FO-005-2014 */
				ps.execute();
			}

			/**FODEA-005-2014  REGISTRAR CAMBIO EN BITACORA ------------------------**/
			qrySentencia =
				" INSERT INTO bit_cambios_gral ( ic_cambio, cc_pantalla_bitacora, cg_clave_afiliado, ic_nafin_electronico, " +
				" ic_usuario, cg_nombre_usuario, " +
				" cg_anterior, cg_actual) " +
				" VALUES(SEQ_BIT_CAMBIOS_GRAL.nextval, " +
				" '" + "TASAS_PREF_NEG" + "', " +
				" '" + "I" + "', " +
				" " + "(SELECT IC_NAFIN_ELECTRONICO FROM COMREL_NAFIN WHERE IC_EPO_PYME_IF ="+ic_if+" AND CG_TIPO ='I')" + ", " +
				" '" + ic_usuario + "','" + nombreCompletoUsuario + "'," +
				" '"+datosAnteriores+"','"+datosNuevos+"')";
			try {
				if(tipoCredito.equals("C")){
					con.ejecutaSQL(qrySentencia);
					System.out.println("************Se ha agregado un nuevo registro a la bitacora general**********\n " + qrySentencia);
				}else{
					System.out.println("************ "+tipoCredito+" **********\n " + qrySentencia);
				}


			} catch(SQLException e) {
				throw new BitacoraException("Error al guardar datos en bitacora");
			}
			/**---**/
		}catch(Exception e){
			resultado = false;
			System.out.println("Excepcion "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	}


public boolean setTasasxEpo(String ic_epo,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos)
  throws NafinException {
    boolean OK = false;
		try {
      StringTokenizer str = new StringTokenizer(esCveTasa,",");
      if (str.countTokens()==0)
        OK = binsertaTasaDisxEpo(ic_epo,ic_if,pAccion,ic_producto_nafin,esCveTasa,esRelMat,esPuntos);
      else{
        while(str.hasMoreElements())
          OK = binsertaTasaDisxEpo(ic_epo,ic_if,pAccion,ic_producto_nafin,str.nextToken(),esRelMat,esPuntos);
      }
		}catch(Exception e){
		  System.out.println("Exception ocurrida en setTasasxEpo(). "+e);
			throw new NafinException("SIST0001");
		}
  return OK;
}

public int getTotalLineas(String ic_producto_nafin,String ic_epo,String ic_if,String ic_pyme,String ic_moneda)
  throws NafinException {
    int TotalLineas = 0;
    AccesoDB lobdConexion = new AccesoDB();
		try {
			lobdConexion.conexionDB();
			String lsQry =
        "SELECT COUNT (*) "+
        "  FROM com_linea_credito "+
        " WHERE ic_epo = "+ic_epo+" "+
        "   AND ic_if = "+ic_if+" "+
        "   AND ic_pyme = "+ic_pyme+" "+
        "   AND ic_moneda = "+ic_moneda+" "+
        "   AND ic_producto_nafin = "+ic_producto_nafin+" "+
        "   AND TRUNC (df_vencimiento_adicional) > TRUNC (SYSDATE) ";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			if(lrsQry.next())
        TotalLineas = lrsQry.getInt(1);
      lrsQry.close();
			lobdConexion.cierraStatement();
		}catch(Exception e){
		  System.out.println("Exception ocurrida en getTotalLineas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta())
        lobdConexion.cierraConexionDB();
		}
  return TotalLineas;
}

 public Vector getComboPlazo(int ic_producto_nafin, String ic_epo)
  throws NafinException {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
          qrySentencia =
            "SELECT cp.ic_plazo, cp.in_plazo_dias "+
            "  FROM comrel_producto_epo pe, comcat_plazo cp, comcat_producto_nafin pn "+
            " WHERE pe.ic_producto_nafin = "+ic_producto_nafin+" "+
            "   AND pe.ic_producto_nafin=pn.ic_producto_nafin "+
            "   AND pn.ic_producto_nafin ="+ic_producto_nafin+" "+
            "   AND cp.ic_producto_nafin ="+ic_producto_nafin+" "+
            "   AND ic_epo = "+ic_epo+" "+
            "   AND cp.in_plazo_dias <= NVL(pe.ig_dias_maximo,pn.in_dias_maximo) "+
            " ORDER BY 2 ";
  	rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
	        vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
	        vecFilas.addElement(vecColumnas);
	      }
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
   }

	public Vector getComboPlazoFactoraje(int ic_producto_nafin, String ic_epo, String cadPlazos){
		StringBuffer	qrySentencia	=	new StringBuffer();
		AccesoDB			con				=	null;
		PreparedStatement	ps				= null;
		ResultSet		rs					=	null;
		Vector			vecFilas			=	new Vector();
		Vector			vecColumnas		=	new Vector();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia.append(
				"SELECT cp.ic_plazo, cp.in_plazo_dias "+
				"  FROM comrel_producto_epo pe, comcat_plazo cp, comcat_producto_nafin pn "+
				" WHERE pe.ic_producto_nafin = ? "+
				"   AND pe.ic_producto_nafin=pn.ic_producto_nafin "+
				"   AND pn.ic_producto_nafin = ? "+
				"   AND cp.ic_producto_nafin = ? "+
				"   AND ic_epo = ? "+
				"   AND cp.in_plazo_dias <= NVL(pe.ig_dias_maximo,pn.in_dias_maximo) ");

			if(cadPlazos != null && !cadPlazos.equals("")){
				qrySentencia.append("   AND cp.in_plazo_dias NOT IN ("+ cadPlazos +") ");
			}

			qrySentencia.append(" ORDER BY 2 ");

			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,ic_producto_nafin);
			ps.setInt(2,ic_producto_nafin);
			ps.setInt(3,ic_producto_nafin);
			ps.setString(4,ic_epo);
			rs = ps.executeQuery();

			while(rs.next()) {
				vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
				vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
				vecFilas.addElement(vecColumnas);
			}
		} catch(Exception e){
			throw new AppException("Error al obtener los plazos");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			}
		return vecFilas;
	}

   	public Vector ovgetTasasxPyme(int ic_producto_nafin, String ic_pyme,String ic_if,String ic_moneda)
    throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovRegistrosTasas = new Vector();
		try{
			lobdConexion.conexionDB();
			String lsQry =
        "SELECT  /*+ leading(mt) */ cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
        "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
        "       mt.fn_valor AS valor, tip.cg_rel_mat AS relmat, "+
        "       tip.fn_puntos AS puntos, "+
        "       DECODE ( "+
        "          tip.cg_rel_mat, "+
        "          '+', (mt.fn_valor + tip.fn_puntos), "+
        "          '-', (mt.fn_valor - tip.fn_puntos), "+
        "          '*', (mt.fn_valor * tip.fn_puntos), "+
        "          '/', (mt.fn_valor / tip.fn_puntos), "+
        "          0 "+
        "       ) "+
        "             AS tasapiso, "+
        "       TO_CHAR (tip.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo, "+
        "       cm.ic_moneda, "+
		  "		cp.in_plazo_dias, "+
		  " TO_CHAR (tip.df_captura, 'dd/mm/yyyy hh24:mi:ss') AS fecha_cambio, tip.cg_usuario_cambio "+
        "  FROM com_tasa_if_pyme tip, "+
        "       com_mant_tasa mt, "+
        "       comcat_plazo cp, "+
        "       comcat_tasa ct, "+
        "       comcat_moneda cm, "+
        "       comrel_tasa_producto tp "+
        " WHERE ct.ic_tasa = tip.ic_tasa "+
        "   AND ct.ic_plazo = cp.ic_plazo "+
        "   AND ct.ic_tasa = mt.ic_tasa "+
        "   AND mt.ic_tasa = tp.ic_tasa "+
        "   AND ct.ic_tasa = tp.ic_tasa "+
        "   AND tip.ic_pyme = "+ic_pyme+" "+
        "   AND tip.ic_if = "+ic_if+" "+
        "   AND tip.ic_producto_nafin = tp.ic_producto_nafin "+
        "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
        "   AND ct.ic_moneda IN ("+ic_moneda+") "+
        "   AND cm.ic_moneda = ct.ic_moneda "+
        "   AND mt.dc_fecha = "+
        "          (SELECT MAX (mi.dc_fecha) "+
        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
        "            WHERE mi.ic_tasa = mt.ic_tasa "+
        "              AND ti.ic_tasa = mi.ic_tasa "+
        "              AND ti.ic_tasa = ctp.ic_tasa "+
        "              AND ctp.ic_producto_nafin = tp.ic_producto_nafin) "+
        " ORDER BY cm.ic_moneda, cp.ic_plazo ";
      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
/*0*/				ovDatosTasa.add(lrsQry.getString("moneda"));
/*1*/				ovDatosTasa.add(lrsQry.getString("tipotasa"));
/*2*/				ovDatosTasa.add(lrsQry.getString("cvetasa"));
/*3*/				ovDatosTasa.add(lrsQry.getString("plazo"));
/*4*/				ovDatosTasa.add(lrsQry.getString("valor"));
/*5*/				ovDatosTasa.add(lrsQry.getString("relmat"));
/*6*/				ovDatosTasa.add(lrsQry.getString("puntos"));
/*7*/				ovDatosTasa.add(lrsQry.getString("tasapiso"));
/*8*/				ovDatosTasa.add(lrsQry.getString("fecha"));
/*9*/				ovDatosTasa.add(lrsQry.getString("ic_moneda"));
/*10*/				ovDatosTasa.add(lrsQry.getString("in_plazo_dias"));
/*11*/				ovDatosTasa.add(lrsQry.getString("fecha_cambio"));
/*12*/				ovDatosTasa.add(	lrsQry.getString("cg_usuario_cambio")==null?"":lrsQry.getString("cg_usuario_cambio")	);
				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovRegistrosTasas;
	}

  public Vector getTasasDispxPyme(String ic_producto_nafin,String ic_pyme,String ic_if,String ic_moneda, String in_plazo_dias)
   throws NafinException {
    AccesoDB lobdConexion = new AccesoDB();
    Vector		vecFilas 	= new Vector();
    Vector		vecColumnas 	= null;
    try{
		  lobdConexion.conexionDB();
      String lsQry =
        "SELECT ct.ic_tasa, cm.cd_nombre, ct.cd_nombre, cp.cg_descripcion AS plazo, mt.fn_valor AS valor "+
        "  FROM comcat_tasa ct "+
        "       , comrel_tasa_producto tp "+
        "       , comcat_plazo cp "+
        "       , com_mant_tasa mt "+
        "       , comcat_moneda cm "+
        " WHERE ct.ic_moneda = "+ic_moneda+" "+
        "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
        "   AND cm.ic_moneda = ct.ic_moneda "+
        "   AND ct.ic_tasa NOT IN (SELECT DISTINCT tip.ic_tasa "+
        "                            FROM com_tasa_if_pyme tip "+
        "                           WHERE tip.ic_producto_nafin = tp.ic_producto_nafin "+
        "                             AND tip.ic_pyme = "+ic_pyme+" "+
        "                             AND tip.ic_if = "+ic_if+") "+
        "   AND ct.cs_disponible = 'S' "+
        "   AND ct.ic_tasa = tp.ic_tasa "+
        "   AND ct.ic_tasa IN (SELECT mt.ic_tasa "+
        "                        FROM com_mant_tasa mt "+
        "                       WHERE mt.dc_fecha = (SELECT MAX (mi.dc_fecha) "+
        "                                              FROM com_mant_tasa mi "+
        "                                             WHERE mi.ic_tasa = mt.ic_tasa)) "+
        "   AND cp.in_plazo_dias <= "+in_plazo_dias+" "+
        "   AND ct.ic_plazo = cp.ic_plazo "+
        "   AND ct.ic_tasa = mt.ic_tasa "+
        "   AND mt.dc_fecha = "+
        "          (SELECT MAX (mi.dc_fecha) "+
        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
        "            WHERE mi.ic_tasa = mt.ic_tasa "+
        "              AND ti.ic_tasa = mi.ic_tasa "+
        "              AND ti.ic_tasa = ctp.ic_tasa "+
        "              AND ctp.ic_producto_nafin = tp.ic_producto_nafin) ";
      System.out.println("getTasasDispxEpo "+lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
      while(lrsQry.next()) {
        vecColumnas = new Vector();
				vecColumnas.addElement((lrsQry.getString(2)==null)?"":lrsQry.getString(2));//moneda
				vecColumnas.addElement((lrsQry.getString(3)==null)?"":lrsQry.getString(3));//tipo tasa
				vecColumnas.addElement((lrsQry.getString(1)==null)?"":lrsQry.getString(1));//clave tasa
				vecColumnas.addElement((lrsQry.getString(4)==null)?"":lrsQry.getString(4));//plazo
				vecColumnas.addElement((lrsQry.getString(5)==null)?"":lrsQry.getString(5));//valor
        vecFilas.addElement(vecColumnas);
			}
			lobdConexion.cierraStatement();
    } catch(Exception e){
		  System.out.println("Exception ocurrida en getTasasDispxEpo(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta())
        lobdConexion.cierraConexionDB();
    }
  	return vecFilas;
  }


/**
 * Se sobrecarga el m�todo para el caso de tipo de cr�dito factoraje con recurso
 * FODEA. 019
 */
	public Vector getTasasDispxPyme(String ic_producto_nafin,String ic_pyme,String ic_if,String ic_moneda, String in_plazo_dias,	boolean fac){
		AccesoDB lobdConexion	= new AccesoDB();
		Vector	vecFilas			= new Vector();
		Vector	vecColumnas		= null;
		try{
			lobdConexion.conexionDB();
			String lsQry =
				"SELECT ct.ic_tasa, cm.cd_nombre, ct.cd_nombre, cp.cg_descripcion AS plazo, mt.fn_valor AS valor "+
				"  FROM comcat_tasa ct "+
				"       , comrel_tasa_producto tp "+
				"       , comcat_plazo cp "+
				"       , com_mant_tasa mt "+
				"       , comcat_moneda cm "+
				" WHERE ct.ic_moneda = "+ic_moneda+" "+
				"   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
				"   AND cm.ic_moneda = ct.ic_moneda "+
				"   AND ct.ic_tasa NOT IN (SELECT DISTINCT tip.ic_tasa "+
				"                            FROM com_tasa_if_pyme tip "+
				"                           WHERE tip.ic_producto_nafin = tp.ic_producto_nafin "+
				"                             AND tip.ic_pyme = "+ic_pyme+" "+
				"                             AND tip.ic_if = "+ic_if+") "+
				"   AND ct.cs_disponible = 'S' "+
				"   AND ct.ic_tasa = tp.ic_tasa "+
				"   AND ct.ic_tasa IN (SELECT mt.ic_tasa "+
				"                        FROM com_mant_tasa mt "+
				"                       WHERE mt.dc_fecha = (SELECT MAX (mi.dc_fecha) "+
				"                                              FROM com_mant_tasa mi "+
				"                                             WHERE mi.ic_tasa = mt.ic_tasa)) "+
				"   AND cp.in_plazo_dias = "+in_plazo_dias+" "+
				"   AND ct.ic_plazo = cp.ic_plazo "+
				"   AND ct.ic_tasa = mt.ic_tasa "+
				"   AND mt.dc_fecha = "+
				"          (SELECT MAX (mi.dc_fecha) "+
				"             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
				"            WHERE mi.ic_tasa = mt.ic_tasa "+
				"              AND ti.ic_tasa = mi.ic_tasa "+
				"              AND ti.ic_tasa = ctp.ic_tasa "+
				"              AND ctp.ic_producto_nafin = tp.ic_producto_nafin) ";

			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				vecColumnas = new Vector();
				vecColumnas.addElement((lrsQry.getString(2)==null)?"":lrsQry.getString(2));//moneda
				vecColumnas.addElement((lrsQry.getString(3)==null)?"":lrsQry.getString(3));//tipo tasa
				vecColumnas.addElement((lrsQry.getString(1)==null)?"":lrsQry.getString(1));//clave tasa
				vecColumnas.addElement((lrsQry.getString(4)==null)?"":lrsQry.getString(4));//plazo
				vecColumnas.addElement((lrsQry.getString(5)==null)?"":lrsQry.getString(5));//valor
				vecFilas.addElement(vecColumnas);
			}
			lobdConexion.cierraStatement();
		} catch(Exception e){
			throw new AppException("Error al obtener las tasas por pyme");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta())
				lobdConexion.cierraConexionDB();
		}
		return vecFilas;
	}

  public boolean setTasasxPyme(String ic_pyme,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos)
  throws NafinException {
    boolean OK = false;
		try {
      StringTokenizer str = new StringTokenizer(esCveTasa,",");
      if (str.countTokens()==0)
        OK = binsertaTasaDisxPyme(ic_pyme,ic_if,pAccion,ic_producto_nafin,esCveTasa,esRelMat,esPuntos);
      else{
        while(str.hasMoreElements())
          OK = binsertaTasaDisxPyme(ic_pyme,ic_if,pAccion,ic_producto_nafin,str.nextToken(),esRelMat,esPuntos);
      }
		}catch(Exception e){
		  System.out.println("Exception ocurrida en setTasasxEpo(). "+e);
			throw new NafinException("SIST0001");
		}
  return OK;
}

	public boolean binsertaTasaDisxPyme(String ic_pyme,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos)
    throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsQry ="";
	boolean lbOK = true;
		try {
			lobdConexion.conexionDB();
      if ("GA".equals(pAccion)) {
        lsQry =
          "INSERT INTO com_tasa_if_pyme tip "+
          "            (tip.ic_tasa_if_pyme, "+
          "             tip.ic_producto_nafin, "+
          "             tip.ic_tasa, "+
          "             tip.cg_rel_mat, "+
          "             tip.fn_puntos, "+
          "             tip.df_captura, "+
          "             tip.ic_if, "+
          "             tip.ic_pyme "+
          "            ) "+
          "        SELECT NVL (MAX (ic_tasa_if_pyme), 0) + 1 AS nuevo, "+ic_producto_nafin+", "+esCveTasa+", "+
          "               '"+esRelMat+"', "+esPuntos+", SYSDATE, "+ic_if+", "+ic_pyme+" "+
          "          FROM com_tasa_if_pyme";
      }
      if ("GM".equals(pAccion)) {
        lsQry =
          "UPDATE com_tasa_if_pyme "+
          "   SET cg_rel_mat = '"+esRelMat+"', "+
          "       fn_puntos = "+esPuntos+", "+
          "       df_captura = SYSDATE "+
          " WHERE ic_tasa = "+esCveTasa+" "+
          "   AND ic_producto_nafin = "+ic_producto_nafin+" "+
          "   AND ic_pyme = "+ic_pyme+" "+
          "   AND ic_if = "+ic_if+" ";
      }
      System.out.println(lsQry);
			try{
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				throw new NafinException("ANTI0022");
			}
	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en binsertaTasa(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOK;
	}

/***********************************************/
/*******Sobrecarga de metodo para Fodea.019*****/
/*************By Hugo Vargas *******************/
	public boolean setTasasxPyme(String ic_pyme,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos, String usuario)
	throws AppException {
		boolean OK = false;
		try {
			StringTokenizer str = new StringTokenizer(esCveTasa,",");
			StringTokenizer strRelMat = new StringTokenizer(esRelMat,",");
			StringTokenizer strPuntos = new StringTokenizer(esPuntos,",");
			if (str.countTokens()==0	&& strRelMat.countTokens()==0 && strPuntos.countTokens()==0){
				OK = binsertaTasaDisxPyme(ic_pyme,ic_if,pAccion,ic_producto_nafin,esCveTasa,esRelMat,esPuntos,usuario);
			}else{
				while(str.hasMoreElements()){
					OK = binsertaTasaDisxPyme(ic_pyme,ic_if,pAccion,ic_producto_nafin,str.nextToken(),strRelMat.nextToken(),strPuntos.nextToken(),usuario);
				}
			}
		}catch(Exception e){
		//System.out.println("Exception ocurrida en setTasasxEpo(). "+e);
			throw new AppException("Ocurrio un error al establecer las tasas por pyme"+e);
		}
		return OK;
	}
	public boolean binsertaTasaDisxPyme(String ic_pyme,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos, String usuario)
		throws AppException {
		AccesoDB lobdConexion = new AccesoDB();
		String lsQry ="";
		boolean lbOK = true;
		try {
			lobdConexion.conexionDB();
			if ("GA".equals(pAccion)) {
			  lsQry =
				 "INSERT INTO com_tasa_if_pyme tip "+
				 "            (tip.ic_tasa_if_pyme, "+
				 "             tip.ic_producto_nafin, "+
				 "             tip.ic_tasa, "+
				 "             tip.cg_rel_mat, "+
				 "             tip.fn_puntos, "+
				 "             tip.df_captura, "+
				 "             tip.ic_if, "+
				 "             tip.ic_pyme, "+
				 "             tip.cg_usuario_cambio "+
				 "            ) "+
				 "        SELECT NVL (MAX (ic_tasa_if_pyme), 0) + 1 AS nuevo, "+ic_producto_nafin+", "+esCveTasa+", "+
				 "               '"+esRelMat+"', "+esPuntos+", SYSDATE, "+ic_if+", "+ic_pyme+", '"+usuario+"'"+
				 "          FROM com_tasa_if_pyme";
			}
			if ("GM".equals(pAccion)) {
			  lsQry =
				 "UPDATE com_tasa_if_pyme "+
				 "   SET cg_rel_mat = '"+esRelMat+"', "+
				 "       fn_puntos = "+esPuntos+", "+
				 "       df_captura = SYSDATE, "+
				 "       cg_usuario_cambio = '"+usuario+"'"+
				 " WHERE ic_tasa = "+esCveTasa+" "+
				 "   AND ic_producto_nafin = "+ic_producto_nafin+" "+
				 "   AND ic_pyme = "+ic_pyme+" "+
				 "   AND ic_if = "+ic_if+" ";
			}
			System.out.println(lsQry);

			System.out.println(lsQry);

			try{
				lobdConexion.ejecutaSQL(lsQry);
			} catch (SQLException sqle){
				lbOK = false;
				throw new AppException("Ocurrio un error al insertar la informaci�n"+sqle);
			}
		} catch (AppException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en binsertaTasa(). "+e);
			throw new AppException("Ocurrio un error al insertar la informaci�n"+e);
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return lbOK;
	}

	public boolean eliminaTasaDisxPyme(String ic_pyme,	String ic_if,	int ic_producto_nafin,	String esCveTasa [],	String usuario)
		throws AppException {
		AccesoDB lobdConexion = new AccesoDB();
		String lsQry ="";
		boolean lbOK = true;
		try {
			lobdConexion.conexionDB();
			for (int j=0; j < esCveTasa.length; j++) {
				if(!esCveTasa[j].equals("")){
					lsQry =" DELETE FROM com_tasa_if_pyme "+
							 " WHERE ic_tasa = "+esCveTasa[j]+" "+
							 "   AND ic_producto_nafin = "+ic_producto_nafin+" "+
							 "   AND ic_pyme = "+ic_pyme+" "+
							 "   AND ic_if = "+ic_if+" ";

					System.out.println(lsQry);
					try{
						lobdConexion.ejecutaSQL(lsQry);
					} catch (SQLException sqle){
						lbOK = false;
						throw new AppException("Ocurrio un error al eliminar la informaci�n"+sqle);
					}
				}
			}
		} catch (AppException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en eliminaTasaDisxPyme(). "+e);
			throw new AppException("Ocurrio un error al eliminar la informaci�n"+e);
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return lbOK;
	}
//Termina modificacion de tipoCredito Factoraje con recurso
	public Vector getComboPlazoAmort(String in)
		throws NafinException{
		return getComboPlazoAmort(in,"1,54");
	}

	public Vector getComboPlazoAmort(String in, String monedas)
		throws NafinException{
		return getComboPlazoAmort(in, monedas, "C");
	}

	public Vector getComboPlazoAmort(String in, String monedas, String tipoLinea)
			throws NafinException{
		AccesoDB    con           	= null;
		String      qrySentencia 	= "";
    	ResultSet   rs				= null;
		Vector		vecFilas		= null;
		Vector		vecColumnas		= null;
    	try {
      		con = new AccesoDB();
        	con.conexionDB();

			if(monedas==null||monedas.equals(""))
				monedas = "1,54";
			if(!"E".equals(tipoLinea)) {
				qrySentencia =
					" SELECT cp.ic_plazo, cp.in_plazo_meses"   +
					"   FROM comcat_plazo cp,"   +
					"        (SELECT cp.in_plazo_meses"   +
					"           FROM comcat_producto_nafin cpn, comcat_plazo cp"   +
					"          WHERE cp.ic_plazo = cpn.ig_numero_min_amort"   +
					"            AND cpn.ic_producto_nafin = 5) t1,"   +
					"        (SELECT cp.in_plazo_meses"   +
					"           FROM comcat_producto_nafin cpn, comcat_plazo cp"   +
					"          WHERE cp.ic_plazo = cpn.ic_numero_max_amort"   +
					"            AND cpn.ic_producto_nafin = 5) t2"   +
					"  WHERE cp.in_plazo_meses BETWEEN t1.in_plazo_meses AND t2.in_plazo_meses"   +
					"    AND cp.ic_producto_nafin IS NULL"   +
					"     AND cp.ic_plazo "+in+" (SELECT tg.ic_plazo"   +
					"                               FROM com_tasa_general tg"   +
					" 							  	   ,comcat_tasa t"   +
					"                              WHERE tg.ic_tasa = t.ic_tasa"   +
					" 							 and t.ic_moneda in("+monedas+") "   +
					" 							 and tg.ic_producto_nafin = 5)"   +
					"  ORDER BY 2"  ;
			} else {
				qrySentencia =
					" SELECT   cp.ic_plazo, cp.in_plazo_meses"   +
					"     FROM comcat_plazo cp,"   +
					"          (SELECT cp.in_plazo_meses"   +
					"             FROM comcat_producto_nafin cpn, comcat_plazo cp"   +
					"            WHERE cp.ic_plazo = cpn.ig_numero_max_amort_exp"   +
					"              AND cpn.ic_producto_nafin = 5) t1"   +
					"    WHERE cp.in_plazo_meses <= t1.in_plazo_meses"   +
					"      AND cp.ic_producto_nafin IS NULL"   +
					"      AND cp.ic_plazo "+in+" ("   +
					"             SELECT tg.ic_plazo"   +
					"               FROM com_tasa_general tg, comcat_tasa t"   +
					"              WHERE tg.ic_tasa = t.ic_tasa"   +
					"                AND t.ic_moneda IN ("+monedas+")"   +
					"                AND tg.ic_producto_nafin = 5)"   +
					" ORDER BY 2"  ;
			}
			rs = con.queryDB(qrySentencia);
			vecFilas = new Vector();
			while(rs.next()){
				vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
/*1*/			vecColumnas.add((rs.getString("IN_PLAZO_MESES")==null)?"":rs.getString("IN_PLAZO_MESES"));
				vecFilas.add(vecColumnas);
			}
			con.cierraStatement();
      	} catch(Exception e){
      		System.out.println("Exception: getComboPlazoAmort"+e);
			throw new NafinException("SIST0001");
		} finally {
      		if(con.hayConexionAbierta())
	       		con.cierraConexionDB();
      	}
		return vecFilas;
	}

	//UTILIZADO
	public Vector ovgetTasasPreferNego(String ic_producto_nafin, String cg_tipo, String ic_if, String ic_epo, String ic_moneda, String ic_pyme, String ic_plazo, String tipoCredito)
    		throws NafinException {
		System.out.println("CapturaTasasEJB::ovgetTasasPreferNego(E)");

		AccesoDB lobdConexion	= new AccesoDB();
		Vector ovDatosTasa 		= null;
		Vector ovRegistrosTasas	= new Vector();
		PreparedStatement	ps	= null;
		String campos = "";
		String tablas = "";
		String condicion = "";
		try{
			lobdConexion.conexionDB();
			if(!"".equals(ic_moneda))
				condicion += "    AND ct.ic_moneda = ? ";
			if("P".equals(cg_tipo)) {
				campos =
			        "       ,DECODE ( "+
			        "          tie.cg_rel_mat, "+
			        "          '+', (mt.fn_valor + tie.fn_puntos) - nvl(pre.fn_puntos,0), "+
			        "          '-', (mt.fn_valor - tie.fn_puntos) - nvl(pre.fn_puntos,0), "+
			        "         '*', (mt.fn_valor * tie.fn_puntos) - nvl(pre.fn_puntos,0), "+
			        "          '/', (mt.fn_valor / tie.fn_puntos) - nvl(pre.fn_puntos,0), "+
			        "          0 "+
			        "       ) AS tasafinal, pre.fn_puntos as puntospref ,'' AS fechaNego " +
					"		,'P' as tipoTasaBPN ";//, "+
					//"	'Modalidad 2 (Riesgo Distribuidor)' as tipo_credito ";//MOD +(all)

				tablas =
					"        ,(SELECT fn_puntos,ic_moneda, ic_plazo,cg_tipos_credito"   +
					"           FROM dis_tasa_prefer_nego"   +
					"          WHERE ic_pyme = ? "   +
					"            AND ic_producto_nafin = ?"   +
					"            AND ic_epo = ?"   +
					"            AND ic_if = ?";
				if(!"".equals(ic_moneda))
					tablas +=
						 "            AND ic_moneda = ?";
				tablas +=((tipoCredito.equals("C")?" and CG_TIPOS_CREDITO = 'C'  ":" and CG_TIPOS_CREDITO = 'D' "))+" "   ;
				tablas +="            AND cg_tipo = 'P') pre"  ;
				condicion +=
					"    AND pre.fn_puntos(+) > 0"  +
   					"	 AND pre.ic_moneda(+) = ct.ic_moneda"  +
   					"	 AND pre.ic_plazo(+) = ct.ic_plazo" +
					"	 AND ct.ic_tasa NOT IN (SELECT neg.ic_tasa"   +
					"	   FROM dis_tasa_prefer_nego neg"   +
					"	  WHERE neg.ic_pyme = pym.ic_pyme"   +
					"	    AND neg.ic_producto_nafin = tp.ic_producto_nafin"   +
					"	    AND neg.ic_epo = epo.ic_epo"   +
					"	    AND neg.ic_if = tie.ic_if"   +
					"	    AND neg.ic_moneda = cm.ic_moneda"   +
					"	    AND neg.cg_tipo = '"+cg_tipo+"'"   +
					"	    AND TRUNC (neg.df_fecha) = TRUNC (SYSDATE))";



			}
			else {
				if("N".equals(cg_tipo)) {
					campos =
						"		 ,tpn.fn_valor AS tasafinal, '' as puntospref, TO_CHAR(tpn.df_fecha,'dd/mm/yyyy') AS fechaNego "+
						"		,'N' as tipoTasaBPN ";//, "+
						//"	'Modalidad 2 (Riesgo Distribuidor) ' as tipo_credito ";//MOD +(all);

					tablas =
						"        ,dis_tasa_prefer_nego tpn ";

					condicion +=
						"    AND ct.ic_tasa = tpn.ic_tasa"   +
						"    AND tpn.ic_producto_nafin = tp.ic_producto_nafin"   +
						"    AND tpn.ic_if = tie.ic_if"   +
						((tipoCredito.equals("C")?"  ":" and  tpn.ic_epo = tie.ic_epo "))+" "   +
						"    AND tpn.ic_moneda = ct.ic_moneda"   +
						"    AND tpn.ic_pyme = pym.ic_pyme"   +
						"    AND tpn.cg_tipo = 'N'"   +
						"    AND TRUNC (df_fecha) = TRUNC (SYSDATE)"  ;
				}
			}
			if("".equals(campos)) {
				campos =
			        "       ,DECODE ( "+
			        "          tie.cg_rel_mat, "+
			        "          '+', (mt.fn_valor + tie.fn_puntos), "+
			        "          '-', (mt.fn_valor - tie.fn_puntos), "+
			        "         '*', (mt.fn_valor * tie.fn_puntos), "+
			        "          '/', (mt.fn_valor / tie.fn_puntos), "+
			        "          0 "+
			        "       ) "+
			        "             AS tasafinal, '' as puntospref ,'' as fechaNego"+
					"		,'B' as tipoTasaBPN " ;

			}

			if(!"".equals(ic_plazo))
				condicion += "  AND cp.ic_plazo = ? ";

			String lsQry =
					 " SELECT  /*+index(mt CP_COM_MANT_TASA_PK , tie CF_COMREL_IF_EPO_X_PROD_02_FK )*/ "+
		       "  cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
		        "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
		        "       mt.fn_valor AS valor, tie.cg_rel_mat AS relmat, "+
		        "       tie.fn_puntos AS puntos, "+
		        "       DECODE ( "+
		        "          tie.cg_rel_mat, "+
		        "          '+', (mt.fn_valor + tie.fn_puntos), "+
		        "          '-', (mt.fn_valor - tie.fn_puntos), "+
		        "         '*', (mt.fn_valor * tie.fn_puntos), "+
		        "          '/', (mt.fn_valor / tie.fn_puntos), "+
		        "          0 "+
		        "       ) "+
		        "             AS tasapiso, "+
		        "       TO_CHAR (tie.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo, "+
		        "       cm.ic_moneda,cp.in_plazo_dias "+
				" 		,epo.cg_razon_social AS nombreEpo "+
				" 		,pym.cg_razon_social AS nombrePyme "+campos+" "+
				"   FROM comcat_pyme pym,"   +
				"        comcat_epo epo,"   +

				"  "+((tipoCredito.equals("C"))?" com_tasa_if_pyme ":" com_tasa_if_epo ") +" tie ,"+//     com_tasa_if_epo tie,"   +

				"        com_mant_tasa mt,"   +
				"        comcat_tasa ct,"   +
				"        comrel_tasa_producto tp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_plazo cp "+tablas+" "+
				"  WHERE "+((tipoCredito.equals("C"))?" ":" epo.ic_epo = tie.ic_epo  and") +
				"    ct.ic_tasa = tie.ic_tasa"   +
				"    AND ct.ic_plazo = cp.ic_plazo"   +
				"    AND ct.ic_tasa = mt.ic_tasa"   +
				"    AND mt.ic_tasa = tp.ic_tasa"   +
				"    AND ct.ic_tasa = tp.ic_tasa"   +
				"    AND cm.ic_moneda = ct.ic_moneda"   +
				"    AND tie.ic_producto_nafin = tp.ic_producto_nafin"   +
				"    AND mt.dc_fecha = (SELECT MAX (mi.dc_fecha)"   +
				"                         FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp"   +
				"                        WHERE mi.ic_tasa = mt.ic_tasa"   +
				"                          AND ti.ic_tasa = mi.ic_tasa"   +
				"                          AND ti.ic_tasa = ctp.ic_tasa"   +
				"                          AND ctp.ic_producto_nafin = tp.ic_producto_nafin)"   +
				"    AND pym.ic_pyme = ?"   +
				"    AND tp.ic_producto_nafin = ?"   +
				"  "+((tipoCredito.equals("C"))?" AND tie.ic_pyme =pym.ic_pyme and epo.ic_epo = ? ":"    AND tie.ic_epo = ? ") +" "+//     com_tasa_if_epo tie,"   +
				//"    AND tie.ic_epo = ?"   +
				"    AND tie.ic_if = ?"+condicion+" "+
				"  ORDER BY cm.ic_moneda, cp.ic_plazo"  ;
			ps = lobdConexion.queryPrecompilado(lsQry);
System.out.println("\n **** \n **** \n ovgetTasasPreferNego.lsQry: "+lsQry);
			ps.setInt(1,Integer.parseInt(ic_pyme));
			ps.setInt(2,Integer.parseInt(ic_producto_nafin));
			ps.setInt(3,Integer.parseInt(ic_epo));
			ps.setInt(4,Integer.parseInt(ic_if));
			int cont = 4;
			if(!"".equals(ic_moneda)) {
				cont++;ps.setInt(cont,Integer.parseInt(ic_moneda));
			}
			if("P".equals(cg_tipo)) {
				cont++;ps.setInt(cont,Integer.parseInt(ic_pyme));
				cont++;ps.setInt(cont,Integer.parseInt(ic_producto_nafin));
				cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
				cont++;ps.setInt(cont,Integer.parseInt(ic_if));
				if(!"".equals(ic_moneda)) {
					cont++;ps.setInt(cont,Integer.parseInt(ic_moneda));
				}
			}
			if(!"".equals(ic_plazo)){
				cont++;ps.setInt(cont,Integer.parseInt(ic_plazo));
			}

			ResultSet lrsQry = ps.executeQuery();
			ps.clearParameters();
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
/*0*/				ovDatosTasa.add(lrsQry.getString("moneda"));
/*1*/				ovDatosTasa.add(lrsQry.getString("tipotasa"));
/*2*/				ovDatosTasa.add(lrsQry.getString("cvetasa"));
/*3*/				ovDatosTasa.add(lrsQry.getString("plazo"));
/*4*/				ovDatosTasa.add(lrsQry.getString("valor"));
/*5*/				ovDatosTasa.add(lrsQry.getString("relmat"));
/*6*/				ovDatosTasa.add(lrsQry.getString("puntos"));
/*7*/				ovDatosTasa.add(lrsQry.getString("tasapiso"));
/*8*/				ovDatosTasa.add(lrsQry.getString("fecha"));
/*9*/				ovDatosTasa.add(lrsQry.getString("nombreEpo"));
/*10*/				ovDatosTasa.add(lrsQry.getString("nombrePyme").replaceAll(",","") );
/*11*/				ovDatosTasa.add(lrsQry.getString("tasafinal"));
/*12*/				ovDatosTasa.add(lrsQry.getString("fechaNego"));
/*13*/				ovDatosTasa.add(lrsQry.getString("ic_moneda"));
/*14*/				ovDatosTasa.add(lrsQry.getString("in_plazo_dias"));
/*15*/				ovDatosTasa.add(lrsQry.getString("puntospref"));
/*16*/				ovDatosTasa.add(lrsQry.getString("tipoTasaBPN"));
/*17*/				//ovDatosTasa.add(lrsQry.getString("tipo_credito"));
/* */				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lrsQry.close();
			if(ps!=null) ps.close();

		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasasPreferNego(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
			System.out.println("CapturaTasasEJB::ovgetTasasPreferNego(S)");
		}
		return ovRegistrosTasas;
	}
//<<<<Israel Herrera

	/*********************************************************************************************
	*	    ArrayList getComboPlazo()
	*       Agregado 03/08/2004	--HDG
	*********************************************************************************************/
    public ArrayList getComboPlazo(int iNoProductoNafin, int iNoMoneda) throws NafinException {
	String sQuery = "";
    ArrayList alRegistro = null;
    ArrayList alDatos = new ArrayList();
    boolean bOk = true;
	AccesoDB con  = new AccesoDB();
        try{
        	con.conexionDB();

		   	sQuery =" select P.ic_plazo, P.in_plazo_dias "+
					" from comrel_tasa_producto tp, comcat_tasa t, comcat_plazo p "+
					" where tp.ic_tasa = t.ic_tasa "+
					" and t.ic_plazo = p.ic_plazo "+
	   				" and tp.ic_producto_nafin = "+iNoProductoNafin+
		   			" and t.ic_moneda = " + iNoMoneda +
		   			" and t.cs_disponible = 'S' "+
					" order by p.ic_plazo ";
			ResultSet rsResultado = con.queryDB(sQuery);
			while (rsResultado.next()) {
                alRegistro = new ArrayList();
                bOk = alRegistro.add((rsResultado.getString(1)==null)?"":rsResultado.getString(1));
                bOk = alRegistro.add((rsResultado.getString(2)==null)?"":rsResultado.getString(2));
                bOk = alDatos.add(alRegistro);
		   }
		   rsResultado.close();
	   	   con.cierraStatement();
		} catch (Exception e) {
			System.out.println("Error en getComboPlazo: "+e.getMessage());
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) con.cierraConexionDB();
		}
		return alDatos;
    }

	/*********************************************************************************************
	*	    ArrayList getTasasPreferenciales()
	*       Agregado 03/08/2004	--HDG
	*********************************************************************************************/

	public ArrayList getTasasPreferenciales(int iNoProductoNafin, int iNoMoneda) throws NafinException {
	AccesoDB con = new AccesoDB();
	ArrayList alDatosTasa = null;
	ArrayList alRegistrosTasas = new ArrayList();
    boolean bOk = true;
		try{
			con.conexionDB();
			String lsQry =	" SELECT M.cd_nombre AS moneda, T.cd_nombre AS tipotasa,"   +
							"        T.ic_tasa AS cvetasa, P.cg_descripcion AS plazo,"   +
							"        MT.fn_valor AS valor, TG.cg_rel_mat AS relmat,"   +
							"        TG.fn_puntos AS puntos,"   +
							"        DECODE ("   +
							"           TG.cg_rel_mat,"   +
							"           '+', (MT.fn_valor + TG.fn_puntos),"   +
							"           '-', (MT.fn_valor - TG.fn_puntos),"   +
							"           '*', (MT.fn_valor * TG.fn_puntos),"   +
							"           '/', (MT.fn_valor / TG.fn_puntos),"   +
							"           0   ) AS tasapiso,"   +
							"        TO_CHAR (TG.df_captura, 'dd/mm/yyyy') AS fecha, P.ic_plazo,"   +
							"        M.ic_moneda"   +
							"   FROM (SELECT CTG.* FROM com_tasa_general CTG, "+
							"                          (SELECT MAX(ic_tasa_general) as ic_tasa_general, "+
							"                           MAX(df_captura), ic_tasa "+
							"                           FROM com_tasa_general "+
							"                           WHERE ic_producto_nafin = "+iNoProductoNafin+
							"                           GROUP BY ic_tasa) VTG "+
							"         WHERE CTG.ic_tasa_general = VTG.ic_tasa_general) TG,"   +
							"        com_mant_tasa MT,"   +
							"        comcat_plazo P,"   +
							"        comcat_tasa T,"   +
							"        comcat_moneda M,"   +
							"        comrel_tasa_producto TP"   +
							"  WHERE T.ic_tasa = TG.ic_tasa"   +
							"    AND T.ic_plazo = P.ic_plazo"   +
							"    AND T.ic_tasa = MT.ic_tasa"   +
							"    AND MT.ic_tasa = TP.ic_tasa"   +
							"    AND T.ic_tasa = TP.ic_tasa"   +
							"    AND M.ic_moneda = T.ic_moneda"   +
							"    AND T.ic_moneda = "   +iNoMoneda+
							"    AND TP.ic_producto_nafin = "   +iNoProductoNafin+
							"    AND MT.dc_fecha = (SELECT MAX (mi.dc_fecha)"   +
							"                       FROM com_mant_tasa mi"   +
							"                       WHERE mi.ic_tasa = MT.ic_tasa)"   +
							"  ORDER BY M.ic_moneda, P.ic_plazo"  ;
			//System.out.println(lsQry);
			ResultSet rsQry = con.queryDB(lsQry);
			while(rsQry.next()) {
				alDatosTasa = new ArrayList();
				bOk = alDatosTasa.add(rsQry.getString("moneda"));
				bOk = alDatosTasa.add(rsQry.getString("tipotasa"));
				bOk = alDatosTasa.add(rsQry.getString("cvetasa"));
				bOk = alDatosTasa.add(rsQry.getString("plazo"));
				bOk = alDatosTasa.add(rsQry.getString("valor"));
				bOk = alDatosTasa.add(rsQry.getString("relmat"));
				bOk = alDatosTasa.add(rsQry.getString("puntos"));
				bOk = alDatosTasa.add(rsQry.getString("tasapiso"));
				bOk = alDatosTasa.add(rsQry.getString("fecha"));
				bOk = alRegistrosTasas.add(alDatosTasa);
			}
			con.cierraStatement();

		} catch(Exception e){
			System.out.println("Exception ocurrida en getTasasPreferenciales(). "+e.getMessage());
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}
		finally	{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return alRegistrosTasas;
	}

	/*********************************************************************************************
	*	    ArrayList ovgetTasaNegociada()
	*       Agregado 04/08/2004	--HDG
	*********************************************************************************************/
	public ArrayList getTasaNegociada(int iNoProductoNafin, String sNoEpo, String sNoIf,
										 String sNoPyme, String sNoPlazo, int iNoMoneda) throws NafinException {
	AccesoDB con = new AccesoDB();
	ArrayList alDatosTasa = null;
	ArrayList alRegistrosTasas = new ArrayList();
	String sQuery = "";
	ResultSet rsQry = null;
	boolean bOk = true;
	String	valor = "";
		try{
			con.conexionDB();
			sQuery = " SELECT tpn.fn_valor"   +
					" FROM dis_tasa_prefer_nego tpn, comcat_tasa ct"   +
					" WHERE ct.ic_tasa = tpn.ic_tasa "   +
					" AND tpn.ic_epo = "  +sNoEpo+
					" AND tpn.ic_pyme = "   +sNoPyme+
					" AND tpn.ic_if = "   +sNoIf+
					" AND tpn.ic_producto_nafin = "   +iNoProductoNafin+
					" AND tpn.cg_tipo = 'N'"   +
					" AND ct.ic_plazo = "+sNoPlazo;
			rsQry = con.queryDB(sQuery);
			if(rsQry.next()){
				valor = rsQry.getString("fn_valor");
			}
			con.cierraStatement();

			sQuery =" SELECT M.cd_nombre AS moneda, T.cd_nombre AS tipotasa, "   +
					" T.ic_tasa AS cvetasa, P.cg_descripcion AS plazo,        "   +
					" MT.fn_valor AS valor, TG.cg_rel_mat AS relmat, TG.fn_puntos AS puntos,        "   +
					" DECODE(TG.cg_rel_mat, '+', (MT.fn_valor + TG.fn_puntos), "   +
					"                        '-', (MT.fn_valor - TG.fn_puntos), "   +
					"                        '*', (MT.fn_valor * TG.fn_puntos), "   +
					"                        '/', (MT.fn_valor / TG.fn_puntos), 0) AS tasapiso,"   +
					" TO_CHAR (TG.df_captura, 'dd/mm/yyyy') AS fecha, P.ic_plazo, M.ic_moneda"   +
					" FROM (SELECT CTG.* FROM com_tasa_general CTG, "+
					"                        (SELECT MAX(ic_tasa_general) as ic_tasa_general, "+
					"                         MAX(df_captura), ic_tasa "+
					"                         FROM com_tasa_general "+
					"                         WHERE ic_producto_nafin = "+iNoProductoNafin+
					"                         GROUP BY ic_tasa) VTG "+
      				"		WHERE CTG.ic_tasa_general = VTG.ic_tasa_general) TG, "+
					" com_mant_tasa MT, comcat_plazo P,"   +
					" comcat_tasa T, comcat_moneda M, comrel_tasa_producto TP"   +
					" WHERE T.ic_tasa = TG.ic_tasa"   +
					" AND T.ic_plazo = P.ic_plazo"   +
					" AND T.ic_tasa = MT.ic_tasa"   +
					" AND MT.ic_tasa = TP.ic_tasa"   +
					" AND T.ic_tasa = TP.ic_tasa"   +
					" AND M.ic_moneda = T.ic_moneda"   +
					" AND TP.ic_producto_nafin = "   +iNoProductoNafin+
					" AND T.ic_moneda = "   +iNoMoneda+
					" AND P.ic_plazo = "   +sNoPlazo+
					" AND MT.dc_fecha = (SELECT MAX(VMT.dc_fecha) "   +
					"                    FROM com_mant_tasa VMT"   +
					"                    WHERE VMT.ic_tasa = MT.ic_tasa)  "   +
					" ORDER BY M.ic_moneda, P.ic_plazo"  ;

	      	System.out.println(sQuery);
			rsQry = con.queryDB(sQuery);
			while(rsQry.next()) {
				alDatosTasa = new ArrayList();
				bOk = alDatosTasa.add(rsQry.getString("moneda"));
				bOk = alDatosTasa.add(rsQry.getString("tipotasa"));
				bOk = alDatosTasa.add(rsQry.getString("cvetasa"));
				bOk = alDatosTasa.add(rsQry.getString("plazo"));
				bOk = alDatosTasa.add(rsQry.getString("valor"));
				bOk = alDatosTasa.add(rsQry.getString("relmat"));
				bOk = alDatosTasa.add(rsQry.getString("puntos"));
				bOk = alDatosTasa.add(rsQry.getString("tasapiso"));
				bOk = alDatosTasa.add(rsQry.getString("fecha"));
				bOk = alDatosTasa.add(valor);
				bOk = alRegistrosTasas.add(alDatosTasa);
			}
			con.cierraStatement();

		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasaNegociada(). "+e.getMessage());
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}
		finally	{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
	return alRegistrosTasas;
	}

	public Acuse guardaTasaPreferencialCred(
					String ic_pyme, String ic_if, String ic_moneda,
					String cg_rel_mat, String fn_puntos, String ic_producto_nafin,
					String ic_usuario, String cg_recibo_electronico) throws NafinException {
		System.out.println("CapturaTasasEJB::guardaTasaPreferencialCred(E)");
		AccesoDB			con 			= null;
		PreparedStatement	ps				= null;
		ResultSet			rs 				= null;
		String				qrySentencia	= "";
		Acuse 				acuse 			= new Acuse(3, "5");
		boolean				bOK				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			String cc_acuse = "C"+acuse.toString();
			qrySentencia =
				" SELECT ic_tasa_prefer_nego"   +
				"   FROM dis_tasa_prefer_nego"   +
				"  WHERE cg_tipo = 'P'"   +
				"    AND ic_pyme = ?"   +
				"    AND ic_if = ?"   +
				"    AND ic_moneda = ?"   +
				"    AND ic_producto_nafin = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_pyme));
			ps.setInt(2, Integer.parseInt(ic_if));
			ps.setInt(3, Integer.parseInt(ic_moneda));
			ps.setInt(4, Integer.parseInt(ic_producto_nafin));
			rs = ps.executeQuery();
			int ic_tasa_prefer_nego = 0;
			if(rs.next()) {
				ic_tasa_prefer_nego = rs.getInt(1);
			}
			rs.close();ps.close();

			qrySentencia =
				" INSERT INTO com_acuse3"   +
				"             (cc_acuse, df_fecha_hora, ic_usuario, cg_recibo_electronico, ic_producto_nafin)"   +
				"      VALUES (?, SYSDATE, ?, ?, ?)"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, cc_acuse);
			ps.setString(2, ic_usuario);
			ps.setString(3, cg_recibo_electronico);
			ps.setInt(4, Integer.parseInt(ic_producto_nafin));
			ps.execute();
			ps.close();

			if(ic_tasa_prefer_nego>0){
				qrySentencia =
					" UPDATE dis_tasa_prefer_nego"   +
					"    SET fn_puntos = ?,"   +
					"        cg_rel_mat = ?,"   +
					"        cc_acuse = ?"   +
					"  WHERE ic_tasa_prefer_nego = ?"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setDouble(1, Double.parseDouble(fn_puntos));
				ps.setString(2, cg_rel_mat);
				ps.setString(3, cc_acuse);
				ps.setInt(4, ic_tasa_prefer_nego);
				ps.execute();
			} else {
				qrySentencia =
					" INSERT INTO dis_tasa_prefer_nego"   +
					"             (ic_tasa_prefer_nego, ic_producto_nafin, ic_pyme, ic_if, fn_puntos, cg_rel_mat, cg_tipo, ic_moneda, cc_acuse)"   +
					"    SELECT NVL (MAX (ic_tasa_prefer_nego), 0) + 1, 5, ?, ?, ?, ?, 'P', ?, ?"   +
					"      FROM dis_tasa_prefer_nego"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1, Integer.parseInt(ic_pyme));
				ps.setInt(2, Integer.parseInt(ic_if));
				ps.setDouble(3, Double.parseDouble(fn_puntos));
				ps.setString(4, cg_rel_mat);
				ps.setInt(5, Integer.parseInt(ic_moneda));
				ps.setString(6, cc_acuse);
				ps.execute();
			}
		}catch(Exception e){
			bOK = false;
			System.out.println("CapturaTasasEJB::guardaTasaPreferencialCred(Exception) "+e);
			throw new NafinException("SIST0001");
		}finally{
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("CapturaTasasEJB::guardaTasaPreferencialCred(S)");
		}
		return acuse;
	}

	public List getTasasPrefCred(String ic_producto_nafin, String ic_if, String ic_pyme, String ic_moneda, String cg_tipo) throws NafinException {
		System.out.println("CapturaTasasEJB::getTasasPrefCred(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		List				columnas		= null;
		List				renglones		= new ArrayList();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT cg_rel_mat, fn_puntos"   +
				"   FROM dis_tasa_prefer_nego"   +
				"  WHERE ic_producto_nafin = ?"   +
				"    AND ic_if = ?"   +
				"    AND ic_pyme = ?"   +
				"    AND ic_moneda = ?"   +
				"    AND cg_tipo = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_producto_nafin));
			ps.setInt(2, Integer.parseInt(ic_if));
			ps.setInt(3, Integer.parseInt(ic_pyme));
			ps.setInt(4, Integer.parseInt(ic_moneda));
			ps.setString(5, cg_tipo);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				String rs_rel_mat	= (rs.getString(1)==null)?"":rs.getString(1);
				String rs_puntos	= (rs.getString(2)==null)?"0":rs.getString(2);
				renglones.add(rs_rel_mat);
				renglones.add(rs_puntos);
			}
			rs.close();ps.close();
		} catch(Exception e) {
			System.out.println("CapturaTasasEJB::getTasasPrefCred(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("CapturaTasasEJB::getTasasPrefCred(S)");
		}
		return renglones;
	}

	public List getTasasCredicadenas(String ic_producto_nafin, String ic_if, String ic_pyme[], String ic_moneda, String tipo_tasa) throws NafinException {
		System.out.println("CapturaTasasEJB::getTasasCredicadenas(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		List 				lTasa			= null;
		List				columnas		= null;
		List				renglones		= new ArrayList();
		String				cg_rel_mat 		= "";
		String				fn_puntos 		= "";
		try {
			con = new AccesoDB();
			con.conexionDB();

			int cTasas = 1;
			if("P".equals(tipo_tasa))
				cTasas = ic_pyme.length;
			String rs_pyme = "";
			for(int t=0;t<cTasas;t++) {
				if("P".equals(tipo_tasa)) {
					qrySentencia =
						" SELECT cg_razon_social"   +
						"   FROM comcat_pyme"   +
						"  WHERE ic_pyme = ?"  ;
					ps = con.queryPrecompilado(qrySentencia);
					ps.setInt(1, Integer.parseInt(ic_pyme[t]));
					rs = ps.executeQuery();
					ps.clearParameters();
					if(rs.next()) {
						rs_pyme = rs.getString(1)==null?"":rs.getString(1);
					}
					rs.close();ps.close();
					lTasa = getTasasPrefCred(ic_producto_nafin, ic_if, ic_pyme[t], ic_moneda, "P");
					if(lTasa.size()>0) {
						cg_rel_mat	= lTasa.get(0).toString();
						fn_puntos	= lTasa.get(1).toString();
					}
				}
				if(!("P".equals(tipo_tasa)&&lTasa.size()==0)) {
					Vector vTasas = ovgetTasasInvent(5, ic_moneda);
					for(int i=0;i<vTasas.size();i++) {
						Vector vDatos = (Vector)vTasas.get(i);
						//String rs_moneda		= vDatos.get(0).toString();
						String rs_nombre_tasa 	= vDatos.get(1).toString();
						//String rs_tasa 			= vDatos.get(2).toString();
						String rs_plazo 		= vDatos.get(3).toString();
						String rs_valor_tasa 	= vDatos.get(4).toString();
						String rs_rel_mat 		= vDatos.get(5).toString();
						String rs_puntos 		= vDatos.get(6).toString();
						String rs_valor 		= vDatos.get(7).toString();
						String rs_tipo_tasa		= rs_nombre_tasa+" ("+rs_valor_tasa+") "+rs_rel_mat+" "+rs_puntos;
						String rs_referencia = "Base";
						if("P".equals(tipo_tasa)) {
							rs_referencia = "Preferencial";
							rs_tipo_tasa += " "+cg_rel_mat+" "+fn_puntos;

							double valorTasaPref = 0;
							if("+".equals(cg_rel_mat))
								valorTasaPref = Double.parseDouble(rs_valor) + Double.parseDouble(fn_puntos);
							else if("-".equals(cg_rel_mat))
								valorTasaPref = Double.parseDouble(rs_valor) - Double.parseDouble(fn_puntos);

							rs_valor = String.valueOf(valorTasaPref);
						}
						columnas = new ArrayList();
						columnas.add(rs_pyme);
						columnas.add(rs_referencia);
						columnas.add(rs_plazo);
						columnas.add(rs_tipo_tasa);
						columnas.add(rs_valor);
						renglones.add(columnas);
					}//for(int i=0;i<vTasas.size();i++)
				}//if(!("P".equals(tipo_tasa)&&lTasa.size()==0))
			}//for(int t=0;t<cTasas;t++)
		} catch(Exception e) {
			System.out.println("CapturaTasasEJB::getTasasCredicadenas(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("CapturaTasasEJB::getTasasCredicadenas(S)");
		}
		return renglones;
	}

	/***************************************************************/

	public Vector ovgetTasasPreferNego(String ic_producto_nafin, String cg_tipo, String ic_if, String ic_epo, String ic_moneda, String ic_pyme)
    		throws NafinException {
		System.out.println("CapturaTasasEJB::ovgetTasasPreferNego(E)");
		AccesoDB lobdConexion	= new AccesoDB();
		Vector ovDatosTasa 		= null;
		Vector ovRegistrosTasas	= new Vector();
		PreparedStatement	ps	= null;
		String campos = "";
		String tablas = "";
		String condicion = "";
		try{
			lobdConexion.conexionDB();
			if(!"".equals(ic_moneda))
				condicion += "    AND ct.ic_moneda = ? ";
			if("P".equals(cg_tipo)) {
				campos =
			        "       ,DECODE ( "+
			        "          tie.cg_rel_mat, "+
			        "          '+', (mt.fn_valor + tie.fn_puntos) - pre.fn_puntos, "+
			        "          '-', (mt.fn_valor - tie.fn_puntos) - pre.fn_puntos, "+
			        "         '*', (mt.fn_valor * tie.fn_puntos) - pre.fn_puntos, "+
			        "          '/', (mt.fn_valor / tie.fn_puntos) - pre.fn_puntos, "+
			        "          0 "+
			        "       ) AS tasafinal, pre.fn_puntos as puntospref ,'' AS fechaNego " +
					"		,'P' as tipoTasaBPN " ;

				tablas =
					"        ,(SELECT fn_puntos,ic_moneda"   +
					"           FROM dis_tasa_prefer_nego"   +
					"          WHERE ic_pyme = ? "   +
					"            AND ic_producto_nafin = ?"   +
					"            AND ic_epo = ?"   +
					"            AND ic_if = ?";
				if(!"".equals(ic_moneda))
					tablas +=
						 "            AND ic_moneda = ?";
				tablas +=
					"            AND cg_tipo = 'P') pre"  ;
				condicion +=
					"    AND pre.fn_puntos > 0"  +
   					"	 AND pre.ic_moneda = ct.ic_moneda"  +
					"	 AND ct.ic_tasa NOT IN (SELECT neg.ic_tasa"   +
					"	   FROM dis_tasa_prefer_nego neg"   +
					"	  WHERE neg.ic_pyme = pym.ic_pyme"   +
					"	    AND neg.ic_producto_nafin = tp.ic_producto_nafin"   +
					"	    AND neg.ic_epo = epo.ic_epo"   +
					"	    AND neg.ic_if = tie.ic_if"   +
					"	    AND neg.ic_moneda = cm.ic_moneda"   +
					"	    AND neg.cg_tipo = 'N'"   +
					"	    AND TRUNC (neg.df_fecha) = TRUNC (SYSDATE))"  ;


			}
			else {
				if("N".equals(cg_tipo)) {
					campos =
						"		 ,tpn.fn_valor AS tasafinal, '' as puntospref, TO_CHAR(tpn.df_fecha,'dd/mm/yyyy') AS fechaNego "+
						"		,'N' as tipoTasaBPN " ;

					tablas =
						"        ,dis_tasa_prefer_nego tpn ";

					condicion +=
						"    AND ct.ic_tasa = tpn.ic_tasa"   +
						"    AND tpn.ic_producto_nafin = tp.ic_producto_nafin"   +
						"    AND tpn.ic_if = tie.ic_if"   +
						"    AND tpn.ic_epo = tie.ic_epo"   +
						"    AND tpn.ic_moneda = ct.ic_moneda"   +
						"    AND tpn.ic_pyme = pym.ic_pyme"   +
						"    AND tpn.cg_tipo = 'N'"   +
						"    AND TRUNC (df_fecha) = TRUNC (SYSDATE)"  ;
				}
			}
			if("".equals(campos)) {
				campos =
			        "       ,DECODE ( "+
			        "          tie.cg_rel_mat, "+
			        "          '+', (mt.fn_valor + tie.fn_puntos), "+
			        "          '-', (mt.fn_valor - tie.fn_puntos), "+
			        "         '*', (mt.fn_valor * tie.fn_puntos), "+
			        "          '/', (mt.fn_valor / tie.fn_puntos), "+
			        "          0 "+
			        "       ) "+
			        "             AS tasafinal, '' as puntospref ,'' as fechaNego"+
					"		,'B' as tipoTasaBPN " ;

			}
			String lsQry =
		        " SELECT cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
		        "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
		        "       mt.fn_valor AS valor, tie.cg_rel_mat AS relmat, "+
		        "       tie.fn_puntos AS puntos, "+
		        "       DECODE ( "+
		        "          tie.cg_rel_mat, "+
		        "          '+', (mt.fn_valor + tie.fn_puntos), "+
		        "          '-', (mt.fn_valor - tie.fn_puntos), "+
		        "         '*', (mt.fn_valor * tie.fn_puntos), "+
		        "          '/', (mt.fn_valor / tie.fn_puntos), "+
		        "          0 "+
		        "       ) "+
		        "             AS tasapiso, "+
		        "       TO_CHAR (tie.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo, "+
		        "       cm.ic_moneda,cp.in_plazo_dias "+
				" 		,epo.cg_razon_social AS nombreEpo "+
				" 		,pym.cg_razon_social AS nombrePyme "+campos+" "+
				"   FROM comcat_pyme pym,"   +
				"        comcat_epo epo,"   +
				"        com_tasa_if_epo tie,"   +
				"        com_mant_tasa mt,"   +
				"        comcat_tasa ct,"   +
				"        comrel_tasa_producto tp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_plazo cp "+tablas+" "+
				"  WHERE epo.ic_epo = tie.ic_epo"   +
				"    AND ct.ic_tasa = tie.ic_tasa"   +
				"    AND ct.ic_plazo = cp.ic_plazo"   +
				"    AND ct.ic_tasa = mt.ic_tasa"   +
				"    AND mt.ic_tasa = tp.ic_tasa"   +
				"    AND ct.ic_tasa = tp.ic_tasa"   +
				"    AND cm.ic_moneda = ct.ic_moneda"   +
				"    AND tie.ic_producto_nafin = tp.ic_producto_nafin"   +
				"    AND mt.dc_fecha = (SELECT MAX (mi.dc_fecha)"   +
				"                         FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp"   +
				"                        WHERE mi.ic_tasa = mt.ic_tasa"   +
				"                          AND ti.ic_tasa = mi.ic_tasa"   +
				"                          AND ti.ic_tasa = ctp.ic_tasa"   +
				"                          AND ctp.ic_producto_nafin = tp.ic_producto_nafin)"   +
				"    AND pym.ic_pyme = ?"   +
				"    AND tp.ic_producto_nafin = ?"   +
				"    AND tie.ic_epo = ?"   +
				"    AND tie.ic_if = ?"+condicion+" "+
				"  ORDER BY cm.ic_moneda, cp.ic_plazo"  ;
			ps = lobdConexion.queryPrecompilado(lsQry);
System.out.println("\n **** \n **** \n ovgetTasasPreferNego.lsQry: "+lsQry);
			ps.setInt(1,Integer.parseInt(ic_pyme));
			ps.setInt(2,Integer.parseInt(ic_producto_nafin));
			ps.setInt(3,Integer.parseInt(ic_epo));
			ps.setInt(4,Integer.parseInt(ic_if));
			int cont = 4;
			if(!"".equals(ic_moneda)) {
				cont++;ps.setInt(cont,Integer.parseInt(ic_moneda));
			}
			if("P".equals(cg_tipo)) {
				cont++;ps.setInt(cont,Integer.parseInt(ic_pyme));
				cont++;ps.setInt(cont,Integer.parseInt(ic_producto_nafin));
				cont++;ps.setInt(cont,Integer.parseInt(ic_epo));
				cont++;ps.setInt(cont,Integer.parseInt(ic_if));
				if(!"".equals(ic_moneda)) {
					cont++;ps.setInt(cont,Integer.parseInt(ic_moneda));
				}
			}
			ResultSet lrsQry = ps.executeQuery();
			ps.clearParameters();
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
/*0*/				ovDatosTasa.add(lrsQry.getString("moneda"));
/*1*/				ovDatosTasa.add(lrsQry.getString("tipotasa"));
/*2*/				ovDatosTasa.add(lrsQry.getString("cvetasa"));
/*3*/				ovDatosTasa.add(lrsQry.getString("plazo"));
/*4*/				ovDatosTasa.add(lrsQry.getString("valor"));
/*5*/				ovDatosTasa.add(lrsQry.getString("relmat"));
/*6*/				ovDatosTasa.add(lrsQry.getString("puntos"));
/*7*/				ovDatosTasa.add(lrsQry.getString("tasapiso"));
/*8*/				ovDatosTasa.add(lrsQry.getString("fecha"));
/*9*/				ovDatosTasa.add(lrsQry.getString("nombreEpo"));
/*10*/				ovDatosTasa.add(lrsQry.getString("nombrePyme"));
/*11*/				ovDatosTasa.add(lrsQry.getString("tasafinal"));
/*12*/				ovDatosTasa.add(lrsQry.getString("fechaNego"));
/*13*/				ovDatosTasa.add(lrsQry.getString("ic_moneda"));
/*14*/				ovDatosTasa.add(lrsQry.getString("in_plazo_dias"));
/*15*/				ovDatosTasa.add(lrsQry.getString("puntospref"));
/*16*/				ovDatosTasa.add(lrsQry.getString("tipoTasaBPN"));
/* */				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lrsQry.close();
			if(ps!=null) ps.close();

		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasasPreferNego(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
			System.out.println("CapturaTasasEJB::ovgetTasasPreferNego(S)");
		}
		return ovRegistrosTasas;
	}

	public Vector ovgetTasasxEpo(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda)
    throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	Vector ovDatosTasa = null;
	Vector ovRegistrosTasas = new Vector();
		try{
			lobdConexion.conexionDB();
			String lsQry =
	        " SELECT cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
	        "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
	        "       mt.fn_valor AS valor, tie.cg_rel_mat AS relmat, "+
	        "       tie.fn_puntos AS puntos, "+
	        "       DECODE ( "+
	        "          tie.cg_rel_mat, "+
	        "          '+', (mt.fn_valor + tie.fn_puntos), "+
	        "          '-', (mt.fn_valor - tie.fn_puntos), "+
	        "         '*', (mt.fn_valor * tie.fn_puntos), "+
	        "          '/', (mt.fn_valor / tie.fn_puntos), "+
	        "          0 "+
	        "       ) "+
	        "             AS tasapiso, "+
	        "       TO_CHAR (tie.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo, "+
	        "       cm.ic_moneda "+
					" 			,cp.in_plazo_dias "+
	        "  FROM com_tasa_if_epo tie, "+
	        "       com_mant_tasa mt, "+
	        "       comcat_plazo cp, "+
	        "       comcat_tasa ct, "+
	        "       comcat_moneda cm, "+
	        "       comrel_tasa_producto tp "+
	        " WHERE ct.ic_tasa = tie.ic_tasa "+
	        "   AND ct.ic_plazo = cp.ic_plazo "+
	        "   AND ct.ic_tasa = mt.ic_tasa "+
	        "   AND mt.ic_tasa = tp.ic_tasa "+
	        "   AND ct.ic_tasa = tp.ic_tasa "+
	        "   AND tie.ic_epo = "+ic_epo+" "+
	        "   AND tie.ic_if = "+ic_if+" "+
	        "   AND tie.ic_producto_nafin = tp.ic_producto_nafin "+
	        "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
	        "   AND ct.ic_moneda in ("+ic_moneda+") "+
	        "   AND cm.ic_moneda = ct.ic_moneda "+
	        "   AND mt.dc_fecha = "+
	        "          (SELECT MAX (mi.dc_fecha) "+
	        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp "+
	        "            WHERE mi.ic_tasa = mt.ic_tasa "+
	        "              AND ti.ic_tasa = mi.ic_tasa "+
	        "              AND ti.ic_tasa = ctp.ic_tasa "+
	        "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+") "+
	        " ORDER BY cm.ic_moneda, cp.ic_plazo ";
	      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
/*0*/		ovDatosTasa.add(lrsQry.getString("moneda"));
/*1*/		ovDatosTasa.add(lrsQry.getString("tipotasa"));
/*2*/		ovDatosTasa.add(lrsQry.getString("cvetasa"));
/*3*/		ovDatosTasa.add(lrsQry.getString("plazo"));
/*4*/		ovDatosTasa.add(lrsQry.getString("valor"));
/*5*/		ovDatosTasa.add(lrsQry.getString("relmat"));
/*6*/		ovDatosTasa.add(lrsQry.getString("puntos"));
/*7*/		ovDatosTasa.add(lrsQry.getString("tasapiso"));
/*8*/   ovDatosTasa.add(lrsQry.getString("fecha"));
/*9*/		ovDatosTasa.add(lrsQry.getString("ic_moneda"));
/*10*/		ovDatosTasa.add(lrsQry.getString("in_plazo_dias"));
       ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en ovgetTasas(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return ovRegistrosTasas;
	}

	public void guardaTasaPreferencial(String ic_epo, String ic_pyme, String ic_if, String ic_moneda,
									   String tipo_tasa, String puntos, int ic_producto_nafin
									   ,String cc_acuse,String ic_usuario,String _acuse)
		throws NafinException{
		AccesoDB			con 			= null;
		PreparedStatement	ps				= null;
		ResultSet			rs 				= null;
		String				qrySentencia	= "";
		boolean				resultado		= true;
		int					icTasaPrefer	= 0;
		try{
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" select ic_tasa_prefer_nego"+
				" from dis_tasa_prefer_nego"+
				" where ic_epo = ?"+
				" and ic_pyme = ?"+
				" and ic_if = ?"+
				" and cg_tipo = ?"+
				" and ic_moneda = ?"+
				" and ic_producto_nafin = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_epo);
			ps.setString(2,ic_pyme);
			ps.setString(3,ic_if);
			ps.setString(4,tipo_tasa);
			ps.setString(5,ic_moneda);
			ps.setInt(6,ic_producto_nafin);
			rs = ps.executeQuery();
			if(rs.next()){
				icTasaPrefer = rs.getInt(1);
			}
			ps.close();

			qrySentencia =
				" INSERT INTO COM_ACUSE3"+
				"(CC_ACUSE,DF_FECHA_HORA,IC_USUARIO,CG_RECIBO_ELECTRONICO,IC_PRODUCTO_NAFIN)"+
				" VALUES(?,sysdate,?,?,?)";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,cc_acuse);
			ps.setString(2,ic_usuario);
			ps.setString(3,_acuse);
			ps.setInt(4,ic_producto_nafin);
			ps.execute();
			ps.close();

			if(icTasaPrefer>0){
				qrySentencia =
					" update dis_tasa_prefer_nego"+
					" set fn_puntos = ?"+
					" ,cg_tipo = ?"+
					" ,cc_acuse = ?"+
					" where ic_tasa_prefer_nego = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setDouble(1,Double.parseDouble(puntos));
				ps.setString(2,tipo_tasa);
				ps.setString(3,cc_acuse);
				ps.setInt(4,icTasaPrefer);
				ps.execute();
			}else{
				qrySentencia =
					" insert into dis_tasa_prefer_nego"+
					" (ic_tasa_prefer_nego,ic_producto_nafin,ic_epo,ic_pyme,ic_if,fn_puntos,cg_tipo,ic_moneda,cc_acuse)"+
					" SELECT NVL(MAX(ic_tasa_prefer_nego),0)+1,?,?,?,?,?,?,?,? "+
					" FROM dis_tasa_prefer_nego";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,ic_producto_nafin);
				ps.setString(2,ic_epo);
				ps.setString(3,ic_pyme);
				ps.setString(4,ic_if);
				ps.setDouble(5,Double.parseDouble(puntos));
				ps.setString(6,tipo_tasa);
				ps.setString(7,ic_moneda);
				ps.setString(8,cc_acuse);
				ps.execute();
			}
		}catch(Exception e){
			System.out.println("Error = "+e);
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	}


/**
	 * Descripcion: M�todo que se encarga de realizar una consulta para obtener los estatus
	 * 				 de los checkboxs de las tasas en la pantalla de tasas por producto, adem�s
	 *              de informaci�n adicional para su procesamiento.
	 * @return List con informaci�n derivada de la consulta.
*/
	public List getEstatusDeTasas(){
		log.info("getEstatusDeTasas(E)" );
		AccesoDB 			con 			= new AccesoDB();
		String				qrySentencia 	= "";
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		Vector 				vProducto;
		HashMap hmDatos = null;
		List registros = new ArrayList();

		try{
			con.conexionDB();
			qrySentencia =
				" SELECT ic_producto_nafin, ic_nombre"   +
				"   FROM comcat_producto_nafin"   +
				"  WHERE ic_producto_nafin IN (1, 4)"  +
				" ORDER BY 1 ";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			vProducto = new Vector();
			while(rs.next()) {
				String rs_producto	= rs.getString(1)==null?"":rs.getString(1);
				//String rs_nombre 	= rs.getString(2)==null?"":rs.getString(2);
				vProducto.add(rs_producto);
			}//while(rs.next()) producto
			rs.close();
			if(ps!=null)
				ps.close();
			qrySentencia =
				" SELECT ic_tasa, cd_nombre"   +
				"   FROM comcat_tasa"   +
				"  WHERE cs_disponible = 'S'"   +
				"    AND ic_moneda IS NOT NULL"   +
				"  ORDER BY 2"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			int elemento = 0;
			while(rs.next()) {
				String rs_tasa		= rs.getString(1)==null?"":rs.getString(1);
				String rs_nombre	= rs.getString(2)==null?"":rs.getString(2);

				hmDatos = new HashMap();
				hmDatos.put("TIPOTASA", rs_nombre);
				hmDatos.put("CLAVETASA", rs_tasa);
				qrySentencia =
					" SELECT COUNT (1)"   +
					"   FROM comrel_tasa_producto"   +
					"  WHERE ic_tasa = ?"   +
					"    AND ic_producto_nafin = ?"  ;
				ResultSet			rsP				= null;
				PreparedStatement	psP				= null;
				psP = con.queryPrecompilado(qrySentencia);

				for(int i=0;i<vProducto.size();i++) {
					psP.clearParameters();
					psP.setInt(1, Integer.parseInt(rs_tasa));
					psP.setInt(2, Integer.parseInt(vProducto.get(i).toString()));
					rsP = psP.executeQuery();
					int existe = 0;

					if(rsP.next())
						existe = rsP.getInt(1);
					rsP.close();
					String chkTasaProd = "";
					String strDisabled = "";

					if(existe>0) {
						chkTasaProd = "checked";
						hmDatos.put("CHKPRODUCTO"+vProducto.get(i).toString(),"S" );
					}else{
						hmDatos.put("CHKPRODUCTO"+vProducto.get(i).toString(),"N" );
					}

					if(!"1".equals(vProducto.get(i).toString())){
						strDisabled = "disabled";
						hmDatos.put("VERCHKPRODUCTO"+vProducto.get(i).toString(),"N" );
					 }else{
						hmDatos.put("VERCHKPRODUCTO"+vProducto.get(i).toString(),"S" );
					 }
					elemento++;
				}//fin del for
				if(psP != null)
					psP.close();
				registros.add(hmDatos);
			}//while(rs.next()) producto

			rs.close();
			if(ps!=null)
				ps.close();

			return registros;
		}catch(Exception e){
			log.error("Error en M�todo getEstatusDeTasas: ",e);
			throw new AppException("Error en getEstatusDeTasas ", e);
		} finally{
			log.info("getEstatusDeTasas(S)" );
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		//return registros;
	}//fin metodo



/**
	 * Descripcion: M�todo que se encarga de realizar las actualizacioes a los estatus de
	 * 				las tasas en la pantalla de Tasas por Producto.
	 * @param String[] txt_cambio Contiene los datos de las tasas que ser�n modificadas, en
	 * 			caso de ser vac�o un elemento ser� descartado.
	 * @return Mapa con la leyenda que ser� mostrada de acuerdo al resultado obtenido.
*/

	public Map cambioTasas(String[] txt_cambio){
		log.info("cambioTasas(E)");
		AccesoDB 			con 			= new AccesoDB();
		String				qrySentencia 	= "";
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		HashMap dato = new HashMap();
		boolean resultado = false;

		//***********Validaci�n de parametros:*****
		try {
			if (txt_cambio == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." + "\n" +
					" txt_cambio=" + txt_cambio);
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//******************************************

		try{
			con.conexionDB();
			String mensaje="";

			for(int i=0;i<txt_cambio.length;i++) {

				if(!"".equals(txt_cambio[i].trim())) {
					StringTokenizer datos = new StringTokenizer(txt_cambio[i],"|");
					String st_ant = "";
					String st_tasa = "";
					String st_producto = "";
					String st_act = "";

					if(datos.hasMoreTokens()){
						st_ant = datos.nextToken().toString();
						st_tasa = datos.nextToken().toString();
						st_producto = datos.nextToken().toString();
						st_act = datos.nextToken().toString();
					}

					if("S".equals(st_ant) && "N".equals(st_act)) {
						qrySentencia =
							" SELECT COUNT (1)"   +
							"   FROM comrel_tasa_producto_epo"   +
							"  WHERE ic_tasa = ?"   +
							"    AND ic_producto_nafin = ?"  ;
						ps = con.queryPrecompilado(qrySentencia);
						ps.setInt(1, Integer.parseInt(st_tasa));
						ps.setInt(2, Integer.parseInt(st_producto));
						rs = ps.executeQuery();
						int existeTasaEpo = 0;
						if(rs.next()) {
							existeTasaEpo = rs.getInt(1);
						}//if(rs.next())
						rs.close();
						if(ps!=null) ps.close();
						if(existeTasaEpo>0) {
							resultado =  false;
							mensaje="La tasa esta relacionada con una EPO-Producto y no se podr� quitar la relaci�n, favor de verificar";
						}
						else {
							qrySentencia =
								" DELETE"   +
								"   FROM comrel_tasa_producto"   +
								"  WHERE ic_tasa = ?"   +
								"    AND ic_producto_nafin = ?"  ;
							ps = con.queryPrecompilado(qrySentencia);
							ps.setInt(1, Integer.parseInt(st_tasa));
							ps.setInt(2, Integer.parseInt(st_producto));
							ps.executeUpdate();
							if(ps!=null) ps.close();
							resultado =  true;
						}
					}//if("N".equals(st_ant) && "N".equals(st_act))
					else {
						if("N".equals(st_ant) && "S".equals(st_act)){
							qrySentencia =
								" INSERT INTO comrel_tasa_producto"   +
								"             (ic_tasa, ic_producto_nafin)"   +
								"      VALUES(?, ?)"  ;
							ps = con.queryPrecompilado(qrySentencia);
							ps.setInt(1, Integer.parseInt(st_tasa));
							ps.setInt(2, Integer.parseInt(st_producto));
							ps.executeUpdate();
							if(ps!=null)
								ps.close();
							resultado =  true;
						}//if("N".equals(st_ant) && "S".equals(st_act))
					}
				}//if(!"".equals(txt_cambio[i].trim()))
			}//fin for

			if(resultado){
				mensaje="Tasas asignadas al producto satisfactoriamente";
				dato.put("MENSAJE",mensaje);
			}
			else{
				dato.put("MENSAJE",mensaje);
			}
		}catch(Exception e) {
			resultado = false;
			dato.put("MENSAJE","Hubo errores al realizar las actualizaciones");
		} finally {
			con.terminaTransaccion(resultado);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			log.info("cambioTasas(S)");
			}
		}
		return dato;
	}//fin metodo


	/**
	 * Descripcion: M�todo que se encarga de realizar una consulta para obtener los encabezados
	 *              del grid en la pantalla Tasas por Producto.
	 *
	 * @return Mapa con informaci�n de los nombres de encabezados derivados de la consulta.
 */

	public Map getEncabezadosProductos() {
		log.info("getEncabezadosProductos(E)");
		HashMap hmProductos = new HashMap();
		AccesoDB con = null;
		PreparedStatement	ps	= null;
		ResultSet rsAPI = null;

		try {
			con = new AccesoDB();
			con.conexionDB();
			String sQuery = 			" SELECT ic_producto_nafin, ic_nombre"   +
											"   FROM comcat_producto_nafin"   +
											"  WHERE ic_producto_nafin IN (1, 4)"  +
											" ORDER BY 1 ";

			ps = con.queryPrecompilado(sQuery);
			rsAPI = ps.executeQuery();

			while(rsAPI.next()) {
				hmProductos.put(rsAPI.getString("ic_producto_nafin"), rsAPI.getString("ic_nombre"));
				hmProductos.put("CLAVEPRODUCTO"+rsAPI.getString("ic_producto_nafin"), rsAPI.getString("ic_producto_nafin"));
			}
			log.info("getEncabezadosProductos(S)");

			return hmProductos;

		}catch (Exception e) {
			log.error("Error en getEncabezadosProductos: ", e);
			throw new AppException("Error en consulta para encabezados de Tasas por Producto", e);
		} finally {
			if(rsAPI != null) {
				try {
					rsAPI.close();
				}catch(Exception e){
					log.error("Error en getEncabezadosProductos: ", e);
				}
			}
		 	if(ps != null) {
				try {
					ps.close();
				}catch(Exception e){
					log.error("Error en getEncabezadosProductos: ", e);
				}
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

	}//fin metodo
	/**
	 * FODEA-005-2014
	 * Metodo para generar el catalodo plazo para el tipo de tasa PREFERENCIAL
	 * @param ic_plazo
	 * @param ic_moneda
	 * @param ic_if
	 * @param ic_epo
	 * @param ic_producto_nafin
	 * @return Vectos con la clave y descripcion  de plazo
	 * @throws com.netro.exception.NafinException
	 */
	public Vector getCatalogoPlazo(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda,String ic_plazo)
    throws NafinException {
		AccesoDB lobdConexion = new AccesoDB();
		Vector ovDatosTasa = null;
		Vector ovRegistrosTasas = new Vector();
		try{
			lobdConexion.conexionDB();
			String lsQry =
	        " SELECT  cp.cg_descripcion AS plazo,	\n"+
			  "	cp.ic_plazo AS icplazo 	\n"+
	        "  FROM com_tasa_if_epo tie, 	\n"+
	        "       com_mant_tasa mt, 	\n"+
	        "       comcat_plazo cp, 	\n"+
	        "       comcat_tasa ct, 	\n"+
	        "       comcat_moneda cm, 	\n"+
	        "       comrel_tasa_producto tp 	\n"+
	        " WHERE ct.ic_tasa = tie.ic_tasa 	\n"+
	        "   AND ct.ic_plazo = cp.ic_plazo 	\n"+
	        "   AND ct.ic_tasa = mt.ic_tasa 	\n"+
	        "   AND mt.ic_tasa = tp.ic_tasa 	\n"+
	        "   AND ct.ic_tasa = tp.ic_tasa 	\n"+
	        "   AND tie.ic_epo = "+ic_epo+"  	\n"+
	        "   AND tie.ic_if = "+ic_if+"  	\n"+
	        "   AND tie.ic_producto_nafin = tp.ic_producto_nafin 	\n"+
	        "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
	        "   AND ct.ic_moneda in ("+ic_moneda+")  	\n"+
	        "   AND cm.ic_moneda = ct.ic_moneda  	\n"+
	        "   AND mt.dc_fecha =  	\n"+
	        "          (SELECT MAX (mi.dc_fecha)  	\n"+
	        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp 	\n"+
	        "            WHERE mi.ic_tasa = mt.ic_tasa 	\n"+
	        "              AND ti.ic_tasa = mi.ic_tasa 	\n"+
	        "              AND ti.ic_tasa = ctp.ic_tasa 	\n"+
	        "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+") ";
	        if(!ic_plazo.equals(""))
	       		lsQry += "\n and cp.ic_plazo = "+ ic_plazo;
	        lsQry += "\n ORDER BY cm.ic_moneda, cp.ic_plazo";
	      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
				ovDatosTasa.add(lrsQry.getString("plazo"));
				ovDatosTasa.add(lrsQry.getString("icplazo"));
				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en getCatalogoPlazo(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return ovRegistrosTasas;
	}

	/**
	 * FODEA-005-2014
	 * * Metodo para generar el catalodo plazo para el tipo de tasa PREFERENCIAL EN MODALIDAD 2
	 * @throws com.netro.exception.NafinException
	 * @return Vectos con la clave y descripcion  de plazo
	 * @param ic_plazo
	 * @param ic_moneda
	 * @param ic_if
	 * @param ic_pyme
	 * @param ic_producto_nafin
	 */
	public Vector getCatalogoPlazoPyme(int ic_producto_nafin, String ic_pyme,String ic_if,String ic_moneda,String ic_plazo)
    throws NafinException {
		AccesoDB lobdConexion = new AccesoDB();
		Vector ovDatosTasa = null;
		Vector ovRegistrosTasas = new Vector();
		try{
			lobdConexion.conexionDB();
			String lsQry =
	        " SELECT  cp.cg_descripcion AS plazo,	\n"+
			  "	cp.ic_plazo AS icplazo 	\n"+
	        "  FROM com_tasa_if_pyme tip, 	\n"+
	        "       com_mant_tasa mt, 	\n"+
	        "       comcat_plazo cp, 	\n"+
	        "       comcat_tasa ct, 	\n"+
	        "       comcat_moneda cm, 	\n"+
	        "       comrel_tasa_producto tp 	\n"+
	        " WHERE ct.ic_tasa = tip.ic_tasa 	\n"+
	        "   AND ct.ic_plazo = cp.ic_plazo 	\n"+
	        "   AND ct.ic_tasa = mt.ic_tasa 	\n"+
	        "   AND mt.ic_tasa = tp.ic_tasa 	\n"+
	        "   AND ct.ic_tasa = tp.ic_tasa 	\n"+
	        "   AND tip.ic_pyme = "+ic_pyme+"  	\n"+
	        "   AND tip.ic_if = "+ic_if+"  	\n"+
	        "   AND tip.ic_producto_nafin = tp.ic_producto_nafin 	\n"+
	        "   AND tp.ic_producto_nafin = "+ic_producto_nafin+" "+
	        "   AND ct.ic_moneda in ("+ic_moneda+")  	\n"+
	        "   AND cm.ic_moneda = ct.ic_moneda  	\n"+
	        "   AND mt.dc_fecha =  	\n"+
	        "          (SELECT MAX (mi.dc_fecha)  	\n"+
	        "             FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp 	\n"+
	        "            WHERE mi.ic_tasa = mt.ic_tasa 	\n"+
	        "              AND ti.ic_tasa = mi.ic_tasa 	\n"+
	        "              AND ti.ic_tasa = ctp.ic_tasa 	\n"+
	        "              AND ctp.ic_producto_nafin = "+ic_producto_nafin+") ";
	        if(!ic_plazo.equals(""))
	       		lsQry += "\n and cp.ic_plazo = "+ ic_plazo;
	        lsQry += "\n ORDER BY cm.ic_moneda, cp.ic_plazo";
	      System.out.println(lsQry);
			ResultSet lrsQry = lobdConexion.queryDB(lsQry);
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
				ovDatosTasa.add(lrsQry.getString("plazo"));
				ovDatosTasa.add(lrsQry.getString("icplazo"));
				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lobdConexion.cierraStatement();

		/*} catch (NafinException ne) {
			throw ne; */
		} catch(Exception e){
			System.out.println("Exception ocurrida en getCatalogoPlazo(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return ovRegistrosTasas;
	}
	public void setValoresAnteriores(String valoresAnteriores) {
		this.valoresAnteriores = valoresAnteriores;
	}


	public String getValoresAnteriores() {
		return valoresAnteriores;
	}


	/**
	 *  Fodea 05-2014  Pantalla E: Distribuidores / Capturas / Selecci�n de Documentos Cr�dito en Cuenta Corriente
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_pyme
	 * @param ic_epo
	 * @param ic_if
	 * @param cg_tipo
	 * @param ic_producto_nafin
	 * @param  ic_moneda, 
	 * @param  modalidad   
	 */
	
	public Vector getTasasPreferNego(String ic_producto_nafin, String cg_tipo, String ic_if, String ic_epo, String ic_pyme, String ic_moneda, String modalidad  )
    		throws NafinException {
		log.info ("getTasasPreferNego  (E)");

		AccesoDB lobdConexion	= new AccesoDB();
		Vector ovDatosTasa 		= null;
		Vector ovRegistrosTasas	= new Vector();
		PreparedStatement	ps	= null;
		String campos = "";
		String tablas = "";
		String condicion = "";
		List lVarBind		= new ArrayList();

		try{
			lobdConexion.conexionDB();

			if(!"".equals(ic_moneda))
				condicion += "    AND ct.ic_moneda = ? ";

			if("P".equals(cg_tipo)) {
				campos =
			        "       ,DECODE ( "+
			        "          tie.cg_rel_mat, "+
			        "          '+', (mt.fn_valor + tie.fn_puntos) - nvl(pre.fn_puntos,0), "+
			        "          '-', (mt.fn_valor - tie.fn_puntos) - nvl(pre.fn_puntos,0), "+
			        "         '*', (mt.fn_valor * tie.fn_puntos) - nvl(pre.fn_puntos,0), "+
			        "          '/', (mt.fn_valor / tie.fn_puntos) - nvl(pre.fn_puntos,0), "+
			        "          0 "+
			        "       ) AS tasafinal, pre.fn_puntos as puntospref ,'' AS fechaNego " +
					"		,'P' as tipoTasaBPN " ;

				tablas =
					"        ,(SELECT fn_puntos,ic_moneda, ic_plazo, CG_TIPOS_CREDITO "   +
					"           FROM dis_tasa_prefer_nego"   +
					"          WHERE ic_pyme = ? "   +
					"            AND ic_producto_nafin = ?"   +
					"            AND ic_epo = ?"   +
					"            AND ic_if = ?";
			if(!"".equals(ic_moneda))
				tablas += "  AND ic_moneda = ?";

				tablas +=
					"            AND cg_tipo = 'P') pre"  ;
				condicion +=
					"    AND pre.fn_puntos(+) > 0"  +
   					"	 AND pre.ic_moneda(+) = ct.ic_moneda"  +
   					"	 AND pre.ic_plazo(+) = ct.ic_plazo" +
					"	 AND ct.ic_tasa NOT IN (SELECT neg.ic_tasa"   +
					"	   FROM dis_tasa_prefer_nego neg"   +
					"	  WHERE neg.ic_pyme = pym.ic_pyme"   +
					"	    AND neg.ic_producto_nafin = tp.ic_producto_nafin"   +
					"	    AND neg.ic_epo = epo.ic_epo"   +
					"	    AND neg.ic_if = tie.ic_if"   +
					"	    AND neg.ic_moneda = cm.ic_moneda"   +
					"	    AND neg.cg_tipo = 'N'"   +
					"	    AND TRUNC (neg.df_fecha) = TRUNC (SYSDATE))"  +
					"    and  pre.CG_TIPOS_CREDITO   = 'C'  " ;


			}	else {
				if("N".equals(cg_tipo)) {
					campos =
						"		 ,tpn.fn_valor AS tasafinal, '' as puntospref, TO_CHAR(tpn.df_fecha,'dd/mm/yyyy') AS fechaNego "+
						"		,'N' as tipoTasaBPN " ;

					tablas =
						"        ,dis_tasa_prefer_nego tpn ";
					
					if("1".equals(modalidad) ) {	
					condicion +=
						"    AND ct.ic_tasa = tpn.ic_tasa"   +
						"    AND tpn.ic_producto_nafin = tp.ic_producto_nafin"   +
						"    AND tpn.ic_if = tie.ic_if"   +
						"    AND tpn.ic_epo = tie.ic_epo"   +
						"    AND tpn.ic_moneda = ct.ic_moneda"   +
						"    AND tpn.ic_pyme = pym.ic_pyme"   +
						"    AND tpn.cg_tipo = 'N'"   +
						"    AND TRUNC (df_fecha) = TRUNC (SYSDATE)" +
						"    and  tpn.CG_TIPOS_CREDITO   = 'C'  " ;	
						
					}else  if("2".equals(modalidad) ) {
						condicion +=
						"    AND ct.ic_tasa = tpn.ic_tasa"   +
						"    AND tpn.ic_producto_nafin = tp.ic_producto_nafin"   +
						"    AND tpn.ic_if = tie.ic_if"   +
						"    AND tpn.ic_pyme = tie.ic_pyme "   +
						"    AND tpn.ic_moneda = ct.ic_moneda"   +
						"    AND tpn.ic_pyme = pym.ic_pyme"   +
						"    AND tpn.cg_tipo = 'N'"   +
						"    AND TRUNC (df_fecha) = TRUNC (SYSDATE)" +
						"    and  tpn.CG_TIPOS_CREDITO   = 'C'  " ;			
					
					}
				}
			}
			if("".equals(campos)) {
				campos =
			        "       ,DECODE ( "+
			        "          tie.cg_rel_mat, "+
			        "          '+', (mt.fn_valor + tie.fn_puntos), "+
			        "          '-', (mt.fn_valor - tie.fn_puntos), "+
			        "         '*', (mt.fn_valor * tie.fn_puntos), "+
			        "          '/', (mt.fn_valor / tie.fn_puntos), "+
			        "          0 "+
			        "       ) "+
			        "             AS tasafinal, '' as puntospref ,'' as fechaNego"+
					"		,'B' as tipoTasaBPN " ;

			}


			String lsQry =
					 " SELECT  /*+index(mt CP_COM_MANT_TASA_PK , tie CF_COMREL_IF_EPO_X_PROD_02_FK )*/ "+
		       "  cm.cd_nombre AS moneda, ct.cd_nombre AS tipotasa, "+
		        "       ct.ic_tasa AS cvetasa, cp.cg_descripcion AS plazo, "+
		        "       mt.fn_valor AS valor, tie.cg_rel_mat AS relmat, "+
		        "       tie.fn_puntos AS puntos, "+
		        "       DECODE ( "+
		        "          tie.cg_rel_mat, "+
		        "          '+', (mt.fn_valor + tie.fn_puntos), "+
		        "          '-', (mt.fn_valor - tie.fn_puntos), "+
		        "         '*', (mt.fn_valor * tie.fn_puntos), "+
		        "          '/', (mt.fn_valor / tie.fn_puntos), "+
		        "          0 "+
		        "       ) "+
		        "             AS tasapiso, "+
		        "       TO_CHAR (tie.df_captura, 'dd/mm/yyyy') AS fecha, cp.ic_plazo, "+
		        "       cm.ic_moneda,cp.in_plazo_dias "+
				" 		,epo.cg_razon_social AS nombreEpo "+
				" 		,pym.cg_razon_social AS nombrePyme "+campos+" "+
				"   FROM comcat_pyme pym,"   +
				"        comcat_epo epo," ;
				if("1".equals(modalidad) ) {
				lsQry+="  com_tasa_if_epo tie," +
				"       com_mant_tasa mt,"   +
				"        comcat_tasa ct,"   +
				"        comrel_tasa_producto tp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_plazo cp "+tablas+" "+
				"  WHERE epo.ic_epo = tie.ic_epo"   +
				"    AND ct.ic_tasa = tie.ic_tasa"   +
				"    AND ct.ic_plazo = cp.ic_plazo"   +
				"    AND ct.ic_tasa = mt.ic_tasa"   +
				"    AND mt.ic_tasa = tp.ic_tasa"   +
				"    AND ct.ic_tasa = tp.ic_tasa"   +
				"    AND cm.ic_moneda = ct.ic_moneda"   +
				"    AND tie.ic_producto_nafin = tp.ic_producto_nafin"   +
				"    AND mt.dc_fecha = (SELECT MAX (mi.dc_fecha)"   +
				"                         FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp"   +
				"                        WHERE mi.ic_tasa = mt.ic_tasa"   +
				"                          AND ti.ic_tasa = mi.ic_tasa"   +
				"                          AND ti.ic_tasa = ctp.ic_tasa"   +
				"                          AND ctp.ic_producto_nafin = tp.ic_producto_nafin)"   +
				"    AND pym.ic_pyme = ?"   +
				"    AND tp.ic_producto_nafin = ?"   +
				"    AND tie.ic_epo = ?"   +				
				"    AND tie.ic_if = ?"+condicion+" "+				
				"  ORDER BY cm.ic_moneda, cp.ic_plazo"  ;
		
		
		}else if("2".equals(modalidad) ) {
			lsQry+="  com_tasa_if_pyme tie," +										
				"       com_mant_tasa mt,"   +
				"        comcat_tasa ct,"   +
				"        comrel_tasa_producto tp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_plazo cp "+tablas+" "+
				"  WHERE pym.ic_pyme = tie.ic_pyme "   +
				"    AND ct.ic_tasa = tie.ic_tasa"   +
				"    AND ct.ic_plazo = cp.ic_plazo"   +
				"    AND ct.ic_tasa = mt.ic_tasa"   +
				"    AND mt.ic_tasa = tp.ic_tasa"   +
				"    AND ct.ic_tasa = tp.ic_tasa"   +
				"    AND cm.ic_moneda = ct.ic_moneda"   +
				"    AND tie.ic_producto_nafin = tp.ic_producto_nafin"   +
				"    AND mt.dc_fecha = (SELECT MAX (mi.dc_fecha)"   +
				"                         FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp"   +
				"                        WHERE mi.ic_tasa = mt.ic_tasa"   +
				"                          AND ti.ic_tasa = mi.ic_tasa"   +
				"                          AND ti.ic_tasa = ctp.ic_tasa"   +
				"                          AND ctp.ic_producto_nafin = tp.ic_producto_nafin)"   +
				"    AND pym.ic_pyme = ?"   +
				"    AND tp.ic_producto_nafin = ?"   +
				"    AND tie.ic_pyme = ?"   +				
				"    AND tie.ic_if = ?"+condicion+" "+				
				"  ORDER BY cm.ic_moneda, cp.ic_plazo"  ;
				}
			if("1".equals(modalidad) ) {
						
				lVarBind.add(ic_pyme);
				lVarBind.add(ic_producto_nafin);
				lVarBind.add(ic_epo);
				lVarBind.add(ic_if);			
				
				if(!"".equals(ic_moneda)) {
					lVarBind.add(ic_moneda);
				}
				
				if("P".equals(cg_tipo)) {
					lVarBind.add(ic_pyme);
					lVarBind.add(ic_producto_nafin);
					lVarBind.add(ic_epo);
					lVarBind.add(ic_if);	
					if(!"".equals(ic_moneda)) {
						lVarBind.add(ic_moneda);
					}
				}
			}else  if("2".equals(modalidad) ) {
			
				lVarBind.add(ic_pyme);
				lVarBind.add(ic_producto_nafin);
				lVarBind.add(ic_epo);
				lVarBind.add(ic_if);
				if(!"".equals(ic_moneda)) {
					lVarBind.add(ic_moneda);
				}
			
			
				if("P".equals(cg_tipo)) {
					lVarBind.add(ic_pyme);
					lVarBind.add(ic_producto_nafin);
					lVarBind.add(ic_pyme);
					lVarBind.add(ic_if);	
					if(!"".equals(ic_moneda)) {
						lVarBind.add(ic_moneda);
					}
				}	
				
				
			}
					
			System.out.println("------modalidad-----------------------------"+modalidad);
			System.out.println("lsQry::" +lsQry );
			System.out.println("lVarBind::" +lVarBind );

			ps = lobdConexion.queryPrecompilado(lsQry, lVarBind);
			ResultSet lrsQry = ps.executeQuery();
			ps.clearParameters();
			while(lrsQry.next()) {
				ovDatosTasa = new Vector();
/*0*/				ovDatosTasa.add(lrsQry.getString("moneda"));
/*1*/				ovDatosTasa.add(lrsQry.getString("tipotasa"));
/*2*/				ovDatosTasa.add(lrsQry.getString("cvetasa"));
/*3*/				ovDatosTasa.add(lrsQry.getString("plazo"));
/*4*/				ovDatosTasa.add(lrsQry.getString("valor"));
/*5*/				ovDatosTasa.add(lrsQry.getString("relmat"));
/*6*/				ovDatosTasa.add(lrsQry.getString("puntos"));
/*7*/				ovDatosTasa.add(lrsQry.getString("tasapiso"));
/*8*/				ovDatosTasa.add(lrsQry.getString("fecha"));
/*9*/				ovDatosTasa.add(lrsQry.getString("nombreEpo"));
/*10*/				ovDatosTasa.add(lrsQry.getString("nombrePyme"));
/*11*/				ovDatosTasa.add(lrsQry.getString("tasafinal"));
/*12*/				ovDatosTasa.add(lrsQry.getString("fechaNego"));
/*13*/				ovDatosTasa.add(lrsQry.getString("ic_moneda"));
/*14*/				ovDatosTasa.add(lrsQry.getString("in_plazo_dias"));
/*15*/				ovDatosTasa.add(lrsQry.getString("puntospref"));
/*16*/				ovDatosTasa.add(lrsQry.getString("tipoTasaBPN"));
/* */				ovRegistrosTasas.addElement(ovDatosTasa);
			}
			lrsQry.close();
			if(ps!=null) ps.close();

		} catch(Exception e){
			log.error("Exception ocurrida en getTasasPreferNego(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
			log.info(" getTasasPreferNego(S)");
		}
		return ovRegistrosTasas;
	}


	/**
	 * Bitacora de Pantalla Admin IF /Distribuidores/Capturas/Tasas por PYMES
	 * @param Actuales
	 * @param Anteriores
	 * @param strNombreUsuario
	 * @param strLogin
	 * @param iNoNafinElectronico
	 */
	public void  setBitacoraTasasPyme(String ic_if, String strLogin, String strNombreUsuario, String Anteriores, String Actuales  ){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		List lVarBind		= new ArrayList();
		String iNoNafinElectronico ="";
		log.info("setBitacoraTasasPyme (E)");

		 try{

			con.conexionDB();

			StringBuffer strQry = new StringBuffer();
			strQry.append(" select IC_NAFIN_ELECTRONICO  from comrel_nafin  "+
								" where  IC_EPO_PYME_IF = ?  "+
								" and cg_tipo = ?   ");

			lVarBind		= new ArrayList();
			lVarBind.add(new Integer(ic_if));
			lVarBind.add("I");
			log.info("strQry  " +strQry);
			log.info("lVarBind  " +lVarBind);

			ps = con.queryPrecompilado(strQry.toString(),lVarBind );
         rs = ps.executeQuery();

			if(rs.next()){
				iNoNafinElectronico= (rs.getString("IC_NAFIN_ELECTRONICO")==null?"":rs.getString("IC_NAFIN_ELECTRONICO"));
         }
			rs.close();
       	ps.close();

			StringBuffer insertBitacoraGral = new StringBuffer();
					insertBitacoraGral.append(
					"\n INSERT INTO BIT_CAMBIOS_GRAL ( " +
					"\n IC_CAMBIO,  " +
					"\n CC_PANTALLA_BITACORA,  " +
					"\n CG_CLAVE_AFILIADO, " +
					"\n IC_NAFIN_ELECTRONICO, " +
					"\n DF_CAMBIO, " +
					"\n IC_USUARIO, " +
					"\n CG_NOMBRE_USUARIO, " +
					"\n CG_ANTERIOR, " +
					"\n CG_ACTUAL  ) " +
					"\n VALUES(  " +
					"\n SEQ_BIT_CAMBIOS_GRAL.nextval, " +
					"\n 'TASAS_PYME_D', " +
					"\n 'I', " +
					"\n '"+iNoNafinElectronico+"', " +
					"\n SYSDATE, " +
					"\n '"+strLogin+"',  " +
					"\n '"+strNombreUsuario+"', " +
					"\n '"+Anteriores+"', " +
					"\n '"+Actuales+"' ) " );
			log.error("insertBitacoraGral  " +insertBitacoraGral);
			ps = con.queryPrecompilado(insertBitacoraGral.toString());
			ps.executeUpdate();
			ps.close();


      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  setBitacoraTasasPyme (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getcuentaClablePyme (S)");
	}

}// Fin del Bean
