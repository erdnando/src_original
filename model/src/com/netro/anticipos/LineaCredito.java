package com.netro.anticipos;

import com.netro.exception.NafinException;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.Acuse;
import netropology.utilerias.Combos;

@Remote
public interface LineaCredito {

  public abstract String getFolioArmado(String ic_linea,String ic_pyme,String tipo_solicitud)
    throws NafinException;

  public abstract String getFolioArmado(String ic_linea)
    throws NafinException;

  public abstract void setFolioArmado(String folio);

  public abstract String getTipoSolicitud()
    throws NafinException;

  public abstract String getNumeroSirac()
    throws NafinException;

  public abstract String getConsecutivo();

  public abstract void borraSolicitud(String ic_linea_credito)
    throws NafinException;

  public abstract void generaSolicitud(String ic_linea_credito,String ic_epo,String ic_pyme,String ic_if,String cg_tipo_solicitud,String ic_linea_padre)
    throws NafinException;

  public abstract String getNombreEpo(String ic_epo)
    throws NafinException;

  public abstract String reservaLinea(String ic_pyme)
    throws NafinException;

 	public abstract String getNombreIF(String ic_if)
  	throws NafinException;

	public abstract String getTipoPersona(String ic_pyme)
  	throws NafinException;

	public abstract void AutorizaNafin(String folio [],String estatus [],String [] huboCambio,String sElementos,String causa [],String ic_producto_nafin)
  	throws NafinException;

 	public abstract Vector getDireccion(String ic_if)
	  	throws NafinException;

  public abstract void autorizaPyme(String [] lineas,int iElementos)
  	throws NafinException;

  public Vector consultaPyme(String ic_pyme,String fecha_sol, String ic_producto_nafin)
  	throws NafinException;

  public Vector getEpoRelacionadaPyme(String ic_pyme,String ic_if,String ic_producto_nafin)
  	throws NafinException;

	public Vector consultaPyme(String lineas[],int elementos)
  	throws NafinException;

	public abstract int getLineasAutorizadas();

  public abstract int getLineasAutorizadas(String ic_pyme)
  	throws NafinException;

  public abstract int getLineasAutorizadas2(String ic_pyme, int ic_producto_nafin)
  	throws NafinException;

  public abstract Combos getEstatus(String tipoUsuario) //deprecated
  	throws NafinException;

	public Vector getVecEstatus(String tipoUsuario)
		throws NafinException;

  public abstract String getEstatus(String tipoUsuario,String selected)
  	throws NafinException;

 	public abstract Combos getBancos()
  	throws NafinException;

  	public Vector getVecBancos()
  	throws NafinException;

 	public abstract Combos getPlazos()	//deprecated
  	throws NafinException;

  	public Vector getVecPlazos()
  	throws NafinException;
  	
  public abstract Vector consultaIF(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin,String cc_acuse,String ic_modalidad)
  	throws NafinException;

  public abstract Vector consultaIF(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin,String cc_acuse)
  	throws NafinException;
	
  public abstract Vector consultaIF(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin)
  	throws NafinException;

  public abstract void AutorizaNafin(String folio [],String estatus [],String [] huboCambio,String sElementos)
		throws NafinException;

  public abstract Vector consultaNafin(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin)
  	throws NafinException;

  public abstract Vector getComboLineas(String ic_pyme,String ic_epo,String tipoSolicitud)
  	throws NafinException;

  public abstract Vector getLineasProceso(String ic_pyme,String ic_epo,String tipoSolicitud)
         throws NafinException;

  public abstract Vector getEpoRelacionada(String ic_pyme,String ic_epo)
   	throws NafinException;

	public abstract Vector consultaDoctos(String ic_pyme,String ic_if,String ic_linea,String cg_tipo_solicitud,String ic_producto_nafin)
  throws NafinException;

  //	Metodos de Rafael Flores
  public abstract Vector LineaCredito(String NumCliente, String NumEpo,String NumMoneda)
  	throws NafinException;

  public abstract void VerificaLineaCredito(String NumCliente)
  	throws NafinException;
//Omar Torres
  public abstract void generaSolicitud(String ic_linea_credito,String ic_epo,String ic_pyme,String ic_if,	String cg_tipo_solicitud,String ic_linea_padre, String ic_producto_nafin)
  	throws NafinException;

  public abstract void autorizaPyme(String [] lineas,int iElementos, String ic_producto_nafin)
  	throws NafinException;

  public abstract Vector consultaPymeXProd(String lineas[],int elementos, String ic_producto_nafin)
  	throws NafinException;

  public abstract Vector consultaPymeInv(String ic_pyme,String fecha_sol, String ic_producto_nafin, String ic_epo,String tipoSolic,String Eestatus,String ic_moneda, String ic_if)
  	throws NafinException;

  public Vector getComboLineas(String ic_pyme,String ic_epo,String tipoSolicitud, String ic_producto_nafin)
  	throws NafinException;

 public Vector getLineasProceso(String ic_pyme,String ic_epo,String tipoSolicitud,String ic_producto_nafin)
  	throws NafinException;

 public abstract void AutorizaNafin(String folio [],String estatus [],String [] huboCambio,String sElementos,String causa [])
    throws NafinException;

 public Vector getPlazoIF(String ic_producto_nafin, String ic_if, String credExp)
    throws NafinException;
    
 public Vector getPlazoIF(String ic_producto_nafin, String ic_if)
    throws NafinException;
    
 public abstract Vector getDispVig(String ic_producto_nafin, String ic_if)
    throws NafinException;
 public abstract String getMontoMaxLinea(String ic_producto_nafin, String ic_moneda)
    throws NafinException;
 public abstract String validaCtaMoneda(String ic_if, String folio, String ic_moneda)
    throws NafinException;
 public abstract String validaLineaCredito(String ic_epo, String ic_if, String ic_pyme, String producto)
    throws NafinException;
 public abstract Vector getEstatus_INV(String tipo)
    throws NafinException;
 public abstract Vector getConsultaLC_Inv(String ic_pyme, String ic_if, String ic_moneda,
  String num_disposicion, String f_operacion_ini,String f_operacion_fin, String ic_estatus,
  String monto_credito_ini,String monto_credito_fin, String ic_epo, String tipo,
  String numero_prestamo, String numero_solicitud)
    throws NafinException;
 public abstract Vector getDeatalleMensualidad_INV(String cc_disposicion,String condicion)
    throws NafinException;
 public abstract Vector getDeatallePago_INV(String cc_disposicion, String ic_mensualidad)
    throws NafinException;
/* Cambios Febrero 2003*/
public Vector consultaLC(String ic_linea_credito, String ic_producto, String ic_epo)
    throws NafinException;

public Vector setLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					   String banco, String cuenta, String plazo, String estatus,
					   String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					   String monto_autorizado, String linea_automatica, String linea_individual,
					   String credito_adicional, String linea_total, String ic_linea,
					   String ic_producto, String ic_moneda, String cuenta_clabe)
    throws NafinException;

public Vector consultaLCtmp(String ic_producto, String ic_proceso, String ic_if)
    throws NafinException;

public void delLCtmp(String ic_proceso, String ic_linea)
    throws NafinException;

public void updtLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					  String banco, String cuenta, String plazo, String estatus,
					  String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					  String monto_autorizado, String linea_automatica, String linea_individual,
					  String credito_adicional, String linea_total, String ic_linea,
					  String ic_producto, String linCredito, String ic_moneda, String cuenta_clabe)
    throws NafinException;

public Vector getIcPyme_NafinElec(String ic_epo, String ic_if, String ic_producto)
   throws NafinException;

public Vector getComboLineas(String ic_pyme,String ic_epo,String tipoSolicitud, String ic_producto_nafin, String ic_if)
   throws NafinException;

public String getProductoIF(String ic_if)
   throws NafinException;

public String getLinInicialXMoneda(String ic_pyme, String ic_epo, String ic_if, String ic_producto_nafin, String ic_moneda)
   throws NafinException;
/* ------------------------------------------------------------------------------------------
M�todos nuevos para modificaciones de actualizacion de cuentas clabe y speua
-------------------------------------------------------------------------------------------*/
public Vector getNafinElec_Prod(String ic_linea_credito)
   throws NafinException;

public String validaLineaCreditoIni(String ic_if, String ic_pyme, String tipo_solicitud)
   throws NafinException;

public String validaLineaCreditoIni(String ic_if, String ic_pyme, String tipo_solicitud, String ic_epo)
   throws NafinException;

public String validaLineaCreditoIni(String ic_if, String ic_pyme, String tipo_solicitud, String ic_epo,String ic_producto_nafin)
   throws NafinException;   

public Vector InsertaLC(String producto, String proceso, String ic_if, String iNoUsuario)
   throws NafinException;
   
public Vector InsertaLC(String producto, String proceso, String ic_if, String iNoUsuario,String ccAcuse)
   throws NafinException;   

public Vector BloqueoIF(String folio [],String estatus_ant [],
		String estatus_act [],String estatus_nuevo [],String causa [],
		String banco_ant [],String banco [],String cuenta [],
		String cuentaClabe [],String cuenta_ant[],String cuentaClabe_ant [],
		String [] huboCambio,String tipo[],String sElementos, String moneda[],
		String iNoUsuario,	String monto[], 
		String tipoPagoNuevo[],	//F011-2006
		String tipoPagoAnterior[],	//F011-2006
		String ccAcuse)
    throws NafinException;

public Vector BloqueoIF(String folio [],String estatus_ant [],String estatus_act [],String estatus_nuevo [],String causa [],String banco_ant [],String banco [],String cuenta [],String cuentaClabe [],String cuenta_ant[],String cuentaClabe_ant [],String [] huboCambio,String tipo[],String sElementos, String moneda[],  String iNoUsuario)
    throws NafinException;
    
public Vector BloqueoIF(
				String folio [], String estatus_ant [], String estatus_act [], String estatus_nuevo [],
				String causa [], String banco_ant [], String banco [], String cuenta [], 
				String cuentaClabe [], String cuenta_ant[], String cuentaClabe_ant [], String tipo[], 
				String sElementos, String moneda[], String iNoUsuario, String monto[], 
				String ccAcuse, String fecha_limite[])
    throws NafinException;

public Vector BloqueoIF(
		String folio []
		,String estatus_ant []
		,String estatus_act []
		,String estatus_nuevo []
		,String causa []
		,String banco_ant []
		,String banco []
		,String cuenta []
		,String cuentaClabe []
		,String cuenta_ant[]
		,String cuentaClabe_ant []
		,String huboCambio []
		,String tipo[]
		,String sElementos
		,String moneda[]
		,String iNoUsuario
		,String tipoPagoNuevo []
		,String tipoPagoAnterior [] )
		throws NafinException;


/*-------------------- M�todos para bloqueo -----------------------------------*/
public Vector consultaIFBloqueo(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin, String tipo)
    throws NafinException;

public Vector consultaIFBloqueo(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin, String tipo,String in)
    throws NafinException;	
	
public Vector consultaNafinBloqueo(String ic_epo,String ic_if,String ic_pyme,String ic_linea, String ic_producto_nafin)
    throws NafinException;

//AEC
///////////////////////////////////
	public abstract int existeDocto(String ic_epo, String ic_pyme) throws NafinException;
	public abstract int existeDocto(String ic_pyme) throws NafinException;

/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> IHJ*/
public Vector AutorizacionIF(String iNoUsuario,int numRegistrosMN,double montoAutoMN,int numRegistrosDL,double montoAutoDL,String aTipo[],String tipo_liq,String aFolio[], String aMoneda[], String aMonto [],String aEstatus[],String aTipoCobro[],String aCausa [],String aMontoAuto [],String aFechaVto [],String aBanco [],String aCuenta [],int solMN,double montoMN,int solDL, double montoDL)
    throws NafinException;

public Vector getDescripcion(String ic_moneda,String ic_estatus,String tipo_cobro)
    throws NafinException;

public boolean getDiaHabil()
    throws NafinException;

public Vector getComboSolicitudRelacionada(String ic_if,String ic_epo,String ic_pyme, String solicitud,String moneda, String ic_producto_nafin)
    throws NafinException;

public String getMontoAutorizado(String ic_if, String ic_epo,String ic_pyme, String folio)
	throws NafinException;

 public Vector getMontoAutFechaVto(String ic_if, String ic_epo,String ic_pyme, String folio)
  	throws NafinException;

public Vector getDetalleInicial(String ic_linea_credito)
	throws NafinException;

public boolean getLineaExistente(String moneda,String ic_if,String ic_epo, String ic_pyme)	//DEPRECATED
	throws NafinException;
//SUSTITUYE A getLineaExistente
public void validaLinea(String moneda,String ic_if,String ic_epo,String ic_pyme,String fechaVenc,String tipoSolic)
  throws NafinException;

public Vector getValores(String ic_epo, String ic_pyme, String solicitud, String ic_moneda, String ic_tipo_cobro_interes, String ic_financiera)
	throws NafinException;

public Vector setLineasCredito(String iNoUsuario, String ic_if,String totDocs,String totMtoAuto,String totDocsDol,String totMtoAutoDol,String ic_epos_aux[],String ic_pymes_aux[],String solics_aux[],String folios[],String monedas_aux[],String montoAutos[],String vencimientos[],String tipo_cobro_ints_aux[],String nCtaDists[],String ifBancos[],String nCtaIfs[])
	throws NafinException;

public Vector setLineasCredito(String iNoUsuario, String nombreUsuario, String ic_if,String totDocs,String totMtoAuto,String totDocsDol,String totMtoAutoDol,String ic_epos_aux[],
										String ic_pymes_aux[],String solics_aux[],String monedas_aux[],String montoAutos[],String vencimientos[],
										String tipo_cobro_ints_aux[],String nPlazoMax[], String nCtaBancaria[]);

public Vector getSolicRelac(String ic_linea_credito)
	throws NafinException;
	
public List getLineasExp(String ic_linea_credito[])
	throws NafinException;

public List getLineasExp(String folio, String ic_epo, String ic_pyme, String ic_linea_credito[])
	throws NafinException;

public Acuse setLiberacion(String ic_linea_credito[], String fn_monto_liberar[], String df_liberacion[], String df_lim_liberacion[], String ic_usuario, String cg_recibo_electronico)
	throws NafinException;
/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< IHJ*/
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Agregado 28/02/2003	--CARP
	public Vector getLineasProceso(String esCvePyme,	String esCveEPO,
    								String esTipoSolicitud,String esProducto,
							   		String esCveMoneda)
    throws NafinException;

// Agregado 28/02/2003	--CARP
	public Vector getComboLineas(String esCvePyme, String esCveEPO,
    							 String esTipoSolicitud, int esProducto,
                                 String esCveMoneda)
    throws NafinException;

// Agregado 28/02/2003	--CARP
	public void generaSolicitud(String esLineaCredito, String esCveEPO, String esCvePyme,
    							String esCveIF, String esTipoSolicitud, String ic_linea_padre,
                                String esProducto, String esCveMoneda)
    throws NafinException;

// Agregado 10/03/2003	--CARP
// Monitor de solicitudes Multiproducto
   public Vector getEpoRelacionadaPyme(String esPyme,String esCveIf,
    									String esProducto, String esCveLinea)
    	throws NafinException;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/*/////////////////////////////////////////////////////////////////////////////////////////*/
/*       Metodo para actualizar los numeros de solicitud de troya FODA 099*/
public String ActualizaSolicTroya(String ic_linea,String iClienteTroya, String iSolicTroya)
 	throws NafinException;

public String obtenTipoPiso (String cif)
 	throws NafinException;

public String obtenTipoTroya (String ic_producto_nafin)
 	throws NafinException;

public String obtenClienteTroya (String icPyme)
 	throws NafinException;

public String VerificaSolicTroya(String ic_linea,String iClienteTroya, String iSolicTroya)
 	throws NafinException;
 	
public Vector setLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					   String banco, String cuenta, String plazo, String estatus,
					   String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					   String monto_autorizado, String linea_automatica, String linea_individual,
					   String credito_adicional, String linea_total, String ic_linea,
					   String ic_producto, String ic_moneda, String cuenta_clabe, 
					   String fecha_autorizacion, String solic_troya, String fecha_limite, 
					   String cred_exp, String num_max_amort)
    throws NafinException;

public Vector setLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					   String banco, String cuenta, String plazo, String estatus,
					   String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					   String monto_autorizado, String linea_automatica, String linea_individual,
					   String credito_adicional, String linea_total, String ic_linea,
					   String ic_producto, String ic_moneda, String cuenta_clabe, 
					   String fecha_autorizacion,String solicTroya, String fecha_limite)
    throws NafinException;
    
public Vector setLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					   String banco, String cuenta, String plazo, String estatus,
					   String tipo_solicitud, String causa_rechazo, String monto_solicitado,
					   String monto_autorizado, String linea_automatica, String linea_individual,
					   String credito_adicional, String linea_total, String ic_linea,
					   String ic_producto, String ic_moneda, String cuenta_clabe, 
					   String fecha_autorizacion,String solicTroya)
    throws NafinException;

public void updtLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					  String banco, String cuenta, String plazo, String estatus,
					  String tipo_solicitud, String causa_rechazo, String monto_solicitado, String monto_autorizado, 
					  String linea_automatica, String linea_individual, String credito_adicional, String linea_total, 
					  String ic_linea, String ic_producto, String linCredito, String ic_moneda, 
					  String cuenta_clabe , String fecha_autorizacion, String ic_solic_troya)
    throws NafinException;
    
public void updtLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					  String banco, String cuenta, String plazo, String estatus,
					  String tipo_solicitud, String causa_rechazo, String monto_solicitado, String monto_autorizado, 
					  String linea_automatica, String linea_individual, String credito_adicional, String linea_total, 
					  String ic_linea, String ic_producto, String linCredito, String ic_moneda, 
					  String cuenta_clabe , String fecha_autorizacion, String ic_solic_troya, String fecha_limite)
    throws NafinException;
    
public void updtLCtmp(String ic_proceso, String ic_pyme, String ic_if, String ic_epo,
					  String banco, String cuenta, String plazo, String estatus,
					  String tipo_solicitud, String causa_rechazo, String monto_solicitado, String monto_autorizado, 
					  String linea_automatica, String linea_individual, String credito_adicional, String linea_total, 
					  String ic_linea, String ic_producto, String linCredito, String ic_moneda, 
					  String cuenta_clabe , String fecha_autorizacion, String ic_solic_troya, String fecha_limite,
					  String cred_exp, String num_max_amort)
    throws NafinException;    

public int getLineasAutorizadas2(String ic_pyme, int ic_producto_nafin, String cg_linea_tipo)    
    throws NafinException;

/*/////////////////////////////////////////////////////////////////////////////////////////*/

 public int getNumDisposiciones(String ic_linea_credito)
	throws NafinException;


 public Vector getEstatusXAsignar(String tipoUsuario) throws NafinException;
 public Vector cambiaEstatusLinea(String folios[],String estatus[],String tiposCredito[]) throws NafinException;
 public Vector cambiaEstatusLinea(String folios[],String estatus[],String tiposCredito[], String montosAut[], String saldoTot[], String fechaVence[]);

 public ArrayList getComboPymes(String sNoIf, String sNoEPO, String sNoMoneda, int iNoProducto) throws NafinException;
 public ArrayList getComboEpos(String sNoIf, int iNoProducto) throws NafinException;
 public ArrayList getComboEpos(String sNoIf, int iNoProducto, String ic_modalidad) throws NafinException;
 
 public List getDetLiberacion(String ic_linea_credito)
  throws NafinException;

}
