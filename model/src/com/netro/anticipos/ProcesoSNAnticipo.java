package com.netro.anticipos;

import java.util.*;
import netropology.utilerias.*;
import com.netro.exception.*;
import javax.ejb.Remote;

@Remote
public interface ProcesoSNAnticipo {
    // Metodos para Anticipos "Sirac-Nafinet".

    public String sgetNumaxProcesoSirac() throws NafinException;

    public Hashtable ohprocesarAnticipos(String esDocumentos, String lsProcSirac) throws NafinException;

    public boolean bborrarAnticiposSiracTmp(String lsProcSirac) throws NafinException;

    public boolean bactualizarAnticipo(
	    String esEstatusAntic,
	    String esFechaOperacion,
	    String esNumPrestamo,
	    String esCausaRechazo,
	    String esPedido
    ) throws NafinException;

    public Vector ovgetAnticiposProcesados(String lsProcSirac) throws NafinException;

    public Hashtable ohprocesarAnticipos(String esDocumentos, String lsProcSirac, String lsProducto) throws NafinException;

    public boolean bactualizarAnticipo(
	    String esEstatusAntic,
	    String esFechaOperacion,
	    String esNumPrestamo,
	    String esCausaRechazo,
	    String esPedido,
	    String lsProducto
    ) throws NafinException;

    public Vector ovgetAnticiposProcesados(String lsProcSirac, String lsProducto) throws NafinException;									
	
}