package com.netro.anticipos;

import com.netro.exception.NafinException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;

@Remote
public interface CapturaTasas{

	// Metodos para Anticipos "Capturas-Tasas".
	public abstract boolean binsertaTasa(String esCveTasa, String esRelMat, String esPuntos) throws  NafinException;
	public abstract boolean binsertaTasa(String esCveTasa, String esRelMat, String esPuntos, String ic_producto_nafin) throws  NafinException;
	public abstract Vector ovgetDatosTasa(String esCveTasa) throws  NafinException;
	public abstract Vector ovgetDatosTasaCap(String esCveTasa) throws  NafinException;  
	// Metodos para Anticipos "Capturas-Tasas".
	public abstract Vector ovgetTasas(int ic_producto_nafin) throws  NafinException;
	public abstract Vector ovgetTasasInvent(int ic_producto_nafin) throws  NafinException;
	public abstract Vector ovgetTasas(int ic_producto_nafin,String ic_monedas) throws NafinException; 
	public abstract Vector ovgetTasasxEpo(int ic_producto_nafin, String ic_epo,String ic_if) throws  NafinException;
	public abstract Vector ovgetTasasDisponibles(int ic_producto_nafin) throws  NafinException;

	public abstract boolean binsertaTasaDis(String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos) throws  NafinException;
	public abstract boolean binsertaTasaInvent(String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos, String ic_plazo, String ic_tasa_general, String esPuntosEmp) throws  NafinException;
	public abstract boolean binsertaTasaDisxEpo(String ic_epo,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos) throws  NafinException;
	public abstract boolean eliminarTasasDis(String esCveTasa)  throws  NafinException;
	public abstract boolean eliminarTasasDisxEpo(String ic_epo,String ic_if,String esCveTasa)  throws  NafinException;
	public abstract Vector ovgetDatosTasaDis(String pAccion,int ic_producto_nafin,String esCveTasa) throws  NafinException;
	public abstract Vector ovgetDatosTasaDisxEpo(String ic_epo,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa) throws  NafinException;


  public abstract String getAuto(String ic_if,String ic_epo) throws  NafinException;
	public abstract Vector disOpera(String ic_epo,int ic_producto_nafin, String ic_pyme) throws  NafinException;
	public abstract String paramPagoInteres(String ic_epo,int ic_producto_nafin) throws  NafinException;
	public abstract Vector esquemaParticular(String ic_epo,int ic_producto_nafin,String autorizado,String ic_pyme,String cg_tipo_credito) throws  NafinException;
	public abstract Vector esquemaParticular(String ic_epo,int ic_producto_nafin,String autorizado, String ic_pyme) throws  NafinException;

//>>>Israel Herrera
  public abstract String getPuntosMaximosxIf(String ic_producto_nafin, String ic_if)
    throws  NafinException;  
  public abstract int getTotalLineas(String ic_producto_nafin,String ic_epo,String ic_if,String ic_moneda)
    throws  NafinException;
  public Vector getTasasDispxEpo(String ic_producto_nafin,String ic_epo,String ic_if,String ic_moneda)
    throws  NafinException;
	public abstract Vector ovgetTasasxEpo(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda,String ic_plazo)
    throws  NafinException;
	
  public abstract boolean setTasasxEpo(String ic_epo,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos)
    throws  NafinException;
  public abstract int getTotalLineas(String ic_producto_nafin,String ic_epo,String ic_if,String ic_pyme,String ic_moneda)
    throws  NafinException;
  public abstract Vector getComboPlazo(int ic_producto_nafin, String ic_epo)
   throws  NafinException;
	
	public Vector getComboPlazoFactoraje(int ic_producto_nafin, String ic_epo, String cadPlazos);	//Agregado por Hugo para el caso de Factoraje con Rec.
 	
	public abstract Vector ovgetTasasxPyme(int ic_producto_nafin, String ic_pyme,String ic_if,String ic_moneda)
   throws  NafinException;
  public abstract Vector getTasasDispxPyme(String ic_producto_nafin,String ic_pyme,String ic_if,String ic_moneda, String in_plazo_dias)
   throws  NafinException;

	public Vector getTasasDispxPyme(String ic_producto_nafin,String ic_pyme,String ic_if,String ic_moneda, String in_plazo_dias,	boolean fac);

  public abstract boolean setTasasxPyme(String ic_pyme,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos)
   throws  NafinException;

	public boolean setTasasxPyme(String ic_pyme,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos, String usuario);

	public abstract boolean binsertaTasaDisxPyme(String ic_pyme,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos)
   throws  NafinException;

	public boolean binsertaTasaDisxPyme(String ic_pyme,String ic_if,String pAccion,int ic_producto_nafin,String esCveTasa, String esRelMat, String esPuntos, String usuario);
	
	public boolean eliminaTasaDisxPyme(String ic_pyme,	String ic_if,	int ic_producto_nafin,	String esCveTasa[],	String usuario);
   
	public abstract Vector getComboPlazoAmort(String in)
   throws  NafinException;
   
	public abstract Vector getComboPlazoAmort(String in,String monedas)
   throws  NafinException;
   
	public abstract Vector ovgetTasasDisponiblesInvet(String ic_producto_nafin)
   throws  NafinException;
 
   
	public List getTasasPrefCred(String ic_producto_nafin, String ic_if, String ic_pyme, String ic_moneda, String cg_tipo)
   throws  NafinException;
	
	public List getTasasCredicadenas(String ic_producto_nafin, String ic_if, String ic_pyme[], String ic_moneda, String tipo_tasa)
   throws  NafinException;
//<<<Israel Herrera
 public abstract void actualizaParamTasas(String ic_epo,String ic_producto_nafin)
   throws  NafinException;
/** FODEA-005-2014
 * SE AGREGA NUEVO PARAMETRO tipoCredito 
*/
 public abstract Vector ovgetTasasxEpo(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda,String ic_plazo,String ic_pyme,String tipoCredito)
   throws  NafinException;
 
 
   
 public abstract void guardaTasaPreferencial(String ic_epo,String ic_pyme,String ic_if,String ic_moneda,String tipo_tasa,String puntos)
   throws NafinException;
/** FODEA-005-2014
 * SE AGREGA NUEVO PARAMETRO tipoCredito 
*/
 public abstract void guardaTasaPreferencial(String ic_epo,String ic_pyme,String ic_if,String ic_moneda,String tipo_tasa,String puntos, int ic_producto_nafin
											 ,String cc_acuse,String ic_usuario,String _acuse, String icplazo, String tipoCredito)
   throws NafinException;

 public abstract void guardaTasaNegociada(String ic_epo,String ic_pyme,String ic_if,String ic_moneda,String tipo_tasa,String valor_tasa_neg,String ic_tasa)
	throws NafinException;
/** FODEA-005-2014
 * SE AGREGA NUEVO PARAMETRO tipoCredito 
*/ 	
 public abstract void guardaTasaNegociada(String ic_epo,String ic_pyme,String ic_if,String ic_moneda,String tipo_tasa,String valor_tasa_neg,String ic_tasa, int ic_producto_nafin
											,String cc_acuse,String ic_usuario,String _acuse,String tipoCredito) 
	throws NafinException;

 public abstract ArrayList getComboPlazo(int iNoProductoNafin, int iNoMoneda)
   throws  NafinException; 
 public abstract ArrayList getTasasPreferenciales(int iNoProductoNafin, int iNoMoneda)
   throws  NafinException;
 public abstract ArrayList getTasaNegociada(int iNoProductoNafin, String sNoEpo, String sNoIf, String sNoPyme, String sNoPlazo, int iNoMoneda)
   throws  NafinException; 
   
 public Vector ovgetTasasInvent(int ic_producto_nafin, String ic_moneda)
	throws  NafinException;
	
 public Acuse guardaTasaPreferencialCred(
					String ic_pyme, String ic_if, String ic_moneda, 
					String cg_rel_mat, String fn_puntos, String ic_producto_nafin, 
					String ic_usuario, String cg_recibo_electronico)
	throws  NafinException;
	
public Vector getComboPlazoAmort(String in, String monedas, String tipoLinea)
	throws  NafinException;	
		

	public Vector ovgetTasasPreferNego(String ic_producto_nafin, String cg_tipo, String ic_if, String ic_epo, String ic_moneda, String ic_pyme)
		throws  NafinException;	
		
	public Vector ovgetTasasxEpo(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda)
		throws  NafinException;	
		
	public void guardaTasaPreferencial(String ic_epo, String ic_pyme, String ic_if, String ic_moneda,
									   String tipo_tasa, String puntos, int ic_producto_nafin
									   ,String cc_acuse,String ic_usuario,String _acuse)throws  NafinException;	
									
	public List getEstatusDeTasas ();

	public Map cambioTasas(String[] txt_cambio) throws AppException;
	public Map getEncabezadosProductos() throws AppException;
	
	/**
	 * FODEA-005-2104 
	 * 
	 */
	 public Vector ovgetTasasxEpo1(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda,String ic_plazo,String ic_pyme,String tipoCredito)
    throws NafinException;
	 public Vector getCatalogoPlazo(int ic_producto_nafin, String ic_epo,String ic_if,String ic_moneda,String ic_plazo)
     throws NafinException;
	 
	 public Vector getTasasPreferNego(String ic_producto_nafin, String cg_tipo, String ic_if, String ic_epo, String ic_pyme, String ic_moneda, String modalidad   ) throws NafinException;  // FODEA-005-2104 
	 
	 public Vector getCatalogoPlazoPyme(int ic_producto_nafin, String ic_pyme,String ic_if,String ic_moneda,String ic_plazo)
    throws NafinException;
	 /** FODEA-005-2014
 * SE AGREGA NUEVO PARAMETRO tipoCredito 
*/
	public abstract Vector ovgetTasasPreferNego(String ic_producto_nafin, String cg_tipo, String ic_if, String ic_epo, String ic_moneda, String ic_pyme, String ic_plazo, String tipoCredito)
   throws  NafinException;

	public void  setBitacoraTasasPyme(String ic_if, String strLogin, String strNombreUsuario, String Anteriores, String Actuales  )throws  NafinException;  
  
}