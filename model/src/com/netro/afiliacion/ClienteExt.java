package com.netro.afiliacion;

public class ClienteExt implements java.io.Serializable
{
	private String 	txtNoSirac;
	private String 	txtRfc;
	private String 	tipoPersona;
	private String 	txtRazonSoc;
	private String 	txtApellidoP;
	private String 	txtApellidoM;
	private String 	txtNombre;
	private String		operacion;
	private String		rdoafiliar;
	//Agregados para la afiliacion
	private String		txtCalle;
	private String		txtColonia;
	private String		txtCP;
	private String		cboPais;
	private String		cboEstado;
	private String		cboDelegacion;
	private String		txtTelefono;
	private String		txtEmail;
	private String		txtFax;
	private String		txtCApellidoP;
	private String		txtCApellidoM;
	private String		txtCNombre;
	private String		txtCTelefono;
	private String		txtCFax;
	private String		txtCEmail;
	private String		txtCProveedor;
	private String		txtCConfirmEmail;
	private int 		hayDatos;
	
	//GETERS
	public String getTxtNoSirac() {
		return this.txtNoSirac;
	}
	public String getTxtRfc() {
		return this.txtRfc;
	}
	public String getTipoPersona() {
		return this.tipoPersona;
	}
	public String getTxtRazonSoc() {
		return this.txtRazonSoc;
	}
	public String getTxtApellidoP() {
		return this.txtApellidoP;
	}
	public String getTxtApellidoM() {	
		return this.txtApellidoM;
	}
	public String getTxtNombre() {
		return this.txtNombre;
	}
	public String getOperacion() {
		return this.operacion;
	}
	public String getRdoafiliar() {return this.rdoafiliar;}
	public String getTxtCalle() {return this.txtCalle;}
	public String getTxtColonia() {return this.txtColonia;}
	public String getTxtCP() {return this.txtCP;}
	public String getCboPais() {return this.cboPais;}
	public String getCboEstado() {return this.cboEstado;}
	public String getCboDelegacion() {return this.cboDelegacion;}
	public String getTxtTelefono() {return this.txtTelefono;}
	public String getTxtEmail() {return this.txtEmail;}
	public String getTxtFax() {return this.txtFax;}
	public String getTxtCApellidoP() {return this.txtCApellidoP;}
	public String getTxtCApellidoM() {return this.txtCApellidoM;}
	public String getTxtCNombre() {return this.txtCNombre;}
	public String getTxtCTelefono() {return this.txtCTelefono;}
	public String getTxtCFax() {return this.txtCFax;}
	public String getTxtCEmail() {return this.txtCEmail;}
	public String getTxtCProveedor() {return this.txtCProveedor;}
	public String getTxtCConfirmEmail() {return this.txtCConfirmEmail;}
	public int getHayDatos(){return hayDatos;}
	
	//SETERS
	public void setTxtNoSirac(String txtNoSirac) {
		this.txtNoSirac = txtNoSirac;
	}
	public void setTxtRfc(String txtRfc) {
		this.txtRfc = txtRfc;
	}
	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public void setTxtRazonSoc(String txtRazonSoc) {
		this.txtRazonSoc = txtRazonSoc;
	}
	public void setTxtApellidoP(String txtApellidoP) {
		this.txtApellidoP = txtApellidoP;
	}
	public void setTxtApellidoM(String txtApellidoM) {
		this.txtApellidoM = txtApellidoM;
	}
	public void setTxtNombre(String txtNombre) {
		this.txtNombre = txtNombre;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public void setRdoafiliar(String rdoafiliar) {this.rdoafiliar = rdoafiliar;}
	public void setTxtCalle(String txtCalle) {this.txtCalle = txtCalle;}
	public void setTxtColonia(String txtColonia) {this.txtColonia = txtColonia;}
	public void setTxtCP(String txtCP) {this.txtCP = txtCP;}
	public void setCboPais(String cboPais) {this.cboPais = cboPais;}
	public void setCboEstado(String cboEstado) {this.cboEstado = cboEstado;}
	public void setCboDelegacion(String cboDelegacion) {this.cboDelegacion = cboDelegacion;}
	public void setTxtTelefono(String txtTelefono) {this.txtTelefono = txtTelefono;}
	public void setTxtEmail(String txtEmail) {this.txtEmail = txtEmail;}
	public void setTxtFax(String txtFax) {this.txtFax = txtFax;}
	public void setTxtCApellidoP(String txtCApellidoP) {this.txtCApellidoP = txtCApellidoP;}
	public void setTxtCApellidoM(String txtCApellidoM) {this.txtCApellidoM = txtCApellidoM;}
	public void setTxtCNombre(String txtCNombre) {this.txtCNombre = txtCNombre;}
	public void setTxtCTelefono(String txtCTelefono) {this.txtCTelefono = txtCTelefono;}
	public void setTxtCFax(String txtCFax) {this.txtCFax = txtCFax;}
	public void setTxtCEmail(String txtCEmail) {this.txtCEmail = txtCEmail;}
	public void setTxtCProveedor(String txtCProveedor) {this.txtCProveedor = txtCProveedor;}
	public void setTxtCConfirmEmail(String txtCConfirmEmail) {this.txtCConfirmEmail = txtCConfirmEmail;}
	public void setHayDatos(int hayDatos){this.hayDatos=hayDatos;}
	
	public String toString()
  {
    String cadena = "";
    cadena = " No Sirac = "+txtNoSirac+
             " Rfc = "+txtRfc+
             " Tipo de Persona = "+tipoPersona+
             " Razon Social = "+txtRazonSoc+
             " Apellido Paterno = "+txtApellidoP+
             " Apellido Materno = "+txtApellidoM+
             " Nombre(s) = "+txtNombre+
             " Tipo de operacion = "+operacion+
             " Afiliado = "+rdoafiliar+
             " Calle =" +txtCalle+
             " Colonia ="+txtColonia+
             " C�digo Postal = "+txtCP+
             " Pais = "+cboPais+
             " Estado = "+cboEstado+
             " Delegacion = "+cboDelegacion+
             " Telefono = "+txtTelefono+
             " Mail = "+txtEmail+
             " Fax = "+txtFax+
             " Apellido Paterno de Contacto = "+txtCApellidoP+
             " Apellido Materno de Contacto = "+txtCApellidoM+
             " Nombre de Contacto = "+txtCNombre+
             " Telefono de Contacto = "+txtCTelefono+
             " Fax de Contacto = "+txtCFax+
             " Mail de Contacto = "+txtCEmail+
             " Proveedor de Contacto = "+txtCProveedor+
             " Confirmacion de Mail de Contacto= "+txtCConfirmEmail;
    return cadena;
  }
	
}