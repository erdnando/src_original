package com.netro.afiliacion;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class AcuseEnl {

	private String	cc_acuse			= "";

	private int 	in_reg_proc			= 0;
	private int 	in_reg_ok			= 0;
	private int 	in_reg_rech			= 0;

	public AcuseEnl () {
		Calendar cal= Calendar.getInstance();
		SimpleDateFormat sdf=new SimpleDateFormat("ddMMyyyyhhmmss");
		java.util.Date facuse=cal.getTime();
		this.cc_acuse = sdf.format(facuse).toString();

	}

	public void setCcAcuse(String cc_acuse)	{
		this.cc_acuse = cc_acuse;
	}

	public void addRegProc(){
		this.in_reg_proc = this.in_reg_proc + 1;
	}
	public void addRegOk(){
		this.in_reg_ok = this.in_reg_ok + 1;
	}
	public void addRegRech(){
		this.in_reg_rech = this.in_reg_rech + 1;
	}

	public String getCcAcuse(){
		return this.cc_acuse;
	}
	public int 	getRegProc(){
		return this.in_reg_proc;
	}
	public int getRegOk(){
		return this.in_reg_ok;
	}
	public int getRegRech(){
		return this.in_reg_rech;
	}

	public String toString(){
		String strAcuse =
			"\n[\nAcuse				= "+cc_acuse+
			",\nTotal RegProc		= "+in_reg_proc+
			",\nTotal Reg Ok    	= "+in_reg_ok+
			",\nTotal Reg Rech		= "+in_reg_rech+
			"\n]";

		return strAcuse;
	}

}
