/********************************************************************
*  
* Nombre de Clase o de Archivo: Afiliacion.java
* Versi�n: 0.1  
* Fecha Creaci�n:12/02/2002
* Autor: Javier Cuanalo
* Fecha Ult. Modificaci�n:
* Descripci�n de Clase:
*
********************************************************************/

package com.netro.afiliacion;

import com.netro.exception.NafinException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.AppException;
import netropology.utilerias.Registros;

@Remote
public interface Afiliacion{

	public String afiliaPyme(
			String claveUsuario,
			String cboTipoAfiliacion,
			String iCveEpo,
			String tp,
			String Num_Sirac,
			String Numero_de_cliente,
			String Nombre_comercial,
			String Razon_Social,
			String chkDescuento,
			String R_F_C,
			String Calle,
			String Colonia,
			String Estado,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Pais,
			String Telefono,
			String Email,
			String Fax,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Fax_C,
			String Email_C,
			String Numero_Escritura,
			String Fecha_Constitucion,
			String generarCuenta,
			String Sin_Num_Prov,
			String cuenta_usuario_asignada,
			String cesion_derechos, //Fodea 037-2009
			String fianza_electronica, //Fodea ###-2011
			String OperaFideicomiso,
			String chkEntidadGobierno,  //F034-2014 
			String chkFactDistribuido,  //F034-2014 
			String chkProvExtranjero,  //2019 QC
			String chkOperaDescAutoEPO  //2019 QC
                        
	) throws NafinException;


	public Vector afiliaPyme(
			String claveUsuario,
			String cboTipoAfiliacion,
			String iCveEpo,
			String tp,
			String Num_Sirac,
			String Numero_de_cliente,
			String Nombre_comercial,
			String Razon_Social,
			String chkDescuento,
			String R_F_C,
			String Calle,
			String Colonia,
			String Estado,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Pais,
			String Telefono,
			String Email,
			String Fax,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Fax_C,
			String Email_C,
			String chlGeneraCuenta
	) throws NafinException;

	public String afiliaIF(
			String claveUsuario,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Ejecutivo_de_cuenta,
			String Domicilio_correspondencia,
			String Numero_de_IF,
			String Tipo_de_IF,
			String Avales,
			String Limite_maximo_endeudamiento,
			String Facultades,
			String Porcentaje_capital_contable,
			String Ente_contable,
			String Oficina_controladora,
			String Viabilidad,
			String Calle,
			String Colonia,
			String Estado,
			String Pais,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Telefono,
			String Email,
			String Fax,
			String Fecha_convenio,
			String Fecha_convenio_anticipo,
			String Tipo_de_IF_piso,
			String msChkFinan,
			String enlace_aut,
			String nombre_tabla,
			String autorizauto_opersf,
			String tipo_riesgo,
			String if_siag
			) throws NafinException;

	public String afiliaIF(
			String claveUsuario,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Ejecutivo_de_cuenta,
			String Domicilio_correspondencia,
			String Numero_de_IF,
			String Tipo_de_IF,
			String Avales,
			String Limite_maximo_endeudamiento,
			String Facultades,
			String Porcentaje_capital_contable,
			String Ente_contable,
			String Oficina_controladora,
			String Viabilidad,
			String Calle,
			String Colonia,
			String Estado,
			String Pais,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Telefono,
			String Email,
			String Fax,
			String Fecha_convenio,
			String Fecha_convenio_anticipo,
			String Tipo_de_IF_piso,
			String msChkFinan,
			String enlace_aut,
			String nombre_tabla,
			String autorizauto_opersf,
			String tipo_riesgo,
			String if_siag,
			String if_sucre,
			String convenio_unico, //FODEA 020 - 2009
			String mandato_documento, //FODEA 041 2009
			String claveDescontante1, //FODEA 012 - 2010 ACF
			String claveDescontante2, //FODEA 012 - 2010 ACF
			String tipoEpoInstruccion, //FODEA 033 - 2010 ACF
			String  fideicomiso,  // Fodea 
			String num_sirac,
			String chkFactDistribuido,  //QC2018
			String opMontosMen
			)throws NafinException;

	public String afiliaContragarante(
			String claveUsuario,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Domicilio_correspondencia,
			String Oficina_controladora,
			String Calle,
			String Colonia,
			String Estado,
			String Pais,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Telefono,
			String Email,
			String Fax,
			String ic_contragarante
			)throws NafinException;


	public String afiliaDF(
				String claveUsuario,
				String Razon_Social,
				String Nombre_comercial,
				String R_F_C,
				String Domicilio_correspondencia,
				String Calle,
				String Colonia,
				String Estado,
				String Pais,
				String Delegacion_o_municipio,
				String Codigo_postal,
				String Telefono,
				String Email,
				String Fax,
				String url,
				String distribuidor_nafin,
				String nombre_contacto,
				String cargo_contacto,
				String area_contacto,
				String telefono_contacto,
				String Fecha_convenio,
				String contacto_Centro_A_tel,
				String tel_Centro_A_tel,
				String informacion_general
		)throws NafinException;


		public void actualizaDF(
			String ic_distribuidor,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Domicilio_correspondencia,
			String nombre_contacto,
			String cargo_contacto,
			String area_contacto,
			String telefono_contacto,
			String Fecha_convenio,
			String contacto_Centro_A_tel,
			String tel_Centro_A_tel,
			String informacion_general,
			String distribuidor_nafin,
			String url,
			String ic_domicilio,
			String Calle,
			String Colonia,
			String Estado,
			String Pais,
			String Codigo_postal,
			String Telefono,
			String Fax,
			String Email,
			String Delegacion
		)throws NafinException;


	public String afiliaMasiva(
			String claveUsuario,
			String cboTipoAfiliacion,
			String iCveEpo,
			String tp,
			String Num_Sirac,
			String Numero_de_cliente,
			String Nombre_comercial,
			String Razon_Social,
			String chkDescuento,
			String R_F_C,
			String Calle,
			String Colonia,
			String Estado,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Pais,
			String Telefono,
			String Email,
			String Fax,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Fax_C,
			String Email_C
	)throws NafinException;

	public Vector consultaDomicilio(
			String tipo,
			String numCliente,
			String ic_domicilio_epo
	)throws NafinException;

	public void insertaDomicilioAdicional(String numCliente, String tipo, String Calle, String iDelMun, String strDelMun,
			String Codigo_postal, String Pais, String Telefono_1, String Telefono_2,
			String Colonia, String Estado, String Fax, String Email, String Nun_Ext, String Nun_Int
	)throws NafinException;

	public void actualizaDomicilioAdicional(String numCliente, String tipo, String inden, String Calle, String iDelMun, String strDelMun,
				String Codigo_postal, String Pais, String Telefono_1, String Telefono_2,
				String Colonia, String Estado, String Fax, String Email, String Nun_Ext, String Nun_Int
	)throws NafinException;

	public void borraDomicilioAdicional(String numCliente, String tipo, String inden
	)throws NafinException;

	public Vector consultaContacto(
			String tipo,
			String numCliente,
			String ic_domicilio_epo
	)throws NafinException;

	public void insertaContactoAdicional(String numCliente, String tipo, String Apellido_Paterno,
			 String Apellido_Materno, String Nombre, String Telefono, String Fax, String Mail
	)throws NafinException;

	public void actualizaContactoAdicional(String numCliente, String tipo, String inden, String Apellido_Paterno,
		String Apellido_Materno, String Nombre, String Telefono, String Fax, String Mail
	)throws NafinException;

	public void borraContactoAdicional(String numCliente, String tipo, String inden
	)throws NafinException;

	public Vector afiliaDescuento(
			String claveUsuario,
			String iNoEPO2,
			String iNoCliente2,
			String txtIC_Domicilio,
			String txtIC_Contacto,
			String txtTipoPersona,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String R_F_C,
			String Apellido_casada,
			String Sexo,
			String Estado_civil,
			String Fecha_de_nacimiento,
			String Pais_de_origen,
			String Nombre_Comercial,
			String Calle,
			String Colonia,
			String Delegacion_o_municipio,
			String Estado,
			String Codigo_postal,
			String Pais,
			String Telefono,
			String Fax,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String Tel_Rep_Legal,
			String Fax_Rep_Legal,
			String Email_Rep_Legal,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Fax_C,
			String Email_C,
			String Identificacion,
			String Grado_escolaridad,
			String Tipo_categoria,
			String Ejecutivo_de_cuenta,
			String No_de_escritura,
			String No_de_notaria,
			String Empleos_a_generar,
			String Activo_total,
			String Capital_contable,
			String Ventas_netas_exportacion,
			String Ventas_netas_totales,
			String Fecha_de_Constitucion,
			String Numero_de_empleados,
			String Subsector,
			String Rama,
			String Principales_productos,
			String Tipo_empresa,
			String Domicilio_Correspondencia,
			String Clase,
			String Sector_economico,
			String r_matrimonial,
			String d_proveedores,
			String Rfc_Rep_Legal,
			String fecha,
			String No_Identificacion,
			String desc_automatico,
			String alta_troya,
			String Version_Convenio,
			String strOficinaTramitadora,
			String cuenta_usuario_asignada,
      String convenio_unico,//FODEA 020 - 2009
      String fecha_convenio_unico,//FODEA 020 - 2009
      String fiel, //Fodea 00-2010
      String curp, // Fodea 00-2010
      String ciudad,//Fodea 00-2010
		String csNotificaciones
//			String tipoSectorEmpresa
			)throws NafinException;


	public List afiliaDescuentoInternacional(
			String claveUsuario,
			String iNoEPO2, String iNoCliente2,
			String txtIC_Domicilio, String txtIC_Contacto,
			String txtTipoPersona, String NIT,
			String Apellido_paterno, String Apellido_materno,
			String Nombre, String R_F_C,
			String Apellido_casada, String Sexo,
			String Estado_civil, String r_matrimonial,
			String Fecha_de_nacimiento, String Pais_de_origen,
			String Nombre_Comercial, String Calle,
			String Colonia, String Delegacion_o_municipio,
			String Estado, String Codigo_postal,
			String Pais, String Telefono,
			String Fax, String Apellido_paterno_L,
			String Apellido_materno_L, String Nombre_L,
			String Rfc_Rep_Legal, String Tel_Rep_Legal,
			String Fax_Rep_Legal, String Email_Rep_Legal,
			String Apellido_paterno_C, String Apellido_materno_C,
			String Nombre_C, String Telefono_C,
			String Fax_C, String Email_C,
			String Identificacion, String No_Identificacion,
			String Grado_escolaridad, String Tipo_categoria,
			String Ejecutivo_de_cuenta, String No_de_escritura,
			String No_de_notaria, String Empleos_a_generar,
			String Activo_total, String Capital_contable,
			String Ventas_netas_exportacion, String Ventas_netas_totales,
			String Fecha_de_Constitucion, String Numero_de_empleados,
			String Sector_economico, String Subsector,
			String Rama, String Clase,
			String Principales_productos, String Tipo_empresa,
			String Domicilio_Correspondencia, String d_proveedores,
			String fecha, String desc_automatico,
			String alta_troya,String Version_Convenio,
      String convenio_unico,//FODEA 020 - 2009
      String fecha_convenio_unico//FODEA 020 - 2009
			//, String tipoSectorEmpresa
			) throws NafinException;

	public String borraEpo(
			String claveUsuario,
			String ic_epo
			)throws NafinException;
	
	public HashMap consultaUsuarioUniversidad (
			String claveUsuario, 
			String tipoAfiliado
			) throws NafinException ;
	
	public String borraUniversidad(
			String claveUsuario,
			String ic_univer
			) throws NafinException;
			
	public void actualizaUniversidad(
			String claveUsuario,
			String ic_universidad,
			String Grupo,
			String Razon_Social,
			String R_F_C,
			String Campus,
			String Estatus,			
			String Calle,
			String Colonia,
			String Codigo_postal,
			String Pais,
			String Estado,			
			String Delegacion_o_municipio,
			String Telefono,
			String Email,
			String Fax,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String Identificacion,
			String No_Identificacion,
			String Numero_de_escritura,
			String Fecha_del_poder_notarial
			) throws NafinException;
			
	public void actualizaEpo(
			String claveUsuario,
			String ic_epo,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Numero_de_escritura,
			String Fecha_del_poder_notarial,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String c_proveedores,
/*			String ic_contacto,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Fax_C,
			String Email_C,
*/
			String ic_domicilio,
			String Calle,
			String Numero_Exterior,
			String Numero_interior,
			String Colonia,
			String Estado,
			String Pais,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Telefono,
			String Email,
			String Fax,
			String cliente_primer_piso,
			String no_sirac,
			String chkFinanciamiento,
			String BancoRetiro,
			String CtaRetiro,
			String SSId_Number,
			String Dab_Number
	) throws NafinException;
	
	public void actualizaEpo(
			String claveUsuario,
			String ic_epo,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Numero_de_escritura,
			String Fecha_del_poder_notarial,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String c_proveedores,
			String ic_domicilio,
			String Calle,
			String Numero_Exterior,
			String Numero_interior,
			String Colonia,
			String Estado,
			String Pais,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Telefono,
			String Email,
			String Fax,
			String cliente_primer_piso,
			String no_sirac,
			String chkFinanciamiento,
			String BancoRetiro,
			String CtaRetiro,
			String SSId_Number,
			String Dab_Number,
			String noBancoFondeo,
			String ramo_siaff,//FODEA 034 - 2009 ACF
			String unidad_siaff,//FODEA 034 - 2009 ACF
			String convenio_unico,  //FODEA 040 - 2009 FVR
			String mandato_documento,  // Fodea 041-2009
			String sector_epo,  // Fodea 057-2010 FVR
			String subdireccion,  // Fodea 057-2010 FVR
			String lider_promotor  // Fodea 057-2010 FVR
	) throws NafinException;
			
		public void actualizaEpo(
			String claveUsuario,
			String ic_epo,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Numero_de_escritura,
			String Fecha_del_poder_notarial,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String c_proveedores,
			String ic_domicilio,
			String Calle,
			String Numero_Exterior,
			String Numero_interior,
			String Colonia,
			String Estado,
			String Pais,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Telefono,
			String Email,
			String Fax,
			String cliente_primer_piso,
			String no_sirac,
			String chkFinanciamiento,
			String BancoRetiro,
			String CtaRetiro,
			String SSId_Number,
			String Dab_Number,
			String noBancoFondeo,
			String ramo_siaff,//FODEA 034 - 2009 ACF
			String unidad_siaff,//FODEA 034 - 2009 ACF
			String convenio_unico, //FODEA 040 - 2009 FVR
			String mandato_documento,  // Fodea 041-2009
			String sector_epo,  // Fodea 057-2010 FVR
			String subdireccion,  // Fodea 057-2010 FVR
			String lider_promotor,  // Fodea 057-2010 FVR
			String sector_econ,		//Fodea023-2012
			String subsector_econ,  //Fodea023-2012
			String rama_econ			//Fodea023-2012
	) throws NafinException;

	public String borraPyme(
			String claveUsuario,
			String ic_pyme,
			String ic_epo/*,
			String strTipoUsuario,
			String iNoEPO*/
	)throws NafinException;

	public Vector actualizaPyme(
			String claveUsuario,
			String ic_epo,
			String ic_pyme,
			String Razon_Social,
			String Nombre_Comercial,
			String R_F_C,
			String chkDescuento,
			String Num_Sirac,
			String Numero_Exterior,
			String Colonia,
			String Estado,
			String Fecha_de_nacimiento,
			String chkAnticipo,
			String Estado_civil,
			String Pais_de_origen,
			String TAfilia,
			String Sector_economico,
			String Numero_de_empleados,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String Telefono,
			String Fax,
			String Sexo,
			String r_matrimonial,
			String Apellido_casada,
			String No_identificacion,
			String txtTipoPersona,
			String Calle,
			String Codigo_postal,
			String Delegacion_o_municipio,
			String Pais,
			String Grado_escolaridad,
			String No_de_notaria,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String Tel_Rep_Legal,
			String Fax_Rep_Legal,
			String Email_Rep_Legal,
			String RFC_legal,
			String Identificacion,
			String Tipo_categoria,
			String Ejecutivo_de_cuenta,
			String Empleos_a_generar,
			String Activo_total,
			String Capital_contable,
			String Ventas_netas_totales,
			String Ventas_netas_exportacion,
			String Fecha_de_Constitucion,
			String Subsector,
			String Rama,
			String Clase,
			String Tipo_empresa,
			String Domicilio_Correspondencia,
			String Principales_productos,
			String ic_domicilio,
			String No_de_escritura,
			String ic_contacto,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Email_C,
			String Fax_C,
			String Numero_de_cliente,
			String Numero_de_distribuidor,
			String chkFinanciamiento,
			String chkInventario,
			String Num_Troya,
			String strOficinaTramitadoraDescuento,
			String strOficinaTramitadoraAnticipo,
			String strOficinaTramitadoraInventario,
			//String tipoSectorEmpresa,
			boolean invalidar,
			boolean actualizaSirac
			) throws NafinException;

		//Pymes Internacionales
	public Vector actualizaPyme(
			String claveUsuario,
			String ic_epo,
			String ic_pyme,
			String Razon_Social,
			String Nombre_Comercial,
			String R_F_C,
			String chkDescuento,
			String Num_Sirac,
			String Numero_Exterior,
			String Colonia,
			String Estado,
			String Fecha_de_nacimiento,
			String chkAnticipo,
			String Estado_civil,
			String Pais_de_origen,
			String TAfilia,
			String Sector_economico,
			String Numero_de_empleados,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String Telefono,
			String Fax,
			String Sexo,
			String r_matrimonial,
			String Apellido_casada,
			String No_identificacion,
			String txtTipoPersona,
			String Calle,
			String Codigo_postal,
			String Delegacion_o_municipio,
			String Pais,
			String Grado_escolaridad,
			String No_de_notaria,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String Tel_Rep_Legal,
			String Fax_Rep_Legal,
			String Email_Rep_Legal,
			String RFC_legal,
			String Identificacion,
			String Tipo_categoria,
			String Ejecutivo_de_cuenta,
			String Empleos_a_generar,
			String Activo_total,
			String Capital_contable,
			String Ventas_netas_totales,
			String Ventas_netas_exportacion,
			String Fecha_de_Constitucion,
			String Subsector,
			String Rama,
			String Clase,
			String Tipo_empresa,
			String Domicilio_Correspondencia,
			String Principales_productos,
			String ic_domicilio,
			String No_de_escritura,
			String ic_contacto,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Email_C,
			String Fax_C,
			String Numero_de_cliente,
			String Numero_de_distribuidor,
			String chkFinanciamiento,
			String chkInventario,
			String Num_Troya,
			String strOficinaTramitadoraDescuento,
			String strOficinaTramitadoraAnticipo,
			String strOficinaTramitadoraInventario,
			//String tipoSectorEmpresa,
			boolean invalidar,
			boolean actualizaSirac,
			String NIT,
			String ic_version_convenio
			) throws NafinException;


	//Pymes Internacionales
	public Vector actualizaPyme(
			String claveUsuario,
			String ic_epo,
			String ic_pyme,
			String Razon_Social,
			String Nombre_Comercial,
			String R_F_C,
			String chkDescuento,
			String Num_Sirac,
			String Numero_Exterior,
			String Colonia,
			String Estado,
			String Fecha_de_nacimiento,
			String chkAnticipo,
			String Estado_civil,
			String Pais_de_origen,
			String TAfilia,
			String Sector_economico,
			String Numero_de_empleados,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String Telefono,
			String Fax,
			String Sexo,
			String r_matrimonial,
			String Apellido_casada,
			String No_identificacion,
			String txtTipoPersona,
			String Calle,
			String Codigo_postal,
			String Delegacion_o_municipio,
			String Pais,
			String Grado_escolaridad,
			String No_de_notaria,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String Tel_Rep_Legal,
			String Fax_Rep_Legal,
			String Email_Rep_Legal,
			String RFC_legal,
			String Identificacion,
			String Tipo_categoria,
			String Ejecutivo_de_cuenta,
			String Empleos_a_generar,
			String Activo_total,
			String Capital_contable,
			String Ventas_netas_totales,
			String Ventas_netas_exportacion,
			String Fecha_de_Constitucion,
			String Subsector,
			String Rama,
			String Clase,
			String Tipo_empresa,
			String Domicilio_Correspondencia,
			String Principales_productos,
			String ic_domicilio,
			String No_de_escritura,
			String ic_contacto,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Email_C,
			String Fax_C,
			String Numero_de_cliente,
			String Numero_de_distribuidor,
			String chkFinanciamiento,
			String chkInventario,
			String Num_Troya,
			String strOficinaTramitadoraDescuento,
			String strOficinaTramitadoraAnticipo,
			String strOficinaTramitadoraInventario,
			//String tipoSectorEmpresa,
			boolean invalidar,
			boolean actualizaSirac,
			String NIT
			) throws NafinException;

	public String borraDF(
			String cveDf
	)throws NafinException;

	public String borrarContragarante(
			String claveUsuario,
			String ic_if
	)throws NafinException;


	public List buscarClienteSirac(String numSirac)
			throws NafinException;

	public String borraIf(
			String claveUsuario,
			String ic_if
	)throws NafinException;

	public void actualizaIf(
			String claveUsuario,
			String ic_if,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Ejecutivo_de_cuenta,
			String Domicilio_correspondencia,
			String Numero_de_IF,
			String Tipo_de_IF,
			String Avales,
			String Limite_maximo_endeudamiento,
			String Facultades,
			String Porcentaje_capital_contable,
			String Ente_contable,
			String Oficina_controladora,
			String Viabilidad,
			String Fecha_convenio,
			String Fecha_convenio_anticipo,
			String ic_domicilio,
			String Calle,
			String Numero_Exterior,
			String Numero_interior,
			String Colonia,
			String Estado,
			String Pais,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Telefono,
			String Email,
			String Fax,
			String Tipo_de_IF_piso,
			String enlace_aut,
			String nombre_tabla,
			String autorizauto_opersf,
			String tipo_riesgo,
			String if_siag
	)throws NafinException;

		public void actualizaIf(
			String claveUsuario,
			String ic_if,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Ejecutivo_de_cuenta,
			String Domicilio_correspondencia,
			String Numero_de_IF,
			String Tipo_de_IF,
			String Avales,
			String Limite_maximo_endeudamiento,
			String Facultades,
			String Porcentaje_capital_contable,
			String Ente_contable,
			String Oficina_controladora,
			String Viabilidad,
			String Fecha_convenio,
			String Fecha_convenio_anticipo,
			String ic_domicilio,
			String Calle,
			String Numero_Exterior,
			String Numero_interior,
			String Colonia,
			String Estado,
			String Pais,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Telefono,
			String Email,
			String Fax,
			String Tipo_de_IF_piso,
			String enlace_aut,
			String nombre_tabla,
			String autorizauto_opersf,
			String tipo_riesgo,
			String if_siag,
			String if_sucre,
			String convenio_unico,//FODEA 020 - 2009
			String mandato_documento, //FODEA 041 2009
			String claveDescontante1, //FODEA 012 - 2010 ACF
			String claveDescontante2, //FODEA 012 - 2010 ACF
			String tipoEpoInstruccion, //FODEA 033 - 2010 ACF
			String fideicomiso, //F017-2013
			String Numero_Clie_Sirac,
			String chkFactDistribuido, //QC-2018
			String idOperaMontosMenores		
	)throws NafinException;

	public void actualizaContra(
			String claveUsuario,
			String ic_if,
			String Razon_social,
			String Nombre_comercial,
			String R_F_C,
			String Domicilio_correspondencia,
			String Oficina_controladora,
			String ic_domicilio,
			String Calle,
			String Numero_Exterior,
			String Numero_interior,
			String Colonia,
			String Estado,
			String Pais,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Telefono,
			String Email,
			String Fax,
			String contragarante
	)throws NafinException;

	public String obtenDatosGrales(String lsRFC, String lsNumEPO, String lsDelimitador)
		throws NafinException;


	public List obtenEstados(String clavePais)
			throws NafinException;

	public Vector obtenMunicipios(String lsClaveEstado, String lsClavePais)
		throws NafinException;

	public String obtenTipoCredito(String lsNumEPO)
		throws NafinException;

	public boolean pymeBloqueada(String esClavePyme, String esClaveProducto)
			throws NafinException;

	public void vborraCliente(String sIC_CLIENTE, String sRH)  throws NafinException;

	public Hashtable afiliaPymeCE(String sDatosPymes,
			String TipoPersona, String fechaNacimiento,
			String revisada, String NoIf, String NoUsuario, String strTipoUsuario )// Fodea campos PLD 2010
			throws NafinException;

	public String afiliaPymeInternacional(
			String claveUsuario,
			String iCveEpo, String tp, String NIT, String Razon_Social,
			String Apellido_paterno, String Apellido_materno, String Nombre,
			String Calle, String Colonia,
			String Pais, String Estado, String Delegacion_o_municipio,
			String Codigo_postal, String Telefono, String Email, String Fax,
			String Apellido_paterno_C, String Apellido_materno_C, String Nombre_C,
			String Telefono_C, String Fax_C, String Email_C,
			String Numero_de_cliente, String chkDescuento, String Num_Sirac, String Sin_Num_Prov)
			throws NafinException;

	public void afiliarExempleado(String numSirac)
			throws NafinException;

	public Vector getPymeAfiliadasCE(String sNoPymes) throws NafinException;

	public String afiliaEpo(
			String claveUsuario,
			String Razon_Social,
			String Nombre_comercial,
			String R_F_C,
			String Calle,
			String Numero_exterior,
			String Numero_interior,
			String Colonia,
			String Estado,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Pais,
			String Telefono,
			String Email,
			String Fax,
			String Fecha_del_poder_notarial,
			String Numero_de_escritura,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Fax_C,
			String Email_C,
			String c_proveedores,
			String cliente_primer_piso,
			String no_sirac,
			String msChkFinan,
			String Banco_Retiro,
			String Cuenta_Retiro,
			String Identificacion,
			String No_Identificacion,
			String internacional,
			String SSId_Number,
			String Dab_Number,
			String Numero_escritura,
			String Fecha_Constitucion,
			String noBancoFondeo,
			String ramo_siaff,//FODEA 034 - 2009 ACF
			String unidad_siaff,//FODEA 034 -2009 ACF
			String convenio_unico,  //FODEA 040-2009
			String mandato_documento, //FODEA 041 2009	
			String sector_epo,//FODEA 057-2010 FVR
			String subdireccion,//FODEA 057-2010 FVR
			String lider_promotor,//FODEA 057-2010 FVR
			String sector_eco,//FODEA 2012
			String subsector_eco,//FODEA 2012
			String rama_eco//FODEA 2012
			
		)throws NafinException;

	public void controlaInterfazEContract(String sFechaProceso)
		throws NafinException;

	public void actualizaFechaConvenio(String sFechaConvenio, String sNoProducto,
					String sNoIf, String dispersion) throws NafinException;

	public void actualizaDispersion(String sNoProducto, String sNoEpo, String dispersion, String dispersionNoNeg)
		throws NafinException;

	public String inhabilitarEpo(String ic_epo, String login)
			throws NafinException;

	public void reafiliarPymeEnEpos(String claveUsuario,
			String clavePyme, String claveEpo,
			List clavesEpo, List numerosCliente, List sinNumProv)
				throws NafinException;

	public void actualizaContacto(
			String ic_epo,
			String ic_contacto,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Fax_C,
			String Email_C
		)throws NafinException;
	public void actualizaCorreoTelefono(String contacto,String telefono,String mail);
	public boolean existeNumeroCliente(String numeroProveedorDistribuidor,
			String claveEpo, String tipoPyme)
			throws NafinException;

	//Agregado EGB Foda 031 Obtiene los datos parametrizados de la linea de fondeo y su disponibilidad en sirac
	public Hashtable getDatosLineaFondeo(String sNoProducto, String sNoIf ) throws NafinException;
	//Agregado EGB Foda 031 Actualiza los datos de la linea de fondeo y verifica si es necesario la disponibilidad
	public void actualizaLineaFondeo(String sNoProducto, String sNoIf, String sLineaFondeo,
										String sDisponibilidad, String sActDisponible, String nombreUsuario) throws NafinException;

	//Agregado EGB Foda 031 Metodo que actualiza el disponible de SIRAC de todos los intermediarios parametrizados
	public void actualizaLineasFondeo() throws NafinException;

	public boolean requiereOficinaTramitadora(String strProducto)
			throws NafinException;

	public List getOficinaTramitadora(String strProducto, String strEPO, String strPyme)
			throws NafinException;

	// Metodos de usados para el registro de una pyme en equipamiento. por Hugoro.
/*	Antes de Pregunta y Respuesta Secreta
	public void setAltaDePyme(String sTipoPersona, String sRFC, String sAppPaterno,
								String sAppMaterno,	String sNombre, String sRazonSocialPyme,
								String sVentasNetas, String sNoEmpleados,
								String sNoSector, String sNoSubSector, String sNoRama, String sNoClase,
								String sAppPaternoC, String sAppMaternoC, String sNombreC,
								String sTelefonoC, String sEmailC, String sFaxC, String sCalleNoExtInt,
								String sColonia, String sNoEstado, String sNoPais, String sDelegMuni,
								String sCodigoPostal, String sTelefono, String sEmail, String sFax)
								throws NafinException;
*/

	public void setAltaDePyme(String sTipoPersona, String sRFC, String sAppPaterno,
								String sAppMaterno,	String sNombre, String sRazonSocialPyme,
								String sVentasNetas, String sNoEmpleados,
								String sNoSector, String sNoSubSector, String sNoRama, String sNoClase,
								String sAppPaternoC, String sAppMaternoC, String sNombreC,
								String sTelefonoC, String sEmailC, String sFaxC, String sCalleNoExtInt,
								String sColonia, String sNoEstado, String sNoPais, String sDelegMuni,
								String sCodigoPostal, String sTelefono, String sEmail, String sFax,
								String spreguntaSecreta, String srespuestaSecreta)
								throws NafinException;

	public String getExisteElUsuario(String sRFC, String sTipoUsuario) throws NafinException;

	public ArrayList getUsuario(String sRFC, String sTipoUsuario) throws NafinException;

	public ArrayList setAltaDePyme(String sTipoPersona, String sRFC, String sAppPaterno, String sAppMaterno,
								String sNombre, String sSexo, String sFechaNacimiento, String sEstadoCivil,
								String sAppCasada, String sRegMatrimonial, String sRazonSocialPyme,
								String sVentasNetas, String sNoEmpleados,
								String sNoSector, String sNoSubSector, String sNoRama, String sNoClase,
								String sAppPaternoRL, String sAppMaternoRL, String sNombreRL,
								String sTelefonoRL, String sRFCRL, String sFaxRL,
								String sAppPaternoC, String sAppMaternoC, String sNombreC,
								String sTelefonoC, String sEmailC, String sFaxC, String sCalleNoExtInt,
								String sColonia, String sNoPais, String sNoEstado, String sDelegMuni,
								String sCodigoPostal, String sTelefono, String sEmail, String sFax,
								String sAltaSirac, String sAltaTroya, boolean bExisteLaPyme,
								String sNoPyme, String sNoContacto, String sNoDomicilio
								//String tipoSectorEmpresa
								) throws NafinException;

	public int getTipoPiso(String sNoIF) throws NafinException;

	public ArrayList getDatosPymeAfiliada(String sRFC) throws NafinException;

	public String actualizaPymeCE(String claveUsuario, String ic_pyme,
			String sDatosPymes, String TipoPersona,
			String fechaNacimiento,String ic_domicilio)
			throws NafinException;

	public void actualizaNumeroProveedor(String sEpo, String sPyme, String sNumProv, List olDatosAnt)
			throws NafinException;

	public StringBuffer procesoCE(String intermediario, IFEnlaceCE ifEnl)
			throws NafinException;

	public AfiliadoCE altaClienteCE(AfiliadoCE afiliadoCE)
			throws NafinException;

	public String validaAfiliaMasiva(
			String claveUsuario,
			String cboTipoAfiliacion,
			String iCveEpo,
			String Num_Sirac,
			String Numero_de_cliente,
			String Nombre_comercial,
			String Razon_Social,
			String chkDescuento,
			String R_F_C,
			String Calle,
			String Colonia,
			String Estado,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Pais,
			String Telefono,
			String Email,
			String Fax,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Fax_C,
			String Email_C
	)throws NafinException;

	public String afiliaMasivaValidado(
			String claveUsuario,
			String cboTipoAfiliacion,
			String iCveEpo,
			String tp,
			String Num_Sirac,
			String Numero_de_cliente,
			String Nombre_comercial,
			String Razon_Social,
			String chkDescuento,
			String R_F_C,
			String Calle,
			String Colonia,
			String Estado,
			String Delegacion_o_municipio,
			String Codigo_postal,
			String Pais,
			String Telefono,
			String Email,
			String Fax,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Fax_C,
			String Email_C,
			String chkGeneraCuenta,
			String operacion
	)throws NafinException;

	public StringBuffer reasignaCtaBan(String ruta) throws NafinException;

	public String guardar_facultades(String stric_if, String clavMoneda, String strmonto_if, String strmonto_ifAnt, String strUsuario, String dtfecha, String strfacultad)
        throws NafinException;

	public List imprimir_acuse(String stric_if) throws NafinException;

    public List consulta_general(String stric_if) throws NafinException;

    public String consultaDatosExternos(String icPyme, String icEpo) throws NafinException;

    public void insertaDatosExternosPyme(List lDatos) throws NafinException;

    public void borrarDatosGrupo(String ic_grupo) throws NafinException;

    public void borrarDatosPymeExt(String[] epos, String ic_grupo) throws NafinException;

    public String insertarDatosGrupo(String grupo, String desde, String a, String[]cadenaDestino)
    	 throws NafinException;

    public List consultarDatosGrupo() throws NafinException;

    public List consultarEposGrupo(String ic_grupo) throws NafinException;

    public String getParamEPO(String iNoEPO) throws NafinException;

		public String getPymesAfiliadasWS(String claveEPO, String usuario, 
				String password, String estatus );

    public List getPreafiliacionPyME(String ic_pyme) throws NafinException;

   	public int getExisteArchivo(String ic_if) throws NafinException;

   	public String getExisteArchivoConvenio(String ic_if, String ic_epo)throws NafinException;

   	public List getCuentasBancariasPyME(String iNoCliente)throws NafinException;

   	public List getCuentasBancariasPyME(String iNoCliente, String ic_cuenta_bancaria)throws NafinException;

   	public void setCuentaBancaria(String ic_epo, String ic_if, String ic_cuenta_bancaria ,String  ic_usuario,String ic_nafin_electronico, String  cg_ip)throws NafinException;
    
    public void setCuentaBancaria(String ic_epo, String ic_if, String ic_cuenta_bancaria ,String  ic_usuario,String ic_nafin_electronico, String  cg_ip, String ic_pyme)throws NafinException;

   	public List getDatosPyME(String ic_if, String ic_epo, String ic_pyme, String dfSol_ini, String dfSol_fin)throws NafinException;

   	public void setAutorizaPreafiliacionIF(String ic_epo_aux[],String ic_pyme_aux[], String ic_if, String ic_cuenta_bancaria[],String ic_usuario,String ic_nafin_electronico,String cg_ip)throws NafinException;

   	public List getDatosPyME_cons(String ic_if, String ic_epo, String ic_pyme, String dfSol_ini, String dfSol_fin)throws NafinException;

   	public List getDatosPyME_Nafin(String ic_if, String ic_epo, String ic_pyme, String dfSol_ini, String dfSol_fin, String estatus, String noBancoFondeo)throws NafinException;

   	public List getExisteExpedientePyme(String rfc) throws NafinException;

   	public StringBuffer reafiliaAutIfs(String ruta)  throws NafinException;

		public void setDatosPymeMasiva(String Ruta, int  numeroProceso,int claveEpo)  throws NafinException;

		public List getDatosPymeMasiva(int numeroDeProceso) throws NafinException;

		public List procesarPymeMasiva(String iNoUsuario, String numeroDeProceso) throws NafinException;

		public void validaDatosPymeMasiva(int  numeroProceso, int numeroLinea)  throws NafinException;

		public ArrayList getClientesNoAfiliados(ClienteExt clienteExterno) throws NafinException;

		public ClienteExt consultaDatosNoNafin(String NoSirac) throws NafinException;

		public String insertaDatosNoAfiliado(ClienteExt clienteExterno) throws NafinException;

		public List CountRFCs(String sRFC)  throws NafinException;

		public String registroAutClave(HashMap map)  throws NafinException;

		public HashMap OperaPyme(String ic_pyme) throws NafinException;

		public HashMap OperaPymeDifH(String ic_pyme) throws NafinException;

		public HashMap ValidaPublicacion(String ic_pyme) throws NafinException;

		public HashMap PublicaEpos(String ic_pyme) throws NafinException;

		public int insertEntAutClve(String ic_pyme,String sCorreoBase,String sLogin,String sCorreo, int countPublicaciones, String Nombre, int entrego_usuario) throws NafinException;

		public int countEFCEntAutCve(String sRFC) throws NafinException;

		public String getNombrePyme(String sIc_Pyme) throws NafinException;
//****************************************************************************** FODEA 032 - 2008 PROMOCION A PYMES	- ACF
		public void actualizaCelular(String nafinElectronico, String numero_celular) throws NafinException;

		public void establecePromocionPyme(String nafinElectronico, String cveUsuario, String nombreUsuario, String aceptaEmail, String aceptaCelular) throws NafinException;
//****************************************************************************** FODEA 032 - 2008 PROMOCION A PYMES	- ACF
		public ArrayList getGrupoEpo() throws NafinException;

		public boolean insertarGrupo(String descripcion,String ic_usuario) throws NafinException;

		public boolean buscaGrupo(String claveGrupo) throws NafinException;

		public boolean eliminarGrupo(String claveGrupo,String ic_usuario) throws NafinException;

		public boolean buscaGrupoRepetido(String descripcion) throws NafinException;

		public boolean modificarGrupo(String claveGrupo,String descripcion,String ic_usuario) throws NafinException;

		public List insertarRelacionGrupoEpo(String eposSeleccionadasForm[],String grupo,String bancoFondeo,String ic_usuario)throws NafinException;

		public ArrayList getEposDeGrupo(String grupo) throws NafinException;

		//****METODO AGREGAGO POR FODEA 034-2008 CALL CENTER
		public Vector actualizaPyme(
			String claveUsuario,
			String ic_epo,
			String ic_pyme,
			String Razon_Social,
			String Nombre_Comercial,
			String R_F_C,
			String chkDescuento,
			String Num_Sirac,
			String Numero_Exterior,
			String Colonia,
			String Estado,
			String Fecha_de_nacimiento,
			String chkAnticipo,
			String Estado_civil,
			String Pais_de_origen,
			String TAfilia,
			String Sector_economico,
			String Numero_de_empleados,
			String Apellido_paterno,
			String Apellido_materno,
			String Nombre,
			String Telefono,
			String Fax,
			String Sexo,
			String r_matrimonial,
			String Apellido_casada,
			String No_identificacion,
			String txtTipoPersona,
			String Calle,
			String Codigo_postal,
			String Delegacion_o_municipio,
			String Pais,
			String Grado_escolaridad,
			String No_de_notaria,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String Tel_Rep_Legal,
			String Fax_Rep_Legal,
			String Email_Rep_Legal,
			String RFC_legal,
			String Identificacion,
			String Tipo_categoria,
			String Ejecutivo_de_cuenta,
			String Empleos_a_generar,
			String Activo_total,
			String Capital_contable,
			String Ventas_netas_totales,
			String Ventas_netas_exportacion,
			String Fecha_de_Constitucion,
			String Subsector,
			String Rama,
			String Clase,
			String Tipo_empresa,
			String Domicilio_Correspondencia,
			String Principales_productos,
			String ic_domicilio,
			String No_de_escritura,
			String ic_contacto,
			String Apellido_paterno_C,
			String Apellido_materno_C,
			String Nombre_C,
			String Telefono_C,
			String Email_C,
			String Fax_C,
			String Numero_de_cliente,
			String Numero_de_distribuidor,
			String chkFinanciamiento,
			String chkInventario,
			String Num_Troya,
			String strOficinaTramitadoraDescuento,
			String strOficinaTramitadoraAnticipo,
			String strOficinaTramitadoraInventario,
			//String tipoSectorEmpresa,
			boolean invalidar,
			boolean actualizaSirac,
			String NIT,
			String ic_version_convenio,
			String sin_num_proveedor,
      String convenio_unico,//FODEA 020 - 2009
      String fecha_convenio_unico,//FODEA 020 - 2009
      String cesion_derechos, //Fodea 037-2009
      String curp, //Fodea campos PLD
      String fiel, //Fodea campos PLD
      String ciudad,  //Fodea campos PLD
	  String fianza_electronica //Fodea ###-2011
			) throws NafinException;

			//****METODO AGREGAGO POR FODEA 034-2008 CALL CENTER
			public String actualizaProvSinNumero(List lstDatosTabla, String iNoUsuario) throws NafinException;
			//****************************************************************************** FODEA 048 - 2008 FACTORAJE M�VIL	- ACF
			public void agregaPymeFactorajeMovil(String nafinElectronico, String cveUsuario, String nombreUsuario, String numeroCelular) throws NafinException;
			public void modificaPymeFactorajeMovil(String nafinElectronico, String cveUsuario, String nombreUsuario, String numeroCelular, String modifEmail) throws NafinException;
			//****************************************************************************** FODEA 048 - 2008 FACTORAJE M�VIL	- ACF
			public void setDatosPymeMasivaEco(String Ruta, int numeroProceso, int claveEpo) throws NafinException;
			public void validaDatosPymeMasivaEco(int  numeroProceso, int numeroLinea)  throws NafinException;
			//public List cargaPymeMasivaEco(String iNoUsuario, String numeroDeProceso, String valForma, String accion, String rutaTmp, String errores) throws NafinException;
			public List cargaPymeMasivaEco(String iNoUsuario, String numeroDeProceso, String valForma, String accion, String rutaTmp, String errores, String iTotalPymes,String iTotalPymesSinError,String iTotalPymesConError,String iTotalPymesPrevReg)throws NafinException;
			//public List consultaResumenCargaEcon(String ic_resumen)  throws NafinException;
			public void procesoBachVentaCruzadaEcon(String ruta)  throws NafinException;
			public List determinaIfsReafiliaAut(String ic_pyme, String ic_epo) throws NafinException;


			//------------------------- FODEA000 - Erik Barba ------------------//
			public ArrayList getDoctos(ArrayList alIds, String sTipoPersona) throws NafinException;
			public ArrayList getCausas(String sIdDocto, int proceso) throws NafinException;
			public boolean realizaRechazo(String ic_cuenta,String ic_if,String ic_epo,String notaIF,String causas )throws NafinException;
			public HashMap getDoctosCausas(String ic_cuenta, String ic_if, String ic_epo) throws NafinException;
			public String getNota(String ic_cuenta, String ic_if, String ic_epo) throws NafinException;
			public HashMap getPymedeCuentaBanc(String ic_cuenta, String ic_if, String ic_epo)  throws NafinException;
			//------------------------- FODEA000 /- Erik Barba ------------------//

			//METODOS INCLUIDOS POR FODEA 002-2009
			public void registroAfiliaPendSirac(String ic_pyme, String ic_epo, String troya) throws NafinException;
			public void afiliacionPendSirac (String ic_sirac_pend, String ic_pyme, String ic_epo, String troya) throws NafinException;
			public void actualizaRegPendSirac(String accion, String ic_sirac_pend, String ic_pyme, String error) throws NafinException;
			public void batchAfiliaPendSirac() throws NafinException;
      public Registros getDatosContacto(String rfc, String numeroNE);

      //METODO INCLUIDO POR FODEA 020-2009 CONVENIO UNICO
      public StringBuffer reafiliacionesConvenioUnico(String ruta)  throws NafinException;

     public List obtenerPyme(String ic_nafin_electronico) throws NafinException;//FODEA 020 - 2009

			//FODEA 018-2009
			public void registroBitPymeSinNumProv(String ic_epo, String ic_pyme)throws NafinException;
			public void restablecePymeSinNumProv(List lstDatosTabla, String usuario) throws NafinException;


public String afiliaMandate(String cboTipoAfiliacion,	String cboEPO,
									String Apellido_paterno, String Apellido_materno,
									String Nombre,  String R_F_C,
									String Calle, String Colonia,
									String Codigo_postal, String Telefono,
									String Email, String Fax,
									String Apellido_paterno_C, String Apellido_materno_C,
									String Nombre_C, String Telefono_C,
									String Fax_C, String Email_C,
									String Confirmacion_Email_C,  String Numero_de_cliente,
									String Pais, String Delegacion_o_municipio,
									String Estado, String persona, String Celular, String Razon_Social, String DF_Nacimiento, String reafilia )  throws NafinException;

public String actualizaMandate(String Apellido_paterno, String Apellido_materno,
										 String  Nombre, String R_F_C, String Calle, String Colonia,
										 String Codigo_postal, String Telefono, String Email, String Fax,
										 String Apellido_paterno_C, String Apellido_materno_C, String Nombre_C,
										 String Telefono_C, String Fax_C,String Email_C, String Confirmacion_Email_C,
										 String Numero_de_cliente, String Pais, String Delegacion_o_municipio,
										 String Estado,String cboTipoPersona, String numeroCelular, String  Razon_Social,
										 String NoMandate, String DF_Nacimiento, String Comercial, String reafilia ) throws NafinException;



public String BorrarMandate(String NoMandate ) throws NafinException;

public String  reafiliarMandate(String Mandate, String cadenasproductivas) throws NafinException;

public void getExpedientePymeEFile(String rfc) throws NafinException;

public Vector obtenCiudad(String lsClaveEstado, String lsClavePais) 	throws NafinException;

public ArrayList getCuentasBeneficiarios(String icEpo, String icIf) throws AppException; //FODEA 032-2010 FVR
public String setCuentasBeneficiarios(ArrayList lstDatosCuentasBenef) throws AppException; //FODEA 032-2010 FVR
public String setAutorizaCtasBancBenef(String icCuentaBenef, String icIf, String csAutoriza, String cgUsuario) throws AppException; //FODEA 032-2010 FVR
public ArrayList getBitacoraCtasBancBenef(String icEpo, String icIf) throws AppException; //FODEA 032-2010 FVR
public ArrayList getCtasBenefAutorizaDesautoriza(String icEpo, String icIf) throws AppException; //FODEA 032-2010 FVR
public void setInformacion(String ic_epo, String ic_if, String  ic_usuario) throws AppException; //FODEA 061-2010 DLHC
public List getBiSolicAfilia( String ic_epo,String ic_pyme,	String clave_if, String dfSol_ini,	String dfSol_fin)throws AppException;//FODEA 061-2010 DLHC

public void bitacoraCargaPymeMasiva(String epo, String iNoUsuario, String strNombreUsuario, String numeroDeProceso, String ruta) throws AppException; //FODEA 057-2010 FVR
public void bitacoraCargaPymeMasiva(String epo, String iNoUsuario, String strNombreUsuario, String numeroDeProceso, String ruta, String nombreArchivo) throws AppException; // Fodea 057 - 2010

public List getBitacoraCargaMasiva(String epo, String fecIni, String fecFin) throws AppException; //FODEA 057-2010 FVR

//****METODO AGREGAGO POR FODEA 057-2010 FVR
public Vector actualizaPyme(
	String claveUsuario,
	String ic_epo,
	String ic_pyme,
	String Razon_Social,
	String Nombre_Comercial,
	String R_F_C,
	String chkDescuento,
	String Num_Sirac,
	String Numero_Exterior,
	String Colonia,
	String Estado,
	String Fecha_de_nacimiento,
	String chkAnticipo,
	String Estado_civil,
	String Pais_de_origen,
	String TAfilia,
	String Sector_economico,
	String Numero_de_empleados,
	String Apellido_paterno,
	String Apellido_materno,
	String Nombre,
	String Telefono,
	String Fax,
	String Sexo,
	String r_matrimonial,
	String Apellido_casada,
	String No_identificacion,
	String txtTipoPersona,
	String Calle,
	String Codigo_postal,
	String Delegacion_o_municipio,
	String Pais,
	String Grado_escolaridad,
	String No_de_notaria,
	String Apellido_paterno_L,
	String Apellido_materno_L,
	String Nombre_L,
	String Tel_Rep_Legal,
	String Fax_Rep_Legal,
	String Email_Rep_Legal,
	String RFC_legal,
	String Identificacion,
	String Tipo_categoria,
	String Ejecutivo_de_cuenta,
	String Empleos_a_generar,
	String Activo_total,
	String Capital_contable,
	String Ventas_netas_totales,
	String Ventas_netas_exportacion,
	String Fecha_de_Constitucion,
	String Subsector,
	String Rama,
	String Clase,
	String Tipo_empresa,
	String Domicilio_Correspondencia,
	String Principales_productos,
	String ic_domicilio,
	String No_de_escritura,
	String ic_contacto,
	String Apellido_paterno_C,
	String Apellido_materno_C,
	String Nombre_C,
	String Telefono_C,
	String Email_C,
	String Fax_C,
	String Numero_de_cliente,
	String Numero_de_distribuidor,
	String chkFinanciamiento,
	String chkInventario,
	String Num_Troya,
	String strOficinaTramitadoraDescuento,
	String strOficinaTramitadoraAnticipo,
	String strOficinaTramitadoraInventario,
	//String tipoSectorEmpresa,
	boolean invalidar,
	boolean actualizaSirac,
	String NIT,
	String ic_version_convenio,
	String sin_num_proveedor,
	String convenio_unico,//FODEA 020 - 2009
	String fecha_convenio_unico,//FODEA 020 - 2009
	String cesion_derechos, //Fodea 037-2009
	String curp, //Fodea campos PLD
	String fiel, //Fodea campos PLD
	String ciudad,  //Fodea campos PLD
	String solicitante, //Fodea 057-2010
	String fianza_electronica  //Fodea ###-2011
	) throws NafinException;
	
	public Vector actualizaPyme(
	String claveUsuario,
	String ic_epo,
	String ic_pyme,
	String Razon_Social,
	String Nombre_Comercial,
	String R_F_C,
	String chkDescuento,
	String Num_Sirac,
	String Numero_Exterior,
	String Colonia,
	String Estado,
	String Fecha_de_nacimiento,
	String chkAnticipo,
	String Estado_civil,
	String Pais_de_origen,
	String TAfilia,
	String Sector_economico,
	String Numero_de_empleados,
	String Apellido_paterno,
	String Apellido_materno,
	String Nombre,
	String Telefono,
	String Fax,
	String Sexo,
	String r_matrimonial,
	String Apellido_casada,
	String No_identificacion,
	String txtTipoPersona,
	String Calle,
	String Codigo_postal,
	String Delegacion_o_municipio,
	String Pais,
	String Grado_escolaridad,
	String No_de_notaria,
	String Apellido_paterno_L,
	String Apellido_materno_L,
	String Nombre_L,
	String Tel_Rep_Legal,
	String Fax_Rep_Legal,
	String Email_Rep_Legal,
	String RFC_legal,
	String Identificacion,
	String Tipo_categoria,
	String Ejecutivo_de_cuenta,
	String Empleos_a_generar,
	String Activo_total,
	String Capital_contable,
	String Ventas_netas_totales,
	String Ventas_netas_exportacion,
	String Fecha_de_Constitucion,
	String Subsector,
	String Rama,
	String Clase,
	String Tipo_empresa,
	String Domicilio_Correspondencia,
	String Principales_productos,
	String ic_domicilio,
	String No_de_escritura,
	String ic_contacto,
	String Apellido_paterno_C,
	String Apellido_materno_C,
	String Nombre_C,
	String Telefono_C,
	String Email_C,
	String Fax_C,
	String Numero_de_cliente,
	String Numero_de_distribuidor,
	String chkFinanciamiento,
	String chkInventario,
	String Num_Troya,
	String strOficinaTramitadoraDescuento,
	String strOficinaTramitadoraAnticipo,
	String strOficinaTramitadoraInventario,
	//String tipoSectorEmpresa,
	boolean invalidar,
	boolean actualizaSirac,
	String NIT,
	String ic_version_convenio,
	String sin_num_proveedor,
	String convenio_unico,//FODEA 020 - 2009
	String fecha_convenio_unico,//FODEA 020 - 2009
	String cesion_derechos, //Fodea 037-2009
	String curp, //Fodea campos PLD
	String fiel, //Fodea campos PLD
	String ciudad,  //Fodea campos PLD
	String solicitante, //Fodea 057-2010
	String fianza_electronica,  //Fodea ###-2011
	String csNotificaciones,
	String OperaFideicomiso ,
	String chkEntidadGobierno, //F034-2014
	String chkFactDistribuido, //QC2018
	String chkProvExtranjero,  //QC2019
	String chkOperaDescAutoEPO  //QC2019
	) throws NafinException;
	
	public String getDetalleBitCargaPymeMasiva(String icBitCarga, String rutatmp, String tipoRegistros)throws AppException;//FODEA 057-2010 FVR
	public String afiliaAfianzadora(Afianzadora afianzadora) throws AppException;//FODEA XXX - 2011 Fianza Electr�nica
	public boolean existeNumeroCnsf(String numeroCnsf) throws AppException;//FODEA XXX - 2011 Fianza Electr�nica

	public List getConsAfianzadora(String ic_afianzadora) throws AppException;//FODEA ###-2011 
	public String  setUpdateAfianzadora(List datos) throws AppException;//FODEA ###-2011 
	public String  setDeleteAfianzadora(String  ic_afianzadora)  throws AppException;//FODEA ###-201
 
	public Map buscaAfiliado(String numeroNafinElectronico)	throws AppException; // Fodea Migracion EPO - 2012 (Cuentas CLABE y SWIFT)
	public String getTipoCliente(String clavePyme) throws AppException; //  Fodea Migracion EPO - 2012 (Cuentas CLABE y SWIFT)
	public boolean pymeParametrizadaConEPO(String clavePyme, String claveEPO, String claveProducto) throws AppException;  //  Fodea Migracion EPO - 2012 (Cuentas CLABE y SWIFT)
 
	public Registros getPymeSinNumProv(String ic_pyme, String txtRFC);
	//Migraci�n EPO 2012
	public List obtenerProveedores(String claveEpo, String clavePymeSelecionada, String clavePymeIntroducida,String rfcIntroducido,String nombrePymeIntroducido);
	
	public List obtenerProv(String claveEpo, String clavePymeSelecionada, String clavePymeIntroducida,String rfcIntroducido,String nombrePymeIntroducido);
	
	public HashMap obtenerPymeAfiliada (String claveEpo, String nafinElectronico) ;
	public Registros getNumeros(String cmb_afiliado,String nombre,String rfc,String num_ne,String noBancoFondeo);
	public Registros getProveedores(String ic_producto_nafin, String ic_epo, String num_pyme, String rfc_pyme, String nombre_pyme, String ic_pyme, String pantalla,String ic_if);
	public Registros getProveedoresDist(String ic_epo, String num_pyme, String rfc_pyme, String nombre_pyme, String ic_pyme);
	public Registros getProveedores(String ic_epo, String num_pyme, String rfc_pyme, String nombre_pyme) throws AppException;
	//Migracion IF
	public List consultaSolicAficiacion(String cuentasBancarias, String claveIF, String pantalla);
	public boolean autorizacionIF(String ic_if, String ic_epo_aux[],String ic_pyme_aux[], String chk_cta[] , String strLogin , String  iNoNafinElectronico, String cg_ip);		
	public String  AcusePDFAuto(List datos);		
	public List  datosPymes(String  num_electronico , String ic_epo );
	public String  descripCausa(String  id_docto , String  causa ); 
	public List obtenerProveedoresBusqueda( List parametros ) ;
	public String  getNombrePymeSirac(String  clienteSirac) ;
	//Migracion EPO
	public Registros getDatosEPO(String ic_epo);
	public Registros getDatosUniversidad(String  ic_universidad);
	public String getTotalSolicitudes(String  ic_epo_if, String tipo);
	public Registros getProductoNafin(String ic_epo);
	public boolean  verificaRFC (String esRfc,String tipo) ;
	
	public String afiliaUniversidad(
			String claveUsuario,
			String Grupo,
			String Razon_Social,
			String R_F_C,			
			String Nombre_Campus,
			String Calle,
			String Colonia,
			String Codigo_postal,
			String Pais,
			String Estado,
			String Delegacion_o_municipio,
			String Telefono,
			String Email,
			String Fax,
			String Apellido_paterno_L,
			String Apellido_materno_L,
			String Nombre_L,
			String Identificacion,
			String No_Identificacion,
			String Numero_de_escritura, 
			String Fecha_del_poder_notarial
			)	throws NafinException; 
			
	//********************* Migraci�n Pantalla Relaci�n EPO -IF ------------
		public List  actOtrosParametros( String noEpo )throws NafinException;
		public List  consultRelEPO_IF(String noEpo, String noIf ,  String noBancoFondeo, String cvePerf    )throws NafinException;
		public List  consultaMoneda(String sEpo, String sIf, String cvePerf  )throws NafinException;
		public List   insertaMoneda( String vEPO , String vIF , String[] claveMoneda ,  String[] seleccion , int  numRegistros   )throws NafinException;
		public String    guardaRelEPO_IF( String [] chkEPO,  String [] porcent,  String [] porcent_dl,  String [] benef ,  String [] sIfEpo ,  String [] csBloqueo ,  String [] convUnico , String [] FactoNormal ,  String [] cuentaActiva,  String strNombreUsuario , String [] csReafiAutomatico , String [] factVencido,  
		String [] factDistribuido , String [] referencia) throws NafinException;
		
		//	MIGRACION Nafin 2013 Afiliados-Registro de Afiliados/Intermediario Financiero
		public List obtiene_datos_Descontante()throws NafinException;
		public String obtiene_ic_if(String lsNoNafinElectronico)throws NafinException;
		public Registros getDatosIF(String  ic_if) ;
		public String getAutorizacionAutomaticaOP(String  ic_if);
		public Registros getDescontantesIF(String  ic_if) ;
		public String getSolicitudesConDescontantesIF(String  ic_if, String descontanteIf) ;
		public Registros getProductoIF(String  ic_if) ;
		public String getFechaSIAG(String  if_siag) ;
		public Registros numeroLineaFondeo(String  Numero_de_IF) ;
		public HashMap getConsultaAfiliacionDescuento(String TxtNumElectronico,String sel_epo,String TxtNumProveedor);
		public HashMap getDatosAfiliadoInternacional(String iNoEPO2,String iNoCliente2,String strLogin);
		public HashMap getDatosAfiliado(String iNoEPO2,String iNoCliente2,String strLogin);
		public HashMap  getDatosGrupo(String ic_grupo)  throws NafinException;
		public List consultarEposGrupoModificar(String ic_grupo ,String inOut) throws NafinException ;
		
		public List estadoAsignacionProductos(String cveEpo) throws AppException;
		public List estadoAsignacionIf(String prod1,String prod4,String txtCveEPO) throws AppException;
		public String asignaProductos(String [] objeto, String [] strHiden, String txtNumChk,String strAsignaIFEPO, String txtCveEPO) throws AppException;
		
		public List getBusquedaAvanzada(String ic_epo, String nombre_pyme, String rfc_pyme, String num_pyme)throws NafinException;
		public List getNombrePyme(String ic_epo, String num_pyme)throws NafinException;
		public String crearCustomFileFromList(List rs, String path, String tipo)throws NafinException;



//------------------------------------------------------------------------------	
//--------------PARAMETRIZACION/GRUPOS DE OPERACION-----------------------------
//-----------------------------------------------------------------------------

	public ArrayList getGrupoEpoOpera() throws NafinException;
	public boolean buscaGrupoRepetidoOpera(String descripcion)throws NafinException;
	public boolean insertarGrupoOpera(String descripcion,String ic_usuario) throws NafinException ;
	public boolean modificarGrupoOpera(String claveGrupo,	String descripcion,String ic_usuario)throws NafinException;
	public boolean eliminarGrupoOpera(String claveGrupo,String ic_usuario)throws NafinException;
	public ArrayList getEposDeGrupoOpera(String grupo) throws NafinException;
	public List insertarRelacionGrupoEpoOpera(String eposSeleccionadasForm[],String grupo,String bancoFondeo,String ic_usuario)throws NafinException;
	
	public String getPyme_Epo_Bloqueo(String productoNafin, String ic_pyme, String ic_epo) throws AppException;  //Fodea 015-2014
	
	public void cambiaEstatusBloqueoPymeXEpo(List lstRegAprocesar, String usuario)throws AppException;
	public List obtienePymesXEpoBloqueadas(List lstRegCons)throws AppException;
	
	  
	public HashMap  validaArchivo( String rutaNombreArchivo, String cboEPO, String respAfil, String iNoUsuario  )  throws AppException;  //Fodea 011-2014 Migraci�n Nafin 
	public String getPyme_EntidaGobierno( String ic_pyme) throws AppException;  // F034-2014
	public HashMap resultadoCargaMasivaProveedores(ValidaDatosPymeMasivaThread ValidaDatos);
	public String validaPymeParamConvUnico(String ic_pyme) throws AppException; 
	public void  setPlantillaCorreos ( List parametros  )  throws AppException; 
	public HashMap  getPlantillaCorreos ( String operacion )   throws AppException;  
	
	public String afiliaClienteExterno(List parametros) throws AppException;   //Fodea 01-2015	
	public String borraClienteExterno(String num_electronico, String claveUsuario,  List datosCliente  )throws NafinException;   //Fodea 01-2015
	public String getRfcSirac (String ic_cliente) throws NafinException;//FODEA001-2015
	public Registros getDatosClienteExterno(String  num_electronico);  //Fodea 01-2015
	public String  actualizaClienteExterno(List parametros) throws AppException;  //Fodea 01-2015
	public String  getClienteSirac ( String numClienteSirac ) throws AppException;  //Fodea 01-2015
	
	public String   getBleoqueCliExt( String numElectronico) throws AppException;  //Fodea 01-2015
	public String   validaNumClieSurac( String IN_NUMERO_SIRAC) throws AppException;  //Fodea 01-2015
	public String afiliaPyme(String claveUsuario, List datosAfiliacion) throws AppException;
	public List setRegAProcCargaMasivaProv(String procesoId, String tipoCarga)  throws AppException; // F025-2015
        
        /**
         * METODO PARA EL ENVIO DE NOTIFICACIONES A LOS IF�S SOBRE LA 
         * MODIFICACI�N DE UNA PYME 
         * @param ruta
         * @throws AppException
         */
	public void enviodeEmail(String ruta)   throws AppException; //2019_01 CE
        
        
            
         
}