package com.netro.afiliacion;

import java.util.*;
import javax.naming.*;
import netropology.utilerias.*;
import java.text.*;
import java.sql.*;
import com.netro.exception.ValidacionAtributosException;


/**
 * Esta clase tiene por objetivo, el manipular los par�metros necesario 
 * durante la afiliaci�n y actualizacion de datos de Sirac. 
 *
 * Utilizando esta clase se centraliza el manejo de par�metros en lugar
 * de tener que establecer una y otra vez todos los valores requeridos por
 * MG_K_ALTA_DE_CLIENTES.Mg_P_Alta_de_clientes.
 *
 * Adem�s esta clase permite simplificar el manejo de varios par�metros ya que 
 * algunos son fijos, y otros s�lo dependen de
 * el tipo de Afiliado (Credito Electronico, Proveedor, Distribuidor, etc.) y
 * del tipo de persona (Fisica o Moral).
 * Por ejemplo, el requisito 2 (codigoTipoIdentificacion) se 
 * establece en 5 si es fisica y 7 si es moral
 *
 * Los atributos de esta clase deben ser llenado con los valores de N@E
 * y a trav�s del m�todo getParametrosSirac() se obtienen los
 * par�metros con el formato necesario para su registro en SIRAC.
 *
 * @author Gilberto Aparicio
 */
public class ParametrosAfiliacionSirac 
		implements java.io.Serializable {
	
	public static final String[] nombreParametrosSirac = new String[60];
	
	static {
		//Nombres de cada uno de los par�metros de SIRAC.
		nombreParametrosSirac[0] = "Codigo_cliente_sirac";			//1 Parametro de Salida
		nombreParametrosSirac[1] = "Codigo_tipo_identificacion";	//2
		nombreParametrosSirac[2] = "Numero_identificacion";			//3
		nombreParametrosSirac[3] = "Codigo_tipo_cliente";			//4
		nombreParametrosSirac[4] = "Codigo_categoria";				//5
		nombreParametrosSirac[5] = "Codigo_agencia";				//6
		nombreParametrosSirac[6] = "Codigo_pais";					//7
		nombreParametrosSirac[7] = "Persona";						//8
		nombreParametrosSirac[8] = "Nombre";						//9
		nombreParametrosSirac[9] = "Apellido_paterno";				//10
		nombreParametrosSirac[10] = "Apellido_materno";				//11
		nombreParametrosSirac[11] = "APCasada";						//12
		nombreParametrosSirac[12] = "Sexo";							//13
		nombreParametrosSirac[13] = "Fecha_nacimiento";				//14
		nombreParametrosSirac[14] = "Estado_Civil";					//15
		nombreParametrosSirac[15] = "Razon_social";					//16
		nombreParametrosSirac[16] = "Persona_contactar";			//17
		nombreParametrosSirac[17] = "Representante_legal";			//18
		nombreParametrosSirac[18] = "Codigo_tipo_identificacion";	//19
		nombreParametrosSirac[19] = "Numero_identificacion_r";		//20
		nombreParametrosSirac[20] = "Tipo_de_empresa";				//21
		nombreParametrosSirac[21] = "Nombre_comercial";				//22
		nombreParametrosSirac[22] = "Registro_mercantil";			//23
		nombreParametrosSirac[23] = "Clase_sociedad";				//24
		nombreParametrosSirac[24] = "Punto_de_acta";				//25
		nombreParametrosSirac[25] = "Estatuto";						//26
		nombreParametrosSirac[26] = "Adicionado_por";				//27
		nombreParametrosSirac[27] = "Fecha_adicion";				//28
		nombreParametrosSirac[28] = "Codigo_profesion";				//29
		nombreParametrosSirac[29] = "Nivel_educativo";				//30
		nombreParametrosSirac[30] = "Vinculacion";					//31
		nombreParametrosSirac[31] = "Codigo_ejecutivo";				//32
		nombreParametrosSirac[32] = "Tipo_cliente";					//33
		nombreParametrosSirac[33] = "No_empleados";					//34
		nombreParametrosSirac[34] = "Ventas_netas";					//35
		nombreParametrosSirac[35] = "Sector";						//36
		nombreParametrosSirac[36] = "Subsector";					//37
		nombreParametrosSirac[37] = "Rama";							//38
		nombreParametrosSirac[38] = "Clase";						//39
		nombreParametrosSirac[39] = "Bloqueo";						//40
		nombreParametrosSirac[40] = "Ente_contable";				//41
		nombreParametrosSirac[41] = "RMatrimonial";					//42
		nombreParametrosSirac[42] = "Direccion";					//43
		nombreParametrosSirac[43] = "Pais";							//44
		nombreParametrosSirac[44] = "Departamento";					//45
		nombreParametrosSirac[45] = "Municipio";					//46
		nombreParametrosSirac[46] = "Nomenclatura";					//47
		nombreParametrosSirac[47] = "Es_de_trabajo";				//48
		nombreParametrosSirac[48] = "Colonia";						//49
		nombreParametrosSirac[49] = "CP";							//50
		nombreParametrosSirac[50] = "Correspondencia";				//51
		nombreParametrosSirac[51] = "Email";						//52
		nombreParametrosSirac[52] = "Telefono";						//53
		nombreParametrosSirac[53] = "Tipo Sector Empresa";			//54 
		nombreParametrosSirac[54] = "Codigo Salida SP";				//55 Parametro de salida
    nombreParametrosSirac[55] = "Pais Origen";				//Fodea campos PLD
    nombreParametrosSirac[56] = "curp";				//Fodea campos PLD
    nombreParametrosSirac[57] = "fiel";				//Fodea campos PLD
    nombreParametrosSirac[58] = "ciudad";				//Fodea campos PLD
    
	}


	public ParametrosAfiliacionSirac () {}


	
	public String getTipoAfiliado() {
		return this.tipoAfiliado;
	}
	
	/**
	 * Establece el tipo de Afiliado
	 * @param tipoAfiliado Tipo de Afiliado. 
	 * 		CE Credito Electronico
	 * 		P Proveedor
	 * 		D Distribuidor
	 *
	 */
	public void setTipoAfiliado (String tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}

	public String getTipoPersona() {
		return this.tipoPersona;
	}
	
	/**
	 * Establece el tipo de persona
	 * @param tipoPersona Tipo de Persona. 
	 * 		F F�sica
	 * 		M Moral
	 *
	 */
	public void setTipoPersona (String tipoPersona) {
		this.tipoPersona = tipoPersona;
		
		
	}

	
	/**
	 * Obtiene los nombres correspondientes a los parametros Sirac
	 * @return Arreglo con los nombres de los par�metros
	 */
	public static String getNombreParametroSirac(int numeroParametroSirac) {
		if (numeroParametroSirac >= 0 && 
				numeroParametroSirac < nombreParametrosSirac.length) {
			return ParametrosAfiliacionSirac.
					nombreParametrosSirac[numeroParametroSirac - 1];
		} else {
			return "No existe el parametro " + numeroParametroSirac;
		}
	}

	/**
	 * Este m�todo genera los par�metros para Sirac, a partir de la 
	 * informaci�n suministrada de N@E. 
	 *
	 * Ajusta tama�os, y determina automaticamente el valor de los par�metros 
	 * que depende del tipo de persona (fisica o moral) y del tipo de afiliado
	 * (Credito Electr�nico, Proveedor, Distribuidor)
	 * @throws ValidacionAtributosException Si los atributos necesarios para
	 * 		generar los par�metros Sirac, no est�n establecidos o no son
	 * 		adecuados.
	 *
	 */
	public Object[] getValorParametrosSirac() 
			throws ValidacionAtributosException {
		
		
		// Validaci�n de atributos para realizar el proceso de obtenci�n de param.
		//--------------------------------------------------------------------------
		try {
			if (this.getTipoAfiliado() == null || this.getTipoAfiliado().equals("")) {
				throw new Exception("El tipo de afiliado es requerido");
			}
			if (this.getTipoPersona() == null || this.getTipoPersona().equals("")) {
				throw new Exception("El tipo de persona es requerido");
			}
			
			this.validarAtributos(this.getTipoAfiliado(), this.getTipoPersona());
			
			
			//-------------------------------------------------------------------------

			if (this.getPais() == null || this.getPais().equals("")) {
				this.setPais("24"); //De manera predeterminada establece Mexico como pais
			}
			
			//Valores comunes a todos los afiliados independientemente del tipo de persona
			setParametroSirac(3, this.getRFC());
			setParametroSirac(5, new Integer(99));
			setParametroSirac(6, new Integer(90));
			setParametroSirac(21, "O");
			setParametroSirac(23, null);
			setParametroSirac(24, null);
			setParametroSirac(25, null);
			setParametroSirac(26, null);
			setParametroSirac(27, null); //Es ignorado por afiliaSirac
			setParametroSirac(28, 
					(new SimpleDateFormat("yyyy-MM-dd")).format(new java.util.Date()));
			setParametroSirac(29, null); //Es ignorado por afiliaSirac
			setParametroSirac(30, "O");
			setParametroSirac(32, this.getCodigoEjecutivo());
			setParametroSirac(33, "O");
			setParametroSirac(34, new Integer(this.getNumeroEmpleados()));
			setParametroSirac(35, new Double(this.getVentasNetas()));
			setParametroSirac(36, new Integer(this.getSector()));
			setParametroSirac(37, new Integer(this.getSubsector()));
			setParametroSirac(38, new Integer(this.getRama()));
			setParametroSirac(39, new Integer(this.getClase()));
			setParametroSirac(40, null); //Es ignorado por afiliaSirac
			setParametroSirac(41, null); //Es ignorado por afiliaSirac
			setParametroSirac(43, null); //Es ignorado por afiliaSirac
			setParametroSirac(45, new Integer(
					getClaveDepartamentoSirac(this.getPais(),
							this.getEstado())));
			setParametroSirac(46, new Integer(this.getMunicipio()));
			setParametroSirac(47, truncar(this.getCalle(), 40));
			setParametroSirac(48, "S");
			setParametroSirac(49, truncar(this.getColonia(), 40));
			setParametroSirac(50, truncar(this.getCP(), 5));
			setParametroSirac(51, "S");
			setParametroSirac(52, this.getEmail());
			setParametroSirac(53, this.getTelefono());
			//setParametroSirac(54, this.getTipoSectorEmpresa());
			
			//0 Es sector privado. ver mg_tipo_sector_empresa en sirac.
			setParametroSirac(54, "0");

			
			//Valores que dependen unicamente del tipo de persona
			if (this.getTipoPersona().equals("F")) { //Persona Fisica
				setParametroSirac(2, new Integer(5));
				setParametroSirac(4, new Integer(1));
				setParametroSirac(8, "N");
				setParametroSirac(9, truncar(this.getNombre(), 25));
				setParametroSirac(10, truncar(this.getApellidoPaterno(), 20));
				setParametroSirac(11, truncar(this.getApellidoMaterno(), 20));
				setParametroSirac(13, this.getSexo());
				setParametroSirac(14, java.sql.Date.valueOf(
						this.getFechaNacimiento().substring(6,10) + "-" +
						this.getFechaNacimiento().substring(3,5) + "-" +
						this.getFechaNacimiento().substring(0,2)
						));
				setParametroSirac(16, null);
				setParametroSirac(22, null);
				setParametroSirac(31, "S");
	
	
			} else if (this.getTipoPersona().equals("M")) { //Persona Moral
				setParametroSirac(2, new Integer(7));
				setParametroSirac(4, new Integer(14));
				setParametroSirac(8, "J");
				setParametroSirac(9, null);
				setParametroSirac(10, null);
				setParametroSirac(11, null);
				setParametroSirac(13, null);
				setParametroSirac(14, null);
				setParametroSirac(16, truncar(this.getRazonSocial(),50));
				setParametroSirac(31, "N");
			}
			
			//Valores que dependen del tipo de afiliado y tipo de persona
			
			//----------------- Credito Electronico ------------------
			if (this.getTipoAfiliado().equals("CE")) {
				
				setParametroSirac(7, new Integer(24));
				setParametroSirac(12, null);
				setParametroSirac(17, null);
				setParametroSirac(18, null);
				setParametroSirac(19, new Integer("0"));
				setParametroSirac(20, null);
				setParametroSirac(42, null);
				setParametroSirac(44, new Integer(24));
				
				
				if (this.getTipoPersona().equals("F")) { //Persona Fisica
					setParametroSirac(15, "O");
				} else if (this.getTipoPersona().equals("M")) { //Persona Moral
					setParametroSirac(22, truncar(this.getNombreComercial(),50));
					setParametroSirac(15, null);
				}
				
			}
			
			
			//---------------------- Proveedor -----------------------
			if (this.getTipoAfiliado().equals("P")) {
				
				setParametroSirac(19, new Integer(5));
				setParametroSirac(44, new Integer(this.getPais()));
			
				if (this.getTipoPersona().equals("F")) { //Persona Fisica
					setParametroSirac(7, new Integer(this.getPaisOrigen()));
					if (this.getRegimenMatrimonial() != null &&
							this.getRegimenMatrimonial().equalsIgnoreCase("M")) { // Mancomunados
						setParametroSirac(12, truncar(this.getApellidoCasada(),19));
					} else {
						setParametroSirac(12, null);
					}
					
					String estadoCivil = null;
					if (this.getEstadoCivil().equals("1")) {
						estadoCivil = "S";
					} else if (this.getEstadoCivil().equals("2")) {
						estadoCivil = "C";
					} else {
						estadoCivil = "O";
					}
					
					setParametroSirac(15, estadoCivil);
					setParametroSirac(17, null);
					setParametroSirac(18, null);
					setParametroSirac(20, null);
					setParametroSirac(42, this.getRegimenMatrimonial());
					
				} else if (this.getTipoPersona().equals("M")) { //Persona Moral
					setParametroSirac(7, new Integer(24));
					setParametroSirac(12, null);
					setParametroSirac(15, null);
					setParametroSirac(17, 
							truncar(this.getNombreContacto() + " " +
							this.getApellidoPaternoContacto() + " " +
							this.getApellidoMaternoContacto(), 39));
					setParametroSirac(18,
							truncar(this.getNombreRepLegal() + " " +
							this.getApellidoPaternoRepLegal() + " " +
							this.getApellidoMaternoRepLegal(), 39));
					setParametroSirac(20, this.getRFCRepLegal());
					setParametroSirac(22, truncar(this.getNombreComercial(),50));
					setParametroSirac(42, null);
				}
				
			}
			
			
			
			//--------------------- Distribuidor ---------------------
			
			if (this.getTipoAfiliado().equals("D")) {
				
				setParametroSirac(19, new Integer(5));
				setParametroSirac(44, new Integer(this.getPais()));
				
				
				if (this.getTipoPersona().equals("F")) { //Persona Fisica
					setParametroSirac(7, new Integer(this.getPaisOrigen()));
					if (this.getRegimenMatrimonial() != null &&
							this.getRegimenMatrimonial().equalsIgnoreCase("M")) { // Mancomunados
						setParametroSirac(12, truncar(this.getApellidoCasada(),19));
					} else {
						setParametroSirac(12, null);
					}

					
					String estadoCivil = null;
					if (this.getEstadoCivil().equals("1")) {
						estadoCivil = "S";
					} else if (this.getEstadoCivil().equals("2")) {
						estadoCivil = "C";
					} else {
						estadoCivil = "O";
					}
					
					setParametroSirac(15, estadoCivil);
					setParametroSirac(17, null);
					setParametroSirac(18, null);
					setParametroSirac(20, null);
					setParametroSirac(42, this.getRegimenMatrimonial());
					
				} else if (this.getTipoPersona().equals("M")) { //Persona Moral
					setParametroSirac(7, new Integer(24));
					setParametroSirac(12, null);
					setParametroSirac(15, null);
					setParametroSirac(17, 
							truncar(this.getNombreContacto() + " " +
							this.getApellidoPaternoContacto() + " " +
							this.getApellidoMaternoContacto(), 39));
					setParametroSirac(18,
							truncar(this.getNombreRepLegal() + " " +
							this.getApellidoPaternoRepLegal() + " " +
							this.getApellidoMaternoRepLegal(), 39));
					setParametroSirac(20, this.getRFCRepLegal());
					
					//nombre comercial = Razon Social
					setParametroSirac(22, truncar(this.getRazonSocial(),50));
					setParametroSirac(42, null);
				}
			}
			
				setParametroSirac(55, this.getPaisOrigenPLD());
				setParametroSirac(56, this.getCurpPLD());
         	setParametroSirac(57, this.getFielPLD());
				setParametroSirac(58, this.getCiudadPLD());

			return this.valorParametrosSirac;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ValidacionAtributosException("Error en los atributos. " +
					e.getMessage() + "\n" +
					this.toString());
		}

	}
	
	





	/**
	 * Obtiene RFC del afiliado
	 */
	public String getRFC() {
		return this.rfc;
	}
	
	/**
	 * Establece RFC del afiliado
	 */
	public void setRFC (String rfc) {
		this.rfc = rfc;
	}
	
	/**
	 * Obtiene Clave del pais de origen.
	 */
	public String getPaisOrigen() {
		return this.paisOrigen;
	}
	
	/**
	 * Establece Clave del pais de origen.
	 */
	public void setPaisOrigen (String paisOrigen) {
		this.paisOrigen = paisOrigen;
	}
	
	/**
	 * Obtiene Nombre del afiliado
	 */
	public String getNombre() {
		return this.nombre;
	}
	
	/**
	 * Establece Nombre del afiliado
	 */
	public void setNombre (String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Obtiene Apellido Paterno del afiliado
	 */
	public String getApellidoPaterno() {
		return this.apellidoPaterno;
	}
	
	/**
	 * Establece Apellido Paterno del afiliado
	 */
	public void setApellidoPaterno (String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	
	/**
	 * Obtiene Apellido Materno del afiliado
	 */
	public String getApellidoMaterno() {
		return this.apellidoMaterno;
	}
	
	/**
	 * Establece Apellido Materno del afiliado
	 */
	public void setApellidoMaterno (String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	
	/**
	 * Obtiene Apellido de casada
	 */
	public String getApellidoCasada() {
		return this.apellidoCasada;
	}
	
	/**
	 * Establece Apellido de casada
	 */
	public void setApellidoCasada (String apellidoCasada) {
		this.apellidoCasada = apellidoCasada;
	}
	
	/**
	 * Obtiene Sexo
	 */
	public String getSexo() {
		return this.sexo;
	}
	
	/**
	 * Establece Sexo
	 */
	public void setSexo (String sexo) {
		this.sexo = sexo;
	}
	
	/**
	 * Obtiene Fecha de Nacimiento
	 */
	public String getFechaNacimiento() {
		return this.fechaNacimiento;
	}
	
	/**
	 * Establece Fecha de Nacimiento
	 */
	public void setFechaNacimiento (String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	/**
	 * Obtiene Estado Civil
	 */
	public String getEstadoCivil() {
		return this.estadoCivil;
	}
	
	/**
	 * Establece Estado Civil
	 */
	public void setEstadoCivil (String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	
	/**
	 * Obtiene Razon Social
	 */
	public String getRazonSocial() {
		return this.razonSocial;
	}
	
	/**
	 * Establece Razon Social
	 */
	public void setRazonSocial (String razonSocial) {
		this.razonSocial = razonSocial;
	}
	
	/**
	 * Obtiene Nombre del contacto
	 */
	public String getNombreContacto() {
		return this.nombreContacto;
	}
	
	/**
	 * Establece Nombre del contacto
	 */
	public void setNombreContacto (String nombreContacto) {
		this.nombreContacto = nombreContacto;
	}
	
	/**
	 * Obtiene Apellido Paterno del contacto
	 */
	public String getApellidoPaternoContacto() {
		return this.apellidoPaternoContacto;
	}
	
	/**
	 * Establece Apellido Paterno del contacto
	 */
	public void setApellidoPaternoContacto (String apellidoPaternoContacto) {
		this.apellidoPaternoContacto = apellidoPaternoContacto;
	}
	
	/**
	 * Obtiene Apellido Materno del contacto
	 */
	public String getApellidoMaternoContacto() {
		return this.apellidoMaternoContacto;
	}
	
	/**
	 * Establece Apellido Materno del contacto
	 */
	public void setApellidoMaternoContacto (String apellidoMaternoContacto) {
		this.apellidoMaternoContacto = apellidoMaternoContacto;
	}
	
	/**
	 * Obtiene Nombre del representante legal
	 */
	public String getNombreRepLegal() {
		return this.nombreRepLegal;
	}
	
	/**
	 * Establece Nombre del representante legal
	 */
	public void setNombreRepLegal (String nombreRepLegal) {
		this.nombreRepLegal = nombreRepLegal;
	}
	
	/**
	 * Obtiene Apellido Paterno del representante legal
	 */
	public String getApellidoPaternoRepLegal() {
		return this.apellidoPaternoRepLegal;
	}
	
	/**
	 * Establece Apellido Paterno del representante legal
	 */
	public void setApellidoPaternoRepLegal (String apellidoPaternoRepLegal) {
		this.apellidoPaternoRepLegal = apellidoPaternoRepLegal;
	}
	
	/**
	 * Obtiene Apellido Materno del representante legal
	 */
	public String getApellidoMaternoRepLegal() {
		return this.apellidoMaternoRepLegal;
	}
	
	/**
	 * Establece Apellido Materno del representante legal
	 */
	public void setApellidoMaternoRepLegal (String apellidoMaternoRepLegal) {
		this.apellidoMaternoRepLegal = apellidoMaternoRepLegal;
	}
	
	/**
	 * Obtiene RFC del representante legal
	 */
	public String getRFCRepLegal() {
		return this.rfcRepLegal;
	}
	
	/**
	 * Establece RFC del representante legal
	 */
	public void setRFCRepLegal (String rfcRepLegal) {
		this.rfcRepLegal = rfcRepLegal;
	}
	
	/**
	 * Obtiene Nombre comercial
	 */
	public String getNombreComercial() {
		return this.nombreComercial;
	}
	
	/**
	 * Establece Nombre comercial
	 */
	public void setNombreComercial (String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	
	/**
	 * Obtiene Codigo de ejecutivo
	 */
	public String getCodigoEjecutivo() {
		return this.codigoEjecutivo;
	}
	
	/**
	 * Establece Codigo de ejecutivo
	 */
	public void setCodigoEjecutivo (String codigoEjecutivo) {
		this.codigoEjecutivo = codigoEjecutivo;
	}
	
	/**
	 * Obtiene Numero de Empleados
	 */
	public String getNumeroEmpleados() {
		return this.numeroEmpleados;
	}
	
	/**
	 * Establece Numero de Empleados
	 */
	public void setNumeroEmpleados (String numeroEmpleados) {
		this.numeroEmpleados = numeroEmpleados;
	}
	
	/**
	 * Obtiene Ventas netas
	 */
	public String getVentasNetas() {
		return this.ventasNetas;
	}
	
	/**
	 * Establece Ventas netas
	 */
	public void setVentasNetas (String ventasNetas) {
		this.ventasNetas = ventasNetas;
	}
	
	/**
	 * Obtiene Clave del Sector
	 */
	public String getSector() {
		return this.sector;
	}
	
	/**
	 * Establece Clave del Sector
	 */
	public void setSector (String sector) {
		this.sector = sector;
	}
	
	/**
	 * Obtiene Clave del Subsector
	 */
	public String getSubsector() {
		return this.subsector;
	}
	
	/**
	 * Establece Clave del Subsector
	 */
	public void setSubsector (String subsector) {
		this.subsector = subsector;
	}
	
	/**
	 * Obtiene Clave de la Rama
	 */
	public String getRama() {
		return this.rama;
	}
	
	/**
	 * Establece Clave de la Rama
	 */
	public void setRama (String rama) {
		this.rama = rama;
	}
	
	/**
	 * Obtiene Clave de la Clase
	 */
	public String getClase() {
		return this.clase;
	}
	
	/**
	 * Establece Clave de la Clase
	 */
	public void setClase (String clase) {
		this.clase = clase;
	}
	
	/**
	 * Obtiene Regimen matrimonial
	 */
	public String getRegimenMatrimonial() {
		return this.regimenMatrimonial;
	}
	
	/**
	 * Establece Regimen matrimonial
	 */
	public void setRegimenMatrimonial (String regimenMatrimonial) {
		this.regimenMatrimonial = regimenMatrimonial;
	}
	
	/**
	 * Obtiene Clave del pais
	 */
	public String getPais() {
		return this.pais;
	}
	
	/**
	 * Establece Clave del pais
	 */
	public void setPais (String pais) {
		this.pais = pais;
	}
	
	/**
	 * Obtiene Clave del estado
	 */
	public String getEstado() {
		return this.estado;
	}
	
	/**
	 * Establece Clave del estado
	 */
	public void setEstado (String estado) {
		this.estado = estado;
	}
	
	/**
	 * Obtiene Clave del municipio
	 */
	public String getMunicipio() {
		return this.municipio;
	}
	
	/**
	 * Establece Clave del municipio
	 */
	public void setMunicipio (String municipio) {
		this.municipio = municipio;
	}
	
	/**
	 * Obtiene Calle
	 */
	public String getCalle() {
		return this.calle;
	}
	
	/**
	 * Establece Calle
	 */
	public void setCalle (String calle) {
		this.calle = calle;
	}
	
	/**
	 * Obtiene Colonia
	 */
	public String getColonia() {
		return this.colonia;
	}
	
	/**
	 * Establece Colonia
	 */
	public void setColonia (String colonia) {
		this.colonia = colonia;
	}
	
	/**
	 * Obtiene CP
	 */
	public String getCP() {
		return this.cp;
	}
	
	/**
	 * Establece CP
	 */
	public void setCP (String cp) {
		this.cp = cp;
	}
	
	/**
	 * Obtiene Email
	 */
	public String getEmail() {
		return this.email;
	}
	
	/**
	 * Establece Email
	 */
	public void setEmail (String email) {
		this.email = email;
	}
	
	/**
	 * Obtiene Telefono
	 */
	public String getTelefono() {
		return this.telefono;
	}
	
	/**
	 * Establece Telefono
	 */
	public void setTelefono (String telefono) {
		this.telefono = telefono;
	}


	/**
	 * Obtiene Tipo de Sector
	 */
/*
	public String getTipoSectorEmpresa() {
		return this.tipoSectorEmpresa;
	}
*/	
	/**
	 * Establece Tipo de Sector
	 */
/*
	public void setTipoSectorEmpresa (String tipoSectorEmpresa) {
		this.tipoSectorEmpresa = tipoSectorEmpresa;
	}
*/

	public String toString() {
		
		return 
				"rfc=" + this.rfc + "\n" +
				"paisOrigen=" + this.paisOrigen + "\n" +
				"nombre=" + this.nombre + "\n" +
				"apellidoPaterno=" + this.apellidoPaterno + "\n" +
				"apellidoMaterno=" + this.apellidoMaterno + "\n" +
				"apellidoCasada=" + this.apellidoCasada + "\n" +
				"sexo=" + this.sexo + "\n" +
				"fechaNacimiento=" + this.fechaNacimiento + "\n" +
				"estadoCivil=" + this.estadoCivil + "\n" +
				"razonSocial=" + this.razonSocial + "\n" +
				"nombreContacto=" + this.nombreContacto + "\n" +
				"apellidoPaternoContacto=" + this.apellidoPaternoContacto + "\n" +
				"apellidoMaternoContacto=" + this.apellidoMaternoContacto + "\n" +
				"nombreRepLegal=" + this.nombreRepLegal + "\n" +
				"apellidoPaternoRepLegal=" + this.apellidoPaternoRepLegal + "\n" +
				"apellidoMaternoRepLegal=" + this.apellidoMaternoRepLegal + "\n" +
				"rfcRepLegal=" + this.rfcRepLegal + "\n" +
				"nombreComercial=" + this.nombreComercial + "\n" +
				"codigoEjecutivo=" + this.codigoEjecutivo + "\n" +
				"numeroEmpleados=" + this.numeroEmpleados + "\n" +
				"ventasNetas=" + this.ventasNetas + "\n" +
				"sector=" + this.sector + "\n" +
				"subsector=" + this.subsector + "\n" +
				"rama=" + this.rama + "\n" +
				"clase=" + this.clase + "\n" +
				"regimenMatrimonial=" + this.regimenMatrimonial + "\n" +
				"pais=" + this.pais + "\n" +
				"estado=" + this.estado + "\n" +
				"municipio=" + this.municipio + "\n" +
				"calle=" + this.calle + "\n" +
				"colonia=" + this.colonia + "\n" +
				"cp=" + this.cp + "\n" +
				"email=" + this.email + "\n" +
//				"tipoSectorEmpresa=" + this.tipoSectorEmpresa + "\n" + 
				"telefono=" + this.telefono + "\n"+        
        "pais Origen=" + this.paisOrigenPLD + "\n"+        
        "curp=" + this.curpPLD + "\n"+
        "fiel=" + this.fielPLD + "\n"+
        "ciudad=" + this.ciudadPLD + "\n";
	}

	//--------------------- Metodos y Variables Privadas ---------------------
	/**
	 * Establece los par�metros de Sirac.
	 * El primer parametro empieza en 1
	 *
	 */
	private void setParametroSirac(int numParametro, Object valorParametro) {
		this.valorParametrosSirac[numParametro - 1] = valorParametro;
	}
	
	/**
	 * Trunca una cadena si su longitud es m�s grande que el valor especificado
	 * por tamanoMax. De lo contrario deja la cadena sin cambio alguno
	 * por ejemplo:
	 * truncar("Hola", 5) ---> Hola
	 * truncar("Hola", 4) ---> Hola
	 * truncar("Hola", 3) ---> Hol
	 *
	 *
	 * @param cadena Cadena a truncar
	 * @param tamanoMax Tama�o m�ximo permitido de la cadena
	 *
	 */
	private String truncar(String cadena, int tamanoMax) {
		return (cadena.length() > tamanoMax)?cadena.substring(0,tamanoMax):cadena;
	}
	
	/**
	 * Permite validar que esten establecidos los atributos requeridos.
	 * Para generar los par�metros de sirac.
	 * @param tipoAfiliado Tipo de afiliado
	 * 		CE Credito Electr�nico
	 * 		P Proveedor
	 * 		D Distribuidor
	 * @param tipoPersona Tipo de persona
	 * 		F Fisica M moral
	 */
	private void validarAtributos(String tipoAfiliado, String tipoPersona)
			throws Exception {
		if (getTipoAfiliado().equals("CE") && getTipoPersona().equals("F")) {
			if (this.getRFC() == null || this.getRFC().equals("") ||
				this.getNombre() == null || this.getNombre().equals("") ||
				this.getApellidoPaterno() == null || this.getApellidoPaterno().equals("") ||
				this.getApellidoMaterno() == null || this.getApellidoMaterno().equals("") ||
				this.getSexo() == null || this.getSexo().equals("") ||
				this.getFechaNacimiento() == null || this.getFechaNacimiento().equals("") ||
				this.getCodigoEjecutivo() == null || this.getCodigoEjecutivo().equals("") ||
				this.getNumeroEmpleados() == null || this.getNumeroEmpleados().equals("") ||
				this.getVentasNetas() == null || this.getVentasNetas().equals("") ||
				this.getSector() == null || this.getSector().equals("") ||
				this.getSubsector() == null || this.getSubsector().equals("") ||
				this.getRama() == null || this.getRama().equals("") ||
				this.getClase() == null || this.getClase().equals("") ||
				this.getEstado() == null || this.getEstado().equals("") ||
				this.getMunicipio() == null || this.getMunicipio().equals("") ||
				this.getCalle() == null || this.getCalle().equals("") ||
				this.getColonia() == null || this.getColonia().equals("") ||
				this.getCP() == null || this.getCP().equals("") ||
				this.getEmail() == null || this.getEmail().equals("") ||
				this.getTelefono() == null || this.getTelefono().equals("")
//				this.getTipoSectorEmpresa() == null || this.getTipoSectorEmpresa().equals("")
			) {
				throw new Exception("Un atributo requerido no est� presente.");
			}
		} else if (getTipoAfiliado().equals("CE") && getTipoPersona().equals("M")) {
			if (this.getRFC() == null || this.getRFC().equals("") ||
				this.getRazonSocial() == null || this.getRazonSocial().equals("") ||
				this.getNombreComercial() == null || this.getNombreComercial().equals("") ||
				this.getCodigoEjecutivo() == null || this.getCodigoEjecutivo().equals("") ||
				this.getNumeroEmpleados() == null || this.getNumeroEmpleados().equals("") ||
				this.getVentasNetas() == null || this.getVentasNetas().equals("") ||
				this.getSector() == null || this.getSector().equals("") ||
				this.getSubsector() == null || this.getSubsector().equals("") ||
				this.getRama() == null || this.getRama().equals("") ||
				this.getClase() == null || this.getClase().equals("") ||
				this.getEstado() == null || this.getEstado().equals("") ||
				this.getMunicipio() == null || this.getMunicipio().equals("") ||
				this.getCalle() == null || this.getCalle().equals("") ||
				this.getColonia() == null || this.getColonia().equals("") ||
				this.getCP() == null || this.getCP().equals("") ||
				this.getEmail() == null || this.getEmail().equals("") ||
				this.getTelefono() == null || this.getTelefono().equals("")
//				this.getTipoSectorEmpresa() == null || this.getTipoSectorEmpresa().equals("")
			) {
				throw new Exception("Un atributo requerido no est� presente.");
			}
		} else if (getTipoAfiliado().equals("P") && getTipoPersona().equals("F")) {
			if (this.getRFC() == null || this.getRFC().equals("") ||
				this.getPaisOrigen() == null || this.getPaisOrigen().equals("") ||
				this.getNombre() == null || this.getNombre().equals("") ||
				this.getApellidoPaterno() == null || this.getApellidoPaterno().equals("") ||
				this.getApellidoMaterno() == null || this.getApellidoMaterno().equals("") ||
				this.getSexo() == null || this.getSexo().equals("") ||
				this.getFechaNacimiento() == null || this.getFechaNacimiento().equals("") ||
				this.getEstadoCivil() == null || this.getEstadoCivil().equals("") ||
				this.getCodigoEjecutivo() == null || this.getCodigoEjecutivo().equals("") ||
				this.getNumeroEmpleados() == null || this.getNumeroEmpleados().equals("") ||
				this.getVentasNetas() == null || this.getVentasNetas().equals("") ||
				this.getSector() == null || this.getSector().equals("") ||
				this.getSubsector() == null || this.getSubsector().equals("") ||
				this.getRama() == null || this.getRama().equals("") ||
				this.getClase() == null || this.getClase().equals("") ||
				this.getPais() == null || this.getPais().equals("") ||
				this.getEstado() == null || this.getEstado().equals("") ||
				this.getMunicipio() == null || this.getMunicipio().equals("") ||
				this.getCalle() == null || this.getCalle().equals("") ||
				this.getColonia() == null || this.getColonia().equals("") ||
				this.getCP() == null || this.getCP().equals("") ||
				this.getEmail() == null || this.getEmail().equals("") ||
				this.getTelefono() == null || this.getTelefono().equals("")
//				this.getTipoSectorEmpresa() == null || this.getTipoSectorEmpresa().equals("")
			) {
				throw new Exception("Un atributo requerido no est� presente.");
			}
		} else if (getTipoAfiliado().equals("P") && getTipoPersona().equals("M")) {
			if (this.getRFC() == null || this.getRFC().equals("") ||
				this.getRazonSocial() == null || this.getRazonSocial().equals("") ||
				(this.getNombreContacto() == null && 
						this.getApellidoPaternoContacto() == null &&
						this.getApellidoMaternoContacto() == null) ||
				(this.getNombreRepLegal() == null && 
						this.getApellidoPaternoRepLegal() == null &&
						this.getApellidoMaternoRepLegal() == null) ||
				this.getRFCRepLegal() == null || this.getRFCRepLegal().equals("") ||
				this.getNombreComercial() == null || this.getNombreComercial().equals("") ||
				this.getCodigoEjecutivo() == null || this.getCodigoEjecutivo().equals("") ||
				this.getNumeroEmpleados() == null || this.getNumeroEmpleados().equals("") ||
				this.getVentasNetas() == null || this.getVentasNetas().equals("") ||
				this.getSector() == null || this.getSector().equals("") ||
				this.getSubsector() == null || this.getSubsector().equals("") ||
				this.getRama() == null || this.getRama().equals("") ||
				this.getClase() == null || this.getClase().equals("") ||
				this.getPais() == null || this.getPais().equals("") ||
				this.getEstado() == null || this.getEstado().equals("") ||
				this.getMunicipio() == null || this.getMunicipio().equals("") ||
				this.getCalle() == null || this.getCalle().equals("") ||
				this.getColonia() == null || this.getColonia().equals("") ||
				this.getCP() == null || this.getCP().equals("") ||
				this.getEmail() == null || this.getEmail().equals("") ||
				this.getTelefono() == null || this.getTelefono().equals("")
//				this.getTipoSectorEmpresa() == null || this.getTipoSectorEmpresa().equals("")
			) {
				throw new Exception("Un atributo requerido no est� presente.");
			}
		} else if (getTipoAfiliado().equals("D") && getTipoPersona().equals("F")) {
			if (this.getRFC() == null || this.getRFC().equals("") ||
				this.getPaisOrigen() == null || this.getPaisOrigen().equals("") ||
				this.getNombre() == null || this.getNombre().equals("") ||
				this.getApellidoPaterno() == null || this.getApellidoPaterno().equals("") ||
				this.getApellidoMaterno() == null || this.getApellidoMaterno().equals("") ||
				this.getSexo() == null || this.getSexo().equals("") ||
				this.getFechaNacimiento() == null || this.getFechaNacimiento().equals("") ||
				this.getEstadoCivil() == null || this.getEstadoCivil().equals("") ||
				this.getCodigoEjecutivo() == null || this.getCodigoEjecutivo().equals("") ||
				this.getNumeroEmpleados() == null || this.getNumeroEmpleados().equals("") ||
				this.getVentasNetas() == null || this.getVentasNetas().equals("") ||
				this.getSector() == null || this.getSector().equals("") ||
				this.getSubsector() == null || this.getSubsector().equals("") ||
				this.getRama() == null || this.getRama().equals("") ||
				this.getClase() == null || this.getClase().equals("") ||
				this.getEstado() == null || this.getEstado().equals("") ||
				this.getMunicipio() == null || this.getMunicipio().equals("") ||
				this.getCalle() == null || this.getCalle().equals("") ||
				this.getColonia() == null || this.getColonia().equals("") ||
				this.getCP() == null || this.getCP().equals("") ||
				this.getEmail() == null || this.getEmail().equals("") ||
				this.getTelefono() == null || this.getTelefono().equals("")
//				this.getTipoSectorEmpresa() == null || this.getTipoSectorEmpresa().equals("")
			) {
				throw new Exception("Un atributo requerido no est� presente.");
			}
		} else if (getTipoAfiliado().equals("D") && getTipoPersona().equals("M")) {
			if (this.getRFC() == null || this.getRFC().equals("") ||
				this.getRazonSocial() == null || this.getRazonSocial().equals("") ||
				(this.getNombreContacto() == null && 
						this.getApellidoPaternoContacto() == null &&
						this.getApellidoMaternoContacto() == null) ||
				(this.getNombreRepLegal() == null && 
						this.getApellidoPaternoRepLegal() == null &&
						this.getApellidoMaternoRepLegal() == null) ||
				this.getRFCRepLegal() == null || this.getRFCRepLegal().equals("") ||
				this.getCodigoEjecutivo() == null || this.getCodigoEjecutivo().equals("") ||
				this.getNumeroEmpleados() == null || this.getNumeroEmpleados().equals("") ||
				this.getVentasNetas() == null || this.getVentasNetas().equals("") ||
				this.getSector() == null || this.getSector().equals("") ||
				this.getSubsector() == null || this.getSubsector().equals("") ||
				this.getRama() == null || this.getRama().equals("") ||
				this.getClase() == null || this.getClase().equals("") ||
				this.getEstado() == null || this.getEstado().equals("") ||
				this.getMunicipio() == null || this.getMunicipio().equals("") ||
				this.getCalle() == null || this.getCalle().equals("") ||
				this.getColonia() == null || this.getColonia().equals("") ||
				this.getCP() == null || this.getCP().equals("") ||
				this.getEmail() == null || this.getEmail().equals("") ||
				this.getTelefono() == null || this.getTelefono().equals("")
//				this.getTipoSectorEmpresa() == null || this.getTipoSectorEmpresa().equals("")
			){
				throw new Exception("Un atributo requerido no est� presente.");
			}
		} 
	}
	
	
	/**
	 * Determina el c�digo de departamento a partir del pais y clave de estado
	 *
	 * @param clavePais Clave del pais
	 * @param claveEstado Clave del estado
	 * @return Cadena con la "clave de departamento" (parametro de sirac)
	 *
	 */
	private String getClaveDepartamentoSirac(
			String clavePais, String claveEstado) 
			throws SQLException, NamingException {
		
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			// Se obtiene el ID del Estado que se tiene en Sirac para el
			//parametro recibido de pais-estado
			String strEstado = "SELECT codigo_departamento " +
					" FROM COMCAT_ESTADO " +
					" WHERE ic_pais = ? AND  ic_estado = ? ";
			PreparedStatement psCodDepto = con.queryPrecompilado(strEstado);
			psCodDepto.setInt(1, Integer.parseInt(clavePais));
			psCodDepto.setInt(2, Integer.parseInt(claveEstado));
			ResultSet rsEstado = psCodDepto.executeQuery();
			rsEstado.next();
			String claveDepartamento = rsEstado.getString("Codigo_Departamento");
			rsEstado.close();
			psCodDepto.close();
			
			return claveDepartamento;
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

	private String tipoAfiliado;	
	private String tipoPersona;	//F Fisica M Moral
	

	private String rfc;
	private String paisOrigen;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String apellidoCasada;
	private String sexo;
	private String fechaNacimiento;
	private String estadoCivil;
	private String razonSocial;
	private String nombreContacto;
	private String apellidoPaternoContacto;
	private String apellidoMaternoContacto;
	private String nombreRepLegal;
	private String apellidoPaternoRepLegal;
	private String apellidoMaternoRepLegal;
	private String rfcRepLegal;
	private String nombreComercial;
	private String codigoEjecutivo;
	private String numeroEmpleados;
	private String ventasNetas;
	private String sector;
	private String subsector;
	private String rama;
	private String clase;
	private String regimenMatrimonial;
	private String pais;
	private String estado;
	private String municipio;
	private String calle;
	private String colonia;
	private String cp;
	private String email;
	private String telefono;
//	private String tipoSectorEmpresa;


	private Object[] valorParametrosSirac = new Object[60];
  private String paisOrigenPLD;
  private String fielPLD;
  private String curpPLD;
  private String ciudadPLD;

  public String getPaisOrigenPLD() {
    return paisOrigenPLD;
  }

  public void setPaisOrigenPLD(String paisOrigenPLD) {
    this.paisOrigenPLD = paisOrigenPLD;
  }

  public String getFielPLD() {
    return fielPLD;
  }

  public void setFielPLD(String fielPLD) {
    this.fielPLD = fielPLD;
  }

  public String getCurpPLD() {
    return curpPLD;
  }

  public void setCurpPLD(String curpPLD) {
    this.curpPLD = curpPLD;
  }

  public String getCiudadPLD() {
    return ciudadPLD;
  }

  public void setCiudadPLD(String ciudadPLD) {
    this.ciudadPLD = ciudadPLD;
  }

}