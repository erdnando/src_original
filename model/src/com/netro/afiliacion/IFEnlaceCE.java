package com.netro.afiliacion;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface IFEnlaceCE extends java.io.Serializable {

	public abstract String getTablaAfiliados();

	public abstract String getTablaAcuse();

	public abstract String getTablaResultados();

	public abstract String getAfiliados();

	public abstract String getUpdateAcuse(AcuseEnl acu);

	public abstract String getInsertAcuse(AcuseEnl acu);

	public abstract String getInsertaResultados();

	public abstract PreparedStatement setInsertaResultados(ResultadosEnl res, PreparedStatement ps)
		throws SQLException;

	public abstract String getDeleteResultados(AcuseEnl acu);

	public abstract String getDeleteAfiliadoTMP(AfiliadoCE afiliadoCE);

	public abstract PreparedStatement setDeleteAfiliadoTMP(AfiliadoCE afiliadoCE, PreparedStatement ps)
		throws SQLException;

	//public abstract String getCondicionQuery();

}//IFEnlaceCE
