package com.netro.afiliacion;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import netropology.utilerias.*;
import com.netro.pdf.*;
import com.netro.xls.*;
import org.apache.commons.logging.Log;



public class ConsCargaProvEcon implements Serializable {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(AfiliacionBean.class);
	public ConsCargaProvEcon() {
	}
	
	public List getConsCargaEcon(String numProceso, String cveEpo, String csProcesado, String fecCargaIni, String fecCargaFin) throws AppException{
		log.info("ConsCargaProvEcon::getConsCargaEcon(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		String query = "";
		List lstRegCons = new ArrayList();
		
		try{
			con.conexionDB();
			
			query = "SELECT distinct EAR.IC_RESUMEN, EAP.IC_EPO, CE.VRAZON_SOCIAL, EAR.CG_NOMBRE_ARCHIVO, EAR.IG_REG_VALIDOS,  " +
					"	   EAR.IG_REG_ERROR, EAR.IG_SUSCEPTIBLES, EAR.IG_DUPLICADOS, EAR.IG_AFILIADOS, EAR.IG_REAFILIA_NUEVAS, "+
					"		EAR.IG_REAFI_AUT, EAR.IG_X_AFILIAR, EAR.IG_ECON_CARGADOS, EAR.IG_ECON_X_CARGAR, "+
					"		EAR.CS_PROCESADO_X_ECON, TO_CHAR(EAR.DF_CARGA, 'DD/MM/YYYY HH24:MI:SS') DF_CARGA, EAR.CG_TIPO_CARGA, " +
					"		EAR.CG_CARGA_ECON, EAR.CG_CRUCE_DB, EAR.IC_PRODUCTO, EAR.CS_DIRECTO_PROMO, EAR.CS_VENTA_CRUZADA, EAR.IG_CONV_UNICO, "+
					// Agregado por Fodea 020 Afiliacion Masiva N@E
					"		EAR.IG_REG_TOTALES, EAR.IG_REG_SINERROR_NE, EAR.IG_REG_CONERROR_NE, EAR.IG_REG_PROV_YA_EXISTENTES_NE " +
					"FROM ECON_AFILIA_PYME_MASIVA EAP, ECON_AFILIA_RESUMEN_CARGA EAR, ECON_CAT_EPOS CE " +
					"WHERE EAP.IC_NUMERO_PROCESO = EAR.IC_NUMERO_PROCESO " +
					"AND EAP.IC_EPO = CE.ECON_CAT_EPOS_PK ";
			if(!numProceso.equals(""))
				query += " AND EAR.IC_RESUMEN = ? ";
			if(!cveEpo.equals(""))
				query += " AND EAP.IC_EPO = ? ";
			if(!csProcesado.equals("A"))
				query += " AND EAR.CS_PROCESADO_X_ECON = ? ";
			if(!fecCargaIni.equals(""))
				query += "AND EAR.DF_CARGA >= TRUNC(TO_DATE(?, 'DD/MM/YYYY')) ";
			if(!fecCargaFin.equals(""))
				query += "AND EAR.DF_CARGA < TRUNC(TO_DATE(?, 'DD/MM/YYYY')+1) ";
			query += " ORDER BY EAR.IC_RESUMEN ";
			
			int param = 0;
			ps = con.queryPrecompilado(query);
			
			if(!numProceso.equals(""))
				ps.setLong(++param, Long.parseLong(numProceso));
			if(!cveEpo.equals(""))
				ps.setLong(++param, Long.parseLong(cveEpo));
			if(!csProcesado.equals("A"))
				ps.setString(++param, csProcesado);
			if(!fecCargaIni.equals(""))
				ps.setString(++param, fecCargaIni);
			if(!fecCargaFin.equals(""))
				ps.setString(++param, fecCargaFin);
				
			rs = ps.executeQuery();
			con.terminaTransaccion(true);
			
			/****obtencion de datos de despliegue*****/
			
			String c_cargaId 			= "";
			String c_nombreArchivo	= "";
			String c_carga_econ		= "";
			String c_cruce_db			= "";
			String c_ic_producto		= "";
			String c_directo_prom	= "";
			String c_venta_cruzada	= "";
			String datosDeCarga		= "";
			String datosDeCargaFile	= "";
			
			
			int cont				= 0;
			int c_afil			= 0;
			int c_reafil		= 0;
			int c_reg_proc		= 0;
			int c_reg_no_proc = 0;
			
			/****Fodea 020 Carga Masiva N@E*****/
			int c_reg_total_ne  	 = 0; 
			int c_reg_sinerror_ne = 0; 
			int c_reg_conerror_ne = 0; 
			int c_reg_prov_ya_ne  = 0; 
			
			int c_reg_dupli		= 0;
			int c_reg_susc			= 0;
			int c_reg_afilia		= 0;
			int c_reg_reafilia	= 0;
			int c_reg_reafi_aut	= 0;
			int c_reg_x_afilia	= 0;
			int c_reg_en_econ		= 0;
			int c_reg_x_econ		= 0;
			int c_reg_conv_unico	= 0;
			
			String c_fecCargaArch	= "";
			String c_procesado		= "";
			String c_nomEpo			= "";
			String c_tipoCarga		= "";
			
			while(rs!=null && rs.next()){
				HashMap map = new HashMap();
				cont++;
				c_afil				= 0;
				c_reafil 			= 0;
				c_cargaId			= rs.getString("IC_RESUMEN");
				c_nombreArchivo	= rs.getString("CG_NOMBRE_ARCHIVO");
				c_reg_proc 			= rs.getInt("IG_REG_VALIDOS");
				c_reg_no_proc		= rs.getInt("IG_REG_ERROR");
				
				c_reg_total_ne		= rs.getInt("IG_REG_TOTALES");
				c_reg_sinerror_ne	= rs.getInt("IG_REG_SINERROR_NE");
				c_reg_conerror_ne	= rs.getInt("IG_REG_CONERROR_NE");
				c_reg_prov_ya_ne	= rs.getInt("IG_REG_PROV_YA_EXISTENTES_NE");
				
				c_reg_dupli			= rs.getInt("IG_DUPLICADOS");
				c_reg_susc			= rs.getInt("IG_SUSCEPTIBLES");
				c_reg_afilia		= rs.getInt("IG_AFILIADOS");
				c_reg_reafilia		= rs.getInt("IG_REAFILIA_NUEVAS");
				c_reg_reafi_aut	= rs.getInt("IG_REAFI_AUT");
				c_reg_x_afilia		= rs.getInt("IG_X_AFILIAR");
				c_reg_en_econ		= rs.getInt("IG_ECON_CARGADOS");
				c_reg_x_econ		= rs.getInt("IG_ECON_X_CARGAR");
				c_reg_conv_unico	= rs.getInt("IG_CONV_UNICO"); 
	
				c_fecCargaArch		= rs.getString("DF_CARGA");
				c_procesado			= rs.getString("CS_PROCESADO_X_ECON");
				c_nomEpo				= rs.getString("VRAZON_SOCIAL");
				c_tipoCarga			= rs.getString("CG_TIPO_CARGA");
	
				c_carga_econ		= rs.getString("CG_CARGA_ECON")!=null?rs.getString("CG_CARGA_ECON"):"";
				c_cruce_db			= rs.getString("CG_CRUCE_DB")!=null?rs.getString("CG_CRUCE_DB"):"";
				c_ic_producto		= rs.getString("IC_PRODUCTO")!=null?rs.getString("IC_PRODUCTO"):"";
				c_directo_prom		= rs.getString("CS_DIRECTO_PROMO")!=null?rs.getString("CS_DIRECTO_PROMO"):"";
				c_venta_cruzada	= rs.getString("CS_VENTA_CRUZADA")!=null?rs.getString("CS_VENTA_CRUZADA"):"";
	
				if(!c_carga_econ.equals("")){
					datosDeCarga += "*Carga";
					datosDeCargaFile += "*Carga";
				}
				if(!c_cruce_db.equals("")){
					datosDeCarga += !datosDeCarga.equals("")?" y Cruce":"*Cruce";
					datosDeCargaFile += !datosDeCargaFile.equals("")?" y Cruce":"*Cruce";
				}
				if(c_ic_producto!=null && c_ic_producto.equals("1")){
					datosDeCarga +="<br>*Factoraje";
					datosDeCargaFile +="- Factoraje";
				}
				if(c_ic_producto!=null && c_ic_producto.equals("11")){
					datosDeCarga +="<br>*Reafiliacion IF";
					datosDeCargaFile +="- Reafiliacion IF";
				}
				if(!c_directo_prom.equals("")){
					datosDeCarga +="<br>*Directo a Promotoria";
					datosDeCargaFile +="- Directo a Promotoria";
				}
				if(!c_venta_cruzada.equals("")){
					datosDeCarga +="<br>*Venta Cruzada";
					datosDeCargaFile +="- Venta Cruzada";
				}
				
				String contAfiReafi =
					"SELECT EAP.ECON_AFI_REAFI FROM ECON_AFILIA_PYME_MASIVA EAP, ECON_AFILIA_RESUMEN_CARGA EAR "+
					"WHERE EAP.IC_NUMERO_PROCESO = EAR.IC_NUMERO_PROCESO " +
					"AND EAR.IC_RESUMEN = ? "+
					"AND EAP.ECON_AFI_REAFI IN (?,?) "+
					"ORDER BY EAP.ECON_AFI_REAFI";

				ps2 = con.queryPrecompilado(contAfiReafi);
				ps2.setInt(1,Integer.parseInt(c_cargaId));
				ps2.setString(2,"A");
				ps2.setString(3,"R");
				rs2 = ps2.executeQuery();
				while(rs2!=null && rs2.next()){
					if((rs2.getString("ECON_AFI_REAFI")).equals("A"))
						c_afil++;
					if((rs2.getString("ECON_AFI_REAFI")).equals("R"))
						c_reafil++;
				}
				rs2.close();
				ps2.close();
				
				map.put("NUMPROC",c_cargaId);
				map.put("EPO",c_nomEpo);
				
				map.put("REG_TOTALES", String.valueOf(c_reg_total_ne));
				map.put("REG_SINERROR_NE", String.valueOf(c_reg_sinerror_ne));
				map.put("REG_CONERROR_NE", String.valueOf(c_reg_conerror_ne));
				map.put("REG_PROV_YA_EXISTENTES_NE", String.valueOf(c_reg_prov_ya_ne));
				
				
				map.put("REGTOTAL", String.valueOf(c_reg_proc+c_reg_dupli));
				map.put("REGDUPLI", String.valueOf(c_reg_dupli));
				map.put("REGUNICO", String.valueOf(c_reg_proc));
				map.put("REGYAAFIL", String.valueOf(c_reg_afilia));
				map.put("REGREAFIL", String.valueOf(c_reg_reafilia));
				map.put("REGREAFILAUT", String.valueOf(c_reg_reafi_aut));
				map.put("REGCONVUNICO", String.valueOf(c_reg_conv_unico));
				map.put("REGPORAFIL", String.valueOf(c_reg_x_afilia));
				map.put("REGCARGADOS", String.valueOf(c_reg_en_econ));
				map.put("REGPORCARGAR", String.valueOf(c_reg_x_econ));
				
				map.put("NOMBREARCH", c_nombreArchivo);
				map.put("FECHACARGA", c_fecCargaArch);
				map.put("DATOSCARGA", datosDeCarga);
				map.put("CSPROCESADO", c_procesado);
				
				map.put("TIPOCARGA", c_tipoCarga);
				map.put("CONTAFIL", String.valueOf(c_afil));
				map.put("CONTREAFIL", String.valueOf(c_reafil));
				
				lstRegCons.add(map);
			}
			
			rs.close();
			ps.close();
			
			return lstRegCons;
			
		}catch(Throwable t){
			throw new AppException("Error al consultar cargas de proveedores para econtract",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			log.info("ConsCargaProvEcon::getConsCargaEcon(S)");
		}
	}
	
	private List infoPyme(AccesoDB conn, String i_rfc, String i_epo, int caso){
		PreparedStatement ps = null;
		ResultSet rs = null;
		List datPyme = null;
		String qryInfoPyme = null;
		try{
			qryInfoPyme =
					"SELECT /*+ ORDERED USE_NL(PE,P,D,C,CP,CE) */  " +
					" P.CG_RFC, PE.CG_PYME_EPO_INTERNO CG_NUMERO_PROVEEDOR, "+
					" P.CG_APPAT, P.CG_APMAT, P.CG_NOMBRE,  " +
					" P.CG_RAZON_SOCIAL, D.CG_CALLE, D.CG_COLONIA, " +
					" CE.CD_NOMBRE CG_ESTADO, CP.CD_DESCRIPCION CG_PAIS, D.CG_MUNICIPIO,  " +
					" D.CN_CP CG_CP, D.CG_TELEFONO1, D.CG_FAX, D.CG_EMAIL,  " +
					" C.CG_APPAT CG_APPAT_C, C.CG_APMAT CG_APMAT_C, C.CG_NOMBRE CG_NOMBRE_C, "+
					" C.CG_TEL CG_TELEFONO_C, C.CG_FAX CG_FAX_C, C.CG_EMAIL CG_EMAIL_C,   " +
					" P.IC_TIPO_CLIENTE CG_TIPO_CLIENTE, PE.CS_ACEPTACION CG_SUSCEPTIBLE_DESCONTAR, " +
					" P.IC_PYME, PE.IC_EPO "+
					" FROM COMREL_PYME_EPO PE, COMCAT_PYME P, COM_DOMICILIO D,  " +
					" COM_CONTACTO C, COMCAT_PAIS CP, COMCAT_ESTADO CE " +
					" WHERE P.IC_PYME = PE.IC_PYME " +
					" AND P.IC_PYME = D.IC_PYME " +
					" AND P.IC_PYME = C.IC_PYME " +
					" AND D.IC_PAIS = CP.IC_PAIS  " +
					" AND D.IC_ESTADO = CE.IC_ESTADO " +
					" AND PE.cs_aceptacion = 'H' "+
					" AND P.CG_RFC = ?   ";
					if(caso!=4)
						qryInfoPyme +=" AND PE.IC_EPO != ? ";
					else
						qryInfoPyme +=" AND PE.IC_EPO = ? ";
					qryInfoPyme +=" AND rownum <2 ";
			System.out.println("qryInfoPyme>>>>>>>"+qryInfoPyme);
			ps = conn.queryPrecompilado(qryInfoPyme);
			ps.setString(1, i_rfc);
			ps.setInt(2, Integer.parseInt(i_epo));
			rs = ps.executeQuery();
	
			if(rs!=null && rs.next()){
				datPyme = new ArrayList();
				/*0 */datPyme.add(rs.getString("CG_RFC"));
				/*1 */datPyme.add(rs.getString("CG_NUMERO_PROVEEDOR"));
				/*2 */datPyme.add(rs.getString("CG_APPAT"));
				/*3 */datPyme.add(rs.getString("CG_APMAT"));
				/*4 */datPyme.add(rs.getString("CG_NOMBRE"));
				/*5 */datPyme.add(rs.getString("CG_RAZON_SOCIAL"));
				/*6 */datPyme.add(rs.getString("CG_CALLE"));
				/*7 */datPyme.add(rs.getString("CG_COLONIA"));
				/*8 */datPyme.add(rs.getString("CG_ESTADO"));
				/*9 */datPyme.add(rs.getString("CG_PAIS"));
				/*10*/datPyme.add(rs.getString("CG_MUNICIPIO"));
				/*11*/datPyme.add(rs.getString("CG_CP"));
				/*12*/datPyme.add(rs.getString("CG_TELEFONO1"));
				/*13*/datPyme.add(rs.getString("CG_FAX"));
				/*14*/datPyme.add(rs.getString("CG_EMAIL"));
				/*15*/datPyme.add(rs.getString("CG_APPAT_C"));
				/*16*/datPyme.add(rs.getString("CG_APMAT_C"));
				/*17*/datPyme.add(rs.getString("CG_NOMBRE_C"));
				/*18*/datPyme.add(rs.getString("CG_TELEFONO_C"));
				/*19*/datPyme.add(rs.getString("CG_FAX_C"));
				/*20*/datPyme.add(rs.getString("CG_EMAIL_C"));
				/*21*/datPyme.add(rs.getString("CG_TIPO_CLIENTE"));
				/*22*/datPyme.add(rs.getString("CG_SUSCEPTIBLE_DESCONTAR"));
				/*23*/datPyme.add(rs.getString("IC_PYME"));
				/*24*/datPyme.add(rs.getString("IC_EPO"));
				/*25*/datPyme.add(i_epo);
	
			}
			rs.close();
			if(ps!=null)ps.close();
	
		}catch(Exception e){
			e.printStackTrace();
		}
	
		return datPyme;
	}

	private ComunesXLS generaFilaXls(ComunesXLS xlsDoc, List valRegArchivo, int x, String var_x) throws Exception{

		// se determina Ifs asociadas a la pyme cuando es reafiliacion automatica
		if(x==6){
			List lstIfsAsociados = null;
			AfiliacionBean BeanAfiliacion = new AfiliacionBean();
			try{

				System.out.println("<>>>>>>><<<<<<<<<<<<<<<VERIFICA VALORES DE BANCOS<<<<<<<<<<<<<<>>>>>>>>"+(String)valRegArchivo.get(25));
				lstIfsAsociados = BeanAfiliacion.determinaIfsReafiliaAut((String)valRegArchivo.get(23), (String)valRegArchivo.get(25));
				if(lstIfsAsociados !=null && lstIfsAsociados.size()>0 ){
					for(int z=0;z<lstIfsAsociados.size();z++){
						List lstIfAsoc = (List)lstIfsAsociados.get(z);
						xlsDoc.setCelda(valRegArchivo.get(0)!=null?(String)valRegArchivo.get(0):"", "formas", ComunesXLS.LEFT, 1);		//1
						xlsDoc.setCelda(valRegArchivo.get(5)!=null?(String)valRegArchivo.get(5):"", "formas", ComunesXLS.LEFT, 1);		//2
						xlsDoc.setCelda(lstIfAsoc.get(2)!=null?(String)lstIfAsoc.get(2):"", "formas", ComunesXLS.LEFT, 1);//3
						xlsDoc.setCelda(lstIfAsoc.get(3)!=null?(String)lstIfAsoc.get(3):"", "formas", ComunesXLS.LEFT, 1);//4
						xlsDoc.setCelda(lstIfAsoc.get(0)!=null?(String)lstIfAsoc.get(0):"", "formas", ComunesXLS.LEFT, 1);//5
						xlsDoc.setCelda(lstIfAsoc.get(1)!=null?(String)lstIfAsoc.get(1):"", "formas", ComunesXLS.LEFT, 1);//6
					}//cierre for
				}//ciere if(lstIfsAsociados)


			}catch(Exception e){
				e.printStackTrace();
			}


		}else if(x==9){

			AccesoDB con2 =  new AccesoDB();
			ResultSet rs2 = null;
			PreparedStatement ps2 = null;
			try{
				con2.conexionDB();
				String []  ifsAsoc = ((String)valRegArchivo.get(33)).split(",");
				for(int n=0; n<ifsAsoc.length;n++){
					System.out.println("ifsAsoc[n]>>>>"+ifsAsoc[n]);
					String qryInfoIf =
						"SELECT IC_IF, CG_RAZON_SOCIAL FROM COMCAT_IF "+
						"WHERE CG_RFC = ? ";
					ps2 = con2.queryPrecompilado(qryInfoIf);
					ps2.setString(1, ifsAsoc[n]);
					rs2 = ps2.executeQuery();

					if(rs2!=null && rs2.next()){
						xlsDoc.setCelda(valRegArchivo.get(26)!=null?(String)valRegArchivo.get(26):"", "formas", ComunesXLS.LEFT, 1);		//1
						xlsDoc.setCelda(valRegArchivo.get(0)!=null?(String)valRegArchivo.get(0):"", "formas", ComunesXLS.LEFT, 1);		//1
						xlsDoc.setCelda(valRegArchivo.get(5)!=null?(String)valRegArchivo.get(5):"", "formas", ComunesXLS.LEFT, 1);		//2
						xlsDoc.setCelda(valRegArchivo.get(25)!=null?(String)valRegArchivo.get(25):"", "formas", ComunesXLS.LEFT, 1);		//1
						xlsDoc.setCelda(var_x, "formas", ComunesXLS.LEFT, 1);		//1
						xlsDoc.setCelda(rs2.getString("IC_IF"), "formas", ComunesXLS.LEFT, 1);//3
						xlsDoc.setCelda(rs2.getString("CG_RAZON_SOCIAL"), "formas", ComunesXLS.LEFT, 1);//4
					}//cierre if
					rs2.close();
					ps2.close();
				}//cierre for
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(con2.hayConexionAbierta())
					con2.cierraConexionDB();
			}



		}else{

			if(x==11){
				xlsDoc.setCelda(valRegArchivo.get(26)!=null?(String)valRegArchivo.get(26):"", "formas", ComunesXLS.LEFT, 1);		//2
				xlsDoc.setCelda(valRegArchivo.get(27)!=null?(String)valRegArchivo.get(27):"", "formas", ComunesXLS.LEFT, 1);		//3
				xlsDoc.setCelda(valRegArchivo.get(29)!=null?(String)valRegArchivo.get(29):"", "formas", ComunesXLS.LEFT, 1);		//5
				System.out.println("var_x>>>>>>>>>>>>>>>>>"+var_x);
				xlsDoc.setCelda(var_x.equals("A")?"AFILIACION":"REAFILIACION", "formas", ComunesXLS.LEFT, 1);		//4
				//xlsDoc.setCelda(valRegArchivo.get(34)!=null?(var_x.equals("A")?"AFILIACION":"REAFILIACION"):"", "formas", ComunesXLS.LEFT, 1);		//4
			}
			xlsDoc.setCelda(valRegArchivo.get(0)!=null?(String)valRegArchivo.get(0):"", "formas", ComunesXLS.LEFT, 1);		//1
			if(x==10){
				xlsDoc.setCelda(valRegArchivo.get(26)!=null?(String)valRegArchivo.get(26):"", "formas", ComunesXLS.LEFT, 1);		//2
				xlsDoc.setCelda(valRegArchivo.get(27)!=null?(String)valRegArchivo.get(27):"", "formas", ComunesXLS.LEFT, 1);		//3
				xlsDoc.setCelda(valRegArchivo.get(28)!=null?(String)valRegArchivo.get(28):"", "formas", ComunesXLS.LEFT, 1);		//4
				xlsDoc.setCelda(valRegArchivo.get(29)!=null?(String)valRegArchivo.get(29):"", "formas", ComunesXLS.LEFT, 1);		//5
			}
			xlsDoc.setCelda(valRegArchivo.get(1)!=null?(String)valRegArchivo.get(1):"", "formas", ComunesXLS.LEFT, 1);		//2
			xlsDoc.setCelda(valRegArchivo.get(2)!=null?(String)valRegArchivo.get(2):"", "formas", ComunesXLS.LEFT, 1);		//3
			xlsDoc.setCelda(valRegArchivo.get(3)!=null?(String)valRegArchivo.get(3):"", "formas", ComunesXLS.LEFT, 1);		//4
			xlsDoc.setCelda(valRegArchivo.get(4)!=null?(String)valRegArchivo.get(4):"", "formas", ComunesXLS.LEFT, 1);		//5
			xlsDoc.setCelda(valRegArchivo.get(5)!=null?(String)valRegArchivo.get(5):"", "formas", ComunesXLS.LEFT, 1);		//6
			xlsDoc.setCelda(valRegArchivo.get(6)!=null?(String)valRegArchivo.get(6):"", "formas", ComunesXLS.LEFT, 1);		//7
			xlsDoc.setCelda(valRegArchivo.get(7)!=null?(String)valRegArchivo.get(7):"", "formas", ComunesXLS.LEFT, 1);		//8
			xlsDoc.setCelda(valRegArchivo.get(8)!=null?(String)valRegArchivo.get(8):"", "formas", ComunesXLS.LEFT, 1);		//9
			xlsDoc.setCelda(valRegArchivo.get(9)!=null?(String)valRegArchivo.get(9):"", "formas", ComunesXLS.LEFT, 1);		//10
			xlsDoc.setCelda(valRegArchivo.get(10)!=null?(String)valRegArchivo.get(10):"", "formas", ComunesXLS.LEFT, 1);	//11
			xlsDoc.setCelda(valRegArchivo.get(11)!=null?(String)valRegArchivo.get(11):"", "formas", ComunesXLS.LEFT, 1);	//12
			xlsDoc.setCelda(valRegArchivo.get(12)!=null?(String)valRegArchivo.get(12):"", "formas", ComunesXLS.LEFT, 1);	//13
			xlsDoc.setCelda(valRegArchivo.get(13)!=null?(String)valRegArchivo.get(13):"", "formas", ComunesXLS.LEFT, 1);	//14
			xlsDoc.setCelda(valRegArchivo.get(14)!=null?(String)valRegArchivo.get(14):"", "formas", ComunesXLS.LEFT, 1);	//15
			xlsDoc.setCelda(valRegArchivo.get(15)!=null?(String)valRegArchivo.get(15):"", "formas", ComunesXLS.LEFT, 1);	//16
			xlsDoc.setCelda(valRegArchivo.get(16)!=null?(String)valRegArchivo.get(16):"", "formas", ComunesXLS.LEFT, 1);	//17
			xlsDoc.setCelda(valRegArchivo.get(17)!=null?(String)valRegArchivo.get(17):"", "formas", ComunesXLS.LEFT, 1);	//18
			xlsDoc.setCelda(valRegArchivo.get(18)!=null?(String)valRegArchivo.get(18):"", "formas", ComunesXLS.LEFT, 1);	//19
			xlsDoc.setCelda(valRegArchivo.get(19)!=null?(String)valRegArchivo.get(19):"", "formas", ComunesXLS.LEFT, 1);	//20
			xlsDoc.setCelda(valRegArchivo.get(20)!=null?(String)valRegArchivo.get(20):"", "formas", ComunesXLS.LEFT, 1);	//21
			xlsDoc.setCelda(valRegArchivo.get(21)!=null?(String)valRegArchivo.get(21):"", "formas", ComunesXLS.LEFT, 1);	//22
			xlsDoc.setCelda(valRegArchivo.get(22)!=null?(String)valRegArchivo.get(22):"", "formas", ComunesXLS.LEFT, 1);	//23
			if(valRegArchivo.size()>26){
			xlsDoc.setCelda(valRegArchivo.get(37)!=null?(String)valRegArchivo.get(37):"", "formas", ComunesXLS.LEFT, 1);	//24
                        }
			
			if(x==1){	
			
			 xlsDoc.setCelda(valRegArchivo.get(38)!=null?(String)valRegArchivo.get(38):"", "formas", ComunesXLS.LEFT, 1);	//F025-2015
			 xlsDoc.setCelda(valRegArchivo.get(39)!=null?(String)valRegArchivo.get(39):"", "formas", ComunesXLS.LEFT, 1);	//F025-2015
			 xlsDoc.setCelda(valRegArchivo.get(40)!=null?(String)valRegArchivo.get(40):"", "formas", ComunesXLS.LEFT, 1);	//F025-2015
			 xlsDoc.setCelda(valRegArchivo.get(41)!=null?(String)valRegArchivo.get(41):"", "formas", ComunesXLS.LEFT, 1);	//F025-2015	 			 
			}
			
		}//cierre del else if


		return xlsDoc;
	}
	
	public String generaArchivoPDF(String strDirectorioTemp, String numProc, String c_afil, String c_reafil){
		AccesoDB con				 = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs				 = null;
		String qryDetResumen = null;
		
		try {
		
			con.conexionDB();
			CreaArchivo archivo		= new CreaArchivo();
			String nombreArchivo	   = archivo.nombreArchivo()+".xls";
			String tipoReg				= null;
			List lstArchCompleto	   = new ArrayList();
			List lstArchRegistros	= null;
			List lstArchOrig			= null;
			
			List lstExistReafi		= new ArrayList();
			List lstExistAfili		= new ArrayList();
			ComunesXLS xlsDoc			= null;
			
			String ic_resumen			= numProc;
			
			
			int c_reg_proc			= 0;
			int c_reg_no_proc		= 0;
			int c_reg_dupli		= 0;
			int c_reg_afilia		= 0;
			int c_reg_reafilia	= 0;
			int c_reg_reafi_aut	= 0;
			int c_reg_x_afilia	= 0;
			int c_reg_en_econ		= 0;
			int c_reg_x_econ		= 0;
			int c_reg_conv_unico = 0;
			
			/****Fodea 020 Carga Masiva N@E*****/
			int c_reg_total_ne  	 = 0; 
			int c_reg_sinerror_ne = 0; 
			int c_reg_conerror_ne = 0; 
			int c_reg_prov_ya_ne  = 0; 
			
			int c_afil_econ			= 0;
			int c_reafil_econ		   = 0;
			String c_nomEpo			= "";
			
			
			String qryArchivosProc =
								"SELECT distinct EAR.IC_RESUMEN, EAP.IC_EPO, CE.VRAZON_SOCIAL, EAR.CG_NOMBRE_ARCHIVO, EAR.IG_REG_VALIDOS,  " +
								"	   EAR.IG_REG_ERROR, EAR.IG_SUSCEPTIBLES, EAR.IG_DUPLICADOS, EAR.IG_AFILIADOS, EAR.IG_REAFILIA_NUEVAS, "+
								"		EAR.IG_REAFI_AUT, EAR.IG_X_AFILIAR, EAR.IG_ECON_CARGADOS, EAR.IG_ECON_X_CARGAR, "+
								"		EAR.CS_PROCESADO_X_ECON, TO_CHAR(EAR.DF_CARGA, 'DD/MM/YYYY HH24:MI:SS') DF_CARGA, EAR.CG_TIPO_CARGA,  " +
								"		EAR.IG_CONV_UNICO, " +
								// Agregado por Fodea 020 Afiliacion Masiva N@E
								"		EAR.IG_REG_TOTALES, EAR.IG_REG_SINERROR_NE, EAR.IG_REG_CONERROR_NE, EAR.IG_REG_PROV_YA_EXISTENTES_NE " +
								
								"FROM ECON_AFILIA_PYME_MASIVA EAP, ECON_AFILIA_RESUMEN_CARGA EAR, ECON_CAT_EPOS CE " +
								"WHERE EAP.IC_NUMERO_PROCESO = EAR.IC_NUMERO_PROCESO " +
								"AND EAR.IC_RESUMEN = ? "+
								"AND EAP.IC_EPO = CE.ECON_CAT_EPOS_PK ";
			ps = con.queryPrecompilado(qryArchivosProc);
			ps.setInt(1,Integer.parseInt(ic_resumen));
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
					c_reg_proc 			= rs.getInt("IG_REG_VALIDOS");
					c_reg_no_proc		= rs.getInt("IG_REG_ERROR");
					c_reg_dupli			= rs.getInt("IG_DUPLICADOS");
					c_reg_afilia		= rs.getInt("IG_AFILIADOS");
					c_reg_reafilia	   = rs.getInt("IG_REAFILIA_NUEVAS");
					c_reg_reafi_aut	= rs.getInt("IG_REAFI_AUT");
					c_reg_x_afilia	   = rs.getInt("IG_X_AFILIAR");
					c_reg_en_econ		= rs.getInt("IG_ECON_CARGADOS");
					c_reg_x_econ		= rs.getInt("IG_ECON_X_CARGAR");
					c_nomEpo				= rs.getString("VRAZON_SOCIAL");
					c_reg_conv_unico	= rs.getInt("IG_CONV_UNICO");
					
					c_reg_total_ne  	 = rs.getInt("IG_REG_TOTALES"); 
					c_reg_sinerror_ne  = rs.getInt("IG_REG_SINERROR_NE");
					c_reg_conerror_ne  = rs.getInt("IG_REG_CONERROR_NE"); 
					c_reg_prov_ya_ne   = rs.getInt("IG_REG_PROV_YA_EXISTENTES_NE");
			}
			
			rs.close();
			ps.close();
			con.terminaTransaccion(true);
			qryDetResumen =
				"SELECT EAP.IC_NUMERO_PROCESO, EAP.IC_NUMERO_LINEA, EAP.IC_EPO, EAP.CG_NUMERO_PROVEEDOR,  " +
				"	   EAP.CG_RFC, EAP.CG_APPAT, EAP.CG_APMAT, EAP.CG_NOMBRE,  " +
				"     EAP.CG_RAZON_SOCIAL, EAP.CG_CALLE, EAP.CG_COLONIA, EAP.CG_ESTADO,  " +
				"     EAP.CG_PAIS, EAP.CG_MUNICIPIO, EAP.CG_CP, EAP.CG_TELEFONO,  " +
				"     EAP.CG_FAX, EAP.CG_EMAIL, EAP.CG_APPAT_C, EAP.CG_APMAT_C,  " +
				"     EAP.CG_NOMBRE_C, EAP.CG_TELEFONO_C, EAP.CG_FAX_C, EAP.CG_EMAIL_C, " +
				"     EAP.CG_SUSCEPTIBLE_DESCONTAR, EAP.CG_TIPO_CLIENTE, EAP.CG_GENERA_CUENTA, EAP.CG_MENSAJES_ERROR, " +
				"	   EAP.CG_TIPO_REGISTRO, EAP.CS_NOMBRE_ARCHIVO, EAP.CS_PROCESADO_NE, EAR.DF_CARGA , EAR.CG_TIPO_CARGA, "+
				"		EAP.ECON_SOLICITUD, EAP.ECON_PROMOTORIA, EAP.ECON_PROMOTOR, EAP.ECON_ESTATUS, EAP.ECON_NOTA, "+
				"		EAP.ECON_PYME_NUEVA, EAP.ECON_PYME_RECHAZADA, EAP.ECON_IFS_ASOCIADAS, EAP.ECON_AFI_REAFI, EAR.CS_PROCESADO_X_ECON, "+
				"		EAP.CS_NE_CONV_UNICO,  " +
				"     decode ( c.CS_ERROR_NE ,'N', 'No', 'S', 'Si') as  CS_ERROR_NE  ,  "+
				"     c.CG_MENSAJES_ERROR_NE  ,   "+
				"	   decode ( c.CS_ERROR_EC ,'N', 'No', 'S', 'Si') as  CS_ERROR_EC  , "+
				"	   c.CG_MENSAJES_ERROR_EC     "+		
				
				" FROM ECON_AFILIA_PYME_MASIVA EAP, ECON_AFILIA_RESUMEN_CARGA EAR  " +
				" ,com_hist_carga_pyme_masiva c "+
				
				"	WHERE EAP.IC_NUMERO_PROCESO = EAR.IC_NUMERO_PROCESO " +
				"	AND EAR.IC_RESUMEN = ? " +
				//"	AND (EAP.CG_MENSAJES_ERROR IS NULL OR ( 	EAP.CG_MENSAJES_ERROR IS NOT NULL AND EAP.CG_TIPO_REGISTRO = 'D')) " +
				" and EAP.IC_NUMERO_PROCESO =  c.IC_NUMERO_PROCESO  "+
            " AND EAP.IC_NUMERO_LINEA= C.IC_NUMERO_LINEA  "+
				
				"ORDER BY EAP.CG_TIPO_REGISTRO, EAP.IC_NUMERO_LINEA ";
			
			ps = con.queryPrecompilado(qryDetResumen);
			ps.setInt(1,Integer.parseInt(ic_resumen));
			rs = ps.executeQuery();
			
			while(rs != null && rs.next()){
				lstArchRegistros = new ArrayList();
				lstArchOrig = new ArrayList();
			
				/*0 */lstArchRegistros.add(rs.getString("CG_RFC"));
				/*1 */lstArchRegistros.add(rs.getString("CG_NUMERO_PROVEEDOR"));
				/*2 */lstArchRegistros.add(rs.getString("CG_APPAT"));
				/*3 */lstArchRegistros.add(rs.getString("CG_APMAT"));
				/*4 */lstArchRegistros.add(rs.getString("CG_NOMBRE"));
				/*5 */lstArchRegistros.add(rs.getString("CG_RAZON_SOCIAL"));
				/*6 */lstArchRegistros.add(rs.getString("CG_CALLE"));
				/*7 */lstArchRegistros.add(rs.getString("CG_COLONIA"));
				/*8 */lstArchRegistros.add(rs.getString("CG_ESTADO"));
				/*9 */lstArchRegistros.add(rs.getString("CG_PAIS"));
				/*10*/lstArchRegistros.add(rs.getString("CG_MUNICIPIO"));
				/*11*/lstArchRegistros.add(rs.getString("CG_CP"));
				/*12*/lstArchRegistros.add(rs.getString("CG_TELEFONO"));
				/*13*/lstArchRegistros.add(rs.getString("CG_FAX"));
				/*14*/lstArchRegistros.add(rs.getString("CG_EMAIL"));
				/*15*/lstArchRegistros.add(rs.getString("CG_APPAT_C"));
				/*16*/lstArchRegistros.add(rs.getString("CG_APMAT_C"));
				/*17*/lstArchRegistros.add(rs.getString("CG_NOMBRE_C"));
				/*18*/lstArchRegistros.add(rs.getString("CG_TELEFONO_C"));
				/*19*/lstArchRegistros.add(rs.getString("CG_FAX_C"));
				/*20*/lstArchRegistros.add(rs.getString("CG_EMAIL_C"));
				/*21*/lstArchRegistros.add(rs.getString("CG_TIPO_CLIENTE"));
				/*22*/lstArchRegistros.add(rs.getString("CG_SUSCEPTIBLE_DESCONTAR"));
				/*23*/lstArchRegistros.add(rs.getString("CG_MENSAJES_ERROR"));
				/*24*/lstArchRegistros.add(rs.getString("CG_TIPO_REGISTRO"));
				/*25*/lstArchRegistros.add(rs.getString("IC_EPO"));
			
				/*26*/lstArchRegistros.add(rs.getString("ECON_SOLICITUD"));
				/*27*/lstArchRegistros.add(rs.getString("ECON_PROMOTORIA"));
				/*28*/lstArchRegistros.add(rs.getString("ECON_PROMOTOR"));
				/*29*/lstArchRegistros.add(rs.getString("ECON_ESTATUS"));
				/*30*/lstArchRegistros.add(rs.getString("ECON_NOTA"));
				/*31*/lstArchRegistros.add(rs.getString("ECON_PYME_NUEVA"));
				/*32*/lstArchRegistros.add(rs.getString("ECON_PYME_RECHAZADA"));
				/*33*/lstArchRegistros.add(rs.getString("ECON_IFS_ASOCIADAS"));
				/*34*/lstArchRegistros.add(rs.getString("ECON_AFI_REAFI"));
				/*35*/lstArchRegistros.add(rs.getString("CS_PROCESADO_X_ECON"));
				/*36*/lstArchRegistros.add(rs.getString("CS_NE_CONV_UNICO"));
				/*37*/lstArchRegistros.add(rs.getString("CG_GENERA_CUENTA"));
				
				
				/*38*/lstArchRegistros.add(rs.getString("CS_ERROR_NE")  );     
				/*39*/lstArchRegistros.add(rs.getString("CG_MENSAJES_ERROR_NE") );
				/*40*/lstArchRegistros.add(rs.getString("CS_ERROR_EC") );
				/*41*/lstArchRegistros.add(rs.getString("CG_MENSAJES_ERROR_EC") );
				
			
				lstArchCompleto.add(lstArchRegistros);
			}
			rs.close();
			if(ps!=null)ps.close();
			
			if (lstArchCompleto.size()>0){
			
			if(lstArchCompleto!=null && lstArchCompleto.size()>0){
				for(int x=1; x<=12;x++){
			
					switch(x){
						case 1:
							xlsDoc	= new ComunesXLS(strDirectorioTemp+nombreArchivo, String.valueOf(c_reg_proc+c_reg_dupli)+"-BASE ORIGINAL");
							tipoReg	= "TODO";
							break;
						case 2:
							xlsDoc.creaHoja(String.valueOf(c_reg_dupli)+"-DUPLICADOS");
							tipoReg = "D";
							break;
						case 3:
							xlsDoc.creaHoja(String.valueOf(c_reg_proc)+"-UNICOS");
							tipoReg = "U";
							break;
						case 4:
							xlsDoc.creaHoja(String.valueOf(c_reg_afilia)+"-YA AFILIADAS");
							tipoReg = "A";
							break;
						case 5:
							xlsDoc.creaHoja(String.valueOf(c_reg_reafilia)+"-REAFILIACIONES NUEVAS");
							tipoReg = "R";
							break;
						case 6:
							xlsDoc.creaHoja(String.valueOf(c_reg_reafi_aut)+"-REAFI-AUT x BANCO");
							tipoReg = "RA";
							break;
						case 7://SE AGREGA ESTA CASO POR F034-2011
							xlsDoc.creaHoja(String.valueOf(c_reg_conv_unico)+"-CONTRATO UNICO");
							tipoReg = "S";
							break;
						case 8:
							xlsDoc.creaHoja(String.valueOf(c_reg_x_afilia)+"-NO AFILIADOS");
							tipoReg = "S";
							break;
						case 9:
							xlsDoc.creaHoja("REAFILIACION IF");
							tipoReg = "IF";
							break;
						case 10:
							xlsDoc.creaHoja(String.valueOf(c_reg_en_econ)+"-SOLICITUDES EN ECONTRACT");
							break;
						case 11:
							xlsDoc.creaHoja(String.valueOf(c_reg_x_econ)+"-SOLICITUDES POR CARGAR");
							break;
						case 12:
							xlsDoc.creaHoja("REPORTE");
							break;
					}
			
			
					if(x==6) xlsDoc.setTabla(6);
					else if(x==9) xlsDoc.setTabla(7);
					else if(x==10 || x==11) xlsDoc.setTabla(32);
					else if(x==12) xlsDoc.setTabla(20);
					else if(x==1) xlsDoc.setTabla(28);
					else xlsDoc.setTabla(24);
			
					for (int i=0;i<lstArchCompleto.size();i++){
						List valRegArchivo = (List)lstArchCompleto.get(i);
						if(i==0){
			
							if(x==6 || x==9){
								if(x==9)
									xlsDoc.setCelda("SOLICITUD", "encabezado", ComunesXLS.CENTER, 1);				//1
								xlsDoc.setCelda("RFC PYME", "encabezado", ComunesXLS.CENTER, 1);				//1
								xlsDoc.setCelda("RAZON SOCIAL", "encabezado", ComunesXLS.CENTER, 1);		//2
								xlsDoc.setCelda("CLAVE_EPO", "encabezado", ComunesXLS.CENTER, 1);				//3
								xlsDoc.setCelda("EPO", "encabezado", ComunesXLS.CENTER, 1);							//4
								xlsDoc.setCelda("CLAVE_IF", "encabezado", ComunesXLS.CENTER, 1);				//5
								xlsDoc.setCelda("BANCO", "encabezado", ComunesXLS.CENTER, 1);						//6
							}else if(x==12){
			
									xlsDoc.setCelda("RESUMEN DEL ANALISIS DE LA BASE DE DATOS", "formasb", ComunesXLS.CENTER, 20);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 20);
						
									xlsDoc.setCelda("CARGA N@E", "celda01", ComunesXLS.CENTER, 4);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda("LIMPIEZA", "celda01", ComunesXLS.CENTER, 3);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda("DISPERSION DE REGISTROS", "celda01", ComunesXLS.CENTER, 5);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda("ECONTRACT", "celda01", ComunesXLS.CENTER, 2);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda("POR CARGAR", "celda01", ComunesXLS.CENTER, 2);
									
									xlsDoc.setCelda("REGISTROS TOTALES",   "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("REGISTROS CON ERROR", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("REGISTROS SIN ERROR", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("YA EXISTENTES",       "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda("ORIGINALES", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("DUPLICADOS", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("UNICOS", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda("YA AFILIADOS", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("REAFILIACIONES", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("AUTOMATICAS", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("CONTRATO UNICO", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("POR AFILIAR", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda("YA CARGADOS", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("POR CARGAR", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda("AFILIACIONES", "encabezado", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("REAFILIACIONES", "encabezado", ComunesXLS.CENTER, 1);
			
									xlsDoc.setCelda(String.valueOf(c_reg_total_ne),    "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_sinerror_ne), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_conerror_ne), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_prov_ya_ne),  "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda(String.valueOf(c_reg_proc+c_reg_dupli), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_dupli), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_proc), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda(String.valueOf(c_reg_afilia), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_reafilia), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_reafi_aut), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_conv_unico), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_x_afilia), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda(String.valueOf(c_reg_en_econ), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_x_econ), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda(String.valueOf(c_afil_econ), "formas", ComunesXLS.CENTER, 1);
									xlsDoc.setCelda(String.valueOf(c_reafil_econ), "formas", ComunesXLS.CENTER, 1);
									
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 20);
									
									xlsDoc.setCelda("TOTALES", "celda01", ComunesXLS.LEFT, 1);
									xlsDoc.setCelda(String.valueOf(c_reg_total_ne), "celda01", ComunesXLS.RIGHT, 3);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda(String.valueOf(c_reg_proc+c_reg_dupli), "celda01", ComunesXLS.RIGHT, 3);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda(String.valueOf(c_reg_afilia+c_reg_reafilia+c_reg_reafi_aut+c_reg_conv_unico+c_reg_x_afilia), "celda01", ComunesXLS.RIGHT, 5);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda(String.valueOf(c_reg_en_econ+c_reg_x_econ), "celda01", ComunesXLS.RIGHT, 2);
									xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 1);//*****
									xlsDoc.setCelda(String.valueOf(c_afil_econ+c_reafil_econ), "celda01", ComunesXLS.RIGHT, 2);
			
			
							}else{
								if(x==11){
									xlsDoc.setCelda("SOLICITUD", "encabezado", ComunesXLS.CENTER, 1);			//1
									xlsDoc.setCelda("PROMOTORIA", "encabezado", ComunesXLS.CENTER, 1);		//2
									xlsDoc.setCelda("ESTATUS_ECON", "encabezado", ComunesXLS.CENTER, 1);	//3
									xlsDoc.setCelda("TIPO", "encabezado", ComunesXLS.CENTER, 1);					//4
								}
			
								xlsDoc.setCelda("RFC OK", "encabezado", ComunesXLS.CENTER, 1);					//1
								if(x==10){
									xlsDoc.setCelda("SOLICITUD", "encabezado", ComunesXLS.CENTER, 1);			//2
									xlsDoc.setCelda("PROMOTORIA", "encabezado", ComunesXLS.CENTER, 1);		//4
									xlsDoc.setCelda("PROMOTOR", "encabezado", ComunesXLS.CENTER, 1);			//5
									xlsDoc.setCelda("ESTATUS_ECON", "encabezado", ComunesXLS.CENTER, 1);	//6
								}
			
			
								xlsDoc.setCelda("NO PROV.", "encabezado", ComunesXLS.CENTER, 1);					//2
								xlsDoc.setCelda("A. PATERNO (PF)", "encabezado", ComunesXLS.CENTER, 1);		//3
								xlsDoc.setCelda("A. MATERNO (PF)", "encabezado", ComunesXLS.CENTER, 1);		//4
								xlsDoc.setCelda("NOMBRE (PF)", "encabezado", ComunesXLS.CENTER, 1);				//5
								xlsDoc.setCelda("RAZON SOCIAL (PM)", "encabezado", ComunesXLS.CENTER, 1);	//6
								xlsDoc.setCelda("CALLE", "encabezado", ComunesXLS.CENTER, 1);							//7
								xlsDoc.setCelda("CIUDAD", "encabezado", ComunesXLS.CENTER, 1);						//8
								xlsDoc.setCelda("COLONIA", "encabezado", ComunesXLS.CENTER, 1);						//9
								xlsDoc.setCelda("PAIS", "encabezado", ComunesXLS.CENTER, 1);							//10
								xlsDoc.setCelda("CIUDAD", "encabezado", ComunesXLS.CENTER, 1);						//11
								xlsDoc.setCelda("C.P.", "encabezado", ComunesXLS.CENTER, 1);							//12
								xlsDoc.setCelda("T 1", "encabezado", ComunesXLS.CENTER, 1);								//13
								xlsDoc.setCelda("FAX1", "encabezado", ComunesXLS.CENTER, 1);							//14
								xlsDoc.setCelda("EMAIL 1", "encabezado", ComunesXLS.CENTER, 1);						//15
								xlsDoc.setCelda("A. PATERNO CONTACTO", "encabezado", ComunesXLS.CENTER, 1);//16
								xlsDoc.setCelda("A. MATERNO CONTACTO", "encabezado", ComunesXLS.CENTER, 1);//17
								xlsDoc.setCelda("NOMBRE CONTACTO", "encabezado", ComunesXLS.CENTER, 1);		//18
								xlsDoc.setCelda("TEL CONTACTO (2)", "encabezado", ComunesXLS.CENTER, 1);	//19
								xlsDoc.setCelda("FAX CONTACTO (2)", "encabezado", ComunesXLS.CENTER, 1);	//20
								xlsDoc.setCelda("EMAIL CONTACTO (2)", "encabezado", ComunesXLS.CENTER, 1);//21
								xlsDoc.setCelda("TP", "encabezado", ComunesXLS.CENTER, 1);								//22
								xlsDoc.setCelda("SUS", "encabezado", ComunesXLS.CENTER, 1);								//23
								xlsDoc.setCelda("GENERA CLAVE CONS", "encabezado", ComunesXLS.CENTER, 1);								//24
							
							  if(x==1){
								xlsDoc.setCelda("Con error N@E ", "encabezado", ComunesXLS.CENTER, 1);	//24 F025-2015
								xlsDoc.setCelda("Detalle error N@E  ", "encabezado", ComunesXLS.CENTER, 1);	//25 F025-2015
								xlsDoc.setCelda("Con error EContract   ", "encabezado", ComunesXLS.CENTER, 1);	//26 F025-2015
								xlsDoc.setCelda("Detalle error EContract ", "encabezado", ComunesXLS.CENTER, 1);	//27 F025-2015 
								 
							  }
							
							}//cierre else if
			
						}//cierre if
			
			
						if(x==1 && tipoReg.equals("TODO")){//GENERAL
							xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, "");
						}
			
						if(x==2 &&	valRegArchivo.get(24)!=null && ((String)valRegArchivo.get(24)).equals(tipoReg)){//DUPLICADOS
							xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, "");
						}
			
						if(x==3 &&	valRegArchivo.get(23)==null && "U".equals(tipoReg)){//UNICOS
								xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, "");
						}
			
						if(x==4 &&	valRegArchivo.get(24)!=null && ((String)valRegArchivo.get(24)).equals(tipoReg)){//YA AFILIADOS
							lstArchOrig = infoPyme(con, (String)valRegArchivo.get(0), (String)valRegArchivo.get(25), x);
							xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, "");
						}
			
						if(x==5 &&	valRegArchivo.get(24)!=null && ((String)valRegArchivo.get(24)).equals(tipoReg)){//REAFILIACIONES NUEVAS
							lstExistReafi.add((String)valRegArchivo.get(0));
							lstArchOrig = infoPyme(con, (String)valRegArchivo.get(0), (String)valRegArchivo.get(25),x);
							xlsDoc = generaFilaXls(xlsDoc, lstArchOrig, x, "");
			
						}
			
						if(x==6 &&	valRegArchivo.get(24)!=null && ((String)valRegArchivo.get(24)).equals(tipoReg)){//REAFI_AUTO_X_BANCO
			
							System.out.println("valor epo enviada a infopyme>>>>"+(String)valRegArchivo.get(25));
							lstArchOrig = infoPyme(con, (String)valRegArchivo.get(0), (String)valRegArchivo.get(25),x);
							xlsDoc = generaFilaXls(xlsDoc, lstArchOrig, x, "");
						}
			
						//SE AGREGA POR FODEA 034-2011
						if(x==7 && (valRegArchivo.get(23)==null && ((String)valRegArchivo.get(36)).equals("S"))){//CONTRATO UNICO
							//lstExistAfili.add((String)valRegArchivo.get(0));
							xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, "");
						}
						
						if(x==8 && (valRegArchivo.get(23)==null && (valRegArchivo.get(24)==null || ((String)valRegArchivo.get(24)).equals(tipoReg)))){//NO AFILIADOS
							lstExistAfili.add((String)valRegArchivo.get(0));
							xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, "");
						}
			
						if(x==9 && valRegArchivo.get(23)==null && valRegArchivo.get(33)!=null &&  ((String)valRegArchivo.get(24)).equals("A") ){//|| ((String)valRegArchivo.get(24)).equals("R"))){//REAFILIACION IF
										xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, c_nomEpo);
						}
			
						if(x==10 &&  ((String)valRegArchivo.get(35)).equals("S") && valRegArchivo.get(23)==null && !((String)valRegArchivo.get(36)).equals("S") &&
							( valRegArchivo.get(24)==null || ((String)valRegArchivo.get(24)).equals("S") || ((String)valRegArchivo.get(24)).equals("R")) &&
							valRegArchivo.get(34)==null && valRegArchivo.get(26)!=null  && valRegArchivo.get(27)!=null && valRegArchivo.get(28)!=null  && valRegArchivo.get(29)!=null ){//SOLICITUDES EN ECONTRACT
										xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, "");
						}
			
						if(x==11 &&  ((String)valRegArchivo.get(35)).equals("S") && valRegArchivo.get(23)==null && !((String)valRegArchivo.get(36)).equals("S") &&
								(  valRegArchivo.get(24)==null || ((String)valRegArchivo.get(24)).equals("S") || ((String)valRegArchivo.get(24)).equals("R")) &&
								(valRegArchivo.get(34)!=null || (valRegArchivo.get(34)==null && valRegArchivo.get(26)==null  && valRegArchivo.get(27)==null && valRegArchivo.get(28)==null  && valRegArchivo.get(29)==null )   )){//SOLICITUDES POR CARGAR EN ECONTRACT
			
									if(lstExistAfili.contains((String)valRegArchivo.get(0))){
										c_afil_econ++;
										xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, "A");
									}
									if(lstExistReafi.contains((String)valRegArchivo.get(0))){
										c_reafil_econ++;
										xlsDoc = generaFilaXls(xlsDoc, valRegArchivo, x, "R");
									}
			
						}

					}//cierre for de valRegArchivo
			
					xlsDoc.cierraTabla();
				}
				xlsDoc.cierraXLS();
				}
			}
			
			return nombreArchivo;
		}catch(Throwable t){
			throw new AppException("Error al generar archivo de excel en la consulta de carga masiva de proveedores",t);
		}finally{
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
}