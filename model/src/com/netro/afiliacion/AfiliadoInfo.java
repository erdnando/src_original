package com.netro.afiliacion;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/**
 * Clase para obtener informaci�n de un afiliado
 * @author Gilberto Aparicio
 *
 */

public class AfiliadoInfo  {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(AfiliadoInfo.class);

	public AfiliadoInfo() {
	}
	
	/**
	 * Obtiene la raz�n social o nombre del afiliado.
	 * @param claveAfiliado Clave del afiliado (comcat_epo.ic_epo, comcat_pyme.ic_pyme, comcat_epo.ic_epo, etc...)
	 * @param tipoAfiliado Clave del tipo de afiliado (comcat_tipo_afiliado.ic_tipo_afiliado) � con notaci�n de letra E, P, I, A, M, U,etc.
	 * @return Cadena con el nombre
	 */
	public static String getNombreAfiliado(int claveAfiliado, String tipoAfiliado) {
		
		AccesoDB con = new AccesoDB();
		String nombreAfiliado = "";
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (tipoAfiliado == null || tipoAfiliado.equals("")) {
				throw new Exception("Los parametros no pueden ser nulos ni vac�os");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" tipoAfiliado=" + tipoAfiliado);
			throw new AppException("Error en lo parametros recibidos");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL = null;
			if(tipoAfiliado.equals("E") || tipoAfiliado.equals("1")) {	//---------------------------------------EPO
				strSQL = " SELECT cg_razon_social AS nombre " +
				" FROM comcat_epo " +
				" WHERE ic_epo = ? ";
			} else if(tipoAfiliado.equals("I") || tipoAfiliado.equals("2")) {	//---------------------------------------IF
				strSQL = " SELECT cg_razon_social AS nombre " +
				" FROM comcat_if " +
				" WHERE ic_if = ? ";
			} else if(tipoAfiliado.equals("P") || tipoAfiliado.equals("3")) {	//---------------------------------------PYME
				String qryTipoPersona = 
						" SELECT cs_tipo_persona " +
						" FROM comcat_pyme " +
						" WHERE ic_pyme = ? ";
				String strTipoPersona = "";
				List params = new ArrayList();
				params.add(new Integer(claveAfiliado));
				Registros regTipoPersona = con.consultarDB(qryTipoPersona, params);
					
				if(regTipoPersona.next()) {
					strTipoPersona = regTipoPersona.getString("CS_TIPO_PERSONA");
				}
			
				if(strTipoPersona.equals("M")) {
					strSQL = 
							" SELECT cg_razon_social AS nombre" +
							" FROM comcat_pyme " + 
							" WHERE ic_pyme =  ? ";
				} else {
					strSQL = 
							" SELECT cg_nombre || ' ' || cg_appat || ' ' || cg_apmat AS nombre "+
							" FROM comcat_pyme " +
							" WHERE ic_pyme = ? " ;
				}
			/*} else if(iClaveTipoAfiliado == 4) {	//---------------------------------------NAFIN
			
			} else if(iClaveTipoAfiliado == 5) {	//---------------------------------------Distribuidor(ofin)
			
			} else if(iClaveTipoAfiliado == 6) {	//---------------------------------------Contragarante
			
			} else if(iClaveTipoAfiliado == 7) {	//---------------------------------------Ex empleado
			
			} else if(iClaveTipoAfiliado == 8) {	//---------------------------------------Bancomext
			*/
			} else if(tipoAfiliado.equals("9")) {	//---------------------------------------Exportador
				String qryTipoPersona = 
						" SELECT cs_tipo_persona " +
						" FROM bmx_prereg_pyme " +
						" WHERE ic_prereg_pyme = ? ";
		
				String strTipoPersona = "";
				List params = new ArrayList();
				params.add(new Integer(claveAfiliado));
				Registros regTipoPersona = con.consultarDB(qryTipoPersona, params);
		
				if(regTipoPersona.next()) {
					strTipoPersona = regTipoPersona.getString("CS_TIPO_PERSONA");
				}
		
				if(strTipoPersona.equals("M")) {
					strSQL = 
							" SELECT cg_razon_social as NOMBRE, '20secure/carga_variables.jsp' " +
							" FROM bmx_prereg_pyme " +
							" WHERE ic_prereg_pyme =  ? ";
				} else {
					strSQL = 
							" SELECT cg_nombre || ' ' || cg_appat || ' ' || cg_apmat as NOMBRE, '20secure/carga_variables.jsp' "+
							" FROM bmx_prereg_pyme " +
							" WHERE ic_prereg_pyme = ? " ;
				}			
			} else if(tipoAfiliado.equals("10")) {	//---------------------------------------Cliente Externo
				//QUERY PARA TRAERNOS EL TIPO DE PERSONA
				/*String qryTipoPersona = 
						" SELECT cs_tipo_persona " +
						" FROM comcat_clientes " +
						" WHERE ic_cliente = ? ";
				*/
				String qryTipoPersona = 
						" SELECT cs_tipo_persona " +
						" FROM comcat_cli_externo " +
						" WHERE ic_nafin_electronico = ? ";
				String strTipoPersona = "";
				List params = new ArrayList();
				params.add(new Integer(claveAfiliado));
				Registros regTipoPersona = con.consultarDB(qryTipoPersona, params);
		
				if(regTipoPersona.next()) {
					strTipoPersona = regTipoPersona.getString("CS_TIPO_PERSONA");
				}
				if(strTipoPersona.equals("M")) {
					strSQL = 
							" SELECT cg_razon_social AS nombre, '20secure/carga_variables.jsp' " +
							" FROM comcat_cli_externo " +//" FROM comcat_clientes " +
							" WHERE ic_nafin_electronico =  ? ";
				} else {
					strSQL = 
							" SELECT cg_nombre || ' ' || cg_appat || ' ' || cg_apmat AS nombre " +
							" FROM comcat_cli_externo " +//" FROM comcat_clientes " +
							" WHERE ic_nafin_electronico = ? ";
				}

			} else if(tipoAfiliado.equals("M") || tipoAfiliado.equals("11")) {	//---------------------------------------Mandante
				//QUERY PARA TRAERNOS EL TIPO DE PERSONA
				String qryTipoPersona = 
						" SELECT cs_tipo_persona " +
						" FROM comcat_mandante " +
						" WHERE ic_mandante = ? ";
				//out.print(qryTipoPersona+"<br>");
				String strTipoPersona = "";
				List params = new ArrayList();
				params.add(new Integer(claveAfiliado));
				Registros regTipoPersona = con.consultarDB(qryTipoPersona, params);
		
				if(regTipoPersona.next()) {
					strTipoPersona = regTipoPersona.getString("CS_TIPO_PERSONA");
				}
		
				if(strTipoPersona.equals("M")) {
					strSQL = 
							" SELECT cg_razon_social as NOMBRE " +
							" FROM comcat_mandante " +
							" WHERE ic_mandante =  ? ";
				} else {
					strSQL = 
							" SELECT cg_nombre || ' ' || cg_appat || ' ' || cg_apmat AS nombre "+
							" FROM comcat_mandante " +
							" WHERE ic_mandante = ? " ;
				}
			} else if(tipoAfiliado.equals("A") || tipoAfiliado.equals("12")) {	//---------------------------------------Afianzadora
				strSQL = 
						" SELECT cg_razon_social as NOMBRE " +
						" FROM fe_afianzadora " +
						" WHERE ic_afianzadora =  ? ";
			} else if(tipoAfiliado.equals("F") || tipoAfiliado.equals("13")) {	//---------------------------------------Fiado
				strSQL = 
						" SELECT cg_nombre as NOMBRE " +
						" FROM fe_fiado " +
						" WHERE ic_fiado =  ? ";			
			} else if(tipoAfiliado.equals("U") || tipoAfiliado.equals("14")) {	//---------------------------------------Universidad
				strSQL = 
						" SELECT cg_razon_social AS nombre " +
						" FROM comcat_universidad " +
						" WHERE ic_universidad = ? ";
			} else {
				throw new AppException("Error al obtener el nombre del afiliado. El tipo de afiliado no esta implementado");
			}
			
			List params = new ArrayList();
			params.add(new Integer(claveAfiliado));
			Registros regNombreEmp = con.consultarDB(strSQL, params);
			if(regNombreEmp.next()) {
				nombreAfiliado = regNombreEmp.getString(1);
			}

			return nombreAfiliado;
		} catch(Throwable t) {
			throw new AppException("Error al obtener el nombre del afiliado", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	
	/**
	 * Obtiene la raz�n social o nombre de los afiliados.
	 * @param tipoAfiliado Clave del afiliado (com_menu_afiliado_restriccion.ic_tipoAfiliado).
	 * @param ccPerfil Clave del tipo de afiliado (com_menu_afiliado_restriccion.cc_perfil).
	 * @param ccMenu Clave de la pantalla (com_menu_afiliado_restriccion.cc_menu).
	 * @param tipoRestriccion Clave de la pantalla (com_menu_afiliado_restriccion.cg_tipo_restriccion) o con notaci�n D(Directa) � R(Restringida).
	 * @return Lista de afiliados
	 */
	public static List getNombresAfiliados(String tipoAfiliado, String ccPerfil, String ccMenu, String tipoRestriccion) {
		
		AccesoDB con = new AccesoDB();
		List lstAfiliados = new ArrayList();
		String nombreAfiliado = "";
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (tipoAfiliado == null || tipoAfiliado.equals("")) {
				throw new Exception("Los parametros no pueden ser nulos ni vac�os");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" tipoAfiliado=" + tipoAfiliado);
			throw new AppException("Error en lo parametros recibidos");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL = 
				"SELECT  cm.ic_menu_afiliado_restriccion, cm.ic_nafin_electronico, " +
				"         cn.ic_epo_pyme_if, cn.cg_tipo, cm.cs_excluir " +
				"    FROM com_menu_afiliado_restriccion cm, comrel_nafin cn " +
				"   WHERE cm.ic_nafin_electronico = cn.ic_nafin_electronico " +
				"     AND cm.cc_menu = ? " +
				"     AND cm.ic_tipo_afiliado = ? " +
				"     AND cm.cc_perfil = ? " +
				"     AND cm.cg_tipo_restriccion = ? " +
				"  ORDER BY cm.ic_menu_afiliado_restriccion ";

			
			List params = new ArrayList();
			params.add(ccMenu);
			params.add(new Integer(tipoAfiliado));
			params.add(ccPerfil);
			params.add(tipoRestriccion);
			
			Registros regNombreEmp = con.consultarDB(strSQL, params);
			HashMap mpData = new HashMap();
			while(regNombreEmp.next()) {
				nombreAfiliado = getNombreAfiliado(Integer.parseInt(regNombreEmp.getString(3)), regNombreEmp.getString(4));
				mpData = new HashMap();
				mpData.put("cg_tipo", regNombreEmp.getString(4));
				mpData.put("nombre_afiliado", nombreAfiliado);
				mpData.put("ic_nafin_electronico", regNombreEmp.getString("ic_nafin_electronico"));
				mpData.put("cs_excluir", "S".equals(regNombreEmp.getString("cs_excluir"))?"true":"");
				lstAfiliados.add(mpData);
			}

			return lstAfiliados;
		} catch(Throwable t) {
			throw new AppException("Error al obtener los nombres de los afiliados", t);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
}