package com.netro.afiliacion;

import com.netro.exception.NafinException;

import java.io.Serializable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;

public class AfiliadoCE implements Serializable {
	private String cg_rfc				= null;
	private String cg_num_solicitud		= null;
	private String df_fecha				= null;
	private String cg_nombre			= null;
	private String cg_appat				= null;
	private String cg_apmat				= null;
	private String cg_sexo				= null;
	private String cg_razon_social		= null;
	private long in_numero_emp			= 0;
	private double fn_ventas_netas_tot	= 0;
	private int ic_clase				= 0;
	private int ic_rama					= 0;
	private int ic_subsector			= 0;
	private int ic_sector_econ			= 0;
	private int ic_municipio			= 0;
	private int ic_estado				= 0;
	private String cg_calle				= null;
	private String cg_colonia			= null;
	private String cn_cp				= null;
	private String cs_troya				= null;
	private String cg_email				= "pyme@credito.com";
	private String cg_telefono			= "12345678";

	// Atributos adicionales
	private String cg_tipo_persona		= null;
	private String cg_fecha_nac			= null;
	private String cg_desc_estado		= null;
	private String cg_desc_munic		= null;
	private String cg_numero_sirac		= null;
	private String ic_nafin_electronico	= null;

  // Atributos del Contacto
  private String cgCelular            = null;
  private String cgContactoNombre     = null;
  private String cgContactoAppat      = null;
  private String cgContactoApmat      = null;
  private String cgContactoTelefono   = null;
	
	//CAMPOS PLD - FVR 10/02/2010
	private String cg_curp			= null;
	private String cn_fiel			= null;
	private String ic_pais_origen	= null;
	private String ic_pais			= null;
	//private int ic_estado 		= 0;
	private String ic_ciudad 		= null;

	private int ic_if					= 0; // Clave del Intermediario Financiero
	private String usuarioSirac			= null; // Clave del usuario en SIRAC para el enlace

	private String ic_folio			= null; // Clave que requiere el FIDE, ese valor lo usan los IF's y regresamos exactamente el mismo valor en los resultados

    
	private StringBuffer error 	= new StringBuffer("");
	private String codigoError 	= "0";
	PreparedStatement ps 			= null;
	String sQuery						= null;

	/** Metodo generico para la validaci�n del atributo cg_rfc, envia una exception si hay un error
	*	en la validaci�n y se guarda la descripcion en el atributo error y el codigo de error en
	*	el atributo codigoError
	*/

	public void validarRFC() throws NafinException{
		StringTokenizer st = null;

		try {
			System.out.println("AfiliadoCE.validarRFC(E)");
			error.delete(0, error.length());
			codigoError = "0";

			if("".equals(cg_rfc) || cg_rfc == null) {
				error.append("El campo RFC es obligatorio.");
				codigoError = "9001";
				throw new NafinException("AFIL0045");
			} else if(cg_rfc.length() > 15) {
				error.append("El RFC ("+cg_rfc+") excede la longitud permitida que es de 15 Pna. F�sica y 14 Pna. Moral. El Formato es:\"(N)NNN-AAMMDD-XXX\", (N)NNN son las Iniciales 4 para Pna. F�sica y 3 para Pna. Moral, AAMMDD fecha de nacimiento o creaci�n de la empresa y XXX es la Homoclave");
				codigoError = "9002";
				throw new NafinException("AFIL0045");
			} else {
				st = new StringTokenizer(cg_rfc,"-");
				if(st.countTokens()==3) {
					boolean bIniciales = true;
					String sIniciales = st.nextToken();
					for(int i=0; i<	sIniciales.length(); i++) {
						Character c = new Character(sIniciales.charAt(i));
						if(!Character.isLetter(sIniciales.charAt(i)) )
							bIniciales = false;
					}
					if(!bIniciales) {
						error.append("Las Iniciales del RFC ("+cg_rfc+") son incorrectas Solo Letras, 4 para Pna. F�sica y 3 para Pna. Moral.");
						codigoError = "9003";
						throw new NafinException("AFIL0045");
					}

					boolean bFecha = true;
					String sFecha = st.nextToken();
					for(int i=0; i<sFecha.length(); i++) {
						Character c = new Character(sFecha.charAt(i));
						if(!Character.isDigit(sFecha.charAt(i)) )
							bFecha = false;
					}
					if(!bFecha) {
						error.append("La Fecha del RFC ("+cg_rfc+") no es valida Solo Valores N�meros.");
						codigoError = "9004";
						throw new NafinException("AFIL0045");
					} else {
						int ano = Integer.parseInt(sFecha.substring(0,2));
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
						int finAnoActual = Integer.parseInt(sdf.format(new java.util.Date()).substring(2,4));
						int Ano = (ano>finAnoActual && ano<=99)?Integer.parseInt("19"+sFecha.substring(0,2)):Integer.parseInt("20"+sFecha.substring(0,2));
						int Mes = Integer.parseInt(sFecha.substring(2,4));
						int Dia = Integer.parseInt(sFecha.substring(4,6));
						if(!Comunes.checaFecha(Dia+"/"+Mes+"/"+Ano)) {
							error.append("La Fecha del RFC ("+cg_rfc+") es inexistente, por favor revisela.");
							codigoError = "9005";
							throw new NafinException("AFIL0045");
						} else {
							Calendar gc = new GregorianCalendar(Ano, Mes-1, Dia);
							java.util.Date dFechaRFC = gc.getTime();
							int iResComp = dFechaRFC.compareTo(new java.util.Date());
							
							System.out.println( Ano +"--"+ Mes +"--"+  Dia);
							System.out.println("dFechaRFC  "+dFechaRFC);
							
							if(iResComp > 0) {
								error.append("La fecha del RFC ("+cg_rfc+") debe de ser menor o igual a la de hoy.");
								codigoError = "9006";
								throw new NafinException("AFIL0045");
							} else { // Obtenemos la Fecha de Nacimiento del RFC, solo persona Fisica (En moral validarse el tipo persona, se asigna a null).
								cg_fecha_nac = sFecha.substring(4,6)+"/"+sFecha.substring(2,4)+"/"+Ano;
							}
						}
					}

					boolean bHomo = true;
					String sHomoclave = st.nextToken();
					for(int i=0; i<sHomoclave.length(); i++) {
						Character c = new Character(sHomoclave.charAt(i));
						if(!Character.isLetterOrDigit(sHomoclave.charAt(i)) )
							bHomo = false;
					}
					if(!bHomo) {
						error.append("La Homoclave del RFC ("+cg_rfc+") no es valida.");
						codigoError = "9007";
						throw new NafinException("AFIL0045");
					}

				} else {
					error.append("El Formato del RFC ("+cg_rfc+") es incorrecto, ejemplo: (N)NNN-AAMMDD-XXX, (N)NNN son las Iniciales 4 para Pna. F�sica y 3 para Pna. Moral, AAMMDD fecha de nacimiento o creaci�n de la empresa y XXX es la Homoclave.");
					codigoError = "9008";
					throw new NafinException("AFIL0045");
				}
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarRFC(Exception)");
			e.printStackTrace();
			error.append("Error inesperado RFC ("+cg_rfc+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarRFC(S)");
		}

	}


	public void validarTipoPersona() throws NafinException{

		String cgNombre 	= (cg_nombre==null)?"":cg_nombre;
		String cgAppat 	= (cg_appat==null)?"":cg_appat;
		String cgApmat 	= (cg_apmat==null)?"":cg_apmat;
		String cgSexo 		= (cg_sexo==null)?"":cg_sexo;
		String cgRazonSocial = (cg_razon_social==null)?"":cg_razon_social;
		
		//CAMPOS PLD FVR 10/02/2010
		String cnFiel		= (cn_fiel==null)?"":cn_fiel;
		String cgCurp		= (cg_curp==null)?"":cg_curp;
		try {
			System.out.println("AfiliadoCE.validarTipoPersona(E)");
			error.delete(0, error.length());
			codigoError = "0";

			if( (!cgNombre.equals("") || !cgAppat.equals("") || !cgApmat.equals("") || !cgSexo.equals("") || !cgCurp.equals("")) && (!cgRazonSocial.equals("")) ) {
				error.append("Datos Inconsistentes. No puede registrar una persona moral y complementar datos de persona fisica.");
				codigoError = "9009";
				throw new NafinException("AFIL0045");
			}
			if(cgRazonSocial.equals("")) {
				//cg_tipo_persona = "F";
				if(cgNombre.equals("") && cgAppat.equals("") && cgApmat.equals("") && cgSexo.equals("")) {
					error.append("Datos inconsistentes, No es posible determinar si se trata de persona F�sica o Moral.");
					codigoError = "9010";
					throw new NafinException("AFIL0045");
				} else if(cgNombre.equals("") || cgAppat.equals("") || cgApmat.equals("") || cgSexo.equals("")) {
					error.append("Debe de introducir el Nombre, Apellido Paterno, Apellido Materno y el Tipo de Sexo para Persona F�sica.");
					codigoError = "9011";
					throw new NafinException("AFIL0045");
				}
				if(!cgNombre.equals("") && !cgAppat.equals("") && !cgApmat.equals("") && !cgSexo.equals("")) {
					cg_tipo_persona="F";
					validarSexo();
				}
			}

			if(cgNombre.equals("") && cgAppat.equals("") && cgApmat.equals("") && cgSexo.equals("") && cgCurp.equals("")) {
				if(cgRazonSocial.equals("")) {
					error.append("Debe de introducir la Raz�n Social para Persona Moral.");
					codigoError = "9012";
					throw new NafinException("AFIL0045");
				} else {
					cg_tipo_persona="M";
					cg_fecha_nac = null; // Se aigna a null la propiedad ya que  no debe asignarse en Morales
				}
			}


		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarTipoPersona(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Tipo de Persona: " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarTipoPersona(S)");
		}

	}

	public void validarSexo() throws NafinException{

		try {
			System.out.println("AfiliadoCE.validarSexo(E)");
			error.delete(0, error.length());
			codigoError = "0";
			if(!"F".equals(cg_sexo) && !"M".equals(cg_sexo)) {
				error.append("El tipo de Sexo ("+cg_sexo+") no existe, (F) Femenimo y (M) Masculino.");
				codigoError = "9013";
				throw new NafinException("AFIL0045");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarSexo(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Sexo ("+cg_sexo+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarSexo(S)");
		}

	}


	public void validarNumeroEmpleados() throws NafinException{

		try {
			System.out.println("AfiliadoCE.validarNumeroEmpleados(E)");
			error.delete(0, error.length());
			codigoError = "0";
			if(in_numero_emp <= 0 ) {
				error.append("El numero de empleados ("+in_numero_emp+") debe ser mayor a 0");
				codigoError = "9014";
				throw new NafinException("AFIL0045");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarNumeroEmpleados(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Numero Empleados ("+in_numero_emp+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarNumeroEmpleados(S)");
		}

	}

	public void validarVtasNetasTot() throws NafinException{

		try {
			System.out.println("AfiliadoCE.validarVtasNetasTot(E)");
			error.delete(0, error.length());
			codigoError = "0";
			if(fn_ventas_netas_tot <= 5000 ) {
				error.append("Las ventas netas totales ("+fn_ventas_netas_tot+") debe ser mayor a 5000");
				codigoError = "9015";
				throw new NafinException("AFIL0045");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarVtasNetasTot(Exception)");
			e.printStackTrace();
			error.append("Error inesperado VtasNetasTotales ("+fn_ventas_netas_tot+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarVtasNetasTot(S)");
		}

	}

	public void validarDomicilio() throws NafinException{

		try {
			System.out.println("AfiliadoCE.validarDomicilio(E)");
			error.delete(0, error.length());
			codigoError = "0";
			if(cg_calle == null || "".equals(cg_calle) ) {
				error.append("Debe introducir el campo calle ");
				codigoError = "9016";
				throw new NafinException("AFIL0045");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarDomicilio(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Domicilio ("+cg_calle+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarDomicilio(S)");
		}

	}

	public void validarColonia() throws NafinException{
		try {
			System.out.println("AfiliadoCE.validarColonia(E)");
			error.delete(0, error.length());
			codigoError = "0";
			if(cg_colonia == null || "".equals(cg_colonia) ) {
				error.append("Debe introducir el campo colonia ");
				codigoError = "9017";
				throw new NafinException("AFIL0045");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarColonia(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Colonia ("+cg_colonia+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarColonia(S)");
		}

	}

	public void validarCP() throws NafinException{
		try {
			System.out.println("AfiliadoCE.validarCP(E)");
			error.delete(0, error.length());
			codigoError = "0";
			if(cn_cp == null || "".equals(cn_cp) ) {
				error.append("Debe introducir el campo Codigo Postal ");
				codigoError = "9018";
				throw new NafinException("AFIL0045");
			}
			if(!Comunes.esNumero(cn_cp) ) {
				error.append("Deben de introducir solo valores num�ricos en el C�digo Postal.");
				codigoError = "9019";
				throw new NafinException("AFIL0045");
			}
			if(cn_cp.length()>5) {
				error.append("Deben de introducir m�ximo 5 numeros en el codigo postal");
				codigoError = "9020";
				throw new NafinException("AFIL0045");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarCP(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Codigo Postal ("+cn_cp+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarCP(S)");
		}

	}

	public void validarTroya() throws NafinException{
		try {
			System.out.println("AfiliadoCE.validarTroya(E)");
			error.delete(0, error.length());
			codigoError = "0";
			if(cs_troya == null || "".equals(cs_troya) ) {
				error.append("Debe introducir el campo Codigo Postal ");
				codigoError = "9021";
				throw new NafinException("AFIL0045");
			}
			if(!cs_troya.equalsIgnoreCase("S") && !cs_troya.equalsIgnoreCase("N")) {
				error.append("Deben de venir S para su alta a Troya � N en caso de no darse de alta.");
				codigoError = "9021";
				throw new NafinException("AFIL0045");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarTroya(Exception)");
			e.printStackTrace();
			error.append("Error inesperado troya ("+cs_troya+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarTroya(S)");
		}

	}

	public void validarEmail() throws NafinException{
		try {
			System.out.println("AfiliadoCE.validarEmail(E)");
			error.delete(0, error.length());
			codigoError = "0";
			if(cg_email == null || "".equals(cg_email) ) {
				error.append("Debe introducir el campo Email ");
				codigoError = "9022";
				throw new NafinException("AFIL0045");
			}
			if(cg_email.length() > 30) {
				error.append("El Email excede la longitud permitida que es de 30");
				codigoError = "9023";
				throw new NafinException("AFIL0045");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarEmail(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Email ("+cg_email+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarEmail(S)");
		}

	}

	public void validarTelefono() throws NafinException{
		try {
			System.out.println("AfiliadoCE.validarTelefono(E)");
			error.delete(0, error.length());
			codigoError = "0";
			if(cg_telefono == null || "".equals(cg_telefono) ) {
				error.append("Debe introducir el campo Telefono ");
				codigoError = "9024";
				throw new NafinException("AFIL0045");
			}
			if(cg_telefono.length() > 30) {
				error.append("El Telefono excede la longitud permitida que es de 30");
				codigoError = "9025";
				throw new NafinException("AFIL0045");
			}
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarTelefono(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Telefono ("+cg_telefono+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarTelefono(S)");
		}

	}

	public void validarClavesSectorEcon(AccesoDB con) throws NafinException{
		ResultSet rs = null;
		try {
			System.out.println("AfiliadoCE.validarClavesSectorEcon(E)");
			error.delete(0, error.length());
			codigoError = "0";
			ps = null;
			sQuery = null;

			sQuery = "select SE.cd_nombre, 'AfiliadoCE.validarClavesSectorEcon(query)' as Bean "+
					"from comcat_sector_econ SE, comcat_subsector SUB, comcat_clase C, comcat_rama R "+
					"where SE.ic_sector_econ = SUB.ic_sector_econ "+
					"and SUB.ic_subsector = R.ic_subsector "+
					"and SUB.ic_sector_econ = R.ic_sector_econ "+
					"and R.ic_rama = C.ic_rama "+
					"and R.ic_sector_econ = C.ic_sector_econ "+
					"and R.ic_subsector = C.ic_subsector "+
					"and C.ic_sector_econ = ? " +
					"and C.ic_subsector = ?" +
					"and C.ic_rama = ?" +
					"and C.ic_clase = ?";
			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, ic_sector_econ);
			ps.setInt(2, ic_subsector);
			ps.setInt(3, ic_rama);
			ps.setInt(4, ic_clase);
			rs = ps.executeQuery();
			if(!rs.next()) {
				error.append("Alguna de las claves no existe para el Sector Economico, Subsector, Rama y Clase especificada.");
				codigoError = "9026";
				throw new NafinException("AFIL0045");
			}
			if(ps != null) ps.close();
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarClavesSectorEcon(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Telefono ("+cg_telefono+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarClavesSectorEcon(S)");
		}

	}


	public void validarMunicipioEstado(AccesoDB con) throws NafinException{
		ResultSet rs = null;
		try {
			System.out.println("AfiliadoCE.validarMunicipioEstado(E)");
			error.delete(0, error.length());
			codigoError = "0";
			ps = null;
			sQuery = null;

			if(ic_municipio == 0) {
				error.append("La clave del municipio no es valida");
				codigoError = "9027";
				throw new NafinException("AFIL0045");
			}
			if(ic_estado == 0) {
				error.append("La clave del estado no es valida");
				codigoError = "9028";
				throw new NafinException("AFIL0045");
			}

			sQuery = "select E.cd_nombre as estado, M.cd_nombre as municipio, 'AfiliadoCE.validarClavesSectorEcon(query)' as Bean " +
					" from comcat_estado E, comcat_municipio M "+
					" where E.ic_estado = M.ic_estado "+
					" and M.ic_municipio = ?"+
					" and M.ic_estado = ?"+
					" and M.ic_pais = 24";

			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, ic_municipio);
			ps.setInt(2, ic_estado);
			rs = ps.executeQuery();
			if(rs.next()) {
				cg_desc_estado = rs.getString("ESTADO");
				cg_desc_munic = rs.getString("MUNICIPIO");
			} else {
				error.append("Alguna de las claves no existe para el Estado y el Municipio proporcionado");
				codigoError = "9029";
				throw new NafinException("AFIL0045");
			}
			if(ps != null) ps.close();
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			System.out.println("AfiliadoCE.validarMunicipioEstado(Exception)");
			e.printStackTrace();
			error.append("Error inesperado Estado ("+ic_estado+"), Municipio ("+ic_municipio+"): " + e.toString());
			codigoError = "9000"; // Error generico, sin causa
			throw new NafinException("AFIL0045");
		} finally {
			System.out.println("AfiliadoCE.validarMunicipioEstado(S)");
		}

	}
	
	public void validarDomicilioPLD(AccesoDB con)throws NafinException{
		ResultSet rs = null;
		int existeClave = 0;
		String table = "";
		String condiciones = "";
		
		try{
			System.out.println("AfiliadoCE.validarDomicilioPLD(E)");
			error.delete(0, error.length());
			codigoError = "0";
			ps = null;
			sQuery = null;
			
			//validacion para Pais Origen
			if(ic_pais_origen !=null && !ic_pais_origen.equals("") ){
				if(!Comunes.esNumero(ic_pais_origen) ) {
					error.append("Deben de introducir solo valores num�ricos en la Clave del Pais de Origen.");
					codigoError = "9037";
					throw new NafinException("AFIL0045");
				}
								
				sQuery = "select count(1) existe from comcat_pais where ic_pais = ? ";
				ps = con.queryPrecompilado(sQuery);
				ps.setInt(1,Integer.parseInt(ic_pais_origen));
				rs = ps.executeQuery();
				if(rs.next())
					existeClave = rs.getInt("existe");
					
				
				if(rs != null) rs.close();
				if(ps != null) ps.close();
				

				if(existeClave == 0) {
					error.append("No existe la clave introducida para el pais origen");
					codigoError = "9038";
					throw new NafinException("AFIL0045");
				}

				if(ic_pais_origen.length()>2) {
					error.append("Deben de introducir m�ximo 2 numeros en el pais de origen");
					codigoError = "9039";
					throw new NafinException("AFIL0045");
				}
			}//finaliza validacion Pais Origen
			
			/********************************************************************************/
			//validacion Pais			
			if(ic_pais !=null && !ic_pais.equals("") ){
				if(!Comunes.esNumero(ic_pais) ) {
					error.append("Deben de introducir solo valores num�ricos en la Clave del Pais.");
					codigoError = "9040";
					throw new NafinException("AFIL0045");
				}
				
				if(ic_pais.length()>2) {
					error.append("Deben de introducir m�ximo 2 numeros en la clave del pais");
					codigoError = "9041";
					throw new NafinException("AFIL0045");
				}
								
				table = " comcat_pais cp ";
				condiciones = "where cp.ic_pais = ? ";
				//sQuery = "select count(1) existe from comcat_pais where ic_pais = ? ";				
			}//finaliza validacion Pais Origen
			
			//validacion Estado
			if(ic_estado !=0 ){
				if(!Comunes.esNumero(String.valueOf(ic_estado))) {
					error.append("Deben de introducir solo valores num�ricos en la Clave del Estado.");
					codigoError = "9042";
					throw new NafinException("AFIL0045");
				}
				
				if(String.valueOf(ic_estado).length()>2) {
					error.append("Deben de introducir m�ximo 2 numeros en la clave del estado ");
					codigoError = "9043";
					throw new NafinException("AFIL0045");
				}
								
				table += !table.equals("")?", comcat_estado ce ":" comcat_estado ce ";
				condiciones += !condiciones.equals("")?" and ce.codigo_departamento = ?  and cp.ic_pais=ce.ic_pais ":" where ce.codigo_departamento = ? ";
				//sQuery = "select count(1) existe from comcat_pais where ic_pais = ? ";				
			}//finaliza validacion Estado
			
			//validacion Ciudad
			if(ic_ciudad!=null && !ic_ciudad.equals("") ){
				if(!Comunes.esNumero(ic_ciudad)) {
					error.append("Deben de introducir solo valores num�ricos en la clave de la Ciudad.");
					codigoError = "9044";
					throw new NafinException("AFIL0045");
				}
				
				if(ic_ciudad.length()>2) {
					error.append("Deben de introducir m�ximo 2 numeros en la clave de la Ciudad ");
					codigoError = "9045";
					throw new NafinException("AFIL0045");
				}
								
				table += !table.equals("")?", comcat_ciudad cc ":" comcat_ciudad cc ";
				condiciones += !condiciones.equals("")?" and cc.codigo_ciudad = ?  and ce.codigo_departamento = cc.codigo_departamento ":" where cc.codigo_ciudad = ? ";
				//sQuery = "select count(1) existe from comcat_pais where ic_pais = ? ";				
			}//finaliza validacion Pais Origen
			
			/***************************************************************************/
						
			/*
				//validacion del estado del domicilio fiscal
				if(ic_estado!=0){
					if(ic_pais==null || ic_pais.equals("") ){
						error.append("Al indicar el estado se requiere clave de pais.");
						codigoError = "9046";
						throw new NafinException("AFIL0045");
					}
				}//finaliza validacion Estado
				
				
				if(ic_ciudad!=null && !ic_ciudad.equals("")){
					if(ic_pais==null || ic_pais.equals("") || ic_estado==0){
						error.append("Al indicar la ciudad se requiere clave de pais y estado");
						codigoError = "9047";
						throw new NafinException("AFIL0045");
					}
				}//finaliza validacion de la calve de la ciudad
			*/
			int i = 0;
			existeClave = 0;
			
			if (!table.equals("")){
				sQuery = "select count(1) existe from "+table+condiciones;
				
				System.out.println("sQuery>>>>pais = "+ic_pais+" estado = "+ic_estado+" ciudad = "+ic_ciudad +" query ==="+sQuery);
				
				ps = con.queryPrecompilado(sQuery);
				
				if(ic_pais !=null && !ic_pais.equals("") )
					ps.setInt(++i,Integer.parseInt(ic_pais));
				if(ic_estado!=0)
					ps.setInt(++i,ic_estado);
				if(ic_ciudad!=null && !ic_ciudad.equals(""))
					ps.setInt(++i,Integer.parseInt(ic_ciudad));
				
				rs = ps.executeQuery();
				
				if(rs.next()){
					existeClave = rs.getInt("existe");
				}
				if(rs != null) rs.close();
				if(ps != null) ps.close();
					
				if(existeClave == 0) {
					error.append("No existe la relacion pais-estado-ciudad");
					codigoError = "9048";
					throw new NafinException("AFIL0045");
				}
			}
			
			
		}catch(NafinException ne){
			ne.printStackTrace();
			throw ne;
		}catch(Exception e){
			
		}finally{
			
		}
	}

	public String getInsertPymeCE(){//se agrgan los campos cg_curp y cn_fiel CAMPOS PLD -FVR 11/02/2010
		return "insert into comcat_pyme(ic_pyme, ic_clase, ic_rama, ic_subsector, "+
									"ic_sector_econ, cg_appat, cg_apmat, cg_nombre, cg_razon_social, "+
									"cg_nombre_comercial, cg_rfc, cg_sexo, df_nacimiento, "+
									"ic_pais_origen, cs_tipo_persona, in_numero_emp, "+
									"ic_estrato, fn_ventas_net_tot, ic_tipo_cliente, " +
									"in_numero_sirac, cs_credele, cg_curp, cn_fiel) " +
									" values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, to_date(?,'dd/mm/yyyy'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	}

	public String getInsertContactoPymeCE(){
    
		return "insert into com_contacto(ic_contacto, ic_epo, ic_if, ic_pyme, "+
                    "cg_appat, cg_apmat, cg_nombre, cg_tel, cg_fax, "+
									"cg_email, cs_primer_contacto, cg_celular ) "+
									" values(SEQ_COM_CONTACTO_IC_CONTACTO.NextVal, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	}


	public String getUpdatePymeCE(){
		/*return "update comcat_pyme set in_numero_sirac ="+cg_numero_sirac+
				", cs_credele = 'S' "+
				" where ic_pyme = ?" ;*/
		/*return "update comcat_pyme set in_numero_sirac = ?" +
				", cs_credele = ? "+
				" where ic_pyme = ?" ;JC*/
        
      return "UPDATE comcat_pyme SET "+
				"ic_clase = ?" +
				", ic_rama = ?"+
				", ic_subsector = ?"+
				", ic_sector_econ = ?"+
				", cg_appat = ?"+
				", cg_apmat = ?"+
				", cg_nombre = ?"+
				", cg_razon_social = ?"+
				", cg_nombre_comercial = ?"+
				", cg_rfc = ?"+
				", cg_sexo = ?"+
				", df_nacimiento = to_date(?,'dd/mm/yyyy') "+
				", ic_pais_origen = ?"+
				", cs_tipo_persona = ?"+
				", in_numero_emp = ?"+
				", ic_estrato = ?"+
				", fn_ventas_net_tot = ?"+
				", ic_tipo_cliente = ?"+
				", in_numero_sirac = ?"+
				", cs_credele = ?"+
				", cg_curp = ?"+//SE AGREGA CAMPO  - CAMPOS PLD - FVR 11/02/2010
				", cn_fiel = ?"+//SE AGREGA CAMPO  - CAMPOS PLD - FVR 11/02/2010
				" WHERE ic_pyme = ?";      
	}

  public String getUpdateDomicilioCE(){
    return " UPDATE com_domicilio SET "+
      " CG_CALLE = ? "+
      ", CG_COLONIA = ? "+
      ", IC_ESTADO = ? "+
      ", IC_PAIS = ? "+
      ", IC_MUNICIPIO = ? "+
      ", CG_MUNICIPIO = ? "+
      ", CN_CP = ? "+
      ", CS_FISCAL = ? "+
      ", CG_TELEFONO1 = ? "+
      ", CG_EMAIL = ? "+
		", IC_CIUDAD = ? "+////SE AGREGA CAMPO  - CAMPOS PLD - FVR 16/02/2010
      " WHERE IC_PYME = ?";
  }  
  

	public String getUpdateContactoPymeCE(){
		return "UPDATE com_contacto SET "+
        " cg_appat = ? "+
        ", cg_apmat = ? "+
        ", cg_nombre = ? "+
        ", cg_tel = ? "+
        ", cg_fax = ? "+
				", cg_email = ? "+
        ", cg_celular = ? "+
      " WHERE IC_PYME = ?";
	}



	public String getInsertPymeIFCE(){
		return " INSERT into comrel_pyme_if_credele (ic_pyme, ic_if, ic_usuario) "+
				" values (?, ?, ?) ";
	}

	public String getInsertDomicilioCE(){//SE AGREGA CAMPO IC_CIUDAD - CAMPOS PLD - FVR 11/02/2010
		return "insert into com_domicilio (IC_DOMICILIO_EPO,IC_PYME,CG_CALLE,"+
				"CG_COLONIA,IC_ESTADO,IC_PAIS,IC_MUNICIPIO,CG_MUNICIPIO,CN_CP,CS_FISCAL,CG_TELEFONO1,CG_EMAIL, IC_CIUDAD) "+
				"values (seq_com_domicilio_ic_domicilio.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	}

	//setters
	public void setCgRfc(String cg_rfc){
		this.cg_rfc = cg_rfc;
	}
	public void setCgNumSolicitud(String cg_num_solicitud){
		this.cg_num_solicitud = cg_num_solicitud;
	}
	public void setDfFecha(String df_fecha){
		this.df_fecha = df_fecha;
	}
	public void setCgNombre(String cg_nombre){
		this.cg_nombre = cg_nombre;
	}
	public void setCgAppat(String cg_appat){
		this.cg_appat = cg_appat;
	}
	public void setCgApmat(String cg_apmat){
		this.cg_apmat = cg_apmat;
	}
	public void setCgSexo(String cg_sexo){
		this.cg_sexo = cg_sexo;
	}
	public void setCgRazonSocial(String cg_razon_social){
		this.cg_razon_social = cg_razon_social;
	}
	public void setInNumeroEmp(long in_numero_emp){
		this.in_numero_emp = in_numero_emp;
	}
	public void setFnVentasNetasTot(double fn_ventas_netas_tot){
		this.fn_ventas_netas_tot = fn_ventas_netas_tot;
	}
	public void setIcClase(int ic_clase){
		this.ic_clase = ic_clase;
	}
	public void setIcRama(int ic_rama){
		this.ic_rama = ic_rama;
	}
	public void setIcSubsector(int ic_subsector){
		this.ic_subsector = ic_subsector;
	}
	public void setIcSectorEcon(int ic_sector_econ){
		this.ic_sector_econ = ic_sector_econ;
	}
	public void setIcMunicipio(int ic_municipio){
		this.ic_municipio = ic_municipio;
	}
	public void setIcEstado(int ic_estado){
		this.ic_estado = ic_estado;
	}
	public void setCgCalle(String cg_calle){
		this.cg_calle = cg_calle;
	}
	public void setCgColonia(String cg_colonia){
		this.cg_colonia = cg_colonia;
	}
	public void setCnCp(String cn_cp){
		this.cn_cp = cn_cp;
	}
	public void setCsTroya(String cs_troya){
		this.cs_troya = cs_troya;
	}
	public void setCgEmail(String cg_email){
		if (cg_email != null)
			this.cg_email = cg_email;
	}
	public void setCgTelefono(String cg_telefono){
		if (cg_telefono != null)
			this.cg_telefono = cg_telefono;
	}
	public void setCgNumeroSirac(String cg_numero_sirac){
		this.cg_numero_sirac = cg_numero_sirac;
	}
	public void setIcNafinElectronico(String ic_nafin_electronico){
		this.ic_nafin_electronico = ic_nafin_electronico;
	}

	public void setError(StringBuffer sbError){
		this.error = sbError;
	}
	public void setCodigoError(String sCodigoError){
		this.codigoError = sCodigoError;
	}
	public void setIcIf(int ic_if){
		this.ic_if = ic_if;
	}
	public void setUsuarioSirac(String usuarioSirac){
		this.usuarioSirac = usuarioSirac;
	}
  public void setCgCelular(String cgCelular){
		this.cgCelular = cgCelular;
	}
	public void setCgContactoNombre(String cgContactoNombre){
		this.cgContactoNombre = cgContactoNombre;
	}
	public void setCgContactoAppat (String cgContactoAppat){
		this.cgContactoAppat  = cgContactoAppat ;
	}
	public void setCgContactoApmat (String cgContactoApmat){
		this.cgContactoApmat  = cgContactoApmat ;
	}
	public void setCgContactoTelefono (String cgContactoTelefono){
		this.cgContactoTelefono  = cgContactoTelefono ;
	}
	//getters

	public String getCgRfc(){
		return cg_rfc;
	}
	public String getCgNumSolicitud(){
		return cg_num_solicitud;
	}
	public String getDfFecha(){
		return df_fecha;
	}
	public String getCgNombre(){
		return cg_nombre;
	}
	public String getCgAppat(){
		return cg_appat;
	}
	public String getCgApmat(){
		return cg_apmat;
	}
	public String getCgSexo(){
		return cg_sexo;
	}
	public String getCgRazonSocial(){
		return cg_razon_social;
	}
	public long getInNumeroEmp(){
		return in_numero_emp;
	}
	public double getFnVentasNetasTot(){
		return fn_ventas_netas_tot;
	}
	public int getIcClase(){
		return ic_clase;
	}
	public int getIcRama(){
		return ic_rama;
	}
	public int getIcSubsector(){
		return ic_subsector;
	}
	public int getIcSectorEcon(){
		return ic_sector_econ;
	}
	public int getIcMunicipio(){
		return ic_municipio;
	}
	public int getIcEstado(){
		return ic_estado;
	}
	public String getCgCalle(){
		return cg_calle;
	}
	public String getCgColonia(){
		return cg_colonia;
	}
	public String getCnCp(){
		return cn_cp;
	}
	public String getCsTroya(){
		return cs_troya;
	}
	public String getCgEmail(){
		return cg_email;
	}
	public String getCgTelefono(){
		return cg_telefono;
	}
	public String getCgTipoPersona(){
		return cg_tipo_persona;
	}
	public String getCgFechaNac(){
		return cg_fecha_nac;
	}
	public String getCgDescEstado(){
		return cg_desc_estado;
	}
	public String getCgDescMunic(){
		return cg_desc_munic;
	}
	public String getCgNumeroSirac(){
		return cg_numero_sirac;
	}
	public String getIcNafinElectronico(){
		return ic_nafin_electronico;
	}
	public StringBuffer getError(){
		return error;
	}
	public String getCodigoError(){
		return codigoError;
	}
	public int getIcIf(){
		return ic_if;
	}
	public String getUsuarioSirac(){
		return usuarioSirac;
	}
	public String getCgCelular(){
		return cgCelular;
	}
  public String getCgContactoNombre(){
		return cgContactoNombre;
	}
  public String getCgContactoAppat(){
		return cgContactoAppat;
	}
  public String getCgContactoApmat(){
		return cgContactoApmat;
	}
  public String getCgContactoTelefono(){
		return cgContactoTelefono;
	}
	public String toString(){
		String strAfiliadoCE =
			"\n[\n cg_rfc				= "+cg_rfc+
			",\n cg_num_solicitud		= "+cg_num_solicitud+
			",\n df_fecha				= "+df_fecha+
			",\n cg_nombre				= "+cg_nombre+
			",\n cg_appat				= "+cg_appat+
			",\n cg_apmat				= "+cg_apmat+
			",\n cg_sexo				= "+cg_sexo+
			",\n cg_razon_social		= "+cg_razon_social+
			",\n in_numero_emp			= "+in_numero_emp+
			",\n fn_ventas_netas_tot	= "+fn_ventas_netas_tot+
			",\n ic_clase				= "+ic_clase+
			",\n ic_rama				= "+ic_rama+
			",\n ic_subsector			= "+ic_subsector+
			",\n ic_sector_econ			= "+ic_sector_econ+
			",\n ic_municipio			= "+ic_municipio+
			",\n ic_estado				= "+ic_estado+
			",\n cg_calle				= "+cg_calle+
			",\n cg_colonia				= "+cg_colonia+
			",\n cn_cp					= "+cn_cp+
			",\n cs_troya				= "+cs_troya+
			",\n cg_email				= "+cg_email+
			",\n cg_telefono			= "+cg_telefono+
			",\n cg_tipo_persona		= "+cg_tipo_persona+
			",\n cg_fecha_nac			= "+cg_fecha_nac+
			",\n cg_numero_sirac		= "+cg_numero_sirac+
			",\n ic_nafin_electronico	= "+ic_nafin_electronico+
			",\n ic_if					= "+ic_if+
			",\n usuarioSirac			= "+usuarioSirac+
			//SE AGREGAN LOS CAMPOS QUE SE AGREGARON ULTIMAMENTE - FVR 10/02/2010
			",\n cgCelular				= "+cgCelular+
			",\n cgContactoNombre	= "+cgContactoNombre+
			",\n cgContactoAppat		= "+cgContactoAppat+
			",\n cgContactoApmat		= "+cgContactoApmat+
			",\n cgContactoTelefono	= "+cgContactoTelefono+
			",\n cg_curp				= "+cg_curp+
			",\n cn_fiel				= "+cn_fiel+
			",\n ic_pais_origen		= "+ic_pais_origen+
			",\n ic_pais				= "+ic_pais+
			",\n ic_ciudad				= "+ic_ciudad+
			",\n codigoError			= "+codigoError+
			",\n error					= "+error.toString()+
			"\n]";

		return strAfiliadoCE;
	}


	public void setCg_curp(String cg_curp) {
		this.cg_curp = cg_curp;
	}


	public String getCg_curp() {
		return cg_curp;
	}


	public void setCn_fiel(String cn_fiel) {
		this.cn_fiel = cn_fiel;
	}


	public String getCn_fiel() {
		return cn_fiel;
	}


	public void setIc_pais_origen(String ic_pais_origen) {
		this.ic_pais_origen = ic_pais_origen;
	}


	public String getIc_pais_origen() {
		return ic_pais_origen;
	}


	public void setIc_pais(String ic_pais) {
		this.ic_pais = ic_pais;
	}


	public String getIc_pais() {
		return ic_pais;
	}


	public void setIc_ciudad(String ic_ciudad) {
		this.ic_ciudad = ic_ciudad;
	}


	public String getIc_ciudad() {
		return ic_ciudad;
	}


  public void setIcFolio(String ic_folio) {
    this.ic_folio = ic_folio;
  }


  public String getIcFolio() {
    return ic_folio;
  }



}
