package com.netro.afiliacion;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class ResultadosEnl {

	private String	cc_acuse			= null;
	private	String	cg_rfc 				= null;
	private String	cg_num_solicitud	= null;
	private String	in_numero_sirac		= null;
	private String	cc_rechazo			= null;
	private String	cg_desc_rechazo		= null;
	private String	ic_folio		= null;

	//getters
	public String getCcAcuse(){
		return cc_acuse;
	}
	public String getCgRfc(){
		return cg_rfc;
	}
	public String getCgNumSolicitud(){
		return cg_num_solicitud;
	}
	public String getInNumeroSirac(){
		return in_numero_sirac;
	}
	public String getCcRechazo(){
		return cc_rechazo;
	}
	public String getCgDescRechazo(){
		return cg_desc_rechazo;
	}
	public String getIcFolio(){
		return ic_folio;
	}
	//setters
	public void setCcAcuse(String cc_acuse){
		this.cc_acuse = cc_acuse;
	}
	public void setCgRfc(String cg_rfc){
		this.cg_rfc = cg_rfc;
	}
	public void setCgNumSolicitud(String cg_num_solicitud){
		this.cg_num_solicitud = cg_num_solicitud;
	}
	public void setInNumeroSirac(String in_numero_sirac){
		this.in_numero_sirac = in_numero_sirac;
	}
	public void setCcRechazo(String cc_rechazo){
		this.cc_rechazo = cc_rechazo;
	}
	public void setCgDescRechazo(String cg_desc_rechazo){
		this.cg_desc_rechazo = cg_desc_rechazo;
	}
	public void setIcFolio(String ic_folio){
		this.ic_folio = ic_folio;
	}

}
