package com.netro.afiliacion;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.*;
import netropology.utilerias.*;
import com.netro.exception.*;

/**
 * Esta clase carga archivos de Excel de una Pyme
 * @author Hugo Vargas 08/11/2011
 *
 */
public class CargaArchivoPyme{

	public CargaArchivoPyme(){};


	/**
	 * Establece el tama�o de un archivo
	 * @param tama�o de un archivo
	 *
	 */
	public void setSize(int tamanio) {
		this.tamanio = tamanio;
	}

	/**
	 * Establece la clave de la epo
	 * @param claveEpo Clave de la epo
	 *
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	/**
	 * Establece la extensi�n del archivo
	 * @param extension Extension del archivo
	 *
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * Obtiene la extensi�n del archivo
	 * @return extension
	 *
	 */
	public String getExtension() {
		return this.extension;
	}

	/**
	 * Inserta el archivo cargado Pyme.
	 * @param , el usuario
	 * @return "folio" si el resultado es correcto.
	 */
	public String insertaCargaArchivoPyme(String nombreDocto, InputStream bi_documento,String usuario, String nombreUsuario, String fechaHora) {
		AccesoDB con = new AccesoDB();
		StringBuffer strQuery;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean bcommit = true;
		String icFolio = "";
		String fechaAct = (new SimpleDateFormat ("yyyyMMdd")).format(new java.util.Date());
		
		try {
			con.conexionDB();	

			strQuery = new StringBuffer();
			strQuery.append(" SELECT seq_com_carga_docto_pyme.NEXTVAL AS ic_folio FROM dual");
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			String ic_folio = "";
			if (rs.next()) {
				ic_folio = rs.getString("ic_folio")==null?"":rs.getString("ic_folio");
			}
			rs.close();
			ps.close();
			
			icFolio = fechaAct+cadenaAleatoria(8); 
			
			strQuery = new StringBuffer();
			strQuery.append("INSERT INTO com_carga_docto_pyme " + 
							" ( ic_folio, ic_epo, cg_nombre_docto, cg_extension, bi_documento, cg_usuario, cg_nombre_usuario, ic_estatus_carga_pyme, df_carga, df_fecha_alta ) " +
							" VALUES (?,?,?,?,?,?,?,?,TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'),TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'))");

			ps = con.queryPrecompilado(strQuery.toString());
			ps.setString(1, icFolio);
			ps.setInt(2, Integer.parseInt(claveEpo));
			ps.setString(3, nombreDocto);
			ps.setString(4, this.extension);
			ps.setBinaryStream(5, bi_documento, this.tamanio);
			ps.setString(6, usuario);
			ps.setString(7, nombreUsuario);
			ps.setInt(8, 1);
			ps.setString(9, fechaHora);
			ps.setString(10, fechaHora);
			ps.executeUpdate();
			ps.close();

			return icFolio;

		} catch (Exception e) {
			bcommit = false;
			throw new AppException("Ocurrio un error al insertar el archivo cargado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bcommit);
			  con.cierraConexionDB();
			}
		}
	}
	public static String cadenaAleatoria(int longitud) {
			String x[]={"9","8","7","6","5","4","3","2","1", "0"};
			StringBuffer sb=new StringBuffer();
			for(int a=1; a<=longitud; a++) {
				int i=(int)((Math.random()*8)+1);				
				sb.append(x[i-1]);
			}
			return sb.toString();
		}
	
	/**
	 * Obtiene y la lista de docuemntos
	 * @param Lista de folio(s)
	 *
	 */
	public List getArchivosCargados(List arrFolios, String rutaDestino){
		StringBuffer strSQL = new StringBuffer();
		List conditions = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccesoDB con = new AccesoDB();
		List documentos = new ArrayList();
		List totaldocumentos = new ArrayList();
		String nombreArchivoTmp = null;
		
		try {
			if (arrFolios == null) {
				throw new Exception("Los parametros son requeridos");
			}
				
		} catch(Exception e) {
			throw new AppException("Error inesperado. ", e);
		}
		
		try {
			con.conexionDB();	
			strSQL.append(
					" SELECT /*+ INDEX (DOC CP_COM_CARGA_DOCTO_PYME_PK)*/"+
					" doc.ic_folio "+
					", doc.cg_nombre_docto"+
					", doc.bi_documento"+
					", doc.cg_extension"+
					" FROM com_carga_docto_pyme doc"+
					" WHERE cs_borrado = ? "
			);
			conditions.add("N");
			for(int i=0;i<arrFolios.size();i++) {
				List lItem = (ArrayList)arrFolios.get(i);
				if(i==0) {
					strSQL.append(" AND doc.ic_folio IN ( ");
				}
				strSQL.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(arrFolios.size()-1)) {
					strSQL.append(",");
				} else {
					strSQL.append(" ) ");
				}
			}//for(int i=0;i<arrFolios.size();i++)

			ps = con.queryPrecompilado(strSQL.toString(),conditions);
			rs = ps.executeQuery();
			String elFolio = "";
			String elNombre = "";
			String nombreArchivo = "";
			String ruta = "";
			while(rs.next()){
				InputStream inStream = rs.getBinaryStream("bi_documento");
				elFolio = rs.getString("ic_folio");
				elNombre = rs.getString("cg_nombre_docto");
				nombreArchivo = elFolio+"_"+elNombre;
				ruta = rutaDestino+nombreArchivo;
				
				CreaArchivo creaArchivo = new CreaArchivo();
				creaArchivo.setNombre(nombreArchivo);

				if (!creaArchivo.make(inStream, ruta)) {
					throw new AppException("Error al generar el archivo en " + nombreArchivo);
				}
				nombreArchivoTmp = creaArchivo.getNombre();
				inStream.close();
				
				documentos = new ArrayList();
				documentos.add(nombreArchivoTmp);
				totaldocumentos.add(documentos);
			}
			rs.close();
			ps.close();

		}catch(Exception e) {
			throw new AppException("Error al cargar los archivos. ", e);
		}
		return totaldocumentos;
	}

	/**
	 * Elimina los archivos cargados.
	 * @param el, o los folios a eliminar los archivos
	 * @return "ok" si el resultado es correcto.
	 */
	public String eliminaArchivosCargados(List arrFolios) {
		AccesoDB con = new AccesoDB();
		StringBuffer strQuery;
		List conditions = new ArrayList();
		PreparedStatement ps = null;
		boolean bcommit = true;
		try {
			con.conexionDB();	

			strQuery = new StringBuffer();
			strQuery.append(" UPDATE com_carga_docto_pyme SET "+
								 " bi_documento = NULL, "+
								 " cs_borrado = ? ");
			conditions.add("S");
			for(int i=0;i<arrFolios.size();i++) {
				List lItem = (ArrayList)arrFolios.get(i);
				if(i==0) {
					strQuery.append(" WHERE ic_folio IN ( ");
				}
				strQuery.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(arrFolios.size()-1)) {
					strQuery.append(",");
				} else {
					strQuery.append(" ) ");
				}
			}//for(int i=0;i<arrFolios.size();i++)

			ps = con.queryPrecompilado(strQuery.toString(),conditions);
			ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			bcommit = false;
			throw new AppException("Ocurrio un error al eliminar el archivo cargado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bcommit);
			  con.cierraConexionDB();
			}
		}
		return "ok";
	}

	/**
	 * Cambia el estatus archivos cargados.
	 * @param el, o los folios a cambiarles el estatus
	 * @return "ok" si el resultado es correcto.
	 */
	public String cambiarEstatus(List arrFolios, String icStatus, String icMotivo, String area) {
		AccesoDB con = new AccesoDB();
		StringBuffer strQuery;
		List conditions = new ArrayList();
		PreparedStatement ps = null;
		boolean bcommit = true;
		try {
			con.conexionDB();	

			strQuery = new StringBuffer();
			strQuery.append(" UPDATE com_carga_docto_pyme SET "+
								 " ic_estatus_carga_pyme = ?, "+
								 //" df_carga = sysdate ");
								 "ic_fecha_cambio_estatus =  sysdate ");
			conditions.add(new Integer(icStatus));
			if (icMotivo != null && !icMotivo.equals("")){
				strQuery.append(" ,ic_motivo = ? ");
				conditions.add(new Integer(icMotivo));
				if (area != null && !area.equals("")){
					strQuery.append(" ,cg_obs_motivo = ? ");
					conditions.add(area);
				}
			}

			for(int i=0;i<arrFolios.size();i++) {
				List lItem = (ArrayList)arrFolios.get(i);
				if(i==0) {
					strQuery.append(" WHERE ic_folio IN ( ");
				}
				strQuery.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(arrFolios.size()-1)) {
					strQuery.append(",");
				} else {
					strQuery.append(" ) ");
				}
			}//for(int i=0;i<arrFolios.size();i++)
			System.out.println("strQuery	" + strQuery);
			System.out.println("conditions	" + conditions);
			ps = con.queryPrecompilado(strQuery.toString(),conditions);
			ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			bcommit = false;
			throw new AppException("Ocurrio un error al eliminar el archivo cargado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bcommit);
			  con.cierraConexionDB();
			}
		}
		return "ok";
	}

	public String getMotivoRechazoDesc(String icMotivo) {
		AccesoDB con = new AccesoDB();
		StringBuffer strQuery;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List conditions = new ArrayList();
		String desc = "";
		try {
			con.conexionDB();	

			strQuery = new StringBuffer();
			strQuery.append(" SELECT /*+ INDEX (RE CP_COMCATRECHAZADOCTO_PK)*/ re.cg_descripcion FROM comcat_rechaza_docto re ");
			strQuery.append(" WHERE ic_motivo = ? ");
			conditions.add(new Integer(icMotivo));
			ps = con.queryPrecompilado(strQuery.toString(), conditions);
			rs = ps.executeQuery();
			if (rs.next()) {
				desc = rs.getString("cg_descripcion")==null?"":rs.getString("cg_descripcion");
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			throw new AppException("Ocurrio un error al obtener la descripcion de motivo de rechazo", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
		}
		return desc;
	}

	private String claveEpo;
	private String extension;
	private int tamanio;

}