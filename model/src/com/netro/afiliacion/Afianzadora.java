package com.netro.afiliacion;

import java.io.Serializable;

public class Afianzadora implements Serializable  {
	private String icAfianzadora;
	private String nafinElectronico;
	private String razonSocial;
	private String nombreComercial;
	private String rfc;
	private String numeroCnsf;
	private String urlValidacion;
	private String calleYNumero;
	private String colonia;
	private String codigoPostal;
	private String clavePais;
	private String claveEstado;
	private String claveMunicipio;
	private String numeroTelefono;
	private String correoElectronico;
	private String numeroFax;
	private String loginUsuario;

	//Constructor de la clase
	public Afianzadora() {}

	//setters
	public void setIcAfianzadora(String icAfianzadora) {this.icAfianzadora = icAfianzadora;}
	public void setNafinElectronico(String nafinElectronico) {this.nafinElectronico = nafinElectronico;}
	public void setRazonSocial(String razonSocial) {this.razonSocial = razonSocial;}
	public void setNombreComercial(String nombreComercial) {this.nombreComercial = nombreComercial;}
	public void setRfc(String rfc) {this.rfc = rfc;}
	public void setNumeroCnsf(String numeroCnsf) {this.numeroCnsf = numeroCnsf;}
	public void setUrlValidacion(String urlValidacion) {this.urlValidacion = urlValidacion;}
	public void setCalleYNumero(String calleYNumero) {this.calleYNumero = calleYNumero;}
	public void setColonia(String colonia) {this.colonia = colonia;}
	public void setCodigoPostal(String codigoPostal) {this.codigoPostal = codigoPostal;}
	public void setClavePais(String clavePais) {this.clavePais = clavePais;}
	public void setClaveEstado(String claveEstado) {this.claveEstado = claveEstado;}
	public void setClaveMunicipio(String claveMunicipio) {this.claveMunicipio = claveMunicipio;}
	public void setNumeroTelefono(String numeroTelefono) {this.numeroTelefono = numeroTelefono;}
	public void setCorreoElectronico(String correoElectronico) {this.correoElectronico = correoElectronico;}
	public void setNumeroFax(String numeroFax) {this.numeroFax = numeroFax;}
	public void setLoginUsuario(String loginUsuario) {this.loginUsuario = loginUsuario;}

	//getters
	public String getIcAfianzadora() {return this.icAfianzadora;}
	public String getNafinElectronico() {return this.nafinElectronico;}
	public String getRazonSocial() {return this.razonSocial;}
	public String getNombreComercial() {return this.nombreComercial;}
	public String getRfc() {return this.rfc;}
	public String getNumeroCnsf() {return this.numeroCnsf;}
	public String getUrlValidacion() {return this.urlValidacion;}
	public String getCalleYNumero() {return this.calleYNumero;}
	public String getColonia() {return this.colonia;}
	public String getCodigoPostal() {return this.codigoPostal;}
	public String getClavePais() {return this.clavePais;}
	public String getClaveEstado() {return this.claveEstado;}
	public String getClaveMunicipio() {return this.claveMunicipio;}
	public String getNumeroTelefono() {return this.numeroTelefono;}
	public String getCorreoElectronico() {return this.correoElectronico;}
	public String getNumeroFax() {return this.numeroFax;}
	public String getLoginUsuario() {return this.loginUsuario;}
}