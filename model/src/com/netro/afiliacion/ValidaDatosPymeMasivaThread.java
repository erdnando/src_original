package com.netro.afiliacion;

import java.io.Serializable;
import com.netro.exception.*;
import netropology.utilerias.*;
import java.sql.*;
import java.text.*;
import java.math.*;

/**
 * Clase que se encarga de invocar al metodo validaDatosPymeMasiva del
 * Bean de Afiliacion, actulizando el porcentaje de avance en el
 * atributo: percent
 *
 * @author  Jesus Salim Hernandez David
 * @version 1.0.14
 */

public class ValidaDatosPymeMasivaThread implements Runnable, Serializable {
	
   private int 				numberOfRegisters;
   private float				percent;
   private boolean 			started;
   private boolean 			running;
	private int 				processID;
	private int 				processedRegisters;
   private AfiliacionBean afiliacion;
	private boolean			error;
	private int 				accion;
	
	private String 			nombreArchivo; // Fodea 057 - 2010

   public ValidaDatosPymeMasivaThread() {
        numberOfRegisters 	= 0;
        percent 		   	= 0;
        started 			   = false;
        running 			   = false;
	     processID 	   	= 0;
		  processedRegisters = 0;
		  afiliacion			= new AfiliacionBean();
		  error					= false;
		  accion					= 0;
   }
	 
   protected void validateRegisters() {
		
	   try {
			
			if(accion==1 || accion==3){
				afiliacion.validaDatosPymeMasiva(processID,processedRegisters+1);				
			}
			if(accion==2 || accion==3){
				afiliacion.validaDatosPymeMasivaEco(processID,processedRegisters+1);
			}
				
			processedRegisters++;
			percent = (processedRegisters/(float)numberOfRegisters)*100;
		} catch(Exception e){
			setRunning(false);
			processedRegisters = numberOfRegisters ;
			setError(true);
			e.printStackTrace();
			System.out.println("Error en ValidaDatosPymeMasivaThread::validateRegisters al extraer numero de registro de la base de datos");
			// terminar el proceso si falla la funcion de validacion para algun registro
		}
		
   }

   public synchronized float getPercent() {
       return percent;
   }
	
	public synchronized int getNumberOfRegisters() {
       return numberOfRegisters;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		return((processedRegisters == 0 && numberOfRegisters == 0)?false:(processedRegisters == numberOfRegisters));
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
	public synchronized void setProcessID(int processID) {
       this.processID = processID;
   }
	
	public synchronized void setError(boolean error) {
       this.error = error;
   }
	
	public synchronized void setAccion(int accion) {
       this.accion = accion;
   }
	
	public synchronized boolean hasError() {
       return error;
   }
	
	public synchronized int getProcessID() {
       return processID;
   }

	public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }
	
	public synchronized int getAccion() {
       return accion;
   }
	 

   public void run() {
		 
		// Obtener numero de registros
		AccesoDB 	con 	=	new AccesoDB();
		boolean		lbOK	=  true;
		try {
			con.conexionDB();
			String 		query = 	"SELECT MAX(IC_NUMERO_LINEA) "+
		 								"FROM COMTMP_AFILIA_PYME_MASIVA "+
										"WHERE IC_NUMERO_PROCESO = " + processID;
			if(accion==2){
				query = 	"SELECT MAX(IC_NUMERO_LINEA) "+
		 					"FROM ECON_AFILIA_PYME_MASIVA "+
							"WHERE IC_NUMERO_PROCESO = " + processID;
			}
			
			ResultSet	rs		= con.queryDB(query);
			if(rs.next())	
				numberOfRegisters = rs.getInt(1);
				rs.close();
				con.cierraStatement();
		}catch (Exception e){
			lbOK = false;
			e.printStackTrace();
			System.out.println("Error en ValidaDatosPymeMasivaThread::run() al extraer de la base de datos el numero de registros a procesar");
			setRunning(false);
		}finally{
			con.terminaTransaccion(lbOK);
			con.cierraConexionDB();
		}
		
		// Verificar que no se hayan devuelto datos vacios
		if ( numberOfRegisters == 0){
			numberOfRegisters  	= -1;
			processedRegisters 	= -1;
			lbOK 						= false;
		}
		
		// Realizar la Validacion de los datos
      try {
			
			if(lbOK == false) 
				setRunning(false);
			else
				setRunning(true);
			
				while (isRunning() && !isCompleted()){
					validateRegisters();
					// Debug info, 
					System.out.println("Enterin Validate Registers");
					System.out.println("numberOfRegisters:	" + numberOfRegisters);
					System.out.println("percent:    			" + percent);
					System.out.println("started:       		" + started);
					System.out.println("running:       		" + running);
					System.out.println("processID: 			" + processID);
					System.out.println("processedRegisters:" + processedRegisters );
					// Debug info
				}
		
      } finally {
			setRunning(false);
      }

    }

	 public String getNombreArchivo(){ // Fodea 057 - 2010
		return nombreArchivo;
	 }
 
	 public void setNombreArchivo(String nombreArchivo){ // Fodea 057 - 2010
		this.nombreArchivo = nombreArchivo;
	 }

}