package com.netro.afiliacion;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class FIDEEnlaceCE implements IFEnlaceCE,Serializable {


	public String getTablaAfiliados()	{	return "comtmp_carga_clientes_fide";	}
	public String getTablaAcuse()		{	return "com_afilia_acuse_fide";	}
	public String getTablaResultados()		{	return "com_afilia_acuse_resul_fide";	}

	public String getAfiliados(){

		return  " select CG_RFC, " +
				" CG_NUM_SOLICITUD, " +
				" to_char(DF_FECHA,'dd/mm/yyyy') as DF_FECHA, " +
				" TRIM(CG_NOMBRE) as CG_NOMBRE, " +
				" TRIM(CG_APPAT) as CG_APPAT, " +
				" TRIM(CG_APMAT) as CG_APMAT, " +
				" TRIM(CG_SEXO) as CG_SEXO, " +
				" TRIM(CG_RAZON_SOCIAL) as CG_RAZON_SOCIAL, " +
				" IN_NUMERO_EMP, " +
				" FN_VENTAS_NETAS_TOT, " +
				" IC_CLASE, " +
				" IC_RAMA, " +
				" IC_SUBSECTOR, " +
				" IC_SECTOR_ECON, " +
				" IC_MUNICIPIO, " +
				" IC_ESTADO, " +
				" CG_CALLE, " +
				" CG_COLONIA, " +
				" CN_CP, " +
				" CS_TROYA, " +
				" TRIM(CG_EMAIL) as CG_EMAIL, " +
				" TRIM(CG_TELEFONO) as CG_TELEFONO, "+
				" TRIM(CG_CELULAR) as CG_CELULAR, "+
				" TRIM(CG_CONTACTO_NOMBRE) as CG_CONTACTO_NOMBRE, "+
				" TRIM(CG_CONTACTO_APPAT) as CG_CONTACTO_APPAT, "+
				" TRIM(CG_CONTACTO_APMAT) as CG_CONTACTO_APMAT, "+
				" TRIM(CG_CONTACTO_TELEFONO) as CG_CONTACTO_TELEFONO, "+
				//SE AGREGAN CAMPOS PLD - FVR
				" CG_CURP, "+
				" CN_FIEL, "+
				" IC_PAIS_ORIGEN, "+
				" IC_PAIS, "+
				//" IC_ESTADO, "+  ya existia
				" IC_CIUDAD, "+ 
				" IC_FOLIO_FIDE "+ // CAMPO NECESARIO PARA EL FIDE, YA QUE LO UTILIZA PARA SABER QUE REGISTRO ENVIO
				" from " + getTablaAfiliados();
	}

	public String getUpdateAcuse(AcuseEnl acu){
		return	" update "+getTablaAcuse()+" set IN_REG_PROC="+acu.getRegProc()+
				" ,IN_REG_OK="+acu.getRegOk()+
				" ,IN_REG_RECH="+acu.getRegRech()+
				" WHERE CC_ACUSE='"+acu.getCcAcuse()+"' ";
	}

	public String getInsertAcuse(AcuseEnl acu){
		return	"insert into "+getTablaAcuse()+" (CC_ACUSE, IN_REG_PROC, IN_REG_OK, IN_REG_RECH) "+
				"values('"+acu.getCcAcuse()+"',"+acu.getRegProc()+","+acu.getRegOk()+","+acu.getRegRech()+")";
	}

	public String getInsertaResultados(){
/*
		return	"insert into "+getTablaResultados()+" (CC_ACUSE, CG_RFC, CG_NUM_SOLICITUD, IN_NUMERO_SIRAC, CC_RECHAZO, CG_DESC_RECHAZO) "+
				" values('"+res.getCcAcuse()+"','" +
					res.getCgRfc()+"','" +
					res.getCgNumSolicitud()+"', " +
					res.getInNumeroSirac()+", " +
					res.getCcRechazo()+", '" +
					res.getCgDescRechazo()+"')";
*/
		return	"insert into "+getTablaResultados()+" (CC_ACUSE, CG_RFC, CG_NUM_SOLICITUD, IN_NUMERO_SIRAC, CC_RECHAZO, CG_DESC_RECHAZO, IC_FOLIO_FIDE) "+
				" values(?, ?, ?, ?, ?, ?, ?)";
	}
	public PreparedStatement setInsertaResultados(ResultadosEnl res, PreparedStatement ps)
		throws SQLException
	{
		if (ps != null) {
			ps.setString(1, res.getCcAcuse());
			ps.setString(2, res.getCgRfc());
			ps.setString(3, res.getCgNumSolicitud());
			ps.setString(4, res.getInNumeroSirac());
			ps.setString(5, res.getCcRechazo());
			ps.setString(6, res.getCgDescRechazo());
			ps.setString(7, res.getIcFolio());
		}
		return ps;
	}

	public String getDeleteResultados(AcuseEnl acu){
		return	" DELETE "+getTablaResultados() +
				" WHERE cc_acuse != '"+acu.getCcAcuse()+"'";
	}

	public String getDeleteAfiliadoTMP(AfiliadoCE afiliadoCE){
/*
		return	" DELETE "+getTablaAfiliados() +
				" WHERE cg_rfc = '"+ afiliadoCE.getCgRfc()+"'"+
				" AND cg_num_solicitud = '" + afiliadoCE.getCgNumSolicitud()+"'";
*/
		return	" DELETE "+getTablaAfiliados() +
				" WHERE cg_rfc = ? "+
				" AND cg_num_solicitud = ?";
	}

	public PreparedStatement setDeleteAfiliadoTMP(AfiliadoCE afiliadoCE, PreparedStatement ps)
		throws SQLException
	{
		if (ps != null) {
			ps.setString(1, afiliadoCE.getCgRfc());
			ps.setString(2, afiliadoCE.getCgNumSolicitud());
		}
		return	ps;
	}

	/*public String getCondicionQuery(){
		return " ";
	}*/

}
