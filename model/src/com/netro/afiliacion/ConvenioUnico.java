package com.netro.afiliacion;

import com.netro.exception.NafinException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConvenioUnico implements Serializable {
	private StringBuffer sDescEjecicion 			= new StringBuffer("");

	/**  
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConvenioUnico.class);



	/** Clase que contiene los m�todos necesarios para la implementaci�n de los procesos de Convenio Unico
	*	en nafin electronico
  * @author Edgar Gonz�lez
	*/


  /** Metodo encargado de realizar las reafiliaciones por Convenio Unico para los nuevos IF's que
   * se integran a una cadena.
   *
   */
  public void procesaConvenioUnicoXIntermediario(String ruta) throws NafinException{
      
    //Las librerias de conexi�n a base de datos que utilizaban el pool de
    //conexiones del Application Server son remplazadas por conexiones
    //directas jdbc de Oracle.
    AccesoDBOracle con = new AccesoDBOracle();
    String query = "";
    List lVarBind = new ArrayList();
    Registros	registros	= null;
    Registros	registros2	= null;
    Registros	registros3	= null;
    Registros	registros4	= null;
    String sIntermediario = "";
    boolean intermediarioProc = false;
    PreparedStatement ps = null;
	 PreparedStatement ps2 = null;//SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
	 ResultSet rs = null; //SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
	 //List lstDatosGral = new ArrayList();
	 
	 //SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
	 BufferedWriter bw = null; 


    log.info("ConvenioUnico.procesaConvenioUnicoXIntermediario(E)");
		try {
      con.conexionDB();
		

		//SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY 
		bw = new BufferedWriter(new FileWriter(ruta,true));
		
		this.sDescEjecicion.delete(0, sDescEjecicion.length());
      this.sDescEjecicion.append("ConvenioUnico.procesaConvenioUnicoXIntermediario(E)\n");
		bw.write("ConvenioUnico.procesaConvenioUnicoXIntermediario(E)\n");

		//Verifico que EPO tiene Parametizado convenio Unico
		/*
		String queryCU ="select CS_CONVENIO_UNICO, IC_EPO    from comrel_producto_epo  "+
							 " where IC_PRODUCTO_NAFIN = 1  " +
							 " AND CS_CONVENIO_UNICO  IS NOT NULL ";
		*/
		
		//FODEA 017-2013 INI
		String queryCU = "SELECT DISTINCT cp.cs_convenio_unico CS_CONVENIO_UNICO, cp.ic_epo IC_EPO, " +
							"                cpe.cg_valor CG_VALOR " +
							"           FROM comrel_producto_epo cp, com_parametrizacion_epo cpe " +
							"          WHERE cp.ic_epo = cpe.ic_epo(+) " +
							"            AND cp.ic_producto_nafin = 1 " + 
							"            AND cp.cs_convenio_unico IS NOT NULL " +
							"            AND cpe.cc_parametro_epo(+) = 'CS_OPERA_FIDEICOMISO'  ";
		//FODEA 017-2013 FIN 

							 
		PreparedStatement psC = con.queryPrecompilado(queryCU);
		ResultSet rsC = psC.executeQuery();				 
		if (rsC != null) {
			while (rsC.next()){
				String qryFideicomiso = "";
				String sEpoC = rsC.getString("IC_EPO");
				String sConvenio = rsC.getString("CS_CONVENIO_UNICO");
				//FODEA 017-2013 INI
				String sOperFid = rsC.getString("CG_VALOR")==null?"":rsC.getString("CG_VALOR");
				//FODEA 017-2013 FIN
				String  cs_aceptacion ="";
				String qryF10= "";
            if(sConvenio.equals("AF"))  {   
					qryF10 = " AND pe.cs_aceptacion =  'H' " ;
            }
            if(sConvenio.equals("PA"))  {   
					qryF10 = " AND pe.cs_aceptacion <> 'H' " ;
            }			
				
				
				if(sOperFid.equals("S")){
					qryFideicomiso = " AND i.cs_opera_fideicomiso = ? ";
				}
				
				query =  " SELECT e.ic_epo, " +
						  " i.ic_if, "+
						  " i.cg_razon_social as Intermediario, " +
						  " e.cg_razon_social as EPO, 'ConvenioUnico.procesaConvenioUnicoXIntermediario()' as proceso "   +
						  " FROM comcat_if i, comrel_if_epo ie, comcat_epo e" +
									" WHERE i.ic_if = ie.ic_if "+
									" AND ie.ic_epo = e.ic_epo "+
						  //" AND ie.cg_reaf_CU = ? " + //SE BUSCAN LOS REGISTROS DE IF QUE AUN NO HAN SIDO PROCESADOS AL CONVENIO UNICO (EXISTENTES Y NUEVOS)
						  " AND (ie.cg_reaf_CU = ? OR ie.cg_reaf_CU = ? ) "+
						  " AND i.cs_convenio_unico = ? " +
						  " AND ie.cs_aceptacion = ? " +
						  " AND e.cs_convenio_unico = ? " +  // FODEA 040-2009 CONVENIO UNICO FASE 2
						  " AND ie.cs_convenio_unico = ? " + // FODEA 040-2009 CONVENIO UNICO FASE 2
						  " AND ie.cs_vobo_nafin = ? " + //FODEA 040-2010
						  " AND ie.cs_bloqueo = ? " + //FODEA 002-2012
						  " AND ie.ic_epo = ?  "+
						  qryFideicomiso + //FODEA 017-2013						
						  //" AND ie.ic_if = 1 "+
							" ORDER BY ic_if";
				log.debug(" query "+query );
				log.debug(" N,  A ,  S,  S,   S , S,  S,  N, "+sEpoC);
				
				
				//SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
				ps = con.queryPrecompilado(query);
				ps.setString(1,"N");
				ps.setString(2,"A");
				ps.setString(3,"S");
				ps.setString(4,"S"); // cs_aceptacion 
				ps.setString(5,"S");
				ps.setString(6,"S");
				ps.setString(7,"S");
				ps.setString(8,"N"); //FODEA 002-2012
				ps.setString(9,sEpoC); 
				if(sOperFid.equals("S")){
					ps.setString(10,sOperFid); 
				}
				/*
				lVarBind.add("N"); //N = nuevos y no procesados, los registros antiguos se les asigno el valor A
				lVarBind.add("A");
				lVarBind.add("S");
				lVarBind.add("S");
				lVarBind.add("S");
				lVarBind.add("S");
				lVarBind.add("S");
				*/
				//registros = con.consultarDB(query, lVarBind, false);
				rs = ps.executeQuery();
				lVarBind.removeAll(lVarBind);
				//if (registros.getNumeroRegistros()!= 0) {
				if (rs!= null) {
				  while (rs.next()){
					 intermediarioProc = false;
					 String sEpo = rs.getString("ic_epo");
					 String sEpoDesc = rs.getString("EPO");
					 String sIF = rs.getString("ic_if");
					 sIntermediario = rs.getString("Intermediario");
					 /*
					 String sEpo = registros.getString("ic_epo");
					 String sEpoDesc = registros.getString("EPO");
					 String sIF = registros.getString("ic_if");
					 sIntermediario = registros.getString("Intermediario");
					 */
					 //this.sDescEjecicion.append("PROCESANDO NUEVA RELACION IF("+sIF+"): "  + sIntermediario + " EPO("+sEpo+"): " + sEpoDesc + "\n");
					 bw.write("PROCESANDO NUEVA RELACION IF("+sIF+"): "  + sIntermediario + " EPO("+sEpo+"): " + sEpoDesc + "\n");
		
					 // Este query determina las monedas que opera la EPO para que se parametricen al CU las pymes
					 registros2 = getMonedasEPO(sEpo, sIF, "procesaConvenioUnicoXIntermediario()", con);
					 if (registros2.getNumeroRegistros()!= 0) {
						while (registros2.next()){
						  String sMoneda = registros2.getString("ic_moneda");
		
						  //this.sDescEjecicion.append("PROCESANDO MONEDA: "  + sMoneda + "\n");
						  bw.write("PROCESANDO MONEDA: "  + sMoneda + "\n");
						  
						  if(sOperFid.equals("S")){
								qryFideicomiso = " AND p.cs_opera_fideicomiso = ? ";
								// PARA SOLO OBTENER LAS PYMES QUE OPERAN FIDEICOMISO 
							}
		 
						  // query para obtener las pymes relacionadas con la epo que tienen activado el convenio �nico
						  query = "SELECT pe.ic_pyme, p.cg_razon_social as pyme, 'ConvenioUnico.procesaConvenioUnicoXIntermediario()' as proceso " +
									 " FROM comrel_pyme_epo pe, comcat_pyme p " +
									 " WHERE pe.ic_epo = ? " +
									 " AND pe.ic_pyme = p.ic_pyme " +
									 " AND p.cs_invalido = ? " +
									 " AND p.cs_convenio_unico = ? " +									 
									 qryFideicomiso + //FODEA 017-2013									 
									   qryF10 +//FODEA 010-2013
									" and p.CS_ENTIDAD_GOBIERNO = ?  "+ //F034-2014
									 " ORDER BY pe.ic_pyme";
						  lVarBind.add(sEpo);
						  lVarBind.add("N");
						  lVarBind.add("S");
						  if(sOperFid.equals("S")){
								lVarBind.add("S");
							}
							lVarBind.add("N"); // SOLO LOS PROVEEDORES NO MARCADOS COMO ENTIDAD DE GOBIERNO -- FODEA 034-2014
						  log.debug(" query "+query );
						  log.debug(" lVarBind "+lVarBind );
						  
						  registros3 = con.consultarDB(query, lVarBind, false);
						  lVarBind.removeAll(lVarBind);
						  if (registros3.getNumeroRegistros()!= 0) {
							 while (registros3.next()){
								String sPyme = registros3.getString("ic_pyme");
								String sDescPyme = registros3.getString("pyme");
		
								//Fodea 019-2014(E)
								String operaFide_Pyme =   getOperaFideicomisoPYME( sPyme, con);   
								boolean OperaIFEPOFISO  =  getOperaIFEPOFISO(sIF,sEpo, con);
								String  ic_nafin_electronico = obtengoNafin(sPyme);
								String vClabePYME =  getValidaCcuentaClablePyme(ic_nafin_electronico, con ) ; 
								String mensajeCtaClabe ="";
								boolean CtaClabe = false;	
								String  validaEpoRea= 	getValidaPymeEpoRea(sPyme, sEpo, con ); // esta validacion es apra una reafiliaci�n de una pyme que opera fideicomiso en otra epo  que opera fideicomiso 
								
								log.debug("operaFide_Pyme :::::  "+operaFide_Pyme);
								log.debug("OperaIFEPOFISO :::::  "+OperaIFEPOFISO);
								log.debug("vClabePYME :::::  "+vClabePYME);	
								log.debug("validaEpoRea :::::  "+validaEpoRea);	
									
								if( "S".equals(validaEpoRea) ) {								
																		 											
									if("S".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {	 
										mensajeCtaClabe =  getcuentaClablePyme(ic_nafin_electronico, "CU", sEpo, con );  	  
										if(!mensajeCtaClabe.equals("")) {   
											CtaClabe = true;	  
										}									
									}else  if("N".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {									
										mensajeCtaClabe="La PYME no tiene parametrizada una Cuenta CLABE, favor de verificar"; 									
										CtaClabe = true;	
										
									}else  if("E".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {	
										mensajeCtaClabe="La Cuenta CLABE de la PYME no tiene estatus de Cuenta Correcta, favor de validarla"; 									
										CtaClabe = true;	
									} 			
									log.debug(" CtaClabe   " +CtaClabe +"   mensajeCtaClabe  :::::: "+mensajeCtaClabe); 
								//Fodea 019-2014(S)							
								}
									 
							if(!CtaClabe )  {
								//this.sDescEjecicion.append("PROCESANDO PYME("+sPyme+"): "  + sDescPyme + "\n");
								bw.write("PROCESANDO PYME("+sPyme+"): "  + sDescPyme + "\n");
		
								// Verifico por cada pyme si el IF ya autoriz� el convenio �nico a la pyme
								// si es falso �nicamente se parametrizar� la relaci�n, si es verdadero se parametriza y autoriza
								boolean convUnicoAut = false;
									
								if(!OperaIFEPOFISO ){
									convUnicoAut = verificaAutorizacionCU(sIF, sPyme, sMoneda, con);
								}else {
									convUnicoAut =  verificaAutorizacionFideicomiso( sPyme,  sMoneda, con); //Fodea 019-2014
								}
																
								//Se obtiene la cuenta bancaria por moneda a utilizar
								String cuentaBancaria = getCuentaBancariaCU(sPyme, sMoneda, con);
								// Si no se encuentra la cuenta se envia al LOG y se continua con el siguiente registro
								if (!"".equals(cuentaBancaria)){
								  //Verificamos si existe relacion previa
								  boolean existeCuenta = verificaRelacionPymeIF(sIF, sPyme, sEpo, sMoneda, con);
								  if (!existeCuenta) {
									 //INI - MODIFICACION FODEA 036-2009 FVR
									 /*
									 List lstRegDatosGral = new ArrayList();
									 lstRegDatosGral.add(sPyme);
									 lstRegDatosGral.add(sEpo);
									 lstRegDatosGral.add(sIF);
									 lstRegDatosGral.add(cuentaBancaria);
									 lstDatosGral.add(lstRegDatosGral);
									 */
									 //FIN - MODIFICACION FODEA 036-2009 FVR
									log.debug(" cuentaBancaria "+cuentaBancaria );
									
									 //Realiza la parametrizacion de la cuenta de acuerdo a los parametros previos
									 parametrizaCU(sIF, sPyme, sEpo, sMoneda, convUnicoAut, cuentaBancaria, "IF", con);
									 log.debug(" convUnicoAut "+convUnicoAut );
						  
									// Fodea 010-2013   Reafiliacion Automatica, Parametrizo el descuento Automatico									
									if(convUnicoAut) {
										paramCUDescAuto(sIF,  sIntermediario, sPyme, sDescPyme, sEpo , sEpoDesc,  sMoneda,  "DESAUTOMA" ,  "Proceso de Afiliaci�n Autom�tica de IFs con CU(procesaConvenioUnicoXIntermediario)", "DESAUTOMA", con);
									}
									 
									 /*
									  this.sDescEjecicion.append("PYME PARAMETRIZADA:\n");
									 this.sDescEjecicion.append("  IF ("+sIF+"):" + sIntermediario + "\n");
									 this.sDescEjecicion.append("  EPO ("+sEpo+"):" + sEpoDesc + "\n");
									 this.sDescEjecicion.append("  PYME("+sPyme+") : " + sDescPyme + "\n");
									 this.sDescEjecicion.append("  MONEDA : "+sMoneda+"\n");
									 this.sDescEjecicion.append("  CuentaBancaria : "+cuentaBancaria+"\n");
									 */
									 bw.write("PYME PARAMETRIZADA:\n");
									 bw.write("  IF ("+sIF+"):" + sIntermediario + "\n");
									 bw.write("  EPO ("+sEpo+"):" + sEpoDesc + "\n");
									 bw.write("  PYME("+sPyme+") : " + sDescPyme + "\n");
									 bw.write("  MONEDA : "+sMoneda+"\n");
									 bw.write("  CuentaBancaria : "+cuentaBancaria+"\n");
									 bw.write("  Mensaje : "+mensajeCtaClabe+"\n");
									 
									 
									 intermediarioProc = true;
								  } else{
		
									 /*
									 this.sDescEjecicion.append("LA PYME YA TENIA RELACION PARAMETRIZADA CON EL IF EN LA MONEDA\n");
									 this.sDescEjecicion.append("NO SE ACTUALIZA EL REGISTRO YA QUE SE PARAMETRIZO POR PANTALLAS\n");
									 this.sDescEjecicion.append("SE CONTINUA CON EL SIGUIENTE REGISTRO\n");
									 */
									 bw.write("LA PYME YA TENIA RELACION PARAMETRIZADA CON EL IF EN LA MONEDA\n");
									 bw.write("NO SE ACTUALIZA EL REGISTRO YA QUE SE PARAMETRIZO POR PANTALLAS\n");
									 bw.write("SE CONTINUA CON EL SIGUIENTE REGISTRO\n");
		
								  }
								} else{
		
									 /*
									 this.sDescEjecicion.append("ERROR EN LA PARAMETRIZACION, NO SE ENCONTRO LA CUENTA BANCARIA DE CONVENIO UNICO: \n");
									 this.sDescEjecicion.append("  PYME("+sPyme+") : " + sDescPyme + "\n");
									 this.sDescEjecicion.append("  MONEDA : "+sMoneda+"\n");
									 */
									 bw.write("ERROR EN LA PARAMETRIZACION, NO SE ENCONTRO LA CUENTA BANCARIA DE CONVENIO UNICO: \n");
									 bw.write("  PYME("+sPyme+") : " + sDescPyme + "\n");
									 bw.write("  MONEDA : "+sMoneda+"\n");
		
								}
								
								} // if(!CtaClabe )  { F019-2014
		
							 }
						  } else {
		
							 //this.sDescEjecicion.append("NO SE ENCONTRARON PYMES CON CONVENIO UNICO AUTORIZADO EN LA EPO\n");
							 bw.write("NO SE ENCONTRARON PYMES CON CONVENIO UNICO AUTORIZADO EN LA EPO\n");
		
						  }
						}
					 } else {
		
						//this.sDescEjecicion.append("NO SE ENCONTRARON TASAS AUTORIZADAS EN LA EPO\n");
						bw.write("NO SE ENCONTRARON TASAS AUTORIZADAS EN LA EPO\n");
		
					 }
		
					 // SI AL MENOS UN REGISTRO SE PARAMETRIZ�, SE ACTUALIZA LA RELACION EPO - IF A "P" (PROCESADO)
					 if (intermediarioProc){
		
						query = "UPDATE comrel_if_epo " +
							 " SET cg_reaf_CU = ? " +
							 " WHERE ic_epo = ? " +
							 " AND ic_if = ? ";
						log.debug(" query "+query );
						log.debug(" P, "+sEpo+",  "+sIF );
						
						ps2 = con.queryPrecompilado(query);
						ps2.clearParameters();
						ps2.setString(1, "P"); //PROCESADO
						ps2.setInt(2, Integer.parseInt(sEpo));
						ps2.setInt(3, Integer.parseInt(sIF));
						ps2.executeUpdate();
						ps2.close();
						con.terminaTransaccion(true);
		
					 }
		
				  }
				} else {
		
				  //this.sDescEjecicion.append("NO SE ENCONTRARON NUEVOS INTERMEDIARIOS PARA PARAMETRIZAR EL CONVENIO UNICO\n");
				  bw.write("NO SE ENCONTRARON NUEVOS INTERMEDIARIOS PARA PARAMETRIZAR EL CONVENIO UNICO\n");
		
				}
				//INI - MODIFICACION FODEA 036-2009 FVR
				/*
				Context context_a = ContextoJNDI.getInitialContext();
				ParametrosDescuentoHome parametrosDescuentoHome = (ParametrosDescuentoHome) context_a.lookup("ParametrosDescuentoEJB");
				ParametrosDescuento beanParametrosDescuento = parametrosDescuentoHome.create();
				beanParametrosDescuento.setParamEpoPymeIf(lstDatosGral,"");
				*/
				//FIN - MODIFICACION FODEA 036-2009 FVR
					if(rs!=null)rs.close();
					if(ps!=null)ps.close();
					
				}/// while (rsC.next())			
			}
			if(rsC!=null)rsC.close();
			if(psC!=null)psC.close();
		
		
		} catch (NafinException ne) {
      //log.info(this.sDescEjecicion.toString());
			throw ne;
		} catch (Exception e) {
			log.info("ConvenioUnico.procesaConvenioUnicoXIntermediario(Exception)");
			e.printStackTrace();
			this.sDescEjecicion.append("Error inesperado procesaConvenioUnicoXIntermediario " + e.toString());
			try {
				bw.write("Error inesperado procesaConvenioUnicoXIntermediario " + e.toString());
			} catch (IOException ioe) {}
      //log.info(this.sDescEjecicion.toString());
			throw new NafinException("AFIL0045");
		} finally {
			if (con.hayConexionAbierta()){
			  con.cierraConexionDB();
			}
			if (bw != null) try {
					bw.flush();
					bw.close();
			} catch (IOException ioe2) {}
			log.info("ConvenioUnico.procesaConvenioUnicoXIntermediario(S)");
      this.sDescEjecicion.append("ConvenioUnico.procesaConvenioUnicoXIntermediario(S)");
		}

	}

	public void procesaConvenioUnicoXProveedor(String ruta) throws NafinException{
        
    //Las librerias de conexi�n a base de datos que utilizaban el pool de
    //conexiones del Application Server son remplazadas por conexiones
    //directas jdbc de Oracle
    AccesoDBOracle con = new AccesoDBOracle();
    String query = "";
    List lVarBind = new ArrayList();
    Registros	registros	= null;
    Registros	registros2	= null;
    Registros	registros3	= null;
    Registros	registros4	= null;
    PreparedStatement ps = null;
	 PreparedStatement ps2 = null;//SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
	 ResultSet rs = null; //SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
	 //List lstDatosGral = new ArrayList();
	 
	 //SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
	 BufferedWriter bw = null;


    log.info("ConvenioUnico.procesaConvenioUnicoXProveedor(E)");
		try {
      con.conexionDB();

		//SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
		bw = new BufferedWriter(new FileWriter(ruta,true));
		
		this.sDescEjecicion.delete(0, sDescEjecicion.length());
      this.sDescEjecicion.append("ConvenioUnico.procesaConvenioUnicoXProveedor(E)");
		bw.write("ConvenioUnico.procesaConvenioUnicoXProveedor(E)");

		//Verifico que EPO tiene Parametizado convenio Unico
		/*
		String queryCU ="select CS_CONVENIO_UNICO, IC_EPO    from comrel_producto_epo  "+
							 " where IC_PRODUCTO_NAFIN = 1  "+
							 "  AND CS_CONVENIO_UNICO  IS NOT NULL ";
		*/
		
		//FODEA 017-2013 INI
		String queryCU  = "SELECT DISTINCT cp.cs_convenio_unico cs_convenio_unico, cp.ic_epo ic_epo, " +
					"                cpe.cg_valor cg_valor " +
					"           FROM comrel_producto_epo cp, com_parametrizacion_epo cpe " +
					"          WHERE cp.ic_epo = cpe.ic_epo(+) " +
					"            AND cp.ic_producto_nafin = 1 " +
					"            AND cp.cs_convenio_unico IS NOT NULL " +
					"            AND cpe.cc_parametro_epo(+) = 'CS_OPERA_FIDEICOMISO'   "; 
		//FODEA 017-2013 FIN
							 
		PreparedStatement psC = con.queryPrecompilado(queryCU);
		ResultSet rsC = psC.executeQuery();				 
		if (rsC != null) {
			while (rsC.next()){
				String qryFideicomiso = "";
				String sEpoC = rsC.getString("IC_EPO");
				String sConvenio = rsC.getString("CS_CONVENIO_UNICO");
				String sOperFid = rsC.getString("CG_VALOR")==null?"":rsC.getString("CG_VALOR");//FODEA 017-2013
				String  cs_aceptacion ="";
				String qryF10= "";
				if(sConvenio.equals("AF"))  {   
					qryF10 = " AND pe.cs_aceptacion =  'H' " ;
				}
				if(sConvenio.equals("PA"))  {   
					qryF10 = " AND pe.cs_aceptacion <> 'H' " ;
				}
				//if(sConvenio.equals("A"))  {    cs_aceptacion ="S";  }
				if(sOperFid.equals("S")){
					qryFideicomiso = " AND i.cs_opera_fideicomiso = ? " +
										  " AND p.cs_opera_fideicomiso = ? ";
				}
			
				query =  " SELECT e.ic_epo, " +
						  " ie.ic_if, "+
						  " pe.ic_pyme, "+
						  " i.cg_razon_social as Intermediario, " +
						  " e.cg_razon_social as EPO, "+
						  " p.cg_razon_social as PYME, "+
						  " 'ConvenioUnico.procesaConvenioUnicoXProveedor()' as proceso "   +
						  " FROM comrel_pyme_epo pe, " +
						  " comrel_if_epo ie, "+
						  " comcat_if i, " +
						  " comcat_pyme p, " +
						  " comcat_epo e" +
									" WHERE pe.ic_epo = ie.ic_epo " +
						  " AND pe.ic_pyme = p.ic_pyme "+
						  " AND i.ic_if = ie.ic_if "+
						  " AND e.ic_epo = ie.ic_epo "+
						  //" AND pe.cg_reaf_CU = ? " + //SE BUSCAN LOS REGISTROS DE IF QUE AUN NO HAN SIDO PROCESADOS AL CONVENIO UNICO (EXISTENTES Y NUEVOS)
						  " AND (pe.cg_reaf_CU = ? OR pe.cg_reaf_CU = ? ) "+
						  " AND p.cs_convenio_unico = ? " +
						  " AND i.cs_convenio_unico = ? " +
									" AND ie.cs_aceptacion = ? " +
						  " AND e.cs_convenio_unico = ? " +  // FODEA 040-2009 CONVENIO UNICO FASE 2
						  " AND ie.cs_convenio_unico = ? " + // FODEA 040-2009 CONVENIO UNICO FASE 2
						  " AND ie.cs_bloqueo = ? " + // FODEA 002-2012
						  " AND pe.ic_epo = ? "+
						   " and p.CS_ENTIDAD_GOBIERNO = ? "+//F034-2014
						  qryFideicomiso +//FODEA 017-2013
						  qryF10 + //Fodea 010-2013
						
						  
						  //" AND pe.ic_pyme = 805360 "+
						  //" AND pe.ic_epo = 658 "+
						  //" AND ie.ic_if = 1 "+
						  " ORDER BY ic_if";   
				//SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
				log.debug(" query "+query );
				log.debug(" N,  A,  S,  S,  S,  S,  S,  N, "+sEpoC);
				
				ps = con.queryPrecompilado(query);
				ps.setString(1,"N");
				ps.setString(2,"A");
				ps.setString(3,"S");
				ps.setString(4,"S");
				ps.setString(5,"S"); //cs_aceptacion
				ps.setString(6,"S");
				ps.setString(7,"S");
				ps.setString(8,"N");// FODEA 002-2012
				ps.setString(9,sEpoC); //ic_epo
				ps.setString(10,"N");// SOLO LOS PROVEEDORES NO MARCADOS COMO ENTIDAD DE GOBIERNO -- FODEA 034-2014
				if(sOperFid.equals("S")){
					ps.setString(11,"S");
					ps.setString(12,"S");
				}
				
				/*
				lVarBind.add("N"); //N = nuevos y no procesados, los registros antiguos se les asigno el valor A
				lVarBind.add("A");
				lVarBind.add("S");
				lVarBind.add("S");
				lVarBind.add("S");
				lVarBind.add("S");
				lVarBind.add("S");
				*/
				//registros = con.consultarDB(query, lVarBind, false);
				rs = ps.executeQuery();
				lVarBind.removeAll(lVarBind);
				//if (registros.getNumeroRegistros()!= 0) {
				if (rs != null) {
				  while (rs.next()){
					 //SE AGREGA POR ERROR DE PRODUCCION OUTOFMEMORY
					 String sEpo = rs.getString("ic_epo");
					 String sEpoDesc = rs.getString("EPO");
					 String sIF = rs.getString("ic_if");
					 String sIntermediario = rs.getString("Intermediario");
					 String sPyme = rs.getString("ic_pyme");
					 String sDescPyme = rs.getString("PYME");
					 /*
					 String sEpo = registros.getString("ic_epo");
					 String sEpoDesc = registros.getString("EPO");
					 String sIF = registros.getString("ic_if");
					 String sIntermediario = registros.getString("Intermediario");
					 String sPyme = registros.getString("ic_pyme");
					 String sDescPyme = registros.getString("PYME");
					 */
					String  operaFide_Pyme =   getOperaFideicomisoPYME( sPyme, con );  // Fodea 19-2014
					boolean OperaIFEPOFISO  =  getOperaIFEPOFISO(sIF,sEpo, con);						
					
					String  ic_nafin_electronico = obtengoNafin(sPyme);
					String vClabePYME =  getValidaCcuentaClablePyme(ic_nafin_electronico, con ) ; //Fodea 019-2014
					String mensajeCtaClabe ="";
					boolean CtaClabe = false;	 
					String  validaEpoRea=  	getValidaPymeEpoRea(sPyme, sEpo, con ); // esta validacion es apra una reafiliaci�n de una pyme que opera fideicomiso en otra epo  que opera fideicomiso 
					
					log.debug("operaFide_Pyme :::::  "+operaFide_Pyme);
					log.debug("OperaIFEPOFISO :::::  "+OperaIFEPOFISO);
					log.debug("vClabePYME :::::  "+vClabePYME);	 
					log.debug("validaEpoRea :::::  "+validaEpoRea);	
					
					if( "S".equals(validaEpoRea) ) {
						if("S".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {	
							mensajeCtaClabe =  getcuentaClablePyme(ic_nafin_electronico, "CU", sEpo, con ); 	
							if(!mensajeCtaClabe.equals("")) {   
								CtaClabe = true;	  
							}									
						}else  if("N".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {									
							mensajeCtaClabe="La PYME no tiene parametrizada una Cuenta CLABE, favor de verificar"; 									
							CtaClabe = true;	
						}else  if("E".equals(vClabePYME) && "S".equals(operaFide_Pyme) && OperaIFEPOFISO )  {	
							mensajeCtaClabe="La Cuenta CLABE de la PYME no tiene estatus de Cuenta Correcta, favor de validarla"; 									
							CtaClabe = true;	
						} 			
						log.debug(" CtaClabe   " +CtaClabe +"   mensajeCtaClabe  :::::: "+mensajeCtaClabe); 
						//Fodea 019-2014(S)	
					
						}


							
					if(!CtaClabe )  {  // Fodea 19-2014				  
				  
					 //this.sDescEjecicion.append("PROCESANDO NUEVA RELACION PYME("+sPyme+"): "+sDescPyme+"IF("+sIF+"): "  + sIntermediario + " EPO("+sEpo+"): " + sEpoDesc + "\n");
					 bw.write("PROCESANDO NUEVA RELACION PYME("+sPyme+"): "+sDescPyme+"IF("+sIF+"): "  + sIntermediario + " EPO("+sEpo+"): " + sEpoDesc + "\n");
		
					 registros2 = getMonedasEPO(sEpo, sIF, "procesaConvenioUnicoXProveedor()", con);
		
					 if (registros2.getNumeroRegistros()!= 0) {
						while (registros2.next()){
						  String sMoneda = registros2.getString("ic_moneda");
		
						  //this.sDescEjecicion.append("PROCESANDO MONEDA: "  + sMoneda + "\n");
						  bw.write("PROCESANDO MONEDA: "  + sMoneda + "\n");
		
						  // Verifico por cada pyme si el IF ya autoriz� el convenio �nico a la pyme
						  // si es falso �nicamente se parametrizar� la relaci�n, si es verdadero se parametriza y autoriza
						  boolean convUnicoAut  = false;
						  if(!OperaIFEPOFISO ){
								convUnicoAut = verificaAutorizacionCU(sIF, sPyme, sMoneda, con);
							}else {
								convUnicoAut =  verificaAutorizacionFideicomiso( sPyme,  sMoneda, con);//Fodea 019-2014
							}						  
						  //Se obtiene la cuenta bancaria por moneda a utilizar
						  String cuentaBancaria = getCuentaBancariaCU(sPyme, sMoneda, con);
						  // Si no se encuentra la cuenta se envia al LOG y se continua con el siguiente registro
						  if (!"".equals(cuentaBancaria)){
							 //Verificamos si existe relacion previa
							 boolean existeCuenta = verificaRelacionPymeIF(sIF, sPyme, sEpo, sMoneda, con);
							 if (!existeCuenta) {
								//INI - MODIFICACION FODEA 036-2009 FVR
								/*
								 List lstRegDatosGral = new ArrayList();
								 lstRegDatosGral.add(sPyme);
								 lstRegDatosGral.add(sEpo);
								 lstRegDatosGral.add(sIF);
								 lstRegDatosGral.add(cuentaBancaria);
								 lstDatosGral.add(lstRegDatosGral);
								 */
								 //FIN - MODIFICACION FODEA 036-2009 FVR
								log.debug(" cuentaBancaria "+cuentaBancaria );
								
								//Realiza la parametrizacion de la cuenta de acuerdo a los parametros previos
								parametrizaCU(sIF, sPyme, sEpo, sMoneda, convUnicoAut, cuentaBancaria, "PYME", con);
															 
								log.debug(" convUnicoAut "+convUnicoAut );
								// Fodea 010-2013   Reafiliacion Automatica
															
								if(convUnicoAut) {
									paramCUDescAuto(sIF,  sIntermediario, sPyme, sDescPyme, sEpo , sEpoDesc,  sMoneda,  "DESAUTOMA" ,  "Proceso de Afiliaci�n Autom�tica de IFs con CU(procesaConvenioUnicoXProveedor)", "DESAUTOMA", con);
								}								
								/*
								this.sDescEjecicion.append("PYME PARAMETRIZADA:\n");
								this.sDescEjecicion.append("  IF ("+sIF+"):" + sIntermediario + "\n");
								this.sDescEjecicion.append("  EPO ("+sEpo+"):" + sEpoDesc + "\n");
								this.sDescEjecicion.append("  PYME("+sPyme+") : " + sDescPyme + "\n");
								this.sDescEjecicion.append("  MONEDA : "+sMoneda+"\n");
								this.sDescEjecicion.append("  CuentaBancaria : "+cuentaBancaria+"\n");
								*/
								bw.write("PYME PARAMETRIZADA:\n");
								bw.write("  IF ("+sIF+"):" + sIntermediario + "\n");
								bw.write("  EPO ("+sEpo+"):" + sEpoDesc + "\n");
								bw.write("  PYME("+sPyme+") : " + sDescPyme + "\n");
								bw.write("  MONEDA : "+sMoneda+"\n");
								bw.write("  CuentaBancaria : "+cuentaBancaria+"\n");
		
							 } else {
		
								/*
								this.sDescEjecicion.append("LA PYME YA TENIA RELACION PARAMETRIZADA CON EL IF EN LA MONEDA\n");
								this.sDescEjecicion.append("NO SE ACTUALIZA EL REGISTRO YA QUE SE PARAMETRIZO POR PANTALLAS\n");
								this.sDescEjecicion.append("SE CONTINUA CON EL SIGUIENTE REGISTRO\n");
								*/
								bw.write("LA PYME YA TENIA RELACION PARAMETRIZADA CON EL IF EN LA MONEDA\n");
								bw.write("NO SE ACTUALIZA EL REGISTRO YA QUE SE PARAMETRIZO POR PANTALLAS\n");
								bw.write("SE CONTINUA CON EL SIGUIENTE REGISTRO\n");
		
							 }
						  } else{
		
							 /*
							 this.sDescEjecicion.append("ERROR EN LA PARAMETRIZACION, NO SE ENCONTRO LA CUENTA BANCARIA DE CONVENIO UNICO: \n");
							 this.sDescEjecicion.append("  PYME("+sPyme+") : " + sDescPyme + "\n");
							 this.sDescEjecicion.append("  MONEDA : "+sMoneda+"\n");
							 */
							 bw.write("ERROR EN LA PARAMETRIZACION, NO SE ENCONTRO LA CUENTA BANCARIA DE CONVENIO UNICO: \n");
							 bw.write("  PYME("+sPyme+") : " + sDescPyme + "\n");
							 bw.write("  MONEDA : "+sMoneda+"\n");
		
						  }
		
						}
						
					 } else {
		
						//this.sDescEjecicion.append("NO SE ENCONTRARON TASAS AUTORIZADAS EN LA EPO\n");
						bw.write("NO SE ENCONTRARON TASAS AUTORIZADAS EN LA EPO\n");
		
					 }
					 
				  }
		
				  } // while (rs.next())
				} else {
		
				  //this.sDescEjecicion.append("NO SE ENCONTRARON NUEVOS PROVEEDORES PARA PARAMETRIZAR EL CONVENIO UNICO\n");
				  bw.write("NO SE ENCONTRARON NUEVOS PROVEEDORES PARA PARAMETRIZAR EL CONVENIO UNICO\n");
		
				}
				rs.close();
				ps.close();
				
			}/// while (rsC.next())
			
		}
		rsC.close();
		psC.close();
		
		//INI - MODIFICACION FODEA 036-2009 FVR
		/*
		Context context_a = ContextoJNDI.getInitialContext();
		ParametrosDescuentoHome parametrosDescuentoHome = (ParametrosDescuentoHome) context_a.lookup("ParametrosDescuentoEJB");
		ParametrosDescuento beanParametrosDescuento = parametrosDescuentoHome.create();
		beanParametrosDescuento.setParamEpoPymeIf(lstDatosGral,"");
		*/
		//FIN - MODIFICACION FODEA 036-2009 FVR
		
		} catch (NafinException ne) {
			throw ne;
		} catch (Exception e) {
			log.info("ConvenioUnico.procesaConvenioUnicoXProveedor(Exception)");
			e.printStackTrace();
			sDescEjecicion.append("Error inesperado procesaConvenioUnicoXProveedor " + e.toString());
			try{
				bw.write("Error inesperado procesaConvenioUnicoXProveedor " + e.toString());
			} catch (IOException ioe) {}
			throw new NafinException("AFIL0045");
		} finally {
			if (con.hayConexionAbierta()){
			  con.cierraConexionDB();
			}
			if (bw != null) try {
				bw.write("ConvenioUnico.procesaConvenioUnicoXProveedor(S)");
				bw.flush();
				bw.close();
			} catch (IOException ioe2) {}
			log.info("ConvenioUnico.procesaConvenioUnicoXProveedor---(S)");
			this.sDescEjecicion.append("ConvenioUnico.procesaConvenioUnicoXProveedor(S)");
		}

	}

  public StringBuffer getCodigoEjecucion(){
    return sDescEjecicion;

  }

  public StringBuffer getDescEjecucion(){
    return sDescEjecicion;
  }

  private boolean verificaAutorizacionCU(String sIF, String sPyme, String sMoneda, AccesoDBOracle conexion) throws NafinException{
    List lVarBind = new ArrayList();
    Registros	registros	= null;
    boolean retorno = false;
    try
    {
      log.info("ConvenioUnico.verificaAutorizacionCU(E)");
      String query = "SELECT count(1) as autCU, 'ConvenioUnico.procesaConvenioUnicoXIntermediario()' as proceso " +
              " FROM comrel_cuenta_bancaria cb,  " +
              " comrel_pyme_if pi"+
              " WHERE cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria "+
              " AND pi.ic_if = ? " +
              " AND cb.ic_pyme = ? "+
              " AND cb.ic_moneda = ? "+
              " AND cb.cs_borrado = ? "+
              " AND pi.cs_borrado = ? "+
              " AND pi.cs_vobo_if = ? "+
              " AND pi.cs_convenio_unico = ?";
      lVarBind.add(sIF);
      lVarBind.add(sPyme);
      lVarBind.add(sMoneda);
      lVarBind.add("N");
      lVarBind.add("N");
      lVarBind.add("S");
      lVarBind.add("S");
      registros = conexion.consultarDB(query, lVarBind, false);
      if (registros.next()){
        retorno = (Integer.parseInt(registros.getString("autCU"))==0)?false:true;
      }
    } catch(Exception e) {
        log.error("ConvenioUnico.verificaAutorizacionCU(Exception)" + e.getMessage());
        e.printStackTrace();
        throw new NafinException("AFIL0045");
    } finally {
        log.info("ConvenioUnico.verificaAutorizacionCU(S)");
    }
    return retorno;
  }

	/**
	 * Fodea 019-2014 
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param conexion
	 * @param sMoneda
	 * @param sPyme
	 * @param sIF
	 */
  private boolean verificaAutorizacionFideicomiso(String sPyme, String sMoneda, AccesoDBOracle conexion) throws NafinException{
    List lVarBind = new ArrayList();
    Registros	registros	= null;
    boolean retorno = false;
    try
    {
      log.info ("verificaAutorizacionFideicomiso (E)");
											
      String query = "SELECT count(1) as total  " 					+
							" FROM  COMREL_PYME_IF          PI, " 					+
							"       COMREL_CUENTA_BANCARIA  CB, " 					+
							"       COMREL_IF_EPO           IE  " 					+
							" WHERE PI.IC_CUENTA_BANCARIA = CB.IC_CUENTA_BANCARIA "	+
							" AND   PI.IC_IF              = IE.IC_IF "      		+
							" AND   PI.IC_EPO             = IE.IC_EPO "     		+									
							" AND   CB.IC_PYME            = ? " + 		
							" AND   CB.IC_MONEDA          = ? " +    	
							" AND   PI.CS_VOBO_IF         = ?  "           		+
							" AND   PI.CS_BORRADO         = ?  "           		+
							" AND   CB.CS_BORRADO         = ? "           		+
							" AND   IE.CS_VOBO_NAFIN      = ?  "           		+
							" AND   IE.CS_ACEPTACION      = ? " ;							
    
      lVarBind.add(sPyme);
      lVarBind.add(sMoneda);
		lVarBind.add("S");
      lVarBind.add("N");
      lVarBind.add("N");
      lVarBind.add("S");
      lVarBind.add("S");
		
      registros = conexion.consultarDB(query, lVarBind, false);
      if (registros.next()){
        retorno = (Integer.parseInt(registros.getString("total"))==0)?false:true;
      }
    }catch(Exception e)   {
		log.error("verificaAutorizacionFideicomiso )" + e.getMessage());
      e.printStackTrace();
      throw new NafinException("AFIL0045");
    }finally{
      log.info("verificaAutorizacionFideicomiso(S)");
    }
    return retorno;
  }
  
  
  private String getCuentaBancariaCU(String sPyme, String sMoneda, AccesoDBOracle conexion) throws NafinException{
    List lVarBind = new ArrayList();
    Registros	registros	= null;
    String retorno = "";
    try {
      log.info("ConvenioUnico.getCuentaBancariaCU(E)");
      String query = "SELECT ic_cuenta_bancaria, 'ConvenioUnico.getCuentaBancariaCU()' as proceso " +
              " FROM comrel_cuenta_bancaria " +
              " WHERE ic_pyme = to_number(?) "  +
              " AND ic_moneda = to_number(?) "+
              " AND cs_convenio_unico = ? "+
              " AND cs_borrado = ?";
      lVarBind.add(sPyme);
      lVarBind.add(sMoneda);
      lVarBind.add("S");
      lVarBind.add("N");
      registros = conexion.consultarDB(query, lVarBind, false);
      if (registros.next()){
        retorno = registros.getString("ic_cuenta_bancaria");
      }
    } catch(Exception e) {
        log.error("ConvenioUnico.getCuentaBancariaCU(Exception)" + e.getMessage());
        e.printStackTrace();
        throw new NafinException("AFIL0045");
    } finally {
        log.info("ConvenioUnico.getCuentaBancariaCU(S)");
    }
    return retorno;
  }

  private boolean verificaRelacionPymeIF(String sIF, String sPyme, String sEpo, String sMoneda, AccesoDBOracle conexion) throws NafinException{
    List lVarBind = new ArrayList();
    Registros	registros	= null;
    boolean retorno = false;
    try {
      log.info("ConvenioUnico.verificaRelacionPymeIF(E)");
      String 	query = "select pi.ic_if, pi.ic_epo, cb.ic_pyme, cb.ic_moneda, pi.cs_vobo_if, pi.cs_borrado, 'AfiliacionBean.reafiliaAutIfs()' as proceso " +
                    "from comrel_pyme_if pi, comrel_cuenta_bancaria cb " +
                    "where pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria " +
                    "and pi.ic_if = ? " +
                    "and pi.ic_epo = ? " +
                    "and cb.ic_pyme = ? " +
                    "and cb.ic_moneda = ? " +
                    "and pi.cs_borrado = ? "+
                    "and cb.cs_borrado = ? ";
      lVarBind.add(sIF);
      lVarBind.add(sEpo);
      lVarBind.add(sPyme);
      lVarBind.add(sMoneda);
      lVarBind.add("N");
      lVarBind.add("N");
      registros = conexion.consultarDB(query, lVarBind, false);
      retorno = (registros.getNumeroRegistros()==0)?false:true;
    } catch(Exception e) {
      log.error("ConvenioUnico.verificaRelacionPymeIF(Exception)" + e.getMessage());
      e.printStackTrace();
      throw new NafinException("AFIL0045");
    } finally {
      log.info("ConvenioUnico.verificaRelacionPymeIF(S)");
    }
    return retorno;
  }

  private Registros getMonedasEPO(String sEpo, String sIF, String metodo, AccesoDBOracle conexion) throws NafinException{
    List lVarBind = new ArrayList();
    Registros	registros	= null;
    try {
      log.info("ConvenioUnico.getMonedasEPO(E)");
        // Este query determina las monedas que opera la EPO para que se parametricen al CU las pymes
      String query = "select distinct ic_moneda, 'ConvenioUnico."+metodo+"' as proceso " +
            "from comrel_tasa_base tb, comrel_tasa_autorizada ta, comcat_tasa t " +
            "where tb.dc_fecha_tasa = ta.dc_fecha_tasa " +
            "and tb.ic_tasa = t.ic_tasa " +
            "and ta.ic_epo = ? " +
            "and ta.ic_if = ? " +
            "and tb.cs_tasa_activa = ? " +
            "and ta.CS_VOBO_EPO = ? " +
            "and ta.CS_VOBO_IF = ? " +
            "and ta.CS_VOBO_NAFIN = ?";
      lVarBind.add(sEpo);
      lVarBind.add(sIF);
      lVarBind.add("S");
      lVarBind.add("S");
      lVarBind.add("S");
      lVarBind.add("S");
      registros = conexion.consultarDB(query, lVarBind, false);
    } catch(Exception e) {
      log.error("ConvenioUnico.getMonedasEPO(Exception)" + e.getMessage());
      e.printStackTrace();
      throw new NafinException("AFIL0045");
    } finally {
      log.info("ConvenioUnico.getMonedasEPO(S)");
    }
    return registros;
  }



  private void parametrizaCU(String sIF, String sPyme, String sEpo, String sMoneda, boolean convUnicoAut, String cuentaBancaria, String sProceso, AccesoDBOracle conexion) throws NafinException {
    List lVarBind = new ArrayList();
    PreparedStatement ps = null;
    PreparedStatement ps2 = null;
	 ResultSet rs = null;
	 ResultSet rs2 = null;
    int regInsertados = 0;
	 int regRelCta = 0;
    String query = "";
    boolean ok = false;
    try   {
      log.info("ConvenioUnico.parametrizaCU(E)");
      log.info("IF: " + sIF);
      log.info("sPyme: " + sPyme);
      log.info("sEpo: " + sEpo);
      log.info("sMoneda: " + sMoneda);
      log.info("convUnicoAut: " + convUnicoAut);
      log.info("cuentaBancaria: " + cuentaBancaria);
      log.info("sProceso: " + sProceso);
      
		//SE VERIFICA EN COMREL_PYME_IF SI EXISTE LA RELACION DE LA CUENTA CON LA EPO E IF COMO BORRADA (CS_BORRADO = S)
		query = "SELECT COUNT (*) as existRel " +
				"  FROM comrel_pyme_if " +
				" WHERE ic_cuenta_bancaria = ? " +
				"   AND ic_if = ? " +
				"   AND ic_epo = ? " +
				"   AND cs_borrado = ? ";

		ps = conexion.queryPrecompilado(query);
      ps.clearParameters();
      ps.setInt(1, Integer.parseInt(cuentaBancaria));
      ps.setInt(2, Integer.parseInt(sIF));
      ps.setInt(3, Integer.parseInt(sEpo));
		ps.setString(4, "S");
		rs = ps.executeQuery();
      if(rs!=null && rs.next()){
			regRelCta = rs.getInt("existRel");
		}
		rs.close();
		ps.close();

		//SI YA EXISTE LA RELACION COMO BORRADA, ENTOCES SE REALIZA UPDATE,
		//EN CASO CONTRARIO SE REALIZA LA INSERCION
		if(regRelCta>0){
			query = "UPDATE comrel_pyme_if " +
				"   SET cs_vobo_if = ?, " +
				"     cs_borrado = ?, " +
				"     cs_opera_descuento = ?, " +
				"     df_vobo_if = SYSDATE, " +
				"     df_param_aut_conv_unico = SYSDATE, " +
				"		cs_dscto_automatico = 'N', " +
				"		cs_dscto_automatico_dia = 'P', " +
				"		cs_estatus = null, " +
				"		df_envio = null, " +
				"		df_autorizacion = null, " +
				"		cs_cambio_cuenta = null, " +
				"		df_reasigno_cta = null, " +
				"		cs_dscto_automatico_proc = 'N', " +
				"		ig_orden = null, " +
				"		df_parametrizacion_orden = null, " +
				"		cs_rechazo = 'N', " +
				"		cg_nota = null, " +
				"		df_ult_causa = null, " +
				"		cs_convenio_unico = 'N' " +
				" WHERE ic_cuenta_bancaria = ? AND ic_if = ? AND ic_epo = ? ";
			
			ps = conexion.queryPrecompilado(query);
			ps.clearParameters();
			ps.setString(1, ((!convUnicoAut)?"N":"S"));
			ps.setString(2, "N");
			ps.setString(3, "S");
			ps.setInt(4, Integer.parseInt(cuentaBancaria));
			ps.setInt(5, Integer.parseInt(sIF));
			ps.setInt(6, Integer.parseInt(sEpo));
			regInsertados = ps.executeUpdate();
			ps.close();
			
		}else{
			query = "INSERT INTO comrel_pyme_if (" +
				 "ic_cuenta_bancaria," +
				 "ic_if," +
				 "ic_epo," +
				 "cs_vobo_if," +
				 "cs_borrado," +
				 "cs_opera_descuento," +
				 "DF_VOBO_IF," +
				 "DF_PARAM_AUT_CONV_UNICO)" +
				 " values(?,?,?,?,?,?,SYSDATE,SYSDATE)";
			ps = conexion.queryPrecompilado(query);
			ps.clearParameters();
			ps.setInt(1, Integer.parseInt(cuentaBancaria));
			ps.setInt(2, Integer.parseInt(sIF));
			ps.setInt(3, Integer.parseInt(sEpo));
			ps.setString(4, ((!convUnicoAut)?"N":"S"));
			ps.setString(5, "N");
			ps.setString(6, "S");
			regInsertados = ps.executeUpdate();
			ps.close();
		}
		
      if (convUnicoAut && "IF".equals(sProceso)){
        query = " UPDATE comrel_pyme_epo " +
            " SET cs_aceptacion = ?" +
            " WHERE ic_epo = ? " +
            " AND ic_pyme = ?";
        ps2 = conexion.queryPrecompilado(query);
        ps2.clearParameters();
        ps2.setString(1, "H"); // SE HABILITA EL REQUISITO 8
        ps2.setInt(2, Integer.parseInt(sEpo));
        ps2.setInt(3, Integer.parseInt(sPyme));
        ps2.executeUpdate();
        ps2.close();
      }

      if ("PYME".equals(sProceso)){
		  int existBloq = 0;
		  query = "SELECT COUNT (1) as existBloq " +
					"  FROM comrel_pyme_epo pe, " +
					"       comrel_if_epo ie, " +
					"       comcat_if i, " +
					"       comcat_pyme p, " +
					"       comcat_epo e " +
					" WHERE pe.ic_epo = ie.ic_epo " +
					"   AND pe.ic_pyme = p.ic_pyme " +
					"   AND i.ic_if = ie.ic_if " +
					"   AND e.ic_epo = ie.ic_epo " +
					"   AND (pe.cg_reaf_cu = ? OR pe.cg_reaf_cu = ?) " +
					"   AND p.cs_convenio_unico = ? " +
					"   AND i.cs_convenio_unico = ? " +
					"   AND ie.cs_aceptacion = ? " +
					"   AND e.cs_convenio_unico = ? " +
					"   AND ie.cs_convenio_unico = ? " +
					"   AND ie.cs_bloqueo = ? " +
					"   AND pe.ic_epo = ? " +
					"   AND pe.ic_pyme = ? ";
					
			ps2 = conexion.queryPrecompilado(query);
			ps2.setString(1,"N");
			ps2.setString(2,"A");
			ps2.setString(3,"S");
			ps2.setString(4,"S");
			ps2.setString(5,"S");
			ps2.setString(6,"S");
			ps2.setString(7,"S");
			ps2.setString(8,"S");
			ps2.setLong(9, Long.parseLong(sEpo));
         ps2.setLong(10, Long.parseLong(sPyme));
			rs2 = ps2.executeQuery();
			
			if(rs2!=null && rs2.next()){
				existBloq = rs2.getInt("existBloq");
			}
			rs2.close();
			ps2.close();
			
			if(existBloq <= 0 || (existBloq > 0 && convUnicoAut)){
				if(existBloq > 0 && convUnicoAut){
					query = "UPDATE comrel_pyme_epo " +
					" SET cs_aceptacion = ? "+
					" WHERE ic_epo = ? " +
					" AND ic_pyme = ? ";		
				}else if(existBloq <= 0){
					query = "UPDATE comrel_pyme_epo " +
					" SET cg_reaf_CU = ? "+
					((convUnicoAut)?", cs_aceptacion = ?":" ")+
					" WHERE ic_epo = ? " +
					" AND ic_pyme = ? ";
				}
			  
			  ps2 = conexion.queryPrecompilado(query);
			  ps2.clearParameters();
			  
			  if(existBloq > 0 && convUnicoAut){
					ps2.setString(1, "H"); // SE HABILITA EL REQUISITO 8
					ps2.setInt(2, Integer.parseInt(sEpo));
					ps2.setInt(3, Integer.parseInt(sPyme));
			  }else if(existBloq <= 0){
				  ps2.setString(1, "P"); //PROCESADO
				  if (convUnicoAut){
					 ps2.setString(2, "H"); // SE HABILITA EL REQUISITO 8
					 ps2.setInt(3, Integer.parseInt(sEpo));
					 ps2.setInt(4, Integer.parseInt(sPyme));
				  }else{
					 ps2.setInt(2, Integer.parseInt(sEpo));
					 ps2.setInt(3, Integer.parseInt(sPyme));
				  }
			  }
			  ps2.executeUpdate();
			  ps2.close();
			}
      }
      ok = true;
    }catch(SQLException sqle){
      ok = false;
      log.info("ConvenioUnico.parametrizaCU(SQLException)" + sqle.getMessage());
      sqle.printStackTrace();
      throw new NafinException("AFIL0045");
    }catch(Exception e){
      ok = false;
      log.info("ConvenioUnico.parametrizaCU(Exception)" + e.getMessage());
      e.printStackTrace();
      throw new NafinException("AFIL0045");
    }finally{
      if (conexion.hayConexionAbierta()){
        conexion.terminaTransaccion(ok);
      }
      log.info("ConvenioUnico.parametrizaCU(S)");
    }
  }



	private String obtengoNafin(String ic_pyme){
		log.info("obtengoNafin(E)");
		AccesoDBOracle	con	 = new AccesoDBOracle();
		PreparedStatement ps = null;
		ResultSet rs			= null;
		boolean bBien = true;
		String numNafinElectronico="";
		try{
			con.conexionDB();
			String 	strSQL = " SELECT ic_nafin_electronico " +
						" FROM comrel_nafin " +
						" WHERE ic_epo_pyme_if = ?" +
						" AND cg_tipo = ?";
				ps = con.queryPrecompilado(strSQL);		
				ps.setString(1, ic_pyme);
				ps.setString(2, "P");
				rs = ps.executeQuery();
				if (rs.next()) {
					numNafinElectronico = rs.getString("ic_nafin_electronico");
				}
				rs.close();
				ps.close();		
		}catch(Exception e){
				bBien = false;
			log.error("obtengoNafin(Error)",e);
		}finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			log.info("obtengoNafin(S)");
		}
		return numNafinElectronico;
	 }// End ObtengoNafin
    
    
    /**  Metodo replicado de ParametrosDescuentoBean
     *  para utilizar la conexion directa AccesoDBOracle
     */
    public void paramCUDescAuto(String sIF,  String sIntermediario, String sPyme, String nombrePyme, String sEpo, String nombreEpo,
                                        String sMoneda, String strLogin, String strNombreUsuario, String pantalla, AccesoDBOracle con) throws NafinException {
    List lVarBind = new ArrayList();
    PreparedStatement ps = null;
    ResultSet rs = null;
    int ic_version_convenio = 0;
    String query = "";
    boolean ok = false;
    String descAuto ="N", nun_elecPyme ="", cs_aceptacion ="";
    boolean existeParam    = false;
    log.info("paramCUDescAuto (E)");
    try   {
            log.debug("sEpo   "+sEpo);
            log.debug("sPyme   "+sPyme);
            log.debug("sIF   "+sIF);
            if(sIntermediario.equals("") && nombrePyme.equals("")  &&  nombreEpo.equals("") )  {
                //obtengo nombre PYME
                query = "SELECT CG_RAZON_SOCIAL   FROM  comcat_pyme  WHERE ic_pyme = ? ";
                ps = con.queryPrecompilado(query);
                ps.clearParameters();
                ps.setInt(1, Integer.parseInt(sPyme));
                rs = ps.executeQuery();
                if(rs!=null && rs.next()){
                    nombrePyme =  rs.getString("CG_RAZON_SOCIAL").trim();
                }
                rs.close();
                ps.close();
                //obtengo nombre EPO
                query = "SELECT CG_RAZON_SOCIAL   FROM  comcat_epo  WHERE ic_epo = ? ";
                ps = con.queryPrecompilado(query);
                ps.clearParameters();
                ps.setInt(1, Integer.parseInt(sEpo));
                rs = ps.executeQuery();
                if(rs!=null && rs.next()){
                    nombreEpo =  rs.getString("CG_RAZON_SOCIAL").trim();
                }
                rs.close();
                ps.close();
                //obtengo nombre del IF
                query = "SELECT CG_RAZON_SOCIAL   FROM  comcat_if  WHERE ic_if= ? ";
                ps = con.queryPrecompilado(query);
                ps.clearParameters();
                ps.setInt(1, Integer.parseInt(sIF));
                rs = ps.executeQuery();
                if(rs!=null && rs.next()){
                    sIntermediario =  rs.getString("CG_RAZON_SOCIAL").trim();
                }
                rs.close();
                ps.close();
            }
            //verifica  sea una  reafiliaci�n
            query = "SELECT cs_aceptacion    " +
                    "  FROM  comrel_pyme_epo  " +
                    " WHERE ic_pyme = ? "+
                    "  AND  ic_epo = ? ";
            ps = con.queryPrecompilado(query);
            ps.clearParameters();
            ps.setInt(1, Integer.parseInt(sPyme));
            ps.setInt(2, Integer.parseInt(sEpo));
            rs = ps.executeQuery();
            if(rs!=null && rs.next()){
                cs_aceptacion = rs.getString("cs_aceptacion")==null?"":rs.getString("cs_aceptacion");
            }
            rs.close();
            ps.close();
            log.debug("cs_aceptacion   "+cs_aceptacion);
            //if(cs_aceptacion.equals("H")  || cs_aceptacion.equals("R") )  {
            if(cs_aceptacion.equals("H") )  {
                //SE VERIFICA el tipo de convenio
                query = "SELECT ic_version_convenio   " +
                        "  FROM  comcat_pyme " +
                        " WHERE ic_pyme = ? ";
                ps = con.queryPrecompilado(query);
                ps.clearParameters();
                ps.setInt(1, Integer.parseInt(sPyme));
                rs = ps.executeQuery();
                if(rs!=null && rs.next()){
                    ic_version_convenio = rs.getInt("ic_version_convenio");
                }
                rs.close();
                ps.close();
                log.debug("ic_version_convenio   "+ic_version_convenio);
                if(ic_version_convenio>=3){
                    // verifico si la pyme esta parametrizada con descuento Automatico
                     String strQry = " select n.IC_NAFIN_ELECTRONICO AS NUM_NAFIN, "+
                                            " p.CS_DSCTO_AUTOMATICO AS DSCTO_AUT   "+
                                            " FROM  comrel_nafin n ,  "+
                                            " comcat_pyme p  "+
                                            " Where n.IC_EPO_PYME_IF = p.ic_pyme "+
                                            " and CG_TIPO = 'P'  "+
                                            " and p.ic_pyme = ? ";
                    lVarBind        = new ArrayList();
                    lVarBind.add(new Integer(sPyme));
                    ps = con.queryPrecompilado(strQry,lVarBind );
                    rs = ps.executeQuery();
                    if(rs.next() && rs!=null){
                        descAuto = rs.getString("DSCTO_AUT").trim();
                        nun_elecPyme = rs.getString("NUM_NAFIN").trim();
                    }
                    rs.close();
                    ps.close();
                    log.debug("descAuto   "+descAuto);
                        if(descAuto.equals("S")) {
                            int desAutomatico    =0;
                            //reviso que parametrizacion tiene en la version nueva
                            //NA aun no esta parametrizada en la tabla COMREL_DSCTO_AUT_PYME
                            strQry =" SELECT COUNT (*) AS total  " +
                                      " FROM (SELECT a.ic_pyme, a.ic_epo, a.ic_if,  "+
                                      " DECODE (dap.cs_dscto_automatico_proc,  '', 'NA', dap.cs_dscto_automatico_proc    ) AS cs_dscto_automatico_proc  "+
                                      " FROM (SELECT /*+ use_nl(py,cb,pi,e,if,m,mo) */  " +
                                      " pi.ic_epo, cb.ic_pyme, pi.ic_if, cb.ic_moneda  "+
                                      " FROM comrel_cuenta_bancaria cb,   "+
                                      " comrel_pyme_if pi, "+
                                      " comrel_pyme_epo rp "+
                                      " WHERE cb.ic_moneda = ?  "+
                                      " AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria "+
                                      " AND pi.cs_borrado = 'N'  "+
                                      " AND cb.cs_borrado = 'N'  "+
                                      " AND pi.cs_vobo_if = 'S'  "+
                                      " AND pi.cs_opera_descuento = 'S'  "+
                                      " AND cb.ic_pyme = ? "+
                                      " AND cb.ic_pyme = rp.ic_pyme  "+
                                      " AND rp.cs_aceptacion = 'H'  " +
                                      " AND pi.ic_epo = rp.ic_epo  "+
                                      " AND pi.ic_if = ?   "+
                                      " AND pi.ic_epo !=    ?  ) a,  "+
                                      " comrel_dscto_aut_pyme dap  "+
                                      " WHERE a.ic_if = dap.ic_if(+)  "+
                                      " AND a.ic_epo = dap.ic_epo(+)  "+
                                      " AND a.ic_pyme = dap.ic_pyme(+)  "+
                                      " AND a.ic_moneda = dap.ic_moneda(+)) pa  "+
                                      " WHERE cs_dscto_automatico_proc != 'S'  ";
    
                            lVarBind        = new ArrayList();
                            lVarBind.add(new Integer(sMoneda));
                            lVarBind.add(new Integer(sPyme));
                            lVarBind.add(new Integer(sIF));
                            lVarBind.add(new Integer(sEpo));
                            ps = con.queryPrecompilado(strQry,lVarBind );
                            log.debug("strQry::  "+strQry);
                            log.debug("lVarBind::  "+lVarBind);
                            rs = ps.executeQuery();
                            if(rs.next() && rs!=null){
                                desAutomatico = Integer.parseInt(rs.getString("total").trim());
                            }
                            rs.close();
                            ps.close();
                          /*si desAutomatico == 0  entonces todas las parametrizaciones estan en "S"
                            * por lo cual si realizare la parametrizaci�n del Descuento Automatico
                          */
                           log.debug("desAutomatico   "+desAutomatico);
                            if(desAutomatico==0 )  {
                                int desAutomaticoEPO     =0;
                                strQry ="";
                                strQry =" SELECT COUNT (*) AS total  " +
                                          " FROM (SELECT a.ic_pyme, a.ic_epo, a.ic_if,  "+
                                          " DECODE (dap.cs_dscto_automatico_proc,  '', 'NA', dap.cs_dscto_automatico_proc    ) AS cs_dscto_automatico_proc  "+
                                          " FROM (SELECT /*+ use_nl(py,cb,pi,e,if,m,mo) */  " +
                                          " pi.ic_epo, cb.ic_pyme, pi.ic_if, cb.ic_moneda  "+
                                          " FROM comrel_cuenta_bancaria cb,   "+
                                          " comrel_pyme_if pi, "+
                                          " comrel_pyme_epo rp "+
                                          " WHERE cb.ic_moneda = ?  "+
                                          " AND cb.ic_cuenta_bancaria = pi.ic_cuenta_bancaria "+
                                          " AND pi.cs_borrado = 'N'  "+
                                          " AND cb.cs_borrado = 'N'  "+
                                          " AND pi.cs_vobo_if = 'S'  "+
                                          " AND pi.cs_opera_descuento = 'S'  "+
                                          " AND cb.ic_pyme = ? "+
                                          " AND cb.ic_pyme = rp.ic_pyme  "+
                                          " AND rp.cs_aceptacion = 'H'  " +
                                          " AND pi.ic_epo = rp.ic_epo  "+
                                          " AND pi.ic_if != ?   "+
                                          " AND pi.ic_epo =  ?  ) a,  "+
                                          " comrel_dscto_aut_pyme dap  "+
                                          " WHERE a.ic_if = dap.ic_if(+)  "+
                                          " AND a.ic_epo = dap.ic_epo(+)  "+
                                          " AND a.ic_pyme = dap.ic_pyme(+)  "+
                                          " AND a.ic_moneda = dap.ic_moneda(+)) pa  "+
                                          " WHERE cs_dscto_automatico_proc != 'S'  ";
    
                                lVarBind        = new ArrayList();
                                lVarBind.add(new Integer(sMoneda));
                                lVarBind.add(new Integer(sPyme));
                                lVarBind.add(new Integer(sIF));
                                lVarBind.add(new Integer(sEpo));
                                ps = con.queryPrecompilado(strQry,lVarBind );
                                log.debug("strQry::  "+strQry);
                                log.debug("lVarBind::  "+lVarBind);
                                rs = ps.executeQuery();
                                if(rs.next() && rs!=null){
                                    desAutomaticoEPO = Integer.parseInt(rs.getString("total").trim());
                                }
                                    rs.close();
                                    ps.close();
                            log.debug("desAutomaticoEPO::  "+desAutomaticoEPO);
                            if(desAutomaticoEPO==0) {
                                Hashtable alParamEPO1 = getParametrosEPO(String.valueOf(sEpo),1, con);
                                String  bOperaFactorajeVencido = alParamEPO1.get("PUB_EPO_FACTORAJE_VENCIDO").toString();
                                String nombreFactoraje ="", orden ="0";
                                List tipoFactoraje = new ArrayList();
                                tipoFactoraje.add("N");// Factoraje Normal
                                if(bOperaFactorajeVencido.equals("S"))  {
                                    tipoFactoraje.add("V"); // Factoraje Vencido
                                }
                                StringBuffer SQL    =   new StringBuffer();
                                SQL.append("    INSERT INTO COMREL_DSCTO_AUT_PYME   ");
                                SQL.append("        (                                           ");
                                SQL.append("            IC_PYME                             ");
                                SQL.append("            ,IC_EPO                             ");
                                SQL.append("            ,IC_IF                              ");
                                SQL.append("            ,IC_MONEDA                          ");
                                SQL.append("            ,CS_ORDEN                           ");
                                SQL.append("            ,CS_DSCTO_AUTOMATICO_DIA        ");
                                SQL.append("            ,CS_DSCTO_AUTOMATICO_PROC       ");
                                SQL.append("            ,CC_TIPO_FACTORAJE              ");
                                SQL.append("            ,IC_USUARIO                         ");
                                SQL.append("            ,CG_NOMBRE_USUARIO              ");
                                SQL.append("            ,CC_ACUSE                           ");
                                SQL.append("            ,DF_FECHAHORA_OPER              ");
                                SQL.append("        )                                           ");
                                SQL.append("        VALUES (                                    ");
                                SQL.append("            ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,  ?, SYSDATE    ) ");
                                StringBuffer insertBitacora = new StringBuffer();
                                insertBitacora.append(
                                                "\n INSERT INTO BIT_CAMBIOS_GRAL ( " +
                                                "\n IC_CAMBIO,  " +
                                                "\n CC_PANTALLA_BITACORA,  " +
                                                "\n CG_CLAVE_AFILIADO, " +
                                                "\n IC_NAFIN_ELECTRONICO, " +
                                                "\n DF_CAMBIO, " +
                                                "\n IC_USUARIO, " +
                                                "\n CG_NOMBRE_USUARIO, " +
                                                "\n CG_ANTERIOR, " +
                                                "\n CG_ACTUAL, " +
                                                "\n IC_GRUPO_EPO, " +
                                                "\n IC_EPO ) " +
                                                "\n VALUES(  " +
                                                "\n SEQ_BIT_CAMBIOS_GRAL.nextval, " +
                                                "\n 'DESAUTOMA', " +
                                                "\n 'P', " +
                                                "\n  ?  , " +
                                                "\n SYSDATE, " +
                                                "\n    ? ,  " +
                                                "\n   ? , " +
                                                "\n '',  " +
                                                "\n   ?,  " +
                                                "\n '', " +
                                                "\n '' ) " );
                                List variables = new ArrayList();
                                PreparedStatement pst       =   null;
                                for(int y=0; y<tipoFactoraje.size(); y++)  {
                                    String mensajeOrden ="", moneda ="";
                                    String valorFact =  (String)tipoFactoraje.get(y);
                                    //valido si existe ya la parametrizaci�n
                                    existeParam= existeParametrizacion(sEpo, sIF, sPyme, sMoneda, valorFact, con);
                                    log.debug("existeParam   "+existeParam);
                                    if(!existeParam){
                                        if(valorFact.equals("N"))  {
                                            orden=  getOrden(sEpo, sPyme, sMoneda, valorFact, con);
                                            nombreFactoraje = "Normal";  mensajeOrden= "Orden= "+orden+"\n";
                                        }
                                        if(valorFact.equals("V"))  {  orden = "0";  nombreFactoraje = "Vencido"; }
                                        if(sMoneda.equals("54"))    {    moneda = "DOLAR AMERICANO"; }
                                        if(sMoneda.equals("1"))    {    moneda = "MONEDA NACIONAL"; }
                                        String actual =  mensajeOrden+
                                                              " Modalidad= Primer dia desde su publicacion "+"\n"+
                                                              " EPO="+nombreEpo+"\n"+
                                                              " PYME="+nombrePyme+"\n"+
                                                              " IF="+sIntermediario +"\n"+
                                                              " Moneda= "+moneda+   "\n"+
                                                              " Pantalla= "+pantalla+"\n"+
                                                              " Tipo Factoraje =" +nombreFactoraje+"\n"+
                                                              " Descuento Autom�tico = S " +
                                                              " Metodo(paramCUDescAuto)";
                                        // insert en COMREL_DSCTO_AUT_PYME
                                        variables = new ArrayList();
                                        variables.add(new Integer(sPyme));  //PYME
                                        variables.add( new Integer(sEpo));  //EPO
                                        variables.add( new Integer(sIF)); //IC_IF
                                        variables.add( new Integer(sMoneda)); //MONEDA
                                        variables.add(orden);                       //ORDEN
                                        variables.add("P");                         //MODALIDAD p -> primer dia...
                                        variables.add("S");                         //S - APLICA DSCTO AUTOMATICO
                                        variables.add(valorFact);               //TIPO FACTIRAJE
                                        variables.add(strLogin);                    //IC_USUARIO
                                        variables.add(strNombreUsuario);        //NOMBRE USUARIO
                                        variables.add("0");     //ACUSE
                                        log.debug("SQL.toString(  "+SQL.toString());
                                        log.debug("variables(  "+variables);
                                        pst = con.queryPrecompilado(SQL.toString(), variables);
                                        pst.executeUpdate();
                                        pst.close();
                                        //insert en BIT_CAMBIOS_GRAL
                                        variables = new ArrayList();
                                        variables.add(nun_elecPyme);    //nun_elecPyme
                                        variables.add( strLogin);   //EPO
                                        variables.add( strNombreUsuario); //IC_IF
                                        variables.add( actual); //MONEDA
                                        log.debug("SQL.toString(  "+SQL.toString());
                                        log.debug("variables(  "+variables);
                                        pst = con.queryPrecompilado(insertBitacora.toString(), variables);
                                        pst.executeUpdate();
                                        pst.close();
                                        log.debug("S� la �ltima acci�n para todas las EPO�s fue parametrizar el Descuento Autom�tico con el IF, el sistema SI parametrizar� el Descuento Autom�tico a la(s) EPO(s)  ");
                                    }
                                }//for
                                }
                            }   
                            else  {
                                log.debug("S� la �ltima acci�n para alguna EPO fue Desparametrizar el Descuento Autom�tico con el IF  No se hace NADA ");
                            }
                        }
                        else {
                            log.debug("LA PYME NO ESTA PARAMETRIZADA CON DESCUENTO AUTOMATICO  ");
                        }
                }
                else  {
                    log.debug("LA PYME TIENE TIPO DE CONVENIO MENOR A 3  ");
                }
            }
          ok = true;
        }catch(SQLException sqle){
          ok = false;
          log.error("paramCUDescAuto(SQLException)" + sqle.getMessage());
          sqle.printStackTrace();
          throw new NafinException("AFIL0045");
        }catch(Exception e){
          ok = false;
          log.error("paramCUDescAuto(Exception)" + e.getMessage());
          e.printStackTrace();
          throw new NafinException("AFIL0045");
        }finally{
         // es responsabilidad de metodo donde es llamado el commit
         log.info("paramCUDescAuto:::::::::(S)");
        }
    }// End paramCUDescAuto

    /**
     * Metodo replicado de ParametrosDescuentoBean para usar el accedo a Base de Datos Directo 
     *  Para Obtener el Orden
     * @return
     * @param cc_tipo_factoraje
     * @param ic_moneda
     * @param ic_pyme
     * @param ic_if
     * @param ic_epo
     */
    private String getOrden(String ic_epo, String ic_pyme, String ic_moneda, String cc_tipo_factoraje, AccesoDBOracle con){
        ResultSet rs = null;
        PreparedStatement ps = null;
        String orden = "0";
        List lVarBind = new ArrayList();
        int ordenF =0;
        log.info("getOrden (E)");
         try{
         con.conexionDB();
         String strQry = "SELECT MAX(CS_ORDEN)  as orden  "  +
                        "  FROM comrel_dscto_aut_pyme   "  +
                        " WHERE ic_epo = ?              "  +
                        "   AND ic_pyme = ?             "  +
                        "   AND ic_moneda = ?           "  +
                        "   AND cc_tipo_factoraje = ?   ";
        lVarBind        = new ArrayList();
         lVarBind.add(new Integer(ic_epo));
         lVarBind.add(new Integer(ic_pyme));
         lVarBind.add(new Integer(ic_moneda));
         lVarBind.add(cc_tipo_factoraje);
            ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next() && rs!=null){
                orden = rs.getString("orden")==null?"0":rs.getString("orden");
         }
            rs.close();
        ps.close();
            ordenF =  Integer.parseInt(orden)+1;
            orden  =  String.valueOf(ordenF);
      }catch(Exception e){
         e.printStackTrace();
            con.terminaTransaccion(false);
            log.error("Error en  getOrden (S)" +e);
         throw new RuntimeException(e);
      }finally{
            if(con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
            }
      }
      log.info("getOrden (S)");
      return orden;
    }
	 
     /**
      *  Metodo modificado de ParametrosDescuentoBean para usar conecxion directa la BD
      * 
      * */
    private Hashtable getParametrosEPO(String sNoEPO, int iNoProducto, AccesoDBOracle con) throws NafinException {
        Hashtable parametros = new Hashtable();
            log.info("getParametrosEPO(E)");
        try {
        String sQuery = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        sQuery =
            " SELECT " +
            " NVL(CS_FACTORAJE_VENCIDO,'N') AS PUB_EPO_FACTORAJE_VENCIDO " +
            " FROM comrel_producto_epo"   +
            " WHERE ic_epo = ?"   +
            " AND ic_producto_nafin = ?"  ;
        ps = con.queryPrecompilado(sQuery);
        ps.setString(1, sNoEPO);
        ps.setInt(2, iNoProducto);
        log.debug("sNoEPO :::"+sNoEPO);
        log.debug("iNoProducto :::"+iNoProducto);
        log.debug(":::Consulta Parametros:::"+ sQuery);
        rs = ps.executeQuery();
        if(rs.next()) {
            parametros.put("PUB_EPO_FACTORAJE_VENCIDO", rs.getString("PUB_EPO_FACTORAJE_VENCIDO")==null?"":rs.getString("PUB_EPO_FACTORAJE_VENCIDO"));
        }
        rs.close();
        if(ps!=null) ps.close();
        log.debug("::resultado antes de salir del bean::"+parametros.toString());
        } 
        catch(SQLException sqle) {
            sqle.printStackTrace();
            throw new NafinException("DSCT0095");
            } 
            catch (Exception e) {
                e.printStackTrace();
                throw new NafinException("SIST0001");
            }
        log.info("getParametrosEPO(S)");
        return parametros;
    }   // END getParametrosEPO
     
     
    /**
     * Metodo replicado de ParametrosDescuentoBean
     * verifico si existe parametrizaci�n del Descuento Automatico
     * Fodea 010-2013 - Reafiliacion Automatica
     * @return
     * @param cc_tipo_factoraje
     * @param ic_moneda
     * @param ic_pyme
     * @param ic_if
     * @param ic_epo
     */
    private boolean existeParametrizacion(String ic_epo, String ic_if, String ic_pyme, String ic_moneda, String cc_tipo_factoraje, AccesoDBOracle con) {
        ResultSet rs = null;
        PreparedStatement ps = null;
        boolean exiteIf = false;
        List lVarBind = new ArrayList();
        log.info("existeParametrizacion (E)");
        try {
            String strQry =
                "SELECT count(1) AS existe_if  " + 
                "  FROM comrel_dscto_aut_pyme   " + 
                " WHERE ic_if = ?               " + 
                "   AND ic_epo = ?              " + 
                "   AND ic_pyme = ?             " +
                "   AND ic_moneda = ?           " + 
                "   AND cc_tipo_factoraje = ?   ";
            lVarBind = new ArrayList();
            lVarBind.add(new Integer(ic_if));
            lVarBind.add(new Integer(ic_epo));
            lVarBind.add(new Integer(ic_pyme));
            lVarBind.add(new Integer(ic_moneda));
            lVarBind.add(cc_tipo_factoraje);
            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();
            log.debug("strQry " + strQry);
            log.debug("lVarBind " + lVarBind);
            if (rs.next() && rs != null) {
                exiteIf = Integer.parseInt(rs.getString("existe_if").trim()) > 0 ? true : false;
            }
            rs.close();
            ps.close();
        } 
        catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            log.error("Error en  existeParametrizacion (S)" + e);
            throw new RuntimeException(e);
        } 
        finally {
            log.info("existeParametrizacion (S)");
        }
        return exiteIf;
    } // End existeParametrizacion
    
    
    /** Replicado de ParametrosDesuentoBean para usar AccesoDBOracle
     * Metodo para copiar la Cuenta CLABE de la PYME en la nueva Cadena Productiva
     * y se registra con estatus Cuenta Correcta
    * Fodea 019-2014
    * @return
    * @param grupo
    */
    private String getcuentaClablePyme(String ic_nafin_electronico, String login, String ic_epo, AccesoDBOracle con  ){
        ResultSet          rs       =  null;
        PreparedStatement ps    =  null;
        PreparedStatement ps1    =  null;
        StringBuffer qrySentencia = new StringBuffer();
        String respuesta ="";
        List lVarBind       = new ArrayList();
        log.info("getcuentaClablePyme (E)");
        String ic_producto_nafin ="",  ic_moneda= "",  ic_bancos_tef ="",
               cg_cuenta ="", df_transferencia ="",
               df_aplicacion="", ic_tipo_cuenta ="", cg_tipo_afiliado ="",  ic_estatus_cecoban ="",
               cg_plaza_swift ="",  cg_sucursal_swift ="";
        try{
           String sSigLlave        =  sLlaveSiguiente(con);  //  ic_cuenta
           //Obtengo los datos de la Cuenta
           qrySentencia = new StringBuffer();
           qrySentencia.append(" select "+
           "  ic_producto_nafin, ic_moneda , ic_bancos_tef,   df_ultima_mod,  "+
           " TO_CHAR (df_transferencia, 'dd/mm/yyyy') as df_transferencia  , "+
           " TO_CHAR ( df_aplicacion, 'dd/mm/yyyy') as df_aplicacion,  "+
           " ic_tipo_cuenta, cg_tipo_afiliado , ic_estatus_cecoban , "+
           " cg_plaza_swift,  cg_sucursal_swift,     "+
           " cg_cuenta  "+
           " from com_cuentas "+
           " where  ic_nafin_electronico = ?  "+
           " and IC_CUENTA  = (select  MAX(IC_CUENTA)   from com_cuentas  "+
           " where  ic_nafin_electronico = ?   and IC_ESTATUS_CECOBAN = ?  )  ");
           lVarBind        = new ArrayList();
           lVarBind.add(ic_nafin_electronico);
           lVarBind.add(ic_nafin_electronico);
           lVarBind.add("99");
           log.debug("qrySentencia.toString() "+qrySentencia.toString());
           log.debug("varBind "+lVarBind);
           ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
           rs = ps.executeQuery();
        if(rs.next()){
               ic_producto_nafin =(rs.getString("ic_producto_nafin")==null?"":rs.getString("ic_producto_nafin"));
               ic_moneda= (rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda"));
               ic_bancos_tef =(rs.getString("ic_bancos_tef")==null?"":rs.getString("ic_bancos_tef"));
               cg_cuenta =(rs.getString("cg_cuenta")==null?"":rs.getString("cg_cuenta"));
               df_transferencia =(rs.getString("df_transferencia")==null?"":rs.getString("df_transferencia"));
               df_aplicacion=(rs.getString("df_aplicacion")==null?"":rs.getString("df_aplicacion"));
               ic_tipo_cuenta =(rs.getString("ic_tipo_cuenta")==null?"":rs.getString("ic_tipo_cuenta"));
               cg_tipo_afiliado =(rs.getString("cg_tipo_afiliado")==null?"":rs.getString("cg_tipo_afiliado"));
               ic_estatus_cecoban =(rs.getString("ic_estatus_cecoban")==null?"":rs.getString("ic_estatus_cecoban"));
               cg_plaza_swift =(rs.getString("cg_plaza_swift")==null?"":rs.getString("cg_plaza_swift"));
               cg_sucursal_swift =(rs.getString("cg_sucursal_swift")==null?"":rs.getString("cg_sucursal_swift"));
           }
           rs.close();
           ps.close();

         if(respuesta.equals("")) {
               qrySentencia = new StringBuffer();
               qrySentencia.append(" INSERT INTO com_cuentas ( "+
                   " ic_cuenta, ic_nafin_electronico, ic_producto_nafin, ic_moneda,  ic_bancos_tef, "+
                   " cg_cuenta, ic_usuario, df_ultima_mod,  df_registro, df_transferencia , "+
                   " df_aplicacion, ic_tipo_cuenta, cg_tipo_afiliado , ic_epo, ic_estatus_cecoban ,"+
                   " cg_plaza_swift,  cg_sucursal_swift  ) "+
                   " VALUES ( "+sSigLlave+" ,"+ ic_nafin_electronico+","+ic_producto_nafin+","+ic_moneda +","+ic_bancos_tef+", '"+
                                   cg_cuenta+"','"+login+"', SYSDATE, SYSDATE, to_date('"+df_transferencia+"','dd/mm/yyyy') , "+
                   "            to_date( '"+df_aplicacion+"' ,'dd/mm/yyyy'), "+ic_tipo_cuenta+",'"+ cg_tipo_afiliado+"',"+ ic_epo+", 99 ,"+
                   "'"+cg_plaza_swift+"','"+cg_sucursal_swift+"') ");
               log.error("qrySentencia  " +qrySentencia);
               ps1 = con.queryPrecompilado(qrySentencia.toString());
               ps1.executeUpdate();
               ps1.close();
         }
     }catch(Exception e){
        e.printStackTrace();
        log.error("Error en  getValidaFisoXGrupoEpos (S)" +e);
        throw new RuntimeException(e);
     }
     log.info("getcuentaClablePyme (S)");
     return respuesta;
    }// END getcuentaClablePyme
    

    /** Metodo replicado de AurorizacionDescuentoBean.java
     * Para usar acceso a Base de datos AccesoDBOracle
     * 
     * */
    public boolean  getOperaIFEPOFISO(String icIF,String icEPO, AccesoDBOracle con) {
        boolean bandera=false;
        String  operaIF="N",operaEPO="N";
        try {
            if(!icIF.equals("")){
                String qrySentenciaIF=  " select CS_OPERA_FIDEICOMISO from comcat_if where ic_if = "+icIF;  
                PreparedStatement psIf = con.queryPrecompilado(qrySentenciaIF);
                 ResultSet  rsIf     = psIf.executeQuery();
                if(rsIf.next())     {
                    operaIF = rsIf.getString(1);
                }
                rsIf.close();
                psIf.close();
            }
            String qrySentenciaEPO= "select CG_VALOR as CS_OPERA_FIDEICOMISO from COM_PARAMETRIZACION_EPO  where ic_epo = "+icEPO+" and CC_PARAMETRO_EPO =  'CS_OPERA_FIDEICOMISO'" ;
            PreparedStatement psEPo = con.queryPrecompilado(qrySentenciaEPO);
            ResultSet  rsEPo     = psEPo.executeQuery();
            if(rsEPo.next())    {
                operaEPO = rsEPo.getString(1);
            }
            rsEPo.close(); 
            psEPo.close(); 
            log.info("ic_epo:"+icEPO+" operaEPO "+operaEPO +"---operaIF--"+operaIF);    
            if(operaEPO.equals("S")   &&  operaIF.equals("S")  )  {
                bandera =true;
            }else  {
                bandera =false;
            }
        } catch (Exception e) {
            throw  new AppException("Ocurrio un error al consultar la BD");
        } 
        return bandera;
    }// end getOperaIFEPOFISO


    /** Metodo replicado de AurorizacionDescuentoBean.java
     * Para usar acceso a Base de datos AccesoDBOracle
     * */
    public String  getOperaFideicomisoPYME(String ic_pyme, AccesoDBOracle con ){
            ResultSet         rs       =  null;
            PreparedStatement ps    =  null;
            StringBuffer strQry = new StringBuffer();
            String operaFide_Pyme ="N";
            log.info("getOperaFideicomisoPYME (E)");
            
            try{
                // Vetifico si la  Pyme  opera Fideicomiso
                strQry = new StringBuffer();
                strQry.append(" SELECT  cs_opera_fideicomiso  "+
                                            " FROM comcat_pyme  "+
                                            " WHERE ic_pyme =  ?  ");
                
                ps = con.queryPrecompilado(strQry.toString());
                ps.setInt(1, Integer.parseInt(ic_pyme));
                rs = ps.executeQuery();
                
                if(rs.next() && rs!=null){
                    operaFide_Pyme= (rs.getString("cs_opera_fideicomiso")==null?"N":rs.getString("cs_opera_fideicomiso"));
                }
                rs.close();
                ps.close();        
            }catch(Exception e){
                e.printStackTrace();
                con.terminaTransaccion(false);
                log.error("Error en  getOperaFideicomisoPYME (S)" +e);
                throw new RuntimeException(e);
            }
            log.info("getOperaFideicomisoPYME (S)");
            return operaFide_Pyme;
        }// end getOperaFideicomisoPYME

        /**
        * Metodo que verifica si la Pyme tiene cuentas
        * @return
        * @param ic_nafin_electronico
        */
        public String  getValidaCcuentaClablePyme(String ic_nafin_electronico, AccesoDBOracle con  ){
           log.info("getValidaCcuentaClablePyme (E)");
         ResultSet           rs       =  null;
         PreparedStatement ps    =  null;
           StringBuffer qrySentencia = new StringBuffer();
           String validaCcuentaClablePyme ="N", validaCcuentaClablePyme_1 ="N", valida = "N";
           StringBuffer qrySentencia1 = new StringBuffer();
           List lVarBind       = new ArrayList();
           List lVarBind1      = new ArrayList();
            try{
               qrySentencia = new StringBuffer();
               qrySentencia.append(" select    count(*)    "+
               " from com_cuentas "+
               " where  ic_nafin_electronico = ?  "+
               " and IC_CUENTA  = (select  MAX(IC_CUENTA)   from com_cuentas  "+
               " where  ic_nafin_electronico = ?   and IC_ESTATUS_CECOBAN = ?  ) ");

               lVarBind        = new ArrayList();
               lVarBind.add(ic_nafin_electronico);
               lVarBind.add(ic_nafin_electronico);
               lVarBind.add("99");

               log.debug("qrySentencia.toString() "+qrySentencia.toString());
               log.debug("varBind "+lVarBind);

               ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
               rs = ps.executeQuery();
            if(rs.next()){
                   if(rs.getInt(1)>0) {
                       validaCcuentaClablePyme="S";
                   }else{
                       qrySentencia1.append(" select   count(*)    "+
                       " from com_cuentas "+
                       " where  ic_nafin_electronico = ?  "+
                       "  and IC_ESTATUS_CECOBAN is null  ");


                       lVarBind1       = new ArrayList();
                       lVarBind1.add(ic_nafin_electronico);
                       log.debug("qrySentencia1.toString() "+qrySentencia1.toString());
                       log.debug("varBind1 "+lVarBind1);

                       PreparedStatement ps1 = con.queryPrecompilado(qrySentencia1.toString(), lVarBind1);
                       ResultSet  rs1 = ps1.executeQuery();
                       if(rs1.next()){
                           if(rs1.getInt(1)>0) {
                               validaCcuentaClablePyme_1="S";
                           }
                       }
                       rs1.close();
                       ps1.close();
                   }
               }
               rs.close();
               ps.close();
               log.debug("validaCcuentaClablePyme "+validaCcuentaClablePyme);
               log.debug("validaCcuentaClablePyme_1 "+validaCcuentaClablePyme_1);
               if("S".equals(validaCcuentaClablePyme) && "N".equals(validaCcuentaClablePyme_1) )  {
                   valida = "S";
               }else if("N".equals(validaCcuentaClablePyme) && "S".equals(validaCcuentaClablePyme_1) )  {
                   valida = "E";  // La Cuenta CLABE de la PYME no tiene estatus de Cuenta Correcta, favor de validarla
               }
               log.debug("valida "+valida);
         }catch(Exception e){
            e.printStackTrace();
               con.terminaTransaccion(false);
               log.error("Error en  getValidaCcuentaClablePyme (S)" +e);
            throw new RuntimeException(e);
         }
         log.info("getValidaCcuentaClablePyme (S)");
         return valida;
        } // end getValidaCcuentaClablePyme
        
        
        
        
        
        public String  getValidaPymeEpoRea(String ic_pyme, String ic_epo, AccesoDBOracle con ){
            log.info("getValidaPymeEpoRea (E)");
          ResultSet           rs       =  null;
          PreparedStatement ps    =  null;
            StringBuffer qrySentencia = new StringBuffer();
            List lVarBind       = new ArrayList();
            String valida = "N";
             try{
                qrySentencia = new StringBuffer();
                qrySentencia.append(" select  count(*) as total    from " +
                " comrel_pyme_epo r , COM_PARAMETRIZACION_EPO  e , comcat_pyme p  "+
                " where  r.cs_aceptacion = ?   "+
                " and r.ic_pyme  =  ? "+
                " and r.ic_epo != ? "+
                " and e.CC_PARAMETRO_EPO = ?   "+
                " and e.CG_VALOR =  ?  "+
                " and r.ic_epo = e.ic_epo "+
                " and r.ic_pyme   = p.ic_pyme  "+
                " and p.CS_OPERA_FIDEICOMISO  = ?   ");
                lVarBind        = new ArrayList();
                lVarBind.add("H");
                lVarBind.add(ic_pyme);
                lVarBind.add(ic_epo);
                lVarBind.add("CS_OPERA_FIDEICOMISO");
                lVarBind.add("S");
                lVarBind.add("S");
                log.debug("qrySentencia.toString() "+qrySentencia.toString());
                log.debug("varBind "+lVarBind);
                ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
                rs = ps.executeQuery();
             if(rs.next()){
                    if(rs.getInt(1)>0) {
                        valida = "S";
                    }
                }
                rs.close();
                ps.close();
          }catch(Exception e){
             e.printStackTrace();
                con.terminaTransaccion(false);
                log.error("Error en  getValidaPymeEpoRea (S)" +e);
             throw new RuntimeException(e);
          }
          log.info("getValidaPymeEpoRea (S)");
          return valida;
        }// end getValidaPymeEpoRea        



    /**Metodo replicado de DispersionBean 
     * para usar AccesoDBOracle
     * */
    public String sLlaveSiguiente(AccesoDBOracle con) throws NafinException{
        ResultSet       rs              = null;
        String          qrySentencia    = "";
        String          sResultado      = "";
        try {
            qrySentencia = " SELECT  NVL(MAX(ic_cuenta),0)+1 FROM com_cuentas ";
            System.out.print("DispersionBean :: sLlaveSiguiente :: Query "+qrySentencia);
            rs = con.queryDB(qrySentencia);
            while(rs.next()){
            sResultado = rs.getString(1);
            }
            return sResultado;
        
        }
        catch(Exception e){
            throw new NafinException("SIST0001");
        }
    }/*Fin del M�todo sLlaveSiguiente*/



}
