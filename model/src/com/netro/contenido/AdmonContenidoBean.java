package com.netro.contenido;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "AdmonContenidoEJB" , mappedName = "AdmonContenidoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class AdmonContenidoBean implements AdmonContenido {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(AdmonContenidoBean.class);
	
	
	private boolean validateIdTextLogo () throws AppException{
		log.info("validateIdTextLogo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		boolean bExisteNull = false;
		
		try{
			con.conexionDB();
			String cveTextLog = "0";
			
			query = "SELECT NVL (MIN (ic_texto_logo), 0) as cveTextLog " +
					"  FROM admcon_texto_logo " +
					" WHERE cg_texto IS NULL ";
					
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
				cveTextLog = rs.getString("cveTextLog");
			}
			rs.close();
			ps.close();
			
			if(!"0".equals(cveTextLog)){
				bExisteNull = true;
			}

			return bExisteNull;
		}catch(Throwable t){
			log.info("validateIdTextLogo(Error)");
			throw new AppException("Error al validar si ya existe id de Texto de Logo", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("validateIdTextLogo(S)");
		}
	}
	
	private String getIdTextLogo () throws AppException{
		log.info("getIdTextLogo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		
		try{
			con.conexionDB();
			String cveTextLog = "0";
			
			query = "SELECT NVL (MIN (ic_texto_logo), 0) as cveTextLog " +
					"  FROM admcon_texto_logo " +
					" WHERE cg_texto IS NULL ";
					
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
				cveTextLog = rs.getString("cveTextLog");
			}
			rs.close();
			ps.close();
			
			if("0".equals(cveTextLog)){
				query = "SELECT (MAX (nvl(ic_texto_logo,0)) + 1 ) as cveTextLog " +
						"  FROM admcon_texto_logo " +
						" WHERE cg_texto IS NOT NULL ";
				
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				
				if(rs!=null && rs.next()){
					cveTextLog = rs.getString("cveTextLog");
				}else{
					cveTextLog = "1";
				}
				rs.close();
				ps.close();

			}

			return cveTextLog;
		}catch(Throwable t){
			log.info("getIdTextLogo(Error)");
			throw new AppException("Error al obtener clave id de Texto de Logo", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getIdTextLogo(S)");
		}
	}
	
	private String getIcOpcionMenuSeccion(String cveSeccion) throws AppException{
		log.info("getIcOpcionMenuSeccion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		
		try{
			con.conexionDB();
			String cveOpcion = "1";
			
			query = "SELECT (MAX (nvl(ic_opcion,0)) + 1) as cveOpcion" +
				"  FROM admcon_seccion_cont " +
				" WHERE ic_seccion = ? ";
			
			ps = con.queryPrecompilado(query);
			ps.setInt(1,Integer.parseInt(cveSeccion));
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
				cveOpcion = rs.getString("cveOpcion");
			}
			rs.close();
			ps.close();
			
			return cveOpcion;
		}catch(Throwable t){
			log.info("getIcOpcionMenuSeccion(Error)");
			throw new AppException("Error al obtener clave de Opcion(Menu)", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getIcOpcionMenuSeccion(S)");
		}
	}
	
	private String getIdSeccionContDet (AccesoDB con) throws AppException{
		log.info("getIdSeccionContDet(E)");
		//AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		
		try{
			//con.conexionDB();
			String cveSecContDet = "1";
			
			query = "SELECT (MAX (nvl(ic_seccion_cont_det,0)) + 1)  as cveSecContDet " +
					"  FROM admcon_seccion_cont_det ";

					
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
				cveSecContDet = rs.getString("cveSecContDet");
			}
			rs.close();
			ps.close();
			
			return cveSecContDet;
		}catch(Throwable t){
			log.info("getIdSeccionContDet(Error)");
			throw new AppException("Error al obtener clave id del detallde de la Seccion", t);
		}finally{
			if(con.hayConexionAbierta()){
				//con.cierraConexionDB();
			}
			log.info("getIdSeccionContDet(S)");
		}
	}
	
	private String getOrdenSeccionContDet (String cveSeccion, String cveMenu, AccesoDB con) throws AppException{
		log.info("getOrdenSeccionContDet(E)");
		//AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		
		try{
			//con.conexionDB();
			String igOrden = "1";
			
			query = "SELECT (MAX(nvl(ig_orden,0)) + 1) as orden " +
					"  FROM admcon_seccion_cont_det " +
					" WHERE ic_seccion = ? AND ic_opcion = ? ";

			ps = con.queryPrecompilado(query);
			
			ps.setInt(1,Integer.parseInt(cveSeccion));
			ps.setInt(2,Integer.parseInt(cveMenu));
			
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
				igOrden = rs.getString("orden")==null?"1":rs.getString("orden");
			}
			rs.close();
			ps.close();
			
			return igOrden;
		}catch(Throwable t){
			log.info("getOrdenSeccionContDet(Error)");
			throw new AppException("Error al obtener orden id del detallde de la Seccion", t);
		}finally{
			if(con.hayConexionAbierta()){
				//con.cierraConexionDB();
			}
			log.info("getOrdenSeccionContDet(S)");
		}
	}
	
	public List getDataTextLogo()throws AppException{
		log.info("getDataTextLogo(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDataTextLog = new ArrayList();
		String query = "";
		try{
			con.conexionDB();
			
			query = "SELECT   ic_texto_logo, cg_texto " +
					"    FROM admcon_texto_logo " +
					"   WHERE cg_texto IS NOT NULL " +
					"ORDER BY ic_texto_logo ";
					
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				String cveTexto = rs.getString("ic_texto_logo");
				String cgTexto = rs.getString("cg_texto");
				
				mp.put("CVETEXTLOGO",cveTexto);
				mp.put("TEXTOLOGO",cgTexto);
				lstDataTextLog.add(mp);
				
			}
			rs.close();
			ps.close();

		return lstDataTextLog;
		}catch(Throwable t){
			log.info("getDataTextLogo(Error)");
			throw new AppException("Error al obtner info Texto de Logos",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getDataTextLogo(S)");
		}
	}
	
	public void saveTextLogo(List lstCveTextLogos) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryIns = "";
		String queryUpd = "";
		String cveTextLogo = "";
		String textoLogo = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();
			
			//insercion nuevo registro
			queryIns = "INSERT INTO admcon_texto_logo " +
					"     VALUES (?, ?) ";
			
			//actualizacion de registros
			
			queryUpd = "UPDATE admcon_texto_logo " +
					"   SET cg_texto = ? " +
					" WHERE ic_texto_logo = ? ";

			
			if(lstCveTextLogos!=null && lstCveTextLogos.size()>0){
				for(int x=0; x<lstCveTextLogos.size(); x++){
					HashMap mp = new HashMap();
					mp = (HashMap)lstCveTextLogos.get(x);
					cveTextLogo = (String)mp.get("CVETEXTLOGO");
					textoLogo = (String)mp.get("TEXTOLOGO");
					
					if("0".equals(cveTextLogo) && !validateIdTextLogo()){
						cveTextLogo = getIdTextLogo();
						ps = con.queryPrecompilado(queryIns);
						ps.setInt(1, Integer.parseInt(cveTextLogo));
						ps.setString(2, textoLogo);
						ps.executeUpdate();
						ps.close();
					
					}else{
						if(validateIdTextLogo())
							cveTextLogo = getIdTextLogo();
						ps = con.queryPrecompilado(queryUpd);
						ps.setString(1, textoLogo);
						ps.setInt(2, Integer.parseInt(cveTextLogo));
						ps.executeUpdate();
						ps.close();
					}
				}
			}
			
			
		}catch(Throwable t){
			bOk = false;
			throw new AppException("Error al guardar informacion de Texto de Logos", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
	}
	
	
	public void deleteTextLogo(List lstCveTextLogos) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryUpd = "";
		String cveTextLogo = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();
			
			
			//eliminacion logica de registros
			
			queryUpd = "UPDATE admcon_texto_logo " +
					"   SET cg_texto = null " +
					" WHERE ic_texto_logo = ? ";

			
			if(lstCveTextLogos!=null && lstCveTextLogos.size()>0){
				for(int x=0; x<lstCveTextLogos.size(); x++){
					HashMap mp = new HashMap();
					mp = (HashMap)lstCveTextLogos.get(x);
					cveTextLogo = (String)mp.get("CVETEXTLOGO");
					
					ps = con.queryPrecompilado(queryUpd);
					ps.setInt(1, Integer.parseInt(cveTextLogo));
					ps.executeUpdate();
					ps.close();

				}
			}
			
			
		}catch(Throwable t){
			bOk = false;
			throw new AppException("Error al eliminar informacion de Texto de Logos", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
	}
	
	public List getDataSecciones()throws AppException{
		log.info("getDataSecciones(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDataSecciones = new ArrayList();
		String query = "";
		try{
			con.conexionDB();
			
			query = "SELECT   ic_seccion as seccion, cg_nombre as nombre, " +
					"         TO_CHAR (df_modificacion, 'DD/MM/YYYY HH24:MI:SS') as fecmodif " +
					"    FROM admcon_seccion " +
					"ORDER BY ic_seccion ";

					
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				String cveSeccion = rs.getString("seccion");
				String nombreSeccion = rs.getString("nombre");
				String fecModif = rs.getString("fecmodif");
				
				mp.put("CVESECCION",cveSeccion);
				mp.put("NOMBRESECCION",nombreSeccion);
				mp.put("FECMODIF",fecModif);
				lstDataSecciones.add(mp);
				
			}
			rs.close();
			ps.close();

		return lstDataSecciones;
		}catch(Throwable t){
			log.info("getDataSecciones(Error)");
			throw new AppException("Error al obtner info Secciones",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getDataSecciones(S)");
		}
	}
	
	public void saveTextoSecciones(List lstCveTextSeccion) throws AppException{
		log.info("saveTextoSecciones(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryUpd = "";
		String cveSeccion = "";
		String nombreSeccion = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();
			
			//actualizacion de registros
			
			queryUpd = "UPDATE admcon_seccion " +
					"   SET cg_nombre = ?, df_modificacion = sysdate " +
					" WHERE ic_seccion = ? ";

			
			if(lstCveTextSeccion!=null && lstCveTextSeccion.size()>0){
				for(int x=0; x<lstCveTextSeccion.size(); x++){
					HashMap mp = new HashMap();
					mp = (HashMap)lstCveTextSeccion.get(x);
					cveSeccion = (String)mp.get("CVESECCION");
					nombreSeccion = (String)mp.get("NOMBRESECCION");
					
					ps = con.queryPrecompilado(queryUpd);
					ps.setString(1, nombreSeccion);
					ps.setInt(2, Integer.parseInt(cveSeccion));
					ps.executeUpdate();
					ps.close();
				}
			}
			
			
		}catch(Throwable t){
			log.info("saveTextoSecciones(Error)");
			bOk = false;
			throw new AppException("Error al guardar nombres de las Secciones", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("saveTextoSecciones(S)");
		}
	}
	
	
	public List getDataMenus(String cveSeccion, String cveMenu)throws AppException{
		log.info("getDataMenus(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDataSecciones = new ArrayList();
		StringBuffer query = new StringBuffer();
		try{
			con.conexionDB();
			
			query.append("SELECT  a.ic_seccion, a.cg_nombre, b.ic_opcion, b.cg_descripcion, " +
							"         b.cg_titulo, TO_CHAR (b.df_modificacion, 'dd/mm/yyyy HH24:MI:SS') fecmod, " +
							"			 b.cs_nuevo " +
							"    FROM admcon_seccion a, admcon_seccion_cont b " +
							"   WHERE a.ic_seccion = b.ic_seccion ");
			
			if(cveSeccion!=null && !cveSeccion.equals(""))
				query.append("	AND a.ic_seccion = ? ");			
			if(cveMenu!=null && !cveMenu.equals(""))
				query.append("	AND b.ic_opcion = ? ");
				
			query.append("	ORDER BY a.ic_seccion, b.ic_opcion ");

			ps = con.queryPrecompilado(query.toString());
			int p = 0;
			if(cveSeccion!=null && !cveSeccion.equals(""))
				ps.setInt(++p, Integer.parseInt(cveSeccion));
			
			if(cveMenu!=null && !cveMenu.equals(""))
				ps.setInt(++p, Integer.parseInt(cveMenu));
			
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				String icSeccion = rs.getString("ic_seccion");
				String nombreSeccion = rs.getString("cg_nombre");
				String opcion = rs.getString("ic_opcion");
				String descOpcion = rs.getString("cg_descripcion");
				String titulo = rs.getString("cg_titulo");
				String nuevo = rs.getString("cs_nuevo");
				String fecModif = rs.getString("fecmod");
				
				
				mp.put("CVESECCION",icSeccion);
				mp.put("NOMBRESECCION",nombreSeccion);
				mp.put("OPCION",opcion);
				mp.put("DESCOPCION",descOpcion);
				mp.put("TITULO",titulo);
				mp.put("NUEVO",nuevo);
				mp.put("FECMODIF",fecModif);

				lstDataSecciones.add(mp);
				
			}
			rs.close();
			ps.close();

		return lstDataSecciones;
		}catch(Throwable t){
			log.info("getDataMenus(Error)");
			throw new AppException("Error al obtener info del contenido de Menus",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getDataMenus(S)");
		}
	}
	
	public void saveTextoMenusSeccion(List lstCveTextMenus) throws AppException{
		log.info("saveTextoMenusSeccion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryUpd = "";
		String cveSeccion = "";
		String cveMenu = "";
		String descOpcion = "";
		String texoTitulo = "";
		String csNuevo = "";
		
		boolean bOk = true;
		
		try{
			con.conexionDB();
			
			//actualizacion de registros
			
			queryUpd = "UPDATE admcon_seccion_cont " +
						"   SET cg_descripcion = ?, " +
						"       cg_titulo = ?, " +
						"       cs_nuevo = ?, " +
						"       df_modificacion = SYSDATE " +
						" WHERE ic_seccion = ? AND ic_opcion = ? ";

			
			if(lstCveTextMenus!=null && lstCveTextMenus.size()>0){
				for(int x=0; x<lstCveTextMenus.size(); x++){
					HashMap mp = new HashMap();
					mp = (HashMap)lstCveTextMenus.get(x);
					cveSeccion = (String)mp.get("CVESECCION");
					cveMenu = (String)mp.get("OPCION");
					descOpcion = (String)mp.get("DESCOPCION");
					texoTitulo = (String)mp.get("TITULO");
					csNuevo = (String)mp.get("NUEVO");
					
					ps = con.queryPrecompilado(queryUpd);
					ps.setString(1, descOpcion);
					ps.setString(2, texoTitulo);
					ps.setString(3, csNuevo);
					ps.setInt(4, Integer.parseInt(cveSeccion));
					ps.setInt(5, Integer.parseInt(cveMenu));
					ps.executeUpdate();
					ps.close();
				}
			}
			
			
		}catch(Throwable t){
			log.info("saveTextoMenusSeccion(Error)");
			bOk = false;
			throw new AppException("Error al guardar textos del contenido de Menus", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("saveTextoMenusSeccion(S)");
		}
	}
	
	
	public void deleteMenusSeccion(List lstCveTextMenus) throws AppException{
		log.info("deleteMenusSeccion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryDelDetalle = "";
		String queryDelMenu = "";
		String cveSeccion = "";
		String cveMenu = "";
		
		boolean bOk = true;
		
		try{
			con.conexionDB();
			
			//actualizacion de registros
			queryDelDetalle = " DELETE admcon_seccion_cont_det " +
						" WHERE ic_seccion = ? AND ic_opcion = ? ";
						
			queryDelMenu = "DELETE admcon_seccion_cont " +
						" WHERE ic_seccion = ? AND ic_opcion = ? ";

			if(lstCveTextMenus!=null && lstCveTextMenus.size()>0){
				for(int x=0; x<lstCveTextMenus.size(); x++){
					HashMap mp = new HashMap();
					mp = (HashMap)lstCveTextMenus.get(x);
					cveSeccion = (String)mp.get("CVESECCION");
					cveMenu = (String)mp.get("OPCION");

					
					ps = con.queryPrecompilado(queryDelDetalle);
					ps.setInt(1, Integer.parseInt(cveSeccion));
					ps.setInt(2, Integer.parseInt(cveMenu));
					ps.executeUpdate();
					ps.close();
					
					ps = con.queryPrecompilado(queryDelMenu);
					ps.setInt(1, Integer.parseInt(cveSeccion));
					ps.setInt(2, Integer.parseInt(cveMenu));
					ps.executeUpdate();
					ps.close();
					
				}
			}
			
			
		}catch(Throwable t){
			log.info("deleteMenusSeccion(Error)");
			bOk = false;
			throw new AppException("Error al eliminar menus del contenido de Secciones", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("deleteMenusSeccion(S)");
		}
	}
	
	
	public void addNewMenuSeccion(HashMap hmDataMenuSecc) throws AppException{
		log.info("addNewMenuSeccion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryIns = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();

			//actualizacion de registros
			
			String cveSeccion = (String)hmDataMenuSecc.get("CVESECCION");
			String nombreMenu = (String)hmDataMenuSecc.get("NOMBREMENU");
			String textoTitulo = (String)hmDataMenuSecc.get("TEXTOTITULO");
			String numVinetas = (String)hmDataMenuSecc.get("NUMVINETAS");
			String csNuevo = (String)hmDataMenuSecc.get("CSNUEVO");
			
			String icOpcion = getIcOpcionMenuSeccion(cveSeccion);
			
			queryIns = "INSERT INTO admcon_seccion_cont " +
						"            (ic_seccion, ic_opcion, cg_descripcion, cg_titulo, cs_nuevo " +
						"            ) " +
						"     VALUES (?, ?, ?, ?, ? " +
						"            ) ";


			ps = con.queryPrecompilado(queryIns);
			ps.setInt(1, Integer.parseInt(cveSeccion));
			ps.setInt(2, Integer.parseInt(icOpcion));
			ps.setString(3, nombreMenu);
			ps.setString(4, textoTitulo);
			ps.setString(5, csNuevo);
			ps.executeUpdate();
			ps.close();
			
			//se inserta detalle del menu seccion
			queryIns = "INSERT INTO admcon_seccion_cont_det " +
						"            (ic_seccion_cont_det, ic_seccion, ic_opcion, ig_orden, " +
						"             cg_descripcion " +
						"            ) " +
						"     VALUES (?, ?, ?, ?, " +
						"             ? " +
						"            )  ";
			int iVineta = Integer.parseInt(numVinetas);
			
			for(int x=0; x<iVineta; x++){
				log.debug("cveSeccion=="+cveSeccion);
				log.debug("icOpcion=="+icOpcion);
				String textoVineta = (String)hmDataMenuSecc.get("NEWVINETA"+x);
				String cveSecContDet = getIdSeccionContDet(con);
				String igOrden = getOrdenSeccionContDet(cveSeccion,icOpcion,con);

				log.debug("igOrden=="+igOrden);
				ps = con.queryPrecompilado(queryIns);
				ps.setInt(1, Integer.parseInt(cveSecContDet));
				ps.setInt(2, Integer.parseInt(cveSeccion));
				ps.setInt(3, Integer.parseInt(icOpcion));
				ps.setInt(4, Integer.parseInt(igOrden));
				ps.setString(5, textoVineta);
				ps.executeUpdate();
				ps.close();
			}

			
		}catch(Throwable t){
			log.info("addNewMenuSeccion(Error)");
			bOk = false;
			throw new AppException("Error al insertar informacion de nueva vineta", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("addNewMenuSeccion(S)");
		}
	}
	
	
	public List getDataVinetas(String cveSeccion, String cveMenu)throws AppException{
		log.info("getDataVinetas(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDataVinetas = new ArrayList();
		StringBuffer query = new StringBuffer();
		try{
			con.conexionDB();
			
			query.append("SELECT   c.ic_seccion_cont_det ic_seccion_cont_det, c.ig_orden ig_orden, c.cg_descripcion cg_descripcion " +
							"    FROM admcon_seccion a, admcon_seccion_cont b, admcon_seccion_cont_det c " +
							"   WHERE a.ic_seccion = b.ic_seccion " +
							"     AND b.ic_seccion = c.ic_seccion " +
							"     AND b.ic_opcion = c.ic_opcion " +
							"     AND c.ic_seccion = ? " +
							"     AND c.ic_opcion = ? " +
							"ORDER BY c.ig_orden ");

			int p = 0;
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(++p, Integer.parseInt(cveSeccion));
			ps.setInt(++p, Integer.parseInt(cveMenu));
			
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				String vCveSeccContDet = rs.getString("ic_seccion_cont_det");
				String vOrden = rs.getString("ig_orden");
				String vDescripcion = rs.getString("cg_descripcion");
				
				mp.put("CEVESECCONTDET",vCveSeccContDet);
				mp.put("IGORDEN",vOrden);
				mp.put("DESCRIPCION",vDescripcion);

				lstDataVinetas.add(mp);
				
			}
			rs.close();
			ps.close();

		return lstDataVinetas;
		}catch(Throwable t){
			log.info("getDataVinetas(Error)");
			throw new AppException("Error al obtener info de vi�etas",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getDataVinetas(S)");
		}
	}
	
	public void addMenuVinetas(HashMap hmDatosVineta) throws AppException{
		log.info("addMenuVinetas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryIns = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();

			//actualizacion de registros
			
			String cveSeccion = (String)hmDatosVineta.get("CVESECCION");
			String cveOpcion = (String)hmDatosVineta.get("CVEOPCION");
			String textoVineta = (String)hmDatosVineta.get("TEXTVINETA");
			String cveSecContDet = getIdSeccionContDet(con);
			String igOrden = getOrdenSeccionContDet(cveSeccion,cveOpcion, con);
			
			
			queryIns = "INSERT INTO admcon_seccion_cont_det " +
					"            (ic_seccion_cont_det, ic_seccion, ic_opcion, ig_orden, " +
					"             cg_descripcion " +
					"            ) " +
					"     VALUES (?, ?, ?, ?, " +
					"             ? " +
					"            )  ";


			ps = con.queryPrecompilado(queryIns);
			ps.setInt(1, Integer.parseInt(cveSecContDet));
			ps.setInt(2, Integer.parseInt(cveSeccion));
			ps.setInt(3, Integer.parseInt(cveOpcion));
			ps.setInt(4, Integer.parseInt(igOrden));
			ps.setString(5, textoVineta);
			ps.executeUpdate();
			ps.close();

			
		}catch(Throwable t){
			log.info("addMenuVinetas(Error)");
			bOk = false;
			throw new AppException("Error al insertar informacion de nueva vineta", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("addMenuVinetas(S)");
		}
	}
	
	public void modifyMenuVinetas(String cveSecContDet, String textoVineta) throws AppException{
		log.info("modifyMenuVinetas(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryUpd = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();

			//actualizacion de registros
			
			queryUpd = "UPDATE admcon_seccion_cont_det " +
						"   SET cg_descripcion = ? " +
						" WHERE ic_seccion_cont_det = ? ";

			ps = con.queryPrecompilado(queryUpd);
			ps.setString(1, textoVineta);
			ps.setInt(2, Integer.parseInt(cveSecContDet));
			ps.executeUpdate();
			ps.close();

			
		}catch(Throwable t){
			log.info("modifyMenuVinetas(Error)");
			bOk = false;
			throw new AppException("Error al modificar informacion de la Vineta", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("modifyMenuVinetas(S)");
		}
	}
	
	public void deleteVineta(List lstCveDataVineta) throws AppException{
		log.info("deleteVineta(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryDel = "";
		String cveSeccionDet = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();
			
			
			//eliminacion de registros
			
			queryDel = "DELETE admcon_seccion_cont_det " +
						"WHERE ic_seccion_cont_det = ? ";

			
			if(lstCveDataVineta!=null && lstCveDataVineta.size()>0){
				for(int x=0; x<lstCveDataVineta.size(); x++){
					
					cveSeccionDet = (String)lstCveDataVineta.get(x);
					
					ps = con.queryPrecompilado(queryDel);
					ps.setInt(1, Integer.parseInt(cveSeccionDet));
					ps.executeUpdate();
					ps.close();

				}
			}
			
			
		}catch(Throwable t){
			log.info("deleteVineta(Error)");
			bOk = false;
			throw new AppException("Error al eliminar Vinetas", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("deleteVineta(S)");
		}
	}
	
	
	public List getDataLinks()throws AppException{
		log.info("getDataLinks(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDataLinks = new ArrayList();
		String query = "";
		try{
			con.conexionDB();
			
			query = "SELECT   ic_link, cg_nombre, cg_url " +
						"    FROM admcon_link " +
						"ORDER BY ic_link ";

					
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				String cveLink = rs.getString("ic_link");
				String nombreLink = rs.getString("cg_nombre");
				String urlLink = rs.getString("cg_url");
				
				mp.put("CVELINK",cveLink);
				mp.put("NOMBRELINK",nombreLink);
				mp.put("URLLINK",urlLink);
				lstDataLinks.add(mp);
				
			}
			rs.close();
			ps.close();

		return lstDataLinks;
		}catch(Throwable t){
			log.info("getDataLinks(Error)");
			throw new AppException("Error al obtner info Links",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getDataLinks(S)");
		}
	}
	
	public void saveDataLinks(List lstDataLinks) throws AppException{
		log.info("saveDataLinks(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryUpd = "";
		String cveLink = "";
		String nombreLink = "";
		String urlLink = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();
			
			//actualizacion de registros
			
			queryUpd = "UPDATE admcon_link " +
						"   SET cg_nombre = ?, " +
						"       cg_url = ? " +
						" WHERE ic_link = ? ";

			
			if(lstDataLinks!=null && lstDataLinks.size()>0){
				for(int x=0; x<lstDataLinks.size(); x++){
					HashMap mp = new HashMap();
					mp = (HashMap)lstDataLinks.get(x);
					cveLink = (String)mp.get("CVELINK");
					nombreLink = (String)mp.get("NOMBRELINK");
					urlLink = (String)mp.get("URLLINK");
					
					ps = con.queryPrecompilado(queryUpd);
					ps.setString(1, nombreLink);
					ps.setString(2, urlLink);
					ps.setInt(3, Integer.parseInt(cveLink));
					ps.executeUpdate();
					ps.close();
				}
			}
			
			
		}catch(Throwable t){
			log.info("saveTextoSecciones(Error)");
			bOk = false;
			throw new AppException("Error al guardar nombres de las Secciones", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("saveTextoSecciones(S)");
		}
	}
	
	public List getDataBanners(String cveBanner)throws AppException{
		log.info("getDataBanners(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDataBanners = new ArrayList();
		String query = "";
		try{
			con.conexionDB();
			
			query = "SELECT   a.ic_banner cvebanner, a.cg_nombre nombre, " +
				"         a.cs_activo activo, " +
				"         TO_CHAR (a.df_modificacion, 'dd/mm/yyyy HH24:MI:SS') fecmod " +
				"    FROM admcon_banner a ";
			if(cveBanner!=null && !cveBanner.equals(""))
				query += "		WHERE a.ic_banner = ? ";
			
			query += " ORDER BY a.cg_nombre ";


					
			ps = con.queryPrecompilado(query);
			if(cveBanner!=null && !cveBanner.equals(""))
				ps.setInt(1,Integer.parseInt(cveBanner));
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				String cvebanner = rs.getString("cvebanner");
				String nombre = rs.getString("nombre");
				//String descripcion = rs.getString("descripcion");
				String activo = rs.getString("activo");
				String fecmod = rs.getString("fecmod");
				
				mp.put("CVEBANNER",cvebanner);
				mp.put("NOMBREBANN",nombre);
				//mp.put("DESCBANN",descripcion);
				mp.put("ACTIVOBANN",activo);
				mp.put("FECMODBANN",fecmod);
				lstDataBanners.add(mp);
				
			}
			rs.close();
			ps.close();

		return lstDataBanners;
		}catch(Throwable t){
			log.info("getDataBanners(Error)");
			throw new AppException("Error al obtner info de Banners",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getDataBanners(S)");
		}
	}
	
	
	public List getDataDetBanners(String cveBanner)throws AppException{
		log.info("getDataDetBanners(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstDataDetBanner = new ArrayList();
		String query = "";
		try{
			con.conexionDB();
			
			query = "SELECT   b.ic_banner, b.cg_titulo, b.cg_texto, b.ig_posicion " +
					"    FROM admcon_banner a, admcon_banner_det b " +
					"   WHERE a.ic_banner = b.ic_banner ";
			if(cveBanner!=null && !"".equals(cveBanner))
				query += "  AND b.ic_banner = ? ";
			
			query +=	"ORDER BY b.ig_posicion ";

					
			ps = con.queryPrecompilado(query);
			if(cveBanner!=null && !"".equals(cveBanner));
				ps.setInt(1,Integer.parseInt(cveBanner));
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				HashMap mp = new HashMap();
				String icBanner = rs.getString("ic_banner");
				String cgTitulo = rs.getString("cg_titulo");
				String cgTexto = rs.getString("cg_texto");
				String igPosicion = rs.getString("ig_posicion");
				
				mp.put("CVEBANNER",icBanner);
				mp.put("TITULOBANNEDIT",cgTitulo);
				mp.put("TEXTOBANNEDIT",cgTexto);
				mp.put("POSICIONBANNEDIT",igPosicion);
				lstDataDetBanner.add(mp);
				
			}
			rs.close();
			ps.close();

		return lstDataDetBanner;
		}catch(Throwable t){
			log.info("getDataDetBanners(Error)");
			throw new AppException("Error al obtner el detalle de la info del Banner",t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getDataDetBanners(S)");
		}
	}
	
	public void modifyTextBanners(List lstDatosBann) throws AppException{
		log.info("modifyTextBanners(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryUpd = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();

			queryUpd = "UPDATE admcon_banner " +
						"   SET cg_nombre = ?, " +
						//"       cg_descripcion = ?, " +
						"       df_modificacion = sysdate " +
						" WHERE ic_banner = ? ";
			
			if(lstDatosBann!=null && lstDatosBann.size()>0){
			//actualizacion de registros
				for(int x=0; x<lstDatosBann.size(); x++){
					HashMap hmDatosBann = new HashMap();
					hmDatosBann = (HashMap)lstDatosBann.get(x);
					String cveBanner = (String)hmDatosBann.get("CVEBANNER");
					String nombreBann = (String)hmDatosBann.get("NOMBREBANN");
					//String descbann = (String)hmDatosBann.get("DESCBANN");
	
					ps = con.queryPrecompilado(queryUpd);
					ps.setString(1, nombreBann);
					//ps.setString(2, descbann);
					ps.setInt(2, Integer.parseInt(cveBanner));
					ps.executeUpdate();
					ps.close();
				}
			
			}

			
		}catch(Throwable t){
			log.info("modifyTextBanners(Error)");
			bOk = false;
			throw new AppException("Error al modificar informacion del banner", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("modifyTextBanners(S)");
		}
	}
	
	
	public void deleteBanners(List lstCveBanners) throws AppException{
		log.info("deleteBanners(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryDelDetalle = "";
		String queryDelBanner = "";
		String cveBanner = "";
		
		boolean bOk = true;
		
		try{
			con.conexionDB();
			
			//actualizacion de registros
			queryDelDetalle = " DELETE admcon_banner_det " +
						" WHERE ic_banner = ? ";
						
			queryDelBanner = " DELETE admcon_banner " +
						" WHERE ic_banner = ? ";

			if(lstCveBanners!=null && lstCveBanners.size()>0){
				for(int x=0; x<lstCveBanners.size(); x++){
					
					cveBanner = (String)lstCveBanners.get(x);
					
					ps = con.queryPrecompilado(queryDelDetalle);
					ps.setInt(1, Integer.parseInt(cveBanner));
					ps.executeUpdate();
					ps.close();
					
					ps = con.queryPrecompilado(queryDelBanner);
					ps.setInt(1, Integer.parseInt(cveBanner));
					ps.executeUpdate();
					ps.close();
					
				}
			}
			
			
		}catch(Throwable t){
			log.info("deleteBanners(Error)");
			bOk = false;
			throw new AppException("Error al eliminar banners", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("deleteBanners(S)");
		}
	}
	
	public void invertirEstatusBanners(List lstDataBanners) throws AppException{
		log.info("invertirEstatusBanners(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String query = "";
		String cveBanner = "";
		String csActivo = "";
				
		boolean bOk = true;
		
		try{
			con.conexionDB();
			
			//actualizacion de registros
						
			query = "UPDATE admcon_banner " +
					"   SET cs_activo = ?, " +
					"       df_modificacion = sysdate " +
					" WHERE ic_banner = ?  ";


			if(lstDataBanners!=null && lstDataBanners.size()>0){
				for(int x=0; x<lstDataBanners.size(); x++){
					HashMap mp = new HashMap();
					mp = (HashMap)lstDataBanners.get(x);
					cveBanner = (String)mp.get("CVEBANNER");
					csActivo = (String)mp.get("ACTIVOBANN");

					
					ps = con.queryPrecompilado(query);
					ps.setString(1,  "N".equals(csActivo)?"S":"N" );
					ps.setInt(2, Integer.parseInt(cveBanner));
					ps.executeUpdate();
					ps.close();
					
				}
			}
			
			
		}catch(Throwable t){
			log.info("invertirEstatusBanners(Error)");
			bOk = false;
			throw new AppException("Error al cambiar estaus de banners", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("invertirEstatusBanners(S)");
		}
	}
	
	
	public void deleteDetBanner(String cveBanner, List lstCveDetBanner) throws AppException{
		log.info("deleteDetBanner(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryDel = "";
		String cvePosicion = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();

			//eliminacion de registros
			
			queryDel = " DELETE  admcon_banner_det " +
						" WHERE ic_banner = ? AND ig_posicion = ? ";

			
			if(lstCveDetBanner!=null && lstCveDetBanner.size()>0){
				for(int x=0; x<lstCveDetBanner.size(); x++){
					
					cvePosicion = (String)lstCveDetBanner.get(x);
					
					ps = con.queryPrecompilado(queryDel);
					ps.setInt(1, Integer.parseInt(cveBanner));
					ps.setInt(2, Integer.parseInt(cvePosicion));
					ps.executeUpdate();
					ps.close();

				}
			}
			
			
		}catch(Throwable t){
			log.info("deleteDetBanner(Error)");
			bOk = false;
			throw new AppException("Error al eliminar el detalle del Banner", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("deleteDetBanner(S)");
		}
	}
	
	public void addNewDetBanner(HashMap hmDatosDetBann) throws AppException{
		log.info("addNewDetBanner(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryIns = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();

			//actualizacion de registros
			
			String cveBanner = (String)hmDatosDetBann.get("CVEBANNER");
			String textoTitulo = (String)hmDatosDetBann.get("TEXTTITULO");
			String textoTexto = (String)hmDatosDetBann.get("TEXTTEXTO");
			String posicion = (String)hmDatosDetBann.get("POSICION");
			
			
			queryIns = "INSERT INTO admcon_banner_det " +
				"            (ic_banner, ig_posicion, cg_titulo, cg_texto " +
				"            ) " +
				"     VALUES (?, ?, ?, ? " +
				"            ) ";



			ps = con.queryPrecompilado(queryIns);
			ps.setInt(1, Integer.parseInt(cveBanner));
			ps.setInt(2, Integer.parseInt(posicion));
			ps.setString(3, textoTitulo);
			ps.setString(4, textoTexto);
			ps.executeUpdate();
			ps.close();

			
		}catch(Throwable t){
			log.info("addNewDetBanner(Error)");
			bOk = false;
			throw new AppException("Error al insertar informacion de detalle del banner", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("addNewDetBanner(S)");
		}
	}
	
	public List getCboPosicion(String cveBanner) throws AppException{
		log.info("getCboPosicion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		List lstCboPosicion = new ArrayList();
		HashMap mp = new HashMap();
		
		try{
			con.conexionDB();
			String maxPosicion = "0";
			
			query = "SELECT MAX (ig_posicion) posicion " +
					"  FROM admcon_banner_det " +
					" WHERE ic_banner = ? ";

			
			ps = con.queryPrecompilado(query);
			ps.setInt(1,Integer.parseInt(cveBanner));
			rs = ps.executeQuery();
			
			if(rs!=null && rs.next()){
				maxPosicion = rs.getString("posicion")==null?"0":rs.getString("posicion");
			}
			rs.close();
			ps.close();
			
			query = "SELECT count(1) existePos " +
				"  FROM admcon_banner_det " +
				" WHERE ic_banner = ? " +
				" AND ig_posicion = ? ";

			if("0".equals(maxPosicion)){
				mp = new HashMap();
				mp.put("clave","1");
				mp.put("descripcion","1");
				lstCboPosicion.add(mp);
			}else{
				for(int x=1; x <= (Integer.parseInt(maxPosicion)+1); x++){
					ps = con.queryPrecompilado(query);
					ps.setInt(1, Integer.parseInt(cveBanner));
					ps.setInt(2, x);
					rs = ps.executeQuery();
					
					if(rs!=null && rs.next()){
						int pos = rs.getInt("existePos");
						if(pos==0){
							mp = new HashMap();
							mp.put("clave",String.valueOf(x));
							mp.put("descripcion",String.valueOf(x));
							lstCboPosicion.add(mp);
						}
					}
					
					rs.close();
					ps.close();
					
				}
			}
			
			return lstCboPosicion;
		}catch(Throwable t){
			log.info("getCboPosicion(Error)");
			throw new AppException("Error al generar Combo de Posicion", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getCboPosicion(S)");
		}
	}
	
	public void modifyDetBanner(HashMap hmDatosDetBann, String oldPosicion) throws AppException{
		log.info("modifyDetBanner(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String queryUpd = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();

			//actualizacion de registros
			String cveBanner = (String)hmDatosDetBann.get("CVEBANNER");
			String textoTitulo = (String)hmDatosDetBann.get("TEXTTITULO");
			String textoTexto = (String)hmDatosDetBann.get("TEXTTEXTO");
			String posicion = (String)hmDatosDetBann.get("POSICION");
			
			queryUpd = "UPDATE admcon_banner_det " +
						"   SET cg_titulo = ?, " +
						"       cg_texto = ?, " +
						"       ig_posicion = ? " +
						" WHERE ic_banner = ? AND ig_posicion = ? ";

			ps = con.queryPrecompilado(queryUpd);
			ps.setString(1, textoTitulo);
			ps.setString(2, textoTexto);
			ps.setInt(3, Integer.parseInt(posicion));
			ps.setInt(4, Integer.parseInt(cveBanner));
			ps.setInt(5, Integer.parseInt(oldPosicion));
			ps.executeUpdate();
			ps.close();

			
		}catch(Throwable t){
			log.info("modifyDetBanner(Error)");
			bOk = false;
			throw new AppException("Error al modificar informacion del detalle del banner", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("modifyDetBanner(S)");
		}
	}
}