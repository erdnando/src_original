package com.netro.contenido;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.*;
import netropology.utilerias.*;
//import com.netro.exception.*;

public class CargaArchivoAdmonCont  {
	public CargaArchivoAdmonCont() {	}
	
	private String claveContenido;
	private String contenido;
	private String extension;
	private int tamanio;

	/**
	 * Inserta el archivo cargado Pyme.
	 * @param , el usuario
	 * @return "folio" si el resultado es correcto.
	 */
	public void insertaCargaArchivoSeccion(InputStream bi_documento) {
		AccesoDB con = new AccesoDB();
		StringBuffer strQuery;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean bcommit = true;
		String fechaAct = (new SimpleDateFormat ("ddMMyyyy")).format(new java.util.Date());
		try {
			con.conexionDB();	
			
			strQuery = new StringBuffer();
			strQuery.append("UPDATE admcon_seccion " +
							"   SET bi_imagen = ?, " +
							"       df_modificacion = SYSDATE " +
							" WHERE ic_seccion = ? ");

			ps = con.queryPrecompilado(strQuery.toString());
			ps.setBinaryStream(1, bi_documento, this.tamanio);
			ps.setInt(2, Integer.parseInt(claveContenido));
			ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			bcommit = false;
			throw new AppException("Ocurrio un error al insertar el archivo cargado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bcommit);
			  con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene y la lista de docuemntos
	 * @param Lista de folio(s)
	 *
	 */
	public List getArchivosCargadoSeccion(String rutaDestino){
		StringBuffer strSQL = new StringBuffer();
		List conditions = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccesoDB con = new AccesoDB();
		List documentos = new ArrayList();
		List totaldocumentos = new ArrayList();
		String nombreArchivoTmp = null;
		
		
		try {
			con.conexionDB();	
			strSQL.append(
					"SELECT ic_seccion, bi_imagen " +
					"  FROM admcon_seccion " +
					" WHERE ic_seccion = ? "
			);


			ps = con.queryPrecompilado(strSQL.toString());
			ps.setInt(1,Integer.parseInt(claveContenido));
			
			rs = ps.executeQuery();
			String nombreArchivo = "";
			String ruta = "";
			while(rs.next()){
				InputStream inStream = rs.getBinaryStream("bi_imagen");
				nombreArchivo = rs.getString("ic_seccion")+"_imgSeccion";
				ruta = rutaDestino+nombreArchivo+".jpg";
				
				CreaArchivo creaArchivo = new CreaArchivo();
				creaArchivo.setNombre(nombreArchivo);

				if (!creaArchivo.make(inStream, ruta)) {
					throw new AppException("Error al generar el archivo en " + nombreArchivo);
				}
				nombreArchivoTmp = creaArchivo.getNombre();
				inStream.close();
				
				documentos = new ArrayList();
				documentos.add(nombreArchivoTmp);
				totaldocumentos.add(documentos);
			}
			rs.close();
			ps.close();

		}catch(Exception e) {
			throw new AppException("Error al cargar los archivos. ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return totaldocumentos;
	}
	
	
	public void insertaCargaArchivoBanner(InputStream bi_documento) {
		AccesoDB con = new AccesoDB();
		StringBuffer strQuery;
		PreparedStatement ps = null;
		boolean bcommit = true;

		try {
			con.conexionDB();
			
			strQuery = new StringBuffer();
			strQuery.append("UPDATE admcon_banner " +
						"   SET bi_imagen_miniatura = ?, " +
						"       df_modificacion = SYSDATE " +
						" WHERE ic_banner = ? ");

			ps = con.queryPrecompilado(strQuery.toString());
			ps.setBinaryStream(1, bi_documento, this.tamanio);
			ps.setInt(2, Integer.parseInt(claveContenido));
			ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			bcommit = false;
			throw new AppException("Ocurrio un error al insertar imagen cargada Banner", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bcommit);
			  con.cierraConexionDB();
			}
		}
	}
	
	
	public List getArchivosCargadoBanner(String rutaDestino){
		StringBuffer strSQL = new StringBuffer();
		List conditions = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccesoDB con = new AccesoDB();
		List documentos = new ArrayList();
		List totaldocumentos = new ArrayList();
		String nombreArchivoTmp = null;
		
		
		try {
			con.conexionDB();	
			strSQL.append(
					"SELECT ic_banner, bi_imagen_miniatura " +
					"  FROM admcon_banner " +
					" WHERE ic_banner = ? "
			);


			ps = con.queryPrecompilado(strSQL.toString());
			ps.setInt(1,Integer.parseInt(claveContenido));
			
			rs = ps.executeQuery();
			String nombreArchivo = "";
			String ruta = "";
			while(rs.next()){
				InputStream inStream = rs.getBinaryStream("bi_imagen_miniatura");
				nombreArchivo = rs.getString("ic_banner")+"_imgBanner";
				ruta = rutaDestino+nombreArchivo+".jpg";
				
				CreaArchivo creaArchivo = new CreaArchivo();
				creaArchivo.setNombre(nombreArchivo);

				if (!creaArchivo.make(inStream, ruta)) {
					throw new AppException("Error al generar el archivo en " + nombreArchivo);
				}
				nombreArchivoTmp = creaArchivo.getNombre();
				inStream.close();
				
				documentos = new ArrayList();
				documentos.add(nombreArchivoTmp);
				totaldocumentos.add(documentos);
			}
			rs.close();
			ps.close();

		}catch(Exception e) {
			throw new AppException("Error al obtener imagen del Banner. ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return totaldocumentos;
	}
	
	
	public void insertaCargaArchivoBannerFondo(InputStream bi_documento) {
		AccesoDB con = new AccesoDB();
		StringBuffer strQuery;
		PreparedStatement ps = null;
		boolean bcommit = true;

		try {
			con.conexionDB();
			
			strQuery = new StringBuffer();
			strQuery.append("UPDATE admcon_banner " +
						"   SET bi_imagen = ?, " +
						"       df_modificacion = SYSDATE " +
						" WHERE ic_banner = ? ");

			ps = con.queryPrecompilado(strQuery.toString());
			ps.setBinaryStream(1, bi_documento, this.tamanio);
			ps.setInt(2, Integer.parseInt(claveContenido));
			ps.executeUpdate();
			ps.close();

		} catch (Exception e) {
			bcommit = false;
			throw new AppException("Ocurrio un error al insertar imagen cargada para el fondo del banner", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(bcommit);
			  con.cierraConexionDB();
			}
		}
	}
	
	public List getArchivosCargadoBannerFondo(String rutaDestino){
		StringBuffer strSQL = new StringBuffer();
		List conditions = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccesoDB con = new AccesoDB();
		List documentos = new ArrayList();
		List totaldocumentos = new ArrayList();
		String nombreArchivoTmp = null;
		
		
		try {
			con.conexionDB();	
			strSQL.append(
					"SELECT ic_banner, bi_imagen " +
					"  FROM admcon_banner " +
					" WHERE ic_banner = ? "
			);


			ps = con.queryPrecompilado(strSQL.toString());
			ps.setInt(1,Integer.parseInt(claveContenido));
			
			rs = ps.executeQuery();
			String nombreArchivo = "";
			String ruta = "";
			while(rs.next()){
				InputStream inStream = rs.getBinaryStream("bi_imagen");
				nombreArchivo = rs.getString("ic_banner")+"_imgBannerFondo";
				ruta = rutaDestino+nombreArchivo+".jpg";
				
				CreaArchivo creaArchivo = new CreaArchivo();
				creaArchivo.setNombre(nombreArchivo);

				if (!creaArchivo.make(inStream, ruta)) {
					throw new AppException("Error al generar el archivo en " + nombreArchivo);
				}
				nombreArchivoTmp = creaArchivo.getNombre();
				inStream.close();
				
				documentos = new ArrayList();
				documentos.add(nombreArchivoTmp);
				totaldocumentos.add(documentos);
			}
			rs.close();
			ps.close();

		}catch(Exception e) {
			throw new AppException("Error al obtener imagen de fondo del Banner. ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return totaldocumentos;
	}
	
	private String getIdBanner () throws AppException{
		//log.info("getIdBanner(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		
		try{
			con.conexionDB();
			HashMap mp = new HashMap();
			String cveBanner = "1";
			
			//query = "SELECT (MAX(nvl(ic_banner,0)) + 1) as cveBanner " +
			query = "SELECT ic_banner as cveBanner " +
					"  FROM admcon_banner ";

			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			
			while(rs!=null && rs.next()){
				cveBanner = rs.getString("cveBanner")==null?"1":rs.getString("cveBanner");
				mp.put(cveBanner,cveBanner);
			}
			rs.close();
			ps.close();
			
			for(int x=1; x<=6; x++){
				if(!mp.containsValue(String.valueOf(x))){
					cveBanner = String.valueOf(x);
					break;
				}
			}
			
			return cveBanner;
		}catch(Throwable t){
			//log.info("getIdBanner(Error)");
			throw new AppException("Error al obtener  id para un nuevo banner", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			//log.info("getIdBanner(S)");
		}
	}
	
	public String addNewBanner(HashMap hmDataBanner, InputStream bi_documento, InputStream bi_documentoB, int tamanio, int tamanioB) throws AppException{
		//log.info("addNewBanner(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String queryIns = "";
		boolean bOk = true;
		
		try{
			con.conexionDB();

			//actualizacion de registros
			
			String cveBanner = getIdBanner();
			String nombreBann = (String)hmDataBanner.get("NOMBREBANN");
			//String descBann = (String)hmDataBanner.get("DESCBANN");
			
			
			queryIns = "INSERT INTO admcon_banner " +
						"            (ic_banner, cg_nombre, bi_imagen_miniatura, bi_imagen " +
						"            ) " +
						"     VALUES (?, ?, ?, ? " +
						"            ) ";


			ps = con.queryPrecompilado(queryIns);
			ps.setInt(1, Integer.parseInt(cveBanner));
			ps.setString(2, nombreBann);
			//ps.setString(3, descBann);
			ps.setBinaryStream(3, bi_documento, tamanio);
			ps.setBinaryStream(4, bi_documentoB, tamanioB);
			ps.executeUpdate();
			ps.close();

			return cveBanner;
		}catch(Throwable t){
			//log.info("addNewBanner(Error)");
			bOk = false;
			throw new AppException("Error al insertar informacion de nueva vineta", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			//log.info("addNewBanner(S)");
		}
	}


	public void setContenido(String contenido) {
		this.contenido = contenido;
	}


	public String getContenido() {
		return contenido;
	}
	
	/**
	 * Establece el tama�o de un archivo
	 * @param tama�o de un archivo
	 *
	 */
	public void setSize(int tamanio) {
		this.tamanio = tamanio;
	}

	/**
	 * Establece la clave de contido
	 * @param claveEpo Clave de contenido
	 *
	 */
	public void setClaveContenido(String claveContenido) {
		this.claveContenido = claveContenido;
	}

	/**
	 * Establece la extensi�n del archivo
	 * @param extension Extension del archivo
	 *
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * Obtiene la extensi�n del archivo
	 * @return extension
	 *
	 */
	public String getExtension() {
		return this.extension;
	}

}