package com.netro.contenido;

import java.util.*;
import java.io.*;
import javax.ejb.Remote;
import netropology.utilerias.*;

@Remote
public interface AdmonContenido{

    public List getDataTextLogo()throws AppException;

    public void saveTextLogo(List lstCveTextLogos)throws AppException;

    public void deleteTextLogo(List lstCveTextLogos)throws AppException;

    public List getDataSecciones()throws AppException;

    public void saveTextoSecciones(List lstCveTextLogos) throws AppException;

    public List getDataMenus(String cveSeccion, String cveMenu) throws AppException;

    public void saveTextoMenusSeccion(List lstCveTextMenus) throws AppException;

    public void deleteMenusSeccion(List lstCveTextMenus) throws AppException;

    public List getDataVinetas(String cveSeccion, String cveMenu) throws AppException;

    public void addMenuVinetas(HashMap hmDatosVineta) throws AppException;

    public void modifyMenuVinetas(String cveSecContDet, String textoVineta) throws  AppException;

    public void deleteVineta(List lstCveDataVineta) throws AppException;

    public void addNewMenuSeccion(HashMap hmDataMenuSecc) throws AppException;

    public List getDataLinks() throws AppException;

    public void saveDataLinks(List lstDataLinks) throws AppException;

    public List getDataBanners(String cveBanner) throws AppException;

    public List getDataDetBanners(String cveBanner) throws AppException;

    public void modifyTextBanners(List lstDatosBann) throws AppException;

    public void deleteBanners(List lstCveBanners) throws AppException;

    public void invertirEstatusBanners(List lstDataBanners) throws AppException;

    public void deleteDetBanner(String cveBanner, List lstCveDetBanner) throws AppException;

    public void addNewDetBanner(HashMap hmDatosDetBann) throws AppException;

    public List getCboPosicion(String cveBanner) throws AppException;

    public void modifyDetBanner(HashMap hmDatosDetBann, String oldPosicion) throws AppException;

}