package com.netro.fondeopropio;

import com.netro.exception.NafinException;

import java.util.HashMap;
import java.util.List;

import javax.ejb.Remote;

import netropology.utilerias.Registros;

@Remote
public interface ComisionFondeoPropio {

	
	public void calcularComisionMensual(String ic_if, String mes_calculo,
			String anio_calculo, String perfil, String tipo_moneda) throws NafinException;
	
	public boolean manejaVariosTiposComision(String ic_if, String ic_mes_calculo, 
			String ic_anio_calculo, String strPerfil, String strTipoMoneda, boolean ambasMonedas) throws NafinException;

	public void actualizarEstatusPago(String strIF, String strAnioCalculo,
			String strMesCalculo, String[] arrProductosPagados, String strPerfil, String strMoneda)
			throws NafinException;
	
	public List getRegistroAjusteAnual(String strIF, String strAnioCalculo) 
			throws NafinException;
	
	public void calcularAjusteAnual(String strIF, String strAnioCalculo,
			String strMesBase, String strPerfil, String strMoneda) throws NafinException;

	public void actualizarEstatusPagoAnual(String strIF, String strAnioCalculo,
			String strEstatusPagado) throws NafinException;

	public void grabarDetComision(String lsCadena, String ic_producto_nafin, 
			String ic_if, String ic_mes_calculo,
			String ic_anio_calculo, String cg_tipo_comision, String ic_banco_fondeo, String ic_moneda) 
			throws NafinException;
	public List getComision(String ic_producto, String ic_if,
			String ic_mes, String ic_anio, String cg_tipo_comision, 
			double dblSaldoPromedioMensual, String banco_fondeo, String tipo_moneda, String ClaveEPo  )
			throws NafinException;
	
	public boolean existeDetalleComision(String strIF,
			String strMes, String strAnio, String strPerfil, String strTipoMoneda, boolean ambasMonedas)
			throws NafinException;
			
			/*Agregar nuevo metodo para insercion en la bd*/
	public boolean grabarPorcComisionMensual(String porc)
			throws NafinException;
			
	public List getDatosComisionAgteFondeador(String mes, 
			String anio, String cveMoneda, String cveBancoFondeo, 
			String cveProductoNafin )
			throws NafinException;

	public List getRsmnCobroComisionAgteFondeador(String mes, 
			String anio, String cveMoneda, String cveBancoFondeo, 
			String cveProductoNafin )
			throws NafinException;
			
	public void setStatusPagadoToComisionAgteFondeador(String mes, 
			String anio, String cveMoneda, String cveBancoFondeo, 
			String cveProductoNafin )
			throws NafinException;
			
	public List getResumenesCobroComisionAgteFondeador(String mes_inicial,
			String mes_final,	String anio, String cveMoneda,	
			String cveBancoFondeo, String cveProductoNafin,	
			String pagado )
			throws NafinException;
			
	public List getListaConSaldosPorDiaYTotalDelMes(String mes, 
			String anio, String cveMoneda,String cveBancoFondeo,
			String cveProductoNafin)
			throws NafinException;
			
	public String getPorcentajeDeComisionAnual(String ic_producto_nafin)
			throws NafinException;
			
	public String getTipoDeCambio(String mes, String anio, String cveMoneda)
			throws NafinException;		
			
	public Registros getComisionResumen(String ic_if,String mes_calculo,String anio_calculo,String ic_moneda,String ic_banco_fondeo);	
			
	public Registros getAjusteAnual(String ic_if,String mes_calculo,String anio_calculo,String ic_moneda,String ic_banco_fondeo);	
	
	public   HashMap  getParametrizacionIF (String ic_if );	 
	
	public void actualizarEstatusPago(String strIF, String strAnioCalculo,
			String strMesCalculo, String[] arrProductosPagados, String[] arrFechasPagos, String strPerfil, String strMoneda)
			throws NafinException;

}
