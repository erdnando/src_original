package com.netro.fondeopropio;

import com.netro.exception.NafinException;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.Fecha;
import netropology.utilerias.Iva;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "ComisionFondeoPropioEJB" , mappedName = "ComisionFondeoPropioEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ComisionFondeoPropioBean implements ComisionFondeoPropio {

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ComisionFondeoPropioBean.class);


	/**
	 * Actualiza el estatus de los pagos, del producto especificado
	 * @param strIF Clave del if
	 * @param strAnioCalculo A�o de c�lculo de la comision
	 * @param strMesCalculo Mes de calculo de la comision
	 * @param arrProductosPagados Claves de los productos cuyas comisiones
	 *  		se establecer�n como pagadas.
	 * @param strPerfil puede ser ADMIN NAFIN o ADMIN BANCOMEXT
	 * @param strMoneda pueden ser Pesos o Dolares
	 * @throws NafinException si existe un error durante la ejecuci�n del m�todo.
	 */
	public void actualizarEstatusPago(String strIF, String strAnioCalculo,
			String strMesCalculo, String[] arrProductosPagados, String strPerfil, String strMoneda)
			throws NafinException {
		log.info("actualizarEstatusPago(E)");
		String banco_fondeo = "";

		if(strPerfil.equals("ADMIN NAFIN")) {
			banco_fondeo = "1";
		}
		if(strPerfil.equals("ADMIN BANCOMEXT")) {
			banco_fondeo = "2";
		}

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		int ic_if = 0;
		int ic_mes_calculo = 0;
		int ic_anio_calculo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (strIF == null || strMesCalculo == null || strAnioCalculo == null || strPerfil == null || strMoneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(strIF);
			ic_mes_calculo = Integer.parseInt(strMesCalculo);
			ic_anio_calculo = Integer.parseInt(strAnioCalculo);
			if (ic_mes_calculo <1 || ic_mes_calculo > 12) {
				throw new Exception("Mes fuera de rango (01-12)");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" strIF=" + strIF +
					" strMesCalculo=" + strMesCalculo +
					" strAnioCalculo=" + strAnioCalculo +
					" arrProductosPagados = " + arrProductosPagados +
					" strPerfil = " + strPerfil +
					" strMoneda = " + strMoneda);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			// Se marcan todos como no pagados
			String strSQL = " UPDATE com_pago_comision_fondeo " +
					" SET cg_estatus_pagado = 'N' " +
					" WHERE ic_if = " + strIF +
					" AND ic_anio_calculo = " + strAnioCalculo +
					" AND ic_mes_calculo = " + strMesCalculo +
					" AND ic_moneda = " + strMoneda +
					" AND ic_banco_fondeo =" + banco_fondeo;
			//Se actualizan los que si estan pagados:

			con.ejecutaSQL(strSQL);

			if (arrProductosPagados != null) {
				StringBuffer clavesProductos = new StringBuffer();
				for (int i = 0; i < arrProductosPagados.length; i++) {
					if (i > 0) {
						clavesProductos.append(",");
					}
					clavesProductos.append(arrProductosPagados[i]);
				}
				if(!clavesProductos.toString().equals("") ) {
				String strSQL1 = " UPDATE com_pago_comision_fondeo " +
						" SET cg_estatus_pagado = 'S' " +
						" WHERE ic_if = " + strIF +
						" AND ic_anio_calculo = " + strAnioCalculo +
						" AND ic_mes_calculo = " + strMesCalculo +
						" AND ic_producto_nafin in (" + clavesProductos + ") " +
						" AND ic_moneda = " + strMoneda +
						" AND ic_banco_fondeo =" + banco_fondeo;
					con.ejecutaSQL(strSQL1);
				}

			}
		} catch (SQLException e) {
			exito = false;
			log.error("Error al actualizar el estatus de pago", e);
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			exito = false;
			log.error("Error al actualizar el estatus de pago", e);
			throw new NafinException("SIST0001");
		} finally {
			log.info("actualizarEstatusPago(S)");
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}
	/**
	 * <strong>FODEA-039-2014</strong><br>
	 * Actualiza el estatus de los pagos, del producto especificado en la fecha especidicada
	 * @param strIF Clave del if
	 * @param strAnioCalculo A�o de c�lculo de la comision
	 * @param strMesCalculo Mes de calculo de la comision
	 * @param arrProductosPagados Claves de los productos cuyas comisiones
	 *  		se establecer�n como pagadas.
	 * @param arrFechasPagos Fechas de pago
	 * @param strPerfil puede ser ADMIN NAFIN o ADMIN BANCOMEXT
	 * @param strMoneda pueden ser Pesos o Dolares
	 * @throws NafinException si existe un error durante la ejecuci�n del m�todo.
	 */
	public void actualizarEstatusPago(String strIF, String strAnioCalculo,
			String strMesCalculo, String[] arrProductosPagados, String[] arrFechasPagos, String strPerfil, String strMoneda)
			throws NafinException {
		log.info("actualizarEstatusPago(E)");
		String banco_fondeo = "";

		if(strPerfil.equals("ADMIN NAFIN")) {
			banco_fondeo = "1";
		}
		if(strPerfil.equals("ADMIN BANCOMEXT")) {
			banco_fondeo = "2";
		}

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		int ic_if = 0;
		int ic_mes_calculo = 0;
		int ic_anio_calculo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (strIF == null || strMesCalculo == null || strAnioCalculo == null || strPerfil == null || strMoneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(strIF);
			ic_mes_calculo = Integer.parseInt(strMesCalculo);
			ic_anio_calculo = Integer.parseInt(strAnioCalculo);
			if (ic_mes_calculo <1 || ic_mes_calculo > 12) {
				throw new Exception("Mes fuera de rango (01-12)");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" strIF=" + strIF +
					" strMesCalculo=" + strMesCalculo +
					" strAnioCalculo=" + strAnioCalculo +
					" arrProductosPagados = " + arrProductosPagados +
					" strPerfil = " + strPerfil +
					" strMoneda = " + strMoneda);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			if (arrProductosPagados != null && arrFechasPagos	!=	null) {
				PreparedStatement ps = null;
				for (int i = 0; i < arrProductosPagados.length; i++) {
					String strSQL1 = " UPDATE com_pago_comision_fondeo " +
						" SET cg_estatus_pagado = 'S', df_fecha_pago = to_date(?,'DD/MM/YYYY') " +
						" WHERE ic_if = ? "+// + strIF +
						" AND ic_anio_calculo = ? "+// + strAnioCalculo +
						" AND ic_mes_calculo = ? "+// + strMesCalculo +
						" AND ic_producto_nafin = ? "+// (" + clavesProductos + ") " +
						" AND ic_moneda = ? "+// + strMoneda +
						" AND ic_banco_fondeo = ?";// + banco_fondeo;

						ps=con.queryPrecompilado(strSQL1);
						ps.setString(1,arrFechasPagos[i].toString());
						ps.setString(2,strIF);
						ps.setString(3,strAnioCalculo);
						ps.setString(4,strMesCalculo);
						ps.setString(5,arrProductosPagados[i].toString());
						ps.setString(6,strMoneda );
						ps.setString(7,banco_fondeo);

						ps.executeUpdate();
						ps.close();
				}
			}
		} catch (SQLException e) {
			exito = false;
			log.error("Error al actualizar el estatus de pago", e);
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			exito = false;
			log.error("Error al actualizar el estatus de pago", e);
			throw new NafinException("SIST0001");
		} finally {
			log.info("actualizarEstatusPago(S)");
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Calcula la Comisi�n mensual que corresponde al IF, en el mes y a�o especificado
	 * @param strIF	Clave del IF.
	 * @param strMesCalculo	Mes de calculo
	 * @param strAnioCalculo	A�o de c�lculo
	 * @param strPerfil Perfil del usuario Admin Nafin o Admin Bancomext
	 * @param strTipoMoneda tipo de moneda que se desea manejar Pesos o Dolares
	 * @exception NafinException si existe un error durante la ejecuci�n del m�todo.
	 */
	public void calcularComisionMensual(String strIF, String strMesCalculo,
			String strAnioCalculo, String strPerfil, String strTipoMoneda)
			throws NafinException {
		long tiempoInicial, tiempoIniGlobal = 0;
		long tiempoFinal = 0;

		tiempoIniGlobal = (new java.util.Date()).getTime();
		log.info("calcularComisionMensual(E)::" + strIF+"_"+strMesCalculo+"_"+strAnioCalculo+"_"+strTipoMoneda);
		String bancoSegunPerfil = "";

		/*Verificar que perfil es*/
		if(strPerfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			log.debug(".::Perfil del usuario admin nafin::.");
		}
		if(strPerfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			log.debug(".::Perfil del usuario admin bancomext::.");
		}

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		boolean exito = true;
		StringBuffer strSQLConsulta = new StringBuffer();
		int ic_mes_calculo = 0;
		int ic_anio_calculo = 0;
		int ic_if = 0;
		double dblSaldoPromedioMensualMN = 0,  dblSaldoPromedioMensualMN_M1 = 0,  dblSaldoPromedioMensualMN_M1V = 0;
		double contraprestacionTotal =0, dblSaldoPromedio = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (strIF == null || strMesCalculo == null || strAnioCalculo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(strIF);
			ic_mes_calculo = Integer.parseInt(strMesCalculo);
			ic_anio_calculo = Integer.parseInt(strAnioCalculo);
			if (ic_mes_calculo <1 || ic_mes_calculo > 12) {
				throw new Exception("Mes fuera de rango (01-12)");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" strIF=" + strIF +
					" strMesCalculo=" + strMesCalculo +
					" strAnioCalculo=" + strAnioCalculo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.YEAR, ic_anio_calculo);
		cal.set(Calendar.MONTH, ic_mes_calculo - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes
		int ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		boolean bFechaValidaParaCalculo = this.esFechaValidaParaCalculo(strMesCalculo, strAnioCalculo);

		if (!bFechaValidaParaCalculo) {
			throw new NafinException("CORP0001");	//No es fecha valida para realizar c�lculo
		}
		String porcentajeComision ="0", strClaveEPOAnterior="";

		try {
			con.conexionDB();

			String strSQL = "SELECT COUNT(*) as numRegistros " +
					" FROM COM_PAGO_COMISION_FONDEO " +
					" WHERE ic_if = " + strIF +
					" 	AND ic_anio_calculo = " + strAnioCalculo +
					" 	AND ic_mes_calculo = " + strMesCalculo +
					"	AND ic_moneda = " + strTipoMoneda + 					//Peso o Dolar este sera parametro que reciba el metodo desde el jsp
					"	AND ic_banco_fondeo = " + bancoSegunPerfil;   //Nafin o Bancomext este sera parametro que reciba el metodo desde el jsp
			tiempoInicial = (new java.util.Date()).getTime();
			rs = con.queryDB(strSQL);
			tiempoFinal = (new java.util.Date()).getTime();
			log.info("calcularComisionMensual::Verificacion de calculo previo existente (t=" + String.valueOf(tiempoFinal - tiempoInicial) + "ms)");
			rs.next();

			if (rs.getInt("numRegistros") > 0) {	//El calculo se realizo previamente, por lo tanto sale del metodo
				tiempoFinal = (new java.util.Date()).getTime();
				log.info("calcularComisionMensual(S):: Ya existe calculo previo. (t=" + String.valueOf(tiempoFinal-tiempoIniGlobal) + "ms)" + strIF);
				rs.close();
				con.cierraStatement();
				return;
			}
			rs.close();
			con.cierraStatement();

			String strTemp = " SELECT /*+ leading (sa) use_nl(sa ds d iexp cpn cm ce ci) */ " +
					" 	ci.ic_if, ci.cg_razon_social as NombreIF, " +
					" 	ce.ic_epo, " +
					" 	ce.cg_razon_social as NombreEPO, " +
					" 	cm.ic_moneda,cm.cd_nombre, " +
					" 	cpn.ic_producto_nafin as claveProductoNafin, " +
					" 	cpn.ic_nombre, " +
					" 	iexp.cg_tipo_comision, " +
					" 	count(*) as numDocumentos, " +
					" 	SUM(d.fn_monto) as montoDocumentos, cm.ic_moneda  as tipo_moneda, sa.ic_banco_fondeo as banco_fondeo " +
					" FROM com_documento d, " +
					" 	com_docto_seleccionado ds, " +
					" 	comcat_if ci, " +
					" 	comcat_epo ce, " +
					" 	comcat_moneda cm, " +
					" 	com_solicitud sa, " +
					" 	comcat_producto_nafin cpn, " +
					" 	comrel_if_epo_x_producto iexp " +
					" WHERE ds.ic_if = ci.ic_if " +
					" 	AND sa.ic_documento = ds.ic_documento " +
					" 	AND ce.ic_epo = d.ic_epo " +
					" 	AND ds.ic_documento = d.ic_documento " +
					" 	AND d.ic_moneda = cm.ic_moneda " +
					" 	AND ds.ic_if = iexp.ic_if " +
					" 	AND d.ic_epo = iexp.ic_epo " +
					" 	AND cpn.ic_producto_nafin = iexp.ic_producto_nafin " +
					" 	AND iexp.ic_producto_nafin = 1 " +
					" 	AND sa.ic_estatus_solic = 10" +
					" 	AND cm.ic_moneda = d.ic_moneda " +
					"	AND cm.ic_moneda = " + strTipoMoneda + 					//Peso o Dolar este sera parametro que reciba el metodo desde el jsp
					"	AND sa.ic_banco_fondeo = " + bancoSegunPerfil +    //Nafin o Bancomext este sera parametro que reciba el metodo desde el jsp
					" 	AND sa.df_v_descuento >= trunc(TO_DATE('01/" + strMesCalculo + "/" + strAnioCalculo + "', 'DD/MM/YYYY')) " +
					" 	AND sa.df_fecha_solicitud < trunc(TO_DATE('" + ultimoDiaDelMes + "/" + strMesCalculo + "/" + strAnioCalculo + "', 'DD/MM/YYYY')+1) " +
					" 	AND ds.ic_if = " + strIF +
					" GROUP BY ci.ic_if, ci.cg_razon_social," +
					" 	ce.ic_epo, ce.cg_razon_social," +
					" 	cm.ic_moneda,cm.cd_nombre," +
					" 	cpn.ic_producto_nafin, cpn.ic_nombre, iexp.cg_tipo_comision, sa.ic_banco_fondeo";

			String strTemp4 =
          "SELECT /*+ leading(sa) use_nl(sa d iexp cpn cm ce ci) */  ci.ic_if, ci.cg_razon_social AS nombreif, ce.ic_epo, " +
          "         ce.cg_razon_social AS nombreepo, cm.ic_moneda, cm.cd_nombre, " +
          "         cpn.ic_producto_nafin AS claveproductonafin, cpn.ic_nombre, " +
          "         iexp.cg_tipo_comision, COUNT (d.ic_documento) AS numdocumentos, " +
          "         SUM (d.fn_monto) AS montodocumentos, cm.ic_moneda AS tipo_moneda, " +
          "         1 AS banco_fondeo " +
          "    FROM dis_solicitud sa, " +
          "         dis_documento d, " +
          "         comcat_if ci, " +
          "         comcat_epo ce, " +
          "         comcat_moneda cm, " +
          "         comcat_producto_nafin cpn, " +
          "         comrel_if_epo_x_producto iexp " +
          "   WHERE sa.ic_if = ci.ic_if " +
          "     AND sa.ic_documento = d.ic_documento " +
          "     AND ce.ic_epo = d.ic_epo " +
          "     AND d.ic_moneda = cm.ic_moneda " +
          "     AND sa.ic_if = iexp.ic_if " +
          "     AND d.ic_epo = iexp.ic_epo " +
          "     AND cpn.ic_producto_nafin = iexp.ic_producto_nafin " +
          "     AND iexp.ic_producto_nafin = 4 " +
          "     AND sa.ic_estatus_solic = 10 " +
          "     AND cm.ic_moneda = d.ic_moneda " +
          "     AND d.ic_moneda = " + strTipoMoneda + 				//Peso o Dolar este sera parametro que reciba el metodo desde el jsp
          "     AND sa.df_v_credito >= trunc(TO_DATE('01/" + strMesCalculo + "/" + strAnioCalculo + "', 'DD/MM/YYYY')) " +
          "     AND sa.df_fecha_solicitud <  trunc(TO_DATE('" + ultimoDiaDelMes + "/" + strMesCalculo + "/" + strAnioCalculo + "', 'DD/MM/YYYY')+1) " +
          "     AND sa.ic_if =  " + strIF +
          " GROUP BY ci.ic_if, " +
          "         ci.cg_razon_social, " +
          "         ce.ic_epo, " +
          "         ce.cg_razon_social, " +
          "         cm.ic_moneda, " +
          "         cm.cd_nombre, " +
          "         cpn.ic_producto_nafin, " +
          "         cpn.ic_nombre, " +
          "         iexp.cg_tipo_comision, " +
          "         1 ";


			strSQLConsulta.append(strTemp + " UNION ALL " +  strTemp4);

			strSQLConsulta.append(" ORDER BY claveProductoNafin, cg_tipo_comision ");

			log.debug("---------"+strSQLConsulta.toString());

			String claveProductoAnterior = "",tipoComisionAnterior = "";

			tiempoInicial = (new java.util.Date()).getTime();
			rs = con.queryDB(strSQLConsulta.toString());
			tiempoFinal = (new java.util.Date()).getTime();
			log.info("calcularComisionMensual::Ejecucion query principal (t=" + String.valueOf(tiempoFinal - tiempoInicial) + "ms)");

			while (rs.next()) {

				String strClaveProducto = rs.getString("claveProductoNafin");
				String strClaveEPO = rs.getString("ic_epo");
				String strMoneda = rs.getString("tipo_moneda"); 		//Obtener el tipo de moneda
				String strBancoFondeo = rs.getString("banco_fondeo"); //Obtener el banco de fondeo
				double dblMontoDocumentos = rs.getDouble("montoDocumentos");
				int totalDocumentos = rs.getInt("numdocumentos");

				//*********************Fodea 020-2014 (Inicia)	********************
				HashMap info =   this.getParametrizacionIF ( strIF );
				String  strTipoComision =info.get("COMISION").toString();
				String comiDesElect = info.get("COM_DESC_ELECTRICO").toString();

				String comiDesMercantil =info.get("COM_DESC_MERCANTIL").toString();
				String comiVentCar =info.get("COM_VENTACARTERA").toString();
				String comiModalidad2 =info.get("COM_MODALIDAD2").toString();
				String com_fija =info.get("COM_FIJA").toString();
				//**************Fodea 020-2014 (Final)******************


				List comision = new ArrayList();
				List comisionM1 = new ArrayList();
				List comisionM1V = new ArrayList();
				String porcentajeAplicado = "0", contraprestacion = "0",
				porcentajeAplicadoM1 = "0", contraprestacionM1 = "0",  contraprestacionM2 = "0", porcentajeAplicadoM1V = "0", contraprestacionM1V = "0" ;

				if (!claveProductoAnterior.equals(strClaveProducto)){
					claveProductoAnterior = strClaveProducto;
					tipoComisionAnterior = "";	//se resetea el tipo de comision
					this.guardarRegistroPago(con, strClaveProducto, strIF, 	strMesCalculo, strAnioCalculo, strBancoFondeo, strMoneda);

				} //fin cambio de producto


				log.debug("strClaveProducto  ---------->  "+strClaveProducto+" -----strTipoComision  ---------->  "+strTipoComision );

				if (!tipoComisionAnterior.equals(strTipoComision)){
					tipoComisionAnterior = strTipoComision;
					tiempoInicial = (new java.util.Date()).getTime();

				//*********************General Fodea 020-2014  **************************
					if(strTipoComision.equals("G")  )  {

						porcentajeComision = com_fija ;
						log.debug("porcentajeComision  ---G------->  "+porcentajeComision);
						dblSaldoPromedioMensualMN = this.calcularSaldoPromedioMensual(strClaveProducto,	strIF, strAnioCalculo, strMesCalculo, strTipoComision, strBancoFondeo, strMoneda, "" );
						comision = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo, strTipoComision, dblSaldoPromedioMensualMN, strBancoFondeo, strMoneda, porcentajeComision  );
						porcentajeAplicado = (String) comision.get(0);
						contraprestacion = (String) comision.get(1);

					//*********************Especifica Fodea 020-2014  **************************

					}else  if(strTipoComision.equals("E")  )  {


						if(strClaveProducto.equals("1")  )  {

							dblSaldoPromedioMensualMN = this.calcularSaldoPromedioMensual(strClaveProducto,	strIF, strAnioCalculo, strMesCalculo, strTipoComision, strBancoFondeo, strMoneda, "" );
							porcentajeComision = comiDesElect ;
							log.debug("porcentajeComision  ---E1------->  "+porcentajeComision);
							comision = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo,	strTipoComision, dblSaldoPromedioMensualMN, strBancoFondeo, strMoneda, porcentajeComision );
							porcentajeAplicado = (String) comision.get(0);
							contraprestacion = (String) comision.get(1);


						}else  if(strClaveProducto.equals("4")  )  {

							HashMap datMontos = this.calcularSaldoPromedioMensualEspDis(strClaveProducto, strIF, strAnioCalculo, strMesCalculo, strTipoComision, strBancoFondeo, strMoneda, "" );

							//Modalidad 1 sin  Venta de Cartera
							if(!datMontos.get("SaldoPromedioMensual_M1").toString().equals("0.0")) {
								dblSaldoPromedioMensualMN_M1 =  Double.parseDouble((String)datMontos.get("SaldoPromedioMensual_M1"));
								porcentajeComision = comiDesMercantil ;
								log.debug("  Modalidad 1 ==   dblSaldoPromedioMensualMN_M1  === "+dblSaldoPromedioMensualMN_M1 +"   porcentajeComision  === "+porcentajeComision);
								comisionM1 = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo,	strTipoComision, dblSaldoPromedioMensualMN_M1, strBancoFondeo, strMoneda, porcentajeComision );
								porcentajeAplicadoM1 = (String) comisionM1.get(0);
								contraprestacionM1= (String) comisionM1.get(1);
							}

							//Modalidad 1 con Venta de Cartera
							if(!datMontos.get("SaldoPromedioMensual_M1V").toString().equals("0.0")) {
								dblSaldoPromedioMensualMN_M1V=   Double.parseDouble((String)datMontos.get("SaldoPromedioMensual_M1V"));
								porcentajeComision = comiVentCar ;
								log.debug("  Modalidad 1 v ==   dblSaldoPromedioMensualMN_M1V  === "+dblSaldoPromedioMensualMN_M1V +"   porcentajeComision  === "+porcentajeComision);
								comisionM1V = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo,	strTipoComision, dblSaldoPromedioMensualMN_M1V, strBancoFondeo, strMoneda, porcentajeComision );
								porcentajeAplicadoM1V = (String) comisionM1V.get(0);
								contraprestacionM1V = (String) comisionM1V.get(1);
							}

							//Modalidad 2
							if(!datMontos.get("SaldoPromedioMensual_M2").toString().equals("0.0")) {
								dblSaldoPromedioMensualMN =    Double.parseDouble((String)datMontos.get("SaldoPromedioMensual_M2"));
								porcentajeComision = comiModalidad2;
								log.debug("  Modalidad 2==   dblSaldoPromedioMensualMN  === "+dblSaldoPromedioMensualMN +"   porcentajeComision  === "+porcentajeComision);
								comision = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo,	strTipoComision, dblSaldoPromedioMensualMN, strBancoFondeo, strMoneda, porcentajeComision );
								porcentajeAplicado = (String) comision.get(0);
								contraprestacionM2 = (String) comision.get(1);
							}

							contraprestacionTotal =  Double.parseDouble((String)contraprestacionM1) + Double.parseDouble((String)contraprestacionM1V) + Double.parseDouble((String)contraprestacionM2);
							contraprestacion = Double.toString (contraprestacionTotal) ;
						}

					}

					log.debug("contraprestacion    "+contraprestacion);
					this.guardarRegistroComisionDetalle(con, strClaveProducto, strIF,	strMesCalculo, strAnioCalculo, "F",	porcentajeAplicado, contraprestacion,	String.valueOf(dblSaldoPromedioMensualMN), strBancoFondeo, strMoneda ,
							porcentajeAplicadoM1 , contraprestacionM1 , porcentajeAplicadoM1V , contraprestacionM1V ,   contraprestacionM2 , String.valueOf(dblSaldoPromedioMensualMN_M1), String.valueOf(dblSaldoPromedioMensualMN_M1V) 	);

					tiempoFinal = (new java.util.Date()).getTime();
					log.info("calcularComisionMensual::CalculoComision tipo " + strTipoComision + "if=" + strIF + " (t=" + String.valueOf(tiempoFinal - tiempoInicial) + "ms)");

				}

				tiempoInicial = (new java.util.Date()).getTime();

				//**************************PARA CALCULAR LA CONTRAPRESTACI�N POR EPO  ********************
				contraprestacionM1V =  "0";	contraprestacionM2 ="0";  contraprestacionM1 ="0";

				if (!strClaveEPOAnterior.equals(strClaveEPO)){

					strClaveEPOAnterior = strClaveEPO;

					if(strTipoComision.equals("G")  )  {

						porcentajeComision = com_fija ;

							dblSaldoPromedioMensualMN = this.calcularSaldoPromedioMensual(strClaveProducto,	strIF, strAnioCalculo, strMesCalculo, strTipoComision, strBancoFondeo, strMoneda, strClaveEPO );
							comision = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo, strTipoComision, dblSaldoPromedioMensualMN, strBancoFondeo, strMoneda, porcentajeComision  );
							porcentajeAplicado = (String) comision.get(0);
							contraprestacion = (String) comision.get(1);
								dblSaldoPromedio  = dblSaldoPromedioMensualMN;

					}else  if(strTipoComision.equals("E")  )  {


						if(strClaveProducto.equals("1")  )  {

							dblSaldoPromedioMensualMN = this.calcularSaldoPromedioMensual(strClaveProducto,	strIF, strAnioCalculo, strMesCalculo, strTipoComision, strBancoFondeo, strMoneda, strClaveEPO );
							porcentajeComision = comiDesElect ;
							comision = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo,	strTipoComision, dblSaldoPromedioMensualMN, strBancoFondeo, strMoneda, porcentajeComision );
							porcentajeAplicado = (String) comision.get(0);
							contraprestacion = (String) comision.get(1);

							dblSaldoPromedio  = dblSaldoPromedioMensualMN;

						}else  if(strClaveProducto.equals("4")  )  {

							HashMap datMontos =  this.calcularSaldoPromedioMensualEspDis(strClaveProducto, strIF, strAnioCalculo, strMesCalculo, strTipoComision, strBancoFondeo, strMoneda, strClaveEPO );

							//Modalidad 1 sin  Venta de Cartera
							if(!datMontos.get("SaldoPromedioMensual_M1").toString().equals("0.0")) {
								dblSaldoPromedioMensualMN_M1 =  Double.parseDouble((String)datMontos.get("SaldoPromedioMensual_M1"));
								porcentajeComision = comiDesMercantil ;
								log.debug("  Modalidad 1 ==   dblSaldoPromedioMensualMN_M1  === "+dblSaldoPromedioMensualMN_M1 +"   porcentajeComision  === "+porcentajeComision);
								comisionM1 = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo,	strTipoComision, dblSaldoPromedioMensualMN_M1, strBancoFondeo, strMoneda, porcentajeComision );
								porcentajeAplicadoM1 = (String) comisionM1.get(0);
								contraprestacionM1= (String) comisionM1.get(1);
							}

							//Modalidad 1 con Venta de Cartera
							if(!datMontos.get("SaldoPromedioMensual_M1V").toString().equals("0.0")) {
								dblSaldoPromedioMensualMN_M1V=   Double.parseDouble((String)datMontos.get("SaldoPromedioMensual_M1V"));
								porcentajeComision = comiVentCar ;
								log.debug("  Modalidad 1 v ==   dblSaldoPromedioMensualMN_M1V  === "+dblSaldoPromedioMensualMN_M1V +"   porcentajeComision  === "+porcentajeComision);
								comisionM1V = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo,	strTipoComision, dblSaldoPromedioMensualMN_M1V, strBancoFondeo, strMoneda, porcentajeComision );
								porcentajeAplicadoM1V = (String) comisionM1V.get(0);
								contraprestacionM1V = (String) comisionM1V.get(1);
							}

							//Modalidad 2
							if(!datMontos.get("SaldoPromedioMensual_M2").toString().equals("0.0")) {
								dblSaldoPromedioMensualMN =    Double.parseDouble((String)datMontos.get("SaldoPromedioMensual_M2"));
								porcentajeComision = comiModalidad2;
								log.debug("  Modalidad 2==   dblSaldoPromedioMensualMN  === "+dblSaldoPromedioMensualMN +"   porcentajeComision  === "+porcentajeComision);
								comision = this.getComision(strClaveProducto, strIF, strMesCalculo, strAnioCalculo,	strTipoComision, dblSaldoPromedioMensualMN, strBancoFondeo, strMoneda, porcentajeComision );
								porcentajeAplicado = (String) comision.get(0);
								contraprestacionM2 = (String) comision.get(1);
							}

							contraprestacionTotal =  Double.parseDouble((String)contraprestacionM1) + Double.parseDouble((String)contraprestacionM1V) + Double.parseDouble((String)contraprestacionM2);
							contraprestacion = Double.toString (contraprestacionTotal) ;

							dblSaldoPromedio  = dblSaldoPromedioMensualMN+ dblSaldoPromedioMensualMN_M1 + dblSaldoPromedioMensualMN_M1V;

						}
					}

					log.debug( "-EPO:::::::::::::--"+strClaveEPO + "---------contraprestacion  "+contraprestacion);
					tiempoFinal = (new java.util.Date()).getTime();
					log.info("calcularComisionMensual::Calculo Contraprestacion x Epo " + strClaveEPO + "if=" + strIF + " (t=" + String.valueOf(tiempoFinal - tiempoInicial) + "ms)");
				}

				this.guardarRegistroComision(con, strClaveProducto, strIF, strClaveEPO,
						strMesCalculo, strAnioCalculo, "F",
						String.valueOf(dblMontoDocumentos),
						String.valueOf(totalDocumentos), strBancoFondeo, strMoneda, contraprestacion,  dblSaldoPromedio  );

			}

			rs.close();
			con.cierraStatement();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			tiempoFinal = (new java.util.Date()).getTime();
			log.info("calcularComisionMensual(S)::(t=" + String.valueOf(tiempoFinal-tiempoIniGlobal) + "ms)" + strIF+"_"+strMesCalculo+"_"+strAnioCalculo+"_"+strTipoMoneda);
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}



/**
 * Determina si el IF tiene epos con diferentes tipos de comisiones (F y R)
 * para el mes y a�o especicicado de calculo.
 * El metodo asume que previamente se realizo el calculo de las comisiones
 * correspondientes a dicho a�o y mes.
 * Si strMesCalculo es 13 se considera entonces se refiere a la Anual.
 * @param strIF	Clave del IF.
 * @param strAnioCalculo A�o de calculo
 * @param strMesCalculo Mes de calculo
 * @param strPerfil Banco de fondeo Nafin o Bancomext.
 * @param strTipoMoneda Pesos o Dolares.
 * @param ambasMonedas especifica que se deben de tomar en cuenta en la consultas Pesos y Dolares
 * @return true si el IF maneja varios tipos de comisiones
 * @exception NafinException si existe un error durante la ejecuci�n del m�todo.
 */
	public boolean manejaVariosTiposComision(String strIF, String strMesCalculo,
			String strAnioCalculo, String strPerfil, String strTipoMoneda, boolean ambasMonedas) throws NafinException {

		String bancoSegunPerfil = "";

		/*Verificar que perfil es*/
		if(strPerfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			System.out.println(".::Perfil del usuario admin nafin::.");
		}
		if(strPerfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			System.out.println(".::Perfil del usuario admin bancomext::.");
		}

		if(strPerfil.equalsIgnoreCase("AMBOS")){
			bancoSegunPerfil = "";
			System.out.println(".::Perfil del usuario AMBOS::.");
		}
		boolean variosTiposComision;
		PreparedStatement ps = null;
		AccesoDB con = new AccesoDB();
		int ic_mes_calculo = 0;
		int ic_anio_calculo = 0;
		int ic_if = 0;


		//**********************************Validaci�n de parametros:*****************************
		try {
			if (strIF == null || strMesCalculo == null || strAnioCalculo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(strIF);
			ic_mes_calculo = Integer.parseInt(strMesCalculo);
			ic_anio_calculo = Integer.parseInt(strAnioCalculo);
			if (ic_mes_calculo <1 || ic_mes_calculo > 13) {
				throw new Exception("Mes fuera de rango (01-13)");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" strIF=" + strIF +
					" strMesCalculo=" + strMesCalculo +
					" strAnioCalculo=" + strAnioCalculo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();

			StringBuffer strSQL = new StringBuffer();

			strSQL.append(" SELECT COUNT(DISTINCT iexp.cg_tipo_comision) as numTiposComision " +
					" FROM comrel_if_epo_x_producto iexp " +
					" WHERE " +
					" 	iexp.ic_if = ? " +
					" 	AND iexp.ic_epo in ( " +
					"		SELECT DISTINCT ic_epo " +
					" 		FROM com_comision_fondeo cf " +
					" 		WHERE " +
					" 			cf.ic_if = ? " +
					" 	 		AND cf.ic_anio_calculo = ? " );

					if(((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == false){
						strSQL.append("	AND cf.ic_banco_fondeo = ? AND cf.ic_moneda = ? ");
					}

					if(((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == true){
						strSQL.append("	AND cf.ic_banco_fondeo = ? AND cf.ic_moneda in (1, 54)");
					}

			if (ic_mes_calculo != 13 && ((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == false) {	//Si no es la anual se especifica el mes.
				strSQL.append(" AND cf.ic_mes_calculo = ? ");
			}
			strSQL.append(")");

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setInt(1, ic_if);
			ps.setInt(2, ic_if);
			ps.setInt(3, ic_anio_calculo);

			if(((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == false){
				ps.setInt(4, Integer.parseInt(bancoSegunPerfil));
				ps.setInt(5, Integer.parseInt(strTipoMoneda));
			}
			if(((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == true){
				ps.setInt(4, Integer.parseInt(bancoSegunPerfil));
			}
			if (ic_mes_calculo != 13 && ((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == false) {	//Si no es la anual se especifica el mes.
				ps.setInt(6, ic_mes_calculo);
			}

			System.out.println(".::manejaVariosTiposComision::strSQL::. " + strSQL);

			ResultSet rs = ps.executeQuery();

			rs.next();
			int numTiposComision = rs.getInt("numTiposComision");
			if (numTiposComision <= 1) {
				variosTiposComision = false;
			} else {
				variosTiposComision = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		System.out.println(".::ComisionFondeoPropioBean::manejaVariosTiposComision::.");

		return variosTiposComision;
	}






/**
 * Obtiene el registro del ajuste anual dentro de la base de datos.
 * @param strIF Clave del IF
 * @param strAnioCalculo A�o de c�lculo del ajuste anual
 * @return Lista con los sig. datos de tipo cadena:
 * 		0 - Estatus del pago
 * 		1 - Mes Base utilizado en el calculo
 * 		2.- Saldo Promedio de Referencia
 * 		3.- Tasa Ajustada
 * 		En caso de que no exista el registro, regresa una Lista vacia.
 * @exception NafinException si existe un error durante la ejecuci�n del m�todo.
 */
	public List getRegistroAjusteAnual(String strIF, String strAnioCalculo)
			throws NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		List registro = new ArrayList();

		int ic_if = 0;
		int ic_anio_calculo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (strIF == null || strAnioCalculo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(strIF);
			ic_anio_calculo = Integer.parseInt(strAnioCalculo);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" strIF=" + strIF +
					" strAnioCalculo=" + strAnioCalculo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {
			con.conexionDB();
			String strSQL = " SELECT cg_estatus_pagado, " +
					" ig_mes_base, fg_saldo_referencia, " +
					" fg_porcentaje_ajustado " +
					" FROM com_pago_ajuste_anual_fondeo " +
					" WHERE ic_if = ? " +
					" AND ic_anio_calculo = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);
			ps.setInt(2, ic_anio_calculo);

			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				String estatus = rs.getString("cg_estatus_pagado");
				String mesBase = rs.getString("ig_mes_base");
				String saldoReferencia = rs.getString("fg_saldo_referencia");
				String tasaAjustada = rs.getString("fg_porcentaje_ajustado");

				registro.add(0, estatus);
				registro.add(1, mesBase);
				registro.add(2, saldoReferencia);
				registro.add(3, tasaAjustada);
			}
			ps.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return registro;
	}



/**
 * Almacena en la BD el detalle de la comisi�n mensual para el
 * mes e IF especificado. La cadena guardada contiene
 * valores separados por coma, para poder generar posteriomente un archivo csv.
 * @param lsCadena Cadena a guardar.
 * @param ic_producto_nafin Clave del producto Nafin
 * @param ic_if Clave del IF
 * @param ic_mes_calculo Mes del cual se guarda el detalle
 * @param ic_anio_calculo A�o del cual se guarda el detalle
 * @param cg_tipo_comision El detalle corresponde al
 * 		tipo de comision especificada. R Rangos, F Fija
  * @exception NafinException si existe un error durante la ejecuci�n del m�todo.
 */
	public void grabarDetComision(String lsCadena, String ic_producto_nafin,
			String ic_if, String ic_mes_calculo,
			String ic_anio_calculo, String cg_tipo_comision, String bancoFondeo, String ic_moneda)
			throws NafinException {

		AccesoDB con = null;
		boolean exito = true;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_if == null || ic_mes_calculo == null ||
					ic_anio_calculo == null || lsCadena == null ||
					cg_tipo_comision ==null || bancoFondeo == null || ic_moneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			Integer.parseInt(ic_if);
			int iMesCalculo = Integer.parseInt(ic_mes_calculo);
			if (iMesCalculo <1 || iMesCalculo > 12) {
				throw new Exception("Mes fuera de rango (01-12)");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" lsCadena=" + lsCadena +
					" ic_producto_nafin=" + ic_producto_nafin +
					" ic_if=" + ic_if +
					" ic_mes_calculo = " + ic_mes_calculo +
					" ic_anio_calculo = " + ic_anio_calculo +
					" cg_tipo_comision = " + cg_tipo_comision +
					" perfil = " + bancoFondeo +
					" ic_moneda = " + ic_moneda);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************



		int liTamanoCadena = lsCadena.length();
		//System.out.println("\nliTamanoCadena: "+liTamanoCadena);
		//System.out.println("\nlsCadena: "+lsCadena);
		int liTamanok = 31999;	//liTamanok es el tama�o m�ximo que acepta Oracle (dada la configuraci�n actual)
		int liIniciok = 0, liFink = 0;
		int liNumeroDePartes = (int) (liTamanoCadena / liTamanok);
		if (liTamanoCadena % liTamanok != 0) { //Si la divisi�n no es exacta se agrega una parte m�s.
			liNumeroDePartes++;
		}
		String lsPiezaCadena = "";
		try {
		 	con = new AccesoDB();
   			con.conexionDB();
			CallableStatement cs = con.ejecutaSP("SP_INSERTA_LOB2(?,?,?,?,?,?,?)");
			for(int v=0; v < liNumeroDePartes; v++) {
				liIniciok = v * liTamanok;
				liFink = (v+1) * liTamanok;
				if (liFink > liTamanoCadena) {	//El indice Final sobrepasa el tama�o de la cadena?
					liFink = liTamanoCadena;
				}

				lsPiezaCadena = lsCadena.substring(liIniciok, liFink);
				cs.setString(1, "com_comision_fondeo_det"); //tabla donde se almacena el archivo
				cs.setString(2, "");
				cs.setString(3, "tt_contenido_detalle_comision");	//Campos	CLOB que se insertan
				cs.setString(4, "");	//Campos	CLOB que se insertan
				cs.setString(5, lsPiezaCadena);
				cs.setString(6, "ic_producto_nafin = "+ic_producto_nafin+
						" AND ic_if = "+ic_if+

						" AND ic_moneda = " + ic_moneda +
						" AND ic_banco_fondeo = " + bancoFondeo +
						" AND ic_mes_calculo = "+Integer.parseInt(ic_mes_calculo)+
						" AND ic_anio_calculo = "+Integer.parseInt(ic_anio_calculo)//+
						//" AND cg_tipo_comision = '"+cg_tipo_comision+"'"
						);	//Condicion
				cs.setInt(7, 1);	//Condicion
				cs.execute();
			}
			if(cs != null) {
				 cs.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}



/**
 * Calcula el ajuste anual que corresponde al IF, en el a�o especificado
 * @param strIF	Clave del IF.
 * @param strAnioCalculo A�o de c�lculo
 * @param strMesBase Mes de base para el calculo de la contraprestacion ajustada
 * @exception NafinException si existe un error durante la ejecuci�n del m�todo.
 */
	public void calcularAjusteAnual(String strIF, String strAnioCalculo, String strMesBase, String strPerfil, String strMoneda)
			throws NafinException {

		String bancoSegunPerfil = "";

		/*Verificar que perfil es*/
		if(strPerfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			System.out.println(".::Perfil del usuario admin nafin::.");
		}
		if(strPerfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			System.out.println(".::Perfil del usuario admin bancomext::.");
		}

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		boolean exito = true;

		int ic_if = 0;
		int ig_mes_base = 0;
		int ic_anio_calculo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (strIF == null || strMesBase == null || strAnioCalculo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(strIF);
			ig_mes_base = Integer.parseInt(strMesBase);
			ic_anio_calculo = Integer.parseInt(strAnioCalculo);
			if (ig_mes_base <1 || ig_mes_base > 12) {
				throw new Exception("Mes Base fuera de rango (01-12)");
			}
		} catch(Exception e) {
			System.out.println(".::calcularAjusteAnual(Exception 1)::.");
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" strIF=" + strIF +
					" strAnioCalculo=" + strAnioCalculo +
					" strMesBase=" + strMesBase);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************


		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.YEAR, ic_anio_calculo);
		cal.set(Calendar.MONTH, ig_mes_base - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes
		int numDiasMesBase = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		boolean bFechaValidaParaCalculoAnual = esFechaValidaParaCalculoAnual(strAnioCalculo);

		if (!bFechaValidaParaCalculoAnual) {
			throw new NafinException("CORP0001"); //no es fecha valida.
		}


		try {
			con.conexionDB();

			String strSQL = "SELECT COUNT(*) as numRegistros " +
					" FROM com_pago_ajuste_anual_fondeo " +
					" WHERE ic_if = " + strIF +
					" 	AND ic_anio_calculo = " + strAnioCalculo +
					"	AND ic_moneda = " + strMoneda +
					"	AND ic_banco_fondeo = " + bancoSegunPerfil ;
			rs = con.queryDB(strSQL);
			rs.next();

			if (rs.getInt("numRegistros") > 0) {	//El calculo se realizo previamente, por lo tanto sale del metodo
				return;
			}
			rs.close();
			con.cierraStatement();

			double dblSaldoPromedioDeReferencia = calcularSaldoPromedioDeReferencia(strIF, strAnioCalculo, strMoneda, bancoSegunPerfil);
			/*Dado que la tasa ajustada no depende del producto, y que actualmente
			 *las comisiones para cualquiera de los productos es la misma, entonces
			 *colocamos cualquier producto en el par�metro del metodo getcomisionXRango,
			 *en este caso colocamos el "1".
			 */
			String strTasaAjustada = getComisionXRango("1", dblSaldoPromedioDeReferencia, bancoSegunPerfil);

			strSQL =
					" SELECT " +
					" 	cfd.cg_tipo_comision, " +
					" 	cfd.ic_producto_nafin, " +
					" 	pcf.ic_mes_calculo, " +
					" 	cfd.fg_saldo_promedio,  " +
					" 	cfd.fg_porcentaje_aplicado, " +
					" 	cfd.fg_contraprestacion " +
					" FROM " +
					" 	com_comision_fondeo_det cfd,  " +
					" 	com_pago_comision_fondeo pcf " +
					" WHERE " +
					" 	pcf.ic_producto_nafin = cfd.ic_producto_nafin " +
					" 	AND pcf.ic_if = cfd.ic_if " +
					" 	AND pcf.ic_mes_calculo = cfd.ic_mes_calculo " +
					" 	AND pcf.ic_anio_calculo = cfd.ic_anio_calculo " +
					"	AND pcf.ic_moneda = cfd.ic_moneda " +
               "	AND pcf.ic_banco_fondeo = cfd.ic_banco_fondeo" +
					" 	AND pcf.ic_if = " + strIF +
					" 	AND pcf.ic_anio_calculo = " + strAnioCalculo +
					" 	AND cfd.ic_moneda = " + strMoneda +
					"	AND cfd.ic_banco_fondeo = " + bancoSegunPerfil +
					" ORDER BY cfd.cg_tipo_comision, cfd.ic_producto_nafin, " +
					" 	pcf.ic_mes_calculo ";
			System.out.println(strSQL);

			rs = con.queryDB(strSQL);

			while (rs.next()) {
				String strTipoComision = rs.getString("cg_tipo_comision");
				String strClaveProducto = rs.getString("ic_producto_nafin");
				double dblSaldoPromedio = rs.getDouble("fg_saldo_promedio");
				String strMes = rs.getString("ic_mes_calculo");
				//double dblContraprestacion = rs.getDouble("fg_contraprestacion");


				double dblContraprestacionAjustada =  dblSaldoPromedio * Double.parseDouble(strTasaAjustada) / 100 * numDiasMesBase / 360;

				guardarAjuste(con, strClaveProducto, strIF, strMes, strAnioCalculo,
						strTipoComision, String.valueOf(dblContraprestacionAjustada), bancoSegunPerfil, strMoneda);
			}
			rs.close();

			guardarRegistroPagoAjusteAnual(con, strIF, strAnioCalculo, strTasaAjustada,
						String.valueOf(dblSaldoPromedioDeReferencia), strMesBase, strMoneda, bancoSegunPerfil);
			con.cierraStatement();
		} catch (SQLException e) {
			System.out.println(".::calcularAjusteAnual(SQLException)::.");
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(".::calcularAjusteAnual(NamingException)::.");
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		}catch(Exception ex)
		{
			System.out.println(".::calcularAjusteAnual(Exception)::.");
			System.out.println("Error en el metodo calcularAjusteAnual " + ex.getMessage());
			System.out.println("Error en el metodo calcularAjusteAnual " + ex.getCause());
			ex.printStackTrace();
		}
		finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}

		System.out.println(".::calcularAjusteAnual(S)::.");
	}



/**
 * Actualiza el estatus de los pagos, del producto especificado
 * @param strIF Clave del if
 * @param strAnioCalculo A�o de c�lculo de la comision
 * @param strEstatusPagado Estatus del pago S = pagado N = No pagado
 * @exception NafinException si existe un error durante la ejecuci�n del m�todo.
 */
	public void actualizarEstatusPagoAnual(String strIF, String strAnioCalculo,
			String strEstatusPagado)
			throws NafinException {

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		int ic_if = 0;
		int ic_anio_calculo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (strIF == null || strEstatusPagado == null || strAnioCalculo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(strIF);
			ic_anio_calculo = Integer.parseInt(strAnioCalculo);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" strIF=" + strIF +
					" strAnioCalculo=" + strAnioCalculo +
					" strEstatusPagado=" + strEstatusPagado);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************


		try {
			con.conexionDB();
			String strSQL = " UPDATE com_pago_ajuste_anual_fondeo " +
					" SET cg_estatus_pagado = '" + strEstatusPagado + "' " +
					" WHERE ic_if = " + strIF +
					" AND ic_anio_calculo = " + strAnioCalculo;
			con.ejecutaSQL(strSQL);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			exito = false;
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}

	}

/**
 * Determina si el detalle de comisiones ya fue generado o no
 * @param strIF	Clave del IF.
 * @param strMes	Mes de calculo
 * @param strAnio	A�o de c�lculo
 * @param strPerfil Banco de fondeo Nafin o Bancomext.
 * @param strTipoMoneda Pesos o Dolares.
 * @return	true si existe el detalle o false de lo contrario
 */
	public boolean existeDetalleComision(String strIF,
			String strMes, String strAnio, String strPerfil, String strTipoMoneda, boolean ambasMonedas)
			throws NafinException {

		String bancoSegunPerfil = "";

		/*Verificar que perfil es*/
		if(strPerfil.equalsIgnoreCase("ADMIN NAFIN")){
			bancoSegunPerfil = "1";
			System.out.println(".::Perfil del usuario admin nafin::.");
		}
		if(strPerfil.equalsIgnoreCase("ADMIN BANCOMEXT")){
			bancoSegunPerfil = "2";
			System.out.println(".::Perfil del usuario admin bancomext::.");
		}
		if(strPerfil.equalsIgnoreCase("AMBOS")){
			bancoSegunPerfil = "";
			System.out.println(".::Perfil del usuario ambos::.");
		}

		boolean existe = false;

		PreparedStatement ps = null;
		AccesoDB con = new AccesoDB();

		int ic_mes_calculo = 0;
		int ic_anio_calculo = 0;
		int ic_if = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (strIF == null || strMes == null ||
					strAnio == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(strIF);
			ic_mes_calculo = Integer.parseInt(strMes);
			ic_anio_calculo = Integer.parseInt(strAnio);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" strIF=" + strIF +
					" strMes=" + strMes +
					" strAnio=" + strAnio);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************
		try {
			con.conexionDB();
			String strSQL = " SELECT count(*) " +
					" FROM com_comision_fondeo_det " +
					" WHERE DBMS_LOB.getLength(tt_contenido_detalle_comision) > 1 " +
					" AND ic_if = ? " +
					" AND ic_mes_calculo = ? " +
					" AND ic_anio_calculo = ? ";

					if(((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == false){
						strSQL += "	AND ic_moneda = ? AND ic_banco_fondeo = ? ";
					}
					if(((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == true){
						strSQL += "	AND ic_moneda in (1, 54) AND ic_banco_fondeo = ? ";
					}

			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);
			ps.setInt(2, ic_mes_calculo);
			ps.setInt(3, ic_anio_calculo);

			if(((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == false){
				ps.setInt(4, Integer.parseInt(strTipoMoneda));
				ps.setInt(5, Integer.parseInt(bancoSegunPerfil));
			}

			if(((strPerfil.equals("ADMIN NAFIN") || strPerfil.equals("ADMIN BANCOMEXT"))) && ambasMonedas == true){
				ps.setInt(4, Integer.parseInt(bancoSegunPerfil));
			}

			System.out.println(".::existeDetalleComision::strSQL::. " + strSQL);
			System.out.println(".::ic_if " + ic_if);
			System.out.println(".::ic_mes_calculo::: " + ic_mes_calculo);
			System.out.println(".::ic_anio_calculo:: " + ic_anio_calculo);
			System.out.println(".::strTipoMoneda:: " + strTipoMoneda);
			System.out.println(".::bancoSegunPerfil:: " + bancoSegunPerfil);

			ResultSet rs = ps.executeQuery();

			if(rs.next()) {
				int numRegistros = rs.getInt(1);

				if (numRegistros > 0) {
					//Ya existe el contenidos del archivo.
					existe = true;
				} else {
					existe = false;
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		System.out.println(".::ComisionFondeoPropioBean::existeDetalleComision::.");

		return existe;
	}

/**
 * Obtiene la comisi&oacute;n que corresponde
 * @param ic_producto	Clave del producto nafin.
 * @param ic_if	Clave del IF.
 * @param ic_mes	Mes de calculo
 * @param ic_anio	A�o de c�lculo
 * @param  porcentajeComision  //Fodea 020-2014
 * @param cg_tipo_comision	Tipo de Comision "F" Fija y "R" por Rangos.
 * @param dblSaldoPromedioMensual	Saldo promedio mensual del IF. Este par�metro permite calcular
 * 		en caso de ser tipo de comision "R", el porcentaje de que corresponde a dicho monto.
 * @param banco_fondeo	Banco de fondeo Nafin o Bancomext.
 * @param tipo_moneda	Tipo de moneda Pesos o Dolares.
 * @return	Lista con dos valores de tipo cadena: El primero es el porcentaje de comision y el
 * 		segundo es la contraprestacion. Los valores pueden ser nulos en el caso de que
 * 		la comision no puede ser obtenida porque el mes no ha terminado y el tipo de
 * 		comision es por rangos.
 */
	public List getComision(String ic_producto, String ic_if,
			String ic_mes, String ic_anio, String cg_tipo_comision,
			double dblSaldoPromedioMensual, String banco_fondeo, String tipo_moneda, String porcentajeComision)
			throws NafinException {
		log.info("getComision(E)::" + ic_if);

		String strContraprestacion = null;

		List comision = new ArrayList();
		AccesoDB con = new AccesoDB();
		double contraprestacion = 0;


		int ic_mes_calculo = 0;
		int ic_anio_calculo = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_if == null || ic_mes == null ||
					ic_anio == null || cg_tipo_comision == null ||
					ic_producto == null || banco_fondeo == null || tipo_moneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			Integer.parseInt(ic_if);
			ic_mes_calculo = Integer.parseInt(ic_mes);
			ic_anio_calculo = Integer.parseInt(ic_anio);
		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() +
					" ic_producto=" + ic_producto +
					" ic_if=" + ic_if +
					" ic_mes=" + ic_mes +
					" ic_anio=" + ic_anio +
					" cg_tipo_comision=" + cg_tipo_comision +
					" dblSaldoPromedioMensual=" + dblSaldoPromedioMensual +
					"banco_fondeo="+ banco_fondeo +
					"tipo_moneda="+ tipo_moneda);

			throw new NafinException("SIST0001");
		}

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.YEAR, ic_anio_calculo);
		cal.set(Calendar.MONTH, ic_mes_calculo - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes
		int numDiasMesCalculo = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

			strContraprestacion = null;
		try {
			con.conexionDB();

				if (this.esFechaValidaParaCalculo(ic_anio, ic_mes)) {
					contraprestacion =
							dblSaldoPromedioMensual * Double.parseDouble(porcentajeComision) /
							100 * numDiasMesCalculo / 360;
					strContraprestacion = String.valueOf(contraprestacion);

					log.debug(".::ComisionFondeoPropioEJB::getComision::.");
					log.debug("dblSaldoPromedioMensual " + dblSaldoPromedioMensual);
					log.debug("porcentajeComision " + porcentajeComision);
					log.debug("numDiasMesCalculo " + numDiasMesCalculo);

				} else {
					if (cg_tipo_comision.equals("F")) {
						strContraprestacion = null;
					}
				}
			//}
			comision.add(porcentajeComision);
			comision.add(strContraprestacion);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			log.info("getComision(S)::" + ic_if);
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return comision;
	}




/*************************** METODOS  PRIVADOS ***************************/


/** Mod: 27/05/2004 17:03
 * Calcula el saldo promedio mensual para la IF y fecha especificada. Solo para Moneda Nacional
 * @param ic_producto_nafin Clave del producto nafin
 * @param ic_if Clave del IF
 * @param anio_calculo A�o de c�lculo
 * @param mes_calculo Mes de c�lculo
 * @param cg_tipo_comision Tipo de comision F- Fija o R- Rangos
 */
	private double calcularSaldoPromedioMensual(String ic_producto_nafin, String ic_if,
			String anio_calculo, String mes_calculo, String cg_tipo_comision,
			String banco_fondeo, String tipo_moneda, String strClaveEPO )
			throws NamingException, SQLException {
		log.info("calcularSaldoPromedioMensual(E)::" + ic_if);
		AccesoDB con = new AccesoDB();
		double dblSaldoPromedioMensual = 0;
		StringBuffer strTemp = new StringBuffer();
		StringBuffer strTemp4 = new StringBuffer();
		String strSQLConsulta = null;

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.YEAR, Integer.parseInt(anio_calculo));
		cal.set(Calendar.MONTH, Integer.parseInt(mes_calculo) - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes

		int ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		/*
		 * El campo que corresponde al monto vigente, se calcula, determinando los dias que un documento estuvo
		 * vigente durante el mes de calculo. Si el documento vence en el mes de calculo, entonces se le resta
		 * un d�a, ya que el d�a de vencimiento no se debe contemplar.
		 */


		if(!strClaveEPO.equals(""))  {
			strTemp.append(" SELECT  " );
		}else  {
			strTemp.append(" SELECT  /*+ leading(sa) use_nl(sa ds d) */ " );
		}
		strTemp.append("	SUM( " +
				" 		(CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_descuento, 'YYYY')) > " + anio_calculo + " THEN  " +
							ultimoDiaDelMes +
				" 		ELSE  " +
				" 			CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_descuento, 'MM')) > " + mes_calculo + " THEN " +
								ultimoDiaDelMes +
				" 			ELSE " +
				" 				TO_NUMBER(TO_CHAR(sa.df_v_descuento, 'DD')) - 1 " +
				" 			END " +
				" 		END " +
				" 		- " +
				" 		CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY')) < " + anio_calculo + " THEN " +
				" 			1 " +
				" 		ELSE " +
				" 			CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) < " + mes_calculo + " THEN " +
				" 				1 " +
				" 			ELSE " +
				" 				TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'DD')) " +
				" 			END " +
				" 		END " +
				" 		+ 1 ) * d.fn_monto ) AS monto_vigente  " +
				" FROM com_documento d, " +
				" 	com_docto_seleccionado ds, " +
				" 	com_solicitud sa, " +
				" 	comcat_producto_nafin cpn, " +
				" 	comrel_if_epo_x_producto iexp " +
				" WHERE " +
				" 	sa.ic_documento = ds.ic_documento " +
				" 	AND ds.ic_documento = d.ic_documento " +
				" 	AND ds.ic_if = iexp.ic_if " +
				" 	AND d.ic_epo = iexp.ic_epo " +
				" 	AND cpn.ic_producto_nafin = iexp.ic_producto_nafin " +
				" 	AND iexp.ic_producto_nafin = 1 " +
				" 	AND sa.ic_estatus_solic = 10 " +
				"	AND d.ic_moneda = " + tipo_moneda + // Tipo de moneda Pesos o Dolares
				"	AND sa.ic_banco_fondeo = " + banco_fondeo + //Banco de fondeo Nafin o Bancomext
				" 	AND sa.df_v_descuento >= trunc(TO_DATE('01/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY')) " +
				" 	AND sa.df_fecha_solicitud < trunc(TO_DATE('" + ultimoDiaDelMes + "/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY'))+1 " +
				" 	AND ds.ic_if = " + ic_if );
				if(!strClaveEPO.equals(""))  {
					strTemp.append(" and  d.ic_epo =  "+ strClaveEPO );
				}

		if(!strClaveEPO.equals(""))  {
			strTemp4.append(" SELECT  " );
		}else {
			strTemp4.append(" SELECT /*+ leading(sa)*/ " );
		}

		strTemp4.append(" 	SUM( " +
				" 		(CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'YYYY')) > " + anio_calculo + " THEN  " +
							ultimoDiaDelMes +
				" 		ELSE  " +
				" 			CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'MM')) > " + mes_calculo + " THEN " +
								ultimoDiaDelMes +
				" 			ELSE " +
				" 				TO_NUMBER(TO_CHAR(sa.df_v_credito, 'DD')) - 1 " +
				" 			END " +
				" 		END " +
				" 		- " +
				" 		CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY')) < " + anio_calculo + " THEN " +
				" 			1 " +
				" 		ELSE " +
				" 			CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) < " + mes_calculo + " THEN " +
				" 				1 " +
				" 			ELSE " +
				" 				TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'DD')) " +
				" 			END " +
				" 		END " +
				" 		+ 1 ) * d.fn_monto ) AS monto_vigente  " +
				" FROM dis_documento d, " +
				" 	dis_solicitud sa, " +
				" 	comcat_producto_nafin cpn, " +
				" 	comrel_if_epo_x_producto iexp " +
				" WHERE " +
				" 	sa.ic_documento = d.ic_documento " +
				" 	AND sa.ic_if = iexp.ic_if " +
				" 	AND d.ic_epo = iexp.ic_epo " +
				" 	AND cpn.ic_producto_nafin = iexp.ic_producto_nafin " +
				" 	AND iexp.ic_producto_nafin = 4 " +
				" 	AND sa.ic_estatus_solic = 10 " +
				"	AND d.ic_moneda = " + tipo_moneda + // Tipo de moneda Pesos o Dolares
				" 	AND sa.df_v_credito >= trunc(TO_DATE('01/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY')) " +
				" 	AND sa.df_fecha_solicitud < trunc(TO_DATE('" + ultimoDiaDelMes + "/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY')+1) " +
				" 	AND sa.ic_if = " + ic_if );
				if(!strClaveEPO.equals(""))  {
					strTemp4.append(" and  d.ic_epo =  "+ strClaveEPO );
				}


		try {
			con.conexionDB();
			if ("1".equals(ic_producto_nafin)) {
				strSQLConsulta = strTemp.toString();
			} else if ("4".equals(ic_producto_nafin)) {
				strSQLConsulta = strTemp4.toString();
			} else {
				throw new RuntimeException("El producto especificado no es soportado para esta operacion");
			}
			log.debug("calcularSaldoPromedioMensual::" + strSQLConsulta);
			ResultSet rs = con.queryDB(strSQLConsulta);
			double dblTotalMontoVigente = 0;
			while (rs.next()) {
				dblTotalMontoVigente += rs.getDouble("monto_vigente");
			}
			rs.close();
			con.cierraStatement();

			//saldoPromedioMensual = monto vigente / numero de dias del mes de calculo
			dblSaldoPromedioMensual = dblTotalMontoVigente / ultimoDiaDelMes;

			System.out.println(".::ComisionFondeoPropioBean::calcularSaldoPromedioMensual::.");
			System.out.println("dblTotalMontoVigente " + dblTotalMontoVigente);
			System.out.println("ultimoDiaDelMes " + ultimoDiaDelMes);
			System.out.println("dblSaldoPromedioMensual " + dblSaldoPromedioMensual);

		} finally {
			log.info("calcularSaldoPromedioMensual(S)" + ic_if);
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return dblSaldoPromedioMensual;
	}

/**
 * Determina si la fecha actual es valida para poder realizar ciertas operaciones.
 * @param strAnioCalculo A�o de c�lculo
 * @param strMesCalculo Mes de c�lculo
 * @return true solo si se cumple algunas de las siguientes condiciones:
 * 		a�o de calculo es anterior al a�o en curso.
 * 		a�o de calculo es igual a�o en curso y el mes de calculo es anterior al mes en curso.
 * 		a�o y mes de calculo son iguales al a�o y mes en curso y el dia actual es el ultimo dia del mes.
 */
	private boolean esFechaValidaParaCalculo(String strAnioCalculo, String strMesCalculo) {

		int anioCalculo = Integer.parseInt(strAnioCalculo);
		int mesCalculo = Integer.parseInt(strMesCalculo);

		Calendar calFechaActual = Calendar.getInstance();
		int ultimoDiaDelMesActual = calFechaActual.getActualMaximum(Calendar.DAY_OF_MONTH);
		int diaActual = calFechaActual.get(Calendar.DAY_OF_MONTH);
		int mesActual = calFechaActual.get(Calendar.MONTH) + 1;	//+1 porque Enero es 0 en Calendar.
		int anioActual = calFechaActual.get(Calendar.YEAR);

		return anioCalculo < anioActual || (anioCalculo == anioActual &&
			( (mesCalculo < mesActual) || (mesCalculo == mesActual && ultimoDiaDelMesActual == diaActual) ) );
	}

/**
 * Obtiene la comision que aplica al monto especificado y producto.
 * @param productoNafin	El producto nafin
 * @param saldoPromedioMensual	El Saldo Promedio Mensual operado en el mes de calculo
 * @return Regresa una cadena vacia si no existe ningun rango parametrizado que
 * concuerde con el monto especificado. De lo contrario regresa el porcentaje
 * de comision
 */
	private String getComisionXRango(String productoNafin, double saldoPromedioMensual, String banco_fondeo)
			throws SQLException, NamingException, NafinException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String porcentajeComision = "";
		try {
			con.conexionDB();
			String strSQL = " SELECT fn_porcentaje " +
					" FROM comrel_producto_comision " +
					" WHERE ic_producto_nafin = ? " +
					" AND ROUND(?,2) " +
					" BETWEEN fn_saldo_de AND fn_saldo_a " +
					" AND ic_banco_fondeo = ? ";

					System.out.println(".::ComisionFondeoPropioBean::getComisionXRango::.");

			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(productoNafin));
			ps.setDouble(2, saldoPromedioMensual);
			ps.setString(3, banco_fondeo);

			ResultSet rs = ps.executeQuery();

			System.out.println(strSQL);
			if (rs.next()) {
				porcentajeComision = rs.getString("fn_porcentaje");
			} else {
				throw new NafinException("PARM0013");
			}
			ps.close();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return porcentajeComision;
	}




	/**
	 * Guarda el registro de la comision calculada para el mes y a�o especificados.
	 * @param ic_producto	Clave del producto nafin.
	 * @param ic_if	Clave del IF.
	 * @param ic_epo Clave de la EPO
	 * @param ic_mes	Mes de calculo
	 * @param ic_anio	A�o de c�lculo
	 * @param cg_tipo_comision Tipo de comision F fija o R por Rangos
	 * @param fg_monto_doctos Monto de los documentos
	 * @param ig_doctos_vigentes Numero de documentos Vigentes
	 * @param banco_fondeo Banco de fondeo.
	 * @param ic_moneda	Tipo de moneda Pesos o Dolares.
	 */
	private void guardarRegistroComision(AccesoDB con, String ic_producto, String ic_if, String ic_epo,
			String ic_mes, String ic_anio, String cg_tipo_comision,
			String fg_monto_doctos, String ig_doctos_vigentes, String banco_fondeo, String ic_moneda, String fn_contri_comision, double fg_saldo_promedio )
			throws NamingException, SQLException {
			log.info("guardarRegistroComision(E)::" + ic_if);

		try {
			String strSQL = " insert into com_comision_fondeo " +
					" (ic_producto_nafin, ic_if, ic_epo, ic_mes_calculo, " +
					" ic_anio_calculo, cg_tipo_comision, " +
					" ig_doctos_vigentes, fg_monto_doctos, ic_moneda, ic_banco_fondeo, FG_CONTRI_COMISION, FG_SALDO_PROMEDIO ) " +
					" values("+ic_producto + "," + ic_if + "," + ic_epo +
					"," + ic_mes + "," + ic_anio +
					",'" + cg_tipo_comision + "'" +
					"," + ig_doctos_vigentes + "," + fg_monto_doctos + ", " + ic_moneda + ", " + banco_fondeo + ", " +  fn_contri_comision + ", " +  fg_saldo_promedio + ")";
			con.ejecutaSQL(strSQL);

			log.info("strSQL "+strSQL);

		} catch(SQLException e) {
			throw e;
		}
		log.info("guardarRegistroComision(S)::" + ic_if);

	}

/** 27/05/2004 18:03
 * Almacena en BD. el registro el detalle de la comision
 * @param ic_producto	Clave del producto nafin.
 * @param ic_if	Clave del IF.
 * @param ic_mes	Mes de calculo
 * @param ic_anio	A�o de c�lculo
 * @param cg_tipo_comision	Tipo de comision
 * @param fg_porcentaje_aplicado Porcentaje de comision aplicado
 * @param fg_contraprestacion Contraprestacion.
 * @param fg_saldo_promedio Saldo promedio mensual
 * @param banco_fondeo Banco de fondeo.
 * @param ic_moneda	Tipo de moneda Pesos o Dolares.
 */
	private void guardarRegistroComisionDetalle(AccesoDB con, String ic_producto, String ic_if,
			String ic_mes, String ic_anio, String cg_tipo_comision,
			String fg_porcentaje_aplicado, String fg_contraprestacion,
			String fg_saldo_promedio, String banco_fondeo, String ic_moneda ,
			String porcentajeAplicadoM1 , String contraprestacionM1 , String  porcentajeAplicadoM1V , String contraprestacionM1V ,
			String contraprestacionM2,	String fg_saldo_promedioM1, String fg_saldo_promedioM1V  )

			throws NamingException, SQLException {

		log.info("guardarRegistroComisionDetalle(E)::"+ic_if);

		List lVarBind		= new ArrayList();

      try {
			String strSQL = " insert into com_comision_fondeo_det " +
					" (ic_producto_nafin, ic_if, ic_mes_calculo, " +
					" ic_anio_calculo, cg_tipo_comision, fg_porcentaje_aplicado, " +
					" fg_contraprestacion, fg_saldo_promedio, ic_moneda, ic_banco_fondeo ,  "+
					" fg_porcentaje_aplicadoM1 , fg_porcentaje_aplicadoM1V  , FG_SALDO_PROMEDIOM1 ,   FG_SALDO_PROMEDIOM1V   , "+
					" FG_CONTRAPRESTACIONM1 ,  FG_CONTRAPRESTACIONM1V   , FG_CONTRAPRESTACIONM2   ) " +
					" values( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ?, ?  ) ";

			lVarBind.add(ic_producto);
			lVarBind.add(ic_if);
			lVarBind.add(ic_mes);
			lVarBind.add(ic_anio);
			lVarBind.add(cg_tipo_comision);
			lVarBind.add(fg_porcentaje_aplicado);
			lVarBind.add(fg_contraprestacion);
			lVarBind.add(fg_saldo_promedio);
			lVarBind.add(ic_moneda);
			lVarBind.add(banco_fondeo);
			lVarBind.add(porcentajeAplicadoM1);
			lVarBind.add(porcentajeAplicadoM1V);

			lVarBind.add(fg_saldo_promedioM1);
			lVarBind.add(fg_saldo_promedioM1V);

			lVarBind.add(contraprestacionM1);
			lVarBind.add(contraprestacionM1V);
			lVarBind.add(contraprestacionM2);

			log.debug("strSQL.toString() "+strSQL.toString());
			log.debug("lVarBind "+lVarBind );

			PreparedStatement ps  = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();

			log.info("guardarRegistroComisionDetalle(S)::"+ic_if);

		} catch(SQLException e) {
			throw e;
		}
	}

/** 27/05/2004 17:55
 * Almacena en BD. el registro del pago de comision con estatus de "No pagado"
 * @param ic_producto	Clave del producto nafin.
 * @param ic_if	Clave del IF.
 * @param ic_mes	Mes de calculo
 * @param ic_anio	A�o de c�lculo
 * @param fg_saldo_promedio	Saldo promedio del IF en la fecha especificada.
 * @param banco_fondeo Banco de fondeo.
 * @param ic_moneda	Tipo de moneda Pesos o Dolares.
 */

	private void guardarRegistroPago(AccesoDB con, String ic_producto, String ic_if,
			String ic_mes, String ic_anio, String banco_fondeo, String ic_moneda)
			throws NamingException, SQLException {

		try {
			String strSQL = " insert into com_pago_comision_fondeo " +
					" (ic_producto_nafin, ic_if, ic_mes_calculo, " +
					" ic_anio_calculo, cg_estatus_pagado, ic_moneda, ic_banco_fondeo) " +
					" values("+ic_producto + "," + ic_if +
					"," + ic_mes + "," + ic_anio + ", 'N', " + ic_moneda + ", " + banco_fondeo + ")";
			con.ejecutaSQL(strSQL);
		} catch(SQLException e) {
			throw e;
		}

		System.out.println(".::ComisionFondeoPropioBean::guardarRegistroPago::.");
	}


/**
 * Determina si la fecha actual es valida para poder realizar el calculo anual.
 * @return true solo si se cumple algunas de las siguientes condiciones:
 * 	a�o de calculo es anterior al a�o en curso.
 * 	a�o de calculo es igual a�o en curso y el mes actual es el ultimo dia de diciembre.
 */
	private boolean esFechaValidaParaCalculoAnual(String strAnioCalculo) {

		int anioCalculo = Integer.parseInt(strAnioCalculo);

		Calendar calFechaActual = Calendar.getInstance();
		int diaActual = calFechaActual.get(Calendar.DAY_OF_MONTH);
		int mesActual = calFechaActual.get(Calendar.MONTH) + 1;	//+1 porque Enero es 0 en Calendar.
		int anioActual = calFechaActual.get(Calendar.YEAR);

		return (anioCalculo < anioActual || anioCalculo == anioActual && mesActual == 12 && diaActual == 31);
	}


/**
 * Obtiene el saldo promedio de referencia.
 * El m�todo asume que la parametrizacion de meses de ajuste, est� establecida.
 * @param ic_if Clave del IF
 * @param ic_anio_calculo A�o del cual se desea obtener el promedio de referencia.
 * 		Este par�metro permite validar que el a�o a ajustar, est� parametrizado correctamente.
 * @return Saldo Promedio de Referencia.
 */
	private double calcularSaldoPromedioDeReferencia(String ic_if, String ic_anio_calculo, String ic_moneda, String ic_banco_fondeo)
			throws NamingException, SQLException, NafinException {

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		double dblSaldoPromedioDeReferencia = 0;
		int mesAjusteDe = 0;
		int mesAjusteA = 0;
		int anioAjuste = 0;


		try {
			con.conexionDB();

			String strSQL = "SELECT ig_mes_ajuste_de, ig_mes_ajuste_a, ig_anio_ajuste " +
					" FROM COMCAT_IF " +
					" WHERE ic_if = " + ic_if +
					" 	AND ig_anio_ajuste = " + ic_anio_calculo;
			rs = con.queryDB(strSQL);
			if (rs.next()) {
				mesAjusteDe = rs.getInt("ig_mes_ajuste_de");
				mesAjusteA = rs.getInt("ig_mes_ajuste_a");
				anioAjuste = rs.getInt("ig_anio_ajuste");
			} else {
				throw new NafinException("PARM0014");
			}

			strSQL =
					" SELECT SUM(fg_saldo_promedio)/ " + (mesAjusteA - mesAjusteDe + 1) +
					" 	AS saldoPromedioDeReferencia " +
					" FROM com_comision_fondeo_det " +
					" WHERE ic_if = " + ic_if +
					" 	AND ic_mes_calculo BETWEEN " + mesAjusteDe + " AND " + mesAjusteA +
					" 	AND ic_anio_calculo = " + anioAjuste +
					"	AND ic_moneda = " + ic_moneda +
					"	AND ic_banco_fondeo= " + ic_banco_fondeo ;

			rs = con.queryDB(strSQL);
			if (rs.next()) {
				dblSaldoPromedioDeReferencia = rs.getDouble("saldoPromedioDeReferencia");
			}

			rs.close();
			con.cierraStatement();
		} catch (SQLException sqle) {
			throw sqle;
		} catch (NamingException e) {
			throw e;
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return dblSaldoPromedioDeReferencia;
	}






/**
 * Guarda el registro del ajuste realizado.
 *
 * @param ic_producto_nafin	Clave del producto nafin.
 * @param ic_if	Clave del IF.
 * @param ic_mes_calculo Mes de calculo
 * @param ic_anio_calculo	A�o de c�lculo
 * @param cg_tipo_comision Tipo de comision F fija o R por Rangos
 * @param fg_contraprestacion_ajustada Contraprestacion ajustada
 */
	private void guardarAjuste(AccesoDB con, String ic_producto_nafin, String ic_if,
			String ic_mes_calculo, String ic_anio_calculo, String cg_tipo_comision,
			String fg_contraprestacion_ajustada, String strBancoFondeo, String strMoneda)
			throws SQLException {

		try {
			String strSQL = " UPDATE com_comision_fondeo_det " +
					" SET " +
					" fg_contraprestacion_ajustada = ROUND(" + fg_contraprestacion_ajustada + ", 2) " +
					" WHERE " +
					" ic_producto_nafin = " + ic_producto_nafin +
					" AND ic_if = " + ic_if +
					" AND ic_mes_calculo = " + ic_mes_calculo +
					" AND ic_anio_calculo = " + ic_anio_calculo +
					" AND cg_tipo_comision = '" + cg_tipo_comision + "'" +
					" AND ic_moneda = " + strMoneda +
					" AND ic_banco_fondeo = " + strBancoFondeo;
						con.ejecutaSQL(strSQL);

					System.out.println(".::ComisionFondeoPropioBean::guardaAjuste::." + strSQL);

		} catch(SQLException e) {
			throw e;
		}
	}


/**
 * Guarda el registro del ajuste anual, con estatus NO PAGADO
 * @param ic_if	Clave del IF.
 * @param ic_anio_calculo A�o de c�lculo
 * @param fg_tasa_ajustada Tasa resultante del ajuste
 * @param fg_saldo_referencia Saldo de referencia anual
 * @param ig_mes_base Mes de base utilizado para el calculo de la contraprestacion ajustada
 * @param ic_moneda Tipo de moneda Nacional o Dolares
 * @param ic_banco_fondeo Tipo de Banco de Fondeo
 */
	private void guardarRegistroPagoAjusteAnual(AccesoDB con, String ic_if,
			String ic_anio_calculo, String fg_tasa_ajustada, String fg_saldo_referencia, String ig_mes_base, String ic_moneda, String ic_banco_fondeo)
			throws SQLException {

		try {
			String strSQL = " INSERT INTO com_pago_ajuste_anual_fondeo " +
					" (ic_if, ic_anio_calculo, fg_porcentaje_ajustado, " +
					" fg_saldo_referencia, ig_mes_base, cg_estatus_pagado, ic_moneda, ic_banco_fondeo) " +
					" VALUES("+ ic_if + "," + ic_anio_calculo +
					", ROUND(" + fg_tasa_ajustada + ", 2)" +
					", ROUND(" + fg_saldo_referencia + ", 2)" +
					"," + ig_mes_base + ", 'N'," +
					ic_moneda + "," +
					ic_banco_fondeo + ")";
			con.ejecutaSQL(strSQL);
		} catch (SQLException sqle) {
			throw sqle;
		}
	}



	//////////////////////////////////////////////////////////////////
	/**
 * Almacena en la BD el porcentaje de la comisi�n mensual
 * @param lsCadena Cadena a guardar.
 * @param ic_producto_nafin Clave del producto Nafin
 * @param ic_if Clave del IF
 * @param ic_mes_calculo Mes del cual se guarda el detalle
 * @param ic_anio_calculo A�o del cual se guarda el detalle
 * @param cg_tipo_comision El detalle corresponde al
 * 		tipo de comision especificada. R Rangos, F Fija
  * @exception NafinException si existe un error durante la ejecuci�n del m�todo.
 */


 public boolean grabarPorcComisionMensual(String porc) throws NafinException
 {
 		AccesoDB con = new AccesoDB();
		boolean exito = true;
		System.out.println("ComisionFondeoPropio::grabarPorcComisionMensual(E)");
		try {
			con.conexionDB();
			// Se marcan todos como no pagados
			String strSQL = " UPDATE comcat_producto_nafin " +
					" SET FG_PORC_COMISION_FONDEO_AF  =  " + porc + " " +
					" WHERE IC_PRODUCTO_NAFIN = 1";

			System.out.println("strSQL: "+strSQL);
			con.ejecutaSQL(strSQL);

		} catch (SQLException e) {
			System.out.println("ComisionFondeoPropio::grabarPorcComisionMensual(exception)");
			exito = false;
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} catch (NamingException e) {
			System.out.println("ComisionFondeoPropio::grabarPorcComisionMensual(exception)");
			exito = false;
			System.out.println(e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		System.out.println("ComisionFondeoPropio::grabarPorcComisionMensual(S)");
		return exito;

 }

 	public List getDatosComisionAgteFondeador
		 (
			String 		mes,
			String 		anio,
			String 		cveMoneda,
			String 		cveBancoFondeo,
			String		cveProductoNafin
		 )
	throws NafinException
	{

		AccesoDB 		con 							= new AccesoDB();
		String 			qrySentencia				= null;
		List 				lVarBind						= new ArrayList();
		List				listaRegistros				= new ArrayList();
		Registros		registros					= null;


		System.out.println("ComisionFondeoPropioBean::getDatosComisionAgteFondeador(E)");
		generaDatosComisionAgteFondeador(mes,anio,cveMoneda,cveBancoFondeo,cveProductoNafin);

		try {

				String 			nombreMes 					= Fecha.getNombreDelMes(Integer.parseInt(mes));

				con.conexionDB();

				qrySentencia =
					"SELECT " +
					"	IC_MONEDA, " +
					"	IG_NUM_DOCTOS, " +
					"	FG_SALDO_PROMEDIO, " +
					"	FG_PORC_COMISION_ANUAL, " +
					"	FG_MONTO_COMISION " +
					"FROM " +
					"	COM_COMISION_FONDEO_AF " +
					"WHERE " +
					"	IC_BANCO_FONDEO     = ?      AND " +
					"	IC_MES              = ?      AND " +
					"	IC_ANIO             = ?      AND " +
					"	IC_MONEDA           = ?      AND " +
					"	IC_PRODUCTO_NAFIN   = ?";
				lVarBind.add(new Integer(cveBancoFondeo));
				lVarBind.add(new Integer(mes));
				lVarBind.add(new Integer(anio));
				lVarBind.add(new Integer(cveMoneda));
				lVarBind.add(new Integer(cveProductoNafin));
				registros = con.consultarDB(qrySentencia, lVarBind, false);

        //Fodea 001 Iva 2010
      String fecha   = "28/"+ mes + "/" + anio; // formato fecha: DD/MM/YYYY
      double valorIva = ((BigDecimal)Iva.getValor(fecha)).doubleValue();

      log.debug("valorIva :: "+valorIva);

				while(registros != null && registros.next()){
					HashMap registro = new HashMap();

					BigDecimal 	saldoPromedio 							= 	new BigDecimal(registros.getString("FG_SALDO_PROMEDIO").equals("")?"0.0":registros.getString("FG_SALDO_PROMEDIO"));
					BigDecimal	porcentajeCmsnAnual					=  new BigDecimal(registros.getString("FG_PORC_COMISION_ANUAL").equals("")?"0.0":registros.getString("FG_PORC_COMISION_ANUAL"));
					BigDecimal	subtotal									=	saldoPromedio.multiply(porcentajeCmsnAnual).divide(new BigDecimal("100"),2,BigDecimal.ROUND_HALF_UP);
					BigDecimal  comisionDiaria							=  subtotal.divide(new BigDecimal("360"),2,BigDecimal.ROUND_HALF_UP);
					BigDecimal	diasEfectivamenteTranscurridos	=	new BigDecimal(Integer.toString(Fecha.getNumeroDeDiasDelMes(mes+"/"+anio,"MM/yyyy")));
					BigDecimal	comisionMensual						=	comisionDiaria.multiply(diasEfectivamenteTranscurridos).setScale(2,BigDecimal.ROUND_HALF_UP);
					BigDecimal  iva										= 	new BigDecimal(valorIva);
					BigDecimal  ivaComisionMensual 					= 	comisionMensual.multiply(iva).setScale(2,BigDecimal.ROUND_HALF_UP);
					BigDecimal	total										=	comisionMensual.add(ivaComisionMensual).setScale(2,BigDecimal.ROUND_HALF_UP);

					registro.put("MES",											nombreMes + "&nbsp;-&nbsp;" + anio);
					registro.put("NUM_DOCTOS",									registros.getString("IG_NUM_DOCTOS"));
					registro.put("MONEDA",										getNombreMoneda(registros.getString("IC_MONEDA")));
					registro.put("SALDO_PROMEDIO",							"$&nbsp;" + Comunes.formatoDecimal(saldoPromedio,2,true));
					registro.put("PORC_COMISION_ANUAL",						Comunes.formatoDecimal(porcentajeCmsnAnual,5,true) + "&nbsp;%");
					registro.put("SUBTOTAL",									"$&nbsp;" + Comunes.formatoDecimal(subtotal,2,true));
					registro.put("COMISION_DIARIA",							"$&nbsp;" + Comunes.formatoDecimal(comisionDiaria,2,true));
					registro.put("DIAS_EFECTIVAMENTE_TRANSCURRIDOS", 	Comunes.formatoDecimal(diasEfectivamenteTranscurridos,0,false));
					registro.put("COMISION_MENSUAL",							"$&nbsp;" + Comunes.formatoDecimal(comisionMensual,2,true));
					registro.put("IVA",											"$&nbsp;" + Comunes.formatoDecimal(ivaComisionMensual,2,true));
					registro.put("TOTAL",										"$&nbsp;" + Comunes.formatoDecimal(total,2,true));
					listaRegistros.add(registro);
				}

		} catch(Exception e) {
			System.out.println("ComisionFondeoPropioBean::getDatosComisionAgteFondeador(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ComisionFondeoPropioBean::getDatosComisionAgteFondeador(S)");
		}
		return listaRegistros;
	}

	private BigDecimal getPorcentajeDeComisionAnual()
	throws NafinException{
		System.out.println("ComisionFondeoPropioBean::getPorcentajeDeComisionMensual(E)");
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String 			strPorcentaje  = null;
		BigDecimal		porcentaje		= null;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT " +
						"FG_PORC_COMISION_FONDEO_AF " +
					"FROM "+
						"COMCAT_PRODUCTO_NAFIN " +
					"WHERE "+
						"IC_PRODUCTO_NAFIN = ? ";
				lVarBind.add(new Integer(1));
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					strPorcentaje = registros.getString("FG_PORC_COMISION_FONDEO_AF");
					porcentaje=new BigDecimal(strPorcentaje.equals("")?"0.0":strPorcentaje);
				}

		} catch(Exception e) {
			System.out.println("ComisionFondeoPropioBean::getPorcentajeDeComisionMensual(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ComisionFondeoPropioBean::getPorcentajeDeComisionMensual(S)");
		}
		return porcentaje;
	}

	public String getPorcentajeDeComisionAnual(String ic_producto_nafin)
	throws NafinException{
		System.out.println("ComisionFondeoPropioBean::getPorcentajeDeComisionMensual(E)");
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String 			strPorcentaje  = null;
		BigDecimal		porcentaje		= null;

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT " +
						"FG_PORC_COMISION_FONDEO_AF " +
					"FROM "+
						"COMCAT_PRODUCTO_NAFIN " +
					"WHERE "+
						"IC_PRODUCTO_NAFIN = ? ";
				lVarBind.add(new Integer(ic_producto_nafin));
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					strPorcentaje = registros.getString("FG_PORC_COMISION_FONDEO_AF");
					porcentaje=new BigDecimal(strPorcentaje.equals("")?"0.0":strPorcentaje);
				}

		} catch(Exception e) {
			System.out.println("ComisionFondeoPropioBean::getPorcentajeDeComisionMensual(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ComisionFondeoPropioBean::getPorcentajeDeComisionMensual(S)");
		}
		return porcentaje.toPlainString();
	}

	private void generaDatosComisionAgteFondeador
		(
			String  		mes,
			String 		anio,
			String 		cveMoneda,
			String 		cveBancoFondeo,
			String		cveProductoNafin
		)
		throws NafinException
	{

		System.out.println("ComisionFondeoPropioBean::generaDatosComisionAgteFondeador(E)");
		AccesoDB 		con 							= new AccesoDB();
		String 			qrySentencia				= null;
		List 				lVarBind						= new ArrayList();
		Registros		registros					= null;
		boolean 			hayRegistros				= false;
		boolean			lbOK							= true;
		List				listaRegistros				= new ArrayList();
		BigDecimal		porcentajeCmsnAnual		= getPorcentajeDeComisionAnual();

		// VERIFICAR SI HAY DATOS
		try {
				con.conexionDB();
				qrySentencia =
               "SELECT " +
               "   CASE WHEN " +
               "     COUNT(?) = ? " +
               "   THEN " +
               "    'false' " +
               "   ELSE " +
               "    'true' " +
               "   END  " +
               "   AS HAY_REGISTROS " +
					"FROM " +
					"	COM_COMISION_FONDEO_AF " +
					"WHERE " +
               "            IC_BANCO_FONDEO       = ?       AND " +
               "            IC_MES                = ?       AND " +
               "            IC_ANIO               = ?       AND " +
               "            IC_MONEDA             = ?       AND " +
               "            IC_PRODUCTO_NAFIN     = ?  ";
				lVarBind.add(new Integer(1));
				lVarBind.add(new Integer(0));
				lVarBind.add(new Integer(cveBancoFondeo)); 	// IC_BANCO_FONDEO
				lVarBind.add(new Integer(mes)); 					// IC_MES
				lVarBind.add(new Integer(anio)); 				// IC_ANIO
				lVarBind.add(new Integer(cveMoneda)); 			// IC_MONEDA
				lVarBind.add(new Integer(cveProductoNafin)); // IC_PRODUCTO_NAFIN
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() ){
					hayRegistros = Boolean.valueOf(registros.getString("HAY_REGISTROS")).booleanValue();
				}
		} catch(Exception e) {
			System.out.println("ComisionFondeoPropioBean::generaDatosComisionAgteFondeador(Exception) "+e);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}

		// GENERAR DATOS DE LA TABLA
		lVarBind.clear();
		if(!hayRegistros){
			try {
				String   tipoDeCambio				= getTipoDeCambio(mes,anio,cveMoneda);
				String 	primerDiaDelMes 			= "01/"+mes+"/"+anio;
				String 	ultimoDiaDelMes 			= Fecha.getUltimoDiaDelMes(primerDiaDelMes,"dd/MM/yyyy");
				String	[]arryFechaUltmoDiaMes	= ultimoDiaDelMes.split("/");
				String	numeroDelDiaUltimo		= arryFechaUltmoDiaMes[0];
				int 		diaUltimo					= Integer.parseInt(numeroDelDiaUltimo);

				qrySentencia =
					" SELECT "+
					"              COUNT(?) AS NUM_DOCTOS, "+
					"					D.IC_MONEDA IC_MONEDA,"+
					"					IEXP.IC_PRODUCTO_NAFIN IC_PRODUCTO_NAFIN," +
					"              SUM( "+
					" 		(CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_descuento, 'YYYY')) >  ?   THEN ? "+
					" 		ELSE  "+
					" 			CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_descuento, 'MM')) >  ?   THEN ? "+
					" 			ELSE "+
					" 				TO_NUMBER(TO_CHAR(sa.df_v_descuento, 'DD')) - ? "+
					"                     	END "+
					" 		END "+
					" 		- "+
					" 		CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY'))  <  ?   THEN ? "+
					" 		ELSE "+
					" 			CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) <  ?   THEN ? "+
					" 			ELSE "+
					" 			 TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'DD')) "+
					"                     	END "+
					"                 END 	+ ? ) "+
					//"                            * d.fn_monto  "+ (cveMoneda.equals("1")?"":" * ? ")+")  / ? as saldos_promedios, ";
          "                            * ds.in_importe_recibir  "+ (cveMoneda.equals("1")?"":" * ? ")+")  / ? as saldos_promedios, "; //FODEA 015 - 2009 ACF
				for(int i=1;i<=diaUltimo;i++){
					qrySentencia += " SUM " +
											"( " +
												" CASE WHEN " +
                                    	 " TRUNC(TO_DATE(?, 'dd/mm/yyyy')) between TRUNC(sa.df_fecha_solicitud) and TRUNC(sa.df_v_descuento - ?) " +
											   " THEN " +
													//" d.fn_monto " + (cveMoneda.equals("1")?"":" * ? ") +
                          " ds.in_importe_recibir " + (cveMoneda.equals("1")?"":" * ? ") + //FODEA 015 - 2009 ACF
												" ELSE " +
													" ? " +
												" END " +
                                 ") " ;
					if(i!=diaUltimo)
						qrySentencia += " || ',' || ";
				}
				qrySentencia +=
					" 	as SALDOS_X_DIA " +
					" FROM com_documento d, "+
					" 	com_docto_seleccionado ds, "+
					" 	com_solicitud sa, "+
					" 	comcat_producto_nafin cpn, "+
					" 	comrel_if_epo_x_producto iexp "+
					" WHERE "+
					" 	sa.ic_documento = ds.ic_documento "+
					" 	AND ds.ic_documento = d.ic_documento "+
					" 	AND ds.ic_if = iexp.ic_if "+
					" 	AND d.ic_epo = iexp.ic_epo "+
					"	AND cpn.ic_producto_nafin = iexp.ic_producto_nafin "+
					" 	AND iexp.ic_producto_nafin = ? "+
					"	AND sa.ic_estatus_solic in (?, ?) "+
					" 	AND d.ic_moneda = ? "+
					"	AND sa.df_v_descuento >= trunc(TO_DATE(?, 'DD/MM/YYYY')) "+
					"	AND sa.df_fecha_solicitud <= trunc(TO_DATE(?, 'DD/MM/YYYY')) "+
					"            and sa.ic_banco_fondeo = ? "+
					"            group by d.ic_moneda, IEXP.IC_PRODUCTO_NAFIN ";
				lVarBind.add(new Integer(1));
				lVarBind.add(new Integer(anio));
				lVarBind.add(new Integer(numeroDelDiaUltimo));
				lVarBind.add(new Integer(mes));
				lVarBind.add(new Integer(numeroDelDiaUltimo));
				lVarBind.add(new Integer(1));
				lVarBind.add(new Integer(anio));
				lVarBind.add(new Integer(1));
				lVarBind.add(new Integer(mes));
				lVarBind.add(new Integer(1));
				lVarBind.add(new Integer(1));
				if(!cveMoneda.equals("1")) lVarBind.add(tipoDeCambio);
				lVarBind.add(new Integer(numeroDelDiaUltimo));
				for(int i=1;i<=diaUltimo;i++){
					lVarBind.add(i+"/"+mes+"/"+anio);
					lVarBind.add(new Integer(1));
					if(!cveMoneda.equals("1")) lVarBind.add(tipoDeCambio);
					lVarBind.add(new Integer(0));
				}
				lVarBind.add(new Integer(cveProductoNafin));
				lVarBind.add(new Integer(3));
				lVarBind.add(new Integer(5));
				lVarBind.add(new Integer(cveMoneda));
				lVarBind.add(primerDiaDelMes);
				lVarBind.add(ultimoDiaDelMes);
				lVarBind.add(new Integer(cveBancoFondeo));
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next()){
					HashMap registro = new HashMap();

					// Calcular Monto de la Comision
					BigDecimal 	saldosPromedios 	= 	new BigDecimal(registros.getString("SALDOS_PROMEDIOS").equals("")?"0.0":registros.getString("SALDOS_PROMEDIOS"));
									saldosPromedios 	= 	saldosPromedios.setScale(2,BigDecimal.ROUND_HALF_UP);
					BigDecimal	subtotal				=	saldosPromedios.multiply(porcentajeCmsnAnual).divide(new BigDecimal("100"),2,BigDecimal.ROUND_HALF_UP);
					BigDecimal  comisionDiaria		=  subtotal.divide(new BigDecimal("360"),2,BigDecimal.ROUND_HALF_UP);
					BigDecimal	diasEfectivamenteTranscurridos	=	new BigDecimal(Integer.toString(Fecha.getNumeroDeDiasDelMes(mes+"/"+anio,"MM/yyyy")));
					BigDecimal	comisionMensual	=	comisionDiaria.multiply(diasEfectivamenteTranscurridos).setScale(2,BigDecimal.ROUND_HALF_UP);


					registro.put("IC_BANCO_FONDEO",			cveBancoFondeo);
					registro.put("IC_MES",						mes);
					registro.put("IC_ANIO",						anio);
					registro.put("IC_MONEDA",					registros.getString("IC_MONEDA"));
					registro.put("IC_PRODUCTO_NAFIN",		registros.getString("IC_PRODUCTO_NAFIN"));
					registro.put("IG_NUM_DOCTOS",				registros.getString("NUM_DOCTOS"));
					registro.put("FG_SALDO_PROMEDIO",		Comunes.formatoDecimal(saldosPromedios,2,false));
					registro.put("FG_PORC_COMISION_ANUAL",	Comunes.formatoDecimal(porcentajeCmsnAnual,5,false));
					registro.put("FG_MONTO_COMISION",		Comunes.formatoDecimal(comisionMensual,2,false));
					registro.put("CS_PAGADO",					"N");
					registro.put("CG_SALDOS_X_DIA",			registros.getString("SALDOS_X_DIA"));

					listaRegistros.add(registro);
				}

			} catch(Exception e) {
				System.out.println("ComisionFondeoPropioBean::generaDatosComisionAgteFondeador(Exception) "+e);
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
				e.printStackTrace();
				throw new NafinException("SIST0001");
			}

			// INSERTAR DATOS
			try {
				qrySentencia =
					"INSERT INTO COM_COMISION_FONDEO_AF " +
					"	( " +
					"		IC_BANCO_FONDEO, " +
					"		IC_MES, " +
					"		IC_ANIO, " +
					" 		IC_MONEDA, " +
					"		IC_PRODUCTO_NAFIN, " +
					"		IG_NUM_DOCTOS, " +
					"		FG_SALDO_PROMEDIO, " +
					"		FG_PORC_COMISION_ANUAL, " +
					"		FG_MONTO_COMISION, " +
					"		CS_PAGADO, " +
					"		CG_SALDOS_X_DIA " +
					"	) " +
					"VALUES " +
					"	( " +
					"		?, " +
					"		?, " +
					"		?, " +
					"		?, " +
					"		?, " +
					"		?, " +
					"		?, " +
					"		?, " +
					"		?, " +
					"		?, " +
					"		?  " +
					"	) ";

				if(listaRegistros != null && listaRegistros.size() > 0){
					for(int i=0;i<listaRegistros.size();i++){
						HashMap registro = (HashMap)listaRegistros.get(i);

						lVarBind.clear();

						lVarBind.add(registro.get("IC_BANCO_FONDEO"));
						lVarBind.add(registro.get("IC_MES"));
						lVarBind.add(registro.get("IC_ANIO"));
						lVarBind.add(registro.get("IC_MONEDA"));
						lVarBind.add(registro.get("IC_PRODUCTO_NAFIN"));
						lVarBind.add(registro.get("IG_NUM_DOCTOS"));
						lVarBind.add(registro.get("FG_SALDO_PROMEDIO"));
						lVarBind.add(registro.get("FG_PORC_COMISION_ANUAL"));
						lVarBind.add(registro.get("FG_MONTO_COMISION"));
						lVarBind.add(registro.get("CS_PAGADO"));
						lVarBind.add(registro.get("CG_SALDOS_X_DIA"));

						con.ejecutaUpdateDB(qrySentencia, lVarBind);
					}
				}

			} catch(Exception e) {
				System.out.println("ComisionFondeoPropioBean::generaDatosComisionAgteFondeador(Exception) "+e);
				e.printStackTrace();
				lbOK = false;
				throw new NafinException("SIST0001");
			} finally {
				con.terminaTransaccion(lbOK);
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
				System.out.println("ComisionFondeoPropioBean::generaDatosComisionAgteFondeador(S)");
			}
		}
	}

	private String getNombreMoneda(String cveMoneda)
	throws NafinException{
		System.out.println("ComisionFondeoPropioBean::getNombreMoneda(E)");
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String 			nombreMoneda  	= null;


		try {
				con.conexionDB();
				qrySentencia =
					" SELECT " +
					"    CD_NOMBRE NOMBRE_MONEDA " +
					" FROM " +
					"    COMCAT_MONEDA " +
					" WHERE " +
					"    IC_MONEDA = ?";
				lVarBind.add(new Integer(cveMoneda));
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() )
					nombreMoneda = registros.getString("NOMBRE_MONEDA");

		} catch(Exception e) {
			System.out.println("ComisionFondeoPropioBean::getNombreMoneda(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ComisionFondeoPropioBean::getNombreMoneda(S)");
		}
		return nombreMoneda;
	}

	public List getRsmnCobroComisionAgteFondeador
		 (
			String 		mes,
			String 		anio,
			String 		cveMoneda,
			String 		cveBancoFondeo,
			String		cveProductoNafin
		 )
	throws NafinException
	{

		AccesoDB 		con 							= new AccesoDB();
		String 			qrySentencia				= null;
		List 				lVarBind						= new ArrayList();
		List				listaRegistros				= new ArrayList();
		Registros		registros					= null;

		System.out.println("ComisionFondeoPropioBean::getRsmnCobroComisionAgteFondeador(E)");

		try {

				String 			nombreMes 					= Fecha.getNombreDelMes(Integer.parseInt(mes));

				con.conexionDB();

				qrySentencia =
					"SELECT " +
					"	FG_MONTO_COMISION, " +
					"	CS_PAGADO " +
					"FROM " +
					"	COM_COMISION_FONDEO_AF " +
					"WHERE " +
					"	IC_BANCO_FONDEO     = ?      AND " +
					"	IC_MES              = ?      AND " +
					"	IC_ANIO             = ?      AND " +
					"	IC_MONEDA           = ?      AND " +
					"	IC_PRODUCTO_NAFIN   = ?";
				lVarBind.add(new Integer(cveBancoFondeo));
				lVarBind.add(new Integer(mes));
				lVarBind.add(new Integer(anio));
				lVarBind.add(new Integer(cveMoneda));
				lVarBind.add(new Integer(cveProductoNafin));
				registros = con.consultarDB(qrySentencia, lVarBind, false);

        //Fodea 01-tasa IVa 2010
        String fecha   = "28/"+ mes + "/" + anio; // formato fecha: DD/MM/YYYY
        String porcentajeIva   = Iva.getPorcentaje(fecha);
        double valorIva = ((BigDecimal)Iva.getValor(fecha)).doubleValue();
        log.debug(" Iva ::: "+valorIva);

				while(registros != null && registros.next()){
					HashMap registro = new HashMap();

					// Calcular IVA de la COMISION
					BigDecimal comision			= new BigDecimal(registros.getString("FG_MONTO_COMISION").equals("")?"0.0":(String)registros.getString("FG_MONTO_COMISION"));
					BigDecimal iva					= new BigDecimal(valorIva);
					BigDecimal ivaComision 		= comision.multiply(iva).setScale(2,BigDecimal.ROUND_HALF_UP);
					BigDecimal comisionTotal	= new BigDecimal("0.0");

					comisionTotal = comision.add(ivaComision);

					registro.put("MES",							nombreMes + " - " + anio);
					registro.put("COMISION",					Comunes.formatoDecimal(comision,2,false));
					registro.put("IVA",							  porcentajeIva +"%");
					registro.put("IVA_COMISION",				Comunes.formatoDecimal(ivaComision,2,false));
					registro.put("COMISION_TOTAL",			Comunes.formatoDecimal(comisionTotal,2,false));
					registro.put("ESTATUS_PAGADO",			registros.getString("CS_PAGADO"));

					listaRegistros.add(registro);
				}

		} catch(Exception e) {
			System.out.println("ComisionFondeoPropioBean::getRsmnCobroComisionAgteFondeador(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ComisionFondeoPropioBean::getRsmnCobroComisionAgteFondeador(S)");
		}
		return listaRegistros;
	}

	public void setStatusPagadoToComisionAgteFondeador(
			String 		mes,
			String 		anio,
			String 		cveMoneda,
			String 		cveBancoFondeo,
			String		cveProductoNafin
		 )
	throws NafinException
	{
		System.out.println("ComisionFondeoPropioBean::setStatusPagadoToComisionAgteFondeador(E)");
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		boolean			lbOK				= true;


		try {

				con.conexionDB();
				qrySentencia =
					"UPDATE COM_COMISION_FONDEO_AF " +
					"	SET " +
					"    CS_PAGADO = ? " +
					"WHERE "+
					"	IC_BANCO_FONDEO     = ?      AND " +
					"	IC_MES              = ?      AND " +
					"	IC_ANIO             = ?      AND " +
					"	IC_MONEDA           = ?      AND " +
					"	IC_PRODUCTO_NAFIN   = ? ";
				lVarBind.add("S");
				lVarBind.add(new Integer(cveBancoFondeo));
				lVarBind.add(new Integer(mes));
				lVarBind.add(new Integer(anio));
				lVarBind.add(new Integer(cveMoneda));
				lVarBind.add(new Integer(cveProductoNafin));

				con.ejecutaUpdateDB(qrySentencia, lVarBind);

			} catch(Exception e) {
				System.out.println("ComisionFondeoPropioBean::setStatusPagadoToComisionAgteFondeador(Exception) "+e);
				e.printStackTrace();
				lbOK = false;
				throw new NafinException("SIST0001");
			} finally {
				con.terminaTransaccion(lbOK);
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
				System.out.println("ComisionFondeoPropioBean::setStatusPagadoToComisionAgteFondeador(S)");
			}
	}

	public List getResumenesCobroComisionAgteFondeador
		 (
			String 		mes_inicial,
			String		mes_final,
			String 		anio,
			String 		cveMoneda,
			String 		cveBancoFondeo,
			String		cveProductoNafin,
			String 		pagado
		 )
	throws NafinException
	{

		AccesoDB 		con 							= new AccesoDB();
		String 			qrySentencia				= null;
		List 				lVarBind						= new ArrayList();
		List				listaRegistros				= new ArrayList();
		Registros		registros					= null;

		System.out.println("ComisionFondeoPropioBean::getResumenesCobroComisionAgteFondeador(E)");

		try {
				int mesInicio 	= 	Integer.parseInt(mes_inicial);
				int mesFin		=	Integer.parseInt(mes_final);
				String qMeses 	= 	"";

				for(int mes=mesInicio;mes<=mesFin;mes++){
					qMeses += "?";
					if(mes!=mesFin)
						qMeses += ",";
				}

				con.conexionDB();

				qrySentencia =
					"SELECT " +
               "  IC_MES, " +
               "  FG_SALDO_PROMEDIO, " +
					"	FG_MONTO_COMISION, " +
					"	CS_PAGADO " +
					"FROM " +
					"	COM_COMISION_FONDEO_AF " +
					"WHERE " +
					"	IC_BANCO_FONDEO     = ?        		AND " +
					"	IC_MES              IN ("+qMeses+") AND " +
					"	IC_ANIO             = ?     			AND " +
					"	IC_MONEDA           = ?        		AND " +
					"	IC_PRODUCTO_NAFIN   = ?        		AND " +
               "  CS_PAGADO           = ? " +
					"ORDER BY IC_MES ASC " ;

				lVarBind.add(new Integer(cveBancoFondeo));
				for(int mes=mesInicio;mes<=mesFin;mes++){
					lVarBind.add(new Integer(mes));
				}
				lVarBind.add(new Integer(anio));
				lVarBind.add(new Integer(cveMoneda));
				lVarBind.add(new Integer(cveProductoNafin));
				lVarBind.add(pagado);

				registros = con.consultarDB(qrySentencia, lVarBind, false);
				//Fodea 01  tsa Iva 2010
        String fecha   = "01/"+ mes_final + "/" + anio;
        String porcentajeIva   = Iva.getPorcentaje(fecha);
        double valorIva = ((BigDecimal)Iva.getValor(fecha)).doubleValue();

        log.debug("porcentajeIva::  "+porcentajeIva);

				while(registros != null && registros.next()){
					HashMap registro = new HashMap();

					// Calcular IVA de la COMISION
					BigDecimal comision			= new BigDecimal(registros.getString("FG_MONTO_COMISION").equals("")?"0.0":(String)registros.getString("FG_MONTO_COMISION"));
					BigDecimal iva					= new BigDecimal(valorIva); //Fodea tasa iva
					BigDecimal ivaComision 		= comision.multiply(iva).setScale(2,BigDecimal.ROUND_HALF_UP);
					BigDecimal comisionTotal	= new BigDecimal("0.0");

					comisionTotal = comision.add(ivaComision);

					registro.put("MES",							Fecha.getNombreDelMes(Integer.parseInt(registros.getString("IC_MES"))));
					registro.put("ANIO",							anio);
					registro.put("SALDO_PROMEDIO",			Comunes.formatoDecimal(registros.getString("FG_SALDO_PROMEDIO").equals("")?"0.0":(String)registros.getString("FG_SALDO_PROMEDIO"),2,false));
					registro.put("COMISION",					Comunes.formatoDecimal(comision,2,false));
					registro.put("IVA",							porcentajeIva +"%"); //Fodea tasa iva
					registro.put("IVA_COMISION",				Comunes.formatoDecimal(ivaComision,2,false));
					registro.put("COMISION_TOTAL",			Comunes.formatoDecimal(comisionTotal,2,false));
					registro.put("ESTATUS_PAGADO",			registros.getString("CS_PAGADO"));

					listaRegistros.add(registro);
				}

		} catch(Exception e) {
			System.out.println("ComisionFondeoPropioBean::getResumenesCobroComisionAgteFondeador(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ComisionFondeoPropioBean::getResumenesCobroComisionAgteFondeador(S)");
		}
		return listaRegistros;
	}

	public List getListaConSaldosPorDiaYTotalDelMes
		 (
			String 		mes,
			String 		anio,
			String 		cveMoneda,
			String 		cveBancoFondeo,
			String		cveProductoNafin
		 )
	throws NafinException
	{

		AccesoDB 		con 							= new AccesoDB();
		String 			qrySentencia				= null;
		List 				lVarBind						= new ArrayList();
		List				listaRegistros				= new ArrayList();
		Registros		registros					= null;

		System.out.println("ComisionFondeoPropioBean::getListaDeSaldosPorDia(E)");

		try {
				con.conexionDB();

				qrySentencia =
					"SELECT " +
					"	CG_SALDOS_X_DIA " +
					"FROM " +
					"	COM_COMISION_FONDEO_AF " +
					"WHERE " +
					"	IC_BANCO_FONDEO     = ?      AND " +
					"	IC_MES              = ?      AND " +
					"	IC_ANIO             = ?      AND " +
					"	IC_MONEDA           = ?      AND " +
					"	IC_PRODUCTO_NAFIN   = ?";
				lVarBind.add(new Integer(cveBancoFondeo));
				lVarBind.add(new Integer(mes));
				lVarBind.add(new Integer(anio));
				lVarBind.add(new Integer(cveMoneda));
				lVarBind.add(new Integer(cveProductoNafin));
				registros = con.consultarDB(qrySentencia, lVarBind, false);

				while(registros != null && registros.next()){
					HashMap registro = new HashMap();

					String  			listaDeSaldosPorDia			= registros.getString("CG_SALDOS_X_DIA");
					StringBuffer  	listaDeSaldosPorDiaConFmt	= new StringBuffer();
					String  			[]arryListaDeSaldosPorDia 	= listaDeSaldosPorDia.split(",");
					BigDecimal		totalSaldosDelMes				= new BigDecimal("0.00");

					for(int indice=0;indice<arryListaDeSaldosPorDia.length;indice++){
						BigDecimal saldoDelDia 	= new BigDecimal(arryListaDeSaldosPorDia[indice]);
						totalSaldosDelMes 		= totalSaldosDelMes.add(saldoDelDia);
						listaDeSaldosPorDiaConFmt.append("\"$ " + Comunes.formatoDecimal(saldoDelDia,2,true) + "\",");
					}

					registro.put("LISTA_SALDOS_POR_DIA",	listaDeSaldosPorDiaConFmt.toString());
					registro.put("TOTAL_SALDOS_DEL_MES",	"\"$ " + Comunes.formatoDecimal(totalSaldosDelMes,2,true) + "\"");

					listaRegistros.add(registro);
				}

		} catch(Exception e) {
			System.out.println("ComisionFondeoPropioBean::getListaDeSaldosPorDia(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ComisionFondeoPropioBean::getListaDeSaldosPorDia(S)");
		}
		return listaRegistros;
	}

	public String getTipoDeCambio(String mes, String anio, String cveMoneda)
	throws NafinException{
		System.out.println("ComisionFondeoPropioBean::getTipoDeCambio(E)");
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String 			tipoCambio  	= "";

		try {
				con.conexionDB();
				qrySentencia =
					"SELECT " +
					"	FN_VALOR_COMPRA TIPO_CAMBIO " +
					"FROM " +
					"  COM_TIPO_CAMBIO " +
					"WHERE " +
					"  IC_MONEDA = ? AND " +
					"	DC_FECHA  >= TO_DATE(?,'mm/yyyy') AND " +
					"  DC_FECHA  < LAST_DAY(TO_DATE(?,'mm/yyyy'))+? AND " +
					"  ROWNUM = ? " +
					"ORDER BY DC_FECHA DESC ";
				lVarBind.add(new Integer(cveMoneda));
				lVarBind.add(mes + "/" + anio);
				lVarBind.add(mes + "/" + anio);
				lVarBind.add(new Integer(1));
				lVarBind.add(new Integer(1));
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				if(registros != null && registros.next() )
					tipoCambio = registros.getString("TIPO_CAMBIO");

		} catch(Exception e) {
			System.out.println("ComisionFondeoPropioBean::getTipoDeCambio(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ComisionFondeoPropioBean::getTipoDeCambio(S)");
		}
		return tipoCambio;
	}

	public Registros getComisionResumen(String ic_if,String mes_calculo,String anio_calculo,String ic_moneda,String ic_banco_fondeo){

		log.info("getComisionResumen(E)");
		AccesoDB 		con 				= new AccesoDB();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String strConsulta = "";
		String fecha 				= "01/"+ mes_calculo + "/" + anio_calculo;
		try {
				con.conexionDB();
				double iva 					= ((BigDecimal)Iva.getValor(fecha)).doubleValue();
				//String porcentajeIva 	= Iva.getPorcentaje(fecha);
				strConsulta=			" SELECT  " +
					" 	cfd.ic_producto_nafin, pn.ic_nombre, i.cg_razon_social as nombreIF, " +
					" 	pcf.cg_estatus_pagado, " +
					" 	SUM(cfd.fg_contraprestacion) as contraprestacion, " +
					" 	SUM(cfd.fg_contraprestacion) *"+String.valueOf(iva)+" as iva, " +
					" 	SUM(cfd.fg_contraprestacion) *"+String.valueOf(1 + iva)+" as contraprestacionTotal " +
					" FROM  " +
					" 	com_comision_fondeo_det cfd, comcat_producto_nafin pn, comcat_if i, " +
					" 	com_pago_comision_fondeo pcf " +
					" WHERE  " +
					" 	cfd.ic_producto_nafin = pcf.ic_producto_nafin " +
					" 	AND cfd.ic_if = pcf.ic_if " +
					" 	AND cfd.ic_anio_calculo = pcf.ic_anio_calculo " +
					" 	AND cfd.ic_mes_calculo = pcf.ic_mes_calculo " +
					" 	AND cfd.ic_producto_nafin = pn.ic_producto_nafin " +
					" 	AND cfd.ic_if = i.ic_if " +
					" 	AND cfd.ic_if = ?  " +
					" 	AND cfd.ic_anio_calculo = ?  " +
					" 	AND cfd.ic_mes_calculo = ?  " +
					"	AND cfd.ic_moneda = pcf.ic_moneda " +
					"	AND cfd.ic_banco_fondeo = pcf.ic_banco_fondeo " +
					"	AND cfd.ic_banco_fondeo = ? " + 	//Banco de fondeo
					"	AND cfd.ic_moneda = ? " + 			//Pesos o Dolares
					" GROUP BY cfd.ic_producto_nafin, pn.ic_nombre, i.cg_razon_social, " +
					" 	pcf.cg_estatus_pagado " +
					" ORDER BY cfd.ic_producto_nafin ";
					lVarBind.add(ic_if);
					lVarBind.add(anio_calculo);
					lVarBind.add(mes_calculo);
					lVarBind.add(ic_banco_fondeo);
					lVarBind.add(ic_moneda);
					registros = con.consultarDB(strConsulta, lVarBind, false);


		} catch(Exception e) {
				log.info("ComisionFondeoPropioBean::getComisionResumen(Exception) "+e);
				e.printStackTrace();
		} finally {
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
						log.info("ComisionFondeoPropioBean::getComisionResumen(S)");
		}

				log.info(".::strConsulta::. " + strConsulta );
				log.debug(" lVarBind "+lVarBind);
				return registros;

	}

	public Registros getAjusteAnual(String ic_if,String mes_calculo,String anio_calculo,String ic_moneda,String ic_banco_fondeo){

		log.info("getAjusteAnual(E)");
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		try {
				con.conexionDB();
				qrySentencia =
						" SELECT i.cg_razon_social AS NombreIF,  " +
						" 	cfd.cg_tipo_comision, " +
						" 	pn.ic_producto_nafin, " +
						" 	pn.ic_nombre, " +
						" 	pcf.ic_mes_calculo, " +
						" 	pcf.cg_estatus_pagado, " +
						" 	cfd.fg_saldo_promedio,  " +
						" 	cfd.fg_porcentaje_aplicado, " +
						" 	cfd.fg_contraprestacion, " +
						" 	paaf.fg_porcentaje_ajustado, " +
						" 	cfd.fg_contraprestacion_ajustada, " +
						" 	paaf.fg_saldo_referencia " +
						" FROM " +
						" 	comcat_if i, com_comision_fondeo_det cfd,  " +
						" 	com_pago_comision_fondeo pcf, comcat_producto_nafin pn, " +
						" 	com_pago_ajuste_anual_fondeo paaf " +
						" WHERE " +
						" 	pcf.ic_if = i.ic_if " +
						" 	AND pcf.ic_if = paaf.ic_if " +
						" 	AND pcf.ic_anio_calculo = paaf.ic_anio_calculo " +
						" 	AND pcf.ic_producto_nafin = pn.ic_producto_nafin " +
						" 	AND pcf.ic_producto_nafin = cfd.ic_producto_nafin " +
						" 	AND pcf.ic_if = cfd.ic_if " +
						" 	AND pcf.ic_mes_calculo = cfd.ic_mes_calculo " +
						" 	AND pcf.ic_anio_calculo = cfd.ic_anio_calculo " +
						"	AND cfd.ic_moneda = pcf.ic_moneda " +
						"	AND cfd.ic_banco_fondeo = pcf.ic_banco_fondeo " +
						"	AND pcf.ic_moneda = paaf.ic_moneda " +
						"  AND pcf.ic_banco_fondeo = paaf.ic_banco_fondeo " +
						" 	AND pcf.ic_if = ?" +
						" 	AND pcf.ic_anio_calculo = ?"  +
						" 	AND cfd.ic_moneda = ?" + 					//Pesos o Dolares
						"	AND cfd.ic_banco_fondeo = ?" + //Banco de fondeo
						" ORDER BY cfd.cg_tipo_comision, pn.ic_producto_nafin, " +
						" 	pcf.ic_mes_calculo ";
						lVarBind.add(ic_if);
					lVarBind.add(anio_calculo);
					lVarBind.add(ic_moneda);
					lVarBind.add(ic_banco_fondeo);

					registros = con.consultarDB(qrySentencia, lVarBind, false);


		} catch(Exception e) {
				log.info("ComisionFondeoPropioBean::getAjusteAnual(Exception) "+e);
				e.printStackTrace();
		} finally {
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
						log.info("ComisionFondeoPropioBean::getAjusteAnual(S)");
		}
				log.info(".::strConsulta::. " + qrySentencia );
				log.debug(" lVarBind "+lVarBind);
				return registros;

	}


	/**
	 *  Metodo que me permite saber la parametrizaci�n de la pantalla
	 *   Administraci�n/Comisi�n Recursos Propios/Parametrizaci�n/ Parametrizaci�n IF/ Generar o Especifica
	 * @return
	 * @param ic_if
	 */
	public   HashMap  getParametrizacionIF (String ic_if ){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer strQry = new StringBuffer();
		String comision ="", comiDesElect ="", comiDesMercantil ="",
				comiVentCar ="",  comiModalidad2 ="", com_fija ="";
		HashMap informacion = new HashMap();

		List lVarBind		= new ArrayList();
		log.info("getParametrizacionIF (E)");

		 try{

			con.conexionDB();

			strQry = new StringBuffer();
			strQry.append(" select CG_COMISION  from com_param_if  "+
			" where ic_if  =  ?  ");
			lVarBind		= new ArrayList();
			lVarBind.add(new Integer(ic_if));
			log.debug("strQry  " +strQry);
			log.debug("lVarBind  " +lVarBind);

			ps = con.queryPrecompilado(strQry.toString(),lVarBind );
         rs = ps.executeQuery();

			if(rs.next()){
				comision= (rs.getString("CG_COMISION")==null?"":rs.getString("CG_COMISION"));
         }
			rs.close();
       	ps.close();


			if(comision.equals("E") )  {  //Especifica

				strQry = new StringBuffer();
				strQry.append(" SELECT   "+
								" FG_PORC_COMISION_DE  , "+ //
								" FG_PORC_COMISION_DM  , "+
								" FG_PORC_COMISION_VC  , "+
								" FG_PORC_COMISION_M2    "+
								" from com_param_if      "+
								" where ic_if  =  ?      "+
								" and CG_COMISION = ? "  );
				lVarBind		= new ArrayList();
				lVarBind.add(new Integer(ic_if));
				lVarBind.add("E");
				log.debug("strQry  " +strQry);
				log.debug("lVarBind  " +lVarBind);

				ps = con.queryPrecompilado(strQry.toString(),lVarBind );
				rs = ps.executeQuery();
				if(rs.next()){
					comiDesElect = (rs.getString("FG_PORC_COMISION_DE")==null?"":rs.getString("FG_PORC_COMISION_DE"));
					comiDesMercantil = (rs.getString("FG_PORC_COMISION_DM")==null?"":rs.getString("FG_PORC_COMISION_DM"));
					comiVentCar =  (rs.getString("FG_PORC_COMISION_VC")==null?"":rs.getString("FG_PORC_COMISION_VC"));
					comiModalidad2 = (rs.getString("FG_PORC_COMISION_M2")==null?"":rs.getString("FG_PORC_COMISION_M2"));
				}
				rs.close();
				ps.close();

			}else  { //General

				strQry = new StringBuffer();
				strQry.append("  select fg_porc_comision_fondeo  "+
					" from comcat_producto_nafin    "+
					" Where ic_producto_nafin not in ( ? )  "  );
				lVarBind		= new ArrayList();
				lVarBind.add("3");

				log.debug("strQry  " +strQry);
				log.debug("lVarBind  " +lVarBind);

				ps = con.queryPrecompilado(strQry.toString(),lVarBind );
				rs = ps.executeQuery();
				if(rs.next()){
					com_fija = (rs.getString("fg_porc_comision_fondeo")==null?"":rs.getString("fg_porc_comision_fondeo"));
				}
				rs.close();
				ps.close();
			}

			informacion.put("COMISION",comision);
			informacion.put("COM_DESC_ELECTRICO",comiDesElect);
			informacion.put("COM_DESC_MERCANTIL",comiDesMercantil);
			informacion.put("COM_VENTACARTERA",comiVentCar);
			informacion.put("COM_MODALIDAD2",comiModalidad2);
			informacion.put("COM_FIJA",com_fija);


      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getParametrizacionIF (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getParametrizacionIF (S)");
      return informacion;
    }


	 /**
	 *  Fodea 020-2014
	 *  para el calculo de la comisi�n especifica
	 *
	 * @throws java.sql.SQLException
	 * @throws javax.naming.NamingException
	 * @return
	 * @param tipo_moneda
	 * @param banco_fondeo
	 * @param cg_tipo_comision
	 * @param mes_calculo
	 * @param anio_calculo
	 * @param ic_if
	 * @param ic_producto_nafin
	 */

	private HashMap calcularSaldoPromedioMensualEspDis(String ic_producto_nafin, String ic_if,
			String anio_calculo, String mes_calculo, String cg_tipo_comision, String banco_fondeo, String tipo_moneda, String strClaveEPO )
			throws NamingException, SQLException {
		log.info ("calcularSaldoPromedioMensualEspDis(E)::" + ic_if  );

		AccesoDB con = new AccesoDB();
		double dblSaldoPromedioMensualM1 = 0,   dblSaldoPromedioMensualM1V = 0, dblSaldoPromedioMensualM2 = 0;
		StringBuffer strTempM1 = new StringBuffer();
		StringBuffer strTempM1V = new StringBuffer();
		StringBuffer strTempM2 = new StringBuffer();

		HashMap informacion = new HashMap();

		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.DATE, 1);
		cal.set(Calendar.YEAR, Integer.parseInt(anio_calculo));
		cal.set(Calendar.MONTH, Integer.parseInt(mes_calculo) - 1 );	//Enero es el mes 0 para Calendar, por lo cual le restamos 1 al mes

		int ultimoDiaDelMes = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

		double   dblTotalMontoVigenteM1 = 0, dblTotalMontoVigenteM1V = 0, dblTotalMontoVigenteM2 =0;

		/*-------------------- Modalidad 1 sin venta de cartera-------------------*/

		strTempM1.append( " SELECT 	"+
						"	sum(  (((CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'YYYY')) >  " + anio_calculo +  "   and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is null  THEN  "+ultimoDiaDelMes +
						"  ELSE  "+
						" CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'MM')) >  " + mes_calculo + "    and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is null THEN  "+ultimoDiaDelMes +
						"   ELSE  "+
						"    CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'MM')) =  " + mes_calculo + "  and  TO_NUMBER(TO_CHAR(sa.df_v_credito, 'YYYY')) =  " + anio_calculo +  "   and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is null THEN  TO_NUMBER(TO_CHAR(sa.df_v_credito, 'DD')) - 1   "+
						"      ELSE 0    "+
						"    end  "+
					  "  	END   "+
					  " 	END)    -   "+
					  " (CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY')) < " + anio_calculo +  "  and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is null  THEN     1   "+
					  " 	ELSE  "+
					  "     CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) <  " + mes_calculo + "   and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is null    THEN       1  "+
					  "     ELSE "+
					  "        CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) = " + mes_calculo + "  and  TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY')) =  " + anio_calculo +  "  and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is null THEN   "+
					  "            TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'DD'))     "+
					  "        ELSE  "+
					  "         0 "+
					  "         end  "+
					  "     END   "+
					  " END)   "+
					  " + 1) ) * d.fn_monto )   "+
					  " AS monto_vigente   "+

				" FROM dis_documento d, " +
				" 	dis_solicitud sa, " +
				" 	comcat_producto_nafin cpn, " +
				" 	comrel_if_epo_x_producto iexp " +
				" WHERE " +
				" 	sa.ic_documento = d.ic_documento " +
				" 	AND sa.ic_if = iexp.ic_if " +
				" 	AND d.ic_epo = iexp.ic_epo " +
				" 	AND cpn.ic_producto_nafin = iexp.ic_producto_nafin " +
				" 	AND iexp.ic_producto_nafin = 4 " +
				" 	AND sa.ic_estatus_solic = 10 " +
				"	AND d.ic_moneda = " + tipo_moneda + // Tipo de moneda Pesos o Dolares
				" 	AND sa.df_v_credito >= trunc(TO_DATE('01/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY')) " +
				" 	AND sa.df_fecha_solicitud <= trunc(TO_DATE('" + ultimoDiaDelMes + "/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY')+1) " +
				" 	AND sa.ic_if = " + ic_if +
				" and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is null  ");
				if(!strClaveEPO.equals(""))  {
					strTempM1.append(" and   d.ic_epo =  "+ strClaveEPO );
				}


		/*-------------------- Modalidad 1 con venta de cartera-------------------*/
			strTempM1V.append( " SELECT 	"+
						"	sum(  (((CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'YYYY')) >  " + anio_calculo +  "   and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is not null  THEN  "+ultimoDiaDelMes +
						"  ELSE  "+
						" CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'MM')) >  " + mes_calculo + "    and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is  not null THEN  "+ultimoDiaDelMes +
						"   ELSE  "+
						"    CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'MM')) =  " + mes_calculo + "  and  TO_NUMBER(TO_CHAR(sa.df_v_credito, 'YYYY')) =  " + anio_calculo +  "   and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is  not null THEN  TO_NUMBER(TO_CHAR(sa.df_v_credito, 'DD')) - 1   "+
						"      ELSE 0    "+
						"    end  "+
					  "  	END   "+
					  " 	END)    -   "+
					  " (CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY')) < " + anio_calculo +  "  and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is not  null  THEN     1   "+
					  " 	ELSE  "+
					  "     CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) <  " + mes_calculo + "   and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is  not null    THEN       1  "+
					  "     ELSE "+
					  "        CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) = " + mes_calculo + "  and  TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY')) =  " + anio_calculo +  "  and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is not null THEN   "+
					  "            TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'DD'))     "+
					  "        ELSE  "+
					  "         0 "+
					  "         end  "+
					  "     END   "+
					  " END)   "+
					  " + 1) ) * d.fn_monto )   "+
					  " AS monto_vigente   "+

				" FROM dis_documento d, " +
				" 	dis_solicitud sa, " +
				" 	comcat_producto_nafin cpn, " +
				" 	comrel_if_epo_x_producto iexp " +
				" WHERE " +
				" 	sa.ic_documento = d.ic_documento " +
				" 	AND sa.ic_if = iexp.ic_if " +
				" 	AND d.ic_epo = iexp.ic_epo " +
				" 	AND cpn.ic_producto_nafin = iexp.ic_producto_nafin " +
				" 	AND iexp.ic_producto_nafin = 4 " +
				" 	AND sa.ic_estatus_solic = 10 " +
				"	AND d.ic_moneda = " + tipo_moneda + // Tipo de moneda Pesos o Dolares
				" 	AND sa.df_v_credito >= trunc(TO_DATE('01/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY')) " +
				" 	AND sa.df_fecha_solicitud <= trunc(TO_DATE('" + ultimoDiaDelMes + "/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY')+1) " +
				" 	AND sa.ic_if = " + ic_if +
				" and  d.IC_LINEA_CREDITO_DM is not null  and CG_VENTACARTERA is not  null  ");
				if(!strClaveEPO.equals(""))  {
					strTempM1V.append(" and  d.ic_epo =  "+ strClaveEPO );
				}


			/*-------------------- Modalidad 2-----------------*/
			strTempM2.append( " SELECT 	"+
						"	sum(  (((CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'YYYY')) >  " + anio_calculo +  "   and  d.IC_LINEA_CREDITO  is not null   and CG_VENTACARTERA is null   THEN  "+ultimoDiaDelMes +
						"  ELSE  "+
						" CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'MM')) >  " + mes_calculo + "    and  d.IC_LINEA_CREDITO  is not null  and CG_VENTACARTERA is null   THEN  "+ultimoDiaDelMes +
						"   ELSE  "+
						"    CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_credito, 'MM')) =  " + mes_calculo + "  and  TO_NUMBER(TO_CHAR(sa.df_v_credito, 'YYYY')) =  " + anio_calculo +  "   and  d.IC_LINEA_CREDITO  is not null   and CG_VENTACARTERA is null   THEN  TO_NUMBER(TO_CHAR(sa.df_v_credito, 'DD')) - 1   "+
						"      ELSE 0    "+
						"    end  "+
					  "  	END   "+
					  " 	END)    -   "+
					  " (CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY')) < " + anio_calculo +  "  and  d.IC_LINEA_CREDITO  is not null  and CG_VENTACARTERA is null   THEN     1   "+
					  " 	ELSE  "+
					  "     CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) <  " + mes_calculo + "   and  d.IC_LINEA_CREDITO  is not null    and CG_VENTACARTERA is null      THEN       1  "+
					  "     ELSE "+
					  "        CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) = " + mes_calculo + "  and  TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY')) =  " + anio_calculo +  "  and  d.IC_LINEA_CREDITO is not null   and CG_VENTACARTERA is null  THEN   "+
					  "            TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'DD'))     "+
					  "        ELSE  "+
					  "         0 "+
					  "         end  "+
					  "     END   "+
					  " END)   "+
					  " + 1) ) * d.fn_monto )   "+
					  " AS monto_vigente   "+

				" FROM dis_documento d, " +
				" 	dis_solicitud sa, " +
				" 	comcat_producto_nafin cpn, " +
				" 	comrel_if_epo_x_producto iexp " +
				" WHERE " +
				" 	sa.ic_documento = d.ic_documento " +
				" 	AND sa.ic_if = iexp.ic_if " +
				" 	AND d.ic_epo = iexp.ic_epo " +
				" 	AND cpn.ic_producto_nafin = iexp.ic_producto_nafin " +
				" 	AND iexp.ic_producto_nafin = 4 " +
				" 	AND sa.ic_estatus_solic = 10 " +
				"	AND d.ic_moneda = " + tipo_moneda + // Tipo de moneda Pesos o Dolares
				" 	AND sa.df_v_credito >= trunc(TO_DATE('01/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY')) " +
				" 	AND sa.df_fecha_solicitud <= trunc(TO_DATE('" + ultimoDiaDelMes + "/" + mes_calculo + "/" + anio_calculo + "', 'DD/MM/YYYY')+1) " +
				" 	AND sa.ic_if = " + ic_if +
				" and  d.IC_LINEA_CREDITO  is not null and CG_VENTACARTERA is null   ");
				if(!strClaveEPO.equals(""))  {
					strTempM2.append(" and   d.ic_epo =  "+ strClaveEPO );
				}

		try {
			con.conexionDB();

			//Modalidad 1
			log.debug("strTempM1 ======="+strTempM1.toString());
			ResultSet rsM1 = con.queryDB(strTempM1.toString());
			while (rsM1.next()) {
				dblTotalMontoVigenteM1 += rsM1.getDouble("monto_vigente");
			}
			rsM1.close();

			//Modalidad 1 con  venta de Cartera
			log.debug("strTempM1V ======="+strTempM1V.toString());
			ResultSet rsM1V = con.queryDB(strTempM1V.toString());
			while (rsM1V.next()) {
				dblTotalMontoVigenteM1V += rsM1V.getDouble("monto_vigente");
			}
			rsM1V.close();


			//Modalidad 1
			log.debug("strTempM2 ======="+strTempM2.toString());
			ResultSet rsM2 = con.queryDB(strTempM2.toString());
			while (rsM2.next()) {
				dblTotalMontoVigenteM2 += rsM2.getDouble("monto_vigente");
			}
			rsM2.close();


			con.cierraStatement();

			//saldoPromedioMensual = monto vigente / numero de dias del mes de calculo
			dblSaldoPromedioMensualM1 = dblTotalMontoVigenteM1 / ultimoDiaDelMes;
			dblSaldoPromedioMensualM1V = dblTotalMontoVigenteM1V / ultimoDiaDelMes;
			dblSaldoPromedioMensualM2 = dblTotalMontoVigenteM2 / ultimoDiaDelMes;

			informacion.put("SaldoPromedioMensual_M1",Double.toString (dblSaldoPromedioMensualM1) );
			informacion.put("SaldoPromedioMensual_M1V",Double.toString (dblSaldoPromedioMensualM1V) );
			informacion.put("SaldoPromedioMensual_M2",Double.toString (dblSaldoPromedioMensualM2) );

			log.debug("informacion ===========   "+informacion);

		} finally {
			log.info ("calcularSaldoPromedioMensualEsp(S)" + ic_if  );
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return informacion;
	}


}// Fin del Bean
