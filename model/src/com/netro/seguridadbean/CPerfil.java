package com.netro.seguridadbean;

import java.sql.SQLException;
import java.util.*;
import java.io.*;

/****************************************************************************
*
* clase CPerfil
* @author Pedro A. Garcia Ramirez
*
*
*****************************************************************************/
public class CPerfil {

	Hashtable 	m_htRefPerfilesProt;
	String 		m_sCvePerfil;
	String 		m_sBloqueado;
	int 		m_iCveOID;
	int 		m_iTipoAfiliado;

	/*****************************************************************************
	*		Constructor
	*****************************************************************************/
	public CPerfil()
	{
                if (CConst.TRACE)
	        	System.out.println("CPerfil::Constructor");

		m_htRefPerfilesProt = new Hashtable();
		m_sCvePerfil = "";
		m_sBloqueado = "";
		m_iCveOID = 0;
	}
	/*****************************************************************************
	*		 void	agregarPerfilInsertDB()
	*****************************************************************************/
	public void agregarPerfilInsertDB()
					throws Exception,		NullPointerException, 	SQLException,
					 		 SeguException
	{
		CConexionDBPool	loConexion = new CConexionDBPool();
		int liCodigoError = 0;
		String lsSQL = "";
		boolean lbExito = true;

		lsSQL = " INSERT INTO seg_perfil " +
				" (cc_perfil, cs_bloqueado, ig_clave_oid, sc_tipo_usuario) " +
				" VALUES (" +
				"'" + m_sCvePerfil + "','" + m_sBloqueado + "'," +
				m_iCveOID + "," + m_iTipoAfiliado + ")";

                if (CConst.TRACE)
	        	System.out.println("\nCPerfil::agregarPerfilInsertDB()");
		try{
	  	loConexion.conectarDB();
		 loConexion.ejecutarInsertDB(lsSQL);
		} catch(SQLException ex){
			  lbExito = false;
			  liCodigoError = ex.getErrorCode();
	 		  if(liCodigoError == CConst.ERRC_REGIST_DUPLICADO)
			     throw new SeguException(CConst.SEG_ERRC_SIS_YA_EXIST);
			  if(liCodigoError == CConst.ERRC_INTEGR_REFERENC)
				  throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			  System.out.println("query :" + lsSQL );
			  System.out.println("El codigo de error es:" + liCodigoError + ex.getMessage());
	  } finally{
			  loConexion.cerrarDB(lbExito );
		}
	}
	/*****************************************************************************
	*		void agregarRefPerfilPerfPr()
	*****************************************************************************/
	public void agregarRefPerfilPerfPr(CRefPerfilProt eRefPerfilProt)
					throws NullPointerException
	{
		m_htRefPerfilesProt.put(new Integer(m_htRefPerfilesProt.size() + 1), eRefPerfilProt);
	}
	/*****************************************************************************
	*		 void	asociarPerfilPerfilProtDeleteDB()
	*****************************************************************************/
 	public void asociarPerfilPerfilProtDeleteDB( CConexionDBPool 	eoConexionDBPool)
					throws SQLException
	{
		int		liNumRegBorrados;
		int 		liCodigoError;
	  String 	lsSQL;
		liNumRegBorrados = 0;
		liCodigoError = 0;

		lsSQL = " DELETE FROM seg_rel_perfil_perfpr";
		lsSQL += " WHERE cc_perfil = '" + m_sCvePerfil + "'";

		System.out.println("\nCPerfil::asociarPerfilPerfilProtDeleteDB()");

		liNumRegBorrados = eoConexionDBPool.ejecutarUpdateDB(lsSQL);

                if (CConst.TRACE)
	        	System.out.println("El numero de registros borrados fue:" + liNumRegBorrados);

	}
	/*****************************************************************************
	*		 void	asociarPerfilPerfilProtInsertDB()
	*****************************************************************************/
	public void asociarPerfilPerfilProtInsertDB(String esCveSistema, String esCvePerfilProt,
														  CConexionDBPool eoConexionDBPool)
					throws SQLException
	{

	  String    lsSQL = "";

                if (CConst.TRACE)
	        	System.out.println("\nCPerfil::asociarFaCPerfilInsertDB()");

		lsSQL = "INSERT INTO seg_rel_perfil_perfpr (cc_perfil, c_sistema, c_perfil_prot) ";
		lsSQL += " VALUES(" ;
		lsSQL += "'" + m_sCvePerfil + "'," ;
		lsSQL += "'" + esCveSistema + "'," ;
		lsSQL += "'" + esCvePerfilProt + "'" ;
		lsSQL += ")" ;

  		eoConexionDBPool.ejecutarInsertDB(lsSQL);
	}
	/*****************************************************************************
   *      void borrarPerfilDeleteDB()
   *****************************************************************************/
	public void borrarPerfilDeleteDB()
					throws	Exception,		NullPointerException,	SQLException,
								SeguException
   {
		int		liNumRegBorrados;
		int	 	liCodigoError;
		boolean	lbExito = true;
		liNumRegBorrados = 0;
		liCodigoError =0;

	  CConexionDBPool loConexion = new CConexionDBPool();
	  String lsSQL;

	  lsSQL = "DELETE FROM seg_perfil WHERE cc_perfil ='" + m_sCvePerfil + "'";

        if (CConst.TRACE)
		System.out.println("\nCPerfil::borrarPerfilDeleteDB()");

	  try{
		loConexion.conectarDB();
                liNumRegBorrados = loConexion.ejecutarUpdateDB(lsSQL);
                if (CConst.TRACE)
	        	System.out.println("El numero de registros borrados fue:" + liNumRegBorrados);
	  	if(liNumRegBorrados == 0)
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ABORR_INEXIST);
		} catch(SQLException ex){
		        lbExito = false;
			liCodigoError = ex.getErrorCode();
			if(liCodigoError == CConst.ERRC_INTEGR_REFERENC)
				throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			System.out.println("El codigo de error es:" + liCodigoError );
		} finally{
			  loConexion.cerrarDB(lbExito);
		}
   }
	/*****************************************************************************
	*		void borrarRefPerfiles()
	*****************************************************************************/
	public void borrarRefPerfiles()
	{
                if (CConst.TRACE)
	        	System.out.println("CPerfil::borrarRefPerfiles(E)");

		if (m_htRefPerfilesProt.size() > 0){
	 		for (Enumeration e = m_htRefPerfilesProt.keys() ; e.hasMoreElements() ;) {
		 	m_htRefPerfilesProt.remove(e.nextElement());
	   	}
		}

                if (CConst.TRACE)
	        	System.out.println("CPerfil::borrarRefPerfiles(S)");
 	}
	/*****************************************************************************
	*		CPerfilProt buscarPerfilProt()
	*****************************************************************************/
	public CPerfilProt buscarPerfilProt(String esCveSistema,	String esCvePerfilProt)
	{
		CRefPerfilProt loRefPerfilProt = null;
		CPerfilProt loPerfilProt = null;
		boolean lbPerfEncontrado = false;

                if (CConst.TRACE){
        		System.out.println("\tCPerfil::buscarPerfilProt(E)");
	        	System.out.println("\t\tPerfilProt(" + esCvePerfilProt + ")");
                }

		if (m_htRefPerfilesProt.size() > 0)
		{
			Enumeration enumx = (Enumeration) m_htRefPerfilesProt.keys();
			while (enumx.hasMoreElements() && (lbPerfEncontrado == false))
			{
				loRefPerfilProt = (CRefPerfilProt) m_htRefPerfilesProt.get(enumx.nextElement()) ;
				if ((loRefPerfilProt.getCveSistema()).equals(esCveSistema) &&
						(loRefPerfilProt.getCvePerfProt()).equals(esCvePerfilProt))
				{
					lbPerfEncontrado = true;
					loPerfilProt = loRefPerfilProt.getPerfilProt();
                                        if (CConst.TRACE)
				        	System.out.println("\t\tPerfil Encontrado");
				}
			}
		}

                if (CConst.TRACE)
	        	System.out.println("\tCPerfil::buscarPerfilProt(S)");
		return loPerfilProt ;
	}
	
	
	/**
	 * Obtiene de la informaci�n de los perfiles de la base de datos.
 	 * @return	Vector con la informaci�n del perfil
	 * 		[0] Clave del perfil
	 *		[1] Estatus de Bloqueo
	 *		[2] Clave asignada al perfil dentro del OID
	 * 	Nunca regresa null
	 *
	 */
	public Vector consultarPerfilesSelectDB()
			throws Exception, SQLException,SeguException {
		
		Vector svTabla = new Vector();
		String	lsSQL;
		int 		liNumCols = 0;
		int 		liNumReg = 0;
		boolean	lbExito = true;

		if (CConst.TRACE)
			System.out.println("\nCPerfil:: consultarPerfilesSelectDB()");

		lsSQL = "SELECT cc_perfil, cs_bloqueado, ig_clave_oid, sc_tipo_usuario " + 
				" FROM seg_perfil pp " +
				" ORDER BY cc_perfil ";

		CConexionDBPool loConexion = new CConexionDBPool();
		try {
			loConexion.conectarDB();
			loConexion.ejecutarQueryDB(lsSQL, liNumCols, svTabla);
			if(svTabla.size()==0) {
				throw new SeguException(CConst.SEG_ERRC_SIS_TABLA_VACIA);
			}
		} finally{
			loConexion.cerrarDB(lbExito);
 		}

		return svTabla;
	}
	
	
	/**
	 * Consulta de Perfiles de la BD, filtrando por tipo de afiliado.
	 * Es decir muestra los perfiles asociados a cierto tipo de afiliado.
	 * 
	 *
	 * @param	esTipoAfiliado Clave del tipo de afiliado:
	 * 		1. EPO, 2. IF, 4. NAFIN
	 * 
	 * @return	Lista de perfiles.
	 * [0] cc_perfil (clave del perfil)
	 * [1] cs_bloqueado (estatus bloqueado o no)
	 * [2] ig_clave_oid (Clave dentro del oid)
	 * [3] sc_tipo_usuario (Tipo de afiliado)
	 * 
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Vector consultarPerfilesXAsignacSelectDB()
			throws Exception,	SQLException, SeguException {
		Vector svTabla;

	  String	lsSQL;
		int 		liNumCols = 0;
		int 		liNumReg = 0;
		boolean	lbExito = true;

	  svTabla = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\nCPerfil:: consultarPerfilesXAsignacSelectDB(E)");

		CConexionDBPool loConexion = new CConexionDBPool();

		lsSQL = "SELECT cc_perfil, cs_bloqueado, ig_clave_oid, sc_tipo_usuario";
		lsSQL += " FROM seg_perfil pp ";

		if (m_iTipoAfiliado > 0)
			lsSQL += " WHERE sc_tipo_usuario=" + m_iTipoAfiliado;


		lsSQL += " ORDER BY cc_perfil ";

		try{
			loConexion.conectarDB();
 			loConexion.ejecutarQueryDB(lsSQL, liNumCols,svTabla);
			if(svTabla.size()==0)
				throw new SeguException(CConst.SEG_ERRC_SIS_TABLA_VACIA);
		} catch(SQLException ex){
			lbExito = false;
			System.out.println("Error en el select"+ ex.getMessage());
		} finally{
			loConexion.cerrarDB(lbExito);
			System.out.println("\nCPerfil:: consultarPerfilesXAsignacSelectDB(S)");
 		}
		return svTabla;
	}
	/*****************************************************************************
   *      Hashtable consultarPerfPrXPerfilSelectDB()
   *****************************************************************************/
	public Hashtable consultarPerfPrXPerfilSelectDB()
					  	  throws 	Exception, 	NullPointerException,	SQLException,
						  SeguException
	{
		Hashtable htFacxPerfProt = new Hashtable();

	  String 	lsSQL = "";
	  Vector 	svTabla = null;
		Vector 	lvRegistro = null;
		Vector 	lvHashTable = null;
		String 	lsCveSistema = "";
		String 	lsCveSistemaAnt = "";
		String 	lsCvePerfilProt = "";
		Integer  loiNumRestriccion = null;
		int 		liNumCols = 0;
		int 		liContador = 0;
		int 		liCodigoError;
		boolean	lbExito = true;
		boolean	lbPrimera = true;

	  svTabla = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\nCPerfil::consultarPerfPrXPerfilSelectDB(E)");

		CConexionDBPool loConexion = new CConexionDBPool();

		lsSQL = "SELECT c_sistema, c_perfil_prot ";
		lsSQL += " FROM seg_rel_perfil_perfpr";
		lsSQL += " WHERE cc_perfil ='" + m_sCvePerfil + "'";
		lsSQL += " ORDER BY c_sistema";

		try{
			loConexion.conectarDB();
 			loConexion.ejecutarQueryDB(lsSQL, liNumCols,svTabla);
			if( (svTabla.size()) > 0)
			{

				lvHashTable = new Vector();

				for(liContador = 0; liContador < svTabla.size(); liContador ++ )
				{
					lvRegistro = (Vector) svTabla.elementAt(liContador);

					lsCveSistema = (String)lvRegistro.elementAt(0);
					lsCvePerfilProt = (String)lvRegistro.elementAt(1);

					if (!lsCveSistema.equals(lsCveSistemaAnt) && liContador > 0)
					{
						htFacxPerfProt.put(lsCveSistemaAnt,lvHashTable);
						lvHashTable = new Vector();
						lvHashTable.addElement(lsCvePerfilProt);

					}else
					{
						lvHashTable.addElement(lsCvePerfilProt);
					}

					lsCveSistemaAnt = lsCveSistema;
					lbPrimera = false;
				}
				if (lvHashTable.size() > 0)
					htFacxPerfProt.put(lsCveSistemaAnt,lvHashTable);

			}

		} catch(SQLException ex){
			lbExito = false;
			liCodigoError= ex.getErrorCode();
			System.out.println("Error en codigo.."+ liCodigoError +"  "+ ex.getMessage());
		} finally{
	 	   loConexion.cerrarDB(lbExito);
		}
                if (CConst.TRACE)
	        	System.out.println("\nCPerfil::consultarPerfPrXPerfilSelectDB(S)");
		return htFacxPerfProt ;
	}
	/*****************************************************************************
	*		Hashtable devolverRefPerfilesProt()
	*****************************************************************************/
	public Hashtable devolverRefPerfilesProt()
					throws NullPointerException
	{
		return m_htRefPerfilesProt;
	}

	/*****************************************************************************
	*		int getClaveOID()
	*****************************************************************************/
   public int getClaveOID()
	{
		return m_iCveOID;
	}


	/*****************************************************************************
	*		int getTipoAfiliado()
	*****************************************************************************/
   public int getTipoAfiliado()
	{
		return m_iTipoAfiliado;
	}

	/*****************************************************************************
	*		String getBloqueado
	*****************************************************************************/
   public String getBloqueado()
	{
		return m_sBloqueado;
	}
	/*****************************************************************************
	*		String getCvePerfil
	*****************************************************************************/
   public String getCvePerfil()
	{
		return m_sCvePerfil;
	}
	/*****************************************************************************
   *		 void modificarPerfilUpdateDB()
   *****************************************************************************/
	public void modificarPerfilUpdateDB()
			   throws Exception, SQLException, SeguException
   {
		CConexionDBPool	loConexion = new CConexionDBPool();
	  int 					liCodigoError;
	  int 					liNumRegActualizados;
	  boolean				lbExito = true;
	  liNumRegActualizados = 0;
	  String lsSQL;
	  lsSQL = "UPDATE seg_perfil SET cs_bloqueado = '" + m_sBloqueado + "'" +
	  		" , ig_clave_oid = " +	 m_iCveOID +
	  		" , sc_tipo_usuario = " + m_iTipoAfiliado +
			" WHERE cc_perfil = '" +	 m_sCvePerfil + "'";

        if (CConst.TRACE)
          System.out.println("\nCPerfil::bloquearPerfilUpdateDB()");

	  try{
	  	loConexion.conectarDB();
		 liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if(liNumRegActualizados == 0)
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
		} catch(SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
        		 System.out.println("Error en el update, el c�digo es : (" + liCodigoError +")");
			System.out.println("\n" + ex.getMessage());
	  } finally{
			loConexion.cerrarDB(lbExito);
		}
	}
	/*****************************************************************************
	*		void setClaveOID()
	*****************************************************************************/
   public void setClaveOID(int eiCveOID)
	{
		m_iCveOID = eiCveOID;
	}

	/*****************************************************************************
	*		void setTipoAfiliado()
	*****************************************************************************/
   public void setTipoAfiliado(int eiTipoAfiliado)
	{
		m_iTipoAfiliado = eiTipoAfiliado;
	}


	/*****************************************************************************
	*		void	setBloqueado
	*****************************************************************************/
   public void	setBloqueado(String esBloqueado)
	{
		m_sBloqueado = esBloqueado;
	}
	/*****************************************************************************
	*		void setCvePerfil
	*****************************************************************************/
	public void setCvePerfil(String esCvePerfil)
	{
		m_sCvePerfil = esCvePerfil;
	}
}