package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CLEmpresa.java
*
*	Proposito:	Clase CLEmpresa
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.util.*;
import java.io.*;
import java.lang.Long;


/****************************************************************************
*
*	clase CLEmpresa
*
*****************************************************************************/
public class CLEmpresa {

	public Hashtable	m_htEmpresas ;
	public String 		m_sSQLCargaEmpresa ;

	/*****************************************************************************
	*		Constructor
	*****************************************************************************/

	public CLEmpresa(){
                if (CConst.TRACE)
	        	System.out.println("\t CLEmpresa:Constructor");

		m_htEmpresas = new Hashtable();

		m_sSQLCargaEmpresa = "SELECT c_empresa, d_nombre, b_bloqueado ";
		m_sSQLCargaEmpresa = m_sSQLCargaEmpresa + "FROM seg_empresas " ;
		m_sSQLCargaEmpresa = m_sSQLCargaEmpresa + "ORDER BY  c_empresa " ;
	}
	/*****************************************************************************
	*		CEmpresa buscarEmpresa()
	*****************************************************************************/
	public CEmpresa buscarEmpresa(String esCveEmpresa)
	{
		return ( (CEmpresa) m_htEmpresas.get(esCveEmpresa) );
	}
	/*****************************************************************************
	*		void cargarEmpresas
	*****************************************************************************/
	public void cargarEmpresas(Vector evTmp)
	{
		Vector lvRegistro ;
		int liNumRegs = 0;
		CEmpresa loEmpresa ;
		String lsCveEmpresa = "";
		String lsDescripcion = "";
		String lsBloqueado = "";

   	try {

			liNumRegs = evTmp.size() ;
			lvRegistro = new Vector();

                if (CConst.TRACE)
	          	System.out.println("Tama�o Vector(" + liNumRegs + ")");
			for (int liPos = 0; liPos < liNumRegs; liPos++ ) {
				lvRegistro = (Vector) evTmp.elementAt(liPos);

				lsCveEmpresa = (String) lvRegistro.elementAt(0) ;
				lsDescripcion = (String) lvRegistro.elementAt(1) ;
				lsBloqueado = (String) lvRegistro.elementAt(2) ;

        if (CConst.TRACE)
	      	System.out.println("CveEmpresa{" + lsCveEmpresa + "]");

				loEmpresa = new CEmpresa(	lsCveEmpresa,	lsDescripcion,
													lsBloqueado);

				m_htEmpresas.put(lsCveEmpresa, loEmpresa);
			}

			/* Codigo temporal */
			Enumeration enum1 = m_htEmpresas.keys();
			while (enum1.hasMoreElements()){
                                if (CConst.TRACE)
			        	System.out.println("\t Llaves en Hash[" + enum1.nextElement() + "]");
			}

		} catch (Exception e) {
	  	System.out.println("Error:" + e);
		}
	}
	/*****************************************************************************
	*		String getSQLCargaEmpresas()
	*****************************************************************************/
	public String getSQLCargaEmpresas()
	{
		return m_sSQLCargaEmpresa;
	}
}