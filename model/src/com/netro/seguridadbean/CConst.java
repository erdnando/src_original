package com.netro.seguridadbean;

import java.sql.*;
import java.util.*;

/****************************************************************************
*
*	clase CConst
*
*****************************************************************************/
public class CConst {

	public static boolean TRACE = false ;

	public final static int ERRC_OK = 0 ;
	public final static int ERRC_NO = -1 ;
	public final static String USR_INACTIVO = "I" ;
	public final static String USR_ACTIVO = "A" ;
	public final static String SI = "S" ;
	public final static String NO = "N" ;
	public final static String MAS = "+" ;
	public final static String MENOS = "-" ;

	public final static int TP_USR_IF = 2;
	public final static int TP_USR_EPO = 1;
	public final static int TP_USR_PYME = 3;
	public final static int TP_USR_NAFIN = 4 ;

	public final static int iNUM_INTENTOS_PASSWD = 7 ;


	//public final static int ERRC_REGIST_DUPLICADO = 2627 ;//SQLServer
	//public final static int ERRC_INTEGR_REFERENC= 547;//SQLServer

	public final static int ERRC_REGIST_DUPLICADO = 1 ;//Oracle
	public final static int ERRC_INTEGR_REFERENC= 2292;//Oracle

	public final static int TIPO_FECHA = 93 ;
	public final static int TIPO_CHAR = 1 ;
	public final static int TIPO_VARCHAR = 12 ;
	public final static int TIPO_LONG = 3 ;
	public final static int TIPO_SMALLINT = 5;
	public final static int TIPO_NUMBER = 2;
//	public final static int TIPO_NUMBER = 6;
	public final static int TIPO_RARO = 8;
	public final static int TIPO_RARO_2 = -5;

	public final static String FUNC_FECHA_DB_1 = "to_date('";
	public final static String FUNC_FECHA_DB_2 = "','yyyy-MM-dd')";
	public final static String FUNC_FECHA_DB_3 = "','yyyy-MM-dd HH24:MI:SS')";
    // Agregado 09/10/2002  --CARP
	public final static String FUNC_FECHA_DB_4 = "','HH24:MI:SS')";

	public final static char TP_USR_CLIENTE = 'C';
	public final static char TP_USR_BANCO = 'B';

	public final static int N_PWD_HIST = 3 ;//Numero de Passwords historicos a validar
    // Agregado 09/10/2002		--CARP
	public final static int iNUM_MESES_VIGEN_PWD = 3 ;//Meses vigentes para el password

	public final static char ALTA = 'A';
	public final static char BAJA = 'B';
	public final static char CAMBIO = 'C';

	public final static int COT_ERRC_DB = 0; //Error en la Base de Datos
	public final static int SEG_ERRC_USR_INEXIST = 1;
	public final static int SEG_ERRC_USR_INACTIVO = 2 ;
	public final static int SEG_ERRC_SIS_INEXIST = 3 ;
	public final static int SEG_ERRC_SIS_BLOQ = 4 ;
	public final static int SEG_ERRC_PP_INEXIST = 5 ;
	public final static int SEG_ERRC_PP_BLOQ = 6 ;
	public final static int SEG_ERRC_FAC_BLOQ = 7 ;
	public final static int SEG_ERRC_FAC_MENOS = 8 ;
	public final static int SEG_ERRC_FAC_PERF_INEXIST = 9 ;
	public final static int SEG_ERRC_FEC_VENC = 10 ;
	public final static int SEG_ERRC_FUERA_HR = 11 ;
	public final static int SEG_ERRC_EMPR_BLOQ = 12 ;
	public final static int SEG_ERRC_USR_CAD = 13 ;
	public final static int SEG_ERRC_CONTRA_INV = 14 ;
	public final static int SEG_ERRC_FAC_INEXIST = 15 ;
	public final static int SEG_ERRC_MANCOMUNIDAD = 16 ;

	public final static int SEG_ERRC_SISXFAC_INEXIST = 17 ;//SISTEMA POR FACULTAD INEXISTENTE
	public final static int SEG_ERRC_SIS_YA_EXIST = 18 ;	//EL SISTEMA YA EXISTE
	public final static int SEG_ERRC_SIS_TABLA_VACIA = 19 ;//RESULTADO DE LA CONSULTA ES CERO
	public final static int SEG_ERRC_SIS_REG_ABORR_INEXIST = 20 ;//REGISTRO A BORRAR NO EXISTE
	public final static int SEG_ERRC_SIS_INTEG_REFERENCIAL = 21 ;//INTEGRIDAD REFERENCIAL
	public final static int SEG_ERRC_SIS_REG_ACTUAL_NOEXIS= 22 ;//REGIST A ACTUALIZAR NO EXISTE
	public final static int SEG_ERRC_SIS_REG_INSERT_REFERENC= 23 ;//REGIST A INSERTAR CON

	public final static int SEG_ERRC_CONTRA_UT_ANT = 24 ;//CONTRASENA UTILIZADA EN ULTIMOS 3 INTENTOS

	public final static int SEG_ERRC_SESION_ACT = 25 ; //SESON ACTIVA

	public final static int SEG_ERRC_PERFIL_INEXIST = 26 ;
	public final static int SEG_ERRC_PERFIL_BLOQ = 27 ;
	public final static int SEG_ERRC_PERFIL_NO_ASIG = 28 ;

    // Agregado 09/10/2002		--CARP
    public final static int SEG_ERRC_CONTRA_VENC = 29;	//Contraseņa vencida
    public final static int SEG_ERRC_CAMBIO_CONTRA = 30;	//Primera vez que entra al sistema

	public final static String CADENA_CONEXION_DB = "jdbc:weblogic:pool:seguridadPool";

}