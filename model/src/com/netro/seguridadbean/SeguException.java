package com.netro.seguridadbean;

import java.util.*;
import java.io.*;
import java.lang.*;


/**
 * Clase de Excepciones para el m�dulo de Seguridad.
 *
 * @author Pedro A Garcia Ramirez
 * @author Gilberto Aparicio Modificado para meter metodos estandares getMessage()
 */
public class SeguException extends Exception {
	int	m_iIndice ;
	String 	m_sMsgError ;


	/**
	 * Crea una excepci�n a partir del c�digo de error recibido.
	 * Los codigos de Errores van del 0 al 30.	
	 * @param eiIndiceError C�digo de error.
	 */
	public SeguException(int eiIndiceError){
		String 	m_aMsgError[] = new String[50];

		m_aMsgError[0] = new String("OCURRIO UN ERROR EN LA BASE DE DATOS");
		m_aMsgError[1] = new String("USUARIO NO EXISTE");
		m_aMsgError[2] = new String("USUARIO INACTIVO");
		m_aMsgError[3] = new String("SISTEMA NO EXISTE");
		m_aMsgError[4] = new String("SISTEMA BLOQUEADO");
		m_aMsgError[5] = new String("PERFIL PROTOTIPO NO ASIGNADO");
		m_aMsgError[6] = new String("PERFIL PROTOTIPO BLOQUEADO");
		m_aMsgError[7] = new String("FACULTAD BLOQUEADA");
		m_aMsgError[8] = new String("FACULTAD MENOS");
		m_aMsgError[9] = new String("FACULTAD NO ASIGNADA A PERFIL PROTOTIPO");
		m_aMsgError[10] = new String("FECHA VENCIDA");
		m_aMsgError[11] = new String("FUERA DE HORARIO");
		m_aMsgError[12] = new String("EMPRESA BLOQUEADA");
		m_aMsgError[13] = new String("USUARIO CADUCO");
		m_aMsgError[14] = new String("CONTRASE�A INVALIDA");
		m_aMsgError[15] = new String("FACULTAD NO EXISTE");
		m_aMsgError[16] = new String("FACULTAD MANCOMUNADA");

		m_aMsgError[17] = new String("NO HAY REGISTROS A MOSTRAR CON LA CLAVE ESPECIFICADA");	//17
		m_aMsgError[18] = new String("REGISTRO YA EXISTE");	//18
		m_aMsgError[19] = new String("NO HAY REGISTROS A MOSTRAR EN LA B.D.");	//19
		m_aMsgError[20] = new String("EL REGISTRO QUE UD. QUISO BORRAR NO EXISTE EN LA B.D.");	//20
		m_aMsgError[21] = new String("REGISTRO REFERENCIADO A " +
									  "OTRA TABLA EN LA B.D.");	//21
		m_aMsgError[22] = new String("REGISTRO A MODIFICAR NO EXISTE");	//22
		m_aMsgError[23] = new String("EL REGISTRO QUE UD. QUISO INSERTAR ESTA REFERENCIADO A " +
									  "OTRA TABLA EN LA B.D.");	//23
		m_aMsgError[24] = new String("CONTRASE�A UTILIZADA EN ULTIMOS 3 INTENTOS");	//24

		m_aMsgError[25] = new String("NO PUEDE TENER DOS SESIONES ACTIVAS");	//25
		m_aMsgError[26] = new String("PERFIL NO EXISTE");	//26
		m_aMsgError[27] = new String("PERFIL BLOQUEADO");	//27
		m_aMsgError[28] = new String("PERFIL NO ASIGNADO");	//28
		m_aMsgError[29] = new String("CONTRASE�A VENCIDA. DEBES CAMBIARLA");	//29
		m_aMsgError[30] = new String("DEBES CAMBIAR TU CONTRASE�A");	//30

		m_sMsgError = (String) m_aMsgError[eiIndiceError] ;

		m_iIndice = eiIndiceError;
	}



	/**
	 * Crea una excepci�n a partir del mensaje de Error
	 * 
	 * @param esMsgError Mensaje de Error.
	 */
	public SeguException(String esMsgError)
	{
		m_iIndice = -1;
		m_sMsgError = esMsgError;
	}
	
	
	/**
	 * Obtiene el c�digo de error generado.
	 * El m�todo regresa siempre -1 cuando la excepcion generada es a 
	 * trav�s de un mensaje de Error.
	 * 
	 * @return int Codigo de error
	 */
	public int getCodError()
	{
		return m_iIndice;
	}
	
	
	/**
	 * Obtiene el mensaje de la excepcion.
	 * @return Mensaje de error.
	 */
	public String getMessage() {
		return m_sMsgError;
	}


	/**
	 * Obtiene el mensaje de la excepcion. Tiene la misma funcionalidad que
	 * getMessage()
	 * @return Mensaje de error.
	 */
	public String getMsgError()
	{
		return m_sMsgError;
	}
}