package com.netro.seguridadbean;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Types;

import java.util.Vector;


/**
 * La clase contiene los metodos necesarios para el manejo de datos
 * en la Base de datos.
 *
 */
public class CConexionDBPool {

	Connection  m_cConexion ;

	/**
	 *	Constructor predeterminado
	 */
	public CConexionDBPool(){

		if (CConst.TRACE)
			System.out.println("CConexionDBPool::CConexionDBPool");
		m_cConexion = null;

	}
	
	/**
	 * Cierra la conexion a la Base de datos.
	 * @param enResultado. true para hacer commit o false
	 * 		para hacer un rollback antes de cerrar la conexion.
	 */
	public void cerrarDB(	boolean		ebResultado) {
		if (CConst.TRACE)
			System.out.println("CConexionDBPool::cerrarDB(E)");

		if (m_cConexion != null) {
			try {
				if (ebResultado)
					m_cConexion.commit();
				else
					m_cConexion.rollback();
			}catch(SQLException sqlEx) {
				System.out.println("cerrarDB(" + sqlEx + ")");
			}
			
			try {
				m_cConexion.close();
			}catch(SQLException sqlEx2) {
				System.out.println("cerrarDB(" + sqlEx2 + ")");
			}

		}
     	m_cConexion = null;
		if (CConst.TRACE)
			System.out.println("CConexionDBPool::cerrarDB(S)");
	}

	/**
	 * Realiza la conexion a la Base de datos.
	 */
	public void conectarDB()
			throws SQLException, Exception {
		if (CConst.TRACE)
			System.out.println("\tCConexionDBPool::ConectarDB");

		javax.naming.Context naming = new javax.naming.InitialContext();
		javax.sql.DataSource ds = (javax.sql.DataSource) naming.lookup("seguridadData");
		m_cConexion = ds.getConnection();

		m_cConexion.setAutoCommit(false);
		m_cConexion.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

	}

	/**
	 * Ejecuta una sentecia de insercion de datos en la BD.
	 * @param sql Query de actualizaci�n de datos.
	 */
	public void ejecutarInsertDB(	String sql)
					throws SQLException
	{
		if (CConst.TRACE){
			System.out.println("\tCConexionDBPool::ejecutarInsertDB(E)");
			System.out.println("\tCConexionDBPool::ejecutarInsertDB(" + sql + ")");
		}
		Statement statement = m_cConexion.createStatement();
		try {
			statement.execute(sql);
		}catch(SQLException sqle){
			sqle.printStackTrace();
			throw sqle;
		}finally{
			if (CConst.TRACE)
				System.out.println("\tCConexionDBPool::ejecutarInsertDB(S)");
			if (statement != null){
				statement.close();
			}
		}
	

	}
	
	/**
	 * Ejecuta una consulta y ingresa los resultados a un vector de datos.
	 * @param sql Query de la consulta a la BD.
	 * @param inumcols (Obsoleta. no tiene ningun uso)
	 * @param svTabla Vector donde se almacenan los resultados de la consulta
	 */
	// Modificado por -GEAG- para soportar tipo de datos CLOB
	// La implementaci�n solo soporta CLOB de tama�o hasta el tama�o de un "int"
	public void ejecutarQueryDB(	String sql,	int inumcols,	Vector svTabla)
			throws Exception, SQLException {
		int 					liNumCols = 0;
		int					liTipoDato = 0;
		String 				lsTmp = "";
		Time	 				ltmTmp = null;
		Date	 				ldtTmp = null;
		Vector				lvRegistro = null;
		ResultSetMetaData rsMetaData = null;
		ResultSet   		lrsResultSet = null;

	   String lsCampo = new String();
		Integer loiCampo ;
		int liCampo = 0;

		if (CConst.TRACE){
			System.out.println("\tCConexionDBPool::EjecutarQueryBD");
			System.out.println("\t\tSQL(" + sql + ")");
		}

		Statement statement = m_cConexion.createStatement();

		lrsResultSet = statement.executeQuery(sql);

		rsMetaData = lrsResultSet.getMetaData();

		liNumCols = rsMetaData.getColumnCount() ;

		if (CConst.TRACE)
			System.out.println("\t Numero de Columnas(" + rsMetaData.getColumnCount() + ")");

		while (lrsResultSet.next()) {
			lvRegistro = new Vector();
			for (int i = 1; i <= liNumCols ; i++) {
				liTipoDato = rsMetaData.getColumnType(i);

				if (CConst.TRACE)
					System.out.println("\t Tipo de Dato (" + liTipoDato + ")");

			if (liTipoDato == java.sql.Types.VARCHAR || liTipoDato == java.sql.Types.CHAR){
					lsCampo = lrsResultSet.getString(i);
					if (lsCampo == null)
						lsCampo = "";
					lvRegistro.addElement(lsCampo.trim());
			} else if (liTipoDato == java.sql.Types.CLOB) {
					Clob loClob = lrsResultSet.getClob(i);
					//loClob.length debe ser entero para que la siguiente instrucci�n sea v�lida
					lsCampo = loClob.getSubString(1, (int)loClob.length());	//El indice empieza en 1.
					if (lsCampo == null) {
						lsCampo = "";
					}
					lvRegistro.addElement(lsCampo.trim());
			} else if (liTipoDato == java.sql.Types.DATE || liTipoDato == java.sql.Types.TIMESTAMP){
					ldtTmp = lrsResultSet.getDate(i);
					ltmTmp = lrsResultSet.getTime(i);
					if (ldtTmp == null)
						lsCampo = "";
					else
						lsCampo = ldtTmp.toString() + " " + ltmTmp.toString();

					lvRegistro.addElement(lsCampo.trim());
				}else if(liTipoDato == Types.INTEGER || liTipoDato == Types.SMALLINT || liTipoDato == Types.DECIMAL || liTipoDato == Types.NUMERIC){
 					liCampo = lrsResultSet.getInt(i);
					loiCampo = new Integer(liCampo);
					lvRegistro.addElement(loiCampo);
				} else {
					System.out.print(" " + i + "(" + lsCampo + ")");
				}
			}
			svTabla.addElement(lvRegistro);
                        if (CConst.TRACE)
		        	System.out.println("\n ");
		}

		statement.close();
		lrsResultSet.close();

	}
	
	
	/**
	 * Ejecuta una sentecia de actualizacion de datos en la BD.
	 * @param sql Query de actualizaci�n de datos.
	 */
	public int ejecutarUpdateDB(	String sql)
					throws SQLException {
		int liNumRegActualizados;
		liNumRegActualizados = 0;
		PreparedStatement prepare;

		prepare = m_cConexion.prepareStatement(sql);
		if (CConst.TRACE)
			System.out.println("\tCConexionDBPool::ejecutarUpdateDB(" + sql + ")");
		liNumRegActualizados = prepare.executeUpdate();
		prepare.close();

		if (CConst.TRACE)
			System.out.println("\tRegistros actualizados:" + liNumRegActualizados + ")");
	  return liNumRegActualizados;//REGRESA EL NUMERO DE REGISTROS ACTUALIZADOS
	}

	public PreparedStatement queryPrecompilado(String query) throws SQLException {
		try {
			return m_cConexion.prepareStatement(query);
		} catch (SQLException e) {
			System.out.println("Error en el query:\n\n"+query);
			throw e;
		}
	}	

	/**
	 * Permite ejecutar un procedimiento almacenado de la BD.
	 * @param s Instrucci�n para ejecutar el Procedimiento almacenado
	 * @return objeto CallableStament Con el cual se puede establecer
	 * 		los parametros del procedimiento almacenado
	 */
	
	public CallableStatement ejecutarSP(String s) throws SQLException {
		CallableStatement loCallableStatement = null;
		try {
			loCallableStatement = m_cConexion.prepareCall("{call " + s + "}");
		} catch(SQLException sqlexception) {
			
			System.out.println("CConexionDBPool::ejecutaSP(): " + sqlexception.getMessage());
			System.out.println("\tSP: " + s);
			throw sqlexception;
		}
		return loCallableStatement;
	}
	
}