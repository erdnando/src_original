package com.netro.seguridadbean;

import java.lang.ref.*;
import java.sql.SQLException;
import java.util.*;
import java.io.*;

/**
 * Representaci�n de los sistemas de la aplicacion
 * @author Pedro A. Garcia Ramirez
 * @author Cindy A. Ramos P�rez (Modificacion 26/03/2002)
 * @author Gilberto Aparicio (Modificacion 23/08/2005)
 * 
 */

public class CSistema {

	Hashtable m_htFacultades = new Hashtable();
	Hashtable m_htPerfilesProt= new Hashtable();
	String m_sCveSistema = "";
	String m_sDescripcion = "";
	String m_sDescripcionIngles = "";
	String m_sBloqueado = "";
	String m_sImagen = "";
	String m_sSwImg = "";
	String m_sLiga = "";
	String m_sQuitaMnu = "";
	int m_iOrdenMenu = 0;
	int m_iAccLiga = 0;

	/**
	 * Constructor predeterminado
	 *
	 */
	public CSistema() {
		if (CConst.TRACE) {
			System.out.println("\tCSistema::Constructor_1");
		}
	}
	
	
	/**
	 * Constructor
	 * @param esCveSistema Clave del sistema
	 * @param esDescripcion Descripcion del sistema
	 * @param esDescripcionIngles Descripci�n del seistema en idioma ingles
	 * @param esBloqueado Identifica si el sistema esta bloqueado o no
	 * @param esImagen Ruta de la imagen a desplegar
	 * @param eiOrdenMenu Orden de precedencia para despliegue
	 * @param esSwImg  Identifica si se muestra la imagen (I) o no (D)
	 * @param esLiga URL al que redirige el sistema
	 * @param eiAccLiga	Acci�n de la liga.
	 * 		0: Abre la pagina en la ventana actual
	 * 		1: Abre la pagina en una ventana nueva 
	 * @param esQuitaMnu ???
	 *
	 */
	public CSistema(String esCveSistema, String esDescripcion,
			String esDescripcionIngles, String esBloqueado,
			String esImagen, int eiOrdenMenu, String esSwImg,
			String esLiga, int eiAccLiga, String esQuitaMnu) {
		
		if (CConst.TRACE) {
			System.out.println("CSistema::Constructor_2");
		}

		m_sCveSistema = esCveSistema;
		m_sDescripcion = esDescripcion;
		m_sDescripcionIngles = esDescripcionIngles;
		m_sBloqueado = esBloqueado;
		m_sImagen = esImagen;
		m_iOrdenMenu = eiOrdenMenu;
		m_sSwImg = esSwImg;
		m_sLiga = esLiga;
		m_iAccLiga = eiAccLiga;
		m_sQuitaMnu = esQuitaMnu;
	}
	
	
	/**
	 * Constructor
	 * @param esCveSistema Clave del sistema
	 * @param esDescripcion Descripcion del sistema
	 * @param esDescripcionIngles Descripci�n del seistema en idioma ingles
	 * @param esBloqueado Identifica si el sistema esta bloqueado o no
	 * @param esImagen Ruta de la imagen a desplegar
	 * @param esSwImg  Identifica si se muestra la imagen (I) o no (D)
	 * @param esLiga URL al que redirige
	 * @param eiAccLiga	Acci�n de la liga.
	 * 		0: Abre la pagina en la ventana actual
	 * 		1: Abre la pagina en una ventana nueva 
	 * @param esQuitaMnu ???
	 */
	public CSistema(String esCveSistema, String esDescripcion,
			String esDescripcionIngles, 
			String esBloqueado, String esImagen,
			String esSwImg, String esLiga,
			int eiAccLiga, String esQuitaMnu) {

		this(esCveSistema, esDescripcion,
				esDescripcionIngles, esBloqueado,
				esImagen, 0, esSwImg,	//int liOrdenMenu = 0;
				esLiga, eiAccLiga, esQuitaMnu);
		
		if (CConst.TRACE) {
			System.out.println("CSistema::Constructor_3");
		}
		
	}
	

	/*****************************************************************************
	*		void agregarFacultad
	*****************************************************************************/
	public void agregarFacultad(CFacultad eFacultad) {
		if (CConst.TRACE) {
			System.out.println("\t CSistema::agregarFacultad ");
		}
		m_htFacultades.put(eFacultad.getCveFacultad(), eFacultad);
	}
	/*****************************************************************************
	*		void agregarPerfilProt
	*****************************************************************************/
	public void agregarPerfilProt(CPerfilProt ePerfilProt) {
		if (CConst.TRACE) {
			System.out.println("\t CSistema::agregarPerfilProt ");
		}
		m_htPerfilesProt.put(ePerfilProt.getCvePerfilProt(), ePerfilProt);
	}
	
	
	/**
	 * Permite registrar en la BD el sistema especificado.
	 * @param esCveSistPadre Clave del sistema Padre
	 * @param eoLRelSistemas Relacion entre sistemas
	 * @param esStatus ???
	 *
	 * @return numero que controla la aparicion en el menu del sistema
	 */
	public int agregarSistemaInsertDB(String esCveSistPadre, 
			CLRelSistema eoLRelSistemas, String esStatus)
			throws Exception, SQLException, SeguException {
		CConexionDBPool	loConexion = new CConexionDBPool();
		int liCodigoError = 0;
		int	liNumColsRefPP = 1;
		int	liConsecSistema = 0;
		String lsSQL = "";
		String lsSQLSelect = "";
		boolean lbExito = false;
		Vector lvMaxOrdenMnu = new Vector();
		Vector lvRegistro;

		lsSQLSelect = "SELECT max(sg_orden_mnu) as mx_orden_mnu " +
				" FROM seg_sistemas ";

		if (CConst.TRACE){
			System.out.println("\nCSistema::agregarSistemaInsertDB(E)");
			System.out.println("\t Sistema Padre::" + esCveSistPadre);
		}
		try{
			loConexion.conectarDB();
		}catch(Exception e){
			System.out.println("CSistema::cancelarOperacion(" + e + ")");
			throw new SeguException(CConst.COT_ERRC_DB);
		}
		try {
			loConexion.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvMaxOrdenMnu);

			if (lvMaxOrdenMnu != null) {
				lvRegistro = (Vector) lvMaxOrdenMnu.elementAt(0);
				liConsecSistema = ((Integer) lvRegistro.elementAt(0)).intValue();
				liConsecSistema++;
			}

			lsSQL =  " INSERT INTO seg_sistemas " +
					" (c_sistema,d_descripcion, d_descripcion_ing, " +
					" b_bloqueado, t_imagen, sg_orden_mnu, cg_sw_img_desc, " +
					" cg_liga, sg_sw_accion_click, b_menu) " +
					" VALUES ("+ "'" + m_sCveSistema + "','" +
					m_sDescripcion + "','" + m_sDescripcionIngles + "','" +
					m_sBloqueado + "','" + m_sImagen + "'," + 
					liConsecSistema + ",'" + m_sSwImg + "','" +
					m_sLiga +" '," + m_iAccLiga + ",'" +  m_sQuitaMnu +"')";

			loConexion.ejecutarInsertDB(lsSQL);

			if (esCveSistPadre.length() > 0) {

				int liNivel = eoLRelSistemas.devolverNivelPadre(esCveSistPadre);
				if (CConst.TRACE) {
					System.out.print("\tNivel del Sistema Padre::" + liNivel);
				}
				liNivel++;
				if (CConst.TRACE) {
					System.out.println("\tNivel del Sistema::" + liNivel);
				}
				lsSQL =  " INSERT INTO seg_rel_sist (cc_sistema, " +
						" cc_sistema_2,n_nivel, cg_status) " +
						" VALUES ('" + m_sCveSistema + "','" +
						esCveSistPadre + "', " + liNivel + ", '"+
						esStatus +"')";
				loConexion.ejecutarInsertDB(lsSQL);
			}
			lbExito = true;
		} catch(SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			if (liCodigoError == CConst.ERRC_REGIST_DUPLICADO) {
				throw new SeguException(CConst.SEG_ERRC_SIS_YA_EXIST);
			}
		} finally{
			loConexion.cerrarDB(lbExito);
			if (CConst.TRACE) {
				System.out.println("CSistema::agregarSistemaInsertDB(S)");
			}
		}
		return liConsecSistema;
	}
	
	
	
	
	/**
	 * Permite registrar en la BD el sistema especificado.
	 * @param esCveSistPadre Clave del sistema Padre
	 *
	 * @return numero que controla la aparicion en el menu del sistema
	 */
	public int agregarSistemaInsertDB(String esCveSistPadre)
			throws Exception, SQLException, SeguException {
		CConexionDBPool	loConexion = new CConexionDBPool();
		int liCodigoError = 0;
		int	liNumColsRefPP = 1;
		int	liConsecSistema = 0;
		String lsSQL = "";
		String lsSQLSelect = "";
		boolean lbExito = false;
		Vector lvMaxOrdenMnu = new Vector();
		Vector lvRegistro;

		lsSQLSelect = "SELECT max(sg_orden_mnu) as mx_orden_mnu " +
				" FROM seg_sistemas ";

		if (CConst.TRACE) {
			System.out.println("\nCSistema::agregarSistemaInsertDB()");
		}

		try {
			loConexion.conectarDB();
		} catch(Exception e) {
			System.out.println("CSistema::cancelarOperacion(" + e + ")");
			throw new SeguException(CConst.COT_ERRC_DB);
		}
		try{
			loConexion.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvMaxOrdenMnu);

			if (lvMaxOrdenMnu != null) {
				lvRegistro = (Vector) lvMaxOrdenMnu.elementAt(0);
				liConsecSistema = ((Integer) lvRegistro.elementAt(0)).intValue();
				liConsecSistema++;
			}
			lsSQL =  " INSERT INTO seg_sistemas " +
					" (c_sistema,d_descripcion, d_descripcion_ing, " +
					" b_bloqueado, t_imagen, sg_orden_mnu, cg_sw_img_desc, " +
					" cg_liga, sg_sw_accion_click) " +
					" VALUES ("+ "'" + m_sCveSistema + "','" +
					m_sDescripcion + "','" + m_sDescripcionIngles + "','" +
					m_sBloqueado + "','" +
					m_sImagen + "'," + liConsecSistema + ",'" +
					m_sSwImg + "','" + m_sLiga + "'," + m_iAccLiga + ")";

			loConexion.ejecutarInsertDB(lsSQL);

			if (esCveSistPadre.length() > 0) {
				lsSQL = " INSERT INTO seg_rel_sist (cc_sistema,cc_sistema_2) " +
						" VALUES ('" + m_sCveSistema + "','" +
						esCveSistPadre + "')";
				loConexion.ejecutarInsertDB(lsSQL);
			}
			lbExito = true;
		} catch(SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			if(liCodigoError == CConst.ERRC_REGIST_DUPLICADO) {
				throw new SeguException(CConst.SEG_ERRC_SIS_YA_EXIST);
			}
		} finally{
			loConexion.cerrarDB(lbExito);
		}
		return liConsecSistema;
	}


	/*****************************************************************************
   *		 void bloquearSistemaUpdateDB()
   *****************************************************************************/
	public void bloquearSistemaUpdateDB( String esCveSistema,String esBloqueo)
			throws Exception, SQLException, SeguException {
		CConexionDBPool	loConexion = new CConexionDBPool();
		int 					liCodigoError;
		int 					liNumRegActualizados;
		boolean 				lbExito = false;

		liNumRegActualizados = 0;
		String lsSQL;
		lsSQL= "UPDATE SEG_SISTEMAS SET b_bloqueado = '" + esBloqueo + "' " +
				" WHERE c_sistema ='"+esCveSistema + "'";
		if (CConst.TRACE) {
			System.out.println("\nCSistema::bloquearSistemaUpdateDB()");
		}
		try {
			loConexion.conectarDB();
		} catch(Exception e) {
			throw new SeguException(CConst.COT_ERRC_DB);
		}
		try{
			liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if (CConst.TRACE) {
				System.out.println("El numero de reg actualizados fue:" + liNumRegActualizados);
			}
			if(liNumRegActualizados == 0) {
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
			}
			lbExito = true;
		} catch(SQLException ex) {
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			System.out.println("Error en el Update de bloquearSistemaUpdateDB()");
			System.out.println("el c�digo es : (" + liCodigoError +")");
			System.out.println("\n" + ex.getMessage());
		}finally{
			loConexion.cerrarDB(lbExito);
		}
	}
	/*****************************************************************************
	*    void borrarFacultadMem
	*****************************************************************************/
	public void borrarFacultadMem(String esCveFacultad) {
		if (CConst.TRACE) {
			System.out.println("\tCSistema::borrarFacultadMem()");
		}
		m_htFacultades.remove(esCveFacultad);
	}
	/*****************************************************************************
	*    void borrarPerfilProtMem
	*****************************************************************************/
	public void borrarPerfilProtMem(String esCvePerfilProt) {
		if (CConst.TRACE) {
			System.out.println("\tCSistema::borrarPerfilProtMem()");
		}
		m_htPerfilesProt.remove(esCvePerfilProt);
	}
	/*****************************************************************************
	*      void borrarSistemaDeleteDB()
	*****************************************************************************/
	public void borrarSistemaDeleteDB(	String esCveSistema	)
					throws Exception, SQLException, SeguException {
		int liNumRegBorrados;
		int liCodigoError;
		boolean lbExito = false;
		liNumRegBorrados = 0;
		liCodigoError =0;

		CConexionDBPool loConexion = new CConexionDBPool();
		String lsSQL;
		String lsSQLRelSist;

		lsSQLRelSist = "DELETE FROM seg_rel_sist WHERE cc_sistema ='"+ esCveSistema+ "'";
		lsSQL = "DELETE FROM seg_sistemas WHERE c_sistema ='"+ esCveSistema+ "'";

		if (CConst.TRACE) {
			System.out.println("\nCSistema::borrarSistemaDeleteDB()");
		}
		try{
			loConexion.conectarDB();
		} catch(Exception e) {
			throw new SeguException(CConst.COT_ERRC_DB);
		}
		try{
			loConexion.ejecutarUpdateDB(lsSQLRelSist);
			liNumRegBorrados = loConexion.ejecutarUpdateDB(lsSQL);

			if (CConst.TRACE) {
				System.out.println("El numero de registros borrados fue:" + liNumRegBorrados);
			}
		if(liNumRegBorrados == 0)
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ABORR_INEXIST);
			lbExito = true;
		} catch(SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			if(liCodigoError == CConst.ERRC_INTEGR_REFERENC)
				throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			System.out.println("El codigo de error es:" + liCodigoError );
		}finally{
			loConexion.cerrarDB(lbExito);
		}
	}
	/*****************************************************************************
	*		CFacultad buscarFacultad()
	*****************************************************************************/
	public CFacultad buscarFacultad(String esCveFacultad)
	{
		CFacultad loFacultad = null;

                if (CConst.TRACE)
	         	System.out.println("\tCFacultad::buscarFacultad()" );

		if (m_htFacultades.size() > 0)
			loFacultad = (CFacultad) m_htFacultades.get(esCveFacultad);

		return loFacultad ;
	}
	/*****************************************************************************
	*		CPerfilProt buscarPerfilProt()
	*****************************************************************************/
	public CPerfilProt buscarPerfilProt(String esCvePerfilProt)
	{
		CPerfilProt loPerfilProt = null;

		if (m_htPerfilesProt.size() > 0)
			loPerfilProt = (CPerfilProt) m_htPerfilesProt.get(esCvePerfilProt) ;

		return loPerfilProt ;
	}
	/*****************************************************************************
	*		void cambiarDato()
	*****************************************************************************/
	public void cambiarDato(String ssCvePerfil, String ssBloqueado)
	{
		((CPerfilProt) m_htPerfilesProt.get(ssCvePerfil)).setBloqueado(ssBloqueado);
	}



	/**
	 * Consulta las facultades registradas en la BD
	 * @param esCveFacultad Clave de la facultad
	 * @return Objeto facultad si si encontro la facultad o null de lo contrario
	 *
	 */
	public CFacultad consultarFacultadSelectDB(String esCveFacultad) {
		boolean 	lbResultado = true;
		CFacultad loFacultad = null;
		Vector lvRegistro = new Vector();
		Vector lvFacultad = null;

		int liNumColsUsr = 10;
		String lsSQLCargaFac = "";

		CConexionDBPool loCPDB = new CConexionDBPool();

		String lsCveSistema = "";
		String lsCveFacultad = "";
		String lsDescripcion = "";
		String lsDescripcionIngles = "";
		
		String lsBloqueado = "";
		String lsTipoFac = "";
		long llNRestriccion = 0;
		String lsHoraIni = "";
		String lsHoraFin = "";
		String lsFchVencimiento = "";
		String lsPagina = "";
		String lsPaginaIngles = "";
		String lsImagen = "";
		String lsSwImgDesc = "";
		int	liQuitaMnu = 0;
		int	liOrdenMnu = 0;
		String lsClaveRevisar = "";

		lvFacultad = new Vector();

		if (CConst.TRACE) {
			System.out.println("\tCSistema::consultarFacultadSelectDB[" + esCveFacultad +"]");
		}

		lsSQLCargaFac = "SELECT sf.c_sistema, sf.c_facultad, " +
				" sf.d_descripcion," +
				" sf.b_bloqueado, sf.t_tipo_facultad, sf.n_restriccion, " +
				" sr.f_hora_inicial, " +
				" sr.f_hora_final, " +
				" sr.f_vencimiento, "  +
				" p_pagina, " +
				" sf.sg_sw_quita_mnu, sf.sg_orden_mnu, " +
				" sf.cg_imagen,sf.cg_sw_img_desc, " +
				" sf.d_descripcion_ing, p_pagina_ing, " +
				" sf.cg_clave_revisar " +
				" FROM seg_facultades sf, seg_restricfac sr " +
				" WHERE sf.n_restriccion = sr.n_restriccion(+) " +
				" AND sf.c_facultad = '" + esCveFacultad + "' " +
				" AND sf.c_sistema = '" + m_sCveSistema + "' " +
				" ORDER BY sf.c_sistema, sf.c_facultad ";

		if (CConst.TRACE) {
			System.out.println("\tCLUsuario::consultarFacultadSelectDB[" + lsSQLCargaFac +"]");
		}

		try {
			loCPDB.conectarDB();
			try {
				loCPDB.ejecutarQueryDB(lsSQLCargaFac,liNumColsUsr,lvFacultad);

				if (lvFacultad.size() > 0){
					lvRegistro = (Vector) lvFacultad.elementAt(0);

					lsCveSistema = (String) lvRegistro.elementAt(0);
					lsCveFacultad = (String) lvRegistro.elementAt(1);
					lsDescripcion = (String) lvRegistro.elementAt(2);
					lsDescripcionIngles = (String) lvRegistro.elementAt(14);
					lsBloqueado = (String) lvRegistro.elementAt(3);
					lsTipoFac = (String) lvRegistro.elementAt(4);
					llNRestriccion = ((Integer)lvRegistro.elementAt(5)).longValue();
					lsHoraIni = (String) lvRegistro.elementAt(6);
					lsHoraFin = (String) lvRegistro.elementAt(7);
					lsFchVencimiento = (String) lvRegistro.elementAt(8);
					lsPagina = (String) lvRegistro.elementAt(9);
					lsPaginaIngles = (String) lvRegistro.elementAt(15);
					liQuitaMnu = ((Integer) lvRegistro.elementAt(10)).intValue();
					liOrdenMnu = ((Integer) lvRegistro.elementAt(11)).intValue();
					lsImagen = (String) lvRegistro.elementAt(12);
					lsSwImgDesc = (String) lvRegistro.elementAt(13);
					lsClaveRevisar = (String) lvRegistro.elementAt(16);

					loFacultad = new CFacultad(
							lsCveSistema, lsCveFacultad,
							lsDescripcion, lsDescripcionIngles,
							lsBloqueado, lsTipoFac,
							llNRestriccion, lsHoraIni, lsHoraFin,
							lsFchVencimiento, lsPagina, lsPaginaIngles,
							liQuitaMnu, liOrdenMnu,
							lsImagen, lsSwImgDesc, lsClaveRevisar);
				}
			}catch(SQLException e){
				lbResultado = false;
				System.out.println(e);
				//throw new SeguException("CSistema::consultarFacultadSelectDB() Error en la BD");
			}finally{
				loCPDB.cerrarDB(lbResultado);
			}
		}catch(Exception e){
			System.out.println("CSistema::consultarFacultadSelectDB() Error en la BD");
			//throw new SeguException(CConst.COT_ERRC_DB);
		}
		return loFacultad ;
	}


	/**
	 * Consulta las Facultades x Sistema
	 * @param esCveSistema Clave del sistema
	 * @return Vector con los datos de las facultades
	 *		0 Clave del sistema
	 *		1 Clave de la facultad
	 *		2 Descripcion
	 *		3 Indicador de bloqueo
	 *		4 Tipo de facultad
	 *		5 Restriccion
	 *		6 Pagina
	 *		7 ???
	 *		8 ???
	 *		9 ???
	 *		10 ???
	 *		11 ???
	 *		12 Imagen a mostrar (cuando aplique)
	 *		13 indicador de uso de imagen o texto
	 *		14 Descripcion en idioma ingles
	 *		15 Pagina para idioma ingles
	 * 		16 Clave Revisar.
	 *
	 */
	public Vector consultarFacxSistema(String esCveSistema)
			throws SeguException, Exception {
		Vector svTabla;

		String lsSQL ;
		int liNumCols ;
		int liNumReg ;
		boolean	lbExito = false;
		liNumCols = 0;

		svTabla = new Vector();

		if (CConst.TRACE) {
			System.out.println("CSistema::consultarFacxSistema()");
		}

		CConexionDBPool loConexion = new CConexionDBPool();

		try {
			loConexion.conectarDB();
		} catch(Exception e) {
			throw new SeguException(CConst.COT_ERRC_DB);
		}
		try{
			lsSQL = "SELECT f.c_sistema, f.c_facultad, " +
					" f.d_descripcion, " +
					" f.b_bloqueado,f.t_tipo_facultad, " +
					" f.n_restriccion, f.p_pagina, " +
					" f.sc_asignacion, " +
					" ca.cg_sw_banco, ca.cg_sw_cliente, " +
					" ca.cg_sw_central, f.sg_sw_quita_mnu, " +
					" f.cg_imagen,f.cg_sw_img_desc, " +
					" f.d_descripcion_ing, f.p_pagina_ing, " +
					" f.cg_clave_revisar " +
					" FROM seg_facultades f, deccat_asignacion ca " +
					" WHERE f.c_sistema = '" + esCveSistema + "'" +
					" AND ca.sc_asignacion = f.sc_asignacion " +
					" ORDER BY f.c_sistema, f.c_facultad ";

			loConexion.ejecutarQueryDB(lsSQL, liNumCols,svTabla);
			if(svTabla.size()== 0) {
				throw new SeguException(CConst.SEG_ERRC_SISXFAC_INEXIST);
			}
			if (CConst.TRACE) {
				System.out.println("CSistema::el tam de la tabla es: " + svTabla.size());
			}
			lbExito = true;
		} catch(SQLException ex){
			lbExito = false;
			System.out.println("Error en el select"+ ex.getMessage());
		} finally{
			loConexion.cerrarDB(lbExito);
		}
		return svTabla;
	}
	
	/*****************************************************************************
	*		void consultarRefFacPPSelectDB()
	*****************************************************************************/
	public void consultarRefFacPPSelectDB(String esCvePerfilProt, Vector svPPFacultades)
			throws Exception, SQLException, SeguException {
		boolean	lbResultado = true;
		int liNumColsPP = 6;
		String lsSQLCargaPP = "";
		Vector lvRefFacultades = null;

		CConexionDBPool loCPDB = new CConexionDBPool();

		lvRefFacultades = new Vector();

		if (CConst.TRACE) {
			System.out.println("\tCSistema::consultarRefFacPPSelectDB[" + esCvePerfilProt +"]");
		}

		lsSQLCargaPP = "SELECT fpp.c_facultad, fpp.n_restriccion, ";
		lsSQLCargaPP += "rf.f_hora_inicial, ";
		lsSQLCargaPP += "rf.f_hora_final, ";
		lsSQLCargaPP += "rf.f_vencimiento, ";
		lsSQLCargaPP += "fpp.b_mancomunidad ";
		lsSQLCargaPP += "FROM seg_perfilprot pp, seg_facperfpr fpp, seg_restricfac rf " ;
		lsSQLCargaPP += "WHERE pp.c_sistema = fpp.c_sistema " ;
		lsSQLCargaPP += " AND pp.c_perfil_prot = fpp.c_perfil_prot " ;
		//lsSQLCargaPP += " AND fpp.n_restriccion *= rf.n_restriccion " ;
		lsSQLCargaPP += " AND fpp.n_restriccion = rf.n_restriccion(+) " ;
		lsSQLCargaPP += " AND pp.c_perfil_prot = '" + esCvePerfilProt + "'" ;
		lsSQLCargaPP += " AND pp.c_sistema = '" + m_sCveSistema + "'" ;
		lsSQLCargaPP += " ORDER BY pp.c_sistema, pp.c_perfil_prot " ;

		if (CConst.TRACE) {
			System.out.println("\tCSistema::consultarRefFacPPSelectDB[" + lsSQLCargaPP +"]");
		}

		try{
			loCPDB.conectarDB();

			loCPDB.ejecutarQueryDB(lsSQLCargaPP,liNumColsPP,lvRefFacultades);

			svPPFacultades.addAll(lvRefFacultades);

			if (CConst.TRACE){
				System.out.println("\tCSistema::consultarRefFacPPSelectDB TAMANO lvRefFacultades [" + lvRefFacultades.size() +"]");
				System.out.println("\tCSistema::consultarRefFacPPSelectDB TAMANO svPPFacultades [" + svPPFacultades.size() +"]");
			}
		}catch(SQLException e){
			lbResultado = false;
			System.out.println(e);
			throw new SeguException("CSistema::consultarRefFacPPSelectDB() Error en la BD");
		}catch(Exception e){
			lbResultado = false;
			System.out.println(e);
			throw new SeguException("CSistema::consultarRefFacPPSelectDB() Error en la BD");
		}finally{
			loCPDB.cerrarDB(lbResultado);
		}
	}


	/**
	 * Consulta de los Sistemas de la aplicacion.
	 * @return Vector con la informaci�n de los sistemas registrados:
	 * 		0. Clave del sistema
	 * 		1. Descripcion en espa�ol
	 * 		2. Indicador de bloqueo
	 * 		3. Ruta de la imagen
	 * 		4. Indicador de orden
	 * 		5. Accion de la liga (Ventana Arriba, Ventana Adentro)
	 * 		6. URL al que hace referencia el sistema
	 * 		7. Identificador de si se usa la imagen o el texto.
	 * 		8. Sistema padre si existe
	 * 		9. ??b_menu
	 * 		10. Nivel
	 * 		11. ??estatus
	 * 		12. Descripcion en Ingles
	 *
	 */
	public Vector consultarSistemasSelectDB()
			throws Exception, SQLException, SeguException {
		Vector svTabla;

		String lsSQL;
		int	 liNumCols = 0;
		int liNumReg = 0;
		boolean lbExito = false;

		svTabla = new Vector();

		if (CConst.TRACE) {
			System.out.println("\nCSistema::consultarSistemasSelectDB()");
		}
		CConexionDBPool loConexion = new CConexionDBPool();
		lsSQL = " SELECT ss.c_sistema, ss.d_descripcion," +
				" ss.b_bloqueado, " +
				" ss.t_imagen, ss.sg_orden_mnu, ss.sg_sw_accion_click, " +
				" ss.cg_liga, ss.cg_sw_img_desc, rs.cc_sistema_2, ss.b_menu, " +
				" rs.n_nivel, rs.cg_status, ss.d_descripcion_ing " +
				" FROM seg_sistemas ss, seg_rel_sist rs " +
				" WHERE ss.c_sistema = rs.cc_sistema(+) " +
				" ORDER BY ss.c_sistema ";
		try {
			loConexion.conectarDB();
		} catch(Exception ex) {
			lbExito = false;
			throw new SeguException(CConst.COT_ERRC_DB);
		}
		try {
			loConexion.ejecutarQueryDB(lsSQL, liNumCols,svTabla);
			if(svTabla.size()==0) {
				throw new SeguException(CConst.SEG_ERRC_SIS_TABLA_VACIA);
			}
			lbExito = true;
		} catch (SQLException ex){
			lbExito = false;
			System.out.println("Error en la consulta"+ ex.getMessage());
		} finally {
			loConexion.cerrarDB(lbExito);
		}
		return svTabla;
	}



	/**
	 * Genera el segmento del xml correspondiente al sistema
	 * @return Cadena con un fragmento de xml
	 *
	 */
	public String generarXML() {
		char lcComilla = '"';
		StringBuffer lsCadenaXML = new StringBuffer();

		lsCadenaXML.append("<sistema cvesistema=" + lcComilla + m_sCveSistema + lcComilla);
		lsCadenaXML.append(" descsistema=" + lcComilla + m_sDescripcion + lcComilla);
		lsCadenaXML.append(" descsistemaingles=" + lcComilla + m_sDescripcionIngles + lcComilla);
		lsCadenaXML.append(" imagen=" + lcComilla + m_sImagen + lcComilla);
		lsCadenaXML.append(" accionclick=" + lcComilla + m_iAccLiga + lcComilla);
		lsCadenaXML.append(" sw_img_desc=" + lcComilla + m_sSwImg + lcComilla);
		lsCadenaXML.append(" liga_open=" + lcComilla + m_sLiga + lcComilla);
		lsCadenaXML.append(" orden_menu=" + lcComilla + m_iOrdenMenu + lcComilla + ">");

		return lsCadenaXML.toString();
	}
	
	/**
	 * Genera el segmento de xml correpondiente al sistema
	 * @param eiNSubSistema ???
	 * @param esCveSistPadre Clave del sistema Padre
	 * @return Cadena con un fragmento de xml
	 *
	 */
	public String generarXML(int eiNSubSistema, String esCveSistPadre) {
		char lcComilla = '"' ;
		StringBuffer lsCadenaXML = new StringBuffer();

		if ( eiNSubSistema == 0 ) {
			lsCadenaXML.append( generarXML() );
		} else {
			lsCadenaXML.append("<subsistema" + " cvesubsistema=" + lcComilla + m_sCveSistema + lcComilla);
			lsCadenaXML.append(" descsistema=" + lcComilla + m_sDescripcion + lcComilla);
			lsCadenaXML.append(" descsistemaingles=" + lcComilla + m_sDescripcionIngles + lcComilla);
			lsCadenaXML.append(" imagen=" + lcComilla + m_sImagen + lcComilla );
			lsCadenaXML.append(" cvesistpadre=" + lcComilla + esCveSistPadre + lcComilla );
			lsCadenaXML.append(" accionclick=" + lcComilla + m_iAccLiga + lcComilla);
			lsCadenaXML.append(" sw_img_desc=" + lcComilla + m_sSwImg + lcComilla);
			lsCadenaXML.append(" liga_open=" + lcComilla + m_sLiga + lcComilla);
			lsCadenaXML.append(" orden_menu=" + lcComilla + m_iOrdenMenu + lcComilla);
			lsCadenaXML.append(" nivel=" + lcComilla + eiNSubSistema + lcComilla );
			lsCadenaXML.append(" quitar_mnu=" + lcComilla + m_sQuitaMnu + lcComilla + ">");
		}
		return lsCadenaXML.toString();
	}
	
	
	/*****************************************************************************
	*		String getBloqueado
	*****************************************************************************/
	public String getBloqueado() {
		return m_sBloqueado;
	}
	/*****************************************************************************
	*		String getCveSistema
	*****************************************************************************/
	public String getCveSistema() {
		return m_sCveSistema;
	}
	
	/**
	 * Obtiene la descripcion del sistema
	 * @return Cadena con la descripcion del sistema
	 */
	public String getDescripcion() { 
		return m_sDescripcion;
	}

	/**
	 * Obtiene la descripcion del sistema en idioma ingles
	 * @return Cadena con la descripcion del sistema
	 */
	public String getDescripcionIngles() {
		return m_sDescripcionIngles;
	}

	/*****************************************************************************
	*		CFacultad getFacultad
	*****************************************************************************/
	public CFacultad getFacultad(String esCveFacultad)
	{
		if (CConst.TRACE) {
			System.out.println("\tCSistema::getFacultad()");
		}
		return ( (CFacultad) m_htFacultades.get(esCveFacultad) );
	}
	/*****************************************************************************
	*		String getImagen
	*****************************************************************************/
	public String getImagen() {
		return m_sImagen;
	}
	/*****************************************************************************
	*		int getOrdenMenu
	*****************************************************************************/
	public int getOrdenMenu() {
		return m_iOrdenMenu;
	}
	/*****************************************************************************
	*		CPerfilProt getPerfilProt
	*****************************************************************************/
	public CPerfilProt getPerfilProt(String esCvePerfilProt) {
		CPerfilProt loPerfilProt = null;
		if (CConst.TRACE) {
			System.out.println("\tCSistema::getPerfilProt()");
		}
		loPerfilProt = (CPerfilProt) m_htPerfilesProt.get(esCvePerfilProt);
		return loPerfilProt;
	}
	/*****************************************************************************
	*		void	getQuitaMnu
	*****************************************************************************/
	public String	getQuitaMnu() {
		return m_sQuitaMnu;
	}

	/**
	 * Permite modificar los datos del sistema en BD con los datos
	 * actuales de este bean
	 */
/*	public void modificarSistema()
			throws Exception, SQLException {
		boolean lbResultDB = true;
		CConexionDBPool loConexion = new CConexionDBPool();
		int liCodigoError;
		int liNumRegActualizados;

		liNumRegActualizados = 0;
		String lsSQL = 
				" UPDATE SEG_SISTEMAS " +
				" SET d_descripcion = '" + m_sDescripcion + "'," +
				" d_descripcion_ing = '" + m_sDescripcionIngles + "'," +
				" b_bloqueado = '" + m_sBloqueado + "'," +
				" t_imagen =" + "'" + m_sImagen + "'" +
				" WHERE c_sistema = '" + m_sCveSistema + "'";
		try {
			loConexion.conectarDB();
			liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if(liNumRegActualizados == 0) {
				throw	new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
			}
			if (CConst.TRACE) {
				System.out.println("El numero de registros actualizados fue:" + liNumRegActualizados);
			}
		} catch(SQLException ex) {
			lbResultDB = false;
			liCodigoError = ex.getErrorCode();
			System.out.println("Error en el update, el c�digo es : (" + liCodigoError +")");
		}finally{
			loConexion.cerrarDB(lbResultDB);
		}
	}
*/	
	

	/**
	 * Permite modificar los datos del sistema en BD con los datos
	 * actuales de este bean
	 * @param esCveSistPadre Clave del sistema padre
	 * @param eoLRelSistemas Relaciones de sistemas
	 * @param esEstatus ??
	 */
	public void modificarSistemaUpdateDB(
			String esCveSistPadre, CLRelSistema eoLRelSistemas,
			String esStatus)
			throws Exception, SQLException {
		CConexionDBPool loConexion = new CConexionDBPool();
		int liCodigoError;
		int liNumRegActualizados;
		boolean	lbExito = false;

		liNumRegActualizados=0;
		String lsSQL ;
		lsSQL = " UPDATE SEG_SISTEMAS " +
				" SET d_descripcion = '" + m_sDescripcion + "'," +
				" d_descripcion_ing = '" + m_sDescripcionIngles + "'," +
				" b_bloqueado = '" + m_sBloqueado + "'," +
				" t_imagen =" + "'" + m_sImagen + "'," +
				" cg_sw_img_desc = '" + m_sSwImg + "'," +
				" cg_liga = '" + m_sLiga+ "'," +
				" sg_sw_accion_click = " + m_iAccLiga + "," +
				" b_menu = '" + m_sQuitaMnu + "'" +
				" WHERE" + " c_sistema = '" + m_sCveSistema + "'";

		if (CConst.TRACE) {
			System.out.println("\nCSistema::modificarSistemaUpdateDB()");
		}
		try {
			loConexion.conectarDB();
		} catch (SQLException ex) {
			throw	new SeguException(CConst.COT_ERRC_DB);
		}
		try	{
			liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if (liNumRegActualizados == 0) {
				throw	new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
			}
			if (CConst.TRACE) {
				System.out.println("El numero de registros actualizados fue:" + liNumRegActualizados);
			}

			lsSQL =  " DELETE FROM seg_rel_sist " +
					" WHERE cc_sistema = '" + m_sCveSistema + "'";

			loConexion.ejecutarInsertDB(lsSQL);
			if (esCveSistPadre.length() > 0) {
				int liNivel = eoLRelSistemas.devolverNivelPadre(esCveSistPadre);
				if (CConst.TRACE) {
					System.out.print("\tNivel del Sistema Padre::" + liNivel);
				}
				liNivel++;
				if (CConst.TRACE) {
					System.out.println("\tNivel del Sistema::" + liNivel);
				}

				lsSQL =  " INSERT INTO seg_rel_sist " +
						" (cc_sistema,cc_sistema_2, n_nivel, cg_status ) " +
						" VALUES ('" + m_sCveSistema + "'," +
						" '" + esCveSistPadre + "'," +
						liNivel +",'"+ esStatus +"')";
				loConexion.ejecutarInsertDB(lsSQL);
			}
			lbExito = true;
		} catch (SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			System.out.println("Error en el update, el c�digo es : (" + liCodigoError +")");
		} finally {
			loConexion.cerrarDB(lbExito);
		}
	}


	/**
	 * Permite modificar los datos del sistema en BD con los datos
	 * actuales de este bean
	 * @param esCveSistPadre Clave del sistema padre
	 */
	public void modificarSistemaUpdateDB(String esCveSistPadre)
			throws Exception, SQLException {
		CConexionDBPool loConexion = new CConexionDBPool();
		int liCodigoError;
		int liNumRegActualizados;
		boolean	lbExito = false;

		liNumRegActualizados = 0;
		String lsSQL ;
		lsSQL = " UPDATE SEG_SISTEMAS " +
				" SET d_descripcion = '" + m_sDescripcion + "'," +
				" d_descripcion_ing = '" + m_sDescripcionIngles + "'," +	
				" b_bloqueado = '" + m_sBloqueado + "'," +
				" t_imagen =" + "'" + m_sImagen + "'," +
				" cg_sw_img_desc = '" + m_sSwImg + "'," +
				" cg_liga = '" + m_sLiga+ "'," +
				" sg_sw_accion_click = " + m_iAccLiga + "," +
				" b_menu = '" + m_sQuitaMnu + "'" +
				" WHERE" + " c_sistema ='" + m_sCveSistema + "'";

		if (CConst.TRACE) {
			System.out.println("\nCSistema::modificarSistemaUpdateDB()");
		}
		try {
			loConexion.conectarDB();
		} catch(SQLException ex) {
			throw new SeguException(CConst.COT_ERRC_DB);
		}
		try {
			liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if(liNumRegActualizados == 0) {
				throw	new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
			}
			if (CConst.TRACE) {
				System.out.println("El numero de registros actualizados fue:" + liNumRegActualizados);
			}

			lsSQL =  " DELETE FROM seg_rel_sist " +
					" WHERE cc_sistema = '" + m_sCveSistema + "'";

			loConexion.ejecutarInsertDB(lsSQL);

			if (esCveSistPadre.length() > 0) {
				lsSQL =  " INSERT INTO seg_rel_sist " +
						" (cc_sistema,cc_sistema_2) VALUES ('" +
						m_sCveSistema + "','" + esCveSistPadre + "')";
				loConexion.ejecutarInsertDB(lsSQL);
			}
			lbExito = true;
		} catch(SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			System.out.println("Error en el update, el c�digo es : (" + liCodigoError +")");
		} finally{
			loConexion.cerrarDB(lbExito);
		}
	}
	
	/*****************************************************************************
	*		void printFacultades
	*****************************************************************************/
	public void printFacultades() {
		if (CConst.TRACE) {
			System.out.println("\t CSistema::printFacultades ");
		}

		Enumeration enum1 = m_htFacultades.keys();
		while (enum1.hasMoreElements()){
			if (CConst.TRACE) {
				System.out.println("\t Llaves en Facultades[" + enum1.nextElement() + "]");
			}
		}
	}
	/*****************************************************************************
	*		void setAccionLiga
	*****************************************************************************/
	public void setAccionLiga(int eiAccLiga) {
		m_iAccLiga = eiAccLiga;
	}
	/*****************************************************************************
	*		void	setBloqueado
	*****************************************************************************/
	public void	setBloqueado(String vsBloqueado) {
		m_sBloqueado = vsBloqueado;
	}
	/*****************************************************************************
	*		void setCveSistema
	*****************************************************************************/
	public void setCveSistema(String vsCveSistema) {
		m_sCveSistema = vsCveSistema;
	}

	/**
	 * Establece la descripcion
	 * @param vsDescripcion Descripcion del sistema
	 */
	public void	setDescripcion(String vsDescripcion) {
		m_sDescripcion = vsDescripcion;
	}

	/**
	 * Establece la descripcion en idioma ingl�s
	 * @param vsDescripcionIngles Descripcion en ingles
	 */
	public void	setDescripcionIngles(String vsDescripcionIngles) {
		m_sDescripcionIngles = vsDescripcionIngles;
	}
	
	/*****************************************************************************
	*		void setImagen
	*****************************************************************************/
	public void setImagen(String esImagen) {
		m_sImagen = esImagen ;
	}
	/*****************************************************************************
	*		void setLiga
	*****************************************************************************/
	public void setLiga(String esLiga) {
		m_sLiga = esLiga ;
	}
	/*****************************************************************************
	*		void setOrdenMenu
	*****************************************************************************/
	public void setOrdenMenu(int eiOrdenMenu) {
		m_iOrdenMenu = eiOrdenMenu;
	}
	/*****************************************************************************
	*		void	setQuitaMnu
	*****************************************************************************/
	public void	setQuitaMnu(String esQuitaMnu) {
		m_sQuitaMnu = esQuitaMnu;
	}
	/*****************************************************************************
	*		void setSwImg
	*****************************************************************************/
	public void setSwImg(String esSwImg) {
		m_sSwImg = esSwImg ;
	}
}
