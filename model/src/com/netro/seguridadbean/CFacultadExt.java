package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CFacultadExt.java
*
*	Proposito:	Clase CFacultadExt
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

/****************************************************************************
*
*	clase CFacultadExt
*
*****************************************************************************/
public class CFacultadExt {
		CFacultad	m_fRefFacultad;
		String  	m_sCveUsuario;
		String 	m_sCveFacultadExt;
		String 	m_sSigno;
		long 	m_lNRestriccion;
		String 	m_sHoraInicial;
		String 	m_sHoraFinal;
		String 	m_sFchVencimiento;
		String   m_sMancomunidad;

	/*****************************************************************************
	*		Constructor_1
	*****************************************************************************/
	public CFacultadExt()
	{
                if (CConst.TRACE)
	        	System.out.println("CFacultadExt::Constructor_1");

		m_fRefFacultad = null;
		m_sCveUsuario = "";
		m_sCveFacultadExt = "";
		m_sSigno = "";
		m_lNRestriccion = 0;
		m_sHoraInicial = "";
		m_sHoraFinal = "";
		m_sFchVencimiento = "";
		m_sMancomunidad = "";
	}
	/*****************************************************************************
	*		Constructor_2
	*****************************************************************************/
	public CFacultadExt(		String esCveUsuario,			String esCveFacultadExt,
							long elRestriccion,			String esSigno,
							String esHoraInicial, 		String esHoraFinal,
							String esFchVencimiento,	String esMancomunidad,
							CFacultad efFacultad)
	{
                if (CConst.TRACE)
	        	System.out.println("CFacultadExt::Constructor_2");

		m_fRefFacultad = efFacultad;

                if (CConst.TRACE)
	        	System.out.println("\t\t\t\tCFacultadExt::" + m_fRefFacultad.getCveFacultad());

		m_sCveUsuario = esCveUsuario ;
		m_sCveFacultadExt = esCveFacultadExt ;
		m_sSigno = esSigno ;
		m_lNRestriccion = elRestriccion ;
		m_sHoraInicial = esHoraInicial ;
		m_sHoraFinal = esHoraFinal ;
		m_sFchVencimiento = esFchVencimiento ;
		m_sMancomunidad = esMancomunidad;

	}
	/*****************************************************************************
	*		String generarXML()
	*****************************************************************************/
	public String generarXML()
	{
		String lsCadenaXML = "";
		char lcComilla = '"';

		lsCadenaXML = "<facultadext cvefacultadext=" + lcComilla + m_sCveFacultadExt + lcComilla ;
		lsCadenaXML = lsCadenaXML + ">";
		lsCadenaXML = lsCadenaXML + "</facultadext>";

                if (CConst.TRACE)
	        	System.out.println("\tCFacultadExt[" + lsCadenaXML + "]");

		return lsCadenaXML ;
	}
	/*****************************************************************************
	*		String getCveFacultadExt
	*****************************************************************************/
	  public String getCveFacultadExt()
	{
		return m_sCveFacultadExt;
	}
	/*****************************************************************************
	*		String getCveUsuario
	*****************************************************************************/
	  public String getCveUsuario()
	{
		return m_sCveUsuario;
	}
	/*****************************************************************************
	*		String getFchVencimiento
	*****************************************************************************/
	  public String getFchVencimiento()
	{
		return m_sFchVencimiento;
	}
	/*****************************************************************************
	*		String getHoraFinal
	*****************************************************************************/
	  public String getHoraFinal()
	{
		return m_sHoraFinal;
	}
	/*****************************************************************************
	*		String getHoraInicial
	*****************************************************************************/
	  public String getHoraInicial()
	{
		return m_sHoraInicial;
	}
	/*****************************************************************************
	*		String getMancomunidad()
	*****************************************************************************/
	public String getMancomunidad()
	{
		return m_sMancomunidad;
	}
	/*****************************************************************************
	*		CFacultad getRefFacultad()
	*****************************************************************************/
	public CFacultad getRefFacultad()
	{
		return m_fRefFacultad;
	}
	/*****************************************************************************
	*		long getRestriccion
	*****************************************************************************/
	  public long getRestriccion()
	{
		return m_lNRestriccion;
	}
	/*****************************************************************************
	*		String getSigno
	*****************************************************************************/
	  public String getSigno()
	{
		return m_sSigno;
	}
	/*****************************************************************************
	*		void setCveFacultadExt
	*****************************************************************************/
	  public void setCveFacultadExt(String esCveFacultadExt)
	{
		m_sCveFacultadExt = esCveFacultadExt;
	}
	/*****************************************************************************
	*		void setCveUsuario
	*****************************************************************************/
	  public void setCveUsuario(String esCveUsuario)
	{
		m_sCveUsuario = esCveUsuario;
	}
	/*****************************************************************************
	*		void	setFchVencimiento
	*****************************************************************************/
	  public void	setFchVencimiento(String esFchVencimiento)
	{
		m_sFchVencimiento = esFchVencimiento;
	}
	/*****************************************************************************
	*		void	setHoraFinal
	*****************************************************************************/
	  public void	setHoraFinal(String esHoraFinal)
	{
		m_sHoraFinal = esHoraFinal;
	}
	/*****************************************************************************
	*		void	setHoraInicial
	*****************************************************************************/
	  public void	setHoraInicial(String esHoraInicial)
	{
		m_sHoraInicial = esHoraInicial;
	}
	/*****************************************************************************
	*		void setMancomunidad
	*****************************************************************************/
   public void setMancomunidad(String esMancomunidad)
	{
		m_sMancomunidad = esMancomunidad;
	}
	/*****************************************************************************
	*		void setRefFacultad()
	*****************************************************************************/
	public void setRefFacultad(CFacultad efRefFacultad)
	{
		m_fRefFacultad = efRefFacultad;
	}
	/*****************************************************************************
	*		void	setRestriccion
	*****************************************************************************/
	  public void	setRestriccion(long elRestriccion)
	{
		m_lNRestriccion = elRestriccion;
	}
	/*****************************************************************************
	*		void	setSigno
	*****************************************************************************/
	  public void	setSigno(String esSigno)
	{
		m_sSigno = esSigno;
	}
	/*****************************************************************************
	*		int validarRestriccion()
	*****************************************************************************/
  	public int validarRestriccion()
	{
		int liResult = CConst.ERRC_OK;

                if (CConst.TRACE)
	        	System.out.println("\tCFacultadExt::validarRestriccion(E)");

		if (m_lNRestriccion > 0){
			try{
				String lsPatronFch = "yyyy-MM-dd";
				SimpleDateFormat formatoFch =  new SimpleDateFormat(lsPatronFch);
				String lsPatronHr = "hh:mm:ss";
				SimpleDateFormat formatoHr =  new SimpleDateFormat(lsPatronHr);
				Date ldFechaActual = new Date();
				int liComp = 0;

				if (m_sFchVencimiento.length() > 0){
	  		 		Date ldFchVencimiento = formatoFch.parse(m_sFchVencimiento);
					liComp = ldFechaActual.compareTo(ldFchVencimiento);

					if (liComp < 0){
                                                if (CConst.TRACE)
				 	        	System.out.println("\t\tCFacultad::fecha No Vencida[" + liComp + "]");
					}else{
						liResult = CConst.SEG_ERRC_FEC_VENC;
                                                if (CConst.TRACE)
		 			        	System.out.println("\t\tCFacultad::fecha Vencida[" + liComp + "]");
					}
				}
				if (liResult == CConst.ERRC_OK)
				{
					Date ldHoraActual = new Date();
					CHora lohrHoraActual = new CHora();
					CHora lohrHoraInicial = new CHora();
					CHora lohrHoraFinal = new CHora();

					lohrHoraActual.setCadHora(formatoHr.format(new Date()));
					lohrHoraInicial.setCadHora(m_sHoraInicial);
					lohrHoraFinal.setCadHora(m_sHoraFinal);

					if (lohrHoraActual.compararHora(lohrHoraInicial) > 0){
                                                if (CConst.TRACE)
					        	System.out.println("\t\tBien Hora Inicial");
						if (lohrHoraActual.compararHora(lohrHoraFinal) < 0){
                                                        if (CConst.TRACE)
						        	System.out.println("\t\tBien Hora Final");
						}else{
                                                        if (CConst.TRACE)
						        	System.out.println("\t\tHora Final Invalida");
							liResult = CConst.SEG_ERRC_FUERA_HR;
						}
					}else{
                                                if (CConst.TRACE)
					        	System.out.println("\t\tHora Inicial Invalida");
						liResult = CConst.SEG_ERRC_FUERA_HR;
					}
				}

			}catch(ParseException e){
				System.out.println("Error: " + e);
			}
		}
                if (CConst.TRACE)
	        	System.out.println("\tCFacultadExt::validarRestriccion(S)");
		return liResult;
	}
}