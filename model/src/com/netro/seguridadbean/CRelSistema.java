package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CRelSistema.java
*
*	Proposito:	Clase CRelSistema
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/
/**************************************************************************
 *
 * Modificado por: Cindy A. Ramos Pérez
 *
 * Fecha Modificación: 26/03/2002
 *
 *  Modificado para que soporte multinivel en la relación de sistemas.
 *
***************************************************************************/

import java.util.*;
import java.io.*;

/****************************************************************************
*
*	clase CRelSistema
*
*****************************************************************************/
public class CRelSistema {

	String  m_sCveSistPadre;
	String 	m_sCveSistHijo;
    String  m_sStatus;
    int     m_iNivel;

	/*****************************************************************************
	*		Constructor_1
	*****************************************************************************/
	public CRelSistema()
	{
                if (CConst.TRACE)
	        	System.out.println("CRelSistema::Constructor_1");

		m_sCveSistPadre = "";
		m_sCveSistHijo = "";
	}
	/*****************************************************************************
	*		Constructor_2
	*****************************************************************************/
	public CRelSistema(String esCveSistPadre, String esCveSistHijo)
	{
                if (CConst.TRACE)
	        	System.out.println("CRelSistema::Constructor_2");

		m_sCveSistPadre = esCveSistPadre;
		m_sCveSistHijo = esCveSistHijo;
	}
	/*****************************************************************************
	*		Constructor_3
	*****************************************************************************/
	public CRelSistema(String esCveSistPadre, String esCveSistHijo, int eiNivel,
                String esStatus)
	{
                if (CConst.TRACE)
	        	System.out.println("CRelSistema::Constructor_2");

		m_sCveSistPadre = esCveSistPadre;
		m_sCveSistHijo  = esCveSistHijo;
        m_sStatus       = esStatus;
        m_iNivel        = eiNivel;
	}
	/*****************************************************************************
	*		String getCveSistHijo
	*****************************************************************************/
	public String getCveSistHijo()
	{
		return m_sCveSistHijo;
	}
	/*****************************************************************************
	*		String getCveSistPadre
	*****************************************************************************/
	public String getCveSistPadre()
	{
		return m_sCveSistPadre;
	}
	/*****************************************************************************
	*		String getNivel
	*****************************************************************************/
	public int getNivel()
	{
		return m_iNivel;
	}
	/*****************************************************************************
	*		String getStatus
	*****************************************************************************/
	public String getStatus()
	{
		return m_sStatus;
	}
    /*****************************************************************************
	*		void setCveSistHijo
	*****************************************************************************/
	public void setCveSistHijo(String esCveSistHijo)
	{
		m_sCveSistHijo = esCveSistHijo;
	}
	/*****************************************************************************
	*		void setCveSistPadre
	*****************************************************************************/
	public void setCveSistPadre(String esCveSistPadre)
	{
		m_sCveSistPadre = esCveSistPadre;
	}
    	/*****************************************************************************
	*		void setNivel
	*****************************************************************************/
	public void setNivel(int eiNivel)
	{
		m_iNivel = eiNivel;
	}
	/*****************************************************************************
	*		void setStatus
	*****************************************************************************/
	public void setStatus(String esStatus)
	{
		m_sStatus = esStatus;
	}
}