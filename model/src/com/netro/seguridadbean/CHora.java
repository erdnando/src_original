package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CHora.java
*
*	Proposito:	Clase CHora
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.lang.*;
import java.util.*;
import java.io.*;

/****************************************************************************
*
*	clase CHora
*
*****************************************************************************/
public class CHora {
	String m_sCadHora;
	int	m_iHora;
	int	m_iMinuto;
	int	m_iSegundo;

	/*****************************************************************************
	*		void Constructor 1
	*****************************************************************************/
	public CHora(){
		m_sCadHora = "";
		m_iHora = 0;
		m_iMinuto = 0;
		m_iSegundo = 0;
	}
	/*****************************************************************************
	*		void Constructor 2
	*****************************************************************************/
	public CHora(int eiHora, int eiMinuto, int eiSegundo){
		m_iHora = eiHora;
		m_iMinuto = eiMinuto;
		m_iSegundo = eiSegundo;
	}
	/*****************************************************************************
	*		int compararHora
	*****************************************************************************/
	public int compararHora(CHora ehrHora){
		int liResult = 0;

                if (CConst.TRACE){
	        	System.out.println("Hora Actual[" + m_sCadHora + "]");
		        System.out.println("Hora Comparada[" + ehrHora.getCadHora() + "]");
                }
		if (getHora() > ehrHora.getHora()){
			liResult = 1;
		}else{
			if (getHora() == ehrHora.getHora()){
				if (getMinuto() > ehrHora.getMinuto()){
					liResult = 1;
				}else{
					if (getMinuto() != ehrHora.getMinuto()){
						liResult = -1;
					}
				}
			}else{
				liResult = -1;
			}
		}

                if (CConst.TRACE)
	        	System.out.println("Resultado[" + liResult + "]");
		return liResult;
	}
	/*****************************************************************************
	*		String getCadHora()
	*****************************************************************************/
	public String getCadHora(){
		return m_sCadHora ;
	}
	/*****************************************************************************
	*		int getHora()
	*****************************************************************************/
	public int getHora(){
		return m_iHora ;
	}
	/*****************************************************************************
	*		int getMinuto()
	*****************************************************************************/
	public int getMinuto(){
		return m_iMinuto ;
	}
	/*****************************************************************************
	*		int getSegundo()
	*****************************************************************************/
	public int getSegundo(){
		return m_iSegundo ;
	}
	/*****************************************************************************
	*		int setCadHora()
	*****************************************************************************/
	public int setCadHora(String esCadHora)
	{
		String lsHora = null;
		String lsMinuto = null;
		String lsSegundo = null;
		String lsRestoCad = null;
		int liResult = 0;

		try{

                        if (CConst.TRACE)
		        	System.out.println("CHora::setCadHora(" + esCadHora + ")");

			m_sCadHora = esCadHora;

			if (esCadHora.length() > 0) {
				lsHora = esCadHora.substring(0,esCadHora.indexOf(":"));

				m_iHora = (int) Long.parseLong(lsHora,16);

				lsRestoCad = esCadHora.substring(esCadHora.indexOf(":") + 1, esCadHora.length() );
                                if (CConst.TRACE)
			        	System.out.println("\tResto cadena->" + lsRestoCad);
				lsMinuto = lsRestoCad.substring(0,esCadHora.indexOf(":"));
				m_iMinuto = (int) Long.parseLong(lsMinuto,16);

				lsSegundo = lsRestoCad.substring(lsRestoCad.indexOf(":") + 1, lsRestoCad.length() );
                                if (CConst.TRACE)
			        	System.out.println("\tCadena Segundo->" + lsSegundo);
				m_iSegundo = (int) Long.parseLong(lsSegundo,16);

                                if (CConst.TRACE)
			        	System.out.println("\tHora(" + lsHora + ") Minuto(" + lsMinuto + ") Segundo(" + lsSegundo + ")");

			}else{
				liResult = -1;
			}
		}catch(NumberFormatException e){
			System.out.println("Error:CHora:: " + e);
			liResult = -1;
		}catch(StringIndexOutOfBoundsException e){
			System.out.println("Error:CHora:: " + e);
			liResult = -1;
		}

		return liResult;
	}
	/*****************************************************************************
	*		void setHora()
	*****************************************************************************/
	public void setHora(int eiHora){
		m_iHora = eiHora;
	}
	/*****************************************************************************
	*		void setMinuto()
	*****************************************************************************/
	public void setMinuto(int eiMinuto){
		m_iMinuto = eiMinuto;
	}
	/*****************************************************************************
	*		void setSegundo
	*****************************************************************************/
	public void setSegundo(int eiSegundo){
		m_iSegundo = eiSegundo;
	}
}