package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CEmpresa.java
*
*	Proposito:	Clase CEmpresa
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.util.*;
import java.io.*;

/****************************************************************************
*
*	clase CEmpresa
*
*****************************************************************************/
public class CEmpresa {

	String  m_sCveEmpresa;
	String 	m_sDescripcion;
	String 	m_sBloqueado;

	/*****************************************************************************
	*		Constructor_1
	*****************************************************************************/
	public CEmpresa()
	{
                if (CConst.TRACE)
	        	System.out.println("CEmpresa::Constructor_1");

		m_sCveEmpresa = "";
		m_sDescripcion = "";
		m_sBloqueado = "";
	}
	/*****************************************************************************
	*		Constructor_2
	*****************************************************************************/
	public CEmpresa(		String esCveEmpresa,			String esDescripcion,
						String esBloqueado	)
	{
                if (CConst.TRACE)
	        	System.out.println("CEmpresa::Constructor_2");

		m_sCveEmpresa = esCveEmpresa ;
		m_sDescripcion = esDescripcion ;
		m_sBloqueado = esBloqueado ;
	}
	/*****************************************************************************
	*		String getBloqueado
	*****************************************************************************/
	public String getBloqueado()
	{
		return m_sBloqueado;
	}
	/*****************************************************************************
	*		String getCveEmpresa
	*****************************************************************************/
	public String getCveEmpresa()
	{
		return m_sCveEmpresa;
	}
	/*****************************************************************************
	*		String getDescripcion
	*****************************************************************************/
	public String getDescripcion()
	{
		return m_sDescripcion;
	}
	/*****************************************************************************
	*		void	setBloqueado
	*****************************************************************************/
	public void	setBloqueado(String esBloqueado)
	{
		m_sBloqueado = esBloqueado;
	}
	/*****************************************************************************
	*		void setCveEmpresa
	*****************************************************************************/
	public void setCveEmpresa(String esCveEmpresa)
	{
		m_sCveEmpresa = esCveEmpresa;
	}
	/*****************************************************************************
	*		void	setDescripcion
	*****************************************************************************/
	public void	setDescripcion(String esDescripcion)
	{
		m_sDescripcion = esDescripcion;
	}
}