package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CRefPerfil.java
*
*	Proposito:	Clase CRefPerfil
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.util.*;
import java.io.*;
import java.lang.ref.*;

/****************************************************************************
*
*	clase CRefPerfil
*
*****************************************************************************/
public class CRefPerfil {

	CPerfil  m_perPerfil;
	String 	m_sCvePerfil;

	/*****************************************************************************
	*		Constructor_1
	*****************************************************************************/
	public CRefPerfil()
	{
                if (CConst.TRACE)
	        	System.out.println("CRefPerfil::Constructor");

		m_perPerfil = null;
		m_sCvePerfil = "";
	}
	/*****************************************************************************
	*		String checaPerfBloqueado()
	*****************************************************************************/
	public String checaPerfBloqueado()
	{
		String lsBloqueado = "";

		lsBloqueado = m_perPerfil.getBloqueado();

                if (CConst.TRACE)
	        	System.out.println("\t\tChecaPerfBloqueado 3[" + lsBloqueado + "]");

		return lsBloqueado;
	}
	/*****************************************************************************
	*		String getCvePerfil
	*****************************************************************************/
	public String getCvePerfil()
	{
		return m_sCvePerfil;
	}
	/*****************************************************************************
	*		CPerfil getPerfil()
	*****************************************************************************/
	public CPerfil getPerfil()
	{
		return m_perPerfil;
	}
	/*****************************************************************************
	*		void	setCvePerfil
	*****************************************************************************/
	public void	setCvePerfil(String esCvePerfil)
	{
		m_sCvePerfil = esCvePerfil;
	}
	/*****************************************************************************
	*		void setPerfil()
	*****************************************************************************/
	public void setPerfil(CPerfil eoPerfil)
	{
		m_perPerfil = eoPerfil;
	}
}