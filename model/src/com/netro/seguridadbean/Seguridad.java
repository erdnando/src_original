package com.netro.seguridadbean;

import java.sql.SQLException;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import javax.ejb.EJBObject;

import netropology.utilerias.AppException;
import netropology.utilerias.usuarios.SolicitudArgus;
import netropology.utilerias.usuarios.Usuario;
import javax.ejb.Remote;

@Remote
public interface Seguridad {

/*******************************************************************************************************
*			Mtto Facultades
*******************************************************************************************************/
	public void agregarFacultad(String 	esCveSistema, 	String 	esCveFacultad,
										 String 	esDescripcion,	String 	esDescripcionIngles, String 	esBloqueado,
										 String 	esTipo, 			int 		elNRestriccion,
										 String 	esPagina,	String 	esPaginaIngles,	int 		eiCveAsignac,
										 String 	esImg,         String 	esSwImg,
										 int 		eiSwQuitaMnu, String esClaveRevisar)
					throws	Exception,	SQLException;
	/*****************************************************************************
	*      Mtto Perfiles
	*****************************************************************************/

	public String agregarPerfil(String esCvePerfil, String esBloqueado,
			int eiCveOID, int eiTipoAfiliado)
			throws	Exception, SQLException, SeguException;
/************************************************************************************
*	Mtto Perfiles Prototipo
*************************************************************************************/
	public void agregarPerfilProt(String 	esCveSistema, 		String 			esCvePerfilProt,
											String 	esDescripcion,		String 			esBloqueado,
											int 		eiCveAsignac)
					throws	Exception,	SQLException,	SeguException;
/*******************************************************************************************************
*			Mtto Sistemas
*******************************************************************************************************/
	public void agregarSistema(	String 	esCveSistema, 	String 	esDescripcion,
								String 	esDescripcionIngles,
								String 	esBloqueo,		String 	esImagen,
								String 	esSwImg,      	String 	esLiga,
								int    	eiAccLiga,		String   esCveSistPadre)
							throws Exception;
	public void agregarSistema(	String 	esCveSistema, 	String 	esDescripcion,
								String 	esDescripcionIngles,
								String 	esBloqueo,		String 	esImagen,
								String 	esSwImg,      	String 	esLiga,
								int    	eiAccLiga,		String   esCveSistPadre,
								String  esQuitaMnu,     String esStatus)
							throws Exception;
/************************************************************************************************
*	Mtto Usuarios
************************************************************************************************/

	public void agregarUsuario(	String 	esCveLogin, 	String	esCveUsrNafin,	String	esNombre,
								String 	esAPaterno,		String	esAMaterno,		String 	esEstatus,
								String 	esCorreo,		String 	esContrasena, 	String 	esFechVencim,
								int		eiTipoUsr,		int 	eiCveEPO,		int		eiCvePYME,
								int		eiCveIF,		String	esTipoAfiliacion,String esSIRAC)
						throws	SeguException;
	public void asociarFacPerfilProt(String esCveSistema, String esCvePerfilProt,
												Vector evTablaFacPerfProt)
					throws	Exception,	SQLException, SeguException;
	public void asociarPerfilPerfilesProt(String esCvePerfil, Vector evPerfilesProt)
						throws	Exception,	SQLException, SeguException;
	public void bloquearFacultad(	String esCveSistema, 	String esCveFacultad,
											String esBloqueo)
			   throws 	Exception, 	SQLException;
	public void bloquearPerfilProt(	String 	esCveSistema, 	String esCvePerfilProt,
												String 	esBloqueado)
				throws	SeguException,		Exception,
							SQLException;
	public void bloquearSistema(String esCveSistema, String esBloqueo)
			   throws Exception;
	public void borrarFacultad(	String esCveSistema, 	String esCveFacultad)
					throws 	Exception, 	SQLException;
	public void borrarPerfil(	String esCvePerfil)
						throws	SeguException,		Exception,
						SQLException;
	public void borrarPerfilProt(	String 	esCveSistema, String esCvePerfilProt)
					throws	SeguException,		Exception,
								SQLException;
	public void borrarSistema(String esCveSistema)
					throws SeguException, Exception;

   public void CambiaTRACE()
		throws	Exception,	 	SQLException,
        SeguException;

	public void cambiarDato(String ssCveSistema, String ssCvePerfil, String ssBloqueado)
					throws SeguException;
	public void cerrarSession(String esCveUsuario)
					throws SeguException;
	public Hashtable consultarFacxPerfilProt(String	esCveSistema,	String 	esCvePerfilProt)
					throws	Exception,	 SeguException;
	public Vector consultarFacxSistema(String esCveSistema)
					  throws Exception,  SQLException;
	public Vector consultarPerfiles()
					  throws 	Exception,	 	SQLException,
									SeguException;
	public Vector consultarPerfiles(int eiCveAsignac)
					  throws 	Exception,	 	SQLException,
									SeguException;

	public Vector consultarPerfilesProt()
					  throws 	Exception,	SQLException,
									SeguException;
	public Hashtable consultarPerfilesProtXPerfil(String esCvePerfil)
					  throws 	Exception,	 	SQLException,
									SeguException;
	public Vector consultarPerfilesPxSistema(String esCveSistema)
					  	throws 	Exception,	 	SQLException,
									SeguException;
	public Vector consultarPerfilesXUsuario(String esCveUsuario)
					  	throws 	Exception,	 	SQLException,
									SeguException;
	public Vector consultarSistemas()
						throws Exception, SQLException;
	public Vector consultarUsuarios(	int eiTipoUsr, 		int eiCveEPO,
												int eiCveIF,			int eiCvePYME,
												String esNombre,		String esApPaterno,
												String esApMaterno,	String esCveUsuario)
						throws 	Exception,	 	SQLException,
									SeguException;
	public void crearDatos() throws SeguException;

	public String generaXML( String esClaveAfiliado,
			String esTipoAfiliado, String esCvePerfil )
			throws SeguException;

	public String getXMLUsuario(String esCveUsuario)
			throws SeguException;

	public String getXMLUsuario(Usuario usuario)
			throws SeguException;

	public String getTipoAfiliadoXPerfil(String esCvePerfil)
			throws SeguException;

	public void modificarFacultad(String 	esCveSistema, 	String 	esCveFacultad,
			String 	esDescripcion, String 	esDescripcionIngles, String 	esBloqueo,
			String 	esTipo, int elNRestriccion,
			String 	esPagina, String 	esPaginaIngles,	int eiCveAsignac,
			String 	esImg, String 	esSwImg,
			int eiSwQuitaMnu, String esClaveRevisar)
			throws 	Exception, 	 	SQLException;

	public void modificarPerfil(		String 	esCvePerfil,
										  	  	String 	esBloqueado,
										  	  	int 		eiClaveOId, int eiTipoAfiliado)
						throws	Exception,	 	SQLException,	SeguException;
	public void modificarPerfilProt(String		esCveSistema,	String 	esCvePerfilProt,
										  	  String 	esDescripcion,	String 	esBloqueado,
										  	  int 		eiCveAsignac)
					throws	Exception,	 	SQLException,	SeguException;
	public void modificarSistema(	String esCveSistema,	String esDescripcion,
											String esDescripcionIngles,
										  	String esBloqueo,		String esImagen,
							     			String esSwImg,    	String esLiga,
							     			int    eiAccLiga,		String esCveSistPadre)
					throws	Exception,	 	SQLException;
	public void modificarSistema(	String esCveSistema,	String esDescripcion,
									String esDescripcionIngles,
								  	String esBloqueo,	String esImagen,
									String esSwImg,    	String esLiga,
									int    eiAccLiga,	String esCveSistPadre,
									String  esQuitaMnu, String esStatus)
					throws	Exception,	 	SQLException;
	public void modificarUsuario(	String 	esCveLogin, 	String	esCveUsrNafin,	String	esNombre,
									String 	esAPaterno,		String	esAMaterno,		String 	esEstatus,
									String 	esCorreo,		String 	esContrasena, 	String 	esFechVencim,
									int	 	eiTipoUsr,		int 	eiCveEPO,		int		eiCvePYME,
									int		eiCveIF,		String	esTipoAfiliacion,String esSIRAC)
						throws	SQLException, SeguException;

	public void validaFacultad(	String esCveUsuario, 	String esCvePerfil,
											String esCvePerfProt,	String esCveFacultad,
											String esCveSistema,		String esIP,
											String esHost )
					throws SeguException;

	public boolean vvalidarCveCesion(String esLogin) throws SeguException;

	public void guardarCveCesEncriptada(String esLogin, String esCve) throws SeguException;
	public void guardarCveCesEncriptada(String esLogin, String esCve, String solicitaCveCDer) throws SeguException;

	public boolean vvalidarCveConfirmacion(String esLogin, List valores) throws SeguException;

	public void actualizarEstatusCorreo(String esLogin, String correo) throws SeguException;

	public boolean vvalidarEstatusCorreo(String esLogin) throws SeguException;

	public List getDatosPyME(String claveAfiliado) throws SeguException;

	public void eliminaCesion(String cg_login) throws SeguException;

	public void transferirPrivilegios(String loginOrigen, String loginDestino, String claveUsuario);

	public boolean validaNumeroFISO(String claveUsuario);//FODEA 002 - 2010 ACF

// Fodea 024-2011 (E)
  public List validaCargaMasiva( String archivo, String tipoUsuario);
	public SolicitudArgus generarSolicitudArgus( String tipoAfiliado, String claveAfiliado,
																						 String nombre, String apellidoPaterno, String apellidoMaterno,
																						 String email, String perfil)
																						 throws	Exception,	SQLException,	SeguException;
	public String tmpUsarios(List resultados, String  tipoAfiliado,  String  clave_afiliado);
	public List getUsarios(String numero);
	public String  nombreAfiliado(String claveAfiliado, String tipoAfiliado);
  public String  getBitacora(String ic_nafin, String ic_usuario, String nombreCompletoUsuario, String lista, int numero, String claveAfiliado, String empresa2 )	throws Exception;

	public boolean certificadoGenerado(String usuario);
//Fodea 024-2011 (S)

//FODEA 017-2011 (FVR)
	public void validarUsuario(String esLoginSes, String esLogin, String esPassword)	throws AppException;

	public void validarUsuario(String esLoginSes, String esLogin, String esPassword,
		String esIP, String esHost, String esSistema, String sCvePerfProt,
		String sCveFacultad, String validaFac)
		throws AppException;
	public boolean existeDirector(String area)	throws AppException;

	public String getFecVigenciaCertificado(String noUsuario) throws AppException;
	public boolean validaFolioRevocacionVigente(String noUsuari) throws AppException;
	public void removerCertificadoDigital(String noUsuario, String nafelec, String cveAfiliado)throws AppException;
	public String generaFolioRevocacionCert(String noUsuario, String nafelec, String cveAfiliado)throws AppException;
	public boolean validaFolioCert(String folio, String noUsuario)throws AppException;
	public String getSerialCertDigital(String noUsuari) throws AppException;
	public String generaFolioClaveCesion(String noUsuario)throws AppException;
	public String getFolioClaveCesion(String noUsuario) throws AppException;


	public void validaPermiso( String tipoAfiliado, String ccPerfil, String ccMenu, String ccNombrePermiso, String login )
		throws SeguException;
	public List getPermisosPorMenu( String tipoAfiliado, String ccPerfil, String ccMenu, String[] permisosSolicitados )
		throws AppException;
	public void revocarCertificadoDigital(Usuario usuario, HashMap datosRevocador)
		throws AppException;

	public List consultaHistCveCDer(String loginUsuario) throws AppException;
	public void guardarHistCveCDer(String loginUsuario, String solicitaCveCDer) throws AppException;
	public String consultaParamValidCveCder(String loginUsuario) throws AppException;

}

