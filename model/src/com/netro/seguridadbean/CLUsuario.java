package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CLUsuario.java
*
*	Proposito:	Clase CLUsuario
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.sql.SQLException;
import java.lang.Long;
import java.util.*;
import java.io.*;

/****************************************************************************
*
*	clase CLUsuario
*
*****************************************************************************/
public class CLUsuario {

	Hashtable m_htUsuarios;
	String m_sSQLCargaUsr;
	String m_sSQLCargaFacExt;
	String m_sSQLCargaPerfProt;

	/*****************************************************************************
	*		CLUsuario
	*****************************************************************************/
	public CLUsuario()
	{
		if (CConst.TRACE)
			System.out.println("\t CLUsuario:Constructor");

		m_htUsuarios = new Hashtable();

		m_sSQLCargaUsr = "SELECT ' ', u.cg_login, u.cg_nombre, u.cg_appaterno, ";
		m_sSQLCargaUsr += "u.cg_apmaterno, u.cg_password, u.cg_status, ";
		m_sSQLCargaUsr += "u.sn_intentos, u.df_vencimiento " ;
		m_sSQLCargaUsr += "FROM seg_usuario u " ;
		m_sSQLCargaUsr += "ORDER BY u.cg_login " ;

		m_sSQLCargaFacExt = "SELECT fe.c_usuario, fe.c_facultad, fe.n_restriccion, fe.t_signo, ";
		m_sSQLCargaFacExt += "rf.f_hora_inicial, ";
		m_sSQLCargaFacExt += "rf.f_hora_final, ";
		m_sSQLCargaFacExt += "rf.f_vencimiento, " ;
		m_sSQLCargaFacExt += "fe.c_sistema, fe.b_mancomunidad ";
		m_sSQLCargaFacExt += "FROM seg_facultadex fe, seg_restricFac rf " ;
//		m_sSQLCargaFacExt += "WHERE fe.n_restriccion *= rf.n_restriccion " ;
		m_sSQLCargaFacExt += "WHERE fe.n_restriccion = rf.n_restriccion(+) " ;
		m_sSQLCargaFacExt += "ORDER BY fe.c_usuario, fe.c_facultad " ;
/*
		m_sSQLCargaPerfProt = "SELECT c_usuario, c_sistema, c_perfil_prot ";
		m_sSQLCargaPerfProt = m_sSQLCargaPerfProt + "FROM seg_usrperfpr ";
		m_sSQLCargaPerfProt = m_sSQLCargaPerfProt + "ORDER BY c_usuario, c_sistema, c_perfil_prot ";
*/
		m_sSQLCargaPerfProt = "SELECT upp.c_usuario, upp.c_sistema, upp.c_perfil_prot ";
		m_sSQLCargaPerfProt += "FROM seg_usrperfpr upp, seg_sistemas s ";
		m_sSQLCargaPerfProt += "WHERE upp.c_sistema = s.c_sistema ";
		m_sSQLCargaPerfProt += "ORDER BY upp.c_usuario, s.sg_orden_mnu, upp.c_perfil_prot ";
	}
	/*****************************************************************************
	*		void agregarUsuarioMem()
	*****************************************************************************/
	public void agregarUsuarioMem(CUsuario 	eoUsuario)
	{
		if (eoUsuario != null)
			m_htUsuarios.put(eoUsuario.getCveUsuario(),eoUsuario);
	}

/*****************************************************************************
*		CUsuario bloquearUsuario()
*****************************************************************************/
public void bloquearUsuarios(int esTipo, int esClave, String esBloqueo) {
	boolean existe = false;
	CUsuario loUsuario = null;
        if (CConst.TRACE)
	        System.out.println("\tCLUsuario::bloquearUsuarios(E/S)");

	Vector vector_en = new Vector();
	Enumeration en = m_htUsuarios.keys();
	for (; en.hasMoreElements();) {
		loUsuario = (CUsuario) en.nextElement();
		existe = false;
		if (loUsuario != null) {
			switch (esTipo) {
				case CConst.TP_USR_EPO :
					if(loUsuario.getCveEPO() == esClave)
						existe = true;
					break;
				case CConst.TP_USR_IF :
					if(loUsuario.getCveIF() == esClave)
						existe = true;
					break;
				case CConst.TP_USR_PYME :
					if(loUsuario.getCvePYME() == esClave)
						existe = true;
					break;
			}
			if(existe)
				loUsuario.setStatus(esBloqueo);
		}
	}
}
	/*****************************************************************************
	*		void darBajaUsuarioUpdateDB()
	*****************************************************************************/
	public void bloquearUsuariosUpdateDB(CConexionDBPool eoconDB, int esTipo, int esClave, String esBloqueo)
				   throws SQLException, NullPointerException, Exception
	{
		String lsCadSQL = "";

                if (CConst.TRACE)
	        	System.out.println("\t CLUsuario::bloquearUsuariosUpdateDB()");

		lsCadSQL = "UPDATE seg_usuario ";
		lsCadSQL = lsCadSQL + "SET cg_status = '" + esBloqueo + "' " ;
		lsCadSQL = lsCadSQL + "WHERE " ;
		switch(esTipo){
			case CConst.TP_USR_EPO: lsCadSQL += " ic_epo = " + esClave;
			break;
			case CConst.TP_USR_IF: lsCadSQL += " ic_if = " + esClave;
			break;
			case CConst.TP_USR_PYME: lsCadSQL += " ic_pyme = " + esClave;
			break;
		}

		eoconDB.ejecutarUpdateDB(lsCadSQL);
	}
	/*****************************************************************************
	*		void borrarUsuario()
	*****************************************************************************/
	public void borrarUsuario(String esCveUsuario)
	{
		m_htUsuarios.remove(esCveUsuario);
	}
	/*****************************************************************************
	*		CUsuario buscarUsuario()
	*****************************************************************************/
	public CUsuario buscarUsuario(String esCveUsuario){
		CUsuario loUsuario = null;
		if(CConst.TRACE)
			System.out.println("\tCLUsuario::buscarUsuario(E/S)");

		//return ( (CUsuario) m_htUsuarios.get(esCveUsuario) );
		if(m_htUsuarios.containsKey(esCveUsuario))
			loUsuario = (CUsuario) m_htUsuarios.get(esCveUsuario);

		return loUsuario;
	}
	/*****************************************************************************
	*		void cargarFacultadesExt
	*****************************************************************************/
	public void cargarFacultadesExt(Vector evTmp,	CLSistema elsLSistemas)
					throws NullPointerException, NumberFormatException
	{
		int liNumRegs = 0;
		CFacultadExt loFacultadExt;
		CFacultad	loFacultad;
		Vector lvRegistro ;
		String lsCveUsuario = "";
		String lsCveFacultadExt = "";
		String lsSigno = "";
		long llNRestriccion = 0;
		String lsHoraIni = "";
		String lsHoraFin = "";
		String lsFchVencimiento = "";
		String lsCveSistema = "";
		String lsMancomunidad = "";

                if (CConst.TRACE)
	        	System.out.println("\tCLUsuario::cargarFacultadesExt()");

		liNumRegs = evTmp.size();
		lvRegistro = new Vector();

		for (int liPos = 0; liPos < liNumRegs; liPos++){
			lvRegistro = (Vector) evTmp.elementAt(liPos);

			lsCveUsuario = (String) lvRegistro.elementAt(0);
			lsCveFacultadExt = (String) lvRegistro.elementAt(1);
			llNRestriccion = ((Integer)lvRegistro.elementAt(2)).longValue();

			lsSigno = (String) lvRegistro.elementAt(3);
			lsHoraIni = (String) lvRegistro.elementAt(4);
			lsHoraFin = (String) lvRegistro.elementAt(5);
			lsFchVencimiento = (String) lvRegistro.elementAt(6);
			lsCveSistema = (String) lvRegistro.elementAt(7);
			lsMancomunidad = (String) lvRegistro.elementAt(8);

			loFacultad = elsLSistemas.getFacultad( lsCveSistema, lsCveFacultadExt);


                        if (CConst.TRACE)
		        	System.out.println("\t\t\t\tCFacultadExt::" + loFacultad.getCveFacultad());

			loFacultadExt = new CFacultadExt(	lsCveUsuario,		lsCveFacultadExt,
															llNRestriccion,	lsSigno,
															lsHoraIni,			lsHoraFin,
															lsFchVencimiento,	lsMancomunidad,
															loFacultad);

			((CUsuario) m_htUsuarios.get(lsCveUsuario)).agregarFacultadExt( loFacultadExt );
		}
	}
	/*****************************************************************************
	*		void cargarRefPerfiles
	*****************************************************************************/
	public void cargarRefPerfiles(Vector evTmp,	CLPerfil eoLPerfiles)
					throws NullPointerException
	{
		int liNumRegs = 0;
		CRefPerfil	loPerfilRef = null;
		CPerfil		loppPerfil = null;
		Vector 		lvRegistro = null;
		String 		lsCveUsuario = "";
		String 		lsCvePerfil = "";

		liNumRegs = evTmp.size();

                if (CConst.TRACE)
	        	System.out.println("\tCLUsuario::cargarRefPerfiles");

		for (int liPos = 0; liPos < liNumRegs; liPos++)
		{
			lvRegistro = (Vector) evTmp.elementAt(liPos);

			lsCveUsuario = (String) lvRegistro.elementAt(0);
			lsCvePerfil = (String) lvRegistro.elementAt(1);

			loppPerfil = eoLPerfiles.getPerfil( lsCvePerfil);

			if (loppPerfil != null)
			{
                                if (CConst.TRACE)
			        	System.out.println("\tCLUsuario::cargarRefPerfiles(" + lsCvePerfil + "-" + lsCveUsuario +
													") - Asociado");

				loPerfilRef = new CRefPerfil();

				loPerfilRef.setCvePerfil(lsCvePerfil);
				loPerfilRef.setPerfil(loppPerfil);

				( (CUsuario) m_htUsuarios.get(lsCveUsuario) ).agregarRefPerfil( loPerfilRef );

			}else
			{
                                if (CConst.TRACE)
			        	System.out.println("\tCLUsuario::cargarRefPerfiles(" + lsCvePerfil + "-" + lsCveUsuario +
													") - No Asociado");
			}
		}
	}
	/*****************************************************************************
	*		boolean cargarUsuario
	*****************************************************************************/
	public boolean cargarUsuario(String esCveUsuario	)
				  throws Exception, NullPointerException, SQLException, SeguException
	{
		boolean lbResultDB = true;
		boolean lbResult = false;
		Vector lvUsuario = null;
		Vector lvRegistro = null;

		int liNumColsUsr = 9;
		String lsSQLSelect = "";

		CUsuario loUsuario =null;
		CConexionDBPool loCPDB = new CConexionDBPool();

	  	String 	lsCveEmpresa = "" ;
	  	String 	lsCveUsuario = "" ;
	  	String 	lsNombre = "" ;
	  	String 	lsContrasena = "" ;
	  	String 	lsStatus = "" ;
	  	int liNumIntentos ;
	  	String 	lsFchVencimiento = "" ;
	  	String 	lsCveUsuarioNafin = "" ;
	  	String 	lsNumSIRAC = "" ;

		lvUsuario = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\tCLUsuario::cargarUsuario[" + esCveUsuario +"]");

		lsSQLSelect = "SELECT ' ', u.cg_login, u.cg_nombre, u.cg_appaterno, ";
		lsSQLSelect += " u.cg_apmaterno, u.cg_password, u.cg_status, ";
		lsSQLSelect += " u.sn_intentos, u.df_vencimiento, u.ic_usuario," ;
		lsSQLSelect += " u.cg_numero_sirac" ;
		lsSQLSelect += " FROM seg_usuario u " ;
		lsSQLSelect += " WHERE u.cg_login = '" + esCveUsuario + "' " ;
		lsSQLSelect += " ORDER BY u.cg_login " ;

		try{
			loCPDB.conectarDB();

			if (CConst.TRACE)
				System.out.println("\tCLUsuario::QUERY[" + lsSQLSelect +"]");

			loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsUsr,lvUsuario);

			for (int liPos = 0; liPos < lvUsuario.size(); liPos++ ) {
				lvRegistro = (Vector) lvUsuario.elementAt(liPos);

  				lsCveEmpresa = (String) lvRegistro.elementAt(0) ;
  				lsCveUsuario = (String) lvRegistro.elementAt(1) ;
	  			lsNombre = (String) lvRegistro.elementAt(2) + " " + (String) lvRegistro.elementAt(3) + " " + (String) lvRegistro.elementAt(4) ;
  				lsContrasena = (String) lvRegistro.elementAt(5) ;
  				lsStatus = (String) lvRegistro.elementAt(6) ;
				liNumIntentos = ((Integer)lvRegistro.elementAt(7)).intValue();
	  			lsFchVencimiento = (String) lvRegistro.elementAt(8) ;
	  			lsCveUsuarioNafin = (String) lvRegistro.elementAt(9) ;
	  			lsNumSIRAC = (String) lvRegistro.elementAt(10) ;

                                if (CConst.TRACE)
           	         	        System.out.println("CveUsuario{" + lsCveUsuario + "]");

				loUsuario = new CUsuario(	lsCveEmpresa,			lsCveUsuario,
											lsNombre, 				lsContrasena,
											lsStatus, 				liNumIntentos,
											lsFchVencimiento	);
				loUsuario.setCveUsuarioNafin(lsCveUsuarioNafin);
				loUsuario.setSIRAC(lsNumSIRAC);

                                if (CConst.TRACE)
                                        System.out.println("\tFecha Vencimiento -->"+ loUsuario.getFchVencimiento());
				m_htUsuarios.put(lsCveUsuario, loUsuario);
				lbResult = true;
			}
		}catch(SQLException e){
			lbResult = false;
			System.out.println("CLUsuario::cargarUsuario()" + e);
			throw new SQLException(e.getMessage());
		}finally{
			loCPDB.cerrarDB(lbResultDB);
		}
		return lbResult;
	}
	/*****************************************************************************
	*		void cargarUsuarios
	*****************************************************************************/
	public void cargarUsuarios(Vector evTmp)
					throws NullPointerException, NumberFormatException
	{
		Vector lvRegistro ;
		int liNumRegs = 0;
		CUsuario loUsuario ;

	  	String 	lsCveEmpresa = "" ;
	  	String 	lsCveUsuario = "" ;
	  	String 	lsNombre = "" ;
	  	String 	lsContrasena = "" ;
	  	String 	lsStatus = "" ;
	  	int liNumIntentos = 0;
	  	String 	lsFchVencimiento = "" ;

		liNumRegs = evTmp.size() ;
		lvRegistro = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\tCLUsuario::cargarUsuarios");

		for (int liPos = 0; liPos < liNumRegs; liPos++ ) {
			lvRegistro = (Vector) evTmp.elementAt(liPos);

  			lsCveEmpresa = (String) lvRegistro.elementAt(0) ;
  			lsCveUsuario = (String) lvRegistro.elementAt(1) ;
  			lsNombre = (String) lvRegistro.elementAt(2) + " " + (String) lvRegistro.elementAt(3) + " " + (String) lvRegistro.elementAt(4) ;
  			lsContrasena = (String) lvRegistro.elementAt(5) ;
  			lsStatus = (String) lvRegistro.elementAt(6) ;
			liNumIntentos = ((Integer)lvRegistro.elementAt(7)).intValue();

  			lsFchVencimiento = (String) lvRegistro.elementAt(8) ;

                        if (CConst.TRACE)
	  	                System.out.println("CveUsuario{" + lsCveUsuario + "]");

			loUsuario = new CUsuario(	lsCveEmpresa,			lsCveUsuario,
												lsNombre, 				lsContrasena,
												lsStatus, 				liNumIntentos,
												lsFchVencimiento	);

			m_htUsuarios.put(lsCveUsuario, loUsuario);
		}
	}
	/*****************************************************************************
	*		String checaPerfBloqueado ()
	*****************************************************************************/
	public String checaPerfBloqueado(String esCveUsuario,	String esCveSistema,
												String esCvePerfProt)
	{
		String	lsBloqueado = "";
                if (CConst.TRACE)
	        	System.out.println("\t\tChecaPerfBloqueado 1");

		lsBloqueado =
			((CUsuario) m_htUsuarios.get(esCveUsuario)).checaPerfBloqueado(esCvePerfProt,esCveSistema);

		return lsBloqueado;
	}
	/*****************************************************************************
	*		String getSQLCargaFacExt()
	*****************************************************************************/
	public String getSQLCargaFacExt()
	{
                if (CConst.TRACE)
	        	System.out.println("\tCLUsuario::" + m_sSQLCargaFacExt);
		return m_sSQLCargaFacExt;
	}
	/*****************************************************************************
	*		String getSQLCargaPerfilesProt()
	*****************************************************************************/
	public String getSQLCargaPerfilesProt()
	{
                if (CConst.TRACE)
	        	System.out.println("\tCLUsuario::" + m_sSQLCargaPerfProt);
		return m_sSQLCargaPerfProt;
	}
	/*****************************************************************************
	*		String getSQLCargaUsuarios()
	*****************************************************************************/
	public String getSQLCargaUsuarios()
	{
		return m_sSQLCargaUsr;
	}
	/*****************************************************************************
	*		Enumeration getUsuarios
	*****************************************************************************/
	public Enumeration getUsuarios()
	{
                if (CConst.TRACE)
	        	System.out.println("\tCLUsuario::getUsuarios()");

		Enumeration enum1 = m_htUsuarios.keys();
		return enum1;
	}
}