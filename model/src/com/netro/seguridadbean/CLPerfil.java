package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CLPerfil.java
*
*	Proposito:	Clase CLPerfil
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.util.*;
import java.io.*;
/****************************************************************************
*
*	clase CLPerfil
*
*****************************************************************************/
public class CLPerfil {

	Hashtable m_htPerfiles;
	String m_sSQLCargaPerfiles;
	String m_sSQLCargaPerfilPerfPr;


	/*****************************************************************************
	*		CLPerfil
	*****************************************************************************/

	public CLPerfil()
	{
                if (CConst.TRACE)
	        	System.out.println("\t CLPerfil:Constructor");

		m_htPerfiles = new Hashtable();

		m_sSQLCargaPerfiles = "SELECT cc_perfil, cs_bloqueado, ig_clave_oid, " +
				"sc_tipo_usuario " +
				"FROM seg_perfil " +
				"ORDER BY  cc_perfil " ;

		m_sSQLCargaPerfilPerfPr = "SELECT cc_perfil, c_sistema, c_perfil_prot" +
				" FROM seg_rel_perfil_perfpr" +
				" ORDER BY cc_perfil";

	}
	/*****************************************************************************
	*		void actualizarPerfilPerfilesProt
	*****************************************************************************/
	public void actualizarPerfilPerfilesProt(String esCvePerfil,	Vector evTmp,	CLSistema elsLSistemas)
					throws NullPointerException
	{
		int liNumRegs = 0;
		CRefPerfilProt	loPerfilProtRef = null;
		CPerfilProt	loppPerfilProt = null;
		Vector lvRegistro = null;
		String lsCveSistema = "";
		String lsCvePerfilProt = "";
		CPerfil	loPerfil = null;

		liNumRegs = evTmp.size();

                if (CConst.TRACE)
	        	System.out.println("\tCLPerfil::actualizarPerfilPerfilesProt");

		loPerfil =  (CPerfil) m_htPerfiles.get(esCvePerfil) ;
		if (loPerfil != null)
			loPerfil.borrarRefPerfiles();

		for (int liPos = 0; liPos < liNumRegs; liPos++){
			lvRegistro = (Vector) evTmp.elementAt(liPos);

			lsCveSistema = (String) lvRegistro.elementAt(0);
			lsCvePerfilProt = (String) lvRegistro.elementAt(1);

			loppPerfilProt = elsLSistemas.getPerfilProt( lsCveSistema, lsCvePerfilProt);

			if (loppPerfilProt != null){
                                if (CConst.TRACE)
			        	System.out.println("\tCLPerfil::actualizarPerfilPerfilesProt(" + lsCvePerfilProt + "-" + esCvePerfil +
													") - Asociado");

				loPerfilProtRef = new CRefPerfilProt( 	esCvePerfil, 		lsCveSistema,
																	lsCvePerfilProt,	loppPerfilProt);


				loPerfil.agregarRefPerfilPerfPr( loPerfilProtRef );


			}else{
                                if (CConst.TRACE)
			        	System.out.println("\tCLPerfil::actualizarPerfilPerfilesProt(" + lsCvePerfilProt + "-" + esCvePerfil +
													") - No Asociado");
			}

		}
	}
	/*****************************************************************************
	*		void borrarPerfilMem()
	*****************************************************************************/
	public void borrarPerfilMem(String esCvePerfil)
	{
                if (CConst.TRACE)
	         	System.out.println("\tCLPerfil::borrarPerfilMem()" );

		m_htPerfiles.remove(esCvePerfil) ;
	}
	/*****************************************************************************
	*		CPerfil buscarPerfil()
	*****************************************************************************/
	public CPerfil buscarPerfil(String esCvePerfil)
	{
                if (CConst.TRACE)
	         	System.out.println("\tCLPerfil::buscarPerfil()" );

		return ( (CPerfil) m_htPerfiles.get(esCvePerfil) );
	}
	/*****************************************************************************
	*		void cargarPerfiles
	*****************************************************************************/
	public void cargarPerfiles(Vector evTmp) {
		Vector 		lvRegistro = null;
		int 		liNumRegs = 0;
		CPerfil 	loPerfil = null;
		String 		lsCvePerfil = "";
		String 		lsBloqueado = "";
		int	 		liClaveOID = 0;
		int	 		liTipoAfiliado = 0;

		liNumRegs = evTmp.size() ;
		lvRegistro = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\tCLPerfil::cargarPerfiles(E)");

		for (int liPos = 0; liPos < liNumRegs; liPos++ ) {
			lvRegistro = (Vector) evTmp.elementAt(liPos);

			lsCvePerfil = (String) lvRegistro.elementAt(0) ;
			lsBloqueado = (String) lvRegistro.elementAt(1) ;
			liClaveOID = ((Integer) lvRegistro.elementAt(2)).intValue() ;
			liTipoAfiliado = ((Integer) lvRegistro.elementAt(3)).intValue() ;

                        if (CConst.TRACE)
	  	                System.out.println("CvePerfil{" + lsCvePerfil + "]");

			loPerfil = new CPerfil();

			loPerfil.setCvePerfil(lsCvePerfil);
			loPerfil.setBloqueado(lsBloqueado);
			loPerfil.setClaveOID(liClaveOID);
			loPerfil.setTipoAfiliado(liTipoAfiliado);

			m_htPerfiles.put(lsCvePerfil, loPerfil);
		}
	}
	/*****************************************************************************
	*		void cargarPerfilMem
	*****************************************************************************/
	public void cargarPerfilMem(CPerfil eoPerfil)
				   throws NullPointerException,ArrayIndexOutOfBoundsException
	{
		m_htPerfiles.put(eoPerfil.getCvePerfil(), eoPerfil);
	}
	/*****************************************************************************
	*		void cargarRefPerfilesProt
	*****************************************************************************/
	public void cargarRefPerfilesProt(Vector evTmp,	CLSistema elsLSistemas)
					throws NullPointerException
	{
		int liNumRegs = 0;
		CRefPerfilProt	loPerfilProtRef = null;
		CPerfilProt	loppPerfilProt = null;
		Vector lvRegistro = null;
		String lsCvePerfil = "";
		String lsCveSistema = "";
		String lsCvePerfilProt = "";

		liNumRegs = evTmp.size();

                if (CConst.TRACE)
	        	System.out.println("\tCLUsuario::cargarRefPerfilesProt");

		for (int liPos = 0; liPos < liNumRegs; liPos++){
			lvRegistro = (Vector) evTmp.elementAt(liPos);

			lsCvePerfil = (String) lvRegistro.elementAt(0);
			lsCveSistema = (String) lvRegistro.elementAt(1);
			lsCvePerfilProt = (String) lvRegistro.elementAt(2);

			loppPerfilProt = elsLSistemas.getPerfilProt( lsCveSistema, lsCvePerfilProt);

			if (loppPerfilProt != null){
                                if (CConst.TRACE)
			        	System.out.println("\tCLPerfil::cargarRefPerfilesProt(" + lsCvePerfilProt + "-" + lsCvePerfil +
													") - Asociado");

				loPerfilProtRef = new CRefPerfilProt( 	lsCvePerfil, 		lsCveSistema,
																	lsCvePerfilProt,	loppPerfilProt);

				( (CPerfil) m_htPerfiles.get(lsCvePerfil) ).agregarRefPerfilPerfPr( loPerfilProtRef );

			}else{
                                if (CConst.TRACE)
			        	System.out.println("\tCLUsuario::cargarRefPerfilesProt(" + lsCvePerfilProt + "-" + lsCvePerfil +
													") - No Asociado");
			}

		}
	}


	/*****************************************************************************
	*		Enumeration getClavesPerfiles
	*		Regresa todas las claves de Perfiles contenidas en la clase.
	*****************************************************************************/
	public Enumeration getClavesPerfiles()
	{
		Enumeration loeClavesPerfiles = m_htPerfiles.keys();
		return loeClavesPerfiles;
	}

	/*****************************************************************************
	*		CPerfil getPerfil
	*****************************************************************************/
	public CPerfil getPerfil(	String esCvePerfil)
						  throws NullPointerException
	{
		CPerfil loPerfil = null;

                if (CConst.TRACE)
	        	System.out.println("\tCLPerfil::getPerfil()");

		loPerfil = (CPerfil) m_htPerfiles.get(esCvePerfil);

		return loPerfil;
	}
	/*****************************************************************************
	*		String getSQLCargaPerfiles()
	*****************************************************************************/
	public String getSQLCargaPerfiles()
	{
                if (CConst.TRACE)
	        	System.out.println("\t CLPerfil:" + m_sSQLCargaPerfiles);
		return m_sSQLCargaPerfiles;
	}
	/*****************************************************************************
	*		String getSQLCargaPerfilPerfPr()
	*****************************************************************************/
	public String getSQLCargaPerfilPerfPr()
	{
                if (CConst.TRACE)
	        	System.out.println("\t CLPerfil:" + m_sSQLCargaPerfilPerfPr);
		return m_sSQLCargaPerfilPerfPr;
	}
	
	/*****************************************************************************
	*		void modificarPerfilMem()
	*****************************************************************************/
	public void modificarPerfilMem(String esCvePerfil, String esBloqueo,int eiCveOID, int eiTipoAfiliado)
	{
		CPerfil loPerfil = null;

                if (CConst.TRACE)
	        	System.out.println("\tCLPerfil::bloquearPerfilMem()" );

		loPerfil = (CPerfil) m_htPerfiles.get(esCvePerfil);

		loPerfil.setBloqueado(esBloqueo);
		loPerfil.setClaveOID(eiCveOID);
		loPerfil.setTipoAfiliado(eiTipoAfiliado);
	}
}