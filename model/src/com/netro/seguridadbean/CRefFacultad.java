package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CRefFacultad.java
*
*	Proposito:	Clase CRefFacultad
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

/****************************************************************************
*
*	clase CRefFacultad
*
*****************************************************************************/
public class CRefFacultad {

	CFacultad m_facFacultad;
	String 	m_sCveFacultad;
	long 	m_lNRestriccion;
	String 	m_sHoraInicial;
	String 	m_sHoraFinal;
	String 	m_sFchVencimiento;
	String 	m_sMancomunidad;


	/*****************************************************************************
	*		Constructor_1
	*****************************************************************************/
	public CRefFacultad()
	{
                if (CConst.TRACE)
	        	System.out.println("CRefFacultad::Constructor_1");

		m_sCveFacultad = "";
		m_lNRestriccion = 0;
		m_sHoraInicial = "";
		m_sHoraFinal = "";
		m_sFchVencimiento = "";
		m_sMancomunidad = "";
	}
	/*****************************************************************************
	*		Constructor_2
	*****************************************************************************/
	public CRefFacultad(	String esCveFacultad,	long elRestriccion, 		String esHoraInicial,
						String esHoraFinal,		String esFchVencimiento,String esMancomunidad,
						CFacultad efacFacultad)
	{
                if (CConst.TRACE){
	        	System.out.println("CRefFacultad::Constructor_2");
		        System.out.println("CRefFacultad::Referencia a Facaultad[" + efacFacultad.getCveFacultad() + "]");
                }
		m_facFacultad = efacFacultad;
		m_sCveFacultad = esCveFacultad ;
		m_lNRestriccion = elRestriccion ;
		m_sHoraInicial = esHoraInicial;
		m_sHoraFinal = esHoraFinal;
		m_sFchVencimiento = esFchVencimiento;
		m_sMancomunidad = esMancomunidad;

	}
	/*****************************************************************************
	*		String getCveFacultad
	*****************************************************************************/
	public String getCveFacultad()
	{
		return m_sCveFacultad;
	}
	/*****************************************************************************
	*		CFacultad getFacultad()
	*****************************************************************************/
	public CFacultad getFacultad()
	{
		return m_facFacultad;
	}
	/*****************************************************************************
	*		String getFchVencimiento
	*****************************************************************************/
	public String getFchVencimiento()
	{
		return m_sFchVencimiento;
	}
	/*****************************************************************************
	*		String getHoraFinal
	*****************************************************************************/
	public String getHoraFinal()
	{
		return m_sHoraFinal;
	}
	/*****************************************************************************
	*		String getHoraInicial
	*****************************************************************************/
	public String getHoraInicial()
	{
		return m_sHoraInicial;
	}
	/*****************************************************************************
	*		String getMancomunidad
	*****************************************************************************/
	public String getMancomunidad()
	{
		return m_sMancomunidad;
	}
	/*****************************************************************************
	*		long getRestriccion
	*****************************************************************************/
	public long getRestriccion()
	{
		return m_lNRestriccion;
	}
	/*****************************************************************************
	*		void setCveFacultad
	*****************************************************************************/
	public void setCveFacultad(String esCveFacultad)
	{
		m_sCveFacultad = esCveFacultad;
	}
	/*****************************************************************************
	*		void	setFchVencimiento
	*****************************************************************************/
	public void	setFchVencimiento(String esFchVencimiento)
	{
		m_sFchVencimiento = esFchVencimiento;
	}
	/*****************************************************************************
	*		void	setHoraFinal
	*****************************************************************************/
	public void	setHoraFinal(String esHoraFinal)
	{
		m_sHoraFinal = esHoraFinal;
	}
	/*****************************************************************************
	*		void	setHoraInicial
	*****************************************************************************/
	public void	setHoraInicial(String esHoraInicial)
	{
		m_sHoraInicial = esHoraInicial;
	}
	/*****************************************************************************
	*		void	setMancomunidad
	*****************************************************************************/
	public void	setMancomunidad(String esMancomunidad)
	{
		m_sMancomunidad = esMancomunidad;
	}
	/*****************************************************************************
	*		void	setRestriccion
	*****************************************************************************/
	public void	setRestriccion(long elRestriccion)
	{
		m_lNRestriccion = elRestriccion;
	}
	/*****************************************************************************
	*		int validarRestriccion()
	*****************************************************************************/
  	public int validarRestriccion()
	{
                if (CConst.TRACE)
	        	System.out.println("\tCRefFacultad::validarRestriccion(E)");

		int liResult = CConst.ERRC_OK;
		if (m_lNRestriccion > 0){
			try{
				String lsPatronFch = "yyyy-MM-dd";
				SimpleDateFormat formatoFch =  new SimpleDateFormat(lsPatronFch);
				String lsPatronHr = "hh:mm:ss";
				SimpleDateFormat formatoHr =  new SimpleDateFormat(lsPatronHr);
				Date ldFechaActual = new Date();
				int liComp = 0;

				if (m_sFchVencimiento.length() > 0){
	  		 		Date ldFchVencimiento = formatoFch.parse(m_sFchVencimiento);
					liComp = ldFechaActual.compareTo(ldFchVencimiento);

					if (liComp < 0){
                                                if (CConst.TRACE)
				 	        	System.out.println("\t\tCFacultad::fecha No Vencida[" + liComp + "]");
					}else{
						liResult = CConst.SEG_ERRC_FEC_VENC;
                                                if (CConst.TRACE)
		 			        	System.out.println("\t\tCFacultad::fecha Vencida[" + liComp + "]");
					}
				}
				if (liResult == CConst.ERRC_OK)
				{
					Date ldHoraActual = new Date();
					CHora lohrHoraActual = new CHora();
					CHora lohrHoraInicial = new CHora();
					CHora lohrHoraFinal = new CHora();

					lohrHoraActual.setCadHora(formatoHr.format(new Date()));
					lohrHoraInicial.setCadHora(m_sHoraInicial);
					lohrHoraFinal.setCadHora(m_sHoraFinal);

					if (lohrHoraActual.compararHora(lohrHoraInicial) > 0){
                                                if (CConst.TRACE)
					        	System.out.println("\tBien Hora Inicial");
						if (lohrHoraActual.compararHora(lohrHoraFinal) < 0){
                                                        if (CConst.TRACE)
						        	System.out.println("\tBien Hora Final");
						}else{
                                                        if (CConst.TRACE)
						        	System.out.println("\tHora Final Invalida");
							liResult = CConst.SEG_ERRC_FUERA_HR;
						}
					}else{
                                                if (CConst.TRACE)
					        	System.out.println("\tHora Inicial Invalida");
						liResult = CConst.SEG_ERRC_FUERA_HR;
					}
				}

			}catch(ParseException e){
				System.out.println("Error: " + e);
			}
		}
                if (CConst.TRACE)
		        System.out.println("\tCRefFacultad::validarRestriccion(S)");
                return liResult;
	}
}