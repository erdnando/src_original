package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CPerfilProt.java
*
*	Proposito:	Clase CPerfilProt
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.sql.SQLException;
import java.util.*;
import java.io.*;

/****************************************************************************
*
*	clase CPerfilProt
*
*****************************************************************************/
public class CPerfilProt {

	Hashtable 	m_htRefFacultades;
	String 	m_sCveSistema;
	String 	m_sCvePerfilProt;
	String 	m_sDescripcion;
	String 	m_sBloqueado;
	int 			m_iCveAsignac;

	/*****************************************************************************
	*		Constructor_1
	*****************************************************************************/
	public CPerfilProt()
	{
                if (CConst.TRACE)
	        	System.out.println("CPerfilProt::Constructor_1");

		m_htRefFacultades = new Hashtable();
		m_sCveSistema = "";
		m_sCvePerfilProt = "";
		m_sDescripcion = "";
		m_sBloqueado = "";
	}
	/*****************************************************************************
	*		Constructor_3
	*****************************************************************************/
	public CPerfilProt(	String esCveSistema, 	String esCvePerfilProt,
						String esDescripcion, 	String esBloqueado     )
	{
                if (CConst.TRACE)
	        	System.out.println("CPerfilProt::Constructor_2");

		m_htRefFacultades = new Hashtable();
		m_sCveSistema = esCveSistema ;
		m_sCvePerfilProt = esCvePerfilProt ;
		m_sDescripcion = esDescripcion ;
		m_sBloqueado = esBloqueado ;
	}
	/*****************************************************************************
	*		Constructor_2
	*****************************************************************************/
	public CPerfilProt(	String esCveSistema, 	String esCvePerfilProt,
						String esDescripcion, 	String esBloqueado,
						int 	 eiCveAsignac)
	{
                if (CConst.TRACE)
	        	System.out.println("CPerfilProt::Constructor_2");

		m_htRefFacultades = new Hashtable();
		m_sCveSistema = esCveSistema ;
		m_sCvePerfilProt = esCvePerfilProt ;
		m_sDescripcion = esDescripcion ;
		m_sBloqueado = esBloqueado ;
		m_iCveAsignac = eiCveAsignac ;
	}
	/*****************************************************************************
	*		 void	agregarPerfilProtInsertDB()
	*****************************************************************************/
	public void agregarPerfilProtInsertDB()
					throws Exception,		NullPointerException, 	SQLException,
					 		 SeguException
	{
		CConexionDBPool	loConexion = new CConexionDBPool();
	  int 					liCodigoError = 0;
	  String      		lsSQL = "";
	  boolean 				lbExito = true;

		lsSQL = "INSERT INTO SEG_PERFILPROT(c_sistema, c_perfil_prot, d_descripcion," +
				  " b_bloqueado, t_checksum, sc_asignacion ) VALUES('" + m_sCveSistema + "','" +
				  m_sCvePerfilProt + "','" + m_sDescripcion + "','" + m_sBloqueado
				  + "','0'," + m_iCveAsignac +" )";

                if (CConst.TRACE)
	        	System.out.println("\nCPerfilProt::agregarPerfilProtInsertDB()");
		try{
	  	loConexion.conectarDB();
		 loConexion.ejecutarInsertDB(lsSQL);
		} catch(SQLException ex){
			  lbExito = false;
			  liCodigoError = ex.getErrorCode();
	 		  if(liCodigoError == CConst.ERRC_REGIST_DUPLICADO)
			     throw new SeguException(CConst.SEG_ERRC_SIS_YA_EXIST);
			  if(liCodigoError == CConst.ERRC_INTEGR_REFERENC)
				  throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			  System.out.println("query :" + lsSQL );
			  System.out.println("El codigo de error es:" + liCodigoError + ex.getMessage());
	  } finally{
			  loConexion.cerrarDB(lbExito );
		}
	}
	/*****************************************************************************
	*		void agregarRefFacultadPP()
	*****************************************************************************/
	public void agregarRefFacultadPP(CRefFacultad eRefFacultad)
					throws NullPointerException
	{
//		m_htRefFacultades.put(eRefFacultad.getCveFacultad(), eRefFacultad);
		m_htRefFacultades.put(new Integer(m_htRefFacultades.size() + 1), eRefFacultad);
	}
	/*****************************************************************************
	*		 void	asociarFacPerfilProtDeleteDB()
	*****************************************************************************/
 	public void asociarFacPerfilProtDeleteDB(String 	esCveSistema, 	String 	esCvePerfilProt,
														  CConexionDBPool 	eoConexionDBPool)
					throws Exception,		NullPointerException, 	SQLException,	SeguException
	{
		int		liNumRegBorrados;
		int 		liCodigoError;
	  String 	lsSQL;
		liNumRegBorrados = 0;
		liCodigoError = 0;

		lsSQL = "DELETE FROM SEG_FACPERFPR WHERE c_sistema = '" + esCveSistema + "'" +
				  " AND c_perfil_prot = '" + esCvePerfilProt + "'";

                if (CConst.TRACE)
	        	System.out.println("\nCPerfilProt::asociarFacPerfilProtDeleteDB()");

		liNumRegBorrados = eoConexionDBPool.ejecutarUpdateDB(lsSQL);

                if (CConst.TRACE)
			System.out.println("El numero de registros borrados fue:" + liNumRegBorrados);
	}
	/*****************************************************************************
	*		 void	asociarFacPerfilProtInsertDB()
	*****************************************************************************/
	public void asociarFacPerfilProtInsertDB(String	esCveSistema,	String 	esCvePerfilProt,
   													  String esCveFacultad, Integer	loiNumRestriccion,
														  String esMancomunidad,
														  CConexionDBPool eoConexionDBPool)
					throws Exception,		NullPointerException, 	SQLException,	SeguException
	{

	  int 					liCodigoError = 0;
	  String      		lsSQL = "";
		int 					liNumRestriccion ;

		liNumRestriccion = loiNumRestriccion.intValue();

                if (CConst.TRACE)
	        	System.out.println("\nCPerfilProt::asociarFacPerfilProtInsertDB()");

		if(liNumRestriccion == 0){
			lsSQL = "INSERT INTO SEG_FACPERFPR(c_sistema, c_perfil_prot, c_facultad," +
			"b_mancomunidad) VALUES('" + esCveSistema + "','" + esCvePerfilProt +
			"','" + esCveFacultad+ "','" + esMancomunidad +"')";
		} else{
			lsSQL = "INSERT INTO SEG_FACPERFPR(c_sistema, c_perfil_prot, c_facultad," +
			"n_restriccion, b_mancomunidad) VALUES('" + esCveSistema + "','" +
			esCvePerfilProt + "','" + esCveFacultad+ "','" + loiNumRestriccion
			+ "','" + esMancomunidad +"')";
		}

  		eoConexionDBPool.ejecutarInsertDB(lsSQL);
	}
	/*****************************************************************************
   *		 void bloquearPerfilProtUpdateDB()
   *****************************************************************************/
	public void bloquearPerfilProtUpdateDB(	String 	esCveSistema,	String esCvePerfilProt,
															String 	esBloqueado)
			   throws Exception, NullPointerException, 	SQLException, SeguException
   {
		CConexionDBPool	loConexion = new CConexionDBPool();
	  int 					liCodigoError;
	  int 					liNumRegActualizados;
	  boolean				lbExito = true;
	  liNumRegActualizados = 0;
	  String lsSQL;
	  lsSQL= "UPDATE SEG_PERFILPROT SET b_bloqueado = '" + esBloqueado +
				 "' WHERE c_sistema ='" + esCveSistema + "' AND c_perfil_prot = '" +
				 esCvePerfilProt + "'";

        if (CConst.TRACE)
                System.out.println("\nCPerfilProt::bloquearPerfilProtUpdateDB()");

	  try{
	  	loConexion.conectarDB();
		 liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if(liNumRegActualizados == 0)
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
		} catch(SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
	        	 System.out.println("Error en el update, el c�digo es : (" + liCodigoError +")");
			System.out.println("\n" + ex.getMessage());
	  } finally{
			loConexion.cerrarDB(lbExito);
		}
	}
	/*****************************************************************************
   *      void borrarPerfilProtDeleteDB()
   *****************************************************************************/
	public void borrarPerfilProtDeleteDB(String esCveSistema, String esCvePerfilProt)
					throws	Exception,		NullPointerException,	SQLException,
								SeguException
   {
		int		liNumRegBorrados;
		int	 	liCodigoError;
		boolean	lbExito = true;
		liNumRegBorrados = 0;
		liCodigoError =0;

	  CConexionDBPool loConexion = new CConexionDBPool();
	  String lsSQL;

	  lsSQL = "DELETE FROM SEG_PERFILPROT WHERE c_sistema ='"+ esCveSistema+ "'"+
				  " AND c_perfil_prot ='" + esCvePerfilProt + "'";

        if (CConst.TRACE)
                System.out.println("\nCPerfilProt::borrarPerfilProtDeleteDB()");

	  try{
		loConexion.conectarDB();
		 liNumRegBorrados = loConexion.ejecutarUpdateDB(lsSQL);
			System.out.println("El numero de registros borrados fue:" + liNumRegBorrados);
	  	if(liNumRegBorrados == 0)
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ABORR_INEXIST);
		} catch(SQLException ex){
		 lbExito = false;
			liCodigoError = ex.getErrorCode();
			if(liCodigoError == CConst.ERRC_INTEGR_REFERENC)
				throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			System.out.println("El codigo de error es:" + liCodigoError );
		} finally{
			  loConexion.cerrarDB(lbExito);
		}
   }
	/*****************************************************************************
	*		void borrarRefFacultades()
	*****************************************************************************/
	public void borrarRefFacultades()
	{
                if (CConst.TRACE)
	        	System.out.println("CPerfilProt::borrarRefFacultades");

		if (m_htRefFacultades.size() > 0){
	 		for (Enumeration e = m_htRefFacultades.keys() ; e.hasMoreElements() ;) {
		 	m_htRefFacultades.remove(e.nextElement());
	   	}
		}
                if (CConst.TRACE)
	        	System.out.println("CPerfilProt::saliendo de borrarRefFacultades");
 	}
	/*****************************************************************************
	*		CRefFacultad buscarRefFacultad()
	*****************************************************************************/
	public CRefFacultad buscarRefFacultad(String esCveFacultad)
	{
		CRefFacultad loRefFacultad = null;
		CRefFacultad loRefFacultadRet = null;
		boolean 		lbExiste = false;

                if (CConst.TRACE){
	        	System.out.println("\tCPerfilProt::buscarRefFacultad(E)");
		        System.out.println("\t\tesCveFacultad[" + esCveFacultad + "]");
                }
		if (m_htRefFacultades.size() > 0)
			loRefFacultad = (CRefFacultad) m_htRefFacultades.get(esCveFacultad);
		Iterator loitFacultades =  (Iterator) m_htRefFacultades.elements();

		while(loitFacultades.hasNext() && lbExiste == false)
		{
			loRefFacultad = (CRefFacultad) loitFacultades.next();

                        if (CConst.TRACE)
		        	System.out.println("\t\tFacultad:" + loRefFacultad.getCveFacultad());

			if (esCveFacultad.equals(loRefFacultad.getCveFacultad()))
			{
				lbExiste = true;
				loRefFacultadRet = loRefFacultad;
			}
		}
                if (CConst.TRACE)
	        	System.out.println("\tCPerfilProt::buscarRefFacultad(S)");
		return loRefFacultadRet ;
	}
	/*****************************************************************************
   *      Hashtable consultarFacxPerfilProtSelectDB()
   *****************************************************************************/
	public Hashtable consultarFacxPerfilProtSelectDB(String 	esCveSistema,
																	 String 	esCvePerfilProt)
					  	  throws 	Exception, 	NullPointerException,	SQLException,
						  SeguException
	{
		Hashtable htFacxPerfProt = new Hashtable();

	  String 	lsSQL;
	  Vector 	svTabla;
		Vector 	lvRegistro;
		Vector 	lvHashTable;
		String 	lsCveFacultad;
		String 	lsBandMancomunidad;
		Integer  loiNumRestriccion ;
		int 		liNumCols = 0;
		int 		liNumReg = 0;
		int 		liContador = 0;
		int 		liCodigoError;
		boolean	lbExito = true;

	  svTabla = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\nCPerfilProt::consultarFacxPerfilProtSelectDB()");

		CConexionDBPool loConexion = new CConexionDBPool();
		lsSQL = "SELECT c_facultad, n_restriccion, b_mancomunidad FROM "+
				  "SEG_FACPERFPR WHERE c_sistema ='" + esCveSistema + "' AND c_perfil_prot ='"+
					esCvePerfilProt + "' ORDER BY c_facultad";
		try{
			loConexion.conectarDB();
 			loConexion.ejecutarQueryDB(lsSQL, liNumCols,svTabla);
			if( (svTabla.size()) > 0){

				//HACEMOS LA TRANSFERENCIA HACIA LA HASHTABLE
				for(liContador = 0; liContador < svTabla.size(); liContador ++ ){
					lvRegistro = (Vector) svTabla.elementAt(liContador);
					lsCveFacultad = (String)lvRegistro.elementAt(0);
					loiNumRestriccion = (Integer)lvRegistro.elementAt(1);
					lsBandMancomunidad = (String)lvRegistro.elementAt(2);
					lvHashTable = new Vector();
					lvHashTable.addElement(loiNumRestriccion);
					lvHashTable.addElement(lsBandMancomunidad);
					htFacxPerfProt.put(lsCveFacultad,lvHashTable);
				}
			}
			liNumReg = svTabla.size();
                        if (CConst.TRACE)
		        	System.out.println("Tama�o de la tabla: " + liNumReg);
			if( liNumReg == 0)
				throw new SeguException(CConst.SEG_ERRC_SIS_TABLA_VACIA);
		} catch(SQLException ex){
			lbExito = false;
			liCodigoError= ex.getErrorCode();
			System.out.println("Error en codigo.."+ liCodigoError +"  "+ ex.getMessage());
		} finally{
	 	   loConexion.cerrarDB(lbExito);
		}
		return htFacxPerfProt ;
	}
	/*****************************************************************************
   *      Vector consultarPerfilesProtSelectDB()
   *****************************************************************************/
	public Vector consultarPerfilesProtSelectDB()
					  throws Exception,		NullPointerException, 	SQLException,
								SeguException
	{
		Vector svTabla;

	  String	lsSQL;
		int 		liNumCols = 0;
		int 		liNumReg = 0;
		boolean	lbExito = true;

	  svTabla = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\nCPerfilProt:: consultarPerfilesProtSelectDB()");

		CConexionDBPool loConexion = new CConexionDBPool();

		lsSQL = "SELECT pp.c_sistema, pp.c_perfil_prot, pp.d_descripcion, pp.b_bloqueado, ";
		lsSQL += " pp.sc_asignacion, ca.cg_sw_banco, ca.cg_sw_cliente, ca.cg_sw_central ";
		lsSQL += " FROM seg_perfilprot pp, deccat_asignacion ca ";
		lsSQL += " WHERE pp.sc_asignacion = ca.sc_asignacion ";
		lsSQL += " ORDER BY pp.c_sistema, pp.c_perfil_prot";

		try{
			loConexion.conectarDB();
 			loConexion.ejecutarQueryDB(lsSQL, liNumCols,svTabla);
			if(svTabla.size()==0)
				throw new SeguException(CConst.SEG_ERRC_SIS_TABLA_VACIA);
		} catch(SQLException ex){
			lbExito = false;
			System.out.println("Error en el select"+ ex.getMessage());
		} finally{
			loConexion.cerrarDB(lbExito);
 		}
		return svTabla;
	}
	/*****************************************************************************
	*		String generarXML()
	*****************************************************************************/
	public String generarXML(String esCvePerfil)
	{
		int liValor1 = 0;
		int liValor2 = 0;
		int liValorTmp = 0;
		char lcComilla = '"';
		String lsCadenaXML = "";
		String lsCadFacXML = "";
		CFacultad loFacultad = null;
		//Hashtable lhtFacultadTmp = new Hashtable();
		//Vector	 lvOrdenFac = new Vector();
		Enumeration enumx = m_htRefFacultades.keys();

		if(CConst.TRACE)
			System.out.println("\tCPerfilProt::generarXML()");

		while (enumx.hasMoreElements())
		//for (int i = 1; i <= m_htRefFacultades.size();i++)
		{
			loFacultad = ((CRefFacultad) m_htRefFacultades.get(enumx.nextElement())).getFacultad();
			//loFacultad = ((CRefFacultad) m_htRefFacultades.get(new Integer(i))).getFacultad();

			/*if (!lhtFacultadTmp.containsKey(new Integer(loFacultad.getOrdenMenu())))
			{
				lhtFacultadTmp.put(new Integer(loFacultad.getOrdenMenu()),loFacultad);
			}*/

			lsCadFacXML = loFacultad.generarXML(esCvePerfil, m_sCvePerfilProt);
			lsCadenaXML = lsCadenaXML + lsCadFacXML;
		}
/*
		Enumeration enumTmpFacultades = lhtFacultadTmp.keys();

		while (enumTmpFacultades.hasMoreElements()){
			lvOrdenFac.addElement((Integer) enumTmpFacultades.nextElement());
		}
		int liTamano = lvOrdenFac.size();
		for (int iCont = 0; iCont < liTamano; iCont++)
		{
			liValor1 = ((Integer) lvOrdenFac.get(iCont)).intValue();
			//System.out.println("\t\t Valor1(" + liValor1 + ")");

			for (int iCont_2 = iCont + 1; iCont_2 < liTamano; iCont_2++)
			{
				liValor2 = ((Integer) lvOrdenFac.get(iCont_2)).intValue();
			//	System.out.println("\t\t Valor2(" + liValor2 + ")");
				if (liValor1 > liValor2)
				{
					liValorTmp = ((Integer) lvOrdenFac.get(iCont_2)).intValue();
					lvOrdenFac.remove(iCont_2);
					lvOrdenFac.add(iCont_2,(Integer) lvOrdenFac.get(iCont)) ;
					lvOrdenFac.remove(iCont);
					lvOrdenFac.add(iCont,new Integer(liValorTmp)) ;
					liValor1 = liValorTmp ;
			//		System.out.println("\t\t Cambio(" + liValorTmp + ")");
				}
			}
		}
		for(int k = 0; k < lvOrdenFac.size(); k++)
		{
			loFacultad = (CFacultad) lhtFacultadTmp.get((Integer) lvOrdenFac.get(k));
			lsCadFacXML = loFacultad.generarXML(m_sCvePerfilProt);
			lsCadenaXML = lsCadenaXML + lsCadFacXML;
		}
		System.out.println("\tCPerfilProt[" + lsCadenaXML + "]");
*/
		return lsCadenaXML ;
	}
	/*****************************************************************************
	*		String getBloqueado
	*****************************************************************************/
   public String getBloqueado()
	{
		return m_sBloqueado;
	}
	/*****************************************************************************
	*		String getCvePerfilProt
	*****************************************************************************/
   public String getCvePerfilProt()
	{
		return m_sCvePerfilProt;
	}
	/*****************************************************************************
	*		String getCveSistema
	*****************************************************************************/
   public String getCveSistema()
	{
		return m_sCveSistema;
	}
	/*****************************************************************************
   *	    void modificarPerfilProtUpdateDB()
   *****************************************************************************/
	public void modificarPerfilProtUpdateDB()
					throws Exception,		NullPointerException, 	SQLException,
					 		 SeguException
	{
		CConexionDBPool loConexion = new CConexionDBPool();
	  int liCodigoError;
	  int liNumRegActualizados;
		boolean	lbExito = true;
	  liNumRegActualizados = 0;
	  String lsSQL ;

        if (CConst.TRACE)
		System.out.println("\nCPerfilProt::modificarPerfilProtUpdateDB()");

		lsSQL = "UPDATE SEG_PERFILPROT SET d_descripcion = '" + m_sDescripcion +
					"',b_bloqueado = '" + m_sBloqueado + "', sc_asignacion = " +
					m_iCveAsignac + " WHERE c_sistema ='" + m_sCveSistema +
					"' AND c_perfil_prot ='" + m_sCvePerfilProt + "'";
	  try{
			loConexion.conectarDB();
		 liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if(liNumRegActualizados == 0)
				throw	new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
                if (CConst.TRACE)
	        	 System.out.println("El numero de registros actualizados fue:" + liNumRegActualizados);
	  } catch(SQLException ex){
			lbExito = false;
	  	liCodigoError = ex.getErrorCode();
		 System.out.println("Error en el update, el c�digo es : (" + liCodigoError +")");
	  } finally{
			  loConexion.cerrarDB(lbExito);
		}
	}//FIN modificarPerfilProtUpdateDB()
	/*****************************************************************************
	*		void	setBloqueado
	*****************************************************************************/
   public void	setBloqueado(String esBloqueado)
	{
		m_sBloqueado = esBloqueado;
	}
	/*****************************************************************************
	*		void setCvePerfilProt
	*****************************************************************************/
	public void setCvePerfilProt(String esCvePerfilProt)
	{
		m_sCvePerfilProt = esCvePerfilProt;
	}
	/*****************************************************************************
	*		void	setCveSistema
	*****************************************************************************/
	public void	setCveSistema(String esCveSistema)
	{
		m_sCveSistema = esCveSistema;
	}
	/*****************************************************************************
	*		void	setDescripcion
	*****************************************************************************/
	public void setDescripcion(String esDescripcion)
	{
		m_sDescripcion = esDescripcion;
	}
}