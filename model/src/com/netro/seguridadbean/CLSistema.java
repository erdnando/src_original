package com.netro.seguridadbean;

import java.lang.Long;
import java.util.*;
import java.io.*;

/**
 * Representacion de Sistemas para la generacion del menu
 * @author Pedro A. Garcia Ramirez
 * @author Cindy A. Ramos P�rez (Modificacion)
 * @author Gilberto Aparicio (Modificacion F044-2005)
 *
 */

public class CLSistema {

	Hashtable m_htSistemas;
	String m_sSQLCargaSist;
	String m_sSQLCargaFac;
	String m_sSQLCargaFacPerfProt;
	String m_sSQLCargaPerfProt;

	/**
	 * Constructor predeterminado
	 */
	public CLSistema() {
		if (CConst.TRACE) {
			System.out.println("\t CLSistema:Constructor");
		}

		m_htSistemas = new Hashtable();

		m_sSQLCargaSist = 
				"SELECT c_sistema, d_descripcion, b_bloqueado, " +
				" t_imagen, sg_orden_mnu, " +
				" sg_sw_accion_click, cg_liga,cg_sw_img_desc, " +
				" b_menu, d_descripcion_ing " +
				" FROM seg_sistemas " +
				" ORDER BY  sg_orden_mnu ";

		m_sSQLCargaFac = 
				" SELECT sf.c_sistema, sf.c_facultad, " +
				" sf.d_descripcion, sf.b_bloqueado, " +
				" sf.t_tipo_facultad, sf.n_restriccion, " +
				" sr.f_hora_inicial, sr.f_hora_final, " +
				" sr.f_vencimiento, sf.p_pagina, sf.sg_sw_quita_mnu, " +
				" sf.sg_orden_mnu,sf.cg_imagen,sf.cg_sw_img_desc, " +
				" sf.d_descripcion_ing,  sf.p_pagina_ing, " +
				" sf.cg_clave_revisar " +
				" FROM seg_facultades sf, seg_restricfac sr " +
				" WHERE sf.n_restriccion = sr.n_restriccion(+) " +
				" ORDER BY sf.c_sistema, sf.sg_orden_mnu ";

		m_sSQLCargaPerfProt = 
				" SELECT pp.c_sistema, pp.c_perfil_prot, " +
				" pp.d_descripcion, pp.b_bloqueado " +
				" FROM seg_perfilprot pp, seg_sistemas s " +
				" WHERE s.c_sistema = pp.c_sistema " +
				" ORDER BY s.sg_orden_mnu, pp.c_perfil_prot ";

		m_sSQLCargaFacPerfProt = 
				" SELECT pp.c_sistema, pp.c_perfil_prot, " +
				" fpp.c_facultad, fpp.n_restriccion, " +
				" rf.f_hora_inicial, rf.f_hora_final, " +
				" rf.f_vencimiento, fpp.b_mancomunidad " +
				" FROM seg_perfilprot pp, seg_facperfpr fpp, " +
				" seg_restricfac rf, seg_sistemas s, seg_facultades f " +
				" WHERE pp.c_sistema = fpp.c_sistema AND " +
				" pp.c_perfil_prot = fpp.c_perfil_prot AND " +
				" fpp.n_restriccion = rf.n_restriccion(+) " +
				" AND s.c_sistema = pp.c_sistema " +
				" AND f.c_sistema = fpp.c_sistema " +
				" AND f.c_facultad = fpp.c_facultad " +
				" ORDER BY s.sg_orden_mnu,  pp.c_perfil_prot, f.sg_orden_mnu";
	}


	/**
	 * Cambia el estatus de bloqueo del sistema en memoria
	 * @param esCveSistema Clave del sistema
	 * @param esBloqueo Indicador de bloqueo
	 *
	 */
	public void bloquearSistemaMem(String esCveSistema, String esBloqueo) {
		CSistema loSistema = null;
		if (CConst.TRACE) {
			System.out.println("\tCLSistema::bloquearSistema()" );
		}

		loSistema = (CSistema) m_htSistemas.get(esCveSistema);
		loSistema.setBloqueado(esBloqueo);
	}
	
	/*****************************************************************************
	*		void borrarSistemaMem()
	*****************************************************************************/
	public void borrarSistemaMem(String esCveSistema)
	{
                if (CConst.TRACE)
	         	System.out.println("\tCLSistema::borrarSistemaMem()" );

		m_htSistemas.remove(esCveSistema) ;
	}
	/*****************************************************************************
	*		CSistema buscarSistema()
	*****************************************************************************/
	public CSistema buscarSistema(String esCveSistema)
	{
                if (CConst.TRACE)
	         	System.out.println("\tCLSistema::buscarSistema()" );

		return ( (CSistema) m_htSistemas.get(esCveSistema) );
	}
	/*****************************************************************************
	*		void cambiarDato()
	*****************************************************************************/
	public void cambiarDato(String ssCveSistema, String ssCvePerfil, String ssBloqueado)
	{
		( (CSistema) m_htSistemas.get(ssCveSistema) ).cambiarDato(ssCvePerfil, ssBloqueado);
	}
	
	
	/**
	 * Carga las facultades en memoria.
	 * @param evTmp Vector con los registros de las facultades de la BD.
	 *
	 */
	public void cargarFacultades(Vector evTmp) {
		int liNumRegs;
		CFacultad loFacultad;
		Vector lvRegistro ;
		String lsCveSistema = "";
		String lsCveFacultad = "";
		String lsDescripcion = "";
		String lsDescripcionIngles = "";
		String lsBloqueado = "";
		String lsTipoFac = "";
		long llNRestriccion = 0;
		String lsHoraIni = "";
		String lsHoraFin = "";
		String lsHoraIniTmp = "";
		String lsHoraFinTmp = "";
		String lsFchVencimiento = "";
		String lsFchVencTmp = "";
		String lsPagina = "";
		String lsPaginaIngles = "";
		int		liQuitaMnu = 0;
		int		liOrdenMnu = 0;
		String lsImagen = "";
		String lsSwImgDesc = "";
		String lsClaveRevisar = "";

		liNumRegs = evTmp.size();
		lvRegistro = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\tCLSistema::cargarFacultades");

		for (int liPos = 0; liPos < liNumRegs; liPos++){
			lvRegistro = (Vector) evTmp.elementAt(liPos);

			lsCveSistema = (String) lvRegistro.elementAt(0);
			lsCveFacultad = (String) lvRegistro.elementAt(1);
			lsDescripcion = (String) lvRegistro.elementAt(2);
			lsDescripcionIngles = (String) lvRegistro.elementAt(14);
			lsBloqueado = (String) lvRegistro.elementAt(3);
			lsTipoFac = (String) lvRegistro.elementAt(4);
			llNRestriccion = ((Integer)lvRegistro.elementAt(5)).longValue();
			lsHoraIniTmp = (String) lvRegistro.elementAt(6);
			lsHoraFinTmp = (String) lvRegistro.elementAt(7);

			if (lsHoraIniTmp.length() > 0 && lsHoraFinTmp.length() > 0)
			{
				lsHoraIni = lsHoraIniTmp.substring(lsHoraIniTmp.indexOf(" ") + 1, lsHoraIniTmp.length() );
				lsHoraFin = lsHoraFinTmp.substring(lsHoraFinTmp.indexOf(" ") + 1, lsHoraFinTmp.length() );
			} else{
				lsHoraIni = lsHoraIniTmp ;
				lsHoraFin = lsHoraFinTmp ;
			}

			lsFchVencTmp = (String) lvRegistro.elementAt(8);

			if (lsFchVencTmp.length() > 0){
				lsFchVencimiento = lsFchVencTmp.substring(0, lsFchVencTmp.indexOf(" ") );
			}
			else{

				lsFchVencimiento = lsFchVencTmp ;
			}
			lsPagina = (String) lvRegistro.elementAt(9);
			lsPaginaIngles = (String) lvRegistro.elementAt(15);
			liQuitaMnu = ((Integer) lvRegistro.elementAt(10)).intValue();
			liOrdenMnu = ((Integer) lvRegistro.elementAt(11)).intValue();
			lsImagen = (String) lvRegistro.elementAt(12);
			lsSwImgDesc = (String) lvRegistro.elementAt(13);
			lsClaveRevisar = (String) lvRegistro.elementAt(16);

			loFacultad = new CFacultad(	lsCveSistema,		lsCveFacultad,
										lsDescripcion,		lsDescripcionIngles,
										lsBloqueado,
										lsTipoFac,			llNRestriccion,
										lsHoraIni,			lsHoraFin,
										lsFchVencimiento,	lsPagina,
										lsPaginaIngles,
										liQuitaMnu,			liOrdenMnu,
										lsImagen,			lsSwImgDesc,
										lsClaveRevisar);

			((CSistema) m_htSistemas.get(lsCveSistema)).agregarFacultad( loFacultad );
		}
	}
	
	/**
	 * Carga los perfiles prototipos a memoria
	 *
	 */
	public void cargarPerfilesProt(Vector evTmpFacPerfP, Vector evTmpPerfP) {
		int liNumRegs = 0;
		int liNumRegs2 = 0;
		CPerfilProt loPerfilProt = null;
		CRefFacultad loRefFacultad = null;
		CSistema loSistema = null;
		CFacultad loFacultad = null;
		Vector lvRegistro = null;
		String lsCveSistemaAnt = "";
		String lsCvePerfilProtAnt = "";
		String lsCveSistema = "";
		String lsCvePerfilProt = "";
		String lsDescripcion = "";
		String lsBloqueado = "";
		String lsCveFacultad = "";
		long   llNRestriccion = 0;
		String lsHoraInicial = "";
		String lsHoraFinal = "";
		String lsFchVencimiento = "";
		String lsMancomunidad = "";

		liNumRegs = evTmpFacPerfP.size();
		liNumRegs2 = evTmpPerfP.size();
		lvRegistro = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\tCLSistema::cargarPerfilesProt");

		for (int liPos = 0; liPos < liNumRegs2; liPos++){
			lvRegistro = (Vector) evTmpPerfP.elementAt(liPos);

			lsCveSistema = (String) lvRegistro.elementAt(0);
			lsCvePerfilProt = (String) lvRegistro.elementAt(1);
			lsDescripcion = (String) lvRegistro.elementAt(2);
			lsBloqueado = (String) lvRegistro.elementAt(3);

			loSistema = ( (CSistema) m_htSistemas.get(lsCveSistema) ) ;

			loPerfilProt = new CPerfilProt(	lsCveSistema,	lsCvePerfilProt,
														lsDescripcion,	lsBloqueado);
			loSistema.agregarPerfilProt( loPerfilProt );
		}

		for (int liPos = 0; liPos < liNumRegs; liPos++){
			lvRegistro = (Vector) evTmpFacPerfP.elementAt(liPos);

			lsCveSistema = (String) lvRegistro.elementAt(0);
			lsCvePerfilProt = (String) lvRegistro.elementAt(1);
			lsCveFacultad = (String) lvRegistro.elementAt(2);
			llNRestriccion = ((Integer)lvRegistro.elementAt(3)).longValue();
			lsHoraInicial = (String) lvRegistro.elementAt(4);
			lsHoraFinal = (String) lvRegistro.elementAt(5);
			lsFchVencimiento = (String) lvRegistro.elementAt(6);
			lsMancomunidad = (String) lvRegistro.elementAt(7);

			loSistema = ( (CSistema) m_htSistemas.get(lsCveSistema) ) ;

			loFacultad = loSistema.buscarFacultad(lsCveFacultad);

			loRefFacultad = new CRefFacultad(lsCveFacultad, 	llNRestriccion, 	lsHoraInicial,
														lsHoraFinal, 		lsFchVencimiento, lsMancomunidad,
														loFacultad);

			( (CPerfilProt) loSistema.buscarPerfilProt(lsCvePerfilProt) ).agregarRefFacultadPP( loRefFacultad );
		}
	}
	/*****************************************************************************
	*		void cargarSistemaMem
	*****************************************************************************/
	public void cargarSistemaMem(CSistema eoSistema)
				   throws NullPointerException,ArrayIndexOutOfBoundsException
	{
		m_htSistemas.put(eoSistema.getCveSistema(), eoSistema);
	}


	/**
	 * Carga los sistemas registrados en la BD en memoria
	 * @param evTmp Vector con los registros de sistemas
	 *
	 */
	public void cargarSistemas(Vector evTmp) {
		int liNumRegs = evTmp.size();
		Vector lvRegistro = new Vector();

		if (CConst.TRACE) {
			System.out.println("\tCLSistema::cargarSismteas");
		}
		for (int liPos = 0; liPos < liNumRegs; liPos++ ) {
			lvRegistro = (Vector) evTmp.elementAt(liPos);

			String lsCveSistema = (String) lvRegistro.elementAt(0) ;
			String lsDescripcion = (String) lvRegistro.elementAt(1) ;
			String lsBloqueado = (String) lvRegistro.elementAt(2) ;
			String lsImagen = (String) lvRegistro.elementAt(3) ;
			int liOrdenMenu = ((Integer) lvRegistro.elementAt(4)).intValue() ;
			int liSwAccionClick = ((Integer) lvRegistro.elementAt(5)).intValue() ;
			String lsLiga = (String) lvRegistro.elementAt(6) ;
			String lsSwImagen = (String) lvRegistro.elementAt(7) ;
			String lsQuitaMnu = (String) lvRegistro.elementAt(8) ;
			String lsDescripcionIngles = (String) lvRegistro.elementAt(9) ;

			if (CConst.TRACE) {
				System.out.println("CveSistema{" + lsCveSistema + "]");
			}

			CSistema loSistema = new CSistema(lsCveSistema,
					lsDescripcion, lsDescripcionIngles,
					lsBloqueado, lsImagen,
					liOrdenMenu, lsSwImagen,
					lsLiga, liSwAccionClick,
					lsQuitaMnu);

			m_htSistemas.put(lsCveSistema, loSistema);
		}
	}
	
	
	/*****************************************************************************
	*		CFacultad getFacultad
	*****************************************************************************/
	public CFacultad getFacultad(	String esCveSistema,		String esCveFacultad)
						  throws NullPointerException
	{
		CSistema losiSistema = null;
		CFacultad loFacultad = null;

                if (CConst.TRACE)
	        	System.out.println("\tCLSistema::getFacultad()");

		losiSistema = (CSistema) m_htSistemas.get(esCveSistema);

		loFacultad = losiSistema.getFacultad(	esCveFacultad );

		return loFacultad;
	}
	/*****************************************************************************
	*		CPerfilProt getPerfilProt
	*****************************************************************************/
	public CPerfilProt getPerfilProt(	String esCveSistema,		String esCvePerfilProt)
						  	 throws NullPointerException
	{
		CSistema losiSistema = null;
		CPerfilProt sppPerfilProt = null;

                if (CConst.TRACE)
	        	System.out.println("\tCLSistema::getPerfilProt()");

		losiSistema = (CSistema) m_htSistemas.get(esCveSistema);

		sppPerfilProt = losiSistema.getPerfilProt(	esCvePerfilProt );

		if (sppPerfilProt != null)
                        if (CConst.TRACE)
		        	System.out.println("\tCLSistema::getPerfilProt(" + sppPerfilProt.getCvePerfilProt() + ")");

		return sppPerfilProt;
	}
	/*****************************************************************************
	*		void getSistema
	*****************************************************************************/
	public void getSistema(	String esCveSistema,	CSistema	ssiSistema)
	{
                if (CConst.TRACE)
	        	System.out.println("\tCLSistema::getSistema()");
		ssiSistema = (CSistema) m_htSistemas.get(esCveSistema);

	}
	/*****************************************************************************
	*		Enumeration getSistemas
	*****************************************************************************/
	public Enumeration getSistemas()
	{
                if (CConst.TRACE)
	        	System.out.println("\tCLSistema::getSistemas()");

		Enumeration enum1 = m_htSistemas.keys();
		return enum1;
	}
	/*****************************************************************************
	*		String getSQLCargaFac()
	*****************************************************************************/
	public String getSQLCargaFac()
	{
                if (CConst.TRACE)
	        	System.out.println("\t CLSistema:" + m_sSQLCargaFac);
		return m_sSQLCargaFac;
	}
	/*****************************************************************************
	*		String getSQLCargaFacPerfProt()
	*****************************************************************************/
	public String getSQLCargaFacPerfProt()
	{
                if (CConst.TRACE)
	        	System.out.println("\t CLSistema:" + m_sSQLCargaFacPerfProt);
		return m_sSQLCargaFacPerfProt;
	}
	/*****************************************************************************
	*		String getSQLCargaPerfProt()
	*****************************************************************************/
	public String getSQLCargaPerfProt()
	{
                if (CConst.TRACE)
	        	System.out.println("\t CLSistema:" + m_sSQLCargaPerfProt);
		return m_sSQLCargaPerfProt;
	}


	/**
	 * Obtiene el query necesario para traer la informaci�n de los sistemas
	 * que hay registrados en la aplicacion.
	 * @return Cadena con el query
	 */
	public String getSQLCargaSistemas() {
		return m_sSQLCargaSist;
	}
	
	public int size() {
		return m_htSistemas.size();
	}
	
	
}