package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CUsuario.java
*
*	Proposito:	Clase CUsuario
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/
/**************************************************************************
 *
 * Modificado por: Cindy A. Ramos P�rez
 *
 * Fecha Modificaci�n: 26/03/2002
 *
 *  Modificado para que soporte multinivel en la relaci�n de sistemas.
 *
***************************************************************************/
/**************************************************************************
 *
 * Modificado por: Cindy A. Ramos P�rez
 *
 * Fecha Modificaci�n: 07/10/2002
 *
 *  Modificado para validar primer acceso al sistema y vigencia en contrase�a.
 *
***************************************************************************/
/**************************************************************************
 *
 * Modificado por: Cindy A. Ramos P�rez
 *
 * Fecha Modificaci�n: 27/12/2002
 *
 *  Modificado para que no mande el sistema de Financiamiento a Inventarios
 * 		en el XML
***************************************************************************/
/**************************************************************************
 *
 * Modificado por: Cindy A. Ramos P�rez
 *
 * Fecha Modificaci�n: 13/03/2003
 *
 *  Modificado para que no mande el sistema de L�nea de Cr�dito del Modulo de
 * 		Financiamiento a Distribuidores en el XML
***************************************************************************/


import java.security.*;
import java.lang.String;
import java.text.*;
import java.util.*;
import java.io.*;
import java.sql.SQLException;
import netropology.utilerias.*;

import java.sql.*;

/****************************************************************************
*
*	clase CUsuario
*
*****************************************************************************/
public class CUsuario {

		Hashtable	m_htFacultadesExt;
		Hashtable	m_htRefPerfiles;

	  	String 	m_sCveEmpresa;
	  	String 	m_sCveUsuario;
	  	String 	m_sCveUsuarioNafin;
	  	String 	m_sNombre;
	  	String 	m_sContrasena;
	  	String 	m_sStatus;
	  	int 		m_iNumIntentos;
	  	int 		m_iCveEPO;
	  	int 		m_iCvePYME;
	  	int 		m_iCveIF;
	  	String 	m_sFchVencimiento;

		String	m_sAPaterno;
		String	m_sAMaterno;
		String	m_sCorreo;
		String	m_sFecUltCambio;

		String	m_sIP;
		String	m_sHost;

		int		m_iTpUsuario;
		String	m_sTipoAfiliacion;

		String 	m_sAtribEmpresa;

		String  m_sSIRAC;

	/*****************************************************************************
	*		Constructor_1
	*****************************************************************************/
	public CUsuario()
	{
                if (CConst.TRACE)
	        	System.out.println("CUsuario::Constructor_1");

		m_htFacultadesExt = new Hashtable();
		m_htRefPerfiles = new Hashtable();

		m_sCveUsuario = "";
		m_sCveUsuarioNafin = "";
		m_sNombre = "";
		m_sContrasena = "";
		m_sCveEmpresa = "";
		m_sFchVencimiento = "";
		m_sStatus = "";
		m_iNumIntentos = 0;

		m_sAPaterno = "";
		m_sAMaterno = "";
		m_sCorreo = "";
		m_sFecUltCambio = "";

		m_sIP = "";
		m_sHost = "";
		m_sAtribEmpresa = "";

		m_iCveEPO = 0;
		m_iCvePYME = 0;
	  	m_iCveIF = 0;

	  	m_sTipoAfiliacion = "";

	  	m_sSIRAC = "";

	}
	/*****************************************************************************
	*		Constructor_2
	*****************************************************************************/
	public CUsuario(		String vsCveEmpresa,			String vsCveUsuario,
						String vsNombre, 				String vsContrasena,
						String vsStatus, 				int viNumIntentos,
						String vsFchVencimiento)
	{
                if (CConst.TRACE)
	        	System.out.println("CUsuario::Constructor_2");

		m_htFacultadesExt = new Hashtable();
		m_htRefPerfiles = new Hashtable();

		m_sCveUsuario = vsCveUsuario ;
		m_sCveUsuarioNafin = "";
		m_sNombre = vsNombre ;
		m_sContrasena = vsContrasena ;
		m_sCveEmpresa = vsCveEmpresa ;
		m_sFchVencimiento = vsFchVencimiento ;
		m_sStatus = vsStatus ;
		m_iNumIntentos = viNumIntentos ;

		m_sIP = "";
		m_sHost = "";

		m_sAtribEmpresa = "";

		m_iCveEPO = 0;
		m_iCvePYME = 0;
	  	m_iCveIF = 0;

	  	m_sTipoAfiliacion = "";
	  	m_sSIRAC = "";
	}
	/*****************************************************************************
	*		Constructor_3
	*****************************************************************************/
	public CUsuario(	String esCveUsuario,		String esNombre,	String esAPaterno,
					String esAMaterno, 		String esStatus,	String esCorreo,
					String esContrasena,		String esFchVencimiento,  String esAtribEmpresa)
	{
                if (CConst.TRACE)
	        	System.out.println("CUsuario::Constructor_3");

		m_htFacultadesExt = new Hashtable();
		m_htRefPerfiles = new Hashtable();

		m_sCveEmpresa = "";
		m_sStatus = "";
		m_iNumIntentos = 0;

		m_sFchVencimiento = esFchVencimiento ;
		m_sCveUsuario = esCveUsuario ;
		m_sCveUsuarioNafin = "";
		m_sNombre = esNombre ;
		m_sAPaterno = esAPaterno;
		m_sAMaterno = esAMaterno;
		m_sStatus = esStatus;
		m_sCorreo = esCorreo;
		m_sContrasena = esContrasena ;
		m_sAtribEmpresa = esAtribEmpresa;

		m_sIP = "";
		m_sHost = "";

		m_iCveEPO = 0;
		m_iCvePYME = 0;
	  	m_iCveIF = 0;

	  	m_sTipoAfiliacion = "";

	  	m_sSIRAC = "";
	}

   	/*****************************************************************************
	*		void ActivoDB()
	*****************************************************************************/
// Agregado 09/10/2002		-- CARP
  	public boolean ActivoDB()
				  throws SQLException, NullPointerException,  Exception,
							SeguException
	{
		boolean lbActivo = true;
        boolean lbResultDB = true;
		String lsPatronFch = "yyyy-MM-dd HH:mm:ss";
        Calendar    locfecha = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat(lsPatronFch);
		java.util.Date ldFechaActual = new java.util.Date();

        StringBuffer lsbSQL = new StringBuffer();
        Vector lvRegistro = null;
        Vector lvCol 	  = null;
        String lsfecha	  = "";

		int liComp = 0;
		CConexionDBPool	loconBD = new CConexionDBPool();

        if (CConst.TRACE){
		System.out.println("\tCUsuario::ActivoDB() ");
		System.out.println("Fecha actual=" + ldFechaActual);
        }
        lsbSQL.append(" SELECT max(to_char(f_consulta, 'yyyy-MM-dd HH24:MI:SS'))");
        lsbSQL.append("         , to_char(sysdate,'HH24'), to_char(sysdate,'MI'), to_char(sysdate,'SS')");
		lsbSQL.append(" FROM bit_diaria");
        lsbSQL.append(" WHERE ic_usuario = '" + m_sCveUsuarioNafin + "'" );   //m_sCveUsuario

		try{
			loconBD.conectarDB();

			loconBD.ejecutarQueryDB(lsbSQL.toString(), 1, lvRegistro);

            if ( lvRegistro.size() > 0) {
				lvCol = (Vector) lvRegistro.elementAt(0);
				lsfecha = lvCol.elementAt(0).toString();

				java.util.Date ldFchActividad = df.parse(lsfecha);

                locfecha.add(Calendar.MINUTE, -45);

				ldFechaActual = locfecha.getTime();

        if (CConst.TRACE){
                System.out.println("Fecha actual - 45'= " + ldFechaActual);
        	System.out.println("Fecha actividad = " + ldFchActividad);
        }
				liComp = ldFechaActual.compareTo(ldFchActividad);

				if (liComp < 0){
                                        if (CConst.TRACE)
				        	System.out.println("\t\tCUsuario::Activo[" + liComp + "]");
				}else{
                                        if (CConst.TRACE)
				        	System.out.println("\t\tCUsuario::No Activo[" + liComp + "]");
                    lbActivo = false;
				}
            }
		}catch(SQLException e){
			lbResultDB = false;
			throw new SQLException("CUsuario::ActivoDB(ERROR EN LA BASE DE DATOS)");
		}finally{
			loconBD.cerrarDB(lbResultDB);
		}
      return lbActivo;
	}
    /*****************************************************************************
	*		void actualizarAccesosDB()
	*****************************************************************************/
    // Agregado 11/10/2002		--CARP
	public void actualizarAccesosDB(CConexionDBPool eoconnDB)
				   throws SQLException, SeguException
	{
		String lsCadSQL = "";

                if (CConst.TRACE)
	        	System.out.println("\t CUsuario::actualizarAccesosDB()");

		lsCadSQL = "UPDATE seg_usuario ";
		lsCadSQL = lsCadSQL + "SET in_accesos = 1 " ;
		lsCadSQL = lsCadSQL + "WHERE cg_login = '" + m_sCveUsuario + "' " ;

		eoconnDB.ejecutarUpdateDB(lsCadSQL);
	}

	/*****************************************************************************
	*		void actualizarFchUltCambioUpdateDB()
	*****************************************************************************/
  	public void actualizarFchUltCambioUpdateDB(CConexionDBPool eoconDB)
  					throws SQLException
	{
		String lsSQLUpdate = "";
		java.util.Date	ldtFechaActual = new java.util.Date();
		String lsPatronFch = "yyyy-MM-dd";
		SimpleDateFormat df = new SimpleDateFormat(lsPatronFch);

                if (CConst.TRACE)
	        	System.out.println("\tCUsuario::actualizarFchUltCambioUpdateDB()");

		lsSQLUpdate = 	"UPDATE seg_usuario " +
							" SET df_ult_mod = " + CConst.FUNC_FECHA_DB_1 +
							df.format(ldtFechaActual) + CConst.FUNC_FECHA_DB_2 +
							" WHERE cg_login='" + m_sCveUsuario + "' ";

		eoconDB.ejecutarUpdateDB(lsSQLUpdate);
	}
	/*****************************************************************************
	*		void actualizarNIntentosUpdateDB()
	*****************************************************************************/
	public void actualizarNIntentosUpdateDB(CConexionDBPool	eoconDB)
				  throws SQLException
	{
		java.util.Date	ldtFechaActual = new java.util.Date();
		String lsPatronFch = "yyyy-MM-dd";
		SimpleDateFormat df = new SimpleDateFormat(lsPatronFch);
		String lsCadSQL = "";

                if (CConst.TRACE)
	        	System.out.println("\t CUsuario::actualizarNIntentosUpdateDB()");


		lsCadSQL = "UPDATE seg_usuario ";
		lsCadSQL += "SET sn_intentos = ?,";
		lsCadSQL += " df_ult_login = " + CConst.FUNC_FECHA_DB_1 ;
		lsCadSQL += df.format(ldtFechaActual) + CConst.FUNC_FECHA_DB_2 ;
		lsCadSQL += " WHERE cg_login = ? " ;
		PreparedStatement pstmt = null;
		pstmt = eoconDB.queryPrecompilado(lsCadSQL);
		pstmt.setInt(1,m_iNumIntentos);
		pstmt.setString(2,m_sCveUsuario	);
		pstmt.executeUpdate();
		pstmt.clearParameters();


		/*lsCadSQL = "UPDATE seg_usuario ";
		lsCadSQL += "SET sn_intentos = " + m_iNumIntentos + ",";
		lsCadSQL += " df_ult_login = " + CConst.FUNC_FECHA_DB_1 ;
		lsCadSQL += df.format(ldtFechaActual) + CConst.FUNC_FECHA_DB_2 ;
		lsCadSQL += " WHERE cg_login = '" + m_sCveUsuario + "' " ;

		eoconDB.ejecutarUpdateDB(lsCadSQL);
		*/
                if (CConst.TRACE)
	        	System.out.println("\tCUsuario::Saliendo actualizarNIntentosUpdateDB()");
	}
	/*****************************************************************************
	*		void agregarFacultadExt
	*****************************************************************************/
	public void agregarFacultadExt(CFacultadExt eFacultadExt)
					throws NullPointerException
	{
                if (CConst.TRACE)
	        	System.out.println("\t CUsuario::agregarFacultadExt(" + eFacultadExt.getCveFacultadExt() + ")");

		m_htFacultadesExt.put(eFacultadExt.getCveFacultadExt(), eFacultadExt);
	}
	/*****************************************************************************
	*		void agregarRefPerfil
	*****************************************************************************/
	public void agregarRefPerfil(CRefPerfil erppPerfil)
	{
                if (CConst.TRACE)
	        	System.out.println("\t CUsuario::agregarRefPerfil()");

		m_htRefPerfiles.put(new Integer(m_htRefPerfiles.size() + 1), erppPerfil);
                if (CConst.TRACE)
	        	System.out.println("\t\terppPerfilProt-Numero:" + (m_htRefPerfiles.size()+1) );
	}
	/*****************************************************************************
	*		void agregarRelUsrCte
	*****************************************************************************/
	public void agregarRelUsrCte(	char ecTipoUsr, 	int eiCveBanco,
											int eiCveCliente,	CConexionDBPool loConexion)
					throws SQLException
	{
		String lsSQLInsert = "";
                if (CConst.TRACE)
	        	System.out.println("\t CUsuario::agregarRelUsrCte()");

		if (ecTipoUsr == CConst.TP_USR_CLIENTE){
			lsSQLInsert = "INSERT INTO decrel_usr_cte (cc_usuario,sc_cte,cg_tipo_cte_dic) ";
			lsSQLInsert += " VALUES ('" + m_sCveUsuario + "'," ;
			lsSQLInsert += eiCveCliente + ",'" + ecTipoUsr + "')";
		} else{
			lsSQLInsert = "INSERT INTO decrel_usr_cte (cc_usuario,sc_bnc,cg_tipo_cte_dic) ";
			lsSQLInsert += " VALUES ('" + m_sCveUsuario + "'," + eiCveBanco + ",'";
			lsSQLInsert += ecTipoUsr + "')";
		}

                if (CConst.TRACE)
	        	System.out.println("\t CUsuario::agregarRelUsrCte(" + lsSQLInsert + ")");

		loConexion.ejecutarInsertDB(lsSQLInsert);
	}
	/*****************************************************************************
	*		 void	agregarUsuarioInsertDB()
	*****************************************************************************/
	public void agregarUsuarioInsertDB()
					throws SeguException
	{
		CConexionDBPool	loConexion = new CConexionDBPool();
	  int 		liCodigoError = 0;
	  String    lsSQL = "";
	  boolean 	lbExito = true;
	  String	lsSQLicUsuario = "";
	  int		liConsecutivo = 0;
	  String    lsFolUsuario = "";
	  String	lsFolCveUsr = "";

		java.util.Date ldtFechaVencimto ;
		String lsPatronFch = "yyyy-MM-dd";
		SimpleDateFormat df = new SimpleDateFormat(lsPatronFch);

                if (CConst.TRACE)
	        	System.out.println("\nCUsuario::agregarUsuarioInsertDB()");

		try
		{
	  		loConexion.conectarDB();

	  		lsSQL = "INSERT INTO seg_usuario (ic_usuario, cg_nombre, cg_appaterno, " +
			 				  	"cg_apmaterno, cg_status, sn_intentos, cg_mail, cg_password, "+
							  	"df_vencimiento,cg_login,cs_visitas,cs_tipo_afiliacion,cg_numero_sirac";
				if (m_iTpUsuario == CConst.TP_USR_EPO)
					lsSQL += ",IC_EPO";
				else if (m_iTpUsuario == CConst.TP_USR_PYME)
					lsSQL += ",IC_PYME";
				else if (m_iTpUsuario == CConst.TP_USR_IF)
					lsSQL += ",IC_IF";

				lsSQL +=   	") VALUES ('" + m_sCveUsuarioNafin + "','" + m_sNombre + "','" +
							  	m_sAPaterno + "','" + m_sAMaterno + "','" + m_sStatus + "', 0,'" + m_sCorreo +
							  	"','" + encriptaPassword(m_sContrasena, loConexion) + "'," + CConst.FUNC_FECHA_DB_1 + m_sFchVencimiento + CConst.FUNC_FECHA_DB_2 +
				  				",'" + m_sCveUsuario + "','0','" + m_sTipoAfiliacion + "','" + m_sSIRAC + "'" ;

				if (m_iTpUsuario == CConst.TP_USR_EPO)
					lsSQL += "," + m_iCveEPO;
				else if (m_iTpUsuario == CConst.TP_USR_PYME)
					lsSQL += "," + m_iCvePYME;
				else if (m_iTpUsuario == CConst.TP_USR_IF)
					lsSQL += "," + m_iCveIF;

				lsSQL +=   	")";


			loConexion.ejecutarInsertDB(lsSQL);

			grabarHistPasswdInsertDB(m_sContrasena,loConexion);

		} catch(SQLException ex){
			System.out.println("El codigo de error es:" + liCodigoError + ex.getMessage());

			lbExito = false;
			liCodigoError = ex.getErrorCode();
			if(liCodigoError == CConst.ERRC_REGIST_DUPLICADO)
				throw new SeguException(CConst.SEG_ERRC_SIS_YA_EXIST);
			if(liCodigoError == CConst.ERRC_INTEGR_REFERENC)
				throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			else
				throw new SeguException(CConst.COT_ERRC_DB);

		}
		catch(Exception ex)
	  {
			throw new SeguException(CConst.COT_ERRC_DB);
		}
	  finally
	  {
			loConexion.cerrarDB(lbExito);
		}
	}
	/*****************************************************************************
	*		 void	agregarUsuarioInsertDB()
	*****************************************************************************/
	public void agregarUsuarioInsertDB(int eiTipoUsr, int eiCveCliente)
					throws SeguException
	{
		CConexionDBPool	loConexion = new CConexionDBPool();
	  int 			liCodigoError = 0;
	  String      	lsSQL = "";
	  boolean 		lbExito = true;
	  String		lsSQLicUsuario = "";
	  Vector 		lvFolio = new Vector();
	  Vector 		lvRegistro = null;
	  int			liConsecutivo = 0;
	  String      	lsFolUsuario = "";
	  String		lsFolCveUsr = "";

		java.util.Date ldtFechaVencimto ;
		String lsPatronFch = "yyyy-MM-dd";
		SimpleDateFormat df = new SimpleDateFormat(lsPatronFch);


		lsSQLicUsuario = "SELECT TO_NUMBER(MAX(SUBSTR(ic_usuario,1,7)))+1 ";
		lsSQLicUsuario += " AS cveu FROM seg_usuario";

        if (CConst.TRACE)
		System.out.println("\nCUsuario::agregarUsuarioInsertDB()");
		try{
	  		loConexion.conectarDB();

	  		loConexion.ejecutarQueryDB(	lsSQLicUsuario,	0,	lvFolio);

	  		lvRegistro = (Vector) lvFolio.elementAt(0);

	  		liConsecutivo = ((Integer) lvRegistro.elementAt(0)).intValue();

	  		lsFolUsuario = Comunes.calculaFolio(liConsecutivo,7);

	  		lsFolCveUsr = lsFolUsuario.substring(0,7)+"-"+lsFolUsuario.substring(7,8);

	  		lsSQL = "INSERT INTO seg_usuario (ic_usuario, cg_nombre, cg_appaterno, " +
			 				  	"cg_apmaterno, cg_status, sn_intentos, cg_mail, cg_password, "+
							  	"df_vencimiento,cg_login,cs_visitas";
				if (eiTipoUsr == CConst.TP_USR_EPO)
					lsSQL += ",IC_EPO";
				else if (eiTipoUsr == CConst.TP_USR_PYME)
					lsSQL += ",IC_PYME";
				else if (eiTipoUsr == CConst.TP_USR_IF)
					lsSQL += ",IC_IF";

				lsSQL +=   	") VALUES ('" + lsFolCveUsr + "','" + m_sNombre + "','" +
							  	m_sAPaterno + "','" + m_sAMaterno + "','" + m_sStatus + "', 0,'" + m_sCorreo +
							  	"','" + encriptaPassword(m_sContrasena, loConexion) + "'," + CConst.FUNC_FECHA_DB_1 + m_sFchVencimiento + CConst.FUNC_FECHA_DB_2 +
				  				",'" + m_sCveUsuario + "','0'";
				if (eiTipoUsr == CConst.TP_USR_EPO ||
						eiTipoUsr == CConst.TP_USR_PYME ||
						eiTipoUsr == CConst.TP_USR_IF)
				lsSQL += "," + eiCveCliente;

				lsSQL +=   	")";

			loConexion.ejecutarInsertDB(lsSQL);

			grabarHistPasswdInsertDB(m_sContrasena,loConexion);

		} catch(SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			System.out.println("El codigo de error es:" + liCodigoError + ex.getMessage());
			if(liCodigoError == CConst.ERRC_REGIST_DUPLICADO)
				throw new SeguException(CConst.SEG_ERRC_SIS_YA_EXIST);
			if(liCodigoError == CConst.ERRC_INTEGR_REFERENC)
				throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			else
				throw new SeguException(CConst.COT_ERRC_DB);

	  }
	  catch(Exception ex)
	  {
			throw new SeguException(CConst.COT_ERRC_DB);
		}
	  finally{
			loConexion.cerrarDB(lbExito);
		}
	}
	/*****************************************************************************
	*		 void	asociarPerfilUsrDeleteDB()
	*****************************************************************************/
 	public void asociarPerfilUsrDeleteDB(	String 	esCveUsuario,
														 CConexionDBPool 	eoConexionDBPool)
					throws Exception,		NullPointerException, 	SQLException,	SeguException
	{
		int		liNumRegBorrados;
		int 		liCodigoError;
	  String 	lsSQL;

		liNumRegBorrados = 0;
		liCodigoError = 0;

                if (CConst.TRACE)
	        	System.out.println("\nCUsuario::asociarPerfilUsrDeleteDB(E)");

		lsSQL = "DELETE FROM seg_rel_usuario_perfil WHERE ic_usuario = '" + esCveUsuario + "'";

		liNumRegBorrados = eoConexionDBPool.ejecutarUpdateDB(lsSQL);
                if (CConst.TRACE){
        		System.out.println("El numero de registros borrados fue:" + liNumRegBorrados);
	        	System.out.println("\nCUsuario::asociarPerfilUsrDeleteDB(S)");
                }
	}
	/*****************************************************************************
	*		 void	asociarPerfilUsrInsertDB()
	*****************************************************************************/
	public void asociarPerfilUsrInsertDB(	String	esCveUsuario,
													 	String 	esCvePerfil,
													 	CConexionDBPool 	eoConexionDBPool)
					throws Exception,		NullPointerException, 	SQLException,	SeguException
	{
	   int 					liCodigoError = 0;
	  String      		lsSQL = "";
		int 					liNumRestriccion ;


        if (CConst.TRACE)
		System.out.println("\nCUsuario::asociarPerfilUsrInsertDB(E)");

		lsSQL = "INSERT INTO seg_rel_usuario_perfil (ic_usuario, cc_perfil)" +
				  " VALUES('" + esCveUsuario + "','" + esCvePerfil + "')";

  	   eoConexionDBPool.ejecutarInsertDB(lsSQL);
                if (CConst.TRACE)
	        	System.out.println("\nCUsuario::asociarPerfilUsrInsertDB(S)");
	}
	/*****************************************************************************
	*		void aumentarNumIntentos
	*****************************************************************************/
  	public void aumentarNumIntentos()
	{
		m_iNumIntentos++;
	}
	/*****************************************************************************
	*		void borrarRefPerfiles()
	*****************************************************************************/
	public void borrarRefPerfiles()
	{
                if (CConst.TRACE)
	        	System.out.println("CUsuario::borrarRefPerfiles");

		if (m_htRefPerfiles.size() > 0){
	 		for (Enumeration e = m_htRefPerfiles.keys() ; e.hasMoreElements() ;) {
		 	m_htRefPerfiles.remove(e.nextElement());
	   	}
		}
 	}
	/*****************************************************************************
   *      void borrarUsuarioUpdateDB()
   *****************************************************************************/
	public void borrarUsuarioUpdateDB(String esCveUsuario)
					throws	Exception,		NullPointerException,	SQLException,
								SeguException
   {
		int		liNumActualizados;
		int 		liCodigoError;
		liNumActualizados = 0;
		liCodigoError =0;
		boolean	lbExito = true;

	  CConexionDBPool loConexion = new CConexionDBPool();
	  String lsSQL;

	  lsSQL = "UPDATE seg_usuario set cg_status = 'I' WHERE cg_login = '"+
				  esCveUsuario + "'";

        if (CConst.TRACE)
                System.out.println("\nCUsuario::borrarUsuarioUpdateDB()");

	  try{
			loConexion.conectarDB();
		 liNumActualizados = loConexion.ejecutarUpdateDB(lsSQL);
                if (CConst.TRACE)
        		System.out.println("El numero de registros actualizados fue:" + liNumActualizados);
	  	if(liNumActualizados == 0)
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
		} catch(SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			System.out.println("El codigo de error es:" + liCodigoError );
		} finally{
			  loConexion.cerrarDB(lbExito);
		}
	}
	/*****************************************************************************
	*		CFacultadExt buscarFacultadExt
	*****************************************************************************/
	public CFacultadExt buscarFacultadExt(String esCveFacultadExt)
	{
		CFacultadExt loFacultadExt = null;

                if (CConst.TRACE)
	        	System.out.println("\tCFacultadExt::buscarFacultadExt(E)");

		if (m_htFacultadesExt.size() > 0)
			loFacultadExt = (CFacultadExt) m_htFacultadesExt.get(esCveFacultadExt);

                if (CConst.TRACE)
	        	System.out.println("\tCFacultadExt::buscarFacultadExt(S)");

		return loFacultadExt;
	}
	/*****************************************************************************
	*		CPerfil buscarPerfil()
	*****************************************************************************/
	public CPerfil buscarPerfil(String esCvePerfil)
	{
		CRefPerfil loRefPerfil = null;
		CPerfil loPerfil = null;
		boolean lbPerfEncontrado = false;

                if (CConst.TRACE){
	        	System.out.println("\tCUsuario::buscarPerfil(E)");
		        System.out.println("\t\tPerfil(" + esCvePerfil + ")");
                }

		if (m_htRefPerfiles.size() > 0)
		{
			Enumeration enumx = (Enumeration) m_htRefPerfiles.keys();
			while (enumx.hasMoreElements() && (lbPerfEncontrado == false))
			{
				loRefPerfil = (CRefPerfil) m_htRefPerfiles.get(enumx.nextElement()) ;
				if ((loRefPerfil.getCvePerfil()).equals(esCvePerfil))
				{
					lbPerfEncontrado = true;
					loPerfil = loRefPerfil.getPerfil();
                                        if (CConst.TRACE)
				        	System.out.println("\t\tPerfil Encontrado");
				}
			}
		}

                if (CConst.TRACE)
	        	System.out.println("\tCUsuario::buscarPerfil(S)");
		return loPerfil ;
	}
	/*****************************************************************************
	*		void cambiarContrasenaUpdateDB()
	*****************************************************************************/
	public void cambiarContrasenaUpdateDB(String esContrasenaNva, CConexionDBPool eoconnDB)
				   throws SQLException, SeguException
	{
		String lsCadSQL = "";

                if (CConst.TRACE)
	        	System.out.println("\t CUsuario::cambiarContrasenaUpdateDB()");

		lsCadSQL = "UPDATE seg_usuario ";
		lsCadSQL = lsCadSQL + "SET cg_password = '" + encriptaPassword(esContrasenaNva, eoconnDB) + "' " ;
		lsCadSQL = lsCadSQL + "WHERE cg_login = '" + m_sCveUsuario + "' " ;
		eoconnDB.ejecutarUpdateDB(lsCadSQL);
	}
	/*****************************************************************************
	*		String checaPerfBloqueado ()
	*****************************************************************************/
	public String checaPerfBloqueado(String esCveSistema, String esCvePerfProt)
	{
		String lsBloqueado = "S";
		CRefPerfilProt loRefPerfilProt = null;
		boolean lbPerfEncontrado = false;

                if (CConst.TRACE)
	        	System.out.println("\t\tChecaPerfBloqueado ");

		//lsBloqueado = ((CRefPerfilProt) m_htRefPerfiles.get(esCvePerfProt)).checaPerfBloqueado();


		if (m_htRefPerfiles.size() > 0){

			Enumeration enumx = (Enumeration) m_htRefPerfiles.keys();
			while (enumx.hasMoreElements() && (lbPerfEncontrado == false)) {
				loRefPerfilProt = (CRefPerfilProt) m_htRefPerfiles.get(enumx.nextElement()) ;
				if ((loRefPerfilProt.getCveSistema()).equals(esCveSistema) &&
						(loRefPerfilProt.getCvePerfProt()).equals(esCvePerfProt))
					lbPerfEncontrado = true;
			}

			if (loRefPerfilProt != null){
				lsBloqueado = (loRefPerfilProt.getPerfilProt()).getBloqueado();
                                if (CConst.TRACE)
			        	System.out.println("CUsuario::buscarPerfilProt(Perfil Encontrado)");
			}else{
                                if (CConst.TRACE)
			        	System.out.println("CUsuario::buscarPerfilProt(Perfil no Encontrado)");
			}
		}

		return lsBloqueado;
	}
	/*****************************************************************************
   *      Vector consultarPerfilesPxSistemaDB()
   *****************************************************************************/
	public Vector consultarPerfilesPxSistemaDB(String esCveSistema)
					  throws Exception,		NullPointerException, 	SQLException,
								SeguException
	{
		Vector svTabla;

	  String lsSQL;
		int liNumCols = 0;
		int liNumReg = 0;
		boolean	lbExito = true;

	  svTabla = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\nCUsuario::consultarPerfilesPxSistemaDB()");

		CConexionDBPool loConexion = new CConexionDBPool();

 		lsSQL = "SELECT pp.c_perfil_prot, pp.d_descripcion, pp.b_bloqueado, ";
 		lsSQL += " pp.sc_asignacion, ca.cg_sw_banco, ca.cg_sw_cliente, ca.cg_sw_central ";
 		lsSQL += " FROM seg_perfilprot pp, deccat_asignacion ca ";
 		lsSQL += " WHERE pp.c_sistema = '" + esCveSistema + "' ";
 		lsSQL += " AND pp.sc_asignacion = ca.sc_asignacion ";
 		lsSQL += " ORDER BY pp.c_perfil_prot ";

		try{
			loConexion.conectarDB();
 			loConexion.ejecutarQueryDB(lsSQL, liNumCols,svTabla);
			if(svTabla.size()==0)
				throw new SeguException(CConst.SEG_ERRC_SIS_TABLA_VACIA);
		} catch(SQLException ex){
 		   lbExito = false;
                        if (CConst.TRACE)
		        	System.out.println("Error en el select"+ ex.getMessage());
		}finally{
			loConexion.cerrarDB(lbExito);
		}
		return svTabla;
	}
	/*****************************************************************************
   *      Vector consultarPerfilesXUsuarioSelectDB()
   *****************************************************************************/
	public Vector consultarPerfilesXUsuarioSelectDB(String esCveUsuario)
  		    throws Exception,		SQLException,
	 				  SeguException
	{
		Vector 	svTabla;
	  String 	lsSQL;
		int 		liNumCols = 0;
		int 		liNumReg = 0;
		boolean	lbExito = true;

	  svTabla = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\nCUsuario::consultarPerfilesPxUsuarioSelectDB()");

		CConexionDBPool loConexion = new CConexionDBPool();
		lsSQL = "SELECT rup.cc_perfil ";
		lsSQL += " FROM seg_rel_usuario_perfil rup, seg_usuario u";
		lsSQL += " WHERE rup.ic_usuario=u.ic_usuario " ;
		lsSQL += " AND u.cg_login = '" + esCveUsuario + "'" ;

		try{
			loConexion.conectarDB();
 			loConexion.ejecutarQueryDB(lsSQL, liNumCols,svTabla);
			if(svTabla.size()==0)
				throw new SeguException(CConst.SEG_ERRC_SIS_TABLA_VACIA);
		} catch(SQLException ex){
			lbExito = false;
			System.out.println("Error en el select"+ ex.getMessage());
		} finally{
			loConexion.cerrarDB(lbExito);
		}
		return svTabla;
	}
	/*****************************************************************************
   *      Vector consultarUsuariosSelectDB()
   *****************************************************************************/
	public Vector consultarUsuariosSelectDB(	int eiTipoUsr, 		int eiCveEPO,
															int eiCveIF,			int eiCvePYME,
															String esNombre,		String esApPaterno,
															String esApMaterno,	String esCveUsuario)
					  throws Exception,		NullPointerException, 	SQLException,
								SeguException
	{
		Vector 	svTabla=null;
	  String 	lsSQL;
		int	 	liNumCols = 0;
		int 		liNumReg = 0;
		boolean	lbExito = true;
		boolean	lbExisteCond = false;
		CConexionDBPool loConexion = new CConexionDBPool();

	  svTabla = new Vector();

                if (CConst.TRACE)
	          System.out.println("\nCUsuario::consultarUsuariosSelectDB()");

		lsSQL = "SELECT u.cg_login, u.cg_nombre, u.cg_appaterno, u.cg_apmaterno,";
		lsSQL += " u.cg_status, u.cg_mail, u.df_vencimiento, u.df_ult_login,";
		lsSQL += " u.df_ult_mod, cs_tipo_afiliacion, u.ic_if, u.ic_epo, decode(u.ic_pyme,0,-1,u.ic_pyme) as ic_pyme,";
		lsSQL += " u.ic_usuario,u.cg_numero_sirac";
		lsSQL += " FROM seg_usuario u ";

			lsSQL += " WHERE ";
			if (esNombre.length() > 0)
			{
				lsSQL += " u.cg_nombre = '" + esNombre + "'";
				lbExisteCond = true;
			}
			if (esApPaterno.length() > 0)
			{
				if (lbExisteCond == true)
					lsSQL += " AND u.cg_appaterno = '" + esApPaterno + "'";
				else
					lsSQL += " u.cg_appaterno = '" + esApPaterno + "'";
				lbExisteCond = true;
			}
			if (esApMaterno.length() > 0)
			{
				if (lbExisteCond == true)
					lsSQL += " AND u.cg_apmaterno = '" + esApMaterno + "'";
				else
					lsSQL += " u.cg_apmaterno = '" + esApMaterno + "'";
				lbExisteCond = true;
			}
			if (esCveUsuario.length() > 0)
			{
				if (lbExisteCond == true)
					lsSQL += " AND u.cg_login = '" + esCveUsuario + "'";
				else
					lsSQL += " u.cg_login = '" + esCveUsuario + "'";
				lbExisteCond = true;
			}
			if (eiCveEPO > 0)
			{
				if (lbExisteCond == true)
					lsSQL += " AND u.ic_epo = " + eiCveEPO ;
				else
					lsSQL += " u.ic_epo = " + eiCveEPO;
				lbExisteCond = true;
			}
			if (eiCveIF > 0)
			{
				if (lbExisteCond == true)
					lsSQL += " AND u.ic_if = " + eiCveIF ;
				else
					lsSQL += " u.ic_if = " + eiCveIF ;
				lbExisteCond = true;
			}
			if (eiCvePYME > 0)
				if (lbExisteCond == true)
					lsSQL += " AND u.ic_pyme = " + eiCvePYME;
				else
					lsSQL += " u.ic_pyme = " + eiCvePYME;

		lsSQL += " ORDER BY u.cg_nombre ";

                if (CConst.TRACE)
	        	System.out.println("\nCUsuario::consultarUsuariosSelectDB(" + lsSQL + ")");
		try{
			loConexion.conectarDB();
 			loConexion.ejecutarQueryDB(lsSQL, liNumCols,svTabla);
			if(svTabla.size()==0)
				throw new SeguException(CConst.SEG_ERRC_SIS_TABLA_VACIA);
		} catch(SQLException ex){
			lbExito = false;
			System.out.println("Error en el select de consultarUsuariosSelectDB() "+
									ex.getMessage());
		} finally {
			loConexion.cerrarDB(lbExito);
		}
		return svTabla;
	}
	/*****************************************************************************
	*		void darBajaUsuarioUpdateDB()
	*****************************************************************************/
	public void darBajaUsuarioUpdateDB(CConexionDBPool eoconDB)
				   throws SQLException, NullPointerException, Exception
	{
		String lsCadSQL = "";

                if (CConst.TRACE)
	        	System.out.println("\t CUsuario::darBajaUsuarioUpdateDB()");

		lsCadSQL = "UPDATE seg_usuario ";
		lsCadSQL = lsCadSQL + "SET cg_status = '" + CConst.USR_INACTIVO + "' " ;
		lsCadSQL = lsCadSQL + "WHERE cg_login = '" + m_sCveUsuario + "' " ;

		eoconDB.ejecutarUpdateDB(lsCadSQL);
	}
	/*****************************************************************************
	*		 void	encriptaPassword()
	*****************************************************************************/
    // Modificado 181202	--CARP
    // Se agrego c�mo parametro de entrada la conexion a las Base de Datos
	public String encriptaPassword( String esContrasenia, CConexionDBPool eoconDB)
    	throws SQLException,SeguException
    {
		return getEncriptarPwd(esContrasenia, eoconDB);
		//return getKeyedDigest(esContrase�a);
		//return esContrase�a;
	}


	/*****************************************************************************
	*		 String getAMaterno()
	*****************************************************************************/
	public String getAMaterno()
	{
		return m_sAMaterno;
	}
	/*****************************************************************************
	*		 String getAPaterno()
	*****************************************************************************/
	public String getAPaterno()
	{
		return m_sAPaterno;
	}
	/*****************************************************************************
	*		String getContrasena
	*****************************************************************************/
	  public String getContrasena()
	{
		return m_sContrasena;
	}
	/*****************************************************************************
	*		 String getCorreo()
	*****************************************************************************/
	public String getCorreo()
	{
		return m_sCorreo;
	}
	/*****************************************************************************
	*		String getCveEmpresa
	*****************************************************************************/
	  public String getCveEmpresa()
	{
		return m_sCveEmpresa;
	}
	/*****************************************************************************
	*		int getCveEPO()
	*****************************************************************************/
	public int getCveEPO()
	{
		return m_iCveEPO ;
	}
	/*****************************************************************************
	*		int getCveIF()
	*****************************************************************************/
	public int getCveIF()
	{
		return m_iCveIF ;
	}
	/*****************************************************************************
	*		int getCvePYME()
	*****************************************************************************/
	public int getCvePYME()
	{
		return m_iCvePYME ;
	}

/*****************************************************************************
 *	String getCvePerfil()
 *
 * Descripcion: Obtiene la clave del perfil asociado al usuario. Se asume 
 *		que un usuario solo tiene un perfil.
 *
 * Valor de regreso: String con la clave del perfil
 *****************************************************************************/
	public String getCvePerfil()
	{
		String lsCvePerfil;
		
		if (m_htRefPerfiles.size() == 0) {	//El Usuario no tiene perfil asociado
			lsCvePerfil = null;
		} else {
			CRefPerfil loRefPerfil = ((CRefPerfil) m_htRefPerfiles.get(new Integer(1)));	//Se asume que m_htRefPerfiles s�lo tiene un elemento
			CPerfil loPerfil = loRefPerfil.getPerfil();
			lsCvePerfil = loRefPerfil.getCvePerfil();
		}

		return lsCvePerfil;
	}


	/*****************************************************************************
	*		String getCveUsuario
	*****************************************************************************/
	  public String getCveUsuario()
	{
		return m_sCveUsuario;
	}
	/*****************************************************************************
	*		String getCveUsuarioNafin
	*****************************************************************************/
	public String getCveUsuarioNafin()
	{
		return m_sCveUsuarioNafin;
	}
	/*****************************************************************************
	*		String getFchVencimiento
	*****************************************************************************/
	  public String getFchVencimiento()
	{
		return m_sFchVencimiento;
	}
    /*****************************************************************************
	*		String buscaHijos()
	*****************************************************************************/
    private String buscaHijos( String esSistema,     Hashtable  ehtTmpSistemas,
                                Vector eovSistPadre, Hashtable ehtTmpSistPadre,
                                CLSistema eoLSistema, CLRelSistema eoLRelSistema,
                                String esSistPadre  )
    {
        //String lsCadenaXML = "";
        StringBuffer lsCadenaXML = new StringBuffer();
        String lsCadFacXML2 = "";
        CSistema loSistema = null;
        int liNivel = 0;
        boolean lbExiste = false;

        if (CConst.TRACE)
                System.out.println("\nCUsuario::buscaHijos() \t "+ esSistema);

        liNivel = eoLRelSistema.devolverNivelPadre(esSistema);

        if (ehtTmpSistemas.containsKey(esSistema) && liNivel != 0
            && !eovSistPadre.contains(esSistema)) {

            if (CConst.TRACE)
                System.out.println("Encontro el sistema");

            if (!ehtTmpSistPadre.contains(esSistema)) {
    			loSistema = eoLSistema.buscarSistema(esSistema);
                lsCadFacXML2 = (String) ehtTmpSistemas.get(esSistema);

                    if (lsCadFacXML2.length() > 0) {
                        System.out.print("\tSistema -->" + esSistema + "Nivel:" + liNivel);
                        lsCadenaXML.append(loSistema.generarXML(liNivel,esSistPadre));
                        lsCadenaXML.append(lsCadFacXML2);
                        lsCadenaXML.append("</subsistema>");
                    }
                }
        }
        if (ehtTmpSistPadre.contains(esSistema) && !eovSistPadre.contains(esSistema)) {

            Vector lovSistemasHijos = null;
            String lsTmpSistemaHijo = "";

            eovSistPadre.addElement(esSistema);

            lovSistemasHijos = new Vector();
            lovSistemasHijos.addAll(eoLRelSistema.devolverSistemasHijos(esSistema));

            loSistema = eoLSistema.buscarSistema(esSistema);
            if (CConst.TRACE)
                    System.out.println("\tSistema -->" + esSistema + "Nivel:" + liNivel);

            if (CConst.TRACE)
                    System.out.println("Status segun BuscaSistPadre -->" + eoLRelSistema.devolverStatus(esSistema));

            if( eoLRelSistema.devolverStatus(esSistema).equals("A") || liNivel == 0 ) {

                lsCadenaXML.append(loSistema.generarXML(liNivel,esSistPadre));

            if (CConst.TRACE)
                System.out.print("\t Sistemas Hijos: " + lovSistemasHijos +" size "+ lovSistemasHijos.size());

                for (int k=0; k < lovSistemasHijos.size(); k++) {

                    lsTmpSistemaHijo = (String) lovSistemasHijos.elementAt(k);
                    if (CConst.TRACE)
                            System.out.print("\t Busca " + lsTmpSistemaHijo);
                        lsCadenaXML.append(buscaHijos( lsTmpSistemaHijo,  ehtTmpSistemas, eovSistPadre,
                                                   ehtTmpSistPadre, eoLSistema,     eoLRelSistema,
                                                   esSistPadre ));
                }
                if (ehtTmpSistemas.containsKey(esSistema))
                    lsCadenaXML.append(ehtTmpSistemas.get(esSistema));
                if ( liNivel == 0)
                    lsCadenaXML.append("</sistema>");
                else
                    lsCadenaXML.append("</subsistema>");
            } else {
                Enumeration loenSistPadres = ehtTmpSistPadre.elements();
                while(loenSistPadres.hasMoreElements())
                    eovSistPadre.addElement(loenSistPadres.nextElement());
            }
        }
        return lsCadenaXML.toString();
    }
	/*****************************************************************************
	*		 String getKeyedDigest()
	*****************************************************************************/
	private String getKeyedDigest(String buffer) {
		String resp = "";
	  	byte[] tmp;

	/*  	try
	  	{
		 	MessageDigest md5=MessageDigest.getInstance("MD5");
		 	md5.update(buffer.getBytes());
		 	tmp = md5.digest();
		 	for (int i = 0; i < tmp.length; i++)
			 	resp += new Utiles.Format("%02X").form(tmp[i] &0xFF);

		 }
		 catch (NoSuchAlgorithmException e)
		 { }
		 catch (NumberFormatException en)
		 { }
*/
		 return resp;
	}

	/*****************************************************************************
	*		 String getEncriptarPwd()
	*****************************************************************************/
    // Modificado 181202	--CARP
    // Se agrego c�mo parametro de entrada la conexion a las Base de Datos
	private String getEncriptarPwd(String esPwd, CConexionDBPool eoconDB)
    	throws SQLException, SeguException
    {
//		CConexionDBPool	loConexion = new CConexionDBPool();
		String lsEncriptaPWD = "";
		String lsQueryEncriptaPWD = "";
		Vector lvRegistro = new Vector();
		Vector lvCol = null;
        boolean lbExito = false;

            if (CConst.TRACE)
		System.out.println("CUsuarios::getEncriptarPwd (E)");
		try{
//			loConexion.conectarDB();

			lsQueryEncriptaPWD = "SELECT encripta_pwd ('" + m_sCveUsuario + "','" + esPwd + "') FROM DUAL";
                        if (CConst.TRACE)
		        	System.out.println("CUsuarios::getEncriptarPwd::sql: " + lsQueryEncriptaPWD);

//			loConexion.ejecutarQueryDB(lsQueryEncriptaPWD, 0, lvRegistro);
			eoconDB.ejecutarQueryDB(lsQueryEncriptaPWD, 0, lvRegistro);
			lvCol = (Vector) lvRegistro.elementAt(0);
			lsEncriptaPWD = lvCol.elementAt(0).toString();
                        if (CConst.TRACE)
        			System.out.println("CUsuarios::getEncriptarPwd::sql: " + lsEncriptaPWD);

		}
		catch(Exception ex){
			lbExito = false;
			throw new SeguException(CConst.COT_ERRC_DB);
		}
		finally{
//			loConexion.cerrarDB(lbExito);
			lvRegistro.removeAllElements();
			lvCol.removeAllElements();
                        if (CConst.TRACE)
	        		System.out.println("CUsuarios::getEncriptarPwd (S)");
		}
		return lsEncriptaPWD;
	}



	/*****************************************************************************
	*		String getNombre
	*****************************************************************************/
	  public String getNombre()
	{
		return m_sNombre;
	}
	/*****************************************************************************
	*		String getNumIntentos
	*****************************************************************************/
	public int getNumIntentos()
	{
		return m_iNumIntentos;
	}
	/*****************************************************************************
	*		 String getSIRAC()
	*****************************************************************************/
	public String getSIRAC()
	{
		return m_sSIRAC;
	}
	/*****************************************************************************
	*		String getStatus
	*****************************************************************************/
	  public String getStatus()
	{
		return m_sStatus;
	}
	/*****************************************************************************
	*		String getTipoAfiliacion()
	*****************************************************************************/
	public String getTipoAfiliacion()
	{
	  	return m_sTipoAfiliacion;
	}
	/*****************************************************************************
	*		int getTpUsuario()
	*****************************************************************************/
	public int getTpUsuario()
	{
		return m_iTpUsuario;
	}
	/*****************************************************************************
	*		void grabarHistPasswdInsertDB()
	*****************************************************************************/
	public void grabarHistPasswdInsertDB(String esContrasenaNva, CConexionDBPool eoconnDB)
				   throws SQLException,SeguException
	{
		java.util.Date	ldtFechaActual = new java.util.Date();
		String lsPatronFch = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat df = new SimpleDateFormat(lsPatronFch);
		String lsCadSQL = "";

                if (CConst.TRACE)
	        	System.out.println("\t CUsuario::grabarHistPasswdInsertDB()");

		lsCadSQL = 	"INSERT INTO seg_histpasswd " +
						" (ic_usuario,f_cambio,t_contrasena)" +
						" VALUES('" + m_sCveUsuarioNafin + "'," + CConst.FUNC_FECHA_DB_1 +
						df.format(ldtFechaActual) + CConst.FUNC_FECHA_DB_3 + ",'" + encriptaPassword(esContrasenaNva,eoconnDB) + "')";

                if (CConst.TRACE)
        		System.out.println("\t CUsuario::grabarHistPasswdInsertDB(" + lsCadSQL + ")");

		eoconnDB.ejecutarInsertDB(lsCadSQL);

		//Agregado 11/10/2002  	--CARP
        if ( validaPrimerLogin( eoconnDB) )
        	actualizarAccesosDB(eoconnDB);
	}
	/*****************************************************************************
	*		void limpiarSession()
	*****************************************************************************/
	public void limpiarSession()
	{
                if (CConst.TRACE)
        		System.out.println("\nCUsuario::limpiarSession()");
		m_sHost = "";
		m_sIP = "";
	}
	/*****************************************************************************
   *	    void modificarUsuarioUpdateDB()
   *****************************************************************************/
	public void modificarUsuarioUpdateDB()
					throws SeguException, SQLException
	{
	   CConexionDBPool	loConexion = new CConexionDBPool();
	  int 				 	liCodigoError;
	  int 					liNumRegActualizados;
		java.util.Date 					ldtFechaActual = new java.util.Date();
		java.util.Date 					ldtFechaVencimto ;
		boolean				lbExito = true;
		String 				lsFechVencimto ="";
		String 				lsPatronFch = "yyyy-MM-dd";
		SimpleDateFormat 	df = new SimpleDateFormat(lsPatronFch);

	  liNumRegActualizados = 0;
	  String lsSQL ;

          if (CConst.TRACE){
        	  System.out.println("\nCUsuario::Fecha de vencimiento: " + m_sFchVencimiento);
		System.out.println("\nCUsuario::modificarUsuarioUpdateDB()");
        }
		String lsFecha = "";

		lsSQL = "UPDATE seg_usuario ";
		lsSQL += " SET cg_nombre = '" + m_sNombre + "'";
		lsSQL += " ,cg_appaterno = '" + m_sAPaterno + "'";
		lsSQL += " ,cg_apmaterno = '" + m_sAMaterno + "'";
		lsSQL += " ,cg_status = '" + m_sStatus + "'";
		lsSQL += " ,cg_mail = '" + m_sCorreo + "'";
		lsSQL += " ,cg_numero_sirac = '" + m_sSIRAC + "'";

		if (m_sContrasena != null)
			if (m_sContrasena.length() > 0)
				lsSQL += ", cg_password = '" +	encriptaPassword(m_sContrasena, loConexion) + "'";

		lsSQL += ", df_ult_mod =" + CConst.FUNC_FECHA_DB_1 + df.format(ldtFechaActual) + CConst.FUNC_FECHA_DB_2;
		lsSQL += ", df_vencimiento = " + CConst.FUNC_FECHA_DB_1 + m_sFchVencimiento + CConst.FUNC_FECHA_DB_2;
		if (m_iCveEPO > 0)
			lsSQL += ", ic_epo = " + m_iCveEPO;
		else
			lsSQL += ", ic_epo = NULL";

		if (m_iCvePYME == -1 || m_iCvePYME > 0) {	//-1 es un cliente de fondos de inversi�n.
			if (m_iCvePYME == -1) {
				m_iCvePYME = 0;	//Se asigna el 0 para definir que es un cliente de Fondos de inversi�n
			}
			lsSQL += ", ic_pyme = " + m_iCvePYME;
		} else {
			lsSQL += ", ic_pyme = NULL";
		}

		if (m_iCveIF > 0)
			lsSQL += ", ic_if = " + m_iCveIF;
		else
			lsSQL += ", ic_if = NULL";

		lsSQL += " WHERE cg_login ='" + m_sCveUsuario + "'" ;

            if (CConst.TRACE)
		System.out.println("\nCUsuario::SQL: " + lsSQL);

	  try{
			loConexion.conectarDB();
		 liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);

            if (CConst.TRACE)
		 System.out.println("El numero de registros actualizados fue:" + liNumRegActualizados);
			if(liNumRegActualizados == 0)
				throw	new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);

	  } catch(SQLException ex){
		   lbExito = false;
	  	liCodigoError = ex.getErrorCode();
		 System.out.println("Error en el update, el c�digo es : (" + liCodigoError +")");
		 System.out.println(" " + ex.getMessage());
			throw new SeguException(CConst.COT_ERRC_DB);
	  }catch(Exception ex)
		{
			throw new SeguException(CConst.COT_ERRC_DB);
		}finally{

			  loConexion.cerrarDB(lbExito);
		}
	}

    // Agregado 11/10/2002		--CARP
    public int obtieneNumMaxIntentosDB( CConexionDBPool eoconDB)
    {
    	int liNumIntentos 	= CConst.iNUM_INTENTOS_PASSWD;
        Vector lovRegistro 	= new Vector();
        Vector lovDato 		= null;

		String lsSQLSelect 	= "SELECT ig_numero_intentos FROM com_param_gral";

		try {
	        eoconDB.ejecutarQueryDB(lsSQLSelect, 1, lovRegistro);

            if ( lovRegistro.size() > 0){
             	lovDato = (Vector) lovRegistro.elementAt(0);
                liNumIntentos = ((Integer) lovDato.elementAt(0)).intValue();
            }

        } catch ( Exception e){
         	System.out.println("Error al obtener par�metro de n�mero de intentos");
        }
        return liNumIntentos;
    }
	/*****************************************************************************
	*		 void setAMaterno()
	*****************************************************************************/
	public void setAMaterno(String esAMaterno)
	{
		m_sAMaterno = esAMaterno;
	}
	/*****************************************************************************
	*		 void setAMaterno()
	*****************************************************************************/
	public void setAPaterno(String esAPaterno)
	{
		m_sAPaterno = esAPaterno;
	}
	/*****************************************************************************
	*		void	setContrasena
	*****************************************************************************/
	  public void	setContrasena(String esContrasena)
	{
		m_sContrasena = esContrasena;
	}
	/*****************************************************************************
	*		 void setCorreo()
	*****************************************************************************/
	public void setCorreo(String esCorreo)
	{
		m_sCorreo = esCorreo;
	}
	/*****************************************************************************
	*		void	setCveEmpresa
	*****************************************************************************/
	  public void	setCveEmpresa(String esCveEmpresa)
	{
		m_sCveEmpresa = esCveEmpresa;
	}
	/*****************************************************************************
	*		void setCveEPO()
	*****************************************************************************/
	public void setCveEPO(int eiCveEPO)
	{
		m_iCveEPO = eiCveEPO;
	}
	/*****************************************************************************
	*		void setCveIF()
	*****************************************************************************/
	public void setCveIF(int eiCveIF)
	{
		m_iCveIF = eiCveIF;
	}
	/*****************************************************************************
	*		void setCvePYME()
	*****************************************************************************/
	public void setCvePYME(int eiCvePYME)
	{
		m_iCvePYME = eiCvePYME;
	}
	/*****************************************************************************
	*		void setCveUsuario
	*****************************************************************************/
  	public void setCveUsuario(String esCveUsuario)
	{
		m_sCveUsuario = esCveUsuario;
	}
	/*****************************************************************************
	*		void setCveUsuarioNafin
	*****************************************************************************/
	public void setCveUsuarioNafin(String esCveUsuarioNafin)
	{
		m_sCveUsuarioNafin = esCveUsuarioNafin;
	}
	/*****************************************************************************
	*		void	setFchVencimiento
	*****************************************************************************/
	  public void	setFchVencimiento(String esFchVencimiento)
	{
		m_sFchVencimiento = esFchVencimiento;
	}
	/*****************************************************************************
	*		void	setNombre
	*****************************************************************************/
	  public void	setNombre(String esNombre)
	{
		m_sNombre = esNombre;
	}
	/*****************************************************************************
	*		void	setNumIntentos
	*****************************************************************************/
	  public void	setNumIntentos(int eiNumIntentos)
	{
		m_iNumIntentos = eiNumIntentos;
	}
	/*****************************************************************************
	*		 void setSIRAC()
	*****************************************************************************/
	public void setSIRAC(String esSIRAC)
	{
		m_sSIRAC = esSIRAC;
	}
	/*****************************************************************************
	*		void	setStatus
	*****************************************************************************/
	  public void	setStatus(String esStatus)
	{
		m_sStatus = esStatus;
	}
	/*****************************************************************************
	*		void setTipoAfiliacion()
	*****************************************************************************/
	public void setTipoAfiliacion(String esTipoAfiliacion)
	{
	  	m_sTipoAfiliacion = esTipoAfiliacion;
	}
	/*****************************************************************************
	*		void setTpUsuario()
	*****************************************************************************/
	public void setTpUsuario(int eiTpUsuario)
	{
		m_iTpUsuario = eiTpUsuario;
	}
   	/*****************************************************************************
	*		boolean validaPrimerLogin()
	*****************************************************************************/
    // Agregado 10/10/2002		--CARP
  	public boolean validaPrimerLogin(CConexionDBPool eoconDB)
	{
		boolean lbResult = false;
		StringBuffer lsSQLSelect = new StringBuffer();
		int	liNumCols = 1;
		int	liCont = 0;
		Vector lvResultado = new Vector();
		Vector lvRegistro = null;

            if (CConst.TRACE)
		System.out.println("\tCUsuario::validaPrimerLogin()");
		try {
			lsSQLSelect.append("SELECT nvl(in_accesos,0) FROM seg_usuario");
			lsSQLSelect.append(" WHERE cg_login = '" + m_sCveUsuario + "'");

			eoconDB.ejecutarQueryDB(lsSQLSelect.toString(),liNumCols,lvResultado);

        	if( lvResultado.size() > 0 ){
				lvRegistro =  (Vector) lvResultado.elementAt(0);
				liCont 	   =  ((Integer) lvRegistro.elementAt(0)).intValue();

        	    if( liCont == 0 )
            		lbResult = true;
			}
        } catch (Exception e ){
         	System.out.println("Error en validaPrimerLogin() = " + e.getMessage());
        }
		return lbResult;
	}

	/*****************************************************************************
	*		boolean validarContrasena()
	*****************************************************************************/
    // Modificado 181202	--CARP
    // Se agrego c�mo parametro de entrada la conexion a las Base de Datos
  	public boolean validarContrasena(String esContrasenaIn, CConexionDBPool eoconDB)
		throws SQLException,SeguException
	{
		boolean lbResult = true;

		if (!m_sContrasena.equals(esContrasenaIn) && !m_sContrasena.equals(encriptaPassword(esContrasenaIn, eoconDB)))
			lbResult = false;

		return lbResult;
	}
	/*****************************************************************************
	*		boolean validarContrasenaHistSelectDB()
	*****************************************************************************/
  	public boolean validarContrasenaHistSelectDB(String esContrasenaNvaIn, CConexionDBPool eoconDB)
  						throws SQLException,Exception
	{
		boolean lbResult = true;
		String lsSQLSelect = "";
		String lsContrTmp = "";
		int	liNumCols = 1;
		int	liCont = 0;
		Vector lvResultado = new Vector();
		Vector lvRegistro = null;

                if (CConst.TRACE)
        		System.out.println("\tCUsuario::validarContrasenaHist()");

		lsSQLSelect = 	"SELECT t_contrasena" +
							" FROM seg_histpasswd" +
							" WHERE ic_usuario='" + m_sCveUsuarioNafin + "' ";

		eoconDB.ejecutarQueryDB(lsSQLSelect,liNumCols,lvResultado);

		while (liCont < lvResultado.size() && liCont < CConst.N_PWD_HIST){
			lvRegistro =  (Vector) lvResultado.elementAt(liCont);
			lsContrTmp =  (String) lvRegistro.elementAt(0);

			if (lsContrTmp.equals(encriptaPassword(esContrasenaNvaIn, eoconDB))){
				lbResult = false;
			}
			liCont++;
		}
		return lbResult;
	}
	/*****************************************************************************
	*		void validarUsr()
	*****************************************************************************/
  	public void validarUsr(String esContrasena,		String esIP,	String esHost)
				  throws SQLException, NullPointerException,  Exception,
							SeguException,ParseException
	{
		boolean lbResultDB = true;
		int liNumIntentos = CConst.iNUM_INTENTOS_PASSWD;
		int liResult = CConst.ERRC_OK;
		String lsPatronFch = "yyyy-MM-dd";
//		String lsPatronFch = "yyyy/MM/dd";
		SimpleDateFormat df = new SimpleDateFormat(lsPatronFch);
		java.util.Date ldFechaActual = new java.util.Date();
		int liComp = 0;
		CConexionDBPool	loconBD = new CConexionDBPool();

            if (CConst.TRACE){
		System.out.println("\tCUsuario::validarUsr() ");
		System.out.println("\t\tIP:" + esIP);
		System.out.println("\t\tHost:" + esHost);
		System.out.println("\t\tFecha de Vencimiento:" + m_sFchVencimiento);
            }
		if (m_sStatus.equals(CConst.USR_INACTIVO))	{
			throw new SeguException(CConst.SEG_ERRC_USR_INACTIVO);
		}

        m_sFchVencimiento = m_sFchVencimiento.replace('/','-');

//		System.out.print("\t\tFecha de Vencimiento (R):" + m_sFchVencimiento);

		java.util.Date ldFchVencimiento = df.parse(m_sFchVencimiento);

		liComp = ldFechaActual.compareTo(ldFchVencimiento);

		if (liComp < 0){
                    if (CConst.TRACE)
			System.out.println("\t\tCUsuario::No Caduco[" + liComp + "]");
		}else{
                    if (CConst.TRACE)
			System.out.println("\t\tCUsuario::Caduco[" + liComp + "]");
			throw new SeguException(CConst.SEG_ERRC_USR_CAD);
		}
		try{
			loconBD.conectarDB();

            //Agregado 11/10/2002		--CARP
            liNumIntentos = obtieneNumMaxIntentosDB(loconBD);

    // Modificado 181202	--CARP
    // Se agrego c�mo parametro de entrada la conexion a las Base de Datos
			if (validarContrasena(esContrasena, loconBD)){
				setNumIntentos(0);
				actualizarNIntentosUpdateDB(loconBD);

				// Agregado 10/10/2002  		-- CARP
                if (validaPrimerLogin(loconBD)){
	                throw new SeguException(CConst.SEG_ERRC_CAMBIO_CONTRA);
                }
                if (!vigenciaContrasena(loconBD)){
	                throw new SeguException(CConst.SEG_ERRC_CONTRA_VENC);
                }
				m_sIP = esIP;
				m_sHost = esHost;
			}else{
                            if (CConst.TRACE)
            	                System.out.println("\tContrase�a erronea ni=" + getNumIntentos());
				aumentarNumIntentos();
				actualizarNIntentosUpdateDB(loconBD);
				if (m_iNumIntentos == liNumIntentos){
					darBajaUsuarioUpdateDB(loconBD);
					setStatus(CConst.USR_INACTIVO);
				}
				throw new SeguException(CConst.SEG_ERRC_CONTRA_INV);
			}
		}catch (SeguException e) {
		  	System.out.println("CUsuario::validaUsr " + e.getMessage());
			throw new SeguException(e.getCodError());
		}catch(SQLException e){
			lbResultDB = false;
			throw new SQLException("CUsuario::validaUsr(ERROR EN LA BASE DE DATOS)");
		}catch(Exception e){
			lbResultDB = false;
			throw new SQLException("CUsuario::validaUsr(ERROR DESCONOCIDO)");
		}finally{
			loconBD.cerrarDB(lbResultDB);
		}
	}

   	/*****************************************************************************
	*		boolean vigenciaContrase�a()
	*****************************************************************************/
    // Agregada 10/10/2002		--CARP
  	public boolean vigenciaContrasena(CConexionDBPool eoconDB)
  						throws SQLException,Exception
	{
		boolean lbResult = true;
		StringBuffer lsSQLSelect = new StringBuffer();
		String lsContrTmp = "";
		int	liNumCols = 1;
		int	liCont = 0;
        int liDias = 0;
        int liNReg = 0;
		Vector lvResultado = new Vector();
		Vector lvRegistro = null;

            if (CConst.TRACE)
		System.out.println("\tCUsuario::vigenciaContrasena()");

		lsSQLSelect.append("SELECT nvl(in_dias_vig_pwd,0)");
		lsSQLSelect.append(" FROM com_param_gral");

		eoconDB.ejecutarQueryDB(lsSQLSelect.toString(),liNumCols,lvResultado);

        if( lvResultado.size() > 0 ){
			lvRegistro =  (Vector) lvResultado.elementAt(0);
			liDias 	   =  ((Integer) lvRegistro.elementAt(0)).intValue();
		}

        lvResultado.clear();
        lsSQLSelect.delete(0,lsSQLSelect.length());
		lsSQLSelect.append("SELECT count(*)");
		lsSQLSelect.append(" FROM seg_histpasswd");
		lsSQLSelect.append(" WHERE ic_usuario='"+ m_sCveUsuarioNafin +"'");

		eoconDB.ejecutarQueryDB(lsSQLSelect.toString(),liNumCols,lvResultado);

        if( lvResultado.size() > 0 ){
			lvRegistro =  (Vector) lvResultado.elementAt(0);
			liNReg 	   =  ((Integer) lvRegistro.elementAt(0)).intValue();
		}

		lvResultado = new Vector();
  	    lsSQLSelect.delete(0,lsSQLSelect.length());
        if ( liNReg == 0){
			lsSQLSelect.append("SELECT count(*)");
			lsSQLSelect.append(" FROM seg_usuario");
			lsSQLSelect.append(" WHERE ic_usuario='" + m_sCveUsuarioNafin + "'");
			lsSQLSelect.append(" AND sysdate < df_alta + "+ liDias);
        } else {
			lsSQLSelect.append("SELECT count(*)");
			lsSQLSelect.append(" FROM seg_histpasswd");
			lsSQLSelect.append(" WHERE ic_usuario='" + m_sCveUsuarioNafin + "'");
			lsSQLSelect.append(" AND sysdate < f_cambio +  " + liDias );
        }
		eoconDB.ejecutarQueryDB(lsSQLSelect.toString(),liNumCols,lvResultado);

   	    if( lvResultado.size() > 0 ){
			lvRegistro =  (Vector) lvResultado.elementAt(0);
			liCont 	   =  ((Integer) lvRegistro.elementAt(0)).intValue();

            if( liCont == 0 )
   	        	lbResult = false;
		}
		return lbResult;
	}

}