package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CRefPerfilProt.java
*
*	Proposito:	Clase CRefPerfilProt
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/

import java.util.*;
import java.io.*;
import java.lang.ref.*;

/****************************************************************************
*
*	clase CRefPerfilProt
*
*****************************************************************************/
public class CRefPerfilProt {

	CPerfilProt m_perPerfilProt;
	String 	m_sCvePerfProt;
	String 	m_sCveSistema;
	String 	m_sCvePerfil;

	/*****************************************************************************
	*		Constructor_1
	*****************************************************************************/
	public CRefPerfilProt()
	{
                if (CConst.TRACE)
	        	System.out.println("CRefPerfilProt::Constructor_1");

		m_perPerfilProt = null;
		m_sCvePerfProt = "";
		m_sCveSistema = "";
		m_sCvePerfil = "";
	}
	/*****************************************************************************
	*		Constructor_2
	*****************************************************************************/
	public CRefPerfilProt(	String esCvePerfil, 	String esCveSistema,
							String esCvePerfProt,	CPerfilProt eperPerfilProt )
	{
                if (CConst.TRACE)
	        	System.out.println("CRefPerfilProt::Constructor_2");

		m_perPerfilProt = eperPerfilProt;
		m_sCvePerfProt = esCvePerfProt ;
		m_sCveSistema = esCveSistema;
		m_sCvePerfil = esCvePerfil;

                if (CConst.TRACE)
	        	System.out.println("CRefPerfilProt::Constructor_2 ->" + eperPerfilProt.getCvePerfilProt() + "-" + m_perPerfilProt.getCvePerfilProt() );

	}
	/*****************************************************************************
	*		String checaPerfBloqueado()
	*****************************************************************************/
	public String checaPerfBloqueado()
	{
		String lsBloqueado = "";

		lsBloqueado = m_perPerfilProt.getBloqueado();

                if (CConst.TRACE)
	        	System.out.println("\t\tChecaPerfBloqueado 3[" + lsBloqueado + "]");

		return lsBloqueado;
	}
	/*****************************************************************************
	*		String getCvePerfil
	*****************************************************************************/
	public String getCvePerfil()
	{
		return m_sCvePerfil;
	}
	/*****************************************************************************
	*		String getCvePerfProt
	*****************************************************************************/
	public String getCvePerfProt()
	{
		return m_sCvePerfProt;
	}
	/*****************************************************************************
	*		String getCveSistema
	*****************************************************************************/
	public String getCveSistema()
	{
		return m_sCveSistema;
	}
	/*****************************************************************************
	*		CPerfilProt getPerfilProt()
	*****************************************************************************/
	public CPerfilProt getPerfilProt()
	{
		return m_perPerfilProt;
	}
	/*****************************************************************************
	*		void	setCvePerfil
	*****************************************************************************/
	public void	setCvePerfil(String esCvePerfil)
	{
		m_sCvePerfil = esCvePerfil;
	}
	/*****************************************************************************
	*		void setCvePerfProt
	*****************************************************************************/
	public void setCvePerfProt(String esCvePerfProt)
	{
		m_sCvePerfProt = esCvePerfProt;
	}
	/*****************************************************************************
	*		void	setCveSistema
	*****************************************************************************/
	public void	setCveSistema(String esCveSistema)
	{
		m_sCveSistema = esCveSistema;
	}
}