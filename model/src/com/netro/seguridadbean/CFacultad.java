package com.netro.seguridadbean;

import java.util.*;
import java.lang.*;
import java.text.*;
import java.io.*;
import java.util.Date;
import java.sql.SQLException;

/**
 * Manejo de Facultades de la aplicacion.
 * @author Pedro A. Garcia Ramirez (10/01/2001)
 * @author Gilberto Aparicio. (29/08/2005)
 *
 */
public class CFacultad {

	String m_sCveSistema = "";
	String m_sCveFacultad = "";
	String m_sDescripcion ="";
	String m_sDescripcionIngles ="";
	String m_sTipo = "";
	String m_sBloqueado = "";
	long m_lNRestriccion = 0;
	String m_sHoraInicial = "";
	String m_sHoraFinal = "";
	String m_sFchVencimiento = "";
	String m_sPagina = "";
	String m_sPaginaIngles = "";
	int m_iCveAsignac = 0;
	int m_iQuitaMnu = 0;
	int m_iOrdenMenu = 0;
	String m_sImg = "";
	String m_sSwImg ="";
	String m_sClaveRevisar ="";
	

	/**
	 * Constructor predeterminado
	 */
	public CFacultad() {
		if (CConst.TRACE) {
			System.out.println("CFacultad::Constructor_1");
		}
	}
	
	/**
	 * Constructor
	 * @param esCveSistema Clave del sistema al que pertenece la facultad
	 * @param esCveFacultad Clave de la facultad
	 * @param esDescripcion Descripcion de la facultad
	 * @param esDescripcionIngles Descripcion de la facultad cuando es idioma ingles
	 * @param esBloqueado Indicador de bloqueo de la facultad
	 * @param esTipo Tipo de facultad
	 * @param elRestriccion ???
	 * @param esPagina URL al que redirecciona
	 * @param esPaginaIngles URL al que redirecciona cuando es idioma ingles
	 * @param eiCveAsignac ???
	 * @param esImg Ruta de la imagen a mostrar
	 * @param esSwImg Indicador para usar imagen (I) o texto (D) al mostrar la facultad
	 * @param eiSwQuitaMnu ???
	 * @param esClaveRevisar Clave con la cual se compara para determinar si se muestra o no la facultad
	 * 		Un ejemplo de uso es que se puede emplear para mostrar facultades
	 * 		s�lo a ciertos afiliados, para lo cual la clave de la facultad debe concordar con la que se
	 * 		pase como par�metro al XSL en "clave_revisar"
	 */
	public CFacultad( String esCveSistema, String esCveFacultad,
			String esDescripcion, String esDescripcionIngles,
			String esBloqueado,
			String esTipo, long elRestriccion,
			String esPagina, String esPaginaIngles,
			int eiCveAsignac,
			String esImg, String esSwImg,
			int eiSwQuitaMnu, String esClaveRevisar) {
		if (CConst.TRACE) {
			System.out.println("CFacultad::Constructor_2");
		}

		m_sCveSistema = esCveSistema;
		m_sCveFacultad = esCveFacultad;
		m_sDescripcion = esDescripcion;
		m_sDescripcionIngles = esDescripcionIngles;
		m_sTipo = esTipo;
		m_sBloqueado = esBloqueado;
		m_lNRestriccion = elRestriccion;
		m_sHoraInicial = "";
		m_sHoraFinal = "";
		m_sFchVencimiento = "";
		m_sPagina = esPagina;
		m_sPaginaIngles = esPaginaIngles;
		m_iCveAsignac = eiCveAsignac;
		m_iQuitaMnu = eiSwQuitaMnu;
		m_iOrdenMenu = 0;
		m_sImg = esImg;
		m_sSwImg = esSwImg;
		m_sClaveRevisar = esClaveRevisar;
		
	}
	
	/**
	 * Constructor.
	 * @param esCveSistema Clave del sistema al que pertenece la facultad
	 * @param esCveFacultad Clave de la facultad
	 * @param esDescripcion Descripcion de la facultad
	 * @param esDescripcionIngles Descripcion de la facultad cuando es idioma ingles
	 * @param esBloqueado Indicador de bloqueo de la facultad
	 * @param esTipo Tipo de facultad
	 * @param elRestriccion ???
	 * @param esHoraInicial ???
	 * @param esHoraFinal ???
	 * @param esFchVencimiento ???
	 * @param esPagina URL al que redirecciona
	 * @param esPaginaIngles URL al que redirecciona cuando es idioma ingl�s
	 * @param eiQuitaMnu ???
	 * @param eiOrdenMenu Numero que indica la precedencia para aparecer en menu
	 * @param esImg Ruta de la imagen a usar
	 * @param esSwImg Indicar de uso de imagen (I)  o texto (D)
	 * @param esClaveRevisar Clave con la cual se compara para determinar si se muestra o no la facultad
	 * 		Un ejemplo de uso es que se puede emplear para mostrar facultades
	 * 		s�lo a ciertos afiliados, para lo cual la clave de la facultad debe concordar con la que se
	 * 		pase como par�metro al XSL en "clave_revisar"
	 *
	 */
	public CFacultad( String esCveSistema, String esCveFacultad,
			String esDescripcion, String esDescripcionIngles,
			String esBloqueado,
			String esTipo, long elRestriccion,
			String esHoraInicial, String esHoraFinal,
			String esFchVencimiento,
			String esPagina, String esPaginaIngles,
			int eiQuitaMnu, int eiOrdenMenu,
			String esImg, String esSwImg, String esClaveRevisar) {
		if (CConst.TRACE) {
			System.out.println("CFacultad::Constructor_3");
		}

		m_sCveSistema = esCveSistema;
		m_sCveFacultad = esCveFacultad;
		m_sDescripcion = esDescripcion;
		m_sDescripcionIngles = esDescripcionIngles;
		m_sTipo = esTipo;
		m_sBloqueado = esBloqueado;
		m_lNRestriccion = elRestriccion;
		m_sHoraInicial = esHoraInicial;
		m_sHoraFinal = esHoraFinal;
		m_sFchVencimiento = esFchVencimiento;
		m_sPagina = esPagina;
		m_sPaginaIngles = esPaginaIngles;
		m_iQuitaMnu = eiQuitaMnu;
		m_iOrdenMenu = eiOrdenMenu;
		m_sImg = esImg;
		m_sSwImg = esSwImg;
		m_sClaveRevisar = esClaveRevisar;

		if (CConst.TRACE){
			System.out.println("CFacultad::Hora inicial(" + m_sHoraInicial + ")");
			System.out.println("CFacultad::Hora final(" + m_sHoraFinal + ")");
			System.out.println("CFacultad::Fecha Venc(" + m_sFchVencimiento + ")");
		}
	}
	
	
	/*****************************************************************************
	*		void actualizarFacultad()
	*****************************************************************************/
	public void actualizarFacultad(CFacultad eofacFacultad) {
		if (CConst.TRACE) {
			System.out.println("\tCFacultad::actualizarFacultad");
		}

		m_sCveSistema = eofacFacultad.getCveSistema();
		m_sCveFacultad = eofacFacultad.getCveFacultad();
		m_sDescripcion = eofacFacultad.getDescripcion();
		m_sDescripcionIngles = eofacFacultad.getDescripcionIngles();
		m_sTipo = eofacFacultad.getTipo();
		m_sBloqueado = eofacFacultad.getBloqueado();
		m_lNRestriccion = eofacFacultad.getRestriccion();
		m_sHoraInicial = eofacFacultad.getHoraInicial();
		m_sHoraFinal = eofacFacultad.getHoraFinal();
		m_sFchVencimiento = eofacFacultad.getFchVencimiento();
		m_sPagina = eofacFacultad.getPagina();
		m_sPaginaIngles = eofacFacultad.getPaginaIngles();
		m_sClaveRevisar = eofacFacultad.getClaveRevisar();
	}
	
	
	/**
	 * Agrega la facultad en la BD tomando los valores del bean.
	 *
	 */
	public void agregarFacultadInsertDB()
			throws Exception, SQLException, SeguException {
		CConexionDBPool loConexion = new CConexionDBPool();
		int liCodigoError = 0;
		String lsSQL = "";
		String lsSQLSelect = "";
		int liNumColsRefPP = 1;
		int liConsecSistema = 0;
		boolean lbExito= false;
		Vector lvMaxOrdenMnu = new Vector();
		Vector lvRegistro;

		if (CConst.TRACE) {
			System.out.println("\nCFacultad::agregarFacultadInsertDB()");
		}

		lsSQLSelect = " SELECT max(sg_orden_mnu) as mx_orden_mnu FROM " +
				" seg_facultades ";

		try{
			loConexion.conectarDB();
		} catch(Exception ex) {
			throw new SeguException(CConst.COT_ERRC_DB);
		}

		try {
			loConexion.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvMaxOrdenMnu);

			if (lvMaxOrdenMnu != null) {
				lvRegistro = (Vector) lvMaxOrdenMnu.elementAt(0);
				liConsecSistema = ((Integer) lvRegistro.elementAt(0)).intValue();
				liConsecSistema++;
			}
			String restriccion = (m_lNRestriccion== 0)?"null":String.valueOf(m_lNRestriccion);
			
			lsSQL = "INSERT INTO SEG_FACULTADES (" +
					" c_sistema,c_facultad, d_descripcion,"+
					" d_descripcion_ing, " +
					" b_bloqueado, t_tipo_facultad, " +
					" n_restriccion, " +
					" p_pagina, p_pagina_ing, " +
					" sc_asignacion, sg_orden_mnu, " +
					" cg_imagen, cg_sw_img_desc, " +
					" sg_sw_quita_mnu, cg_clave_revisar)" +
					" VALUES('" + m_sCveSistema + "', " +
					" '" + m_sCveFacultad + "'," +
					" '" + m_sDescripcion + "'," + 
					" '" + m_sDescripcionIngles + "'," + 
					" '" + m_sBloqueado + "', '" + m_sTipo + "'," +
					"" + restriccion + "," +
					" '" + m_sPagina + "','" + m_sPaginaIngles + "'," +
					"" + m_iCveAsignac + "," + liConsecSistema + "," +
					" '" + m_sImg + "', '" + m_sSwImg + "'," +
					"" + m_iQuitaMnu + ", '" + m_sClaveRevisar + "')";

			loConexion.ejecutarInsertDB(lsSQL);
			
			lbExito= true ;
		} catch(SQLException ex){
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			if(liCodigoError == CConst.ERRC_REGIST_DUPLICADO) {
				throw new SeguException(CConst.SEG_ERRC_SIS_YA_EXIST);
			} else if(liCodigoError == CConst.ERRC_INTEGR_REFERENC){
				System.out.println("mensaje de error: " + ex.getMessage() );
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_INSERT_REFERENC);
			}
			System.out.println("Error en agregarFacultadInsertDB(): " + ex.getMessage());
			System.out.println("Error codigo(" + liCodigoError  + ")");
		} finally{
			loConexion.cerrarDB(lbExito);
		}
	}



	/*****************************************************************************
	*      void bloquearFacultadUpdateDB()
	*****************************************************************************/
	public void bloquearFacultadUpdateDB(String esCveSistema, 
			String esCveFacultad, String esBloqueo)
			throws Exception, SQLException, SeguException {
		CConexionDBPool	loConexion = new CConexionDBPool();
		int liCodigoError;
		int liNumRegActualizados;
		boolean lbExito = true ;

		liNumRegActualizados = 0;
		String lsSQL;
		lsSQL= "UPDATE SEG_FACULTADES " +
				" SET b_bloqueado = '" + esBloqueo + "' " +
				" WHERE c_sistema ='"+ esCveSistema + "'" +
				" AND c_facultad = '"+ esCveFacultad + "'";

		if (CConst.TRACE) {
			System.out.println("\nCFacultad::bloquearFacultadUpdateDB()");
		}

		try {
			loConexion.conectarDB();
			liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if(liNumRegActualizados == 0) {
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
			}
			if (CConst.TRACE) {
				System.out.println("El numero de regisros actualizados fue:" +
						liNumRegActualizados);
			}
		} catch(SQLException ex) {
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			System.out.println("Error en el update, el c�digo es : (" +
					liCodigoError +")");
		} finally {
			loConexion.cerrarDB(lbExito);
		}
	}
	
	/*****************************************************************************
	*    	 void borrarFacultadDeleteDB()
	*****************************************************************************/
	public void borrarFacultadDeleteDB( String esCveSistema, String esCveFacultad)
			throws Exception, SQLException, SeguException {
		CConexionDBPool	loConexion = new CConexionDBPool();
		boolean lbExito = true;
		int liNumRegBorrados;
		int  liCodigoError;
		liNumRegBorrados = 0;
		liCodigoError = 0;

		String lsSQL;

		lsSQL = "DELETE FROM seg_facultades WHERE c_sistema ='"+ esCveSistema+
				"' AND c_facultad = '" + esCveFacultad + "'";


		if (CConst.TRACE) {
			System.out.println("\nCFacultad::borrarFacultadDeleteDB()");
		}
		try {
			loConexion.conectarDB();
			liNumRegBorrados = loConexion.ejecutarUpdateDB(lsSQL);
			if (CConst.TRACE) {
				System.out.println("El numero de registros borrados fue:" + liNumRegBorrados);
			}
			if(liNumRegBorrados == 0) {
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ABORR_INEXIST);
			}
		} catch(SQLException ex){
			lbExito = false ;
			liCodigoError = ex.getErrorCode();
			if(liCodigoError == CConst.ERRC_INTEGR_REFERENC ) {
				throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			}
			System.out.println("Error en borrarFacultadDeleteDB():" + ex.getMessage());
		} finally{
			loConexion.cerrarDB(lbExito);
		}
	}
	
	/**
	 * Genera el fragmento xml correspondiente a la facultad
	 * @param esCvePerfil Clave del Perfil
	 * @param esCvePerfilProt Clave del Perfil Prototipo
	 * @return Cadena con el fragmento de xml correspondiente a la facultad.
	 *
	 */
	public String generarXML(String esCvePerfil, String esCvePerfilProt) {
		char lcComilla = '"' ;
		StringBuffer lsCadenaXML = new StringBuffer();

		lsCadenaXML.append("<facultad cvefacultad=" + lcComilla + m_sCveFacultad + lcComilla);
		lsCadenaXML.append(" cvesistema=" + lcComilla + m_sCveSistema + lcComilla);
		lsCadenaXML.append(" cveperfil=" + lcComilla + esCvePerfil + lcComilla);
		lsCadenaXML.append(" cveperfilprot=" + lcComilla + esCvePerfilProt + lcComilla);
		lsCadenaXML.append(" descripcion=" + lcComilla + m_sDescripcion + lcComilla);
		lsCadenaXML.append(" descripcioningles=" + lcComilla + m_sDescripcionIngles + lcComilla);
		lsCadenaXML.append(" tipo=" + lcComilla + m_sTipo + lcComilla);
		lsCadenaXML.append(" pagina=" + lcComilla + m_sPagina + lcComilla);
		lsCadenaXML.append(" paginaingles=" + lcComilla + m_sPaginaIngles + lcComilla);
		lsCadenaXML.append(" quitar_mnu=" + lcComilla + m_iQuitaMnu + lcComilla);
		lsCadenaXML.append(" orden_menu=" + lcComilla + m_iOrdenMenu + lcComilla);

		lsCadenaXML.append(" sw_img_desc=" + lcComilla + m_sSwImg + lcComilla);
		lsCadenaXML.append(" imagen=" + lcComilla + m_sImg + lcComilla);
		
		lsCadenaXML.append(" clave_revisar=" + lcComilla + m_sClaveRevisar + lcComilla);
		
		lsCadenaXML.append(">");
		lsCadenaXML.append("</facultad>");    

		return lsCadenaXML.toString();
	}
	
	
	/*****************************************************************************
	*		String getBloqueado
	*****************************************************************************/
	public String getBloqueado() {
		return m_sBloqueado;
	}
	
	/*****************************************************************************
	*		String getCveFacultad
	*****************************************************************************/
	public String getCveFacultad() {
		return m_sCveFacultad;
	}

	/*****************************************************************************
	*		String getCveSistema
	*****************************************************************************/
	public String getCveSistema() {
		return m_sCveSistema;
	}

	/*****************************************************************************
	*		String getDescripcion
	*****************************************************************************/
	public String getDescripcion() {
		return m_sDescripcion;
	}


	/**
	 * Obtiene la descripcion de la faculta para el idioma ingl�s
	 */
	public String getDescripcionIngles() {
		return m_sDescripcionIngles;
	}

	/*****************************************************************************
	*		String getFchVencimiento
	*****************************************************************************/
	public String getFchVencimiento() {
		return m_sFchVencimiento;
	}
	/*****************************************************************************
	*		String getHoraFinal
	*****************************************************************************/
	public String getHoraFinal() {
		return m_sHoraFinal;
	}
	/*****************************************************************************
	*		String getHoraInicial
	*****************************************************************************/
	public String getHoraInicial() {
		return m_sHoraInicial;
	}
	/*****************************************************************************
	*		int getOrdenMenu
	*****************************************************************************/
	public int getOrdenMenu() {
		return m_iOrdenMenu;
	}

	/*****************************************************************************
	*		String getPagina
	*****************************************************************************/
	public String getPagina() {
		return m_sPagina;
	}

	/**
	 * Obtiene el URL de la facultad, para el idioma ingl�s
	 */
	public String getPaginaIngles() {
		return m_sPaginaIngles;
	}

	/**
	 * Obtiene La clave a comparar, que determina si una facultad aparece o no
	 */
	public String getClaveRevisar() {
		return m_sClaveRevisar;
	}

	/*****************************************************************************
	*		long getRestriccion
	*****************************************************************************/
	public long getRestriccion() {
		return m_lNRestriccion;
	}
	/*****************************************************************************
	*		String getTipo
	*****************************************************************************/
	public String getTipo() {
		return m_sTipo;
	}
	/*****************************************************************************
	*	    void modificarFacultadUpdateDB()
	*****************************************************************************/
	/*public void modificarFacultadUpdateDB()
			throws Exception, SQLException, SeguException {
		CConexionDBPool	loConexion = new CConexionDBPool();
		int 					liCodigoError;
		int 					liNumRegActualizados;
		boolean				lbExito = true ;

		liNumRegActualizados = 0;
		String lsSQL ;

		lsSQL = "UPDATE SEG_FACULTADES SET d_descripcion = '" + m_sDescripcion +
				"',b_bloqueado = '" + m_sBloqueado + "',t_tipo_facultad = '" +
				m_sTipo + "',n_restriccion = '" + m_lNRestriccion + "',p_pagina = '" +
				m_sPagina + "', sc_asignacion =" + m_iCveAsignac + " WHERE c_sistema ='" +
				m_sCveSistema + "' AND c_facultad = '"+ m_sCveFacultad + "';";

		System.out.println("\n CFacultad::modificarFacultadUpdateDB()");

		try{
			loConexion.conectarDB();
			liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if(liNumRegActualizados == 0) {
				throw	new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
			}
			System.out.println("El numero de registros actualizados fue:" + liNumRegActualizados);
		} catch(SQLException ex) {
			lbExito = false ;
			liCodigoError = ex.getErrorCode();
			if(liCodigoError == CConst.ERRC_INTEGR_REFERENC) {
				throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			}
			System.out.println("Error en el update, c�digo : (" + liCodigoError +")");
		} finally {
			loConexion.cerrarDB(lbExito);
		}
	}
	*/

	/**
	 * Modificacion en BD de la facultad
	 *
	 */
	public void modificarFacultadUpdateDB()
			throws Exception, SQLException, SeguException {
		CConexionDBPool loConexion = new CConexionDBPool();
		int liCodigoError;
		int  liNumRegActualizados;
		boolean lbExito = true;
		liNumRegActualizados = 0;
		String lsSQL;

		String restriccion = (m_lNRestriccion == 0)?"null":String.valueOf(m_lNRestriccion);
		lsSQL = "UPDATE SEG_FACULTADES " +
				" SET d_descripcion = '" + m_sDescripcion + "'," +
				" d_descripcion_ing = '" + m_sDescripcionIngles + "'," +
				" b_bloqueado = '" + m_sBloqueado + "'," +
				" t_tipo_facultad = '" + m_sTipo + "'," +
				" n_restriccion = " + restriccion + "," +
				" p_pagina = '" + m_sPagina + "'," +
				" p_pagina_ing = '" + m_sPaginaIngles + "'," +
				" sc_asignacion =" + m_iCveAsignac + "," +
				" cg_imagen = '" + m_sImg + "'," +
				" cg_sw_img_desc = '" + m_sSwImg + "', " +
				" sg_sw_quita_mnu = " + m_iQuitaMnu + "," +
				" cg_clave_revisar = '" + m_sClaveRevisar + "'" +
				" WHERE c_sistema ='" +m_sCveSistema + "'" +
				" AND c_facultad = '"+ m_sCveFacultad + "'";

		if (CConst.TRACE) {
			System.out.println("\n CFacultad::modificarFacultadUpdateDB()");
		}


		try {
			loConexion.conectarDB();
			liNumRegActualizados = loConexion.ejecutarUpdateDB(lsSQL);
			if(liNumRegActualizados == 0) {
				if (CConst.TRACE) {
					System.out.println("El numero de registros actualizados fue:" + liNumRegActualizados);
				}
				throw new SeguException(CConst.SEG_ERRC_SIS_REG_ACTUAL_NOEXIS);
			}
		} catch(SQLException ex) {
			lbExito = false;
			liCodigoError = ex.getErrorCode();
			if(liCodigoError == CConst.ERRC_INTEGR_REFERENC) {
				throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			}
			System.out.println("Error en el update, c�digo : (" + liCodigoError +")");
		} finally {
			loConexion.cerrarDB(lbExito);
		}
	}


	/*****************************************************************************
	*		void	setBloqueado
	*****************************************************************************/
	public void	setBloqueado(String esBloqueado) {
		m_sBloqueado = esBloqueado;
	}
	/*****************************************************************************
	*		void setCveFacultad
	*****************************************************************************/
	public void setCveFacultad(String esCveFacultad) {
		m_sCveFacultad = esCveFacultad;
	}
	/*****************************************************************************
	*		void setCveSistema
	*****************************************************************************/
	public void setCveSistema(String esCveSistema) {
		m_sCveSistema = esCveSistema;
	}

	/*****************************************************************************
	*		void	setDescripcion
	*****************************************************************************/
	public void setDescripcion(String esDescripcion) {
		m_sDescripcion = esDescripcion;
	}

	/**
	 * Establece la descripcion en ingles de la facultad
	 * @param esDescripcionIngles Descripcion en Ingles
	 */
	public void setDescripcionIngles(String esDescripcionIngles) {
		m_sDescripcionIngles = esDescripcionIngles;
	}

	/*****************************************************************************
	*		void	setFchVencimiento
	*****************************************************************************/
	public void	setFchVencimiento(String esFchVencimiento) {
		m_sFchVencimiento = esFchVencimiento;
	}
	/*****************************************************************************
	*		void	setHoraFinal
	*****************************************************************************/
	public void	setHoraFinal(String esHoraFinal) {
		m_sHoraFinal = esHoraFinal;
	}
	/*****************************************************************************
	*		void	setHoraInicial
	*****************************************************************************/
	public void	setHoraInicial(String esHoraInicial) {
		m_sHoraInicial = esHoraInicial;
	}
	/*****************************************************************************
	*		void setOrdenMenu
	*****************************************************************************/
	public void setOrdenMenu(int eiOrdenMenu) {
		m_iOrdenMenu = eiOrdenMenu;
	}
	
	
	/*****************************************************************************
	*		void	setPagina
	*****************************************************************************/
	public void setPagina(String esPagina) {
		m_sPagina = esPagina;
	}

	/**
	 * Establece el URL para la facultad en idioma ingles
	 */
	public void setPaginaIngles(String esPaginaIngles) {
		m_sPaginaIngles = esPaginaIngles;
	}


	/*****************************************************************************
	*		void	setRestriccion
	*****************************************************************************/
	public void setRestriccion(long elRestriccion) {
		m_lNRestriccion = elRestriccion;
	}

	/**
	 * Establece la Clave para comparar que determina si se muestra la facultad o no.
	 */
	public void setClaveRevisar(String esClaveRevisar) {
		m_sClaveRevisar = esClaveRevisar;
	}


	/*****************************************************************************
	*		void	setTipo
	*****************************************************************************/
	public void setTipo(String esTipo) {
		m_sTipo = esTipo;
	}
	
	/**
	 * 
	 * 
	 * 
	 */
	public int validarRestriccion() {
		if (CConst.TRACE) {
			System.out.println("\tCFacultad::validarRestriccion(E)");
		}

		int liResult = CConst.ERRC_OK;
		if (m_lNRestriccion > 0){
			try{
				String lsPatronFch = "yyyy-MM-dd";
				SimpleDateFormat formatoFch =  new SimpleDateFormat(lsPatronFch);
				String lsPatronHr = "hh:mm:ss";
				SimpleDateFormat formatoHr =  new SimpleDateFormat(lsPatronHr);
				Date ldFechaActual = new Date();
				int liComp = 0;

				if (m_sFchVencimiento.length() > 0){
					Date ldFchVencimiento = formatoFch.parse(m_sFchVencimiento);

					liComp = ldFechaActual.compareTo(ldFchVencimiento);

					if (liComp < 0){
						if (CConst.TRACE){
							System.out.println("\t\tCFacultad::fecha No Vencida[" + ldFechaActual + "]");
							System.out.println("\t\tCFacultad::fecha No Vencida[" + ldFchVencimiento + "]");
						}
					}else{
						liResult = CConst.SEG_ERRC_FEC_VENC;
						if (CConst.TRACE) {
							System.out.println("\t\tCFacultad::fecha Vencida[" + liComp + "]");
						}
					}
				}
				if (liResult == CConst.ERRC_OK) {
					Date ldHoraActual = new Date();
					CHora lohrHoraActual = new CHora();
					CHora lohrHoraInicial = new CHora();
					CHora lohrHoraFinal = new CHora();

					lohrHoraActual.setCadHora(formatoHr.format(new Date()));
					lohrHoraInicial.setCadHora(m_sHoraInicial);
					lohrHoraFinal.setCadHora(m_sHoraFinal);

					if (lohrHoraActual.compararHora(lohrHoraInicial) > 0) {
						if (CConst.TRACE) {
							System.out.println("\tBien Hora Inicial");
						}
						if (lohrHoraActual.compararHora(lohrHoraFinal) < 0){
							if (CConst.TRACE) {
								System.out.println("\tBien Hora Final");
							}
						}else{
							if (CConst.TRACE) {
								System.out.println("\tHora Final Invalida");
							}
							liResult = CConst.SEG_ERRC_FUERA_HR;
						}
					}else{
						if (CConst.TRACE) {
							System.out.println("\tHora Inicial Invalida");
						}
						liResult = CConst.SEG_ERRC_FUERA_HR;
					}
				}

			}catch(ParseException e){
				System.out.println("Error: " + e);
			}
		}
		if (CConst.TRACE) {
			System.out.println("\tCFacultad::validarRestriccion(S)");
		}
		return liResult;
	}
}