package com.netro.seguridadbean;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Bitacora;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.QueryDebugger;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.usuarios.SolicitudArgus;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

/**
 * SeguridadBean implementa mecanismos de seguridad, controlando y
 * administrando los Sistemas, Perfiles, Facultades y principalmente
 * Usuarios.
 *
 * @author Pedro A. Garcia Ramirez (10/01/2001)
 * @author Cindy A. Ramos Perez -CARP-(07/10/2002)
 * @author Gilberto E. Aparicio Guerrero -GEAG- (03/09/2003)
 */

@Stateless(name = "SeguridadEJB" , mappedName = "SeguridadEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class SeguridadBean implements Seguridad {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(SeguridadBean.class);

	static CLEmpresa 	m_oLEmpresa;
	static CLSistema 	m_oLSistema;			// Listado de Sistemas.
	static CLUsuario 	m_oLUsuario;			// Listado de Usuarios.
	static CLPerfil 	m_oLPerfil;				// Listado de Perfiles.
	static CLRelSistema m_oLRelSistemas;	// Listado de Relaci�n de Sistemas.

	/**
	 * Crea una facultad, la graba en la BD y actualiza el listado en memoria.
	 *
	 * @param esCveSistema	Clave del sistema
	 * @param esCveFacultad	Clave de la facultad
	 * @param esDescripcion	Descripci�n de la Facultad
	 * @param esDescripcionIngles	Descripci�n de la Facultad en idioma ingles
	 * @param esBloqueado	Bloqueado?
	 * @param esTipo	???
	 * @param elNRestriccion	???
	 * @param esPagina	URL correpondiente a la facultad
	 * @param esPaginaIngles	URL correpondiente a la facultad en idioma ingles
	 * @param eiCveAsignac	???
	 * @param esImg	Ruta de la imagen
	 * @param esSwImg	Indicador de si se utilizara la imagen (I) o no (D)
	 * @param eiSwQuitaMnu	???
	 * @param esClaveRevisar. Clave que permite especificar si una facultad aparecera o no.
	 * 		Si la clave concuerda con el parametro establecido en el XSL aparece de lo contrario no.
	 * @exception SeguException	Si la operaci�n falla
	 */
	public void agregarFacultad(String esCveSistema, String esCveFacultad,
			String esDescripcion, String esDescripcionIngles,
			String esBloqueado,
			String esTipo, int elNRestriccion,
			String esPagina, String esPaginaIngles, int eiCveAsignac,
			String esImg, String esSwImg,
			int eiSwQuitaMnu, String esClaveRevisar )
			throws	Exception,	SQLException, SeguException {

		/*if(log.isTraceEnabled()) {
			log.trace("CMttoSeguridad::agregarFacultad()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Clave Facultad: " + esCveFacultad);
			log.trace("Descripcion Fac.: " + esDescripcion);
			log.trace("Descripcion Fac Ingles.: " + esDescripcionIngles);
			log.trace("Bloqueado : " + esBloqueado);
			log.trace("Tipo : " + esTipo);
			log.trace("Restriccion : " + elNRestriccion);
			log.trace("Pagina : " + esPagina);
			log.trace("Pagina Ingles: " + esPaginaIngles);
			log.trace("Asignacion : " + eiCveAsignac);
			log.trace("Imagen : " + esImg);
			log.trace("Sw Imagen : " + esSwImg);
			log.trace("Quita Menu: " + eiSwQuitaMnu);
			log.trace("Clave Revisar: " + esClaveRevisar);
			log.trace("\n");
		}*/

		CFacultad loFacultad = new CFacultad(
				esCveSistema, esCveFacultad,
				esDescripcion, esDescripcionIngles, esBloqueado,
				esTipo, elNRestriccion,
				esPagina, esPaginaIngles, eiCveAsignac,
				esImg, esSwImg,
				eiSwQuitaMnu, esClaveRevisar);
		loFacultad.agregarFacultadInsertDB();

		actualizarFacultad( CConst.ALTA, esCveSistema, esCveFacultad);

	}

	/**
	 * Crea un Perfil, lo graba en la BD, lo registra en el OID,
	 * actualiza el listado en memoria y regenera todos los XML.
	 * El bloqueo independientemente del valor especificado siembre es "S"
	 * en Cperfil.agregarPerfilInsertDB()
	 *
	 * Si existe algun error durante la generaci�n del grupo en el OID
	 * simplemente es ignorado y si no se genero, habria que generarlo
	 * manualmente.
	 *
	 * @param esCvePerfil	Clave del perfil a agregar
	 * @param esBloqueado	Especifica si el perfil est� bloqueado o no
	 * @param eiCveOID	Clave del perfil dentro del OID
	 * @param eiTipoAfiliado	Tipo de afiliado al que est� orientado el perfil
	 * @return Cadena con el mensaje de error (en caso de que hubiese)
	 * 		al tartar de registrar el perfil dentro del OID.
	 *
	 * @exception	SeguException	Si la operaci�n falla
	 */
	public String agregarPerfil( String esCvePerfil, String esBloqueado,
			int eiCveOID, int eiTipoAfiliado)
			throws Exception, SQLException, SeguException {
		/*if(log.isTraceEnabled()) {
			log.trace("SeguridadBean::agregarPerfilProt(E)");
			log.trace("SeguridadBean::agregarPerfilProt()-Clave del Perfil: " + esCvePerfil);
			log.trace("SeguridadBean::agregarPerfilProt()-Bloqueo: " + esBloqueado);
			log.trace("SeguridadBean::agregarPerfilProt()-Clave OID " + eiCveOID);
			log.trace("SeguridadBean::agregarPerfilProt()-Tipo Afiliado " + eiTipoAfiliado);
		}*/
		CPerfil loPerfil = new CPerfil();

		loPerfil.setCvePerfil(esCvePerfil);
		loPerfil.setBloqueado(esBloqueado);
		loPerfil.setClaveOID(eiCveOID);
		loPerfil.setTipoAfiliado(eiTipoAfiliado);

		loPerfil.agregarPerfilInsertDB();

		//log.trace("SeguridadBean::agregarPerfilProt(S)");

		m_oLPerfil.cargarPerfilMem(loPerfil);
		generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-

		String error ="";
		try {
			UtilUsr utilUsr = new UtilUsr();
			error = utilUsr.registrarGrupo(esCvePerfil); //registra el perfil en OID.
		} catch(Exception e) {
			error = "Error de WebService";
			log.error("ERROR:SeguridadBean::agregarPerfil() ", e);
		}
		return error;

	}

	/**
	 * Crea un Perfil Prototipo, lo graba en la BD y
	 * actualiza el listado en memoria.
	 *
	 * @param esCveSistema	???
	 * @param esCvePerfilProt	Clave del perfil prototipo
	 * @param esDescripcion	Descripci�n del perfil prototipo
	 * @param esBloqueado	???
	 * @param eiCveAsignac	???
	 * @exception	SeguException	Si la operaci�n falla
	 */
	public void agregarPerfilProt(String esCveSistema, String esCvePerfilProt,
											String esDescripcion, String esBloqueado,
											int eiCveAsignac)
					throws Exception, SQLException, SeguException
	{
		/*if(log.isTraceEnabled()) {
			log.trace("CMttoSeguridad::agregarPerfilProt()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Clave del Perfil Prototipo: " + esCvePerfilProt);
			log.trace("Descripcion: " + esDescripcion);
			log.trace("Bloqueo: " + esBloqueado);
			log.trace("Clave de asignacion " + eiCveAsignac);
			log.trace("\n");
		}*/
		CPerfilProt loPerfilProt = new CPerfilProt(esCveSistema,esCvePerfilProt,
																 esDescripcion,esBloqueado,
																 eiCveAsignac);
		loPerfilProt.agregarPerfilProtInsertDB();

		actualizarPerfilProt(CConst.ALTA,	esCveSistema,	esCvePerfilProt,
									esDescripcion,	esBloqueado);
	}

	/**
	 * Crea un Sistema, lo graba en la BD y
	 * actualiza el listado en memoria.
	 *
	 * @param esCveSistema	Clave del sistema
	 * @param esDescripcion	Descripci�n del sistema
	 * @param esDescripcionIngles	Descripci�n del sistema en ingl�s
	 * @param esBloqueo	???
 	 * @param esImg	Ruta de la imagen
	 * @param esSwImg	Indicador de si se utilizara la imagen (I) o no (D)
	 * @param esLiga	URL al que redirecciona el sistema
	 * @param eiAccLiga	Acci�n de la liga.
	 * 		0: Abre la pagina en la ventana actual
	 * 		1: Abre la pagina en una ventana nueva
	 * @param esCveSistPadre	Clave del sistema padre (En caso de existir)
	 * @param esQuitaMnu	???
	 * @param esStatus	???
	 * @exception	SeguException	Si la operaci�n falla
	 */
	 public void agregarSistema(String esCveSistema,
	 		String esDescripcion, String esDescripcionIngles,
			String esBloqueo, String esImagen,
			String esSwImg, String esLiga,
			int eiAccLiga, String esCveSistPadre,
			String esQuitaMnu, String esStatus )
			throws Exception, SeguException {

		int liConsecSistema = 0;
		/*if(log.isTraceEnabled()) {
			log.trace("CMttoSeguridad::agregarSistema() .... 2");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Descripcion: " + esDescripcion);
			log.trace("DescripcionIngles: " + esDescripcionIngles);
			log.trace("Bloqueo: " + esBloqueo);
			log.trace("Imagen: " + esImagen);
			log.trace("esSwImg:	 " + esSwImg);
			log.trace("esLiga:		" + esLiga);
			log.trace("eiAccLiga:	" + eiAccLiga);
			log.trace("esCveSistPadre:	" + esCveSistPadre);
			log.trace("esQuitaMnu:	" + esQuitaMnu);
			log.trace("esStatus:	" + esStatus);
			log.trace("\n");
		}*/
		CSistema loSistema = new CSistema(esCveSistema,
				esDescripcion, esDescripcionIngles,
				esBloqueo, esImagen,
				esSwImg, esLiga, eiAccLiga, esQuitaMnu);

		try {
			liConsecSistema = loSistema.agregarSistemaInsertDB(
					esCveSistPadre, m_oLRelSistemas, esStatus);

			loSistema.setOrdenMenu(liConsecSistema);
			m_oLSistema.cargarSistemaMem(loSistema);

			if (esCveSistPadre.length() > 0) {
				CConexionDBPool loCPDB = new CConexionDBPool();
				loCPDB.conectarDB();
				Vector lvTmp = new Vector();
				String lsSQLSelect = m_oLRelSistemas.getSQLCargaRelSist() ;
				int liNumColsRefPP =0;
				m_oLRelSistemas = new CLRelSistema();
				loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvTmp);
				m_oLRelSistemas.cargarRelacionSistemas(lvTmp);
				loCPDB.cerrarDB(true);
			}
		} catch(SQLException es) {
			throw new SeguException(CConst.COT_ERRC_DB);
		}
	}


	/**
	 * Crea un Sistema, lo graba en la BD y
	 * actualiza el listado en memoria.
	 *
	 * @param esCveSistema	Clave del sistema
	 * @param esDescripcion	Descripci�n del sistema
	 * @param esDescripcionIngles	Descripci�n del sistema en ingl�s
	 * @param esBloqueo	???
	 * @param esImagen	???
	 * @param esImg	Ruta de la imagen
	 * @param esSwImg	Indicador de si se utilizara la imagen (I) o no (D)
	 * @param esLiga	URL al que redirecciona el sistema
	 * @param eiAccLiga	Acci�n de la liga.
	 * 		0: Abre la pagina en la ventana actual
	 * 		1: Abre la pagina en una ventana nueva
	 * @param esCveSistPadre	Clave del sistema padre (En caso de existir)
	 * @exception	SeguException	Si la operaci�n falla
	 */
	public void agregarSistema(String esCveSistema,
			String esDescripcion, String esDescripcionIngles,
			String esBloqueo, String esImagen,
			String esSwImg, String esLiga,
			int eiAccLiga, String esCveSistPadre)
			throws Exception, SeguException {

		int liConsecSistema = 0;
		/*if (log.isTraceEnabled()) {
			log.trace("CMttoSeguridad::agregarSistema()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Descripcion: " + esDescripcion);
			log.trace("DescripcionIngles: " + esDescripcionIngles);
			log.trace("Bloqueo: " + esBloqueo);
			log.trace("Imagen: " + esImagen);
			log.trace("esSwImg:	 " + esSwImg);
			log.trace("esLiga:		" + esLiga);
			log.trace("eiAccLiga:	" + eiAccLiga);
			log.trace("esCveSistPadre:	" + esCveSistPadre);
			log.trace("\n");
		}*/
		String lsQuitaMenu = "";
		CSistema loSistema = new CSistema(
				esCveSistema, esDescripcion,
				esDescripcionIngles,
				esBloqueo,esImagen, esSwImg,
				esLiga, eiAccLiga, lsQuitaMenu);

		try {
			liConsecSistema = loSistema.agregarSistemaInsertDB(esCveSistPadre);

			loSistema.setOrdenMenu(liConsecSistema);

			m_oLSistema.cargarSistemaMem(loSistema);

			if (esCveSistPadre.length() > 0) {
				CConexionDBPool loCPDB = new CConexionDBPool();
				loCPDB.conectarDB();
				Vector lvTmp = new Vector();
				String lsSQLSelect = m_oLRelSistemas.getSQLCargaRelSist() ;
				int liNumColsRefPP =0;
				m_oLRelSistemas = new CLRelSistema();
				loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvTmp);
				m_oLRelSistemas.cargarRelacionSistemas(lvTmp);
				loCPDB.cerrarDB(true);
			}
		} catch(SQLException es) {
			throw new SeguException(CConst.COT_ERRC_DB);
		}
	}

	/**
	 * Crea un Usuario, lo graba en la BD y
	 * actualiza el listado en memoria.
	 *
	 * Con la entrada del OID. Este m�todo se vuelve obsoleto
	 * ya que los usuarios son manejados por el OID
	 *
	 * @param esCveLogin	Login del Usuario
	 * @param esCveUsrNafin	Clave Nafin para el usuario
	 * @param esNombre	Nombre del usuario
	 * @param esAPaterno	Apellido paterno del usuario
	 * @param esAMaterno	Apellido materno del usuario
	 * @param esEstatus	???
	 * @param esCorreo	Correo Electr�nico del usuario
	 * @param esContrasena	Contrase�a del usuario
	 * @param esFechVencim	Fecha de vencimiento del usuario
	 * @param eiTipoUsr	Clave del tipo de usuario
	 * @param eiCveEPO	Clave EPO
	 * @param eiCvePyme	Clave Pyme
	 * @param eiCveIF	Clave IF
	 * @param esTipoAfiliacion	???
	 * @param esSIRAC	???
	 * @exception	SeguException	Si la operaci�n falla
	 */
	public void agregarUsuario(String esCveLogin, String esCveUsrNafin, String	esNombre,
									String esAPaterno, String esAMaterno, String esEstatus,
									String esCorreo, String esContrasena, String esFechVencim,
									int eiTipoUsr, int eiCveEPO, int eiCvePYME,
									int eiCveIF, String esTipoAfiliacion, String esSIRAC)
					throws	SeguException
	{
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::agregarUsuario()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave login: " + esCveLogin);
			log.trace("Clave usr nafin: " + esCveUsrNafin);
			log.trace("Nombre: " + esNombre);
			log.trace("ApPaterno: " + esAPaterno);
			log.trace("ApMaterno: " + esAMaterno);
			log.trace("Estatus: " + esEstatus);
			log.trace("Correo: " + esCorreo);
			log.trace("Contrasena: (no se despliega aqui)" );
			log.trace("Fecha de vencimiento " + esFechVencim);
			log.trace("Tipo Usuario: " + eiTipoUsr);
			log.trace("Cve EPO: " + eiCveEPO);
			log.trace("Cve PYME: " + eiCvePYME);
			log.trace("Cve IF: " + eiCveIF);
			log.trace("Tipo Afiliacion: " + esTipoAfiliacion);
			log.trace("SIRAC: " + esSIRAC);
		}*/
		CUsuario loUsuario = new CUsuario();

		loUsuario.setCveUsuario(esCveLogin);
		loUsuario.setCveUsuarioNafin(esCveUsrNafin);
		loUsuario.setNombre(esNombre);
		loUsuario.setAPaterno(esAPaterno);
		loUsuario.setAMaterno(esAMaterno);
		loUsuario.setStatus(esEstatus);
		loUsuario.setCorreo(esCorreo);
		loUsuario.setContrasena(esContrasena);

		loUsuario.setFchVencimiento(esFechVencim);
		loUsuario.setTpUsuario(eiTipoUsr);
		loUsuario.setCveEPO(eiCveEPO);
		loUsuario.setCvePYME(eiCvePYME);
		loUsuario.setCveIF(eiCveIF);
		loUsuario.setTipoAfiliacion(esTipoAfiliacion);

		loUsuario.setSIRAC(esSIRAC);

		try
		{
			loUsuario.agregarUsuarioInsertDB();
			m_oLUsuario.agregarUsuarioMem(loUsuario);
		} catch(SeguException se) {
			throw new SeguException(se.getCodError());
		}
	}

	/**
	 * Asocia Facultades al Perfil Prototipo especificado, tanto en BD
	 * como en memoria. Regenera todos los XML.
	 *
	 * @param esCveSistema	Clave del Sistema
	 * @param esCvePerfilProt	Clave del perfil prototipo
	 * @param evTablaFacPerfProt	Claves de las Facultades
	 *
	 * @exception	SeguException	Si la asociaci�n no es posible
	 */
	public void asociarFacPerfilProt(String esCveSistema, String esCvePerfilProt,
												Vector evTablaFacPerfProt)
					throws	Exception,	SQLException, SeguException
	{
		Vector lvRegistro;

		Integer 	loiNRestriccion ;
		String 	lsCveFacultad;
		String 	lsMancomunidad;
		boolean 	lbExito = true;

		int 		liContador;
		int		liCodError;
		CConexionDBPool loConexion = new CConexionDBPool();

		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::asociarFacPerfilProt()");

			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Clave del Perfil Prototipo: " + esCvePerfilProt);
			log.trace("Un vector:(no se muestra aqui) ");
			log.trace("\n");
		}*/
		try {
			loConexion.conectarDB();
		} catch(Exception ex){
			throw new SeguException(CConst.COT_ERRC_DB);
		}
		try {
			CPerfilProt loPerfilProt = new CPerfilProt();

			//BORRAMOS LA INFORMACION ANTERIOR DE TODOS LOS REGISTROS EN LA TABLA SEG_FACPERFPR
			//DEPENDIENDO DE LA CVESIST, CVPERFILPROT
			loPerfilProt.asociarFacPerfilProtDeleteDB(esCveSistema, esCvePerfilProt,loConexion);
			try{

				//AHORA INSERTAMOS EN LOS MISMOS REGISTROS, PERO CON INFORMACION NUEVA
				for(liContador = 0; liContador < evTablaFacPerfProt.size(); liContador++){
					lvRegistro = (Vector)evTablaFacPerfProt.elementAt(liContador);
 		 			lsCveFacultad = (String)lvRegistro.elementAt(0);
					loiNRestriccion = (Integer)lvRegistro.elementAt(1);
					lsMancomunidad = (String)lvRegistro.elementAt(2);
					loPerfilProt.asociarFacPerfilProtInsertDB(esCveSistema, 	esCvePerfilProt,
																		lsCveFacultad, loiNRestriccion,
																		lsMancomunidad, loConexion );
				}
			} catch(SQLException ex){
				lbExito = false;
				liCodError = ex.getErrorCode();
				log.error("asociarFacPerfilProt(Error) ", ex);
				if(liCodError == CConst.ERRC_REGIST_DUPLICADO)
					throw new SeguException(CConst.SEG_ERRC_SIS_YA_EXIST);
				if(liCodError == CConst.ERRC_INTEGR_REFERENC)
					throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
	 		}
 		} catch(SQLException ex){
			lbExito = false;
 			liCodError = ex.getErrorCode();
			log.error("asociarFacPerfilProt(Error) ", ex);
			log.error("asociarFacPerfilProt(Error) Codigo del error: " + liCodError);
		} finally{
			loConexion.cerrarDB(lbExito);
			actualizarFacPerfilProt(esCveSistema, esCvePerfilProt);
			generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-
		}
	}

	/**
	 * Asocia perfiles prototipo al perfil especificado, tanto en BD
	 * como en memoria. Regenera todos los XML.
	 *
	 * @param esCvePerfil	Clave del perfil
	 * @param evPerfilesProt	Claves de los Perfiles Prototipo
	 *
	 * @exception	SeguException	Si la asociaci�n no es posible
	 */
	public void asociarPerfilPerfilesProt(String esCvePerfil, Vector evPerfilesProt)
					throws	Exception,	SQLException, SeguException
	{
		Vector 	lvRegistro;
		String 	lsCveSistema = "";
		String 	lsCvePerfilProt = "";
		int 		liCodError = 0;
		boolean	lbExito = true;

		int 		liContador;
		CConexionDBPool loConexion = new CConexionDBPool();

		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::asociarPerfilPerfilesProt(E)");
			log.trace("CMttoSeguridad::asociarPerfilPerfilesProt()-Perfil: " + esCvePerfil);
		}*/
		CPerfil loPerfil = new CPerfil();

		try{
			loConexion.conectarDB();

			loPerfil.setCvePerfil(esCvePerfil);
			loPerfil.asociarPerfilPerfilProtDeleteDB(	loConexion);

			try{
				for(liContador = 0; liContador < evPerfilesProt.size(); liContador++){
					lvRegistro = (Vector)evPerfilesProt.elementAt(liContador);
					lsCveSistema = (String)lvRegistro.elementAt(0);
					lsCvePerfilProt = (String)lvRegistro.elementAt(1);

					loPerfil.asociarPerfilPerfilProtInsertDB(lsCveSistema,lsCvePerfilProt,loConexion);
				}

				m_oLPerfil.actualizarPerfilPerfilesProt(esCvePerfil,evPerfilesProt,m_oLSistema);
				generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-
			} catch(SQLException ex){

				lbExito = false;
				liCodError = ex.getErrorCode();
				if(liCodError == CConst.ERRC_REGIST_DUPLICADO)
					throw new SeguException(CConst.SEG_ERRC_SIS_YA_EXIST);
				if(liCodError == CConst.ERRC_INTEGR_REFERENC)
					throw new SeguException(CConst.SEG_ERRC_SIS_INTEG_REFERENCIAL);
			}
		} catch(SQLException ex){
			lbExito = false;
			liCodError = ex.getErrorCode();
			log.error("asociarPerfilPerfilesProt(Error) ", ex);
		}finally{
			loConexion.cerrarDB(lbExito);
		}
			//log.trace("CMttoSeguridad::asociarPerfilPerfilesProt(S)");
	}

	/**
	 * Bloquea una Facultad registr�ndolo �nicamente en la BD.
	 *
	 * @param esCveSistema	Clave del sistema
	 * @param esCveFacultad	Clave de la facultad
	 * @param esBloqueo	???
	 *
	 * @exception	SeguException	Si el bloqueo no es posible
	 */
	public void bloquearFacultad(	String esCveSistema, 	String esCveFacultad,
											String esBloqueo)
				throws 	Exception, 	SQLException, SeguException

	{
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::bloquearFacultad()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Clave Facultad: " + esCveFacultad);
			log.trace("Bloqueo: " + esBloqueo);
			log.trace("\n");
		}*/
		CFacultad loFacultad = new CFacultad();
		loFacultad.bloquearFacultadUpdateDB(esCveSistema, esCveFacultad, esBloqueo);
	}

	/**
	 * Bloquea un Perfil Prototipo
	 *
	 * @param esCveSistema	Clave del sistema
	 * @param esCvePerfilProt	Clave del perfil prototipo
	 * @param esBloqueo	???
	 *
	 * @exception	SeguException	Si el bloqueo no es posible
	 */
	public void bloquearPerfilProt(	String 	esCveSistema, 	String esCvePerfilProt,
												String 	esBloqueado)
					throws	SeguException,		Exception,
								SQLException
	{
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::bloquearPerfilProt()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Clave del Perfil Prototipo: " + esCvePerfilProt);
			log.trace("Bloqueo: " + esBloqueado);
			log.trace("\n");
		}*/
		CPerfilProt loPerfilProt = new CPerfilProt();
		loPerfilProt.bloquearPerfilProtUpdateDB(esCveSistema, esCvePerfilProt,
				esBloqueado);
	}

	/**
	 * Bloquea un Sistema, tanto en BD como en memoria.
	 * Regenera todos los XML.
	 *
	 * @param esCveSistema	Clave del sistema
	 * @param esBloqueo	???
	 *
	 * @exception	SeguException	Si el bloqueo no es posible
	 */
	public void bloquearSistema(	String 	esCveSistema, 	String 	esBloqueo)
				throws 	Exception,	SeguException
	{
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::bloquearSistema()");
			log.trace("\tParametros recibidos:\n ");
	 		log.trace("Clave del sistema: " + esCveSistema);
		 	log.trace("Bloqueo: " + esBloqueo);
			log.trace("\n");
		}*/
		CSistema loSistema = new CSistema();

		try {
			loSistema.bloquearSistemaUpdateDB(esCveSistema, esBloqueo);
			m_oLSistema.bloquearSistemaMem(esCveSistema, esBloqueo);
			generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-

		} catch(SQLException es) {
			throw new SeguException(CConst.COT_ERRC_DB);
		}
	}

	/**
	 * Elimina la Facultad de la BD y de la lista
	 * de Facultades en memoria. Regenera todos los XML.
	 *
	 * @param esCveSistema	Clave del Sistema
	 * @param esCveFacultad	Clave de la Facultad
	 *
	 * @exception	SeguException	Si la eliminaci�n no es posible
	 */
	public void borrarFacultad( String esCveSistema, String esCveFacultad)
			throws Exception, SQLException, SeguException {
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::borrarFacultad()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Clave Facultad: " + esCveFacultad);
			log.trace("\n");
		}*/
		CFacultad loFacultad = new CFacultad();
		loFacultad.borrarFacultadDeleteDB(esCveSistema, esCveFacultad);

		actualizarFacultad(CConst.BAJA, esCveSistema, esCveFacultad);
		generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-

	}

	/**
	 * Elimina el Perfil (y sus XML asociados) de la BD y de la lista
	 * de Perfiles en memoria.
	 *
	 * @param esCvePerfil	Clave del perfil
	 *
	 * @exception	SeguException	Si la eliminaci�n no es posible
	 */
	public void borrarPerfil(	String esCvePerfil)
			throws SeguException, Exception,
			SQLException {

		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::borrarPerfil(E)");
			log.trace("CMttoSeguridad::borrarPerfil()-Perfil: " + esCvePerfil);
		}*/
		CPerfil loPerfil = new CPerfil();
		loPerfil.setCvePerfil(esCvePerfil);

		// Al borrar en la base de datos, se borra tambi�n el XML de seg_xml_x_perfil
		// mediante el uso de un CASCADE en la relaci�n
		loPerfil.borrarPerfilDeleteDB();

		log.trace("CMttoSeguridad::borrarPerfil(S)");

		m_oLPerfil.borrarPerfilMem(esCvePerfil);
	}

	/**
	 * Elimina el Perfil Prototipo de la BD y de la lista
	 * de Perfiles Protipo en memoria. Regenera todos los XML.
	 *
	 * @param esCveSistema	Clave del sistema
	 * @param esCvePerfilProt	Clave del perfil prototipo
	 *
	 * @exception	SeguException	Si la eliminaci�n no es posible
	 */
	public void borrarPerfilProt(	String 	esCveSistema, String esCvePerfilProt)
					throws	SeguException,		Exception,
								SQLException
	{
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::borrarPerfilProt()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Clave del Perfil Prototipo: " + esCvePerfilProt);
			log.trace("\n");
		}*/
		CPerfilProt loPerfilProt = new CPerfilProt();
		loPerfilProt.borrarPerfilProtDeleteDB(esCveSistema, esCvePerfilProt);

		actualizarPerfilProt(CConst.BAJA, esCveSistema, esCvePerfilProt, "", "");
		generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-
	}

	/**
	 * Elimina el Sistema de la BD y de la lista de sistemas en memoria.
	 * Regenera todos los XML.
	 *
	 * @param esCveSistema	Clave del sistema
	 *
	 * @exception	SeguException	Si la eliminaci�n no es posible
	 */
	public void borrarSistema(	String 	esCveSistema)
					throws	SeguException,		Exception {

		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::borrarSistema()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("\n");
		}*/
		CSistema loSistema = new CSistema();

		try {
			loSistema.borrarSistemaDeleteDB(esCveSistema);
			m_oLSistema.borrarSistemaMem(esCveSistema);
			generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-
		} catch(SQLException es) {
			throw new SeguException(CConst.COT_ERRC_DB);
		}

	}

	/**
	 * Bloquea un sistema en memoria. (Al parecer este m�todo no se emplea)
	 * Regenera todos los XMLs.
	 *
	 * @param ssCveSistema	Clave del sistema
	 * @param ssCvePerfil	Clave del perfil
	 * @param ssBloqueado	???
	 */
	public void cambiarDato(String ssCveSistema, String ssCvePerfil,
			String ssBloqueado) throws SeguException {

		m_oLSistema.cambiarDato(ssCveSistema, ssCvePerfil, ssBloqueado);
		generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-
	}

	/**
	 * Cierra la sesion y realiza labores de liberaci�n de recursos
	 * para el usuario especificado
	 *
	 * @param esCveUsuario	Clave del usuario
	 */
		public void cerrarSession(String esCveUsuario)
	{
		CUsuario loUsuario = null;
		//log.trace("\tSeguridadBean::cerrarSession()");
		loUsuario = m_oLUsuario.buscarUsuario(esCveUsuario);

		if (loUsuario != null)
		{
			loUsuario.limpiarSession();
		}
	}

	/**
	 * Consulta de Facultades por Perfil Prototipo
	 *
	 * @param esCveSistema	Clave del sistema
	 * @param esCvePerfilProt	Clave del perfil prototipo
	 *
	 * @return	???
	 *
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Hashtable consultarFacxPerfilProt(String	esCveSistema,	String 	esCvePerfilProt)
					throws	Exception,	SQLException, SeguException
	{
		Hashtable htFacxPerfProt = new Hashtable();
					 /*if (log.isTraceEnabled()){
							log.trace("CMttoSeguridad::consultarFacxPerfilProt()");
							log.trace("\tParametros recibidos:\n ");
							log.trace("Clave del sistema: " + esCveSistema);
							log.trace("Clave del Perfil Prototipo: " + esCvePerfilProt);
							log.trace("\n");
					 }*/
		CPerfilProt loPerfilProt = new CPerfilProt();

		htFacxPerfProt = loPerfilProt.consultarFacxPerfilProtSelectDB(esCveSistema,
																							esCvePerfilProt);
		return htFacxPerfProt;
	}

	/**
	 * Consulta de Facultades por Sistema
	 *
	 * @param esCveSistema	Clave del sistema
	 *
	 * @return	Vector con los datos de las facultades:
 	 *		0 Clave del sistema
	 *		1 Clave de la facultad
	 *		2 Descripcion
	 *		3 Indicador de bloqueo
	 *		4 Tipo de facultad
	 *		5 Restriccion
	 *		6 Pagina
	 *		7 ???
	 *		8 ???
	 *		9 ???
	 *		10 ???
	 *		11 ???
	 *		12 Imagen a mostrar (cuando aplique)
	 *		13 indicador de uso de imagen o texto
	 *		14 Descripcion en idioma ingles
	 *		15 Pagina para idioma ingles

	 *
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Vector consultarFacxSistema(	String 	esCveSistema	)
						throws Exception, SQLException, SeguException
	{
					 /*if (log.isTraceEnabled()){
								log.trace("CMttoSeguridad::consultarFacxSistema()");
								log.trace("\tParametros recibidos:\n ");
								log.trace("Clave del sistema: " + esCveSistema);
								log.trace("\n");
					 }*/
		Vector lvDatosFacxSistema = new Vector();
		CSistema loSistema = new CSistema();
		lvDatosFacxSistema = loSistema.consultarFacxSistema(esCveSistema);

		return lvDatosFacxSistema;
	}

	/**
	 * Consulta de Perfiles de la BD
	 *
	 * @return	Vector con la informaci�n del perfil:
	 * 		[0] Clave del perfil
	 *		[1] Estatus de Bloqueo
	 *		[2] Clave asignada al perfil dentro del OID
	 *		[3] Tipo de afiliado al que est� orientado el perfil
	 *
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Vector consultarPerfiles()
						throws 	Exception,	SQLException,
									SeguException
	{
		Vector 		lvDatosPerfil;
		//log.trace("CMttoSeguridad::consultarPerfiles(E)");

		CPerfil	loPerfil = new CPerfil();
		lvDatosPerfil= loPerfil.consultarPerfilesSelectDB();

			//log.trace("CMttoSeguridad::consultarPerfiles(S)");

		return lvDatosPerfil;
	}

	/**
	 * Consulta de Perfiles de la BD, filtrando por tipo de afiliado.
	 * Es decir muestra los perfiles asociados a cierto tipo de afiliado.
	 *
	 *
	 * @param esTipoAfiliado Clave del tipo de afiliado:
	 * 		1. EPO, 2. IF, 4. NAFIN
	 *
	 * @return	Lista de perfiles.
	 *
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Vector consultarPerfiles(int eiTipoAfiliado)
						throws 	Exception,	SQLException,
									SeguException {
		Vector lvDatosPerfil;
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::consultarPerfiles(E)");
			log.trace("CMttoSeguridad::consultarPerfiles()-eiTipoAfiliado:" + eiTipoAfiliado);
		}*/
		CPerfil	loPerfil = new CPerfil();
		loPerfil.setTipoAfiliado(eiTipoAfiliado);

		lvDatosPerfil = loPerfil.consultarPerfilesXAsignacSelectDB();
		//log.trace("CMttoSeguridad::consultarPerfiles(S)");

		return lvDatosPerfil;
	}




	/**
	 * Consulta de Perfiles Prototipo de la BD
	 *
	 * @return	???
	 *
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Vector consultarPerfilesProt()
						throws 	Exception,	SQLException,
									SeguException
	{
		Vector 		lvDatosPerfProt;
					 /*if (log.isTraceEnabled()){
						log.trace("CMttoSeguridad::consultarPerfilesProt()");
						log.trace("\tEste metodo no recibe parametros:\n ");
						log.trace("\n");
					 }*/
		CPerfilProt	loPerfilProt = new CPerfilProt();
		lvDatosPerfProt = loPerfilProt.consultarPerfilesProtSelectDB();
		return lvDatosPerfProt;
	}

	/**
	 * Consulta de Perfiles Prototipo por Perfil
	 *
	 * @param esCvePerfil	Clave del perfil
	 *
	 * @return	???
	 *
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Hashtable consultarPerfilesProtXPerfil(String esCvePerfil)
						throws 	Exception,	SQLException,
									SeguException
	{
		Hashtable	lhtDatosPerfProt;
		//log.trace("CMttoSeguridad::consultarPerfilesProtXPerfil(E)");

		CPerfil	loPerfil = new CPerfil();
		loPerfil.setCvePerfil(esCvePerfil);
		lhtDatosPerfProt = loPerfil.consultarPerfPrXPerfilSelectDB();

		//log.trace("CMttoSeguridad::consultarPerfilesProtXPerfil(S)");

		return lhtDatosPerfProt;
	}

	/**
	 * Consulta de Perfiles por Sistema
	 *
	 * @return	???
	 *
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Vector consultarPerfilesPxSistema(String esCveSistema)
						throws 	Exception,	SQLException,
									SeguException
	{
		Vector 		lvPerfilesPxSist ;
					 /*if (log.isTraceEnabled()){
					log.trace("CMttoSeguridad::consultarPerfilesPxSistema()");
					log.trace("\tParametros recibidos:\n ");
					log.trace("Clave del Sistema: " + esCveSistema);
					log.trace("\n");
					 }*/
		CUsuario loUsuario = new CUsuario();
		lvPerfilesPxSist = loUsuario.consultarPerfilesPxSistemaDB(esCveSistema);

		return lvPerfilesPxSist ;
	}

	/**
	 * Consulta de Perfiles por Usuarios
	 *
	 * @return	???
	 *
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Vector consultarPerfilesXUsuario(String esCveUsuario)
							throws 	Exception,	SQLException,
										SeguException
	{
		Vector 	lvPerfilesPxUsuario ;

		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::consultarPerfilesPxUsuario()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del Usuario: " + esCveUsuario);
			log.trace("\n");
		}*/
		CUsuario loUsuario = new CUsuario();
		lvPerfilesPxUsuario = loUsuario.consultarPerfilesXUsuarioSelectDB(esCveUsuario);

		return lvPerfilesPxUsuario ;
	}

	/**
	 * Consulta de Sistemas en la BD.
	 *
	 * @return	Vector con las lista de los sistemas:
	 * 		0. Clave del sistema
	 * 		1. Descripcion en espa�ol
	 * 		2. Indicador de bloqueo
	 * 		3. Ruta de la imagen
	 * 		4. Indicador de orden
	 * 		5. Accion de la liga (Ventana Arriba, Ventana Adentro)
	 * 		6. URL al que hace referencia el sistema
	 * 		7. Identificador de si se usa la imagen o el texto.
	 * 		8. Sistema padre si existe
	 * 		9. ??b_menu
	 * 		10. Nivel
	 * 		11. ??estatus
	 * 		12. Descripcion en Ingles
	 *
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Vector consultarSistemas()
			throws 	Exception,	SQLException, SeguException {

		Vector lvDatosSistemas;
		/*if (log.isTraceEnabled()) {
			log.trace("CMttoSeguridad::consultarSistemas()");
			log.trace("\tEste metodo no recibe parametros:\n ");
			log.trace("\n");
		}*/
		CSistema loSistema = new CSistema();
		lvDatosSistemas = loSistema.consultarSistemasSelectDB();
		return lvDatosSistemas;
	}

	/**
	 * Consulta de usuarios en la BD.
	 *
	 * @param eiTipoUsr	Tipo de Usuario
	 * @param eiCveEPO	Clave EPO
	 * @param eiCveIF	Clave IF
	 * @param eiCvePYME	ClavePYME
	 * @param esNombre	Nombre del usuario
	 * @param esApPaterno	Apellido Paterno
	 * @param esApMaterno	Apellido Materno
	 * @param esCveUsuario	Clave del usuario
	 *
	 * @return	???
	 * @exception	SeguException	Si la consulta falla.
	 */
	public Vector consultarUsuarios(	int eiTipoUsr, 		int eiCveEPO,
												int eiCveIF,			int eiCvePYME,
												String esNombre,		String esApPaterno,
												String esApMaterno,	String esCveUsuario)
						throws 	Exception,	SQLException,
									SeguException
	{
		Vector 		lvDatosUsuarios ;
					 /*if (log.isTraceEnabled()){
					log.trace("CMttoSeguridad::consultarUsuarios()");
					log.trace("\tParametros recibidos:\n ");
					log.trace("Este metodo no recibe parametros");
					log.trace("\n");
					 }*/
		CUsuario loUsuario = new CUsuario();
		lvDatosUsuarios =
					loUsuario.consultarUsuariosSelectDB(	eiTipoUsr, 	eiCveEPO,
																		eiCveIF,		eiCvePYME,
																		esNombre,	esApPaterno,
																		esApMaterno,esCveUsuario);
		return lvDatosUsuarios;
	}

	/**
	 * Carga los datos en memoria de Sistemas, Facultades, Facultades por
	 *	perfiles prototipo, perfiles prototipo, Relaci�n de sistemas,
	 *	Perfiles y Perfiles Prototipo por Perfil. La funcion debe ser
	 *	ejecutada una sola ocasi�n al iniciar el servidor de aplicacion.
	 *
	 * @exception	SeguException	Si falla el proceso.
	 */
	public void crearDatos() throws SeguException
	{
		log.info("---------- Iniciando la carga de datos del modulo de seguridad ----------");
		boolean	lbResultado = true;
		CConexionDBPool loCPDB = new CConexionDBPool();
		int liNumColsSist = 3;
		int liNumColsFac = 15;
		int liNumColsPP = 10;
		int liNumColsRefPP = 3;
		Vector lvTmp = null;
		Vector lvTmp2 = null;
		String lsSQLSelect;

		try{

			loCPDB.conectarDB();

			m_oLSistema = new CLSistema();
			m_oLEmpresa = new CLEmpresa();
			m_oLUsuario = new CLUsuario();
			m_oLPerfil = new CLPerfil();
			m_oLRelSistemas = new CLRelSistema();

			lsSQLSelect = m_oLSistema.getSQLCargaSistemas() ;
			lvTmp = new Vector();
			loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsSist,lvTmp);
			m_oLSistema.cargarSistemas(lvTmp);
			log.debug(m_oLSistema.size() + "Sistemas cargados");

			lsSQLSelect = m_oLSistema.getSQLCargaFac() ;
			lvTmp = new Vector();
			loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsFac,lvTmp);
			m_oLSistema.cargarFacultades(lvTmp);

			lsSQLSelect = m_oLSistema.getSQLCargaFacPerfProt() ;
			lvTmp = new Vector();
			loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsPP,lvTmp);
			lsSQLSelect = m_oLSistema.getSQLCargaPerfProt() ;
			lvTmp2 = new Vector();
			loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsPP,lvTmp2);
			m_oLSistema.cargarPerfilesProt(lvTmp,lvTmp2);

			lvTmp = new Vector();
			lsSQLSelect = m_oLRelSistemas.getSQLCargaRelSist() ;
			loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvTmp);
			m_oLRelSistemas.cargarRelacionSistemas(lvTmp);
			log.debug(m_oLRelSistemas.size() + "Relaci�n de Sistemas cargados");

			lvTmp = new Vector();
			lsSQLSelect = m_oLPerfil.getSQLCargaPerfiles() ;
			loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvTmp);
			m_oLPerfil.cargarPerfiles(lvTmp);

			lvTmp = new Vector();
			lsSQLSelect = m_oLPerfil.getSQLCargaPerfilPerfPr() ;
			loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvTmp);
			m_oLPerfil.cargarRefPerfilesProt(lvTmp,m_oLSistema);

			generarXMLPorPerfil();


			log.info("----------------------------------------------------------------------");
		}catch (SQLException e) {
			lbResultado = false;
							log.error("SeguridadBean::crearDatos()", e);
			throw new SeguException("SeguridadBean::crearDatos()(ERROR EN LA BASE DE DATOS)");
		}catch (NullPointerException e) {
					 	log.error("SeguridadBean::crearDatos()", e);
			throw new SeguException("SeguridadBean::crearDatos()(ERROR AL CARGAR UNA LISTA)");
		}catch (ArrayIndexOutOfBoundsException e){
							log.error("SeguridadBean::crearDatos()",e);
			throw new SeguException("SeguridadBean::crearDatos()(ERROR EN INDICE DE UN ARREGLO)");
		}catch (NumberFormatException e){
							log.error("SeguridadBean::crearDatos()",e);
			throw new SeguException("SeguridadBean::crearDatos()(ERROR DE CONVERSION DE DATOS)");
		}catch (Exception e) {
			e.printStackTrace();
			lbResultado = false;
					 	log.error("SeguridadBean::crearDatos()",e);
			throw new SeguException("SeguridadBean::crearDatos()(ERROR DESCONOCIDO)");
		}
		finally{
			loCPDB.cerrarDB(lbResultado);
		}
	}


	/**
	 * Modifica los datos de una Facultad en BD y memoria.
	 * Regenera todos los XML.
	 *
	 * @param esCveSistema	Clave del sistema
	 * @param esCveFacultad	Clave de la facultad
	 * @param esDescripcion	Descripci�n de la facultad
	 * @param esDescripcionIngles	Descripci�n de la facultad en ingles
	 * @param esBloqueo	???
	 * @param esTipo	???
	 * @param elNRestriccion	???
	 * @param esPagina	URL correspondiente a la facultad
	 * @param esPaginaIngles	URL correspondiente a la facultad en idioma Ingles
	 * @param eiCveAsignac	???
	 * @param esImg	Ruta de la imagen
	 * @param esSwImg	Indicador de si se utilizara la imagen (I) o no (D)
	 * @param eiSwQuitaMnu	???
	 * @param esClaveRevisar Clave que permite establecer si una facultad aparece o no
	 * 		de acuerdo a que si concuerda con el parametro que se envia al XSL o no.
	 *
	 * @exception	SeguException	Si la modificaci�n falla.
	 */
	public void modificarFacultad(
			String esCveSistema, String esCveFacultad,
			String esDescripcion, String esDescripcionIngles,
			String esBloqueo, String esTipo, int elNRestriccion,
			String esPagina, String esPaginaIngles, int eiCveAsignac,
			String esImg, String esSwImg, int eiSwQuitaMnu,
			String esClaveRevisar)
			throws 	Exception, 	SQLException, SeguException {
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::modificarFacultad()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
 			log.trace("Clave Facultad: " + esCveFacultad);
 			log.trace("Descripcion: " + esDescripcion);
 			log.trace("Bloqueo: " + esBloqueo);
 			log.trace("Tipo: " + esTipo);
 			log.trace("Restriccion: " + elNRestriccion);
 			log.trace("Pagina: " + esPagina);
			log.trace("Clave de asignacion: " + eiCveAsignac);
			log.trace("Imgagen : " + esImg);
			log.trace("Sw Imagen : " + esSwImg);
			log.trace("eiSwQuitaMnu : " + eiSwQuitaMnu);
			log.trace("esClaveRevisar : " + esClaveRevisar);
			log.trace("\n");
		}*/
		CFacultad loFacultad = new CFacultad(
				esCveSistema, esCveFacultad,
				esDescripcion, esDescripcionIngles,
				esBloqueo, esTipo, elNRestriccion,
				esPagina, esPaginaIngles,
				eiCveAsignac, esImg, esSwImg,
				eiSwQuitaMnu, esClaveRevisar);
		loFacultad.modificarFacultadUpdateDB();

		actualizarFacultad(	CConst.CAMBIO, 	esCveSistema,	esCveFacultad);
		generarXMLPorPerfil();		//Agregado 04/09/2003 -GEAG-
		//System.out.println("TRACE::modificarFacultad(). XML por Perfil regenerados");
	}

	/**
	 * Modifica los datos del perfil en BD y memoria.
	 * Regenera todos los XML.
	 *
	 * @param esCvePerfil	Clave del perfil
	 * @param esBloqueado	Determian si el perfil esta bloqueado o no
	 * @param eiCveOID Clave asignada al perfil dentro del OID
	 * @param eiTipoAfiliado Tipo de afiliado al que est� orientado el perfil
	 *
	 * @exception	SeguException	Si la modificaci�n falla.
	 */
	public void modificarPerfil(String 	esCvePerfil,
			String 	esBloqueado, int eiCveOID, int eiTipoAfiliado)
					throws	Exception,	SQLException,	SeguException
	{
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::modificarPerfil(E)");

			log.trace("CMttoSeguridad::modificarPerfil()-Perfil: " + esCvePerfil);
			log.trace("CMttoSeguridad::modificarPerfil()-Bloqueo: " + esBloqueado);
			log.trace("CMttoSeguridad::modificarPerfil()-ClaveOID: " + eiCveOID);
			log.trace("CMttoSeguridad::modificarPerfil()-TipoAfiliado: " + eiTipoAfiliado);
			log.trace("\n");
		}*/
		CPerfil loPerfil = new CPerfil();
		loPerfil.setCvePerfil(esCvePerfil);
		loPerfil.setBloqueado(esBloqueado);
		loPerfil.setClaveOID(eiCveOID);
		loPerfil.setTipoAfiliado(eiTipoAfiliado);

		loPerfil.modificarPerfilUpdateDB();

		m_oLPerfil.modificarPerfilMem(esCvePerfil,
								esBloqueado,
								eiCveOID,
								eiTipoAfiliado);
		generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-
		//System.out.println("TRACE::modificarPerfil(). XML por Perfil regenerados");
		/*if (log.isTraceEnabled())
			log.trace("CMttoSeguridad::modificarPerfil(S)");*/
	}

	/**
	 * Modifica los datos del perfil prototipo
	 * Regenera todos los XML.
	 *
	 * @param esCveSistema	Clave del Sistema
	 * @param esCvePerfilProt	Clave del perfil prototipo
	 * @param esDescripcion	Descripci�n del perfil prototipo
	 * @param esBloqueado	???
	 * @param eiCveAsignac	???
	 *
	 * @exception	SeguException	Si la modificaci�n falla.
	 */
	public void modificarPerfilProt(String		esCveSistema,	String 	esCvePerfilProt,
													String 	esDescripcion,	String 	esBloqueado,
													int 		eiCveAsignac)
					throws	Exception, 	SQLException,	SeguException
	{
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::modificarPerfilProt()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Clave del Perfil Prototipo: " + esCvePerfilProt);
			log.trace("Descripcion: " + esDescripcion);
			log.trace("Bloqueo: " + esBloqueado);
			log.trace("Clave de la asignacion: " + eiCveAsignac);
			log.trace("\n");
		}*/
		CPerfilProt loPerfilProt = new CPerfilProt(esCveSistema,esCvePerfilProt,
																 esDescripcion,esBloqueado,eiCveAsignac);
		loPerfilProt.modificarPerfilProtUpdateDB();

		actualizarPerfilProt(CConst.CAMBIO,	esCveSistema,	esCvePerfilProt,
									esDescripcion,	esBloqueado);
		generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-
		//System.out.println("TRACE::modificarPerfilProt(). XML por Perfil regenerados");
	}

	/**
	 * Modifica los datos del sistema en BD y Memoria.
	 * Regenera todos los XML.
	 *
	 * @param esCveSistema	Clave del Sistema
	 * @param esDescripcion	Descripci�n
	 * @param esDescripcionIngles	Descripci�n del sistema en ingl�s
	 * @param esBloqueo	???
	 * @param esImg	Ruta de la imagen
	 * @param esSwImg	Indicador de si se utilizara la imagen (I) o no (D)
	 * @param esLiga	URL al que redirige el sistema
	 * @param eiAccLiga	Acci�n de la liga.
	 * 		0: Abre la pagina en la ventana actual
	 * 		1: Abre la pagina en una ventana nueva
	 * @param esCveSistPadre	Clave del Sistema padre (En caso que exista)
	 * @param esQuitaMenu	???
	 * @param esEstatus	???
	 *
	 * @exception	SeguException	Si la modificaci�n falla.
	 */
	public void modificarSistema( String  esCveSistema,
			String esDescripcion, String esDescripcionIngles,
			String esBloqueo, String esImagen,
			String esSwImg, String esLiga,
			int eiAccLiga, String esCveSistPadre,
			String esQuitaMnu, String esStatus)
			throws	Exception,	SQLException, SeguException {

		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::modificarSistema()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Descripcion: " + esDescripcion);
			log.trace("DescripcionIngles: " + esDescripcionIngles);
			log.trace("Bloqueo: " + esBloqueo);
			log.trace("Imagen: " + esImagen);
			log.trace("esSwImg:	 " + esSwImg);
			log.trace("esLiga:		" + esLiga);
			log.trace("eiAccLiga:	" + eiAccLiga);
			log.trace("esCveSistPadre:	" + esCveSistPadre);
			log.trace("esQuitaMnu	:	" + esQuitaMnu);
			log.trace("esStatus	:	" + esStatus);
		}*/

		CSistema loSistemaAct = null;

		CSistema loSistema = new CSistema(
				esCveSistema, esDescripcion,
				esDescripcionIngles,
				esBloqueo, esImagen, esSwImg, esLiga,
				eiAccLiga, esQuitaMnu);

		//Guarda los cambios en BD
		loSistema.modificarSistemaUpdateDB(
				esCveSistPadre, m_oLRelSistemas, esStatus);


		//Realiza los cambios en Memoria
		loSistemaAct = m_oLSistema.buscarSistema(esCveSistema);

		loSistemaAct.setDescripcion(esDescripcion);
		loSistemaAct.setDescripcionIngles(esDescripcionIngles);
		loSistemaAct.setBloqueado(esBloqueo);
		loSistemaAct.setImagen(esImagen);
		loSistemaAct.setSwImg(esSwImg);
		loSistemaAct.setLiga(esLiga);
		loSistemaAct.setAccionLiga(eiAccLiga);
		loSistemaAct.setQuitaMnu(esQuitaMnu);

		CConexionDBPool loCPDB = new CConexionDBPool();
		loCPDB.conectarDB();
		Vector lvTmp = new Vector();
		String lsSQLSelect = m_oLRelSistemas.getSQLCargaRelSist() ;
		int liNumColsRefPP = 0;
		m_oLRelSistemas = new CLRelSistema();
		loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvTmp);
		m_oLRelSistemas.cargarRelacionSistemas(lvTmp);
		loCPDB.cerrarDB(true);
		generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-
		//System.out.println("TRACE::modificarSistema(1). XML por Perfil regenerados");
	}

	/**
	 * Modifica los datos del sistema en BD y Memoria.
	 * Regenera todos los XML.
	 *
	 * @param esCveSistema	Clave del Sistema
	 * @param esDescripcion	Descripci�n
	 * @param esDescripcionIngles	Descripci�n en Ingl�s
	 * @param esBloqueo	???
	 * @param esImg	Ruta de la imagen
	 * @param esSwImg	Indicador de si se utilizara la imagen (I) o no (D)
	 * @param esLiga	URL al que redirige el sistema
	 * @param eiAccLiga	Acci�n de la liga.
	 * 		0: Abre la pagina en la ventana actual
	 * 		1: Abre la pagina en una ventana nueva
	 * @param esCveSistPadre	Clave del Sistema padre (En caso que exista)
	 *
	 * @exception	SeguException	Si la modificaci�n falla.
	 */
	public void modificarSistema(String esCveSistema,
			String esDescripcion, String esDescripcionIngles,
			String esBloqueo, String esImagen,
			String esSwImg, String esLiga,
			int eiAccLiga, String esCveSistPadre)
			throws	Exception,	SQLException, SeguException {

		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::modificarSistema()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave del sistema: " + esCveSistema);
			log.trace("Descripcion: " + esDescripcion);
			log.trace("DescripcionIngles: " + esDescripcionIngles);
			log.trace("Bloqueo: " + esBloqueo);
			log.trace("Imagen: " + esImagen);
			log.trace("esSwImg:	 " + esSwImg);
			log.trace("esLiga:		" + esLiga);
			log.trace("eiAccLiga:	" + eiAccLiga);
			log.trace("esCveSistPadre:	" + esCveSistPadre);
			log.trace("\n");
		}*/

		String lsQuitaMenu = "";
		CSistema loSistemaAct = null;

		CSistema loSistema = new CSistema(
				esCveSistema, esDescripcion,
				esDescripcionIngles,
				esBloqueo,esImagen,
				esSwImg,esLiga,eiAccLiga, lsQuitaMenu);

		loSistema.modificarSistemaUpdateDB(esCveSistPadre);

		loSistemaAct = m_oLSistema.buscarSistema(esCveSistema);

		loSistemaAct.setDescripcion(esDescripcion);
		loSistemaAct.setDescripcionIngles(esDescripcionIngles);
		loSistemaAct.setBloqueado(esBloqueo);
		loSistemaAct.setImagen(esImagen);
		loSistemaAct.setSwImg(esSwImg);
		loSistemaAct.setLiga(esLiga);
		loSistemaAct.setAccionLiga(eiAccLiga);

		CConexionDBPool loCPDB = new CConexionDBPool();
		loCPDB.conectarDB();
		Vector lvTmp = new Vector();
		String lsSQLSelect = m_oLRelSistemas.getSQLCargaRelSist() ;
		int liNumColsRefPP = 0;
		m_oLRelSistemas = new CLRelSistema();
		loCPDB.ejecutarQueryDB(lsSQLSelect,liNumColsRefPP,lvTmp);
		m_oLRelSistemas.cargarRelacionSistemas(lvTmp);
		loCPDB.cerrarDB(true);
		generarXMLPorPerfil();		//Agregado 04/09/2003 -GAP-
		//System.out.println("TRACE::modificarSistema(2). XML por Perfil regenerados");

	}

	/**
	 * Modifica los datos del usuario.
	 *
	 * Con la entrada del OID. Este m�todo se vuelve obsoleto
	 * ya que los usuarios son manejados por el OID
	 *
	 *
	 * @param esCveLogin	Login del usuario
	 * @param esCveUsrNafin	Clave Nafin del usuario
	 * @param esNombre	Nombre
	 * @param esAPaterno	Apellido Paterno
	 * @param esAMaterno	Apellido Materno
	 * @param esEstatus	Estatus
	 * @param esCorreo	Direcci�n de Correo Electr�nica
	 * @param esContrasena	Constrase�a
	 * @param esFechVencim	Fecha de vencimiento de la cuenta
	 * @param eiTipoUsr	Clave del tipo de usuario
	 * @param eiCveEPO	Clave EPO
	 * @param eiCvePYME	Clave Pyme
	 * @param eiCveIF	Clave IF
	 * @param esTipoAfiliacion	Tipo de afiliaci�n
	 * @param esSIRAC	N�mero SIRAC
	 *
	 * @exception	SeguException	Si la modificaci�n falla.
	 */
	public void modificarUsuario(	String 	esCveLogin, 	String	esCveUsrNafin,	String	esNombre,
									String 	esAPaterno,		String	esAMaterno,		String 	esEstatus,
									String 	esCorreo,		String 	esContrasena, 	String 	esFechVencim,
									int	 	eiTipoUsr,		int 		eiCveEPO,	int		eiCvePYME,
									int		eiCveIF,		String	esTipoAfiliacion,String esSIRAC)
					throws	SQLException, SeguException
	{
		/*if (log.isTraceEnabled()){
			log.trace("CMttoSeguridad::agregarUsuario()");
			log.trace("\tParametros recibidos:\n ");
			log.trace("Clave login: " + esCveLogin);
			log.trace("Clave usr nafin: " + esCveUsrNafin);
			log.trace("Nombre: " + esNombre);
			log.trace("ApPaterno: " + esAPaterno);
			log.trace("ApMaterno: " + esAMaterno);
			log.trace("Estatus: " + esEstatus);
			log.trace("Correo: " + esCorreo);
			log.trace("Contrasena: (no se despliega aqui)" );
			log.trace("Fecha de vencimiento " + esFechVencim);
			log.trace("Tipo Usuario: " + eiTipoUsr);
			log.trace("Cve EPO: " + eiCveEPO);
			log.trace("Cve PYME: " + eiCvePYME);
			log.trace("Cve IF: " + eiCveIF);
			log.trace("Tipo Afiliacion: " + esTipoAfiliacion);
			log.trace("SIRAC: " + esSIRAC);
		}*/
		CUsuario loUsuario = new CUsuario();
		CUsuario loUsuarioTmp = null;

		loUsuario.setCveUsuario(esCveLogin);
		loUsuario.setCveUsuarioNafin(esCveUsrNafin);
		loUsuario.setNombre(esNombre);
		loUsuario.setAPaterno(esAPaterno);
		loUsuario.setAMaterno(esAMaterno);
		loUsuario.setStatus(esEstatus);
		loUsuario.setCorreo(esCorreo);

		loUsuario.setFchVencimiento(esFechVencim);
		loUsuario.setTpUsuario(eiTipoUsr);
		loUsuario.setCveEPO(eiCveEPO);
		loUsuario.setCvePYME(eiCvePYME);
		loUsuario.setCveIF(eiCveIF);
		loUsuario.setTipoAfiliacion(esTipoAfiliacion);
		loUsuario.setSIRAC(esSIRAC);

		try
		{
			loUsuario.modificarUsuarioUpdateDB();
			loUsuarioTmp = m_oLUsuario.buscarUsuario(esCveLogin);

			if (loUsuarioTmp != null)
			{
				loUsuarioTmp.setCveUsuario(esCveLogin);
				loUsuarioTmp.setCveUsuarioNafin(esCveUsrNafin);
				loUsuarioTmp.setNombre(esNombre);
				loUsuarioTmp.setAPaterno(esAPaterno);
				loUsuarioTmp.setAMaterno(esAMaterno);
				loUsuarioTmp.setStatus(esEstatus);
				loUsuarioTmp.setCorreo(esCorreo);

				loUsuarioTmp.setFchVencimiento(esFechVencim);
				loUsuarioTmp.setTpUsuario(eiTipoUsr);
				loUsuarioTmp.setCveEPO(eiCveEPO);
				loUsuarioTmp.setCvePYME(eiCvePYME);
				loUsuarioTmp.setCveIF(eiCveIF);
				loUsuarioTmp.setTipoAfiliacion(esTipoAfiliacion);
				loUsuarioTmp.setSIRAC(esSIRAC);
			}
		} catch(SeguException se) {
			throw new SeguException(se.getCodError());
		}
	}

	/**
	 * Valida Facultad.
	 *
	 * @param esCveUsuario	Clave del usuario
	 * @param esCvePerfil	Clave del perfil
	 * @param esCvePerfProt	Clave del perfil prototipo
	 * @param esCveFacultad	Clave de la facultad
	 * @param esCveSistema	Clave del sistema
	 * @param esIP	Direcci�n IP
	 * @param esHost	???
	 *
	 */
	public void validaFacultad(	String esCveUsuario, String esCvePerfil,
			String esCvePerfProt, String esCveFacultad,
			String esCveSistema, String esIP,
			String esHost ) throws SeguException {

		int liResRest = CConst.ERRC_OK;
		boolean lbExistFactExt = false;

		CSistema loSistema = null;
		CPerfil 	loPerfil = null;
		CPerfilProt loPerfilProt = null;
		CFacultad loFacultad = null;
		CRefFacultad loRefFacultad = null;

		try{

			loPerfil = m_oLPerfil.buscarPerfil(esCvePerfil);


			//---------------------
			//Si el perfil recibido como par�metro no existe marca error
			if (loPerfil == null) throw new SeguException(CConst.SEG_ERRC_PERFIL_NO_ASIG);
			//---------------------

			if (loPerfil.getBloqueado().equals(CConst.SI)) throw new SeguException(CConst.SEG_ERRC_PERFIL_BLOQ);


			// Validacion de Perfil Prot
			loPerfilProt = loPerfil.buscarPerfilProt(esCveSistema, esCvePerfProt);

			if (loPerfilProt == null) {
				throw new SeguException(CConst.SEG_ERRC_PP_INEXIST);
			}


			if (loPerfilProt.getBloqueado().equals(CConst.SI)) throw new SeguException(CConst.SEG_ERRC_PP_BLOQ);

			loSistema = m_oLSistema.buscarSistema(esCveSistema);

			if (loSistema == null) throw new SeguException(CConst.SEG_ERRC_SIS_INEXIST);


			if (loSistema.getBloqueado().equals(CConst.SI)) throw new SeguException(CConst.SEG_ERRC_SIS_BLOQ);

			// Validacion por Perfil Prototipo
			loRefFacultad = loPerfilProt.buscarRefFacultad(esCveFacultad);

			if (loRefFacultad == null){
				if (lbExistFactExt == false){
					throw new SeguException(CConst.SEG_ERRC_FAC_PERF_INEXIST);
				}
			} else {

				liResRest = loRefFacultad.validarRestriccion() ;
				if (liResRest != CConst.ERRC_OK)
					throw new SeguException(liResRest);

				loFacultad = loRefFacultad.getFacultad();

				// Validar Facultad bloqueada por Perfil Prototipo
				if (loFacultad.getBloqueado().equals(CConst.SI))
					throw new SeguException(CConst.SEG_ERRC_FAC_BLOQ);


				liResRest = loFacultad.validarRestriccion() ;
				if (liResRest != CConst.ERRC_OK)
					throw new SeguException(liResRest);

				if (loRefFacultad.getMancomunidad().equals(CConst.SI))
					throw new SeguException(CConst.SEG_ERRC_MANCOMUNIDAD);

				//Modificado	171202	--CARP
				// Agregada la condici�n de si la facultad es Tipo Pantalla la guarda en bitacora
				if (loFacultad != null )
					grabarBitacora(esCveUsuario, esCveFacultad,	esCveSistema, "0", esIP, esHost );
			}
		}catch(SeguException e){
				throw new SeguException(e.getCodError());

		} catch(Exception e) {
			throw new SeguException(e.getMessage());
		}

	}

	/**
	 * Obtiene el XML con informaci�n del menu que corresponde al
	 * usuario especificado.
	 *
	 * @param esCveUsuario	Login del usuario
	 *
	 * @return	Regresa una cadena con el XML correspondiente al usuario
	 *
	 * @exception	SeguException	Si la operaci�n falla
	 */
	public String getXMLUsuario(String esCveUsuario)
			throws SeguException {
		String lsCadenaXML = "";
		String lsCvePerfil = null;
		String lsClaveAfiliado = null;
		String lsTipoAfiliado = null;
		int liTipoDeXML = 0;
		CConexionDBPool loCPDB = new CConexionDBPool();

		try {
			loCPDB.conectarDB();

			UtilUsr utilUsr = new UtilUsr();


			Usuario usuario = utilUsr.getUsuario(esCveUsuario);
			lsClaveAfiliado = usuario.getClaveAfiliado();
			lsTipoAfiliado = usuario.getTipoAfiliado();
			lsCvePerfil = usuario.getPerfil();

			if (lsCvePerfil == null || lsCvePerfil.equals("")) {	//El usuario no tiene perfil asociado
				throw new SeguException("El Usuario no tiene asociado ningun perfil");
			}

			boolean esPymeInternacional = false;
			if (lsTipoAfiliado.equals("P")) {

				String lsSQLSelect = "SELECT cs_internacional FROM COMCAT_PYME " +
						" WHERE ic_pyme = ? ";

				PreparedStatement ps = loCPDB.queryPrecompilado(lsSQLSelect);
				ps.setInt(1, Integer.parseInt(lsClaveAfiliado));

				ResultSet rs = ps.executeQuery();
				rs.next();
				String cs_internacional = rs.getString("cs_internacional");
				rs.close();
				ps.close();

				esPymeInternacional = (cs_internacional !=null && cs_internacional.equals("S"))?true:false;
			}

			if (esPymeInternacional) {
				liTipoDeXML = 5; //Tipo de XML para pyme internacional
			} else {
				liTipoDeXML = this.getTipoDeXML("INICIO",
						lsClaveAfiliado, lsTipoAfiliado);
			}

			lsCadenaXML = this.getXML(lsCvePerfil, liTipoDeXML);
			return lsCadenaXML;

		} catch (SeguException e) {
			throw e;
		} catch (Exception e) {
			log.error("SeguridadBean::Error:", e);
			throw new SeguException("SeguridadBean::" + e.getMessage());
		} finally {
			loCPDB.cerrarDB(true);
		}
	}

	/**
	 * Obtiene el XML con informaci�n del menu que corresponde al
	 * usuario especificado.
	 * Creado EGB 22/09/2006 para evitar conectarse nuevamente al WS
	 *
	 * @param Usuario	Objeto usuario inicializado
	 *
	 * @return	Regresa una cadena con el XML correspondiente al usuario
	 *
	 * @exception	SeguException	Si la operaci�n falla
	 */
	public String getXMLUsuario(Usuario usuario)
			throws SeguException {
		String lsCadenaXML = "";
		String lsCvePerfil = null;
		String lsClaveAfiliado = null;
		String lsTipoAfiliado = null;
		int liTipoDeXML = 0;
		CConexionDBPool loCPDB = new CConexionDBPool();

		try {
			loCPDB.conectarDB();

			//UtilUsr utilUsr = new UtilUsr();
			//Usuario usuario = utilUsr.getUsuario(esCveUsuario);
			lsClaveAfiliado = usuario.getClaveAfiliado();
			lsTipoAfiliado = usuario.getTipoAfiliado();
			lsCvePerfil = usuario.getPerfil();

			if (lsCvePerfil == null || lsCvePerfil.equals("")) {	//El usuario no tiene perfil asociado
				throw new SeguException("El Usuario no tiene asociado ningun perfil");
			}

			boolean esPymeInternacional = false;
			if (lsTipoAfiliado.equals("P")) {
				String lsSQLSelect = "SELECT cs_internacional FROM COMCAT_PYME " +
						" WHERE ic_pyme = ? ";

				PreparedStatement ps = loCPDB.queryPrecompilado(lsSQLSelect);
				ps.setInt(1, Integer.parseInt(lsClaveAfiliado));

				ResultSet rs = ps.executeQuery();
				rs.next();
				String cs_internacional = rs.getString("cs_internacional");
				rs.close();
				ps.close();

				esPymeInternacional = (cs_internacional !=null && cs_internacional.equals("S"))?true:false;

			}

			if (esPymeInternacional) {
				liTipoDeXML = 5; //Tipo de XML para pyme internacional
			} else {
				liTipoDeXML = this.getTipoDeXML("INICIO",
						lsClaveAfiliado, lsTipoAfiliado);
			}

			lsCadenaXML = this.getXML(lsCvePerfil, liTipoDeXML);
			return lsCadenaXML;

		} catch (SeguException e) {
			throw e;
		} catch (Exception e) {
			log.error("SeguridadBean::Error:", e);
			throw new SeguException("SeguridadBean::" + e.getMessage());
		} finally {
			loCPDB.cerrarDB(true);
		}
	}



	/**
	 * Genera XML de "SUBMENU" para el usuario especificado.
	 *
	 * @param esClaveAfiliado	Clave del Afiliado
	 * @param esTipoAfiliado	Tipo del Afiliado
	 * @param esCvePerfil	Perfil del usuario
	 *
	 * @return	Regresa una cadena con el XML correspondiente al usuario
	 *
	 * @exception	SeguException	Si la operaci�n falla
	 */
	public String generaXML( String esClaveAfiliado,
			String esTipoAfiliado, String esCvePerfil )
			throws SeguException {
		String lsCadenaXML = "";
		//String lsCvePerfil = null;

		//String lsClaveAfiliado = null;
		//String lsTipoAfiliado = null;


		int liTipoDeXML = 0;
		boolean lbExito = true;

		try {
			//UtilUsr utilUsr = new UtilUsr();


			//Usuario usuario = utilUsr.getUsuario(esCveUsuario);
			//lsClaveAfiliado = usuario.getClaveAfiliado();
			//lsTipoAfiliado = usuario.getTipoAfiliado();
			//lsCvePerfil = usuario.getPerfil();
      log.debug("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: generaXML() ::..");

			liTipoDeXML = this.getTipoDeXML("SUBMENU", esClaveAfiliado, esTipoAfiliado);

			if (esCvePerfil == null) {	//El usuario no tiene perfil asociado
				throw new SeguException("El Usuario no tiene asociado ningun perfil");
			}

			lsCadenaXML = this.getXML(esCvePerfil, liTipoDeXML);

			return lsCadenaXML;
		}catch (SeguException e) {
				log.error("SeguridadBean::generaXML()", e);
				if( e.getCodError() != CConst.SEG_ERRC_SESION_ACT)
					lbExito = false;
			throw new SeguException(e.getCodError());
		}catch (NullPointerException e) {
				log.error("SeguridadBean::", e);
				lbExito = false;
			throw new SeguException("SeguridadBean::(ERROR AL CARGAR USUARIO EN LA LISTA)");
		}catch (Exception e) {
				log.error("SeguridadBean::Error:", e);
				lbExito = false;
			throw new SeguException("SeguridadBean::(ERROR DESCONOCIDO)");
		}
	}


	public void CambiaTRACE()
					throws	Exception,	SQLException,	SeguException
	{
			log.debug("SeguridadBean::CambiaTRACE(E):" + CConst.TRACE);
			if(CConst.TRACE)
					 CConst.TRACE = false;
			else
					 CConst.TRACE = true;
			log.debug("SeguridadBean::CambiaTRACE(S):" + CConst.TRACE);
	}



/******************************************************************************
							M�todos Privados
******************************************************************************/

	/**
	 * Actualiza Facultades de un Perfil prototipo
	 * @param esCveSistema	Clave del Sistema
	 * @param esCvePerfilProt	Clave del Perfil Prototipo
	 * @exception	SeguException	Si la actualizaci�n falla
	 */
	private void actualizarFacPerfilProt(String esCveSistema, String esCvePerfilProt)
 					throws SeguException
	{
		CSistema loSistema = null;
		CPerfilProt loPerfilProt = null;
		CRefFacultad loRefFacultad = null;
		CFacultad loFacultad = null;

		Vector lvPPFacultades = null;
		Vector lvRegistro = null;

		int liNumRegs = 0;

		String lsCveFacultad = "";
		long	llNRestriccion = 0;
		String lsHoraInicial = "";
		String lsHoraFinal = "";
		String lsFchVencimiento = "";
		String lsMancomunidad = "";

		try{
			lvPPFacultades = new Vector();

			/*if (log.isTraceEnabled())
				log.trace("\tSeguridadBean::actualizarFacPerfilProt()" );*/

			loSistema = m_oLSistema.buscarSistema(esCveSistema);

			if (loSistema == null) throw new SeguException(CConst.SEG_ERRC_SIS_INEXIST);

								/*if (log.isTraceEnabled())
							log.trace("\t[SISTEMA " + loSistema.getCveSistema() + "]");*/

			loPerfilProt = loSistema.buscarPerfilProt(esCvePerfilProt);

			if (loPerfilProt == null)
				throw new SeguException(CConst.SEG_ERRC_PP_INEXIST);

			loSistema.consultarRefFacPPSelectDB(esCvePerfilProt,lvPPFacultades);

			liNumRegs = lvPPFacultades.size();
								/*if (log.isTraceEnabled())
						log.trace("\tSeguridadBean::actualizarFacPerfilProt TAMANO lvPPFacultades [" + lvPPFacultades.size() +"]");*/

			loPerfilProt.borrarRefFacultades();

			for (int liPos = 0; liPos < liNumRegs; liPos++){
											/*if (log.isTraceEnabled())
							log.trace("\tSeguridadBean::dentro del for OK 1]");*/
				lvRegistro = (Vector) lvPPFacultades.elementAt(liPos);

				lsCveFacultad = (String) lvRegistro.elementAt(0);
				llNRestriccion = ((Integer)lvRegistro.elementAt(1)).longValue();
				lsHoraInicial = (String) lvRegistro.elementAt(2);
				lsHoraFinal = (String) lvRegistro.elementAt(3);
				lsFchVencimiento = (String) lvRegistro.elementAt(4);
				lsMancomunidad = (String) lvRegistro.elementAt(5);

				loFacultad = loSistema.buscarFacultad(lsCveFacultad);

				loRefFacultad = new CRefFacultad(lsCveFacultad, 	llNRestriccion, 	lsHoraInicial,
															lsHoraFinal, 		lsFchVencimiento, lsMancomunidad,
															loFacultad);

				loPerfilProt.agregarRefFacultadPP( loRefFacultad );
			}
		}catch (SeguException e) {
						log.error("SeguridadBean::",e);
			throw new SeguException(e.getCodError());
		}catch (NullPointerException e) {
				 	log.error("SeguridadBean::",e);
			throw new SeguException("SeguridadBean::(ERROR AL CARGAR FACULTAD)");
		}catch (SQLException e) {
						log.error("SeguridadBean::",e);
			throw new SeguException("SeguridadBean::(ERROR EN LA BASE DE DATOS)");
		}catch (Exception e) {
						log.error("SeguridadBean::Error:",e);
			throw new SeguException("SeguridadBean::(ERROR DESCONOCIDO)");
		}
	}

	/**
	 * Actualiza Facultad
	 * @param ecTipo	Tipo de Actualizaci�n:
	 * 				CConst.ALTA, CConst.BAJA, CConst.CAMBIO
	 * @param esCveSistema	Clave del sistema
	 * @param esCveFacultad	Clave de la facultad
	 * @exception	SeguException	Si la actualizaci�n falla
	 */
	private void actualizarFacultad(char ecTipo, String esCveSistema,
			String esCveFacultad)
 					throws SeguException
	{
		CSistema loSistema = null;
		CFacultad loFacultad = null;
		CFacultad loFacultadTmp = null;

		try{
			/*if (log.isTraceEnabled())
				log.trace("\tSeguridadBean::actualizarFacultad()" );*/

			loSistema = m_oLSistema.buscarSistema(esCveSistema);

			if (loSistema == null) throw new SeguException(CConst.SEG_ERRC_SIS_INEXIST);

			/*if (log.isTraceEnabled())
				log.trace("\t[SISTEMA " + loSistema.getCveSistema() + "]");*/

			switch (ecTipo)
			{
				case CConst.ALTA:
							/*if (log.isTraceEnabled())
								log.trace("Alta");*/
							loFacultadTmp = loSistema.consultarFacultadSelectDB(esCveFacultad);

							if (loFacultadTmp != null)
								loSistema.agregarFacultad(loFacultadTmp);
							else
								throw new SeguException (CConst.SEG_ERRC_FAC_INEXIST);
							break;
				case CConst.BAJA:
							/*if (log.isTraceEnabled())
								log.trace("Baja");*/
							loSistema.borrarFacultadMem(esCveFacultad);
							break;
				case CConst.CAMBIO:
							/*if (log.isTraceEnabled())
								log.trace("Cambio");*/
							loFacultad = loSistema.buscarFacultad(esCveFacultad);
							if (loFacultad == null)
								throw new SeguException (CConst.SEG_ERRC_FAC_INEXIST);

							loFacultadTmp = loSistema.consultarFacultadSelectDB(esCveFacultad);

							if (loFacultadTmp != null)
								loFacultad.actualizarFacultad(loFacultadTmp);
							else
								throw new SeguException (CConst.SEG_ERRC_FAC_INEXIST);
							break;
			}

		}catch (SeguException e) {
							log.error("SeguridadBean::",e);
					throw new SeguException(e.getCodError());
		}catch (NullPointerException e) {
				 	log.error("SeguridadBean::",e);
			throw new SeguException("SeguridadBean::(ERROR AL CARGAR FACULTAD)");
		}catch (Exception e) {
						log.error("SeguridadBean::Error:",e);
			throw new SeguException("SeguridadBean::(ERROR DESCONOCIDO)");
		}
	}


 	/**
	 * Actualiza Perfiles Prototipos
	 * @param ecTipo	Tipo de Actualizaci�n:
	 * 				CConst.ALTA, CConst.CAMBIO, CConst.BAJA
	 * @param esCveSistema	Clave del sistema
	 * @param esCvePerfilProt	Clave del perfil prototipo
	 * @param esDescripcion	Descripci�n
	 * @param esBloqueo	???
	 * @exception	SeguException	Si la actualizaci�n falla
	 */
	private void actualizarPerfilProt(char ecTipo, String esCveSistema,
												String esCvePerfilProt,	String esDescripcion,
												String esBloqueo)
 					throws SeguException
	{
		CSistema loSistema = null;
		CPerfilProt loPerfilProt = null;

		try
		{
			/*if (log.isTraceEnabled())
				log.trace("\tCTemporal::actualizarPerfilProt()" );*/

			loSistema = m_oLSistema.buscarSistema(esCveSistema);

			if (loSistema == null) throw new SeguException(CConst.SEG_ERRC_SIS_INEXIST);

			/*if (log.isTraceEnabled())
				log.trace("\t[SISTEMA " + loSistema.getCveSistema() + "]");*/

			switch (ecTipo) {
				case CConst.ALTA:
							/*if (log.isTraceEnabled())
								log.trace("Alta");*/

							loPerfilProt = new CPerfilProt(	esCveSistema,		esCvePerfilProt,
																		esDescripcion,		esBloqueo);

							if (loPerfilProt == null)
								throw new SeguException(CConst.SEG_ERRC_PP_INEXIST);

							loSistema.agregarPerfilProt( loPerfilProt );
							break;
				case CConst.BAJA:
							/*if (log.isTraceEnabled())
								log.trace("Baja");*/
							loSistema.borrarPerfilProtMem(esCvePerfilProt);
							break;
				case CConst.CAMBIO:
							/*if (log.isTraceEnabled())
								log.trace("Cambio");*/
							loPerfilProt = loSistema.buscarPerfilProt(esCvePerfilProt);

							if (loPerfilProt == null)
								throw new SeguException(CConst.SEG_ERRC_PP_INEXIST);

							loPerfilProt.setCveSistema(esCveSistema);
							loPerfilProt.setCvePerfilProt(esCvePerfilProt);
							loPerfilProt.setDescripcion(esDescripcion);
							loPerfilProt.setBloqueado(esBloqueo);
							break;
			}
		}catch (SeguException e) {
						log.error("CTemporal::",e);
			throw new SeguException(e.getCodError());
		}catch (NullPointerException e) {
				 	log.error("CTemporal::",e);
			throw new SeguException("CTemporal::(ERROR AL CARGAR FACULTAD)");
		}catch (Exception e) {
						log.error("CTemporal::Error:", e);
			throw new SeguException("CTemporal::(ERROR DESCONOCIDO)");
		}
	}


	/**
	 * Este m�todo debe ser llamado cada vez que se realice un cambio
	 * (Altas, Bajas, Cambios en MEMORIA) en Sistemas, Perfiles,
	 * Perfiles Prototipos,Facultades o en cualquiera de las relaciones
	 * que existen entre estos.
	 * El m�todo genera los XML por cada uno de los perfiles incluyendo
	 * las variantes y los almacena en la BD.
	 *
	 * Los datos de seguridad tienen que estar cargados previamente a
	 * esta operaci�n usando crearDatos().
	 *
	 * @exception	SeguException	Si la operaci�n falla
	 */
	private void generarXMLPorPerfil() throws SeguException{
		try {

			int liTipoAfiliado = 0;	//Tipo de usuario
			char lcComilla = '"' ;
			String lsTipo = "";
			Enumeration loeClavesPerfiles = null;	//Enumeraci�n de claves de perfil
			boolean lbCuentaCorriente = false;	//true: Es Pyme y trabaja con Cuenta Corriente
			String lsCvePerfil = null;		//Clave del perfil
			CPerfil loPerfil = null;	//Perfil
			Hashtable lohtRefPerfProt = null;	//Perfiles prototipos asociados al perfil

			loeClavesPerfiles = m_oLPerfil.getClavesPerfiles();	//obtiene todas las claves de perfiles registradas

			while ( loeClavesPerfiles.hasMoreElements() ){

				lsCvePerfil = (String) loeClavesPerfiles.nextElement();
				loPerfil = m_oLPerfil.getPerfil(lsCvePerfil);
				liTipoAfiliado = loPerfil.getTipoAfiliado();	// 3 = PYME
				lohtRefPerfProt = loPerfil.devolverRefPerfilesProt();	//Asigna los perfiles prototipos asociados al perfil

				for (int liTipoDeXML = 1; liTipoDeXML <= 5; liTipoDeXML++) {
					//EL tipo 5 es una variante del 1. La diferencia radica
					//en que el 5 est� enfocado a pymes internacionales
					//y solo mostrara aquellas que tengan descripcion en ingles
					if (liTipoDeXML == 1) {	// XML de Inicio
						lsTipo = "INICIO";
						lbCuentaCorriente = true;
					} else if (liTipoDeXML == 2) {	// XML de Submenu
						lsTipo = "SUBMENU";
						lbCuentaCorriente = true;
					} else if (liTipoDeXML == 3) {	// XML de Inicio con pyme sin cuenta corriente
						if (liTipoAfiliado != 3) {	//Si no es pyme se omite la generaci�n de este XML
							continue;
						}
						lsTipo = "INICIO";
						lbCuentaCorriente = false;
					} else if (liTipoDeXML == 4) {	//XML de Submenu con pyme sin cuenta corriente
						if (liTipoAfiliado != 3) {	//Si no es pyme se omite la generaci�n de este XML
							continue;
						}
						lsTipo = "SUBMENU";
						lbCuentaCorriente = false;
					} else if (liTipoDeXML == 5) {	//Pymes internacionales
						if (liTipoAfiliado != 3) {
							continue;
						}
						lsTipo = "INICIO";
						lbCuentaCorriente = true;
					}


					Hashtable lhtTmpSistemas = new Hashtable();
					StringBuffer lsCadenaXML = new StringBuffer();	//Cadena que contiene el XML generado para el perfil
					Enumeration enumTmpSistemas = null;
					Vector lovSistPadre = new Vector();	//Vector donde se registran los Sistemas Padres ya procesados
					Vector lovSistemasHijos = new Vector();


					//System.out.println("Clave del perfil: " + lsCvePerfil);

					lsCadenaXML.append("<?xml version=" + lcComilla + "1.0" + lcComilla + " encoding=" + lcComilla + "ISO-8859-1" + lcComilla + "?>");
					lsCadenaXML.append("<seguridad>");
					lsCadenaXML.append("<usuario></usuario>");
					lsCadenaXML.append("<nombre></nombre>");
					lsCadenaXML.append("<cveempresa></cveempresa>");

					if(lohtRefPerfProt != null){
							//System.out.println("SeguridadBean.generarXML(" + "tama�o PerfilesProt:" + lohtRefPerfProt.size() + ")");

						for(int iCont = 1; iCont <= lohtRefPerfProt.size(); iCont++){	//Por cada Perfil prototipo relacionado con el perfil
							String lsTmpSistema = "";
							CPerfilProt loPerfilProt = null;
							CRefPerfilProt loRefPerfilProt = null;
							String lsCadPPXML = "";
							String lsCadPPXMLConcat = "";

							loRefPerfilProt = (CRefPerfilProt) lohtRefPerfProt.get(new Integer(iCont));	//Obtiene uno por uno los perfiles prototipos relacionados
							lsTmpSistema = loRefPerfilProt.getCveSistema();	//Obtiene la clave del sistema al cual pertenece el p�rfil prototipo relacionado

							if( (	lsTmpSistema.equals("INVEN") || lsTmpSistema.equals("FDIST") ||
									lsTmpSistema.equals("ANTIC") || lsTmpSistema.equals("PRELI") ||
									lsTmpSistema.equals("FOBRA") || lsTmpSistema.equals("PEQUI") ||
									lsTmpSistema.equals("INICI") || lsTmpSistema.equals("CENAF") ||
									lsTmpSistema.equals("GSINF") || lsTmpSistema.equals("NEMP") ||
									lsTmpSistema.equals("CDER") ||
									lsTmpSistema.equals("FELEC") ||
									lsTmpSistema.equals("EXPF") || ("ADMIN EPO TOIC".equals(lsCvePerfil) && lsTmpSistema.equals("NANET")))
									&& lsTipo.equals("INICIO")) {
								continue;
							}

							if(lsTmpSistema.equals("DLINC") && !lbCuentaCorriente) {
								continue;
							}

							loPerfilProt = loRefPerfilProt.getPerfilProt();	//Obtiene el Perfil prototipo a apartir de la referencia del perfil prototipo
							lsCadPPXML	= loPerfilProt.generarXML(lsCvePerfil);

							if(!lhtTmpSistemas.containsKey(lsTmpSistema)){	//En lhtTmpSistemas se colocan los distintos sistemas a los que tiene acceso el perfil
								lhtTmpSistemas.put(lsTmpSistema, lsCadPPXML);
							}
							else{
								//System.out.println("El sistema " + lsTmpSistema + "ya se encontraba en lhtTmpSistemas");
								lsCadPPXMLConcat = (String) lhtTmpSistemas.get(lsTmpSistema) + lsCadPPXML;
								lhtTmpSistemas.put(lsTmpSistema, lsCadPPXMLConcat);
							}
						} // fin del for (Por cada perfil prototipo)
					}
					else{	//No hay perfil prototipos asociados al perfil
						/*if(log.isTraceEnabled())
								log.trace("PerfilesProt NULO del Perfil");*/
					}

					/*if(log.isTraceEnabled()){
						Enumeration enumTmpSistemasTrace = lhtTmpSistemas.keys();
							while(enumTmpSistemasTrace.hasMoreElements()){
							log.trace("\tSistemas en Enumeration: " + (String)enumTmpSistemasTrace.nextElement() + ")");
						}
					}*/

					enumTmpSistemas = lhtTmpSistemas.keys();

					while(enumTmpSistemas.hasMoreElements()){
						boolean lbBandera = false;
						boolean lbTienePadre = false;
						CSistema loSistema = null;
						CRelSistema	loRelSistema = null;
						String lsTmpSistema = "";

						lsTmpSistema = (String) enumTmpSistemas.nextElement();
						//System.out.print(lsTmpSistema);
						while(lbBandera == false){
							loRelSistema = m_oLRelSistemas.buscarSistemaPadre(lsTmpSistema);
							if(loRelSistema == null){
								lbBandera = true;
							}
							else{
								lsTmpSistema = loRelSistema.getCveSistPadre();
								lbTienePadre = true;
							}
						}

						if(lbTienePadre == true){
							/*if(log.isTraceEnabled())
								log.trace("\tSistema Padre: "+ lsTmpSistema);*/

							if( (		!lsTmpSistema.equals("INVEN") && !lsTmpSistema.equals("FDIST")
									&& 	!lsTmpSistema.equals("ANTIC") && !lsTmpSistema.equals("FOBRA")
									&& 	!lsTmpSistema.equals("PEQUI") && !lsTmpSistema.equals("PRELI")
									&& 	!lsTmpSistema.equals("GSINF") && !lsTmpSistema.equals("CENAF")
									&& 	!lsTmpSistema.equals("INICI") && !lsTmpSistema.equals("NEMP")
									&&  !lsTmpSistema.equals("CDER")
									&&  !lsTmpSistema.equals("FELEC")
									&& 	!lsTmpSistema.equals("EXPF")  && !("ADMIN EPO TOIC".equals(lsCvePerfil) && lsTmpSistema.equals("NANET")))
								 	||	lsTipo.equals("SUBMENU")){
								if(!lovSistPadre.contains(lsTmpSistema)){ //Si el sistema ya se encuentra en el vector, ya no se genera XML correspondiente
									lovSistPadre.addElement(lsTmpSistema);

									lovSistemasHijos = new Vector();
									lovSistemasHijos.addAll(m_oLRelSistemas.devolverSistemasHijos(lsTmpSistema));

									loSistema = m_oLSistema.buscarSistema(lsTmpSistema);



									//Si es pyme internacional y no existe una descripci�n en ingl�s para
									//el sistema entonces no lo considera.
									if (liTipoDeXML == 5 && loSistema.getDescripcionIngles().equals("")) {
										continue;
									} else {
										lsCadenaXML.append(loSistema.generarXML());

										for(int k=0; k < lovSistemasHijos.size(); k++){

											String lsTmpSistemaHijo = "";

											lsTmpSistemaHijo = (String) lovSistemasHijos.elementAt(k);
											loSistema = m_oLSistema.buscarSistema(lsTmpSistemaHijo);

											if(lhtTmpSistemas.containsKey(lsTmpSistemaHijo)){

												String lsCadFacXML2 = (String) lhtTmpSistemas.get(lsTmpSistemaHijo);
												if(lsCadFacXML2.length() > 0){

													lsCadenaXML.append(loSistema.generarXML(k + 1,lsTmpSistema));
													lsCadenaXML.append(lsCadFacXML2);
													lsCadenaXML.append("</subsistema>");
												}
											}
										}

										if(lhtTmpSistemas.containsKey(lsTmpSistema)){
											lsCadenaXML.append(lhtTmpSistemas.get(lsTmpSistema));
										}
										lsCadenaXML.append("</sistema>");
									}
								}
							}
						} // if de sistema Padre
						else if((m_oLRelSistemas.devolverSistemasHijos(lsTmpSistema)).size() > 0){	//generar Subsistemas
//							System.out.println("\t No tiene padre pero si tiene susbsistemas: "+ lsTmpSistema);
							if(!lovSistPadre.contains(lsTmpSistema)){		//Si el sistema ya se encuentra en el vector, ya no se genera XML correspondiente

								lovSistPadre.addElement(lsTmpSistema);
								lovSistemasHijos = new Vector();
								lovSistemasHijos.addAll(m_oLRelSistemas.devolverSistemasHijos(lsTmpSistema));

								loSistema = m_oLSistema.buscarSistema(lsTmpSistema);

								//Si es pyme internacional y no existe una descripci�n en ingl�s para
								//el sistema entonces no lo considera.
								if (liTipoDeXML == 5 && loSistema.getDescripcionIngles().equals("")) {
									continue;
								} else {


									lsCadenaXML.append(loSistema.generarXML());	//Cuando es Sistema padre y tiene hijos y no es de SUBMENU agrega la cadena para generar el XML del sistema
									for (int k=0; k < lovSistemasHijos.size(); k++){

										String lsTmpSistemaHijo = (String) lovSistemasHijos.elementAt(k);
										loSistema = m_oLSistema.buscarSistema(lsTmpSistemaHijo);

										if(lhtTmpSistemas.containsKey(lsTmpSistemaHijo)){
											String lsCadFacXML2 = (String) lhtTmpSistemas.get(lsTmpSistemaHijo);
											if (lsCadFacXML2.length() > 0){
												lsCadenaXML.append(loSistema.generarXML(k + 1,lsTmpSistema));	//Se genera el XML para los Subsistemas
												lsCadenaXML.append(lsCadFacXML2);
												lsCadenaXML.append("</subsistema>");
											}
										}
									}

									if(lhtTmpSistemas.containsKey(lsTmpSistema)){
										lsCadenaXML.append(lhtTmpSistemas.get(lsTmpSistema));
									}
									lsCadenaXML.append("</sistema>");
								}
							} //if(!lovSistPadre.contains(lsTmpSistema))
						}	//fin if...
						else{	//No tiene padre ni hijos..
//						 	System.out.println("\t No tiene Padre ni hijos");
							loSistema = m_oLSistema.buscarSistema(lsTmpSistema);

							//Si es pyme internacional y no existe una descripci�n en ingl�s para
							//el sistema entonces no lo considera.
							if (liTipoDeXML == 5 && loSistema.getDescripcionIngles().equals("")) {
								continue;
							} else {


								lsCadenaXML.append(loSistema.generarXML());

								if(lhtTmpSistemas.containsKey(lsTmpSistema)){
									lsCadenaXML.append(lhtTmpSistemas.get(lsTmpSistema));
								}
								lsCadenaXML.append("</sistema>");
							}
						}
					}	//fin del while	de los sistemas...
					lsCadenaXML.append("</seguridad>");

					grabarXML(lsCadenaXML.toString(), lsCvePerfil, liTipoDeXML);
				}	//fin del for

			}	//fin del while
		} catch(Exception e) {
			throw new SeguException("Error al generar los perfiles por prototipo");
		}
	} // fin de generarXMLPorPerfil()

	/**
	 * Obtiene el XML de la BD.
	 *
	 * @param lsCvePerfil	Clave del perfil del cual se desea obtener el XML
	 * @param liTipoDeXML	Tipo de XML:
	 *					1.-Inicio, 2.-Submenu, 3.-Inicio pyme sin cuenta corriente,
 	 *					4.-Submenu pyme sin cuenta corriente
	 * @return	Regresa una cadena con el XML correspondiente
	 *
	 * @exception	SeguException	Si la operaci�n falla
	 */
	private String getXML(String lsCvePerfil, int liTipoDeXML)
			throws SQLException, Exception {


/*		String lsSQL = " SELECT cg_xml FROM seg_xml_x_perfil " +
				" WHERE cc_perfil = '" + lsCvePerfil + "'" +
				" AND ci_tipo_xml = " +liTipoDeXML;
*/

		String lsSQL =
				" SELECT cg_xml FROM seg_xml_x_perfil " +
				" WHERE cc_perfil = ? " +
				" AND ci_tipo_xml = ? ";

		boolean lbResultado = true;
		String lsCadenaXML = null;
		CConexionDBPool loCPDB = new CConexionDBPool();

		try {
			loCPDB.conectarDB();

			PreparedStatement ps = loCPDB.queryPrecompilado(lsSQL);

			ps.setString(1, lsCvePerfil);
			ps.setInt(2, liTipoDeXML);

			ResultSet rs = ps.executeQuery();

			/*loCPDB.ejecutarQueryDB(lsSQL, liNumColumnas, lvTmp);

			if (lvTmp.size() != 0) {
				Vector lvTmpRegistro = (Vector) lvTmp.elementAt(0);	//Solo hay un registro.
				lsCadenaXML = (String)lvTmpRegistro.elementAt(0);	//cg_xml
			}
			*/
			if (rs.next()) {

				Clob loClob = rs.getClob("cg_xml");
				//loClob.length debe estar entre 0 y 2^31-1 para que la siguiente instrucci�n sea v�lida
				lsCadenaXML = loClob.getSubString(1, (int)loClob.length());	//
			}
			rs.close();
			ps.close();

			//System.out.println("cg_xml" + lsCadenaXML);

			return lsCadenaXML;

		} catch(SQLException sqlException) {
			lbResultado = false;
			log.error("Error. ", sqlException);
			throw sqlException;
		} finally {
			loCPDB.cerrarDB(lbResultado);
		}
	}


	/**
	 * Graba en la bit�cora los datos del usuario que accesa al sistema
	 *
	 * @param esCveUsuario	Clave del Usuario que accesa al sistema
	 * @param esCveFacultad	Clave de la Facultad
	 * @param esCveSistema	Clave del Sistema
	 * @param esCodError	???
	 * @param esIP	IP desde donde se accesa al sistema
	 * @param esHost	???
	 * @exception	SeguException	Si no es posible registrar en bit�cora.
	 */
	private void grabarBitacora(String esCveUsuario, 	String esCveFacultad,
										String esCveSistema,		String esCodError,
										String esIP,				String esHost )
					throws SeguException
	{
		boolean	lbResultado = true;
		String 	lsSQLInsert = "";
		CConexionDBPool loconConecta = new CConexionDBPool();

		lsSQLInsert =
				"INSERT INTO bit_diaria " +
				"(f_consulta,ic_usuario,c_sistema,c_facultad,t_IP,c_error,c_host) " +
				"VALUES (sysdate, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = null;
		try{
			loconConecta.conectarDB();

			ps = loconConecta.queryPrecompilado(lsSQLInsert);

			ps.setString(1, esCveUsuario);
			ps.setString(2, esCveSistema);
			ps.setString(3, esCveFacultad);
			ps.setString(4, esIP);
			ps.setString(5, esCodError);
			ps.setString(6, esHost);

			ps.executeUpdate();

		}catch(SQLException e){
			lbResultado = false;
			List parametros = new ArrayList();
			parametros.add(esCveUsuario);
			parametros.add(esCveSistema);
			parametros.add(esCveFacultad);
			parametros.add(esIP);
			parametros.add(esCodError);
			parametros.add(esHost);

			QueryDebugger qd = new QueryDebugger(lsSQLInsert, parametros);

			log.error("\t\tgrabarBitacora(" + e + "):: " + qd.getQueryStringWithParameters());
		}catch(Exception e){
			lbResultado = false;
			log.error("\t\tgrabarBitacora(" + e + ")", e);
		}finally{
			if (ps != null) {
				try { ps.close(); } catch(SQLException e) {log.error("\t\tgrabarBitacora(" + e + ")", e);}
			}
			loconConecta.cerrarDB(lbResultado);
		}
	}



	/**
	 * Almacena en BD una cadena de XML.
	 *
	 * @param lsCadenaXML	Contenido del XML
	 * @param lsCvePerfil	Clave del perfil
	 * @param liTipoDeXML	Tipo de XML:
	 *					1.-Inicio, 2.-Submenu, 3.-Inicio pyme sin cuenta corriente,
 	 *					4.-Submenu pyme sin cuenta corriente
	 * @return	Regresa una cadena con el XML correspondiente
	 *
	 * @exception	SeguException	Si la operaci�n falla
	 */
	private void grabarXML(String lsCadenaXML, String lsCvePerfil,
			int liTipoDeXML) throws SQLException, Exception {

		CConexionDBPool loCPDB = new CConexionDBPool();
		boolean lbResultado = true;
		int liTamanoCadena = lsCadenaXML.length();
		int liTamanok = 31999;	//liTamanok es el tama�o m�ximo que acepta Oracle (dada la configuraci�n actual)
		int liIniciok = 0, liFink = 0;
		int liNumeroDePartes = (int) (liTamanoCadena / liTamanok);
		if (liTamanoCadena % liTamanok != 0) { //Si la divisi�n no es exacta se agrega una parte m�s.
			liNumeroDePartes++;
		}

		String lsPiezaCadena = "";

		try {
			loCPDB.conectarDB();

			// Inicio. Cambio por PreparedStatement GEAG 10/10/2006
			PreparedStatement ps = loCPDB.queryPrecompilado(
					" DELETE FROM seg_xml_x_perfil WHERE cc_perfil = ? " +
					" AND ci_tipo_xml = ? ");
			ps.setString(1, lsCvePerfil);
			ps.setInt(2, liTipoDeXML);

			ps.executeUpdate();

			ps.close();
			// Fin. Cambio por PreparedStatement GEAG 10/10/2006

//			loCPDB.ejecutarUpdateDB("DELETE FROM seg_xml_x_perfil WHERE cc_perfil = " +
//					"'" + lsCvePerfil + "' AND ci_tipo_xml = " + liTipoDeXML);


			CallableStatement cs = loCPDB.ejecutarSP("SP_INSERTA_LOB2(?,?,?,?,?,?,?)");

			for(int v=0; v < liNumeroDePartes; v++) {
				liIniciok = v * liTamanok;
				liFink = (v+1) * liTamanok;
				if (liFink > liTamanoCadena) {	//El indice Final sobrepasa el tama�o de la cadena?
					liFink = liTamanoCadena;
				}

				lsPiezaCadena = lsCadenaXML.substring(liIniciok, liFink);

				cs.setString(1, "seg_xml_x_perfil"); //tabla donde se almacena el XML
				cs.setString(2, "cc_perfil, ci_tipo_xml");	//Campos que se insertan (NO CLOB)
				cs.setString(3, "cg_xml");	//Campos	CLOB que se insertan
				cs.setString(4, "'" + lsCvePerfil + "', " + liTipoDeXML);	//Valores de los campos del parametro 2
				cs.setString(5, lsPiezaCadena);
				cs.setString(6, "cc_perfil = '" + lsCvePerfil + "' AND ci_tipo_xml = " + liTipoDeXML);	//Condici�n para hacer los insert subsecuentes en el CLOB
				cs.setInt(7, v);	//v=0 primera vez...
				cs.execute();
			}
			if(cs != null) {
				 cs.close();
			}

		} catch(SQLException e) {
			lbResultado = false;
			log.error("grabarXML(Error)", e);
			throw e;
		} finally {
			loCPDB.cerrarDB(lbResultado);
		}

	}


	/**
	 * Determina si el usuario es PyME sin cuenta corriente
	 * El metodo asume que la clave del afiliado corresponde a una Pyme.
	 * @param esUsuario Clave del usuario
	 * @return true si es pyme sin cuenta corriente o false de lo contrario
	 */
	private boolean esPymeSinCuentaCorriente(String esClaveAfiliado)
			throws Exception {
		CConexionDBPool	loConexion = new CConexionDBPool();
		StringBuffer lsbSQL = new StringBuffer();

		int liClaveAfiliado = Integer.parseInt(esClaveAfiliado);
		int liCont 	  = 1;
		boolean lbSinCuentaCorriente = false;

		try {
			loConexion.conectarDB();
			//Si la clave es 0 corresponde a usuario de Recursos humanos
			//o Callcenter para Fondos de inversion
			//Por lo cual no se consideran
			if ( liClaveAfiliado != 0) {
				lsbSQL.append("  SELECT count(*)" +
						"  FROM comrel_pyme_epo_x_producto" +
						"  WHERE cg_tipo_credito = 'C'" +
						"  AND ic_producto_nafin = 4" +
						"  AND ic_pyme = ?");

				PreparedStatement ps = loConexion.queryPrecompilado(lsbSQL.toString());
				ps.setInt(1, liClaveAfiliado);
				ResultSet rs = ps.executeQuery();
				rs.next();
				liCont = rs.getInt(1);
				rs.close();
				ps.close();

				/*loConexion.ejecutarQueryDB(lsbSQL.toString(), liNumCols, lovDatos);
    	        if( lovDatos != null  ){
					lovRegistro = (Vector) lovDatos.elementAt(0);
	        	    if( lovRegistro != null)
  		  				liCont = ((Integer) lovRegistro.elementAt(0)).intValue();
	            }
	            */
    	        lbSinCuentaCorriente = (liCont == 0)? true: false;
            }

		} catch (Exception error) {
			log.error("Error en esPymeCuentaCorriente(): ", error);
			throw error;
		} finally {
			loConexion.cerrarDB(true);
		}
		return lbSinCuentaCorriente;
	}



/**
 * Obtiene el tipo de XML correspondiente al usuario especificado
 * @param esUsuario Clave de usuario
 * @param esTipo Tipo de XML a obtener:
 *		INICIO
 *		SUBMENU
 * @param esClaveAfiliado Clave del afiliado
 * 		(clave del If, EPO, Pyme, Nafin Distribuidor,etc)
 * @param esTipoAfiliado Tipo de Afiliado: P Pyme, N Nafin, E Epo, D Distribuidor, I IF
 * @return entero con la clave del tipo de XML que corresponde al usuario
 */
	private int getTipoDeXML(String esTipo,
			String esClaveAfiliado, String esTipoAfiliado)
			throws Exception {
		int liTipoDeXML = 0;

	  if (esTipo.equals("INICIO")) {
			if ( esTipoAfiliado.equals("P") &&
					esPymeSinCuentaCorriente(esClaveAfiliado)) {
				liTipoDeXML = 3;	// 3.- INICIO PYME SIN CUENTA CORRIENTE
			} else {
				liTipoDeXML = 1;	// 1.- INICIO
			}
		} else if (esTipo.equals("SUBMENU")) {
			if ( esTipoAfiliado.equals("P") &&
					esPymeSinCuentaCorriente(esClaveAfiliado)) {
				liTipoDeXML = 4;	// 4.- SUBMENU PYME SIN CUENTA CORRIENTE
			} else {
				liTipoDeXML = 2;	// 2.- SUBMENU
			}
		}

		return liTipoDeXML;
	}


	/*********************************************************************************************
	*
	*	 	    boolean vvalidarCveCesion()
	*
	*********************************************************************************************/
	public boolean vvalidarCveCesion(String esLogin) throws SeguException{
		log.info("vvalidarCveCesion(E)");
		String lsQrySentencia = null;
		boolean ok = false;
		CConexionDBPool lodbConexion = new CConexionDBPool();
		try {
			ResultSet lrsSentencia = null;
			lodbConexion.conectarDB();

			lsQrySentencia = "SELECT ic_usuario " +
				" FROM seg_cve_cesion " +
				" WHERE ic_usuario = ? "+
				" AND CG_CVE_CONFIRMACION is not null ";

			PreparedStatement ps = lodbConexion.queryPrecompilado(lsQrySentencia);
			ps.setString(1, esLogin);
			lrsSentencia = ps.executeQuery();

			if (lrsSentencia.next()){
				ok = true;
			}
			ps.close();
		}catch (Exception e) {
			log.error("vvalidarCveCesion(Error)", e);
			throw new SeguException(e.getMessage());
		} finally {
			lodbConexion.cerrarDB(true);
			log.info("vvalidarCveCesion(S)");
		}
		return ok;
	}

	/*********************************************************************************************
	*
	*	 	    void guardarCveCesEncriptada()
	*
	*********************************************************************************************/
	public void guardarCveCesEncriptada(String esLogin, String esCve) throws SeguException{
		try {
			guardarCveCesEncriptada(esLogin, esCve, "") ;
		}catch (Exception e) {
			throw new SeguException(e.getMessage());
		}
	}
	public void guardarCveCesEncriptada(String esLogin, String esCve, String solicitaCveCDer) throws SeguException{
		log.info("guardarCveCesEncriptada(E)");
		String lsQrySentencia = null;
		String Cve_encriptada = "";
		boolean ok = true;
		CConexionDBPool lodbConexion = new CConexionDBPool();
		try {
			ResultSet lrsSentencia = null;
			lodbConexion.conectarDB();

			lsQrySentencia = "select ENCRIPTA_PWD(?,?) from dual";

			PreparedStatement ps = lodbConexion.queryPrecompilado(lsQrySentencia);
			ps.setString(1, esLogin);
			ps.setString(2, esCve);
			lrsSentencia = ps.executeQuery();

			if (lrsSentencia.next()){
				Cve_encriptada = lrsSentencia.getString(1);
			}

			lrsSentencia.close();
			ps.close();

			lsQrySentencia = "insert into seg_cve_cesion(ic_usuario, cg_cve_confirmacion, cs_req_cve_cder) values (?,?,?)";

			ps = lodbConexion.queryPrecompilado(lsQrySentencia);
			ps.setString(1, esLogin);
			ps.setString(2, Cve_encriptada);
			ps.setString(3, solicitaCveCDer);
			ps.executeUpdate();
			ps.close();
			
			lsQrySentencia = "INSERT INTO bit_req_cve_cder "+
						" (ic_usuario, df_cambio, cg_movimiento ) " +
						" VALUES (?, sysdate, ? ) ";
			
			ps = lodbConexion.queryPrecompilado(lsQrySentencia);
			ps.setString(1, esLogin);
			ps.setString(2, solicitaCveCDer);
			ps.executeUpdate();
			ps.close();

		}catch (Exception e) {
			ok = false;
			log.error("guardarCveCesEncriptada(Error) ", e);
			throw new SeguException(e.getMessage());
		} finally {
			//lodbConexion.terminaTransaccion();
			lodbConexion.cerrarDB(ok);
			log.info("guardarCveCesEncriptada(S)");
		}
	}

	/*********************************************************************************************
	*
	*	 	    boolean vvalidarCveConfirmacion()
	*
	*********************************************************************************************/
	public boolean vvalidarCveConfirmacion(String esLogin, List lvalores) throws SeguException{
		log.info("vvalidarCveConfirmacion(E)");
		String lsQrySentencia = null;
		String Cve_desencriptada = "";
		String cuno="", cdos="", ctres="";
		int hcuno=0, hcdos=0, hctres=0;
		boolean ok = true;
		CConexionDBPool lodbConexion = new CConexionDBPool();
		try {
			ResultSet lrsSentencia = null;
			lodbConexion.conectarDB();

			lsQrySentencia = "select DESENCRIPTA_PWD(?) from dual";
			PreparedStatement ps = lodbConexion.queryPrecompilado(lsQrySentencia);
			ps.setString(1, esLogin);
			lrsSentencia = ps.executeQuery();

			if (lrsSentencia.next()){
				Cve_desencriptada = lrsSentencia.getString(1);
			}
			lrsSentencia.close();
			ps.close();

			cuno = lvalores.get(0).toString();
			cdos = lvalores.get(1).toString();
			ctres = lvalores.get(2).toString();
			hcuno = Integer.parseInt(lvalores.get(3).toString());
			hcdos = Integer.parseInt(lvalores.get(4).toString());
			hctres = Integer.parseInt(lvalores.get(5).toString());

			String u = Cve_desencriptada.substring(hcuno,hcuno+1);
			String d = Cve_desencriptada.substring(hcdos,hcdos+1);
			String t = Cve_desencriptada.substring(hctres,hctres+1);

			if(cuno.equals(u)){
				if(cdos.equals(d)){
					if(ctres.equals(t))
						ok = true;
					else
						ok = false;
				}else
					ok = false;
			}else
				ok = false;

		}catch (Exception e) {
			ok = false;
			log.error("vvalidarCveConfirmacion(Error)", e);
			throw new SeguException(e.getMessage());
		} finally {
			lodbConexion.cerrarDB(true);
			log.info("vvalidarCveConfirmacion(S)");
		}
		return ok;
	}

	/*********************************************************************************************
	*
	*	 	    void actualizarEstatusCorreo()
	*
	*********************************************************************************************/
	public void actualizarEstatusCorreo(String esLogin, String correo) throws SeguException{
		log.info("actualizarEstatusCorreo(E)");
		String lsQrySentencia = null;
		boolean ok = true;
		CConexionDBPool lodbConexion = new CConexionDBPool();
		try {
			lodbConexion.conectarDB();

			lsQrySentencia = "update seg_cve_cesion " +
							" set cs_estatus_mail = ? " +
							" where ic_usuario = ?";
			PreparedStatement ps = lodbConexion.queryPrecompilado(lsQrySentencia);
			ps.setString(1, correo);
			ps.setString(2, esLogin);
			ps.executeUpdate();
			ps.close();

		}catch (Exception e) {
			ok = false;
			log.error("actualizarEstatusCorreo(Error): ", e);
			throw new SeguException(e.getMessage());
		} finally {
			//lodbConexion.terminaTransaccion();
			lodbConexion.cerrarDB(ok);
			log.info("actualizarEstatusCorreo(S)");
		}
	}

	/*********************************************************************************************
	*
	*	 	    boolean vvalidarEstatusCorreo()
	*
	*********************************************************************************************/
	public boolean vvalidarEstatusCorreo(String esLogin) throws SeguException{
		log.info("vvalidarEstatusCorreo(E)");
		String lsQrySentencia = null;
		String mail = "";
		boolean ok = false;
		CConexionDBPool lodbConexion = new CConexionDBPool();
		try {
			ResultSet lrsSentencia = null;
			lodbConexion.conectarDB();

			lsQrySentencia = "SELECT cs_estatus_mail " +
				" FROM seg_cve_cesion " +
				" WHERE ic_usuario = ?";

			PreparedStatement ps = lodbConexion.queryPrecompilado(lsQrySentencia);
			ps.setString(1, esLogin);
			lrsSentencia = ps.executeQuery();

			if (lrsSentencia.next()){
				mail = lrsSentencia.getString("cs_estatus_mail");
			}
			if(!mail.equals("")){
				if(mail.equals("S"))
					ok = true;
			}
			ps.close();
		}catch (Exception e) {
			log.error("vvalidarEstatusCorreo(Error): ", e);
			throw new SeguException(e.getMessage());
		} finally {
			lodbConexion.cerrarDB(true);
			log.info("vvalidarEstatusCorreo(S)");
		}
		return ok;
	}

	/**************************************************
	 *
	 *	List getDatosPyME()
	 *
	 *****************************************************/
	public List getDatosPyME(String claveAfiliado) throws SeguException {
		log.info("getDatosPyME(E)");
		CConexionDBPool 			con				= null;
        String              qrySentencia    = "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		List 				renglones		= new ArrayList();
		try {
			con = new CConexionDBPool();
			con.conectarDB();

				qrySentencia =
				" SELECT cr.ic_nafin_electronico, p.cg_razon_social, p.cg_rfc,"   +
				"        p.in_numero_sirac"   +
				"   FROM comcat_pyme p, comrel_nafin cr"   +
				"  WHERE cr.ic_epo_pyme_if = p.ic_pyme AND p.ic_pyme = ?"   +
				"        AND cr.cg_tipo = 'P'"  ;

			log.debug("qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, claveAfiliado);
			rs = ps.executeQuery();
			if(rs.next()) {
				String ic_nafin_electronico = rs.getString(1)==null?"":rs.getString(1);
				String cg_razon_social 	= rs.getString(2)==null?"":rs.getString(2);
				String cg_rfc 	        = rs.getString(3)==null?"":rs.getString(3);
				String in_numero_sirac  = rs.getString(4)==null?"":rs.getString(4);
				renglones.add(ic_nafin_electronico);
				renglones.add(cg_razon_social);
				renglones.add(cg_rfc);
				renglones.add(in_numero_sirac);

				log.debug("renglones::::::: "+renglones);
			}//if(rs.next())
			rs.close();ps.close();
		} catch(Exception e) {
			log.error("getDatosPyME(Exception) ",e);
			throw new SeguException(e.getMessage());
		} finally {
			con.cerrarDB(true);
			log.info("getDatosPyME(S)");
		}
		return renglones;
	}//getTipoIntermediario



	/**
	 * Obtiene la clave de tipo de afiliado, con base en la clave
	 * de perfil especificada.
	 *
	 * @param esCvePerfil	Clave del perfil
	 *
	 * @return Cadena con la clave del tipo de afiliado de acuerod al perfil especificado.
	 * Actualmente no hay una tabla donde esten estos valores:
	 * 1.-EPO
	 * 2.-IF
	 * 3.-PYME
	 * 4.-NAFIN
	 * 5.-DISTRIBUIDOR (OFIN)
	 * 6.-CONTRAGARANTE
	 * 7.-EX EMPLEADO
	 * 8.-BANCOMEXT
	 * 9.-EXPORTADOR
	 *
	 *
	 */
	public String getTipoAfiliadoXPerfil(String esCvePerfil)
			throws SeguException {

		CPerfil 	loPerfil = null;

		try{

			loPerfil = m_oLPerfil.buscarPerfil(esCvePerfil);
			//---------------------
			//Si el perfil recibido como par�metro no existe marca error
			if (loPerfil == null) throw new SeguException(CConst.SEG_ERRC_PERFIL_NO_ASIG);
			//---------------------
			return String.valueOf(loPerfil.getTipoAfiliado());
		}catch(SeguException e){
				throw e;
		} catch(Exception e) {
			throw new SeguException(e.getMessage());
		} finally{
		}
	}



	/********************************************
	 *
	 *	void eliminaCesion()
	 *
	 **********************************************/
	public void eliminaCesion(String cg_login) throws SeguException{
    	String qry = "";
    	CConexionDBPool con = new CConexionDBPool();
    	boolean ok = true;
		PreparedStatement	ps				= null;
    	try{

    		con.conectarDB();
    		qry = "Delete seg_cve_cesion " +
    			" WHERE ic_usuario = ?";


    		ps = con.queryPrecompilado(qry);
			ps.setString(1, cg_login);
			ps.executeUpdate();

		}catch (Exception e){
			ok = false;
			log.error("eliminaCesion(Error)", e);
			throw new SeguException(e.getMessage());
		}finally{
			//con.terminaTransaccion(ok);
			con.cerrarDB(ok);
			log.info("eliminaCesion(S)");
        }
    }

//	public List getCuentasNECompras(String rfc, String numeroNE) {
//		//**********************************Validaci�n de parametros:*****************************
//		try {
//			if (rfc == null && numeroNE == null) {
//				throw new Exception("Los parametros no pueden ser nulos");
//			}
//		} catch(Exception e) {
//			log.error("Error en los parametros recibidos. " + e.getMessage() + "\n" +
//					" rfc=" + rfc + "\n" +
//					" numeroNE=" + numeroNE + "\n");
//			throw new AppException("Error Inesperado", e);
//		}
//		//***************************************************************************************
//
//		AccesoDB con = new AccesoDB();
//		try {
//			con.conexionDB();
//			StringBuffer strSQL = new StringBuffer(
//					"SELECT c.cg_appat as contacto_appat, c.cg_apmat as contacto_apmat,   " +
//					"c.cg_nombre as contacto_nombre, c.cg_email as email_contacto,  " +
//					"p.cg_razon_social, p.cg_appat, p.cg_apmat, p.cg_nombre, p.cg_rfc,  " +
//					"p.cs_tipo_persona  " +
//					"FROM comcat_pyme p, com_contacto c, comrel_nafin n  " +
//					"WHERE   " +
//					"p.ic_pyme = c.ic_pyme " +
//					"AND p.ic_pyme = n.ic_epo_pyme_if " +
//					"AND n.cg_tipo = ?  " +
//					"AND c.cs_primer_contacto = ? ");
//
//			List varBind = new ArrayList();
//			varBind.add("P");	//Tipo Pyme
//			varBind.add("S");	//Primer contacto=S
//
//			if (rfc != null && !rfc.equals("")) {
//					strSQL.append(" AND p.cg_rfc = ? ");
//					varBind.add(rfc);
//			}
//			if (numeroNE != null && !numeroNE.equals("")) {
//					strSQL.append(" AND n.ic_nafin_electronico = ?  ");
//					varBind.add(numeroNE);
//			}
//
//			Registros reg = con.consultarDB(strSQL.toString(),varBind);
//
//			return new ArrayList();
//		} catch(Exception e) {
//			throw new AppException("Error al consultar las cuentas de usuario.", e);
//		} finally {
//			if (con.hayConexionAbierta()) {
//				con.cierraConexionDB();
//			}
//		}
//
//	}

	public void transferirPrivilegios(String loginOrigen, String loginDestino, String claveUsuario) {
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (loginOrigen == null || loginDestino == null || claveUsuario == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" loginOrigen=" + loginOrigen + "\n" +
					" loginDestino=" + loginDestino + "\n" +
					" claveUsuario=" + claveUsuario);
			throw new AppException("Error Inesperado", e);
		}
		//***************************************************************************************


		try {
			UtilUsr utilUsr = new UtilUsr();
			Usuario usrOrigen = utilUsr.getUsuario(loginOrigen);
			Usuario usrDestino = utilUsr.getUsuario(loginDestino);

			if (usrOrigen != null && usrDestino != null) {

				//Se deshabilita el usuario Origen en N@E
				utilUsr.inhabilitarUsuario(loginOrigen);

				//Se habilita el usuario destino en N@E
				utilUsr.habilitarUsuario(loginDestino, usrOrigen.getClaveAfiliado(),
						usrOrigen.getTipoAfiliado(), usrOrigen.getPerfil());

				AccesoDB con = new AccesoDB();
				boolean bOk = true;
				try {
					con.conexionDB();
					String strSQL =
							" SELECT ic_nafin_electronico " +
							" FROM comrel_nafin " +
							" WHERE ic_epo_pyme_if = ? " +
							" AND cg_tipo =  ? ";
					List varBind = new ArrayList();
					varBind.add(usrOrigen.getClaveAfiliado());
					varBind.add(usrOrigen.getTipoAfiliado());

					Registros reg = con.consultarDB(strSQL, varBind);
					String numeroElectronico = "";
					if (reg.next()) {
						numeroElectronico = reg.getString("ic_nafin_electronico");
					} else {
						throw new AppException("No existe el numero de nafin electronico");
					}

					Bitacora.grabarEnBitacora(con, "TRANSFPRIV", usrOrigen.getTipoAfiliado(),
							numeroElectronico, claveUsuario,
							"Cuenta usuario N@E=" + loginOrigen, "Cuenta usuario N@E=" + loginDestino);
				} catch(Exception e ) {
					bOk = false;
					throw new AppException();
				}	finally {
					if (con.hayConexionAbierta()) {
						con.terminaTransaccion(bOk);
						con.cierraConexionDB();
					}
				}
			}

		} catch(Exception e) {
			throw new AppException("Error al transferir los privilegios de una cuenta a otra", e);
		}

	}

	/**
	 * M�todo que se encarga de validar de las tablas del SIAG la clave de contragarante (numero FISO)
	 * asociado a un intermediario financiero.
	 * @param claveUsuario Clave interna del intermediario financiero.
	 * @return claveContragarante Clave del contragarante.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	public boolean validaNumeroFISO(String claveUsuario) {
		log.info("validaNumeroFISO(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		String claveContragarante = "";
		boolean hayNumeroFISO = false;
		try {
			con.conexionDB();

			strSQL.append(" SELECT fso_clave AS clave_contragarante");
			strSQL.append(" FROM siag_fiso_intermediarios sfi");
			strSQL.append(" , comcat_if cif");
			strSQL.append(" WHERE sfi.clave = cif.ic_if_siag");
			strSQL.append(" And cif.ic_if = ?");

			varBind.add(new Integer(claveUsuario));

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			if (rst.next()) {
				claveContragarante = rst.getString("clave_contragarante")==null?"":rst.getString("clave_contragarante");
			}
			rst.close();
			pst.close();

			if (!claveContragarante.equals("")) {
				hayNumeroFISO = true;
			}
		} catch(Exception e) {
			transactionOk = false;
			e.printStackTrace();
			throw new AppException("validaNumeroFISO(ERROR) ::..", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("validaNumeroFISO(S) ::..");
		}
		return hayNumeroFISO;
	}

//Fodea 024-2011 DLHC (E)
/**
	 * metodo que valida la carga Masiva de los Usuarios Para NAfin
	 * @return
	 * @param tipoUsuario
	 * @param perfilC
	 * @param archivo
	 */
public List validaCargaMasiva( String archivo, String tipoAfiliado) {

	log.info("validaCargaMasiva(E)");

	AccesoDB con = new AccesoDB();
	List listas = new ArrayList();
	List Totalerrores = new ArrayList();
	List Totaldatosbien = new ArrayList();
	List errores = new ArrayList();

	String nombre="", paterno="", materno ="", email ="", perfil, noSiac="";
	/*List erroresN0  = new ArrayList(), erroresPA = new ArrayList();
	List erroresMA = new ArrayList(),  erroresE = new ArrayList();
	List erroresPE = new ArrayList(),  erroresSI = new ArrayList();
	*/
	List datosbienNO = new ArrayList(), datosbienPA = new ArrayList();
	List datosbienMA = new ArrayList(),  datosbienE = new ArrayList();
	List datosbienPE = new ArrayList(),  datosbienSI = new ArrayList();
	List lineaError = new ArrayList(),   lineasError = new ArrayList();
	List TlineasError = new ArrayList();  List datosbienCE = new ArrayList();
	List TparaGuardar = new ArrayList(), TTparaGuardar = new ArrayList();
	List Errorlineas = new ArrayList(),  paraGuardar = new ArrayList();

	//String RefString="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890";
	List datosOK = new ArrayList();
	List TdatosOK = new ArrayList();
	List TdatosOKs = new ArrayList();

	try{
		con.conexionDB();

				String 		lsLinea 			= "";
				int iNumLinea = 0;
				VectorTokenizer vt = null;
				Vector camposLineaArchivo = null;

				// ABRIR ARCHIVO CARGADO
				java.io.File 	lofArchivo 				= new java.io.File(archivo);
				BufferedReader 	br 						= new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo),"ISO-8859-1"));

			while((lsLinea=br.readLine())!=null) {

					iNumLinea ++;
					vt = new VectorTokenizer(lsLinea,"|");
					camposLineaArchivo = vt.getValuesVector();

						Totalerrores = new ArrayList();
				    Totaldatosbien  = new ArrayList();
				    TTparaGuardar = new ArrayList();
						TlineasError = new ArrayList();
						TdatosOK  = new ArrayList();

					   log.debug("camposLineaArchivo--->"+camposLineaArchivo);

							for(int i = 0; i < camposLineaArchivo.size()-1; i++) 	{

					      Errorlineas = new ArrayList();
							//valida que los campos sean los correcto
								if(tipoAfiliado.equals("NAFIN")){
									if(camposLineaArchivo.size()>6){
										Errorlineas.add(Integer.toString(iNumLinea));
										Errorlineas.add("Campos");
										Errorlineas.add("No cuenta con la cantidad de campos requeridos");
								 }else  if(camposLineaArchivo.size()<6){
										Errorlineas.add(Integer.toString(iNumLinea));
										Errorlineas.add("Campos");
										Errorlineas.add("No cuenta con la cantidad de campos requeridos");
								 }
								}else{
									if(camposLineaArchivo.size()>5){
											Errorlineas.add(Integer.toString(iNumLinea));
											Errorlineas.add("Campos");
											Errorlineas.add("No cuenta con la cantidad de campos requeridos");
									}else if(camposLineaArchivo.size()<5){
											Errorlineas.add(Integer.toString(iNumLinea));
											Errorlineas.add("Campos");
											Errorlineas.add("No cuenta con la cantidad de campos requeridos");
									}
								}
						/*
							erroresN0 = new ArrayList();  erroresPA = new ArrayList(); 	erroresMA = new ArrayList();
							erroresE = new ArrayList(); erroresPE = new ArrayList(); 	erroresSI = new ArrayList();
							*/
							datosbienNO = new ArrayList(); 	datosbienPA = new ArrayList(); 	datosbienMA = new ArrayList();
							datosbienE = new ArrayList(); datosbienPE = new ArrayList(); 	datosbienSI = new ArrayList();
							paraGuardar  = new ArrayList();  lineaError = new ArrayList();  datosbienCE = new ArrayList();


						  nombre   = (camposLineaArchivo.size()>= 1)?camposLineaArchivo.get(0)==null?"":((String)camposLineaArchivo.get(0)).trim():"";
							paterno  = (camposLineaArchivo.size()>= 2)?camposLineaArchivo.get(1)==null?"":((String)camposLineaArchivo.get(1)).trim():"";
							materno  = (camposLineaArchivo.size()>= 3)?camposLineaArchivo.get(2)==null?"":((String)camposLineaArchivo.get(2)).trim():"";
							email    = (camposLineaArchivo.size()>= 4)?camposLineaArchivo.get(3)==null?"":((String)camposLineaArchivo.get(3)).trim():"";
							perfil   = (camposLineaArchivo.size()>= 5)?camposLineaArchivo.get(4)==null?"":((String)camposLineaArchivo.get(4)).trim():"";
							if(tipoAfiliado.equals("NAFIN")){
							noSiac   = (camposLineaArchivo.size()>= 6)?camposLineaArchivo.get(5)==null?"":((String)camposLineaArchivo.get(5)).trim():"";
							}
							perfil = perfil.toUpperCase();


								lineaError.add(nombre);
								lineaError.add(paterno);
								lineaError.add(materno);
								lineaError.add(email);
								lineaError.add(perfil);
								lineaError.add(noSiac);

					if(Errorlineas.size()==0) {

					//NOMBRE
						if(nombre.equals("")){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Nombre");
							Errorlineas.add("El campo Nombre es obligatorio. Favor de capturarlo.");
						}else if(nombre.length()>40){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Nombre");
							Errorlineas.add("El campo Nombre rebasa la cantidad de caracteres permitida (40).");
						}else{
							datosbienNO.add(Integer.toString(iNumLinea));
							datosbienNO.add("Nombre");
							datosbienNO.add(nombre);
							paraGuardar.add(nombre);
						}

						//APELLIDO PARTERNO
						if(paterno.equals("")){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Apellido Paterno");
							Errorlineas.add("El campo Apellido Paterno es obligatorio. Favor de capturarlo.");
						}else if(paterno.length()>40){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Apellido Paterno");
							Errorlineas.add("El campo Apellido Paterno rebasa la cantidad de  caracteres (40).");
						}else{
							datosbienPA.add(Integer.toString(iNumLinea));
							datosbienPA.add("Apellido Paterno");
							datosbienPA.add(paterno);
							paraGuardar.add(paterno);
					}
						//APELLIDO MATERNO
						if(materno.equals("")){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Apellido Materno");
							Errorlineas.add("El campo Apellido Materno es obligatorio. Favor de capturarlo.");
						}else if(materno.length()>40){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Apellido Materno");
							Errorlineas.add("El campo Apellido Materno rebasa la cantidad de  caracteres (40).");
						}else{
							datosbienMA.add(Integer.toString(iNumLinea));
							datosbienMA.add("Apellido Materno");
							datosbienMA.add(materno);
							paraGuardar.add(materno);
					}

						//EMAIL
						if(email.equals("")){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Email");
							Errorlineas.add("El campo Email es obligatorio. Favor de capturarlo..");
						}else if(email.length()>100){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Email");
							Errorlineas.add("El campo Email rebasa la cantidad de  caracteres (100).");
						}	else if(Comunes.validaEmail(email)==false){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Email");
							Errorlineas.add("El formato del campo Email no es correcto. Formato: xxx@xxxx.xx.");
						}else{
							datosbienE.add(Integer.toString(iNumLinea));
							datosbienE.add("Email");
							datosbienE.add(email);
							paraGuardar.add(email);
   					}

						//PERFIL
							if(perfil.equals("")){
								Errorlineas.add(Integer.toString(iNumLinea));
								Errorlineas.add("Perfil");
								Errorlineas.add("El campo Perfil es obligatorio. Favor de capturarlo..");
						}else if(perfil.length()>15){
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Perfil");
							Errorlineas.add("El campo Perfil rebasa la cantidad de  caracteres (15).");
						}	else if(validaPerfil(perfil, tipoAfiliado)==false) {
							Errorlineas.add(Integer.toString(iNumLinea));
							Errorlineas.add("Perfil");
							Errorlineas.add("El campo Perfil no es un perfil valido. Favor de capturar el correcto.");
						}else{
							datosbienPE.add(Integer.toString(iNumLinea));
							datosbienPE.add("Perfil");
							datosbienPE.add(perfil);
							paraGuardar.add(perfil);
						}

						//NUMERO DE SIRAC
						if(tipoAfiliado.equals("NAFIN")){
							if(noSiac.equals("")){
								Errorlineas.add(Integer.toString(iNumLinea));
								Errorlineas.add("N�mero SIRAC");
								Errorlineas.add("El campo N�mero SIRAC es obligatorio para el tipo de usuario NAFIN. Favor de capturarlo.");
							}else if(noSiac.length()>20){
								Errorlineas.add(Integer.toString(iNumLinea));
								Errorlineas.add("N�mero SIRAC");
								Errorlineas.add("El campo N�mero SIRAC rebasa la cantidad de  caracteres (15).");
							}else{
								datosbienSI.add(Integer.toString(iNumLinea));
								datosbienSI.add("N�mero SIRAC");
								datosbienSI.add(noSiac);
								paraGuardar.add(noSiac);
						}
						}	//if(perfilC.equals("NAFIN")){

					 } //If de Errores

						}//for(int i = 0; i < camposLineaArchivo.size()-1; i++) 	{

						 datosOK = new ArrayList();
						 	if(Errorlineas.size()==0){
							 datosOK.add(Integer.toString(iNumLinea));
							 datosOK.add("OK");
							 TdatosOK.add(datosOK);
						 }

						// if(Errorlineas.size()>0){  	Totalerrores.add(Errorlineas); }

						 //if ( erroresN0.size()==0 && erroresPA.size()==0  && erroresMA.size()==0  && erroresE.size()==0 && erroresPE.size()==0  && erroresSI.size()==0 ){
						 if(Errorlineas.size()==0){
							 if(datosbienNO.size()>0){  	Totaldatosbien.add(datosbienNO);  }
							 if(datosbienPA.size()>0){  	Totaldatosbien.add(datosbienPA);  }
							 if(datosbienMA.size()>0){  	Totaldatosbien.add(datosbienMA);  }
							 if(datosbienE.size()>0){  	  Totaldatosbien.add(datosbienE);   }
							 if(datosbienPE.size()>0){  	Totaldatosbien.add(datosbienPE);  }
							 if(datosbienSI.size()>0){  	Totaldatosbien.add(datosbienSI);  }
						}

					//si al menos tienes una error
						 if(Errorlineas.size()>0){
					       Totalerrores.add(Errorlineas);
								 lineasError.add(lineaError);

						}

					 if(Errorlineas.size()==0){
							if(paraGuardar.size()>0){  TparaGuardar.add(paraGuardar);  }
						}

					 if(TparaGuardar.size()>0){  TTparaGuardar.add(TparaGuardar);  }
					 if(Totalerrores.size()>0){   errores.add(Totalerrores);  }
					 if(TdatosOK.size()>0){  TdatosOKs.add(TdatosOK);  }
					 if(lineasError.size()>0){  TlineasError.add(lineasError);  }



			}//while

			listas.add(TTparaGuardar);
			listas.add(errores);
			listas.add(TdatosOKs);
			listas.add(TlineasError);


		//log.debug("TTparaGuardar "+TTparaGuardar);
		//log.debug("TlineasError "+TlineasError);
		log.debug("TdatosOKs "+TdatosOKs);
		//log.debug("listas "+listas);

		return listas;

	}catch(Exception e){
		e.printStackTrace();
		throw new AppException("Error al validar el Archivo de Carga ",e);
	}finally{
		if(con.hayConexionAbierta()){
			con.terminaTransaccion(true);
			con.cierraConexionDB();
		}
		log.info("validaCargaMasiva(S)");
	}
}


/** metodo que valida si el perfil que se esta capturan es correcto
	 * @return
	 * @param tipoUsuario
	 * @param perfil
	 */
private static boolean validaPerfil(String perfil, String tipoAfiliado){

	log.info("validaPerfil(E)");

	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	PreparedStatement ps = null;
	boolean  valor = false;

	try{
		con.conexionDB();

		StringBuffer	sQuery	= new StringBuffer();

		sQuery.append( " SELECT cc_perfil ");
		sQuery.append( " FROM seg_perfil pp     ");
		sQuery.append( " WHERE  cc_perfil = ?  ");
		if (tipoAfiliado.equals("NAFIN") ){
		sQuery.append( " AND sc_tipo_usuario= 4 ");
		}
		if (tipoAfiliado.equals("EPO") ){
		sQuery.append( " AND sc_tipo_usuario= 1 ");
		}
		if (tipoAfiliado.equals("IF") ){
		sQuery.append( " AND sc_tipo_usuario= 2 ");
		}
		sQuery.append( " ORDER BY cc_perfil   ");

		ps = con.queryPrecompilado(sQuery.toString());
		ps.setString (1, perfil);
		rs = ps.executeQuery();
		if(rs.next()) {
			valor = true;
		}
		rs.close();
		ps.close();

		return valor;

	}catch(Exception e){
		e.printStackTrace();
		throw new AppException("Error al validar el Perfil ",e);
	}finally{
		if(con.hayConexionAbierta()){
			con.terminaTransaccion(true);
			con.cierraConexionDB();
		}
		log.info("validaPerfil(S)");
	}
}


/**
	 *metodo que me regresa el nombre del Afiliado cuando
	 * este es EPO o IF
	 * @return
	 * @param tipoAfiliado
	 * @param claveAfiliado
	 */
 public String  nombreAfiliado(String claveAfiliado, String tipoAfiliado){

	log.info("nombreAfiliado(E)");

	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	PreparedStatement ps = null;
	StringBuffer	sQuery	= new StringBuffer();
	String nombre	 ="";

	try{
		con.conexionDB();

		sQuery	= new StringBuffer();
		if(tipoAfiliado.equals("EPO")){
			sQuery.append( " SELECT cg_razon_social as nombre  FROM  comcat_epo ");
			sQuery.append( "  WHERE ic_epo = "+claveAfiliado);
		}
		if(tipoAfiliado.equals("IF")){
			sQuery.append( " SELECT cg_razon_social as nombre  FROM  comcat_if ");
			sQuery.append( "  WHERE ic_if = "+claveAfiliado);
		}
		ps = con.queryPrecompilado(sQuery.toString());
		rs = ps.executeQuery();
		while(rs.next()) {
			nombre = rs.getString("nombre")==null?"":rs.getString("nombre");
		}
		rs.close();
		ps.close();

		return nombre;

	}catch(Exception e){
		e.printStackTrace();
		throw new AppException("Error al consultar el nombre del Afiliado ",e);
	}finally{
		if(con.hayConexionAbierta()){
			con.terminaTransaccion(true);
			con.cierraConexionDB();
		}
		log.info("nombreAfiliado(S)");
	}
}

/**
	 * metodo en donde capturo temporalmente los Usuarios antes del Acuse
	 * @return
	 * @param clave_afiliado
	 * @param tipoAfiliado
	 * @param resultados
	 */

public String tmpUsarios(List resultados, String  tipoAfiliado,  String  clave_afiliado){

	log.info("tmpUsarios(E)");

	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	PreparedStatement ps = null;
	StringBuffer	sQuery	= new StringBuffer();
  String numero ="", nombre =  "",  apellidoPaterno =  "", apellidoMaterno = "",
	 email = "", perfil = "", sirac ="";


	try{
		con.conexionDB();

		//obtengo el numero consecutivo
		sQuery	= new StringBuffer();
		sQuery.append( " select SEQ_com_tmp_usuarios.nextval as valor  from dual ");
		ps = con.queryPrecompilado(sQuery.toString());
		rs = ps.executeQuery();
		if(rs.next()) {
			numero = rs.getString("valor");
		}
		rs.close();
		ps.close();


		for( int v = 0; v<resultados.size(); v++){
			List infor = (ArrayList)resultados.get(v);

			for(int y=0; y<infor.size(); y++) {
				List infor1 = (ArrayList)infor.get(y);
			  //log.debug("infor1--->"+infor1);

				 nombre =  (String)infor1.get(0);
				 apellidoPaterno =  (String)infor1.get(1);
				 apellidoMaterno =  (String)infor1.get(2);
				 email =  (String)infor1.get(3);
				 perfil =  (String)infor1.get(4);
				if(tipoAfiliado.equals("NAFIN")){
					 sirac = (String)infor1.get(5);
				}else{
					 sirac ="";
				}
				//cg_claveAfiliado  este es cuando es IF o EPO
				//cg_tipoAfiliado NAFIN, EPO, IF
				sQuery	= new StringBuffer();
				sQuery.append( " INSERT INTO com_tmp_usuarios ");
				sQuery.append( " ( ic_numero,  cg_tipoAfiliado , cg_claveAfiliado,  cg_nombre , cg_apellidoPaterno ,");
				sQuery.append( "   cg_apellidoMaterno  , cg_email , cg_perfil , cg_sirac ) ");
				sQuery.append( " VALUES ("+numero+",'"+tipoAfiliado+"','"+clave_afiliado+"','"+nombre+"','"+apellidoPaterno+"'," +
				 "'"+apellidoMaterno+"','"+email+"','"+perfil+"','"+sirac+"')");

				//log.debug("sQuery.toString()  "+sQuery.toString());
				ps = con.queryPrecompilado(sQuery.toString());
				ps.executeUpdate();
				ps.close();
			}
		}
		return numero;

	}catch(Exception e){
		e.printStackTrace();
		throw new AppException("Error al inserta los Usuarios en tabla temporal ",e);
	}finally{
		if(con.hayConexionAbierta()){
			con.terminaTransaccion(true);
			con.cierraConexionDB();
		}
		log.info("tmpUsarios(S)");
	}
}

/***  metodo que regresa los registros dados de alta
	 * @return
	 * @param numero
	 */

public List getUsarios(String numero){

	log.info("getUsarios(E)");

	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	PreparedStatement ps = null;
	StringBuffer	sQuery	= new StringBuffer();
	List datos =  new ArrayList();
	List registros =  new ArrayList();

	try{
		con.conexionDB();

		sQuery	= new StringBuffer();
		sQuery.append( " SELECT cg_tipoAfiliado as tipoAfiliado, cg_claveAfiliado as claveAfiliado,  cg_nombre as nombre ,");
		sQuery.append( " cg_apellidoPaterno  as paterno, cg_apellidoMaterno  as materno , cg_email as email , ");
		sQuery.append( " cg_perfil as perfil, cg_sirac as siac  ");
		sQuery.append( "  FROM  com_tmp_usuarios ");
		sQuery.append( "  WHERE ic_numero = "+numero);

		ps = con.queryPrecompilado(sQuery.toString());
		rs = ps.executeQuery();
		while(rs.next()) {
		  datos =  new ArrayList();
			datos.add(rs.getString("tipoAfiliado")==null?"":rs.getString("tipoAfiliado"));
			datos.add(rs.getString("claveAfiliado")==null?"":rs.getString("claveAfiliado"));
			datos.add(rs.getString("nombre")==null?"":rs.getString("nombre"));
			datos.add(rs.getString("paterno")==null?"":rs.getString("paterno"));
			datos.add(rs.getString("materno")==null?"":rs.getString("materno"));
			datos.add(rs.getString("email")==null?"":rs.getString("email"));
			datos.add(rs.getString("perfil")==null?"":rs.getString("perfil"));
			datos.add(rs.getString("siac")==null?"":rs.getString("siac"));
			registros.add(datos);
		}
		rs.close();
		ps.close();


		return registros;

	}catch(Exception e){
		e.printStackTrace();
		throw new AppException("Error al Regresa los Usuarios en tabla temporal ",e);
	}finally{
		if(con.hayConexionAbierta()){
			con.terminaTransaccion(true);
			con.cierraConexionDB();
		}
		log.info("getUsarios(S)");
	}
}

/**
	 * metodo que da de alta los usuarios en ARGUS
	 * @throws com.netro.seguridadbean.SeguException
	 * @throws java.sql.SQLException
	 * @throws java.lang.Exception
	 * @return
	 * @param perfil
	 * @param email
	 * @param apellidoMaterno
	 * @param apellidoPaterno
	 * @param nombre
	 * @param claveAfiliado
	 * @param tipoAfiliado
	 */
public SolicitudArgus generarSolicitudArgus( String tipoAfiliado, String claveAfiliado,
																						 String nombre, String apellidoPaterno, String apellidoMaterno,
																						 String email, String perfil)
																						 throws	Exception,	SQLException, SeguException {
	log.info("generarSolicitudArgus(E)");

	Usuario usr = new Usuario();
	SolicitudArgus solicitud = new SolicitudArgus();


	usr.setNombre(nombre);
	usr.setApellidoPaterno(apellidoPaterno);
	usr.setApellidoMaterno(apellidoMaterno);
	usr.setEmail(email);
	usr.setClaveAfiliado(claveAfiliado);
	usr.setTipoAfiliado(tipoAfiliado);
	usr.setPerfil(perfil);

	solicitud.setUsuario(usr);

	UtilUsr utilUsr = new UtilUsr();
	String mensajeError = utilUsr.complementarSolicitudArgus(solicitud, tipoAfiliado, claveAfiliado);
	if(!mensajeError.equals("")){//Si se genera algun mensaje, debido a que algun campo de la consulta fue nulo, telefono, cp, etc
		throw new Exception(mensajeError);
	}
	log.info("generarSolicitudArgus(S)");
	return solicitud;

}


/**
	 *
	 * @throws java.lang.Exception
	 * @return
	 * @param empresa2
	 * @param claveAfiliado
	 * @param numero
	 * @param lista
	 * @param nombreCompletoUsuario
	 * @param ic_usuario
	 * @param ic_nafin
	 */
public String  getBitacora(String ic_nafin, String ic_usuario, String nombreCompletoUsuario, String lista, int numero, String claveAfiliado, String empresa2 )	throws Exception {

	AccesoDB con = new AccesoDB();
	StringBuffer sQuery	= new StringBuffer();
	String perfil ="",login ="" , nombre ="", paterno ="", materno ="", email ="";
	String datosAnteriores ="";
	PreparedStatement ps2 = null;
	String bitacora ="G";
	boolean exito = true;
	try {
		con.conexionDB();
		if(!lista.equals("")){
			List datos = new VectorTokenizer(lista, "\n").getValuesVector();
			for(int i = 0; i < datos.size(); i++) 	{
				if(datos.get(i)!=null){
					List unregistro = new VectorTokenizer((String)datos.get(i), "|").getValuesVector();
					System.out.println("unregistro -->"+unregistro);

					for(int e = 0; e < unregistro.size()-1; e++) 	{

						List dat = new VectorTokenizer((String)unregistro.get(e), ",").getValuesVector();

						System.out.println("dat) -->"+dat);

						perfil  = (String)dat.get(0);
						login   = (String)dat.get(1);
						nombre  = (String)dat.get(2);
						paterno = (String)dat.get(3);
						materno = (String)dat.get(4);
						email   = (String)dat.get(5);

						datosAnteriores = " Perfil="+perfil+" Login="+login+" Nombre="+nombre+" "+paterno+" "+materno+" Email= "+email;
						sQuery	= new StringBuffer();
						sQuery.append( " INSERT INTO bit_cambios_gral  ");
						sQuery.append( "	(ic_cambio,   ");
						sQuery.append( " cc_pantalla_bitacora,  ");
						sQuery.append( " IC_NAFIN_ELECTRONICO,  ");
						sQuery.append( " IC_USUARIO, ");
						sQuery.append( " CG_NOMBRE_USUARIO, ");
						sQuery.append( " DF_CAMBIO, ");
						sQuery.append( " CG_ANTERIOR,  ");
						sQuery.append( " CG_ACTUAL,				 ");
						sQuery.append( " CG_CLAVE_AFILIADO, ");
						sQuery.append( " IC_EPO )");

						sQuery.append( " VALUES(SEQ_BIT_CAMBIOS_GRAL.nextval,  ");
						sQuery.append( " '" + "CONSUSUARIO" + "','" + ic_nafin + "','" + ic_usuario + "','" + nombreCompletoUsuario + "',"+"Sysdate"+",'");
						sQuery.append( datosAnteriores+"','"+"ELIMINADO"+"','"+claveAfiliado+"','"+empresa2+"')");

						ps2 = con.queryPrecompilado(sQuery.toString());
						ps2.executeUpdate();
						ps2.close();

						//metodo que almacena en tabla de Argus AG_DELETEUSER
	          eliminaUsuarioArgus( login,  nombre,  paterno,  materno, email);


					}
				}
			}
		}
		return bitacora;

		} catch(SQLException sqle) {
			exito = false;
			System.out.println("Error  al  insertar  los usuarios en bit_cambios_gral y  AG_DELETEUSER "+sqle);
			throw sqle;

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}

}


/**
	 * Almacenar�  las cuentas a dar de baja  en las tablas de la base de datos de la Aplicacion Argus
	 * @throws com.netro.seguridadbean.SeguException
	 * @throws java.sql.SQLException
	 * @throws java.lang.Exception
	 * @param email
	 * @param apellidoMaterno
	 * @param apellidoPaterno
	 * @param nombre
	 * @param login
	 */

private void eliminaUsuarioArgus( String login, String nombre, String apellidoPaterno, String apellidoMaterno,	 String email) 	 throws	Exception,	SQLException, SeguException {
	log.info("eliminaUsuarioArgus(E)");

	try {

		Usuario usr = new Usuario();
		SolicitudArgus solicitud = new SolicitudArgus();
		usr.setNombre(nombre);
		usr.setApellidoPaterno(apellidoPaterno);
		usr.setApellidoMaterno(apellidoMaterno);
		usr.setLogin(login);
		usr.setEmail(email);
		solicitud.setUsuario(usr);

		UtilUsr utilUsr = new UtilUsr();
	  utilUsr.eliminarUsuario(solicitud);

	} catch(SQLException sqle) {
		System.out.println("Error  al  insertar  los usuarios en AG_DELETEUSER "+sqle);
		throw sqle;
	} finally {
		log.info("eliminaUsuarioArgus(S)");
	}

}
//Fodea 024-2011 DLHC (E)
	/**
	 * Validaci�n del usuario.
	 * @param esLoginSes Clave del usuario logeado (debe de venir de la sesi�n de HTML)
	 * @param esLogin Clave del usuario (Viene de la forma)
	 * @param esPassword Password (Viene de la forma)

	 */
public void validarUsuario(String esLoginSes, String esLogin, String esPassword)
			throws AppException
	{
		validarUsuario(esLoginSes, esLogin, esPassword, "", "", "", "", "", "N");
	}

	/**
	 * Metodo para determinar si el certificado del usuario ya fue generado
	 * o no
	 * @param usuario Login del usuario
	 * @return true Si ya existe un certificado para ese usuario o
	 * 		false de lo contrario
	 */
	public boolean certificadoGenerado(String usuario) {
		log.info("certificadoGenerado(E)");
		AccesoDB con = new AccesoDB();
		boolean certificadoGenerado = false;
		try {
			con.conexionDB();
			String strSQL =
					" SELECT count(*) " +
					" FROM users_seguridata " +
					" WHERE table_uid like UPPER(?) ";
			log.debug("certificadoGenerado::strSQL = " + strSQL);
			log.debug("certificadoGenerado::params = " + usuario);
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, usuario + "%");
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				log.debug("certificadoGenerado::" + rs.getInt(1));
				certificadoGenerado = (rs.getInt(1) > 0)?true:false;
			}
			rs.close();
			ps.close();
			return certificadoGenerado;
		} catch (Exception e) {
			throw new AppException("Error al determinar la existencia del certificado digital del usuario " + usuario, e);
		} finally {
			log.info("certificadoGenerado(S)");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Validaci�n del usuario
	 * @param esLoginSes Usuario logeado en la aplicaci�n
	 * @param esLogin	Usuario especificado en la confirmaci�n
	 * @param esPassword Password especificado en la confirmaci�n
	 * @param esIP	Parametro NO USADO
	 * @param esHost	Parametro NO USADO
	 * @param esSistema	Parametro NO USADO
	 * @param sCvePerfProt	Parametro NO USADO
	 * @param sCveFacultad	Parametro NO USADO
	 * @param validaFac Parametro NO USADO
	 */
	public void validarUsuario(String esLoginSes, String esLogin, String esPassword,
			String esIP, String esHost, String esSistema, String sCvePerfProt,
			String sCveFacultad, String validaFac)
			throws AppException
	{
		log.info(" validarUsuario(E)");

		AccesoDB lodbConexion = new AccesoDB();
		try {
			UtilUsr utils = new UtilUsr();

			//Valida el usuario ingresado con el usuario de sesion sea el mismo
			if(!esLoginSes.equalsIgnoreCase(esLogin))
				throw new AppException("Error en la validacion del usuario");

			//Valida el Usuario por medio de Seguridad, para cargar en Memoria el usuario, si es el caso
			boolean usaurioOK = utils.esUsuarioValido(esLogin, esPassword);
			if(!usaurioOK)
				throw new AppException("Error en la validacion del usuario");

			/*if("S".equals(validaFac)){
				Usuario usuario = utils.getUsuario(esLogin);
				String perfil = usuario.getPerfil();

				try{
					this.validaFacultad( (String) esLogin, (String) perfil, sCvePerfProt , sCveFacultad , (String) esSistema, (String) esIP, (String) esHost );
				}catch( Throwable ErrorSeg){
					log.info("ERROR: validaFacultad(" + esLogin + "," +
							perfil + "," + sCvePerfProt + "," + sCveFacultad + "," +
							esSistema + "," +  esIP + "," + esHost+")");
					ErrorSeg.printStackTrace();
					//throw new NafinException("DSCT0014");
					throw new AppException("Error en la validacion de facultad", ErrorSeg);
				}
			}*/

		} catch ( Throwable ErrorSeg){
			throw new AppException("Error en validacion de usuario", ErrorSeg);
		} finally {
			if (lodbConexion.hayConexionAbierta() == true)
				lodbConexion.cierraConexionDB();
			log.info("vvalidarUsuario(S)");
		}
	}

/**
 * Validacion si existe director de un area
 * @param area de la cual solicitamos la informacion
 */
public boolean existeDirector(String area)throws AppException{
	//**********************************Validaci�n de parametros:*****************************
		try {
			if (area == null ) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos. " + e.getMessage() + "\n" +
					" area=" + area );
			throw new AppException("Error Inesperado", e);
		}
		//***************************************************************************************
		AccesoDB con = new AccesoDB();
		boolean existe_director=false;
		try {
			con.conexionDB();
			StringBuffer strSQL = new StringBuffer(
					"select * from cotcat_ejecutivo where ic_area = ? and cs_director = 'S' ");

			List varBind = new ArrayList();
			varBind.add(area);

			Registros reg = con.consultarDB(strSQL.toString(),varBind);
			if(reg.next()){//ya existe un director para esta area
					existe_director=true;

				}
			return existe_director;
		} catch(Exception e) {
			throw new AppException("Error al consultar existe_director", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
}

	public String getSerialCertDigital(String noUsuari) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		String strSerial = "";
		try{
			con.conexionDB();

			strSQL = "SELECT TABLE_UID, dn_user FROM users_seguridata " +
					"    WHERE table_uid =  RPAD(?,9,' ') ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, noUsuari);
			rs = ps.executeQuery();
			if(rs.next()){
				strSerial = rs.getString("dn_user");
			}
			rs.close();
			ps.close();

			return strSerial;
		}catch(Throwable t){
			throw new AppException("Error al obtener serial de certificado digital", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}

		}
	}

	public String getFecVigenciaCertificado(String noUsuario) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";

		try{
			con.conexionDB();
			String fecVigenciaCert = "";

			strSQL = "SELECT " +
						"  to_char(cert.invalido_despues, 'dd/mm/yyyy') " +
						"FROM " +
						"  ( " +
						"    SELECT " +
						"      /*+use_nl(cer segu)*/ " +
						"      trim(TO_CHAR(segu.table_uid)) table_uid, " +
						"      cer.num_serie, " +
						"      cer.razon_social, " +
						"      cer.nombre, " +
						"      cer.rfc, " +
						"      cer.invalido_despues " +
						"    FROM certificados cer, " +
						"      users_seguridata segu " +
						"    WHERE cer.num_serie       = segu.dn_user " +
						"  ) " +
						"  cert, " +
						"  ag_person per " +
						"WHERE per.user_name = cert.table_uid " +
						"and per.user_name = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, noUsuario);
			rs = ps.executeQuery();
			if(rs.next()){
				fecVigenciaCert = rs.getString(1);
			}
			rs.close();
			ps.close();

			con.terminaTransaccion(true);

			return fecVigenciaCert;

		}catch(Throwable t ){
			throw new AppException("Error al consultar si esta vigente el certificado", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	public boolean validaFolioRevocacionVigente(String noUsuari) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		boolean fecValida = false;
		try{
			con.conexionDB();

			//pi = fr valido, fr vencido, fr no existe.

			strSQL = "SELECT folio " +
						"	FROM  com_revocacion_cert " +
						"    WHERE ic_usuario = ?  " +
						"	  AND cs_revocado = ?  " +
						"	  AND df_envio_folio+1 > sysdate";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, noUsuari);
			ps.setString(2, "N");
			rs = ps.executeQuery();
			if(rs.next()){
				fecValida = true;
			}
			rs.close();
			ps.close();

			return fecValida;
		}catch(Throwable t){
			throw new AppException("Error al veriricar si existe folio de revocacion vigente", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}

		}
	}

	public String generaFolioRevocacionCert(String noUsuario, String nafelec, String cveAfiliado)throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		boolean commit = true;

		try{
			con.conexionDB();
			String folio = "";
			String msgBit = "Revocacion Solicitada";
			int existeReg = 0;

			strSQL = "SELECT SEQ_COM_REVOCACION_CERT.NEXTVAL FROM dual ";
			ps = con.queryPrecompilado(strSQL);
			rs = ps.executeQuery();

			if(rs.next()){
				folio = rs.getString(1);
			}
			rs.close();
			ps.close();

			strSQL = "SELECT count(1) " +
					"  FROM com_revocacion_cert " +
					" WHERE ic_usuario = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1,noUsuario);
			rs = ps.executeQuery();

			if(rs.next()){
				existeReg = rs.getInt(1);
			}
			rs.close();
			ps.close();

			if(existeReg > 0){
				if(this.validaFolioRevocacionVigente(noUsuario))
					msgBit = "Folio Revocaci�n reenviado";

				strSQL = "UPDATE com_revocacion_cert " +
						"   SET folio = ?, " +
						"       df_envio_folio = SYSDATE, " +
						"       cs_revocado = ? " +
						" WHERE ic_usuario = ? ";

				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1, Long.parseLong(folio));
				ps.setString(2, "N");
				ps.setString(3, noUsuario);
				ps.executeUpdate();
				ps.close();

			}else{
				strSQL = "INSERT INTO com_revocacion_cert " +
						"            (folio, ic_usuario) " +
						"     VALUES (?, ?) ";

				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1, Long.parseLong(folio));
				ps.setString(2, noUsuario);
				ps.executeUpdate();
				ps.close();
			}

			guardaBitacoraCertDigit(con, noUsuario, nafelec, cveAfiliado, msgBit);
			envioCorreoFolioRevCert(folio,noUsuario);

			return folio;
		}catch(Throwable t){
			commit = false;
			throw new AppException("Error al generar folio de revcacion", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}

	public void removerCertificadoDigital(String noUsuario, String nafelec, String cveAfiliado)throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String strSQL = "";
		boolean commit = true;
		try{
			con.conexionDB();

			strSQL = "UPDATE com_revocacion_cert " +
						"   SET cs_revocado = ? " +
						" WHERE ic_usuario = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, "S");
			ps.setString(2, noUsuario);
			ps.executeUpdate();
			ps.close();

			strSQL = "DELETE users_seguridata " +
					"    WHERE table_uid =  RPAD(?,9,' ') ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, noUsuario);
			int i = ps.executeUpdate();
			System.out.println("num reg afectaod === "+i);
			ps.close();

			guardaBitacoraCertDigit(con, noUsuario, nafelec, cveAfiliado, "Revocacion Realizada");

		}catch(Throwable t){
			commit = false;
			throw new AppException("Error al revocar certificado digital del usuairo: "+noUsuario, t );
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}

	private void envioCorreoFolioRevCert(String folio, String noUsuario)throws AppException{
		try{
			Correo correo = new Correo();
			UtilUsr util = new UtilUsr();
			Usuario usuario= new Usuario();
			String nombreUsuario = "";
			String mail = "";

			usuario = util.getUsuario(noUsuario);
			nombreUsuario = usuario.getNombre()+" "+usuario.getApellidoMaterno()+" "+usuario.getApellidoPaterno();
			mail = usuario.getEmail();

			String msgHTML =	"\n <html>  "  +
									"\n   <head>  "  +
									"\n     <title>Carga de Documentos para tramites EPOs</title>  "  +
									"\n     <style type=\"text/css\"> " +
									"\n       <!-- " +
									"\n         body { font-family: Tahoma, arial; font-size: 10pt; }" +
									"\n         .titulo {font-weight: bold; font-size:12px; }" +
									"\n         .style2 {color: #FFFFFF; font-weight: bold; font-size:10px; }" +
									"\n	        .style3 {color: #000000; font-weight: bold; font-size:10px; TEXT-ALIGN: center;}" +
									"\n			  .dataTextLeft2 { FONT-WEIGHT: bolder;BACKGROUND-COLOR: #666699; FONT-SIZE: 12px; COLOR: white; FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif; " +
									"\n				TEXT-ALIGN: center;		TEXT-TRANSFORM: uppercase;}"+
									"\n       --> " +
									"\n     </style> " +
									"\n   </head>  "  +
									"\n   <body>  "  +
									"\n     <table border=\"0\">  "  +
									"\n       <tr>       "  +
									"\n         <td style=\"font-family:arial, 'lucida console', sans-serif; font-size: 10pt;text-align:justify;\"><br/><b>Estimado Usuario(a): "+nombreUsuario+"</b><br/>Por este conducto Nacional Financiera te ha asignado el siguiente FOLIO DE SEGURIDAD con el cual podr&aacute;s concluir la revocaci&oacute;n de tu Certificado Digital.</td>  "  +
									"\n       </tr>  "  +
									"\n       <tr>       "  +
									"\n         <td><br/>	" +
									"\n     <table border=\"1\">  "  +
									"\n       <tr>       "  +
									"\n         <td class=\"dataTextLeft2\">FOLIO DE SEGURIDAD</td>  "  +
									"\n       </tr>  " +
									"\n       <tr>       "  +
									"\n         <td class=\"style3\">"+folio+"</td>  "  +
									"\n       </tr>  " +
									"\n     	</table>  " +
									"\n     </td>  " +
									"\n     </tr>  "  +
									"\n       <tr>       "  +
									"\n         <td style=\"font-family:arial, 'lucida console', sans-serif; font-size: 10pt;text-align:justify;\"><br/>Favor de ingresar a NAFINET para continuar con el proceso.<br/></td>  "  +
									"\n       </tr>  "  +
									"\n         <td class=\"titulo\"><br/>ATENTAMENTE</td>  "  +
									"\n       <tr>       "  +
									"\n         <td>Coordinaci&oacute;n de Administraci&oacute;n de Productos Electr&oacute;nicos.</td>  "  +
									"\n       </tr>  "  +
									"\n       </tr>  "  +
									"\n       <tr>       "  +
									"\n         <td style=\"font-family:arial, 'lucida console', sans-serif; font-size: 10pt;text-align:justify;\"><br/><br/><span class=\"titulo\"><b>Nota:</b>&nbsp;Este mensaje fue enviado desde una cuenta " +
									"\n             de notificaciones que no puede aceptar correos electr&oacute;nicos de entrada. <br/>Favor de no responder este mensaje.</span></td>  "  +
									"\n       </tr>  "  +
									"\n     </table>  "  +
									"\n   </body>" +
									"\n </html>";

			correo.enviarTextoHTML("no_response@nafin.gob.mx", mail, "Folio de Seguridad", msgHTML);
		}catch(Throwable t){
			throw new AppException("Error al enviar correo con el folio para confirmacion de revocacion de certificado", t);
		}
	}

	public boolean validaFolioCert(String folio, String noUsuario)throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		boolean folioCorrecto = false;
		try{
			con.conexionDB();

			//pi = fr valido, fr vencido, fr no existe.

			strSQL = "SELECT folio " +
						"	FROM  com_revocacion_cert " +
						"    WHERE ic_usuario = ?  " +
						"	  AND cs_revocado = ?  " +
						"	  AND folio = ?  ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, noUsuario);
			ps.setString(2, "N");
			ps.setLong(3, Long.parseLong(folio));
			rs = ps.executeQuery();
			if(rs.next()){
				folioCorrecto = true;
			}
			rs.close();
			ps.close();

			return folioCorrecto;
		}catch(Throwable t){
			throw new AppException("Error al veriricar si el folio es correcto", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}

		}
	}

	private void guardaBitacoraCertDigit(AccesoDB con, String noUsuario, String nafinElec, String cveAfiliado, String eventoActual){
		PreparedStatement ps = null;
		String strSQL = "";
		try{
			UtilUsr utilUsr = new UtilUsr();
			Usuario usuario = utilUsr.getUsuario(noUsuario);
			String nombreCompletoUsuario = usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();

			strSQL = " INSERT INTO bit_cambios_gral ( " +
				" ic_cambio, cc_pantalla_bitacora, cg_clave_afiliado," +
				" ic_nafin_electronico, " +
				" ic_usuario, cg_nombre_usuario, " +
				" cg_actual) " +
				" VALUES(SEQ_BIT_CAMBIOS_GRAL.nextval, " +
				" ?, ?, ? , ?, ?, ? )";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, "CERTDIGIT");
			ps.setString(2, cveAfiliado);
			ps.setLong(3, Long.parseLong(nafinElec));
			ps.setString(4, noUsuario);
			ps.setString(5, nombreCompletoUsuario);
			ps.setString(6, eventoActual);
			ps.executeUpdate();
			ps.close();

		}catch(Throwable t){
			throw new AppException("Error al guardar en bitacora", t);
		}
	}
	
	/**
	 * Genera un folio para ser enviado por correo a la pyme para que pueda revocar o generar la clave de cesion
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param noUsuario
	 */
	public String generaFolioClaveCesion(String noUsuario)throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		String csRevocacion = "N";
		boolean commit = true;
		
		try{
			con.conexionDB();
			String folio = "";
			int existeReg = 0;
			
			strSQL = "SELECT SEQ_SEG_CVE_CESION_FOLIO.NEXTVAL FROM dual ";
			ps = con.queryPrecompilado(strSQL);
			rs = ps.executeQuery();
			
			if(rs.next()){
				folio = rs.getString(1);
			}
			rs.close();
			ps.close();
			
			strSQL = "SELECT count(1) " +
					"  FROM seg_cve_cesion " +
					" WHERE ic_usuario = ? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setString(1,noUsuario);
			rs = ps.executeQuery();
			
			if(rs.next()){
				existeReg = rs.getInt(1);
			}
			rs.close();
			ps.close();

			if(existeReg > 0){
				
				strSQL = "UPDATE seg_cve_cesion " +
						"   SET ig_folio = ?, " +
						"       df_envio_folio = SYSDATE " +
						" WHERE ic_usuario = ? ";
				
				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1, Long.parseLong(folio));
				ps.setString(2, noUsuario);
				ps.executeUpdate();
				ps.close();
				
				csRevocacion = "S";

			}else{
				strSQL = "INSERT INTO seg_cve_cesion " +
						"            (ig_folio, ic_usuario, df_envio_folio) " +
						"     VALUES (?, ?, SYSDATE) ";
						
				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1, Long.parseLong(folio));
				ps.setString(2, noUsuario);
				ps.executeUpdate();
				ps.close();
			}
			
			try{
				envioCorreoFolioClaveCesion(folio,noUsuario, csRevocacion);
			}catch(Throwable e){
				throw new AppException("Fallo el env�o de correo no se puede continuar con el proceso");
			}
			
			return folio;
		}catch(AppException ap){
			commit = false;
			ap.printStackTrace();
			throw ap;
		}catch(Throwable t){
			commit = false;
			t.printStackTrace();
			throw new AppException("Error al generar folio de revcacion", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * Se encarga de enviar el correo a la pyme con el folio generado para la clave de cesion
	 * @throws netropology.utilerias.AppException
	 * @param noUsuario
	 * @param folio
	 */
	private void envioCorreoFolioClaveCesion(String folio, String noUsuario, String csRevocacion)throws AppException{
		try{
			Correo correo = new Correo();
			UtilUsr util = new UtilUsr();
			Usuario usuario= new Usuario();
			String nombreUsuario = "";
			String mail = "";
			
			usuario = util.getUsuario(noUsuario);
			nombreUsuario = usuario.getNombre()+" "+usuario.getApellidoMaterno()+" "+usuario.getApellidoPaterno();
			mail = usuario.getEmail();
								  
			String msgHTML =	"\n <html>  "  +
									"\n   <head>  "  +
									"\n     <title>GENERAR FOLIO PARA CLAVE DE CESION</title>  "  +
									"\n     <style type=\"text/css\"> " +
									"\n       <!-- " +
									"\n         body { font-family: Tahoma, arial; font-size: 10pt; }" +
									"\n         .titulo {font-weight: bold; font-size:12px; }" +
									"\n         .style2 {color: #FFFFFF; font-weight: bold; font-size:10px; }" +
									"\n	        .style3 {color: #000000; font-weight: bold; font-size:10px; TEXT-ALIGN: center;}" +
									"\n			  .dataTextLeft2 { FONT-WEIGHT: bolder;BACKGROUND-COLOR: #666699; FONT-SIZE: 12px; COLOR: white; FONT-FAMILY: Tahoma, Arial, Helvetica, sans-serif; " +
									"\n				TEXT-ALIGN: center;		TEXT-TRANSFORM: uppercase;}"+
									"\n       --> " +
									"\n     </style> " +
									"\n   </head>  "  +
									"\n   <body>  "  +
									"\n     <table border=\"0\">  "  +
									"\n       <tr>       " ;
			if("S".equals(csRevocacion)){
					msgHTML +=  "\n         <td style=\"font-family:arial, 'lucida console', sans-serif; font-size: 10pt;text-align:justify;\"><br/><b>Estimado Usuario(a): "+nombreUsuario+"</b><br/>Por este conducto Nacional Financiera te ha asignado el siguiente FOLIO DE SEGURIDAD con el cual podr&aacute;s revocar y generar una nueva Clave de Cesi&oacute;n de Derechos.</td>  ";
			}else{
					msgHTML +=  "\n         <td style=\"font-family:arial, 'lucida console', sans-serif; font-size: 10pt;text-align:justify;\"><br/><b>Estimado Usuario(a): "+nombreUsuario+"</b><br/>Por este conducto Nacional Financiera te ha asignado el siguiente FOLIO DE SEGURIDAD con el cual podr&aacute;s generar tu Clave de Cesi&oacute;n de Derechos.</td>  ";
			}
			msgHTML += 			"\n       </tr>  "  +
									"\n       <tr>       "  +
									"\n         <td><br/>	" +
									"\n     <table border=\"1\">  "  +
									"\n       <tr>       "  +
									"\n         <td class=\"dataTextLeft2\">FOLIO DE SEGURIDAD</td>  "  +
									"\n       </tr>  " +
									"\n       <tr>       "  +
									"\n         <td class=\"style3\">"+folio+"</td>  "  +
									"\n       </tr>  " +
									"\n     	</table>  " +
									"\n     </td>  " +
									"\n     </tr>  "  +
									"\n       <tr>       "  +
									"\n         <td style=\"font-family:arial, 'lucida console', sans-serif; font-size: 10pt;text-align:justify;\"><br/>Favor de ingresar a NAFINET para continuar con el proceso a la siguiente ruta: Herramientas/Clave de Cesi&oacute;n de Derechos.<br/></td>  "  +
									"\n       </tr>  "  +
									"\n         <td class=\"titulo\"><br/>ATENTAMENTE</td>  "  +
									"\n       <tr>       "  +
									"\n         <td>Coordinaci&oacute;n de Informaci&oacute;n Canales Alternos.</td>  "  +
									"\n       </tr>  "  +
									"\n       </tr>  "  +
									"\n       <tr>       "  +
									"\n         <td style=\"font-family:arial, 'lucida console', sans-serif; font-size: 10pt;text-align:justify;\"><br/><br/><span class=\"titulo\"><b>Nota:</b>&nbsp;Este mensaje fue enviado desde una cuenta " +
									"\n             de notificaciones que no puede aceptar correos electr&oacute;nicos de entrada. <br/>Favor de no responder este mensaje.</span></td>  "  +
									"\n       </tr>  "  +
									"\n     </table>  "  +
									"\n   </body>" +
									"\n </html>";
			
			if("S".equals(csRevocacion)){
				correo.enviarTextoHTML("no_response@nafin.gob.mx", mail, "Folio de Seguridad - Revocar Clave de Cesi�n", msgHTML);
			}else{
				correo.enviarTextoHTML("no_response@nafin.gob.mx", mail, "Folio de Seguridad - Generar Clave de Cesi�n", msgHTML);
			}
		}catch(Throwable t){
			throw new AppException("Error al enviar correo con el folio para la revocacion de la clave de cesion", t);
		}
	}
	
	public String getFolioClaveCesion(String noUsuario) throws AppException{
		log.info("getFolioClaveCesion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		String folio = "";
		
		try{
			con.conexionDB();
			
			strSQL = "SELECT ig_folio " +
					"  FROM seg_cve_cesion " +
					" WHERE ic_usuario = ? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, noUsuario);
			rs = ps.executeQuery();
			if(rs.next()){
				folio = rs.getString(1)==null?"":rs.getString(1);
			}
			rs.close();
			ps.close();
			
			return folio;
		}catch(Throwable t){
			log.info("getFolioClaveCesion(Error)");
			throw new AppException("Error al verificar si existe folio para clave de cesion", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getFolioClaveCesion(S)");
		}
	}

	/**
	 * Valida que se tenga permiso para ejecutar la acci�n indicada en el argumento <tt>permiso</tt>.
	 * En caso de que no se tenga permiso, lanza una excepci�n: SeguException y registra en el log 
	 * el acceso denegado.
	 * Nota: Para obtener la clave del Afiliado(tipo de afiliado), usar la variable 
	 * en sesi�n: iTipoPerfil.
	 * @throws SeguException
	 *
	 * @param tipoAfiliado    Clave del Afiliado.
	 * @param ccPerfil 		  Clave del perfil.
	 * @param ccMenu 			  Clave de la pantalla.
	 * @param ccPermiso 		  Clave del Permiso. Acciones para la cual se solicita permiso.
	 * @param login	   	  Login del usuario, s�lo se usa con fines informativos, para registrar
	 *                   	  en el log a quien se le deneg� el acceso.
	 *
	 * @author jshernandez
	 * @since F021 - 2015 -- ADMIN NAFIN -- Revocar Certificados Digitales Administrador; 14/09/2015 01:16:58 p.m.
	 *
	 */
	public void validaPermiso( String tipoAfiliado, String ccPerfil, String ccMenu, String ccPermiso, String login )  
		throws SeguException  {
		
		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement ps 			= null;
		ResultSet 			rs 			= null;
		StringBuffer		query 		= new StringBuffer();
		
		boolean				hayPermiso = false;
		
		try {
			
			// Conectarse a la Base de Datos
			con.conexionDB();
			
			// Preparar Query
			query.append(
				"SELECT                                                    "  +
				"   PERFIL.CS_ACTIVO AS HAY_PERMISO                        "  +
				"FROM                                                      "  +
				"   COMCAT_PERMISO_AFILIADO PERMISO,                       "  +
				"   COMREL_PERMISO_X_PERFIL PERFIL                         "  +
				"WHERE                                                     "  +
				"   PERMISO.IC_TIPO_AFILIADO = PERFIL.IC_TIPO_AFILIADO AND "  +
				"   PERMISO.CC_MENU          = PERFIL.CC_MENU          AND "  +
				"   PERMISO.CC_PERMISO       = PERFIL.CC_PERMISO       AND "  +
				"   PERMISO.CS_ACTIVO        = 'S'                     AND "  +
				"   PERMISO.IC_TIPO_AFILIADO =  ?                      AND "  +
				"   PERMISO.CC_MENU          =  ?                      AND "  +
				"   PERMISO.CC_PERMISO       =  ?                      AND "  +
				"   PERFIL.CC_PERFIL         =  ?  "
			);
			
			// Realizar la consulta
			ps = con.queryPrecompilado(query.toString());
			ps.setString( 1, tipoAfiliado.toString()			); 
			ps.setString( 2, ccMenu.toString()       			); 
			ps.setString( 3, ccPermiso.toUpperCase()	);
			ps.setString( 4, ccPerfil.toString()     			); 
			
			rs = ps.executeQuery();
			if(rs.next()){
				hayPermiso = "S".equalsIgnoreCase(rs.getString("HAY_PERMISO"))?true:false;
			}
			
			if( !hayPermiso ){
				throw new SeguException("ACCESO DENEGADO.");
			}
			
		} catch(SeguException se){
			
			log.error( 	"validaPermiso(SeguException.ACCESO_DENEGADO): " +
							"tipoAfiliado    = <" + tipoAfiliado    + ">," +
							"ccPerfil        = <" + ccPerfil        + ">," +
							"ccMenu          = <" + ccMenu          + ">," +
							"ccPermiso       = <" + ccPermiso       + ">," +
							"login           = <" + login           + ">," +
							"query           = <" + query           + ">"
							,se
						);
			throw se;
			
		} catch(Exception e){
			
			log.error( 	"validaPermiso(Exception): Error al validar permisos." +
							"tipoAfiliado    = <" + tipoAfiliado    + ">," +
							"ccPerfil        = <" + ccPerfil        + ">," +
							"ccMenu          = <" + ccMenu          + ">," +
							"ccPermiso       = <" + ccPermiso       + ">," +
							"login           = <" + login           + ">," +
							"query           = <" + query           + ">"
							,e
						);
			throw new SeguException("Error al validar permisos: " + e.getMessage());
			
		} finally { 
			
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
		}
		
	}

	/**
	 * Devuelve una lista de acciones de las que se tiene permiso realizar en pantalla
	 * para un perfil espec�fico.
	 * Nota: Para obtener la clave del Afiliado(tipo de afiliado), usar la variable 
	 * en sesi�n: iTipoPerfil.
	 * @throws AppException
	 *
	 * @param tipoAfiliado   			Clave del Afiliado.
	 * @param ccPerfil 					Clave del perfil.
	 * @param ccMenu 						Clave de la pantalla.
	 * @param permisosSolicitados    Array de Claves de Permisos. Acciones para las cuales se solicita el permiso.
	 *
	 * @return <tt>List</tt> con los permisos aprobados. En caso de que no se tenga ning�n permiso,
	 * regresa una lista vac�a.
	 *
	 * @author jshernandez
	 * @since F021 - 2015 -- ADMIN NAFIN -- Revocar Certificados Digitales Administrador; 14/09/2015 01:15:11 p.m.
	 *
	 */
	public List getPermisosPorMenu( String tipoAfiliado, String ccPerfil, String ccMenu, String[] permisosSolicitados ) 
		throws AppException {

		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement ps 			= null;
		ResultSet 			rs 			= null;
		StringBuffer		query 		= new StringBuffer();

		List					permisos		= new ArrayList();
		boolean				hayPermiso 	= false;

		try {

			// Revisar que se haya indicado al menos un permiso
			if( permisosSolicitados == null || permisosSolicitados.length == 0 ){ 
				return permisos;
			}

			// Conectarse a la Base de Datos
			con.conexionDB();

			// Preparar Query
			query.append(
				"SELECT                                                      "  +
				"   PERFIL.CC_PERMISO AS PERMISO_AUTORIZADO                  "  +
				"FROM                                                        "  +
				"   COMCAT_PERMISO_AFILIADO PERMISO,                         "  +
				"   COMREL_PERMISO_X_PERFIL PERFIL                           "  +
				"WHERE                                                       "  +
				"   PERMISO.IC_TIPO_AFILIADO = PERFIL.IC_TIPO_AFILIADO  AND  "  +
				"   PERMISO.CC_MENU          = PERFIL.CC_MENU           AND  "  +
				"   PERMISO.CC_PERMISO       = PERFIL.CC_PERMISO        AND  "  +
				"   PERMISO.CS_ACTIVO        = 'S'                      AND  "  +
				"   PERMISO.IC_TIPO_AFILIADO =  ?                       AND  "  +
				"   PERMISO.CC_MENU          =  ?                       AND  "  +
				"   PERMISO.CC_PERMISO       IN ("
			);
			
			// Agregar "placeholders" de las variables bind
			for(int i=0;i<permisosSolicitados.length;i++){
				if(i>0){
					query.append(",");
				}
				query.append("?");
			}
			
			query.append(
				") AND "
			);
			query.append(
				"   PERFIL.CC_PERFIL         =  ?                       AND  "  +
				"   PERFIL.CS_ACTIVO         = 'S'                           "
			);

			// Realizar la consulta
			ps = con.queryPrecompilado(query.toString());
			int idx = 1;
			ps.setString( idx++, tipoAfiliado.toString() ); 
			ps.setString( idx++, ccMenu.toString()       ); 
			// Agregar lista de permisos a consultar
			for(int i=0;i<permisosSolicitados.length;i++){
				ps.setString( idx++, permisosSolicitados[i].toUpperCase() ); 
			}
			ps.setString( idx++, ccPerfil.toString()     ); 
			
			rs = ps.executeQuery();
			while(rs.next()){
				String permiso = rs.getString("PERMISO_AUTORIZADO");
				permisos.add(permiso);
			}
			
		} catch(Exception e){
			
			log.error( 	"getPermisosPorMenu(Exception): Error al consultar permisos." +
							"tipoAfiliado 			= <" + tipoAfiliado 			+ ">," +
							"ccPerfil     			= <" + ccPerfil     			+ ">," +
							"ccMenu       			= <" + ccMenu       			+ ">," +
							"permisosSolicitados	= <" + permisosSolicitados	+ ">," +
							"query        			= <" + query        			+ ">"
							,e
						);
			throw new AppException("Error al consultar permisos: " + e.getMessage(),e);
			
		} finally { 
			
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }

		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
		}
		
		return permisos;
		
	}

	/**
	 *
	 * Realiza la "revocaci&oacute;n" del certificado digital, borr�ndolo de users_seguridata.
	 * @throws AppException
	 * @param usuario 				Objeto <tt>Usuario</tt> con el detalle de este.
	 * @param datosRevocador	   <tt>HashMap</tt> con el detalle del usuario revocador.
	 *
	 * @author jshernandez
	 * @since F021 - 2015 -- ADMIN NAFIN -- Revocar Certificados Digitales Administrador; 11/09/2015 05:36:06 p.m.
	 *
	 */
	public void revocarCertificadoDigital(Usuario usuario, HashMap datosRevocador)
		throws AppException {
			
		AccesoDB 			con 					= new AccesoDB();
		PreparedStatement ps 					= null;
		ResultSet			rs						= null;
		String 				querySentencia 	= "";
			
		boolean 				exito 				= true;
		
		String 				login 						= null;
		String 				claveAfiliado 				= null;
		String 				tipoAfiliado 				= null;
		String 				leyendaRevocacion			= null;
		String 				nafinElectronico			= null;
		String 				claveAfiliadoBitCambios = null;
		String 				fechaVigencia 				= "";
		boolean				tieneFechaVigencia		= false;
		
		try {
			
			con.conexionDB();
			
			login 						= 	usuario.getLogin();
			claveAfiliado 				= 	usuario.getClaveAfiliado();
			tipoAfiliado				=  usuario.getTipoAfiliado(); 
			leyendaRevocacion			= 	"Revocaci�n Realizada; " + 
												"usuario revoc�: "   + datosRevocador.get("usuario")       + 
												"; nombre usuario: " + datosRevocador.get("nombreUsuario") +
												".";
			claveAfiliadoBitCambios = tipoAfiliado;
			
			// Verificar que el certificado sea v�lido
			fechaVigencia 			= getFecVigenciaCertificado(login);
			tieneFechaVigencia	= Comunes.esVacio(fechaVigencia)?false:true;
			if( !tieneFechaVigencia ){
				throw new AppException("El certificado digial no puede ser revocado debido a que el usuario no cuenta con uno.");
			}

			// Obtener n�mero NAFIN Electr�nico
			querySentencia	= 	" SELECT                   " +
									"   ic_nafin_electronico   " +
									" FROM                     " +
									"   comrel_nafin           " +
									" WHERE                    " +
									"   ic_epo_pyme_if = ? AND " +
									"   cg_tipo        = ?     ";
			ps = con.queryPrecompilado(querySentencia);
			ps.setString(1, claveAfiliado);
			ps.setString(2, tipoAfiliado);
			rs = ps.executeQuery();
			if(rs.next()) {
				nafinElectronico = rs.getString("ic_nafin_electronico");
			}
			rs.close();
			ps.close();
	
			querySentencia = "DELETE users_seguridata WHERE table_uid = RPAD(?,9,' ')";
			ps = con.queryPrecompilado(querySentencia);
			ps.setString(1, login);
			ps.executeUpdate();
			ps.close();
			
			guardaBitacoraCertDigit(con, login, nafinElectronico, claveAfiliadoBitCambios, leyendaRevocacion);

		} catch(Throwable t) {
			
			exito = false;
			log.error(
				"revocarCertificadoDigital(Exception):\n" +
				"usuario          = <" + usuario          + ">\n" +
				"nafinElectronico = <" + nafinElectronico + ">\n" +
				"datosRevocador   = <" + datosRevocador   + ">\n" +
				"querySentencia	= <" + querySentencia	+ ">",
				t
			);
			throw new AppException("Error al revocar certificado digital del usuario.", t );
			
		} finally {
			
			if(ps != null) { try { ps.close();}catch(Exception e){} }
			if(rs != null) { try { rs.close();}catch(Exception e){} }
			
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			
		}
		
	}
	
	public List consultaHistCveCDer(String loginUsuario) throws AppException{
		AccesoDB 			con 					= new AccesoDB();
		PreparedStatement ps 					= null;
		ResultSet			rs						= null;
		String 				querySentencia 	= "";
		List lstHistorial = new ArrayList();
		try{
			con.conexionDB();
			
			querySentencia = " SELECT cg_movimiento, ic_usuario, " +
								" 		TO_CHAR (df_cambio, 'dd/mm/yyyy HH24:mi:ss') df_cambio " +
								" FROM bit_req_cve_cder " +
								" WHERE ic_usuario = ? " +
								" ORDER BY df_cambio ";
			
			ps = con.queryPrecompilado(querySentencia);
			ps.setString(1, loginUsuario);
			rs = ps.executeQuery();
			Map hmReg = new HashMap();
			while(rs.next()){
				hmReg = new HashMap();
				hmReg.put("CG_MOVIMIENTO", rs.getString("cg_movimiento")==null?"":rs.getString("cg_movimiento"));
				hmReg.put("IC_USUARIO", rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario"));
				hmReg.put("DF_CAMBIO", rs.getString("df_cambio")==null?"":rs.getString("df_cambio"));
				
				lstHistorial.add(hmReg);
			}
			
			rs.close();
			ps.close();
			
			return lstHistorial;
			
		}catch(Exception e){
			throw new AppException("Error al consultar el historico de la clave de cedion", e);
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			
		}
	}
	
	public void guardarHistCveCDer(String loginUsuario, String solicitaCveCDer) throws AppException{
		AccesoDB 			con 					= new AccesoDB();
		PreparedStatement ps 					= null;
		String 				querySentencia 	= "";
		boolean commit = true;
		try{
			con.conexionDB();
			
			querySentencia = "UPDATE seg_cve_cesion "+
						" SET cs_req_cve_cder = ? " +
						" WHERE ic_usuario = ? ";
			
			ps = con.queryPrecompilado(querySentencia);
			ps.setString(1, solicitaCveCDer);
			ps.setString(2, loginUsuario);
			ps.executeUpdate();
			ps.close();
			
			querySentencia = "INSERT INTO bit_req_cve_cder "+
						" (ic_usuario, df_cambio, cg_movimiento ) " +
						" VALUES (?, sysdate, ? ) ";
			
			ps = con.queryPrecompilado(querySentencia);
			ps.setString(1, loginUsuario);
			ps.setString(2, solicitaCveCDer);
			ps.executeUpdate();
			ps.close();
			
			
		}catch(Exception e){
			commit = false;
			throw new AppException("Error al guardar en bitacora el cambio del paramereo que indica si realizara validacion clave cesion derechos", e);
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			
		}
	}
	
	public String consultaParamValidCveCder(String loginUsuario) throws AppException{
		AccesoDB 			con 					= new AccesoDB();
		PreparedStatement ps 					= null;
		ResultSet			rs						= null;
		String 				querySentencia 	= "";
		String param = "";
		try{
			con.conexionDB();
			
			querySentencia = "SELECT cs_req_cve_cder " +
				" FROM seg_cve_cesion " +
				" WHERE ic_usuario = ? ";
			
			ps = con.queryPrecompilado(querySentencia);
			ps.setString(1, loginUsuario);
			rs = ps.executeQuery();
			if(rs.next()){
				param = rs.getString("cs_req_cve_cder")==null?"":rs.getString("cs_req_cve_cder");
			}
			
			rs.close();
			ps.close();
			
			return param;
			
		}catch(Exception e){
			throw new AppException("Error al consultar el parametro de validacion para la clave de cesion", e);
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			
		}
	}
	
} // Fin del la clase
