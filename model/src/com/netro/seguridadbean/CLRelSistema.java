package com.netro.seguridadbean;

/****************************************************************************************************************
*
*	Netropology, S.C.
*
*	Archivo:		CLRelSistema.java
*
*	Proposito:	Clase CLRelSistema
*
*	Lenguaje:	Java
*
*	Autor:		Pedro A. Garcia Ramirez
*
*	Fecha Creacion:	10/01/2001
*
****************************************************************************************************************/
/**************************************************************************
 *
 * Modificado por: Cindy A. Ramos Pérez
 *
 * Fecha Modificación: 26/03/2002
 *
 *  Modificado para que soporte multinivel en la relación de sistemas.
 *
***************************************************************************/
import java.util.*;
import java.io.*;

/****************************************************************************
*
*	clase CLRelSistema
*
*****************************************************************************/
public class CLRelSistema {

	Vector	lovRelSistema;
	String 	m_sSQLCargaRelSist;

	/*****************************************************************************
	*		Constructor_1
	*****************************************************************************/
	public CLRelSistema()
	{
                if (CConst.TRACE)
	        	System.out.println("CLRelSistema::Constructor_1");

		lovRelSistema = new Vector();

		m_sSQLCargaRelSist = " SELECT cc_sistema, cc_sistema_2, n_nivel, cg_status ";
		m_sSQLCargaRelSist += " FROM seg_rel_sist ";
		m_sSQLCargaRelSist += " ORDER BY cc_sistema_2 ";
	}
	/*****************************************************************************
	*		void agregarRelSistema
	*****************************************************************************/
	public void agregarRelSistema(CRelSistema eoRelSistema)
	{
		lovRelSistema.addElement(eoRelSistema);
	}
	/*****************************************************************************
	*		CRelSistema buscarSistemaPadre
	*****************************************************************************/
	public CRelSistema buscarSistemaPadre(String esCveSistHijo)
	{
		CRelSistema loRelSistema = null;
		CRelSistema loRelSistema_2 = null;

		for(int i=0; i< lovRelSistema.size(); i++)
		{
			loRelSistema = (CRelSistema) lovRelSistema.elementAt(i);
			if (esCveSistHijo.equals(loRelSistema.getCveSistHijo()))
			{
				loRelSistema_2 = loRelSistema;
			}
		}
		return loRelSistema_2;
	}
	/*****************************************************************************
	*		void cargarRelacionSistemas
	*****************************************************************************/
	public void cargarRelacionSistemas(Vector evTmp)
				   throws NullPointerException,ArrayIndexOutOfBoundsException
	{
		Vector lvRegistro = null ;
		int liNumRegs = 0;
		CRelSistema loRelSistema = null;
		String lsCveSistemaHijo = "";
		String lsCveSistemaPadre = "";
		int liNivel              = 0;
		String lsStatus          = "";

		liNumRegs = evTmp.size() ;
		lvRegistro = new Vector();

                if (CConst.TRACE)
	        	System.out.println("\tCLSistema::cargarRelacionSistemas");
		for (int liPos = 0; liPos < liNumRegs; liPos++ ) {
			lvRegistro = (Vector) evTmp.elementAt(liPos);

			lsCveSistemaHijo  = (String) lvRegistro.elementAt(0) ;
			lsCveSistemaPadre = (String) lvRegistro.elementAt(1) ;
			liNivel           = ((Integer)lvRegistro.elementAt(2)).intValue();
            lsStatus          = (String) lvRegistro.elementAt(3) ;

                if (CConst.TRACE)
    	          	System.out.println("CveSistemaPadre[" + lsCveSistemaPadre + "]->" + "CveSistemaHijo[" + lsCveSistemaHijo + "]");

			loRelSistema = new CRelSistema(	lsCveSistemaPadre,	lsCveSistemaHijo, liNivel, lsStatus);

			agregarRelSistema(loRelSistema);
		}
	}
	/*****************************************************************************
	*		int devolverNivelPadre
	*****************************************************************************/
	public int devolverNivelPadre(String esSistPadre)
	{
		CRelSistema loRelSistema = null;
        int liNivel = 0;

		for(int i=0; i< lovRelSistema.size(); i++)
		{
			loRelSistema = (CRelSistema) lovRelSistema.elementAt(i);
			if (esSistPadre.equals(loRelSistema.getCveSistHijo())) {
				liNivel = loRelSistema.getNivel();
			}
		}
		return liNivel;
	}
	/*****************************************************************************
	*		Vector devolverSistemasHijos
	*****************************************************************************/
	public Vector devolverSistemasHijos(String esCveSistPadre)
	{
		CRelSistema loRelSistema = null;
		Vector 	lvSistemasHijos = new Vector();

//		System.out.println("TRACE::devolverSistemasHijos(" + esCveSistPadre + ") Tama~o del vector de sistemas relacionados: " + lovRelSistema.size());
		for(int i=0; i< lovRelSistema.size(); i++)
		{
			loRelSistema = (CRelSistema) lovRelSistema.elementAt(i);
			if (esCveSistPadre.equals(loRelSistema.getCveSistPadre()))
			{
				lvSistemasHijos.addElement(loRelSistema.getCveSistHijo());
			}
		}
//		System.out.println("TRACE::devolverSistemasHijos(. Tama~o del vector de retorno:" + lvSistemasHijos.size());
		return lvSistemasHijos;
	}
	/*****************************************************************************
	*		String devolverStatus
	*****************************************************************************/
	public String devolverStatus(String esCveSistemaHijo)
	{
		CRelSistema loRelSistema = null;
        String lsStatus = "";

                if (CConst.TRACE)
	        	System.out.println("\n\tCLRelSistema::devolverStatus(E)");

		for(int i=0; i< lovRelSistema.size(); i++)
		{
			loRelSistema = (CRelSistema) lovRelSistema.elementAt(i);
			if (esCveSistemaHijo.equals(loRelSistema.getCveSistHijo()))
			{
				lsStatus = loRelSistema.getStatus();
			}
		}
                if (CConst.TRACE)
                        System.out.println("\tCLRelSistema::devolverStatus(S) --> Status:" + lsStatus);
		return lsStatus;
	}
	/*****************************************************************************
	*		String getSQLCargaRelSist()
	*****************************************************************************/
	public String getSQLCargaRelSist()
	{
		return m_sSQLCargaRelSist;
	}

	public int size() {
		return lovRelSistema.size();
	}

}