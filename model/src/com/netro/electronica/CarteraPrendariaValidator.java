package com.netro.electronica;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.carga.RenglonValidator;
import netropology.utilerias.carga.ValidacionResult;

import org.apache.commons.logging.Log;

public class CarteraPrendariaValidator  implements RenglonValidator, java.io.Serializable {

	/*
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CarteraPrendariaValidator.class);
	
	public CarteraPrendariaValidator(){
		
		this.numeroConsecutivo		= new StringBuffer(1);
		this.numeroCredito			= new StringBuffer(51);
		this.nombreAcreditado		= new StringBuffer(101);
		this.tipoOperacion			= new StringBuffer(51);
		this.fechaDisposicion		= new StringBuffer(11);
		this.tipoTasa					= new StringBuffer(51);
		this.tasaBase					= new StringBuffer(51);
		this.puntosAdicionales		= new StringBuffer(12);
		this.montoOtorgado			= new StringBuffer(31);
		this.recursosSolicitados	= new StringBuffer(31);
		this.plazoOriginal			= new StringBuffer(10);
		this.capitalVigente			= new StringBuffer(31);
		this.interesesVigentes		= new StringBuffer(31);
		this.capitalVencido			= new StringBuffer(31);
		this.interesesVencidos		= new StringBuffer(31);
		this.interesesMoratorios	= new StringBuffer(31);
		this.saldoTotal				= new StringBuffer(31);
		this.diasRetraso				= new StringBuffer(12);
		this.descripcion				= new StringBuffer(21);

		this.fieldIndex				= 0;
		this.fieldSize					= 0;
		this.acumulaAnterior			= -1;
		this.extraChar					= -1;
	
		this.claveIF					= null;
		this.directorioTemporal		= null;
		this.login						= null;
		this.operacionElectronicaBean 		= null;
		this.rutaArchivoCsv			= null;
		this.archivoCsv				= null;
		this.rutaArchivoPdf			= null;
		this.archivoPdf				= null;
		this.numerosCreditoRepetidos			= null;
		this.existeCargaPreviaSoloNafinet 	= false;
		
		this.processID 				= null; 
		this.resultados 				= null;
		this.hayError					= false;
		this.recordsNumber			= 0;

	}
	
	/*
	 * M�todos asociados a los campos por registro
	 */
	// Campos del Archivo CSV
	private StringBuffer numeroConsecutivo		= null;
	private StringBuffer numeroCredito			= null;
	private StringBuffer nombreAcreditado		= null;
	private StringBuffer tipoOperacion			= null;
	private StringBuffer fechaDisposicion		= null;
	private StringBuffer tipoTasa					= null;
	private StringBuffer tasaBase					= null;
	private StringBuffer puntosAdicionales		= null;
	private StringBuffer montoOtorgado			= null;
	private StringBuffer recursosSolicitados	= null;
	private StringBuffer plazoOriginal			= null;
	private StringBuffer capitalVigente			= null;
	private StringBuffer interesesVigentes		= null;
	private StringBuffer capitalVencido			= null;
	private StringBuffer interesesVencidos		= null;
	private StringBuffer interesesMoratorios	= null;
	private StringBuffer saldoTotal				= null;
	private StringBuffer diasRetraso				= null;
	private StringBuffer descripcion				= null;
	// Variables Auxiliares
	private int				fieldIndex				= 0;
	private int				fieldSize				= 0;
	private int				acumulaAnterior		= -1;
	private int				extraChar				= -1; // Guarda el ultimo caracter de contenido que no es parte de los campos del archivo
	
	// Campos del Archivo CSV: Setters & Getters
	
	public StringBuffer getNumeroConsecutivo() {
		return numeroConsecutivo;
	}

	public void setNumeroConsecutivo(StringBuffer numeroConsecutivo) {
		this.numeroConsecutivo = numeroConsecutivo;
	}

	public StringBuffer getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroCredito(StringBuffer numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public StringBuffer getNombreAcreditado() {
		return nombreAcreditado;
	}

	public void setNombreAcreditado(StringBuffer nombreAcreditado) {
		this.nombreAcreditado = nombreAcreditado;
	}

	public StringBuffer getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(StringBuffer tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public StringBuffer getFechaDisposicion() {
		return fechaDisposicion;
	}

	public void setFechaDisposicion(StringBuffer fechaDisposicion) {
		this.fechaDisposicion = fechaDisposicion;
	}

	public StringBuffer getTipoTasa() {
		return tipoTasa;
	}

	public void setTipoTasa(StringBuffer tipoTasa) {
		this.tipoTasa = tipoTasa;
	}

	public StringBuffer getTasaBase() {
		return tasaBase;
	}

	public void setTasaBase(StringBuffer tasaBase) {
		this.tasaBase = tasaBase;
	}

	public StringBuffer getPuntosAdicionales() {
		return puntosAdicionales;
	}

	public void setPuntosAdicionales(StringBuffer puntosAdicionales) {
		this.puntosAdicionales = puntosAdicionales;
	}

	public StringBuffer getMontoOtorgado() {
		return montoOtorgado;
	}

	public void setMontoOtorgado(StringBuffer montoOtorgado) {
		this.montoOtorgado = montoOtorgado;
	}

	public StringBuffer getRecursosSolicitados() {
		return recursosSolicitados;
	}

	public void setRecursosSolicitados(StringBuffer recursosSolicitados) {
		this.recursosSolicitados = recursosSolicitados;
	}

	public StringBuffer getPlazoOriginal() {
		return plazoOriginal;
	}

	public void setPlazoOriginal(StringBuffer plazoOriginal) {
		this.plazoOriginal = plazoOriginal;
	}

	public StringBuffer getCapitalVigente() {
		return capitalVigente;
	}

	public void setCapitalVigente(StringBuffer capitalVigente) {
		this.capitalVigente = capitalVigente;
	}

	public StringBuffer getInteresesVigentes() {
		return interesesVigentes;
	}

	public void setInteresesVigentes(StringBuffer interesesVigentes) {
		this.interesesVigentes = interesesVigentes;
	}

	public StringBuffer getCapitalVencido() {
		return capitalVencido;
	}

	public void setCapitalVencido(StringBuffer capitalVencido) {
		this.capitalVencido = capitalVencido;
	}

	public StringBuffer getInteresesVencidos() {
		return interesesVencidos;
	}

	public void setInteresesVencidos(StringBuffer interesesVencidos) {
		this.interesesVencidos = interesesVencidos;
	}

	public StringBuffer getInteresesMoratorios() {
		return interesesMoratorios;
	}

	public void setInteresesMoratorios(StringBuffer interesesMoratorios) {
		this.interesesMoratorios = interesesMoratorios;
	}

	public StringBuffer getSaldoTotal() {
		return saldoTotal;
	}

	public void setSaldoTotal(StringBuffer saldoTotal) {
		this.saldoTotal = saldoTotal;
	}

	public StringBuffer getDiasRetraso() {
		return diasRetraso;
	}

	public void setDiasRetraso(StringBuffer diasRetraso) {
		this.diasRetraso = diasRetraso;
	}

	public StringBuffer getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(StringBuffer descripcion) {
		this.descripcion = descripcion;
	}
	
	// Variables Auxiliares: Setters & Getters
	public int getFieldIndex() {
		return fieldIndex;
	}

	public void setFieldIndex(int fieldIndex) {
		this.fieldIndex = fieldIndex;
	}

	public int getFieldSize() {
		return fieldSize;
	}

	public void setFieldSize(int fieldSize) {
		this.fieldSize = fieldSize;
	}

	public int getAcumulaAnterior() {
		return acumulaAnterior;
	}

	public void setAcumulaAnterior(int acumulaAnterior) {
		this.acumulaAnterior = acumulaAnterior;
	}

	public int getExtraChar() {
		return extraChar;
	}

	public void setExtraChar(int extraChar) {
		this.extraChar = extraChar;
	}
	
	
	public StringBuffer getField(){
		
		StringBuffer buffer = null;
		
		switch(fieldIndex){
			case  0: buffer = getNumeroConsecutivo();		this.fieldSize =   1; 	this.fieldIndex++; break;
			case  1: buffer = getNumeroCredito();			this.fieldSize =  51; 	this.fieldIndex++; break;
			case  2: buffer = getNombreAcreditado();		this.fieldSize = 101; 	this.fieldIndex++; break;
			case  3: buffer = getTipoOperacion();			this.fieldSize =  51; 	this.fieldIndex++; break;
			case  4: buffer = getFechaDisposicion();		this.fieldSize =  11; 	this.fieldIndex++; break;
			case  5: buffer = getTipoTasa();					this.fieldSize =  51; 	this.fieldIndex++; break;
			case  6: buffer = getTasaBase();					this.fieldSize =  51; 	this.fieldIndex++; break;
			case  7: buffer = getPuntosAdicionales();		this.fieldSize =  12; 	this.fieldIndex++; break; // 7  + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case  8: buffer = getMontoOtorgado();			this.fieldSize =  31; 	this.fieldIndex++; break; // 26 + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case  9: buffer = getRecursosSolicitados();	this.fieldSize =  31; 	this.fieldIndex++; break; // 26 + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case 10: buffer = getPlazoOriginal();			this.fieldSize =  10; 	this.fieldIndex++; break; // 5  + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case 11: buffer = getCapitalVigente();			this.fieldSize =  31; 	this.fieldIndex++; break; // 26 + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case 12: buffer = getInteresesVigentes();		this.fieldSize =  31; 	this.fieldIndex++; break; // 26 + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case 13: buffer = getCapitalVencido();			this.fieldSize =  31; 	this.fieldIndex++; break; // 26 + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case 14: buffer = getInteresesVencidos();		this.fieldSize =  31; 	this.fieldIndex++; break; // 26 + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case 15: buffer = getInteresesMoratorios();	this.fieldSize =  31; 	this.fieldIndex++; break; // 26 + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case 16: buffer = getSaldoTotal();				this.fieldSize =  31; 	this.fieldIndex++; break; // 26 + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case 17: buffer = getDiasRetraso();				this.fieldSize =  12; 	this.fieldIndex++; break; //  7 + 5 // Para reportar m�s f�cilmente los errores de precisi�n // Esto es debido al uso de comas en los n�meros
			case 18: buffer = getDescripcion();				this.fieldSize =  21; 	this.fieldIndex++; break;
			default:
				buffer          = null;
				this.fieldSize  = -1;
				this.fieldIndex = -1;
		}
		
		return buffer;
		
	}
		  
	public void doReset(){

		this.numeroConsecutivo.setLength(0);
		this.numeroCredito.setLength(0);
		this.nombreAcreditado.setLength(0);
		this.tipoOperacion.setLength(0);
		this.fechaDisposicion.setLength(0);
		this.tipoTasa.setLength(0);
		this.tasaBase.setLength(0);
		this.puntosAdicionales.setLength(0);
		this.montoOtorgado.setLength(0);
		this.recursosSolicitados.setLength(0);
		this.plazoOriginal.setLength(0);
		this.capitalVigente.setLength(0);
		this.interesesVigentes.setLength(0);
		this.capitalVencido.setLength(0);
		this.interesesVencidos.setLength(0);
		this.interesesMoratorios.setLength(0);
		this.saldoTotal.setLength(0);
		this.diasRetraso.setLength(0);
		this.descripcion.setLength(0);

		this.fieldIndex				= 0;
		this.fieldSize					= 0;
		//this.acumulaAnterior		= -1; // Nota: Este campo no se puede resetear
		this.extraChar					= -1;
		
		//this.claveIF					= null; // No se puede resetear porque es global a todos los registros
		//this.directorioTemporal	= null; // No se puede resetear porque es global a todos los registros
		//this.login					= null; // No se puede resetear porque es global a todos los registros
		//this.operacionElectronicaBean 		= null; 	// No se puede resetear porque es global a todos los registros
		//this.rutaArchivoCsv		= null; // No se puede resetear porque es global a todos los registros
		//this.archivoCsv				= null; // No se puede resetear porque es global a todos los registros
		//this.rutaArchivoPdf		= null; // No se puede resetear porque es global a todos los registros
		//this.archivoPdf				= null; // No se puede resetear porque es global a todos los registros
		//this.numerosCreditoRepetidos		= null; 	// No se puede resetear porque es global a todos los registros
		//this.existeCargaPreviaSoloNafinet = false;	// No se puede resetear porque es global a todos los registros
		
		//this.processID 				= null; // No se puede resetear porque es global a todos los registros 
		//this.resultados 			= null; // No se puede resetear porque es global a todos los registros
		this.hayError					= false;
		//this.recordsNumber			= 0;    // No se puede resetear porque es global a todos los registros
		
	}
	
	public void parse(BufferedReader br)
		throws Exception {
			
		doReset();
		
		int 		 	 c 							= -1;
		StringBuffer field 						= getField();
		int			 bufferSize					= getFieldSize();
		boolean		 enclosingDoubleQuote	= false;	// Double-quotes are used to enclose fields.
		boolean 		 firstFieldChar			= true;  // Auxiliary flag used to detect enclosing Double Quote Fields.
		boolean		 escapeDoubleQuote		= false;	// A double-quote appearing inside an enclosed field 
																	// must be escaped by preceding it with another 
																	// double quote.
		int			 caracteres 				= 0;
		boolean      carriageReturn 			= false;
		boolean		 controlChar				= false; // enclosingDoubleQuote or escapeDoubleQuote
		do {

			// LEER SIGUIENTE CARACTER
			do {
				
				// Leer siguiente caracter
				if( this.acumulaAnterior != -1 ){
					c = this.acumulaAnterior;
					this.acumulaAnterior = -1;
				} else {
					c = br.read();
				}

				// Considerar el caracter carriage return, que ven�a s�lo, como caracter de fin de l�nea
				if( carriageReturn && c != '\n' ){
					this.acumulaAnterior = c;
					carriageReturn 		= false;
					c 							= -1;
					controlChar				= true;
				}
				
				// Resetear flag que indica si el caracter no es parte del contenido
				controlChar					= false; 
				
				// Determinar si el campo le�do viene escapado
				if( firstFieldChar ){
					enclosingDoubleQuote	= c == '"'?true:false;
					controlChar				= enclosingDoubleQuote?true:false;
					firstFieldChar 		= false;
				// En caso de que se haya especificado el caracter de escape de comillas
				//   Caso (Segundo caracter): ""<, ""@<, """<, """@<  
				//    Primer caracter => Campo delimitado por comillas
				//    @               => Cualquier otro caracter
				//    <					 => Fin de la cadena
				// Nota: Tambi�n se consideran condiciones en el cual las secuencias: "< y "@< 
				// podr�an aparecer despu�s del segundo caracter.
				} else if( enclosingDoubleQuote && !escapeDoubleQuote && c == '"' ){
					escapeDoubleQuote 	= true;
					controlChar				= true;
				//  Verificar si se cierra la cadena escapada antes de tiempo
				//  Caso (Tercer caracter): ""<, ""@<, """<, """@< tambi�n podr�a ser: "@@@"<, "@@@"@< 
				//    Primer caracter => Campo delimitado por comillas
				//    @               => Cualquier otro caracter
				//    <					 => Fin de la cadena
				// Nota: Bajo este esquema solo es posible detectar casos como: ""@<
				} else if( enclosingDoubleQuote && escapeDoubleQuote  && c != '"' ){
					enclosingDoubleQuote = false;
					escapeDoubleQuote    = false;
					controlChar				= false; 
				} else if( enclosingDoubleQuote && escapeDoubleQuote  && c == '"' ){
					escapeDoubleQuote    = false;
					controlChar				= false; 
				} 

				// Agregar ultimo caracter extra de contenido, que no forma parte de ninguno de los campos definidos
				if( 
					field      == null
						&&
					(
						enclosingDoubleQuote  && !controlChar
							||
						!enclosingDoubleQuote && c != ','
					)
						&& 
					c != '\r' && c != '\n' && c != -1 
						&& 
					!Character.isWhitespace((char)c) // No se consideran espacios en blanco
				){
					this.extraChar = c;
				}

			} while (
				( 
					caracteres >= bufferSize
						||
					field      == null      
				)
					&&
				(
					enclosingDoubleQuote
						||
					!enclosingDoubleQuote && c != ','
				)
					&& 
				c != '\r' && c != '\n' && c != -1 
			);
					
			// ASIGNAR CARACTER LEIDO
			switch(c){
				case  ',': // Pasar al siguiente campo
					if( !enclosingDoubleQuote ){
						field 	  				= getField();
						bufferSize 				= getFieldSize();
						enclosingDoubleQuote	= false;
						firstFieldChar			= true;
						escapeDoubleQuote		= false;
						// Resetear Conteo
						caracteres 				= field == null?-1:0;
					} else if( !controlChar && field != null ){
						field.append((char)c);
						++caracteres;
					}
					break;
				case '\r':
					carriageReturn = true;
					break;
				case '\n':
					c = -1; // Se encontr� fin de l�nea
					break;
				case   -1:
					break;
				default  :
					if( !controlChar && field != null ){
						field.append((char)c);
						++caracteres;
					}
			}

		} while( c != -1 );

	}
	
	/*
	 * Variables generales
	 */
	
	private String 	claveIF							= null;
	private String 	directorioTemporal			= null;
	private String 	login								= null;
	private OperacionElectronicaBean operacionElectronicaBean 	= null;
	private String 	rutaArchivoCsv					= null;
	private String 	archivoCsv						= null;
	private String 	rutaArchivoPdf					= null;
	private String 	archivoPdf						= null;
	private List  		numerosCreditoRepetidos		= null;
	private boolean 	existeCargaPreviaSoloNafinet 					= false;
	
	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public String getDirectorioTemporal() {
		return directorioTemporal;
	}

	public void setDirectorioTemporal(String directorioTemporal) {
		this.directorioTemporal = directorioTemporal;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	public OperacionElectronicaBean getOperacionElectronicaBean() {
		return operacionElectronicaBean;
	}

	public void setOperacionElectronicaBean(OperacionElectronicaBean operacionElectronicaBean) {
		this.operacionElectronicaBean = operacionElectronicaBean;
	}

	public String getRutaArchivoCsv() {
		return rutaArchivoCsv;
	}

	public void setRutaArchivoCsv(String rutaArchivoCsv) {
		this.rutaArchivoCsv = rutaArchivoCsv;
	}

	public String getArchivoCsv() {
		return archivoCsv;
	}

	public void setArchivoCsv(String archivoCsv) {
		this.archivoCsv = archivoCsv;
	}

	public String getRutaArchivoPdf() {
		return rutaArchivoPdf;
	}

	public void setRutaArchivoPdf(String rutaArchivoPdf) {
		this.rutaArchivoPdf = rutaArchivoPdf;
	}

	public String getArchivoPdf() {
		return archivoPdf;
	}

	public void setArchivoPdf(String archivoPdf) {
		this.archivoPdf = archivoPdf;
	}
	
	
	/*
	 * Obtener lista de claves de financiamiento repetidas
	 */
	

	public void setNumerosCreditoRepetidos(List numerosCreditoRepetidos){
		this.numerosCreditoRepetidos = numerosCreditoRepetidos;
	}
	
	public List getNumerosCreditoRepetidos(){
		return this.numerosCreditoRepetidos;
	}
	
	public boolean queryNumerosCreditoRepetidos(String[] msg)
		throws AppException {
		
		log.info("queryNumerosCreditoRepetidos(E)");
		
		BufferedReader br       			= null;
		
		boolean 			exito					= true;
		
		List				repetidos			= new ArrayList();
		int 				indexRepetidos		= 0;
		List				numeros				= new ArrayList();
		int 				indexNumeros		= 0;
		String    		numeroCredito		= null;
		
		try {

			// Obtener BufferedReader del Archivo CSV			
			br = createBufferedReaderArchivoValidacion();
			
			// Leer contenido del encabezado: primeras 8 l�neas.
			this.leeEncabezado(br);
			
			// Nota (09/12/2014 04:37:48 p.m.): Bajo un estimado m�ximo de 100,000 registros y con una longitud promedio de 11 caracteres,
			// se estar�a usando 1.1 MB de espacio en memoria que para este desarrollo es suficiente, por
			// lo que si en un futuro se decide incrementar el tama�o m�ximo de 5MB del archvo csv, se deber�
			// evaluar si aqu� tambi�n se requerir� cambiar la implementaci�n.
			// @author jshernandez
			
			// Determinar los numeros de credito que se encuentran repetidos
			for(int i=0;i<this.getRecordsNumber();i++){
				
				this.parse(br);
				
				numeroCredito = this.getNumeroCredito().toString();
				
				if( Comunes.esVacio(numeroCredito) ){
					continue;
				}
				
				// Revisar si el n�mero de credito ya existe en la lista de claves repetidas
				if(        ( indexRepetidos = Collections.binarySearch(repetidos, numeroCredito) ) >= 0 ){
					continue;
				// Revisar si el n�mero de cr�dito ya existe en la lista normal 
				} else if( ( indexNumeros   = Collections.binarySearch(numeros,   numeroCredito) ) >= 0 ){
					numeros.remove(indexNumeros);
					// Agregar numero de credito a la lista de claves repetidas
					indexRepetidos = -indexRepetidos-1;
					repetidos.add(indexRepetidos,numeroCredito);
					// En caso de que se hayan detectado m�s de mil claves de n�meros de cr�ditos repetidas, cancelar
					// la b�squeda.
					if(repetidos.size() > 1000 ){
						break;
					}
				// Agregar el n�mero de cr�dito a la lista inicial de n�meros
				} else {
					indexNumeros = -indexNumeros-1;
					numeros.add(indexNumeros,numeroCredito);
				}
								
			}
			
			if(        repetidos.size() >  1000 ){
				exito 	= false;
				msg[0] 	= "Se encontraron mas de 1,000 n�meros de cr�dito repetidos. Se aborta la operaci�n.";
			// Solo inicializar lista si se encontr� al menos un n�mero de cr�dito repetido
			} else if( repetidos.size() != 0    ){
				this.setNumerosCreditoRepetidos(repetidos);	
			}
				
		} catch(Exception e){
					
			exito  = false;
			
			log.error("queryBasesOperacionErrorComplementos(Exception)");
			log.error("queryBasesOperacionErrorComplementos.msg = <" + msg+ ">");
			e.printStackTrace();
			
			throw new AppException("Error al construir lista con los n�meros de cr�dito repetidos:", e);
			
		} finally {
			
			if( br      != null) { try { br.close();      }catch(Exception e){} }
			
			log.info("queryBasesOperacionErrorComplementos(S)");
			
		}
		
		return exito;
		
	}
	 
	public boolean getExisteCargaPreviaSoloNafinet() {
		return this.existeCargaPreviaSoloNafinet;
	}

	public void setExisteCargaPreviaSoloNafinet(boolean existeCargaPreviaSoloNafinet) {
		this.existeCargaPreviaSoloNafinet = existeCargaPreviaSoloNafinet;
	}
	
	/*
	 * M�todos correspondientes a la interfaz RenglonValidator
	 */
	private String 				processID 		= null; 
	private ValidacionResult 	resultados 		= null;
	private boolean				hayError			= false;
	private int 					recordsNumber	= 0;
	
	public void validaRegistro( int processedRegisters, BufferedReader br) 
		throws Exception {
		parse(br);
		this.operacionElectronicaBean.validarCargaCarteraPrendaria( processedRegisters+7, this ); // Ajuste de n�meros de l�nea
	}
	
	public void validaCifrasControl(){
		this.operacionElectronicaBean.validarCifrasControlCarteraPrendaria( this ); 
	}
	
	public void setProcessID(String processID){
		this.processID = processID;
	}
	
	public String getProcessID(){
		return this.processID;
	}
	
	public void setValidacionResult(ValidacionResult resultados){
		this.resultados = resultados;
	}
	
	public ValidacionResult getValidacionResult(){
		return this.resultados;
	}
	
	public boolean getHayError() {
		return this.hayError;
	}

	public void setHayError(boolean hayError) {
		this.hayError = hayError;
	}
	
	public int getRecordsNumber(){
		return this.recordsNumber;
	}
		
	public void setRecordsNumber(int recordsNumber){
		this.recordsNumber = recordsNumber;
	}
	
	public boolean queryRecordsNumber(String[] msg)
		throws AppException {
		
		log.info("queryRecordsNumber(E)");
		
		BufferedReader br       			= null;
		boolean 			exito					= true;
		
		try {
			
			// Obtener BufferedReader del Archivo CSV				
			br = createBufferedReaderArchivoValidacion();
			
			// Nota: En caso de que se necesite mejorar el performance, se podr�a utilizar
			// los metodos: leeEncabezado y parse; tamb�en se tendr�an que crear un m�todo
			// valide cuando un StringBuffer venga vac�o o con espacios en blanco.
			
			// Leer las primeras ocho l�neas del encabezado
			String linea 			= null;
			int 	 ctaRegistros 	= 0;
			while( (linea = br.readLine() ) != null ){
				ctaRegistros++;
				if( ctaRegistros == 8 ){
					break;
				}
			}
			

			// Determinar l�nea donde vienen los totales
			boolean 	sumatoriaSaldoTotalEncontrado = false;
			ctaRegistros 	  								= 0;
			while( ( linea = br.readLine() ) != null ){
				ctaRegistros++;
				if( linea.matches("^(?i)(\"[ \\t]*Total[ \\t]*\"[ \\t]*|[ \\t]*Total[ \\t]*)(,.*)*$") ){
					sumatoriaSaldoTotalEncontrado = true;
					break;
				}
			}
					
			/*
			// Nota (19/12/2014 04:11:35 p.m.):
			// Zara, @dy: A solicitud de Zara y con el proposito de facilitarle la vida a los usuarios, solo se validar� que
			// el primer campo traiga la etiqueta totales. Por lo que se deja comentada la siguiente secci�n.
			Pattern 	pattern 								= Pattern.compile("^(\"[ \\t]*Total[ \\t]*\"|[ \\t]*Total[ \\t]*),((\"[ \\t]+\"|[ \\t]+)*,){10}([\"]([\\-]?[\\d,.]+)[\"]|[\\-]?[\\d,.]+)(,(\"[ \\t]+\"|[ \\t]+)*)*$"); 
			Matcher 	matcher 								= null;
			boolean 	sumatoriaSaldoTotalEncontrado = false;
			ctaRegistros 	  								= 0;
			while( (linea = br.readLine() ) != null ){
				ctaRegistros++;
				matcher = pattern.matcher(linea);
				if( matcher.matches() ){
					String	sumatoriaSaldosTotales	= matcher.group(5) != null ? matcher.group(5):matcher.group(4);
					boolean	tieneComas 					= sumatoriaSaldosTotales.indexOf(",") >= 0?true:false;
					if(        tieneComas  && ( sumatoriaSaldosTotales.matches("^[\\-]?[\\d]{1,3}(,\\d\\d\\d)+[\\.]?$") || sumatoriaSaldosTotales.matches("^[\\-]?[\\d]{1,3}(,\\d\\d\\d)+[\\.][\\d]+$") ) ){
						sumatoriaSaldoTotalEncontrado = true;
						break;
					} else if( !tieneComas && ( sumatoriaSaldosTotales.matches("^[\\-]?[\\d]+[\\.]?$")                  || sumatoriaSaldosTotales.matches("^[\\-]?[\\d]*[\\.][\\d]+$") )                  ){
						sumatoriaSaldoTotalEncontrado = true;
						break;
					}
				}
			}
			*/
			
			// Hacer ajuste para no incluir el renglon de totales como un registro valido
			ctaRegistros = ctaRegistros - 1;
			
			// Verificar que se haya encontrado la l�nea con los totales
			if(        !sumatoriaSaldoTotalEncontrado                     ){
				exito  = false;
				msg[0] = "La etiqueta \"Total\" que indica la conclusi�n de cr�ditos a procesar no se encontr� en el archivo.";
			} else if( sumatoriaSaldoTotalEncontrado && ctaRegistros <= 0 ){
				exito  = false;
				msg[0] = "Se envi� etiqueta \"Total\" que indica la conclusi�n de cr�ditos a procesar pero no se especific� ning�n cr�dito.";
			}
			
			// En caso de que la operaci�n haya sido exitosa, asignar n�mero de registros validar
			if( exito ){
				setRecordsNumber(ctaRegistros);
			}

		} catch(Exception e){
			
			exito = false;
			
			log.error("queryRecordsNumber(Exception)");
			log.error("queryRecordsNumber.msg = <" + msg + ">");
			e.printStackTrace();
			
			throw new AppException("Error al determinar n�mero de registros a validar:", e);
			
		} finally {
			
			if( br != null ){ try { br.close(); }catch(Exception e){} }
			
			log.info("queryRecordsNumber(S)");
			
		}
		
		return exito;
		
	}
	
	public void leeEncabezado(BufferedReader br) 
		throws Exception{
			
		// Leer las primeras ocho l�neas que corresponden con el encabezado
		for(int i=0;i<8;i++){
			parse(br);
		}
		
	}
	
	public boolean inicializar(String[] msg)
		throws Exception {
		
		log.info("inicializar(E)");
		
		boolean	exito = true;
		
   	try {

   		// Nota: Ser�a �til no dejar cargar archivos csv y pdf vac�os => length == 0

   		String[] hint = new String[1];
   		
			// Validar que el archivo csv exista
			if(			Comunes.esVacio(this.archivoCsv) ){
				exito 	= false;
				msg[0] 	= "El archivo csv es obligatorio.";
			// Validar que el archivo pdf exista
			} else if(	Comunes.esVacio(this.archivoPdf) ){
				exito 	= false;
				msg[0] 	= "El archivo pdf es obligatorio.";
			} else if(	Comunes.excedeTamanioMaximo(this.getRutaArchivoCsv(), 5242880L)){ // > 5 MB
				exito 	= false;
				msg[0] 	= "El tama�o del archivo csv excede el l�mite permitido que es de 5MB.";
			} else if(	Comunes.excedeTamanioMaximo(this.getRutaArchivoPdf(), 5242880L)){ // > 5 MB
				exito 	= false;
				msg[0] 	= "El tama�o del archivo pdf excede el l�mite permitido que es de 5MB.";
			} else if( !this.archivoCsv.matches("(?i)^.*\\.csv$") ){
				exito 	= false;
				msg[0] 	= "El archivo csv seleccionado, debe tener extensi�n csv.";
			} else if( !this.archivoPdf.matches("(?i)^.*\\.pdf$") ){
				exito 	= false;
				msg[0] 	= "El archivo pdf seleccionado, debe tener extensi�n pdf.";
			} else if(	!this.operacionElectronicaBean.validaNombreArchivoAnexo5(this.archivoCsv,this.claveIF,hint) ){
				exito 	= false;
				msg[0] 	= "El nombre del archivo csv es incorrecto." + hint[0];
			} else if(	!this.operacionElectronicaBean.validaNombreArchivoAnexo5(this.archivoPdf,this.claveIF,hint) ){
				exito 	= false;
				msg[0]	= "El nombre del archivo pdf es incorrecto." + hint[0];
			} else if(	this.operacionElectronicaBean.existeCargaPreviaCarteraPrendaria(this.archivoCsv)){
				exito 	= false;
				msg[0] 	= "La cartera prendar�a ya ha sido procesada, por favor comun�quese al �rea de Subdirecci�n de Supervisi�n y Seguimiento de Cr�dito.";
				/*
				// Nota: Se omite la validaci�n debido a que el PDF y CSV se insertan en la misma carga.
				} else if(	!this.operacionElectronicaBean.existeCargaPreviaCarteraPrendaria(this.archivoPdf)){
					exito = false;
					msg[0] = "Se intent� procesar un periodo que ya est� cargado.";
				*/
			// Obtener n�mero de cr�ditos a validar.
			} else if( !queryRecordsNumber(msg)){
				exito 	= false;
   			//msg[0] 	= msg[0];
   		// Obtener lista de n�meros de cr�dito repetidos. En caso de que hayan m�s de mil claves abortar la validaci�n.
   		} else if( !queryNumerosCreditoRepetidos(msg) ){
   			exito 	= false;
   			//msg[0] 	= msg[0];
			} else {	
				/*
				Determinar si los archivos cargados existen en NAFINET pero no en CCBE
				*/
				if( this.operacionElectronicaBean.existeCargaPreviaCarteraPrendariaSoloNafinet(this.archivoCsv) ){
					this.setExisteCargaPreviaSoloNafinet(true);
				} else {
					this.setExisteCargaPreviaSoloNafinet(false);
				}
			}
			
   	} catch (Exception e) {
			
   		log.error("inicializar(Exception): Fall� la inicializaci�n de la validaci�n: "+e.getMessage());
			log.error("inicializar.numeroConsecutivo             = <" + numeroConsecutivo             + ">");
			log.error("inicializar.numeroCredito                 = <" + numeroCredito                 + ">");
			log.error("inicializar.nombreAcreditado              = <" + nombreAcreditado              + ">");
			log.error("inicializar.tipoOperacion                 = <" + tipoOperacion                 + ">");
			log.error("inicializar.fechaDisposicion              = <" + fechaDisposicion              + ">");
			log.error("inicializar.tipoTasa                      = <" + tipoTasa                      + ">");
			log.error("inicializar.tasaBase                      = <" + tasaBase                      + ">");
			log.error("inicializar.puntosAdicionales             = <" + puntosAdicionales             + ">");
			log.error("inicializar.montoOtorgado                 = <" + montoOtorgado                 + ">");
			log.error("inicializar.recursosSolicitados           = <" + recursosSolicitados           + ">");
			log.error("inicializar.plazoOriginal                 = <" + plazoOriginal                 + ">");
			log.error("inicializar.capitalVigente                = <" + capitalVigente                + ">");
			log.error("inicializar.interesesVigentes             = <" + interesesVigentes             + ">");
			log.error("inicializar.capitalVencido                = <" + capitalVencido                + ">");
			log.error("inicializar.interesesVencidos             = <" + interesesVencidos             + ">");
			log.error("inicializar.interesesMoratorios           = <" + interesesMoratorios           + ">");
			log.error("inicializar.saldoTotal                    = <" + saldoTotal                    + ">");
			log.error("inicializar.diasRetraso                   = <" + diasRetraso                   + ">");
			log.error("inicializar.descripcion                   = <" + descripcion                   + ">");
			
			log.error("inicializar.fieldIndex                    = <" + fieldIndex                    + ">");
			log.error("inicializar.fieldSize                     = <" + fieldSize                     + ">");
					
			log.error("inicializar.claveIF                       = <" + claveIF                       + ">");
			log.error("inicializar.directorioTemporal            = <" + directorioTemporal            + ">");
			log.error("inicializar.login                         = <" + login                         + ">");
			log.error("inicializar.operacionElectronicaBean      = <" + operacionElectronicaBean      + ">");
			log.error("inicializar.rutaArchivoCsv                = <" + rutaArchivoCsv                + ">");
			log.error("inicializar.archivoCsv                    = <" + archivoCsv                    + ">");
			log.error("inicializar.rutaArchivoPdf                = <" + rutaArchivoPdf                + ">");
			log.error("inicializar.archivoPdf                    = <" + archivoPdf                    + ">");
			log.error("inicializar.numerosCreditoRepetidos       = <" + numerosCreditoRepetidos       + ">");
			log.error("inicializar.existeCargaPreviaSoloNafinet  = <" + existeCargaPreviaSoloNafinet  + ">");
			
			log.error("inicializar.processID                     = <" + processID                     + ">");
			log.error("inicializar.resultados                    = <" + resultados                    + ">");
			log.error("inicializar.hayError                      = <" + hayError                      + ">");
			log.error("inicializar.recordsNumber                 = <" + recordsNumber                 + ">");
			e.printStackTrace();
			
			/*
			exito 	= false;
			msg[0] 	= "Fall� la inicializaci�n de la validaci�n: " + e.getMessage();
			*/
			
			throw e;
			
		} finally {
			
			log.info("inicializar(S)");
			
		}
		
		return exito;
		
	}
	
	public void finalizar(){}
	
	public BufferedReader createBufferedReaderArchivoValidacion()
		throws Exception {
		
		return new BufferedReader(new InputStreamReader(new FileInputStream( getRutaArchivoCsv() ),"ISO-8859-1"));
		
	}
	
}