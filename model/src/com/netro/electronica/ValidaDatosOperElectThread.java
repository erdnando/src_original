package com.netro.electronica;
import java.io.Serializable;
import com.netro.exception.*;
import netropology.utilerias.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class ValidaDatosOperElectThread implements Runnable, Serializable {
	
	private int 				numberOfRegisters;
	private boolean 			started;
	private boolean 			running;
	private int 				processedRegisters;
	private boolean			error;
	private boolean 			terminoValid;
	private String 			cveCargaTmp;
	private OperacionElectronicaBean operElectronica;
	

   public ValidaDatosOperElectThread() {
	numberOfRegisters 	= 0;
	started 			   	= false;
	running 			   	= false;
	terminoValid			= false;
	processedRegisters 	= 0;
	operElectronica		= new OperacionElectronicaBean();
	error						= false;
   }
	 
   protected void validaRegistrosOperElect() {
		
	try {

		operElectronica.validaCargaTmpFondoLiquido(cveCargaTmp, "");
		terminoValid = true;

	} catch(Exception e){
			setRunning(false);
			processedRegisters = numberOfRegisters ;
			setError(true);
			e.printStackTrace();
			System.out.println("Error en ValidaDatosPymeMasivaThread::validateRegisters al extraer numero de registro de la base de datos");
			// terminar el proceso si falla la funcion de validacion para algun registro
	}
		
   }
	
	public synchronized int getNumberOfRegisters() {
       return numberOfRegisters;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		return((processedRegisters == 0 && numberOfRegisters == 0)?false:(processedRegisters == numberOfRegisters));
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
	public synchronized void setError(boolean error) {
       this.error = error;
   }
	
	public synchronized boolean hasError() {
       return error;
   }
	
	public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }
	

   public void run() {
		 
		boolean		lbOK	=  true;
		
		// Realizar la Validacion de los datos
      try {
			
			//if(lbOK == false) 
				setRunning(true);
			
				while (isRunning() && !terminoValid){
					if(lbOK && !terminoValid){
						lbOK = false;
						validaRegistrosOperElect();
					}
				}
		
      } finally {
			setRunning(false);
      }

    }


	public void setCveCargaTmp(String cveCargaTmp) {
		this.cveCargaTmp = cveCargaTmp;
	}


	public String getCveCargaTmp() {
		return cveCargaTmp;
	}


	public void setTerminoValid(boolean terminoValid) {
		this.terminoValid = terminoValid;
	}


	public boolean getTerminoValid() {
		return terminoValid;
	}

	

}