package com.netro.electronica;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;
import com.netro.zip.ComunesZIP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.carga.ValidacionResult;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "OperacionElectronicaEJB", mappedName = "OperacionElectronicaEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class OperacionElectronicaBean implements OperacionElectronica {

    /*
	 * Variable que se emplea para enviar mensajes al log.
	 */
    private final static Log log = ServiceLocator.getInstance().getLog(OperacionElectronicaBean.class);
    private Writer xml;
    private int indiceRenglon;
    private boolean datosHojaInicializados;
    private HashMap estilos;
    private String directorioTemporal;
    private String[] longitudesColumna;
    private StringBuffer columnasCombinadas;

    private int cuentaColumnasCombinadas;
    private CreaArchivo archivo;
    private String hoja;
    private int cuentaHojas;


    private final static BigDecimal CERO = new BigDecimal("0");

    //*************************** CASO DE USO: CU09 - FONDO LIQUIDO **************************************

    public String obtieneArchivoErroresFL(String rutaDestino, String cveCargaTmp) throws AppException {
        String strSQL = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        AccesoDB con = new AccesoDB();
        String nombreArchivoTmp = null;

        try {
            con.conexionDB();

            CreaArchivo creaArchivo = new CreaArchivo();
            nombreArchivoTmp = creaArchivo.nombreArchivo() + ".xls";
            ComunesXLS xls = new ComunesXLS(rutaDestino + nombreArchivoTmp);

            xls.setTabla(3);
            xls.setCelda("Linea", "celda01", ComunesXLS.CENTER, 1);
            xls.setCelda("Contrato", "celda01", ComunesXLS.CENTER, 1);
            xls.setCelda("Descripcion", "celda01", ComunesXLS.CENTER, 1);

            strSQL =
                "SELECT   a.ic_carga_tmp ic_carga_tmp, a.ig_linea ig_linea, " +
                "         a.ic_num_contrato ic_num_contrato, a.ic_financiera ic_financiera, " +
                "         a.fg_saldo_hoy fg_saldo_hoy, a.ic_moneda ic_moneda, " +
                "         b.cg_razon_social nombreif, a.cg_error errores " +
                "    FROM opetmp_carga_saldo_fliquido a, comcat_if b " +
                "   WHERE DECODE (LENGTH (TRIM (TRANSLATE (a.ic_financiera, '0123456789', ' '))), " +
                "                 NULL, TO_NUMBER (a.ic_financiera), " + "                 -1 " +
                "                ) = b.ic_financiera(+) " + "   AND a.ic_carga_tmp = ? " +
                "   AND a.cg_error IS NOT NULL " + "ORDER BY a.ig_linea ";

            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(cveCargaTmp));
            rs = ps.executeQuery();
            while (rs.next()) {
                String linea = rs.getString("ig_linea") == null ? "" : rs.getString("ig_linea");
                String numContrato = rs.getString("ic_num_contrato") == null ? "" : rs.getString("ic_num_contrato");
                String errores = rs.getString("errores") == null ? "" : rs.getString("errores");

                xls.setCelda(linea, "formas", ComunesXLS.LEFT, 1);
                xls.setCelda(numContrato, "formas", ComunesXLS.LEFT, 1);
                xls.setCelda(errores, "formas", ComunesXLS.LEFT, 1);

            }
            rs.close();
            ps.close();

            xls.cierraTabla();
            xls.cierraXLS();

            return nombreArchivoTmp;

        } catch (Exception e) {
            throw new AppException("Error al obtener archivo de errores de carga de fondos liquidos ", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public void enviaCargaFondoLiquido(String cveCargaTmp, String usuario, String nombreUser) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        boolean commit = true;
        String strSQL = "";
        try {
            con.conexionDB();

            strSQL =
                "SELECT   a.ic_carga_tmp ic_carga_tmp, a.ig_linea ig_linea, a.ic_num_contrato ic_num_contrato, a.ic_financiera ic_financiera,  " +
                "         a.fg_saldo_hoy fg_saldo_hoy, a.ic_moneda ic_moneda, cm.cd_nombre nombremoneda,  " +
                "			 decode(b.cg_razon_social,null, a.cg_nombre_if_arch, b.cg_razon_social)  nombreif " +
                "    FROM opetmp_carga_saldo_fliquido a, comcat_if b , comcat_moneda cm " +
                "   WHERE to_number(a.ic_financiera) = b.ic_financiera(+) " +
                "   AND to_number(a.ic_moneda) = cm.ic_moneda " + "   AND a.ic_carga_tmp = ? " +
                "   AND a.cg_error IS NULL " + "ORDER BY a.ig_linea ";


            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(cveCargaTmp));
            rs = ps.executeQuery();


            //int linea = 0;
            List lstRegCorreo = new ArrayList();
            HashMap hmData = new HashMap();
            String cveIf = "";
            while (rs.next()) {
                cveIf = "";
                hmData = new HashMap();
                String numSIRAC = rs.getString("ic_financiera");

                hmData.put("CONTRATO", rs.getString("ic_num_contrato"));
                hmData.put("NOMBREIF", rs.getString("nombreif") == null ? "" : rs.getString("nombreif"));
                hmData.put("CVESIRAC", rs.getString("ic_financiera"));
                hmData.put("SALDOHOY", rs.getString("fg_saldo_hoy"));
                hmData.put("MONEDA", rs.getString("ic_moneda"));
                hmData.put("NOMBREMONEDA", rs.getString("nombremoneda"));
                lstRegCorreo.add(hmData);

                cveIf = existeIFxCveSIRAC(numSIRAC);

                if (!"".equals(cveIf)) {
                    strSQL =
                        "INSERT INTO ope_carga_saldo_fliquido " +
                        "            (ic_carga,  ic_num_contrato, ic_financiera, fg_saldo_hoy, " +
                        "             ic_moneda, ic_usuario, cg_nom_usuario, ic_if " + "            ) " +
                        "     VALUES (ope_carga_saldo_fliquido_seq.nextval, ?, ?, ?, " + "             ?, ?, ?, ? " +
                        "            ) ";

                    ps2 = con.queryPrecompilado(strSQL);
                    ps2.setLong(1, Long.parseLong(rs.getString("ic_num_contrato")));
                    ps2.setLong(2, Long.parseLong(rs.getString("ic_financiera")));
                    ps2.setBigDecimal(3, new BigDecimal(rs.getString("fg_saldo_hoy")));
                    ps2.setLong(4, Long.parseLong(rs.getString("ic_moneda")));
                    ps2.setString(5, usuario);
                    ps2.setString(6, nombreUser);
                    ps2.setLong(7, Long.parseLong(cveIf));
                    ps2.executeUpdate();
                    ps2.close();
                }
            }
            rs.close();
            ps.close();

            if (lstRegCorreo != null && lstRegCorreo.size() > 0) {
                enviaCorreoSaldoFLiquido(lstRegCorreo, nombreUser);
            }

        } catch (Throwable t) {
            commit = false;
            throw new AppException("Error al cargar y enviar el Fondo Liquido ", t);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    private String existeIFxCveSIRAC(String cveSIRAC) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String strSQL = "";
        String cveIf = "";
        try {
            con.conexionDB();

            strSQL = "SELECT ic_if cveif " + "  FROM comcat_if " + " WHERE ic_financiera = ? ";

            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(cveSIRAC));
            rs = ps.executeQuery();
            if (rs.next())
                cveIf = rs.getString("cveif");
            rs.close();
            ps.close();

            return cveIf;
        } catch (Throwable t) {
            throw new AppException("Error al verificar si el IF existe en NAFINET", t);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    private void enviaCorreoSaldoFLiquido(List lstRegEnviar, String nombreUser) throws AppException {
        Correo correo = new Correo();
        String msgHTML = "";
        BigDecimal montoSaldo = new BigDecimal("0.00000");
        BigDecimal montoTotal = new BigDecimal("0.00000");
        try {

            String styleEncabezados =
                " style='font-family: Verdana, Arial, Helvetica, sans-serif; " + "font-size: 10px; " +
                "font-weight: bold;" + "color: #FFFFFF;" + "background-color: #4d6188;" + "padding-top: 3px;" +
                "padding-right: 1px;" + "padding-bottom: 1px;" + "padding-left: 3px;" + "height: 25px;" +
                "border: 1px solid #1a3c54;'";

            String style =
                "style='font-family:Verdana, Arial, Helvetica, sans-serif;" + "color:#000066; " + "padding-top:1px; " +
                "padding-right:2px; " + "padding-bottom:1px; " + "padding-left:2px; " + "height:22px; " +
                "font-size:11px;'";

            msgHTML =
                "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'><p>Buen d&iacute;a.<br>Anexo los saldos del d&iacute;a de hoy. Saludos </p><br>";

            if (lstRegEnviar != null && lstRegEnviar.size() > 0) {
                msgHTML += "<table border=\"1\">";
                for (int x = 0; x < lstRegEnviar.size(); x++) {
                    HashMap hm = (HashMap) lstRegEnviar.get(x);
                    montoTotal = montoTotal.add(new BigDecimal((String) hm.get("SALDOHOY")));
                    if (x == 0) {
                        msgHTML +=
                            "	<tr>" + "		<td align=\"center\" " + styleEncabezados + ">CONTRATO</td>" +
                            "		<td align=\"center\" " + styleEncabezados + ">NOMBRE</td>" + "		<td align=\"center\" " +
                            styleEncabezados + ">CLAVE SIRAC</td>" + "		<td align=\"center\" " + styleEncabezados +
                            ">SALDO HOY</td>" + "		<td align=\"center\" " + styleEncabezados + ">MONEDA</td>" +
                            "	</tr>";
                    }
                    montoSaldo.add(new BigDecimal((String) hm.get("SALDOHOY")));
                    if (montoSaldo != null)
                        montoTotal = montoTotal.add(montoSaldo);
                    msgHTML +=
                        "	<tr> " + "	<td align=\"center\" " + style + "> " + (String) hm.get("CONTRATO") +
                        "</td>" + "	<td align=\"right\" " + style + "> " + (String) hm.get("NOMBREIF") + "</td>" +
                        "	<td align=\"right\" " + style + "> " + (String) hm.get("CVESIRAC") + "</td>" +
                        "	<td align=\"right\" " + style + "> $" + (String) hm.get("SALDOHOY") + "</td>" +
                        "	<td align=\"center\" " + style + "> " + (String) hm.get("NOMBREMONEDA") + "</td>" +
                        " </tr> ";
                }

                msgHTML +=
                    "	<tr>" + "		<td align=\"center\" " + styleEncabezados + ">TOTAL</td>" +
                    "		<td align=\"center\" " + styleEncabezados + "></td>" + "		<td align=\"center\" " +
                    styleEncabezados + "></td>" + "	<td align=\"center\" " + styleEncabezados + ">$" +
                    montoTotal.toPlainString() + "</td>" + "		<td align=\"center\" " + styleEncabezados + "></td>" +
                    "	</tr>";
                msgHTML += "</table>";
            }

            msgHTML += "<p>" + nombreUser + "</p>";
            msgHTML += "<p>NACIONAL FINANCIERA S.N.C. Direcci&oacute;n de Promoci&oacute;n de Mercados.</p></form>";


            HashMap hmCorreos = obtieneInfoGerenteSubdirector();
            log.debug("hmCorreos  " + hmCorreos);
            log.info("MAILS_FONDO_LIQUIDO" + (String) hmCorreos.get("MAILS_FONDO_LIQUIDO"));

            correo.enviarTextoHTML("no_response@nafin.gob.mx", (String) hmCorreos.get("MAILS_FONDO_LIQUIDO"),
                                   "Consulta de Saldo de Fondo L�quido", msgHTML);
            //correo.enviarTextoHTML("no_response@nafin.gob.mx","abautista@servicio.nafin.gob.mx","Consulta de Saldo de Fondo L�quido", msgHTML);
        } catch (Throwable t) {
            throw new AppException("Error al enviar correo de la carga de Fondo Liquido", t);
        }
    }

    public String cargaDatosTmpFondoLiquido(String rutaArchivo) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean commit = true;
        BufferedReader brt = null;
        String strSQL = "";
        try {
            con.conexionDB();
            java.io.File ft = new java.io.File(rutaArchivo);
            brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));

            String linea = "";
            VectorTokenizer vtd = null;
            Vector vecdet = null;
            String numContrato = "";
            String nombreIF = "";
            String cveSirac = "";
            String saldoHoy = "";
            String cveMoneda = "";
            String cveCargaTmp = "";

            strSQL = "SELECT NVL (MAX (ic_carga_tmp),0)+1 cvecarga " + "  FROM opetmp_carga_saldo_fliquido ";

            ps = con.queryPrecompilado(strSQL);
            rs = ps.executeQuery();
            if (rs.next())
                cveCargaTmp = rs.getString("cvecarga");
            rs.close();
            ps.close();

            strSQL =
                "INSERT INTO opetmp_carga_saldo_fliquido " +
                "            (ic_carga_tmp, ig_linea, cg_nombre_if_arch, ic_num_contrato, ic_financiera, fg_saldo_hoy, ic_moneda ) " +
                "     VALUES (?, ?, ?, ?, ?, ?, ? ) ";

            int totalRegistros = 0;
            while ((linea = brt.readLine()) != null) {

                System.out.println("LINEA==========" + linea);
                vtd = new VectorTokenizer(linea, ",");
                vecdet = vtd.getValuesVector();

                numContrato = (vecdet.size() >= 1) ? vecdet.get(0).toString().trim() : "";
                nombreIF = (vecdet.size() >= 2) ? vecdet.get(1).toString().trim().toUpperCase() : "";
                cveSirac = (vecdet.size() >= 3) ? vecdet.get(2).toString().trim().toUpperCase() : "";
                saldoHoy = (vecdet.size() >= 4) ? vecdet.get(3).toString().trim().toUpperCase() : "0.0";
                cveMoneda = (vecdet.size() >= 5) ? vecdet.get(4).toString().trim().toUpperCase() : "";
                numContrato = ((numContrato.length() > 10) ? numContrato.substring(0, 11) : numContrato);
                nombreIF = ((nombreIF.length() > 100) ? nombreIF.substring(0, 101) : nombreIF);
                cveSirac = ((cveSirac.length() > 6) ? cveSirac.substring(0, 7) : cveSirac);
                saldoHoy = ((saldoHoy.length() > 23) ? saldoHoy.substring(0, 24) : saldoHoy);
                cveMoneda = ((cveMoneda.length() > 3) ? cveMoneda.substring(0, 4) : cveMoneda);
                totalRegistros++;

                ps = con.queryPrecompilado(strSQL);
                ps.setLong(1, Long.parseLong(cveCargaTmp));
                ps.setInt(2, totalRegistros);
                ps.setString(3, nombreIF);
                ps.setString(4, numContrato);
                ps.setString(5, cveSirac);
                ps.setString(6, saldoHoy);
                ps.setString(7, cveMoneda);
                ps.executeUpdate();
                ps.close();

            }
            brt.close();
            return cveCargaTmp;
        } catch (Throwable t) {
            commit = false;
            t.printStackTrace();
            throw new AppException("Error al cargar temporalmente el archivo de fondos liquidos", t);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public void validaCargaTmpFondoLiquido(String cveCargaTmp, String usuario) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        boolean commit = true;
        //boolean commit = true;
        String strSQL = "";

        try {
            con.conexionDB();
            strSQL =
                "SELECT ic_carga_tmp, ig_linea, cg_nombre_if_arch, ic_num_contrato, ic_financiera, fg_saldo_hoy, ic_moneda " +
                "  FROM opetmp_carga_saldo_fliquido " + " WHERE ic_carga_tmp = ? ";

            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(cveCargaTmp));
            rs = ps.executeQuery();
            StringBuffer strbErrores = new StringBuffer();

            while (rs.next()) {
                strbErrores = new StringBuffer();
                String numLinea = rs.getString("ig_linea") == null ? "" : rs.getString("ig_linea");
                String nombreIF = rs.getString("cg_nombre_if_arch") == null ? "" : rs.getString("cg_nombre_if_arch");
                String numContrato = rs.getString("ic_num_contrato") == null ? "" : rs.getString("ic_num_contrato");
                String numSirac = rs.getString("ic_financiera") == null ? "" : rs.getString("ic_financiera");
                String saldoHoy = rs.getString("fg_saldo_hoy") == null ? "" : rs.getString("fg_saldo_hoy");
                String cveMoneda = rs.getString("ic_moneda") == null ? "" : rs.getString("ic_moneda");
                //Validar campos obligatorios
                if (numContrato.equals(""))
                    strbErrores.append("El valor del campo \"Numero de Contrato\" es obligatorio. \n");
                if (nombreIF.equals(""))
                    strbErrores.append("El valor del campo \"Nombre del Intermediario\" es obligatorio. \n");
                if (numSirac.equals(""))
                    strbErrores.append("El valor del campo \"Clave SIRAC del IF\" es obligatorio. \n");
                if (saldoHoy.equals(""))
                    strbErrores.append("El valor del campo \"Saldo Hoy\" es obligatorio. \n");
                if (cveMoneda.equals(""))
                    strbErrores.append("EL valor del campo  \"Moneda\" es obligatorio. \n");

                //Validar longitud maxima permitida
                if (nombreIF.length() > 100)
                    strbErrores.append("El valor del campo \"Nombre del Intermediario\" excede la logitud m�xima permitida (100). \n");
                if (numContrato.length() > 10)
                    strbErrores.append("El valor del campo \"Numero de Contrato\" excede la logitud m�xima permitida (10). \n");
                if (numSirac.length() > 7)
                    strbErrores.append("El valor del campo \"Clave SIRAC del IF\" excede la logitud m�xima permitida (6). \n");
                String varAux = "";
                if (!saldoHoy.equals("") && saldoHoy.substring(0, 1).equals("-")) {
                    varAux = saldoHoy.substring(1, saldoHoy.length());
                } else if (!saldoHoy.equals("")) {
                    varAux = saldoHoy.substring(0, saldoHoy.length());
                }
                StringTokenizer tokens = new StringTokenizer(varAux, ".");
                String[] valor = new String[2];
                int cont = 0;
                String entero = "";
                String decimal = "";
                while (tokens.hasMoreTokens()) {
                    valor[cont] = tokens.nextToken();
                    cont++;
                }
                if (cont == 2) {
                    entero = valor[0];
                    decimal = valor[1];
                } else if (cont == 1) {
                    entero = valor[0];
                    decimal = "";
                }
                if (cveMoneda.length() > 3)
                    strbErrores.append("El valor del campo \"Moneda\" excede la logitud m�xima permitida (3). \n");

                //Validar tipo de dato por campo
                log.debug(" tama�o " + entero.length());
                log.debug(" tama�o " + decimal.length());
                if (!numContrato.equals("") && !Comunes.esNumeroEnteroPositivo(numContrato))
                    strbErrores.append("El valor del campo \"Numero de Contrato\" debe ser de tipo numerico. \n");
                if (!saldoHoy.equals("") && !Comunes.esDecimalPositivo(saldoHoy)) {
                    if (!this.esNumeroNegativo(saldoHoy)) {
                        strbErrores.append("El valor del campo \"Saldo Hoy\" debe ser de tipo numerico. \n");
                    } else if (entero.length() > 17 || decimal.length() > 5) {
                        strbErrores.append("El valor del campo \"Saldo Hoy\" excede la logitud m�xima permitida (17,5). \n");
                    }
                } else {
                    if (!saldoHoy.equals("") && saldoHoy.substring(0, 1).equals("-")) {
                        if (entero.length() > 17 || decimal.length() > 5) {
                            strbErrores.append("El valor del campo \"Saldo Hoy\" excede la logitud m�xima permitida (17,5). \n");
                        }
                    } else if (entero.length() > 17 || decimal.length() > 5) {
                        strbErrores.append("El valor del campo \"Saldo Hoy\" excede la logitud m�xima permitida (17,5). \n");
                    }
                }
                if (!cveMoneda.equals("") && !Comunes.esNumeroEnteroPositivo(cveMoneda))
                    strbErrores.append("El valor del campo \"Moneda\" debe ser de tipo numerico. \n");
                if (!numSirac.equals("") && !Comunes.esNumeroEnteroPositivo(numSirac))
                    strbErrores.append("El valor del campo \"Clave SIRAC del IF\" debe ser de tipo numerico,");

                if (strbErrores.length() >
                    0) {
                    //Guaraddo de errores detectados en la info.
                    strSQL =
 "UPDATE opetmp_carga_saldo_fliquido " + "   SET cg_error = ? " + " WHERE ic_carga_tmp = ? AND ig_linea = ? ";

                    ps2 = con.queryPrecompilado(strSQL);
                    ps2.setString(1,
                                  (strbErrores.toString().length() > 400) ? (strbErrores.toString()).substring(0, 399) :
                                  strbErrores.toString());
                    ps2.setLong(2, Long.parseLong(cveCargaTmp));
                    ps2.setLong(3, Long.parseLong(numLinea));
                    ps2.executeUpdate();
                    ps2.close();
                }

            }

            rs.close();
            ps.close();

        } catch (Throwable t) {
            commit = false;
            t.printStackTrace();
            throw new AppException("Error al validar info de Saldo de Fondo Liquido de la carga temporal realizada", t);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public HashMap obtieneCargaTmpFondoLiquido(String cveCargaTmp) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lstCorrectos = new ArrayList();
        List lstErroneos = new ArrayList();
        List contMonto = new ArrayList();
        HashMap hmDataCarga = new HashMap();
        String strSQL = "";
        String monto = "";
        BigDecimal montoTotal = new BigDecimal("0.00000");
        try {
            con.conexionDB();

            strSQL =
                "SELECT   ic_carga_tmp, ig_linea, ic_num_contrato, ic_financiera, " +
                "         fg_saldo_hoy, ic_moneda " + "    FROM opetmp_carga_saldo_fliquido " +
                "   WHERE ic_carga_tmp = ?  " + "   AND cg_error IS NULL " + "ORDER BY ig_linea ";

            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(cveCargaTmp));
            rs = ps.executeQuery();
            HashMap hmReg = new HashMap();
            while (rs.next()) {
                hmReg = new HashMap();
                hmReg.put("CVECARGATMP", rs.getString("ic_carga_tmp") == null ? "" : rs.getString("ic_carga_tmp"));
                hmReg.put("LINEA", rs.getString("ig_linea") == null ? "" : rs.getString("ig_linea"));
                hmReg.put("CONTRATO", rs.getString("ic_num_contrato") == null ? "" : rs.getString("ic_num_contrato"));
                hmReg.put("SALDOHOY", rs.getString("fg_saldo_hoy") == null ? "0.0" : rs.getString("fg_saldo_hoy"));
                monto = rs.getString("fg_saldo_hoy");
                if (monto != null)
                    montoTotal = montoTotal.add(new BigDecimal(monto));

                lstCorrectos.add(hmReg);
            }
            contMonto.add(montoTotal.toPlainString());
            rs.close();
            ps.close();
            hmDataCarga.put("CORRECTOS", lstCorrectos);
            hmDataCarga.put("MONTO", contMonto);


            strSQL =
                "SELECT   ic_carga_tmp, ig_linea, ic_num_contrato, ic_financiera, " +
                "         fg_saldo_hoy, ic_moneda, cg_error " + "    FROM opetmp_carga_saldo_fliquido " +
                "   WHERE ic_carga_tmp = ?  " + "   AND cg_error IS NOT NULL " + "ORDER BY ig_linea ";

            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(cveCargaTmp));
            rs = ps.executeQuery();

            while (rs.next()) {
                hmReg = new HashMap();
                hmReg.put("CVECARGATMP", rs.getString("ic_carga_tmp") == null ? "" : rs.getString("ic_carga_tmp"));
                hmReg.put("LINEA", rs.getString("ig_linea") == null ? "" : rs.getString("ig_linea"));
                hmReg.put("CONTRATO", rs.getString("ic_num_contrato") == null ? "" : rs.getString("ic_num_contrato"));
                hmReg.put("ERROR", rs.getString("cg_error") == null ? "" : rs.getString("cg_error"));
                lstErroneos.add(hmReg);
            }
            rs.close();
            ps.close();
            hmDataCarga.put("ERRONEOS", lstErroneos);

            return hmDataCarga;
        } catch (Throwable t) {
            t.printStackTrace();
            throw new AppException("Error al obtener registros erroneos y correctos de la carga tmp de fondo liquido",
                                   t);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }
    //*************************** CASO DE USO: CU03-CONCENTRADOR DE SOLICITUDES **************************************
    public List consultaAtencionSolicRec(String cveIf, String cveEstatus, String numSolic, String fecRecpIni,
                                         String fecRecpFin, String usuarioOP) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        List lstSolicRec = new ArrayList();
        String qrySql = "";
        try {
            con.conexionDB();

            qrySql =
                "SELECT ors.ic_solicitud cvesolicitud, ors.ic_if cveif, ors.ic_contrato cvecontrato, " +
                "       TO_CHAR (ors.df_solicitud, 'dd/mm/yyyy HH24:mi:ss') fecsolicitud, " +
                "       oc.cg_nombre_contrato nombrecontrato, " +
                "		  TO_CHAR (odi.df_firma_contrato, 'dd/mm/yyyy') fecfirmacontrato, " +
                "       odi.cg_contrato_oe nombrecontratooe, " +
                "       TO_CHAR (odi.df_firma_contrato_oe, 'dd/mm/yyyy') fecfirmacontratooe, " +
                "       ors.fn_monto_solicitud importesolic, cm.cd_nombre nombremoneda, " +
                "       ors.cg_des_recursos destinorecurso, " +
                "		  TO_CHAR (ors.df_pago_capital, 'dd/mm/yyyy') fecpagocapital, " +
                "       TO_CHAR (ors.df_pago_interes, 'dd/mm/yyyy') fecpagointeres,  " +
                "       TO_CHAR (ors.df_vencimiento, 'dd/mm/yyyy')  fecvencimiento, " +
                "       TO_CHAR (ors.df_asig_ejeop, 'dd/mm/yyyy HH24:mi:ss')  fecasignaejeop, " +
                "       ors.cg_nom_usuario_con1  nombreusrcon1, " +
                "       ors.ic_estatus_ope estatussolic, oe.cd_descripcion nombreestatus, " +
                "       ors.cg_observaciones_if observif, ors.cg_observaciones_op observop, " +
                //"	ors.cg_tipo_tasa || ' ' || ors.cg_tasa_interes || ' % ' AS tasainteres,  " +
                "       ors.cg_tipo_tasa tipotasa, " + "       ors.ic_tasa cvetasa, " +
                "       ct.cd_nombre nombretasa, " + "       ors.in_tasa_aceptada tasainteres,  " +
                "       ci.cg_razon_social nombreinter,  " + "		  ors.ic_moneda cvemoneda, " +
                "		  ors.ig_numero_prestamo numprestamo, " + "		  TO_CHAR(ors.df_operado,'dd/mm/yyyy') fecop, " +
                "       TO_CHAR(ors.df_rechazo,'dd/mm/yyyy HH24:mi:ss') fecrechazo, " +
                //"		  ors.cg_hora_operado horaop, " +
                "		  odi.ic_num_cuenta numcuenta, " + "		  odi.cg_banco nombrebanco, " + "		  ors.no_folio folio, " +
                "		  ors.cg_usuario_if usuarioif, " + "		  ors.cg_causas_rechazo causasRech, " +
                "		  odi.cg_cont_modificatorios countmodif, " + "		  ors.cc_acuse ccacuse, " +
                "		  nvl(dbms_lob.getlength(ors.bi_doc_cotizacion),0) sizefilecotiza, " +
                "		  nvl(dbms_lob.getlength(ors.bi_doc_cartera_pdf),0) sizefileCarterapdf " +
                "  FROM ope_reg_solicitud ors, " + "       ope_datos_if odi, " + "       comcat_moneda cm, " +
                "       opecat_estatus oe, " + "		  opecat_contrato oc, " + "		  comcat_if ci, " +
                "		  comcat_tasa ct " + " WHERE ors.ic_if = odi.ic_if " + "   AND odi.ic_if = ci.ic_if " +
                "   AND ors.ic_moneda = cm.ic_moneda " + "   AND ors.ic_tasa = ct.ic_tasa(+) " +
                "   AND ors.ic_estatus_ope = oe.ic_estatus_ope " + "	 AND ors.ic_contrato = oc.ic_contrato " +
                //"   AND ors.ic_if = ? " +
                "	 AND ors.ic_ejecutivo_op = ? ";


            if (cveIf != null && !"".equals(cveIf)) {
                qrySql += "   AND ors.ic_if = ? ";
            }
            if (cveEstatus != null && !"".equals(cveEstatus)) {
                qrySql += "   AND ors.ic_estatus_ope = ? ";
            }
            if (numSolic != null && !"".equals(numSolic)) {
                //qrySql += "   AND ors.ic_solicitud = ? ";
                qrySql += "   AND ors.cc_acuse = ? ";
            }
            if (fecRecpIni != null && !"".equals(fecRecpIni)) {
                qrySql += "   AND ors.df_solicitud >= TO_DATE (?, 'dd/mm/yyyy') ";
            }
            if (fecRecpFin != null && !"".equals(fecRecpFin)) {
                qrySql += "   AND ors.df_solicitud < (TO_DATE (?, 'dd/mm/yyyy') + 1) ";
            }

            int p = 0;
            ps = con.queryPrecompilado(qrySql);
            ps.setString(++p, usuarioOP);
            if (cveIf != null && !"".equals(cveIf))
                ps.setLong(++p, Long.parseLong(cveIf));
            if (cveEstatus != null && !"".equals(cveEstatus))
                ps.setLong(++p, Long.parseLong(cveEstatus));
            //if(numSolic!=null && !"".equals(numSolic)) ps.setLong(++p, Long.parseLong(numSolic));
            if (numSolic != null && !"".equals(numSolic))
                ps.setString(++p, numSolic);
            if (fecRecpIni != null && !"".equals(fecRecpIni))
                ps.setString(++p, fecRecpIni);
            if (fecRecpFin != null && !"".equals(fecRecpFin))
                ps.setString(++p, fecRecpFin);
            rs = ps.executeQuery();

            qrySql =
                "SELECT ofm.cg_descripcion descmod, " +
                "       TO_CHAR (ofm.df_firma_modificatoria, 'dd/mm/yyyy') fecmod " + "  FROM ope_det_fir_modifi ofm " +
                " WHERE ofm.ic_if = ? " + " ORDER BY ofm.ic_firma_modifi ";


            HashMap hmData;
            String allFecModCont = "";
            String fecModCont = "";
            String userIF = "";
            String nombreUserIF = "";
            String existeFileCotiza = "N";
            String existeFileCarteraPdf = "N";
            String revi = "";
            while (rs.next()) {
                allFecModCont = "";
                ps2 = con.queryPrecompilado(qrySql);
                ps2.setLong(1, Long.parseLong(rs.getString("cveif")));
                rs2 = ps2.executeQuery();
                while (rs2.next()) {
                    fecModCont = rs2.getString("descmod") + ": " + rs2.getString("fecmod");
                    allFecModCont += ("".equals(allFecModCont)) ? fecModCont : (", " + fecModCont);
                }
                rs2.close();
                ps2.close();

                userIF = rs.getString("usuarioif") == null ? "" : rs.getString("usuarioif");

                if (userIF != null && !"".equals(userIF)) {
                    nombreUserIF = (String) (obtieneInfoCorreo(userIF)).get("USUARIO_NOMBRE");
                }

                existeFileCotiza = (rs.getDouble("sizefilecotiza") > 0) ? "S" : "N";
                existeFileCarteraPdf = (rs.getDouble("sizefileCarterapdf") > 0) ? "S" : "N";
                revi = this.getExisteRevision(rs.getString("cvesolicitud"));
                hmData = new HashMap();
                hmData.put("CVESOLICITUD", (rs.getString("cvesolicitud") == null) ? "" : rs.getString("cvesolicitud"));
                hmData.put("CVEIF", (rs.getString("cveif") == null) ? "" : rs.getString("cveif"));
                hmData.put("CCACUSE", (rs.getString("ccacuse") == null) ? "" : rs.getString("ccacuse"));
                hmData.put("CVECONTRATO", (rs.getString("cvecontrato") == null) ? "" : rs.getString("cvecontrato"));
                hmData.put("FECSOLICITUD", (rs.getString("fecsolicitud") == null) ? "" : rs.getString("fecsolicitud"));
                hmData.put("NOMBRECONTRATO",
                           (rs.getString("nombrecontrato") == null) ? "" : rs.getString("nombrecontrato"));
                hmData.put("FECFIRMACONTRATO",
                           (rs.getString("fecfirmacontrato") == null) ? "" : rs.getString("fecfirmacontrato"));
                hmData.put("NOMBRECONTRATOOE",
                           (rs.getString("nombrecontratooe") == null) ? "" : rs.getString("nombrecontratooe"));
                hmData.put("FECFIRMACONTRATOOE",
                           (rs.getString("fecfirmacontratooe") == null) ? "" : rs.getString("fecfirmacontratooe"));
                hmData.put("IMPORTESOLIC", (rs.getString("importesolic") == null) ? "" : rs.getString("importesolic"));
                hmData.put("CVEMONEDA", (rs.getString("cvemoneda") == null) ? "" : rs.getString("cvemoneda"));
                hmData.put("NOMBREMONEDA", (rs.getString("nombremoneda") == null) ? "" : rs.getString("nombremoneda"));
                hmData.put("DESTINORECURSO",
                           (rs.getString("destinorecurso") == null) ? "" : rs.getString("destinorecurso"));
                hmData.put("FECPAGOCAPITAL",
                           (rs.getString("fecpagocapital") == null) ? "" : rs.getString("fecpagocapital"));
                hmData.put("FECPAGOINTERES",
                           (rs.getString("fecpagointeres") == null) ? "" : rs.getString("fecpagointeres"));
                hmData.put("FECVENCIMIENTO",
                           (rs.getString("fecvencimiento") == null) ? "" : rs.getString("fecvencimiento"));
                hmData.put("ESTATUSSOLIC", (rs.getString("estatussolic") == null) ? "" : rs.getString("estatussolic"));
                hmData.put("NOMBREESTATUS",
                           (rs.getString("nombreestatus") == null) ? "" : rs.getString("nombreestatus"));
                hmData.put("OBSERVIF", (rs.getString("observif") == null) ? "" : rs.getString("observif"));
                hmData.put("OBSERVOP", (rs.getString("observop") == null) ? "" : rs.getString("observop"));
                hmData.put("TASAINTERES", rs.getString("tasainteres") == null ? "" : rs.getString("tasainteres"));
                hmData.put("TIPOTASA", rs.getString("tipotasa"));
                hmData.put("CVETASA", rs.getString("cvetasa"));
                hmData.put("NOMBRETASA", rs.getString("nombretasa"));
                hmData.put("NUMPRESTAMO", (rs.getString("numprestamo") == null) ? "" : rs.getString("numprestamo"));
                hmData.put("FECOP", (rs.getString("fecop") == null) ? "" : rs.getString("fecop"));
                hmData.put("FECRECHAZO", (rs.getString("fecrechazo") == null) ? "" : rs.getString("fecrechazo"));
                hmData.put("NUMCUENTA", (rs.getString("numcuenta") == null) ? "" : rs.getString("numcuenta"));
                hmData.put("NOMBREBANCO", (rs.getString("nombrebanco") == null) ? "" : rs.getString("nombrebanco"));
                hmData.put("FOLIO", (rs.getString("folio") == null) ? "" : rs.getString("folio"));
                hmData.put("USUARIOIF", (rs.getString("usuarioif") == null) ? "" : rs.getString("usuarioif"));
                hmData.put("CAUSASRECH", (rs.getString("causasRech") == null) ? "" : rs.getString("causasRech"));
                hmData.put("COUNTMODIF", (rs.getString("countmodif") == null) ? "" : rs.getString("countmodif"));
                hmData.put("ALLFECMODCONT", allFecModCont);
                hmData.put("NOMBREUSERIF", nombreUserIF);
                hmData.put("EXISTEFILECOTIZA", existeFileCotiza);
                hmData.put("FECASIGNAEJEOP",
                           (rs.getString("fecasignaejeop") == null) ? "" : rs.getString("fecasignaejeop"));
                hmData.put("NOMBREUSRCON1",
                           (rs.getString("nombreusrcon1") == null) ? "" : rs.getString("nombreusrcon1"));
                hmData.put("NOMBREINTER", (rs.getString("nombreinter") == null) ? "" : rs.getString("nombreinter"));
                hmData.put("EXISTEFILECARTERAPDF", existeFileCarteraPdf);
                hmData.put("EXISTE_REVISION", revi);


                lstSolicRec.add(hmData);

            }

            rs.close();
            ps.close();

            return lstSolicRec;
        } catch (Throwable e) {
            throw new AppException("error al generar consulta de solicitudes de recursos", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }


    public List obtieneInfoRegAceptRech(List lstCveSolicitudes) throws AppException {
        try {
            return obtieneRegInfoSolics(lstCveSolicitudes);
        } catch (Throwable t) {
            t.printStackTrace();
            throw new AppException("error al obtener solicitudes que seran atendias(Aceptadas o Rechazadas)", t);
        }
    }

    public void aceptaSolicsParaConfirmacion(List lstSolicitudes, String fecAbono, String observ,
                                             String nombreUsrOP) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String strSQL = "";
        boolean commit = true;

        try {
            con.conexionDB();

            strSQL =
                "UPDATE ope_reg_solicitud " + "   SET ig_numero_prestamo = ?, df_operado = sysdate,  " +
                "	 ic_estatus_ope = ?, cg_observaciones_op = ?, df_abono_recursos = TO_DATE(?, 'dd/mm/yyyy'), " +
                "	 cg_nom_usu_op = ?  " + " WHERE ic_solicitud = ? ";


            if (lstSolicitudes != null && lstSolicitudes.size() > 0) {
                HashMap hmData = new HashMap();
                for (int x = 0; x < lstSolicitudes.size(); x++) {
                    hmData = (HashMap) lstSolicitudes.get(x);
                    ps = con.queryPrecompilado(strSQL);
                    ps.setLong(1, Long.parseLong((String) hmData.get("NUMPRESTAMO")));
                    //ps.setString(2, (String)hmData.get("FECOP"));
                    //ps.setString(3, (String)hmData.get("HORAOP"));
                    ps.setLong(2, Long.parseLong("3")); //aceptada para confirmacion
                    ps.setString(3, observ);
                    ps.setString(4, fecAbono);
                    ps.setString(5, nombreUsrOP);
                    ps.setLong(6, Long.parseLong((String) hmData.get("CVESOLICITUD")));
                    ps.executeUpdate();
                    ps.close();
                }
            }

        } catch (Throwable t) {
            commit = false;
            throw new AppException("Error al atender solicitudes que seran aceptadas para confirmar", t);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public void aceptaSolicsParaConfirmacionConCheck(List lstSolicitudes, String fecAbono, String observ,
                                                     String nombreUsrOP, String iNoUsuario) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String strSQL = "";
        boolean commit = true;

        try {
            con.conexionDB();

            strSQL =
                "UPDATE ope_reg_solicitud " + "   SET ig_numero_prestamo = ?, df_operado = sysdate,  " +
                "	 ic_estatus_ope = ?, cg_observaciones_op = ?, df_abono_recursos = TO_DATE(?, 'dd/mm/yyyy'), " +
                "	 cg_nom_usu_op = ?  " + " WHERE ic_solicitud = ? ";

            String datosCheck = "";
            if (lstSolicitudes != null && lstSolicitudes.size() > 0) {
                HashMap hmData = new HashMap();
                for (int x = 0; x < lstSolicitudes.size(); x++) {

                    hmData = (HashMap) lstSolicitudes.get(x);
                    datosCheck = this.obtenChkSoli("OP", (String) hmData.get("CVESOLICITUD"), iNoUsuario);
                    this.guardaBitacoraCheck((String) hmData.get("CVESOLICITUD"), "3", iNoUsuario, datosCheck, "", con,
                                             "OP");

                    ps = con.queryPrecompilado(strSQL);
                    ps.setLong(1, Long.parseLong((String) hmData.get("NUMPRESTAMO")));
                    ps.setLong(2, Long.parseLong("3")); //aceptada para confirmacion
                    ps.setString(3, observ);
                    ps.setString(4, fecAbono);
                    ps.setString(5, nombreUsrOP);
                    ps.setLong(6, Long.parseLong((String) hmData.get("CVESOLICITUD")));
                    ps.executeUpdate();
                    ps.close();
                }
            }
            System.out.println("corrio hasta aqui ::: " + strSQL);
        } catch (Throwable t) {
            commit = false;
            throw new AppException("Error al atender solicitudes que seran aceptadas para confirmar", t);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public void rechazaSolicsParaConfirmacion(List lstSolicitudes, String fecRechazo, String horaRechazo,
                                              String causasRech, String nombreUsrOP) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String strSQL = "";
        boolean commit = true;

        try {
            con.conexionDB();

            strSQL =
                "UPDATE ope_reg_solicitud " + "	 SET ic_estatus_ope = ?, cg_causas_rechazo = ?, no_folio = ?, " +
                "	 df_rechazo = sysdate, " + "	 cg_nom_usu_op = ?  " + " WHERE ic_solicitud = ? ";


            if (lstSolicitudes != null && lstSolicitudes.size() > 0) {
                HashMap hmData = new HashMap();
                for (int x = 0; x < lstSolicitudes.size(); x++) {
                    hmData = (HashMap) lstSolicitudes.get(x);
                    ps = con.queryPrecompilado(strSQL);
                    ps.setLong(1, Long.parseLong("4")); //rechazada para confirmacion
                    ps.setString(2, causasRech);
                    ps.setLong(3, Long.parseLong((String) hmData.get("FOLIO")));
                    ps.setString(4, nombreUsrOP);
                    ps.setLong(5, Long.parseLong((String) hmData.get("CVESOLICITUD")));
                    ps.executeUpdate();
                    ps.close();
                }
            }

        } catch (Throwable t) {
            commit = false;
            throw new AppException("Error al atender solicitudes que seran rechazadas para confirmar", t);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public void rechazaSolicsParaConfirmacionConCheck(List lstSolicitudes, String fecRechazo, String horaRechazo,
                                                      String causasRech, String nombreUsrOP,
                                                      String iNoUsuario) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String strSQL = "";
        boolean commit = true;

        try {
            con.conexionDB();

            strSQL =
                "UPDATE ope_reg_solicitud " + "	 SET ic_estatus_ope = ?, cg_causas_rechazo = ?, no_folio = ?, " +
                "	 df_rechazo = sysdate, " + "	 cg_nom_usu_op = ?  " + " WHERE ic_solicitud = ? ";
            String datosCheck = "";

            if (lstSolicitudes != null && lstSolicitudes.size() > 0) {
                HashMap hmData = new HashMap();
                for (int x = 0; x < lstSolicitudes.size(); x++) {

                    hmData = (HashMap) lstSolicitudes.get(x);
                    datosCheck = this.obtenChkSoli("OP", (String) hmData.get("CVESOLICITUD"), iNoUsuario);
                    this.guardaBitacoraCheck((String) hmData.get("CVESOLICITUD"), "4", iNoUsuario, datosCheck, "", con,
                                             "OP");
                    ps = con.queryPrecompilado(strSQL);
                    ps.setLong(1, Long.parseLong("4"));
                    ps.setString(2, causasRech);
                    ps.setLong(3, Long.parseLong((String) hmData.get("FOLIO")));
                    ps.setString(4, nombreUsrOP);
                    ps.setLong(5, Long.parseLong((String) hmData.get("CVESOLICITUD")));
                    ps.executeUpdate();
                    ps.close();
                }
            }

        } catch (Throwable t) {
            commit = false;
            throw new AppException("Error al atender solicitudes que seran rechazadas para confirmar", t);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    //*************************** CASO DE USO: CU02-CONCENTRADOR DE SOLICITUDES **************************************

    public List consultaSolicRec(String cveIf, String cveEstatus, String numSolic, String fecRecpIni,
                                 String fecRecpFin) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        List lstSolicRec = new ArrayList();
        String qrySql = "";
        try {
            con.conexionDB();

            qrySql =
                "SELECT ors.ic_solicitud cvesolicitud, ors.ic_if cveif, ors.ic_contrato cvecontrato,  " +
                "       TO_CHAR (ors.df_solicitud, 'dd/mm/yyyy HH24:mi:ss') fecsolicitud,  " +
                "       oc.cg_nombre_contrato nombrecontrato,  " +
                "	   TO_CHAR (odi.df_firma_contrato, 'dd/mm/yyyy') fecfirmacontrato,  " +
                "       odi.cg_contrato_oe nombrecontratooe,  " +
                "       TO_CHAR (odi.df_firma_contrato_oe, 'dd/mm/yyyy') fecfirmacontratooe,  " +
                "       ors.fn_monto_solicitud importesolic, cm.cd_nombre nombremoneda,  " +
                "       ors.cg_des_recursos destinorecurso,  " +
                "	   TO_CHAR (ors.df_pago_capital, 'dd/mm/yyyy') fecpagocapital,  " +
                "       TO_CHAR (ors.df_pago_interes, 'dd/mm/yyyy') fecpagointeres,   " +
                "       TO_CHAR (ors.df_vencimiento, 'dd/mm/yyyy')  fecvencimiento,  " +
                "       ors.ic_estatus_ope estatussolic, oe.cd_descripcion nombreestatus,  " +
                "       ors.cg_observaciones_if observif, ors.cg_observaciones_op observop,    " +
                "       ors.cg_tipo_tasa tipotasa, " + "       ors.ic_tasa cvetasa, " +
                "       ct.cd_nombre nombretasa, " + "       ors.in_tasa_aceptada tasainteres,  " +
                "       ors.ic_moneda cvemoneda,  " + "       ors.ig_numero_prestamo numprestamo,  " +
                "       TO_CHAR(ors.df_operado,'dd/mm/yyyy') fecop, " +
                "       TO_CHAR(ors.df_rechazo,'dd/mm/yyyy HH24:mi:ss') fecrechazo,  " +
                "       odi.ic_num_cuenta numcuenta,  " + "       odi.cg_banco nombrebanco,  " +
                "       ors.no_folio folio,  " + "       ors.cg_usuario_if usuarioif,  " +
                "       ors.cg_causas_rechazo causasRech,  " + "       odi.cg_cont_modificatorios countmodif,  " +
                "       nvl(dbms_lob.getlength(ors.bi_doc_cotizacion),0) sizefilecotiza, " +
                "       nvl(dbms_lob.getlength(ors.bi_doc_cartera_pdf),0) sizefileCarterapdf, " +
                "       ci.cg_razon_social nombreinter,  " + "       ors.cc_acuse ccacuse, " +
                "		  odi.cg_ejec_seg usuariosegui, " + "		  odi.cg_ejec_promo usuariopromo, " +
                "       ors.cg_nom_usu_op nombreusrop,  " + "       odi.IC_CTA_CLABE numClabe  " +
                "  FROM ope_reg_solicitud ors,  " + "       ope_datos_if odi,  " + "       comcat_moneda cm,  " +
                "       opecat_estatus oe,  " + "	   opecat_contrato oc, " + "       comcat_if ci, " +
                "       comcat_tasa ct " + " WHERE ors.ic_if = odi.ic_if " + "   AND odi.ic_if = ci.ic_if " +
                "   AND ors.ic_tasa = ct.ic_tasa(+)  " + "   AND ors.ic_moneda = cm.ic_moneda  " +
                "   AND ors.ic_estatus_ope = oe.ic_estatus_ope  " + "	 AND ors.ic_contrato = oc.ic_contrato  " +
                "   AND ors.ic_estatus_ope in (1,3,4)  ";


            if (cveIf != null && !"".equals(cveIf)) {
                qrySql += "   AND ors.ic_if = ?  ";
            }
            if (cveEstatus != null && !"".equals(cveEstatus)) {
                qrySql += "   AND ors.ic_estatus_ope = ? ";
            }
            if (numSolic != null && !"".equals(numSolic)) {
                //qrySql += "   AND ors.ic_solicitud = ? ";
                qrySql += "   AND ors.cc_acuse = ? ";
            }
            if (fecRecpIni != null && !"".equals(fecRecpIni)) {
                qrySql += "   AND ors.df_solicitud >= TO_DATE (?, 'dd/mm/yyyy') ";
            }
            if (fecRecpFin != null && !"".equals(fecRecpFin)) {
                qrySql += "   AND ors.df_solicitud < (TO_DATE (?, 'dd/mm/yyyy') + 1) ";
            }

            qrySql += "  order by ci.cg_razon_social, ors.df_solicitud ";

            System.out.println("qrySql  ---" + qrySql);


            int p = 0;
            ps = con.queryPrecompilado(qrySql);
            if (cveIf != null && !"".equals(cveIf))
                ps.setLong(++p, Long.parseLong(cveIf));
            if (cveEstatus != null && !"".equals(cveEstatus))
                ps.setLong(++p, Long.parseLong(cveEstatus));
            //if(numSolic!=null && !"".equals(numSolic)) ps.setLong(++p, Long.parseLong(numSolic));
            if (numSolic != null && !"".equals(numSolic))
                ps.setString(++p, numSolic);
            if (fecRecpIni != null && !"".equals(fecRecpIni))
                ps.setString(++p, fecRecpIni);
            if (fecRecpFin != null && !"".equals(fecRecpFin))
                ps.setString(++p, fecRecpFin);
            System.out.println("fecRecpIni  ---" + fecRecpIni);
            System.out.println("fecRecpFin  ---" + fecRecpFin);
            System.out.println("cveIf  ---" + cveIf);
            rs = ps.executeQuery();

            qrySql =
                "SELECT ofm.cg_descripcion descmod, " +
                "       TO_CHAR (ofm.df_firma_modificatoria, 'dd/mm/yyyy') fecmod " + "  FROM ope_det_fir_modifi ofm " +
                " WHERE ofm.ic_if = ? " + " ORDER BY ofm.ic_firma_modifi ";


            HashMap hmData;
            String allFecModCont = "";
            String fecModCont = "";
            String existeFileCotiza = "N";
            String existeFileCarteraPdf = "N";
            String revi = "";
            while (rs.next()) {
                allFecModCont = "";
                ps2 = con.queryPrecompilado(qrySql);
                ps2.setLong(1, Long.parseLong(rs.getString("cveif")));
                rs2 = ps2.executeQuery();
                while (rs2.next()) {
                    fecModCont = rs2.getString("descmod") + ": " + rs2.getString("fecmod");
                    allFecModCont += ("".equals(allFecModCont)) ? fecModCont : (", " + fecModCont);
                }
                rs2.close();
                ps2.close();

                existeFileCotiza = (rs.getDouble("sizefilecotiza") > 0) ? "S" : "N";
                existeFileCarteraPdf = (rs.getDouble("sizefileCarterapdf") > 0) ? "S" : "N";
                revi = this.getExisteRevision(rs.getString("cvesolicitud"));
                hmData = new HashMap();

                hmData.put("CVESOLICITUD", rs.getString("cvesolicitud"));
                hmData.put("CCACUSE", rs.getString("ccacuse"));
                hmData.put("CVEIF", rs.getString("cveif"));
                hmData.put("NOMBREINTER", rs.getString("nombreinter"));
                hmData.put("NOMBREUSROP", rs.getString("nombreusrop"));
                hmData.put("CVECONTRATO", rs.getString("cvecontrato"));
                hmData.put("FECSOLICITUD", rs.getString("fecsolicitud"));

                hmData.put("NOMBRECONTRATO", rs.getString("nombrecontrato"));
                hmData.put("FECFIRMACONTRATO", rs.getString("fecfirmacontrato"));
                hmData.put("NOMBRECONTRATOOE", rs.getString("nombrecontratooe"));
                hmData.put("FECFIRMACONTRATOOE", rs.getString("fecfirmacontratooe"));
                hmData.put("IMPORTESOLIC", rs.getString("importesolic"));

                hmData.put("CVEMONEDA", rs.getString("cvemoneda"));
                hmData.put("NOMBREMONEDA", rs.getString("nombremoneda"));
                hmData.put("DESTINORECURSO", rs.getString("destinorecurso"));
                hmData.put("FECPAGOCAPITAL", rs.getString("fecpagocapital"));
                hmData.put("FECPAGOINTERES", rs.getString("fecpagointeres"));

                hmData.put("FECVENCIMIENTO", rs.getString("fecvencimiento"));
                hmData.put("ESTATUSSOLIC", rs.getString("estatussolic"));
                hmData.put("NOMBREESTATUS", rs.getString("nombreestatus"));
                hmData.put("OBSERVIF", rs.getString("observif"));
                hmData.put("OBSERVOP", rs.getString("observop"));
                hmData.put("TASAINTERES", rs.getString("tasainteres"));

                hmData.put("TIPOTASA", rs.getString("tipotasa"));
                hmData.put("CVETASA", rs.getString("cvetasa"));
                hmData.put("NOMBRETASA", rs.getString("nombretasa"));

                hmData.put("NUMPRESTAMO", rs.getString("numprestamo"));
                hmData.put("FECOP", rs.getString("fecop"));
                hmData.put("FECRECHAZO", rs.getString("fecrechazo"));
                hmData.put("NUMCUENTA", rs.getString("numcuenta"));
                hmData.put("NOMBREBANCO", rs.getString("nombrebanco"));
                hmData.put("FOLIO", rs.getString("folio"));
                hmData.put("USUARIOIF", rs.getString("usuarioIF"));
                hmData.put("CAUSASRECH", rs.getString("causasRech"));
                hmData.put("COUNTMODIF", rs.getString("countmodif"));
                hmData.put("ALLFECMODCONT", allFecModCont);
                hmData.put("EXISTEFILECOTIZA", existeFileCotiza);
                hmData.put("EXISTEFILECARTERAPDF", existeFileCarteraPdf);

                hmData.put("USUARIOPROMO", rs.getString("usuariopromo"));
                hmData.put("USUARIOSEGUI", rs.getString("usuariosegui"));
                hmData.put("NUMCLABE", (rs.getString("numClabe") == null) ? "" : rs.getString("numClabe"));

                hmData.put("EXISTE_REVISION", revi);


                lstSolicRec.add(hmData);


            }

            rs.close();
            ps.close();


            return lstSolicRec;
        } catch (Throwable e) {
            throw new AppException("error al generar consulta de solicitudes de recursos", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public String obtieneArchivoSolic(String rutaDestino, String cveSolicitud, String tipoArchivo) throws AppException {
        StringBuffer strSQL = new StringBuffer();
        PreparedStatement ps = null;
        ResultSet rs = null;
        AccesoDB con = new AccesoDB();
        String nombreArchivoTmp = null;
        String campoArch = "";
        String extension = "";


        try {
            con.conexionDB();

            if ("COTIZA".equals(tipoArchivo)) {
                campoArch = "bi_doc_cotizacion";
                extension = ".pdf";
            } else if ("AMORTIZACION".equals(tipoArchivo)) {
                nombreArchivoTmp = obtieneArchivoAmort(rutaDestino, cveSolicitud);
                extension = "";
            } else if ("CARTERA".equals(tipoArchivo)) {
                campoArch = "bi_doc_cartera";
                extension = ".csv";
            } else if ("CARTERA_PDF".equals(tipoArchivo)) {
                campoArch = "bi_doc_cartera_pdf";
                extension = ".pdf";
            } else if ("RUG".equals(tipoArchivo)) {
                campoArch = "bi_doc_boletarug";
                extension = ".pdf";
            } else if ("PRENDA".equals(tipoArchivo)) {
                campoArch = "bi_doc_prenda";
                extension = ".prn";
            } else if ("ACUSE".equals(tipoArchivo)) {
                campoArch = "bi_doc_acuse_solic";
                extension = ".pdf";
            }

            if (!"AMORTIZACION".equals(tipoArchivo)) {

                strSQL.append("SELECT " + campoArch + " " + "  FROM ope_reg_solicitud " + " WHERE ic_solicitud = ? ");


                ps = con.queryPrecompilado(strSQL.toString());
                ps.setInt(1, Integer.parseInt(cveSolicitud));
                rs = ps.executeQuery();
                String nombreArchivo = "";
                String ruta = "";

                while (rs.next()) {
                    InputStream inStream = rs.getBinaryStream(campoArch);
                    nombreArchivo = campoArch;
                    ruta = rutaDestino + nombreArchivo + extension;
                    CreaArchivo creaArchivo = new CreaArchivo();
                    creaArchivo.setNombre(nombreArchivo);
                    if (!creaArchivo.make(inStream, ruta)) {
                        throw new AppException("Error al generar el archivo en " + nombreArchivo);
                    }
                    nombreArchivoTmp = creaArchivo.getNombre();
                    inStream.close();

                }
                rs.close();
                ps.close();
            }

            return nombreArchivoTmp + extension;

        } catch (Exception e) {
            throw new AppException("Error al obtener archivo de la solicitud ", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public List obtieneInfoSolicCompleta(String cveSolicitud) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lstInfoSolicComp = new ArrayList();
        List lstAmort = new ArrayList();
        String strSQL = "";
        try {
            con.conexionDB();

            String nombreUserIF = "";
            String nombreUserOP = "";
            strSQL =
                "SELECT ors.ic_solicitud cvesolicitud,   " +
                "       TO_CHAR (ors.df_solicitud, 'dd/mm/yyyy') fecsolicitud, " +
                "       ci.cg_razon_social nombrebanco, " + "       odi.cg_banco banco, " +
                "       odi.ic_cta_clabe cuentaclabe, " + "       oc.cg_nombre_contrato nombrecontrato, " +
                "       TO_CHAR(odi.df_firma_contrato,'dd/mm/yyyy') fecfirmacontrato, " +
                "       odi.cg_contrato_oe nombrecontratooe, " +
                "       TO_CHAR(odi.df_firma_contrato_oe,'dd/mm/yyyy') fecfirmacontratooe, " +
                "       ors.fn_monto_solicitud importesolic, " + "       cm.cd_nombre nombremoneda, " +
                "       ors.ic_if cveif,  " + "       ors.ic_contrato cvecontrato, " +
                "       ors.ig_numero_prestamo numprestamo, " + "       ors.cg_des_recursos destinorecurso, " +
                "       TO_CHAR(ors.df_pago_capital,'dd/mm/yyyy') fecpagocap, " +
                "       TO_CHAR(ors.df_pago_interes,'dd/mm/yyyy') fecpagoint, " +
                "       TO_CHAR(ors.df_vencimiento,'dd/mm/yyyy') fecvencimiento, " +
                //"		  ors.cg_tipo_tasa || ' ' || ors.cg_tasa_interes || ' % ' AS tasainteres,  " +
                "       ors.cg_tipo_tasa tipotasa, " + "       ors.ic_tasa cvetasa, " +
                "       ct.cd_nombre nombretasa, " + "       ors.in_tasa_aceptada tasainteres,  " +
                "       odi.ic_num_cuenta numcuenta, " + "       ors.no_folio folio, " +
                "       ors.ic_moneda cvemoneda, " + "       ors.cg_observaciones_if observif,  " +
                "       ors.cg_observaciones_op observop, " + "       ors.cg_causas_rechazo causasrech, " +
                "       ors.cg_usuario_op usuarioop, " + "       ors.cg_usuario_if usuarioif " +
                "  FROM ope_reg_solicitud ors,  " + "       ope_datos_if odi,  " + "       comcat_moneda cm,  " +
                "       opecat_estatus oe,  " + "       opecat_contrato oc, " + "       comcat_if ci, " +
                "       comcat_tasa ct " + " WHERE ors.ic_if = odi.ic_if  " + "   AND ors.ic_moneda = cm.ic_moneda  " +
                "   AND ors.ic_tasa = ct.ic_tasa  " + "   AND ors.ic_estatus_ope = oe.ic_estatus_ope  " +
                "   AND ors.ic_contrato = oc.ic_contrato " + "   AND odi.ic_if = ci.ic_if  " +
                "   AND ors.ic_solicitud = ? ";

            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(cveSolicitud));
            rs = ps.executeQuery();

            if (rs.next()) {
                HashMap hmData = new HashMap();
                String userIF = rs.getString("usuarioif") == null ? "" : rs.getString("usuarioif");
                String userOP = rs.getString("usuarioop") == null ? "" : rs.getString("usuarioop");

                if (userIF != null && !"".equals(userIF)) {
                    nombreUserIF = (String) (obtieneInfoCorreo(userIF)).get("USUARIO_NOMBRE");
                }
                if (userOP != null && !"".equals(userOP)) {
                    nombreUserOP = (String) (obtieneInfoCorreo(userOP)).get("USUARIO_NOMBRE");
                }

                hmData.put("CVESOLICITUD", rs.getString("cvesolicitud") == null ? "" : rs.getString("cvesolicitud"));
                hmData.put("FECSOLICITUD", rs.getString("fecsolicitud") == null ? "" : rs.getString("fecsolicitud"));
                hmData.put("NOMBREBANCO", rs.getString("nombrebanco") == null ? "" : rs.getString("nombrebanco"));
                hmData.put("BANCO", rs.getString("banco") == null ? "" : rs.getString("banco"));
                hmData.put("CUENTACLABE", rs.getString("cuentaclabe") == null ? "" : rs.getString("cuentaclabe"));
                hmData.put("NOMBRECONTRATO",
                           rs.getString("nombrecontrato") == null ? "" : rs.getString("nombrecontrato"));
                hmData.put("FECFIRMACONTRATO",
                           rs.getString("fecfirmacontrato") == null ? "" : rs.getString("fecfirmacontrato"));
                hmData.put("NOMBRECONTRATOOE",
                           rs.getString("nombrecontratooe") == null ? "" : rs.getString("nombrecontratooe"));
                hmData.put("FECFIRMACONTRATOOE",
                           rs.getString("fecfirmacontratooe") == null ? "" : rs.getString("fecfirmacontratooe"));
                hmData.put("IMPORTESOLIC", rs.getString("importesolic") == null ? "" : rs.getString("importesolic"));
                hmData.put("NOMBREMONEDA", rs.getString("nombremoneda") == null ? "" : rs.getString("nombremoneda"));
                hmData.put("CVEIF", rs.getString("CVEIF") == null ? "" : rs.getString("CVEIF"));
                hmData.put("CVECONTRATO", rs.getString("cvecontrato") == null ? "" : rs.getString("cvecontrato"));
                hmData.put("NUMPRESTAMO", rs.getString("numprestamo") == null ? "" : rs.getString("numprestamo"));
                hmData.put("DESTINORECURSO",
                           rs.getString("destinorecurso") == null ? "" : rs.getString("destinorecurso"));
                hmData.put("FECPAGOCAP", rs.getString("fecpagocap") == null ? "" : rs.getString("fecpagocap"));
                hmData.put("FECPAGOINT", rs.getString("fecpagoint") == null ? "" : rs.getString("fecpagoint"));
                hmData.put("FECVENCIMIENTO",
                           rs.getString("fecvencimiento") == null ? "" : rs.getString("fecvencimiento"));
                hmData.put("TASAINTERES", rs.getString("tasainteres") == null ? "" : rs.getString("tasainteres"));
                hmData.put("TIPOTASA", rs.getString("tipotasa"));
                hmData.put("CVETASA", rs.getString("cvetasa"));
                hmData.put("NOMBRETASA", rs.getString("nombretasa"));
                hmData.put("NUMCUENTA", rs.getString("numcuenta") == null ? "" : rs.getString("numcuenta"));
                hmData.put("FOLIO", rs.getString("folio") == null ? "" : rs.getString("folio"));
                hmData.put("CVEMONEDA", rs.getString("cvemoneda") == null ? "" : rs.getString("cvemoneda"));
                hmData.put("OBSERVIF", rs.getString("observif") == null ? "" : rs.getString("observif"));
                hmData.put("OBSERVOP", rs.getString("observop") == null ? "" : rs.getString("observop"));
                hmData.put("CAUSASRECH", rs.getString("causasrech") == null ? "" : rs.getString("causasrech"));
                hmData.put("USUARIOOP", rs.getString("usuarioop") == null ? "" : rs.getString("usuarioop"));
                hmData.put("USUARIOIF", rs.getString("usuarioif") == null ? "" : rs.getString("usuarioif"));
                hmData.put("NOMBREUSERIF", nombreUserIF);
                hmData.put("NOMBREUSEROP", nombreUserOP);

                lstInfoSolicComp.add(hmData);
            }
            rs.close();
            ps.close();

            strSQL =
                "SELECT ota.ic_solicitud cvesolicitud,  " + "       ota.ic_tabla_amort cvetablaamort,  " +
                "       ota.in_num_periodo numperiodo,  " + "       TO_CHAR(ota.df_alta,'dd/mm/yyyy') fecalta, " +
                "       ota.fn_monto_amort montoamort" + "  FROM ope_reg_solicitud ors, ope_tabla_amort ota " +
                " WHERE ors.ic_solicitud = ota.ic_solicitud " + "   AND ors.ic_solicitud = ? " +
                " ORDER BY ota.ic_tabla_amort ";

            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(cveSolicitud));
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap hmData = new HashMap();
                hmData.put("CVESOLICITUD", rs.getString("cvesolicitud") == null ? "" : rs.getString("cvesolicitud"));
                hmData.put("CVETABLAAMORT", rs.getString("cvetablaamort") == null ? "" : rs.getString("cvetablaamort"));
                hmData.put("NUMPERIODO", rs.getString("numperiodo") == null ? "" : rs.getString("numperiodo"));
                hmData.put("FECALTA", rs.getString("fecalta") == null ? "" : rs.getString("fecalta"));
                hmData.put("MONTOAMORT", rs.getString("montoamort") == null ? "" : rs.getString("montoamort"));
                lstAmort.add(hmData);

            }
            rs.close();
            ps.close();
            lstInfoSolicComp.add(lstAmort);

            return lstInfoSolicComp;
        } catch (Throwable t) {
            throw new AppException("Error al obtener la info de la solicitud completa", t);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public void asignaEjecutivoOP(String nombreIf, String cveEjecutivo, List lstSolicitudes, List lstEstatus,
                                  String userConc, String nombreConc) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String strSQL = "";
        boolean commit = true;

        try {
            con.conexionDB();

            strSQL =
                "UPDATE ope_reg_solicitud " + "   SET ic_ejecutivo_op = ?, " + "		  df_asig_ejeop = sysdate, " +
                "		  cg_usuario_con1 = ?, " + "		  cg_nom_usuario_con1 = ?, " + "       ic_estatus_ope = ? " +
                " WHERE ic_solicitud = ? ";

            String datosCheck = "";

            if (lstSolicitudes != null && lstSolicitudes.size() > 0) {
                for (int x = 0; x < lstSolicitudes.size(); x++) {
                    if (lstEstatus != null && lstEstatus.size() > 0) {
                        String estatus = (String) lstEstatus.get(x);
                        if (!estatus.equals("1")) {
                            datosCheck = this.obtenChkSoli("CON", (String) lstSolicitudes.get(x), userConc);
                            this.guardaBitacoraCheck((String) lstSolicitudes.get(x), "2", cveEjecutivo, datosCheck,
                                                     userConc, con, "CON");
                        }
                    }

                    ps = con.queryPrecompilado(strSQL);
                    ps.setString(1, cveEjecutivo);
                    ps.setString(2, userConc);
                    ps.setString(3, nombreConc);
                    ps.setLong(4, Long.parseLong("2"));
                    ps.setLong(5, Long.parseLong((String) lstSolicitudes.get(x)));
                    ps.executeUpdate();
                    ps.close();
                }
            }

            List lstInfoSolics = obtieneRegInfoSolics(lstSolicitudes);
            enviaCorreoSolicRec("ASIGNADAS", nombreIf, cveEjecutivo, "", lstInfoSolics);

        } catch (Throwable t) {
            commit = false;
            t.printStackTrace();
            throw new AppException("Error al asignar el ejecutivo OP", t);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public void cambiaEstatusSolic(String cveEstatusGral, String observaciones, String nombreIf, List lstSolicitudes,
                                   String iNoUsuario, String strNombreUsuario) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String strSQL = "";
        String tipoCorreo = "";
        String parametro = "";
        boolean commit = true;

        try {
            con.conexionDB();
            String cveEstatusNew = "3".equals(cveEstatusGral) ? "5" : "6";
            tipoCorreo = "3".equals(cveEstatusGral) ? "ACEPTADAS" : "RECHAZADAS";
            //parametro = "3".equals(cveEstatusGral)?"cg_observaciones_op":"cg_causas_rechazo";

            if ("3".equals(cveEstatusGral)) {
                parametro = " cg_observaciones_op = ?, ";
                parametro += " df_operado_con = sysdate, ";
            }
            if ("4".equals(cveEstatusGral)) {
                parametro = " cg_causas_rechazo = ?, ";
                parametro += " df_rechazado_con = sysdate, ";
            }

            strSQL =
                "UPDATE ope_reg_solicitud " + "   SET ic_estatus_ope = ?, " + parametro + " cg_usuario_con2 = ?, " +
                " cg_nom_usuario_con2 = ? " + " WHERE ic_solicitud = ? ";
            String datosCheck = "";

            if (lstSolicitudes != null && lstSolicitudes.size() > 0) {
                for (int x = 0; x < lstSolicitudes.size(); x++) {
                    datosCheck = this.obtenChkSoli("CON", (String) lstSolicitudes.get(x), iNoUsuario);
                    this.guardaBitacoraCheck((String) lstSolicitudes.get(x), cveEstatusNew, iNoUsuario, datosCheck, "",
                                             con, "CON");
                    ps = con.queryPrecompilado(strSQL);
                    ps.setString(1, cveEstatusNew);
                    ps.setString(2, observaciones);
                    ps.setString(3, iNoUsuario);
                    ps.setString(4, strNombreUsuario);
                    ps.setLong(5, Long.parseLong((String) lstSolicitudes.get(x)));
                    ps.executeUpdate();
                    ps.close();
                }
            }

            List lstInfoSolics = obtieneRegInfoSolics(lstSolicitudes);
            enviaCorreoSolicRec(tipoCorreo, nombreIf, iNoUsuario, observaciones, lstInfoSolics);

        } catch (Throwable t) {
            commit = false;
            throw new AppException("Error al cambiar el estatus de las Solicitudes", t);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    private void enviaCorreoSolicRec(String tipoCorreo, String nombreIf, String usuarioEO, String observaciones,
                                     List lstSolicitudes) throws AppException {
        Correo correo = new Correo();
        int contador = 0;
        BigDecimal bdImporte = new BigDecimal("0.0");
        String aQuien = "";
        String userIf = "";
        String CVEIF = "";
        String nombreUserIf = "";
        String nombreMoneda = "";
        String nombreEO = "";
        String textoCorreo = "";
        String toCorreo = "";
        String ccCorreo = "";
        String asunto = "";
        String txtObs = "";
        String textoA = "";
        String textoB = "";
        String textoC = "";
        String tabla1 = "";
        String operador = "";

        String auto1 = "";
        String auto2 = "";

        try {

            String perfilUsrIf = "";
            String usuarioSegui = "";
            String usuarioPromo = "";

            HashMap hmInfoUsuario = new HashMap();
            HashMap hmInfoOperador = new HashMap();
            HashMap hmInfoAut1 = new HashMap();
            HashMap hmInfoAut2 = new HashMap();
            HashMap hmInfoGerSub = obtieneInfoGerenteSubdirector();
            HashMap hmInfoGerConc = this.obtieneInfoConcentrador(usuarioEO);
            String puesto = "";

            String styleEncabezados =
                " style='font-family: Verdana, Arial, Helvetica, sans-serif; " + "font-size: 10px; " +
                "font-weight: bold;" + "color: #FFFFFF;" + "background-color: #4d6188;" + "padding-top: 3px;" +
                "padding-right: 1px;" + "padding-bottom: 1px;" + "padding-left: 3px;" + "height: 25px;" +
                "border: 1px solid #1a3c54;'";

            String style =
                "style='font-family:Verdana, Arial, Helvetica, sans-serif;" + "color:#000066; " + "padding-top:1px; " +
                "padding-right:2px; " + "padding-bottom:1px; " + "padding-left:2px; " + "height:22px; " +
                "font-size:11px;'";


            if (lstSolicitudes != null && lstSolicitudes.size() > 0) {
                tabla1 = "<table border=\"1\">";
                for (int x = 0; x < lstSolicitudes.size(); x++) {
                    HashMap hm = (HashMap) lstSolicitudes.get(x);
                    if (x == 0) {
                        nombreMoneda = (String) hm.get("NOMBREMONEDA");
                        usuarioPromo = (String) hm.get("USUARIOPROMO");
                        usuarioSegui = (String) hm.get("USUARIOSEGUI");

                        if ("ASIGNADAS".equals(tipoCorreo)) {

                            tabla1 +=
                                "	<tr>" + "		<td align=\"center\" " + styleEncabezados + ">No. Solicitud</td>" +
                                "		<td align=\"center\" " + styleEncabezados + ">Fecha de Solicitud</td>" +
                                "		<td align=\"center\" " + styleEncabezados + ">Importe</td>" +
                                "		<td align=\"center\" " + styleEncabezados + ">Moneda</td>" +
                                "	</tr>";
                        } else if ("ACEPTADAS".equals(tipoCorreo)) {
                            userIf = (String) hm.get("USUARIOIF");
                            CVEIF = (String) hm.get("CVEIF");

                            tabla1 +=
                                "	<tr>" + "		<td align=\"center\" " + styleEncabezados + ">Pr&eacute;stamo</td>" +
                                "		<td align=\"center\" " + styleEncabezados + ">Importe</td>" +
                                "		<td align=\"center\" " + styleEncabezados + ">Moneda</td>" +
                                "		<td align=\"center\" " + styleEncabezados + ">Fecha Operado</td>" +
                                //"		<td align=\"center\" "+styleEncabezados+">Hora Operado</td>" +
                                "		<td align=\"center\" " + styleEncabezados + ">N&uacute;mero de Cuenta</td>" +
                                "		<td align=\"center\" " + styleEncabezados + ">Cuenta CLABE</td>" +
                                "		<td align=\"center\" " + styleEncabezados + ">Nombre Banco</td>" +
                                "	</tr>";


                        } else if ("RECHAZADAS".equals(tipoCorreo)) {
                            userIf = (String) hm.get("USUARIOIF");
                            CVEIF = (String) hm.get("CVEIF");
                            tabla1 +=
                                "	<tr>" + "		<td align=\"center\" " + styleEncabezados +
                                ">N&uacute;mero de Solicitud</td>" + "		<td align=\"center\" " + styleEncabezados +
                                ">No. Folio</td>" + "		<td align=\"center\" " + styleEncabezados + ">Importe</td>" +
                                "	</tr>";
                        }
                    }

                    if ("ASIGNADAS".equals(tipoCorreo)) {
                        tabla1 +=
                            "	<tr> " + "	<td align=\"center\" " + style + "> " + (String) hm.get("CCACUSE") +
                            "</td>" + "	<td align=\"right\" " + style + "> " + (String) hm.get("FECSOLICITUD") +
                            "</td>" + "	<td align=\"right\" " + style + "> $" +
                            Comunes.formatoDecimal((String) hm.get("IMPORTESOLIC"), 2) + "</td>" +
                            "	<td align=\"center\" " + style + "> " + (String) hm.get("NOMBREMONEDA") + "</td>" +
                            " </tr> ";
                    } else if ("ACEPTADAS".equals(tipoCorreo)) {
                        tabla1 +=
                            "	<tr> " + "	<td align=\"center\" " + style + "> " + (String) hm.get("NUMPRESTAMO") +
                            "</td>" + "	<td align=\"right\" " + style + "> $" +
                            Comunes.formatoDecimal((String) hm.get("IMPORTESOLIC"), 2) + "</td>" +
                            "	<td align=\"center\" " + style + "> " + (String) hm.get("NOMBREMONEDA") + "</td>" +
                            "	<td align=\"right\" " + style + "> " + (String) hm.get("FECOP") + "</td>" +
                            //"	<td align=\"right\" "+style+"> " + (String)hm.get("HORAOP") +"</td>" +
                            "	<td align=\"right\" " + style + "> " + (String) hm.get("NUMCUENTA") + "</td>" +
                            "	<td align=\"right\" " + style + "> " + (String) hm.get("NUMCLABE") + "</td>" +
                            "	<td align=\"center\" " + style + "> " + (String) hm.get("DESCBANCO") + "</td>" +
                            " </tr> ";
                    } else if ("RECHAZADAS".equals(tipoCorreo)) {
                        tabla1 +=
                            "	<tr> " + "	<td align=\"center\" " + style + "> " + (String) hm.get("CCACUSE") +
                            "</td>" + "	<td align=\"center\" " + style + "> " + (String) hm.get("FOLIO") + "</td>" +
                            "	<td align=\"right\" " + style + "> $" +
                            Comunes.formatoDecimal((String) hm.get("IMPORTESOLIC"), 2) + "</td>" + " </tr> ";
                    }
                    bdImporte = bdImporte.add(new BigDecimal((String) hm.get("IMPORTESOLIC")));
                    contador++;
                }
                tabla1 += "</table><br><br>";

                if ("ASIGNADAS".equals(tipoCorreo) || "RECHAZADAS".equals(tipoCorreo)) {
                    tabla1 +=
                        "<table border=\"1\">" + "	<tr>" + "		<td align=\"center\" " + styleEncabezados +
                        ">N&uacute;mero de Solicitudes</td>" + "		<td align=\"center\" " + styleEncabezados +
                        ">Importe Total</td>" + "	</tr>" + "	<tr>" + "		<td align=\"center\" " + style + ">" +
                        contador + "</td>" + "		<td align=\"center\" " + style + "> $" +
                        Comunes.formatoDecimal(bdImporte.toPlainString(), 2) + "</td>" + "	</tr>" +
                        "</table><br><br>";
                } else if ("ACEPTADAS".equals(tipoCorreo)) {
                    tabla1 +=
                        "<table border=\"1\">" + "	<tr>" + "		<td align=\"center\" " + styleEncabezados +
                        ">No. de Pr&eacute;stamos</td>" + "		<td align=\"center\" " + styleEncabezados +
                        ">Moneda</td>" + "		<td align=\"center\" " + styleEncabezados + ">Importe Total</td>" +
                        "	</tr>" + "	<tr>" + "		<td align=\"center\" " + style + ">" + contador + "</td>" +
                        "		<td align=\"center\" " + style + ">" + nombreMoneda + "</td>" + "		<td align=\"center\" " +
                        style + "> $" + Comunes.formatoDecimal(bdImporte.toPlainString(), 2) + "</td>" + "	</tr>" +
                        "</table><br><br>";
                }

                if ("ASIGNADAS".equals(tipoCorreo)) {
                    hmInfoUsuario = obtieneInfoCorreo(usuarioEO);
                    nombreEO = (String) hmInfoUsuario.get("USUARIO_NOMBRE");
                    toCorreo = (String) hmInfoUsuario.get("USUARIO_MAIL");

                    asunto = "Atenci\u00F3n de Solicitudes";
                    aQuien = nombreEO;
                    textoA =
                        "<p>Por este conducto, hago de su conocimiento que el d&iacute;a de hoy se le ha (n) asignado la (s) siguiente (s) solicitudes de recursos para su revisi&oacute;n correspondiente (s) de " +
                        nombreIf + "</p><br><br>";
                    textoB =
                        "<p><br>Sin otro particular, reciba un cordial saludo.</p> " + "<p>ATENTAMENTE<br>" +
                        hmInfoGerSub.get("GERENTE_NOMBRE") + "<br>" + "Gerente de Operaciones de Cr&eacute;dito<br>" +
                        hmInfoGerSub.get("GERENTE_MAIL") + "<br>" + hmInfoGerSub.get("GERENTE_TELEFONO") + "</p>";
                } else if ("ACEPTADAS".equals(tipoCorreo)) {
                    String OpeFirmaMan = this.getOperaFirmaMancomunada(CVEIF);
                    if (OpeFirmaMan.equals("S")) {
                        operador = this.getCampoRegDatosIf("CG_OPERADOR", CVEIF);
                        auto1 = this.getCampoRegDatosIf("CG_AUTORIZADOR_UNO", CVEIF);
                        auto2 = this.getCampoRegDatosIf("CG_AUTORIZADOR_DOS", CVEIF);
                        hmInfoOperador = obtieneInfoCorreo(operador);
                        toCorreo = (String) hmInfoOperador.get("USUARIO_MAIL");
                        hmInfoAut1 = obtieneInfoCorreo(auto1);
                        ccCorreo = (String) hmInfoAut1.get("USUARIO_MAIL");
                        if (!auto2.equals("")) {
                            hmInfoAut2 = obtieneInfoCorreo(auto2);
                            ccCorreo += "," + (String) hmInfoAut2.get("USUARIO_MAIL");
                        }
                    } else {
                        hmInfoUsuario = obtieneInfoCorreo(userIf);
                        nombreUserIf = (String) hmInfoUsuario.get("USUARIO_NOMBRE");
                        toCorreo = (String) hmInfoUsuario.get("USUARIO_MAIL");
                        perfilUsrIf = (String) hmInfoUsuario.get("USUARIO_PERFIL");
                        /*
						if(!"".equals(userIf)){
							List usuariosPorPerfil = new ArrayList();
							usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("SUB SEG", "N");
							if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
								HashMap hmData = null;
								for(int i=0;i<usuariosPorPerfil.size();i++){
									hmData = new HashMap();
									String loginUsuario = (String)usuariosPorPerfil.get(i);
									Usuario usuarioNaf = utilUsr.getUsuario(loginUsuario);
									String correos =  usuarioNaf.getEmail();
									ccCorreo += "".equals(ccCorreo)?correos:(","+correos);
								}
							}
							usuariosPorPerfil = new ArrayList();
							System.out.println("perfilUsrIf================================="+perfilUsrIf);
							if( "IF LI".equals(perfilUsrIf) || "IF 4CP".equals(perfilUsrIf) || "IF 5CP".equals(perfilUsrIf)){
								usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("SUB LI CP", "N");
							}

							if( "IF 4MIC".equals(perfilUsrIf) || "IF 5MIC".equals(perfilUsrIf) ){
								usuariosPorPerfil = utilUsr.getUsuariosxAfiliado("SUB MICRO", "N");
							}
							if(usuariosPorPerfil!=null && usuariosPorPerfil.size()>0){
								HashMap hmData = null;
								for(int i=0;i<usuariosPorPerfil.size();i++){
									hmData = new HashMap();
									String loginUsuario = (String)usuariosPorPerfil.get(i);
									Usuario usuarioNaf = utilUsr.getUsuario(loginUsuario);
									String correos =  usuarioNaf.getEmail();
									ccCorreo += "".equals(ccCorreo)?correos:(","+correos);
								}
							}
						}
						hmInfoUsuario = obtieneInfoCorreo(usuarioPromo);
						ccCorreo += "".equals(ccCorreo)?(String)hmInfoUsuario.get("USUARIO_MAIL"):","+(String)hmInfoUsuario.get("USUARIO_MAIL");

						hmInfoUsuario = obtieneInfoCorreo(usuarioSegui);
						ccCorreo += "".equals(ccCorreo)?(String)hmInfoUsuario.get("USUARIO_MAIL"):","+(String)hmInfoUsuario.get("USUARIO_MAIL");
					*/
                    }
                    asunto = "Confirmaci\u00F3n de Abono";

                    aQuien = "<b>" + nombreIf + "<br>" + nombreUserIf + "</b>";
                    textoA =
                        "<p>Por este conducto, hago de su conocimiento que el d&iacute;a de hoy se abon&oacute; a la cuenta de cheques que nos fue proporcionada, la cantidad de $" +
                        Comunes.formatoDecimal(bdImporte.toPlainString(), 2) + " " + nombreMoneda +
                        " derivado de la realizaci&oacute;n de sus operaciones de cr&eacute;dito solicitadas ante esta instituci&oacute;n, de acuerdo con  el siguiente detalle:</p><br><br>";
                    txtObs = "<p>Observaciones Ejecutivo OP:</p>";
                    if (hmInfoGerConc.get("CONCENTRADOR").equals("1")) {
                        puesto = "Gerente de Operaciones de Cr&eacute;dito";
                    }
                    if (hmInfoGerConc.get("CONCENTRADOR").equals("2")) {
                        puesto = "Mesa de Control de Cr&eacute;dito";
                    }
                    textoB =
                        "<p><br>Sin otro particular, reciba un cordial saludo.</p>" + "<p>ATENTAMENTE<br>" +
                        hmInfoGerConc.get("GERENTE_NOMBRE") + "<br>" + puesto + "<br>" +
                        hmInfoGerConc.get("GERENTE_MAIL") + "<br>" + hmInfoGerConc.get("GERENTE_TELEFONO") +
                        "<br><br></p>";
                    textoC =
                        "<p><br>Con fundamento en el art&iacute;culo 142 de la Ley de Instituciones de Cr&eacute;dito, 14 fracci&oacute;n I y 15 de la Ley Federal de Transparencia y Acceso a la informaci&oacute;n P&uacute;blica Gubernamental; as&iacute; como al art&iacute;culo 30 de su Reglamento, el contenido del presente mensaje de correo electr&oacute;nico es de car&aacute;cter Reservado.</p>";

                } else if ("RECHAZADAS".equals(tipoCorreo)) {
                    String OpeFirmaMan = this.getOperaFirmaMancomunada(CVEIF);
                    if (OpeFirmaMan.equals("S")) {
                        operador = this.getCampoRegDatosIf("CG_OPERADOR", CVEIF);
                        auto1 = this.getCampoRegDatosIf("CG_AUTORIZADOR_UNO", CVEIF);
                        auto2 = this.getCampoRegDatosIf("CG_AUTORIZADOR_DOS", CVEIF);
                        hmInfoOperador = obtieneInfoCorreo(operador);
                        toCorreo = (String) hmInfoOperador.get("USUARIO_MAIL");
                        hmInfoAut1 = obtieneInfoCorreo(auto1);
                        ccCorreo = (String) hmInfoAut1.get("USUARIO_MAIL");
                        if (!auto2.equals("")) {
                            hmInfoAut2 = obtieneInfoCorreo(auto2);
                            ccCorreo += "," + (String) hmInfoAut2.get("USUARIO_MAIL");
                        }
                    } else {
                        hmInfoUsuario = obtieneInfoCorreo(userIf);
                        nombreUserIf = (String) hmInfoUsuario.get("USUARIO_NOMBRE");
                        toCorreo = (String) hmInfoUsuario.get("USUARIO_MAIL");
                        perfilUsrIf = (String) hmInfoUsuario.get("USUARIO_PERFIL");
                        //hmInfoUsuario = obtieneInfoCorreo(usuarioPromo);
                        //ccCorreo += "".equals(ccCorreo)?(String)hmInfoUsuario.get("USUARIO_MAIL"):","+(String)hmInfoUsuario.get("USUARIO_MAIL");

                        //hmInfoUsuario = obtieneInfoCorreo(usuarioSegui);
                        //ccCorreo += "".equals(ccCorreo)?(String)hmInfoUsuario.get("USUARIO_MAIL"):","+(String)hmInfoUsuario.get("USUARIO_MAIL");
                    }
                    asunto = "Solicitud de Recursos Rechazada";
                    aQuien = nombreIf + "<br>" + nombreUserIf;
                    textoA =
                        "Por este medio, se hace de su conocimiento, que la operaci&oacute;n de cr&eacute;dito solicitada a Nacional Financiera, " +
                        "S.N.C. por un importe total de $" + Comunes.formatoDecimal(bdImporte.toPlainString(), 2) +
                        " " + nombreMoneda +
                        ", no se concluy&oacute; como tr&aacute;mite de disposici&oacute;n, en virtud de las causas se�aladas a continuaci&oacute;n:<br><br>";
                    txtObs = "<p>Causas de Rechazo:<p>";
                    if (hmInfoGerConc.get("CONCENTRADOR").equals("1")) {
                        puesto = "Gerente de Operaciones de Cr�dito";
                    }
                    if (hmInfoGerConc.get("CONCENTRADOR").equals("2")) {
                        puesto = "Mesa de Control de Cr�dito";
                    }
                    textoB =
                        "<p><br>Derivado de lo anterior y con el prop&oacute;sito de estar en posibilidades de atender su solitud de recursos, le agradecemos a la brevedad posible las correcciones y/o aclaraciones correspondientes.</p><br>" +
                        "<p>Sin otro particular, reciba un cordial saludo.</p>" + "<p>ATENTAMENTE<br>" +
                        hmInfoGerConc.get("GERENTE_NOMBRE") + "<br>" + puesto + "<br>" +
                        hmInfoGerConc.get("GERENTE_MAIL") + "<br>" + hmInfoGerConc.get("GERENTE_TELEFONO") +
                        "<br><br></p>";
                    textoC =
                        "<p><br>Con fundamento en el art&iacute;culo 142 de la Ley de Instituciones de Cr�dito, 14 fracci�n I y 15 de la Ley Federal de Transparencia y Acceso a la informaci&oacute;n P&uacute;blica Gubernamental; as&iacute; como al art&iacute;culo 30 de su Reglamento, el contenido del presente mensaje de correo electr&oacute;nico es de car&aacute;cter Reservado.</p>";
                }

            }
            log.debug("toCorreo :: " + toCorreo);
            log.debug("ccCorreo :: " + ccCorreo);
            log.debug("asunto :: " + asunto);
            textoCorreo =
                "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'><p>" + aQuien +
                "<br><br></p>" + textoA + tabla1 + txtObs + observaciones + textoB + textoC + "</form>";
            correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", toCorreo, ccCorreo, asunto,
                                               textoCorreo.toString(), null, null);

        } catch (Throwable t) {
            throw new AppException("Error al generar y enviar correo", t);
        }
    }

    public HashMap obtieneInfoCorreo(String usuario) throws AppException {
        UtilUsr utilUsr = new UtilUsr();
        HashMap hmData = new HashMap();
        String nombre;
        String mail;
        String perfil;
        try {
            Usuario usuarioObj = utilUsr.getUsuario(usuario);
            nombre =
                usuarioObj.getNombre() + " " + usuarioObj.getApellidoPaterno() + " " + usuarioObj.getApellidoMaterno();
            mail = usuarioObj.getEmail();
            perfil = usuarioObj.getPerfil();
            hmData.put("USUARIO_NOMBRE", nombre);
            hmData.put("USUARIO_MAIL", mail);
            hmData.put("USUARIO_PERFIL", perfil);


            return hmData;
        } catch (Throwable t) {
            throw new AppException("Error al obetener corrreo del usuario : " + usuario, t);
        }
    }

    public HashMap obtieneInfoGerenteSubdirector() throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String strSQL = "";
        String nombreConc = "", mailConc = "", nombreSub = "", mailSub = "";
        log.info("obtieneInfoGerenteSubdirector (E) ");


        try {
            con.conexionDB();
            strSQL =
                "SELECT cg_mail, cg_subdirector usuariosub, cg_tel_subdirector telefonosub,  " +
                "       cg_concentrador usuarioconc, cg_tel_concentrador telefonoconc " + "  FROM ope_paramgenerales " +
                " where ic_email = 1";

            ps = con.queryPrecompilado(strSQL);
            rs = ps.executeQuery();

            HashMap hmData = new HashMap();
            if (rs.next()) {
                String usuarioSub = rs.getString("usuariosub") == null ? "" : rs.getString("usuariosub");
                String telefonoSub = rs.getString("telefonosub") == null ? "" : rs.getString("telefonosub");
                String usuarioConc = rs.getString("usuarioconc") == null ? "" : rs.getString("usuarioconc");
                String telefonoConc = rs.getString("telefonoconc") == null ? "" : rs.getString("telefonoconc");
                String mailFondoLiquido = rs.getString("cg_mail") == null ? "" : rs.getString("cg_mail");

                UtilUsr utilUsr = new UtilUsr();
                System.out.println("usuarioSub  :::::::::::::::::::::" + usuarioSub.replaceAll("\\s", "") + ":::");
                System.out.println("usuarioConc  :::::::::::::::::::::" + usuarioConc.replaceAll("\\s", "") + ":::");

                if (!"".equals(usuarioSub.replaceAll("\\s", ""))) {
                    Usuario userSub = utilUsr.getUsuario(usuarioSub);
                    nombreSub =
                        userSub.getNombre() + " " + userSub.getApellidoPaterno() + " " + userSub.getApellidoMaterno();

                    mailSub = userSub.getEmail();
                }
                if (!"".equals(usuarioConc.replaceAll("\\s", ""))) {
                    Usuario userConc = utilUsr.getUsuario(usuarioConc);
                    nombreConc =
                        userConc.getNombre() + " " + userConc.getApellidoPaterno() + " " +
                        userConc.getApellidoMaterno();

                    mailConc = userConc.getEmail();
                }
                hmData.put("GERENTE_USUARIO", usuarioConc);
                hmData.put("GERENTE_NOMBRE", nombreConc);
                hmData.put("GERENTE_MAIL", mailConc);
                hmData.put("GERENTE_TELEFONO", telefonoConc);

                hmData.put("SUBDIRECTOR_USUARIO", usuarioSub);
                hmData.put("SUBDIRECTOR_NOMBRE", nombreSub);
                hmData.put("SUBDIRECTOR_MAIL", mailSub);
                hmData.put("SUBDIRECTOR_TELEFONO", telefonoSub);

                hmData.put("MAILS_FONDO_LIQUIDO", mailFondoLiquido);


            }
            rs.close();
            ps.close();

            return hmData;
        } catch (Throwable t) {
            throw new AppException("Error al obetener datos de gerente y subdirector paramaterizados", t);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("obtieneInfoGerenteSubdirector (S) ");
        }

    }

    private List obtieneRegInfoSolics(List lstSolics) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String strSQL = "";
        try {
            con.conexionDB();

            strSQL =
                "SELECT ors.ic_solicitud cvesolicitud,   " +
                "       TO_CHAR (ors.df_solicitud, 'dd/mm/yyyy') fecsolicitud, " +
                "       ors.fn_monto_solicitud importesolic, " +
                "       ors.ic_if cveif, ors.ic_contrato cvecontrato, " +
                "       ors.ig_numero_prestamo numprestamo, " + "       cm.cd_nombre nombremoneda, " +
                "       TO_CHAR(ors.df_operado,'dd/mm/yyyy') fecop, " +
                "       TO_CHAR(ors.df_rechazo,'dd/mm/yyyy HH24:mi:ss') fecrechazo, " +
                //"       ors.cg_hora_operado horaop, " +
                "       odi.ic_num_cuenta numcuenta, " + "       odi.cg_banco descbanco, " +
                "       ci.cg_razon_social nombrebanco, " + "       ors.no_folio folio, " +
                "       ors.ic_moneda cvemoneda, " + "       ors.cg_observaciones_if observif,  " +
                "       ors.cg_observaciones_op observop, " + "       ors.cg_causas_rechazo causasrech, " +
                "       ors.cg_usuario_op usuarioop, " + "       ors.cc_acuse ccacuse, " +
                "		  odi.cg_ejec_seg usuariosegui, " + "		  odi.cg_ejec_promo usuariopromo, " +
                "       ors.cg_usuario_if usuarioif, " + "       odi.IC_CTA_CLABE numClabe  " +
                "  FROM ope_reg_solicitud ors,  " + "       ope_datos_if odi,  " + "       comcat_moneda cm,  " +
                "       opecat_estatus oe,  " + "       opecat_contrato oc, " + "       comcat_if ci " +
                " WHERE ors.ic_if = odi.ic_if  " + "   AND ors.ic_moneda = cm.ic_moneda  " +
                "   AND ors.ic_estatus_ope = oe.ic_estatus_ope  " + "   AND ors.ic_contrato = oc.ic_contrato " +
                "   AND odi.ic_if = ci.ic_if " + "   AND ors.ic_solicitud = ? ";

            String nombreUserIF = "";
            String userIF = "";
            List lstRegSolics = new ArrayList();
            for (int x = 0; x < lstSolics.size(); x++) {
                HashMap hmData = new HashMap();
                ps = con.queryPrecompilado(strSQL);
                ps.setLong(1, Long.parseLong((String) lstSolics.get(x)));
                rs = ps.executeQuery();
                if (rs.next()) {
                    userIF = rs.getString("usuarioif") == null ? "" : rs.getString("usuarioif");

                    if (userIF != null && !"".equals(userIF)) {
                        nombreUserIF = (String) (obtieneInfoCorreo(userIF)).get("USUARIO_NOMBRE");
                    }

                    hmData.put("CVESOLICITUD",
                               rs.getString("cvesolicitud") == null ? "" : rs.getString("cvesolicitud"));
                    hmData.put("FECSOLICITUD",
                               rs.getString("fecsolicitud") == null ? "" : rs.getString("fecsolicitud"));
                    hmData.put("IMPORTESOLIC",
                               rs.getString("importesolic") == null ? "" : rs.getString("importesolic"));
                    hmData.put("CVEIF", rs.getString("cveif") == null ? "" : rs.getString("cveif"));
                    hmData.put("CVECONTRATO", rs.getString("cvecontrato") == null ? "" : rs.getString("cvecontrato"));
                    hmData.put("NUMPRESTAMO", rs.getString("numprestamo") == null ? "" : rs.getString("numprestamo"));
                    hmData.put("NOMBREMONEDA",
                               rs.getString("nombremoneda") == null ? "" : rs.getString("nombremoneda"));
                    hmData.put("FECOP", rs.getString("fecop") == null ? "" : rs.getString("fecop"));
                    hmData.put("FECRECHAZO", rs.getString("fecrechazo") == null ? "" : rs.getString("fecrechazo"));
                    hmData.put("NUMCUENTA", rs.getString("numcuenta") == null ? "" : rs.getString("numcuenta"));
                    hmData.put("NOMBREBANCO", rs.getString("nombrebanco") == null ? "" : rs.getString("nombrebanco"));
                    hmData.put("DESCBANCO", rs.getString("descbanco") == null ? "" : rs.getString("descbanco"));
                    hmData.put("FOLIO", rs.getString("folio") == null ? "" : rs.getString("folio"));
                    hmData.put("CVEMONEDA", rs.getString("cvemoneda") == null ? "" : rs.getString("cvemoneda"));
                    hmData.put("OBSERVIF", rs.getString("observif") == null ? "" : rs.getString("observif"));
                    hmData.put("OBSERVOP", rs.getString("observop") == null ? "" : rs.getString("observop"));
                    hmData.put("CAUSASRECH", rs.getString("causasrech") == null ? "" : rs.getString("causasrech"));
                    hmData.put("USUARIOOP", rs.getString("usuarioop") == null ? "" : rs.getString("usuarioop"));
                    hmData.put("CCACUSE", rs.getString("ccacuse") == null ? "" : rs.getString("ccacuse"));
                    hmData.put("USUARIOIF", userIF);
                    hmData.put("NOMBREUSERIF", nombreUserIF);
                    hmData.put("USUARIOPROMO", rs.getString("usuariopromo"));
                    hmData.put("USUARIOSEGUI", rs.getString("usuariosegui"));
                    hmData.put("NUMCLABE", (rs.getString("numClabe") == null) ? "" : rs.getString("numClabe"));
                    lstRegSolics.add(hmData);
                }
                rs.close();
                ps.close();
            }

            return lstRegSolics;
        } catch (Throwable t) {
            throw new AppException("Error al obtener info de solic", t);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

        }
    }

    private String obtieneArchivoAmort(String rutaDestino, String cveSolicitud) throws AppException {
        String strSQL = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        AccesoDB con = new AccesoDB();
        String nombreArchivoTmp = null;

        try {
            con.conexionDB();

            CreaArchivo creaArchivo = new CreaArchivo();
            nombreArchivoTmp = creaArchivo.nombreArchivo() + ".xls";
            ComunesXLS xls = new ComunesXLS(rutaDestino + nombreArchivoTmp);

            xls.setTabla(3);
            xls.setCelda("PERIODO", "celda01", ComunesXLS.CENTER, 1);
            xls.setCelda("FECHA", "celda01", ComunesXLS.CENTER, 1);
            xls.setCelda("AMORTIZACION", "celda01", ComunesXLS.CENTER, 1);

            strSQL =
                "SELECT ota.ic_solicitud cvesolicitud,  " + "       ota.ic_tabla_amort cvetablaamort,  " +
                "       ota.in_num_periodo numperiodo,  " + "       TO_CHAR(ota.df_alta,'dd/mm/yyyy') fecalta, " +
                "       ota.fn_monto_amort montoamort" + "  FROM ope_reg_solicitud ors, ope_tabla_amort ota " +
                " WHERE ors.ic_solicitud = ota.ic_solicitud " + "   AND ors.ic_solicitud = ? " +
                " ORDER BY ota.ic_tabla_amort ";

            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(cveSolicitud));
            rs = ps.executeQuery();
            while (rs.next()) {
                String numPeriodo = rs.getString("numperiodo") == null ? "" : rs.getString("numperiodo");
                String fecAlta = rs.getString("fecalta") == null ? "" : rs.getString("fecalta");
                String montoAmort = rs.getString("montoamort") == null ? "" : rs.getString("montoamort");

                xls.setCelda(numPeriodo, "formas", ComunesXLS.LEFT, 1);
                xls.setCelda(fecAlta, "formas", ComunesXLS.LEFT, 1);
                xls.setCelda("$" + Comunes.formatoDecimal(montoAmort, 2), "formas", ComunesXLS.LEFT, 1);

            }
            rs.close();
            ps.close();

            xls.cierraTabla();
            xls.cierraXLS();

            return nombreArchivoTmp;

        } catch (Exception e) {
            throw new AppException("Error al obtener archivo de la solicitud ", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }


    //*************************** CASO DE USO: CU01-REGISTRAR SOLICITUDES DE RECURSOS*********************************


    /**
     * Metodo para sacar la fecha del d�a siguiente h�bil
     * @throws java.lang.Exception
     * @return
     */
    public boolean getvalidaDiaHabil(String diaMes) {
        log.info("getvalidaDiaHabil (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        boolean diahabil = true;
        List varBind = new ArrayList();
        try {

            con.conexionDB();
            SQL.append(" SELECT to_char(DF_DIA_INHABIL,'dd/mm/yyyy') FROM comcat_dia_inhabil where DF_DIA_INHABIL is not null  " +
                       " and  DF_DIA_INHABIL  = TO_DATE ( ?, 'dd/mm/yyyy') ");

            varBind.add(diaMes);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);
            PreparedStatement ps = con.queryPrecompilado(SQL.toString(), varBind);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                diahabil = false;
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getSiguienteDiaHabil " + e);
            throw new AppException("Error al getSiguienteDiaHabil", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getSiguienteDiaHabil (S)");
        }
        return diahabil;
    }


    /**
     * Metodo que valida el horario
     * Para el registro de las nuevas solicitudes
     * @throws java.lang.Exception
     * @return
     */
    public boolean getValidaHora(String horario) {
        log.info("getValidaHora (E)");
        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        boolean horaActiva = false;

        try {

            con.conexionDB();

            SQL.append(" SELECT TO_CHAR(SYSDATE,'hh24:mi') HORA_ACTUAL  " +
                       "  FROM DUAL  WHERE TO_CHAR(SYSDATE,'hh24:mi')  <= '" + horario + "'");
            log.trace("SQL.toString():" + SQL.toString());

            PreparedStatement ps = con.queryPrecompilado(SQL.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                horaActiva = true;
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getValidaHora " + e);
            throw new AppException("Error al getValidaHora", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getValidaHora (S)");
        }
        return horaActiva;
    }

    /**
     * Metodo que genere el numeor de solicitud
     * @throws com.netro.exception.NafinException
     * @return
     */
    public String getNumaxSolicitud() {
        AccesoDB con = new AccesoDB();
        String icSolicitud = "";
        boolean commit = true;
        log.info("getNumaxSolicitud(E)");
        try {

            con.conexionDB();
            String query = "select SEQTMP_OPE_REG_SOLICITUD.NEXTVAL from dual";

            PreparedStatement ps = con.queryPrecompilado(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                icSolicitud = rs.getString(1);
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getNumaxSolicitud " + e);
            throw new AppException("Error al getNumaxSolicitud", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getNumaxSolicitud (S)");
        }
        return icSolicitud;
    }


    /**
     *
     * @return
     * @param perfil
     */
    public HashMap getContratoXPerfil(String perfil) {
        log.info("getContratoXPerfil (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();
        HashMap registros = new HashMap();
        try {
            con.conexionDB();

            SQL.append(" select IC_CONTRATO,  CG_NOMBRE_CONTRATO  from OPECAT_CONTRATO where CG_PERFL = ?  ");

            varBind.add(perfil);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);

            ps = con.queryPrecompilado(SQL.toString(), varBind);
            rs = ps.executeQuery();

            if (rs.next()) {
                registros.put("IC_CONTRATO", rs.getString("IC_CONTRATO") == null ? "" : rs.getString("IC_CONTRATO"));
                registros.put("CG_NOMBRE_CONTRATO",
                              rs.getString("CG_NOMBRE_CONTRATO") == null ? "" : rs.getString("CG_NOMBRE_CONTRATO"));
            }

            rs.close();
            ps.close();

        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getContratoXPerfil " + e);
            throw new AppException("Error al getContratoXPerfil", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getContratoXPerfil (S)");
        }
        return registros;
    }


    /**
     * metodo para obtener los datos de las secci�n
     * nombre completo y contrato
     * @throws java.lang.Exception
     * @return
     * @param ic_if
     */
    public HashMap getDatosIF(String ic_if) {
        log.info("getDatosIF (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();
        HashMap registros = new HashMap();
        try {
            con.conexionDB();

            SQL.append(" SELECT  i.CG_RAZON_SOCIAL AS NOMBRE_IF,  " +
                       " d.IC_NUM_CUENTA  AS NUM_CUENTAS,   d.CG_BANCO AS BANCO,  d.IC_CTA_CLABE as CLABE, " +
                       " to_char(d.DF_FIRMA_CONTRATO,'dd/mm/yyyy')  as FECHACONTRATO,   " +
                       " d.CG_CONTRATO_OE as NOMBRECONTRATO_OE, to_char(d.DF_FIRMA_CONTRATO_OE, 'dd/mm/yyyy')  as FECHACONTRATO_OE,  " +
                       " CG_CONT_MODIFICATORIOS  AS MODIFICATORIOS, " +
                       " CG_HORARIO_ENVIO as HORAENVIO, CG_HORARIO_REENVIO AS HORAREENVIO  " +
                       " FROM OPE_DATOS_IF d,   comcat_if i  " + " where d.ic_if = i.ic_if  " + "  and d.ic_if  = ? ");

            varBind.add(ic_if);
            //System.out.println(":::  SQL :::: "+SQL);
            //System.out.println(":::  varBind :::: "+varBind);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);

            ps = con.queryPrecompilado(SQL.toString(), varBind);
            rs = ps.executeQuery();

            if (rs.next()) {
                registros.put("NOMBRE_IF", rs.getString("NOMBRE_IF") == null ? "" : rs.getString("NOMBRE_IF"));
                registros.put("NUM_CUENTAS", rs.getString("NUM_CUENTAS") == null ? "" : rs.getString("NUM_CUENTAS"));
                registros.put("BANCO", rs.getString("BANCO") == null ? "" : rs.getString("BANCO"));
                registros.put("CLABE", rs.getString("CLABE") == null ? "" : rs.getString("CLABE"));
                registros.put("FECHACONTRATO",
                              rs.getString("FECHACONTRATO") == null ? "" : rs.getString("FECHACONTRATO"));
                registros.put("NOMBRECONTRATO_OE",
                              rs.getString("NOMBRECONTRATO_OE") == null ? "" : rs.getString("NOMBRECONTRATO_OE"));
                registros.put("FECHACONTRATO_OE",
                              rs.getString("FECHACONTRATO_OE") == null ? "" : rs.getString("FECHACONTRATO_OE"));
                registros.put("MODIFICATORIOS",
                              rs.getString("MODIFICATORIOS") == null ? "" : rs.getString("MODIFICATORIOS"));
                registros.put("HORAENVIO", rs.getString("HORAENVIO") == null ? "" : rs.getString("HORAENVIO"));
                registros.put("HORAREENVIO", rs.getString("HORAREENVIO") == null ? "" : rs.getString("HORAREENVIO"));
            }
            rs.close();
            ps.close();

        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getDatosIF " + e);
            throw new AppException("Error al getDatosIF", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getDatosIF (S)");
        }
        return registros;
    }

    /**
     *  @return
     * @param ic_if
     */
    public List getFirmasModificatorias(String ic_if) {
        log.info("getFirmasModificatorias (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();
        HashMap datos = new HashMap();
        List registros = new ArrayList();
        try {
            con.conexionDB();

            SQL.append(" SELECT  CG_DESCRIPCION AS  DESCRIPCION, to_char(DF_FIRMA_MODIFICATORIA,'dd/mm/yyyy') as  FECHA " +
                       " from OPE_DET_FIR_MODIFI  f ,  OPE_DATOS_IF i " + " where f.ic_if  = i.IC_IF  " +
                       "  AND  i.ic_if =  ? " + "  order by IC_FIRMA_MODIFI asc");
            varBind.add(ic_if);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);

            ps = con.queryPrecompilado(SQL.toString(), varBind);
            rs = ps.executeQuery();

            while (rs.next()) {
                datos = new HashMap();
                datos.put("DESCRIPCION", rs.getString("DESCRIPCION") == null ? "" : rs.getString("DESCRIPCION"));
                datos.put("FECHA", rs.getString("FECHA") == null ? "" : rs.getString("FECHA"));
                registros.add(datos);
            }

            rs.close();
            ps.close();

        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getFirmasModificatorias " + e);
            throw new AppException("Error al getFirmasModificatorias", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getFirmasModificatorias (S)");
        }
        return registros;
    }

    /**
     *  @return
     * @param ic_if
     */
    public List getFirmasModificatoriasCon(String ic_if, AccesoDB con) throws NafinException {
        log.info("getFirmasModificatorias (E)");

        boolean commit = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();
        HashMap datos = new HashMap();
        List registros = new ArrayList();
        try {

            SQL.append(" SELECT  CG_DESCRIPCION AS  DESCRIPCION, to_char(DF_FIRMA_MODIFICATORIA,'dd/mm/yyyy') as  FECHA " +
                       " from OPE_DET_FIR_MODIFI  f ,  OPE_DATOS_IF i " + " where f.ic_if  = i.IC_IF  " +
                       "  AND  i.ic_if =  ? " + "  order by IC_FIRMA_MODIFI asc");
            varBind.add(ic_if);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);

            ps = con.queryPrecompilado(SQL.toString(), varBind);
            rs = ps.executeQuery();

            while (rs.next()) {
                datos = new HashMap();
                datos.put("DESCRIPCION", rs.getString("DESCRIPCION") == null ? "" : rs.getString("DESCRIPCION"));
                datos.put("FECHA", rs.getString("FECHA") == null ? "" : rs.getString("FECHA"));
                registros.add(datos);
            }
            rs.close();
            ps.close();


        } catch (Exception e) {
            System.out.println(" Error getFirmasModificatorias " + e);
            throw new NafinException("Error al getFirmasModificatorias");
        } finally {

            log.info("getFirmasModificatorias (S)");
        }
        return registros;
    }

    /**
     * Metodo para guardar los archivos de Cotizaci�n, Cartera con Prenda y Comprobante RUG
     * @return
     * @param tipoArchivo
     * @param rutaArchivo
     * @param ic_solicitud
     */
    public boolean guardarArchivoSolic(String ic_solicitud, String rutaArchivo, String tipoArchivo,
                                       String tipoSolicitud) {
        log.info("guardarArchivoSolic (E)");

        AccesoDB con = new AccesoDB();
        boolean exito = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String strSQL = "", campo = "", tabla = "OPETMP_REG_SOLICITUD";

        if (tipoArchivo.equals("Cotiza")) {
            campo = "BI_DOC_COTIZACION";
        }
        if (tipoArchivo.equals("Cartera_Prenda")) {
            campo = "BI_DOC_CARTERA";
        }
        if (tipoArchivo.equals("Cartera_Prenda_Pdf")) {
            campo = "BI_DOC_CARTERA_PDF";
        }
        if (tipoArchivo.equals("RUG")) {
            campo = "BI_DOC_BOLETARUG";
        }
        if (tipoArchivo.equals("Empresa")) {
            campo = "BI_EMPRESAS";
        }

        //if(tipoSolicitud.equals("Modificar"))  {  		tabla =  "OPE_REG_SOLICITUD";    }

        try {
            con.conexionDB();

            File archivo = new File(rutaArchivo);
            FileInputStream fileinputstream = new FileInputStream(archivo);

            strSQL = " Select count(*) AS numRegistros  from " + tabla + "  WHERE IC_SOLICITUD = ? ";
            log.debug("strSQL " + strSQL);
            ps = con.queryPrecompilado(strSQL);
            ps.setInt(1, Integer.parseInt(ic_solicitud));
            rs = ps.executeQuery();
            rs.next();
            boolean existeRegistro = (rs.getInt("numRegistros") == 0) ? false : true;
            rs.close();
            ps.close();

            log.debug("existeRegistro " + existeRegistro);

            if (!existeRegistro) {
                strSQL =
                    " INSERT INTO " + tabla + " ( " + campo + " , DF_SOLICITUD, IC_SOLICITUD, DF_ALTA_SOLICITUD  ) " +
                    "VALUES (  '0',  Sysdate , ?, Sysdate )";
                log.debug("strSQL " + strSQL);
                ps = con.queryPrecompilado(strSQL);
                ps.setString(1, ic_solicitud);
                ps.executeUpdate();
                ps.close();
            }

            log.debug(" rutaArchivo ---------------->" + rutaArchivo);
            log.debug(" archivo ---------------->" + archivo);
            log.debug("(int)archivo.length() ---------------->" + (int) archivo.length());
            log.debug(" ic_solicitud= " + ic_solicitud);
            log.debug(" campo = " + campo);


            strSQL = " UPDATE " + tabla + " SET " + campo + "  = ? " + " WHERE IC_SOLICITUD = ? ";
            log.debug("strSQL " + strSQL);
            ps = con.queryPrecompilado(strSQL);
            ps.setBinaryStream(1, fileinputstream, (int) archivo.length());
            ps.setString(2, ic_solicitud);
            ps.executeUpdate();
            ps.close();

        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("guardarArchivoSolic  " + e);
            throw new AppException("SIST0001");

        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.debug("  guardarArchivoSolic (S) ");
            }
        }
        return exito;
    }

    /**
     * Cosulta de  archivos  en la alta de regsitos de solicitud
     * @return
     * @param extension
     * @param tipoArchivo
     * @param pantalla
     * @param ic_solicitud
     * @param rutaDestino
     */
    public String consArchCotizacion(String rutaDestino, String ic_solicitud, String pantalla, String tipoArchivo,
                                     String extension, String tipoSolicitud) {
        log.info("consArchCotizacion(E) ::..");

        AccesoDB con = new AccesoDB();
        String nombreArchivoTmp = null;
        List lVarBind = new ArrayList();

        String tabla = "OPETMP_REG_SOLICITUD", columna = "";

        log.debug(" rutaDestino ::: " + rutaDestino);

        if (tipoArchivo.equals("Cotiza")) {
            columna = "BI_DOC_COTIZACION";
        }
        if (tipoArchivo.equals("Cartera")) {
            columna = "BI_DOC_CARTERA";
        }
        if (tipoArchivo.equals("Cartera_Pdf")) {
            columna = "BI_DOC_CARTERA_PDF";
        }
        if (tipoArchivo.equals("Boleta")) {
            columna = "BI_DOC_BOLETARUG";
        }
        if (pantalla.equals("Acuse")) {
            tabla = "OPE_REG_SOLICITUD";
        }

        try {
            con.conexionDB();

            String strSQL = " SELECT " + columna + " FROM " + tabla + " WHERE IC_SOLICITUD = ? ";
            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);

            PreparedStatement ps = con.queryPrecompilado(strSQL, lVarBind);
            ResultSet rs = ps.executeQuery();
            log.debug(" strSQL ::: " + strSQL);
            log.debug(" lVarBind ::: " + lVarBind);

            if (rs.next()) {
                InputStream inStream = rs.getBinaryStream(columna);

                CreaArchivo creaArchivo = new CreaArchivo();
                if (!creaArchivo.make(inStream, rutaDestino, "." + extension)) {
                    throw new AppException("Error al generar el archivo en " + rutaDestino);
                }
                nombreArchivoTmp = creaArchivo.getNombre();
                inStream.close();
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            log.error("consArchCotizacion(ERROR) ::..");
            throw new AppException("Error Inesperado", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("consArchCotizacion(S) ::..");
        }
        return nombreArchivoTmp;
    }


    /**
     * Metodo que valida el archivo a cargar sobre las tablas de amortizaci�n
     * @return
     * @param montoSolicitado
     * @param rutaNombreArchivo
     * @param ic_solicitud
     */
    public List validaTasaAmortizacion(String ic_solicitud, String rutaNombreArchivo, String montoSolicitado) {

        log.info("validaTasaAmortizacion (E)");

        List listas = new ArrayList();
        List Errorlineas = new ArrayList();
        List lineasBien = new ArrayList();
        List datosBien = new ArrayList();

        List lErrores = new ArrayList();
        List lBien = new ArrayList();
        List lineasOK = new ArrayList();
        String linea = "";

        BigDecimal montoSolicitadoT = new BigDecimal(montoSolicitado);
        BigDecimal montoTotal = new BigDecimal(0.0);

        try {

            java.io.File lofArchivo = new java.io.File(rutaNombreArchivo);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            String periodo = "", fecha = "", amortizacion = "";
            int iNumLinea = 0;

            while ((linea = br.readLine()) != null) {
                iNumLinea++;

                VectorTokenizer vt = new VectorTokenizer(linea, ",");
                Vector lineaArchivo = vt.getValuesVector();

                lineasOK = new ArrayList();

                if (lineaArchivo.size() > 0) {
                    int i = 0;
                    String campo = Comunes.quitaComitasSimples(lineaArchivo.get(i).toString()).trim();
                    campo = campo.replace('"', ' ').trim();
                    lineaArchivo.set(i, campo);

                    Errorlineas = new ArrayList();
                    lBien = new ArrayList();

                    if (lineaArchivo.size() > 3) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea +
                                        " No cuenta con la cantidad de campos requeridos");
                    } else if (lineaArchivo.size() < 3) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea +
                                        " No cuenta con la cantidad de campos requeridos");
                    }

                    int columna = 1;
                    //periodo
                    if (lineaArchivo.size() >= columna) {
                        periodo = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        fecha = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        amortizacion = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }


                    //Validaci�n del campos Periodo
                    if (periodo.equals("")) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea + " El n�mero de Periodo es requerido.");

                    } else if (!Comunes.esNumero(periodo)) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea + " El Periodo debe ser un n�mero entero");

                    } else if (periodo.length() > 3) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea +
                                        " El n�mero de Periodo rebasa la longitud m�xima de 3 d�gitos");

                    } else if (Integer.parseInt(periodo) != iNumLinea) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea +
                                        " El n�mero de Periodo debe ser consecutivo.");
                    }

                    //Validaci�n del campos Fecha
                    if (fecha.equals("")) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea + " La Fecha es requerida");

                    } else if (!Comunes.checaFecha(fecha)) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea +
                                        " La Fecha debe tener el siguiente formato: dd/mm/aaaa, donde dd: indica el d�a mm: indica el mes y aaaa indica el a�o, por ejemplo: 01/09/2013");

                    } else if (fecha.length() > 10) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea +
                                        " La Fecha rebasa la longitud m�xima de 10 d�gitos");
                    }

                    //Validaci�n del campos Amortizaci�n
                    if (amortizacion.equals("")) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea + " La Amortizaci�n es requerida");

                    } else if (!Comunes.esDecimal(amortizacion)) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea +
                                        " La Amortizaci�n debe ser n�meros enteros y decimales.");

                    } else if (!Comunes.precisionValida(amortizacion, 17, 2)) {
                        Errorlineas.add("Error en la Linea: " + iNumLinea +
                                        " La Amortizaci�n rebasa la longitud m�xima de 17 d�gitos y 2 decimales");

                    }

                    if (Errorlineas.size() > 0) { //cuando hay errores
                        lErrores.add(Errorlineas);
                    } else if (Errorlineas.size() == 0) { //cuando no hay error
                        lineasBien.add(periodo);
                        lBien.add(periodo);
                        lBien.add(fecha);
                        lBien.add(amortizacion);
                        montoTotal = montoTotal.add(new BigDecimal(amortizacion));

                    }

                } // for

                if (lBien.size() > 0) {
                    datosBien.add(lBien);
                }
            } // while

            int valorM = montoTotal.compareTo(montoSolicitadoT);

            log.debug("montoTotal  " + montoTotal.toPlainString());
            log.debug("montoSolicitadoT  " + montoSolicitadoT.toPlainString());
            log.debug("valorM  " + valorM);

            if (valorM != 0) {
                Errorlineas = new ArrayList();
                Errorlineas.add("Error El Monto total de las amortizaciones debe ser igual al monto solicitado por el IF.");
                lErrores.add(Errorlineas);
            }

            listas.add(lErrores);
            listas.add(lineasBien);
            listas.add(datosBien);


        } catch (Throwable e) {
            e.printStackTrace();
            log.error("validaTasaAmortizacion  " + e);
            throw new AppException("SIST0001");

        } finally {
            log.info("validaTasaAmortizacion(S)  ");

        }
        return listas;
    }

    /**
     * Una vez validado el archivo  se guarda en las tablas temporales la amortizaci�n
     * @param amortizacion
     * @param fecha
     * @param periodo
     * @param ic_solicitud
     */
    public String guardaTasaAmortizacionTMP(String ic_solicitud, String periodo[], String fecha[],
                                            String amortizacion[]) {

        log.info("guardaTasaAmortizacionTMP(E)  ");

        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String hayAmortizacion = "N";

        try {
            con.conexionDB();

            strSQL = new StringBuffer();
            strSQL.append(" SELECT count(*)  as total  FROM OPETMP_TABLA_AMORT  WHERE IC_SOLICITUD  = ? ");
            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            rs = ps.executeQuery();
            rs.next();
            boolean existeRegistro = (rs.getInt("total") == 0) ? false : true;
            rs.close();
            ps.close();

            if (existeRegistro) {
                strSQL = new StringBuffer();
                strSQL.append(" DELETE   FROM OPETMP_TABLA_AMORT  WHERE IC_SOLICITUD  = ?   ");
                lVarBind = new ArrayList();
                lVarBind.add(ic_solicitud);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();
            }

            for (int i = 0; i < periodo.length; i++) {
                strSQL = new StringBuffer();
                strSQL.append(" INSERT INTO OPETMP_TABLA_AMORT  ( IC_SOLICITUD, IC_TABLA_AMORT , IN_NUM_PERIODO ,  DF_ALTA  ,  FN_MONTO_AMORT  ) " +
                              " VALUES(  ? ,  SEQTMP_OPE_TABLA_AMORT.NEXTVAL,  ? , TO_DATE(?, 'dd/mm/yyyy'), ? )   ");

                lVarBind = new ArrayList();
                lVarBind.add(ic_solicitud);
                lVarBind.add(periodo[i]);
                lVarBind.add(fecha[i]);
                lVarBind.add(amortizacion[i]);
                log.debug("strSQL " + strSQL.toString());
                log.debug("lVarBind " + lVarBind);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();

            }

        } catch (Throwable e) {
            exito = false;
            hayAmortizacion = "N";
            e.printStackTrace();
            log.error("guardaTasaAmortizacionTMP  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                hayAmortizacion = "S";
                log.info("  guardaTasaAmortizacionTMP (S) ");
            }
        }
        return hayAmortizacion;
    }

    /**
     *
     * @throws com.netro.exception.NafinException
     * @param datosSolicitud
     */
    public void cargaSolicTMP(List parametros) {

        log.info("cargaSolicTMP(E)  ");

        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con.conexionDB();

            String ic_if = parametros.get(0).toString();
            String montoSolic = parametros.get(1).toString();
            String ic_moneda = parametros.get(2).toString();
            String des_recuersos = parametros.get(3).toString();
            String df_pago_capital = parametros.get(4).toString();
            String df_pago_interes = parametros.get(5).toString();
            String df_vencimiento = parametros.get(6).toString();
            String cg_tipo_tasa = parametros.get(7).toString();
            String cg_tasaInteres = parametros.get(8).toString();
            String observaciones = parametros.get(9).toString();
            String ic_solicitud = parametros.get(10).toString();
            String ic_contrato = parametros.get(11).toString();
            //String fechaSolicitud =   parametros.get(12).toString();
            String combTasa = parametros.get(13).toString();
            String loginUsuario = parametros.get(14).toString();
            String OpeFirma = this.getOperaFirmaMancomunada(ic_if);
            String estatus = "";
            if (OpeFirma.equals("S")) {
                estatus = "7";
            } else {
                estatus = "1";
            }
            strSQL = new StringBuffer();
            strSQL.append(" Select count(*) AS numRegistros  from OPETMP_REG_SOLICITUD  WHERE IC_SOLICITUD = ? ");
            log.debug("strSQL " + strSQL);

            ps = con.queryPrecompilado(strSQL.toString());
            ps.setInt(1, Integer.parseInt(ic_solicitud));
            rs = ps.executeQuery();
            rs.next();
            boolean existeRegistro = (rs.getInt("numRegistros") == 0) ? false : true;
            rs.close();
            ps.close();

            if (!existeRegistro) {
                strSQL = new StringBuffer();
                strSQL.append(" INSERT INTO  OPETMP_REG_SOLICITUD  " +
                              "	(IC_SOLICITUD,  IC_IF ,  FN_MONTO_SOLICITUD ,  IC_MONEDA ,   CG_DES_RECURSOS ,  " +
                              "    DF_PAGO_CAPITAL, DF_PAGO_INTERES,  DF_VENCIMIENTO,  CG_TIPO_TASA, IC_TASA, IN_TASA_ACEPTADA ,  " +
                              "     CG_OBSERVACIONES_IF, IC_CONTRATO , IC_ESTATUS_OPE, DF_SOLICITUD , DF_ALTA_SOLICITUD , CG_USUARIO_IF)  " +
                              " VALUES ( ?, ?, ?, ?, ?, TO_DATE(?, 'dd/mm/yyyy') , TO_DATE(?, 'dd/mm/yyyy') " +
                              " , TO_DATE(?, 'dd/mm/yyyy') , ? , ?, ? , ?, ?,?, Sysdate, Sysdate, ?   )");

                lVarBind = new ArrayList();
                lVarBind.add(ic_solicitud);
                lVarBind.add(ic_if);
                lVarBind.add(montoSolic);
                lVarBind.add(ic_moneda);
                lVarBind.add(des_recuersos);
                lVarBind.add(df_pago_capital);
                lVarBind.add(df_pago_interes);
                lVarBind.add(df_vencimiento);
                lVarBind.add(cg_tipo_tasa);
                lVarBind.add(combTasa);
                lVarBind.add(cg_tasaInteres);
                lVarBind.add(observaciones);
                lVarBind.add(ic_contrato);
                lVarBind.add(estatus);
                lVarBind.add(loginUsuario);

            } else {

                strSQL = new StringBuffer();
                strSQL.append(" UPDATE OPETMP_REG_SOLICITUD  " + " SET  IC_IF = ? " + " , FN_MONTO_SOLICITUD =   ?  " +
                              " , IC_MONEDA =  ? " + " , CG_DES_RECURSOS =  ? " +
                              " , DF_PAGO_CAPITAL = TO_DATE(?, 'dd/mm/yyyy')  " +
                              " , DF_PAGO_INTERES = TO_DATE(?, 'dd/mm/yyyy')  " +
                              " ,DF_VENCIMIENTO = TO_DATE(?, 'dd/mm/yyyy')  " + " , CG_TIPO_TASA  =  ? " +
                              " , IC_TASA = ? " + " , IN_TASA_ACEPTADA  = ? " + " , CG_OBSERVACIONES_IF =? " +
                              " , IC_CONTRATO  =  ? " + " , IC_ESTATUS_OPE = ? " + //13
                              " , DF_SOLICITUD  =  Sysdate " + " , CG_USUARIO_IF =  ? " + " WHERE IC_SOLICITUD = ? ");
                lVarBind = new ArrayList();
                lVarBind.add(ic_if);
                lVarBind.add(montoSolic);
                lVarBind.add(ic_moneda);
                lVarBind.add(des_recuersos);
                lVarBind.add(df_pago_capital);
                lVarBind.add(df_pago_interes);
                lVarBind.add(df_vencimiento);
                lVarBind.add(cg_tipo_tasa);
                lVarBind.add(combTasa);
                lVarBind.add(cg_tasaInteres);
                lVarBind.add(observaciones);
                lVarBind.add(ic_contrato);
                lVarBind.add(estatus);
                lVarBind.add(loginUsuario);
                lVarBind.add(ic_solicitud);

            }

            log.debug("strSQL " + strSQL.toString());
            log.debug("lVarBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();

        } catch (Exception e) {
            exito = false;
            e.printStackTrace();
            log.error("cargaSolicTMP  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  cargaSolicTMP (S) ");
            }
        }
    }

    /**
     * Metodo para visualizar los datos en el PreAcuse
     * @throws com.netro.exception.NafinException
     * @param parametros
     */
    public HashMap getDatosSolicTMP(String ic_solicitud, String ic_if) {

        log.info("getDatosSolicTMP(E)  ");

        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap registros = new HashMap();

        try {
            con.conexionDB();

            strSQL = new StringBuffer();
            strSQL.append(" SELECT  to_char(S. DF_SOLICITUD , 'dd/mm/yyyy')   as FECHASOLICITUD , S.FN_MONTO_SOLICITUD AS IMPORTE,   M.CD_NOMBRE AS MONEDA , S.CG_DES_RECURSOS  AS RECURSOS, " +
                          " to_char(S.DF_PAGO_CAPITAL, 'dd/mm/yyyy') AS FECHACAPITAL,  to_char(S.DF_PAGO_INTERES, 'dd/mm/yyyy')  AS  FECHAINTERES,  to_char(S.DF_VENCIMIENTO, 'dd/mm/yyyy') AS FECHAVENCIMIENTO,  " +
                          " DECODE(t.CD_NOMBRE ,'', S.CG_TIPO_TASA  || '  ' ||  s.IN_TASA_ACEPTADA ||' % ', S.CG_TIPO_TASA  ||'  '||  t.CD_NOMBRE  || ' + ' ||  s.IN_TASA_ACEPTADA ||' % '  ) TASAINTERES ,  " +
                          " CG_OBSERVACIONES_IF AS OBSERVACIONES , " + " i.CG_RAZON_SOCIAL AS NOMBRE_IF,  " +
                          " d.IC_NUM_CUENTA  AS NUM_CUENTAS,   d.CG_BANCO AS BANCO,  d.IC_CTA_CLABE as CLABE, " +
                          " c.CG_NOMBRE_CONTRATO as NOMBRECONTRATO, to_char(d.DF_FIRMA_CONTRATO,'dd/mm/yyyy')  as FECHACONTRATO,   " +
                          " d.CG_CONTRATO_OE as NOMBRECONTRATO_OE, to_char(d.DF_FIRMA_CONTRATO_OE, 'dd/mm/yyyy')  as FECHACONTRATO_OE,  " + " CG_CONT_MODIFICATORIOS  AS MODIFICATORIOS  " +

                          " FROM OPETMP_REG_SOLICITUD S, COMCAT_MONEDA M , " +
                          " OPE_DATOS_IF d,   comcat_if i , OPECAT_CONTRATO c, " + " comcat_tasa t " +

                          " WHERE S.IC_SOLICITUD =  ? " + " AND S.IC_MONEDA = M.IC_MONEDA   " +
                          " and d.ic_if = i.ic_if  " + " and s.ic_if = d.ic_if  " +
                          " and s.ic_contrato = c.ic_contrato " + " and s.ic_tasa = t.ic_tasa(+) " +
                          " and d.ic_if  = ?  ");


            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            lVarBind.add(ic_if);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);

            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            rs = ps.executeQuery();

            if (rs.next()) {
                registros.put("FECHASOLICITUD",
                              rs.getString("FECHASOLICITUD") == null ? "" : rs.getString("FECHASOLICITUD"));
                registros.put("IMPORTE", rs.getString("IMPORTE") == null ? "" : rs.getString("IMPORTE"));
                registros.put("MONEDA", rs.getString("MONEDA") == null ? "" : rs.getString("MONEDA"));
                registros.put("RECURSOS", rs.getString("RECURSOS") == null ? "" : rs.getString("RECURSOS"));
                registros.put("FECHACAPITAL", rs.getString("FECHACAPITAL") == null ? "" : rs.getString("FECHACAPITAL"));
                registros.put("FECHAINTERES", rs.getString("FECHAINTERES") == null ? "" : rs.getString("FECHAINTERES"));
                registros.put("FECHAVENCIMIENTO",
                              rs.getString("FECHAVENCIMIENTO") == null ? "" : rs.getString("FECHAVENCIMIENTO"));
                registros.put("TASAINTERES", rs.getString("TASAINTERES") == null ? "" : rs.getString("TASAINTERES"));
                registros.put("OBSERVACIONES",
                              rs.getString("OBSERVACIONES") == null ? "" : rs.getString("OBSERVACIONES"));
                registros.put("NOMBRE_IF", rs.getString("NOMBRE_IF") == null ? "" : rs.getString("NOMBRE_IF"));
                registros.put("NUM_CUENTAS", rs.getString("NUM_CUENTAS") == null ? "" : rs.getString("NUM_CUENTAS"));
                registros.put("BANCO", rs.getString("BANCO") == null ? "" : rs.getString("BANCO"));
                registros.put("CLABE", rs.getString("CLABE") == null ? "" : rs.getString("CLABE"));
                registros.put("NOMBRECONTRATO",
                              rs.getString("NOMBRECONTRATO") == null ? "" : rs.getString("NOMBRECONTRATO"));
                registros.put("FECHACONTRATO",
                              rs.getString("FECHACONTRATO") == null ? "" : rs.getString("FECHACONTRATO"));
                registros.put("NOMBRECONTRATO_OE",
                              rs.getString("NOMBRECONTRATO_OE") == null ? "" : rs.getString("NOMBRECONTRATO_OE"));
                registros.put("FECHACONTRATO_OE",
                              rs.getString("FECHACONTRATO_OE") == null ? "" : rs.getString("FECHACONTRATO_OE"));
                registros.put("MODIFICATORIOS",
                              rs.getString("MODIFICATORIOS") == null ? "" : rs.getString("MODIFICATORIOS"));


            }
            rs.close();
            ps.close();

        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("getDatosSolicTMP  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  getDatosSolicTMP (S) ");
            }
        }
        return registros;
    }


    /**
     *
     * @throws com.netro.exception.NafinException
     * @return
     * @param parametros
     */

    public String getguardarSolicitud(List parametros) {

        log.info("getguardarSolicitud(E)  ");

        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        String icSolicitud = "";

        try {
            con.conexionDB();


            String ic_solicitud = parametros.get(0).toString();
            String nombreUsuario = parametros.get(1).toString();
            String cc_acuse = parametros.get(4).toString();
            String tipoSolicitud = parametros.get(5).toString();
            String combTasa = parametros.get(6).toString();

            if (tipoSolicitud.equals("Captura")) {

                String query = "select SEQ_OPE_REG_SOLICITUD.NEXTVAL from dual";

                ps = con.queryPrecompilado(query);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    icSolicitud = rs.getString(1);
                }
                rs.close();
                ps.close();

                //Inserta Acuse
                strSQL = new StringBuffer();
                strSQL.append(" INSERT INTO OPE_ACUSE  ( CC_ACUSE,  DF_FECHAHORA_CARGA ,  CG_NOMBRE_USUARIO ) " +
                              " VALUES( ?, Sysdate, ? ) ");
                lVarBind = new ArrayList();
                lVarBind.add(cc_acuse);
                lVarBind.add(nombreUsuario);
                log.debug("SQL.toString() " + strSQL.toString());
                log.debug("varBind " + lVarBind);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();

                //INSERTA  TABLA ORIGINAL
                strSQL = new StringBuffer();
                strSQL.append(" INSERT INTO OPE_REG_SOLICITUD  " +
                              " ( IC_SOLICITUD, DF_SOLICITUD, DF_ALTA_SOLICITUD, IC_IF , FN_MONTO_SOLICITUD ,  IC_MONEDA ,   CG_DES_RECURSOS ,   DF_PAGO_CAPITAL ,   " +
                              "   DF_PAGO_INTERES,  DF_VENCIMIENTO , CG_TIPO_TASA ,   IC_TASA ,  IN_TASA_ACEPTADA  ,  CG_OBSERVACIONES_IF,  BI_DOC_COTIZACION , " +
                              "   CC_ACUSE, IC_CONTRATO,  IC_ESTATUS_OPE,  BI_DOC_CARTERA ,  BI_DOC_CARTERA_PDF ,BI_DOC_BOLETARUG, CG_USUARIO_IF    ) " +
                              "  SELECT ? , Sysdate,  Sysdate, IC_IF , FN_MONTO_SOLICITUD ,  IC_MONEDA ,   CG_DES_RECURSOS ,   DF_PAGO_CAPITAL ,  " +
                              "   DF_PAGO_INTERES,  DF_VENCIMIENTO , CG_TIPO_TASA ,   IC_TASA ,  IN_TASA_ACEPTADA ,  CG_OBSERVACIONES_IF,  BI_DOC_COTIZACION ,  ?, " +
                              "	IC_CONTRATO , IC_ESTATUS_OPE, BI_DOC_CARTERA ,BI_DOC_CARTERA_PDF ,  BI_DOC_BOLETARUG  , CG_USUARIO_IF   " +
                              " FROM  OPETMP_REG_SOLICITUD  WHERE IC_SOLICITUD = ? ");

                lVarBind = new ArrayList();
                lVarBind.add(icSolicitud);
                lVarBind.add(cc_acuse);
                lVarBind.add(ic_solicitud);
                log.debug("SQL.toString() " + strSQL.toString());
                log.debug("varBind " + lVarBind);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();


            } else {
                icSolicitud = ic_solicitud;

                String ic_if = parametros.get(3).toString();
                String montoSolic = parametros.get(7).toString();
                String ic_moneda = parametros.get(8).toString();
                String des_recuersos = parametros.get(9).toString();
                String df_pago_capital = parametros.get(10).toString();
                String df_pago_interes = parametros.get(11).toString();
                String df_vencimiento = parametros.get(12).toString();
                String cg_tipo_tasa = parametros.get(13).toString();
                String cg_tasaInteres = parametros.get(14).toString();
                String observaciones = parametros.get(15).toString();
                String ic_contrato = parametros.get(16).toString();
                //String fechaSolic=  parametros.get(17).toString();
                String loginUsuario = parametros.get(18).toString();
                String OpeFirma = this.getOperaFirmaMancomunada(ic_if);
                String estatus = "";
                if (OpeFirma.equals("S")) {
                    estatus = "7";
                } else {
                    estatus = "1";
                }
                strSQL = new StringBuffer();
                strSQL.append(" UPDATE OPE_REG_SOLICITUD  " + " SET  IC_IF = ? " + " , FN_MONTO_SOLICITUD =   ?  " +
                              " , IC_MONEDA =  ? " + " , CG_DES_RECURSOS =  ? " +
                              " , DF_PAGO_CAPITAL = TO_DATE(?, 'dd/mm/yyyy')  " +
                              " , DF_PAGO_INTERES = TO_DATE(?, 'dd/mm/yyyy')  " +
                              " ,DF_VENCIMIENTO = TO_DATE(?, 'dd/mm/yyyy')  " + " , CG_TIPO_TASA  =  ? " +
                              " , IC_TASA  =?  " + " , IN_TASA_ACEPTADA  =  ? " + " , CG_OBSERVACIONES_IF =? " +
                              " , IC_CONTRATO  =  ? " + " , IC_ESTATUS_OPE = ? " + " , DF_SOLICITUD  = Sysdate " +
                              " , BI_DOC_CARTERA  =  (select BI_DOC_CARTERA   from OPETMP_REG_SOLICITUD    WHERE IC_SOLICITUD = ? ) " +
                              " , BI_DOC_CARTERA_PDF  =  (select BI_DOC_CARTERA_PDF   from OPETMP_REG_SOLICITUD    WHERE IC_SOLICITUD = ? ) " +
                              " , BI_DOC_BOLETARUG   =  (select BI_DOC_BOLETARUG   from OPETMP_REG_SOLICITUD    WHERE IC_SOLICITUD = ? ) " +
                              " , BI_DOC_COTIZACION   =  (select BI_DOC_COTIZACION  from OPETMP_REG_SOLICITUD    WHERE IC_SOLICITUD = ? )  " +
                              " , CG_USUARIO_IF = ?  " + " WHERE IC_SOLICITUD = ? ");
                lVarBind = new ArrayList();
                lVarBind.add(ic_if);
                lVarBind.add(montoSolic);
                lVarBind.add(ic_moneda);
                lVarBind.add(des_recuersos);
                lVarBind.add(df_pago_capital);
                lVarBind.add(df_pago_interes);
                lVarBind.add(df_vencimiento);
                lVarBind.add(cg_tipo_tasa);
                lVarBind.add(combTasa);
                lVarBind.add(cg_tasaInteres);
                lVarBind.add(observaciones);
                lVarBind.add(ic_contrato);
                lVarBind.add(estatus); //
                lVarBind.add(icSolicitud);
                lVarBind.add(icSolicitud);
                lVarBind.add(icSolicitud);
                lVarBind.add(icSolicitud);
                lVarBind.add(loginUsuario);
                lVarBind.add(icSolicitud);

                log.debug("SQL.toString() " + strSQL.toString());
                log.debug("varBind " + lVarBind);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();


                //BORRAR TABLA TEMPORAL DE aMORTIZACIONES
                strSQL = new StringBuffer();
                strSQL.append("DELETE  FROM OPE_TABLA_AMORT  WHERE IC_SOLICITUD = ? ");
                lVarBind = new ArrayList();
                lVarBind.add(icSolicitud);
                log.debug("SQL.toString() " + strSQL.toString());
                log.debug("varBind " + lVarBind);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();


            }


            //Tabla Amortizaciones
            strSQL = new StringBuffer();
            strSQL.append(" INSERT INTO OPE_TABLA_AMORT  ( IC_SOLICITUD, IC_TABLA_AMORT , IN_NUM_PERIODO ,  DF_ALTA  ,  FN_MONTO_AMORT  ) " +
                          " SELECT ?, SEQ_OPE_TABLA_AMORT.NEXTVAL , IN_NUM_PERIODO ,  DF_ALTA  ,  FN_MONTO_AMORT " +
                          " FROM  OPETMP_TABLA_AMORT   " + " WHERE IC_SOLICITUD = ?");

            lVarBind = new ArrayList();
            lVarBind.add(icSolicitud);
            lVarBind.add(ic_solicitud);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();


            //BORRAR TABLA TEMPORAL DE SOLICITUD
            strSQL = new StringBuffer();
            strSQL.append("DELETE  FROM OPETMP_REG_SOLICITUD  WHERE IC_SOLICITUD = ? ");
            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();


            //BORRAR TABLA TEMPORAL DE aMORTIZACIONES
            strSQL = new StringBuffer();
            strSQL.append("DELETE  FROM OPETMP_TABLA_AMORT  WHERE IC_SOLICITUD = ? ");
            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();


        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("getguardarSolicitud  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  getguardarSolicitud (S) ");
            }
        }
        return icSolicitud;
    }

    /**
     *
     * @throws com.netro.exception.NafinException
     * @return
     * @param parametros
     */

    public String getguardarSolicitudCon(List parametros, AccesoDB con) throws NafinException {

        log.info("getguardarSolicitud(E)  ");

        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        String icSolicitud = "";

        try {

            String ic_solicitud = parametros.get(0).toString();
            String nombreUsuario = parametros.get(1).toString();
            String cc_acuse = parametros.get(4).toString();
            String tipoSolicitud = parametros.get(5).toString();
            String combTasa = parametros.get(6).toString();
            String ic_if = parametros.get(3).toString();

            if (tipoSolicitud.equals("Captura")) {

                String query = "select SEQ_OPE_REG_SOLICITUD.NEXTVAL from dual";

                ps = con.queryPrecompilado(query);
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    icSolicitud = rs.getString(1);
                }
                rs.close();
                ps.close();

                //Inserta Acuse
                strSQL = new StringBuffer();
                strSQL.append(" INSERT INTO OPE_ACUSE  ( CC_ACUSE,  DF_FECHAHORA_CARGA ,  CG_NOMBRE_USUARIO ) " +
                              " VALUES( ?, Sysdate, ? ) ");
                lVarBind = new ArrayList();
                lVarBind.add(cc_acuse);
                lVarBind.add(nombreUsuario);
                log.debug("SQL.toString() " + strSQL.toString());
                log.debug("varBind " + lVarBind);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();

                //INSERTA  TABLA ORIGINAL
                strSQL = new StringBuffer();
                strSQL.append(" INSERT INTO OPE_REG_SOLICITUD  " +
                              " ( IC_SOLICITUD, DF_SOLICITUD, DF_ALTA_SOLICITUD, IC_IF , FN_MONTO_SOLICITUD ,  IC_MONEDA ,   CG_DES_RECURSOS ,   DF_PAGO_CAPITAL ,   " +
                              "   DF_PAGO_INTERES,  DF_VENCIMIENTO , CG_TIPO_TASA ,   IC_TASA ,  IN_TASA_ACEPTADA  ,  CG_OBSERVACIONES_IF,  BI_DOC_COTIZACION , " +
                              "   CC_ACUSE, IC_CONTRATO,  IC_ESTATUS_OPE,  BI_DOC_CARTERA ,  BI_DOC_CARTERA_PDF ,BI_DOC_BOLETARUG, CG_USUARIO_IF    ) " +
                              "  SELECT ? , Sysdate,  Sysdate, IC_IF , FN_MONTO_SOLICITUD ,  IC_MONEDA ,   CG_DES_RECURSOS ,   DF_PAGO_CAPITAL ,  " +
                              "   DF_PAGO_INTERES,  DF_VENCIMIENTO , CG_TIPO_TASA ,   IC_TASA ,  IN_TASA_ACEPTADA ,  CG_OBSERVACIONES_IF,  BI_DOC_COTIZACION ,  ?, " +
                              "	IC_CONTRATO , IC_ESTATUS_OPE, BI_DOC_CARTERA ,BI_DOC_CARTERA_PDF ,  BI_DOC_BOLETARUG  , CG_USUARIO_IF   " +
                              " FROM  OPETMP_REG_SOLICITUD  WHERE IC_SOLICITUD = ? AND IC_IF = ? ");

                lVarBind = new ArrayList();
                lVarBind.add(icSolicitud);
                lVarBind.add(cc_acuse);
                lVarBind.add(ic_solicitud);
                lVarBind.add(ic_if);
                log.debug("SQL.toString() " + strSQL.toString());
                log.debug("varBind " + lVarBind);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();


            } else {
                icSolicitud = ic_solicitud;

                String montoSolic = parametros.get(7).toString();
                String ic_moneda = parametros.get(8).toString();
                String des_recuersos = parametros.get(9).toString();
                String df_pago_capital = parametros.get(10).toString();
                String df_pago_interes = parametros.get(11).toString();
                String df_vencimiento = parametros.get(12).toString();
                String cg_tipo_tasa = parametros.get(13).toString();
                String cg_tasaInteres = parametros.get(14).toString();
                String observaciones = parametros.get(15).toString();
                String ic_contrato = parametros.get(16).toString();
                String fechaSolic = parametros.get(17).toString();
                String loginUsuario = parametros.get(18).toString();
                String OpeFirma = this.getOperaFirmaMancomunada(ic_if);
                String estatus = "";
                if (OpeFirma.equals("S")) {
                    estatus = "7";
                } else {
                    estatus = "1";
                }
                strSQL = new StringBuffer();
                strSQL.append(" UPDATE OPE_REG_SOLICITUD  " + " SET  IC_IF = ? " + " , FN_MONTO_SOLICITUD =   ?  " +
                              " , IC_MONEDA =  ? " + " , CG_DES_RECURSOS =  ? " +
                              " , DF_PAGO_CAPITAL = TO_DATE(?, 'dd/mm/yyyy')  " +
                              " , DF_PAGO_INTERES = TO_DATE(?, 'dd/mm/yyyy')  " +
                              " ,DF_VENCIMIENTO = TO_DATE(?, 'dd/mm/yyyy')  " + " , CG_TIPO_TASA  =  ? " +
                              " , IC_TASA  =?  " + " , IN_TASA_ACEPTADA  =  ? " + " , CG_OBSERVACIONES_IF =? " +
                              " , IC_CONTRATO  =  ? " + " , IC_ESTATUS_OPE = ? " + " , DF_SOLICITUD  = Sysdate " +
                              " , BI_DOC_CARTERA  =  (select BI_DOC_CARTERA   from OPETMP_REG_SOLICITUD    WHERE IC_SOLICITUD = ? ) " +
                              " , BI_DOC_CARTERA_PDF  =  (select BI_DOC_CARTERA_PDF   from OPETMP_REG_SOLICITUD    WHERE IC_SOLICITUD = ? ) " +
                              " , BI_DOC_BOLETARUG   =  (select BI_DOC_BOLETARUG   from OPETMP_REG_SOLICITUD    WHERE IC_SOLICITUD = ? ) " +
                              " , BI_DOC_COTIZACION   =  (select BI_DOC_COTIZACION  from OPETMP_REG_SOLICITUD    WHERE IC_SOLICITUD = ? )  " +
                              " , CG_USUARIO_IF = ?  " + " WHERE IC_SOLICITUD = ? ");
                lVarBind = new ArrayList();
                lVarBind.add(ic_if);
                lVarBind.add(montoSolic);
                lVarBind.add(ic_moneda);
                lVarBind.add(des_recuersos);
                lVarBind.add(df_pago_capital);
                lVarBind.add(df_pago_interes);
                lVarBind.add(df_vencimiento);
                lVarBind.add(cg_tipo_tasa);
                lVarBind.add(combTasa);
                lVarBind.add(cg_tasaInteres);
                lVarBind.add(observaciones);
                lVarBind.add(ic_contrato);
                lVarBind.add(estatus); // 															
                lVarBind.add(icSolicitud);
                lVarBind.add(icSolicitud);
                lVarBind.add(icSolicitud);
                lVarBind.add(icSolicitud);
                lVarBind.add(loginUsuario);
                lVarBind.add(icSolicitud);

                log.debug("SQL.toString() " + strSQL.toString());
                log.debug("varBind " + lVarBind);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();


                //BORRAR TABLA TEMPORAL DE aMORTIZACIONES
                strSQL = new StringBuffer();
                strSQL.append("DELETE  FROM OPE_TABLA_AMORT  WHERE IC_SOLICITUD = ? ");
                lVarBind = new ArrayList();
                lVarBind.add(icSolicitud);
                log.debug("SQL.toString() " + strSQL.toString());
                log.debug("varBind " + lVarBind);
                ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                ps.executeUpdate();
                ps.close();


            }


            //Tabla Amortizaciones
            strSQL = new StringBuffer();
            strSQL.append(" INSERT INTO OPE_TABLA_AMORT  ( IC_SOLICITUD, IC_TABLA_AMORT , IN_NUM_PERIODO ,  DF_ALTA  ,  FN_MONTO_AMORT  ) " +
                          " SELECT ?, SEQ_OPE_TABLA_AMORT.NEXTVAL , IN_NUM_PERIODO ,  DF_ALTA  ,  FN_MONTO_AMORT " +
                          " FROM  OPETMP_TABLA_AMORT   " + " WHERE IC_SOLICITUD = ? ");

            lVarBind = new ArrayList();
            lVarBind.add(icSolicitud);
            lVarBind.add(ic_solicitud);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();


            //BORRAR TABLA TEMPORAL DE SOLICITUD
            strSQL = new StringBuffer();
            strSQL.append("DELETE  FROM OPETMP_REG_SOLICITUD  WHERE IC_SOLICITUD = ? AND IC_IF = ? ");
            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            lVarBind.add(ic_if);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();


            //BORRAR TABLA TEMPORAL DE aMORTIZACIONES
            strSQL = new StringBuffer();
            strSQL.append("DELETE  FROM OPETMP_TABLA_AMORT  WHERE IC_SOLICITUD = ? ");
            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();


        } catch (Exception e) {
            System.out.println(" OperacionElectronicaEJB::getguardarSolicitud(Exception): " + e);
            throw new NafinException("SIST0001");
        } finally {
            System.out.println(" OperacionElectronicaEJB::getguardarSolicitud(S)");
        }
        return icSolicitud;
    }


    /**
     * M�todo que obtiene la tabla de Amortizaci�n
     * @throws java.lang.Exception
     * @return
     * @param ic_solicitud
     */
    public List getTablaAmortizacion(String ic_solicitud) {
        log.info("getTablaAmortizacion (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();
        HashMap datos = new HashMap();
        List registros = new ArrayList();

        try {
            con.conexionDB();

            SQL.append(" SELECT  IN_NUM_PERIODO as PERIODO , to_char(DF_ALTA,'dd/mm/yyyy')   AS FECHA ,  FN_MONTO_AMORT AS  AMORTIZACION  " +
                       " FROM  OPE_TABLA_AMORT  " + " WHERE IC_SOLICITUD = ?  ORDER BY IN_NUM_PERIODO ");
            varBind.add(ic_solicitud);

            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);

            ps = con.queryPrecompilado(SQL.toString(), varBind);
            rs = ps.executeQuery();

            while (rs.next()) {
                datos = new HashMap();
                datos.put("PERIODO", rs.getString("PERIODO") == null ? "" : rs.getString("PERIODO"));
                datos.put("FECHA", rs.getString("FECHA") == null ? "" : rs.getString("FECHA"));
                datos.put("AMORTIZACION", rs.getString("AMORTIZACION") == null ? "" : rs.getString("AMORTIZACION"));
                registros.add(datos);
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getTablaAmortizacion " + e);
            throw new AppException("Error al getTablaAmortizacion", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getTablaAmortizacion (S)");
        }
        return registros;
    }


    /**
     * M�todo que obtiene la tabla de Amortizaci�n
     * @throws java.lang.Exception
     * @return
     * @param ic_solicitud
     */
    public List getTablaAmortizacionCon(String ic_solicitud, AccesoDB con) throws NafinException {
        log.info("getTablaAmortizacionCon (E)");

        boolean commit = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();
        HashMap datos = new HashMap();
        List registros = new ArrayList();

        try {

            SQL.append(" SELECT  IN_NUM_PERIODO as PERIODO , to_char(DF_ALTA,'dd/mm/yyyy')   AS FECHA ,  FN_MONTO_AMORT AS  AMORTIZACION  " +
                       " FROM  OPE_TABLA_AMORT  " + " WHERE IC_SOLICITUD = ?  ");
            varBind.add(ic_solicitud);

            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);

            ps = con.queryPrecompilado(SQL.toString(), varBind);
            rs = ps.executeQuery();

            while (rs.next()) {
                datos = new HashMap();
                datos.put("PERIODO", rs.getString("PERIODO") == null ? "" : rs.getString("PERIODO"));
                datos.put("FECHA", rs.getString("FECHA") == null ? "" : rs.getString("FECHA"));
                datos.put("AMORTIZACION", rs.getString("AMORTIZACION") == null ? "" : rs.getString("AMORTIZACION"));
                registros.add(datos);
            }
            rs.close();
            ps.close();


        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(" Error getTablaAmortizacionCon " + e);
            throw new NafinException("Error al getTablaAmortizacionCon");
        } finally {

            log.info("getTablaAmortizacionCon (S)");
        }
        return registros;
    }


    /**
     *
     * @throws com.netro.exception.NafinException
     * @return
     * @param ic_if
     * @param ic_solicitud
     */
    public HashMap getDatosSolicitud(String ic_solicitud, String ic_if) {

        log.info("getDatosSolicitud(E)  ");

        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap registros = new HashMap();

        try {
            con.conexionDB();

            strSQL = new StringBuffer();
            strSQL.append(" SELECT  to_char(S. DF_SOLICITUD , 'dd/mm/yyyy')   as FECHASOLICITUD ,  S.FN_MONTO_SOLICITUD AS IMPORTE,  M.IC_MONEDA AS IC_MONEDA,  M.CD_NOMBRE AS MONEDA , S.CG_DES_RECURSOS  AS RECURSOS, " +
                          " to_char(S.DF_PAGO_CAPITAL, 'dd/mm/yyyy') AS FECHACAPITAL,  to_char(S.DF_PAGO_INTERES, 'dd/mm/yyyy')  AS  FECHAINTERES,  to_char(S.DF_VENCIMIENTO, 'dd/mm/yyyy') AS FECHAVENCIMIENTO,  " +
                          " DECODE(t.CD_NOMBRE ,'', S.CG_TIPO_TASA  || '  ' ||  s.IN_TASA_ACEPTADA ||' % ', S.CG_TIPO_TASA  ||'  '||  t.CD_NOMBRE  || ' + ' ||  s.IN_TASA_ACEPTADA ||' % '  ) TASAINTERES ,  " +
                          " t.IC_TASA AS CLAVE_TASA , " +
                          " S.CG_TIPO_TASA   AS CG_TIPO_TASA ,   S.IN_TASA_ACEPTADA  AS  CG_TASA_INTERES,  " +
                          " CG_OBSERVACIONES_IF AS OBSERVACIONES  " + " ,i.CG_RAZON_SOCIAL AS NOMBRE_IF,  " +
                          " d.IC_NUM_CUENTA  AS NUM_CUENTAS,   d.CG_BANCO AS BANCO,  d.IC_CTA_CLABE as CLABE, " +
                          " c.IC_CONTRATO as IC_CONTRATO , c.CG_NOMBRE_CONTRATO as NOMBRECONTRATO, to_char(d.DF_FIRMA_CONTRATO,'dd/mm/yyyy')  as FECHACONTRATO,   " +
                          " d.CG_CONTRATO_OE as NOMBRECONTRATO_OE, to_char(d.DF_FIRMA_CONTRATO_OE, 'dd/mm/yyyy')  as FECHACONTRATO_OE,  " + " CG_CONT_MODIFICATORIOS  AS MODIFICATORIOS  " +

                          ", a.CC_ACUSE AS ACUSE,  a.CG_NOMBRE_USUARIO AS USUARIO  " + ", to_char(a.DF_FECHAHORA_CARGA,'dd/mm/yyyy')  as FECHACARGA , to_char(a.DF_FECHAHORA_CARGA,'HH24:mi:ss')  as HORACARGA " +

                          ", to_char(S.DF_ALTA_SOLICITUD , 'dd/mm/yyyy')   as FECHAALTA_SOLICITUD  " +

                          " FROM OPE_REG_SOLICITUD S, COMCAT_MONEDA M , " + " OPE_DATOS_IF d,   comcat_if i , " +
                          "  OPE_ACUSE a , OPECAT_CONTRATO c,  " + " comcat_tasa t  " +

                          " WHERE S.IC_SOLICITUD =  ? " + " AND S.IC_MONEDA = M.IC_MONEDA   " +
                          " and d.ic_if = i.ic_if  " + " and s.ic_if = d.ic_if  " +
                          " and s.ic_contrato = c.ic_contrato " + " and d.ic_if  = ?  " +
                          " and s.cc_acuse = a.cc_acuse " + " and s.ic_tasa = t.ic_tasa(+) ");

            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            lVarBind.add(ic_if);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);

            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            rs = ps.executeQuery();

            if (rs.next()) {
                registros.put("FECHASOLICITUD",
                              rs.getString("FECHASOLICITUD") == null ? "" : rs.getString("FECHASOLICITUD"));
                registros.put("IMPORTE", rs.getString("IMPORTE") == null ? "" : rs.getString("IMPORTE"));
                registros.put("IC_MONEDA", rs.getString("IC_MONEDA") == null ? "" : rs.getString("IC_MONEDA"));
                registros.put("MONEDA", rs.getString("MONEDA") == null ? "" : rs.getString("MONEDA"));
                registros.put("RECURSOS", rs.getString("RECURSOS") == null ? "" : rs.getString("RECURSOS"));
                registros.put("FECHACAPITAL", rs.getString("FECHACAPITAL") == null ? "" : rs.getString("FECHACAPITAL"));
                registros.put("FECHAINTERES", rs.getString("FECHAINTERES") == null ? "" : rs.getString("FECHAINTERES"));
                registros.put("FECHAVENCIMIENTO",
                              rs.getString("FECHAVENCIMIENTO") == null ? "" : rs.getString("FECHAVENCIMIENTO"));
                registros.put("TASAINTERES", rs.getString("TASAINTERES") == null ? "" : rs.getString("TASAINTERES"));
                registros.put("CG_TIPO_TASA", rs.getString("CG_TIPO_TASA") == null ? "" : rs.getString("CG_TIPO_TASA"));
                registros.put("CG_TASA_INTERES",
                              rs.getString("CG_TASA_INTERES") == null ? "" : rs.getString("CG_TASA_INTERES"));
                registros.put("CLAVE_TASA", rs.getString("CLAVE_TASA") == null ? "" : rs.getString("CLAVE_TASA"));
                registros.put("OBSERVACIONES",
                              rs.getString("OBSERVACIONES") == null ? "" : rs.getString("OBSERVACIONES"));
                registros.put("NOMBRE_IF", rs.getString("NOMBRE_IF") == null ? "" : rs.getString("NOMBRE_IF"));
                registros.put("NUM_CUENTAS", rs.getString("NUM_CUENTAS") == null ? "" : rs.getString("NUM_CUENTAS"));
                registros.put("BANCO", rs.getString("BANCO") == null ? "" : rs.getString("BANCO"));
                registros.put("CLABE", rs.getString("CLABE") == null ? "" : rs.getString("CLABE"));
                registros.put("IC_CONTRATO", rs.getString("IC_CONTRATO") == null ? "" : rs.getString("IC_CONTRATO"));
                registros.put("NOMBRECONTRATO",
                              rs.getString("NOMBRECONTRATO") == null ? "" : rs.getString("NOMBRECONTRATO"));
                registros.put("FECHACONTRATO",
                              rs.getString("FECHACONTRATO") == null ? "" : rs.getString("FECHACONTRATO"));
                registros.put("NOMBRECONTRATO_OE",
                              rs.getString("NOMBRECONTRATO_OE") == null ? "" : rs.getString("NOMBRECONTRATO_OE"));
                registros.put("FECHACONTRATO_OE",
                              rs.getString("FECHACONTRATO_OE") == null ? "" : rs.getString("FECHACONTRATO_OE"));
                registros.put("MODIFICATORIOS",
                              rs.getString("MODIFICATORIOS") == null ? "" : rs.getString("MODIFICATORIOS"));
                registros.put("ACUSE", rs.getString("ACUSE") == null ? "" : rs.getString("ACUSE"));
                registros.put("USUARIO", rs.getString("USUARIO") == null ? "" : rs.getString("USUARIO"));
                registros.put("FECHACARGA", rs.getString("FECHACARGA") == null ? "" : rs.getString("FECHACARGA"));
                registros.put("HORACARGA", rs.getString("HORACARGA") == null ? "" : rs.getString("HORACARGA"));
                registros.put("FECHAALTA_SOLICITUD",
                              rs.getString("FECHAALTA_SOLICITUD") == null ? "" : rs.getString("FECHAALTA_SOLICITUD"));

            }
            rs.close();
            ps.close();

        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("getDatosSolicitud  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  getDatosSolicitud (S) ");
            }
        }
        return registros;
    }

    /**
     *
     * @throws com.netro.exception.NafinException
     * @return
     * @param ic_if
     * @param ic_solicitud
     */
    public HashMap getDatosSolicitudCon(String ic_solicitud, String ic_if, AccesoDB con) throws NafinException {

        log.info("getDatosSolicitud(E)  ");

        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        HashMap registros = new HashMap();

        try {

            strSQL = new StringBuffer();
            strSQL.append(" SELECT  to_char(S. DF_SOLICITUD , 'dd/mm/yyyy')   as FECHASOLICITUD ,  S.FN_MONTO_SOLICITUD AS IMPORTE,  M.IC_MONEDA AS IC_MONEDA,  M.CD_NOMBRE AS MONEDA , S.CG_DES_RECURSOS  AS RECURSOS, " +
                          " to_char(S.DF_PAGO_CAPITAL, 'dd/mm/yyyy') AS FECHACAPITAL,  to_char(S.DF_PAGO_INTERES, 'dd/mm/yyyy')  AS  FECHAINTERES,  to_char(S.DF_VENCIMIENTO, 'dd/mm/yyyy') AS FECHAVENCIMIENTO,  " +
                          " DECODE(t.CD_NOMBRE ,'', S.CG_TIPO_TASA  || '  ' ||  s.IN_TASA_ACEPTADA ||' % ', S.CG_TIPO_TASA  ||'  '||  t.CD_NOMBRE  || ' + ' ||  s.IN_TASA_ACEPTADA ||' % '  ) TASAINTERES ,  " +
                          " t.IC_TASA AS CLAVE_TASA , " +
                          " S.CG_TIPO_TASA   AS CG_TIPO_TASA ,   S.IN_TASA_ACEPTADA  AS  CG_TASA_INTERES,  " +
                          " CG_OBSERVACIONES_IF AS OBSERVACIONES  " + " ,i.CG_RAZON_SOCIAL AS NOMBRE_IF,  " +
                          " d.IC_NUM_CUENTA  AS NUM_CUENTAS,   d.CG_BANCO AS BANCO,  d.IC_CTA_CLABE as CLABE, " +
                          " c.IC_CONTRATO as IC_CONTRATO , c.CG_NOMBRE_CONTRATO as NOMBRECONTRATO, to_char(d.DF_FIRMA_CONTRATO,'dd/mm/yyyy')  as FECHACONTRATO,   " +
                          " d.CG_CONTRATO_OE as NOMBRECONTRATO_OE, to_char(d.DF_FIRMA_CONTRATO_OE, 'dd/mm/yyyy')  as FECHACONTRATO_OE,  " + " CG_CONT_MODIFICATORIOS  AS MODIFICATORIOS  " +

                          ", a.CC_ACUSE AS ACUSE,  a.CG_NOMBRE_USUARIO AS USUARIO  " + ", to_char(a.DF_FECHAHORA_CARGA,'dd/mm/yyyy')  as FECHACARGA , to_char(a.DF_FECHAHORA_CARGA,'HH24:mi:ss')  as HORACARGA " +

                          ", to_char(S.DF_ALTA_SOLICITUD , 'dd/mm/yyyy')   as FECHAALTA_SOLICITUD  " +

                          " FROM OPE_REG_SOLICITUD S, COMCAT_MONEDA M , " + " OPE_DATOS_IF d,   comcat_if i , " +
                          "  OPE_ACUSE a , OPECAT_CONTRATO c,  " + " comcat_tasa t  " +

                          " WHERE S.IC_SOLICITUD =  ? " + " AND S.IC_MONEDA = M.IC_MONEDA   " +
                          " and d.ic_if = i.ic_if  " + " and s.ic_if = d.ic_if  " +
                          " and s.ic_contrato = c.ic_contrato " + " and d.ic_if  = ?  " +
                          " and s.cc_acuse = a.cc_acuse " + " and s.ic_tasa = t.ic_tasa(+) ");

            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            lVarBind.add(ic_if);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);

            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            rs = ps.executeQuery();

            if (rs.next()) {
                registros.put("FECHASOLICITUD",
                              rs.getString("FECHASOLICITUD") == null ? "" : rs.getString("FECHASOLICITUD"));
                registros.put("IMPORTE", rs.getString("IMPORTE") == null ? "" : rs.getString("IMPORTE"));
                registros.put("IC_MONEDA", rs.getString("IC_MONEDA") == null ? "" : rs.getString("IC_MONEDA"));
                registros.put("MONEDA", rs.getString("MONEDA") == null ? "" : rs.getString("MONEDA"));
                registros.put("RECURSOS", rs.getString("RECURSOS") == null ? "" : rs.getString("RECURSOS"));
                registros.put("FECHACAPITAL", rs.getString("FECHACAPITAL") == null ? "" : rs.getString("FECHACAPITAL"));
                registros.put("FECHAINTERES", rs.getString("FECHAINTERES") == null ? "" : rs.getString("FECHAINTERES"));
                registros.put("FECHAVENCIMIENTO",
                              rs.getString("FECHAVENCIMIENTO") == null ? "" : rs.getString("FECHAVENCIMIENTO"));
                registros.put("TASAINTERES", rs.getString("TASAINTERES") == null ? "" : rs.getString("TASAINTERES"));
                registros.put("CG_TIPO_TASA", rs.getString("CG_TIPO_TASA") == null ? "" : rs.getString("CG_TIPO_TASA"));
                registros.put("CG_TASA_INTERES",
                              rs.getString("CG_TASA_INTERES") == null ? "" : rs.getString("CG_TASA_INTERES"));
                registros.put("CLAVE_TASA", rs.getString("CLAVE_TASA") == null ? "" : rs.getString("CLAVE_TASA"));
                registros.put("OBSERVACIONES",
                              rs.getString("OBSERVACIONES") == null ? "" : rs.getString("OBSERVACIONES"));
                registros.put("NOMBRE_IF", rs.getString("NOMBRE_IF") == null ? "" : rs.getString("NOMBRE_IF"));
                registros.put("NUM_CUENTAS", rs.getString("NUM_CUENTAS") == null ? "" : rs.getString("NUM_CUENTAS"));
                registros.put("BANCO", rs.getString("BANCO") == null ? "" : rs.getString("BANCO"));
                registros.put("CLABE", rs.getString("CLABE") == null ? "" : rs.getString("CLABE"));
                registros.put("IC_CONTRATO", rs.getString("IC_CONTRATO") == null ? "" : rs.getString("IC_CONTRATO"));
                registros.put("NOMBRECONTRATO",
                              rs.getString("NOMBRECONTRATO") == null ? "" : rs.getString("NOMBRECONTRATO"));
                registros.put("FECHACONTRATO",
                              rs.getString("FECHACONTRATO") == null ? "" : rs.getString("FECHACONTRATO"));
                registros.put("NOMBRECONTRATO_OE",
                              rs.getString("NOMBRECONTRATO_OE") == null ? "" : rs.getString("NOMBRECONTRATO_OE"));
                registros.put("FECHACONTRATO_OE",
                              rs.getString("FECHACONTRATO_OE") == null ? "" : rs.getString("FECHACONTRATO_OE"));
                registros.put("MODIFICATORIOS",
                              rs.getString("MODIFICATORIOS") == null ? "" : rs.getString("MODIFICATORIOS"));
                registros.put("ACUSE", rs.getString("ACUSE") == null ? "" : rs.getString("ACUSE"));
                registros.put("USUARIO", rs.getString("USUARIO") == null ? "" : rs.getString("USUARIO"));
                registros.put("FECHACARGA", rs.getString("FECHACARGA") == null ? "" : rs.getString("FECHACARGA"));
                registros.put("HORACARGA", rs.getString("HORACARGA") == null ? "" : rs.getString("HORACARGA"));
                registros.put("FECHAALTA_SOLICITUD",
                              rs.getString("FECHAALTA_SOLICITUD") == null ? "" : rs.getString("FECHAALTA_SOLICITUD"));

            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            exito = false;
            e.printStackTrace();
            System.out.println(" OperacionElectronicaEJB::getDatosSolicitud(Exception): " + e);
            throw new NafinException("SIST0001");
        } finally {

            System.out.println(" OperacionElectronicaEJB::getDatosSolicitud(S)");
        }
        return registros;
    }

    /**
     * Generaci�n del Archivo del Acuse
     * @throws java.lang.Exception
     * @return
     * @param parametros
     * @param request
     */

    public String generarArchivoPDF(List parametros) {
        log.info("  generarArchivoPDF (E) ");
        String nombreArchivo = "";
        ComunesPDF pdfDoc = new ComunesPDF();
        String path = "", ic_solicitud = "", ic_if = "", strPais = "", iNoNafinElectronico = "", sesExterno =
            "", strNombre = "", strNombreUsuario = "", strLogo = "", strDirectorioPublicacion = "", fondoLiquido =
            "", strPerfil = "";

        StringBuffer mensajeModifi = new StringBuffer();

        try {

            path = parametros.get(0).toString();
            ic_solicitud = parametros.get(1).toString();
            ic_if = parametros.get(2).toString();
            strPais = parametros.get(3).toString();
            iNoNafinElectronico = parametros.get(4).toString();
            strNombre = parametros.get(5).toString();
            strNombreUsuario = parametros.get(6).toString();
            strLogo = parametros.get(7).toString();
            strDirectorioPublicacion = parametros.get(8).toString();
            fondoLiquido = parametros.get(9).toString();
            strPerfil = parametros.get(10).toString();
            //strTipoUsr =  parametros.get(11).toString();

            //Consulta para obtener los datos de la solicitud
            HashMap registros = this.getDatosSolicitud(ic_solicitud, ic_if);
            List firmas = this.getFirmasModificatorias(ic_if);

            nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
            pdfDoc = new ComunesPDF(2, path + nombreArchivo);

            String meses[] = {
                "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
                "Noviembre", "Diciembre"
            };
            String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual = fechaActual.substring(0, 2);
            String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
            String anioActual = fechaActual.substring(6, 10);
            String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

            pdfDoc.encabezadoConImagenes(pdfDoc, strPais, iNoNafinElectronico, sesExterno, strNombre, strNombreUsuario,
                                         strLogo, strDirectorioPublicacion);
            pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
                           " ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);

            pdfDoc.addText(" La Solicitud se envio con �xito.", "formas", ComunesPDF.CENTER);
            pdfDoc.addText(" Acuse de Solicitud " + registros.get("ACUSE").toString(), "formas", ComunesPDF.CENTER);

            //---------------------Acuse -------------------------
            pdfDoc.setTable(2, 50);
            pdfDoc.setCell("Importe ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell("$" + Comunes.formatoDecimal(String.valueOf(registros.get("IMPORTE")), 2), "formas",
                           ComunesPDF.LEFT);
            pdfDoc.setCell("Moneda ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("MONEDA").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Fecha de Env�o de Solicitud ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHACARGA").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Hora de Env�o  ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("HORACARGA").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Usuario IF  ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("USUARIO").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.addTable();

            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
            mensajeModifi.append(" La presente solicitud de recursos se realiza al amparo del  " +
                                 registros.get("NOMBRECONTRATO") + " con fecha de firma" +
                                 registros.get("FECHACONTRATO"));
            int x = 1;
            if (firmas.size() > 0) {
                mensajeModifi.append(" asi como,   ");
                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);
                    mensajeModifi.append(" Contrato Modificatorio " + x + " con fecha de firma " +
                                         datos.get("FECHA").toString() + ", ");
                    x++;
                }
            }
            mensajeModifi.append(" y el Contrato de Operaci�n Electr�nica con fecha de firma " +
                                 registros.get("FECHACONTRATO_OE") + ", celebrados entre  " +
                                 registros.get("NOMBRE_IF") + "  y Nacional Financiera, S.N.C.");


            pdfDoc.addText(mensajeModifi.toString(), "formas", ComunesPDF.JUSTIFIED);
            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
            pdfDoc.addText("Con fundamento en lo dispuesto por los art�culos 1205 y 1298-A del C�digo de Comercio y del 52 de la ley de Instituciones de Cr�dito , la informaci�n e instrucciones que el Intermediario y Nacional Financiera, S.N.C. se transmitan o comuniquen mutuamente mediante el sistema, tendr�n pleno valor probatorio y fuerza legal para acreditar la operaci�n realizada, el importe de la misma, su naturaleza; as� como, las caracter�sticas y alcance de sus instrucciones.",
                           "formas", ComunesPDF.JUSTIFIED);


            if (strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP")) {
                pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
                pdfDoc.addText("Bajo protesta de decir verdad, " + registros.get("NOMBRE_IF") +
                               " manifiesta que la informaci�n que env�a en archivo electr�nico mediante la herramienta 'Carga Cartera en Prenda', corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN " +
                               " que garantiza la presente disposici�n de recursos del cr�dito, formalizada en t�rminos del Contrato de L�nea de Cr�dito en Cuenta Corriente celebrado con NAFIN. ",
                               "formas", ComunesPDF.JUSTIFIED);
                /* 01/03/2017 IHJ
				pdfDoc.addText("Bajo protesta de decir verdad, "+registros.get("NOMBRE_IF")+" manifiesta que la informaci�n que env�a en archivo electr�nico mediante la herramienta 'Carga Cartera en Prenda' corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN  que garantiza la presente disposici�n de recursos del cr�dito. En t�rminos del Contrato de L�nea de Cr�dito en Cuenta Corriente celebrado con NAFIN, la ACREDITADA se obliga a enviar a NAFIN ,de manera f�sica "+
				" y debidamente firmada, la informaci�n correspondiente a la cartera crediticia otorgada en prenda a favor de NAFIN, dentro de los siguientes 5 d�as h�biles posteriores a la presente solicitud electr�nica de disposici�n de recursos. ","formas",ComunesPDF.JUSTIFIED);
				*/

            }

            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);

            //---------------Datos de la Solicitud ----------------------
            pdfDoc.setTable(2, 80);
            pdfDoc.setCell("Fecha Solicitud :", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHASOLICITUD").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Nombre IF: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("NOMBRE_IF").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("N�mero de Cuenta:  ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("NUM_CUENTAS").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Banco: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("BANCO").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Cuenta CLABE: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("CLABE").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Contratos ", "celda02", ComunesPDF.LEFT, 2);

            pdfDoc.setCell("Nombre Contrato: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("NOMBRECONTRATO").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Fecha Firma Contrato: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHACONTRATO").toString(), "formas", ComunesPDF.LEFT);

            firmas = this.getFirmasModificatorias(ic_if); //obtengo las firmas Modificatorias
            if (firmas.size() > 0) {

                pdfDoc.setCell("Contratos Modificatorios: ", "celda02", ComunesPDF.LEFT);
                pdfDoc.setCell(registros.get("MODIFICATORIOS").toString(), "formas", ComunesPDF.LEFT);

                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);
                    String descripcion = datos.get("DESCRIPCION").toString();
                    String fecha = datos.get("FECHA").toString();

                    pdfDoc.setCell(descripcion + ":", "celda02", ComunesPDF.LEFT);
                    pdfDoc.setCell(fecha, "formas", ComunesPDF.LEFT);
                }
            }

            pdfDoc.setCell("Nombre Contrato: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("NOMBRECONTRATO_OE").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Fecha Firma Contrato Operaci�n Electr�nica: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHACONTRATO_OE").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Datos del Pr�stamo ", "celda02", ComunesPDF.LEFT, 2);
            pdfDoc.setCell("Destino de los Recursos: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("RECURSOS").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Fecha de Primer Pago de Capital: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHACAPITAL").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Fecha de Primer Pago de Intereses: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHAINTERES").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Fecha de Vencimiento: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHAVENCIMIENTO").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Tasa de Interes: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("TASAINTERES").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Observaciones: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("OBSERVACIONES").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Usuario IF: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(strNombreUsuario, "formas", ComunesPDF.LEFT);

            if (strPerfil.equals("IF 5MIC") || strPerfil.equals("IF 5CP") || strPerfil.equals("IF 4MIC")) {
                pdfDoc.setCell("Fondo L�quido: ", "celda02", ComunesPDF.LEFT);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(fondoLiquido, 2), "formas", ComunesPDF.LEFT);
            }

            pdfDoc.addTable();

            //------------------------- Tabla de Amortizaciones -------------------------
            List regTabla = this.getTablaAmortizacion(ic_solicitud);
            pdfDoc.setTable(3, 50);
            pdfDoc.setCell("Periodo ", "celda02", ComunesPDF.CENTER);
            pdfDoc.setCell("Fecha :", "celda02", ComunesPDF.CENTER);
            pdfDoc.setCell("Amortizaci�n", "celda02", ComunesPDF.CENTER);
            for (int i = 0; i < regTabla.size(); i++) {
                HashMap datos = (HashMap) regTabla.get(i);
                pdfDoc.setCell(datos.get("PERIODO").toString(), "formas", ComunesPDF.CENTER);
                pdfDoc.setCell(datos.get("FECHA").toString(), "formas", ComunesPDF.CENTER);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(String.valueOf(datos.get("AMORTIZACION").toString()), 2),
                               "formas", ComunesPDF.CENTER);
            }
            pdfDoc.addTable();
            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
            pdfDoc.addText("La presente solicitud de recursos estar� sujeta a validaci�n por parte de Nacional Financiera, S.N.C. para su autorizaci�n. ",
                           "formas", ComunesPDF.CENTER);


            pdfDoc.endDocument();

        } catch (Throwable e) {
            log.error(" Error  generarArchivoPDF (S) " + e);
            throw new AppException("Error al generar el archivo ", e);
        } finally {
            try {
            } catch (Exception e) {
            }
            log.info("  generarArchivoPDF (S) ");
        }
        return nombreArchivo;
    }

    /**
     * Generaci�n del Archivo del Acuse
     * @throws java.lang.Exception
     * @return
     * @param parametros
     * @param request
     */

    public String generarArchivoPDFCon(List parametros, AccesoDB con) throws NafinException {
        log.info("  generarArchivoPDF (E) ");
        String nombreArchivo = "";
        ComunesPDF pdfDoc = new ComunesPDF();
        CreaArchivo creaArchivo = new CreaArchivo();
        String path = "", ic_solicitud = "", ic_if = "", strPais = "", iNoNafinElectronico = "", sesExterno =
            "", strNombre = "", strNombreUsuario = "", strLogo = "", strDirectorioPublicacion = "", fondoLiquido =
            "", strPerfil = "", strTipoUsr = "";

        StringBuffer mensajeModifi = new StringBuffer();

        try {

            path = parametros.get(0).toString();
            ic_solicitud = parametros.get(1).toString();
            ic_if = parametros.get(2).toString();
            strPais = parametros.get(3).toString();
            iNoNafinElectronico = parametros.get(4).toString();
            strNombre = parametros.get(5).toString();
            strNombreUsuario = parametros.get(6).toString();
            strLogo = parametros.get(7).toString();
            strDirectorioPublicacion = parametros.get(8).toString();
            fondoLiquido = parametros.get(9).toString();
            strPerfil = parametros.get(10).toString();
            //strTipoUsr =  parametros.get(11).toString();	

            //Consulta para obtener los datos de la solicitud 		
            HashMap registros = this.getDatosSolicitudCon(ic_solicitud, ic_if, con);
            List firmas = this.getFirmasModificatoriasCon(ic_if, con);

            nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
            pdfDoc = new ComunesPDF(2, path + nombreArchivo);

            String meses[] = {
                "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
                "Noviembre", "Diciembre"
            };
            String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual = fechaActual.substring(0, 2);
            String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
            String anioActual = fechaActual.substring(6, 10);
            String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

            pdfDoc.encabezadoConImagenes(pdfDoc, strPais, iNoNafinElectronico, sesExterno, strNombre, strNombreUsuario,
                                         strLogo, strDirectorioPublicacion);
            pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
                           " ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);

            pdfDoc.addText(" La Solicitud se envio con �xito.", "formas", ComunesPDF.CENTER);
            pdfDoc.addText(" Acuse de Solicitud " + registros.get("ACUSE").toString(), "formas", ComunesPDF.CENTER);

            //---------------------Acuse -------------------------
            pdfDoc.setTable(2, 50);
            pdfDoc.setCell("Importe ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell("$" + Comunes.formatoDecimal(String.valueOf(registros.get("IMPORTE")), 2), "formas",
                           ComunesPDF.LEFT);
            pdfDoc.setCell("Moneda ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("MONEDA").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Fecha de Env�o de Solicitud ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHACARGA").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Hora de Env�o  ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("HORACARGA").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Usuario IF  ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("USUARIO").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.addTable();

            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
            mensajeModifi.append(" La presente solicitud de recursos se realiza al amparo del  " +
                                 registros.get("NOMBRECONTRATO") + " con fecha de firma" +
                                 registros.get("FECHACONTRATO"));
            int x = 1;
            if (firmas.size() > 0) {
                mensajeModifi.append(" asi como,   ");
                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);
                    mensajeModifi.append(" Contrato Modificatorio " + x + " con fecha de firma " +
                                         datos.get("FECHA").toString() + ", ");
                    x++;
                }
            }
            mensajeModifi.append(" y el Contrato de Operaci�n Electr�nica con fecha de firma " +
                                 registros.get("FECHACONTRATO_OE") + ", celebrados entre  " +
                                 registros.get("NOMBRE_IF") + "  y Nacional Financiera, S.N.C.");


            pdfDoc.addText(mensajeModifi.toString(), "formas", ComunesPDF.JUSTIFIED);
            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
            pdfDoc.addText("Con fundamento en lo dispuesto por los art�culos 1205 y 1298-A del C�digo de Comercio y del 52 de la ley de Instituciones de Cr�dito , la informaci�n e instrucciones que el Intermediario y Nacional Financiera, S.N.C. se transmitan o comuniquen mutuamente mediante el sistema, tendr�n pleno valor probatorio y fuerza legal para acreditar la operaci�n realizada, el importe de la misma, su naturaleza; as� como, las caracter�sticas y alcance de sus instrucciones.",
                           "formas", ComunesPDF.JUSTIFIED);


            if (strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP")) {
                pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
                pdfDoc.addText("Bajo protesta de decir verdad, (nombre del IF) manifiesta que la informaci�n que env�a en archivo electr�nico mediante la herramienta 'Carga Cartera en Prenda', corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN " +
                               " que garantiza la presente disposici�n de recursos del cr�dito, formalizada en t�rminos del Contrato de L�nea de Cr�dito en Cuenta Corriente celebrado con NAFIN. ",
                               "formas", ComunesPDF.JUSTIFIED);
                /* 01/03/2017 IHJ
				pdfDoc.addText("Bajo protesta de decir verdad, " +registros.get("NOMBRE_IF") + "manifiesta que la informaci�n que env�a en archivo electr�nico mediante la herramienta 'Carga Cartera en Prenda' corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN  que garantiza la presente disposici�n de recursos del cr�dito. En t�rminos del Contrato de L�nea de Cr�dito en Cuenta Corriente celebrado con NAFIN, la ACREDITADA se obliga a enviar a NAFIN ,de manera f�sica "+
				" y debidamente firmada, la informaci�n correspondiente a la cartera crediticia otorgada en prenda a favor de NAFIN, dentro de los siguientes 5 d�as h�biles posteriores a la presente solicitud electr�nica de disposici�n de recursos. ","formas",ComunesPDF.JUSTIFIED);	
				*/
            }

            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);

            //---------------Datos de la Solicitud ----------------------
            pdfDoc.setTable(2, 80);
            pdfDoc.setCell("Fecha Solicitud :", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHASOLICITUD").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Nombre IF: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("NOMBRE_IF").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("N�mero de Cuenta:  ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("NUM_CUENTAS").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Banco: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("BANCO").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Cuenta CLABE: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("CLABE").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Contratos ", "celda02", ComunesPDF.LEFT, 2);

            pdfDoc.setCell("Nombre Contrato: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("NOMBRECONTRATO").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Fecha Firma Contrato: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHACONTRATO").toString(), "formas", ComunesPDF.LEFT);

            firmas = this.getFirmasModificatoriasCon(ic_if, con); //obtengo las firmas Modificatorias
            if (firmas.size() > 0) {

                pdfDoc.setCell("Contratos Modificatorios: ", "celda02", ComunesPDF.LEFT);
                pdfDoc.setCell(registros.get("MODIFICATORIOS").toString(), "formas", ComunesPDF.LEFT);

                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);
                    String descripcion = datos.get("DESCRIPCION").toString();
                    String fecha = datos.get("FECHA").toString();

                    pdfDoc.setCell(descripcion + ":", "celda02", ComunesPDF.LEFT);
                    pdfDoc.setCell(fecha, "formas", ComunesPDF.LEFT);
                }
            }

            pdfDoc.setCell("Nombre Contrato: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("NOMBRECONTRATO_OE").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Fecha Firma Contrato Operaci�n Electr�nica: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHACONTRATO_OE").toString(), "formas", ComunesPDF.LEFT);

            pdfDoc.setCell("Datos del Pr�stamo ", "celda02", ComunesPDF.LEFT, 2);
            pdfDoc.setCell("Destino de los Recursos: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("RECURSOS").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Fecha de Primer Pago de Capital: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHACAPITAL").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Fecha de Primer Pago de Intereses: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHAINTERES").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Fecha de Vencimiento: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("FECHAVENCIMIENTO").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Tasa de Interes: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("TASAINTERES").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Observaciones: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(registros.get("OBSERVACIONES").toString(), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Usuario IF: ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(strNombreUsuario, "formas", ComunesPDF.LEFT);

            if (strPerfil.equals("IF 5MIC") || strPerfil.equals("IF 5CP") || strPerfil.equals("IF 4MIC")) {
                pdfDoc.setCell("Fondo L�quido: ", "celda02", ComunesPDF.LEFT);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(fondoLiquido, 2), "formas", ComunesPDF.LEFT);
            }

            pdfDoc.addTable();

            //------------------------- Tabla de Amortizaciones -------------------------
            List regTabla = this.getTablaAmortizacionCon(ic_solicitud, con);
            pdfDoc.setTable(3, 50);
            pdfDoc.setCell("Periodo ", "celda02", ComunesPDF.CENTER);
            pdfDoc.setCell("Fecha :", "celda02", ComunesPDF.CENTER);
            pdfDoc.setCell("Amortizaci�n", "celda02", ComunesPDF.CENTER);
            for (int i = 0; i < regTabla.size(); i++) {
                HashMap datos = (HashMap) regTabla.get(i);
                pdfDoc.setCell(datos.get("PERIODO").toString(), "formas", ComunesPDF.CENTER);
                pdfDoc.setCell(datos.get("FECHA").toString(), "formas", ComunesPDF.CENTER);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(String.valueOf(datos.get("AMORTIZACION").toString()), 2),
                               "formas", ComunesPDF.CENTER);
            }
            pdfDoc.addTable();
            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
            pdfDoc.addText("La presente solicitud de recursos estar� sujeta a validaci�n por parte de Nacional Financiera, S.N.C. para su autorizaci�n. ",
                           "formas", ComunesPDF.CENTER);


            pdfDoc.endDocument();

        } catch (Throwable e) {
            log.error(" Error  generarArchivoPDF (S) ", e);
            throw new AppException("Error al generar el archivo ", e);
        } finally {
            try {
            } catch (Exception e) {
            }
            log.info("  generarArchivoPDF (S) ");
        }
        return nombreArchivo;
    }

    /**
     *  Guardar el archivo que se genera en el acuse en la base
     * @throws com.netro.exception.NafinException
     * @return
     * @param rutaArchivo
     * @param ic_solicitud
     */
    public boolean guardarArchSolic(List parametros) {
        log.info("guardarArchSolic (E)");

        AccesoDB con = new AccesoDB();
        boolean exito = true;
        PreparedStatement ps = null;
        String strSQL = "";


        try {
            con.conexionDB();

            String rutaArchivo = parametros.get(0).toString();
            String ic_solicitud = parametros.get(1).toString();

            String nombreArchivo = generarArchivoPDF(parametros);


            File archivo = new File(rutaArchivo + nombreArchivo);
            FileInputStream fileinputstream = new FileInputStream(archivo);


            strSQL = " UPDATE OPE_REG_SOLICITUD " + " SET BI_DOC_ACUSE_SOLIC  = ? " + " WHERE IC_SOLICITUD = ? ";
            log.debug("strSQL " + strSQL);
            ps = con.queryPrecompilado(strSQL);
            ps.setBinaryStream(1, fileinputstream, (int) archivo.length());
            ps.setString(2, ic_solicitud);
            ps.executeUpdate();
            ps.close();


        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("guardarCotizacion  " + e);
            throw new AppException("SIST0001");

        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.debug("  guardarArchSolic (S) ");
            }
        }
        return exito;
    }

    /**
     *  Guardar el archivo que se genera en el acuse en la base
     * @throws com.netro.exception.NafinException
     * @return
     * @param rutaArchivo
     * @param ic_solicitud
     */
    public boolean guardarArchSolicCon(List parametros, AccesoDB con) throws NafinException {
        log.info("guardarArchSolic (E)");

        boolean exito = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        byte abyte0[] = new byte[4096];
        java.io.OutputStream outstream = null;
        String strSQL = "";

        try {
            String rutaArchivo = parametros.get(0).toString();
            String ic_solicitud = parametros.get(1).toString();
            String nombreArchivo = generarArchivoPDFCon(parametros, con);

            File archivo = new File(rutaArchivo + nombreArchivo);
            FileInputStream fileinputstream = new FileInputStream(archivo);


            strSQL = " UPDATE OPE_REG_SOLICITUD " + " SET BI_DOC_ACUSE_SOLIC  = ? " + " WHERE IC_SOLICITUD = ? ";
            log.debug("strSQL " + strSQL);
            ps = con.queryPrecompilado(strSQL);
            ps.setBinaryStream(1, fileinputstream, (int) archivo.length());
            ps.setString(2, ic_solicitud);
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(" OperacionElectronicaEJB::getguardarSolicitud(Exception): " + e);
            throw new NafinException("SIST0001");
        } finally {
            System.out.println(" OperacionElectronicaEJB::getguardarSolicitud(S)");
        }
        return exito;
    }

    /**
     * Obtengo los correos parametrizados
     * @throws java.lang.Exception
     * @return
     * @param parametros
     */
    private String getConcentrador() {
        log.info("getConcentrador (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        String loginUsuario = "";

        try {

            con.conexionDB();
            SQL.append(" SELECT CG_CONCENTRADOR  as USUARIO FROM OPE_PARAMGENERALES WHERE ic_email = 1");

            log.debug("SQL.toString() " + SQL.toString());

            PreparedStatement ps = con.queryPrecompilado(SQL.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                loginUsuario = rs.getString("USUARIO") == null ? "" : rs.getString("USUARIO");
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getConcentrador " + e);
            throw new AppException("Error al getConcentrador", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getConcentrador (S)");
        }
        return loginUsuario;
    }

    /**
     *
     * @throws netropology.utilerias.AppException
     * @return
     * @param ic_if
     * @param ic_solicitud
     */
    public void envioCorreo(List parametros, String ic_solicitud) {
        log.info("envioCorreo(E)");

        Correo correo = new Correo();
        StringBuffer contenido = new StringBuffer();
        UtilUsr utilUsr = new UtilUsr();

        try {

            String strNombreUsuario = parametros.get(1).toString();
            String fechaActual = parametros.get(2).toString();
            String ic_if = parametros.get(3).toString();
            String nombreUsuario =
                strNombreUsuario.substring(strNombreUsuario.lastIndexOf("-") + 1, strNombreUsuario.length());

            String fondoLiquido = parametros.get(19).toString();
            String strPerfil = parametros.get(20).toString();
            String iNoUsuario = parametros.get(18).toString();
            // informacion del usuario
            Usuario usuarioIfOperador = utilUsr.getUsuario(iNoUsuario);
            String nombreOper = (String) (obtieneInfoCorreo(iNoUsuario)).get("USUARIO_NOMBRE");
            String mailOper = usuarioIfOperador.getEmail();
            String tel = this.getTelefonoIfOperador(ic_if);

            HashMap registros = this.getDatosSolicitud(ic_solicitud, ic_if);
            List firmas = this.getFirmasModificatorias(ic_if);
            String firmaMancomunada = this.getOperaFirmaMancomunada(ic_if);
            //Informacion de Concentrador
            String loginUsuario = this.getConcentrador();
            Usuario usuarioObj = utilUsr.getUsuario(loginUsuario);
            String destinatario = usuarioObj.getEmail();
            String nombreUserIF = (String) (obtieneInfoCorreo(loginUsuario)).get("USUARIO_NOMBRE");
            //Informacion de Concentrador2
            String loginUsuario2 = this.getCampoOpeParamGenerales("CG_CONCENTRADOR2");
            Usuario usuarioObj2 = null;
            String destinatario2 = "";
            String nombreUserIF2 = "";
            if (!loginUsuario2.equals("")) {
                usuarioObj2 = utilUsr.getUsuario(loginUsuario2);
                destinatario2 = usuarioObj2.getEmail();
                nombreUserIF2 = (String) (obtieneInfoCorreo(loginUsuario2)).get("USUARIO_NOMBRE");
            }
            // informacion del Subdirector de Mesa de Control de Cr�dito
            String loginSubDirector = this.getCampoOpeParamGenerales("CG_SUBDIRECTOR");
            Usuario usuarioSubDir = utilUsr.getUsuario(loginSubDirector);
            String correoSub = usuarioSubDir.getEmail();
            //String nombreUserSub = (String)(obtieneInfoCorreo(loginSubDirector)).get("USUARIO_NOMBRE");
            // datos del Autorizador 1
            String usuarioAuto1 = this.getCampoRegDatosIf("CG_AUTORIZADOR_UNO", ic_if);
            Usuario usuarioObjAuto1 = null;
            String mailAuto1 = "";
            String nombreUserAuto1 = "";
            if (!usuarioAuto1.equals("")) {
                usuarioObjAuto1 = utilUsr.getUsuario(usuarioAuto1);
                mailAuto1 = usuarioObjAuto1.getEmail();
                nombreUserAuto1 = (String) (obtieneInfoCorreo(usuarioAuto1)).get("USUARIO_NOMBRE");
            }
            // datos del Autorizador 2
            String usuarioAuto2 = this.getCampoRegDatosIf("CG_AUTORIZADOR_DOS", ic_if);
            Usuario usuarioObjAuto2 = null;
            String mailAuto2 = "";
            String nombreUserAuto2 = "";
            if (!usuarioAuto2.equals("")) {
                usuarioObjAuto2 = utilUsr.getUsuario(usuarioAuto2);
                mailAuto2 = usuarioObjAuto2.getEmail();
                nombreUserAuto2 = (String) (obtieneInfoCorreo(usuarioAuto2)).get("USUARIO_NOMBRE");
            }
            // loguin operador
            String usuarioOperador = this.getCampoRegDatosIf("CG_OPERADOR", ic_if);
            String Dest = "";
            String asunto = "";
            String CC = "";

            if (!firmaMancomunada.equals("N") && usuarioOperador.equals(iNoUsuario)) {

                if (!usuarioAuto2.equals("")) {
                    asunto = "Registro de Solicitud de Recursos - Pre Solicitada ";
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                        // En caso de que tenga Firma Mancomunada
                        " Estimado(s) " + nombreUserAuto1 + " y " + nombreUserAuto2 + " <BR>" +
                        " Por este medio se solicita de la manera m&aacute;s atenta la validaci&oacute;n " +
                        " de la siguiente solicitud de recursos, esperando pueda ser aprobada y " +
                        " posteriormente enviada a Nacional Financiera, N.C. al &aacute;rea de \"Mesa " +
                        " de control de Cr&eacute;dito\" para la autorizaci&oacute;n correspondiente.<BR>");
                    Dest = mailAuto1 + "," + mailAuto2;
                } else {
                    asunto = "Registro de Solicitud de Recursos ";
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                        // En caso de que tenga Firma Mancomunada
                        " Estimado(s) " + nombreUserAuto1 + " <BR>" +
                        " Por este medio se solicita de la manera m&aacute;s atenta la validaci&oacute;n " +
                        " de la siguiente solicitud de recursos, esperando pueda ser aprobada y " +
                        " posteriormente enviada a Nacional Financiera, N.C. al &aacute;rea de \"Mesa " +
                        " de control de Cr&eacute;dito\" para la autorizaci&oacute;n correspondiente.<BR>");
                    Dest = mailAuto1;
                }
            } else if (firmaMancomunada.equals("N")) {
                asunto = "Registro de Solicitud de Recursos ";
                if (!loginUsuario2.equals("")) {
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                                     " Estimado(s): C.P. " + nombreUserIF + " y " + nombreUserIF2 + " <BR>" +
                                     " Por este conducto, hago de su conocimiento que el d&iacute;a de hoy " +
                                     fechaActual + " se registr&oacute; la solicitud de recursos del Usuario " +
                                     nombreUsuario + " correspondiente al Intermediario Financiero " +
                                     registros.get("NOMBRE_IF") + " <BR>");

                    Dest = destinatario + "," + destinatario2;
                } else {
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                                     " Estimado (a): C.P. " + nombreUserIF + " <BR>" +
                                     " Por este conducto, hago de su conocimiento que el d&iacute;a de hoy " +
                                     fechaActual + " se registr&oacute; la solicitud de recursos del Usuario " +
                                     nombreUsuario + " correspondiente al Intermediario Financiero " +
                                     registros.get("NOMBRE_IF") + " <BR>");
                    Dest = destinatario;
                }
                CC = correoSub;

            }

            contenido.append("<br> <table width='600' align='CENTER' border='1'  >" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Importe                         </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>$" +
                             Comunes.formatoDecimal(String.valueOf(registros.get("IMPORTE")), 2) + "</td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Moneda                          </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("MONEDA") + "</td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Fecha de Env&iacute;o Solicitud </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACARGA") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Hora de Env&iacute;o            </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("HORACARGA") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Usuario IF                      </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("USUARIO") + "</td> </tr>" + " </table> <br>  ");

            contenido.append(" <br>  <table width='600' align='center'  style='font-size:11pt;font-family: Calibri'> " +
                             "<tr  align='justify' > " +
                             "<td align='justify'  style='font-size:11pt;font-family: Calibri; text-align: justify;'>" +
                             " La presente solicitud de recursos se realiza al amparo del " +
                             registros.get("NOMBRECONTRATO") + " con fecha de firma " + registros.get("FECHACONTRATO"));
            if (firmas.size() > 0) {
                int x = 1;
                contenido.append(" asi como,   ");
                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);
                    contenido.append(" Contrato Modificatorio " + x + " con fecha de firma " +
                                     datos.get("FECHA").toString() + ", ");
                    x++;
                }
            }
            contenido.append(" y el Contrato de Operaci&oacute;n Electr&oacute;nica con fecha de firma  " +
                             registros.get("FECHACONTRATO_OE") + ", celebrados entre  " + registros.get("NOMBRE_IF") +
                             "  y Nacional Financiera, S.N.C. <BR>" +
                             "<BR>Con fundamento en lo dispuesto por los art&iacute;culos 1205 y 1298-A del C&oacute;digo de Comercio y del 52 de la ley de Instituciones de Cr&eacute;dito, la informaci&oacute;n e instrucciones que el Intermediario y Nacional Financiera, S.N.C. se transmitan o comuniquen mutuamente mediante el sistema, tendr&aacute;n pleno valor probatorio y fuerza legal para acreditar la operaci&oacute;n realizada, el importe de la misma, su naturaleza; as� como, las caracter&iacute;sticas y alcance de sus instrucciones." +
                             "<BR></td></tr>");

            if (strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP")) {

                contenido.append("<tr> <td align='justify' class='formas'>" + " <BR>  Bajo protesta de decir verdad, " +
                                 registros.get("NOMBRE_IF") +
                                 " manifiesta que la informaci&oacute;n que env&iacute;a en archivo electr&oacute;nico mediante la herramienta 'Carga Cartera en Prenda', corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN " +
                                 " que garantiza la presente disposici&oacute;n de recursos del cr&eacute;dito, formalizada en t&eacute;rminos del Contrato de L&iacute;nea de Cr&eacute;dito en Cuenta Corriente celebrado con NAFIN." +
                                 "</td></tr>");

                /* 01/03/2017 IHJ
				contenido.append("<tr> <td align='justify' class='formas'>"+
				" <BR>  Bajo protesta de decir verdad, "+registros.get("NOMBRE_IF")+" manifiesta que la informaci&oacute;n que env&iacute;a en archivo electr&oacute;nico mediante la herramienta 'Carga Cartera en Prenda' corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN  que garantiza la presente disposici&oacute;n de recursos del cr&eacute;dito."+
				" En t&eacute;rminos del Contrato de L&iacute;nea de Cr&eacute;dito en Cuenta Corriente celebrado con NAFIN, la ACREDITADA se obliga a enviar a NAFIN ,de manera f&iacute;sica y debidamente firmada, la informaci&oacute;n correspondiente a la cartera crediticia otorgada en prenda a favor de NAFIN, dentro de los siguientes 5 d�as h&aacute;biles posteriores a la presente solicitud electr&oacute;nica de disposici&oacute;n de recursos."+
				"</td></tr>");
				*/

            }

            contenido.append("</table> <br>");

            contenido.append(" <br>  <table width='800' align='CENTER' border='1'  style='font-size:11pt;font-family: Calibri' >" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			   <b>  Fecha Solicitud 								    </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHASOLICITUD") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				   <b>  Nombre IF  											 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NOMBRE_IF") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				   <b>  N&uacute;mero de Cuenta 									 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NUM_CUENTAS") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			   <b>  Banco 													 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("BANCO") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			   <b>  Cuenta CLABE 										 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("CLABE") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'colspan='2'>  <b>  Contratos  											 </td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>             <b>  Nombre Contrato 									 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NOMBRECONTRATO") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>             <b>  Fecha Firma Contrato 							    </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACONTRATO") + "</td></tr>");

            firmas = this.getFirmasModificatorias(ic_if); //obtengo las firmas Modificatorias
            if (firmas.size() > 0) {
                contenido.append(" <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>             <b>  Contratos Modificatorios 						 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                                 registros.get("MODIFICATORIOS") + "</td></tr>");

                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);
                    String descripcion = datos.get("DESCRIPCION").toString();
                    String fecha = datos.get("FECHA").toString();
                    contenido.append(" <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>  <b>" +
                                     descripcion + " </td> <td align='left' width='400' >" + fecha + "</td></tr>");
                }
            }
            contenido.append("<tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 	  <b>  Nombre Contrato   </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NOMBRECONTRATO_OE") + "</td></tr>" +
                             " <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 	  <b>  Fecha Firma Contrato Operaci&oacute;n Electr&oacute;nica    </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACONTRATO_OE") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300' colspan='2'  style='font-size:11pt;font-family: Calibri'><b>  Datos del Pr&eacute;stamo  			             	  </td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				  <b>  Destino de los recursos			              </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("RECURSOS") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			  <b>  Fecha de Primer pago de Capital               </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACAPITAL") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				  <b>  Fecha de Primer pago de Inter�s               </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHAINTERES") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			  <b>  Fecha de Vencimiento 				              </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHAVENCIMIENTO") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Tasa de Inter&eacute;s                               </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("TASAINTERES") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Observaciones                                 </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("OBSERVACIONES") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Usuario IF                                    </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             nombreUsuario + "</td></tr>");

            if (strPerfil.equals("IF 5MIC") || strPerfil.equals("IF 5CP") || strPerfil.equals("IF 4MIC")) {
                contenido.append(" <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Fondo L&iacute;quido                                     </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'> $" +
                                 Comunes.formatoDecimal(fondoLiquido, 2) + "</td></tr>");
            }

            contenido.append(" </table> <BR>");


            //Tabla de Amortizaci�n
            List regTabla = this.getTablaAmortizacion(ic_solicitud);

            contenido.append("<br>  <table width='500' align='CENTER' border='1' >" +
                             " <tr> <td bgcolor='silver' align='center' style='font-size:11pt;font-family: Calibri'><b>Periodo </td> " +
                             "<td bgcolor='silver' align='center' style='font-size:11pt;font-family: Calibri'><b> Fecha</td>  " +
                             "<td bgcolor='silver' align='center' style='font-size:11pt;font-family: Calibri'><b> Amortizaci&oacute;n</td></tr>");
            for (int i = 0; i < regTabla.size(); i++) {
                HashMap datos = (HashMap) regTabla.get(i);

                contenido.append(" <tr>" +
                                 " <td align='center' width='100'  style='font-size:11pt;font-family: Calibri'>" +
                                 datos.get("PERIODO").toString() + "</td>" +
                                 " <td align='center' width='100'  style='font-size:11pt;font-family: Calibri'>" +
                                 datos.get("FECHA").toString() + "</td>" +
                                 " <td align='center' width='100'  style='font-size:11pt;font-family: Calibri'> $" +
                                 Comunes.formatoDecimal(String.valueOf(datos.get("AMORTIZACION").toString()), 2) +
                                 "</td>" + "</tr>");
            }
            contenido.append(" </table> <BR>");

            if (!firmaMancomunada.equals("N") && usuarioOperador.equals(iNoUsuario)) {
                contenido.append(" <BR>Por favor ingrese a la siguiente ruta del sitio de Operaci&oacute;n Electr&oacute;nica en" +
                                 " http://cadenas.nafin.com.mx para validar las solicitudes:" +
                                 " Operaci&oacute;n Electr&oacute;nica IFNBS / Capturas / Revisi&oacute;n  Solicitudes de Recursos" + "<BR><BR>Sin otro particular, reciba un cordial saludo." +

                                 " <BR><BR>ATENTAMENTE" + " <BR>" + nombreOper + " <BR>" + mailOper + " <BR>" + tel);

            } else if (firmaMancomunada.equals("N")) {
                contenido.append(" <BR>ATENTAMENTE" + " <BR>Nacional Financiera S.N.C. <BR>");
                contenido.append("</span></span>");
            }

            log.debug("Dest  " + Dest);
            log.debug("CC  " + CC);
            log.debug("firmaMancomunada  " + firmaMancomunada);
            if (!firmaMancomunada.equals("N") && usuarioOperador.equals(iNoUsuario)) {
                correo.enviarTextoHTML("no_response@nafin.gob.mx", Dest, asunto, contenido.toString());
            } else if (firmaMancomunada.equals("N")) {
                correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", Dest, CC, asunto, contenido.toString(),
                                                   null, null);
            }


        } catch (Throwable e) {
            log.error("Error al enviar correo  " + e);
            throw new AppException("Error al enviar correo ", e);
        } finally {
            log.info("envioCorreo(S)");
        }
    }

    /**
     * Funci�n que realiza el envio de correo, para notificar el registr� de la solicitud de recursos
     * @return
     * @param parametros
     * @param ic_solicitud
     */
    public boolean envioCorreoAux(List parametros, String ic_solicitud) {
        log.info("envioCorreoAux(E)");

        Correo correo = new Correo();
        correo.setCodificacion("charset=UTF-8");
        StringBuffer contenido = new StringBuffer();
        UtilUsr utilUsr = new UtilUsr();
        boolean env = true;
        try {

            String strNombreUsuario = parametros.get(1).toString();
            String fechaActual = parametros.get(2).toString();
            String ic_if = parametros.get(3).toString();
            String nombreUsuario =
                strNombreUsuario.substring(strNombreUsuario.lastIndexOf("-") + 1, strNombreUsuario.length());

            String fondoLiquido = parametros.get(19).toString();
            String strPerfil = parametros.get(20).toString();
            String iNoUsuario = parametros.get(18).toString();
            // informacion del usuario
            Usuario usuarioIfOperador = utilUsr.getUsuario(iNoUsuario);
            String nombreOper = (String) (obtieneInfoCorreo(iNoUsuario)).get("USUARIO_NOMBRE");
            String mailOper = usuarioIfOperador.getEmail();
            String tel = this.getTelefonoIfOperador(ic_if);

            HashMap registros = this.getDatosSolicitud(ic_solicitud, ic_if);
            List firmas = this.getFirmasModificatorias(ic_if);
            String firmaMancomunada = this.getOperaFirmaMancomunada(ic_if);
            //Informacion de Concentrador
            String loginUsuario = this.getConcentrador();
            Usuario usuarioObj = utilUsr.getUsuario(loginUsuario);
            String destinatario = usuarioObj.getEmail();
            String nombreUserIF = (String) (obtieneInfoCorreo(loginUsuario)).get("USUARIO_NOMBRE");
            //Informacion de Concentrador2
            String loginUsuario2 = this.getCampoOpeParamGenerales("CG_CONCENTRADOR2");
            Usuario usuarioObj2 = null;
            String destinatario2 = "";
            String nombreUserIF2 = "";
            if (!loginUsuario2.equals("")) {
                usuarioObj2 = utilUsr.getUsuario(loginUsuario2);
                destinatario2 = usuarioObj2.getEmail();
                nombreUserIF2 = (String) (obtieneInfoCorreo(loginUsuario2)).get("USUARIO_NOMBRE");
            }
            // informacion del Subdirector de Mesa de Control de Cr�dito
            String loginSubDirector = this.getCampoOpeParamGenerales("CG_SUBDIRECTOR");
            Usuario usuarioSubDir = utilUsr.getUsuario(loginSubDirector);
            String correoSub = usuarioSubDir.getEmail();
            String nombreUserSub = (String) (obtieneInfoCorreo(loginSubDirector)).get("USUARIO_NOMBRE");
            // datos del Autorizador 1
            String usuarioAuto1 = this.getCampoRegDatosIf("CG_AUTORIZADOR_UNO", ic_if);
            Usuario usuarioObjAuto1 = null;
            String mailAuto1 = "";
            String nombreUserAuto1 = "";
            if (!usuarioAuto1.equals("")) {
                usuarioObjAuto1 = utilUsr.getUsuario(usuarioAuto1);
                mailAuto1 = usuarioObjAuto1.getEmail();
                nombreUserAuto1 = (String) (obtieneInfoCorreo(usuarioAuto1)).get("USUARIO_NOMBRE");
            }
            // datos del Autorizador 2
            String usuarioAuto2 = this.getCampoRegDatosIf("CG_AUTORIZADOR_DOS", ic_if);
            Usuario usuarioObjAuto2 = null;
            String mailAuto2 = "";
            String nombreUserAuto2 = "";
            if (!usuarioAuto2.equals("")) {
                usuarioObjAuto2 = utilUsr.getUsuario(usuarioAuto2);
                mailAuto2 = usuarioObjAuto2.getEmail();
                nombreUserAuto2 = (String) (obtieneInfoCorreo(usuarioAuto2)).get("USUARIO_NOMBRE");
            }
            // loguin operador
            String usuarioOperador = this.getCampoRegDatosIf("CG_OPERADOR", ic_if);
            String Dest = "";
            String asunto = "";
            String CC = "";

            if (!firmaMancomunada.equals("N") && usuarioOperador.equals(iNoUsuario)) {

                if (!usuarioAuto2.equals("")) {
                    asunto = "Registro de Solicitud de Recursos - Pre Solicitada ";
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                        // En caso de que tenga Firma Mancomunada
                        " Estimado(s) " + nombreUserAuto1 + " y " + nombreUserAuto2 + " <BR>" +
                        " Por este medio se solicita de la manera m&aacute;s atenta la validaci&oacute;n " +
                        " de la siguiente solicitud de recursos, esperando pueda ser aprobada y " +
                        " posteriormente enviada a Nacional Financiera, N.C. al &aacute;rea de \"Mesa " +
                        " de control de Cr&eacute;dito\" para la autorizaci&oacute;n correspondiente.<BR>");
                    Dest = mailAuto1 + "," + mailAuto2;
                } else {
                    asunto = "Registro de Solicitud de Recursos ";
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                        // En caso de que tenga Firma Mancomunada
                        " Estimado(s) " + nombreUserAuto1 + " <BR>" +
                        " Por este medio se solicita de la manera m&aacute;s atenta la validaci&oacute;n " +
                        " de la siguiente solicitud de recursos, esperando pueda ser aprobada y " +
                        " posteriormente enviada a Nacional Financiera, N.C. al &aacute;rea de \"Mesa " +
                        " de control de Cr&eacute;dito\" para la autorizaci&oacute;n correspondiente.<BR>");
                    Dest = mailAuto1;
                }
            } else if (firmaMancomunada.equals("N")) {
                asunto = "Registro de Solicitud de Recursos ";
                if (!loginUsuario2.equals("")) {
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                                     " Estimado(s): C.P. " + nombreUserIF + " y " + nombreUserIF2 + " <BR>" +
                                     " Por este conducto, hago de su conocimiento que el d&iacute;a de hoy " +
                                     fechaActual + " se registr&oacute; la solicitud de recursos del Usuario " +
                                     nombreUsuario + " correspondiente al Intermediario Financiero " +
                                     registros.get("NOMBRE_IF") + " <BR>");

                    Dest = destinatario + "," + destinatario2;
                } else {
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                                     " Estimado (a): C.P. " + nombreUserIF + " <BR>" +
                                     " Por este conducto, hago de su conocimiento que el d&iacute;a de hoy " +
                                     fechaActual + " se registr&oacute; la solicitud de recursos del Usuario " +
                                     nombreUsuario + " correspondiente al Intermediario Financiero " +
                                     registros.get("NOMBRE_IF") + " <BR>");
                    Dest = destinatario;
                }
                CC = correoSub;

            }

            contenido.append("<br> <table width='600' align='CENTER' border='1'  >" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Importe						 </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>$" +
                             Comunes.formatoDecimal(String.valueOf(registros.get("IMPORTE")), 2) + "</td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Moneda 						 </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("MONEDA") + "</td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Fecha de Env&iacute;o Solicitud  </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACARGA") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Hora de Env&iacute;o 				 </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("HORACARGA") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Usuario IF 					 </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("USUARIO") + "</td> </tr>" + " </table> <br>  ");

            contenido.append(" <br>  <table width='600' align='center'  style='font-size:11pt;font-family: Calibri'> " +
                             "<tr  align='justify' > " +
                             "<td align='justify'  style='font-size:11pt;font-family: Calibri; text-align: justify;'>" +
                             " La presente solicitud de recursos se realiza al amparo del " +
                             registros.get("NOMBRECONTRATO") + " con fecha de firma " + registros.get("FECHACONTRATO"));
            if (firmas.size() > 0) {
                int x = 1;
                contenido.append(" asi como,   ");
                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);
                    contenido.append(" Contrato Modificatorio " + x + " con fecha de firma " +
                                     datos.get("FECHA").toString() + ", ");
                    x++;
                }
            }
            contenido.append(" y el Contrato de Operaci&oacute;n Electr&oacute;nica con fecha de firma  " +
                             registros.get("FECHACONTRATO_OE") + ", celebrados entre  " + registros.get("NOMBRE_IF") +
                             "  y Nacional Financiera, S.N.C. <BR>" +
                             "<BR>Con fundamento en lo dispuesto por los art&iacute;culos 1205 y 1298-A del C&oacute;digo de Comercio y del 52 de la ley de Instituciones de Cr&eacute;dito, la informaci&oacute;n e instrucciones que el Intermediario y Nacional Financiera, S.N.C. se transmitan o comuniquen mutuamente mediante el sistema, tendr&aacute;n pleno valor probatorio y fuerza legal para acreditar la operaci&oacute;n realizada, el importe de la misma, su naturaleza; as&iacute; como, las caracter&iacute;sticas y alcance de sus instrucciones." +
                             "<BR></td></tr>");

            if (strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP")) {

                contenido.append("<tr> <td align='justify' class='formas'>" + " <BR>  Bajo protesta de decir verdad," +
                                 registros.get("NOMBRE_IF") +
                                 " manifiesta que la informaci&oacute;n que env&iacute;a en archivo electr&oacute;nico mediante la herramienta 'Carga Cartera en Prenda' corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN  que garantiza la presente disposici&oacute;n de recursos del cr&eacute;dito," +
                                 " formalizada en t&eacute;rminos del Contrato de L&iacute;nea de Cr&eacute;dito en Cuenta Corriente celebrado con NAFIN." +
                                 "</td></tr>");

                /* 01/03/2017 IHJ
				contenido.append("<tr> <td align='justify' class='formas'>"+				
				" <BR>  Bajo protesta de decir verdad," +registros.get("NOMBRE_IF") +" manifiesta que la informaci&oacute;n que env&iacute;a en archivo electr&oacute;nico mediante la herramienta 'Carga Cartera en Prenda' corresponde a la cartera crediticia otorgada en prenda a favor de NAFIN  que garantiza la presente disposici&oacute;n de recursos del cr&eacute;dito."+
				" En t&eacute;rminos del Contrato de L&iacute;nea de Cr&eacute;dito en Cuenta Corriente celebrado con NAFIN, la ACREDITADA se obliga a enviar a NAFIN ,de manera f&iacute;sica y debidamente firmada, la informaci&oacute;n correspondiente a la cartera crediticia otorgada en prenda a favor de NAFIN, dentro de los siguientes 5 d&iacute;as h&aacute;biles posteriores a la presente solicitud electr&oacute;nica de disposici&oacute;n de recursos."+
				"</td></tr>");					
				*/
            }

            contenido.append("</table> <br>");

            contenido.append(" <br>  <table width='800' align='CENTER' border='1'  style='font-size:11pt;font-family: Calibri' >" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			   <b>  Fecha Solicitud 								    </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHASOLICITUD") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				   <b>  Nombre IF  											 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NOMBRE_IF") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				   <b>  N&uacute;mero de Cuenta 									 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NUM_CUENTAS") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			   <b>  Banco 													 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("BANCO") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			   <b>  Cuenta CLABE 										 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("CLABE") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'colspan='2'>  <b>  Contratos  											 </td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>             <b>  Nombre Contrato 									 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NOMBRECONTRATO") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>             <b>  Fecha Firma Contrato 							    </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACONTRATO") + "</td></tr>");

            firmas = this.getFirmasModificatorias(ic_if); //obtengo las firmas Modificatorias
            if (firmas.size() > 0) {
                contenido.append(" <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>             <b>  Contratos Modificatorios 						 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                                 registros.get("MODIFICATORIOS") + "</td></tr>");

                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);
                    String descripcion = datos.get("DESCRIPCION").toString();
                    String fecha = datos.get("FECHA").toString();
                    contenido.append(" <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>  <b>" +
                                     descripcion + " </td> <td align='left' width='400' >" + fecha + "</td></tr>");
                }
            }
            contenido.append("<tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 	  <b>  Nombre Contrato   </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NOMBRECONTRATO_OE") + "</td></tr>" +
                             " <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 	  <b>  Fecha Firma Contrato Operaci&oacute;n Electr&oacute;nica    </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACONTRATO_OE") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300' colspan='2'  style='font-size:11pt;font-family: Calibri'><b>  Datos del Pr&eacute;stamo  			             	  </td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				  <b>  Destino de los recursos			              </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("RECURSOS") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			  <b>  Fecha de Primer pago de Capital               </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACAPITAL") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				  <b>  Fecha de Primer pago de Inter&eacute;s               </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHAINTERES") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			  <b>  Fecha de Vencimiento 				              </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHAVENCIMIENTO") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Tasa de Inter&eacute;s                               </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("TASAINTERES") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Observaciones                                 </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("OBSERVACIONES") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Usuario IF                                    </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             nombreUsuario + "</td></tr>");

            if (strPerfil.equals("IF 5MIC") || strPerfil.equals("IF 5CP") || strPerfil.equals("IF 4MIC")) {
                contenido.append(" <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Fondo L&iacute;quido                                     </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'> $" +
                                 Comunes.formatoDecimal(fondoLiquido, 2) + "</td></tr>");
            }

            contenido.append(" </table> <BR>");


            //Tabla de Amortizaci�n
            List regTabla = this.getTablaAmortizacion(ic_solicitud);

            contenido.append("<br>  <table width='500' align='CENTER' border='1' >" +
                             " <tr> <td bgcolor='silver' align='center' style='font-size:11pt;font-family: Calibri'><b>Periodo </td> " +
                             "<td bgcolor='silver' align='center' style='font-size:11pt;font-family: Calibri'><b> Fecha</td>  " +
                             "<td bgcolor='silver' align='center' style='font-size:11pt;font-family: Calibri'><b> Amortizaci&oacute;n</td></tr>");
            for (int i = 0; i < regTabla.size(); i++) {
                HashMap datos = (HashMap) regTabla.get(i);

                contenido.append(" <tr>" +
                                 " <td align='center' width='100'  style='font-size:11pt;font-family: Calibri'>" +
                                 datos.get("PERIODO").toString() + "</td>" +
                                 " <td align='center' width='100'  style='font-size:11pt;font-family: Calibri'>" +
                                 datos.get("FECHA").toString() + "</td>" +
                                 " <td align='center' width='100'  style='font-size:11pt;font-family: Calibri'> $" +
                                 Comunes.formatoDecimal(String.valueOf(datos.get("AMORTIZACION").toString()), 2) +
                                 "</td>" + "</tr>");
            }
            contenido.append(" </table> <BR>");

            if (!firmaMancomunada.equals("N") && usuarioOperador.equals(iNoUsuario)) {
                contenido.append(" <BR>Por favor ingrese a la siguiente ruta del sitio de Operaci&oacute;n Electr&oacute;nica en" +
                                 " http://cadenas.nafin.com.mx para validar las solicitudes:" +
                                 " Operaci&oacute;n Electr&oacute;nica IFNBS / Capturas / Revisi&oacute;n  Solicitudes de Recursos" + "<BR><BR>Sin otro particular, reciba un cordial saludo." +

                                 " <BR><BR>ATENTAMENTE" + " <BR>" + nombreOper + " <BR>" + mailOper + " <BR>" + tel);

            } else if (firmaMancomunada.equals("N")) {
                contenido.append(" <BR>ATENTAMENTE" + " <BR>Nacional Financiera S.N.C. <BR>");
                contenido.append("</span></span>");
            }

            log.debug("Dest  " + Dest);
            log.debug("CC  " + CC);
            log.debug("firmaMancomunada  " + firmaMancomunada);
            if (!firmaMancomunada.equals("N") && usuarioOperador.equals(iNoUsuario)) {
                correo.enviarTextoHTML("no_response@nafin.gob.mx", Dest, asunto, contenido.toString());
            } else if (firmaMancomunada.equals("N")) {
                correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", Dest, CC, asunto, contenido.toString(),
                                                   null, null);
            }


        } catch (Throwable e) {
            env = false;
            log.error("Error al envioCorreoAux  " + e);
            throw new AppException("Error al enviar correo ", e);
        } finally {
            log.info("envioCorreoAux(S)");
        }
        return env;
    }

    /**
     *
     * @return
     * @param ic_if
     */
    public String getSaldoLiquido(String ic_if) {
        log.info("getSaldoLiquido (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String saldoLiquido = "";

        try {
            con.conexionDB();

            //Fondo l�quido que el Intermediario Financiero dep�sito a NAFIN CU9
            SQL = new StringBuffer();
            varBind = new ArrayList();
            SQL.append(" select  FG_SALDO_HOY as SALDOLIQUIDO  from   OPE_CARGA_SALDO_FLIQUIDO " +
                       " where ic_carga =  (select max(ic_carga)  from OPE_CARGA_SALDO_FLIQUIDO  where ic_if  = ? ) ");
            varBind.add(ic_if);
            log.info("SQL.toString() " + SQL.toString());
            log.info("varBind " + varBind);

            ps = con.queryPrecompilado(SQL.toString(), varBind);
            rs = ps.executeQuery();
            if (rs.next()) {
                saldoLiquido = rs.getString("SALDOLIQUIDO") == null ? "0" : rs.getString("SALDOLIQUIDO");
            }
            rs.close();
            ps.close();


        } catch (Exception e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getSaldoLiquido " + e);
            throw new AppException("Error al getSaldoLiquido", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getSaldoLiquido (S)");
        }
        return saldoLiquido;
    }


    //*************************** Caso de Uso 4    7  Caso de Uso 5*********************************

    /**
     * Metodo para generar el archivo de la tablas modificatorias
     * se usa en  Caso de Uso 4  y Caso de Uso 5
     * @throws java.lang.Exception
     * @return
     * @param ic_solicitud
     * @param path
     */

    public String getArchivoTablaAmortizacion(String path, String ic_solicitud) {
        log.info("getArchivoTablaAmortizacion (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();

        String nombreArchivo = "";
        StringBuffer contenidoArchivo = new StringBuffer();
        OutputStreamWriter writer = null;
        BufferedWriter buffer = null;
        int total = 0;

        try {
            con.conexionDB();

            nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
            writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
            buffer = new BufferedWriter(writer);

            contenidoArchivo.append("Tabla Amortizaci�n  \n");
            contenidoArchivo.append("Per�odo, Fecha, Amortizaci�n  \n");


            SQL.append(" SELECT  IN_NUM_PERIODO as PERIODO , to_char(DF_ALTA,'dd/mm/yyyy')   AS FECHA ,  FN_MONTO_AMORT AS  AMORTIZACION  " +
                       " FROM  OPE_TABLA_AMORT  " + " WHERE IC_SOLICITUD = ?  ");

            varBind.add(ic_solicitud);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);
            ps = con.queryPrecompilado(SQL.toString(), varBind);
            rs = ps.executeQuery();

            while (rs.next()) {
                String periodo = rs.getString("PERIODO") == null ? "" : rs.getString("PERIODO");
                String fecha = rs.getString("FECHA") == null ? "" : rs.getString("FECHA");
                String amortizacion = rs.getString("AMORTIZACION") == null ? "" : rs.getString("AMORTIZACION");

                contenidoArchivo.append(periodo.replace(',', ' ') + "," + fecha.replace(',', ' ') + "," +
                                        amortizacion.replace(',', ' ') + "\n");
                total++;
                if (total == 1000) {
                    total = 0;
                    buffer.write(contenidoArchivo.toString());
                    contenidoArchivo = new StringBuffer(); //Limpio
                }

            }
            rs.close();
            ps.close();

            buffer.write(contenidoArchivo.toString());
            buffer.close();
            contenidoArchivo = new StringBuffer(); //Limpio


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getArchivoTablaAmortizacion " + e);
            throw new AppException("Error al getArchivoTablaAmortizacion", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getArchivoTablaAmortizacion (S)");
        }
        return nombreArchivo;
    }

    /**
     *  se usa en  Caso de Uso 4  y Caso de Uso 5
     * Metodo para descarga los archivos de
     * Archivo de Carga de Cotizaci�n en PDF
     * Archivo de Carga Cartera en Prenda
     * Archivo de Comprobante Boleta RUG  PDF
     * Archivo de Documentaci�n Prenda  PDF
     * @throws netropology.utilerias.AppException
     * @return
     * @param extension
     * @param tipoArchivo
     * @param ic_solicitud
     * @param rutaDestino
     */
    public String consDescargaArchivos(String rutaDestino, String ic_solicitud, String tipoArchivo, String extension) {
        log.info("consDescargaArchivos(E) ::..");

        AccesoDB con = new AccesoDB();
        String nombreArchivoTmp = null;
        String columna = "";

        if (tipoArchivo.equals("Cotiza")) {
            columna = "BI_DOC_COTIZACION";
        }
        if (tipoArchivo.equals("Cartera")) {
            columna = "BI_DOC_CARTERA";
        }
        if (tipoArchivo.equals("Cartera_Pdf")) {
            columna = "BI_DOC_CARTERA_PDF";
        }
        if (tipoArchivo.equals("Boleta")) {
            columna = "BI_DOC_BOLETARUG";
        }
        if (tipoArchivo.equals("Prenda")) {
            columna = "BI_DOC_PRENDA";
        }
        if (tipoArchivo.equals("Empresa")) {
            columna = "BI_EMPRESAS";
        }

        try {
            con.conexionDB();

            String strSQL = " SELECT " + columna + " FROM  OPE_REG_SOLICITUD " + " WHERE IC_SOLICITUD = ? ";

            log.debug(" strSQL ::: " + strSQL + " ic_solicitud :::" + ic_solicitud);

            PreparedStatement ps = con.queryPrecompilado(strSQL);
            ps.setInt(1, Integer.parseInt(ic_solicitud));
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {

                InputStream inStream = rs.getBinaryStream(columna);

                CreaArchivo creaArchivo = new CreaArchivo();
                if (!creaArchivo.make(inStream, rutaDestino, "." + extension)) {
                    throw new AppException("Error al generar el archivo en " + rutaDestino);
                }
                nombreArchivoTmp = creaArchivo.getNombre();
                inStream.close();
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            log.error("consArchCotizacion(ERROR) ::..");
            throw new AppException("Error Inesperado", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
            log.info("consArchCotizacion(S) ::..");
        }
        return nombreArchivoTmp;
    }


    //*****************************************Caso de Uso 5 Carga de empresas apoyadas*******************************

    /**
     * Metodo que obtiene un  numero consecutivo para poner la clave del Archivo
     * @return
     */
    public String claveCarga() {

        log.info("claveCarga (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        String claveCarga = "";
        try {

            con.conexionDB();

            String query = "select SEQ_ARCHIVOLINEAS.NEXTVAL from dual";

            PreparedStatement ps = con.queryPrecompilado(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                claveCarga = rs.getString(1);
            }
            rs.close();
            ps.close();

        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("ClaveCarga  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  ClaveCarga (S) ");
            }
        }
        return claveCarga;
    }

    /**
     * Tipo de cambio Udis
     * @return
     */
    private String tipodeCambio() {

        log.info("tipodeCambio (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        String cambio = "0";
        try {

            con.conexionDB();

            String query =
                "   SELECT FN_VALOR_COMPRA  FROM  COM_TIPO_CAMBIO WHERE IC_MONEDA =  40  and  ROWID=(SELECT MAX(ROWID) FROM  COM_TIPO_CAMBIO WHERE IC_MONEDA =  40) ";

            log.debug("query ::: " + query);
            PreparedStatement ps = con.queryPrecompilado(query);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                cambio = rs.getString(1) == null ? "" : rs.getString(1);
            }
            rs.close();
            ps.close();

        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("tipodeCambio  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  tipodeCambio (S) ");
            }
        }
        return cambio;
    }

    /** Lee el archivo txt e inserta los datos en la tabla LINREV_CARGA_LINEAS
     * @return
     * @param nombreArchivo
     * @param strDirectorioTemp
     * @param ic_solicitud
     */
    public List CargaListadeArchivosTMP(String noPrestamo, String strDirectorioTemp, String nombreArchivo,
                                        String claveCarga, String strPerfil, String importe, String iNoCliente) {

        log.info("CargaListadeArchivosTMP (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;

        String linea = "";
        PreparedStatement ps = null;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();

        List Errorlineas = new ArrayList();
        HashMap Errores = new HashMap();
        List Bienlineas = new ArrayList();
        HashMap Bien = new HashMap();
        StringBuffer mError = new StringBuffer();
        StringBuffer mErrorVMonto = new StringBuffer();
        List registros = new ArrayList();

        int iNumLinea = 0, totalRegBienN = 0;
        BigDecimal totalNacional = new BigDecimal(0.0);

        String fechaOperacion = "", intermediario = "", noCredito = "", moneda = "", fecha = "", nombre = "", nombre2 =
            "", aPaterno = "", aMaterno = "", rfc = "", fechaNacimiento = "", monto = "0", producto = "", actEconomica =
            "", estado = "", municipio = "", estrato = "", ventas = "", noEmpleados = "", saldoCapital =
            "", montoOtorgado = "", validaMonto = "N", domicilio = "", telefono = "", correo = "";
        String calle = "", noInterior = "", noExterior = "", colonia = "", cp = "", curp = "", tasaIntM = "", comision =
            "", tasaIntA = "";
        String mujEmpresarias = this.getCampoRegDatosIf("CS_MUJERES_EMPRESARIAS", iNoCliente);

        try {
            log.info("ANTES CON.CONEXIONDB****************");
            con.conexionDB();

            strSQL = new StringBuffer();
            strSQL.append(" INSERT INTO linrev_carga_lineas_tmp (FECHA_OPERACION, INTERMEDIARIO, CREDITO, MONEDA, " +
                          " FECHA, NOMBRE, NOMBRE2, APATERNO,  AMATERNO ," +
                          " RFC, FNACIMIENTO, MONTO, PRODUCTO, CLASE, " +
                          " ESTADO, MUNICIPIO, ESTRATO, VENTAS, NO_EMPLEADOS, ERROR, " +
                          " ARCHIVO, NOMBREARCHIVO, CG_VALIDA_MONTO )" +
                          " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?  )");

            java.io.File lofArchivo = new java.io.File(strDirectorioTemp + nombreArchivo);
            log.info("file   ****************  " + strDirectorioTemp + nombreArchivo);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            while ((linea = br.readLine()) != null) {

                iNumLinea++;
                VectorTokenizer vt = new VectorTokenizer(linea, "|");
                Vector lineaArchivo = vt.getValuesVector();

                Bien = new HashMap();
                Errores = new HashMap();
                mError = new StringBuffer();
                mErrorVMonto = new StringBuffer();

                if (lineaArchivo.size() > 0) {
                    int i = 0;
                    String campo = Comunes.quitaComitasSimples(lineaArchivo.get(i).toString()).trim();
                    campo = campo.replace('"', ' ').trim();
                    lineaArchivo.set(i, campo);
                    int columna = 1;
                    if (lineaArchivo.size() >= columna) {
                        fechaOperacion = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        intermediario = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        noCredito = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        moneda = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        fecha = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        nombre = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        nombre2 = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        aPaterno = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        aMaterno = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        rfc = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        fechaNacimiento = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        monto = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        producto = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        actEconomica = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        estado = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        municipio = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        estrato = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        ventas = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        noEmpleados = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        saldoCapital = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }
                    if (lineaArchivo.size() >= columna) {
                        montoOtorgado = (String) lineaArchivo.get(columna - 1);
                        columna++;
                    }

                    if (strPerfil.equals("IF 5CP") || strPerfil.equals("IF 4CP")) {
                        if (lineaArchivo.size() >= columna) {
                            domicilio = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            telefono = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            correo = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                    }

                    if ((strPerfil.equals("IF 4MIC") || strPerfil.equals("IF 5MIC")) && mujEmpresarias.equals("S")) {
                        System.out.println(" PASO 1 ");
                        if (lineaArchivo.size() >= columna) {
                            calle = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            noInterior = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            noExterior = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            colonia = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            cp = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            curp = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            tasaIntM = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            comision = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                        if (lineaArchivo.size() >= columna) {
                            tasaIntA = (String) lineaArchivo.get(columna - 1);
                            columna++;
                        }
                    }

                    if (lineaArchivo.size() != 21 && strPerfil.equals("IF LI")) {

                        mError.append("El registro  con cantidad de campos es incorrecto");
                    } else if ((lineaArchivo.size() != 24 && strPerfil.equals("IF 5CP")) ||
                               (lineaArchivo.size() != 24 && strPerfil.equals("IF 4CP"))) {
                        mError.append("El registro  con cantidad de campos es incorrecto");
                    } else if (lineaArchivo.size() != 30 &&
                               (strPerfil.equals("IF 4MIC") || strPerfil.equals("IF 5MIC")) &&
                               mujEmpresarias.equals("S")) {
                        mError.append("El registro  con cantidad de campos es incorrecto");
                    } else if (lineaArchivo.size() != 21 &&
                               (strPerfil.equals("IF 4MIC") || strPerfil.equals("IF 5MIC")) &&
                               mujEmpresarias.equals("N")) {
                        mError.append("El registro  con cantidad de campos es incorrecto");
                    } else if (Valida(monto) == false) {
                        mError.append("El dato Recursos Solicitados a NAFIN  es incorrecto");
                    } else if (Valida(ventas) == false) {
                        mError.append("El dato Venta es incorrecto");
                    } else if (!noCredito.equals(noPrestamo)) {
                        mError.append(" El N�mero de Pr�stamo  no corresponde al n�mero de Pr�stamo asignado a la solicitud de Recursos. ");
                    } else if (!moneda.equalsIgnoreCase("PESO MEXICANO MONEDA NACIONAL")) {
                        mError.append(" Solo se permite moneda en PESO MEXICANO MONEDA NACIONAL ");
                    } else if ((lineaArchivo.size() == 24 && strPerfil.equals("IF 5CP")) ||
                               (lineaArchivo.size() == 24 && strPerfil.equals("IF 4CP"))) {
                        if (domicilio.equals("")) {
                            mError.append("El valor del campo Domicilio es obligatorio. \n");
                        } else if (telefono.equals("")) {
                            mError.append("El valor del campo Tel�fono es obligatorio. \n");
                        } else if (correo.equals("")) {
                            mError.append("El valor del campo Correo Electr�nico es obligatorio. \n");
                        }
                    } else if ((lineaArchivo.size() == 30 && strPerfil.equals("IF 4MIC") &&
                                mujEmpresarias.equals("S")) ||
                               (lineaArchivo.size() == 30 && strPerfil.equals("IF 5MIC") &&
                                mujEmpresarias.equals("S"))) {
                        if (calle.equals("")) {
                            mError.append("El valor del campo Calle es obligatorio. \n");
                        } else if (noInterior.equals("")) {
                            mError.append("El valor del campo No. Interior es obligatorio. \n");
                        } else if (noExterior.equals("")) {
                            mError.append("El valor del campo No. Externo es obligatorio. \n");
                        } else if (colonia.equals("")) {
                            mError.append("El valor del campo Colonia es obligatorio. \n");
                        } else if (cp.equals("")) {
                            mError.append("El valor del campo C.P es obligatorio. \n");
                        } else if (curp.equals("")) {
                            mError.append("El valor del campo CURP es obligatorio. \n");
                        } else if (tasaIntM.equals("")) {
                            mError.append("El valor del campo Tasa de Int. (% mensual) es obligatorio. \n");
                        } else if (comision.equals("")) {
                            mError.append("El valor del campo Comisi�n (% monto dispuesto) es obligatorio. \n");
                        } else if (tasaIntA.equals("")) {
                            mError.append("El valor del campo Tasa de Int. (% anual) es obligatorio. \n");
                        }
                    }

                    if (Valida(monto) == true && moneda.equalsIgnoreCase("PESO MEXICANO MONEDA NACIONAL")) {
                        totalNacional = totalNacional.add(new BigDecimal(monto.trim()));
                    }

                } //if(lineaArchivo.size()>0){

                log.debug("mError ::: " + mError);

                if (mError.length() > 0) {
                    Errores.put("ARCHIVO", nombreArchivo);
                    Errores.put("NO_LINEA", String.valueOf(iNumLinea));
                    Errores.put("ERROR", mError.toString());
                    Errorlineas.add(Errores);
                } else {
                    if (moneda.equalsIgnoreCase("PESO MEXICANO MONEDA NACIONAL")) {
                        totalRegBienN++;
                    }
                    lVarBind = new ArrayList();
                    lVarBind.add(fechaOperacion);
                    lVarBind.add(intermediario);
                    lVarBind.add(noCredito);
                    lVarBind.add(moneda);
                    lVarBind.add(fecha);
                    lVarBind.add(nombre);
                    lVarBind.add(nombre2);
                    lVarBind.add(aPaterno);
                    lVarBind.add(aMaterno);
                    lVarBind.add(rfc);
                    lVarBind.add(fechaNacimiento);
                    lVarBind.add(monto);
                    lVarBind.add(producto);
                    lVarBind.add(actEconomica);
                    lVarBind.add(estado);
                    lVarBind.add(municipio);
                    lVarBind.add(estrato);
                    lVarBind.add(ventas.trim());
                    lVarBind.add(noEmpleados);

                    lVarBind.add(mError.toString());
                    lVarBind.add(claveCarga);
                    lVarBind.add(nombreArchivo);
                    lVarBind.add(validaMonto);

                    //log.debug("strSQL "+strSQL.toString());
                    //log.debug("lVarBind "+lVarBind);
                    ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                    ps.executeUpdate();
                    ps.close();

                }

            } // while

            log.debug("totalNacional " + totalNacional.toPlainString());
            log.debug("importe " + importe);
            int valorMonto = totalNacional.compareTo(new BigDecimal(importe));

            if (valorMonto > 0) {
                Errores = new HashMap();
                mError = new StringBuffer();
                mError.append(" El importe total del archivo no coincide con el Importe de la disposici�n de la Solicitud de Recursos");
                Errores.put("ARCHIVO", nombreArchivo);
                Errores.put("NO_LINEA", "");
                Errores.put("ERROR", mError.toString());
                Errorlineas.add(Errores);
            }

            if (totalRegBienN > 0) {
                Bien = new HashMap();
                Bien.put("MONEDA", "PESO MEXICANO MONEDA NACIONAL");
                Bien.put("NO_REGISTROS", String.valueOf(totalRegBienN));
                Bien.put("MONTO", totalNacional);
                Bienlineas.add(Bien);

            }
            registros.add(Errorlineas);
            registros.add(Bienlineas);


            if (Errorlineas.size() > 0) {
                con.terminaTransaccion(false);
                log.debug("TRACE::rollback()");
            } else {
                con.terminaTransaccion(true);
                log.debug("TRACE::commit()");
            }

        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("CargaListadeArchivosTMP  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  CargaListadeArchivosTMP (S) ");
            }
        }
        return registros;
    }


    /**
     * @return
     * @param valor
     */

    private boolean Valida(String valor) {
        try {
            System.out.println("valorvalorvalor operacion electonica111" + valor.trim());
            if (valor.trim().equals("")) {
                return false;
            }
            Double.parseDouble(valor);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /** Metodo que realiza el llamado al proceso Linrev_Carga_Lineas_Sp para validar cada unos de los
     * datos insertados en la tabla  LINREV_CARGA_LINEAS
     * @return
     * @param parametros
     * @param ClaveCarga
     */

    public List validaCargaDatos(String claveCarga) throws NafinException {

        log.info("validaCargaDatos (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        ResultSet rs;

        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        List registros = new ArrayList();
        HashMap datos = new HashMap();
        try {

            con.conexionDB();

            strSQL = new StringBuffer();
            strSQL.append(" INSERT INTO LINREV_CARGA_LINEAS (FECHA_OPERACION, INTERMEDIARIO, CREDITO, MONEDA, " +
                          " FECHA, NOMBRE, NOMBRE2, APATERNO,  AMATERNO ," +
                          " RFC, FNACIMIENTO, MONTO, PRODUCTO, CLASE, " +
                          " ESTADO, MUNICIPIO, ESTRATO, VENTAS, NO_EMPLEADOS, ERROR, " +
                          " ARCHIVO, NOMBREARCHIVO, CG_VALIDA_MONTO  )" +
                          " SELECT FECHA_OPERACION, INTERMEDIARIO, CREDITO, MONEDA, " +
                          " FECHA, NOMBRE, NOMBRE2, APATERNO,  AMATERNO ," +
                          " RFC, FNACIMIENTO, MONTO, PRODUCTO, CLASE, " +
                          " ESTADO, MUNICIPIO, ESTRATO, VENTAS, NO_EMPLEADOS, ERROR, " +
                          " ARCHIVO, NOMBREARCHIVO, CG_VALIDA_MONTO  from linrev_carga_lineas_tmp  WHERE ARCHIVO =  ?  ");

            lVarBind = new ArrayList();
            lVarBind.add(claveCarga);
            log.debug("strSQL " + strSQL.toString());
            log.debug("lVarBind " + lVarBind);

            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();

            strSQL = new StringBuffer();
            strSQL.append("INSERT INTO LINREV_PROCESO_CARGA (PRCA_CVE_ARCHIVO, PRCA_ESTATUS, PRCA_AVANCE,  PRCA_CORRECTOS, PRCA_INCORRECTOS) " +
                          " VALUES( ?, ?, ?, ?, ?  )");

            lVarBind = new ArrayList();
            lVarBind.add(claveCarga);
            lVarBind.add("R");
            lVarBind.add("0");
            lVarBind.add("0");
            lVarBind.add("0");
            //log.debug("strSQL "+strSQL.toString());
            //log.debug("lVarBind "+lVarBind);
            PreparedStatement psi = con.queryPrecompilado(strSQL.toString(), lVarBind);
            psi.executeUpdate();
            psi.close();
            con.terminaTransaccion(true);


            log.debug("Inicia Ejecuci�n de Proceso   Linrev_Carga_Lineas_Sp ");

            //se ejecuta Proceso
            CallableStatement cs = con.ejecutaSP("Linrev_Carga_Lineas_Sp(?)");
            cs.setString(1, claveCarga);
            cs.execute();
            cs.close();
            con.cierraStatement();

            log.debug("Termina Ejecuci�n de Proceso    ");


            strSQL = new StringBuffer();
            lVarBind = new ArrayList();
            strSQL.append(" SELECT  ERROR, FECHA_OPERACION, INTERMEDIARIO, CREDITO, MONEDA, FECHA, NOMBRE, NOMBRE2, APATERNO,  AMATERNO ," +
                          " RFC, FNACIMIENTO, MONTO, PRODUCTO, CLASE,  ESTADO, MUNICIPIO, ESTRATO, VENTAS, NO_EMPLEADOS,  ARCHIVO, NOMBREARCHIVO " +
                          " FROM  LINREV_CARGA_LINEAS  " + " WHERE ARCHIVO =? " + " AND   ERROR <> '1'" +
                          " AND   ERROR IS NOT NULL ");

            lVarBind.add(claveCarga);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            rs = ps.executeQuery();

            while (rs.next()) {
                datos = new HashMap();
                datos.put("ERROR", rs.getString("ERROR") == null ? "" : rs.getString("ERROR"));
                datos.put("FECHA_OPERACION",
                          rs.getString("FECHA_OPERACION") == null ? "" : rs.getString("FECHA_OPERACION"));
                datos.put("NOMBRE_IF", rs.getString("INTERMEDIARIO") == null ? "" : rs.getString("INTERMEDIARIO"));
                datos.put("CREDITO", rs.getString("CREDITO") == null ? "" : rs.getString("CREDITO"));
                datos.put("MONEDA", rs.getString("MONEDA") == null ? "" : rs.getString("MONEDA"));
                datos.put("FECHA", rs.getString("FECHA") == null ? "" : rs.getString("FECHA"));
                datos.put("NOMBRE_1", rs.getString("NOMBRE") == null ? "" : rs.getString("NOMBRE"));
                datos.put("NOMBRE_2", rs.getString("NOMBRE2") == null ? "" : rs.getString("NOMBRE2"));
                datos.put("AP_PATERNO", rs.getString("APATERNO") == null ? "" : rs.getString("APATERNO"));
                datos.put("AP_MATERNO", rs.getString("AMATERNO") == null ? "" : rs.getString("AMATERNO"));
                datos.put("RFC", rs.getString("RFC") == null ? "" : rs.getString("RFC"));
                datos.put("FECHA_NACIMIENTO", rs.getString("FNACIMIENTO") == null ? "" : rs.getString("FNACIMIENTO"));
                datos.put("MONTO", rs.getString("MONTO") == null ? "" : rs.getString("MONTO"));
                datos.put("PRODUCTO", rs.getString("PRODUCTO") == null ? "" : rs.getString("PRODUCTO"));
                datos.put("ACT_ECONOMICA", rs.getString("CLASE") == null ? "" : rs.getString("CLASE"));
                datos.put("SECTOR", "-");
                datos.put("ESTADO", rs.getString("ESTADO") == null ? "" : rs.getString("ESTADO"));
                datos.put("MUNICIPIO", rs.getString("MUNICIPIO") == null ? "" : rs.getString("MUNICIPIO"));
                datos.put("ESTRATO", rs.getString("ESTRATO") == null ? "" : rs.getString("ESTRATO"));
                datos.put("VENTAS", rs.getString("VENTAS") == null ? "" : rs.getString("VENTAS"));
                datos.put("NO_EMPLEADOS", rs.getString("NO_EMPLEADOS") == null ? "" : rs.getString("NO_EMPLEADOS"));
                registros.add(datos);
            }
            rs.close();
            ps.close();


        } catch (SQLException sqle) {
            exito = false;
            log.error("validaCargaDatos  ::::   sqle.getErrorCode() ---->" + sqle.getErrorCode());
            if (sqle.getErrorCode() == 20001) {
                System.out.println("Excepcion " + sqle.getMessage());
                throw new NafinException(sqle.getMessage());
            }
        } catch (Throwable e) {
            exito = false;
            log.error("Excepcion " + e);
            throw new NafinException("Hubo error en validaCargaDatos ");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

            log.info("validaCargaDatos (S) ");
        }
        return registros;

    }

    /**
     * Metodo que Valida el  Monto  de los registros Ingresados a
     * las tablas de l�neas revolventes
     * @return
     * @param parametros
     */
    public List validaRebasaLimite(List parametros) {

        log.info("validaRebasaLimite (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;


        List registros = new ArrayList();
        List ValidaMontos = new ArrayList();
        BigDecimal totalenUdis = new BigDecimal(0.0);

        //String ic_solicitud =  parametros.get(0).toString();
        String claveCarga = parametros.get(1).toString();
        String prestamo = parametros.get(2).toString();
        String strPerfil = parametros.get(3).toString();
        String nombreArchivo = parametros.get(4).toString();

        StringBuffer mErrorVMonto = new StringBuffer();
        HashMap montos = new HashMap();
        String validaMonto = "N";
        int iNumLinea = 1;
        List fechas = new ArrayList();
        String Udis = this.tipodeCambio(); // obtengo valor de los Udi�s

        log.debug("Valor de Udis::::::::::" + Udis);

        try {

            con.conexionDB();

            strSQL = new StringBuffer();
            strSQL.append(" select l.MONTO ,  to_char(l.RGLO_FECHA_OPERACION , 'dd/mm/yyyy')  as RGLO_FECHA_OPERACION , " +
                          "  ac.RFC  as RFC     " + " FROM  linrev_lineas_revolventes  l , linrev_acreditados ac  " +
                          " where ac.clave = l.acre_clave  " + " and  RGLO_NUMERO_CREDITO =  ? ");
            lVarBind.add(prestamo);
            log.debug("SQL.toString() " + strSQL.toString());
            log.debug("varBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            rs = ps.executeQuery();


            while (rs.next()) {

                mErrorVMonto = new StringBuffer();
                montos = new HashMap();
                validaMonto = "N";

                String monto = rs.getString("MONTO") == null ? "" : rs.getString("MONTO");
                String fechaOperacion =
                    rs.getString("RGLO_FECHA_OPERACION") == null ? "" : rs.getString("RGLO_FECHA_OPERACION");
                String rfc = rs.getString("RFC") == null ? "" : rs.getString("RFC");

                fechas.add(fechaOperacion);

                totalenUdis = new BigDecimal(monto).divide(new BigDecimal(Udis), 2);
                int valorUdis9 = totalenUdis.compareTo(new BigDecimal("900000"));

                int valorPesos10 = new BigDecimal(monto).compareTo(new BigDecimal("10000000"));

                log.debug("valorUdis10 ::: " + valorPesos10 + "             valorUdis9 ::: " + valorUdis9);


                if (valorPesos10 > 0 && strPerfil.equals("IF LI")) { // Pesos
                    mErrorVMonto.append(" El Recurso Solicitado a NAFIN del cr�dito es mayor a $10,000,000.00");
                    montos.put("ARCHIVO", nombreArchivo);
                    montos.put("NO_LINEA", rfc);
                    montos.put("ERROR", mErrorVMonto.toString());
                    ValidaMontos.add(montos);
                    validaMonto = "S";
                    //Udis
                } else if (valorUdis9 > 0 &&
                           (strPerfil.equals("IF 4CP") || strPerfil.equals("IF 5CP") || strPerfil.equals("IF 4MIC") ||
                            strPerfil.equals("IF 5MIC"))) {
                    mErrorVMonto.append(" El Recurso Solicitado a NAFIN  del cr�dito es mayor a $900,000.00� UDI�s ");
                    montos.put("ARCHIVO", nombreArchivo);
                    montos.put("NO_LINEA", rfc);
                    montos.put("ERROR", mErrorVMonto.toString());
                    ValidaMontos.add(montos);
                    validaMonto = "S";
                }
                iNumLinea++;

                if (validaMonto.equals("S")) {

                    // si hay algun registro rebasa el limite se actualiza el campo cg_valida_monto = 'S' en lineas revolventes
                    strSQL = new StringBuffer();
                    lVarBind = new ArrayList();
                    strSQL.append(" UPDATE linrev_lineas_revolventes " + " set cg_valida_monto = ?  " +
                                  " where rglo_fecha_operacion  = TO_date ( ?, 'DD/MM/YYYY')     " +
                                  " and RGLO_NUMERO_CREDITO = ?  " + " and ARCHIVO = ? " + " and MONTO =  ? ");

                    lVarBind.add(validaMonto);
                    lVarBind.add(fechaOperacion);
                    lVarBind.add(prestamo);
                    lVarBind.add(claveCarga);
                    lVarBind.add(monto);

                    log.debug("strSQL " + strSQL);
                    log.debug("lVarBind " + lVarBind);
                    PreparedStatement ps1 = con.queryPrecompilado(strSQL.toString(), lVarBind);
                    ps1.executeUpdate();
                    ps1.close();

                }
            } //while( rs.next() ){
            rs.close();
            ps.close();

            log.debug("ValidaMontos  " + ValidaMontos);

            registros.add(ValidaMontos);
            registros.add(fechas);

        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("validaRebasaLimite  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  validaRebasaLimite (S) ");
            }
        }
        return registros;
    }


    /**
     * Metodo que cancela la carga de Empresas Apoyas
     * @param parametros
     */
    public void cancelaCargaEmpresas(List parametros) {

        log.info("cancelaCargaEmpresas (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        //String ic_solicitud =  parametros.get(0).toString();
        //String claveCarga =  parametros.get(1).toString();
        String prestamo = parametros.get(2).toString();
        List fechas = (List) parametros.get(3);

        try {

            con.conexionDB();

            //Elimina los registros cargados  en lineas revolventes
            strSQL = new StringBuffer();
            lVarBind = new ArrayList();
            strSQL.append(" DELETE  FROM   linrev_lineas_revolventes " + " WHERE  RGLO_NUMERO_CREDITO =  ? ");

            lVarBind.add(prestamo);

            log.debug("strSQL " + strSQL);
            log.debug("lVarBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();

            // Actualiza los registros globales por Prestamo y fecha

            if (fechas.size() > 0) {
                for (int i = 0; i < fechas.size(); i++) {

                    String fecha = fechas.get(i).toString();

                    strSQL = new StringBuffer();
                    lVarBind = new ArrayList();
                    strSQL.append(" UPDATE linrev_registros_globales " + " set estatus = ?   " +
                                  " where NUMERO_CREDITO = ? " +
                                  " and  FECHA_OPERACION  = TO_date ( ?, 'DD/MM/YYYY')   ");
                    lVarBind.add("2");
                    lVarBind.add(prestamo);
                    lVarBind.add(fecha);
                    log.debug("strSQL " + strSQL);
                    log.debug("lVarBind " + lVarBind);


                    ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
                    ps.executeUpdate();
                    ps.close();

                }
            }

        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("cancelaCargaEmpresas  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  cancelaCargaEmpresas (S) ");
            }
        }
    }


    /**
     * Metodo que cancela la carga de Empresas Apoyas
     * @param parametros
     */
    public void confirmaCargaCargaEmpresas(List parametros) {

        log.info("confirmaCargaCargaEmpresas (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        PreparedStatement ps = null;

        try {

            con.conexionDB();

            String ic_solicitud = parametros.get(0).toString();
            String validaMontoMax = parametros.get(1).toString();
            String rutaArchivo = parametros.get(2).toString();


            File archivo = new File(rutaArchivo);
            FileInputStream fileinputstream = new FileInputStream(archivo);

            strSQL = new StringBuffer();
            strSQL.append(" UPDATE OPE_REG_SOLICITUD " + " set cg_valida_monto = ?  , " + " CG_EMP_APOYADAS = ? ,  " +
                          " BI_EMPRESAS = ?  " + " where ic_solicitud = ? ");

            log.debug("strSQL " + strSQL);
            log.debug("1 -- " + validaMontoMax);
            log.debug("2 -- " + "S");
            log.debug("4 -- " + ic_solicitud);
            ps = con.queryPrecompilado(strSQL.toString());
            ps.setString(1, validaMontoMax);
            ps.setString(2, "S");
            ps.setBinaryStream(3, fileinputstream, (int) archivo.length());
            ps.setString(4, ic_solicitud);
            ps.executeUpdate();
            ps.close();


        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("confirmaCargaCargaEmpresas  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  confirmaCargaCargaEmpresas (S) ");
            }
        }
    }


    /**
     * Metodo para sacar el total de los datos correctos e incorrectos
     * despues de la carga ya validaci�n del proceso Linrev_Carga_Lineas_Sp
     * @return
     * @param claveCarga
     */
    public List totalCargaLinea(String claveCarga) {
        log.info("totalCargaLinea (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;

        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();
        HashMap datos = new HashMap();
        List registros = new ArrayList();

        try {
            con.conexionDB();

            SQL.append(" select prca_correctos as NO_CORRECTOS , prca_incorrectos as NO_INCORRECTOS  " +
                       "	from   LINREV_PROCESO_CARGA " + " where  prca_cve_archivo = ?  ");
            varBind.add(claveCarga);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);
            PreparedStatement ps = con.queryPrecompilado(SQL.toString(), varBind);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                datos.put("NO_CORRECTOS", rs.getString("NO_CORRECTOS") == null ? "" : rs.getString("NO_CORRECTOS"));
                datos.put("NO_INCORRECTOS",
                          rs.getString("NO_INCORRECTOS") == null ? "" : rs.getString("NO_INCORRECTOS"));
            }
            rs.close();
            ps.close();

            registros.add(datos);

        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error totalCargaLinea " + e);
            throw new AppException("Error al totalCargaLinea", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("totalCargaLinea (S)");
        }
        return registros;
    }


    /**
     * @return
     * @param parametros
     */
    public String getArchivoAcuse(List parametros) {
        log.info("  getArchivoAcuse (E) ");
        String nombreArchivo = "";
        ComunesPDF pdfDoc = new ComunesPDF();
        StringBuffer mensajeModifi = new StringBuffer();


        try {

            String path = parametros.get(0).toString();
            String ic_if = parametros.get(1).toString();
            String strPais = parametros.get(2).toString();
            String iNoNafinElectronico = parametros.get(3).toString();
            String strNombre = parametros.get(4).toString();
            String strNombreUsuario = parametros.get(5).toString();
            String strLogo = parametros.get(6).toString();
            String strDirectorioPublicacion = parametros.get(7).toString();
            String no_Prestamo = parametros.get(8).toString();
            String acuse = parametros.get(9).toString();
            String moneda = parametros.get(10).toString();
            String monto = parametros.get(11).toString();
            String nombreContrato = parametros.get(12).toString();
            String fechaContrato = parametros.get(13).toString();
            String fechaContrato_OE = parametros.get(14).toString();
            String regCorrectos = parametros.get(15).toString();
            String fecha = parametros.get(16).toString();
            String hora = parametros.get(17).toString();
            String usuario = parametros.get(18).toString();
            String no_solicitud2 = parametros.get(19).toString();
            //String strTipoUsuario =  parametros.get(20).toString();

            List firmas = this.getFirmasModificatorias(ic_if);
            HashMap registros = this.getDatosSolicitud(no_solicitud2, ic_if); // Obtengo los datos de la solicitud

            nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
            pdfDoc = new ComunesPDF(2, path + nombreArchivo);

            String meses[] = {
                "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
                "Noviembre", "Diciembre"
            };
            String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual = fechaActual.substring(0, 2);
            String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
            String anioActual = fechaActual.substring(6, 10);
            String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

            pdfDoc.encabezadoConImagenes(pdfDoc, strPais, iNoNafinElectronico, "", strNombre, strNombreUsuario, strLogo,
                                         strDirectorioPublicacion);
            pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
                           " ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);


            pdfDoc.addText(" La Solicitud se env�o con �xito. Acuse de Solicitud " + acuse.toString(), "formas",
                           ComunesPDF.CENTER);
            pdfDoc.addText(" ", "formas", ComunesPDF.CENTER);


            //---------------------Acuse -------------------------
            pdfDoc.setTable(2, 40);
            pdfDoc.setCell("Resumen de Solicitud ", "celda02", ComunesPDF.CENTER, 2);
            pdfDoc.setCell("Importe ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell("$" + Comunes.formatoDecimal(monto, 2), "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Moneda ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(moneda, "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("No. Pr�stamo ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(no_Prestamo, "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Fecha de Env�o de Solicitud ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(fecha, "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Hora de Env�o  ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(hora, "formas", ComunesPDF.LEFT);
            pdfDoc.setCell("Usuario IF  ", "celda02", ComunesPDF.LEFT);
            pdfDoc.setCell(usuario, "formas", ComunesPDF.LEFT);
            pdfDoc.addTable();

            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
            mensajeModifi.append(" Al amparo del  " + nombreContrato + " con fecha de firma  " + fechaContrato);
            int x = 1;
            if (firmas.size() > 0) {
                mensajeModifi.append(" asi como,   ");
                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);

                    mensajeModifi.append(" Contrato Modificatorio " + x + " con fecha de firma " +
                                         datos.get("FECHA").toString() + ", ");
                    x++;
                }
            }
            mensajeModifi.append(" y el Contrato de Operaci�n Electr�nica con fecha de firma  " + fechaContrato_OE +
                                 ", celebrados entre  " + registros.get("NOMBRE_IF") +
                                 "  y Nacional Financiera, S.N.C. se env�an los datos de las empresas apoyadas con recursos NAFIN.");
            pdfDoc.addText(mensajeModifi.toString(), "formas", ComunesPDF.JUSTIFIED);
            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);
            pdfDoc.addText("Con fundamento en lo dispuesto por los art�culos 1205 y 1298-A del C�digo de Comercio y del 52 de la ley de Instituciones de Cr�dito, la informaci�n e instrucciones que el Intermediario y Nacional Financiera, S.N.C. se transmitan o comuniquen mutuamente mediante el sistema, tendr�n pleno valor probatorio y fuerza legal para acreditar la operaci�n realizada, el importe de la misma, su naturaleza; as� como, las caracter�sticas y alcance de sus instrucciones.",
                           "formas", ComunesPDF.JUSTIFIED);
            pdfDoc.addText(" ", "formas", ComunesPDF.JUSTIFIED);

            pdfDoc.addText(" ", "formas", ComunesPDF.CENTER);

            pdfDoc.setTable(2, 40);
            pdfDoc.setCell("RESUMEN DE CARGA DE DATOS \n   CARGA DE DATOS FINALIZADA CORRECTAMENTE", "celda02",
                           ComunesPDF.CENTER, 2);
            pdfDoc.setCell("No. Registros Correctos ", "formas", ComunesPDF.CENTER);
            pdfDoc.setCell("No. Registros Incorrectos ", "formas", ComunesPDF.CENTER);
            pdfDoc.setCell(regCorrectos, "formas", ComunesPDF.CENTER);
            pdfDoc.setCell("0", "formas", ComunesPDF.CENTER);
            pdfDoc.addTable();
            pdfDoc.endDocument();


        } catch (Throwable e) {
            log.error(" Error  getArchivoAcuse (S) " + e);
            throw new AppException("Error al generar el archivo ", e);
        } finally {
            try {
            } catch (Exception e) {
            }
            log.info("  getArchivoAcuse (S) ");
        }
        return nombreArchivo;
    }

    /**
     *
     * @throws netropology.utilerias.AppException
     * @return
     * @param usuario
     */
    private String obtieneCorreos(String ic_if) throws AppException {
        UtilUsr utilUsr = new UtilUsr();
        List varBind = new ArrayList();
        StringBuffer SQL = new StringBuffer();
        String eje_seg = "", eje_promo = "", correos = "";
        AccesoDB con = new AccesoDB();
        boolean commit = true;
        log.info("obtieneCorreos  (E)");

        try {

            con.conexionDB();

            SQL.append(" select CG_EJEC_PROMO , CG_EJEC_SEG  from OPE_DATOS_IF " + "  where ic_if =  ?  ");
            varBind.add(ic_if);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);
            PreparedStatement ps = con.queryPrecompilado(SQL.toString(), varBind);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                eje_seg = rs.getString("CG_EJEC_SEG") == null ? "" : rs.getString("CG_EJEC_SEG");
                eje_promo = rs.getString("CG_EJEC_PROMO") == null ? "" : rs.getString("CG_EJEC_PROMO");
            }
            rs.close();
            ps.close();

            log.debug("eje_seg  " + eje_seg + "  eje_promo  " + eje_promo);

            Usuario usuarioObj = utilUsr.getUsuario(eje_seg);
            String correo_seg = usuarioObj.getEmail();

            usuarioObj = utilUsr.getUsuario(eje_promo);
            String correo_promo = usuarioObj.getEmail();

            correos = correo_seg + "," + correo_promo;

            log.debug("correos  " + correos);


        } catch (Throwable e) {
            commit = false;
            log.error("Error al enviar correo  " + e);
            throw new AppException("Error al obetener corrreo de los Usuarios" + e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("obtieneCorreos (S)");

            log.info("obtieneCorreos(S)");
        }
        return correos;
    }

    /**
     * correo que se notifica que ya se realizo Carga de Empresa Apoyada
     * @param parametros
     */
    public void envioCorreoCargaEmpresas(List parametros) {
        log.info("envioCorreoCargaEmpresas(E)");

        Correo correo = new Correo();
        correo.setCodificacion("charset=UTF-8");
        StringBuffer contenido = new StringBuffer();
        UtilUsr utilUsr = new UtilUsr();

        try {

            String importe = parametros.get(0).toString();
            String moneda = parametros.get(1).toString();
            String no_prestamo = parametros.get(2).toString();
            String fecha = parametros.get(3).toString();
            String hora = parametros.get(4).toString();
            String usuario = parametros.get(5).toString();
            String nombreIF = parametros.get(6).toString();
            String ic_if = parametros.get(7).toString();

            String loginUsuario = this.getConcentrador();
            Usuario usuarioObj = utilUsr.getUsuario(loginUsuario);
            String correoConcentrador = usuarioObj.getEmail();


            String asunto = "Carga de Empresas Apoyadas";
            String destinatario = this.obtieneCorreos(ic_if) + "," + correoConcentrador;

            log.debug("destinatario:::::::::::::::::  " + destinatario);

            contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                             " Estimados (as): " +
                             "<br> Por este conducto, hago de su conocimiento que el d�a de hoy " + fecha +
                             " se registr� la Carga de Empresas Apoyadas de " + nombreIF + " con  No. de Pr�stamo " +
                             no_prestamo + "<br>");

            contenido.append(" <BR> <table width='700' align='JUSTIFIED' border='1'  style='font-size:11pt;font-family: Calibri' >" +
                             " <tr> <td bgcolor='silver' align='left' style='font-size:11pt;font-family: Calibri'> <b> Importe						  </td> <td align='left' style='font-size:11pt;font-family: Calibri'>$" +
                             Comunes.formatoDecimal(String.valueOf(importe), 2) + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' style='font-size:11pt;font-family: Calibri'> <b> Moneda 						  </td> <td align='left' style='font-size:11pt;font-family: Calibri'>" +
                             moneda + "</td></tr> " +
                             " <tr> <td bgcolor='silver' align='left' style='font-size:11pt;font-family: Calibri'> <b> No. de Pr�stamo			  </td> <td align='left' style='font-size:11pt;font-family: Calibri'>" +
                             no_prestamo + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' style='font-size:11pt;font-family: Calibri'> <b> Fecha de Env�o Solicitud  </td> <td align='left' style='font-size:11pt;font-family: Calibri'>" +
                             fecha + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' style='font-size:11pt;font-family: Calibri'> <b> Hora de Env�o 			     </td> <td align='left' style='font-size:11pt;font-family: Calibri'>" +
                             hora + "</td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left'  style='font-size:11pt;font-family: Calibri'><b> Usuario IF 				  </td> <td align='left'  style='font-size:11pt;font-family: Calibri'>" +
                             usuario + "</td> </tr>" + " </table> ");

            contenido.append(" <BR>ATENTAMENTE" + " <BR>Nacional Financiera S.N.C. <BR>");
            contenido.append("</span></span>");

            correo.enviarTextoHTML("no_response@nafin.gob.mx", destinatario, asunto, contenido.toString());


        } catch (Throwable e) {
            log.error("Error al enviar correo  " + e);
            throw new AppException("Error al enviar correo ", e);
        } finally {
            log.info("envioCorreoCargaEmpresas(S)");
        }
    }

    /**
     * Se usa en Caso de Uso 6
     * */
    public ArrayList getListaUsuarios(String tipoAfiliado, String claveAfiliado, String perfil) throws Exception {
        ArrayList listaUsuarios = new ArrayList();
        UtilUsr oUtilUsr = new UtilUsr();
        UtilUsr utilUsr = new UtilUsr();

        List cuentas = utilUsr.getUsuariosxAfiliado(claveAfiliado, tipoAfiliado);

        Iterator itCuentas = cuentas.iterator();
        while (itCuentas.hasNext()) {
            netropology.utilerias.usuarios.Usuario oUsuario = null;
            String login = (String) itCuentas.next();
            oUsuario = oUtilUsr.getUsuario(login);
            if ("".equals(perfil)) {
                listaUsuarios.add(oUsuario);
            } else {
                if (perfil.equals(oUsuario.getPerfil())) {
                    listaUsuarios.add(oUsuario);
                }
            }
        }
        Collections.sort(listaUsuarios);
        return listaUsuarios;
    }

    public List getConsultar(String tipoUsuario, String perfil) throws Exception {
        AccesoDB con = new AccesoDB();
        List informacion = new ArrayList();
        HashMap registros = new HashMap();
        List listaUsuarios = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con.conexionDB();

            listaUsuarios = getListaUsuarios("N", "", perfil);

            if (listaUsuarios.size() > 0) {

                for (int indice = 0; indice < listaUsuarios.size(); indice++) {
                    netropology.utilerias.usuarios.Usuario oUsuario = (Usuario) listaUsuarios.get(indice);
                    int num = 0;
                    String SQLConsulta =
                        "SELECT count(CC_PERFIL) FROM SEG_PERFIL WHERE SC_TIPO_USUARIO = " + tipoUsuario +
                        " AND CC_PERFIL='" + oUsuario.getPerfil() + "'";
                    ps = con.queryPrecompilado(SQLConsulta);
                    rs = ps.executeQuery();
                    ps.clearParameters();
                    if (rs.next()) {
                        num = rs.getInt(1);
                    }
                    ps.close();
                    rs.close();
                    if (num != 0) {
                        registros = new HashMap();
                        registros.put("clave",
                                      oUsuario.getLogin() + " - " + oUsuario.getNombre() + " " +
                                      oUsuario.getApellidoPaterno() + " " + oUsuario.getApellidoMaterno());
                        registros.put("descripcion",
                                      oUsuario.getLogin() + " - " + oUsuario.getNombre() + " " +
                                      oUsuario.getApellidoPaterno() + " " + oUsuario.getApellidoMaterno());
                        informacion.add(registros);
                    }
                }
            }
            return informacion;
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
    }

    public boolean guardaDatosIF(String cve_if, String cuenta, String banco, String clabe, String fec_fir_cont,
                                 String cont_modif, String nom_cont, String fec_fir_ope, String usr_promo,
                                 String tel_promo, String usr_super, String tel_super, String hr_envio, String reenvio,
                                 String operaFirma, String operador, String autorizadorUno, String autorizadorDos,
                                 String mujeresEmpresarias) throws NafinException {

        AccesoDB con = new AccesoDB();
        boolean exito = true;

        String qry =
            "INSERT INTO OPE_DATOS_IF (IC_IF, IC_NUM_CUENTA, CG_BANCO, IC_CTA_CLABE ,	DF_FIRMA_CONTRATO, CG_CONTRATO_OE, DF_FIRMA_CONTRATO_OE,	" +
            " CG_CONT_MODIFICATORIOS, CG_EJEC_PROMO, CG_TEL_PROM0, CG_EJEC_SEG, CG_TEL_SEG,  CG_HORARIO_ENVIO, CG_HORARIO_REENVIO, CG_OPERA_FIRMA_MANC, CG_OPERADOR, CG_AUTORIZADOR_UNO,  CG_AUTORIZADOR_DOS,CS_MUJERES_EMPRESARIAS) " +
            "VALUES (" + cve_if + ", '" + cuenta + "', '" + banco + "', '" + clabe + "', TO_DATE('" + fec_fir_cont +
            "','DD/MM/YYYY hh24:mi:ss'), '" + nom_cont + "', TO_DATE('" + fec_fir_ope + "','DD/MM/YYYY hh24:mi:ss'), " +
            cont_modif + ", '" + usr_promo + "', '" + tel_promo + "', '" + usr_super + "', '" + tel_super + "','" +
            hr_envio + "', '" + reenvio + "','" + operaFirma + "','" + operador + "','" + autorizadorUno + "','" +
            autorizadorDos + "','" + mujeresEmpresarias + "' )  ";

        PreparedStatement psguarda = null;
        try {
            con.conexionDB();

            psguarda = con.queryPrecompilado(qry);

            System.out.println(" ConCargaOpeElec ::qry::   " + qry);

            psguarda.executeUpdate();

            psguarda.close();

        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("ConCargaOpeElec (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

        }
        return exito;
    }

    public boolean actualizaDatosIF(String cve_if, String cuenta, String banco, String clabe, String fec_fir_cont,
                                    String cont_modif, String nom_cont, String fec_fir_ope, String usr_promo,
                                    String tel_promo, String usr_super, String tel_super, String hr_envio,
                                    String reenvio, String operaFirma, String operador, String autorizadorUno,
                                    String autorizadorDos, String mujeresEmpresarias) throws NafinException {

        AccesoDB con = new AccesoDB();
        boolean exito = true;

        String qry =
            "UPDATE OPE_DATOS_IF SET IC_NUM_CUENTA = '" + cuenta + "', CG_BANCO = '" + banco + "', IC_CTA_CLABE = '" +
            clabe + "',	DF_FIRMA_CONTRATO = TO_DATE('" + fec_fir_cont +
            "','DD/MM/YYYY hh24:mi:ss'), CG_CONTRATO_OE = '" + nom_cont + "',	" + "DF_FIRMA_CONTRATO_OE = TO_DATE('" +
            fec_fir_ope + "','DD/MM/YYYY hh24:mi:ss'), CG_CONT_MODIFICATORIOS = " + cont_modif + ", CG_EJEC_PROMO = '" +
            usr_promo + "', CG_TEL_PROM0 = '" + tel_promo + "', CG_EJEC_SEG = '" + usr_super + "', CG_TEL_SEG = '" +
            tel_super + "', " + " CG_HORARIO_ENVIO = '" + hr_envio + "', CG_HORARIO_REENVIO = '" + reenvio +
            "', CG_OPERA_FIRMA_MANC='" + operaFirma + "', CG_OPERADOR='" + operador + "', CG_AUTORIZADOR_UNO ='" +
            autorizadorUno + "', CG_AUTORIZADOR_DOS='" + autorizadorDos + "', CS_MUJERES_EMPRESARIAS='" +
            mujeresEmpresarias + "'" + " WHERE IC_IF = " + cve_if;

        PreparedStatement psactualiza = null;
        try {
            con.conexionDB();

            psactualiza = con.queryPrecompilado(qry);

            System.out.println(" ConCargaOpeElec ::qry::   " + qry);

            psactualiza.executeUpdate();

            psactualiza.close();

        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("ConCargaOpeElec (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

        }
        return exito;
    }

    public void guardaFecMod(String cve_if, String desc, String fec_fir_modif) {
        AccesoDB con = new AccesoDB();
        boolean exito = true;

        String qry =
            "INSERT INTO OPE_DET_FIR_MODIFI " + "VALUES ( SEQ_OPE_DET_FIR_MODIFI.NEXTVAL, " + cve_if + ", '" + desc +
            "', TO_DATE ('" + fec_fir_modif + "', 'DD/MM/YYYY hh24:mi:ss') )  ";

        PreparedStatement psguarda = null;
        try {
            con.conexionDB();

            psguarda = con.queryPrecompilado(qry);

            System.out.println(" ConCargaOpeElec ::qry::   " + qry);

            psguarda.executeUpdate();

            psguarda.close();

        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("ConCargaOpeElec (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
        }

    }

    public String getModificatorios(String clave_if) {
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        PreparedStatement ps = null;
        String num = "";
        List lVarBind = new ArrayList();
        log.info("getModificatorios (E)");
        try {
            con.conexionDB();

            String strQry =
                "select CG_CONT_MODIFICATORIOS as TOTALMOD " + "  FROM OPE_DATOS_IF " + " WHERE ic_if = ?  ";

            lVarBind = new ArrayList();
            lVarBind.add(new Long(clave_if));
            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();

            if (rs.next()) {
                num = rs.getString("TOTALMOD");
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
        }
        log.info("getModificatorios (S)");
        return num;
    }

    public List cargaFecMod(String clave_if, int filas, int inicio) {
        AccesoDB con = new AccesoDB();
        List informacion = new ArrayList();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con.conexionDB();

            String strQry =
                " SELECT cg_descripcion as DESCRIPCION, TO_CHAR (df_firma_modificatoria, 'dd/mm/yyyy') as FECHAMOD, 'S' as EDO " +
                "  FROM OPE_DET_FIR_MODIFI " + "  WHERE ic_if = ? ORDER BY ic_firma_modifi ";

            lVarBind = new ArrayList();
            lVarBind.add(new Long(clave_if));
            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap registros = new HashMap();
                registros.put("EDO", rs.getString("EDO"));
                registros.put("FECHAMOD", rs.getString("FECHAMOD"));
                registros.put("DESCRIPCION", rs.getString("DESCRIPCION"));
                informacion.add(registros);
            }
            rs.close();
            ps.close();
            for (int i = 1; i <= filas; i++) {
                HashMap registros1 = new HashMap();
                registros1.put("EDO", "N");
                registros1.put("FECHAMOD", "");
                registros1.put("DESCRIPCION", "Fecha Firma Modificatorio " + (inicio + i));
                informacion.add(registros1);
            }

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return informacion;
    }

    /**
     *  se usa en  Caso de Uso 7
     * Metodo para Consulta de datos de contratos
     * @throws netropology.utilerias.AppException
     * @return Lista con los datos
     */
    public List cargaNomContratos() {
        AccesoDB con = new AccesoDB();
        List informacion = new ArrayList();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con.conexionDB();

            String strQry =
                " SELECT ic_contrato as IC_CONT, cg_perfl as PERFIL_IF, cg_descripcion as DESC_RIESGO, cg_nombre_contrato as NOM_CONT " +
                "  FROM opecat_contrato ";

            lVarBind = new ArrayList();
            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap registros = new HashMap();
                registros.put("IC_CONT", rs.getString("IC_CONT"));
                registros.put("PERFIL_IF", rs.getString("PERFIL_IF"));
                registros.put("DESC_RIESGO", rs.getString("DESC_RIESGO"));
                registros.put("NOM_CONT", rs.getString("NOM_CONT"));
                informacion.add(registros);
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return informacion;
    }

    public boolean actualizaNombreContratos(String cveCont, String nomCont) {

        AccesoDB con = new AccesoDB();
        boolean exito = true;

        String qry =
            "UPDATE opecat_contrato SET CG_NOMBRE_CONTRATO = '" + nomCont + "'" + "WHERE IC_CONTRATO = " + cveCont;

        PreparedStatement psactualiza = null;
        try {
            con.conexionDB();

            psactualiza = con.queryPrecompilado(qry);

            System.out.println(" actualizaNombreContratos ::qry::   " + qry);

            psactualiza.executeUpdate();

            psactualiza.close();

        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("actualizaNombreContratos (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

        }
        return exito;
    }

    /**
     * Se usa en Caso de Uso 08
     * */
    public boolean guardaParamGrales(String usr_sub, String tel_sub, String usr_con, String tel_con,
                                     String mails) throws NafinException {

        AccesoDB con = new AccesoDB();
        boolean exito = true;

        String qry =
            "INSERT INTO OPE_PARAMGENERALES  " + "VALUES (SEQ_OPE_PARAMGENERALES.NEXTVAL, '" + mails + "', '" +
            usr_sub + "', '" + tel_sub + "', '" + usr_con + "', '" + tel_con + "' )  ";

        PreparedStatement psguarda = null;
        try {
            con.conexionDB();

            psguarda = con.queryPrecompilado(qry);

            System.out.println(" guardaParamGrales ::qry::   " + qry);

            psguarda.executeUpdate();

            psguarda.close();

        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("guardaParamGrales (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

        }
        return exito;
    }

    /**
     * Actualiza TODOS los campos de la tabla OPE_PARAMGENERALES
     *
     * @throws com.netro.exception.NafinException
     * @return true si la actualizaci�n fue exitosa.
     * @param emailFondoLiquido
     * @param emailDirector
     * @param telefonoConcentradorDos
     * @param concentradorDos
     * @param telefonoConcentrador
     * @param concentrador
     * @param telefonoSubdirector
     * @param subDirector
     * @param icEmail
     */
    public boolean actualizaParamGrales(String subDirector, String telefonoSubdirector, String concentrador,
                                        String telefonoConcentrador, String concentradorDos,
                                        String telefonoConcentradorDos, String emailDirector, String emailFondoLiquido,
                                        String icEmail) throws NafinException {

        AccesoDB con = new AccesoDB();
        boolean exito = true;

        String qry =
            " UPDATE OPE_PARAMGENERALES SET CG_SUBDIRECTOR ='" + subDirector + "', CG_TEL_SUBDIRECTOR = '" +
            telefonoSubdirector + "', CG_CONCENTRADOR = '" + concentrador + "', CG_TEL_CONCENTRADOR = '" +
            telefonoConcentrador + "', CG_CONCENTRADOR2 ='" + concentradorDos + "', CG_TEL_CONCENTRADOR2 = '" +
            telefonoConcentradorDos + "', CG_EMAIL_DIRECTOR ='" + emailDirector + "', CG_MAIL ='" + emailFondoLiquido +
            "' WHERE IC_EMAIL=" + icEmail;

        PreparedStatement psactualiza = null;
        try {
            con.conexionDB();

            psactualiza = con.queryPrecompilado(qry);

            System.out.println(" actualizaParamGrales ::qry::   " + qry);

            psactualiza.executeUpdate();

            psactualiza.close();

        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("actualizaParamGrales (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

        }
        return exito;
    }

    /**
     * Obtiene todos los campos de la tabla OPE_PARAMGENERALES
     * @return Lista informacion.
     */
    public List cargaDatosGrales() {
        AccesoDB con = new AccesoDB();
        List informacion = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con.conexionDB();

            String strQry = " SELECT * " + "  FROM OPE_PARAMGENERALES " + "  WHERE ic_email = 1 ";

            ps = con.queryPrecompilado(strQry);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap registros = new HashMap();
                registros.put("IC_MAIL", rs.getString("ic_email"));
                registros.put("MAILS", rs.getString("cg_mail"));
                registros.put("USR_SUB", rs.getString("cg_subdirector"));
                registros.put("TEL_SUB", rs.getString("cg_tel_subdirector"));
                registros.put("USR_CON", rs.getString("cg_concentrador"));
                registros.put("TEL_CON", rs.getString("cg_tel_concentrador"));
                registros.put("USR_CON_DOS", rs.getString("cg_concentrador2"));
                registros.put("TEL_CON_DOS", rs.getString("cg_tel_concentrador2"));
                registros.put("EMAIL_DIR", rs.getString("cg_email_director"));
                informacion.add(registros);
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return informacion;
    }

    /**
     *  Obtiene el icFinanciera del intermediario Financiera
     * @return el ic_financiera del IF

     * @param ic_if
     */
    public String getIcFinanciera(String icIf) {
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        PreparedStatement ps = null;
        String icFinanciera = "";
        List lVarBind = new ArrayList();
        log.info("getIcFinanciera (E)");
        try {
            con.conexionDB();

            String strQry = "select IC_FINANCIERA as IC_FINANCIERA from comcat_if  where ic_if = ? ";
            lVarBind = new ArrayList();
            lVarBind.add(new Integer(icIf));
            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();

            if (rs.next() && rs != null) {
                icFinanciera = (rs.getString("IC_FINANCIERA") == null ? "-1" : rs.getString("IC_FINANCIERA"));
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            log.error("Error en  getIcFinanciera (S)" + e);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
        }
        log.info("getIcFinanciera (S)");
        return icFinanciera;
    }

    /**
     *  Verifica si la fecha del periodo ya fue procesada
     * @return el resultado

     * @param fecha a verificar
     *
     */
    public String fechaProcesada(String fecha, String ifFinanciera) {
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        PreparedStatement ps = null;
        String fechaProcesada = "";
        List lVarBind = new ArrayList();
        log.info("fechaProcesada (E)");
        try {
            con.conexionDB();

            String strQry =
                " SELECT COUNT(*) as contador " + " FROM CCBE_IFNB_FILES " +
                " WHERE to_char(IFL_PERIOD,'mm/yyyy') = ? " + " AND   IFL_IFNB_CODE = ? ";
            lVarBind = new ArrayList();
            lVarBind.add(fecha);
            lVarBind.add(ifFinanciera);
            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();

            if (rs.next() && rs != null) {
                fechaProcesada = (rs.getString("CONTADOR") == null ? "-1" : rs.getString("CONTADOR"));
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            log.error("Error en  getIcFinanciera (S)" + e);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
        }
        log.info("getIcFinanciera (S)");
        return fechaProcesada;
    }

    /**
     *  Este metodo hace el primer barrido al archivo de captura de cartera
     * @return el resultado en un mapa

     * @param el nombre del archivo y su directorio
     *
     */
    public HashMap getValidacionInicial(String dataFile, String path) {
        HashMap retorno = new HashMap();
        int line_number = 0;
        int total_num_records = 0;
        int disc_num_records = 0;
        double total_balance = 0;
        double disc_balance = 0;
        BufferedReader brt = null;
        String existeError = "N";
        String existeError2 = "N";
        //Constantes
        final int DATAFILE_FIELD_RECORDTYPE = 1;
        final int DATAFILE_RECORD_HEADER = 1;
        final int DATAFILE_RECORD_DETAIL = 2;
        final int DATAFILE_FIELD_DISCOUNTID = 3;
        final int DATAFILE_FIELD_BALANCE = 20;
        final int DATAFILE_RECORD_PRINCIPAL = 3;
        final int DATAFILE_RECORD_EXPIRED = 4;
        final int DATAFILE_RECORD_RELATED = 5;
        //Fin COnstantes
        try {


            String linea = "";
            java.io.File ft = new java.io.File(path + dataFile);
            brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
            //Ciclo para recorrer todas las lineas del archivo
            HashMap hmReg = new HashMap();
            while ((linea = brt.readLine()) != null) {
                line_number++;
                //String line = rawFile.readln();
                if (linea.length() > 0) {
                    Integer recordType = new Integer(getToken(linea, DATAFILE_FIELD_RECORDTYPE, "|"));
                    if (linea.length() > 0 && line_number > 1) {
                        String discountAx = getToken(linea, DATAFILE_FIELD_DISCOUNTID, "|");
                        if (!discountAx.equals("D") && !discountAx.equals("L") && !discountAx.equals("P")) {
                            existeError = "S";
                        }
                        if (discountAx.equals("D") || discountAx.equals("L") || discountAx.equals("P")) {
                            existeError2 = "S";
                        }
                    }
                    switch (recordType.intValue()) {
                    case DATAFILE_RECORD_HEADER:
                        continue;
                    case DATAFILE_RECORD_DETAIL:
                        {
                            hmReg = new HashMap();
                            String discount = getToken(linea, DATAFILE_FIELD_DISCOUNTID, "|");
                            Double balance = new Double(getToken(linea, DATAFILE_FIELD_BALANCE, "|"));
                            total_num_records += 1;
                            total_balance += balance.doubleValue();
                            if (discount.equals("D") || discount.equals("L")) {
                                disc_num_records += 1;
                                disc_balance += balance.doubleValue();
                            }
                        }
                    case DATAFILE_RECORD_PRINCIPAL:
                    case DATAFILE_RECORD_EXPIRED:
                    case DATAFILE_RECORD_RELATED:
                        break;
                    }
                } else {
                    //flgOut = true;
                }
            }
            brt.close();
            retorno.put("EXISTEERROR", existeError);
            retorno.put("EXISTEERROR2", existeError2);
            retorno.put("TOTAL_REGISTROS", total_num_records + "");
            retorno.put("TOTAL_BALANCE", total_balance + "");
            retorno.put("DISC_NUM_RECORDS", disc_num_records + "");
            retorno.put("DISC_BALANCE", disc_balance + "");

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new AppException("?", ex);
        }
        return retorno;
    }

    private static String getToken(String str, int pos, String del) {
        String token = "";
        List tokens = new ArrayList();
        String dato = "";
        int count = 1;
        for (int strpos = 0; strpos < str.length(); strpos++) {
            String chr = str.substring(strpos, strpos + 1);
            if (!chr.equals("|")) {
                dato += chr;
            } else {
                tokens.add(dato);
                dato = "";
                count++;
            }
        }
        tokens.add(dato);
        if (pos > count) {
            token = "";
        } else {
            token = (String) tokens.get(pos - 1);
        }
        return token.replace('\'', '�');
    }


    /**
     * Metodo para insertar el detalle del archivo en la BD  """"" this this this this """"
     */
    public void insertRecordDetail(String periodDate, int ifnbCode, String file, String path) {
        AccesoDB con = new AccesoDB();
        PreparedStatement stmt = null;
        PreparedStatement stmt1 = null;
        PreparedStatement stmt2 = null;
        Statement stmt4 = null;
        ResultSet rset = null;
        PrintStream ps = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedReader brt = null;

        // Constantes
        final int DATAFILE_RECORD_HEADER = 1;
        final int DATAFILE_RECORD_DETAIL = 2;
        final int DATAFILE_FIELD_RECORDTYPE = 1;
        final int DATAFILE_FIELD_RECORDNUM = 2;
        int DATAFILE_FIELD_DISCOUNTID = 3;
        int DATAFILE_FIELD_ACCNAME = 4;
        int DATAFILE_FIELD_IFNBCREDIT = 5;
        int DATAFILE_FIELD_NAFINCREDIT = 6;
        int DATAFILE_FIELD_GRANTDATE = 7;
        int DATAFILE_FIELD_GRANTAMOUNT = 8;
        int DATAFILE_FIELD_CURRENCYID = 9;
        int DATAFILE_FIELD_INITIALTERM = 10;
        int DATAFILE_FIELD_RATETYPE = 11;
        int DATAFILE_FIELD_OVERRATE = 12;
        int DATAFILE_FIELD_RESTRUCTUREID = 13;
        int DATAFILE_FIELD_RESTRUCTUREDATE = 14;
        int DATAFILE_FIELD_CURRENTCAPITAL = 15;
        int DATAFILE_FIELD_CURRENTINT = 16;
        int DATAFILE_FIELD_EXPIREDCAPITAL = 17;
        int DATAFILE_FIELD_EXPIREDINT = 18;
        int DATAFILE_FIELD_PENALTYINT = 19;
        //int DATAFILE_FIELD_BALANCE = 20;
        int DATAFILE_FIELD_EXPIREMONTHS = 21;
        int DATAFILE_FIELD_IFNBPAIDID = 22;
        int DATAFILE_FIELD_IFNBPAIDAMOUNT = 23;
        int DATAFILE_FIELD_COMMONRISKID = 24;
        int DATAFILE_FIELD_RELATEDID = 25;
        int DATAFILE_FIELD_PCTIFNBCAPITAL = 26;
        int DATAFILE_FIELD_GUARANTEETYPE = 27;
        int DATAFILE_FIELD_CREATEDRESERVE = 28;
        int DATAFILE_FIELD_RECOVERYSITUATION = 29;
        int DATAFILE_FIELD_BALANCE_WITHOUT_CHARGES = 31;
        int DATAFILE_FIEEL_COMPRENDA = 33; //Nuevos Ajustes

        final int DATAFILE_RECORD_PRINCIPAL = 3;
        final int DATAFILE_RECORD_RELATED = 5;
        final int DATAFILE_FIELD_GUARANTEETYPE2 = 7;
        final int DATAFILE_FIELD_CREATERESERVE2 = 8;
        final int DATAFILE_FIELD_GUARANTEEPRIORITY = 9;
        final int DATAFILE_FIELD_GUARANTEEGRADE = 10;
        final int DATAFILE_FIELD_RECOVERYSITUATION2 = 11;

        final int DATAFILE_RECORD_EXPIRED = 4;
        //

        boolean flgOut = false;
        int line_number = 0;
        try {
            con.conexionDB();
            //this.insertRecord(periodDate, ifnbCode, conn);
            String linea = "";
            java.io.File ft = new java.io.File(path + file);
            brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));
            ps = new PrintStream(baos);
            String sqlsentence =
                " INSERT INTO CCBE_FILES_CONTENT ( FCO_RECORD_NUM, IFL_FILE_CODE," +
                " FCO_DISCOUNT_ID, FCO_ACC_NAME, FCO_IFNB_CREDIT, FCO_NAFIN_CREDIT," +
                " FCO_GRANT_DATE, FCO_GRANT_AMOUNT, FCO_CURRENCY_ID, FCO_INITIAL_TERM," +
                " FCO_RATE_TYPE, FCO_OVERRATE, FCO_RESTRUCTURE_ID, FCO_RESTRUCTURE_DATE," +
                " FCO_CURRENT_CAPITAL, FCO_CURRENT_INT, FCO_EXPIRED_CAPITAL, FCO_EXPIRED_INT," +
                " FCO_PENALTY_INT, FCO_BALANCE, FCO_EXPIRED_MONTHS, FCO_IFNB_PAID_ID," +
                " FCO_IFNB_PAID_AMOUNT, FCO_COMMON_RISK_ID, FCO_RELATED_ID, FCO_PCT_IFNB_CAPITAL," +
                " FCO_GUARANTEE_TYPE, FCO_CREATED_RESERVE, FCO_RECOVERY_SITUATION,FCO_CONPRENDA) " +
                " VALUES (?, ?, ?, ?, ?, ?, TO_DATE(?, 'dd/mm/yyyy'), ?, ?, ?, ?, ?, ?, TO_DATE(?, 'dd/mm/yyyy'), ?, ?, ?, ?, ?, ?, " +
                "  ?, ?, ?, ?, ?, ?, ?, ?, ? , ? ) ";
            String sqlupdate3y5 =
                " UPDATE CCBE_FILES_CONTENT SET FCO_GUARANTEE_TYPE = ?," +
                " FCO_CREATED_RESERVE = ?, FCO_GUARANTEE_PRIORITY = ?, FCO_GUARANTEE_GRADE = ? " +
                " WHERE IFL_FILE_CODE = ? AND FCO_RECORD_NUM = ? ";

            String sqlupdate4 =
                " UPDATE CCBE_FILES_CONTENT SET FCO_GUARANTEE_TYPE = ?," +
                " FCO_CREATED_RESERVE = ?, FCO_GUARANTEE_PRIORITY = ?, FCO_GUARANTEE_GRADE = ?," +
                " FCO_RECOVERY_SITUATION = ? WHERE IFL_FILE_CODE = ? AND FCO_RECORD_NUM = ? ";
            stmt = con.queryPrecompilado(sqlsentence);
            stmt1 = con.queryPrecompilado(sqlupdate3y5);
            stmt2 = con.queryPrecompilado(sqlupdate4);
            //Ciclo para recorrer todas las lineas del archivo
            int file_code = 0;

            String sql =
                " SELECT IFL_FILE_CODE " + " FROM CCBE_IFNB_FILES " + " WHERE to_char(IFL_PERIOD,'mm/yyyy') = '" +
                periodDate + "' " + " AND   IFL_IFNB_CODE = " + ifnbCode;
            stmt4 = con.queryPrecompilado(sql);
            rset = stmt4.executeQuery(sql);
            if (rset.next()) {
                file_code = rset.getInt(1);
            }
            while ((linea = brt.readLine()) != null) {
                line_number++;
                if (linea.length() > 0) {
                    Integer recordType =
                        new Integer(OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RECORDTYPE, "|"));
                    switch (recordType.intValue()) {
                    case DATAFILE_RECORD_HEADER:
                        continue;
                    case DATAFILE_RECORD_DETAIL:
                        {
                            String record_num = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RECORDNUM, "|");
                            String discount = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_DISCOUNTID, "|");
                            String acc_name = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_ACCNAME, "|");
                            String ifnb_credit =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_IFNBCREDIT, "|");
                            String nafin_credit =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_NAFINCREDIT, "|");
                            String grant_date = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_GRANTDATE, "|");
                            String grant_amount =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_GRANTAMOUNT, "|");
                            String currency_id =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_CURRENCYID, "|");
                            String initial_term =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_INITIALTERM, "|");
                            String rate_type = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RATETYPE, "|");
                            String overrate = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_OVERRATE, "|");
                            String reestructure =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RESTRUCTUREID, "|");
                            String restructure_date =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RESTRUCTUREDATE, "|");
                            String current_capital =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_CURRENTCAPITAL, "|");
                            String current_int =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_CURRENTINT, "|");
                            String exp_capital =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_EXPIREDCAPITAL, "|");
                            String exp_int = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_EXPIREDINT, "|");
                            String penalty_int =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_PENALTYINT, "|");
                            String expired_months =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_EXPIREMONTHS, "|");
                            String paid_id = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_IFNBPAIDID, "|");
                            String paid_amount =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_IFNBPAIDAMOUNT, "|");
                            String risk = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_COMMONRISKID, "|");
                            String related_id = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RELATEDID, "|");
                            String ifnb_capital =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_PCTIFNBCAPITAL, "|");
                            String guar_type =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_GUARANTEETYPE, "|");
                            String created_reserve =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_CREATEDRESERVE, "|");
                            String rec_situation =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RECOVERYSITUATION, "|");
                            String balance_charges =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_BALANCE_WITHOUT_CHARGES, "|");
                            String comprenda = OperacionElectronicaBean.getToken(linea, DATAFILE_FIEEL_COMPRENDA, "|");
                            if (record_num.equals(""))
                                stmt.setNull(1, Types.INTEGER);
                            else
                                stmt.setInt(1, Integer.parseInt(record_num));

                            stmt.setInt(2, file_code);

                            if (discount.equals(""))
                                stmt.setNull(3, Types.INTEGER);
                            else {
                                if (discount.equals("D")) {
                                    stmt.setInt(3, 1);
                                } else if (discount.equals("P")) {
                                    stmt.setInt(3, 0);
                                } else {
                                    stmt.setInt(3, 2);
                                }
                            }
                            stmt.setString(4, acc_name);
                            if (ifnb_credit.equals(""))
                                stmt.setNull(5, Types.VARCHAR);
                            else
                                stmt.setString(5, ifnb_credit);
                            if (nafin_credit.equals(""))
                                stmt.setNull(6, Types.INTEGER);
                            else
                                stmt.setInt(6, Integer.parseInt(nafin_credit));
                            if (grant_date.equals(""))
                                stmt.setString(7, "");
                            else
                                stmt.setString(7, grant_date);
                            if (grant_amount.equals(""))
                                stmt.setNull(8, Types.DOUBLE);
                            else
                                stmt.setDouble(8, Double.parseDouble(grant_amount));
                            if (currency_id.equals(""))
                                stmt.setInt(9, 0);
                            else
                                stmt.setInt(9, 1);
                            if (initial_term.equals(""))
                                stmt.setNull(10, Types.INTEGER);
                            else
                                stmt.setInt(10, Integer.parseInt(initial_term));
                            if (rate_type.equals(""))
                                stmt.setNull(11, Types.VARCHAR);
                            else
                                stmt.setString(11, rate_type);
                            if (overrate.equals(""))
                                stmt.setNull(12, Types.DOUBLE);
                            else
                                stmt.setDouble(12, Double.parseDouble(overrate));
                            if (reestructure.equals(""))
                                stmt.setNull(13, Types.INTEGER);
                            else
                                stmt.setInt(13, Integer.parseInt(reestructure));
                            if (restructure_date.equals(""))
                                stmt.setString(14, "");
                            else
                                stmt.setString(14, restructure_date);
                            if (current_capital.equals(""))
                                stmt.setNull(15, Types.DOUBLE);
                            else
                                stmt.setDouble(15, Double.parseDouble(current_capital));
                            if (current_int.equals(""))
                                stmt.setNull(16, Types.DOUBLE);
                            else
                                stmt.setDouble(16, Double.parseDouble(current_int));
                            if (exp_capital.equals(""))
                                stmt.setNull(17, Types.DOUBLE);
                            else
                                stmt.setDouble(17, Double.parseDouble(exp_capital));
                            if (exp_int.equals(""))
                                stmt.setNull(18, Types.DOUBLE);
                            else
                                stmt.setDouble(18, Double.parseDouble(exp_int));
                            if (penalty_int.equals(""))
                                stmt.setNull(19, Types.DOUBLE);
                            else
                                stmt.setDouble(19, Double.parseDouble(penalty_int));
                            if (balance_charges.equals(""))
                                stmt.setNull(20, Types.DOUBLE);
                            else
                                stmt.setDouble(20, Double.parseDouble(balance_charges));
                            stmt.setInt(21, Integer.parseInt(expired_months));
                            if (paid_id.equals(""))
                                stmt.setInt(22, 0);
                            else
                                stmt.setInt(22, 1);
                            if (paid_amount.equals(""))
                                stmt.setNull(23, Types.DOUBLE);
                            else
                                stmt.setDouble(23, Double.parseDouble(paid_amount));
                            stmt.setString(24, risk);
                            if (related_id.equals(""))
                                stmt.setInt(25, 0);
                            else
                                stmt.setInt(25, 1);
                            if (ifnb_capital.equals(""))
                                stmt.setNull(26, Types.DOUBLE);
                            else
                                stmt.setDouble(26, Double.parseDouble(ifnb_capital));
                            stmt.setString(27, guar_type);
                            if (created_reserve.equals(""))
                                stmt.setNull(28, Types.DOUBLE);
                            else
                                stmt.setDouble(28, Double.parseDouble(created_reserve));
                            stmt.setString(29, rec_situation);

                            if (comprenda.equals(""))
                                stmt.setNull(30, Types.INTEGER);
                            else if (comprenda.equals("S")) {
                                stmt.setInt(30, 1);
                            } else {
                                stmt.setInt(30, 0);
                            }

                            stmt.executeUpdate();
                            stmt.clearParameters();
                        }
                        break;
                    case DATAFILE_RECORD_PRINCIPAL:
                    case DATAFILE_RECORD_RELATED:
                        {
                            String rec_num = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RECORDNUM, "|");
                            String guar_type_2 =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_GUARANTEETYPE2, "|");
                            String created_reserve_2 =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_CREATERESERVE2, "|");
                            String guar_priority =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_GUARANTEEPRIORITY, "|");
                            String guar_grade =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_GUARANTEEGRADE, "|");
                            stmt1.setString(1, guar_type_2);
                            if (created_reserve_2.equals(""))
                                stmt1.setNull(2, Types.INTEGER);
                            else
                                stmt1.setDouble(2, Double.parseDouble(created_reserve_2));
                            stmt1.setInt(3, Integer.parseInt(guar_priority));
                            stmt1.setInt(4, Integer.parseInt(guar_grade));
                            stmt1.setInt(5, file_code);
                            if (rec_num.equals(""))
                                stmt1.setNull(6, Types.INTEGER);
                            else
                                stmt1.setInt(6, Integer.parseInt(rec_num));

                            stmt1.executeUpdate();
                            stmt1.clearParameters();
                        }
                        break;
                    case DATAFILE_RECORD_EXPIRED:
                        {
                            String rec_num = OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RECORDNUM, "|");
                            String guar_type_2 =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_GUARANTEETYPE2, "|");
                            String created_reserve_2 =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_CREATERESERVE2, "|");
                            String guar_priority =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_GUARANTEEPRIORITY, "|");
                            String guar_grade =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_GUARANTEEGRADE, "|");
                            String rec_situation_2 =
                                OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RECOVERYSITUATION2, "|");
                            stmt2.setString(1, guar_type_2);
                            if (created_reserve_2.equals(""))
                                stmt2.setNull(2, Types.INTEGER);
                            else
                                stmt2.setDouble(2, Double.parseDouble(created_reserve_2));

                            stmt2.setInt(3, Integer.parseInt(guar_priority));
                            stmt2.setInt(4, Integer.parseInt(guar_grade));
                            stmt2.setString(5, rec_situation_2);
                            stmt2.setInt(6, file_code);
                            if (rec_num.equals(""))
                                stmt2.setNull(7, Types.INTEGER);
                            else
                                stmt2.setInt(7, Integer.parseInt(rec_num));
                            stmt2.executeUpdate();
                            stmt2.clearParameters();
                        }
                        break;
                    }
                } else {
                    flgOut = true;
                }
            }
            con.terminaTransaccion(true);
            brt.close();
        } catch (java.sql.SQLException se) {
            con.terminaTransaccion(false);
            log.error("Error en  getIcFinanciera (S)" + se);
            throw new AppException(baos.toString() + "Error en la linea " + line_number + "\n" + se.getMessage(), se);
        } catch (NumberFormatException ne) {
            con.terminaTransaccion(false);
            log.error("Error en  getIcFinanciera (S)" + ne);
            throw new AppException(baos.toString() + "Error en la linea " + line_number + "\n" + ne.getMessage(), ne);
        } catch (Exception ex) {

            con.terminaTransaccion(false);
            log.error("Error en  getIcFinanciera (S)" + ex);
            throw new AppException("Error en la linea " + line_number + "\n" + ex.getMessage() + baos.toString(), ex);

        } finally {
            if (rset != null)
                try {
                    rset.close();
                } catch (Exception ignore) {
                }
            if (stmt != null)
                try {
                    stmt.close();
                } catch (Exception ignore) {
                }
            if (stmt1 != null)
                try {
                    stmt1.close();
                } catch (Exception ignore) {
                }
            if (stmt2 != null)
                try {
                    stmt2.close();
                } catch (Exception ignore) {
                }
            if (stmt4 != null)
                try {
                    stmt4.close();
                } catch (Exception ignore) {
                }
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
        }
        return;
    }

    public String getIfnbNombre(int ifnbCode) {
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        PreparedStatement ps = null;
        String nombreIFNB = "";
        List lVarBind = new ArrayList();
        log.info("getIfnbNombre (E)");
        try {
            con.conexionDB();

            String strQry = " SELECT DESCRIPCION " + " FROM DESC_COMITES " + " WHERE CODIGO = ? " + " AND TIPO = 3 ";
            lVarBind = new ArrayList();
            lVarBind.add(ifnbCode + "");

            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();

            if (rs.next() && rs != null) {
                nombreIFNB = (rs.getString("DESCRIPCION") == null ? "-1" : rs.getString("DESCRIPCION"));
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            log.error("Error en  getIfnbNombre (S)" + e);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
        }
        log.info("getIfnbNombre (S)");
        return nombreIFNB;
    }

    public String getIfnbNumeroArchivo(int ifnbCode, String periodDate) {
        AccesoDB con = new AccesoDB();
        ResultSet rs = null;
        PreparedStatement ps = null;
        String numeroIFNB = "";
        List lVarBind = new ArrayList();
        log.info("getIfnbNombre (E)");
        try {
            con.conexionDB();

            String strQry =
                " SELECT IFL_FILE_CODE " + " FROM CCBE_IFNB_FILES " + " WHERE to_char(IFL_PERIOD,'mm/yyyy') = ? " +
                " AND   IFL_IFNB_CODE = ? ";
            lVarBind = new ArrayList();
            lVarBind.add(periodDate);
            lVarBind.add(ifnbCode + "");

            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();

            if (rs.next() && rs != null) {
                numeroIFNB = (rs.getString("IFL_FILE_CODE") == null ? "-1" : rs.getString("IFL_FILE_CODE"));
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            log.error("Error en  getIfnbNombre (S)" + e);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
        }
        log.info("getIfnbNombre (S)");
        return numeroIFNB;
    }

    public void insertRecord(String periodDate, int ifnbCode) {
        AccesoDB con = new AccesoDB();

        final int STATUS_CONTENT_VALIDATED = 1;
        Statement stmt = null;
        ResultSet rset = null;
        try {
            con.conexionDB();
            String sqlsentence =
                " INSERT INTO CCBE_IFNB_FILES " + " (IFL_IFNB_CODE, IFL_PERIOD, IFL_STATUS_CODE, " +
                "  IFL_LOAD_DATE) VALUES (" + ifnbCode + ", " + "  LAST_DAY(TO_DATE('" + periodDate +
                "','mm/yyyy')), " + STATUS_CONTENT_VALIDATED + ", " + "  SYSDATE) ";
            stmt = con.queryPrecompilado(sqlsentence);
            stmt.executeUpdate(sqlsentence);
        } catch (Exception ex) {
            ex.printStackTrace();
            con.terminaTransaccion(false);
            throw new AppException(ex.getMessage(), ex);
        } finally {
            if (rset != null)
                try {
                    rset.close();
                } catch (Exception ignore) {
                }
            if (stmt != null)
                try {
                    stmt.close();
                } catch (Exception ignore) {
                }
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true);
                con.cierraConexionDB();
            }
        }
        return;
    }

    /**
     * Metodo para validar codigo del IFNB del archivo
     */
    public boolean checkIfnbCode(int ifnbCode, String archivo)

        {
        String linea = "";
        BufferedReader brt = null;
        boolean banderaRetorno = true;
        final int DATAFILE_FIELD_RECORDTYPE = 1;
        final int DATAFILE_RECORD_HEADER = 1;
        final int DATAFILE_FIELD_IFNBCODE = 4;
        //ps = new PrintStream(baos);
        try {
            java.io.File ft = new java.io.File(archivo);
            brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));


            if ((linea = brt.readLine()) == null) {
                banderaRetorno = false;
            }
            Integer recordType = new Integer(OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_RECORDTYPE, "|"));
            if (recordType.intValue() == DATAFILE_RECORD_HEADER) {
                int ifnbCode2 =
                    Integer.parseInt(OperacionElectronicaBean.getToken(linea, DATAFILE_FIELD_IFNBCODE, "|"));
                if (ifnbCode != ifnbCode2) {
                    banderaRetorno = false;
                }
            }

            brt.close();
        } catch (NumberFormatException ex) {
            banderaRetorno = false;
        } catch (Exception ex) {
            ex.printStackTrace();
            banderaRetorno = false;
        }

        return banderaRetorno;
    }

    //*************************** F035 - 2014 -- Segunda Etapa Operacion Electronica *********************************
    // PANTALLA A: IF 4CP/IF 5CP - OPERACI�N ELECTR�NICA IFNBS - CAPTURAS - ACTUALIZACI�N DE LA CARTERA PRENDARIA

    private boolean validaCampoObligatorio(String numLinea, String campo, String mensajeError, ValidacionResult vr) {

        boolean valido = true;
        if (campo == null || campo.matches("^\\s*$")) {

            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            //vr.appendErrores(" ");
            //vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

            valido = false;
        }
        return valido;

    }

    private boolean validarLongitud(String numLinea, String campo, String mensajeError, ValidacionResult vr,
                                    int longitudMax) {

        boolean validacion = true;
        if (campo != null && campo.length() > longitudMax) {

            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

            validacion = false;

        }
        return validacion;

    }

    private boolean validaEntero(String numLinea, String campo, String mensajeError, ValidacionResult vr) {

        boolean error = false;
        boolean tieneComas = false;

        if (campo == null) {
            return !error;
        }

        tieneComas = campo.indexOf(",") >= 0 ? true : false;

        if (tieneComas && !campo.matches("^[\\-]?[\\d]{1,3}(,\\d\\d\\d)+$")) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        } else if (!tieneComas && !campo.matches("^[\\-]?[\\d]+$")) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        }

        return !error;

    }

    private boolean validaPrecisionDecimal(String numLinea, String campo, String mensajeError, ValidacionResult vr,
                                           int enteros, int decimales) {

        boolean validacion = true;
        campo = campo == null ? "" : campo;

        int indexSeptarator = campo.indexOf('.');

        String parteEntera = indexSeptarator >= 0 ? campo.substring(0, indexSeptarator) : campo;
        String parteDecimal = indexSeptarator >= 0 ? campo.substring(indexSeptarator + 1) : "";

        int ctaEnteros = 0;
        int ctaComas = 0;
        for (int i = 0; i < parteEntera.length(); i++) {
            if (i == 0 && parteEntera.charAt(i) == '-') {
                continue;
            } else if (parteEntera.charAt(i) == ',') {
                ctaComas++;
            } else {
                ctaEnteros++;
            }
        }

        // Determinar numero de comas permitidas
        int comasPermitidas = ((enteros - 1) / 3);

        // Reajustar enteros si se excede el n�mero de comas permitidas
        ctaEnteros = ctaComas > comasPermitidas ? ctaEnteros + (ctaComas - comasPermitidas) : ctaEnteros;

        if (parteDecimal.length() > decimales) {
            //Error. El numero de decimales es mayor al especificado
            validacion = false;
        } else if (ctaEnteros > enteros) {
            //Error. El numero de enteros es mayor al permitido
            validacion = false;
        }

        if (!validacion) {

            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

            validacion = false;

        }
        return validacion;

    }

    private boolean validaPrecisionEntero(String numLinea, String campo, String mensajeError, ValidacionResult vr,
                                          int enteros) {

        boolean validacion = true;
        String parteEntera = campo == null ? "" : campo;

        int ctaEnteros = 0;
        int ctaComas = 0;
        for (int i = 0; i < parteEntera.length(); i++) {
            if (i == 0 && parteEntera.charAt(i) == '-') {
                continue;
            } else if (parteEntera.charAt(i) == ',') {
                ctaComas++;
            } else {
                ctaEnteros++;
            }
        }

        // Determinar numero de comas permitidas
        int comasPermitidas = ((enteros - 1) / 3);

        // Reajustar enteros si se excede el n�mero de comas permitidas
        ctaEnteros = ctaComas > comasPermitidas ? ctaEnteros + (ctaComas - comasPermitidas) : ctaEnteros;

        if (ctaEnteros > enteros) {
            //Error. El numero de enteros es mayor al permitido
            validacion = false;
        }

        if (!validacion) {

            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

            validacion = false;

        }

        return validacion;

    }

    private boolean validaFlotante(String numLinea, String campo, String mensajeError, ValidacionResult vr) {

        boolean error = false;
        boolean tieneComas = false;

        if (campo == null) {
            return !error;
        }

        tieneComas = campo.indexOf(",") >= 0 ? true : false;

        if (tieneComas && !campo.matches("^[\\-]?[\\d]{1,3}(,\\d\\d\\d)+[\\.]?$") &&
            !campo.matches("^[\\-]?[\\d]{1,3}(,\\d\\d\\d)+[\\.][\\d]+$")) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        } else if (!tieneComas && !campo.matches("^[\\-]?[\\d]+[\\.]?$") &&
                   !campo.matches("^[\\-]?[\\d]*[\\.][\\d]+$")) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        }

        return !error;

    }

    private boolean validaNoExistenciaEnListaBinaria(String numLinea, String campo, String mensajeError,
                                                     ValidacionResult vr, List listaClaves) {

        boolean validacion = true;

        if (listaClaves != null && campo != null && Collections.binarySearch(listaClaves, campo) >= 0) {

            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

            validacion = false;

        }

        return validacion;

    }

    /**
     *
     * Valida que la fecha proporcionada, cumpla con el formato DD/MM/AAAA.
     *
     * @param numLinea Numero de linea que le corresponde en el archivo que se esta procesando.
     * @param campo Valor a validar.
     * @param mensajeError Mensaje de error a utilizar en caso de error.
     * @param vr Objeto donde se guardar� el mensaje de error en la validaci�n.
     *
     * @return <tt>true</tt> si la validacion es exitosa y <tt>false</tt> en caso contrario.
     *
     */
    private boolean validaFecha(String numLinea, String campo, String mensajeError, ValidacionResult vr) {

        boolean esValido = true;

        if (campo != null && campo.length() > 10) {

            esValido = false;

        } else if (campo != null) {

            try {

                int dia = Integer.parseInt(campo.substring(0, 2));
                int mes = Integer.parseInt(campo.substring(3, 5));
                int anio = Integer.parseInt(campo.substring(6, 10));

                mes = mes - 1; // Requerido por GregorianCalendar

                Calendar fecha = new GregorianCalendar(anio, mes, dia);
                // Nota: Esta implementaci�n se podr�a mejorar con la utilizada en la
                // funci�n isdigit de valida.js
                int parsedDia = fecha.get(Calendar.DAY_OF_MONTH);
                int parsedMes = fecha.get(Calendar.MONTH);
                int parsedAnio = fecha.get(Calendar.YEAR);


                //System.out.println(year+" "+month+" "+day);
                if (dia != parsedDia || mes != parsedMes || anio != parsedAnio) {
                    esValido = false;
                }

            } catch (Exception e) {

                esValido = false;

            }

        }

        if (!esValido) {

            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(campo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        }

        return esValido;

    }


    private boolean validaFlotanteMenorOIgual(String numLinea, String valorCampo, BigDecimal campo01,
                                              BigDecimal campo02, String mensajeError, ValidacionResult vr) {

        boolean error = false;
        if (campo01 != null && campo02 != null && campo01.compareTo(campo02) > 0) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(valorCampo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        }
        return !error;

    }

    private boolean validaFlotantesIguales(String numLinea, String valorCampo, BigDecimal campo01, BigDecimal campo02,
                                           String mensajeError, ValidacionResult vr) {

        boolean error = false;
        if (campo01 != null && campo02 != null && campo01.compareTo(campo02) != 0) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(valorCampo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        }
        return !error;

    }

    /**
     * Si Capital Vencido � Intereses Vencidos son diferentes de cero,
     * se valida que D�as de Mora tenga valor diferente de cero;
     */
    private boolean validaObligatoriedadDiasMora(String numLinea, String valorCampo, BigDecimal diasMora,
                                                 BigDecimal capitalVencido, BigDecimal interesesVencidos,
                                                 String mensajeError, ValidacionResult vr) {

        boolean error = false;
        if (diasMora != null && capitalVencido != null && capitalVencido.compareTo(CERO) != 0 &&
            diasMora.compareTo(CERO) == 0) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(valorCampo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        } else if (diasMora != null && interesesVencidos != null && interesesVencidos.compareTo(CERO) != 0 &&
                   diasMora.compareTo(CERO) == 0) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(valorCampo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        }

        return !error;

    }

    private boolean validaTipoTasa(String numLinea, String valorCampo, String mensajeError, ValidacionResult vr) {

        boolean error = false;
        if (!"Fija".equalsIgnoreCase(valorCampo) && !"Variable".equalsIgnoreCase(valorCampo)) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeError);
            vr.appendErrores(" ");
            vr.appendErrores(valorCampo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        }

        return !error;

    }

    private boolean validaTasaBase(String numLinea, String valorCampo, String tipoTasa, String mensajeErrorTasaFija,
                                   String mensajeErrorTasaVariable, ValidacionResult vr) {

        boolean error = false;

        if (valorCampo == null) {
            return !error;
        }

        if ("Fija".equalsIgnoreCase(tipoTasa) && !validaFlotante(numLinea, valorCampo, mensajeErrorTasaFija, vr)) {

            error = true;

        } else if ("Variable".equalsIgnoreCase(tipoTasa) && !valorCampo.matches(".*[^0987654321\\.].*")) {

            error = true;
            vr.appendErrores("Error en la l�nea ");
            vr.appendErrores(numLinea);
            vr.appendErrores(": ");
            vr.appendErrores(mensajeErrorTasaVariable);
            vr.appendErrores(" ");
            vr.appendErrores(valorCampo);
            vr.appendErrores("\n");

            vr.incNumMensajesError();

        }

        return !error;

    }

    private BigDecimal parseBigDecimal(String s) {

        String n = null;

        n = s.replaceAll(",", "");

        return new BigDecimal(n);

    }

    public void validarCargaCarteraPrendaria(int numeroLinea, CarteraPrendariaValidator registro) {

        ValidacionResult retorno = registro.getValidacionResult();
        List numerosCreditoRepetidos = registro.getNumerosCreditoRepetidos();
        String numLinea = String.valueOf(numeroLinea + 1); // + 1 Para que concuerde con el excel.

        // II. REALIZAR VALIDACIONES POR CAMPO.
        boolean campoValido = false;
        String valorCampo = null;

        String tipoTasa = null;
        BigDecimal montoOtorgado = null;
        BigDecimal capitalVigente = null;
        BigDecimal capitalVencido = null;
        BigDecimal interesesVigentes = null;
        BigDecimal interesesVencidos = null;
        BigDecimal interesesMoratorios = null;
        BigDecimal saldoTotal = null;
        BigDecimal diasMora = null;

        // Num.       Nombre del Campo													Tipo							Longitud			  Obligatoriedad
        //................................................................................................................................

        //  1.        No. Consecutivo NAFIN                                           -                 -
        //            OBSERVACIONES: N�mero consecutivo que registre la garant�a otorgada a NAFIN (1,2,3,4...)
        /* Nota: No se valida nada con respecto a este campo */

        //  2.        N�mero de Cr�dito                                      Alfanum�rico               50            OBLIGATORIO
        //            OBSERVACIONES: N�mero de Cr�dito asignado por la Acreditada al cr�dito otorgado a su cliente/acreditado.
        campoValido = true;
        valorCampo = registro.getNumeroCredito().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "El N�mero de Cr�dito es obligatorio", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo,
                                "El N�mero de Cr�dito rebasa la longitud m�xima de 50 caracteres:", retorno, 50);
        if (campoValido)
            campoValido =
                validaNoExistenciaEnListaBinaria(numLinea, valorCampo, "El N�mero de Cr�dito est� repetido:", retorno,
                                                 numerosCreditoRepetidos);
        if (!campoValido)
            registro.setHayError(true);

        //  3.        Nombre Acreditado                                      Alfanum�rico              100            OBLIGATORIO
        //            OBSERVACIONES: Nombre del Acreditado/Cliente de la Acreditada.
        campoValido = true;
        valorCampo = registro.getNombreAcreditado().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "El Nombre del Acreditado es obligatorio", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo,
                                "El Nombre del Acreditado rebasa la longitud m�xima de 100 caracteres:", retorno, 100);
        if (!campoValido)
            registro.setHayError(true);

        //  4.        Tipo Operaci�n                                         Alfanum�rico               50             OBLIGATORIO
        //            OBSERVACIONES: Tipo de Operaci�n del Cr�dito (Cr�dito Simple, Arrendamiento Financiero, Arrendamiento Puro, Factoraje, etc.)
        campoValido = true;
        valorCampo = registro.getTipoOperacion().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "El Tipo de Operaci�n es obligatorio", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo,
                                "El Tipo de Operaci�n rebasa la longitud m�xima de 50 caracteres:", retorno, 50);
        if (!campoValido)
            registro.setHayError(true);

        //  5.        Fecha de Disposici�n de los recursos                   Fecha             			10            OBLIGATORIO
        //            OBSERVACIONES: Fecha de disposici�n de los recursos solicitados a NAFIN
        campoValido = true;
        valorCampo = registro.getFechaDisposicion().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "La Fecha de Disposici�n de los Recursos es obligatoria",
                                       retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo,
                                "La Fecha de Disposici�n de los Recursos rebasa la longitud m�xima de 10 caracteres:",
                                retorno, 10);
        if (campoValido)
            campoValido =
                validaFecha(numLinea, valorCampo,
                            "La Fecha de Disposici�n de los Recursos debe ser del formato DD/MM/AAAA:", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  6.        Tipo Tasa                                               Alfanum�rico              50            OBLIGATORIO
        //            OBSERVACIONES: Fija o Variable, de acuerdo con el tipo de tasa que la acreditada haya otorgado a su Cliente/Acreditado.
        campoValido = true;
        valorCampo = registro.getTipoTasa().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "El Tipo de Tasa es obligatorio", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo, "El Tipo de Tasa rebasa la longitud m�xima de 50 caracteres:",
                                retorno, 50);
        if (campoValido)
            campoValido =
                validaTipoTasa(numLinea, valorCampo, "El Tipo de Tasa solo debe ser Fija o Variable:", retorno);
        tipoTasa = campoValido ? valorCampo : null;
        if (!campoValido) {
            registro.setHayError(true);
        }

        //  7.        Tasa Base                                               Alfanum�rico              50            OBLIGATORIO
        //            OBSERVACIONES: En caso de que la Acreditada haya otorgado el Cr�dito con TASA FIJA a su cliente/acreditado, ser� el porcentaje correspondiente a �sta. En caso de que la Acreditada haya otorgado el Cr�dito con TASA VARIABLE a su cliente/acreditado, ser� la base correspondiente: TIIE, LIBOR, etc.
        campoValido = true;
        valorCampo = registro.getTasaBase().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "La Tasa Base es obligatoria", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo, "La Tasa Base rebasa la longitud m�xima de 50 caracteres:",
                                retorno, 50);
        if (campoValido)
            campoValido =
                validaTasaBase(numLinea, valorCampo, tipoTasa,
                               "El valor de la Tasa Base debe ser un porcentaje ya que se envi� Tipo Tasa Fija:",
                               "El valor de la Tasa Base debe tener un nombre de Tasa ya que se envi� Tipo Tasa Variable:",
                               retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  8.        Puntos Adicionales a la Tasa                            Num�rico                  4,2            OBLIGATORIO
        //            OBSERVACIONES: En caso de que la Acreditada haya otorgado el cr�dito con TASA VARIABLE a su cliente/acreditado, se registraran los puntos base adicionales la base indicada en el campo 7(Tasa Base). En caso de que se haya otorgado el cr�dito con TASA FIJA los puntos no aplicaran.
        campoValido = true;
        valorCampo = registro.getPuntosAdicionales().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Los Puntos Adicionales son obligatorios", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo,
                                       "Los Puntos Adicionales a la Tasa Base rebasan la longitud m�xima de 2 enteros 2 decimales:",
                                       retorno, 2, 2);
        if (campoValido)
            campoValido =
                validaFlotante(numLinea, valorCampo, "Los Puntos Adicionales a la Tasa Base debe ser num�ricos:",
                               retorno);
        /*
		Nota (16/12/2014 06:23:34 p.m.): A solicitud de Zara se suprime esta validaci�n:
		if(campoValido )  campoValido = validaPuntosAdicionales( 						numLinea, valorCampo, tipoTasa,
																																				 "Los Puntos Adicionales deben tener valor 0 ya que el Tipo de Tasa es Fija:",
																																			 	 "Los Puntos Adicionales deben tener valor diferente de 0 ya que el Tipo de Tasa es Variable:", retorno 			);
		*/

        if (!campoValido)
            registro.setHayError(true);

        //  9.        Monto Otorgado (pesos)                                  Num�rico                  19,5          OBLIGATORIO
        //            OBSERVACIONES: Monto del Cr�dito otorgado por la Acreditada a su cliente/acreditado
        campoValido = true;
        valorCampo = registro.getMontoOtorgado().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "El Monto Otorgado es obligatorio", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo,
                                       "El Monto Otorgado rebasa la longitud m�xima de 14 enteros 5 decimales:",
                                       retorno, 14, 5);
        if (campoValido)
            campoValido = validaFlotante(numLinea, valorCampo, "El Monto Otorgado debe ser num�rico:", retorno);
        montoOtorgado = campoValido ? parseBigDecimal(valorCampo) : null;
        if (!campoValido)
            registro.setHayError(true);

        //  10.       Recursos Solicitados a NAFIN                            Num�rico                  19,5          OBLIGATORIO
        //            OBSERVACIONES: Monto de los recursos solicitados a NAFIN
        campoValido = true;
        valorCampo = registro.getRecursosSolicitados().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Los Recursos Solicitados a NAFIN son obligatorios",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo,
                                       "Los Recursos Solicitados a NAFIN rebasan la longitud m�xima de 14 enteros 5 decimales:",
                                       retorno, 14, 5);
        if (campoValido)
            campoValido =
                validaFlotante(numLinea, valorCampo, "Los Recursos Solicitados a NAFIN deben ser num�ricos:", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  11.       Plazo Original (meses)                                  Num�rico                  3            OBLIGATORIO
        //            OBSERVACIONES: Meses que corresponden al plazo del cr�dito otorgado por la Acreditada a su cliente/acreditado
        campoValido = true;
        valorCampo = registro.getPlazoOriginal().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "El Plazo Original es obligatorio", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionEntero(numLinea, valorCampo, "El Plazo Original rebasa la longitud m�xima de 3 enteros:",
                                      retorno, 3);
        if (campoValido)
            campoValido = validaEntero(numLinea, valorCampo, "El Plazo Original debe ser un n�mero entero:", retorno);
        if (!campoValido)
            registro.setHayError(true);

        //  12.       Capital Vigente (pesos)                                 Num�rico                  19,5          OBLIGATORIO
        //            OBSERVACIONES: Monto del capital vigente del cr�dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr�dito registra menos de 90 d�as de atraso en el pago.
        campoValido = true;
        valorCampo = registro.getCapitalVigente().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "El Capital Vigente es obligatorio", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo,
                                       "El Capital Vigente rebasa la longitud m�xima de 14 enteros 5 decimales:",
                                       retorno, 14, 5);
        if (campoValido)
            campoValido = validaFlotante(numLinea, valorCampo, "El Capital Vigente debe ser num�rico:", retorno);
        capitalVigente = campoValido ? parseBigDecimal(valorCampo) : null;
        if (campoValido)
            campoValido =
                validaFlotanteMenorOIgual(numLinea, valorCampo, capitalVigente, montoOtorgado,
                                          "El Capital Vigente es mayor al monto otorgado:", retorno);
        if (!campoValido) {
            capitalVigente = null;
            registro.setHayError(true);
        }

        //  13.       Intereses Vigentes (pesos)                              Num�rico                  19,5            OBLIGATORIO
        //            OBSERVACIONES: Monto de los intereses vigentes del cr�dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr�dito registra menos de 90 d�as de atraso en el pago.
        campoValido = true;
        valorCampo = registro.getInteresesVigentes().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Los Intereses Vigentes son obligatorios", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo,
                                       "Los Intereses Vigentes rebasan la longitud m�xima de 14 enteros 5 decimales:",
                                       retorno, 14, 5);
        if (campoValido)
            campoValido = validaFlotante(numLinea, valorCampo, "Los Intereses Vigentes debe ser num�ricos:", retorno);
        interesesVigentes = campoValido ? parseBigDecimal(valorCampo) : null;
        if (!campoValido)
            registro.setHayError(true);

        //  14.       Capital Vencido (pesos)                                 Num�rico                  19,5            OBLIGATORIO
        //            OBSERVACIONES: Monto del capital vencido del cr�dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr�dito registra al menos, 90 d�as de atraso en el pago.
        campoValido = true;
        valorCampo = registro.getCapitalVencido().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "El Capital Vencido es obligatorio", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo,
                                       "El Capital Vencido rebasa la longitud m�xima de 14 enteros 5 decimales:",
                                       retorno, 14, 5);
        if (campoValido)
            campoValido = validaFlotante(numLinea, valorCampo, "El Capital Vencido debe ser num�ricos:", retorno);
        capitalVencido = campoValido ? parseBigDecimal(valorCampo) : null;
        if (campoValido && capitalVigente != null && capitalVencido != null && montoOtorgado != null) {
            campoValido =
                validaFlotanteMenorOIgual(numLinea, valorCampo, capitalVigente.add(capitalVencido), montoOtorgado,
                                          "La suma del Capital Vigente y el Capital Vencido es mayor al Monto Otorgado:",
                                          retorno);
        }
        if (!campoValido) {
            capitalVencido = null;
            registro.setHayError(true);
        }

        //  15.       Intereses Vencidos (pesos)                              Num�rico                  19,5            OBLIGATORIO
        //            OBSERVACIONES: Monto de los intereses vencidos del cr�dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr�dito registra al menos, 90 d�as de atraso en el pago.
        campoValido = true;
        valorCampo = registro.getInteresesVencidos().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Los Intereses Vencidos son obligatorios", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo,
                                       "Los Intereses Vencidos rebasan la longitud m�xima de 14 enteros 5 decimales:",
                                       retorno, 14, 5);
        if (campoValido)
            campoValido = validaFlotante(numLinea, valorCampo, "Los Intereses Vencidos debe ser num�ricos:", retorno);
        interesesVencidos = campoValido ? parseBigDecimal(valorCampo) : null;
        if (!campoValido)
            registro.setHayError(true);

        //  16.       Intereses Moratorios (pesos)                            Num�rico                  19,5            OBLIGATORIO
        //            OBSERVACIONES: Monto de los intereses moratorios del cr�dito otorgado por la Acreditada a su cliente/acreditado, cuando el cr�dito est� vencido (registra m�s de 90 d�as de atraso en el pago).
        campoValido = true;
        valorCampo = registro.getInteresesMoratorios().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Los Intereses Moratorios son obligatorios", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo,
                                       "Los Intereses Moratorios rebasan la longitud m�xima de 14 enteros 5 decimales:",
                                       retorno, 14, 5);
        if (campoValido)
            campoValido = validaFlotante(numLinea, valorCampo, "Los Intereses Moratorios debe ser num�ricos:", retorno);
        interesesMoratorios = campoValido ? parseBigDecimal(valorCampo) : null;
        if (!campoValido)
            registro.setHayError(true);

        //  17.       Saldo total (pesos)                                     Num�rico                  19,5             OBLIGATORIO
        //            OBSERVACIONES: Saldo del monto del cr�dito otorgado por la Acreditada  a su cliente/acreditado, al cierre del mes inmediato anterior a la fecha de disposici�n  de la Acreditada.
        campoValido = true;
        valorCampo = registro.getSaldoTotal().toString();
        if (campoValido)
            campoValido = validaCampoObligatorio(numLinea, valorCampo, "El Saldo Total es obligatorio", retorno);
        if (campoValido)
            campoValido =
                validaPrecisionDecimal(numLinea, valorCampo,
                                       "El Saldo Total rebasa la longitud m�xima de 14 enteros 5 decimales:", retorno,
                                       14, 5);
        if (campoValido)
            campoValido = validaFlotante(numLinea, valorCampo, "El Saldo Total debe ser num�rico:", retorno);
        saldoTotal = campoValido ? parseBigDecimal(valorCampo) : null;
        if (campoValido && capitalVigente != null && interesesVigentes != null && capitalVencido != null &&
            interesesVencidos != null && interesesMoratorios != null && saldoTotal != null) {
            campoValido =
                validaFlotantesIguales(numLinea, valorCampo,
                                       capitalVigente.add(interesesVigentes).add(capitalVencido).add(interesesVencidos).add(interesesMoratorios),
                                       saldoTotal, "El Saldo Total no concuerda:", retorno);
        }
        if (!campoValido) {
            saldoTotal = null;
            registro.setHayError(true);
        }

        //  18.       D�as de Retraso en el Pago                              Num�rico                  4            OBLIGATORIO
        //            OBSERVACIONES: N�mero de d�as de atraso en el pago que presente el cr�dito otorgado por la Acreditada a su cliente/acreditado.
        campoValido = true;
        valorCampo = registro.getDiasRetraso().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "Los D�as de Retraso en el Pago son obligatorios",
                                       retorno);
        if (campoValido)
            campoValido =
                validaPrecisionEntero(numLinea, valorCampo,
                                      "Los D�as de Retraso en el Pago rebasan la longitud m�xima de 4 caracteres:",
                                      retorno, 4);
        if (campoValido)
            campoValido =
                validaEntero(numLinea, valorCampo,
                             "Los D�as de Retraso en el Pago deben corresponder a un n�mero entero:", retorno);
        diasMora = campoValido ? parseBigDecimal(valorCampo) : null;
        if (campoValido) {
            campoValido =
                validaObligatoriedadDiasMora(numLinea, valorCampo, diasMora, capitalVencido, interesesVencidos,
                                             "Deben existir D�as de Mora en el pago:", retorno);
        }
        if (!campoValido) {
            diasMora = null;
            registro.setHayError(true);
        }

        //  19.       Descripci�n del Bien                                      Alfanum�rico              20        OBLIGATORIO
        //            OBSERVACIONES: Breve descripci�n del bien o del destino del cr�dito.
        campoValido = true;
        valorCampo = registro.getDescripcion().toString();
        if (campoValido)
            campoValido =
                validaCampoObligatorio(numLinea, valorCampo, "La Descripci�n del Bien es obligatoria", retorno);
        if (campoValido)
            campoValido =
                validarLongitud(numLinea, valorCampo,
                                "La Descripci�n del Bien rebasa la longitud m�xima de 20 caracteres:", retorno, 20);
        if (!campoValido)
            registro.setHayError(true);

        // VALIDAR QUE NO SE HAYAN ENVIADO M�S DEL N�MERO DE CAMPOS REQUERIDOS
        /*
		// Nota (19/12/2014 02:16:33 p.m.):
		// Zara, @dy: Se va limitar a enviar solo el contenido de las celdas. No se consideran casos erroneos en descripcion del bi�n.
		if( registro.getExtraChar() != -1 ){

			retorno.appendErrores("Error en la l�nea ");
			retorno.appendErrores(numLinea);
			retorno.appendErrores(": ");
			retorno.appendErrores("Se enviaron m�s campos que los solicitados.\n");
			retorno.incNumMensajesError();

			registro.setHayError(true);

		}
		*/
        // ACTUALIZAR CONTEO/DETALLE DE REGISTROS CON ERROR
        if (registro.getHayError()) {
            retorno.incNumRegErr();
        } else {
            retorno.incNumRegOk();
        }

    }

    // validarCifrasControl
    public void validarCifrasControlCarteraPrendaria(CarteraPrendariaValidator fileValidator) {
    }

    /**
     * Devuelve la clave de financiera para el Intermediario Financiero No Bancario.
     * @throws AppException
     *
     * @param claveIf String con la clave if (COMCAT_IF.IC_IF) con el que fue registrado en NAFIN.
     *
     * @return String con la clave de financiera � <tt>null</tt> en caso de que esta no exista.
     * @author Salim Hernandez
     * @date   01/12/2014 03:45:30 p.m.
     * @fodea  F035 - 2014 -- Segunda Etapa Operacion Electronica
     */
    public String getClaveFinanciera(String claveIf) throws AppException {

        log.info("getClaveFinanciera(E)");

        AccesoDB con = new AccesoDB();
        String query = "";
        ResultSet rs = null;
        PreparedStatement ps = null;

        String claveFinanciera = null;

        try {

            con.conexionDB();

            query = "SELECT IC_FINANCIERA FROM COMCAT_IF WHERE IC_IF = ?";

            ps = con.queryPrecompilado(query);
            ps.setInt(1, Integer.parseInt(claveIf));
            rs = ps.executeQuery();

            if (rs.next()) {
                claveFinanciera = rs.getString("IC_FINANCIERA");
            }


        } catch (Exception e) {

            log.error("getClaveFinanciera(Exception)");
            log.error("getClaveFinanciera.claveIf = <" + claveIf + ">");
            e.printStackTrace();

            throw new AppException("No se pudo obtener la clave de financiera", e);

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("getClaveFinanciera(S)");
        }

        return claveFinanciera;

    }

    /**
     * Verifica que el nombre del archivo correspondiente Anexo 5 � Actualizaci�n de la Cartera Prendaria.
     * cumpla con el siguiente formato:<br>
     * <ul>
     *	   <li>IFNB: es la nomenclatura de 6 d�gitos correspondiente a la clave del Intermediario Financiero No Bancario (ic_financiera), por ejemplo si la clave del IF es 3 se deber�n anteponer 5 ceros (0) al 3, quedando 000003.</li>
     *    <li>A5: corresponde al n�mero del anexo, en este caso al Anexo 5 (Actualizaci�n de la Cartera Prendaria).</li>
     *    <li>AAAAMM: es el a�o y mes actual en que se carga la cartera, por ejemplo 201410 (a�o 2014 mes 10 (Octubre)).</li>
     *    <li>csv/pdf: extensi�n del tipo de archivo a cargar.</li>
     * </ul>
     * La clave del IFNB se valida contra el campo ic_financiera, parametrizado en comcat_if.
     * @throws AppException
     *
     * @param nombreArchivo String con el nombre del archivo.
     * @param claveIf String con la clave del if asociado al intermediario bancario, la cual se usara para obtener la clave de ic_financiera.
     *
     * @return <tt>true</tt> si la validaci�n es exitosa � <tt>false</tt> en caso contrario.
     *
     * @author Salim Hernandez
     * @date   01/12/2014 06:18:33 p.m.
     * @fodea  F035 - 2014 -- Segunda Etapa Operacion Electronica
     */
    public boolean validaNombreArchivoAnexo5(String nombreArchivo, String claveIf, String[] hint) throws AppException {

        log.info("validaNombreArchivoAnexo5(E)");

        boolean nombreValido = true;

        try {

            Pattern pattern =
                Pattern.compile("^([\\d]{6})_[aA]5_([\\d]{4})([01])([\\d])\\.([pP][dD][fF]|[cC][sS][vV])$");
            Matcher matcher = pattern.matcher(nombreArchivo);
            if (matcher.matches()) {

                String claveFinancieraArchivo = matcher.group(1);
                String claveMesDecena = matcher.group(3);
                String claveMesUnidad = matcher.group(4);

                String claveFinanciera = getClaveFinanciera(claveIf);

                claveFinancieraArchivo = claveFinancieraArchivo.replaceFirst("^[0]+", "");

                if (claveMesDecena.equals("0") && claveMesUnidad.equals("0")) {
                    nombreValido = false;
                    hint[0] = ""; // " La clave AAAAMM no es valida.";
                } else if (claveMesDecena.equals("1") && claveMesUnidad.matches("[3-9]")) {
                    nombreValido = false;
                    hint[0] = ""; // " La clave AAAAMM no es valida.";
                } else if (!claveFinancieraArchivo.equals(claveFinanciera)) {
                    nombreValido = false;
                    hint[0] = " El c�digo IFNB no coincide.";
                }

            } else {

                nombreValido = false;
                hint[0] = "";

            }

        } catch (Exception e) {

            log.error("validaNombreArchivoAnexo5(Exception)");
            log.error("validaNombreArchivoAnexo5.nombreArchivo = <" + nombreArchivo + ">");
            log.error("validaNombreArchivoAnexo5.claveIf       = <" + claveIf + ">");
            e.printStackTrace();

            throw new AppException("Ocurri� un error al validar el nombre del archivo: ", e);

        } finally {

            log.info("validaNombreArchivoAnexo5(S)");

        }

        return nombreValido;

    }

    /**
     * Verifica si el archivo (csv � pdf) correspondiente Anexo 5 � Actualizaci' || chr(243) || 'n de la Cartera Prendaria ha sido
     * cargados. Valida que el contenido del CSV se haya enviado correctamente al CCBE.
     * Se asume que el nombre del archivo ( ##IFNB_A5_AAAAMM.csv � ##IFNB_A5_AAAAMM.pdf ) cumple con el siguiente formato:
     * <ul>
     *	   <li>IFNB: es la nomenclatura de 6 d�gitos correspondiente a la clave del Intermediario Financiero No Bancario (ic_financiera), por ejemplo si la clave del IF es 3 se deber�n anteponer 5 ceros (0) al 3, quedando 000003.</li>
     *    <li>A5: corresponde al n�mero del anexo, en este caso al Anexo 5 (Actualizaci�n de la Cartera Prendaria).</li>
     *    <li>AAAAMM: es el a�o y mes actual en que se carga la cartera, por ejemplo 201410 (a�o 2014 mes 10 (Octubre)).</li>
     *    <li>csv/pdf: extensi�n del tipo de archivo a cargar.</li>
     * </ul>
     * La clave del IFNB se valida contra el campo ic_financiera, parametrizado en comcat_if.
     * @throws AppException
     *
     * @param nombreArchivo String con el nombre del archivo.
     *
     * @return <tt>true</tt> si ya se relaiz� carga previa del archivo � <tt>false</tt> en caso contrario.
     *
     * @author Salim Hernandez
     * @date   01/12/2014 06:33:13 p.m.
     * @fodea  F035 - 2014 -- Segunda Etapa Operacion Electronica
     *
     */
    public boolean existeCargaPreviaCarteraPrendaria(String nombreArchivo) throws AppException {

        log.info("existeCargaPreviaCarteraPrendaria(E)");

        AccesoDB con = new AccesoDB();
        String query = "";
        ResultSet rs = null;
        PreparedStatement ps = null;

        boolean existeCarga = false;

        String claveFinanciera = null;
        String claveMesCarga = null;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Parsear campos del nombre del archivo

            claveFinanciera = nombreArchivo.substring(0, 6).replaceFirst("^[0]+", "");
            claveMesCarga = nombreArchivo.substring(10, 16);

            // Debido a que la validaci�n importante es la que se realiza en el CCBE, se omite la validaci�n en
            // base de datos local.
            /*
				Se adapt� query del m�todo: isDateProcessed de la clase troya.ejbs.ifnbfilesanexo4.IfnbFilesAnexo4Bean del sistema CCBE,
				con el proposito de hacer la consulta m�s eficiente:

					String sqlsentence = " SELECT COUNT(*) " +
                             " FROM CCBE_ANEXO_CTRO " +
                             " WHERE to_char(ANX_PERIOD,'mm/yyyy') = '"+ periodDate.getFormated("MM/yyyy") + "' " +
                             " AND   ANX_IFNB_CODE = " + ifnbCode;

			*/
            query =
                "SELECT                                                     " +
                "  DECODE(                                                  " +
                "    COUNT(1),                                              " +
                "       0, 'false',                                         " +
                "       'true'                                              " +
                "  ) AS EXISTE_CARGA                                        " +
                "FROM                                                       " +
                "   R_CCBE_ANEXO_CTRO                                       " +
                "WHERE                                                      " +
                "   ANX_PERIOD    >= TO_DATE(?,'YYYYMMDD')             AND  " +
                "   ANX_PERIOD    <  LAST_DAY(TO_DATE(?,'YYYYMMDD'))+1 AND  " +
                "   ANX_IFNB_CODE =  ?                                      ";

            ps = con.queryPrecompilado(query);
            ps.setString(1, claveMesCarga + "01");
            ps.setString(2, claveMesCarga + "01");
            ps.setInt(3, Integer.parseInt(claveFinanciera));
            rs = ps.executeQuery();

            if (rs.next()) {
                existeCarga = "true".equals(rs.getString("EXISTE_CARGA")) ? true : false;
            }

        } catch (Exception e) {

            log.error("existeCargaPreviaCarteraPrendaria(Exception)");
            log.error("existeCargaPreviaCarteraPrendaria.nombreArchivo = <" + nombreArchivo + ">");
            e.printStackTrace();

            throw new AppException("No se pudo validar la existencia de una carga previa", e);

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(true); // Debido a que se trata de una tabla remota
                con.cierraConexionDB();
            }

            log.info("existeCargaPreviaCarteraPrendaria(S)");

        }

        return existeCarga;

    }

    /**
     * Verifica que uno o ambos de los archivos csv y pdf correspondiente Anexo 5 � Actualizaci�n de la Cartera Prendaria ha sido
     * cargados en NAFINET y que no exista referencia de la carga en el Sistema CCBE.
     * Se asume que el nombre del archivo ( ##IFNB_A5_AAAAMM.csv � ##IFNB_A5_AAAAMM.pdf ) cumple con el siguiente formato:
     * <ul>
     *	   <li>IFNB: es la nomenclatura de 6 d�gitos correspondiente a la clave del Intermediario Financiero No Bancario (ic_financiera), por ejemplo si la clave del IF es 3 se deber�n anteponer 5 ceros (0) al 3, quedando 000003.</li>
     *    <li>A5: corresponde al n�mero del anexo, en este caso al Anexo 5 (Actualizaci�n de la Cartera Prendaria).</li>
     *    <li>AAAAMM: es el a�o y mes actual en que se carga la cartera, por ejemplo 201410 (a�o 2014 mes 10 (Octubre)).</li>
     *    <li>csv/pdf: extensi�n del tipo de archivo a cargar.</li>
     * </ul>
     * La clave del IFNB se valida contra el campo ic_financiera, parametrizado en comcat_if.
     * @throws AppException
     *
     * @param nombreArchivo String con el nombre del archivo.
     *
     * @return <tt>true</tt> si la carga s�lo existe en NAFINET � <tt>false</tt> en caso contrario.
     *
     * @author Salim Hernandez
     * @date   17/12/2014 06:06:23 p.m.
     * @fodea  F035 - 2014 -- Segunda Etapa Operacion Electronica
     *
     */
    public boolean existeCargaPreviaCarteraPrendariaSoloNafinet(String nombreArchivo) throws AppException {

        log.info("existeCargaPreviaCarteraPrendariaSoloNafinet(E)");

        AccesoDB con = new AccesoDB();
        String query = "";
        ResultSet rs = null;
        PreparedStatement ps = null;

        boolean soloNafinet = false;

        String claveFinanciera = null;
        String claveMesCarga = null;

        try {

            if (existeCargaPreviaCarteraPrendaria(nombreArchivo)) {

                soloNafinet = false;
                return soloNafinet;

            }

            // Conectarse a la Base de Datos
            con.conexionDB();

            // Parsear campos del nombre del archivo

            claveFinanciera = nombreArchivo.substring(0, 6).replaceFirst("^[0]+", "");
            claveMesCarga = nombreArchivo.substring(10, 16);

            query =
                "SELECT                                                        " +
                "  DECODE(COUNT(1),1,'true',2,'true','false') AS EXISTE_CARGA  " +
                "FROM                                                          " +
                "  COM_CART_PRENDARIA_A5                                       " +
                "WHERE                                                         " +
                "  IC_FINANCIERA  = ? AND                                      " +
                "  IC_MES_CARTERA = ? AND                                      " +
                "  CS_TIPO_ARCH IN  ('CSV','PDF')                              ";

            ps = con.queryPrecompilado(query);
            ps.setInt(1, Integer.parseInt(claveFinanciera));
            ps.setInt(2, Integer.parseInt(claveMesCarga));
            rs = ps.executeQuery();

            rs.next();
            soloNafinet = "true".equals(rs.getString("EXISTE_CARGA")) ? true : false;


        } catch (Exception e) {

            soloNafinet = false;

            log.error("existeCargaPreviaCarteraPrendariaSoloNafinet(Exception)");
            log.error("existeCargaPreviaCarteraPrendariaSoloNafinet.nombreArchivo = <" + nombreArchivo + ">");
            log.error("existeCargaPreviaCarteraPrendariaSoloNafinet.query         = <" + query + ">");
            e.printStackTrace();

            throw new AppException("No se pudo validar la existencia de una carga previa s�lo en NAFINET", e);

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("existeCargaPreviaCarteraPrendariaSoloNafinet(S)");

        }

        return soloNafinet;

    }


    /*
     Nota (18/12/2014 01:31:21 p.m.): Debido a un commit dentro del procedimiento remoto:
	  Se obten�a una excepci�n del tipo:
	  ORA-02064: distributed operation not supported
	  Por lo que se decidi� implementar en java, lo que se le proporcionaba al procedimiento
	  almacenado.
	*/
    /**
    * Versi�n original del m�todo para insertar un registro en la bit�cora del Sistema CCBE
    */
    /*
   public BigDecimal registraEnBitacoraCCBE(
   	int 		eventType,
   	int 		msgCode,
   	int 		ifnbCode,
   	String 	date,  // No se usa, se deja por compatibilidad
   	String 	period,
   	String 	user,
   	String 	coment
   ) throws AppException {

		log.info("registraEnBitacoraCCBE(E)");
		log.debug("registraEnBitacoraCCBE: Almacenando en Bit�cora del Sistem CCBE");

		AccesoDB   			con 			= new AccesoDB();
		CallableStatement	cs 			= null;
		BigDecimal 			resultado 	= null;
		boolean				exito			= true;

		try {

			resultado 	= new BigDecimal("0");

			// Conectarse a la Base de Datos
			con.conexionDB();

			cs = con.ejecutaSP("R_CCBE_SP_INSERT_REGISTRY(?,?,?,?,?,?,?)");
			cs.setInt(1, eventType);
			cs.setInt(2, msgCode);
			cs.setInt(3, ifnbCode);
			if ( period == null ){
				cs.setString(4,"null");
			} else {
				cs.setString(4, period); // MM-yyyy
			}
			cs.setString(5, user.toUpperCase());
			//cs.setString(5, "");
			if (coment == null) {
				cs.setString(6,"null");
			} else {
				if(coment.length()>299){
					cs.setString(6,coment.substring(0, 299));
				} else {
					cs.setString(6,coment);
				}
			}
			cs.registerOutParameter(7, Types.NUMERIC);
			cs.executeQuery();

			resultado = cs.getBigDecimal(7);

		} catch (Exception e) {

			exito = false;

			log.error("registraEnBitacoraCCBE(Exception)");
			log.error("registraEnBitacoraCCBE.eventType = <" + eventType + ">");
			log.error("registraEnBitacoraCCBE.msgCode   = <" + msgCode   + ">");
			log.error("registraEnBitacoraCCBE.ifnbCode  = <" + ifnbCode  + ">");
			log.error("registraEnBitacoraCCBE.date      = <" + date      + ">");
			log.error("registraEnBitacoraCCBE.period    = <" + period    + ">");
			log.error("registraEnBitacoraCCBE.user      = <" + user      + ">");
			log.error("registraEnBitacoraCCBE.coment    = <" + coment    + ">");
			e.printStackTrace();

			throw new AppException("No se pudo insertar registro de la operaci�n en la bit�cora del Sistema CCBE",e);

		} finally {

			if( cs != null ){ try { cs.close(); } catch(Exception e){} }

			if( con.hayConexionAbierta() ){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

			log.info("registraEnBitacoraCCBE(S)");

		}

		return resultado;

	}
   */

    /**
     * M�todo para insertar un registro en la bit�cora del Sistema CCBE
     */
    public void registraEnBitacoraCCBE(int eventType, int msgCode, int ifnbCode, String date, // No se usa, se deja por compatibilidad
                                       String period, String user, String coment) throws AppException {

        log.info("registraEnBitacoraCCBE(E)");
        log.debug("registraEnBitacoraCCBE: Almacenando en Bit�cora del Sistem CCBE");

        /*
			Versi�n del prodecimiento que se us� para crear este m�todo:

			CREATE OR REPLACE
			PROCEDURE CCBE_SP_INSERT_REGISTRY (
				p_event        IN    NUMBER,
				p_message      IN    NUMBER,
				p_ifnb         IN    NUMBER,
				p_period       IN    VARCHAR2,
				p_user         IN    VARCHAR2,
				p_coment       IN    VARCHAR2,
				p_new_id       OUT   NUMBER)
			IS
			BEGIN
				SELECT ccbe_seq_registry.nextval
				INTO   p_new_id
				FROM   DUAL;
				INSERT INTO CCBE_REGISTRY (REG_ID,ETP_CODE,MSG_CODE,REG_IFNB_CODE,
													REG_DATE_TIME,REG_PERIOD,REG_USER,REG_COMENT)
										 VALUES (p_new_id,p_event,p_message,p_ifnb,
													SYSDATE,DECODE(p_period,'null',null,LAST_DAY(TO_DATE(p_period,'MM-YYYY'))),
													P_USER, DECODE(P_COMENT,'null',NULL,P_COMENT));
				COMMIT;
				EXCEPTION
				WHEN OTHERS THEN
					INSERT INTO CCBE_REGISTRY(REG_ID,ETP_CODE,MSG_CODE,REG_IFNB_CODE,REG_DATE_TIME,REG_USER,REG_COMENT)
					VALUES (ccbe_seq_registry.nextval,99,99,0,sysdate,'BD','Error al insertar en la bitacora');
			END;

		*/

        AccesoDB con = new AccesoDB();
        String sentence = null;
        PreparedStatement ps = null;

        boolean exito = true;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            sentence =
                "INSERT INTO                              " + "   R_CCBE_REGISTRY                       " +
                "      (                                  " + "         REG_ID,                         " +
                "         ETP_CODE,                       " + "         MSG_CODE,                       " +
                "         REG_IFNB_CODE,                  " + "         REG_DATE_TIME,                  " +
                "         REG_PERIOD,                     " + "         REG_USER,                       " +
                "         REG_COMENT                      " + "      )                                  " +
                "      VALUES (                           " + "         R_CCBE_SEQ_REGISTRY.NEXTVAL,    " +
                "         ?,                              " + "         ?,                              " +
                "         ?,                              " + "         SYSDATE,                        " +
                "         LAST_DAY(TO_DATE(?,'MM-YYYY')), " + "         ?,                              " +
                "         ?                               " + "      )                                  ";

            ps = con.queryPrecompilado(sentence);
            ps.setInt(1, eventType);
            ps.setInt(2, msgCode);
            ps.setInt(3, ifnbCode);
            if (period == null || period.equals("null")) {
                ps.setNull(4, Types.VARCHAR);
            } else {
                ps.setString(4, period); // MM-yyyy
            }
            ps.setString(5, user.toUpperCase());
            if (coment == null || coment.equals("null")) {
                ps.setNull(6, Types.VARCHAR);
            } else {
                if (coment.length() > 299) {
                    ps.setString(6, coment.substring(0, 299));
                } else {
                    ps.setString(6, coment);
                }
            }
            ps.executeUpdate();

        } catch (Exception e) {

            exito = false;

            log.error("registraEnBitacoraCCBE(Exception)");
            log.error("registraEnBitacoraCCBE.eventType = <" + eventType + ">");
            log.error("registraEnBitacoraCCBE.msgCode   = <" + msgCode + ">");
            log.error("registraEnBitacoraCCBE.ifnbCode  = <" + ifnbCode + ">");
            log.error("registraEnBitacoraCCBE.date      = <" + date + ">");
            log.error("registraEnBitacoraCCBE.period    = <" + period + ">");
            log.error("registraEnBitacoraCCBE.user      = <" + user + ">");
            log.error("registraEnBitacoraCCBE.coment    = <" + coment + ">");
            log.error("registraEnBitacoraCCBE.sentence  = <" + sentence + ">");
            e.printStackTrace();

            //throw new AppException("No se pudo insertar registro de la operaci�n en la bit�cora del Sistema CCBE",e);

        } finally {

            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

            if (exito) {
                log.info("registraEnBitacoraCCBE(S)");
            }

        }

        if (!exito) {

            log.debug("registraEnBitacoraCCBE: Regristrar en bit�cora, fallo de inserci�n de registro en bit�cora.");

            con = new AccesoDB();
            sentence = null;
            ps = null;

            exito = true;

            try {

                // Conectarse a la Base de Datos
                con.conexionDB();

                sentence =
                    "INSERT INTO                              " + "   R_CCBE_REGISTRY(                      " +
                    "      REG_ID,                            " + "      ETP_CODE,                          " +
                    "      MSG_CODE,                          " + "      REG_IFNB_CODE,                     " +
                    "      REG_DATE_TIME,                     " + "      REG_USER,                          " +
                    "      REG_COMENT                         " + "   ) VALUES (                            " +
                    "      R_CCBE_SEQ_REGISTRY.NEXTVAL,       " + "      99,                                " +
                    "      99,                                " + "      0,                                 " +
                    "      SYSDATE,                           " + "      'BD',                              " +
                    "      'Error al insertar en la bitacora' " + "   )                                     ";

                ps = con.queryPrecompilado(sentence);
                ps.executeUpdate();

            } catch (Exception e) {

                exito = false;

                log.error("registraEnBitacoraCCBE(Exception)");
                log.error("registraEnBitacoraCCBE.eventType = <" + eventType + ">");
                log.error("registraEnBitacoraCCBE.msgCode   = <" + msgCode + ">");
                log.error("registraEnBitacoraCCBE.ifnbCode  = <" + ifnbCode + ">");
                log.error("registraEnBitacoraCCBE.date      = <" + date + ">");
                log.error("registraEnBitacoraCCBE.period    = <" + period + ">");
                log.error("registraEnBitacoraCCBE.user      = <" + user + ">");
                log.error("registraEnBitacoraCCBE.coment    = <" + coment + ">");
                log.error("registraEnBitacoraCCBE.sentence  = <" + sentence + ">");
                e.printStackTrace();

                throw new AppException("No se pudo insertar registro de la operaci�n en la bit�cora del Sistema CCBE",
                                       e);

            } finally {

                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception e) {
                    }
                }

                if (con.hayConexionAbierta()) {
                    con.terminaTransaccion(exito);
                    con.cierraConexionDB();
                }

                log.info("registraEnBitacoraCCBE(S)");

            }

        }

    }

    /**
     * Verifica si el usuario firmado con clave de IF claveIF tiene clave de financiera configurada COMCAT_IF.IC_FINANCIERA.
     * @throws AppException
     *
     * @param claveIf String con la clave del IF
     *
     * @return <tt>true</tt> si la clave existe � <tt>false</tt> en caso contrario.
     *
     * @author Salim Hernandez
     * @date   18/12/2014 07:58:15 p.m.
     * @fodea  F035 - 2014 -- Segunda Etapa Operacion Electronica
     *
     */
    public boolean existeClaveFinanciera(String claveIf) throws AppException {

        log.info("existeClaveFinanciera(E)");

        AccesoDB con = new AccesoDB();
        String query = "";
        ResultSet rs = null;
        PreparedStatement ps = null;

        boolean existeClave = false;

        try {

            // Conectarse a la Base de Datos
            con.conexionDB();

            query =
                "SELECT                                               " +
                "  DECODE(COUNT(1),0,'false','true') AS EXISTE_CLAVE  " +
                "FROM                                                 " +
                "  COMCAT_IF                                          " +
                "WHERE                                                " +
                "  IC_FINANCIERA IS NOT NULL AND                      " + "  IC_IF          = ?     ";

            ps = con.queryPrecompilado(query);
            ps.setInt(1, Integer.parseInt(claveIf));
            rs = ps.executeQuery();

            rs.next();
            existeClave = "true".equals(rs.getString("EXISTE_CLAVE")) ? true : false;

        } catch (Exception e) {

            existeClave = false;

            log.error("existeClaveFinanciera(Exception)");
            log.error("existeClaveFinanciera.claveIf = <" + claveIf + ">");
            log.error("existeClaveFinanciera.query   = <" + query + ">");
            e.printStackTrace();

            throw new AppException("No se pudo validar la existencia de la Clave de Financiera", e);

        } finally {

            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }

            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }

            log.info("existeClaveFinanciera(S)");

        }

        return existeClave;

    }

    //***************** FUNCIONES PARA FODEA-035-2014*********

    /**
     * Funcion que me indica si existen 1 o 2 Autorizadores de la pantalla Registro datos IF�s y datos del Operador correspondiente
     * @throws netropology.utilerias.AppException
     * @return
     * @param IF
     */
    public HashMap obtieneInfoDeFirmaMancom(String IF, String Usuario) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String strSQL = "";
        UtilUsr utilUsr = new UtilUsr();
        String nombreOpe = "", mailOpe = "", telOpe = "", cveAfOpe = "", nombreAut1 = "", mailAut1 = "", telAut1 =
            "", cveAfAut1 = "";
        log.info("obtieneInfoDeFirmaMancom (E) ");
        try {
            con.conexionDB();
            strSQL =
                "select CG_OPERADOR , CG_AUTORIZADOR_UNO , CG_AUTORIZADOR_DOS" + "   from OPE_DATOS_IF " +
                "  where IC_IF =" + IF;
            log.debug("strSQL :::::  " + strSQL);
            ps = con.queryPrecompilado(strSQL);
            rs = ps.executeQuery();
            HashMap hmData = new HashMap();
            if (rs.next()) {
                String usuarioOpe = rs.getString("CG_OPERADOR") == null ? "" : rs.getString("CG_OPERADOR");
                String EXISAUT1 = rs.getString("CG_AUTORIZADOR_UNO") == null ? "" : rs.getString("CG_AUTORIZADOR_UNO");
                String EXISAUT2 = rs.getString("CG_AUTORIZADOR_DOS") == null ? "" : rs.getString("CG_AUTORIZADOR_DOS");
                System.out.println("usuarioConc  :::::::::::::::::::::" + usuarioOpe.replaceAll("\\s", "") + ":::");
                if (!"".equals(usuarioOpe.replaceAll("\\s", ""))) {
                    Usuario userOpe = utilUsr.getUsuario(usuarioOpe);
                    nombreOpe =
                        userOpe.getNombre() + " " + userOpe.getApellidoPaterno() + " " + userOpe.getApellidoMaterno();
                    mailOpe = userOpe.getEmail();
                    cveAfOpe = userOpe.getClaveAfiliado();
                    telOpe = this.getTelefonoIfOperador(cveAfOpe);
                }
                hmData.put("OPERADOR_USUARIO", usuarioOpe);
                hmData.put("OPERADOR_NOMBRE", nombreOpe);
                hmData.put("OPERADOR_MAIL", mailOpe);
                hmData.put("OPERADOR_TELEFONO", telOpe);
                if (!EXISAUT1.equals("") && !EXISAUT2.equals("")) {
                    hmData.put("NUM_AUTORIZADOR", "2");
                } else if (EXISAUT1.equals("") && EXISAUT2.equals("")) {
                    hmData.put("NUM_AUTORIZADOR", "0");
                } else {
                    hmData.put("NUM_AUTORIZADOR", "1");

                }
                if (EXISAUT1.equals(Usuario)) {
                    UtilUsr utilUsr1 = new UtilUsr();
                    System.out.println("usuarioConc  :::::::::::::::::::::" + EXISAUT1.replaceAll("\\s", "") + ":::");
                    if (!"".equals(EXISAUT1.replaceAll("\\s", ""))) {
                        Usuario userAut = utilUsr1.getUsuario(EXISAUT1);
                        nombreAut1 =
                            userAut.getNombre() + " " + userAut.getApellidoPaterno() + " " +
                            userAut.getApellidoMaterno();
                        mailAut1 = userAut.getEmail();
                        cveAfAut1 = userAut.getClaveAfiliado();
                        telAut1 = this.getTelefonoIfOperador(cveAfAut1);
                    }
                    hmData.put("AUTORIZADOR", "1");
                    hmData.put("AUTORIZADOR_USUARIO", EXISAUT1);
                    hmData.put("AUTORIZADOR_NOMBRE", nombreAut1);
                    hmData.put("AUTORIZADOR_MAIL", mailAut1);
                    hmData.put("AUTORIZADOR_TELEFONO", telAut1);
                } else if (EXISAUT2.equals(Usuario)) {
                    UtilUsr utilUsr1 = new UtilUsr();
                    System.out.println("usuarioConc  :::::::::::::::::::::" + EXISAUT2.replaceAll("\\s", "") + ":::");
                    if (!"".equals(EXISAUT2.replaceAll("\\s", ""))) {
                        Usuario userAut = utilUsr1.getUsuario(EXISAUT2);
                        nombreAut1 =
                            userAut.getNombre() + " " + userAut.getApellidoPaterno() + " " +
                            userAut.getApellidoMaterno();
                        mailAut1 = userAut.getEmail();
                        cveAfAut1 = userAut.getClaveAfiliado();
                        telAut1 = this.getTelefonoIfOperador(cveAfAut1);
                    }
                    hmData.put("AUTORIZADOR", "2");
                    hmData.put("AUTORIZADOR_USUARIO", EXISAUT2);
                    hmData.put("AUTORIZADOR_NOMBRE", nombreAut1);
                    hmData.put("AUTORIZADOR_MAIL", mailAut1);
                    hmData.put("AUTORIZADOR_TELEFONO", telAut1);
                } else {
                    hmData.put("AUTORIZADOR", "0");
                }


            }

            rs.close();
            ps.close();
            return hmData;
        } catch (Throwable t) {
            throw new AppException("Error al obetener datos de gerente y subdirector paramaterizados", t);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("obtieneInfoDeFirmaMancom (S) ");
        }

    }

    /**
     * Funci�n que obtiene el num�ro telefonico de 'Nombre del usuario IF Operador'
     *
     */
    public String getTelefonoIfOperador(String IC_IF) {
        log.info("getTelefonoIfOperador (E)");
        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        String telefono = "";
        List lVarBind = null;
        Registros registros;
        try {
            con.conexionDB();
            SQL.append(" SELECT cd.CG_TELEFONO1  FROM com_domicilio cd , comcat_if ci where cd.ic_if = ci.ic_if " +
                       "	and ci.ic_if = ? ");
            lVarBind = new ArrayList();
            lVarBind.add(IC_IF);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("ic_if ::  " + SQL.toString());
            registros = con.consultarDB(SQL.toString(), lVarBind);
            if (registros.next()) {
                telefono = registros.getString("CG_TELEFONO1") == null ? "" : registros.getString("CG_TELEFONO1");
            }

        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getTelefonoIfOperador " + e);
            throw new AppException("Error al getTelefonoIfOperador", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getTelefonoIfOperador (S)");
        }
        return telefono;
    }

    /**
     * Funcion para Obtener algun dato de la tabla OPE_PARAMGENERALES
     * @throws java.lang.Exception
     * @return
     * @param parametros
     */

    public String getCampoOpeParamGenerales(String Campo) {
        log.info("getConcentrador2 (E)");
        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        String campo = "";
        try {
            con.conexionDB();
            SQL.append(" SELECT " + Campo + "  as USUARIO FROM OPE_PARAMGENERALES WHERE  ic_email = 1");
            log.debug("SQL.toString() " + SQL.toString());
            PreparedStatement ps = con.queryPrecompilado(SQL.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                campo = rs.getString(1) == null ? "" : rs.getString(1);
            }
            rs.close();
            ps.close();
        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getConcentrador " + e);
            throw new AppException("Error al getConcentrador", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getConcentrador2 (S)");
        }
        return campo;
    }

    /**
     * Funcion para Obtener algun dato de la tabla OPE_PARAMGENERALES
     * @throws java.lang.Exception
     * @return
     * @param parametros
     */

    public String getCampoRegDatosIf(String Campo, String IF) {
        log.info("getCampoRegDatosIf (E)");
        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        String campo = "";
        try {
            con.conexionDB();
            SQL.append(" SELECT " + Campo + "  as USUARIO FROM OPE_DATOS_IF where ic_if = " + IF);
            log.debug("SQL.toString() " + SQL.toString());
            PreparedStatement ps = con.queryPrecompilado(SQL.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                campo = rs.getString(1) == null ? "" : rs.getString(1);
            }
            rs.close();
            ps.close();
        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getConcentrador " + e);
            throw new AppException("Error al getConcentrador", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getCampoRegDatosIf (S)");
        }
        System.out.println("CAMPO: " + campo);
        return campo;
    }

    /**
     * Se autorizan los documentos con estatus �Pre Solicitada� o �Pre Autorizada�.
     * @throws com.netro.exception.NafinException
     * @return
     * @param numDoctos -> Contiene valores de los documentos a autorizar
     * @param ic_estatus ->Se indica el estatus de documentos a considerar
     * @param cc_acuse ->  identificador asigna a una carga en especifica
     * @param ic_epo -> Clave de la cadena que realizo la carga
     */

    public boolean actualizaEstatusManto(String ic_if, String cc_acuse, String ic_estatus, String numDoctos,
                                         String Auto, String noUsuario, String causa, List parametros,
                                         String Accion) throws NafinException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        boolean lbActualizo = true;
        boolean commit = true;
        StringBuffer query = new StringBuffer();

        log.debug("actualizaEstatusManto (E)");
        try {
            con.conexionDB();
            if (Auto.equals("1")) {
                if (!ic_estatus.equals("6")) {
                    query.append("  update ope_reg_solicitud " + "  set ic_estatus_ope =" + ic_estatus +
                                 "	,CG_USUARIO_AUTOR1 = '" + noUsuario + "'" + "	,DF_AUTORIZA_AUTOR1 = sysdate " +
                                 "  where ic_solicitud = " + numDoctos);
                } else {
                    query.append("  update ope_reg_solicitud " + "  set ic_estatus_ope =" + ic_estatus +
                                 "	,CG_USUARIO_AUTOR1 = '" + noUsuario + "'" + "	,DF_RECHAZA_AUTOR1 = sysdate " +
                                 "	,CG_CAUSA_RECHAZO_AUTOR = '" + causa + "'" + "  where ic_solicitud = " +
                                 numDoctos);

                }
            } else if (Auto.equals("2")) {
                if (!ic_estatus.equals("6")) {
                    query.append("  update ope_reg_solicitud " + "  set ic_estatus_ope =" + ic_estatus +
                                 "	,CG_USUARIO_AUTOR2 = '" + noUsuario + "'" + "	,DF_AUTORIZA_AUTOR2 = sysdate " +
                                 "  where ic_solicitud = " + numDoctos);
                } else {
                    query.append("  update ope_reg_solicitud " + "  set ic_estatus_ope =" + ic_estatus +
                                 "	,CG_USUARIO_AUTOR2 = '" + noUsuario + "'" + "	,DF_RECHAZA_AUTOR2 = sysdate " +
                                 "	,CG_CAUSA_RECHAZO_AUTOR = '" + causa + "'" + "  where ic_solicitud = " +
                                 numDoctos);

                }
            } else {
                lbActualizo = false;
                commit = false;
            }
            System.out.println("actualizaEstatusManto QUERY: " + query.toString());
            ps = con.queryPrecompilado(query.toString());
            ps.executeUpdate();
            if (ps != null)
                ps.close();

            if (Accion.equals("Eliminar")) {
                //String Num_Autorizador=  parametros.get(11).toString();
                //String Autorizador =  parametros.get(12).toString();
                //String estatus =  parametros.get(13).toString();
                this.envioCorreoNotificacion(parametros, numDoctos, "Eliminar");
            } else {
                String Num_Autorizador = parametros.get(10).toString();
                String Autorizador = parametros.get(11).toString();
                String estatus = parametros.get(12).toString();
                if (Num_Autorizador.equals("1")) {
                    this.envioCorreoNotificacion(parametros, numDoctos, "Generar");
                } else if (Num_Autorizador.equals("2")) {
                    if (Autorizador.equals("2") || Autorizador.equals("1")) {
                        if (estatus.equals("8")) {
                            this.envioCorreoNotificacion(parametros, numDoctos, "Generar");
                        }
                    }
                }
            }
        } catch (Exception e) {
            commit = false;
            lbActualizo = false;
            e.printStackTrace();
            log.error("Error actualizaEstatusManto " + e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }

        log.debug("bactualizaEstatusPendiente (S)");
        return lbActualizo;
    }

    public void envioCorreoNotificacion(List parametros, String ic_solicitud, String Operador) {
        log.info("envioCorreoNotificacion(E)");
        Correo correo = new Correo();
        StringBuffer contenido = new StringBuffer();
        UtilUsr utilUsr = new UtilUsr();
        try {
            //************* DATOS DEL USUARIO **************
            String iNoUsuario = parametros.get(7).toString();
            Usuario usuarioIfOperador = utilUsr.getUsuario(iNoUsuario);
            String nombreOper = (String) (obtieneInfoCorreo(iNoUsuario)).get("USUARIO_NOMBRE");
            String mailOper = usuarioIfOperador.getEmail();
            String telOpe = this.getTelefonoIfOperador(usuarioIfOperador.getClaveAfiliado());
            String fechaActual = parametros.get(9).toString();
            String ic_if = parametros.get(6).toString();
            String strNombreUsuario = parametros.get(0).toString();
            String nombreUsuario =
                strNombreUsuario.substring(strNombreUsuario.lastIndexOf("-") + 1, strNombreUsuario.length());
            String strPerfil = parametros.get(8).toString();
            String lblFondoLiquido = "";
            //********* DATOS DEL IF OPERADOR **************
            String num_autorizador = parametros.get(1).toString();
            //String operador_usuario =  parametros.get(2).toString();
            String operador_nombre = parametros.get(3).toString();
            String operador_mail = parametros.get(4).toString();
            //String operador_telefono =  parametros.get(5).toString();

            //********* GERENTE DE OPERACI�N DE CR�DITO  **************
            String loginUsuario = this.getCampoOpeParamGenerales("CG_CONCENTRADOR");
            Usuario usuarioObj = utilUsr.getUsuario(loginUsuario);
            String destinatario = usuarioObj.getEmail();
            String nombreUserIF = (String) (obtieneInfoCorreo(loginUsuario)).get("USUARIO_NOMBRE");
            //********* GERENTE DE OPERACI�N DE CR�DITO 2  **************
            String loginUsuario2 = this.getCampoOpeParamGenerales("CG_CONCENTRADOR2");
            Usuario usuarioObj2 = utilUsr.getUsuario(loginUsuario2);
            String destinatario2 = usuarioObj2.getEmail();
            String nombreUserIF2 = (String) (obtieneInfoCorreo(loginUsuario2)).get("USUARIO_NOMBRE");
            //********* SUBDIRECTOR DE MESA DE CONTROL DE CR�DITO  **************
            String loginSubDirector = this.getCampoOpeParamGenerales("CG_SUBDIRECTOR");
            Usuario usuarioSubDir = utilUsr.getUsuario(loginSubDirector);
            String correoSub = usuarioSubDir.getEmail();
            //String nombreUserSub = (String)(obtieneInfoCorreo(loginSubDirector)).get("USUARIO_NOMBRE");
            // datos del Autorizador 1
            String usuarioAuto1 = this.getCampoRegDatosIf("CG_AUTORIZADOR_UNO", ic_if);
            Usuario usuarioObjAuto1 = null;
            String mailAuto1 = "";
            String nombreUserAuto1 = "";
            if (!usuarioAuto1.equals("")) {
                usuarioObjAuto1 = utilUsr.getUsuario(usuarioAuto1);
                mailAuto1 = usuarioObjAuto1.getEmail();
                nombreUserAuto1 = (String) (obtieneInfoCorreo(usuarioAuto1)).get("USUARIO_NOMBRE");
            }
            // datos del Autorizador 2
            String usuarioAuto2 = this.getCampoRegDatosIf("CG_AUTORIZADOR_DOS", ic_if);
            Usuario usuarioObjAuto2 = null;
            String mailAuto2 = "";
            String nombreUserAuto2 = "";
            if (!usuarioAuto2.equals("")) {
                usuarioObjAuto2 = utilUsr.getUsuario(usuarioAuto2);
                mailAuto2 = usuarioObjAuto2.getEmail();
                nombreUserAuto2 = (String) (obtieneInfoCorreo(usuarioAuto2)).get("USUARIO_NOMBRE");
            }
            String Dest = "";
            String asunto = "";
            String CC = "";
            String causas_rechazo = "";
            //Consulta para obtener los datos de la solicitud
            HashMap registros = this.getDatosSolicitud(ic_solicitud, ic_if);
            List firmas = this.getFirmasModificatorias(ic_if);
            if (Operador.equals("Generar")) {
                if (!loginUsuario2.equals("")) {
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                        // En caso de que tenga Firma Mancomunada
                        " Estimado(s): C.P. " + nombreUserIF + " y " + nombreUserIF2 + " <BR>" +
                        " Por este conducto, hago de su conocimiento que el d�a de hoy " + fechaActual +
                        " se registr� la solicitud de recursos del Usuario " + registros.get("USUARIO") +
                        " correspondiente al Intermediario Financiero " + registros.get("NOMBRE_IF") + " <BR>");
                    if (num_autorizador.equals("1")) {
                        asunto = "Registro de Solicitud de Recursos ";
                        Dest = destinatario + "," + destinatario2;
                        CC = correoSub + "," + operador_mail + "," + mailAuto1;
                    }
                    if (num_autorizador.equals("2")) {
                        asunto = "Registro de Solicitud de Recursos ";
                        Dest = destinatario + "," + destinatario2 + "," + correoSub;
                        CC = operador_mail;
                    }


                } else {
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                        // En caso de que tenga Firma Mancomunada
                        " Estimado (a): C.P. " + nombreUserIF + " <BR>" +
                        " Por este conducto, hago de su conocimiento que el d�a de hoy " + fechaActual +
                        " se registr� la solicitud de recursos del Usuario " + registros.get("USUARIO") +
                        " correspondiente al Intermediario Financiero " + registros.get("NOMBRE_IF") + " <BR>");
                    if (num_autorizador.equals("1")) {
                        asunto = "Registro de Solicitud de Recursos ";
                        Dest = destinatario;
                        CC = correoSub + "," + operador_mail + "," + mailAuto1;
                    }
                    if (num_autorizador.equals("2")) {
                        asunto = "Registro de Solicitud de Recursos - Pre Solicitada ";
                        Dest = destinatario + "," + correoSub;
                        CC = operador_mail;
                    }
                    asunto = "Registro de Solicitud de Recursos - Pre Solicitada ";
                }
            } else if (Operador.equals("Eliminar")) {

                asunto = "Rechazo de Solicitud de Recursos-Pre Solicitada";
                causas_rechazo = parametros.get(10).toString();
                Dest = operador_mail;
                if (num_autorizador.equals("1")) {
                    Dest = operador_mail;
                }
                if (num_autorizador.equals("2")) {
                    Dest = operador_mail + "," + mailAuto1 + "," + mailAuto2;

                }
                contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                    // En caso de que tenga Firma Mancomunada
                    operador_nombre + " : <BR>" +
                    "Por este conducto, se hace de su conocimiento que la Solicitud de  " +
                    " Recursos a continuaci�n descrita no fue aprobada para enviarse a Nacional " +
                    " Financiera, N.C. como tramite de disposici�n, en virtud de las causas " +
                    " se�aladas a continuaci�n:<BR>");

            }
            contenido.append("<br> <table width='600' align='CENTER' border='1'  >" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Importe						 </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>$" +
                             Comunes.formatoDecimal(String.valueOf(registros.get("IMPORTE")), 2) + "</td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Moneda 						 </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("MONEDA") + "</td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Fecha de Env�o Solicitud  </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACARGA") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Hora de Env�o 				 </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("HORACARGA") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='200'  style='font-size:11pt;font-family: Calibri'> <b> Usuario IF 					 </td>  <td align='left' width='300'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("USUARIO") + "</td> </tr>" + " </table> <br>  ");


            contenido.append(" <br>  <table width='800' align='CENTER' border='1'  style='font-size:11pt;font-family: Calibri' >" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			   <b>  Fecha Solicitud 								    </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHASOLICITUD") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				   <b>  Nombre IF  											 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NOMBRE_IF") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				   <b>  N�mero de Cuenta 									 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NUM_CUENTAS") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			   <b>  Banco 													 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("BANCO") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			   <b>  Cuenta CLABE 										 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("CLABE") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'colspan='2'>  <b>  Contratos  											 </td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>             <b>  Nombre Contrato 									 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NOMBRECONTRATO") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>             <b>  Fecha Firma Contrato 							    </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACONTRATO") + "</td></tr>");

            firmas = this.getFirmasModificatorias(ic_if); //obtengo las firmas Modificatorias
            if (firmas.size() > 0) {
                contenido.append(" <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>             <b>  Contratos Modificatorios 						 </td> <td align='left' width='500'  style='font-size:11pt;font-family: Calibri'>" +
                                 registros.get("MODIFICATORIOS") + "</td></tr>");

                for (int i = 0; i < firmas.size(); i++) {
                    HashMap datos = (HashMap) firmas.get(i);
                    String descripcion = datos.get("DESCRIPCION").toString();
                    String fecha = datos.get("FECHA").toString();
                    contenido.append(" <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>  <b>" +
                                     descripcion + " </td> <td align='left' width='400' >" + fecha + "</td></tr>");
                }
            }
            contenido.append("<tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 	  <b>  Nombre Contrato   </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("NOMBRECONTRATO_OE") + "</td></tr>" +
                             " <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 	  <b>  Fecha Firma Contrato Operaci�n Electronica    </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACONTRATO_OE") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300' colspan='2'  style='font-size:11pt;font-family: Calibri'><b>  Datos del Prestamo  			             	  </td> </tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				  <b>  Destino de los recursos			              </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("RECURSOS") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			  <b>  Fecha de Primer pago de Capital               </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHACAPITAL") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>				  <b>  Fecha de Primer pago de Inter�s               </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHAINTERES") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'> 			  <b>  Fecha de Vencimiento 				              </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("FECHAVENCIMIENTO") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Tasa de Inter�s                               </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("TASAINTERES") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Observaciones                                 </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             registros.get("OBSERVACIONES") + "</td></tr>" +
                             " <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Usuario IF                                    </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'>" +
                             nombreUsuario + "</td></tr>");
            if (strPerfil.equals("IF 5MIC") || strPerfil.equals("IF 5CP") || strPerfil.equals("IF 4MIC")) {
                lblFondoLiquido = this.getSaldoLiquido(ic_if);
                contenido.append(" <tr> <td bgcolor='silver' align='left' width='300'  style='font-size:11pt;font-family: Calibri'>            <b>  Fondo L�quido                                     </td> <td align='left' width='500' style='font-size:11pt;font-family: Calibri'> $" +
                                 Comunes.formatoDecimal(lblFondoLiquido, 2) + "</td></tr>");
            }
            contenido.append(" </table> <BR>");
            //Tabla de Amortizaci�n
            List regTabla = this.getTablaAmortizacion(ic_solicitud);
            contenido.append("<br>  <table width='500' align='CENTER' border='1' >" +
                             " <tr> <td bgcolor='silver' align='center' style='font-size:11pt;font-family: Calibri'><b>Periodo </td> " +
                             "<td bgcolor='silver' align='center' style='font-size:11pt;font-family: Calibri'><b> Fecha</td>  " +
                             "<td bgcolor='silver' align='center' style='font-size:11pt;font-family: Calibri'><b> Amortizaci�n</td></tr>");
            for (int i = 0; i < regTabla.size(); i++) {
                HashMap datos = (HashMap) regTabla.get(i);
                contenido.append(" <tr>" +
                                 " <td align='center' width='100'  style='font-size:11pt;font-family: Calibri'>" +
                                 datos.get("PERIODO").toString() + "</td>" +
                                 " <td align='center' width='100'  style='font-size:11pt;font-family: Calibri'>" +
                                 datos.get("FECHA").toString() + "</td>" +
                                 " <td align='center' width='100'  style='font-size:11pt;font-family: Calibri'> $" +
                                 Comunes.formatoDecimal(String.valueOf(datos.get("AMORTIZACION").toString()), 2) +
                                 "</td>" + "</tr>");
            }
            contenido.append(" </table> <BR>");
            if (Operador.equals("Generar")) {
                contenido.append(" <BR>ATENTAMENTE" + " <BR>Nacional Financiera S.N.C. <BR>");
                contenido.append("</span></span>");
            }
            if (Operador.equals("Eliminar")) {
                contenido.append(" <BR>Causas de Rechazo:<BR>" + causas_rechazo + " <BR><BR>" +
                                 " <BR>Sin otro particular, reciba un cordial saludos." + " <BR><BR>ATENTAMENTE" +
                                 " <BR>" + nombreOper + " <BR>" + mailOper + " <BR>" + telOpe);

            }
            if (Operador.equals("Generar")) {
                correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", Dest, CC, asunto, contenido.toString(),
                                                   null, null);
            }

            if (Operador.equals("Eliminar")) {
                correo.enviarTextoHTML("no_response@nafin.gob.mx", Dest, asunto, contenido.toString());
            }

        } catch (Throwable e) {
            log.error("Error al enviar correo  " + e);
            throw new AppException("Error al enviar correo ", e);
        } finally {
            log.info("envioCorreoNotificacion(S)");
        }
    }

    /**
     * Funci�n que env�a por correo electr�nico el concentrado de las solicitudes de recursos autorizadas por la Gerencia de Operaciones de Cr�dito
     * @throws netropology.utilerias.AppException
     * @return
     * @param ic_if
     * @param ic_solicitud
     */
    public void envioCorreoConcentrado(String path, String dirPantalla) throws NafinException {
        log.info("envioCorreoConcentrado(E)");
        Correo correo = new Correo();
        StringBuffer contenido = new StringBuffer();
        UtilUsr utilUsr = new UtilUsr();
        ArrayList listaDeImagenes = null;
        ArrayList listaDeArchivos = new ArrayList();
        HashMap archivoAdjunto = new HashMap();
        String Dest = "";
        String asunto = "";
        String rutaArchivo = "";
        try {
            String correoDirector = this.getCampoOpeParamGenerales("CG_EMAIL_DIRECTOR");
            if (!correoDirector.equals("")) {
                // DATOS DEL GERENTE DE OPERCONES DE CR�DITO
                String loginConcentrador = this.getCampoOpeParamGenerales("CG_CONCENTRADOR");
                Usuario usuarioConcentrador = utilUsr.getUsuario(loginConcentrador);
                String correoCon = usuarioConcentrador.getEmail();
                //String nombreUserCon = (String)(obtieneInfoCorreo(loginConcentrador)).get("USUARIO_NOMBRE");
                // DATOS DEL GERENTE DE OPERCONES DE CR�DITO 2
                String loginGOC2 = this.getCampoOpeParamGenerales("CG_CONCENTRADOR2");
                Usuario usuarioGOC2 = null;
                String correoGOC2 = "";
                if (!loginGOC2.equals("")) {
                    usuarioGOC2 = utilUsr.getUsuario(loginGOC2);
                    correoGOC2 = usuarioGOC2.getEmail();
                }
                // DATOS DEL SUBDIRECTOR DE MESA DE CONTROL DE CREDITO
                String loginSubDir = this.getCampoOpeParamGenerales("CG_SUBDIRECTOR");
                Usuario usuarioSubDir = null;
                String correoSubDir = "";
                if (!loginSubDir.equals("")) {
                    usuarioSubDir = utilUsr.getUsuario(loginSubDir);
                    correoSubDir = usuarioSubDir.getEmail();
                }

                String nombreArchivo = this.getArchivoSoliAutorizada(path, "5", dirPantalla);
                //asunto ="Operacion Electronica � Solicitudes de Recursos Autorizadas ";
                asunto = "Operaci\u00F3n Electr\u00F3nica � Solicitudes de Recursos Autorizadas";
                if (!correoGOC2.equals("")) {
                    Dest = correoDirector + "," + correoCon + "," + correoSubDir + "," + correoGOC2;
                } else {
                    Dest = correoDirector + "," + correoCon + "," + correoSubDir;
                }
                if (!nombreArchivo.equals("")) {
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                                     " Estimado(s): <BR><BR>" +
                                     " Por medio del presente se les notifica que las solicitudes de recursos especificadas en el archivo adjunto fueron autorizadas por la  " +
                                     " Gerencia de Operaciones de Cr�dito el d�a de hoy.");
                } else {
                    contenido.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">" +
                                     " Estimado(s): <BR><BR>" +
                                     " Por medio del presente se les notifica que el d�a de hoy no se autorizaron " +
                                     " Solicitudes de Recursos.");
                }
                contenido.append(" <BR><BR>Sin otro particular, reciba un cordial saludo." +
                                 " <BR><BR><BR><BR>ATENTAMENTE" +
                                 " <BR> Nacional Financiera, S.N.C. " +

                                 " <BR><BR><BR>Con fundamento en el art�culo 142 de la Ley de Instituciones de Cr�dito, 14 fracci�n I y 15 de la Ley Federal de Transparencia y Acceso a la" +
                                 " informaci�n P�blica Gubernamental; as� como al art�culo 30 de su Reglamento, el contenido del presente mensaje de correo electr�nico es de " +
                                 " car�cter Reservado." +

                                 " <BR><BR>Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr�nicos de entrada. " +
                                 " Por favor no responda este mensaje." + " <BR>");
                rutaArchivo = path + nombreArchivo;
                if (!nombreArchivo.equals("")) {
                    archivoAdjunto.put("FILE_FULL_PATH", rutaArchivo);
                    listaDeArchivos.add(archivoAdjunto);
                    correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", Dest, "", asunto,
                                                       contenido.toString(), listaDeImagenes, listaDeArchivos);
                    log.debug("Concentrado de Solicitudes Autorizadas enviado con �xito");
                } else {
                    correo.enviarTextoHTML("no_response@nafin.gob.mx", Dest, asunto, contenido.toString());
                    log.debug("Correo de notificaci�n enviado con �xito.");
                }
            }
        } catch (Throwable e) {
            log.error("Error al enviar correo  " + e);
            throw new AppException("Error al enviar correo ", e);
        } finally {
            log.info("envioCorreoConcentrado(S)");
        }
    }

    /**
     * Metodo para generar el archivo con las solicitudes que
     * se autorizaron
     * F-035-2014
     * @throws java.lang.Exception
     * @return
     * @param ic_solicitud
     * @param path
     */
    public String getArchivoSoliAutorizada(String path, String lisEstatus, String dirPantalla) {
        log.info("getArchivoSoliAutorizada (E)");
        AccesoDB con = new AccesoDB();
        boolean commit = true;
        PreparedStatement ps = null;
        PreparedStatement ps2 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        //String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
        String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
        //String diaActual    = fechaActual.substring(0,2);
        //String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
        //String anioActual   = fechaActual.substring(6,10);
        //String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

        StringBuffer SQL = new StringBuffer();
        List varBind = new ArrayList();

        String nombreArchivo = "";
        //ComunesXLSX				documentoXLSX 	= null;
        Writer writer = null;
        // Estilos en la plantilla xlsx
        HashMap estilos = new HashMap();
        estilos.put("PERCENT", "1");
        estilos.put("COEFF", "2");
        estilos.put("CURRENCY", "3");
        estilos.put("DATE", "4");
        estilos.put("HEADER", "5");
        estilos.put("CSTRING", "6");
        estilos.put("TITLE", "7");
        estilos.put("SUBTITLE", "8");
        estilos.put("RIGHTCSTRING", "9");
        estilos.put("CENTERCSTRING", "10");

        boolean exiReg = false;
        int total = 0;
        String qrySql = "";
        try {

            con.conexionDB();
            // Crear archivo XLSX
            this.inicializaHoja(path, estilos);
            writer = this.creaHojaAux(path);
            short indiceRenglon = 0;
            short indiceCelda = 0;

            SQL.append(" SELECT ors.ic_if cveif,ci.cg_razon_social nombreinter,ors.cc_acuse ccacuse,  ors.ig_numero_prestamo numprestamo,   " +
                       " TO_CHAR (ors.df_solicitud, 'dd/mm/yyyy') fecsolicitud,  oc.cg_nombre_contrato nombrecontrato,   " +
                       " TO_CHAR (odi.df_firma_contrato, 'dd/mm/yyyy') fecfirmacontrato, odi.cg_cont_modificatorios countmodif,  " +
                       " odi.cg_contrato_oe nombrecontratooe,TO_CHAR(odi.df_firma_contrato_oe,'dd/mm/yyyy') fecfirmacontratooe,ors.fn_monto_solicitud importesolic, cm.cd_nombre nombremoneda, " +
                       "  ors.cg_des_recursos destinorecurso,TO_CHAR (ors.df_pago_capital, 'dd/mm/yyyy') fecpagocapital,  " +
                       "	TO_CHAR (ors.df_pago_interes, 'dd/mm/yyyy') fecpagointeres,  " +
                       "  TO_CHAR (ors.df_vencimiento, 'dd/mm/yyyy') fecvencimiento,ors.cg_tipo_tasa tipotasa, ct.cd_nombre nombretasa, " +
                       " ors.in_tasa_aceptada tasainteres,ors.ic_estatus_ope estatussolic, oe.cd_descripcion nombreestatus,   " +
                       "  TO_CHAR (ors.df_operado, 'dd/mm/yyyy HH24:mi:ss') fecAceptada  " +
                       " FROM ope_reg_solicitud ors,  " + " ope_datos_if odi,  " + " comcat_moneda cm,    " +
                       " opecat_estatus oe, " + " opecat_contrato oc,   " + " comcat_if ci ,  " + " comcat_tasa ct,  " +
                       "  ope_acuse ac   " + " WHERE ors.ic_if = odi.ic_if   " + " AND ac.cc_acuse = ors.CC_ACUSE    " +
                       " AND ors.ic_moneda = cm.ic_moneda    " + " AND ors.ic_tasa = ct.ic_tasa(+)     " +
                       " AND ors.ic_estatus_ope = oe.ic_estatus_ope  " + " AND ors.ic_contrato = oc.ic_contrato  " +
                       " AND odi.ic_if = ci.ic_if   " + " AND df_operado >= (TO_DATE ('" + fechaActual +
                       " 17:01:00', 'dd/mm/yyyy hh24:mi:ss') -1)   " + " AND  df_operado <= TO_DATE ('" + fechaActual +
                       " 17:00:00', 'dd/mm/yyyy hh24:mi:ss')   " + " AND ors.ic_estatus_ope in (?)");
            varBind.add(lisEstatus);
            log.debug("SQL.toString() " + SQL.toString());
            log.debug("varBind " + varBind);
            ps = con.queryPrecompilado(SQL.toString(), varBind);
            rs = ps.executeQuery();
            qrySql =
                "SELECT ofm.cg_descripcion descmod, " +
                "       TO_CHAR (ofm.df_firma_modificatoria, 'dd/mm/yyyy') fecmod " + "  FROM ope_det_fir_modifi ofm " +
                " WHERE ofm.ic_if = ? " + " ORDER BY ofm.ic_firma_modifi ";
            String allFecModCont = "";
            String fecModCont = "";

            while (rs.next()) {
                if (total == 0) {
                    this.agregaRenglon(indiceRenglon++);
                    indiceCelda = 0;
                    this.agregaCelda(indiceCelda++, "Intermediario", "HEADER");
                    this.agregaCelda(indiceCelda++, "No. Solicitud", "HEADER");
                    this.agregaCelda(indiceCelda++, "No. Pr�stamo", "HEADER");
                    this.agregaCelda(indiceCelda++, "Fecha y hora env�o", "HEADER");
                    this.agregaCelda(indiceCelda++, "Nombre Contrato", "HEADER");
                    this.agregaCelda(indiceCelda++, " Fecha Firma Contrato", "HEADER");
                    this.agregaCelda(indiceCelda++, " Contratos Modificatorios", "HEADER");
                    this.agregaCelda(indiceCelda++, "Fecha Firma Modificatorios", "HEADER");
                    this.agregaCelda(indiceCelda++, "Nombre Contrato OE", "HEADER");
                    this.agregaCelda(indiceCelda++, "Fecha Firma Contrato OE", "HEADER");
                    this.agregaCelda(indiceCelda++, "Importe", "HEADER");
                    this.agregaCelda(indiceCelda++, "Moneda", "HEADER");
                    this.agregaCelda(indiceCelda++, "Destino de los Recursos", "HEADER");
                    this.agregaCelda(indiceCelda++, "Fecha Primer pago de Capital", "HEADER");
                    this.agregaCelda(indiceCelda++, "Fecha Primer pago de Intereses", "HEADER");
                    this.agregaCelda(indiceCelda++, " Fecha de Vencimiento", "HEADER");
                    this.agregaCelda(indiceCelda++, "Tipo de Tasa", "HEADER");
                    this.agregaCelda(indiceCelda++, "Tasa", "HEADER");
                    this.agregaCelda(indiceCelda++, "Valor", "HEADER");
                    this.agregaCelda(indiceCelda++, "Estatus", "HEADER");
                    this.agregaCelda(indiceCelda++, "Fecha y hora Autorizaci�n", "HEADER");
                    this.finalizaRenglon();

                }
                exiReg = true;
                allFecModCont = "";
                ps2 = con.queryPrecompilado(qrySql);
                ps2.setLong(1, Long.parseLong(rs.getString("cveif")));
                rs2 = ps2.executeQuery();
                fecModCont = "";
                allFecModCont = "";
                while (rs2.next()) {
                    fecModCont = rs2.getString("descmod") + ": " + rs2.getString("fecmod");
                    allFecModCont += ("".equals(allFecModCont)) ? fecModCont : (", " + fecModCont);
                }
                rs2.close();
                ps2.close();

                String NOMBREINTER = rs.getString("NOMBREINTER") == null ? "" : rs.getString("NOMBREINTER");
                String CCACUSE = rs.getString("CCACUSE") == null ? "" : rs.getString("CCACUSE");
                String NUMPRESTAMO = rs.getString("NUMPRESTAMO") == null ? "" : rs.getString("NUMPRESTAMO");
                String FECSOLICITUD = rs.getString("FECSOLICITUD") == null ? "" : rs.getString("FECSOLICITUD");
                String NOMBRECONTRATO = rs.getString("NOMBRECONTRATO") == null ? "" : rs.getString("NOMBRECONTRATO");
                String FECFIRMACONTRATO =
                    rs.getString("FECFIRMACONTRATO") == null ? "" : rs.getString("FECFIRMACONTRATO");
                String COUNTMODIF = rs.getString("COUNTMODIF") == null ? "" : rs.getString("COUNTMODIF");
                String FECFIRMACONTRATOOE =
                    rs.getString("FECFIRMACONTRATOOE") == null ? "" : rs.getString("FECFIRMACONTRATOOE");
                String NOMBRECONTRATOOE =
                    rs.getString("NOMBRECONTRATOOE") == null ? "" : rs.getString("NOMBRECONTRATOOE");
                String IMPORTESOLIC = rs.getString("IMPORTESOLIC") == null ? "" : rs.getString("IMPORTESOLIC");
                String NOMBREMONEDA = rs.getString("NOMBREMONEDA") == null ? "" : rs.getString("NOMBREMONEDA");
                String DESTINORECURSO = rs.getString("DESTINORECURSO") == null ? "" : rs.getString("DESTINORECURSO");
                String FECPAGOCAPITAL = rs.getString("FECPAGOCAPITAL") == null ? "" : rs.getString("FECPAGOCAPITAL");
                String FECPAGOINTERES = rs.getString("FECPAGOINTERES") == null ? "" : rs.getString("FECPAGOINTERES");
                String FECVENCIMIENTO = rs.getString("FECVENCIMIENTO") == null ? "" : rs.getString("FECVENCIMIENTO");
                String TIPOTASA = rs.getString("TIPOTASA") == null ? "" : rs.getString("TIPOTASA");
                String NOMBRETASA = rs.getString("NOMBRETASA") == null ? "" : rs.getString("NOMBRETASA");
                String TASAINTERES = rs.getString("TASAINTERES") == null ? "" : rs.getString("TASAINTERES");
                //String ESTATUSSOLIC =  rs.getString("ESTATUSSOLIC")==null?"":rs.getString("ESTATUSSOLIC");
                String NOMBREESTATUS = rs.getString("NOMBREESTATUS") == null ? "" : rs.getString("NOMBREESTATUS");
                String FECACEPTADA = rs.getString("FECACEPTADA") == null ? "" : rs.getString("FECACEPTADA");

                this.agregaRenglon(indiceRenglon++);
                indiceCelda = 0;
                this.agregaCelda(indiceCelda++, NOMBREINTER, "CSTRING");
                this.agregaCelda(indiceCelda++, CCACUSE, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, NUMPRESTAMO, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, FECSOLICITUD, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, NOMBRECONTRATO, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, FECFIRMACONTRATO, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, COUNTMODIF, "CSTRING");
                this.agregaCelda(indiceCelda++, allFecModCont, "CSTRING");
                this.agregaCelda(indiceCelda++, NOMBRECONTRATOOE, "CSTRING");
                this.agregaCelda(indiceCelda++, FECFIRMACONTRATOOE, "CENTERCSTRING");

                this.agregaCelda(indiceCelda++, IMPORTESOLIC, "RIGHTCSTRING");
                this.agregaCelda(indiceCelda++, NOMBREMONEDA, "CSTRING");
                this.agregaCelda(indiceCelda++, DESTINORECURSO, "CSTRING");
                this.agregaCelda(indiceCelda++, FECPAGOCAPITAL, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, FECPAGOINTERES, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, FECVENCIMIENTO, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, TIPOTASA, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, NOMBRETASA, "CSTRING");
                this.agregaCelda(indiceCelda++, TASAINTERES, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, NOMBREESTATUS, "CENTERCSTRING");
                this.agregaCelda(indiceCelda++, FECACEPTADA, "CENTERCSTRING");
                this.finalizaRenglon();
                if (total % 250 == 0) {
                    this.flush();
                    total = 1;
                }
                total++;
            }
            if (!exiReg) {
                this.agregaRenglon(indiceRenglon++);
                indiceCelda = 0;
                this.agregaCelda(indiceCelda++, "No se encontr� ning�n registro.", "HEADER", 20);
                this.finalizaRenglon();

                log.debug("No hay solicitudes autorizadas a notificar.");
                nombreArchivo = "";
            } else {
                this.finalizaHoja();
                nombreArchivo =
                    this.finalizaDocumentoAux(path, dirPantalla + "Solicitudes_Recursos_Autorizadas.template.xlsx");
            }
        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getArchivoSoliAutorizada " + e);
            if (writer != null) {
                try {
                    writer.close();
                } catch (Exception exception) {
                }
            }

            throw new AppException("Ocurri� un error al generar el Archivo XLS con el detalle de las Comisiones.");
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                }
            }
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getArchivoSoliAutorizada (S)");
        }
        return nombreArchivo;
    }
    //*****************************************************************************
    private void flush() throws IOException {
        xml.flush();
    }

    private void inicializaHoja(String directorioTemporal, HashMap estilos) {

        this.estilos = estilos;
        this.directorioTemporal = directorioTemporal;

        this.archivo = new CreaArchivo();
        this.cuentaHojas = 0;

        this.datosHojaInicializados = false;
        this.columnasCombinadas = new StringBuffer(256);
        this.cuentaColumnasCombinadas = 0;

    }

    private Writer creaHojaAux(String directorioTemporal) throws Exception {
        CreaArchivo archivo = new CreaArchivo();
        cuentaHojas++;
        final String XML_ENCODING = "UTF-8";
        if (cuentaHojas > 1) {
            throw new Exception("La versi�n actual no tiene soporte para agregar m�s de una hoja.");
        }

        hoja = "sheet_" + archivo.nombreArchivo() + ".xml";
        xml = new OutputStreamWriter(new FileOutputStream(directorioTemporal + hoja), XML_ENCODING);
        xml.write("<?xml version=\"1.0\" encoding=\"" + XML_ENCODING + "\"?>" +
                  "<worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">");

        return xml;

    }

    private String finalizaDocumentoAux(String directorioTemporal, String plantilla)

        throws Exception {
        String meses[] = {
            "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
            "Noviembre", "Diciembre"
        };
        String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
        String diaActual = fechaActual.substring(0, 2);
        String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
        String anioActual = fechaActual.substring(6, 10);
        List lista = new ArrayList();

        HashMap updatedFile = new HashMap();
        updatedFile.put("NAME", "xl/worksheets/sheet1.xml");
        updatedFile.put("PATH", directorioTemporal + hoja);
        lista.add(updatedFile);

        String archivoXLSX =
            "Solicitudes_Recursos_Autorizadas_" + diaActual + "_" + mesActual + "_" + anioActual + ".xlsx";
        ComunesZIP.updateFiles(plantilla, lista, directorioTemporal + archivoXLSX);

        return archivoXLSX;

    }


    private void escribeLongitudesColumnas() throws IOException {

        if (longitudesColumna == null || longitudesColumna.length == 0)
            return;

        xml.write("<cols>");
        for (int i = 0, columna = 1; i < longitudesColumna.length; i++, columna++) {
            xml.write("<col min=\"");
            xml.write(String.valueOf(columna));
            xml.write("\" max=\"");
            xml.write(String.valueOf(columna));
            xml.write("\" width=\"");
            xml.write(longitudesColumna[i]);
            xml.write("\" bestFit=\"1\" customWidth=\"1\"/>");
        }
        xml.write("</cols>");

    }

    private void agregaRenglon(int rownum) throws IOException {
        if (!datosHojaInicializados) {
            escribeLongitudesColumnas();
            xml.write("<sheetData>\n");
            datosHojaInicializados = true;
        }
        xml.write("<row r=\"" + (rownum + 1) + "\">\n");
        this.indiceRenglon = rownum;
    }

    private void finalizaRenglon() throws IOException {
        xml.write("</row>\n");
    }

    private void agregaCelda(int indiceColumna, String valor, String estilo, int colspan) throws Exception {

        String ref = getClaveReferencia(indiceRenglon, indiceColumna);
        String indiceEstilo = (String) estilos.get(estilo);

        xml.write("<c r=\"");
        xml.write(ref);
        xml.write("\" t=\"inlineStr\"");
        if (indiceEstilo != null) {
            xml.write(" s=\"");
            xml.write(indiceEstilo);
            xml.write("\"");
        }
        xml.write(">");
        xml.write("<is><t>");
        escapaCaracteresEspeciales(valor);
        xml.write("</t></is>");
        xml.write("</c>");

        if (colspan > 1) {
            this.combinaColumnas(indiceRenglon, indiceColumna, colspan);
            // Agregar nodos fantasma
            for (int i = 1; i < colspan; i++) {
                ref = getClaveReferencia(indiceRenglon, indiceColumna + i);
                xml.write("<c r=\"");
                xml.write(ref);
                xml.write("\" t=\"inlineStr\"");
                if (indiceEstilo != null) {
                    xml.write(" s=\"");
                    xml.write(indiceEstilo);
                    xml.write("\"");
                }
                xml.write(">");
                xml.write("<is><t>");
                xml.write("");
                xml.write("</t></is>");
                xml.write("</c>");
            }
        }

    }

    private String getClaveReferencia(int indiceRenglon, int indiceColumna) throws Exception {

        StringBuffer buffer = new StringBuffer();
        int valorA = (int) 'A';
        if (indiceColumna > 25) { // Solo rango A - Z
            throw new Exception("No se como determinar la clave de la siguiente columna");
        }

        buffer.append((char) (valorA + indiceColumna));
        buffer.append(String.valueOf(indiceRenglon + 1));
        return buffer.toString();

    }

    private void agregaCelda(int indiceColumna, String valor, String estilo) throws Exception {
        agregaCelda(indiceColumna, valor, estilo, 1);
    }

    private void escapaCaracteresEspeciales(String cadena) throws IOException {

        if (cadena == null || cadena.length() == 0) {
            return;
        }

        for (int i = 0; i < cadena.length(); i++) {
            char c = cadena.charAt(i);
            if (c == '<') {
                xml.write("&lt;");
            } else if (c == '>') {
                xml.write("&gt;");
            } else if (c == '"') {
                xml.write("&quot;");
            } else if (c == '&') {
                xml.write("&amp;");
            } else {
                xml.write(c);
            }
        }

    }

    private void combinaColumnas(int indiceRenglon, int indiceColumna, int colspan) throws Exception {

        colspan = colspan - 1;

        String inicio = getClaveReferencia(indiceRenglon, indiceColumna);
        String fin = getClaveReferencia(indiceRenglon, indiceColumna + colspan);

        columnasCombinadas.append("<mergeCell ref=\"");
        columnasCombinadas.append(inicio);
        columnasCombinadas.append(":");
        columnasCombinadas.append(fin);
        columnasCombinadas.append("\"/>");
        cuentaColumnasCombinadas++;

    }

    private void finalizaHoja() throws IOException {
        if (!datosHojaInicializados) {
            escribeLongitudesColumnas();
            xml.write("<sheetData>\n");
            datosHojaInicializados = true;
        }
        xml.write("</sheetData>");
        finalizaCombinacionColumnas();
        xml.write("</worksheet>");
        xml.flush();
        xml.close();
    }

    private void finalizaCombinacionColumnas() throws IOException {
        if (cuentaColumnasCombinadas > 0) {
            xml.write("<mergeCells count=\"");
            xml.write(String.valueOf(cuentaColumnasCombinadas));
            xml.write("\">");
            xml.write(columnasCombinadas.toString());
            xml.write("</mergeCells>");
        }
    }
    //*****************************************************************************

    /**
     * Funcion que regresa datos de la pantalla Par�metros Generales, pasandole como parametro el usuario
     * @param Usuario
     * @return
     * @throws netropology.utilerias.AppException
     */
    public HashMap obtieneInfoConcentrador(String Usuario) throws AppException {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        String strSQL = "";
        String nombreConc = "", mailConc = "";
        log.info("obtieneInfoConcentrador (E) ");
        try {
            con.conexionDB();
            strSQL =
                "SELECT cg_mail, cg_concentrador usuarioconc, cg_tel_concentrador telefonoconc  " +
                "   FROM ope_paramgenerales " + "  where  CG_CONCENTRADOR =" + Usuario + "	AND  ic_email = 1";
            log.debug("strSQL :::::  " + strSQL);
            ps = con.queryPrecompilado(strSQL);
            rs = ps.executeQuery();
            HashMap hmData = new HashMap();
            if (rs.next()) {
                String usuarioConc = rs.getString("usuarioconc") == null ? "" : rs.getString("usuarioconc");
                String telefonoConc = rs.getString("telefonoconc") == null ? "" : rs.getString("telefonoconc");
                String mailFondoLiquido = rs.getString("cg_mail") == null ? "" : rs.getString("cg_mail");

                UtilUsr utilUsr = new UtilUsr();
                System.out.println("usuarioConc  :::::::::::::::::::::" + usuarioConc.replaceAll("\\s", "") + ":::");

                if (!"".equals(usuarioConc.replaceAll("\\s", ""))) {
                    Usuario userConc = utilUsr.getUsuario(usuarioConc);
                    nombreConc =
                        userConc.getNombre() + " " + userConc.getApellidoPaterno() + " " +
                        userConc.getApellidoMaterno();
                    mailConc = userConc.getEmail();
                }
                hmData.put("CONCENTRADOR", "1");
                hmData.put("GERENTE_USUARIO", usuarioConc);
                hmData.put("GERENTE_NOMBRE", nombreConc);
                hmData.put("GERENTE_MAIL", mailConc);
                hmData.put("GERENTE_TELEFONO", telefonoConc);
                hmData.put("MAILS_FONDO_LIQUIDO", mailFondoLiquido);
            } else {
                rs.close();
                ps.close();
                strSQL = "";
                strSQL =
                    "SELECT cg_mail, CG_CONCENTRADOR2 usuarioconc, CG_TEL_CONCENTRADOR2 telefonoconc  " +
                    " FROM ope_paramgenerales " + " where  CG_CONCENTRADOR2 =" + Usuario + "	AND  ic_email = 1";

                log.info("strSQL :::::  " + strSQL);
                ps1 = con.queryPrecompilado(strSQL);
                rs1 = ps1.executeQuery();
                if (rs1.next()) {
                    String usuarioConc = rs1.getString("usuarioconc") == null ? "" : rs1.getString("usuarioconc");
                    String telefonoConc = rs1.getString("telefonoconc") == null ? "" : rs1.getString("telefonoconc");
                    String mailFondoLiquido = rs1.getString("cg_mail") == null ? "" : rs1.getString("cg_mail");

                    UtilUsr utilUsr = new UtilUsr();
                    System.out.println("usuarioConc  :::::::::::::::::::::" + usuarioConc.replaceAll("\\s", "") +
                                       ":::");

                    if (!"".equals(usuarioConc.replaceAll("\\s", ""))) {
                        Usuario userConc = utilUsr.getUsuario(usuarioConc);
                        nombreConc =
                            userConc.getNombre() + " " + userConc.getApellidoPaterno() + " " +
                            userConc.getApellidoMaterno();
                        mailConc = userConc.getEmail();
                    }
                    hmData.put("CONCENTRADOR", "2");
                    hmData.put("GERENTE_USUARIO", usuarioConc);
                    hmData.put("GERENTE_NOMBRE", nombreConc);
                    hmData.put("GERENTE_MAIL", mailConc);
                    hmData.put("GERENTE_TELEFONO", telefonoConc);
                    hmData.put("MAILS_FONDO_LIQUIDO", mailFondoLiquido);
                }
                rs1.close();
                ps1.close();
            }
            rs.close();
            ps.close();

            return hmData;
        } catch (Throwable t) {
            throw new AppException("Error al obetener datos de gerente y subdirector paramaterizados", t);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("obtieneInfoConcentrador (S) ");
        }

    }

    /**
     * Funci�n que nos dice s� Opera Firma Mancomunada
     *
     */
    public String getOperaFirmaMancomunada(String IC_IF) {
        log.info("getOperaFirmaMancomunada (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        String operaFirmaManc = "";

        try {

            con.conexionDB();
            SQL.append(" select CG_OPERA_FIRMA_MANC from ope_datos_if" + "	where ic_if  = " + IC_IF);


            log.debug("SQL.toString() " + SQL.toString());

            PreparedStatement ps = con.queryPrecompilado(SQL.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                operaFirmaManc = rs.getString("CG_OPERA_FIRMA_MANC") == null ? "" : rs.getString("CG_OPERA_FIRMA_MANC");
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getOperaFirmaMancomunada " + e);
            throw new AppException("Error al getOperaFirmaMancomunada", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getOperaFirmaMancomunada (S)");
        }
        return operaFirmaManc;
    }

    private boolean esNumeroNegativo(String numero) {
        int cont = 0;
        String cadena_b;
        if (numero.charAt(0) == '-') {
            if (numero.length() > 1) {
                cont++;
            } else {
                return false;
            }
            cadena_b = numero.substring(1);
            if (!Comunes.esDecimalPositivo(cadena_b)) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     *  Este metodo inserta en la tabla DETALLE_ERROR_ANAPM los errores encontrados en el archivo cargado
     */
    public List insertarTablaDetalle(String dataFile, String path, String IF, String FileCode) {
        int line_number = 0;

        BufferedReader brt = null;
        AccesoDB con = new AccesoDB();
        boolean commit = true;
        boolean reg = false;
        String strSQL = "";
        PreparedStatement ps = null;
        ResultSet rs = null;

        //Constantes

        final int DATAFILE_FIELD_DISCOUNTID = 3;

        String cveCargaTmp = "";
        //Fin COnstantes
        List hmReg = new ArrayList();
        try {

            con.conexionDB();
            strSQL = "SELECT NVL (MAX (IC_CARGA_TMP),0)+1 cvecarga  " + "  FROM DETALLE_ERROR_ANAPM ";
            ps = con.queryPrecompilado(strSQL);
            rs = ps.executeQuery();
            if (rs.next())
                cveCargaTmp = rs.getString("cvecarga");
            rs.close();
            ps.close();
            int cveTmp = Integer.parseInt(cveCargaTmp);

            strSQL =
                "insert into DETALLE_ERROR_ANAPM" +
                "  (IC_CARGA_TMP,IG_LINEA , IC_IF,IFL_FILE_CODE,IFL_COMM_ERROR   ) " + "  values(?, ?, ?, ?, ?)";
            String linea = "";
            java.io.File ft = new java.io.File(path + dataFile);
            brt = new BufferedReader(new InputStreamReader(new FileInputStream(ft)));

            while ((linea = brt.readLine()) != null) {
                line_number++;
                if (linea.length() > 0 && line_number > 1) {
                    String discount = getToken(linea, DATAFILE_FIELD_DISCOUNTID, "|");
                    if (!discount.equals("D") && !discount.equals("L") && !discount.equals("P")) {
                        log.debug("SQL.toString() " + strSQL.toString());
                        ps = con.queryPrecompilado(strSQL);
                        ps.setLong(1, cveTmp);
                        ps.setInt(2, line_number);
                        ps.setInt(3, Integer.parseInt(IF));
                        ps.setInt(4, Integer.parseInt(FileCode));
                        ps.setString(5,
                                     "Error en la Linea: " + line_number +
                                     " El valor del campo Tipo de Recursos no es correcto, debe ser L-Linea, P-Propia o D-Descontada");
                        ps.executeUpdate();
                        ps.close();
                        hmReg.add(Integer.toString(cveTmp));
                        cveTmp++;
                    }
                }
            }
            brt.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            commit = false;
            reg = false;

            throw new AppException("Error en", ex);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return hmReg;
    }

    public HashMap obtieneErrorDoc(String fileCode, String IF, String cadIc) throws AppException {
        log.debug("obtieneErrorDoc(E): ");
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lstErroneos = new ArrayList();
        HashMap hmDataCarga = new HashMap();
        String strSQL = "";
        try {
            con.conexionDB();
            strSQL =
                "select IG_LINEA ,IFL_COMM_ERROR as errores from DETALLE_ERROR_ANAPM " + " where IFL_FILE_CODE =  ?  " +
                " and IC_IF = ? " + //AND ors.ic_estatus_ope in(8,7)
                " AND IC_CARGA_TMP IN(" + cadIc + ")" + "ORDER BY IG_LINEA ";
            log.debug("query :: " + strSQL);
            ps = con.queryPrecompilado(strSQL);
            ps.setLong(1, Long.parseLong(fileCode));
            ps.setLong(2, Long.parseLong(IF));
            rs = ps.executeQuery();
            HashMap hmReg = new HashMap();
            while (rs.next()) {
                hmReg = new HashMap();
                hmReg.put("IG_LINEA", rs.getString("IG_LINEA") == null ? "" : rs.getString("IG_LINEA"));
                hmReg.put("ERROR", rs.getString("errores") == null ? "" : rs.getString("errores"));
                log.debug("--->> " + rs.getString("IG_LINEA"));
                log.debug("--->> " + rs.getString("errores"));
                lstErroneos.add(hmReg);
            }
            rs.close();
            ps.close();
            hmDataCarga.put("ERRONEOS", lstErroneos);

        } catch (Throwable t) {
            t.printStackTrace();
            throw new AppException("Error al obtener registros erroneos y correctos de la carga tmp de fondo liquido",
                                   t);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        log.debug("obtieneErrorDoc(S): ");
        return hmDataCarga;
    }

    public String autorizadorRegistrado(String ic_solicitud, String noUsuario) {
        log.info("autorizadorRegistrado (E)");
        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        String CG_USUARIO_AUTOR1 = "";
        String CG_USUARIO_AUTOR2 = "";
        String valor = "";
        try {
            con.conexionDB();
            SQL.append(" select CG_USUARIO_AUTOR1, CG_USUARIO_AUTOR2 from ope_reg_solicitud where IC_SOLICITUD = " +
                       ic_solicitud);
            log.debug("SQL.toString() " + SQL.toString());
            PreparedStatement ps = con.queryPrecompilado(SQL.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                CG_USUARIO_AUTOR1 = rs.getString(1) == null ? "" : rs.getString(1);
                CG_USUARIO_AUTOR2 = rs.getString(2) == null ? "" : rs.getString(2);
            }
            rs.close();
            ps.close();
            if (CG_USUARIO_AUTOR1.equals(noUsuario) || CG_USUARIO_AUTOR1.equals(noUsuario)) {
                valor = "S";
            } else {
                valor = "N";
            }
        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getConcentrador " + e);
            throw new AppException("Error al getConcentrador", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("autorizadorRegistrado (S)");
        }
        return valor;
    }

    public String guardaSolicitud1(ArrayList parametros, String ic_if, String strDirectorioTemp, String strPais,
                                   Long iNoNafinElectronico, String strNombre, String strNombreUsuario,
                                   String strDirectorioPublicacion, String lblFondoLiquido, String strPerfil) {
        log.info("guardaSolicitud1 (E)");
        AccesoDB con = new AccesoDB();
        boolean commit = true;
        String icSolicitud1 = "";
        List parametrosPDF = new ArrayList();
        try {
            con.conexionDB();
            icSolicitud1 = this.getguardarSolicitudCon(parametros, con); // Guarda Solicitud
            parametrosPDF.add(strDirectorioTemp);
            parametrosPDF.add(icSolicitud1);
            parametrosPDF.add(ic_if);
            parametrosPDF.add(strPais);
            parametrosPDF.add(iNoNafinElectronico);
            parametrosPDF.add(strNombre);
            parametrosPDF.add(strNombreUsuario);
            parametrosPDF.add("if.gif");
            parametrosPDF.add(strDirectorioPublicacion);
            parametrosPDF.add(lblFondoLiquido);
            parametrosPDF.add(strPerfil);
            con.terminaTransaccion(commit);
            boolean valor = this.guardarArchSolicCon(parametrosPDF, con); // Guarda el archivo pdf del Acuse

        } catch (Throwable e) {
            commit = false;
            icSolicitud1 = "";
            e.printStackTrace();
            log.error("Error guardaSolicitud1 " + e);
            throw new AppException("Error al guardaSolicitud1", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();

        }
        log.info("guardaSolicitud1 (S)");
        return icSolicitud1;
    }

    /**
     * Funci�n que recupera informaci�n del avance de la solicitud.
     * @return
     * @param cc_usuario
     * @param solicitud
     * @param tipoUsuario
     */
    public List consultaCheckList(String tipoUsuario, String solicitud, String cc_usuario) {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List lstSolicRec = new ArrayList();
        List varBind = new ArrayList();
        try {
            con.conexionDB();

            String strQry =
                " 		select  ocs.IC_SOLICITUD,ocs.CC_USUARIO, oce.IC_CHKLIST_ELEMENTO, oce.CC_TIPO_USUARIO,  ocs.CS_CORRECTO,ocs.cs_correcto as cs_correcto_ant, decode(ocs.cs_correcto,'N','S','S','S','','N' )AS cs_correcto_rev,decode(ocs.cs_correcto,'N','S','S','S','','N' )AS cs_correcto_rev_aux,  ocs.CG_OBSERVACIONES , ocs.cg_observaciones as cg_observaciones_ant,  oce.CG_DESCRIPCION " +
                "		from opetmp_chklist_solicitud ocs, opecat_chklist_elemento oce" +
                "		where ocs.IC_CHKLIST_ELEMENTO(+) = oce.IC_CHKLIST_ELEMENTO" +
                "		and ocs.CC_TIPO_USUARIO(+) = oce.CC_TIPO_USUARIO" + "		and oce.CC_TIPO_USUARIO = ? " +
                "		and ocs.IC_SOLICITUD(+) =? " + "		and ocs.CC_USUARIO(+) = ? " +
                "		ORDER BY  oce.IC_CHKLIST_ELEMENTO ";

            log.debug("strQry " + strQry);
            varBind.add(tipoUsuario);
            varBind.add(solicitud);
            varBind.add(cc_usuario);
            log.debug("varBind " + varBind);
            ps = con.queryPrecompilado(strQry, varBind);
            rs = ps.executeQuery();
            HashMap hmData;
            int lin = 1;
            while (rs.next()) {

                hmData = new HashMap();
                hmData.put("IC_SOLICITUD", rs.getString("IC_SOLICITUD"));
                hmData.put("CC_USUARIO", rs.getString("CC_USUARIO"));
                hmData.put("IC_CHKLIST_ELEMENTO", rs.getString("IC_CHKLIST_ELEMENTO"));
                hmData.put("CC_TIPO_USUARIO", rs.getString("CC_TIPO_USUARIO"));
                hmData.put("CS_CORRECTO", rs.getString("CS_CORRECTO"));
                hmData.put("CS_CORRECTO_ANT", rs.getString("CS_CORRECTO_ANT"));
                hmData.put("CS_CORRECTO_REV", rs.getString("CS_CORRECTO_REV"));
                hmData.put("CS_CORRECTO_REV_AUX", rs.getString("CS_CORRECTO_REV_AUX"));
                hmData.put("CG_OBSERVACIONES", rs.getString("CG_OBSERVACIONES"));
                hmData.put("CG_OBSERVACIONES_ANT", rs.getString("CG_OBSERVACIONES_ANT"));
                hmData.put("CG_DESCRIPCION", lin + ". " + rs.getString("CG_DESCRIPCION"));
                lstSolicRec.add(hmData);
                lin++;
            }
            rs.close();
            ps.close();

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return lstSolicRec;
    }

    /**
     * Funci�n para guardar informaci�n en la tabla temporal opetmp_chklist_solicitud de los puntos que ya se revisaron
     * @throws com.netro.exception.NafinException
     * @return
     * @param checkObservaciones
     * @param checkCorrecto
     * @param tipoUsuario
     * @param checkRevisado
     * @param iNoUsuario
     * @param cveSolicitud
     */
    public boolean guardaChkList(String cveSolicitud, String iNoUsuario, String checkRevisado, String tipoUsuario,
                                 String checkCorrecto, String checkObservaciones) throws NafinException {

        AccesoDB con = new AccesoDB();
        boolean exito = true;
        List lVarBind = new ArrayList();

        log.info("guardaChkList(E)");

        PreparedStatement ps = null;
        try {

            String existeregElemento = this.getcheckSoli(cveSolicitud, iNoUsuario, checkRevisado, tipoUsuario);
            con.conexionDB();
            if (!existeregElemento.equals("")) {
                String qry =
                    "	update opetmp_chklist_solicitud	" + "	set CS_CORRECTO = ?, CG_OBSERVACIONES =?" +
                    "	where IC_SOLICITUD = ? " + "	and CC_USUARIO = ? " + "	and IC_CHKLIST_ELEMENTO = ? " +
                    "	and CC_TIPO_USUARIO = ? ";
                lVarBind = new ArrayList();
                lVarBind.add(checkCorrecto);
                lVarBind.add(checkObservaciones);
                lVarBind.add(cveSolicitud);
                lVarBind.add(iNoUsuario);
                lVarBind.add(checkRevisado);
                lVarBind.add(tipoUsuario);
                ps = con.queryPrecompilado(qry, lVarBind);
                ps.executeUpdate();
                ps.close();
                log.debug(" qry ::    " + qry);
                log.debug(" lVarBind ::    " + lVarBind);
            } else {

                String qry =
                    "INSERT INTO opetmp_chklist_solicitud (IC_SOLICITUD, CC_USUARIO, IC_CHKLIST_ELEMENTO, CC_TIPO_USUARIO,CS_CORRECTO ,	CG_OBSERVACIONES) " +
                    "VALUES (?,?,?,?,?,? )  ";

                lVarBind = new ArrayList();
                lVarBind.add(cveSolicitud);
                lVarBind.add(iNoUsuario);
                lVarBind.add(checkRevisado);
                lVarBind.add(tipoUsuario);
                lVarBind.add(checkCorrecto);
                lVarBind.add(checkObservaciones);
                ps = con.queryPrecompilado(qry, lVarBind);
                ps.executeUpdate();
                ps.close();
                log.debug(" qry ::    " + qry);
                log.debug(" lVarBind ::    " + lVarBind);
            }

        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("guardaChkList (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

        }
        return exito;
    }

    /**
     * Obtiene
     * @return
     * @param tipoUsuario
     * @param checkRevisado
     * @param iNoUsuario
     * @param cveSolicitud
     */
    public String getcheckSoli(String cveSolicitud, String iNoUsuario, String checkRevisado, String tipoUsuario) {
        log.info("getcheckSoli (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        String existeSoli = "";
        List lVarBind = new ArrayList();
        try {

            con.conexionDB();
            SQL.append(" 	select IC_SOLICITUD	" + "	from opetmp_chklist_solicitud 	" + "	where IC_SOLICITUD = ?	 " +
                       "	and CC_USUARIO = ?	" + "	and IC_CHKLIST_ELEMENTO = ?	" +
                       "	and CC_TIPO_USUARIO = ?	");


            lVarBind = new ArrayList();
            lVarBind.add(cveSolicitud);
            lVarBind.add(iNoUsuario);
            lVarBind.add(checkRevisado);
            lVarBind.add(tipoUsuario);
            PreparedStatement ps = con.queryPrecompilado(SQL.toString(), lVarBind);
            ResultSet rs = ps.executeQuery();
            log.debug(" qry ::    " + SQL.toString());
            log.debug(" lVarBind ::    " + lVarBind);
            if (rs.next()) {
                existeSoli = rs.getString("IC_SOLICITUD") == null ? "" : rs.getString("IC_SOLICITUD");
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getcheckSoli " + e);
            throw new AppException("Error al getcheckSoli", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getcheckSoli (S)");
        }
        return existeSoli;
    }

    /**
     * Funci�n que obtiene la informaci�n necesar�a, para realizar la validaci�n del check list
     * @return
     * @param tipoUsuario
     * @param iNoUsuario
     * @param cveSolicitud
     */
    public HashMap validaListCheck(String cveSolicitud, String iNoUsuario, String tipoUsuario) {
        log.info("validaListCheck (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        String existeSoli = "";
        List lVarBind = new ArrayList();
        String total = "";
        HashMap hmData = new HashMap();
        try {

            con.conexionDB();
            SQL.append(" 	select  count(IC_CHKLIST_ELEMENTO) as TOTAL_ELE,SUM(DECODE(CS_CORRECTO, 'S',0,'N',1)) as TOTAL_INCORRECTO, SUM(DECODE(CG_OBSERVACIONES, null,0,1)) as TOTAL_OBSERVACIONES   " +
                       " 	FROM opetmp_chklist_solicitud " + " 	where IC_SOLICITUD = ? " + " 	and  CC_USUARIO = ? " +
                       " 	and CC_TIPO_USUARIO = ? ");


            lVarBind = new ArrayList();
            lVarBind.add(cveSolicitud);
            lVarBind.add(iNoUsuario);
            lVarBind.add(tipoUsuario);
            PreparedStatement ps = con.queryPrecompilado(SQL.toString(), lVarBind);
            ResultSet rs = ps.executeQuery();
            log.debug(" qry ::    " + SQL.toString());
            log.debug(" lVarBind ::    " + lVarBind);
            if (rs.next()) {
                hmData = new HashMap();
                hmData.put("TOTAL_ELE", rs.getString("TOTAL_ELE") == null ? "" : rs.getString("TOTAL_ELE"));
                hmData.put("TOTAL_INCORRECTO",
                           rs.getString("TOTAL_INCORRECTO") == null ? "" : rs.getString("TOTAL_INCORRECTO"));
                hmData.put("TOTAL_OBSERVACIONES",
                           rs.getString("TOTAL_OBSERVACIONES") == null ? "" : rs.getString("TOTAL_OBSERVACIONES"));
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error validaListCheck " + e);
            throw new AppException("Error al validaListCheck", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("validaListCheck (S)");
        }
        return hmData;
    }

    /**
     * Elimina un registro de la tabla opetmp_chklist_solicitud
     * @param tipoUsuario
     * @param checkRevisado
     * @param iNoUsuario
     * @param cveSolicitud
     */
    public void limpiarCheck(String cveSolicitud, String iNoUsuario, String checkRevisado, String tipoUsuario) {

        log.info("limpiarCheck (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;


        try {

            con.conexionDB();

            //Elimina los registros cargados  en lineas revolventes
            strSQL = new StringBuffer();
            lVarBind = new ArrayList();
            strSQL.append(" delete opetmp_chklist_solicitud " + " where IC_SOLICITUD = ? " + "	and CC_USUARIO = ? " +
                          "	and IC_CHKLIST_ELEMENTO = ? " + "	and CC_TIPO_USUARIO = ? ");


            lVarBind = new ArrayList();
            lVarBind.add(cveSolicitud);
            lVarBind.add(iNoUsuario);
            lVarBind.add(checkRevisado);
            lVarBind.add(tipoUsuario);
            log.debug("strSQL " + strSQL);
            log.debug("lVarBind " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();

            // Actualiza los registros globales por Prestamo y fecha


        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("cancelaCargaEmpresas  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  cancelaCargaEmpresas (S) ");
            }
        }
    }

    /**
     * Funci�n que obtiene el acuse de la solicitud
     * @return
     * @param ic_solicitudes
     */
    public String getAcuseCheck(String ic_solicitudes) {

        log.info("getAcuseCheck (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String listAcuse = "";
        String listAcuseAux = "";


        try {

            con.conexionDB();

            //Elimina los registros cargados  en lineas revolventes
            strSQL = new StringBuffer();
            lVarBind = new ArrayList();
            strSQL.append(" select cc_acuse " + " from ope_reg_solicitud " + "	where IC_SOLICITUD in(" +
                          ic_solicitudes + ") ");


            log.debug("strSQL " + strSQL);
            ps = con.queryPrecompilado(strSQL.toString());
            rs = ps.executeQuery();
            while (rs.next()) {
                listAcuse += rs.getString("CC_ACUSE") == null ? "" : rs.getString("CC_ACUSE") + ", ";
            }
            rs.close();
            ps.close();
            listAcuseAux = listAcuse.substring(0, (listAcuse.length() - 2));


        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("getAcuseCheck  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  getAcuseCheck (S) ");
            }
        }
        return listAcuseAux;
    }

    public String obtenChkSoli(String tipo_soli, String ic_solicitud, String cc_usuario) {

        log.info("obtenChkSoli (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String listDatos = "";


        try {

            con.conexionDB();

            strSQL = new StringBuffer();
            lVarBind = new ArrayList();
            strSQL.append(" SELECT ocs.ic_chklist_elemento, oce.cg_descripcion, ocs.cs_correcto, ocs.cg_observaciones " +
                          " FROM opetmp_chklist_solicitud ocs, opecat_chklist_elemento oce " +
                          "	WHERE ocs.ic_chklist_elemento(+) = oce.ic_chklist_elemento " +
                          "	AND ocs.cc_tipo_usuario = oce.cc_tipo_usuario " + "	AND oce.cc_tipo_usuario = ? " +
                          "	AND ocs.ic_solicitud = ?  " + "	AND ocs.cc_usuario = ?  " +
                          "	order by IC_CHKLIST_ELEMENTO ");


            lVarBind = new ArrayList();
            lVarBind.add(tipo_soli);
            lVarBind.add(ic_solicitud);
            lVarBind.add(cc_usuario);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            rs = ps.executeQuery();
            log.debug(" qry ::    " + strSQL.toString());
            log.debug(" lVarBind ::    " + lVarBind);
            while (rs.next()) {
                String eleCheck =
                    rs.getString("IC_CHKLIST_ELEMENTO") == null ? "" : rs.getString("IC_CHKLIST_ELEMENTO");
                String descrip = rs.getString("CG_DESCRIPCION") == null ? "" : rs.getString("CG_DESCRIPCION");
                String correcto = rs.getString("CS_CORRECTO") == null ? "" : rs.getString("CS_CORRECTO");
                String obser = rs.getString("CG_OBSERVACIONES") == null ? "" : rs.getString("CG_OBSERVACIONES");
                String reng = eleCheck + "|" + descrip + "|" + correcto + "|" + obser + ",";
                listDatos += reng;
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("obtenChkSoli  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  obtenChkSoli (S) ");
            }
        }
        return listDatos;
    }

    /**
     * Motodo que me dice que documentos ya tienen almenos una revisi�n.
     * @throws com.netro.exception.NafinException
     * @return
     */
    public String getExisteRevision(String ic_solicitud) {
        AccesoDB con = new AccesoDB();
        String existe = "";
        boolean commit = true;
        List lVarBind = new ArrayList();
        log.info("getExisteRevision(E)");
        try {

            con.conexionDB();
            String query =
                "	select count(*) as existe " + "	from  opebit_chklist_solicitud	" +
                "	where IC_SOLICITUD = ?	";

            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            log.debug(" qry ::    " + query);
            log.debug(" lVarBind ::    " + lVarBind);
            PreparedStatement ps = con.queryPrecompilado(query, lVarBind);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                existe = rs.getString("EXISTE") == null ? "" : rs.getString("EXISTE");
            }

            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error getExisteRevision " + e);
            throw new AppException("Error al getExisteRevision", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("getExisteRevision (S)");
        }
        return existe;
    }

    /**
     * Funci�n que guarda informacion en la tabla opebit_chklist_solicitud
     * @param check
     * @param cc_usuario
     * @param estatus
     * @param ic_solicitud
     */
    public void guardaBitacoraCheck(String ic_solicitud, String estatus, String cc_usuario, String check,
                                    String cc_usuario_con, AccesoDB con, String tipo_soli) throws NafinException {
        boolean exito = true;
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;

        log.info("guardaBitacoraCheck(E)");
        String existeRechazo = "";
        String qry = "";
        StringBuffer strSQL = new StringBuffer();

        try {
            existeRechazo = this.existeRechazoInterno(ic_solicitud, cc_usuario);
            if (!cc_usuario_con.equals("")) {
                qry =
                    "INSERT INTO opebit_chklist_solicitud(IC_CHKLIST_SOLICITUD,IC_SOLICITUD,IC_ESTATUS_OPE,CC_USUARIO,DF_ALTA,CG_CHECKLIST,CG_RECHAZO_INT,CC_USUARIO_CON) " +
                    "VALUES ( seq_bit_chklist_solicitud.NEXTVAL,?,?,?,sysdate,? ,?,? )  ";
                lVarBind = new ArrayList();
                lVarBind.add(ic_solicitud);
                lVarBind.add(estatus);
                lVarBind.add(cc_usuario);
                lVarBind.add(check);
                if (!existeRechazo.equals("no") && estatus.equals("2")) {
                    lVarBind.add("S");
                } else {
                    lVarBind.add("N");
                }
                lVarBind.add(cc_usuario_con);
            } else {
                qry =
                    "INSERT INTO opebit_chklist_solicitud(IC_CHKLIST_SOLICITUD,IC_SOLICITUD,IC_ESTATUS_OPE,CC_USUARIO,DF_ALTA,CG_CHECKLIST,CG_RECHAZO_INT) " +
                    "VALUES ( seq_bit_chklist_solicitud.NEXTVAL,?,?,?,sysdate,? ,? )  ";
                lVarBind = new ArrayList();
                lVarBind.add(ic_solicitud);
                lVarBind.add(estatus);
                lVarBind.add(cc_usuario);
                lVarBind.add(check);
                if (!existeRechazo.equals("no") && estatus.equals("2")) {
                    lVarBind.add("S");
                } else {
                    lVarBind.add("N");
                }
            }


            ps = con.queryPrecompilado(qry, lVarBind);
            ps.executeUpdate();
            ps.close();
            log.debug("  ::qry::   " + qry);

            strSQL = new StringBuffer();
            lVarBind = new ArrayList();
            strSQL.append(" DELETE   FROM opetmp_chklist_solicitud  WHERE IC_SOLICITUD  = ?  AND  CC_USUARIO = ? AND CC_TIPO_USUARIO = ?  ");
            lVarBind = new ArrayList();
            lVarBind.add(ic_solicitud);
            if (!cc_usuario_con.equals("")) {
                lVarBind.add(cc_usuario_con);
            } else {
                lVarBind.add(cc_usuario);
            }
            lVarBind.add(tipo_soli);

            log.debug(" qry ::    " + strSQL.toString());
            log.debug(" lVarBind ::    " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            ps.executeUpdate();
            ps.close();


        } catch (Exception e) {
            System.out.println(" OperacionElectronicaEJB::guardaBitacoraCheck(Exception): " + e);
            throw new NafinException("SIST0001");
        } finally {
            System.out.println("guardaBitacoraCheck (S)");

        }

    }

    /**
     * Funci�n que obtiene la informaci�n del campo donde se guardo el check list.
     * @return
     * @param IC_CHKLIST_SOLICITUD
     * @param path
     */
    public String obtenChkBitacora(String path, String IC_CHKLIST_SOLICITUD) {

        log.info("obtenChkBitacora (E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        StringBuffer strSQL = new StringBuffer();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String checkList = "";

        String nombreArchivo = "";
        CreaArchivo creaArchivo = new CreaArchivo();
        StringBuffer contenidoArchivo = new StringBuffer();
        OutputStreamWriter writer = null;
        BufferedWriter buffer = null;
        int total = 0;

        try {

            con.conexionDB();
            nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
            writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
            buffer = new BufferedWriter(writer);

            contenidoArchivo.append("Puntos revisados, �Es correcto? , Observaciones \n");

            strSQL = new StringBuffer();
            lVarBind = new ArrayList();
            strSQL.append(" select CG_CHECKLIST " + " 	from opebit_chklist_solicitud " +
                          "	where  IC_CHKLIST_SOLICITUD = ? ");


            lVarBind = new ArrayList();
            lVarBind.add(IC_CHKLIST_SOLICITUD);
            log.debug(" qry ::    " + strSQL.toString());
            log.debug(" lVarBind ::    " + lVarBind);
            ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
            rs = ps.executeQuery();

            if (rs.next()) {
                checkList = rs.getString("CG_CHECKLIST") == null ? "" : rs.getString("CG_CHECKLIST");
            }
            rs.close();
            ps.close();
            VectorTokenizer vt = new VectorTokenizer(checkList, ",");
            Vector registrosArchivo = vt.getValuesVector();
            int h = 1;
            if (registrosArchivo.size() > 0) {
                for (int i = 0; i < registrosArchivo.size(); i++) {
                    System.out.println("registrosArchivo " + (String) registrosArchivo.get(i));
                    VectorTokenizer vtCol = new VectorTokenizer((String) registrosArchivo.get(i), "|");
                    Vector colArchivo = vtCol.getValuesVector();
                    if (colArchivo.size() > 0) {
                        String correcto = (String) colArchivo.get(2);
                        if (correcto.equals("S")) {
                            contenidoArchivo.append(h + "." + (String) colArchivo.get(1) + "," + "SI" + "," +
                                                    (String) colArchivo.get(3) + "\n");

                        } else {
                            contenidoArchivo.append(h + "." + (String) colArchivo.get(1) + "," + "NO" + "," +
                                                    (String) colArchivo.get(3) + "\n");
                        }
                        h++;
                        total++;
                        if (total == 1000) {
                            total = 0;
                            buffer.write(contenidoArchivo.toString());
                            contenidoArchivo = new StringBuffer(); //Limpio
                        }
                    }
                }
            }
            buffer.write(contenidoArchivo.toString());
            buffer.close();
            contenidoArchivo = new StringBuffer();

        } catch (Throwable e) {
            exito = false;
            e.printStackTrace();
            log.error("obtenChkBitacora  " + e);
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
                log.info("  obtenChkBitacora (S) ");
            }
        }
        return nombreArchivo;
    }

    /**
     * Recupera informaci�n de la tabla opebit_chklist_solicitud
     * @return
     * @param cveSolicitud
     */
    public List consultarBitacoraRevision(String cveSolicitud) {
        log.info("consultarBitacoraRevision (E)");

        AccesoDB con = new AccesoDB();
        boolean commit = true;
        StringBuffer SQL = new StringBuffer();
        String existeSoli = "";
        List lVarBind = new ArrayList();
        String total = "";
        HashMap hmData = new HashMap();
        UtilUsr utilUsr = new UtilUsr();
        List lstBitacora = new ArrayList();
        try {

            con.conexionDB();
            SQL.append(" 	SELECT ocs.IC_CHKLIST_SOLICITUD, ocs.IC_SOLICITUD, ocs.IC_ESTATUS_OPE, oe.CD_DESCRIPCION, ocs.CC_USUARIO,TO_CHAR (ocs.DF_ALTA, 'dd/mm/yyyy HH24:mi:ss')  as DF_ALTA,CG_RECHAZO_INT , CC_USUARIO_CON       " +
                       " 	FROM opebit_chklist_solicitud ocs ,opecat_estatus  oe " +
                       " WHERE ocs.IC_ESTATUS_OPE = oe.IC_ESTATUS_OPE " + " 	AND IC_SOLICITUD = ? " +
                       "	ORDER BY IC_CHKLIST_SOLICITUD");


            lVarBind = new ArrayList();
            lVarBind.add(cveSolicitud);
            PreparedStatement ps = con.queryPrecompilado(SQL.toString(), lVarBind);
            ResultSet rs = ps.executeQuery();
            System.out.println(" qry ::    " + SQL.toString());
            System.out.println(" lVarBind ::    " + lVarBind);
            while (rs.next()) {

                hmData = new HashMap();
                hmData.put("IC_CHKLIST_SOLICITUD",
                           rs.getString("IC_CHKLIST_SOLICITUD") == null ? "" : rs.getString("IC_CHKLIST_SOLICITUD"));
                hmData.put("IC_SOLICITUD", rs.getString("IC_SOLICITUD") == null ? "" : rs.getString("IC_SOLICITUD"));
                hmData.put("IC_ESTATUS_OPE",
                           rs.getString("IC_ESTATUS_OPE") == null ? "" : rs.getString("IC_ESTATUS_OPE"));
                String rechazo = rs.getString("CG_RECHAZO_INT") == null ? "" : rs.getString("CG_RECHAZO_INT");
                String usuario_con = rs.getString("CC_USUARIO_CON") == null ? "" : rs.getString("CC_USUARIO_CON");
                String iNoUsuario = "";
                if (rechazo.equals("S")) {
                    hmData.put("CD_DESCRIPCION",
                               rs.getString("CD_DESCRIPCION") == null ? "" :
                               rs.getString("CD_DESCRIPCION") + "(rechazo interno)");
                } else {
                    hmData.put("CD_DESCRIPCION",
                               rs.getString("CD_DESCRIPCION") == null ? "" : rs.getString("CD_DESCRIPCION"));
                }

                hmData.put("DF_ALTA", rs.getString("DF_ALTA") == null ? "" : rs.getString("DF_ALTA"));
                if (usuario_con.equals("")) {
                    iNoUsuario = rs.getString("CC_USUARIO") == null ? "" : rs.getString("CC_USUARIO");
                } else {
                    iNoUsuario = usuario_con;
                }

                Usuario usuarioValidoDoc = utilUsr.getUsuario(iNoUsuario);
                String nombreUsuarios = (String) (obtieneInfoCorreo(iNoUsuario)).get("USUARIO_NOMBRE");
                hmData.put("CC_USUARIO_NOM", nombreUsuarios);
                lstBitacora.add(hmData);
            }
            rs.close();
            ps.close();


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error consultarBitacoraRevision " + e);
            throw new AppException("Error al consultarBitacoraRevision", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("consultarBitacoraRevision (S)");
        }
        return lstBitacora;
    }

    /**
     * Metodo que nos dice si existe rechazo interno
     * @throws com.netro.exception.NafinException
     * @return
     */
    public String existeRechazoInterno(String ic_solicitud, String ejecutivoOpe) {
        AccesoDB con = new AccesoDB();
        String existe_bit = "";
        String existe_ope = "";
        String existeRechazo = "";
        String query = "";
        boolean commit = true;
        List lVarBind = new ArrayList();
        log.info("existeRechazoInterno(E)");
        try {

            con.conexionDB();
            query =
                "	select count(*) as existe_bit " + "	from opebit_chklist_solicitud	" + "	where IC_SOLICITUD = ?	" +
                "	and IC_ESTATUS_OPE = ?	" + "	and CC_USUARIO =  ? ";

            lVarBind = new ArrayList();

            lVarBind.add(ic_solicitud);
            lVarBind.add("2");
            lVarBind.add(ejecutivoOpe);
            System.out.println(" BIT qry ::    " + query);
            System.out.println(" BIT lVarBind ::    " + lVarBind);
            PreparedStatement ps = con.queryPrecompilado(query, lVarBind);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                existe_bit = rs.getString("EXISTE_BIT") == null ? "" : rs.getString("EXISTE_BIT");
            }
            rs.close();
            ps.close();
            if (existe_bit.equals("0")) {
                query = "";
                query =
                    "	select count(*) as existe_ope	" + "	from ope_reg_solicitud	" + "	where IC_SOLICITUD = ?	" +
                    "	AND IC_EJECUTIVO_OP = ?	";

                lVarBind = new ArrayList();
                lVarBind.add(ic_solicitud);
                lVarBind.add(ejecutivoOpe);
                System.out.println(" OPE qry ::    " + query);
                System.out.println(" OPE lVarBind ::    " + lVarBind);
                PreparedStatement ps1 = con.queryPrecompilado(query, lVarBind);
                ResultSet rs1 = ps1.executeQuery();
                if (rs1.next()) {
                    existe_ope = rs1.getString("EXISTE_OPE") == null ? "" : rs1.getString("EXISTE_OPE");
                }
                rs1.close();
                ps1.close();
                if (existe_ope.equals("0")) {
                    existeRechazo = "no";
                } else {
                    existeRechazo = "si";
                }

            } else {
                existeRechazo = "si";
            }


        } catch (Throwable e) {
            commit = false;
            e.printStackTrace();
            log.error("Error existeRechazoInterno " + e);
            throw new AppException("Error al existeRechazoInterno", e);
        } finally {
            con.terminaTransaccion(commit);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("existeRechazoInterno (S)" + existeRechazo);
        }
        return existeRechazo;
    }

    public List cargaContratos(String clave_if, int filas, int inicio) {
        AccesoDB con = new AccesoDB();
        List informacion = new ArrayList();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con.conexionDB();

            String strQry =
                " select IC_CONTRATO, CG_CONTRATO_OE, to_char(DF_FECHA_FIRMA,'dd/mm/yyyy') DF_FECHA_FIRMA," +
                "        to_char(DF_FIRMA_OE,'dd/mm/yyyy') DF_FIRMA_OE  from ope_contrato_if" +
                "        where ic_if=?  and rownum =1 order by ic_contrato desc ";

            lVarBind = new ArrayList();
            lVarBind.add(new Long(clave_if));
            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap registros = new HashMap();
                registros.put("NUM_CONT", rs.getString("IC_CONTRATO"));
                registros.put("NOM_CONT", rs.getString("CG_CONTRATO_OE"));
                registros.put("FECHA_FIR_CONT", rs.getString("DF_FECHA_FIRMA"));
                registros.put("FECHA_FIR_OPE_ELE", rs.getString("DF_FIRMA_OE"));
                informacion.add(registros);
            }
            rs.close();
            ps.close();
            Date hoy = new Date();
            SimpleDateFormat sdfDia = new SimpleDateFormat("dd/MM/YYY");
            for(int i=1; i<=filas; i++){
                HashMap registros1 = new HashMap();
                registros1.put("NUM_CONT", "");
                registros1.put("NOM_CONT", "Nuevo Contrato");
                registros1.put("FECHA_FIR_CONT", sdfDia.format(hoy));
                registros1.put("FECHA_FIR_OPE_ELE", sdfDia.format(hoy));
                informacion.add(registros1);
            }

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return informacion;
    }

    public String guardaContratoIF(String ic_contrato, String ic_if, String cg_contrato_oe, String df_fecha_firma,
                                   String df_firma_oe) throws NafinException {
        log.info("OperacionElectronicaBean.guardaContratoIF(E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int indx = 1;
        Integer secuencia = null;
        String tmpReturn = null;
        try {
            con.conexionDB();

            Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(df_fecha_firma);
            Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(df_firma_oe);

            if (ic_contrato == null || ic_contrato.equals("")) { //inserta contrato

                String sqlMaxContrato = " select SEQ_OPE_CONTRATO_IF.nextval secuencia from dual ";
                ps = con.queryPrecompilado(sqlMaxContrato);
                rs = ps.executeQuery();
                while (rs.next()) {
                    secuencia = rs.getInt("secuencia");
                }

                String sqlInsContrato =
                    " insert into ope_contrato_if (ic_contrato,ic_if,cg_contrato_oe," +
                    " df_fecha_firma,df_firma_oe)  values " + " (?,?,?,?,?) ";
                ps = con.queryPrecompilado(sqlInsContrato);
                indx = 1;
                ps.setInt(indx++, secuencia);
                ps.setInt(indx++, Integer.parseInt(ic_if));
                ps.setString(indx++, cg_contrato_oe);
                ps.setDate(indx++, new java.sql.Date(date1.getTime()));
                ps.setDate(indx++, new java.sql.Date(date2.getTime()));
                ps.executeUpdate();

                rs.close();
                ps.close();
                tmpReturn = String.valueOf(secuencia);
            } else { //actualiza contrato
                String sqlMaxContrato = " select max(ic_contrato) ic_contrato from ope_contrato_if where ic_if= ? ";
                ps = con.queryPrecompilado(sqlMaxContrato);
                indx = 1;
                ps.setInt(indx++, Integer.parseInt(ic_if));
                rs = ps.executeQuery();
                Integer maxContrato = null;
                while (rs.next()) {
                    maxContrato = rs.getInt("ic_contrato");
                }

                String sqlUpdContrato =
                    " update OPE_CONTRATO_IF SET IC_CONTRATO = ? ,IC_IF = ? ,CG_CONTRATO_OE = ? ,DF_FECHA_FIRMA = ? ,DF_FIRMA_OE = ? " +
                    " where IC_CONTRATO= ? ";
                ps = con.queryPrecompilado(sqlUpdContrato);
                indx = 1;
                ps.setInt(indx++, maxContrato);
                ps.setInt(indx++, Integer.parseInt(ic_if));
                ps.setString(indx++, cg_contrato_oe);
                ps.setDate(indx++, new java.sql.Date(date1.getTime()));
                ps.setDate(indx++, new java.sql.Date(date2.getTime()));
                ps.setInt(indx++, maxContrato);
                ps.executeUpdate();
                rs.close();
                ps.close();
                tmpReturn = String.valueOf(maxContrato);

            }
        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("OperacionElectronicaBean.guardaContratoIF (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

        }
        log.info("OperacionElectronicaBean.guardaContratoIF(S)");
        return tmpReturn;
    }

    public boolean guardaModificatorioIF(String ic_contrato, String cg_contrato_oe, String df_fecha_firma,
                                         String df_firma_oe) throws NafinException {
        log.info("OperacionElectronicaBean.guardaModificatorioIF(E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int indx = 1;
        try {
            con.conexionDB();

            Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(df_fecha_firma);
            Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(df_firma_oe);

            String sqlInsModificatorio =
                " insert into ope_modificatorio_if (ic_modificatorio,ic_contrato,cg_contrato_oe,df_fecha_firma,df_firma_oe) " +
                " values (seq_ope_modificatorio_if.nextval,?,?,?,?) ";
            ps = con.queryPrecompilado(sqlInsModificatorio);
            indx = 1;
            ps.setInt(indx++, Integer.parseInt(ic_contrato));
            ps.setString(indx++, cg_contrato_oe);
            ps.setDate(indx++, new java.sql.Date(date1.getTime()));
            ps.setDate(indx++, new java.sql.Date(date2.getTime()));
            ps.executeUpdate();
            ps.close();


        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("OperacionElectronicaBean.guardaContratoIF (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

        }
        log.info("OperacionElectronicaBean.guardaModificatorioIF(S)");
        return exito;
    }
    
    public boolean actualizaModificatorioIF(String ic_contrato, String ic_modificatorio, String cg_contrato_oe, String df_fecha_firma,
                                         String df_firma_oe) throws NafinException {
        log.info("OperacionElectronicaBean.actualizaModificatorioIF(E)");
        AccesoDB con = new AccesoDB();
        boolean exito = false;
        PreparedStatement ps = null;
        try {
            con.conexionDB();
            String sqlUpdateModificatorio =
                " UPDATE ope_modificatorio_if set cg_contrato_oe= ?, df_fecha_firma = TO_DATE(?,'dd/mm/yyyy'), df_firma_oe = TO_DATE(?,'dd/mm/yyyy') " +
                " WHERE ic_contrato= ?  AND ic_modificatorio= ?" ;
            ps = con.queryPrecompilado(sqlUpdateModificatorio);
            ps.setString(1, cg_contrato_oe.trim());
            ps.setString(2, df_fecha_firma);
            ps.setString(3, df_firma_oe);
            ps.setInt(4, Integer.parseInt(ic_contrato));
            ps.setInt(5,  Integer.parseInt(ic_modificatorio));
            ps.executeUpdate();
            ps.close();
            exito = true;
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }
        }
        log.info("OperacionElectronicaBean.actualizaModificatorioIF(S)");
        return exito;
    }    

    public List getContrato(String clave_if) {
        AccesoDB con = new AccesoDB();
        List informacion = new ArrayList();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con.conexionDB();

            String strTotModif =" select count(ope_modificatorio_if.ic_modificatorio) total, ope_contrato_if.ic_contrato ic_contrato" +
            " from ope_contrato_if, ope_modificatorio_if " + 
            " where ope_contrato_if.ic_contrato = ope_modificatorio_if.ic_contrato " + 
            " and ope_contrato_if.ic_if= ? " + 
            " and ope_modificatorio_if.ic_contrato=(SELECT MAX(ic_contrato) FROM ope_contrato_if WHERE ic_if = ?) " + 
            " GROUP by ope_contrato_if.ic_contrato ";

            lVarBind = new ArrayList();
            lVarBind.add(new Integer(clave_if));
            lVarBind.add(new Integer(clave_if));
            ps = con.queryPrecompilado(strTotModif, lVarBind);
            rs = ps.executeQuery();
            String totalModifi="0";
            String numContrato="";
            while (rs.next()) {
                totalModifi=rs.getString("TOTAL");
                numContrato=rs.getString("IC_CONTRATO");
            }


            String strQry =
                " SELECT " + "    ic_contrato, " + "    ic_if, " + "    cg_contrato_oe, " +
                "    to_char(df_fecha_firma,'dd/mm/yyyy') df_fecha_firma, " +
                "    to_char(df_firma_oe,'dd/mm/yyyy') df_firma_oe " + "FROM " + "    ope_contrato_if " + "WHERE " +
                "    ic_contrato = (SELECT MAX(ic_contrato) FROM ope_contrato_if WHERE ic_if = ? ) ";

            lVarBind = new ArrayList();
            lVarBind.add(new Integer(clave_if));
            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap registros = new HashMap();
                registros.put("NUM_CONT", rs.getString("IC_CONTRATO"));
                registros.put("NOM_CONT", rs.getString("CG_CONTRATO_OE"));
                registros.put("FECHA_FIR_CONT", rs.getString("DF_FECHA_FIRMA"));
                registros.put("FECHA_FIR_OPE_ELE", rs.getString("DF_FIRMA_OE"));
                registros.put("TOT_MODIFI", totalModifi);
                registros.put("NUM_CONTRATO", numContrato);
                informacion.add(registros);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return informacion;
    }

    public boolean borraModificatorios(String ic_contrato) {

        log.info("OperacionElectronicaBean.borraModificatorios(E)");
        AccesoDB con = new AccesoDB();
        boolean exito = true;
        PreparedStatement ps = null;
        int indx = 1;
        try {
            con.conexionDB();

            String sqlDelModif = " delete from ope_modificatorio_if where ic_contrato= ? ";
            ps = con.queryPrecompilado(sqlDelModif);
            ps.setInt(indx++, Integer.parseInt(ic_contrato));
            ps.executeUpdate();

            ps.close();


        } catch (Exception e) {
            exito = false;
            System.out.println("\nError\n" + e);
            e.printStackTrace();
        } finally {
            System.out.println("OperacionElectronicaBean.borraModificatorios (S)");
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(exito);
                con.cierraConexionDB();
            }

        }
        log.info("OperacionElectronicaBean.borraModificatorios(S)");
        return exito;


    }

    public List cargaModModificatorios(String clave_if, int filas, int inicio) {
        AccesoDB con = new AccesoDB();
        List informacion = new ArrayList();
        List lVarBind = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            con.conexionDB();

            String strQry =" select ope_modificatorio_if.ic_modificatorio IC_MODIFICATORIO, " + 
            " ope_modificatorio_if.ic_contrato IC_CONTRATO, " + 
            " ope_modificatorio_if.cg_contrato_oe CG_CONTRATO_OE, " + 
            " TO_CHAR(ope_modificatorio_if.df_fecha_firma, 'dd/mm/yyyy') DF_FECHA_FIRMA, " + 
            " TO_CHAR(ope_modificatorio_if.df_firma_oe, 'dd/mm/yyyy') DF_FIRMA_OE, " + 
            " 'S' as EDO " + 
            " from ope_contrato_if,ope_modificatorio_if where " + 
            " ope_contrato_if.ic_contrato= ope_modificatorio_if.ic_contrato " + 
            " and ope_contrato_if.ic_if= ? " + 
            " and ope_modificatorio_if.ic_contrato= " + 
            "    (select max(ope_contrato_if.ic_contrato) from ope_contrato_if where ic_if= ? ) " + 
             "order by ope_modificatorio_if.ic_modificatorio ";

            lVarBind = new ArrayList();
            lVarBind.add(new Integer(clave_if));
            lVarBind.add(new Integer(clave_if));
            ps = con.queryPrecompilado(strQry, lVarBind);
            rs = ps.executeQuery();
            while (rs.next()) {
                HashMap registros = new HashMap();
                registros.put("DESCRIPCION", rs.getString("CG_CONTRATO_OE"));
                registros.put("FECHAMOD", rs.getString("DF_FIRMA_OE"));
                registros.put("EDO", rs.getString("EDO"));
                registros.put("IC_MOD", rs.getString("IC_MODIFICATORIO"));
                informacion.add(registros);
            }
            rs.close();
            ps.close();
            Date hoy = new Date();
            SimpleDateFormat sdfDia = new SimpleDateFormat("dd/MM/YYY");
            //for (int i = 1; i <= filas; i++) {
            if(filas > 0){
                HashMap registros1 = new HashMap();
                registros1.put("FECHAMOD", sdfDia.format(hoy));
                registros1.put("DESCRIPCION", "Nombre de Modificatorio");
                registros1.put("EDO", "N");
                registros1.put("IC_MOD", "0");
                informacion.add(registros1);
            }

        } catch (Exception e) {
            e.printStackTrace();
            con.terminaTransaccion(false);
            throw new RuntimeException(e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        return informacion;
    }
}
