package com.netro.electronica;

import com.netro.exception.NafinException;

import java.util.List;

import netropology.utilerias.AppException;

import java.util.*;

import java.math.*;

import netropology.utilerias.*;

import com.netro.exception.*;

import javax.ejb.Remote;

@Remote
public interface OperacionElectronica {


    //*************************** CASO DE USO: CU09 - FONDO LIQUIDO **************************************
    public String cargaDatosTmpFondoLiquido(String rutaArchivo) throws AppException;

    public void validaCargaTmpFondoLiquido(String cveCargaTmp, String usuario) throws AppException;

    public HashMap obtieneCargaTmpFondoLiquido(String cveCargaTmp) throws AppException;

    public void enviaCargaFondoLiquido(String cveCargaTmp, String usuario, String nombreUser) throws AppException;

    public String obtieneArchivoErroresFL(String rutaDestino, String cveCargaTmp) throws AppException;

    //*************************** Caso de Uso: CU02  *******************************************
    public void asignaEjecutivoOP(String nombreIf, String cveEjecutivo, List lstSolicitudes, List lstEstatus,
                                  String userConc, String nombreConc) throws AppException;

    public void cambiaEstatusSolic(String cveEstatusGral, String observaciones, String nombreIf, List lstSolicitudes,
                                   String iNoUsuario, String strNombreUsuario) throws AppException;

    public List consultaSolicRec(String cveIf, String cveEstatus, String numSolic, String fecRecpIni,
                                 String fecRecpFin) throws AppException;

    public String obtieneArchivoSolic(String rutaDestino, String cveSolicitud, String tipoArchivo) throws AppException;

    public HashMap obtieneInfoCorreo(String usuario) throws AppException;

    public HashMap obtieneInfoGerenteSubdirector() throws AppException;

    public List obtieneInfoSolicCompleta(String cveSolicitud) throws AppException;

    //*************************** Caso de Uso: CU03 *******************************************	
    public List consultaAtencionSolicRec(String cveIf, String cveEstatus, String numSolic, String fecRecpIni,
                                         String fecRecpFin, String usuarioOP) throws AppException;

    public List obtieneInfoRegAceptRech(List lstCveSolicitudes) throws AppException;

    public void rechazaSolicsParaConfirmacion(List lstSolicitudes, String fecRechazo, String horaRechazo,
                                              String causasRech, String nombreUsrOP) throws AppException;

    public void aceptaSolicsParaConfirmacion(List lstSolicitudes, String fecAbono, String observ,
                                             String nombreUsrOP) throws AppException;


    //*************************** CASO DE USO: CU01-REGISTRAR SOLICITUDES DE RECURSOS*********************************
    public boolean getvalidaDiaHabil(String diaMes);

    public boolean getValidaHora(String horario);

    public String getNumaxSolicitud();

    public HashMap getContratoXPerfil(String perfil);

    public HashMap getDatosIF(String ic_if);

    public List getFirmasModificatorias(String ic_if);

    public boolean guardarArchivoSolic(String ic_solicitud, String rutaArchivo, String tipoArchivo,
                                       String tipoSolicitud);

    public String consArchCotizacion(String rutaDestino, String ic_solicitud, String pantalla, String tipoArchivo,
                                     String extension, String tipoSolicitud);

    public List validaTasaAmortizacion(String ic_solicitud, String rutaNombreArchivo, String montoSolicitado);

    public String guardaTasaAmortizacionTMP(String ic_solicitud, String periodo[], String fecha[],
                                            String amortizacion[]);

    public void cargaSolicTMP(List parametros);

    public HashMap getDatosSolicTMP(String ic_solicitud, String ic_if);

    public String getguardarSolicitud(List parametros);

    public List getTablaAmortizacion(String ic_solicitud);

    public HashMap getDatosSolicitud(String ic_solicitud, String ic_if);

    public String generarArchivoPDF(List parametros);

    public boolean guardarArchSolic(List parametros);

    public void envioCorreo(List parametros, String ic_solicitud);

    public String getSaldoLiquido(String ic_if);

    //*************************** Caso de Uso: CU04-  y  Caso de Uso: CU05 *********************************	
    public String consDescargaArchivos(String rutaDestino, String ic_solicitud, String tipoArchivo, String extension);

    public String getArchivoTablaAmortizacion(String path, String ic_solicitud);

    //*****************************************Caso de Uso 5 Carga de empresas apoyadas*******************************	
    public List CargaListadeArchivosTMP(String noPrestamo, String strDirectorioTemp, String nombreArchivo,
                                        String claveCarga, String strPerfil, String importe, String iNoCliente);

    public String getArchivoAcuse(List parametros);

    public List validaCargaDatos(String claveCarga) throws NafinException;

    public String claveCarga();

    public List totalCargaLinea(String claveCarga);

    public List validaRebasaLimite(List parametros);

    public void cancelaCargaEmpresas(List parametros);

    public void confirmaCargaCargaEmpresas(List parametros);

    public void envioCorreoCargaEmpresas(List parametros);

    //*************************** Caso de Uso: CU06 *********************************
    public ArrayList getListaUsuarios(String tipoAfiliado, String claveAfiliado, String perfil) throws Exception;

    public List getConsultar(String tipoUsuario, String perfil) throws Exception;

    public boolean guardaDatosIF(String cve_if, String cuenta, String banco, String clabe, String fec_fir_cont,
                                 String cont_modif, String nom_cont, String fec_fir_ope, String usr_promo,
                                 String tel_promo, String usr_super, String tel_super, String hr_envio, String reenvio,
                                 String operaFirma, String operador, String autorizadorUno, String autorizadorDos,
                                 String mujeresEmpresarias) throws NafinException;

    public boolean actualizaDatosIF(String cve_if, String cuenta, String banco, String clabe, String fec_fir_cont,
                                    String cont_modif, String nom_cont, String fec_fir_ope, String usr_promo,
                                    String tel_promo, String usr_super, String tel_super, String hr_envio,
                                    String reenvio, String operaFirma, String operador, String autorizadorUno,
                                    String autorizadorDos, String mujeresEmpresarias) throws NafinException;

    public void guardaFecMod(String cve_if, String desc, String fec_fir_modif);

    public String getModificatorios(String clave_if);

    public List cargaFecMod(String clave_if, int filas, int inicio);

    //*************************** Caso de Uso: CU07 *********************************
    public List cargaNomContratos();

    public boolean actualizaNombreContratos(String cveCont, String nomCont);

    //*************************** Caso de Uso: CU08 *********************************
    public boolean guardaParamGrales(String usr_sub, String tel_sub, String usr_con, String tel_con,
                                     String mails) throws NafinException;

    public boolean actualizaParamGrales(String subDirector, String telefonoSubdirector, String concentrador,
                                        String telefonoConcentrador, String concentradorDos,
                                        String telefonoConcentradorDos, String emailDirector, String emailFondoLiquido,
                                        String icEmail) throws NafinException;

    public List cargaDatosGrales();


    //*************************** Caso de Uso: CU13 *********************************

    public String getIcFinanciera(String icIf);

    public String fechaProcesada(String fecha, String ifFinanciera);

    public HashMap getValidacionInicial(String dataFile, String path);

    public void insertRecordDetail(String periodDate, int ifnbCode, String file, String path);

    public String getIfnbNombre(int ifnbCode);

    public String getIfnbNumeroArchivo(int ifnbCode, String periodDate);

    public void insertRecord(String periodDate, int ifnbCode);

    public boolean checkIfnbCode(int ifnbCode, String path);

    //*************************** F035 - 2014 -- Segunda Etapa Operacion Electronica *********************************
    // PANTALLA A: IF 4CP/IF 5CP - OPERACIÓN ELECTRÓNICA IFNBS - CAPTURAS - ACTUALIZACIÓN DE LA CARTERA PRENDARIA

    public void validarCargaCarteraPrendaria(int numeroLinea, CarteraPrendariaValidator registro);

    public void validarCifrasControlCarteraPrendaria(CarteraPrendariaValidator fileValidator);

    public String getClaveFinanciera(String claveIf) throws AppException;

    public boolean validaNombreArchivoAnexo5(String nombreArchivo, String claveIf, String[] hint) throws AppException;

    public boolean existeCargaPreviaCarteraPrendaria(String nombreArchivo) throws AppException;

    public boolean existeCargaPreviaCarteraPrendariaSoloNafinet(String nombreArchivo) throws AppException;

    public void registraEnBitacoraCCBE(int eventType, int msgCode, int ifnbCode, String date, String period,
                                       String user, String coment) throws AppException;

    public boolean existeClaveFinanciera(String claveIf) throws AppException;

    //****************** F0352014 *******************
    // --- pantalla Manto. Solicitudes de Recursos

    public HashMap obtieneInfoDeFirmaMancom(String IF, String Usuario);

    public String getTelefonoIfOperador(String IC_IF);

    public boolean actualizaEstatusManto(String ic_if, String cc_acuse, String ic_estatus, String numDoctos,
                                         String Auto, String noUsuario, String causa, List parametros,
                                         String Accion) throws NafinException;

    public void envioCorreoNotificacion(List parametros, String ic_solicitud, String Operador);
    //-- PROCESO
    public void envioCorreoConcentrado(String path, String dirPantalla) throws NafinException;

    public String getArchivoSoliAutorizada(String path, String lisEstatus, String dirPantalla);

    public HashMap obtieneInfoConcentrador(String Usuario); //checar esta funcion donde la creo
    // IF - pantalla Registrar solicitus de registros
    public String getOperaFirmaMancomunada(String IC_IF);

    public List insertarTablaDetalle(String dataFile, String path, String IF, String FileCode) throws NafinException;

    public HashMap obtieneErrorDoc(String cveCargaTmp, String IF, String cadIc);
    // consulta operacion
    public String getCampoRegDatosIf(String Campo, String IF);

    public String getCampoOpeParamGenerales(String Campo);

    public String autorizadorRegistrado(String ic_solicitud, String noUsuario);

    public String guardaSolicitud1(ArrayList parametros, String ic_if, String strDirectorioTemp, String strPais,
                                   Long iNoNafinElectronico, String strNombre, String strNombreUsuario,
                                   String strDirectorioPublicacion, String lblFondoLiquido,
                                   String strPerfil) throws NafinException;

    public String getguardarSolicitudCon(List parametros, AccesoDB con) throws NafinException;

    public boolean guardarArchSolicCon(List parametros, AccesoDB con) throws NafinException;

    public String generarArchivoPDFCon(List parametros, AccesoDB con) throws NafinException;

    public HashMap getDatosSolicitudCon(String ic_solicitud, String ic_if, AccesoDB con) throws NafinException;

    public List getFirmasModificatoriasCon(String ic_if, AccesoDB con) throws NafinException;

    public List getTablaAmortizacionCon(String ic_solicitud, AccesoDB con) throws NafinException;

    public boolean envioCorreoAux(List parametros, String ic_solicitud);

    //*************** F014-2015**********************
    public List consultaCheckList(String tipoUsuario, String solicitud, String cc_usuario);

    public boolean guardaChkList(String cveSolicitud, String iNoUsuario, String checkRevisado, String tipoUsuario,
                                 String checkCorrecto, String checkObservaciones) throws NafinException;

    public String getcheckSoli(String cveSolicitud, String iNoUsuario, String checkRevisado, String tipoUsuario);

    public void limpiarCheck(String cveSolicitud, String iNoUsuario, String checkRevisado, String tipoUsuario);

    public HashMap validaListCheck(String cveSolicitud, String iNoUsuario, String tipoUsuario);

    public String getAcuseCheck(String ic_solicitudes);

    public String obtenChkSoli(String tipo_soli, String ic_solicitud, String cc_usuario);

    public String getExisteRevision(String ic_solicitud);

    public void guardaBitacoraCheck(String ic_solicitud, String estatus, String cc_usuario, String check,
                                    String cc_usuario_con, AccesoDB con, String tipo_soli) throws NafinException;

    public void aceptaSolicsParaConfirmacionConCheck(List lstSolicitudes, String fecAbono, String observ,
                                                     String nombreUsrOP, String iNoUsuario) throws AppException;

    public void rechazaSolicsParaConfirmacionConCheck(List lstSolicitudes, String fecRechazo, String horaRechazo,
                                                      String causasRech, String nombreUsrOP,
                                                      String iNoUsuario) throws AppException;

    public String obtenChkBitacora(String path, String IC_CHKLIST_SOLICITUD);

    public List consultarBitacoraRevision(String cveSolicitud);

    public String existeRechazoInterno(String ic_solicitud, String ejecutivoOpe);

    List cargaContratos(String clave_if, int filas, int inicio);

    String guardaContratoIF(String ic_contrato, String ic_if, String cg_contrato_oe, String df_fecha_firma,
                            String df_firma_oe) throws NafinException;

    boolean guardaModificatorioIF(String ic_contrato, String cg_contrato_oe, String df_fecha_firma,
                                  String df_firma_oe) throws NafinException;
    public boolean actualizaModificatorioIF(String ic_contrato, String ic_modificatorio, String cg_contrato_oe, String df_fecha_firma,
                                         String df_firma_oe) throws NafinException;
    List getContrato(String clave_if);

    boolean borraModificatorios(String ic_contrato);

    List cargaModModificatorios(String clave_if, int filas, int inicio);
}


