package com.netro.electronica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class AuxiliarCarteraPrendaria {

	/*
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(AuxiliarCarteraPrendaria.class);

	public static final int STATUS_CARGA_ANEXO_4					= 14;
	public static final int EVENT_CARGA_ANEXO_4					= 15;
	public static final int MESSAGE_NO_ERROR   					= 0;
	public static final int MESSAGE_ERROR_NO_IDENTIFICADO   	= 99;
	public static final int MESSAGE_ANEXO4_ERROR_DETALLE     = 5;

	private String inicializaCampo(String valorCampo,String valorDefault){

		if( Comunes.esVacio(valorCampo) ){
			return valorDefault;
		}
		return valorCampo;

	}

	private void replaceChars(StringBuffer buffer,char oldChar, int newChar){

		for(int i=0;i<buffer.length();i++){
			char c = buffer.charAt(i);
			if(c == oldChar ){
				if( newChar == -1 ){
					buffer.deleteCharAt(i);
				}else{
					buffer.setCharAt(i,(char)newChar);
				}
			}
		}

	}

	public boolean transmiteCarteraPrendaria(CarteraPrendariaValidator registro,String[] msg){

		log.info("transmiteCarteraPrendaria(E)");

		AccesoDB				con		 				= new AccesoDB();
		PreparedStatement ps							= null;
		ResultSet			rs			 				= null;
		String 				sentence	 				= null;

		boolean 				exito						= true;

		String 				claveFinanciera 		= null;
		String 				claveMesCarga	  		= null;
		String 				login						= null;

		String 				archivoCsv 				= null;
		File 				 	csvFile 					= null;
		FileInputStream 	csvFileInputstream	= null;
		File 				 	pdfFile 					= null;
		FileInputStream 	pdfFileInputstream	= null;
		BufferedReader		br							= null;
		int 					numeroRegistros 		= 0;

		// Obtener instancia del EJB de Garantias
		OperacionElectronica operacionElectronica = null;
		try {
			operacionElectronica 		= ServiceLocator.getInstance().lookup("OperacionElectronicaEJB",OperacionElectronica.class);

		} catch(Exception e) {

			exito			= false;

			log.error("transmiteCarteraPrendaria(Exception): Ocurri� un error al obtener instancia del EJB de Operaci�n Electr�nica: " + e.getMessage());
			e.printStackTrace();

			msg[0] = "Ocurri� un error al obtener instancia del EJB de Operaci�n Electr�nica: " + e.getMessage();

		}

		// Verificar que se haya podido obtener instancia del EJB del Operaci�n Electr�nica
		if( !exito ){

			log.info("transmiteCarteraPrendaria(S)");
			return exito;

		}

		try {

			// Nota: en caso se evalue usar una tabla temporal, simil de CCBE_...CONTENT, lo primero que se har�a ser�a insertar en esta tabla
			// la operaci�n corresponder�a a una transacci�n independiente. Si 100,000 es el promedio, la tabla temporal podr�a no
			// necesitarse, ya que para este caso las inserciones corresponder�an a 1 minuto.

			// Prepar archivo csv a subir
			csvFile 						= new File(registro.getRutaArchivoCsv());
			csvFileInputstream 		= new FileInputStream(csvFile);
			// Prepar archivo pdf a subir
			pdfFile 						= new File(registro.getRutaArchivoPdf());
			pdfFileInputstream 		= new FileInputStream(pdfFile);

			// Parsear campos del nombre del archivo
			archivoCsv					= registro.getArchivoCsv();
			claveFinanciera 			= archivoCsv.substring(0,6).replaceFirst("^[0]+","");
			claveMesCarga	  			= archivoCsv.substring(10,16);
			login							= registro.getLogin();

			// Conectarse a la Base de Datos
			con.conexionDB();

			sentence =
				"DELETE FROM                  "  +
				"   COM_CART_PRENDARIA_A5     "  +
				"WHERE                        "  +
				"   IC_FINANCIERA   = ? AND   "  +
				"   IC_MES_CARTERA  = ? AND   "  +
				"   CS_TIPO_ARCH    = 'CSV'   ";
			ps = con.queryPrecompilado(sentence);
			ps.setInt( 1, Integer.parseInt(claveFinanciera) );
			ps.setInt( 2, Integer.parseInt(claveMesCarga)	);
			ps.executeUpdate();
			ps.close();

			sentence =
				"DELETE FROM                  "  +
				"   COM_CART_PRENDARIA_A5     "  +
				"WHERE                        "  +
				"   IC_FINANCIERA    = ? AND  "  +
				"   IC_MES_CARTERA   = ? AND  "  +
				"   CS_TIPO_ARCH     = 'PDF'  ";
			ps = con.queryPrecompilado(sentence);
			ps.setInt( 1, Integer.parseInt(claveFinanciera) );
			ps.setInt( 2, Integer.parseInt(claveMesCarga)	);
			ps.executeUpdate();
			ps.close();

			sentence =
				"INSERT INTO                "  +
				"   COM_CART_PRENDARIA_A5   "  +
				"   (                       "  +
				"      IC_FINANCIERA,       "  +
				"      IC_MES_CARTERA,      "  +
				"      CS_TIPO_ARCH,        "  +
				"      BI_ARCHIVO,          "  +
				"      CG_USUARIO_REGISTRO, "  +
				"      DF_FECHA_REGISTRO    "  +
				"   )                       "  +
				"   VALUES                  "  +
				"   (                       "  +
				"      ?,                   "  +
				"      ?,                   "  +
				"      'CSV',               "  +
				"      ?,                   "  +
				"      ?,                   "  +
				"      SYSDATE              "  +
				"   )                       ";

			ps = con.queryPrecompilado(sentence);
			ps.setInt(1,  			Integer.parseInt(claveFinanciera)			);
			ps.setInt(2,  			Integer.parseInt(claveMesCarga)				);
			ps.setBinaryStream(3,csvFileInputstream, (int)csvFile.length() );
			ps.setString(4, 		login													);
			ps.executeUpdate();
			ps.close();

			sentence =
				"INSERT INTO                "  +
				"   COM_CART_PRENDARIA_A5   "  +
				"   (                       "  +
				"      IC_FINANCIERA,       "  +
				"      IC_MES_CARTERA,      "  +
				"      CS_TIPO_ARCH,        "  +
				"      BI_ARCHIVO,          "  +
				"      CG_USUARIO_REGISTRO, "  +
				"      DF_FECHA_REGISTRO    "  +
				"   )                       "  +
				"   VALUES                  "  +
				"   (                       "  +
				"      ?,                   "  +
				"      ?,                   "  +
				"      'PDF',               "  +
				"      ?,                   "  +
				"      ?,                   "  +
				"      SYSDATE              "  +
				"   )                       ";

			ps = con.queryPrecompilado(sentence);
			ps.setInt(1,  			Integer.parseInt(claveFinanciera)			);
			ps.setInt(2,  			Integer.parseInt(claveMesCarga)				);
			ps.setBinaryStream(3,pdfFileInputstream, (int)pdfFile.length() );
			ps.setString(4, 		login													);
			ps.executeUpdate();
			ps.close();

        // Insertar "cabecera" en CCBE_ANEXO_CTRO
        /*
        		" INSERT INTO CCBE_ANEXO_CTRO " +
        		" (ANX_FILE_CODE,ANX_IFNB_CODE, ANX_PERIOD, ANX_STATUS_CODE, " +
            "  ANX_LOAD_DATE) VALUES ( CCBE_SEQ_ANEXO_CTRO.Nextval ," + ifnbCode + ", " +
            "  LAST_DAY(TO_DATE('"+periodDate.getFormated("MM/yyyy")+ "','mm/yyyy')), " +
            ConstantsSupport.STATUS_CARGA_ANEXO_4 + ", " +
            "  TO_DATE('"+loadDate.getFormated("dd/MM/yyyy")+"','dd/mm/yyyy')) "
        */

        sentence =
				"INSERT INTO                          "  +
				"   R_CCBE_ANEXO_CTRO                 "  +
				"   (                                 "  +
				"      ANX_FILE_CODE,                 "  +
				"      ANX_IFNB_CODE,                 "  +
				"      ANX_PERIOD,                    "  +
				"      ANX_STATUS_CODE,               "  +
				"      ANX_LOAD_DATE                  "  +
				"   )                                 "  +
				"   VALUES                            "  +
				"   (                                 "  +
				"      R_CCBE_SEQ_ANEXO_CTRO.NEXTVAL, "  +
				"      ?,                             "  +
				"      LAST_DAY(TO_DATE(?,'YYYYMM')), "  +
				"      ?,                             "  +
				"      TRUNC(SYSDATE)                 "  +
				"   )                                 ";

			ps = con.queryPrecompilado(sentence);
			ps.setInt(1,	 Integer.parseInt(claveFinanciera) 					);
			ps.setString(2, claveMesCarga							 					);
			ps.setInt(3, 	 AuxiliarCarteraPrendaria.STATUS_CARGA_ANEXO_4 	);
			ps.executeUpdate();
			ps.close();

			// Obtener el ANX_FILE_CODE de un INFB y un PERIODO
			/*
				" SELECT ANX_FILE_CODE " +
         	" FROM CCBE_ANEXO_CTRO " +
         	" WHERE to_char(ANX_PERIOD,'mm/yyyy') = '"+ periodDate.getFormated("MM/yyyy") + "' " +
         	" AND   ANX_IFNB_CODE = " + ifnbCode;
         */
         int anxFileCode = 0;
         sentence =
				"SELECT                                                    "  +
				"   ANX_FILE_CODE                                          "  +
				"FROM                                                      "  +
				"   R_CCBE_ANEXO_CTRO                                      "  +
				"WHERE                                                     "  +
				"   ANX_PERIOD    >= TO_DATE(?,'YYYYMMDD')             AND "  +
				"   ANX_PERIOD    <  LAST_DAY(TO_DATE(?,'YYYYMMDD'))+1 AND "  +
				"   ANX_IFNB_CODE =  ?                                     ";

			ps = con.queryPrecompilado(sentence);
			ps.setString(1, claveMesCarga	+ "01"				  );
			ps.setString(2, claveMesCarga + "01"				  );
			ps.setInt(3,	 Integer.parseInt(claveFinanciera) );
			rs = ps.executeQuery();

			if( rs.next() ){
         	anxFileCode = rs.getInt("ANX_FILE_CODE");
         }
         rs.close();
         ps.close();

         // Insertar un registro el la tabla CCBI_IFNB_FILES
         /*

				"INSERT INTO CCBE_ANEXO_CTRO_CONTENT\n" +
				"(   RECORD_NUM,\n" +
				"    ANX_FILE_CODE,\n" +
				"    NUMERO_CREDITO,\n" +
				"    NOMBRE_ACREDITADO,\n" +
				// "    RUG,\n" +
				"     TIPO_OPERACION,\n" +
				"    FECHA_INSCRIPCION,    \n" +  //5
				"    TIPO_TASA,    \n" +
				"    TASA_BASE,    \n" +
				"    PUNTOS_ADICIONALES,    \n" +
				"    MONTO_OTORGADO,    \n" +
				"    RECURSOS_SOLICITADOS,    \n" + //10
				"    PLAZO_ORIGINAL,    \n" +
				"    CAPITAL_VIGENTE,    \n" +
				"    INTERESES_VIGENTES,    \n" +
				"    CAPITAL_VENCIDO,    \n" +
				"    INTERESES_VENCIDOS,    \n" +  //15
				"    INTERESES_MORATORIOS,\n" +
				"    SALDO_TOTAL,    \n" +
				"    DIAS_MORA\n" +                //18
				" )\n" +
				" VALUES \n" +
				" (CCBE_SEQ_ANEXO_CTRO_CONTENT.Nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)

         */
        sentence =
				"INSERT INTO                                "  +
				"   R_CCBE_ANEXO_CTRO_CONTENT               "  +
				"   (                                       "  +
				"      RECORD_NUM,                          "  +
				"      ANX_FILE_CODE,                       "  +
				"      NUMERO_CREDITO,                      "  +
				"      NOMBRE_ACREDITADO,                   "  +
				"      TIPO_OPERACION,                      "  +
				"      FECHA_INSCRIPCION,                   "  +
				"      TIPO_TASA,                           "  +
				"      TASA_BASE,                           "  +
				"      PUNTOS_ADICIONALES,                  "  +
				"      MONTO_OTORGADO,                      "  +
				"      RECURSOS_SOLICITADOS,                "  +
				"      PLAZO_ORIGINAL,                      "  +
				"      CAPITAL_VIGENTE,                     "  +
				"      INTERESES_VIGENTES,                  "  +
				"      CAPITAL_VENCIDO,                     "  +
				"      INTERESES_VENCIDOS,                  "  +
				"      INTERESES_MORATORIOS,                "  +
				"      SALDO_TOTAL,                         "  +
				"      DIAS_MORA,                           "  +
				"      DESCRIPCION_BIEN                     "  +
				"   )                                       "  +
				"   VALUES                                  "  +
				"   (                                       "  +
				"      R_CCBE_SEQ_ANEXO_CTRO_CONTENT.NEXTVAL, "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      TO_DATE(?,'DD/MM/YYYY'),             "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?,                                   "  +
				"      ?                                    "  +
				"   )                                       ";

			ps = con.queryPrecompilado(sentence);

			br 									= registro.createBufferedReaderArchivoValidacion();

			// Insertar cada uno de los registros
			registro.doReset();
			registro.leeEncabezado(br);

			numeroRegistros 					= registro.getRecordsNumber();
			int 		index 					= 0;

			String 	numeroCredito			= null;
			String 	nombreAcreditado		= null;
			String 	tipoOperacion			= null;
			String 	fechaDisposicion		= null; // FECHA_INSCRIPCION
			String 	tipoTasa					= null;
			String 	tasaBase					= null;
			String 	puntosAdicionales		= null;
			String 	montoOtorgado			= null;
			String 	recursosSolicitados	= null;
			String 	plazoOriginal			= null;
			String 	capitalVigente			= null;
			String 	interesesVigentes		= null;
			String 	capitalVencido			= null;
			String 	interesesVencidos		= null;
			String 	interesesMoratorios	= null;
			String 	saldoTotal				= null;
			String 	diasRetraso				= null; // DIAS_MORA
			String 	descripcionBien 		= null;

			for(int i=0;i<numeroRegistros;i++){

				registro.parse(br);

				// Filtrar comas, de los montos. replaceAll(",","")
				replaceChars(registro.getPuntosAdicionales(),	',',  -1);
				replaceChars(registro.getMontoOtorgado(),			',',  -1);
				replaceChars(registro.getRecursosSolicitados(),	',',  -1);
				replaceChars(registro.getPlazoOriginal(),			',',  -1);
				replaceChars(registro.getCapitalVigente(),		',',  -1);
				replaceChars(registro.getInteresesVigentes(),	',',  -1);
				replaceChars(registro.getCapitalVencido(),		',',  -1);
				replaceChars(registro.getInteresesVencidos(),	',',  -1);
				replaceChars(registro.getInteresesMoratorios(),	',',  -1);
				replaceChars(registro.getSaldoTotal(),				',',  -1);
				replaceChars(registro.getDiasRetraso(),			',',  -1);

				// Reemplazar caracter: "'" por caracter: "�"
				replaceChars(registro.getNumeroCredito(),			'"', '�');
				replaceChars(registro.getTipoOperacion(),			'"', '�');
				replaceChars(registro.getFechaDisposicion(),		'"', '�');
				replaceChars(registro.getTipoTasa(),				'"', '�');
				replaceChars(registro.getTasaBase(),				'"', '�');
				replaceChars(registro.getPuntosAdicionales(),	'"', '�');
				replaceChars(registro.getPlazoOriginal(),			'"', '�');
				replaceChars(registro.getDiasRetraso(),			'"', '�');
				replaceChars(registro.getDescripcion(),			'"', '�');

				numeroCredito			= registro.getNumeroCredito().toString();
				nombreAcreditado		= registro.getNombreAcreditado().toString();
				tipoOperacion			= registro.getTipoOperacion().toString();
				fechaDisposicion		= registro.getFechaDisposicion().toString(); // FECHA_INSCRIPCION
				tipoTasa					= registro.getTipoTasa().toString();
				tasaBase					= registro.getTasaBase().toString();
				puntosAdicionales		= registro.getPuntosAdicionales().toString();
				montoOtorgado			= registro.getMontoOtorgado().toString();
				recursosSolicitados	= registro.getRecursosSolicitados().toString();
				plazoOriginal			= registro.getPlazoOriginal().toString();
				capitalVigente			= registro.getCapitalVigente().toString();
				interesesVigentes		= registro.getInteresesVigentes().toString();
				capitalVencido			= registro.getCapitalVencido().toString();
				interesesVencidos		= registro.getInteresesVencidos().toString();
				interesesMoratorios	= registro.getInteresesMoratorios().toString();
				saldoTotal				= registro.getSaldoTotal().toString();
				diasRetraso				= registro.getDiasRetraso().toString(); // DIAS_MORA
				descripcionBien		= registro.getDescripcion().toString();

				// Nota: Debido al �ltimo cambio solicitado, la siguiente secci�n de valores default
				// ya no es relevante.

				//numeroCredito // Se lee la cadena tal cual venga, no se hace ning�n ajuste.
				nombreAcreditado 			= inicializaCampo(nombreAcreditado,		"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.
				//tipoOperacion 			= inicializaCampo(tipoOperacion,			""); // Se deber� insertar null si vienen vac�os o con espacios en blanco
					// Nota: no es necesario asignar "" debido a que el campo se confirm� como obligatorio.
				fechaDisposicion 			= inicializaCampo(fechaDisposicion,		null); // Se deber� insertar null si vienen vac�os o con espacios en blanco
				tipoTasa 					= inicializaCampo(tipoTasa,				null); // Se deber� insertar null si vienen vac�os o con espacios en blanco
				tasaBase 					= inicializaCampo(tasaBase,				null); // Se deber� insertar null si vienen vac�os o con espacios en blanco
				puntosAdicionales 		= inicializaCampo(puntosAdicionales,	"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.
				//montoOtorgado 			= inicializaCampo(montoOtorgado,       "-1"); // Debe ser obligatorio, ya si viene vac�o o con espacios en blanco autom�ticamente le asigna -1
					// Nota: no es necesario asignar -1 debido a que el campo es obligatorio.
				//recursosSolicitados 	= inicializaCampo(recursosSolicitados, "-1"); // Debe ser obligatorio, ya si viene vac�o o con espacios en blanco autom�ticamente le asigna -1
					// Nota: no es necesario asignar -1 debido a que el campo es obligatorio.
				plazoOriginal 				= inicializaCampo(plazoOriginal,			"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.
				capitalVigente 			= inicializaCampo(capitalVigente,		"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.
				interesesVigentes 		= inicializaCampo(interesesVigentes,	"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.
				capitalVencido 			= inicializaCampo(capitalVencido,		"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.
				interesesVencidos 		= inicializaCampo(interesesVencidos,	"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.
				interesesMoratorios 		= inicializaCampo(interesesMoratorios,	"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.
				saldoTotal 					= inicializaCampo(saldoTotal,				"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.
				diasRetraso 				= inicializaCampo(diasRetraso,			"0"); // Si viene vac�o o con espacios, internamente se les asigna un valor de 0.

				index = 1;
				ps.clearParameters();
				ps.setInt(			index++,	anxFileCode									);
				ps.setString(		index++,	numeroCredito								);
         	ps.setString(		index++,	nombreAcreditado							);
         	ps.setString(		index++,	tipoOperacion								);
         	if( fechaDisposicion == null ){
         		ps.setNull(		index++, Types.DATE 			);
         	} else {
         		ps.setString(	index++, fechaDisposicion	);
            }
				ps.setString(		index++, tipoTasa										);
				ps.setString(		index++, tasaBase										);
				ps.setBigDecimal(	index++, new BigDecimal(puntosAdicionales)	);
				ps.setBigDecimal(	index++, new BigDecimal(montoOtorgado)			);
				ps.setBigDecimal(	index++, new BigDecimal(recursosSolicitados) );
				ps.setInt(			index++, Integer.parseInt(plazoOriginal) 		);
				ps.setBigDecimal(	index++, new BigDecimal(capitalVigente)		);
				ps.setBigDecimal(	index++, new BigDecimal(interesesVigentes)	);
				ps.setBigDecimal(	index++, new BigDecimal(capitalVencido)		);
				ps.setBigDecimal(	index++, new BigDecimal(interesesVencidos)	);
				ps.setBigDecimal(	index++, new BigDecimal(interesesMoratorios)	);
				ps.setBigDecimal(	index++, new BigDecimal(saldoTotal)				);
				ps.setInt(			index++, Integer.parseInt(diasRetraso)			);
				ps.setString(		index++, descripcionBien							);
				ps.executeUpdate();

				// Nota: Para una carga con un n�mero excesivo de registros 3x10^6, evaluar dar commit cada cierto numero de registros, para evitar
				// problemas con la cache de la base de datos, revisar si es posible que creen una tabla
				// temporal de la cual se insertar�an los registros mediante un INSERT SELECT y no
				// se tendr�a bloqueada la tabla original de contenido tanto tiempo.

			}
			ps.close();

        // Guardar exito en bit�cora
        operacionElectronica.registraEnBitacoraCCBE(
        	  AuxiliarCarteraPrendaria.EVENT_CARGA_ANEXO_4,
        	  AuxiliarCarteraPrendaria.MESSAGE_NO_ERROR,
        	  Integer.parseInt(claveFinanciera),
        	  null, // No se usa, se deja por compatibilidad
        	  claveMesCarga.substring(4,6) + "-" + claveMesCarga.substring(0,4),
        	  login,
        	  null
        );

        	// Nota: Debido a que la base de datos del Sistema CCBE no tiene regla alguna para impedir
        	// duplicidad de registros se agrega la siguiente validaci�n
        	sentence =
				"SELECT                                                                 "  +
				"   DECODE(COUNT(1),0,'false',1,'false','true') AS ERROR_CONCURRENCIA   "  +
				"FROM                                                                   "  +
				"   R_CCBE_ANEXO_CTRO                                                   "  +
				"WHERE                                                                  "  +
				"   ANX_PERIOD    >= TO_DATE(?,'YYYYMMDD')             AND              "  +
				"   ANX_PERIOD    <  LAST_DAY(TO_DATE(?,'YYYYMMDD'))+1 AND              "  +
				"   ANX_IFNB_CODE =  ?                                                  ";

			ps = con.queryPrecompilado(sentence);
			ps.setString(1, claveMesCarga	+ "01"				  );
			ps.setString(2, claveMesCarga + "01"				  );
			ps.setInt(3,	 Integer.parseInt(claveFinanciera) );
			rs = ps.executeQuery();

			rs.next();
			if( "true".equals(rs.getString("ERROR_CONCURRENCIA")) ){
         	exito	 = false;
         	msg[0] = "Error de concurrencia, se intent� transmitir un periodo que ya est� cargado.";
         }
         rs.close();
         ps.close();

		} catch(SQLException sqlException ){

			exito = false;

			log.error("transmiteCarteraPrendaria(SQL Exception)");
			log.error("transmiteCarteraPrendaria.registro    = <" + registro    + ">");
			if( registro != null ){
				log.error("transmiteCarteraPrendaria.registro.getClaveIF()            = <" + registro.getClaveIF()            + ">");
				log.error("transmiteCarteraPrendaria.registro.getDirectorioTemporal() = <" + registro.getDirectorioTemporal() + ">");
				log.error("transmiteCarteraPrendaria.registro.getLogin()              = <" + registro.getLogin()              + ">");
				log.error("transmiteCarteraPrendaria.registro.getRutaArchivoCsv()     = <" + registro.getRutaArchivoCsv()     + ">");
				log.error("transmiteCarteraPrendaria.registro.getArchivoCsv()         = <" + registro.getArchivoCsv()         + ">");
				log.error("transmiteCarteraPrendaria.registro.getRutaArchivoPdf()     = <" + registro.getRutaArchivoPdf()     + ">");
				log.error("transmiteCarteraPrendaria.registro.getArchivoPdf()         = <" + registro.getArchivoPdf()         + ">");
				log.error("transmiteCarteraPrendaria.registro.getProcessID()          = <" + registro.getProcessID()          + ">");
			}
			log.error("transmiteCarteraPrendaria.sentence    = <" + sentence    + ">");
			sqlException.printStackTrace();

			if( sqlException.getErrorCode() == 1 ){ // "ORA-00001: unique constraint" // Debido al esquema actual, este error no se va a presentar.
				msg[0] = "Se intent� procesar un periodo que ya est� cargado.";
			} else {
				msg[0] = "Fall� la transmisi�n de archivos: " + sqlException.getMessage();
			}

			// Insertar en bit�cora del CCBE el error de programaci�n
			try {

				operacionElectronica.registraEnBitacoraCCBE(
				  AuxiliarCarteraPrendaria.EVENT_CARGA_ANEXO_4,
				  AuxiliarCarteraPrendaria.MESSAGE_ERROR_NO_IDENTIFICADO,
				  Integer.parseInt(claveFinanciera),
				  null, // No se usa, se deja por compatibilidad
				  claveMesCarga.substring(4,6) + "-" + claveMesCarga.substring(0,4),
				  login,
				  null //"Error no identificado. " + msg[0]
				);

        	} catch(Exception e1){

        		log.error("transmiteCarteraPrendaria.registraEnBitacoraCCBE(Exception): No se pudo registrar error en bit�cora del CCBE.");
        		e1.printStackTrace();

        	}

		} catch(Exception e){

			exito = false;

			log.error("transmiteCarteraPrendaria(Exception)");
			log.error("transmiteCarteraPrendaria.registro    = <" + registro    + ">");
			if( registro != null ){
				log.error("transmiteCarteraPrendaria.registro.getClaveIF()            = <" + registro.getClaveIF()            + ">");
				log.error("transmiteCarteraPrendaria.registro.getDirectorioTemporal() = <" + registro.getDirectorioTemporal() + ">");
				log.error("transmiteCarteraPrendaria.registro.getLogin()              = <" + registro.getLogin()              + ">");
				log.error("transmiteCarteraPrendaria.registro.getRutaArchivoCsv()     = <" + registro.getRutaArchivoCsv()     + ">");
				log.error("transmiteCarteraPrendaria.registro.getArchivoCsv()         = <" + registro.getArchivoCsv()         + ">");
				log.error("transmiteCarteraPrendaria.registro.getRutaArchivoPdf()     = <" + registro.getRutaArchivoPdf()     + ">");
				log.error("transmiteCarteraPrendaria.registro.getArchivoPdf()         = <" + registro.getArchivoPdf()         + ">");
				log.error("transmiteCarteraPrendaria.registro.getProcessID()          = <" + registro.getProcessID()          + ">");
			}
			log.error("transmiteCarteraPrendaria.sentence    = <" + sentence    + ">");
			e.printStackTrace();

			msg[0] = "Fall� la transmisi�n de archivos: " + e.getMessage();

			// Insertar en bit�cora del CCBE el error de programaci�n
			try {

				operacionElectronica.registraEnBitacoraCCBE(
				  AuxiliarCarteraPrendaria.EVENT_CARGA_ANEXO_4,
				  AuxiliarCarteraPrendaria.MESSAGE_ERROR_NO_IDENTIFICADO,
				  Integer.parseInt(claveFinanciera),
				  null, // No se usa, se deja por compatibilidad
				  claveMesCarga.substring(4,6) + "-" + claveMesCarga.substring(0,4),
				  login,
				  null // "Error no identificado. " + msg[0]
				);

        	} catch(Exception e1){

        		log.error("transmiteCarteraPrendaria.registraEnBitacoraCCBE(Exception): No se pudo registrar error en bit�cora del CCBE.");
        		e1.printStackTrace();

        	}

		} finally {

			if( br 						!= null ){ try { br.close(); 						} catch(Exception e){} }
			if( csvFileInputstream 	!= null ){ try { csvFileInputstream.close(); } catch(Exception e){} }
			if( rs 						!= null ){ try { rs.close(); 						} catch(Exception e){} }
			if( ps 						!= null ){ try { ps.close(); 						} catch(Exception e){} }
			if( con.hayConexionAbierta() ){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

			log.info("transmiteCsvCarteraPrendaria(S)");

		}

		return exito;

	}

}