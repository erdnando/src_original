package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.math.BigDecimal;

import java.security.MessageDigest;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.Seguridad;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

import java.text.SimpleDateFormat;
import java.util.Map;

@Stateless(name = "CesionEJB" , mappedName = "CesionEJB")
@TransactionManagement(TransactionManagementType.BEAN)

public class  CesionEJBBean implements CesionEJB  {
	public CesionEJBBean(){}
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CesionEJBBean.class);
	
	/**
	 * Regresa verdadero o falso si la pyme pertenece aun grupo de cesion y es la empresa principal
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param cvePyme
	 */
	public boolean getEmpresaPrincipalGrupoCDer(String cvePyme)throws AppException{
		log.info("CesionEJBBean::getEmpresaPrincipalGrupoCDer(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer("");
		boolean csGrupoCesion = false;
		try{
			con.conexionDB();

			strSQL.append("SELECT COUNT (*) csgrupocesion ");
			strSQL.append("  FROM cder_grupo cg, cderrel_pyme_x_grupo cpg ");
			strSQL.append(" WHERE cg.ic_grupo_cesion = cpg.ic_grupo_cesion ");
			strSQL.append("   AND cs_pyme_principal = 'S' ");
			strSQL.append("   AND cpg.ic_pyme = ? ");

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setLong(1, Long.parseLong(cvePyme));
			rs = ps.executeQuery();

			if( rs.next() && rs.getInt("csgrupocesion")>0){
				csGrupoCesion = true;
			}

			rs.close();
			ps.close();

			return csGrupoCesion;

		}catch(Throwable e){
			log.error("CesionEJBBean::getEmpresaPrincipalGrupoCDer(Error)");
			throw new AppException("Error al consultar si la pyme pertenece a un grupo de cesion como empresa principal", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("CesionEJBBean::getEmpresaPrincipalGrupoCDer(S)");
		}
	}

	/**
	 * Obtiene las empresas asociadas al grupo de cesion de derechos seleccionado
	 * @throws netropology.utilerias.AppException
	 * @return Lista de empresas que pertenecen al grupo
	 * @param icGrupo
	 */
	public List getEmpresasXGrupoCder(String icGrupo) throws AppException{
		log.info("CesionEJBBean::getEmpresasXGrupoCder(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer("");
		List lstEmpresas = new ArrayList();
		UtilUsr utilUsr = new UtilUsr();
		try{
			con.conexionDB();
			HashMap hmEmpresa;
			strSQL.append("SELECT DISTINCT cp.ic_pyme, cp.cg_razon_social, cs_pyme_principal ");
			strSQL.append("  FROM cder_grupo cg, cderrel_pyme_x_grupo cpg, comcat_pyme cp ");
			strSQL.append(" WHERE cg.ic_grupo_cesion = cpg.ic_grupo_cesion ");
			strSQL.append("   AND cpg.ic_pyme = cp.ic_pyme ");
			strSQL.append("   AND cg.ic_grupo_cesion = ? ");
			strSQL.append("   order by cs_pyme_principal  desc ");

			log.info("strSQL  "+strSQL);

			ps = con.queryPrecompilado(strSQL.toString());
			ps.setLong(1, Long.parseLong(icGrupo));
			rs = ps.executeQuery();

			while(rs.next()){
				hmEmpresa = new HashMap();
				hmEmpresa.put("ic_pyme", rs.getString("ic_pyme"));
				hmEmpresa.put("cg_razon_social", rs.getString("cg_razon_social"));

				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(rs.getString("ic_pyme"), "P");
				String nombreRepPyme = "";
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {
					String loginUsrPyme = (String)usuariosPorPerfil.get(i);
					Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
					nombreRepPyme += (nombreRepPyme.equals("")?"":", ") +usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
				}

				hmEmpresa.put("representantes", nombreRepPyme);

				lstEmpresas.add(hmEmpresa);
			}
			rs.close();
			ps.close();

			return lstEmpresas;

		}catch(Throwable e){
			log.error("CesionEJBBean::getEmpresasXGrupoCder(ERROR)");
			throw new AppException("Error al consultar las empresas asociadas al grupo seleccionado");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("CesionEJBBean::getEmpresasXGrupoCder(S)");
		}
	}

	/**
	 * Metodo que ejecuta proceso batch para cambiar de estatus
	 * los contratos que ya expiraron y notificarles por correo
	 * @throws netropology.utilerias.AppException
	 */
	public void procesoSolicRech() throws AppException{
	   log.info("CesionEJBBean::procesoSolicRech(E)");
	   AccesoDB con = new AccesoDB();
	   PreparedStatement ps = null;
	   ResultSet rs = null;
	   StringBuffer strSQL = new StringBuffer("");
	   UtilUsr utilUsr = new UtilUsr();
	   boolean commit = true;
	   try{
	      con.conexionDB();
	      HashMap hmSolic = new HashMap();
	      
	      strSQL.append("SELECT cs.ic_solicitud, cs.ic_pyme, cs.ic_if, cc_acuse4, cc_acuse6, cs.df_fecha_aceptacion, cs.cg_no_contrato, ");
	      strSQL.append("       (CASE ");
	      strSQL.append("  WHEN SYSDATE > dias_laborables (cs.df_fecha_aceptacion, (SELECT  nvl(cpe.cg_valor,30) ");
	      strSQL.append("                                                                    FROM comcat_parametro_epo pe, com_parametrizacion_epo cpe  ");
	      strSQL.append("                                                                   WHERE pe.cc_parametro_epo = cpe.cc_parametro_epo(+)   ");
	      strSQL.append("                                                                     AND cpe.ic_epo(+) = cs.ic_epo  ");
	      strSQL.append("                                                                     AND pe.CC_PARAMETRO_EPO like 'CDER_DIAS_FIRMAR_CONTRATO'))  ");
	      strSQL.append("          THEN 'S' ");
	      strSQL.append("          ELSE 'N' ");
	      strSQL.append("       END ) cs_rechazar");
	      strSQL.append("  FROM cder_solicitud cs  ");
	      strSQL.append(" WHERE cs.ic_estatus_solic IN (3,14,7) AND cs.df_fecha_aceptacion IS NOT NULL ");
	      strSQL.append("  ORDER BY cs.ic_solicitud ");

	      log.info("strSQL  "+strSQL);

	      ps = con.queryPrecompilado(strSQL.toString());
	      rs = ps.executeQuery();

	      PreparedStatement ps2 = null;
	      ResultSet rs2 = null;
	      Correo correo = new Correo();
	      Usuario usuario = null;
	      String mail = "";


	      while(rs.next()){
	         if(rs.getString("cs_rechazar").equals("S")){
	            StringBuffer textCorreo = new StringBuffer("");
	            textCorreo.append("<b>PyME �nica o PyME Capturista</b><br><br>");
	            textCorreo.append("Con relaci�n al procedimiento de Cesi�n que se ha iniciado, ");
	            textCorreo.append("referente al Contrato No. ");
	            textCorreo.append(rs.getString("cg_no_contrato"));
	            textCorreo.append(", le informamos que han transcurrido ");
	            textCorreo.append("los 30 (treinta) d�as h�biles para llevar a cabo el tr�mite de Cesi�n ");
	            textCorreo.append("de Derechos de Cobro. Por lo que, si desea continuar con el Proceso de ");
	            textCorreo.append("Cesi�n de Derechos de Cobro, deber� tramitar nuevamente el consentimiento ");
	            textCorreo.append("del contrato correspondiente.<br><br>");
	            textCorreo.append("Para cualquier duda o aclaraci�n favor de comunicarse al 01800-22-33-627 (CADENAS).");

	            strSQL = new StringBuffer("");
	            strSQL.append("UPDATE cder_solicitud ");
	            strSQL.append("   SET ic_estatus_solic = 25 ");
	            strSQL.append(" WHERE ic_solicitud = ? ");

	            ps2 = con.queryPrecompilado(strSQL.toString());
	            ps2.setLong(1, Long.parseLong(rs.getString("ic_solicitud")));
	            ps2.executeUpdate();
	            ps2.close();

	            strSQL = new StringBuffer("");
	            strSQL.append("SELECT csr.ic_usuario ");
	            strSQL.append("  FROM cder_solicitud cs, cder_solic_x_representante csr ");
	            strSQL.append(" WHERE cs.ic_solicitud = csr.ic_solicitud AND cs.ic_solicitud = ? ");

	            ps2 = con.queryPrecompilado(strSQL.toString());
	            ps2.setLong(1, Long.parseLong(rs.getString("ic_solicitud")));
	            rs2 = ps2.executeQuery();
	            while(rs2.next()){
	               usuario = utilUsr.getUsuario(rs2.getString("ic_usuario"));
	               mail = usuario.getEmail();
	               if(mail!=null && !mail.equals(""))
	                  correo.enviarTextoHTML("No_response@nafin.gob.mx", mail, "Notificaci�n Contrato Expirado", textCorreo.toString(), "");
	            }
	            rs2.close();
	            ps2.close();

	            if(rs.getString("cc_acuse4")!=null && !rs.getString("cc_acuse4").equals("")){
	               strSQL = new StringBuffer("");
	               strSQL.append("SELECT ca.cg_usuario ");
	               strSQL.append("  FROM cder_solicitud cs, cder_acuse ca ");
	               strSQL.append(" WHERE cs.cc_acuse4 = ca.cc_acuse  ");
	               strSQL.append(" AND cs.ic_solicitud = ? ");

	               ps2 = con.queryPrecompilado(strSQL.toString());
	               ps2.setLong(1, Long.parseLong(rs.getString("ic_solicitud")));
	               rs2 = ps2.executeQuery();
	               if(rs2.next()){
	                  usuario = utilUsr.getUsuario(rs2.getString("cg_usuario"));
	                  mail = usuario.getEmail();
	                  if(mail!=null && !mail.equals(""))
	                     correo.enviarTextoHTML("No_response@nafin.gob.mx", mail, "Notificaci�n Contrato Expirado", textCorreo.toString(), "");
	               }
	               rs2.close();
	               ps2.close();

	            }

	            if(rs.getString("cc_acuse6")!=null && !rs.getString("cc_acuse6").equals("")){
	               strSQL = new StringBuffer("");
	               strSQL.append("SELECT ca.cg_usuario ");
	               strSQL.append("  FROM cder_solicitud cs, cder_acuse ca ");
	               strSQL.append(" WHERE cs.cc_acuse6 = ca.cc_acuse  ");
	               strSQL.append(" AND cs.ic_solicitud = ? ");

	               ps2 = con.queryPrecompilado(strSQL.toString());
	               ps2.setLong(1, Long.parseLong(rs.getString("ic_solicitud")));
	               rs2 = ps2.executeQuery();
	               if(rs2.next()){
	                  usuario = utilUsr.getUsuario(rs2.getString("cg_usuario"));
	                  mail = usuario.getEmail();
	                  if(mail!=null && !mail.equals(""))
	                     correo.enviarTextoHTML("No_response@nafin.gob.mx", mail, "Notificaci�n Contrato Expirado", textCorreo.toString(), "");
	               }
	               rs2.close();
	               ps2.close();
	            }

	            con.terminaTransaccion(commit);
	         }
	      }
	      rs.close();
	      ps.close();


	   }catch(Throwable e){
	      commit = false;
	      log.error("CesionEJBBean::procesoSolicRech(ERROR)");
	      throw new AppException("Error al rechazar solicitud expirada", e);
	   }finally{
	      if(con.hayConexionAbierta()){
	         con.terminaTransaccion(commit);
	         con.cierraConexionDB();
	      }
	      log.info("CesionEJBBean::procesoSolicRech(S)");
	   }
	}

	public boolean existeCedenteXSolic(String claveSolicitud) throws AppException{
		log.info("CesionEJBBean::existeCedenteXSolic(E)");
		AccesoDB	con					= new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL	= new StringBuffer();
		boolean	bOK					= false;
		try {
			con.conexionDB();

			strSQL.append("SELECT  count(*) ");
			strSQL.append("    FROM cder_solicitud cs, cder_solic_x_representante csr ");
			strSQL.append("   WHERE cs.ic_solicitud = csr.ic_solicitud ");
			strSQL.append("     AND cs.ic_solicitud = ? ");
			strSQL.append("     AND NOT EXISTS ( ");
			strSQL.append("            SELECT 1 ");
			strSQL.append("              FROM cder_solic_pyme_notariales csn ");
			strSQL.append("             WHERE csn.ic_solicitud = csr.ic_solicitud ");
			strSQL.append("               AND csn.ic_pyme = csr.ic_pyme) ");

            log.debug("strSQL:"+strSQL+toString());
			ps  = con.queryPrecompilado(strSQL.toString());
			ps.setLong(1, Long.parseLong(claveSolicitud));
		    log.debug("["+claveSolicitud+"]");
			rs = ps.executeQuery();
            
			if(rs.next()){
				if(rs.getInt(1)==0){
					bOK = true;
				}
			}
		    log.debug("bOK:"+bOK);
			return bOK;
		} catch(Exception e){
			log.error("CesionEJBBean::existeCedenteXSolic(Exception)"+e);
			throw new AppException("Error al obtener el cedente", e);
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CesionEJBBean::existeCedenteXSolic(S)");
		}
	}

	public List getFirmasRepresentantes(String ic_solicitud ) throws AppException{
		log.info("getRepresentantesCEDENTE (E)");
		HashMap datosCons = new HashMap();
		List lstData = new ArrayList();

		AccesoDB	 con 	     =   new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try{

			con.conexionDB();

			strSQL.append("SELECT csr.ic_pyme, cp.cg_razon_social, csr.ic_usuario, ");
			strSQL.append("       csr.cc_acuse_firma_rep, csr.cg_firma_representante, ");
			strSQL.append("       csr.df_firma_representante, ca.cg_nombre_usuario, ");
			strSQL.append("       csr.DF_EXTINCION_REP ");
			strSQL.append("  FROM cder_solicitud cs, ");
			strSQL.append("       cder_solic_x_representante csr, ");
			strSQL.append("       comcat_pyme cp, ");
			strSQL.append("       cder_acuse ca ");
			strSQL.append(" WHERE cs.ic_solicitud = csr.ic_solicitud ");
			strSQL.append("   AND csr.ic_pyme = cp.ic_pyme ");
			strSQL.append("   AND csr.cc_acuse_firma_rep = ca.cc_acuse ");
			strSQL.append("   AND csr.ic_usuario = ca.cg_usuario ");
			strSQL.append("   AND csr.cg_firma_representante IS NOT NULL ");
			strSQL.append("   AND cs.ic_solicitud = ? ");



			varBind.add(new Integer(ic_solicitud));
			ps = con.queryPrecompilado(strSQL.toString(), varBind);
			rs = ps.executeQuery();

			while(rs.next()){
				datosCons = new HashMap();
				datosCons.put("ic_pyme",rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme"));
				datosCons.put("cg_razon_social",rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social"));
				datosCons.put("ic_usuario",rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario"));
				datosCons.put("cg_nombre_usuario",rs.getString("cg_nombre_usuario")==null?"":rs.getString("cg_nombre_usuario"));
				datosCons.put("cg_firma_representante",rs.getString("cg_firma_representante")==null?"":rs.getString("cg_firma_representante"));
				datosCons.put("cc_acuse_firma_rep",rs.getString("cc_acuse_firma_rep")==null?"":rs.getString("cc_acuse_firma_rep"));
				datosCons.put("df_firma_representante",rs.getString("df_firma_representante")==null?"":rs.getString("df_firma_representante"));
				datosCons.put("DF_EXTINCION_REP",rs.getString("DF_EXTINCION_REP")==null?"":rs.getString("DF_EXTINCION_REP")); //F024-2015
				lstData.add(datosCons);
			}
			rs.close();
			ps.close();

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("No se pudieron obtener los Representantes", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getRepresentantesCEDENTE (S)");
		 }
		 return lstData;
	 }

	/**
	 * Metodo que Captura la Solicitud de Cesion
	 * se guarda con estatus  "Por Solicitar" * 
	 * @throws com.netro.exception.AppException
	 * @return 
	 * @param contrato
	 * @param campo5
	 * @param campo4
	 * @param campo3
	 * @param campo2
	 * @param campo1
	 * @param fVigencia_Fin
	 * @param fVigencia_Ini
	 * @param contratacion
	 * @param moneda
	 * @param monto
	 * @param noContrato
	 * @param clasificacionEPO
	 * @param noIF
	 * @param noEpo
	 */
	public String insertSolicitud(String noEpo, String noIF, String clasificacionEPO,
											String noContrato,String monto, String moneda,
											String contratacion, String fVigencia_Ini, String fVigencia_Fin,
											String campo1,	String campo2, String campo3, String campo4, 
											String  campo5,String contrato, String pyme )	throws AppException {	
											
			log.info(":insertSolicitud (E)");
			
			log.debug("***********************************************************************************");
			log.debug("noEpo: "+noEpo+"  noIF  : "+noIF+ "  clasificacionEPO: "+clasificacionEPO);
			log.debug("noContrato: "+noContrato+"  monto  : "+monto+ "  moneda: "+moneda);
			log.debug("contratacion: "+contratacion+"  fVigencia_Ini  : "+fVigencia_Ini+ "  fVigencia_Fin: "+fVigencia_Fin);
			log.debug("campo1: "+campo1+"  campo2  : "+campo2+ "  campo3: "+campo3);
			log.debug("campo4: "+campo4+"  campo4  : "+campo4+ "  campo4: "+campo4);
			log.debug("contrato: "+contrato+"  contrato  : "+contrato+ "  contrato: "+contrato);
			log.debug("***********************************************************************************");
			
	
			AccesoDB con  = new AccesoDB();
		
			PreparedStatement ps = null;
			PreparedStatement ps1 = null;		
			ResultSet  rs1 = null;
			ResultSet  rs = null;
			boolean   commit = true;
			String qrySQL = "" ;
			String qry = "";
			List lvarbind = new ArrayList();
			String valor = "";
			int seqNEXTVAL	= 0;
			String estatus = "1";
			int tamanio = 0;
			String noContrato1 =noContrato.toUpperCase();
			try {
			
					con.conexionDB();
				
			qry = "SELECT seq_cder_solicitud.nextval FROM dual";
			ps1 = con.queryPrecompilado(qry);
			rs1 = ps1.executeQuery();
				if(rs1.next())
				{
					seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
				}
			ps1.close();
   		rs1.close();

			
			qrySQL =  " INSERT INTO cder_solicitud (ic_solicitud, ic_epo, ic_if, ic_clasificacion_epo, "+
						  " cg_no_contrato, fn_monto_contrato, ic_moneda, ic_tipo_contratacion,"+
						  " df_fecha_vigencia_Ini, df_fecha_vigencia_Fin, cg_campo1, cg_campo2, "+
						  " cg_campo3, cg_campo4, cg_campo5, cg_obj_contrato, "+
						  " ic_estatus_solic, ic_pyme) "+
						" values(" + seqNEXTVAL + ",'" + noEpo + "','" + noIF + "','" + clasificacionEPO + "','" + 
						 noContrato1 + "','" + monto + "','" +moneda + "','" + contratacion + "', TO_DATE('" + fVigencia_Ini +"','DD/MM/YYYY')," +
						" TO_DATE('" + fVigencia_Fin +"','DD/MM/YYYY'),'"+ campo1 + "','" +campo2 + "','" + campo3 + "','" + campo4 + "','" +
						campo5 + "','" + contrato + "',"+estatus + ","+pyme+")";
						
	
			
 			 con.ejecutaSQL(qrySQL);
			
			
			log.debug("Query::::  "+qrySQL );
			
			valor = Integer.toString (seqNEXTVAL);
					
			} 	catch(Exception e) {
						commit = false;
						log.info("insertSolicitud(Exception) " + e);
						e.printStackTrace();				
				throw new AppException("insertSolicitud(Exception) ", e);
				}
				finally
				{
					con.terminaTransaccion(commit);
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
				}				
				
				log.info("insertSolicitud (S)");
					
		return valor;
		}		
		
	/**
	 * Metodo que Modifica la Solicitud de Cesion
	 * siempre y cuando esta se encuentre en estatus 
	 * Por Solicitar y Solicitud Rechazada
	 * @throws com.netro.exception.AppException
	 * @return 
	 * @param contrato
	 * @param campo5
	 * @param campo4
	 * @param campo3
	 * @param campo2
	 * @param campo1
	 * @param fVigencia_Fin
	 * @param fVigencia_Ini
	 * @param contratacion
	 * @param moneda
	 * @param monto
	 * @param noContrato
	 * @param clasificacionEPO
	 * @param noIF
	 * @param noEpo
	 * @param solicitud
	 */
	
	public String modificaSolicitud(String solicitud, String  noEpo, String noIF, String clasificacionEPO,
											String noContrato,String monto, String moneda,
											String contratacion, String fVigencia_Ini, String fVigencia_Fin,
											String campo1,	String campo2, String campo3, String campo4, 
											String  campo5,String contrato) throws AppException	{
											
			log.info("ModificaSolicitud (E)");
			AccesoDB con  = new AccesoDB();
		
			PreparedStatement ps = null;
			PreparedStatement ps1 = null;
			ResultSet  rs = null;
			boolean   commit = true;
			StringBuffer qrySQL = new StringBuffer();
			String qry = "";
			List lvarbind = new ArrayList();
			String valor = "";
			int seqNEXTVAL				= 0;

			String  estatus = "1";
						
			
			log.debug("***********************************************************************************");
			log.debug("noEpo: "+noEpo+"  noIF  : "+noIF+ "  clasificacionEPO: "+clasificacionEPO);
			log.debug("noContrato: "+noContrato+"  monto  : "+monto+ "  moneda: "+moneda);
			log.debug("contratacion: "+contratacion+"  fVigencia_Ini  : "+fVigencia_Ini+ "  fVigencia_Fin: "+fVigencia_Fin);
			log.debug("campo1: "+campo1+"  campo2  : "+campo2+ "  campo3: "+campo3);
			log.debug("campo4: "+campo4+"  campo4  : "+campo4+ "  campo4: "+campo4);
			log.debug("contrato: "+contrato+"  solicitud   "+solicitud);
			log.debug("***********************************************************************************");
			
	
	
			try {
					con.conexionDB();
			
			qrySQL.append(" UPDATE cder_solicitud ");
			qrySQL.append(" SET ic_epo = " + noEpo + 
								 "  , ic_if = " + noIF + 
								 " , ic_clasificacion_epo = " + clasificacionEPO + 
								 " , cg_no_contrato = '" + noContrato + "'" + 
								 " , fn_monto_contrato = " + monto + 
								 " , ic_moneda = " + moneda + 
								 " , ic_tipo_contratacion = " + contratacion + 
								 " , df_fecha_vigencia_Ini = TO_DATE('" + fVigencia_Ini + "','DD/MM/YYYY') "+	
								 " , df_fecha_vigencia_Fin = TO_DATE('" + fVigencia_Fin + "','DD/MM/YYYY') "+	
								 " , cg_campo1 ='" + campo1 + "'" + 
								 " , cg_campo2 = '" + campo2 + "'" + 
								 " , cg_obj_contrato ='" + contrato + "'" +   
								 " , ic_estatus_solic = " + estatus +" ");
		
		
			if ( campo3 != null ) {
				qrySQL.append(", cg_campo3 = '" + campo3 + "'");					
			}		
			
			if (campo4 != null ) {
				qrySQL.append(", cg_campo4 = '" + campo4 + "'");				
			}
			
			if (campo5 != null  ) {
				qrySQL.append(", cg_campo5 = '" + campo5 + "'");	
				
			}
			
			qrySQL.append("  WHERE ic_solicitud = "+ solicitud);					

						
			ps = con.queryPrecompilado(qrySQL.toString());
			ps.executeUpdate();
			ps.close();
				
			log.debug("ModificaSolicitud :: "+qrySQL.toString()  );
				
			valor = "La Modificacion fue  Exitosamente";	
				
					
			} 	catch(Exception e) {
						commit = false;
						log.error("ModificaSolicitud(Exception) " + e);
						e.printStackTrace();				
					throw new AppException("ModificaSolicitud(Exception) ", e);
				}
				finally
				{
					con.terminaTransaccion(commit);
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
				}
				log.info("ModificaSolicitud (S)");
					
		return solicitud;
		}		
		

/**
	 *Metodo que regresa el registro capturado y lo muestra en el preAcuse
	 * este metodo es usado tanto en Perfil Pyme como en Epo
	 * @throws com.netro.exception.AppException
	 * @return 
	 * @param solicitud
	 */
	 
public List  inicializarPreAcuse (String solicitud)	throws AppException	{

		log.info("inicializarPreAcuse (E)");


			PreparedStatement ps = null;
			ResultSet rs        = null;
			AccesoDB	 con 	     =   new AccesoDB();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			List lvarbind       = new ArrayList();		
			List registro = new ArrayList();
			
			try {
			con.conexionDB();
			  qrySentencia.append(" SELECT e.cg_razon_social as Dependencia, "+
									" i.cg_razon_social as NombreIf,  "+
									" sol.cg_no_contrato as NoContrato,  "+
									" sol.fn_monto_contrato as Monto,  "+
									" m.cd_nombre as Moneda,  "+
									" con.cg_nombre as Contratacion,  "+
									" TO_CHAR(sol.df_fecha_vigencia_Ini,'DD/MM/YYYY') as FContratoIni,  "+
									" TO_CHAR(df_fecha_vigencia_Fin,'DD/MM/YYYY')  as FContratoFin,  "+
									" cl.cg_area as Clasificacion,  "+
									" sol.cg_campo1 as Campo1,  "+
									" sol.cg_campo2 as Campo2,  "+
									" sol.cg_campo3 as Campo3,  "+
									" sol.cg_campo4 as Campo4,  "+
									" sol.cg_campo5 as Campo5,  "+
									" sol.cg_obj_contrato as ObContrato,  "+
									" es.cg_descripcion as Estatus, "+
									" ic_solicitud  as solicitud, "+
									" cc_acuse1 as Acuse ");
			
			qrySentencia.append(" FROM  cder_solicitud  sol, comcat_epo e, comcat_if i, cdercat_clasificacion_epo cl, "+
									" cdercat_tipo_contratacion con, comcat_moneda m, cdercat_estatus_solic es ");
	
	
			qrySentencia.append(" WHERE  sol.ic_epo =e.ic_epo "+
									" AND sol.ic_if = i.ic_if "+
									" AND sol.ic_clasificacion_epo  = cl.ic_clasificacion_epo "+
									" AND sol.ic_moneda = m.ic_moneda "+
									" AND SOL.ic_tipo_contratacion = CON.ic_tipo_contratacion "+
									" AND SOL.ic_estatus_solic = ES.ic_estatus_solic ");
			
			qrySentencia.append(" and ic_solicitud = ? ");
					
			lvarbind.add(solicitud);					
			registro = con.consultaDB(qrySentencia.toString(),lvarbind, false);	
			
			log.debug("inicializarPreAcuse "+ qrySentencia.toString()+ lvarbind);
			log.debug("inicializarPreAcuse "+ registro);
			
	
		}catch(Exception e){
			log.error("inicializarPreAcuse (Exception) " + e);			
			throw new AppException("inicializarPreAcuse(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("inicializarPreAcuse (E)");
		
		return registro;
	
}

	/**
	 * metodo que elimina la solicitud de consentimiento de cesion  
	 * siempre y cuando se encuentre en estatus de Por Solicitar
	 * @throws com.netro.exception.AppException
	 * @param solicitud
	 */
	public void  eliminaSolicitud (String solicitud ) throws AppException	{
		log.info("EliminaSolicitud (E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		
		try {
			con.conexionDB();

			strSQL.append(" DELETE cder_monto_x_solic WHERE ic_solicitud  = ?");
			varBind.add(new Long(solicitud));
			
			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: "+varBind);
		
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" DELETE cder_solic_x_representante WHERE ic_solicitud  = ?");
			varBind.add(new Long(solicitud));

			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" DELETE CDER_SOLIC_PYME_NOTARIALES WHERE ic_solicitud  = ?");
			varBind.add(new Long(solicitud));

			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" DELETE cder_solicitud WHERE ic_solicitud  = ?");
			varBind.add(new Long(solicitud));
			
			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: "+varBind);
		
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
		} catch(Exception e) {
			transactionOk = false;
			log.error("EliminaSolicitud(Exception) " + e);
			e.printStackTrace();
			throw new AppException("EliminaSolicitud(Exception) ", e);
		} finally  {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
		}
		log.info("EliminaSolicitud (E) ::..");
	}

	
	/**
	 * Metodo que EnviaSolicitud  la Solicitud de Cesion  
	 * cuando esta en proceso de "En Proceso" y muestra acuse de
	 * la solicitud
	 *  @throws com.netro.exception.AppException
	 * @return 
	 * @param solicitud
	 */
	

public String enviaSolicitud(String solicitud, String serial, String textoFirmado, String pkcs7, String folio, boolean validCert, String usuario, String nombreUsuario)	throws AppException	{
											
			log.info("enviaSolicitud (E)");
			
			AccesoDB con  = new AccesoDB();
		
			PreparedStatement ps = null;
			PreparedStatement ps1 = null;
			ResultSet  rs = null;
			ResultSet  rs1= null;
			boolean   commit = true;
			StringBuffer qrySQL = new StringBuffer();			
			String qrySQLAcuse = "";
			List lvarbind = new ArrayList();		
		
			String  estatus = "2"; // En Proceso
			String  acuse = "";
			String mostrarError ="";
			int seqNEXTVAL	= 0;
			
			log.debug("***********************************************************************************");
			log.debug("pkcs7: "+pkcs7);	
			log.debug("textoFirmado: "+textoFirmado);				
			log.debug("serial: "+serial);				
			log.debug("folio: "+folio);
			log.debug("solicitud : "+solicitud);
			log.debug("usuario: "+usuario);	
			log.debug("nombreUsuario: "+nombreUsuario);	
			log.debug("***********************************************************************************");
			
			try {
				
				con.conexionDB();
				
						String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
							if(rs1.next())
							{
								seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
							}
						ps1.close();
						rs1.close();
			
			
				
				char getReceipt = 'Y';
				if ( !serial.equals("") && !textoFirmado.equals("") && !pkcs7.equals("") ) {
			
				Seguridad s = new Seguridad();
		
			  if ( s.autenticar(folio, serial, pkcs7, textoFirmado,getReceipt)  ) {
		
					acuse = s.getAcuse();
			
					qrySQL.append(" UPDATE cder_solicitud SET ic_estatus_solic = ?, cc_acuse1 = ? , df_fecha_Solicitud_Ini = SYSDATE  WHERE ic_solicitud  = ? ");
					
					
					lvarbind.add(estatus);
					lvarbind.add(new Integer(seqNEXTVAL));		
					lvarbind.add(solicitud);
					
					ps = con.queryPrecompilado(qrySQL.toString(),lvarbind);
					
					ps.executeUpdate();
					ps.close();
		
					log.debug("EnviaSolicitud :: "+qrySQL.toString() + "   "+lvarbind  );							
					log.debug("EnviaSolicitud :: "+acuse);	
					
					
					// Inserta el Numero Acuse 	
							qrySQLAcuse =  " INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo, df_operacion) "+
												" 	VALUES('" + seqNEXTVAL + "','" + usuario + "','" + nombreUsuario + "','"+ acuse + "',SYSDATE) ";
														
							
							con.ejecutaSQL(qrySQLAcuse);
					
					log.debug("EnviaSolicitud :: "+qrySQLAcuse);	
					}else {
					
					mostrarError = 	s.mostrarError();
					
					log.info("EnviaSolicitud :: "+mostrarError);		
					}
			}
		
		
					
			} 	catch(Exception e) {
						commit = false;
						log.error("EnviaSolicitud(Exception) " + e);
						e.printStackTrace();				
					throw new AppException("EnviaSolicitud(Exception) ", e);
				}
				finally
				{
					con.terminaTransaccion(commit);
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
				}
				log.info("EnviaSolicitud (S)");
					
		return acuse;
		}		
		
	/**
	 * metodo para verifivar si existe el numer de contrato
	 * Quita todos los signos raros que puede capturar el usuario 
	 * y solo verifica letras y numeros.
	 * @throws com.netro.exception.AppException
	 * @return el numero de contrato
	 * @param noContrato
	 */
	public boolean existeNoContrato(String numeroContrato, String claveEpo, String claveIfCesionario, String clavePyme, String numeroSolicitud) throws AppException	{
		log.info("existeNoContrato (E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean existeContrato = false;
		int solicitudes = 0;

		try {		
			con.conexionDB();

			numeroContrato = numeroContrato.replaceAll("[-|&|*|:|.|'|{|}|(|)|=|;|'|/|%|$|#|�|!|@|+|<|>|^|`|~|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|�|/]", "");

			strSQL.append(" SELECT COUNT(s.ic_solicitud) AS solicitudes");
			strSQL.append(" FROM cder_solicitud s");
			strSQL.append(" WHERE s.cg_no_contrato = ?");
			strSQL.append(" AND s.ic_epo = ?");
			strSQL.append(" AND s.ic_if = ?");
			strSQL.append(" AND s.ic_pyme = ?");

			varBind.add(numeroContrato.toUpperCase());
			varBind.add(new Integer(claveEpo));
			varBind.add(new Integer(claveIfCesionario));
			varBind.add(new Integer(clavePyme));

			if (!numeroSolicitud.equals("")) {
				strSQL.append(" AND s.ic_solicitud != ?");
				varBind.add(new Integer(numeroSolicitud));
			}

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			ps = con.queryPrecompilado(strSQL.toString(), varBind);
			rs = ps.executeQuery();

			if (rs.next()) {
				solicitudes = rs.getInt("solicitudes");
			}

			if (solicitudes > 0) {
				existeContrato = true;
			}
			
			rs.close();
			ps.close();
		} catch(Exception e) {
			log.error("existeNoContrato(ERROR) ::..");
			throw new AppException("existeNoContrato(Exception)", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("existeNoContrato(S) ::..");
		}
		return existeContrato;
	}

	/**
	 * metodo que obtiene los campos adicionales obtenidos  para la epo
	 * deacuerdo a la parametrizacion de la epo
	 * @throws com.netro.exception.AppException
	 * @return 
	 * @param con
	 * @param sNoCliente
	 */	
	
public Vector getCamposAdicionales(String sNoCliente, String campos) throws AppException {
	
	log.info("getCamposAdicionales :: (E)");	
	
	Vector vCamposAdcionales = new Vector();
	Vector vca = null;
	AccesoDB con  = new AccesoDB();
	ResultSet rs = null;
	PreparedStatement ps = null;	
	boolean   commit = true;
	String sNoCliente1 = "";
	
	if(sNoCliente.equals("") ){
		sNoCliente1 ="0";
	}else {
		sNoCliente1 = sNoCliente;
	}
	
	
		try{
		
		con.conexionDB();	
		
		
			String query = "select ic_no_campo, initcap(cg_nombre_campo), "+
							" cg_tipo_dato, ig_longitud, cs_obligatorio "+
							" from comrel_visor "	+						
							" where ic_epo = "+sNoCliente1+
							" and ic_producto_nafin = 9"+							
							" and ic_no_campo between 1 and "+campos+
							" order by ic_no_campo";			
				
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
	
			
				while (rs.next()) {
					vca = new Vector();
					vca.add(rs.getString(1));
					vca.add(rs.getString(2).trim());
					vca.add(rs.getString(3).trim());
					vca.add(rs.getString(4));
					vca.add(rs.getString(5));
					vCamposAdcionales.addElement(vca);
				}
			
				rs.close();		
				ps.close();		
				
			
			log.debug("query getCamposAdicionales :: "+query);	
			log.debug(" vCamposAdcionales :: "+vCamposAdcionales);	
			
		} 	catch(Exception e) {
				commit = false;
			   log.info("getCamposAdicionales(Exception) " + e);
				e.printStackTrace();			
			throw new AppException("getCamposAdicionales(Exception) ", e);
		}
		finally
			{
				con.terminaTransaccion(commit);
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
			}
				
		log.info("getCamposAdicionales :: (S)");			
	return vCamposAdcionales;

	
	}
	
		
/**
	 *Metodo que cambia el estatus de la solicitus a 
	 * Envia = Solicitud Aceptada 
	 * Reactivar = Solicitud Reactivada
	 * cuando la epo la acepta 
	 * @throws com.netro.exception.AppException
	 * @return 
	 * @param solicitud
	 */
public String solicitudEnviActivarReactivar(String solicitud, String cambiaEstatus, String causaRechazo)	throws AppException	{
											
			log.info("solicitudactivarReactivar (E)");
			
			AccesoDB con  = new AccesoDB();
		
			PreparedStatement ps = null;
			PreparedStatement ps1 = null;
			ResultSet  rs = null;
			boolean   commit = true;
			StringBuffer qrySQL = new StringBuffer();			
			String qrySQLAcuse = "";
			List lvarbind = new ArrayList();		
			String valor = "S";
			
			log.debug("***********************************************************************************");			
			log.debug("solicitud: "+solicitud);	
			log.debug("***********************************************************************************");
			
			try {
				
				con.conexionDB();				
						
					qrySQL.append(" UPDATE cder_solicitud ");
					qrySQL.append(" SET ic_estatus_solic = ?,  ");
					qrySQL.append(" CG_CAUSAS_RECHAZO = ? ");
					
					if (cambiaEstatus.equals("3") || cambiaEstatus.equals("5")  ) { // solo cuando el estatus sea Solicitud Aceptada						
						qrySQL.append(" ,df_fecha_aceptacion = SYSDATE ");						
					}
					
					 qrySQL.append(" WHERE ic_solicitud  = ?  ");			
					
					lvarbind.add(cambiaEstatus);	
					lvarbind.add(causaRechazo);					
					lvarbind.add(solicitud);
					
					ps = con.queryPrecompilado(qrySQL.toString(),lvarbind);
					
					ps.executeUpdate();
					ps.close();
		
					log.debug("solicitudactivarReactivar :: "+qrySQL.toString() + "   "+lvarbind  );							
						
					
			} 	catch(Exception e) {
						commit = false;
						log.error("solicitudactivarReactivar(Exception) " + e);
						e.printStackTrace();
					throw new AppException("solicitudactivarReactivar(Exception) ", e);
				}
				finally
				{
					con.terminaTransaccion(commit);
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
				}
				log.info("solicitudactivarReactivar (S)");
					
		return valor;
		}		
		

		

		/**
			 * Metodo que regresa el registro de la consulta cuando el estatus cambia
			 * y lo muestra en el preAcuse	 
			 * @throws com.netro.exception.AppException
			 * @return 
			 * @param solicitud
			 */
			 
		public List  inicializarPreAcuseActivaReactiva (String solicitud)	throws AppException	{
            log.info("CesionEJBBean::inicializarPreAcuseActivaReactiva(E)");
            PreparedStatement ps = null;
            ResultSet rs        = null;
            AccesoDB	 con 	     =   new AccesoDB();
            boolean	 resultado = true;
            StringBuffer qrySentencia = new StringBuffer();
            List lvarbind       = new ArrayList();		
            List registro = new ArrayList();            
            try {
                con.conexionDB();            
                qrySentencia.append(
                    " SELECT   i.cg_razon_social as Cecionario,  "+
                    "  p.CG_RAZON_SOCIAL, "+
                    "  p.CG_RFC  as Rfc, "+ 
                    "  p.IC_PYME as NoProveedor, "+   
                    " TO_CHAR(df_fecha_Solicitud_Ini,'DD/MM/YYYY')  as FSolicitudIni,  "+
                    " sol.cg_no_contrato as NoContrato,  "+
                    " sol.fn_monto_contrato as Monto,  "+
                    " sol.ic_moneda as Moneda,  "+
                    " con.cg_nombre as Contratacion,  "+
                    " DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS FContratoIni,"+
                    " DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS FContratoFin,"+
                    " cl.cg_area as Clasificacion,  "+
                    " sol.cg_campo1 as Campo1,  "+
                    " sol.cg_campo2 as Campo2,  "+
                    " sol.cg_campo3 as Campo3,  "+//15
                    " sol.cg_campo4 as Campo4,  "+
                    " sol.cg_campo5 as Campo5,  "+  
                    " sol.cg_obj_contrato as ObContrato,  "+                                
                    " TO_CHAR(sol.df_fecha_limite_firma,'DD/MM/YYYY') as FLimite,  "+
                    " es.cg_descripcion as Estatus , "+
                    " sol.ic_solicitud as Solicitud, "+                                     
                    " sol.ic_estatus_solic as NoEstatus, "+
                    " sol.cc_acuse2 as Acuse, "+														
                    " TO_CHAR(sol.df_fecha_aceptacion,'DD/MM/YYYY') as FAceptacion,  "+
                    " sol.cg_comentarios AS comentarios " +
                    ", DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato"+
                    ", cvp.cg_descripcion AS venanilla_pago,"+
                    " sol.cg_sup_adm_resob AS sup_adm_resob,"+
                    " sol.cg_numero_telefono AS numero_telefono,"+
                    " TO_CHAR(sol.df_alta_solicitud, 'dd/mm/yyyy') as fecha_solicitud, "+//30
                    " e.cg_razon_social AS Dependencia, "+
                    " TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato, "+
                    " sol.CG_EMPRESAS_REPRESENTADAS,  "+
                    " sol.IC_GRUPO_CESION as GRUPO "+                    
                    " FROM  cder_solicitud  sol, comcat_epo e, comcat_if i, cdercat_clasificacion_epo cl, "+
                    " cdercat_tipo_contratacion con, cdercat_estatus_solic es,"+
                    " cdercat_tipo_plazo stp"+
                    ", cdercat_ventanilla_pago cvp"+
                    ", COMCAT_PYME p  WHERE  sol.ic_epo =e.ic_epo "+
                    " AND sol.ic_if = i.ic_if "+
                    " AND sol.ic_clasificacion_epo  = cl.ic_clasificacion_epo "+
                    " AND SOL.ic_tipo_contratacion = CON.ic_tipo_contratacion "+
                    " AND SOL.ic_estatus_solic = ES.ic_estatus_solic "+
                    " AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)"+
                    " AND sol.ic_epo = cvp.ic_epo(+)"+
                    " AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)"+
                    " AND sol.IC_PYME = p.IC_PYME ");                
                qrySentencia.append(" and ic_solicitud = ? ");
                lvarbind.add(solicitud);
                registro = con.consultaDB(qrySentencia.toString(),lvarbind, false);                
                log.debug("inicializarPreAcuseActivaReactiva "+ qrySentencia.toString()+ lvarbind);
                log.debug("inicializarPreAcuseActivaReactiva "+ registro);
            } catch(Exception e) {
                log.error("CesionEJBBean::inicializarPreAcuseActivaReactiva (Exception) " + e);
                throw new AppException("CesionEJBBean::inicializarPreAcuseActivaReactiva(Exception) ", e);
            } finally {
                if(con.hayConexionAbierta()){
                    con.terminaTransaccion(resultado);
                    con.cierraConexionDB();
                }
            }
            log.info("CesionEJBBean::inicializarPreAcuseActivaReactiva(S)");
            return registro;
		}


	/**
		 * Metodo que Transmite la  Solicitud de Cesion  
		 * cuando esta en proceso de "En Proceso" y muestra acuse de
		 * la solicitud
		 * y guarda un segundo acuse en la tabla de cder_acuse
		 *  @throws com.netro.exception.AppException
		 * @return 
		 * @param solicitud
		 */
		
	
	public String transmitesolicitud(String solicitud, String serial, String textoFirmado, String pkcs7, String folio, boolean validCert, String usuario, String nombreUsuario, String estatus, String causasRechazo,String strDirectorioTemp)	throws AppException	{
												
				log.info("transmitesolicitud (E)");
				
				AccesoDB con  = new AccesoDB();
			
				PreparedStatement ps = null;
				PreparedStatement ps1 = null;
				ResultSet  rs = null;
				ResultSet  rs1 = null;
				boolean   commit = true;
				StringBuffer qrySQL = new StringBuffer();			
				StringBuffer qrySQLAcuse = new StringBuffer();	
				StringBuffer qrySQLArchivo = new StringBuffer();
				List lvarbind = new ArrayList();		
				List lvarbind1 = new ArrayList();	
				int seqNEXTVAL	= 0;
				PreparedStatement ps2 = null;
				ResultSet  rs2 = null;
				String  acuse = "";				
				String mostrarError = "";
				
				log.debug("***********************************************************************************");
				log.debug("pkcs7: "+pkcs7);	
				log.debug("textoFirmado: "+textoFirmado);				
				log.debug("serial: "+serial);					
				log.debug("folio: "+folio);
				log.debug("solicitud : "+solicitud);
				log.debug("usuario: "+usuario);	
				log.debug("nombreUsuario: "+nombreUsuario);	
				log.debug("estatus: "+estatus);					
				log.debug("***********************************************************************************");
				
				try {
					
					con.conexionDB();
					
						String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
							if(rs1.next())
							{
								seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
							}
						ps1.close();
						rs1.close();
						
						
					
					char getReceipt = 'Y';
				if ( !serial.equals("") && !textoFirmado.equals("") && !pkcs7.equals("") ) {
				
					
					Seguridad s = new Seguridad();
			
					  if (s.autenticar(folio, serial, pkcs7, textoFirmado,getReceipt) ) {
				
							acuse = s.getAcuse();
					
							qrySQL.append(" UPDATE cder_solicitud SET ic_estatus_solic = ?, cc_acuse2 = ? ");
							
						if (estatus.equals("3")   ) { // solo cuando el estatus sea Solicitud Aceptada						
								qrySQL.append(" , df_fecha_aceptacion = SYSDATE ");//,BI_CONSENTIMIENTO = empty_blob() ");
								qrySQL.append(" , DF_FECHA_PRORROGA = SYSDATE "); // F023-2015  
							}
							if (estatus.equals("5")){
								qrySQL.append(", CG_CAUSAS_RECHAZO = ? ");
							}
						
							qrySQL.append(" WHERE ic_solicitud  = ? ");
						
							lvarbind.add(estatus);
							lvarbind.add(new Integer(seqNEXTVAL));
													
							if(estatus.equals("5")){
								lvarbind.add(causasRechazo);
							}
							lvarbind.add(solicitud);
							
							ps = con.queryPrecompilado(qrySQL.toString(),lvarbind);
							
							ps.executeUpdate();
							ps.close();
							
							if(estatus.equals("3")){
								File archivoPDF = new File(strDirectorioTemp+obtenerConsentimiento(solicitud,nombreUsuario, strDirectorioTemp));
								FileInputStream fileinputstream = new FileInputStream(archivoPDF);
								qrySQLArchivo.append("UPDATE cder_solicitud SET BI_CONSENTIMIENTO = ?, CS_BANDERA_CONSENTIMIENTO = 'S'  WHERE ic_solicitud = ?");
								ps = con.queryPrecompilado(qrySQLArchivo.toString());
								ps.setBinaryStream(1, fileinputstream, (int)archivoPDF.length());
								ps.setInt(2, Integer.parseInt(solicitud));
								ps.executeUpdate();
								ps.close();
								fileinputstream.close();
							}
							
							log.debug("::transmitesolicitud :: "+qrySQL.toString() + "   "+lvarbind  );							
							log.debug("::transmitesolicitud :: "+acuse);	
							
							// Inserta el Numero Acuse 							
								qrySQLAcuse.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo, df_operacion) ");
								qrySQLAcuse.append(" VALUES (?, ?, ?, ?, SYSDATE)");
												
							
							lvarbind1.add(new Integer(seqNEXTVAL));
							lvarbind1.add(usuario);
							lvarbind1.add(nombreUsuario);
							lvarbind1.add(acuse);
												
							ps2 = con.queryPrecompilado(qrySQLAcuse.toString(),lvarbind1);
							
							ps2.executeUpdate();
							ps2.close();							
							
				
						 log.debug("::transmitesolicitud :: "+qrySQLAcuse.toString() + "   "+lvarbind1  );							
							
							
					}else {
						mostrarError = 	s.mostrarError();
						log.info("EnviaSolicitud :: "+mostrarError);		
					}
				}
			
						
				} 	catch(Exception e) {
							commit = false;
							log.error("transmitesolicitud(Exception) " + e);
							e.printStackTrace();
						throw new AppException("transmitesolicitud(Exception) ", e);
					}
					finally
					{
						con.terminaTransaccion(commit);
						if(con.hayConexionAbierta())
							con.cierraConexionDB();
					}
					log.info("transmitesolicitud (S)");
						
			return acuse;
			}		
			


/**
	 *metodo para obtener el monitor de Cesion de Derechos 
	 * de los perfiles de Pyme, Epo, If y Nafin
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param usuarios
	 * @param tipousuario
	 */
		public List  monitorCesionDerechos(String tipousuario, String usuarios) throws AppException {
		log.debug("monitorCesionDerechos ::(E)");

		boolean	 resultado = true;
		AccesoDB	 con 	     =   new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();

		List clavesEstatus = new ArrayList();
      List totalEstatus= new ArrayList();

		PreparedStatement ps = null;
		ResultSet rs = null;

		String clavesEstatus1 = "";
		String estatus1 = "";

		try {

			con.conexionDB();


			 strSQL.append(" SELECT DISTINCT s.ic_estatus_solic as clavesEstatus, " );
			 strSQL.append(" COUNT(*) AS estatus  ");
			 strSQL.append(" FROM cder_solicitud s, cdercat_estatus_solic e  ");
			
		   if (tipousuario.equals("PYME")) {       // si el usuario es Pyme           
				strSQL.append(" ,  (select  distinct   ic_solicitud , ic_pyme   FROM cder_solic_x_representante)  r   ");
			}else  {
				 strSQL.append(" ,  (select  distinct   ic_solicitud    FROM cder_solic_x_representante)  r   ");
			}
			 strSQL.append(" WHERE s.ic_estatus_solic = e.ic_estatus_solic  ");

			 strSQL.append(" and s.ic_solicitud = r.ic_solicitud  ");

			if (tipousuario.equals("PYME")) {		 // si el usuario es Pyme
				strSQL.append(" AND r.ic_pyme = ?  ");
				strSQL.append(" AND s.ic_estatus_solic IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,18,19,20,21,22,23,24,25) ");

				strSQL.append(" GROUP BY s.ic_estatus_solic ");
			 // strSQL.append(" ORDER BY s.ic_estatus_solic    ");
				//Para los grupos
				strSQL.append("   UNION ALL   ");

				strSQL.append(" SELECT  DISTINCT s.ic_estatus_solic as clavesEstatus, " );
				strSQL.append(" COUNT(*) AS estatus  ");
				strSQL.append(" FROM cder_solicitud s, cdercat_estatus_solic e ,  ");
				strSQL.append("  (select  distinct   ic_solicitud            FROM cder_solic_x_representante)  r ");
				strSQL.append(" WHERE s.ic_estatus_solic = e.ic_estatus_solic  ");
				strSQL.append(" and s.ic_solicitud = r.ic_solicitud   ");
				strSQL.append(" AND s.ic_estatus_solic IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,18,19,20,21,22,23,24,25) ");
				strSQL.append(" AND s.ic_grupo_cesion in (  select pg.ic_grupo_cesion    from  CDERREL_PYME_X_GRUPO  pg, CDER_GRUPO g  "+
								  " where pg.ic_grupo_cesion = g.ic_grupo_cesion   and  pg.ic_pyme =  ?    )    ");

				strSQL.append(" GROUP BY s.ic_estatus_solic ");
				strSQL.append(" ORDER BY estatus ");

				varBind.add(new Integer(usuarios));
				varBind.add(new Integer(usuarios));


			} else if  (tipousuario.equals("EPO")) { // Si el Usuaio es Epo

				strSQL.append(" AND s.ic_epo = ? ");
				strSQL.append(" AND s.ic_estatus_solic IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,18,19,20,21,22,23,24,25)");

				varBind.add(new Integer(usuarios));

				strSQL.append(" GROUP BY s.ic_estatus_solic ");
				strSQL.append(" ORDER BY s.ic_estatus_solic ");

			} else if  (tipousuario.equals("IF")) { // Si el Usuaio es IF

				strSQL.append(" AND s.ic_if = ? ");
				strSQL.append(" AND s.ic_estatus_solic IN (3,7,9,10,11,12,15,16,19,20,21,22,23,24,25)");
				varBind.add(new Integer(usuarios));

				strSQL.append(" GROUP BY s.ic_estatus_solic ");
				strSQL.append(" ORDER BY s.ic_estatus_solic ");

			}else if (tipousuario.equals("NAFIN")) {			 //nafin

				strSQL.append(" AND s.ic_estatus_solic IN (1,2,3,4,5,6,7,8,9,10,11,12,15,16,18, 19, 20, 21, 22, 24,25) ");

				strSQL.append(" GROUP BY s.ic_estatus_solic ");
				strSQL.append(" ORDER BY s.ic_estatus_solic ");
			}


		log.debug("strSQL ::"+strSQL+"    varBind  ::  "+ varBind);
			
		ps = con.queryPrecompilado(strSQL.toString(), varBind);
		rs = ps.executeQuery();

		while(rs.next()){

			 clavesEstatus1 = rs.getString("clavesEstatus")==null?"":rs.getString("clavesEstatus");
			 estatus1 = rs.getString("estatus")==null?"":rs.getString("estatus");

			 totalEstatus.add(clavesEstatus1+"e"+estatus1);

			}

			rs.close();
			ps.close();



		}catch(Exception e){
			log.error("monitorCesionDerechos (Exception) " + e);
			throw new AppException("monitorCesionDerechos (Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("monitorPyme :: (S)");
	return totalEstatus;
	}
/**
 	 * Metodo que regresa si la epo Opera o no Solicitud de Consentimiento 
	 * en caso de que opere se mostrara la leyenda de Firmar en Lugar de Enviar 
	 * en la columna de Accion de la Bandeja de la Pyme
	 * cder_acuse
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param sNoEPO
	 */
	public ArrayList getParametrosEPO(String sNoEPO) throws AppException{
	
		 log.info("getParametrosEPO :: (E)");
		 ArrayList alParametros = new ArrayList();
		 
		 
		 boolean	 resultado = true;			
		AccesoDB	 con 	     =   new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;		
		try {
		 	con.conexionDB();
			
			strSQL.append(" SELECT pe.cc_parametro_epo as cc_parametro_epo , pz.cg_valor as cg_valor");
			strSQL.append(" FROM com_parametrizacion_epo pz, comcat_parametro_epo pe ");
			strSQL.append(" WHERE pz.cc_parametro_epo(+) = pe.cc_parametro_epo  ");
			strSQL.append(" AND pe.CC_PARAMETRO_EPO in('OPER_SOLIC_CONS_CDER') ");
			strSQL.append(" AND pz.ic_epo(+) = ? ORDER BY 1 " );
		 
			varBind.add(new Integer(sNoEPO));	
			ps = con.queryPrecompilado(strSQL.toString(), varBind);			
			rs = ps.executeQuery();
		
			while(rs.next()){		
				alParametros.add(0,rs.getString("cg_valor")); // OPER_SOLIC_CONS_CDER				
			}
	
			rs.close();
			ps.close();
			
			log.debug("strSQL::    "+strSQL+"    varBind::    "+varBind);			
			log.debug("alParametros::    "+alParametros);
		
		
		}catch(Exception e){
			log.error("getParametrosEPO (Exception) " + e);			
			throw new AppException("getParametrosEPO (Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("getParametrosEPO :: (S)");
	return alParametros;
	}
	
/**
	 *este metodo tiene como funcion que cuando el usuario
	 * da cancelar en la pantalla de Preacuse  tanto como Pyme y Epo
	 * la Solicitud regresa a su estatus anterior.
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param cambiaEstatus
	 * @param solicitud
	 */
public String cancelaPreacuse(String solicitud, String cambiaEstatus)	 throws AppException{		
	
		log.info("cancelaPreacuse (E)");
			
			AccesoDB con  = new AccesoDB();
		
			PreparedStatement ps = null;
			PreparedStatement ps1 = null;
			ResultSet  rs = null;
			boolean   commit = true;
			StringBuffer qrySQL = new StringBuffer();			
			String qrySQLAcuse = "";
			List lvarbind = new ArrayList();		
			String valor = "S";
			
			log.debug("***********************************************************************************");			
			log.debug("solicitud: "+solicitud);	
			log.debug("cambiaEstatus: "+cambiaEstatus);				
			log.debug("***********************************************************************************");
			
			try {
				
				con.conexionDB();				
						
					qrySQL.append(" UPDATE cder_solicitud SET ic_estatus_solic = ?  ");
					 qrySQL.append(" WHERE ic_solicitud  = ?  ");
			
					
					lvarbind.add(cambiaEstatus);					
					lvarbind.add(solicitud);
					
					ps = con.queryPrecompilado(qrySQL.toString(),lvarbind);
					
					ps.executeUpdate();
					ps.close();
		
					log.debug("cancelaPreacuse :: "+qrySQL.toString() + "   "+lvarbind  );							
						
					
			} 	catch(Exception e) {
						commit = false;
						log.error("cancelaPreacuse(Exception) " + e);
						e.printStackTrace();
					throw new AppException("cancelaPreacuse(Exception) ", e);
				}
				finally
				{
					con.terminaTransaccion(commit);
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
				}
				log.info("cancelaPreacuse (S)");
					
		return valor;
		}			
	


	public String clasificacionEpo(String sNoEPO)  throws AppException{		

		 log.info("clasificacionEpo :: (E)");
		 ArrayList alParametros = new ArrayList();


			 boolean	 resultado = true;			
			AccesoDB	 con 	     =   new AccesoDB();
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			PreparedStatement ps = null;
			ResultSet rs = null;		
			String clasificacionEpo = "";
			String  sNoEPO2 = "";
		try {
		 	con.conexionDB();
			
			if (sNoEPO.equals("")){				
				sNoEPO2 = "0";
			}else {
			
				sNoEPO2 = sNoEPO;
			}
			
			strSQL.append(" Select cg_clasificacion_epo as Clasificacion from comcat_epo  where ic_epo = ? " );
		 
			varBind.add(new Integer(sNoEPO2));	
			ps = con.queryPrecompilado(strSQL.toString(), varBind);			
			rs = ps.executeQuery();
		
			while(rs.next()){		
				clasificacionEpo = rs.getString("Clasificacion"); 			
			}	
			rs.close();
			ps.close();
			
			log.debug("strSQL::    "+strSQL+"    varBind::    "+varBind);			
			log.debug("clasificacionEpo::    "+clasificacionEpo);
		
		
		}catch(Exception e){
			log.error("clasificacionEpo (Exception) " + e);			
			throw new AppException("clasificacionEpo (Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("clasificacionEpo :: (S)");
	return clasificacionEpo;
	}
	
	
	


// ������������������������ IA  FODEA 037 - 2009 (I) ���������������������������
	/**
	 * M�todo que inserta el Clausulado del Contrato (ADMIN IF)
	 * @author  Ivan Almaguer
	 * @param  clausulado	cadena de texto capturada por el usuario
	 * @param  clave_if
	 * @return nuevoClausulado
	 * @since  FODEA 037 - 2009 Cesi�n de Derechos
	 * @throws AppException lanzada cuando ocurre un error.
	 */
	 public List guardaClausulado(String clausulado, String clave_if) throws AppException{
		 AccesoDB con = new AccesoDB();
		 PreparedStatement pst = null;
		 ResultSet rst = null;
		 boolean bcommit = true;
		 List nuevoClausulado = new ArrayList();
		 List varBind = new ArrayList();
		 StringBuffer qrySQL = new StringBuffer();
		 boolean bandera = false;
     
		 try{
			 log.info("guardaClausulado (E)");
			 con.conexionDB();
				        
				qrySQL.append("SELECT ic_if FROM cdercat_clausulado WHERE ic_if = ? ");
				varBind.add(clave_if);
				pst = con.queryPrecompilado(qrySQL.toString(), varBind);
				rst = pst.executeQuery();
				
				if(rst.next()){
					bandera = true;
				}
				pst.close();
								
				if(bandera){
          varBind = new ArrayList();
					qrySQL = new StringBuffer();
          qrySQL.append(" UPDATE cdercat_clausulado set lt_clausulado_cesion = ? WHERE ic_if = ? ");
          varBind.add(clausulado);
          varBind.add(clave_if);
          
          pst = con.queryPrecompilado(qrySQL.toString(), varBind);
          pst.executeUpdate();
          pst.close();
        }else{
          varBind = new ArrayList();
          qrySQL = new StringBuffer();
          qrySQL.append(" INSERT INTO cdercat_clausulado ");
          qrySQL.append("             (ic_clausulado, ic_if, lt_clausulado_cesion ");
          qrySQL.append("             ) ");
          qrySQL.append("      VALUES (SEQ_CDER_CLAUSULADO.NEXTVAL, ?, ? ");
          qrySQL.append("             ) ");
          varBind.add(clave_if);
          varBind.add(clausulado);
          
          pst = con.queryPrecompilado(qrySQL.toString(), varBind);
          pst.executeUpdate();
          pst.close();                  
        }
								
		 }catch(Exception e){
			 bcommit = false;
			 e.printStackTrace();	
			 throw new AppException("No se pudo guardar el clausulado");
			 
		 }finally{
			 nuevoClausulado.add("success");
			 if(con.hayConexionAbierta()){
				 con.terminaTransaccion(bcommit);
				 con.cierraConexionDB();
			 }
			 log.info("guardaClausulado (S)");
		 }
		 return nuevoClausulado;
	 }
	   
	 /**
	 * M�todo que obtiene el loggin y nombre de los usuarios para crear el Combo de la p�gina Asignaci�n de Testigos
	 * @author  Ivan Almaguer
	 * @param  tipoAfiliado. Se le asigna por default "I" (IF)
	 * @param  claveAfiliado. Clave del IF que se loggea
	 * @param  perfil. Perfil al que esta asociado el IF
	 * @return catalogoTestigo
	 * @since  FODEA 037 - 2009 Cesi�n de Derechos
	 * @throws AppException lanzada cuando ocurre un error.
	 */
	 public ArrayList getComboTestigos(String tipoAfiliado, String claveAfiliado, String perfil, String strLogin ) throws AppException{
		ArrayList 	listaUsuarios 	 = new ArrayList();
		UtilUsr 		oUtilUsr 		 = new UtilUsr();
		UtilUsr 		utilUsr 			 = new UtilUsr();
		ArrayList   catalogoTestigo = new ArrayList();
		
		log.info("getComboTestigos (E)");
		try {
		
		String  loginIFFirmado = getloginRepresentante2(claveAfiliado);
		log.info("loginIFFirmado ====="+loginIFFirmado);
		
		List cuentas = utilUsr.getUsuariosxAfiliado( claveAfiliado, tipoAfiliado);
		
		Iterator itCuentas = cuentas.iterator();
		while (itCuentas.hasNext()) {
			netropology.utilerias.usuarios.Usuario oUsuario = null;
			String login = (String) itCuentas.next();
			oUsuario = oUtilUsr.getUsuario(login);
			if( "".equals(perfil)) {		
				listaUsuarios.add(oUsuario);
			} else {
				if(perfil.equals(oUsuario.getPerfil())){
					listaUsuarios.add(oUsuario);
					}
				}
			}
			for (int indice=0; indice < listaUsuarios.size(); indice++){
				netropology.utilerias.usuarios.Usuario oUsuario = (Usuario)listaUsuarios.get(indice);
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();	
				
				if(!loginIFFirmado.equals("") && loginIFFirmado.equals(strLogin)  )  { 
					elementoCatalogo.setClave(oUsuario.getLogin());
					elementoCatalogo.setDescripcion(oUsuario.getLogin()+" - "+oUsuario.getNombre()+" "+oUsuario.getApellidoPaterno()+" "+oUsuario.getApellidoMaterno());					
					catalogoTestigo.add(elementoCatalogo);
				}else  {
					if(  !oUsuario.getLogin().equals(strLogin)  )  {
						elementoCatalogo.setClave(oUsuario.getLogin());
						elementoCatalogo.setDescripcion(oUsuario.getLogin()+" - "+oUsuario.getNombre()+" "+oUsuario.getApellidoPaterno()+" "+oUsuario.getApellidoMaterno());					
						catalogoTestigo.add(elementoCatalogo);					
					}
				}				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("No se pudo obtener el catalogo");
		}
		log.info("getComboTestigos (S)");
		Collections.sort(listaUsuarios);
		return catalogoTestigo;
	 }
	 
	 /**
	 * M�todo que guarda los Testigos que firmaran el contrato de cesion de derechos.
	 * @author  Ivan Almaguer
	 * @param  clave_if
	 * @param  testigo1
	 * @param  testigo2
	 * @return lTestigos
	 * @since  FODEA 037 - 2009 Cesi�n de Derechos
	 * @throws AppException lanzada cuando ocurre un error.
	 */
	 public List guardaTestigos(String clave_if, String testigo) throws AppException{
		 AccesoDB con = new AccesoDB();
		 PreparedStatement pst = null;
		 ResultSet rst = null;
		 boolean bcommit = true;
		 boolean bandera = false;
		 List lTestigos = new ArrayList();
		 List varBind = new ArrayList();
		 StringBuffer qrySQL = new StringBuffer();
		 int reg_total = 0;
		 String test = "";
		 
		 
		 try{
			 log.info("guardaTestigos (E)");
			 con.conexionDB();
				
				//revisa si esta ya esta insertado el registro
				qrySQL.append(	" 	 SELECT COUNT(1) AS contador, CG_TESTIGO AS testigo " +
									"     FROM CDERCAT_TESTIGO_IF  " +
									"    WHERE IC_IF = ?  " +
									"      AND CG_TESTIGO = ?  "+
									" GROUP BY CG_TESTIGO ");
				varBind.add(clave_if);
				varBind.add(testigo);
				pst = con.queryPrecompilado(qrySQL.toString(), varBind);
				rst = pst.executeQuery();
				
				if(rst.next()){
					reg_total = rst.getInt(1);
					test = rst.getString(2);
					if(reg_total > 0 && !test.equals(testigo)){
					bandera = true;
					}
				}
				pst.close();
				
				//si ya hay registro entonces solamente se actualiza
				if(bandera){
					varBind = new ArrayList();
					qrySQL = new StringBuffer();
					qrySQL.append( " UPDATE CDERCAT_TESTIGO_IF SET "+
										" CG_TESTIGO = ? " +
										" WHERE IC_IF = ?  ");
						varBind.add(testigo.replaceAll(","," "));
						varBind.add(clave_if);
						log.debug("qrySQL ::: "+qrySQL);
						log.debug("varBind ::: "+varBind);
						pst = con.queryPrecompilado(qrySQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
				}else{ // sino hay registro se inserta
					varBind = new ArrayList();
					qrySQL = new StringBuffer();
					
					if(!test.equals(testigo)){
					qrySQL.append( " INSERT INTO CDERCAT_TESTIGO_IF ( "+
															"	IC_TESTIGO_IF, "+
															"	IC_IF, "+	 
															"	CG_TESTIGO	) "+
									 " VALUES ((select nvl(max(ic_testigo_if), 0) + 1 from cdercat_testigo_if),?,?) ");
				
						varBind.add(clave_if);
						varBind.add(testigo.replaceAll(","," "));
						log.debug("qrySQL ::: "+qrySQL);
						log.debug("varBind ::: "+varBind);
						pst = con.queryPrecompilado(qrySQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
						lTestigos.add("Success");
					}else{
					lTestigos.add("Failure");
					}
				}	
		 }catch(Exception e){
			 bcommit = false;
			 e.printStackTrace();
			 throw new AppException(" No se pudieron guardar los Testigos");

		 }finally{
			log.debug("lTestigos :: "+lTestigos);	
			 if(con.hayConexionAbierta()){
				 con.terminaTransaccion(bcommit);
				 con.cierraConexionDB();
			 }
			 log.info("guardaTestigos (S)");
		 }
		 return lTestigos;
	 }
	 
	 /**
	 * M�todo que obtiene la info para desplegar en el layout Asignacion de Testigos (IF).
	 * @author  Ivan Almaguer
	 * @param  clave_if
	 * @return hmTestigos
	 * @since  FODEA 037 - 2009 Cesi�n de Derechos
	 */
	 public HashMap getTestigos(String clave_if) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		HashMap hmTestigos = new HashMap();
		List varBind = new ArrayList();
		StringBuffer qrySQL = new StringBuffer();
		int index = 0;
		try{
		log.info("getTestigos (E)");
		con.conexionDB();
			
			//se consulta y se envian los datos al action
			qrySQL.append(	"	SELECT CG_TESTIGO AS testigo "+
								"    FROM CDERCAT_TESTIGO_IF  " +
								"   WHERE IC_IF = ?  " );
			varBind.add(clave_if);
			pst = con.queryPrecompilado(qrySQL.toString(), varBind);
			rst = pst.executeQuery();
			
			
			while(rst.next()){
				HashMap hmRegistrosTestigos = new HashMap();
				hmRegistrosTestigos.put("testigo", rst.getString("testigo")==null?"":rst.getString("testigo"));
				hmTestigos.put("hmRegistrosTestigos"+index, hmRegistrosTestigos);
				index++;
			}
				
			hmTestigos.put("index", Integer.toString(index));
			pst.close();
			
		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("No se pudieron obtener los testigos");
		}finally{
			if(con.hayConexionAbierta()){
				 con.cierraConexionDB();
			}
			log.info("getTestigos (S)");
		 }
		 return hmTestigos;
	 }
	 /**
	  * M�todo que borra los Testigos capturados previamente
	  * @author	Ivan Almaguer
	  * @param	clave_if
	  * @since	FODEA 037 - 2009
	  * @throws AppException lanzada cuando ocurre un error.
	  */
	  public List borraTestigos(String clave_if, String testigo) throws AppException{
		  AccesoDB con = new AccesoDB();
		  PreparedStatement pst = null;
		  ResultSet rst = null;
		  boolean bcommit = true;
		  List lborrado = new ArrayList();
		  StringBuffer qrySQL = new StringBuffer();
		  
		  try{
			log.info("borraTestigos (E)");
			con.conexionDB();
			
				qrySQL.append(" DELETE CDERCAT_TESTIGO_IF WHERE IC_IF = "+clave_if+" AND CG_TESTIGO = '"+testigo+"'  ");
				con.ejecutaSQL(qrySQL.toString());
			  
		  }catch(Exception e){
			  bcommit = false;
			  e.printStackTrace();
			  throw new AppException("No se pudieron borrar los Testigos");
			  
		  }finally{
				lborrado.add("Success");
			  if(con.hayConexionAbierta()){
				  con.terminaTransaccion(bcommit);
				  con.cierraConexionDB();
			  }
			  log.info("borraTestigos (S)");
		  }
		  return lborrado;
	  }
	  
	  /**
		* M�todo que obtiene la info para desplegar en el layout Notificaciones Pyme
		* @author  Ivan Almaguer
		* @param  hmParamNotifPyme
		* @return hmConsNotifPyme
		* @since  FODEA 037 - 2009 Cesi�n de Derechos
		* @throws AppException lanzada cuando ocurre un error.
		*/
		public HashMap getNotificacionesPyme(HashMap hmParamNotifPyme) throws AppException{
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			ResultSet rst = null;
			StringBuffer qrySQL = new StringBuffer();
			List varBind = new ArrayList();
			HashMap hmConsNotifPyme = new HashMap();
			int index = 0;
			
			try{
				log.info("consNotificacionesfPyme (E)");
				con.conexionDB();
				
				
				String clave_contrat = hmParamNotifPyme.get("clave_contrat")==null? "" :(String)hmParamNotifPyme.get("clave_contrat");
				String clave_epo		= hmParamNotifPyme.get("clave_epo")==null?     "" :(String)hmParamNotifPyme.get("clave_epo");
				String clave_estatus	= hmParamNotifPyme.get("clave_estatus")==null? "" :(String)hmParamNotifPyme.get("clave_estatus");
				String clave_if		= hmParamNotifPyme.get("clave_if")==null?      "" :(String)hmParamNotifPyme.get("clave_if");
				String clave_pyme		= hmParamNotifPyme.get("clave_pyme")==null?      "" :(String)hmParamNotifPyme.get("clave_pyme");
				String clave_moneda	= hmParamNotifPyme.get("clave_moneda")==null?  "" :(String)hmParamNotifPyme.get("clave_moneda");
				String desde			= hmParamNotifPyme.get("desde")==null?			  "" :(String)hmParamNotifPyme.get("desde");
				String hasta			= hmParamNotifPyme.get("hasta")==null?			  "" :(String)hmParamNotifPyme.get("hasta");
				String num_contrato	= hmParamNotifPyme.get("num_contrato")==null?  "" :(String)hmParamNotifPyme.get("num_contrato");
        String clave_plazo_contrato = hmParamNotifPyme.get("clave_plazo_contrato")==null?"":(String)hmParamNotifPyme.get("clave_plazo_contrato");
        String plazo_contrato = hmParamNotifPyme.get("plazo_contrato")==null?"":(String)hmParamNotifPyme.get("plazo_contrato");
				List	lAdicionales	= hmParamNotifPyme.get("lAdicionales")==null?new ArrayList():(List)hmParamNotifPyme.get("lAdicionales");
				List  tipo_dato      = hmParamNotifPyme.get("tipo_dato")==null?new ArrayList():(List)hmParamNotifPyme.get("tipo_dato");
				
				qrySQL.append(
									" SELECT sol.ic_solicitud AS clave_solicitud, " +
									"        sol.ic_epo AS clave_epo," +
									"        sol.ic_pyme AS clave_pyme," +
									"        sol.ic_if AS clave_if," +
									"        epo.cg_razon_social AS dependencia, " +
									"        cif.cg_razon_social AS cesionario, " +
									"        sol.cg_no_contrato AS numero_contrato, " +
									//"        sol.fn_monto_contrato AS monto_contrato, " +
									//"        mon.cd_nombre AS moneda, " +
									"        tcn.cg_nombre AS tipo_contratacion, " +
									"        TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy') AS inicio_contrato, " +
									"        TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy') AS fin_contrato, " +
									"        cep.cg_area AS clasificacion_epo, " +
									"        sol.cg_campo1 AS camp_adic_1, " +
									"        sol.cg_campo2 AS camp_adic_2, " +
									"        sol.cg_campo3 AS camp_adic_3, " +
									"        sol.cg_campo4 AS camp_adic_4, " +
									"        sol.cg_campo5 AS camp_adic_5, " +
									"        sol.cg_obj_contrato AS objeto_contrato, " +
									"        sol.cg_comentarios AS comentarios, " +
									"        sol.cg_cuenta AS cuenta, " +
									"        sol.cg_cuenta_clabe AS cuenta_clabe, " +
									"        sts.cg_descripcion AS estatus, " + 
                  "        ctp.cg_descripcion as desc_plazo, " + 
                  "        sol.ig_plazo as plazo, " +
                  "        sol.cg_observ_rechazo as observ_rechazo " +
									"   FROM cder_solicitud sol, " +
									"        comcat_epo epo, " +
									"        comcat_if cif, " +
									"			comcat_pyme cp, " +
									//"        comcat_moneda mon, " +
									"        cdercat_tipo_contratacion tcn, " +
									"        cdercat_clasificacion_epo cep, " +
									"        cdercat_estatus_solic sts, " +
									"        cdercat_tipo_plazo ctp " +
									"  WHERE sol.ic_epo = epo.ic_epo " +
									"    AND sol.ic_if = cif.ic_if " +
									"    AND sol.ic_pyme = cp.ic_pyme " + 
									//"    AND sol.ic_moneda = mon.ic_moneda " +
									"    AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion " +
									"    AND sol.ic_epo = cep.ic_epo " +
									"    AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo " +
									"    AND sol.ic_estatus_solic = sts.ic_estatus_solic " +
									"    AND cp.ic_pyme = ? " +
									"   AND sol.ic_estatus_solic IN (11, 12, 15, 16, 17) " +
									"   AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+) "
								 );
									
									 varBind.add(clave_pyme);
								 
								 if(!clave_contrat.equals("")){
									 qrySQL.append("   AND tcn.ic_tipo_contratacion = ? ");
									 varBind.add(clave_contrat);
								 }
								 if(!clave_estatus.equals("")){
									 qrySQL.append("   AND sts.ic_estatus_solic = ? ");
									 varBind.add(clave_estatus);
								 }
								 if(!clave_epo.equals("")){
									 qrySQL.append("   AND epo.ic_epo = ? ");
									 varBind.add(clave_epo);
								 }
								 if(!clave_if.equals("")){
									 qrySQL.append("   AND cif.ic_if = ? " );
									 varBind.add(clave_if);
								 }
								 /*
								 if(!clave_moneda.equals("")){
									 qrySQL.append("   AND mon.ic_moneda = ? ");
									 varBind.add(clave_moneda);
								 }
								 */
								 if(!desde.equals("") && !hasta.equals("")){
									qrySQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
									qrySQL.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
									varBind.add(desde);
									varBind.add(hasta);
								}
								if(!num_contrato.equals("")){
									 qrySQL.append("   AND sol.cg_no_contrato = ? ");
									 varBind.add(num_contrato);
								}
								if(!lAdicionales.isEmpty()){
									for(int i = 0; i < lAdicionales.size(); i++){
										String campo_adicional = (String)lAdicionales.get(i);
										String tipo = (String)tipo_dato.get(i);
										if(!campo_adicional.equals("")){
											qrySQL.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
											if(tipo_dato.equals("N")){varBind.add(new Long(campo_adicional));}else{varBind.add(campo_adicional);}
										}
									}
								}
								
								if (clave_moneda != null && !"".equals(clave_moneda)) {
                  if (clave_moneda.equals("0")) {
                    qrySQL.append(" AND sol.cs_multimoneda = ?");
                    varBind.add("S");
                  } else {
                    qrySQL.append(" AND sol.cs_multimoneda = ?");
                    qrySQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
                    qrySQL.append(" FROM cder_monto_x_solic mxs");
                    qrySQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
                    qrySQL.append(" AND mxs.ic_moneda = ?)");
                    varBind.add("N");
                    varBind.add(new Integer(clave_moneda));
                  }
                }      
                if(!clave_plazo_contrato.equals("")){
                  qrySQL.append(" AND sol.ic_tipo_plazo = ? ");
                  varBind.add(new Integer(clave_plazo_contrato));
                }
                if(!plazo_contrato.equals("")){
                  qrySQL.append(" AND sol.ig_plazo = ? ");
                  varBind.add(new Integer(plazo_contrato));
                }
								qrySQL.append(" ORDER BY sol.df_alta_solicitud DESC"); 
								
								 
				log.debug("qrySQL ::: "+qrySQL);
				log.debug("varBind ::: "+varBind);
				
				pst = con.queryPrecompilado(qrySQL.toString(), varBind);
				rst = pst.executeQuery();
				
				while(rst.next()){
				HashMap hmRegistrosNotifPyme = new HashMap();
				hmRegistrosNotifPyme.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				hmRegistrosNotifPyme.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				hmRegistrosNotifPyme.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				hmRegistrosNotifPyme.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				hmRegistrosNotifPyme.put("dependencia", rst.getString("dependencia")==null?"":rst.getString("dependencia"));
				hmRegistrosNotifPyme.put("cesionario", rst.getString("cesionario")==null?"":rst.getString("cesionario"));
				hmRegistrosNotifPyme.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//hmRegistrosNotifPyme.put("monto_contrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				//hmRegistrosNotifPyme.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				hmRegistrosNotifPyme.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				hmRegistrosNotifPyme.put("inicio_contrato", rst.getString("inicio_contrato")==null?"":rst.getString("inicio_contrato"));
				hmRegistrosNotifPyme.put("fin_contrato", rst.getString("fin_contrato")==null?"":rst.getString("fin_contrato"));
				hmRegistrosNotifPyme.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				hmRegistrosNotifPyme.put("camp_adic_1", rst.getString("camp_adic_1")==null?"":rst.getString("camp_adic_1"));
				hmRegistrosNotifPyme.put("camp_adic_2", rst.getString("camp_adic_2")==null?"":rst.getString("camp_adic_2"));
				hmRegistrosNotifPyme.put("camp_adic_3", rst.getString("camp_adic_3")==null?"":rst.getString("camp_adic_3"));
				hmRegistrosNotifPyme.put("camp_adic_4", rst.getString("camp_adic_4")==null?"":rst.getString("camp_adic_4"));
				hmRegistrosNotifPyme.put("camp_adic_5", rst.getString("camp_adic_5")==null?"":rst.getString("camp_adic_5"));
				hmRegistrosNotifPyme.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				hmRegistrosNotifPyme.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));
				hmRegistrosNotifPyme.put("cuenta", rst.getString("cuenta")==null?"":rst.getString("cuenta"));
				hmRegistrosNotifPyme.put("cuenta_clabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				hmRegistrosNotifPyme.put("estatus", rst.getString("estatus")==null?"":rst.getString("estatus"));
				
        StringBuffer monto_moneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

        hmRegistrosNotifPyme.put("monto_moneda", monto_moneda.toString());
        hmRegistrosNotifPyme.put("desc_plazo", rst.getString("desc_plazo")==null?"":rst.getString("desc_plazo"));
        hmRegistrosNotifPyme.put("plazo", rst.getString("plazo")==null?"":rst.getString("plazo"));
        hmRegistrosNotifPyme.put("observ_rechazo", rst.getString("observ_rechazo")==null?"NA":rst.getString("observ_rechazo"));
				
				hmConsNotifPyme.put("hmRegistrosNotifPyme"+index, hmRegistrosNotifPyme);
				index++;
				}
				
				hmConsNotifPyme.put("index", Integer.toString(index));
				rst.close();
				pst.close();
			}catch(Exception e){
				e.printStackTrace();
				throw new AppException("Error al ejecutar la consulta");
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
			}
			log.info("consNotificacionesfPyme (S)");
			return hmConsNotifPyme;
		}


	  /**
		* M�todo que obtiene la info para desplegar en el layout Notificaciones IF
		* @author  Ivan Almaguer
		* @param  hmParamNotifIf
		* @return hmConsNotifIf
		* @since  FODEA 037 - 2009 Cesi�n de Derechos
		* @throws AppException lanzada cuando ocurre un error.
		*/
		public HashMap getNotificacionesIf(HashMap hmParamNotifIf) {
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			ResultSet rst = null;
			StringBuffer qrySQL = new StringBuffer();
			List varBind = new ArrayList();
			HashMap hmConsNotifIf = new HashMap();
			int index = 0;
			
			try{
				log.info("getNotificacionesIf (E)");
				con.conexionDB();				
				
				String clave_contrat = hmParamNotifIf.get("clave_contrat")==null? "" :(String)hmParamNotifIf.get("clave_contrat");
				String clave_epo		= hmParamNotifIf.get("clave_epo")==null?     "" :(String)hmParamNotifIf.get("clave_epo");
				String clave_if		= hmParamNotifIf.get("clave_if")==null?      "" :(String)hmParamNotifIf.get("clave_if");
				String clave_pyme		= hmParamNotifIf.get("clave_pyme")==null?    "" :(String)hmParamNotifIf.get("clave_pyme");
				String clave_estatus	= hmParamNotifIf.get("clave_estatus")==null? "" :(String)hmParamNotifIf.get("clave_estatus");
				String clave_moneda	= hmParamNotifIf.get("clave_moneda")==null?  "" :(String)hmParamNotifIf.get("clave_moneda");
				String desde			= hmParamNotifIf.get("desde")==null?			"" :(String)hmParamNotifIf.get("desde");
				String hasta			= hmParamNotifIf.get("hasta")==null?			"" :(String)hmParamNotifIf.get("hasta");
				String num_contrato	= hmParamNotifIf.get("num_contrato")==null?  "" :(String)hmParamNotifIf.get("num_contrato");
				List	lAdicionales	= hmParamNotifIf.get("lAdicionales")==null?new ArrayList():(List)hmParamNotifIf.get("lAdicionales");
				List  tipo_dato      = hmParamNotifIf.get("tipo_dato")==null?new ArrayList():(List)hmParamNotifIf.get("tipo_dato");
        String clave_plazo_contrato = hmParamNotifIf.get("clave_plazo_contrato")==null?"":(String)hmParamNotifIf.get("clave_plazo_contrato");
        String plazo_contrato = hmParamNotifIf.get("plazo_contrato")==null?"":(String)hmParamNotifIf.get("plazo_contrato");
				
				qrySQL.append(
									" SELECT sol.ic_solicitud AS clave_solicitud,  " +
									"        sol.ic_epo AS clave_epo," +
									"        sol.ic_pyme AS clave_pyme," +
									"        sol.ic_if AS clave_if," +
									"        epo.cg_razon_social AS dependencia,  " +
									"        cp.cg_razon_social AS pyme,  " +
									"        cp.cg_rfc AS RFC, " +
									"        cpe.cg_pyme_epo_interno AS numero_proveedor,  " +
									"        TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud, " +
									"        sol.cg_no_contrato AS numero_contrato,  " +
									//"        sol.fn_monto_contrato AS monto_contrato,  " +
									//"        mon.cd_nombre AS moneda,  " +
									"        tcn.cg_nombre AS tipo_contratacion,  " +
									"        TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy') AS inicio_contrato,  " +
									"        TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy') AS fin_contrato,  " +
									"        cep.cg_area AS clasificacion_epo,  " +
									"        sol.cg_campo1 AS camp_adic_1,  " +
									"        sol.cg_campo2 AS camp_adic_2,  " +
									"        sol.cg_campo3 AS camp_adic_3,  " +
									"        sol.cg_campo4 AS camp_adic_4,  " +
									"        sol.cg_campo5 AS camp_adic_5,  " +
									"        sol.cg_obj_contrato AS objeto_contrato, " +
									"        sol.cg_comentarios AS comentarios, " +//FODEA 041 - 2010 ACF
									"        sol.fn_monto_credito AS monto_credito, " +
									"        sol.cg_referencia AS referencia, " +
									"        TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento, " +
									"        sol.cg_banco_deposito AS banco_deposito, " +
									"        sol.cg_cuenta AS cuenta,  " +
									"        sol.cg_cuenta_clabe AS cuenta_clabe,  " +
									"        sts.cg_descripcion AS estatus,  " +
                  "        ctp.cg_descripcion as desc_plazo, " + 
                  "        sol.ig_plazo as plazo, " +
                  "        sol.cg_observ_rechazo as observ_rechazo " +
									"   FROM cder_solicitud sol,  " +
									"        comcat_epo epo,  " +
									"        comcat_if cif,  " +
									"        comcat_pyme cp, " +
									//"        comcat_moneda mon, " +
									"        comrel_pyme_epo cpe,  " +
									"        cdercat_tipo_contratacion tcn,  " +
									"        cdercat_clasificacion_epo cep,  " +
									"        cdercat_estatus_solic sts, " +
                  "        cdercat_tipo_plazo ctp " +
									"  WHERE sol.ic_epo = epo.ic_epo  " +
									"    AND sol.ic_if = cif.ic_if " +
									"    AND sol.ic_pyme = cp.ic_pyme  " +
									//"    AND sol.ic_moneda = mon.ic_moneda  " +
									"    AND sol.ic_epo = cpe.ic_epo(+)" +
									"    AND sol.ic_pyme = cpe.ic_pyme(+)" +
									"    AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion  " +
									"    AND sol.ic_epo = cep.ic_epo  " +
									"    AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo  " +
									"    AND sol.ic_estatus_solic = sts.ic_estatus_solic " +
									"	  AND cif.ic_if = ? "+
									"	  AND sol.ic_estatus_solic IN (11,12,15,16) " + 
                  "   AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+) "
								 );
								 
									varBind.add(clave_if);									
									
								 if(!clave_contrat.equals("")){
									 qrySQL.append("   AND tcn.ic_tipo_contratacion = ? ");
									 varBind.add(clave_contrat);
								 }
								 if(!clave_estatus.equals("")){
									 qrySQL.append("   AND sts.ic_estatus_solic = ? ");
									 varBind.add(clave_estatus);
								 }
								 if(!clave_epo.equals("")){
									 qrySQL.append("   AND epo.ic_epo = ? ");
									 varBind.add(clave_epo);
								 }
								 if(!clave_pyme.equals("")){
									 qrySQL.append("   AND cp.ic_pyme = ? " );
									 varBind.add(clave_pyme);
								 }
								 /*if(!clave_moneda.equals("")){
									 qrySQL.append("   AND mon.ic_moneda = ? ");
									 varBind.add(clave_moneda);
								 }*/
								 if(!desde.equals("") && !hasta.equals("")){
									qrySQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
									qrySQL.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
									varBind.add(desde);
									varBind.add(hasta);
								}
								if(!num_contrato.equals("")){
									 qrySQL.append("   AND sol.cg_no_contrato = ? ");
									 varBind.add(num_contrato);
								}
								if(!lAdicionales.isEmpty()){
									for(int i = 0; i < lAdicionales.size(); i++){
										String campo_adicional = (String)lAdicionales.get(i);
										String tipo = (String)tipo_dato.get(i);
										if(!campo_adicional.equals("")){
											qrySQL.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
											if(tipo_dato.equals("N")){varBind.add(new Long(campo_adicional));}else{varBind.add(campo_adicional);}
										}
									}
								}
								if (clave_moneda != null && !"".equals(clave_moneda)) {
                  if (clave_moneda.equals("0")) {
                    qrySQL.append(" AND sol.cs_multimoneda = ?");
                    varBind.add("S");
                  } else {
                    qrySQL.append(" AND sol.cs_multimoneda = ?");
                    qrySQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
                    qrySQL.append(" FROM cder_monto_x_solic mxs");
                    qrySQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
                    qrySQL.append(" AND mxs.ic_moneda = ?)");
                    varBind.add("N");
                    varBind.add(new Integer(clave_moneda));
                  }
                }      
                if(!clave_plazo_contrato.equals("")){
                  qrySQL.append(" AND sol.ic_tipo_plazo = ? ");
                  varBind.add(new Integer(clave_plazo_contrato));
                }
                if(!plazo_contrato.equals("")){
                  qrySQL.append(" AND sol.ig_plazo = ? ");
                  varBind.add(new Integer(plazo_contrato));
                }
                qrySQL.append(" ORDER BY sol.df_alta_solicitud DESC"); 
                
				log.debug("qrySQL ::: "+qrySQL);
				log.debug("varBind ::: "+varBind);
				
				pst = con.queryPrecompilado(qrySQL.toString(), varBind);
				rst = pst.executeQuery();
				
				while(rst.next()){
				HashMap hmRegistrosNotifIf = new HashMap();
				hmRegistrosNotifIf.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				hmRegistrosNotifIf.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				hmRegistrosNotifIf.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				hmRegistrosNotifIf.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				hmRegistrosNotifIf.put("dependencia", rst.getString("dependencia")==null?"":rst.getString("dependencia"));
				hmRegistrosNotifIf.put("pyme", rst.getString("pyme")==null?"":rst.getString("pyme"));
				hmRegistrosNotifIf.put("RFC", rst.getString("RFC")==null?"":rst.getString("RFC"));
				hmRegistrosNotifIf.put("numero_proveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				hmRegistrosNotifIf.put("fecha_solicitud", rst.getString("fecha_solicitud")==null?"":rst.getString("fecha_solicitud"));
				hmRegistrosNotifIf.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//hmRegistrosNotifIf.put("monto_contrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				//hmRegistrosNotifIf.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				hmRegistrosNotifIf.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				hmRegistrosNotifIf.put("inicio_contrato", rst.getString("inicio_contrato")==null?"":rst.getString("inicio_contrato"));
				hmRegistrosNotifIf.put("fin_contrato", rst.getString("fin_contrato")==null?"":rst.getString("fin_contrato"));
				hmRegistrosNotifIf.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				hmRegistrosNotifIf.put("camp_adic_1", rst.getString("camp_adic_1")==null || "null".equals(rst.getString("camp_adic_1")) ? "":rst.getString("camp_adic_1"));
				hmRegistrosNotifIf.put("camp_adic_2", rst.getString("camp_adic_2")==null || "null".equals(rst.getString("camp_adic_2")) ? "":rst.getString("camp_adic_2"));
				hmRegistrosNotifIf.put("camp_adic_3", rst.getString("camp_adic_3")==null || "null".equals(rst.getString("camp_adic_3")) ? "":rst.getString("camp_adic_3"));
				hmRegistrosNotifIf.put("camp_adic_4", rst.getString("camp_adic_4")==null || "null".equals(rst.getString("camp_adic_4")) ? "":rst.getString("camp_adic_4"));
				hmRegistrosNotifIf.put("camp_adic_5", rst.getString("camp_adic_5")==null || "null".equals(rst.getString("camp_adic_5")) ? "":rst.getString("camp_adic_5"));
				hmRegistrosNotifIf.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				hmRegistrosNotifIf.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));//041 - 2010 ACF
				hmRegistrosNotifIf.put("monto_credito", rst.getString("monto_credito")==null?"":rst.getString("monto_credito"));
				hmRegistrosNotifIf.put("referencia", rst.getString("referencia")==null?"":rst.getString("referencia"));
				hmRegistrosNotifIf.put("fecha_vencimiento", rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento"));
				hmRegistrosNotifIf.put("banco_deposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				hmRegistrosNotifIf.put("cuenta", rst.getString("cuenta")==null?"":rst.getString("cuenta"));
				hmRegistrosNotifIf.put("cuenta_clabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				hmRegistrosNotifIf.put("estatus", rst.getString("estatus")==null?"":rst.getString("estatus"));
        
        StringBuffer monto_moneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

        hmRegistrosNotifIf.put("monto_moneda", monto_moneda.toString());
        hmRegistrosNotifIf.put("desc_plazo", rst.getString("desc_plazo")==null?"":rst.getString("desc_plazo"));
        hmRegistrosNotifIf.put("plazo", rst.getString("plazo")==null?"":rst.getString("plazo"));
        hmRegistrosNotifIf.put("observ_rechazo", rst.getString("observ_rechazo")==null?"NA":rst.getString("observ_rechazo"));
        
				hmConsNotifIf.put("hmRegistrosNotifIf"+index, hmRegistrosNotifIf);
				index++;
				}
				
				hmConsNotifIf.put("index", Integer.toString(index));
				rst.close();
				pst.close();
			}catch(Exception e){
				e.printStackTrace();
				throw new AppException("Error al ejecutar la consulta", e);
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
			}
			log.info("getNotificacionesIf (S)");
			return hmConsNotifIf;
		}


	  /**
		* M�todo que obtiene la info para desplegar en el layout Notificaciones NAFIN
		* @author  Ivan Almaguer
		* @param  hmParamNotifNafin
		* @return hmConsNotifNafin
		* @since  FODEA 037 - 2009 Cesi�n de Derechos
		* @throws AppException lanzada cuando ocurre un error.
		*/
		public HashMap getNotificacionesNafin(HashMap hmParamNotifNafin) {
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			ResultSet rst = null;
			StringBuffer qrySQL = new StringBuffer();
			List varBind = new ArrayList();
			HashMap hmConsNotifNafin = new HashMap();
			int index = 0;
			
			try{
				log.info("getNotificacionesNafin (E)");
				con.conexionDB();
				
				
				String clave_contrat = hmParamNotifNafin.get("clave_contrat")==null? "" :(String)hmParamNotifNafin.get("clave_contrat");
				String clave_epo		= hmParamNotifNafin.get("clave_epo")==null?     "" :(String)hmParamNotifNafin.get("clave_epo");
				String clave_if		= hmParamNotifNafin.get("clave_if")==null?      "" :(String)hmParamNotifNafin.get("clave_if");
				String clave_pyme		= hmParamNotifNafin.get("clave_pyme")==null?    "" :(String)hmParamNotifNafin.get("clave_pyme");
				String clave_estatus	= hmParamNotifNafin.get("clave_estatus")==null? "" :(String)hmParamNotifNafin.get("clave_estatus");
				String clave_moneda	= hmParamNotifNafin.get("clave_moneda")==null?  "" :(String)hmParamNotifNafin.get("clave_moneda");
				String desde			= hmParamNotifNafin.get("desde")==null?			"" :(String)hmParamNotifNafin.get("desde");
				String hasta			= hmParamNotifNafin.get("hasta")==null?			"" :(String)hmParamNotifNafin.get("hasta");
				String num_contrato	= hmParamNotifNafin.get("num_contrato")==null?  "" :(String)hmParamNotifNafin.get("num_contrato");
				List	lAdicionales	= hmParamNotifNafin.get("lAdicionales")==null?new ArrayList():(List)hmParamNotifNafin.get("lAdicionales");
				List  tipo_dato      = hmParamNotifNafin.get("tipo_dato")==null?new ArrayList():(List)hmParamNotifNafin.get("tipo_dato");
				String clave_plazo_contrato = hmParamNotifNafin.get("clave_plazo_contrato")==null?"":(String)hmParamNotifNafin.get("clave_plazo_contrato");
        String plazo_contrato = hmParamNotifNafin.get("plazo_contrato")==null?"":(String)hmParamNotifNafin.get("plazo_contrato");
        
				qrySQL.append(
									" SELECT sol.ic_solicitud AS clave_solicitud,  " +
									"        sol.ic_epo AS clave_epo," +
									"        sol.ic_pyme AS clave_pyme," +
									"        sol.ic_if AS clave_if," +
									"        epo.cg_razon_social AS dependencia,  " +
									"        cif.cg_razon_social AS cesionario, " +
									"        cp.cg_razon_social AS pyme,  " +
									"        cp.cg_rfc AS RFC, " +
									"        cpe.cg_pyme_epo_interno AS numero_proveedor,  " +
									"        TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud, " +
									"        sol.cg_no_contrato AS numero_contrato,  " +
									//"        sol.fn_monto_contrato AS monto_contrato,  " +
									//"        mon.cd_nombre AS moneda,  " +
									"        tcn.cg_nombre AS tipo_contratacion,  " +
									//"        TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy') AS inicio_contrato,  " +
									//"        TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy') AS fin_contrato,  " +
									" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,"+
									" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,"+
									" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS plazo_contrato,"+
									"        cep.cg_area AS clasificacion_epo, " +
									"        TO_CHAR(TO_DATE(sol.df_fecha_aceptacion, 'dd/mm/yyyy')+lim.ig_dias_pnotificacion) AS fecha_limite_notificacion, " +
									"        sol.cg_campo1 AS camp_adic_1,  " +
									"        sol.cg_campo2 AS camp_adic_2,  " +
									"        sol.cg_campo3 AS camp_adic_3,  " +
									"        sol.cg_campo4 AS camp_adic_4,  " +
									"        sol.cg_campo5 AS camp_adic_5,  " +
									" cvp.cg_descripcion AS venanilla_pago,"+
									" sol.cg_sup_adm_resob AS sup_adm_resob,"+
									" sol.cg_numero_telefono AS numero_telefono,"+
									"        sol.cg_obj_contrato AS objeto_contrato, " +
									"        sol.cg_comentarios AS comentarios, " +//FODEA 041 - 2010 ACF
									"        sol.fn_monto_credito AS monto_credito, " +
									"        sol.cg_referencia AS referencia, " +
									"        TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,  " +
									"        sol.cg_banco_deposito AS banco_deposito, " +
									"        sol.cg_cuenta AS cuenta,  " +
									"        sol.cg_cuenta_clabe AS cuenta_clabe,  " +
									"        TO_CHAR(ca.df_operacion, 'dd/mm/yyyy') AS fecha_autrech_pyme,  " +
									"        TO_CHAR(ca2.df_operacion, 'dd/mm/yyyy') AS fecha_autrech_epo,  " +
									"        sts.cg_descripcion AS estatus,  " +
									"			   epo.cg_clasificacion_epo AS nombre_clasificaion_epo, " +
                  //"        ctp.cg_descripcion as desc_plazo," +
                  //"        sol.ig_plazo as plazo," +
                  "        sol.cg_observ_rechazo as observ_rechazo " +
									"   FROM cder_solicitud sol, " +
									"        cder_acuse ca,  " +
									"        cder_acuse ca2, " +
									"        comcat_epo epo,  " +
									"        comcat_if cif,  " +
									"        comcat_pyme cp, " +
									" cdercat_ventanilla_pago cvp,"+
									"        comrel_pyme_epo cpe,  " +
									"        comrel_producto_epo cpepo, " +
									"        cdercat_tipo_contratacion tcn,  " +
									"        cdercat_clasificacion_epo cep,  " +
									"        cdercat_estatus_solic sts, " +
									"        (select ig_dias_pnotificacion  " +
									"           from comrel_producto_epo  " +
									"          where ic_epo = ?  " +
									"            and ic_producto_nafin = ?) lim, " +
                  "        cdercat_tipo_plazo ctp " +
									"  WHERE sol.ic_epo = epo.ic_epo  " +
									"    AND sol.ic_if = cif.ic_if " +
									"    AND sol.ic_pyme = cp.ic_pyme  " +
									//"    AND sol.ic_moneda = mon.ic_moneda  " +
									"    AND sol.ic_epo = cpe.ic_epo(+)" +
									"    AND sol.ic_pyme = cpe.ic_pyme(+)" +
									"    AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion  " +
									"    AND sol.ic_epo = cep.ic_epo  " +
									"    AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo  " +
									"    AND sol.ic_estatus_solic = sts.ic_estatus_solic   " +
									"    AND sol.cc_acuse3 = ca.cc_acuse(+)" +
									"    AND sol.cc_acuse1 = ca2.cc_acuse(+)" +
									"    AND sol.ic_epo = cpepo.ic_epo " +
									"    AND cpepo.ic_producto_nafin = ? " +
									" AND sol.ic_epo = cvp.ic_epo(+)"+
									" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)"+
									"	   AND sol.ic_estatus_solic IN (11, 12, 15, 16) " + 
                  "    AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+) "
								 );
									
									varBind.add(clave_epo);
									varBind.add("9");
									varBind.add("9");
									
								 if(!clave_contrat.equals("")){
									 qrySQL.append("   AND tcn.ic_tipo_contratacion = ? ");
									 varBind.add(clave_contrat);
								 }
								 if(!clave_estatus.equals("")){
									 qrySQL.append("   AND sts.ic_estatus_solic = ? ");
									 varBind.add(clave_estatus);
								 }
								 if(!clave_epo.equals("")){
									 qrySQL.append("   AND epo.ic_epo = ? ");
									 varBind.add(clave_epo);
								 }
								 if(!clave_pyme.equals("")){
									 qrySQL.append("   AND cp.ic_pyme = ? " );
									 varBind.add(clave_pyme);
								 }
								 if(!clave_if.equals("")){
									 qrySQL.append("   AND cif.ic_if = ? " );
									 varBind.add(clave_if);
								 }								 
								 if(!desde.equals("") && !hasta.equals("")){
									qrySQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
									qrySQL.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
									varBind.add(desde);
									varBind.add(hasta);
								}
								if(!num_contrato.equals("")){
									 qrySQL.append("   AND sol.cg_no_contrato = ? ");
									 varBind.add(num_contrato);
								}
								if(!lAdicionales.isEmpty()){
									for(int i = 0; i < lAdicionales.size(); i++){
										String campo_adicional = (String)lAdicionales.get(i);
										String tipo = (String)tipo_dato.get(i);
										if(!campo_adicional.equals("")){
											qrySQL.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
											if(tipo_dato.equals("N")){varBind.add(new Long(campo_adicional));}else{varBind.add(campo_adicional);}
										}
									}
								}
                if (clave_moneda != null && !"".equals(clave_moneda)) {
                  if (clave_moneda.equals("0")) {
                    qrySQL.append(" AND sol.cs_multimoneda = ?");
                    varBind.add("S");
                  } else {
                    qrySQL.append(" AND sol.cs_multimoneda = ?");
                    qrySQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
                    qrySQL.append(" FROM cder_monto_x_solic mxs");
                    qrySQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
                    qrySQL.append(" AND mxs.ic_moneda = ?)");
                    varBind.add("N");
                    varBind.add(new Integer(clave_moneda));
                  }
                }      
                if(!clave_plazo_contrato.equals("")){
                  qrySQL.append(" AND sol.ic_tipo_plazo = ? ");
                  varBind.add(new Integer(clave_plazo_contrato));
                }
                if(!plazo_contrato.equals("")){
                  qrySQL.append(" AND sol.ig_plazo = ? ");
                  varBind.add(new Integer(plazo_contrato));
                }
                qrySQL.append(" ORDER BY sol.df_alta_solicitud DESC");
								 
				log.debug("qrySQL ::: "+qrySQL);
				log.debug("varBind ::: "+varBind);
				
				pst = con.queryPrecompilado(qrySQL.toString(), varBind);
				rst = pst.executeQuery();
				
				while(rst.next()){
				HashMap hmRegistrosNotifNafin = new HashMap();
				hmRegistrosNotifNafin.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				hmRegistrosNotifNafin.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				hmRegistrosNotifNafin.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				hmRegistrosNotifNafin.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				hmRegistrosNotifNafin.put("dependencia", rst.getString("dependencia")==null?"":rst.getString("dependencia"));
				hmRegistrosNotifNafin.put("cesionario", rst.getString("cesionario")==null?"":rst.getString("cesionario"));
				hmRegistrosNotifNafin.put("pyme", rst.getString("pyme")==null?"":rst.getString("pyme"));
				hmRegistrosNotifNafin.put("RFC", rst.getString("RFC")==null?"":rst.getString("RFC"));
				hmRegistrosNotifNafin.put("numero_proveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				hmRegistrosNotifNafin.put("fecha_solicitud", rst.getString("fecha_solicitud")==null?"":rst.getString("fecha_solicitud"));
				hmRegistrosNotifNafin.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//hmRegistrosNotifNafin.put("monto_contrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				//hmRegistrosNotifNafin.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				hmRegistrosNotifNafin.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				hmRegistrosNotifNafin.put("inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				hmRegistrosNotifNafin.put("fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				hmRegistrosNotifNafin.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				hmRegistrosNotifNafin.put("fecha_limite_notificacion", rst.getString("fecha_limite_notificacion")==null?"":rst.getString("fecha_limite_notificacion"));
				hmRegistrosNotifNafin.put("camp_adic_1", rst.getString("camp_adic_1")==null || "null".equals(rst.getString("camp_adic_1")) ? "":rst.getString("camp_adic_1"));
				hmRegistrosNotifNafin.put("camp_adic_2", rst.getString("camp_adic_2")==null || "null".equals(rst.getString("camp_adic_2")) ? "":rst.getString("camp_adic_2"));
				hmRegistrosNotifNafin.put("camp_adic_3", rst.getString("camp_adic_3")==null || "null".equals(rst.getString("camp_adic_3")) ? "":rst.getString("camp_adic_3"));
				hmRegistrosNotifNafin.put("camp_adic_4", rst.getString("camp_adic_4")==null || "null".equals(rst.getString("camp_adic_4")) ? "":rst.getString("camp_adic_4"));
				hmRegistrosNotifNafin.put("camp_adic_5", rst.getString("camp_adic_5")==null || "null".equals(rst.getString("camp_adic_5")) ? "":rst.getString("camp_adic_5"));
				hmRegistrosNotifNafin.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
				hmRegistrosNotifNafin.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
				hmRegistrosNotifNafin.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));
				hmRegistrosNotifNafin.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				hmRegistrosNotifNafin.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));
				hmRegistrosNotifNafin.put("monto_credito", rst.getString("monto_credito")==null?"":rst.getString("monto_credito"));
				hmRegistrosNotifNafin.put("referencia", rst.getString("referencia")==null?"":rst.getString("referencia"));
				hmRegistrosNotifNafin.put("fecha_vencimiento", rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento"));
				hmRegistrosNotifNafin.put("banco_deposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				hmRegistrosNotifNafin.put("cuenta", rst.getString("cuenta")==null?"":rst.getString("cuenta"));
				hmRegistrosNotifNafin.put("cuenta_clabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				hmRegistrosNotifNafin.put("fecha_autrech_pyme", rst.getString("fecha_autrech_pyme")==null?"":rst.getString("fecha_autrech_pyme"));
				hmRegistrosNotifNafin.put("fecha_autrech_epo", rst.getString("fecha_autrech_epo")==null?"":rst.getString("fecha_autrech_epo"));
				hmRegistrosNotifNafin.put("estatus", rst.getString("estatus")==null?"":rst.getString("estatus"));
				hmRegistrosNotifNafin.put("nombre_clasificaion_epo", rst.getString("nombre_clasificaion_epo")==null?"":rst.getString("nombre_clasificaion_epo"));
				
        StringBuffer monto_moneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

        hmRegistrosNotifNafin.put("monto_moneda", monto_moneda.toString());
				hmRegistrosNotifNafin.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
        //hmRegistrosNotifNafin.put("desc_plazo", rst.getString("desc_plazo")==null?"":rst.getString("desc_plazo"));
        //hmRegistrosNotifNafin.put("plazo", rst.getString("plazo")==null?"":rst.getString("plazo"));
        hmRegistrosNotifNafin.put("observ_rechazo", rst.getString("observ_rechazo")==null?"NA":rst.getString("observ_rechazo"));
        
        hmConsNotifNafin.put("hmRegistrosNotifNafin"+index, hmRegistrosNotifNafin);
				index++;
				}
				
				hmConsNotifNafin.put("index", Integer.toString(index));
				rst.close();
				pst.close();
			}catch(Exception e){
				e.printStackTrace();
				throw new AppException("Error al ejecutar la consulta", e);
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
			}
			log.info("getNotificacionesNafin (S)");
			return hmConsNotifNafin;
		}
	

	/**
		* M�todo que obtiene la info para desplegar en el layout Solicitud de Consentimiento NAFIN
		* @author  Ivan Almaguer
		* @param  hmParamSolicCons
		* @return hmSolicCons
		* @since  FODEA 037 - 2009 Cesi�n de Derechos
		* @throws AppException lanzada cuando ocurre un error.
		*/
		public HashMap getSolicitudConsentimiento(HashMap hmParamSolicCons) {
			log.info("getSolicitudConsentimiento(E) ::..");
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			ResultSet rst = null;
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			HashMap hmSolicCons = new HashMap();
			int index = 0;
			
			try{
				con.conexionDB();

				String clavePyme = hmParamSolicCons.get("clavePyme")==null?"":(String)hmParamSolicCons.get("clavePyme");
				String claveEpo = hmParamSolicCons.get("claveEpo")==null?"":(String)hmParamSolicCons.get("claveEpo");
				String claveIfCesionario = hmParamSolicCons.get("claveIfCesionario")==null?"":(String)hmParamSolicCons.get("claveIfCesionario");
				String numeroContrato = hmParamSolicCons.get("numeroContrato")==null?"":(String)hmParamSolicCons.get("numeroContrato");
				String claveMoneda = hmParamSolicCons.get("claveMoneda")==null?"":(String)hmParamSolicCons.get("claveMoneda");
				String claveEstatusSolicitud = hmParamSolicCons.get("claveEstatusSolicitud")==null?"":(String)hmParamSolicCons.get("claveEstatusSolicitud");
				String claveTipoContratacion = hmParamSolicCons.get("claveTipoContratacion")==null?"":(String)hmParamSolicCons.get("claveTipoContratacion");
				String fechaVigenciaContratoIni = hmParamSolicCons.get("fechaVigenciaContratoIni")==null?"":(String)hmParamSolicCons.get("fechaVigenciaContratoIni");
				String fechaVigenciaContratoFin = hmParamSolicCons.get("fechaVigenciaContratoFin")==null?"":(String)hmParamSolicCons.get("fechaVigenciaContratoFin");
				String csTipoMonto = hmParamSolicCons.get("csTipoMonto")==null?"":(String)hmParamSolicCons.get("csTipoMonto");
				String plazoContrato = hmParamSolicCons.get("plazoContrato")==null?"":(String)hmParamSolicCons.get("plazoContrato");
				String claveTipoPlazo = hmParamSolicCons.get("claveTipoPlazo")==null?"":(String)hmParamSolicCons.get("claveTipoPlazo");
				String perfil = hmParamSolicCons.get("perfil")==null?"":(String)hmParamSolicCons.get("perfil");
				List lAdicionales	= hmParamSolicCons.get("lAdicionales")==null?new ArrayList():(List)hmParamSolicCons.get("lAdicionales");
				List tipo_dato = hmParamSolicCons.get("tipo_dato")==null?new ArrayList():(List)hmParamSolicCons.get("tipo_dato");

				strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
				strSQL.append(" sol.ic_pyme AS clave_pyme,");
				strSQL.append(" sol.ic_epo AS clave_epo,");
				strSQL.append(" sol.ic_if AS clave_if,");
				strSQL.append(" sol.ic_clasificacion_epo AS clave_clasificacion_epo,");
				strSQL.append(" sol.ic_moneda AS clave_moneda,");
				strSQL.append(" sol.ic_tipo_contratacion AS clave_tipo_contratacion,");
				strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
				strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
				strSQL.append(" pym.cg_rfc AS rfc,");
				strSQL.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
				strSQL.append(" epo.cg_razon_social AS nombre_epo,");
				strSQL.append(" cif.cg_razon_social AS nombre_if,");
				strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
				strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
				strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
				strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
				strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
				strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud,");
				strSQL.append(" TO_CHAR(sol.df_fecha_limite_firma, 'dd/mm/yyyy') AS fecha_limite_firma,");
				strSQL.append(" cep.cg_area AS clasificacion_epo,");
				strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
				strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
				strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
				strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
				strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
				strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
				strSQL.append(" sol.cg_comentarios AS comentarios,");
				strSQL.append(" DECODE(sol.ic_estatus_solic, 5, sol.cg_causas_rechazo, 'NA') AS causas_rechazo,");
				strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
				strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
				strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
				strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
				strSQL.append(" sol.cc_acuse1 AS acuse1,");
				strSQL.append(" sol.cc_acuse_envio_rep1 AS acuse_envio_rep1,");
				strSQL.append(" sol.cc_acuse_envio_rep2 AS acuse_envio_rep2");
				strSQL.append(" FROM cder_solicitud sol");
				strSQL.append(", comrel_pyme_epo cpe");
				strSQL.append(", comcat_pyme pym");
				strSQL.append(", comcat_epo epo");
				strSQL.append(", comcat_if cif");
				strSQL.append(", cdercat_tipo_contratacion tcn");
				strSQL.append(", cdercat_clasificacion_epo cep");
				strSQL.append(", cdercat_estatus_solic sts");
				strSQL.append(", cdercat_tipo_plazo stp");
				strSQL.append(", cdercat_ventanilla_pago cvp");
				strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
				strSQL.append(" AND sol.ic_if = cif.ic_if");
				strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
				strSQL.append(" AND sol.ic_epo = cpe.ic_epo(+)");
				strSQL.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
				strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
				strSQL.append(" AND sol.ic_epo = cep.ic_epo");
				strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
				strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
				strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
				strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
				strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
		
				if (clavePyme != null && !"".equals(clavePyme)) {
					strSQL.append(" AND sol.ic_pyme = ?");
					varBind.add(new Long(clavePyme));
				}
		
				if (claveEpo != null && !"".equals(claveEpo)) {
					strSQL.append(" AND sol.ic_epo = ?");
					varBind.add(new Integer(claveEpo));
				}
		
				if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
					strSQL.append(" AND sol.ic_if = ?");
					varBind.add(new Integer(claveIfCesionario));
				}
		
				if (numeroContrato != null && !"".equals(numeroContrato)) {
					strSQL.append(" AND sol.cg_no_contrato = ?");
					varBind.add(numeroContrato);
				}

			if (claveMoneda != null && !"".equals(claveMoneda)) {
				if (claveMoneda.equals("0")) {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("S");
				} else {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.ic_moneda = ?)");
					varBind.add("N");
					varBind.add(new Integer(claveMoneda));
				}
			}

				if (claveEstatusSolicitud != null && !claveEstatusSolicitud.equals("")) {
					strSQL.append(" AND sol.ic_estatus_solic = ?");
					varBind.add(new Integer(claveEstatusSolicitud));
				} else {
						strSQL.append(" AND sol.ic_estatus_solic IN (?, ?, ?, ?, ?, ?)");
						varBind.add(new Integer(1));
						varBind.add(new Integer(13));
						varBind.add(new Integer(2));
						varBind.add(new Integer(5));
						varBind.add(new Integer(6));
						varBind.add(new Integer(8));
				}
				
				if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
					strSQL.append(" AND sol.ic_tipo_contratacion = ?");
					varBind.add(new Integer(claveTipoContratacion));
				}		

				if (csTipoMonto != null && !"".equals(csTipoMonto)) {
					if (csTipoMonto.equals("D")) {
						strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
						strSQL.append(" FROM cder_monto_x_solic mxs");
						strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
						strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
						strSQL.append(" AND sol.cs_multimoneda = ?");
						varBind.add("N");
					} else if (csTipoMonto.equals("M")) {
						strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
						strSQL.append(" FROM cder_monto_x_solic mxs");
						strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
						strSQL.append(" AND mxs.fn_monto_moneda IS NULL)");
						strSQL.append(" AND sol.cs_multimoneda = ?");
						varBind.add("N");
					} else {
						strSQL.append(" AND (EXISTS (SELECT mxs.ic_monto_x_solic");
						strSQL.append(" FROM cder_monto_x_solic mxs");
						strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
						strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
						strSQL.append(" OR EXISTS (SELECT mxs.ic_monto_x_solic");
						strSQL.append(" FROM cder_monto_x_solic mxs");
						strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
						strSQL.append(" AND mxs.fn_monto_moneda IS NULL))");
						strSQL.append(" AND sol.cs_multimoneda = ?");
						varBind.add("N");
					}
				}
		
				if (plazoContrato != null && !"".equals(plazoContrato)) {
					strSQL.append(" AND sol.ig_plazo = ?");
					varBind.add(new Integer(plazoContrato));
				}		
		
				if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
					strSQL.append(" AND sol.ic_tipo_plazo = ?");
					varBind.add(new Integer(claveTipoPlazo));
				}

				if (fechaVigenciaContratoIni != null && !"".equals(fechaVigenciaContratoIni)) {
						strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
						strSQL.append(" AND sol.df_fecha_vigencia_ini < TO_DATE(?, 'dd/mm/yyyy') + 1");
						varBind.add(fechaVigenciaContratoIni);
						varBind.add(fechaVigenciaContratoIni);
				}
		
				if (fechaVigenciaContratoFin != null && !"".equals(fechaVigenciaContratoFin)) {
						strSQL.append(" AND sol.df_fecha_vigencia_fin >= TO_DATE(?, 'dd/mm/yyyy')");
						strSQL.append(" AND sol.df_fecha_vigencia_fin < TO_DATE(?, 'dd/mm/yyyy') + 1");
						varBind.add(fechaVigenciaContratoFin);
						varBind.add(fechaVigenciaContratoFin);
				}
				if (!lAdicionales.isEmpty()) {
					for(int i = 0; i < lAdicionales.size(); i++){
						String campo_adicional = (String)lAdicionales.get(i);
						String tipo = (String)tipo_dato.get(i);
						if(!campo_adicional.equals("")){
							strSQL.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
							if(tipo_dato.equals("N")){varBind.add(new Long(campo_adicional));}else{varBind.add(campo_adicional);}
						}
					}
				}
		
				strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");
				
				log.debug("..:: strSQL: "+strSQL.toString());
				log.debug("..:: varBind: "+varBind);
				
				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				rst = pst.executeQuery();
				
				while (rst.next()) {
					HashMap hmRegistrosSolicCons = new HashMap();
					StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

					hmRegistrosSolicCons.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
					hmRegistrosSolicCons.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
					hmRegistrosSolicCons.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
					hmRegistrosSolicCons.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
					hmRegistrosSolicCons.put("claveClasificacionEpo", rst.getString("clave_clasificacion_epo")==null?"":rst.getString("clave_clasificacion_epo"));
					hmRegistrosSolicCons.put("claveTipoContratacion", rst.getString("clave_tipo_contratacion")==null?"":rst.getString("clave_tipo_contratacion"));
					hmRegistrosSolicCons.put("clave_estatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
					hmRegistrosSolicCons.put("nombre_pyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
					hmRegistrosSolicCons.put("rfc", rst.getString("rfc")==null?"":rst.getString("rfc"));
					hmRegistrosSolicCons.put("numero_proveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
					hmRegistrosSolicCons.put("nombre_epo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
					hmRegistrosSolicCons.put("nombre_if", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
					hmRegistrosSolicCons.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
					hmRegistrosSolicCons.put("montosPorMoneda", montosPorMoneda.toString());
					hmRegistrosSolicCons.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
					hmRegistrosSolicCons.put("fecha_inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
					hmRegistrosSolicCons.put("fecha_fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
					hmRegistrosSolicCons.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
					hmRegistrosSolicCons.put("fecha_solicitud", rst.getString("fecha_solicitud")==null?"":rst.getString("fecha_solicitud"));
					hmRegistrosSolicCons.put("fecha_limite_firma", rst.getString("fecha_limite_firma")==null?"":rst.getString("fecha_limite_firma"));
					hmRegistrosSolicCons.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
					hmRegistrosSolicCons.put("campo_adicional_1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
					hmRegistrosSolicCons.put("campo_adicional_2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
					hmRegistrosSolicCons.put("campo_adicional_3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
					hmRegistrosSolicCons.put("campo_adicional_4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
					hmRegistrosSolicCons.put("campo_adicional_5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
					hmRegistrosSolicCons.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
					hmRegistrosSolicCons.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));
					hmRegistrosSolicCons.put("causas_rechazo", rst.getString("causas_rechazo")==null?"":rst.getString("causas_rechazo"));
					hmRegistrosSolicCons.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
					hmRegistrosSolicCons.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
					hmRegistrosSolicCons.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));
					hmRegistrosSolicCons.put("estatus_solicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
					hmSolicCons.put("hmRegistrosSolicCons"+index, hmRegistrosSolicCons);
					index++;
				}
				
				hmSolicCons.put("index", Integer.toString(index));
				rst.close();
				pst.close();
			}catch(Exception e){
				e.printStackTrace();
				throw new AppException("Error al ejecutar la consulta", e);
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
			}
			log.info("getSolicitudConsentimiento (S)");
			return hmSolicCons;
		}
		
	  /**
		* M�todo que obtiene la info para desplegar en el layout Contratos de Cesi�n NAFIN
		* @author  Ivan Almaguer
		* @param  hmParamContratosCes
		* @return hmConsContratosCes
		* @since  FODEA 037 - 2009 Cesi�n de Derechos
		* @throws AppException lanzada cuando ocurre un error.
		*/
		public HashMap getContratosCesion(HashMap hmParamContratosCes) {
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			ResultSet rst = null;
			StringBuffer qrySQL = new StringBuffer();
			List varBind = new ArrayList();
			HashMap hmConsContratosCes = new HashMap();
			int index = 0;
			
			try{
				log.info("getContratosCesion (E)");
				con.conexionDB();
				
				
				String clave_contrat = hmParamContratosCes.get("clave_contrat")==null? "" :(String)hmParamContratosCes.get("clave_contrat");
				String clave_epo		= hmParamContratosCes.get("clave_epo")==null?     "" :(String)hmParamContratosCes.get("clave_epo");
				String clave_if		= hmParamContratosCes.get("clave_if")==null?      "" :(String)hmParamContratosCes.get("clave_if");
				String clave_pyme		= hmParamContratosCes.get("clave_pyme")==null?    "" :(String)hmParamContratosCes.get("clave_pyme");
				String clave_estatus	= hmParamContratosCes.get("clave_estatus")==null? "" :(String)hmParamContratosCes.get("clave_estatus");
				String clave_moneda	= hmParamContratosCes.get("clave_moneda")==null?  "" :(String)hmParamContratosCes.get("clave_moneda");
				String desde			= hmParamContratosCes.get("desde")==null?			"" :(String)hmParamContratosCes.get("desde");
				String hasta			= hmParamContratosCes.get("hasta")==null?			"" :(String)hmParamContratosCes.get("hasta");
				String num_contrato	= hmParamContratosCes.get("num_contrato")==null?  "" :(String)hmParamContratosCes.get("num_contrato");
				List	lAdicionales	= hmParamContratosCes.get("lAdicionales")==null?new ArrayList():(List)hmParamContratosCes.get("lAdicionales");
				List  tipo_dato      = hmParamContratosCes.get("tipo_dato")==null?new ArrayList():(List)hmParamContratosCes.get("tipo_dato");
        String clave_plazo_contrato = hmParamContratosCes.get("clave_plazo_contrato")==null?  "" :(String)hmParamContratosCes.get("clave_plazo_contrato");
        String plazo_contrato = hmParamContratosCes.get("plazo_contrato")==null?  "" :(String)hmParamContratosCes.get("plazo_contrato");
				
				qrySQL.append(
									" SELECT sol.ic_solicitud AS clave_solicitud,  " +
									"        sol.ic_epo AS clave_epo," +
									"        sol.ic_pyme AS clave_pyme," +
									"        sol.ic_if AS clave_if," +
									"        epo.cg_razon_social AS dependencia,  " +
									"        cif.cg_razon_social AS cesionario, " +
									"        cp.cg_razon_social AS pyme,  " +
									"        cp.cg_rfc AS RFC, " +
									"        cpe.cg_pyme_epo_interno AS numero_proveedor,  " +
									"        TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud, " +
									"        sol.cg_no_contrato AS numero_contrato,  " +
									//"        sol.fn_monto_contrato AS monto_contrato,  " +
									//"        mon.cd_nombre AS moneda,  " +
									"        tcn.cg_nombre AS tipo_contratacion,  " +
									" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,"+
									" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,"+
									" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS plazo_contrato,"+
									"        cep.cg_area AS clasificacion_epo, " +
									"        TO_CHAR(TO_DATE(sol.df_fecha_aceptacion, 'dd/mm/yyyy')+lim.ig_dias_pnotificacion) AS fecha_limite_notificacion, " +
									"        sol.cg_campo1 AS camp_adic_1,  " +
									"        sol.cg_campo2 AS camp_adic_2,  " +
									"        sol.cg_campo3 AS camp_adic_3,  " +
									"        sol.cg_campo4 AS camp_adic_4,  " +
									"        sol.cg_campo5 AS camp_adic_5,  " +
									" cvp.cg_descripcion AS venanilla_pago,"+
									" sol.cg_sup_adm_resob AS sup_adm_resob,"+
									" sol.cg_numero_telefono AS numero_telefono,"+
									"        sol.cg_obj_contrato AS objeto_contrato, " +
									"        sol.cg_comentarios AS comentarios, " +//FODEA 041 - 2010 ACF
									"        sol.fn_monto_credito AS monto_credito, " +
									"        sol.cg_referencia AS referencia, " +
									"        TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,  " +
									"        sol.cg_banco_deposito AS banco_deposito, " +
									"        sol.cg_cuenta AS cuenta,  " +
									"        sol.cg_cuenta_clabe AS cuenta_clabe,  " +
									"        TO_CHAR(ca.df_operacion, 'dd/mm/yyyy') AS fecha_autrech_pyme,  " +
									"        TO_CHAR(ca2.df_operacion, 'dd/mm/yyyy') AS fecha_autrech_epo,  " +
									"        sts.cg_descripcion AS estatus,  " +
									"			   epo.cg_clasificacion_epo AS nombre_clasificaion_epo " +
                  //"        ctp.cg_descripcion as desc_plazo, " +
                  //"        sol.ig_plazo as plazo " +
									"   FROM cder_solicitud sol, " +
									"        cder_acuse ca,  " +
									"        cder_acuse ca2, " +
									"        comcat_epo epo,  " +
									"        comcat_if cif,  " +
									"        comcat_pyme cp, " +
									//"        comcat_moneda mon, " +
									"        comrel_pyme_epo cpe,  " +
									"        comrel_producto_epo cpepo, " +
									"        cdercat_tipo_contratacion tcn,  " +
									"        cdercat_clasificacion_epo cep,  " +
									" cdercat_ventanilla_pago cvp,"+
									"        cdercat_estatus_solic sts, " +
									"        (select ig_dias_pnotificacion  " +
									"           from comrel_producto_epo  " +
									"          where ic_epo = ?  " +
									"            and ic_producto_nafin = ?) lim, " +
                  "        cdercat_tipo_plazo ctp " + 
									"  WHERE sol.ic_epo = epo.ic_epo  " +
									"    AND sol.ic_if = cif.ic_if " +
									"    AND sol.ic_pyme = cp.ic_pyme  " +
									"    AND sol.ic_epo = cpe.ic_epo(+)" +
									"    AND sol.ic_pyme = cpe.ic_pyme(+)" +
									"    AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion  " +
									"    AND sol.ic_epo = cep.ic_epo  " +
									"    AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo  " +
									"    AND sol.ic_estatus_solic = sts.ic_estatus_solic   " +
									"    AND sol.cc_acuse3 = ca.cc_acuse(+)" +
									"    AND sol.cc_acuse1 = ca2.cc_acuse(+)" +
									"    AND sol.ic_epo = cpepo.ic_epo " +
									"    AND cpepo.ic_producto_nafin = ? " +
									" AND sol.ic_epo = cvp.ic_epo(+)"+
									" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)"+
									"	   AND sol.ic_estatus_solic IN (7, 9, 10, 14) " + 
                  "    AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+) "
								 );
									
									varBind.add(clave_epo);
									varBind.add("9");
									varBind.add("9");
									
								 if(!clave_contrat.equals("")){
									 qrySQL.append("   AND tcn.ic_tipo_contratacion = ? ");
									 varBind.add(clave_contrat);
								 }
								 if(!clave_estatus.equals("")){
									 qrySQL.append("   AND sts.ic_estatus_solic = ? ");
									 varBind.add(clave_estatus);
								 }
								 if(!clave_epo.equals("")){
									 qrySQL.append("   AND epo.ic_epo = ? ");
									 varBind.add(clave_epo);
								 }
								 if(!clave_pyme.equals("")){
									 qrySQL.append("   AND cp.ic_pyme = ? " );
									 varBind.add(clave_pyme);
								 }
								 if(!clave_if.equals("")){
									 qrySQL.append("   AND cif.ic_if = ? " );
									 varBind.add(clave_if);
								 }
								 /*if(!clave_moneda.equals("")){
									 qrySQL.append("   AND mon.ic_moneda = ? ");
									 varBind.add(clave_moneda);
								 }*/
								 if(!desde.equals("") && !hasta.equals("")){
									qrySQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
									qrySQL.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
									varBind.add(desde);
									varBind.add(hasta);
								}
								if(!num_contrato.equals("")){
									 qrySQL.append("   AND sol.cg_no_contrato = ? ");
									 varBind.add(num_contrato);
								}
								if(!lAdicionales.isEmpty()){
									for(int i = 0; i < lAdicionales.size(); i++){
										String campo_adicional = (String)lAdicionales.get(i);
										String tipo = (String)tipo_dato.get(i);
										if(!campo_adicional.equals("")){
											qrySQL.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
											if(tipo_dato.equals("N")){varBind.add(new Long(campo_adicional));}else{varBind.add(campo_adicional);}
										}
									}
								}
								
                if (clave_moneda != null && !"".equals(clave_moneda)) {
                  if (clave_moneda.equals("0")) {
                    qrySQL.append(" AND sol.cs_multimoneda = ?");
                    varBind.add("S");
                  } else {
                    qrySQL.append(" AND sol.cs_multimoneda = ?");
                    qrySQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
                    qrySQL.append(" FROM cder_monto_x_solic mxs");
                    qrySQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
                    qrySQL.append(" AND mxs.ic_moneda = ?)");
                    varBind.add("N");
                    varBind.add(new Integer(clave_moneda));
                  }
                }      
                if(!clave_plazo_contrato.equals("")){
                  qrySQL.append(" AND sol.ic_tipo_plazo = ? ");
                  varBind.add(new Integer(clave_plazo_contrato));
                }
                if(!plazo_contrato.equals("")){
                  qrySQL.append(" AND sol.ig_plazo = ? ");
                  varBind.add(new Integer(plazo_contrato));
                }
                
                qrySQL.append(" ORDER BY sol.df_alta_solicitud DESC");
                
				log.debug("qrySQL ::: "+qrySQL);
				log.debug("varBind ::: "+varBind);
				
				pst = con.queryPrecompilado(qrySQL.toString(), varBind);
				rst = pst.executeQuery();
				
				while(rst.next()){
				HashMap hmRegistrosContratosCes = new HashMap();
				hmRegistrosContratosCes.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				hmRegistrosContratosCes.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				hmRegistrosContratosCes.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				hmRegistrosContratosCes.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				hmRegistrosContratosCes.put("dependencia", rst.getString("dependencia")==null?"":rst.getString("dependencia"));
				hmRegistrosContratosCes.put("cesionario", rst.getString("cesionario")==null?"":rst.getString("cesionario"));
				hmRegistrosContratosCes.put("pyme", rst.getString("pyme")==null?"":rst.getString("pyme"));
				hmRegistrosContratosCes.put("RFC", rst.getString("RFC")==null?"":rst.getString("RFC"));
				hmRegistrosContratosCes.put("numero_proveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				hmRegistrosContratosCes.put("fecha_solicitud", rst.getString("fecha_solicitud")==null?"":rst.getString("fecha_solicitud"));
				hmRegistrosContratosCes.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//hmRegistrosContratosCes.put("monto_contrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				//hmRegistrosContratosCes.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				hmRegistrosContratosCes.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				hmRegistrosContratosCes.put("inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				hmRegistrosContratosCes.put("fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				hmRegistrosContratosCes.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				hmRegistrosContratosCes.put("fecha_limite_notificacion", rst.getString("fecha_limite_notificacion")==null?"":rst.getString("fecha_limite_notificacion"));
				hmRegistrosContratosCes.put("camp_adic_1", rst.getString("camp_adic_1")==null || "null".equals(rst.getString("camp_adic_1")) ? "":rst.getString("camp_adic_1"));
				hmRegistrosContratosCes.put("camp_adic_2", rst.getString("camp_adic_2")==null || "null".equals(rst.getString("camp_adic_2")) ? "":rst.getString("camp_adic_2"));
				hmRegistrosContratosCes.put("camp_adic_3", rst.getString("camp_adic_3")==null || "null".equals(rst.getString("camp_adic_3")) ? "":rst.getString("camp_adic_3"));
				hmRegistrosContratosCes.put("camp_adic_4", rst.getString("camp_adic_4")==null || "null".equals(rst.getString("camp_adic_4")) ? "":rst.getString("camp_adic_4"));
				hmRegistrosContratosCes.put("camp_adic_5", rst.getString("camp_adic_5")==null || "null".equals(rst.getString("camp_adic_5")) ? "":rst.getString("camp_adic_5"));
				hmRegistrosContratosCes.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
				hmRegistrosContratosCes.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
				hmRegistrosContratosCes.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));
				hmRegistrosContratosCes.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				hmRegistrosContratosCes.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));
				hmRegistrosContratosCes.put("monto_credito", rst.getString("monto_credito")==null?"":rst.getString("monto_credito"));
				hmRegistrosContratosCes.put("referencia", rst.getString("referencia")==null?"":rst.getString("referencia"));
				hmRegistrosContratosCes.put("fecha_vencimiento", rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento"));
				hmRegistrosContratosCes.put("banco_deposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				hmRegistrosContratosCes.put("cuenta", rst.getString("cuenta")==null?"":rst.getString("cuenta"));
				hmRegistrosContratosCes.put("cuenta_clabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				hmRegistrosContratosCes.put("fecha_autrech_pyme", rst.getString("fecha_autrech_pyme")==null?"":rst.getString("fecha_autrech_pyme"));
				hmRegistrosContratosCes.put("fecha_autrech_epo", rst.getString("fecha_autrech_epo")==null?"":rst.getString("fecha_autrech_epo"));
				hmRegistrosContratosCes.put("estatus", rst.getString("estatus")==null?"":rst.getString("estatus"));
				hmRegistrosContratosCes.put("nombre_clasificaion_epo", rst.getString("nombre_clasificaion_epo")==null?"":rst.getString("nombre_clasificaion_epo"));
				
        StringBuffer monto_moneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

        hmRegistrosContratosCes.put("monto_moneda", monto_moneda.toString());
				hmRegistrosContratosCes.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
        //hmRegistrosContratosCes.put("desc_plazo", rst.getString("desc_plazo")==null?"":rst.getString("desc_plazo"));
        //hmRegistrosContratosCes.put("plazo", rst.getString("plazo")==null?"":rst.getString("plazo"));
        
        hmConsContratosCes.put("hmRegistrosContratosCes"+index, hmRegistrosContratosCes);
				index++;
				}
				
				hmConsContratosCes.put("index", Integer.toString(index));
				rst.close();
				pst.close();
			}catch(Exception e){
				e.printStackTrace();
				throw new AppException("Error al ejecutar la consulta", e);
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
			}
			log.info("getContratosCesion (S)");
			return hmConsContratosCes;
		}
		
// ������������������������ IA  FODEA 037 - 2009 (F) ���������������������������	

	//..::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: FODEA 037 - 2009 ACF (I)
	/**
	 * M�todo que obtiene el nombre y tipo de dato de los campos adicionales parametrizados
	 * por una epo.
	 * @param clave_epo Clave de la EPO seleccionada.
	 * @param clave_producto Clave del producto que maneja la EPO seleccionada.
	 * @return camposAdicionalesParametrizados Hashmap con los nombre y el tipo de dato parametrizados por la EPO.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap getCamposAdicionalesParametrizados(String clave_epo, String clave_producto) throws AppException{
		log.info("getCamposAdicionalesParametrizados(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap camposAdicionales = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();			      
      
			strSQL.append(" SELECT cg_nombre_campo AS nombre_campo,");
			strSQL.append(" cg_tipo_dato AS tipo_dato,");
			strSQL.append(" ig_longitud AS longitud_campo");
			strSQL.append(" FROM comrel_visor");
			strSQL.append(" WHERE ic_epo = ?");
			strSQL.append(" AND ic_producto_nafin = ?");
			strSQL.append(" ORDER BY ic_no_campo");
			
			varBind.add(new Integer(clave_epo));
			varBind.add(new Integer(clave_producto));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());			
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				camposAdicionales.put("nombre_campo_"+indice, rst.getString("nombre_campo")==null?"":rst.getString("nombre_campo"));
				camposAdicionales.put("tipo_dato_"+indice, rst.getString("tipo_dato")==null?"":rst.getString("tipo_dato"));
				camposAdicionales.put("longitud_campo_"+indice, rst.getString("longitud_campo")==null?"":rst.getString("longitud_campo"));
				indice++;
			}
			
			camposAdicionales.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		}catch(Exception e){
			log.error("Error al consultar base de datos");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getCamposAdicionalesParametrizados(S)");
		}
		return camposAdicionales;
	}

	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Contratos de Cesi�n para la pyme.
	 * @param parametros_consulta HashMap con los parametros con los que se realiza la consulta de contratos de cesi�n.
	 * @return consultaContratosCesion Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultaContratosCesionPyme(HashMap parametros_consulta) throws AppException{
		log.info("consultaContratosCesionPyme(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaContratosCesion = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();
			
			String clave_pyme = parametros_consulta.get("clave_pyme")==null?"":(String)parametros_consulta.get("clave_pyme");
			String clave_epo = parametros_consulta.get("clave_epo")==null?"":(String)parametros_consulta.get("clave_epo");
			String clave_if = parametros_consulta.get("clave_if")==null?"":(String)parametros_consulta.get("clave_if");
			String numero_contrato = parametros_consulta.get("numero_contrato")==null?"":(String)parametros_consulta.get("numero_contrato");
			String claveMoneda = parametros_consulta.get("clave_moneda")==null?"":(String)parametros_consulta.get("clave_moneda");
			String clave_estatus_sol = parametros_consulta.get("clave_estatus_sol")==null?"":(String)parametros_consulta.get("clave_estatus_sol");
			String clave_contratacion = parametros_consulta.get("clave_contratacion")==null?"":(String)parametros_consulta.get("clave_contratacion");
			String fecha_vigencia_ini = parametros_consulta.get("fecha_vigencia_ini")==null?"":(String)parametros_consulta.get("fecha_vigencia_ini");
			String fecha_vigencia_fin = parametros_consulta.get("fecha_vigencia_fin")==null?"":(String)parametros_consulta.get("fecha_vigencia_fin");
			String plazoContrato = parametros_consulta.get("plazoContrato")==null?"":(String)parametros_consulta.get("plazoContrato");
			String claveTipoPlazo = parametros_consulta.get("claveTipoPlazo")==null?"":(String)parametros_consulta.get("claveTipoPlazo");
			List campos_adicionales = parametros_consulta.get("campos_adicionales")==null?new ArrayList():(List)parametros_consulta.get("campos_adicionales");
			List tipo_dato = parametros_consulta.get("tipo_dato")==null?new ArrayList():(List)parametros_consulta.get("tipo_dato");
			String strTipoUsuario = parametros_consulta.get("strTipoUsuario")==null?"":(String)parametros_consulta.get("strTipoUsuario");
		
		
			strSQL.append(" SELECT DISTINCT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_tipo_contratacion AS clave_tipo_contratacion,");
			strSQL.append(" sol.ic_tipo_plazo AS clave_plazo_contrato,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" pyme.cg_razon_social AS nombre_pyme_principal,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
			strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" TO_CHAR(sol.df_fecha_aceptacion, 'dd/mm/yyyy') AS fecha_aceptacion,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" sol.cc_acuse2 AS acuse2,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud,");
			strSQL.append(" TO_CHAR(sol.df_alta_solicitud,'DD/MM/YYYY') as fechasolicitud,");
			strSQL.append(" cif.ic_if  as clave_if,");
			strSQL.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato, ");
			strSQL.append(" TO_CHAR(sol.df_fecha_prorroga, 'dd/mm/yyyy') AS fecha_prorroga, ");
			strSQL.append(" sol.cs_estatus_prorroga AS estatus_prorroga, ");
			strSQL.append(" sol.cg_causas_retorno AS causas_retorno, ");
			strSQL.append(" sol.IN_CONTADOR_PRORROGA AS contador_prorroga, ");
			strSQL.append(" ( CASE WHEN trunc(sol.DF_FECHA_PRORROGA)<trunc(sysdate) then  'true' else 'false' END) termino_prorroga ");
			strSQL.append("	,sol.CG_EMPRESAS_REPRESENTADAS as empresas");
			strSQL.append(" ,tb_num_rep.num_rep AS num_rep ");
			strSQL.append(" ,tb_num_rep.num_firma_rep AS num_firma_rep ");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", comcat_pyme pyme");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
			strSQL.append(", cdercat_tipo_plazo stp");
			strSQL.append(", cdercat_ventanilla_pago cvp");
			strSQL.append(", cder_solic_x_representante csr ");
			strSQL.append(",(SELECT   ic_solicitud, COUNT (*) num_rep, ");
			strSQL.append("         SUM (DECODE (CG_FIRMA_REPRESENTANTE, NULL, 0, 1)) AS num_firma_rep ");
			strSQL.append("    FROM cder_solic_x_representante ");
			strSQL.append("GROUP BY ic_solicitud) tb_num_rep ");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_solicitud = tb_num_rep.ic_solicitud ");
			strSQL.append("AND sol.ic_solicitud = csr.ic_solicitud ");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			
			if (strTipoUsuario.equals("PYME")) {	
				if(!clave_pyme.equals("")){
					strSQL.append(" AND csr.ic_pyme = ?");
					varBind.add(new Long(clave_pyme));
				}
			} 
			
			if(!clave_epo.equals("")){
				strSQL.append(" AND sol.ic_epo = ?");
				varBind.add(new Integer(clave_epo));
			}
			if(!clave_if.equals("")){
				strSQL.append(" AND sol.ic_if = ?");
				varBind.add(new Integer(clave_if));
			}
			if(!numero_contrato.equals("")){
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(numero_contrato);
			}
			/*
			if(!clave_moneda.equals("")){
				strSQL.append(" AND sol.ic_moneda = ?");
				varBind.add(new Integer(clave_moneda));
			}
			*/
			
			if (claveMoneda != null && !"".equals(claveMoneda)) {
				if (claveMoneda.equals("0")) {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("S");
				} else {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.ic_moneda = ?)");
					varBind.add("N");
					varBind.add(new Integer(claveMoneda));
				}
			}
			
			if(!clave_estatus_sol.equals("")){
				strSQL.append(" AND sol.ic_estatus_solic = ?");
				varBind.add(new Integer(clave_estatus_sol));
			} else {
				strSQL.append(" AND sol.ic_estatus_solic IN (?, ?, ?, ?, ?, ?)");
				varBind.add(new Integer(3));
				varBind.add(new Integer(7));
				varBind.add(new Integer(9));
				varBind.add(new Integer(10));
				varBind.add(new Integer(14));
				varBind.add(new Integer(25));//nuevo estatus F023-2015
			}
			if(!clave_contratacion.equals("")){
				strSQL.append(" AND sol.ic_tipo_contratacion = ?");
				varBind.add(new Integer(clave_contratacion));
			}
			if(!fecha_vigencia_ini.equals("") && !fecha_vigencia_fin.equals("")){
				strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
				varBind.add(fecha_vigencia_ini);
				varBind.add(fecha_vigencia_fin);
			}
			
			if (plazoContrato != null && !"".equals(plazoContrato)) {
				strSQL.append(" AND sol.ig_plazo = ?");
				varBind.add(new Integer(plazoContrato));
			}		
	
			if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
				strSQL.append(" AND sol.ic_tipo_plazo = ?");
				varBind.add(new Integer(claveTipoPlazo));
			}
			
			if(!campos_adicionales.isEmpty()){
				for(int i = 0; i < campos_adicionales.size(); i++){
					String campo_adicional = (String)campos_adicionales.get(i);
					String tipo = (String)tipo_dato.get(i);
					if(!campo_adicional.equals("")){
						strSQL.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
						if(tipo_dato.equals("N")){varBind.add(new Long(campo_adicional));}else{varBind.add(campo_adicional);}
					}
				}
			}
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				String strClaveEpo = rst.getString("clave_epo");
				int diasMinimosNotificacion = this.getDiasMinimosNotificacion(strClaveEpo);
				StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));
			
				HashMap contratoCesion = new HashMap();
				contratoCesion.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				contratoCesion.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				contratoCesion.put("clavePyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				contratoCesion.put("claveTipoContratacion", rst.getString("clave_tipo_contratacion")==null?"":rst.getString("clave_tipo_contratacion"));
				contratoCesion.put("nombre_epo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				contratoCesion.put("nombre_if", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
				contratoCesion.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				contratoCesion.put("montosPorMoneda", montosPorMoneda.toString());
				contratoCesion.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				contratoCesion.put("fecha_inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				contratoCesion.put("fecha_fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				contratoCesion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				contratoCesion.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				contratoCesion.put("campo_adicional_1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				contratoCesion.put("campo_adicional_2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				contratoCesion.put("campo_adicional_3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				contratoCesion.put("campo_adicional_4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				contratoCesion.put("campo_adicional_5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				contratoCesion.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
				contratoCesion.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
				contratoCesion.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));				
				contratoCesion.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				contratoCesion.put("fechaAceptacion", rst.getString("fecha_aceptacion")==null?"":rst.getString("fecha_aceptacion"));
				contratoCesion.put("clave_estatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				contratoCesion.put("estatus_solicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				contratoCesion.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));
				contratoCesion.put("diasMinimosNotificacion", Integer.toString(diasMinimosNotificacion));
				contratoCesion.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				contratoCesion.put("firma_contrato", rst.getString("firma_contrato")==null?"":rst.getString("firma_contrato"));
				contratoCesion.put("fecha_prorroga", rst.getString("fecha_prorroga")==null?"":rst.getString("fecha_prorroga"));
				contratoCesion.put("estatus_prorroga", rst.getString("estatus_prorroga")==null?"":rst.getString("estatus_prorroga"));
				contratoCesion.put("causas_retorno", rst.getString("causas_retorno")==null?"":rst.getString("causas_retorno"));
				contratoCesion.put("contador_prorroga", rst.getString("contador_prorroga")==null?"":rst.getString("contador_prorroga"));
				contratoCesion.put("termino_prorroga", rst.getString("termino_prorroga")==null?"":rst.getString("termino_prorroga"));
				contratoCesion.put("empresas", rst.getString("empresas")==null?"":rst.getString("empresas"));
				
				//ACUSE DE ACEPTACION DE LA SOLICITUD DE CONSENTIMIENTO POR PARTE DE LA EPO
				String numeroAcuseEpo = rst.getString("acuse2")==null?"":rst.getString("acuse2");
				if (!numeroAcuseEpo.equals("")) {
					contratoCesion.put("acuseEnvioSolicEpo", rst.getString("acuse2")==null?"":rst.getString("acuse2"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuseEpo);

					if (!datosAcuse.isEmpty()) {
						contratoCesion.put("claveUsrEpoAutCesion", datosAcuse.get("claveUsuario"));
						contratoCesion.put("nombreUsrEpoAutCesion", datosAcuse.get("nombreUsuario"));
						contratoCesion.put("fechaEpoAutCesion", datosAcuse.get("fechaAcuse"));
						contratoCesion.put("horaEpoAutCesion", datosAcuse.get("horaAcuse"));
					}
				}
				contratoCesion.put("num_rep", rst.getString("num_rep")==null?"":rst.getString("num_rep"));
				contratoCesion.put("num_firma_rep", rst.getString("num_firma_rep")==null?"":rst.getString("num_firma_rep"));
				contratoCesion.put("nombre_pyme_principal", rst.getString("nombre_pyme_principal")==null?"":rst.getString("nombre_pyme_principal"));
				consultaContratosCesion.put("contratoCesion"+indice, contratoCesion);
				indice++;
			}
			
			consultaContratosCesion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		}catch(Exception e){
			log.error("Error al consultar base de datos");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("consultaContratosCesionPyme(S)");
		}
		return consultaContratosCesion;
	}

	public HashMap consultaContratoCesionPyme(String clave_solicitud) throws AppException{
		return consultaContratoCesionPyme(clave_solicitud ,"");
	}

	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Contratos de Cesi�n para la pyme.
	 * @param clave_solicitud Clave de la solicitud de la que se realiza la consulta.
	 * @return consultaContratoCesion Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultaContratoCesionPyme(String clave_solicitud, String cgUsuario) throws AppException{
		log.info("consultaContratoCesionPyme(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaContratoCesion = new HashMap();
		String numeroAcuse = "";
		int indice = 0;

		try {
			con.conexionDB();

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" sol.ic_tipo_plazo AS clave_plazo_contrato,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme, ");
			strSQL.append(" pym.cg_rfc AS rfc_pyme,");
			strSQL.append(" crpe.cg_pyme_epo_interno AS numero_proveedor,");
			strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud_consentimiento,");
			strSQL.append(" TO_CHAR(SYSDATE, 'dd/mm/yyyy') AS fecha_formal_contrato,");
			strSQL.append(" TO_CHAR(sol.df_notif_vent, 'dd/mm/yyyy') AS fecha_notif_vent,");
			strSQL.append(" TO_CHAR(sol.df_redireccion, 'dd/mm/yyyy') AS fecha_redireccion,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
			strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
			strSQL.append(" sol.cg_cuenta_clabe AS cuenta_clabe,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" TO_CHAR(sol.df_fecha_limite_firma, 'dd/mm/yyyy') AS fecha_limite_firma,");
			strSQL.append(" TO_CHAR(sol.df_fecha_aceptacion, 'dd/mm/yyyy') AS fecha_aceptacion_epo,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
			strSQL.append(" sol.cc_acuse2 AS acuse2,");//ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			strSQL.append(" sol.cc_acuse3 AS acuse3,");//ESTE ES EL ACUSE DE LA PYME CUANDO FIRMA EL ECONTRATO
			strSQL.append(" sol.cc_acuse4 AS acuse4,");//ESTE ES EL ACUSE DEL IF CUANDO FIRMA EL ECONTRATO
			strSQL.append(" sol.cc_acuse5 AS acuse5,");//ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA LA NOTIFICACION
			strSQL.append(" sol.cc_acuse6 AS acuse6,");//ESTE ES EL ACUSE DEl REPRESENTANTE IF 2
			if(!"".equals(cgUsuario)){
				strSQL.append(" csr.cc_acuse_firma_rep as acuse_firma_rep1, ");
			}else{
			strSQL.append(" sol.cc_acuse_firma_rep1 AS acuse_firma_rep1,");
			}
			strSQL.append(" sol.cc_acuse_firma_rep2 AS acuse_firma_rep2,");
			strSQL.append(" sol.cc_acuse_testigo1 AS acuse_firma_test1,");
			strSQL.append(" sol.cc_acuse_testigo2 AS acuse_firma_test2,");
			strSQL.append(" sol.cc_acuse_redirec AS acuse_redirec,");
			strSQL.append(" sol.cg_firma1 AS firma1,");
			strSQL.append(" sol.cg_firma2 AS firma2,");//FIRMA DEL IF
			strSQL.append(" sol.cg_firma3 AS firma3,");//FIRMA DEL PRIMER TESTIGO
			strSQL.append(" sol.cg_firma4 AS firma4,");//FIRMA DEL SEGUNDO TESTIGO
			strSQL.append(" sol.cg_firma5 AS firma5,");//FIRMA DEL Representante IF 2
			if(!"".equals(cgUsuario)){
				strSQL.append(" csr.cg_firma_representante as firma_rep1, ");
			}else{
			strSQL.append(" sol.cg_firma_rep1 AS firma_rep1,");//FIRMA DEL PRIMER REPRESENTANTE LEGAL PYME
			}
			strSQL.append(" sol.cg_firma_rep2 AS firma_rep2,");//FIRMA DEL SEGUNDO REPRESENTANTE LEGAL PYME
			strSQL.append(" TO_CHAR(sol.df_firma_contrato,'dd/mm/yyyy') AS firma_contrato,");//FIRMA DEL CONTRATO
			strSQL.append(" TO_CHAR(sol.df_firma_testigo2, 'dd/mm/yyyy') AS fecha_firma_testigo2,");//Firma del segundo testigo
			strSQL.append(" TO_CHAR (sol.df_firma_testigo2, 'dd')||' d�as del mes de '||TO_CHAR (sol.df_firma_testigo2, 'MONTH', 'NLS_DATE_LANGUAGE=SPANISH')  ||' de '|| TO_CHAR (sol.df_firma_testigo2, 'yyyy')  as char_fecha_testigo2, ");  
			strSQL.append(" TO_CHAR (sol.df_firma_if, 'dd')||' d�as del mes de '||TO_CHAR (sol.df_firma_if, 'MONTH', 'NLS_DATE_LANGUAGE=SPANISH')  ||' de '|| TO_CHAR (sol.df_firma_if, 'yyyy')  as char_fecha_if, ");  
			strSQL.append(" NVL(sol.CG_EMPRESAS_REPRESENTADAS,'') as empresas  ");  
		   strSQL.append(" , sol.ic_grupo_cesion  as  ic_grupo_cesion  ");      
		         
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comrel_pyme_epo crpe");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			//strSQL.append(", comcat_moneda mon");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
			strSQL.append(", cdercat_tipo_plazo stp");
			if(!"".equals(cgUsuario)){
				strSQL.append(", cder_solic_x_representante csr ");
			}
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_pyme = crpe.ic_pyme(+)");
			strSQL.append(" AND sol.ic_epo = crpe.ic_epo(+)");
			//strSQL.append(" AND sol.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			if(!"".equals(cgUsuario)){
				strSQL.append("And sol.ic_solicitud = csr.ic_solicitud(+) ");
				strSQL.append("   AND csr.ic_usuario(+) = ? ");
			}
			strSQL.append(" AND sol.ic_solicitud = ?");
			if(!"".equals(cgUsuario)){
				varBind.add(cgUsuario);
			}
			varBind.add(new Long(clave_solicitud));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				StringBuffer montosPorMoneda = this.getMontoMoneda(clave_solicitud);
				consultaContratoCesion.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				consultaContratoCesion.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				consultaContratoCesion.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				consultaContratoCesion.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				consultaContratoCesion.put("nombre_pyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				consultaContratoCesion.put("rfcPyme", rst.getString("rfc_pyme")==null?"":rst.getString("rfc_pyme"));
				consultaContratoCesion.put("numeroProveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				consultaContratoCesion.put("fechaSolicitudConsentimiento", rst.getString("fecha_solicitud_consentimiento")==null?"":rst.getString("fecha_solicitud_consentimiento"));
				consultaContratoCesion.put("fechaNotifVentanilla", rst.getString("fecha_notif_vent")==null?"":rst.getString("fecha_notif_vent"));
				consultaContratoCesion.put("fechaRedirecccion", rst.getString("fecha_redireccion")==null?"":rst.getString("fecha_redireccion"));
				consultaContratoCesion.put("nombre_epo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				consultaContratoCesion.put("nombre_if", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
				consultaContratoCesion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				consultaContratoCesion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				consultaContratoCesion.put("numeroCuentaClabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				consultaContratoCesion.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//consultaContratoCesion.put("monto_contrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				//consultaContratoCesion.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				consultaContratoCesion.put("montosPorMoneda", montosPorMoneda.toString());
				consultaContratoCesion.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				consultaContratoCesion.put("fecha_inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				consultaContratoCesion.put("fecha_fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				consultaContratoCesion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				consultaContratoCesion.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				consultaContratoCesion.put("campo_adicional_1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				consultaContratoCesion.put("campo_adicional_2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				consultaContratoCesion.put("campo_adicional_3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				consultaContratoCesion.put("campo_adicional_4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				consultaContratoCesion.put("campo_adicional_5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				consultaContratoCesion.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				consultaContratoCesion.put("fecha_limite_firma", rst.getString("fecha_limite_firma")==null?"":rst.getString("fecha_limite_firma"));
				consultaContratoCesion.put("fechaAceptacionEpo", rst.getString("fecha_aceptacion_epo")==null?"":rst.getString("fecha_aceptacion_epo"));
				consultaContratoCesion.put("clave_estatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				consultaContratoCesion.put("estatus_solicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				consultaContratoCesion.put("firma_contrato", rst.getString("firma_contrato")==null?"":rst.getString("firma_contrato"));
				consultaContratoCesion.put("fecha_firma_testigo2", rst.getString("fecha_firma_testigo2")==null?"":rst.getString("fecha_firma_testigo2"));
				consultaContratoCesion.put("char_fecha_testigo2", rst.getString("char_fecha_testigo2")==null?"":rst.getString("char_fecha_testigo2"));
				consultaContratoCesion.put("char_fecha_if", rst.getString("char_fecha_if")==null?"":rst.getString("char_fecha_if"));
				consultaContratoCesion.put("CG_EMPRESAS_REPRESENTADAS", rst.getString("empresas")==null?"":rst.getString("empresas"));
			   consultaContratoCesion.put("ic_grupo_cesion", rst.getString("ic_grupo_cesion")==null?"":rst.getString("ic_grupo_cesion"));
			               
				//ACUSE DE ACEPTACION DE LA SOLICITUD DE CONSENTIMIENTO POR PARTE DE LA EPO fecha_firma_testigo2
				numeroAcuse = rst.getString("acuse2")==null?"":rst.getString("acuse2");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseEnvioSolicEpo", rst.getString("acuse2")==null?"":rst.getString("acuse2"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrEpoAutCesion", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrEpoAutCesion", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaEpoAutCesion", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaEpoAutCesion", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE CONSENTIMIENTO POR EL PRIMER REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("acuse_firma_rep1")==null?"":rst.getString("acuse_firma_rep1");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaRep1", rst.getString("acuse_firma_rep1")==null?"":rst.getString("acuse_firma_rep1"));
					consultaContratoCesion.put("firmaRep1", rst.getString("firma_rep1")==null?"":rst.getString("firma_rep1"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrAutRep1", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrAutRep1", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaAutRep1", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaAutRep1", datosAcuse.get("horaAcuse"));
						consultaContratoCesion.put("fechaFormalContrato", datosAcuse.get("fechaAcuse"));
					}
				} else {
					numeroAcuse = rst.getString("acuse3")==null?"":rst.getString("acuse3");
						if (!numeroAcuse.equals("")) {
						consultaContratoCesion.put("acuseFirmaRep1", rst.getString("acuse3")==null?"":rst.getString("acuse3"));
						consultaContratoCesion.put("firmaRep1", rst.getString("firma1")==null?"":rst.getString("firma1"));
						
						HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);
	
						if (!datosAcuse.isEmpty()) {
							consultaContratoCesion.put("claveUsrAutRep1", datosAcuse.get("claveUsuario"));
							consultaContratoCesion.put("nombreUsrAutRep1", datosAcuse.get("nombreUsuario"));
							consultaContratoCesion.put("fechaAutRep1", datosAcuse.get("fechaAcuse"));
							consultaContratoCesion.put("horaAutRep1", datosAcuse.get("horaAcuse"));
							consultaContratoCesion.put("fechaFormalContrato", datosAcuse.get("fechaAcuse"));
						}
					}
				}
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE CONSENTIMIENTO POR EL SEGUNDO REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("acuse_firma_rep2")==null?"":rst.getString("acuse_firma_rep2");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaRep2", rst.getString("acuse_firma_rep2")==null?"":rst.getString("acuse_firma_rep2"));
					consultaContratoCesion.put("firmaRep2", rst.getString("firma_rep2")==null?"":rst.getString("firma_rep2"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrAutRep2", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrAutRep2", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaAutRep2", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaAutRep2", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL IF
				numeroAcuse = rst.getString("acuse4")==null?"":rst.getString("acuse4");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaIf", rst.getString("acuse4")==null?"":rst.getString("acuse4"));
					consultaContratoCesion.put("firmaIf", rst.getString("firma2")==null?"":rst.getString("firma2"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrIf", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrIf", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaIf", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaIf", datosAcuse.get("horaAcuse"));
					}
				}
				
				//Acuse Aceptacion IF REP 2
				numeroAcuse = rst.getString("acuse6")==null?"":rst.getString("acuse6");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaIfRep2", rst.getString("acuse6")==null?"":rst.getString("acuse6"));
					consultaContratoCesion.put("firmaIfRep2", rst.getString("firma5")==null?"":rst.getString("firma5"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrIfRep2", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrIfRep2", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaIfRep2", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaIfRep2", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL PRIMER TESTIGO DEL IF
				numeroAcuse = rst.getString("acuse_firma_test1")==null?"":rst.getString("acuse_firma_test1");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaTest1", rst.getString("acuse_firma_test1")==null?"":rst.getString("acuse_firma_test1"));
					consultaContratoCesion.put("firmaTest1", rst.getString("firma3")==null?"":rst.getString("firma3"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrTest1", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrTest1", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaTest1", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaTest1", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL SEGUNDO TESTIGO DEL IF
				numeroAcuse = rst.getString("acuse_firma_test2")==null?"":rst.getString("acuse_firma_test2");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaTest2", rst.getString("acuse_firma_test2")==null?"":rst.getString("acuse_firma_test2"));
					consultaContratoCesion.put("firmaTest2", rst.getString("firma4")==null?"":rst.getString("firma4"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrTest2", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrTest2", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaTest2", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaTest2", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DE LA NOTIFICACION DEL CONTRATO DE CESI�N POR PARTE DE LA EPO
				numeroAcuse = rst.getString("acuse5")==null?"":rst.getString("acuse5");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseNotificacionEpo", rst.getString("acuse5")==null?"":rst.getString("acuse5"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrNotifEpo", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrNotifEpo", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaNotifEpo", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaNotifEpo", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE REDIRECCIONAMIENTO DE LA CTA DE LA EPO POR LA VENTANILLA
				numeroAcuse = rst.getString("acuse_redirec")==null?"":rst.getString("acuse_redirec");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseRedireccion", rst.getString("acuse_redirec")==null?"":rst.getString("acuse_redirec"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrRedirec", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrRedirec", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaRedirec", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaRedirec", datosAcuse.get("horaAcuse"));
					}
				}		
				indice++;
			}
			
			consultaContratoCesion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		}catch(Exception e){
			log.error("Error al consultar base de datos");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("consultaContratoCesionPyme(S)");
		}
		return consultaContratoCesion;
	}

	/**
	 * M�todo que obtiene los datos correspondientes a un determinado numero de acuse.
	 * @param numeroAcuse N�mero de acuse del que se obtiene la informaci�n.
	 * @return HashMap HashMap con el resultado de la consulta.
	 * @throws netropology.utilerias.AppException Al encontrar un error inesperado
	 */
	private HashMap obtenerDatosAcuse(String numeroAcuse) throws AppException{
		log.info("obtenerDatosAcuse(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap datosAcuse = new HashMap();
		
		try{
			con.conexionDB();

			strSQL.append(" SELECT cg_usuario clave_usuario,");
			strSQL.append(" cg_nombre_usuario nombre_usuario,");
			strSQL.append(" TO_CHAR(df_operacion, 'dd/mm/yyyy') fecha_acuse,");
			strSQL.append(" TO_CHAR(df_operacion, 'HH:MI AM') hora_acuse");
			strSQL.append(" FROM cder_acuse");
			strSQL.append(" WHERE cc_acuse = ?");
			
			varBind.add(new Integer(numeroAcuse));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while (rst.next()) {
				datosAcuse.put("claveUsuario", rst.getString("clave_usuario")==null?"":rst.getString("clave_usuario"));
				datosAcuse.put("nombreUsuario", rst.getString("nombre_usuario")==null?"":rst.getString("nombre_usuario"));
				datosAcuse.put("fechaAcuse", rst.getString("fecha_acuse")==null?"":rst.getString("fecha_acuse"));
				datosAcuse.put("horaAcuse", rst.getString("hora_acuse")==null?"":rst.getString("hora_acuse"));
			}
	
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar la base de datos: ", e);
		}finally{
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerDatosAcuse(S) ::..");
		}
		return datosAcuse;
	}
	
	/**
	 * Obtiene de la base de datos el archivo del contrato de cesi�n de derechos 
	 * y lo prepara para su descarga desde la pantalla de contratos de cesi�n.
	 * @param clave_solicitud Es la clave de la solicitud de la que se obtiene el archivo del contrato de cesi�n.
	 * @param strDirectorioTemp Es el directorio donde se genera de manera temporal el archivo.
	 * @return file El archivo del contrato de cesi�n obtenido de la base de datos, este archivo est� en formato PDF.
	 * @throws netropology.utilerias.AppException Al encontrar un error inesperado
	 */
	public String descargaContratoCesion(String clave_solicitud, String strDirectorioTemp) throws AppException{
		log.info("descargaContratoCesion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "";
		String nombreArchivo = "";
		
		try{
			con.conexionDB();

			strSQL.append(" SELECT bi_documento");
			strSQL.append(" FROM cder_solicitud");
			strSQL.append(" WHERE ic_solicitud = ?");
			
			varBind.add(new Long(clave_solicitud));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){
				nombreArchivo = Comunes.cadenaAleatoria(8) + ".pdf";
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob("bi_documento");
				InputStream inStream = blob.getBinaryStream();
				int size = (int)blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1){
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("descargaContratoCesion(E)");
		}
		//return pathname;
		return nombreArchivo;
	}
	
	/**
	 * M�todo que obtiene el clausulado legal del contrato de cesi�n de derechos parametrizado por
	 * el intermediario financiero.
	 * @param clave_if clave interna del intermediario financiero
	 * @return clausulado_parametrizado cadena con el clausulado parametrizado por el IF obtenido de la base
	 * de datos.
	 * @throws AppException cuando ocurre un error al realizar la consulta.
	 */
	 public StringBuffer consultaClausuladoParametrizado(String clave_if) throws AppException{
		log.info("consultaClausuladoParametrizado(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		StringBuffer clausulado_parametrizado = new StringBuffer();
		try{
			con.conexionDB();
			
			strSQL.append(" SELECT lt_clausulado_cesion as clausulado_parametrizado");
			strSQL.append("   FROM cdercat_clausulado ");
			strSQL.append("  WHERE ic_if = ? ");
			
			varBind.add(new Integer(clave_if));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){clausulado_parametrizado.append(rst.getString("clausulado_parametrizado")==null?"":rst.getString("clausulado_parametrizado"));}
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar el clausulado parametrizado: ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("consultaClausuladoParametrizado(S)");
		}
		return clausulado_parametrizado;
	 }
	 
	/**
	 * M�todo que guarda la firma de la solicitud de cesi�n de derechos por parte de la pyme.
	 * @param parametrosFirma HashMap con los parametros con los que se reliza la autenticaci�n del usuario.
	 * @return Acuse Cadena con el numero de acuse generado al autenticar al usuario.
	 * @throws Appexception Cuando ocurre un error al obtener el acuse y/o guardar la firma de la pyme.
	 */
	public String acuseFirmaContratoCesionPyme(HashMap parametrosFirma) throws AppException{
		log.info("acuseFirmaContratoCesionPyme(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		String acuse = "";
			
		String _serial = parametrosFirma.get("_serial")==null?"":(String)parametrosFirma.get("_serial");
		String texto_plano = parametrosFirma.get("texto_plano")==null?"":(String)parametrosFirma.get("texto_plano");
		String pkcs7 = parametrosFirma.get("pkcs7")==null?"":(String)parametrosFirma.get("pkcs7");
		String folioCert = parametrosFirma.get("folioCert")==null?"":(String)parametrosFirma.get("folioCert");
		boolean validCert = Boolean.getBoolean((String)parametrosFirma.get("validCert"));
		String clave_solicitud = parametrosFirma.get("clave_solicitud")==null?"":(String)parametrosFirma.get("clave_solicitud");
		String nuevoEstatus = parametrosFirma.get("nuevoEstatus")==null?"":(String)parametrosFirma.get("nuevoEstatus");
		String login_usuario = parametrosFirma.get("login_usuario")==null?"":(String)parametrosFirma.get("login_usuario");
		String nombre_usuario = parametrosFirma.get("nombre_usuario")==null?"":(String)parametrosFirma.get("nombre_usuario");
		String sello_digital = parametrosFirma.get("sello_digital")==null?"":(String)parametrosFirma.get("sello_digital");
		String fecha_carga = parametrosFirma.get("fecha_carga")==null?"":(String)parametrosFirma.get("fecha_carga");
		String hora_carga = parametrosFirma.get("hora_carga")==null?"":(String)parametrosFirma.get("hora_carga");
		String strDirectorioTemp = parametrosFirma.get("strDirectorioTemp")==null?"":(String)parametrosFirma.get("strDirectorioTemp");
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		
		/*log.debug("_serial "+_serial);
		log.debug("texto_plano "+texto_plano);
		log.debug("pkcs7 "+pkcs7);
		log.debug("folioCert "+folioCert);
		log.debug("validCert "+validCert);
		log.debug("clave_solicitud "+clave_solicitud);
		log.debug("nuevoEstatus "+nuevoEstatus);
		log.debug("login_usuario "+login_usuario);
		log.debug("nombre_usuario "+nombre_usuario);
		log.debug("sello_digital "+sello_digital);
		log.debug("fecha_carga "+fecha_carga);
		log.debug("hora_carga "+hora_carga);
		*/
		boolean commit = true;
		PreparedStatement ps = null;
	
		try{
			con.conexionDB();

			char getReceipt = 'Y';
			if( ! _serial.equals("") && !texto_plano.equals("") && !pkcs7.equals("") ){
				Seguridad s = new Seguridad();
				
				if( s.autenticar(folioCert, _serial, pkcs7, texto_plano, getReceipt) ){
					acuse = s.getAcuse();						
					
					String acuseRepresentante1 = "";
					String ic_if="";
					strSQL = new StringBuffer();
					varBind = new ArrayList();
		
					/*
					strSQL.append(" SELECT cc_acuse_firma_rep1 AS acuse_rep1,ic_if");
					strSQL.append(" FROM cder_solicitud");
					strSQL.append(" WHERE ic_solicitud = ?");
					

					varBind.add(new Long(clave_solicitud));
		
					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind.toString());
					
					PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ResultSet rst1 = pst1.executeQuery();
					
					while (rst1.next()) {
						acuseRepresentante1 = rst1.getString("acuse_rep1")==null?"":rst1.getString("acuse_rep1");
						ic_if = rst1.getString("ic_if")==null?"":rst1.getString("ic_if");
					}
					
					rst1.close();
					pst1.close();
					*/
					//-----------------------------------
					PreparedStatement pst1 = null;
					ResultSet rst1 = null;
					String cuentaClabe= "";
					if(!ic_if.equals("")){
							strSQL = new StringBuffer();
							varBind = new ArrayList();
				
							strSQL.append(" SELECT CG_CUENTA_CLABE ");
							strSQL.append(" FROM COMCAT_CESIONARIO ");
							strSQL.append(" WHERE ic_if = ?");
							
							varBind.add(new Long(ic_if));
				
							log.debug("..:: strSQL : "+strSQL.toString());
							log.debug("..:: varBind : "+varBind.toString());
							
							 pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
							 rst1 = pst1.executeQuery();
							
							while (rst1.next()) {
								cuentaClabe = rst1.getString("CG_CUENTA_CLABE")==null?"":rst1.getString("CG_CUENTA_CLABE");
							}
							
							rst1.close();
							pst1.close();
					}
					/*
					if (!acuseRepresentante1.equals("")) {
						String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
						if (rs1.next()) {
							acuseRepresentante1 = rs1.getString(1)==null?"1":rs1.getString(1);
						}
						ps1.close();
						rs1.close();
						
						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" UPDATE cder_solicitud  "+						
						" SET cc_acuse_firma_rep2 = ?, "+
						" ic_estatus_solic = ?, "+
						" cg_firma_rep2 = ?, "+
						" df_firma_rep2 = SYSDATE, "+
						" CG_CUENTA_CLABE = ?  "+
						" WHERE ic_solicitud = ?"); 
	
						varBind.add(new Integer(acuseRepresentante1));	
						varBind.add(new Integer(nuevoEstatus));
						varBind.add(sello_digital);
						varBind.add(cuentaClabe);
						varBind.add(new Long(clave_solicitud));
						
						
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
					} else {
						String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
						if (rs1.next()) {
							acuseRepresentante1 = rs1.getString(1)==null?"1":rs1.getString(1);
						}
						ps1.close();
						rs1.close();
						
						strSQL = new StringBuffer();
						varBind = new ArrayList();
					
						strSQL.append(" UPDATE cder_solicitud SET cc_acuse_firma_rep1 = ?, ic_estatus_solic = ?, cg_firma_rep1 = ?, df_firma_rep1 = SYSDATE, CG_CUENTA_CLABE = ? WHERE ic_solicitud = ?");
	
						varBind.add(new Integer(acuseRepresentante1));	
						varBind.add(new Integer(nuevoEstatus));
						varBind.add(sello_digital);
						varBind.add(cuentaClabe);
						varBind.add(new Long(clave_solicitud));
						
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
					}
					*/

					//----------------

					String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
					ps1 = con.queryPrecompilado(qry);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						acuseRepresentante1 = rs1.getString(1)==null?"1":rs1.getString(1);
					}
					ps1.close();
					rs1.close();


					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" UPDATE cder_solicitud  "+
					" SET ic_estatus_solic = ?, "+
					" CG_CUENTA_CLABE = ?  "+
					" WHERE ic_solicitud = ?");

					varBind.add(new Integer(nuevoEstatus));
					varBind.add(cuentaClabe);
					varBind.add(new Long(clave_solicitud));

					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" UPDATE CDER_SOLIC_X_REPRESENTANTE");
					strSQL.append(" SET CG_FIRMA_REPRESENTANTE = ?, ");
					strSQL.append(" CC_ACUSE_FIRMA_REP = ?, ");
					strSQL.append(" DF_FIRMA_REPRESENTANTE = SYSDATE ");
					strSQL.append(" WHERE IC_SOLICITUD = ? ");
					strSQL.append(" AND IC_USUARIO = ?");

					varBind.add(sello_digital);
					varBind.add(new Integer(acuseRepresentante1));
					varBind.add(clave_solicitud);
					varBind.add(login_usuario);

					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(acuseRepresentante1)); 
					varBind.add(login_usuario);
					varBind.add(nombre_usuario);
					varBind.add(acuse);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
					con.terminaTransaccion(true);
					String nombreArchivo= this.generarContrato(clave_solicitud,strDirectorioTemp);
					File archivoPDF = new File(strDirectorioTemp+nombreArchivo);
					FileInputStream fileinputstream = new FileInputStream(archivoPDF);
					StringBuffer qrySQLArchivo = new StringBuffer("");
					qrySQLArchivo.append("UPDATE cder_solicitud SET BI_CONTRATO_CESION = ?, CS_BANDERA_CONTRATO = 'S'  WHERE ic_solicitud = ?");
					ps = con.queryPrecompilado(qrySQLArchivo.toString());
					ps.setBinaryStream(1, fileinputstream, (int)archivoPDF.length());
					
					ps.setInt(2, Integer.parseInt(clave_solicitud));
					ps.executeUpdate();
					ps.close();
					fileinputstream.close();
					
				}else {
					acuse ="|"+s.mostrarError();					
				}
			}
			
			
		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error al firmar el contrato de ceci�n de derechos: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseFirmaContratoCesionPyme(S)");
		}
		return acuse;
	}
	
	/**
	 * M�todo que genera el combo de pymes que tienen solicitudes pendientes de autorizaci�n
	 * por el Intermediario financiero.
	 * @param clave_if Clave interna del IF que tiene solicitudes pendientes por autorizar con alguna pyme.
	 * @return combo_pyme Lista de objetos ElementoCatalogo con las pymes seleccionadas.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	 public List getComboPymesContratoCesion(String clave_if) throws AppException{
		log.info("getComboPymesContratoCesion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List combo_pyme = new ArrayList();
		try{
			con.conexionDB();
			
			strSQL.append(" SELECT DISTINCT pyme.ic_pyme AS clave,");
			strSQL.append(" pyme.cg_razon_social AS descripcion");
			strSQL.append(" FROM cder_solicitud cds,");
			strSQL.append(" comcat_pyme pyme");
			strSQL.append(" WHERE cds.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND cds.ic_if = ?");
			strSQL.append(" AND cds.ic_estatus_solic IN (?, ?, ?, ?)");
			strSQL.append(" ORDER BY pyme.cg_razon_social");

			varBind.add(new Integer(clave_if));
			varBind.add(new Integer(7));
			varBind.add(new Integer(9));
			varBind.add(new Integer(10));
			varBind.add(new Integer(12));
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while(rst.next()){
				ElementoCatalogo elemento_catalogo = new ElementoCatalogo();
				elemento_catalogo.setClave(rst.getString("clave")==null?"":rst.getString("clave"));
				elemento_catalogo.setDescripcion(rst.getString("descripcion")==null?"":rst.getString("descripcion"));
				combo_pyme.add(elemento_catalogo);
			}
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getComboPymesContratoCesion(S)");
		}
		return combo_pyme;
	}
	
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Contratos de Cesi�n para el IF.
	 * @param parametros_consulta HashMap con los parametros con los que se realiza la consulta de contratos de cesi�n.
	 * @return consultaContratosCesion Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultaContratosCesionIf(HashMap parametros_consulta) throws AppException{
		log.info("consultaContratosCesionIf(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaContratosCesion = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();
			
			String clave_if = parametros_consulta.get("clave_if")==null?"":(String)parametros_consulta.get("clave_if");
			String clave_epo = parametros_consulta.get("clave_epo")==null?"":(String)parametros_consulta.get("clave_epo");
			String clave_pyme = parametros_consulta.get("clave_pyme")==null?"":(String)parametros_consulta.get("clave_pyme");
			String numero_contrato = parametros_consulta.get("numero_contrato")==null?"":(String)parametros_consulta.get("numero_contrato");
			String clave_moneda = parametros_consulta.get("clave_moneda")==null?"":(String)parametros_consulta.get("clave_moneda");
			String clave_estatus_sol = parametros_consulta.get("clave_estatus_sol")==null?"":(String)parametros_consulta.get("clave_estatus_sol");
			String clave_contratacion = parametros_consulta.get("clave_contratacion")==null?"":(String)parametros_consulta.get("clave_contratacion");
			String fecha_vigencia_ini = parametros_consulta.get("fecha_vigencia_ini")==null?"":(String)parametros_consulta.get("fecha_vigencia_ini");
			String fecha_vigencia_fin = parametros_consulta.get("fecha_vigencia_fin")==null?"":(String)parametros_consulta.get("fecha_vigencia_fin");
			List campos_adicionales = parametros_consulta.get("campos_adicionales")==null?new ArrayList():(List)parametros_consulta.get("campos_adicionales");
			List tipo_dato = parametros_consulta.get("tipo_dato")==null?new ArrayList():(List)parametros_consulta.get("tipo_dato");
      String clave_plazo_contrato = parametros_consulta.get("clave_plazo_contrato")==null?"":(String)parametros_consulta.get("clave_plazo_contrato");
      String plazo_contrato = parametros_consulta.get("plazo_contrato")==null?"":(String)parametros_consulta.get("plazo_contrato");
      
			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_tipo_contratacion AS clave_tipo_contratacion,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			strSQL.append(" pym.cg_rfc AS rfc,");
			strSQL.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
			strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" TO_CHAR(sol.df_fecha_aceptacion, 'dd/mm/yyyy') AS fecha_aceptacion,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
			strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" sol.fn_monto_credito AS monto_credito,");
			strSQL.append(" sol.cg_referencia AS numero_referencia,");
			strSQL.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,");
			strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
			strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
			strSQL.append(" sol.cg_cuenta_clabe AS clabe,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud,");
			strSQL.append(" sol.cs_firmado_if AS firmado_if,");
			strSQL.append(" TO_CHAR(sol.df_firma_testigo1, 'dd/mm/yyyy') AS fecha_firma_testigo1,");
			strSQL.append(" TO_CHAR(sol.df_firma_testigo2, 'dd/mm/yyyy') AS fecha_firma_testigo2,");
      strSQL.append(" cda.cg_nombre_usuario as nombre_usuario");
		
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comrel_pyme_epo cpe");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_ventanilla_pago cvp");
			strSQL.append(", cdercat_estatus_solic sts");
      strSQL.append(", cder_acuse cda");      
      strSQL.append(", cdercat_tipo_plazo ctp");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_epo = cpe.ic_epo(+)");
			strSQL.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
      strSQL.append(" AND sol.cc_acuse2 = cda.cc_acuse(+)");
      strSQL.append(" AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			if(!clave_if.equals("")){
				strSQL.append(" AND sol.ic_if = ?");
				varBind.add(new Integer(clave_if));
			}
			if(!clave_epo.equals("")){
				strSQL.append(" AND sol.ic_epo = ?");
				varBind.add(new Integer(clave_epo));
			}
			if(!clave_pyme.equals("")){
				strSQL.append(" AND sol.ic_pyme = ?");
				varBind.add(new Long(clave_pyme));
			}
			if(!numero_contrato.equals("")){
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(numero_contrato);
			}     
			if(!clave_estatus_sol.equals("")){
				strSQL.append(" AND sol.ic_estatus_solic = ?");
				varBind.add(new Integer(clave_estatus_sol));
			}else{
				strSQL.append(" AND sol.ic_estatus_solic IN (?, ?, ?, ?)");
				varBind.add(new Integer(7));
				varBind.add(new Integer(9));
				varBind.add(new Integer(10));
				varBind.add(new Integer(12));
			}
			if(!clave_contratacion.equals("")){
				strSQL.append(" AND sol.ic_tipo_contratacion = ?");
				varBind.add(new Integer(clave_contratacion));
			}
			if(!fecha_vigencia_ini.equals("") && !fecha_vigencia_fin.equals("")){
				strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
				varBind.add(fecha_vigencia_ini);
				varBind.add(fecha_vigencia_fin);
			}
			if(!campos_adicionales.isEmpty()){
				for(int i = 0; i < campos_adicionales.size(); i++){
					String campo_adicional = (String)campos_adicionales.get(i);
					String tipo = (String)tipo_dato.get(i);
					if(!campo_adicional.equals("")){
						strSQL.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
						if(tipo_dato.equals("N")){varBind.add(new Long(campo_adicional));}else{varBind.add(campo_adicional);}
					}
				}
			}
      if (clave_moneda != null && !"".equals(clave_moneda)) {
				if (clave_moneda.equals("0")) {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("S");
				} else {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.ic_moneda = ?)");
					varBind.add("N");
					varBind.add(new Integer(clave_moneda));
				}
			}      
      if(!clave_plazo_contrato.equals("")){
        strSQL.append(" AND sol.ic_tipo_plazo = ? ");
        varBind.add(new Integer(clave_plazo_contrato));
      }
      if(!plazo_contrato.equals("")){
        strSQL.append(" AND sol.ig_plazo = ? ");
        varBind.add(new Integer(plazo_contrato));
      }
			strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");
      
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				String claveEpo = rst.getString("clave_epo");
				int diasMinimosNotificacion = this.getDiasMinimosNotificacion(claveEpo);
				
				HashMap contratoCesion = new HashMap();
				contratoCesion.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				contratoCesion.put("nombre_epo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				contratoCesion.put("nombre_pyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				contratoCesion.put("rfc", rst.getString("rfc")==null?"":rst.getString("rfc"));
				contratoCesion.put("numero_proveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				contratoCesion.put("fecha_sol_pyme", rst.getString("fecha_sol_pyme")==null?"":rst.getString("fecha_sol_pyme"));
				contratoCesion.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));        
        contratoCesion.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				contratoCesion.put("fecha_inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				contratoCesion.put("fecha_fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				contratoCesion.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				contratoCesion.put("fecha_aceptacion", rst.getString("fecha_aceptacion")==null?"":rst.getString("fecha_aceptacion"));
				contratoCesion.put("campo_adicional_1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				contratoCesion.put("campo_adicional_2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				contratoCesion.put("campo_adicional_3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				contratoCesion.put("campo_adicional_4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				contratoCesion.put("campo_adicional_5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				contratoCesion.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
				contratoCesion.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
				contratoCesion.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));
				contratoCesion.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				contratoCesion.put("monto_credito", rst.getString("monto_credito")==null?"":rst.getString("monto_credito"));
				contratoCesion.put("numero_referencia", rst.getString("numero_referencia")==null?"":rst.getString("numero_referencia"));
				contratoCesion.put("fecha_vencimiento", rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento"));
				contratoCesion.put("banco_deposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				contratoCesion.put("numero_cuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				contratoCesion.put("clabe", rst.getString("clabe")==null?"":rst.getString("clabe"));
				contratoCesion.put("firmado_if", rst.getString("firmado_if")==null?"":rst.getString("firmado_if"));
				contratoCesion.put("clave_estatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				contratoCesion.put("estatus_solicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				contratoCesion.put("fecha_firma_testigo1", rst.getString("fecha_firma_testigo1")==null?"":rst.getString("fecha_firma_testigo1"));
				contratoCesion.put("fecha_firma_testigo2", rst.getString("fecha_firma_testigo2")==null?"":rst.getString("fecha_firma_testigo2"));
				contratoCesion.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));//FODEA 041 - 2010 ACF
				contratoCesion.put("claveTipoContratacion", rst.getString("clave_tipo_contratacion")==null?"":rst.getString("clave_tipo_contratacion"));//FODEA 041 - 2010 ACF
				contratoCesion.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));//FODEA 041 - 2010 ACF
        contratoCesion.put("nombre_usuario", rst.getString("nombre_usuario")==null?"":rst.getString("nombre_usuario"));
				contratoCesion.put("dias_minimos_notificacion", Integer.toString(diasMinimosNotificacion)); 
        
        StringBuffer monto_moneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

        contratoCesion.put("monto_moneda", monto_moneda.toString());
				contratoCesion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
        //contratoCesion.put("desc_plazo", rst.getString("desc_plazo")==null?"":rst.getString("desc_plazo"));
        //contratoCesion.put("plazo", rst.getString("plazo")==null?"":rst.getString("plazo"));
				consultaContratosCesion.put("contratoCesion"+indice, contratoCesion);
				indice++;
			}
			
			consultaContratosCesion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("consultaContratosCesionIf(S)");
		}
		return consultaContratosCesion;
	}
  
  /**
	 * M�todo que obtiene las monedas y montos parametrizados por solicitud.
	 * @param clave_solicitud Clave de la solicitud de Cesi�n de Derechos.
	 * @return montoMoneda StringBuffer con el valor obtenido de la base de datos.
	 * @throws AppException Cuando ocurre alg�n error al realizar la consulta.
	 */
	public StringBuffer getMontoMoneda(String clave_solicitud) throws AppException{
		log.info("getMontoMoneda(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		StringBuffer montoMoneda = new StringBuffer();
    
		try{
			con.conexionDB();

			strSQL.append(" SELECT cms.fn_monto_moneda as monto,");
      strSQL.append("        cms.fn_monto_moneda_min as monto_min, ");
      strSQL.append("        cms.fn_monto_moneda_max as monto_max,");
      strSQL.append("        ccm.cd_nombre as moneda");
      strSQL.append("   FROM cder_monto_x_solic cms, comcat_moneda ccm");
      strSQL.append("  WHERE ccm.ic_moneda = cms.ic_moneda");        
      strSQL.append(" AND cms.ic_solicitud = ?");
      varBind.add(new Integer(clave_solicitud));
						
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();			
      
			while(rst.next()){
				  montoMoneda.append(rst.getString("monto")==null?"":Comunes.formatoDecimal(rst.getString("monto"), 2) + " ");
				  montoMoneda.append(rst.getString("monto_min")==null?"":Comunes.formatoDecimal(rst.getString("monto_min"), 2) + "-");
				  montoMoneda.append(rst.getString("monto_max")==null?"":Comunes.formatoDecimal(rst.getString("monto_max"), 2) + " ");
				  montoMoneda.append(rst.getString("moneda")==null?"":rst.getString("moneda"));
				  montoMoneda.append(" <br/>");
			}
			montoMoneda.delete(montoMoneda.length()-5,montoMoneda.length());
			
      System.out.println("montoMoneda: " + montoMoneda.toString());
      
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar en la base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getMontoMoneda(S)");
		}
		return montoMoneda;
	}
    
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de acuse y preacuse de aceptaci�n
	 * y rechazo de Contratos de Cesi�n para el IF.
	 * @param clave_solicitud Clave del contrato del que se realiza la consulta.
	 * @return consultaContratoCesion Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultaContratoCesionIf(String clave_solicitud) throws AppException{
		log.info("consultaContratoCesionIf(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaContratoCesion = new HashMap();
		String numeroAcuse = "";
		int indice = 0;

		try {
			con.conexionDB();

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			strSQL.append(" pym.cg_rfc AS rfc_pyme,");
			strSQL.append(" crpe.cg_pyme_epo_interno AS numero_proveedor,");
			strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud_consentimiento,");
			strSQL.append(" TO_CHAR(SYSDATE, 'dd/mm/yyyy') AS fecha_formal_contrato,");
			strSQL.append(" TO_CHAR(sol.df_notif_vent, 'dd/mm/yyyy') AS fecha_notif_vent,");
			strSQL.append(" TO_CHAR(sol.df_redireccion, 'dd/mm/yyyy') AS fecha_redireccion,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
			strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
			strSQL.append(" sol.cg_cuenta_clabe AS cuenta_clabe,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			//strSQL.append(" sol.fn_monto_contrato AS monto_contrato,");
			//strSQL.append(" mon.cd_nombre AS moneda,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" TO_CHAR(sol.df_fecha_limite_firma, 'dd/mm/yyyy') AS fecha_limite_firma,");
			strSQL.append(" TO_CHAR(sol.df_fecha_aceptacion, 'dd/mm/yyyy') AS fecha_aceptacion_epo,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
			//strSQL.append(" sol.cc_acuse1 AS acuse1,");//ESTE ES EL ACUSE DE LA PYME CUANDO ENVIA LA SOLIC
			strSQL.append(" sol.cc_acuse2 AS acuse2,");//ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			strSQL.append(" sol.cc_acuse3 AS acuse3,");//ESTE ES EL ACUSE DE LA PYME CUANDO FIRMA EL ECONTRATO
			strSQL.append(" sol.cc_acuse4 AS acuse4,");//ESTE ES EL ACUSE DEL IF CUANDO FIRMA EL ECONTRATO
			strSQL.append(" sol.cc_acuse5 AS acuse5,");//ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA LA NOTIFICACION
			strSQL.append(" sol.cc_acuse6 AS acuse6,");//ESTE ES EL ACUSE DEl REPRESENTANTE IF 2
			strSQL.append(" sol.cc_acuse_firma_rep1 AS acuse_firma_rep1,");
			strSQL.append(" sol.cc_acuse_firma_rep2 AS acuse_firma_rep2,");
			strSQL.append(" sol.cc_acuse_testigo1 AS acuse_firma_test1,");
			strSQL.append(" sol.cc_acuse_testigo2 AS acuse_firma_test2,");
			strSQL.append(" sol.cc_acuse_redirec AS acuse_redirec,");
			strSQL.append(" sol.cg_firma1 AS firma1,");
			strSQL.append(" sol.cg_firma2 AS firma2,");//FIRMA DEL IF
			strSQL.append(" sol.cg_firma3 AS firma3,");//FIRMA DEL PRIMER TESTIGO
			strSQL.append(" sol.cg_firma4 AS firma4,");//FIRMA DEL SEGUNDO TESTIGO
			strSQL.append(" sol.cg_firma5 AS firma5,");//FIRMA DEL Representante IF 2
			strSQL.append(" sol.cg_firma_rep1 AS firma_rep1,");//FIRMA DEL PRIMER REPRESENTANTE LEGAL PYME
			strSQL.append(" sol.cg_firma_rep2 AS firma_rep2");//FIRMA DEL SEGUNDO REPRESENTANTE LEGAL PYME
      //strSQL.append(" ctp.cg_descripcion AS desc_plazo,");
      //strSQL.append(" sol.ig_plazo AS plazo");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comrel_pyme_epo crpe");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
      //strSQL.append(", comcat_moneda mon");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
      strSQL.append(", cdercat_tipo_plazo ctp");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_pyme = crpe.ic_pyme(+)");
			strSQL.append(" AND sol.ic_epo = crpe.ic_epo(+)");
			//strSQL.append(" AND sol.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strSQL.append(" AND sol.ic_solicitud = ?");
      strSQL.append(" AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)");
			
			varBind.add(new Long(clave_solicitud));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				String claveEpo = rst.getString("clave_epo");
				int diasMinimosNotificacion = this.getDiasMinimosNotificacion(claveEpo);
        
				consultaContratoCesion.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				consultaContratoCesion.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				consultaContratoCesion.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				consultaContratoCesion.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				consultaContratoCesion.put("nombre_pyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				consultaContratoCesion.put("rfcPyme", rst.getString("rfc_pyme")==null?"":rst.getString("rfc_pyme"));
				consultaContratoCesion.put("numeroProveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				consultaContratoCesion.put("fechaSolicitudConsentimiento", rst.getString("fecha_solicitud_consentimiento")==null?"":rst.getString("fecha_solicitud_consentimiento"));
				consultaContratoCesion.put("fechaNotifVentanilla", rst.getString("fecha_notif_vent")==null?"":rst.getString("fecha_notif_vent"));
				consultaContratoCesion.put("fechaRedirecccion", rst.getString("fecha_redireccion")==null?"":rst.getString("fecha_redireccion"));
				consultaContratoCesion.put("nombre_epo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				consultaContratoCesion.put("nombre_if", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
				consultaContratoCesion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				consultaContratoCesion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				consultaContratoCesion.put("numeroCuentaClabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				consultaContratoCesion.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//consultaContratoCesion.put("monto_contrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				//consultaContratoCesion.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				consultaContratoCesion.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				consultaContratoCesion.put("fecha_inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				consultaContratoCesion.put("fecha_fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				consultaContratoCesion.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				consultaContratoCesion.put("campo_adicional_1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				consultaContratoCesion.put("campo_adicional_2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				consultaContratoCesion.put("campo_adicional_3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				consultaContratoCesion.put("campo_adicional_4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				consultaContratoCesion.put("campo_adicional_5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				consultaContratoCesion.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				consultaContratoCesion.put("fecha_limite_firma", rst.getString("fecha_limite_firma")==null?"":rst.getString("fecha_limite_firma"));
				consultaContratoCesion.put("fechaAceptacionEpo", rst.getString("fecha_aceptacion_epo")==null?"":rst.getString("fecha_aceptacion_epo"));
				consultaContratoCesion.put("clave_estatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				consultaContratoCesion.put("estatus_solicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
        //consultaContratoCesion.put("desc_plazo", rst.getString("desc_plazo")==null?"":rst.getString("desc_plazo"));
        //consultaContratoCesion.put("plazo", rst.getString("plazo")==null?"":rst.getString("plazo"));
				consultaContratoCesion.put("plazo_contrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				consultaContratoCesion.put("dias_minimos_notificacion", Integer.toString(diasMinimosNotificacion));
        
        StringBuffer monto_moneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

        consultaContratoCesion.put("monto_moneda", monto_moneda.toString());
              
				//ACUSE DE ACEPTACION DE LA SOLICITUD DE CONSENTIMIENTO POR PARTE DE LA EPO
				numeroAcuse = rst.getString("acuse2")==null?"":rst.getString("acuse2");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseEnvioSolicEpo", rst.getString("acuse2")==null?"":rst.getString("acuse2"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrEpoAutCesion", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrEpoAutCesion", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaEpoAutCesion", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaEpoAutCesion", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE CONSENTIMIENTO POR EL PRIMER REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("acuse_firma_rep1")==null?"":rst.getString("acuse_firma_rep1");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaRep1", rst.getString("acuse_firma_rep1")==null?"":rst.getString("acuse_firma_rep1"));
					consultaContratoCesion.put("firmaRep1", rst.getString("firma_rep1")==null?"":rst.getString("firma_rep1"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrAutRep1", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrAutRep1", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaAutRep1", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaAutRep1", datosAcuse.get("horaAcuse"));
						consultaContratoCesion.put("fechaFormalContrato", datosAcuse.get("fechaAcuse"));
					}
				} else {
					numeroAcuse = rst.getString("acuse3")==null?"":rst.getString("acuse3");
						if (!numeroAcuse.equals("")) {
						consultaContratoCesion.put("acuseFirmaRep1", rst.getString("acuse3")==null?"":rst.getString("acuse3"));
						consultaContratoCesion.put("firmaRep1", rst.getString("firma1")==null?"":rst.getString("firma1"));
						
						HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);
	
						if (!datosAcuse.isEmpty()) {
							consultaContratoCesion.put("claveUsrAutRep1", datosAcuse.get("claveUsuario"));
							consultaContratoCesion.put("nombreUsrAutRep1", datosAcuse.get("nombreUsuario"));
							consultaContratoCesion.put("fechaAutRep1", datosAcuse.get("fechaAcuse"));
							consultaContratoCesion.put("horaAutRep1", datosAcuse.get("horaAcuse"));
							consultaContratoCesion.put("fechaFormalContrato", datosAcuse.get("fechaAcuse"));
						}
					}
				}
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE CONSENTIMIENTO POR EL SEGUNDO REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("acuse_firma_rep2")==null?"":rst.getString("acuse_firma_rep2");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaRep2", rst.getString("acuse_firma_rep2")==null?"":rst.getString("acuse_firma_rep2"));
					consultaContratoCesion.put("firmaRep2", rst.getString("firma_rep2")==null?"":rst.getString("firma_rep2"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrAutRep2", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrAutRep2", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaAutRep2", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaAutRep2", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL IF
				numeroAcuse = rst.getString("acuse4")==null?"":rst.getString("acuse4");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaIf", rst.getString("acuse4")==null?"":rst.getString("acuse4"));
					consultaContratoCesion.put("firmaIf", rst.getString("firma2")==null?"":rst.getString("firma2"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrIf", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrIf", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaIf", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaIf", datosAcuse.get("horaAcuse"));
					}
				}
				
				
				//Acuse Aceptacion IF REP 2
				numeroAcuse = rst.getString("acuse6")==null?"":rst.getString("acuse6");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaIfRep2", rst.getString("acuse6")==null?"":rst.getString("acuse6"));
					consultaContratoCesion.put("firmaIfRep2", rst.getString("firma5")==null?"":rst.getString("firma5"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrIfRep2", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrIfRep2", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaIfRep2", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaIfRep2", datosAcuse.get("horaAcuse"));
					}
				}
				
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL PRIMER TESTIGO DEL IF
				
				numeroAcuse = rst.getString("acuse_firma_test1")==null?"":rst.getString("acuse_firma_test1");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaTest1", rst.getString("acuse_firma_test1")==null?"":rst.getString("acuse_firma_test1"));
					consultaContratoCesion.put("firmaTest1", rst.getString("firma3")==null?"":rst.getString("firma3"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrTest1", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrTest1", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaTest1", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaTest1", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL SEGUNDO TESTIGO DEL IF
				numeroAcuse = rst.getString("acuse_firma_test2")==null?"":rst.getString("acuse_firma_test2");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaTest2", rst.getString("acuse_firma_test2")==null?"":rst.getString("acuse_firma_test2"));
					consultaContratoCesion.put("firmaTest2", rst.getString("firma4")==null?"":rst.getString("firma4"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrTest2", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrTest2", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaTest2", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaTest2", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DE LA NOTIFICACION DEL CONTRATO DE CESI�N POR PARTE DE LA EPO
				numeroAcuse = rst.getString("acuse5")==null?"":rst.getString("acuse5");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseNotificacionEpo", rst.getString("acuse5")==null?"":rst.getString("acuse5"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrNotifEpo", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrNotifEpo", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaNotifEpo", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaNotifEpo", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE REDIRECCIONAMIENTO DE LA CTA DE LA EPO POR LA VENTANILLA
				numeroAcuse = rst.getString("acuse_redirec")==null?"":rst.getString("acuse_redirec");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseRedireccion", rst.getString("acuse_redirec")==null?"":rst.getString("acuse_redirec"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrRedirec", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrRedirec", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaRedirec", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaRedirec", datosAcuse.get("horaAcuse"));
					}
				}
				indice++;
			}
			
			consultaContratoCesion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("consultaContratoCesionIf(S)");
		}
		return consultaContratoCesion;
	}
	
	/**
	 * M�todo que obtiene de la base de datos el par�metro d�as minimos para la notificaci�n de una
	 * solicitude de cesi�n de derechos. Este parametro se emplea para calcular la fecha que se tiene
	 * como l�mite para la notificaci�n.
	 * @param clave_epo Clave interna de la EPO que parametriza el valor de los d�as minimos para la notificaci�n.
	 * @return dias_notificacion Entero con el valor obtenido de la base de datos.
	 * @throws AppException Cuando ocurre alg�n error al realizar la consulta.
	 */
	public int getDiasMinimosNotificacion(String clave_epo) throws AppException{
		log.info("getDiasMinimosNotificacion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		int dias_notificacion = 0;
		try{
			con.conexionDB();

			strSQL.append(" SELECT NVL(ig_dias_pnotificacion, 0) AS dias_notificacion");
			strSQL.append(" FROM comrel_producto_epo");
			strSQL.append(" WHERE ic_epo = ?");
			strSQL.append(" AND ic_producto_nafin = ?");
			
			varBind.add(new Long(clave_epo));
			varBind.add(new Integer(9));
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while(rst.next()){
				dias_notificacion = rst.getInt("dias_notificacion");
			}
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar en la base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getDiasMinimosNotificacion(S)");
		}
		return dias_notificacion;
	}
	
	/**
	 * M�todo que guarda la firma de la solicitud de cesi�n de derechos por parte del IF.
	 * @param parametrosFirma HashMap con los parametros con los que se reliza la autenticaci�n del usuario.
	 * @return Acuse Cadena con el numero de acuse generado al autenticar al usuario.
	 * @throws Appexception Cuando ocurre un error al obtener el acuse y/o guardar la firma de la pyme.
	 */
	public String acuseFirmaContratoCesionIF(HashMap parametrosFirma) throws AppException{
		log.info("acuseFirmaContratoCesionIF(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		String acuse = "";
			
		String _serial = parametrosFirma.get("_serial")==null?"":(String)parametrosFirma.get("_serial");
		String texto_plano = parametrosFirma.get("texto_plano")==null?"":(String)parametrosFirma.get("texto_plano");
		String pkcs7 = parametrosFirma.get("pkcs7")==null?"":(String)parametrosFirma.get("pkcs7");
		String folioCert = parametrosFirma.get("folioCert")==null?"":(String)parametrosFirma.get("folioCert");
		boolean validCert = Boolean.getBoolean((String)parametrosFirma.get("validCert"));
		String clave_solicitud = parametrosFirma.get("clave_solicitud")==null?"":(String)parametrosFirma.get("clave_solicitud");
		String login_usuario = parametrosFirma.get("login_usuario")==null?"":(String)parametrosFirma.get("login_usuario");
		String nombre_usuario = parametrosFirma.get("nombre_usuario")==null?"":(String)parametrosFirma.get("nombre_usuario");
		String sello_digital = parametrosFirma.get("sello_digital")==null?"":(String)parametrosFirma.get("sello_digital");
		String fecha_carga = parametrosFirma.get("fecha_carga")==null?"":(String)parametrosFirma.get("fecha_carga");
		String hora_carga = parametrosFirma.get("hora_carga")==null?"":(String)parametrosFirma.get("hora_carga");
		String tipoOperacion = parametrosFirma.get("tipoOperacion")==null?"":(String)parametrosFirma.get("tipoOperacion");
		String monto_credito = parametrosFirma.get("monto_credito")==null?"":(String)parametrosFirma.get("monto_credito");
		String referencia = parametrosFirma.get("referencia")==null?"":(String)parametrosFirma.get("referencia");
		String fecha_vencimiento = parametrosFirma.get("fecha_vencimiento")==null?"":(String)parametrosFirma.get("fecha_vencimiento");
		String banco_deposito = parametrosFirma.get("banco_deposito")==null?"":(String)parametrosFirma.get("banco_deposito");
		String numero_cuenta = parametrosFirma.get("numero_cuenta")==null?"":(String)parametrosFirma.get("numero_cuenta");
		String clabe = parametrosFirma.get("clabe")==null?"":(String)parametrosFirma.get("clabe");
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		int seqNEXTVAL= 0;
		String firmoIF2="";
		String rep2="";
		
		try{
			con.conexionDB();
			
			String qry = "SELECT SEQ_CDER_ACUSE.nextval, cg_firma5,cs_bandera_rep2 FROM cder_solicitud sol,comcat_cesionario ces WHERE"+
			" sol.ic_if=ces.ic_if "+
			" AND sol.ic_solicitud = "+ clave_solicitud;
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
							if(rs1.next())
							{
								seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
								firmoIF2 = rs1.getString(2)==null?"":rs1.getString(2);
								rep2= rs1.getString(3)==null?"":rs1.getString(3);
							}
						ps1.close();
						rs1.close();
						
			
			char getReceipt = 'Y';
			if( !_serial.equals("") && !texto_plano.equals("") && !pkcs7.equals("") ){
				Seguridad s = new Seguridad();
				if( s.autenticar(folioCert, _serial, pkcs7, texto_plano, getReceipt) ){
					acuse = s.getAcuse();
					
					strSQL.append(" UPDATE cder_solicitud");
					strSQL.append(" SET cc_acuse4 = ?,");		//Acuse de firma de IF
					strSQL.append(" cg_firma2 = ?,");			//Firma del IF
					strSQL.append(" df_firma_if = SYSDATE");			// Fecha Firma del IF
					
					if(!tipoOperacion.equals("ACEPTAR")){
						strSQL.append(" ,ic_estatus_solic = ?");	//Nuevo estatus
					}
					if(!monto_credito.equals("")){
						strSQL.append(" ,fn_monto_credito = ?");
					}
					if(!referencia.equals("")){
						strSQL.append(" ,cg_referencia = ?");
					}
					if(!fecha_vencimiento.equals("")){
						strSQL.append(" ,df_vencimiento_credito = TO_DATE(?, 'dd/mm/yyyy')");
					}
					if(!banco_deposito.equals("")){
						strSQL.append(" ,cg_banco_deposito = ?");
					}
					if(!numero_cuenta.equals("")){
						strSQL.append(" ,cg_cuenta = ?");
					}
					if(!clabe.equals("")){
						strSQL.append(" ,cg_cuenta_clabe = ?");
					}
					if(!firmoIF2.equals("")||!rep2.equals("S")){
						strSQL.append(", cs_firmado_if = ?");
					}
					strSQL.append(" WHERE ic_solicitud = ?");

					
					varBind.add(new Integer(seqNEXTVAL));
					varBind.add(sello_digital);
					if(!tipoOperacion.equals("ACEPTAR")){
						varBind.add(new Integer(10));
					}
					if(!monto_credito.equals("")){
						varBind.add(new Double(monto_credito));
					}
					if(!referencia.equals("")){
						varBind.add(referencia);
					}
					if(!fecha_vencimiento.equals("")){
						varBind.add(fecha_vencimiento);
					}
					if(!banco_deposito.equals("")){
						varBind.add(banco_deposito);
					}
					if(!numero_cuenta.equals("")){
						varBind.add(numero_cuenta);
					}
					if(!clabe.equals("")){
						varBind.add(clabe);
					}
					if(!firmoIF2.equals("")||!rep2.equals("S")){
						varBind.add("S");
					}
					varBind.add(clave_solicitud);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(seqNEXTVAL));
					varBind.add(login_usuario);
					varBind.add(nombre_usuario);
					varBind.add(acuse);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}
			}
		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error al firmar el contrato de ceci�n de derechos: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseFirmaContratoCesionIF(S)");
		}
		return acuse;
	}
	
	/**
	 * M�todo que guarda la firma de la solicitud de cesi�n de derechos por parte del IF.
	 * @param parametrosFirma HashMap con los parametros con los que se reliza la autenticaci�n del usuario.
	 * @return Acuse Cadena con el numero de acuse generado al autenticar al usuario.
	 * @throws Appexception Cuando ocurre un error al obtener el acuse y/o guardar la firma de la pyme.
	 */
	public String acuseFirmaContratoCesionIFRep2(HashMap parametrosFirma) throws AppException{
		log.info("acuseFirmaContratoCesionIFRep2(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		String acuse = "";
			
		String _serial = parametrosFirma.get("_serial")==null?"":(String)parametrosFirma.get("_serial");
		String texto_plano = parametrosFirma.get("texto_plano")==null?"":(String)parametrosFirma.get("texto_plano");
		String pkcs7 = parametrosFirma.get("pkcs7")==null?"":(String)parametrosFirma.get("pkcs7");
		String folioCert = parametrosFirma.get("folioCert")==null?"":(String)parametrosFirma.get("folioCert");
		boolean validCert = Boolean.getBoolean((String)parametrosFirma.get("validCert"));
		String clave_solicitud = parametrosFirma.get("clave_solicitud")==null?"":(String)parametrosFirma.get("clave_solicitud");
		String login_usuario = parametrosFirma.get("login_usuario")==null?"":(String)parametrosFirma.get("login_usuario");
		String nombre_usuario = parametrosFirma.get("nombre_usuario")==null?"":(String)parametrosFirma.get("nombre_usuario");
		String sello_digital = parametrosFirma.get("sello_digital")==null?"":(String)parametrosFirma.get("sello_digital");
		String fecha_carga = parametrosFirma.get("fecha_carga")==null?"":(String)parametrosFirma.get("fecha_carga");
		String hora_carga = parametrosFirma.get("hora_carga")==null?"":(String)parametrosFirma.get("hora_carga");
		String tipoOperacion = parametrosFirma.get("tipoOperacion")==null?"":(String)parametrosFirma.get("tipoOperacion");
		String monto_credito = parametrosFirma.get("monto_credito")==null?"":(String)parametrosFirma.get("monto_credito");
		String referencia = parametrosFirma.get("referencia")==null?"":(String)parametrosFirma.get("referencia");
		String fecha_vencimiento = parametrosFirma.get("fecha_vencimiento")==null?"":(String)parametrosFirma.get("fecha_vencimiento");
		String banco_deposito = parametrosFirma.get("banco_deposito")==null?"":(String)parametrosFirma.get("banco_deposito");
		String numero_cuenta = parametrosFirma.get("numero_cuenta")==null?"":(String)parametrosFirma.get("numero_cuenta");
		String clabe = parametrosFirma.get("clabe")==null?"":(String)parametrosFirma.get("clabe");
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		int seqNEXTVAL= 0;
		String firmoIF1="";
		
		try{
			con.conexionDB();
			
			String qry = "SELECT SEQ_CDER_ACUSE.nextval,cg_firma2  FROM cder_solicitud where ic_solicitud = "+ clave_solicitud;
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
							if(rs1.next())
							{
								seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
								firmoIF1= rs1.getString(2)==null?"":rs1.getString(2);
							}
						ps1.close();
						rs1.close();
						
			
			char getReceipt = 'Y';
			if( !_serial.equals("") && !texto_plano.equals("") && !pkcs7.equals("") ){
				Seguridad s = new Seguridad();
				if( s.autenticar(folioCert, _serial, pkcs7, texto_plano, getReceipt) ){
					acuse = s.getAcuse();
					
					strSQL.append(" UPDATE cder_solicitud");
					strSQL.append(" SET CC_ACUSE6 = ?,");		//Acuse de firma de IF
					strSQL.append(" cg_firma5 = ?,");			//Firma del IF
					strSQL.append(" df_firma_if2 = SYSDATE");			// Fecha Firma del IF
					
					if(!firmoIF1.equals("")){
						strSQL.append(" ,cs_firmado_if = ?");	//Nuevo estatus
					}
					
					strSQL.append(" WHERE ic_solicitud = ?");

					
					varBind.add(new Integer(seqNEXTVAL));
					varBind.add(sello_digital);
					if(!firmoIF1.equals("")){
						varBind.add("S");
					}
					varBind.add(clave_solicitud);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(seqNEXTVAL));
					varBind.add(login_usuario);
					varBind.add(nombre_usuario);
					varBind.add(acuse);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}
			}
		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error al firmar el contrato de ceci�n de derechos: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseFirmaContratoCesionIFRep2(S)");
		}
		return acuse;
	}

	/**
	 * M�todo que genera el combo de pymes que tienen solicitudes pendientes de autorizaci�n
	 * por la EPO.
	 * @param clave_epo Clave interna de la EPO que tiene solicitudes pendientes por autorizar con alguna pyme.
	 * @return combo_pyme Lista de objetos ElementoCatalogo con las pymes seleccionadas.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	 public List getComboPymesNotificacion(String clave_epo) throws AppException{
		log.info("getComboPymesNotificacion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List combo_pyme = new ArrayList();
		try{
			con.conexionDB();
			
			strSQL.append(" SELECT DISTINCT pyme.ic_pyme AS clave,");
			strSQL.append(" pyme.cg_razon_social AS descripcion");
			strSQL.append(" FROM cder_solicitud cds,");
			strSQL.append(" comcat_pyme pyme");
			strSQL.append(" WHERE cds.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND cds.ic_epo = ?");
			strSQL.append(" AND cds.ic_estatus_solic IN (?, ?, ?, ?, ?)");
			strSQL.append(" ORDER BY pyme.cg_razon_social");

			varBind.add(new Integer(clave_epo));
			varBind.add(new Integer(9));
			varBind.add(new Integer(11));
			varBind.add(new Integer(12));
			varBind.add(new Integer(15));
			varBind.add(new Integer(16));
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while(rst.next()){
				ElementoCatalogo elemento_catalogo = new ElementoCatalogo();
				elemento_catalogo.setClave(rst.getString("clave")==null?"":rst.getString("clave"));
				elemento_catalogo.setDescripcion(rst.getString("descripcion")==null?"":rst.getString("descripcion"));
				combo_pyme.add(elemento_catalogo);
			}
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getComboPymesNotificacion(S)");
		}
		return combo_pyme;
	}
	
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Notificaciones para la EPO.
	 * @param parametrosConsulta HashMap con los parametros con los que se realiza la consulta de contratos de cesi�n.
	 * @return consultaNotificaciones Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultaNotificacionesEpo(HashMap parametrosConsulta) throws AppException{
		log.info("consultaNotificacionesEpo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaNotificaciones = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();
			
			String claveEpo = parametrosConsulta.get("claveEpo")==null?"":(String)parametrosConsulta.get("claveEpo");
			String claveIf = parametrosConsulta.get("claveIf")==null?"":(String)parametrosConsulta.get("claveIf");
			String clavePyme = parametrosConsulta.get("clavePyme")==null?"":(String)parametrosConsulta.get("clavePyme");
			String claveEstatusSol = parametrosConsulta.get("claveEstatusSol")==null?"":(String)parametrosConsulta.get("claveEstatusSol");
			String numeroContrato = parametrosConsulta.get("numeroContrato")==null?"":(String)parametrosConsulta.get("numeroContrato");
			String fechaVigenciaIni = parametrosConsulta.get("fechaVigenciaIni")==null?"":(String)parametrosConsulta.get("fechaVigenciaIni");
			String fechaVigenciaFin = parametrosConsulta.get("fechaVigenciaFin")==null?"":(String)parametrosConsulta.get("fechaVigenciaFin");
			String fechaSolicitudIni = parametrosConsulta.get("fechaSolicitudIni")==null?"":(String)parametrosConsulta.get("fechaSolicitudIni");
			String fechaSolicitudFin = parametrosConsulta.get("fechaSolicitudFin")==null?"":(String)parametrosConsulta.get("fechaSolicitudFin");
			String plazoContrato = parametrosConsulta.get("plazoContrato")==null?"":(String)parametrosConsulta.get("plazoContrato");
			String claveTipoPlazo = parametrosConsulta.get("claveTipoPlazo")==null?"":(String)parametrosConsulta.get("claveTipoPlazo");
			List camposAdicionales = parametrosConsulta.get("camposAdicionales")==null?new ArrayList():(List)parametrosConsulta.get("camposAdicionales");
			List tipoDato = parametrosConsulta.get("tipoDato")==null?new ArrayList():(List)parametrosConsulta.get("tipoDato");
			String loginUsuario = parametrosConsulta.get("loginUsuario")==null?"":(String)parametrosConsulta.get("loginUsuario");

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			strSQL.append(" pym.cg_rfc AS rfc,");
			strSQL.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
			strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
			strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" sol.fn_monto_credito AS monto_credito,");
			strSQL.append(" sol.cg_referencia AS numero_referencia,");
			strSQL.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,");
			strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
			strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
			strSQL.append(" sol.cg_cuenta_clabe AS clabe,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
			strSQL.append(" sol.cg_causas_rechazo_notif AS causas_rechazo_notif,");
			strSQL.append(" sol.CG_OBSERV_RECHAZO AS causasRechazo, ");
			strSQL.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comrel_pyme_epo cpe");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
			strSQL.append(", cdercat_tipo_plazo stp");
			strSQL.append(", cdercat_ventanilla_pago cvp");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_epo = cpe.ic_epo(+)");
			strSQL.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			
			if (loginUsuario != null && !"".equals(loginUsuario)) {
				strSQL.append(" AND SUBSTR(cep.cg_responsable, 1, 8) = ?");
				varBind.add(loginUsuario);
			}

			if(!claveEpo.equals("")){
				strSQL.append(" AND sol.ic_epo = ?");
				varBind.add(new Integer(claveEpo));
			}
			if(!claveIf.equals("")){
				strSQL.append(" AND sol.ic_if = ?");
				varBind.add(new Integer(claveIf));
			}
			if(!clavePyme.equals("")){
				strSQL.append(" AND sol.ic_pyme = ?");
				varBind.add(clavePyme);
			}
			if(!claveEstatusSol.equals("")){
				strSQL.append(" AND sol.ic_estatus_solic = ?");
				varBind.add(new Integer(claveEstatusSol));
			}else{
				strSQL.append(" AND sol.ic_estatus_solic IN (?,?,?,?,?,?)");
				varBind.add(new Integer(9));
				varBind.add(new Integer(11));
				varBind.add(new Integer(12));
				varBind.add(new Integer(15));
				varBind.add(new Integer(16));
				varBind.add(new Integer(17));
			}
			if(!numeroContrato.equals("")){
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(numeroContrato);
			}
			if(!fechaVigenciaIni.equals("") && !fechaVigenciaFin.equals("")){
				strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
				varBind.add(fechaVigenciaIni);
				varBind.add(fechaVigenciaFin);
			}
			if(!fechaSolicitudIni.equals("") && !fechaSolicitudFin.equals("")){
				strSQL.append(" AND sol.df_fecha_solicitud_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_solicitud_ini <= TO_DATE(?, 'dd/mm/yyyy')");
				varBind.add(fechaSolicitudIni);
				varBind.add(fechaSolicitudFin);
			}
			if(!camposAdicionales.isEmpty()){
				for(int i = 0; i < camposAdicionales.size(); i++){
					String campoAdicional = (String)camposAdicionales.get(i);
					String tipo = (String)tipoDato.get(i);
					if(!campoAdicional.equals("")){
						strSQL.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
						if(tipo.equals("N")){varBind.add(campoAdicional);}else{varBind.add(campoAdicional);}
					}
				}
			}
			
			if (plazoContrato != null && !"".equals(plazoContrato)) {
				strSQL.append(" AND sol.ig_plazo = ?");
				varBind.add(new Integer(plazoContrato));
			}		
	
			if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
				strSQL.append(" AND sol.ic_tipo_plazo = ?");
				varBind.add(new Integer(claveTipoPlazo));
			}
			
			strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");
			
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				HashMap contratoCesion = new HashMap();
				StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

				contratoCesion.put("claveSolicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				contratoCesion.put("clavePyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				contratoCesion.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				contratoCesion.put("claveIf", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				contratoCesion.put("nombreIf", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
				contratoCesion.put("nombrePyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				contratoCesion.put("rfc", rst.getString("rfc")==null?"":rst.getString("rfc"));
				contratoCesion.put("numeroProveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				contratoCesion.put("fechaSolicitudPyme", rst.getString("fecha_sol_pyme")==null?"":rst.getString("fecha_sol_pyme"));
				contratoCesion.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				contratoCesion.put("montosPorMoneda", montosPorMoneda.toString());
				contratoCesion.put("tipoContratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				contratoCesion.put("fechaInicioContrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				contratoCesion.put("fechaFinContrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				contratoCesion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				contratoCesion.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				contratoCesion.put("campoAdicional1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				contratoCesion.put("campoAdicional2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				contratoCesion.put("campoAdicional3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				contratoCesion.put("campoAdicional4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				contratoCesion.put("campoAdicional5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				contratoCesion.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
				contratoCesion.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
				contratoCesion.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));
				contratoCesion.put("objetoContrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				contratoCesion.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));
				contratoCesion.put("montoCredito", rst.getString("monto_credito")==null?"":rst.getString("monto_credito"));
				contratoCesion.put("numeroReferencia", rst.getString("numero_referencia")==null?"":rst.getString("numero_referencia"));
				contratoCesion.put("fechaVencimiento", rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento"));
				contratoCesion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				contratoCesion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				contratoCesion.put("clabe", rst.getString("clabe")==null?"":rst.getString("clabe"));
				contratoCesion.put("claveEstatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				contratoCesion.put("estatusSolicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				contratoCesion.put("causasRechazo", rst.getString("causasRechazo")==null?"":rst.getString("causasRechazo"));
				contratoCesion.put("causasRechazoNotificacion", rst.getString("causas_rechazo_notif")==null?"":rst.getString("causas_rechazo_notif"));
				contratoCesion.put("firma_contrato", rst.getString("firma_contrato")==null?"":rst.getString("firma_contrato"));
				consultaNotificaciones.put("contratoCesion"+indice, contratoCesion);
				indice++;
			}
			
			consultaNotificaciones.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("consultaNotificacionesEpo(S)");
		}
		return consultaNotificaciones;
	}
	
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de preacuse Notificaciones para la EPO.
	 * @param claveSolicitud Clave interna de la solicitud que se consulta.
	 * @return consultaNotificacion Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultaNotificacionEpo(String claveSolicitud) throws AppException{
		log.info("consultaNotificacionEpo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaNotificacion = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			
			strSQL.append(" pym.cg_rfc AS rfc,");
			strSQL.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
			strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
			strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" sol.fn_monto_credito AS monto_credito,");
			strSQL.append(" sol.cg_referencia AS numero_referencia,");
			strSQL.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,");
			strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
			strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
			strSQL.append(" sol.cg_cuenta_clabe AS clabe,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud,");
			strSQL.append(" sol.cc_acuse_redirec AS acuse_redirec,");
			strSQL.append(" sol.cc_acuse5 AS acuse5,");
			strSQL.append(" sol.cg_causas_rechazo_notif AS causas_rechazo_notif,");
			strSQL.append(" sol.cg_observ_rechazo AS observaciones_rechazo, ");
			strSQL.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato"); 
			strSQL.append(" , sol.CG_EMPRESAS_REPRESENTADAS as empresas ");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comrel_pyme_epo cpe");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
			strSQL.append(", cdercat_tipo_plazo stp");
			strSQL.append(", cdercat_ventanilla_pago cvp");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_epo = cpe.ic_epo(+)");
			strSQL.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			
			if(!claveSolicitud.equals("")){
				strSQL.append(" AND sol.ic_solicitud = ?");
				varBind.add(claveSolicitud);
			}

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));
				consultaNotificacion.put("claveSolicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				consultaNotificacion.put("clavePyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				consultaNotificacion.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				consultaNotificacion.put("claveIf", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				consultaNotificacion.put("nombreIf", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
				consultaNotificacion.put("nombrePyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				
				consultaNotificacion.put("rfc", rst.getString("rfc")==null?"":rst.getString("rfc"));
				consultaNotificacion.put("numeroProveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				consultaNotificacion.put("fechaSolicitudPyme", rst.getString("fecha_sol_pyme")==null?"":rst.getString("fecha_sol_pyme"));
				consultaNotificacion.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//consultaNotificacion.put("montoContrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				//consultaNotificacion.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				consultaNotificacion.put("montosPorMoneda", montosPorMoneda.toString());
				consultaNotificacion.put("tipoContratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				consultaNotificacion.put("fechaInicioContrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				consultaNotificacion.put("fechaFinContrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				consultaNotificacion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				consultaNotificacion.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				consultaNotificacion.put("campoAdicional1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				consultaNotificacion.put("campoAdicional2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				consultaNotificacion.put("campoAdicional3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				consultaNotificacion.put("campoAdicional4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				consultaNotificacion.put("campoAdicional5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				consultaNotificacion.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
				consultaNotificacion.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
				consultaNotificacion.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));
				consultaNotificacion.put("objetoContrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				consultaNotificacion.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));
				consultaNotificacion.put("montoCredito", rst.getString("monto_credito")==null?"":rst.getString("monto_credito"));
				consultaNotificacion.put("numeroReferencia", rst.getString("numero_referencia")==null?"":rst.getString("numero_referencia"));
				consultaNotificacion.put("fechaVencimiento", rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento"));
				consultaNotificacion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				consultaNotificacion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				consultaNotificacion.put("clabe", rst.getString("clabe")==null?"":rst.getString("clabe"));
				consultaNotificacion.put("claveEstatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				consultaNotificacion.put("estatusSolicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				consultaNotificacion.put("acuse5", rst.getString("acuse5")==null?"":rst.getString("acuse5"));
				consultaNotificacion.put("acuseRedirec", rst.getString("acuse_redirec")==null?"":rst.getString("acuse_redirec"));
				consultaNotificacion.put("causasRechazoNotificacion", rst.getString("causas_rechazo_notif")==null?"":rst.getString("causas_rechazo_notif"));
				consultaNotificacion.put("observacionesRechazo", rst.getString("observaciones_rechazo")==null?"":rst.getString("observaciones_rechazo"));
				consultaNotificacion.put("firma_contrato", rst.getString("firma_contrato")==null?"":rst.getString("firma_contrato"));
				consultaNotificacion.put("empresas", rst.getString("empresas")==null?"":rst.getString("empresas"));
				indice++;
			}
			
			consultaNotificacion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("consultaNotificacionEpo(S)");
		}
		return consultaNotificacion;
	}

	/**
	 * M�todo que guarda la firma de la Notificaci�n del contrato de cesi�n de derechos por parte de la EPO.
	 * @param parametrosFirma HashMap con los parametros con los que se reliza la autenticaci�n del usuario.
	 * @return Acuse Cadena con el numero de acuse generado al autenticar al usuario.
	 * @throws Appexception Cuando ocurre un error al obtener el acuse y/o guardar la firma de la pyme.
	 */
	public String acuseFirmaNotificacionEpo(HashMap parametrosFirma) throws AppException{
		log.info("acuseFirmaNotificacionEpo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		String acuse = "";
			
		String _serial = parametrosFirma.get("_serial")==null?"":(String)parametrosFirma.get("_serial");
		String textoPlano = parametrosFirma.get("textoPlano")==null?"":(String)parametrosFirma.get("textoPlano");
		String pkcs7 = parametrosFirma.get("pkcs7")==null?"":(String)parametrosFirma.get("pkcs7");
		String folioCert = parametrosFirma.get("folioCert")==null?"":(String)parametrosFirma.get("folioCert");
		boolean validCert = Boolean.getBoolean((String)parametrosFirma.get("validCert"));
		String claveSolicitud = parametrosFirma.get("claveSolicitud")==null?"":(String)parametrosFirma.get("claveSolicitud");
		String loginUsuario = parametrosFirma.get("loginUsuario")==null?"":(String)parametrosFirma.get("loginUsuario");
		String nombreUsuario = parametrosFirma.get("nombreUsuario")==null?"":(String)parametrosFirma.get("nombreUsuario");
		String tipoOperacion = parametrosFirma.get("tipoOperacion")==null?"":(String)parametrosFirma.get("tipoOperacion");
		String catVentanilla = parametrosFirma.get("catVentanilla")==null?"":(String)parametrosFirma.get("catVentanilla");
		String causasRechazoNotificacion = parametrosFirma.get("causasRechazoNotificacion")==null?"":(String)parametrosFirma.get("causasRechazoNotificacion");
	
		int seqNEXTVAL = 0;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		String correo ="";
		
		try{
			con.conexionDB();
			
			
			String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
							if(rs1.next())
							{
								seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
							}
						ps1.close();
						rs1.close();
			
			
				String qry2 = "select CG_CORREO from cder_solicitud "+ 
											" where ic_solicitud = "+claveSolicitud;
						ps2 = con.queryPrecompilado(qry2);
						rs2 = ps2.executeQuery();
							if(rs2.next())
							{
								correo = rs2.getString("CG_CORREO")==null?"":rs2.getString("CG_CORREO");
							}
						ps2.close();
						rs2.close();
						
						
			char getReceipt = 'Y';
			if(!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")){
				Seguridad s = new Seguridad();
				if( s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt) ){
					acuse = s.getAcuse();
					
					strSQL.append(" UPDATE cder_solicitud");
					strSQL.append(" SET cc_acuse5 = ?,");		//Acuse de Firma de Notificaci�n EPO
					strSQL.append(" ic_estatus_solic = ?, ");	//Nuevo estatus
					strSQL.append(" IC_VENTANILLA = ? , ");	//ventanilla Destino
					strSQL.append(" CS_BANDERA_CONTRATO  = 'N',  ");
					if (!causasRechazoNotificacion.equals("")) {
						strSQL.append(" cg_causas_rechazo_notif = ? , ");	//causas del rechazo de la notificacion a ventanilla
					}
					strSQL.append(" DF_NOTIF_VENT = SYSDATE  "); //FECHA DE NOTIFICACION VENTANILLA
					strSQL.append(" WHERE ic_solicitud = ?");

					
					varBind.add(new Integer(seqNEXTVAL));
					if(tipoOperacion.equals("ACEPTAR") && correo.equals("S")){
						varBind.add(new Integer(16)); //Notificada a Ventanilla
					}	else if(tipoOperacion.equals("ACEPTAR") && correo.equals("")){
						varBind.add(new Integer(15));  //No Aceptado Ventanilla
					}	else if(tipoOperacion.equals("Aceptar")){
						varBind.add(new Integer(11));//Notificaci�n Aceptada
					}else{
						varBind.add(new Integer(12));//Notificaci�n Rechazada
					}
					varBind.add(catVentanilla); 
					if (!causasRechazoNotificacion.equals("")) {
						varBind.add(causasRechazoNotificacion);//causas del rechazo de la notificacion a ventanilla
					}
					varBind.add(claveSolicitud);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(seqNEXTVAL));
					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(acuse);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}
			}
		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error al firmar el contrato de ceci�n de derechos: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseFirmaNotificacionEpo(S)");
		}
		return acuse;
	}

	/**
	 * M�todo que genera el combo de pymes que tienen contratos de cesi�n pendientes de autorizaci�n
	 * por el IF.
	 * @param claveIf Clave interna del IF que tiene contrato de cesi�n pendientes por autorizar con alguna pyme.
	 * @return comboPyme Lista de objetos ElementoCatalogo con las pymes seleccionadas.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	 public List getComboPymesContratoCesionTestigo(String claveIf) throws AppException{
		log.info("getComboPymesContratoCesionTestigo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List comboPyme = new ArrayList();
		try{
			con.conexionDB();
			
			strSQL.append(" SELECT DISTINCT pyme.ic_pyme AS clave,");
			strSQL.append(" pyme.cg_razon_social AS descripcion");
			strSQL.append(" FROM cder_solicitud cds,");
			strSQL.append(" comcat_pyme pyme");
			strSQL.append(" WHERE cds.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND cds.ic_if = ?");
			strSQL.append(" AND cds.ic_estatus_solic IN (?, ?)");
			strSQL.append(" ORDER BY pyme.cg_razon_social");

			varBind.add(new Integer(claveIf));
			varBind.add(new Integer(7));
			varBind.add(new Integer(9));
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while(rst.next()){
				ElementoCatalogo elemento_catalogo = new ElementoCatalogo();
				elemento_catalogo.setClave(rst.getString("clave")==null?"":rst.getString("clave"));
				elemento_catalogo.setDescripcion(rst.getString("descripcion")==null?"":rst.getString("descripcion"));
				comboPyme.add(elemento_catalogo);
			}
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getComboPymesContratoCesionTestigo(S)");
		}
		return comboPyme;
	}
	
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Contratos de Cesi�n
	 * para el TestigoIF.
	 * @param parametrosConsulta HashMap con los parametros con los que se realiza la consulta de contratos de cesi�n.
	 * @return consultaContratoCesionTestigo Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultaContratoCesionTestigo(HashMap parametrosConsulta) throws AppException{
		log.info("consultaContratoCesionTestigo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaContratoCesionTestigo = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();
			
			String claveIf = parametrosConsulta.get("claveIf")==null?"":(String)parametrosConsulta.get("claveIf");
			String claveEpo = parametrosConsulta.get("claveEpo")==null?"":(String)parametrosConsulta.get("claveEpo");
			String clavePyme = parametrosConsulta.get("clavePyme")==null?"":(String)parametrosConsulta.get("clavePyme");
			String numeroContrato = parametrosConsulta.get("numeroContrato")==null?"":(String)parametrosConsulta.get("numeroContrato");
			String claveEstatusSol = parametrosConsulta.get("claveEstatusSol")==null?"":(String)parametrosConsulta.get("claveEstatusSol");
			String claveTipoContratacion = parametrosConsulta.get("claveTipoContratacion")==null?"":(String)parametrosConsulta.get("claveTipoContratacion");
			String fechaVigenciaIni = parametrosConsulta.get("fechaVigenciaIni")==null?"":(String)parametrosConsulta.get("fechaVigenciaIni");
			String fechaVigenciaFin = parametrosConsulta.get("fechaVigenciaFin")==null?"":(String)parametrosConsulta.get("fechaVigenciaFin");
			List camposAdicionales = parametrosConsulta.get("camposAdicionales")==null?new ArrayList():(List)parametrosConsulta.get("camposAdicionales");
			List tipoDato = parametrosConsulta.get("tipoDato")==null?new ArrayList():(List)parametrosConsulta.get("tipoDato");
			String loginUsuario = parametrosConsulta.get("loginUsuario")==null?"":(String)parametrosConsulta.get("loginUsuario");
      String clave_plazo_contrato = parametrosConsulta.get("clave_plazo_contrato")==null?"":(String)parametrosConsulta.get("clave_plazo_contrato");
      String plazo_contrato = parametrosConsulta.get("plazo_contrato")==null?"":(String)parametrosConsulta.get("plazo_contrato");
      String claveMoneda = parametrosConsulta.get("claveMoneda")==null?"":(String)parametrosConsulta.get("claveMoneda");
      
			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_tipo_contratacion AS clave_tipo_contratacion,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			strSQL.append(" pym.cg_rfc AS rfc,");
			strSQL.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
			strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
			strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
			strSQL.append(" TO_CHAR(sol.df_fecha_aceptacion, 'dd/mm/yyyy') AS fecha_aceptacion,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" sol.fn_monto_credito AS monto_credito,");
			strSQL.append(" sol.cg_referencia AS numero_referencia,");
			strSQL.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,");
			strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
			strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
			strSQL.append(" sol.cg_cuenta_clabe AS clabe,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud");      
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comrel_pyme_epo cpe");
			strSQL.append(", cdercat_ventanilla_pago cvp");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
      strSQL.append(", cdercat_tipo_plazo ctp");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_epo = cpe.ic_epo(+)");
			strSQL.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");      
			strSQL.append(" AND sol.cs_firmado_if = ?");
      strSQL.append(" AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			
			varBind.add("S");
			if(!claveIf.equals("")){
				strSQL.append(" AND sol.ic_if = ?");
				varBind.add(new Integer(claveIf));
			}
			if(!claveEpo.equals("")){
				strSQL.append(" AND sol.ic_epo = ?");
				varBind.add(new Integer(claveEpo));
			}
			if(!clavePyme.equals("")){
				strSQL.append(" AND sol.ic_pyme = ?");
				varBind.add(clavePyme);
			}
			if(!numeroContrato.equals("")){
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(numeroContrato);
			}
			if(!claveEstatusSol.equals("")){
				strSQL.append(" AND sol.ic_estatus_solic = ?");
				varBind.add(new Integer(claveEstatusSol));
			}else{
				strSQL.append(" AND sol.ic_estatus_solic IN (?, ?)");
				varBind.add(new Integer(7));
				varBind.add(new Integer(9));
			}
			if(!claveTipoContratacion.equals("")){
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(new Integer(claveTipoContratacion));
			}
			if(!fechaVigenciaIni.equals("") && !fechaVigenciaFin.equals("")){
				strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
				varBind.add(fechaVigenciaIni);
				varBind.add(fechaVigenciaFin);
			}
			if(!camposAdicionales.isEmpty()){
				for(int i = 0; i < camposAdicionales.size(); i++){
					String campoAdicional = (String)camposAdicionales.get(i);
					String tipo = (String)tipoDato.get(i);
					if(!campoAdicional.equals("")){
						strSQL.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
						if(tipo.equals("N")){varBind.add(campoAdicional);}else{varBind.add(campoAdicional);}
					}
				}
			}
      if (claveMoneda != null && !"".equals(claveMoneda)) {
        if (claveMoneda.equals("0")) {
          strSQL.append(" AND sol.cs_multimoneda = ?");
          varBind.add("S");
        } else {
          strSQL.append(" AND sol.cs_multimoneda = ?");
          strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
          strSQL.append(" FROM cder_monto_x_solic mxs");
          strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
          strSQL.append(" AND mxs.ic_moneda = ?)");
          varBind.add("N");
          varBind.add(new Integer(claveMoneda));
        }
      }      
      if(!clave_plazo_contrato.equals("")){
        strSQL.append(" AND sol.ic_tipo_plazo = ? ");
        varBind.add(new Integer(clave_plazo_contrato));
      }
      if(!plazo_contrato.equals("")){
        strSQL.append(" AND sol.ig_plazo = ? ");
        varBind.add(new Integer(plazo_contrato));
      }
      strSQL.append(" ORDER BY sol.df_alta_solicitud DESC"); 
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				String strClaveSolicitud = rst.getString("clave_solicitud");
				String strClaveEpo = rst.getString("clave_epo");
				int diasMinimosNotificacion = this.getDiasMinimosNotificacion(strClaveEpo);
			
				HashMap contratoCesion = new HashMap();
				contratoCesion.put("claveSolicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				contratoCesion.put("nombreEpo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				contratoCesion.put("nombrePyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				contratoCesion.put("rfc", rst.getString("rfc")==null?"":rst.getString("rfc"));
				contratoCesion.put("numeroProveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				contratoCesion.put("fechaSolicitudPyme", rst.getString("fecha_sol_pyme")==null?"":rst.getString("fecha_sol_pyme"));
				contratoCesion.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//contratoCesion.put("montoContrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				//contratoCesion.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				contratoCesion.put("tipoContratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				contratoCesion.put("fechaInicioContrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				contratoCesion.put("fechaFinContrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				contratoCesion.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				contratoCesion.put("fechaAceptacion", rst.getString("fecha_aceptacion")==null?"":rst.getString("fecha_aceptacion"));
				contratoCesion.put("campoAdicional1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				contratoCesion.put("campoAdicional2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				contratoCesion.put("campoAdicional3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				contratoCesion.put("campoAdicional4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				contratoCesion.put("campoAdicional5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				contratoCesion.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
				contratoCesion.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
				contratoCesion.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));
				contratoCesion.put("objetoContrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				contratoCesion.put("montoCredito", rst.getString("monto_credito")==null?"":rst.getString("monto_credito"));
				contratoCesion.put("numeroReferencia", rst.getString("numero_referencia")==null?"":rst.getString("numero_referencia"));
				contratoCesion.put("fechaVencimiento", rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento"));
				contratoCesion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				contratoCesion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				contratoCesion.put("clabe", rst.getString("clabe")==null?"":rst.getString("clabe"));
				contratoCesion.put("estatusSolicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				contratoCesion.put("claveEstatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				contratoCesion.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));//FODEA 041 - 2010 ACF
				contratoCesion.put("claveTipoContratacion", rst.getString("clave_tipo_contratacion")==null?"":rst.getString("clave_tipo_contratacion"));//FODEA 041 - 2010 ACF
				contratoCesion.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));//FODEA 041 - 2010 ACF
				contratoCesion.put("diasMinimosNotificacion", Integer.toString(diasMinimosNotificacion));
				
        StringBuffer monto_moneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

        contratoCesion.put("monto_moneda", monto_moneda.toString());
				contratoCesion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
        //contratoCesion.put("desc_plazo", rst.getString("desc_plazo")==null?"":rst.getString("desc_plazo"));
        //contratoCesion.put("plazo", rst.getString("plazo")==null?"":rst.getString("plazo"));
                
				if (!loginUsuario.equals("")) {
					if(this.esSolicitudFirmadaPorTestigo(strClaveSolicitud, loginUsuario)){
						contratoCesion.put("solicitudFirmadaTestigo", "S");
					} else {
						contratoCesion.put("solicitudFirmadaTestigo", "N");
					}
				}
				
				consultaContratoCesionTestigo.put("contratoCesion"+indice, contratoCesion);
				indice++;
			}
			
			consultaContratoCesionTestigo.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("consultaContratoCesionTestigo(S)");
		}
		return consultaContratoCesionTestigo;
	}
	
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Contratos de Cesi�n
	 * para el TestigoIF.
	 * @param claveSolicitud Clave interna de la solicitud con la que se realiza la consulta del contrato de cesi�n.
	 * @return consultaContratoCesionTestigo Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultaContratoCesionTestigo(String claveSolicitud) throws AppException{
		log.info("consultaContratoCesionTestigo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaContratoCesion = new HashMap();
		String numeroAcuse = "";
		int indice = 0;

		try {
			con.conexionDB();

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			strSQL.append(" pym.cg_rfc AS rfc_pyme,");
			strSQL.append(" crpe.cg_pyme_epo_interno AS numero_proveedor,");
			strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud_consentimiento,");
			strSQL.append(" TO_CHAR(SYSDATE, 'dd/mm/yyyy') AS fecha_formal_contrato,");
			strSQL.append(" TO_CHAR(sol.df_notif_vent, 'dd/mm/yyyy') AS fecha_notif_vent,");
			strSQL.append(" TO_CHAR(sol.df_redireccion, 'dd/mm/yyyy') AS fecha_redireccion,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
			strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
			strSQL.append(" sol.cg_cuenta_clabe AS cuenta_clabe,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			//strSQL.append(" sol.fn_monto_contrato AS monto_contrato,");
			//strSQL.append(" mon.cd_nombre AS moneda,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" TO_CHAR(sol.df_fecha_limite_firma, 'dd/mm/yyyy') AS fecha_limite_firma,");
			strSQL.append(" TO_CHAR(sol.df_fecha_aceptacion, 'dd/mm/yyyy') AS fecha_aceptacion_epo,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
			//strSQL.append(" sol.cc_acuse1 AS acuse1,");//ESTE ES EL ACUSE DE LA PYME CUANDO ENVIA LA SOLIC
			strSQL.append(" sol.cc_acuse2 AS acuse2,");//ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			strSQL.append(" sol.cc_acuse3 AS acuse3,");//ESTE ES EL ACUSE DE LA PYME CUANDO FIRMA EL ECONTRATO
			strSQL.append(" sol.cc_acuse4 AS acuse4,");//ESTE ES EL ACUSE DEL IF CUANDO FIRMA EL ECONTRATO
			strSQL.append(" sol.cc_acuse5 AS acuse5,");//ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA LA NOTIFICACION
			strSQL.append(" sol.cc_acuse_firma_rep1 AS acuse_firma_rep1,");
			strSQL.append(" sol.cc_acuse_firma_rep2 AS acuse_firma_rep2,");
			strSQL.append(" sol.cc_acuse_testigo1 AS acuse_firma_test1,");
			strSQL.append(" sol.cc_acuse_testigo2 AS acuse_firma_test2,");
			strSQL.append(" sol.cc_acuse_redirec AS acuse_redirec,");
			strSQL.append(" sol.cg_firma1 AS firma1,");
			strSQL.append(" sol.cg_firma2 AS firma2,");//FIRMA DEL IF
			strSQL.append(" sol.cg_firma3 AS firma3,");//FIRMA DEL PRIMER TESTIGO
			strSQL.append(" sol.cg_firma4 AS firma4,");//FIRMA DEL SEGUNDO TESTIGO
			strSQL.append(" sol.cg_firma_rep1 AS firma_rep1,");//FIRMA DEL PRIMER REPRESENTANTE LEGAL PYME
			strSQL.append(" sol.cg_firma_rep2 AS firma_rep2");//FIRMA DEL SEGUNDO REPRESENTANTE LEGAL PYME
      //strSQL.append(" ctp.cg_descripcion as desc_plazo,");
      //strSQL.append(" sol.ig_plazo as plazo");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comrel_pyme_epo crpe");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			//strSQL.append(", comcat_moneda mon");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
      strSQL.append(", cdercat_tipo_plazo ctp");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_pyme = crpe.ic_pyme(+)");
			strSQL.append(" AND sol.ic_epo = crpe.ic_epo(+)");
			//strSQL.append(" AND sol.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strSQL.append(" AND sol.ic_solicitud = ?");
      strSQL.append(" AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)");			     
                
			varBind.add(claveSolicitud);
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				String strClaveEpo = rst.getString("clave_epo");
				int diasMinimosNotificacion = this.getDiasMinimosNotificacion(strClaveEpo);
				
				consultaContratoCesion.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				consultaContratoCesion.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				consultaContratoCesion.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				consultaContratoCesion.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				consultaContratoCesion.put("nombre_pyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				consultaContratoCesion.put("rfcPyme", rst.getString("rfc_pyme")==null?"":rst.getString("rfc_pyme"));
				consultaContratoCesion.put("numeroProveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				consultaContratoCesion.put("fechaSolicitudConsentimiento", rst.getString("fecha_solicitud_consentimiento")==null?"":rst.getString("fecha_solicitud_consentimiento"));
				consultaContratoCesion.put("fechaNotifVentanilla", rst.getString("fecha_notif_vent")==null?"":rst.getString("fecha_notif_vent"));
				consultaContratoCesion.put("fechaRedirecccion", rst.getString("fecha_redireccion")==null?"":rst.getString("fecha_redireccion"));
				consultaContratoCesion.put("nombre_epo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				consultaContratoCesion.put("nombre_if", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
				consultaContratoCesion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				consultaContratoCesion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				consultaContratoCesion.put("numeroCuentaClabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				consultaContratoCesion.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//consultaContratoCesion.put("monto_contrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				//consultaContratoCesion.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				consultaContratoCesion.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				consultaContratoCesion.put("fecha_inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				consultaContratoCesion.put("fecha_fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				consultaContratoCesion.put("clasificacion_epo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				consultaContratoCesion.put("campo_adicional_1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				consultaContratoCesion.put("campo_adicional_2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				consultaContratoCesion.put("campo_adicional_3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				consultaContratoCesion.put("campo_adicional_4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				consultaContratoCesion.put("campo_adicional_5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				consultaContratoCesion.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				consultaContratoCesion.put("fecha_limite_firma", rst.getString("fecha_limite_firma")==null?"":rst.getString("fecha_limite_firma"));
				consultaContratoCesion.put("fechaAceptacionEpo", rst.getString("fecha_aceptacion_epo")==null?"":rst.getString("fecha_aceptacion_epo"));
				consultaContratoCesion.put("clave_estatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				consultaContratoCesion.put("estatus_solicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				consultaContratoCesion.put("dias_minimos_notificacion", Integer.toString(diasMinimosNotificacion));
        
        StringBuffer monto_moneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

        consultaContratoCesion.put("monto_moneda", monto_moneda.toString());
				consultaContratoCesion.put("plazo_contrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
        //consultaContratoCesion.put("desc_plazo", rst.getString("desc_plazo")==null?"":rst.getString("desc_plazo"));
        //consultaContratoCesion.put("plazo", rst.getString("plazo")==null?"":rst.getString("plazo"));

				//ACUSE DE ACEPTACION DE LA SOLICITUD DE CONSENTIMIENTO POR PARTE DE LA EPO
				numeroAcuse = rst.getString("acuse2")==null?"":rst.getString("acuse2");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseEnvioSolicEpo", rst.getString("acuse2")==null?"":rst.getString("acuse2"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrEpoAutCesion", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrEpoAutCesion", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaEpoAutCesion", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaEpoAutCesion", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE CONSENTIMIENTO POR EL PRIMER REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("acuse_firma_rep1")==null?"":rst.getString("acuse_firma_rep1");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaRep1", rst.getString("acuse_firma_rep1")==null?"":rst.getString("acuse_firma_rep1"));
					consultaContratoCesion.put("firmaRep1", rst.getString("firma_rep1")==null?"":rst.getString("firma_rep1"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrAutRep1", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrAutRep1", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaAutRep1", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaAutRep1", datosAcuse.get("horaAcuse"));
						consultaContratoCesion.put("fechaFormalContrato", datosAcuse.get("fechaAcuse"));
					}
				} else {
					numeroAcuse = rst.getString("acuse3")==null?"":rst.getString("acuse3");
						if (!numeroAcuse.equals("")) {
						consultaContratoCesion.put("acuseFirmaRep1", rst.getString("acuse3")==null?"":rst.getString("acuse3"));
						consultaContratoCesion.put("firmaRep1", rst.getString("firma1")==null?"":rst.getString("firma1"));
						
						HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);
	
						if (!datosAcuse.isEmpty()) {
							consultaContratoCesion.put("claveUsrAutRep1", datosAcuse.get("claveUsuario"));
							consultaContratoCesion.put("nombreUsrAutRep1", datosAcuse.get("nombreUsuario"));
							consultaContratoCesion.put("fechaAutRep1", datosAcuse.get("fechaAcuse"));
							consultaContratoCesion.put("horaAutRep1", datosAcuse.get("horaAcuse"));
							consultaContratoCesion.put("fechaFormalContrato", datosAcuse.get("fechaAcuse"));
						}
					}
				}
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE CONSENTIMIENTO POR EL SEGUNDO REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("acuse_firma_rep2")==null?"":rst.getString("acuse_firma_rep2");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaRep2", rst.getString("acuse_firma_rep2")==null?"":rst.getString("acuse_firma_rep2"));
					consultaContratoCesion.put("firmaRep2", rst.getString("firma_rep2")==null?"":rst.getString("firma_rep2"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrAutRep2", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrAutRep2", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaAutRep2", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaAutRep2", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL IF
				numeroAcuse = rst.getString("acuse4")==null?"":rst.getString("acuse4");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaIf", rst.getString("acuse4")==null?"":rst.getString("acuse4"));
					consultaContratoCesion.put("firmaIf", rst.getString("firma2")==null?"":rst.getString("firma2"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrIf", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrIf", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaIf", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaIf", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL PRIMER TESTIGO DEL IF
				numeroAcuse = rst.getString("acuse_firma_test1")==null?"":rst.getString("acuse_firma_test1");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaTest1", rst.getString("acuse_firma_test1")==null?"":rst.getString("acuse_firma_test1"));
					consultaContratoCesion.put("firmaTest1", rst.getString("firma3")==null?"":rst.getString("firma3"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrTest1", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrTest1", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaTest1", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaTest1", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL SEGUNDO TESTIGO DEL IF
				numeroAcuse = rst.getString("acuse_firma_test2")==null?"":rst.getString("acuse_firma_test2");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseFirmaTest2", rst.getString("acuse_firma_test2")==null?"":rst.getString("acuse_firma_test2"));
					consultaContratoCesion.put("firmaTest2", rst.getString("firma4")==null?"":rst.getString("firma4"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrTest2", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrTest2", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaTest2", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaTest2", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DE LA NOTIFICACION DEL CONTRATO DE CESI�N POR PARTE DE LA EPO
				numeroAcuse = rst.getString("acuse5")==null?"":rst.getString("acuse5");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseNotificacionEpo", rst.getString("acuse5")==null?"":rst.getString("acuse5"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrNotifEpo", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrNotifEpo", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaNotifEpo", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaNotifEpo", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE REDIRECCIONAMIENTO DE LA CTA DE LA EPO POR LA VENTANILLA
				numeroAcuse = rst.getString("acuse_redirec")==null?"":rst.getString("acuse_redirec");
				if (!numeroAcuse.equals("")) {
					consultaContratoCesion.put("acuseRedireccion", rst.getString("acuse_redirec")==null?"":rst.getString("acuse_redirec"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						consultaContratoCesion.put("claveUsrRedirec", datosAcuse.get("claveUsuario"));
						consultaContratoCesion.put("nombreUsrRedirec", datosAcuse.get("nombreUsuario"));
						consultaContratoCesion.put("fechaRedirec", datosAcuse.get("fechaAcuse"));
						consultaContratoCesion.put("horaRedirec", datosAcuse.get("horaAcuse"));
					}
				}
				indice++;
			}
			
			consultaContratoCesion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("consultaContratoCesionTestigo(S)");
		}
		return consultaContratoCesion;
	}

	/**
	 * M�todo que guarda la firma del contrato de cesi�n de derechos por parte del Testigo del IF.
	 * @param parametrosFirma HashMap con los parametros con los que se reliza la autenticaci�n del usuario.
	 * @return acuse Cadena con el numero de acuse generado al autenticar al usuario.
	 * @throws Appexception Cuando ocurre un error al obtener el acuse y/o guardar la firma de la pyme.
	 */
	public String acuseFirmaContratoCesionTestigo(HashMap parametrosFirma) throws AppException{
		log.info("acuseFirmaContratoCesionTestigo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		String acuse = "";
			
		String _serial = parametrosFirma.get("_serial")==null?"":(String)parametrosFirma.get("_serial");
		String textoPlano = parametrosFirma.get("textoPlano")==null?"":(String)parametrosFirma.get("textoPlano");
		String pkcs7 = parametrosFirma.get("pkcs7")==null?"":(String)parametrosFirma.get("pkcs7");
		String folioCert = parametrosFirma.get("folioCert")==null?"":(String)parametrosFirma.get("folioCert");
		boolean validCert = Boolean.getBoolean((String)parametrosFirma.get("validCert"));
		String claveSolicitud = parametrosFirma.get("claveSolicitud")==null?"":(String)parametrosFirma.get("claveSolicitud");
		String loginUsuario = parametrosFirma.get("loginUsuario")==null?"":(String)parametrosFirma.get("loginUsuario");
		String nombreUsuario = parametrosFirma.get("nombreUsuario")==null?"":(String)parametrosFirma.get("nombreUsuario");
		String tipoOperacion = parametrosFirma.get("tipoOperacion")==null?"":(String)parametrosFirma.get("tipoOperacion");
		String selloDigital = parametrosFirma.get("selloDigital")==null?"":(String)parametrosFirma.get("selloDigital");
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		int seqNEXTVAL = 0;
		
		try{
			con.conexionDB();
			
			String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
							if(rs1.next())
							{
								seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
							}
						ps1.close();
						rs1.close();
						
			
			char getReceipt = 'Y';
			if(!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("") ){
				Seguridad s = new Seguridad();
				if( s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt) ){
					acuse = s.getAcuse();

					String acuseTestigo1 = "";
					
					strSQL = new StringBuffer();
					varBind = new ArrayList();
		
					strSQL.append(" SELECT cc_acuse_testigo1 AS acuse_testigo1");
					strSQL.append(" FROM cder_solicitud");
					strSQL.append(" WHERE ic_solicitud = ?");
					
					varBind.add(claveSolicitud);
		
					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind.toString());
					
					PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ResultSet rst1 = pst1.executeQuery();
					
					while (rst1.next()) {
						acuseTestigo1 = rst1.getString("acuse_testigo1")==null?"":rst1.getString("acuse_testigo1");
					}
					
					rst1.close();
					pst1.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					if (!acuseTestigo1.equals("")){
						//Se genera el contrato con la fecha actualizada
						
						strSQL.append(" UPDATE cder_solicitud");
						strSQL.append(" SET cc_acuse_testigo2 = ?,");		//Acuse de Firma del Segundo Testigo IF
						strSQL.append(" df_firma_testigo2 = SYSDATE,");		//Fecha de Firma del Segundo Testigo IF
						strSQL.append(" cg_firma4 = ?,");						//Firma del Segundo Testigo IF
						strSQL.append(" ic_estatus_solic = ?");				//Nuevo estatus
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(new Integer(seqNEXTVAL)); 
						varBind.add(selloDigital);
						varBind.add(new Integer(9));
						varBind.add(claveSolicitud);
					} else {
						strSQL.append(" UPDATE cder_solicitud");
						strSQL.append(" SET cc_acuse_testigo1 = ?,");		//Acuse de Firma del Primer Testigo IF
						strSQL.append(" df_firma_testigo1 = SYSDATE,");		//Firma del Primer Testigo IF
						strSQL.append(" cg_firma3 = ?");							//Firma del Primer Testigo IF
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(new Integer(seqNEXTVAL));  
						varBind.add(selloDigital);
						varBind.add(claveSolicitud);
					}					
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(seqNEXTVAL)); 
					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(acuse);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}
			}
		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error al firmar el contrato de cesi�n de derechos: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseFirmaContratoCesionTestigo(S)");
		}
		return acuse;
	}

	/**
	 * M�todo que consulta si la solicitud ya fue firmada por un determinado testigo,
	 * de ser asi, inabilita las opciones de firma en pantalla para dicho testigo.
	 * @param claveSolicitud Clave interna de la solicitud de cesi�n que se va a firmar.
	 * @param loginUsuario Clave de acceso al sistema del testigo que va a firmar la solicitud.
	 * @return solicitudFirmada Bandera con valor true si el testigo ya firmo la solicitud, false en caso contrario.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	 public boolean esSolicitudFirmadaPorTestigo(String claveSolicitud, String loginUsuario) throws AppException{
		log.info("esSolicitudFirmadaPorTestigo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean solicitudFirmada = false;
		
		try{
			con.conexionDB();

			strSQL.append(" SELECT acs.cg_usuario AS login_usuario");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(" , cder_acuse acs");
			strSQL.append(" WHERE sol.cc_acuse_testigo1 = acs.cc_acuse");
			strSQL.append(" AND sol.ic_solicitud = ?");
			
			varBind.add(claveSolicitud);
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			String accLoginUsuario = "";
			
			while (rst.next()) {
				accLoginUsuario = rst.getString("login_usuario")==null?"":rst.getString("login_usuario");
			}
			
			if (loginUsuario.equals(accLoginUsuario)) {
				solicitudFirmada = true;
			}
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("esSolicitudFirmadaPorTestigo(S)");
		}
		return solicitudFirmada;
	}
	//..::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: FODEA 037 - 2009 ACF (F)
/**
	 * @String noEpo numero de la epo
	 * @String mensaje mensaje de Notificacion de Cesion de derechos
	 * @throws netropology.utilerias.AppException
	 * @return metodo que actualiza el mensaje de Notificacion de Cesion de derechos
	 * de la epo
	 * @param mensaje
	 * @param noEpo
	 */
	public String MensajeNotiCesionDerechos(String noEpo,String mensaje)  throws AppException{
		
		log.info("MensajeNotiCesionDerechos(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean   commit = true;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String actualizada = "";	
		
		
		try{
			con.conexionDB();
			
			
			strSQL.append(" UPDATE comcat_epo SET CG_MENSAJE_CESION = ? WHERE  ic_epo  = ? ");
			
			varBind.add(mensaje);
			varBind.add(new Integer(noEpo));
					
			ps = con.queryPrecompilado(strSQL.toString(),varBind);
			ps.executeUpdate();
			ps.close();
		
			log.debug("MensajeNotiCesionDerechos :: "+strSQL.toString() + "   "+varBind  );							
			actualizada = "S";
			
		} 	catch(Exception e) {
						commit = false;
						log.error("MensajeNotiCesionDerechos(Exception) " + e);
						e.printStackTrace();				
					throw new AppException("MensajeNotiCesionDerechos(Exception) ", e);
				}
				finally
				{
					con.terminaTransaccion(commit);
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
				}
				log.info("MensajeNotiCesionDerechos (S)");
				return actualizada;
	}
	
	/**
	 * @String noEpo numero de la epo
	 * @throws netropology.utilerias.AppException
	 * @return metodo que muestra el mensaje de Notificacion de Cesion de derechos de la epo
	 * @param noEpo
	 */
	public String MostrarMensajeNotiCesionDerechos(String noEpo)  throws AppException{
		
		log.info("MostrarMensajeNotiCesionDerechos(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean   commit = true;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String mensaje = "";	
		
		
		try{
			con.conexionDB();
			
		
			strSQL.append(" SELECT CG_MENSAJE_CESION as mensaje FROM COMCAT_EPO  WHERE  ic_epo  = ? ");
			varBind.add(new Integer(noEpo));
			
			ps = con.queryPrecompilado(strSQL.toString(), varBind);			
			rs = ps.executeQuery();
		
			while(rs.next()){		
				mensaje =  rs.getString("mensaje")==null?"":rs.getString("mensaje"); 
				 			
			}
	
			rs.close();
			ps.close();
			
		
			log.debug("MostrarMensajeNotiCesionDerechos :: "+strSQL.toString() + "   "+varBind  );							
		
			
			} 	catch(Exception e) {
						commit = false;
						log.error("MostrarMensajeNotiCesionDerechos(Exception) " + e);
						e.printStackTrace();				
					throw new AppException("MostrarMensajeNotiCesionDerechos(Exception) ", e);
				}
				finally
				{
					con.terminaTransaccion(commit);
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
				}
				log.info("MostrarMensajeNotiCesionDerechos (S)");
				
		return mensaje;
	}
	
/**
	 * @String solicitud numero de la epo
	 * @throws netropology.utilerias.AppException
	 * @return metodo que inicializa el Aviso de Notificacion de la epo
	 * realizando una consulta sobre la tabla con cder_solicitud
	 * @param solicitud
	 */
public List  inicializarAvisoNotificacionCesion (String solicitud)	throws AppException	{
		
				log.info("inicializarAvisoNotificacionCesion (E)");


					PreparedStatement ps = null;
					ResultSet rs        = null;
					AccesoDB	 con 	     =   new AccesoDB();
					boolean	 resultado = true;
					StringBuffer qrySentencia = new StringBuffer();
					List lvarbind       = new ArrayList();		
					List registro = new ArrayList();
					boolean   commit = true;
					
					try {
					con.conexionDB();
					
												
					qrySentencia.append(" SELECT i.cg_razon_social as Cecionario,  "+
														"  p.CG_RAZON_SOCIAL as pyme,"+
														"  p.CG_RFC  as Rfc, "+ 
														"  p.IC_PYME as NoProveedor, "+   
														" TO_CHAR(df_fecha_Solicitud_Ini,'DD/MM/YYYY')  as FSolicitudIni,  "+
														" sol.cg_no_contrato as NoContrato,  "+
														" sol.fn_monto_contrato as Monto,  "+
														" sol.ic_moneda as Moneda,  "+
														" con.cg_nombre as Contratacion,  "+
														" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS FContratoIni,"+
														" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS FContratoFin,"+
														" cl.cg_area as Clasificacion,  "+
														" sol.cg_campo1 as Campo1,  "+
														" sol.cg_campo2 as Campo2,  "+
														" sol.cg_campo3 as Campo3,  "+
														" sol.cg_campo4 as Campo4,  "+
														" sol.cg_campo5 as Campo5,  "+  
														" sol.cg_obj_contrato as ObContrato,  "+                                
														" TO_CHAR(sol.df_fecha_limite_firma,'DD/MM/YYYY') as FLimite,  "+
														" es.cg_descripcion as Estatus , "+
														" sol.ic_solicitud as Solicitud, "+                                     
														" sol.ic_estatus_solic as NoEstatus, "+
														" sol.cc_acuse2 as Acuse, "+														
														" TO_CHAR(sol.df_fecha_aceptacion,'DD/MM/YYYY') as FAceptacion,  "+
														" e.CG_MENSAJE_CESION as mensaje "+
														", DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato"+
														", cvp.cg_descripcion AS venanilla_pago,"+
														" sol.cg_sup_adm_resob AS sup_adm_resob,"+
														" sol.cg_numero_telefono AS numero_telefono,"
														+" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato, "+
														" NVL(sol.CG_EMPRESAS_REPRESENTADAS,'') as empresas"
														);
					
					
 
						qrySentencia.append(" FROM cder_solicitud  sol, "+
													" comcat_epo e, "+
													" comcat_if i, "+
													" cdercat_clasificacion_epo cl, "+
													" cdercat_tipo_contratacion con, "+
													" cdercat_estatus_solic es,"+
													" cdercat_tipo_plazo stp,"+
													" COMCAT_PYME p "+
													", cdercat_ventanilla_pago cvp"
													);
													
						qrySentencia.append(" WHERE  sol.ic_epo =e.ic_epo "+
													" AND sol.ic_if = i.ic_if "+
													" AND sol.ic_clasificacion_epo  = cl.ic_clasificacion_epo "+
													" AND SOL.ic_tipo_contratacion = CON.ic_tipo_contratacion "+
													" AND SOL.ic_estatus_solic = ES.ic_estatus_solic "+
													" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)"+
													" AND sol.ic_epo = cvp.ic_epo(+)"+
													" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)"+
													" AND sol.IC_PYME = p.IC_PYME ");
					
					qrySentencia.append(" and ic_solicitud = ? ");
							
					lvarbind.add(solicitud);					
					registro = con.consultaDB(qrySentencia.toString(),lvarbind, false);	
					
					log.debug("inicializarAvisoNotificacionCesion "+ qrySentencia.toString()+ lvarbind);
					log.debug("inicializarAvisoNotificacionCesion "+ registro);
					
			
			} 	catch(Exception e) {
						commit = false;
						log.error("inicializarAvisoNotificacionCesion(Exception) " + e);
						e.printStackTrace();				
					throw new AppException("inicializarAvisoNotificacionCesion(Exception) ", e);
				}
				finally
				{
					con.terminaTransaccion(commit);
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
				}
				log.info("inicializarAvisoNotificacionCesion (S)");
		
				
				return registro;
			
		}
	//FODEA 041 - 2010 ACF (I)
	/**
	 * M�todo que obtiene las solicitudes de consentimiento que la pyme ha enviado y que han sido aceptadas
	 * por las respectivas epos con las que fueron capturadas para ser presentadas en la pantalla de aviso.
	 * @param clavePyme Clave interna de la pyme de la que se van a consultar las solicitudes.
	 * @return solicitudesAceptadas HashMap con las solicitudes de la pyme que han sido aceptadas por la epo.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 * @author Alberto Cruz Flores.
	 */
	public HashMap consultaAvisoSolicitudConsentimientoPyme(String clavePyme) throws AppException {
		log.info("consultaAvisoSolicitudConsentimientoPyme(E) ::..");
		AccesoDB con = new AccesoDB();
		ResultSet rst = null;
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap solicitudesAceptadasEpo = new HashMap();
		try {
			con.conexionDB();
			
			strSQL.append(" SELECT csol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
			strSQL.append(" pyme.cg_razon_social AS nombre_cedente, ");
			strSQL.append(" pyme.cg_rfc AS rfc_cendente,"); 
			strSQL.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
			strSQL.append(" TO_CHAR(csol.df_alta_solicitud, 'dd/mm/yyyy') AS fecha_solicitud,");
			strSQL.append(" csol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" TO_CHAR(csol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato, ");
			strSQL.append(" ( CASE WHEN TRUNC (DIAS_LABORABLES(csol.DF_FECHA_ACEPTACION,15-3)) < TRUNC (SYSDATE)   or  (dias_laborables (csol.df_fecha_aceptacion, 15)) <= TRUNC (SYSDATE )  	then  'false' else 'true' END) as vence_prorroga,");
			
			strSQL.append(" NVL(csol.CG_EMPRESAS_REPRESENTADAS,'') as empresas");//FODEA-024-0214 MOD()
			strSQL.append(" FROM cder_solicitud csol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", comcat_pyme pyme");
			strSQL.append(", comrel_pyme_epo cpe");
			strSQL.append(" WHERE csol.ic_epo = epo.ic_epo");
			strSQL.append(" AND csol.ic_if = cif.ic_if");
			strSQL.append(" AND csol.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND csol.ic_epo = cpe.ic_epo(+)");
			strSQL.append(" AND csol.ic_pyme = cpe.ic_pyme(+)");
			strSQL.append(" AND csol.ic_pyme = ?");
			strSQL.append(" AND csol.ic_estatus_solic in (  3, 7, 9, 14 ) ");  
			strSQL.append(" order by vence_prorroga desc"); 
			
			varBind.add(clavePyme);
			
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			int numeroRegistros = 0;
			
			while (rst.next()) {
				HashMap solicitudAceptadaEpo = new HashMap();
				StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));
				solicitudAceptadaEpo.put("nombreEpo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				solicitudAceptadaEpo.put("nombreCesionario", rst.getString("nombre_cesionario")==null?"":rst.getString("nombre_cesionario"));
				solicitudAceptadaEpo.put("nombreCedente", rst.getString("nombre_cedente")==null?"":rst.getString("nombre_cedente"));
				solicitudAceptadaEpo.put("rfcCendente", rst.getString("rfc_cendente")==null?"":rst.getString("rfc_cendente"));
				solicitudAceptadaEpo.put("numeroProveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				solicitudAceptadaEpo.put("fechaSolicitud", rst.getString("fecha_solicitud")==null?"":rst.getString("fecha_solicitud"));
				solicitudAceptadaEpo.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				//solicitudAceptadaEpo.put("montoContrato", rst.getString("monto_contrato")==null?"":rst.getString("monto_contrato"));
				solicitudAceptadaEpo.put("montosPorMoneda", montosPorMoneda.toString());
				solicitudAceptadaEpo.put("firma_contrato", rst.getString("firma_contrato")==null?"":rst.getString("firma_contrato"));
				solicitudAceptadaEpo.put("vence_prorroga", rst.getString("vence_prorroga")==null?"":rst.getString("vence_prorroga"));
				
				solicitudAceptadaEpo.put("empresas", rst.getString("empresas")==null?"":rst.getString("empresas"));//FODEA-024-2014 MOD()
				
				solicitudesAceptadasEpo.put("solicitudAceptadaEpo" + numeroRegistros, solicitudAceptadaEpo);
				
				
				numeroRegistros++;
			}
			
			solicitudesAceptadasEpo.put("numeroRegistros", Integer.toString(numeroRegistros));
			
			rst.close();
			pst.close();
		} catch (Exception e) {
			log.error("consultaAvisoSolicitudConsentimientoPyme(ERROR) ::..");
			throw new AppException("Error al consultar las solicitudes aceptadas.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultaAvisoSolicitudConsentimientoPyme(S) ::..");
		}
		return solicitudesAceptadasEpo;
	}
	
	/**
	 * M�todo que inserta una nueva solicitud de consentimiento de la pyme.
	 * @param datosCapturaSolicitud HashMap con la informaci�n con la que se va a capturar la solicitud de consentiemiento.
	 * @return numeroProceso numero del proceso de carga de la nueva solicitud.
	 * @throws AppException Cuando ocurre un error al insertar el registro.
	 * @author Alberto Cruz Flores.
	 */
	public String insertarSolicitudConsentimientoTemp(HashMap datosCapturaSolicitud) throws AppException {
		log.info("insertarSolicitudConsentimientoTemp(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		String numeroProceso = "";
		String numeroAcuse = "";
		String numeroSolicitud = "";
		
		try {
			con.conexionDB();
			
			String clavePyme = datosCapturaSolicitud.get("clavePyme")==null?"":(String)datosCapturaSolicitud.get("clavePyme");
			String claveEpo = datosCapturaSolicitud.get("claveEpo")==null?"":(String)datosCapturaSolicitud.get("claveEpo");
			String claveIfCesionario = datosCapturaSolicitud.get("claveIfCesionario")==null?"":(String)datosCapturaSolicitud.get("claveIfCesionario");
			String claveClasificacionEpo = datosCapturaSolicitud.get("claveClasificacionEpo")==null?"":(String)datosCapturaSolicitud.get("claveClasificacionEpo");
			String numeroContrato = datosCapturaSolicitud.get("numeroContrato")==null?"":(String)datosCapturaSolicitud.get("numeroContrato");
			String montoContrato = datosCapturaSolicitud.get("montoContrato")==null?"":(String)datosCapturaSolicitud.get("montoContrato");
			String claveMoneda = datosCapturaSolicitud.get("claveMoneda")==null?"":(String)datosCapturaSolicitud.get("claveMoneda");
			String claveTipoContratacion = datosCapturaSolicitud.get("claveTipoContratacion")==null?"":(String)datosCapturaSolicitud.get("claveTipoContratacion");
			String fechaVigenciaContratoIni = datosCapturaSolicitud.get("fechaVigenciaContratoIni")==null?"":(String)datosCapturaSolicitud.get("fechaVigenciaContratoIni");
			String fechaVigenciaContratoFin = datosCapturaSolicitud.get("fechaVigenciaContratoFin")==null?"":(String)datosCapturaSolicitud.get("fechaVigenciaContratoFin");
			String objetoContrato = datosCapturaSolicitud.get("objetoContrato")==null?"":(String)datosCapturaSolicitud.get("objetoContrato");
			String comentariosSolicitud = datosCapturaSolicitud.get("comentariosSolicitud")==null?"":(String)datosCapturaSolicitud.get("comentariosSolicitud");
			String csTipoMonto = datosCapturaSolicitud.get("csTipoMonto")==null?"":(String)datosCapturaSolicitud.get("csTipoMonto");
			String montoContratoMin = datosCapturaSolicitud.get("montoContratoMin")==null?"":(String)datosCapturaSolicitud.get("montoContratoMin");
			String montoContratoMax = datosCapturaSolicitud.get("montoContratoMax")==null?"":(String)datosCapturaSolicitud.get("montoContratoMax");
			String plazoContrato = datosCapturaSolicitud.get("plazoContrato")==null?"":(String)datosCapturaSolicitud.get("plazoContrato");
			String claveTipoPlazo = datosCapturaSolicitud.get("claveTipoPlazo")==null?"":(String)datosCapturaSolicitud.get("claveTipoPlazo");
			String claveVentanillaPago = datosCapturaSolicitud.get("claveVentanillaPago")==null?"":(String)datosCapturaSolicitud.get("claveVentanillaPago");
			String supervisorAdministradorResidente = datosCapturaSolicitud.get("supervisorAdministradorResidente")==null?"":(String)datosCapturaSolicitud.get("supervisorAdministradorResidente");
			String numeroTelefono = datosCapturaSolicitud.get("numeroTelefono")==null?"":(String)datosCapturaSolicitud.get("numeroTelefono");
			List claveMonedaHid = datosCapturaSolicitud.get("claveMonedaHid")==null?new ArrayList():(List)datosCapturaSolicitud.get("claveMonedaHid");
			List montoMoneda = datosCapturaSolicitud.get("montoMoneda")==null?new ArrayList():(List)datosCapturaSolicitud.get("montoMoneda");
			List camposAdicionales = datosCapturaSolicitud.get("camposAdicionales")==null?new ArrayList():(List)datosCapturaSolicitud.get("camposAdicionales");
			
			//Multimoneda
			String montoContratoMinDL = datosCapturaSolicitud.get("montoContratoMinDL")==null?"":(String)datosCapturaSolicitud.get("montoContratoMinDL");
			String montoContratoMaxDL = datosCapturaSolicitud.get("montoContratoMaxDL")==null?"":(String)datosCapturaSolicitud.get("montoContratoMaxDL");
			String montoContratoMinEU = datosCapturaSolicitud.get("montoContratoMinEU")==null?"":(String)datosCapturaSolicitud.get("montoContratoMinEU");
			String montoContratoMaxEU = datosCapturaSolicitud.get("montoContratoMaxEU")==null?"":(String)datosCapturaSolicitud.get("montoContratoMaxEU");
			//
			String empresasRep = datosCapturaSolicitud.get("empresas")==null?"":(String)datosCapturaSolicitud.get("empresas");
			String firmaContrato = datosCapturaSolicitud.get("firmaContrato")==null?"":(String)datosCapturaSolicitud.get("firmaContrato");
			
			String grupoCDer = datosCapturaSolicitud.get("grupoCDer")==null?"":(String)datosCapturaSolicitud.get("grupoCDer");
			
			strSQL.append(" SELECT seq_cder_solicitud.nextval AS numero_solicitud FROM dual");
			log.debug("..:: strSQL: "+strSQL.toString());
			pst = con.queryPrecompilado(strSQL.toString());
			rst = pst.executeQuery();
			
			while (rst.next()) {
				numeroSolicitud = rst.getString("numero_solicitud")==null?"1":rst.getString("numero_solicitud");
			}
			
			rst.close();
			pst.close();
			
			strSQL = new StringBuffer();
			strSQL.append(" SELECT seq_cder_acuse.nextval AS numero_acuse FROM dual");
			log.debug("..:: strSQL: "+strSQL.toString());
			pst = con.queryPrecompilado(strSQL.toString());
			rst = pst.executeQuery();
			
			while (rst.next()) {
				numeroAcuse = rst.getString("numero_acuse")==null?"1":rst.getString("numero_acuse");
			}
			
			rst.close();
			pst.close();

			strSQL = new StringBuffer();
			strSQL.append(" SELECT NVL(MAX(ic_proceso), 0) + 1 AS numero_proceso FROM cdertmp_solicitud");
			log.debug("..:: strSQL: "+strSQL.toString());
			pst = con.queryPrecompilado(strSQL.toString());
			rst = pst.executeQuery();
			
			while (rst.next()) {
				numeroProceso = rst.getString("numero_proceso")==null?"1":rst.getString("numero_proceso");
			}
			
			rst.close();
			pst.close();
			
			strSQL = new StringBuffer();
			strSQL.append(" INSERT INTO cdertmp_solicitud (");
			strSQL.append(" ic_proceso,");
			strSQL.append(" ic_solicitud,");
			strSQL.append(" cc_acuse1,");
			strSQL.append(" ic_pyme,");
			strSQL.append(" ic_epo,");
			strSQL.append(" ic_if,");
			strSQL.append(" ic_clasificacion_epo,");
			strSQL.append(" cg_no_contrato,");
			strSQL.append(" cs_multimoneda,");
			strSQL.append(" ic_tipo_contratacion,");
			if (!claveTipoPlazo.equals("")) {
				strSQL.append(" ig_plazo,");
				strSQL.append(" ic_tipo_plazo,");
			} else {
				strSQL.append(" df_fecha_vigencia_Ini,");
				strSQL.append(" df_fecha_vigencia_Fin,");
			}
			if (camposAdicionales != null && !camposAdicionales.isEmpty()) {
				for (int i = 0; i < camposAdicionales.size(); i++) {
					strSQL.append(" cg_campo" + (i + 1) + ",");
				}
			}
			strSQL.append(" cg_obj_contrato,");
			if (!comentariosSolicitud.equals("")) {
				strSQL.append(" cg_comentarios,");
			}
			
			//Se agregan Empresas representadas FODEA XXX-2014
			if (!empresasRep.equals("")) {
				strSQL.append(" cg_empresas_representadas,");
			}
			//
			strSQL.append(" ic_ventanilla_pago,");
			strSQL.append(" cg_sup_adm_resob,");
			strSQL.append(" cg_numero_telefono,");
			strSQL.append(" ic_estatus_solic, df_firma_contrato ");
			//
			if (!grupoCDer.equals("")) {
				strSQL.append(" ,ic_grupo_cesion ");
			}
			strSQL.append(" ) VALUES (");
			strSQL.append(" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,");
			if (!claveTipoPlazo.equals("")) {
				strSQL.append(" ?, ?,");
			} else {
				strSQL.append(" TO_DATE(?, 'dd/mm/yyyy'), TO_DATE(?, 'dd/mm/yyyy'),");
			}
			if (camposAdicionales != null && !camposAdicionales.isEmpty()) {
				for (int i = 0; i < camposAdicionales.size(); i++) {
					strSQL.append(" ?,");
				}
			}
			strSQL.append(" ?,");
			if (!comentariosSolicitud.equals("")) {
				strSQL.append(" ?,");
			}
			if (!empresasRep.equals("")) {
				strSQL.append(" ?,");
			}
			strSQL.append(" ?,");
			strSQL.append(" ?,");
			strSQL.append(" ?,");
			strSQL.append(" ? , TO_DATE(?,'dd/mm/yyyy') ");
			if (!grupoCDer.equals("")) {
				strSQL.append(" , ? ");
			}

			strSQL.append(")");
			
			varBind.add(numeroProceso);
			varBind.add(numeroSolicitud);
			varBind.add(numeroAcuse);
			varBind.add(clavePyme);
			varBind.add(new Integer(claveEpo));
			varBind.add(new Integer(claveIfCesionario));
			varBind.add(new Integer(claveClasificacionEpo));
			varBind.add(numeroContrato.toUpperCase());
			if (!claveMoneda.equals("0")) {
				varBind.add("N");
			} else {
				varBind.add("S");
			}
			varBind.add(new Integer(claveTipoContratacion));
			if (!claveTipoPlazo.equals("")) {
				varBind.add(plazoContrato);
				varBind.add(claveTipoPlazo);
			} else {
				varBind.add(fechaVigenciaContratoIni);
				varBind.add(fechaVigenciaContratoFin);
			}
			if (camposAdicionales != null && !camposAdicionales.isEmpty()) {
				for (int i = 0; i < camposAdicionales.size(); i++) {
					varBind.add((String)camposAdicionales.get(i));
				}
			}
			varBind.add(objetoContrato);
			if (!comentariosSolicitud.equals("")) {
				varBind.add(comentariosSolicitud);
			}
			if (!empresasRep.equals("")) {
				varBind.add(empresasRep);
			}
			varBind.add(new Integer(claveVentanillaPago));
			varBind.add(supervisorAdministradorResidente);
			varBind.add(numeroTelefono);
			varBind.add(new Integer(1));
			varBind.add(firmaContrato);
			if (!grupoCDer.equals("")) {
				varBind.add(grupoCDer);
			}
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
			
			if (!claveMoneda.equals("0")) {
				strSQL = new StringBuffer();
				varBind = new ArrayList();
				strSQL.append(" INSERT INTO cdertmp_monto_x_solic (");
				strSQL.append(" ic_proceso,");
				strSQL.append(" ic_solicitud,");
				strSQL.append(" ic_moneda,");
				strSQL.append(" fn_monto_moneda,");
				strSQL.append(" fn_monto_moneda_min,");
				strSQL.append(" fn_monto_moneda_max");
				strSQL.append(" ) VALUES (");
				strSQL.append(" ?, ?, ?,");
				if (csTipoMonto.equals("D")) {
					strSQL.append(" ?, null, null");
				} else {
					strSQL.append(" null, ?, ?");
				}
				strSQL.append(")");
				
				varBind.add(numeroProceso);
				varBind.add(numeroSolicitud);
				varBind.add(new Integer(claveMoneda));
				if (csTipoMonto.equals("D")) {
					varBind.add(new BigDecimal(montoContrato));
				} else {
					if(claveMoneda.equals("25")){
						montoContratoMin=montoContratoMinDL;
						montoContratoMax=montoContratoMaxDL;
					}else if(claveMoneda.equals("54")){
						montoContratoMin=montoContratoMinEU;
						montoContratoMax=montoContratoMaxEU;
					}
					varBind.add(new BigDecimal(montoContratoMin));
					varBind.add(new BigDecimal(montoContratoMax));
				}
	
				log.debug("..:: strSQL: "+strSQL.toString());
				log.debug("..:: varBind: "+varBind);
				
				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				pst.executeUpdate();
				pst.close();
			} else {
				strSQL = new StringBuffer();
				strSQL.append(" INSERT INTO cdertmp_monto_x_solic (");
				strSQL.append(" ic_proceso,");
				strSQL.append(" ic_solicitud,");
				strSQL.append(" ic_moneda,");
				strSQL.append(" fn_monto_moneda,");
				strSQL.append(" fn_monto_moneda_min,");
				strSQL.append(" fn_monto_moneda_max");
				strSQL.append(" ) VALUES (");
				strSQL.append(" ?, ?, ?,");
				if (csTipoMonto.equals("D")) {
					strSQL.append(" ?, null, null");
				} else {
					strSQL.append(" null, ?, ?");
				}
				strSQL.append(")");
				
				if (csTipoMonto.equals("D")) {
					for (int i= 0; i < montoMoneda.size(); i++) {
						if (montoMoneda.get(i) != null && !"".equals(montoMoneda.get(i)) && (Integer.parseInt(montoMoneda.get(i).toString()) > 0)) {
							varBind = new ArrayList();
							varBind.add(numeroProceso);
							varBind.add(numeroSolicitud);
							varBind.add(new Integer((String)claveMonedaHid.get(i)));
							varBind.add(new BigDecimal(montoMoneda.get(i).toString().replaceAll(",", "")));
				
							log.debug("..:: strSQL: "+strSQL.toString());
							log.debug("..:: varBind: "+varBind);
							
							pst = con.queryPrecompilado(strSQL.toString(), varBind);
							pst.executeUpdate();
							pst.close();
						}
					}
				}else{
					
					pst = con.queryPrecompilado(strSQL.toString());
					log.debug("..:: strSQL: "+strSQL.toString());
					if(!montoContratoMin.equals("")&&!montoContratoMax.equals("")){
							pst.clearParameters();
							pst.setLong(1,Long.parseLong(numeroProceso));
							pst.setLong(2,Long.parseLong(numeroSolicitud));
							pst.setInt(3,1);//Moneda Nacional
							pst.setBigDecimal(4,new BigDecimal(montoContratoMin.toString().replaceAll(",", "")));
							pst.setBigDecimal(5,new BigDecimal(montoContratoMax.toString().replaceAll(",", "")));
							pst.executeUpdate();
							
					}
					if(!montoContratoMinDL.equals("")&&!montoContratoMaxDL.equals("")){
							pst.clearParameters();
							pst.setLong(1,Long.parseLong(numeroProceso));
							pst.setLong(2,Long.parseLong(numeroSolicitud));
							pst.setInt(3,54);//Dolar
							pst.setBigDecimal(4,new BigDecimal(montoContratoMinDL.toString().replaceAll(",", "")));
							pst.setBigDecimal(5,new BigDecimal(montoContratoMaxDL.toString().replaceAll(",", "")));
							pst.executeUpdate();
							
					}
					
					if(!montoContratoMinEU.equals("")&&!montoContratoMaxEU.equals("")){
							pst.clearParameters();
							pst.setLong(1,Long.parseLong(numeroProceso));
							pst.setLong(2,Long.parseLong(numeroSolicitud));
							pst.setInt(3,25);//Euro
							pst.setBigDecimal(4,new BigDecimal(montoContratoMinEU.toString().replaceAll(",", "")));
							pst.setBigDecimal(5,new BigDecimal(montoContratoMaxEU.toString().replaceAll(",", "")));
							pst.executeUpdate();
							
					}
					pst.close();
				
				}
			}
		} catch (Exception e) {
			transactionOk = false;
			log.error("insertarSolicitudConsentimientoTemp(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al insertar el preacuse de la solicitud de consentimiento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("insertarSolicitudConsentimientoTemp(S) ::..");
		}
		return numeroProceso;
	}		

	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de preacuse de 
	 * captura de solicitudes de consentimiento para la pyme.
	 * @param numeroProceso Numero de proceso de captura de la solicitud en la tabla temporal.
	 * @return solicitudConsentimientoTemp Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultarSolicitudConsentimientoTemp(String numeroProceso) throws AppException{
		log.info("consultarSolicitudConsentimientoTemp(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap solicitudConsentimientoTemp = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
			strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
			strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
			strSQL.append(" sol.cc_acuse1 AS acuse1,");
			strSQL.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato, ");
			strSQL.append(" NVL(sol.CG_EMPRESAS_REPRESENTADAS,'') as empresas ");//CORRECCION()FODEA-024-2014			
			strSQL.append(" FROM cdertmp_solicitud sol");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
			strSQL.append(", cdercat_tipo_plazo stp");
			strSQL.append(", cdercat_ventanilla_pago cvp");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_epo = cvp.ic_epo");
			strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago");
			strSQL.append(" AND sol.ic_proceso = ?");
			
			varBind.add(numeroProceso);
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){

				StringBuffer montosPorMoneda = new StringBuffer();
				strSQL = new StringBuffer();
				varBind = new ArrayList();
				strSQL.append(" SELECT mxs.fn_monto_moneda,");
				strSQL.append(" mxs.fn_monto_moneda_min,");
				strSQL.append(" mxs.fn_monto_moneda_max,");
				strSQL.append(" mon.cd_nombre");
				strSQL.append(" FROM cdertmp_monto_x_solic mxs");
				strSQL.append(", comcat_moneda mon");
				strSQL.append(" WHERE mxs.ic_moneda = mon.ic_moneda");
				strSQL.append(" AND mxs.ic_proceso = ?");
				
				varBind.add(numeroProceso);
				
				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind.toString());

				PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				
				ResultSet rst1 = pst1.executeQuery();
				
				while (rst1.next()) {
					String fnMontoMoneda = rst1.getString("fn_monto_moneda")==null?"":rst1.getString("fn_monto_moneda");
					String fnMontoMonedaMin = rst1.getString("fn_monto_moneda_min")==null?"":rst1.getString("fn_monto_moneda_min");
					String fnMontoMonedaMax = rst1.getString("fn_monto_moneda_max")==null?"":rst1.getString("fn_monto_moneda_max");
					if (!fnMontoMoneda.equals("")) {
						montosPorMoneda.append(Comunes.formatoDecimal(fnMontoMoneda, 2) + " " + rst1.getString("cd_nombre") + "<br/>");
					} else {
						montosPorMoneda.append(Comunes.formatoDecimal(fnMontoMonedaMin, 2) + " - " + Comunes.formatoDecimal(fnMontoMonedaMax, 2) + " " + rst1.getString("cd_nombre") + "<br/>");
					}
				}
				
				rst1.close();
				pst1.close();

				solicitudConsentimientoTemp.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				solicitudConsentimientoTemp.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				solicitudConsentimientoTemp.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				solicitudConsentimientoTemp.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				solicitudConsentimientoTemp.put("nombre_pyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				solicitudConsentimientoTemp.put("nombre_epo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				solicitudConsentimientoTemp.put("nombre_if", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
				solicitudConsentimientoTemp.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				solicitudConsentimientoTemp.put("montosPorMoneda", montosPorMoneda.toString());
				solicitudConsentimientoTemp.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				solicitudConsentimientoTemp.put("fecha_inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				solicitudConsentimientoTemp.put("fecha_fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				solicitudConsentimientoTemp.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				solicitudConsentimientoTemp.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				solicitudConsentimientoTemp.put("campo_adicional_1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				solicitudConsentimientoTemp.put("campo_adicional_2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				solicitudConsentimientoTemp.put("campo_adicional_3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				solicitudConsentimientoTemp.put("campo_adicional_4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				solicitudConsentimientoTemp.put("campo_adicional_5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				solicitudConsentimientoTemp.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				solicitudConsentimientoTemp.put("clave_estatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				solicitudConsentimientoTemp.put("estatus_solicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				solicitudConsentimientoTemp.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
				solicitudConsentimientoTemp.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
				solicitudConsentimientoTemp.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));
				solicitudConsentimientoTemp.put("acuse1", rst.getString("acuse1")==null?"":rst.getString("acuse1"));
				solicitudConsentimientoTemp.put("firma_contrato", rst.getString("firma_contrato")==null?"":rst.getString("firma_contrato"));
				solicitudConsentimientoTemp.put("empresas", rst.getString("empresas")==null?"":rst.getString("empresas")); //FODEA-024-2014 MOD()
				indice++;
			}
			
			solicitudConsentimientoTemp.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		} catch(Exception e) {
			log.error("consultarSolicitudConsentimientoTemp(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al consultar en la base de datos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultarSolicitudConsentimientoTemp(S) ::..");
		}
		return solicitudConsentimientoTemp;
	}

	/**
	 * M�todo que inserta una nueva solicitud de consentimiento de la pyme.
	 * @param datosPreacuseSolicitud HashMap con la informaci�n con la que se va a capturar la solicitud de consentimiento.
	 * @return numeroSolicitud numero de la nueva solicitud capturada.
	 * @throws AppException Cuando ocurre un error al insertar el registro.
	 * @author Alberto Cruz Flores.
	 */
	public String insertarSolicitudConsentimiento(HashMap datosPreacuseSolicitud) throws AppException {
		log.info("insertarSolicitudConsentimiento(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		String numeroSolicitud = "";
		String numeroAcuse = "";
		String clavePyme = "";
		String claveGrupo = "";

		try {
			con.conexionDB();
			
			String numeroProceso = datosPreacuseSolicitud.get("numeroProceso")==null?"":(String)datosPreacuseSolicitud.get("numeroProceso");
			String loginUsuario = datosPreacuseSolicitud.get("loginUsuario")==null?"":(String)datosPreacuseSolicitud.get("loginUsuario");
			String nombreUsuario = datosPreacuseSolicitud.get("nombreUsuario")==null?"":(String)datosPreacuseSolicitud.get("nombreUsuario");

			strSQL.append(" SELECT ic_solicitud AS numero_solicitud,");
			strSQL.append(" cc_acuse1 AS numero_acuse, ic_pyme AS clave_pyme, ic_grupo_cesion AS clave_grupo ");
			strSQL.append(" FROM cdertmp_solicitud");
			strSQL.append(" WHERE ic_proceso = ?");
			
			varBind.add(new Integer(numeroProceso));
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while (rst.next()) {
				numeroSolicitud = rst.getString("numero_solicitud")==null?"1":rst.getString("numero_solicitud");
				numeroAcuse = rst.getString("numero_acuse")==null?"1":rst.getString("numero_acuse");
				clavePyme = rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme");
				claveGrupo = rst.getString("clave_grupo")==null?"":rst.getString("clave_grupo");
			}
			
			rst.close();
			pst.close();
			
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" INSERT INTO cder_solicitud (");
			strSQL.append(" ic_solicitud,");
			strSQL.append(" cc_acuse1,");
			strSQL.append(" ic_pyme,");
			strSQL.append(" ic_epo,");
			strSQL.append(" ic_if,");
			strSQL.append(" ic_clasificacion_epo,");
			strSQL.append(" cg_no_contrato,");
			strSQL.append(" cs_multimoneda,");
			strSQL.append(" ic_tipo_contratacion,");
			strSQL.append(" df_fecha_vigencia_Ini,");
			strSQL.append(" df_fecha_vigencia_Fin,");
			strSQL.append(" ig_plazo,");
			strSQL.append(" ic_tipo_plazo,");
			strSQL.append(" cg_campo1,");
			strSQL.append(" cg_campo2,");
			strSQL.append(" cg_campo3,");
			strSQL.append(" cg_campo4,");
			strSQL.append(" cg_campo5,");
			strSQL.append(" cg_obj_contrato,");
			strSQL.append(" cg_comentarios,");
			strSQL.append(" cg_empresas_representadas,");
			strSQL.append(" ic_estatus_solic,");
			strSQL.append(" ic_ventanilla_pago,");
			strSQL.append(" cg_sup_adm_resob,");
			strSQL.append(" cg_numero_telefono,");
			strSQL.append(" df_alta_solicitud,");
			strSQL.append(" bi_documento, df_firma_contrato, ");
			strSQL.append(" ic_usuario, ic_grupo_cesion ");
			strSQL.append(" ) SELECT ic_solicitud,");
			strSQL.append(" cc_acuse1,");
			strSQL.append(" ic_pyme,");
			strSQL.append(" ic_epo,");
			strSQL.append(" cder.ic_if,");
			strSQL.append(" ic_clasificacion_epo,");
			strSQL.append(" cg_no_contrato,");
			strSQL.append(" cs_multimoneda,");
			strSQL.append(" ic_tipo_contratacion,");
			strSQL.append(" df_fecha_vigencia_Ini,");
			strSQL.append(" df_fecha_vigencia_Fin,");
			strSQL.append(" ig_plazo,");
			strSQL.append(" ic_tipo_plazo,");
			strSQL.append(" cg_campo1,");
			strSQL.append(" cg_campo2,");
			strSQL.append(" cg_campo3,");
			strSQL.append(" cg_campo4,");
			strSQL.append(" cg_campo5,");
			strSQL.append(" cg_obj_contrato,");
			strSQL.append(" cg_comentarios,");
			strSQL.append(" cg_empresas_representadas,");
			strSQL.append(" ic_estatus_solic,");
			strSQL.append(" ic_ventanilla_pago,");
			strSQL.append(" cg_sup_adm_resob,");
			strSQL.append(" cg_numero_telefono,");
			strSQL.append(" SYSDATE,");
			strSQL.append(" bi_documento, df_firma_contrato, ");
			strSQL.append("'"+loginUsuario+"', ic_grupo_cesion ");
			strSQL.append(" FROM cdertmp_solicitud cder");
			strSQL.append(" WHERE ic_proceso = ? ");
			
			varBind.add(numeroProceso);

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();
			
			strSQL.append(" SELECT mxs.ic_solicitud,");
			strSQL.append(" mxs.ic_moneda,");
			strSQL.append(" mxs.fn_monto_moneda,");
			strSQL.append(" mxs.fn_monto_moneda_min,");
			strSQL.append(" mxs.fn_monto_moneda_max");
			strSQL.append(" FROM cdertmp_monto_x_solic mxs");
			strSQL.append(" WHERE mxs.ic_proceso = ?");
			
			varBind.add(numeroProceso);
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while (rst.next()) {
				String icMontoSolic = "";
				String icSolicitud = rst.getString("ic_solicitud")==null?"":rst.getString("ic_solicitud");
				String icMoneda = rst.getString("ic_moneda")==null?"":rst.getString("ic_moneda");
				String fnMontoMoneda = rst.getString("fn_monto_moneda")==null?"":rst.getString("fn_monto_moneda");
				String fnMontoMonedaMin = rst.getString("fn_monto_moneda_min")==null?"":rst.getString("fn_monto_moneda_min");
				String fnMontoMonedaMax = rst.getString("fn_monto_moneda_max")==null?"":rst.getString("fn_monto_moneda_max");

				strSQL = new StringBuffer();
				varBind = new ArrayList();
				
				strSQL.append(" SELECT seq_cder_monto_x_solic.NEXTVAL AS icMontoSolic FROM dual");
				
				log.debug("..:: strSQL : "+strSQL.toString());
				
				PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString());
				
				ResultSet rst1 = pst1.executeQuery();
				
				while (rst1.next()) {
					icMontoSolic = rst1.getString("icMontoSolic")==null?"":rst1.getString("icMontoSolic");
				}
				
				rst1.close();
				pst1.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" INSERT INTO cder_monto_x_solic (");
				strSQL.append(" ic_monto_x_solic,");
				strSQL.append(" ic_solicitud,");
				strSQL.append(" ic_moneda,");
				strSQL.append(" fn_monto_moneda,");
				strSQL.append(" fn_monto_moneda_min,");
				strSQL.append(" fn_monto_moneda_max");
				strSQL.append(" ) VALUES (?, ?, ?,");

				varBind.add(icMontoSolic);
				varBind.add(numeroSolicitud);
				varBind.add(new Integer(icMoneda));

				if (!fnMontoMoneda.equals("")) {
					strSQL.append(" ?,");
					varBind.add(new BigDecimal(fnMontoMoneda));
				} else {
					strSQL.append(" null,");
				}
				if (!fnMontoMonedaMin.equals("") ) {
					strSQL.append(" ?,");
					varBind.add(new BigDecimal(fnMontoMonedaMin));
				} else {
					strSQL.append(" null,");
				}
				if (!fnMontoMonedaMax.equals("")) {
					strSQL.append(" ?");
					varBind.add(new BigDecimal(fnMontoMonedaMax));
				} else {
					strSQL.append(" null");
				}
				strSQL.append(")");
				
				log.debug("..:: strSQL: "+strSQL.toString());
				log.debug("..:: varBind: "+varBind);			
				
				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				pst1.executeUpdate();
				pst1.close();
			}
			
			rst.close();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario)");
			strSQL.append(" VALUES (?, ?, ?)");

			varBind.add(numeroAcuse); 
			varBind.add(loginUsuario);
			varBind.add(nombreUsuario);

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			//INSERCION RELACION REPRESENTANTES DEL GRUPO
			strSQL = new StringBuffer();

			varBind = new ArrayList();
			strSQL.append("INSERT INTO cder_solic_x_representante ");
			strSQL.append("            (ic_solicitud, ic_pyme, ic_usuario ");
			strSQL.append("            ) ");
			strSQL.append("     VALUES (?, ?, ? ");
			strSQL.append("            ) ");

			UtilUsr utilUsr = new UtilUsr();
			String loginUsrPyme = "";

			if(!"".equals(claveGrupo)){
				List lstEmpresas = getEmpresasXGrupoCder(claveGrupo);
				for(int x=0; x<lstEmpresas.size(); x++){
					clavePyme = (String)((HashMap)lstEmpresas.get(x)).get("ic_pyme");
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");
					for (int i = 0; i < usuariosPorPerfil.size(); i++) {
						loginUsrPyme = (String)usuariosPorPerfil.get(i);
						varBind = new ArrayList();
						varBind.add(numeroSolicitud);
						varBind.add(clavePyme);
						varBind.add(loginUsrPyme);

						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
					}
				}

			}else{

				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {
					loginUsrPyme = (String)usuariosPorPerfil.get(i);
					varBind = new ArrayList();
					varBind.add(numeroSolicitud);
					varBind.add(clavePyme);
					varBind.add(loginUsrPyme);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}

			}

		} catch (Exception e) {
			transactionOk = false;
			log.error("insertarSolicitudConsentimiento(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al insertar el preacuse de la solicitud de consentimiento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("insertarSolicitudConsentimiento(S) ::..");
		}
		return numeroSolicitud;
	}
	

	public HashMap consultarSolicitudConsentimiento(String numeroSolicitud) throws AppException{
		return consultarSolicitudConsentimiento(numeroSolicitud, "");
	}

	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de acuse de 
	 * captura de solicitudes de consentimiento para la pyme.
	 * @param numeroSolicitud Numero de solicitud de consentimiento de la que se obtiene la informaci�n.
	 * @return solicitudConsentimiento Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultarSolicitudConsentimiento(String numeroSolicitud, String icUsuario) throws AppException{
		log.info("consultarSolicitudConsentimiento(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap solicitudConsentimiento = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" sol.ic_clasificacion_epo AS clave_clasificacion_epo,");
			strSQL.append(" sol.ic_tipo_plazo AS clave_plazo_contrato,");
			strSQL.append(" sol.ic_tipo_contratacion AS clave_tipo_contratacion,");
			strSQL.append(" sol.ic_ventanilla_pago AS clave_ventanilla_pago,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" sol.cg_empresas_representadas AS empresas,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
			strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
			strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
			strSQL.append(" sol.cc_acuse1 AS acuse1,");
			strSQL.append(" sol.cc_acuse_envio_rep1 AS acuse_envio_rep1,");
			strSQL.append(" sol.cc_acuse_envio_rep2 AS acuse_envio_rep2,");
			strSQL.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato,");
			strSQL.append(" sol.ic_grupo_cesion AS clave_grupo_cesion");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
			strSQL.append(", cdercat_tipo_plazo stp");
			strSQL.append(", cdercat_ventanilla_pago cvp");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			strSQL.append(" AND sol.ic_solicitud = ?");
			
			varBind.add(numeroSolicitud);
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while (rst.next()) {
				StringBuffer montosPorMoneda = new StringBuffer();
				strSQL = new StringBuffer();
				varBind = new ArrayList();
				strSQL.append(" SELECT mxs.fn_monto_moneda,");
				strSQL.append(" mxs.fn_monto_moneda_min,");
				strSQL.append(" mxs.fn_monto_moneda_max,");
				strSQL.append(" mon.cd_nombre");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(", comcat_moneda mon");
				strSQL.append(" WHERE mxs.ic_moneda = mon.ic_moneda");
				strSQL.append(" AND mxs.ic_solicitud = ?");
				
				varBind.add(numeroSolicitud);
				
				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind.toString());

				PreparedStatement pst2 = con.queryPrecompilado(strSQL.toString(), varBind);
				
				ResultSet rst2 = pst2.executeQuery();
				
				while (rst2.next()) {
					String fnMontoMoneda = rst2.getString("fn_monto_moneda")==null?"":rst2.getString("fn_monto_moneda");
					String fnMontoMonedaMin = rst2.getString("fn_monto_moneda_min")==null?"":rst2.getString("fn_monto_moneda_min");
					String fnMontoMonedaMax = rst2.getString("fn_monto_moneda_max")==null?"":rst2.getString("fn_monto_moneda_max");
					if (!fnMontoMoneda.equals("")) {
						montosPorMoneda.append(Comunes.formatoDecimal(fnMontoMoneda, 2) + " " + rst2.getString("cd_nombre") + "<br/>");
					} else {
						montosPorMoneda.append(Comunes.formatoDecimal(fnMontoMonedaMin, 2) + " - " + Comunes.formatoDecimal(fnMontoMonedaMax, 2) + " " + rst2.getString("cd_nombre") + "<br/>");
					}
				}
				
				rst2.close();
				pst2.close();

				solicitudConsentimiento.put("clave_solicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				solicitudConsentimiento.put("clave_pyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				solicitudConsentimiento.put("clave_epo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				solicitudConsentimiento.put("clave_if", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				solicitudConsentimiento.put("claveClasificacionEpo", rst.getString("clave_clasificacion_epo")==null?"":rst.getString("clave_clasificacion_epo"));
				solicitudConsentimiento.put("clavePlazoContrato", rst.getString("clave_plazo_contrato")==null?"":rst.getString("clave_plazo_contrato"));
				solicitudConsentimiento.put("claveTipoContratacion", rst.getString("clave_tipo_contratacion")==null?"":rst.getString("clave_tipo_contratacion"));
				solicitudConsentimiento.put("claveVentanillaPago", rst.getString("clave_ventanilla_pago")==null?"":rst.getString("clave_ventanilla_pago"));
				solicitudConsentimiento.put("clave_estatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				solicitudConsentimiento.put("nombre_pyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				solicitudConsentimiento.put("nombre_epo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				solicitudConsentimiento.put("nombre_if", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
				solicitudConsentimiento.put("numero_contrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				solicitudConsentimiento.put("montosPorMoneda", montosPorMoneda.toString());
				solicitudConsentimiento.put("tipo_contratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				solicitudConsentimiento.put("fecha_inicio_contrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				solicitudConsentimiento.put("fecha_fin_contrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				solicitudConsentimiento.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				solicitudConsentimiento.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				solicitudConsentimiento.put("campo_adicional_1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				solicitudConsentimiento.put("campo_adicional_2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				solicitudConsentimiento.put("campo_adicional_3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				solicitudConsentimiento.put("campo_adicional_4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				solicitudConsentimiento.put("campo_adicional_5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				solicitudConsentimiento.put("objeto_contrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				solicitudConsentimiento.put("comentarios", rst.getString("comentarios")==null?"":rst.getString("comentarios"));
				solicitudConsentimiento.put("empresas", rst.getString("empresas")==null?"":rst.getString("empresas"));
				
				solicitudConsentimiento.put("estatus_solicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				solicitudConsentimiento.put("venanillaPago", rst.getString("venanilla_pago")==null?"":rst.getString("venanilla_pago"));
				solicitudConsentimiento.put("supAdmResob", rst.getString("sup_adm_resob")==null?"":rst.getString("sup_adm_resob"));
				solicitudConsentimiento.put("numeroTelefono", rst.getString("numero_telefono")==null?"":rst.getString("numero_telefono"));
				solicitudConsentimiento.put("acuse1", rst.getString("acuse1")==null?"":rst.getString("acuse1"));
				solicitudConsentimiento.put("firma_contrato", rst.getString("firma_contrato")==null?"":rst.getString("firma_contrato"));
				solicitudConsentimiento.put("clave_grupo_cesion", rst.getString("clave_grupo_cesion")==null?"":rst.getString("clave_grupo_cesion"));
				
				String numeroAcuse = rst.getString("acuse1")==null?"":rst.getString("acuse1");
				
				if (!numeroAcuse.equals("")) {
					strSQL = new StringBuffer();
					varBind = new ArrayList();
	
					strSQL.append(" SELECT cg_usuario || ' - ' || cg_nombre_usuario usuario_carga,");
					strSQL.append(" TO_CHAR(df_operacion, 'dd/mm/yyyy') fecha_carga,");
					strSQL.append(" TO_CHAR(df_operacion, 'HH:MI AM') hora_carga");
					strSQL.append(" FROM cder_acuse");
					strSQL.append(" WHERE cc_acuse = ?");
					
					varBind.add(new Integer(numeroAcuse));
					
					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind.toString());
					
					PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ResultSet rst1 = pst1.executeQuery();
					
					while (rst1.next()) {
						solicitudConsentimiento.put("usuarioCarga", rst1.getString("usuario_carga")==null?"":rst1.getString("usuario_carga"));
						solicitudConsentimiento.put("fechaCarga", rst1.getString("fecha_carga")==null?"":rst1.getString("fecha_carga"));
						solicitudConsentimiento.put("horaCarga", rst1.getString("hora_carga")==null?"":rst1.getString("hora_carga"));
					}
	
					rst1.close();
					pst1.close();
				}
				
				if(!"".equals(icUsuario)){
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append("SELECT ca.cg_usuario || ' - ' || ca.cg_nombre_usuario usuario_carga, ");
					strSQL.append("       TO_CHAR (ca.df_operacion, 'dd/mm/yyyy') fecha_carga, ");
					strSQL.append("       TO_CHAR (ca.df_operacion, 'HH:MI AM') hora_carga, csr.cc_acuse_envio_rep ");
					strSQL.append("  FROM cder_solic_x_representante csr, cder_acuse ca ");
					strSQL.append(" WHERE csr.cc_acuse_envio_rep = ca.cc_acuse ");
					strSQL.append("   AND csr.ic_solicitud = ? ");
					strSQL.append("   AND csr.ic_usuario = ? ");

					varBind.add(new Integer(numeroSolicitud));
					varBind.add(icUsuario);

					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind.toString());

					PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ResultSet rst1 = pst1.executeQuery();

					while (rst1.next()) {
						solicitudConsentimiento.put("acuseEnvioRep", rst1.getString("cc_acuse_envio_rep")==null?"":rst1.getString("cc_acuse_envio_rep"));
						solicitudConsentimiento.put("usuarioEnvRep", rst1.getString("usuario_carga")==null?"":rst1.getString("usuario_carga"));
						solicitudConsentimiento.put("fechaEnvRep", rst1.getString("fecha_carga")==null?"":rst1.getString("fecha_carga"));
						solicitudConsentimiento.put("horaEnvRep", rst1.getString("hora_carga")==null?"":rst1.getString("hora_carga"));
					}

					rst1.close();
					pst1.close();
				}
				/*
				numeroAcuse = rst.getString("acuse_envio_rep2")==null?"":rst.getString("acuse_envio_rep2");
				
				if (!numeroAcuse.equals("")) {
					solicitudConsentimiento.put("acuseEnvioRep2", rst.getString("acuse_envio_rep2")==null?"":rst.getString("acuse_envio_rep2"));

					strSQL = new StringBuffer();
					varBind = new ArrayList();
	
					strSQL.append(" SELECT cg_usuario || ' - ' || cg_nombre_usuario usuario_carga,");
					strSQL.append(" TO_CHAR(df_operacion, 'dd/mm/yyyy') fecha_carga,");
					strSQL.append(" TO_CHAR(df_operacion, 'HH:MI AM') hora_carga");
					strSQL.append(" FROM cder_acuse");
					strSQL.append(" WHERE cc_acuse = ?");
					
					varBind.add(new Integer(numeroAcuse));
					
					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind.toString());
					
					PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ResultSet rst1 = pst1.executeQuery();
					
					while (rst1.next()) {
						solicitudConsentimiento.put("usuarioEnvRep2", rst1.getString("usuario_carga")==null?"":rst1.getString("usuario_carga"));
						solicitudConsentimiento.put("fechaEnvRep2", rst1.getString("fecha_carga")==null?"":rst1.getString("fecha_carga"));
						solicitudConsentimiento.put("horaEnvRep2", rst1.getString("hora_carga")==null?"":rst1.getString("hora_carga"));
					}
	
					rst1.close();
					pst1.close();
				} else {
					solicitudConsentimiento.put("acuseEnvioRep1", rst.getString("acuse_envio_rep1")==null?"":rst.getString("acuse_envio_rep1"));
					numeroAcuse = rst.getString("acuse_envio_rep1")==null?"":rst.getString("acuse_envio_rep1");
					
					if (!numeroAcuse.equals("")) {
						strSQL = new StringBuffer();
						varBind = new ArrayList();
		
						strSQL.append(" SELECT cg_usuario || ' - ' || cg_nombre_usuario usuario_carga,");
						strSQL.append(" TO_CHAR(df_operacion, 'dd/mm/yyyy') fecha_carga,");
						strSQL.append(" TO_CHAR(df_operacion, 'HH:MI AM') hora_carga");
						strSQL.append(" FROM cder_acuse");
						strSQL.append(" WHERE cc_acuse = ?");
						
						varBind.add(new Integer(numeroAcuse));
						
						log.debug("..:: strSQL : "+strSQL.toString());
						log.debug("..:: varBind : "+varBind.toString());
						
						PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
						ResultSet rst1 = pst1.executeQuery();
						
						while (rst1.next()) {
							solicitudConsentimiento.put("usuarioEnvRep1", rst1.getString("usuario_carga")==null?"":rst1.getString("usuario_carga"));
							solicitudConsentimiento.put("fechaEnvRep1", rst1.getString("fecha_carga")==null?"":rst1.getString("fecha_carga"));
							solicitudConsentimiento.put("horaEnvRep1", rst1.getString("hora_carga")==null?"":rst1.getString("hora_carga"));
						}
		
						rst1.close();
						pst1.close();
					}
				}
				*/
				indice++;
			}
			
			solicitudConsentimiento.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		} catch(Exception e) {
			log.error("consultarSolicitudConsentimiento(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al consultar en la base de datos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultarSolicitudConsentimiento(S) ::..");
		}
		return solicitudConsentimiento;
	}

	/**
	 * M�todo que actualiza una solicitud de consentimiento.
	 * @param datosCapturaSolicitud HashMap con la informaci�n con la que se va a actualizar la solicitud de consentimiento.
	 * @return numeroProceso numero del proceso de carga de la nueva solicitud.
	 * @throws AppException Cuando ocurre un error al actualizar el registro.
	 * @author Alberto Cruz Flores.
	 */
	public void modificarSolicitudConsentimiento(HashMap datosCapturaSolicitud) throws AppException {
		log.info("modificarSolicitudConsentimiento(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		
		try {
			con.conexionDB();
			
			String numeroSolicitud = datosCapturaSolicitud.get("numeroSolicitud")==null?"":(String)datosCapturaSolicitud.get("numeroSolicitud");
			String claveEpo = datosCapturaSolicitud.get("claveEpo")==null?"":(String)datosCapturaSolicitud.get("claveEpo");
			String claveIfCesionario = datosCapturaSolicitud.get("claveIfCesionario")==null?"":(String)datosCapturaSolicitud.get("claveIfCesionario");
			String claveClasificacionEpo = datosCapturaSolicitud.get("claveClasificacionEpo")==null?"":(String)datosCapturaSolicitud.get("claveClasificacionEpo");
			String numeroContrato = datosCapturaSolicitud.get("numeroContrato")==null?"":(String)datosCapturaSolicitud.get("numeroContrato");
			String montoContrato = datosCapturaSolicitud.get("montoContrato")==null?"":(String)datosCapturaSolicitud.get("montoContrato");
			String claveMoneda = datosCapturaSolicitud.get("claveMoneda")==null?"":(String)datosCapturaSolicitud.get("claveMoneda");
			String claveTipoContratacion = datosCapturaSolicitud.get("claveTipoContratacion")==null?"":(String)datosCapturaSolicitud.get("claveTipoContratacion");
			String fechaVigenciaContratoIni = datosCapturaSolicitud.get("fechaVigenciaContratoIni")==null?"":(String)datosCapturaSolicitud.get("fechaVigenciaContratoIni");
			String fechaVigenciaContratoFin = datosCapturaSolicitud.get("fechaVigenciaContratoFin")==null?"":(String)datosCapturaSolicitud.get("fechaVigenciaContratoFin");
			String objetoContrato = datosCapturaSolicitud.get("objetoContrato")==null?"":(String)datosCapturaSolicitud.get("objetoContrato");
			String comentariosSolicitud = datosCapturaSolicitud.get("comentariosSolicitud")==null?"":(String)datosCapturaSolicitud.get("comentariosSolicitud");
			String empresas = datosCapturaSolicitud.get("empresas")==null?"":(String)datosCapturaSolicitud.get("empresas");
			String csTipoMonto = datosCapturaSolicitud.get("csTipoMonto")==null?"":(String)datosCapturaSolicitud.get("csTipoMonto");
			String montoContratoMin = datosCapturaSolicitud.get("montoContratoMin")==null?"":(String)datosCapturaSolicitud.get("montoContratoMin");
			String montoContratoMax = datosCapturaSolicitud.get("montoContratoMax")==null?"":(String)datosCapturaSolicitud.get("montoContratoMax");
			String plazoContrato = datosCapturaSolicitud.get("plazoContrato")==null?"":(String)datosCapturaSolicitud.get("plazoContrato");
			String claveTipoPlazo = datosCapturaSolicitud.get("claveTipoPlazo")==null?"":(String)datosCapturaSolicitud.get("claveTipoPlazo");
			String claveVentanillaPago = datosCapturaSolicitud.get("claveVentanillaPago")==null?"":(String)datosCapturaSolicitud.get("claveVentanillaPago");
			String supervisorAdministradorResidente = datosCapturaSolicitud.get("supervisorAdministradorResidente")==null?"":(String)datosCapturaSolicitud.get("supervisorAdministradorResidente");
			String numeroTelefono = datosCapturaSolicitud.get("numeroTelefono")==null?"":(String)datosCapturaSolicitud.get("numeroTelefono");
			List claveMonedaHid = datosCapturaSolicitud.get("claveMonedaHid")==null?new ArrayList():(List)datosCapturaSolicitud.get("claveMonedaHid");
			List montoMoneda = datosCapturaSolicitud.get("montoMoneda")==null?new ArrayList():(List)datosCapturaSolicitud.get("montoMoneda");

			List camposAdicionales = datosCapturaSolicitud.get("camposAdicionales")==null?new ArrayList():(List)datosCapturaSolicitud.get("camposAdicionales");
			
			//Multimoneda
			String montoContratoMinDL = datosCapturaSolicitud.get("montoContratoMinDL")==null?"":(String)datosCapturaSolicitud.get("montoContratoMinDL");
			String montoContratoMaxDL = datosCapturaSolicitud.get("montoContratoMaxDL")==null?"":(String)datosCapturaSolicitud.get("montoContratoMaxDL");
			String montoContratoMinEU = datosCapturaSolicitud.get("montoContratoMinEU")==null?"":(String)datosCapturaSolicitud.get("montoContratoMinEU");
			String montoContratoMaxEU = datosCapturaSolicitud.get("montoContratoMaxEU")==null?"":(String)datosCapturaSolicitud.get("montoContratoMaxEU");
			//
			String firmaContrato = datosCapturaSolicitud.get("firmaContrato")==null?"":(String)datosCapturaSolicitud.get("firmaContrato");
			strSQL.append(" UPDATE cder_solicitud SET");
			if (!claveEpo.equals("")) {
				strSQL.append(" ic_epo = ?,");
				varBind.add(claveEpo);
			}
			if (!claveIfCesionario.equals("")) {
				strSQL.append(" ic_if = ?,");
				varBind.add(new Integer(claveIfCesionario));
			}
			if (!claveClasificacionEpo.equals("")) {
				strSQL.append(" ic_clasificacion_epo = ?,");
				varBind.add(new Integer(claveClasificacionEpo));
			}
			if (!claveVentanillaPago.equals("")) {
				strSQL.append(" ic_ventanilla_pago = ?,");
				varBind.add(new Integer(claveVentanillaPago));
			}
			if (!supervisorAdministradorResidente.equals("")) {
				strSQL.append(" cg_sup_adm_resob = ?,");
				varBind.add(supervisorAdministradorResidente);
			}			
			if (!numeroTelefono.equals("")) {
				strSQL.append(" cg_numero_telefono = ?,");
				varBind.add(numeroTelefono);
			}
			if (!numeroContrato.equals("")) {
				strSQL.append(" cg_no_contrato = ?,");
				varBind.add(numeroContrato);
			}

			if (!claveTipoContratacion.equals("")) {
				strSQL.append(" ic_tipo_contratacion = ?,");
				varBind.add(new Integer(claveTipoContratacion));
			}

			if (!claveMoneda.equals("0")) {
				strSQL.append(" cs_multimoneda = ?,");
				varBind.add("N");
			} else {
				strSQL.append(" cs_multimoneda = ?,");
				varBind.add("S");
			}
			if (!claveTipoPlazo.equals("")) {
				strSQL.append(" ic_tipo_plazo = ?,");
				strSQL.append(" ig_plazo = ?,");
				varBind.add(claveTipoPlazo);
				varBind.add(plazoContrato);
			} else {
				if (!fechaVigenciaContratoIni.equals("")) {
					strSQL.append(" df_fecha_vigencia_ini = TO_DATE(?, 'dd/mm/yyyy'),");
					varBind.add(fechaVigenciaContratoIni);
				}
				if (!fechaVigenciaContratoFin.equals("")) {
					strSQL.append(" df_fecha_vigencia_fin = TO_DATE(?, 'dd/mm/yyyy'),");
					varBind.add(fechaVigenciaContratoFin);
				}
			}

			if (camposAdicionales != null && !camposAdicionales.isEmpty()) {
				for (int i = 0; i < camposAdicionales.size(); i++) {
					strSQL.append(" cg_campo" + (i + 1) + " = ?,");
					varBind.add((String)camposAdicionales.get(i));
				}
			}
			if (!objetoContrato.equals("")) {
				strSQL.append(" cg_obj_contrato = ?,");
				varBind.add(objetoContrato);
			}
			if (!comentariosSolicitud.equals("")) {
				strSQL.append(" cg_comentarios = ?,");
				varBind.add(comentariosSolicitud);
			}
			
			if (!empresas.equals("")) {
				strSQL.append(" cg_empresas_representadas = ?,");
				varBind.add(empresas);
			}
			
			if (!firmaContrato.equals("")) {
				strSQL.append(" df_firma_contrato = TO_DATE(?, 'dd/mm/yyyy'),");
				varBind.add(firmaContrato);
			}
			
			strSQL.delete(strSQL.lastIndexOf(","), strSQL.length());
			strSQL.append(" WHERE ic_solicitud = ?");
			varBind.add(numeroSolicitud);
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
			
			
			strSQL = new StringBuffer();
			varBind = new ArrayList();
			
			strSQL.append(" DELETE cder_monto_x_solic WHERE ic_solicitud = ?");
			
			varBind.add(numeroSolicitud);
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
			
			
			if (!claveMoneda.equals("0")) {
				strSQL = new StringBuffer();
				varBind = new ArrayList();
				
				strSQL.append(" SELECT seq_cder_monto_x_solic.NEXTVAL AS icMontoSolic FROM dual");
				
				log.debug("..:: strSQL : "+strSQL.toString());
				
				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();
				
				String icMontoSolic = "";
				
				while (rst.next()) {
					icMontoSolic = rst.getString("icMontoSolic")==null?"":rst.getString("icMontoSolic");
				}
				
				rst.close();
				pst.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" INSERT INTO cder_monto_x_solic (");
				strSQL.append(" ic_monto_x_solic,");
				strSQL.append(" ic_solicitud,");
				strSQL.append(" ic_moneda,");
				strSQL.append(" fn_monto_moneda,");
				strSQL.append(" fn_monto_moneda_min,");
				strSQL.append(" fn_monto_moneda_max");
				strSQL.append(" ) VALUES (");
				strSQL.append(" ?, ?, ?,");
				if (csTipoMonto.equals("D")) {
					strSQL.append(" ?, null, null");
				} else {
					strSQL.append(" null, ?, ?");
				}
				strSQL.append(")");
				
				varBind.add(icMontoSolic);
				varBind.add(numeroSolicitud);
				varBind.add(new Integer(claveMoneda));
				if (csTipoMonto.equals("D")) {
					varBind.add(new BigDecimal(montoContrato));
				} else {
					if(claveMoneda.equals("25")){
						montoContratoMin=montoContratoMinDL;
						montoContratoMax=montoContratoMaxDL;
					}else if(claveMoneda.equals("54")){
						montoContratoMin=montoContratoMinEU;
						montoContratoMax=montoContratoMaxEU;
					}
					varBind.add(new BigDecimal(montoContratoMin));
					varBind.add(new BigDecimal(montoContratoMax));
				}
	
				log.debug("..:: strSQL: "+strSQL.toString());
				log.debug("..:: varBind: "+varBind);
				
				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				pst.executeUpdate();
				pst.close();
			} else {
				strSQL = new StringBuffer();

				strSQL.append(" INSERT INTO cder_monto_x_solic (");
				strSQL.append(" ic_monto_x_solic,");
				strSQL.append(" ic_solicitud,");
				strSQL.append(" ic_moneda,");
				strSQL.append(" fn_monto_moneda,");
				strSQL.append(" fn_monto_moneda_min,");
				strSQL.append(" fn_monto_moneda_max");
				strSQL.append(" ) VALUES (");
				strSQL.append(" ?, ?, ?,");
				if (csTipoMonto.equals("D")) {
					strSQL.append(" ?, null, null");
				} else {
					strSQL.append(" null, ?, ?");
				}
				strSQL.append(")");
				
				if (csTipoMonto.equals("D")) {
					for (int i= 0; i < claveMonedaHid.size(); i++) {
						StringBuffer strSQLi = new StringBuffer();
						
						strSQLi.append(" SELECT seq_cder_monto_x_solic.NEXTVAL AS icMontoSolic FROM dual");
						
						log.debug("..:: strSQLi : "+strSQLi.toString());
						
						PreparedStatement psti = con.queryPrecompilado(strSQLi.toString());
						ResultSet rsti = psti.executeQuery();
						
						String icMontoSolic = "";
						
						while (rsti.next()) {
							icMontoSolic = rsti.getString("icMontoSolic")==null?"":rsti.getString("icMontoSolic");
						}
						
						rsti.close();
						psti.close();
					
						varBind = new ArrayList();
						varBind.add(icMontoSolic);
						varBind.add(numeroSolicitud);
						varBind.add(new Integer((String)claveMonedaHid.get(i)));
						varBind.add(new BigDecimal(montoMoneda.get(i).toString().replaceAll(",", "")));
			
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
					}
				}else{
					
					pst = con.queryPrecompilado(strSQL.toString());
					log.debug("..:: strSQL: "+strSQL.toString());
					if(!montoContratoMin.equals("")&&!montoContratoMax.equals("")){
							
						StringBuffer strSQLi = new StringBuffer();
						
						strSQLi.append(" SELECT seq_cder_monto_x_solic.NEXTVAL AS icMontoSolic FROM dual");
						
						log.debug("..:: strSQLi : "+strSQLi.toString());
						
						PreparedStatement psti = con.queryPrecompilado(strSQLi.toString());
						ResultSet rsti = psti.executeQuery();
						
						String icMontoSolic = "";
						
						while (rsti.next()) {
							icMontoSolic = rsti.getString("icMontoSolic")==null?"":rsti.getString("icMontoSolic");
						}
						
						rsti.close();
						psti.close();
					
						varBind = new ArrayList();
						varBind.add(icMontoSolic);
						varBind.add(numeroSolicitud);
						varBind.add(new Integer("1"));
						varBind.add(new BigDecimal(montoContratoMin.toString().replaceAll(",", "")));
						varBind.add(new BigDecimal(montoContratoMax.toString().replaceAll(",", "")));
			
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
					}
					if(!montoContratoMinDL.equals("")&&!montoContratoMaxDL.equals("")){
							
							
						StringBuffer strSQLi = new StringBuffer();
						
						strSQLi.append(" SELECT seq_cder_monto_x_solic.NEXTVAL AS icMontoSolic FROM dual");
						
						log.debug("..:: strSQLi : "+strSQLi.toString());
						
						PreparedStatement psti = con.queryPrecompilado(strSQLi.toString());
						ResultSet rsti = psti.executeQuery();
						
						String icMontoSolic = "";
						
						while (rsti.next()) {
							icMontoSolic = rsti.getString("icMontoSolic")==null?"":rsti.getString("icMontoSolic");
						}
						
						rsti.close();
						psti.close();
					
						varBind = new ArrayList();
						varBind.add(icMontoSolic);
						varBind.add(numeroSolicitud);
						varBind.add(new Integer("54"));
						varBind.add(new BigDecimal(montoContratoMinDL.toString().replaceAll(",", "")));
						varBind.add(new BigDecimal(montoContratoMaxDL.toString().replaceAll(",", "")));
			
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
					}
					
					if(!montoContratoMinEU.equals("")&&!montoContratoMaxEU.equals("")){
								
						StringBuffer strSQLi = new StringBuffer();
						
						strSQLi.append(" SELECT seq_cder_monto_x_solic.NEXTVAL AS icMontoSolic FROM dual");
						
						log.debug("..:: strSQLi : "+strSQLi.toString());
						
						PreparedStatement psti = con.queryPrecompilado(strSQLi.toString());
						ResultSet rsti = psti.executeQuery();
						
						String icMontoSolic = "";
						
						while (rsti.next()) {
							icMontoSolic = rsti.getString("icMontoSolic")==null?"":rsti.getString("icMontoSolic");
						}
						
						rsti.close();
						psti.close();
					
						varBind = new ArrayList();
						varBind.add(icMontoSolic);
						varBind.add(numeroSolicitud);
						varBind.add(new Integer("25"));
						varBind.add(new BigDecimal(montoContratoMinEU.toString().replaceAll(",", "")));
						varBind.add(new BigDecimal(montoContratoMaxEU.toString().replaceAll(",", "")));
			
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
					}
					
				
				}
				
				
				
			}
			
		} catch (Exception e) {
			transactionOk = false;
			log.error("modificarSolicitudConsentimiento(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al modificar la solicitud de consentimiento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("modificarSolicitudConsentimiento(S) ::..");
		}
	}

	/**
	 * M�todo que inserta el acuse de env�o de el(los) representante(s) legal(es) de la pyme
	 * en la solicitud de consentimiento.
	 * @param parametrosEnvio HashMap con la informaci�n con la que se va a enviar la solicitud de consentimiento.
	 * @return acuseEnvio n�mero de acuse de env�o de la solicitud.
	 * @throws AppException Cuando ocurre un error al actualizar el registro.
	 * @author Alberto Cruz Flores.
	 */
	public String enviaSolicitudConsentimiento(HashMap parametrosEnvio)	throws AppException	{
		log.info("enviaSolicitudConsentimiento");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		String acuseEnvio = "";
			
		String _serial = parametrosEnvio.get("_serial")==null?"":(String)parametrosEnvio.get("_serial");
		String textoFirmado = parametrosEnvio.get("textoFirmado")==null?"":(String)parametrosEnvio.get("textoFirmado");
		String pkcs7 = parametrosEnvio.get("pkcs7")==null?"":(String)parametrosEnvio.get("pkcs7");
		String folioCert = parametrosEnvio.get("folioCert")==null?"":(String)parametrosEnvio.get("folioCert");
		boolean validCert = Boolean.getBoolean((String)parametrosEnvio.get("validCert"));
		String numeroSolicitud = parametrosEnvio.get("numeroSolicitud")==null?"":(String)parametrosEnvio.get("numeroSolicitud");
		String nuevoEstatus = parametrosEnvio.get("nuevoEstatus")==null?"":(String)parametrosEnvio.get("nuevoEstatus");
		String loginUsuario = parametrosEnvio.get("loginUsuario")==null?"":(String)parametrosEnvio.get("loginUsuario");
		String nombreUsuario = parametrosEnvio.get("nombreUsuario")==null?"":(String)parametrosEnvio.get("nombreUsuario");
		String fechaCarga = parametrosEnvio.get("fechaCarga")==null?"":(String)parametrosEnvio.get("fechaCarga");
		String horaCarga = parametrosEnvio.get("horaCarga")==null?"":(String)parametrosEnvio.get("horaCarga");
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		
		try {
			con.conexionDB();
			
			char getReceipt = 'Y';
			if ( ! _serial.equals("") && !textoFirmado.equals("") && !pkcs7.equals("") ) {
				Seguridad s = new Seguridad();
				if ( s.autenticar(folioCert, _serial, pkcs7, textoFirmado, getReceipt) ) {
					acuseEnvio = s.getAcuse();

					String acuseRepresentante1 = "";
					
					/*
					strSQL = new StringBuffer();
					varBind = new ArrayList();
		
					strSQL.append(" SELECT cc_acuse_envio_rep1 AS acuse_rep1");
					strSQL.append(" FROM cder_solicitud");
					strSQL.append(" WHERE ic_solicitud = ?");
					
					varBind.add(numeroSolicitud);
		
					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind.toString());
					
					PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ResultSet rst1 = pst1.executeQuery();
					
					while (rst1.next()) {
						acuseRepresentante1 = rst1.getString("acuse_rep1")==null?"":rst1.getString("acuse_rep1");
					}
					
					rst1.close();
					pst1.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					if (!acuseRepresentante1.equals("")) {
						String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
						if (rs1.next()) {
							acuseRepresentante1 = rs1.getString(1)==null?"1":rs1.getString(1);
						}
						ps1.close();
						rs1.close();

						strSQL.append(" UPDATE cder_solicitud");
						strSQL.append(" SET cc_acuse_envio_rep2 = ?,");
						strSQL.append(" df_envio_rep2 = SYSDATE,");
						strSQL.append(" df_fecha_solicitud_ini = SYSDATE,");
						strSQL.append(" ic_estatus_solic = ?");
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(new Integer(acuseRepresentante1));
						varBind.add(new Integer(nuevoEstatus));
						varBind.add(numeroSolicitud);
					} else {
						String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
						if (rs1.next()) {
							acuseRepresentante1 = rs1.getString(1)==null?"1":rs1.getString(1);
						}
						ps1.close();
						rs1.close();
					
						strSQL.append(" UPDATE cder_solicitud");
						strSQL.append(" SET cc_acuse_envio_rep1 = ?,");
						strSQL.append(" df_envio_rep1 = SYSDATE,");
						strSQL.append(" df_fecha_solicitud_ini = SYSDATE,");
						strSQL.append(" ic_estatus_solic = ?");
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(new Integer(acuseRepresentante1));
						varBind.add(new Integer(nuevoEstatus));
						varBind.add(numeroSolicitud);
					}					*/

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
					ps1 = con.queryPrecompilado(qry);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						acuseRepresentante1 = rs1.getString(1)==null?"1":rs1.getString(1);
					}					
					ps1.close();
					rs1.close();

					strSQL.append(" UPDATE cder_solicitud");
					strSQL.append(" SET df_fecha_solicitud_ini = SYSDATE,");
					strSQL.append(" ic_estatus_solic = ?");
					strSQL.append(" WHERE ic_solicitud = ?");

					varBind.add(new Integer(nuevoEstatus));
					varBind.add(numeroSolicitud);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
					//-----

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" UPDATE CDER_SOLIC_X_REPRESENTANTE");
					strSQL.append(" SET CC_ACUSE_ENVIO_REP = ?, ");
					strSQL.append(" DF_ENVIO_REPRESENTANTE = SYSDATE ");
					strSQL.append(" WHERE IC_SOLICITUD = ? ");
					strSQL.append(" AND IC_USUARIO = ?");

					varBind.add(new Integer(acuseRepresentante1));
					varBind.add(numeroSolicitud);
					varBind.add(loginUsuario);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(acuseRepresentante1)); 
					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(acuseEnvio);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}	else {
					acuseEnvio ="|"+s.mostrarError();					
				}
			}	
			
		} catch (Exception e) {
			transactionOk = false;
			log.error("modificarSolicitudConsentimiento(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al modificar la solicitud de consentimiento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("modificarSolicitudConsentimiento(S) ::..");
		}
					
		return acuseEnvio;
	}

	/**
	 * M�todo que obtiene las solicitudes que han sido enviadas por un usuario de la pyme.
	 * @param parametrosConsulta Parametros que vienen de la pantalla de consulta.
	 * @return solicitudesEnviadasPorUsuario HashMap con las solicitudes que han sido enviadas por un usuario.
	 * @throws AppException Cuando ocurre un error al actualizar el registro.
	 * @author Alberto Cruz Flores.
	 */
	public HashMap obtieneSolicitudesEnviadasPorUsuario(HashMap parametrosConsulta)	throws AppException	{
		log.info("obtieneSolicitudesEnviadasPorUsuario(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap solicitudesEnviadasPorUsuario = new HashMap();
		
		try {
			con.conexionDB();
			
			String loginUsuario = parametrosConsulta.get("loginUsuario")==null?"":(String)parametrosConsulta.get("loginUsuario");
			String clavePyme = parametrosConsulta.get("clavePyme")==null?"":(String)parametrosConsulta.get("clavePyme");
			String claveEpo = parametrosConsulta.get("claveEpo")==null?"":(String)parametrosConsulta.get("claveEpo");
			String claveIfCesionario = parametrosConsulta.get("claveIfCesionario")==null?"":(String)parametrosConsulta.get("claveIfCesionario");
			String numeroContrato = parametrosConsulta.get("numeroContrato")==null?"":(String)parametrosConsulta.get("numeroContrato");
			String claveMoneda = parametrosConsulta.get("claveMoneda")==null?"":(String)parametrosConsulta.get("claveMoneda");
			String claveTipoContratacion = parametrosConsulta.get("claveTipoContratacion")==null?"":(String)parametrosConsulta.get("claveTipoContratacion");
			String fechaVigenciaContratoIni = parametrosConsulta.get("fechaVigenciaContratoIni")==null?"":(String)parametrosConsulta.get("fechaVigenciaContratoIni");
			String fechaVigenciaContratoFin = parametrosConsulta.get("fechaVigenciaContratoFin")==null?"":(String)parametrosConsulta.get("fechaVigenciaContratoFin");
			String csTipoMonto = parametrosConsulta.get("csTipoMonto")==null?"":(String)parametrosConsulta.get("csTipoMonto");
			String plazoContrato = parametrosConsulta.get("plazoContrato")==null?"":(String)parametrosConsulta.get("plazoContrato");
			String claveTipoPlazo = parametrosConsulta.get("claveTipoPlazo")==null?"":(String)parametrosConsulta.get("claveTipoPlazo");
			
			strSQL.append(" SELECT sol.ic_solicitud AS numero_solicitud");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", cder_solic_x_representante csr");
			strSQL.append(", cder_acuse cac");
			strSQL.append(" WHERE sol.ic_solicitud = csr.ic_solicitud");
			strSQL.append(" AND csr.cc_acuse_envio_rep = cac.cc_acuse");
			strSQL.append(" AND csr.ic_usuario = cac.cg_usuario");
			strSQL.append(" AND cac.cg_usuario = ?");
			strSQL.append(" AND sol.ic_estatus_solic = ?");

			/*strSQL.append(" SELECT sol.ic_solicitud AS numero_solicitud");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", cder_acuse cac");
			strSQL.append(" WHERE sol.cc_acuse_envio_rep1 = cac.cc_acuse");
			strSQL.append(" AND cac.cg_usuario = ?");
			strSQL.append(" AND sol.ic_estatus_solic = ?");*/
			
			varBind.add(loginUsuario);
			varBind.add(new Integer(13));
			
			if (clavePyme != null && !"".equals(clavePyme)) {
				strSQL.append(" AND csr.ic_pyme = ?");
				varBind.add(clavePyme);
			}
			
			if (claveEpo != null && !"".equals(claveEpo)) {
				strSQL.append(" AND sol.ic_epo = ?");
				varBind.add(new Integer(claveEpo));
			}
			
			if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
				strSQL.append(" AND sol.ic_if = ?");
				varBind.add(new Integer(claveIfCesionario));
			}
			
			if (numeroContrato != null && !"".equals(numeroContrato)) {
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(numeroContrato);
			}

			
			if (claveMoneda != null && !"".equals(claveMoneda)) {
				if (claveMoneda.equals("0")) {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("S");
				} else {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.ic_moneda = ?)");
					varBind.add("N");
					varBind.add(new Integer(claveMoneda));
				}
			}
			

			
			if (csTipoMonto != null && !"".equals(csTipoMonto)) {
				if (csTipoMonto.equals("D")) {
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("N");
				} else {
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.fn_monto_moneda IS NULL)");
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("N");
				}
			}		
	
			if (plazoContrato != null && !"".equals(plazoContrato)) {
				strSQL.append(" AND sol.ig_plazo = ?");
				varBind.add(new Integer(plazoContrato));
			}		
	
			if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
				strSQL.append(" AND sol.ic_tipo_plazo = ?");
				varBind.add(new Integer(claveTipoPlazo));
			}
			
			if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
				strSQL.append(" AND sol.ic_tipo_contratacion = ?");
				varBind.add(new Integer(claveTipoContratacion));
			}		
			
			if (fechaVigenciaContratoIni != null && !"".equals(fechaVigenciaContratoIni)) {
				strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_ini < TO_DATE(?, 'dd/mm/yyyy') + 1");
				varBind.add(fechaVigenciaContratoIni);
				varBind.add(fechaVigenciaContratoIni);
			}
			
			if (fechaVigenciaContratoFin != null && !"".equals(fechaVigenciaContratoFin)) {
				strSQL.append(" AND sol.df_fecha_vigencia_fin >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_fin < TO_DATE(?, 'dd/mm/yyyy') + 1");
				varBind.add(fechaVigenciaContratoFin);
				varBind.add(fechaVigenciaContratoFin);
			}		 
			
			strSQL.append(" ORDER BY sol.ic_solicitud");
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			int numeroRegistros = 0;
			
			while (rst.next()) {
				solicitudesEnviadasPorUsuario.put("numeroSolicitud" + numeroRegistros, rst.getString("numero_solicitud")==null?"":rst.getString("numero_solicitud"));
				numeroRegistros++;
			}
			
			solicitudesEnviadasPorUsuario.put("numeroRegistros", Integer.toString(numeroRegistros));
			
			rst.close();
			pst.close();
		} catch (Exception e) {
			log.error("obtieneSolicitudesEnviadasPorUsuario(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al consultar la base de datos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtieneSolicitudesEnviadasPorUsuario(S) ::..");
		}
		return solicitudesEnviadasPorUsuario;
	}
	
	/**
	 * M�todo que obtiene las solicitudes que han sido enviadas por un usuario de la pyme.
	 * @param parametrosConsulta Parametros que vienen de la pantalla de consulta.
	 * @return solicitudesEnviadasPorUsuario HashMap con las solicitudes que han sido enviadas por un usuario.
	 * @throws AppException Cuando ocurre un error al actualizar el registro.
	 * @author Alberto Cruz Flores.
	 */
	public HashMap obtieneSolicitudesAutorizadasPorUsuario(HashMap parametrosConsulta)	throws AppException	{
		log.info("obtieneSolicitudesAutorizadasPorUsuario(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap solicitudesAutorizadasPorUsuario = new HashMap();
		
		try {
			con.conexionDB();
			
			String loginUsuario = parametrosConsulta.get("loginUsuario")==null?"":(String)parametrosConsulta.get("loginUsuario");
			String clavePyme = parametrosConsulta.get("clavePyme")==null?"":(String)parametrosConsulta.get("clavePyme");
			String claveEpo = parametrosConsulta.get("claveEpo")==null?"":(String)parametrosConsulta.get("claveEpo");
			String claveIfCesionario = parametrosConsulta.get("claveIfCesionario")==null?"":(String)parametrosConsulta.get("claveIfCesionario");
			String numeroContrato = parametrosConsulta.get("numeroContrato")==null?"":(String)parametrosConsulta.get("numeroContrato");
			//String montoContrato = parametrosConsulta.get("montoContrato")==null?"":(String)parametrosConsulta.get("montoContrato");
			String claveMoneda = parametrosConsulta.get("claveMoneda")==null?"":(String)parametrosConsulta.get("claveMoneda");
			String claveTipoContratacion = parametrosConsulta.get("claveTipoContratacion")==null?"":(String)parametrosConsulta.get("claveTipoContratacion");
			String fechaVigenciaContratoIni = parametrosConsulta.get("fechaVigenciaContratoIni")==null?"":(String)parametrosConsulta.get("fechaVigenciaContratoIni");
			String fechaVigenciaContratoFin = parametrosConsulta.get("fechaVigenciaContratoFin")==null?"":(String)parametrosConsulta.get("fechaVigenciaContratoFin");
			String csTipoMonto = parametrosConsulta.get("csTipoMonto")==null?"":(String)parametrosConsulta.get("csTipoMonto");
			String plazoContrato = parametrosConsulta.get("plazoContrato")==null?"":(String)parametrosConsulta.get("plazoContrato");
			String claveTipoPlazo = parametrosConsulta.get("claveTipoPlazo")==null?"":(String)parametrosConsulta.get("claveTipoPlazo");

			/*strSQL.append(" SELECT sol.ic_solicitud AS numero_solicitud");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", cder_acuse cac");
			strSQL.append(" WHERE sol.cc_acuse_firma_rep1 = cac.cc_acuse");
			strSQL.append(" AND cac.cg_usuario = ?");
			strSQL.append(" AND sol.ic_estatus_solic = ?");*/

			strSQL.append(" SELECT sol.ic_solicitud AS numero_solicitud");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", cder_solic_x_representante csr");
			strSQL.append(", cder_acuse cac");
			strSQL.append(" WHERE sol.ic_solicitud = csr.ic_solicitud");
			strSQL.append(" AND csr.cc_acuse_firma_rep = cac.cc_acuse");
			strSQL.append(" AND csr.ic_usuario = cac.cg_usuario");
			strSQL.append(" AND cac.cg_usuario = ?");
			strSQL.append(" AND sol.ic_estatus_solic = ?");
			
			varBind.add(loginUsuario);
			varBind.add(new Integer(14));
			
			if (clavePyme != null && !"".equals(clavePyme)) {
				strSQL.append(" AND csr.ic_pyme = ?");
				varBind.add(clavePyme);
			}
			
			if (claveEpo != null && !"".equals(claveEpo)) {
				strSQL.append(" AND sol.ic_epo = ?");
				varBind.add(new Integer(claveEpo));
			}
			
			if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
				strSQL.append(" AND sol.ic_if = ?");
				varBind.add(new Integer(claveIfCesionario));
			}
			
			if (numeroContrato != null && !"".equals(numeroContrato)) {
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(numeroContrato);
			}
/*
			if (montoContrato != null && !"".equals(montoContrato)) {
				if (montoContrato.indexOf(",") != -1) {montoContrato = montoContrato.replaceAll(",", "");}
					strSQL.append(" AND sol.cg_no_contrato = ?");
					varBind.add(new BigDecimal(montoContrato).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
*/
			
			if (claveMoneda != null && !"".equals(claveMoneda)) {
				if (claveMoneda.equals("0")) {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("S");
				} else {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.ic_moneda = ?)");
					varBind.add("N");
					varBind.add(new Integer(claveMoneda));
				}
			}
			
/*
			if (claveMoneda != null && !"".equals(claveMoneda)) {
				strSQL.append(" AND sol.ic_moneda = ?");
				varBind.add(new Integer(claveMoneda));
			}
*/
			if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
				strSQL.append(" AND sol.ic_tipo_contratacion = ?");
				varBind.add(new Integer(claveTipoContratacion));
			}		
			
			if (csTipoMonto != null && !"".equals(csTipoMonto)) {
				if (csTipoMonto.equals("D")) {
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("N");
				} else {
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.fn_monto_moneda IS NULL)");
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("N");
				}
			}		
	
			if (plazoContrato != null && !"".equals(plazoContrato)) {
				strSQL.append(" AND sol.ig_plazo = ?");
				varBind.add(new Integer(plazoContrato));
			}		
	
			if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
				strSQL.append(" AND sol.ic_tipo_plazo = ?");
				varBind.add(new Integer(claveTipoPlazo));
			}
			
			if (fechaVigenciaContratoIni != null && !"".equals(fechaVigenciaContratoIni)) {
				strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_ini < TO_DATE(?, 'dd/mm/yyyy') + 1");
				varBind.add(fechaVigenciaContratoIni);
				varBind.add(fechaVigenciaContratoIni);
			}
			
			if (fechaVigenciaContratoFin != null && !"".equals(fechaVigenciaContratoFin)) {
				strSQL.append(" AND sol.df_fecha_vigencia_fin >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_fin < TO_DATE(?, 'dd/mm/yyyy') + 1");
				varBind.add(fechaVigenciaContratoFin);
				varBind.add(fechaVigenciaContratoFin);
			}		 
			
			strSQL.append(" ORDER BY sol.ic_solicitud");
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			int numeroRegistros = 0;
			
			while (rst.next()) {
				solicitudesAutorizadasPorUsuario.put("numeroSolicitud" + numeroRegistros, rst.getString("numero_solicitud")==null?"":rst.getString("numero_solicitud"));
				numeroRegistros++;
			}
			
			solicitudesAutorizadasPorUsuario.put("numeroRegistros", Integer.toString(numeroRegistros));
			
			rst.close();
			pst.close();
		} catch (Exception e) {
			log.error("obtieneSolicitudesAutorizadasPorUsuario(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al consultar la base de datos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtieneSolicitudesAutorizadasPorUsuario(S) ::..");
		}
		return solicitudesAutorizadasPorUsuario;
	}
	
	/**
	 * M�todo que permite al perfil EPO VENTANILLA rechazar la notificaci�n del contrato de cesi�n,
	 * y regresala al estatus Aceptada y Notificada a Ventanilla.
	 *
	 * @deprecated
	 *
	 * @param numeroSolicitud Clave de la solicitud que ser� rechazada.
	 * @throws AppException Cuando ocurre un error al actualizar el registro.
	 * @author Alberto Cruz Flores.
	 */
	public void rechazarNotifEpoVentanilla(String numeroSolicitud)	throws AppException	{
		log.info("rechazarNotifEpoVentanilla ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		
		try {
			con.conexionDB();

			strSQL.append(" UPDATE cder_solicitud");
			strSQL.append(" SET ic_estatus_solic = ?");
			strSQL.append(" WHERE ic_solicitud = ?");
			
			varBind.add(new Integer(15));
			varBind.add(numeroSolicitud);

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
		} catch (Exception e) {
			transactionOk = false;
			log.error("rechazarNotifEpoVentanilla(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al modificar la solicitud de consentimiento.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("rechazarNotifEpoVentanilla(S) ::..");
		}
	}

	/**
	 * M�todo que realiza el rechazo y almacenamiento de la firma de rechazo del contrato de cesi�n de derechos por 
	 * parte de la EPO Ventanilla.
	 *
	 *	@author Jesus Salim Hernandez David
	 * @since Fodea 041 - 2010
	 *
	 * @param parametrosFirma HashMap con los parametros con los que se reliza la autenticaci�n del usuario.
	 * @return Acuse Cadena con el numero de acuse generado al autenticar al usuario.
	 * @throws Appexception Cuando ocurre un error al obtener el acuse y/o guardar la firma de la pyme.
	 *
	 */
	public String acuseNotifEpoVentRechazo(HashMap parametrosFirma,String observaciones) 
		throws AppException{
			
		log.info("acuseNotifEpoVentRechazo(E)");
		
		AccesoDB 			con 			= new AccesoDB();
		PreparedStatement pst 			= null;
		ResultSet 			rst 			= null;
		StringBuffer 		strSQL 		= new StringBuffer();
		List 					varBind 		= new ArrayList();
		char 					getReceipt 	= 'Y';
		boolean 				trans_op 	= true;
		String 				acuse 		= "";
			
		String _serial 			= parametrosFirma.get("_serial")			== null?"":(String)parametrosFirma.get("_serial");
		String textoPlano 		= parametrosFirma.get("textoPlano")		== null?"":(String)parametrosFirma.get("textoPlano");
		String pkcs7 				= parametrosFirma.get("pkcs7")			== null?"":(String)parametrosFirma.get("pkcs7");
		String folioCert 			= parametrosFirma.get("folioCert")		== null?"":(String)parametrosFirma.get("folioCert");
		boolean validCert 		= Boolean.getBoolean((String)parametrosFirma.get("validCert"));
		String claveSolicitud 	= parametrosFirma.get("claveSolicitud")== null?"":(String)parametrosFirma.get("claveSolicitud");
		String loginUsuario 		= parametrosFirma.get("loginUsuario")	== null?"":(String)parametrosFirma.get("loginUsuario");
		String nombreUsuario 	= parametrosFirma.get("nombreUsuario")	== null?"":(String)parametrosFirma.get("nombreUsuario");
		String tipoOperacion 	= parametrosFirma.get("tipoOperacion")	== null?"":(String)parametrosFirma.get("tipoOperacion");
		
		int seqNEXTVAL = 0;
		
		PreparedStatement ps1 = null;
		ResultSet 			rs1 = null;
		
		try {
			con.conexionDB();
			
			if (!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")) {
				Seguridad s = new Seguridad();
				if (s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt)) {
					acuse = s.getAcuse();
					
					strSQL.append(" SELECT SEQ_CDER_ACUSE.nextval FROM dual");
					ps1 = con.queryPrecompilado(strSQL.toString());
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
					}
					ps1.close();
					rs1.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();
					
					strSQL.append(" UPDATE cder_solicitud       ");
					strSQL.append(" SET ic_estatus_solic 	= ?, ");
					strSQL.append(" cc_acuse_redirec 		= ?, ");
					strSQL.append(" cg_observ_rechazo 		= ?, ");
					strSQL.append(" df_redireccion 			= SYSDATE ");
					strSQL.append(" WHERE ic_solicitud 		= ?  ");

					varBind.add(new Integer(15));
					varBind.add(new Integer(seqNEXTVAL));
					varBind.add(observaciones);
					varBind.add(claveSolicitud);
					
					log.debug("---> strSQL:  "+strSQL.toString());
					log.debug("---> varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(seqNEXTVAL));
					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(acuse);
					
					log.debug("---> strSQL:  "+strSQL.toString());
					log.debug("---> varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}
			}
		}catch(Exception e){
			log.info("acuseNotifEpoVentRechazo(Exception)");
			e.printStackTrace();
			trans_op = false;
			throw new AppException("Error realizar el rechazo y almacenamiento de la firma de cesion de derechos por parte de la EPO Ventanilla ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseNotifEpoVentRechazo(S) ::..");
		}
		return acuse;
	}
	
	/**
	 * M�todo que guarda la firma de la Notificaci�n del contrato de cesi�n de derechos por parte de la EPO Ventanilla.
	 * @param parametrosFirma HashMap con los parametros con los que se reliza la autenticaci�n del usuario.
	 * @return Acuse Cadena con el numero de acuse generado al autenticar al usuario.
	 * @throws Appexception Cuando ocurre un error al obtener el acuse y/o guardar la firma de la pyme.
	 */
	public String acuseNotifEpoVentRedireccionamiento(HashMap parametrosFirma) throws AppException{
		log.info("acuseNotifEpoVentRedireccionamiento(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		char getReceipt = 'Y';
		boolean trans_op = true;
		String acuse = "";
			
		String _serial = parametrosFirma.get("_serial")==null?"":(String)parametrosFirma.get("_serial");
		String textoPlano = parametrosFirma.get("textoPlano")==null?"":(String)parametrosFirma.get("textoPlano");
		String pkcs7 = parametrosFirma.get("pkcs7")==null?"":(String)parametrosFirma.get("pkcs7");
		String folioCert = parametrosFirma.get("folioCert")==null?"":(String)parametrosFirma.get("folioCert");
		boolean validCert = Boolean.getBoolean((String)parametrosFirma.get("validCert"));
		String claveSolicitud = parametrosFirma.get("claveSolicitud")==null?"":(String)parametrosFirma.get("claveSolicitud");
		String loginUsuario = parametrosFirma.get("loginUsuario")==null?"":(String)parametrosFirma.get("loginUsuario");
		String nombreUsuario = parametrosFirma.get("nombreUsuario")==null?"":(String)parametrosFirma.get("nombreUsuario");
		String tipoOperacion = parametrosFirma.get("tipoOperacion")==null?"":(String)parametrosFirma.get("tipoOperacion");
		
		
		
		int seqNEXTVAL = 0;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			con.conexionDB();
			
			if (!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")) {
				Seguridad s = new Seguridad();
				if (s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt)) {
					acuse = s.getAcuse();
					
					strSQL.append(" SELECT SEQ_CDER_ACUSE.nextval FROM dual");
					ps1 = con.queryPrecompilado(strSQL.toString());
					rs1 = ps1.executeQuery();

					if (rs1.next()) {
						seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
					}

					ps1.close();
					rs1.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();
					
					strSQL.append(" UPDATE cder_solicitud");
					strSQL.append(" SET ic_estatus_solic = ?,");
					strSQL.append(" cc_acuse_redirec = ?,");
					strSQL.append(" df_redireccion = SYSDATE");
					strSQL.append(" WHERE ic_solicitud = ?");

					varBind.add(new Integer(17));
					varBind.add(new Integer(seqNEXTVAL));
					varBind.add(claveSolicitud);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(seqNEXTVAL));
					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(acuse);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}
			}
		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error al firmar el contrato de cesi�n de derechos: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseNotifEpoVentRedireccionamiento(S) ::..");
		}
		return acuse;
	}
	
	/**
	 * M�todo que genera el catalogo de tipo de contrataci�n por epo.
	 * @param claveEpo clave interna de la EPO
	 * @return comboTipoContratacion List con los elementos del catalogo encontratos.
	 * @throws Appexception Cuando ocurre un error al consultar en la BD.
	 */
	public List obtenerComboTipoContratacion(String claveEpo) throws AppException{
		log.info("obtenerComboTipoContratacion(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List comboTipoContratacion =  new ArrayList();
			
		try {
			con.conexionDB();

			strSQL.append(" SELECT clave, descripcion");
			strSQL.append(" FROM (");
			strSQL.append(" SELECT ic_tipo_contratacion clave,");
			strSQL.append(" cg_nombre descripcion");
			strSQL.append(" from cdercat_tipo_contratacion");
			strSQL.append(" WHERE ic_epo IS NULL");
			strSQL.append(" UNION");
			strSQL.append(" SELECT ic_tipo_contratacion clave,");
			strSQL.append(" cg_nombre descripcion");
			strSQL.append(" from cdercat_tipo_contratacion");
			strSQL.append(" WHERE ic_epo = ?");
			strSQL.append(" ) cat_temp");
			strSQL.append(" ORDER BY descripcion");

			varBind.add(new Integer(claveEpo));

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while(rst.next()){
				ElementoCatalogo elemento_catalogo = new ElementoCatalogo();
				elemento_catalogo.setClave(rst.getString("clave")==null?"":rst.getString("clave"));
				elemento_catalogo.setDescripcion(rst.getString("descripcion")==null?"":rst.getString("descripcion"));
				comboTipoContratacion.add(elemento_catalogo);
			}
			
			rst.close();
			pst.close();
			
			
			
		}catch(Exception e){
			log.error("obtenerComboTipoContratacion(ERROR) ::..");
			throw new AppException("Error al generar el catalogo: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("obtenerComboTipoContratacion(S) ::..");
		}
		return comboTipoContratacion;
	}
	//FODEA 041 - 2010 ACF (F)
	
		//Fodea 041-2010 DLHC
	
	/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param catalogo
	 */
	 
	public String obtenerClave (String catalogo)	throws AppException	{

		log.info("obtenerClave (E)");
			PreparedStatement ps = null;			
			ResultSet rs        = null;
			AccesoDB	 con 	     =   new AccesoDB();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
		  String clave ="";
			try {
			 con.conexionDB();
			 
			 if(catalogo.equals("C")) {
			 
			  qrySentencia.append(" select MAX(ic_tipo_contratacion)+1 as clave");
				qrySentencia.append("  from  cdercat_tipo_contratacion ");
				 
				ps = con.queryPrecompilado(qrySentencia.toString());			
				rs = ps.executeQuery();
				
				while(rs.next()){
					clave = rs.getString("clave")==null?"":rs.getString("clave");
				}
				rs.close();
				ps.close();
		}
		
		}catch(Exception e){
			log.error("consultaCatalogos (Exception) " + e);			
			throw new AppException("consultaCatalogos(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("consultaCatalogos (E)");		
		return clave;	
}

/**
	 * metodo que regresa los registros del catalogo seleccionado
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param catalogo
	 */
	public List  consultaCatalogos (String catalogo)	throws AppException	{

		log.info("consultaCatalogos (E)");
	

			PreparedStatement ps = null;			
			ResultSet rs        = null;
			AccesoDB	 con 	     =   new AccesoDB();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			List registros = new ArrayList();
			List datos = new ArrayList();
			try {
			 con.conexionDB();
			 
			 if(catalogo.equals("C")) {
			 
			  qrySentencia.append(" SELECT  ic_tipo_contratacion, cg_nombre" );
				qrySentencia.append(" FROM cdercat_tipo_contratacion ");
				qrySentencia.append(" ORDER BY ic_tipo_contratacion");
			
				ps = con.queryPrecompilado(qrySentencia.toString());			
				rs = ps.executeQuery();
				
				
				
				while(rs.next()){
					datos = new ArrayList();
					datos.add(rs.getString(1)==null?"":rs.getString(1));
					datos.add(rs.getString(2)==null?"":rs.getString(2));
					registros.add(datos);
				}
			rs.close();
			ps.close();
		}
		
		}catch(Exception e){
			log.error("consultaCatalogos (Exception) " + e);			
			throw new AppException("consultaCatalogos(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("consultaCatalogos (E)");		
		return registros;	
}

/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param descripcion
	 * @param clave
	 * @param catalogo
	 */
	public String agregarDatoCatalogo (String catalogo, String clave, String descripcion, String epo)	throws AppException	{

		log.info("agregarDatoCatalogo (E)");
	

			PreparedStatement ps = null;			
			ResultSet rs        = null;
			AccesoDB	 con 	     =   new AccesoDB();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			List registros = new ArrayList();
			List datos = new ArrayList();
			String respuesta = "";
			List lvarbind = new ArrayList();
			
			try {
			 con.conexionDB();
			 
			 if(catalogo.equals("C")) {
			 
			  qrySentencia.append(" insert into cdercat_tipo_contratacion ");
				qrySentencia.append(" (ic_tipo_contratacion, cg_nombre, IC_EPO) ");
				qrySentencia.append("  values (?,?,?) " );

				lvarbind.add(clave);							
				lvarbind.add(descripcion);	
				lvarbind.add(epo);
				
				ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);
				ps.executeUpdate();
				ps.close();
			
			respuesta = "A";
		}
		
		}catch(Exception e){
			log.error("agregarDatoCatalogo (Exception) " + e);			
			throw new AppException("agregarDatoCatalogo(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("agregarDatoCatalogo (E)");		
		return respuesta;	
}


/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param descripcion
	 * @param clave
	 * @param catalogo
	 */
	public String  modificarCatalogo (String catalogo, String clave, String descripcion, String epo)	throws AppException	{

		log.info("modificarCatalogo (E)");
	

			PreparedStatement ps = null;			
			ResultSet rs        = null;
			AccesoDB	 con 	     =   new AccesoDB();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			List registros = new ArrayList();
			List datos = new ArrayList();
			String respuesta = "";
			List lvarbind = new ArrayList();
			
			try {
			 con.conexionDB();
			 
			 if(catalogo.equals("C")) {
			 
			  qrySentencia.append(" UPDATE  cdercat_tipo_contratacion ");
				qrySentencia.append(" SET cg_nombre = ?, ");
				qrySentencia.append(" ic_epo = ? ");
				qrySentencia.append(" WHERE ic_tipo_contratacion = ?");
			
				lvarbind.add(descripcion);
				lvarbind.add(epo);
				lvarbind.add(clave);							
					
				ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);
				ps.executeUpdate();
				ps.close();
			
			respuesta = "M";
		}
		
		}catch(Exception e){
			log.error("modificarCatalogo (Exception) " + e);			
			throw new AppException("modificarCatalogo(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("modificarCatalogo (E)");		
		return respuesta;	
}
/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param clave
	 * @param catalogo
	 */
	public String  eliminaCatalogo (String catalogo, String clave)	throws AppException	{

		log.info("eliminaCatalogo (E)");
	

			PreparedStatement ps = null;			
			ResultSet rs        = null;
			AccesoDB	 con 	     =   new AccesoDB();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			List registros = new ArrayList();
			List datos = new ArrayList();
			String respuesta = "";
			List lvarbind = new ArrayList();
			
			try {
			 con.conexionDB();
			 
			 if(catalogo.equals("C")) {
			 
			  qrySentencia.append(" DELETE FROM  cdercat_tipo_contratacion ");
				qrySentencia.append(" WHERE ic_tipo_contratacion = ?");
			
				lvarbind.add(clave);							
					
				ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);
				ps.executeUpdate();
				ps.close();
			
			respuesta = "E";
		}
		
		}catch(Exception e){
			log.error("eliminaCatalogo (Exception) " + e);			
			throw new AppException("eliminaCatalogo(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("eliminaCatalogo (E)");		
		return respuesta;	
}
	/**
	 * M�todo que obtiene las solicitudes de consentimiento pendientes de autorizar por la EPO.
	 * @param noEpo Clabe interna de la EPO.
	 * @return registros Objeto Registros con el resultado de la consulta
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	public List  consultaSolicitudes (String noEpo)	throws AppException	{
		log.info("consultaSolicitudes (E)");
		PreparedStatement ps = null;			
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		List registros = new ArrayList();
		List datos = new ArrayList();
		List lvarbind = new ArrayList();
		
		try {
			con.conexionDB();
			
			qrySentencia.append(" SELECT sol.ic_solicitud AS noSolicitud, ");
			qrySentencia.append(" i.cg_razon_social AS cecionario, ");
			qrySentencia.append(" p.cg_razon_social AS pyme,");
			qrySentencia.append(" p.cg_rfc AS rfc, ");
			qrySentencia.append(" p.ic_pyme AS noproveedor, ");
			qrySentencia.append(" TO_CHAR (df_fecha_solicitud_ini, 'DD/MM/YYYY') AS fsolicitudini, ");
			qrySentencia.append(" sol.cg_no_contrato AS nocontrato,");
			//qrySentencia.append(" sol.fn_monto_contrato AS monto ");
			qrySentencia.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato,  ");
			qrySentencia.append(" (CASE WHEN sol.IN_CONTADOR_PRORROGA>0 then 'true' else 'false' end) as solicitud_prorroga, ");
			qrySentencia.append(" NVL(sol.CG_EMPRESAS_REPRESENTADAS,'') as empresas");//FODEA-024-2014 MOD()
			qrySentencia.append(" FROM cder_solicitud sol, ");
			qrySentencia.append(" comcat_epo e, comcat_if i, ");
			qrySentencia.append(" cdercat_clasificacion_epo cl,  ");
			qrySentencia.append(" cdercat_tipo_contratacion con, ");
			//qrySentencia.append(" comcat_moneda m, ");
			qrySentencia.append(" cdercat_estatus_solic es, ");
			qrySentencia.append(" comcat_pyme p ");
			qrySentencia.append(" WHERE sol.ic_epo = e.ic_epo ");
			qrySentencia.append(" AND sol.ic_if = i.ic_if ");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo ");
			//qrySentencia.append(" AND sol.ic_moneda = m.ic_moneda ");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion ");
			qrySentencia.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic ");
			qrySentencia.append(" AND sol.ic_pyme = p.ic_pyme ");
			qrySentencia.append(" AND sol.ic_estatus_solic IN (2, 3, 4, 5, 6, 8, 14, 7, 9, 10 ) "); 
			qrySentencia.append(" AND sol.ic_epo = ? ");
			qrySentencia.append(" AND (  sol.cg_enterado IS NULL or  sol.in_contador_prorroga > 0)  ");   
			qrySentencia.append(" AND ( sol.cs_estatus_prorroga = 'P' or  sol.cs_estatus_prorroga is null)  "); 
			qrySentencia.append(" ORDER BY solicitud_prorroga ");
			
			lvarbind.add(noEpo);					
			
			log.debug("..:: qrySentencia: "+qrySentencia);
			log.debug("..:: lvarbind: "+lvarbind);
			
			ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				datos = new ArrayList();
				StringBuffer montosPorMoneda = this.getMontoMoneda(rs.getString("noSolicitud"));
				datos.add(rs.getString("noSolicitud")==null?"":rs.getString("noSolicitud"));
				datos.add(rs.getString("cecionario")==null?"":rs.getString("cecionario"));
				datos.add(rs.getString("pyme")==null?"":rs.getString("pyme"));
				datos.add(rs.getString("rfc")==null?"":rs.getString("rfc"));
				datos.add(rs.getString("noproveedor")==null?"":rs.getString("noproveedor"));
				datos.add(rs.getString("fsolicitudini")==null?"":rs.getString("fsolicitudini"));
				datos.add(rs.getString("nocontrato")==null?"":rs.getString("nocontrato"));
				datos.add(montosPorMoneda.toString());
				datos.add(rs.getString("firma_contrato")==null?"":rs.getString("firma_contrato"));
				datos.add(rs.getString("solicitud_prorroga")==null?"":rs.getString("solicitud_prorroga"));
				datos.add(rs.getString("empresas")==null?"":rs.getString("empresas"));
				registros.add(datos);
			}
			rs.close();
			ps.close();
		} catch(Exception e) {
			log.error("consultaSolicitudes (Exception) " + e);			
			throw new AppException("consultaCatalogos(Exception) ", e); 
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			log.info("consultaSolicitudes (S)");
		}
		return registros;	
	}

	/**
	 * M�todo que actualiza la bandera que muestra el aviso de solicitudes de consentimiento
	 * pendientes de autorizar por la EPO.
	 * @return respuesta String con el resultado de la actualizaci�n.
	 * @param solicitudes List con las solicitudes a actualizar.
	 * @throws AppException Cuando ocurre un error en la actualizaci�n de la bandera.
	 */
		public String  aceptaEnterado (List solicitudes) throws AppException {
			log.info("aceptaEnterado (E)");
			AccesoDB con = new AccesoDB();
			PreparedStatement ps = null;
			boolean transactionOk = true;
			StringBuffer qrySentencia = new StringBuffer();
			List registros = new ArrayList();
			String respuesta = "Actualizado";
			List lvarbind = new ArrayList();
		
		try {
			con.conexionDB();
			
			log.debug("..:: solicitudes: "+ solicitudes.size());
			log.debug("..:: solicitudes: "+ solicitudes);
			
			for (int i = 0; i < solicitudes.size(); i++) {
				String solicitud = (String)solicitudes.get(i);
				
				log.debug("..:: solicitud: "+ solicitud );
				
				qrySentencia = new StringBuffer();
				lvarbind = new ArrayList();
				qrySentencia.append(" UPDATE  cder_solicitud ");
				qrySentencia.append(" SET cg_enterado = ? ");				
				qrySentencia.append(" WHERE ic_solicitud = ? ");  
				
				lvarbind.add("S");
				lvarbind.add(solicitud);	
				
				log.debug("..:: qrySentencia: "+qrySentencia.toString());
				log.debug("..:: lvarbind: "+lvarbind);
				
				
				ps = con.queryPrecompilado(qrySentencia.toString(), lvarbind);
				ps.executeUpdate();
				ps.close();
			}
		} catch(Exception e) {
			transactionOk = false;
			log.error("aceptaEnterado (Exception) " + e);			
			e.printStackTrace();
			throw new AppException("aceptaEnterado(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
			con.terminaTransaccion(transactionOk);
			con.cierraConexionDB();
			}
			log.info("aceptaEnterado (S)");
		}
		return respuesta;	
	}
	
	/**
	 * M�todo que realiza la consulta que se muestra en la pantalla de env�o de correo de notificaci�n
	 * a las ventanillas y en el correo que se env�a a las mismas.
	 * @param noEpo String con la clave de la EPO.
	 * @param solicitud String con el n�mero de la solicitud de cesi�n de derechos.
	 * @return registros List con el resultado de la consulta.
	 * @throws AppException cuando ocurre un error en la consulta.
	 */
	public List  IniEnvioCorreo (String noEpo, String solicitud)	throws AppException	{
		log.info("IniEnvioCorreo (E)");
		PreparedStatement ps = null;
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qrySentencia2 = new StringBuffer();
		PreparedStatement ps2 = null;			
		ResultSet rs2        = null;
		List registros = new ArrayList();
		List datos = new ArrayList();
		List lvarbind = new ArrayList();
		
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			lvarbind = new ArrayList();

			qrySentencia.append(" SELECT  ");
			qrySentencia.append(" pym.CG_RAZON_SOCIAL as pyme, ");
			qrySentencia.append(" sol.CG_EMPRESAS_REPRESENTADAS as empresas, ");
			qrySentencia.append(" pym.CG_RFC as rfc, ");				
			qrySentencia.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,  ");
			qrySentencia.append(" sol.df_fecha_solicitud_ini  AS FSOLICITUDCESION,  ");
			//INFORMACION DEL CONTRATO'
			qrySentencia.append(" sol.cg_no_contrato AS numero_contrato,   ");
			//qrySentencia.append(" sol.fn_monto_contrato AS monto_contrato,  ");
			//qrySentencia.append(" mon.cd_nombre AS moneda,  ");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			qrySentencia.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			//qrySentencia.append(" TO_CHAR (sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')  || '  a  ' || TO_CHAR (sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')  AS FECHAVIGENCIA,     ");
			qrySentencia.append(" tcn.cg_nombre AS tipo_contratacion,  ");
			qrySentencia.append(" sol.cg_obj_contrato AS objeto_contrato,  ");
			qrySentencia.append(" sol.cg_campo1 AS campo_adicional_1,  ");
			qrySentencia.append(" sol.cg_campo2 AS campo_adicional_2,   ");
			qrySentencia.append(" sol.cg_campo3 AS campo_adicional_3,  ");
			qrySentencia.append(" sol.cg_campo4 AS campo_adicional_4,  ");
			qrySentencia.append(" sol.cg_campo5 AS campo_adicional_5, ");
			//INFORMACION DE LA EMPRESA DE PRIMER ORDEN
			qrySentencia.append(" epo.CG_RAZON_SOCIAL AS nombrePrimerOrden,  ");
			qrySentencia.append(" epo.CG_NOMBREREP_LEGAL || epo.CG_APPAT_REP_LEGAL || epo.CG_APMAT_REP_LEGAL as personAutorizada, ");
			//informacion del CESIONARIO
			qrySentencia.append(" cif.CG_RAZON_SOCIAL as cesionario,  ");
			qrySentencia.append(" sol.cg_banco_deposito AS banco_deposito,  ");
			qrySentencia.append(" sol.cg_cuenta AS numero_cuenta,   ");
			qrySentencia.append(" sol.cg_cuenta_clabe AS clabe,  ");
			//informacion de la cesion de Derechos
			qrySentencia.append(" TO_CHAR (sol.DF_FECHA_SOLICITUD_INI, 'dd/mm/yyyy') as fechaSolic,  ");
			//para obetener los datos de 	 informacion de la cesion de Derechos
			qrySentencia.append("  sol.cc_acuse2 AS acuse2,  ");  //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			qrySentencia.append("  sol.cc_acuse3 AS acuse3,  "); // ESTE ES EL ACUSE DE LA PYME CUANDO FIRMA EL ECONTRATO
			qrySentencia.append("  sol.cc_acuse4 AS acuse4,   "); // ESTE ES EL ACUSE DEL IF CUANDO FIRMA EL ECONTRATO
			qrySentencia.append("  sol.cc_acuse5 AS acuse5,  "); // ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA LA NOTIFICACION
			qrySentencia.append(" sol.cc_acuse_firma_rep1 AS acuse_firma_rep1,  "); //para obtener el representante 1
			qrySentencia.append(" sol.cc_acuse_firma_rep2 AS acuse_firma_rep2,   "); //para obtener el representante 2
			qrySentencia.append(" sol.cc_acuse_testigo1 AS acuse_firma_test1,"); //TESTIGO 1
			qrySentencia.append(" sol.cc_acuse_testigo2 AS acuse_firma_test2,"); //TESTIGO 2
			qrySentencia.append(" sol.cc_acuse_redirec AS acuse_redirec ,"); //ventanilla
			qrySentencia.append(" TO_CHAR (sol.DF_NOTIF_VENT, 'dd/mm/yyyy') AS feInVentanilla ,"); //ventanilla
			qrySentencia.append(" TO_CHAR (sol.DF_NOTIF_VENT, 'dd/mm/yyyy') AS feRecepVentanilla , "); //ventanilla
			qrySentencia.append(" TO_CHAR (sol.df_redireccion, 'dd/mm/yyyy') AS feAplicaRedirecc , "); //ventanilla
			qrySentencia.append(" sol.cc_acuse_redirec AS acuseRedic , "); //ventanilla
			qrySentencia.append(" TO_CHAR (sol.DF_FIRMA_REP1, 'dd/mm/yyyy') AS DF_FIRMA_REP1, "); //fecha de formalizaci�n del contrato
			qrySentencia.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato"); 
			
			qrySentencia.append(" from  ");
			qrySentencia.append(" cder_solicitud sol,  ");
			qrySentencia.append(" comcat_pyme pym,  ");
			qrySentencia.append(" comrel_pyme_epo cpe,  ");
			//qrySentencia.append(" comcat_moneda mon,  ");
			qrySentencia.append(" cdercat_tipo_contratacion tcn,  ");
			qrySentencia.append(" comcat_epo epo,  ");
			qrySentencia.append("comcat_if cif  ");
			qrySentencia.append(", cdercat_tipo_plazo stp");
			qrySentencia.append("  where  sol.ic_pyme = pym.ic_pyme  ");
			qrySentencia.append("  AND sol.ic_epo = cpe.ic_epo(+)  ");
			qrySentencia.append(" AND sol.ic_pyme = cpe.ic_pyme(+)  ");
			//qrySentencia.append(" AND sol.ic_moneda = mon.ic_moneda  ");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion  ");
			qrySentencia.append(" and sol.ic_epo = epo.ic_epo  ");
			qrySentencia.append(" AND sol.ic_if = cif.ic_if  ");
			qrySentencia.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			qrySentencia.append(" AND sol.ic_epo = ? ");
			qrySentencia.append(" AND sol.ic_solicitud = ? ");
			
			lvarbind.add(noEpo);
			lvarbind.add(solicitud);	
			
			ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);			
			rs = ps.executeQuery();
			
			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("lvarbind "+lvarbind);
			
			qrySentencia2.append(" SELECT cg_nombre_usuario as nombre_usuario,");
			qrySentencia2.append(" TO_CHAR(df_operacion, 'dd/mm/yyyy') fecha_acuse  ");
			qrySentencia2.append(" FROM cder_acuse ");
			qrySentencia2.append(" WHERE cc_acuse = ? ");
			
			
			while(rs.next()){
			datos = new ArrayList();
			datos.add(rs.getString("pyme")==null?"":rs.getString("pyme"));
			datos.add(rs.getString("rfc")==null?"":rs.getString("rfc"));
			StringBuffer montosPorMoneda = this.getMontoMoneda(solicitud);
			
			
			String acuse_firma_rep1  =  rs.getString("acuse_firma_rep1")==null?"":rs.getString("acuse_firma_rep1"); 
			String acuse_firma_rep2  =  rs.getString("acuse_firma_rep2")==null?"":rs.getString("acuse_firma_rep2");  
			
			
			if(!acuse_firma_rep1.equals("")){ //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			ps2 = con.queryPrecompilado(qrySentencia2.toString());				
			ps2.setString(1,acuse_firma_rep1);	
			rs2 = ps2.executeQuery(); 
			
			if(rs2.next()){				
				datos.add(rs2.getString("nombre_usuario")==null?"":rs2.getString("nombre_usuario"));					
			}else {
				datos.add(" ");				
			}
			rs2.close();
			ps2.close();
			
			}else  {
				datos.add(" ");						
			}
			
			if(!acuse_firma_rep2.equals("")){ //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			
			ps2 = con.queryPrecompilado(qrySentencia2.toString());				
			ps2.setString(1,acuse_firma_rep2);	
			rs2 = ps2.executeQuery(); 
			
			if(rs2.next()){				
				datos.add(rs2.getString("nombre_usuario")==null?"":rs2.getString("nombre_usuario"));					
			}else {
				datos.add(" ");
			}
			rs2.close();
			ps2.close();
			}else {
			datos.add(" ");				
			}
			
			datos.add(rs.getString("numero_proveedor")==null?"":rs.getString("numero_proveedor"));
			datos.add(rs.getString("FSOLICITUDCESION")==null?"":rs.getString("FSOLICITUDCESION"));
			//
			datos.add(rs.getString("numero_contrato")==null?"":rs.getString("numero_contrato"));
			//datos.add(rs.getString("monto_contrato")==null?"":rs.getString("monto_contrato"));	
			//datos.add(rs.getString("moneda")==null?"":rs.getString("moneda"));	
			datos.add(montosPorMoneda.toString());
			datos.add(rs.getString("fecha_inicio_contrato")==null?"":rs.getString("fecha_inicio_contrato"));	
			datos.add(rs.getString("fecha_fin_contrato")==null?"":rs.getString("fecha_fin_contrato"));
			datos.add(rs.getString("plazo_contrato")==null?"":rs.getString("plazo_contrato"));	
			
			datos.add(rs.getString("tipo_contratacion")==null?"":rs.getString("tipo_contratacion"));	
			datos.add(rs.getString("objeto_contrato")==null?"":rs.getString("objeto_contrato"));	
			datos.add(rs.getString("campo_adicional_1")==null?"":rs.getString("campo_adicional_1"));	
			datos.add(rs.getString("campo_adicional_2")==null?"":rs.getString("campo_adicional_2"));	
			datos.add(rs.getString("campo_adicional_3")==null?"":rs.getString("campo_adicional_3"));	
			datos.add(rs.getString("campo_adicional_4")==null?"":rs.getString("campo_adicional_4"));	
			datos.add(rs.getString("campo_adicional_5")==null?"":rs.getString("campo_adicional_5"));	
			//
			datos.add(rs.getString("nombrePrimerOrden")==null?"":rs.getString("nombrePrimerOrden"));
			datos.add(rs.getString("personAutorizada")==null?"":rs.getString("personAutorizada"));	
			//
			datos.add(rs.getString("cesionario")==null?"":rs.getString("cesionario"));
			datos.add(rs.getString("banco_deposito")==null?"":rs.getString("banco_deposito"));	
			datos.add(rs.getString("numero_cuenta")==null?"":rs.getString("numero_cuenta"));
			datos.add(rs.getString("clabe")==null?"":rs.getString("clabe"));
			datos.add(rs.getString("fechaSolic")==null?"":rs.getString("fechaSolic"));
			
			
			String acuse2  =  rs.getString("acuse2")==null?"":rs.getString("acuse2");  //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			String acuse3  =  rs.getString("acuse3")==null?"":rs.getString("acuse3");  //ESTE ES EL ACUSE DE LA PYME CUANDO FIRMA EL ECONTRATO
			String acuse4  =  rs.getString("acuse4")==null?"":rs.getString("acuse4");  //ESTE ES EL ACUSE DEL IF CUANDO FIRMA EL ECONTRATO
			String acuse5  =  rs.getString("acuse5")==null?"":rs.getString("acuse5");  //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA LA NOTIFICACION
			String acuse_firma_test1  =  rs.getString("acuse_firma_test1")==null?"":rs.getString("acuse_firma_test1");  //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA LA NOTIFICACION
			String acuse_firma_test2  =  rs.getString("acuse_firma_test2")==null?"":rs.getString("acuse_firma_test2");  //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA LA NOTIFICACION
			String acuseRedic =  rs.getString("acuseRedic")==null?"":rs.getString("acuseRedic"); 
			
			if(!acuse2.equals("")){ //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			ps2 = con.queryPrecompilado(qrySentencia2.toString());				
			ps2.setString(1,acuse2);		
			
			rs2 = ps2.executeQuery(); 
			
			while(rs2.next()){				
			datos.add(rs2.getString("fecha_acuse")==null?"":rs2.getString("fecha_acuse"));
			datos.add(rs2.getString("nombre_usuario")==null?"":rs2.getString("nombre_usuario"));					
			}
			rs2.close();
			ps2.close();
			}else {
			datos.add(" ");
			datos.add(" ");	
			}
			
			datos.add(rs.getString("DF_FIRMA_REP1")==null?"":rs.getString("DF_FIRMA_REP1"));	
			
			String acuse_rep  =  rs.getString("acuse_firma_rep1")==null?"":rs.getString("acuse_firma_rep1");  //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			
			if(!acuse_rep.equals("")){ //NOMBRE DEL FIRMANTE DE CEDENTE (PYME):
			ps2 = con.queryPrecompilado(qrySentencia2.toString());					
			ps2.setString(1,acuse_rep);			
			
			rs2 = ps2.executeQuery(); 
			
			if(rs2.next()){				
			//	datos.add(rs2.getString("fecha_acuse")==null?"":rs2.getString("fecha_acuse"));
			datos.add(rs2.getString("nombre_usuario")==null?"":rs2.getString("nombre_usuario"));					
			}else {
				datos.add(" ");
			}
			rs2.close();
			ps2.close();
			
			}else {
			//datos.add(" ");
			datos.add(" ");	
			}
			
			
			if(!acuse4.equals("")){ //ESTE ES EL ACUSE DEL IF CUANDO FIRMA EL ECONTRATO
			ps2 = con.queryPrecompilado(qrySentencia2.toString());					
			ps2.setString(1,acuse4);
			
			rs2 = ps2.executeQuery(); 
			if(rs2.next()){						
				datos.add(rs2.getString("nombre_usuario")==null?"":rs2.getString("nombre_usuario"));					
			}else {
				datos.add(" ");	
			}
			rs2.close();
			ps2.close();
			
			}else {
			datos.add(" ");						
			}
			
			
			if(!acuse_firma_test1.equals("")){  // TESTIGO 1
			
			ps2 = con.queryPrecompilado(qrySentencia2.toString());					
			ps2.setString(1,acuse_firma_test1);
			
			rs2 = ps2.executeQuery(); 
			if(rs2.next()){						
			datos.add(rs2.getString("nombre_usuario")==null?"":rs2.getString("nombre_usuario"));					
			}else {
			datos.add(" ");	
			}
			rs2.close();
			ps2.close();
			
			}else {
			datos.add(" ");						
			}
			
			if(!acuse_firma_test2.equals("")){  // TESTIGO 2
			
			ps2 = con.queryPrecompilado(qrySentencia2.toString());					
			ps2.setString(1,acuse_firma_test2);
			
			rs2 = ps2.executeQuery(); 
			if(rs2.next()){						
			datos.add(rs2.getString("nombre_usuario")==null?"":rs2.getString("nombre_usuario"));					
			}else {
			datos.add(" ");
			}
			rs2.close();
			ps2.close();
			
			}else {
			datos.add(" ");						
			}
			
			
			if(!acuse5.equals("")){ //ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA LA NOTIFICACION
			
			ps2 = con.queryPrecompilado(qrySentencia2.toString());					
			ps2.setString(1,acuse5);	
			
			rs2 = ps2.executeQuery(); 
			if(rs2.next()){				
			datos.add(rs2.getString("fecha_acuse")==null?"":rs2.getString("fecha_acuse"));
			datos.add(rs2.getString("nombre_usuario")==null?"":rs2.getString("nombre_usuario"));					
			}else  {
				datos.add(" ");
				datos.add(" ");		
			}
			rs2.close();
			ps2.close();
			
			}else {
			datos.add(" ");
			datos.add(" ");	
			}
			
			datos.add(rs.getString("feInVentanilla")==null?"":rs.getString("feInVentanilla"));
			datos.add(rs.getString("feRecepVentanilla")==null?"":rs.getString("feRecepVentanilla"));
			datos.add(rs.getString("feAplicaRedirecc")==null?"":rs.getString("feAplicaRedirecc"));
			
			
			if(!acuseRedic.equals("")){ 
			
			ps2 = con.queryPrecompilado(qrySentencia2.toString());					
			ps2.setString(1,acuse5);	
			
			rs2 = ps2.executeQuery(); 
			if(rs2.next()){						
			datos.add(rs2.getString("nombre_usuario")==null?"":rs2.getString("nombre_usuario"));					
			}else {
			datos.add(" ");	
			}
			rs2.close();
			ps2.close();
			
			}else {
			datos.add(" ");
			
			}
			datos.add(rs.getString("firma_contrato")==null?"":rs.getString("firma_contrato"));
			datos.add(rs.getString("empresas")==null?"":rs.getString("empresas"));
			registros.add(datos);
			
			}//while(rs.next()){
			rs.close();
			ps.close();
			
		} catch(Exception e) {
			log.error("IniEnvioCorreo (Exception) " + e);			
			throw new AppException("IniEnvioCorreo(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("IniEnvioCorreo (S)");		
		return registros;	
	}
/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param comentarios
	 * @param correoPara
	 * @param correoDe
	 * @param claveSolicitud
	 */
	public String  EnviodeCorreo (String claveEpo, String claveSolicitud, String nombre, String correoPara, String comentarios, String ventanilla,String nombreArchivo )	throws AppException	{

		log.info("EnviodeCorreo (E)");


			PreparedStatement ps = null;			
			ResultSet rs        = null;
			AccesoDB	 con 	     =   new AccesoDB();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			List registros = new ArrayList();
			List datos = new ArrayList();
			String respuesta = "Actualizado";
			List lvarbind = new ArrayList();
			String codigoHTML = "";
			String mensajeCorreo2 	= 	"";
			
			String color = "";
				 String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
				"font-size: 10px; "+
				"font-weight: bold;"+
				"color: #FFFFFF;"+
				"background-color: #4d6188;"+
				"padding-top: 3px;"+
				"padding-right: 1px;"+
				"padding-bottom: 1px;"+
				"padding-left: 3px;"+
				"height: 25px;"+
				"border: 1px solid #1a3c54;'";

				String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
				"color:#000066; "+
				"padding-top:1px; "+
				"padding-right:2px; "+
				"padding-bottom:1px; "+
				"padding-left:2px; "+
				"height:22px; "+
				"font-size:11px;'";

				String styleRegistros = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
				"color:#000066; "+
				"padding-top:1px; "+
				"padding-right:2px; "+
				"padding-bottom:1px; "+
				"padding-left:2px; "+
				"height:22px; "+
				"font-size:11px; ";

				String fechaHoy  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

			try {
			 con.conexionDB();
			 
			 String nombreUsuario ="";	
			 
						
			UtilUsr utilUsr = new UtilUsr();
			boolean usuarioEncontrado = false;
			boolean perfilIndicado = false;
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(claveEpo, "E");
			for (int i = 0; i < usuariosPorPerfil.size(); i++) {
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);					
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					if ("EPO VENTANILLA".equals(usuarioEpo.getPerfil())) {
						perfilIndicado = true;
						nombreUsuario = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno();						
					}			
			}
			
				registros  =  IniEnvioCorreo (claveEpo, claveSolicitud);
			
				if (registros.size()>0){
					for(int i = 0; i< registros.size(); i++){	
					 datos = (List)registros.get(i);				
							
				log.debug((String)datos.get(0));
				log.debug((String)datos.get(1));
				log.debug((String)datos.get(2));
					
				StringBuffer mensajeCorreo =  new StringBuffer();
				
				
				
				
				
				mensajeCorreo.append("<html>");
				mensajeCorreo.append("<head>");
				mensajeCorreo.append("<title>");
				mensajeCorreo.append("NOTIFICACION DE CONTRATO DE CESION DE DERECHOS");
				mensajeCorreo.append("</title>");
				mensajeCorreo.append("</head>");

				mensajeCorreo.append("<body>");

				mensajeCorreo.append("<table style=\"text-align:justify\" cellspacing='0' cellpadding='0' border='0' align='left' width='700'>");
				mensajeCorreo.append("<tr><td "+style+">Estimado (a): <b>"+nombreUsuario+"</b></td></tr>");				
				//mensajeCorreo.append("<tr><td "+style+" width='500'>"+comentarios+"</td></tr>");
				//mensajeCorreo.append("</table>");
				
				//mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='0' align='left' width='700'>");
				mensajeCorreo.append("<tr><td "+style+">Aviso a Ventanilla �nica "+"</b></td></tr>");	
				String cedente = (String)datos.get(0)+(((String)datos.get(39)!=null)?";"+(String)datos.get(39):"");
				mensajeCorreo.append("<tr><td "+style+" width='500'>"+" <p>Me permito notificar a usted que con fecha  " +(String)datos.get(35)+ " se notific� por medio del Sistema NAFIN a la Empresa el Contrato de Cesi�n Electr�nica de Derechos de Cobro "+
					               " que celebraron el (Proveedor o Contratista), como cedente(s) ("+cedente.replaceAll(";",",")+")y el (Intermediario Financiero), como cesionario, respecto del  100% (cien por ciento)  de los derechos de cobro que se deriven por la ejecuci�n y cumplimiento del Contrato No."+(String)datos.get(6)+
					               " y cuya documentaci�n se encuentra previamente validada y contenida en el Sistema NAFIN."+
					               
					               " <p> Lo anterior, a efecto de que a partir de la fecha de notificaci�n del referido Contrato de Cesi�n de Derechos de Cobro y hasta en tanto las partes o el Cesionario no notifiquen a la Empresa disposici�n en contrario, esa �rea a su cargo proceda de manera inmediata a realizar todo lo necesario "+
					               " para que todos los pagos que se deriven de la ejecuci�n del referido Contrato cuyos derechos de cobro son materia de cesi�n, se realicen directamente al cesionario en la cuenta e instituci�n bancaria que se se�ala en la cl�usula Tercera del Contrato de Cesi�n de Derechos de Cobro, previas deducciones "+
					               " o retenciones que deban hacerse al cedente en t�rminos del mencionado Contrato."+(String)datos.get(6)+"</td></tr>");
					            
				mensajeCorreo.append("<tr><td "+style+" width='500'>"+comentarios+"</td></tr>");
				//mensajeCorreo.append("</table>");
				
				//mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='0' align='left' width='700'>");
				
				mensajeCorreo.append("<tr><td "+style+" width='500'>"+" <p>No omito se�alar que la documentaci�n soporte relativa a la presente Cesi�n Electr�nica de Derechos de Cobro se encuentra disponible para su consulta y revisi�n en el sistema NAFIN.</td></tr>"+
				"<tr><td "+style+" width='500'>"+"Favor de Checar el Contrato de Cesi�n de Derechos  que viene adjunto al correo en formato PDF. </td></tr>");
				//mensajeCorreo.append("</table>");
				/*mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='1' align='center' width='700'>");
				mensajeCorreo.append("<tr><td "+style+" "+styleEncabezados+" align='center' colspan='2'><b>NOTIFICACI�N:<b></td></tr>");				
				mensajeCorreo.append("<tr><td "+style+" "+styleEncabezados+" align='center' colspan='2'><b>INFORMACION DE LA PYME CEDENTE:<b></td></tr>");				
				mensajeCorreo.append("<tr><td "+style+">NOMBRE: </td><td "+style+" width='500'>"+(String)datos.get(0)+"</td></tr>");
				mensajeCorreo.append("<tr><td "+style+">RFC:</td><td "+style+" width='500'>"+(String)datos.get(1)+"</td></tr>");
			  mensajeCorreo.append("<tr><td "+style+">REPRESENTANTE PyME 1: </td><td "+style+" width='500'>"+(String)datos.get(2)+"</td></tr>");
			  mensajeCorreo.append("<tr><td "+style+">REPRESENTANTE PyME 2: </td><td "+style+" width='500'>"+(String)datos.get(3)+"</td></tr>");
			  mensajeCorreo.append("<tr><td "+style+">NUM. PROVEEDOR: </td><td "+style+" width='500'>"+(String)datos.get(4)+"</td></tr>");
			  mensajeCorreo.append("<tr><td "+style+">FECHA DE SOLICITUD DE CESION: </td><td "+style+" width='500'>"+(String)datos.get(5)+"</td></tr>");
			
				mensajeCorreo.append("<tr><td "+style+" "+styleEncabezados+" align='center' colspan='2'><b>INFORMACION DEL CONTRATO:</b> </td></tr>");				
				mensajeCorreo.append("<tr><td "+style+">NUMERO DE CONTRATO: </td><td "+style+" width='500'>"+(String)datos.get(6)+"</td></tr>");
				//mensajeCorreo.append("<tr><td "+style+">MONTO: </td><td "+style+" width='500'>"+(String)datos.get(7)+"</td></tr>");
			  //mensajeCorreo.append("<tr><td "+style+">MONEDA: </td><td "+style+" width='500'>"+(String)datos.get(8)+"</td></tr>");
				mensajeCorreo.append("<tr><td "+style+">MONTO / MONEDA: </td><td "+style+" width='500'>"+(String)datos.get(7)+"</td></tr>");
			  mensajeCorreo.append("<tr><td "+style+">FECHA DE VIGENCIA: </td><td "+style+" width='500'>"+(datos.get(8).toString().equals("N/A")?"N/A":(datos.get(8) + " a "+ datos.get(9)))+"</td></tr>");
				mensajeCorreo.append("<tr><td "+style+">PLAZO DEL CONTRATO: </td><td "+style+" width='500'>"+(String)datos.get(10)+"</td></tr>");
			  mensajeCorreo.append("<tr><td "+style+">TIPO DE CONTRATACION: </td><td "+style+" width='500'>"+(String)datos.get(11)+"</td></tr>");
			  mensajeCorreo.append("<tr><td "+style+">OBJETO DEL CONTRATO:</td><td "+style+" width='500'>"+(String)datos.get(12)+"</td></tr>");
			  mensajeCorreo.append("<tr><td "+style+">CAMPO ADICIONAL 1: </td><td "+style+" width='500'>"+(String)datos.get(13)+"</td></tr>");
				mensajeCorreo.append("<tr><td "+style+">CAMPO ADICIONAL 2: </td><td "+style+" width='500'>"+(String)datos.get(14)+"</td></tr>");
				mensajeCorreo.append("<tr><td "+style+">CAMPO ADICIONAL 3: </td><td "+style+" width='500'>"+(String)datos.get(15)+"</td></tr>");
				mensajeCorreo.append("<tr><td "+style+">CAMPO ADICIONAL 4: </td><td "+style+" width='500'>"+(String)datos.get(16)+"</td></tr>");
				mensajeCorreo.append("<tr><td "+style+">CAMPO ADICIONAL 5: </td><td "+style+" width='500'>"+(String)datos.get(17)+"</td></tr>");
				
			 mensajeCorreo.append("<tr><td "+style+" "+styleEncabezados+" align='center' colspan='2' ><b>INFORMACION DE LA EMPRESA PRIMER ORDEN: </b></td></tr>");				
			 mensajeCorreo.append("<tr><td "+style+">NOMBRE: </td><td "+style+" width='500'>"+(String)datos.get(18)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">PERSONA QUE AUTORIZO LA CESION: <b>"+"</b></td><td "+style+" width='500'>"+(String)datos.get(19)+"</td></tr>");
			  			
			 mensajeCorreo.append("<tr><td "+style+" "+styleEncabezados+" align='center' colspan='2'><b>INFORMACION DEL CESIONARIO (IF)</b></td></tr>");				
			 mensajeCorreo.append("<tr><td "+style+">NOMBRE: </td><td "+style+" width='500'>"+(String)datos.get(20)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">BANCO DE DEPOSITO: </td><td "+style+" width='500'>"+(String)datos.get(21)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">NUMERO DE CUENTA PARA DEPOSITO: </td><td "+style+" width='500'>"+(String)datos.get(22)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">NUMERO DE CUENTA CLABE PARA DEPOSITO: </td><td "+style+" width='500'>"+(String)datos.get(23)+"</td></tr>");
				 
			 mensajeCorreo.append("<tr><td "+style+" "+styleEncabezados+" align='center' colspan='2'>INFORMACION DE LA CESION DE DERECHOS </td></tr>");				
			 mensajeCorreo.append("<tr><td "+style+">FECHA DE LA SOLICITUD DE CONSENTIMIENTO: </td><td "+style+" width='500'>"+(String)datos.get(24)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">FECHA DE CONSENTIMIENTO DE LA EPO: </td><td "+style+" width='500'>"+(String)datos.get(25)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">PERSONA QUE OTORGO EL CONSENTIMIENTO: </td><td "+style+" width='500'>"+(String)datos.get(26)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">FECHA DE FORMALIZACION DEL CONTRATO DE CESION: </td><td "+style+" width='500'>"+(String)datos.get(27)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">NOMBRE DEL FIRMANTE DE CEDENTE (PYME): </td><td "+style+" width='500'>"+(String)datos.get(28)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">NOMBRE DEL FIRMANTE DEL CESIONARIO (IF): </td><td "+style+" width='500'>"+(String)datos.get(29)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">NOMBRE DEL TESTIGO 1 DEL CESIONARIO: </td><td "+style+" width='500'>"+(String)datos.get(30)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">NOMBRE DEL TESTIGO 2 DEL CESIONARIO: </td><td "+style+" width='500'>"+(String)datos.get(31)+"</td></tr>");
			 
			String fecha=(String)datos.get(32); 
			if(fecha.equals(" ")){
			fecha = fechaHoy;
			}
			 mensajeCorreo.append("<tr><td "+style+">FECHA DE LA NOTIFICACION ACEPTADA POR LA EPO: </td><td "+style+" width='500'>"+fecha+"</td></tr>");
			 String persona=(String)datos.get(33); 
				if(persona.equals(" ")){
				persona = nombre;
				}
			 mensajeCorreo.append("<tr><td "+style+">PERSONA QUE ACEPTO LA NOTIFICACION: </td><td "+style+" width='500'>"+persona+"</td></tr>");
			 String fechapagos=(String)datos.get(34); 
				if(fechapagos.equals("")){
				fechapagos = fechaHoy;
			}		
			 mensajeCorreo.append("<tr><td "+style+">FECHA DE INFORMACION A VENTANILLA DE PAGOS: </td><td "+style+" width='500'>"+fechapagos+"</td></tr>");
			 String fechaVentanilla=(String)datos.get(35); 
				if(fechaVentanilla.equals("")){
				fechaVentanilla = fechaHoy;
				}
			 mensajeCorreo.append("<tr><td "+style+">FECHA DE RECEPCION EN VENTANILLA: </td><td "+style+" width='500'>"+fechaVentanilla+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">FECHA DE APLICACION DEL REDIRECCIONAMIENTO: </td><td "+style+" width='500'>"+(String)datos.get(36)+"</td></tr>");
			 mensajeCorreo.append("<tr><td "+style+">PERSONA QUE REDIRECCIONA LA CUENTA DE PAGO: </td><td "+style+" width='500'>"+(String)datos.get(37)+"</td></tr>");
			 mensajeCorreo.append("</table>");
			 */
			 
			//mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='0' align='left' width='700'>");
			mensajeCorreo.append("<tr><td "+style+" width='500'>"+"Para consultarla favor de ingresar al sitio de Cadenas Productivas en http://www.nafin.com"+"</td></tr>");
			mensajeCorreo.append("<tr><td "+style+" width='500'>"+"Atentamente"+"</td></tr>");
		  mensajeCorreo.append("<tr><td "+style+" width='500'>"+"Nacional Financiera  S.N.C."+"</td></tr>");
			mensajeCorreo.append("</table>");
					
			
			mensajeCorreo.append("</body>");
			mensajeCorreo.append("</html>");
			
			mensajeCorreo2 = this.convierteAcentosACodigoHTML(mensajeCorreo.toString()); 
				
					}
				}
				
			 Correo correo = new Correo();
				
				if(!correoPara.equals("")){

					List correos = new VectorTokenizer(correoPara, "\n").getValuesVector();
		
					for(int i = 0; i < correos.size(); i++) 	{

						if(correos.get(i)!=null){						
						
								List unCorreo = new VectorTokenizer((String)correos.get(i), ";").getValuesVector();
								
								for(int e = 0; e < unCorreo.size(); e++) 	{	
										
										String unCorreo1   = (unCorreo.size()>= 1)?unCorreo.get(e)==null?"":((String)unCorreo.get(e)).trim():"";				 
										
										log.debug("unCorreo---------------> "+unCorreo1);
										if(!unCorreo1.equals("")){
											//correo.enviarTextoHTML("cadenas@nafin.gob.mx", unCorreo1, "NOTIFICACION DE CONTRATO DE CESION DE DERECHOS", mensajeCorreo2);
											ArrayList listaArchivos = new ArrayList();
											HashMap 			archivoAdjunto 		 = new HashMap();
											archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );//applicationPath + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif");
											listaArchivos.add(archivoAdjunto);
											
											correo.enviaCorreoConDatosAdjuntos("cadenas@nafin.gob.mx", unCorreo1,"", "NOTIFICACION DE CONTRATO DE CESION DE DERECHOS", mensajeCorreo2,null,listaArchivos);
											
										}
								}
						}
						
					}
				}
				
				qrySentencia = new StringBuffer();
				lvarbind = new ArrayList();
			  qrySentencia.append(" UPDATE  cder_solicitud ");
				qrySentencia.append(" SET IC_VENTANILLA   = ?,  ");	
				qrySentencia.append(" CG_CORREO    = ? ");					
				qrySentencia.append(" WHERE ic_solicitud = ? ");
				
							
				log.debug(qrySentencia.toString());
				log.debug( lvarbind);
				lvarbind.add(ventanilla);	
				lvarbind.add("S");	//bandera para saber si fue enviado el correo
				lvarbind.add(claveSolicitud);							
					
				ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);
				ps.executeUpdate();
				ps.close();	
				
			respuesta = "E"; 
		
		}catch(Exception e){
			respuesta = "N"; 
			log.error("EnviodeCorreo (Exception) " + e);			
			throw new AppException("EnviodeCorreo(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("EnviodeCorreo (E)");		
		return respuesta;	
}


	private String convierteAcentosACodigoHTML(String cadena){
		String resultado = cadena;
		resultado = resultado.replaceAll("�","&aacute;");
		resultado = resultado.replaceAll("�","&eacute;");
		resultado = resultado.replaceAll("�","&iacute;");
		resultado = resultado.replaceAll("�","&oacute;");
		resultado = resultado.replaceAll("�","&uacute;");
		resultado = resultado.replaceAll("�","&Aacute;");
		resultado = resultado.replaceAll("�","&Eacute;");
		resultado = resultado.replaceAll("�","&Iacute;");
		resultado = resultado.replaceAll("�","&Oacute;");
		resultado = resultado.replaceAll("�","&Uacute;");
		resultado = resultado.replaceAll("�","&ntilde;");
		resultado = resultado.replaceAll("�","&Ntilde;");
		resultado = resultado.replaceAll("�","&uuml;");
		resultado = resultado.replaceAll("�","&Uuml;");
		return resultado;
	}
	
/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param noEpo
	 */
public String  CorreoUsuario (String noEpo)	throws AppException	{

		log.info("CorreoUsuario (E)");
	
			PreparedStatement ps = null;
			ResultSet rs        = null;
			AccesoDB	 con 	     =   new AccesoDB();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			List lvarbind = new ArrayList();
			String correo ="";
			try {
			 con.conexionDB();

				/*qrySentencia.append(" select cg_email from com_contacto   ");
				qrySentencia.append("where ic_epo = ?  ");
				qrySentencia.append(" and cs_primer_contacto = 'S'   ");

				lvarbind.add(noEpo);
				ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);
				rs = ps.executeQuery();

				if(rs.next()){
					correo = rs.getString("cg_email")==null?"":rs.getString("cg_email");
				}
			rs.close();
			ps.close();
			*/

			 
			UtilUsr utilUsr = new UtilUsr();
			boolean usuarioEncontrado = false;
			boolean perfilIndicado = false;
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noEpo, "E");
			for (int i = 0; i < usuariosPorPerfil.size(); i++) {
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);					
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					if ("EPO VENTANILLA".equals(usuarioEpo.getPerfil())) {
						perfilIndicado = true;
						correo = usuarioEpo.getEmail();						
					}			
			}
			log.debug("correo  "+correo);
			
			
	
		
		}catch(Exception e){
			log.error("CorreoUsuario (Exception) " + e);			
			throw new AppException("CorreoUsuario(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("CorreoUsuario (S)");		
		return correo;	
		
	}
	
	/**
	 * Este metodo registra en el campo CDER_SOLICITUD.CS_NOTIF_PROVEEDOR que el proveedor
	 * ha sido notificado de la solicitud.
	 *
	 * @param claveSolicitud <tt>String</tt> con la clave de la solicitud que fue notificada.
	 *
	 */
	public void setProveedorNotificado(String claveSolicitud)
		throws AppException{
		
		log.info("setProveedorNotificado(E)");
		if(claveSolicitud == null || claveSolicitud.trim().equals("")){
			log.info("setProveedorNotificado(S)");
			return;
		}
		claveSolicitud = claveSolicitud.trim();
		
		AccesoDB 			con 				= new AccesoDB();
		StringBuffer		query 			= new StringBuffer();
		PreparedStatement ps 				= null;
		boolean 				exitoOperacion = true;
		
		try {
			con.conexionDB();
			
			query.append(
				"UPDATE 							"  +
				"	CDER_SOLICITUD 			"  +
				"SET 								"  +
				"	CS_NOTIF_PROVEEDOR = ? 	"  + // 'S'
				"WHERE 							"  +
				"	IC_SOLICITUD = ? 			" 
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,	"S");
			ps.setInt(2, 		Integer.parseInt(claveSolicitud));
			ps.executeUpdate();
			ps.close();
			
		}catch(Exception e){
			log.error("setProveedorNotificado(Exception)");
			log.error("setProveedorNotificado.claveSolicitud = <"+claveSolicitud+">");
			e.printStackTrace();
			exitoOperacion = false;	
			throw new AppException("Error al registrar envio exitoso de notificacion de solicitud al proveedor.");
		}finally{
			if (ps != null) { try { ps.close();}catch(Exception e){} }
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exitoOperacion);
				con.cierraConexionDB();
			}
			log.info("setProveedorNotificado(S)");
		}
		
	}
	
	/**
	 * Este metodo resetea el campo CDER_SOLICITUD.CS_NOTIF_PROVEEDOR donde se registra la notificacion exitosa de la solicitud al proveedor.
	 * Se emplea cuando se Reactiva la Solicitud.
	 *
	 * @param claveSolicitud <tt>String</tt> con la clave de la solicitud que fue notificada.
	 *
	 */
	public void resetNotificacionProveedor(String claveSolicitud)
		throws AppException{
		
		log.info("resetNotificacionProveedor(E)");
		if(claveSolicitud == null || claveSolicitud.trim().equals("")){
			log.info("resetNotificacionProveedor(S)");
			return;
		}
		claveSolicitud = claveSolicitud.trim();
		
		AccesoDB 			con 				= new AccesoDB();
		StringBuffer		query 			= new StringBuffer();
		PreparedStatement ps 				= null;
		boolean 				exitoOperacion = true;
		
		try {
			con.conexionDB();
			
			query.append(
				"UPDATE 							"  +
				"	CDER_SOLICITUD 			"  +
				"SET 								"  +
				"	CS_NOTIF_PROVEEDOR = ? 	"  + // 'N'
				"WHERE 							"  +
				"	IC_SOLICITUD = ? 			" 
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,	"N");
			ps.setInt(2, 		Integer.parseInt(claveSolicitud));
			ps.executeUpdate();
			ps.close();
			
		}catch(Exception e){
			log.error("resetNotificacionProveedor(Exception)");
			log.error("resetNotificacionProveedor.claveSolicitud = <"+claveSolicitud+">");
			e.printStackTrace();
			exitoOperacion = false;	
			throw new AppException("Error al resetear estatus de notificacion de solicitud al proveedor.");
		}finally{
			if (ps != null) { try { ps.close();}catch(Exception e){} }
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exitoOperacion);
				con.cierraConexionDB();
			}
			log.info("resetNotificacionProveedor(S)");
		}
		
	}
	
	/**
	 * Este metodo sirve para consultar si el proveedor ya ha sido notificado por correo de la solicitud
	 *
	 * @param claveSolicitud <tt>String</tt> con la clave de la solicitud que fue notificada.
	 *
	 */
	public boolean proveedorNotificado(String claveSolicitud)
		throws AppException{
			
		log.info("proveedorNotificado(E)");
		if(claveSolicitud == null || claveSolicitud.trim().equals("")){
			log.info("proveedorNotificado(S)");
			return false;
		}
		claveSolicitud = claveSolicitud.trim();
		
		AccesoDB 			con 				= new AccesoDB();
		StringBuffer		query 			= new StringBuffer();
		PreparedStatement ps 				= null;
		ResultSet			rs					= null;
		boolean				notificado		= false;
		
		try {
			con.conexionDB();
			
			query.append(
				"SELECT 															"  +
				"	DECODE(CS_NOTIF_PROVEEDOR,'S','true','false') 	"  +
				"		AS NOTIFICACION_PROVEEDOR							"  +
				"FROM 															"  +
				"	CDER_SOLICITUD 											"  +
				"WHERE 															"  +
				"	IC_SOLICITUD = ? 											"
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, 	Integer.parseInt(claveSolicitud) );
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()){
				notificado = "true".equals(rs.getString("NOTIFICACION_PROVEEDOR"))?true:false;
			}
			
		}catch(Exception e){
			log.info("proveedorNotificado(Exception)");
			log.info("proveedorNotificado.claveSolicitud = <"+claveSolicitud+">");
			e.printStackTrace();
			notificado = false;
			throw new AppException("Error al consultar estatus de notificacion del proveedor.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("proveedorNotificado(S)");
		}
		
		return notificado;
	}
 
	/**
	 * para verificar si ya fue enviado el correo
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param claveSolicitud
	 */
	public String  banderaCorreo (String claveSolicitud)	throws AppException	{

		log.info("banderaCorreo (E)");
	
			PreparedStatement ps = null;			
			ResultSet rs        = null;
			AccesoDB	 con 	     =   new AccesoDB();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			List lvarbind = new ArrayList();
			String correo ="N";
			
			log.debug("claveSolicitud " + claveSolicitud);	
			try {
			 con.conexionDB();
			 
			String qry2 = "select CG_CORREO from cder_solicitud "+ 
											" where ic_solicitud = "+claveSolicitud;
			log.debug("..: qry2: "+qry2);
			ps = con.queryPrecompilado(qry2);
			rs = ps.executeQuery();
			if(rs.next())	{
				correo = rs.getString("CG_CORREO")==null?"N":rs.getString("CG_CORREO");
			}else{
				correo = "N";
			}
			ps.close();
			rs.close();
			log.debug("correo " + correo);		
		
		}catch(Exception e){
			log.error("CorreoUsuario (Exception) " + e);			
			throw new AppException("CorreoUsuario(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("banderaCorreo (S)");		
		return correo;	
		
	}
  /**
	 * M�todo que obtiene las monedas y montos parametrizados por solicitud.
	 * @param clave_solicitud Clave de la solicitud de Cesi�n de Derechos.
	 * @return montoMoneda StringBuffer con el valor obtenido de la base de datos.
	 * @throws AppException Cuando ocurre alg�n error al realizar la consulta.
	 */
	public HashMap getMontosMonedasSolicitud(String claveSolicitud) throws AppException{
		log.info("getMontosMonedasSolicitud(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap montosMonedasSolicitud = new HashMap();
    
		try{
			con.conexionDB();

			strSQL.append(" SELECT ccm.ic_moneda AS clave_moneda,");
			strSQL.append(" ccm.cd_nombre as nombre_moneda,");
			strSQL.append(" cms.fn_monto_moneda AS monto,");
			strSQL.append(" cms.fn_monto_moneda_min AS monto_min,");
			strSQL.append(" cms.fn_monto_moneda_max AS monto_max");
			strSQL.append(" FROM cder_monto_x_solic cms");
			strSQL.append(", comcat_moneda ccm");
			strSQL.append(" WHERE ccm.ic_moneda = cms.ic_moneda");
			strSQL.append(" AND cms.ic_solicitud = ?");

      varBind.add(new Integer(claveSolicitud));
						
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();			
      
			int i = 0;
			while (rst.next()) {
				HashMap montosMonedas = new HashMap();
				montosMonedas.put("claveMoneda", rst.getString("clave_moneda")==null?"":rst.getString("clave_moneda"));
				montosMonedas.put("nombreMoneda", rst.getString("nombre_moneda")==null?"":rst.getString("nombre_moneda"));
				montosMonedas.put("montoContrato", rst.getString("monto")==null?"":Comunes.formatoDecimal(rst.getString("monto"), 2));
				montosMonedas.put("montoContratoMin", rst.getString("monto_min")==null?"":Comunes.formatoDecimal(rst.getString("monto_min"), 2));
				montosMonedas.put("montoContratoMax", rst.getString("monto_max")==null?"":Comunes.formatoDecimal(rst.getString("monto_max"), 2));
				montosMonedasSolicitud.put("montosMonedas" + i, montosMonedas);
				i++;
			}
			
			montosMonedasSolicitud.put("indice", Integer.toString(i));
      
      
			rst.close();
			pst.close();
		} catch(Exception e) {
			throw new AppException("Error al consultar en la base de datos: ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getMontosMonedasSolicitud(S)");
		}
		return montosMonedasSolicitud;
	}
	
	/**
	 * M�todo que envia el correo de redireccionamiento de las ventanillas de la EPO.
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param comentarios
	 * @param correoPara
	 * @param correoDe
	 * @param claveSolicitud
	 */
	public void enviarCorreoRedireccionamiento(HashMap parametrosEnvioCorreo) throws AppException	{
		log.info("enviarCorreoRedireccionamiento(E)");
		Correo correo = new Correo();
		try {
			String claveEpo = parametrosEnvioCorreo.get("claveEpo")==null?"":parametrosEnvioCorreo.get("claveEpo").toString();
			String claveSolicitud = parametrosEnvioCorreo.get("claveSolicitud")==null?"":parametrosEnvioCorreo.get("claveSolicitud").toString();
			String nombreRemitente = parametrosEnvioCorreo.get("nombreRemitente")==null?"":parametrosEnvioCorreo.get("nombreRemitente").toString();
			String nombreDestinatario = parametrosEnvioCorreo.get("nombreDestinatario")==null?"":parametrosEnvioCorreo.get("nombreDestinatario").toString();
			String correoDestinatario = parametrosEnvioCorreo.get("correoDestinatario")==null?"":parametrosEnvioCorreo.get("correoDestinatario").toString();
			String comentarios = parametrosEnvioCorreo.get("comentarios")==null?"":parametrosEnvioCorreo.get("comentarios").toString();
			String numeroContrato = parametrosEnvioCorreo.get("numeroContrato")==null?"":parametrosEnvioCorreo.get("numeroContrato").toString();
			String nombreArchivo = parametrosEnvioCorreo.get("nombreArchivo")==null?"":parametrosEnvioCorreo.get("nombreArchivo").toString();
			if (claveEpo.equals("") || claveSolicitud.equals("") || nombreRemitente.equals("") || correoDestinatario.equals("")) {
				log.error("..:: parametrosEnvioCorreo: "+parametrosEnvioCorreo);
				throw new AppException("Los par�metros de env�o de correo de redireccionamiento no pueden ser nulos.");
			}		
			ArrayList listaArchivos = new ArrayList();
			HashMap 			archivoAdjunto 		 = new HashMap();
			archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );//applicationPath + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif");
			List registros = this.IniEnvioCorreo(claveEpo, claveSolicitud);
			listaArchivos.add(archivoAdjunto);
			if (registros.size() > 0 && !correoDestinatario.equals("")) {
				correo.enviaCorreoConDatosAdjuntos("cadenas@nafin.gob.mx", correoDestinatario,"", "NOTIFICACION DE REDIRECCIONAMIENTO APLICADO", this.generaMensajeCorreoVentanilla(nombreRemitente, nombreDestinatario, comentarios, registros,numeroContrato).toString(),null,listaArchivos);
			}
		} catch(Exception e) {
			log.error("enviarCorreoRedireccionamiento(Error)");
			throw new AppException("Ocurri� un error al enviar el correo de redireccionamiento.", e);
		} finally {
			log.info("enviarCorreoRedireccionamiento(S)");		
		}
	}
	
	/**
	 * M�todo que genera cuerpo del mensaje del correo de las ventanillas.
	 * @return mensajeCorreo StringBuffer con el cuerpo del mensaje.
	 */
	 private StringBuffer generaMensajeCorreoVentanilla(String nombreRemitente, String nombreDestinatario, String comentarios, List registros,String numeroContrato) {
		log.info("generaMensajeCorreoVentanilla(E)");
		StringBuffer mensajeCorreo =  new StringBuffer();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		try {
			String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
			"font-size: 10px; "+
			"font-weight: bold;"+
			"color: #FFFFFF;"+
			"background-color: #4d6188;"+
			"padding-top: 3px;"+
			"padding-right: 1px;"+
			"padding-bottom: 1px;"+
			"padding-left: 3px;"+
			"height: 25px;"+
			"border: 1px solid #1a3c54;'";

			String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
			"color:#000066; "+
			"padding-top:1px; "+
			"padding-right:2px; "+
			"padding-bottom:1px; "+
			"padding-left:2px; "+
			"height:22px; "+
			"font-size:11px;'";
			
			String styleRegistros = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
			"color:#000066; "+
			"padding-top:1px; "+
			"padding-right:2px; "+
			"padding-bottom:1px; "+
			"padding-left:2px; "+
			"height:22px; "+
			"font-size:11px; ";

			for (int i = 0; i < registros.size(); i++) {
				List datos = (List)registros.get(i);
		
				mensajeCorreo.append("<html>");
				mensajeCorreo.append("<head>");
				mensajeCorreo.append("<title>");
				mensajeCorreo.append("NOTIFICACION DE CONTRATO DE CESION DE DERECHOS");
				mensajeCorreo.append("</title>");
				mensajeCorreo.append("</head>");
				
				mensajeCorreo.append("<body>");
				
				mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='0' align='left' width='700'>");
				mensajeCorreo.append("<tr><td "+style+">&nbsp;</td></tr>");
				if (!numeroContrato.equals("")) {
					mensajeCorreo.append("<tr><td "+style+">Redireccionamiento aplicado respecto al Contrato No. <b>"+numeroContrato+"</b></td></tr>");
				}
				mensajeCorreo.append("<tr><td "+style+" width='500'>"+comentarios+"</td></tr>");
				//mensajeCorreo.append("</table>");
				
				//mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='0' align='left' width='700'>");
				mensajeCorreo.append("<tr><td "+style+">&nbsp;</td></tr>");
				
				mensajeCorreo.append("<tr><td "+style+" width='500'>Favor de Checar el Contrato de Cesi�n de Derechos que viene adjunto al correo en formato PDF.</td></tr>");
				//mensajeCorreo.append("</table>");
				
				
				//mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='0' align='left' width='700'>");
				mensajeCorreo.append("<tr><td "+style+" width='500'>"+"Por consultarla favor de ingresar al sitio de Cadenas Productivas en http://www.nafin.com"+"</td></tr>");
				mensajeCorreo.append("<tr><td "+style+" width='500'>"+"Atentamente"+"</td></tr>");
				mensajeCorreo.append("<tr><td "+style+" width='500'>"+"Nacional Financiera  S.N.C."+"</td></tr>");
				mensajeCorreo.append("</table>");
				mensajeCorreo.append("</body>");
				mensajeCorreo.append("</html>");
			}
		} catch (Exception e) {
			log.error("");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al generar el cuerpor del mensaje.", e);
		} finally {
			log.info("generaMensajeCorreoVentanilla(S)");
		}
		return new StringBuffer(this.convierteAcentosACodigoHTML(mensajeCorreo.toString())); 
	 }
	
	//---------------------------------------------------------------------------- FODEA 030 - 2011 ACF - I
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Contratos de Cesi�n para la pyme.
	 * @param parametros_consulta HashMap con los parametros con los que se realiza la consulta de contratos de cesi�n.
	 * @return consultaContratosCesion Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultarExtincionContratosPyme(HashMap parametrosConsulta) throws AppException{
		log.info("consultarExtincionContratosPyme(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaContratosCesion = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();
			
			String clavePyme = parametrosConsulta.get("clavePyme")==null?"":(String)parametrosConsulta.get("clavePyme");
			String claveEpo = parametrosConsulta.get("claveEpo")==null?"":(String)parametrosConsulta.get("claveEpo");
			String claveIfCesionario = parametrosConsulta.get("claveIfCesionario")==null?"":(String)parametrosConsulta.get("claveIfCesionario");
			String numeroContrato = parametrosConsulta.get("numeroContrato")==null?"":(String)parametrosConsulta.get("numeroContrato");
			String claveMoneda = parametrosConsulta.get("claveMoneda")==null?"":(String)parametrosConsulta.get("claveMoneda");
			String claveTipoContratacion = parametrosConsulta.get("claveTipoContratacion")==null?"":(String)parametrosConsulta.get("claveTipoContratacion");

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,"); 
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" sol.ic_tipo_contratacion AS clave_tipo_contratacion,");
			strSQL.append(" epo.cg_razon_social AS dependencia,");
			strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" con.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cl.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
			strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
			strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
			strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
			strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");
			strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Por Solicitar', es.cg_descripcion) AS estatus_solicitud,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", cdercat_clasificacion_epo cl");
			strSQL.append(", cdercat_tipo_contratacion con");
			strSQL.append(", cdercat_estatus_solic es");
			strSQL.append(", cdercat_tipo_plazo stp");
			strSQL.append(", cder_extincion_contrato extc");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if ");
			strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
			strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
			strSQL.append(" AND sol.ic_estatus_solic IN (?, ?)");
			varBind.add(new Integer(17));//17.- Redireccionamiento Aplicado
			varBind.add(new Integer(18));//18.- Extinci�n Pre-Solicitada
		
			if (clavePyme != null && !"".equals(clavePyme)) {
				strSQL.append(" AND sol.ic_pyme = ?");
				varBind.add(clavePyme);
			}
		
			if (claveEpo != null && !"".equals(claveEpo)) {
				strSQL.append(" AND sol.ic_epo = ?");
				varBind.add(new Integer(claveEpo));
			}
		
			if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
				strSQL.append(" AND sol.ic_if = ?");
				varBind.add(new Integer(claveIfCesionario));
			}
		
			if (numeroContrato != null && !"".equals(numeroContrato)) {
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(numeroContrato);
			}
		
			if (claveMoneda != null && !"".equals(claveMoneda)) {
				if (claveMoneda.equals("0")) {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("S");
				} else {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.ic_moneda = ?)");
					varBind.add("N");
					varBind.add(new Integer(claveMoneda));
				}
			}
		
			if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
				strSQL.append(" AND sol.ic_tipo_contratacion = ?");
				varBind.add(new Integer(claveTipoContratacion));
			}	
			strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while (rst.next()) {
				String strClaveEpo = rst.getString("clave_epo");
				StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));
			
				HashMap contratoCesion = new HashMap();
				contratoCesion.put("claveSolicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				contratoCesion.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				contratoCesion.put("clavePyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				contratoCesion.put("claveTipoContratacion", rst.getString("clave_tipo_contratacion")==null?"":rst.getString("clave_tipo_contratacion"));
				contratoCesion.put("nombreEpo", rst.getString("dependencia")==null?"":rst.getString("dependencia"));
				contratoCesion.put("nombreIfCesionario", rst.getString("nombre_cesionario")==null?"":rst.getString("nombre_cesionario"));
				contratoCesion.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				contratoCesion.put("montosPorMoneda", montosPorMoneda.toString());
				contratoCesion.put("tipoContratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				contratoCesion.put("fechaInicioContrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				contratoCesion.put("fechaFinContrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				contratoCesion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				contratoCesion.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				contratoCesion.put("campoAdicional1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				contratoCesion.put("campoAdicional2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				contratoCesion.put("campoAdicional3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				contratoCesion.put("campoAdicional4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				contratoCesion.put("campoAdicional5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				contratoCesion.put("objetoContrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				contratoCesion.put("fechaExtincion", rst.getString("fecha_extincion")==null?"":rst.getString("fecha_extincion"));
				contratoCesion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				contratoCesion.put("cuentaClabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				contratoCesion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				contratoCesion.put("estatusSolicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				contratoCesion.put("claveEstatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				contratoCesion.put("causasRechazo", rst.getString("causas_rechazo")==null?"":rst.getString("causas_rechazo"));

				consultaContratosCesion.put("contratoCesion"+indice, contratoCesion);
				indice++;
			}
			
			consultaContratosCesion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		} catch(Exception e) {
			log.error("Error al consultar base de datos");
			e.printStackTrace();
			throw new AppException("Error al consultar los contratos para extinci�n.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultarExtincionContratosPyme(S)");
		}
		return consultaContratosCesion;
	}
	
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Contratos de Cesi�n para la pyme.
	 * @param parametros_consulta HashMap con los parametros con los que se realiza la consulta de contratos de cesi�n.
	 * @return consultaContratosCesion Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultarExtincionContratosPyme(String claveSolicitud) throws AppException{
		log.info("consultarExtincionContratosPyme(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap contratoCesion = new HashMap();
		String numeroAcuse = "";
		int indice = 0;

		try {
			con.conexionDB();

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,"); 
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" sol.ic_tipo_contratacion AS clave_tipo_contratacion,");
			strSQL.append(" epo.cg_razon_social AS dependencia,");
			strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" con.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cl.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
			strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
			strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
			strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
			strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");
			strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Por Solicitar', es.cg_descripcion) AS estatus_solicitud,");
			strSQL.append(" extc.cc_acuse_ext_rep1 AS cc_acuse_ext_rep1,");
			strSQL.append(" extc.cc_acuse_ext_rep2 AS cc_acuse_ext_rep2,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus, ");
			strSQL.append(" cpy.CG_RAZON_SOCIAL ||';'|| sol.CG_EMPRESAS_REPRESENTADAS AS EMPRESAS ");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", cdercat_clasificacion_epo cl");
			strSQL.append(", cdercat_tipo_contratacion con");
			strSQL.append(", cdercat_estatus_solic es");
			strSQL.append(", cdercat_tipo_plazo stp");
			strSQL.append(", cder_extincion_contrato extc");
			strSQL.append(", comcat_pyme cpy");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_pyme = cpy.ic_pyme ");
			strSQL.append(" AND sol.ic_if = cif.ic_if ");
			strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
			strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
			strSQL.append(" AND sol.ic_solicitud = ?");

			varBind.add(new Integer(claveSolicitud));

			strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while (rst.next()) {
				String strClaveEpo = rst.getString("clave_epo");
				StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

				contratoCesion.put("claveSolicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				contratoCesion.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				contratoCesion.put("clavePyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				contratoCesion.put("claveTipoContratacion", rst.getString("clave_tipo_contratacion")==null?"":rst.getString("clave_tipo_contratacion"));
				contratoCesion.put("nombreEpo", rst.getString("dependencia")==null?"":rst.getString("dependencia"));
				contratoCesion.put("nombreIfCesionario", rst.getString("nombre_cesionario")==null?"":rst.getString("nombre_cesionario"));
				contratoCesion.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				contratoCesion.put("montosPorMoneda", montosPorMoneda.toString());
				contratoCesion.put("tipoContratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				contratoCesion.put("fechaInicioContrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				contratoCesion.put("fechaFinContrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				contratoCesion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				contratoCesion.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				contratoCesion.put("campoAdicional1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				contratoCesion.put("campoAdicional2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				contratoCesion.put("campoAdicional3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				contratoCesion.put("campoAdicional4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				contratoCesion.put("campoAdicional5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				contratoCesion.put("objetoContrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				contratoCesion.put("fechaExtincion", rst.getString("fecha_extincion")==null?"":rst.getString("fecha_extincion"));
				contratoCesion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				contratoCesion.put("cuentaClabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				contratoCesion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				contratoCesion.put("estatusSolicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				contratoCesion.put("claveEstatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				contratoCesion.put("causasRechazo", rst.getString("causas_rechazo")==null?"":rst.getString("causas_rechazo"));
				contratoCesion.put("EMPRESAS", rst.getString("EMPRESAS")==null?"":rst.getString("EMPRESAS"));
				
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE EXTINCI�N POR EL PRIMER REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("cc_acuse_ext_rep1")==null?"":rst.getString("cc_acuse_ext_rep1");
				if (!numeroAcuse.equals("")) {
					contratoCesion.put("acuseExtRep1", rst.getString("cc_acuse_ext_rep1")==null?"":rst.getString("cc_acuse_ext_rep1"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						contratoCesion.put("claveUsrExtRep1", datosAcuse.get("claveUsuario"));
						contratoCesion.put("nombreUsrExtRep1", datosAcuse.get("nombreUsuario"));
						contratoCesion.put("fechaExtRep1", datosAcuse.get("fechaAcuse"));
						contratoCesion.put("horaExtRep1", datosAcuse.get("horaAcuse"));
					}
				} else{
						contratoCesion.put("acuseExtRep1",     "");
						contratoCesion.put("claveUsrExtRep1",  "");
						contratoCesion.put("nombreUsrExtRep1", "");
						contratoCesion.put("fechaExtRep1",     "");
						contratoCesion.put("horaExtRep1",      "");
				}
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE EXTINCI�N POR EL SEGUNDO REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("cc_acuse_ext_rep2")==null?"":rst.getString("cc_acuse_ext_rep2");
				if (!numeroAcuse.equals("")) {
					contratoCesion.put("acuseExtRep2", rst.getString("cc_acuse_ext_rep2")==null?"":rst.getString("cc_acuse_ext_rep2"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						contratoCesion.put("claveUsrExtRep2", datosAcuse.get("claveUsuario"));
						contratoCesion.put("nombreUsrExtRep2", datosAcuse.get("nombreUsuario"));
						contratoCesion.put("fechaExtRep2", datosAcuse.get("fechaAcuse"));
						contratoCesion.put("horaExtRep2", datosAcuse.get("horaAcuse"));
					}
				} else{
						contratoCesion.put("acuseExtRep2",     "");
						contratoCesion.put("claveUsrExtRep2",  "");
						contratoCesion.put("nombreUsrExtRep2", "");
						contratoCesion.put("fechaExtRep2",     "");
						contratoCesion.put("horaExtRep2",      "");
				}
				
				indice++;
			}
			
			contratoCesion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		} catch(Exception e) {
			log.error("Error al consultar base de datos");
			e.printStackTrace();
			throw new AppException("Error al consultar los contratos para extinci�n.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultarExtincionContratosPyme(S)");
		}
		return contratoCesion;
	}
	
	/**
	 * M�todo que obtiene las solicitudes que han sido enviadas por un usuario de la pyme.
	 * @param parametrosConsulta Parametros que vienen de la pantalla de consulta.
	 * @return solicitudesEnviadasPorUsuario HashMap con las solicitudes que han sido enviadas por un usuario.
	 * @throws AppException Cuando ocurre un error al actualizar el registro.
	 * @author Alberto Cruz Flores.
	 */
	public HashMap obtenerExtincionesPorUsuario(HashMap parametrosConsulta)	throws AppException	{
		log.info("obtenerExtincionesPorUsuario(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap extincionesPorUsuario = new HashMap();
		
		try {
			con.conexionDB();
			
			String loginUsuario          = parametrosConsulta.get("loginUsuario")          ==null?"":(String)parametrosConsulta.get("loginUsuario");
			String clavePyme             = parametrosConsulta.get("clavePyme")             ==null?"":(String)parametrosConsulta.get("clavePyme");
			String claveEpo              = parametrosConsulta.get("claveEpo")              ==null?"":(String)parametrosConsulta.get("claveEpo");
			String claveIfCesionario     = parametrosConsulta.get("claveIfCesionario")     ==null?"":(String)parametrosConsulta.get("claveIfCesionario");
			String numeroContrato        = parametrosConsulta.get("numeroContrato")        ==null?"":(String)parametrosConsulta.get("numeroContrato");
			String claveMoneda           = parametrosConsulta.get("claveMoneda")           ==null?"":(String)parametrosConsulta.get("claveMoneda");
			String claveTipoContratacion = parametrosConsulta.get("claveTipoContratacion") ==null?"":(String)parametrosConsulta.get("claveTipoContratacion");
			String estatusSolicitud      = parametrosConsulta.get("estatusSolicitud")      ==null?"":(String)parametrosConsulta.get("estatusSolicitud");
			String perfilUsuario         = parametrosConsulta.get("perfilUsuario")         ==null?"":(String)parametrosConsulta.get("perfilUsuario");
			
			if (perfilUsuario.equals("")) {
        log.error("..:: perfilUsuario: "+perfilUsuario);
				throw new AppException("Los par�metros son obligatorios.");
			}
		
			if (perfilUsuario.equalsIgnoreCase("ADMIN PYME") || perfilUsuario.equalsIgnoreCase("ADMIN CESION")){

				System.out.println("ADMIN PYME");
				strSQL.append("SELECT SOL.IC_SOLICITUD AS NUMERO_SOLICITUD "  );
				strSQL.append("FROM CDER_SOLICITUD SOL, "                     );
				strSQL.append("CDER_ACUSE CAC, "                              );
				strSQL.append("CDER_SOLIC_X_REPRESENTANTE REP "               );
				strSQL.append("WHERE SOL.IC_SOLICITUD = REP.IC_SOLICITUD "    );
				strSQL.append("AND REP.CC_ACUSE_EXTINCION_REP = CAC.CC_ACUSE ");
				strSQL.append("AND CAC.CG_USUARIO = ? "                       );

				varBind.add(loginUsuario);

				if (clavePyme != null && !"".equals(clavePyme)){
					strSQL.append("AND REP.IC_PYME = ? ");
					varBind.add(clavePyme);
				}
				if (claveEpo != null && !"".equals(claveEpo)){
					strSQL.append("AND SOL.IC_EPO = ? ");
					varBind.add(new Integer(claveEpo));
				}
				if (claveIfCesionario != null && !"".equals(claveIfCesionario)){
					strSQL.append("AND SOL.IC_IF = ? ");
					varBind.add(new Integer(claveIfCesionario));
				}
				if (numeroContrato != null && !"".equals(numeroContrato)){
					strSQL.append(" AND SOL.CG_NO_CONTRATO = ?");
					varBind.add(numeroContrato);
				}
				if (claveMoneda != null && !"".equals(claveMoneda)){
					if (claveMoneda.equals("0")){
						strSQL.append(" SOL.CS_MULTIMONEDA = ?");
						varBind.add("S");
					} else{
						strSQL.append(" AND SOL.CS_MULTIMONEDA = ?");
						strSQL.append(" AND EXISTS (SELECT MXS.IC_MONTO_X_SOLIC");
						strSQL.append(" FROM CDER_MONTO_X_SOLIC MXS");
						strSQL.append(" WHERE MXS.IC_SOLICITUD = SOL.IC_SOLICITUD");
						strSQL.append(" AND MXS.IC_MONEDA = ?)");
						varBind.add("N");
						varBind.add(new Integer(claveMoneda));
					}
				}
				if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)){
					strSQL.append(" AND SOL.IC_TIPO_CONTRATACION = ?");
					varBind.add(new Integer(claveTipoContratacion));
				}
				strSQL.append(" ORDER BY SOL.IC_SOLICITUD");

			} else{

				System.out.println("OTRO PERFIL");
				strSQL.append(" SELECT sol.ic_solicitud AS numero_solicitud");
				strSQL.append(" FROM cder_solicitud sol");
				strSQL.append(", cder_extincion_contrato ext");
				strSQL.append(", cder_acuse cac");
				strSQL.append(" WHERE sol.ic_solicitud = ext.ic_solicitud");
				if (perfilUsuario.equalsIgnoreCase("ADMIN PYME")) {
					strSQL.append(" AND ext.cc_acuse_ext_rep1 = cac.cc_acuse");
				} else if(perfilUsuario.equalsIgnoreCase("ADMIN IF")){
					strSQL.append(" AND ext.cc_acuse_ext_if = cac.cc_acuse");
				} else if(perfilUsuario.equalsIgnoreCase("ADMIN TEST")){
					strSQL.append(" AND ext.cc_acuse_ext_test1 = cac.cc_acuse");
				}
				strSQL.append(" AND cac.cg_usuario = ?");
				strSQL.append(" AND sol.ic_estatus_solic = ?");

				varBind.add(loginUsuario);
				varBind.add(new Integer(estatusSolicitud));

				if (clavePyme != null && !"".equals(clavePyme)){
					strSQL.append(" AND sol.ic_pyme = ?");
					varBind.add(clavePyme);
				}
				if (claveEpo != null && !"".equals(claveEpo)){
					strSQL.append(" AND sol.ic_epo = ?");
					varBind.add(new Integer(claveEpo));
				}
				if (claveIfCesionario != null && !"".equals(claveIfCesionario)){
					strSQL.append(" AND sol.ic_if = ?");
					varBind.add(new Integer(claveIfCesionario));
				}
				if (numeroContrato != null && !"".equals(numeroContrato)){
					strSQL.append(" AND sol.cg_no_contrato = ?");
					varBind.add(numeroContrato);
				}
				if (claveMoneda != null && !"".equals(claveMoneda)){
					if (claveMoneda.equals("0")){
						strSQL.append(" AND sol.cs_multimoneda = ?");
						varBind.add("S");
					} else{
						strSQL.append(" AND sol.cs_multimoneda = ?");
						strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
						strSQL.append(" FROM cder_monto_x_solic mxs");
						strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
						strSQL.append(" AND mxs.ic_moneda = ?)");
						varBind.add("N");
						varBind.add(new Integer(claveMoneda));
					}
				}
				if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)){
					strSQL.append(" AND sol.ic_tipo_contratacion = ?");
					varBind.add(new Integer(claveTipoContratacion));
				}

			}
/*
			strSQL.append(" SELECT sol.ic_solicitud AS numero_solicitud"); 
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", cder_extincion_contrato ext");
			strSQL.append(", cder_acuse cac");
			strSQL.append(" WHERE sol.ic_solicitud = ext.ic_solicitud");
      if (perfilUsuario.equalsIgnoreCase("ADMIN PYME")) {
        strSQL.append(" AND ext.cc_acuse_ext_rep1 = cac.cc_acuse");
      } else if (perfilUsuario.equalsIgnoreCase("ADMIN IF")) {
        strSQL.append(" AND ext.cc_acuse_ext_if = cac.cc_acuse");
      } else if (perfilUsuario.equalsIgnoreCase("ADMIN TEST")) {
        strSQL.append(" AND ext.cc_acuse_ext_test1 = cac.cc_acuse");
      }
			strSQL.append(" AND cac.cg_usuario = ?");
			strSQL.append(" AND sol.ic_estatus_solic = ?");
			
			varBind.add(loginUsuario);
			varBind.add(new Integer(estatusSolicitud));
			
			if (clavePyme != null && !"".equals(clavePyme)) {
				strSQL.append(" AND sol.ic_pyme = ?");
				varBind.add(clavePyme);
			}
			if (claveEpo != null && !"".equals(claveEpo)) {
				strSQL.append(" AND sol.ic_epo = ?");
				varBind.add(new Integer(claveEpo));
			}
			if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
				strSQL.append(" AND sol.ic_if = ?");
				varBind.add(new Integer(claveIfCesionario));
			}
			if (numeroContrato != null && !"".equals(numeroContrato)) {
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(numeroContrato);
			}
			if (claveMoneda != null && !"".equals(claveMoneda)) {
				if (claveMoneda.equals("0")) {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("S");
				} else {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.ic_moneda = ?)");
					varBind.add("N");
					varBind.add(new Integer(claveMoneda));
				}
			}
			if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
				strSQL.append(" AND sol.ic_tipo_contratacion = ?");
				varBind.add(new Integer(claveTipoContratacion));
			}		
			strSQL.append(" ORDER BY sol.ic_solicitud");
*/
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			int numeroRegistros = 0;
			
			while (rst.next()) {
				extincionesPorUsuario.put("numeroSolicitud" + numeroRegistros, rst.getString("numero_solicitud")==null?"":rst.getString("numero_solicitud"));
				numeroRegistros++;
			}
			
			extincionesPorUsuario.put("numeroRegistros", Integer.toString(numeroRegistros));
			
			rst.close();
			pst.close();
		} catch (Exception e) {
			log.error("obtenerExtincionesPorUsuario(ERROR) ::..");
			e.printStackTrace();
			throw new AppException("Error al consultar la base de datos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerExtincionesPorUsuario(S) ::..");
		}
		return extincionesPorUsuario;
	}
	
	/**
	 * M�todo que genera el acuse de la solicitud de extinci�n del contrato de cesi�n de derechos de cobro.
	 * @param parametrosCambioEstatus <code>HashMap</code> condiciones bajo las que se realiza el cambio de estatus.
	 * @return numeroAcuse <code>String</code> con el numero de acuse generado al autenticar al usuario.
	 * @throws <code>AppException</code> cuando ocurre un error en la actualizaci�n del estatus.
	 */
	public String acuseEnvioSolicitudExtincion(HashMap parametrosFirma) throws AppException{
		log.info("acuseEnvioSolicitudExtincion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		String numeroAcuse = "";
		int existeSolicitud   = 0;
			
		String _serial                = parametrosFirma.get("_serial")                ==null?"":(String)parametrosFirma.get("_serial");
		String pkcs7                  = parametrosFirma.get("pkcs7")                  ==null?"":(String)parametrosFirma.get("pkcs7");
		String textoPlano             = parametrosFirma.get("textoPlano")             ==null?"":(String)parametrosFirma.get("textoPlano");
		String folioCert              = parametrosFirma.get("folioCert")              ==null?"":(String)parametrosFirma.get("folioCert");
		String loginUsuario           = parametrosFirma.get("loginUsuario")           ==null?"":(String)parametrosFirma.get("loginUsuario");
		String nombreUsuario          = parametrosFirma.get("nombreUsuario")          ==null?"":(String)parametrosFirma.get("nombreUsuario");
		String claveSolicitud         = parametrosFirma.get("claveSolicitud")         ==null?"":(String)parametrosFirma.get("claveSolicitud");
		String nuevoEstatus           = parametrosFirma.get("nuevoEstatus")           ==null?"":(String)parametrosFirma.get("nuevoEstatus");
		String fechaExtincionContrato = parametrosFirma.get("fechaExtincionContrato") ==null?"":(String)parametrosFirma.get("fechaExtincionContrato");
		String numeroCuenta           = parametrosFirma.get("numeroCuenta")           ==null?"":(String)parametrosFirma.get("numeroCuenta");
		String numeroCuentaClabe      = parametrosFirma.get("numeroCuentaClabe")      ==null?"":(String)parametrosFirma.get("numeroCuentaClabe");
		String bancoDeposito          = parametrosFirma.get("bancoDeposito")          ==null?"":(String)parametrosFirma.get("bancoDeposito");
		String strDirectorioTemp      = parametrosFirma.get("strDirectorioTemp")      ==null?"":(String)parametrosFirma.get("strDirectorioTemp");

		boolean validCert = Boolean.getBoolean((String)parametrosFirma.get("validCert"));
		
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			con.conexionDB();

			if (nuevoEstatus.equals("17")) {
				if (fechaExtincionContrato.equals("") || numeroCuenta.equals("") || numeroCuentaClabe.equals("") || bancoDeposito.equals("")) {
					log.error("..:: fechaExtincionContrato: "+fechaExtincionContrato);
					log.error("..:: numeroCuenta: "+numeroCuenta);
					log.error("..:: numeroCuentaClabe: "+numeroCuentaClabe);
					log.error("..:: bancoDeposito: "+bancoDeposito);
					throw new AppException("Los par�metros no pueden ser nulos.");
				}
			}
			
			char getReceipt = 'Y';
			if (!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")) {
				Seguridad s = new Seguridad();
				if (s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt)){
				//if(true){ // Este if es para desarrollo
					numeroAcuse = s.getAcuse();//"2345678";//
					String acuseRepresentante = "";
					strSQL = new StringBuffer();
					varBind = new ArrayList();
		
					System.out.println(">>>>> Obtengo el consecutivo del ACUSE <<<<<");// Obtengo el consecutivo del ACUSE
						String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
						ps1 = con.queryPrecompilado(qry);
						rs1 = ps1.executeQuery();
					if(rs1.next()){
						acuseRepresentante = rs1.getString(1)==null?"1":rs1.getString(1);
						}
						ps1.close();
						rs1.close();
						
					System.out.println(">>>>> Se actualiza la tabla cder_solicitud <<<<<");// Se actualiza la tabla cder_solicitud
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						strSQL.append(" UPDATE cder_solicitud SET ic_estatus_solic = ? WHERE ic_solicitud = ?");
						varBind.add(new Integer(nuevoEstatus));
						varBind.add(claveSolicitud);
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
						
					System.out.println(">>>>> Busco si existe la solicitud <<<<<");// Busco si existe la solicitud
					strSQL = new StringBuffer();
					varBind = new ArrayList();
					strSQL.append("SELECT COUNT (*) ");
					strSQL.append("FROM CDER_EXTINCION_CONTRATO ");
					strSQL.append("WHERE IC_SOLICITUD = ? ");
					varBind.add(claveSolicitud);
					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind.toString());
					PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ResultSet rst1 = pst1.executeQuery();
					while (rst1.next()){
						existeSolicitud = rst1.getInt(1);
					}
					rst1.close();
					pst1.close();

					System.out.println(">>>>> Si la solicitud no existe, se inserta una nueva <<<<<");// Si la solicitud no existe, se inserta una nueva
					if (existeSolicitud == 0){

						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" INSERT INTO cder_extincion_contrato (");
						strSQL.append(" ic_solicitud,");
						strSQL.append(" df_extincion_contrato,");
						strSQL.append(" cg_cuenta_ext,");
						strSQL.append(" cg_cuenta_clabe_ext,");
						strSQL.append(" cg_banco_deposito_ext");
						strSQL.append(") VALUES (");
						strSQL.append("?, TO_DATE(?, 'dd/mm/yyyy'), ?, ?, ?)");

						varBind.add(claveSolicitud);
						varBind.add(fechaExtincionContrato);
						varBind.add(numeroCuenta);
						varBind.add(numeroCuentaClabe);
						varBind.add(bancoDeposito);
						
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
						
						con.terminaTransaccion(true);
						String nombreArchivo= this.generarContratoExtincion(claveSolicitud,strDirectorioTemp);
						File archivoPDF = new File(strDirectorioTemp+nombreArchivo);
						FileInputStream fileinputstream = new FileInputStream(archivoPDF);
						StringBuffer qrySQLArchivo = new StringBuffer("");
						qrySQLArchivo.append("UPDATE cder_extincion_contrato SET BI_CONTRATO_EXTINCION = ?, CS_BANDERA_CONTRATO_EXT = 'S'  WHERE ic_solicitud = ?");
						pst = con.queryPrecompilado(qrySQLArchivo.toString());
						pst.setBinaryStream(1, fileinputstream, (int)archivoPDF.length());
						
						pst.setInt(2, Integer.parseInt(claveSolicitud));
						pst.executeUpdate();
						pst.close();
						fileinputstream.close();
						System.out.println(">>>>> Se creo la nueva solicitud <<<<<");
						}
						
					System.out.println(">>>>> Actualizo la tabla CDER_SOLIC_X_REPRESENTANTE <<<<<");// Actualizo la tabla CDER_SOLIC_X_REPRESENTANTE
						strSQL = new StringBuffer();
						varBind = new ArrayList();
					
					strSQL.append(" UPDATE CDER_SOLIC_X_REPRESENTANTE");
					strSQL.append(" SET CC_ACUSE_EXTINCION_REP = ?, ");
					strSQL.append(" DF_EXTINCION_REP = SYSDATE ");
					strSQL.append(" WHERE IC_SOLICITUD = ? ");
					strSQL.append(" AND IC_USUARIO = ?");
	
					varBind.add(new Integer(acuseRepresentante));
						varBind.add(claveSolicitud);
					varBind.add(loginUsuario);
						
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();

					System.out.println(">>>>> Inserto el nuevo acuse en la tabla cder_acuse <<<<<");// Inserto el nuevo acuse en la tabla cder_acuse
						strSQL = new StringBuffer();
						varBind = new ArrayList();
					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");
					varBind.add(new Integer(acuseRepresentante));
					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(numeroAcuse);
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

				}
			}
		} catch(Exception e) {
			transactionOk = false;
			throw new AppException("Error al enviar la solicitud de extinci�n del contrato de cesi�n de derechos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("acuseEnvioSolicitudExtincion(S)");
		}
		return numeroAcuse;
	}
	
	/**
	 * M�todo que cancela la solicitud de extinci�n del contrato de cesi�n de derechos de cobro.
	 * @param claveSolicitud <code>String</code> clave de la solicitud de extinci�n que se va a cancelar.
	 * @return numeroAcuse <code>String</code> con el numero de acuse generado al autenticar al usuario.
	 * @throws <code>AppException</code> cuando ocurre un error en la actualizaci�n del estatus.
	 */
	public void cancelarSolicitudExtincion(String claveSolicitud) throws AppException{
		log.info("cancelarSolicitudExtincion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		String acuseRep1 = "";
		String acuseRep2 = "";

		try {
			con.conexionDB();

			if (claveSolicitud.equals("")) {
				log.error("..:: claveSolicitud: "+claveSolicitud);
				throw new AppException("Los par�metros no pueden ser nulos.");

			}

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT cc_acuse_ext_rep1 AS acuse_rep1,");
			strSQL.append(" cc_acuse_ext_rep2 AS acuse_rep2");
			strSQL.append(" FROM cder_extincion_contrato");
			strSQL.append(" WHERE ic_solicitud = ?");
			
			varBind.add(claveSolicitud);

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while (rst.next()) {
				acuseRep1 = rst.getString("acuse_rep1")==null?"":rst.getString("acuse_rep1");
				acuseRep2 = rst.getString("acuse_rep2")==null?"":rst.getString("acuse_rep2");
			}
			
			rst.close();
			pst.close();
			
			if (acuseRep1.equals("")) {
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" DELETE cder_acuse WHERE cc_acuse = ?");
				varBind.add(acuseRep1);
				
				log.debug("..:: strSQL: "+strSQL.toString());
				log.debug("..:: varBind: "+varBind);
				
				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				pst.executeUpdate();
				pst.close();
			}
			
			if (acuseRep2.equals("")) {
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" DELETE cder_acuse WHERE cc_acuse = ?");
				varBind.add(acuseRep2);
				
				log.debug("..:: strSQL: "+strSQL.toString());
				log.debug("..:: varBind: "+varBind);
				
				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				pst.executeUpdate();
				pst.close();
			}

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" DELETE cder_extincion_contrato WHERE ic_solicitud = ?");
			varBind.add(claveSolicitud);
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();
		
			strSQL.append(" UPDATE cder_solicitud SET ic_estatus_solic = ? WHERE ic_solicitud = ?");

			varBind.add(new Integer(17));
			varBind.add(claveSolicitud);
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
		} catch(Exception e) {
			transactionOk = false;
			throw new AppException("Error al cancelar la extinci�n del contrato de cesi�n de derechos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("cancelarSolicitudExtincion(S)");
		}
	}
	
	/**
	 * M�todo que obtiene las solicitudes de consentimiento que la pyme ha enviado y que han sido aceptadas
	 * por las respectivas epos con las que fueron capturadas para ser presentadas en la pantalla de aviso.
	 * @param clavePyme Clave interna de la pyme de la que se van a consultar las solicitudes.
	 * @return solicitudesAceptadas HashMap con las solicitudes de la pyme que han sido aceptadas por la epo.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 * @author Alberto Cruz Flores.
	 */
	public HashMap consultaAvisoSolicitudExtincionIf(String claveIf) throws AppException {
		log.info("consultaAvisoSolicitudExtincionIf(E) ::..");
		AccesoDB con = new AccesoDB();
		ResultSet rst = null;
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap solicitudesExtincion = new HashMap();
		try {
			con.conexionDB();
			
			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" pyme.cg_razon_social AS nombre_cedente,");
			strSQL.append(" pyme.cg_rfc AS rfc_cendente,");
			strSQL.append(" DECODE(TO_CHAR(extc.df_ext_rep2, 'dd/mm/yyyy'), null, TO_CHAR(extc.df_ext_rep1, 'dd/mm/yyyy'), TO_CHAR(extc.df_ext_rep2, 'dd/mm/yyyy')) AS fecha_solicitud,");
			//strSQL.append(" (SELECT TO_CHAR(MAX(csr.DF_EXTINCION_REP), 'DD/MM/YYYY') FROM cder_solic_x_representante csr WHERE sol.ic_solicitud = csr.ic_solicitud) AS fecha_solicitud, ");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato");
			strSQL.append(" ,sol.CG_EMPRESAS_REPRESENTADAS AS empresas");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_pyme pyme");
			strSQL.append(", cder_extincion_contrato extc");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud");
			strSQL.append(" AND sol.ic_if = ?");
			strSQL.append(" AND sol.ic_estatus_solic = ?");
			strSQL.append(" ORDER BY 5 DESC");
			
			varBind.add(claveIf);
			varBind.add(new Integer(19));
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			int numeroRegistros = 0;
			
			while (rst.next()) {
				HashMap solicitudExtincion = new HashMap();
				StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));
				solicitudExtincion.put("nombreEpo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				solicitudExtincion.put("nombreCedente", rst.getString("nombre_cedente")==null?"":rst.getString("nombre_cedente"));
				solicitudExtincion.put("empresa", rst.getString("empresas")==null?"":rst.getString("empresas"));
				solicitudExtincion.put("rfcCendente", rst.getString("rfc_cendente")==null?"":rst.getString("rfc_cendente"));
				solicitudExtincion.put("fechaSolicitud", rst.getString("fecha_solicitud")==null?"":rst.getString("fecha_solicitud"));
				solicitudExtincion.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				solicitudExtincion.put("montosPorMoneda", montosPorMoneda.toString());
				solicitudesExtincion.put("solicitudExtincion" + numeroRegistros, solicitudExtincion);
				numeroRegistros++;
			}
			
			solicitudesExtincion.put("numeroRegistros", Integer.toString(numeroRegistros));
			
			rst.close();
			pst.close();
		} catch (Exception e) {
			log.error("consultaAvisoSolicitudExtincionIf(ERROR): " +  e.getMessage());
			throw new AppException("Error al consultar las solicitudes de extinci�n.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultaAvisoSolicitudExtincionIf(S) ::..");
		}
		return solicitudesExtincion;
	}
	
	/**
	 * M�todo que genera el combo de pymes que tienen solicitudes pendientes de Extinci�n de contrato.
	 * @param claveIf Clave interna del IF que tiene solicitudes pendientes por autorizar con alguna pyme.
	 * @return comboPyme Lista de objetos ElementoCatalogo con las pymes seleccionadas.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	 public List getComboPymesExtincion(String claveIf) throws AppException{
		log.info("getComboPymesExtincion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List comboPyme = new ArrayList();
		try{
			con.conexionDB();
			
			strSQL.append(" SELECT DISTINCT pyme.ic_pyme AS clave,");
			strSQL.append(" pyme.cg_razon_social AS descripcion");
			strSQL.append(" FROM cder_solicitud cds,");
			strSQL.append(" comcat_pyme pyme");
			strSQL.append(" WHERE cds.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND cds.ic_if = ?");
			strSQL.append(" AND cds.ic_estatus_solic IN (?)");
			strSQL.append(" ORDER BY pyme.cg_razon_social");

			varBind.add(new Integer(claveIf));
			varBind.add(new Integer(19));
			
			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while(rst.next()){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(rst.getString("clave")==null?"":rst.getString("clave"));
				elementoCatalogo.setDescripcion(rst.getString("descripcion")==null?"":rst.getString("descripcion"));
				comboPyme.add(elementoCatalogo);
			}
			
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getComboPymesExtincion(S)");
		}
		return comboPyme;
	}
	
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Contratos de Cesi�n para la pyme.
	 * @param parametros_consulta HashMap con los parametros con los que se realiza la consulta de contratos de cesi�n.
	 * @return consultaContratosCesion Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultarExtincionContratosIf(HashMap parametrosConsulta) throws AppException{
		log.info("consultarExtincionContratosIf(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap consultaContratosCesion = new HashMap();
		int indice = 0;

		try {
			con.conexionDB();
			
			String clavePyme = parametrosConsulta.get("clavePyme")==null?"":(String)parametrosConsulta.get("clavePyme");
			String claveEpo = parametrosConsulta.get("claveEpo")==null?"":(String)parametrosConsulta.get("claveEpo");
			String claveIfCesionario = parametrosConsulta.get("claveIfCesionario")==null?"":(String)parametrosConsulta.get("claveIfCesionario");
			String numeroContrato = parametrosConsulta.get("numeroContrato")==null?"":(String)parametrosConsulta.get("numeroContrato");
			String claveMoneda = parametrosConsulta.get("claveMoneda")==null?"":(String)parametrosConsulta.get("claveMoneda");
			String claveTipoContratacion = parametrosConsulta.get("claveTipoContratacion")==null?"":(String)parametrosConsulta.get("claveTipoContratacion");

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,"); 
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" sol.ic_tipo_contratacion AS clave_tipo_contratacion,");
			strSQL.append(" epo.cg_razon_social AS dependencia,");
			strSQL.append(" pyme.cg_razon_social AS nombre_cedente,");
			strSQL.append(" pyme.cg_rfc AS rfc_cedente,");
			strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" con.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cl.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
			strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
			strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
			strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
			strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");
			strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Extinci�n Rechazada', es.cg_descripcion) AS estatus_solicitud,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", comcat_pyme pyme");
			strSQL.append(", cdercat_clasificacion_epo cl");
			strSQL.append(", cdercat_tipo_contratacion con");
			strSQL.append(", cdercat_estatus_solic es");
			strSQL.append(", cdercat_tipo_plazo stp");
			strSQL.append(", cder_extincion_contrato extc");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if ");
			strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
			strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
			strSQL.append(" AND sol.ic_estatus_solic IN (?)");
			varBind.add(new Integer(19));//19.- Extinci�n Solicitada
		
			if (clavePyme != null && !"".equals(clavePyme)) {
				strSQL.append(" AND sol.ic_pyme = ?");
				varBind.add(clavePyme);
			}
		
			if (claveEpo != null && !"".equals(claveEpo)) {
				strSQL.append(" AND sol.ic_epo = ?");
				varBind.add(new Integer(claveEpo));
			}
		
			if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
				strSQL.append(" AND sol.ic_if = ?");
				varBind.add(new Integer(claveIfCesionario));
			}
		
			if (numeroContrato != null && !"".equals(numeroContrato)) {
				strSQL.append(" AND sol.cg_no_contrato = ?");
				varBind.add(numeroContrato);
			}
		
			if (claveMoneda != null && !"".equals(claveMoneda)) {
				if (claveMoneda.equals("0")) {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					varBind.add("S");
				} else {
					strSQL.append(" AND sol.cs_multimoneda = ?");
					strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
					strSQL.append(" FROM cder_monto_x_solic mxs");
					strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
					strSQL.append(" AND mxs.ic_moneda = ?)");
					varBind.add("N");
					varBind.add(new Integer(claveMoneda));
				}
			}
		
			if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
				strSQL.append(" AND sol.ic_tipo_contratacion = ?");
				varBind.add(new Integer(claveTipoContratacion));
			}	
			strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while (rst.next()) {
				String strClaveEpo = rst.getString("clave_epo");
				StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));
			
				HashMap contratoCesion = new HashMap();
				contratoCesion.put("claveSolicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				contratoCesion.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				contratoCesion.put("clavePyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				contratoCesion.put("claveTipoContratacion", rst.getString("clave_tipo_contratacion")==null?"":rst.getString("clave_tipo_contratacion"));
				contratoCesion.put("nombreEpo", rst.getString("dependencia")==null?"":rst.getString("dependencia"));
				contratoCesion.put("nombreCedente", rst.getString("nombre_cedente")==null?"":rst.getString("nombre_cedente"));
				contratoCesion.put("rfcCedente", rst.getString("rfc_cedente")==null?"":rst.getString("rfc_cedente"));
				contratoCesion.put("nombreIfCesionario", rst.getString("nombre_cesionario")==null?"":rst.getString("nombre_cesionario"));
				contratoCesion.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				contratoCesion.put("montosPorMoneda", montosPorMoneda.toString());
				contratoCesion.put("tipoContratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				contratoCesion.put("fechaInicioContrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				contratoCesion.put("fechaFinContrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				contratoCesion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				contratoCesion.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				contratoCesion.put("campoAdicional1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				contratoCesion.put("campoAdicional2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				contratoCesion.put("campoAdicional3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				contratoCesion.put("campoAdicional4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				contratoCesion.put("campoAdicional5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				contratoCesion.put("objetoContrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				contratoCesion.put("fechaExtincion", rst.getString("fecha_extincion")==null?"":rst.getString("fecha_extincion"));
				contratoCesion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				contratoCesion.put("cuentaClabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				contratoCesion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				contratoCesion.put("estatusSolicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				contratoCesion.put("claveEstatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				contratoCesion.put("causasRechazo", rst.getString("causas_rechazo")==null?"":rst.getString("causas_rechazo"));

				consultaContratosCesion.put("contratoCesion"+indice, contratoCesion);
				indice++;
			}
			
			consultaContratosCesion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		} catch(Exception e) {
			log.error("Error al consultar base de datos");
			e.printStackTrace();
			throw new AppException("Error al consultar los contratos para extinci�n.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultarExtincionContratosIf(S)");
		}
		return consultaContratosCesion;
	}
	
	/**
	 * M�todo que genera la consulta que se muestra en la pantalla de Contratos de Cesi�n para la pyme.
	 * @param parametros_consulta HashMap con los parametros con los que se realiza la consulta de contratos de cesi�n.
	 * @return consultaContratosCesion Hashmap con el resultado de la consulta.
	 * @throws NafinException Cuando ocurre un error durante la consulta del m�todo.
	 */
	public HashMap consultarExtincionContratosIf(String claveSolicitud) throws AppException{
		log.info("consultarExtincionContratosIf(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap contratoCesion = new HashMap();
		String numeroAcuse = "";
		int indice = 0;

		try {
			con.conexionDB();
			
			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,"); 
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" sol.ic_tipo_contratacion AS clave_tipo_contratacion,");
			strSQL.append(" epo.cg_razon_social AS dependencia,");
			strSQL.append(" pyme.cg_razon_social AS nombre_cedente,");
			strSQL.append(" pyme.cg_rfc AS rfc_cedente,");
			strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" con.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cl.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" sol.cg_comentarios AS comentarios,");
			strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
			strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
			strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
			strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
			strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");
			strSQL.append(" extc.cc_acuse_ext_if AS acuse_if,");
      strSQL.append(" extc.cc_acuse_ext_test1 AS acuse_ext_test1,");
      strSQL.append(" extc.cc_acuse_ext_test2 AS acuse_ext_test2,");
			strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Extinci�n Rechazada', es.cg_descripcion) AS estatus_solicitud,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus");
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", comcat_pyme pyme");
			strSQL.append(", cdercat_clasificacion_epo cl");
			strSQL.append(", cdercat_tipo_contratacion con");
			strSQL.append(", cdercat_estatus_solic es");
			strSQL.append(", cdercat_tipo_plazo stp");
			strSQL.append(", cder_extincion_contrato extc");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_if = cif.ic_if ");
			strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
			strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
			strSQL.append(" AND sol.ic_solicitud = ?");

			varBind.add(new Integer(claveSolicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			while (rst.next()) {
				String strClaveEpo = rst.getString("clave_epo");
				StringBuffer montosPorMoneda = this.getMontoMoneda(rst.getString("clave_solicitud"));

				contratoCesion.put("claveSolicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				contratoCesion.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				contratoCesion.put("clavePyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				contratoCesion.put("claveTipoContratacion", rst.getString("clave_tipo_contratacion")==null?"":rst.getString("clave_tipo_contratacion"));
				contratoCesion.put("nombreEpo", rst.getString("dependencia")==null?"":rst.getString("dependencia"));
				contratoCesion.put("nombreCedente", rst.getString("nombre_cedente")==null?"":rst.getString("nombre_cedente"));
				contratoCesion.put("rfcCedente", rst.getString("rfc_cedente")==null?"":rst.getString("rfc_cedente"));
				contratoCesion.put("nombreIfCesionario", rst.getString("nombre_cesionario")==null?"":rst.getString("nombre_cesionario"));
				contratoCesion.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				contratoCesion.put("montosPorMoneda", montosPorMoneda.toString());
				contratoCesion.put("tipoContratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				contratoCesion.put("fechaInicioContrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				contratoCesion.put("fechaFinContrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				contratoCesion.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				contratoCesion.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				contratoCesion.put("campoAdicional1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				contratoCesion.put("campoAdicional2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				contratoCesion.put("campoAdicional3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				contratoCesion.put("campoAdicional4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				contratoCesion.put("campoAdicional5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				contratoCesion.put("objetoContrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				contratoCesion.put("fechaExtincion", rst.getString("fecha_extincion")==null?"":rst.getString("fecha_extincion"));
				contratoCesion.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				contratoCesion.put("cuentaClabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				contratoCesion.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				contratoCesion.put("estatusSolicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				contratoCesion.put("claveEstatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				contratoCesion.put("causasRechazo", rst.getString("causas_rechazo")==null?"":rst.getString("causas_rechazo"));

				//ACUSE DE FIRMA DE AUTORIZACION DE EXTINCI�N POR EL IF
				numeroAcuse = rst.getString("acuse_if")==null?"":rst.getString("acuse_if");
				if (!numeroAcuse.equals("")) {
					contratoCesion.put("acuseExtIf", rst.getString("acuse_if")==null?"":rst.getString("acuse_if"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						contratoCesion.put("claveUsrExtIf", datosAcuse.get("claveUsuario"));
						contratoCesion.put("nombreUsrExtIf", datosAcuse.get("nombreUsuario"));
						contratoCesion.put("fechaExtIf", datosAcuse.get("fechaAcuse"));
						contratoCesion.put("horaExtIf", datosAcuse.get("horaAcuse"));
					}
				}

				//ACUSE DE FIRMA DE AUTORIZACION DE EXTINCI�N POR EL PRIMER TESTIGO IF
				numeroAcuse = rst.getString("acuse_ext_test1")==null?"":rst.getString("acuse_ext_test1");
				if (!numeroAcuse.equals("")) {
					contratoCesion.put("acuseExtTest1", rst.getString("acuse_ext_test1")==null?"":rst.getString("acuse_ext_test1"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						contratoCesion.put("claveUsrExtTest1", datosAcuse.get("claveUsuario"));
						contratoCesion.put("nombreUsrExtTest1", datosAcuse.get("nombreUsuario"));
						contratoCesion.put("fechaExtTest1", datosAcuse.get("fechaAcuse"));
						contratoCesion.put("horaExtTest1", datosAcuse.get("horaAcuse"));
					}
				}

				//ACUSE DE FIRMA DE AUTORIZACION DE EXTINCI�N POR EL SEGUNDO TESTIGO IF
				numeroAcuse = rst.getString("acuse_ext_test2")==null?"":rst.getString("acuse_ext_test2");
				if (!numeroAcuse.equals("")) {
					contratoCesion.put("acuseExtTest2", rst.getString("acuse_ext_test2")==null?"":rst.getString("acuse_ext_test2"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						contratoCesion.put("claveUsrExtTest2", datosAcuse.get("claveUsuario"));
						contratoCesion.put("nombreUsrExtTest2", datosAcuse.get("nombreUsuario"));
						contratoCesion.put("fechaExtTest2", datosAcuse.get("fechaAcuse"));
						contratoCesion.put("horaExtTest2", datosAcuse.get("horaAcuse"));
					}
				}
        
				indice++;
			}
			
			contratoCesion.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();
		} catch(Exception e) {
			log.error("Error al consultar base de datos");
			e.printStackTrace();
			throw new AppException("Error al consultar los contratos para extinci�n.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultarExtincionContratosIf(S)");
		}
		return contratoCesion;
	}
	
	/**
	 * M�todo que genera el acuse de la solicitud de extinci�n del contrato de cesi�n de derechos de cobro.
	 * @param parametrosCambioEstatus <code>HashMap</code> condiciones bajo las que se realiza el cambio de estatus.
	 * @return numeroAcuse <code>String</code> con el numero de acuse generado al autenticar al usuario.
	 * @throws <code>AppException</code> cuando ocurre un error en la actualizaci�n del estatus.
	 */
	public String acuseExtincionContratoIf(HashMap parametrosFirma) throws AppException{

		log.info("acuseExtincionContratoIf(E)");

		StringBuffer      strSQL        = new StringBuffer();
		StringBuffer      strSQL1       = new StringBuffer();
		List              varBind       = new ArrayList();
		AccesoDB          con           = new AccesoDB();
		PreparedStatement pst           = null;
		boolean           transactionOk = true;
		String            numeroAcuse    = "";

		String _serial                = parametrosFirma.get("_serial")               ==null?"":(String)parametrosFirma.get("_serial");
		String pkcs7                  = parametrosFirma.get("pkcs7")                 ==null?"":(String)parametrosFirma.get("pkcs7");
		String textoPlano             = parametrosFirma.get("textoPlano")            ==null?"":(String)parametrosFirma.get("textoPlano");
		String folioCert              = parametrosFirma.get("folioCert")             ==null?"":(String)parametrosFirma.get("folioCert");
		String loginUsuario           = parametrosFirma.get("loginUsuario")          ==null?"":(String)parametrosFirma.get("loginUsuario");
		String nombreUsuario          = parametrosFirma.get("nombreUsuario")         ==null?"":(String)parametrosFirma.get("nombreUsuario");
		String claveSolicitud         = parametrosFirma.get("claveSolicitud")        ==null?"":(String)parametrosFirma.get("claveSolicitud");
		String nuevoEstatus           = parametrosFirma.get("nuevoEstatus")          ==null?"":(String)parametrosFirma.get("nuevoEstatus");
		String causasRechazoExtincion = parametrosFirma.get("causasRechazoExtincion")==null?"":(String)parametrosFirma.get("causasRechazoExtincion");
		String tipoExtincion          = parametrosFirma.get("tipoExtincion")         ==null?"":(String)parametrosFirma.get("tipoExtincion");
		String strDirectorioTemp      = parametrosFirma.get("strDirectorioTemp")     ==null?"":(String)parametrosFirma.get("strDirectorioTemp");
		String fechaExtincion         = parametrosFirma.get("fechaExtincion")        ==null?"":(String)parametrosFirma.get("fechaExtincion");
		String numeroCuenta           = parametrosFirma.get("numeroCuenta")          ==null?"":(String)parametrosFirma.get("numeroCuenta");
		String cuentaClabe            = parametrosFirma.get("cuentaClabe")           ==null?"":(String)parametrosFirma.get("cuentaClabe");
		String bancoDeposito          = parametrosFirma.get("bancoDeposito")         ==null?"":(String)parametrosFirma.get("bancoDeposito");
		String existeRepIf2           = parametrosFirma.get("existeRepIf2")          ==null?"":(String)parametrosFirma.get("existeRepIf2");

		boolean validCert             = Boolean.getBoolean((String)parametrosFirma.get("validCert"));

		PreparedStatement ps1      = null;
		ResultSet         rs1      = null;
		String            loginIf2 = "";

		try {
			con.conexionDB();

			String qryIF2 = "select count(*)  total from COMCAT_CESIONARIO where IC_IF_REP2 = '"+loginUsuario+"'";
			ps1 = con.queryPrecompilado(qryIF2);
			rs1 = ps1.executeQuery();
			if (rs1.next()){
				loginIf2 = rs1.getString(1)==null?"0":rs1.getString(1);
			}
			ps1.close();
			rs1.close();

			char getReceipt = 'Y';
			if (!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")) {
				Seguridad s = new Seguridad();
				if (s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt)) {
					numeroAcuse = s.getAcuse();
				//if(true){
					//numeroAcuse = "123456";

					String acuseIf = "";
					int existeSolicitud = 0;

					String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
					ps1 = con.queryPrecompilado(qry);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						acuseIf = rs1.getString(1)==null?"1":rs1.getString(1);
					}
					ps1.close();
					rs1.close();

					System.out.println(">>>>> Busco si existe la solicitud <<<<<");
					strSQL1 = new StringBuffer();
					varBind = new ArrayList();
					strSQL1.append("SELECT COUNT (*) ");
					strSQL1.append("FROM CDER_EXTINCION_CONTRATO ");
					strSQL1.append("WHERE IC_SOLICITUD = ? ");
					varBind.add(claveSolicitud);
					log.debug("..:: strSQL1 : "+strSQL1.toString());
					log.debug("..:: varBind : "+varBind.toString());
					PreparedStatement pst1 = con.queryPrecompilado(strSQL1.toString(), varBind);
					ResultSet rst1 = pst1.executeQuery();
					while (rst1.next()){
						existeSolicitud = rst1.getInt(1);
					}
					rst1.close();
					pst1.close();

					System.out.println(">>>>> Si la solicitud no existe, se inserta una nueva <<<<<");
					if (existeSolicitud == 0){

						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" INSERT INTO cder_extincion_contrato (");
						strSQL.append(" ic_solicitud,");
						strSQL.append(" df_extincion_contrato,");
						strSQL.append(" cg_cuenta_ext,");
						strSQL.append(" cg_cuenta_clabe_ext,");
						strSQL.append(" cg_banco_deposito_ext");
						strSQL.append(") VALUES (");
						strSQL.append("?, TO_DATE(?, 'dd/mm/yyyy'), ?, ?, ?)");

						varBind.add(claveSolicitud);
						varBind.add(fechaExtincion);
						varBind.add(numeroCuenta);
						varBind.add(cuentaClabe);
						varBind.add(bancoDeposito);

						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);

						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();

						con.terminaTransaccion(true);
						String nombreArchivo= this.generarContratoExtincion(claveSolicitud,strDirectorioTemp);
						File archivoPDF = new File(strDirectorioTemp+nombreArchivo);
						FileInputStream fileinputstream = new FileInputStream(archivoPDF);
						StringBuffer qrySQLArchivo = new StringBuffer("");
						qrySQLArchivo.append("UPDATE cder_extincion_contrato SET BI_CONTRATO_EXTINCION = ?, CS_BANDERA_CONTRATO_EXT = 'S'  WHERE ic_solicitud = ?");
						pst = con.queryPrecompilado(qrySQLArchivo.toString());

						pst.setBinaryStream(1, fileinputstream, (int)archivoPDF.length());
						pst.setInt(2, Integer.parseInt(claveSolicitud));

						pst.executeUpdate();
						pst.close();
						fileinputstream.close();
						System.out.println(">>>>> Se creo la nueva solicitud <<<<<");
					}

				if (nuevoEstatus.equals("17")) {
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" UPDATE cder_solicitud SET ic_estatus_solic = ? WHERE ic_solicitud = ?");

					varBind.add(new Integer(nuevoEstatus));
					varBind.add(claveSolicitud);

					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}

					// Si no hay 2o representante o si ya firm� el representante IF2 se actualiza el estatus
					if("C".equals(tipoExtincion)){ // Esta acci�n solo aplica para extinci�n corta
						// Preparo la consulta
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						strSQL.append(" UPDATE cder_solicitud SET ic_estatus_solic = ? WHERE ic_solicitud = ?");
						varBind.add(new Integer(nuevoEstatus));
						varBind.add(claveSolicitud);
						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);

						if("N".equals(existeRepIf2)){
							pst.executeUpdate();
						} else{
							if(!"0".equals(loginIf2)){
								pst.executeUpdate();
							}
						}
						pst.close();
					}

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" UPDATE cder_extincion_contrato");

					if (loginIf2.equals("0")) {
						strSQL.append(" SET cc_acuse_ext_if = ?,");
						strSQL.append(" df_ext_if = SYSDATE");
					}else  {
						strSQL.append(" SET cc_acuse_ext_if2 = ? ,");
						strSQL.append(" df_ext_if2 = SYSDATE");
					}

					if (nuevoEstatus.equals("17")) {
						strSQL.append(" , cg_causa_rechazo_ext = ?");
					}

					strSQL.append(" WHERE ic_solicitud  = ?");

					
					varBind.add(new Integer(acuseIf));	
					if (nuevoEstatus.equals("17")) {
						varBind.add(causasRechazoExtincion);
					}
					varBind.add(claveSolicitud);

					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(acuseIf));
					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(numeroAcuse);

					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

				}
			}
		} catch(Exception e) {
			transactionOk = false;
			throw new AppException("Error al enviar la solicitud de extinci�n del contrato de cesi�n de derechos.", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("acuseExtincionContratoIf(S)");
		}
		return numeroAcuse;
	}

	/**
	 * M�todo que guarda el acuse de extinci�n del contrato de cesi�n de derechos por parte del Testigo del IF.
	 * @param parametrosFirma HashMap con los parametros con los que se reliza la autenticaci�n del usuario.
	 * @return numeroAcuse Cadena con el numero de acuse generado al autenticar al usuario.
	 * @throws Appexception Cuando ocurre un error al obtener el acuse y/o guardar la firma de la pyme.
	 */
	public String acuseExtincionContratoTestigo(HashMap parametrosFirma) throws AppException{
		log.info("acuseExtincionContratoTestigo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		String numeroAcuse = "";
			
		String _serial = parametrosFirma.get("_serial")==null?"":(String)parametrosFirma.get("_serial");
		String pkcs7 = parametrosFirma.get("pkcs7")==null?"":(String)parametrosFirma.get("pkcs7");
		String textoPlano = parametrosFirma.get("textoPlano")==null?"":(String)parametrosFirma.get("textoPlano");
		boolean validCert = Boolean.getBoolean((String)parametrosFirma.get("validCert"));
		String folioCert = parametrosFirma.get("folioCert")==null?"":(String)parametrosFirma.get("folioCert");
		String loginUsuario = parametrosFirma.get("loginUsuario")==null?"":(String)parametrosFirma.get("loginUsuario");
		String nombreUsuario = parametrosFirma.get("nombreUsuario")==null?"":(String)parametrosFirma.get("nombreUsuario");
		String claveSolicitud = parametrosFirma.get("claveSolicitud")==null?"":(String)parametrosFirma.get("claveSolicitud");
		String nuevoEstatus = parametrosFirma.get("nuevoEstatus")==null?"":(String)parametrosFirma.get("nuevoEstatus");
		String causasRechazoExtincion = parametrosFirma.get("causasRechazoExtincion")==null?"":(String)parametrosFirma.get("causasRechazoExtincion");
		
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			con.conexionDB();
			
			char getReceipt = 'Y';
			if (!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")) {
				Seguridad s = new Seguridad();
				if (s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt)) {
					numeroAcuse = s.getAcuse();
          String acuseTestigo1 = "";
					String nuevoAcuseTestigo = "";
					
					String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
					ps1 = con.queryPrecompilado(qry);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						nuevoAcuseTestigo = rs1.getString(1)==null?"1":rs1.getString(1);
					}
					ps1.close();
					rs1.close();
					
					strSQL = new StringBuffer();
					varBind = new ArrayList();
		
					strSQL.append(" SELECT cc_acuse_ext_test1 AS acuse_ext_test1");
					strSQL.append(" FROM cder_extincion_contrato");
					strSQL.append(" WHERE ic_solicitud = ?");
					
					varBind.add(claveSolicitud);
		
					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind.toString());
					
					PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ResultSet rst1 = pst1.executeQuery();
					
					while (rst1.next()) {
						acuseTestigo1 = rst1.getString("acuse_ext_test1")==null?"":rst1.getString("acuse_ext_test1");
					}
					
					rst1.close();
					pst1.close();

					strSQL = new StringBuffer();
					varBind = new ArrayList();

					if (!acuseTestigo1.equals("")){
            strSQL = new StringBuffer();
            varBind = new ArrayList();

						strSQL.append(" UPDATE cder_solicitud");
						strSQL.append(" SET ic_estatus_solic = ?");
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(new Integer(20));
						varBind.add(claveSolicitud);
            
            log.debug("..:: strSQL: "+strSQL.toString());
            log.debug("..:: varBind: "+varBind);
            
            pst = con.queryPrecompilado(strSQL.toString(), varBind);
            pst.executeUpdate();
            pst.close();

            strSQL = new StringBuffer();
            varBind = new ArrayList();

						strSQL.append(" UPDATE cder_extincion_contrato");
						strSQL.append(" SET cc_acuse_ext_test2 = ?,");
						strSQL.append(" df_ext_test2 = SYSDATE");
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(new Integer(nuevoAcuseTestigo)); 
						varBind.add(claveSolicitud);
            
            log.debug("..:: strSQL: "+strSQL.toString());
            log.debug("..:: varBind: "+varBind);
            
            pst = con.queryPrecompilado(strSQL.toString(), varBind);
            pst.executeUpdate();
            pst.close();
					} else {
            strSQL = new StringBuffer();
            varBind = new ArrayList();

						strSQL.append(" UPDATE cder_extincion_contrato");
						strSQL.append(" SET cc_acuse_ext_test1 = ?,");
						strSQL.append(" df_ext_test1 = SYSDATE");
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(new Integer(nuevoAcuseTestigo));  
						varBind.add(claveSolicitud);

            log.debug("..:: strSQL: "+strSQL.toString());
            log.debug("..:: varBind: "+varBind);
            
            pst = con.queryPrecompilado(strSQL.toString(), varBind);
            pst.executeUpdate();
            pst.close();
					}					
					
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
					strSQL.append(" VALUES (?, ?, ?, ?)");

					varBind.add(new Integer(nuevoAcuseTestigo)); 
					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(numeroAcuse);
					
					log.debug("..:: strSQL: "+strSQL.toString());
					log.debug("..:: varBind: "+varBind);
					
					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();
				}
			}
		} catch(Exception e) {
			transactionOk = false;
			throw new AppException("Error al enviar la solicitud de extinci�n del contrato de cesi�n de derechos.", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("acuseExtincionContratoTestigo(S)");
		}
		return numeroAcuse;
	}
  
  /**
   * Este m�todo se implementa para la generaci�n de la pantalla del Convenio de Extinci�n del contrato de Cesi�n
   * de Derechos para todos los perfiles involucrados en el proceso.
   * @param claveSolicitud <code>String</code> es el valor de la clave interna del contrato de cesi�n.
   * @return convenioExtincionCesionDerechos Es el resultado de la consulta mapeado en un objeto <code>HashMap</code>.
   * @throws AppException Se lanza una excepci�n cuando ocurre un error en la ejecuci�n del m�todo.
   */
  public HashMap consultaConvenioExtincionCesionDerechos(String claveSolicitud) throws AppException {
    log.info("consultaConvenioExtincionCesionDerechos() ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap convenioExtincionCesionDerechos = new HashMap();
		String numeroAcuse = "";
		int indice = 0; 

		try {
			con.conexionDB();

			strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strSQL.append(" sol.ic_pyme AS clave_pyme,");
			strSQL.append(" sol.ic_epo AS clave_epo,");
			strSQL.append(" sol.ic_if AS clave_if,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			strSQL.append(" pym.cg_rfc AS rfc_pyme,");
			strSQL.append(" crpe.cg_pyme_epo_interno AS numero_proveedor,");
			strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud_consentimiento,");
			strSQL.append(" TO_CHAR(SYSDATE, 'dd/mm/yyyy') AS fecha_formal_contrato,");
			strSQL.append(" TO_CHAR(sol.df_notif_vent, 'dd/mm/yyyy') AS fecha_notif_vent,");
			strSQL.append(" TO_CHAR(sol.df_redireccion, 'dd/mm/yyyy') AS fecha_redireccion,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
			strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
			strSQL.append(" sol.cg_cuenta_clabe AS cuenta_clabe,");
			strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
			strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS plazo_contrato,");
			strSQL.append(" cep.cg_area AS clasificacion_epo,");
			strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
			strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
			strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
			strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
			strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
			strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strSQL.append(" TO_CHAR(sol.df_fecha_limite_firma, 'dd/mm/yyyy') AS fecha_limite_firma,");
			strSQL.append(" TO_CHAR(sol.df_fecha_aceptacion, 'dd/mm/yyyy') AS fecha_aceptacion_epo,");
			strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
			strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
			strSQL.append(" sol.cc_acuse2 AS acuse2,");//ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA O RECHAZA LA SOLIC
			strSQL.append(" sol.cc_acuse3 AS acuse3,");//ESTE ES EL ACUSE DE LA PYME CUANDO FIRMA EL ECONTRATO
			strSQL.append(" sol.cc_acuse4 AS acuse4,");//ESTE ES EL ACUSE DEL IF CUANDO FIRMA EL ECONTRATO
			strSQL.append(" sol.cc_acuse5 AS acuse5,");//ESTE ES EL ACUSE DE LA EPO CUANDO ACEPTA LA NOTIFICACION
			strSQL.append(" sol.cc_acuse_firma_rep1 AS acuse_firma_rep1,");
			strSQL.append(" sol.cc_acuse_firma_rep2 AS acuse_firma_rep2,");
			strSQL.append(" sol.cc_acuse_testigo1 AS acuse_firma_test1,");
			strSQL.append(" sol.cc_acuse_testigo2 AS acuse_firma_test2,");
			strSQL.append(" sol.cc_acuse_redirec AS acuse_redirec,");
			strSQL.append(" sol.cg_firma1 AS firma1,");
			strSQL.append(" sol.cg_firma2 AS firma2,");//FIRMA DEL IF
			strSQL.append(" sol.cg_firma3 AS firma3,");//FIRMA DEL PRIMER TESTIGO
			strSQL.append(" sol.cg_firma4 AS firma4,");//FIRMA DEL SEGUNDO TESTIGO
			strSQL.append(" sol.cg_firma_rep1 AS firma_rep1,");//FIRMA DEL PRIMER REPRESENTANTE LEGAL PYME
			strSQL.append(" sol.cg_firma_rep2 AS firma_rep2,");//FIRMA DEL SEGUNDO REPRESENTANTE LEGAL PYME
			strSQL.append(" DECODE(sol.cc_acuse_firma_rep2, null, TO_CHAR(ext.df_ext_rep1, 'dd/mm/yyyy'), TO_CHAR(ext.df_ext_rep2, 'dd/mm/yyyy')) AS fecha_solicitud_extincion,");
			strSQL.append(" TO_CHAR(ext.df_ext_if , 'dd/mm/yyyy') AS fecha_form_convenio,");
			strSQL.append(" TO_CHAR(ext.df_ext_test2, 'dd/mm/yyyy') AS fecha_notificacion_epo,");
			strSQL.append(" TO_CHAR(ext.df_ext_epo, 'dd/mm/yyyy') AS fecha_acynot_ventanilla,");
			strSQL.append(" TO_CHAR(ext.df_ext_vent, 'dd/mm/yyyy') AS fecha_redireccionamiento,");
			strSQL.append(" ext.cc_acuse_ext_rep1 AS acuse_ext_rep1,");
			strSQL.append(" ext.cc_acuse_ext_rep2 AS acuse_ext_rep2,"); //F024-2014 
			
			strSQL.append(" ext.cc_acuse_ext_if AS acuse_ext_if,");
			strSQL.append(" ext.CC_ACUSE_EXT_IF2 AS acuse_ext_if2 ,");			
			strSQL.append(" ext.cc_acuse_ext_test1 AS acuse_ext_test1,");
			strSQL.append(" ext.cc_acuse_ext_test2 AS acuse_ext_test2,");
			strSQL.append(" ext.cc_acuse_ext_epo AS acuse_ext_epo,");
			strSQL.append(" ext.cc_acuse_ext_vent AS acuse_ext_vent,");//
			strSQL.append(" TO_CHAR (ext.df_ext_if, 'dd')||' d�as del mes de '||TO_CHAR (ext.df_ext_if, 'MONTH', 'NLS_DATE_LANGUAGE=SPANISH')  ||' de '|| TO_CHAR (ext.df_ext_if, 'yyyy')  as dfExtIf ");  
			strSQL.append(" FROM cder_solicitud sol");
			strSQL.append(", cder_extincion_contrato ext");
			strSQL.append(", comrel_pyme_epo crpe");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", cdercat_tipo_contratacion tcn");
			strSQL.append(", cdercat_clasificacion_epo cep");
			strSQL.append(", cdercat_estatus_solic sts");
			strSQL.append(", cdercat_tipo_plazo ctp");
			strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
			strSQL.append(" AND sol.ic_solicitud = ext.ic_solicitud(+)");
			strSQL.append(" AND sol.ic_if = cif.ic_if");
			strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND sol.ic_pyme = crpe.ic_pyme(+)");
			strSQL.append(" AND sol.ic_epo = crpe.ic_epo(+)");
			strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strSQL.append(" AND sol.ic_epo = cep.ic_epo");
			strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strSQL.append(" AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)");
			strSQL.append(" AND sol.ic_solicitud = ?");
			
			varBind.add(claveSolicitud);
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			
			rst = pst.executeQuery();
			
			while(rst.next()){
				String claveEpo = rst.getString("clave_epo");
				int diasMinimosNotificacion = this.getDiasMinimosNotificacion(claveEpo);
        
				convenioExtincionCesionDerechos.put("claveSolicitud", rst.getString("clave_solicitud")==null?"":rst.getString("clave_solicitud"));
				convenioExtincionCesionDerechos.put("clavePyme", rst.getString("clave_pyme")==null?"":rst.getString("clave_pyme"));
				convenioExtincionCesionDerechos.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				convenioExtincionCesionDerechos.put("claveIf", rst.getString("clave_if")==null?"":rst.getString("clave_if"));
				convenioExtincionCesionDerechos.put("nombrePyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				convenioExtincionCesionDerechos.put("rfcPyme", rst.getString("rfc_pyme")==null?"":rst.getString("rfc_pyme"));
				convenioExtincionCesionDerechos.put("numeroProveedor", rst.getString("numero_proveedor")==null?"":rst.getString("numero_proveedor"));
				convenioExtincionCesionDerechos.put("fechaSolicitudConsentimiento", rst.getString("fecha_solicitud_consentimiento")==null?"":rst.getString("fecha_solicitud_consentimiento"));
				convenioExtincionCesionDerechos.put("fechaNotifVentanilla", rst.getString("fecha_notif_vent")==null?"":rst.getString("fecha_notif_vent"));
				convenioExtincionCesionDerechos.put("fechaRedirecccion", rst.getString("fecha_redireccion")==null?"":rst.getString("fecha_redireccion"));
				convenioExtincionCesionDerechos.put("nombreEpo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				convenioExtincionCesionDerechos.put("nombreIf", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
				convenioExtincionCesionDerechos.put("bancoDeposito", rst.getString("banco_deposito")==null?"":rst.getString("banco_deposito"));
				convenioExtincionCesionDerechos.put("numeroCuenta", rst.getString("numero_cuenta")==null?"":rst.getString("numero_cuenta"));
				convenioExtincionCesionDerechos.put("numeroCuentaClabe", rst.getString("cuenta_clabe")==null?"":rst.getString("cuenta_clabe"));
				convenioExtincionCesionDerechos.put("numeroContrato", rst.getString("numero_contrato")==null?"":rst.getString("numero_contrato"));
				convenioExtincionCesionDerechos.put("tipoContratacion", rst.getString("tipo_contratacion")==null?"":rst.getString("tipo_contratacion"));
				convenioExtincionCesionDerechos.put("fechaInicioContrato", rst.getString("fecha_inicio_contrato")==null?"":rst.getString("fecha_inicio_contrato"));
				convenioExtincionCesionDerechos.put("fechaFinContrato", rst.getString("fecha_fin_contrato")==null?"":rst.getString("fecha_fin_contrato"));
				convenioExtincionCesionDerechos.put("clasificacionEpo", rst.getString("clasificacion_epo")==null?"":rst.getString("clasificacion_epo"));
				convenioExtincionCesionDerechos.put("campoAdicional1", rst.getString("campo_adicional_1")==null?"":rst.getString("campo_adicional_1"));
				convenioExtincionCesionDerechos.put("campoAdicional2", rst.getString("campo_adicional_2")==null?"":rst.getString("campo_adicional_2"));
				convenioExtincionCesionDerechos.put("campoAdicional3", rst.getString("campo_adicional_3")==null?"":rst.getString("campo_adicional_3"));
				convenioExtincionCesionDerechos.put("campoAdicional4", rst.getString("campo_adicional_4")==null?"":rst.getString("campo_adicional_4"));
				convenioExtincionCesionDerechos.put("campoAdicional5", rst.getString("campo_adicional_5")==null?"":rst.getString("campo_adicional_5"));
				convenioExtincionCesionDerechos.put("objetoContrato", rst.getString("objeto_contrato")==null?"":rst.getString("objeto_contrato"));
				convenioExtincionCesionDerechos.put("fechaLimiteFirma", rst.getString("fecha_limite_firma")==null?"":rst.getString("fecha_limite_firma"));
				convenioExtincionCesionDerechos.put("fechaAceptacionEpo", rst.getString("fecha_aceptacion_epo")==null?"":rst.getString("fecha_aceptacion_epo"));
				convenioExtincionCesionDerechos.put("claveEstatus", rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				convenioExtincionCesionDerechos.put("estatusSolicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
				convenioExtincionCesionDerechos.put("plazoContrato", rst.getString("plazo_contrato")==null?"":rst.getString("plazo_contrato"));
				convenioExtincionCesionDerechos.put("fechaSolicitudExtincion", rst.getString("fecha_solicitud_extincion")==null?"":rst.getString("fecha_solicitud_extincion"));
				convenioExtincionCesionDerechos.put("fechaFormConvenio", rst.getString("fecha_form_convenio")==null?"":rst.getString("fecha_form_convenio"));
				convenioExtincionCesionDerechos.put("fechaNotificacionEpo", rst.getString("fecha_notificacion_epo")==null?"":rst.getString("fecha_notificacion_epo"));
				convenioExtincionCesionDerechos.put("fechaAcynotVentanilla", rst.getString("fecha_acynot_ventanilla")==null?"":rst.getString("fecha_acynot_ventanilla"));
				convenioExtincionCesionDerechos.put("fechaRedireccionamiento", rst.getString("plazo_contrato")==null?"":rst.getString("fecha_redireccionamiento"));
				convenioExtincionCesionDerechos.put("diasMinimosNotificacion", Integer.toString(diasMinimosNotificacion));
				convenioExtincionCesionDerechos.put("dfExtIf",rst.getString("dfExtIf")==null?"":rst.getString("dfExtIf"));
				StringBuffer monto_moneda = this.getMontoMoneda(rst.getString("clave_solicitud"));
				
				convenioExtincionCesionDerechos.put("montoMoneda", monto_moneda.toString());
              
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE EXTINCION POR EL PRIMER REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("acuse_ext_rep1")==null?"":rst.getString("acuse_ext_rep1");
				if (!numeroAcuse.equals("")) {
					convenioExtincionCesionDerechos.put("acuseExtRep1", rst.getString("acuse_ext_rep1")==null?"":rst.getString("acuse_ext_rep1"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						convenioExtincionCesionDerechos.put("claveUsrExtRep1", datosAcuse.get("claveUsuario"));
						convenioExtincionCesionDerechos.put("nombreUsrExtRep1", datosAcuse.get("nombreUsuario"));
						convenioExtincionCesionDerechos.put("fechaExtRep1", datosAcuse.get("fechaAcuse"));
						convenioExtincionCesionDerechos.put("horaExtRep1", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE FIRMA DE ENV�O DE LA SOLICITUD DE EXTINCION POR EL SEGUNDO REPRESENTANTE LEGAL DE LA PYME
				numeroAcuse = rst.getString("acuse_ext_rep2")==null?"":rst.getString("acuse_ext_rep2");
				if (!numeroAcuse.equals("")) {
					convenioExtincionCesionDerechos.put("acuseExtRep2", rst.getString("acuse_ext_rep2")==null?"":rst.getString("acuse_ext_rep2"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						convenioExtincionCesionDerechos.put("claveUsrExtRep2", datosAcuse.get("claveUsuario"));
						convenioExtincionCesionDerechos.put("nombreUsrExtRep2", datosAcuse.get("nombreUsuario"));
						convenioExtincionCesionDerechos.put("fechaExtRep2", datosAcuse.get("fechaAcuse"));
						convenioExtincionCesionDerechos.put("horaExtRep2", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DE LA EXTINCION POR PARTE DEL IF
				numeroAcuse = rst.getString("acuse_ext_if")==null?"":rst.getString("acuse_ext_if");
				if (!numeroAcuse.equals("")) {
					convenioExtincionCesionDerechos.put("acuseExtIf", rst.getString("acuse_ext_if")==null?"":rst.getString("acuse_ext_if"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						convenioExtincionCesionDerechos.put("claveUsrExtIf", datosAcuse.get("claveUsuario"));
						convenioExtincionCesionDerechos.put("nombreUsrExtIf", datosAcuse.get("nombreUsuario"));
						convenioExtincionCesionDerechos.put("fechaExtIf", datosAcuse.get("fechaAcuse"));
						convenioExtincionCesionDerechos.put("horaExtIf", datosAcuse.get("horaAcuse"));
					}
				}
								
				//ACUSE DE ACEPTACION DE LA EXTINCION POR PARTE DEL IF 2F024-2014				
				String numeroAcuseIf2 = rst.getString("acuse_ext_if2")==null?"":rst.getString("acuse_ext_if2");
				if (!numeroAcuseIf2.equals("")) {
					convenioExtincionCesionDerechos.put("acuseExtIf2", rst.getString("acuse_ext_if2")==null?"":rst.getString("acuse_ext_if2"));

					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuseIf2);

					if (!datosAcuse.isEmpty()) {
						convenioExtincionCesionDerechos.put("claveUsrExtIf2", datosAcuse.get("claveUsuario"));
						convenioExtincionCesionDerechos.put("nombreUsrExtIf2", datosAcuse.get("nombreUsuario"));
						convenioExtincionCesionDerechos.put("fechaExtIf2", datosAcuse.get("fechaAcuse"));
						convenioExtincionCesionDerechos.put("horaExtIf2", datosAcuse.get("horaAcuse"));
					}
				}
				
				//ACUSE DE ACEPTACION DE LA EXTINCION POR PARTE DEL PRIMER TESTIGO DEL IF
				numeroAcuse = rst.getString("acuse_ext_test1")==null?"":rst.getString("acuse_ext_test1");
				if (!numeroAcuse.equals("")) {
					convenioExtincionCesionDerechos.put("acuseExtTest1", rst.getString("acuse_ext_test1")==null?"":rst.getString("acuse_ext_test1"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						convenioExtincionCesionDerechos.put("claveUsrExtTest1", datosAcuse.get("claveUsuario"));
						convenioExtincionCesionDerechos.put("nombreUsrExtTest1", datosAcuse.get("nombreUsuario"));
						convenioExtincionCesionDerechos.put("fechaExtTest1", datosAcuse.get("fechaAcuse"));
						convenioExtincionCesionDerechos.put("horaExtTest1", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE DE ACEPTACION DEL CONTRATO DE CESI�N POR PARTE DEL SEGUNDO TESTIGO DEL IF
				numeroAcuse = rst.getString("acuse_ext_test2")==null?"":rst.getString("acuse_ext_test2");
				if (!numeroAcuse.equals("")) {
					convenioExtincionCesionDerechos.put("acuseExtTest2", rst.getString("acuse_ext_test2")==null?"":rst.getString("acuse_ext_test2"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						convenioExtincionCesionDerechos.put("claveUsrExtTest2", datosAcuse.get("claveUsuario"));
						convenioExtincionCesionDerechos.put("nombreUsrExtTest2", datosAcuse.get("nombreUsuario"));
						convenioExtincionCesionDerechos.put("fechaExtTest2", datosAcuse.get("fechaAcuse"));
						convenioExtincionCesionDerechos.put("horaExtTest2", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE NOTIFICACI�N DE LA EXTINCI�N POR PARTE DE LA EPO
				numeroAcuse = rst.getString("acuse_ext_epo")==null?"":rst.getString("acuse_ext_epo");
				if (!numeroAcuse.equals("")) {
					convenioExtincionCesionDerechos.put("acuseExtEpo", rst.getString("acuse_ext_epo")==null?"":rst.getString("acuse_ext_epo"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						convenioExtincionCesionDerechos.put("claveUsrExtEpo", datosAcuse.get("claveUsuario"));
						convenioExtincionCesionDerechos.put("nombreUsrExtEpo", datosAcuse.get("nombreUsuario"));
						convenioExtincionCesionDerechos.put("fechaExtEpo", datosAcuse.get("fechaAcuse"));
						convenioExtincionCesionDerechos.put("horaExtEpo", datosAcuse.get("horaAcuse"));
					}
				}
				//ACUSE REDIRECCIONAMIENTO POR PARTE DE LA VENTANILLA
				numeroAcuse = rst.getString("acuse_ext_vent")==null?"":rst.getString("acuse_ext_vent");
				if (!numeroAcuse.equals("")) {
					convenioExtincionCesionDerechos.put("acuseExtVent", rst.getString("acuse_ext_vent")==null?"":rst.getString("acuse_ext_vent"));
					
					HashMap datosAcuse = this.obtenerDatosAcuse(numeroAcuse);

					if (!datosAcuse.isEmpty()) {
						convenioExtincionCesionDerechos.put("claveUsrExtVent", datosAcuse.get("claveUsuario"));
						convenioExtincionCesionDerechos.put("nombreUsrExtVent", datosAcuse.get("nombreUsuario"));
						convenioExtincionCesionDerechos.put("fechaExtVent", datosAcuse.get("fechaAcuse"));
						convenioExtincionCesionDerechos.put("horaExtVent", datosAcuse.get("horaAcuse"));
					}
				}
				indice++;
			}
			
			convenioExtincionCesionDerechos.put("indice", Integer.toString(indice));
			
			rst.close();
			pst.close();    
    } catch (Exception e) {
      log.error("consultaConvenioExtincionCesionDerechos(ERROR) ::..");
      throw new AppException("Ocurri� un error durante la ejecuci�n del m�todo." , e);
    } finally {
      if (con.hayConexionAbierta()) {
        con.cierraConexionDB();
      }
      log.info("consultaConvenioExtincionCesionDerechos() ::..");
    }
    return convenioExtincionCesionDerechos;
  }
	//---------------------------------------------------------------------------- FODEA 030 - 2011 ACF - I
	//----------------------------------------------------------------------------FODEA 030 - 2011 DLHC - I	
	/**
	 * con estatus Extinci�n Aceptada IF
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param noEpo
	 */
	public List  consSoliExtincion (String noEpo)	throws AppException	{
		log.info("consSoliExtincion (E)");
		PreparedStatement ps = null;			
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		List registros = new ArrayList();
		List datos = new ArrayList();
		List lvarbind = new ArrayList();
		
		try {
			con.conexionDB();
			
			qrySentencia.append(" SELECT sol.ic_solicitud AS noSolicitud, ");
			qrySentencia.append(" i.cg_razon_social AS cecionario, ");
			qrySentencia.append(" p.cg_razon_social AS pyme, ");
			qrySentencia.append(" sol.cg_empresas_representadas AS empresas, ");
			qrySentencia.append(" p.cg_rfc AS rfc, ");
			qrySentencia.append(" TO_CHAR (df_fecha_solicitud_ini, 'DD/MM/YYYY') AS fsolicitudini, ");
			qrySentencia.append(" sol.cg_no_contrato AS nocontrato,");
			qrySentencia.append(" sol.fn_monto_contrato AS monto, ");
			qrySentencia.append("  TO_CHAR (ex.DF_EXTINCION_CONTRATO, 'DD/MM/YYYY') AS fextionContrato ");
			qrySentencia.append(" FROM cder_solicitud sol,  ");
			qrySentencia.append(" comcat_epo e, comcat_if i,  ");
			qrySentencia.append(" cdercat_clasificacion_epo cl,   ");
			qrySentencia.append(" cdercat_tipo_contratacion con,  ");
			qrySentencia.append(" cdercat_estatus_solic es,  ");
			qrySentencia.append(" comcat_pyme p, ");
			qrySentencia.append("  CDER_EXTINCION_CONTRATO ex ");		 
			qrySentencia.append(" WHERE sol.ic_epo = e.ic_epo ");
			qrySentencia.append(" AND sol.ic_if = i.ic_if  ");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo  ");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion  ");
			qrySentencia.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic  ");
			qrySentencia.append(" AND sol.ic_pyme = p.ic_pyme  ");
			qrySentencia.append(" AND sol.ic_solicitud = ex.ic_solicitud  ");
			qrySentencia.append(" AND sol.ic_estatus_solic IN (20)   ");
			qrySentencia.append(" AND sol.ic_epo = ?  ");
			qrySentencia.append(" ORDER BY i.cg_razon_social ");
			lvarbind.add(noEpo);					
			
			log.debug("..:: qrySentencia: "+qrySentencia);
			log.debug("..:: lvarbind: "+lvarbind);
			
			ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				datos = new ArrayList();
				StringBuffer montosPorMoneda = this.getMontoMoneda(rs.getString("noSolicitud"));
				datos.add(rs.getString("noSolicitud")==null?"":rs.getString("noSolicitud"));
				datos.add(rs.getString("cecionario")==null?"":rs.getString("cecionario"));
				datos.add(rs.getString("pyme")==null?"":rs.getString("pyme"));
				datos.add(rs.getString("rfc")==null?"":rs.getString("rfc"));				
				datos.add(rs.getString("fsolicitudini")==null?"":rs.getString("fsolicitudini"));
				datos.add(rs.getString("nocontrato")==null?"":rs.getString("nocontrato"));
				datos.add(montosPorMoneda.toString());
				datos.add(rs.getString("fextionContrato")==null?"":rs.getString("fextionContrato"));
				datos.add(rs.getString("empresas")==null?"":rs.getString("empresas"));
				registros.add(datos);
			}
			rs.close();
			ps.close();
		} catch(Exception e) {
			log.error("consSoliExtincion (Exception) " + e);			
			throw new AppException("consSoliExtincion(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consSoliExtincion (S)");
		}
		return registros;	
	}

	/**
	 * metodo que regresa los registros en el preacuse de
	 * Extincion de contrato como EPO
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param claveSolicitud
	 */
	public List  consExtincionPreacuse (String claveSolicitud, String epo, String ventanilla)	throws AppException	{
		log.info("consExtincionPreacuse (E)");
		PreparedStatement ps = null;			
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		List registros = new ArrayList();
		List datos = new ArrayList();
		List lvarbind = new ArrayList();
		
		try {
			con.conexionDB();
			
		 	qrySentencia.append(" SELECT   sol.ic_solicitud AS clave_solicitud, sol.ic_pyme AS clave_pyme, ");
			qrySentencia.append("  sol.ic_epo AS clave_epo, sol.ic_if AS clave_if,");
			qrySentencia.append("  cif.cg_razon_social AS nombre_if, pym.cg_razon_social AS nombre_pyme,");
			qrySentencia.append("  pym.cg_rfc AS rfc, cpe.cg_pyme_epo_interno AS numero_proveedor,");
			qrySentencia.append("  TO_CHAR (sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme, ");
			qrySentencia.append("  sol.cg_no_contrato AS numero_contrato, ");
			qrySentencia.append("  tcn.cg_nombre AS tipo_contratacion, ");
			qrySentencia.append(" DECODE (sol.df_fecha_vigencia_ini, NULL, 'N/A',  TO_CHAR (sol.df_fecha_vigencia_ini, 'dd/mm/yyyy') ) AS fecha_inicio_contrato, ");
			qrySentencia.append(" DECODE (sol.df_fecha_vigencia_fin, NULL, 'N/A',  TO_CHAR (sol.df_fecha_vigencia_fin, 'dd/mm/yyyy') ) AS fecha_fin_contrato, ");
			qrySentencia.append(" DECODE (sol.ig_plazo, NULL, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion   ) AS plazo_contrato, ");
			qrySentencia.append(" cep.cg_area AS clasificacion_epo, sol.cg_campo1 AS campo_adicional_1, ");
			qrySentencia.append(" sol.cg_campo2 AS campo_adicional_2, ");
			qrySentencia.append(" sol.cg_campo3 AS campo_adicional_3, ");
			qrySentencia.append(" sol.cg_campo4 AS campo_adicional_4, ");
			qrySentencia.append(" sol.cg_campo5 AS campo_adicional_5, ");
			qrySentencia.append(" sol.cg_obj_contrato AS objeto_contrato, ");
			qrySentencia.append(" sol.cg_comentarios AS comentarios, ");
			qrySentencia.append(" sol.fn_monto_credito AS monto_credito, ");
			qrySentencia.append(" sol.cg_referencia AS numero_referencia, ");
			qrySentencia.append(" TO_CHAR (sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento, ");
			qrySentencia.append(" ex.CG_BANCO_DEPOSITO_EXT AS banco_deposito,");
			qrySentencia.append(" ex.CG_CUENTA_EXT AS numero_cuenta,");
			qrySentencia.append(" ex.CG_CUENTA_CLABE_EXT AS clabe,");
			qrySentencia.append("  sol.ic_estatus_solic AS clave_estatus,");			
			qrySentencia.append(" sts.cg_descripcion AS estatus_solicitud, ");
			qrySentencia.append(" sol.cg_causas_rechazo_notif AS causas_rechazo_notif, ");
			qrySentencia.append(" ex.CG_CAUSA_RECHAZO_EXT AS causasRechazo, ");
			qrySentencia.append(" TO_CHAR (ex.DF_EXTINCION_CONTRATO, 'DD/MM/YYYY') AS fextionContrato ");
			qrySentencia.append(" , sol.CG_CORREOEXT as  correo ");
			qrySentencia.append(" , sol.cg_empresas_representadas as  empresas ");
			qrySentencia.append(" FROM cder_solicitud sol, ");
			qrySentencia.append(" comcat_epo epo, ");
			qrySentencia.append(" comcat_if cif, ");
			qrySentencia.append(" comcat_pyme pym, ");
			qrySentencia.append(" comrel_pyme_epo cpe, ");
			qrySentencia.append(" cdercat_tipo_contratacion tcn, ");
			qrySentencia.append(" cdercat_clasificacion_epo cep,");
			qrySentencia.append(" cdercat_estatus_solic sts, ");
			qrySentencia.append(" cdercat_tipo_plazo stp,  ");
			qrySentencia.append(" CDER_EXTINCION_CONTRATO ex ");
			qrySentencia.append(" WHERE sol.ic_epo = epo.ic_epo ");
			qrySentencia.append(" AND sol.ic_if = cif.ic_if ");
			qrySentencia.append(" AND sol.ic_pyme = pym.ic_pyme ");
			qrySentencia.append(" AND sol.ic_epo = cpe.ic_epo(+) ");
			qrySentencia.append(" AND sol.ic_pyme = cpe.ic_pyme(+) ");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion ");
			qrySentencia.append(" AND sol.ic_epo = cep.ic_epo ");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo ");
			qrySentencia.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic ");
			qrySentencia.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+) ");
			qrySentencia.append(" AND sol.ic_solicitud = ex.ic_solicitud ");
			qrySentencia.append(" and sol.ic_solicitud =? ");
			qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC ");
			
			lvarbind.add(claveSolicitud);					
			
			log.debug("..:: qrySentencia: "+qrySentencia);
			log.debug("..:: lvarbind: "+lvarbind);
			
			ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				datos = new ArrayList();
				//obtengo el monto 
				StringBuffer montosPorMoneda = this.getMontoMoneda(rs.getString("clave_solicitud"));
				//obtengo el nombre de la Ventanilla
				String Ventanilla  = this.consVentanilla(epo,ventanilla);
				
				datos.add(rs.getString("clave_solicitud")==null?"":rs.getString("clave_solicitud")); //0
				datos.add(rs.getString("nombre_if")==null?"":rs.getString("nombre_if")); //1
				datos.add(rs.getString("nombre_pyme")==null?"":rs.getString("nombre_pyme")); //2
				datos.add(rs.getString("rfc")==null?"":rs.getString("rfc"));		 //3
				datos.add("");					 // 4
				datos.add(rs.getString("numero_contrato")==null?"":rs.getString("numero_contrato")); //5
				datos.add(montosPorMoneda.toString()); //6
				datos.add(rs.getString("tipo_contratacion")==null?"":rs.getString("tipo_contratacion")); //7
				datos.add(rs.getString("fecha_inicio_contrato")==null?"":rs.getString("fecha_inicio_contrato"));  //8
				datos.add(rs.getString("fecha_fin_contrato")==null?"":rs.getString("fecha_fin_contrato"));  //9
				datos.add(rs.getString("plazo_contrato")==null?"":rs.getString("plazo_contrato")); //10 
				datos.add(rs.getString("clasificacion_epo")==null?"":rs.getString("clasificacion_epo")); //11
				datos.add(rs.getString("campo_adicional_1")==null?"":rs.getString("campo_adicional_1")); //12
				datos.add(rs.getString("campo_adicional_2")==null?"":rs.getString("campo_adicional_2"));//13
				datos.add(rs.getString("campo_adicional_3")==null?"":rs.getString("campo_adicional_3"));//14
				datos.add(rs.getString("campo_adicional_4")==null?"":rs.getString("campo_adicional_4")); //15
				datos.add(rs.getString("campo_adicional_5")==null?"":rs.getString("campo_adicional_5")); //16
				datos.add(rs.getString("objeto_contrato")==null?"N/A":rs.getString("objeto_contrato")); //17
				datos.add(rs.getString("fextionContrato")==null?"N/A":rs.getString("fextionContrato")); //18
				datos.add(rs.getString("numero_cuenta")==null?"N/A":rs.getString("numero_cuenta")); //19
				datos.add(rs.getString("clabe")==null?"N/A":rs.getString("clabe")); //20
				datos.add(rs.getString("banco_deposito")==null?"N/A":rs.getString("banco_deposito")); //21
				datos.add(rs.getString("causasrechazo")==null?"N/A":rs.getString("causasrechazo"));	 //22
				datos.add(rs.getString("estatus_solicitud")==null?"N/A":rs.getString("estatus_solicitud"));	 //23
				datos.add(Ventanilla);	 //24	
				datos.add(rs.getString("correo")==null?"N":rs.getString("correo"));	//25
				datos.add(rs.getString("clave_pyme")==null?"N/A":rs.getString("clave_pyme"));	 //26
				datos.add(rs.getString("empresas")==null?"":rs.getString("empresas"));	 //27
				registros.add(datos);
			}
			rs.close();
			ps.close();
		} catch(Exception e) {
			log.error("consExtincionPreacuse (Exception) " + e);			
			throw new AppException("consExtincionPreacuse(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consExtincionPreacuse (S)");
		}
		return registros;	
	}

/**  se obtien el nombre de la ventanilla
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param ventanilla
	 * @param epo
	 */
private String consVentanilla(String epo, String ventanilla) throws AppException{
		log.info("consVentanilla(E)");
	
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean   commit = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String respuesta ="";
		
		try{
			con.conexionDB();
			
				strSQL.append("select CG_DESCRIPCION  ");
				strSQL.append(" from comcat_ventanilla_epo  ");
				strSQL.append("where ic_epo = ? and ic_ventanilla =  ? ");
				varBind.add(epo);
				varBind.add(ventanilla);
					
				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				rs = ps.executeQuery();
				if(rs.next()) {
					respuesta = rs.getString(1)==null?"":rs.getString(1);
				}
				ps.close();
				rs.close();
			
			
		}catch(Exception e){
				commit = false;
			throw new AppException("Error consVentanilla: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("consVentanilla(S)");
		}
		return respuesta;
	}



/**
	 * metodo que forma  el cuerpo del correo a enviarse 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param comentarios
	 * @param correoPara
	 * @param correoDe
	 * @param claveSolicitud
	 */
	public String  EnviodeCorreoEXT (String claveEpo, String claveSolicitud,  String correoPara, String comentarios, String ventanilla, String asunto )	throws AppException	{

		log.info("EnviodeCorreoEXT (E)");
	
		PreparedStatement ps = null;			
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		List registros = new ArrayList();
		List datos = new ArrayList();
		String respuesta = "";
		List lvarbind = new ArrayList();
		String codigoHTML = "";
		String mensajeCorreo2 	= 	"";
			
		String color = "";
		String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
		"font-size: 10px; "+
		"font-weight: bold;"+
		"color: #FFFFFF;"+
		"background-color: #4d6188;"+
		"padding-top: 3px;"+
		"padding-right: 1px;"+
		"padding-bottom: 1px;"+
		"padding-left: 3px;"+
		"height: 25px;"+
		"border: 1px solid #1a3c54;'";

		String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
		"color:#000066; "+
		"padding-top:1px; "+
		"padding-right:2px; "+
		"padding-bottom:1px; "+
		"padding-left:2px; "+
		"height:22px; "+
		"font-size:11px;'";

		String styleRegistros = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
		"color:#000066; "+
		"padding-top:1px; "+
		"padding-right:2px; "+
		"padding-bottom:1px; "+
		"padding-left:2px; "+
		"height:22px; "+
		"font-size:11px; ";

		String fechaHoy  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

		try {
			 con.conexionDB();
			 
			 String nombreUsuario ="";	
			 
						
			UtilUsr utilUsr = new UtilUsr();
			boolean usuarioEncontrado = false;
			boolean perfilIndicado = false;
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(claveEpo, "E");
			for (int i = 0; i < usuariosPorPerfil.size(); i++) {
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);					
				Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
				perfilIndicado = true;
				nombreUsuario = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno();											
			}
			
			//obtiene los registros 
			registros = this.consExtincionPreacuse(claveSolicitud, claveEpo, ventanilla);
			 
			//Campos Adicionales por EPO
			HashMap campos_adicionales = this.getCamposAdicionalesParametrizados(claveEpo, "9");
			int indice = Integer.parseInt((String)campos_adicionales.get("indice"));
			int num_campos_adicionales = Integer.parseInt((String)campos_adicionales.get("indice"));
			
			//titulo de la clasificacion EPO
			String clasificacionEpo = this.clasificacionEpo(claveEpo);
			if(clasificacionEpo ==null){   clasificacionEpo=""; }
				
			if (registros.size()>0){
				
				StringBuffer mensajeCorreo =  new StringBuffer(); 
			
				mensajeCorreo.append("<html>");
				mensajeCorreo.append("<head>");
				mensajeCorreo.append("<title>");
				mensajeCorreo.append(asunto);
				mensajeCorreo.append("</title>");
				mensajeCorreo.append("</head>");
	
				mensajeCorreo.append("<body>"); 
				mensajeCorreo.append("<table style=\"text-align:justify\" cellspacing='0' cellpadding='0' border='0' align='center' width='700'>");
				mensajeCorreo.append("<tr><td "+style+">Estimado (a): <b>"+nombreUsuario+"</b></td></tr>");				
				mensajeCorreo.append("<tr><td "+style+" width='500'>"+comentarios+"</td></tr>");
				mensajeCorreo.append("</table>");
					 
				mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='1' align='center' width='700'>");
				mensajeCorreo.append("<tr>");
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Intermediario Financiero (Cesionario)<b></td>");
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>PYME (Cedente)<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>RFC<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Representante Legal<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>No. de Contrato<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Monto / Moneda<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Tipo de Contrataci�n<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Fecha Inicio Contrato<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Fecha Final Contrato<b></td>");
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Plazo del Contrato<b></td>");
				if(!clasificacionEpo.equals("")){
					mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>"+clasificacionEpo+"<b></td>");				
				}
				if(indice > 0){
					indice = Integer.parseInt((String)campos_adicionales.get("indice"));
					for(int i = 0; i < indice; i++){		
						String nombreCampo = campos_adicionales.get("nombre_campo_"+i).toString();					
						mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>"+nombreCampo+"<b></td>");				
					}		
				}
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Objeto del Contrato<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Fecha de Extinci�n del contrato<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Cuenta<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Cuenta CLABE<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Banco de Deposito<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Causa de Rechazo<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Estatus<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Ventanilla Destino<b></td>");				
				mensajeCorreo.append("</tr>");
				
	
				for(int i = 0; i< registros.size(); i++){	
					datos = (List)registros.get(i);				
					//Se obtiene el representante Legal de la Pyme
					String noProveedor = (String)datos.get(26);
					StringBuffer nombreContacto = new StringBuffer();
					List usuariosPorPerfil2 = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for (int y = 0; y < usuariosPorPerfil2.size(); y++) {
						String loginUsuarioEpo = (String)usuariosPorPerfil2.get(y);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(y>0){
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+"  ");	
						nombreContacto.append("\n");
					}
					String cedente = (String)datos.get(2)+((!((String)datos.get(27)).equals(""))?";"+(String)datos.get(27):"");
					mensajeCorreo.append("<tr>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(1)+"</td>");
//					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(2)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+cedente.replaceAll(";","<br><br>")+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(3)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+nombreContacto+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(5)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(6)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(7)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(8)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(9)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(10)+"</td>");
						
					if(!clasificacionEpo.equals("")){
						mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(11)+"</td>");
					}
					if(indice > 0){
						for(int j = 0; j < num_campos_adicionales; j++){
							mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(+(11 + 1+j))+"</td>");
						} 
					}				
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(17)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(18)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(19)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(20)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(21)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(22)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(23)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(24)+"</td>");
					mensajeCorreo.append("</tr>");
						
				}			
				mensajeCorreo.append("</table>"); 
				mensajeCorreo.append("</body>");
				mensajeCorreo.append("</html>");
		
				mensajeCorreo2 = this.convierteAcentosACodigoHTML(mensajeCorreo.toString()); 
					
			}
				
			Correo correo = new Correo();
				
			if(!correoPara.equals("")){
				List correos = new VectorTokenizer(correoPara, "\n").getValuesVector();
				for(int i = 0; i < correos.size(); i++) 	{
					if(correos.get(i)!=null){						
						List unCorreo = new VectorTokenizer((String)correos.get(i), ",").getValuesVector();
						for(int e = 0; e < unCorreo.size(); e++) 	{	
							String unCorreo1   = (unCorreo.size()>= 1)?unCorreo.get(e)==null?"":((String)unCorreo.get(e)).trim():"";				 
							if(!unCorreo1.equals("")){
								correo.enviarTextoHTML("cadenas@nafin.gob.mx", unCorreo1, asunto, mensajeCorreo2);
							}
						}
					}
				}
			}
				
			qrySentencia = new StringBuffer();
			lvarbind = new ArrayList();
			qrySentencia.append(" UPDATE  cder_solicitud ");
			qrySentencia.append(" SET IC_VENTANILLA   = ?,  ");	
			qrySentencia.append(" CG_CORREOEXT    = ? ");					
			qrySentencia.append(" WHERE ic_solicitud = ? ");
				
							
			lvarbind.add(ventanilla);	
			lvarbind.add("S");	//bandera para saber si fue enviado el correo
			lvarbind.add(claveSolicitud);							
					
			ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);
			ps.executeUpdate();
			ps.close();	
				
			respuesta = "EXITO"; 
		
		}catch(Exception e){
			respuesta = "ERROR"; 
			log.error("EnviodeCorreoEXT (Exception) " + e);			
			throw new AppException("EnviodeCorreoEXT(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("EnviodeCorreoEXT (E)");		
		return respuesta;	
	}	


	/**
	 * metodo que genera el acuse de la Extincion de contrato EPo
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param parametrosFirma
	 */

	public String acuseExtinContratoEPO(List parametrosFirma) throws AppException{
		log.info("acuseExtinContratoEPO(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		String acuse = "";
		int seqNEXTVAL = 0;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		
		
		String _serial = (String)parametrosFirma.get(0);
		String textoPlano = (String)parametrosFirma.get(1);
		String pkcs7 =  (String)parametrosFirma.get(2);
		String folioCert =  (String)parametrosFirma.get(3);
		String validCert =  (String)parametrosFirma.get(4);
		String claveSolicitud =  (String)parametrosFirma.get(5);
		String loginUsuario =  (String)parametrosFirma.get(6);
		String nombreUsuario =  (String)parametrosFirma.get(7);
		String tipoOperacion =  (String)parametrosFirma.get(8);
		String catVentanilla =  (String)parametrosFirma.get(9);
		String causasRechazo= (String)parametrosFirma.get(10);
	
		try{
			con.conexionDB();
					
			String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
			ps1 = con.queryPrecompilado(qry);
			rs1 = ps1.executeQuery();
			if(rs1.next()) {
				seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
			}
			ps1.close();
			rs1.close(); 
			
			char getReceipt = 'Y';
			if (!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")) {
				Seguridad s = new Seguridad();
				
				if (s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt)) {
					acuse = s.getAcuse();
					
					if (!acuse.equals("")) {
						//modifica la tabla de cder_solicitud
						strSQL.append(" UPDATE cder_solicitud");
						strSQL.append(" SET ic_estatus_solic = ?,");
						strSQL.append(" IC_VENTANILLA = ?,");
						strSQL.append(" CG_CORREOEXT = 'N'");//inicializo de nuevo el campo  CG_CORREOEXT
						strSQL.append(" WHERE ic_solicitud = ?");
						
						if(tipoOperacion.equals("A")){
							varBind.add(new Integer(21)); //Extincion Aceptada  y Notificada a Ventanilla
						}else if(tipoOperacion.equals("R") ){
							varBind.add(new Integer(19));  //Extincion  Solicitada
						}
						varBind.add(catVentanilla); 					
						varBind.add(claveSolicitud);
            log.debug("..:: strSQL: "+strSQL);
            log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();

						//modifica la tabla de CDER_EXTINCION_CONTRATO
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						strSQL.append(" UPDATE CDER_EXTINCION_CONTRATO");
						strSQL.append(" SET DF_EXT_EPO = SYSDATE , ");	
						strSQL.append(" CC_ACUSE_EXT_EPO = ?  ");
						
						if(tipoOperacion.equals("R") ){
						  strSQL.append(", cc_acuse_ext_if = null");
						  strSQL.append(", df_ext_if = null");
						  strSQL.append(", cc_acuse_ext_test1 = null");
						  strSQL.append(", df_ext_test1 = null");
						  strSQL.append(", cc_acuse_ext_test2 = null");
						  strSQL.append(", df_ext_test2 = null");
							strSQL.append(", CG_CAUSA_RECHAZO_EXT = ?  ");
						}
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(new Integer(seqNEXTVAL)); 
						if(tipoOperacion.equals("R") ){
							varBind.add(causasRechazo); 
						}
						varBind.add(claveSolicitud);
						log.debug("..:: strSQL: "+strSQL);
						log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
						
						// INSERTA TABLA ACUSE 
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
						strSQL.append(" VALUES (?, ?, ?, ?)");
						
						
						varBind.add(new Integer(seqNEXTVAL));
						varBind.add(loginUsuario);
						varBind.add(nombreUsuario);
						varBind.add(acuse);
						log.debug("..:: strSQL: "+strSQL);
						log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
						
					}
				}
			}
		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error acuseExtinContratoEPO: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseExtinContratoEPO(S)");
		}
		return acuse;
	}

	/**
	 * metodo que genera el acuse de la Extincion de contrato EPo cuando se cambia a estatus Extinci�n Notificaci�n Aceptada
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param parametrosFirma
	 */

	public String acuseExtinContratoEpoNotificacion(List parametrosFirma){
		log.info("acuseExtinContratoEpoNotificacion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		int seqNEXTVAL = 0;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		String acuse = "";
		
		String _serial			= (String)parametrosFirma.get(0);
		String textoPlano		= (String)parametrosFirma.get(1);
		String pkcs7			=  (String)parametrosFirma.get(2);
		String folioCert		=  (String)parametrosFirma.get(3);
		String validCert		=  (String)parametrosFirma.get(4);
		String claveSolicitud=  (String)parametrosFirma.get(5);
		String loginUsuario	=  (String)parametrosFirma.get(6);
		String nombreUsuario	=  (String)parametrosFirma.get(7);
		String tipoOperacion	=  (String)parametrosFirma.get(8);
		String catVentanilla	=  (String)parametrosFirma.get(9);
		String causasRechazo	= (String)parametrosFirma.get(10);
	
		try{
			con.conexionDB();
					
			String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
			ps1 = con.queryPrecompilado(qry);
			rs1 = ps1.executeQuery();
			if(rs1.next()) {
				seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
			}
			ps1.close();
			rs1.close();

			char getReceipt = 'Y';
			if (!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")) {
				Seguridad s = new Seguridad();

				if (s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt)) {
				//if(true){
					acuse = s.getAcuse();

					if (!acuse.equals("")) {
						//modifica la tabla de cder_solicitud
						strSQL.append(" UPDATE cder_solicitud");
						strSQL.append(" SET ic_estatus_solic = ?,");
						strSQL.append(" CG_CORREOEXT = 'N'");//inicializo de nuevo el campo  CG_CORREOEXT
						strSQL.append(" WHERE ic_solicitud = ?");
						
						if(tipoOperacion.equals("Autorizar")){
							varBind.add(new Integer(23)); //Extincion Notificaci�n Aceptada
						}else if(tipoOperacion.equals("Rechazar") ){
							varBind.add(new Integer(19));  //Extincion  Solicitada
						}
						varBind.add(claveSolicitud);
						log.debug("..:: strSQL: "+strSQL);
						log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();

						//modifica la tabla de CDER_EXTINCION_CONTRATO
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						strSQL.append(" UPDATE CDER_EXTINCION_CONTRATO");
						strSQL.append(" SET DF_EXT_EPO = SYSDATE , ");	
						strSQL.append(" CC_ACUSE_EXT_EPO = ?  ");
						
						if(tipoOperacion.equals("Rechazar") ){
							strSQL.append(", cc_acuse_ext_if = null");
							strSQL.append(", df_ext_if = null");
							strSQL.append(", cc_acuse_ext_test1 = null");
							strSQL.append(", df_ext_test1 = null");
							strSQL.append(", cc_acuse_ext_test2 = null");
							strSQL.append(", df_ext_test2 = null");
							strSQL.append(", CG_CAUSA_RECHAZO_EXT = ?  ");
						}
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(new Integer(seqNEXTVAL)); 
						if(tipoOperacion.equals("Rechazar") ){
							varBind.add(causasRechazo); 
						}
						varBind.add(claveSolicitud);
						log.debug("..:: strSQL: "+strSQL);
						log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
						
						// INSERTA TABLA ACUSE
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
						strSQL.append(" VALUES (?, ?, ?, ?)");
						
						varBind.add(new Integer(seqNEXTVAL));
						varBind.add(loginUsuario);
						varBind.add(nombreUsuario);
						varBind.add(acuse);
						log.debug("..:: strSQL: "+strSQL);
						log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
						
					}
				}
			}
		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error acuseExtinContratoEpoNotificacion: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseExtinContratoEpoNotificacion(S)");
		}
		return acuse;
	}

	/**
	 * metodo que regresa los registros  del preacuse de Epo Ventanilla
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param claveSolicitud
	 */
public List  consExtiPreacuseEpoVen (String claveSolicitud, String epo)	throws AppException	{
		log.info("consExtiPreacuseEpoVen (E)");
	
		PreparedStatement ps = null;			
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		List registros = new ArrayList();
		List datos = new ArrayList();
		List lvarbind = new ArrayList();
		
		try {
			con.conexionDB();
			
			qrySentencia.append(" SELECT   sol.ic_solicitud AS clave_solicitud, sol.ic_pyme AS clave_pyme, ");
			qrySentencia.append("  sol.ic_epo AS clave_epo, sol.ic_if AS clave_if,");
			qrySentencia.append("  cif.cg_razon_social AS nombre_if, pym.cg_razon_social  nombre_pyme,");
			qrySentencia.append("  pym.cg_rfc AS rfc, cpe.cg_pyme_epo_interno AS numero_proveedor,");
			qrySentencia.append("  TO_CHAR (sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme, ");
			qrySentencia.append("  sol.cg_no_contrato AS numero_contrato, ");
			qrySentencia.append("  tcn.cg_nombre AS tipo_contratacion, ");
			qrySentencia.append(" DECODE (sol.df_fecha_vigencia_ini, NULL, 'N/A',  TO_CHAR (sol.df_fecha_vigencia_ini, 'dd/mm/yyyy') ) AS fecha_inicio_contrato, ");
			qrySentencia.append(" DECODE (sol.df_fecha_vigencia_fin, NULL, 'N/A',  TO_CHAR (sol.df_fecha_vigencia_fin, 'dd/mm/yyyy') ) AS fecha_fin_contrato, ");
			qrySentencia.append(" DECODE (sol.ig_plazo, NULL, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion   ) AS plazo_contrato, ");
			qrySentencia.append(" cep.cg_area AS clasificacion_epo, sol.cg_campo1 AS campo_adicional_1, ");
			qrySentencia.append(" sol.cg_campo2 AS campo_adicional_2, ");
			qrySentencia.append(" sol.cg_campo3 AS campo_adicional_3, ");
			qrySentencia.append(" sol.cg_campo4 AS campo_adicional_4, ");
			qrySentencia.append(" sol.cg_campo5 AS campo_adicional_5, ");
			qrySentencia.append(" sol.cg_obj_contrato AS objeto_contrato, ");
			qrySentencia.append(" sol.cg_comentarios AS comentarios, ");
			qrySentencia.append(" sol.fn_monto_credito AS monto_credito, ");
			qrySentencia.append(" sol.cg_referencia AS numero_referencia, ");
			qrySentencia.append(" TO_CHAR (sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento, ");
			qrySentencia.append(" ex.CG_BANCO_DEPOSITO_EXT AS banco_deposito,");
			qrySentencia.append(" ex.CG_CUENTA_EXT AS numero_cuenta,");
			qrySentencia.append(" ex.CG_CUENTA_CLABE_EXT AS clabe,");
			qrySentencia.append("  sol.ic_estatus_solic AS clave_estatus,");			
			qrySentencia.append(" sts.cg_descripcion AS estatus_solicitud, ");
			qrySentencia.append(" sol.cg_causas_rechazo_notif AS causas_rechazo_notif, ");
			qrySentencia.append(" ex.CG_CAUSA_RECHAZO_EXT AS causasRechazo, ");
			qrySentencia.append(" TO_CHAR (ex.DF_EXTINCION_CONTRATO, 'DD/MM/YYYY') AS fextionContrato ");
			qrySentencia.append(" , sol.CG_CORREOEXT as  correo ");
			qrySentencia.append(" , sol.cg_empresas_representadas as empresas ");
			qrySentencia.append(" FROM cder_solicitud sol, ");
			qrySentencia.append(" comcat_epo epo, ");
			qrySentencia.append(" comcat_if cif, ");
			qrySentencia.append(" comcat_pyme pym, ");
			qrySentencia.append(" comrel_pyme_epo cpe, ");
			qrySentencia.append(" cdercat_tipo_contratacion tcn, ");
			qrySentencia.append(" cdercat_clasificacion_epo cep,");
			qrySentencia.append(" cdercat_estatus_solic sts, ");
			qrySentencia.append(" cdercat_tipo_plazo stp,  ");
			qrySentencia.append(" CDER_EXTINCION_CONTRATO ex ");
			qrySentencia.append(" WHERE sol.ic_epo = epo.ic_epo ");
			qrySentencia.append(" AND sol.ic_if = cif.ic_if ");
			qrySentencia.append(" AND sol.ic_pyme = pym.ic_pyme ");
			qrySentencia.append(" AND sol.ic_epo = cpe.ic_epo(+) ");
			qrySentencia.append(" AND sol.ic_pyme = cpe.ic_pyme(+) ");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion ");
			qrySentencia.append(" AND sol.ic_epo = cep.ic_epo ");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo ");
			qrySentencia.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic ");
			qrySentencia.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+) ");
			qrySentencia.append(" AND sol.ic_solicitud = ex.ic_solicitud ");
			qrySentencia.append(" and sol.ic_solicitud =? ");
			qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC ");
			
			lvarbind.add(claveSolicitud);					
			ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				datos = new ArrayList();
				//obtengo el monto 
				StringBuffer montosPorMoneda = this.getMontoMoneda(rs.getString("clave_solicitud"));
						
				datos.add(rs.getString("clave_solicitud")==null?"":rs.getString("clave_solicitud")); //0
				datos.add(rs.getString("nombre_if")==null?"":rs.getString("nombre_if")); //1
				datos.add(rs.getString("nombre_pyme")==null?"":rs.getString("nombre_pyme")); //2
				datos.add(rs.getString("rfc")==null?"":rs.getString("rfc"));		 //3
				datos.add(rs.getString("fecha_sol_pyme")==null?"":rs.getString("fecha_sol_pyme"));		 //4
				datos.add(rs.getString("numero_contrato")==null?"":rs.getString("numero_contrato")); //5
				datos.add(montosPorMoneda.toString()); //6
				datos.add(rs.getString("tipo_contratacion")==null?"":rs.getString("tipo_contratacion")); //7
				datos.add(rs.getString("fecha_inicio_contrato")==null?"":rs.getString("fecha_inicio_contrato"));  //8
				datos.add(rs.getString("fecha_fin_contrato")==null?"":rs.getString("fecha_fin_contrato"));  //9
				datos.add(rs.getString("plazo_contrato")==null?"":rs.getString("plazo_contrato")); //10 
				datos.add(rs.getString("clasificacion_epo")==null?"":rs.getString("clasificacion_epo")); //11
				datos.add(rs.getString("campo_adicional_1")==null?"":rs.getString("campo_adicional_1")); //12
				datos.add(rs.getString("campo_adicional_2")==null?"":rs.getString("campo_adicional_2"));//13
				datos.add(rs.getString("campo_adicional_3")==null?"":rs.getString("campo_adicional_3"));//14
				datos.add(rs.getString("campo_adicional_4")==null?"":rs.getString("campo_adicional_4")); //15
				datos.add(rs.getString("campo_adicional_5")==null?"":rs.getString("campo_adicional_5")); //16
				datos.add(rs.getString("objeto_contrato")==null?"N/A":rs.getString("objeto_contrato")); //17
				datos.add(rs.getString("fextionContrato")==null?"N/A":rs.getString("fextionContrato")); //18
				datos.add(rs.getString("numero_cuenta")==null?"N/A":rs.getString("numero_cuenta")); //19
				datos.add(rs.getString("clabe")==null?"N/A":rs.getString("clabe")); //20
				datos.add(rs.getString("banco_deposito")==null?"N/A":rs.getString("banco_deposito")); //21
				datos.add(rs.getString("causasrechazo")==null?"N/A":rs.getString("causasrechazo"));	 //22
				datos.add(rs.getString("estatus_solicitud")==null?"N/A":rs.getString("estatus_solicitud"));	 //23
				datos.add(rs.getString("correo")==null?"N":rs.getString("correo"));	//24
				datos.add(rs.getString("clave_pyme")==null?"N/A":rs.getString("clave_pyme"));	 //25
				datos.add(rs.getString("empresas")==null?"":rs.getString("empresas")); //26
				registros.add(datos);
			}
			rs.close();
			ps.close();
		} catch(Exception e) {
			log.error("consExtiPreacuseEpoVen (Exception) " + e);			
			throw new AppException("consExtiPreacuseEpoVen(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consExtiPreacuseEpoVen (S)");
		}
		return registros;	
	}



	/**
	 * metodo que forma el cuerpo del envio de correo en Epo Ventenilla	
	 * cuando es Extincion de Contrato
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param comentarios
	 * @param correoPara
	 * @param correoDe
	 * @param claveSolicitud
	 */
	public String  EnviodeCorreoEpoVenEXT (String claveEpo, String claveSolicitud,  String correoPara, 
											String comentarios,  String asunto )	throws AppException	{

		log.info("EnviodeCorreoEpoVenEXT (E)");
	
		PreparedStatement ps = null;			
		ResultSet rs        = null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		StringBuffer qrySentencia = new StringBuffer();
		List registros = new ArrayList();
		List datos = new ArrayList();
		String respuesta = "";
		List lvarbind = new ArrayList();
		String codigoHTML = "";
		String mensajeCorreo2 	= 	"";
			
		String color = "";
		String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
		"font-size: 10px; "+
		"font-weight: bold;"+
		"color: #FFFFFF;"+
		"background-color: #4d6188;"+
		"padding-top: 3px;"+
		"padding-right: 1px;"+
		"padding-bottom: 1px;"+
		"padding-left: 3px;"+
		"height: 25px;"+
		"border: 1px solid #1a3c54;'";

		String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
		"color:#000066; "+
		"padding-top:1px; "+
		"padding-right:2px; "+
		"padding-bottom:1px; "+
		"padding-left:2px; "+
		"height:22px; "+
		"font-size:11px;'";

		String styleRegistros = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
		"color:#000066; "+
		"padding-top:1px; "+
		"padding-right:2px; "+
		"padding-bottom:1px; "+
		"padding-left:2px; "+
		"height:22px; "+
		"font-size:11px; ";

		String fechaHoy  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

		try {
			con.conexionDB();
			 
			String nombreUsuario ="";	
						
			UtilUsr utilUsr = new UtilUsr();
			boolean usuarioEncontrado = false;
			boolean perfilIndicado = false;
			List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(claveEpo, "E");
			for (int i = 0; i < usuariosPorPerfil.size(); i++) {
				String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);					
				Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
				perfilIndicado = true;
				nombreUsuario = usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno();											
			}
			
			//obtiene los registros 
			registros = this.consExtiPreacuseEpoVen(claveSolicitud, claveEpo);
			
			//Campos Adicionales por EPO
			HashMap campos_adicionales = this.getCamposAdicionalesParametrizados(claveEpo, "9");
			int indice = Integer.parseInt((String)campos_adicionales.get("indice"));
			int num_campos_adicionales = Integer.parseInt((String)campos_adicionales.get("indice"));
			
			//titulo de la clasificacion EPO
			String clasificacionEpo = this.clasificacionEpo(claveEpo);
			if(clasificacionEpo ==null){   clasificacionEpo=""; }
				
				
			if (registros.size()>0){
					
				StringBuffer mensajeCorreo =  new StringBuffer(); 
			
				mensajeCorreo.append("<html>");
				mensajeCorreo.append("<head>");
				mensajeCorreo.append("<title>");
				mensajeCorreo.append(asunto);
				mensajeCorreo.append("</title>");
				mensajeCorreo.append("</head>");
	
				mensajeCorreo.append("<body>"); 
	
				mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='0' align='center' width='700'>");
				mensajeCorreo.append("<tr><td "+style+">Estimado (a): <b>"+nombreUsuario+"</b></td></tr>");				
				mensajeCorreo.append("<tr><td "+style+" width='500'>"+comentarios+"</td></tr>");
				mensajeCorreo.append("</table>");
					 
				mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='1' align='center' width='700'>");
				mensajeCorreo.append("<tr>");
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Intermediario Financiero (Cesionario)<b></td>");
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>PYME (Cedente)<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>RFC<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Representante Legal<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>No. de Contrato<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Monto / Moneda<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Tipo de Contrataci�n<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Fecha Inicio Contrato<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Fecha Final Contrato<b></td>");
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Plazo del Contrato<b></td>");
				if(!clasificacionEpo.equals("")){
					mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>"+clasificacionEpo+"<b></td>");				
				}
				if(indice > 0){
					indice = Integer.parseInt((String)campos_adicionales.get("indice"));
					for(int i = 0; i < indice; i++){		
						String nombreCampo = campos_adicionales.get("nombre_campo_"+i).toString();					
						mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>"+nombreCampo+"<b></td>");				
					}		
				}
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Objeto del Contrato<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Fecha de Extinci�n del contrato<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Cuenta<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Cuenta CLABE<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Banco de Deposito<b></td>");				
				mensajeCorreo.append("<td "+style+" "+styleEncabezados+" width='500' align='center' ><b>Estatus<b></td>");				
				mensajeCorreo.append("</tr>");
				
	
				for(int i = 0; i< registros.size(); i++){	
					datos = (List)registros.get(i);				
					//Se obtiene el representante Legal de la Pyme
					String noProveedor = (String)datos.get(25);
					StringBuffer nombreContacto = new StringBuffer();
					List usuariosPorPerfil2 = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for (int y = 0; y < usuariosPorPerfil2.size(); y++) {
						String loginUsuarioEpo = (String)usuariosPorPerfil2.get(y);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(y>0){
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+"  ");	
						nombreContacto.append("\n");
					}
					String pyme = (String)datos.get(2);
					String empresas = (String)datos.get(26);
					String pymeEmpresas =pyme+((!empresas.equals(""))?";"+empresas:"");
					mensajeCorreo.append("<tr>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(1)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+pymeEmpresas.replaceAll(";","<br><br>")+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(3)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+nombreContacto+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(5)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(6)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(7)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(8)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(9)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(10)+"</td>");
					
					if(!clasificacionEpo.equals("")){
						mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(11)+"</td>");
					}
					if(indice > 0){
						for(int j = 0; j < num_campos_adicionales; j++){
							mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(+(11 + 1+j))+"</td>");
						} 
					}				
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(17)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(18)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(19)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(20)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(21)+"</td>");
					mensajeCorreo.append("<td "+style+" width='500'>"+(String)datos.get(23)+"</td>");
					mensajeCorreo.append("</tr>");						
				}			
				mensajeCorreo.append("</table>");  				
				mensajeCorreo.append("</body>");
				mensajeCorreo.append("</html>"); 
		
				mensajeCorreo2 = this.convierteAcentosACodigoHTML(mensajeCorreo.toString()); 
					
			}
				
			Correo correo = new Correo();
				
			if(!correoPara.equals("")){
				List correos = new VectorTokenizer(correoPara, "\n").getValuesVector();
				for(int i = 0; i < correos.size(); i++) 	{
					if(correos.get(i)!=null){						
						List unCorreo = new VectorTokenizer((String)correos.get(i), ",").getValuesVector();
						for(int e = 0; e < unCorreo.size(); e++) 	{	
							String unCorreo1   = (unCorreo.size()>= 1)?unCorreo.get(e)==null?"":((String)unCorreo.get(e)).trim():"";				 
							if(!unCorreo1.equals("")){
								correo.enviarTextoHTML("cadenas@nafin.gob.mx", unCorreo1, asunto, mensajeCorreo2);
							}
						}
					}
				}	
			}
				
			qrySentencia = new StringBuffer();
			lvarbind = new ArrayList();
			qrySentencia.append(" UPDATE  cder_solicitud ");
			qrySentencia.append(" SET CG_CORREOEXT = ? ");					
			qrySentencia.append(" WHERE ic_solicitud = ? ");
			log.debug( lvarbind);
			lvarbind.add("S");	//bandera para saber si fue enviado el correo
			lvarbind.add(claveSolicitud);							
			ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);
			ps.executeUpdate();
			ps.close();	
				
			respuesta = "EXITO"; 
		
		}catch(Exception e){
			respuesta = "ERROR"; 
			log.error("EnviodeCorreoEXT (Exception) " + e);			
			throw new AppException("EnviodeCorreoEXT(Exception) ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
		log.info("EnviodeCorreoEXT (E)");		
		return respuesta;	
}


	/**
	 * metodo que regresa el acuse cuando  es Extioncion de Contrato
	 * epo Ventanilla 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param parametrosFirma
	 */

	public String acuseExtinContratoEPOVen(List parametrosFirma) throws AppException{
	
		log.info("acuseExtinContratoEPOVen(E)");
		
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		String acuse = "";
			
		String _serial = (String)parametrosFirma.get(0);
		String textoPlano = (String)parametrosFirma.get(1);
		String pkcs7 =  (String)parametrosFirma.get(2);
		String folioCert =  (String)parametrosFirma.get(3);
		String validCert =  (String)parametrosFirma.get(4);
		String claveSolicitud =  (String)parametrosFirma.get(5);
		String loginUsuario =  (String)parametrosFirma.get(6);
		String nombreUsuario =  (String)parametrosFirma.get(7);
		String tipoOperacion =  (String)parametrosFirma.get(8);
		String causasRechazo= (String)parametrosFirma.get(9);
	
		int seqNEXTVAL = 0;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;

		try{
			con.conexionDB();
		
			String qry = "SELECT SEQ_CDER_ACUSE.nextval FROM dual";
			ps1 = con.queryPrecompilado(qry);
			rs1 = ps1.executeQuery();
			if(rs1.next())	{
				seqNEXTVAL = rs1.getString(1)==null?1:rs1.getInt(1);
			}
			ps1.close();
			rs1.close(); 
			
			char getReceipt = 'Y';
			if(!_serial.equals("") && !textoPlano.equals("") && !pkcs7.equals("")){
				Seguridad s = new Seguridad();
				
				if( s.autenticar(folioCert, _serial, pkcs7, textoPlano, getReceipt) ){				
					acuse = s.getAcuse();	
					//if(true){ // Esta linea es para desarrollo
					//acuse = "1255245"; // Esta linea es para desarrollo
					
					if(!acuse.equals("")){						
						//modifica la tabla de cder_solicitud
						strSQL.append(" UPDATE cder_solicitud");
						strSQL.append(" SET  ic_estatus_solic = ?, ");	
						strSQL.append(" CG_CORREOEXT = 'N' ");	// veulvo a poner en N la bandera de envio de correo 
						strSQL.append(" WHERE ic_solicitud = ?");
						if(tipoOperacion.equals("R")){
							varBind.add(new Integer(20)); //Extinci�n Aceptada IF
						}else if(tipoOperacion.equals("RE") ){
							varBind.add(new Integer(22));  //Extinci�n Redireccionamiento Aplicado
						}							
						varBind.add(claveSolicitud);
						log.debug("..:: strSQL: "+strSQL);
						log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();


						//modifica la tabla de CDER_EXTINCION_CONTRATO
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						strSQL.append(" UPDATE CDER_EXTINCION_CONTRATO");
						strSQL.append(" SET df_ext_vent = SYSDATE , ");	
						strSQL.append(" cc_acuse_ext_vent = ?");
						if (tipoOperacion.equals("R")) {
							strSQL.append(", CG_CAUSA_RECHAZO_EXT = ?  ");
						}
						strSQL.append(" WHERE ic_solicitud = ?");
						
						varBind.add(acuse); 
						if(tipoOperacion.equals("R") ){
							varBind.add(causasRechazo); 
						}
						varBind.add(claveSolicitud);
						log.debug("..:: strSQL: "+strSQL);
						log.debug("..:: varBind: "+varBind);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
						
						// INSERTA TABLA ACUSE 
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						strSQL.append(" INSERT INTO cder_acuse (cc_acuse, cg_usuario, cg_nombre_usuario, cc_recibo)");
						strSQL.append(" VALUES (?, ?, ?, ?)");
						log.debug("..:: strSQL: "+strSQL);
						log.debug("..:: varBind: "+varBind);
						varBind.add(new Integer(seqNEXTVAL));
						varBind.add(loginUsuario);
						varBind.add(nombreUsuario);
						varBind.add(acuse);
						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
						
					}
				}
			}
		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error acuseExtinContratoEPOVen: ", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("acuseExtinContratoEPOVen(S)");
		}
		return acuse;
	}


	//----------------------------------------------------------------------------FODEA 030 - 2011 DLHC - F
	
	public  String generaSelloDigital(String clave_solicitud, String strNombreUsuario ) throws AppException{
	
		log.info("generaSelloDigital(E)");
		String sello_digital = "";
		try {
			StringBuffer cadena_original = new StringBuffer();
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			
		
			HashMap contratoCesion = this.consultaContratoCesionPyme(clave_solicitud);
			String clave_epo = (String)contratoCesion.get("clave_epo");
			String clave_if = (String)contratoCesion.get("clave_if");
						
			HashMap campos_adicionales = this.getCamposAdicionalesParametrizados(clave_epo, "9");
			int indice_camp_adic = Integer.parseInt((String)campos_adicionales.get("indice"));
			
			StringBuffer clausulado_parametrizado = this.consultaClausuladoParametrizado(clave_if);
			String clasificacionEpo = this.clasificacionEpo(clave_epo);

			cadena_original.append("CONTRATO DE CESI�N DE DERECHOS \n\n");
			cadena_original.append("INFORMACI�N DE LA PYME CEDENTE\n");
			cadena_original.append("NOMBRE:|"+contratoCesion.get("nombre_pyme")+"\n");
			cadena_original.append("RFC:|"+contratoCesion.get("rfcPyme")+"\n");
			cadena_original.append("REPRESENTANTE PYME:|"+(contratoCesion.get("nombreUsrAutRep1")==null?strNombreUsuario:contratoCesion.get("nombreUsrAutRep1"))+"\n");
			cadena_original.append("REPRESENTANTE PYME:|"+(contratoCesion.get("nombreUsrAutRep2")==null?(contratoCesion.get("nombreUsrAutRep1")==null?"":strNombreUsuario):contratoCesion.get("nombreUsrAutRep2"))+"\n");
			cadena_original.append("NUM. PROVEEDOR:|"+contratoCesion.get("numeroProveedor")+"\n");
			
			cadena_original.append("INFORMACI�N DEL CONTRATO");
			cadena_original.append("N�MERO DE CONTRATO:|"+contratoCesion.get("numero_contrato")+"\n");
			//cadena_original.append("MONTO:|"+Comunes.formatoDecimal(contratoCesion.get("monto_contrato"), 2)+"\n");
			//cadena_original.append("MONEDA:|"+contratoCesion.get("moneda")+"\n");
			cadena_original.append("MONTO / MONEDA:|"+contratoCesion.get("montosPorMoneda").toString().replaceAll("<br/>", "")+"\n");
			cadena_original.append("FECHA DE VIGENCIA:|"+contratoCesion.get("fecha_inicio_contrato") + " a " + contratoCesion.get("fecha_fin_contrato")+"\n");
			cadena_original.append("PLAZO DEL CONTRATO:|"+contratoCesion.get("plazoContrato")+"\n");
			cadena_original.append("TIPO DE CONTRATACI�N:|"+contratoCesion.get("tipo_contratacion").toString().toUpperCase()+"\n");
			cadena_original.append("OBJETO DEL CONTRATO:|"+contratoCesion.get("objeto_contrato").toString().toUpperCase()+"\n");
			for (int i = 0; i < indice_camp_adic; i++) {
			cadena_original.append(campos_adicionales.get("nombre_campo_"+i).toString().toUpperCase()+":|"+contratoCesion.get("campo_adicional_"+(i+1)).toString().toUpperCase()+"\n");
			}
			
			cadena_original.append("INFORMACI�N DE LA EMPRESA DE PRIMER ORDEN\n\n");
			cadena_original.append("NOMBRE:|"+contratoCesion.get("nombre_epo")+"\n");
			
			cadena_original.append("INFORMACI�N DEL CESIONARIO (IF)\n\n");
			cadena_original.append("NOMBRE:|"+contratoCesion.get("nombre_if")+"\n");
			cadena_original.append("BANCO DE DEP�SITO:|"+contratoCesion.get("bancoDeposito")+"\n");
			cadena_original.append("N�MERO DE CUENTA PARA DEP�SITO:|"+contratoCesion.get("numeroCuenta")+"\n");
			cadena_original.append("N�MERO DE CUENTA CLABE PARA DEP�SITO:|"+contratoCesion.get("numeroCuentaClabe")+"\n");
			
			cadena_original.append("INFORMACI�N DE LA CESI�N DE DERECHOS\n\n");
			cadena_original.append("FECHA DE SOLICITUD DE CONSENTIMIENTO:|"+contratoCesion.get("fechaSolicitudConsentimiento")+"\n");
			cadena_original.append("FECHA DE CONSENTIMIENTO DE LA EPO:|"+contratoCesion.get("fechaAceptacionEpo")+"\n");
			cadena_original.append("PERSONA QUE OTORG� EL CONSENTIMIENTO:|"+(contratoCesion.get("nombreUsrEpoAutCesion")==null?"":contratoCesion.get("nombreUsrEpoAutCesion"))+"\n");
			cadena_original.append("FECHA DE FORMALIZACI�N DEL CONTRATO DE CESI�N:|"+contratoCesion.get("fechaFormalContrato")+"\n");
			cadena_original.append("NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):|"+(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1"))+"\n");
			cadena_original.append("NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):|"+(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2"))+"\n");
			cadena_original.append("NOMBRE DEL FIRMANTE DE CESIONARIO (IF):|"+(contratoCesion.get("nombreUsrIf")==null?"":contratoCesion.get("nombreUsrIf"))+"\n");
			cadena_original.append("NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:|"+(contratoCesion.get("nombreUsrTest1")==null?"":contratoCesion.get("nombreUsrTest1"))+"\n");
			cadena_original.append("NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:|"+(contratoCesion.get("nombreUsrTest2")==null?"":contratoCesion.get("nombreUsrTest2"))+"\n");
			cadena_original.append("FECHA DE NOTIFICACI�N ACEPTADA POR LA EPO:|"+(contratoCesion.get("fechaNotifEpo")==null?"":contratoCesion.get("fechaNotifEpo"))+"\n");
			cadena_original.append("PERSONA QUE ACEPT� LA NOTIFICACI�N:|"+(contratoCesion.get("nombreUsrNotifEpo")==null?"":contratoCesion.get("nombreUsrNotifEpo"))+"\n");
			cadena_original.append("FECHA DE INFORMACI�N A VENTANILLA DE PAGOS:|"+(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla"))+"\n");
			cadena_original.append("FECHA DE RECEPCI�N EN VENTANILLA:|"+(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla"))+"\n");
			cadena_original.append("FECHA DE LA APLICACI�N DEL REDIRECCIONAMIENTO:|"+(contratoCesion.get("fechaRedirecccion")==null?"":contratoCesion.get("fechaRedirecccion"))+"\n");
			cadena_original.append("PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:|"+(contratoCesion.get("nombreUsrRedirec")==null?"":contratoCesion.get("nombreUsrRedirec"))+"\n");

			cadena_original.append("\nSE SUJETA A LAS SIGUIENTES CL�USULAS\n");
			cadena_original.append(clausulado_parametrizado.toString()+"\n");
			
			byte[] bytes_cadena_original = cadena_original.toString().getBytes();
			messageDigest.update(bytes_cadena_original);
			byte[] bytes_md5 = messageDigest.digest();
			byte[] bytesBase64 = org.apache.commons.codec.binary.Base64.encodeBase64(bytes_md5);
			sello_digital = new String(bytesBase64);
			
		}catch(Exception e){
			throw new AppException("Error al generar sello digital: ", e);
		}finally{
			log.info("generaSelloDigital(S)");
		}		
		return sello_digital;
	 }
	 
  /**
	 *metodo para generar el archivo de Contrato de Cesion pyme 
	 * para la pantalla de  Extinci�n de Contrato
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param datos
	 */
public String DescargaContratoCesionPyme(List datos)  throws AppException {	
	ComunesPDF pdfDoc = new ComunesPDF();
	String nombreArchivo = "";		
	log.info("DescargaContratoCesionPyme(E)");
		try {
		
		String pais  = (String)datos.get(0);
		String nafinElectronico = (String)datos.get(1);
		String noExterno = (String)datos.get(2);
		String nombre =  (String)datos.get(3);
		String nombreUsua = (String)datos.get(4);
		String logo = (String)datos.get(5);
		String strDirectorioTemp =  (String)datos.get(6);
		String strDirectorioPublicacion  = (String)datos.get(7);
		String tipoArchivo = (String)datos.get(8);
		String claveSolicitud  =(String)datos.get(9);
		String claveEpo = (String)datos.get(10);
		String claveIF = (String)datos.get(11);
				
		if (tipoArchivo.equals("CONTRATO_CESION_PYME")) {
				
				HashMap contratoCesion = this.consultaContratoCesionPyme(claveSolicitud);
				HashMap camposAdic = this.getCamposAdicionalesParametrizados(claveEpo, "9");
				StringBuffer clausuladoParametrizado = this.consultaClausuladoParametrizado(claveIF);
				String clasificacionEpo = this.clasificacionEpo(claveEpo);
				int indiceCampAdic = Integer.parseInt((String)camposAdic.get("indice"));
				
				
				CreaArchivo archivoPDF = new CreaArchivo();
				nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
				pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
									
				pdfDoc.encabezadoConImagenes(pdfDoc,pais,nafinElectronico,noExterno, nombre, nombreUsua, logo, strDirectorioPublicacion);	
					
				float anchoCelda4[] = {100f};
				//====================================================================>> TESTIGO 1 IF
				
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
					pdfDoc.setTable(1, 70, anchoCelda4);
				//	pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
					pdfDoc.setCell("NOTIFICACION " , "formas", ComunesPDF.CENTER, 1, 1, 0);		
					pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "formas", ComunesPDF.LEFT, 1, 1, 0);
					
					pdfDoc.setCell("Con base en lo dispuesto  en el C�digo de Comercio, por este conducto me permito notificar a la Entidad, por conducto de esa Unidad Administrativa adscrita a la Direcci�n Jur�dica de Petr�leos Mexicanos, el Contrato de Cesi�n Electr�nica de Derechos de Cobro, que mediante el uso de firmas electr�nicas hemos formalizado con la Empresa Cedente, respecto del Contrato referido en el apartado siguiente", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);    
					pdfDoc.setCell("Por lo anterior, me permito solicitar a usted gire sus instrucciones a fin de que una vez que surta efectos la presente Cesi�n Electr�nica de Derechos de Cobro, se registre la misma a efecto de que "+contratoCesion.get("nombre_epo")+", realice el pago de las facturas derivadas del cumplimiento y ejecuci�n del Contrato referido en el apartado inmediato anterior a la cuenta del suscrito INTERMEDIARIO FINANCIERO.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.setCell("Los datos de la cuenta en la que se deben depositar los pagos derivados de esta operaci�n se detallan en el apartado siguiente como Nombre del INTERMEDIARIO FINANCIERO, Banco de Dep�sito,  N�mero de Cuenta �PEMEX� para Dep�sito y N�mero de Cuenta CLABE.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.addTable();
				}
				
				//====================================================================>> TESTIGO 1 IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				
				
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);					
					pdfDoc.setCell((String)contratoCesion.get("firmaTest1"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> TESTIGO 2 IF
				if (contratoCesion.get("claveUsrTest2") != null && !contratoCesion.get("claveUsrTest2").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("CONTRATO DE CESI�N DE DERECHOS", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				float anchoCelda2[] = {50f, 50f};
				pdfDoc.setTable(2, 80, anchoCelda2);

				pdfDoc.setCell("INFORMACI�N DE LA PYME CEDENTE", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("RFC:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("rfcPyme"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("REPRESENTANTE PYME:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("REPRESENTANTE PYME:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NUM. PROVEEDOR:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroProveedor"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACI�N DEL CONTRATO", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("N�MERO DE CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numero_contrato"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("MONTO / MONEDA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(contratoCesion.get("montosPorMoneda").toString().replaceAll("<br/>", "\n"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE VIGENCIA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N/A".equals(contratoCesion.get("fecha_inicio_contrato"))?"N/A":(contratoCesion.get("fecha_inicio_contrato") + " a " + contratoCesion.get("fecha_fin_contrato")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PLAZO DEL CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("plazoContrato"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("TIPO DE CONTRATACI�N:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("tipo_contratacion").toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("OBJETO DEL CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("objeto_contrato").toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				for (int i = 0; i < indiceCampAdic; i++) {
					pdfDoc.setCell(camposAdic.get("nombre_campo_"+i).toString().toUpperCase() + ":", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(contratoCesion.get("campo_adicional_"+(i+1)).toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				}
				pdfDoc.setCell("INFORMACI�N DE LA EMPRESA DE PRIMER ORDEN", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("INFORMACI�N DEL CESIONARIO (IF)", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_if"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("BANCO DE DEP�SITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N�MERO DE CUENTA PARA DEP�SITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N�MERO DE CUENTA CLABE PARA DEP�SITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroCuentaClabe"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("INFORMACI�N DE LA CESI�N DE DERECHOS", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("FECHA DE SOLICITUD DE CONSENTIMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("fechaSolicitudConsentimiento"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE CONSENTIMIENTO DE LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("fechaAceptacionEpo"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE OTORG� EL CONSENTIMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrEpoAutCesion")==null?"":contratoCesion.get("nombreUsrEpoAutCesion")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE FORMALIZACI�N DEL CONTRATO DE CESI�N:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaFormalContrato")==null?"":contratoCesion.get("fechaFormalContrato")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE DE CESIONARIO (IF):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrIf")==null?"":contratoCesion.get("nombreUsrIf")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrTest1")==null?"":contratoCesion.get("nombreUsrTest1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrTest2")==null?"":contratoCesion.get("nombreUsrTest2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE NOTIFICACI�N ACEPTADA POR LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifEpo")==null?"":contratoCesion.get("fechaNotifEpo")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE ACEPT� LA NOTIFICACI�N:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrNotifEpo")==null?"":contratoCesion.get("nombreUsrNotifEpo")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE INFORMACI�N A VENTANILLA DE PAGOS:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE RECEPCI�N EN VENTANILLA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE LA APLICACI�N DEL REDIRECCIONAMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaRedirecccion")==null?"":contratoCesion.get("fechaRedirecccion")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrRedirec")==null?"":contratoCesion.get("nombreUsrRedirec")), "formas", ComunesPDF.LEFT);

				pdfDoc.addTable();

				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("SE SUJETA A LAS SIGUIENTES CLAUSULAS", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				float anchoCelda3[] = {100f};
				pdfDoc.setTable(1, 70, anchoCelda3);
				
				pdfDoc.setCell(clausuladoParametrizado.toString(), "formas", ComunesPDF.LEFT, 1);

				pdfDoc.addTable();
				
				
				//====================================================================>> REPRESENTANTE LEGAL 1 PYME
				if (contratoCesion.get("claveUsrAutRep1") != null && !contratoCesion.get("claveUsrAutRep1").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep1") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
	
					pdfDoc.setCell((String)contratoCesion.get("firmaRep1"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> REPRESENTANTE LEGAL 2 PYME
				if (contratoCesion.get("claveUsrAutRep2") != null && !contratoCesion.get("claveUsrAutRep2").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep2") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaRep2"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> REPRESENTANTE LEGAL IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}				

				pdfDoc.endDocument();
							
			}	
			
	}catch(Exception e){
			throw new AppException("Error al Descarga Contrato Cesion Pyme: ", e);
		}finally{		
			log.info("DescargaContratoCesionPyme(S)");
		}		
		return nombreArchivo;
	 }		

/**
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param datos
	 */
	public String DescargaContratoCesionAcuse(List datos)  throws AppException {	
		ComunesPDF pdfDoc = new ComunesPDF();
		String nombreArchivo = "";		
		log.info("DescargaContratoCesionAcuse(E)");
		
		String pais  = (String)datos.get(0);
		String nafinElectronico = (String)datos.get(1);
		String noExterno = (String)datos.get(2);
		String nombre =  (String)datos.get(3);
		String nombreUsua = (String)datos.get(4);
		String logo = (String)datos.get(5);
		String strDirectorioTemp =  (String)datos.get(6);
		String strDirectorioPublicacion  = (String)datos.get(7);
		String tipoArchivo = (String)datos.get(8);
		String claveSolicitud  =(String)datos.get(9);
		String claveEpo = (String)datos.get(10);
		String claveIF = (String)datos.get(11);
		String clavePyme = (String)datos.get(12);
		String mensaje = (String)datos.get(13);
		String nombre_em = (String)datos.get(14);
		String numeroAcuse= (String)datos.get(15);
		String fechaCarga= (String)datos.get(16);
		String horaCarga= (String)datos.get(17);
		String usuarioCaptura= (String)datos.get(18);
	
		try {
			
			if (tipoArchivo.equals("ACUSE_ENVIO_SOL")) {
				StringBuffer strbuffLeyendaLegal = new StringBuffer();
				
				HashMap contratoCesion = this.consultarExtincionContratosPyme(claveSolicitud);
				HashMap camposAdicionalesParametrizados = this.getCamposAdicionalesParametrizados(claveEpo, "9");
				String clasificacionEpo = this.clasificacionEpo(claveEpo);
				int indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));

				UtilUsr utilUsr = new UtilUsr();
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");
							
				String nombresRepresentantesPyme = "";
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {
					String loginUsrPyme = (String)usuariosPorPerfil.get(i);
					Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
					nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre() + ",";
				}

				CreaArchivo archivoPDF = new CreaArchivo();
				nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
				pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo);	
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
									
				pdfDoc.encabezadoConImagenes(pdfDoc,pais,nafinElectronico,noExterno, nombre_em, nombreUsua, logo, strDirectorioPublicacion);	
					
				float anchoCelda1[] = {10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f, 10f};
				int numeroColumnas = 11;
				
				pdfDoc.setTable(numeroColumnas, 100, anchoCelda1);
				
				pdfDoc.setCell("A", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Dependencia", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Intermediario \n Financiero \n (Cesionario)", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Pyme (Cedente)", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("No. de\nContrato", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Monto / Moneda", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Tipo de\nContrataci�n", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Fecha Inicio\nContrato", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Fecha Final\nContrato", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Plazo del\n Contrato", "celda02", ComunesPDF.CENTER, 1);
				pdfDoc.setCell(clasificacionEpo, "celda02", ComunesPDF.CENTER, 1);
				
				pdfDoc.setCell("B", "celda02", ComunesPDF.CENTER, 1);
				if (indiceCamposAdicionales == 0) {
					pdfDoc.setCell("Objeto del\nContrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Fecha de\n Extinci�n\n del Contrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta CLABE", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Banco de Dep�sito", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 1) {
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_0"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Objeto del\nContrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Fecha de\n Extinci�n\n del Contrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta CLABE", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Banco de Dep�sito", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 2) {
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_0"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_1"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Objeto del\nContrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Fecha de\n Extinci�n\n del Contrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta CLABE", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Banco de Dep�sito", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 3) {
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_0"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_1"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_2"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Objeto del\nContrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Fecha de\n Extinci�n\n del Contrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta CLABE", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Banco de Dep�sito", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 4) {
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_0"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_1"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_2"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_3"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Objeto del\nContrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Fecha de\n Extinci�n\n del Contrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta CLABE", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Banco de Dep�sito", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					
					pdfDoc.setCell("C", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 5) {
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_0"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_1"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_2"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_3"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)camposAdicionalesParametrizados.get("nombre_campo_4"), "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Objeto del\nContrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Fecha de\n Extinci�n\n del Contrato", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Cuenta CLABE", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					
					pdfDoc.setCell("C", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Banco de Dep�sito", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER, 1);
				}

				pdfDoc.setHeaders();
				
				String tmpEmpresas = (String)contratoCesion.get("EMPRESAS");
				tmpEmpresas = tmpEmpresas.replaceAll(";","\n");

				pdfDoc.setCell("A", "formas", ComunesPDF.CENTER, 1);
				pdfDoc.setCell((String)contratoCesion.get("nombreEpo"), "formas", ComunesPDF.CENTER, 1);
				pdfDoc.setCell((String)contratoCesion.get("nombreIfCesionario"), "formas", ComunesPDF.CENTER, 1);
				pdfDoc.setCell(tmpEmpresas, "formas", ComunesPDF.LEFT, 1);
				pdfDoc.setCell((String)contratoCesion.get("numeroContrato"), "formas", ComunesPDF.CENTER, 1);
				pdfDoc.setCell(((String)contratoCesion.get("montosPorMoneda")).replaceAll("<br/>", "\n"), "formas", ComunesPDF.LEFT, 1);
				pdfDoc.setCell((String)contratoCesion.get("tipoContratacion"), "formas", ComunesPDF.CENTER, 1);
				pdfDoc.setCell((String)contratoCesion.get("fechaInicioContrato"), "formas", ComunesPDF.CENTER, 1);
				pdfDoc.setCell((String)contratoCesion.get("fechaFinContrato"), "formas", ComunesPDF.CENTER, 1);
				pdfDoc.setCell((String)contratoCesion.get("plazoContrato"), "formas", ComunesPDF.CENTER, 1);
				pdfDoc.setCell((String)contratoCesion.get("clasificacionEpo"), "formas", ComunesPDF.CENTER, 1);
				
				pdfDoc.setCell("B", "formas", ComunesPDF.CENTER, 1);
				if (indiceCamposAdicionales == 0) {
					pdfDoc.setCell((String)contratoCesion.get("objetoContrato"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("fechaExtincion"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("cuentaClabe"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("estatusSolicitud"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 1) {
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional1"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("objetoContrato"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("fechaExtincion"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("cuentaClabe"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("estatusSolicitud"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 2) {
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional1"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional2"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("objetoContrato"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("fechaExtincion"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("cuentaClabe"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("estatusSolicitud"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 3) {
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional1"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional2"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional3"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("objetoContrato"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("fechaExtincion"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("cuentaClabe"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("estatusSolicitud"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 4) {
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional1"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional2"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional3"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional4"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("objetoContrato"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("fechaExtincion"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("cuentaClabe"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					
					pdfDoc.setCell("C", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("estatusSolicitud"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
				} else if (indiceCamposAdicionales == 5) {
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional1"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional2"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional3"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional4"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("campoAdicional5"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("objetoContrato"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("fechaExtincion"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("cuentaClabe"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					
					pdfDoc.setCell("C", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)contratoCesion.get("estatusSolicitud"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 1);
				}

				pdfDoc.addTable();
				//pdfDoc.newPage();
				//pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("\n ","formas",ComunesPDF.CENTER);

				float anchoCelda2[] = {35f, 65f};
				pdfDoc.setTable(2, 40, anchoCelda2);

				pdfDoc.setCell("Cifras de Control", "celda02", ComunesPDF.CENTER, 2);
				
				pdfDoc.setCell("N�mero de Acuse", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(numeroAcuse, "formas",ComunesPDF.CENTER);
				//pdfDoc.setCell((String)(contratoCesion.get("acuseExtRep2")==null?contratoCesion.get("acuseExtRep1"):contratoCesion.get("acuseExtRep2")), "formas",ComunesPDF.CENTER);
				
				pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(fechaCarga, "formas",ComunesPDF.CENTER);
				//pdfDoc.setCell((String)(contratoCesion.get("fechaExtRep2")==null?contratoCesion.get("fechaExtRep1"):contratoCesion.get("fechaExtRep2")), "formas", ComunesPDF.CENTER);

				pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(horaCarga, "formas",ComunesPDF.CENTER);
				//pdfDoc.setCell((String)(contratoCesion.get("horaExtRep2")==null?contratoCesion.get("horaExtRep1"):contratoCesion.get("horaExtRep2")), "formas", ComunesPDF.CENTER);

				pdfDoc.setCell("Usuario", "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(usuarioCaptura, "formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(contratoCesion.get("nombreUsrExtRep2")==null?(contratoCesion.get("claveUsrExtRep1") + " - " + contratoCesion.get("nombreUsrExtRep1")):(contratoCesion.get("claveUsrExtRep2") + " - " + contratoCesion.get("nombreUsrExtRep2")), "formas",ComunesPDF.CENTER);

				pdfDoc.addTable();

				pdfDoc.addText("\n ","formas",ComunesPDF.CENTER);

				float anchoCelda3[] = {100f};
				pdfDoc.setTable(1, 100, anchoCelda3); 

				pdfDoc.setCell(mensaje, "formas", ComunesPDF.JUSTIFIED, 1, 1, 1);

				pdfDoc.addTable();

				pdfDoc.endDocument();
				
				
			}	
				
		}catch(Exception e){
				throw new AppException("Error al Descarga Contrato CesionAcuse: ", e);
		}finally{		
			log.info("DescargaContratoCesionAcuse(S)");
		}		
		return nombreArchivo;
	}		

	/**
	 *
	 * Actualiza el Nombre de la Clasificaci�n EPO.
	 * @param claveEPO <tt>String</tt> con el ID de la EPO ( como aparece en <tt>COMCAT_EPO.IC_EPO</tt> )
	 * @param nombreClasificacionEPO  <tt>String</tt> con el nombre de la clasificaci�n de la EPO
	 * 
	 * @return <tt>boolean</tt> con valor <tt>true</tt> si se pudo actualizar la clasificaci�n y <tt>false</tt> 
	 * en caso contrario.
	 *
	 */
	public boolean actualizarNombreClasificacionEpo(String claveEPO, String nombreClasificacionEPO) {
		
		log.info("actualizarNombreClasificacionEpo(E)");
		
		AccesoDB 			con 	= new AccesoDB();
		PreparedStatement ps	 	= null;
		StringBuffer		query	= new StringBuffer();
		boolean				exito = true;
 
		try {
			
			// Validar que se haya especificado la clave de la EPO
			if( claveEPO == null || claveEPO.trim().equals("") ){
				throw new Exception("La Clave de la EPO no puede venir vac�a");
			}
		
			// Conectarse a la base de datos
			con.conexionDB();
 
			// Actualizar nombre de la clasificaci�n
			query.append(" update COMCAT_EPO set CG_CLASIFICACION_EPO = ? where IC_EPO = ? ");
			ps = con.queryPrecompilado(query.toString());
			if( nombreClasificacionEPO != null ){
				ps.setString(1, nombreClasificacionEPO		);
			} else {
				ps.setNull(1,	 Types.VARCHAR 				);
			}
			ps.setInt(2,	 Integer.parseInt(claveEPO));
			ps.executeUpdate();
			
		} catch( Exception e ){
			
			exito = false;
			
			log.error("actualizarNombreClasificacionEpo(Exception)");
			log.error("actualizarNombreClasificacionEpo.claveEPO               = <" + claveEPO+ ">");
			log.error("actualizarNombreClasificacionEpo.nombreClasificacionEPO = <" + nombreClasificacionEPO+ ">");
			e.printStackTrace();
 
			//throw new AppException("Ocurri� un error al actualizar el Nombre de la Clasificaci�n EPO");
			
		} finally {
			
			if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
			
			if( con.hayConexionAbierta() ){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			
		}
		log.info("actualizarNombreClasificacionEpo(S)");
		
		return exito;
		
	}

	/**
	 * metodo que regresa si ya se envio correo de una solicitud
	 * Extincion de contrato como EPO
	 * @return 'S' o 'N' o ''
	 * @param claveSolicitud
	 */
	public String consultaEnvioCorreo (String claveSolicitud){
		log.info("consultaEnvioCorreo (E)");
		PreparedStatement ps = null;			
		ResultSet rs			= null;
		AccesoDB con			= new AccesoDB();
		String resultado		= "";
		List lvarbind			= new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();
		
		
		try {
			con.conexionDB();
			
		 	qrySentencia.append(" SELECT sol.CG_CORREOEXT as correo ");
			qrySentencia.append(" FROM cder_solicitud sol ");
			qrySentencia.append(" WHERE sol.ic_solicitud = ? ");

			lvarbind.add(claveSolicitud);					
			
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: lvarbind: "+lvarbind);
			
			ps = con.queryPrecompilado(qrySentencia.toString(),lvarbind);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				resultado = rs.getString("correo")==null?"N":rs.getString("correo");
			}

			rs.close();
			ps.close();
		} catch(Exception e) {
			log.error("consultaEnvioCorreo (Exception) " + e);			
			throw new AppException("consultaEnvioCorreo(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultaEnvioCorreo (S)");
		}
		return resultado;	
	}

	public String  archNotifiEPO ( HashMap parametros)  throws AppException {	
		String nombreArchivo ="";
		CreaArchivo archivo = new CreaArchivo();
	
		try {

			StringBuffer clausuladoParametrizado = new StringBuffer();
			String clave_solicitud = parametros.get("clave_solicitud")==null?"":(String)parametros.get("clave_solicitud");
			String claveEpo = parametros.get("claveEpo")==null?"":(String)parametros.get("claveEpo");
			String acuse = parametros.get("acuse")==null?"":(String)parametros.get("acuse");
			String hora = parametros.get("hora")==null?"":(String)parametros.get("hora");
			String fecha = parametros.get("fecha")==null?"":(String)parametros.get("fecha");
			String nombreUsuario = parametros.get("nombreUsuario")==null?"":(String)parametros.get("nombreUsuario");
			String	loginUsuario =  parametros.get("loginUsuario")==null?"":(String)parametros.get("loginUsuario");
			String claveIf = parametros.get("claveIf")==null?"":(String)parametros.get("claveIf");
			String strDirectorioTemp = parametros.get("strDirectorioTemp")==null?"":(String)parametros.get("strDirectorioTemp");
			String tipoOperacion = parametros.get("tipoOperacion")==null?"":(String)parametros.get("tipoOperacion");
			
			String pais = parametros.get("pais")==null?"":(String)parametros.get("pais");   
			String noCliente = parametros.get("noCliente")==null?"":(String)parametros.get("noCliente");  
			String nombre = parametros.get("nombre")==null?"":(String)parametros.get("nombre");  
			String nombreUsr = parametros.get("nombreUsr")==null?"":(String)parametros.get("nombreUsr"); 
			String logo = parametros.get("logo")==null?"":(String)parametros.get("logo");  
			String strDirectorioPublicacion =  parametros.get("strDirectorioPublicacion")==null?"":(String)parametros.get("strDirectorioPublicacion");  
			
			HashMap camposAdicionalesEpo = this.getCamposAdicionalesParametrizados(claveEpo, "9");
			int indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesEpo.get("indice"));
					
			HashMap consultaNotificaciones = this.consultaNotificacionEpo(clave_solicitud);
					
			int num_registros = Integer.parseInt((String)consultaNotificaciones.get("indice"));
					
			if(!claveIf.equals("")){
			clausuladoParametrizado = this.consultaClausuladoParametrizado(claveIf);
			}
					
			String clasificacionEpo = this.clasificacionEpo(claveEpo);
					
			nombreArchivo = archivo.nombreArchivo()+".pdf";
			
			ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
			
				
				
			pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
				
			pdfDoc.addText("La autentificaci�n se llev� a cabo con �xito \n Recibo: "+acuse, "titulo", ComunesPDF.CENTER);

			float anchoCeldaA[] = {5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f};
			pdfDoc.setTable(17, 100, anchoCeldaA);
				
			pdfDoc.setCell("A", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Intermediario Financiero(Cesionario)", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Pyme (Cedente)", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("RFC", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("No. Proveedor", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Solicitud PyME", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("No. Contrato", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Firma Contrato", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto / Moneda", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Contrataci�n", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Inicio Contrato", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Final Contrato", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo del Contrato", "celda02", ComunesPDF.CENTER);
			pdfDoc.setCell(clasificacionEpo, "celda02", ComunesPDF.CENTER);
			
			pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);
			pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);
			
			pdfDoc.setCell("B", "celda02", ComunesPDF.CENTER);
				for(int i = 0; i < indiceCamposAdicionales; i++){pdfDoc.setCell(camposAdicionalesEpo.get("nombre_campo_"+i) + "", "celda02", ComunesPDF.CENTER);}
				pdfDoc.setCell("Ventanilla de Pago", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Supervisor/Administrador/Residente de Obra", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Objeto del Contrato", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Comentarios", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto del Cr�dito", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia / No. Cr�dito", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento Cr�dito", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Banco de Dep�sito", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta CLABE", "celda02", ComunesPDF.CENTER);
				for(int i = 0; i < 5 - indiceCamposAdicionales; i++){pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);}

				pdfDoc.setCell("C", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("Observaciones", "celda02", ComunesPDF.CENTER);
				if (!tipoOperacion.equals("ACEPTAR")) {
					pdfDoc.setCell("Causas de Rechazo Notificaci�n", "celda02", ComunesPDF.CENTER);
				} else {
					pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				}
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.CENTER);
				pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);
				pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);
				pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);
				
				pdfDoc.setHeaders();
				
				for(int i = 0; i < num_registros; i++){
					pdfDoc.setCell("A", "formas", ComunesPDF.CENTER);
					String pyme= consultaNotificaciones.get("nombrePyme")==null?"":consultaNotificaciones.get("nombrePyme").toString().replaceAll(",", "");
					String empresas =consultaNotificaciones.get("empresas")==null?"":consultaNotificaciones.get("empresas").toString().replaceAll(",", "");
					String cedente =pyme+((!empresas.equals(""))?";"+empresas:"");
					pdfDoc.setCell(consultaNotificaciones.get("nombreIf")==null?"":consultaNotificaciones.get("nombreIf").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(cedente.replaceAll(";","\n") + "", "formas", ComunesPDF.LEFT);//pdfDoc.setCell(consultaNotificaciones.get("nombrePyme")==null?"":consultaNotificaciones.get("nombrePyme").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("rfc")==null?"":consultaNotificaciones.get("rfc").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("numeroProveedor")==null?"":consultaNotificaciones.get("numeroProveedor").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("fechaSolicitudPyme")==null?"":consultaNotificaciones.get("fechaSolicitudPyme").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("numeroContrato")==null?"":consultaNotificaciones.get("numeroContrato").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("firma_contrato")==null?"":consultaNotificaciones.get("firma_contrato") + "", "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(consultaNotificaciones.get("montosPorMoneda")==null?"":consultaNotificaciones.get("montosPorMoneda").toString().replaceAll("<br/>", "\n") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("tipoContratacion")==null?"":consultaNotificaciones.get("tipoContratacion") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("fechaInicioContrato")==null?"":consultaNotificaciones.get("fechaInicioContrato") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("fechaFinContrato")==null?"":consultaNotificaciones.get("fechaFinContrato") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("plazoContrato")==null?"":consultaNotificaciones.get("plazoContrato") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("clasificacionEpo")==null?"":consultaNotificaciones.get("clasificacionEpo") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					
					pdfDoc.setCell("B", "formas", ComunesPDF.CENTER);
					for(int j = 0; j < indiceCamposAdicionales; j++){pdfDoc.setCell(consultaNotificaciones.get("campoAdicional"+(j + 1))==null?"":consultaNotificaciones.get("campoAdicional"+(j + 1)) + "", "formas", ComunesPDF.LEFT);}
					pdfDoc.setCell(consultaNotificaciones.get("venanillaPago")==null?"":consultaNotificaciones.get("venanillaPago").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("supAdmResob")==null?"":consultaNotificaciones.get("supAdmResob").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("numeroTelefono")==null?"":consultaNotificaciones.get("numeroTelefono").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("objetoContrato")==null?"":consultaNotificaciones.get("objetoContrato").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("cg_comentarios")==null?"":consultaNotificaciones.get("cg_comentarios").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("$" + consultaNotificaciones.get("montoCredito")==null?"0":consultaNotificaciones.get("montoCredito").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("numeroReferencia")==null?"":consultaNotificaciones.get("numeroReferencia").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("fechaVencimiento")==null?"":consultaNotificaciones.get("fechaVencimiento").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("bancoDeposito")==null?"":consultaNotificaciones.get("bancoDeposito").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("numeroCuenta")==null?"":consultaNotificaciones.get("numeroCuenta").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("clabe")==null?"":consultaNotificaciones.get("clabe").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					for(int j = 0; j < 5 - indiceCamposAdicionales; j++){pdfDoc.setCell("", "formas", ComunesPDF.CENTER);}

					pdfDoc.setCell("C", "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(consultaNotificaciones.get("estatusSolicitud")==null?"":consultaNotificaciones.get("estatusSolicitud") + "", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(consultaNotificaciones.get("observacionesRechazo")==null?"":consultaNotificaciones.get("observacionesRechazo") + "", "formas", ComunesPDF.LEFT);
					if (!tipoOperacion.equals("ACEPTAR")) {
						pdfDoc.setCell(consultaNotificaciones.get("causasRechazoNotificacion")==null?"":consultaNotificaciones.get("causasRechazoNotificacion").toString().replaceAll(",", "") + "", "formas", ComunesPDF.LEFT);
					} else {
						pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					}
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
				
					acuse = consultaNotificaciones.get("acuseRedirec")==null?"":consultaNotificaciones.get("acuseRedirec").toString();
				}
				pdfDoc.addTable();

				float anchoCelda1[] = {50f, 50f};
				pdfDoc.setTable(2, 50, anchoCelda1);
				
				pdfDoc.setCell("Cifras de Control", "celda02", ComunesPDF.CENTER, 2);				
				pdfDoc.setCell("N�mero de Acuse", "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell(acuse, "formas",ComunesPDF.LEFT);				
				pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell(fecha, "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell(hora, "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Usuario", "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell(loginUsuario + " - " + nombreUsuario, "formas",ComunesPDF.LEFT);
				pdfDoc.addTable();

				pdfDoc.endDocument();
				

	
			}catch(Exception e){
				throw new AppException("Error al Descarga Contrato Cesion Pyme: ", e);
			}finally{		
			log.info("DescargaContratoCesionPyme(S)");
			}		
		return nombreArchivo;
	}

	/**
	 * Migracin IF cCOntrato de Cesion IF 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param parametros
	 */

	public String  archConCesionIF ( HashMap parametros)  throws AppException {	
		String nombreArchivo ="";  
		CreaArchivo archivo = new CreaArchivo();
		System.out.println("DescargaContratoCesionIF Acuse (E)");
		
		String tipo_archivo = parametros.get("tipo_archivo")==null?"":parametros.get("tipo_archivo").toString();
		String clave_solicitud = parametros.get("clave_solicitud")==null?"":parametros.get("clave_solicitud").toString();
		String recibo_carga = parametros.get("recibo_carga")==null?"":parametros.get("recibo_carga").toString();
		String fecha_carga = parametros.get("fecha_carga")==null?"":parametros.get("fecha_carga").toString();
		String tipoOperacion = parametros.get("tipoOperacion")==null?"":parametros.get("tipoOperacion").toString();
		String nombre_usuario = parametros.get("nombre_usuario")==null?"":parametros.get("nombre_usuario").toString();
		String strDirectorioTemp = parametros.get("strDirectorioTemp")==null?"":parametros.get("strDirectorioTemp").toString();
		String pais = parametros.get("pais")==null?"":parametros.get("pais").toString();
		String noCliente = parametros.get("noCliente")==null?"":parametros.get("noCliente").toString();
		String nombre = parametros.get("nombre")==null?"":parametros.get("nombre").toString();
		String nombreUsr = parametros.get("nombreUsr")==null?"":parametros.get("nombreUsr").toString();
		String logo = parametros.get("logo")==null?"":parametros.get("logo").toString();
		String strDirectorioPublicacion = parametros.get("strDirectorioPublicacion")==null?"":parametros.get("strDirectorioPublicacion").toString();
		String clave_epo ="", clave_if ="";
		
				
		try {
		
			nombreArchivo = archivo.nombreArchivo()+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
			pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
				
			if (tipo_archivo.equals("CONTRATO_CESION_IF")) {
			
				HashMap contratoCesion = this.consultaContratoCesionPyme(clave_solicitud);
				int indice_camp_adic = 0;
				String clasificacionEpo ="", claveEstatus ="";
				HashMap campos_adic =  new HashMap();
				StringBuffer clausulado_parametrizado =  new  StringBuffer();
				
				pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
							
				if(contratoCesion.size()>1){
				clave_epo = (String)contratoCesion.get("clave_epo");
				clave_if = (String)contratoCesion.get("clave_if");
				claveEstatus = (String)contratoCesion.get("clave_estatus");
				
				campos_adic = this.getCamposAdicionalesParametrizados(clave_epo, "9");
				clausulado_parametrizado = this.consultaClausuladoParametrizado(clave_if);
				clasificacionEpo = this.clasificacionEpo(clave_epo);
				
				 indice_camp_adic = Integer.parseInt((String)campos_adic.get("indice"));
				float anchoCelda4[] = {100f};
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
					pdfDoc.setTable(1, 70, anchoCelda4);
					pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
					pdfDoc.setCell("NOTIFICACION " , "formas", ComunesPDF.CENTER, 1, 1, 0);		
					pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "formas", ComunesPDF.LEFT, 1, 1, 0);	 //nombre de la EPo
					
					pdfDoc.setCell("Asunto: "+"Notificaci�n de Cesi�n de Derechos de Cobro" , "formas", ComunesPDF.LEFT, 1, 1, 0);		
					pdfDoc.setCell("Con base en lo dispuesto  en el C�digo de Comercio, por este conducto me permito notificar a la Entidad, por conducto de esa Unidad Administrativa adscrita a la Direcci�n Jur�dica de Petr�leos Mexicanos, el Contrato de Cesi�n Electr�nica de Derechos de Cobro, que mediante el uso de firmas electr�nicas hemos formalizado con la Empresa Cedente, respecto del Contrato referido en el apartado siguiente", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);    
					pdfDoc.setCell("Por lo anterior, me permito solicitar a usted gire sus instrucciones a fin de que una vez que surta efectos la presente Cesi�n Electr�nica de Derechos de Cobro, se registre la misma a efecto de que "+contratoCesion.get("nombre_epo")+", realice el pago de las facturas derivadas del cumplimiento y ejecuci�n del Contrato referido en el apartado inmediato anterior a la cuenta del suscrito INTERMEDIARIO FINANCIERO.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.setCell("Los datos de la cuenta en la que se deben depositar los pagos derivados de esta operaci�n se detallan en el apartado siguiente como Nombre del INTERMEDIARIO FINANCIERO, Banco de Dep�sito,  N�mero de Cuenta �PEMEX� para Dep�sito y N�mero de Cuenta CLABE.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.addTable();
				}
			
				//====================================================================>> REPRESENTANTE LEGAL IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")&&contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				
				//====================================================================>> TESTIGO 1 IF
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaTest1"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> TESTIGO 2 IF
				if (contratoCesion.get("claveUsrTest2") != null && !contratoCesion.get("claveUsrTest2").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("CONTRATO DE CESI�N DE DERECHOS", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				float anchoCelda2[] = {50f, 50f};
				pdfDoc.setTable(2, 80, anchoCelda2);

				pdfDoc.setCell("INFORMACI�N DE LA PYME CEDENTE", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("RFC:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("rfcPyme"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("REPRESENTANTE PYME:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("REPRESENTANTE PYME:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NUM. PROVEEDOR:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroProveedor"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACI�N DEL CONTRATO", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("N�MERO DE CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numero_contrato"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("MONTO / MONEDA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(contratoCesion.get("montosPorMoneda").toString().replaceAll(",", "").replaceAll("<br/>", "\n"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE VIGENCIA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N/A".equals(contratoCesion.get("fecha_inicio_contrato"))?"N/A":(contratoCesion.get("fecha_inicio_contrato") + " a " + contratoCesion.get("fecha_fin_contrato")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PLAZO DEL CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("plazoContrato"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("TIPO DE CONTRATACI�N:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("tipo_contratacion").toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("OBJETO DEL CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("objeto_contrato").toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				for (int i = 0; i < indice_camp_adic; i++) {
					pdfDoc.setCell(campos_adic.get("nombre_campo_"+i).toString().toUpperCase() + ":", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(contratoCesion.get("campo_adicional_"+(i+1)).toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				}
				
				pdfDoc.setCell("INFORMACI�N DE LA EMPRESA DE PRIMER ORDEN", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACI�N DEL CESIONARIO (IF)", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_if"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("BANCO DE DEP�SITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N�MERO DE CUENTA PARA DEP�SITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N�MERO DE CUENTA CLABE PARA DEP�SITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroCuentaClabe"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACI�N DE LA CESI�N DE DERECHOS", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("FECHA DE SOLICITUD DE CONSENTIMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("fechaSolicitudConsentimiento"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE CONSENTIMIENTO DE LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("fechaAceptacionEpo"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE OTORG� EL CONSENTIMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrEpoAutCesion")==null?"":contratoCesion.get("nombreUsrEpoAutCesion")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE FORMALIZACI�N DEL CONTRATO DE CESI�N:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaFormalContrato")==null?"":contratoCesion.get("fechaFormalContrato")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE DE CESIONARIO (IF):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrIf")==null?"":(!claveEstatus.equals("10")?contratoCesion.get("nombreUsrIf"):"")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrTest1")==null?"":contratoCesion.get("nombreUsrTest1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrTest2")==null?"":contratoCesion.get("nombreUsrTest2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE NOTIFICACI�N ACEPTADA POR LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifEpo")==null?"":contratoCesion.get("fechaNotifEpo")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE ACEPT� LA NOTIFICACI�N:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrNotifEpo")==null?"":contratoCesion.get("nombreUsrNotifEpo")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE INFORMACI�N A VENTANILLA DE PAGOS:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE RECEPCI�N EN VENTANILLA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE LA APLICACI�N DEL REDIRECCIONAMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaRedirecccion")==null?"":contratoCesion.get("fechaRedirecccion")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrRedirec")==null?"":contratoCesion.get("nombreUsrRedirec")), "formas", ComunesPDF.LEFT);

				pdfDoc.addTable();

				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("SE SUJETA A LAS SIGUIENTES CLAUSULAS", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				float anchoCelda3[] = {100f};
				pdfDoc.setTable(1, 70, anchoCelda3);
				
				pdfDoc.setCell(clausulado_parametrizado.toString(), "formas", ComunesPDF.LEFT, 1);

				pdfDoc.addTable();
				
				   
				   //Fodea 023-2015  Cuando la solicitud tiene un grupo de pymes (E)
				   String claveGrupo =(String)contratoCesion.get("ic_grupo_cesion"); 
				   
				   UtilUsr utilUsr = new UtilUsr();
				   if(!claveGrupo.equals("")){         
				      List  lstEmpresasXGrupo = this.getEmpresasXGrupoCder(claveGrupo);
				      if(lstEmpresasXGrupo.size()>0)  {
				         for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
				            String cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
				            String nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
				            
				            List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(cvePymeGrupo, "P");
				            
				            for (int i = 0; i < usuariosPorPerfil.size(); i++) {                 
				               String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
				               Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
				               String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
				               HashMap datosRep= this.getRepresentantesCEDENTE( clave_solicitud, cvePymeGrupo, loginUsrPyme );
				               if(datosRep.size()>0) {                
				                  if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
				                     
				                     pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				                     pdfDoc.addText(" Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePymeGrupo, "formas", ComunesPDF.CENTER);
				                     pdfDoc.setTable(1, 40, anchoCelda4);
				                     pdfDoc.setCell( datosRep.get("CG_FIRMA_REPRESENTANTE").toString() , "formas", ComunesPDF.CENTER, 1);
				                     pdfDoc.addTable();
				                     
				                     
				                  }
				               }
				            }           
				         }
				      }  
				      //Fodea 023-2015  Cuando la solicitud tiene un grupo de pymes (S)
				   }else  {
				      String  clave_pyme = (String)contratoCesion.get("clave_pyme");
				      String  nombrePyme=(String)contratoCesion.get("nombre_pyme");
				      
				      List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");
				      
				      for (int i = 0; i < usuariosPorPerfil.size(); i++) {                 
				         String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
				         Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
				         String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
				         HashMap datosRep= this.getRepresentantesCEDENTE( clave_solicitud, clave_pyme, loginUsrPyme );
				         if(datosRep.size()>0) {          
				            if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
				               
				               pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				               pdfDoc.addText(" Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePyme, "formas", ComunesPDF.CENTER);
				               pdfDoc.setTable(1, 40, anchoCelda4);
				               pdfDoc.setCell( datosRep.get("CG_FIRMA_REPRESENTANTE").toString() , "formas", ComunesPDF.CENTER, 1);
				               pdfDoc.addTable();
				               
				               
				            }
				         }
				      }
				   }

				//====================================================================>> REPRESENTANTE LEGAL IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				
				
						
				}
				pdfDoc.endDocument();	
			}
		
		
			if(tipo_archivo.equals("ACUSE")){
				
				HashMap contratoCesion = this.consultaContratoCesionIf(clave_solicitud);
				
				clave_epo = (String)contratoCesion.get("clave_epo");
				clave_if = (String)contratoCesion.get("clave_if");
				
				HashMap campos_adic = this.getCamposAdicionalesParametrizados(clave_epo, "9");
				StringBuffer clausulado_parametrizado = this.consultaClausuladoParametrizado(clave_if);
				String clasificacionEpo = this.clasificacionEpo(clave_epo);
				
				int indice_camp_adic = Integer.parseInt((String)campos_adic.get("indice"));
					
				pdfDoc.addText("La autentificaci�n se llev� a cabo con �xito \n Recibo: "+recibo_carga, "titulo", ComunesPDF.CENTER);

				float anchoCelda1[] = {50f, 50f};
				pdfDoc.setTable(2, 50, anchoCelda1);
				
				pdfDoc.setCell("Cifras de Control", "celda02", ComunesPDF.CENTER, 2);
				
				pdfDoc.setCell("N�mero de Acuse", "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell((String)contratoCesion.get("acuseFirmaIf"), "formas",ComunesPDF.LEFT);
				
				pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell((String)contratoCesion.get("fechaIf"), "formas", ComunesPDF.LEFT);

				pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell((String)contratoCesion.get("horaIf"), "formas", ComunesPDF.LEFT);

				pdfDoc.setCell("Usuario", "formas", ComunesPDF.RIGHT);
				pdfDoc.setCell(contratoCesion.get("claveUsrIf") + " - " + contratoCesion.get("nombreUsrIf"), "formas",ComunesPDF.LEFT);

				pdfDoc.addTable();

				boolean banderaArchivo1=true,banderaArchivo2=false;
				archivo  = new CreaArchivo();
				//nombreArchivo = archivo.nombreArchivo()+".pdf";
				String nombreArchivo2;
				String nombreArchivo3=archivo.nombreArchivo()+".pdf";
				 //pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, false);
				 contratoCesion = this.consultaContratoCesionPyme(clave_solicitud);
				
				
					float anchoCelda4[] = {100f};
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					//====================================================================>> TESTIGO 1 IF
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")&&false) { //Se pone false por que no van las notificaciones se dejan por si en un futuro se necesitan
					pdfDoc.setTable(1, 70, anchoCelda4);
					//pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
					pdfDoc.setCell("NOTIFICACION " , "formas", ComunesPDF.CENTER, 1, 1, 0);		
					pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "formas", ComunesPDF.LEFT, 1, 1, 0);	 //nombre de la EPo
					pdfDoc.setCell("Con base en lo dispuesto  en el C�digo de Comercio, por este conducto me permito notificar a la Entidad, por conducto de esa Unidad Administrativa adscrita a la Direcci�n Jur�dica de Petr�leos Mexicanos, el Contrato de Cesi�n Electr�nica de Derechos de Cobro, que mediante el uso de firmas electr�nicas hemos formalizado con la Empresa Cedente, respecto del Contrato referido en el apartado siguiente", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);    
					pdfDoc.setCell("Por lo anterior, me permito solicitar a usted gire sus instrucciones a fin de que una vez que surta efectos la presente Cesi�n Electr�nica de Derechos de Cobro, se registre la misma a efecto de que "+contratoCesion.get("nombre_epo")+", realice el pago de las facturas derivadas del cumplimiento y ejecuci�n del Contrato referido en el apartado inmediato anterior a la cuenta del suscrito INTERMEDIARIO FINANCIERO.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.setCell("Los datos de la cuenta en la que se deben depositar los pagos derivados de esta operaci�n se detallan en el apartado siguiente como Nombre del INTERMEDIARIO FINANCIERO, Banco de Dep�sito,  N�mero de Cuenta �PEMEX� para Dep�sito y N�mero de Cuenta CLABE.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.addTable();
					banderaArchivo1=true;
				}
				//====================================================================>> TESTIGO 1 IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")&&false) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
					pdfDoc.setTable(1, 40, anchoCelda4);
					pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.addTable();
					banderaArchivo1=true;
				}
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")&&false) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
					pdfDoc.setTable(1, 40, anchoCelda4);					
					pdfDoc.setCell((String)contratoCesion.get("firmaTest1"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.addTable();
					banderaArchivo1=true;
				}
				//====================================================================>> TESTIGO 2 IF
				if (contratoCesion.get("claveUsrTest2") != null && !contratoCesion.get("claveUsrTest2").toString().equals("")&&false) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
					pdfDoc.setTable(1, 40, anchoCelda4);
					pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.addTable();
					banderaArchivo1=true;
				}
				
				pdfDoc.endDocument();
				
				if (this.getBanderaContrato(clave_solicitud).equals("S")){
					nombreArchivo2 = this.descargaContratoCesionNuevo(clave_solicitud,strDirectorioTemp);
				}else{
					nombreArchivo2 = this.generarContrato(clave_solicitud,strDirectorioTemp);
				}
				
				 ComunesPDF pdfDoc2 = new ComunesPDF(2, strDirectorioTemp + nombreArchivo3, "", false, false, false);
			               
			   //Fodea 023-2015  Cuando la solicitud tiene un grupo de pymes (E)
			   String claveGrupo =(String)contratoCesion.get("ic_grupo_cesion"); 
			   
			   UtilUsr utilUsr = new UtilUsr();
			   if(!claveGrupo.equals("")){         
			      List  lstEmpresasXGrupo = this.getEmpresasXGrupoCder(claveGrupo);
			      if(lstEmpresasXGrupo.size()>0)  {
			         for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
			            String cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
			            String nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
			            
			            List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(cvePymeGrupo, "P");
			            
			            for (int i = 0; i < usuariosPorPerfil.size(); i++) {                 
			               String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
			               Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
			               String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
			               HashMap datosRep= this.getRepresentantesCEDENTE( clave_solicitud, cvePymeGrupo, loginUsrPyme );
			               if(datosRep.size()>0) {                
			                  if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
			                     
			                     pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
			                     pdfDoc2.addText(" Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePymeGrupo, "formas", ComunesPDF.CENTER);
			                     pdfDoc2.setTable(1, 40, anchoCelda4);
			                     pdfDoc2.setCell( datosRep.get("CG_FIRMA_REPRESENTANTE").toString() , "formas", ComunesPDF.CENTER, 1);
			                     pdfDoc2.addTable();
			                     
			                     
			                  }
			               }
			            }           
			         }
			      }  
			      //Fodea 023-2015  Cuando la solicitud tiene un grupo de pymes (S)
			   }else  {
			      String  clave_pyme = (String)contratoCesion.get("clave_pyme");
			      String  nombrePyme=(String)contratoCesion.get("nombre_pyme");
			      
			      List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");
			      
			      for (int i = 0; i < usuariosPorPerfil.size(); i++) {                 
			         String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
			         Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
			         String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
			         HashMap datosRep= this.getRepresentantesCEDENTE( clave_solicitud, clave_pyme, loginUsrPyme );
			         if(datosRep.size()>0) {          
			            if(!datosRep.get("CG_FIRMA_REPRESENTANTE").toString().equals("")) {
			               
			               pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
			               pdfDoc2.addText(" Sello Digital de " + nombresRepresentantesPyme + " representante legal de " + nombrePyme, "formas", ComunesPDF.CENTER);
			               pdfDoc2.setTable(1, 40, anchoCelda4);
			               pdfDoc2.setCell( datosRep.get("CG_FIRMA_REPRESENTANTE").toString() , "formas", ComunesPDF.CENTER, 1);
			               pdfDoc2.addTable();
			            }
			         }
			      }
			   }		   
			   				
				
				//====================================================================>> REPRESENTANTE LEGAL IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
					pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
					pdfDoc2.setTable(1, 40, anchoCelda4);
					pdfDoc2.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc2.addTable();
					banderaArchivo2=true;
				}
				
				//====================================================================>> REPRESENTANTE LEGAL IF 2
				if (contratoCesion.get("claveUsrIfRep2") != null && !contratoCesion.get("claveUsrIfRep2").toString().equals("")) {
					pdfDoc2.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc2.addText("Sello Digital de " + contratoCesion.get("nombreUsrIfRep2") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);				                  
					pdfDoc2.setTable(1, 40, anchoCelda4);
					pdfDoc2.setCell((String)contratoCesion.get("firmaIfRep2"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc2.addTable();
					banderaArchivo2=true;
				}
			
				pdfDoc2.endDocument();
				
				ArrayList listaA = new ArrayList();
				
				if(banderaArchivo1){listaA.add(strDirectorioTemp+nombreArchivo);}
				listaA.add(strDirectorioTemp+nombreArchivo2);
				if(banderaArchivo2){listaA.add(strDirectorioTemp+nombreArchivo3);}
				String nombreArchivoNevo = archivo.nombreArchivo()+".pdf";
				listaA.add(strDirectorioTemp+nombreArchivoNevo);
				String args[]= new String[listaA.size()];
				for (int i=0; i<listaA.size(); i++)
					{
						args[i]=(String)listaA.get(i);
					}
				//String args[] =listaA.toArray();//  {strDirectorioTemp+nombreArchivo,strDirectorioTemp+nombreArchivo2,strDirectorioTemp+nombreArchivo3,strDirectorioTemp+nombreArchivoNevo};
				
				if(banderaArchivo1||banderaArchivo2){
					nombreArchivo=nombreArchivoNevo;
					ComunesPDF.doMerge(args);
					
				}else{
					nombreArchivo=nombreArchivo2;
				}
				
				
				
			}
			
			
			
		}catch(Exception e){
				throw new AppException("Error al Descarga Contrato Cesion Pyme: ", e);
			}finally{		
			System.out.println("DescargaContratoCesionIF Acuse (S)");
			}		
		return nombreArchivo; 

}

	/** Descarga COntrato de Cesion IF 
	 * para la pantalla de Notificacion 
	 * para el perfil Admin IF 
	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param parametros
	 */

	public String  archNotifiConCesionIF ( HashMap parametros)  throws AppException {	
		String nombreArchivo ="";  
		CreaArchivo archivo = new CreaArchivo();
		System.out.println("DescargaContratoCesionIF Acuse (E)");
		
	
		try {
		
			String tipo_archivo = parametros.get("tipo_archivo")==null?"":parametros.get("tipo_archivo").toString();
			String clave_solicitud = parametros.get("clave_solicitud")==null?"":parametros.get("clave_solicitud").toString();
			String recibo_carga = parametros.get("recibo_carga")==null?"":parametros.get("recibo_carga").toString();
			String fecha_carga = parametros.get("fecha_carga")==null?"":parametros.get("fecha_carga").toString();
			String tipoOperacion = parametros.get("tipoOperacion")==null?"":parametros.get("tipoOperacion").toString();
			String nombre_usuario = parametros.get("nombre_usuario")==null?"":parametros.get("nombre_usuario").toString();
			String strDirectorioTemp = parametros.get("strDirectorioTemp")==null?"":parametros.get("strDirectorioTemp").toString();
			String pais = parametros.get("pais")==null?"":parametros.get("pais").toString();
			String noCliente = parametros.get("noCliente")==null?"":parametros.get("noCliente").toString();
			String nombre = parametros.get("nombre")==null?"":parametros.get("nombre").toString();
			String nombreUsr = parametros.get("nombreUsr")==null?"":parametros.get("nombreUsr").toString();
			String logo = parametros.get("logo")==null?"":parametros.get("logo").toString();
			String strDirectorioPublicacion = parametros.get("strDirectorioPublicacion")==null?"":parametros.get("strDirectorioPublicacion").toString();
			String clave_epo ="", clave_if ="";
			
		
			HashMap contratoCesion = this.consultaContratoCesionPyme(clave_solicitud);
			int indice_camp_adic = 0;
			String clasificacionEpo ="", claveEstatus ="";
			HashMap campos_adic =  new HashMap();
			StringBuffer clausulado_parametrizado =  new  StringBuffer();
			
			nombreArchivo = archivo.nombreArchivo()+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
				
			if(contratoCesion.size()>1){
				clave_epo = (String)contratoCesion.get("clave_epo");
				clave_if = (String)contratoCesion.get("clave_if");
				claveEstatus = (String)contratoCesion.get("clave_estatus");
				
				campos_adic = this.getCamposAdicionalesParametrizados(clave_epo, "9");
				clausulado_parametrizado = this.consultaClausuladoParametrizado(clave_if);
				clasificacionEpo = this.clasificacionEpo(clave_epo);
				
				indice_camp_adic = Integer.parseInt((String)campos_adic.get("indice"));			
				
				pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
				
				float anchoCelda4[] = {100f};	
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
					pdfDoc.setTable(1, 70, anchoCelda4);
					//pdfDoc.setCell("_________________________________________________________________________________________________________","formas",ComunesPDF.JUSTIFIED, 1, 1, 0); 
					pdfDoc.setCell("NOTIFICACION " , "formas", ComunesPDF.CENTER, 1, 1, 0);		
					pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "formas", ComunesPDF.LEFT, 1, 1, 0);	 //nombre de la EPo					
					pdfDoc.setCell("Con base en lo dispuesto  en el C�digo de Comercio, por este conducto me permito notificar a la Entidad, por conducto de esa Unidad Administrativa adscrita a la Direcci�n Jur�dica de Petr�leos Mexicanos, el Contrato de Cesi�n Electr�nica de Derechos de Cobro, que mediante el uso de firmas electr�nicas hemos formalizado con la Empresa Cedente, respecto del Contrato referido en el apartado siguiente", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);    
					pdfDoc.setCell("Por lo anterior, me permito solicitar a usted gire sus instrucciones a fin de que una vez que surta efectos la presente Cesi�n Electr�nica de Derechos de Cobro, se registre la misma a efecto de que "+contratoCesion.get("nombre_epo")+", realice el pago de las facturas derivadas del cumplimiento y ejecuci�n del Contrato referido en el apartado inmediato anterior a la cuenta del suscrito INTERMEDIARIO FINANCIERO.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.setCell("Los datos de la cuenta en la que se deben depositar los pagos derivados de esta operaci�n se detallan en el apartado siguiente como Nombre del INTERMEDIARIO FINANCIERO, Banco de Dep�sito,  N�mero de Cuenta �PEMEX� para Dep�sito y N�mero de Cuenta CLABE.", "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);   
					pdfDoc.addTable();
				}
			
				//====================================================================>> REPRESENTANTE LEGAL IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				
				//====================================================================>> TESTIGO 1 IF
				if (contratoCesion.get("claveUsrTest1") != null && !contratoCesion.get("claveUsrTest1").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest1") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaTest1"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> TESTIGO 2 IF
				if (contratoCesion.get("claveUsrTest2") != null && !contratoCesion.get("claveUsrTest2").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrTest2") + " testigo de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaTest2"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}	
				
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("CONTRATO DE CESI�N DE DERECHOS", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				float anchoCelda2[] = {50f, 50f};
				pdfDoc.setTable(2, 80, anchoCelda2);

				pdfDoc.setCell("INFORMACI�N DE LA PYME CEDENTE", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("RFC:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("rfcPyme"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("REPRESENTANTE PYME:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("REPRESENTANTE PYME:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NUM. PROVEEDOR:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroProveedor"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACI�N DEL CONTRATO", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("N�MERO DE CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numero_contrato"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("MONTO / MONEDA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(contratoCesion.get("montosPorMoneda").toString().replaceAll(",", "").replaceAll("<br/>", "\n"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE VIGENCIA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N/A".equals(contratoCesion.get("fecha_inicio_contrato"))?"N/A":(contratoCesion.get("fecha_inicio_contrato") + " a " + contratoCesion.get("fecha_fin_contrato")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PLAZO DEL CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("plazoContrato"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("TIPO DE CONTRATACI�N:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("tipo_contratacion").toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("OBJETO DEL CONTRATO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("objeto_contrato").toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				for (int i = 0; i < indice_camp_adic; i++) {
					pdfDoc.setCell(campos_adic.get("nombre_campo_"+i).toString().toUpperCase() + ":", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell(contratoCesion.get("campo_adicional_"+(i+1)).toString().toUpperCase(), "formas", ComunesPDF.LEFT);
				}
				
				pdfDoc.setCell("INFORMACI�N DE LA EMPRESA DE PRIMER ORDEN", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_epo"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACI�N DEL CESIONARIO (IF)", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("NOMBRE:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("nombre_if"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("BANCO DE DEP�SITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("bancoDeposito"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N�MERO DE CUENTA PARA DEP�SITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroCuenta"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("N�MERO DE CUENTA CLABE PARA DEP�SITO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("numeroCuentaClabe"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("INFORMACI�N DE LA CESI�N DE DERECHOS", "celda02", ComunesPDF.CENTER, 2);
				pdfDoc.setCell("FECHA DE SOLICITUD DE CONSENTIMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("fechaSolicitudConsentimiento"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE CONSENTIMIENTO DE LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)contratoCesion.get("fechaAceptacionEpo"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE OTORG� EL CONSENTIMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrEpoAutCesion")==null?"":contratoCesion.get("nombreUsrEpoAutCesion")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE FORMALIZACI�N DEL CONTRATO DE CESI�N:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaFormalContrato")==null?"":contratoCesion.get("fechaFormalContrato")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE 1 DE CEDENTE (PYME):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep1")==null?"":contratoCesion.get("nombreUsrAutRep1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE 2 DE CEDENTE (PYME):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrAutRep2")==null?"":contratoCesion.get("nombreUsrAutRep2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE DE CESIONARIO (IF):", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrIf")==null?"":(!claveEstatus.equals("10")?contratoCesion.get("nombreUsrIf"):"")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE TESTIGO 1 DEL CESIONARIO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrTest1")==null?"":contratoCesion.get("nombreUsrTest1")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("NOMBRE DEL FIRMANTE TESTIGO 2 DEL CESIONARIO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrTest2")==null?"":contratoCesion.get("nombreUsrTest2")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE NOTIFICACI�N ACEPTADA POR LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifEpo")==null?"":contratoCesion.get("fechaNotifEpo")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE ACEPT� LA NOTIFICACI�N:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrNotifEpo")==null?"":contratoCesion.get("nombreUsrNotifEpo")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE INFORMACI�N A VENTANILLA DE PAGOS:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE RECEPCI�N EN VENTANILLA:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaNotifVentanilla")==null?"":contratoCesion.get("fechaNotifVentanilla")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("FECHA DE LA APLICACI�N DEL REDIRECCIONAMIENTO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("fechaRedirecccion")==null?"":contratoCesion.get("fechaRedirecccion")), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("PERSONA QUE REDIRECCIONA LA CUENTA DE LA EPO:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)(contratoCesion.get("nombreUsrRedirec")==null?"":contratoCesion.get("nombreUsrRedirec")), "formas", ComunesPDF.LEFT);

				pdfDoc.addTable();

				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("SE SUJETA A LAS SIGUIENTES CLAUSULAS", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				float anchoCelda3[] = {100f};
				pdfDoc.setTable(1, 70, anchoCelda3);
				
				pdfDoc.setCell(clausulado_parametrizado.toString(), "formas", ComunesPDF.LEFT, 1);

				pdfDoc.addTable();
				
				
				//====================================================================>> REPRESENTANTE LEGAL 1 PYME
				if (contratoCesion.get("claveUsrAutRep1") != null && !contratoCesion.get("claveUsrAutRep1").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep1") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
	
					pdfDoc.setCell((String)contratoCesion.get("firmaRep1"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> REPRESENTANTE LEGAL 2 PYME
				if (contratoCesion.get("claveUsrAutRep2") != null && !contratoCesion.get("claveUsrAutRep2").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrAutRep2") + " representante legal de " + contratoCesion.get("nombre_pyme"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaRep2"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}
				//====================================================================>> REPRESENTANTE LEGAL IF
				if (contratoCesion.get("claveUsrIf") != null && !contratoCesion.get("claveUsrIf").toString().equals("")) {
					pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
					pdfDoc.addText("Sello Digital de " + contratoCesion.get("nombreUsrIf") + " representante legal de " + contratoCesion.get("nombre_if"), "formas", ComunesPDF.CENTER);
	
					pdfDoc.setTable(1, 40, anchoCelda4);
					
					pdfDoc.setCell((String)contratoCesion.get("firmaIf"), "formas", ComunesPDF.CENTER, 1);
	
					pdfDoc.addTable();
				}						
			}
		pdfDoc.endDocument();	
			
		}catch(Exception e){
				throw new AppException("Error al Descarga Contrato Cesion Pyme: ", e);
			}finally{		
			System.out.println("DescargaContratoCesionIF Acuse (S)");
			}		
		return nombreArchivo; 

}



	/**
	 *metodo  para imprimir el acuse del contrato de cesion  Testigo 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param parametros
	 */

	public String  archAcuseContratoTestigoIF ( HashMap parametros)  throws AppException {	
		String nombreArchivo ="";  
		CreaArchivo archivo = new CreaArchivo();
		System.out.println("archAcuseContratoTestigo Acuse (E)");
		String claveEpo ="", acuse ="";
	
		try {
		
			String claveSolicitud = parametros.get("clave_solicitud")==null?"":parametros.get("clave_solicitud").toString();
			String acuseCarga = parametros.get("recibo_carga")==null?"":parametros.get("recibo_carga").toString();
			String fechaCarga = parametros.get("fecha_carga")==null?"":parametros.get("fecha_carga").toString();
			String horaCarga = parametros.get("horaCarga")==null?"":parametros.get("horaCarga").toString();
			String loginUsuario = parametros.get("loginUsuario")==null?"":parametros.get("loginUsuario").toString();
			String claveIf = parametros.get("claveIf")==null?"":parametros.get("claveIf").toString();
			String nombreUsuario = parametros.get("nombre_usuario")==null?"":parametros.get("nombre_usuario").toString();
			
			String strDirectorioTemp = parametros.get("strDirectorioTemp")==null?"":parametros.get("strDirectorioTemp").toString();
			String pais = parametros.get("pais")==null?"":parametros.get("pais").toString();
			String noCliente = parametros.get("noCliente")==null?"":parametros.get("noCliente").toString();
			String nombre = parametros.get("nombre")==null?"":parametros.get("nombre").toString();
			String nombreUsr = parametros.get("nombreUsr")==null?"":parametros.get("nombreUsr").toString();
			String logo = parametros.get("logo")==null?"":parametros.get("logo").toString();
			String strDirectorioPublicacion = parametros.get("strDirectorioPublicacion")==null?"":parametros.get("strDirectorioPublicacion").toString();
		
			
			HashMap contratoCesion = this.consultaContratoCesionTestigo(claveSolicitud);
			claveEpo = contratoCesion.get("clave_epo")==null?"":(String)contratoCesion.get("clave_epo");
			int num_registros = Integer.parseInt((String)contratoCesion.get("indice"));
				
			HashMap campos_adic = this.getCamposAdicionalesParametrizados(claveEpo, "9");
			int indice_camp_adic = Integer.parseInt((String)campos_adic.get("indice"));
			
			StringBuffer clausulado_parametrizado = this.consultaClausuladoParametrizado(claveIf);
			
			String clasificacionEpo = this.clasificacionEpo(claveEpo);
			
			nombreArchivo = archivo.nombreArchivo()+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
									
			acuse = contratoCesion.get("acusetestigo1")==null?"":contratoCesion.get("acusetestigo1").toString();
			
			pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
			pdfDoc.addText("La autentificaci�n se llev� a cabo con �xito \n Recibo: "+acuseCarga, "titulo", ComunesPDF.CENTER);

			float anchoCelda1[] = {50f, 50f};
			pdfDoc.setTable(2, 50, anchoCelda1);
			
			pdfDoc.setCell("Cifras de Control", "celda02", ComunesPDF.CENTER, 2);
			pdfDoc.setCell("N�mero de Acuse", "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell((String)contratoCesion.get("acuseFirmaTest1"), "formas",ComunesPDF.LEFT);
			
			pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell((String)contratoCesion.get("fechaTest1"), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell((String)contratoCesion.get("horaTest1"), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Usuario", "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell(contratoCesion.get("claveUsrTest2")==null?(contratoCesion.get("claveUsrTest1") + " - " + contratoCesion.get("nombreUsrTest1")):(contratoCesion.get("claveUsrTest2") + " - " + contratoCesion.get("nombreUsrTest2")), "formas",ComunesPDF.LEFT);
			pdfDoc.addTable();

			pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
			pdfDoc.endDocument();
			String nombreArchivo3=archivo.nombreArchivo()+".pdf";
			String args[] = {strDirectorioTemp+nombreArchivo,strDirectorioTemp+(parametros.get("nombreArchivo")==null?"":parametros.get("nombreArchivo").toString()),strDirectorioTemp+nombreArchivo3 };
			ComunesPDF.doMerge(args);
			nombreArchivo=nombreArchivo3;
		}catch(Exception e){
				throw new AppException("Error al Descarga Contrato Cesion Pyme: ", e);
			}finally{		
			System.out.println("archAcuseContratoTestigo Acuse (S)");
			}		
		return nombreArchivo; 

}

	/**
	 *metodo para verificar si existe el numero de contrato a consultar
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param parametros
	 */
	public boolean existeNumeroContrato(String clavePyme,	String noContrato, String claveEpo){
		log.info("CesionEJBBean::existeNumeroContrato(E)");

	   try {
	      if (	(clavePyme == null || clavePyme.equals(""))	&& (noContrato == null || noContrato.equals(""))	&& (claveEpo == null || claveEpo.equals("")) ){
	         throw new Exception("Los parametros no pueden ser nulos");
	      }
	   } catch(Exception e) {
	      log.info("CesionEJBBean::existeNumeroContrato(Error) "+"Error en los parametros recibidos. " + e.getMessage() +
						"\n clavePyme=" + clavePyme + "\n claveEpo "+ claveEpo + "\n noContrato "+ noContrato + "\n" );
	      throw new AppException("Error Inesperado", e);
	   }

		AccesoDB	con					= new AccesoDB();
		StringBuffer qrySentencia	= new StringBuffer();
		List		lVarBind				= new ArrayList();
		boolean	bOK					= false;
		try {
			con.conexionDB();
			qrySentencia.append(
				" SELECT cg_no_contrato"   +
				"   FROM cder_solicitud"   +
				"  WHERE cg_no_contrato = ?"	+
				"		AND ic_pyme = ? "	+
				"		AND ic_epo = ? "
			);
			lVarBind.add(noContrato);
			lVarBind.add(clavePyme);
			lVarBind.add(claveEpo);

			Registros reg = con.consultarDB(qrySentencia.toString(), lVarBind);

			while(reg!=null && reg.next()){
				bOK = true;
			}
		} catch(Exception e){
			log.error("CesionEJBBean::existeNumeroContrato(Exception)"+e);
			throw new AppException("Error al obtener el numero de contrato");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CesionEJBBean::existeNumeroContrato(S)");
		}
		return bOK;
	}

	/**
	 *metodo para verificar si existe el numero de contrato a consultar
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param parametros
	 */
	public boolean existeNumeroContratoCedido(String noContrato, String claveEpo){
		log.info("CesionEJBBean::existeNumeroContratoCedido(E)");

	   try {
	      if (	(noContrato == null || noContrato.equals(""))	&& (claveEpo == null || claveEpo.equals("")) ){
	         throw new Exception("Los parametros no pueden ser nulos");
	      }
	   } catch(Exception e) {
	      log.info("CesionEJBBean::existeNumeroContratoCedido(Error) "+"Error en los parametros recibidos. " + e.getMessage() +
						"\n claveEpo "+ claveEpo + "\n noContrato "+ noContrato + "\n" );
	      throw new AppException("Error Inesperado", e);
	   }

		AccesoDB	con					= new AccesoDB();
		StringBuffer qrySentencia	= new StringBuffer();
		List		lVarBind				= new ArrayList();
		boolean	bOK					= false;
		try {
			con.conexionDB();
			qrySentencia.append(
				" SELECT cg_no_contrato "	+
				"   FROM cder_contrato_cedido "	+
				"  WHERE cg_no_contrato = ? "	+
				"		AND ic_epo = ? "
			);
			lVarBind.add(noContrato);
			lVarBind.add(claveEpo);

			Registros reg = con.consultarDB(qrySentencia.toString(), lVarBind);

			while(reg!=null && reg.next()){
				bOK = true;
			}
		} catch(Exception e){
			log.error("CesionEJBBean::existeNumeroContratoCedido(Exception)"+e);
			throw new AppException("Error al obtener el numero de contrato");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CesionEJBBean::existeNumeroContratoCedido(S)");
		}
		return bOK;
	}

	/**
	 * Metodo que Captura los contratos cedidos manualmente
	 * @throws com.netro.exception.AppException
	 * @return 
	 * @param noContrato
	 * @param ic_epo
	 * @param ic_if
	 * @param ic_pyme
	 */
	public void capturaContratosCedidos(String claveEpo, String claveIF, String clavePyme, String contrato){
		log.info("CesionEJBBean:capturaContratosCedidos (E)");

	   try {
	      if (	(clavePyme == null || clavePyme.equals(""))	&& (contrato == null || contrato.equals(""))	
				&& (claveEpo == null || claveEpo.equals(""))	&& (claveIF == null || claveIF.equals("")) ){
	         throw new Exception("Los parametros no pueden ser nulos");
	      }
	   } catch(Exception e) {
	      log.info("CesionEJBBean::capturaContratosCedidos(Error) "+"Error en los parametros recibidos. " + e.getMessage() +
						"\n clavePyme=" + clavePyme + "\n claveEpo "+ claveEpo + "\n contrato "+ contrato + "\n claveIF "+ claveIF + "\n" );
	      throw new AppException("Error Inesperado", e);
	   }

		AccesoDB con			= new AccesoDB();
		PreparedStatement ps	= null;
		boolean commit			= true;
		StringBuffer strSQL	= new StringBuffer();
		List lvarbind			= new ArrayList();
		String noContrato1	= contrato.toUpperCase();
		try {

			con.conexionDB();

			strSQL.append(" INSERT INTO cder_contrato_cedido (cg_no_contrato, ic_epo, ic_if, ic_pyme) VALUES(?,?,?,?)");
			lvarbind.add(noContrato1);
			lvarbind.add(claveEpo);
			lvarbind.add(claveIF);
			lvarbind.add(clavePyme);

			ps = con.queryPrecompilado(strSQL.toString(),lvarbind);
			ps.executeUpdate();
			ps.close();

			log.debug("Query::::  "+strSQL.toString() );
		
		} 	catch(Exception e) {
			commit = false;
			log.info("CesionEJBBean:capturaContratosCedidos(Exception) " + e);
			e.printStackTrace();
			throw new AppException("CesionEJBBean:capturaContratosCedidos(Exception) ", e);

		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
			con.cierraConexionDB();
		}

		log.info("CesionEJBBean:capturaContratosCedidos (S)");

	}

	/**
	 * Metodo que eliminar los contratos cedidos manualmente
	 * @throws com.netro.exception.AppException
	 * @return 
	 * @param noContrato
	 * @param ic_epo
	 * @param ic_if
	 * @param ic_pyme
	 */
	public void eliminaContratoCedido(String claveEpo, String contrato){
		log.info("CesionEJBBean:eliminaContratoCedido (E)");

	   try {
	      if (	(contrato == null || contrato.equals(""))	&& (claveEpo == null || claveEpo.equals(""))	){
	         throw new Exception("Los parametros no pueden ser nulos");
	      }
	   } catch(Exception e) {
	      log.info("CesionEJBBean::eliminaContratoCedido(Error) "+"Error en los parametros recibidos. " + e.getMessage() +
						"\n claveEpo "+ claveEpo + "\n contrato "+ contrato + "\n" );
	      throw new AppException("Error Inesperado", e);
	   }

		AccesoDB con			= new AccesoDB();
		PreparedStatement ps	= null;
		boolean commit			= true;
		StringBuffer strSQL	= new StringBuffer();
		List lvarbind			= new ArrayList();
		String noContrato1	= contrato.toUpperCase();
		try {

			con.conexionDB();

			strSQL.append(" DELETE FROM cder_contrato_cedido WHERE cg_no_contrato = ? AND ic_epo = ? ");
			lvarbind.add(noContrato1);
			lvarbind.add(claveEpo);

			ps = con.queryPrecompilado(strSQL.toString(),lvarbind);
			ps.executeUpdate();
			ps.close();

			log.debug("Query::::  "+strSQL.toString() );
		
		} 	catch(Exception e) {
			commit = false;
			log.info("CesionEJBBean:eliminaContratoCedido(Exception) " + e);
			e.printStackTrace();
			throw new AppException("CesionEJBBean:eliminaContratoCedido(Exception) ", e);

		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
			con.cierraConexionDB();
		}

		log.info("eliminaContratoCedido (S)");

	}
	
	public String obtenerConsentimiento(String clave_solicitud,String  nombreUsuario,String strDirectorioTemp){
				
				
		List registro = this.inicializarPreAcuseActivaReactiva(clave_solicitud);	
		String nombrePymeCedente=null,cesionario=null,nompyme=null,Proveedor=null,FSolicitud=null,contrato=null
			,objeto=null,plazoContrato2=null,fechaSolicitud=null,nombreEpo=null;
			StringBuffer montosPorMoneda=null;
			String empresasRep="";
		for(int i=0;i<registro.size();i++){
			List laux = (List)registro.get(i);
			
			cesionario     =((laux.get(0))==null)?"":(laux.get(0).toString());
			nompyme     =((laux.get(1))==null)?"":(laux.get(1).toString());
			nombrePymeCedente = ((laux.get(1))==null)?"":(laux.get(1).toString());
			Proveedor =  ((laux.get(3))==null)?"":(laux.get(3).toString());
			contrato       = ((laux.get(5))==null)?"":(laux.get(5).toString());
			objeto         = ((laux.get(17))==null)?"":(laux.get(17).toString());
			plazoContrato2 = ((laux.get(25))==null)?"":(laux.get(25).toString());
			fechaSolicitud = ((laux.get(29))==null)?"":(laux.get(29).toString());
			nombreEpo = ((laux.get(30))==null)?"":(laux.get(30).toString());
			empresasRep= ((laux.get(32))==null)?"":(laux.get(32).toString());
			 montosPorMoneda = this.getMontoMoneda(clave_solicitud); 
		}
				CreaArchivo archivo = new CreaArchivo();
				String nombreArchivo = archivo.nombreArchivo()+".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo,"", false, false, false);
				
				SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:SS a");	//HH es formato 24 horas.
				String hora = sdf.format(new java.util.Date());
				String dia	= new SimpleDateFormat("dd/mm/yyyy").format(new java.util.Date());
				//String UsuarioNom = usuario +" "+nombreUsuario;
				
				pdfDoc.setTable(1,70);
				pdfDoc.setCell(nombrePymeCedente+"\n"+empresasRep.replaceAll(";","\n"),"contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Presente","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Consentimiento para Ceder los Derechos de Cobro respecto del Contrato No. "+ contrato,"contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(nombrePymeCedente+", "+empresasRep.replaceAll(";",", "),"contrato",ComunesPDF.JUSTIFIED,1,1,0);

				pdfDoc.setCell("Objeto: "+objeto,"contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Monto: "+montosPorMoneda.toString().replaceAll("<br/>", " "),"contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Plazo: "+plazoContrato2,"contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("Fecha de Solicitud: "+fechaSolicitud,"contrato",ComunesPDF.JUSTIFIED,1,1,0);
                				
				pdfDoc.setCell(
//                    " Es importante se�alar que el Transitorio Octavo, Apartado A, fracci�n VII de la Ley de Petr�leos Mexicanos publicada en el Diario Oficial de la Federaci�n el 11 de agosto de 2014, establece que las nuevas Empresas Productivas Subsidiarias se subrogan en todos los derechos y obligaciones de los Organismos Subsidiarios, seg�n corresponda, anteriores y posteriores a la fecha de entrada en vigor de los Acuerdos de Creaci�n que al efecto se expidan, por lo que una vez que entraron en vigor dichos Acuerdos, las Empresas Productivas Subsidiarias se subrogaron en los derechos y obligaciones del Organismos Subsidiario que formaliz� el contrato, incluyendo los convenios modificatorios que de �l deriven. \n \n"+
                    " En atenci�n a su solicitud, se le informa que con base en la informaci�n recabada al interior de esta Empresa Productiva Subsidiaria "+nombreEpo+", se otorga el consentimiento para ceder los derechos de cobro respecto del Contrato de antecedentes,con fundamento en: \n \n"+
                    " El Art�culo 46 de la Ley de Adquisiciones, Arrendamientos y Servicios del Sector P�blico.  "+
                    " El Art�culo 47 de la Ley de Obras P�blicas y Servicios Relacionados con las Mismas.  "+
                    " Los Art�culos 53, fracci�n XVII, de la Ley de Petr�leos Mexicanos, 48 de su Reglamento, y 54 de las Disposiciones administrativas de contrataci�n en materia de adquisiciones, arrendamientos, obras y servicios de las actividades sustantivas de car�cter productivo de Petr�leos Mexicanos y  Organismos Subsidiarios. "+
                    " El Art�culo 36 de las Disposiciones Generales de Contrataci�n para Petr�leos Mexicanos y sus Empresas Producctivas Subsidiarias, seg�n corresponda, as� como en la Cl�usulas relativas a la cesi�n de derechos Cobro del Contrato.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);

				pdfDoc.setCell(
                    " Es importante se�alar que el Transitorio Octavo, Apartado A, fracci�n VII de la Ley de Petr�leos Mexicanos publicada en el Diario Oficial de la Federaci�n el 11 de agosto de 2014, establece que las nuevas Empresas Productivas Subsidiarias se subrogan en todos los derechos y obligaciones de los Organismos Subsidiarios, seg�n corresponda, anteriores y posteriores a la fecha de entrada en vigor de los Acuerdos de Creaci�n que al efecto se expidan, por lo que una vez que entraron en vigor dichos Acuerdos, las Empresas Productivas Subsidiarias se subrogaron en los derechos y obligaciones del Organismos Subsidiario que formaliz� el contrato, incluyendo los convenios modificatorios que de �l deriven. \n \n","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				
				pdfDoc.setCell("\n No obstante lo anterior, el Consentimiento estar� Condicionado a que: ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				
				pdfDoc.setCell("1.- "+nombrePymeCedente+(!empresasRep.equals("")?"; "+empresasRep:"")+", en lo sucesivo la CEDENTE, deber� celebrar con el INTERMEDIARIO FINANCIERO "+ cesionario   +" en su car�cter de Cesionario un  Contrato de Cesi�n Electr�nica de Derechos de Cobro, mismo que deber� ser firmado electr�nicamente por el o los representantes legales o apoderados de la CEDENTE que cuente(n) con facultades generales o especiales para actos de dominio y que dicho instrumento "+
				         " contractual sea notificado a la Empresa a trav�s del sistema NAFIN para que surta efectos  legales,"+
				         " en t�rminos de lo establecido en el art�culo 390 del C�digo de Comercio, en el entendido que de no efectuarse la mencionada notificaci�n dentro de los 30 (treinta) d�as h�biles siguientes a la fecha en que sea notificada en el sistema NAFIN el presente Consentimiento,"+
							" en el entendido que de no efectuarse la mencionada notificaci�n dentro del plazo se�alado, el consentimiento de que se trata quedar� sin efectos sin necesidad de que la Empresa le comunique dicha situaci�n, quedando bajo su responsabilidad que dicha notificaci�n se realice en los t�rminos indicados. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				
				pdfDoc.setCell(" 2.- La Cesi�n de Derechos de Cobro ser� por el 100% (cien por ciento) de los derechos de cobro del Contrato se�alado y la cesi�n, surtir� sus efectos apartir de la aceptaci�n de la notificaci�n del correspondiente Contrato de Cesi�n Electr�nica de Derechos de Cobro de la Empresa por medio del Sistema NAFIN. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				
				pdfDoc.setCell(" 3.- La Cesi�n de Derechos de Cobro incluir� los incrementos al monto del contrato arriba se�alado que, en su caso, se pacten con posterioridad mediante convenios modificatorios; y que la Cesi�n de Derechos de Cobro �nica y exclusivamente proceder� con respecto de las facturas pendientes de cobro por los trabajos/servicios ejecutados, aceptados e instruidos por la Empresa, "+
							" en t�rminos de los pactado en el Contrato. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				
				pdfDoc.setCell(" 4.- Que el Cedente, bajo protesta de decir verdad, garantiza que los derechos de cobro objeto de cesi�n, no han sido dados en garant�a, pignorados, embargados, ni gravados en forma alguna y que no existe controversia sobre los mismos. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" 5.- Que el Cedente y el Cesionario reconocen que el Cedente es el �nico responsable de garantizar que los derechos de cobro est�n libres de gravamen o disputa.  ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" 6.- En caso de embargo es obligaci�n exclusiva del Cedente, notificar de forma inmediata al Cesionario de cualquier acci�n de cobro, embargo o cualquier gravamen.  ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" 7.- El Cedente queda obligado a sacar en paz y a salvo a la Empresa de cualquier controversia relacionada con los grav�menes o las disputas que se originen por los derechos de cobro objeto de cesi�n. Lo anterior, sin menoscabo de las acciones que correspondan al Cesionario en contra del Cedente. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" 8.- Para el caso de que la Empresa, con posterioridad a la cesi�n de derechos de cobro, llegare a efectuar cualquier pago al Cedente en lugar del Cesionario respecto de dichos derechos de cobro, el Cedente quedar� constituido en depositario de dicho pago, para todos los efectos a que haya lugar y se obliga a restituirlo, en forma inmediata al Cesionario, sin necesidad de que medie aviso o requerimiento para ello. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" 9.- Una vez cedidos los derechos de cobro, en caso de que la Empresa sea requerida por autoridad judicial o administrativa para retener o exhibir cualquier cantidad relacionada con los derechos de cobro objeto de la presente cesi�n, la Empresa, informar� a la autoridad emisora que los derechos de cobro derivados del Contrato se encuentran cedidos a favor del Cesionario; y en caso de que dicha autoridad confirme "+
				         " a la Empresa que �sta retenga y/o exhiba cualquier cantidad relacionada con los derechos de cobro objeto de la presente cesi�n, la Empresa comunicar� dicha situaci�n al Cedente y al Cesionario, para que hagan valer lo que a su derecho convenga ante dicha autoridad. \n"+
							" El Cedente y Cesionario renuncian en este acto y en los t�rminos m�s amplios que en derecho proceda, al ejercicio de cualquier acci�n que les asista o que les pudiera corresponder en contra de la Empresa, derivado del cumplimiento que �ste realice a cualquier requerimiento formulado o instado por las autoridades. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" 10.- El Cesionario acepta y est� de acuerdo en que el pago de los derechos de cobro estar� sujeto a que, en su caso, la Empresa realice las deducciones y/o retenciones que deban hacerse al cedente por adeudos con la misma, penas convencionales, pagos en exceso; cualquier requerimiento de pago derivado de laudo firme ordenado por las Juntas Locales o Federales de Conciliaci�n y Arbitraje, "+
							 " con motivo de los juicios laborales instaurados en contra de la Empresa en relaci�n con el Cedente; o cualquier otra deducci�n derivada del Contrato o de la ley de la materia en relaci�n �nicamente con el mismo, en cuyos supuestos se har�n las deducciones y/o retenciones correspondientes. En tal virtud, la Empresa quedar� liberada de cualquier responsabilidad por las deducciones y/o retenciones a que haya lugar. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" 11.- El Cedente y Cesionario reconocen y est�n de acuerdo en que el consentimiento otorgado por la Empresa para ceder los derechos de cobro del Contrato que nos ocupa, incluidos los incrementos al monto original del mismo no constituye por su parte una garant�a de pago ni reconocimiento de cualquier derecho del Cesionario frente al Cedente, siendo la �nica obligaci�n de la Empresa, la de pagar cantidades"+
							" l�quidas que sean exigibles conforme al Contrato, una vez hechas las deducciones y/o retenciones antes mencionadas. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" 12.- Se deber� considerar la clave del Registro Federal de Contribuyentes (RFC) del Cesionario y los datos de la Cuenta �nica previamente registrada la Empresa en que se realizar�n los pagos correspondientes.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" 13.- Que para dejar sin efectos la cesi�n de los derechos de cobro previo a la extinci�n natural de dicha cesi�n, ser� necesario que el Cesionario as� lo comunique a la Empresa en los mismos t�rminos previstos para la notificaci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro para que surta los efectos legales que le correspondan. "+
				         " A la notificaci�n que se practique a la Empresa mediante el Sistema NAFIN, NAFIN deber� anexar la siguiente documentaci�n: \n "+
				         " \n i) un ejemplar del Contrato de Cesi�n Electr�nica de Derechos de Cobro con las firmas electr�nicas de los representantes de la Cedente y del Cesionario;  \n"+
				         " \n ii) copia certificada por fedatario p�blico de la documentaci�n con la que se acredite la personalidad del o de los representantes legales o apoderados del Cedente con las facultades generales o especiales para actos de dominio, y  \n"+
							" \n  iii) copia certificada por fedatario p�blico de la documentaci�n con la que se acredite la personalidad del o de los representantes legales o apoderados del Cesionario con las facultades suficientes para la celebraci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro en cuesti�n. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" En el caso de que no se anexe a la notificaci�n la documentaci�n se�alada en los numerales anteriores, la misma no se considerar� recibida por la Empresa y por lo tanto no surtir� efectos la cesi�n de derechos de cobro, considerando adem�s de que el t�rmino de 30 (treinta) d�as h�biles para realizar la notificaci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro no se interrumpir�. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(" El presente consentimiento, quedar� sin efecto en caso de que la Empresa antes de la fecha de notificaci�n a que se refiere el punto n�mero '1' del presente documento, reciba alguna notificaci�n de autoridad judicial o administrativa que incida sobre el Contrato cuyos derechos de cobro ser�n materia de cesi�n. Bajo este supuesto, la Empresa lo informar� inmediatamente  "+
							" a trav�s del Sistema  NAFIN al Cedente o al Cesionario, una vez que haya recibido formalmente la notificaci�n de que se trate.  ","contrato",ComunesPDF.JUSTIFIED,1,1,0);

				pdfDoc.setCell(" ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell(nombreUsuario,"contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.addTable();
				pdfDoc.endDocument();
			return nombreArchivo;
	}
	
	/**
	 * Obtiene de la base de datos el archivo del Consentimiento
	 * y lo prepara para su descarga desde la pantallas.
	 * @param clave_solicitud Es la clave de la solicitud de la que se obtiene el archivo del contrato de cesi�n.
	 * @param strDirectorioTemp Es el directorio donde se genera de manera temporal el archivo.
	 * @return file El archivo del contrato de cesi�n obtenido de la base de datos, este archivo est� en formato PDF.
	 * @throws netropology.utilerias.AppException Al encontrar un error inesperado
	 */
	public String descargaConsentimiento(String clave_solicitud, String strDirectorioTemp) throws AppException{
		log.info("descargaConsentimiento(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "";
		String nombreArchivo = "";
		
		try{
			con.conexionDB();

			strSQL.append(" SELECT bi_consentimiento");
			strSQL.append(" FROM cder_solicitud");
			strSQL.append(" WHERE ic_solicitud = ?");
			
			varBind.add(clave_solicitud);
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){
				nombreArchivo = Comunes.cadenaAleatoria(8) + ".pdf";
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob("bi_consentimiento");
				InputStream inStream = blob.getBinaryStream();
				int size = (int)blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1){
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("descargaConsentimiento(S)");
		}
		//return pathname;
		return nombreArchivo;
	}
	
	/**
	 * Fodea 033-2013  
	 * @return 
	 * @param Solicitud  
	 */  
	public String getBanderaConsentimiento(String clave_solicitud){  
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      String       bandera  =  "";
		List lVarBind		= new ArrayList();
		log.info("getBanderaConsentimiento (E)");
		 try{
         con.conexionDB();

         String strQry = "SELECT CS_BANDERA_CONSENTIMIENTO  "  +
                        "  FROM cder_solicitud    "  +
                        " WHERE ic_solicitud = ?  ";
                       
			lVarBind		= new ArrayList();
         lVarBind.add(clave_solicitud);        
			ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next()){
				bandera = rs.getString("CS_BANDERA_CONSENTIMIENTO");
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  clave_solicitud (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getBanderaConsentimiento (S)");
      return bandera;
    }
	//Agregado por FODEA 033 2013
	public void guardaCedente(String claveIF, String clavePyme, String tipoRep, String tipoCed, String figJuridica, String escPublica, String fecEscPublica,
									  String nomLic, String numNotario, String oriNotario, String regComercio, String numFolio,String escPublicaRep, String fecEscPublicaRep,
									  String nomLicRep, String numNotarioRep, String oriNotarioRep,String banderaRep,String domicilioLegal){
		
		log.info("guardaCedente :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			strSQL=" INSERT INTO COMCAT_CEDENTE "+
						  "( IC_IF, IC_PYME, CG_TIPO_REPRESENTANTE, CG_TIPO_CEDENTE, CG_FIGURA_JURIDICA, CG_ESCRITURA_PUBLICA, DF_ESCRITURA_PUBLICA,"+
						  " CG_NOMBRE_LICENCIADO, IN_NUM_NOTARIO_PUBLICO, CG_ORIGEN_NOTARIO, CG_REG_PUB_COMERCIO, IN_NUM_FOLIO_MERCANTIL, CG_ESCRITURA_PUBLICA_REP, "+
						  " DF_ESCRITURA_PUBLICA_REP, CG_NOMBRE_LICENCIADO_REP,IN_NUM_NOTARIO_PUBLICO_REP,CG_ORIGEN_NOTARIO_REP,CS_BANDERA_REP,CG_DOMICILIO_LEGAL) "+
						  " VALUES("+ claveIF +","+ clavePyme +",'"+ tipoRep +"','"+ tipoCed +"','"+ figJuridica +"',"+ escPublica +","+ 
						  " TO_DATE('" + fecEscPublica +"','DD/MM/YYYY'),'" + nomLic +"',"+ numNotario +",'"+ oriNotario +"','"+ regComercio +"',"+ numFolio +
						  ","+escPublicaRep+ ", TO_DATE('" + fecEscPublicaRep +"','DD/MM/YYYY'),'" + nomLicRep +"',"+ numNotarioRep +",'"+ oriNotarioRep +"','"+banderaRep+"','"+domicilioLegal+"')";
			
			con.ejecutaSQL(strSQL);
			log.debug("strSQL:::::::: :: "+strSQL);			
				
			}catch(Exception e){
				commit = false;
				e.printStackTrace();
			}finally{
				log.info("guardaCedente :::: (S)");
				if (con.hayConexionAbierta()){
					con.terminaTransaccion(commit);
					con.cierraConexionDB();
				}	
			}						  
	}
	
	public void eliminaCedente(String claveIF, String clavePyme){
		log.info("eliminaCedente :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			strSQL = " delete from comcat_cedente where ic_if = "+claveIF+" and ic_pyme = "+ clavePyme;
			
			con.ejecutaSQL(strSQL);

			log.debug("strSQL:::::::: :: "+strSQL);			
				
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
		}finally{	
			log.info("eliminaCedente :::: (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}	
		}
	}
	
	public void modificarCedente(String claveIF, String clavePyme, String clavePymeAnt, String tipoRep, String tipoCed, String figJuridica, String escPublica,
									  String fecEscPublica, String nomLic, String numNotario, String oriNotario, String regComercio,
									  String numFolio,String escPublicaRep, String fecEscPublicaRep,
									  String nomLicRep, String numNotarioRep, String oriNotarioRep,String banderaRep,String domicilioLegal){
									  
		log.info("modificarCedente :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			strSQL = " update comcat_cedente set ic_pyme= "+ clavePyme+", CG_TIPO_REPRESENTANTE= '"+tipoRep+"', CG_TIPO_CEDENTE= '"+tipoCed+
						"', CG_FIGURA_JURIDICA= '"+figJuridica+"', CG_ESCRITURA_PUBLICA= "+escPublica+", DF_ESCRITURA_PUBLICA = TO_DATE('" + fecEscPublica +"','DD/MM/YYYY')"+
						", CG_NOMBRE_LICENCIADO= '"+nomLic+"', IN_NUM_NOTARIO_PUBLICO= "+numNotario+", CG_ORIGEN_NOTARIO= '"+oriNotario+"', CG_REG_PUB_COMERCIO= '"+regComercio+"'"+
						", IN_NUM_FOLIO_MERCANTIL= "+numFolio+
						", CG_ESCRITURA_PUBLICA_REP= "+escPublicaRep+", DF_ESCRITURA_PUBLICA_REP = TO_DATE('" + fecEscPublicaRep +"','DD/MM/YYYY'), CG_NOMBRE_LICENCIADO_REP= '"+nomLicRep+"'"+
						", IN_NUM_NOTARIO_PUBLICO_REP= "+numNotarioRep+", CG_ORIGEN_NOTARIO_REP= '"+oriNotarioRep+"', CS_BANDERA_REP ='"+banderaRep+"' , CG_DOMICILIO_LEGAL = '"+domicilioLegal+"' where ic_if= "+claveIF+" and ic_pyme= "+clavePymeAnt;
			
			con.ejecutaSQL(strSQL);

			log.debug("strSQL:::::::: :: "+strSQL);			
				
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
		}finally{
			log.info("modificarCedente :::: (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}	
		}
	}
	
	public boolean existeCedente(String claveIF, String clavePyme){
		log.info("CesionEJBBean::existeCedente(E)");
		AccesoDB	con					= new AccesoDB();
		StringBuffer qrySentencia	= new StringBuffer();
		List		lVarBind				= new ArrayList();
		boolean	bOK					= false;
		try {
			con.conexionDB();
			qrySentencia.append(
				" SELECT 'TRUE' "	+
				"  FROM comcat_cedente "	+
				"  WHERE ic_if = ? "	+
				"	AND ic_pyme = ? "
			);
			lVarBind.add(claveIF);
			lVarBind.add(clavePyme);

			Registros reg = con.consultarDB(qrySentencia.toString(), lVarBind);

			while(reg!=null && reg.next()){
				bOK = true;
			}
		} catch(Exception e){
			log.error("CesionEJBBean::existeCedente(Exception)"+e);
			throw new AppException("Error al obtener el cedente");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CesionEJBBean::existeCedente(S)");
		}
		return bOK;
	}
	
	
	public void guardaCesionario(String claveIF, String tipoRep, String nomRep, String figJuridica, String escPublica,
									  String fecEscPublica, String nomLic, String numNotario, String oriNotario, String regComercio,
									  String numFolio, String clabe,String escPublicaRep, String fecEscPublicaRep,
									  String nomLicRep, String numNotarioRep, String oriNotarioRep,String banderaRep,String domicilioLegal,
									  String strDirectorioTemp,String nombrePoderesIF,String nombreExtincionIF
									  ){
		
		log.info("guardaCesionario :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			strSQL=" INSERT INTO COMCAT_CESIONARIO "+
						  "( IC_IF, CG_TIPO_REPRESENTANTE, CG_NOM_REPRESENTANTE, CG_FIGURA_JURIDICA, CG_ESCRITURA_PUBLICA, DF_ESCRITURA_PUBLICA,"+
						  " CG_NOMBRE_LICENCIADO, IN_NUM_NOTARIO_PUBLICO, CG_ORIGEN_NOTARIO, CG_REG_PUB_COMERCIO, IN_NUM_FOLIO_MERCANTIL, CG_CUENTA_CLABE,"+
						  "CG_ESCRITURA_PUBLICA_REP, DF_ESCRITURA_PUBLICA_REP, CG_NOMBRE_LICENCIADO_REP,"+
				 "IN_NUM_NOTARIO_PUBLICO_REP,CG_ORIGEN_NOTARIO_REP,CS_BANDERA_REP,CG_DOMICILIO_LEGAL) "+
						  " VALUES("+ claveIF +",'"+ tipoRep +"','"+ nomRep +"','"+ figJuridica +"',"+ escPublica +","+ 
						  " TO_DATE('" + fecEscPublica +"','DD/MM/YYYY'),'" + nomLic +"',"+ numNotario +",'"+ oriNotario +"','"+ regComercio +"',"+ numFolio +",'"+clabe+"' , "+
						  escPublicaRep+ ", TO_DATE('" + fecEscPublicaRep +"','DD/MM/YYYY'),'" + nomLicRep +"',"+ numNotarioRep +",'"+ oriNotarioRep +"','"+banderaRep+"','"+domicilioLegal+"')";
			
			con.ejecutaSQL(strSQL);
			
			commit = insertarPoderesIF(con ,nombrePoderesIF,nombreExtincionIF,strDirectorioTemp, claveIF);  

			log.debug("strSQL:::::::: :: "+strSQL);			
				
			}catch(Exception e){
				commit = false;
				e.printStackTrace();
				throw new AppException("Error al guardar el Cedente", e);
			}finally{
				log.info("guardaCesionario :::: (S)");
				if (con.hayConexionAbierta()){
					con.terminaTransaccion(commit);
					con.cierraConexionDB();
				}	
			}				  
	}
	
	public void modificarCesionario(String claveIF, String tipoRep, String nomRep, String figJuridica, String escPublica,
									  String fecEscPublica, String nomLic, String numNotario, String oriNotario, String regComercio,
									  String numFolio, String clabe,String escPublicaRep, String fecEscPublicaRep,
									  String nomLicRep, String numNotarioRep, String oriNotarioRep,String banderaRep,String domicilioLegal,
									  String strDirectorioTemp,String nombrePoderesIF,String nombreExtincionIF){
									  
		log.info("modificarCesionario :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			strSQL = " update comcat_cesionario set CG_TIPO_REPRESENTANTE= '"+tipoRep+"', CG_NOM_REPRESENTANTE= '"+nomRep+
						"', CG_FIGURA_JURIDICA= '"+figJuridica+"', CG_ESCRITURA_PUBLICA= "+escPublica+", DF_ESCRITURA_PUBLICA = TO_DATE('" + fecEscPublica +"','DD/MM/YYYY')"+
						", CG_NOMBRE_LICENCIADO= '"+nomLic+"', IN_NUM_NOTARIO_PUBLICO= "+numNotario+", CG_ORIGEN_NOTARIO= '"+oriNotario+"', CG_REG_PUB_COMERCIO= '"+regComercio+"'"+
						", IN_NUM_FOLIO_MERCANTIL= "+numFolio+", CG_CUENTA_CLABE= '"+clabe+"'"+
						", CG_ESCRITURA_PUBLICA_REP= "+escPublicaRep+", DF_ESCRITURA_PUBLICA_REP = TO_DATE('" + fecEscPublicaRep +"','DD/MM/YYYY'), CG_NOMBRE_LICENCIADO_REP= '"+nomLicRep+"'"+
						", IN_NUM_NOTARIO_PUBLICO_REP= "+numNotarioRep+", CG_ORIGEN_NOTARIO_REP= '"+oriNotarioRep+"', CS_BANDERA_REP ='"+banderaRep+"' , CG_DOMICILIO_LEGAL = '"+domicilioLegal+"'"+
						" where ic_if= "+claveIF;
			
			con.ejecutaSQL(strSQL);
			
			commit = insertarPoderesIF(con ,nombrePoderesIF,nombreExtincionIF,strDirectorioTemp, claveIF);  

			log.debug("strSQL:::::::: :: "+strSQL);			
				
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error al guardar el Cedente", e);
		}finally{
			log.info("eliminaCesionario :::: (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}	
		}								  
	}
	
	public boolean existeCesionario(String claveIF){
		log.info("CesionEJBBean::existeCesionario(E)");
		AccesoDB	con					= new AccesoDB();
		StringBuffer qrySentencia	= new StringBuffer();
		List		lVarBind				= new ArrayList();
		boolean	bOK					= false;
		try {
			con.conexionDB();
			qrySentencia.append(
				" SELECT 'TRUE' "	+
				"  FROM comcat_cesionario "	+
				"  WHERE ic_if = ? "
			);
			lVarBind.add(claveIF);

			Registros reg = con.consultarDB(qrySentencia.toString(), lVarBind);

			while(reg!=null && reg.next()){
				bOK = true;
			}
		} catch(Exception e){
			log.error("CesionEJBBean::existeCesionario(Exception)"+e);
			throw new AppException("Error al obtener el cesionario");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CesionEJBBean::existeCesionario(S)");
		}
		return bOK;
		
	}
	public boolean existeRepresentante2(String claveIF){
		log.info("CesionEJBBean::existeRepresentante2(E)");
		AccesoDB	con					= new AccesoDB();
		StringBuffer qrySentencia	= new StringBuffer();
		List		lVarBind				= new ArrayList();
		boolean	bOK					= false;
		try {
			con.conexionDB();
			qrySentencia.append(
				" SELECT cs_bandera_rep2 "	+
				"  FROM comcat_cesionario "	+
				"  WHERE ic_if = ? "
			);
			lVarBind.add(claveIF);

			Registros reg = con.consultarDB(qrySentencia.toString(), lVarBind);

			while(reg!=null && reg.next()){
				if(reg.getString("CS_BANDERA_REP2").equals("S")){
					bOK = true;
				}
			}
		} catch(Exception e){
			log.error("CesionEJBBean::existeRepresentante2(Exception)"+e);
			throw new AppException("Error al obtener el cesionario");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CesionEJBBean::existeRepresentante2(S)");
		}
		return bOK;
		
	}


	public String  getloginRepresentante2(String claveIF){
		log.info("getloginRepresentante2(E)");
		AccesoDB	con					= new AccesoDB();
		StringBuffer qrySentencia	= new StringBuffer();
		List		lVarBind				= new ArrayList();
		boolean	bOK					= false;
		String login ="";
		try {
			con.conexionDB();
			qrySentencia.append(
				" SELECT IC_IF_REP2 "	+
				"  FROM comcat_cesionario "	+
				"  WHERE ic_if = ? "
			);
			lVarBind.add(claveIF);

			Registros reg = con.consultarDB(qrySentencia.toString(), lVarBind);

			while(reg!=null && reg.next()){
				login = reg.getString("IC_IF_REP2")==null?"":reg.getString("IC_IF_REP2");								
			}
			
		} catch(Exception e){
			log.error("getloginRepresentante2(Exception)"+e);
			throw new AppException("Error al obtener el cesionario");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getloginRepresentante2(S)");
		}
		return login;
		
	}
	
	
/**
	 * Fodea 033-2013  
	 * @return 
	 * @param Solicitud  
	 */  
	 
	 public String generarContrato(String clave_solicitud, String strDirectorioTemp){
			log.debug("generarContrato(E) ");

			HashMap mapaDatosSolicitud = this.consultaContratoCesionPyme(clave_solicitud);
			String icPyme= (String) mapaDatosSolicitud.get("clave_pyme");
			String icIF= (String) mapaDatosSolicitud.get("clave_if");
			Registros mapaDatosCedente = this.getMapaCedente(icPyme,clave_solicitud);
			Registros mapaDatosCesionario = this.getMapaCesionario(icIF);
			String cuentaClabe =(String) mapaDatosSolicitud.get("numeroCuentaClabe");

			CreaArchivo archivo = new CreaArchivo();
			String nombreArchivo = archivo.nombreArchivo()+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo,"", false, false, false);
			SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:SS a");	//HH es formato 24 horas.
			String hora = sdf.format(new java.util.Date());
			String dia	= new SimpleDateFormat("dd/mm/yyyy").format(new java.util.Date());
			String cedente="",tipoRepresentante="",nombreRepresentante1="",nombreRepresentante2="",cesionarioIF="",tipoRepresentanteCes="";
			String nombreRepApo="",fechaCedeDerechos="",tipoCedente="",tipoContratacion="",numeroContrato="",objetoContrato="";
			String nombreOrganismo="",monedas="",fechaConsentimiento="",firmaJuridica="",firmaJuridicaCed="",escrituraPublica="",fechaEscritura="",nombreLic="",numNotario="";
			String ciudadOrig="",registroPublico="",folioMercantil="",fechaRegistroPublico="",fechaEscrituraPublica="",escrituraPublicaCed="",fechaRegistroPublicoCed="";
			String nombreLicCed="",numNotarioCed="",ciudadOrigCed="",registroPubCed="",folioMercantilCed="",fechaFinalContrato="", rfc ="";

			String escrituraPublicaCedRep="",fechaRegistroPublicoCedRep="",nombreLicCedRep="",numNotarioCedRep="",ciudadOrigCedRep="",domicilioLegalCed="";
			String escrituraPublicaCesRep="",fechaRegistroPublicoCesRep="",nombreLicCesRep="",numNotarioCesRep="",ciudadOrigCesRep="",domicilioLegalCes="";

			//Representante IF2
			String banderaRep="",escrituraPubRep2="",fechaEsctituraRep2="",nombreLicRep2="",numNotarioRep2="",ciudadNotarioRep2="";
			String empresasRep =(String)mapaDatosSolicitud.get("CG_EMPRESAS_REPRESENTADAS");
			empresasRep=empresasRep!=null?empresasRep:"";

			//F023-2015 (E)
			String claveGrupo =(String)mapaDatosSolicitud.get("ic_grupo_cesion");
			String cvePymeGrupo = "", nombrePymeGrupo = "", representantes = "",  textRepGrupo = "";
			List lstEmpresasXGrupo  = new ArrayList();

			UtilUsr utilUsr = new UtilUsr();

			try {


			cedente=(String)mapaDatosSolicitud.get("nombre_pyme");
			nombreRepresentante1=(String)mapaDatosSolicitud.get("nombreUsrAutRep1");
			nombreRepresentante1=nombreRepresentante1==null?"":nombreRepresentante1;

			nombreRepresentante2=(String)mapaDatosSolicitud.get("nombreUsrAutRep2");
			nombreRepresentante2=nombreRepresentante2==null?"":", "+nombreRepresentante2;
			cesionarioIF=(String)mapaDatosSolicitud.get("nombre_if");


			fechaCedeDerechos=(String)mapaDatosSolicitud.get("firma_contrato");

			tipoContratacion=(String)mapaDatosSolicitud.get("tipo_contratacion");
			numeroContrato=(String)mapaDatosSolicitud.get("numero_contrato");
			objetoContrato=(String)mapaDatosSolicitud.get("objeto_contrato");
			nombreOrganismo=(String)mapaDatosSolicitud.get("nombre_epo");
			monedas=mapaDatosSolicitud.get("montosPorMoneda").toString().replaceAll("<br/>",", ");
			fechaConsentimiento=(String)mapaDatosSolicitud.get("fechaAceptacionEpo");

			if(mapaDatosCedente.next()){
				tipoCedente=mapaDatosCedente.getString("CG_TIPO_CEDENTE");
				//tipoRepresentante=mapaDatosCedente.getString("CG_TIPO_REPRESENTANTE");
				escrituraPublicaCed=mapaDatosCedente.getString("CG_ESCRITURA_PUBLICA");
				fechaRegistroPublicoCed=mapaDatosCedente.getString("DF_ESCRITURA_PUBLICA");
				nombreLicCed=mapaDatosCedente.getString("CG_NOMBRE_LICENCIADO");
				numNotarioCed=mapaDatosCedente.getString("IN_NUM_NOTARIO_PUBLICO");
				ciudadOrigCed=mapaDatosCedente.getString("CG_ORIGEN_NOTARIO");
				registroPubCed=mapaDatosCedente.getString("CG_REG_PUB_COMERCIO");
				folioMercantilCed=mapaDatosCedente.getString("IN_NUM_FOLIO_MERCANTIL");
				domicilioLegalCed=mapaDatosCedente.getString("CG_DOMICILIO_LEGAL");
				//fechaFinalContrato=mapaDatosCedente.getString("");
				firmaJuridicaCed =mapaDatosCedente.getString("CG_FIGURA_JURIDICA");
				//escrituraPublicaCedRep=mapaDatosCedente.getString("CG_ESCRITURA_PUBLICA_REP");
				//fechaRegistroPublicoCedRep=mapaDatosCedente.getString("DF_ESCRITURA_PUBLICA_REP");
				//nombreLicCedRep=mapaDatosCedente.getString("CG_NOMBRE_LICENCIADO_REP");
				//numNotarioCedRep=mapaDatosCedente.getString("IN_NUM_NOTARIO_PUBLICO_REP");
				//ciudadOrigCedRep=mapaDatosCedente.getString("CG_ORIGEN_NOTARIO_REP");


			}
			if(mapaDatosCesionario.next()){
				nombreRepApo=mapaDatosCesionario.getString("CG_NOM_REPRESENTANTE");
				tipoRepresentanteCes=mapaDatosCesionario.getString("CG_TIPO_REPRESENTANTE");
				escrituraPublica=mapaDatosCesionario.getString("CG_ESCRITURA_PUBLICA");
				fechaRegistroPublico=mapaDatosCesionario.getString("DF_ESCRITURA_PUBLICA");
				nombreLic=mapaDatosCesionario.getString("CG_NOMBRE_LICENCIADO");
				numNotario=mapaDatosCesionario.getString("IN_NUM_NOTARIO_PUBLICO");
				ciudadOrig=mapaDatosCesionario.getString("CG_ORIGEN_NOTARIO");
				registroPublico=mapaDatosCesionario.getString("CG_REG_PUB_COMERCIO");
				folioMercantil=mapaDatosCesionario.getString("IN_NUM_FOLIO_MERCANTIL");
				firmaJuridica=mapaDatosCesionario.getString("CG_FIGURA_JURIDICA");
				domicilioLegalCes=mapaDatosCesionario.getString("CG_DOMICILIO_LEGAL");

				escrituraPublicaCesRep=mapaDatosCesionario.getString("CG_ESCRITURA_PUBLICA_REP");
				fechaRegistroPublicoCesRep=mapaDatosCesionario.getString("DF_ESCRITURA_PUBLICA_REP");
				nombreLicCesRep=mapaDatosCesionario.getString("CG_NOMBRE_LICENCIADO_REP");
				numNotarioCesRep=mapaDatosCesionario.getString("IN_NUM_NOTARIO_PUBLICO_REP");
				ciudadOrigCesRep=mapaDatosCesionario.getString("CG_ORIGEN_NOTARIO_REP");


				banderaRep=mapaDatosCesionario.getString("CS_BANDERA_REP2");
				escrituraPubRep2=mapaDatosCesionario.getString("CG_ESCRITURA_PUBLICA_REP2");
				fechaEsctituraRep2=mapaDatosCesionario.getString("FECHA_ESCRITURA_REP2");
				nombreLicRep2=mapaDatosCesionario.getString("CG_NOMBRE_LICENCIADO_REP2");
				numNotarioRep2=mapaDatosCesionario.getString("IN_NUM_NOTARIO_PUBLICO_REP2");
				ciudadNotarioRep2=mapaDatosCesionario.getString("CG_ORIGEN_NOTARIO_REP2");
				rfc=mapaDatosCesionario.getString("RFC");

			}

			if(tipoCedente==null){
				tipoCedente="";
			}
			if (tipoCedente.equals("C")){
				tipoCedente= "Contratista";
			}else if(tipoCedente.equals("P")){
				tipoCedente="Proveedor";
			}

			if(tipoRepresentanteCes==null){
				tipoRepresentanteCes="";
			}
			if (tipoRepresentanteCes.equals("L")){
				tipoRepresentanteCes= "Representante Legal";
			}else if(tipoRepresentanteCes.equals("A")){
				tipoRepresentanteCes="Apoderado";
			}

			if(tipoRepresentante==null){
				tipoRepresentante="";
			}
			if (tipoRepresentante.equals("L")){
				tipoRepresentante= "Representante Legal";
			}else if(tipoRepresentante.equals("A")){
				tipoRepresentante="Apoderado";
			}

			if(!claveGrupo.equals("") &&  claveGrupo !=null ){

					lstEmpresasXGrupo = getEmpresasXGrupoCder(claveGrupo);
					if(lstEmpresasXGrupo.size()>0)  {
						for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
							cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
							nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
							representantes = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("representantes");
							textRepGrupo += nombrePymeGrupo + " REPRESENTADA EN ESTE ACTO POR "+ representantes+", ";
						}
					}
				}else  {
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(icPyme, "P");
					for (int i = 0; i < usuariosPorPerfil.size(); i++) {
						String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
						Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
						representantes +=  usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
					}
					textRepGrupo = cedente + " REPRESENTADA EN ESTE ACTO POR "+ representantes+", ";

				}

			pdfDoc.addText("\n","contrato",ComunesPDF.CENTER);
			pdfDoc.setTable(1,70);

			pdfDoc.setCell("Contrato de Cesi�n de Derechos de Cobro","contrato",ComunesPDF.CENTER,1,1,0);
			pdfDoc.setCell("CONTRATO DE CESI�N DE DERECHOS DE COBRO (EN LO SUCESIVO EL \"CONTRATO DE CESI�N\") QUE CELEBRAN POR UNA PARTE "+
//			textRepGrupo+" EN TODOS ELLOS , EN SU CAR�CTER DE CEDENTES; Y POR LA OTRA PARTE "+cesionarioIF+", REPRESENTADA EN ESTE ACTO POR "+nombreRepApo +" EN SU CAR�CTER DE CESIONARIO, "+
			textRepGrupo+" EN SU CAR�CTER DE CEDENTES; Y POR LA OTRA PARTE "+cesionarioIF+", REPRESENTADA EN ESTE ACTO POR "+nombreRepApo +" EN SU CAR�CTER DE CESIONARIO, "+
			" AL TENOR DE LOS SIGUIENTES ANTECEDENTES, DECLARACIONES Y CL�USULAS:","contrato",ComunesPDF.JUSTIFIED,1,1,0);


			pdfDoc.setCell("ANTECEDENTES","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("I.- Con fecha "+fechaCedeDerechos+", el Cedente en su car�cter de "+tipoCedente+" y "+nombreOrganismo+" (Empresa) como  contratante, celebraron un Contrato de "+tipoContratacion+" No. "+numeroContrato+", mediante el cual el Cedente se oblig� a: "+objetoContrato+
			". Como contraprestaci�n la Empresa "+nombreOrganismo+" se oblig� a pagar al Cedente, la cantidad de "+monedas+", m�s el Impuesto al Valor Agregado (IVA), en los t�rminos y condiciones que aparecen en dicho instrumento contractual (Contrato).","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("II.-	Con fecha "+fechaConsentimiento+", "+nombreOrganismo+" otorg� su consentimiento al Cedente para ceder a "+cesionarioIF+", el 100% (cien por ciento)  de los derechos de cobro que lleguen a derivarse de la ejecuci�n y cumplimiento del Contrato de "+tipoContratacion+" No. "+numeroContrato+
			", incluidos los incrementos al monto original del mismo que, en su caso, se pacten con posterioridad mediante la formalizaci�n de convenios modificatorios. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("D E C L A R A C I O N E S","contrato",ComunesPDF.CENTER,1,1,0);

			pdfDoc.setCell("I.	Declara el Cesionario, por conducto de su representante, que:","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("I.1  Es una sociedad legalmente constituida de conformidad con las disposiciones legales de los Estados Unidos Mexicanos mediante Escritura P�blica No. "+escrituraPublica+", de fecha "+fechaRegistroPublico+", otorgada ante la fe del Licenciado  "+nombreLic+", Notario P�blico No. "+numNotario+" de la Ciudad de "+ciudadOrig+", cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de "+registroPublico+" en el folio"+
			" mercantil No. "+folioMercantil+", con fecha "+fechaRegistroPublico+".","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			//Representante(s) if
			//pdfDoc.setCell("Representante IF 1","contrato",ComunesPDF.JUSTIFIED,1,1,0);

//			pdfDoc.setCell("I.2	Su(s) representante(s) cuenta(n) con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita(n) mediante Escritura(s) P�blica(s) No. "+escrituraPublicaCesRep+", de fecha "+fechaRegistroPublicoCesRep+", otorgada ante la fe del Licenciado "+nombreLicCesRep+", Notario P�blico No. "+numNotarioCesRep+" de la Ciudad de "+ciudadOrigCesRep+
			pdfDoc.setCell("I.2  Cuenta con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita(n) mediante Escritura(s) P�blica(s) No. "+escrituraPublicaCesRep+", de fecha "+fechaRegistroPublicoCesRep+", otorgada ante la fe del Licenciado "+nombreLicCesRep+", Notario P�blico No. "+numNotarioCesRep+" de la Ciudad de "+ciudadOrigCesRep+
			", que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.","contrato",ComunesPDF.JUSTIFIED,1,1,0);

            int clausula=3;
			if(banderaRep.equals("S")) {
			//	pdfDoc.setCell("Representante IF 2","contrato",ComunesPDF.JUSTIFIED,1,1,0);
				pdfDoc.setCell("I."+clausula+"  Su(s) representante(s) cuenta(n) con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita(n) mediante Escritura(s) P�blica"+
				" No. "+escrituraPubRep2+", de fecha "+fechaEsctituraRep2+", otorgada ante la fe del Licenciado "+nombreLicRep2+", Notario P�blico No. "+numNotarioRep2+" de la Ciudad de "+ciudadNotarioRep2+", que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
                clausula++;
            }

			pdfDoc.setCell("I."+clausula+"  Para efectos del presente contrato se�ala como domicilio legal  el siguiente: "+domicilioLegalCes,"contrato",ComunesPDF.JUSTIFIED,1,1,0);


			//---Datos de la o las Pymes
			pdfDoc.setCell("II.	Declara el CEDENTE, por conducto de su(s) representante(s) que:","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			//F23-2015
				int y = 0;


			if(!claveGrupo.equals("")  && claveGrupo !=null  ){
				if(lstEmpresasXGrupo.size()>0)  {
					for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
						y++;
						cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
						nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
						representantes = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("representantes");

						HashMap datosPyme =  getDatosConstitutivosPyme(clave_solicitud, cvePymeGrupo );

						if(datosPyme.size()>0) {
							escrituraPublicaCed = datosPyme.get("ESCRITURA_PUBLICA").toString();
							fechaRegistroPublicoCed = datosPyme.get("DF_ESCRITURA_PUBLICA").toString();
							nombreLicCed  = datosPyme.get("NOMBRE_LICENCIADO").toString();
							numNotarioCed  = datosPyme.get("NUM_NOTARIO_PUBLICO").toString();
							ciudadOrigCed  = datosPyme.get("ORIGEN_NOTARIO").toString();
							registroPubCed  = datosPyme.get("REG_PUB_COMERCIO").toString();
							folioMercantilCed  = datosPyme.get("NUM_FOLIO_MERCANTIL").toString();
						}

						pdfDoc.setCell("II."+y+"  "+nombrePymeGrupo +" Es una sociedad legalmente constituida de conformidad con las disposiciones legales de los Estados Unidos Mexicanos mediante Escritura P�blica No. "+escrituraPublicaCed+", de fecha "+fechaRegistroPublicoCed+", otorgada ante la fe del Licenciado "+nombreLicCed+", Notario P�blico No. "+numNotarioCed+" de la Ciudad de "+ciudadOrigCed+", cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de "+
						registroPubCed+" en el folio mercantil No. "+folioMercantilCed+", con fecha "+	fechaRegistroPublicoCed+".","contrato",ComunesPDF.JUSTIFIED,1,1,0);


						List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(cvePymeGrupo, "P");
						for (int i = 0; i < usuariosPorPerfil.size(); i++) {
							y++;
							String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
							Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
							String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();

							HashMap datosRep= getRepresentantesCEDENTE( clave_solicitud, cvePymeGrupo, loginUsrPyme );
							if(datosRep.size()>0) {
								escrituraPublicaCedRep= datosRep.get("ESCRITURA_PUBLICA").toString();
								fechaRegistroPublicoCedRep =datosRep.get("DF_PUBLICA").toString();
								nombreLicCedRep = datosRep.get("NOMBRE_LICENCIADO").toString();
								numNotarioCedRep =datosRep.get("NUM_NOTARIO_PUBLICO").toString();
								ciudadOrigCedRep =datosRep.get("ORIGEN_NOTARIO").toString();

//								pdfDoc.setCell("II."+y+ "	Su representante " +nombresRepresentantesPyme + " cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato de cesi�n, lo cual acredita con la Escritura P�blica No. "+escrituraPublicaCedRep+", de fecha "+fechaRegistroPublicoCedRep+", otorgada ante la fe del Licenciado "+nombreLicCedRep+", Notario P�blico No. "+numNotarioCedRep+
								pdfDoc.setCell("II."+y+ "	" +nombresRepresentantesPyme + " cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato de cesi�n, lo cual acredita con la Escritura P�blica No. "+escrituraPublicaCedRep+", de fecha "+fechaRegistroPublicoCedRep+", otorgada ante la fe del Licenciado "+nombreLicCedRep+", Notario P�blico No. "+numNotarioCedRep+
								" de la Ciudad de "+ciudadOrigCedRep+", que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
							}
						}
					}
				}
			}else  {
				icPyme= (String) mapaDatosSolicitud.get("clave_pyme");
				//DATOS DE LA PYME
				y++;
				HashMap datosPyme =  getDatosConstitutivosPyme(clave_solicitud, icPyme );
				log.debug(datosPyme.size() +" === "+datosPyme);
				if(datosPyme.size()>0) {
					escrituraPublicaCed = datosPyme.get("ESCRITURA_PUBLICA").toString();
					fechaRegistroPublicoCed = datosPyme.get("DF_ESCRITURA_PUBLICA").toString();
					nombreLicCed  = datosPyme.get("NOMBRE_LICENCIADO").toString();
					numNotarioCed  = datosPyme.get("NUM_NOTARIO_PUBLICO").toString();
					ciudadOrigCed  = datosPyme.get("ORIGEN_NOTARIO").toString();
					registroPubCed  = datosPyme.get("REG_PUB_COMERCIO").toString();
					folioMercantilCed  = datosPyme.get("NUM_FOLIO_MERCANTIL").toString();
				}
				pdfDoc.setCell("II."+y+"  "+cedente +" Es una sociedad legalmente constituida de conformidad con las disposiciones legales de los Estados Unidos Mexicanos mediante Escritura P�blica No. "+escrituraPublicaCed+", de fecha "+fechaRegistroPublicoCed+", otorgada ante la fe del Licenciado "+nombreLicCed+", Notario P�blico No. "+numNotarioCed+" de la Ciudad de "+ciudadOrigCed+", cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de "+
				registroPubCed+" en el folio mercantil No. "+folioMercantilCed+", con fecha "+	fechaRegistroPublicoCed+".","contrato",ComunesPDF.JUSTIFIED,1,1,0);


					// DATOS DE SUS REPRESENTANTES

				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(icPyme, "P");
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {
					y++;
					String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
					Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
					String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();

					HashMap datosRep= getRepresentantesCEDENTE( clave_solicitud, icPyme, loginUsrPyme );
					log.debug(datosPyme.size() +" === "+datosRep);
					if(datosRep.size()>0) {
						escrituraPublicaCedRep= datosRep.get("ESCRITURA_PUBLICA").toString();
						fechaRegistroPublicoCedRep =datosRep.get("DF_PUBLICA").toString();
						nombreLicCedRep = datosRep.get("NOMBRE_LICENCIADO").toString();
						numNotarioCedRep =datosRep.get("NUM_NOTARIO_PUBLICO").toString();
						ciudadOrigCedRep =datosRep.get("ORIGEN_NOTARIO").toString();

//						pdfDoc.setCell("II."+y+ "	Su representante " +nombresRepresentantesPyme + " cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato de cesi�n, lo cual acredita con la Escritura P�blica No. "+escrituraPublicaCedRep+", de fecha "+fechaRegistroPublicoCedRep+", otorgada ante la fe del Licenciado "+nombreLicCedRep+", Notario P�blico No. "+numNotarioCedRep+" de la Ciudad de "+ciudadOrigCedRep+
						pdfDoc.setCell("II."+y+ "	" +nombresRepresentantesPyme + " cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato de cesi�n, lo cual acredita con la Escritura P�blica No. "+escrituraPublicaCedRep+", de fecha "+fechaRegistroPublicoCedRep+", otorgada ante la fe del Licenciado "+nombreLicCedRep+", Notario P�blico No. "+numNotarioCedRep+" de la Ciudad de "+ciudadOrigCedRep+
						", que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.","contrato",ComunesPDF.JUSTIFIED,1,1,0);

					}

				}

			}


			y++;
			pdfDoc.setCell("II."+y+" La Empresa otorg� Consentimiento Condicionado para ceder a la CESIONARIA el 100% (cien por ciento) de los derechos de cobro que se deriven por la ejecuci�n y cumplimiento del Contrato No. "+numeroContrato+".","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			y++;
			pdfDoc.setCell("II."+y+" Para efectos del presente contrato se�ala como domicilio legal  el siguiente: "+domicilioLegalCed,"contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("III.	Declaran las partes por conducto de sus representantes que:","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("III.1  Que est�n de acuerdo en obligarse en los t�rminos que se precisan en las cl�usulas del presente contrato, mismas que han sido pactadas sin  que exista dolo, error, mala fe, manifestando que conocen las obligaciones que asumen en virtud de la formalizaci�n del mismo. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("III.2   Se reconocen la personalidad con la que se ostentan sus representantes.","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("Expuesto lo anterior, las partes otorgan su consentimiento y se someten a lo que se consigna en las siguientes:\n\n","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("C L � U S U L A S","contrato",ComunesPDF.CENTER,1,1,0);

			pdfDoc.setCell("PRIMERA. El CEDENTE cede a favor de la CESIONARIA el 100% (cien por ciento) de los derechos de cobro que lleguen a generarse conforme a lo estipulado en el Contrato No. "+numeroContrato+", y sus convenios modificatorios al monto original del mismo que, en su caso, se formalicen con posterioridad a la fecha de firma del presente Contrato de Cesi�n, y el Cesionario acepta la cesi�n de derechos de cobro que adquiere bajo el presente Contrato de Cesi�n.","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			pdfDoc.setCell("Por lo anterior las partes  aceptan y acuerdan que:","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("1.- La Cesi�n de Derechos de Cobro ser� por el 100% (cien por ciento) de los derechos de cobro del Contrato se�alado, y la cesi�n surtir� sus efectos a partir de la aceptaci�n de la notificaci�n del presente Contrato de Cesi�n a la Empresa por medio del Sistema NAFIN. En el  entendido de que el Contrato de Cesi�n deber� contener lisa y llanamente la cesi�n de los derechos de cobro; no debi�ndose estipular en �l, "+
			" condiciones contrarias a los intereses de la Empresa.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			
			pdfDoc.setCell("2.- La Cesi�n de Derechos de Cobro incluir� los incrementos al monto del Contrato que, en su caso, se pacten con posterioridad mediante convenios modificatorios; y que la cesi�n de derechos de cobro �nica y exclusivamente proceder� con respecto de las facturas pendientes de cobro por los trabajos/servicios ejecutados, aceptados e instruidos por la Empresa, en t�rminos de lo pactado en el Contrato.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
                
			pdfDoc.setCell("3.- Que el Cedente, bajo protesta de decir verdad, garantiza que los derechos de cobro objeto de cesi�n, no han sido dados en garant�a, pignorados, embargados, ni gravados en forma alguna y que no existe controversia sobre los mismos.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("3.- Que el Cedente, bajo protesta de decir verdad, garantiza que los derechos de cobro objeto de cesi�n, no han sido dados en garant�a, pignorados, embargados, ni gravados en forma alguna y que no existe controversia sobre los mismos. ");
//								" \n En caso de que exista un embargo que no implique el total de los pagos derivados del contrato materia de la cesi�n de derechos de cobro, cedente y cesionario aceptan y est�n de acuerdo que el pago de los derechos de cobro estar� sujeto a que la Empresa realice las retenciones que deban hacerse al cedente con motivo del embargo en cuesti�n. La Empresa quedar� liberada de cualquier responsabilidad por dichas retenciones.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
            
			pdfDoc.setCell(" 4.- Que el Cedente y el Cesionario reconocen que el Cedente es el �nico responsable de garantizar que los derechos de cobro est�n libres de gravamen o disputa.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" 5.- Es obligaci�n exclusiva del Cedente, notificar de forma inmediata al Cesionario de cualquier acci�n de cobro, embargo o cualquier gravamen.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" 6.- El Cedente queda obligado a sacar en paz y a salvo a la Empresa de cualquier controversia relacionada con los grav�menes o las disputas que se originen por los derechos de cobro objeto de cesi�n. Lo anterior, sin menoscabo de las acciones que correspondan al Cesionario en contra del Cedente. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" 7.- Para el caso de que la Empresa, con posterioridad a la cesi�n de derechos de cobro, llegare a efectuar cualquier pago al Cedente en lugar del Cesionario respecto de dichos derechos de cobro, el Cedente quedar� constituido en depositario de dicho pago, para todos los efectos a que haya lugar y se obliga a restituirlo, en forma inmediata al Cesionario, sin necesidad de que medie aviso o requerimiento para ello.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("8.- Una vez cedidos los derechos de cobro, en caso de que la Empresa sea requerida por autoridad judicial o administrativa para retener o exhibir cualquier cantidad relacionada con los derechos de cobro objeto de la presente cesi�n, la Empresa, informar� a la autoridad emisora que los derechos de cobro derivados del Contrato se encuentran cedidos a favor del Cesionario; y en caso de que dicha autoridad confirme a la Empresa que �sta retenga y/o exhiba "+
										" cualquier cantidad relacionada con los derechos de cobro objeto de la presente cesi�n, la Empresa comunicar� dicha situaci�n al Cedente y al Cesionario, para que hagan valer lo que a su derecho convenga ante dicha autoridad.  El Cedente y Cesionario renuncian en este acto y en los t�rminos m�s amplios que en derecho proceda, al ejercicio de cualquier acci�n que les asista o que les pudiera corresponder en contra de la Empresa, derivado del cumplimiento que �ste realice "+
										" a cualquier requerimiento formulado o instado por las autoridades.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" 9.- El Cesionario acepta y est� de acuerdo en que el pago de los derechos de cobro estar� sujeto a que, en su caso, la Empresa realice las deducciones y/o retenciones que deban hacerse al cedente por adeudos con la misma, penas convencionales, pagos en exceso; cualquier requerimiento de pago derivado de laudo firme ordenado por las Juntas Locales o Federales de Conciliaci�n y Arbitraje, con motivo de los juicios laborales instaurados en contra de la Empresa en "+
								" relaci�n con el Cedente; o cualquier otra deducci�n derivada del Contrato o de la ley de la materia en relaci�n �nicamente con el mismo, en cuyos supuestos se har�n las deducciones y/o retenciones correspondientes. En tal virtud, la Empresa quedar� liberada de cualquier responsabilidad por las deducciones y/o retenciones a que haya lugar.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" 10.- El Cedente y Cesionario reconocen y est�n de acuerdo en que el consentimiento otorgado por la Empresa para ceder los derechos de cobro del Contrato que nos ocupa, incluidos los incrementos al monto original del mismo no constituye por su parte una garant�a de pago ni reconocimiento de cualquier derecho del Cesionario frente al Cedente, siendo la �nica obligaci�n de la Empresa, la de pagar cantidades l�quidas que sean exigibles conforme al Contrato, una vez hechas "+
								" las deducciones y/o retenciones antes mencionadas.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
//			pdfDoc.setCell(" SEGUNDA. Notificaci�n.- El Cesionario se obliga notificar a la Empresa el presente  Contrato de Cesi�n mediante el Sistema NAFIN, en un plazo no mayor a 30 (treinta) d�as naturales, contados a partir de la fecha de la notificaci�n del Consentimiento emitido la Empresa, lo anterior para que surta efectos legales la citada cesi�n en t�rminos de lo establecido en el art�culo 390 del C�digo de Comercio, en el entendido que de no efectuarse la mencionada "+
//			" notificaci�n dentro del citado plazo, el Consentimiento otorgado por la Empresa quedar� sin efectos sin necesidad de que la Empresa le comunique a las partes  dicha situaci�n. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" SEGUNDA.- Una vez que surta efectos la cesi�n de derechos, la Empresa deber� depositar los pagos que conforme a los t�rminos y condiciones del Contrato deba realizar a favor del Cedente, directamente en la CUENTA �NICA "+cuentaClabe+" del Banco "+cesionarioIF+" con Registro Federal de Contribuyentes (RFC) "+rfc+", en la inteligencia de que el Cedente no recibir� directamente pago alguno derivado del multicitado Contrato sino, como antes se indica, ser� recibido directamente por el Cesionario "+
			" mediante deposito en la cuenta antes referida. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" Para el caso de que, por cualquier motivo el Cedente llegare a recibir cualquier cantidad derivada de los derechos de cobro materia de este Contrato de Cesi�n, �ste se obliga a entregarla de inmediato al Cesionario, mediante dep�sito en la cuenta bancaria antes mencionada. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" TERCERA.- Libertad de Grav�menes. El Cedente, bajo protesta de decir verdad, garantiza al Cesionario por medio de la presente cl�usula, que los derechos de cobro cedidos en el presente Contrato de Cesi�n, no han sido dados en garant�a, embargados, pignorados ni gravados con anterioridad a la fecha de firma de este Contrato de Cesi�n, y que no existe controversia jur�dica sobre los mismos.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" CUARTA. De las Notificaciones. Todas las notificaciones, avisos y en general, cualquier comunicaci�n que las partes deban hacerse en el cumplimiento de esta cesi�n, se realizaran mediante el Sistema NAFIN. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" QUINTA.  Terminaci�n del Contrato de Cesi�n. Las partes  acuerdan que para dejar sin efectos el presente Contrato de Cesi�n previo a la extinci�n natural de dicha cesi�n, ser� necesario que el Cesionario as� lo comunique a la Empresa; a trav�s del Sistema NAFIN en los mismos t�rminos previstos para la notificaci�n del Contrato de Cesi�n Electr�nica  para que surta los efectos legales que le correspondan. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell(" SEXTA. Jurisdicci�n y Competencia Las partes convienen que todo lo relativo a la interpretaci�n, ejecuci�n y cumplimiento del presente Contrato de Cesi�n, se sujetar� a la jurisdicci�n y competencia de los jueces y tribunales de la Ciudad de M�xico, Distrito Federal, por lo que desde este momento renuncian a cualquier otro fuero que pudiera corresponderles en raz�n de sus domicilios presentes o futuros por privilegiados que fueren. ","contrato",ComunesPDF.JUSTIFIED,1,1,0);


			pdfDoc.addTable();
			pdfDoc.endDocument();



			} 	catch(Exception e) {

						log.error("generarContrato(Exception) " + e);
						e.printStackTrace();
					throw new AppException("generarContrato(Exception) ", e);
				}
				finally
				{

				}
			log.debug("generarContrato(S) ");
		return nombreArchivo;
	}


		/**
	 * Obtiene la informacion de la cedente Cesion de Derechos
	 * @param ic_pyme
	 * @param ic_solicitud
	 * @return tipo de datos Registro, que es una lista de objetos del query resultante
	 * 
	 */
	public Registros getMapaCedente(String icPyme, String icSolicitud) {
		log.info("getMapaCedente(E)");
		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			
			query.append("SELECT CG_TIPO_CEDENTE, \n"                            );
			query.append("       CG_FIGURA_JURIDICA, \n"                         );
			query.append("       CG_ESCRITURA_PUBLICA, \n"                       );
			query.append("       TO_CHAR (DF_ESCRITURA_PUBLICA, 'DD/MM/YYYY') \n");
			query.append("       AS DF_ESCRITURA_PUBLICA, \n"                    );
			query.append("       CG_NOMBRE_LICENCIADO, \n"                       );
			query.append("       IN_NUM_NOTARIO_PUBLICO, \n"                     );
			query.append("       CG_ORIGEN_NOTARIO, \n"                          );
			query.append("       CG_REG_PUB_COMERCIO, \n"                        );
			query.append("       IN_NUM_FOLIO_MERCANTIL, \n"                     );
			query.append("       CG_DOMICILIO_LEGAL \n"                          );
			query.append("  FROM CDER_SOLIC_PYME_NOTARIALES \n"                  );
			query.append(" WHERE IC_PYME = ? AND IC_SOLICITUD = ?"               );

			System.out.println("query: " +query.toString());
			System.out.println("icPyme: " + icPyme);
			System.out.println("icSolicitud: " + icSolicitud);

			List params = new ArrayList();
			params.add(new Integer(icPyme));
			params.add(new Integer(icSolicitud));
			Registros reg = con.consultarDB(query.toString(), params);
				return reg;
		} catch (Exception e) {
			log.error("getMapaCedente(Error)");
			throw new AppException("Ocurrio un error al obtener los campos de la cedente", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getMapaCedente(S)");
		}
	}
/*	 
	public Registros getMapaCedente(String ic_pyme, String ic_if) {
		log.info("getMapaCedente(E) ::..");
		AccesoDB con = new AccesoDB();
		StringBuffer query;
		
		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append("SELECT cg_tipo_representante, cg_tipo_cedente, cg_figura_juridica, " +
								"       cg_escritura_publica, to_char(df_escritura_publica,'dd/mm/yyyy') as df_escritura_publica, cg_nombre_licenciado, " +
								"       in_num_notario_publico, cg_origen_notario, CG_REG_PUB_COMERCIO, " +
								"       in_num_folio_mercantil, " +
								" cg_escritura_publica_rep,"+
								" to_char(df_escritura_publica_rep,'dd/mm/yyyy') as df_escritura_publica_rep,"+
								" cg_nombre_licenciado_rep,"+
								" in_num_notario_publico_rep,"+
								" cg_origen_notario_rep, cg_domicilio_legal "+
								"  FROM comcat_cedente " +
								" WHERE ic_pyme = ? AND ic_if = ? ");
			List params = new ArrayList();
			params.add(new Integer(ic_pyme));
			params.add(new Integer(ic_if));
			Registros reg = con.consultarDB(query.toString(), params);
				return reg;
		} catch (Exception e) {
			log.error("getMapaCedente(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los campos de la cedente", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getMapaCedente(S) ::..");
		}
	}
*/
			/**
	 * Obtiene la informacion de la cesionario Cesion de Derechos
	 * @param ic_pyme
	 * @param ic_if
	 * @return tipo de datos Registro, que es una lista de objetos del query resultante
	 * 
	 */
	public Registros getMapaCesionario(String ic_if) {
		log.info("getMapaCesionario(E) ::..");
		AccesoDB con = new AccesoDB();
		StringBuffer query;
		
		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append("SELECT cg_tipo_representante, cg_nom_representante, cg_figura_juridica, " +
								"       cg_escritura_publica, to_char(df_escritura_publica,'dd/mm/yyyy') as df_escritura_publica, cg_nombre_licenciado, " +
								"       in_num_notario_publico, cg_origen_notario, CG_REG_PUB_COMERCIO, " +
								"       in_num_folio_mercantil, " +
								" cg_escritura_publica_rep,"+
								" to_char(df_escritura_publica_rep,'dd/mm/yyyy') as df_escritura_publica_rep,"+
								" cg_nombre_licenciado_rep,"+
								" in_num_notario_publico_rep,"+
								" cg_origen_notario_rep, cg_domicilio_legal, "+
								" CS_BANDERA_REP2 , cg_escritura_publica_rep2 , "+
								" to_char(df_escritura_publica_rep2,'dd/mm/yyyy') as fecha_escritura_rep2 , cg_nombre_licenciado_rep2 , "+
								" in_num_notario_publico_rep2 , cg_origen_notario_rep2 , ic_if_rep2"+
								" ,i.CG_RFC  as RFC "+
		                  "  FROM comcat_cesionario c , comcat_if  i" +
		                  " WHERE  c.ic_if  = i.ic_if "+
		                  " and c.ic_if = ? "  );
			
			List params = new ArrayList();
			params.add(new Integer(ic_if));
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;
		} catch (Exception e) {
			log.error("getMapaCesionario(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los campos del cesionario", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getMapaCesionario(S) ::..");
		}
	}
	
		/**
	 * Obtiene de la base de datos el archivo del Consentimiento
	 * y lo prepara para su descarga desde la pantallas.
	 * @param clave_solicitud Es la clave de la solicitud de la que se obtiene el archivo del contrato de cesi�n.
	 * @param strDirectorioTemp Es el directorio donde se genera de manera temporal el archivo.
	 * @return file El archivo del contrato de cesi�n obtenido de la base de datos, este archivo est� en formato PDF.
	 * @throws netropology.utilerias.AppException Al encontrar un error inesperado
	 */
	public String descargaContratoCesionNuevo(String clave_solicitud, String strDirectorioTemp) throws AppException{
		log.info("descargaConsentimiento(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "";
		String nombreArchivo = "";
		
		try{
			con.conexionDB();

			strSQL.append(" SELECT bi_contrato_cesion");
			strSQL.append(" FROM cder_solicitud");
			strSQL.append(" WHERE ic_solicitud = ?");
			
			varBind.add(clave_solicitud);
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){
				nombreArchivo = Comunes.cadenaAleatoria(8) + ".pdf";
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob("bi_contrato_cesion");
				InputStream inStream = blob.getBinaryStream();
				int size = (int)blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1){
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("descargaConsentimiento(S)");
		}
		//return pathname;
		return nombreArchivo;
	}
	
		public String getBanderaContrato(String clave_solicitud){  
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      String       bandera  =  "";
		List lVarBind		= new ArrayList();
		log.info("getBanderaContrato (E)");
		 try{
         con.conexionDB();

         String strQry = "SELECT CS_BANDERA_CONTRATO  "  +
                        "  FROM cder_solicitud    "  +
                        " WHERE ic_solicitud = ?  ";
                       
			lVarBind		= new ArrayList();
         lVarBind.add(new Long(clave_solicitud));        
			ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next()){
				bandera = rs.getString("CS_BANDERA_CONTRATO");
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  clave_solicitud (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getBanderaContrato (S)");
      return bandera;
    }
	 
	 
	 /**
	 * Fodea 033-2013  
	 * @return 
	 * @param Solicitud  
	 */  
	 public String generarContratoExtincion(String clave_solicitud, String strDirectorioTemp){
			log.debug("generarContratoExtincion(E) ");
			
			HashMap mapaDatosSolicitud = this.consultaContratoCesionPyme(clave_solicitud);
			String icPyme= (String) mapaDatosSolicitud.get("clave_pyme");
			String icIF= (String) mapaDatosSolicitud.get("clave_if");
			Registros mapaDatosCedente = this.getMapaCedente(icPyme,icIF);
			Registros mapaDatosCesionario = this.getMapaCesionario(icIF);
			String cuentaClabe =(String) mapaDatosSolicitud.get("numeroCuentaClabe");
			
			CreaArchivo archivo = new CreaArchivo();
			String nombreArchivo = archivo.nombreArchivo()+".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivo,"", false, false, false);
			SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:SS a");	//HH es formato 24 horas.
			String hora = sdf.format(new java.util.Date());
			String dia	= new SimpleDateFormat("dd/mm/yyyy").format(new java.util.Date());
			String cedente="",tipoRepresentante="",nombreRepresentante1="",nombreRepresentante2="",cesionarioIF="",tipoRepresentanteCes="";
			String nombreRepApo="",fechaCedeDerechos="",tipoCedente="",tipoContratacion="",numeroContrato="",objetoContrato="";
			String nombreOrganismo="",monedas="",fechaConsentimiento="",firmaJuridica="",escrituraPublica="",fechaEscritura="",nombreLic="",numNotario="";
			String ciudadOrig="",registroPublico="",folioMercantil="",fechaRegistroPublico="",fechaEscrituraPublica="",escrituraPublicaCed="",fechaRegistroPublicoCed="";
			String nombreLicCed="",numNotarioCed="",ciudadOrigCed="",registroPubCed="",folioMercantilCed="",fechaFinalContrato="";
			
			String escrituraPublicaCedRep="",fechaRegistroPublicoCedRep="",nombreLicCedRep="",numNotarioCedRep="",ciudadOrigCedRep=""; 
			
			String escrituraPublicaCesRep="",fechaRegistroPublicoCesRep="",nombreLicCesRep="",numNotarioCesRep="",ciudadOrigCesRep="";
			
			//Representante IF2
			String banderaRep="",escrituraPubRep2="",fechaEsctituraRep2="",nombreLicRep2="",numNotarioRep2="",ciudadNotarioRep2="";
			String empresasRep =(String)mapaDatosSolicitud.get("CG_EMPRESAS_REPRESENTADAS");
			empresasRep=empresasRep!=null?empresasRep:"";
			/***** INI F024-2015 *****/
			String claveGrupo      =(String)mapaDatosSolicitud.get("ic_grupo_cesion");
			String cvePymeGrupo    = "";
			String nombrePymeGrupo = "";
			String representantes  = "";
			String textRepGrupo    = "";
			List lstEmpresasXGrupo = new ArrayList();
			UtilUsr utilUsr        = new UtilUsr();
			/***** FIN F024-2015 *****/
			try {

			cedente=(String)mapaDatosSolicitud.get("nombre_pyme");
			nombreRepresentante1=(String)mapaDatosSolicitud.get("nombreUsrAutRep1");
			nombreRepresentante1=nombreRepresentante1==null?"":nombreRepresentante1;
			
			nombreRepresentante2=(String)mapaDatosSolicitud.get("nombreUsrAutRep2");
			nombreRepresentante2=nombreRepresentante2==null?"":", "+nombreRepresentante2;
			cesionarioIF=(String)mapaDatosSolicitud.get("nombre_if");
			
			
			
			fechaCedeDerechos=(String)mapaDatosSolicitud.get("firma_contrato");
			
			tipoContratacion=(String)mapaDatosSolicitud.get("tipo_contratacion");
			numeroContrato=(String)mapaDatosSolicitud.get("numero_contrato");
			objetoContrato=(String)mapaDatosSolicitud.get("objeto_contrato");
			nombreOrganismo=(String)mapaDatosSolicitud.get("nombre_epo");
			monedas=mapaDatosSolicitud.get("montosPorMoneda").toString().replaceAll("<br/>",", ");
			fechaConsentimiento=(String)mapaDatosSolicitud.get("fechaAceptacionEpo");
			
			if(mapaDatosCedente.next()){
				tipoCedente=mapaDatosCedente.getString("CG_TIPO_CEDENTE");
				tipoRepresentante=mapaDatosCedente.getString("CG_TIPO_REPRESENTANTE");
				escrituraPublicaCed=mapaDatosCedente.getString("CG_ESCRITURA_PUBLICA");
				fechaRegistroPublicoCed=mapaDatosCedente.getString("DF_ESCRITURA_PUBLICA");
				nombreLicCed=mapaDatosCedente.getString("CG_NOMBRE_LICENCIADO");
				numNotarioCed=mapaDatosCedente.getString("IN_NUM_NOTARIO_PUBLICO");
				ciudadOrigCed=mapaDatosCedente.getString("CG_ORIGEN_NOTARIO");
				registroPubCed=mapaDatosCedente.getString("CG_REG_PUB_COMERCIO");
				folioMercantilCed=mapaDatosCedente.getString("IN_NUM_FOLIO_MERCANTIL");
				//fechaFinalContrato=mapaDatosCedente.getString("");
				escrituraPublicaCedRep=mapaDatosCedente.getString("CG_ESCRITURA_PUBLICA_REP");
				fechaRegistroPublicoCedRep=mapaDatosCedente.getString("DF_ESCRITURA_PUBLICA_REP");
				nombreLicCedRep=mapaDatosCedente.getString("CG_NOMBRE_LICENCIADO_REP");
				numNotarioCedRep=mapaDatosCedente.getString("IN_NUM_NOTARIO_PUBLICO_REP");
				ciudadOrigCedRep=mapaDatosCedente.getString("CG_ORIGEN_NOTARIO_REP");
			}
			if(mapaDatosCesionario.next()){
				nombreRepApo=mapaDatosCesionario.getString("CG_NOM_REPRESENTANTE");
				tipoRepresentanteCes=mapaDatosCesionario.getString("CG_TIPO_REPRESENTANTE");
				escrituraPublica=mapaDatosCesionario.getString("CG_ESCRITURA_PUBLICA");
				fechaRegistroPublico=mapaDatosCesionario.getString("DF_ESCRITURA_PUBLICA");
				nombreLic=mapaDatosCesionario.getString("CG_NOMBRE_LICENCIADO");
				numNotario=mapaDatosCesionario.getString("IN_NUM_NOTARIO_PUBLICO");
				ciudadOrig=mapaDatosCesionario.getString("CG_ORIGEN_NOTARIO");
				registroPublico=mapaDatosCesionario.getString("CG_REG_PUB_COMERCIO");
				folioMercantil=mapaDatosCesionario.getString("IN_NUM_FOLIO_MERCANTIL");
				firmaJuridica=mapaDatosCesionario.getString("cg_figura_juridica");
				
				
				escrituraPublicaCesRep=mapaDatosCesionario.getString("CG_ESCRITURA_PUBLICA_REP");
				fechaRegistroPublicoCesRep=mapaDatosCesionario.getString("DF_ESCRITURA_PUBLICA_REP");
				nombreLicCesRep=mapaDatosCesionario.getString("CG_NOMBRE_LICENCIADO_REP");
				numNotarioCesRep=mapaDatosCesionario.getString("IN_NUM_NOTARIO_PUBLICO_REP");
				ciudadOrigCesRep=mapaDatosCesionario.getString("CG_ORIGEN_NOTARIO_REP");
				
				banderaRep=mapaDatosCesionario.getString("CS_BANDERA_REP2");
				escrituraPubRep2=mapaDatosCesionario.getString("CG_ESCRITURA_PUBLICA_REP2");
				fechaEsctituraRep2=mapaDatosCesionario.getString("FECHA_ESCRITURA_REP2");
				nombreLicRep2=mapaDatosCesionario.getString("CG_NOMBRE_LICENCIADO_REP2");
				numNotarioRep2=mapaDatosCesionario.getString("IN_NUM_NOTARIO_PUBLICO_REP2");
				ciudadNotarioRep2=mapaDatosCesionario.getString("CG_ORIGEN_NOTARIO_REP2");
			}
			
			if(tipoCedente==null){
				tipoCedente="";
			}
			if (tipoCedente.equals("C")){
				tipoCedente= "Contratista";
			}else if(tipoCedente.equals("P")){
				tipoCedente="Proveedor";
			}
			
			if(tipoRepresentanteCes==null){
				tipoRepresentanteCes="";
			}
			if (tipoRepresentanteCes.equals("L")){
				tipoRepresentanteCes= "Representante Legal";
			}else if(tipoRepresentanteCes.equals("A")){
				tipoRepresentanteCes="Apoderado";
			}
			
			if(tipoRepresentante==null){
				tipoRepresentante="";
			}
			if (tipoRepresentante.equals("L")){
				tipoRepresentante= "Representante Legal";
			}else if(tipoRepresentante.equals("A")){
				tipoRepresentante="Apoderado";
			}
			
			/***** INI F024-2015 *****/
			if(!claveGrupo.equals("") && claveGrupo != null){
			
					lstEmpresasXGrupo = getEmpresasXGrupoCder(claveGrupo);
					if(lstEmpresasXGrupo.size()>0){
						for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
							cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
							nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
							representantes = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("representantes");
							textRepGrupo += nombrePymeGrupo + " REPRESENTADA EN ESTE ACTO POR "+ representantes+", ";
						}
					}

				} else{

					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(icPyme, "P");
					for (int i = 0; i < usuariosPorPerfil.size(); i++) {
						String loginUsrPyme = (String)usuariosPorPerfil.get(i);
						Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
						representantes += usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
					}
					textRepGrupo = cedente + " REPRESENTADA EN ESTE ACTO POR "+ representantes+", ";

				}
			/***** FIN F024-2015 *****/

			pdfDoc.addText("\n\n\n\n\n\n","contrato",ComunesPDF.CENTER);
			pdfDoc.setTable(1,70);
			
			pdfDoc.setCell("Convenio de Extinci�n de Cesi�n de Derechos de Cobro\n\n","contrato",ComunesPDF.CENTER,1,1,0);
			pdfDoc.setCell("CONVENIO DE EXTINCI�N DE  CESI�N DE DERECHOS DE COBRO (EN LO SUCESIVO EL \"CONVENIO DE EXTINCION\") QUE CELEBRAN POR UNA PARTE  " + textRepGrupo + " EN LO SUSCESIVO EN SU CONJUNTO COMO (EL) LOS CEDENTES; Y POR LA OTRA PARTE " + cesionarioIF + " REPRESENTADO EN ESTE ACTO POR " + nombreRepApo + " EN SU CAR�CTER DE CESIONARIO, AL TENOR DE LOS SIGUIENTES"+
			" ANTECEDENTES, DECLARACIONES Y CL�USULAS:\n\n","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("ANTECEDENTES\n\n","contrato",ComunesPDF.CENTER,1,1,0);
			pdfDoc.setCell("Con fecha "+fechaCedeDerechos+", el Cedente en su car�cter de "+tipoCedente+" y cesionario, celebraron un Contrato de Cesi�n de derechos de Cobro respecto del Contrato "+tipoContratacion+" No. "+numeroContrato+".\n\n","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("D E C L A R A C I O N E S\n\n","contrato",ComunesPDF.CENTER,1,1,0);
			pdfDoc.setCell("I. Declara el CESIONARIO, por conducto de su representante que:","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("I.1 Es una sociedad legalmente constituida de conformidad con las disposiciones legales de los Estados Unidos Mexicanos mediante Escritura P�blica No. "+escrituraPublica+", de fecha "+fechaRegistroPublico+", otorgada ante la fe del Licenciado "+  nombreLic+" Notario P�blico No. "+numNotario+" de la Ciudad de "+ciudadOrig+", cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de "+registroPublico+" en el folio mercantil No. "+
			folioMercantil+", con fecha "+fechaRegistroPublico+".","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			
			//Representante(s)
			//pdfDoc.setCell("Representante IF 1 ");
//			pdfDoc.setCell("I.2 Su(s) representante(s) cuenta(n) con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita(n) con la(s) Escritura(s) P�blica(s) No. "+escrituraPublicaCesRep+", de fecha "+fechaRegistroPublicoCesRep+", otorgada ante la fe del Licenciado "+nombreLicCesRep+" , Notario P�blico No. "+numNotarioCesRep+" de la Ciudad de "+ciudadOrigCesRep+
			pdfDoc.setCell("I.2 Cuenta con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita(n) con la(s) Escritura(s) P�blica(s) No. "+escrituraPublicaCesRep+", de fecha "+fechaRegistroPublicoCesRep+", otorgada ante la fe del Licenciado "+nombreLicCesRep+" , Notario P�blico No. "+numNotarioCesRep+" de la Ciudad de "+ciudadOrigCesRep+
			", que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			
			if(banderaRep.equals("S")){
				//pdfDoc.setCell("Representante IF 2");
				pdfDoc.setCell("I.2 Su(s) representante(s) cuenta(n) con los poderes y facultades suficientes para suscribir el presente instrumento, lo cual acredita(n) con la Escritura(s) P�blica(s) No. "+escrituraPubRep2+", de fecha "+fechaEsctituraRep2+", otorgada ante la fe del Licenciado "+nombreLicRep2+" , Notario P�blico No. "+numNotarioRep2+" de la Ciudad de "+ciudadNotarioRep2+", que a la fecha de firma del presente instrumento no le han sido revocados,"+
				" limitados, ni modificados en forma alguna.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			}

			pdfDoc.setCell("II. Declara la CEDENTE, por conducto de su(s) representante(s) que:","contrato",ComunesPDF.JUSTIFIED,1,1,0);

			int y = 0;
			if(!claveGrupo.equals("")  && claveGrupo !=null){
				if(lstEmpresasXGrupo.size()>0){
					for(int x=0; x<lstEmpresasXGrupo.size(); x++ ){
						y++;
						cvePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("ic_pyme");
						nombrePymeGrupo = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("cg_razon_social");
						representantes = (String)((HashMap)lstEmpresasXGrupo.get(x)).get("representantes");

						HashMap datosPyme =  getDatosConstitutivosPyme(clave_solicitud, cvePymeGrupo );

						if(datosPyme.size()>0){
							escrituraPublicaCed = datosPyme.get("ESCRITURA_PUBLICA").toString();
							fechaRegistroPublicoCed = datosPyme.get("DF_ESCRITURA_PUBLICA").toString();
							nombreLicCed  = datosPyme.get("NOMBRE_LICENCIADO").toString();
							numNotarioCed  = datosPyme.get("NUM_NOTARIO_PUBLICO").toString();
							ciudadOrigCed  = datosPyme.get("ORIGEN_NOTARIO").toString();
							registroPubCed  = datosPyme.get("REG_PUB_COMERCIO").toString();
							folioMercantilCed  = datosPyme.get("NUM_FOLIO_MERCANTIL").toString();
						}

						pdfDoc.setCell("II."+y+"  "+nombrePymeGrupo +" Es una sociedad legalmente constituida de conformidad con las disposiciones legales de los Estados Unidos Mexicanos mediante Escritura P�blica No. "+escrituraPublicaCed+", de fecha "+fechaRegistroPublicoCed+", otorgada ante la fe del Licenciado "+nombreLicCed+", Notario P�blico No. "+numNotarioCed+" de la Ciudad de "+ciudadOrigCed+", cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de "+
						registroPubCed+" en el folio mercantil No. "+folioMercantilCed+", con fecha "+ fechaRegistroPublicoCed+".","contrato",ComunesPDF.JUSTIFIED,1,1,0);

						List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(cvePymeGrupo, "P");
						for (int i = 0; i < usuariosPorPerfil.size(); i++) {
							//y++;
							String  loginUsrPyme = (String)usuariosPorPerfil.get(i);
							Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
							String  nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();

							HashMap datosRep= getRepresentantesCEDENTE( clave_solicitud, cvePymeGrupo, loginUsrPyme );
							if(datosRep.size()>0){
								escrituraPublicaCedRep= datosRep.get("ESCRITURA_PUBLICA").toString();
								fechaRegistroPublicoCedRep =datosRep.get("DF_PUBLICA").toString();
								nombreLicCedRep = datosRep.get("NOMBRE_LICENCIADO").toString();
								numNotarioCedRep =datosRep.get("NUM_NOTARIO_PUBLICO").toString();
								ciudadOrigCedRep =datosRep.get("ORIGEN_NOTARIO").toString();
								y++;
//								pdfDoc.setCell("II."+y+ " Su representante " +nombresRepresentantesPyme + " cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato, lo cual acredita con la Escritura P�blica No. "+escrituraPublicaCedRep+", de fecha "+fechaRegistroPublicoCedRep+", otorgada ante la fe del Licenciado "+nombreLicCedRep+", Notario P�blico No. "+numNotarioCedRep+" de la Ciudad de "+ciudadOrigCedRep+
								pdfDoc.setCell("II."+y+ " " +nombresRepresentantesPyme + " cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato, lo cual acredita con la Escritura P�blica No. "+escrituraPublicaCedRep+", de fecha "+fechaRegistroPublicoCedRep+", otorgada ante la fe del Licenciado "+nombreLicCedRep+", Notario P�blico No. "+numNotarioCedRep+" de la Ciudad de "+ciudadOrigCedRep+
								", que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
							}
						}
					}
				}
			} else{
				icPyme= (String) mapaDatosSolicitud.get("clave_pyme");
				//DATOS DE LA PYME
				y++;
				HashMap datosPyme =  getDatosConstitutivosPyme(clave_solicitud, icPyme );
				log.debug(datosPyme.size() +" === "+datosPyme);
				if(datosPyme.size()>0) {
					escrituraPublicaCed = datosPyme.get("ESCRITURA_PUBLICA").toString();
					fechaRegistroPublicoCed = datosPyme.get("DF_ESCRITURA_PUBLICA").toString();
					nombreLicCed  = datosPyme.get("NOMBRE_LICENCIADO").toString();
					numNotarioCed  = datosPyme.get("NUM_NOTARIO_PUBLICO").toString();
					ciudadOrigCed  = datosPyme.get("ORIGEN_NOTARIO").toString();
					registroPubCed  = datosPyme.get("REG_PUB_COMERCIO").toString();
					folioMercantilCed  = datosPyme.get("NUM_FOLIO_MERCANTIL").toString();
				}
				pdfDoc.setCell("II."+y+"  "+cedente +" Es una sociedad legalmente constituida de conformidad con las disposiciones legales de los Estados Unidos Mexicanos mediante Escritura P�blica No. "+escrituraPublicaCed+", de fecha "+fechaRegistroPublicoCed+", otorgada ante la fe del Licenciado "+nombreLicCed+", Notario P�blico No. "+numNotarioCed+" de la Ciudad de "+ciudadOrigCed+", cuyo primer testimonio qued� debidamente inscrito en el Registro P�blico de Comercio de "+
				registroPubCed+" en el folio mercantil No. "+folioMercantilCed+", con fecha "+ fechaRegistroPublicoCed+".","contrato",ComunesPDF.JUSTIFIED,1,1,0);

					// DATOS DE SUS REPRESENTANTES

				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(icPyme, "P");
				//System.out.println("->" + usuariosPorPerfil.toString());
				//System.out.println("-->" + usuariosPorPerfil.size());
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {
					//y++;
					String loginUsrPyme = (String)usuariosPorPerfil.get(i);
					Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
					String nombresRepresentantesPyme = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
					HashMap datosRep = getRepresentantesCEDENTE( clave_solicitud, icPyme, loginUsrPyme );
					log.debug(datosPyme.size() +" === "+datosRep);
					if(datosRep.size()>0){
						escrituraPublicaCedRep = datosRep.get("ESCRITURA_PUBLICA").toString();
						fechaRegistroPublicoCedRep = datosRep.get("DF_PUBLICA").toString();
						nombreLicCedRep = datosRep.get("NOMBRE_LICENCIADO").toString();
						numNotarioCedRep = datosRep.get("NUM_NOTARIO_PUBLICO").toString();
						ciudadOrigCedRep = datosRep.get("ORIGEN_NOTARIO").toString();
						y++;
//						pdfDoc.setCell("II."+y+ " Su representante " +nombresRepresentantesPyme + " cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato, lo cual acredita con la Escritura P�blica No. "+escrituraPublicaCedRep+", de fecha "+fechaRegistroPublicoCedRep+", otorgada ante la fe del Licenciado "+nombreLicCedRep+", Notario P�blico No. " +
						pdfDoc.setCell("II."+y+ " " +nombresRepresentantesPyme + " cuenta con los poderes y facultades para actos de dominio suficientes para suscribir el presente contrato, lo cual acredita con la Escritura P�blica No. "+escrituraPublicaCedRep+", de fecha "+fechaRegistroPublicoCedRep+", otorgada ante la fe del Licenciado "+nombreLicCedRep+", Notario P�blico No. " +
						numNotarioCedRep+" de la Ciudad de "+ciudadOrigCedRep+", que a la fecha de firma del presente instrumento no le han sido revocados, limitados, ni modificados en forma alguna.","contrato",ComunesPDF.JUSTIFIED,1,1,0);

					}

				}

			}
			y++;
			pdfDoc.setCell("II."+y+" Tiene celebrado con el CESIONARIO un Contrato de Cesi�n Electr�nica de Derechos.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("III.	Declaran las partes por conducto de sus representantes que:","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("III.1 De conformidad con lo antes se�alado, el CESIONARIO presentar� a la Entidad junto con la notificaci�n de la Extinci�n a la Cesi�n Electr�nica de Derechos de Cobro, el presente Convenio de Extinci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro respecto del Contrato No. "+numeroContrato+".","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("III.2 Se reconocen la personalidad con la que se ostentan sus representantes.","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("Expuesto lo anterior, las partes otorgan y se someten a lo que se consigna en las siguientes:\n\n","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("C L � U S U L A S\n\n","contrato",ComunesPDF.CENTER,1,1,0);
			pdfDoc.setCell("PRIMERA. El CESIONARIO y el CEDENTE, con la firma de este convenio, expresamente acuerdan en llevar a cabo la extinci�n de la Cesi�n Electr�nica de los Derechos de Cobro que le fueron transmitidos al CESIONARIO mediante la celebraci�n del Contrato de Cesi�n Electr�nica de Derechos de Cobro respecto del Contrato No. "+numeroContrato+"  relativo a "+objetoContrato+
			" , y que a partir de la fecha de notificaci�n a la Entidad quedar� sin efecto legal alguno.\n\n","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("SEGUNDA. Por virtud de la extinci�n de la Cesi�n Electr�nica de los Derechos de Cobro respecto del Contrato No. "+numeroContrato+"  que por el presente convenio se formaliza, el CEDENTE  a trav�s del sistema NAFINET, se obliga a notific�rselo a la Entidad, en un plazo no mayor a 10 (diez) d�as naturales, contados a partir de la fecha de firma del presente convenio, con objeto de que a partir de la fecha de notificaci�n, se efect�en los pagos que se generen del Contrato "
			+"No.  "+numeroContrato+" directamente al CEDENTE.\n\n","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("TERCERA. Las partes reconocen que no existe dolo, error, o mala fe por parte de quienes celebran el presente convenio y que cada uno de ellos se ha obligado en la manera y t�rminos que aparecen en este documento, sin que la validez del mismo dependa de la observancia de formalidades o requisitos determinados.\n\n","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			pdfDoc.setCell("CUARTA. Para la interpretaci�n, ejecuci�n y cumplimiento de los pactos contenidos en este convenio, las partes se someten a las leyes y tribunales competentes de la Ciudad de M�xico, D.F., por lo que renuncian expresamente a cualquier otro fuero que pudiera corresponderles, en virtud de sus domicilios presentes o futuros.\n\n","contrato",ComunesPDF.JUSTIFIED,1,1,0);
			//pdfDoc.setCell("Enteradas del contenido y alcance del presente contrato, las partes lo firman de conformidad, en la Ciudad de M�xico, Distrito Federal, a los "+fechaFinalContrato+".","formasrep",ComunesPDF.JUSTIFIED,1,1,0);
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
			
			
			
			} 	catch(Exception e) {
						
						log.error("generarContrato(Exception) " + e);
						e.printStackTrace();
					throw new AppException("generarContrato(Exception) ", e);
				}
				finally
				{
				
				}
			log.debug("generarContratoExtincion(S) ");
		return nombreArchivo;
	}
	
			/**
	 * Obtiene de la base de datos el archivo del Consentimiento
	 * y lo prepara para su descarga desde la pantallas.
	 * @param clave_solicitud Es la clave de la solicitud de la que se obtiene el archivo del contrato de cesi�n.
	 * @param strDirectorioTemp Es el directorio donde se genera de manera temporal el archivo.
	 * @return file El archivo del contrato de cesi�n obtenido de la base de datos, este archivo est� en formato PDF.
	 * @throws netropology.utilerias.AppException Al encontrar un error inesperado
	 */
	public String descargaContratoExtincionNuevo(String clave_solicitud, String strDirectorioTemp) throws AppException{
		log.info("descargaContratoExtincionNuevo(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "";
		String nombreArchivo = "";
		
		try{
			con.conexionDB();

			strSQL.append(" SELECT bi_contrato_extincion");
			strSQL.append(" FROM cder_extincion_contrato");
			strSQL.append(" WHERE ic_solicitud = ?");
			
			varBind.add(new Long(clave_solicitud));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){
				nombreArchivo = Comunes.cadenaAleatoria(8) + ".pdf";
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob("bi_contrato_extincion");
				InputStream inStream = blob.getBinaryStream();
				int size = (int)blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1){
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("descargaContratoExtincionNuevo(S)");
		}
		//return pathname;
		return nombreArchivo;
	}
	
	/**
	 * Metodo que nos dice si ya se guardo el contrato de extinci�n
	 * 
	 * */
		public String getBanderaContratoExtincion(String clave_solicitud){  
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      String       bandera  =  "";
		List lVarBind		= new ArrayList();
		log.info("getBanderaContrato (E)");
		 try{
         con.conexionDB();

         String strQry = "SELECT cs_bandera_contrato_ext  "  +
                        "  FROM cder_extincion_contrato    "  +
                        " WHERE ic_solicitud = ?  ";
                       
			lVarBind		= new ArrayList();
         lVarBind.add(new Long(clave_solicitud)); 
			ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next()){
				bandera = rs.getString("CS_BANDERA_CONTRATO_EXT");
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  clave_solicitud (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getBanderaContrato (S)");
      return bandera;
    }
	 
	 /**
	  * Metodo que inserta los poderes del IF del contrato de Cesion y del contrato de Extincion  (cesionario)
	  * */
	 private boolean insertarPoderesIF(AccesoDB con , String poderesCesion,String poderesExtincion,String strDirectorioTemp,String claveIF){  
      
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      boolean       bandera  =  true;
		
		List lVarBind		= new ArrayList();
		log.info("insertarPoderesIF(E)");
		StringBuffer qrySQLArchivo= new StringBuffer("");
		
		 try{
		 if(!poderesCesion.equals("")){
			File archivoPDF = new File(strDirectorioTemp+poderesCesion);
			FileInputStream fileinputstream = new FileInputStream(archivoPDF);
			qrySQLArchivo.append("UPDATE comcat_cesionario SET bi_poderes_cesion = ? WHERE ic_if = ?");
			ps = con.queryPrecompilado(qrySQLArchivo.toString());
			ps.setBinaryStream(1, fileinputstream, (int)archivoPDF.length());
			ps.setInt(2, Integer.parseInt(claveIF));
			ps.executeUpdate();
			ps.close();
			fileinputstream.close();
		 }
		qrySQLArchivo = new StringBuffer("");
		if(!poderesExtincion.equals("")){
			File archivoPDF = new File(strDirectorioTemp+poderesExtincion);
			FileInputStream fileinputstream = new FileInputStream(archivoPDF);
			qrySQLArchivo.append("UPDATE comcat_cesionario SET bi_poderes_extincion = ? WHERE ic_if = ? ");
			ps = con.queryPrecompilado(qrySQLArchivo.toString());
			ps.setBinaryStream(1, fileinputstream, (int)archivoPDF.length());
			ps.setInt(2, Integer.parseInt(claveIF));
			ps.executeUpdate();
			ps.close();
			fileinputstream.close();
		 }


      }catch(Exception e){
         e.printStackTrace();
			bandera=false;
			log.error("Error en insertarPoderesIF (S)" +e);
         throw new RuntimeException(e);
      }finally{
			
      }
      log.info("insertarPoderesIF (S)");
      return bandera;
    }
	 
	 /**
	  * Metodo que modifica los datos del representante IF 2
	  * */
	 public void modificarRepresentanteIF2(String claveIF,String tipo_rep,String representante,String escritura_pub,
		String fecha_escritura,String txt_lic_rep,String num_notario,String ciudad_origen,String claveIf2){
									  
		log.info("modificarRepresentanteIF2 :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			strSQL = " update comcat_cesionario "+
						" set CS_BANDERA_REP2 = 'S', "+
						" CG_TIPO_REPRESENTANTE_REP2 = ?, "+
						" CG_NOM_REPRESENTANTE_REP2 = ?  , "+
						" cg_escritura_publica_rep2 = ?, " +
						" df_escritura_publica_rep2 = to_date(?,'dd/mm/yyyy'), "+ 
						" cg_nombre_licenciado_rep2 = ?, "+
						" in_num_notario_publico_rep2 = ? , "+
						" cg_origen_notario_rep2 = ?,  "+
						"  ic_if_rep2 = ? "+ 
						" where ic_if = ?";
			
			
			List params = new ArrayList();
			params.add(tipo_rep);
			params.add(representante);
			params.add(escritura_pub);
			params.add(fecha_escritura);
			params.add(txt_lic_rep);
			params.add(num_notario);
			params.add(ciudad_origen);
			params.add(claveIf2);
			params.add(claveIF);
			
			log.debug("strSQL:::::::: :: "+strSQL);	
			log.debug("params:::::::: :: "+params);	
			
			con.ejecutaUpdateDB(strSQL.toString(), params);
			
			
			log.debug("strSQL:::::::: :: "+strSQL);			
				
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error al guardar el Cedente", e);
		}finally{
			log.info("modificarRepresentanteIF2 :::: (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}	
		}								  
	}
	
	/**
	 * 
	 * Metodo que sirve para solicitar una prorroga en el contrato de Cesion
	 * 
	 * */
	
	public void solicitarProrroga(String claveSolicitud, String  contador){ 
									  
		log.info("solicitarProrroga :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			strSQL = " update cder_solicitud set cs_estatus_prorroga = 'P', "+
						" IN_CONTADOR_PRORROGA = (select in_contador_prorroga+1 from cder_solicitud where ic_solicitud = ?)  ";									
					strSQL += 	" where ic_solicitud = ? ";
			
			List params = new ArrayList();
			params.add(claveSolicitud);
			params.add(claveSolicitud);
					
			ps = con.queryPrecompilado(strSQL.toString(), params ); 
			ps.executeUpdate();
			ps.close();
			
			log.debug("strSQL:::::::: :: "+strSQL);			
				
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException(" Error al solicitar Prorroga", e);
		}finally{
			log.info("solicitarProrroga :::: (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}	
		}								  
	}
	
	/**
	 * Una vez solicitada la prorroga se tiene que aceptar o rechazar con este metodo
	 * 
	 * */
	public void aceptarRechazarProrroga(String claveSolicitud,String estatusProrroga){
									  
		log.info("aceptarRechazarProrroga :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		List params = new ArrayList();  	
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			String condicion="", fecha_prorroga ="";
			int contador =0;
			
			
			String  strSQL1=" select IN_CONTADOR_PRORROGA  , DF_FECHA_PRORROGA     from cder_solicitud sol "+
			" where sol.IC_SOLICITUD =  "+claveSolicitud;  
		
			log.debug("strSQL1:::::::: :: "+strSQL1);	
			ps = con.queryPrecompilado(strSQL1.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				contador = rs.getInt(1);					
				fecha_prorroga = rs.getString("DF_FECHA_PRORROGA")==null?"":rs.getString("DF_FECHA_PRORROGA");
        
			}
			rs.close();
			ps.close();
			
			
			log.debug("contador:::::::: :: "+contador);	
			log.debug("fecha_prorroga:::::::: :: "+fecha_prorroga);	 		
			
			strSQL = " update cder_solicitud set cs_estatus_prorroga = ? "; 
			if(estatusProrroga.equals("A")) {
				if(contador==1) {
					strSQL += " , DF_FECHA_PRORROGA  =   DIAS_LABORABLES(DIAS_LABORABLES(DF_FECHA_ACEPTACION,15),15)  ";				
				}else if(contador>1) {	
					if(fecha_prorroga.equals("") )  {
						strSQL += " ,  DF_FECHA_PRORROGA  =   DIAS_LABORABLES(DIAS_LABORABLES(DF_FECHA_ACEPTACION,15),15) "; 							
					}else  {
						strSQL += " ,  DF_FECHA_PRORROGA  =   DIAS_LABORABLES(DF_FECHA_PRORROGA,15)  ";	
					}
				}				
			}
			
			strSQL += " where ic_solicitud = ? ";
			
			log.debug("estatusProrroga:::::::: :: "+estatusProrroga);	
			log.debug("claveSolicitud:::::::: :: "+claveSolicitud);  	
			log.debug("strSQL:::::::: :: "+strSQL);  	
			params = new ArrayList();  			
			params.add(estatusProrroga);
			params.add(claveSolicitud);  
			
			log.debug("params:::::::: :: "+params);  	
			ps = con.queryPrecompilado(strSQL,params ); 
			ps.executeUpdate();
			ps.close();
			
			log.debug("strSQL:::::::: :: "+strSQL);			
				
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException(" Error al solicitar Prorroga", e);
		}finally{
			log.info("aceptarRechazarProrroga :::: (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}	
		}								  
	}
	
	/**
	 * Se agrega nuevo perfil IF operativo el cual puede rechazar las solicitudes
	 * */
	public void rechazarIfOperativo(String claveSolicitud){
									  
		log.info("autorizarRechazarIfOperativo :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			strSQL = " update cder_solicitud set IC_ESTATUS_SOLIC = ? "+

						" where ic_solicitud = ? ";
			
			List params = new ArrayList();
			params.add("10");//Se cambia a estatus 10 rechazado IF
			params.add(claveSolicitud);
			
			con.ejecutaUpdateDB(strSQL.toString(), params);
			
			log.debug("strSQL:::::::: :: "+strSQL);			
				
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException(" Error al actualiazar el estatus de la solicitud ", e);
		}finally{
			log.info("autorizarRechazarIfOperativo :::: (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}	
		}								  
	}
	
	/**
	 * Se agrega nuevo perfil IF operativo el cual puede retornar las solicitudes
	 * */
	public void retornarSolicOperativo(String claveSolicitud, String causasRetorno,String estatusRetorno){
									  
		log.info("retornarSolicOperativo :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			strSQL = " update cder_solicitud set IC_ESTATUS_SOLIC = ?, "+
						" cg_causas_retorno = ? ,"+
						" cc_acuse_firma_rep1 = '' ,"+
						" cg_firma_rep1 ='',  "+
						" cc_acuse_firma_rep2 = '' ,"+						
						" cg_firma_rep2 ='' ,"+
						" CS_BANDERA_CONTRATO = 'N' "+						
						" where ic_solicitud = ? ";
			
			List params = new ArrayList();
			params.add(estatusRetorno);
			params.add(causasRetorno);
			params.add(claveSolicitud);
			
			con.ejecutaUpdateDB(strSQL.toString(), params);
			
			log.debug("strSQL:::::::: :: "+strSQL);			
				
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException(" Error al actualiazar el estatus de la solicitud ", e);
		}finally{
			log.info("retornarSolicOperativo :::: (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}	
		}								  
	}
	
	/**
	 * Se agrega nuevo perfil IF operativo el cual puede aceptar las solicitudes
	 * */
	 
	public void aceptarIfOperativo(String claveSolicitud,String banco_deposito,String numero_cuenta,String clabe,String monto_credito,String referencia,String fecha_vencimiento, String usuario){
									  
		log.info("aceptarIfOperativo :::: (E)");
		AccesoDB con = new AccesoDB();
		boolean commit = true;
		String strSQL =  "";
		
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			StringBuffer condicion= new StringBuffer("");
			List params = new ArrayList();
			if(!banco_deposito.equals("")){
				condicion.append(" CG_BANCO_DEPOSITO = ?,  ");
				params.add(banco_deposito);
			}
			if(!numero_cuenta.equals("")){
				condicion.append(" CG_CUENTA = ?, ");
				params.add(numero_cuenta);
			}
			if(!clabe.equals("")){
				condicion.append(" CG_CUENTA_CLABE = ?, ");
				params.add(clabe);
			}
			if(!monto_credito.equals("")){
				condicion.append(" FN_MONTO_CREDITO = ?, ");
				params.add(monto_credito);
			}
			if(!referencia.equals("")){
				condicion.append(" CG_REFERENCIA = ?, ");
				params.add(referencia);
			}
			if(!fecha_vencimiento.equals("")){
				condicion.append(" df_vencimiento_credito = TO_DATE(?,'dd/mm/yyyy') , ");
				params.add(fecha_vencimiento);
			}
			//F24-2015 INI: Se agrega el parametro usuario
			if(!usuario.equals("")){
				condicion.append(" CG_USUARIO_AUT_IF_OPER = ?, ");
				params.add(usuario);
			}
			//F24-2015 FIN
			strSQL = " update cder_solicitud set "+condicion.toString()+" CS_AUTORIZO_IF_OPERATIVO = ?, DF_AUTORIZO_IF_OPERATIVO = SYSDATE "+
				
						" where ic_solicitud = ? ";
			
			
			params.add("S");
			params.add(claveSolicitud);
			
			con.ejecutaUpdateDB(strSQL.toString(), params);
			
			log.debug("strSQL:::::::: :: "+strSQL);			
				
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException(" Error al actualiazar el estatus de la solicitud ", e);
		}finally{
			log.info("aceptarIfOperativo :::: (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}	
		}								  
	}
	
			/**
	 * Obtiene el archivo de poderes IF para el contrato de Cesion
	 * y lo prepara para su descarga desde la pantallas.
	 * @param clave_solicitud Es la clave de la solicitud de la que se obtiene el archivo del contrato de cesi�n.
	 * @param strDirectorioTemp Es el directorio donde se genera de manera temporal el archivo.
	 * @throws netropology.utilerias.AppException Al encontrar un error inesperado
	 */
	public String descargaPoderesContratoCesion(String clave_solicitud, String strDirectorioTemp) throws AppException{
		log.info("descargaPoderesContratoCesion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "";
		String nombreArchivo = "";
		
		try{
			con.conexionDB();

			strSQL.append(" select bi_poderes_cesion from cder_solicitud sol,"+
			"comcat_cesionario ces where sol.IC_IF = ces.IC_IF and sol.IC_SOLICITUD = ?");
			
			
			varBind.add(new Long(clave_solicitud));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){
				nombreArchivo = Comunes.cadenaAleatoria(8) + ".pdf";
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob("bi_poderes_cesion");
				if(blob==null){
					throw new AppException(" El Cesionario no ha subido el archivo de Poderes IF ");
				}
				InputStream inStream = blob.getBinaryStream();
				int size = (int)blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1){
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("descargaPoderesContratoCesion(S)");
		}
		//return pathname;
		return nombreArchivo;
	}
	
		/**
	 * Obtiene el archivo de poderes IF para la extincion de contrato
	 * y lo prepara para su descarga desde la pantallas.
	 * @param clave_solicitud Es la clave de la solicitud de la que se obtiene el archivo del contrato de cesi�n.
	 * @param strDirectorioTemp Es el directorio donde se genera de manera temporal el archivo.
	 * @throws netropology.utilerias.AppException Al encontrar un error inesperado
	 */
	public String descargaPoderesContratoExtincion(String clave_solicitud, String strDirectorioTemp) throws AppException{
		log.info("descargaPoderesContratoExtincion(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "";
		String nombreArchivo = "";
		
		try{
			con.conexionDB();

			strSQL.append(" select bi_poderes_extincion from cder_solicitud sol,"+
			"comcat_cesionario ces where sol.IC_IF = ces.IC_IF and sol.IC_SOLICITUD = ?");
			
			varBind.add(new Long(clave_solicitud));
			
			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind.toString());
			
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			
			if(rst.next()){
				nombreArchivo = Comunes.cadenaAleatoria(8) + ".pdf";
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob("bi_poderes_extincion");
				if(blob==null){
					throw new AppException(" El Cesionario no ha subido el archivo de Poderes de Extinci�n IF ");
				}
				InputStream inStream = blob.getBinaryStream();
				int size = (int)blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1){
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();
		}catch(Exception e){
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("descargaPoderesContratoExtincion(S)");
		}
		//return pathname;
		return nombreArchivo;
	}
	
	/**
	 * Metodo que nos dice si ya se guardo el archivo de poderes de extinci�n y del contrato de cesion
	 * 
	 * */
	public String getBanderaPoderes(String clave_solicitud,String columna)throws AppException{  
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      String       bandera  =  "";
		List lVarBind		= new ArrayList();
		log.info("getBanderaPoderes (E)");
		 try{
         con.conexionDB();

         String strQry = " select  nvl((dbms_lob.getlength("+columna+")),0) as archivo  from cder_solicitud sol, "+
			"comcat_cesionario ces where sol.IC_IF = ces.IC_IF and sol.IC_SOLICITUD = ? ";
                       
			lVarBind		= new ArrayList();
         lVarBind.add(new Long(clave_solicitud)); 
			ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next()){
				bandera = rs.getString("archivo");
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  clave_solicitud (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getBanderaPoderes (S)");
      return bandera;
    }
	
	/**
		 * Metodo que Cancelar la solicitud  
		 * Fodea XX-2015 
		 * @throws netropology.utilerias.AppException
		 * @return 
		 * @param motivosCancelacion
		 * @param clave_solicitud
		 */
		public boolean setCancelaNafin(String clave_solicitud,String motivosCancelacion)throws AppException{  
			AccesoDB          con   =  new AccesoDB();      
			PreparedStatement ps    =  null;
			String       bandera  =  "";
			List lVarBind     = new ArrayList();
			log.info("setCancelaNafin (E)");
			boolean bcommit = true;
			 
			 try{
				con.conexionDB();

				String strQry = " Update cder_solicitud  " +
				" set CG_CAUSA_CANCELACION  =  ? ,  " +
				" IC_ESTATUS_SOLIC  =  ?   "+
				"  where IC_SOLICITUD = ? ";
								  
				lVarBind    = new ArrayList();
				lVarBind.add(motivosCancelacion); 
				lVarBind.add("24"); 
				lVarBind.add(new Long(clave_solicitud)); 
				ps = con.queryPrecompilado(strQry,lVarBind );
				ps.executeUpdate();
				ps.close();

			  

			}catch(Exception e){
				e.printStackTrace();
				 bcommit= false;
				con.terminaTransaccion(bcommit);
				log.error("Error en  setCancelaNafin (S)" +e);
				throw new RuntimeException(e);
			}finally{
				bcommit= true;
				if(con.hayConexionAbierta()) {
					con.terminaTransaccion(bcommit);
					con.cierraConexionDB();
				}
			}
			log.info("setCancelaNafin (S)");
			return bcommit;
		 }
		 
		 /**
		 * Obtengo los representantes de la Pyme o del usuario logeado 
		 * @throws netropology.utilerias.AppException
		 * @return 
		 * @param clavePyme
		 * @param loginUsrPyme
		 */
		 public HashMap getRepresentantes(String loginUsrPyme, String clavePyme ) throws AppException{
		   log.info("getRepresentantes (E)");
		   HashMap representantes = new HashMap();
		   String  correos = "",nombresRepresentantesPyme ="";
		   
		   try{
		   
		      UtilUsr utilUsr = new UtilUsr();
		   
		      //Concatena todas la representantes de la Pyme
		      if(!clavePyme.equals("")) {
		      
		         List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");
		         for (int i = 0; i < usuariosPorPerfil.size(); i++) {
		            loginUsrPyme = (String)usuariosPorPerfil.get(i);
		            Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
		            
		            nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre() + ",";
		            correos +=usuario.getEmail()+ ",";  
		         }
		      }else  if(!loginUsrPyme.equals("")) {
		                  
		         Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
		         nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
		         correos +=usuario.getEmail();    
		         
		      }
		         
		      representantes.put("NOMBRE_REPRESENTANTE_LEGAL",nombresRepresentantesPyme);      
		      representantes.put("CORREO_ELECTRONICO",correos);        
		      log.info("representantes  "+representantes);
		         
		   }catch(Exception e){
		      e.printStackTrace();
		      throw new AppException("No se pudieron obtener los Representantes");
		   }finally{
		      
		      log.info("getRepresentantes (S)");
		    }
		    return representantes;
		 }
		 
		 
	/**
		 * Datos Constitutivos / CEDENTE o PyME
		 *  * para mostrarlos en el contrato de Cesi�n 
		 * @throws netropology.utilerias.AppException
		 * @return 
		 * @param clavePyme
		 * @param ic_solicitud
		 */
		  private HashMap getDatosConstitutivosPyme(String ic_solicitud, String clavePyme ) throws AppException{
			log.info("getDatosConstitutivosPyme (E)");
			HashMap datosCons = new HashMap();
			
			AccesoDB  con       =   new AccesoDB();
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			PreparedStatement ps = null;
			ResultSet rs = null;    
			
			try{
			
				con.conexionDB();
				
				strSQL.append(" SELECT CG_FIGURA_JURIDICA ,  CG_ESCRITURA_PUBLICA  ,  TO_CHAR(DF_ESCRITURA_PUBLICA,'DD/MM/YYYY')  as DF_ESCRITURA_PUBLICA  ,  " +
								  " CG_NOMBRE_LICENCIADO  , IN_NUM_NOTARIO_PUBLICO , CG_ORIGEN_NOTARIO ,  IN_NUM_FOLIO_MERCANTIL ,  "+
							  " CG_REG_PUB_COMERCIO ,  CG_DOMICILIO_LEGAL, "+
							  " CASE WHEN CG_TIPO_CEDENTE = 'C' THEN 'Contratista' " +
							  " WHEN CG_TIPO_CEDENTE = 'P' THEN 'No se' " +
							  " ELSE '' END AS TIPO_CEDENTE " +
								  " FROM CDER_SOLIC_PYME_NOTARIALES  "+
								  " WHERE IC_SOLICITUD  =  ?  "+
								  " AND  IC_PYME  =   ? " );
				
				varBind.add(new Integer(ic_solicitud));
				varBind.add(new Integer(clavePyme));
				
			log.debug("strSQL === "+strSQL);
			log.debug("varBind ===  "+varBind);

				ps = con.queryPrecompilado(strSQL.toString(), varBind);        
				rs = ps.executeQuery();
					
				if(rs.next()){
					datosCons.put("FIGURA_JURIDICA",rs.getString("CG_FIGURA_JURIDICA")==null?"":rs.getString("CG_FIGURA_JURIDICA"));     
					datosCons.put("ESCRITURA_PUBLICA",rs.getString("CG_ESCRITURA_PUBLICA")==null?"":rs.getString("CG_ESCRITURA_PUBLICA"));     
					datosCons.put("DF_ESCRITURA_PUBLICA",rs.getString("DF_ESCRITURA_PUBLICA")==null?"":rs.getString("DF_ESCRITURA_PUBLICA"));  
					datosCons.put("NOMBRE_LICENCIADO",rs.getString("CG_NOMBRE_LICENCIADO")==null?"":rs.getString("CG_NOMBRE_LICENCIADO"));  
					datosCons.put("NUM_NOTARIO_PUBLICO",rs.getString("IN_NUM_NOTARIO_PUBLICO")==null?"":rs.getString("IN_NUM_NOTARIO_PUBLICO"));  
					datosCons.put("ORIGEN_NOTARIO",rs.getString("CG_ORIGEN_NOTARIO")==null?"":rs.getString("CG_ORIGEN_NOTARIO"));  
					datosCons.put("NUM_FOLIO_MERCANTIL",rs.getString("IN_NUM_FOLIO_MERCANTIL")==null?"":rs.getString("IN_NUM_FOLIO_MERCANTIL"));  
					datosCons.put("REG_PUB_COMERCIO",rs.getString("CG_REG_PUB_COMERCIO")==null?"":rs.getString("CG_REG_PUB_COMERCIO"));  
					datosCons.put("DOMICILIO_LEGAL",rs.getString("CG_DOMICILIO_LEGAL")==null?"":rs.getString("CG_DOMICILIO_LEGAL"));  
				datosCons.put("TIPO_CEDENTE",rs.getString("TIPO_CEDENTE")==null?"":rs.getString("TIPO_CEDENTE"));
				}
				
				rs.close();
				ps.close();
					
			}catch(Exception e){
				e.printStackTrace();
				throw new AppException("No se pudieron obtener los Representantes");
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
				log.info("getDatosConstitutivosPyme (S)");
			 }
			 return datosCons;
		 }
		
		/**
		 * Obtengo los datos de los representantes  de la Pyme
		 * para mostrarlos en el contrato de Cesi�n 
		 * @throws netropology.utilerias.AppException
		 * @return 
		 * @param loginUsuario
		 * @param clavePyme
		 * @param ic_solicitud
		 */
			public HashMap getRepresentantesCEDENTE(String ic_solicitud, String clavePyme, String loginUsuario ) throws AppException{
			log.info("getRepresentantesCEDENTE (E)");
			HashMap datosCons = new HashMap();
			
			AccesoDB  con       =   new AccesoDB();
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			PreparedStatement ps = null;
			ResultSet rs = null;    
			
			try{
			
				con.conexionDB();
				
			   strSQL.append(" SELECT  CG_ESCRITURA_PUBLICA , TO_CHAR(DF_ESCRITURA_PUBLICA,'DD/MM/YYYY') as DF_ESCRITURA_PUBLICA    , "+
			                           " IN_NUM_NOTARIO_PUBLICO  , CG_ORIGEN_NOTARIO, CG_NOMBRE_LICENCIADO  ,"+
			                           " CG_FIRMA_REPRESENTANTE, DF_FIRMA_REPRESENTANTE   "+
			                                             
			                           " from CDER_SOLIC_X_REPRESENTANTE   "+                      
			                           " where ic_solicitud = ? "+
			                           " and  ic_pyme = ? "+
			                           " and  IC_USUARIO  = ? "   );
			   
			   varBind.add(new Integer(ic_solicitud)); 
			   varBind.add(new Integer(clavePyme));
			   varBind.add(new Integer(loginUsuario));
			   
			   log.debug("strSQL.toString()  "+strSQL.toString() );
			   log.debug("varBind  "+varBind ); 
			   
				ps = con.queryPrecompilado(strSQL.toString(), varBind);        
				rs = ps.executeQuery();
					
				if(rs.next()){
				   datosCons.put("ESCRITURA_PUBLICA",rs.getString("CG_ESCRITURA_PUBLICA")==null?"":rs.getString("CG_ESCRITURA_PUBLICA"));     
				   datosCons.put("DF_PUBLICA",rs.getString("DF_ESCRITURA_PUBLICA")==null?"":rs.getString("DF_ESCRITURA_PUBLICA"));      
				   datosCons.put("NUM_NOTARIO_PUBLICO",rs.getString("IN_NUM_NOTARIO_PUBLICO")==null?"":rs.getString("IN_NUM_NOTARIO_PUBLICO"));  
					datosCons.put("ORIGEN_NOTARIO",rs.getString("CG_ORIGEN_NOTARIO")==null?"":rs.getString("CG_ORIGEN_NOTARIO"));  
				   datosCons.put("NOMBRE_LICENCIADO",rs.getString("CG_NOMBRE_LICENCIADO")==null?"":rs.getString("CG_NOMBRE_LICENCIADO"));
				   datosCons.put("CG_FIRMA_REPRESENTANTE",rs.getString("CG_FIRMA_REPRESENTANTE")==null?"":rs.getString("CG_FIRMA_REPRESENTANTE"));
				   datosCons.put("DF_FIRMA_REPRESENTANTE",rs.getString("DF_FIRMA_REPRESENTANTE")==null?"":rs.getString("DF_FIRMA_REPRESENTANTE"));

	}
				rs.close();
				ps.close();
					 
			}catch(Exception e){
				e.printStackTrace();
				throw new AppException("No se pudieron obtener los Representantes");
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
				log.info("getRepresentantesCEDENTE (S)");
			 }
			 return datosCons;
		 }

	/**
	 * Fodea 24-2015
	 * Este m�todo realiza la consulta a la tabla CDER_ACUSE para obteber el usuario a partir del acuse.
	 * Una vez que se obtiene el usuario obtiene los datos de la clase UtilUsr.
	 * @throws netropology.utilerias.AppException
	 * @return HashMap con el nombre y email del representante legal
	 * @param acuse
	 */
	 public HashMap getRepresentantes(String acuse) throws AppException{

		log.info("getRepresentantes (E)");
		HashMap representantes = new HashMap();
		AccesoDB con           = new AccesoDB();
		PreparedStatement ps   = null;
		ResultSet rs           = null;
		String query           = "";
		String cgUsuario       = "";
		String nombre          = "";
		String email           = "";

		try{

			con.conexionDB();
			query = "SELECT CG_USUARIO FROM CDER_ACUSE WHERE CC_ACUSE = ? ";
			log.debug("QUERY: " + query);
			log.debug("ACUSE: " + acuse);
			ps = con.queryPrecompilado(query);
			ps.clearParameters();
			ps.setString(1, acuse);
			rs = ps.executeQuery();
			if(rs.next()){
				cgUsuario = rs.getString("CG_USUARIO");
			}

			if(!cgUsuario.equals("")){
				UtilUsr utilUsr = new UtilUsr();
				Usuario usuario = utilUsr.getUsuario(cgUsuario);
				nombre = " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
				email =usuario.getEmail();
				representantes.put("NOMBRE_REPRESENTANTE_LEGAL",nombre);
				representantes.put("CORREO_ELECTRONICO",email);
			}

			log.debug("Representantes: " + representantes);

		} catch(Exception e){
			e.printStackTrace();
			throw new AppException("No se pudieron obtener los Representantes");
		} finally{
			try{
				if(rs!=null)
					rs.close();
				if(ps!=null)
					ps.close();
			} catch(Exception e){}
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getRepresentantes (S)");
		 }
		 return representantes;
	 }

	/**
	 * Fodea 24-2015
	 * Este metodo obtiene los datos necesarios para el visor Poderes en las pantallas "Extinci�n de Contrato"
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param icPyme
	 * @param icSolicitud
	 */
	 public HashMap getDatosPoderesPyme(String icSolicitud, String icPyme) throws AppException{

		log.info("getDatosPoderesPyme (E)");
		HashMap datosPyme    = new HashMap();
		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs         = null;
		StringBuffer query   = new StringBuffer();

		try{

			con.conexionDB();
			query.append("SELECT S.IC_SOLICITUD, "              );
			query.append("S.IC_PYME, "                          );
			query.append("S.IC_GRUPO_CESION, "                  );
			query.append("S.CG_NO_CONTRATO, "                   );
			query.append("P.CG_RFC "                            );
			query.append("FROM CDER_SOLICITUD S, COMCAT_PYME P ");
			query.append("WHERE S.IC_PYME = P.IC_PYME "         );
			query.append("AND S.IC_SOLICITUD = ? "              );
			query.append("AND S.IC_PYME = ?"                    );

			log.debug("QUERY: " + query.toString()  );
			log.debug("IC_SOLICITUD: " + icSolicitud);
			log.debug("IC_PYME: " + icPyme          );

			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setInt(1, Integer.parseInt(icSolicitud));
			ps.setInt(2, Integer.parseInt(icPyme));
			rs = ps.executeQuery();
			if(rs.next()){
				datosPyme.put("SOLICITUD",    "" + rs.getInt("IC_SOLICITUD")     );
				datosPyme.put("PYME",         "" + rs.getInt("IC_PYME")          );
				datosPyme.put("GRUPO_CESION", "" + rs.getInt("IC_GRUPO_CESION")  );
				datosPyme.put("NO_CONTRATO",  "" + rs.getString("CG_NO_CONTRATO"));
				datosPyme.put("RFC",          "" + rs.getString("CG_RFC")        );
			}

		} catch(Exception e){
			e.printStackTrace();
			throw new AppException("getDatosPoderesPyme.Exception");
		} finally{
			try{
				if(rs!=null)
					rs.close();
				if(ps!=null)
					ps.close();
			} catch(Exception e){}
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getDatosPoderesPyme (S)");
		 }
		 return datosPyme;
	 }

	/**
	 * Este query lo utilizo en la pantalla ADMIN IF Cesi�n de Derechos/Capturas/Extinci�n de Contrato.
	 * Indica si para el usuario logueado existe uno o dos representantes.
	 * @throws netropology.utilerias.AppException
	 * @return 0: si no existe el representantes. 
	 * > 0: Existe el representante 2
	 * @param loginUsuario
	 */
	public String getIfRepresentante2(String icIf)  throws AppException{

		log.info("getIfRepresentante2 (E)");
		String existeRepresentante = "";
		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs         = null;
		StringBuffer query   = new StringBuffer();

		try{

			con.conexionDB();
			query.append("SELECT CS_BANDERA_REP2 AS EXISTE ");
			query.append("FROM COMCAT_CESIONARIO "          );
			query.append("WHERE IC_IF = ?"                   );

			log.debug("QUERY: " + query.toString()  );
			log.debug("IC_IF: " + icIf);

			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setInt(1, Integer.parseInt(icIf));
			rs = ps.executeQuery();
			if(rs.next()){
				existeRepresentante = rs.getString("EXISTE");
			}

		} catch(Exception e){
			e.printStackTrace();
			throw new AppException("getIfRepresentante2.Exception");
		} finally{
			try{
				if(rs!=null)
					rs.close();
				if(ps!=null)
					ps.close();
			} catch(Exception e){}
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getIfRepresentante2 (S)");
		 }
		 return existeRepresentante;
	 }
	 
	public Map getParametrosEpo(String cveEpo) throws AppException{
			log.info("getParametrosEpo (E)");
			AccesoDB con         = new AccesoDB();
			PreparedStatement ps = null;
			ResultSet rs         = null;
			String strSQL   = "";
			Map mpParams = new HashMap();
			try{
				con.conexionDB();
				
				strSQL = "SELECT   pe.cc_parametro_epo, cpe.cg_valor cg_valor "+
							"    FROM comcat_parametro_epo pe, com_parametrizacion_epo cpe "+
							"   WHERE pe.cc_parametro_epo = cpe.cc_parametro_epo(+)  "+
							"     AND cpe.ic_epo(+) = ? "+
							"     AND pe.CC_PARAMETRO_EPO like 'CDER_%' "+
							"ORDER BY 1 ";

				ps = con.queryPrecompilado(strSQL);
				ps.setInt(1, Integer.parseInt(cveEpo));
				rs = ps.executeQuery();
				while(rs.next()) {
					mpParams.put(rs.getString("cc_parametro_epo").trim(), rs.getString("cg_valor")==null?"":rs.getString("cg_valor"));
				}
				rs.close();
				ps.close();
				
				return mpParams;
			} catch(Exception e){
				e.printStackTrace();
				throw new AppException("getParametrosEpo.Exception");
			} finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
				log.info("getParametrosEpo (S)");
			 }
		}
		 
		public void setParametrosEpo(String cveEpo, Map mpParams) throws AppException{
			log.info("setParametrosEpo (E)");
			AccesoDB con         = new AccesoDB();
			PreparedStatement ps = null;
			ResultSet rs         = null;
			String strSQL   = "";
			boolean commit = true;
			
			try{
				con.conexionDB();
				
				Iterator it = mpParams.keySet().iterator();
				while(it.hasNext()){
					boolean existe = false;
					String key = (String)it.next();
					
					strSQL = "SELECT COUNT(*) contador FROM com_parametrizacion_epo " +
						"            WHERE ic_epo = ? " +
						"           AND cc_parametro_epo = ? ";
					ps = con.queryPrecompilado(strSQL);
					ps.setInt(1, Integer.parseInt(cveEpo));
					ps.setString(2, key);
					rs = ps.executeQuery();
					if(rs.next()){
						existe = rs.getInt("contador")>0?true:false;
					}
					rs.close();
					ps.close();
						
					if(!existe){
						strSQL = "INSERT INTO com_parametrizacion_epo " +
							"            (ic_epo, cc_parametro_epo, cg_valor " +
							"            ) " +
							"     VALUES (?, ?, ? " +
							"            ) ";
						ps = con.queryPrecompilado(strSQL);
						ps.setInt(1, Integer.parseInt(cveEpo));
						ps.setString(2, key);
						ps.setString(3, (String)mpParams.get(key));
						ps.executeUpdate();
					}else{
						strSQL = "UPDATE com_parametrizacion_epo " +
									"   SET cg_valor = ? " +
									" WHERE ic_epo = ? AND cc_parametro_epo = ? ";
						ps = con.queryPrecompilado(strSQL);
						ps.setString(1, (String)mpParams.get(key));
						ps.setInt(2, Integer.parseInt(cveEpo));
						ps.setString(3, key);
						ps.executeUpdate();
					
					}
				}
				
			} catch(Exception e){
				commit = false;
				e.printStackTrace();
				throw new AppException("setParametrosEpo.Exception");
			} finally{
				if(con.hayConexionAbierta()){
					con.terminaTransaccion(commit);
					con.cierraConexionDB();
				}
				log.info("setParametrosEpo (S)");
			 }
		}

	/**
	 * Fodea 28-2015
	 * Obtiene el tipo de extinci�n que tiene la EPO.
	 * Por default tiene el valor L
	 * @return L: Larga, C: Corta
	 * @param claveEpo
	 */
	public String getTipoExtincion(String claveEpo) throws AppException{

		log.info("getTipoExtincion(E)");

		String            tipoExtincion = "";
		StringBuffer      consulta      = new StringBuffer();
		AccesoDB          con           = new AccesoDB();
		PreparedStatement ps            = null;
		ResultSet         rs            = null;

		consulta.append(" SELECT CG_VALOR \n"                                   );
		consulta.append("   FROM COM_PARAMETRIZACION_EPO \n"                    );
		consulta.append("  WHERE CC_PARAMETRO_EPO = 'CDER_TIPO_EXT_CONTRATO' \n");
		consulta.append("    AND IC_EPO = ? "                                   );

		try{

			con.conexionDB();

			ps = con.queryPrecompilado(consulta.toString());
			ps.setInt(1,Integer.parseInt(claveEpo));
			rs = ps.executeQuery();

			if(rs.next()){
				tipoExtincion = (rs.getString("CG_VALOR")== null)?"L" : rs.getString("CG_VALOR");
			}

			rs.close();
			ps.close();

		} catch (Exception e){
			log.error("getTipoExtincion(Exception)");
			throw new AppException("getTipoExtincion(Exception).", e);
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.debug("CONSULTA:\n " + consulta.toString());
		log.debug("IC_EPO: "     + claveEpo           );
		log.debug("CG_VALOR: "   + tipoExtincion      );
		
		log.info("getTipoExtincion(S)");
		return tipoExtincion;

	}

	public int getIcIFRep2(String loginUsuario) throws AppException{

		log.info("getIcIFRep2(E)");

		int               icIFRep2  = 0;
		StringBuffer      consulta  = new StringBuffer();
		AccesoDB          con       = new AccesoDB();
		PreparedStatement ps        = null;
		ResultSet         rs        = null;

		consulta.append(" SELECT COUNT(*) AS TOTAL \n" );
		consulta.append("   FROM COMCAT_CESIONARIO \n" );
		consulta.append("  WHERE IC_IF_REP2 = ? "      );

		try{

			con.conexionDB();

			ps = con.queryPrecompilado(consulta.toString());
			ps.setString(1,loginUsuario);
			rs = ps.executeQuery();

			if(rs.next()){
				icIFRep2 = rs.getInt(1);
			}

			rs.close();
			ps.close();

		} catch (Exception e){
			log.error("getIcIFRep2(Exception)");
			throw new AppException("getIcIFRep2(Exception).", e);
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.debug("CONSULTA:\n " + consulta.toString());
		log.debug("USUARIO: "    + loginUsuario       );
		log.debug("icIFRep2: "   + icIFRep2           );

		log.info("getIcIFRep2(S)");
		return icIFRep2;

	}

}// Termina EJBBean