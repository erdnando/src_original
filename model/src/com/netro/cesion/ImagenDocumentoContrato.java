package com.netro.cesion;

import com.itextpdf.text.DocumentException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collections;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/**
 * Clase para el manejo de imagenes (en PDF) de la documentaci�n de las Pymes.
 * Por ejemplo, Acta constitutiva, Acta de nacimiento, Cedula Fiscal, etc...
 * @author Gilberto Aparicio
 */
public class ImagenDocumentoContrato  {
	
	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ImagenDocumentoContrato.class);

	public ImagenDocumentoContrato() {  }

	private String numeroProceso;//Clave de la pyme. FODEA 041 - 2010 ACF
	private String clavePyme;//Clave de la pyme. FODEA 041 - 2010 ACF
	private String tipoExpediente;//Clave del tipo de expediente (2 Factoraje, 11 Cesion). FODEA 041 - 2010 ACF
	private String loginUsuario;//Login del usuario que hace la consulta al eFile.
	private String solicitud; //Clave de la imagen	
	private int size; //Tama�o del archivo
	private String rfcPyme;
	private String grupo_cesion;
	private String no_contrato;
	
	/**
	 * Guarda una imagen del documento de la pyme
	 * @param is Stream con los datos del archivo origen
	 * 
	 */	
	public void guardarImagenIndividual(InputStream is)
			throws AppException {
		log.info("guardarImagenIndividual(E)");
		//**********************************Validaci�n de parametros:*****************************
		// Se realiza en el m�todo privado
		//****************************************************************************************

		boolean exito = true;
		final AccesoDB SIN_PASAR_CONEXION = null;

		try {
				this.guardarImagen(is, SIN_PASAR_CONEXION);
		} catch(Exception e)  {
			exito = false;
			throw new AppException("Error en la carga de imagen de documentacion de Proveedor", e);
		} finally {
			log.info("guardarImagenIndividual(S)");
		}
	}

	/**
	 * Guarda la imagen del documento del contrato en la BD
	 * @param is Stream con los datos del archivo origen
	 * @param pcon Si se manda una conexion, este m�todo no realiza el commit
	 * depues de insertar la documentaci�n en la BD, dado que se le deja esa
	 * responsabilidad al m�todo que lo invoca.
	 * @throws netropology.utilerias.AppException
	 */
	private void guardarImagen(InputStream is, AccesoDB pcon)
			throws AppException {
		log.debug("guardarImagen(E)");
		boolean hayConexionActivaPrevia = (pcon != null && pcon.hayConexionAbierta());
		//**********************************Validaci�n de parametros:*****************************
		int tamanio = 0;
		int isolicitud = 0;
		
		try {
			if (this.solicitud == null || this.solicitud.equals("") || 	is == null || this.size == 0 ) {
				throw new Exception("Los parametros son requeridos");	
			}		
			isolicitud = Integer.parseInt(this.solicitud);
			tamanio = this.size;
		} catch(Exception e) {
			log.error("guardarImagen(Error). " +
					"Error en los parametros/atributos establecidos: " + e.getMessage() +
					"this.solicitud=" + this.solicitud + "\n" +	
					"this.size=" + this.size + "\n" +
					"is=" + is);
			throw new AppException("Error inesperado. ", e);
		}
		//***************************************************************************************

		boolean exito = false;
		PreparedStatement ps = null;
		AccesoDB con = new AccesoDB();		
		String strSQL = null;
		
		PreparedStatement ps1 = null;
		String strSQL1 = null;
		
		try {
			if (hayConexionActivaPrevia) {
				con = pcon;
			} else {
				con.conexionDB();
			}
						
		
			strSQL1 = 
					" UPDATE cder_solicitud " +
					" SET bi_documento = ? " +				
					" WHERE ic_solicitud = ? " ;
								
			ps1 = con.queryPrecompilado(strSQL1);
			ps1.setBinaryStream(1, is, tamanio);
			ps1.setInt(2, isolicitud);
	
			ps1.executeUpdate();
			ps1.close();
			ps1.close();

			log.debug("strSQL1-------------------"+strSQL1);

			exito = true;
		} catch(Exception e)  {
			exito = false;
			throw new AppException("Error Inesperado", e);
		} finally {
			log.debug("guardarImagen(S)");
			if (con.hayConexionAbierta() && !hayConexionActivaPrevia) {
				//Solo si la carga es de un solo archivo, se realiza el commit.
				//De lo contrario es responsabilidad del que manda a invocar el
				//m�todo, terminar la transacci�n y cerrar la conexion
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}

/**
	 * Obtiene la imagen almacenada y genera un 
	 * archivo temporal para consultarla
	 * @param rutaDestino Directorio donde se genera el archivo temporal
	 * @return Cadena con el nombre del archivo generado con la imagen
	 */
	public String consultarImagen(String rutaDestino) throws AppException {
		log.info("consultarImagen(E) ::..");
		//**********************************Validaci�n de parametros:*****************************
		int isolicitud = 0;
		try {
			if (this.solicitud == null || this.solicitud.equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
			isolicitud = Integer.parseInt(this.solicitud);
		} catch(Exception e) {
			log.error("consultarImagen(Error). " +
					"Error en los parametros/atributos establecidos: " + e.getMessage() +
					"this.solicitud=" + this.solicitud );
			throw new AppException("Error inesperado. ", e);
		}
		//***************************************************************************************
		AccesoDB con = new AccesoDB();
		String strSQL = 
				" SELECT bi_documento " +
				" FROM cder_solicitud " +
				" WHERE ic_solicitud = ? ";
		log.debug("..:: strSQL: "+ strSQL);
		try {
			con.conexionDB();
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, isolicitud);			
			
			ResultSet rs = ps.executeQuery();
			String nombreArchivoTmp = null;
			
			String extension = ".pdf";

			if (rs.next()) {				
				InputStream inStream = rs.getBinaryStream("bi_documento");
				CreaArchivo creaArchivo = new CreaArchivo();
				if (!creaArchivo.make(inStream, rutaDestino, extension)) {
					throw new AppException("Error al generar el archivo en " + rutaDestino);
				}
				nombreArchivoTmp = creaArchivo.getNombre();
				inStream.close();
			}
			rs.close();
			ps.close();

			return nombreArchivoTmp;
		} catch(Exception e) {
			log.error("consultarImagen(ERROR) ::..");
			throw new AppException("Error Inesperado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultarImagen(S) ::..");
		}
	}
	//FODEA 041 - 2010 ACF (I)
	/**
	 * Guarda la imagen del documento del contrato en la tabla temporal para generar
	 * el preacuse de la captura de la solicitud de consentimiento.
	 * @param is InputStream con los datos del archivo origen
	 * @throws netropology.utilerias.AppException
	 */
	public void guardarArchivoContratoTemp(InputStream is) throws AppException {
		log.info("guardarArchivoContratoTemp(E) ::..");
		AccesoDB con = new AccesoDB();		
		boolean exito = true;
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		int tamanio = 0;
		//**********************************Validaci�n de parametros:*****************************
		try {		
			if (this.numeroProceso == null || this.numeroProceso.equals("") || 	is == null || this.size == 0 ) {
				throw new Exception("Los parametros son requeridos");	
			}		
			tamanio = this.size;
		} catch(Exception e) {
			log.error("guardarImagen(Error). " +
			"Error en los parametros/atributos establecidos: " + e.getMessage() +
			"this.solicitud=" + this.solicitud + "\n" +	
			"this.size=" + this.size + "\n" +
			"is=" + is);
			throw new AppException("Error inesperado. ", e);
		}
		//***************************************************************************************
		try {
			con.conexionDB();

			strSQL.append(" UPDATE cdertmp_solicitud");
			strSQL.append(" SET bi_documento = ?");
			strSQL.append(" WHERE ic_proceso = ? ");
			
			log.debug("..:: strSQL: "+strSQL.toString());

			pst = con.queryPrecompilado(strSQL.toString());
			pst.setBinaryStream(1, is, tamanio);
			pst.setInt(2, Integer.parseInt(numeroProceso));
			
			pst.executeUpdate();
			pst.close();
		} catch(Exception e)  {
			exito = false;
			log.error("guardarArchivoContratoTemp(ERROR) ::..");
			throw new AppException("Error Inesperado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("guardarArchivoContratoTemp(S) ::..");
		}
	}
	
	/**
	 * Obtiene la documentaci�n de la Pyme del sistema EFILE almacenandola en archivos temporales para su consulta.
	 * 
	 * @param rutaDestino Directorio donde se genera el archivo temporal
	 * @return Mapa con el nombre de los archivos generados usando como clave el tipo de documento
	 */
	public Map consultarImagenesEFile(String strDirectorioPublicacion) throws AppException {
		log.info("consultarImagenesEFile(E) ::..");
		AccesoDB con = new AccesoDB();
		Map archivosEfile = new HashMap();
		//String rfcPyme = "";

		try {
			if (this.clavePyme == null || this.clavePyme.equals("")   ||  this.rfcPyme == null || this.rfcPyme.equals("")  ) {
				throw new Exception("LOS PARAMETROS SON REQUERIDOS.");
			}
		} catch(Exception e) {
			log.error("consultarImagenesEFile(ERROR) ::... " +
			"\nError en los parametros/atributos establecidos: " + e.getMessage() +
			"this.clavePyme=" + this.clavePyme );
			throw new AppException("Error inesperado. ", e);
		}
		
			try {
				con.conexionDB();
			
				if(grupo_cesion.equals("") )  { grupo_cesion = null;  } 
				
				String strSQL = " SP_OBTIENE_DOC_EFILE_CESION ( ? , ?, ? , ? )";
		      log.debug("..:: strSQL: "+strSQL);
		      log.debug("..:: varBind: ["+rfcPyme+","+clavePyme+","+grupo_cesion+","+no_contrato+"]");
		      
		      CallableStatement cs = con.ejecutaSP(strSQL);
		      cs.setString(1, rfcPyme);
		      cs.setString(2, clavePyme);
		      cs.setString(3, grupo_cesion);
		      cs.setString(4, no_contrato);
		      cs.execute();
				cs.close();
		            
		            
		      String strSQLTmp = " SELECT cg_extension, bi_documento FROM comtmp_doc_pyme WHERE ic_pyme = ?";
		      log.debug("..:: strSQLTmp: "+strSQLTmp);
		      log.debug("..:: varBind: ["+this.clavePyme+"]");
		      PreparedStatement ps = con.queryPrecompilado(strSQLTmp);
		      ps.setInt(1, Integer.parseInt(this.clavePyme));
		      ResultSet rs = ps.executeQuery();
		            
		      String nombreArchivoTmp = null;
			
			if (rs.next()) {
				String extension = rs.getString("cg_extension");
				InputStream inStream = rs.getBinaryStream("bi_documento");
				archivosEfile = this.obtenerExpedienteCesionDerechos(inStream, strDirectorioPublicacion);
				inStream.close();
			}
			rs.close();
			ps.close();
			
			this.guardaAccesoBitacoraEfile();
			
			return archivosEfile;
		} catch(Exception e) {
			throw new AppException("consultarImagenesEFile(ERROR) :: ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultarImagenesEFile(S) ::..");
		}
	}
	
	/**
	 * M�todo que genera el archivo pdf del expediente en el eFile para Cesi�n de Derechos.
	 * @return Mapa con el tipo de documentacion (clave) y nombre de los archivos generados (valor)
	 * @param rutaDestino Ruta absoluta destino, de los archivos a generar
	 * @param inStream Stream con el contenido del archivo PDF.
	 */
	private Map obtenerExpedienteCesionDerechos(InputStream is, String rutaDestino) throws FileNotFoundException, DocumentException, IOException {
		log.info("obtenerExpedienteCesionDerechos(E) ::..");
		Set set;
		Map map = new HashMap();
		ArrayList master = new ArrayList();
		int pageOffset = 0;

		try {
		  Comunes.crearDirectorios(rutaDestino);
			String nombreArchivoTmp = Comunes.cadenaAleatoria(8) + ".pdf";
		  String ruta = rutaDestino + nombreArchivoTmp;
 		  FileOutputStream fos = new FileOutputStream(ruta);      
			byte arrBuff[] = new byte[524288]; //512k Buffer
			int bytesRead;
			while ((bytesRead = is.read(arrBuff)) != -1) {
				fos.write(arrBuff, 0, bytesRead);
			}
			fos.flush();
			fos.close();
			
			String sClaveTipoDocumentacion = "6";	//Cesi�n de Derechos
			map.put(sClaveTipoDocumentacion, nombreArchivoTmp);

			return map;
		} finally {
			log.info("obtenerExpedienteCesionDerechos(S) ::..");
		}
	}
	
	/**
	 * Obtiene la documentaci�n de la Pyme del sistema EFILE almacenandola en archivos temporales para su consulta.
	 * 
	 * @param rutaDestino Directorio donde se genera el archivo temporal
	 * @return Mapa con el nombre de los archivos generados usando como clave el tipo de documento
	 */
	public void guardaAccesoBitacoraEfile() throws AppException {
		log.info("guardaAccesoBitacoraEfile(E) ::..");
		AccesoDB con = new AccesoDB();
		boolean transactionOk = true;
		String rfcPyme = "";

		try {
			if (this.clavePyme == null || this.clavePyme.equals("") || this.loginUsuario == null || this.loginUsuario.equals("")) {
				throw new Exception("Los parametros son requeridos.");
			}
		} catch(Exception e) {
			log.error("consultarImagenesEFile(ERROR) ::... " +
			"\nError en los parametros/atributos establecidos: " + e.getMessage() +
			"\nthis.clavePyme=" + this.clavePyme +
			"\nthis.loginUsuario=" + this.loginUsuario);
			throw new AppException("Error inesperado. ", e);
		}

		try {
			con.conexionDB();

			String strSqlRfc = " SELECT cg_rfc FROM comcat_pyme WHERE ic_pyme = ?";
			log.debug("..:: strSqlRfc: "+strSqlRfc);
			log.debug("..:: varBind: ["+this.clavePyme+"]");
			PreparedStatement pst = con.queryPrecompilado(strSqlRfc);
			pst.setInt(1, Integer.parseInt(this.clavePyme));
			ResultSet rst = pst.executeQuery();
			while (rst.next()) {
				rfcPyme = rst.getString("cg_rfc")==null?"":rst.getString("cg_rfc");
			}
			rst.close();
			pst.close();
			
			String strSQL = "proc_bit_accesos(?, ?, ?, ?)";
			log.debug("..:: strSQL: "+strSQL);
			log.debug("..:: varBind: ["+rfcPyme+","+this.tipoExpediente+","+this.loginUsuario+",Nafin Electr�nico]");
			CallableStatement cs = con.ejecutaSP(strSQL);
			cs.setString(1, rfcPyme);
			cs.setInt(2, Integer.parseInt(this.tipoExpediente));
			cs.setString(3, this.loginUsuario);
			cs.setString(4, "Nafin Electr�nico");
			cs.execute();
			cs.close();
		} catch(Exception e) {
			transactionOk = false;
			throw new AppException("guardaAccesoBitacoraEfile(ERROR) :: ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("guardaAccesoBitacoraEfile(S) ::..");
		}
	}

	public void setNumeroProceso(String numeroProceso) {this.numeroProceso = numeroProceso;}
	public void setClavePyme(String clavePyme) {this.clavePyme = clavePyme;}
	public void setTipoExpediente(String tipoExpediente) {this.tipoExpediente = tipoExpediente;}
	public void setLoginUsuario(String loginUsuario) {this.loginUsuario = loginUsuario;}
	
	public String getNumeroProceso() {return numeroProceso;}
	public String getClavePyme() {return clavePyme;}
	public String getTipoExpediente() {return tipoExpediente;}
	public String getLoginUsuario() {return loginUsuario;}
	//FODEA 041 - 2010 ACF (F)
/** Establece la clave de la Pyme
	 * @param solicitud Cadena con la clave de la Pyme
	 */
	public void setSolicitud(String solicitud) {
		this.solicitud = solicitud;
	}

	/**
	 * Obtiene la clave de la Pyme
	 * @return Cadena con la clave de la Pyme
	 */
	public String getSolicitud() {
		return solicitud;
	}


	/**
	 * Obtiene el tama�o del archivo en bytes
	 * @return tama�o en bytes
	 */
	public int getSize() {
		return size;
	}
	/**
	 * Establece el tama�o del archivo en bytes
	 * @param size tama�o en bytes
	 */
	public void setSize(int size) {
		this.size = size;
	}
	
	
	public String getRfcPyme() {
			return rfcPyme;
		}

		public void setRfcPyme(String rfcPyme) {
			this.rfcPyme = rfcPyme;
		}

		public String getGrupo_cesion() {
			return grupo_cesion;
		}

		public void setGrupo_cesion(String grupo_cesion) {
			this.grupo_cesion = grupo_cesion;
		}

		public String getNo_contrato() {
			return no_contrato;
		}

		public void setNo_contrato(String no_contrato) {
			this.no_contrato = no_contrato;
		}
}