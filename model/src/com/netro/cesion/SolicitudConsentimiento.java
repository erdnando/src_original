package com.netro.cesion;

public class  SolicitudConsentimiento implements java.io.Serializable{


	public String noEpo;
	public String noIF;
	public String clasificacionEPO;
	public String noContrato;
	public String monto;
	public String moneda;
	public String contratacion;
	public String fVigencia_Ini;
	public String fVigencia_Fin;
	public String campo1;
	public String campo2;
	public String campo3;
	public String campo4;
	public String campo5;
	public String contrato;
	public String solicitud;
	public String preacuse;
	

	
	//para modificar
	public String modificar;
	
  //variables para el paginador
	public String paginar;
	public String paginaOffset;
	public String termina;
	
	
	//Variables para el certtificado
	public String pkcs7;
	public String textoFirmado;
	public String firmar;
	public String serial;
	public String folio;
	public String validCert;
	
	
	//SET


	public void setNoEpo(String noEpo) {  this.noEpo = noEpo; 	} 
	public void setNoIF(String noIF) { 		this.noIF = noIF; }
	public void setClasificacionEPO(String clasificacionEPO) { 	this.clasificacionEPO = clasificacionEPO; 	}
	public void setNoContrato(String noContrato) { 	this.noContrato = noContrato; }
	public void setMonto(String monto) { 	this.monto = monto;  	}
	public void setMoneda(String moneda) { 	this.moneda = moneda; 	}
	public void setContratacion(String contratacion) { 	this.contratacion = contratacion;  }
	public void setFVigencia_Ini(String fVigencia_Ini) {   	this.fVigencia_Ini = fVigencia_Ini; 	}
	public void setFVigencia_Fin(String fVigencia_Fin) {  	this.fVigencia_Fin = fVigencia_Fin; 	}
	public void setCampo1(String campo1) { this.campo1 = campo1; }
	public void setCampo2(String campo2) {  this.campo2 = campo2;  }
	public void setCampo3(String campo3) { this.campo3 = campo3;  }
	public void setCampo4(String campo4) {  this.campo4 = campo4; 	}
	public void setCampo5(String campo5) {  this.campo5 = campo5;  	}
	public void setContrato(String contrato) { this.contrato = contrato; }
	public void setSolicitud(String solicitud) { 		this.solicitud = solicitud; 	}	
	public void setPreacuse(String preacuse) { 		this.preacuse = preacuse; 	}
	




	//paginar

	public void setPaginar(String paginar) { 	this.paginar = paginar; }
	public void setPaginaOffset(String paginaOffset) { this.paginaOffset = paginaOffset; }
	public void setTermina(String termina) { 		this.termina = termina; 	}
	
	
	//Certificado 
 	public void setPkcs7(String pkcs7) {   this.pkcs7 = pkcs7;  }
	public void setTextoFirmado(String textoFirmado) { this.textoFirmado = textoFirmado; }
	public void setFirmar(String firmar) {  		this.firmar = firmar;   	}
	public void setSerial(String serial) {  this.serial = serial;   }
	public void setFolio(String folio) {  this.folio = folio;  	}
	public void setValidCert(String validCert) {   this.validCert = validCert;  }
	
	

	// GET
	
	public String getNoEpo() {	return noEpo;  }
	public String getNoIF() { 	return noIF; }
	public String getClasificacionEPO() { 	return clasificacionEPO; 	}
	public String getNoContrato() { 	return noContrato; 	}
	public String getMonto() { return monto;  }
	public String getMoneda() {  	return moneda;   }
	public String getContratacion() { 	return contratacion; }
	public String getFVigencia_Ini() {  	return fVigencia_Ini;  }
	public String getFVigencia_Fin() {  return fVigencia_Fin;  }
	public String getCampo1() {  return campo1;  }
	public String getCampo2() { 	return campo2;  }
	public String getCampo3() { return campo3;  }
	public String getCampo4() { return campo4;  }
	public String getCampo5() { 	return campo5;  	}
	public String getContrato() { return contrato;  }
	public String getSolicitud() { 		return solicitud; 	}
	public String getPreacuse() { 		return preacuse; 	}
	
	
	
	
	
   //paginar
	public String getPaginar() { return paginar; }
	public String getPaginaOffset() { return paginaOffset; }
	public String getTermina() { 	return termina;  }
	
	//Certificado
	public String getPkcs7() {  	return pkcs7;   	}
	public String getTextoFirmado() {  	return textoFirmado;  }
	public String getFirmar() { 	return firmar; }
	public String getSerial() {   return serial;   }
	public String getFolio() {   return folio;   }
	public String getValidCert() {  	return validCert;   	}
	

public String toString()
	{
		String cadena = "";
		cadena = " pkcs7=" + pkcs7 + 
					" TextoFirmado=" + textoFirmado +
				   " serial=" + serial +
					" usuario=" + termina +
					" folio=" + folio;

		return cadena;
	}
	



}