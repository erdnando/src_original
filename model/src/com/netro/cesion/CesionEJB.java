package com.netro.cesion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.AppException;
import netropology.utilerias.Registros;

@Remote
public interface CesionEJB   {

	public boolean getEmpresaPrincipalGrupoCDer(String cvePyme)throws AppException;
	public List getEmpresasXGrupoCder(String icGrupo) throws AppException;
	public void procesoSolicRech() throws AppException;
	public boolean existeCedenteXSolic(String claveSolicitud) throws AppException;
	public List getFirmasRepresentantes(String ic_solicitud ) throws AppException;
	public String insertSolicitud(String noEpo,
										   String noIF,
											String clasificacionEPO,
											String noContrato,
											String monto,
											String moneda,
											String contratacion,
											String fVigencia_Ini,
											String fVigencia_Fin,
											String campo1,
											String campo2,
											String campo3,
											String campo4,
											String  campo5,
											String contrato,
											String pyme) throws AppException;


	public String modificaSolicitud(String solicitud, String noEpo,
												String noIF,
												String clasificacionEPO,
												String noContrato,
												String monto,
												String moneda,
												String contratacion,
												String fVigencia_Ini,
												String fVigencia_Fin,
												String campo1,
												String campo2,
												String campo3,
												String campo4,
												String  campo5,
												String contrato)throws AppException;


	public List  inicializarPreAcuse (String solicitud)	throws AppException;
	public void  eliminaSolicitud (String solicitud)	throws AppException;
	public String enviaSolicitud(String solicitud, String serial, String textoFirmado, String pkcs7, String folio,  boolean validCert, String usuario, String nombreUsuario) throws AppException;
	public boolean existeNoContrato(String numeroContrato, String claveEpo, String claveIfCesionario, String clavePyme, String numeroSolicitud) throws AppException;//FODEA 041 - 2010 ACF
	public String solicitudEnviActivarReactivar(String solicitud, String cambiaEstatus,String causaRechazo )		throws  AppException;
	public List  inicializarPreAcuseActivaReactiva(String solicitud)	throws  AppException;
	public String transmitesolicitud(String solicitud, String serial, String textoFirmado, String pkcs7, String folio, boolean validCert, String usuario, String nombreUsuario, String estatus, String causasRechazo,String strDirectorioTemp)	throws AppException;
	public Vector getCamposAdicionales(String sNoCliente, String campos) 	throws  AppException;
	public List guardaClausulado(String clausulado, String clave_if) throws AppException;
	public List guardaTestigos(String clave_if, String testigo)throws AppException;//FODEA 037 - 2009 I.A.
	public List borraTestigos(String clave_if, String testigo)throws  AppException;//FODEA 037 - 2009 I.A.
	public ArrayList getComboTestigos(String tipoAfiliado, String claveAfiliado, String perfil, String strLogin ) throws  AppException;//FODEA 037 - 2009 I.A.
	public HashMap getTestigos(String clave_if)throws  AppException;//FODEA 037 - 2009 I.A.
	public HashMap getNotificacionesPyme(HashMap hmParamNotifPyme)throws  AppException;//FODEA 037 - 2009 I.A.
	public HashMap getNotificacionesIf(HashMap hmParamNotifIf) throws AppException;//FODEA 037 - 2009 I.A.
	public HashMap getNotificacionesNafin(HashMap hmParamNotifNafin) throws AppException;//FODEA 037 - 2009 I.A.
	public HashMap getSolicitudConsentimiento(HashMap hmParamSolicCons) throws  AppException;//FODEA 037 - 2009 I.A.
	public HashMap getContratosCesion(HashMap hmParamNotifNafin) throws AppException;//FODEA 037 - 2009 I.A.


	public HashMap getCamposAdicionalesParametrizados(String clave_epo, String clave_producto) throws AppException;//FODEA 037 - 2009 ACF
	public HashMap consultaContratosCesionPyme(HashMap parametros_consulta) throws  AppException;//FODEA 037 - 2009 ACF
	public HashMap consultaContratoCesionPyme(String clave_solicitud) throws  AppException;//FODEA 037 - 2009 ACF
	public HashMap consultaContratoCesionPyme(String clave_solicitud, String cgUsuario) throws  AppException;//FODEA 037 - 2009 ACF
	public String descargaContratoCesion(String clave_solicitud, String strDirectorioTemp) throws  AppException;//FODEA 037 - 2009 ACF
	public StringBuffer consultaClausuladoParametrizado(String clave_if) throws  AppException;//FODEA 037 - 2009 ACF
	public String acuseFirmaContratoCesionPyme(HashMap parametrosFirma) throws  AppException;//FODEA 037 - 2009 ACF
	public List getComboPymesContratoCesion(String clave_if) throws  AppException;//FODEA 037 - 2009 ACF
	public HashMap consultaContratosCesionIf(HashMap parametros_consulta) throws  AppException;//FODEA 037 - 2009 ACF
	public HashMap consultaContratoCesionIf(String clave_solicitud) throws  AppException;//FODEA 037 - 2009 ACF
	public int getDiasMinimosNotificacion(String clave_epo) throws  AppException;//FODEA 037 - 2009 ACF
	public String acuseFirmaContratoCesionIF(HashMap parametrosFirma) throws  AppException;//FODEA 037 - 2009 ACF
	public List getComboPymesNotificacion(String clave_epo) throws  AppException;//FODEA 037 - 2009 ACF
	public HashMap consultaNotificacionesEpo(HashMap parametrosConsulta) throws  AppException;//FODEA 037 - 2009 ACF
	public HashMap consultaNotificacionEpo(String claveSolicitud) throws  AppException;//FODEA 037 - 2009 ACF
	public String acuseFirmaNotificacionEpo(HashMap parametrosFirma) throws  AppException;//FODEA 037 - 2009 ACF
	public List getComboPymesContratoCesionTestigo(String claveIf) throws  AppException;//FODEA 037 - 2009 ACF
	public HashMap consultaContratoCesionTestigo(HashMap parametrosConsulta) throws  AppException;//FODEA 037 - 2009 ACF
	public HashMap consultaContratoCesionTestigo(String claveSolicitud) throws  AppException;//FODEA 037 - 2009 ACF
	public String acuseFirmaContratoCesionTestigo(HashMap parametrosFirma) throws  AppException;//FODEA 037 - 2009 ACF

	public List  monitorCesionDerechos(String tipousuario, String usuarios) throws  AppException;
	public ArrayList getParametrosEPO(String sNoEPO) throws  AppException;
	public String cancelaPreacuse(String solicitud, String cambiaEstatus)		throws  AppException;
	public String clasificacionEpo(String sNoEPO)		throws  AppException;

	public String MensajeNotiCesionDerechos(String noEpo,String mensaje)		throws  AppException;
	public String MostrarMensajeNotiCesionDerechos(String noEpo) throws  AppException;
	public List  inicializarAvisoNotificacionCesion (String solicitud)	throws  AppException;

	public HashMap consultaAvisoSolicitudConsentimientoPyme(String clavePyme) throws  AppException;//FODEA 041 - 2010 ACF
	public String insertarSolicitudConsentimientoTemp(HashMap datosCapturaSolicitud) throws  AppException;//FODEA 041 - 2010 ACF
	public HashMap consultarSolicitudConsentimientoTemp(String numeroProceso) throws  AppException;//FODEA 041 - 2010 ACF
	public String insertarSolicitudConsentimiento(HashMap datosPreacuseSolicitud) throws  AppException;//FODEA 041 - 2010 ACF
	public HashMap consultarSolicitudConsentimiento(String numeroSolicitud) throws  AppException;//FODEA 041 - 2010 ACF
	public HashMap consultarSolicitudConsentimiento(String numeroSolicitud, String icUsuario)  throws AppException;//FODEA 023-2015FVR
	public void modificarSolicitudConsentimiento(HashMap datosCapturaSolicitud) throws  AppException;//FODEA 041 - 2010 ACF
	public String enviaSolicitudConsentimiento(HashMap parametrosEnvio)	throws  AppException;//FODEA 041 - 2010 ACF
	public HashMap obtieneSolicitudesEnviadasPorUsuario(HashMap parametrosConsulta)	throws  AppException;//FODEA 041 - 2010 ACF
	public HashMap obtieneSolicitudesAutorizadasPorUsuario(HashMap parametrosConsulta)	throws  AppException;//FODEA 041 - 2010 ACF
	public void rechazarNotifEpoVentanilla(String numeroSolicitud)	throws  AppException;//FODEA 041 - 2010 ACF
	public String acuseNotifEpoVentRedireccionamiento(HashMap parametrosFirma) throws  AppException;//FODEA 041 - 2010 ACF
	public List obtenerComboTipoContratacion(String claveEpo) throws  AppException;//FODEA 041 - 2010 ACF
	public HashMap getMontosMonedasSolicitud(String claveSolicitud) throws  AppException;//FODEA 016 - 2011 ACF

	public String obtenerClave (String catalogo)	throws  AppException;//FODEA 041 - 2010
	public List  consultaCatalogos (String catalogo) throws  AppException;//FODEA 041 - 2010
	public String agregarDatoCatalogo (String catalogo, String clave, String descripcion, String epo) throws  AppException;//FODEA 041 - 2010
	public String  modificarCatalogo (String catalogo, String clave, String descripcion, String epo) throws  AppException;//FODEA 041 - 2010
	public String  eliminaCatalogo (String catalogo, String clave)	throws  AppException;//FODEA 041 - 2010
	public List consultaSolicitudes (String noEpo)	throws  AppException;//FODEA 041 - 2010
	public String  aceptaEnterado (List  solicitudes ) throws  AppException;//FODEA 041 - 2010
	public StringBuffer getMontoMoneda(String clave_solicitud) throws  AppException;
	public List  IniEnvioCorreo (String noEpo, String solicitud) throws  AppException;
	public String EnviodeCorreo (String claveEpo, String claveSolicitud, String nombre, String correoPara, String comentarios, String ventanilla,String nombreArchivo  )	throws  AppException;
	public String  CorreoUsuario (String noEpo) 	throws  AppException;

	public void setProveedorNotificado(String claveSolicitud)
		throws  AppException; // F041 - 2010 By JSHD
	public void resetNotificacionProveedor(String claveSolicitud)
		throws  AppException; // F041 - 2010 By JSHD
	public boolean proveedorNotificado(String claveSolicitud)
		throws  AppException; // F041 - 2010 By JSHD
	public String acuseNotifEpoVentRechazo(HashMap parametrosFirma, String observaciones)
		throws  AppException; // F041 - 2010 By JSHD

	public String  banderaCorreo (String claveSolicitud) throws  AppException;
	public void enviarCorreoRedireccionamiento(HashMap parametrosEnvioCorreo) throws  AppException;//FODEA 016 - 2011 ACF
	//-------------------------------------------------------------------------->> FODEA 030 - 2011 ACF - I
	public HashMap consultarExtincionContratosPyme(HashMap parametrosConsulta) throws  AppException;
	public HashMap consultarExtincionContratosPyme(String claveSolicitud) throws  AppException;
	public HashMap obtenerExtincionesPorUsuario(HashMap parametrosConsulta)	throws  AppException;
	public String acuseEnvioSolicitudExtincion(HashMap parametrosFirma) throws  AppException;
	public void cancelarSolicitudExtincion(String claveSolicitud) throws  AppException;
	public HashMap consultaAvisoSolicitudExtincionIf(String claveIf) throws  AppException;
	public List getComboPymesExtincion(String claveIf) throws  AppException;
	public HashMap consultarExtincionContratosIf(HashMap parametrosConsulta) throws  AppException;
	public HashMap consultarExtincionContratosIf(String claveSolicitud) throws  AppException;
	public String acuseExtincionContratoIf(HashMap parametrosFirma) throws  AppException;
  public String acuseExtincionContratoTestigo(HashMap parametrosFirma) throws  AppException;
  public HashMap consultaConvenioExtincionCesionDerechos(String claveSolicitud) throws  AppException;
	//-------------------------------------------------------------------------->> FODEA 030 - 2011 ACF - F

	public List  consSoliExtincion (String noEpo)	throws  AppException;
	public List  consExtincionPreacuse (String claveSolicitud, String epo, String ventanilla)throws  AppException;
	public String EnviodeCorreoEXT (String claveEpo, String claveSolicitud,  String correoPara, String comentarios, String ventanilla, String asunto )	throws  AppException;
	public String acuseExtinContratoEPO(List parametrosFirma) throws  AppException;
	public String acuseExtinContratoEpoNotificacion(List parametrosFirma) ;

	public List  consExtiPreacuseEpoVen (String claveSolicitud, String epo)		throws  AppException;
	public String acuseExtinContratoEPOVen(List parametrosFirma) 	throws  AppException;
	public String  EnviodeCorreoEpoVenEXT (String claveEpo, String claveSolicitud, String correoPara, String comentarios,  String asunto )throws  AppException;

	public  String generaSelloDigital(String clave_solicitud, String strNombreUsuario ) throws  AppException;
	public String DescargaContratoCesionPyme(List datos) throws  AppException;
	public String DescargaContratoCesionAcuse(List datos)  throws  AppException;

	public boolean actualizarNombreClasificacionEpo(String claveEPO, String nombreClasificacionEPO)
		 ;
	public String consultaEnvioCorreo (String claveSolicitud);

	public String  archNotifiEPO ( HashMap parametros);

	public String  archConCesionIF ( HashMap parametros);

	public String  archNotifiConCesionIF ( HashMap parametros);

	public boolean esSolicitudFirmadaPorTestigo(String claveSolicitud, String loginUsuario);

	public String  archAcuseContratoTestigoIF ( HashMap parametros);

	public boolean existeNumeroContrato(String clavePyme,	String noContrato, String claveEpo);
	public boolean existeNumeroContratoCedido(String noContrato, String claveEpo);
	public void capturaContratosCedidos(String claveEpo, String claveIF, String clavePyme, String contrato);
	public void eliminaContratoCedido(String claveEpo, String contrato);



	public void guardaCedente(String claveIF, String clavePyme, String tipoRep, String tipoCed, String figJuridica, String escPublica,
									  String fecEscPublica, String nomLic, String numNotario, String oriNotario, String regComercio,
									  String numFolio,String escPublicaRep, String fecEscPublicaRep,
									  String nomLicRep, String numNotarioRep, String oriNotarioRep,String banderaRep,String domicilioLegal); //Agregado por FODEA 033 2013

	public void eliminaCedente(String claveIF, String clavePyme); //Agregado por FODEA 033 2013

	public void modificarCedente(String claveIF, String clavePyme, String clavePymeAnt, String tipoRep, String tipoCed, String figJuridica, String escPublica,
									  String fecEscPublica, String nomLic, String numNotario, String oriNotario, String regComercio,
									  String numFolio,String escPublicaRep, String fecEscPublicaRep,
									  String nomLicRep, String numNotarioRep, String oriNotarioRep,String banderaRep,String domicilioLegal) ; //Agregado por FODEA 033 2013

	public boolean existeCedente(String claveIF, String clavePyme) ; //Agregado por FODEA 033 2013

	public void guardaCesionario(String claveIF, String tipoRep, String nomRep, String figJuridica, String escPublica,
									  String fecEscPublica, String nomLic, String numNotario, String oriNotario, String regComercio,
									  String numFolio, String clabe,String escPublicaRep, String fecEscPublicaRep,
									  String nomLicRep, String numNotarioRep, String oriNotarioRep,String banderaRep,String domicilioLegal,
									  String strDirectorioTemp,String nombrePoderesIF,String nombreExtincionIF) ; //Agregado por FODEA 033 2013

	public void modificarCesionario(String claveIF, String tipoRep, String nomRep, String figJuridica, String escPublica,
									  String fecEscPublica, String nomLic, String numNotario, String oriNotario, String regComercio,
									  String numFolio, String clabe,String escPublicaRep, String fecEscPublicaRep,
									  String nomLicRep, String numNotarioRep, String oriNotarioRep,String banderaRep,String domicilioLegal,
									  String strDirectorioTemp,String nombrePoderesIF,String nombreExtincionIF) ; //Agregado por FODEA 033 2013

	public boolean existeCesionario(String claveIF) ; //Agregado por FODEA 033 2013

	public String descargaConsentimiento(String clave_solicitud, String strDirectorioTemp)	;
	public String getBanderaConsentimiento(String clave_solicitud);
	public Registros getMapaCedente(String ic_pyme, String ic_if);
	public Registros getMapaCesionario(String ic_if);
	public String generarContrato(String clave_solicitud, String strDirectorioTemp);
	public String descargaContratoCesionNuevo(String clave_solicitud, String strDirectorioTemp);
	public String getBanderaContrato(String clave_solicitud);
	public String obtenerConsentimiento(String clave_solicitud,String  nombreUsuario,String strDirectorioTemp);
	public String generarContratoExtincion(String clave_solicitud, String strDirectorioTemp);
	public String descargaContratoExtincionNuevo(String clave_solicitud, String strDirectorioTemp);
	public String getBanderaContratoExtincion(String clave_solicitud);
	public boolean existeRepresentante2(String claveIF);
	public void modificarRepresentanteIF2(String claveIF,String tipo_rep,String representante,String escritura_pub,
		String fecha_escritura,String txt_lic_rep,String num_notario,String ciudad_origen,String claveIf2) ;
	public void solicitarProrroga(String claveSolicitud, String contrato) ;
	public void aceptarRechazarProrroga(String claveSolicitud,String estatusProrroga) ;
	public void retornarSolicOperativo(String claveSolicitud, String causasRetorno,String estatusRetorno) ;
	public void rechazarIfOperativo(String claveSolicitud) ;
	public void aceptarIfOperativo(String claveSolicitud, String banco_deposito, String numero_cuenta,String clabe, String monto_credito, String referencia, String fecha_vencimiento, String usuario);
	public String descargaPoderesContratoCesion(String clave_solicitud, String strDirectorioTemp) throws  AppException;
	public String descargaPoderesContratoExtincion(String clave_solicitud, String strDirectorioTemp) throws  AppException;
	public String getBanderaPoderes(String clave_solicitud,String columna)throws  AppException;
	public String acuseFirmaContratoCesionIFRep2(HashMap parametrosFirma) throws  AppException;
	public String  getloginRepresentante2(String claveIF) throws  AppException;

	public boolean setCancelaNafin(String clave_solicitud,String motivosCancelacion)throws AppException; //FXX-2015
	public HashMap getRepresentantes(String loginUsrPyme, String clavePyme) throws  AppException; //FXX-2015
	public HashMap getRepresentantesCEDENTE(String ic_solicitud, String clavePyme, String loginUsuario ) throws AppException; //F23-2015
	public Map getParametrosEpo(String cveEpo) throws AppException;
	public void setParametrosEpo(String cveEpo, Map mpParams) throws AppException;
	public HashMap getRepresentantes(String acuse) throws AppException;
	public HashMap getDatosPoderesPyme(String icSolicitud, String icPyme) throws AppException;
	public String getIfRepresentante2(String icIf) throws AppException;
	public String getTipoExtincion(String claveEpo) throws AppException;
	public int getIcIFRep2(String loginUsuario) throws AppException;

}
