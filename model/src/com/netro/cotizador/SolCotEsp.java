/*************************************************************************************
*
* Nombre de Clase o de Archivo: SolCotEsp
*
* Versi�n: 0.1
*
* Fecha Creaci�n: 30/03/2005
*
* Autor: Ricardo Fuentes
*		Salvador Saldivar Barajas
*
* Fecha Ult. Modificaci�n:
*
* Descripci�n de Clase:
*
*************************************************************************************/

package com.netro.cotizador;

import java.util.Vector;
import com.netro.exception.*;
import java.sql.*;
import java.io.*;
import java.util.*;
import javax.ejb.Remote;

import netropology.utilerias.AppException;

@Remote
public interface SolCotEsp {

	public Solicitud guardaSolicitud(Solicitud sol)
		throws NafinException;

	public Solicitud cargaDatos(String ic_if,String iNoUsuario,String tipoUsuario,Solicitud sol)
		throws NafinException;
		
	public Solicitud complementaDatos(Solicitud sol)
		throws NafinException;
		
	public Solicitud guardaPagos(Solicitud sol,String fechas[],String montos[])
		throws NafinException;
		
	public Solicitud guardaPagosMasiva(Solicitud sol,ArrayList alPagos)
		throws NafinException;
		
	public Solicitud consultaSolicitud(String ic_solicitudS)
		throws NafinException;
		
	public ArrayList obtenerListaIF(String ic_usuario)
		throws NafinException;

	public ArrayList obtenerListaTodosIFCotizador()
		throws NafinException;		

	public boolean mostrarInstrucciones(String ic_usuario,String ic_if,String tipo_usuario) 
		throws NafinException;

	public void modifInstrucciones(String ic_usuario,String ic_if,String tipo_usuario,String cs_instrucciones) 
		throws NafinException;
	
	public abstract Vector getClaveSolicitud(String strClave) 
		throws NafinException;
		
	public abstract void setRechazaTomaFirme(String ic_solic_esp, String cg_observaciones) 
		throws NafinException;

	public List getRiesgos(String tipo)
		throws NafinException;

	public Solicitud consultaCotizacionCalc(String noreferencia)
		throws NafinException;

	public List getCurvasUSD(String icMoneda)
		throws NafinException;

	public Solicitud CalculoTasasUSD(Solicitud sol, boolean bandera)
		throws NafinException;

	public void guardaCotizacionUSD(Solicitud sol,String nombreUsuario, String icUsuario)
		throws NafinException;
		
	public List getDiasPorVencerTrad(Solicitud sol)
		throws NafinException;

	public int getPlazoConv(Solicitud sol);
		
	public List getEsquemasRecup(Solicitud sol)
		throws NafinException;
		
	public List getEsquemasRecup(Solicitud sol,String origen)
		throws NafinException;
		
	public List getDiasAPagar(Solicitud sol)
		throws NafinException;
		
	public List getDiasPagoPP(Solicitud sol,String[] arrFechas)
		throws NafinException;	
			
	public Map getFechasPagos(String fini, String fvenc, String diadpago, String periodo)	
		throws NafinException;
		
	public boolean CotizacionLinea(String icejec, String iNoCliente)
		throws NafinException;
		
	public boolean ExisteSWAP() throws NafinException;
	
	public boolean ValidaCurvas(String icmoneda) throws NafinException;
	
	public boolean validaEsquemaLinea(String icesquema, String icmoneda) throws NafinException;
	
	public  Map<String,Object>  devuelveMail(String icmoneda) throws NafinException;
	
	public void completarDatos(String ic, String iNoCliente, String tipo, String tasa, String ictasa)
		throws NafinException;		
		
	public List tipoOperRiesgo(String iNoCliente) throws NafinException;
		
	public String buscaCurva(String icmoneda) throws NafinException;	
	
	public String getIc_tasa(String ictipotasa, String icmoneda, String icesquema, String plazo_oper, String cg_ut_ppc, String cg_ut_ppi)
		throws NafinException;
	
	public String techoTasaProtegida() throws NafinException;					
		
	public Hashtable getValidaFechasPagos(String fini, String fvenc, String periodo)
		throws NafinException;
	
	public int restaFechas(String fechaDisp, String fechaRestar)
		throws NafinException;
	
	/**
         * metodo para enviar correo QC-DLHC 2017
         * @param datos
         * @throws AppException
         */
        public void enviaCorreoCotizador(Map<String,Object> datos ) throws AppException;
	
	}
