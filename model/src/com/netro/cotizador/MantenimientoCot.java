package com.netro.cotizador;

import netropology.utilerias.*;
import java.util.*;
import java.math.*;
import java.io.*;
import com.netro.exception.*;
import javax.ejb.Remote;

@Remote
public interface MantenimientoCot {
	
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>IHJ*/
	public abstract String getDirectorXArea(String ic_area)
		throws NafinException;

	public abstract Vector getEjecutivosXArea(String ic_area)
		throws NafinException;
        /**
         *
         * @param icArea
         * @param icIF
         * @return
         * @throws NafinException
         */
	public abstract ArrayList getIntermediariosFin(String icArea, String icIF)
		throws NafinException;

	public abstract String getRelacionEjecutivoIF(String ic_ejecutivo, String ic_if)
		throws NafinException;

	public abstract void setRelacionEjecutivoIF(String operacion, String ic_if, String ic_ejecutivo)
		throws NafinException;
/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<IHJ*/
	public void guardaEjecutivo(String ic_usuario, String nombre, String apellido_paterno,
								String apellido_materno, String email, String telefono, String fax,
								String area,String director) throws NafinException;

	public void modificaEjecutivo(int ic_ejecutivo, String nombre, String apellido_paterno,
								String apellido_materno, String email, String telefono, String fax,
								String area,String director) throws NafinException;
		public Vector obtenDatosEjecutivo(int ic_ejecutivo)
			throws NafinException;
		
	public Vector getDatosAutorizacion(String ic_ejecutivo, String ic_if)
		throws NafinException;
	/**
         *
         * @param claveEjecutivo
         * @param claveIF
         * @param tipoAutorizacion
         * @param montoMmayor
         * @param cgCotLinea
         * @param correos
         * @throws NafinException
         */
	public void actualizaDatosAutorizacion(String claveEjecutivo, String claveIF,String tipoAutorizacion,String montoMayor, String cgCotLinea, String correos)
		throws NafinException;

	public String revisarEstatusSolicEjecutivo(String operacion, String ic_if, String ic_ejecutivo)
		throws NafinException;
		
	public Vector consultaCatalogo(String idtipo,String tipo) 
		throws NafinException;

	public Vector consultaCatalogoCurvas(String clave) 
		throws NafinException;

	public boolean insertaCatalogo(String idtipo,String descripcion,String tipo) 
		throws NafinException;

	public boolean insertaCatalogoCurvas(String clave,String nombre,String tipo,String calculo,String base,String def_curv,String moneda,String swap) 
		throws NafinException;

	public Vector consultaArchivo(String ic_archivo) 
		throws NafinException;
	
	public boolean insertaArchivo(String nomArchivo,String descArchivo, String strPath) 
		throws NafinException;

	public boolean modificaArchivo(String icArchivo,String nomArchivo,String descArchivo, String strPath) 
		throws NafinException;

	public String eliminaArchivo(String ic_archivo) 
		throws NafinException;

/*
	public boolean eliminaCatalogoCurvas(String icCurva) 
		throws NafinException;
*/

	public String eliminaCatalogoCurvas(String icCurva)
		throws NafinException;		

	public boolean validaCurva(String nomCurva) 
		throws NafinException;

	public boolean actualizaSWAP(String idCurva) 
		throws NafinException;
		
	public boolean actualizaFondeo(String idCurva,String CsFondeo)
		throws NafinException;
		
	public boolean actualizaDepositos(String idCurva,String cveMoneda)
		throws NafinException;

	public boolean actualizaCurvaD(String idCurva,String cveMoneda)
		throws NafinException;
	
	public boolean existeDirector(String area, String areaAnterior, String antesDirector)
		throws NafinException;
	public String getArchivoTesoreria(String cveArchivo, String strPath) throws AppException;
    
    /**
     *�todo para generar el archivo  csv de la pantalla  
     * ADMIN TESORERIA/ Tasas de Cr�dito Nafin / Parametrizar Tesorer�a / Asignaci�n Ejecutivo al IF
     * @autor QC-DLHC  NE_2017_001 2017
     * @param datos
     * @return
     * @throws AppException
     */
    public String genArchAsignaEjecutivoIF(Map<String, Object> datos) throws AppException;
    
    /**
     * metodo para obtener los ejecutivos por Area e if
     * en la pantalla  ADMIN TESORERIA/ Tasas de Cr�dito Nafin / Parametrizar Tesorer�a / Asignaci�n Ejecutivo al IF
     * @autor QC-DLHC  NE_2017_001 2017
     * @param icArea
     * @param icIf
     * @return
     * @throws NafinException
     */
    public ArrayList getEjecutivosXAreaIF(String icArea, String icIf) throws NafinException;
    
    
}//interface MantenimientoCot