package com.netro.cotizador;


import com.netro.exception.NafinException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "MantenimientoCotEJB" , mappedName = "MantenimientoCotEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class MantenimientoCotBean implements MantenimientoCot {
	
        private String CADENA_CONEXION = "cadenasDS";
        
        private static final Log LOG = ServiceLocator.getInstance().getLog(MantenimientoCotBean.class);
        
	public String getDirectorXArea(String ic_area) throws NafinException {
		System.out.println("MantenimientoCotBean::getDirectorXArea(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		String 				rs_nombreEjec	= "";
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
					" SELECT cg_nombre || ' ' || cg_appaterno || ' ' || cg_apmaterno AS nombre"   +
					"   FROM cotcat_ejecutivo"   +
					"  WHERE cs_director = 'S'"   +
					"   AND ic_area = ?"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1, Integer.parseInt(ic_area));
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next()) {
					rs_nombreEjec	= rs.getString(1)==null?"":rs.getString(1);
				}//if(rs.next())
				rs.close();
				if(ps!=null) ps.close();
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::getDirectorXArea(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::getDirectorXArea(S)");
		}
		return rs_nombreEjec;
	}//getDirectorXArea
 
	public Vector getEjecutivosXArea(String ic_area) throws NafinException {
		System.out.println("MantenimientoCotBean::getEjecutivosXArea(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector				columnas		= null;
		Vector				renglones 		= new Vector();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
				" SELECT ic_ejecutivo, cg_nombre || ' ' || cg_appaterno || ' ' || cg_apmaterno AS nombre"   +
				"   FROM cotcat_ejecutivo"   +
				"  WHERE cs_director = 'N'"   +
				"    AND ic_area = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_area));
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				String rs_ejecutivo 	= rs.getString(1)==null?"":rs.getString(1);
				String rs_nombreEjec	= rs.getString(2)==null?"":rs.getString(2);
				columnas = new Vector();
				columnas.addElement(rs_ejecutivo);
				columnas.addElement(rs_nombreEjec);
				renglones.addElement(columnas);
			}//while(rs.next())
			rs.close();
			if(ps!=null) ps.close();
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::getEjecutivosXArea(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::getEjecutivosXArea(S)");
		}
		return renglones;
	}//getEjecutivosXArea

    /**
     *
     * @param icArea
     * @param icIF
     * @return
     * @throws NafinException
     */
    public ArrayList getIntermediariosFin(String icArea, String icIF) throws NafinException {
       LOG.info(" getIntermediariosFin(E)");
        AccesoDB con = new AccesoDB();     
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList columnas = new ArrayList();
        ArrayList renglones = new ArrayList();
        StringBuilder qrySentencia = new StringBuilder(); 
        List lVarBind = new ArrayList();
        
        try {
            
            con.conexionDB();
            
            lVarBind = new ArrayList();
            qrySentencia = new StringBuilder(); 
            
            qrySentencia.append(" SELECT cif.ic_if as icIF , cif.cg_razon_social as nombreIF " + 
                "   FROM comcat_if cif" +
                "  WHERE cif.idtipointermediario IS NOT NULL" + 
                "    AND NOT EXISTS(SELECT 1" +
                "                     FROM cotrel_if_ejecutivo" + 
                "                    WHERE ic_if = cif.ic_if)" );
                                
                if (!"".equals(icIF)  ) {
                    qrySentencia.append("    AND cif.ic_if = ? " );
                    lVarBind.add(icIF);
                }                
                qrySentencia.append(" UNION");
                qrySentencia.append(" SELECT cif.ic_if, cif.cg_razon_social" +
                "   FROM comcat_if cif, cotrel_if_ejecutivo cie, cotcat_ejecutivo eje" +
                "  WHERE cif.ic_if = cie.ic_if" + 
                "    AND cie.ic_ejecutivo = eje.ic_ejecutivo" +
                "    AND cif.idtipointermediario IS NOT NULL" + 
                "    AND eje.ic_area = ?" );
            
            lVarBind.add(icArea);
            
            if (!"".equals(icIF)  ) {
                qrySentencia.append("    AND cif.ic_if = ? " );
                lVarBind.add(icIF);
            }
                
               qrySentencia.append( "  ORDER BY 2");
            
            
            LOG.debug("qrySentencia.toString() " + qrySentencia.toString() ); 
            LOG.debug("lVarBind " + lVarBind ); 
            
            ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
            rs = ps.executeQuery();
            ps.clearParameters();
            while (rs.next()) {
                String rsIf = rs.getString("icIF") == null ? "" : rs.getString("icIF");
                String rsNombreIF = rs.getString("nombreIF") == null ? "" : rs.getString("nombreIF");
                columnas = new ArrayList();
                columnas.add(rsIf);
                columnas.add(rsNombreIF);
                renglones.add(columnas);
            } //while(rs.next())
            rs.close();
            if (ps != null)
                ps.close();
        } catch (Exception e) {
            LOG.error("MantenimientoCotBean::getIntermediariosFin(Exception) " + e);            
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            LOG.info(" getIntermediariosFin(S)");
        }
        return renglones;
    } //getIntermediariosFin
	
	public String getRelacionEjecutivoIF(String ic_ejecutivo, String ic_if) throws NafinException {
		System.out.println("MantenimientoCotBean::getRelacionEjecutivoIF(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		String				existeRel		= "N";
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
				" SELECT COUNT (1)"   +
				"   FROM cotrel_if_ejecutivo"   +
				"  WHERE ic_ejecutivo = ?"   +
				"    AND ic_if = ?"  ;
                    
                        LOG.debug ("qrySentencia  "+ qrySentencia +"\n ic_ejecutivo  "+ ic_ejecutivo +"   ic_if  "+ ic_if );
                    
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_ejecutivo));
			ps.setInt(2, Integer.parseInt(ic_if));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				int existe = rs.getInt(1);
				if(existe>0)
					existeRel = "S";
			}
			rs.close();
			if(ps!=null) ps.close();
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::getRelacionEjecutivoIF(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::getRelacionEjecutivoIF(S)");
		}
		return existeRel;
	}//getRelacionEjecutivoIF
	
	public void setRelacionEjecutivoIF(String operacion, String ic_if, String ic_ejecutivo) throws NafinException {
		System.out.println("MantenimientoCotBean::setRelacionEjecutivoIF(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		try {
			con = new AccesoDB();
			con.conexionDB();
			if("S".equals(operacion)) {
				qrySentencia = 
					" INSERT INTO cotrel_if_ejecutivo"   +
					"             (ic_ejecutivo, ic_if)"   +
					"      VALUES(?, ?)"  ;
			} else {
				qrySentencia = 
					" DELETE"   +
					"   FROM cotrel_if_ejecutivo"   +
					"  WHERE ic_ejecutivo = ?"   +
					"    AND ic_if = ?"  ;
			}
                    LOG.debug("qrySentencia  "+qrySentencia);
		    LOG.debug("ic_ejecutivo  "+ic_ejecutivo+" \n  ic_if  "+ic_if);
		    
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_ejecutivo));
			ps.setInt(2, Integer.parseInt(ic_if));
			ps.executeUpdate();
			if(ps!=null) ps.close();
			con.terminaTransaccion(true);
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::setRelacionEjecutivoIF(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			con.terminaTransaccion(false);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::setRelacionEjecutivoIF(S)");
		}
	}//setRelacionEjecutivoIF
	
	
         /**
         * Modificado por DLHC cequality
         * @param claveEjecutivo
         * @param claveIF
         * @param tipoAutorizacion
         * @param monto_mayor
         * @param cg_cot_linea
         * @param correos
         * @throws NafinException
         */
	public void actualizaDatosAutorizacion(String claveEjecutivo, String claveIF,String tipoAutorizacion,String montoMayor, String cgCotLinea, String correos) throws NafinException {

            LOG.info("actualizaDatosAutorizacion(E)");
            AccesoDB  con = new AccesoDB();
	    StringBuilder qrySentencia = new StringBuilder(); 
	    StringBuilder qrySentencia2 = new StringBuilder();             
            PreparedStatement 	ps = null;
            ResultSet	rs= null;
	    List lVarBind = new ArrayList();
            try {
                
                con.conexionDB();
		 
                
		qrySentencia = new StringBuilder();  
                qrySentencia.append(" UPDATE cotrel_if_ejecutivo "+
                                    " SET cg_solicitar_aut = ?, "+
                                    " CG_CORREOS  =  ?  , "+
                                    " fn_monto_mayor_a = ?,  "+
                                    " cg_cot_linea = ? "+
                                    " WHERE  ic_if = ? "+
                                    " AND ic_ejecutivo = ? ") ;
							
                lVarBind = new ArrayList();	
                lVarBind.add(tipoAutorizacion);
                lVarBind.add(correos);
                
                if (!"".equals(montoMayor)  ) {
                   lVarBind.add(montoMayor);
                } else  {
                    lVarBind.add(""); 
                }
                
                if(cgCotLinea!=null){
                    lVarBind.add(cgCotLinea);
                } else  {
                    lVarBind.add(""); 
                }
                lVarBind.add(claveIF);	
                lVarBind.add(claveEjecutivo);
                
		LOG.debug("qrySentencia"+qrySentencia.toString());
                LOG.debug("lVarBind"+lVarBind);
                
		ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
		ps.executeUpdate();
                ps.clearParameters();
		ps.close();
		
                
                qrySentencia = new StringBuilder();  
		qrySentencia.append(" select se.ic_solic_esp,ie.ic_ejecutivo from cotrel_if_ejecutivo ie,"+
					" cot_solic_esp se"+
					" where ie.ic_if = se.ic_if"+
					" and ie.ic_ejecutivo != se.ic_ejecutivo");
                
                LOG.debug("qrySentencia"+qrySentencia.toString());
                
                ps = con.queryPrecompilado(qrySentencia.toString());
		rs = ps.executeQuery();
                ps.clearParameters();
                while(rs.next()){  
                    
                    qrySentencia2 = new StringBuilder(); 
                    qrySentencia2.append(" update cot_solic_esp "+
                                         " set ic_ejecutivo = ? "+
				         " where ic_solic_esp = ?");
		    lVarBind = new ArrayList();
		    lVarBind.add(rs.getInt("ic_ejecutivo"));
		    lVarBind.add(rs.getInt("ic_solic_esp"));
		    
                    //log.debug("qrySentencia2"+qrySentencia2.toString()+"  \n  lVarBind"+lVarBind);
                    
                    PreparedStatement 	ps2=null;
                    ps2 = con.queryPrecompilado(qrySentencia2.toString(), lVarBind);
                    ps2.execute();
		    ps2.clearParameters();
		    ps2.close();
		    
		}
                
		rs.close();
		ps.close();
		con.terminaTransaccion(true);
                
                
            }catch(Exception e){
                LOG.error(" actualizaDatosAutorizacion(Exception) "+e);
		throw new NafinException("SIST0001");
            }finally {
                con.terminaTransaccion(false);
		if(con.hayConexionAbierta())
                    con.cierraConexionDB();
		LOG.info("actualizaDatosAutorizacion(S)");
            }	
	}
	
	
	

	
	
	//public void guardaEjecutivo(HttpServletRequest request, String ic_usuario) throws NafinException {
	public void guardaEjecutivo(String ic_usuario, String nombre, String apellido_paterno,
								String apellido_materno, String email, String telefono, String fax,
								String area,String director) throws NafinException {
				
		System.out.println("MantenimientoCotBean::guardaEjecutivo(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		int ic_ejecutivo = 0;

		try {
			con = new AccesoDB();
			con.conexionDB();
			String qry = " select nvl(max(IC_EJECUTIVO),0)+1 as sig_ic_ejecutivo from COTCAT_EJECUTIVO ";
			ps = con.queryPrecompilado(qry);
			rs = ps.executeQuery();
			if(rs.next()){
				ic_ejecutivo = rs.getInt("sig_ic_ejecutivo");
			}
									
			qrySentencia = 
						" INSERT INTO COTCAT_EJECUTIVO ( "+
						"   IC_EJECUTIVO, IC_USUARIO, CG_NOMBRE, "+
						"   CG_APPATERNO, CG_APMATERNO, CG_MAIL, "+
						"   CG_TELEFONO, CG_FAX, IC_AREA, "+
						"   CS_DIRECTOR) "+
						" VALUES ( ?,?,?,?,?,?,?,?,?,?) ";
							
			ps = con.queryPrecompilado(qrySentencia);
			
			ps.setInt(1, ic_ejecutivo);
			ps.setString(2, ic_usuario);
			ps.setString(3, nombre);
			ps.setString(4, apellido_paterno);
			ps.setString(5, apellido_materno);
			ps.setString(6, email);
			ps.setString(7, telefono);
			ps.setString(8, fax);
			ps.setString(9, area);
			ps.setString(10, director);
			ps.execute();
			if(ps!=null) ps.close();
			con.terminaTransaccion(true);
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::guardaEjecutivo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			con.terminaTransaccion(false);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::guardaEjecutivo(S)");
		}
	}//guardaEjecutivo

	public void modificaEjecutivo(int ic_ejecutivo, String nombre, String apellido_paterno,
								String apellido_materno, String email, String telefono, String fax,
								String area,String director) throws NafinException {
				
		System.out.println("MantenimientoCotBean::modificaEjecutivo(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;

		try {
			con = new AccesoDB();
			con.conexionDB();
									
			qrySentencia = 
						"  UPDATE COTCAT_EJECUTIVO SET "+
					 	"	cg_nombre = ?,cg_appaterno = ? ,cg_apmaterno = ? ,cg_mail = ? , "+
					 	"	cg_telefono = ?, cg_fax = ? ,ic_area = ? ,cs_director = ? "+
						"	WHERE ic_ejecutivo = ? ";
							
			ps = con.queryPrecompilado(qrySentencia);
			
			ps.setString(1,nombre);
			ps.setString(2,apellido_paterno);
			ps.setString(3,apellido_materno);
			ps.setString(4,email);
			ps.setString(5,telefono);
			ps.setString(6,fax);
			ps.setString(7,area);
			ps.setString(8,director);
			ps.setInt(9,ic_ejecutivo);
			ps.executeUpdate();
			if(ps!=null) ps.close();
			con.terminaTransaccion(true);
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::modificaEjecutivo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::modificaEjecutivo(S)");
		}
	}//guardaEjecutivo
	
	public Vector obtenDatosEjecutivo(int ic_ejecutivo) throws NafinException {
				
		System.out.println("MantenimientoCotBean::obtenDatosEjecutivo(E)");
		AccesoDB			con				= null;
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector VdatosEjecutivo = new Vector();
		
		try {
			con = new AccesoDB();
			con.conexionDB();
			String qry = " SELECT IC_EJECUTIVO, IC_USUARIO, CG_NOMBRE, "+
						 " CG_APPATERNO, CG_APMATERNO, CG_MAIL, "+
						 " CG_TELEFONO, CG_FAX, IC_AREA, "+
						 " CS_DIRECTOR FROM COTCAT_EJECUTIVO "+ 
						 " WHERE IC_EJECUTIVO = ?";
			ps = con.queryPrecompilado(qry);
			ps.setInt(1, ic_ejecutivo);
			rs = ps.executeQuery();
			if(rs.next()){
				VdatosEjecutivo.add(rs.getString(1));
				VdatosEjecutivo.add(rs.getString(2));
				VdatosEjecutivo.add(rs.getString(3));
				VdatosEjecutivo.add(rs.getString(4));
				VdatosEjecutivo.add(rs.getString(5));
				VdatosEjecutivo.add(rs.getString(6));
				VdatosEjecutivo.add(rs.getString(7));
				VdatosEjecutivo.add(rs.getString(8));
				VdatosEjecutivo.add(rs.getString(9));
				VdatosEjecutivo.add(rs.getString(10));	
			}
			if(ps!=null) ps.close();
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::obtenDatosEjecutivo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::obtenDatosEjecutivo(S)");
		}
		return VdatosEjecutivo;
	}//obtenDatosEjecutivo

	public Vector getDatosAutorizacion(String ic_ejecutivo, String ic_if) throws NafinException {
		System.out.println("MantenimientoCotBean::getRelacionEjecutivoIF(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector				VDatosSolicAut  = new Vector();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
						" SELECT cg_solicitar_aut, fn_monto_mayor_a, cg_cot_linea , CG_CORREOS, ic_ejecutivo  "+
						"  FROM cotrel_if_ejecutivo "+						
						" WHERE ic_if = ? ";
			
			ps = con.queryPrecompilado(qrySentencia);			
			ps.setInt(1, Integer.parseInt(ic_if));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				VDatosSolicAut.add(rs.getString("cg_solicitar_aut"));
				VDatosSolicAut.add(rs.getString("fn_monto_mayor_a"));
				VDatosSolicAut.add(rs.getString("cg_cot_linea"));
			        VDatosSolicAut.add(rs.getString("CG_CORREOS"));
                                VDatosSolicAut.add(rs.getString("ic_ejecutivo"));
			}
			rs.close();
			if(ps!=null) ps.close();
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::getDatosAutorizacion(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::getDatosAutorizacion(S)");
		}
		return VDatosSolicAut;
	}//getRelacionEjecutivoIF

	public String revisarEstatusSolicEjecutivo(String operacion, String ic_if, String ic_ejecutivo) throws NafinException {
	        System.out.println("MantenimientoCotBean::revisarEstatusSolicEjecutivo(E)");
	        AccesoDB                        con                             = null;
	        String                          qrySentencia    = "";
	        PreparedStatement       ps                              = null;
	        ResultSet                       rs                              = null;
	        String                          ic_ejecutivo_relacion = "";
	        String                          Regresacadena = "";
	        try {
	                con = new AccesoDB();
	                con.conexionDB();
	                if("S".equals(operacion)) { //caso insercion
	                        qrySentencia = " select ic_ejecutivo from cotrel_if_ejecutivo where ic_if = ? ";                                
	                        ps = con.queryPrecompilado(qrySentencia);
	                        ps.setInt(1, Integer.parseInt(ic_if));
	                        rs = ps.executeQuery();
	                        if(rs.next()){ //Ya existe una relacion entre el intermediario (IF) con un ejecutivo, por lo que
	                                                   //se determinara si se puede eliminar la relacion entre estos, de acuerdo a al estatus de las solicitudes
	                                ic_ejecutivo_relacion = rs.getString("ic_ejecutivo");
	                        }
	                        rs.close();
	                        ps.close();
	                }//"S".equals(operacion)
	                if("N".equals(operacion) || !ic_ejecutivo_relacion.equals("")){ //para el caso en que se desea eliminar
	                                                                                                                                                //una relacion o se encontro una entre 
	                                                                                                                                                //el intermediario y otro ejecutivo. Por lo tanto se verificara si se puede eliminar esta relacion
	                        qrySentencia = 
	                                                " SELECT DISTINCT cse.ic_ejecutivo, "+
	                                                "                (ce.cg_nombre || ' ' || ce.cg_appaterno || ' '|| ce.cg_apmaterno) AS ejecutivo, "+
	                                                "                cse.ic_estatus "+
	                                                "           FROM cot_solic_esp cse, cotcat_ejecutivo ce "+
	                                                "          WHERE cse.ic_ejecutivo = ? "+
	                                                "            AND cse.ic_if = ? "+
	                                                "            AND cse.ic_ejecutivo = ce.ic_ejecutivo "+
	                                                "            AND cse.ic_estatus IN ('1', '2', '4') ";//1-Solicitada, 2-Cotizada, 4-En proceso
	                        
	                        ic_ejecutivo = (!ic_ejecutivo_relacion.equals(""))?ic_ejecutivo_relacion:ic_ejecutivo;
	                        
	                        ps = con.queryPrecompilado(qrySentencia);
	                        ps.setInt(1, Integer.parseInt(ic_ejecutivo));
	                        ps.setInt(2, Integer.parseInt(ic_if));
	                        
	                        rs = ps.executeQuery();
	                        if(rs.next()){//NO se puede cambiar la relacion de este ejecutivo con este intermediario, 
	                                                  //ya que el ejecutivo tiene solicitudes en alguno(s) de los  estatus (1,2 o 4)
	                                Regresacadena = rs.getString("ejecutivo");                                              
	                        }       
	                        if(ps!=null) ps.close();
	                }//"N".equals(operacion)
	                
	                con.terminaTransaccion(true);
	        }catch(Exception e){
	                con.terminaTransaccion(false);
	                System.out.println("MantenimientoCotBean::revisarEstatusSolicEjecutivo(Exception) "+e);
	                e.printStackTrace();
	                throw new NafinException("SIST0001");
	        }finally {
	                if(con.hayConexionAbierta())
	                        con.cierraConexionDB();
	                System.out.println("MantenimientoCotBean::revisarEstatusSolicEjecutivo(S)");
	        }
	        return Regresacadena;
	}//setRelacionEjecutivoIF
	
	public Vector consultaCatalogo(String idtipo,String tipo) throws NafinException {
		System.out.println("MantenimientoCotBean::consultaCatalogo(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector				columnas		= null;
		Vector				renglones 		= new Vector();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia = 
				" select idtipointermediario,descripcion,cg_tipo "+
				" from tipointermediario ";
			if(!"".equals(idtipo))
				qrySentencia += " where idtipointermediario = ?";
			else if(!"".equals(tipo))
				qrySentencia += " where cg_tipo = ?";
			qrySentencia += " order by 3 desc,1";
			System.out.println("qrySentencia "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			if(!"".equals(idtipo))
				ps.setString(1,idtipo);
			else if(!"".equals(tipo))
				ps.setString(1,tipo);
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				String rs_id 	= rs.getString(1)==null?"":rs.getString(1);
				String rs_desc 	= rs.getString(2)==null?"":rs.getString(2);
				String rs_tipo	= rs.getString(3)==null?"":rs.getString(3);
				columnas = new Vector();
				columnas.addElement(rs_id);
				columnas.addElement(rs_desc);
				columnas.addElement(rs_tipo);
				renglones.addElement(columnas);
			}//while(rs.next())
			rs.close();
			if(ps!=null) ps.close();
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::consultaCatalogo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::consultaCatalogo(S)");
		}
		return renglones;
	}//consultaCatalogo

	public Vector consultaCatalogoCurvas(String clave) throws NafinException {
		System.out.println("MantenimientoCotBean::consultaCatalogoCurvas(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector				columnas		= null;
		Vector				renglones 		= new Vector();
		try {
			con = new AccesoDB();
			con.conexionDB();
			
			qrySentencia = " SELECT c.idcurva, c.nombre, c.tipo, c.modocalculo, +c.base, c.curvadefault,"   +
						   "        m.cd_nombre, c.ic_moneda, c.cs_swap, c.cs_fondeo AS fondeo,"   +
						   "        c.cs_deposito AS deposito, c.cg_curva_if as cg_curva_if "   +
						   "   FROM curva c, comcat_moneda m"   +
						   "  WHERE c.ic_moneda = m.ic_moneda"  ;
			
			if(!"".equals(clave))
				qrySentencia += " and idcurva = ? ";
			
			qrySentencia += " order by 8,1";
			System.out.println(":: qrySentencia  ::"+qrySentencia);   
			ps = con.queryPrecompilado(qrySentencia);
			
			if(!"".equals(clave))
				ps.setString(1,clave);
			
			rs = ps.executeQuery();
			ps.clearParameters();
			
			while(rs.next()) {
				String rs_id 	= (rs.getString(1)==null)?"":rs.getString(1);
				String rs_nom 	= (rs.getString(2)==null)?"":rs.getString(2);
				String rs_tipo 	= (rs.getString(3)==null)?"":rs.getString(3);
				String rs_calc 	= (rs.getString(4)==null)?"":rs.getString(4);
				String rs_base 	= (rs.getString(5)==null)?"":rs.getString(5);
				String rs_def	= (rs.getString(6)==null)?"":rs.getString(6);
				String rs_mon	= (rs.getString(7)==null)?"":rs.getString(7);
				String rs_icmon	= (rs.getString(8)==null)?"":rs.getString(8);
				String rs_swap	= (rs.getString(9)==null)?"":rs.getString(9);
				String rs_fondeo= (rs.getString("fondeo")==null)?"":rs.getString("fondeo");
				String rs_deposito= (rs.getString("deposito")==null)?"":rs.getString("deposito");
				String rs_curva_if= (rs.getString("cg_curva_if")==null)?"":rs.getString("cg_curva_if");
				
				columnas = new Vector();
				columnas.addElement(rs_id);
				columnas.addElement(rs_nom);
				columnas.addElement(rs_tipo);
				columnas.addElement(rs_calc);
				columnas.addElement(rs_base);
				columnas.addElement(rs_def);
				columnas.addElement(rs_mon);
				columnas.addElement(rs_icmon);
				columnas.addElement(rs_swap);
				columnas.addElement(rs_fondeo);
				columnas.addElement(rs_deposito);
				columnas.addElement(rs_curva_if);
			
				renglones.addElement(columnas);
			}//while(rs.next())
			
			rs.close();
			if(ps!=null) ps.close();
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::consultaCatalogoCurvas(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::consultaCatalogoCurvas(S)");
		}
		return renglones;
	}//consultaCatalogo

	public boolean insertaCatalogo(String idtipo,String descripcion,String tipo) 
	throws NafinException {
		System.out.println("MantenimientoCotBean::insertaCatalogo(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		String				idobtenido		= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		boolean				resultado		= true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			if(!"".equals(idtipo)){
				System.out.println("Entrando Update idtipo "+ idtipo);
				System.out.println("Entrando Update descripcion "+ descripcion);
				System.out.println("Entrando Update tipo "+ tipo);
				qrySentencia = 
					" UPDATE tipointermediario "+
	    			" SET descripcion = ?,cg_tipo = ? "+
	    			" WHERE idtipointermediario = ? ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,descripcion);
				ps.setString(2,tipo);
				ps.setString(3,idtipo);
				ps.executeUpdate();
			} else {
				System.out.println("Entrando Insert");
				qrySentencia="select NVL(max(idtipointermediario),0)+1 as Clave "+
							"from tipointermediario";
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next())
					idobtenido = rs.getString("Clave");
				rs.close();
				if(ps!=null) ps.close();
				
				qrySentencia = 
					" INSERT INTO tipointermediario "+
	    			" VALUES (?,?,?) ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,idobtenido);
				ps.setString(2,descripcion);
				ps.setString(3,tipo);
				ps.execute();
				rs.close();
			}
			if(ps!=null) ps.close();
			con.terminaTransaccion(true);
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::insertaCatalogo(Exception) "+e);
			e.printStackTrace();
			resultado = false;
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::insertaCatalogo(S)");
		}
		return resultado;
	}//insertaCatalogo
	
	public boolean insertaCatalogoCurvas(String clave,String nombre,String tipo,String calculo,
										 String base,String def_curv,String moneda,String swap) 
	throws NafinException {
		System.out.println("MantenimientoCotBean::insertaCatalogoCurvas(E)");
		AccesoDB			con				= null;
		String				qrySentencia	= "";
		String				idobtenido		= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		boolean				resultado		= true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			if(!"".equals(clave)){
				System.out.println("Entrando Update clave "+ clave);
				System.out.println("Entrando Update nombre "+ nombre);
				System.out.println("Entrando Update tipo "+ tipo);
				System.out.println("Entrando Update calculo "+ calculo);
				System.out.println("Entrando Update base "+ base);
				System.out.println("Entrando Update default "+ def_curv);
				System.out.println("Entrando Update moneda "+ moneda);
				qrySentencia = 
					" UPDATE curva "+
	    			" SET nombre = ?, tipo = ?, modocalculo = ?, base = ?, "+
	    			"     curvadefault = ?, ic_moneda = ?, cs_swap = ? "+
	    			" WHERE idcurva = ? ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,nombre);
				ps.setString(2,tipo);
				ps.setString(3,calculo);
				ps.setString(4,base);
				ps.setString(5,def_curv);
				ps.setString(6,moneda);
				if(moneda.equals("1"))
					ps.setString(7,"N");
				else
					ps.setString(7,swap);
				ps.setString(8,clave);
				ps.executeUpdate();
			} else {
				System.out.println("Entrando Insert");
				qrySentencia= "select NVL(max(idcurva),0)+1 as Clave "+
							"from curva";
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();
				ps.clearParameters();
				if(rs.next())
					idobtenido = rs.getString("Clave");
				rs.close();
				if(ps!=null) ps.close();
				
				qrySentencia = 
					" INSERT INTO CURVA(IDCURVA,NOMBRE,TIPO,MODOCALCULO,BASE,CURVADEFAULT,IC_MONEDA,CS_SWAP) "+
	    			" VALUES (?,?,?,?,?,?,?,?) ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,idobtenido);
				ps.setString(2,nombre);
				ps.setString(3,tipo);
				ps.setString(4,calculo);
				ps.setString(5,base);
				ps.setString(6,def_curv);
				ps.setString(7,moneda);
				if(moneda.equals("1"))
					ps.setString(8,"N");
				else
					ps.setString(8,swap);
				ps.execute();
				rs.close();
			}

			if(ps!=null) ps.close();
			con.terminaTransaccion(true);
		}catch(Exception e){
			System.out.println("MantenimientoCotBean::insertaCatalogoCurvas(Exception) "+e);
			e.printStackTrace();
			resultado = false;
			throw new NafinException("SIST0001");
		}finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoCotBean::insertaCatalogoCurvas(S)");
		}
		return resultado;
	}//insertaCatalogo
	
	public Vector consultaArchivo(String ic_archivo) 
	throws NafinException{
		System.out.println("MantenimientoCotBean::consultaArchivo (E)");
		String qrySentencia = "";
		Vector	vecFilas = new Vector();
		Vector	vecColumnas = null;
		AccesoDB ad = new AccesoDB();
		ResultSet rs = null;
        try{
			ad.conexionDB();
            qrySentencia =  " SELECT IC_PARAM_ARCHIVOS AS CLAVE, "+
               				" CG_NOMBRE_ARCHIVO AS NOMBRE, "+
							" CG_DESCRIPCION AS DESCRIPCION "+
							" FROM COT_PARAM_ARCHIVOS"  ;

			if(!"".equals(ic_archivo))
              	qrySentencia += " WHERE IC_PARAM_ARCHIVOS = " + ic_archivo;
            System.out.println("Query: "+qrySentencia);
            rs=ad.queryDB(qrySentencia);

            while(rs.next()){
                vecColumnas = new Vector();
	/*0*/		vecColumnas.add((rs.getString("CLAVE")==null)?"":rs.getString("CLAVE"));
	/*1*/		vecColumnas.add((rs.getString("NOMBRE")==null)?"":rs.getString("NOMBRE"));
	/*2*/		vecColumnas.add((rs.getString("DESCRIPCION")==null)?"":rs.getString("DESCRIPCION"));
	            vecFilas.add(vecColumnas);
	        }
	            
	        ad.cierraStatement();
		}catch(SQLException e) {
			System.out.println("MantenimientoCotBean: Error en la consulta de datos");		
			e.printStackTrace();
	    }catch(Exception e){
			System.out.println("MantenimientoCotBean::consultaArchivo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			System.out.println("MantenimientoCotBean::consultaArchivo (S)");
			if(ad.hayConexionAbierta())
			ad.cierraConexionDB();
	    }
	   return vecFilas;
	}//consultaArchivo

	public boolean insertaArchivo(String nomArchivo,String descArchivo, String strPath) 
	throws NafinException{
		String strInsert="";
		boolean salida=true;
		AccesoDB ad = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ad.conexionDB();
		    String archivo = "";
		      
		    strInsert = "SELECT ic_param_archivos FROM COT_PARAM_ARCHIVOS "+
						"WHERE CG_NOMBRE_ARCHIVO = ? ";
		    System.out.println(strInsert);
			
			ps = ad.queryPrecompilado(strInsert);
			ps.setString(1, nomArchivo);
			rs= ps.executeQuery();
		    if(rs.next())
		    	archivo = rs.getString(1);
			rs.close();
			ps.close();
			
			System.out.println("Existe el archivo:" + archivo);
	        ad.cierraStatement();

		    if("".equals(archivo)){
		    	archivo = getClaveArchivo(ad);
		        strInsert = "INSERT INTO COT_PARAM_ARCHIVOS "+
		              		"VALUES( ?, ?, ?, ? )";
		        System.out.println(strInsert);
					
		        try {
					
					File farchivo = new File(strPath+nomArchivo);
					FileInputStream fis = new FileInputStream(farchivo);
					
					ps = ad.queryPrecompilado(strInsert);
					ps.setInt(1, Integer.parseInt(archivo));
					ps.setString(2, nomArchivo);
					ps.setString(3, descArchivo);
					ps.setBinaryStream(4, fis, (int)farchivo.length());
					ps.executeUpdate();
					ps.close();
					fis.close();
					
		            System.out.println("Se insertó el archivo:" + nomArchivo);
           			//ad.terminaTransaccion(true);
		            salida=true;
		        } catch(SQLException e) {
		            System.out.println("MantenimientoCotBean: Error en la inserción de datos");
					e.printStackTrace();
		            salida=false;
		        }		
			} else
		    	salida = modificaArchivo(archivo,nomArchivo,descArchivo, strPath);  	
	    }catch(Exception e){
			System.out.println("MantenimientoCotBean::insertaArchivo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
		 	if(ad.hayConexionAbierta()){
				ad.terminaTransaccion(salida);
				ad.cierraConexionDB();
			}
		}

			return salida;
	}//insertaArchivo


  	private String getClaveArchivo(AccesoDB ad) throws NafinException{
		System.out.println("MantenimientoCotBean::getClaveArchivo (E)");
      	String maxClave = "";
		String qrySentencia="select NVL(max(ic_param_archivos),0)+1 as Clave "+
							"from cot_param_archivos";
	    ResultSet rs = null;
	    try{
			rs = ad.queryDB(qrySentencia);
			if(rs.next())
				maxClave = rs.getString("CLAVE");
        	System.out.println("Clave de Archivo: "+maxClave);
          	ad.cierraStatement();
		} catch(SQLException e) {
			System.out.println("MantenimientoCotBean: Error en la consulta de datos");		
			e.printStackTrace();
        }
		System.out.println("MantenimientoCotBean::getClaveArchivo (S)");
        return maxClave;
	}//getClaveArchivo
	
	public boolean modificaArchivo(String icArchivo,String nomArchivo,String descArchivo, String strPath) 
	throws NafinException{
		String strInsert="";
		boolean salida = true;
		AccesoDB ad = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ad.conexionDB();
		    String condicion = "";
		      
		   	if(!"".equals(nomArchivo))
		    	condicion = ", CG_NOMBRE_ARCHIVO = ? ";
		    
		    strInsert = " UPDATE COT_PARAM_ARCHIVOS "+
		                " SET CG_DESCRIPCION = ?, bi_archivo = ?  " + condicion +
		                " WHERE IC_PARAM_ARCHIVOS = ? ";
		    System.out.println(strInsert);
					
		    try {
				File farchivo = new File(strPath+nomArchivo);
				FileInputStream fis = new FileInputStream(farchivo);
				int param = 0;
				
				ps = ad.queryPrecompilado(strInsert);
				ps.setString(++param, descArchivo);
				ps.setBinaryStream(++param, fis, (int)farchivo.length());
				if(!"".equals(nomArchivo))
					ps.setString(++param, nomArchivo);
				ps.setInt(++param, Integer.parseInt(icArchivo));
				ps.executeUpdate();
				ps.close();
				fis.close();
				
			    System.out.println("Se actualizó el archivo:" + icArchivo);
			    salida=true;
           		//ad.terminaTransaccion(true);
			} catch(SQLException e) {
			    System.out.println("MantenimientoCotBean: Error en la actualización de datos");
				e.printStackTrace();
			    salida=false;
			}		
	    }catch(Exception e){
			System.out.println("MantenimientoCotBean::modificaArchivo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
		 	if(ad.hayConexionAbierta()){
				ad.terminaTransaccion(salida);
				ad.cierraConexionDB();
			}
		}
			return salida;
	}//modificaArchivo

	public String eliminaArchivo(String ic_archivo) 
	throws NafinException{
		AccesoDB ad = new AccesoDB();
		ResultSet rs = null;
		String strInsert="";
      	String nom_archivo = "";
		try{
			ad.conexionDB();
      
     		strInsert = "SELECT CG_NOMBRE_ARCHIVO FROM COT_PARAM_ARCHIVOS "+
        				"WHERE IC_PARAM_ARCHIVOS = " + ic_archivo;

        	System.out.println(strInsert);
			
			try{
				rs=ad.queryDB(strInsert);
        		if(rs.next())
          			nom_archivo = rs.getString(1);
				System.out.println("Existe el archivo:" + nom_archivo);
		        ad.cierraStatement();
			} catch(SQLException e) {
				System.out.println("MantenimientoCotBean: Error en la consulta de base de datos");
				e.printStackTrace();
			}		
      	
      		strInsert = " DELETE FROM COT_PARAM_ARCHIVOS " +
                  		" WHERE IC_PARAM_ARCHIVOS = " + ic_archivo;
			System.out.println(strInsert);
			
			try{
				ad.ejecutaSQL(strInsert);
        		System.out.println("Archivo eliminado:" + nom_archivo);
           		ad.terminaTransaccion(true);
			} catch(SQLException e) {
				System.out.println("MantenimientoCotBean: Error en la eliminación de datos");
				e.printStackTrace();
			}		
	    }catch(Exception e){
			System.out.println("MantenimientoCotBean::eliminaArchivo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
		 	if(ad.hayConexionAbierta())
				ad.cierraConexionDB();
		}

		return nom_archivo;
	}//eliminaArchivo
	
	public String getArchivoTesoreria(String cveArchivo, String strPath) throws AppException
	{
		String strPathFile = "";
		String nombreArchivo = "";
		String strInsert="";
		AccesoDB ad = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ad.conexionDB();

		    strInsert = " SELECT bi_archivo, CG_NOMBRE_ARCHIVO FROM COT_PARAM_ARCHIVOS "+
		                " WHERE IC_PARAM_ARCHIVOS = ? ";
		    System.out.println(strInsert);
					
			int param = 0;
			
			ps = ad.queryPrecompilado(strInsert);
			ps.setInt(++param, Integer.parseInt(cveArchivo));
			rs = ps.executeQuery();
			
			if(rs.next()){
				// Asignar nombre al archivo
				nombreArchivo = rs.getString("CG_NOMBRE_ARCHIVO");
				strPathFile = strPath + nombreArchivo;
				FileOutputStream outputStream	= new FileOutputStream( strPathFile );
				
				// Guardar archivo en disco
				Blob blob = rs.getBlob("bi_archivo");
				InputStream inputStream = blob.getBinaryStream();
				byte[]   buffer      = new byte [ 4 * 1024 ]; // 4 KB 
				int      bytesRead   = 0;
				while(  ( bytesRead  = inputStream.read( buffer ) )  != -1 ){
					outputStream.write( buffer, 0, bytesRead );
					outputStream.flush();
				}
				
				outputStream.close();
				inputStream.close();
				
			}
			rs.close();
			ps.close();
			
			System.out.println("Se actualizó el archivo:" + cveArchivo);

	    }catch(Exception e){
			System.out.println("MantenimientoCotBean::modificaArchivo(Exception) "+e);
			e.printStackTrace();
			throw new AppException("Error al obtener el archivo de tesoreria");
		} finally{
		 	if(ad.hayConexionAbierta()){
				ad.cierraConexionDB();
			}
		}
		
		return nombreArchivo;
		
	}
/*	
	public boolean eliminaCatalogoCurvas(String icCurva) 
	throws NafinException{
		AccesoDB ad = new AccesoDB();
		ResultSet rs = null;
		String strInsert="";
		boolean	resultado = true;
		try{
			System.out.println("MantenimientoCotBean::eliminaCatalogoCurvas(E)");
			ad.conexionDB();
      
     		strInsert = " DELETE FROM CURVA " +
                  		" WHERE IDCURVA = " + icCurva;
			System.out.println(strInsert);
			
			try{
				ad.ejecutaSQL(strInsert);
        		ad.terminaTransaccion(true);
			} catch(SQLException e) {
				resultado = false;
				System.out.println("MantenimientoCotBean: Error en la eliminación de datos");
				e.printStackTrace();
			}		
	    }catch(Exception e){
	    	resultado = false;
			System.out.println("MantenimientoCotBean::eliminaArchivo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			System.out.println("MantenimientoCotBean::eliminaCatalogoCurvas(S)");
		 	if(ad.hayConexionAbierta())
				ad.cierraConexionDB();
		}

		return resultado;
	}//eliminaCatalogoCurvas
*/
	public String eliminaCatalogoCurvas(String icCurva) 
	throws NafinException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean bok = true;
		String Mensaje = "";
		try{
			System.out.println(":eliminaCatalogoCurvas(E)");
		
			con.conexionDB();
			String qryDatosCurva = " SELECT IDCURVA FROM DATOSCURVA WHERE IDCURVA= ? ";
			
			System.out.println("qryDatosCurva :: "+qryDatosCurva);
			System.out.println("icCurva :: "+icCurva);
			
			ps = con.queryPrecompilado(qryDatosCurva);
			ps.setString(1,icCurva);
			rs = ps.executeQuery();	
			//ps.clearParameters();
			System.out.println(":::: Mensaje "+Mensaje);
        	if(rs.next()){
				Mensaje = "No se puede eliminar el archivo ya que\\n existe información relacionada a este";
			}
			ps.close();
			rs.close();
			
			qryDatosCurva = " select count(*) as NUMREGISTRO from COT_CALC_USD where IDCURVA = ? ";
			
			System.out.println("qryDatosCurva :: "+qryDatosCurva);   
			System.out.println("icCurva :: "+icCurva);    
			ps = con.queryPrecompilado(qryDatosCurva);
			ps.setString(1,icCurva);
			rs = ps.executeQuery();	    
			//ps.clearParameters();
        	if(rs.next()){
				if(!rs.getString("NUMREGISTRO").equals("0")){  
					Mensaje = "No se puede eliminar el archivo ya que\\n existe información relacionada a este";
				}
			}
			ps.close();   
			rs.close();       
			
			qryDatosCurva = " select count(*) as NUMREGISTRO from CREDITO where IDCURVADURACION = ? ";
			
			System.out.println("qryDatosCurva :: "+qryDatosCurva);   
			System.out.println("icCurva :: "+icCurva);    
			ps = con.queryPrecompilado(qryDatosCurva);
			ps.setString(1,icCurva);   
			rs = ps.executeQuery();	    
			//ps.clearParameters();   
        	if(rs.next()){
				if(!rs.getString("NUMREGISTRO").equals("0")){  
					Mensaje = "No se puede eliminar el archivo ya que\\n existe información relacionada a este";
				}
			}
			ps.close();
			rs.close();
			
			qryDatosCurva = " select count(*) as NUMREGISTRO from CREDITO where  IDCURVACOTIZACION = ? ";
			
			System.out.println("qryDatosCurva :: "+qryDatosCurva);   
			System.out.println("icCurva :: "+icCurva);    
			ps = con.queryPrecompilado(qryDatosCurva);
			ps.setString(1,icCurva);   
			rs = ps.executeQuery();	    
			//ps.clearParameters();   
        	if(rs.next()){
				if(!rs.getString("NUMREGISTRO").equals("0")){  
					Mensaje = "No se puede eliminar el archivo ya que\\n existe información relacionada a este";
				}
			}
			ps.close();
			rs.close();
			   
			   
			System.out.println(":::: Mensaje"+Mensaje);           
			if(Mensaje.equals("")){
	     		String qryBorraCurva = " DELETE FROM CURVA " +   
	                  				   " WHERE IDCURVA = ? ";
				System.out.println(":::: qryBorraCurva ::  "+qryBorraCurva);	  
				System.out.println(":::: icCurva ::  "+icCurva);	
				ps = con.queryPrecompilado(qryBorraCurva);    
				ps.setString(1,icCurva);
				ps.executeUpdate();
			}
			ps.close();    
			con.terminaTransaccion(true);
			System.out.println(":::: salida ::::  ");	
	    }catch(Exception e){
	    	bok = false;
			System.out.println("MantenimientoCotBean::eliminaCatalogoCurvas(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			System.out.println("MantenimientoCotBean::eliminaCatalogoCurvas(S)");
		 	if(con.hayConexionAbierta())
				con.terminaTransaccion(bok);
				con.cierraConexionDB();
		}
		
		return Mensaje;
	}//eliminaCatalogoCurvas

	public boolean validaCurva(String nomCurva) 
	throws NafinException{
		AccesoDB ad = new AccesoDB();
		ResultSet rs = null;
		String query = "";
		boolean	resultado = true;
		try{
			System.out.println("MantenimientoCotBean::validaCurva(E)");
			ad.conexionDB();
      
     		query = " SELECT 1 FROM CURVA WHERE NOMBRE = '" + nomCurva + "'";
			System.out.println(query);
			
			rs = ad.queryDB(query);
			if(rs.next())
				resultado = true;
			else
				resultado = false;
				
	    }catch(Exception e){
	    	resultado = false;
			System.out.println("MantenimientoCotBean::validaCurva(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			System.out.println("MantenimientoCotBean::validaCurva(S)");
		 	if(ad.hayConexionAbierta())
				ad.cierraConexionDB();
		}

		return resultado;
	}//validaCurva

	
	public boolean actualizaSWAP(String idCurva) 
	throws NafinException{
		AccesoDB ad = new AccesoDB();
		ResultSet rs = null;
		String query = "";
		String valor = "";
		boolean	resultado = true;
		try{
			System.out.println("MantenimientoCotBean::actualizaSWAP(E)");
			ad.conexionDB();
      
     		query = " SELECT CS_SWAP FROM CURVA WHERE IDCURVA = " + idCurva;
			System.out.println(query);
			
			rs = ad.queryDB(query);
			if(rs.next())
				valor = rs.getString(1);
			System.out.println(valor);

			query = " UPDATE CURVA SET CS_SWAP='N'";
			System.out.println(query);
			
			try{
				ad.ejecutaSQL(query);
        		ad.terminaTransaccion(true);
			} catch(SQLException e) {
				resultado = false;
				System.out.println("MantenimientoCotBean: Error en la actualización de datos");
				e.printStackTrace();
			}		

			if(valor.equals("N")){
	     		query = " UPDATE CURVA SET CS_SWAP = 'S' WHERE IDCURVA = "+ idCurva;
				System.out.println(query);
			
				try{
					ad.ejecutaSQL(query);
	        		ad.terminaTransaccion(true);
				} catch(SQLException e) {
					resultado = false;
					System.out.println("MantenimientoCotBean: Error en la actualización de datos");
					e.printStackTrace();
				}		
			}
	    }catch(Exception e){
	    	resultado = false;
			System.out.println("MantenimientoCotBean::actualizaSWAP(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			System.out.println("MantenimientoCotBean::actualizaSWAP(S)");
		 	if(ad.hayConexionAbierta())
				ad.cierraConexionDB();
		}

		return resultado;
	}//actualizaSWAP


	public boolean actualizaFondeo(String idCurva,String CsFondeo)
	throws NafinException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		boolean bok= true;
		boolean	resultado = true;
		try{
			System.out.println("MantenimientoCotBean::actualizaFondeo(E)");
			con.conexionDB();
			String qryActualizaFondeo = " UPDATE CURVA SET cs_fondeo = ? WHERE  IDCURVA = ? ";
			
			ps = con.queryPrecompilado(qryActualizaFondeo);
			if(!"".equals(CsFondeo)){
				CsFondeo = ((CsFondeo.equals("S"))?"N":"S");
				ps.setString(1,CsFondeo);
			}
			if(!"".equals(idCurva))
				ps.setString(2,idCurva);
			ps.executeUpdate();
			
			if(ps!=null) ps.close();
	    }catch(Exception e){
			bok = false;
	    	resultado = false;
			System.out.println("MantenimientoCotBean::actualizaFondeo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			System.out.println("MantenimientoCotBean::actualizaFondeo(S)");
		 	if(con.hayConexionAbierta()){
				con.terminaTransaccion(bok);
				con.cierraConexionDB();
			}
		}
		return resultado;
	}//actualizaFondeo
	
	public boolean actualizaDepositos(String idCurva,String cveMoneda)
	throws NafinException{
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		String valor = "";
		boolean bok= true;
		boolean	resultado = true;
		try{
			System.out.println("MantenimientoCotBean::actualizaDepositos(E)");
			con.conexionDB();
			
	     	String qryDeposito = " SELECT cs_deposito FROM CURVA WHERE IDCURVA = ? ";
			ps = con.queryPrecompilado(qryDeposito);
			if(!"".equals(idCurva))
				ps.setString(1,idCurva);
			rs = ps.executeQuery();
			if(rs.next())
				valor = (rs.getString("cs_deposito")==null)?"":rs.getString("cs_deposito");
			rs.close();
			ps.clearParameters();
			
			String qryActualizaDepositos = " UPDATE CURVA SET cs_deposito ='N' WHERE IC_MONEDA = ?";
			ps = con.queryPrecompilado(qryActualizaDepositos);
			if(!"".equals(cveMoneda))
				ps.setString(1,cveMoneda);
			ps.executeUpdate();
			ps.clearParameters();
			
			if(valor.equals("N")){
				qryActualizaDepositos = " UPDATE CURVA SET cs_deposito ='S' WHERE IDCURVA = ?";
				ps = con.queryPrecompilado(qryActualizaDepositos);
				if(!"".equals(idCurva))
					ps.setString(1,idCurva);
				ps.executeUpdate();
			}
			if(ps!=null) ps.close();
	    }catch(Exception e){
			bok = false;
	    	resultado = false;
			System.out.println("MantenimientoCotBean::actualizaDepositos(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			System.out.println("MantenimientoCotBean::actualizaDepositos(S)");
		 	if(con.hayConexionAbierta()){
				con.terminaTransaccion(bok);
				con.cierraConexionDB();
			}
		}
		return resultado;
	}//actualizaDepositos	
	
//SE AGREGA METODO PARA ACTULIZAR QUE CURVA OCUPARA EL IF POR DEFAULT	SMJ FODA 050 09/10/2006
	
	public boolean actualizaCurvaD(String idCurva,String cveMoneda)
	throws NafinException{
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		String valor = "";
		boolean bok= true;
		boolean	resultado = true;
		try{
			System.out.println("MantenimientoCotBean::actualizaCurvaD(E)");
			con.conexionDB();	
			
	     	String qryCurvaIF = " SELECT cg_curva_if FROM CURVA WHERE IDCURVA = ? ";
			ps = con.queryPrecompilado(qryCurvaIF);
			if(!"".equals(idCurva))
				ps.setString(1,idCurva);
			
			System.out.println("qryCurvaIF de consulta:"+qryCurvaIF);	
				
			rs = ps.executeQuery();
			if(rs.next())
				valor = (rs.getString("cg_curva_if")==null)?"":rs.getString("cg_curva_if");
			rs.close();
			ps.clearParameters();
			
			String qryActualizaCurvaIF = " UPDATE CURVA SET cg_curva_if ='N' WHERE IC_MONEDA = ?";
			ps = con.queryPrecompilado(qryActualizaCurvaIF);
			if(!"".equals(cveMoneda))
				ps.setString(1,cveMoneda);
				
			System.out.println("qryActualizaCurvaIF en N:"+qryActualizaCurvaIF);	
			
			ps.executeUpdate();
			ps.clearParameters();
			
			if(valor.equals("N")){
				qryActualizaCurvaIF = " UPDATE CURVA SET cg_curva_if ='S' WHERE IDCURVA = ?";
				ps = con.queryPrecompilado(qryActualizaCurvaIF);
				if(!"".equals(idCurva))
					ps.setString(1,idCurva);
				
				System.out.println("qryActualizaCurvaIF en S:"+qryActualizaCurvaIF);		
					
				ps.executeUpdate();
			}
			
			if(ps!=null) ps.close();
	    }catch(Exception e){
			bok = false;
	    	resultado = false;
			System.out.println("MantenimientoCotBean::actualizaCurvaD(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			System.out.println("MantenimientoCotBean::actualizaCurvaD(S)");
		 	if(con.hayConexionAbierta()){
				con.terminaTransaccion(bok);
				con.cierraConexionDB();
			}
		}
		return resultado;
	}//actualizaCurvaD
	
	/**
	 * Antes de modificar los datos del usuario, busca si ya existe un director para esta area
	 * @author Agustín Bautista Ruiz.
	 * @throws com.netro.exception.NafinException
	 * @return True: Si ya existe. False: Caso contrario
	 * @param antesDirector
	 * @param areaAnterior
	 * @param area
	 */
	public boolean existeDirector(String area, String areaAnterior, String antesDirector) throws NafinException{
		
		AccesoDB          con =  new AccesoDB();
		PreparedStatement ps  = null;
		ResultSet         rs  = null;
		
		String  qrySentencia = "";
		boolean existe       = false;
		
		try{
			System.out.println("MantenimientoCotBean::existeDirector(E)");
			con.conexionDB();
			qrySentencia = "SELECT * FROM COTCAT_EJECUTIVO WHERE IC_AREA = ? AND CS_DIRECTOR = 'S'";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, area);
			rs = ps.executeQuery();
			if(rs.next()){ //Ya existe un director para esta area
				if(!areaAnterior.equals(rs.getString("IC_AREA"))){ //Para el caso en que no se trate de el mismo ejecutivo de area que ya era director, y que seguira siendo
					existe = true;
				}
				if( areaAnterior.equals(rs.getString("IC_AREA")) & !antesDirector.equals("") ){ //Se trata de la misma area de ejecutivo pero no era director antes de la modificacion
					existe = true;
				}
			}
			
			if(ps!=null){
				ps.close();
			}
		} catch(Exception e){
			System.out.println("MantenimientoCotBean::existeDirector.Exception. " + e);
		} finally{
			System.out.println("qrySentencia: " + qrySentencia);
			con.terminaTransaccion(false);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		
		System.out.println("MantenimientoCotBean::existeDirector(S)");
		return existe;
		
	}

    /**
     * metodo para obtener los ejecutivos por Area e if
     * en la pantalla  ADMIN TESORERIA/ Tasas de Crédito Nafin / Parametrizar Tesorería / Asignación Ejecutivo al IF
     * @autor QC-DLHC  NE_2017_001 2017
     * @param icArea
     * @param icIf
     * @return
     * @throws NafinException
     */
    public ArrayList getEjecutivosXAreaIF(String icArea, String icIf) throws NafinException {
        LOG.info("getEjecutivosXAreaIF (e)");

        AccesoDB con = new AccesoDB();
        StringBuilder qrySentencia = new StringBuilder();  
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList columnas = new ArrayList();
        ArrayList renglones = new ArrayList();
        List lVarBind = new ArrayList();
        
        try {
           
            con.conexionDB();
            
            qrySentencia = new StringBuilder();  
           qrySentencia.append(" SELECT distinct e.ic_ejecutivo, "+
                               " e.cg_nombre || ' ' || e.cg_appaterno || ' ' || e.cg_apmaterno AS nombre  "+
                               " FROM cotcat_ejecutivo e ,  cotrel_if_ejecutivo ie "+
                               " WHERE e.cs_director = ?  "+
                               " AND e.ic_area = ?  "+
                               " and  e.ic_ejecutivo =  ie.ic_ejecutivo ");
                         
            lVarBind = new ArrayList();
            lVarBind.add("N");
            lVarBind.add(icArea); 
            
            if (!"".equalsIgnoreCase(icIf)   ) {
                qrySentencia.append("and  ie.ic_if = ? ");                
                lVarBind.add(icIf);   
            }
            
            LOG.debug("qrySentencia "+qrySentencia.toString()+" \n varBind "+lVarBind);

            ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
            rs = ps.executeQuery();  
            ps.clearParameters();
            while (rs.next()) {
                String rs_ejecutivo = rs.getString(1) == null ? "" : rs.getString(1);
                String rs_nombreEjec = rs.getString(2) == null ? "" : rs.getString(2);
                columnas = new ArrayList();
                columnas.add(rs_ejecutivo);
                columnas.add(rs_nombreEjec);
                renglones.add(columnas);
            } //while(rs.next())
            rs.close();
            if (ps != null)
                ps.close();
        } catch (Exception e) {
            LOG.error("getEjecutivosXAreaIF(Exception) " + e);           
            throw new AppException("SIST0001");
        } finally {
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
            LOG.info("getEjecutivosXAreaIF(S)");
        }
        return renglones;
    } 




    /**
     * Método para generar el archivo  csv de la pantalla  
     * ADMIN TESORERIA/ Tasas de Crédito Nafin / Parametrizar Tesorería / Asignación Ejecutivo al IF
     * @autor QC-DLHC  NE_2017_001 2017
     * @param datos
     * @return
     * @throws AppException
     */
    public String genArchAsignaEjecutivoIF(Map<String, Object> datos) throws AppException {
        LOG.debug(" genArchAsignaEjecutivoIF (E)  ");
        String nombreArchivo = "";
        OutputStreamWriter writer = null;
        BufferedWriter buffer = null;
        StringBuilder linea = new StringBuilder();

        try {
           
            String path = (String) datos.get("strDirectorioTemp");
            String area = (String) datos.get("area");
            String icIf = (String) datos.get("icIf");

            nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
            writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
            buffer = new BufferedWriter(writer);
            linea = new StringBuilder();
            linea.append("Intermediario Financiero, Solicitar Autorización del Ejecutivo, " +
                         " Permitir Cotización, Correo Electrónico, Ejecutivo Nafin  \n");
            buffer.write(linea.toString());

             ArrayList intermeriarios =  this.getIntermediariosFin(area, icIf);

            for (int i = 0; i < intermeriarios.size(); i++) {

                List datosIntermediario =(List) intermeriarios.get(i);
                String claveIF = datosIntermediario.get(0).toString();
                String nombreIF = datosIntermediario.get(1).toString();

                linea = new StringBuilder();
                linea.append(nombreIF.replace(',', ' ') + ", ");

                Vector vDatosSolicAut = new Vector();
                List ejecutivos = this.getEjecutivosXAreaIF(area, claveIF);
                
                if ( !ejecutivos.isEmpty()) { 
                    for (int j = 0; j < ejecutivos.size(); j++) {
                        List datosEjecutivo = (List) ejecutivos.get(j);
                        String claveEjecutivo = datosEjecutivo.get(0).toString();
                        String nombreEjecutivo = datosEjecutivo.get(1).toString();
                        String existeRelacion = this.getRelacionEjecutivoIF(claveEjecutivo, claveIF);
                        String tipoAutorizacion = "";
                        String monto_mayor = "";
                        String cotizaLinea = "";  
                        String correoElectro = "";  

                        if ("S".equals(existeRelacion) ){
                            vDatosSolicAut = this.getDatosAutorizacion(claveEjecutivo, claveIF);
                            tipoAutorizacion = ((vDatosSolicAut.get(0) == null) ? "N" : vDatosSolicAut.get(0).toString());
                            monto_mayor = ((vDatosSolicAut.get(1) == null) ? "" : vDatosSolicAut.get(1).toString());
                            cotizaLinea = ((vDatosSolicAut.get(2) == null) ? "" : vDatosSolicAut.get(2).toString()); 
                            correoElectro = ((vDatosSolicAut.get(3) == null) ? "" : vDatosSolicAut.get(3).toString());

                            if ("N".equals(tipoAutorizacion) || "".equals(tipoAutorizacion)) {
                                linea.append("Ninguno, ");
                            } else if ("T".equals(tipoAutorizacion)) {
                                linea.append("Todos, ");
                            } else if ("M".equals(tipoAutorizacion)) {
                                linea.append("Monto Mayor a  " + "$ " +
                                             Comunes.formatoDecimal(monto_mayor.replace(',', ' '), 2, false) + ", ");
                            }
                            if ("S".equals(cotizaLinea)) {
                                linea.append("Habilitado , ");
                            } else if ("N".equals(cotizaLinea)) {
                                linea.append(" Desabilitado , ");
                            } else {
                                linea.append(" Ninguno , ");
                            }
                            linea.append(correoElectro.replace(',', ' ') + ", ");
                            linea.append(nombreEjecutivo.replace(',', ' ') + "\n ");
                        }
                    } //for  ejecutivos
                } else { //if  ejecutivos
                    linea.append(" Ninguno , ");
                    linea.append(" Desabilitado \n ");

                }
                buffer.write(linea.toString());
            } //for intermeriarios
            buffer.close();


        } catch (Exception e) {
            throw new AppException("Error al generar el archivo", e);
        }

        LOG.debug(" genArchAsignaEjecutivoIF (s)  ");
        return nombreArchivo;
    }
}

