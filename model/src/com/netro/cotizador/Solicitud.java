package com.netro.cotizador;

import netropology.utilerias.*;
import java.sql.*;
import java.util.*;
import java.math.*;
import java.text.*;

public class Solicitud implements java.io.Serializable{
	
        private String  icEstatus = "0";
    
        public Solicitud(){
	}
	private String df_solicitud				= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	private String ic_ejecutivo				= "";
	private String nombre_ejecutivo			= "";
	private String cg_razon_social_if		= "";
	private String idtipointermediario		= "";
	private String riesgoac					= "";
	private String riesgoif					= "";
	private String cg_programa				= "";
	private String ic_moneda				= "";
	private double fn_monto					= 0;
	private String ic_tipo_tasa				= "";
	private String in_num_disp				= "";
	private String cs_tasa_unica			= "";
	private String ig_plazo					= "";
	private String ic_esquema_recup			= "";
	private String ig_ppi 					= "";
	private String cg_ut_ppi				= "";
	private String in_dia_pago				= "";
	private String ig_ppc					= "";
	private String cg_ut_ppc				= "";	
	private String ig_periodos_gracia		= "";
	private String cg_ut_periodos_gracia	= "";
	private int ig_plazo_max_oper			= 0;
	private int ig_num_max_disp				= 0;
	private String mensajeError				= "";
	private String ig_tipo_piso				= "";
	private String ic_if					= "";
	private String ic_solic_esp				= "";
	private String cg_acreditado			= "";
	private String cg_mail					= "";
	private String cg_telefono				= "";
	private String cg_fax					= "";
	private String esquema_recup			= "";
	private String tipo_tasa				= "";
	private int ic_proc_pago				= 0;
	private String ig_riesgo_if				= "";
	private String ig_riesgo_ac				= "";
	private ArrayList alPagos				= null;
	private String fn_monto_disp[]			= null;
	private String df_disposicion[]			= null;
	private String df_vencimiento[]			= null;
	private StringBuffer errores			= null;
	private StringBuffer resumen			= null;
	private String cg_tipo_cotizacion		= "";
	//agregados
	private String ig_num_solic				= "";
	private String numero_solicitud			= "";
	private String df_cotizacion			= "";
	private String hora_solicitud			= "";
	private String ig_num_referencia		= "";
	private String ig_tasa_md				= "";
	private String ig_tasa_spot				= "";
	private String hora_cotizacion			= "";
	private String cg_observaciones			= "";
	private String cs_aceptado_md			= "";
	private String cs_vobo_ejec				= "";
	private int diasIrreg					= 0;
	private String sig_dia_habil			= "";
	private String sig_dia_habil_spot		= "";
	private String df_aceptacion			= "";
	private String dias_inhabiles			= "";
	private String ic_usuario				= "";
	private String nombre_usuario			= "";
	private String nombre_if_ci				= "";
	private String nombre_if_cs				= "";
	private String tipo_oper				= "";
	private String ig_valor_riesgo_ac		= "";
	private String cg_ut_plazo				= "30";
	private String idCurva					= "";
	private double tre						= 0;
	private double treconacarreo			= 0;
	private double	ppv;
	private double	duracion;
	private int		numDec					= 2;
	private double	impuestoUSD				= 0;		
	private double	costoAllIn				= 0;
	private double	factoresRiesgoUSD		= 0;
	private double	tasaCreditoUSD			= 0;
	private double	tasaSWAP				= 0;
	private int		ig_plazo_conv			= 0;
	private int		ig_plazo_conv_ant			= 0;
	private String 	nombre_moneda			= "";
	private String sig_Fecha_Disp_Habil_Perm = "";
	private double	sumaAcarreos			= 0;
	private String metodoCalculo = "";		// metodologia de calculo (ppv o duracion)	
	private String id_interpolacion = "";		// metodologia de interpolacion	
	private double	ppvtmp;					//ppv temporal, para usarse en caso de nueva duracion
	private double	techoTasaProtegida;
	private String cot_en_linea				= "";
	private String cg_pfpc					= "";//primer fecha de pago de capital
	private String cg_pfpi					= "";//primer fecha de pago de interes
	private String cg_pufpc					= "";//pen�ltima fecha de pago de capital
	private String cg_pufpi					= "";//pen�ltima fecha de pago de interes
	private List lDias						= new ArrayList();//subimos los dias a pagar, y evitamos estar llamando el metodo mas de una vez
	private int numPagosC					= 0;
	public String strTipoUsuario;
	private String fn_monto1 = "";
	
/////////////////////////////agregados	
//sets
	public void setMetodoCalculo(String valor){
		this.metodoCalculo = valor;
	}
	public void setId_interpolacion(String inter){
		this.id_interpolacion = inter;
	}
	public void setTmp_ppv(double valor){
		this.ppvtmp = valor;		
	}
	public void setTechoTasaProtegida(double valor){
		this.techoTasaProtegida = valor;
	}
	public void setSumaAcarreos(double newVal){
		this.sumaAcarreos	= newVal;
	}
	public void setIg_plazo_conv(int newPlazoConv){
		this.ig_plazo_conv = newPlazoConv;
	}
	public void setIg_plazo_conv_ant(int newPlazoConvAnt){
		this.ig_plazo_conv_ant = newPlazoConvAnt;
	}
	public void setTasaSWAP(double newTasaSWAP){
		this.tasaSWAP = newTasaSWAP;
	}
	public void setTasaCreditoUSD(double newTasaCreditoUSD){
		this.tasaCreditoUSD = newTasaCreditoUSD;
	}
	public void setFactoresRiesgoUSD(double newFactoresRiesgoUSD){
		this.factoresRiesgoUSD = newFactoresRiesgoUSD;
	}
	public void setCostoAllIn(double newCostoAllIn){
		this.costoAllIn = newCostoAllIn;
	}
	public void setImpuestoUSD(double newImpuestoUSD){
		this.impuestoUSD = newImpuestoUSD;
	}
	public void setNumDec(int newNumDec){
		this.numDec = newNumDec;
	}
	public void setTRE(double newTRE){
		this.tre = newTRE;
	}
	public void setTREConAcarreo(double newTRE){
		this.treconacarreo = newTRE;
	}	
	public void setPPV(double newPPV){
		this.ppv = newPPV;
	}
	public void setDuracion(double newDuracion){
		this.duracion = newDuracion;
	}	
	public void setIdCurva(String newIdCurva){
		this.idCurva = newIdCurva;
	}
	public void setCg_ut_plazo(String newUtPlazo){
		this.cg_ut_plazo = newUtPlazo;
	}
	public void setIg_valor_riesgo_ac(String newRiesgoAc){
		this.ig_valor_riesgo_ac = newRiesgoAc;
	}
	public void setIc_usuario(String ic_usuario){
		this.ic_usuario = ic_usuario;
	}
	public void setNombre_usuario(String nombre_usuario){
		this.nombre_usuario = nombre_usuario;
	}
	public void setDias_inhabiles(String dias_inhabiles){
		this.dias_inhabiles = dias_inhabiles;
	}
	public void setDf_aceptacion(String df_aceptacion){
		this.df_aceptacion = df_aceptacion;
	}
	public void setSig_dia_habil(String sig_dia_habil){
		this.sig_dia_habil = sig_dia_habil;
	}
	public void setSig_dia_habil_spot(String sig_dia_habil_spot){
		this.sig_dia_habil_spot = sig_dia_habil_spot;
	}
	public void setCs_vobo_ejec(String cs_vobo_ejec){
		this.cs_vobo_ejec = cs_vobo_ejec;
	}
	public void setIg_num_solic(String ig_num_solic){
		this.ig_num_solic = ig_num_solic;
	}
	public void setNumero_solicitud(String numero_solicitud){
		this.numero_solicitud = numero_solicitud;
	}
	public void setDiasIrreg(int diasIrreg){
		this.diasIrreg = diasIrreg;
	}
	public void setHora_solicitud(String hora_solicitud){
		this.hora_solicitud = hora_solicitud;
	}
	public void setIg_num_referencia(String ig_num_referencia){
		this.ig_num_referencia = ig_num_referencia;
	}
	public void setIg_tasa_md(String ig_tasa_md){
		this.ig_tasa_md = ig_tasa_md;
	}
	public void setIg_tasa_spot(String ig_tasa_spot){
		this.ig_tasa_spot = ig_tasa_spot;
	}
	public void setDf_cotizacion(String df_cotizacion){
		this.df_cotizacion = df_cotizacion;
	}
	public void setHora_cotizacion(String hora_cotizacion){
		this.hora_cotizacion = hora_cotizacion;
	}
	public void setCg_observaciones(String cg_observaciones){
		this.cg_observaciones = cg_observaciones;
	}
	public void setCs_aceptado_md(String cs_aceptado_md){
		this.cs_aceptado_md = cs_aceptado_md;
	}
	public void setNombre_moneda(String val){
		this.nombre_moneda = val;
	}
	public void setSig_Fecha_Disp_Habil_Perm(String sig_Fecha_Disp_Habil_Perm){
		this.sig_Fecha_Disp_Habil_Perm = sig_Fecha_Disp_Habil_Perm;
	}
//////////////////////////////////////////////////////////////////
	public void setTipoCotizacion(String cg_tipo_cotizacion){
		this.cg_tipo_cotizacion = cg_tipo_cotizacion;
	}
	public void setIg_riesgo_if(String ig_riesgo_if){
		this.ig_riesgo_if = ig_riesgo_if;
	}
	public void setIg_riesgo_ac(String ig_riesgo_ac){
		this.ig_riesgo_ac = ig_riesgo_ac;
	}
	public void setErrores(StringBuffer errores){
		this.errores = errores;
	}
	public void setResumen(StringBuffer resumen){
		this.resumen = resumen;
	}
	public void setPagos(ArrayList alPagos){
		this.alPagos = alPagos;
	}
	public void setIc_proc_pago(int icProcPago){
		this.ic_proc_pago = icProcPago;
	}
	public void setTipo_tasa(String tipo_tasa){
		this.tipo_tasa = tipo_tasa;
	}
	public void setEsquema_recup(String esquema_recup){
		this.esquema_recup = esquema_recup;
	}
	public void setCg_mail(String cg_mail){
		this.cg_mail = cg_mail;
	}
	public void setCg_telefono(String cg_telefono){
		this.cg_telefono = cg_telefono;
	}
	public void setCg_fax(String cg_fax){
		this.cg_fax = cg_fax;
	}
	public void setIc_if(String ic_if){
		this.ic_if = ic_if;
	}
	public void setCg_acreditado(String cg_acreditado){
		this.cg_acreditado = cg_acreditado;
	}
	public void setIg_tipo_piso(String ig_tipo_piso){
		this.ig_tipo_piso = ig_tipo_piso;
	}
	public void setIc_solic_esp(String ic_solic_esp){
		this.ic_solic_esp = ic_solic_esp;
	}
	public void setFn_monto_disp(String [] fn_monto_disp){
		this.fn_monto_disp = fn_monto_disp;
	}
	public void setDf_disposicion(String []df_disposicion){
		this.df_disposicion = df_disposicion;
	}
	public void setDf_vencimiento(String []df_vencimiento){
		this.df_vencimiento = df_vencimiento;
		if(this.df_vencimiento!=null&&this.df_vencimiento.length>0&&this.df_vencimiento[0].length()>2)
			this.in_dia_pago = this.df_vencimiento[0].substring(0,2);
	}	
	public void setDf_solicitud(String df_solicitud){
		this.df_solicitud = df_solicitud;	
	}
	public void setIc_ejecutivo(String ic_ejecutivo){
		this.ic_ejecutivo = ic_ejecutivo;
	}
	public void setNombre_ejecutivo(String nombre_ejecutivo){
		this.nombre_ejecutivo = nombre_ejecutivo;
	}
	public void setCg_razon_social_if(String cg_razon_social_if){
		this.cg_razon_social_if = cg_razon_social_if;
	}
	public void setIdtipointermediario(String idtipointermediario){
		this.idtipointermediario = idtipointermediario;
	}
	public void setRiesgoac(String riesgoac){
		this.riesgoac = riesgoac;
	}
	public void setCg_programa(String cg_programa){
		this.cg_programa = cg_programa;
	}
	public void setIc_moneda(String ic_moneda){
		this.ic_moneda = ic_moneda;
	}
	public void setFn_monto(double fn_monto){
			this.fn_monto = fn_monto;
	}
	public void setIc_tipo_tasa(String ic_tipo_tasa){
		this.ic_tipo_tasa = ic_tipo_tasa;
	}
	public void setIn_num_disp(String in_num_disp){
		this.in_num_disp = in_num_disp;
	}
	public void setCs_tasa_unica(String cs_tasa_unica){
		this.cs_tasa_unica = cs_tasa_unica;
	}
	public void setIg_plazo(String ig_plazo){
		this.ig_plazo = ig_plazo;
	}
	public void setIc_esquema_recup(String ic_esquema_recup){
		this.ic_esquema_recup = ic_esquema_recup;
	}
	public void setIg_ppi(String ig_ppi){
		this.ig_ppi = ig_ppi;
	}
	public void setCg_ut_ppi(String cg_ut_ppi){
		this.cg_ut_ppi = cg_ut_ppi;
	}
	public void setIn_dia_pago(String in_dia_pago){
		this.in_dia_pago = in_dia_pago;
	}
	public void setIg_ppc(String ig_ppc){
		this.ig_ppc = ig_ppc;
	}
	public void setCg_ut_ppc(String cg_ut_ppc){
		this.cg_ut_ppc = cg_ut_ppc;
	}
	public void setIg_periodos_gracia(String ig_periodos_gracia){
		this.ig_periodos_gracia = ig_periodos_gracia;
	}
	public void setCg_ut_periodos_gracia(String cg_ut_periodos_gracia){
		this.cg_ut_periodos_gracia = cg_ut_periodos_gracia;
	}
	public void setMensajeError(String mensajeError){
		this.mensajeError = mensajeError;
	}
	public void setIg_plazo_max_oper(String ig_plazo_max_oper){
		try{
			this.ig_plazo_max_oper = Integer.parseInt(ig_plazo_max_oper);
		}catch(Exception e){
			this.mensajeError = "Parametros insuficientes, favor de ponerse en contacto con su administrador Nafin";
			this.ig_plazo_max_oper = 0;
		}
	}
	public void setIg_num_max_disp(String ig_num_max_disp){
		try{
			this.ig_num_max_disp = Integer.parseInt(ig_num_max_disp);
		}catch(Exception e){
			this.mensajeError = "Parametros insuficientes, favor de ponerse en contacto con su administrador Nafin";
			this.ig_num_max_disp = 0;
		}
	}
///////////////////////////////////////////////////////////////////////////////
	public void setNombre_if_ci(String nombre_if){
		this.nombre_if_ci = nombre_if;
	}
	public void setNombre_if_cs(String nombre_if){
		this.nombre_if_cs = nombre_if;
	}
	public void setRiesgoIf(String riesgo){
		this.riesgoif = riesgo;
	}
	public void setRiesgoAc(String riesgo){
		this.riesgoac = riesgo;
	}
	public void setCot_en_linea(String valor){
		this.cot_en_linea = valor;
	}
//fechas de primer y penultimo pago de capital e interes 			
	public void setCg_pfpc(String pfpc){
		this.cg_pfpc = pfpc;
	}
	public void setCg_pfpi(String pfpi){
		this.cg_pfpi = pfpi;
	}
	public void setCg_pufpc(String pufpc){
		this.cg_pufpc = pufpc;
	}
	public void setCg_pufpi(String pufpi){
		this.cg_pufpi = pufpi;
	}
	public void setlDias(List dias){
		this.lDias = (List)dias;
	}
	public void setnumPagosC(int pag){
		this.numPagosC = pag;
	}
//gets
	public String getMetodoCalculo(){
		return this.metodoCalculo;
	}
	public double getTmp_ppv(){
		return this.ppvtmp;
	}
	public double getTechoTasaProtegida(){
		return this.techoTasaProtegida;
	}
	public String getId_interpolacion(){
		return this.id_interpolacion;
	}
	public double getTasaSWAP(){
		return this.tasaSWAP;
	}
	public double getTasaCreditoUSD(){
		return this.tasaCreditoUSD;
	}
	public double getFactoresRiesgoUSD(){
		return this.factoresRiesgoUSD;
	}
	public double getCostoAllIn(){
		return this.costoAllIn;
	}
	public double getImpuestoUSD(){
		return this.impuestoUSD;
	}
	public int getNumDec(){
		return this.numDec;
	}
	public double getTRE(){
		return this.tre;
	}
	public double getTREConAcarreo(){
		return this.treconacarreo;
	}	
	public double getPPV(){
		return this.ppv;
	}
	public double getDuracion(){
		return this.duracion;
	}
	public String getIdCurva(){
		return this.idCurva;
	}
	public String getCg_ut_plazo(){
		return this.cg_ut_plazo;
	}
	public String getIg_valor_riesgo_ac(){
		return this.ig_valor_riesgo_ac;
	}
	public String getDias_inhabiles(){
		return this.dias_inhabiles;
	}
	public String getDf_aceptacion(){
		return this.df_aceptacion;
	}
	public String getCs_vobo_ejec(){
		return this.cs_vobo_ejec;
	}
	public String getIg_num_solic(){
		return this.ig_num_solic;
	}
	public int getDiasIrreg(){
		return this.diasIrreg;
	}
	public String getNumero_solicitud(){
		return this.numero_solicitud;
	}
	public String getHora_solicitud(){
		return this.hora_solicitud;
	}
	public String getIg_num_referencia(){
		return this.ig_num_referencia;
	}
	public String getIg_tasa_md(){
		return this.ig_tasa_md;
	}
	public String getIg_tasa_spot(){
		return this.ig_tasa_spot;
	}
	public String getDf_cotizacion(){
		return this.df_cotizacion;
	}
	public String getHora_cotizacion(){
		return this.hora_cotizacion;
	}
	public String getCg_observaciones(){
		return this.cg_observaciones;
	}
	public String getCs_aceptado_md(){
		return this.cs_aceptado_md;
	}
	public String getTipoCotizacion(){
		return this.cg_tipo_cotizacion;
	}
	public String getNombre_moneda(){
		return this.nombre_moneda;
	}
	public String getDescTasaIndicativa(){
		String tasaInd = "";
		if("1".equals(this.ic_tipo_tasa)){	//fija MN y USD		
			tasaInd = "";
		}else if("1".equals(this.ic_moneda)){	// variable y protegida MN
			tasaInd = "TIIE 28 d�as + ";
		}else if("54".equals(this.ic_moneda)){	//variable USD
			float plazo = 0;
			if(this.ic_esquema_recup.equals("1"))	//cupon cero, se toma el plazo de la operacion por ser al vencimiento
				plazo = Float.parseFloat(this.ig_plazo);
			else if(this.ic_esquema_recup.equals("4"))		//tipo renta, se toma la periodicidad de pago de capital porque es la misma que la de interes, la cual no se captura
				plazo = Float.parseFloat(this.cg_ut_ppc);
			else
				plazo = Float.parseFloat(this.cg_ut_ppi);	// tradicional y plan de pagos se toma la periodicidad de pago de interes capturada

			if(plazo <= 35f)
				tasaInd = "Libor 1m + ";
			else if(plazo <= 95f)
				tasaInd = "Libor 3m + ";
			else if(plazo <= 185f)
				tasaInd = "Libor 6m + ";
			else if(plazo <= 365f)
				tasaInd = "Libor 12m + ";
			else
				tasaInd = "";	//si excede el plazo no se cotiza o se da tasa fija
		}
		return tasaInd;	
	}	
	public String getPeriodoTasaIndicativa(){
		String tasaInd = "";
		if("1".equals(this.ic_tipo_tasa)){	//fija MN y USD		
			tasaInd = "";
		}else if("54".equals(this.ic_moneda)){	//variable USD
			float plazo = 0;
			if(this.ic_esquema_recup.equals("1"))	//cupon cero, se toma el plazo de la operacion por ser al vencimiento
				plazo = Float.parseFloat(this.ig_plazo);
			else if(this.ic_esquema_recup.equals("4"))		//tipo renta, se toma la periodicidad de pago de capital porque es la misma que la de interes, la cual no se captura
				plazo = Float.parseFloat(this.cg_ut_ppc);
			else
				plazo = Float.parseFloat(this.cg_ut_ppi);	// tradicional y plan de pagos se toma la periodicidad de pago de interes capturada

			if(plazo <= 35f)
				tasaInd = " Mensual";
			else if(plazo <= 95f)
				tasaInd = " Trimestral";
			else if(plazo <= 185f)
				tasaInd = " Semestral";
			else if(plazo <= 365f)
				tasaInd = " Anual";
			else
				tasaInd = "";	//si excede el plazo no se cotiza o se da tasa fija
		}
		return tasaInd;	
	}
	public String getIc_usuario(){
		return this.ic_usuario;
	}
	public String getNombre_usuario(){
		return this.nombre_usuario;
	}
	public String getSig_dia_habil(){
		return this.sig_dia_habil;
	}
	public String getSig_dia_habil_spot(){
		return this.sig_dia_habil_spot;
	}
	public StringBuffer getErrores(){
		return this.errores;
	}
	public StringBuffer getResumen(){
		return this.resumen;
	}
	public ArrayList getPagos(){
		return this.alPagos;
	}
	public int getIc_proc_pago(){
		return this.ic_proc_pago;
	}	
	public String getCg_mail(){
		return this.cg_mail;
	}
	public String getCg_telefono(){
		return this.cg_telefono;
	}
	public String getCg_fax(){
		return this.cg_fax;
	}
	public String getTipo_tasa(){
		return this.tipo_tasa;
	}
	public String getEsquema_recup(){
		return this.esquema_recup;
	}			
	public String getCg_acreditado(){
		return this.cg_acreditado;
	}
	public String getIc_solic_esp(){
		return this.ic_solic_esp;
	}
	public String getDf_solicitud(){
		return this.df_solicitud;	
	}
	public String getIc_ejecutivo(){
		return this.ic_ejecutivo;
	}
	public String getIc_if(){
		return this.ic_if;
	}
	public String getIg_riesgo_if(){
		return this.ig_riesgo_if;
	}
	public String getIg_riesgo_ac(){
		return this.ig_riesgo_ac;
	}
	public String getNombre_ejecutivo(){
		return this.nombre_ejecutivo;
	}
	public String getCg_razon_social_if(){
		return this.cg_razon_social_if;
	}
	public String getIdtipointermediario(){
		return this.idtipointermediario;
	}
	public String getRiesgoac(){
		return this.riesgoac;
	}
	public String getCg_programa(){
		return this.cg_programa;
	}
	public String getIc_moneda(){
		return this.ic_moneda;
	}
	public double getFn_monto(){
		return fn_monto;
	}
	public String getIc_tipo_tasa(){
		return this.ic_tipo_tasa;
	}
	public String getIn_num_disp(){
		if(this.in_num_disp==null||this.in_num_disp.equals("")){
			return "1";
		}
		return this.in_num_disp;
	}
	public String getCs_tasa_unica(){
		return this.cs_tasa_unica;
	}
	public String getIg_plazo(){
		return this.ig_plazo;
	}
	public int getIg_plazo_conv( ){
		return this.ig_plazo_conv;
	}
	public String getIg_tipo_piso(){
		if(this.ig_tipo_piso==null||this.ig_tipo_piso.equals("")){
			if ("".equals(nombre_if_ci)&&"".equals(nombre_if_cs))
				this.ig_tipo_piso = "1";
			else
				this.ig_tipo_piso = "2";
		}
		return this.ig_tipo_piso;
	}
	public String getIc_esquema_recup(){
		return this.ic_esquema_recup;
	}
	public String getIg_ppi(){
		return this.ig_ppi;
	}
	public String getCg_ut_ppi(){
		return this.cg_ut_ppi;
	}
	public String getIn_dia_pago(){
		if((this.in_dia_pago.equals("") || this.in_dia_pago.equals("0"))&& this.df_vencimiento!=null && this.df_vencimiento.length>2){
			return df_vencimiento[df_vencimiento.length-1].substring(0,2);
		}
		if(this.in_dia_pago.equals(""))
			this.in_dia_pago = "0";
		return this.in_dia_pago;
	}
	public String getIg_ppc(){
		return this.ig_ppc;
	}
	public String getCg_ut_ppc(){
		return this.cg_ut_ppc;
	}
	public String getIg_periodos_gracia(){
		if(this.ig_periodos_gracia==null||this.ig_periodos_gracia.equals("0")){
			return "0";
		}
		return this.ig_periodos_gracia;
	}
	public String getCg_ut_periodos_gracia(){
		return this.cg_ut_periodos_gracia;
	}
	public String getMensajeError(){
		return this.mensajeError;
	}
	public int getIg_plazo_max_oper(){
		return this.ig_plazo_max_oper;
	}
	public int getIg_num_max_disp(){
		return this.ig_num_max_disp;
	}
	public String getTasasIntReq(){
	//	String in = "1,2,3";
		String in = "";
	if("IF".equals(this.strTipoUsuario)){	
			in = "1";
	 if("1".equals(this.ic_moneda)){
			if("1".equals(this.ig_tipo_piso)){
				in = "1";
			}else{
				in = "1";
			}
		}else if("54".equals(this.ic_moneda)){
			in = "1,2";
		}
	}else {
				in = "1,2";
			if("1".equals(this.ic_moneda)){
			if("1".equals(this.ig_tipo_piso)){
				in = "1,2";
			}else{
				in = "1,2";
			}
		}else if("54".equals(this.ic_moneda)){
			in = "1,2";
		}
		
	}
		return in;
	}
	public String getEsquemasRecup(String origen){
		StringBuffer in = new StringBuffer("");
		if("1".equals(this.ic_tipo_tasa)){
				if("1".equals(this.ic_moneda))
				in.append("2");
				else
				in.append("1,2,3,4");
			
			if("1".equals(this.in_num_disp)||"F".equals(origen)){
				//in.append(",4");
				in.append(",1,2,3,4");
			}
		}else if("2".equals(this.ic_tipo_tasa))
			if("1".equals(this.ic_moneda))
				in.append("2");				
			else
				in.append("1,2,3,4");
			
		else if("3".equals(this.ic_tipo_tasa)){
			in.append("2");
		}
		if("1".equals(this.in_num_disp)){
			//in.append(",3");
			in.append(",2");
		}
		return in.toString();
	}
	public String getFn_monto_disp(int indice){
		String retorno = "";
		if(this.fn_monto_disp!=null){
			if(this.fn_monto_disp.length>indice){
				retorno = this.fn_monto_disp[indice];
			}				
		}
		return retorno;
	}
	public String getDf_disposicion(int indice){
		String retorno = "";
		if(this.df_disposicion!=null){
			if(this.df_disposicion.length>indice){
				retorno = this.df_disposicion[indice];
			}				
		}
		return retorno;
	}
	public String getDf_vencimiento(int indice){
		String retorno = "";
		if(this.df_vencimiento!=null){
			if(this.df_vencimiento.length>indice){
				retorno = this.df_vencimiento[indice];
			}				
		}
		return retorno;
	}	
	public String getDfVencimientoCalcUSD(int indice){
		String retorno = "";
		try{
			if(this.df_vencimiento!=null){
				if(this.df_vencimiento.length>indice){
					retorno = this.df_vencimiento[indice];
				}				
			}
			if("".equals(retorno)||this.ig_plazo_conv!=this.ig_plazo_conv_ant){
				java.util.Date dDisposicion	= Comunes.parseDate(this.df_disposicion[0]);
				java.util.Date dVencimiento = null;
				Calendar cDisposicion		= Calendar.getInstance();
				long	lVencimiento		= 0;
				long	lDisposicion		= 0;
				int		plazoDias			= 0;
				int		igPlazo				= this.ig_plazo_conv;
				int		cg_ut_plazo			= (int)Float.parseFloat(this.cg_ut_plazo);
				int incremento = 0;
				cDisposicion.setTime(dDisposicion);
				lDisposicion = dDisposicion.getTime();
				switch(cg_ut_plazo){
					case 30:
						incremento = 1;
					break;
					case 90:
						incremento = 3;
					break;
					case 180:
						incremento = 6;
					break;
					case 360:
						incremento = 12;
					break;
				}
				for(int i=0;i<igPlazo;i++){
					cDisposicion.add(Calendar.MONTH,incremento);
				}
				dDisposicion = cDisposicion.getTime();
				retorno = new SimpleDateFormat("dd/MM/yyyy").format(dDisposicion);
				if(df_vencimiento!=null)
					df_vencimiento[0] = retorno;
				dVencimiento = Comunes.parseDate(retorno);
				lVencimiento = dVencimiento.getTime();
				plazoDias = (int)((lVencimiento-lDisposicion+0.0)/86400000.0);
				if(this.in_num_disp.equals("1")){
					if(this.fn_monto_disp==null||this.fn_monto_disp.length==0||(Double.parseDouble(this.fn_monto_disp[0])==0)){
						this.fn_monto_disp = new String[1];
						this.fn_monto_disp[0] = ""+this.fn_monto;
					}
				}
				this.in_dia_pago = retorno.substring(0,2);
				this.ig_plazo = ""+plazoDias;
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Forma incompleta, no conversion de fecha");
		}finally{
			System.out.println("Solicitud::getDfVencimientoCalcUSD (S)");		
		}
		return retorno;
	}
	public String getNombre_if_ci(){
		return this.nombre_if_ci;
	}
	public String getNombre_if_cs(){
		return this.nombre_if_cs;
	}
	public String getRiesgoIf(){
		return this.riesgoif;
	}
	public String getRiesgoAc(){
		return this.riesgoac;
	}
	public String getTipoOper(){
		String tipooper = "";
		if ("".equals(nombre_if_ci)&&"".equals(nombre_if_cs))
			tipooper = "Primer Piso";
		else
			tipooper = "Segundo Piso";
		
		return tipooper;
	}		
	public String descripcionUT(String ut){
		String retorno = "";
			retorno = (("7.5".equals(ut))?"semanas":retorno);
			retorno = (("15".equals(ut))?"quincenas":retorno);
			retorno = (("30".equals(ut))?"meses":retorno);
			retorno = (("60".equals(ut))?"bimestres":retorno);
			retorno = (("90".equals(ut))?"trimestres":retorno);
			retorno = (("120".equals(ut))?"cuatrimestres":retorno);
			retorno = (("180".equals(ut))?"semestres":retorno);
			retorno = (("360".equals(ut))?"a&ntilde;os":retorno);
			retorno = (("0".equals(ut))?"Al Vencimiento":retorno);
		return retorno;
	}
	public String periodoTasa(String ut){
		String retorno = "";
			retorno = (("7.5".equals(ut))?"semanal":retorno);
			retorno = (("15".equals(ut))?"quincenal":retorno);
			retorno = (("30".equals(ut))?"mensual":retorno);
			retorno = (("60".equals(ut))?"bimestral":retorno);
			retorno = (("90".equals(ut))?"trimestral":retorno);
			retorno = (("120".equals(ut))?"cuatrimestral":retorno);
			retorno = (("180".equals(ut))?"semestral":retorno);
			retorno = (("360".equals(ut))?"anual":retorno);
			retorno = (("0".equals(ut))?"Al Vencimiento":retorno);
		return retorno;
	}
	public String formatoDecimal(double cantidad){
		double decimales = Math.pow(10,numDec);
		double redondeada = Math.round(cantidad*decimales)/decimales;
		return Comunes.formatoDecimal(redondeada,numDec,false);
	}
	public String getSig_Fecha_Disp_Habil_Perm(){
		return this.sig_Fecha_Disp_Habil_Perm;
	}
	public String getCot_en_linea(){
		return this.cot_en_linea;
	}
//fechas de primer y penultimo pago de capital e interes 			
	public String getCg_pfpc(){
		return this.cg_pfpc;
	}
	public String getCg_pfpi(){
		return this.cg_pfpi;
	}
	public String getCg_pufpc(){
		return this.cg_pufpc;
	}
	public String getCg_pufpi(){
		return this.cg_pufpi;
	}
	public List getlDias(){
		return this.lDias;
	}
	public int getnumPagosC(){
		return this.numPagosC;
	}		

	public String getStrTipoUsuario() {
		return strTipoUsuario;
	}

	public void setStrTipoUsuario(String strTipoUsuario) {
		this.strTipoUsuario = strTipoUsuario;
	}

	public void setFn_monto1(String fn_monto1){
		this.fn_monto1 = fn_monto1;
	}

	public String getFn_monto1(){
		return fn_monto1;
	}


    public void setIcEstatus(String icEstatus) {
	this.icEstatus = icEstatus;
    }

    public String getIcEstatus() {
	return icEstatus;
    }
}
