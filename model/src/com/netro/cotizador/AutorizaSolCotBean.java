package com.netro.cotizador;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "AutorizaSolCotEJB" , mappedName = "AutorizaSolCotEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class AutorizaSolCotBean implements AutorizaSolCot {
 
 private static final Log LOG = ServiceLocator.getInstance().getLog(AutorizaSolCotBean.class);//Variables para enviar mensajes al Log.
    
    
	public void guardaObservaciones
			(String ic_solic_esp
			,String cg_observaciones)
		throws NafinException{
		System.out.println("AutorizaSolCotBean::guardaObsercaciones (E)");
		AccesoDB 			con	= new AccesoDB();
		PreparedStatement	ps	= null;
		String				qrySentencia = "";
		boolean				ok = true;
		try{
			con.conexionDB();
			qrySentencia =
				" update cot_solic_esp"+
				" set cg_observaciones = ?"+
				" where ic_solic_esp = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,cg_observaciones);
			ps.setString(2,ic_solic_esp);
			ps.execute();
			ps.close();
			ps = null;
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::guardaObservaciones (S)");
		}
	}

	public void operaSolic(String ic_solic_esp[],String no_referencia[],String checked[],String observaciones[],String userLogin,String strNombreUsuario)
		throws NafinException{

		System.out.println("AutorizaSolCot::operaSolic(E)");

		AccesoDB	con				= new AccesoDB();
		String		qrySentencia	= "";
		boolean		ok				= true;
		PreparedStatement	ps 		= null;
		PreparedStatement 	ps2		= null;
		try{
			con.conexionDB();

			qrySentencia =
				" update cot_solic_esp"+
				" set ic_estatus = 9"+
				" ,cg_observaciones_sirac = ?"+
				" ,ic_usuario_modif_estatus = ?"+
				" ,cg_nomuser_mod_estatus = ?"+
				" ,df_modif_estatus = sysdate"+
				" where ic_solic_esp = ?";
			ps = con.queryPrecompilado(qrySentencia);

			qrySentencia =
				" update credito"+
				" set idstatus = 4"+
				" ,cg_observaciones_sirac = ?"+
				" where noreferencia = ?";
			ps2 = con.queryPrecompilado(qrySentencia);
			for(int i=0;i<checked.length;i++){
				if("S".equals(checked[i])){
					if(!"".equals(ic_solic_esp[i])){
						ps.setString(1,observaciones[i]);
						ps.setString(2,userLogin);
						ps.setString(3,strNombreUsuario);
						ps.setString(4,ic_solic_esp[i]);
						ps.execute();
					}
					if(!"".equals(no_referencia[i])){
						ps2.setString(1,observaciones[i]);
						ps2.setString(2,no_referencia[i]);
						ps2.execute();
					}
				}
			}
			ps.close();
			ps = null;
			ps2.close();
			ps2 = null;
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		System.out.println("AutorizaSolCot::operaSolic(S)");
		}
	}

	public void solicEnProceso(String ic_solic_esp)
		throws NafinException{

		System.out.println("AutorizaSolCot::solicEnProceso(E)");

		AccesoDB	con				= new AccesoDB();
		String		qrySentencia	= "";
		boolean		ok				= true;
		PreparedStatement	ps 		= null;
		try{
			con.conexionDB();

			qrySentencia =
				" update cot_solic_esp"+
				" set ic_estatus = 4"+
				" where ic_solic_esp = ?"+
				" and ic_estatus = 1";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_solic_esp);
			ps.execute();
			ps.close();
			ps = null;
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		System.out.println("AutorizaSolCot::solicEnProceso(S)");
		}
	}

	public void setEstatusSolic(String ic_solic_esp,String ic_estatus_nvo,String ic_estatus_ant)
		throws NafinException{
		System.out.println("AutorizaSolCot::setEstatusSolic(E)");
		AccesoDB	con				= new AccesoDB();
		String		qrySentencia	= "";
		boolean		ok				= true;
		PreparedStatement	ps 		= null;
		try{
			con.conexionDB();
			qrySentencia =
				" update cot_solic_esp"+
				" set ic_estatus = ?"+
				" where ic_solic_esp = ?"+
				" and ic_estatus = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_estatus_nvo);
			ps.setString(2,ic_solic_esp);
			ps.setString(3,ic_estatus_ant);
			ps.execute();
			ps.close();
			ps = null;
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		System.out.println("AutorizaSolCot::setEstatusSolic(S)");
		}
	}

	private ArrayList consultaSolic(String iNoUsuario,String ic_solic_esp[],String ic_estatus[],String estatus,AutSolic param,boolean esEjecutivo,String strTipoUsuario,AccesoDB con)
		throws NafinException{
		System.out.println("AutorizaSolCotBean::consultaSolic (E)");
		PreparedStatement	ps	= null;
		ResultSet			rs	= null;
		String				qrySentencia = "";
		ArrayList			alRet = new ArrayList();
		String 				in		= "";
		ArrayList			alIn	= new ArrayList();
		boolean				ejecutivoEsDirector = false;
		String				area	= "";
		String 				ojoin	= "";
		int i=0;
		try{

			if(esEjecutivo){
				qrySentencia =
					" select ic_area,cs_director from cotcat_ejecutivo where ic_usuario = ? ";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,iNoUsuario);
				rs = ps.executeQuery();
				if (rs.next()){
					ejecutivoEsDirector = "S".equals(rs.getString("cs_director"));
					area = rs.getString("ic_area");
				}
				rs.close();
				ps.close();
			}
			if(ic_solic_esp!=null&&ic_estatus!=null){
				for(i=0;i<ic_solic_esp.length;i++){
					if(!"".equals(ic_estatus[i])){
						if(!"".equals(in))
							in +=",";
						in += "?";
						alIn.add(ic_solic_esp[i]);
					}
				}
			}
			if("TESOR".equals(strTipoUsuario)){
				ojoin = "(+)";
			}

			qrySentencia =
				" SELECT SE.ic_solic_esp"+
				"  ,TO_CHAR(SE.df_solicitud,'dd/mm/yyyy')as fecha_solicitud"   +
				"  ,TO_CHAR(se.df_solicitud,'yyyymm')||'-'||se.ig_num_solic AS num_solicitud"   +
				"  ,nvl(I.cg_razon_social,nvl(se.cg_razon_social_if,se.cg_acreditado)) AS nombre_if"   +
				"  ,M.cd_nombre AS moneda"   +
				"  ,E.CG_NOMBRE||' '||e.CG_APPATERNO||' '||e.CG_APMATERNO AS ejecutivo"   +
				"  ,se.fn_monto"   +
				"  ,se.ig_plazo"   +
				"  ,se.ic_estatus"   +
				"  ,se.ig_tasa_md"   +
				"  ,se.ig_tasa_spot"   +
				"  ,se.ig_num_referencia"   +
				"  ,se.cg_causa_rechazo"   +
				"  ,es.cg_descripcion as estatus"+
				"  ,se.cg_observaciones"+
				"  ,TO_CHAR(SE.df_cotizacion,'dd/mm/yyyy HH24:MI')as fecha_cotizacion"   +
				"  ,case "+
			    "     when ie.cg_solicitar_aut = 'T' then 'SI' "+
			    "     when ie.cg_solicitar_aut = 'N' then 'NO' "+
			    "     when ie.cg_solicitar_aut = 'M' and se.fn_monto >= ie.fn_monto_mayor_a then 'SI' "+
			    "     else 'NO' "+
			    "   end as solicitar_aut "+
				"	,se.cs_vobo_ejec"+
				"	,se.ic_tipo_tasa"+
				"	,se.ic_moneda"+
				"	,se.CG_TIPO_COTIZACION"+
				"	,se.cg_ut_ppi"+
				"	,se.cg_ut_ppc"+
				"	,se.ic_esquema_recup"+
				" FROM cot_solic_esp SE"   +
				"  ,comcat_if I"   +
				"  ,comcat_moneda M"   +
				"  ,cotcat_ejecutivo E"   +
				"  ,cotcat_estatus ES"+
				" ,cotrel_if_ejecutivo ie"+
				" WHERE se.ic_moneda = m.ic_moneda"   +
				" AND se.ic_ejecutivo = E.ic_ejecutivo"   +
				" AND se.ic_if = i.ic_if" + ojoin+
				" AND se.ic_if = ie.ic_if" +ojoin+
				" AND se.ic_estatus = ES.ic_estatus";

			if(!"".equals(in))
				qrySentencia += " AND se.ic_solic_esp in("+in+")";
			if(!"".equals(estatus))
				qrySentencia += " AND se.ic_estatus in("+estatus+")";
			// si es la autorizacion de tesoreria
			/*if("TESOR".equals(strTipoUsuario)){
				qrySentencia += " AND (select min(df_disposicion) from cot_disposicion d where d. ic_solic_esp = se.ic_solic_esp)>= sysdate";
			}*/
			if(esEjecutivo){
				qrySentencia +=
					" AND se.cs_vobo_ejec = 'N'"+
					" AND se.cg_tipo_cotizacion = 'F'"+
					" AND (ie.cg_solicitar_aut = 'T' OR (ie.cg_solicitar_aut = 'M' AND se.fn_monto >= ie.fn_monto_mayor_a))"+
					" AND sigdiahabil(sigdiahabil(se.df_cotizacion+1,1)+1,1) >= SYSDATE";
				if(ejecutivoEsDirector){
					qrySentencia += " AND E.ic_usuario in (select ic_usuario from cotcat_ejecutivo where ic_area = ?) ";
				}else{
					qrySentencia += " AND E.ic_usuario = ?";
				}
			}
			if(param!=null){
				if(param.getFecha_solicitud()!=null&&param.getFecha_solicitud_a()!=null&&!param.getFecha_solicitud().equals("")&&!param.getFecha_solicitud_a().equals("")){
					qrySentencia += " AND SE.df_solicitud between to_date(?,'dd/mm/yyyy') and to_date(?,'dd/mm/yyyy')";
				}
				if(param.getNum_solicitud()!=null&&!param.getNum_solicitud().equals("")){
					qrySentencia += " AND TO_CHAR(se.df_solicitud,'yyyymm')||'-'||se.ig_num_solic = ?";
				}
				if(param.getIc_if()!=null&&!param.getIc_if().equals("")){
					qrySentencia += " AND se.ic_if = ? ";
				}
				if(param.getIc_moneda()!=null&&!param.getIc_moneda().equals("")){
					qrySentencia += " AND se.ic_moneda = ?";
				}
				if(param.getFn_monto()>0&&param.getFn_monto_a()>0){
					qrySentencia += " AND se.fn_monto between ? and ? ";
				}
				if(param.getIg_num_referencia()!=null&&!param.getIg_num_referencia().equals("")){
					qrySentencia += " AND se.ig_num_referencia = ?";
				}
			}

			qrySentencia += " ORDER BY ic_solic_esp";

			System.out.println("AutorizaSolCotBean::consultaSOlic QUERY = \n\n"+qrySentencia+"\n\n");

			ps = con.queryPrecompilado(qrySentencia);
			i=0;
			if(!"".equals(in)){
				for(i=0;i<alIn.size();i++){
					ps.setString(i+1,alIn.get(i).toString());
				}
			}
			if(esEjecutivo){
				if(ejecutivoEsDirector){
					ps.setString(++i,area);
				}else{
					ps.setString(++i,iNoUsuario);
				}
			}
			if(param!=null){
				if(param.getFecha_solicitud()!=null&&param.getFecha_solicitud_a()!=null&&!param.getFecha_solicitud().equals("")&&!param.getFecha_solicitud_a().equals("")){
					ps.setString(++i,param.getFecha_solicitud());
					ps.setString(++i,param.getFecha_solicitud_a());
				}
				if(param.getNum_solicitud()!=null&&!param.getNum_solicitud().equals("")){
					ps.setString(++i,param.getNum_solicitud());
				}
				if(param.getIc_if()!=null&&!param.getIc_if().equals("")){
					ps.setString(++i,param.getIc_if());
				}
				if(param.getIc_moneda()!=null&&!param.getIc_moneda().equals("")){
					ps.setString(++i,param.getIc_moneda());
				}
				if(param.getFn_monto()>0&&param.getFn_monto_a()>0){
					ps.setDouble(++i,param.getFn_monto());
					ps.setDouble(++i,param.getFn_monto_a());
				}
				if(param.getIg_num_referencia()!=null&&!param.getIg_num_referencia().equals("")){
					ps.setString(++i,param.getIg_num_referencia());
				}
			}
			rs = ps.executeQuery();
			while(rs.next()){
				AutSolic autSol = new AutSolic();
				autSol.setIc_solic_esp(rs.getString("ic_solic_esp"));
				autSol.setFecha_solicitud(rs.getString("fecha_solicitud"));
				autSol.setFecha_cotizacion(rs.getString("fecha_cotizacion"));
				autSol.setNum_solicitud(rs.getString("num_solicitud"));
				autSol.setNombre_if(rs.getString("nombre_if"));
				autSol.setMoneda(rs.getString("moneda"));
				autSol.setEjecutivo(rs.getString("ejecutivo"));
				autSol.setFn_monto(rs.getDouble("fn_monto"));
				autSol.setPlazo_oper(rs.getString("ig_plazo"));
				autSol.setIc_estatus(rs.getString("ic_estatus"));
				autSol.setIg_tasa_md(rs.getDouble("ig_tasa_md"));
				autSol.setIg_tasa_spot(rs.getDouble("ig_tasa_spot"));
				autSol.setIg_num_referencia((rs.getString("ig_num_referencia")==null)?"":rs.getString("ig_num_referencia"));
				autSol.setCg_causa_rechazo((rs.getString("cg_causa_rechazo")==null)?"":rs.getString("cg_causa_rechazo"));
				autSol.setEstatus((rs.getString("estatus")==null)?"":rs.getString("estatus"));
				autSol.setCg_observaciones((rs.getString("cg_observaciones")==null)?"":rs.getString("cg_observaciones"));
				autSol.setSolicitar_aut(rs.getString("solicitar_aut"));
				autSol.setCs_vobo_ejec(rs.getString("cs_vobo_ejec"));
				autSol.setIc_tipo_tasa(rs.getString("ic_tipo_tasa"));
				autSol.setIc_moneda(rs.getString("ic_moneda"));
				autSol.setTipoCotizacion(rs.getString("CG_TIPO_COTIZACION"));
				autSol.setIc_esquema_recup(rs.getString("ic_esquema_recup"));
				autSol.setCg_ut_ppc(rs.getString("cg_ut_ppc"));
				autSol.setCg_ut_ppi(rs.getString("cg_ut_ppi"));
				alRet.add(autSol);
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("AutorizaSolCotBean::consultaSolic (S)");
		}
		return alRet;
	}

	public ArrayList consultaSolic(String iNoUsuario,String ic_solic_esp[],String ic_estatus[],String estatus,AutSolic param,boolean esEjecutivo)
		throws NafinException{
		System.out.println("AutorizaSolCotBean::consultaSolic (E)");
		AccesoDB 			con	= new AccesoDB();
		ArrayList			alRet = new ArrayList();
		try{
			con.conexionDB();
			alRet = consultaSolic(iNoUsuario,ic_solic_esp,ic_estatus,estatus,param,esEjecutivo,"",con);
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::consultaSolic (S)");
		}
		return alRet;
	}

	public ArrayList consultaSolic(String iNoUsuario,String ic_solic_esp[],String ic_estatus[],String estatus,AutSolic param,boolean esEjecutivo,String strTipoUsuario)
		throws NafinException{
		System.out.println("AutorizaSolCotBean::consultaSolic (E)");
		AccesoDB 			con	= new AccesoDB();
		ArrayList			alRet = new ArrayList();
		try{
			con.conexionDB();
			alRet = consultaSolic(iNoUsuario,ic_solic_esp,ic_estatus,estatus,param,esEjecutivo,strTipoUsuario,con);
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::consultaSolic (S)");
		}
		return alRet;
	}



	public boolean guardaCambios
			(String ic_solic_esp[]
			,String ic_estatus[]
			,String ig_tasa_md[]
			,String ig_tasa_spot[]
			,String ig_num_referencia[]
			,String cg_causa_rechazo[]
			,String cg_tipo_solic[]
			,String ic_tasa[])
		throws NafinException{
		System.out.println("AutorizaSolCotBean::guardaCambios (E)");
		AccesoDB 			con	= new AccesoDB();
		PreparedStatement	ps	= null;
		String				qrySentencia = "";
		boolean				ok = true;
		try{
			con.conexionDB();
			qrySentencia =
				" UPDATE COT_SOLIC_ESP "+
				" SET(IC_ESTATUS,IG_TASA_MD,IG_TASA_SPOT,IG_NUM_REFERENCIA,CG_CAUSA_RECHAZO,DF_COTIZACION,CS_VOBO_EJEC, CG_TIPO_COTIZACION,IC_TASA) = "+
				"  (SELECT ?,?,?,?,?,SYSDATE"   +
				"  	   ,CASE"   +
				"        WHEN se.ic_if is null then 'S'"   +
				"  	   WHEN IE.cg_solicitar_aut = 'N' THEN 'S'"   +
				"  	   WHEN IE.cg_solicitar_aut = 'T' THEN 'N'"   +
				"  	   WHEN IE.cg_solicitar_aut = 'M' AND SE.FN_MONTO >= IE.FN_MONTO_MAYOR_A THEN 'N'"   +
				"  	   WHEN IE.cg_solicitar_aut = 'M' AND SE.FN_MONTO < IE.FN_MONTO_MAYOR_A THEN 'S'"   +
				"  	   END"   +
				"	   ,?,?"+
				"   FROM COT_SOLIC_ESP SE"   +
				"   ,COTREL_IF_EJECUTIVO IE"   +
				"   WHERE ((SE.IC_IF = IE.IC_IF AND SE.IC_EJECUTIVO = IE.IC_EJECUTIVO) OR (SE.IC_IF IS NULL and rownum =1))"   +
				"   AND SE.IC_SOLIC_ESP = ?)"  +
				" where ic_solic_esp = ?";
			System.out.println(qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			for(int i=0;i<ic_estatus.length;i++){
				if(!ic_estatus[i].equals("")){
					double tasaMD = (ig_tasa_md[i].equals("")?0:Double.parseDouble(ig_tasa_md[i]));
					double tasaSpot = (ig_tasa_spot[i].equals("")?0:Double.parseDouble(ig_tasa_spot[i]));
					String numReferencia = (ig_num_referencia[i]).trim();
/*
					System.out.println("<" + ic_estatus[i] + ">");
					System.out.println("<" + tasaMD+ ">");
					System.out.println("<" + tasaSpot + ">");
					System.out.println("<" + numReferencia + ">");
					System.out.println("<" + cg_causa_rechazo[i] + ">");
					System.out.println("<" + cg_tipo_solic[i] + ">");
					System.out.println("<" + ic_tasa[i] + ">");
					System.out.println("<" + ic_solic_esp[i] + ">");
*/
					ps.setString(1,ic_estatus[i]);
					ps.setDouble(2,tasaMD);
					ps.setDouble(3,tasaSpot);
					ps.setInt(4,numReferencia.equals("")?0:Integer.parseInt(numReferencia));
					ps.setString(5,cg_causa_rechazo[i].equals("")?"":cg_causa_rechazo[i]);
					ps.setString(6,cg_tipo_solic[i]);
					ps.setInt(7,(ic_tasa[i].equals("")?0:Integer.parseInt(ic_tasa[i])));
					ps.setString(8,ic_solic_esp[i]);
					ps.setString(9,ic_solic_esp[i]);
					ps.execute();
				}
			}

			ps.close();
			ps = null;
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::guardaCambios (S)");
		}
		return ok;
	}

	public ArrayList autorizaEjec
			(String iNoUsuario
			,String ic_solic_esp[]
			,String cs_vobo_ejec[]
			,String cg_causa_rechazo[])
		throws NafinException{
		System.out.println("AutorizaSolCotBean::autorizaEjec (E)");
		AccesoDB 			con	= new AccesoDB();
		PreparedStatement	ps	= null;
		String				qrySentencia = "";
		boolean				ok = true;
		ArrayList			alRet = null;
		try{
			con.conexionDB();
			qrySentencia =
				" update cot_solic_esp"+
				" set ic_estatus = decode(?,'S',ic_estatus,'N',6)"+
				" ,cs_vobo_ejec = ?"+
				" ,cg_causa_rechazo = ?"+
				" where ic_solic_esp = ?";
			ps = con.queryPrecompilado(qrySentencia);
			for(int i=0;i<cs_vobo_ejec.length;i++){
				if(!cs_vobo_ejec[i].equals("")){
					ps.setString(1,cs_vobo_ejec[i]);
					ps.setString(2,cs_vobo_ejec[i]);
					ps.setString(3,cg_causa_rechazo[i]);
					ps.setString(4,ic_solic_esp[i]);
					ps.execute();
				}
			}
			ps.close();
			ps = null;
			con.terminaTransaccion(true);
			alRet = consultaSolic(iNoUsuario,ic_solic_esp,cs_vobo_ejec,"",null,false);
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::autorizaEjec (S)");
		}
		return alRet;
	}

	private boolean esAntesFinConfirmacion(AccesoDB con)
		throws NafinException {
		System.out.println("AutorizaSolCotBean::esAntesFinConfirmacion (E)");
		PreparedStatement	ps	= null;
		ResultSet			rs	= null;
		String				qrySentencia = "";
		boolean				retorno = false;
		try{

			qrySentencia =
				" SELECT DISTINCT CASE"   +
				"           WHEN TO_DATE (TO_CHAR (SYSDATE, 'hh24mi'), 'hh24mi') <"   +
				"                                     TO_DATE (horariofinconfirmacion, 'hh24mi')"   +
				"              THEN 'S'"   +
				"           ELSE 'N'"   +
				"        END"   +
				"   FROM parametrossistema"  ;

			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				retorno = "S".equals(rs.getString(1));
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("AutorizaSolCotBean::esAntesFinConfirmacion (S)");
		}
		return retorno;
	}
        /**
         * Metodo que realiza la toma en firme 
         * @param ic_solic_esp
         * @param num_referencia
         * @param mismoDia
         * @throws NafinException
         */
	public void tomaEnFirmeIF(String icSolicEsp,String numReferencia, boolean mismoDia ) throws NafinException{
		LOG.debug("tomaEnFirmeIF(E)");
		AccesoDB	con	= new AccesoDB();
                StringBuilder qrySentencia = new StringBuilder();
		boolean	ok= true;
		PreparedStatement ps = null;
	        List lVarBind = new ArrayList();        
		
		try{
			con.conexionDB();
                  
			qrySentencia.append(
				" update cot_solic_esp"+
				" set ic_estatus = 7"+
				" ,cs_aceptado_md = ?"+
				" ,df_aceptacion = sysdate"+
				" ,ig_num_referencia = ?"+
				" where ic_solic_esp = ?");
                        lVarBind = new ArrayList();    
                        lVarBind.add((mismoDia?"S":"N")) ;
                        lVarBind.add(numReferencia) ;
                        lVarBind.add(icSolicEsp) ;                        
                        ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);			
			ps.execute();
			ps.close();
			
                    
		}catch(Exception e){
			ok = false;
                        LOG.error("Exception en tomaEnFirmeIF. "+e);
			throw new NafinException("SIST0001");
		}finally{
                    if(con.hayConexionAbierta()){
                        con.terminaTransaccion(ok);
			con.cierraConexionDB();
                    }
                    LOG.info("AutorizaSolCot::tomaEnFirmeIF(S)");
		}
	}

	public boolean esAntesFinConfirmacion()
		throws NafinException {
		System.out.println("AutorizaSolCotBean::esAntesFinConfirmacion (E)");
		AccesoDB			con = new AccesoDB();
		boolean				retorno = false;
		try{
			con.conexionDB();
			retorno =  esAntesFinConfirmacion(con);
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::esAntesFinConfirmacion (S)");
		}
		return retorno;
	}

	public boolean validaMismoDia(String ic_solic_esp)
		throws NafinException {
		System.out.println("AutorizaSolCotBean::validaMismoDia (E)");
		AccesoDB			con = new AccesoDB();
		PreparedStatement	ps	= null;
		ResultSet			rs	= null;
		String				qrySentencia = "";
		boolean				retorno = false;
		try{
			con.conexionDB();
			retorno =  esAntesFinConfirmacion(con);
			if(retorno){
				qrySentencia =
					" SELECT CASE"   +
					"           WHEN TRUNC (df_cotizacion) = TRUNC (SYSDATE)"   +
					"              THEN 'S'"   +
					"           ELSE 'N'"   +
					"        END"   +
					"   FROM cot_solic_esp"   +
					"  WHERE ic_solic_esp = ?"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_solic_esp);
				rs = ps.executeQuery();
				if(rs.next()){
					retorno = "S".equals(rs.getString(1));
				}else{
					retorno = false;
				}
				rs.close();
				ps.close();
			}
		}catch(Exception e){
			System.out.println("AutorizaSolCotBean::validaMismoDia (Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::validaMismoDia (S)");
		}
		return retorno;
	}


	public int validaHoraToma(String ic_solic_esp)
		throws NafinException {
		System.out.println("AutorizaSolCotBean::validaMismoDia (E)");
		AccesoDB			con = new AccesoDB();
		PreparedStatement	ps	= null;
		ResultSet			rs	= null;
		String				qrySentencia = "";
		boolean				retorno = false;
		int 				ivalor = 4;
		try{
			con.conexionDB();
			retorno =  esAntesFinConfirmacion(con);
			if(retorno){
				qrySentencia =
					" SELECT CASE"   +
					"           WHEN TRUNC (df_cotizacion) = TRUNC (SYSDATE) THEN 1"   +
					"           ELSE 3"   +
					"        END"   +
					"   FROM cot_solic_esp"   +
					"  WHERE ic_solic_esp = ?"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_solic_esp);
				rs = ps.executeQuery();
				if(rs.next()){
					ivalor = rs.getInt(1);
				}else{
					ivalor = 3;
				}
				rs.close();
				ps.close();
			}else{
				qrySentencia =
					" SELECT CASE"   +
					"			WHEN TRUNC(sigdiahabil(sigdiahabil(df_cotizacion+1,1)+1,1)) = trunc(SYSDATE) then 4"+
					"           WHEN TRUNC (df_cotizacion) = TRUNC (SYSDATE) THEN 2"   +
					" 			WHEN TRUNC(sigdiahabil(sigdiahabil(df_cotizacion+1,1)+1,1)) > trunc(SYSDATE) then 3" +
					"           ELSE 4"   +
					"        END"   +
					"   FROM cot_solic_esp"   +
					"  WHERE ic_solic_esp = ?"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_solic_esp);
				rs = ps.executeQuery();
				if(rs.next()){
					ivalor = rs.getInt(1);
				}else{
					ivalor = 3;
				}
				rs.close();
				ps.close();
			}
		}catch(Exception e){
			System.out.println("AutorizaSolCotBean::validaMismoDia (Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::validaMismoDia (S)");
		}
		return ivalor;
	}

	public ArrayList consultaTomaEnFirme(String iNoCliente)
		throws NafinException {
		System.out.println("AutorizaSolCotBean::consultaTomaEnFirme (E)");
		AccesoDB			con = new AccesoDB();
		PreparedStatement	ps	= null;
		ResultSet			rs	= null;
		String				qrySentencia = "";
		ArrayList			alRet = new ArrayList();
		try{
			con.conexionDB();

			qrySentencia =
				"   SELECT SE.ic_solic_esp"   +
				"    ,TO_CHAR(SE.df_solicitud,'dd/mm/yyyy')as fecha_solicitud"   +
				"    ,TO_CHAR(SE.df_cotizacion,'dd/mm/yyyy hh24:mi:ss') as fecha_cotizacion"   +
				"    ,E.CG_NOMBRE||' '||e.CG_APPATERNO||' '||e.CG_APMATERNO AS ejecutivo"   +
				"    ,TO_CHAR(se.df_solicitud,'yyyymm')||'-'||se.ig_num_solic AS num_solicitud"   +
				"    ,M.cd_nombre AS moneda"   +
				"    ,se.fn_monto"   +
				"    ,se.ig_plazo"   +
				"    ,se.cs_vobo_ejec"   +
				"	,se.cg_ut_ppi"+
				"	,se.cg_ut_ppc"+
				"	,se.ic_esquema_recup"+
				"	,se.ic_moneda"+
				"   FROM cot_solic_esp SE"   +
				"    ,comcat_if I"   +
				"    ,comcat_moneda M"   +
				"    ,cotcat_ejecutivo E"   +
				"    ,cotcat_estatus ES"   +
				"    ,cotrel_if_ejecutivo ie"   +
				"   WHERE se.ic_if = i.ic_if"   +
				"   AND se.ic_moneda = m.ic_moneda"   +
				"   AND se.ic_ejecutivo = E.ic_ejecutivo"   +
				"   AND se.ic_if = ie.ic_if"   +
				"   AND se.ic_estatus = ES.ic_estatus"   +
				"   AND (select min(cd.df_disposicion) from cot_disposicion cd where cd.ic_solic_esp = se.ic_solic_esp) >= trunc(sysdate)"   +
				"   AND se.ic_estatus in(2,5)"   +
				//" 	AND trunc(sigdiahabil(sigdiahabil(se.df_cotizacion+1,1)+1,1)) >= trunc(SYSDATE)"  +
				"	AND se.cs_vobo_ejec = 'S'"+
				"	AND se.cg_tipo_cotizacion = 'F'"+
				"   AND se.ic_if = ?"  ;

			qrySentencia += " ORDER BY ic_solic_esp";

			System.out.println("AutorizaSolCotBean::consultaSOlic QUERY = \n\n"+qrySentencia+"\n\n");

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,iNoCliente);
			rs = ps.executeQuery();
			while(rs.next()){
				AutSolic autSol = new AutSolic();
				autSol.setIc_solic_esp(rs.getString("ic_solic_esp"));
				autSol.setFecha_solicitud(rs.getString("fecha_solicitud"));
				autSol.setFecha_cotizacion(rs.getString("fecha_cotizacion"));
				autSol.setEjecutivo(rs.getString("ejecutivo"));
				autSol.setNum_solicitud(rs.getString("num_solicitud"));
				autSol.setMoneda(rs.getString("moneda"));
				autSol.setFn_monto(rs.getDouble("fn_monto"));
				autSol.setPlazo_oper(rs.getString("ig_plazo"));
				autSol.setIc_esquema_recup(rs.getString("ic_esquema_recup"));
				autSol.setCg_ut_ppc(rs.getString("cg_ut_ppc"));
				autSol.setCg_ut_ppi(rs.getString("cg_ut_ppi"));
				autSol.setIc_moneda(rs.getString("ic_moneda"));
				alRet.add(autSol);
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::consultaTomaEnFirme (S)");
		}
		return alRet;
	}


	public List consultaAvisoSolici(String cveUsuario, String tipoAviso) throws SQLException{
		AccesoDB con = new AccesoDB();
		List lSolicitudes = new ArrayList();
		List lInformacion = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qrySolic ="",DiasInhabiles = "",dia_mes="";
		StringBuffer qrySentencia = new StringBuffer();
		//Dias
		int numDias = 2;
		try	{
			System.out.println("AutorizaSolCotBean::consultaAvisoSolici (E)");
			con.conexionDB();

			//Dias inhabiles oficiales
			qrySentencia.append(
						"SELECT CG_DIA_INHABIL as dia_mes "+
						"  FROM comcat_dia_inhabil where cg_dia_inhabil is not null ");

			System.out.println("qrySentencia:"+qrySentencia.toString()+"*");
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				DiasInhabiles += rs.getString("dia_mes") + ",";
			}
			rs.close();
			ps.close();

			Calendar fecha = Calendar.getInstance();

			String fechas_disposicion = "";

			//Obteniendo el dia Habil, segun la parametrizacion
			for(int i=0;i<numDias;){
				fecha.add(Calendar.DATE,1);
				dia_mes = (fecha.get(Calendar.DAY_OF_MONTH)<10)?("0"+fecha.get(Calendar.DAY_OF_MONTH)):(fecha.get(Calendar.DAY_OF_MONTH)+"");
				dia_mes+= ((fecha.get(Calendar.MONTH)+1)<10)?("/0" + (fecha.get(Calendar.MONTH)+1)):("/" + (fecha.get(Calendar.MONTH)+1));
				//1-Domingo 7-Sabado
				if( fecha.get(Calendar.DAY_OF_WEEK)!=1 && fecha.get(Calendar.DAY_OF_WEEK)!=7 && DiasInhabiles.indexOf(dia_mes)==-1 ){
					i++;
					fechas_disposicion += "'"+dia_mes + "/" + fecha.get(Calendar.YEAR)+"',";
				}
			}

			fechas_disposicion = fechas_disposicion.substring(0,fechas_disposicion.length()-1);

			if(tipoAviso.equals("1")){//Aviso Solicitudes Operaciones

				qrySolic =
							"SELECT   COUNT (cd.df_disposicion) AS numerooperaciones, "+
					        "         DECODE (ci.cg_razon_social, "+
					        "                 NULL, cse.cg_razon_social_if, "+
					        "                 ci.cg_razon_social "+
					        "                 ) AS intermediario, "+
							"         to_char(cd.df_disposicion,'dd/mm/yyyy') AS fecha_disposicion "+
							"    FROM cot_solic_esp cse, comcat_if ci, cot_disposicion cd "+
							"   WHERE cse.ic_if = ci.ic_if(+) "+
							"     AND cse.ic_solic_esp = cd.ic_solic_esp "+
							"	  AND cd.ic_disposicion = 1 "+
							"	 AND cse.IC_ESTATUS = 7 "+ // 7 - Confirmada
							//"     AND TRUNC (cd.df_disposicion) = TRUNC (SYSDATE+2) "+
							//"     AND TRUNC (cd.df_disposicion) = trunc(to_date('"+fecha_disposicion+ "','dd/mm/yyyy'))"+
							"     AND to_char(cd.df_disposicion,'dd/mm/yyyy') in ("+fechas_disposicion+ ")"+

							"GROUP BY cd.df_disposicion, ci.cg_razon_social, cse.cg_razon_social_if ";
			}else if(tipoAviso.equals("2")){//Aviso Solicitudes Ejecutivo
				qrySolic =
							" SELECT   COUNT (cd.df_disposicion) AS numerooperaciones,"   +
							"          DECODE (ci.cg_razon_social,"   +
							"                  NULL, cse.cg_razon_social_if,"   +
							"                  ci.cg_razon_social"   +
							"                 ) AS intermediario,"   +
							"          TO_CHAR (cd.df_disposicion, 'dd/mm/yyyy') AS fecha_disposicion"   +
							"     FROM cot_solic_esp cse,"   +
							"          comcat_if ci,"   +
							"          cot_disposicion cd,"   +
							"          cotcat_ejecutivo ce2,"   +
							"          ((SELECT ce.ic_ejecutivo AS cveejecutivo,"   +
							"                   ce.cs_director AS esdirector, ce.ic_area AS area"   +
							"              FROM cotcat_ejecutivo ce"   +
							"             WHERE ic_usuario = ? )) ejecutivo"   +
							"    WHERE cse.ic_if = ci.ic_if(+)"   +
							"      AND cse.ic_solic_esp = cd.ic_solic_esp"   +
							"      AND cd.ic_disposicion = 1"   +
							"      AND cse.ic_estatus = 7"   +
							"      AND to_char(cd.df_disposicion,'dd/mm/yyyy') in ("+fechas_disposicion+ ")"+
							"      AND ce2.ic_ejecutivo ="   +
							"             DECODE (ejecutivo.esdirector,"   +
							"                     'S', ce2.ic_ejecutivo,"   +
							"                     ejecutivo.cveejecutivo"   +
							"                    )"   +
							"      AND ce2.ic_area = ejecutivo.area"   +
							"      AND ce2.ic_ejecutivo = cse.ic_ejecutivo"   +
							" GROUP BY cd.df_disposicion, ci.cg_razon_social, cse.cg_razon_social_if"  ;


			}else if(tipoAviso.equals("3")){//Aviso Solicitudes Administrador Tesoreria
				qrySolic =
							"SELECT   COUNT (cd.df_disposicion) AS numerooperaciones, "+
					        "         DECODE (ci.cg_razon_social, "+
					        "                 NULL, cse.cg_razon_social_if, "+
					        "                 ci.cg_razon_social "+
					        "                 ) AS intermediario, "+
							"         TO_CHAR (cd.df_disposicion, 'dd/mm/yyyy') AS fecha_disposicion "+
							"    FROM cot_disposicion cd, cot_solic_esp cse, comcat_if ci "+
							"   WHERE cse.ic_if = ci.ic_if(+) "+
							"     AND cse.ic_solic_esp = cd.ic_solic_esp "+
							"     AND TRUNC (cd.df_disposicion) < TRUNC (SYSDATE) "+
							"     AND cd.ic_disposicion = 1 "+
							"     AND cse.ic_estatus = 10 "+ // 10 - Confirmada Vencida
							"GROUP BY cd.df_disposicion, ci.cg_razon_social, cse.cg_razon_social_if ";
			}

			System.out.println("qrySolic: "+qrySolic+"*");
			System.out.println("cveUsuario: "+cveUsuario+"*");

			ps = con.queryPrecompilado(qrySolic);

			if(tipoAviso.equals("2")){//Aviso Solicitudes Ejecutivo
				ps.setString(1,cveUsuario);
			}

			rs = ps.executeQuery();

			while( rs.next() ){
				lInformacion = new ArrayList();
				lInformacion.add(((rs.getString("numerooperaciones")==null)?"":rs.getString("numerooperaciones")));
				lInformacion.add(((rs.getString("intermediario")==null)?"":rs.getString("intermediario")));
				lInformacion.add(((rs.getString("fecha_disposicion")==null)?"":rs.getString("fecha_disposicion")));
				lSolicitudes.add(lInformacion);
			}

			rs.close();
			if(ps != null) ps.close();
		} catch (SQLException SQLexc)	{
			System.out.println("AutorizaSolCotBean::consultaAvisoSolici Exception 1");
			System.out.println("Error: " + SQLexc.getMessage());
			throw SQLexc;
		} catch (Exception err)	{
			System.out.println("AutorizaSolCotBean::consultaAvisoSolici Exception 2");
			System.out.println("Error: " + err.getMessage());
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AutorizaSolCotBean::consultaAvisoSolici (S)");
		}
		return lSolicitudes;
	}//consultaAvisoSolici


	public HashMap DiasParaSolicRecursos(String ic_solic_esp,String ic_moneda,String ic_if)
		throws NafinException{
		System.out.println("SolCotEspBean::DiasParaSolicRecursos(E)");
		AccesoDB con = new AccesoDB();
		HashMap hmDatos = new HashMap();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String fechaDisposicion="",hora="",DiasInhabiles = "",dia_mes="";
		StringBuffer qrySentencia = new StringBuffer();
		int numDias =0;
		int numBind = 0;
		String nombreMes[] = {"enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"};
		try{
			con.conexionDB();

			//Dias inhabiles oficiales
			qrySentencia.append(
						"SELECT CG_DIA_INHABIL as dia_mes "+
						"  FROM comcat_dia_inhabil where cg_dia_inhabil is not null ");

			System.out.println("qrySentencia:"+qrySentencia.toString()+"*");
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				DiasInhabiles += rs.getString("dia_mes") + ",";
			}
			rs.close();
			ps.close();

			qrySentencia.delete(0,qrySentencia.length()-1);
			qrySentencia.append(
					" SELECT TO_CHAR (cd.df_disposicion, 'dd/mm/yyyy') AS fecha_disposicion,"   +
					"        parametros.numdias AS numdias, ");

			if(!"".equals(ic_if)){
				qrySentencia.append(
						"        DECODE (horarioif.num_cg_horario_fin,"   +
						"                0, horario.cg_horario_fin,"   +
						" (SELECT cg_horario_fin "+
						"   FROM comrel_horario_x_if "+
						"   WHERE ic_if = ? ) "	+
						"               ) AS hora ");
			}else{
				qrySentencia.append(
						"        horario.cg_horario_fin  AS hora ")   ;
			}

			qrySentencia.append(
					"   FROM cot_disposicion cd, (SELECT ig_dias_solic_rec AS numdias"   +
					"   			   				FROM parametrossistema"   +
					"  	  			  			   WHERE ic_moneda = ?) parametros, "+
					((!"".equals(ic_if))?"(SELECT COUNT(cg_horario_fin) AS num_cg_horario_fin "   +
					" 	  			   		 FROM comrel_horario_x_if"   +
					" 	 			  		WHERE ic_if = ?) horarioif,":"")+
					" 		(SELECT cg_horario_fin FROM com_param_gral) horario"+
					"  WHERE cd.ic_solic_esp = ? AND cd.ic_disposicion = 1 ");

			System.out.println("qrySentencia:"+qrySentencia.toString()+"*");
			ps = con.queryPrecompilado(qrySentencia.toString());

			numBind = 1;
			if(!"".equals(ic_if))
				ps.setString(numBind++,ic_if);

			ps.setString(numBind++,ic_moneda);

			if(!"".equals(ic_if))
				ps.setString(numBind++,ic_if);

			ps.setString(numBind,ic_solic_esp);
			rs = ps.executeQuery();

			if(rs.next()){
				fechaDisposicion = rs.getString("fecha_disposicion");
				hora = rs.getString("hora");
				numDias = rs.getInt("numdias");
			}
			rs.close();
			ps.close();

			Calendar fecha = Calendar.getInstance();
			fecha.set(Integer.parseInt(fechaDisposicion.substring(6,10)),Integer.parseInt(fechaDisposicion.substring(3,5))-1,Integer.parseInt(fechaDisposicion.substring(0,2)));
			dia_mes = fecha.get(Calendar.DAY_OF_MONTH)+ "/" + fecha.get(Calendar.MONTH);

			//Obteniendo el dia Habil, segun la parametrizacion
			for(int i=0;i<numDias;){
				fecha.add(Calendar.DATE,-1);
				dia_mes = (fecha.get(Calendar.DAY_OF_MONTH)<10)?("0"+fecha.get(Calendar.DAY_OF_MONTH)):(fecha.get(Calendar.DAY_OF_MONTH)+"");
				dia_mes+= ((fecha.get(Calendar.MONTH)+1)<10)?("/0" + (fecha.get(Calendar.MONTH)+1)):("/" + (fecha.get(Calendar.MONTH)+1));
				//1-Domingo 7-Sabado
				if( fecha.get(Calendar.DAY_OF_WEEK)!=1 && fecha.get(Calendar.DAY_OF_WEEK)!=7 && DiasInhabiles.indexOf(dia_mes)==-1 ){
					i++;
				}
			}

			Calendar hoy = Calendar.getInstance();
			//java.util.Date fechaLimiteDisp = new java.util.Date(fecha.get(fecha.DAY_OF_MONTH),fecha.get(fecha.MONTH),fecha.get(fecha.YEAR));
			java.util.Date fechaLimiteDisp = Comunes.parseDate(new SimpleDateFormat("dd/MM/yyyy").format(fecha.getTime()));
			java.util.Date fechaHoy = Comunes.parseDate(new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()));

			int val = fechaLimiteDisp.compareTo(fechaHoy);

			hmDatos.put("DIA",((val==0)?"HOY":fecha.get(Calendar.DAY_OF_MONTH)+""));
			hmDatos.put("DESC_MES",nombreMes[fecha.get(Calendar.MONTH)]);
			hmDatos.put("ANIO",fecha.get(Calendar.YEAR)+"");
			hmDatos.put("HORA",hora);

			System.out.println("FECHA LIMITE DISP = "+(new SimpleDateFormat("dd/MM/yyyy hh:mm.ss").format(fechaLimiteDisp)));
			System.out.println("FECHA HOY = "+(new SimpleDateFormat("dd/MM/yyyy hh:mm.ss").format(fechaHoy)));
			System.out.println("VAL = "+val);
			//Si la fecha limite es hoy, comparamos la hora y minuto actual con la de los parametros
			if(val==0){
				if(hoy.get(Calendar.HOUR_OF_DAY) < Integer.parseInt(hora.substring(0,2))){
					hmDatos.put("MOSTRAR_TF","S");
					System.out.println("+++ 1");
				}else if(hoy.get(Calendar.HOUR_OF_DAY) == Integer.parseInt(hora.substring(0,2)) ){
					if(hoy.get(Calendar.MINUTE) < Integer.parseInt(hora.substring(3,5))){
						hmDatos.put("MOSTRAR_TF","S");
						System.out.println("+++ 2");
					}else{
						hmDatos.put("MOSTRAR_TF","N");
						System.out.println("+++ 3");
					}
				}else{
					hmDatos.put("MOSTRAR_TF","N");
					System.out.println("+++ 4");
				}
			}else if(val < 0){//Si la fecha limite de disposicon es menor a la fecha de hoy
				hmDatos.put("MOSTRAR_TF","N");
				System.out.println("+++ 5");
			}else{
				hmDatos.put("MOSTRAR_TF","S");
				System.out.println("+++ 6");
			}

		}catch(Exception e){
			System.out.println("SolCotEspBean::DiasParaSolicRecursos - Exception");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("SolCotEspBean::DiasParaSolicRecursos(S)");
		}
		return hmDatos;
	}//DiasParaSolicRecursos


	public ArrayList consultaTomaEnFirmeEjecutivo(String cve_ejecutivo)
		throws NafinException {
		System.out.println("AutorizaSolCotBean::consultaTomaEnFirmeEjecutivo (E)");
		AccesoDB			con = new AccesoDB();
		PreparedStatement	ps	= null;
		ResultSet			rs	= null;
		String				qrySentencia = "";
		ArrayList			alRet = new ArrayList();
		try{
			con.conexionDB();

			qrySentencia =
						" SELECT   se.ic_solic_esp,"   +
						"          TO_CHAR (se.df_solicitud, 'dd/mm/yyyy') AS fecha_solicitud,"   +
						"          TO_CHAR (se.df_cotizacion,"   +
						"                   'dd/mm/yyyy hh24:mi:ss'"   +
						"                  ) AS fecha_cotizacion,"   +
						"             e.cg_nombre"   +
						"          || ' '"   +
						"          || e.cg_appaterno"   +
						"          || ' '"   +
						"          || e.cg_apmaterno AS ejecutivo,"   +
						"		   se.cg_acreditado as acreditado," +
						"             TO_CHAR (se.df_solicitud, 'yyyymm')"   +
						"          || '-'"   +
						"          || se.ig_num_solic AS num_solicitud,"   +
						"          m.cd_nombre AS moneda, se.fn_monto, se.ig_plazo, se.cs_vobo_ejec,"   +
						"          se.cg_ut_ppi, se.cg_ut_ppc, se.ic_esquema_recup"   +
						"     FROM cot_solic_esp se,"   +
						"          comcat_moneda m,"   +
						"          cotcat_ejecutivo e,"   +
						"          (SELECT ce.ic_ejecutivo AS cveejecutivo,"   +
						"                  ce.cs_director AS esdirector, ce.ic_area AS area"   +
						"             FROM cotcat_ejecutivo ce"   +
						"            WHERE ic_usuario = ? ) ejecutivo " +
						"    WHERE se.ic_if is null"   +
						"      AND se.ic_moneda = m.ic_moneda"   +
						"      AND (SELECT MIN (cd.df_disposicion)"   +
						"             FROM cot_disposicion cd"   +
						"            WHERE cd.ic_solic_esp = se.ic_solic_esp) >= TRUNC (SYSDATE)"   +
						"      AND se.ic_estatus IN (2, 5)"   +
						"      AND TRUNC (sigdiahabil (sigdiahabil (se.df_cotizacion + 1, 1) + 1, 1)) >="   +
						"                                                                TRUNC (SYSDATE)"   +
						"      AND se.cg_tipo_cotizacion = 'F'"   +
						"      AND e.ic_ejecutivo ="   +
						"             DECODE (ejecutivo.esdirector,"   +
						"                     'S', e.ic_ejecutivo,"   +
						"                     ejecutivo.cveejecutivo"   +
						"                    )"   +
						"      AND e.ic_area = ejecutivo.area"   +
						"      AND e.ic_ejecutivo = se.ic_ejecutivo"   +
						" ORDER BY se.ic_solic_esp"  ;

			System.out.println("AutorizaSolCotBean::consultaTomaEnFirmeEjecutivo QUERY = \n\n"+qrySentencia+"\n\n");

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,cve_ejecutivo);
			rs = ps.executeQuery();
			while(rs.next()){
				AutSolic autSol = new AutSolic();
				autSol.setIc_solic_esp(rs.getString("ic_solic_esp"));
				autSol.setFecha_solicitud(rs.getString("fecha_solicitud"));
				autSol.setFecha_cotizacion(rs.getString("fecha_cotizacion"));
				autSol.setCg_acreditado(rs.getString("acreditado"));
				autSol.setNum_solicitud(rs.getString("num_solicitud"));
				autSol.setMoneda(rs.getString("moneda"));
				autSol.setFn_monto(rs.getDouble("fn_monto"));
				autSol.setPlazo_oper(rs.getString("ig_plazo"));
				autSol.setIc_esquema_recup(rs.getString("ic_esquema_recup"));
				autSol.setCg_ut_ppc(rs.getString("cg_ut_ppc"));
				autSol.setCg_ut_ppi(rs.getString("cg_ut_ppi"));
				alRet.add(autSol);
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::consultaTomaEnFirmeEjecutivo (S)");
		}
		return alRet;
	}


	public ArrayList consultaCotSol(String ic_solic_esp[],String ic_estatus[], String ig_tasa_md[], String ig_tasa_spot[], String cg_causa_rechazo[])
		throws NafinException{
		System.out.println("AutorizaSolCotBean:: consultaCotSol (E)");
		PreparedStatement	ps	= null;
		ResultSet			rs	= null;
		String				qrySentencia = "";
		ArrayList			alRet = new ArrayList();
		String 				in		= "";
		ArrayList			alIn	= new ArrayList();
		AccesoDB 			con	= new AccesoDB();
		int i=0;
		try{
			con.conexionDB();
			if(ic_solic_esp!=null&&ic_estatus!=null){
				for(i=0;i<ic_solic_esp.length;i++){
					System.out.println("Estatus::::"+ ic_estatus[i]);
					if(!"".equals(ic_estatus[i])){
						if(!"".equals(in))
							in +=",";
							in += "?";
						alIn.add(ic_solic_esp[i]);
					}
				}
			}

			qrySentencia =
				" SELECT SE.ic_solic_esp"+
				"  ,TO_CHAR(SE.df_solicitud,'dd/mm/yyyy')as fecha_solicitud"   +
				"  ,TO_CHAR(se.df_solicitud,'yyyymm')||'-'||se.ig_num_solic AS num_solicitud"   +
				"  ,nvl(I.cg_razon_social,nvl(se.cg_razon_social_if,se.cg_acreditado)) AS nombre_if"   +
				"  ,M.cd_nombre AS moneda"   +
				"  ,E.CG_NOMBRE||' '||e.CG_APPATERNO||' '||e.CG_APMATERNO AS ejecutivo"   +
				"  ,se.fn_monto"   +
				"  ,se.ig_plazo"   +
				"  ,se.ic_estatus"   +
				"  ,se.ig_tasa_md"   +
				"  ,se.ig_tasa_spot"   +
				"  ,se.ig_num_referencia"   +
				"  ,se.cg_causa_rechazo"   +
				"  ,es.cg_descripcion as estatus"+
				"  ,se.cg_observaciones"+
				"  ,TO_CHAR(SE.df_cotizacion,'dd/mm/yyyy HH24:MI')as fecha_cotizacion"   +
				"  ,case "+
			    "     when ie.cg_solicitar_aut = 'T' then 'SI' "+
			    "     when ie.cg_solicitar_aut = 'N' then 'NO' "+
			    "     when ie.cg_solicitar_aut = 'M' and se.fn_monto >= ie.fn_monto_mayor_a then 'SI' "+
			    "     else 'NO' "+
			    "   end as solicitar_aut "+
				"	,se.cs_vobo_ejec"+
				"	,se.ic_tipo_tasa"+
				"	,se.ic_moneda"+
				"	,se.CG_TIPO_COTIZACION"+
				"	,se.cg_ut_ppi"+
				"	,se.cg_ut_ppc"+
				"	,se.ic_esquema_recup"+
				" FROM cot_solic_esp SE"   +
				"  ,comcat_if I"   +
				"  ,comcat_moneda M"   +
				"  ,cotcat_ejecutivo E"   +
				"  ,cotcat_estatus ES"+
				" ,cotrel_if_ejecutivo ie"+
				" WHERE se.ic_moneda = m.ic_moneda"   +
				" AND se.ic_ejecutivo = E.ic_ejecutivo"   +
				" AND se.ic_if = i.ic_if (+)" +
				" AND se.ic_if = ie.ic_if(+)" +
				" AND se.ic_estatus = ES.ic_estatus";

			if(!"".equals(in))
				qrySentencia += " AND se.ic_solic_esp in("+in+")";


			qrySentencia += " ORDER BY ic_solic_esp";

			System.out.println("PreacuseCotizacion:: consultaCotSol QUERY = \n\n"+qrySentencia+"\n\n");

			ps = con.queryPrecompilado(qrySentencia);
			i=0;
			if(!"".equals(in)){
				for(i=0;i<alIn.size();i++){
					ps.setString(i+1,alIn.get(i).toString());
				}
			}

			rs = ps.executeQuery();

			while(rs.next()){
				AutSolic autSol = new AutSolic();
				autSol.setIc_solic_esp(rs.getString("ic_solic_esp"));
				autSol.setFecha_solicitud(rs.getString("fecha_solicitud"));
				autSol.setFecha_cotizacion(rs.getString("fecha_cotizacion"));
				autSol.setNum_solicitud(rs.getString("num_solicitud"));
				autSol.setNombre_if(rs.getString("nombre_if"));
				autSol.setMoneda(rs.getString("moneda"));
				autSol.setEjecutivo(rs.getString("ejecutivo"));
				autSol.setFn_monto(rs.getDouble("fn_monto"));
				autSol.setPlazo_oper(rs.getString("ig_plazo"));
				autSol.setIg_num_referencia((rs.getString("ig_num_referencia")==null)?"":rs.getString("ig_num_referencia"));
					for(i=0;i<ic_estatus.length;i++){
						if(ic_solic_esp[i].equals(rs.getString("ic_solic_esp"))){
							double tasaMD = (ig_tasa_md[i].equals("")?0:Double.parseDouble(ig_tasa_md[i]));
							double tasaSpot = (ig_tasa_spot[i].equals("")?0:Double.parseDouble(ig_tasa_spot[i]));
							autSol.setEstatus(ic_estatus[i]);
							autSol.setIg_tasa_md(tasaMD);
							autSol.setIg_tasa_spot(tasaSpot);
							autSol.setCg_causa_rechazo(cg_causa_rechazo[i]);
						}
					}
				autSol.setCg_observaciones((rs.getString("cg_observaciones")==null)?"":rs.getString("cg_observaciones"));
				autSol.setSolicitar_aut(rs.getString("solicitar_aut"));
				autSol.setCs_vobo_ejec(rs.getString("cs_vobo_ejec"));
				autSol.setIc_tipo_tasa(rs.getString("ic_tipo_tasa"));
				autSol.setIc_moneda(rs.getString("ic_moneda"));
				autSol.setTipoCotizacion(rs.getString("CG_TIPO_COTIZACION"));
				autSol.setIc_esquema_recup(rs.getString("ic_esquema_recup"));
				autSol.setCg_ut_ppc(rs.getString("cg_ut_ppc"));
				autSol.setCg_ut_ppi(rs.getString("cg_ut_ppi"));
				alRet.add(autSol);
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean:: consultaCotSol(S)");
		}
		return alRet;
	}


	public void mantoCreditosCot(String ic_solic_esp, String ig_tasa_md, String ig_tasa_spot)
			throws NafinException{

		System.out.println("AutorizaSolCotBean::::mantoCreditosCot(E)");

		AccesoDB	con				= new AccesoDB();
		String		qrySentencia	= "";
		boolean		ok				= true;
		PreparedStatement	ps 		= null;
		ResultSet			rs	= null;
		int reg=0;
		try{
			con.conexionDB();

			qrySentencia =
				" SELECT COUNT (1) AS reg"   +
				"   FROM cot_solic_esp"   +
				" WHERE ic_solic_esp =? AND ic_estatus = 2"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_solic_esp);
			rs = ps.executeQuery();
				if (rs.next()){
					reg= rs.getInt(1);
				}
				rs.close();
				ps.close();
			System.out.println("reg"+ reg);
			//Si existe el registro en estatus Cotizado se Actualiza la Tasa
			if ( reg==1){
				qrySentencia =
					" UPDATE cot_solic_esp"   +
					"    SET ig_tasa_md =? "   +
					"        ,ig_tasa_spot = ?"   +
					"  WHERE ic_solic_esp = ?"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setDouble(1,Double.parseDouble(ig_tasa_md));
				ps.setDouble(2,Double.parseDouble(ig_tasa_spot));
				ps.setString(3,ic_solic_esp);
				ps.execute();
				ps.close();
			}//fin if

		}catch(Exception e){
			System.out.println("AutorizaSolCotBean::::mantoCreditosCot(Exception)");
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			System.out.println("AutorizaSolCotBean::::mantoCreditosCot(S)");
		}
	}// mantoCreditosCot


	public void operaSolic(List listaCotizacion, List listaCalculadora, String userLogin,String strNombreUsuario) throws NafinException{

		System.out.println("AutorizaSolCot::operaSolic(E)");

		AccesoDB          con  = new AccesoDB();
		String            qry  = "";
		boolean           ok   = true;
		PreparedStatement ps   = null;
		PreparedStatement ps2  = null;

		HashMap mapaCotizacion  = new HashMap();
		HashMap mapaCalculadora = new HashMap();

		try{

			con.conexionDB();

			qry =
				" update cot_solic_esp"+
				" set ic_estatus = 9"+
				" ,cg_observaciones_sirac = ?"+
				" ,ic_usuario_modif_estatus = ?"+
				" ,cg_nomuser_mod_estatus = ?"+
				" ,df_modif_estatus = sysdate"+
				" where ic_solic_esp = ?";
			ps = con.queryPrecompilado(qry);

			qry =
				" update credito"+
				" set idstatus = 4"+
				" ,cg_observaciones_sirac = ?"+
				" where noreferencia = ?";
			ps2 = con.queryPrecompilado(qry);

			for(int i=0; i<listaCotizacion.size(); i++){
				mapaCotizacion = new HashMap();
				mapaCotizacion = (HashMap) listaCotizacion.get(i);
				ps.setString(1,(String)mapaCotizacion.get("CG_OBSERVACIONES_SIRAC"));
				ps.setString(2,userLogin);
				ps.setString(3,strNombreUsuario);
				ps.setString(4,(String)mapaCotizacion.get("IC_SOLIC_ESP"));
				ps.execute();
				System.out.println("IC_SOLIC_ESP-->" + mapaCotizacion.get("IC_SOLIC_ESP"));
				System.out.println("CG_OBSERVACIONES_SIRAC-->" + mapaCotizacion.get("CG_OBSERVACIONES_SIRAC"));
			}
			for(int i=0; i<listaCalculadora.size(); i++){
				mapaCalculadora = new HashMap();
				mapaCalculadora = (HashMap) listaCalculadora.get(i);
				ps2.setString(1,(String)mapaCalculadora.get("OBSERVACIONES"));
				ps2.setString(2,(String)mapaCalculadora.get("NO_REFERENCIA"));
				ps2.execute();
				System.out.println("NO_REFERENCIA-->" + mapaCalculadora.get("NO_REFERENCIA"));
				System.out.println("OBSERVACIONES-->" + mapaCalculadora.get("OBSERVACIONES"));
			}

		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		}

		System.out.println("AutorizaSolCot::operaSolic(S)");
	}



    /**
     * @autor DLHC cequality
     * @param icSolicEsp
     * @return
     * @throws NafinException
     */
        
    public boolean mismoDiaFCotizacion(String icSolicEsp)  throws NafinException {
	LOG.debug("mismoDiaFCotizacion (E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps	= null;
	ResultSet rs	= null;	
	StringBuilder qrySentencia  = new StringBuilder();
	List lVarBind = new ArrayList();
	boolean	mismoDia= false;
        
        
	try{
	    
	    con.conexionDB();
            
	    qrySentencia  = new StringBuilder();
            qrySentencia.append(" select case "+
                                "	when trunc(df_cotizacion) = trunc(sysdate) then 'S'"+
				"	else 'N'"+
				" END"+
				" from cot_solic_esp"+
				" where ic_solic_esp = ? ");
            lVarBind = new ArrayList();
	    lVarBind.add(icSolicEsp);	  
            
	    LOG.debug("qrySentencia   "+qrySentencia.toString()  +"\n lVarBind  "+lVarBind );
	    ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
	    rs = ps.executeQuery();
	    ps.clearParameters();
            
            if(rs.next()){
                mismoDia = "S".equals(rs.getString(1));
            }else{
                mismoDia = false;
            }
            rs.close();
            ps.close();
            
			
	}catch(Exception e){	    
	    LOG.error("error en metodo  mismoDiaFCotizacion "+e);
	}finally{
	    LOG.info("mismoDiaFCotizacion (S)");
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}		
	return mismoDia;
    }
        
        
        
    /**
     *@autor DLHC cequality
     * @param icSolicEsp
     * @param fechaDisposicion
     * @return
     * @throws NafinException
     */
     public int   validaFechaCotiVsDispo(String icSolicEsp, String fechaDisposicion )  throws NafinException {
	LOG.debug("validaFechaCotiVsDispo (E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps	= null;
	ResultSet rs	= null;	
	StringBuilder qrySentencia  = new StringBuilder();	
	
	List lVarBind = new ArrayList();
	int valor =1;
        
	try{
	    
	    con.conexionDB();
	                           
             // este query es para verificar si es mismo d�a 
	    qrySentencia  = new StringBuilder();
	    qrySentencia.append("    SELECT CASE " + 
	    "     WHEN TRUNC (df_cotizacion) = TRUNC (to_date('"+fechaDisposicion+"','dd/mm/yyyy')) THEN 1 " + 
	    "     WHEN TRUNC (df_cotizacion) < TRUNC (to_date('"+fechaDisposicion+"','dd/mm/yyyy')) THEN 2 " + 
	    "     ELSE 3 " + 
	    "     END " + 
	    "  FROM cot_solic_esp" + 
	    "  WHERE ic_solic_esp = ?   ")  ;
	    
	       
	    lVarBind = new ArrayList();
	    lVarBind.add(icSolicEsp);	   
	    LOG.debug("qrySentencia   "+qrySentencia.toString()  +"\n lVarBind  "+lVarBind );
	    ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
	    rs = ps.executeQuery();
	    ps.clearParameters();
	    if(rs.next()){		
		valor=   rs.getInt(1);
	    }
	    rs.close();
	    ps.close();
	    
			
	}catch(Exception e){	   
	    LOG.error("error en metodo  validaFechaCotiVsDispo "+e);
	}finally{
	    LOG.info("validaFechaCotiVsDispo (S)");
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}		
	return valor;
    }
    
    
    
    /**
     *@autor DLHC cequality
     * @param icSolicEsp
     * @return
     * @throws NafinException
     */
    public String  fechaDisposicion(String icSolicEsp)  throws NafinException {
	LOG.debug("fechaDisposicion (E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps	= null;
	ResultSet rs	= null;	
	StringBuilder qrySentencia  = new StringBuilder();	
	String fechaDisposicion ="";
	List lVarBind = new ArrayList();	
        
	try{
	    
	    con.conexionDB();
	              
	    qrySentencia  = new StringBuilder();
	    qrySentencia.append(" SELECT TO_CHAR(DF_DISPOSICION,'dd/mm/yyyy') AS  DF_DISPOSICION  "+
				" FROM cot_disposicion "+
				" WHERE IC_SOLIC_ESP  =  ?  "+
				" AND IC_DISPOSICION = ?  ");
	    
	    lVarBind = new ArrayList();
	    lVarBind.add(icSolicEsp);
	    lVarBind.add("1");	    
	    LOG.debug("qrySentencia   "+qrySentencia.toString()  +"\n lVarBind  "+lVarBind );	    
	    ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
	    rs = ps.executeQuery();
	    ps.clearParameters();
	    if(rs.next()){
		fechaDisposicion =  (rs.getString("DF_DISPOSICION")==null)?"":rs.getString("DF_DISPOSICION");
                
	    }
	    rs.close();
	    ps.close();
	    
			
	}catch(Exception e){
	    LOG.error("error en metodo  fechaDisposicion "+e);
	}finally{
	    LOG.info("fechaDisposicion (S)");
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}		
	return fechaDisposicion;
    }
    
    
    /**
     *@autor DLHC cequality
     * @param icSolicEsp
     * @return
     * @throws NafinException
     */
    public int maximoFDisposicion(String icSolicEsp)  throws NafinException {
	LOG.debug("maximoFDisposicion (E)");
	AccesoDB con = new AccesoDB();
	PreparedStatement ps	= null;
	ResultSet rs	= null;	
	StringBuilder qrySentencia  = new StringBuilder();
	List lVarBind = new ArrayList();
	int	valor = 1;
        
        
	try{
	    
	    con.conexionDB();
            
	    qrySentencia  = new StringBuilder();
            qrySentencia.append("    SELECT  CASE  "+
               " WHEN trunc(DF_DISPOSICION) =  trunc(sysdate) THEN 1 "+
               " WHEN trunc(DF_DISPOSICION) <  trunc(sysdate) THEN 2 "+
               " ELSE 3 "+
               " END "+
               " FROM cot_disposicion "+
               " WHERE IC_SOLIC_ESP  =  ? "+
               " AND IC_DISPOSICION = ? ");
                
 
            lVarBind = new ArrayList();
	    lVarBind.add(icSolicEsp);	  
            lVarBind.add("1");	 
            
	    LOG.debug("qrySentencia   "+qrySentencia.toString()  +"\n lVarBind  "+lVarBind );
	    ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
	    rs = ps.executeQuery();
	    ps.clearParameters();
            
            if(rs.next()){
               valor = rs.getInt(1);
            }
            rs.close();
            ps.close();
	                
			
	}catch(Exception e){	    
	    LOG.error("error en metodo  maximoFDisposicion "+e);
	}finally{
	    LOG.info("maximoFDisposicion (S)");
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}		
	return valor;
    } 


}	//fin de clase
