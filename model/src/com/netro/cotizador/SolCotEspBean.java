/*
************************************************************************************
*
* Nombre de Clase o de Archivo: SolCotEspBean
*
* Versi�n: 1.0
*
* Fecha Creaci�n: 30/03/2005
*
* Autor: Ricardo Fuentes
*		Salvador Saldivar Barajas
*
* Fecha Ult. Modificaci�n:
*
* Descripci�n de Clase:
************************************************************************************/

package com.netro.cotizador;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

@Stateless(name = "SolCotEspEJB" , mappedName = "SolCotEspEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class SolCotEspBean implements SolCotEsp {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(SolCotEspBean.class);

	public Solicitud guardaSolicitud(Solicitud sol)	throws NafinException{
		log.info("guardaSolicitud (E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qrySentencia = "";
		boolean ok = true;
		try{
			con.conexionDB();
			// obtiene el numero de solicitud
			qrySentencia =
				" SELECT NVL(MAX(ic_solic_esp),0)+1"   +
				" FROM cot_solic_esp"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setIc_solic_esp(rs.getString(1));
			}
			ps.close();
			ps = null;

			qrySentencia =
				"   SELECT NVL(MAX(ig_num_solic),0)+1"   +
				"   ,to_char(sysdate,'yyyymm')"   +
				"   FROM cot_solic_esp"   +
				"   where to_char(df_solicitud,'yyyymm') = to_char(sysdate,'yyyymm')"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setIg_num_solic(rs.getString(1));
				sol.setNumero_solicitud(rs.getString(2)+"-"+rs.getString(1));
			}
			ps.close();
			ps = null;


			log.debug ("sol.getIc_ejecutivo()   "+sol.getIc_ejecutivo()  );
		    
		    
			// guarda los datos de la cotizacion
			qrySentencia =
				" insert into cot_solic_esp "+
				" (IC_SOLIC_ESP, IC_EJECUTIVO, IC_IF, CG_RAZON_SOCIAL_IF, DF_SOLICITUD, CG_ACREDITADO"+
				" , CG_PROGRAMA, IC_MONEDA, FN_MONTO, IC_TIPO_TASA, IN_NUM_DISP, IC_ESQUEMA_RECUP"+
				" , CS_TASA_UNICA, IG_PLAZO, IG_PPI, CG_UT_PPI, IG_DIA_PAGO, IG_PPC, CG_UT_PPC"+
				" , IG_PERIODOS_GRACIA, CG_UT_PERIODOS_GRACIA, IC_ESTATUS,IG_RIESGO_AC,IG_RIESGO_IF"+
				" , IG_NUM_SOLIC,ic_usuario,cg_nombre_usuario,CG_PRIMER_FECHA_CAP,CG_PENULTIMA_FECHA_CAP)"+
				" values(?,?,?,?,sysdate,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,?,?,?,?,?,?"+
				")";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIc_solic_esp());
			ps.setString(2,sol.getIc_ejecutivo());
			ps.setString(3,("0".equals(sol.getIc_if())?null:sol.getIc_if()));
			ps.setString(4,sol.getCg_razon_social_if());
			//ps.setString(5,sol.getDf_solicitud());
			ps.setString(5,sol.getCg_acreditado());
			ps.setString(6,sol.getCg_programa());
			ps.setString(7,sol.getIc_moneda());
			ps.setDouble(8,sol.getFn_monto());
			ps.setString(9,sol.getIc_tipo_tasa());
			ps.setString(10,sol.getIn_num_disp());
			ps.setString(11,sol.getIc_esquema_recup());
			ps.setString(12,sol.getCs_tasa_unica());
			ps.setString(13,sol.getIg_plazo());
			ps.setString(14,sol.getIg_ppi());
			ps.setString(15,sol.getCg_ut_ppi());
			ps.setString(16,sol.getIn_dia_pago());
			ps.setString(17,sol.getIg_ppc());
			ps.setString(18,sol.getCg_ut_ppc());
			ps.setString(19,sol.getIg_periodos_gracia());
			ps.setString(20,sol.getCg_ut_periodos_gracia());
			ps.setString(21,("".equals(sol.getIg_riesgo_ac())?null:sol.getIg_riesgo_ac()));
			ps.setString(22,("".equals(sol.getIg_riesgo_if())?null:sol.getIg_riesgo_if()));
			ps.setString(23,sol.getIg_num_solic());
			ps.setString(24,sol.getIc_usuario());
			ps.setString(25,sol.getNombre_usuario());
			ps.setString(26,sol.getCg_pfpc());
			ps.setString(27,sol.getCg_pufpc());
			ps.execute();
			ps.close();
			ps = null;
			//guarda las disposiciones
			qrySentencia =
				"insert into cot_disposicion"+
				" (ic_disposicion,ic_solic_esp,fn_monto,df_disposicion,df_vencimiento)"+
				" values(?,?,?,to_date(?,'dd/mm/yyyy'),to_date(?,'dd/mm/yyyy'))";
			ps = con.queryPrecompilado(qrySentencia);
			for(int i=0;i<Integer.parseInt(sol.getIn_num_disp());i++){
				ps.setInt(1,i+1);
				ps.setString(2,sol.getIc_solic_esp());
				ps.setDouble(3,Double.parseDouble(sol.getFn_monto_disp(i)));
				ps.setString(4,sol.getDf_disposicion(i));
				ps.setString(5,sol.getDf_vencimiento(i));
				ps.execute();
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;
			if(sol.getIc_proc_pago()>0){
				ArrayList alPagos = new ArrayList();
				qrySentencia =
					"insert into cot_pago"+
					"(ic_pago,ic_solic_esp,df_pago,fn_monto)"+
					" select ic_pago,?,df_pago,fn_monto "+
					" from cottmp_pago "+
					" where ic_proc_pago = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,sol.getIc_solic_esp());
				ps.setInt(2,sol.getIc_proc_pago());
				ps.execute();
				ps.close();
				ps = null;
				qrySentencia =
					"select ic_pago"+
					" ,to_char(df_pago,'dd/mm/yyyy')"+
					" ,fn_monto"+
					" from cot_pago"+
					" where ic_solic_esp = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,sol.getIc_solic_esp());
				rs = ps.executeQuery();
				while(rs.next()){
					ArrayList alColumnas = new ArrayList();
					alColumnas.add(rs.getString(1));
					alColumnas.add(rs.getString(2));
					alColumnas.add(rs.getString(3));
					alPagos.add(alColumnas);
				}
				sol.setPagos(alPagos);
				rs.close();
				ps.close();
				rs = null;
				ps = null;
			}
			// obtiene datos adicionales de la solicitud de cotizacion
			qrySentencia =
				" select cg_mail"+
				" ,cg_telefono"+
				" ,cg_fax"+
				" from cotcat_ejecutivo"+
				" where ic_ejecutivo = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIc_ejecutivo());
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setCg_mail(rs.getString("cg_mail"));
				sol.setCg_telefono(rs.getString("cg_telefono"));
				sol.setCg_fax(rs.getString("cg_fax"));
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;

			qrySentencia =
				"select cg_descripcion"+
				" from cotcat_tipo_tasa"+
				" where ic_tipo_tasa = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIc_tipo_tasa());
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setTipo_tasa(rs.getString("cg_descripcion"));
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;

			qrySentencia =
				"select cg_descripcion"+
				" from cotcat_esquema_recup"+
				" where ic_esquema_recup = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIc_esquema_recup());
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setEsquema_recup(rs.getString("cg_descripcion"));
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;
		/*}catch(NafinException ne){
			ok = false;
			ne.printStackTrace();
			throw ne;*/
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("guardaSolicitud (S)");
		}
		return sol;
	}

	public Solicitud cargaDatos(String ic_if,String iNoUsuario,String tipoUsuario,Solicitud sol) throws NafinException{
		log.info("CargaDatos (E)");
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			sol.setIc_if(ic_if);
			if(sol.getNombre_ejecutivo()==null||sol.getNombre_ejecutivo().equals("")||sol.getIg_plazo_max_oper()==0||sol.getIg_num_max_disp()==0){
				con.conexionDB();
			}
			if(sol.getNombre_ejecutivo()==null||sol.getNombre_ejecutivo().equals("")){
				if("IF".equals(tipoUsuario)){
					qrySentencia =
						" select ce.ic_ejecutivo"+
						" ,ce.cg_nombre||' '||ce.cg_appaterno||' '||ce.cg_apmaterno as nombre"+
						" ,i.ig_tipo_piso"+
						" from cotcat_ejecutivo ce"+
						" ,cotrel_if_ejecutivo cie"+
						" ,comcat_if i"+
						" where cie.ic_ejecutivo = ce.ic_ejecutivo"+
						" and cie.ic_if = i.ic_if"+
						" and cie.ic_if = ?";
						ps = con.queryPrecompilado(qrySentencia);
						ps.setString(1,ic_if);
						rs = ps.executeQuery();
					if(rs.next()){
						sol.setIc_ejecutivo((rs.getString("ic_ejecutivo")==null)?"":rs.getString("ic_ejecutivo"));
						sol.setNombre_ejecutivo((rs.getString("nombre")==null)?"":rs.getString("nombre"));
						sol.setIg_tipo_piso((rs.getString("ig_tipo_piso")==null)?"":rs.getString("ig_tipo_piso"));
					}
					ps.close();
				}else if("NAFIN".equals(tipoUsuario)){
					qrySentencia =
						" select ce.ic_ejecutivo"+
						" ,ce.cg_nombre||' '||ce.cg_appaterno||' '||ce.cg_apmaterno as nombre"+
						" from cotcat_ejecutivo ce"+
						" where ce.ic_usuario = ?";
						ps = con.queryPrecompilado(qrySentencia);
						ps.setString(1,iNoUsuario);
						rs = ps.executeQuery();
					if(rs.next()){
						sol.setIc_ejecutivo((rs.getString("ic_ejecutivo")==null)?"":rs.getString("ic_ejecutivo"));
						sol.setNombre_ejecutivo((rs.getString("nombre")==null)?"":rs.getString("nombre"));
					}
					ps.close();
				}
			}
			//if(sol.getIg_plazo_max_oper()==0||sol.getIg_num_max_disp()==0){
			if(!(sol.getIc_moneda().equals(""))){
				qrySentencia =
					" select ig_plazo_max_oper"+
					" ,ig_num_max_disp, cg_metodo_inter, cg_metodo_calc, NVL(ig_techo_tasa_p,'0.0') as ig_techo_tasa_p"+
					" from parametrossistema where ic_moneda = ? ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,sol.getIc_moneda());
				rs = ps.executeQuery();
				if(rs.next()){
					sol.setIg_num_max_disp(rs.getString("ig_num_max_disp"));
					sol.setIg_plazo_max_oper(rs.getString("ig_plazo_max_oper"));
					sol.setMetodoCalculo(rs.getString("cg_metodo_calc"));
					sol.setId_interpolacion(rs.getString("cg_metodo_inter"));
					sol.setTechoTasaProtegida(rs.getDouble("ig_techo_tasa_p"));
				}
				ps.close();
				if(sol.getIg_plazo_max_oper()==0){
					sol.setMensajeError("Parametros insuficientes, favor de ponerse en contacto con el administrador Nafin");
				}
				if(sol.getIg_num_max_disp()==0){
					sol.setMensajeError("Parametros insuficientes, favor de ponerse en contacto con el administrador Nafin");
				}
			}
			if(sol.getDias_inhabiles().equals("")){
				StringBuffer dias_inhabiles = new StringBuffer();
				qrySentencia = " select cg_dia_inhabil from comcat_dia_inhabil where cg_dia_inhabil is not null "  ;
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();
				while(rs.next()){
					dias_inhabiles.append(","+rs.getString(1));
				}
				rs.close();
				ps.close();
				sol.setDias_inhabiles(dias_inhabiles.toString());
			}
			if(!(sol.getIc_moneda().equals(""))){
				int numDias = 0;
				qrySentencia = " select ic_moneda,ig_dias_solic_rec from parametrossistema where ic_moneda = ? ";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,sol.getIc_moneda());
				rs = ps.executeQuery();
				if(rs.next()){
					numDias = rs.getInt("ig_dias_solic_rec");
				}
				rs.close();
				ps.close();

				qrySentencia =
							"SELECT to_char(SIGDIAHABIL2(sysdate,1,?),'dd/mm/yyyy') AS fecha "+
							"  FROM DUAL ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,numDias);
				rs = ps.executeQuery();
				if(rs.next()){
					//Siguiete Fecha de Disposicion habil permitida segun los parametros
					sol.setSig_Fecha_Disp_Habil_Perm(rs.getString("fecha"));
				}
				rs.close();
				ps.close();
			}

		}catch(Exception e){
			sol.setIc_ejecutivo("");
			sol.setNombre_ejecutivo("");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("cargaDatos (S)");
		}
		return sol;
	}

	public Solicitud complementaDatos(Solicitud sol) throws NafinException{
		log.info("complementaDatos (E)");
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			con.conexionDB();
			qrySentencia =
				" select cg_descripcion"+
				" from cotcat_esquema_recup"+
				" where ic_esquema_recup = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIc_esquema_recup());
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setEsquema_recup(rs.getString("cg_descripcion"));
			}
			ps.close();

			qrySentencia =
				" select cg_descripcion"+
				" from cotcat_tipo_tasa"+
				" where ic_tipo_tasa = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIc_tipo_tasa());
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setTipo_tasa(rs.getString("cg_descripcion"));
			}
			ps.close();

			qrySentencia =
						" SELECT ic_pago, TO_CHAR (df_pago, 'dd/mm/yyyy'), fn_monto "+
						"  FROM cottmp_pago "+
						" WHERE ic_proc_pago = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,sol.getIc_proc_pago());
			rs = ps.executeQuery();
			ArrayList alPagos = new ArrayList();
			while(rs.next()){
				ArrayList alColumnas = new ArrayList();
				alColumnas.add(rs.getString(1));
				alColumnas.add(rs.getString(2));
				alColumnas.add(rs.getString(3));
				alPagos.add(alColumnas);
			}
			sol.setPagos(alPagos);
			rs.close();
			ps.close();
			rs = null;
			ps = null;
		}catch(Exception e){
			sol.setIc_ejecutivo("");
			sol.setNombre_ejecutivo("");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("complementaDatos (S)");
		}
		return sol;
	}

	public Solicitud guardaPagos(Solicitud sol,String fechas[],String montos[])	throws NafinException{
		log.info("guardaPagos (E)");
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean ok = true;
		try{
			con.conexionDB();
			qrySentencia =
				" select NVL(MAX(ic_proc_pago),0)+1"+
				" from cottmp_pago";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setIc_proc_pago(rs.getInt(1));
			}
			rs.close();
			ps.close();
			rs = null;	ps = null;
			qrySentencia =
				" insert into cottmp_pago"+
				" (ic_proc_pago,ic_pago,df_pago,fn_monto)"+
				" values(?,?,to_date(?,'dd/mm/yyyy'),?)";
			ps = con.queryPrecompilado(qrySentencia);
			for(int i=0;i<fechas.length;i++){
				ps.setInt(1,sol.getIc_proc_pago());
				ps.setInt(2,(i+1));
				ps.setString(3,fechas[i]);
				ps.setDouble(4,Double.parseDouble(montos[i]));
				ps.execute();
			}
			ps.close();
			ps = null;
		}catch(Exception e){
			sol.setIc_ejecutivo("");
			sol.setNombre_ejecutivo("");
			e.printStackTrace();
			ok = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("guardaPagos (S)");
		}
		return sol;
	}

	public Solicitud guardaPagosMasiva(Solicitud sol,ArrayList alPagos)	throws NafinException{
		log.info("guardaPagosMasiva (E)");
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean ok = true;
		int numLinea = 1;
		StringBuffer 	errores		= new StringBuffer("");
		StringBuffer 	correctos	= new StringBuffer("");
		Vector		 	fechas		= new Vector();
		Vector 		 	montos		= new Vector();
		double 		 	montoTotal	= 0;
		double			dblMonto	= 0;
		BigDecimal montoTotal1 = new BigDecimal(0.0);
		java.util.Date	dFechaDisp	= null;
		java.util.Date	dFecha		= null;
		java.util.Date	dFechaAnt	= null;
		java.util.Date	dVencimiento= null;
		boolean okLinea = true;
		int numErrores = 0;
		int i = 0;
		try{
			String linea = "";
			Vector			vecdat	= new Vector();
			VectorTokenizer	vt		= new VectorTokenizer();
			con.conexionDB();
			for(i=0;i<alPagos.size();i++) {
				okLinea = true;
				int pipesporlinea=0;
				String fecha	= "";
				String monto	= "";
				linea=(String)alPagos.get(i);
				if(linea.length()>0) {
					for(int n=0; n<linea.length(); n++) {
						String let=linea.substring(n,n+1);
						if(let.equals("|"))
							pipesporlinea++;
					}
					if(pipesporlinea!=2){
						errores.append("Error en la linea "+numLinea+": El layout es incorrecto\n");
						ok = okLinea = false;
						continue;
					}
				}else{
					continue;
				}
				vt=new VectorTokenizer(linea,"|");
				vecdat=vt.getValuesVector();

				fecha = (String)vecdat.get(1);
				monto = (String)vecdat.get(2);
				try{
					dFecha		= Comunes.parseDate(fecha);
					dFechaDisp	= Comunes.parseDate(sol.getDf_disposicion(0));
					dblMonto	= Double.parseDouble(monto);
				}catch(Exception e){
					errores.append("Error en la linea "+numLinea+": El tipo de dato es incorrecto\n");
					ok = okLinea = false;
					continue;
				}
				
				if(!Comunes.checaFecha(fecha)) {
					errores.append("Error en la linea "+numLinea+":La Fecha no es correcta formato: dd/mm/aaaa "+fecha +" \n"); 
				   ok = okLinea =  false;
				}else  { 
				
					if(dFechaDisp.compareTo(dFecha)>0){
						errores.append("Error en la linea "+numLinea+": La fecha de pago debe ser mayor a la fecha de disposicion\n");
						ok = okLinea =  false;
					}
					Calendar ctmpFD = Calendar.getInstance();
					Calendar ctmpFecha = Calendar.getInstance();
					ctmpFD.setTime(dFechaDisp);
					ctmpFecha.setTime(dFecha);
	
					int dif = 0;
					for(dif=0;dif<5;dif++){
						if(ctmpFD.equals(ctmpFecha))
							break;
						ctmpFD.add(Calendar.DAY_OF_MONTH,1);
					}
					if(dif<5){
						errores.append("Error en la linea "+numLinea+": Deben existir cinco dias naturales entre la fecha de disposicion y la primer fecha de pago\n");
						ok = okLinea =  false;
					}
	
					if(i>0){
						Integer.parseInt(sol.getCg_ut_ppi());
						if(dFechaAnt.compareTo(dFecha)>0){
							errores.append("Error en la linea "+numLinea+": La fecha de pago es menor a la anterior\n");
							ok = okLinea = false;
						}
					}
					montoTotal += dblMonto;
					montoTotal1 = montoTotal1.add(new BigDecimal(monto));
					fechas.add(fecha);
					montos.add(monto);
					dFechaAnt = dFecha;
					numLinea ++;
					if(!okLinea)
						numErrores ++;
				
				}
			}	//for
			dVencimiento = Comunes.parseDate(sol.getDf_vencimiento(0));
			if(dVencimiento.compareTo(dFecha)!=0){
				errores.append("La ultima fecha de pago debe coincidir con la fecha de vencimiento\n");
				ok = false;
			}
			//System.out.println(sol.getFn_monto1());
			//System.out.println(Comunes.formatoDecimal(montoTotal1.toString(),2,false));
			//System.out.println(sol.getFn_monto());
			//System.out.println(montoTotal);
			String strMontoTotal = Comunes.formatoDecimal(montoTotal1.toString(),2,false);
			if(!sol.getFn_monto1().equals(strMontoTotal)){
			//if(sol.getFn_monto()!=montoTotal){
				errores.append("El total de los montos es diferente al Monto Total Requerido\n");
				ok = false;
			}
			int nPagos = Integer.parseInt(sol.getIg_ppc());
			if(nPagos!= (numLinea-1)){
				errores.append("El numero de pagos no coincide con el indicado\n");
				ok = false;
			}

			String[] arrFechas = new String[fechas.size()];

			for(i=0;i<fechas.size();i++){
				arrFechas[i] = fechas.get(i).toString();
			}

			List diasPagarInt = new ArrayList();
			List diasPagarCap = new ArrayList();
			diasPagarInt = getDiasAPagar(sol);
			diasPagarCap = getDiasPagoPP(sol,arrFechas);
			int cuenta = 0;
			for(int a=0;a<diasPagarInt.size();a++){
				for(int b=0;b<diasPagarCap.size();b++){
					if(diasPagarInt.get(a).toString().compareTo(diasPagarCap.get(b).toString()) == 0){
						cuenta++;
					}
				}
			}
			if(cuenta != diasPagarCap.size()){
				errores.append("El pago de Capital, debe hacerse en fechas de pago de Interes\n");
				ok = false;
			}

			qrySentencia =
				" select NVL(MAX(ic_proc_pago),0)+1"+
				" from cottmp_pago";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setIc_proc_pago(rs.getInt(1));
			}
			rs.close();
			ps.close();
			rs = null;	ps = null;
			qrySentencia =
				" insert into cottmp_pago"+
				" (ic_proc_pago,ic_pago,df_pago,fn_monto)"+
				" values(?,?,to_date(?,'dd/mm/yyyy'),?)";
			ps = con.queryPrecompilado(qrySentencia);
			for(i=0;i<fechas.size();i++){
				ps.setInt(1,sol.getIc_proc_pago());
				ps.setInt(2,(i+1));
				ps.setString(3,fechas.get(i).toString());
				ps.setDouble(4,Double.parseDouble(montos.get(i).toString()));
				ps.execute();
			}
			ps.close();
			ps = null;
			correctos.append("Lineas leidas :"+(numLinea-1)+"\n");
			correctos.append("Lineas con errores :"+numErrores);
			sol.setErrores(errores);
			sol.setResumen(correctos);

		}catch(Exception e){
			sol.setIc_ejecutivo("");
			sol.setNombre_ejecutivo("");
			e.printStackTrace();
			ok = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("guardaPagosMasiva (S)");
		}
		return sol;
	}

	public Solicitud consultaSolicitud(String ic_solicitudS)
		throws NafinException{

		log.info("consultaSolicitud(E)");

		AccesoDB con = new AccesoDB();
		Solicitud sol= new Solicitud();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			con.conexionDB();
			// DATOS DE LA COTIZACION Y Datos del ejecutivo nafin
			qrySentencia =
						" SELECT (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') || '-' || cs.ig_num_solic "+
						"        ) AS numero_solicitud, "+
						"        TO_CHAR (cs.df_solicitud, 'DD/MM/YYYY') AS fecha_solicitud, "+
						"        TO_CHAR (cs.df_solicitud, 'HH24:MI:SS') AS hora_solicitud, "+
						"        cs.ig_num_referencia AS num_referencia, cs.ig_tasa_md tasa_mismo_dia, "+
						"        cs.ig_tasa_spot AS tasa_spot, "+
						"        TO_CHAR (cs.df_cotizacion, 'DD/MM/YYYY') AS fecha_cotizacion, "+
						"        TO_CHAR (cs.df_cotizacion, 'HH24:MI:SS') AS hora_cotizacion, "+
						"        (ce.cg_nombre || ' ' || ce.cg_appaterno || ' ' || ce.cg_apmaterno "+
						"        ) AS ejecutivo, "+
						"        ce.cg_mail AS correo_electronico, ce.cg_telefono AS telefono, "+
						"        ce.cg_fax AS fax, cs.fn_monto AS monto_total, "+
						"        ctt.cg_descripcion AS tipo_tasa, cs.in_num_disp AS num_disposiciones, "+
						"        cs.cs_tasa_unica AS disposi_multiples, cs.ig_plazo AS plazo_operacion, "+
						"		 cs.ic_esquema_recup as ic_esquema_recup, 	"+
						"        cer.cg_descripcion AS esquema_recuperacion, "+
						"        cs.ig_ppi AS periodicidad_pagos1, cs.cg_ut_ppi periodicidad_pagos2, "+
						"        cs.ig_ppc AS periodicidad_capital1, "+
						"        cs.cg_ut_ppc AS periodicidad_capital2, "+
						"        cs.ig_periodos_gracia AS numero_periodos1, "+
						"        cs.cg_ut_periodos_gracia AS numero_periodos2, "+
						"        cs.ig_dia_pago AS dia_pago, "+
						" 		 cs.cg_observaciones as cg_observaciones, "+
						"		 cs.ic_moneda as ic_moneda"+
						"		, cs.cs_aceptado_md AS cs_aceptado_md "+
						"		, cs.cs_vobo_ejec"+
						"		, sigdiahabil(nvl(cs.df_cotizacion,sysdate)+1,1) as sig_dia_habil"+
						"		, sigdiahabil(sigdiahabil(nvl(cs.df_cotizacion,sysdate)+1,1)+1,1) as sig_dia_habil_spot"+
						"		, cs.cg_acreditado"+
						"       ,TO_CHAR (cs.df_aceptacion, 'DD/MM/YYYY HH24:MI:SS') AS fecha_aceptacion "+
						"		,cs.ic_tipo_tasa"+
						"		,cs.CG_TIPO_COTIZACION"+
						"		,cs.cg_programa"+
						"		,cs.ic_usuario"+
						"		,cs.cg_nombre_usuario"+
                        "       ,ci.cg_razon_social"+
                        "       ,cs.cg_razon_social_if"+
                        "       ,tiif.descripcion as riesgoif"+
                        "       ,tiac.descripcion riesgoac"+
						"		,nvl(cs.ig_riesgo_if,ci.idtipointermediario) as ig_riesgo_if"+
						"		,ce.ic_ejecutivo as ic_ejecutivo "+
						"		,cs.ic_if as ic_if "+
						"		,cs.cg_primer_fecha_cap as pfpc "+
						"		,cs.cg_penultima_fecha_cap as pufpc "+
						"    ,cs.IC_ESTATUS as icEstatus  "+
						"   FROM cot_solic_esp cs, "+
						"        cotcat_ejecutivo ce, "+
						"        cotcat_tipo_tasa ctt, "+
						"        comcat_if ci, "+
						"        tipointermediario tiif, "+
						"        tipointermediario tiac, "+
						"        cotcat_esquema_recup cer "+
						"  WHERE cs.ic_ejecutivo = ce.ic_ejecutivo "+
						"    AND cs.ic_tipo_tasa = ctt.ic_tipo_tasa "+
						"    AND cs.ic_esquema_recup = cer.ic_esquema_recup "+
                        "    AND ci.ic_if(+) = cs.ic_if "+
                        "    AND cs.ig_riesgo_if = tiif.idtipointermediario(+) "+
                        "    AND cs.ig_riesgo_ac = tiac.idtipointermediario(+) "+
                        "    AND cs.ic_solic_esp = ? ";
		    
			log.debug ("qrySentencia    "+ qrySentencia);
		    
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_solicitudS);
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setNumero_solicitud((rs.getString("numero_solicitud")==null)?"":rs.getString("numero_solicitud"));
				sol.setDf_solicitud((rs.getString("fecha_solicitud")==null)?"":rs.getString("fecha_solicitud"));
				sol.setHora_solicitud((rs.getString("hora_solicitud")==null)?"":rs.getString("hora_solicitud"));
				sol.setIg_num_referencia((rs.getString("num_referencia")==null)?"":rs.getString("num_referencia"));
				sol.setIg_tasa_md((rs.getString("tasa_mismo_dia")==null)?"":rs.getString("tasa_mismo_dia"));
				sol.setIg_tasa_spot((rs.getString("tasa_spot")==null)?"":rs.getString("tasa_spot"));
				sol.setDf_cotizacion((rs.getString("fecha_cotizacion")==null)?"":rs.getString("fecha_cotizacion"));
				sol.setHora_cotizacion((rs.getString("hora_cotizacion")==null)?"":rs.getString("hora_cotizacion"));
				sol.setNombre_ejecutivo((rs.getString("ejecutivo")==null)?"":rs.getString("ejecutivo"));
				sol.setCg_mail((rs.getString("correo_electronico")==null)?"":rs.getString("correo_electronico"));
				sol.setCg_telefono((rs.getString("telefono")==null)?"":rs.getString("telefono"));
				sol.setCg_fax((rs.getString("fax")==null)?"":rs.getString("fax"));
				sol.setFn_monto((rs.getDouble("monto_total")==0.0)?0.0:rs.getDouble("monto_total"));
				sol.setTipo_tasa((rs.getString("tipo_tasa")==null)?"":rs.getString("tipo_tasa"));
				sol.setIn_num_disp((rs.getString("num_disposiciones")==null)?"":rs.getString("num_disposiciones"));
				sol.setCs_tasa_unica((rs.getString("disposi_multiples")==null)?"":rs.getString("disposi_multiples"));
				sol.setIg_plazo((rs.getString("plazo_operacion")==null)?"":rs.getString("plazo_operacion"));
				sol.setIc_esquema_recup((rs.getString("ic_esquema_recup")==null)?"":rs.getString("ic_esquema_recup"));
				sol.setEsquema_recup((rs.getString("esquema_recuperacion")==null)?"":rs.getString("esquema_recuperacion"));
				sol.setIg_ppi((rs.getString("periodicidad_pagos1")==null)?"":rs.getString("periodicidad_pagos1"));
				sol.setCg_ut_ppi((rs.getString("periodicidad_pagos2")==null)?"":rs.getString("periodicidad_pagos2"));
				sol.setIg_ppc((rs.getString("periodicidad_capital1")==null)?"":rs.getString("periodicidad_capital1"));
				sol.setCg_ut_ppc((rs.getString("periodicidad_capital2")==null)?"":rs.getString("periodicidad_capital2"));
				sol.setIg_periodos_gracia((rs.getString("numero_periodos1")==null)?"":rs.getString("numero_periodos1"));
				sol.setCg_ut_periodos_gracia((rs.getString("numero_periodos2")==null)?"":rs.getString("numero_periodos2"));
				sol.setIn_dia_pago((rs.getString("dia_pago")==null)?"":rs.getString("dia_pago"));
				sol.setCg_observaciones((rs.getString("cg_observaciones")==null)?"":rs.getString("cg_observaciones"));
				sol.setIc_moneda((rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda"));
				sol.setCs_aceptado_md((rs.getString("cs_aceptado_md")==null)?"":rs.getString("cs_aceptado_md"));
				sol.setCs_vobo_ejec((rs.getString("cs_vobo_ejec")==null)?"N":rs.getString("cs_vobo_ejec"));
				sol.setSig_dia_habil((rs.getString("sig_dia_habil")==null)?"N":rs.getString("sig_dia_habil"));
				sol.setSig_dia_habil_spot((rs.getString("sig_dia_habil_spot")==null)?"N":rs.getString("sig_dia_habil_spot"));
				sol.setCg_acreditado((rs.getString("cg_acreditado")==null)?"":rs.getString("cg_acreditado"));
				sol.setDf_aceptacion((rs.getString("fecha_aceptacion")==null)?"":rs.getString("fecha_aceptacion"));
				sol.setIc_tipo_tasa((rs.getString("ic_tipo_tasa")==null)?"":rs.getString("ic_tipo_tasa"));
				sol.setTipoCotizacion((rs.getString("CG_TIPO_COTIZACION")==null)?"":rs.getString("CG_TIPO_COTIZACION"));
				sol.setCg_programa((rs.getString("CG_PROGRAMA")==null)?"":rs.getString("CG_PROGRAMA"));
				sol.setIc_usuario((rs.getString("IC_USUARIO")==null)?"":rs.getString("IC_USUARIO"));
				sol.setNombre_usuario((rs.getString("cg_nombre_usuario")==null)?"":rs.getString("cg_nombre_usuario"));
				sol.setNombre_if_ci((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
				sol.setNombre_if_cs((rs.getString("cg_razon_social_if")==null)?"":rs.getString("cg_razon_social_if"));
				sol.setCg_razon_social_if((rs.getString("cg_razon_social_if")==null)?"":rs.getString("cg_razon_social_if"));
				if(sol.getCg_razon_social_if()==null||sol.getCg_razon_social_if().equals(""))
					sol.setCg_razon_social_if((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
				sol.setRiesgoIf((rs.getString("riesgoif")==null)?"":rs.getString("riesgoif"));
				sol.setRiesgoAc((rs.getString("riesgoac")==null)?"":rs.getString("riesgoac"));
				sol.setIg_riesgo_if((rs.getString("ig_riesgo_if")==null)?"":rs.getString("ig_riesgo_if"));
				sol.setIc_ejecutivo((rs.getString("ic_ejecutivo")==null)?"":rs.getString("ic_ejecutivo"));
				sol.setIc_if((rs.getString("ic_if")==null)?"":rs.getString("ic_if"));
				sol.setCg_pfpc((rs.getString("pfpc")==null)?"":rs.getString("pfpc"));
				sol.setCg_pufpc((rs.getString("pufpc")==null)?"":rs.getString("pufpc"));
				sol.setIcEstatus((rs.getString("icEstatus")==null)?"":rs.getString("icEstatus"));
				

			} else
				throw new NafinException("SIST0001");
			ps.close();
			rs.close();
			//Detalle de Disposiciones

			qrySentencia = "";

			qrySentencia =
						" SELECT ic_disposicion, fn_monto, TO_CHAR (df_disposicion, 'dd/mm/yyyy') as df_disposicion, TO_CHAR (df_vencimiento, 'dd/mm/yyyy') as df_vencimiento "+
						"   FROM cot_disposicion "+
						"  WHERE ic_solic_esp = ? ";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_solicitudS);
			rs = ps.executeQuery();

			int numero_dispociones = Integer.parseInt(sol.getIn_num_disp());
			String fn_monto_disp[]			= new String[numero_dispociones];
			String df_disposicion[]			= new String[numero_dispociones];
			String df_vencimiento[]			= new String[numero_dispociones];
			int i=0;
			while(rs.next()){
				fn_monto_disp[i] = ((rs.getString("fn_monto")==null)?"":rs.getString("fn_monto"));
				df_disposicion[i] =((rs.getString("df_disposicion")==null)?"":rs.getString("df_disposicion"));
				df_vencimiento[i] =((rs.getString("df_vencimiento")==null)?"":rs.getString("df_vencimiento"));
				i++;
			}
			sol.setFn_monto_disp(fn_monto_disp);
			sol.setDf_disposicion(df_disposicion);
			sol.setDf_vencimiento(df_vencimiento);
			ps.close();
			rs.close();

			//Plan de pagos Capital
			qrySentencia = "";
			qrySentencia =
						" SELECT ic_pago, TO_CHAR (df_pago, 'dd/mm/yyyy'), fn_monto "+
						"  FROM cot_pago "+
						" WHERE ic_solic_esp = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_solicitudS);
			rs = ps.executeQuery();
			ArrayList alPagos = new ArrayList();

			while(rs.next()){
				ArrayList alColumnas = new ArrayList();
				alColumnas.add(rs.getString(1));
				alColumnas.add(rs.getString(2));
				alColumnas.add(rs.getString(3));
				alPagos.add(alColumnas);
			}
			sol.setPagos(alPagos);
			rs.close();
			ps.close();
			rs = null;
			ps = null;

		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("consultaSolicitud(S)");
		}
		return sol;
	}//consultaSolicitud

	public ArrayList obtenerListaIF(String ic_usuario) throws NafinException{

		log.info("obtenerListaIF(E)");

		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		String area ="";
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList Al_Ifs = new ArrayList();
		boolean ejecutivoEsDirector=false;

		try{
			con.conexionDB();

			qrySentencia =
						" select ic_area from cotcat_ejecutivo where ic_usuario = ? and cs_director='S' ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_usuario);
			rs = ps.executeQuery();
			if (rs.next()){
				ejecutivoEsDirector = true;
				area = rs.getString("ic_area");
			}
			rs.close();
			ps.close();

			qrySentencia =
						" SELECT ci.ic_if as ic_if,ci.cg_razon_social as cg_razon_social "+
						"   FROM cotcat_ejecutivo ce, cotrel_if_ejecutivo cie, comcat_if ci "+
						"  WHERE ce.ic_ejecutivo = cie.ic_ejecutivo "+
						"    AND cie.ic_if = ci.ic_if ";
			if(ejecutivoEsDirector)//para el caso en el que el usuario sea director, se obtinene los IFs de de los usuarios del area
				qrySentencia +=
							" AND ce.ic_usuario in (select ic_usuario from cotcat_ejecutivo where ic_area = ?) ";
			else{//solo los IFs de el usuario
				qrySentencia +=
							"    AND ce.ic_usuario = ? ";

			}
			ps = con.queryPrecompilado(qrySentencia);

			if(ejecutivoEsDirector)
				ps.setString(1,area);
			else
				ps.setString(1,ic_usuario);

			rs = ps.executeQuery();
			String intermediario="";
			while (rs.next()){
				intermediario = rs.getString("ic_if")+ "|"+ rs.getString("cg_razon_social");
				Al_Ifs.add(intermediario);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("obtenerListaIF(S)");
		}
		return Al_Ifs;
	}

	public ArrayList obtenerListaTodosIFCotizador() throws NafinException{

		log.info("obtenerListaTodosIFCotizador(E)");

		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList Al_Ifs = new ArrayList();

		try{
			con.conexionDB();

			qrySentencia =
						"	SELECT ic_if,cg_razon_social FROM comcat_if WHERE idtipointermediario IS NOT NULL "+
						"	order by cg_razon_social";

			ps = con.queryPrecompilado(qrySentencia);

			rs = ps.executeQuery();
			String intermediario="";
			while (rs.next()){
				intermediario = rs.getString("ic_if")+ "|"+ rs.getString("cg_razon_social");
				Al_Ifs.add(intermediario);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("obtenerListaIF(S)");
		}
		return Al_Ifs;
	}


	public boolean mostrarInstrucciones(String ic_usuario,String ic_if,String tipo_usuario) throws NafinException{

		log.info("mostrarInstrucciones(E)");

		AccesoDB	con				= new AccesoDB();
		String		qrySentencia	= "";
		boolean		mostrarInstr	= true;
		PreparedStatement	ps 		= null;
		ResultSet 			rs 		= null;
		try{
			con.conexionDB();
			if("IF".equals(tipo_usuario)){
				qrySentencia =
					" select cs_instrucciones"+
					" from cotrel_if_ejecutivo"+
					" where ic_if = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_if);
			}else{
				qrySentencia =
					" select cs_instrucciones"+
					" from cotcat_ejecutivo"+
					" where ic_usuario = ?";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,ic_usuario);
			}
			rs = ps.executeQuery();
			if(rs.next()){
				mostrarInstr = "S".equals(rs.getString("cs_instrucciones"));
			}
			rs.close();
			ps.close();
			rs = null;
			ps = null;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("mostrarInstrucciones(S)");
		}
		return mostrarInstr;
	}

	public void modifInstrucciones(String ic_usuario,String ic_if,String tipo_usuario,String cs_instrucciones) throws NafinException{

		log.info("modirInstrucciones(E)");

		AccesoDB	con				= new AccesoDB();
		String		qrySentencia	= "";
		boolean		ok				= true;
		PreparedStatement	ps 		= null;
		try{
			con.conexionDB();
			if(cs_instrucciones!=null){
				if("IF".equals(tipo_usuario)){
					qrySentencia =
						" update cotrel_if_ejecutivo"+
						" set cs_instrucciones = ?"+
						" where ic_if = ?";
					ps = con.queryPrecompilado(qrySentencia);
					ps.setString(1,cs_instrucciones);
					ps.setString(2,ic_if);
				}else{
					qrySentencia =
						" update cotcat_ejecutivo"+
						" set cs_instrucciones = ?"+
						" where ic_usuario = ?";
					ps = con.queryPrecompilado(qrySentencia);
					ps.setString(1,cs_instrucciones);
					ps.setString(2,ic_usuario);
				}
				ps.execute();
				ps.close();
				ps = null;
			}
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("modirInstrucciones(S)");
		}
	}

	public Vector getClaveSolicitud(String strClave)
			throws NafinException{
		log.info("getClaveSolicitud(E)");

		AccesoDB	con				= new AccesoDB();
		String		qrySentencia	= "";
		boolean		ok				= true;
		PreparedStatement	ps 		= null;
		String 		clave			= "";
		String 		estatus			= "";
		String 		intermediario	= "";
		String 		observaciones	= "";
		ResultSet	rs				= null;
		Vector		detalles		= new Vector();
		try{
			con.conexionDB();
			qrySentencia =
				" SELECT sol.ic_solic_esp, sol.ic_estatus, NVL(cif.cg_razon_social,sol.cg_razon_social_if) as cg_razon_social, sol.cg_observaciones "+
 				" FROM cot_solic_esp sol, comcat_if cif  "+
 				" WHERE sol.ic_if = cif.ic_if(+) " +
 				" AND TO_CHAR (sol.df_solicitud, 'yyyymm') || '-' || sol.ig_num_solic = ? ";

			ps = con.queryPrecompilado(qrySentencia);

			ps.setString(1,strClave);
			rs = ps.executeQuery();
			if(rs.next()) {
				clave 			= rs.getString("ic_solic_esp")==null?"":rs.getString("ic_solic_esp");
				estatus 		= rs.getString("ic_estatus")==null?"":rs.getString("ic_estatus");
				intermediario	= rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social");
				observaciones	= rs.getString("cg_observaciones")==null?"":rs.getString("cg_observaciones");
				detalles.add(clave);
				detalles.add(estatus);
				detalles.add(intermediario);
				detalles.add(observaciones);
			} else
				throw new NafinException("SIST0001");
			rs.close();
			ps.close();

		}catch(Exception e){
			log.info("getClaveSolicitud(Exception)");
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getClaveSolicitud(S)");
		}
		return detalles;
	}

	public void setRechazaTomaFirme(String ic_solic_esp, String cg_observaciones)
			throws NafinException{

		log.info("setRechazaTomaFirme(E)");

		AccesoDB	con				= new AccesoDB();
		String		qrySentencia	= "";
		boolean		ok				= true;
		PreparedStatement	ps 		= null;
		try{
			con.conexionDB();
			qrySentencia =
				" UPDATE cot_solic_esp"   +
				"    SET CG_OBSERVACIONES = substr(?,1,100) "   +
				"        ,ic_estatus = 8"   +
				"        ,df_cotizacion = sysdate"   +
				"  WHERE ic_solic_esp = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, cg_observaciones);
			ps.setString(2, ic_solic_esp);
			ps.execute();
			ps.close();
		}catch(Exception e){
			log.info("setRechazaTomaFirme(Exception)");
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("setRechazaTomaFirme(S)");
		}
	}


	public List getRiesgos(String tipo)
	throws NafinException{
		log.info("getRiesgos(E)");

		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lFilas = new ArrayList();
		List lColumnas = new ArrayList();

		try{
			con.conexionDB();

			qrySentencia =
						" SELECT idtipointermediario,descripcion "+
						" FROM tipointermediario "+
						" WHERE cg_tipo = ?";


			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,tipo);
			rs = ps.executeQuery();
			while (rs.next()){
				lColumnas = new ArrayList();
				lColumnas.add(rs.getString("idtipointermediario"));
				lColumnas.add(rs.getString("descripcion"));
				lFilas.add(lColumnas);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getRiesgos(S)");
		}
		return lFilas;
	}


	public List getCurvasUSD(String icMoneda)
	throws NafinException{
		log.info("getCurvasUSD(E)");

		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lFilas = new ArrayList();
		List lColumnas = new ArrayList();

		try{
			con.conexionDB();

			qrySentencia =
						" SELECT idcurva,nombre "+
						" FROM curva "+
						" WHERE ic_moneda = ?"+
						" AND cs_fondeo = 'S'";


			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,icMoneda);
			rs = ps.executeQuery();
			while (rs.next()){
				lColumnas = new ArrayList();
				lColumnas.add(rs.getString("idcurva"));
				lColumnas.add(rs.getString("nombre"));
				lFilas.add(lColumnas);
			}
			if(lFilas.size()==0){
				lColumnas = new ArrayList();
				lColumnas.add("");
				lColumnas.add("--Seleccione Moneda--");
				lFilas.add(lColumnas);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getCurvasUSD(S)");
		}
		return lFilas;
	}


	public Solicitud consultaCotizacionCalc(String noreferencia)
		throws NafinException{

		log.info("consultaCotizacionCalc(E)");

		AccesoDB con = new AccesoDB();
		Solicitud sol= new Solicitud();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			con.conexionDB();
			// DATOS DE LA COTIZACION Y Datos del ejecutivo nafin
			qrySentencia =
				"  select to_char(cre.fechacotizacion,'dd/mm/yyyy') as fecha_cotizacion"   +
				"  ,to_char(cre.fechacotizacion,'hh:mi:ss') as hora_cotizacion"   +
				"  ,cre.acuserecibo"   +
				"  ,cre.noreferencia"   +
				"  ,e.ejecutivo"   +
				"  ,e.correo_electronico"   +
				"  ,e.telefono"   +
				"  ,e.fax"   +
				"  ,cre.intermediario"   +
				"  ,cre.acreditado"   +
				"  ,rif.descripcion as riesgoif"   +
				"  ,to_char(cre.fechadisposicion,'dd/mm/yyyy') as fecha_disposicion"   +
				"  ,plazo||' '||ut.descripcion||' equivalentes a '||cre.duracioncreditodias||' dias'  as plazo"   +
				"  ,monto"   +
				"  ,case when periodosgracia is not null then 'Tipo Renta'"   +
				"      when periodograciacapital is not null then 'Tradicional'"   +
				"      else 'Cupon cero'"   +
				"      end as esquema"   +
				"  ,tasanetaconvigencia*100 as tasa"   +
				"  ,numeropagosintereses"   +
				"  ,ut.descripcion as utppi"   +
				"  ,numeropagoscapital"   +
				"  ,nvl(periodosgracia,periodograciacapital) as periodosgracia"   +
				"  ,usuario"   +
				"  from credito cre"   +
				"  , unidadtiempo ut"   +
				"  , statuscotizacion ec"   +
				"  ,(select"   +
				"     nvl(ve.cg_nombre,'')||' '||nvl(ve.cg_appaterno,'')||' '||nvl(ve.cg_apmaterno,'') as ejecutivo"   +
				"    ,ve.cg_mail AS correo_electronico"   +
				"    ,ve.cg_telefono AS telefono"   +
				"    ,ve.cg_fax AS fax"   +
				"    ,vI.cg_razon_social"   +
				"    from cotcat_ejecutivo ve"   +
				"    ,cotrel_if_ejecutivo vie"   +
				"    ,comcat_if vI"   +
				"    where vi.ic_if = vie.ic_if"   +
				"    and ve.IC_EJECUTIVO= vie.ic_ejecutivo) e"   +
				"  , tipointermediario rif"   +
				"  where cre.idunidadtiempo = ut.idunidadtiempo"   +
				"  and cre.idstatus = ec.idstatus"   +
				"  and cre.intermediario = e.cg_razon_social(+)"   +
				"  and cre.idtipointermediario = rif.idtipointermediario"   +
				" AND cre.noreferencia = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,noreferencia);
			rs = ps.executeQuery();
			if(rs.next()){
				sol.setNumero_solicitud((rs.getString("noreferencia")==null)?"":rs.getString("noreferencia"));
				sol.setDf_cotizacion((rs.getString("fecha_cotizacion")==null)?"":rs.getString("fecha_cotizacion"));
				sol.setHora_cotizacion((rs.getString("hora_cotizacion")==null)?"":rs.getString("hora_cotizacion"));
				sol.setDf_solicitud((rs.getString("fecha_disposicion")==null)?"":rs.getString("fecha_disposicion"));
				sol.setIc_usuario((rs.getString("USUARIO")==null)?"":rs.getString("USUARIO"));
				sol.setIg_num_referencia((rs.getString("acuserecibo")==null)?"":rs.getString("acuserecibo"));
				sol.setNombre_ejecutivo((rs.getString("ejecutivo")==null)?"":rs.getString("ejecutivo"));
				sol.setCg_mail((rs.getString("correo_electronico")==null)?"":rs.getString("correo_electronico"));
				sol.setCg_telefono((rs.getString("telefono")==null)?"":rs.getString("telefono"));
				sol.setCg_fax((rs.getString("fax")==null)?"":rs.getString("fax"));
				sol.setNombre_if_cs((rs.getString("intermediario")==null)?"":rs.getString("intermediario"));
				sol.setCg_acreditado((rs.getString("acreditado")==null)?"":rs.getString("acreditado"));
				sol.setRiesgoIf((rs.getString("riesgoif")==null)?"":rs.getString("riesgoif"));
				sol.setCg_ut_ppc((rs.getString("plazo")==null)?"":rs.getString("plazo"));
				sol.setFn_monto(rs.getDouble("monto"));
				sol.setEsquema_recup((rs.getString("esquema")==null)?"":rs.getString("esquema"));
				sol.setIg_tasa_md((rs.getString("tasa")==null)?"":rs.getString("tasa"));
				sol.setIg_ppi((rs.getString("numeropagosintereses")==null)?"":rs.getString("numeropagosintereses"));
				sol.setCg_ut_ppi((rs.getString("utppi")==null)?"":rs.getString("utppi"));
				sol.setIg_ppc((rs.getString("numeropagoscapital")==null)?"":rs.getString("numeropagoscapital"));
				sol.setIg_periodos_gracia((rs.getString("periodosgracia")==null)?"":rs.getString("periodosgracia"));
				sol.setCg_ut_periodos_gracia((rs.getString("utppi")==null)?"":rs.getString("utppi"));
			} else
				throw new NafinException("SIST0001");
			ps.close();
			rs.close();


		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("consultaCotizacionCalc(S)");
		}
		return sol;
	}//consultaSolicitud


	public void guardaCotizacionUSD(Solicitud sol,String nombreUsuario, String icUsuario)
		throws NafinException{

		log.info("guardaCotizacionUSD(E)");

		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		PreparedStatement ps = null;
		ResultSet	rs = null;
		boolean		ok = true;
		try{
			int		icCalc	= 0;
			String	area	= "";
			con.conexionDB();

			qrySentencia =
				" select nvl(max(ic_calc_usd),0)+1 from cot_calc_usd"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				icCalc = rs.getInt(1);
			}
			rs.close();
			ps.close();

			qrySentencia =
				" select a.cg_descripcion as area"+
				" from cotcat_ejecutivo e"+
				" ,cotcat_area a"+
				" where e.ic_area = a.ic_area"+
				" and e.ic_ejecutivo = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIc_ejecutivo());
			rs = ps.executeQuery();
			if(rs.next()){
				area = rs.getString("area");
			}
			rs.close();
			ps.close();

			if(area==null||area.equals(""))
				area = "TESOR";

			qrySentencia =
				"insert into cot_calc_usd"+
				" (IC_CALC_USD, CG_NOMBRE_USUARIO, IG_NUM_REFERENCIA, IC_USUARIO"   +
				" , CG_AREA, CG_RAZON_SOCIAL_IF, IDTIPOINTERMEDIARIO"   +
				" , CG_ACREDITADO, FN_MONTO, IG_PLAZO_CRED, CG_UT_PLAZO"   +
				" , IG_PLAZO_CRED_DIAS, IG_TASA_MD, IG_PPI, IG_PPC"   +
				" , IG_PERIODOS_GRACIA, IDCURVA, CG_PROGRAMA, IC_TIPO_TASA"   +
				" , IN_NUM_DISP, IC_ESQUEMA_RECUP, CG_UT_PPI, IG_DIA_PAGO"   +
				" , CG_UT_PPC, CG_UT_PERIODOS_GRACIA, IC_ESTATUS, IG_VALOR_RIESGO_AC"   +
				" , IG_RIESGO_IF, CG_OBSERVACIONES, CG_OBSERVACIONES_SIRAC, IG_PPV_DIAS"   +
				" , IG_COSTO_FONDEO, IG_IMPUESTO, IG_FACTORES_TOTALES,DF_DISPOSICION,DF_COTIZACION)"  +
				" VALUES(?, ?, ?, ?"   +
				" , ?, ?, ?"   +
				" , ?, ?, ?, ?"   +
				" , ?, ?, ?, ?"   +
				" , ?, ?, ?, ?"   +
				" , ?, ?, ?, ?"   +
				" , ?, ?, 2, ?"   +
				" , ?, null, null, ?"   +
				" , ?, ?, ?,to_date(?,'dd/mm/yyyy'),to_date(?,'dd/mm/yyyy'))" ;

			double riesgoAc = 0;
			try{
				riesgoAc = Double.parseDouble(sol.getIg_valor_riesgo_ac());
			}catch(Exception e){
				riesgoAc = 0;
			}

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,icCalc);
			ps.setString(2,nombreUsuario);
			ps.setInt(3,icCalc);
			ps.setString(4,icUsuario);
			ps.setString(5,area);
			ps.setString(6,sol.getCg_razon_social_if());
			ps.setString(7,sol.getIg_riesgo_if());
			ps.setString(8,sol.getCg_acreditado());
			ps.setDouble(9,sol.getFn_monto());
			ps.setInt(10,sol.getIg_plazo_conv());
			ps.setString(11,sol.getCg_ut_plazo());
			ps.setString(12,sol.getIg_plazo());
			ps.setDouble(13,sol.getTasaCreditoUSD()/100);
			ps.setString(14,sol.getIg_ppi());
			ps.setString(15,sol.getIg_ppc());
			ps.setString(16,sol.getIg_periodos_gracia());
			ps.setString(17,sol.getIdCurva());
			ps.setString(18,sol.getCg_programa());
			ps.setString(19,sol.getIc_tipo_tasa());
			ps.setString(20,sol.getIn_num_disp());
			ps.setString(21,sol.getIc_esquema_recup());
			ps.setString(22,sol.getCg_ut_ppi());
			ps.setString(23,sol.getIn_dia_pago());
			ps.setString(24,sol.getCg_ut_ppc());
			ps.setString(25,sol.getCg_ut_periodos_gracia());
			ps.setDouble(26,riesgoAc);
			ps.setString(27,sol.getIg_riesgo_if());
			ps.setDouble(28,sol.getPPV());
			ps.setDouble(29,sol.getTRE()/100);
			ps.setDouble(30,sol.getImpuestoUSD()/100);
			ps.setDouble(31,sol.getFactoresRiesgoUSD()/100);
			ps.setString(32,sol.getDf_disposicion(0));
			ps.setString(33,sol.getDf_cotizacion());

			ps.execute();
			ps.close();
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("guardaCotizacionUSD(S)");
		}
	}

	/*
		METODO: CALCULO TASAS USD.
		ORIGINALMENTE SE PENS� PARA LA CALCULADORA EN DOLARES, PERO EN EL FODA 8 2006 SE LE IMPLEMENT� TAMBI�N FUNCIONALIDAD EN PESOS, POR ESO EL NOMBRE
		Par�metros: Solicitud sol.
			En este objeto vienen todos los datos que se capturaron en la forma
		Retorno: Solicitud sol
			Es el mismo objeto que se envi� como par�metro, incluye los mismos datos de la cotizaci�n que
			se capturaron en la forma y dentro del m�todo se le agregan los valores de las tasas que se calcularon.
	*/

	public Solicitud CalculoTasasUSD(Solicitud sol, boolean bandera)
		throws NafinException{
		log.info("CalculoTasasUSD(E)");
		AccesoDB con = new AccesoDB();
		try{
			PreparedStatement	ps				= null;
			ResultSet			rs				= null;
			String 				qrySentencia	= "";
			Curva				cur 			= new Curva();	//ESTA CLASE GUARDA LOS DATOS DE LA CURVA Y HACE LOS C�LCULOS DE LAS INTERPOLACIONES
			String				metodoCalc		= "P";
			String				metodoInter		= "L";
			con.conexionDB();
			int		esq = Integer.parseInt(sol.getIc_esquema_recup());
			// Se obtiene el valor de pantalla, ya no de base de datos
			metodoCalc = sol.getMetodoCalculo();
			metodoInter = sol.getId_interpolacion();

			//OBTIENE LOS DATOS DE LA CURVA Y LOS GUARDA EN EL OBJETO CUR.

			qrySentencia =
				" select distinct plazo,tasa"+
				" from datoscurva"+
				" where idcurva = ?"+
				" and fecha = ?"+
				" order by plazo";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIdCurva());
			ps.setString(2,sol.getDf_cotizacion().substring(6,10)+sol.getDf_cotizacion().substring(3,5)+sol.getDf_cotizacion().substring(0,2));
			rs = ps.executeQuery();
			while(rs.next()){
				cur.add(rs.getInt("plazo"),rs.getDouble("tasa"));
			}
			rs.close();
			ps.close();

			//OBTIENE LOS METODOS DE CALCULO Y METODOS DE INTERPOLACION Y LOS METE EN LOS OBJETOS SOL Y CUR
			qrySentencia =
				" select m.cd_nombre"   +
				" ,p.cg_metodo_inter"+
				" ,p.cg_metodo_calc"+
				" from comcat_moneda m"   +
				" ,parametrossistema p"+
				" where m.ic_moneda = p.ic_moneda" +
				" and m.ic_moneda = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIc_moneda());
			rs = ps.executeQuery();
			while(rs.next()){
				sol.setNombre_moneda((rs.getString("cd_nombre")==null)?"":rs.getString("cd_nombre"));
				//metodoInter	=	((rs.getString("cg_metodo_inter")==null)?"L":rs.getString("cg_metodo_inter"));
				//metodoCalc	=	((rs.getString("cg_metodo_calc")==null)?"P":rs.getString("cg_metodo_calc"));
				cur.setMetodoCalculo(metodoCalc);
				//sol.setMetodoCalculo(metodoCalc);
				cur.setMetodo(metodoInter);
			}
			rs.close();
			ps.close();

			if(!(sol.getIc_moneda().equals(""))){
				qrySentencia =
					" select ig_decimales"+		//numero de decimales con que se redondea la pantalla
					" ,ig_tasa_impuesto_dl"+	//calculo de la tasa impuesto OJO: es para MN y Dolares y moneda nacional, originalmetne se pens� solo para dolares, por eso el nombre del campo
					" from parametrossistema where ic_moneda = ? ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,sol.getIc_moneda());
				rs = ps.executeQuery();

				if(rs.next()){
					sol.setNumDec(rs.getInt("ig_decimales"));
					sol.setImpuestoUSD(rs.getDouble("ig_tasa_impuesto_dl"));
				}
				rs.close();
				ps.close();
			}

			if(sol.getIg_tipo_piso().equals("1")){		//si es de primer piso el factor de riesgo se captura en la forma
				sol.setFactoresRiesgoUSD(Double.parseDouble(sol.getIg_valor_riesgo_ac()));
			}else{										//si es de segundo piso se obtienen los factores de riesgo de la base de datos
				qrySentencia =
					" SELECT RIESGO*100 AS RIESGOIF"+
					" FROM FACTORESRIESGO"+
					" WHERE PLAZO >=?"+
					" AND IDTIPOINTERMEDIARIO = ?"+
					" AND IC_MONEDA = ?"+
					" ORDER BY PLAZO";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(sol.getIg_plazo()));
				ps.setString(2,sol.getIg_riesgo_if());
				ps.setString(3,sol.getIc_moneda());
				rs = ps.executeQuery();
				if(rs.next()){
					sol.setFactoresRiesgoUSD(rs.getDouble("RIESGOIF"));
				}
				rs.close();
				ps.close();

				// En caso de que el plazo sea mayor al maximo plazo en tabla
				// se asignara el plazo maximo que se tenga, para que no sea un 0
				if(sol.getFactoresRiesgoUSD()==0.0){
					qrySentencia =
					" SELECT (max(RIESGO))*100 AS RIESGOIF"+
					" FROM FACTORESRIESGO"+
					" WHERE IDTIPOINTERMEDIARIO = ?"+
					" AND IC_MONEDA = ?"+
					" ORDER BY PLAZO";
					ps = con.queryPrecompilado(qrySentencia);
					ps.setString(1,sol.getIg_riesgo_if());
					ps.setString(2,sol.getIc_moneda());
					rs = ps.executeQuery();
					if(rs.next()){
						sol.setFactoresRiesgoUSD(rs.getDouble("RIESGOIF"));
					}
					rs.close();
					ps.close();
				}

			}
			//SE REALIZA EL CALCULO DEPENDIENDO DEL METODO (PPV O DURACION)
			if("P".equals(metodoCalc) || ("U".equals(metodoCalc) && bandera)){
				metodoPPV(sol,cur,con);			//plazo promedio de vida
			}else if("D".equals(metodoCalc) || ("U".equals(metodoCalc) && !bandera)){
				metodoDuracion(sol,cur,con);	//antes duracion, ahora flujos
			}
			calculoVigencias(sol,cur,con);
			calculoSwap(sol,cur,con);
			sol.setCostoAllIn(sol.getTREConAcarreo()*(1+(sol.getImpuestoUSD()/100)));
			sol.setTasaCreditoUSD(sol.getCostoAllIn()+sol.getFactoresRiesgoUSD()-sol.getTasaSWAP());
			switch (esq){
				case 1:
					sol.setDuracion(Integer.parseInt(sol.getIg_plazo()));
				break;
				case 2:
					duracionTradicional(sol,cur);
				break;
				case 3:
					duracionPlanPagos(sol,cur,con);
				break;
				case 4:
					duracionTipoRenta(sol,cur);
				break;
			}
			if("U".equals(metodoCalc) && !bandera){
				CalculoTasasUSD(sol,true);
			}
		}catch(NafinException ne){
			ne.printStackTrace();
			throw ne;
		}catch(Exception e){
			log.info("CalculoTasasUSD(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("CalculoTasasUSD(S)");
		}
		return sol;
	}


	private void metodoDuracion(Solicitud sol,Curva cur,AccesoDB con)
		throws NafinException{
		log.info("metodoDuracion(E)");
		try{
			int esq = Integer.parseInt(sol.getIc_esquema_recup());
			List				lDias			= new ArrayList();
			List				lMontos			= new ArrayList();
			double				min				= 0;
			double				max				= cur.max();
			double				tasaObj			= 0;
			double 				monto 			= sol.getFn_monto();
			double				vcMin			= 0;
			double				vcMax			= 0;
			double				vcTasaObj		= 0;
			int i = 0;
			int numDisp			= Integer.parseInt(sol.getIn_num_disp());

			switch(esq){
				case 1:		//cupon cero
					if(lDias.size()==0)
						lDias.add(new Integer(sol.getIg_plazo()));
					if(lMontos.size()==0)
						lMontos.add(new Double(monto));
					break;
				case 2:		//tradicional
					if(lDias.size()==0){

						int ppc = Integer.parseInt(sol.getCg_ut_ppc());

						if(ppc != 0){
					 		lDias = getPagosIntTradpi(sol.getDf_disposicion(0),sol.getCg_pfpc(),sol.getCg_pufpc(),sol.getDf_vencimiento(0),sol.getCg_ut_ppi(),sol.getIn_dia_pago());
					 	}else{
					 		lDias = getDiasAPagar(sol);
					 	}

						for(i=0;i<lDias.size();i++){
							if((numDisp>1)&&(i<numDisp-1)){
								lMontos.add(new Double(0));
							}else{

								if(ppc==0&&i<lDias.size()-1){
									lMontos.add(new Double(0));
								}else if(ppc==0){
									lMontos.add(new Double(monto));
								}
							}
						}
						if(ppc != 0){
							int numPagosCap = Integer.parseInt(sol.getIg_ppc());
							//log.debug("metodoDuracion:::numPagosCap::: "+numPagosCap);
							int incrementa = 0;
							double pagosi = monto / numPagosCap;
							int aux = 0;
							int pos = 0;
							int incremento = 1;
							if("60".equals(sol.getCg_ut_ppc()))
							 	incremento = 2;
							else if("90".equals(sol.getCg_ut_ppc()))
							 	incremento = 3;
							else if("180".equals(sol.getCg_ut_ppc()))
								incremento = 6;
							else if("360".equals(sol.getCg_ut_ppc()))
								incremento = 12;

							int incrementoint = 1;
							if("60".equals(sol.getCg_ut_ppi()))
							 	incrementoint = 2;
							else if("90".equals(sol.getCg_ut_ppi()))
							 	incrementoint = 3;
							else if("180".equals(sol.getCg_ut_ppi()))
								incrementoint = 6;
							else if("360".equals(sol.getCg_ut_ppi()))
								incrementoint = 12;

							//lMontos.add(new Double(monto/(lDias.size()-(numDisp-1))));
							for(aux=0;aux<lDias.size();aux++){
								lMontos.add(new Double(0));
							}
							int puf = restaFechas(sol.getDf_disposicion(0),sol.getCg_pufpc());
							//log.debug("puf[[[[[ "+puf);
							for(int a=0;a<lDias.size()-1;a++){
								if((Integer.parseInt(lDias.get(a).toString()) == puf)){
									pos = a;
								}
							}
							//log.debug("metodoDuracion::::pos::: "+pos);
							//pos = lDias.size()-1;
							/*int pf = restaFechas(sol.getDf_disposicion(0),sol.getCg_pfpc());
							log.debug("pf[[[[[ "+pf);
							int puf = restaFechas(sol.getDf_disposicion(0),sol.getCg_pufpc());
							log.debug("puf[[[[[ "+puf);*/
							while(incrementa<numPagosCap){
									if(incrementa == 0){
										lMontos.set(lDias.size()-1,new Double(pagosi));
									}else{
										lMontos.set(pos,new Double(pagosi));
										pos = pos - (incremento/incrementoint);
									}

								incrementa++;
							}
							/*int pf = restaFechas(sol.getDf_disposicion(0),sol.getCg_pfpc());
							log.debug("pf[[[[[ "+pf);
							int puf = restaFechas(sol.getDf_disposicion(0),sol.getCg_pufpc());
							log.debug("puf[[[[[ "+puf);
							for(int a=0;a<lDias.size()-1;a++){
								if((Integer.parseInt(lDias.get(a).toString()) < pf)){
									lMontos.set(a,new Double(0));
								}else if((Integer.parseInt(lDias.get(a).toString()) > puf) && (a != lDias.size()-1)){
									lMontos.set(a,new Double(0));
								}
							}*/
						}
					}
					break;
				case 3:		//plan de pagos
					if(lDias.size()==0){
						Hashtable	htpp	= getDiasPorVencerPlanPagosDur(sol,con);
						lDias	= (List)htpp.get("dias");
						lMontos	= (List)htpp.get("montos");
					}
					break;
				case 4:		//tipo renta
					min = 0.001;		//no puede ser cero porque implicar�a divisiones entre cero
					if(lDias.size()==0)
						lDias	= getDiasPorVencerTrad(sol);
					break;
			}
			//log.debug("metodoDuracion:::lDias::: "+lDias);
			//log.debug("metodoDuracion:::lMontos::: "+lMontos);
			double vcTasaObjAnt = 0;
			//log.debug("max:::: "+max);
			do{
				vcTasaObjAnt = vcTasaObj;
				tasaObj = (max+min)/2;
				vcTasaObj	= calculaValorControlSumaVpn(sol,cur,lDias,lMontos,tasaObj);
				if(vcTasaObj!=0){
					vcMin		= calculaValorControlSumaVpn(sol,cur,lDias,lMontos,min);
					vcMax		= calculaValorControlSumaVpn(sol,cur,lDias,lMontos,max);
					if(Math.abs(vcMin)>Math.abs(vcMax)){
						min = tasaObj;
					}else{
						max = tasaObj;
					}
				}
			}while(vcTasaObjAnt!=vcTasaObj);
			//log.debug("tasaObj metodo duracion[[[[ "+tasaObj);
			tasaObj *= 100;
			sol.setTRE(tasaObj);

			switch(esq){
				case 1:		//cupon cero
					sol.setPPV(Integer.parseInt(sol.getIg_plazo()));
					break;
				case 2:		//tradicional
					ppvTradicional(sol);
					break;
				case 3:		//plan de pagos
					ppvPlanPagos(sol,con);
					break;
				case 4:		//tipo renta
					ppvTipoRenta(sol);
					break;
			}

		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			log.info("metodoDuracion(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			log.info("metodoDuracion(S)");
		}
	}


	private void calculoVigencias(Solicitud sol,Curva cur,AccesoDB con)
		throws NafinException{
		log.info("calculoVigencias(E)");
		try{
			int esq = Integer.parseInt(sol.getIc_esquema_recup());
			PreparedStatement	ps				= null;
			ResultSet			rs				= null;
			String 				qrySentencia	= "";
			List				lDias			= new ArrayList();
			List				lMontos			= new ArrayList();
			Curva				depositos		= new Curva();
			double				min				= 0;
			double				max				= cur.max();
			double				tasaObj			= 0;
			double 				monto 			= sol.getFn_monto();
			double				vcMin			= 0;
			double				vcMax			= 0;
			double				vcTasaObj		= 0;
			int i = 0;
			int numDisp			= Integer.parseInt(sol.getIn_num_disp());
			double	acarreosDisp	= 0;

			if(numDisp==1&&sol.getDf_disposicion(0).equals(sol.getDf_cotizacion())){
				sol.setTREConAcarreo(sol.getTRE());
				return;
			}

			switch(esq){
				case 1:		//cupon cero
					if(lDias.size()==0)
						lDias.add(new Integer(sol.getIg_plazo()));
					if(lMontos.size()==0)
						lMontos.add(new Double(monto));
					break;
				case 2:		//tradicional
					if(lDias.size()==0){
						lDias	= getDiasPorVencerTrad(sol);
						for(i=0;i<lDias.size();i++){
							if((numDisp>1)&&(i<numDisp-1)){
								lMontos.add(new Double(0));
							}else{
								lMontos.add(new Double(monto/(lDias.size()-(numDisp-1))));
							}
						}
					}
					break;
				case 3:		//plan de pagos
					if(lDias.size()==0){
						Hashtable	htpp	= getDiasPorVencerPlanPagos(sol,con);
						lDias	= (List)htpp.get("dias");
						lMontos	= (List)htpp.get("montos");
					}
					break;
				case 4:		//tipo renta
					if(lDias.size()==0)
						lDias	= getDiasPorVencerTrad(sol);
						lMontos = getMontosTipoRenta(sol,lDias);
					break;
			}
			double vcTasaObjAnt = 0;

			//CALCULO DE LA VIGENCIA
			depositos.setMetodoCalculo(cur.getMetodoCalculo());
			depositos.setMetodo(cur.getMetodo());

			qrySentencia =
				" select distinct "   +
				"   d.plazo"   +
				"  ,d.tasa "   +
				" from "   +
				"  curva c"   +
				" ,datoscurva d"   +
				" where d.idcurva = c.idcurva"   +
				" and c.ic_moneda = ?"   +
				" and d.fecha = ? "   +
				" and c.cs_deposito = 'S'"  +
				" order by d.plazo asc";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,sol.getIc_moneda());
			ps.setString(2,sol.getDf_cotizacion().substring(6,10)+sol.getDf_cotizacion().substring(3,5)+sol.getDf_cotizacion().substring(0,2));
			rs = ps.executeQuery();
			while(rs.next()){
				depositos.add(rs.getInt("plazo"),rs.getDouble("tasa"));
			}
			rs.close();
			ps.close();
			///////////////////////////////
			//////////////////////////////
			acarreosDisp = calculaAcarreoPorDisposicion(sol,depositos,con);
			log.debug("acarreosDisp::: "+acarreosDisp);
			vcTasaObjAnt = 0;
			min = 0;
			max = 1;
			do{
				vcTasaObjAnt = vcTasaObj;
				tasaObj = (max+min)/2;
				vcTasaObj	= calculaValorControlSumaVpnAcarreo(sol,depositos,lDias,lMontos,tasaObj,acarreosDisp);
				if(vcTasaObj!=0){
					vcMin		= calculaValorControlSumaVpnAcarreo(sol,depositos,lDias,lMontos,min,acarreosDisp);
					vcMax		= calculaValorControlSumaVpnAcarreo(sol,depositos,lDias,lMontos,max,acarreosDisp);
					if(Math.abs(vcMin)>Math.abs(vcMax)){
						min = tasaObj;
					}else{
						max = tasaObj;
					}
				}

			}while(vcTasaObjAnt!=vcTasaObj);

			tasaObj *= 100;

			sol.setTREConAcarreo(sol.getTRE()+tasaObj);
			log.debug("calculoVigencias::::sol.getTREConAcarreo()::: "+sol.getTREConAcarreo());
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			log.info("calculoVigencias(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			log.info("calculoVigencias(S)");
		}
	}


	private double calculaValorControlSumaVpnAcarreo(Solicitud sol,Curva cur,List lDias,List lMontos,double tasa,double totAcarreos)
		throws NafinException{
		log.info("calculaValorControlSumaVpnAcarreo(E)");
		BigDecimal sumaVpnAcarreo = new BigDecimal(0);
		try{
			int esq = Integer.parseInt(sol.getIc_esquema_recup());

			double				saldo			= sol.getFn_monto();
			int i = 0;
			int	numDisp			= Integer.parseInt(sol.getIn_num_disp());
			//sol.setTasaCreditoUSD(tasa);

			if(esq==2&&numDisp>1){		//si es tradicional y hay mas disposiciones, las primeras amortizaciones no pagan capital e incrementan el saldo
				saldo = Double.parseDouble(sol.getFn_monto_disp(0));
			}
			for(i=0;i<lMontos.size();i++){
				int periodo = 0;
				double monto		= ((Double)lMontos.get(i)).doubleValue();
				double interes		= 0;
				double acarreo		= 0;
				double tasaPeriodo	= 0;
				double vpnAcarreo		= 0;
				int	   dxv			= ((Integer)lDias.get(i)).intValue();
				if(i==0){
					periodo = dxv;
				}else{
					periodo = dxv-((Integer)lDias.get(i-1)).intValue();
				}

				acarreo = (saldo*periodo*tasa)/360;
				if(esq==2 && numDisp>1 && i<(numDisp-1)){		//si es tradicional y hay mas disposiciones, las primeras amortizaciones no pagan capital e incrementan el saldo
					interes = saldo*(tasa*periodo/360.0);
					saldo += Double.parseDouble(sol.getFn_monto_disp(i+1));
				}else{
					interes = saldo*(tasa*periodo/360.0);
					saldo -= monto;
				}
				cur.setMetodoCalculo(sol.getMetodoCalculo());
				cur.setMetodo(sol.getId_interpolacion());
				cur.setEsquema(sol.getIc_esquema_recup());
				tasaPeriodo = cur.interpolacion(((Integer)lDias.get(i)).doubleValue(),sol.getIg_plazo());

				vpnAcarreo = acarreo/(1+(tasaPeriodo/360.0d)*dxv);
				sumaVpnAcarreo = sumaVpnAcarreo.add(new BigDecimal(vpnAcarreo));
			}
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			log.info("calculaValorControlSumaVpnAcarreo(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}
		log.info("calculaValorControlSumaVpnAcarreo(S)");
		return sumaVpnAcarreo.subtract(new BigDecimal(totAcarreos)).doubleValue();
	}


	private double calculaValorControlSumaVpn(Solicitud sol,Curva cur,List lDias,List lMontos,double tasa)
		throws NafinException{
		//log.info("calculaValorControlSumaVpn(E)");
		BigDecimal sumaVpnFlujo = new BigDecimal(0);
		double 				regreso			= 0;
		try{
			int esq = Integer.parseInt(sol.getIc_esquema_recup());

			double				saldo			= sol.getFn_monto();
			double				renta			= 0;

			int i = 0;
			int	numDisp			= Integer.parseInt(sol.getIn_num_disp());
			sol.setTRE(tasa*100);

			if(esq==4){	// si es tipo renta, los montos dependen de la tasa de inter�s, por lo tanto se debe calcular la lista en cada iteracion
				renta = calculaRenta(sol,lDias);
				lMontos = getMontosTipoRenta(sol,lDias);
			}

			if(esq==2&&numDisp>1){		//si es tradicional y hay mas disposiciones, las primeras amortizaciones no pagan capital e incrementan el saldo
				saldo = Double.parseDouble(sol.getFn_monto_disp(0));
			}
			for(i=0;i<lMontos.size();i++){
				int periodo = 0;
				double monto		= ((Double)lMontos.get(i)).doubleValue();
				double interes		= 0;
				double flujo		= 0;
				double tasaPeriodo	= 0;
				double vpnFlujo		= 0;
				int	   dxv			= ((Integer)lDias.get(i)).intValue();
				if(i==0){
					periodo = dxv;
				}else{
					periodo = dxv-((Integer)lDias.get(i-1)).intValue();
				}

				if(esq==2 && numDisp>1 && i<(numDisp-1)){		//si es tradicional y hay mas disposiciones, las primeras amortizaciones no pagan capital e incrementan el saldo
					interes = saldo*(tasa*periodo/360.0);
					saldo += Double.parseDouble(sol.getFn_monto_disp(i+1));
				}else{
					interes = saldo*(tasa*periodo/360.0);
					saldo -= monto;
				}
				//log.debug("interes::: "+interes);
				if(esq!=4){
					flujo = monto + interes;
				}else{
					flujo = renta;
				}

				cur.setMetodoCalculo(sol.getMetodoCalculo());
				cur.setMetodo(sol.getId_interpolacion());
				cur.setEsquema(sol.getIc_esquema_recup());

				tasaPeriodo = cur.interpolacion(((Integer)lDias.get(i)).doubleValue(),sol.getIg_plazo());
				//log.debug("tasaPeriodo::: "+tasaPeriodo);
				vpnFlujo = flujo/(1+(tasaPeriodo/360.0d)*dxv);
				//log.debug("vpnFlujo::: "+vpnFlujo);
				sumaVpnFlujo = sumaVpnFlujo.add(new BigDecimal(vpnFlujo));
				//log.debug("sumaVpnFlujo::: "+sumaVpnFlujo);
			}
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			log.info("calculaValorControlSumaVpn(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			//log.info("calculaValorControlSumaVpn(S)");
		}

		regreso = sumaVpnFlujo.subtract(new BigDecimal(sol.getFn_monto())).doubleValue();
		//log.debug("regreso[[[[[ "+regreso);
		return regreso;
	}


	private void metodoPPV(Solicitud sol,Curva cur,AccesoDB con)
		throws NafinException{
		log.info("metodoPPV(E)");
		try{

			int esq = Integer.parseInt(sol.getIc_esquema_recup());
			double	tasaInterpolada = 0;
			double	tre				= 0;
			double	tasaFinAnt		= 0;
			double	tasaFin			= 0;
			double	ppInt			= 0;

			switch (esq){
				case 1:
					sol.setPPV(Integer.parseInt(sol.getIg_plazo()));
					ppInt	=	Double.parseDouble(sol.getIg_plazo());
				break;
				case 2:
					ppvTradicional(sol);
					ppInt	= Double.parseDouble(sol.getCg_ut_ppi());
					//log.debug("ppInt:::: "+ppInt);
				break;
				case 3:
					ppvPlanPagos(sol,con);
					ppInt	= Double.parseDouble(sol.getCg_ut_ppi());
				break;
				case 4:		//tipo renta

					if("U".equals(sol.getMetodoCalculo())){
						sol.setTmp_ppv(sol.getPPV());
						sol.setPPV(sol.getDuracion());
					}

					ppInt	= Double.parseDouble(sol.getCg_ut_ppc());

					tasaFin = 7.5;
					sol.setTasaCreditoUSD(7.5);
					int contador = 0;
					while(tasaFinAnt!=tasaFin){
						double plazoCalc = sol.getPPV();

						if(Double.parseDouble(sol.getIg_plazo())<=365){
							if(sol.getPPV() <= 31)
								plazoCalc = 30;
							else if(sol.getPPV() <= 93)
								plazoCalc = 90;
							else if(sol.getPPV() <= 183)
								plazoCalc = 180;
							else
								plazoCalc = 360;
						}

						tasaFinAnt = tasaFin;
						ppvTipoRenta(sol);

						cur.setMetodoCalculo(sol.getMetodoCalculo());
						cur.setMetodo(sol.getId_interpolacion());
						cur.setEsquema(sol.getIc_esquema_recup());

						tasaInterpolada = cur.interpolacion(sol.getPPV(),sol.getIg_plazo());
						tre =100* (Math.pow(((tasaInterpolada*plazoCalc/360)+1),ppInt/plazoCalc)-1)*(360.0/ppInt);
						sol.setTRE(tre);
						sol.setCostoAllIn(tre*(1+(sol.getImpuestoUSD()/100)));
						tasaFin = sol.getCostoAllIn()+sol.getFactoresRiesgoUSD();
						sol.setTasaCreditoUSD(tasaFin);
						contador++;
						if(contador > 1000){
							break;
						}
					}

				break;

			}

			if(esq!=4){

				if("U".equals(sol.getMetodoCalculo())){
					sol.setTmp_ppv(sol.getPPV());
					sol.setPPV(sol.getDuracion());
				}

				double plazoCalc = sol.getPPV();
				//log.debug("plazoCalc:::: "+plazoCalc);

				if(esq == 1){ // foda 86
					if(Double.parseDouble(sol.getIg_plazo())<=365){
						if(sol.getPPV() <= 35)
							plazoCalc = 30;
						else if(sol.getPPV() <= 95)
							plazoCalc = 90;
						else if(sol.getPPV() <= 185)
							plazoCalc = 180;
						else
							plazoCalc = 360;
					}
				}

				//log.debug("plazoCalc:: 1 :: "+plazoCalc);

				cur.setMetodoCalculo(sol.getMetodoCalculo());
				cur.setMetodo(sol.getId_interpolacion());
				cur.setEsquema(sol.getIc_esquema_recup());

				tasaInterpolada = cur.interpolacion(sol.getPPV(),sol.getIg_plazo());
				//log.debug("tasaInterpolada:::: "+tasaInterpolada);

				tre =100* (Math.pow(((tasaInterpolada*plazoCalc/360)+1),ppInt/plazoCalc)-1)*(360.0/ppInt);
				//log.debug("tre:::: "+tre);

				sol.setTRE(tre);

				sol.setCostoAllIn(tre*(1+(sol.getImpuestoUSD()/100)));

				// PARA TASA VARIABLE SE OBTIENE LA TASA SWAP POR INTERPOLACION, SE CALCULA LA TRE  Y SE RESTA A LA TASA DEL CREDITO
				/*if("2".equals(sol.getIc_tipo_tasa())){
					String	fecha				= "";
					String	curva				= "";
					double	tasaInterpoladaSwap	= 0;
					cur = new Curva();

					qrySentencia =
						" select c.idcurva"   +
						" from curva c"   +
						" where c.cs_swap = 'S'"   +
						" and c.ic_moneda = 54";
					ps = con.queryPrecompilado(qrySentencia);
					rs = ps.executeQuery();
					if(rs.next()){
						fecha = sol.getDf_cotizacion().substring(6,10)+sol.getDf_cotizacion().substring(3,5)+sol.getDf_cotizacion().substring(0,2);
						curva = rs.getString("idcurva");
					}
					rs.close();
					ps.close();

					qrySentencia =
						" select distinct plazo,tasa"+
						" from datoscurva"+
						" where idcurva = ?"+
						" and fecha = ?"+
						" order by plazo";
					ps = con.queryPrecompilado(qrySentencia);
					ps.setString(1,curva);
					ps.setString(2,fecha);
					rs = ps.executeQuery();
					while(rs.next()){
						cur.add(rs.getInt("plazo"),rs.getDouble("tasa"));
					}
					rs.close();
					ps.close();
					tasaInterpoladaSwap = cur.interpolacion(sol.getPPV(),sol.getIg_plazo());
					tasaSwap =100* (Math.pow(((tasaInterpoladaSwap*plazoCalc/360)+1),ppInt/plazoCalc)-1)*(360.0/ppInt);

					sol.setTasaSWAP(tasaSwap);
				}*/
			} //esq !=4
			if("U".equals(sol.getMetodoCalculo())){
				sol.setPPV(sol.getTmp_ppv());
			}
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			log.info("metodoPPV(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			log.info("metodoPPV(S)");
		}
	}


	private void calculoSwap(Solicitud sol,Curva cur,AccesoDB con)
		throws NafinException{
		log.info("calculoSWAP(E)");
		try{

			int esq = Integer.parseInt(sol.getIc_esquema_recup());
			double	ppInt			= 0;
			double	tasaSwap		= 0;

			PreparedStatement	ps				= null;
			ResultSet			rs				= null;
			String 				qrySentencia	= "";

			switch (esq){
				case 1:
					ppInt	=	Double.parseDouble(sol.getIg_plazo());
				break;
				case 2:
					ppInt	= Double.parseDouble(sol.getCg_ut_ppi());
				break;
				case 3:
					ppInt	= Double.parseDouble(sol.getCg_ut_ppi());
				break;
				case 4:		//tipo renta
					ppInt	= Double.parseDouble(sol.getCg_ut_ppc());
				break;
			}

			if(esq!=4){
				double plazoCalc = sol.getPPV();

				if(esq == 1){
					if(Double.parseDouble(sol.getIg_plazo())<=365){
						if(sol.getPPV() <= 35)
							plazoCalc = 30;
						else if(sol.getPPV() <= 95)
							plazoCalc = 90;
						else if(sol.getPPV() <= 185)
							plazoCalc = 180;
						else
							plazoCalc = 360;
					}
				}

				// PARA TASA VARIABLE SE OBTIENE LA TASA SWAP POR INTERPOLACION, SE CALCULA LA TRE  Y SE RESTA A LA TASA DEL CREDITO
				if("2".equals(sol.getIc_tipo_tasa())){
					String	fecha				= "";
					String	curva				= "";
					double	tasaInterpoladaSwap	= 0;
					cur = new Curva();

					qrySentencia =
						" select c.idcurva"   +
						" from curva c"   +
						" where c.cs_swap = 'S'"   +
						" and c.ic_moneda = 54";
					ps = con.queryPrecompilado(qrySentencia);
					rs = ps.executeQuery();
					if(rs.next()){
						fecha = sol.getDf_cotizacion().substring(6,10)+sol.getDf_cotizacion().substring(3,5)+sol.getDf_cotizacion().substring(0,2);
						curva = rs.getString("idcurva");
					}
					rs.close();
					ps.close();

					qrySentencia =
						" select distinct plazo,tasa"+
						" from datoscurva"+
						" where idcurva = ?"+
						" and fecha = ?"+
						" order by plazo";
					ps = con.queryPrecompilado(qrySentencia);
					ps.setString(1,curva);
					ps.setString(2,fecha);
					rs = ps.executeQuery();
					while(rs.next()){
						cur.add(rs.getInt("plazo"),rs.getDouble("tasa"));
					}
					cur.setMetodoCalculo(sol.getMetodoCalculo());
					cur.setMetodo(sol.getId_interpolacion());
					cur.setEsquema(sol.getIc_esquema_recup());
					rs.close();
					ps.close();
					tasaInterpoladaSwap = cur.interpolacion(sol.getPPV(),sol.getIg_plazo());
					tasaSwap =100* (Math.pow(((tasaInterpoladaSwap*plazoCalc/360)+1),ppInt/plazoCalc)-1)*(360.0/ppInt);

					sol.setTasaSWAP(tasaSwap);
				}
			} //esq !=4
		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			log.info("calculoSWAP(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			log.info("calculoSWAP(S)");
		}
	}


	private void ppvTradicional(Solicitud sol){
		log.info("ppvTradicional (E)");
		try{
			List		lDias	= getDiasPorVencerTrad(sol);
			double monto	= sol.getFn_monto();
			int numPagosCap	= 1; //foda 89
			int numPagos	= 0;
			String 	utPPC	= sol.getCg_ut_ppc();
			BigDecimal	Ki 		= new BigDecimal(0);
			BigDecimal	sumaKi	= new BigDecimal(0);
			BigDecimal	ppv		= new BigDecimal(0);
			int numDisp		= Integer.parseInt(sol.getIn_num_disp());
			if(utPPC.equals("0")){		//al vencimiento
				sol.setPPV(Integer.parseInt(sol.getIg_plazo()));
				return;
			}else{
				numPagosCap = Integer.parseInt(sol.getIg_ppc());
				//perGracia	= Integer.parseInt(sol.getIg_periodos_gracia()); //foda 89
				/*int pf = restaFechas(sol.getDf_disposicion(0),sol.getCg_pfpc());
				log.debug("pf[[[[[ "+pf);
				int puf = restaFechas(sol.getDf_disposicion(0),sol.getCg_pufpc());
				log.debug("puf[[[[[ "+puf);
				for(int a=0;a<lDias.size()-1;a++){
					if((Integer.parseInt(lDias.get(a).toString()) >= pf) && (Integer.parseInt(lDias.get(a).toString()) <= puf)){
						lAux.add(new Integer(lDias.get(a).toString()));
					}
				}
				lAux.add(new Integer(lDias.get(lDias.size()-1).toString()));
				lDias = new ArrayList();
				lDias = lAux;*/
				//log.debug("ppvTradicional:::lDias:::2::: "+lDias);
			}
			//numPagos	= numPagosCap-perGracia;
			numPagos	= numPagosCap-(numDisp-1);
			Ki = new BigDecimal(monto / numPagos);
			//log.debug("Ki:::: "+Ki);
			for(int i=0;i<lDias.size();i++){
				int dpv = ((Integer)lDias.get(i)).intValue();
				//log.debug("dpv::: "+dpv);
				sumaKi = sumaKi.add(Ki.multiply(new BigDecimal(dpv)));
				//log.debug("sumaKi::: "+sumaKi);
			}
			ppv = sumaKi.divide(new BigDecimal(monto),4,BigDecimal.ROUND_HALF_UP);
			//log.debug("ppv::: "+ppv);
			sol.setPPV(ppv.doubleValue());
			//sol.setPPV(sumaKi.doubleValue());

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("ppvTradicional (S)");
		}
	}

	private void duracionTradicional(Solicitud sol,Curva cur){
		log.info("duracionTradicional (E)");
		try{
			List		lDias	= new ArrayList();//getPagosIntTradpi(sol.getDf_disposicion(0),sol.getCg_pfpc(),sol.getCg_pufpc(),sol.getDf_vencimiento(0),sol.getCg_ut_ppi(),sol.getIn_dia_pago());
			double monto	= sol.getFn_monto();
			int plazo		= sol.getIg_plazo_conv();
			Float.parseFloat(sol.getCg_ut_plazo());
			int numPagosCap	= 0;//lDias.size();
			int numPagos	= 0;
			int numDisp		= 0;
			double tasaPeriodo = 0;
			String 	utPPC	= sol.getCg_ut_ppc();
			BigDecimal	Ki 		= new BigDecimal(0);
			BigDecimal	duracion= new BigDecimal(0);

			BigDecimal	saldoi		= new BigDecimal(monto);
			BigDecimal	inti		= new BigDecimal(0);
			BigDecimal	flujoi		= new BigDecimal(0);
			BigDecimal	vpn			= new BigDecimal(0);
			BigDecimal	vpnDxv		= new BigDecimal(0);
			BigDecimal 	sumaVpn		= new BigDecimal(0);
			BigDecimal	sumaVpnDxv	= new BigDecimal(0);



			if(utPPC.equals("0")){		//al vencimiento
				numPagosCap = 1;
				lDias = getDiasAPagar(sol);
				//perGracia	= lDias.size()-1;
			}else{
				numPagosCap = Integer.parseInt(sol.getIg_ppc());
				lDias = getPagosIntTradpi(sol.getDf_disposicion(0),sol.getCg_pfpc(),sol.getCg_pufpc(),sol.getDf_vencimiento(0),sol.getCg_ut_ppi(),sol.getIn_dia_pago());
				//perGracia	= Integer.parseInt(sol.getIg_periodos_gracia());
			}

			//int liposicionSaldo = lDias.size()-1;
			int liposicionSaldo = 0;
			int liposic = 1;

			numPagos	= numPagosCap;
			numDisp		= Integer.parseInt(sol.getIn_num_disp());
			Ki = new BigDecimal(monto / (numPagos-(numDisp-1)));
			int dpvAnt = 0;
			if(numDisp>1){
				saldoi = new BigDecimal(sol.getFn_monto_disp(0));
			}

			int incremento = 1;
			int incrementoint = 1;
			if(!utPPC.equals("0")){


				if("60".equals(sol.getCg_ut_ppc()))
				 	incremento = 2;
				else if("90".equals(sol.getCg_ut_ppc()))
				 	incremento = 3;
				else if("180".equals(sol.getCg_ut_ppc()))
					incremento = 6;
				else if("360".equals(sol.getCg_ut_ppc()))
					incremento = 12;


				if("60".equals(sol.getCg_ut_ppi()))
				 	incrementoint = 2;
				else if("90".equals(sol.getCg_ut_ppi()))
				 	incrementoint = 3;
				else if("180".equals(sol.getCg_ut_ppi()))
					incrementoint = 6;
				else if("360".equals(sol.getCg_ut_ppi()))
					incrementoint = 12;

				int puf = restaFechas(sol.getDf_disposicion(0),sol.getCg_pufpc());
				//log.debug("puf[[[[[ "+puf);
				for(int a=0;a<lDias.size()-1;a++){
					if((Integer.parseInt(lDias.get(a).toString()) == puf)){
						liposicionSaldo = a;
					}
				}

			}else{
				//log.debug("[[[[[[[[[[ENTROOOOOOOOOOOOOOO]]]]]]]");
				liposicionSaldo = lDias.size()-1;
			}

			List lPosic = new ArrayList();
			int aux = 0;
			while(aux<numPagos){
				if(aux==0){
					lPosic.add(new Integer(lDias.size()-1));
				}else{
					lPosic.add(new Integer(liposicionSaldo));
					liposicionSaldo = liposicionSaldo-(incremento/incrementoint);
				}
				aux++;
			}
			//log.debug("duracionTradicional:::lPosic::: "+lPosic);
			cur.setMetodoCalculo(sol.getMetodoCalculo());
			cur.setMetodo(sol.getId_interpolacion());
			cur.setEsquema(sol.getIc_esquema_recup());
			for(int i=0;i<lDias.size();i++){
				liposic = 1;
				for(int a=0;a<lPosic.size();a++){
					if(Integer.parseInt(lPosic.get(a).toString()) == i){
						liposic = 0;
						break;
					}
				}

				int		dpv = ((Integer)lDias.get(i)).intValue();
				int		diasPeriodo = dpv -dpvAnt;
				double	tasa = sol.getCostoAllIn();
				inti = new BigDecimal(saldoi.doubleValue()*((tasa/100)*(diasPeriodo)/360));
				flujoi = new BigDecimal(inti.doubleValue());
				if(numDisp>1&&(i+1)<numDisp){
						saldoi = saldoi.add(new BigDecimal(sol.getFn_monto_disp(i+1)));
				}else if(liposic == 0){
					flujoi = flujoi.add(Ki);
					saldoi = saldoi.subtract(Ki);
					//log.debug("duracionTradicional:::dia de lDias::: "+lDias.get(i));
				}
				tasaPeriodo = cur.interpolacion(dpv,sol.getIg_plazo());
				vpn = flujoi.divide(new BigDecimal(1+(tasaPeriodo/360)*dpv),BigDecimal.ROUND_HALF_UP);
				vpnDxv = vpn.multiply(new BigDecimal(dpv));
				sumaVpn = sumaVpn.add(vpn);

				sumaVpnDxv = sumaVpnDxv.add(vpnDxv);
				dpvAnt = dpv;
			}
			duracion = sumaVpnDxv.divide(sumaVpn,4,BigDecimal.ROUND_HALF_UP);
			sol.setDuracion(duracion.doubleValue());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("duracionTradicional (S)");
		}
	}


	public List getDiasPorVencerTrad(Solicitud sol){
		log.info("getDiasPorVencerTrad (E)");
		List diasaux = null;
		List dias = null;
		try{
			 java.util.Date dFechaIni	= Comunes.parseDate(sol.getDf_disposicion(0));
			 java.util.Date dFechaFin	= Comunes.parseDate(sol.getDf_vencimiento(0));
			 Calendar 	cFechaIni		= Calendar.getInstance();
			 Calendar	cFechaFin		= Calendar.getInstance();
			 Calendar	cMes			= Calendar.getInstance();
			 Calendar	cMesAnt			= Calendar.getInstance();
			 int  diaPago	= Integer.parseInt(sol.getIn_dia_pago());
			 int  diasDif	= 0;
			 dias = new ArrayList();
			 cFechaIni.setTime(dFechaIni);
			 cFechaFin.setTime(dFechaFin);
			 cMesAnt.setTime(dFechaIni);
			 cMes.setTime(dFechaIni);
			 boolean bnd = true;

			 long lMesAnt = 0;
			long lMes = 0;
			long lFecFin		= cFechaFin.getTime().getTime();
			long lFechaDispIni	= Comunes.parseDate(sol.getDf_disposicion(0)).getTime();
			int numDisp = Integer.parseInt(sol.getIn_num_disp());

			int incremento = 1;
			 dias = new ArrayList();
			 if("60".equals(sol.getCg_ut_ppc()))
			 	incremento = 2;
			 if("90".equals(sol.getCg_ut_ppc()))
			 	incremento = 3;
			else if("180".equals(sol.getCg_ut_ppc()))
				incremento = 6;
			else if("360".equals(sol.getCg_ut_ppc()))
				incremento = 12;

			int incrementoint = 1;
			if("60".equals(sol.getCg_ut_ppi()))
			 	incrementoint = 2;
			else if("90".equals(sol.getCg_ut_ppi()))
			 	incrementoint = 3;
			else if("180".equals(sol.getCg_ut_ppi()))
				incrementoint = 6;
			else if("360".equals(sol.getCg_ut_ppi()))
				incrementoint = 12;

			int esq = Integer.parseInt(sol.getIc_esquema_recup());

			if(esq==4){

			 int i = 1;
			 while(lFecFin>=lMes){
			 	bnd = true;
			 	int		diaPagoTmp = diaPago;
				java.util.Date tmpDate = new java.util.Date();
				if(diaPagoTmp > cMes.getActualMaximum(Calendar.DAY_OF_MONTH))	//si el dia de pago es 31 y el mes tiene 30 dias
					diaPagoTmp = cMes.getActualMaximum(Calendar.DAY_OF_MONTH);

				cMes.set(Calendar.DAY_OF_MONTH,diaPagoTmp);

				/*
				 *ya estaba comentariado
				 *
				 *if(cFechaIni.get(Calendar.MONTH)!=cMes.get(Calendar.MONTH)&&
					cFechaIni.get(Calendar.YEAR)!=cMes.get(Calendar.YEAR)){
				*/
					lMesAnt	= cMesAnt.getTime().getTime();
					if(i==1){
						lMes	= cMes.getTime().getTime();
					}
					if(lMesAnt!=lMes){
						if(cFechaFin.get(Calendar.MONTH)==cMes.get(Calendar.MONTH)&&cFechaFin.get(Calendar.YEAR)==cMes.get(Calendar.YEAR)){
								diasDif += (int)Math.round((lFecFin - lMesAnt)/86400000.0);
								dias.add(new Integer(diasDif));
								break;
						}
						diasDif += (int)Math.round((lMes - lMesAnt)/86400000.0);
						if(numDisp>1&&i<numDisp){
							long			lfechaDisp	= Comunes.parseDate(sol.getDf_disposicion(i-1)).getTime();
							Double.parseDouble(sol.getFn_monto_disp(i-1));
							diasDif		= (int)Math.round((lfechaDisp - lFechaDispIni)/86400000.0);
						}
						int perpagcap = Integer.parseInt(sol.getCg_ut_ppc());

						if(perpagcap != 30 && i<2){
							bnd = false;
						}
						if(diasDif>0 && bnd){
							dias.add(new Integer(diasDif));
						}
					}
					tmpDate.setTime(lMes);
					cMesAnt.setTime(tmpDate);
				//}
				cMes.add(Calendar.MONTH,incremento);
				lMes	= cMes.getTime().getTime();
				i++;
			 } // while
			 int numPagosCap = Integer.parseInt(sol.getIg_ppc());
			 if(dias.size()<numPagosCap){
			 	diasDif = (int)Math.round((cFechaFin.getTime().getTime()-cFechaIni.getTime().getTime())/86400000.0);
			 	dias.add(new Integer(diasDif));
			 }

			 }else{
			 	int ppc = Integer.parseInt(sol.getCg_ut_ppc());
			 	List lDias = new ArrayList();
			 	if(ppc != 0){
			 		lDias = getPagosIntTradpi(sol.getDf_disposicion(0),sol.getCg_pfpc(),sol.getCg_pufpc(),sol.getDf_vencimiento(0),sol.getCg_ut_ppi(),sol.getIn_dia_pago());
			 	}else{
			 		lDias = getDiasAPagar(sol);
			 	}

				int totaldias = lDias.size()-1;
			 	if(ppc != 0){
					int numPagosCap = Integer.parseInt(sol.getIg_ppc());
					//log.debug("numPagosCap::: "+numPagosCap);
					int incrementaciclo = 0;
					int decrementa = numPagosCap-1;
					//log.debug("decrementa::: "+decrementa);

					int posicion = 0;
					diasaux = new ArrayList();

					int puf = restaFechas(sol.getDf_disposicion(0),sol.getCg_pufpc());
					//log.debug("puf[[[[[ "+puf);
					for(int a=0;a<lDias.size()-1;a++){
						if((Integer.parseInt(lDias.get(a).toString()) == puf)){
							posicion = a;
						}
					}

					while(incrementaciclo<numPagosCap){
						if(incrementaciclo==0){
							diasaux.add(lDias.get(totaldias).toString());
						}else{
							//totaldias = posicion;
							diasaux.add(lDias.get(posicion).toString());
							posicion = posicion-(incremento/incrementoint);
						}
						decrementa--;
						incrementaciclo++;
					}
					//log.debug("diasaux::: "+diasaux);
					for(int aux=diasaux.size()-1;aux>=0;aux--){
						int valor = Integer.parseInt(diasaux.get(aux).toString());
						dias.add(new Integer(valor));
					}
			 	}else{
			 		dias.add(new Integer(lDias.get(totaldias).toString()));
			 	}
			 }

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getDiasPorVencerTrad (S)");
		}
		//log.debug("getDiasPorVencerTrad:::dias::: "+dias);
		return dias;
	}

	public List getDiasFechas(Solicitud sol){
		log.info("getDiasPorVencerTrad (E)");
		List dias = null;
		try{
			 java.util.Date dFechaIni	= Comunes.parseDate(sol.getDf_disposicion(0));
			 java.util.Date dFechaFin	= Comunes.parseDate(sol.getDf_vencimiento(0));
			 Calendar 	cFechaIni		= Calendar.getInstance();
			 Calendar	cFechaFin		= Calendar.getInstance();
			 Calendar	cMes			= Calendar.getInstance();
			 Calendar	cMesAnt			= Calendar.getInstance();
			 int  diaPago	= Integer.parseInt(sol.getIn_dia_pago());
			 int  diasDif	= 0;
			 dias = new ArrayList();
			 cFechaIni.setTime(dFechaIni);
			 cFechaFin.setTime(dFechaFin);
			 cMesAnt.setTime(dFechaIni);
			 cMes.setTime(dFechaIni);

			 int incremento = 1;
			 if("60".equals(sol.getCg_ut_ppc()))
			 	incremento = 2;
			 if("90".equals(sol.getCg_ut_ppc()))
			 	incremento = 3;
			else if("180".equals(sol.getCg_ut_ppc()))
				incremento = 6;
			else if("360".equals(sol.getCg_ut_ppc()))
				incremento = 12;
			long lMesAnt = 0;
			long lMes = 0;
			long lFecFin		= cFechaFin.getTime().getTime();
			long lFechaDispIni	= Comunes.parseDate(sol.getDf_disposicion(0)).getTime();
			int numDisp = Integer.parseInt(sol.getIn_num_disp());

			 int i = 1;
			 while(lFecFin>=lMes){
			 	int		diaPagoTmp = diaPago;
				java.util.Date tmpDate = new java.util.Date();
				if(diaPagoTmp > cMes.getActualMaximum(Calendar.DAY_OF_MONTH))	//si el dia de pago es 31 y el mes tiene 30 dias
					diaPagoTmp = cMes.getActualMaximum(Calendar.DAY_OF_MONTH);

				cMes.set(Calendar.DAY_OF_MONTH,diaPagoTmp);
				/*if(cFechaIni.get(Calendar.MONTH)!=cMes.get(Calendar.MONTH)&&
					cFechaIni.get(Calendar.YEAR)!=cMes.get(Calendar.YEAR)){*/
					lMesAnt	= cMesAnt.getTime().getTime();
					lMes	= cMes.getTime().getTime();
					if(lMesAnt!=lMes){
						if(cFechaFin.get(Calendar.MONTH)==cMes.get(Calendar.MONTH)&&
							cFechaFin.get(Calendar.YEAR)==cMes.get(Calendar.YEAR)){
								diasDif += (int)Math.round((lFecFin - lMesAnt)/86400000.0);
								dias.add(new Integer(diasDif));
								break;
						}
						diasDif += (int)Math.round((lMes - lMesAnt)/86400000.0);
						if(numDisp>1&&i<numDisp){
							long			lfechaDisp	= Comunes.parseDate(sol.getDf_disposicion(i-1)).getTime();
							Double.parseDouble(sol.getFn_monto_disp(i-1));
							diasDif		= (int)Math.round((lfechaDisp - lFechaDispIni)/86400000.0);
						}
						if(diasDif>0){
							dias.add(new Integer(diasDif));
						}
					}
					tmpDate.setTime(lMes);
					cMesAnt.setTime(tmpDate);
				//}
				cMes.add(Calendar.MONTH,incremento);
				i++;
			 } // while
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getDiasPorVencerTrad (S)");
		}
		return dias;
	}



	private void ppvPlanPagos(Solicitud sol,AccesoDB con){
		log.info("ppvPlanPagos (E)");
		try{
			Hashtable	htpp	= getDiasPorVencerPlanPagos(sol,con);
			List		lDias	= (List)htpp.get("dias");
			List		lMontos	= (List)htpp.get("montos");
			double monto	= sol.getFn_monto();
			//String 	utPPC	= sol.getCg_ut_ppc();
			BigDecimal	sumaKi	= new BigDecimal(0);
			BigDecimal	ppv		= new BigDecimal(0);


			for(int i=0;i<lDias.size();i++){
				int		dpv = ((Integer)lDias.get(i)).intValue();
				double	ki	= ((Double)lMontos.get(i)).doubleValue();
				sumaKi = sumaKi.add(new BigDecimal(ki).multiply(new BigDecimal(dpv)));

			}
			ppv = sumaKi.divide(new BigDecimal(monto),4,BigDecimal.ROUND_HALF_UP);
			sol.setPPV(ppv.doubleValue());
			//sol.setPPV(sumaKi.doubleValue());

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("ppvPlanPagos (S)");
		}
	}


	private void duracionPlanPagos(Solicitud sol,Curva cur,AccesoDB con){
		log.info("duracionPlanPagos (E)");
		try{
			Hashtable	htpp	= getDiasPorVencerPlanPagosDur(sol,con);
			List		lDias	= (List)htpp.get("dias");
			List		lMontos	= (List)htpp.get("montos");
			double monto	= sol.getFn_monto();
			//String 	utPPC	= sol.getCg_ut_ppc();
			double tasaPeriodo = 0;
			BigDecimal	duracion	= new BigDecimal(0);

			BigDecimal	saldoi		= new BigDecimal(monto);
			BigDecimal	inti		= new BigDecimal(0);
			BigDecimal	flujoi		= new BigDecimal(0);
			BigDecimal	vpn			= new BigDecimal(0);
			BigDecimal	vpnDxv		= new BigDecimal(0);
			BigDecimal 	sumaVpn		= new BigDecimal(0);
			BigDecimal	sumaVpnDxv	= new BigDecimal(0);
			int dpvAnt = 0;

			cur.setMetodoCalculo(sol.getMetodoCalculo());
			cur.setMetodo(sol.getId_interpolacion());
			cur.setEsquema(sol.getIc_esquema_recup());

			for(int i=0;i<lDias.size();i++){
				int			dpv			= ((Integer)lDias.get(i)).intValue();
				BigDecimal	Ki			= new BigDecimal(((Double)lMontos.get(i)).doubleValue());
				int			diasPeriodo	= dpv -dpvAnt;
				double		tasa 		= (sol.getCostoAllIn())/100;
				inti = new BigDecimal(saldoi.doubleValue()*(tasa*(diasPeriodo)/360));

				flujoi = inti.add(Ki);
				saldoi = saldoi.subtract(Ki);

				tasaPeriodo = cur.interpolacion(dpv,sol.getIg_plazo());

				vpn = flujoi.divide(new BigDecimal(1+(tasaPeriodo/360)*dpv),BigDecimal.ROUND_HALF_UP);
				vpnDxv = vpn.multiply(new BigDecimal(dpv));
				sumaVpn = sumaVpn.add(vpn);
				sumaVpnDxv = sumaVpnDxv.add(vpnDxv);
				dpvAnt = dpv;
			}

			duracion = sumaVpnDxv.divide(sumaVpn,4,BigDecimal.ROUND_HALF_UP);
			sol.setDuracion(duracion.doubleValue());

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("duracionPlanPagos (S)");
		}
	}

	private Hashtable getDiasPorVencerPlanPagos(Solicitud sol,AccesoDB con){
		log.info("getDiasPorVencerPlanPagos (E)");
		Hashtable htRetorno = new Hashtable();
		List dias = null;
		List montos = null;

		try{
			 PreparedStatement	ps 				= null;
			 ResultSet			rs				= null;
			 String				qrySentencia	= "";
			 java.util.Date dFechaIni	= Comunes.parseDate(sol.getDf_disposicion(0));
			 Calendar 	cFechaIni		= Calendar.getInstance();
			 int  diasDif	= 0;

			 dias	= new ArrayList();
			 montos	= new ArrayList();

			 cFechaIni.setTime(dFechaIni);

			 long lMesAnt = dFechaIni.getTime();
			 long lMes = 0;

			 qrySentencia =
			 	" select df_pago"+
				" ,fn_monto"+
				" from cottmp_pago"+
				" where ic_proc_pago = ?"+
				" order by ic_pago";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,sol.getIc_proc_pago());
			rs = ps.executeQuery();

			 while(rs.next()){
				lMes	= rs.getDate("df_pago").getTime();

				diasDif += (int)Math.round((lMes - lMesAnt)/86400000.0);
				dias.add(new Integer(diasDif));
				montos.add(new Double(rs.getDouble("fn_monto")));

				lMesAnt = lMes;
			 }
			 ps.close();
			htRetorno.put("dias",dias);
			htRetorno.put("montos",montos);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getDiasPorVencerPlanPagos (S)");
		}
		return htRetorno;
	}


	private Hashtable getDiasPorVencerPlanPagosDur(Solicitud sol,AccesoDB con){
		log.info("getDiasPorVencerPlanPagosDur (E)");
		Hashtable htRetorno = new Hashtable();
		List dias = null;
		List fechas = null;
		List montos = null;
		List montosaux = null;

		try{
			PreparedStatement	ps 				= null;
			ResultSet			rs				= null;
			String				qrySentencia	= "";
			java.util.Date dFechaIni	= Comunes.parseDate(sol.getDf_disposicion(0));
			Calendar 	cFechaIni		= Calendar.getInstance();
			int  i = 0;
			int  j = 0;
			int numPagosCap = Integer.parseInt(sol.getIg_ppc());

			Map mDiasFechas = getDiasFechasPagoIntPP(sol);

			dias	= (List)mDiasFechas.get("dias");
			fechas	= (List)mDiasFechas.get("fechas");

			montos	= new ArrayList();
			for(i=0;i<dias.size();i++){
				montos.add(new Double(0));
			}


			cFechaIni.setTime(dFechaIni);

			//long lMesAnt = dFechaIni.getTime();
			long lMes = 0;
			long lMesInt = 0;

			int incremento = 1;
			if("60".equals(sol.getCg_ut_ppi()))
			 	incremento = 2;
			else if("90".equals(sol.getCg_ut_ppi()))
			 	incremento = 3;
			else if("180".equals(sol.getCg_ut_ppi()))
				incremento = 6;
			else if("360".equals(sol.getCg_ut_ppi()))
				incremento = 12;

			qrySentencia =
			 	" select df_pago"+
				" ,fn_monto"+
				" from cottmp_pago"+
				" where ic_proc_pago = ?"+
				" order by ic_pago";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,sol.getIc_proc_pago());
			rs = ps.executeQuery();

			i = 0;

			String[] arrFechas = new String[numPagosCap];
			//String[] arrMontos = new String[numPagosCap];
			String fechaAux = "";
			String fechaArr = "";

			if(incremento == 1){
				while(rs.next()){
					lMes	= rs.getDate("df_pago").getTime();
					double 	monto  = rs.getDouble("fn_monto");
					double	montoInt = 0;
					while(lMesInt<lMes && i<fechas.size()){
						lMesInt = ((Long)fechas.get(i)).longValue();
						i++;
					}
					montoInt = ((Double)montos.get(i-1)).doubleValue();
					montoInt += monto;
					montos.set(i-1,new Double(montoInt));
				}
			}else{
				montosaux = new ArrayList();
				while(rs.next()){
					fechaAux = rs.getString("df_pago");
					fechaArr = fechaAux.substring(8,10)+"/"+fechaAux.substring(5,7)+"/"+fechaAux.substring(0,4);
					arrFechas[j] = fechaArr;
					double 	monto  = rs.getDouble("fn_monto");
					montosaux.add(new Double(monto));
					j++;
				}

				ps.close();

				List lDiaPagoCap = getDiasPagoPP(sol,arrFechas);
				for(i=0;i<lDiaPagoCap.size();i++){
					for(j=0;j<dias.size();j++){
						if(lDiaPagoCap.get(i).toString().compareTo(dias.get(j).toString()) == 0){
							montos.set(j,new Double(montosaux.get(i).toString()));
						}
					}
				}
			}

			htRetorno.put("dias",dias);
			htRetorno.put("montos",montos);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getDiasPorVencerPlanPagosDur (S)");
		}
		return htRetorno;
	}

	private Map getDiasFechasPagoIntPP(Solicitud sol){
		log.info("getDiasFechasPagoIntPP (E)");
		List dias = null;
		List fechas = null;
		Map  mRetorno = new HashMap();
		try{
			 java.util.Date dFechaIni	= Comunes.parseDate(sol.getDf_disposicion(0));
			 java.util.Date dFechaFin	= Comunes.parseDate(sol.getDf_vencimiento(0));
			 Calendar 	cFechaIni		= Calendar.getInstance();
			 Calendar	cFechaFin		= Calendar.getInstance();
			 Calendar	cMes			= Calendar.getInstance();
			 Calendar	cMesAnt			= Calendar.getInstance();
			 int  diaPago	= Integer.parseInt(sol.getIn_dia_pago());
			 int  diasDif	= 0;
			 dias = new ArrayList();
			 fechas = new ArrayList();
			 cFechaIni.setTime(dFechaIni);
			 cFechaFin.setTime(dFechaFin);
			 cMesAnt.setTime(dFechaIni);
			 cMes.setTime(dFechaIni);

			 int incremento = 1;
			 if("60".equals(sol.getCg_ut_ppi()))
			 	incremento = 2;
			 else if("90".equals(sol.getCg_ut_ppi()))
			 	incremento = 3;
			else if("180".equals(sol.getCg_ut_ppi()))
				incremento = 6;
			else if("360".equals(sol.getCg_ut_ppi()))
				incremento = 12;
			long lMesAnt = 0;
			long lMes = 0;
			long lFecFin		= cFechaFin.getTime().getTime();
			lMes	= cMes.getTime().getTime();
			int diasDifTotal = (int)Math.round((lFecFin - lMes)/86400000.0);
			lMes	= cMes.getTime().getTime();
			//log.debug("::::diasDifTotal[[[[[[[ "+diasDifTotal);
			long lFechaDispIni	= Comunes.parseDate(sol.getDf_disposicion(0)).getTime();
			int numDisp = Integer.parseInt(sol.getIn_num_disp());

			 int i = 1;
			 while(lFecFin>=lMes){
			 	int		diaPagoTmp = diaPago;
				java.util.Date tmpDate = new java.util.Date();
				if(diaPagoTmp > cMes.getActualMaximum(Calendar.DAY_OF_MONTH))	//si el dia de pago es 31 y el mes tiene 30 dias
					diaPagoTmp = cMes.getActualMaximum(Calendar.DAY_OF_MONTH);

				cMes.set(Calendar.DAY_OF_MONTH,diaPagoTmp);
					lMesAnt	= cMesAnt.getTime().getTime();
					lMes	= cMes.getTime().getTime();
					if(lMesAnt!=lMes){
						diasDif += (int)Math.round((lMes - lMesAnt)/86400000.0);
						if(numDisp>1&&i<numDisp){
							long			lfechaDisp	= Comunes.parseDate(sol.getDf_disposicion(i-1)).getTime();
							Double.parseDouble(sol.getFn_monto_disp(i-1));
							diasDif		= (int)Math.round((lfechaDisp - lFechaDispIni)/86400000.0);
						}
						if(diasDif>5 && diasDif <= diasDifTotal){
							dias.add(new Integer(diasDif));
							fechas.add(new Long(cMes.getTime().getTime()));
						}
					}
					tmpDate.setTime(lMes);
					cMesAnt.setTime(tmpDate);
				//}
				cMes.add(Calendar.MONTH,incremento);
				i++;
			 } // while
			 int posicion = dias.size() - 1;
			//log.debug("posicion ::::  "+posicion);
			if(Integer.parseInt(dias.get(posicion).toString()) != diasDifTotal){
				//log.debug("]]]] Se agregara la ultima fecha [[[");
				if((diasDifTotal-Integer.parseInt(dias.get(posicion).toString()))<5){
					dias.set(posicion,new Integer(diasDifTotal));
					fechas.set(posicion,new Long(cFechaFin.getTime().getTime()));
				}else{
					dias.add(new Integer(diasDifTotal));
					fechas.add(new Long(cFechaFin.getTime().getTime()));
				}

			}
		mRetorno.put("dias",dias);
		mRetorno.put("fechas",fechas);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getDiasFechasPagoIntPP (S)");
		}
		return mRetorno;
	}


	private void ppvTipoRenta(Solicitud sol){
		log.info("ppvTipoRenta (E)");
		try{
			List		lDias	= getDiasPorVencerTrad(sol);
			List 		lMontos	= getMontosTipoRenta(sol,lDias);
			double monto	= sol.getFn_monto();
			//String 	utPPC	= sol.getCg_ut_ppc();
			BigDecimal	sumaKi	= new BigDecimal(0);
			BigDecimal	ppv		= new BigDecimal(0);

			for(int i=0;i<lDias.size();i++){
				int		dpv = ((Integer)lDias.get(i)).intValue();
				double	ki	= ((Double)lMontos.get(i)).doubleValue();
				sumaKi = sumaKi.add(new BigDecimal(ki).multiply(new BigDecimal(dpv)));
			}
			ppv = sumaKi.divide(new BigDecimal(monto),4,BigDecimal.ROUND_HALF_UP);
			sol.setPPV(ppv.doubleValue());
			//sol.setPPV(sumaKi.doubleValue());

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("ppvTipoRenta (S)");
		}
	}

	private void duracionTipoRenta(Solicitud sol,Curva cur){
		log.info("duracionTipoRenta (E)");
		try{
			List		lDias	= getDiasPorVencerTrad(sol);
			double		renta	= calculaRenta(sol,lDias);
			//double monto	= sol.getFn_monto();
			//String 	utPPC	= sol.getCg_ut_ppc();
			double tasaPeriodo = 0;
			BigDecimal	duracion	= new BigDecimal(0);

			BigDecimal	flujoi		= new BigDecimal(renta);
			BigDecimal	vpn			= new BigDecimal(0);
			BigDecimal	vpnDxv		= new BigDecimal(0);
			BigDecimal 	sumaVpn		= new BigDecimal(0);
			BigDecimal	sumaVpnDxv	= new BigDecimal(0);
			int dpvAnt = 0;

			cur.setMetodoCalculo(sol.getMetodoCalculo());
			cur.setMetodo(sol.getId_interpolacion());
			cur.setEsquema(sol.getIc_esquema_recup());

			for(int i=0;i<lDias.size();i++){
				int			dpv			= ((Integer)lDias.get(i)).intValue();
				tasaPeriodo = cur.interpolacion(dpv,sol.getIg_plazo());

				vpn = flujoi.divide(new BigDecimal(1+(tasaPeriodo/360)*dpv),BigDecimal.ROUND_HALF_UP);
				vpnDxv = vpn.multiply(new BigDecimal(dpv));
				sumaVpn = sumaVpn.add(vpn);
				sumaVpnDxv = sumaVpnDxv.add(vpnDxv);
				dpvAnt = dpv;
			}
			duracion = sumaVpnDxv.divide(sumaVpn,4,BigDecimal.ROUND_HALF_UP);
			sol.setDuracion(duracion.doubleValue());

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("duracionTipoRenta (S)");
		}
	}


	private List getMontosTipoRenta(Solicitud sol,List lDias){
		log.info("getMontosTipoRenta (E)");
		List lMontos = new ArrayList();
		try{
			double		monto			= sol.getFn_monto();
			int 		numPagos		= lDias.size();
			double		interes			= sol.getTasaCreditoUSD()/100;
			//double		periodicidad	= ((double)lDias.size())/((double)sol.getIg_plazo_conv());
			double		periodicidad	= 12;
			double		renta			= 0;
			double		saldo			= monto;

			if("D".equals(sol.getMetodoCalculo()) || "U".equals(sol.getMetodoCalculo())){
				interes = sol.getTRE()/100;
				// si el metodo es duracion y es la primera iteracion se pone ese valor para evitar divisiones entre cero
				if(interes==0){
					interes = 0.001;
				}
			}

			if("30".equals(sol.getCg_ut_ppc())){
				periodicidad = 12;
			}else if("180".equals(sol.getCg_ut_ppc())){
				periodicidad = 2;
			}else if("90".equals(sol.getCg_ut_ppc())){
				periodicidad = 4;
			}else if("360".equals(sol.getCg_ut_ppc())){
				periodicidad = 1;
			}

			renta = monto/((1-Math.pow((1+(interes/periodicidad)),(numPagos*-1)))/(interes/periodicidad));

			for(int i=0;i<lDias.size();i++){
				double pagoInteres	= saldo*interes/periodicidad;
				double montoPago	= renta - pagoInteres;
				saldo -= montoPago;
				lMontos.add(new Double(montoPago));
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getMontosTipoRenta (S)");
		}
		return lMontos;
	}

	private double calculaRenta(Solicitud sol,List lDias){
		log.info("calculaRenta (E)");
		double		renta			= 0;
		try{
			double		monto			= sol.getFn_monto();
			int 		numPagos		= lDias.size();
			double		interes			= sol.getTRE()/100;
			//double		periodicidad	= ((double)lDias.size())/((double)sol.getIg_plazo_conv());
			double		periodicidad	= 12;

			if("30".equals(sol.getCg_ut_ppc())){
				periodicidad = 12;
			}else if("180".equals(sol.getCg_ut_ppc())){
				periodicidad = 2;
			}else if("90".equals(sol.getCg_ut_ppc())){
				periodicidad = 4;
			}else if("360".equals(sol.getCg_ut_ppc())){
				periodicidad = 1;
			}

			renta = monto/((1-Math.pow((1+(interes/periodicidad)),(numPagos*-1)))/(interes/periodicidad));

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("calculaRenta (S)");
		}
		return renta;
	}



	public int getPlazoConv(Solicitud sol){
		log.info("getPlazoConv (E)");
		int contador = 0;
		try{
			 java.util.Date dFechaIni	= Comunes.parseDate(sol.getDf_disposicion(0));
			 java.util.Date dFechaFin	= Comunes.parseDate(sol.getDf_vencimiento(0));
			 Calendar 	cFechaIni		= Calendar.getInstance();
			 Calendar	cFechaFin		= Calendar.getInstance();
			 Calendar	cMes			= Calendar.getInstance();
			 Calendar	cMesAnt			= Calendar.getInstance();
			 int  diaPago	= Integer.parseInt(sol.getIn_dia_pago());
			 int  diasDif	= 0;
			 cFechaIni.setTime(dFechaIni);
			 cFechaFin.setTime(dFechaFin);
			 cMesAnt.setTime(dFechaIni);
			 cMes.setTime(dFechaIni);
			 int utPlazo = Integer.parseInt(sol.getCg_ut_plazo());
			 int incremento 	= 1;
			 int margen 		= 3;
			 int margenMenor	= 2;
			 boolean cuponIncompleto = false;
			 if(utPlazo==90){
			 	margen = 3;
				margenMenor = 5;
			 	incremento = 3;
			}else if(utPlazo==180){
				incremento = 6;
				margenMenor = 10;
				margen = 4;
			}else if(utPlazo==360){
				margenMenor = 15;
				incremento = 12;
				margen = 5;
			}
			 long lMesAnt = 0;
			 long lMes = 0;
			 long lFecFin = cFechaFin.getTime().getTime();
			 while(lFecFin>=lMes){

			 	int		diaPagoTmp = diaPago;
				java.util.Date tmpDate = new java.util.Date();
				if(diaPagoTmp > cMes.getActualMaximum(Calendar.DAY_OF_MONTH))
					diaPagoTmp = cMes.getActualMaximum(Calendar.DAY_OF_MONTH);
				cMes.set(Calendar.DAY_OF_MONTH,diaPagoTmp);
				/*if(cFechaIni.get(Calendar.MONTH)!=cMes.get(Calendar.MONTH)&&
					cFechaIni.get(Calendar.YEAR)!=cMes.get(Calendar.YEAR)){	*/
					lMesAnt	= cMesAnt.getTime().getTime();
					lMes	= cMes.getTime().getTime();
					if(lMesAnt!=lMes){
						if(cFechaFin.get(Calendar.MONTH)==cMes.get(Calendar.MONTH)&&
							cFechaFin.get(Calendar.YEAR)==cMes.get(Calendar.YEAR)){
								diasDif = (int)Math.round((lFecFin - lMesAnt)/86400000.0);
								if(diasDif>margenMenor){
									if(diasDif<(utPlazo-margen)&&!cuponIncompleto){
										cuponIncompleto = true;
										contador ++;
									}else if(diasDif>=utPlazo-margen){
										contador ++;
									}
									/*else if(cMes.get(Calendar.MONTH)==2&&utPlazo==30&&diasDif>=28){
										contador++;
									}*/
								}

								break;
						}
						diasDif = (int)Math.round((lMes - lMesAnt)/86400000.0);
						if(diasDif>margenMenor){
							if(diasDif<(utPlazo-margen)&&!cuponIncompleto){
								cuponIncompleto = true;
								contador ++;
							}else if(diasDif>=utPlazo-margen){
								contador ++;
							}
							/*else if(cMes.get(Calendar.MONTH)==2&&utPlazo==30&&diasDif>=28){
								contador++;
							}*/
						}
					}
					tmpDate.setTime(lMes);
					cMesAnt.setTime(tmpDate);
				//}

				if(incremento==12){
					cMes.add(Calendar.MONTH,6);
					cMes.add(Calendar.MONTH,6);
				}else{
					cMes.add(Calendar.MONTH,incremento);
				}

			 }	//while
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getPlazoConv (S)");
		}
		return contador;
	}

	public List getEsquemasRecup(Solicitud sol){
		 return getEsquemasRecup(sol,"F");
	}

	public List getEsquemasRecup(Solicitud sol,String origen){
		log.info("getEsquemasRecup (E)");
		List	lFilas = new ArrayList();
		Map		mColumnas = null;
		AccesoDB con = new AccesoDB();
		try{
			PreparedStatement	ps 				= null;
			ResultSet			rs				= null;
			String				qrySentencia	= "";
			String campo	= (sol.getIc_moneda().equals("1")?"cs_disponible_if_mn":"cs_disponible_if_dl");
			con.conexionDB();
			qrySentencia =
				" select ic_esquema_recup"+
				" ,cg_descripcion"+
				" from cotcat_esquema_recup"+
				" where ic_esquema_recup in("+sol.getEsquemasRecup(origen)+")"+
				" and "+campo+" = 'S'";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();

			 while(rs.next()){
				mColumnas = new HashMap();
				mColumnas.put("ic_esquema_recup",rs.getString("ic_esquema_recup"));
				mColumnas.put("descripcion",rs.getString("cg_descripcion"));
				if(rs.getString("ic_esquema_recup").equals(sol.getIc_esquema_recup())){
					mColumnas.put("sel","selected");
				}else{
					mColumnas.put("sel","");
				}
			 	lFilas.add(mColumnas);
			 }
			 ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getEsquemasRecup (S)");
		}
		return lFilas;
	}

	private double calculaAcarreoPorDisposicion(Solicitud sol,Curva depositos,AccesoDB con)
		throws NafinException{
		log.info("calculaAcarreoPorDisposicion(E)");
		BigDecimal		sumaAcarreosDisp	= new BigDecimal(0);
		long			lfechaCotiza		= Comunes.parseDate(sol.getDf_cotizacion()).getTime();

		depositos.setMetodoCalculo(sol.getMetodoCalculo());
		depositos.setMetodo(sol.getId_interpolacion());
		depositos.setEsquema(sol.getIc_esquema_recup());

		try{
			for(int i=0;i<Integer.parseInt(sol.getIn_num_disp());i++){
				long			lfechaDisp	= Comunes.parseDate(sol.getDf_disposicion(i)).getTime();
				double			montoDisp	= Double.parseDouble(sol.getFn_monto_disp(i));
				long			diasDif		= (lfechaDisp - lfechaCotiza)/86400000;
				double			acarreoDisp	= 0;
				double			difTasas	= 0;

				difTasas			= (sol.getTRE()/100) - depositos.interpolacion((double)diasDif,sol.getIg_plazo());
				acarreoDisp			= (montoDisp*diasDif*difTasas)/360;
				sumaAcarreosDisp	= sumaAcarreosDisp.add(new BigDecimal(acarreoDisp));
			}
		}catch(NafinException ne){
			ne.printStackTrace();
			throw ne;
		}catch(Exception e){
			log.info("calculaAcarreoPorDisposicion(Exception)");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			log.info("calculaAcarreoPorDisposicion(S)");
		}
		log.debug("calculaAcarreoPorDisposicion:::sumaAcarreosDisp::: "+sumaAcarreosDisp);
		return sumaAcarreosDisp.doubleValue();
	}

	/*getDiasAPagar
	 *Obtiene los dias de pago de intereses
	 *@return dias Regresa una lista con todos los dias en los cuales se pagara el interes
	 */

public List getDiasAPagar(Solicitud sol){
	log.info("getDiasAPagar (E)");
	List dias = null;
	try{
		java.util.Date dFechaIni	= Comunes.parseDate(sol.getDf_disposicion(0));
		java.util.Date dFechaFin	= Comunes.parseDate(sol.getDf_vencimiento(0));
		Calendar 	cFechaIni		= Calendar.getInstance();
		Calendar	cFechaFin		= Calendar.getInstance();
		Calendar	cMes			= Calendar.getInstance();
		Calendar	cMesAnt			= Calendar.getInstance();
		int  diaPago	= Integer.parseInt(sol.getIn_dia_pago());
		/*===== OBTENER EL DIA DE PAGO VALIDANDO SI ES EL ULTIMO DIA DEL MES =====*/
		boolean esDiaPagoUltimoDelMes = false;
		java.util.Date dateFechaVencimiento = Comunes.parseDate(sol.getDf_vencimiento(0));
		Calendar calVencimiento = Calendar.getInstance();
		calVencimiento.setTime(dateFechaVencimiento);
		if (diaPago == calVencimiento.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			esDiaPagoUltimoDelMes = true;
			log.debug("dias de pago[[[[[[[[[ ULTIMO DIA DEL MES");
		} else {
			log.debug("dias de pago[[[[[[[[[ "+diaPago);
		}
		/*========================================================================*/
		//log.debug("dias de pago[[[[[[[[[ "+diaPago);
		int  diasDif	= 0;
		dias = new ArrayList();
		cFechaIni.setTime(dFechaIni);
		cFechaFin.setTime(dFechaFin);
		cMesAnt.setTime(dFechaIni);
		cMes.setTime(dFechaIni);

		int incremento = 1;
		if("60".equals(sol.getCg_ut_ppi()))
			incremento = 2;
		else if("90".equals(sol.getCg_ut_ppi()))
		 	incremento = 3;
		else if("180".equals(sol.getCg_ut_ppi()))
			incremento = 6;
		else if("360".equals(sol.getCg_ut_ppi()))
			incremento = 12;
		long lMesAnt = 0;
		long lMes = 0;
		long lFecFin		= cFechaFin.getTime().getTime();
		lMes	= cMes.getTime().getTime();
		int diasDifTotal = (int)Math.round((lFecFin - lMes)/86400000.0);
		lMes	= cMes.getTime().getTime();
		//log.debug("::::diasDifTotal[[[[[[[ "+diasDifTotal);
		long lFechaDispIni	= Comunes.parseDate(sol.getDf_disposicion(0)).getTime();
		int numDisp = Integer.parseInt(sol.getIn_num_disp());

		int i = 1;
		while(lFecFin>=lMes){
			int		diaPagoTmp = diaPago;
			java.util.Date tmpDate = new java.util.Date();
			if(diaPagoTmp > cMes.getActualMaximum(Calendar.DAY_OF_MONTH))	//si el dia de pago es 31 y el mes tiene 30 dias
				diaPagoTmp = cMes.getActualMaximum(Calendar.DAY_OF_MONTH);
			/*==================== AJUSTE DIA PAGO - ACF ====================*/
			if (esDiaPagoUltimoDelMes) {
				diaPagoTmp = cMes.getActualMaximum(Calendar.DAY_OF_MONTH);
			}
			/*======================================================================*/
			cMes.set(Calendar.DAY_OF_MONTH,diaPagoTmp);
			lMesAnt	= cMesAnt.getTime().getTime();
			lMes	= cMes.getTime().getTime();
			if(lMesAnt!=lMes){
				diasDif += (int)Math.round((lMes - lMesAnt)/86400000.0);

				if(numDisp>1&&i<numDisp){
					long			lfechaDisp	= Comunes.parseDate(sol.getDf_disposicion(i-1)).getTime();
					Double.parseDouble(sol.getFn_monto_disp(i-1));
					diasDif		= (int)Math.round((lfechaDisp - lFechaDispIni)/86400000.0);
				}
				if(diasDif>5 && diasDif <= diasDifTotal){
					dias.add(new Integer(diasDif));
				}
			}
			tmpDate.setTime(lMes);
			cMesAnt.setTime(tmpDate);
			cMes.add(Calendar.MONTH,incremento);
			i++;
		} // while
		int posicion = dias.size() - 1;
	    
		log.debug ("posicion  "+posicion);
	        log.debug ("diasDifTotal  "+diasDifTotal);
	    
		
		if(Integer.parseInt(dias.get(posicion).toString()) != diasDifTotal){
		//	log.debug("]]]] Se agregara la ultima fecha [[[");
			if((diasDifTotal-Integer.parseInt(dias.get(posicion).toString()))<5){
				dias.set(posicion,new Integer(diasDifTotal));
			}else{
				dias.add(new Integer(diasDifTotal));
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		log.info("getDiasAPagar (S)");
	}
	log.debug("getDiasAPagar:::dias:::: "+dias);
	return dias;
}

	/*getDiasPagoPP
	 *Obtiene los dias de pago de capital en el esquema PP
	 *@return dias Regresa una lista con todos los dias en los cuales se pagara el capital
	 */

	public List getDiasPagoPP(Solicitud sol,String[] arrFechas){
		log.info("getDiasPagoPP (E)");
		List dias = null;
		try{
			java.util.Date dFechaIni	= Comunes.parseDate(sol.getDf_disposicion(0));
			Calendar 	cFechaIni		= Calendar.getInstance();
			int  diasDif	= 0;
			int i = 0;
			long lFechaAnterior = 0;
			long lFecha = 0;
			dias = new ArrayList();

			cFechaIni.setTime(dFechaIni);
			lFechaAnterior = cFechaIni.getTime().getTime();

			for(i=0;i<arrFechas.length;i++){
				dFechaIni	= Comunes.parseDate(arrFechas[i]);
				cFechaIni.setTime(dFechaIni);
				lFecha		= cFechaIni.getTime().getTime();
				diasDif += (int)Math.round((lFecha - lFechaAnterior)/86400000.0);
				dias.add(new Integer(diasDif));
				lFechaAnterior = lFecha;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getDiasPagoPP (S)");
		}
		log.debug("dias:::getDiasPagoPP::: "+dias);
		return dias;
	}

	/*****************************************************************
	 *getFechasPagos Obtiene las fechas en que se haran los pagos
	 *Capital o Intereses
	 *@author Salvador Saldivar Barajas
	 *@param fini. Fecha de inicio
	 *@param fvenc. Fecha de vencimiento
	 *@param diadpago. Dia en el que se realizara el pago
	 *@param periodo. Cada cuando se realizara el pago
	 *@return Map. regresa un mapa con los dias y fechas de pago
	 *****************************************************************/
	public Map getFechasPagos(String fini, String fvenc, String diadpago, String periodo) throws NafinException{
		log.info("getFechasPagos (E)");
		DateFormat myDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		List ldias = null;
		List lfechas = null;
		Map htretorno = new HashMap();
		try{
			java.util.Date dFechaIni	= myDateFormat.parse(fini);
			java.util.Date dFechaFin	= myDateFormat.parse(fvenc);
			Calendar 	cFechaIni		= Calendar.getInstance();
			Calendar	cFechaFin		= Calendar.getInstance();
			Calendar	cMes			= Calendar.getInstance();
			Calendar	cMesAnt			= Calendar.getInstance();
			int  diaPago	= Integer.parseInt(diadpago);
			//log.debug("dias de pago[[[[[[[[[ "+diaPago);
			int  diasDif	= 0;
			ldias = new ArrayList();
			lfechas = new ArrayList();
			cFechaIni.setTime(dFechaIni);
			cFechaFin.setTime(dFechaFin);
			cMesAnt.setTime(dFechaIni);
			cMes.setTime(dFechaIni);

			int incremento = 1;
			if("60".equals(periodo))
				incremento = 2;
			else if("90".equals(periodo))
			 	incremento = 3;
			 else if("120".equals(periodo))
			 	incremento = 4;
			else if("180".equals(periodo))
				incremento = 6;
			else if("360".equals(periodo))
				incremento = 12;

			long lMesAnt = 0;
			long lMes = 0;
			long lFecFin		= cFechaFin.getTime().getTime();
			lMes	= cMes.getTime().getTime();
			int diasDifTotal = (int)Math.round((lFecFin - lMes)/86400000.0);
			//log.debug("::::diasDifTotal[[[[[[[ "+diasDifTotal);
			while(lFecFin>=lMes){
				int		diaPagoTmp = diaPago;
				java.util.Date tmpDate = new java.util.Date();
				if(diaPagoTmp > cMes.getActualMaximum(Calendar.DAY_OF_MONTH))	//si el dia de pago es 31 y el mes tiene 30 dias
					diaPagoTmp = cMes.getActualMaximum(Calendar.DAY_OF_MONTH);

				cMes.set(Calendar.DAY_OF_MONTH,diaPagoTmp);
				lMesAnt	= cMesAnt.getTime().getTime();
				lMes	= cMes.getTime().getTime();
				if(lMesAnt!=lMes){
					diasDif += (int)Math.round((lMes - lMesAnt)/86400000.0);
					if(diasDif>0 && diasDif <= diasDifTotal){
						ldias.add(new Integer(diasDif));
						lfechas.add(myDateFormat.format(cMes.getTime()));
					}
				}
				tmpDate.setTime(lMes);
				cMesAnt.setTime(tmpDate);
				cMes.add(Calendar.MONTH,incremento);
			} // while
			int posicion = ldias.size() - 1;
			//log.debug("posicion ::::  "+posicion);
			if(Integer.parseInt(ldias.get(posicion).toString()) != diasDifTotal){
				//log.debug("]]]] Se agregara la ultima fecha [[[");
				ldias.add(new Integer(diasDifTotal));
				lfechas.add(myDateFormat.format(cFechaFin.getTime()));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getFechasPagos (S)");
		}
	/*	log.debug("No. de pagos::: "+lfechas.size());
		log.debug("Dias de pago::: "+ldias);
		log.debug("Fechas de pago::: "+lfechas);*/
		htretorno.put("dias",ldias);
		htretorno.put("fechas",lfechas);
		return htretorno;
	}

	/***************************************************************************
	 *CotizacionLinea. valida si esta habilitado el parametro para cotizar en linea
	 *@author Salvador Saldivar Barajas
	 *@param icejec. ic del ejecutivo
	 *@param iNoCliente. ic del intermediario financiero
	 *@return boolean. Regresa true si esta habilitado y false si no lo esta.
	 **********************************************************************************/
	public boolean CotizacionLinea(String icejec, String iNoCliente) throws NafinException{
		log.info("CotizacionLinea (E)");
		boolean r = true;
		String cg_cot_linea = "";
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		try{
			con.conexionDB();
			String qry = "SELECT NVL(cg_cot_linea,'N') as cg_cot_linea FROM cotrel_if_ejecutivo WHERE ic_ejecutivo = " + icejec + " AND ic_if = "+ iNoCliente;
			rs = con.queryDB(qry);
			if(rs.next()){
				cg_cot_linea = rs.getString("cg_cot_linea");
			}
			rs.close();
			if(!cg_cot_linea.equals("S")){
				r = false;
			}
		}catch(Exception e){
				e.printStackTrace();
		}finally{
			log.info("CotizacionLinea (S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return r;
	}

	/***************************************************************************
	 *ExisteSWAP. valida si existe cargada una curva swap del dia actual
	 *@author Salvador Saldivar Barajas
	 *@return boolean. Regresa true si existe y false si no.
	 **********************************************************************************/
	public boolean ExisteSWAP() throws NafinException{
		log.info("ExisteSWAP (E)");
		boolean r = true;
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		String qry = "";
		String idcurva = "";
		try{
			con.conexionDB();
			String df_fecha_hoy  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			df_fecha_hoy = df_fecha_hoy.substring(6,10)+df_fecha_hoy.substring(3,5)+df_fecha_hoy.substring(0,2);

			qry = "select idcurva from curva where cs_swap = 'S'";
			rs = con.queryDB(qry);
			if(rs.next()){
				idcurva = rs.getString("idcurva");
				qry = "select idcurva from datoscurva where idcurva = "+ idcurva +" and fecha = '"+df_fecha_hoy+"'";
				rs = con.queryDB(qry);
				if(!rs.next()){
					r = false;
				}
			}else{
				r = false;
			}
			rs.close();
		}catch(Exception e){
				e.printStackTrace();
		}finally{
			log.info("ExisteSWAP (S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return r;
	}

	/***************************************************************************
	 *ValidaCurvas. valida si existe cargada una curva de fondeo y deposito, asi como que
	 *est� marcada una curva de fondeo como default para la moneda extablecida
	 *@author Salvador Saldivar Barajas
	 *@param icmoneda. ic de la moneda utilizada
	 *@return boolean. Regresa true si existen todas y false si falta alguna.
	 **********************************************************************************/
	public boolean ValidaCurvas(String icmoneda) throws NafinException{
		log.info("ValidaCurvas (E)");
		boolean r = true;
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		try{
			con.conexionDB();
			String qry = "select idcurva from curva where ic_moneda = " +icmoneda;
			rs = con.queryDB(qry + " and cg_curva_if = 'S'");
			if(!rs.next()){
				r = false;
			}
			rs.close();
			if(r){
				rs = con.queryDB(qry + " and cs_fondeo = 'S'");
				if(!rs.next()){
					r = false;
				}
			}
			rs.close();
			if(r){
				rs = con.queryDB(qry + " and cs_deposito = 'S'");
				if(!rs.next()){
					r = false;
				}
			}
			rs.close();
		}catch(Exception e){
				e.printStackTrace();
		}finally{
			log.info("ValidaCurvas (S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return r;
	}

	/***************************************************************************
	 *validaEsquemaLinea. valida si el esquema de recuperacion seleccionado esta habilitado
	 *para cotizar en linea.
	 *@author Salvador Saldivar Barajas
	 *@param icesquema. ic del esquema seleccionado
	 *@param icmoneda. ic de la moneda utilizada
	 *@return boolean. Regresa true si esta habilitada y false si no.
	 **********************************************************************************/
	public boolean validaEsquemaLinea(String icesquema, String icmoneda) throws NafinException{
		log.info("validaEsquemaLinea (E)");
		boolean r = true;
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		String qry = "";
		try{
			con.conexionDB();
			if(icmoneda.equals("1")){
				qry = "select cs_disponible_if_mn as disponible from cotcat_esquema_recup where ic_esquema_recup = "+icesquema;
			}else{
				qry = "select cs_disponible_if_dl as disponible from cotcat_esquema_recup where ic_esquema_recup = "+icesquema;
			}
			rs = con.queryDB(qry);
			if(rs.next()){
				String disponible = rs.getString("disponible");
				if(disponible.equals("N")){
					r = false;
				}
			}
			rs.close();
		}catch(Exception e){
				e.printStackTrace();
		}finally{
			log.info("validaEsquemaLinea (S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return r;
	}

	/***************************************************************************
	 *devuelve_mail. Regresa el mail al que se le enviara la notificacion de la cotizacion
	 *@author Salvador Saldivar Barajas
	 *@param icmoneda. ic de la moneda utilizada
	 *@return String. Correo electronico
	 **********************************************************************************/
    public Map<String,Object> devuelveMail(String icmoneda) throws NafinException{
	log.info("devuelveMail (E)");
	
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;	
	PreparedStatement ps	= null;
	Map<String,Object> email = new HashMap<>();
	StringBuilder qry = new StringBuilder();
	List lVarBind = new ArrayList();	
	try {
	    con.conexionDB();

	    qry.append("select cg_correo AS TESORERIA  , "+
		       " CG_CORREOPELEC AS PROELECTRONICOS , "+
		       " CG_CORREOOPE  AS OPERACIONES  "+
		       " from  parametrossistema " +
		       " where ic_moneda = ?   ");
	    lVarBind = new ArrayList();
	    lVarBind.add(icmoneda);
	    
	    log.debug("qry "+qry.toString());
	    log.debug("varBind "+lVarBind);

	    ps = con.queryPrecompilado(qry.toString(), lVarBind);
	    rs = ps.executeQuery();	
	    if (rs.next()) {
		email = new HashMap<>();
		email.put("TESORERIA", rs.getString("TESORERIA") == null ? "" : rs.getString("TESORERIA"));
		email.put("PROELECTRONICOS", rs.getString("PROELECTRONICOS") == null ? "" : rs.getString("PROELECTRONICOS"));
		email.put("OPERACIONES", rs.getString("OPERACIONES") == null ? "" : rs.getString("OPERACIONES"));
	    }
	    rs.close();
	    ps.close();

	} catch (Exception e) {
	    log.error("Exception en devuelveMail. " + e);
	    throw new AppException("Error devuelve EMail   ");
	} finally {
	    log.info("devuelveMail (S)");
	    if (con.hayConexionAbierta()) {
		con.cierraConexionDB();
	    }
	}
	return email;
    }

	/***************************************************************************
	 *completarDatos. Cuando es cotizacion en linea, hacen falta datos, asi que se
	 *insertan en una actualizacion al registro
	 *@author Salvador Saldivar Barajas
	 *@param ic. ic del registro a actualizar
	 *@param iNoCliente. ic del intermediario o ejecutivo
	 *@param tipo. Si es if o ejecutivo
	 *@param tasa. tasa cotizada
	 *@param ictasa. ic de la tasa
	 *@return void.
	 **********************************************************************************/
	public void completarDatos(String ic, String iNoCliente, String tipo, String tasa, String ictasa)
		throws NafinException{
		log.info(":completarDatos (E)");
		String qry = "";
		AccesoDB con = new AccesoDB();
		boolean ok = true;
		try{
			con.conexionDB();
			qry = "update cot_solic_esp set " +
				"ic_estatus = 2, " +
				"cg_tipo_cotizacion = 'F', " +
				"ig_tasa_md = " + tasa +", " +
				"df_cotizacion = sysdate, " +
				"cs_linea = 'S', " +
				"ic_tasa = " + ictasa + ", " +
				"CS_VOBO_EJEC = (SELECT CASE " +
				    "WHEN se.ic_if is null then 'S' " +
				    "WHEN IE.cg_solicitar_aut = 'N' THEN 'S' " +
				    "WHEN IE.cg_solicitar_aut = 'T' THEN 'N' " +
				    "WHEN IE.cg_solicitar_aut = 'M' AND SE.FN_MONTO >= IE.FN_MONTO_MAYOR_A THEN 'N' " +
				    "WHEN IE.cg_solicitar_aut = 'M' AND SE.FN_MONTO < IE.FN_MONTO_MAYOR_A THEN 'S' " +
				    "END as vobo " +
				"FROM COT_SOLIC_ESP SE, COTREL_IF_EJECUTIVO IE " +
				"WHERE ((SE.IC_IF = IE.IC_IF AND SE.IC_EJECUTIVO = IE.IC_EJECUTIVO) OR (SE.IC_IF IS NULL and rownum =1)) " +
				"AND SE.IC_SOLIC_ESP = " + ic + "), ";
				if(tipo.equals("IF"))
					qry += "ic_if = " + iNoCliente;
				else
					qry += "ic_ejecutivo = " + iNoCliente;
				qry += "where ic_solic_esp = " + ic;
				con.ejecutaSQL(qry);
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
		}finally{
			log.info(":completarDatos (S)");
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		}
	}

	/***************************************************************************
	 *tipoOperRiesgo. Regresa el tipo de piso y tipo de intermediario
	 *@author Salvador Saldivar Barajas
	 *@param iNoCliente. ic del intermediario financiero
	 *@return List. Datos en una lista
	 **********************************************************************************/
	public List tipoOperRiesgo(String iNoCliente) throws NafinException{
		log.info(":tipoOperRiesgo (E)");
		List lDatos = new ArrayList();
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		try{
			con.conexionDB();
			String qry = "select ig_tipo_piso, idtipointermediario from comcat_if where ic_if = "+ iNoCliente;
			rs = con.queryDB(qry);
			if(rs.next()){
				lDatos.add(rs.getString("ig_tipo_piso"));
				lDatos.add(rs.getString("idtipointermediario"));
			}
			rs.close();
		}catch(Exception e){
				e.printStackTrace();
		}finally{
			log.info(":tipoOperRiesgo (S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return lDatos;
	}

	/***************************************************************************
	 *buscaCurva. Regresa el id de la curva a utilizar que este marcada como default
	 *@author Salvador Saldivar Barajas
	 *@param icmoneda. ic de la moneda utilizada
	 *@return String. id de la curva
	 **********************************************************************************/
	public String buscaCurva(String icmoneda) throws NafinException{
		log.info(":buscaCurva (E)");
		String r = "";
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		try{
			con.conexionDB();
			String qry = "select idcurva from curva where ic_moneda = " + icmoneda + " and cs_deposito = 'S'";
			rs = con.queryDB(qry);
				if(rs.next()){
					r = rs.getString("idcurva");
				}
			rs.close();
		}catch(Exception e){
				e.printStackTrace();
		}finally{
			log.info(":buscaCurva (S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return r;
	}

	/***************************************************************************
	 *getIc_tasa. Regresa la tasa que le corresponde a la cotizacion
	 *@author Salvador Saldivar Barajas
	 *@param ictipotasa. tipo de tasa
	 *@param icmoneda. ic de la moneda utilizada
	 *@param icesquema. ic del esquema de recuperacion
	 *@param plazo_oper. plazo de la operacion
	 *@param cg_ut_ppc. periodicidad del pago de capital
	 *@param cg_ut_ppi. periodicidad del pago de interes
	 *@return String. ic_tasa
	 **********************************************************************************/
	public String getIc_tasa(String ictipotasa, String icmoneda, String icesquema, String plazo_oper, String cg_ut_ppc, String cg_ut_ppi)
		throws NafinException{
		log.info(":getIc_tasa (E)");
		String ic_tasa = "";
		if("1".equals(ictipotasa)){	//fija MN y USD
			if("1".equals(icmoneda))
				ic_tasa = "26";
			else if("54".equals(icmoneda))
				ic_tasa = "04";
		}else if("1".equals(icmoneda)){	// variable y protegida MN
			ic_tasa = "20";
		}else if("54".equals(icmoneda)){	//variable USD
			float plazo = 0;
			if(icesquema.equals("1"))	//cupon cero, se toma el plazo de la operacion por ser al vencimiento
				plazo = Float.parseFloat(plazo_oper);
			else if(icesquema.equals("4"))		//tipo renta, se toma la periodicidad de pago de capital porque es la misma que la de interes, la cual no se captura
				plazo = Float.parseFloat(cg_ut_ppc);
			else
				plazo = Float.parseFloat(cg_ut_ppi);	// tradicional y plan de pagos se toma la periodicidad de pago de interes capturada

			if(plazo <= 31f){
				ic_tasa = "04";
			}
			else if(plazo <= 93f){
				ic_tasa = "04";
			}
			else if(plazo <= 183f){
				ic_tasa = "04";
			}
			else if(plazo <= 365f){
				ic_tasa = "04";
			}
			else{
				ic_tasa = "04";
			}
		}
		log.info(":getIc_tasa (S)");
		return ic_tasa;
	}

	public String techoTasaProtegida() throws NafinException{
		log.info(":techoTasaProtegida (E)");
		String qry = "select ig_techo_tasa_p from parametrossistema where ic_moneda = 1";
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		String techo = " con techo de ";
		try{
			con.conexionDB();
			rs = con.queryDB(qry);
			if(rs.next()){
				techo += rs.getDouble("ig_techo_tasa_p")+" %";
			}
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info(":techoTasaProtegida (S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return techo;
	}

	/*****************************************************************
	 *getValidaFechasPagos Obtiene las fechas en que se haran los pagos
	 *de Capital
	 *@author Salvador Saldivar Barajas
	 *@param fini. primer Fecha de pago
	 *@param fvenc. penultmia fecha de pago
	 *@param periodo. Cada cuando se realizara el pago
	 *@return boolean. true si las fechas concuerdan con el periodo, false de lo contrario
	 *****************************************************************/
	public Hashtable getValidaFechasPagos(String fini, String fvenc, String periodo) throws NafinException{
		log.info("getFechasPagos (E)");
		DateFormat myDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		List ldias = null;
		List lfechas = null;
		Hashtable retorno = new Hashtable();
		boolean respuesta = true;
		try{
			java.util.Date dFechaIni	= myDateFormat.parse(fini);
			java.util.Date dFechaFin	= myDateFormat.parse(fvenc);
			Calendar 	cFechaIni		= Calendar.getInstance();
			Calendar	cFechaFin		= Calendar.getInstance();
			Calendar	cMes			= Calendar.getInstance();
			Calendar	cMesAnt			= Calendar.getInstance();
			int  diaPago	= Integer.parseInt(fvenc.substring(0,2).toString());
			//log.debug("dias de pago[[[[[[[[[ "+diaPago);
			int  diasDif	= 0;
			ldias = new ArrayList();
			lfechas = new ArrayList();
			cFechaIni.setTime(dFechaIni);
			cFechaFin.setTime(dFechaFin);
			cMesAnt.setTime(dFechaIni);
			cMes.setTime(dFechaIni);

			int incremento = 1;
			if("60".equals(periodo))
				incremento = 2;
			else if("90".equals(periodo))
			 	incremento = 3;
			 else if("120".equals(periodo))
			 	incremento = 4;
			else if("180".equals(periodo))
				incremento = 6;
			else if("360".equals(periodo))
				incremento = 12;

			long lMesAnt = 0;
			long lMes = 0;
			long lFecFin		= cFechaFin.getTime().getTime();
			lMes	= cMes.getTime().getTime();
			int diasDifTotal = (int)Math.round((lFecFin - lMes)/86400000.0);
			//log.debug("::::diasDifTotal[[[[[[[ "+diasDifTotal);
			while(lFecFin>=lMes){
				int		diaPagoTmp = diaPago;
				java.util.Date tmpDate = new java.util.Date();
				if(diaPagoTmp > cMes.getActualMaximum(Calendar.DAY_OF_MONTH))	//si el dia de pago es 31 y el mes tiene 30 dias
					diaPagoTmp = cMes.getActualMaximum(Calendar.DAY_OF_MONTH);

				cMes.set(Calendar.DAY_OF_MONTH,diaPagoTmp);
				lMesAnt	= cMesAnt.getTime().getTime();
				lMes	= cMes.getTime().getTime();
				if(lMesAnt!=lMes){
					diasDif += (int)Math.round((lMes - lMesAnt)/86400000.0);
					if(diasDif>0 && diasDif <= diasDifTotal){
						ldias.add(new Integer(diasDif));
						lfechas.add(myDateFormat.format(cMes.getTime()));
					}
				}
				tmpDate.setTime(lMes);
				cMesAnt.setTime(tmpDate);
				cMes.add(Calendar.MONTH,incremento);
			} // while
			if(ldias.size()>0){
				int posicion = ldias.size() - 1;
				//log.debug("posicion ::::  "+posicion);
				if(Integer.parseInt(ldias.get(posicion).toString()) != diasDifTotal){
					respuesta = false;
					//log.debug("]]]] Periodos incompletos [[[");
					ldias.add(new Integer(diasDifTotal));
					lfechas.add(myDateFormat.format(cFechaFin.getTime()));
				}
			}else{
				//respuesta = false;
			}
			retorno.put("Npagos",new Integer(ldias.size()));
			retorno.put("Respuesta",new Boolean(respuesta));
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info("getFechasPagos (S)");
		}
		/*log.debug("No. de pagos::: "+lfechas.size());
		log.debug("Dias de pago::: "+ldias);
		log.debug("Fechas de pago::: "+lfechas);*/
		return retorno;
	}

	public int restaFechas(String fechaDisp, String fechaRestar) throws NafinException{
		int diasDifTotal = 0;
		try{
			DateFormat myDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date dFechaIni	= myDateFormat.parse(fechaDisp);
			java.util.Date dFechaFin	= myDateFormat.parse(fechaRestar);
			Calendar 	cFechaIni		= Calendar.getInstance();
			Calendar	cFechaFin		= Calendar.getInstance();
			cFechaIni.setTime(dFechaIni);
			cFechaFin.setTime(dFechaFin);
			long lFecFin		= cFechaFin.getTime().getTime();
			long lFecIni	= cFechaIni.getTime().getTime();
			diasDifTotal = (int)Math.round((lFecFin - lFecIni)/86400000.0);
			//log.debug("::::diasDifTotal[[[[[[[ "+diasDifTotal);
		}catch(Exception e){
			e.printStackTrace();
		}
		return diasDifTotal;
	}

	private List getPagosIntTradpi(String inicio,String  primerf, String penultimaf, String fin, String ppi, String diap){
    	DateFormat myDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		List dias = null;
		List ldAux = null;
		List lfechas = null;
		try{
			java.util.Date dFechaIni	= myDateFormat.parse(primerf);
			Calendar	cMes			= Calendar.getInstance();
			Calendar	cMesAnt			= Calendar.getInstance();
			dias = new ArrayList();
			lfechas = new ArrayList();
			cMesAnt.setTime(dFechaIni);
			cMes.setTime(dFechaIni);

			int incremento = -1;
			if("60".equals(ppi))
				incremento = -2;
			else if("90".equals(ppi))
			 	incremento = -3;
			 else if("120".equals(ppi))
			 	incremento = -4;
			else if("180".equals(ppi))
				incremento = -6;
			else if("360".equals(ppi))
				incremento = -12;

			int  diasDifPrimera	= restaFechas(inicio,primerf);
			int  diasDif	= 	diasDifPrimera;
			long lMesAnt = 0;
			long lMes = 0;
			/*log.debug("inicio::: "+inicio);
			log.debug("primerf::: "+primerf);
			log.debug("penultimaf::: "+penultimaf);
			log.debug("fin::: "+fin);
			log.debug("ppi::: "+ppi);
			log.debug("diap::: "+diap);*/
			while(diasDif>5){
				lMesAnt	= cMesAnt.getTime().getTime();
				lMes	= cMes.getTime().getTime();
				diasDif += (int)Math.round((lMes - lMesAnt)/86400000.0);
				if(diasDif>5){
					dias.add(new Integer(diasDif));
					lfechas.add(myDateFormat.format(cMes.getTime()));
				}
				java.util.Date tmpDate = new java.util.Date();
				tmpDate.setTime(lMes);
				cMesAnt.setTime(tmpDate);
				cMes.add(Calendar.MONTH,incremento);
			}
			ldAux = new ArrayList();
			for(int b=dias.size()-1;b>=0;b--){
				ldAux.add(new Integer(Integer.parseInt(dias.get(b).toString())));
			}
			//log.debug("Dias de pago::1: "+ldAux);
			dias = new ArrayList();
			dias = ldAux;
		}catch(Exception e){
			e.printStackTrace();
		}
		//log.debug("Dias de pago::2: "+dias);
		//log.debug("Fechas de pago::: "+lfechas);

		int b = 0;
		int difer = 0;
		//rango entre las fechas de pago de capital
		try{
			difer = restaFechas(inicio, primerf);
		}catch(Exception er){
			er.printStackTrace();
		}
		//log.debug("difer:::1:: "+difer);
		ldAux = new ArrayList();
		ldAux = getDiasAPagarik(primerf,penultimaf,difer,diap,ppi);
		for(b=0;b<ldAux.size();b++){
			dias.add(new Integer(Integer.parseInt(ldAux.get(b).toString())));
		}
		//log.debug("Dias de pago::3: "+dias);

		//rango desde la penultima fecha de pago de capital hasta la fecha de vencimiento
		try{
			difer = restaFechas(inicio, penultimaf);
		}catch(Exception er){
			er.printStackTrace();
		}
		//log.debug("difer:::2:: "+difer);
		ldAux = new ArrayList();
		ldAux = getDiasAPagarik(penultimaf,fin,difer,diap,ppi);
		for(b=0;b<ldAux.size();b++){
			dias.add(new Integer(Integer.parseInt(ldAux.get(b).toString())));
		}
		//log.debug("Dias de pago::4: "+dias);

		return dias;
    }

    private List getDiasAPagarik(String fini, String ffin, int diferencia, String diap, String ppi){
	/*log.info("getDiasAPagarik (E)");
	log.debug("primerf::: "+fini);
	log.debug("penultimaf::: "+ffin);
	log.debug("difer::::: "+diferencia);
	log.debug("ppi::: "+ppi);
	log.debug("diap::: "+diap);*/
	List dias = null;
	try{
		java.util.Date dFechaIni	= Comunes.parseDate(fini);
		java.util.Date dFechaFin	= Comunes.parseDate(ffin);
		Calendar 	cFechaIni		= Calendar.getInstance();
		Calendar	cFechaFin		= Calendar.getInstance();
		Calendar	cMes			= Calendar.getInstance();
		Calendar	cMesAnt			= Calendar.getInstance();
		int  diaPago	= Integer.parseInt(diap);
		//log.debug("dias de pago[[[[[[[[[ "+diaPago);
		int  diasDif	= diferencia;
		dias = new ArrayList();
		cFechaIni.setTime(dFechaIni);
		cFechaFin.setTime(dFechaFin);
		cMesAnt.setTime(dFechaIni);
		cMes.setTime(dFechaIni);

		int incremento = 1;
		if("60".equals(ppi))
			incremento = 2;
		else if("90".equals(ppi))
		 	incremento = 3;
		else if("180".equals(ppi))
			incremento = 6;
		else if("360".equals(ppi))
			incremento = 12;
		long lMesAnt = 0;
		long lMes = 0;
		long lFecFin		= cFechaFin.getTime().getTime();
		lMes	= cMes.getTime().getTime();
		int diasDifTotal = (int)Math.round((lFecFin - lMes)/86400000.0) + diasDif;
		lMes	= cMes.getTime().getTime();
		//log.debug("::::diasDifTotal[[[[[[[ "+diasDifTotal);
		//long lFechaDispIni	= Comunes.parseDate(fini).getTime();
		//int numDisp = Integer.parseInt("1");

		int i = 1;
		while(lFecFin>=lMes){
			int		diaPagoTmp = diaPago;
			java.util.Date tmpDate = new java.util.Date();
			if(diaPagoTmp > cMes.getActualMaximum(Calendar.DAY_OF_MONTH))	//si el dia de pago es 31 y el mes tiene 30 dias
				diaPagoTmp = cMes.getActualMaximum(Calendar.DAY_OF_MONTH);

			cMes.set(Calendar.DAY_OF_MONTH,diaPagoTmp);
			lMesAnt	= cMesAnt.getTime().getTime();
			lMes	= cMes.getTime().getTime();
			if(lMesAnt!=lMes){
				diasDif += (int)Math.round((lMes - lMesAnt)/86400000.0);

				/*if(numDisp>1&&i<numDisp){
					long			lfechaDisp	= Comunes.parseDate(sol.getDf_disposicion(i-1)).getTime();
					double			montoDisp	= Double.parseDouble(sol.getFn_monto_disp(i-1));
					diasDif		= (int)Math.round((lfechaDisp - lFechaDispIni)/86400000.0);
				}*/
				if(diasDif>5 && diasDif <= diasDifTotal){
					dias.add(new Integer(diasDif));
				}
			}
			tmpDate.setTime(lMes);
			cMesAnt.setTime(tmpDate);
			cMes.add(Calendar.MONTH,incremento);
			i++;
		} // while
		if(dias.size()>0){
			int posicion = dias.size() - 1;
			if(Integer.parseInt(dias.get(posicion).toString()) != diasDifTotal){
				//log.debug("]]]] Se agregara la ultima fecha [[[");
				if((diasDifTotal-Integer.parseInt(dias.get(posicion).toString()))<5){
					dias.set(posicion,new Integer(diasDifTotal));
				}else{
					dias.add(new Integer(diasDifTotal));
				}
			}
		}else{
			dias.add(new Integer(diasDifTotal));
		}

	}catch(Exception e){
		e.printStackTrace();
	}finally{
		log.info("getDiasAPagarik (S)");
	}
	//log.debug("getDiasAPagarik:::dias:::: "+dias);
	return dias;
}



    /**
     * metodo para sacar los correos parametrizados en la pantalla
     * ADMIN TESORERIA /Tasas de Cr�dito Nafin / Parametrizar Tesorer�a / Mantenimiento al Ejecutivo 
     * QC - DLHC 2017
     * @param idEjecutivo     
     * @return
     * @throws NafinException
     */
    private String emailEjecutivosNafin(String idEjecutivo ) throws NafinException{
	log.info("emailEjecutivosNafin (E)");
	StringBuilder qry  = new StringBuilder();	
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	String email = "";
	PreparedStatement ps = null;
	List lVarBind = new ArrayList();
	try{
	    
	    con.conexionDB();	    
	    	    
	    qry  = new StringBuilder();	
	    qry.append( " select CG_MAIL  AS EMAIL  from COTCAT_EJECUTIVO "+
			" where IC_EJECUTIVO = ? ");
	    lVarBind = new ArrayList();
	    lVarBind.add(idEjecutivo);
	    
	    log.debug("qry "+qry.toString()+" \n varBind "+lVarBind);

	    ps = con.queryPrecompilado(qry.toString(), lVarBind);
	    rs = ps.executeQuery();	
	    if(rs.next()){
		email = rs.getString("EMAIL");
	    }
	    rs.close();
	    
	    
	}catch(Exception e){
	    e.printStackTrace();
	}finally{
	    log.info("emailEjecutivosNafin (S)");
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}
	return email;
    }

    /**
     * metodo para sacar los correos parametrizados en la pantalla
     * ADMIN TESORERIA/ Tasas de Cr�dito Nafin / Parametrizar Tesorer�a / Asignaci�n Ejecutivo al IF 
     *  QC - DLHC 2017
     * @param idEjecutivo
     * @param icIf
     * @return
     * @throws NafinException
     */
   private String emailAsignaEjecutivo(String idEjecutivo, String icIf ) throws NafinException{
	log.info("emailAsignaEjecutivo (E)");
	StringBuilder qry  = new StringBuilder();	
	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	String email = "";
	PreparedStatement ps = null;
	List lVarBind = new ArrayList();
	try{
	    
	    con.conexionDB();	    
	    	    
	    qry  = new StringBuilder();	
	    qry.append( " select CG_CORREOS as EMAILS  from COTREL_IF_EJECUTIVO "+
			" where IC_EJECUTIVO = ? "+
			" and  IC_IF = ? ");
	    
	    lVarBind = new ArrayList();
	    lVarBind.add(idEjecutivo);
	    lVarBind.add(icIf);
	    log.debug("qry "+qry.toString()+" \n varBind "+lVarBind);

	    ps = con.queryPrecompilado(qry.toString(), lVarBind);
	    rs = ps.executeQuery();	
	    if(rs.next()){
		email = rs.getString("EMAILS");
	    }
	    rs.close();
	    
	    
	}catch(Exception e){
	    log.error(" emailAsignaEjecutivo(Exception) "+e);
	    throw new AppException("Error emailAsignaEjecutivo ");
	}finally{
	    log.info("emailAsignaEjecutivo (S)");
	    if(con.hayConexionAbierta()){
		con.cierraConexionDB();
	    }
	}
	return email;
    }
   
    /**
     * este metodo es para el pdf que se adjunta en el correo que se envia al generar una solicitud
     * ADMIN IF COTIZ/ Tasas de Cr�dito Nafin / Solicitud de Cotizaci�n / Captura de Solicitudes
     * @param datos
     * @throws AppException
     */
    private String generarPDFCapCotizacion(Map<String,Object> datos  ) throws AppException{
	log.debug("generarPDFCapCotizacion  (e) ");
	// solo debe de ser estatus solicitada (1)

	String nombreArchivo = "";
	int tipoClase = 0;
	String clase = "";

	try {

	    String icSolicitud = (String) datos.get("icSolicitud");
	    String strDirectorioTemp = (String) datos.get("strDirectorioTemp");
	    String strPais = (String) datos.get("strPais");
	    String iNoNafinElectronico = (String) datos.get("iNoNafinElectronico");
	    String sesExterno = (String) datos.get("sesExterno");
	    String strNombre = (String) datos.get("strNombre");
	    String strNombreUsuario = (String) datos.get("strNombreUsuario");
	    String strLogo = (String) datos.get("strLogo");
	    String strDirectorioPublicacion = (String) datos.get("strDirectorioPublicacion");	    
	    String strTipoUsuario = (String) datos.get("strTipoUsuario");

	    log.debug("icSolicitud  " + icSolicitud);

	    Solicitud consulta = new Solicitud();
	    consulta = this.consultaSolicitud(icSolicitud);

	    nombreArchivo = consulta.getNumero_solicitud() + ".pdf";
	    boolean muestraCancelado = false;
	    ComunesPDF pdfDoc = new ComunesPDF(1, strDirectorioTemp + nombreArchivo,
				      "Solicitud N�mero: " + consulta.getNumero_solicitud() + "           Pagina ",
				      muestraCancelado);
	    float []  widthsDocs= { 0.35f, 0.75f };
	    String [] meses = {  "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"  };
	    String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	    String diaActual = fechaActual.substring(0, 2);
	    String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
	    String anioActual = fechaActual.substring(6, 10);
	    String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	    String tipooper = consulta.getTipoOper();
	    

	    pdfDoc.encabezadoConImagenes(pdfDoc, strPais, iNoNafinElectronico, sesExterno, strNombre, strNombreUsuario, strLogo, strDirectorioPublicacion);
	    pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +  " ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);
	    pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);

	    pdfDoc.setTable(2, 65);
	    pdfDoc.setCell("Datos de la Cotizaci�n", "celda04", ComunesPDF.CENTER, 2, 1, 1);
	    pdfDoc.setCell("N�mero de Solicitud:", "formas", ComunesPDF.LEFT, 1, 1, 1);
	    pdfDoc.setCell(consulta.getNumero_solicitud(), "formas", ComunesPDF.LEFT, 1, 1, 1);
	    pdfDoc.setCell("Fecha de Solicitud:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  1);
	    pdfDoc.setCell(consulta.getDf_solicitud(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  1);
	    pdfDoc.setCell("Hora de Solicitud:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,   1);
	    pdfDoc.setCell(consulta.getHora_solicitud(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 1);


	    pdfDoc.setCell("Usuario:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 1);
	    pdfDoc.setCell(consulta.getIc_usuario() + " - " + consulta.getNombre_usuario(),((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 1);
	    pdfDoc.addTable();
	    pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
	    pdfDoc.setTable(2, 90, widthsDocs);
	    	    
	    pdfDoc.setCell("Datos del Ejecutivo Nafin", "celda04", ComunesPDF.LEFT, 2, 1, 0);
	    pdfDoc.setCell("Nombre del Ejecutivo", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell(consulta.getNombre_ejecutivo(), "formas", ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("Correo electr�nico", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,			   0);
	    pdfDoc.setCell(consulta.getCg_mail(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,			   0);
	    pdfDoc.setCell("Tel�fono", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell(consulta.getCg_telefono(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    pdfDoc.setCell("Fax", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell(consulta.getCg_fax(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);


	    pdfDoc.setCell("Datos del Intermediario Financiero/Acreditado", "celda04", ComunesPDF.LEFT, 2, 1, 0);
	    pdfDoc.setCell("Tipo de operaci�n", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell(tipooper, "formas", ComunesPDF.LEFT, 1, 1, 0);

	    if (!"Primer Piso".equals(tipooper)) {
		pdfDoc.setCell("Nombre del Intermediario", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
		pdfDoc.setCell((!"".equals(consulta.getNombre_if_ci())) ? consulta.getNombre_if_ci() : consulta.getNombre_if_cs(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		if (!"IF".equals(strTipoUsuario) && !"".equals(consulta.getRiesgoIf())) {
		    pdfDoc.setCell("Riesgo del Intermediario", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(consulta.getRiesgoIf(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
		}
	    }


	    if (!"IF".equals(strTipoUsuario) && !"".equals(consulta.getCg_acreditado())) {
		pdfDoc.setCell("Nombre del Acreditado", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,    0);
		pdfDoc.setCell(consulta.getCg_acreditado(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		if ("Primer Piso".equals(tipooper)) {
		    pdfDoc.setCell("Riesgo del Acreditado", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(consulta.getRiesgoAc(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		}
	    }

	    tipoClase = 1;

	    pdfDoc.setCell("Caracter�sticas de la Operaci�n", "celda04", ComunesPDF.LEFT, 2, 1, 0);

	    if (!"".equals(consulta.getCg_programa())) {
		pdfDoc.setCell("Nombre del programa o\nacreditado final:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),  ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(consulta.getCg_programa(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,0);
	    }
	    pdfDoc.setCell("Monto total requerido:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    pdfDoc.setCell("$ " + Comunes.formatoDecimal(consulta.getFn_monto(), 2) +
			   ( "1".equals(consulta.getIc_moneda()) ? " Pesos" : " D�lares"), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"),
			   ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("Tipo de tasa requerida:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    pdfDoc.setCell(consulta.getTipo_tasa(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,   0);
	    pdfDoc.setCell("N�mero de Disposiciones", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    pdfDoc.setCell(consulta.getIn_num_disp(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    if (Integer.parseInt(consulta.getIn_num_disp()) > 1) {
		pdfDoc.setCell("Disposiciones m�ltiples. Tasa de Inter�s:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),   ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(("S".equals(consulta.getCs_tasa_unica()) ? "�nica" : "Varias"),((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    }
	    pdfDoc.setCell("Plazo de la operaci�n:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    pdfDoc.setCell(consulta.getIg_plazo() + " D�as", ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT,  1, 1, 0);
	    pdfDoc.setCell("Esquema de recuperaci�n del capital:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),  ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell(consulta.getEsquema_recup(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,   0);

	    if (!"1".equals(consulta.getIc_esquema_recup())) {
		
		//1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
		if ("2".equals(consulta.getIc_esquema_recup())  || "3".equals(consulta.getIc_esquema_recup())   ) {
		    pdfDoc.setCell("Periodicidad del pago de Inter�s:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),  ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(consulta.periodoTasa(consulta.getCg_ut_ppi()), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		}
		if (!"3".equals(consulta.getIc_esquema_recup())  && ( "2".equals(consulta.getIc_esquema_recup()) || "4".equals(consulta.getIc_esquema_recup())    ) ) {
		    pdfDoc.setCell("Periodicidad del Capital:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
		    pdfDoc.setCell(consulta.periodoTasa(consulta.getCg_ut_ppc()), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"),  ComunesPDF.LEFT, 1, 1, 0);
		}

		if ( "2".equals(consulta.getIc_esquema_recup())  && !"".equals(consulta.getCg_pfpc())) {
		    pdfDoc.setCell("Primer fecha de pago de Capital:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT,  1, 1, 0);
		    pdfDoc.setCell(consulta.getCg_pfpc(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
		    pdfDoc.setCell("Pen�ltima fecha de pago de Capital:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),  ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(consulta.getCg_pufpc(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
		}
	    }
	    pdfDoc.addTable();

	    pdfDoc.setTable(4, 90);
	    pdfDoc.setCell("Detalle de Disposiciones", "celda04", ComunesPDF.LEFT, 4, 1, 0);
	    pdfDoc.setCell("N�mero de Disposici�n", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("Monto", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("Fecha de Disposici�n", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("Fecha de Vencimiento", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    for (int i = 0; i < Integer.parseInt(consulta.getIn_num_disp()); i++) {
		clase = ((i % 2) != 0) ? "formas" : "celda01";                
		pdfDoc.setCell(Integer.toString(i + 1) + "", clase, ComunesPDF.LEFT, 1, 1, 0);                
		pdfDoc.setCell("$ " + Comunes.formatoDecimal(consulta.getFn_monto_disp(i), 2), clase, ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(consulta.getDf_disposicion(i), clase, ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(consulta.getDf_vencimiento(i), clase, ComunesPDF.LEFT, 1, 1, 0);
	    }
	    pdfDoc.addTable();
	    if ((consulta.getPagos()).size() > 0) {
		ArrayList alFilas = consulta.getPagos();
		pdfDoc.setTable(3, 90);
		pdfDoc.setCell("Plan de Pagos Capital", "celda04", ComunesPDF.LEFT, 3, 1, 0);
		pdfDoc.setCell("N�mero de Pago", "formas", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Fecha de Pago", "formas", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Monto a pagar", "formas", ComunesPDF.LEFT, 1, 1, 0);
		for (int i = 0; i < alFilas.size(); i++) {
		    ArrayList alColumnas = (ArrayList) alFilas.get(i);
		    clase = ((i % 2) != 0) ? "formas" : "celda01";
		    pdfDoc.setCell(alColumnas.get(0).toString(), clase, ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(alColumnas.get(1).toString(), clase, ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(Comunes.formatoDecimal(alColumnas.get(2).toString(), 2), clase, ComunesPDF.LEFT, 1, 1,  0);
		}
		pdfDoc.addTable();
	    }

	    if (!"".equals(consulta.getCg_observaciones().trim())   ) {
		pdfDoc.setTable(3, 90);
		pdfDoc.setCell("Observaciones:", "celda04", ComunesPDF.LEFT, 3, 1, 0);
		pdfDoc.setCell(consulta.getCg_observaciones(), "formas", ComunesPDF.LEFT, 3, 1, 0);
		pdfDoc.addTable();
	    }

	    pdfDoc.endDocument();

	} catch (Exception t) {
	    throw new AppException("Error al generar PDF  ", t);
	}
	log.debug("generarPDFCapCotizacion  (S) ");
	return nombreArchivo;
	
    }
   
   
   
   
   
    /**
     * este metodo es para el pdf que se adjunta en el correo que se envia al hacer la Cotizaci�n para tomar en firme
     * ADMIN IF COTIZ/ Tasas de Cr�dito Nafin / Solicitud de Cotizaci�n / Cotizaci�n para tomar en firme
     * @param datos
     * @throws AppException
     */
    private String generarPDFCotizacionFirme(Map<String,Object> datos  ) throws AppException{
	log.debug("generarPDFCotizacionFirme  (e) ");
	
	String	nombreArchivo= "";
	String periodo = "";	
	String clase="";
	int tipoClase=0;	
	try {

	    String icSolicitud = (String) datos.get("icSolicitud");
	    String strDirectorioTemp = (String) datos.get("strDirectorioTemp");
	    String strPais = (String) datos.get("strPais");
	    String iNoNafinElectronico = (String) datos.get("iNoNafinElectronico");
	    String sesExterno = (String) datos.get("sesExterno");
	    String strNombre = (String) datos.get("strNombre");
	    String strNombreUsuario = (String) datos.get("strNombreUsuario");
	    String strLogo = (String) datos.get("strLogo");
	    String strDirectorioPublicacion = (String) datos.get("strDirectorioPublicacion");	    
	    String strTipoUsuario = (String) datos.get("strTipoUsuario");
	    String recibo = (String) datos.get("recibo");
	    String origen = (String) datos.get("origen");
	    
	    log.debug("icSolicitud  " + icSolicitud);

	    Solicitud consulta = new Solicitud();
	    consulta = this.consultaSolicitud(icSolicitud);
	    
	    
	    nombreArchivo = consulta.getNumero_solicitud() + ".pdf";
	    boolean muestraCancelado = false;
	    if ("8".equals(consulta.getIcEstatus())){
		muestraCancelado = true;
	    }
	    ComunesPDF pdfDoc =   new ComunesPDF(1, strDirectorioTemp + nombreArchivo, "Solicitud N�mero: " + consulta.getNumero_solicitud() + "           Pagina ",muestraCancelado);
	    float [] widthsDocs = { 0.35f, 0.75f };

	    String [] meses = {
		       "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre",
		       "Octubre", "Noviembre", "Diciembre"
	    };
	    String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	    String diaActual = fechaActual.substring(0, 2);
	    String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
	    String anioActual = fechaActual.substring(6, 10);
	    String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());


	    pdfDoc.encabezadoConImagenes(pdfDoc, strPais, iNoNafinElectronico, sesExterno, strNombre, strNombreUsuario, strLogo, strDirectorioPublicacion);
	    pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);
	    pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);

	    if (!"".equalsIgnoreCase(recibo)) {
		pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
		pdfDoc.addText("La autentificaci�n se llev� a cabo con �xito", "formas", ComunesPDF.CENTER);
		pdfDoc.addText("Recibo: " + recibo, "formas", ComunesPDF.CENTER);
		pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
	    }
	    pdfDoc.setTable(2, 65);
	    pdfDoc.setCell("Datos de la Cotizaci�n", "celda04", ComunesPDF.CENTER, 2, 1, 1);
	    pdfDoc.setCell("N�mero de Solicitud:", "formas", ComunesPDF.LEFT, 1, 1, 1);
	    pdfDoc.setCell(consulta.getNumero_solicitud(), "formas", ComunesPDF.LEFT, 1, 1, 1);
	    pdfDoc.setCell("Fecha de Solicitud:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  1);
	    pdfDoc.setCell(consulta.getDf_solicitud(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  1);
	    pdfDoc.setCell("Hora de Solicitud:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  1);
	    pdfDoc.setCell(consulta.getHora_solicitud(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1,  1, 1);

	    if (   !"1".equals(consulta.getIcEstatus())  &&   !"3".equals(consulta.getIcEstatus()) &&  !"4".equals(consulta.getIcEstatus()) ) {  //1=Solicictada,3=Rechazada,4=En proceso
		String auxFecha = "Cotizaci�n";
		if ("8".equals(consulta.getIcEstatus())){
		    auxFecha = "Cancelaci�n";
		}
		pdfDoc.setCell("Fecha de " + auxFecha + ":", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  1);
		pdfDoc.setCell(consulta.getDf_cotizacion(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  1);
		pdfDoc.setCell("Hora de " + auxFecha + ":", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  1);
		pdfDoc.setCell(consulta.getHora_cotizacion(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 1);
	    }
	    
	    pdfDoc.setCell("Usuario:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 1);
	    pdfDoc.setCell(consulta.getIc_usuario() + " - " + consulta.getNombre_usuario(),((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 1);
	   
	   
	   if (  "7".equals(consulta.getIcEstatus()) ||   "8".equals(consulta.getIcEstatus()) || "9".equals(consulta.getIcEstatus()) || "TF".equals(origen)) { 
	  
	  	if (  !"TF".equals(origen) ) {
		    pdfDoc.setCell("N�mero de Recibo:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 1);
		    pdfDoc.setCell(consulta.getIg_num_referencia(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 1);
		}
		pdfDoc.setCell("En caso de que la disposici�n de los recursos no se lleve a cabo total o parcialmente\n" +
			       "en la fecha estipulada, Nacional Financiera aplicar� el cobro de una comisi�n\n" +
			       "sobre los montos no dispuestos, conforme a las pol�ticas de cr�dito aplicables\n" +
			       "a este tipo de operaciones.", "formas", ComunesPDF.CENTER, 2, 1, 1);
	    }
	    pdfDoc.addTable();
	    pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
	    pdfDoc.setTable(2, 90, widthsDocs);
	    
	    tipoClase = 0;

	    if (  !"TFE".equals(origen) ) {
		pdfDoc.setCell("Datos del Ejecutivo Nafin", "celda04", ComunesPDF.LEFT, 2, 1, 0);
		pdfDoc.setCell("Nombre del Ejecutivo", "formas", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(consulta.getNombre_ejecutivo(), "formas", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Correo electr�nico", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,    0);
		pdfDoc.setCell(consulta.getCg_mail(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Tel�fono", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(consulta.getCg_telefono(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Fax", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(consulta.getCg_fax(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    }

	    tipoClase = 0;
	    String tipooper = consulta.getTipoOper();
	    if (  !"TFE".equals(origen) ) {
		pdfDoc.setCell("Datos del Intermediario Financiero/Acreditado", "celda04", ComunesPDF.LEFT, 2, 1, 0);
		pdfDoc.setCell("Tipo de operaci�n", "formas", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(tipooper, "formas", ComunesPDF.LEFT, 1, 1, 0);

		if (!"Primer Piso".equals(tipooper)) {
		    pdfDoc.setCell("Nombre del Intermediario", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
		    pdfDoc.setCell((!"".equals(consulta.getNombre_if_ci())) ? consulta.getNombre_if_ci() : consulta.getNombre_if_cs(),((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		    if (!"IF".equals(strTipoUsuario) && !"".equals(consulta.getRiesgoIf())) {
			pdfDoc.setCell("Riesgo del Intermediario", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
			pdfDoc.setCell(consulta.getRiesgoIf(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		    }
		}
	    }

	    if (!"IF".equals(strTipoUsuario) && !"".equals(consulta.getCg_acreditado())) {
		pdfDoc.setCell("Nombre del Acreditado", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,   0);
		pdfDoc.setCell(consulta.getCg_acreditado(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
		if ("Primer Piso".equals(tipooper)  && !"TFE".equals(origen) ) {
		    pdfDoc.setCell("Riesgo del Acreditado", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(consulta.getRiesgoAc(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		}
	    }

	    tipoClase = 1;

	    pdfDoc.setCell("Caracter�sticas de la Operaci�n", "celda04", ComunesPDF.LEFT, 2, 1, 0);

	    if (!"".equals(consulta.getCg_programa())) {
		pdfDoc.setCell("Nombre del programa o\nacreditado final:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),    ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(consulta.getCg_programa(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    }
	    pdfDoc.setCell("Monto total requerido:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    pdfDoc.setCell("$ " + Comunes.formatoDecimal(consulta.getFn_monto(), 2) +(  "1".equals(consulta.getIc_moneda()) ? " Pesos" : " D�lares"), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("Tipo de tasa requerida:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell(consulta.getTipo_tasa(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("N�mero de Disposiciones", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    pdfDoc.setCell(consulta.getIn_num_disp(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    if (Integer.parseInt(consulta.getIn_num_disp()) > 1) {
		pdfDoc.setCell("Disposiciones m�ltiples. Tasa de Inter�s:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(("S".equals(consulta.getCs_tasa_unica()) ? "�nica" : "Varias"),((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    }
	    pdfDoc.setCell("Plazo de la operaci�n:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
	    pdfDoc.setCell(consulta.getIg_plazo() + " D�as", ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT,  1, 1, 0);
	    pdfDoc.setCell("Esquema de recuperaci�n del capital:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),  ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell(consulta.getEsquema_recup(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
	    
	    if (!"1".equals(consulta.getIc_esquema_recup())) {
		
		//1=Cupon cero,2=Tradicional,3=Plan de pagos,4=Rentas
		
		if ("2".equals(consulta.getIc_esquema_recup())  || "3".equals(consulta.getIc_esquema_recup()) ) {		
		    pdfDoc.setCell("Periodicidad del pago de Inter�s:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),  ComunesPDF.LEFT, 1, 1, 0);		    
		    pdfDoc.setCell(consulta.periodoTasa(consulta.getCg_ut_ppi()), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		}
		if (!"3".equals(consulta.getIc_esquema_recup()) &&
                    ("2".equals(consulta.getIc_esquema_recup()) || "4".equals(consulta.getIc_esquema_recup())) ) {
                    pdfDoc.setCell("Periodicidad del Capital:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),
                                   ComunesPDF.LEFT, 1, 1, 0);
                    pdfDoc.setCell(consulta.periodoTasa(consulta.getCg_ut_ppc()),
                                   ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
                }
		
		if ("2".equals(consulta.getIc_esquema_recup())  || !"".equals(consulta.getCg_pfpc() ) ) {
		    pdfDoc.setCell("Primer fecha de pago de Capital:", ((tipoClase % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(consulta.getCg_pfpc(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
		    pdfDoc.setCell("Pen�ltima fecha de pago de Capital:", ((tipoClase % 2 == 0) ? "celda01" : "formas"),  ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(consulta.getCg_pufpc(), ((tipoClase++ % 2 == 0) ? "celda01" : "formas"), ComunesPDF.LEFT, 1, 1,  0);
		}
	    }
	    pdfDoc.addTable();

	    pdfDoc.setTable(4, 90);
	    pdfDoc.setCell("Detalle de Disposiciones", "celda04", ComunesPDF.LEFT, 4, 1, 0);
	    pdfDoc.setCell("N�mero de Disposici�n", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("Monto", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("Fecha de Disposici�n", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    pdfDoc.setCell("Fecha de Vencimiento", "formas", ComunesPDF.LEFT, 1, 1, 0);
	    for (int i = 0; i < Integer.parseInt(consulta.getIn_num_disp()); i++) {
		clase = ((i % 2) != 0) ? "formas" : "celda01";
		pdfDoc.setCell(Integer.toString(i + 1)+ "", clase, ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("$ " + Comunes.formatoDecimal(consulta.getFn_monto_disp(i), 2), clase, ComunesPDF.LEFT, 1, 1,    0);
		pdfDoc.setCell(consulta.getDf_disposicion(i), clase, ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell(consulta.getDf_vencimiento(i), clase, ComunesPDF.LEFT, 1, 1, 0);
	    }
	    pdfDoc.addTable();
	    if ((consulta.getPagos()).size() > 0) {
		ArrayList alFilas = consulta.getPagos();
		pdfDoc.setTable(3, 90);
		pdfDoc.setCell("Plan de Pagos Capital", "celda04", ComunesPDF.LEFT, 3, 1, 0);
		pdfDoc.setCell("N�mero de Pago", "formas", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Fecha de Pago", "formas", ComunesPDF.LEFT, 1, 1, 0);
		pdfDoc.setCell("Monto a pagar", "formas", ComunesPDF.LEFT, 1, 1, 0);
		for (int i = 0; i < alFilas.size(); i++) {
		    ArrayList alColumnas = (ArrayList) alFilas.get(i);
		    clase = ((i % 2) != 0) ? "formas" : "celda01";
		    pdfDoc.setCell(alColumnas.get(0).toString(), clase, ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(alColumnas.get(1).toString(), clase, ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell(Comunes.formatoDecimal(alColumnas.get(2).toString(), 2), clase, ComunesPDF.LEFT, 1, 1,  0);
		}
		pdfDoc.addTable();
	    }
	    //Obteniendo el periodo de las tasas
	    if ("1".equals(consulta.getIc_esquema_recup())) {
		periodo = "Al Vencimiento";
	    } else if ("4".equals(consulta.getIc_esquema_recup()) && "0".equals(consulta.getCg_ut_ppc())) {
		periodo = "Al Vencimiento";                
	    } else   if (!"".equals( consulta.periodoTasa(consulta.getCg_ut_ppi() )) ) {                    
		periodo = consulta.periodoTasa(consulta.getCg_ut_ppi()) + "mente";	         
            } else   if (!"".equals( consulta.periodoTasa(consulta.getCg_ut_ppc() )) ) {                
		periodo = consulta.periodoTasa(consulta.getCg_ut_ppc()) + "mente";
	    } else {
		periodo = "Al Vencimiento";
	    }

	    if (!"TF".equals(origen) && !"TFE".equals(origen)) {
		
		 if ("2".equals(consulta.getIcEstatus()) || "5".equals(consulta.getIcEstatus())) {
		    pdfDoc.setTable(1, 90);
		    pdfDoc.setCell("TASA INDICATIVA", "celda04", ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell("", "formas", ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell("Tasa Indicativa con oferta mismo d�a:  " + consulta.getDescTasaIndicativa() + " " +
				   consulta.getIg_tasa_md() + " % anual pagadera " + periodo + "\n" + "Tasa Indicativa con oferta 48 horas:     " +
				   consulta.getDescTasaIndicativa() + " " + consulta.getIg_tasa_spot() + " % anual pagadera " + periodo, "celda01",
				   ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell("Nota:\n\n", "formas", ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell("Tasa con oferta Mismo D�a: El Intermediario Financiero tiene la opci�n de tomarla en firme el mismo d�a que haya recibido la cotizaci�n indicativa a m�s tardar a las 12:30 horas.\n\n",
				   "formas", ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell("Tasa con oferta 48 Horas: El Intermediario Financiero tiene la opci�n de tomarla en firme a m�s tardar dos d�as h�biles despu�s de que la Tesorer�a haya proporcionado la cotizaci�n indicativa antes de las 12:30 horas del segundo d�a h�bil.",
				   "formas", ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell("\nLas cotizaciones indicativas proporcionadas por la Tesorer�a de Nacional Financiera son de uso reservado para el intermediario solicitante y ser� responsabilidad de dicho intermediario el mal uso de la informaci�n proporcionada.",
				   "formas", ComunesPDF.LEFT, 1, 1, 0);
		     pdfDoc.addTable();
		}

		    
	    if ("7".equals(consulta.getIcEstatus()) || "8".equals(consulta.getIcEstatus())  || "9".equals(consulta.getIcEstatus())  ) {
		    pdfDoc.setTable(2, 90);
		    pdfDoc.setCell("TASA CONFIRMADA", "celda04", ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell("FECHA Y HORA DE " + ("8".equals(consulta.getIcEstatus()) ? "CANCELACION" : "CONFIRMACION"), "celda04",
				   ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell("", "formas", ComunesPDF.LEFT, 1, 1, 0);
		    pdfDoc.setCell("", "formas", ComunesPDF.LEFT, 1, 1, 0);
		   
		    if ("S".equals(consulta.getCs_aceptado_md())){			
			pdfDoc.setCell("Tasa Confirmada " + consulta.getDescTasaIndicativa() + " " +    Comunes.formatoDecimal(consulta.getIg_tasa_md(), 2) + " % anual pagadera " + periodo, "celda01", ComunesPDF.LEFT, 1, 1, 1);
		    }  else{
			pdfDoc.setCell("Tasa Confirmada " + consulta.getDescTasaIndicativa() + " " + Comunes.formatoDecimal(consulta.getIg_tasa_spot(), 2) + "% anual pagadera " + periodo, "celda01", ComunesPDF.LEFT, 1, 1, 1);
		    }
		    pdfDoc.setCell(("8".equals(consulta.getIcEstatus()) ?consulta.getDf_cotizacion() + " " + consulta.getHora_cotizacion() : consulta.getDf_aceptacion()), "celda01", ComunesPDF.LEFT, 1, 1, 1);
		    pdfDoc.addTable();
		}
	    } else {
		AutorizaSolCot autSol = ServiceLocator.getInstance().lookup("AutorizaSolCotEJB", AutorizaSolCot.class);
		boolean mismoDiaOk = autSol.validaMismoDia(icSolicitud);
		pdfDoc.setTable(1, 90);
		pdfDoc.setCell("TASA COTIZADA", "celda04", ComunesPDF.LEFT, 1, 1, 0);

		if (mismoDiaOk){
		    pdfDoc.setCell("Tasa con oferta mismo dia" + consulta.getDescTasaIndicativa() + " " + Comunes.formatoDecimal(consulta.getIg_tasa_md(), 2) + " % anual pagadera " + periodo, "celda01", ComunesPDF.LEFT,1, 1, 1);
		}else {
		    pdfDoc.setCell("Tasa con oferta 48 horas" + consulta.getDescTasaIndicativa() + " " +Comunes.formatoDecimal(consulta.getIg_tasa_spot(), 2) + "% anual pagadera " + periodo, "celda01", ComunesPDF.LEFT, 1, 1, 1);
		}
		pdfDoc.addTable();
	    }

	    //pdfDoc.newPage();
	    
	    if (!"".equals(consulta.getCg_observaciones().trim())){
	    	pdfDoc.setTable(3, 90);
		pdfDoc.setCell("Observaciones:", "celda04", ComunesPDF.LEFT, 3, 1, 0);
		pdfDoc.setCell(consulta.getCg_observaciones(), "formas", ComunesPDF.LEFT, 3, 1, 0);
		pdfDoc.addTable();
	    }

	    pdfDoc.endDocument();	   
	

	} catch (Exception t) {
	    throw new AppException("Error al generar PDF  ", t);
	}
	log.debug("generarPDFCotizacionFirme  (S) ");
	return nombreArchivo;
	
    }
    
 
    /**
     * metodo para enviar correo
     * QC-DLHC 2017
     * @param datos     
     * @throws AppException
     */
    public void enviaCorreoCotizador(Map<String,Object> datos) throws AppException{
	log.debug("enviaCorreoCotizador (e)");
	
	Correo correo = new Correo();
	StringBuilder msg  = new StringBuilder();
	StringBuilder mail  = new StringBuilder();
	String  nombreArchivo =  "";
	String recibo ="";
		
	try{
	
	    log.debug("datos == "+datos);
	    
	    String  pantalla =  (String) datos.get("pantalla");
	    String icSolicitud = (String) datos.get("icSolicitud");
	    String remitente = (String) datos.get("remitente");
	    String asunto = (String) datos.get("asunto");	
		
	    if ("CotizaFirme".equals(pantalla)   ) { 
		recibo=  (String)datos.get("recibo");		
	    }
	    
	    
	    Solicitud consulta = new Solicitud();
	    consulta = this.consultaSolicitud(icSolicitud);
	
	    String icmoneda=  consulta.getIc_moneda();
	    String icEjecutivo=  consulta.getIc_ejecutivo();
	    String icIf = consulta.getIc_if();
	    String nombreIf= consulta.getCg_razon_social_if();
	    String folio=  consulta.getNumero_solicitud();
	    String monto=  Double.toString (consulta.getFn_monto());
	    String tipoTasa  = consulta.getTipo_tasa();
	    String plazo  = consulta.getIg_plazo();
	    String esquemaPagos= consulta.getEsquema_recup();
	        
	        
	    	
	    //devuelve los correos de ADMIN TESORER�A/ Tasas de Cr�dito Nafin / Parametrizar Tesorer�a / Opciones del Sistema
	    Map<String,Object> correos = this.devuelveMail(icmoneda); 						
	    String  emailTesoreria =  (String)correos.get("TESORERIA"); 
	    String  emailProEelectronicos =  (String)correos.get("PROELECTRONICOS"); 
	    String  emailOperaciones =  (String)correos.get("OPERACIONES"); 
	    
	    //devuelve correos de ejecutivos ADMIN TESORERIA /Tasas de Cr�dito Nafin / Parametrizar Tesorer�a / Mantenimiento al Ejecutivo 
	    String  emailEjecutivos =  this.emailEjecutivosNafin( icEjecutivo );
	    
	    //devuelve correos de ejecutivos ADMIN TESORERIA/ Tasas de Cr�dito Nafin / Parametrizar Tesorer�a / Asignaci�n Ejecutivo al IF 
	    String  emailAsigna =  this.emailAsignaEjecutivo( icEjecutivo, icIf );
	    	    
	    mail.append(mail.length() > 0 ? "," : "").append(emailTesoreria);
	    mail.append(mail.length() > 0 ? "," : "").append(emailEjecutivos);
	    mail.append(mail.length() > 0 ? "," : "").append(emailAsigna);
	    
	    if ("CotizaFirme".equals(pantalla)   ) {
		mail.append(mail.length() > 0 ? "," : "").append(emailProEelectronicos);
		mail.append(mail.length() > 0 ? "," : "").append(emailOperaciones);
                msg.append("Acaba de recibir informaci&oacute;n importante del Intermediario Financiero: <b>" + nombreIf + "</b> con el n&uacute;mero de Folio: <b>" +folio +"</b> <br>");
                 
            }else   if ("CapSolicitud".equals(pantalla)   ) { 
                
                msg.append("Acaba de recibir una solicitud de cotizaci&oacute;n del Intermediario Financiero: <b>" + nombreIf + "</b> con el n&uacute;mero de Folio: <b>" +folio +"</b> <br>");
            }
	    
	    					     
	    msg.append("<br>   ");   
	    msg.append("<b> Monto: </b>  $ "+ Comunes.formatoDecimal( monto , 2 ) +"  <br>");
	    msg.append("<b> Tasa: </b> "+tipoTasa +"  <br>");
	    msg.append("<b> Plazo: </b> "+plazo +" D&iacute;as <br>");
	    msg.append("<b> Esquema de Pagos: </b> "+esquemaPagos +"  <br>");  
	    
	    if ("CotizaFirme".equals(pantalla)   ) { 
		msg.append("<b> N&uacute;mero de Recibo: </b> "+recibo +"  <br>");  
	    }
		
	    msg.append("<br>   ");  
	    msg.append("<B>Favor de revisar el archivo adjunto de este correo. </B> ");  
	    msg.append("<br>   "); 
	    msg.append("<br>   ");
	    msg.append(" ATENTAMENTE");
	    msg.append("<br>   ");  
	    msg.append(" Nacional Financiera, S.N.C. "); 
	    msg.append("<br>   ");	
	    msg.append("<br>   ");
	    msg.append("Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr&oacute;nicos de entrada. ");
	    msg.append("<br>   ");	
	    msg.append("Por favor no responda este mensaje.");
	    
	    if ("CapSolicitud".equals(pantalla)   ) { 
		nombreArchivo =  this.generarPDFCapCotizacion(datos);
	    }else   if ("CotizaFirme".equals(pantalla)   ) { 
		nombreArchivo =  this.generarPDFCotizacionFirme(datos  );
	    }	    
	    
	    String strDirectorioTemp = (String) datos.get("strDirectorioTemp");	    
	    String  rutaArchivo = strDirectorioTemp+nombreArchivo;
	    
	    ArrayList archivosAdjuntos = new ArrayList();
	    HashMap hmArchivoAdjunto = new HashMap();
	    hmArchivoAdjunto.put("FILE_FULL_PATH", rutaArchivo);
	    archivosAdjuntos.add(hmArchivoAdjunto);
	
	  
	    log.debug("remitente   "+remitente);	   
	    
            String[] listEmail;
            listEmail = mail.toString().split(",");
            for(int i=0; i < listEmail.length; i++){
              String  email =  listEmail[i];
              log.debug("email   "+email);                 
             
                try { //Si el env�o del correo falla se ignora.
                    correo.enviaCorreoConDatosAdjuntos(remitente, email, "", asunto , msg.toString(), null, archivosAdjuntos);
                
                } catch(Exception t) {
                    log.error("Error al enviar correo " + t);
                }
                
	      }
            
	}catch(Exception t){
	    throw new AppException("Error al enviar correo", t);
	}
	log.debug("enviaCorreoCotizador (s)");
    }
    
}	//fin de clase