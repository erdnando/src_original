/*************************************************************************************
*
* Nombre de Clase o de Archivo: AutorizacionCot
*
* Versi�n: 0.1
*
* Fecha Creaci�n: 30/03/2005
*
* Autor: Ricardo Fuentes
*
* Fecha Ult. Modificaci�n:
*
* Descripci�n de Clase:
*
*************************************************************************************/

package com.netro.cotizador;

import java.util.Vector;
import com.netro.exception.*;
import java.sql.*;
import java.io.*;
import java.util.*;
import javax.ejb.Remote;

@Remote
public interface AutorizaSolCot{

	public void guardaObservaciones
			(String ic_solic_esp
			,String cg_observaciones)	
		throws NafinException;

	public void solicEnProceso(String ic_solic_esp) 
		throws NafinException;

	public void operaSolic(String ic_solic_esp[],String no_referencia[],String checked[],String observaciones[],String userLogin,String strNombreUsuario) 
		throws NafinException;
		
	public void setEstatusSolic(String ic_solic_esp,String ic_estatus_nvo,String ic_estatus_ant)
		throws NafinException;

	public ArrayList consultaSolic(String iNoUsuario,String ic_solic_esp[],String ic_estatus[],String estatus,AutSolic param,boolean autorizaEjec) 
		throws NafinException;
	
	public ArrayList consultaSolic(String iNoUsuario,String ic_solic_esp[],String ic_estatus[],String estatus,AutSolic param,boolean autorizaEjec,String strTipoUsuario) 
		throws NafinException;	
	
	public ArrayList consultaTomaEnFirme(String iNoCliente) 
		throws NafinException;
			
	public boolean guardaCambios
			(String ic_solic_esp[]
			,String ic_estatus[]
			,String ig_tasa_md[]
			,String ig_tasa_spot[]
			,String ig_num_referencia[]
			,String cg_causa_rechazo[]
			,String cg_tipo_solic[]
			,String ic_tasa[])			
		throws NafinException;
		
	public ArrayList autorizaEjec
			(String iNoUsuario
			,String ic_solic_esp[]
			,String cs_vobo_ejec[]
			,String cg_causa_rechazo[])	
		throws NafinException;
	public boolean esAntesFinConfirmacion()
		throws NafinException;	

	public void tomaEnFirmeIF(String ic_solic_esp,String num_referencia, boolean mismoDia )
		throws NafinException;
		
	public boolean validaMismoDia(String ic_solic_esp)
		throws NafinException;

	public int validaHoraToma(String ic_solic_esp)
		throws NafinException;
		
	public List consultaAvisoSolici(String cveUsuario, String tipoAviso) 
		throws SQLException;
		
	public HashMap DiasParaSolicRecursos(String ic_solic_esp,String ic_moneda,String ic_if)
				throws NafinException;
				
	public ArrayList consultaTomaEnFirmeEjecutivo(String cve_ejecutivo) 
				throws NafinException;
				
	public ArrayList consultaCotSol(String ic_solic_esp[],String ic_estatus[], String ig_tasa_md[], String ig_tasa_spot[], String cg_causa_rechazo[])
		throws NafinException;
		
	public void mantoCreditosCot(String ic_solic_esp, String ig_tasa_md, String ig_tasa_spot) 
		throws NafinException;

	public void operaSolic(List listaCotizacion, List listaCalculadora, String userLogin, String strNombreUsuario) throws NafinException;
    
    /**
     *@autor DLHC cequality
     * @param icSolicEsp
     * @return
     * @throws NafinException
     */
    public boolean mismoDiaFCotizacion(String icSolicEsp)throws NafinException;
    /**
     *@autor DLHC cequality
     * @param icSolicEsp
     * @param fechaDisposicion
     * @return
     * @throws NafinException
     */
    public int   validaFechaCotiVsDispo(String icSolicEsp, String fechaDisposicion )throws NafinException;
    /**
     *@autor DLHC cequality
     * @param icSolicEsp
     * @return
     * @throws NafinException
     */
    public String  fechaDisposicion(String icSolicEsp)throws NafinException;
    /**
     *@autor DLHC cequality
     * @param icSolicEsp
     * @return
     * @throws NafinException
     */
    public int maximoFDisposicion(String icSolicEsp)throws NafinException;

}