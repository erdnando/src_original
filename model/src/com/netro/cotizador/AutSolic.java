package com.netro.cotizador;

import netropology.utilerias.*;
import java.sql.*;
import java.util.*;

public class AutSolic implements java.io.Serializable{

	//campos de descripcion
	private String	ejecutivo			= "";
	private String	estatus				= "";
	private String	fecha_solicitud		= "";
	private String	fecha_solicitud_a	= "";
	private String	fecha_cotizacion	= "";
	private String	moneda				= "";
	private String	nombre_if			= "";
	private String	num_solicitud		= "";
	private String	plazo_oper			= "";
	private String	solicitar_aut		= "NO";
	private String	tipo_cotizacion		= "";
		
	//campos de tabla
	private String	cg_causa_rechazo	= "";
	private String	cg_observaciones	= "";
	private double	fn_monto			= 0;
	private double	fn_monto_a			= 0;
	private String	ic_estatus			= "";
	private String	ic_if				= "";
	private String	ic_moneda			= "";
	private String	ic_solic_esp		= "";
	private String	ig_num_referencia	= "";
	private double	ig_tasa_md			= 0;
	private double	ig_tasa_spot		= 0;
	private String	cs_vobo_ejec		= "";
	private String	ic_tipo_tasa		= "";
	private String	cg_ut_ppi			= "";
	private String	cg_ut_ppc			= "";
	private String 	ic_esquema_recup	= "";
	
	private String cg_acreditado		= "";
	private String ic_tasa				= "";
		
	//getters
	
	public String getDescTasaIndicativa(){
		String tasaInd = "";
		if("1".equals(this.ic_tipo_tasa)){	//fija MN y USD		
			tasaInd = "";
			if("1".equals(ic_moneda))
				this.ic_tasa = "26";
			else if("54".equals(ic_moneda))
				this.ic_tasa = "04";
		}else if("1".equals(this.ic_moneda)){	// variable y protegida MN
			tasaInd = "TIIE 28 d�as + ";
			this.ic_tasa = "20";
		}else if("54".equals(this.ic_moneda)){	//variable USD
			float plazo = 0;
			if(this.ic_esquema_recup.equals("1"))	//cupon cero, se toma el plazo de la operacion por ser al vencimiento
				plazo = Float.parseFloat(this.plazo_oper);
			else if(this.ic_esquema_recup.equals("4"))		//tipo renta, se toma la periodicidad de pago de capital porque es la misma que la de interes, la cual no se captura
				plazo = Float.parseFloat(this.cg_ut_ppc);
			else
				plazo = Float.parseFloat(this.cg_ut_ppi);	// tradicional y plan de pagos se toma la periodicidad de pago de interes capturada

			if(plazo <= 31f){
				tasaInd = "Libor 1m + ";
				this.ic_tasa = "04";
			}
			else if(plazo <= 93f){
				tasaInd = "Libor 3m + ";				
				this.ic_tasa = "04";				
			}
			else if(plazo <= 183f){
				tasaInd = "Libor 6m + ";				
				this.ic_tasa = "04";				
			}
			else if(plazo <= 365f){
				tasaInd = "Libor 12m + ";				
				this.ic_tasa = "04";				
			}
			else{
				tasaInd = "";	//si excede el plazo no se cotiza o se da tasa fija
				this.ic_tasa = "04";
			}
		}
		return tasaInd;	
	}
	
	public String getIc_tipo_tasa(){
		return this.ic_tipo_tasa;
	}
	public String	getSolicitar_aut(){
		return this.solicitar_aut;
	}
	public String	getEjecutivo(){
		return this.ejecutivo;
	}
	public String	getEstatus(){
		return this.estatus;
	}
	public String	getFecha_cotizacion(){
		return this.fecha_cotizacion;
	}
	public String	getFecha_solicitud(){
		return this.fecha_solicitud;
	}
	public String	getFecha_solicitud_a(){
		return this.fecha_solicitud_a;
	}	
	public String	getMoneda(){
		return this.moneda;
	}
	public String	getNombre_if(){
		return this.nombre_if;
	}
	public String	getNum_solicitud(){
		return this.num_solicitud;
	}
	public String	getPlazo_oper(){
		return this.plazo_oper;
	}
	public String 	getCg_causa_rechazo(){
		return this.cg_causa_rechazo;
	}
	public String	getCg_observaciones(){
		return this.cg_observaciones;
	}
	public String	getCs_vobo_ejec(){
		return this.cs_vobo_ejec;
	}
	public double	getFn_monto(){
		return this.fn_monto;
	}
	public double	getFn_monto_a(){
		return this.fn_monto_a;
	}	
	public String	getIc_estatus(){
		return this.ic_estatus;
	}
	public String	getIc_if(){
		return this.ic_if;
	}
	public String	getIc_moneda(){
		return this.ic_moneda;
	}
	public String	getIc_solic_esp(){
		return this.ic_solic_esp;
	}
	public String	getIg_num_referencia(){
		return this.ig_num_referencia;
	}
	public double	getIg_tasa_md(){
		return this.ig_tasa_md;
	}
	public double	getIg_tasa_spot(){
		return this.ig_tasa_spot;
	}
	public String getTipoCotizacion(){
		return this.tipo_cotizacion;
	}
	public String getCg_ut_ppc(){
		return this.cg_ut_ppc;
	}
	public String getCg_ut_ppi(){
		return this.cg_ut_ppi;
	}			
	public String getIc_esquema_recup(){
		return this.ic_esquema_recup;
	}		
	
	public String getCg_acreditado(){
		return this.cg_acreditado;
	}	
	
	public String getIc_tasa(){
		return this.ic_tasa;
	}		
	
	
	//setters
	public void setIc_esquema_recup(String ic_esquema_recup){
		this.ic_esquema_recup = ic_esquema_recup;
	}	
	public void setCg_ut_ppc(String cg_ut_ppc){
		this.cg_ut_ppc = cg_ut_ppc;
	}
	public void setCg_ut_ppi(String cg_ut_ppi){
		this.cg_ut_ppi = cg_ut_ppi;
	}		
	
	public void setIc_tipo_tasa(String ic_tipo_tasa){
		this.ic_tipo_tasa = ic_tipo_tasa;
	}	
	public void setSolicitar_aut(String solicitar_aut){
		this.solicitar_aut = solicitar_aut;
	}
	public void	setEjecutivo(String ejecutivo){
		this.ejecutivo = ejecutivo;
	}
	public void setEstatus(String estatus){
		this.estatus = estatus;
	}
	public void	setFecha_cotizacion(String fecha_cotizacion){
		this.fecha_cotizacion = fecha_cotizacion;
	}
	public void	setFecha_solicitud(String fecha_solicitud){
		this.fecha_solicitud = fecha_solicitud;
	}
	public void	setFecha_solicitud_a(String fecha_solicitud_a){
		this.fecha_solicitud_a = fecha_solicitud_a;
	}	
	public void	setMoneda(String moneda){
		this.moneda = moneda;
	}
	public void	setNombre_if(String nombre_if){
		this.nombre_if = nombre_if;
	}
	public void	setNum_solicitud(String num_solicitud){
		this.num_solicitud = num_solicitud;
	}
	public void	setPlazo_oper(String plazo_oper){
		this.plazo_oper = plazo_oper;
	}
	public void	setCg_causa_rechazo(String cg_causa_rechazo){
		this.cg_causa_rechazo = cg_causa_rechazo;
	}
	public void setCg_observaciones(String cg_observaciones){
		this.cg_observaciones = cg_observaciones;
	}
	public void	setCs_vobo_ejec(String cs_vobo_ejec){
		this.cs_vobo_ejec = cs_vobo_ejec;
	}	
	public void	setFn_monto(double fn_monto){
		this.fn_monto = fn_monto;
	}
	public void	setFn_monto_a(double fn_monto_a){
		this.fn_monto_a = fn_monto_a;
	}	
	public void	setIc_estatus(String ic_estatus){
		this.ic_estatus = ic_estatus;
	}
	public void	setIc_if(String ic_if){
		this.ic_if = ic_if;
	}	
	public void setIc_moneda(String ic_moneda){
		this.ic_moneda = ic_moneda;
	}
	public void setIc_solic_esp(String ic_solic_esp){
		this.ic_solic_esp = ic_solic_esp;
	}
	public void	setIg_num_referencia(String ig_num_referencia){
		this.ig_num_referencia = ig_num_referencia;
	}
	public void	setIg_tasa_md(double ig_tasa_md){
		this.ig_tasa_md = ig_tasa_md;
	}
	public void	setIg_tasa_spot(double ig_tasa_spot){
		this.ig_tasa_spot = ig_tasa_spot;
	}	
	public void	setTipoCotizacion(String tipo_cotizacion){
		this.tipo_cotizacion = tipo_cotizacion;
	}	
	public void setCg_acreditado(String cg_acreditado){
		this.cg_acreditado = cg_acreditado;
	}		
	public void setIc_tasa(String ic_tasa){
		this.ic_tasa = ic_tasa;
	}			

}