package com.netro.cotizador;

import netropology.utilerias.*;
import java.sql.*;
import java.util.*;
import java.math.*;
import com.netro.exception.*;

public class Curva implements java.io.Serializable{
	public Curva(){
	}
	private List lPlazos = new ArrayList();
	private List lTasas	 = new ArrayList();
	private String metodo = "L";			// metodologia de interpolacion (lineal o alambrado)
	private String metodoCalculo = "P";		// metodologia de calculo (ppv o duracion)
	private String esquema = "";
	
	public void add(int plazo,double tasa){
		lPlazos.add(new Integer(plazo));
		lTasas.add(new Double(tasa));
	}
	public void setMetodo(String valor){
		this.metodo = valor;
	}
	public String getMetodo(){
		return this.metodo;
	}		
	public void setMetodoCalculo(String valor){
		this.metodoCalculo = valor;
	}
	public String getMetodoCalculo(){
		return this.metodoCalculo;
	}
	public void setEsquema(String valor){
		this.esquema = valor;
	}
	public String getEsquema(){
		return this.esquema;
	}

	public double min()
		throws NafinException{
		System.out.println("Curva:min (E)");
		Double	valor = null;
		double	retorno = 0;
		try{
			Set sTasas = new TreeSet(lTasas);
			Iterator 	it = sTasas.iterator();			
			if(it.hasNext())
				valor = (Double)it.next();
			if(valor!=null)
				retorno = valor.doubleValue();
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("Curva:min (S)");
		}
		return retorno;
	}
	
	public double max()
		throws NafinException{
		System.out.println("Curva:min (E)");
		Double	valor = null;
		double	retorno = 0;
	
		try{
			for(int i=0;i<lTasas.size();i++){
				valor = (Double)lTasas.get(i);
				if(valor!=null&&valor.doubleValue()>retorno)
					retorno = valor.doubleValue();
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("Curva:min (S)");
		}
		return retorno;
	}	
	
	public double interpolacion(double ppv,String strPlazo)
		throws NafinException{
		double tasaInterpolada = 0;
		try{
			int i = 0;
			double plazoCred = Double.parseDouble(strPlazo);
			if(lPlazos.size()==0)
				throw new NafinException("COTI0001");
			
			if(plazoCred<=365 && "L".equals(metodo)&&"P".equals(metodoCalculo) && "1".equals(esquema)){
				int nodo	= 0;
				int inmsup	= 0;
				if(ppv <= 35)
					nodo = 29;
				else if(ppv <= 95)
					nodo = 89;
				else if(ppv <= 185)
					nodo = 179;
				else 
					nodo = 359;
				while(inmsup < nodo){
					i++;
					inmsup = ((Integer)lPlazos.get(i)).intValue();
				}
				tasaInterpolada = ((Double)lTasas.get(i)).doubleValue();
			}else{
				for(i=0;i<lPlazos.size();i++){
					//System.out.println("ppv::: "+ppv);
					int plazo = ((Integer)lPlazos.get(i)).intValue();
				//	System.out.println("plazo::: "+plazo);
					int plazoAnt = 0;
					if(i>0)
						plazoAnt = ((Integer)lPlazos.get(i-1)).intValue();
					//System.out.println("plazoAnt::: "+plazoAnt);
					if(plazo==ppv){
						tasaInterpolada = ((Double)lTasas.get(i)).doubleValue();
						return tasaInterpolada;
					}
					if(ppv>plazoAnt&&ppv<plazo){
						int p1 = 0;
						double t1 = 0;
						if(i>0)
							p1 = ((Integer)lPlazos.get(i-1)).intValue();
						int p2 = ((Integer)lPlazos.get(i)).intValue();
						if(i>0)
							t1 = ((Double)lTasas.get(i-1)).doubleValue();
						double t2 = ((Double)lTasas.get(i)).doubleValue();
						
						if("L".equals(metodo)){			//INTERPOLACION LINEAL			
							tasaInterpolada = t1+((t2-t1)/(p2-p1)*(ppv-p1));
						}else if("A".equals(metodo)){	//ALAMBRADO
							double tasaFwd	= (((1+((t2*p2)/360.0))/(1+((t1*p1)/360.0)))-1)*(360.0/(p2-p1*1.0));
							double treFwd	= (Math.pow(1+((tasaFwd*(p2-p1))/360.0),((p2-ppv)/(p2-p1)))-1.0)*(360.0/(p2-ppv));
							tasaInterpolada	= (((1+((t2*p2)/360.0)) / (1+((treFwd*(p2-ppv))/360.0))) - 1)*(360.0/ppv);
						}
						break;
					}
				}	//for
			}
			
			if("A".equals(metodo)){
				if((i==lPlazos.size()-1)||i==0){
					tasaInterpolada = ((Double)lTasas.get(i)).doubleValue();
				}
			}
			if(tasaInterpolada == 0){
				tasaInterpolada = ((Double)lTasas.get(lTasas.size()-1)).doubleValue();
			}

		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			System.out.println("Curva::interpolacion Exception");
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}
		return tasaInterpolada;
	}	
}