package com.netro.mandatodoc;

import java.util.Hashtable;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;

import java.util.List;
import java.util.Vector;
import java.util.HashMap;

import javax.ejb.EJBObject;
import javax.ejb.Remote;

/**
 * RemoteInterface del EJB de Mandato de Documentos.
 * @autor ALberto Cruz Flores.
 * @since FODEA 041 - 2009 Creado el 12 de Agosto de 2009.
 */
@Remote
public interface MandatoDocumentos {

	public List evaluaSolicitudDeMandatoPymeEpo(String clave_epo, String clave_pyme, String clave_if) throws AppException;
	public List evaluaSolicitudDeMandatoPymeIf(String clave_epo, String clave_pyme, String clave_if) throws AppException;
	public List evaluaSolicitudDeMandatoPymeSolic(String clave_epo, String clave_pyme, String clave_if) throws AppException;
	//public List insertSolicMandato(String clave_epo, String clave_pyme, String clave_if, String claveDescontante, String usuarioCaptura) throws AppException;//FODEA 012 - 2010 ACF
	public String insertSolicMandato(String clave_epo, String clave_pyme, String clave_if, String claveDescontante, String tipoCadena, String usuarioCaptura) throws AppException;
	//public List obtenerSolicitudMandatoDoctos(String folio_solicitud) throws AppException;//FODEA 041 - 2009 ACF
	public HashMap obtenerSolicitudMandatoDoctos(String folio_solicitud) throws AppException;//FODEA 033 - 2009 ACF
	public void aceptarSolicitudMandatoDoctos(List informacion_solicitud) throws AppException;//FODEA 041 - 2009 ACF
	public void rechazarSolicitudMandatoDoctos(List informacion_solicitud) throws AppException;//FODEA 041 - 2009 ACF
	public void ampliarMonto(List informacion_solicitud) throws AppException;//FODEA 055 - 2010 By JSHD
	public void aceptadoAMoraSolicitudMandatoDoctos(List informacion_solicitud) throws AppException;//FODEA 041 - 2009 ACF
	public void moraAAceptadoSolicitudMandatoDoctos(List informacion_solicitud) throws AppException;//FODEA 041 - 2009 ACF
	public void cancelarSolicitudMandatoDoctos(List informacion_solicitud) throws AppException;//FODEA 041 - 2009 ACF
	public boolean existeMandatoDocumentos(Vector documentos_seleccionados) throws AppException;//FODEA 041 - 2009 ACF
	public List obtenterInformacionMandato(String clave_documento) throws AppException;//FODEA 041 - 2009 ACF
	public Hashtable getMandanteDetalles(String numNafinElec, String clave_epo) throws AppException;//FODEA 046 - 2009 IA
	public Hashtable getIfDetalles(String numNafinElec) throws AppException;//FODEA 046 - 2009 IA
	public HashMap obtenerInformacionInstruccion(String folioSolicitud) throws AppException;//FODEA 012 - 2010 ACF
	public List getComboDescontantes(String claveIf) throws AppException;//FODEA 012 - 2010 ACF
	public String existeRelacionPymeDescontante(String clavePyme, String claveEpo, String claveDescontante) throws AppException;//FODEA 012 - 2010 ACF
	public List getComboBancoServicio() throws AppException;//FODEA 012 - 2010 ACF
	public String getBancoServicio(String claveBancoServicio) throws AppException;//FODEA 012 - 2010 ACF
	public String esIfConDescontante(String claveIf) throws AppException;//FODEA 012 - 2010 ACF
	public List getComboIfTipoCadena(String tipoCadena) throws AppException;//FODEA 033 - 2010 ACF
	public List getComboIfTipoCadena(String tipoCadena, boolean bandera) throws AppException;//FODEA 033 - 2010 ACF
	public HashMap obtenerEposAfiliadas(String clavePyme, String tipoCadena) throws AppException;//FODEA 012 - 2010 ACF
	public Registros consultarIntruccionesIrrevocables(HashMap parametrosConsulta) throws AppException;//FODEA 033 - 2010 ACF
	public String obtenerTipoCadenaInstruccion(String folioIntruccion) throws AppException;//FODEA 033 - 2010 ACF
	public void activaDesactivaDescAut(String folio_solicitud, String accion) throws AppException;//FODEA 033 - 2010 ACF

}