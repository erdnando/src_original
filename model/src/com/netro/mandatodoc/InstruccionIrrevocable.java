package com.netro.mandatodoc;

import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

public class InstruccionIrrevocable  {

/**
	 * metodo para generar el acuse de la Instruccion Irrevocable
	 * @return
	 * @param parametros
	 */
public String  archivAcusePDF(List parametros  )  {

	AccesoDB con =new AccesoDB();
	CreaArchivo archivo 				= new CreaArchivo();
	StringBuffer contenidoArchivo 	= new StringBuffer();
	String 	nombreArchivo 		= null;
	String query = "";
	ComunesPDF pdfDoc = new ComunesPDF();
	try {
		String  tipo_archivo = parametros.get(0).toString();
		String  login_usuario = parametros.get(1).toString();
		String  nombre_usuario = parametros.get(2).toString();
		String  acuse_carga = parametros.get(3).toString();
		String  numero_folio = parametros.get(4).toString();
		String  fecha_carga = parametros.get(5).toString();
		String  hora_carga = parametros.get(6).toString();
		String  folio_solicitud = parametros.get(7).toString();
		String tipoCadenaInstruccion  = parametros.get(8).toString();
		String  strDirectorioTemp = parametros.get(9).toString();
		String  pais = parametros.get(10).toString();
		String  noCliente = parametros.get(11).toString();
		String  nombre = parametros.get(12).toString();
		String  nombreUsr = parametros.get(13).toString();
		String  logo = parametros.get(14).toString();
		String  strDirectorioPublicacion = parametros.get(15).toString();

		if (tipo_archivo.equals("ACUSE")) {
			StringBuffer texto_legal = new StringBuffer();
			//Instanciaci�n para uso del EJB
			MandatoDocumentos mandatoDocumentos = ServiceLocator.getInstance().lookup("MandatoDocumentosEJB",MandatoDocumentos.class);

			HashMap informacionSolicitud = mandatoDocumentos.obtenerSolicitudMandatoDoctos(folio_solicitud);
			HashMap eposInstruccion = (HashMap)informacionSolicitud.get("eposInstruccion");

			int numeroEpos = Integer.parseInt((String)eposInstruccion.get("numeroEpos"));
			nombreArchivo = archivo.nombreArchivo()+".pdf";
			pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);

			pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
			float anchoCelda1[] = {50f, 50f};
			pdfDoc.setTable(2, 40, anchoCelda1);

			pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
			pdfDoc.addText("La autentificaci�n se llev� a cabo con �xito \n Recibo: "+acuse_carga, "titulo", ComunesPDF.CENTER);
			pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

			pdfDoc.setCell("Cifras de Control", "formas", ComunesPDF.CENTER, 2);
			pdfDoc.setCell("N�mero de Acuse", "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell(numero_folio, "formas",ComunesPDF.LEFT);
			pdfDoc.setCell("Fecha de Carga", "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell(fecha_carga, "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Hora de Carga", "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell(hora_carga, "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Usuario", "formas", ComunesPDF.RIGHT);
			pdfDoc.setCell(login_usuario + " - " + nombre_usuario, "formas",ComunesPDF.LEFT);
			pdfDoc.addTable();

			pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
			if (tipoCadenaInstruccion.equals("1")) {
				texto_legal.append("Aceptaci�n del IF\n\n");
				texto_legal.append("Al aceptar el mensaje de datos que contiene la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE que me otorga el SUJETO DE APOYO, ");
				texto_legal.append("en este acto manifiesto que al SUJETO DE APOYO le otorgu� un CR�DITO para el financiamiento de los CONTRATOS que le fueron adjudicados por la DEPENDENCIA O ENTIDAD ");
				texto_legal.append("que se indica, y me comprometo a cancelar la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE una vez que se encuentre totalmente pagado ");
				texto_legal.append("el cr�dito y sus accesorios, en el entendido de que la INSTRUCCI�N IRREVOCABLE NAFIN quedar� cancelada autom�ticamente.\n\n");

				texto_legal.append("Trat�ndose del DESCONTANTE\n\n");
				texto_legal.append("Al aceptar el mensaje de datos que contiene la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE que me otorga el SUJETO DE APOYO, ");
				texto_legal.append("en este acto manifiesto que el SUJETO DE APOYO hizo de mi conocimiento que se le otorg� un CR�DITO por parte del INTERMEDIARIO FINANCIERO para el financiamiento de los CONTRATOS ");
				texto_legal.append("que le fueron adjudicados por la DEPENDENCIA O ENTIDAD que se indica, y me comprometo a cancelar la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE ");
				texto_legal.append("una vez que reciba del INTERMEDIARIO FINANCIERO la confirmaci�n de que el cr�dito se encuentra totalmente pagado as� como de sus accesorios, en el entendido de ");
				texto_legal.append("que la INSTRUCCI�N IRREVOCABLE NAFIN quedar� cancelada autom�ticamente.");

			} else if (tipoCadenaInstruccion.equals("2")) {

				texto_legal.append("Aceptaci�n del IF\n\n");
				texto_legal.append("Al aceptar el mensaje de datos que contiene la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE que me otorga el SUJETO DE APOYO, ");
				texto_legal.append("en este acto manifiesto que al SUJETO DE APOYO le otorgu� un CR�DITO, y me comprometo a cancelar la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE una vez que se encuentre totalmente pagado ");
				texto_legal.append("el cr�dito y sus accesorios, en el entendido de que la INSTRUCCI�N IRREVOCABLE NAFIN quedar� cancelada autom�ticamente.\n\n");
				texto_legal.append("Trat�ndose del DESCONTANTE\n\n");
				texto_legal.append("Al aceptar el mensaje de datos que contiene la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE que me otorga el SUJETO DE APOYO, ");
				texto_legal.append("en este acto manifiesto que el SUJETO DE APOYO hizo de mi conocimiento que se le otorg� un CR�DITO por parte del INTERMEDIARIO FINANCIERO que se indica, y me comprometo a cancelar la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE ");
				texto_legal.append("una vez que reciba del INTERMEDIARIO FINANCIERO la confirmaci�n de que el cr�dito se encuentra totalmente pagado as� como de sus accesorios, en el entendido de ");
				texto_legal.append("que la INSTRUCCI�N IRREVOCABLE NAFIN quedar� cancelada autom�ticamente.");
			}

			float anchoCelda2[] = {100f};
			pdfDoc.setTable(1, 100, anchoCelda2);
			pdfDoc.setCell(texto_legal.toString(), "formas", ComunesPDF.JUSTIFIED, 1, 1, 0);
			pdfDoc.addTable();
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);

			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);
			pdfDoc.addText("   ","formas",ComunesPDF.CENTER);

			float anchoCelda3[] = {13f, 13f, 16f, 16f, 13f, 13f, 16f};

			pdfDoc.setTable(7, 70, anchoCelda3);

			pdfDoc.setCell("Folio", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("N@E PyME", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("PyME", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("EPO", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("IF que tramita el cr�dito", "celda01", ComunesPDF.CENTER);//FODEA 012 - 2010 ACF
			pdfDoc.setCell("Descontante", "celda01", ComunesPDF.CENTER);//FODEA 012 - 2010 ACF
			pdfDoc.setCell("Fecha de Solicitud", "celda01", ComunesPDF.CENTER);
			pdfDoc.setHeaders();
			pdfDoc.setCell((String)informacionSolicitud.get("folioInstruccion"), "formas", ComunesPDF.CENTER, 1, numeroEpos);
			pdfDoc.setCell((String)informacionSolicitud.get("nafinElectronico"), "formas", ComunesPDF.CENTER, 1, numeroEpos);
			pdfDoc.setCell((String)informacionSolicitud.get("nombrePyme"), "formas", ComunesPDF.CENTER, 1, numeroEpos);
			pdfDoc.setCell((String)eposInstruccion.get("nombreEpo"+0), "formas", ComunesPDF.CENTER, 1);
			pdfDoc.setCell((String)informacionSolicitud.get("nombreIf"), "formas", ComunesPDF.CENTER, 1, numeroEpos);
			pdfDoc.setCell((String)informacionSolicitud.get("nombreDescontante"), "formas", ComunesPDF.CENTER, 1, numeroEpos);
			pdfDoc.setCell((String)informacionSolicitud.get("fechaSolicitud"), "formas", ComunesPDF.CENTER, 1, numeroEpos);

			for (int i = 1; i < numeroEpos; i++) {
				pdfDoc.setCell((String)eposInstruccion.get("nombreEpo"+i), "formas", ComunesPDF.CENTER, 1);
			}
			pdfDoc.addTable();
			float anchoCelda4[] = {13f, 16f, 13f, 16f, 13f, 16f, 13f};

			pdfDoc.setTable(7, 70, anchoCelda4);
			pdfDoc.setCell("Fecha de Aceptaci�n", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Monto del Cr�dito", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Vencimiento del Cr�dito", "celda01", ComunesPDF.CENTER);
		   pdfDoc.setCell("Banco de Servicio", "celda01", ComunesPDF.CENTER);//FODEA 012 - 2010 ACF
			pdfDoc.setCell("Cuenta Bancaria", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Cuenta", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de Cr�dito", "celda01", ComunesPDF.CENTER);
			pdfDoc.setHeaders();

			pdfDoc.setCell((String)informacionSolicitud.get("fechaAutorizacionIf"), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(informacionSolicitud.get("montoCredito"), 2), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell((String)informacionSolicitud.get("fechaVencimiento"), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell((String)informacionSolicitud.get("bancoServicio"), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell((String)informacionSolicitud.get("cuentaBancaria"), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell((String)informacionSolicitud.get("tipoCuenta"), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell((String)informacionSolicitud.get("numeroCredito"), "formas", ComunesPDF.CENTER);
			pdfDoc.addTable();

			float anchoCelda5[] = {25f, 25f, 25f, 25f};//FODEA 015 - 2011 ACF
			pdfDoc.setTable(4, 70, anchoCelda5);//FODEA 015 - 2011 ACF
			pdfDoc.setCell("Moneda de Cr�dito", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda para Descuento", "celda01", ComunesPDF.CENTER);//FODEA 015 - 2011 ACF
			pdfDoc.setCell("Observaciones", "celda01", ComunesPDF.CENTER);
			pdfDoc.setCell("Estatus", "celda01", ComunesPDF.CENTER);
			pdfDoc.setHeaders();
			pdfDoc.setCell((String)informacionSolicitud.get("moneda"), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell((String)informacionSolicitud.get("monedaDescuento"), "formas", ComunesPDF.CENTER);//FODEA 015 - 2011 ACF
			pdfDoc.setCell((String)informacionSolicitud.get("observaciones"), "formas", ComunesPDF.CENTER);
			pdfDoc.setCell((String)informacionSolicitud.get("estatusSolicitud"), "formas", ComunesPDF.CENTER);
			pdfDoc.addTable();
			pdfDoc.endDocument();
		}

	} catch(Exception e) {
		e.printStackTrace();
		//	throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())  con.cierraConexionDB();
	}
	return nombreArchivo;
}


/**
** Metodo para obtener el query de Ver Documentos Operados
* @return
* @param fecha_a
*/

public StringBuffer queryDoctosOperados(String  fecha_a  )  {

	System.out.println("queryDoctosOperados  (E) ");
	AccesoDB con =new AccesoDB();
	StringBuffer operadosQry 	= new StringBuffer();
	try {
		con.conexionDB();

		operadosQry.append(
				"SELECT   /*+use_nl(s ds d p pe e i m tf)*/ " +
				"         p.in_numero_sirac AS num_sirac,  " +
				"         p.cg_razon_social AS nombrepyme,  " +
				"         d.ic_documento AS num_documento,   " +
				"         TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS fecha_emision, " +
				"         TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fecha_vencimiento, " +
				"         d.ic_moneda AS moneda,  " +
				"         m.cd_nombre AS nombre_moneda, " +
				"         tf.cg_nombre AS tipo_factoraje,  " +
				"         d.fn_monto AS monto,  " +
				"         d.fn_porc_anticipo AS porc_descuento, " +
				"         d.fn_monto - d.fn_monto_dscto AS recursogarantia,  " +
				"         ds.in_importe_interes AS monto_interes, " +
				"         d.fn_monto_dscto AS monto_descuento, " +
				"         ds.in_importe_recibir AS monto_a_operar, " +
				"         ds.in_tasa_aceptada AS tasa,  " +
				"         s.ig_plazo AS plazo, " +
				"         pe.cg_pyme_epo_interno AS num_proveedor,    " +
				"         'OperadosMandato::getQrysentencia();' " +
				"    FROM com_solicitud s,  " +
				"         com_docto_seleccionado ds, " +
				"         com_documento d, " +
				"         comcat_pyme p, " +
				"         comrel_pyme_epo pe, " +
				"         comcat_epo e, " +
				"         comcat_if i, " +
				"         comcat_moneda m, " +
				"         comcat_tipo_factoraje tf " +
				"   WHERE  " +
				"     s.ic_documento = ds.ic_documento " +
				"     AND s.ic_documento = d.ic_documento " +
				"     AND d.ic_pyme = p.ic_pyme " +
				"     AND d.ic_epo = pe.ic_epo " +
				"     AND d.ic_pyme = pe.ic_pyme  " +
				"     AND d.ic_epo = e.ic_epo " +
				"     AND ds.ic_if = i.ic_if " +
				"     AND d.ic_moneda = m.ic_moneda " +
				"     AND d.cs_dscto_especial = tf.cc_tipo_factoraje      " +
			//	"     AND ds.ic_epo = d.ic_epo " +
				"     AND pe.cs_habilitado = 'S' " +
				"     AND d.cs_dscto_especial != 'C' " +
				"     AND s.ic_estatus_solic in (3,5,6,10) " +//FODEA 012 - 2010 ACF
				"		AND e.ic_epo = ? " +
				"     AND p.ic_pyme = ? " +
				"     AND i.ic_if = ? ");

			operadosQry.append("AND s.df_operacion >= TO_DATE( ? ,'dd/mm/yyyy') ");
			if(fecha_a.equals("")){
				operadosQry.append( "AND s.df_operacion < TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'), 'dd/mm/yyyy') + 1 " );
			}else{
				operadosQry.append( " AND s.df_operacion <  TO_DATE( ? ,'dd/mm/yyyy') + 1 ");
			}

			operadosQry.append( "ORDER BY p.cg_razon_social ");

	} catch(Exception e) {
		e.printStackTrace();
		//	throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())  con.cierraConexionDB();
		System.out.println("queryDoctosOperados  (S) ");

	}
	return operadosQry;
}


/**
** Metodo paa obtener los rango en la Pantalla de Ver Documentos Operados
* @return
* @param folio_solicitud
*/
public List rangos(String  folio_solicitud  )  {

	System.out.println("rangos  (E) ");

	AccesoDB con =new AccesoDB();
	List rangos = new ArrayList();
	List varBind = new ArrayList();
	PreparedStatement psr = null;
	ResultSet rsr = null;
	try {
		con.conexionDB();

		String qryRangos =
				"SELECT csm.ic_epo AS clave_epo, " +
				"       csm.ic_pyme AS clave_pyme,  " +
				"		  csm.ic_if AS clave_if,  " +
				"       hsm.ic_folio AS folio,   " +
				"       to_char(hsm.df_cambio_estatus,'dd/mm/yyyy') AS fecha_cambio,  " +
				"       hsm.ic_estatus_man_doc AS estatus_mandoc,  " +
				"       emd.cg_descripcion AS descripcion_estatus   " +
				"  FROM comhis_solic_mand_doc hsm,  " +
				"       comcat_estatus_man_doc emd,  " +
				"       com_solic_mand_doc csm     " +
				" WHERE hsm.ic_folio = "+folio_solicitud+" " +
				"   AND hsm.ic_estatus_man_doc = emd.ic_estatus_man_doc  " +
				"   AND hsm.ic_estatus_man_doc in (2,4,5) " +
				"   AND hsm.ic_folio = csm.ic_folio   " +
				"ORDER BY hsm.df_cambio_estatus ";

		System.out.println(":::: qryRangos :: ::::! " +qryRangos );
		psr = con.queryPrecompilado(qryRangos);
		rsr = psr.executeQuery();


		while(rsr.next()) {
			  List registro = new ArrayList();
			  registro.add(rsr.getString("clave_epo")==null?"":rsr.getString("clave_epo"));
			  registro.add(rsr.getString("clave_pyme")==null?"":rsr.getString("clave_pyme"));
			  registro.add(rsr.getString("clave_if")==null?"":rsr.getString("clave_if"));
			  registro.add(rsr.getString("fecha_cambio")==null?"":rsr.getString("fecha_cambio"));
		     registro.add(rsr.getString("descripcion_estatus")==null?"":rsr.getString("descripcion_estatus"));
			  rangos.add(registro);
		}
		rsr.close();
		psr.close();

		System.out.println("..:: rangos : "+rangos);

	} catch(Exception e) {
		e.printStackTrace();
		//	throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())  con.cierraConexionDB();
		System.out.println("rangos  (S) ");

	}
	return rangos;
}

public String  archivVerOperados(List parametros  )  {
	System.out.println("archivVerOperados  (E) ");
	AccesoDB con =new AccesoDB();
	CreaArchivo creaArchivo 	= new CreaArchivo();
	StringBuffer contenidoArchivo	= new StringBuffer();
	String 	nombreArchivo 		= null;
	String query = "";
	ComunesPDF pdfDoc = new ComunesPDF();
	ComunesXLS xlsDoc 	 = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	List varBind = new ArrayList();
	StringBuffer operadosQry 	= new StringBuffer();


	try {
		con.conexionDB();

		String  folio_instruccion =  parametros.get(0).toString();
		String  tipo_archivo = parametros.get(1).toString();
		String  login_usuario = parametros.get(2).toString();
		String  strDirectorioTemp = parametros.get(3).toString();
		String  pais = parametros.get(4).toString();
		String  noCliente = parametros.get(5).toString();
		String  nombre = parametros.get(6).toString();
		String  nombreUsr = parametros.get(7).toString();
		String  logo = parametros.get(8).toString();
		String  strDirectorioPublicacion = parametros.get(9).toString();

		String clave_pyme ="",  clave_epo ="", clave_if="", fecha_de ="", fecha_a ="",  num_sirac ="", nombrepyme ="", num_documento = "",  fecha_emision = "", fecha_venci = "", nombre_moneda = "", tipo_factoraje = "",
				 monto = "", porc_descuento ="", recursogarantia ="", monto_interes ="",  monto_descuento = "", monto_a_operar ="", tasa = "",  plazo = "", num_proveedor = "", fecha_cambio = "";


		if (tipo_archivo.equals("XLS")) {
			nombreArchivo	= creaArchivo.nombreArchivo()+".xls";
			xlsDoc = new ComunesXLS(strDirectorioTemp+nombreArchivo);
			xlsDoc.setTabla(16);
			xlsDoc.setCelda("Mandato de Documentos - Operados del Mandato", "celda01", ComunesXLS.CENTER, 16);
		}

		if (tipo_archivo.equals("PDF")) {
			nombreArchivo = creaArchivo.nombreArchivo()+".pdf";
			pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
			pdfDoc.setEncabezado(pais, noCliente, "", nombre, nombreUsr, logo, strDirectorioPublicacion, "");
			pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
			pdfDoc.addText("Mandato de Documentos - Operados del Mandato ", "celda01", ComunesPDF.CENTER);
			pdfDoc.addText("\n","formas",ComunesPDF.CENTER);
		}

		// Metodo para obtener los rangos
		List rangos =  this.rangos(folio_instruccion);
		for (int j = 0; j < rangos.size(); j++) {
			List estatus = (List)rangos.get(j);
			String tipo_estatus = "";
			clave_epo 	 = (String)estatus.get(0);
			clave_pyme 	 = (String)estatus.get(1);
			clave_if 	 = (String)estatus.get(2);
			tipo_estatus = (String)estatus.get(4);
			fecha_de 	 = (String)((List)rangos.get(j)).get(3);
			if(rangos.size()-1 == j){
				fecha_a = "";
			}else{
				fecha_a  = (String)((List)rangos.get(j + 1)).get(3);
			}

			if (tipo_archivo.equals("XLS")) {
				xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 16);
				xlsDoc.setCelda("Estatus", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda(tipo_estatus, "formas", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Fecha de Cambio", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda(fecha_de, "formas", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("", "formas", ComunesXLS.CENTER, 12);
				xlsDoc.setCelda("No. Cliente Sirac", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Proveedor", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("N�mero de Documento", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Fecha Emisi�n", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Fecha Vencimiento.", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Moneda", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Tipo Factoraje", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Monto", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Porcentaje de Descuento", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Recurso en Garant�a", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Monto Descuento", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Monto Inter�s", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Monto a Operar", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Tasa", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("Plazo", "celda01", ComunesXLS.CENTER, 1);
				xlsDoc.setCelda("No. Proveedor", "celda01", ComunesXLS.CENTER, 1);
			}
			if (tipo_archivo.equals("PDF")) {
				pdfDoc.setTable(16,100);
				pdfDoc.setCell("Estatus", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(tipo_estatus, "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Cambio", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(fecha_de, "formas", ComunesPDF.CENTER);
				pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 12);
				pdfDoc.setCell("No. Cliente Sirac", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Proveedor", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Documento", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Emisi�n", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento.", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Factoraje", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de Descuento", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Recurso en Garant�a", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Descuento", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Inter�s", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Operar", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa",  "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo",  "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("No. Proveedor",  "celda01", ComunesPDF.CENTER);
			}

			operadosQry = this.queryDoctosOperados(fecha_a);
			varBind = new ArrayList();
			varBind.add(clave_epo);
			varBind.add(clave_pyme);
			varBind.add(clave_if);
			varBind.add(fecha_de);
			if(!fecha_a.equals("")){varBind.add(fecha_a);}


			ps = con.queryPrecompilado(operadosQry.toString(), varBind);
			rs = ps.executeQuery();
			System.out.println("operadosQry ********* "+operadosQry);
			System.out.println("varBind ********* "+varBind);
			while (rs.next()) {

				num_sirac = (rs.getString("num_sirac")==null)?"":rs.getString("num_sirac");
				nombrepyme 	= (rs.getString("nombrepyme")==null)?"":rs.getString("nombrepyme");
				num_documento = (rs.getString("num_documento")==null)?"":rs.getString("num_documento");
				fecha_emision = (rs.getString("fecha_emision")==null)?"":rs.getString("fecha_emision");
				fecha_venci = (rs.getString("fecha_vencimiento")==null)?"":rs.getString("fecha_vencimiento");
				nombre_moneda = (rs.getString("nombre_moneda")==null)?"":rs.getString("nombre_moneda");
				tipo_factoraje = (rs.getString("tipo_factoraje")==null)?"":rs.getString("tipo_factoraje");
				monto = (rs.getString("monto")==null)?"":rs.getString("monto");
				porc_descuento = (rs.getString("porc_descuento")==null)?"":rs.getString("porc_descuento");
				recursogarantia = (rs.getString("recursogarantia")==null)?"":rs.getString("recursogarantia");
				monto_interes = (rs.getString("monto_interes")==null)?"":rs.getString("monto_interes");
				monto_descuento = (rs.getString("monto_descuento")==null)?"":rs.getString("monto_descuento");
				monto_a_operar = (rs.getString("monto_a_operar")==null)?"":rs.getString("monto_a_operar");
				tasa = (rs.getString("tasa")==null)?"":rs.getString("tasa");
				plazo = (rs.getString("plazo")==null)?"":rs.getString("plazo");
				num_proveedor = (rs.getString("num_proveedor")==null)?"":rs.getString("num_proveedor");
				if (tipo_archivo.equals("XLS")) {
					xlsDoc.setCelda(num_sirac, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(nombrepyme, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(num_documento, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(fecha_emision, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(fecha_venci, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(nombre_moneda, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(tipo_factoraje, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(monto, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(porc_descuento, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(recursogarantia, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(monto_interes, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(monto_descuento, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(monto_a_operar, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(tasa, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(plazo, "formas", ComunesXLS.CENTER, 1);
					xlsDoc.setCelda(num_proveedor, "formas", ComunesXLS.CENTER, 1);
				}
				if (tipo_archivo.equals("PDF")) {
					pdfDoc.setCell(num_sirac, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(nombrepyme, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(num_documento, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_emision, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_venci, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(nombre_moneda, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(tipo_factoraje, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(monto, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(porc_descuento, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(recursogarantia, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(monto_interes, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(monto_descuento, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(monto_a_operar, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(tasa, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(plazo, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(num_proveedor, "formas", ComunesPDF.CENTER);
				}


			}
			if(rs!=null) rs.close();
			if(ps!=null) ps.close();

			if (tipo_archivo.equals("PDF")) {
			pdfDoc.addTable();
			}
		}//for rangos

		if (tipo_archivo.equals("XLS")) {
			xlsDoc.cierraTabla();
			xlsDoc.cierraXLS();
		}

		if (tipo_archivo.equals("PDF")) {
			pdfDoc.endDocument();
		}

	} catch(Exception e) {
		e.printStackTrace();
		//	throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())  con.cierraConexionDB();
		System.out.println("archivVerOperados  (S) ");

	}
	return nombreArchivo;

}

/**
	 * metodo que se usa en ela pantalla de estadisticas de Instrucci�n Irrevocable
	 * donde se obtiene  los datos de la Pyme
	 * @return
	 * @param txt_nafelec
	 * @param rfc
	 */

public List datosPyme(String  rfc, String txt_nafelec   )  {
	System.out.println("datosPyme  (E) ");
	List datosPyme = new ArrayList();
	ResultSet rspyme = null;
	ResultSet rspy = null;
	ResultSet rspym = null;
	StringBuffer strSQL = new StringBuffer();
	List varBind = new ArrayList();
	AccesoDB con =new AccesoDB();
	String ic_pyme ="", NoNafin ="", NOM_PYME ="", RFC ="";

	try {
		con.conexionDB();

		// Query para obtener el Numero de Pyme
		if (!rfc.equals("")) {
			strSQL.append(" SELECT ic_pyme AS pyme FROM comcat_pyme WHERE cg_rfc = '" + rfc + "'");
			rspy = con.queryDB(strSQL.toString());
			System.out.println("==========>> strSQL: "+strSQL.toString());
			while (rspy.next()) {
				ic_pyme = rspy.getString("pyme")==null?"":rspy.getString("pyme");
			}
			rspy.close();
		}

		//para cuando tiene txt_nafelec
		if (!txt_nafelec.equals("")) {
			strSQL = new StringBuffer();
			strSQL.append(" SELECT ic_epo_pyme_if AS pyme FROM comrel_nafin WHERE ic_nafin_electronico = " + txt_nafelec);
			rspym = con.queryDB(strSQL.toString());
			System.out.println("==========>> strSQL: "+strSQL.toString());
			while (rspym.next()) {
				ic_pyme = rspym.getString("pyme")==null?"":rspym.getString("pyme");
			}
			rspym.close();
		}

		if (!ic_pyme.equals("") ) {
			strSQL = new StringBuffer();
			//Query para obtener el Numero de Nafin, Nombre  y RFC de la Pyme
			strSQL.append(" SELECT p.cg_razon_social AS pyme,");
			strSQL.append(" p.cg_rfc AS rfc_pyme,");
			strSQL.append(" na.ic_nafin_electronico AS nonafin");
			strSQL.append(" FROM comcat_pyme p");
			strSQL.append(", comrel_nafin na");
			strSQL.append(" WHERE na.ic_epo_pyme_if = p.ic_pyme");
			strSQL.append(" AND na.cg_tipo = 'P'");
			strSQL.append(" AND na.ic_epo_pyme_if = " + ic_pyme);
			System.out.println("==========>> strSQL: "+strSQL.toString());
			rspyme = con.queryDB(strSQL.toString());
			while (rspyme.next()) {
				NoNafin = rspyme.getString("nonafin")==null?"":rspyme.getString("nonafin");
				NOM_PYME = rspyme.getString("pyme")==null?"":rspyme.getString("pyme");
				RFC = rspyme.getString("rfc_pyme")==null?"":rspyme.getString("rfc_pyme").trim();

				datosPyme.add(NoNafin);
				datosPyme.add(NOM_PYME);
				datosPyme.add(RFC);

			}
			rspyme.close();
		}

	} catch(Exception e) {
		e.printStackTrace();
		//	throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())  con.cierraConexionDB();
		System.out.println("datosPyme  (S) ");

	}
	return datosPyme;
}



	/**
	 * Este m�todo genera un HashMap con los nombres de los meses por a�o que hay entre el mes y el a�o especificado,
	 * 24 meses hacia atras.
	 * @param mesActual entero con el numero del mes a partir de cual se inicia el conteo.
	 * @param anioActual entero con el numero del a�o en el que inicia el conteo.
	 */
	public HashMap generaEncabezadoTabla(int mesActual, int anioActual) {
		HashMap aniosOperacion = new HashMap();
		HashMap mesesOperacion = new HashMap();
		int contadorAnios = 0;

		mesesOperacion = obtenerNombreMeses(mesActual, 12);

		aniosOperacion.put("anio"+contadorAnios, Integer.toString(anioActual - 2));
		aniosOperacion.put("numeroMeses"+(anioActual - 2), mesesOperacion.get("numeroMeses").toString());
		aniosOperacion.put("meses"+(anioActual - 2), mesesOperacion);
		contadorAnios++;

		mesesOperacion = obtenerNombreMeses(1, 12);

		aniosOperacion.put("anio"+contadorAnios, Integer.toString(anioActual - 1));
		aniosOperacion.put("numeroMeses"+(anioActual - 1), mesesOperacion.get("numeroMeses").toString());
		aniosOperacion.put("meses"+(anioActual - 1), mesesOperacion);
		contadorAnios++;

		mesesOperacion = obtenerNombreMeses(1, mesActual);

		aniosOperacion.put("anio"+contadorAnios, Integer.toString(anioActual));
		aniosOperacion.put("numeroMeses"+anioActual, mesesOperacion.get("numeroMeses").toString());
		aniosOperacion.put("meses"+anioActual, mesesOperacion);
		contadorAnios++;

		aniosOperacion.put("numeroAnios", Integer.toString(contadorAnios));

		return aniosOperacion;
	}


/**
	 * Este m�todo genera un HashMap con los nombres de los meses que hay entre un mes y otro, y el total de estos.
	 * El mes inicial debe ser menor que el final o de lo contrario la lista de meses no se genera.
	 * @param mesInicial entero con el numero del mes a partir de cual se inicia el conteo.
	 * @param mesFinal entero con el numero del mes en el que finaliza el conteo.
	 */
	public HashMap obtenerNombreMeses(int mesInicial, int mesFinal) {
		HashMap mesesOperacion = new HashMap();
		int contadorMeses = 0;

		if (mesInicial > 0 && mesInicial < 13 && mesFinal > 0 && mesFinal < 13 && mesInicial < mesFinal) {
			for (int mes = mesInicial; mes < mesFinal + 1; mes++) {
				if (mes == 1) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Enero");
					mesesOperacion.put("numeroMes"+contadorMeses, "01");
					contadorMeses++;
				}else if (mes == 2) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Febrero");
					mesesOperacion.put("numeroMes"+contadorMeses, "02");
					contadorMeses++;
				}else if (mes == 3) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Marzo");
					mesesOperacion.put("numeroMes"+contadorMeses, "03");
					contadorMeses++;
				}else if (mes == 4) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Abril");
					mesesOperacion.put("numeroMes"+contadorMeses, "04");
					contadorMeses++;
				}else if (mes == 5) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Mayo");
					mesesOperacion.put("numeroMes"+contadorMeses, "05");
					contadorMeses++;
				}else if (mes == 6) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Junio");
					mesesOperacion.put("numeroMes"+contadorMeses, "06");
					contadorMeses++;
				}else if (mes == 7) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Julio");
					mesesOperacion.put("numeroMes"+contadorMeses, "07");
					contadorMeses++;
				}else if (mes == 8) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Agosto");
					mesesOperacion.put("numeroMes"+contadorMeses, "08");
					contadorMeses++;
				}else if (mes == 9) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Septiembre");
					mesesOperacion.put("numeroMes"+contadorMeses, "09");
					contadorMeses++;
				}else if (mes == 10) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Octubre");
					mesesOperacion.put("numeroMes"+contadorMeses, "10");
					contadorMeses++;
				}else if (mes == 11) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Noviembre");
					mesesOperacion.put("numeroMes"+contadorMeses, "11");
					contadorMeses++;
				}else if (mes == 12) {
					mesesOperacion.put("nombreMes"+contadorMeses, "Diciembre");
					mesesOperacion.put("numeroMes"+contadorMeses, "12");
					contadorMeses++;
				}
			}
		}
		mesesOperacion.put("numeroMeses", Integer.toString(contadorMeses));
		return mesesOperacion;
	}



/**
	 *  Metodo para la consulta de las estadisticas por meses
	 * @return
	 * @param ultimoDiaMesActual
	 * @param anioActual
	 * @param mesActual
	 * @param ic_pyme
	 */

public List consEstadistica( String ic_pyme , int mesActual, int anioActual, int ultimoDiaMesActual  )  {
	System.out.println("consEstadistica  (E) ");
	HashMap informacion = new HashMap();
	AccesoDB con =new AccesoDB();
	PreparedStatement pst = null;
	ResultSet rst = null;
	StringBuffer strSQL = new StringBuffer();
	List varBind = new ArrayList();
	HashMap estadisticas = new HashMap();
	HashMap informacionPorEpo = new HashMap();
	String epoAnterior = "";
	int numeroEpos = 0;
	int cont = 0;
	List registros = new ArrayList();

	try {
		con.conexionDB();

		strSQL.append(" SELECT epo.cg_razon_social nombre_epo,");
		strSQL.append(" tab_temp.mes_public mes_publicacion,");
		strSQL.append(" COUNT (tab_temp.ic_documento) doctos_publicados,");
		strSQL.append(" SUM (tab_temp.fn_monto) monto_publicado");
		strSQL.append(" FROM (");
		strSQL.append(" SELECT doc.ic_epo,");
		strSQL.append(" doc.ic_documento,");
		strSQL.append(" doc.fn_monto,");
		strSQL.append(" CASE");
		for (int mes = mesActual; mes < 13; mes++) {
			if (mes < 10) {
				strSQL.append(" WHEN TO_CHAR(doc.df_fecha_docto, 'mm/yyyy') = '0" + mes + "/" + (anioActual - 2) + "' THEN '0" + mes + "/" + (anioActual - 2) + "'");
			} else {
				strSQL.append(" WHEN TO_CHAR(doc.df_fecha_docto, 'mm/yyyy') = '" + mes + "/" + (anioActual - 2) + "' THEN '" + mes + "/" + (anioActual - 2) + "'");
			}
		}
		strSQL.append(" END AS mes_public");
		strSQL.append(" FROM com_documento doc");
		if (mesActual < 10) {
			strSQL.append(" WHERE doc.df_fecha_docto >= TO_DATE('01/0" + mesActual + "/" + (anioActual - 2) + "', 'dd/mm/yyyy')");
		} else {
			strSQL.append(" WHERE doc.df_fecha_docto >= TO_DATE('01/" + mesActual + "/" + (anioActual - 2) + "', 'dd/mm/yyyy')");
		}
		strSQL.append(" AND doc.df_fecha_docto < TO_DATE('31/12/" + (anioActual - 2) + "', 'dd/mm/yyyy') + 1");
		strSQL.append(" AND doc.ic_pyme = ?");
		strSQL.append(" ) tab_temp");
		strSQL.append(", comcat_epo epo");
		strSQL.append(" WHERE epo.ic_epo = tab_temp.ic_epo");
		strSQL.append(" GROUP BY epo.cg_razon_social, tab_temp.mes_public");

		strSQL.append(" UNION");

		strSQL.append(" SELECT epo.cg_razon_social nombre_epo,");
		strSQL.append(" tab_temp.mes_public mes_publicacion,");
		strSQL.append(" COUNT (tab_temp.ic_documento) doctos_publicados,");
		strSQL.append(" SUM (tab_temp.fn_monto) monto_publicado");
		strSQL.append(" FROM (");
		strSQL.append(" SELECT doc.ic_epo,");
		strSQL.append(" doc.ic_documento,");
		strSQL.append(" doc.fn_monto,");
		strSQL.append(" CASE");
		for (int mes = 1; mes < 13; mes++) {
			if (mes < 10) {
				strSQL.append(" WHEN TO_CHAR(doc.df_fecha_docto, 'mm/yyyy') = '0" + mes + "/" + (anioActual - 1) + "' THEN '0" + mes + "/" + (anioActual - 1) + "'");
			} else {
				strSQL.append(" WHEN TO_CHAR(doc.df_fecha_docto, 'mm/yyyy') = '" + mes + "/" + (anioActual - 1) + "' THEN '" + mes + "/" + (anioActual - 1) + "'");
			}
		}
		strSQL.append(" END AS mes_public");
		strSQL.append(" FROM com_documento doc");
		strSQL.append(" WHERE doc.df_fecha_docto >= TO_DATE('01/01/" + (anioActual - 1) + "', 'dd/mm/yyyy')");
		strSQL.append(" AND doc.df_fecha_docto < TO_DATE('31/12/" + (anioActual - 1) + "', 'dd/mm/yyyy') + 1");
		strSQL.append(" AND doc.ic_pyme = ?");
		strSQL.append(" ) tab_temp");
		strSQL.append(", comcat_epo epo");
		strSQL.append(" WHERE epo.ic_epo = tab_temp.ic_epo");
		strSQL.append(" GROUP BY epo.cg_razon_social, tab_temp.mes_public");

		strSQL.append(" UNION");

		strSQL.append(" SELECT epo.cg_razon_social nombre_epo,");
		strSQL.append(" tab_temp.mes_public mes_publicacion,");
		strSQL.append(" COUNT (tab_temp.ic_documento) doctos_publicados,");
		strSQL.append(" SUM (tab_temp.fn_monto) monto_publicado");
		strSQL.append(" FROM (");
		strSQL.append(" SELECT doc.ic_epo,");
		strSQL.append(" doc.ic_documento,");
		strSQL.append(" doc.fn_monto,");
		strSQL.append(" CASE");
		for (int mes = 1; mes < mesActual + 1; mes++) {
			if (mes < 10) {
				strSQL.append(" WHEN TO_CHAR(doc.df_fecha_docto, 'mm/yyyy') = '0" + mes + "/" + anioActual + "' THEN '0" + mes + "/" + anioActual + "'");
			} else {
				strSQL.append(" WHEN TO_CHAR(doc.df_fecha_docto, 'mm/yyyy') = '" + mes + "/" + anioActual + "' THEN '" + mes + "/" + anioActual + "'");
			}
		}
		strSQL.append(" END AS mes_public");
		strSQL.append(" FROM com_documento doc");
		strSQL.append(" WHERE doc.df_fecha_docto >= TO_DATE('01/01/" + anioActual + "', 'dd/mm/yyyy')");
		if (mesActual < 10) {
			strSQL.append(" AND doc.df_fecha_docto < TO_DATE('" + ultimoDiaMesActual + "/0" + mesActual + "/" + anioActual + "', 'dd/mm/yyyy') + 1");
		} else {
			strSQL.append(" AND doc.df_fecha_docto < TO_DATE('" + ultimoDiaMesActual + "/" + mesActual + "/" + anioActual + "', 'dd/mm/yyyy') + 1");
		}
		strSQL.append(" AND doc.ic_pyme = ?");
		strSQL.append(" ) tab_temp");
		strSQL.append(", comcat_epo epo");
		strSQL.append(" WHERE epo.ic_epo = tab_temp.ic_epo");
		strSQL.append(" GROUP BY epo.cg_razon_social, tab_temp.mes_public");
		strSQL.append(" ORDER BY nombre_epo, mes_publicacion");

		varBind.add(ic_pyme);
		varBind.add(ic_pyme);
		varBind.add(ic_pyme);

		System.out.println("==========>> strSQL: "+strSQL.toString());
		System.out.println("==========>> varBind: "+varBind);

		pst = con.queryPrecompilado(strSQL.toString(), varBind);
		rst = pst.executeQuery();
		while (rst.next()) {
			if (!epoAnterior.equals(rst.getString("nombre_epo"))) {
				if (!epoAnterior.equals("")) {
					estadisticas.put("informacionPorEpo"+numeroEpos, informacionPorEpo);
					numeroEpos++;
				}
				informacionPorEpo = new HashMap();
				informacionPorEpo.put("nombreEpo", rst.getString("nombre_epo"));
				epoAnterior = rst.getString("nombre_epo");
			}
			informacionPorEpo.put("doctopub_" + rst.getString("mes_publicacion"), rst.getString("doctos_publicados"));
			informacionPorEpo.put("montopub_" + rst.getString("mes_publicacion"), rst.getString("monto_publicado"));
			cont++;
		}

		if(cont>0){
			estadisticas.put("informacionPorEpo"+numeroEpos, informacionPorEpo);
			numeroEpos++;
		}

		rst.close();
		pst.close();

		registros.add(informacionPorEpo);
		registros.add(estadisticas);
		registros.add(String.valueOf(numeroEpos));


	} catch(Exception e) {
		e.printStackTrace();
			//throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())  con.cierraConexionDB();
		System.out.println("datosPyme  (S) ");

	}
	return registros;
}

}