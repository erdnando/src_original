package com.netro.mandatodoc;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Bean del EJB de Mandato de Documentos.
 * @autor ALberto Cruz Flores.
 * @since FODEA 041 - 2009 Creado el 12 de Agosto de 2009.
 */
@Stateless(name = "MandatoDocumentosEJB" , mappedName = "MandatoDocumentosEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class MandatoDocumentosBean implements MandatoDocumentos {
	public MandatoDocumentosBean(){}
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(MandatoDocumentosBean.class);

 /**
   * M�todo que realiza la validacion para la aceptacion de la solicitud de mandato ( relacion PYME - EPO )
   * @return evaluaSolicPymeEpo
   * @throws NafinException Excepcion lanzada cuando ocurre un error.
   * @since FODEA 041 - 2009 Mandato de Documentos
   * @author Ivan Almaguer
   */
    public List evaluaSolicitudDeMandatoPymeEpo(String clave_epo, String clave_pyme, String clave_if) throws AppException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List evaluaSolicPymeEpo = new ArrayList();

    log.info(" :::: MandatoDocumentosBean :: evaluaSolicitudDeMandatoPymeEpo (E) :::: ");
    try{
      con.conexionDB();
		strSQL.append(
			"SELECT IC_EPO, IC_PYME  " +
			"  FROM COMREL_PYME_EPO  " +
			" WHERE CS_HABILITADO = 'S'  " +
			"   AND IC_EPO = ? " +
			"   AND IC_PYME = ? " );
			log.debug(" strSQL ::: "+strSQL);
			varBind.add(clave_epo);
			varBind.add(clave_pyme);
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			log.debug(" varBind ******* "+varBind);

		  while(rst.next()){
		  List registro = new ArrayList();
        registro.add(rst.getString(1)==null?"":rst.getString(1));
        registro.add(rst.getString(2)==null?"":rst.getString(2));
        evaluaSolicPymeEpo.add(registro);
			}
		log.debug(":::: evalua Relacion PYME - EPO :::: "+evaluaSolicPymeEpo);
      pst.close();
      rst.close();

		if(evaluaSolicPymeEpo!=null && evaluaSolicPymeEpo.size()>0){
			log.debug(":::: Registros encontrados ::::");
		}else{
			log.debug(":::: No existen registros ::::");
			evaluaSolicPymeEpo.clear();
		}
    }catch(Exception e){
      log.info(" :::: MandatoDocumentosBean :: evaluaSolicitudDeMandatoPymeEpo -ERROR- :::: ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
         con.cierraConexionDB();
      }
      log.info(" :::: MandatoDocumentosBean :: evaluaSolicitudDeMandatoPymeEpo (S) :::: ");
    }
    return evaluaSolicPymeEpo;
   }//fin evaluaSolicitudDeMandato PYME - EPO


	 /**
   * M�todo que realiza la validacion para la aceptacion de la solicitud de mandato ( relacion PYME - IF )
   * @return evaluaSolicPymeIf
   * @throws NafinException Excepcion lanzada cuando ocurre un error.
   * @since FODEA 041 - 2009 Mandato de Documentos
   * @author Ivan Almaguer
   */
    public List evaluaSolicitudDeMandatoPymeIf(String clave_epo, String clave_pyme, String clave_if) throws AppException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst2 = null;
    ResultSet rst2 = null;
	 StringBuffer strSQL2 = new StringBuffer();
	 List varBind2 = new ArrayList();
	 List evaluaSolicPymeIf = new ArrayList();

    log.info(" :::: MandatoDocumentosBean :: evaluaSolicitudDeMandatoPymeIf  (E) :::: ");
    try{
      con.conexionDB();
		strSQL2.append(
			"SELECT cpi.ic_epo, ccb.IC_PYME, cpi.IC_IF   " +
			"  FROM COMREL_PYME_IF cpi,  " +
			"       COMREL_CUENTA_BANCARIA ccb  " +
			" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria " +
			"   AND cpi.cs_vobo_if = 'S' " +
			"   AND cpi.cs_borrado = 'N' " +
			"   AND cpi.ic_epo = ? " +
			"   AND ccb.ic_pyme = ?  " +
			"   AND cpi.ic_if = ? " );
			log.debug(" strSQL2 ::: "+strSQL2);
			varBind2.add(clave_epo);
			varBind2.add(clave_pyme);
			varBind2.add(clave_if);
			pst2 = con.queryPrecompilado(strSQL2.toString(), varBind2);
			rst2 = pst2.executeQuery();
			log.debug(" varBind2 ******* "+varBind2);

		  while(rst2.next()){
		  List registro2 = new ArrayList();
        registro2.add(rst2.getString(1)==null?"":rst2.getString(1));
        registro2.add(rst2.getString(2)==null?"":rst2.getString(2));
		  registro2.add(rst2.getString(3)==null?"":rst2.getString(3));
        evaluaSolicPymeIf.add(registro2);
			}
		log.debug(":::: evalua Afiliacion y/o Habilotacion PYME - IF :::: "+evaluaSolicPymeIf);
      pst2.close();
      rst2.close();

		if(evaluaSolicPymeIf!=null && evaluaSolicPymeIf.size()>0){
			log.debug(":::: Registros encontrados ::::");
		}else{
			log.debug(":::: No existen registros ::::");
			evaluaSolicPymeIf.clear();
		}
    }catch(Exception e){
      log.info(" :::: MandatoDocumentosBean :: evaluaSolicitudDeMandatoPymeIf -ERROR- :::: ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
         con.cierraConexionDB();
      }
      log.info(" :::: MandatoDocumentosBean :: evaluaSolicitudDeMandatoPymeIf (S) :::: ");
    }
    return evaluaSolicPymeIf;
   }//fin evaluaSolicitudDeMandato PYME - IF

	 /**
   * M�todo que realiza la validacion para la aceptacion de la solicitud de mandato ( relacion PYME - SOLICITUD )
   * @return evaluaSolicPymeSolic
   * @throws NafinException Excepcion lanzada cuando ocurre un error.
   * @since FODEA 041 - 2009 Mandato de Documentos
   * @author Ivan Almaguer
   */
    public List evaluaSolicitudDeMandatoPymeSolic(String clave_epo, String clave_pyme, String clave_if) throws AppException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst3 = null;
    ResultSet rst3 = null;
	 StringBuffer strSQL3 = new StringBuffer();
	 List varBind3 = new ArrayList();
	 List evaluaSolicPymeSolic = new ArrayList();

    log.info(" :::: MandatoDocumentosBean :: evaluaSolicitudDeMandatoPymeSolic (E) :::: ");
    try{
      con.conexionDB();
		strSQL3.append(
			"SELECT mdoc.ic_epo AS clave_epo, mdoc.ic_pyme AS clave_pyme,  " +
			"       mdoc.ic_if AS clave_if, mdoc.ic_estatus_man_doc AS estatus_solicitud   " +
			"  FROM COM_SOLIC_MAND_DOC mdoc " +
			" WHERE (mdoc.ic_estatus_man_doc = 1 OR mdoc.ic_estatus_man_doc = 2 OR mdoc.ic_estatus_man_doc = 4)  " +
			"   AND mdoc.ic_epo = ? " +
			"   AND mdoc.ic_pyme = ? " );
			//"   AND mdoc.ic_if = ? "
			log.debug(" strSQL3 ::: "+strSQL3);
			varBind3.add(clave_epo);
			varBind3.add(clave_pyme);
			//varBind3.add(clave_if);
			pst3 = con.queryPrecompilado(strSQL3.toString(), varBind3);
			rst3 = pst3.executeQuery();
			log.debug(" varBind3 ******* "+varBind3);

		  while(rst3.next()){
		  List registro3 = new ArrayList();
        registro3.add(rst3.getString(1)==null?"":rst3.getString(1));
        registro3.add(rst3.getString(2)==null?"":rst3.getString(2));
		  registro3.add(rst3.getString(3)==null?"":rst3.getString(3));
		  registro3.add(rst3.getString(4)==null?"":rst3.getString(4));
        evaluaSolicPymeSolic.add(registro3);
			}
		log.debug(":::: evalua relacion Pyme - Solicitud :::: "+evaluaSolicPymeSolic);
      pst3.close();
      rst3.close();

		if(evaluaSolicPymeSolic!=null && evaluaSolicPymeSolic.size()>0){
			log.debug(":::: Registros encontrados ::::");
		}else{
			log.debug(":::: No existen registros ::::");
			evaluaSolicPymeSolic.clear();
		}
    }catch(Exception e){
      log.info(" :::: MandatoDocumentosBean :: evaluaSolicitudDeMandatoPymeSolic  -ERROR- :::: ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
         con.cierraConexionDB();
      }
      log.info(" :::: MandatoDocumentosBean :: evaluaSolicitudDeMandatoPymeSolic  (S) :::: ");
    }
    return evaluaSolicPymeSolic;
   }//fin evaluaSolicitudDeMandato PYME - SOLICITUD



	 /**
   * M�todo que realiza la Insersion de la solicitud de instrucci�n irrevocable
	 * @param clave_epo clave de la o las EPOs con las que se captura la solicitud de instruccion irrevocable
	 * @param clave_pyme clave de la pyme que captura la solicitud de instruccion irrevocable
	 * @param clave_if clave del IF con el que se captura la solicitud de instruccion irrevocable
	 * @param claveDescontante clave del IF descontante con el que se captura la solicitud de instruccion irrevocable
	 * @param tipoCadena tipo de cadena a la que pertenecen la(s) EPO(s), 1: PUBLICAS, 2: PRIVADAS
	 * @param usuarioCaptura usuario que realiza la captura de la solicitud de instrucci�n irrevocable
   * @return insertSolicMandato
   * @throws NafinException Excepcion lanzada cuando ocurre un error.
   */
		public String insertSolicMandato(String clave_epo, String clave_pyme, String clave_if, String claveDescontante, String tipoCadena, String usuarioCaptura) throws AppException{
			log.info("insertSolicMandato(E) ::..");
			AccesoDB con = new AccesoDB();
			PreparedStatement pst = null;
			ResultSet rst = null;
			boolean bcommit = true;
			StringBuffer strSQL = new StringBuffer();
			List varBind = new ArrayList();
			String folioInstruccionIrrevocable = "";

			try {
				con.conexionDB();

				strSQL.append(" SELECT NVL(MAX(ig_folio_instruccion), 0) + 1 AS folio_instruccion FROM com_solic_mand_doc");

				log.debug("..:: strSQL: "+strSQL.toString());

				pst = con.queryPrecompilado(strSQL.toString());
				rst = pst.executeQuery();

				while (rst.next()) {
					folioInstruccionIrrevocable = rst.getString("folio_instruccion")==null?"":rst.getString("folio_instruccion");
				}

				rst.close();
				pst.close();

				if (!folioInstruccionIrrevocable.equals("")) {
					if (tipoCadena.equals("1")) {
						String icFolio = "";
						strSQL = new StringBuffer();

						strSQL.append(" SELECT seq_com_solic_mand_doc.NEXTVAL AS ic_folio FROM dual");

						log.debug("..:: strSQL: "+strSQL.toString());

						pst = con.queryPrecompilado(strSQL.toString());
						rst = pst.executeQuery();

						while (rst.next()) {
							icFolio = rst.getString("ic_folio")==null?"":rst.getString("ic_folio");
						}

						rst.close();
						pst.close();

						strSQL = new StringBuffer();

						strSQL.append(" INSERT INTO com_solic_mand_doc (");
						strSQL.append(" ic_folio,");
						strSQL.append(" ig_folio_instruccion,");
						strSQL.append(" ic_epo,");
						strSQL.append(" ic_pyme,");
						strSQL.append(" ic_if,");
						strSQL.append(" ic_descontante,");
						strSQL.append(" df_solicitud,");
						strSQL.append(" ic_estatus_man_doc,");
						strSQL.append(" df_cambio_estatus,");
						strSQL.append(" cg_usuario_solicitud");
						strSQL.append(" ) VALUES (");
						strSQL.append(" ?,");
						strSQL.append(" ?,");
						strSQL.append(" ?,");
						strSQL.append(" ?,");
						strSQL.append(" ?,");
						strSQL.append(" ?,");
						strSQL.append(" SYSDATE,");
						strSQL.append(" ?,");
						strSQL.append(" SYSDATE,");
						strSQL.append(" ?)");

						varBind.add(new Long(icFolio));
						varBind.add(new Long(folioInstruccionIrrevocable));
						varBind.add(new Integer(clave_epo));
						varBind.add(new Integer(clave_pyme));
						varBind.add(new Integer(clave_if));
						varBind.add(new Integer(claveDescontante));
						varBind.add(new Integer(1));
						varBind.add(usuarioCaptura);

						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);

						pst = con.queryPrecompilado(strSQL.toString(), varBind);
						pst.executeUpdate();
						pst.close();
					} else if (tipoCadena.equals("2")) {
						StringTokenizer clavesEposTokenizer = new StringTokenizer(clave_epo, ",");

						while (clavesEposTokenizer.hasMoreTokens()) {
							String claveEpoToken = clavesEposTokenizer.nextToken();
							String icFolio = "";
							strSQL = new StringBuffer();

							strSQL.append(" SELECT seq_com_solic_mand_doc.NEXTVAL AS ic_folio FROM dual");

							log.debug("..:: strSQL: "+strSQL.toString());

							pst = con.queryPrecompilado(strSQL.toString());
							rst = pst.executeQuery();

							while (rst.next()) {
								icFolio = rst.getString("ic_folio")==null?"":rst.getString("ic_folio");
							}

							rst.close();
							pst.close();

							strSQL = new StringBuffer();
							varBind = new ArrayList();

							strSQL.append(" INSERT INTO com_solic_mand_doc (");
							strSQL.append(" ic_folio,");
							strSQL.append(" ig_folio_instruccion,");
							strSQL.append(" ic_epo,");
							strSQL.append(" ic_pyme,");
							strSQL.append(" ic_if,");
							strSQL.append(" ic_descontante,");
							strSQL.append(" df_solicitud,");
							strSQL.append(" ic_estatus_man_doc,");
							strSQL.append(" df_cambio_estatus,");
							strSQL.append(" cg_usuario_solicitud");
							strSQL.append(" ) VALUES (");
							strSQL.append(" ?,");
							strSQL.append(" ?,");
							strSQL.append(" ?,");
							strSQL.append(" ?,");
							strSQL.append(" ?,");
							strSQL.append(" ?,");
							strSQL.append(" SYSDATE,");
							strSQL.append(" ?,");
							strSQL.append(" SYSDATE,");
							strSQL.append(" ?)");

							varBind.add(new Long(icFolio));
							varBind.add(new Long(folioInstruccionIrrevocable));
							varBind.add(new Integer(claveEpoToken));
							varBind.add(new Integer(clave_pyme));
							varBind.add(new Integer(clave_if));
							varBind.add(new Integer(claveDescontante));
							varBind.add(new Integer(1));
							varBind.add(usuarioCaptura);

							log.debug("..:: strSQL: "+strSQL.toString());
							log.debug("..:: varBind: "+varBind);

							pst = con.queryPrecompilado(strSQL.toString(), varBind);
							pst.executeUpdate();
							pst.close();
						}
					}
				}

				log.debug("..:: folioInstruccionIrrevocable: "+folioInstruccionIrrevocable);
			} catch(Exception e) {
				bcommit = false;
				log.info("insertSolicMandato(ERROR) ::..");
				e.printStackTrace();
				throw new AppException();
			} finally {
				if(con.hayConexionAbierta()){
					con.terminaTransaccion(bcommit);
					con.cierraConexionDB();
				}
				log.info("insertSolicMandato(S) ::..");
			}
			return folioInstruccionIrrevocable;
		}

	/**
	 * M�todo que se encarga de obtener la informaci�n de una solicitud de mandato de documentos.
	 * @param folio_solicitud Cadena que contiene el n�mero de folio de la solicitud capturada por el IF.
	 * @throws AppException Exception lanzada cuando ocurre un error al guardar la informaci�n.
	 */
	public HashMap obtenerSolicitudMandatoDoctos(String folio_solicitud) throws AppException{
		log.info("obtenerSolicitudMandatoDoctos(I)");
		AccesoDB con = new AccesoDB();
		ResultSet rst = null;
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap informacionSolicitud = new HashMap();

		try {
			con.conexionDB();

			strSQL.append(" SELECT DISTINCT /*+ index(smd in_com_solic_mand_doc_01_nuk)+*/ ");
			//strSQL.append(" smd.ic_folio folio,");//FODEA 033 - 2010 ACF
			strSQL.append(" smd.ig_folio_instruccion folio_instruccion,");//FODEA 033 - 2010 ACF
			strSQL.append(" crn.ic_nafin_electronico nafin_electronico,");
			strSQL.append(" pym.cg_razon_social nombre_pyme,");
			//strSQL.append(" epo.cg_razon_social nombre_epo,");//FODEA 033 - 2010 ACF
      strSQL.append(" cif.cg_razon_social nombre_if,");
      strSQL.append(" DECODE (cds.cg_razon_social, null, cif.cg_razon_social, cds.cg_razon_social) nombre_descontante,");
			strSQL.append(" TO_CHAR(smd.df_solicitud, 'dd/mm/yyyy') fecha_solicitud,");
			strSQL.append(" TO_CHAR(smd.df_aceptacion, 'dd/mm/yyyy') fecha_autorizacion_if,");
			strSQL.append(" smd.fn_monto monto_credito,");
			strSQL.append(" TO_CHAR(smd.df_vencimiento, 'dd/mm/yyyy') fecha_vencimiento,");
      strSQL.append(" bsr.cg_razon_social banco_servicio,");
			strSQL.append(" smd.cg_cuenta_bancaria cuenta_bancaria,");
			strSQL.append(" cbm.cg_descripcion tipo_cuenta,");
			strSQL.append(" smd.cg_credito numero_credito,");
			strSQL.append(" mon.cd_nombre moneda,");
			strSQL.append(" DECODE(smd.ic_moneda_dscto, null, 'AMBAS', mnd.cd_nombre) AS moneda_descuento,");//FODEA 015 - 2011 ACF
			strSQL.append(" smd.cg_observaciones observaciones,");
			strSQL.append(" emd.cg_descripcion estatus_solicitud");
			strSQL.append(" FROM com_solic_mand_doc smd");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comrel_nafin crn ");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
      strSQL.append(", comcat_if cds");
      strSQL.append(", comcat_if bsr");
			strSQL.append(", comcat_tipo_cta_man_doc cbm");
			strSQL.append(", comcat_moneda mon");
			strSQL.append(", comcat_moneda mnd");//FODEA 015 - 2011 ACF
			strSQL.append(", comcat_estatus_man_doc emd");
			strSQL.append(" WHERE smd.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND smd.ic_pyme = crn.ic_epo_pyme_if");
			strSQL.append(" AND smd.ic_epo = epo.ic_epo");
			strSQL.append(" AND smd.ic_if = cif.ic_if");
			strSQL.append(" AND smd.cc_tipo_cuenta = cbm.cc_tipo_cuenta(+)");
			strSQL.append(" AND smd.ic_moneda = mon.ic_moneda(+)");
			strSQL.append(" AND smd.ic_moneda_dscto = mnd.ic_moneda(+)");//FODEA 015 - 2011 ACF
      strSQL.append(" AND smd.ic_descontante = cds.ic_if(+)");
      strSQL.append(" AND smd.ic_banco_servicio = bsr.ic_if(+)");
			strSQL.append(" AND smd.ic_estatus_man_doc = emd.ic_estatus_man_doc");
			//strSQL.append(" AND smd.ic_folio = ?");//FODEA 033 - 2010 ACF
			strSQL.append(" AND smd.ig_folio_instruccion = ?");//FODEA 033 - 2010 ACF

			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while(rst.next()){
				informacionSolicitud.put("folioInstruccion", rst.getString("folio_instruccion")==null?"":rst.getString("folio_instruccion"));
				informacionSolicitud.put("nafinElectronico", rst.getString("nafin_electronico")==null?"":rst.getString("nafin_electronico"));
				informacionSolicitud.put("nombrePyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				//informacion_solicitud.put("nombreEpo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));//FODEA 033 - 2010 ACF
        informacionSolicitud.put("nombreIf", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
        informacionSolicitud.put("nombreDescontante", rst.getString("nombre_descontante")==null?"":rst.getString("nombre_descontante"));
				informacionSolicitud.put("fechaSolicitud", rst.getString("fecha_solicitud")==null?"":rst.getString("fecha_solicitud"));
				informacionSolicitud.put("fechaAutorizacionIf", rst.getString("fecha_autorizacion_if")==null?"":rst.getString("fecha_autorizacion_if"));
				informacionSolicitud.put("montoCredito", rst.getString("monto_credito")==null?"":rst.getString("monto_credito"));
				informacionSolicitud.put("fechaVencimiento", rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento"));
        informacionSolicitud.put("bancoServicio", rst.getString("banco_servicio")==null?"":rst.getString("banco_servicio"));
				informacionSolicitud.put("cuentaBancaria", rst.getString("cuenta_bancaria")==null?"":rst.getString("cuenta_bancaria"));
				informacionSolicitud.put("tipoCuenta", rst.getString("tipo_cuenta")==null?"":rst.getString("tipo_cuenta"));
				informacionSolicitud.put("numeroCredito", rst.getString("numero_credito")==null?"":rst.getString("numero_credito"));
				informacionSolicitud.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
				informacionSolicitud.put("monedaDescuento", rst.getString("moneda_descuento")==null?"":rst.getString("moneda_descuento"));//FODEA 015 - 2011 ACF
				informacionSolicitud.put("observaciones", rst.getString("observaciones")==null?"":rst.getString("observaciones"));
				informacionSolicitud.put("estatusSolicitud", rst.getString("estatus_solicitud")==null?"":rst.getString("estatus_solicitud"));
			}

			rst.close();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT /*+ index(smd in_com_solic_mand_doc_01_nuk)+*/ ");
			strSQL.append(" epo.ic_epo clave_epo,");
			strSQL.append(" epo.cg_razon_social nombre_epo");
			strSQL.append(" FROM com_solic_mand_doc smd");
			strSQL.append(", comcat_epo epo");
			strSQL.append(" WHERE smd.ic_epo = epo.ic_epo");
			strSQL.append(" AND smd.ig_folio_instruccion = ?");
			strSQL.append(" ORDER BY epo.cg_razon_social ASC");

			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			HashMap eposInstruccion = new HashMap();
			int numeroEpos = 0;

			while(rst.next()){
				eposInstruccion.put("claveEpo"+numeroEpos, rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				eposInstruccion.put("nombreEpo"+numeroEpos, rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				numeroEpos++;
			}

			rst.close();
			pst.close();

			eposInstruccion.put("numeroEpos", Integer.toString(numeroEpos));
			informacionSolicitud.put("eposInstruccion", eposInstruccion);
		} catch(Exception e) {
			log.info("obtenerSolicitudMandatoDoctos(ERROR)");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
		}
		log.info("obtenerSolicitudMandatoDoctos(F)");
		return informacionSolicitud;
	}

	/**
	 * M�todo que se encarga de guardar con estatus ACEPTADO la informaci�n de una solicitud de mandato de documentos
	 * capturada por un IF.
	 * @param folio_solicitud Cadena que contiene el n�mero de folio de la solicitud capturada por el IF.
	 * @throws AppException Exception lanzada cuando ocurre un error al guardar la informaci�n.
	 */
	public void aceptarSolicitudMandatoDoctos(List informacion_solicitud) throws AppException{
		log.info("aceptarSolicitudMandatoDoctos(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		try {
			con.conexionDB();

			String folio_solicitud = (String)informacion_solicitud.get(0);
			String monto_credito = (String)informacion_solicitud.get(1);
			String vencimiento_credito = (String)informacion_solicitud.get(2);
			String cuenta_bancaria = (String)informacion_solicitud.get(3);
			String tipo_cuenta = (String)informacion_solicitud.get(4);
			String numero_credito = (String)informacion_solicitud.get(5);
			String moneda_credito = (String)informacion_solicitud.get(6);
			String observaciones = (String)informacion_solicitud.get(7);
      String claveBancoServicio = (String)informacion_solicitud.get(8);
			String claveMonedaDescuento = (String)informacion_solicitud.get(9);//FODEA 015 - 2011 ACF

			strSQL.append(" UPDATE com_solic_mand_doc SET");
			strSQL.append(" df_aceptacion = sysdate,");
			strSQL.append(" fn_monto = ?,");
			strSQL.append(" df_vencimiento = TO_DATE(?, 'dd/mm/yyyy'),");
			strSQL.append(" cg_cuenta_bancaria = ?,");
			strSQL.append(" cc_tipo_cuenta = ?,");
			if(!numero_credito.equals("")){strSQL.append(" cg_credito = ?,");}
			strSQL.append(" ic_moneda = ?,");
			if(!claveMonedaDescuento.equals("0")){strSQL.append(" ic_moneda_dscto = ?,");}//FODEA 015 - 2011 ACF
			strSQL.append(" cs_dscto_aut_irrev = ?,");//FODEA 015 - 2011 ACF
			strSQL.append(" df_dscto_aut_irrev = SYSDATE,");//FODEA 015 - 2011 ACF
			if(!observaciones.equals("")){strSQL.append(" cg_observaciones = ?,");}
			strSQL.append(" ic_estatus_man_doc = ?,");
      if(!claveBancoServicio.equals("")){strSQL.append(" ic_banco_servicio = ?,");}
			strSQL.append(" df_cambio_estatus = sysdate");
			//strSQL.append(" WHERE ic_folio = ?");
			strSQL.append(" WHERE ig_folio_instruccion = ?");//FODEA 033 - 2010 ACF

			varBind.add(new Double(monto_credito));
			varBind.add(vencimiento_credito);
			varBind.add(cuenta_bancaria);
			varBind.add(tipo_cuenta);
			if(!numero_credito.equals("")){varBind.add(numero_credito);}
			varBind.add(new Integer(moneda_credito));
			if(!claveMonedaDescuento.equals("0")){varBind.add(new Integer(claveMonedaDescuento));}//FODEA 015 - 2011 ACF
			varBind.add("S");//FODEA 015 - 2011 ACF
			if(!observaciones.equals("")){varBind.add(observaciones);}
			varBind.add(new Integer(2));
      if(!claveBancoServicio.equals("")){varBind.add(new Integer(claveBancoServicio));}
			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" INSERT INTO comhis_solic_mand_doc (");
			strSQL.append(" ic_folio,");
			strSQL.append(" ig_folio_instruccion,");//FODEA 033 - 2010 ACF
			strSQL.append(" ic_estatus_man_doc");
			strSQL.append(" ) VALUES (");
			strSQL.append(" (SELECT NVL(MAX(ic_folio), 0) + 1 FROM comhis_solic_mand_doc), ?, ?");//FODEA 033 - 2010 ACF
			strSQL.append(" )");

			varBind.add(new Long(folio_solicitud));
			varBind.add(new Integer(2));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			/* SE COMENTA CODIGO YA QUE ESTA FUNCIONALIDAD NO TENIA REALMENTE UN USO
			//FODEA 033 - 2010 ACF (I)
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT ic_pyme AS clave_pyme,");
			strSQL.append(" ic_epo AS clave_epo,");
			strSQL.append(" DECODE(ic_if, ic_descontante, ic_if, ic_descontante) AS clave_if");
			strSQL.append(" FROM com_solic_mand_doc");
			strSQL.append(" WHERE ig_folio_instruccion = ?");

			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				String clavePyme = rst.getString("clave_pyme");
				String claveEpo = rst.getString("clave_epo");
				String claveIf = rst.getString("clave_if");
				String csDsctoAutPyme = "N";
				String csDsctoAutPymeIf = "N";
				String csDsctoAutDia = "P";
				PreparedStatement pst1 = null;
				ResultSet rst1 = null;

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cs_dscto_automatico FROM comcat_pyme WHERE ic_pyme = ?");
				varBind.add(new Long(clavePyme));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				rst1 = pst1.executeQuery();

				while (rst1.next()) {
					csDsctoAutPyme = rst1.getString("cs_dscto_automatico");
				}

				rst1.close();
				pst1.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cpi.cs_dscto_automatico,");
				strSQL.append(" cs_dscto_automatico_dia");
				strSQL.append(" FROM comrel_cuenta_bancaria ccb");
				strSQL.append(", comrel_pyme_if cpi");
				strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				strSQL.append(" AND ccb.ic_moneda = ?");
				strSQL.append(" AND ccb.ic_pyme = ?");
				strSQL.append(" AND cpi.ic_epo = ?");
				strSQL.append(" AND cpi.ic_if = ?");
				strSQL.append(" AND cpi.cs_vobo_if = ?");
				strSQL.append(" AND cpi.cs_borrado = ?");

				varBind.add(new Integer(1));
				varBind.add(new Long(clavePyme));
				varBind.add(new Long(claveEpo));
				varBind.add(new Long(claveIf));
				varBind.add("S");
				varBind.add("N");

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				rst1 = pst1.executeQuery();

				while (rst1.next()) {
					csDsctoAutPymeIf = rst1.getString("cs_dscto_automatico");
					csDsctoAutDia = rst1.getString("cs_dscto_automatico_dia");
				}

				rst1.close();
				pst1.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" INSERT INTO comrel_dscto_aut_inst_irrev (");
				strSQL.append(" ig_folio_instruccion,");
				strSQL.append(" ic_pyme,");
				strSQL.append(" ic_epo,");
				strSQL.append(" ic_if,");
				strSQL.append(" cs_dscto_aut_pyme,");
				strSQL.append(" cs_dscto_aut_pyme_if,");
				strSQL.append(" cs_dscto_automatico_dia,");
				strSQL.append(" cs_param_activa");
				strSQL.append(" ) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

				varBind.add(new Long(folio_solicitud));
				varBind.add(new Long(clavePyme));
				varBind.add(new Long(claveEpo));
				varBind.add(new Long(claveIf));
				varBind.add(csDsctoAutPyme);
				varBind.add(csDsctoAutPymeIf);
				varBind.add(csDsctoAutDia);
				varBind.add("S");

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				pst1.executeUpdate();
				pst1.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" UPDATE comcat_pyme SET cs_dscto_automatico = ? WHERE ic_pyme = ?");
				varBind.add("S");
				varBind.add(new Long(clavePyme));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				pst1.executeUpdate();
				pst1.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" UPDATE comrel_pyme_if");
				strSQL.append(" SET cs_dscto_automatico = ?,");
				strSQL.append(" cs_dscto_automatico_dia = ?");
				strSQL.append(" WHERE ic_cuenta_bancaria = (");
				strSQL.append(" SELECT cpi.ic_cuenta_bancaria");
				strSQL.append(" FROM comrel_cuenta_bancaria ccb");
				strSQL.append(", comrel_pyme_if cpi");
				strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				strSQL.append(" AND ccb.ic_moneda = ?");
				strSQL.append(" AND ccb.ic_pyme = ?");
				strSQL.append(" AND cpi.ic_epo = ?");
				strSQL.append(" AND cpi.ic_if = ?");
				strSQL.append(" AND cpi.cs_vobo_if = ?");
				strSQL.append(" AND cpi.cs_borrado = ?");
				strSQL.append(" )");
				strSQL.append(" AND ic_epo = ?");
				strSQL.append(" AND ic_if = ?");
				strSQL.append(" AND cs_vobo_if = ?");
				strSQL.append(" AND cs_borrado = ?");

				varBind.add("S");
				varBind.add("P");
				varBind.add(new Integer(moneda_credito));
				varBind.add(new Long(clavePyme));
				varBind.add(new Long(claveEpo));
				varBind.add(new Long(claveIf));
				varBind.add("S");
				varBind.add("N");
				varBind.add(new Long(claveEpo));
				varBind.add(new Long(claveIf));
				varBind.add("S");
				varBind.add("N");

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				pst1.executeUpdate();
				pst1.close();
			}*/

			//rst.close();
			//pst.close();
			//FODEA 033 - 2010 ACF (F)
		} catch(Exception e) {
			log.info("aceptarSolicitudMandatoDoctos(ERROR)");
			trans_op = false;
			e.printStackTrace();
			throw new AppException();
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("aceptarSolicitudMandatoDoctos(F)");
	}

	/**
	 * M�todo que se encarga de guardar con estatus RECHAZADO la informaci�n de una solicitud de mandato de documentos
	 * capturada por un IF.
	 * @param informacion_solicitud Lista con los valores con los que se actualizara la solicitud.
	 * @throws AppException Exception lanzada cuando ocurre un error al guardar la informaci�n.
	 */
	public void rechazarSolicitudMandatoDoctos(List informacion_solicitud) throws AppException{
		log.info("rechazarSolicitudMandatoDoctos(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		try{
			con.conexionDB();

			String folio_solicitud = (String)informacion_solicitud.get(0);
			String observaciones = (String)informacion_solicitud.get(1);

			strSQL.append(" UPDATE com_solic_mand_doc SET");
			if(!observaciones.equals("")){strSQL.append(" cg_observaciones = ?,");}
			strSQL.append(" df_aceptacion = sysdate,");
			strSQL.append(" ic_estatus_man_doc = ?,");
			strSQL.append(" df_cambio_estatus = sysdate");
			//strSQL.append(" WHERE ic_folio = ?");
			strSQL.append(" WHERE ig_folio_instruccion = ?");//FODEA 033 - 2010 ACF

			if(!observaciones.equals("")){varBind.add(observaciones);}
			varBind.add(new Integer(3));
			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" INSERT INTO comhis_solic_mand_doc (");
			strSQL.append(" ic_folio,");
			strSQL.append(" ig_folio_instruccion,");//FODEA 033 - 2010 ACF
			strSQL.append(" ic_estatus_man_doc");
			strSQL.append(" ) VALUES (");
			strSQL.append(" (SELECT NVL(MAX(ic_folio), 0) + 1 FROM comhis_solic_mand_doc), ?, ?");//FODEA 033 - 2010 ACF
			strSQL.append(" )");

			varBind.add(new Long(folio_solicitud));
			varBind.add(new Integer(3));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
		}catch(Exception e){
			log.info("rechazarSolicitudMandatoDoctos(ERROR)");
			trans_op = false;
			e.printStackTrace();
			throw new AppException();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("rechazarSolicitudMandatoDoctos(F)");
	}

	/**
	 * M�todo que se encarga de ampliar el monto capturado por el IF.
	 *
	 * @param informacion_solicitud Lista con los valores con los que se ampliara el monto
	 * @throws AppException Exception lanzada cuando ocurre un error al guardar la informaci�n.
	 *
	 */
	public void ampliarMonto(List informacion_solicitud)
		throws AppException{

		log.info("ampliarMonto(I)");

		AccesoDB 				con 		= new AccesoDB();
		PreparedStatement 	ps 		= null;
		ResultSet				rs			= null;
		StringBuffer 			strSQL 	= null;
		boolean 					trans_op = true;

		String folioSolicitud 						= null;
		String observaciones 						= null;
		String montoCredito							= null;
		String nuevaFechaVencimiento				= null;

		String observacionesAnteriores			= null;
		String montoCreditoAnterior				= null;
		String nuevaFechaVencimientoAnterior	= null;
		String fechaModificacionMontoAnterior	= null;

		try{

			con.conexionDB();

			folioSolicitud 			= informacion_solicitud != null && informacion_solicitud.size() > 0?(String)informacion_solicitud.get(0):null;
			observaciones 				= informacion_solicitud != null && informacion_solicitud.size() > 1?(String)informacion_solicitud.get(1):null;
			montoCredito 				= informacion_solicitud != null && informacion_solicitud.size() > 2?(String)informacion_solicitud.get(2):null;
			nuevaFechaVencimiento 	= informacion_solicitud != null && informacion_solicitud.size() > 3?(String)informacion_solicitud.get(3):null;

			/*if( folio_solicitud == null || folio_solicitud.trim().equals("")){
				throw new Exception("El Folio de la Solicitud no puede venir vac�o");
			}
			if( observaciones == null   || observaciones.trim().equals("")){
				throw new Exception("Las Observaciones no pueden venir vac�as");
			}
			if( monto_credito == null   || monto_credito.trim().equals("")){
				throw new Exception("El Monto del Cr�dito no puede venir vac�o");
			}
			if( nueva_fecha_vencimiento == null   || nueva_fecha_vencimiento.trim().equals("")){
				throw new Exception("La nueva Fecha de Vencimiento del Cr�dito no puede venir vac�a.");
			}*/
			// Leer parametros anteriores
			strSQL = new StringBuffer();
			strSQL.append(
				"SELECT 																		" +
				"	CG_OBSERVACIONES 	AS OBSERVACIONES_ANTERIORES,				"  +
				"	FN_MONTO 			AS MONTO_CREDITO_ANTERIOR,					"  +
				"  TO_CHAR(DF_NUEVO_VENCIMIENTO,'DD/MM/YYYY HH24:MI:SS') 	"  +
				"							AS NUEVA_FECHA_VENC_ANTERIOR,				"  +
				"	TO_CHAR(DF_MOD_MONTO,'DD/MM/YYYY HH24:MI:SS')				"  +
				"							AS FECHA_MOD_MONTO_ANTERIOR				"  +
				" FROM 																		"  +
				" 	COM_SOLIC_MAND_DOC 													"  +
				" WHERE 																		"  +
				"	IG_FOLIO_INSTRUCCION 	= ?										"
			);
			ps = con.queryPrecompilado(strSQL.toString());
			ps.setLong(1, Long.parseLong(folioSolicitud));
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				observacionesAnteriores 			= rs.getString("OBSERVACIONES_ANTERIORES");
				montoCreditoAnterior 				= rs.getString("MONTO_CREDITO_ANTERIOR");
				nuevaFechaVencimientoAnterior		= rs.getString("NUEVA_FECHA_VENC_ANTERIOR");
				fechaModificacionMontoAnterior	= rs.getString("FECHA_MOD_MONTO_ANTERIOR");
			}
			ps.close();
			rs.close();
			// Insertar los parametros leidos
			strSQL = new StringBuffer();
			strSQL.append(
					"INSERT INTO                		"  +
					"	COMHIS_SOLIC_MAND_AMP 			"  +
					"		(                     		"  +
					"			IC_FOLIO_INSTRUCCION,   "  +
					"			DF_INSERCION,		 		"  +
					"			CG_OBSERVACIONES,			"  +
					"			FN_MONTO,			 		"  +
					"			DF_NUEVO_VENCIMIENTO,   "  +
					"			DF_MOD_MONTO				"  +
					"		)                          "  +
					"   VALUES								"  +
					"		(									"  +
					"			?,								"  +
					"			SYSDATE,						"  +
					"			?,								"  +
					"			?,								"  +
					"			TO_DATE(?,'DD/MM/YYYY HH24:MI:SS'), "  +
					"			TO_DATE(?,'DD/MM/YYYY HH24:MI:SS')  "  +
					"		)									"
			);
			ps = con.queryPrecompilado(strSQL.toString());
			ps.setLong(1, Long.parseLong(folioSolicitud));
			if(observacionesAnteriores != null && !observacionesAnteriores.trim().equals("")){
			ps.setString(2, observacionesAnteriores);
			}else{
				ps.setNull(2, Types.VARCHAR);
			}
			if(montoCreditoAnterior != null && !montoCreditoAnterior.trim().equals("")){
				ps.setBigDecimal(3, new BigDecimal(montoCreditoAnterior));
			}else{
				ps.setNull(3, Types.NUMERIC);
			}
			if(nuevaFechaVencimientoAnterior != null && !nuevaFechaVencimientoAnterior.trim().equals("")){
				ps.setString(4, nuevaFechaVencimientoAnterior);
			}else{
				ps.setNull(4, Types.VARCHAR);
			}
			if(fechaModificacionMontoAnterior != null && !fechaModificacionMontoAnterior.trim().equals("")){
				ps.setString(5, fechaModificacionMontoAnterior);
			}else{
				ps.setNull(5, Types.VARCHAR);
			}
			ps.executeUpdate();
			ps.close();
			// Actualizar campos
			strSQL = new StringBuffer();
			strSQL.append(
					"UPDATE 	              				 "  +
					"	COM_SOLIC_MAND_DOC 				 "  +
					"SET										 "  +
					"	FN_MONTO					= ?,		 "  +
					"	CG_OBSERVACIONES		= ?,		 "  +
					"	DF_NUEVO_VENCIMIENTO	= TO_DATE(?,'DD/MM/YYYY'),		 "  +
					"	DF_MOD_MONTO			= SYSDATE "  +
					"WHERE 									 "  +
					"	IG_FOLIO_INSTRUCCION = ?		 "
			);
			ps = con.queryPrecompilado(strSQL.toString());
			if(montoCredito != null && !montoCredito.trim().equals("")){
				ps.setBigDecimal(1, new BigDecimal(montoCredito));
			}else{
				ps.setNull(1, Types.NUMERIC);
			}
			if(observaciones != null && !observaciones.trim().equals("")){
				ps.setString(2, observaciones);
			}else{
				ps.setNull(2, Types.VARCHAR);
			}
			if(observaciones != null && !observaciones.trim().equals("")){
				ps.setString(3, nuevaFechaVencimiento);
			}else{
				ps.setNull(3, Types.VARCHAR);
			}
			ps.setLong(4, Long.parseLong(folioSolicitud));
			ps.executeUpdate();
			ps.close();

		}catch(Exception e){

			log.info("ampliarMonto(Exception)");
			log.info("ampliarMonto.informacion_solicitud 			= <"+informacion_solicitud+">");
			log.info("ampliarMonto.informacion_solicitud.size		= <"+(informacion_solicitud != null?String.valueOf(informacion_solicitud.size()):"NO DISPONIBLE")+">");
			log.info("ampliarMonto.folioSolicitud 						= <"+folioSolicitud+">");
			log.info("ampliarMonto.observaciones 						= <"+observaciones+">");
			log.info("ampliarMonto.montoCredito 						= <"+montoCredito+">");
			log.info("ampliarMonto.nuevaFechaVencimiento 			= <"+nuevaFechaVencimiento+">");
			trans_op = false;
			e.printStackTrace();
			throw new AppException();

		}finally{
			if(ps != null) { try { ps.close();}catch(Exception e){} }
			if(rs != null) { try { rs.close();}catch(Exception e){} }
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}

		}
		log.info("ampliarMonto(F)");
	}

	/**
	 * M�todo que se encarga de guardar con estatus EN MORA la informaci�n de una solicitud de mandato de documentos
	 * capturada por un IF.
	 * @param informacion_solicitud Lista con los valores con los que se actualizara la solicitud.
	 * @throws AppException Exception lanzada cuando ocurre un error al guardar la informaci�n.
	 */
	public void aceptadoAMoraSolicitudMandatoDoctos(List informacion_solicitud) throws AppException{
		log.info("aceptadoAMoraSolicitudMandatoDoctos(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		try{
			con.conexionDB();

			String folio_solicitud = (String)informacion_solicitud.get(0);
			String observaciones = (String)informacion_solicitud.get(1);

			strSQL.append(" UPDATE com_solic_mand_doc SET");
			if(!observaciones.equals("")){strSQL.append(" cg_observaciones = ?,");}
			strSQL.append(" ic_estatus_man_doc = ?,");
			strSQL.append(" df_cambio_estatus = sysdate");
			//strSQL.append(" WHERE ic_folio = ?");
			strSQL.append(" WHERE ig_folio_instruccion = ?");//FODEA 033 - 2010 ACF

			if(!observaciones.equals("")){varBind.add(observaciones);}
			varBind.add(new Integer(4));
			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" INSERT INTO comhis_solic_mand_doc (");
			strSQL.append(" ic_folio,");
			strSQL.append(" ig_folio_instruccion,");//FODEA 033 - 2010 ACF
			strSQL.append(" ic_estatus_man_doc");
			strSQL.append(" ) VALUES (");
			strSQL.append(" (SELECT NVL(MAX(ic_folio), 0) + 1 FROM comhis_solic_mand_doc), ?, ?");//FODEA 033 - 2010 ACF
			strSQL.append(" )");

			varBind.add(new Long(folio_solicitud));
			varBind.add(new Integer(4));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
		}catch(Exception e){
			log.info("aceptadoAMoraSolicitudMandatoDoctos(ERROR)");
			trans_op = false;
			e.printStackTrace();
			throw new AppException();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("aceptadoAMoraSolicitudMandatoDoctos(F)");
	}

	/**
	 * M�todo que se encarga de guardar con estatus ACEPTADO la informaci�n de una solicitud de mandato de documentos
	 * capturada por un IF que se encuentra en estatus EN MORA.
	 * @param informacion_solicitud Lista con los valores con los que se actualizara la solicitud.
	 * @throws AppException Exception lanzada cuando ocurre un error al guardar la informaci�n.
	 */
	public void moraAAceptadoSolicitudMandatoDoctos(List informacion_solicitud) throws AppException{
		log.info("moraAAceptadoSolicitudMandatoDoctos(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		try{
			con.conexionDB();

			String folio_solicitud = (String)informacion_solicitud.get(0);
			String observaciones = (String)informacion_solicitud.get(1);

			strSQL.append(" UPDATE com_solic_mand_doc SET");
			if(!observaciones.equals("")){strSQL.append(" cg_observaciones = ?,");}
			strSQL.append(" ic_estatus_man_doc = ?,");
			strSQL.append(" df_cambio_estatus = sysdate");
			//strSQL.append(" WHERE ic_folio = ?");
			strSQL.append(" WHERE ig_folio_instruccion = ?");//FODEA 033 - 2010 ACF

			if(!observaciones.equals("")){varBind.add(observaciones);}
			varBind.add(new Integer(2));
			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" INSERT INTO comhis_solic_mand_doc (");
			strSQL.append(" ic_folio,");
			strSQL.append(" ig_folio_instruccion,");//FODEA 033 - 2010 ACF
			strSQL.append(" ic_estatus_man_doc");
			strSQL.append(" ) VALUES (");
			strSQL.append(" (SELECT NVL(MAX(ic_folio), 0) + 1 FROM comhis_solic_mand_doc), ?, ?");//FODEA 033 - 2010 ACF
			strSQL.append(" )");

			varBind.add(new Long(folio_solicitud));
			varBind.add(new Integer(2));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();
		}catch(Exception e){
			log.info("moraAAceptadoSolicitudMandatoDoctos(ERROR)");
			trans_op = false;
			e.printStackTrace();
			throw new AppException();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("moraAAceptadoSolicitudMandatoDoctos(F)");
	}

	/**
	 * M�todo que se encarga de guardar con estatus CANCELADO la informaci�n de una solicitud de mandato de documentos
	 * capturada por un IF que se encuentra en estatus EN MORA o ACEPTADO.
	 * @param informacion_solicitud Lista con los valores con los que se actualizara la solicitud.
	 * @throws AppException Exception lanzada cuando ocurre un error al guardar la informaci�n.
	 */
	public void cancelarSolicitudMandatoDoctos(List informacion_solicitud) throws AppException{
		log.info("cancelarSolicitudMandatoDoctos(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean trans_op = true;
		try{
			con.conexionDB();

			String folio_solicitud = (String)informacion_solicitud.get(0);
			String observaciones = (String)informacion_solicitud.get(1);

			strSQL.append(" UPDATE com_solic_mand_doc SET");
			if(!observaciones.equals("")){strSQL.append(" cg_observaciones = ?,");}
			strSQL.append(" ic_estatus_man_doc = ?,");
			strSQL.append(" df_cambio_estatus = sysdate");
			//strSQL.append(" WHERE ic_folio = ?");
			strSQL.append(" WHERE ig_folio_instruccion = ?");

			if(!observaciones.equals("")){varBind.add(observaciones);}
			varBind.add(new Integer(5));
			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" INSERT INTO comhis_solic_mand_doc (");
			strSQL.append(" ic_folio,");
			strSQL.append(" ig_folio_instruccion,");//FODEA 033 - 2010 ACF
			strSQL.append(" ic_estatus_man_doc");
			strSQL.append(" ) VALUES (");
			strSQL.append(" (SELECT NVL(MAX(ic_folio), 0) + 1 FROM comhis_solic_mand_doc), ?, ?");//FODEA 033 - 2010 ACF
			strSQL.append(" )");

			varBind.add(new Long(folio_solicitud));
			varBind.add(new Integer(5));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

			/* SE COMENTA CODIGO YA QUE SU FUNCIONALIDAD NO TENIA UN USO - F054-2010 FVR
			//FODEA 033 - 2010 ACF (I)
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT ic_pyme AS clave_pyme,");
			strSQL.append(" ic_epo AS clave_epo,");
			strSQL.append(" DECODE(ic_if, ic_descontante, ic_if, ic_descontante) AS clave_if");
			strSQL.append(" FROM com_solic_mand_doc");
			strSQL.append(" WHERE ig_folio_instruccion = ?");

			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				String clavePyme = rst.getString("clave_pyme");
				String claveEpo = rst.getString("clave_epo");
				String claveIf = rst.getString("clave_if");
				String csDsctoAutPyme = "N";
				String csDsctoAutPymeIf = "N";
				String csDsctoAutDia = "P";
				PreparedStatement pst1 = null;
				ResultSet rst1 = null;

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cs_dscto_aut_pyme,");
				strSQL.append(" cs_dscto_aut_pyme_if,");
				strSQL.append(" cs_dscto_automatico_dia");
				strSQL.append(" FROM comrel_dscto_aut_inst_irrev");
				strSQL.append(" WHERE ig_folio_instruccion = ?");
				strSQL.append(" AND ic_pyme = ?");
				strSQL.append(" AND ic_epo = ?");
				strSQL.append(" AND ic_if = ?");
				strSQL.append(" AND cs_param_activa = ?");

				varBind.add(new Long(folio_solicitud));
				varBind.add(new Long(clavePyme));
				varBind.add(new Long(claveEpo));
				varBind.add(new Long(claveIf));
				varBind.add("S");

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				rst1 = pst1.executeQuery();

				while (rst1.next()) {
					csDsctoAutPyme = rst1.getString("cs_dscto_aut_pyme");
					csDsctoAutPymeIf = rst1.getString("cs_dscto_aut_pyme_if");
					csDsctoAutDia = rst1.getString("cs_dscto_automatico_dia");
				}

				rst1.close();
				pst1.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" UPDATE comcat_pyme SET cs_dscto_automatico = ? WHERE ic_pyme = ?");
				varBind.add(csDsctoAutPyme);
				varBind.add(new Long(clavePyme));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				pst1.executeUpdate();
				pst1.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" UPDATE comrel_pyme_if");
				strSQL.append(" SET cs_dscto_automatico = ?,");
				strSQL.append(" cs_dscto_automatico_dia = ?");
				strSQL.append(" WHERE ic_cuenta_bancaria = (");
				strSQL.append(" SELECT cpi.ic_cuenta_bancaria");
				strSQL.append(" FROM comrel_cuenta_bancaria ccb");
				strSQL.append(", comrel_pyme_if cpi");
				strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
				strSQL.append(" AND ccb.ic_moneda = ?");
				strSQL.append(" AND ccb.ic_pyme = ?");
				strSQL.append(" AND cpi.ic_epo = ?");
				strSQL.append(" AND cpi.ic_if = ?");
				strSQL.append(" AND cpi.cs_vobo_if = ?");
				strSQL.append(" AND cpi.cs_borrado = ?");
				strSQL.append(" )");
				strSQL.append(" AND ic_epo = ?");
				strSQL.append(" AND ic_if = ?");
				strSQL.append(" AND cs_vobo_if = ?");
				strSQL.append(" AND cs_borrado = ?");

				varBind.add(csDsctoAutPymeIf);
				varBind.add(csDsctoAutDia);
				varBind.add(new Integer(1));
				varBind.add(new Long(clavePyme));
				varBind.add(new Long(claveEpo));
				varBind.add(new Long(claveIf));
				varBind.add("S");
				varBind.add("N");
				varBind.add(new Long(claveEpo));
				varBind.add(new Long(claveIf));
				varBind.add("S");
				varBind.add("N");

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				pst1.executeUpdate();
				pst1.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" UPDATE comrel_dscto_aut_inst_irrev");
				strSQL.append(" SET cs_param_activa = ?");
				strSQL.append(" WHERE ig_folio_instruccion = ?");
				strSQL.append(" AND ic_pyme = ?");
				strSQL.append(" AND ic_epo = ?");
				strSQL.append(" AND ic_if = ?");

				varBind.add("N");
				varBind.add(new Long(folio_solicitud));
				varBind.add(new Long(clavePyme));
				varBind.add(new Long(claveEpo));
				varBind.add(new Long(claveIf));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				pst1.executeUpdate();
				pst1.close();
			}*/

			//rst.close();
			//pst.close();
			//FODEA 033 - 2010 ACF (F)
		}catch(Exception e){
			log.info("cancelarSolicitudMandatoDoctos(ERROR)");
			trans_op = false;
			e.printStackTrace();
			throw new AppException();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("cancelarSolicitudMandatoDoctos(F)");
	}

	/**
	 * Este m�todo obtiene una bandera que indica si el IF tiene documentos pendientes por
	 * autorizar con pymes que tienen solicitude de mandato de documentos.
	 * @param documentos_seleccionados Vector con los documentos seleccionados por la pyme con las que se busca si existe un mandato de documentos.
	 * @return existe_mandato Bandera con valor true si hay mandato de documentos, false en caso contrario.
	 * @throws AppException Cuando ocurre alg�n error en la consulta.
	 */
	 public boolean existeMandatoDocumentos(Vector documentos_seleccionados) throws AppException{
		log.info("existeMandatoDocumentos(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean existe_mandato = false;
		try{
			con.conexionDB();

			if(documentos_seleccionados.size() > 0){
				String condiciones = "";
				int numero_mandatos = 0;

				strSQL.append(" SELECT COUNT(DISTINCT smd.ic_folio) numero_mandatos");
				strSQL.append(" FROM com_documento doc");
				strSQL.append(", com_solic_mand_doc smd");
				strSQL.append(" WHERE doc.ic_epo = smd.ic_epo");
				strSQL.append(" AND doc.ic_if = smd.ic_if");
				strSQL.append(" AND doc.ic_pyme = smd.ic_pyme");
				strSQL.append(" AND smd.ic_estatus_man_doc IN (?, ?)");
				strSQL.append(" AND doc.ic_documento IN (");

				varBind.add(new Integer(2));
				varBind.add(new Integer(4));

				for (int i = 0; i < documentos_seleccionados.size(); i++) {
					Vector documento_seleccionado = (Vector) documentos_seleccionados.get(i);
					condiciones += "?,";
					varBind.add(new Long(documento_seleccionado.get(0).toString().trim()));
				}

				condiciones = condiciones.substring(0, condiciones.lastIndexOf(","));

				strSQL.append(condiciones);
				strSQL.append(")");

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				rst = pst.executeQuery();

				if(rst.next()){numero_mandatos = rst.getInt("numero_mandatos");}

				rst.close();
				pst.close();

				if(numero_mandatos > 0){existe_mandato = true;}
			}
		}catch(Exception e){
			log.info("existeMandatoDocumentos(ERROR)");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
		}
		log.info("existeMandatoDocumentos(F)");
		return existe_mandato;
	 }

	/**
	 * Este m�todo obtiene el estatus y el numero de cuenta parametrizados por el IF de
	 * una solicitud de mandato de documentos
	 * @param clave_documento Cadena con el numero de documento del que se desea obtener el estatus del mandato y la cuenta bancaria.
	 * @return informacion_mandato Lista con los siguientes valores:
	 * 			1)estatus_mandato Estatus en el que se encuentra el mandato de asociado al documento.
	 *          2)cuenta_bancaria Numero de cuenta bancaria parametrizada por el IF para el mandato.
	 * @throws AppException Cuando ocurre alg�n error en la consulta.
	 */
	 public List obtenterInformacionMandato(String clave_documento) throws AppException{
		log.info("obtenterInformacionMandato(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List informacion_mandato = new ArrayList();
		try{
			con.conexionDB();

			strSQL.append(" SELECT smd.ic_estatus_man_doc clave_estatus,");
			strSQL.append(" emd.cg_descripcion estatus_mandato,");
			strSQL.append(" smd.cg_cuenta_bancaria cuenta_bancaria,");
			strSQL.append(" cbm.cg_descripcion tipo_cuenta");
			strSQL.append(" FROM com_documento doc");
			strSQL.append(", com_solic_mand_doc smd");
			strSQL.append(", comcat_tipo_cta_man_doc cbm");
			strSQL.append(", comcat_estatus_man_doc emd");
			strSQL.append(" WHERE doc.ic_epo = smd.ic_epo");
			strSQL.append(" AND doc.ic_if = smd.ic_if");
			strSQL.append(" AND doc.ic_pyme = smd.ic_pyme");
			strSQL.append(" AND smd.cc_tipo_cuenta = cbm.cc_tipo_cuenta");
			strSQL.append(" AND smd.ic_estatus_man_doc = emd.ic_estatus_man_doc");
			strSQL.append(" AND smd.ic_estatus_man_doc IN (?, ?)");
			strSQL.append(" AND doc.ic_documento = ?");

			varBind.add(new Integer(2));
			varBind.add(new Integer(4));
			varBind.add(new Long(clave_documento));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			if(rst.next()){
				informacion_mandato.add(rst.getString("clave_estatus")==null?"":rst.getString("clave_estatus"));
				informacion_mandato.add(rst.getString("estatus_mandato")==null?"":rst.getString("estatus_mandato"));
				informacion_mandato.add(rst.getString("cuenta_bancaria")==null?"":rst.getString("cuenta_bancaria"));
				informacion_mandato.add(rst.getString("tipo_cuenta")==null?"":rst.getString("tipo_cuenta"));
			}

			rst.close();
			pst.close();
		}catch(Exception e){
			log.info("obtenterInformacionMandato(ERROR)");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
		}
		log.info("obtenterInformacionMandato(F)");
		return informacion_mandato;
	 }

   //------------- FODEA 046-2009 ---------------> (I)
	/**
	 * M�todo que realiza la consulta para p�gina Consulta de Mandantes "Detalle" (EPO)
	 * @autor  Ivan Almaguer
	 * @param  numNafinElec	Numero de Nafin electronico de la EPO
	 * @return consultaDetalles
	 * @since  FODEA 046 - 2009 Tasas Y WS Mandato
	 * @throws AppException lanzada cuando ocurre un error.
	 */
    public Hashtable getMandanteDetalles(String numNafinElec, String clave_epo) throws AppException {
    log.info("getMandanteDetalles(E)");
	 StringBuffer sQuery = new StringBuffer();
	 List varBind = new ArrayList();
	 AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
	 Hashtable consultaDetalles = new Hashtable();

    try {
		 con.conexionDB();

        sQuery.append(  " SELECT /*+use_nl(epo cont) */ DISTINCT  " +
								"       cm.cg_razon_social AS razonsocial, cn.ic_nafin_electronico AS numnafinelect,        " +
								"       cm.cg_nombre_comercial AS nombre_comercial, cm.cg_rfc AS rfc,  " +
								"       cm.cg_calle AS calle, cm.cg_colonia AS colonia, " +
								"       cest.cd_nombre AS estado, cm.cg_colonia AS municipio, " +
								"       cm.cg_codigopostal AS codigo_postal, cpa.cd_descripcion AS pais, " +
								"       cm.cg_telefono AS telefono, cm.cg_fax AS fax, " +
								"       cont.cg_appat AS ap_paterno, cont.cg_apmat AS ap_materno,  " +
								"       cont.cg_nombre AS nombre_contacto, cont.cg_tel AS tel_contacto,  " +
								"       cont.cg_fax AS fax_contacto, cont.cg_email AS email_contacto,  " +
								"       cont.cg_celular AS cel_contacto  " +
								"  FROM comrel_nafin cn, " +
								"       comcat_mandante cm, " +
								"       com_domicilio cdom, " +
								"       comcat_estado cest, " +
								"       comcat_pais cpa, " +
								"       com_contacto cont, " +
								"       comcat_epo epo, " +
								"       comrel_mandante_epo cme " +
								" WHERE cm.ic_mandante = cn.ic_epo_pyme_if " +
								"   AND cm.ic_estado = cest.ic_estado " +
								"   AND cme.ic_epo = epo.ic_epo " +
								"   AND cm.ic_mandante = cme.ic_mandante " +
								"   AND epo.ic_epo = cdom.ic_epo " +
								"   AND epo.ic_epo = cont.ic_epo " +
								"   AND cont.cs_primer_contacto = 'S' " +
								"   AND cdom.ic_pais = cpa.ic_pais " +
								"   AND cn.cg_tipo = 'M' " +
								"   AND epo.ic_epo = ? " );
							   varBind.add(clave_epo);


						if(numNafinElec!=null&&!"".equals(numNafinElec)) {
						 sQuery.append(" AND cn.ic_nafin_electronico = ? ");
						 varBind.add(numNafinElec);
						}
						sQuery.append(" ORDER BY cm.cg_razon_social ");

				   ps = con.queryPrecompilado(sQuery.toString(), varBind);
					log.debug("getMandanteDetalles - sQuery: "+ sQuery);
					log.debug("varBind: "+varBind);
					rs = ps.executeQuery();

					if(rs.next()) {
					consultaDetalles.put("razonsocial", rs.getString("razonsocial")==null?"":rs.getString("razonsocial"));
					consultaDetalles.put("numnafinelect", rs.getString("numnafinelect")==null?"":rs.getString("numnafinelect"));
					consultaDetalles.put("nombre_comercial", rs.getString("nombre_comercial")==null?"":rs.getString("nombre_comercial"));
					consultaDetalles.put("rfc", rs.getString("rfc")==null?"":rs.getString("rfc"));
					consultaDetalles.put("calle", rs.getString("calle")==null?"":rs.getString("calle"));
					consultaDetalles.put("colonia", rs.getString("colonia")==null?"":rs.getString("colonia"));
					consultaDetalles.put("estado", rs.getString("estado")==null?"":rs.getString("estado"));
					consultaDetalles.put("municipio", rs.getString("municipio")==null?"":rs.getString("municipio"));
					consultaDetalles.put("codigo_postal", rs.getString("codigo_postal")==null?"":rs.getString("codigo_postal"));
					consultaDetalles.put("pais", rs.getString("pais")==null?"":rs.getString("pais"));
					consultaDetalles.put("telefono", rs.getString("telefono")==null?"":rs.getString("telefono"));
					consultaDetalles.put("fax", rs.getString("fax")==null?"":rs.getString("fax"));
					consultaDetalles.put("ap_paterno", rs.getString("ap_paterno")==null?"":rs.getString("ap_paterno"));
					consultaDetalles.put("ap_materno", rs.getString("ap_materno")==null?"":rs.getString("ap_materno"));
					consultaDetalles.put("nombre_contacto", rs.getString("nombre_contacto")==null?"":rs.getString("nombre_contacto"));
					consultaDetalles.put("tel_contacto", rs.getString("tel_contacto")==null?"":rs.getString("tel_contacto"));
					consultaDetalles.put("fax_contacto", rs.getString("fax_contacto")==null?"":rs.getString("fax_contacto"));
					consultaDetalles.put("email_contacto", rs.getString("email_contacto")==null?"":rs.getString("email_contacto"));
					consultaDetalles.put("cel_contacto", rs.getString("cel_contacto")==null?"":rs.getString("cel_contacto"));
				}
				rs.close();
				if(ps!=null) ps.close();

		 } catch(SQLException sqle) {
			sqle.printStackTrace();
			throw new AppException("Existe error en la consulta: "+sqle);
			} catch (Exception e) {
			e.printStackTrace();
				throw new AppException("Existe error en el codigo: "+e);
			} finally {
				if(con.hayConexionAbierta()) con.cierraConexionDB();
			}
			log.info("getMandanteDetalles(S)");
			return consultaDetalles;
	}	// Fin del metodo getMandanteDetalles


	/**
	 * M�todo que realiza la consulta para p�gina Consulta Intermediarios Financieros "Detalle" EPO)
	 * @autor  Ivan Almaguer
	 * @param  numNafinElec	Numero de Nafin electronico de IF
	 * @return consultaDetallesIf
	 * @since  FODEA 046 - 2009 Tasas Y WS Mandato
	 * @throws AppException lanzada cuando ocurre un error.
	 */
    public Hashtable getIfDetalles(String numNafinElec) throws AppException {
    log.info("getIfDetalles(E)");
	 StringBuffer sQuery = new StringBuffer();
	 List varBind = new ArrayList();
	 AccesoDB con = new AccesoDB();
    PreparedStatement ps = null;
    ResultSet rs = null;
	 Hashtable consultaDetallesIf = new Hashtable();

    try {
		 con.conexionDB();

        sQuery.append(	"  SELECT /*+use_nl(ci cn, cdom, cest, cpa, cont)*/  " +
								"         ci.cg_razon_social AS razonsocial, cn.ic_nafin_electronico AS numnafinelect,  " +
								"         ci.cg_nombre_comercial AS nombre_comercial, ci.cg_rfc AS rfc,  " +
								"         cdom.cg_calle AS calle, cdom.cg_colonia AS colonia,  " +
								"         cest.cd_nombre AS estado, cdom.cg_municipio AS municipio,   " +
								"         cdom.cn_cp AS codigo_postal, cpa.cd_descripcion AS pais,  " +
								"         cdom.cg_telefono1 AS telefono, cont.cg_email AS email_contacto,  " +
								"         cdom.cg_fax AS fax  " +
								"    FROM comcat_if ci,   " +
								"         comrel_nafin cn,  " +
								"         com_domicilio cdom,  " +
								"         comcat_estado cest,  " +
								"         comcat_pais cpa,  " +
								"         com_contacto cont  " +
								"    WHERE ci.ic_if = cn.ic_epo_pyme_if  " +
								"      AND ci.ic_if = cdom.ic_if  " +
								"      AND ci.ic_if = cont.ic_if(+) " +
								"      AND cdom.ic_estado = cest.ic_estado  " +
								"      AND cdom.ic_pais = cpa.ic_pais  " +
								"      AND cdom.cs_fiscal = ? " );
								varBind.add("S");


						if(numNafinElec!=null&&!"".equals(numNafinElec)) {
						 sQuery.append(" AND cn.ic_nafin_electronico = ? ");
						 varBind.add(numNafinElec);
						}
						sQuery.append(" ORDER BY ci.cg_razon_social ");

				   ps = con.queryPrecompilado(sQuery.toString(), varBind);
					log.debug("getIfDetalles - sQuery: "+ sQuery);
					log.debug("varBind: "+varBind);
					rs = ps.executeQuery();

					if(rs.next()) {
					consultaDetallesIf.put("razonsocial", rs.getString("razonsocial")==null?"":rs.getString("razonsocial"));
					consultaDetallesIf.put("numnafinelect", rs.getString("numnafinelect")==null?"":rs.getString("numnafinelect"));
					consultaDetallesIf.put("nombre_comercial", rs.getString("nombre_comercial")==null?"":rs.getString("nombre_comercial"));
					consultaDetallesIf.put("rfc", rs.getString("rfc")==null?"":rs.getString("rfc"));
					consultaDetallesIf.put("calle", rs.getString("calle")==null?"":rs.getString("calle"));
					consultaDetallesIf.put("colonia", rs.getString("colonia")==null?"":rs.getString("colonia"));
					consultaDetallesIf.put("estado", rs.getString("estado")==null?"":rs.getString("estado"));
					consultaDetallesIf.put("municipio", rs.getString("municipio")==null?"":rs.getString("municipio"));
					consultaDetallesIf.put("codigo_postal", rs.getString("codigo_postal")==null?"":rs.getString("codigo_postal"));
					consultaDetallesIf.put("pais", rs.getString("pais")==null?"":rs.getString("pais"));
					consultaDetallesIf.put("telefono", rs.getString("telefono")==null?"":rs.getString("telefono"));
					consultaDetallesIf.put("fax", rs.getString("fax")==null?"":rs.getString("fax"));
				}
				rs.close();
				if(ps!=null) ps.close();

		 } catch(SQLException sqle) {
			sqle.printStackTrace();
			throw new AppException("Existe error en la consulta: "+sqle);
			} catch (Exception e) {
			e.printStackTrace();
				throw new AppException("Existe error en el codigo: "+e);
			} finally {
				if(con.hayConexionAbierta()) con.cierraConexionDB();
			}
			log.info("getIfDetalles(S)");
			return consultaDetallesIf;
	}	// Fin del metodo getIfDetalles
	//------------- FODEA 046-2009 ---------------> (F)
  //FODEA 012 - 2010 ACF (I)
	/**
	 * Este m�todo obtiene la informaci�n de una solicitud de instrucci�n irrevocable.
	 * @param folioSolicitud Cadena que contiene el n�mero de folio de la solicitud capturada.
   * @return imformacionSolicitud HashMap con la informaci�n de la consulta.
	 * @throws AppException Exception lanzada cuando ocurre un error al guardar la informaci�n.
	 */
	public HashMap obtenerInformacionInstruccion(String folioSolicitud) throws AppException{
		log.info("obtenerInformacionInstruccion(I)");
		AccesoDB con = new AccesoDB();
		ResultSet rst = null;
		PreparedStatement pst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap informacionSolicitud = new HashMap();
		int numeroRegistros = 0;
		try{
			con.conexionDB();

			strSQL.append(" SELECT /*+ index(smd cp_com_solic_mand_doc_pk)+*/ ");
			//strSQL.append(" smd.ic_folio folio,");
			strSQL.append(" smd.ig_folio_instruccion folio,");//FODEA 033 - 2010 ACF
			strSQL.append(" crn.ic_nafin_electronico nafin_electronico,");
			strSQL.append(" pym.cg_razon_social nombre_pyme,");
			strSQL.append(" epo.cg_razon_social nombre_epo,");
      strSQL.append(" cif.cg_razon_social nombre_if,");
      strSQL.append(" DECODE (smd.ic_descontante, null, cif.cg_razon_social, cds.cg_razon_social) nombre_descontante,");
			strSQL.append(" TO_CHAR(smd.df_solicitud, 'dd/mm/yyyy') fecha_solicitud,");
      strSQL.append(" TO_CHAR(smd.df_solicitud, 'hh24:mi:ss') hora_solicitud,");
      strSQL.append(" smd.cg_usuario_solicitud usuario_solicitud");
			strSQL.append(" FROM com_solic_mand_doc smd");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comrel_nafin crn ");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
      strSQL.append(", comcat_if cds");
			strSQL.append(" WHERE smd.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND smd.ic_pyme = crn.ic_epo_pyme_if");
			strSQL.append(" AND smd.ic_epo = epo.ic_epo");
			strSQL.append(" AND smd.ic_if = cif.ic_if");
      strSQL.append(" AND smd.ic_descontante = cds.ic_if(+)");
			//strSQL.append(" AND smd.ic_folio = ?");
			strSQL.append(" AND smd.ig_folio_instruccion = ?");//FODEA 033 - 2010 ACF
			strSQL.append(" ORDER BY epo.cg_razon_social ASC");//FODEA 033 - 2010 ACF

			varBind.add(new Long(folioSolicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while(rst.next()){
				HashMap solicitud = new HashMap();
				solicitud.put("folioSolicitud", rst.getString("folio")==null?"":rst.getString("folio"));
				solicitud.put("nafinElectronicoPyme", rst.getString("nafin_electronico")==null?"":rst.getString("nafin_electronico"));
				solicitud.put("nombrePyme", rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme"));
				solicitud.put("nombreEpo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
        solicitud.put("nombreIf", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
        solicitud.put("nombreDescontante", rst.getString("nombre_descontante")==null?"":rst.getString("nombre_descontante"));
				solicitud.put("fechaSolicitud", rst.getString("fecha_solicitud")==null?"":rst.getString("fecha_solicitud"));
        solicitud.put("horaSolicitud", rst.getString("hora_solicitud")==null?"":rst.getString("hora_solicitud"));
        solicitud.put("usuarioSolicitud", rst.getString("usuario_solicitud")==null?"":rst.getString("usuario_solicitud"));
				informacionSolicitud.put("solicitud" + numeroRegistros, solicitud);
				numeroRegistros++;
			}

			informacionSolicitud.put("numeroRegistros", Integer.toString(numeroRegistros));

			rst.close();
			pst.close();
		}catch(Exception e){
			log.info("obtenerInformacionInstruccion(ERROR)");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
		}
		log.info("obtenerInformacionInstruccion(F)");
		return informacionSolicitud;
	}

	/**
	 * M�todo que genera el combo de descontantes que tiene parametrizados un IF
	 * @param claveIf Clave interna del IF.
	 * @return comboDescontante Lista de objetos ElementoCatalogo con los descontantes parametrizados.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	 public List getComboDescontantes(String claveIf) throws AppException{
		log.info("getComboDescontantes(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List comboDescontante = new ArrayList();
    String icDescontante1 = "";
    String icDescontante2 = "";

		try {
			con.conexionDB();

      strSQL.append(" SELECT ic_descontante1, ic_descontante2");
      strSQL.append(" FROM comrel_descontante_if");
      strSQL.append(" WHERE ic_if = ?");

			varBind.add(new Integer(claveIf));

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
        icDescontante1 = rst.getString("ic_descontante1")==null?"":rst.getString("ic_descontante1");
        icDescontante2 = rst.getString("ic_descontante2")==null?"":rst.getString("ic_descontante2");
      }

      rst.close();
      pst.close();

      if (!icDescontante1.equals("") || !icDescontante2.equals("")) {
        strSQL = new StringBuffer();
        varBind = new ArrayList();

        strSQL.append(" SELECT ic_if AS clave, cg_razon_social AS descripcion");
        strSQL.append(" FROM comcat_if");
        strSQL.append(" WHERE ic_if IN (");

        if (!icDescontante1.equals("")) {
          strSQL.append(" ?,");
          varBind.add(new Long(icDescontante1));
        }
        if (!icDescontante2.equals("")) {
          strSQL.append(" ?,");
          varBind.add(new Long(icDescontante2));
        }

        strSQL.replace(strSQL.lastIndexOf(","), strSQL.length(), "");
        strSQL.append(")");

        log.debug("..:: strSQL: "+strSQL.toString());
        log.debug("..:: varBind: "+varBind);

        pst = con.queryPrecompilado(strSQL.toString(), varBind);
        rst = pst.executeQuery();

        while(rst.next()){
          ElementoCatalogo elemento_catalogo = new ElementoCatalogo();
          elemento_catalogo.setClave(rst.getString("clave")==null?"":rst.getString("clave"));
          elemento_catalogo.setDescripcion(rst.getString("descripcion")==null?"":rst.getString("descripcion"));
          comboDescontante.add(elemento_catalogo);
        }

        rst.close();
        pst.close();
      }
		}catch(Exception e){
      log.info("getComboDescontantes(ERROR) ::..");
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getComboDescontantes(S) ::..");
		}
		return comboDescontante;
	}

  /**
   * Este m�todo valida que la pyme que realiza la solicitud de Istrucci�n Irrevocable
   */
  public String existeRelacionPymeDescontante(String clavePyme, String claveEpo, String claveDescontante) throws AppException{
    log.info("existeRelacionPymeDescontante(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
    String relacionPymeDescontante = "N";

		try {
			con.conexionDB();

      strSQL.append(" SELECT DECODE(COUNT(cpi.ic_cuenta_bancaria), 0, 'N', 'S') AS relacion_pyme_descontante");
      strSQL.append(" FROM comrel_pyme_if cpi");
      strSQL.append(", comrel_cuenta_bancaria ccb");
      strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
      strSQL.append(" AND cpi.cs_vobo_if = ?");
      strSQL.append(" AND cpi.cs_borrado = ?");
      strSQL.append(" AND cpi.ic_epo = ?");
      strSQL.append(" AND cpi.ic_if = ?");
      strSQL.append(" AND ccb.ic_pyme = ?");

			varBind.add("S");
      varBind.add("N");
      varBind.add(new Integer(claveEpo));
      varBind.add(new Integer(claveDescontante));
      varBind.add(new Integer(clavePyme));

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
        relacionPymeDescontante = rst.getString("relacion_pyme_descontante")==null?"N":rst.getString("relacion_pyme_descontante");
      }

      rst.close();
      pst.close();
		}catch(Exception e){
      log.info("existeRelacionPymeDescontante(ERROR) ::..");
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("existeRelacionPymeDescontante(S) ::..");
		}
		return relacionPymeDescontante;
	}

	/**
	 * M�todo que genera el combo de bancos de servicio con los que va a trabajar el IF en una Instrucci�n Irrevocable
	 * @return comboDescontante Lista de objetos ElementoCatalogo con los IF que puedes ser bancos de servicio.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	 public List getComboBancoServicio() throws AppException{
		log.info("getComboBancoServicio(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List comboBancoServicio = new ArrayList();

		try {
			con.conexionDB();

      strSQL.append(" SELECT /*+ use_nl(cif tif) index(cif in_comcat_if_07_nuk) ordered*/  cif.ic_if AS clave, cif.cg_razon_social AS descripcion");
      strSQL.append(" FROM comcat_if cif, comcat_financiera tif");
      strSQL.append(" WHERE cif.ic_financiera = tif.ic_financiera");
      strSQL.append(" AND tif.ic_tipo_financiera IN (?, ?, ?, ?, ?, ?, ?)");
      strSQL.append(" ORDER BY cif.cg_razon_social");
      varBind.add(new Integer(1));
      varBind.add(new Integer(4));
      varBind.add(new Integer(5));
      varBind.add(new Integer(6));
      varBind.add(new Integer(7));
      varBind.add(new Integer(10));
      varBind.add(new Integer(23));

      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        ElementoCatalogo elemento_catalogo = new ElementoCatalogo();
        elemento_catalogo.setClave(rst.getString("clave")==null?"":rst.getString("clave"));
        elemento_catalogo.setDescripcion(rst.getString("descripcion")==null?"":rst.getString("descripcion"));
        comboBancoServicio.add(elemento_catalogo);
      }

      rst.close();
      pst.close();
		}catch(Exception e){
      log.info("getComboBancoServicio(ERROR) ::..");
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getComboBancoServicio(S) ::..");
		}
		return comboBancoServicio;
	}

	/**
	 * M�todo que genera el combo de bancos de servicio con los que va a trabajar el IF en una Instrucci�n Irrevocable
	 * @return comboDescontante Lista de objetos ElementoCatalogo con los IF que puedes ser bancos de servicio.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	 public String getBancoServicio(String claveBancoServicio) throws AppException{
		log.info("getBancoServicio(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String bancoServicio = "";

		try {
			con.conexionDB();

      strSQL.append(" SELECT cg_razon_social AS descripcion");
      strSQL.append(" FROM comcat_if");
      strSQL.append(" WHERE ic_if = ?");

      varBind.add(new Integer(claveBancoServicio));

      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        bancoServicio = rst.getString("descripcion")==null?"":rst.getString("descripcion");
      }

      rst.close();
      pst.close();
		}catch(Exception e){
      log.info("getBancoServicio(ERROR) ::..");
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getBancoServicio(S) ::..");
		}
		return bancoServicio;
	}

	/**
	 * M�todo que obtiene el par�metro que determina si un if opera con instrucci�n irrevocable.
	 * @param claveIf Clave interna del IF.
	 * @return parametroIstruccionIrrevocable Cadena con el valor S o N dependiendo si el if opera o no con instrucci�n irrevocable.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	 public String esIfConDescontante(String claveIf) throws AppException{
		log.info("esIfConDescontante(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String esIfConDescontante = "";
    String icDescontante1 = "";
    String icDescontante2 = "";

		try {
			con.conexionDB();

      strSQL.append(" SELECT ic_descontante1, ic_descontante2");
      strSQL.append(" FROM comrel_descontante_if");
      strSQL.append(" WHERE ic_if = ?");

			varBind.add(new Integer(claveIf));

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
        icDescontante1 = rst.getString("ic_descontante1")==null?"":rst.getString("ic_descontante1");
        icDescontante2 = rst.getString("ic_descontante2")==null?"":rst.getString("ic_descontante2");
      }

      rst.close();
      pst.close();

      if (icDescontante1.equals("") && icDescontante2.equals("")) {
        esIfConDescontante = "N";
      } else {
        esIfConDescontante = "S";
      }
		}catch(Exception e){
      log.info("esIfConDescontante(ERROR) ::..");
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("esIfConDescontante(S) ::..");
		}
		return esIfConDescontante;
	}
	//FODEA 012 - 2010 ACF (F)
	//FODEA 033 - 2010 ACF (I)
	/**
    * M�todo que genera el combo de IFs dependiendo del tipo de Cadena con el que
    * operan la Instrucci�n Irrevocable: P�blicas, Privadas o Ambas
    * @return comboDescontante Lista de objetos ElementoCatalogo con los IF que puedes ser bancos de servicio.
    * @throws AppException Cuando ocurre un error en la consulta.
    * @param tipoCadena
    */
    public List getComboIfTipoCadena(String tipoCadena) throws AppException{
       return getComboIfTipoCadena(tipoCadena, false);
    }

	 public List getComboIfTipoCadena(String tipoCadena, boolean bandera) throws AppException{
		log.info("getComboIfTipoCadena(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List comboIfTipoCadena = new ArrayList();

		try {
			con.conexionDB();

			strSQL.append(" SELECT cif.ic_if AS clave,");
			strSQL.append(" cif.cg_razon_social AS descripcion");
			strSQL.append(" FROM comcat_if cif");
			strSQL.append(", comrel_descontante_if cdi");
			strSQL.append(" WHERE cif.ic_if = cdi.ic_if");
			strSQL.append(" AND cdi.ic_tipo_epo_inst IN (?, ?)");
			strSQL.append(" ORDER BY cif.cg_razon_social");

      // FODEA 022-2012
      if(bandera){
         if(tipoCadena.equals("1")){ // FODEA 022-2012
            varBind.add(new Integer("1"));
            varBind.add(new Integer("2"));
         }

         else if(tipoCadena.equals("2")){ // FODEA 022-2012
            varBind.add(new Integer("3"));
            varBind.add(new Integer("4"));
         }

         else{
            varBind.add(new Integer(tipoCadena));
            varBind.add(new Integer(3));
         }
      }

      else{
         varBind.add(new Integer(tipoCadena));
         varBind.add(new Integer(3));
      }

      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
        elementoCatalogo.setClave(rst.getString("clave")==null?"":rst.getString("clave"));
        elementoCatalogo.setDescripcion(rst.getString("descripcion")==null?"":rst.getString("descripcion"));
        comboIfTipoCadena.add(elementoCatalogo);
      }

      rst.close();
      pst.close();
		}catch(Exception e){
      log.info("getComboIfTipoCadena(ERROR) ::..");
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getComboIfTipoCadena(S) ::..");
		}
		return comboIfTipoCadena;
	}

   /**
    * FODEA 022 - 2012
    * @autor garellano
    * @Fecha 06/dic/2012
    */
	 public List getComboIf(String tipoCadena) throws AppException{
		log.info("getComboIfTipoCadena(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		List comboIfTipoCadena = new ArrayList();

		try {
			con.conexionDB();

			strSQL.append(" SELECT cif.ic_if AS clave,");
			strSQL.append(" cif.cg_razon_social AS descripcion");
			strSQL.append(" FROM comcat_if cif");
			strSQL.append(", comrel_descontante_if cdi");
			strSQL.append(" WHERE cif.ic_if = cdi.ic_if");
			strSQL.append(" AND cdi.ic_tipo_epo_inst IN (?, ?)");
			strSQL.append(" ORDER BY cif.cg_razon_social");

      varBind.add(new Integer(tipoCadena));
      varBind.add(new Integer(3));

      log.debug("..:: strSQL: "+strSQL.toString());
      log.debug("..:: varBind: "+varBind);

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
        elementoCatalogo.setClave(rst.getString("clave")==null?"":rst.getString("clave"));
        elementoCatalogo.setDescripcion(rst.getString("descripcion")==null?"":rst.getString("descripcion"));
        comboIfTipoCadena.add(elementoCatalogo);
      }

      rst.close();
      pst.close();
		}catch(Exception e){
      log.info("getComboIfTipoCadena(ERROR) ::..");
			throw new AppException("Error al consultar base de datos: ", e);
		}finally{
			if(con.hayConexionAbierta()){con.cierraConexionDB();}
			log.info("getComboIfTipoCadena(S) ::..");
		}
		return comboIfTipoCadena;
	}

	/**
	 * Este m�todo obtiene las EPOs p�blicas que pueden operar Instrucci�n Irrevocable,
	 * con las que se encuentra afiliada habilitada una Pyme.
	 * @param clavePyme Clave interna de la Pyme.
	 * @param tipoCadena Tipo de cadena a la que pertencen las epos a seleccionar.
	 * @return eposPrivadasAfiliadas HashMap con las EPOs con las que se encuentra afiliada habilitada la pyme.
	 * @throws AppException Cuado ocurre un error en la consulta
	 */
	public HashMap obtenerEposAfiliadas(String clavePyme, String tipoCadena) throws AppException {
		log.info("obtenerEposAfiliadas(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		HashMap eposAfiliadas = new HashMap();
		int numeroRegistros = 0;
		try {
			con.conexionDB();

			strSQL.append(" SELECT epo.ic_epo AS clave_epo,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo");
			strSQL.append(" FROM comrel_pyme_epo cpe, comcat_epo epo");
			strSQL.append(" WHERE epo.ic_epo = cpe.ic_epo");
			strSQL.append(" AND epo.cs_mandato_documento = ?");
			strSQL.append(" AND cpe.cs_aceptacion = ?");
			strSQL.append(" AND epo.ic_tipo_epo IN (?, ?)");
			strSQL.append(" AND cpe.ic_pyme = ?");
			strSQL.append(" ORDER BY epo.cg_razon_social");

			varBind.add("S");
			varBind.add("H");
			if (tipoCadena.equals("1")) {//PUBLICAS
				varBind.add(new Integer(1));
				varBind.add(new Integer(2));
			} else if (tipoCadena.equals("2")) {//PRIVADAS
				varBind.add(new Integer(3));
				varBind.add(new Integer(4));
			}
			varBind.add(new Integer(clavePyme));

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				HashMap epoAfiliada = new HashMap();
				epoAfiliada.put("claveEpo", rst.getString("clave_epo")==null?"":rst.getString("clave_epo"));
				epoAfiliada.put("nombreEpo", rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
				eposAfiliadas.put("epoAfiliada"+numeroRegistros, epoAfiliada);
				numeroRegistros++;
			}

			eposAfiliadas.put("numeroRegistros", Integer.toString(numeroRegistros));

			rst.close();
			pst.close();
		} catch (Exception e) {
			log.info("obtenerEposAfiliadas(ERROR) ::..");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerEposAfiliadas(S) ::..");
		}
		return eposAfiliadas;
	}

	/**
	 * Este metodo tiene la finalidad de consultar las instrucciones irrevocables existentes
	 * de acuerdo a los parametros de consulta
	 * @param parametrosConsulta HashMap con los parametros con los que se realiza la consulta.
	 * @return registros Objeto Registros con los resultados de la consulta
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	public Registros consultarIntruccionesIrrevocables(HashMap parametrosConsulta) throws AppException {
		log.info("consultarIntruccionesIrrevocables(E) ::..");
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		Registros registros = new Registros();

		try {
			con.conexionDB();

			String claveTipoCadena = (String)parametrosConsulta.get("claveTipoCadena");
			String claveEpo = (String)parametrosConsulta.get("claveEpo");
			String claveIf = (String)parametrosConsulta.get("claveIf");
			String numeroNafele = (String)parametrosConsulta.get("numeroNafele");
			String claveEstatus = (String)parametrosConsulta.get("claveEstatus");
			String fechaSolicitudIni = (String)parametrosConsulta.get("fechaSolicitudIni");
			String fechaSolicitudFin = (String)parametrosConsulta.get("fechaSolicitudFin");
			String fechaAceptacionIni = (String)parametrosConsulta.get("fechaAceptacionIni");
			String fechaAceptacionFin = (String)parametrosConsulta.get("fechaAceptacionFin");
			String perfilUsuario = (String)parametrosConsulta.get("perfilUsuario");

			//strSQL.append(" SELECT /*+ use_nl(smd crn pym cdi epo cif cds bsr mon emd tepo) index(smd cp_com_solic_mand_doc_pk) */");
			strSQL.append(" SELECT /*+ use_nl(smd crn pym cdi epo cif cds bsr mon mnd emd tepo) index(smd cp_com_solic_mand_doc_pk) */");//FODEA 015 - 2011 ACF
			strSQL.append(" smd.ig_folio_instruccion folio_instruccion,");
			strSQL.append(" crn.ic_nafin_electronico nafin_electronico,");
			strSQL.append(" pym.cg_razon_social pyme,");
			strSQL.append(" epo.cg_razon_social epo,");
			strSQL.append(" cif.cg_razon_social if_tramita_credito,");
			strSQL.append(" DECODE (cds.cg_razon_social, null, cif.cg_razon_social, cds.cg_razon_social) descontante,");
			strSQL.append(" tepo.cg_descripcion tipo_cadena,");
			strSQL.append(" TO_CHAR(smd.df_solicitud, 'dd/mm/yyyy') fecha_solicitud,");
			strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', TO_CHAR(smd.df_aceptacion, 'dd/mm/yyyy')) fecha_aceptacion,");
			strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', smd.fn_monto) monto_credito,");
			strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', TO_CHAR(smd.df_vencimiento, 'dd/mm/yyyy')) AS fecha_vencimiento_credito,");
			strSQL.append(" bsr.cg_razon_social banco_de_servicio,");
			strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', 1, '', 'NC: ' || smd.cg_cuenta_bancaria) cuenta_bancaria,");
			strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', cbm.cg_descripcion) tipo_cuenta,");
			strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', smd.cg_credito) numero_credito,");
			strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', mon.cd_nombre) moneda_credito,");
			strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', DECODE(smd.ic_moneda_dscto, null, 'AMBAS', mnd.cd_nombre)) AS moneda_descuento,");//FODEA 015 - 2011 ACF
			strSQL.append(" DECODE(smd.ic_estatus_man_doc, 2, DECODE(smd.cs_dscto_aut_irrev, 'S', 'Activado', 'Desactivado') || ' ' || TO_CHAR(smd.df_dscto_aut_irrev, 'dd/mm/yyyy hh:mi:ss am'), 'NA') AS fechaDescAut,");//FODEA 015 - 2011 ACF
			strSQL.append(" smd.cg_observaciones observaciones, ");
			strSQL.append(" emd.cg_descripcion estatus_solicitud, ");
			strSQL.append(" smd.ic_estatus_man_doc, ");//FODEA 055 - 2010 By JSHD
			strSQL.append(" nvl(TO_CHAR(smd.df_nuevo_vencimiento, 'dd/mm/yyyy'),'') as nueva_fecha_vencimiento,  ");//FODEA 055 - 2010 By JSHD
			strSQL.append(" nvl(TO_CHAR(smd.df_mod_monto,         'dd/mm/yyyy'),'') as fecha_modificacion        ");//FODEA 055 - 2010 By JSHD
			strSQL.append(" FROM com_solic_mand_doc smd");
			strSQL.append(", comrel_nafin crn ");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_if cif");
			strSQL.append(", comcat_if cds");
			strSQL.append(", comcat_if bsr");
			strSQL.append(", comcat_tipo_cta_man_doc cbm");
			strSQL.append(", comcat_moneda mon");
			strSQL.append(", comcat_moneda mnd");//FODEA 015 - 2011 ACF
			strSQL.append(", comcat_estatus_man_doc emd");
			strSQL.append(", comcat_tipo_epo tepo");
			strSQL.append(" WHERE smd.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND smd.ic_pyme = crn.ic_epo_pyme_if");
			strSQL.append(" AND smd.ic_epo = epo.ic_epo");
			strSQL.append(" AND smd.ic_if = cif.ic_if");
			strSQL.append(" AND smd.cc_tipo_cuenta = cbm.cc_tipo_cuenta(+)");
			strSQL.append(" AND smd.ic_moneda = mon.ic_moneda(+)");
			strSQL.append(" AND smd.ic_moneda_dscto = mnd.ic_moneda(+)");//FODEA 015 - 2011 ACF
			strSQL.append(" AND smd.ic_estatus_man_doc = emd.ic_estatus_man_doc");
			strSQL.append(" AND smd.ic_descontante = cds.ic_if(+)");
			strSQL.append(" AND smd.ic_banco_servicio = bsr.ic_if(+)");
			strSQL.append(" AND epo.ic_tipo_epo = tepo.ic_tipo_epo(+)");
			strSQL.append(" AND crn.cg_tipo = ?");

			varBind.add("P");

			//Tipo de cadena
			if(claveTipoCadena != null && !claveTipoCadena.equals("")){
				strSQL.append(" AND epo.ic_tipo_epo = ?");
				varBind.add(new Integer(claveTipoCadena));
			}
			//EPO
			if(claveEpo != null && !claveEpo.equals("")){
				strSQL.append(" AND epo.ic_epo = ?");
				varBind.add(new Long(claveEpo));
			}
			//Intermediario Financiero
			if(claveIf != null && !claveIf.equals("")){
				if(!perfilUsuario.equals("DESCONT IF")){
					strSQL.append(" AND cif.ic_if = ?");
				}  else {
					strSQL.append(" AND smd.ic_descontante = ?");
				}
				varBind.add(new Long(claveIf));
			}
			//PyME
			if(numeroNafele != null && !numeroNafele.equals("")){
				strSQL.append(" AND crn.ic_nafin_electronico = ?");
				varBind.add(new Long(numeroNafele));
			}
			//Estatus de la solicitud
			if(claveEstatus != null && !claveEstatus.equals("")){
				strSQL.append(" AND smd.ic_estatus_man_doc = ?");
				varBind.add(new Integer(claveEstatus));
			}
			//Fechas de solicitud de mandato de documentos
			if(fechaSolicitudIni != null && !fechaSolicitudIni.equals("") && fechaSolicitudFin != null && !fechaSolicitudFin.equals("")){
				strSQL.append(" AND smd.df_solicitud >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND smd.df_solicitud < TO_DATE(?, 'dd/mm/yyyy') + 1");
				varBind.add(fechaSolicitudIni);
				varBind.add(fechaSolicitudFin);
			}
			//Fechas de Aceptaci�n IF
			if(fechaAceptacionIni != null && !fechaAceptacionIni.equals("") && fechaAceptacionFin != null && !fechaAceptacionFin.equals("")){
				strSQL.append(" AND smd.df_aceptacion >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND smd.df_aceptacion < TO_DATE(?, 'dd/mm/yyyy') + 1");
				varBind.add(fechaAceptacionIni);
				varBind.add(fechaAceptacionFin);
			}

			strSQL.append(" ORDER BY 1 DESC, 4 ASC");

			log.debug("..:: strSQL : " + strSQL.toString());
			log.debug("..:: varBind : " + varBind);

			registros = con.consultarDB(strSQL.toString(), varBind);
		} catch(Exception e) {
			log.info("consultarIntruccionesIrrevocables(ERROR) ::..");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("consultarIntruccionesIrrevocables(S) ::..");
		}
		return registros;
	}

	/**
	 * Este m�todo obtiene el tipo de cadena al que pertenecen las epos con las que
	 * fu� capturada una instrucci�n irrevocable, englobando PEF y Gobiernos Estatales y Municipales como PUBLICAS valor 1,
	 * y las restantes como PRIVADAS valor 2.
	 * @param claveEpo Clave interna de la epo.
	 * @return tipoCadena Tipo de cadena a la que pertencen la epo.
	 * @throws AppException Cuado ocurre un error en la consulta
	 */
	public String obtenerTipoCadenaInstruccion(String folioIntruccion) throws AppException {
		log.info("obtenerTipoCadenaInstruccion(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String tipoCadenaInstruccion = "";
		try {
			con.conexionDB();

			strSQL.append(" SELECT DISTINCT DECODE (epo.ic_tipo_epo, 1, '1', 2, '1', 3 , '2', 4, '2', null, '2') AS tipo_epo");
			strSQL.append(" FROM comcat_epo epo");
			strSQL.append(", com_solic_mand_doc sir");
			strSQL.append(" WHERE sir.ic_epo = epo.ic_epo");
			strSQL.append(" AND ig_folio_instruccion = ?");

			varBind.add(new Integer(folioIntruccion));

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				tipoCadenaInstruccion = rst.getString("tipo_epo");
			}

			rst.close();
			pst.close();
		} catch (Exception e) {
			log.info("obtenerTipoCadenaInstruccion(ERROR) ::..");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("obtenerTipoCadenaInstruccion(S) ::..");
		}
		return tipoCadenaInstruccion;
	}
	//FODEA 033 - 2010 ACF (F)


	//F054-2010 FVR
	/**
	 * Se altera valor de la solicitud de instruccion irrevocable
	 * para indicar si opera descuento automatico o no
	 * @throws netropology.utilerias.AppException
	 * @param accion
	 * @param folio_solicitud
	 */
	public void activaDesactivaDescAut(String folio_solicitud, String accion) throws AppException{
		log.info("activaDesactivaDescAut(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean commit = true;
		try{
			con.conexionDB();
			//Insercion de codigo
			strSQL.append(" UPDATE com_solic_mand_doc SET");
			strSQL.append(" cs_dscto_aut_irrev = ?");
			strSQL.append(", df_dscto_aut_irrev = SYSDATE");//FODEA 015 - 2011 ACF
			strSQL.append(" WHERE ig_folio_instruccion = ?");

			varBind.add(accion);
			varBind.add(new Long(folio_solicitud));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			pst.executeUpdate();
			pst.close();

		}catch(Exception e){
			commit=false;
			e.printStackTrace();
			throw new AppException("Error al Activar/Desactivar Desc. Aut.");
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("activaDesactivaDescAut(S)");
		}

	}
}