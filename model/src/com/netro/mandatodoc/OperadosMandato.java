package com.netro.mandatodoc;
import java.sql.*;
import netropology.utilerias.*;
import java.util.List;
import java.util.ArrayList;

public class OperadosMandato  {
	private String qrysentencia = "";
	private String folio_solicitud = "";
	private String fecha_de = "";
	private String fecha_a = "";
	private String clave_epo = "";
	private String clave_pyme = "";
	private String clave_if = "";
	

	public void setQrysentencia(String qrysentencia ) { 
	String cond = "";
	if(fecha_a.equals("")){
	cond = ("AND s.df_operacion < TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'), 'dd/mm/yyyy') + 1 ");
	}
	else{
	cond = (" AND s.df_operacion <  TO_DATE( ? ,'dd/mm/yyyy') + 1 "); 
	}
	
	qrysentencia = 	
			"SELECT   /*+use_nl(s ds d p pe e i m tf)*/ " +
			"         p.in_numero_sirac AS num_sirac,  " +
			"         p.cg_razon_social AS nombrepyme,  " +
			"         d.ic_documento AS num_documento,   " +
			"         TO_CHAR (d.df_fecha_docto, 'dd/mm/yyyy') AS fecha_emision, " +
			"         TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fecha_vencimiento, " +
			"         d.ic_moneda AS moneda,  " +
			"         m.cd_nombre AS nombre_moneda, " +
			"         tf.cg_nombre AS tipo_factoraje,  " +
			"         d.fn_monto AS monto,  " +
			"         d.fn_porc_anticipo AS porc_descuento, " +
			"         d.fn_monto - d.fn_monto_dscto AS recursogarantia,  " +
			"         ds.in_importe_interes AS monto_interes, " +
			"         d.fn_monto_dscto AS monto_descuento, " +
			"         ds.in_importe_recibir AS monto_a_operar, " +
			"         ds.in_tasa_aceptada AS tasa,  " +
			"         s.ig_plazo AS plazo, " +
			"         pe.cg_pyme_epo_interno AS num_proveedor,    " +
			"         'OperadosMandato::getQrysentencia();' " +
			"    FROM com_solicitud s,  " +
			"         com_docto_seleccionado ds, " +
			"         com_documento d, " +
			"         comcat_pyme p, " +
			"         comrel_pyme_epo pe, " +
			"         comcat_epo e, " +
			"         comcat_if i, " +
			"         comcat_moneda m, " +
			"         comcat_tipo_factoraje tf " +
			"   WHERE  " +
			"     s.ic_documento = ds.ic_documento " +
			"     AND s.ic_documento = d.ic_documento " +
			"     AND d.ic_pyme = p.ic_pyme " +
			"     AND d.ic_epo = pe.ic_epo " +
			"     AND d.ic_pyme = pe.ic_pyme  " +
			"     AND d.ic_epo = e.ic_epo " +
			"     AND ds.ic_if = i.ic_if " +
			"     AND d.ic_moneda = m.ic_moneda " +
			"     AND d.cs_dscto_especial = tf.cc_tipo_factoraje      " +
		//	"     AND ds.ic_epo = d.ic_epo " +
			"     AND pe.cs_habilitado = 'S' " +
			"     AND d.cs_dscto_especial != 'C' " +
			"     AND s.ic_estatus_solic in (3,5,6,10) " +//FODEA 012 - 2010 ACF
			"		AND e.ic_epo = ? " +
			"     AND p.ic_pyme = ? " +
			"     AND i.ic_if = ? " +
			"     AND s.df_operacion >= TO_DATE( ? ,'dd/mm/yyyy') " + cond +
			"ORDER BY p.cg_razon_social ";
	//System.out.println(":::: OperadosMandato :: setQrysentencia :::: ok! ");
	this.qrysentencia = qrysentencia; 	
	}
	
	public void setFecha_de(String fecha_de) { this.fecha_de = fecha_de;	}
	public void setFecha_a(String fecha_a) { this.fecha_a = fecha_a; }
	public void setFolio_solicitud(String folio_solicitud) {	this.folio_solicitud = folio_solicitud; }
	public void setClave_epo(String clave_epo) { this.clave_epo = clave_epo; }
	public void setClave_pyme(String clave_pyme) { this.clave_pyme = clave_pyme; }
	public void setClave_if(String clave_if) { this.clave_if = clave_if;	}
	
	public String getQrysentencia() { return qrysentencia; }
	public String getFecha_de() {	return fecha_de; }
	public String getFecha_a() { return fecha_a; }
	public String getFolio_solicitud() { return folio_solicitud; }
	public String getClave_epo() { return clave_epo; }
	public String getClave_pyme() { return clave_pyme;	}
	public String getClave_if() { return clave_if; }
	
}