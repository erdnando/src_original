
package com.netro.cedi;

import com.netro.pdf.ComunesPDF;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import netropology.utilerias.usuarios.SolicitudArgus;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "CediEJB", mappedName = "CediEJB")
@TransactionManagement(TransactionManagementType.BEAN)


public class CediBean implements Cedi {

	public CediBean() {
	}

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CediBean.class);

	/**
	 *	M�todo que nos regresara un HashMap con la parametrizaci�n de las preguntas de Cedi.
	 * @return
	 * @throws AppException
	 */


	public String modificacionEncuestaGral(List lstPreguntas, List lstRespuestas,String num_real_preg ) {
		log.info("CediEJB::modificacionEncuestaGral(E)");

		boolean lbSinError = true;
		AccesoDB conexBD = new AccesoDB();
		PreparedStatement ps;
		String SQLEncuesta;

		try {
			conexBD.conexionDB();

			log.debug("lstPreguntas " + lstPreguntas.size() + "lstRespuestas " + lstRespuestas.size());

			guardaPreguntaGral(conexBD, lstPreguntas, lstRespuestas,num_real_preg  );

			if (lstRespuestas.size() > 0) {
				//guardaRespuestaEnc(conexBD, lstRespuestas, num_real_preg );
			}

			lbSinError = true;

			return "Exito";

		} catch (Throwable t) {
			lbSinError = false;
			throw new AppException("Error al modificar la encuesta", t);
		} finally {
			conexBD.terminaTransaccion(lbSinError);
			if (conexBD.hayConexionAbierta()) {
				conexBD.cierraConexionDB();
			}
			log.debug("CediEJB::modificacionEncuestaGral(S)");
		}

	}

	/**
	 *METODO QUE GUARDA LAS PREGUNTA
	 * @param conexBD
	 * @param lstPreguntas
	 */
	public void guardaPreguntaGral(AccesoDB conexBD, List lstPreguntas,List lstRespuestas,String num_real_preg ) {
		log.info("CediEJB::guardaPreguntaGral(E)");

		boolean lbSinError = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
	    PreparedStatement ps1 = null;
	    ResultSet rs1 = null;
		List varBind = new ArrayList();
		String SQLPregunta;
		try {
			int num_re_preg = Integer.parseInt(num_real_preg);
			int tam_param_resp = lstPreguntas.size();
		    HashMap datafila = new HashMap();
		   
			if (lstPreguntas != null && lstPreguntas.size() > 0) {
				for (int j = 0; j < lstPreguntas.size(); j++) {
				    datafila = (HashMap)lstPreguntas.get(j);
					
				    HashMap DatosPregEnc =(HashMap)datafila.get("dato_pregunta");
				    ArrayList DatosResp =(ArrayList)datafila.get("dato_resp");
					String fpPregunta = (String) DatosPregEnc.get("PREGUNTA");
					String fpTipoResp = (String) DatosPregEnc.get("TIPORESP");
					String fpNumOpc = (String) DatosPregEnc.get("NUMOPC");
					String ESTADO = (String) DatosPregEnc.get("ESTADO");
				    String icpregunta = (String) DatosPregEnc.get("icpregunta");
				    String ic_tipo_resp_inicial = (String) DatosPregEnc.get("ic_tipo_resp_inicial");
				    String num_opc_inicial = (String) DatosPregEnc.get("num_opc_inicial");
				    if (fpPregunta.length() > 300) {
				        fpPregunta = fpPregunta.substring(0, 300);
				    }
					
				   
					
				    String existe_pregunta = "N";
					String existe_respuesta = "N";
				    String queryExistePreg = "";
					if(j < num_re_preg){
					    if(!icpregunta.equals("")){
							queryExistePreg =
											   "	SELECT DECODE(COUNT(*),0,'N','S' ) AS EXISTE_PREGUNTA FROM cedi_pregunta " +
											   "	WHERE IC_PREGUNTA = ?" + "	AND   IC_TIPO_RESPUESTA =?";
							varBind = new ArrayList();
							varBind.add(icpregunta);
							varBind.add(ic_tipo_resp_inicial);
							
							log.debug("SQLPregunta -- "+queryExistePreg);
							log.debug("varBind --  "+varBind);
							ps = conexBD.queryPrecompilado(queryExistePreg, varBind);
							rs = ps.executeQuery();
							
							if (rs.next()) {
								existe_pregunta = rs.getString("EXISTE_PREGUNTA") == null ? "" :
												  rs.getString("EXISTE_PREGUNTA");
							}
							rs.close();
							ps.close();
						}
					}
					if(!icpregunta.equals("")){
						log.debug("SQLPregunta --, "+queryExistePreg);
						queryExistePreg =
										   "    SELECT DECODE(COUNT(*),0,'N','S' ) AS EXISTE_RESPUESTA " +
										   "    from  cedi_respuesta_parametrizacion  where IC_PREGUNTA = ? and IC_TIPO_RESPUESTA = ?";
						varBind = new ArrayList();
						varBind.add(icpregunta);
						varBind.add(fpTipoResp);
						
						log.debug("SQLPregunta -- "+queryExistePreg);
						log.debug("varBind -- "+varBind);
						ps = conexBD.queryPrecompilado(queryExistePreg, varBind);
						rs = ps.executeQuery();
						
						if (rs.next()) {
							existe_respuesta = rs.getString("EXISTE_RESPUESTA") == null ? "" :
											  rs.getString("EXISTE_RESPUESTA");
						}
						rs.close();
						ps.close();
					}
					if (existe_pregunta.equals("S")) {
						if(fpTipoResp.equals(ic_tipo_resp_inicial)&&(fpNumOpc.equals(num_opc_inicial)||!fpNumOpc.equals(num_opc_inicial))){
						    SQLPregunta = " UPDATE cedi_pregunta" + "   SET CG_PREGUNTA = ?, IG_NUM_OPCIONES = ? , CS_ACTIVO = ?" +
						                          " WHERE IC_PREGUNTA = ? AND  IC_TIPO_RESPUESTA = ? ";
						    varBind = new ArrayList();
						    varBind.add(fpPregunta);
						    varBind.add(fpNumOpc);
						    varBind.add(ESTADO);
						    varBind.add(icpregunta);
						    varBind.add(fpTipoResp);
						   
						    ps = conexBD.queryPrecompilado(SQLPregunta, varBind);
						    ps.executeUpdate();
						    ps.clearParameters();
						    ps.close();
							
						    if(existe_respuesta.equals("S")&&DatosResp.size()>0){
						       
						        guardaRespuestaEnc(conexBD,DatosResp,num_real_preg,"N",icpregunta,fpTipoResp);
						        
							}else{
								if(existe_respuesta.equals("N")&&DatosResp.size()>0){
									guardaRespuestaEnc(conexBD,DatosResp,num_real_preg,"S",icpregunta,fpTipoResp);
								}
							}
							
						}else {
						    SQLPregunta = " delete cedi_pregunta where IC_PREGUNTA = ? and IC_TIPO_RESPUESTA = ?   ";
						    varBind = new ArrayList();
							varBind.add(icpregunta);
						    varBind.add(ic_tipo_resp_inicial);
						    ps1 = conexBD.queryPrecompilado(SQLPregunta, varBind);
							ps1.executeUpdate();
						    ps1.close();
						    SQLPregunta = " delete cedi_respuesta_parametrizacion where IC_PREGUNTA = ? and IC_TIPO_RESPUESTA = ?   ";
						    varBind = new ArrayList();
						    varBind.add(icpregunta);
						    varBind.add(ic_tipo_resp_inicial);
						    ps1 = conexBD.queryPrecompilado(SQLPregunta, varBind);
						    ps1.executeUpdate();
						    ps1.close();
						    existe_respuesta = "N";
						    SQLPregunta = "  INSERT INTO cedi_pregunta(IC_PREGUNTA,IC_TIPO_RESPUESTA,CG_PREGUNTA,IG_NUM_OPCIONES,CS_ACTIVO)" +
						                          " VALUES(?,?,?,?,?)";
						    varBind = new ArrayList();
						    varBind.add(icpregunta);
						    varBind.add(fpTipoResp);
						    varBind.add(fpPregunta);
						    varBind.add(fpNumOpc);
						    varBind.add(ESTADO);
						   
						    ps = conexBD.queryPrecompilado(SQLPregunta, varBind);
						    ps.executeUpdate();
						    ps.clearParameters();
						    ps.close();
						    
						    if(existe_respuesta.equals("N")&&DatosResp.size()>0){
						        guardaRespuestaEnc(conexBD,DatosResp,num_real_preg,"S",icpregunta,fpTipoResp);
						    }
							
							
						}
					   


					} else {
						String qry = "	SELECT MAX(IC_PREGUNTA) + 1  as IC_PREGUNTA FROM cedi_pregunta";
						String clave_preunta = "";
					    ps1 = conexBD.queryPrecompilado(qry);
					    rs1 = ps1.executeQuery();
					    if (rs1.next()) {
					        clave_preunta = rs1.getString("IC_PREGUNTA") ;
					    }
					    rs1.close();
					    ps1.close();
						
						
						SQLPregunta = "  INSERT INTO cedi_pregunta(IC_PREGUNTA,IC_TIPO_RESPUESTA,CG_PREGUNTA,IG_NUM_OPCIONES,CS_ACTIVO)" +
											  "	VALUES(?,?,?,?,?)";
						varBind = new ArrayList();
					    varBind.add(clave_preunta);
						varBind.add(fpTipoResp);
						varBind.add(fpPregunta);
						varBind.add(fpNumOpc);
						varBind.add(ESTADO);
					    
					    ps = conexBD.queryPrecompilado(SQLPregunta, varBind);
					    ps.executeUpdate();
					    ps.clearParameters();
					    ps.close();
					    if(existe_respuesta.equals("N")&&DatosResp.size()>0){
							 guardaRespuestaEnc(conexBD,DatosResp,num_real_preg,"S",clave_preunta,fpTipoResp);
						}
					               
					        
					}
				
				} //for(int j=0;j<lsPreguntas.length;j++)
			} //end if()

		} catch (Exception e) {
			lbSinError = false;
			throw new AppException("guardaPreguntaGral(Error): Error al guardar preguntas de la encuesta", e);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				
			    if (rs1 != null)
			        rs1.close();
			    if (ps1 != null)
			        ps1.close();
			} catch (Exception t) {
				log.error("guardaPreguntaGral():: Error al cerrar Recursos: " + t.getMessage());
			}
			log.debug("CediEJB::guardaPreguntaGral(S)");
		}
	}

	private void guardaRespuestaEnc(AccesoDB con, List lstRespuestas, String num_real_preg ,String preg_eliminada,String icpregunta ,String fpTipoResp) {
			log.info("ServiciosEJB::guardaRespuesta(E)");
			PreparedStatement ps = null;
			ResultSet rs = null;
			String SQLPregunta = "";
			try {
				HashMap dataRespuestas = null;

				String IC_RESPUESTA = "", IC_PREG = "", TIPO_RESP = "", LIBRE_TIPO_DATO = "", LIBRE_LONG = "", INF_OPC_RESP =
							 "", OPCION_OTROS = "", PREG_EXC = "", CS_ACTIVO = "", RESP_ABIERTA_ASO = "", LIBRE_OBLIG = "", IC_RESP =
							 "";

				List varBind = new ArrayList();
				int num_re_preg = Integer.parseInt(num_real_preg);
			
				for (int x = 0; x < lstRespuestas.size(); x++) {
						dataRespuestas = (HashMap) lstRespuestas.get(x);
						IC_RESPUESTA = (String) dataRespuestas.get("IC_RESPUESTA");
						IC_PREG = (String) dataRespuestas.get("IC_PREG");
						TIPO_RESP = (String) dataRespuestas.get("TIPO_RESP");
						LIBRE_TIPO_DATO = (String) dataRespuestas.get("LIBRE_TIPO_DATO");
						LIBRE_LONG = (String) dataRespuestas.get("LIBRE_LONG");
						INF_OPC_RESP = (String) dataRespuestas.get("INF_OPC_RESP");
						OPCION_OTROS = (String) dataRespuestas.get("OPCION_OTROS");
						PREG_EXC = (String) dataRespuestas.get("PREG_EXC");
						if (TIPO_RESP.equals("2")) {
							RESP_ABIERTA_ASO = (String) dataRespuestas.get("RESP_ABIERTA_ASO");
						} else if (TIPO_RESP.equals("3")) {
							LIBRE_OBLIG = (String) dataRespuestas.get("LIBRE_OBLIG");
							RESP_ABIERTA_ASO = (String) dataRespuestas.get("RESP_ABIERTA_ASO");
						} else {
							LIBRE_OBLIG = (String) dataRespuestas.get("LIBRE_OBLIG");
						}
						CS_ACTIVO = (String) dataRespuestas.get("CS_ACTIVO");
						IC_RESP = (String) dataRespuestas.get("IC_RESP");
						log.debug("IC_RESP   -- > " + IC_RESP);
					    log.debug("TIPO_RESP   -- > " + TIPO_RESP);
						String existe_respuesta = "N";
						if(!IC_RESP.equals("")){
							String queryExisteResp =
											   " SELECT DECODE(COUNT(*),0,'N','S' ) AS EXISTE_RESPUESTA FROM cedi_respuesta_parametrizacion " +
											   "  WHERE IC_PREGUNTA = ?" + "  AND   IC_TIPO_RESPUESTA =?" +
											   "    AND IC_RESPUESTA_PARAMETRIZACION = ?";
							varBind = new ArrayList();
		
							varBind.add(IC_PREG);
							varBind.add(fpTipoResp);
							varBind.add(IC_RESP);
							
							log.debug("queryExisteResp -- " + queryExisteResp + "   varBind -- " + varBind);
		
							ps = con.queryPrecompilado(queryExisteResp, varBind);
							rs = ps.executeQuery();
							
							if (rs.next()) {
								existe_respuesta = rs.getString("EXISTE_RESPUESTA") == null ? "" :
												   rs.getString("EXISTE_RESPUESTA");
							}
							rs.close();
							ps.close();
						}
						if (existe_respuesta.equals("S")&&!IC_RESP.equals("")&&preg_eliminada.equals("N")) {

							String queryComplementario = "";

							if (TIPO_RESP.equals("2")) {
								if(!LIBRE_LONG.equals("")){ 
									LIBRE_LONG = LIBRE_LONG.replace(',','.');
															   
								}
								queryComplementario = "  CS_RESPUESTA_ABIERTA_ASOC = ?,";
							} else if (TIPO_RESP.equals("3")) {
								queryComplementario = "  CS_LIBRE_OBLIGATORIA =  ?, CS_RESPUESTA_ABIERTA_ASOC =  ?,";
							} else {
							    if(!LIBRE_LONG.equals("")){ 
							        LIBRE_LONG = LIBRE_LONG.replace(',','.');
							                                   
							    }
								queryComplementario = "  CS_LIBRE_OBLIGATORIA =  ?,";
							}

							SQLPregunta = " UPDATE cedi_respuesta_parametrizacion " + " SET CG_OPCION_RESPUESTA = ?," +
												  " CG_PREGUNTAS_EXCLUIR = ?," + queryComplementario + "    CS_LIBRE_TIPO_VALIDACION =  ?," +
												  " IG_LIBRE_LONGITUD_MAXIMA = ?," + "  CS_ACTIVO =  ?," + "    CS_OPCION_OTROS =  ?" +
												  " WHERE  IC_RESPUESTA_PARAMETRIZACION = ?" + "    AND IC_PREGUNTA = ?" +
												  " AND IC_TIPO_RESPUESTA = ?";
							varBind = new ArrayList();
							varBind.add(INF_OPC_RESP);
							varBind.add(PREG_EXC);
							if (TIPO_RESP.equals("2")) {
								varBind.add(RESP_ABIERTA_ASO);
							} else if (TIPO_RESP.equals("3")) {
								varBind.add(LIBRE_OBLIG);
								varBind.add(RESP_ABIERTA_ASO);
							} else {
								varBind.add(LIBRE_OBLIG);
							}
							varBind.add(LIBRE_TIPO_DATO);
							varBind.add(LIBRE_LONG);
							varBind.add(CS_ACTIVO);
							varBind.add(OPCION_OTROS);

							varBind.add(IC_RESP);
							varBind.add(icpregunta);
							varBind.add(fpTipoResp);

						} else {
							String queryComplementario = "";
							String complemento ="";
						    if (TIPO_RESP.equals("2")) {
						        if(!LIBRE_LONG.equals("")){ 
						            LIBRE_LONG = LIBRE_LONG.replace(',','.');
						                                       
						        }
						        queryComplementario = "  CS_RESPUESTA_ABIERTA_ASOC";
						        complemento = "?";
						    } else if (TIPO_RESP.equals("3")) {
						        queryComplementario = "  CS_LIBRE_OBLIGATORIA,CS_RESPUESTA_ABIERTA_ASOC";
						        complemento = "?,?";
						    } else {
						        if(!LIBRE_LONG.equals("")){ 
						            LIBRE_LONG = LIBRE_LONG.replace(',','.');
						                                       
						        }
						        queryComplementario = "  CS_LIBRE_OBLIGATORIA";
						        complemento = "?";
						    }
							SQLPregunta = "  INSERT INTO cedi_respuesta_parametrizacion (IC_RESPUESTA_PARAMETRIZACION,IC_PREGUNTA,IC_TIPO_RESPUESTA,CG_OPCION_RESPUESTA,CG_PREGUNTAS_EXCLUIR,"+queryComplementario+",CS_LIBRE_TIPO_VALIDACION,IG_LIBRE_LONGITUD_MAXIMA,CS_ACTIVO,CS_OPCION_OTROS)" +
												  " VALUES((SELECT decode((MAX (ic_respuesta_parametrizacion) + 1),'',1,(MAX (ic_respuesta_parametrizacion) + 1)) FROM cedi_respuesta_parametrizacion)" + 
												  "               ,?,?,?,?,?,?,?,?,"+complemento+")  ";
							if (TIPO_RESP.equals("2")||TIPO_RESP.equals("1")) {
								if(!LIBRE_LONG.equals("")){ 
									LIBRE_LONG = LIBRE_LONG.replace(',','.');
															   
								}
							}
						    
							varBind = new ArrayList();
							varBind.add(icpregunta);
							varBind.add(fpTipoResp);
							varBind.add(INF_OPC_RESP);
							varBind.add(PREG_EXC);
						    if (TIPO_RESP.equals("2")) {
						        varBind.add(RESP_ABIERTA_ASO);
						    } else if (TIPO_RESP.equals("3")) {
						        varBind.add(LIBRE_OBLIG);
						        varBind.add(RESP_ABIERTA_ASO);
						    } else {
						        varBind.add(LIBRE_OBLIG);
						    }
							varBind.add(LIBRE_TIPO_DATO);
							varBind.add(LIBRE_LONG);
							varBind.add(CS_ACTIVO);
							varBind.add(OPCION_OTROS);
						}
						log.debug("SQLPregunta "+x+"-- " + SQLPregunta + "----varBind -- " + varBind);

						ps = con.queryPrecompilado(SQLPregunta, varBind);
						ps.executeUpdate();
						ps.clearParameters();
						ps.close();


					}

			} catch (Throwable t) {
				throw new AppException("Error al guardar la paramterizacion de respuestas para la encuesta", t);
			} finally {
				log.info("ServiciosEJB::guardaRespuesta(S)");
			}
		}


	/**
	 * Metodo que obtiene los documentos de la solicitud
	 * @param ic_solicitud
	 * @return
	 * @throws AppException
	 */
	public List getDoctosCheckListSolicitud(String ic_solicitud) {
		log.info("getDoctosCheckListSolicitud(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		boolean exito = true;
		List datos = new ArrayList();
		String nombreIF = "", nombreDocto = "";

		try {

			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" SELECT d.CG_NOMBRE  as  NOMBRE_DOCUMENTO  " + "  FROM  cedi_solicitud s,  " +
						  " CEDI_DOCUMENTO_CHECKLIST d , CEDI_ARCHIVO_CHECKLIST  a  " +
						  " where d.IC_DOCUMENTO_CHECKLIST  = a.IC_DOCUMENTO_CHECKLIST " + " and s.IC_SOLICITUD =  a. IC_SOLICITUD    " +
						  " and  d.CS_ACTIVO = 'S'   " + " and s.IC_SOLICITUD=  " + ic_solicitud +
						  " order by  d.IC_DOCUMENTO_CHECKLIST  asc  ");

			log.debug("strSQL " + strSQL.toString());

			ps = con.queryPrecompilado(strSQL.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				nombreDocto = rs.getString("NOMBRE_DOCUMENTO") == null ? "" : rs.getString("NOMBRE_DOCUMENTO");
				datos.add(nombreDocto);
			}
			ps.close();
			rs.close();


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("getDoctosCheckListSolicitud  " + e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getDoctosCheckListSolicitud (S) ");
			}
		}
		return datos;
	}


	/**
	 *
	 * @param request
	 * @param parametros
	 * @return
	 * @throws AppException
	 */
	public boolean cambioEstatusConfirma(HashMap parametros) {
		log.info("cambioEstatusConfirma(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
		boolean exito = true;
		String estatus = "";

		try {

			con.conexionDB();

			String no_solicitud = parametros.get("NO_SOLICITUD").toString();
			String pantalla = parametros.get("PANTALLA").toString();


			if (pantalla.equals("PreAnalisis_Aceptada")) {
				estatus = "4";
			} else if (pantalla.equals("PreAnalisis_Rechazada")) {
				estatus = "8";
			} else if (pantalla.equals("Seguimiento_Aceptada")) {
				estatus = "6";
			} else if (pantalla.equals("Seguimiento_Rechazada")) {
				estatus = "9";
			}

			strSQL = new StringBuffer();
			strSQL.append(" UPDATE  CEDI_SOLICITUD  " + " SET  IC_ESTATUS_SOLICITUD  =  ?  " +
						  " WHERE IC_SOLICITUD  =   ?    ");

			lVarBind = new ArrayList();
			lVarBind.add(estatus);
			lVarBind.add(no_solicitud);

			log.debug("strSQL  " + strSQL);
			log.debug("lVarBind  " + lVarBind);

			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();


			envioCorreo(parametros);


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("cambioEstatusConfirma  " + e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  cambioEstatusConfirma (S) ");
			}
		}
		return exito;
	}

	/**
	 *Pantalla Atenci�n de Solicitud  Pre-Analisis y Seguimiento
	 * @param request
	 * @param parametros
	 * @return
	 */
	public String generarPDFCorreo(HttpServletRequest request, HashMap parametros) {
		log.info("generarPDFCorreo(E)");
		String nombreArchivo = "";
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		HttpSession session = request.getSession();

		String path = parametros.get("RUTA").toString();
		String pantalla = parametros.get("PANTALLA").toString();

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(ComunesPDF.HOJA_VERTICAL, path + nombreArchivo);
			//pdfDoc = new ComunesPDF(ComunesPDF.HOJA_VERTICAL,   path + nombreArchivo, "Pagina ", false, false, true);

			pdfDoc.encabezadoConImagenes(pdfDoc, (String) session.getAttribute("strPais"),
										 ((session.getAttribute("iNoNafinElectronico") == null) ? "" :
										  session.getAttribute("iNoNafinElectronico").toString()), (String) session.getAttribute("sesExterno"),
										 (String) session.getAttribute("strNombre"), (String) session.getAttribute("strNombreUsuario"),
										 (String) session.getAttribute("strLogo"),
										 (String) session.getServletContext().getAttribute("strDirectorioPublicacion"));

			String no_solicitud = parametros.get("NO_SOLICITUD").toString();
			String lbFechaActual = parametros.get("LBFECHAACTUAL").toString();
			String txtObservacionesCEDI = parametros.get("OBSERVACIONES").toString();
			String nombreUsuario = parametros.get("NOMBREUSUARIO").toString();


			String lbNombreIFNB = "", lbListaDoctos = "", lbTitulo = "", lbMensaje1 = "", lbMensaje2 = "", lbMensaje3 =
						 "", lbMensaje4 = "", lbMensaje5 = "";

			HashMap datosIF = this.getDatosIF(no_solicitud); // datos del Usuario
			lbNombreIFNB = datosIF.get("NOMBRE_IF").toString();

			List doctos = this.getDoctosCheckListSolicitud(no_solicitud); //documentos de la  solcitud
			int total = doctos.size();
			for (int i = 0; i < total; i++) {
				lbListaDoctos += " * " + doctos.get(i).toString() + "\n";

			}


			if (pantalla.equals("PreAnalisis_Aceptada")) {

				lbTitulo = "Seguimiento de Pre-An�lisis";

				lbMensaje1 = "Agradecemos su inter�s por formar parte de la red de intermediarios Financieros de NAFINSA. \n " +
								" \n Por este medio se hace de su conocimiento que, para continuar  con el pre-an�lisis  de su solicitud se requiere la siguiente documentaci�n, lo cual permitir� validar su viabilidad de incorporaci�n a NAFINSA:";

				lbMensaje2 = " Ingrese nuevamente a la herramienta electr�nica de  NAFINET , en el men�  �Seguimiento Pre-an�lisis� para enviar los documentos antes mencionados. \n " +
								" \n Folio de Solicitud: " + no_solicitud + "\n" + " \n Observaciones Ejecutivo CEDI";


			} else if (pantalla.equals("PreAnalisis_Rechazada")) {

				lbTitulo = " Seguimiento de Pre-An�lisis  - Solicitud Rechazada ";

				lbMensaje1 = " Agradecemos su inter�s por formar parte de la red de Intermediarios Financieros de NAFINSA." +
								"\n Por este medio se hace de su conocimiento que, de acuerdo con la solicitud y la informaci�n que proporcion� mediante nuestra herramienta electr�nica, la viabilidad de incorporaci�n a Nacional Financiera, S.N.C. no es satisfactoria por lo siguiente:";


				lbMensaje3 += " Esperamos contar con su inter�s en otro momento.  \n";

			} else if (pantalla.equals("Seguimiento_Aceptada")) {

				lbTitulo = "Solicitud Aceptada";


				lbMensaje1 = " \n Agradecemos su inter�s por formar parte de la red de Intermediarios Financieros de NAFINSA." +
								" \n \n Por este medio se le informa que su solicitud, con n�mero de Folio " + no_solicitud +
								", es viable para su incorporaci�n a Nacional Financiera, S.N.C." +
								" \n \n En breve un Ejecutivo se comunicar� con usted para continuar con el procedimiento. " +
								" \n \n Adicionalmente, se le proporciona un Dictamen que registrar� la Viabilidad de su solicitud anexo a este correo, con el objeto de integrar el expediente necesario para iniciar su tr�mite de incorporaci�n a la red e Intermediarios de NAFINSA. ";

				lbMensaje4 = "\n  Observaciones Ejecutivo CEDI: ";

			} else if (pantalla.equals("Seguimiento_Rechazada")) {

				lbTitulo = "Solicitud Rechazada";

				lbMensaje1 = " \n Agradecemos su inter�s por formar parte de la red de Intermediarios Financieros de NAFINSA. " +
								" \n \n Por este medio se hace de su conocimiento que, de acuerdo con la solicitud y la informaci�n que proporcion� mediante nuestra herramienta electr�nica, su solicitud de incorporaci�n a la red de Intermediarios de Nacional Financiera, S.N.C. no es viable, por lo siguiente :";


				lbMensaje4 = "\n \n Adicionalmente, se le proporciona un Dictamen que detalla los motivos por los que su solicitud no resulto Viable anexo a este correo, con el objeto de integrar el expediente necesario para iniciar su tr�mite de incorporaci�n a la red de Intermediarios de NAFINSA. ";

				lbMensaje3 += " Esperamos contar con su inter�s en otro momento.  \n";

			}

			lbMensaje3 +=
						 "\n Sin otro particular, reciba un cordial saludo. \n" +
						 " Para cualquier duda o aclaraci�n favor de contactarnos al 01 800 NAFINSA (623 4672). \n " +
						 " \n ATENTAMENTE " + " \n " + nombreUsuario + " \n ";

			lbMensaje5 +=
						 " Con fundamento en el art�culo 142 de la Ley de Instituciones de Cr�dito, 14 fracci�n I y 15 de la Ley Federal de Transparencia y Acceso a la informaci�n P�blica Gubernamental, as� como al art�culo 30 de su reglamento, el contenido del presente mensaje de correo electr�nico es de car�cter Reservado. ";


			pdfDoc.addText(lbTitulo, "formas", ComunesPDF.CENTER);
			pdfDoc.addText(" ", "formas", ComunesPDF.LEFT);

			pdfDoc.addText(lbFechaActual, "formas", ComunesPDF.RIGHT);
			pdfDoc.addText("", "formas", ComunesPDF.LEFT);

			pdfDoc.addText(lbNombreIFNB, "formas", ComunesPDF.LEFT);
			pdfDoc.addText("", "formas", ComunesPDF.LEFT);

			pdfDoc.addText(lbMensaje1, "formas", ComunesPDF.LEFT);
			pdfDoc.addText("", "formas", ComunesPDF.LEFT);

			if (pantalla.equals("PreAnalisis_Aceptada")) {
				pdfDoc.addText(lbListaDoctos, "formas", ComunesPDF.LEFT);
				pdfDoc.addText("", "formas", ComunesPDF.LEFT);


				pdfDoc.addText(lbMensaje2, "formas", ComunesPDF.LEFT);
				pdfDoc.addText("", "formas", ComunesPDF.LEFT);

			}

			if (pantalla.equals("Seguimiento_Rechazada")) {
				pdfDoc.addText(lbMensaje4, "formas", ComunesPDF.LEFT);
				pdfDoc.addText("", "formas", ComunesPDF.LEFT);
			}

			if (pantalla.equals("Seguimiento_Aceptada")) {
				pdfDoc.addText(lbMensaje4, "formas", ComunesPDF.LEFT);
				pdfDoc.addText("", "formas", ComunesPDF.LEFT);
			}


			pdfDoc.addText(txtObservacionesCEDI, "formas", ComunesPDF.LEFT);
			pdfDoc.addText("", "formas", ComunesPDF.LEFT);

			pdfDoc.addText(lbMensaje3, "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);
			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);


			pdfDoc.addText(lbMensaje5, "formas", ComunesPDF.JUSTIFIED);  
			// lbMensaje5 =" Con fundamento en el art�culo 142 de la Ley de Instituciones de Cr�dito, 14 fracci�n I y 15 de la Ley Federal de Transparencia y \n Acceso a la informaci�n P�blica Gubernamental, as� como al art�culo 30 de su reglamento, el \n contenido del presente mensaje de correo electr�nico es de car�cter Reservado. ";


			// pdfDoc.setFooter(lbMensaje5, true, "JUSTIFIED"); 

			pdfDoc.endDocument();


		} catch (Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			log.info("generarPDFCorreo(S)");
		}
		return nombreArchivo;
	}


	private void envioCorreo(HashMap parametros) {

		log.info("envioCorreo(E)");

		try {

			Correo correo = new Correo();
			UtilUsr utilUsr = new UtilUsr();

			StringBuffer contenido = new StringBuffer();

			ArrayList listaDeImagenes = null;
			ArrayList listaDeArchivos = new ArrayList();
			HashMap archivoAdjunto = new HashMap();
			String rutaArchivo = "";

			String no_ifnb = parametros.get("no_ifnb").toString();
			String no_solicitud = parametros.get("NO_SOLICITUD").toString();
			String lbFechaActual = parametros.get("LBFECHAACTUAL").toString();
			String txtObservacionesCEDI = parametros.get("OBSERVACIONES").toString();
			String nombreUsuario = parametros.get("NOMBREUSUARIO").toString().replaceAll("\n", "<br></br>");
			String pantalla = parametros.get("PANTALLA").toString();
			String pdfViabilidad = parametros.get("pdfViabilidad").toString();
			String path = parametros.get("path").toString();


			HashMap datos = getDatosIF(no_solicitud);
			String correoPara = datos.get("EMAIL").toString();
			String lbNombreIFNB = datos.get("NOMBRE_IF").toString();


			String lbListaDoctos = "", lbTitulo = "", lbMensaje1 = "", lbMensaje2 = "", lbMensaje3 = "", lbMensaje4 =
						 "";
			List doctos = this.getDoctosCheckListSolicitud(no_solicitud);

			lbListaDoctos = "<UL type = disk >";
			for (int i = 0; i < doctos.size(); i++) {
				lbListaDoctos += "<LI>" + doctos.get(i).toString() + "</LI>";

			}
			lbListaDoctos += "</UL> ";


			if (pantalla.equals("PreAnalisis_Aceptada")) {

				lbTitulo = "Seguimiento de Pre-An�lisis";

				lbMensaje1 = "Agradecemos su inter�s por formar parte de la red de intermediarios Financieros de NAFINSA. <br></br> " +
								" Por este medio, se hace de su conocimiento que para continuar  con el pre-an�lisis de su solicitud se requiere la siguiente documentaci�n, lo cual permitir� validar su viabilidad de incorporaci�n a NAFINSA:";

				lbMensaje2 = " Ingrese nuevamente a la herramienta electr�nica de  NAFINET , en el men�  'Seguimiento Pre-an�lisis' para enviar los documentos antes mencionados. " +
								" <br></br> Folio de Solicitud: " + no_solicitud + " <br></br> Observaciones Ejecutivo CEDI" + " <br></br>" +
								txtObservacionesCEDI;

			} else if (pantalla.equals("PreAnalisis_Rechazada")) {

				lbTitulo = " Seguimiento de Pre-An�lisis  - Solicitud Rechazada ";

				lbMensaje1 = " Agradecemos su inter�s por formar parte de la red de Intermediarios Financieros de NAFINSA." +
								" <br></br> Por este medio se hace de su conocimiento que, de acuerdo con la solicitud y la informaci�n que proporcion� mediante nuestra herramienta electr�nica, la viabilidad de incorporaci�n a Nacional Financiera, S.N.C. no es satisfactoria por lo siguiente:" +
								" <br></br>" + txtObservacionesCEDI + " <br></br>";

				lbMensaje3 += " Esperamos contar con su inter�s en otro momento.";

			} else if (pantalla.equals("Seguimiento_Aceptada")) {
				lbTitulo = "Solicitud Aceptada";


				lbMensaje1 = " <br></br> Agradecemos su inter�s por formar parte de la red de Intermediarios Financieros de NAFINSA." +
								" <br></br> Por este medio se le informa que su solicitud, con n�mero de Folio " + no_solicitud +
								", es viable para su incorporaci�n a Nacional Financiera, S.N.C." +
								" <br></br> En breve un Ejecutivo se comunicar� con usted para continuar con el procedimiento. " +
								" <br></br> Adicionalmente, se le proporciona un Dictamen que registrar� la Viabilidad de su solicitud anexo a este correo, con el objeto de integrar el expediente necesario para iniciar su tr�mite de incorporaci�n a la red e Intermediarios de NAFINSA. ";

				lbMensaje4 = " <br></br>  Observaciones Ejecutivo CEDI: " + " <br></br>" + txtObservacionesCEDI +
								" <br></br>";

			} else if (pantalla.equals("Seguimiento_Rechazada")) {

				lbTitulo = "Solicitud Rechazada";


				lbMensaje1 = " <br></br> Agradecemos su inter�s por formar parte de la red de Intermediarios Financieros de NAFINSA. " +
								"  <br></br> Por este medio se hace de su conocimiento que, de acuerdo con la solicitud y la informaci�n que proporcion� mediante nuestra herramienta electr�nica, su solicitud de incorporaci�n a la red de Intermediarios de Nacional Financiera, S.N.C. no es viable, por lo siguiente :" +
								"  <br></br>" + txtObservacionesCEDI + " <br></br>";

				lbMensaje4 = "<br></br> Adicionalmente, se le proporciona un Dictamen que detalla los motivos por los que su solicitud no resulto Viable anexo a este correo, con el objeto de integrar el expediente necesario para iniciar su tr�mite de incorporaci�n a la red de Intermediarios de NAFINSA. ";

				lbMensaje3 += " Esperamos contar con su inter�s en otro momento.";

			}

			lbMensaje3 +=
						 "<br></br> Sin otro particular, reciba un cordial saludo. " +
						 " <br></br> Para cualquier duda o aclaraci�n favor de contactarnos al 01 800 NAFINSA (623 4672).  " +
						 " <br></br> ATENTAMENTE " + " <br></br> " + nombreUsuario +
						 " <br></br><br></br><br></br> Con fundamento en el art�culo 142 de la Ley de Instituciones de Cr�dito, 14 fracci�n I y 15 de la Ley Federal de Transparencia y Acceso a la informaci�n P�blica Gubernamental, as� como al art�culo 30 de su reglamento, el contenido del presente mensaje de correo electr�nico es de car�cter Reservado. ";

			contenido.append("<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>" +
							 "<table width='800' align='left' border='0'>" +
							 " <tr> <td align='center' width='200'  style='font-size:11pt;font-family: Calibri'>" + " <H4> " + lbTitulo +
							 " </H4>" + "</td> </tr> " +
							 " <tr> <td align='right' width='200'  style='font-size:11pt;font-family: Calibri'>" + lbFechaActual +
							 "</td> </tr> " + " <tr> <td align='LEFT' width='200'  style='font-size:11pt;font-family: Calibri'> <B>" + lbNombreIFNB + "</B><br></br> </td> </tr> " +
							
							 " <tr> <td align='JUSTIFIED' width='200'  style='font-size:11pt;font-family: Calibri'>" + lbMensaje1 +
							 "</td> </tr> ");

			if (pantalla.equals("PreAnalisis_Aceptada")) {
				contenido.append(" <tr> <td align='LEFT' width='200'  style='font-size:11pt;font-family: Calibri'>" +
								 lbListaDoctos + "</td> </tr> ");
			}

			if (pantalla.equals("PreAnalisis_Aceptada")) {
				contenido.append(" <tr> <td align='JUSTIFIED' width='200'  style='font-size:11pt;font-family: Calibri'>" +
								 lbMensaje2 + "</td> </tr> ");
			}


			if (pantalla.equals("Seguimiento_Rechazada")) {
				contenido.append(" <tr> <td align='JUSTIFIED' width='200'  style='font-size:11pt;font-family: Calibri'>" +
								 lbMensaje4 + "</td> </tr> ");
			}

			if (pantalla.equals("Seguimiento_Aceptada")) {
				contenido.append(" <tr> <td align='JUSTIFIED' width='200'  style='font-size:11pt;font-family: Calibri'>" +
								 lbMensaje4 + "</td> </tr> ");
			}

			contenido.append(" <tr> <td align='LEFT' width='200'  style='font-size:11pt;font-family: Calibri'>" + lbMensaje3 +
							 "</td> </tr> " +
							 " <tr> <td align='LEFT' width='200'  style='font-size:11pt;font-family: Calibri'> <br></br> </td> </tr> " +
							 " <tr> <td align='LEFT' width='200'  style='font-size:11pt;font-family: Calibri'> <br></br> </td> </tr> " +
							 " </td></tr></table>");

		
			if (!pdfViabilidad.equals("")) {
				rutaArchivo = path + pdfViabilidad;
				archivoAdjunto.put("FILE_FULL_PATH", rutaArchivo);
				listaDeArchivos.add(archivoAdjunto);

				log.debug("listaDeArchivos  " + listaDeArchivos);

				correo.setCodificacion("charset=UTF-8");
				correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", correoPara, "", lbTitulo, contenido.toString(),
												   listaDeImagenes, listaDeArchivos);

			} else {
				correo.setCodificacion("charset=UTF-8");
				correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", correoPara, "", lbTitulo, contenido.toString(), null,
												   null);
			}

		} catch (Throwable e) {
			throw new AppException("Error al envioCorreo", e);
		} finally {
			log.info("envioCorreo(S)");
		}

	}

	/**
	 * Metodo apra obtener los datos del Intermrediario
	 * @param solicitud
	 * @return
	 */
	public HashMap getDatosIF(String solicitud) {
		log.info("getDatosIF(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		boolean exito = true;
		String email = "";
		HashMap datos = new HashMap();

		try {

			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" select A.CG_RAZON_SOCIAL as NOMBRE_IF, " + " a.CG_RFC as RFC, " + " a.CG_EMAIL as EMAIL " +
						  " FROM  CEDI_IFNB_ASPIRANTE a, CEDI_SOLICITUD s " + " where a.IC_IFNB_ASPIRANTE = s.IC_IFNB_ASPIRANTE " +
						  " and  IC_SOLICITUD = " + solicitud);


			log.debug("strSQL " + strSQL.toString());

			ps = con.queryPrecompilado(strSQL.toString());
			rs = ps.executeQuery();
			if (rs.next()) {
				datos = new HashMap();
				datos.put("NOMBRE_IF", rs.getString("NOMBRE_IF") == null ? "" : rs.getString("NOMBRE_IF"));
				datos.put("RFC", rs.getString("RFC") == null ? "" : rs.getString("RFC"));
				datos.put("EMAIL", rs.getString("EMAIL") == null ? "" : rs.getString("EMAIL"));

			}
			ps.close();
			rs.close();


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("getDatosIF  " + e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getDatosIF (S) ");
			}
		}
		return datos;
	}

	/**
	 *Descarga de los arhivos Chek List
	 * @param no_solicitud
	 * @param strDirectorioTemp
	 * @param ic_doc_chekList
	 * @param extension
	 * @return
	 */
	public String desArchSolicCheckList(String no_solicitud, String strDirectorioTemp, String ic_doc_chekList,
										String extension) {
		log.info("desArchSolicCheckList(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "", nombreArchivo = "";

		try {
			con.conexionDB();

			strSQL.append(" SELECT bi_archivo ,  CG_EXTENSION ");
			strSQL.append(" FROM cedi_archivo_checklist ");
			strSQL.append(" WHERE IC_SOLICITUD = ? ");
			strSQL.append(" and  ic_documento_checklist   = ? ");

			varBind.add(no_solicitud);
			varBind.add(ic_doc_chekList);

			log.debug(strSQL.toString() + "   " + varBind);


			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			if (rst.next()) {

				extension = rst.getString("CG_EXTENSION") == null ? "" : rst.getString("CG_EXTENSION");

				nombreArchivo = Comunes.cadenaAleatoria(8) + "." + extension;
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob("bi_archivo");
				InputStream inStream = blob.getBinaryStream();
				int size = (int) blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1) {
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();


		} catch (Exception e) {
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("desArchSolicCheckList(S)");
		}
		return nombreArchivo;
	}


	/**
	 *IF /Cedi - Seguimiento Pre-an�lisis
	 * m�todo que descarga los archivos previos
	 * @param no_solicitud
	 * @param strDirectorioTemp
	 * @param ic_doc_chekList
	 * @param extension
	 * @return
	 */
	public String desArchSolicPrevios(String no_solicitud, String strDirectorioTemp, String ic_doc_chekList,
									  String extension) {
		log.info("desArchSolicPrevios(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "", nombreArchivo = "";

		try {
			con.conexionDB();


			strSQL.append(" SELECT BI_ARCHIVO_PRE ,  CG_EXTENSION_PRE ");
			strSQL.append(" FROM cedi_archivo_checklist ");
			strSQL.append(" WHERE IC_SOLICITUD = ? ");
			strSQL.append(" and  ic_documento_checklist   = ? ");

			varBind.add(no_solicitud);
			varBind.add(ic_doc_chekList);

			log.debug(strSQL.toString() + "   " + varBind);


			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			if (rst.next()) {

				extension = rst.getString("CG_EXTENSION_PRE") == null ? "" : rst.getString("CG_EXTENSION_PRE");

				nombreArchivo = Comunes.cadenaAleatoria(8) + "." + extension;
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob("BI_ARCHIVO_PRE");
				InputStream inStream = blob.getBinaryStream();
				int size = (int) blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1) {
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();


		} catch (Exception e) {
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("descargaArchivosSolic(S)");
		}
		return nombreArchivo;
	}


	/**
	 * METODO QUE ME REGRESA LOS DOCUMETOS QUE LE SON REQUERIDOS A LA SOLICITUD
	 * @param solicitud
	 *  @param fijo   N== No es fijo  y S = es un archivo Fijo
	 * @return
	 */
	public List getCheckList(String solicitud, String csFijo) {
		log.info("getCheckList(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		boolean exito = true;
		HashMap datos = new HashMap();
		List reg = new ArrayList();
		List lVarBind = new ArrayList();

		try {

			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" SELECT LENGTH (bi_archivo) as LONGITUD , CG_EXTENSION  ,  " +
						  " d.IC_DOCUMENTO_CHECKLIST as  IC_CHECKLIST, " + " a.ic_solicitud AS NO_SOLICITUD,  " + " CG_NOMBRE as NOMBRE_DOCTO   " +
						
						  "          FROM cedi_documento_checklist d, " + "               cedi_archivo_checklist a " +
						  "         WHERE d.ic_documento_checklist = a.ic_documento_checklist" + "           AND a.ic_solicitud = ? " +
						  "           AND d.cs_activo = ?    " + "           AND d.CS_FIJO  = ?    ");

			lVarBind = new ArrayList();
			lVarBind.add(solicitud);
			lVarBind.add("S");
			lVarBind.add(csFijo);


			log.debug("strSQL " + strSQL.toString());
			log.debug("lVarBind " + lVarBind);

			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				datos = new HashMap();
				datos.put("LONGITUD", rs.getString("LONGITUD") == null ? "" : rs.getString("LONGITUD"));
				datos.put("CG_EXTENSION", rs.getString("CG_EXTENSION") == null ? "" : rs.getString("CG_EXTENSION"));
				datos.put("IC_CHECKLIST", rs.getString("IC_CHECKLIST") == null ? "" : rs.getString("IC_CHECKLIST"));
				datos.put("NOMBRE_DOCTO", rs.getString("NOMBRE_DOCTO") == null ? "" : rs.getString("NOMBRE_DOCTO"));
				datos.put("NO_SOLICITUD", rs.getString("NO_SOLICITUD") == null ? "" : rs.getString("NO_SOLICITUD"));
				reg.add(datos);
			}
			ps.close();
			rs.close();


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("getCheckList  " + e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getCheckList (S) ");
			}
		}

		return reg;
	}

	//Guardar Achivo solicitado al IFNB
	public boolean guardarArchivoSolic(String no_solicitud, String ic_doc_chekList, String rutaArchivo) {
		log.info("guardarArchivoSolic (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte abyte0[] = new byte[4096];
		java.io.OutputStream outstream = null;


		try {
			con.conexionDB();


			String extension = Comunes.getExtensionNombreArchivo(rutaArchivo);
			File archivo = new File(rutaArchivo);
			FileInputStream fileinputstream = new FileInputStream(archivo);

			String strSQL =
						 " UPDATE CEDI_ARCHIVO_CHECKLIST " + " SET  BI_ARCHIVO_PRE  = ?,  " + " CG_EXTENSION_PRE  = ? ,  " +
						 " CS_VISTA_PREVIA  =  ?  " + " WHERE IC_SOLICITUD = ? " + " and   IC_DOCUMENTO_CHECKLIST = ? ";

			log.debug("strSQL " + strSQL);
			log.debug("extension " + extension + "   no_solicitud " + no_solicitud + " ic_doc_chekList " +
					  ic_doc_chekList);

			ps = con.queryPrecompilado(strSQL);
			ps.setBinaryStream(1, fileinputstream, (int) archivo.length());
			ps.setString(2, extension);
			ps.setString(3, "N");
			ps.setString(4, no_solicitud);
			ps.setString(5, ic_doc_chekList);
			ps.executeUpdate();
			ps.close();


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("guardarArchivoSolic  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  guardarArchivoSolic (S) ");
			}
		}
		return exito;
	}


	/**
	 *
	 * @param no_solicitud
	 * @param ic_doc_chekList
	 * @return
	 */
	public boolean enviarArchivo(String no_solicitud, String ic_doc_chekList[], String  estatus, String strLogin ) {
		log.info("enviarArchivo (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;

		ResultSet rs = null;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
	   PreparedStatement ps = null;
	   String  movimiento ="6"; //Doctos. Enviados al CEDI
		try {
			con.conexionDB();
		   if (estatus.equals("4")) {
			   movimiento ="7"; //Reenv�o de Doctos al CEDI.
			}

			for (int i = 0; i < ic_doc_chekList.length; i++) {

				strSQL = new StringBuffer();
				strSQL.append(" UPDATE CEDI_ARCHIVO_CHECKLIST " + " SET   CS_VISTA_PREVIA  =  ?, " +
							  " DF_ULTIMO_ENVIO = Sysdate , " +
							  " bi_archivo = (select  bi_archivo_pre from cedi_archivo_checklist WHERE ic_solicitud = ?  AND ic_documento_checklist = ?  ) ,  " +
							  " cg_extension = (select  cg_extension_pre from cedi_archivo_checklist WHERE ic_solicitud =  ?  AND ic_documento_checklist =  ? )	" +
							  " WHERE IC_SOLICITUD = ? " + " and IC_DOCUMENTO_CHECKLIST  = ? ");
       		
				lVarBind = new ArrayList();
				lVarBind.add("S");
				lVarBind.add(no_solicitud);
				lVarBind.add(ic_doc_chekList[i]);
				lVarBind.add(no_solicitud);
				lVarBind.add(ic_doc_chekList[i]);
				lVarBind.add(no_solicitud);
				lVarBind.add(ic_doc_chekList[i]);


				log.debug("strSQL " + strSQL.toString());
				log.debug("lVarBind " + lVarBind);

				ps = null;
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();
				
				//se limpian las variabeles temporales 
			   strSQL = new StringBuffer();
			   strSQL.append(" UPDATE CEDI_ARCHIVO_CHECKLIST " +
					" SET   bi_archivo_pre  =  '', " +
			      " CG_EXTENSION_PRE  =  '' " +
					" WHERE IC_SOLICITUD = ? " + 
				   " and IC_DOCUMENTO_CHECKLIST  = ? ");

			   lVarBind = new ArrayList();		  
			   lVarBind.add(no_solicitud);
			   lVarBind.add(ic_doc_chekList[i]);	
				
			   log.debug("strSQL " + strSQL.toString());
			   log.debug("lVarBind " + lVarBind);

			   ps = null;
			   ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			   ps.executeUpdate();
			   ps.close();

			  
				//Movimientos
			   this.setMovimientoSolicitus( no_solicitud, strLogin, estatus, movimiento, "") ; 

			}

			if (estatus.equals("4")) {
				strSQL = new StringBuffer();
				strSQL.append(" Update CEDI_SOLICITUD " + "  set  IC_ESTATUS_SOLICITUD = ?  " +
								  "  where IC_SOLICITUD = ? ");

				lVarBind = new ArrayList();
				lVarBind.add("5");
				lVarBind.add(no_solicitud);

				log.debug("strSQL " + strSQL.toString());
				log.debug("lVarBind " + lVarBind);

				ps = null;
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();

			}

		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("enviarArchivo  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  enviarArchivo (S) ");
			}
		}
		return exito;
	}

	// METODOS DEL F013-2015***********************************
	// 					F013-2015
	//************************************************************

	/**
	 * M�todo que nos regresara un HashMap con la parametrizaci�n de las preguntas de Cedi.
	 * @return
	 * @throws AppException
	 */

	public Map getPreguntasIFNB() {
		log.info("CediEJB::getPreguntasIFNB(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		PreparedStatement psR = null;
		ResultSet rsR = null;

		HashMap hmInfPreguntas = new HashMap();
		HashMap hmResp = new HashMap();
		try {
			con.conexionDB();
			String qryConsulta =
						 " select IC_PREGUNTA,IC_TIPO_RESPUESTA,CG_PREGUNTA,IG_NUM_OPCIONES, CS_ACTIVO  " +
						 " from cedi_pregunta" + "	ORDER BY IC_PREGUNTA ASC	";
			ps = con.queryPrecompilado(qryConsulta);
			rs = ps.executeQuery();

			HashMap hmParamPreguntas = null;
			List lstPreg = new ArrayList();
			while (rs.next()) {
				hmParamPreguntas = new HashMap();
				hmParamPreguntas.put("ICPREGUNTA",
									 rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
				hmParamPreguntas.put("CGPREGUNTA",
									 rs.getString("CG_PREGUNTA") == null ? "" : rs.getString("CG_PREGUNTA"));
				hmParamPreguntas.put("TIPORESP",
									 rs.getString("IC_TIPO_RESPUESTA") == null ? "" :
									 rs.getString("IC_TIPO_RESPUESTA"));
				hmParamPreguntas.put("IGORDENPREG",
									 rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
				hmParamPreguntas.put("IGNUMPREG",
									 rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
				hmParamPreguntas.put("IGNUMOPC",
									 rs.getString("IG_NUM_OPCIONES") == null ? "" : rs.getString("IG_NUM_OPCIONES"));
				hmParamPreguntas.put("ICESTADO", rs.getString("CS_ACTIVO") == null ? "" : rs.getString("CS_ACTIVO"));
				lstPreg.add(hmParamPreguntas);
				String qryConsultaRes = " select IC_RESPUESTA_PARAMETRIZACION AS IC_RESP, IC_PREGUNTA,  IC_TIPO_RESPUESTA AS TIPO_RESP, CG_OPCION_RESPUESTA AS OPC_RESP, CG_PREGUNTAS_EXCLUIR AS PREG_EXC, DECODE(CG_PREGUNTAS_EXCLUIR,'','N','S') AS BAN_EXCLUIR,CS_LIBRE_OBLIGATORIA AS LIBRE_OBLIG, CS_RESPUESTA_ABIERTA_ASOC AS RESP_ABIERTA_ASO, CS_LIBRE_TIPO_VALIDACION AS LIBRE_TIPO_DATO,IG_LIBRE_LONGITUD_MAXIMA AS LIBRE_LONG,  CS_ACTIVO, CS_OPCION_OTROS AS OPCION_OTROS " +
								" from cedi_respuesta_parametrizacion" + " where IC_PREGUNTA = " +rs.getString("IC_PREGUNTA")+
								 "	order by IC_RESPUESTA_PARAMETRIZACION asc";
			    log.debug("qryConsultaRes :::  " + qryConsultaRes);
				psR = con.queryPrecompilado(qryConsultaRes);
				rsR = psR.executeQuery();
				
				Map mpDataResp = new HashMap();
				List lstRespPreg = new ArrayList();

				while (rsR.next()) {
					mpDataResp = new HashMap();
					mpDataResp.put("IC_RESP", rsR.getString("IC_RESP") == null ? "" : rsR.getString("IC_RESP"));
					mpDataResp.put("IC_PREGUNTA",
								   rsR.getString("IC_PREGUNTA") == null ? "" : rsR.getString("IC_PREGUNTA"));
					mpDataResp.put("TIPO_RESP", rsR.getString("TIPO_RESP") == null ? "" : rsR.getString("TIPO_RESP"));
					mpDataResp.put("OPC_RESP", rsR.getString("OPC_RESP") == null ? "" : rsR.getString("OPC_RESP"));
					mpDataResp.put("PREG_EXC", rsR.getString("PREG_EXC") == null ? "" : rsR.getString("PREG_EXC"));
					mpDataResp.put("BAN_EXCLUIR",
								   rsR.getString("BAN_EXCLUIR") == null ? "" : rsR.getString("BAN_EXCLUIR"));

					mpDataResp.put("LIBRE_OBLIG",
								   rsR.getString("LIBRE_OBLIG") == null ? "" : rsR.getString("LIBRE_OBLIG"));
					mpDataResp.put("RESP_ABIERTA_ASO",
								   rsR.getString("RESP_ABIERTA_ASO") == null ? "N" : rsR.getString("RESP_ABIERTA_ASO"));
					mpDataResp.put("LIBRE_TIPO_DATO",
								   rsR.getString("LIBRE_TIPO_DATO") == null ? "" : rsR.getString("LIBRE_TIPO_DATO"));
					mpDataResp.put("LIBRE_LONG",
								   rsR.getString("LIBRE_LONG") == null ? "" : rsR.getString("LIBRE_LONG"));
					mpDataResp.put("CS_ACTIVO", rsR.getString("CS_ACTIVO") == null ? "" : rsR.getString("CS_ACTIVO"));
					mpDataResp.put("OPCION_OTROS",
								   rsR.getString("OPCION_OTROS") == null ? "N" : rsR.getString("OPCION_OTROS"));
					lstRespPreg.add(mpDataResp);
				}
				rsR.close();
				psR.close();
				hmResp.put("p" + rs.getString("IC_PREGUNTA"), lstRespPreg);
			}
			rs.close();
			ps.close();
			hmInfPreguntas.put("INFOPARAMETRIZACION", lstPreg);

			hmInfPreguntas.put("INFO_RESPUESTAS", hmResp);

			qryConsulta = "";
			qryConsulta = " select count(*) as preg_iniciales, max(IC_PREGUNTA) as id_preg_max " + " from cedi_pregunta ";
			ps = con.queryPrecompilado(qryConsulta);
			rs = ps.executeQuery();
			if (rs.next()) {
				hmInfPreguntas.put("TOTAL_PREGUNTAS", rs.getString("PREG_INICIALES"));
			    hmInfPreguntas.put("ID_PREG_MAX", rs.getString("ID_PREG_MAX"));
			}
			rs.close();
			ps.close();


		} catch (Throwable t) {
			throw new AppException("getPreguntasIFNB(Error): Error al consultar informaci�n de las preguntas", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getPreguntasIFNB():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getPreguntasIFNB(S)");
		}

		return hmInfPreguntas;
	}


	/*
	    * Descarg los archivo de Balance , resultados y RFc que se encuentran en la base de datos
	    */

	public String descargaArchivosSolic(String no_solicitud, String strDirectorioTemp, String tipoArchivo) {
		log.info("descargaArchivosSolic(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "", nombreArchivo = "", campo = "";

		try {
			con.conexionDB();

			if (tipoArchivo.equals("balance")) {
				campo = "BI_BALANCE";
			}
			if (tipoArchivo.equals("resultados")) {
				campo = "BI_EDO_RESULTADOS";
			}
			if (tipoArchivo.equals("rfc")) {
				campo = "BI_RFC";
			}


			strSQL.append(" SELECT " + campo);
			strSQL.append(" FROM cedi_solicitud ");
			strSQL.append(" WHERE IC_SOLICITUD = ? ");
			varBind.add(no_solicitud);

			log.debug(strSQL.toString() + "   " + varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			if (rst.next()) {
				nombreArchivo = Comunes.cadenaAleatoria(8) + ".pdf";
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob(campo);
				if (blob == null) {
					log.info("El objeto blob es nulo, se regresa el nombre del archivo vacio.");
					return "";
				}
				InputStream inStream = blob.getBinaryStream();
				int size = (int) blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1) {
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();


		} catch (Exception e) {
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("descargaArchivosSolic(S)");
		}
		//return pathname;
		return nombreArchivo;
	}

	/**
	 * Genera la solicitud de alta de usuario al sistema ARGUS
	 * Si los datos estan incompletos, genera un error.
	 * Los datos para generar la solicitud los toma de los par�metros del
	 * request
	 * @param request request de la p�gina
	 */
	public SolicitudArgus generarSolicitudArgus(HttpServletRequest request) {
		Usuario usr = new Usuario();
		SolicitudArgus solicitud = new SolicitudArgus();

		String tipoAfiliado = request.getParameter("tipo_afiliado");

		String txtRazon = request.getParameter("txtRazon");
		String txtRFC = request.getParameter("txtRFC");
		String txtConfRFC = request.getParameter("txtConfRFC");
		String nombre = request.getParameter("nombre");
		String txtApellidoP = request.getParameter("txtApellidoP");
		String txtApellidoM = request.getParameter("txtApellidoM");
		String txtEmail = request.getParameter("txtEmail");
		String txtConmail = request.getParameter("txtConmail");
		String perfil = "IF CEDI";
		String claveAfiliado = perfil;
		String pregunta_secreta = "cuando";
		String respuesta_secreta = "ayer  ";


		usr.setNombre(nombre);
		usr.setApellidoPaterno(txtApellidoP);
		usr.setApellidoMaterno(txtApellidoM);
		usr.setEmail(txtEmail);
		usr.setClaveAfiliado(claveAfiliado);
		usr.setTipoAfiliado(tipoAfiliado);
		usr.setPerfil(perfil);

		solicitud.setUsuario(usr);
		/*if (pregunta_secreta != null && !pregunta_secreta.equals("")) {
	            solicitud.setPreguntaSecreta(pregunta_secreta);
	         }
	         if (respuesta_secreta != null  && !respuesta_secreta.equals("")) {
	            solicitud.setRespuestaSecreta(respuesta_secreta);
	         }  */
		UtilUsr utilUsr = new UtilUsr();
		String mensajeError = "";
		try {
			mensajeError = utilUsr.complementarSolicitudArgus(solicitud, tipoAfiliado, claveAfiliado);
		} catch (Exception e) {
		}
		if (!mensajeError.equals("")) { //Si se genera algun mensaje, debido a que algun campo de la consulta fue nulo, telefono, cp, etc
			try {
				throw new Exception(mensajeError);
			} catch (Exception e) {
			}
		}
		return solicitud;
	}

	/**
	 *Funci�n que se usa para saber si ya existe alg�n usuario.
	 * @param rfc
	 * @return
	 */
	public boolean getExisteUsuarioIFCEDI(String rfc) {
		log.info("CediEJB::getExisteUsuarioIFCEDI(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		boolean existeUsuario = false;

		try {
			con.conexionDB();

			String qryConsulta =
						 " SELECT DECODE(COUNT (CG_RFC),0,'N','S') as existe FROM cedi_ifnb_aspirante " +
						 " where CG_RFC = TRIM(?) ";
			varBind = new ArrayList();
			varBind.add(rfc);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				String aux = rs.getString("EXISTE");
				if (aux.equals("S")) {
					existeUsuario = true;
				}

			}
			log.debug(" resultado1111 :: " + existeUsuario);
			rs.close();
			ps.close();


		} catch (Throwable t) {
			throw new AppException("getExisteUsuarioIFCEDI(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getExisteUsuarioIFCEDI():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getExisteUsuarioIFCEDI(S)");
		}

		return existeUsuario;
	}

	/**
	 *M�doto que guarda la informaci�n del formulario de Registro
	 * @param razonSoc es la raz�n social del usuario que se registro
	 * @param rfc es el rfc que se registro en el formilario regstro
	 * @param usuario es el usuario con el cual se realizo el registro del usuario
	 * @return
	 */
	public boolean guardaDatosIF(String razonSoc, String rfc, String usuario, String existeRFC) {
		log.info("guardaDatosIF(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//PreparedStatement ps1 = null;
		List varBind = new ArrayList();
		String qry = "";
		String qryId = "";
		String claveAsp = "";
		String claveSolic = "";
		String querySEQ = "";

		String IC_SOLICITUD = "";
		String IC_ESTATUS_SOLICITUD = "1";

		try {
			con.conexionDB();
			if (existeRFC.equals("S")) {
				qryId = " SELECT asp.IC_IFNB_ASPIRANTE AS IC_ASPIRANTE,sol.IC_SOLICITUD , sol.IC_ESTATUS_SOLICITUD  " +
								"	FROM cedi_ifnb_aspirante asp, cedi_solicitud sol" + "	WHERE asp.IC_IFNB_ASPIRANTE = sol.IC_IFNB_ASPIRANTE" +
								"	AND CG_RFC = TRIM(?)";
				varBind = new ArrayList();
				varBind.add(rfc);
				log.debug("	qryIdExist ::: " + qryId);
				log.debug(" varBindExist ::: " + varBind);
				ps = con.queryPrecompilado(qryId, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					claveAsp = rs.getString("IC_ASPIRANTE");
					claveSolic = rs.getString("IC_SOLICITUD");
					IC_ESTATUS_SOLICITUD = rs.getString("IC_ESTATUS_SOLICITUD");
				}
				rs.close();
				ps.close();
			} else {

				querySEQ = "select SEQ_CEDI_IFNB_ASPIRANTE.NEXTVAL from dual";

				ps = con.queryPrecompilado(querySEQ);
				rs = ps.executeQuery();
				if (rs.next()) {
					claveAsp = rs.getString(1);
				}
				rs.close();
				ps.close();

				qry = "";
				qry = " INSERT INTO cedi_ifnb_aspirante (IC_IFNB_ASPIRANTE,CG_RAZON_SOCIAL,CG_RFC)" +
								"	VALUES(?,?,?)";
				varBind = new ArrayList();
				varBind.add(claveAsp);
				varBind.add(razonSoc);
				varBind.add(rfc);
				log.debug("qryAsp11:: " + qry);
				log.debug("varBind11 :: " + varBind);
				ps = con.queryPrecompilado(qry, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();

				querySEQ = "";
				querySEQ = "select SEQ_CEDI_SOLICITUD.NEXTVAL from dual";
				ps = con.queryPrecompilado(querySEQ);
				rs = ps.executeQuery();
				if (rs.next()) {
					claveSolic = rs.getString(1);
				}
				rs.close();
				ps.close();

				qry = "";
				qry = " INSERT INTO cedi_solicitud (IC_SOLICITUD,IC_IFNB_ASPIRANTE,IC_ESTATUS_SOLICITUD,DF_ALTA)" +
								" VALUES(?,?,?,sysdate)";
				varBind = new ArrayList();
				varBind.add(claveSolic);
				varBind.add(claveAsp);
				varBind.add("1");
				log.debug("qrySolicitud11 :: " + qry);
				log.debug("varBind11 :: " + varBind);
				ps = con.queryPrecompilado(qry, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();


			}

			qry = "";
			qry = " INSERT INTO cedi_solicitud_movimiento (IC_SOLICITUD_CAMBIO_EST,IC_SOLICITUD,DF_MOVIMIENTO,IC_ESTATUS_SOLICITUD,IC_MOVIMIENTO)" +
						 " VALUES((SELECT decode((MAX (ic_solicitud_cambio_est) + 1),'',1,(MAX (ic_solicitud_cambio_est) + 1)) FROM cedi_solicitud_movimiento),?,sysdate,?,?)";
			varBind = new ArrayList();
			varBind.add(claveSolic);
			varBind.add(IC_ESTATUS_SOLICITUD);
			varBind.add("1");
			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
		} catch (Throwable t) {
			exito = false;
			throw new AppException("getPreguntasIFNB(Error): Error al consultar informaci�n de las preguntas", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getPreguntasIFNB():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		return exito;
	}

	public Map getDatosInicialesCalificate(String rfc) {
		log.info("CediEJB::getDatosInicialesCalificate(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		Map hmInf = new HashMap();
		try {
			Map mpInfInic = new HashMap();
			List lstInf = new ArrayList();
			con.conexionDB();
			// ========== PARA OBTENER LA INFORMACION DEL ASPIRANTE ====================
			String qryConsulta =
						 "  SELECT IC_IFNB_ASPIRANTE AS IC_ASPIRANTE,CG_RAZON_SOCIAL AS RAZON,CG_NOMBRE_COMERCIAL AS NOMCOM, CG_RFC  AS RFC, CG_CALLE,CG_COLONIA, IC_PAIS, IC_ESTADO, IC_MUNICIPIO, CG_CP,CG_CONTACTO,CG_EMAIL, CG_TELEFONO from cedi_ifnb_aspirante " +
						 " WHERE CG_RFC = TRIM(?)";
			varBind = new ArrayList();
			varBind.add(rfc);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				mpInfInic = new HashMap();
				mpInfInic.put("IC_ASPIRANTE", rs.getString("IC_ASPIRANTE") == null ? "" : rs.getString("IC_ASPIRANTE"));
				mpInfInic.put("RAZON", rs.getString("RAZON") == null ? "" : rs.getString("RAZON"));
				mpInfInic.put("RFC", rs.getString("RFC") == null ? "" : rs.getString("RFC"));
				mpInfInic.put("NOMCOM", rs.getString("NOMCOM") == null ? "" : rs.getString("NOMCOM"));
				mpInfInic.put("CG_CALLE", rs.getString("CG_CALLE") == null ? "" : rs.getString("CG_CALLE"));
				mpInfInic.put("CG_COLONIA", rs.getString("CG_COLONIA") == null ? "" : rs.getString("CG_COLONIA"));
				mpInfInic.put("IC_PAIS", rs.getString("IC_PAIS") == null ? "" : rs.getString("IC_PAIS"));
				mpInfInic.put("IC_ESTADO", rs.getString("IC_ESTADO") == null ? "" : rs.getString("IC_ESTADO"));
				mpInfInic.put("IC_MUNICIPIO", rs.getString("IC_MUNICIPIO") == null ? "" : rs.getString("IC_MUNICIPIO"));
				mpInfInic.put("CG_CP", rs.getString("CG_CP") == null ? "" : rs.getString("CG_CP"));
				mpInfInic.put("CG_CONTACTO", rs.getString("CG_CONTACTO") == null ? "" : rs.getString("CG_CONTACTO"));
				mpInfInic.put("CG_EMAIL", rs.getString("CG_EMAIL") == null ? "" : rs.getString("CG_EMAIL"));
				mpInfInic.put("CG_TELEFONO", rs.getString("CG_TELEFONO") == null ? "" : rs.getString("CG_TELEFONO"));

				lstInf.add(mpInfInic);
			}
			hmInf.put("INFORMACION_INICIAL", lstInf);
			rs.close();
			ps.close();

		} catch (Throwable t) {
			throw new AppException("getDatosInicialesCalificate(Error): Error al consultar informaci�n inicial", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getDatosInicialesCalificate():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getDatosInicialesCalificate(S)");
		}
		return hmInf;

	}

	/**
	 * Funci�n que guarda la informaci�n faltante de la solicitud
	 * @param solicitud
	 * @param nomCom
	 * @param CG_CONTACTO
	 * @param CG_EMAIL
	 * @param ic_aspirante
	 * @return
	 */
	public boolean completarInfoAsp(SolicitudArgus solicitud, String razonSocial,String nomCom, String CG_CONTACTO, String CG_EMAIL,
									String ic_aspirante, String IC_SOLICITUD, String usuario, String estatus) {
		log.info("completarInfoAsp(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";
		
	    boolean actualizaMov = true;
	    String existe_rechazo = this.existeRechazoMovimientos(IC_SOLICITUD);
	    int existe_rechazo_aux = 0;
	    if(existe_rechazo.equals("")){
	        existe_rechazo_aux = 0;
	    }else{
	        existe_rechazo_aux = Integer.parseInt(existe_rechazo);
	    }
	    if(existe_rechazo_aux>0){
	        actualizaEstatusSolicitud(IC_SOLICITUD,"1");
	        boolean actualiza = this.actualizaDictamenConfirmacion(IC_SOLICITUD,"N");
	    }
		try {
			con.conexionDB();
			qry = "";
			qry = "	UPDATE cedi_ifnb_aspirante " +
				  "	SET CG_NOMBRE_COMERCIAL = ?, " + 
				  "   CG_RAZON_SOCIAL = ?, " + 
				  "	CG_CALLE = ?, " + 
				  "	CG_COLONIA = ?, " +
				  "	IC_PAIS = ?, " +
				  " 	IC_ESTADO = ?," +
				  "	IC_MUNICIPIO = ?, " +
				  " 	CG_CP = ?, " +
				  " 	CG_CONTACTO = ?, " +
						 "	CG_EMAIL = ?, " + 
				  " 	CG_TELEFONO  = ? " +
				  " 	WHERE IC_IFNB_ASPIRANTE = ?";
			varBind = new ArrayList();
			varBind.add(nomCom);
		    varBind.add(razonSocial);
			varBind.add(solicitud.getDireccion());
			varBind.add(solicitud.getColonia());
			varBind.add(solicitud.getPais());
			varBind.add(solicitud.getEstado());
			varBind.add(solicitud.getMunicipio());
			varBind.add(solicitud.getCodigoPostal());
			varBind.add(CG_CONTACTO);
			varBind.add(CG_EMAIL);
			varBind.add(solicitud.getTelefono());
			varBind.add(ic_aspirante);
			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
			// Actualiza el estatus de la  solicitud
			if(existe_rechazo_aux>0){
			    actualizaMov = this.setMovimientoSolicitus(con, IC_SOLICITUD, usuario, "1", "11","");
			}else{
			    actualizaMov = this.setMovimientoSolicitus(con, IC_SOLICITUD, usuario, "1", "2","");
			}

		} catch (Throwable t) {
			exito = false;
			throw new AppException("completarInfoAsp(Error): Error al guardar la informaci�n en cedi_ifnb_aspirante",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("completarInfoAsp():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		return exito;
	}

	/**
	 *Funci�n que nos regresa la clave de la solicitud
	 * @param clave es la clave del aspirante
	 * @return
	 */
	public String getClaveSolicitud(String clave) {
		log.info("CediEJB::getClaveSolicitud(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String IC_SOLICITUD = "";
		try {
			con.conexionDB();

			String qryConsulta =
						 " select max(IC_SOLICITUD) IC_SOLICITUD from cedi_solicitud" + " where IC_IFNB_ASPIRANTE =  ?";
			varBind = new ArrayList();
			varBind.add(clave);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				IC_SOLICITUD = rs.getString("IC_SOLICITUD");


			}
			log.debug(" IC_SOLICITUD :: " + IC_SOLICITUD);
			rs.close();
			ps.close();


		} catch (Throwable t) {
			throw new AppException("getClaveSolicitud(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getClaveSolicitud():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getClaveSolicitud(S)");
		}

		return IC_SOLICITUD;
	}

	/**
	 * Funci�n que guarda la informaci�n cualitativa
	 * @param ic_solicitud
	 * @param lstRespuestas
	 * @return
	 */
	public boolean actualizarInfoCualitativa(String ic_solicitud, List lstRespuestas, String usuario, String estatus) {
		log.info("actualizarInfoCualitativa(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";
		HashMap dataRespuestas = null;
		String IC_RESP = "";
		String RESP_LIBRE = "";
		String nombre_otro = "";
		String opcion_resp_inicial = "";
		String num_opcion = "";
		String tipo_resp = "";
		
	    
	    String existe_rechazo = this.existeRechazoMovimientos(ic_solicitud);
	    int existe_rechazo_aux = 0;
	    if(existe_rechazo.equals("")){
	         existe_rechazo_aux = 0;
	    }else{
	         existe_rechazo_aux = Integer.parseInt(existe_rechazo);
	    }
	    if(existe_rechazo_aux>0){
			actualizaEstatusSolicitud(ic_solicitud,"1");
	        boolean actualiza = this.actualizaDictamenConfirmacion(ic_solicitud,"N");
	    }

		try {
			con.conexionDB();
			qry = "";
			qry = "  DELETE cedi_respuesta_ifnb" + "  where IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(ic_solicitud);
			log.debug(" qryDelete ::: " + qry);
			log.debug(" varBind ::: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
			if (lstRespuestas != null && lstRespuestas.size() > 0) {
				for (int x = 1; x <= lstRespuestas.size(); x++) {
					int p = x;
					dataRespuestas = (HashMap) lstRespuestas.get(p - 1);
					IC_RESP = (String) dataRespuestas.get("IC_RESP");
					RESP_LIBRE = (String) dataRespuestas.get("RESP_LIBRE");
					nombre_otro = (String) dataRespuestas.get("nombre_otro");
					opcion_resp_inicial = (String) dataRespuestas.get("resp_inicial");
					num_opcion = (String) dataRespuestas.get("num_opcion");
					tipo_resp = (String) dataRespuestas.get("tipo_resp");
					String existe = "";

					/*log.debug(" IC_RESP== "+IC_RESP+" RESP_LIBRE =="+RESP_LIBRE+
								 " nombre_otro =="+nombre_otro+" opcion_resp_inicial =="+opcion_resp_inicial+
								 " num_opcion =="+num_opcion+ " tipo_resp =="+tipo_resp);
					*/

					if (!IC_RESP.equals("")) {
						qry = " select decode(count(*), 0,'N','S') as existe " + " from cedi_respuesta_ifnb" +
											  " where IC_RESPUESTA_PARAMETRIZACION = ?" + " and IC_SOLICITUD =?";
						varBind = new ArrayList();
						varBind.add(IC_RESP);
						varBind.add(ic_solicitud);
						log.debug(" qryIdExist ::: " + qry + "   varBindExist ::: " + varBind);
						ps = con.queryPrecompilado(qry, varBind);
						rs = ps.executeQuery();
						if (rs.next()) {
							existe = rs.getString("EXISTE");
						}
						rs.close();
						ps.close();
						qry = "";
						if (existe.equals("S")) {
							qry = "	UPDATE cedi_respuesta_ifnb" + "	SET CG_RESPUESTA_LIBRE = ?," + "	CG_NOMBRE_OTRO =?" +
													 "	where IC_SOLICITUD = ?" +
													 "	and IC_RESPUESTA_PARAMETRIZACION = ?";
							varBind = new ArrayList();
							varBind.add(RESP_LIBRE);
							varBind.add(nombre_otro);
							varBind.add(ic_solicitud);
							varBind.add(IC_RESP);
						} else {
							qry = "  INSERT INTO cedi_respuesta_ifnb(IC_SOLICITUD,IC_RESPUESTA_PARAMETRIZACION,CG_RESPUESTA_LIBRE,CG_NOMBRE_OTRO)" +
													 "  VALUES(?,?,?,?)";
							varBind = new ArrayList();
							varBind.add(ic_solicitud);
							varBind.add(IC_RESP);
							varBind.add(RESP_LIBRE);
							varBind.add(nombre_otro);

						}
						log.debug(" qryIdExist ::: " + qry + "   varBindExist ::: " + varBind);
						ps = con.queryPrecompilado(qry, varBind);
						ps.executeUpdate();
						ps.clearParameters();
						ps.close();
					} else {
						qry = "";
						qry = "  DELETE cedi_respuesta_ifnb" + "  WHERE IC_RESPUESTA_PARAMETRIZACION = ?" +
											  "  AND  IC_SOLICITUD = ?";
						varBind = new ArrayList();
						varBind.add(IC_RESP);
						varBind.add(ic_solicitud);
						log.debug(" qryDelete ::: " + qry);
						log.debug(" varBind ::: " + varBind);
						ps = con.queryPrecompilado(qry, varBind);
						ps.executeUpdate();
						ps.clearParameters();
						ps.close();


					}

				}

			}
			
			
			boolean actualizaMov = true;
			//*** Cambia estatus de la solicitud verificando s�, es la primera o segunda vuelta, tambi�n inserya el movimiento realizado
			
			if(existe_rechazo_aux>0){
			  actualizaMov = this.setMovimientoSolicitus(con, ic_solicitud, usuario, "1", "11","");
			}else{
			  actualizaMov = this.setMovimientoSolicitus(con, ic_solicitud, usuario, "1", "2","");
			}
			
			
		} catch (Throwable t) {
			exito = false;
			throw new AppException("actualizarInfoCualitativa(Error): Error al guardar la informaci�n", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("actualizarInfoCualitativa():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		return exito;
	}

	/**
	 *Funci�n que obtiene la informacion de inicializaci�n de la pantalla Calif�cate
	 * @param rfc parametro que se usa para obtener la clave del aspirante para as� saber el n�mero de solicitud
	 * @return
	 */
	public Map getInformacionInicialCalificate(String rfc, String pestania) {
		log.info("CediEJB::getInformacionInicialCalificate(E) " + rfc);

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		PreparedStatement psR = null;
		ResultSet rsR = null;

		PreparedStatement psChk = null;
		ResultSet rsChk = null;

		HashMap hmInfPreguntas = new HashMap();
		HashMap hmResp = new HashMap();
		List varBind = new ArrayList();
		String ic_solicitus = "";
		String estatus_solicitud = "";
		int EXISTE_INFO_P2 = 0;
		int EXISTE_INFO_P3 = 0;
		int EXISTE_INFO_P4 = 0;
		int EXISTE_INFO_P5 = 0;
		int EXISTE_INFO_P6 = 0;

		try {
			con.conexionDB();

			String qryConsulSol =
						 "    select MAX(ic_solicitud ) AS IC_SOLICITUD, IC_ESTATUS_SOLICITUD" +
						 "	from cedi_ifnb_aspirante cia, cedi_solicitud cs" + "	where cia.IC_IFNB_ASPIRANTE = cs.IC_IFNB_ASPIRANTE" +
						 "	and CG_RFC = TRIM(?)" +
						 "   AND  ic_solicitud   = (select MAX(ic_solicitud ) AS IC_SOLICITUD from cedi_ifnb_aspirante cia, cedi_solicitud cs  where cia.IC_IFNB_ASPIRANTE = cs.IC_IFNB_ASPIRANTE and CG_RFC = TRIM(?))" +
						 "	GROUP BY IC_ESTATUS_SOLICITUD";

			varBind = new ArrayList();
			varBind.add(rfc);
		    varBind.add(rfc);
			 log.debug("qryConsulta ::: "+qryConsulSol  +"   varBind :: "+varBind);
			ps = con.queryPrecompilado(qryConsulSol, varBind);
			rs = ps.executeQuery();
			String ic_solicitud_ant = "";
		    String ic_solicitud_nueva = "";
			if (rs.next()) {
				ic_solicitus = rs.getString("IC_SOLICITUD") == null ? "" : rs.getString("IC_SOLICITUD");
				estatus_solicitud =
								rs.getString("IC_ESTATUS_SOLICITUD") == null ? "" :
								rs.getString("IC_ESTATUS_SOLICITUD");
			}
			rs.close();
			ps.close();
		    ic_solicitud_ant = ic_solicitus;
			
			if (estatus_solicitud.equals("9")) {
				ic_solicitus = this.generaNuevoFolioAspirante(rfc);
			    guardarInfoNuevaSolicitud(ic_solicitud_ant,ic_solicitus);
			}
			
			hmInfPreguntas.put("CLAVE_SOLICITUD", ic_solicitus);
		    
			
		    // Revisar el estatus de la solicitud
		    String qryConsulta = "";
		    qryConsulta = " select IC_ESTATUS_SOLICITUD from cedi_solicitud " + " where IC_SOLICITUD = ?";
		    varBind = new ArrayList();
		    varBind.add(ic_solicitus);
		    ps = con.queryPrecompilado(qryConsulta, varBind);
		    rs = ps.executeQuery();
		    if (rs.next()) {
		        hmInfPreguntas.put("IC_ESTATUS_SOLICITUD", rs.getString("IC_ESTATUS_SOLICITUD"));
		    }
		    rs.close();
		    ps.close();
//******************
			qryConsulta = "";
			if(pestania.equals("infCualitativa")){
				qryConsulta =
							 " select IC_PREGUNTA,IC_TIPO_RESPUESTA,CG_PREGUNTA,IG_NUM_OPCIONES, CS_ACTIVO  " +
							 " from cedi_pregunta" + " ORDER BY IC_PREGUNTA ASC";
				ps = con.queryPrecompilado(qryConsulta);
				rs = ps.executeQuery();
	
				HashMap hmParamPreguntas = null;
				List lstPreg = new ArrayList();
				while (rs.next()) {
					hmParamPreguntas = new HashMap();
					hmParamPreguntas.put("ICPREGUNTA",
										 rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
					hmParamPreguntas.put("CGPREGUNTA",
										 rs.getString("CG_PREGUNTA") == null ? "" : rs.getString("CG_PREGUNTA"));
					hmParamPreguntas.put("TIPORESP",
										 rs.getString("IC_TIPO_RESPUESTA") == null ? "" :
										 rs.getString("IC_TIPO_RESPUESTA"));
					hmParamPreguntas.put("IGORDENPREG",
										 rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
					hmParamPreguntas.put("IGNUMPREG",
										 rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
					hmParamPreguntas.put("IGNUMOPC",
										 rs.getString("IG_NUM_OPCIONES") == null ? "" : rs.getString("IG_NUM_OPCIONES"));
					hmParamPreguntas.put("ICESTADO", rs.getString("CS_ACTIVO") == null ? "" : rs.getString("CS_ACTIVO"));
					lstPreg.add(hmParamPreguntas);
					String qryConsultaRes =
									" select IC_RESPUESTA_PARAMETRIZACION AS IC_RESP, IC_PREGUNTA,  IC_TIPO_RESPUESTA AS TIPO_RESP, CG_OPCION_RESPUESTA AS OPC_RESP, CG_PREGUNTAS_EXCLUIR AS PREG_EXC, DECODE(CG_PREGUNTAS_EXCLUIR,'','N','S') AS BAN_EXCLUIR,CS_LIBRE_OBLIGATORIA AS LIBRE_OBLIG, CS_RESPUESTA_ABIERTA_ASOC AS RESP_ABIERTA_ASO, CS_LIBRE_TIPO_VALIDACION AS LIBRE_TIPO_DATO,IG_LIBRE_LONGITUD_MAXIMA AS LIBRE_LONG,  CS_ACTIVO, CS_OPCION_OTROS AS OPCION_OTROS " +
									" from cedi_respuesta_parametrizacion" + " where IC_PREGUNTA = " +
									rs.getString("IC_PREGUNTA");
					psR = con.queryPrecompilado(qryConsultaRes);
					rsR = psR.executeQuery();
					log.debug("qryConsultaRes :::  "+qryConsultaRes);
					Map mpDataResp = new HashMap();
					List lstRespPreg = new ArrayList();
	
					while (rsR.next()) {
						mpDataResp = new HashMap();
						mpDataResp.put("IC_RESP", rsR.getString("IC_RESP") == null ? "" : rsR.getString("IC_RESP"));
						mpDataResp.put("IC_PREGUNTA",
									   rsR.getString("IC_PREGUNTA") == null ? "" : rsR.getString("IC_PREGUNTA"));
						mpDataResp.put("TIPO_RESP", rsR.getString("TIPO_RESP") == null ? "" : rsR.getString("TIPO_RESP"));
						mpDataResp.put("OPC_RESP", rsR.getString("OPC_RESP") == null ? "" : rsR.getString("OPC_RESP"));
						mpDataResp.put("PREG_EXC", rsR.getString("PREG_EXC") == null ? "" : rsR.getString("PREG_EXC"));
						mpDataResp.put("BAN_EXCLUIR",
									   rsR.getString("BAN_EXCLUIR") == null ? "" : rsR.getString("BAN_EXCLUIR"));
	
						mpDataResp.put("LIBRE_OBLIG",
									   rsR.getString("LIBRE_OBLIG") == null ? "" : rsR.getString("LIBRE_OBLIG"));
						mpDataResp.put("RESP_ABIERTA_ASO",
									   rsR.getString("RESP_ABIERTA_ASO") == null ? "N" : rsR.getString("RESP_ABIERTA_ASO"));
						mpDataResp.put("LIBRE_TIPO_DATO",
									   rsR.getString("LIBRE_TIPO_DATO") == null ? "" : rsR.getString("LIBRE_TIPO_DATO"));
						mpDataResp.put("LIBRE_LONG",
									   rsR.getString("LIBRE_LONG") == null ? "" : rsR.getString("LIBRE_LONG"));
						mpDataResp.put("CS_ACTIVO", rsR.getString("CS_ACTIVO") == null ? "" : rsR.getString("CS_ACTIVO"));
						mpDataResp.put("OPCION_OTROS",
									   rsR.getString("OPCION_OTROS") == null ? "N" : rsR.getString("OPCION_OTROS"));
	
						String qryCheckOpcion =
										   " SELECT IC_SOLICITUD,CG_RESPUESTA_LIBRE,CG_NOMBRE_OTRO, DECODE(COUNT(IC_SOLICITUD),'0','N','S') AS CHECK_OPCION_RESP" +
										   " FROM cedi_respuesta_ifnb" + " WHERE IC_RESPUESTA_PARAMETRIZACION =?" + " AND IC_SOLICITUD = ?" +
										   " GROUP BY IC_SOLICITUD,CG_RESPUESTA_LIBRE,CG_NOMBRE_OTRO";
						varBind = new ArrayList();
						varBind.add(rsR.getString("IC_RESP") == null ? "" : rsR.getString("IC_RESP"));
						varBind.add(ic_solicitus);
	
						log.debug("qryCheckOpcion :::  "+qryCheckOpcion+"  varBind :::  "+varBind);
	
						psChk = con.queryPrecompilado(qryCheckOpcion, varBind);
						rsChk = psChk.executeQuery();
						if (rsChk.next()) {
							mpDataResp.put("IC_SOLICITUD",
										   rsChk.getString("IC_SOLICITUD") == null ? "" : rsChk.getString("IC_SOLICITUD"));
							mpDataResp.put("CG_RESPUESTA_LIBRE",
										   rsChk.getString("CG_RESPUESTA_LIBRE") == null ? "" :
										   rsChk.getString("CG_RESPUESTA_LIBRE"));
							mpDataResp.put("CG_NOMBRE_OTRO",
										   rsChk.getString("CG_NOMBRE_OTRO") == null ? "" :
										   rsChk.getString("CG_NOMBRE_OTRO"));
							mpDataResp.put("CHECK_OPCION_RESP",
										   rsChk.getString("CHECK_OPCION_RESP") == null ? "N" :
										   rsChk.getString("CHECK_OPCION_RESP"));
							EXISTE_INFO_P2++;
						}
						psChk.close();
						rsChk.close();
						lstRespPreg.add(mpDataResp);
					   
						
						
					}
					rsR.close();
					psR.close();
					hmResp.put("p" + rs.getString("IC_PREGUNTA"), lstRespPreg);
					// EXISTE_INFO_P2++;
				}
				rs.close();
				ps.close();
				
			    qryConsulta = "";
			    qryConsulta = "";
			    qryConsulta = " select count(*) as preg_iniciales " + " from cedi_pregunta ";
			    ps = con.queryPrecompilado(qryConsulta);
			    rs = ps.executeQuery();
			    if (rs.next()) {
			        hmInfPreguntas.put("TOTAL_PREGUNTAS", rs.getString("PREG_INICIALES"));
			    }
			    rs.close();
			    ps.close();
				
				hmInfPreguntas.put("INFOPARAMETRIZACION", lstPreg);
				hmInfPreguntas.put("INFO_RESPUESTAS", hmResp);
				hmInfPreguntas.put("EXISTE_INFO_P2", String.valueOf(EXISTE_INFO_P2));
			} else if(pestania.equals("ceditoVinculado")){
			
				// Se obtienen datos iniciales para Cr�dito vinculado
				Map mpInfoCreditoVin = new HashMap();
				List lstCreditoVin = new ArrayList();
				qryConsulta = "";
				qryConsulta =
							 " SELECT CS_CREDITO_VINCULADO, FG_MONTO_CRED_VINCULADO, IC_SOLICITUD  " + " FROM cedi_solicitud " +
							 " WHERE IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(ic_solicitus);
				log.debug("qryConsulta ::: "+qryConsulta+"   varBind :: "+varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					mpInfoCreditoVin = new HashMap();
					String credito =
									rs.getString("CS_CREDITO_VINCULADO") == null ? "" :
									rs.getString("CS_CREDITO_VINCULADO");
					mpInfoCreditoVin.put("CS_CREDITO_VINCULADO",
										 rs.getString("CS_CREDITO_VINCULADO") == null ? "" :
										 rs.getString("CS_CREDITO_VINCULADO"));
					mpInfoCreditoVin.put("FG_MONTO_CRED_VINCULADO",
										 rs.getString("FG_MONTO_CRED_VINCULADO") == null ? "" :
										 rs.getString("FG_MONTO_CRED_VINCULADO"));
					mpInfoCreditoVin.put("IC_SOLICITUD",
										 rs.getString("IC_SOLICITUD") == null ? "" : rs.getString("IC_SOLICITUD"));
					lstCreditoVin.add(mpInfoCreditoVin);
				   
					if (!credito.equals("")) {
						EXISTE_INFO_P3++;
					}
				}
	
				rs.close();
				ps.close();
			    
				
				hmInfPreguntas.put("INFORMACION_CREDITO_VINCULADO", lstCreditoVin);
				hmInfPreguntas.put("EXISTE_INFO_P3", String.valueOf(EXISTE_INFO_P3));
			}else if(pestania.equals("infoFinanciera")){
				// Se obtienen datos iniciales Informaci�n Financiera
				Map mpInfoFinan = new HashMap();
				List lstSaldoFinan = new ArrayList();
				qryConsulta = "";
				qryConsulta =
							 " SELECT IG_ANIO_INF_FINANCIERA,IG_MES_INF_FINANCIERA  " + " FROM cedi_solicitud " +
							 " WHERE IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(ic_solicitus);
				 log.debug("qryConsulta ::: "+qryConsulta +"  varBind :: "+varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					mpInfoFinan = new HashMap();
					String anio_infoFinanciera =
									rs.getString("IG_ANIO_INF_FINANCIERA") == null ? "" :
									rs.getString("IG_ANIO_INF_FINANCIERA");
					mpInfoFinan.put("IG_ANIO_INF_FINANCIERA",
									rs.getString("IG_ANIO_INF_FINANCIERA") == null ? "" :
									rs.getString("IG_ANIO_INF_FINANCIERA"));
					mpInfoFinan.put("IG_MES_INF_FINANCIERA",
									rs.getString("IG_MES_INF_FINANCIERA") == null ? "" :
									rs.getString("IG_MES_INF_FINANCIERA"));
					lstSaldoFinan.add(mpInfoFinan);
					if (!anio_infoFinanciera.equals("")) {
						EXISTE_INFO_P4++;
					}
					
				    
				}
				rs.close();
				ps.close();
				hmInfPreguntas.put("INFORMACION_FINANCIERA", lstSaldoFinan);
				hmInfPreguntas.put("EXISTE_INFO_P4", String.valueOf(EXISTE_INFO_P4));
			}else if(pestania.equals("infoSaldos")){
			    // Se obtienen datos iniciales Informaci�n Financiera
			    Map mpInfoFinan = new HashMap();
			    List lstSaldoFinan = new ArrayList();
			    qryConsulta = "";
			    qryConsulta =
			                 " SELECT IG_ANIO_INF_FINANCIERA,IG_MES_INF_FINANCIERA  " + " FROM cedi_solicitud " +
			                 " WHERE IC_SOLICITUD = ? ";
			    varBind = new ArrayList();
			    varBind.add(ic_solicitus);
			     log.debug("qryConsulta ::: "+qryConsulta +"  varBind :: "+varBind);
			    ps = con.queryPrecompilado(qryConsulta, varBind);
			    rs = ps.executeQuery();
			    if (rs.next()) {
			        mpInfoFinan = new HashMap();
			        String anio_infoFinanciera =
			                        rs.getString("IG_ANIO_INF_FINANCIERA") == null ? "" :
			                        rs.getString("IG_ANIO_INF_FINANCIERA");
			        mpInfoFinan.put("IG_ANIO_INF_FINANCIERA",
			                        rs.getString("IG_ANIO_INF_FINANCIERA") == null ? "" :
			                        rs.getString("IG_ANIO_INF_FINANCIERA"));
			        mpInfoFinan.put("IG_MES_INF_FINANCIERA",
			                        rs.getString("IG_MES_INF_FINANCIERA") == null ? "" :
			                        rs.getString("IG_MES_INF_FINANCIERA"));
			        lstSaldoFinan.add(mpInfoFinan);
			        if (!anio_infoFinanciera.equals("")) {
			            EXISTE_INFO_P4++;
			        }
			    }
			    rs.close();
			    ps.close();
			    hmInfPreguntas.put("INFORMACION_FINANCIERA", lstSaldoFinan);
				// Se obtienen datos iniciales Saldos
				Map mpInfoSaldos = new HashMap();
				List lstSaldoIni = new ArrayList();
				qryConsulta = "";
				qryConsulta =
							 " SELECT FG_SALDO_VIGENTE_PRINC_PM ,FG_SALDO_VIGENTE_PRINC_PF  " + " FROM cedi_solicitud " +
							 " WHERE IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(ic_solicitus);
				log.debug("qryConsulta ::: " + qryConsulta + " varBind :: " + varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					String PM = rs.getString("FG_SALDO_VIGENTE_PRINC_PM") == null ? "" :
								rs.getString("FG_SALDO_VIGENTE_PRINC_PM");
					String PF = rs.getString("FG_SALDO_VIGENTE_PRINC_PF") == null ? "" :
								rs.getString("FG_SALDO_VIGENTE_PRINC_PF");
					mpInfoSaldos = new HashMap();
					mpInfoSaldos.put("FG_SALDO_VIGENTE_PRINC_PM",
									 rs.getString("FG_SALDO_VIGENTE_PRINC_PM") == null ? "" :
									 rs.getString("FG_SALDO_VIGENTE_PRINC_PM"));
					mpInfoSaldos.put("FG_SALDO_VIGENTE_PRINC_PF",
									 rs.getString("FG_SALDO_VIGENTE_PRINC_PF") == null ? "" :
									 rs.getString("FG_SALDO_VIGENTE_PRINC_PF"));
					lstSaldoIni.add(mpInfoSaldos);
					if (!PM.equals("") || !PF.equals("")) {
						EXISTE_INFO_P5++;
					}
					
				}
				rs.close();
				ps.close();
				hmInfPreguntas.put("INFORMACION_SALDOS", lstSaldoIni);
				hmInfPreguntas.put("EXISTE_INFO_P5", String.valueOf(EXISTE_INFO_P5));
			}else if(pestania.equals("IndicadorFinanciero")){
			    // Se obtienen datos iniciales Informaci�n Financiera
			    Map mpInfoFinan = new HashMap();
			    List lstSaldoFinan = new ArrayList();
			    qryConsulta = "";
			    qryConsulta =
			                 " SELECT IG_ANIO_INF_FINANCIERA,IG_MES_INF_FINANCIERA , CS_INDICADOR_AUX   " + " FROM cedi_solicitud " +
			                 " WHERE IC_SOLICITUD = ? ";
			    varBind = new ArrayList();
			    varBind.add(ic_solicitus);
			    // log.debug("qryConsulta ::: "+qryConsulta +"  varBind :: "+varBind);
			    ps = con.queryPrecompilado(qryConsulta, varBind);
			    rs = ps.executeQuery();
			    if (rs.next()) {
			        mpInfoFinan = new HashMap();
			        String anio_infoFinanciera =
			                        rs.getString("IG_ANIO_INF_FINANCIERA") == null ? "" :
			                        rs.getString("IG_ANIO_INF_FINANCIERA");
			        mpInfoFinan.put("IG_ANIO_INF_FINANCIERA",
			                        rs.getString("IG_ANIO_INF_FINANCIERA") == null ? "" :
			                        rs.getString("IG_ANIO_INF_FINANCIERA"));
			        mpInfoFinan.put("IG_MES_INF_FINANCIERA",
			                        rs.getString("IG_MES_INF_FINANCIERA") == null ? "" :
			                        rs.getString("IG_MES_INF_FINANCIERA"));
			        mpInfoFinan.put("CS_DICTAMEN_CONFIRMADO",
			                        rs.getString("CS_INDICADOR_AUX") == null ? "" :
			                        rs.getString("CS_INDICADOR_AUX"));
			        lstSaldoFinan.add(mpInfoFinan);
			        if (!anio_infoFinanciera.equals("")) {
			            EXISTE_INFO_P4++;
			        }
			    }
			    rs.close();
			    ps.close();
			    hmInfPreguntas.put("INFORMACION_FINANCIERA", lstSaldoFinan);
				// Se a�os de la Informaci�n financiera
				Map mpAnio = new HashMap();
				List lstAnio = new ArrayList();
				qryConsulta = "";
				qryConsulta =
							 " SELECT IC_ANIO FROM cedi_informacion_financiera  " + " WHERE IC_SOLICITUD = ? " +
							 " ORDER BY IC_ANIO DESC ";
				varBind = new ArrayList();
				varBind.add(ic_solicitus);
				log.debug("qryConsulta ::: " + qryConsulta + " varBind :: " + varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				int cont = 0;
				while (rs.next()) {
					cont++;
					mpAnio = new HashMap();
					mpAnio.put("IC_ANIO" + String.valueOf(cont),rs.getString("IC_ANIO") == null ? "" : rs.getString("IC_ANIO"));
					lstAnio.add(mpAnio);
					EXISTE_INFO_P6++;
				}
				rs.close();
				ps.close();
	
				hmInfPreguntas.put("LISTA_ANIO", lstAnio);
				hmInfPreguntas.put("EXISTE_INFO_P6", String.valueOf(EXISTE_INFO_P6));

			}

		} catch (Throwable t) {
			throw new AppException("getInformacionInicialCalificate(Error): Error al consultar informaci�n de las preguntas",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();

				if (psR != null)
					psR.close();
				if (rsR != null)
					rsR.close();

				if (psChk != null)
					psChk.close();
				if (rsChk != null)
					rsChk.close();
			} catch (Exception t) {
				log.error("getInformacionInicialCalificate():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getInformacionInicialCalificate(S)");
		}

		return hmInfPreguntas;
	}
	
	public Map informacionInicialTp(String ic_solicitus) {
		log.info("CediEJB::informacionInicialTp(E) " );

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		PreparedStatement psR = null;
		ResultSet rsR = null;

		PreparedStatement psChk = null;
		ResultSet rsChk = null;

		HashMap hmInfPreguntas = new HashMap(); 
		HashMap hmResp = new HashMap();
		List varBind = new ArrayList();
		String estatus_solicitud = "";
		int EXISTE_INFO_P1 = 0;
		int EXISTE_INFO_P2 = 0;
		int EXISTE_INFO_P3 = 0;
		int EXISTE_INFO_P4 = 0;
		int EXISTE_INFO_P5 = 0;
		int EXISTE_INFO_P6 = 0;

		try {
			con.conexionDB();

			/*String qryConsulSol =
						 "    select MAX(ic_solicitud ) AS IC_SOLICITUD, IC_ESTATUS_SOLICITUD" +
						 "	from cedi_ifnb_aspirante cia, cedi_solicitud cs" + "	where cia.IC_IFNB_ASPIRANTE = cs.IC_IFNB_ASPIRANTE" +
						 "	and CG_RFC = TRIM(?)" + "	GROUP BY IC_ESTATUS_SOLICITUD";

			varBind = new ArrayList();
			varBind.add(rfc);
			// log.debug("qryConsulta ::: "+qryConsulSol  +"   varBind :: "+varBind);
			ps = con.queryPrecompilado(qryConsulSol, varBind);
			rs = ps.executeQuery();

			if (rs.next()) {
				ic_solicitus = rs.getString("IC_SOLICITUD") == null ? "" : rs.getString("IC_SOLICITUD");
				estatus_solicitud =
								rs.getString("IC_ESTATUS_SOLICITUD") == null ? "" :
								rs.getString("IC_ESTATUS_SOLICITUD");
			}
			rs.close();
			ps.close();
			if (estatus_solicitud.equals("9")) {
				ic_solicitus = this.generaNuevoFolioAspirante(rfc);
			}
			hmInfPreguntas.put("CLAVE_SOLICITUD", ic_solicitus);
		    String qryConsulta = "";
		    qryConsulta = "";
		    qryConsulta = " select count(*) as preg_iniciales " + " from cedi_pregunta ";
		    ps = con.queryPrecompilado(qryConsulta);
		    rs = ps.executeQuery();
		    if (rs.next()) {
		        hmInfPreguntas.put("TOTAL_PREGUNTAS", rs.getString("PREG_INICIALES"));
		    }
		    rs.close();
		    ps.close();
			
		    // Revisar el estatus de la solicitud
		    qryConsulta = "";
		    qryConsulta = " select IC_ESTATUS_SOLICITUD from cedi_solicitud " + " where IC_SOLICITUD = ?";
		    varBind = new ArrayList();
		    varBind.add(ic_solicitus);
		    ps = con.queryPrecompilado(qryConsulta, varBind);
		    rs = ps.executeQuery();
		    if (rs.next()) {
		        hmInfPreguntas.put("IC_ESTATUS_SOLICITUD", rs.getString("IC_ESTATUS_SOLICITUD"));
		    }
		    rs.close();
		    ps.close();*/
//******************
		    String qryConsulta = "";
				qryConsulta =
							 " select IC_PREGUNTA,IC_TIPO_RESPUESTA,CG_PREGUNTA,IG_NUM_OPCIONES, CS_ACTIVO  " +
							 " from cedi_pregunta" + " ORDER BY IC_PREGUNTA ASC";
				ps = con.queryPrecompilado(qryConsulta);
				rs = ps.executeQuery();
	
				HashMap hmParamPreguntas = null;
				List lstPreg = new ArrayList();
				while (rs.next()) {
					hmParamPreguntas = new HashMap();
					hmParamPreguntas.put("ICPREGUNTA",
										 rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
					hmParamPreguntas.put("CGPREGUNTA",
										 rs.getString("CG_PREGUNTA") == null ? "" : rs.getString("CG_PREGUNTA"));
					hmParamPreguntas.put("TIPORESP",
										 rs.getString("IC_TIPO_RESPUESTA") == null ? "" :
										 rs.getString("IC_TIPO_RESPUESTA"));
					hmParamPreguntas.put("IGORDENPREG",
										 rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
					hmParamPreguntas.put("IGNUMPREG",
										 rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
					hmParamPreguntas.put("IGNUMOPC",
										 rs.getString("IG_NUM_OPCIONES") == null ? "" : rs.getString("IG_NUM_OPCIONES"));
					hmParamPreguntas.put("ICESTADO", rs.getString("CS_ACTIVO") == null ? "" : rs.getString("CS_ACTIVO"));
					lstPreg.add(hmParamPreguntas);
					String qryConsultaRes =
									" select IC_RESPUESTA_PARAMETRIZACION AS IC_RESP, IC_PREGUNTA,  IC_TIPO_RESPUESTA AS TIPO_RESP, CG_OPCION_RESPUESTA AS OPC_RESP, CG_PREGUNTAS_EXCLUIR AS PREG_EXC, DECODE(CG_PREGUNTAS_EXCLUIR,'','N','S') AS BAN_EXCLUIR,CS_LIBRE_OBLIGATORIA AS LIBRE_OBLIG, CS_RESPUESTA_ABIERTA_ASOC AS RESP_ABIERTA_ASO, CS_LIBRE_TIPO_VALIDACION AS LIBRE_TIPO_DATO,IG_LIBRE_LONGITUD_MAXIMA AS LIBRE_LONG,  CS_ACTIVO, CS_OPCION_OTROS AS OPCION_OTROS " +
									" from cedi_respuesta_parametrizacion" + " where IC_PREGUNTA = " +
									rs.getString("IC_PREGUNTA");
					psR = con.queryPrecompilado(qryConsultaRes);
					rsR = psR.executeQuery();
					log.debug("qryConsultaRes :::  "+qryConsultaRes);
					Map mpDataResp = new HashMap();
					List lstRespPreg = new ArrayList();
	
					while (rsR.next()) {
						mpDataResp = new HashMap();
						mpDataResp.put("IC_RESP", rsR.getString("IC_RESP") == null ? "" : rsR.getString("IC_RESP"));
						mpDataResp.put("IC_PREGUNTA",
									   rsR.getString("IC_PREGUNTA") == null ? "" : rsR.getString("IC_PREGUNTA"));
						mpDataResp.put("TIPO_RESP", rsR.getString("TIPO_RESP") == null ? "" : rsR.getString("TIPO_RESP"));
						mpDataResp.put("OPC_RESP", rsR.getString("OPC_RESP") == null ? "" : rsR.getString("OPC_RESP"));
						mpDataResp.put("PREG_EXC", rsR.getString("PREG_EXC") == null ? "" : rsR.getString("PREG_EXC"));
						mpDataResp.put("BAN_EXCLUIR",
									   rsR.getString("BAN_EXCLUIR") == null ? "" : rsR.getString("BAN_EXCLUIR"));
	
						mpDataResp.put("LIBRE_OBLIG",
									   rsR.getString("LIBRE_OBLIG") == null ? "" : rsR.getString("LIBRE_OBLIG"));
						mpDataResp.put("RESP_ABIERTA_ASO",
									   rsR.getString("RESP_ABIERTA_ASO") == null ? "N" : rsR.getString("RESP_ABIERTA_ASO"));
						mpDataResp.put("LIBRE_TIPO_DATO",
									   rsR.getString("LIBRE_TIPO_DATO") == null ? "" : rsR.getString("LIBRE_TIPO_DATO"));
						mpDataResp.put("LIBRE_LONG",
									   rsR.getString("LIBRE_LONG") == null ? "" : rsR.getString("LIBRE_LONG"));
						mpDataResp.put("CS_ACTIVO", rsR.getString("CS_ACTIVO") == null ? "" : rsR.getString("CS_ACTIVO"));
						mpDataResp.put("OPCION_OTROS",
									   rsR.getString("OPCION_OTROS") == null ? "N" : rsR.getString("OPCION_OTROS"));
	
						String qryCheckOpcion =
										   " SELECT IC_SOLICITUD,CG_RESPUESTA_LIBRE,CG_NOMBRE_OTRO, DECODE(COUNT(IC_SOLICITUD),'0','N','S') AS CHECK_OPCION_RESP" +
										   " FROM cedi_respuesta_ifnb" + " WHERE IC_RESPUESTA_PARAMETRIZACION =?" + " AND IC_SOLICITUD = ?" +
										   " GROUP BY IC_SOLICITUD,CG_RESPUESTA_LIBRE,CG_NOMBRE_OTRO";
						varBind = new ArrayList();
						varBind.add(rsR.getString("IC_RESP") == null ? "" : rsR.getString("IC_RESP"));
						varBind.add(ic_solicitus);
	
						log.debug("qryCheckOpcion :::  "+qryCheckOpcion+"  varBind :::  "+varBind);
	
						psChk = con.queryPrecompilado(qryCheckOpcion, varBind);
						rsChk = psChk.executeQuery();
						if (rsChk.next()) {
							mpDataResp.put("IC_SOLICITUD",
										   rsChk.getString("IC_SOLICITUD") == null ? "" : rsChk.getString("IC_SOLICITUD"));
							mpDataResp.put("CG_RESPUESTA_LIBRE",
										   rsChk.getString("CG_RESPUESTA_LIBRE") == null ? "" :
										   rsChk.getString("CG_RESPUESTA_LIBRE"));
							mpDataResp.put("CG_NOMBRE_OTRO",
										   rsChk.getString("CG_NOMBRE_OTRO") == null ? "" :
										   rsChk.getString("CG_NOMBRE_OTRO"));
							mpDataResp.put("CHECK_OPCION_RESP",
										   rsChk.getString("CHECK_OPCION_RESP") == null ? "N" :
										   rsChk.getString("CHECK_OPCION_RESP"));
							EXISTE_INFO_P2++;
						}
						psChk.close();
						rsChk.close();
	
	
						lstRespPreg.add(mpDataResp);
					}
					rsR.close();
					psR.close();
					hmResp.put("p" + rs.getString("IC_PREGUNTA"), lstRespPreg);
					// EXISTE_INFO_P2++;
				}
				rs.close();
				ps.close();
				hmInfPreguntas.put("INFOPARAMETRIZACION", lstPreg);
				hmInfPreguntas.put("INFO_RESPUESTAS", hmResp);
				hmInfPreguntas.put("EXISTE_INFO_P2", String.valueOf(EXISTE_INFO_P2));
			
				// Se obtienen datos iniciales para Cr�dito vinculado
				Map mpInfoCreditoVin = new HashMap();
				List lstCreditoVin = new ArrayList();
				qryConsulta = "";
				qryConsulta =
							 " SELECT CS_CREDITO_VINCULADO, FG_MONTO_CRED_VINCULADO, IC_SOLICITUD  " + " FROM cedi_solicitud " +
							 " WHERE IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(ic_solicitus);
				//log.debug("qryConsulta ::: "+qryConsulta+"   varBind :: "+varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					mpInfoCreditoVin = new HashMap();
					String credito =
									rs.getString("CS_CREDITO_VINCULADO") == null ? "" :
									rs.getString("CS_CREDITO_VINCULADO");
					mpInfoCreditoVin.put("CS_CREDITO_VINCULADO",
										 rs.getString("CS_CREDITO_VINCULADO") == null ? "" :
										 rs.getString("CS_CREDITO_VINCULADO"));
					mpInfoCreditoVin.put("FG_MONTO_CRED_VINCULADO",
										 rs.getString("FG_MONTO_CRED_VINCULADO") == null ? "" :
										 rs.getString("FG_MONTO_CRED_VINCULADO"));
					mpInfoCreditoVin.put("IC_SOLICITUD",
										 rs.getString("IC_SOLICITUD") == null ? "" : rs.getString("IC_SOLICITUD"));
					lstCreditoVin.add(mpInfoCreditoVin);
					if (!credito.equals("")) {
						EXISTE_INFO_P3++;
					}
				}
	
				rs.close();
				ps.close();
				hmInfPreguntas.put("INFORMACION_CREDITO_VINCULADO", lstCreditoVin);
				hmInfPreguntas.put("EXISTE_INFO_P3", String.valueOf(EXISTE_INFO_P3));
				// Se obtienen datos iniciales Informaci�n Financiera
				Map mpInfoFinan = new HashMap();
				List lstSaldoFinan = new ArrayList();
				qryConsulta = "";
				qryConsulta =
							 " SELECT IG_ANIO_INF_FINANCIERA,IG_MES_INF_FINANCIERA, CS_INDICADOR_AUX     " + " FROM cedi_solicitud " +
							 " WHERE IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(ic_solicitus);
				 log.debug("qryConsulta ::: "+qryConsulta +"  varBind :: "+varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					mpInfoFinan = new HashMap();
					String anio_infoFinanciera =
									rs.getString("IG_ANIO_INF_FINANCIERA") == null ? "" :
									rs.getString("IG_ANIO_INF_FINANCIERA");
					mpInfoFinan.put("IG_ANIO_INF_FINANCIERA",
									rs.getString("IG_ANIO_INF_FINANCIERA") == null ? "" :
									rs.getString("IG_ANIO_INF_FINANCIERA"));
					mpInfoFinan.put("IG_MES_INF_FINANCIERA",
									rs.getString("IG_MES_INF_FINANCIERA") == null ? "" :
									rs.getString("IG_MES_INF_FINANCIERA"));
				    mpInfoFinan.put("CS_DICTAMEN_CONFIRMADO",
				                    rs.getString("CS_INDICADOR_AUX") == null ? "" :
				                    rs.getString("CS_INDICADOR_AUX"));
					lstSaldoFinan.add(mpInfoFinan);
					if (!anio_infoFinanciera.equals("")) {
						EXISTE_INFO_P4++;
					}
				}
				rs.close();
				ps.close();
				hmInfPreguntas.put("INFORMACION_FINANCIERA", lstSaldoFinan);
				hmInfPreguntas.put("EXISTE_INFO_P4", String.valueOf(EXISTE_INFO_P4));
				// Se obtienen datos iniciales Saldos
				Map mpInfoSaldos = new HashMap();
				List lstSaldoIni = new ArrayList();
				qryConsulta = "";
				qryConsulta =
							 " SELECT FG_SALDO_VIGENTE_PRINC_PM ,FG_SALDO_VIGENTE_PRINC_PF  " + " FROM cedi_solicitud " +
							 " WHERE IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(ic_solicitus);
				log.debug("qryConsulta ::: " + qryConsulta + " varBind :: " + varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					String PM = rs.getString("FG_SALDO_VIGENTE_PRINC_PM") == null ? "" :
								rs.getString("FG_SALDO_VIGENTE_PRINC_PM");
					String PF = rs.getString("FG_SALDO_VIGENTE_PRINC_PF") == null ? "" :
								rs.getString("FG_SALDO_VIGENTE_PRINC_PF");
					mpInfoSaldos = new HashMap();
					mpInfoSaldos.put("FG_SALDO_VIGENTE_PRINC_PM",
									 rs.getString("FG_SALDO_VIGENTE_PRINC_PM") == null ? "" :
									 rs.getString("FG_SALDO_VIGENTE_PRINC_PM"));
					mpInfoSaldos.put("FG_SALDO_VIGENTE_PRINC_PF",
									 rs.getString("FG_SALDO_VIGENTE_PRINC_PF") == null ? "" :
									 rs.getString("FG_SALDO_VIGENTE_PRINC_PF"));
					lstSaldoIni.add(mpInfoSaldos);
					if (!PM.equals("") || !PF.equals("")) {
						EXISTE_INFO_P5++;
					}
				}
				rs.close();
				ps.close();
				hmInfPreguntas.put("INFORMACION_SALDOS", lstSaldoIni);
				hmInfPreguntas.put("EXISTE_INFO_P5", String.valueOf(EXISTE_INFO_P5));
				// Se a�os de la Informaci�n financiera
				Map mpAnio = new HashMap();
				List lstAnio = new ArrayList();
				qryConsulta = "";
				qryConsulta =
							 " SELECT IC_ANIO FROM cedi_informacion_financiera  " + " WHERE IC_SOLICITUD = ? " +
							 " ORDER BY IC_ANIO DESC ";
				varBind = new ArrayList();
				varBind.add(ic_solicitus);
				log.debug("qryConsulta ::: " + qryConsulta + " varBind :: " + varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				int cont = 0;
				while (rs.next()) {
					cont++;
					mpAnio = new HashMap();
					mpAnio.put("IC_ANIO" + String.valueOf(cont),
							   rs.getString("IC_ANIO") == null ? "" : rs.getString("IC_ANIO"));
					lstAnio.add(mpAnio);
					EXISTE_INFO_P6++;
				}
				rs.close();
				ps.close();
	
				hmInfPreguntas.put("LISTA_ANIO", lstAnio);
				hmInfPreguntas.put("EXISTE_INFO_P6", String.valueOf(EXISTE_INFO_P6));

			

		} catch (Throwable t) {
			throw new AppException("getInformacionInicialCalificate(Error): Error al consultar informaci�n de las preguntas",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();

				if (psR != null)
					psR.close();
				if (rsR != null)
					rsR.close();

				if (psChk != null)
					psChk.close();
				if (rsChk != null)
					rsChk.close();
			} catch (Exception t) {
				log.error("getInformacionInicialCalificate():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getInformacionInicialCalificate(S)");
		}

		return hmInfPreguntas;
	}

	/**
	 *Funci�n que actualiza la informaci�n del credito vinculado
	 * @param clave_solicitud clave de la solicitud que se le asignara el credito
	 * @param tieneCredVinc nos dice si tiene cr�dito vinculado
	 * @param montoCredVinc nos dice cu�l es el monto del credito
	 * @return
	 */
	public boolean actualizaInfoCreditoVinculado(String clave_solicitud, String tieneCredVinc, String montoCredVinc,
												 String usuario, String estatus) {
		log.info("actualizaInfoCreditoVinculado(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";
		
	    String existe_rechazo = this.existeRechazoMovimientos(clave_solicitud);
	    int existe_rechazo_aux = 0;
	    if(existe_rechazo.equals("")){
	      existe_rechazo_aux = 0;
	    }else{
	       existe_rechazo_aux = Integer.parseInt(existe_rechazo);
	    }
	    if(existe_rechazo_aux>0){
	       actualizaEstatusSolicitud(clave_solicitud,"1");
	        boolean actualiza = this.actualizaDictamenConfirmacion(clave_solicitud,"N");
	    }
		try {
			con.conexionDB();
			qry = "";
			qry = "  UPDATE cedi_solicitud " + "  set CS_CREDITO_VINCULADO = ?, " + "  FG_MONTO_CRED_VINCULADO = ?" +
						 "  WHERE IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(tieneCredVinc);
			varBind.add(montoCredVinc);
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
			boolean actualizaMov = true;
		    if(existe_rechazo_aux>0){
		       actualizaMov = this.setMovimientoSolicitus(con, clave_solicitud, usuario, "1", "11","");
		    }else{
		        actualizaMov = this.setMovimientoSolicitus(con, clave_solicitud, usuario, "1", "2","");
		    }
		
		} catch (Throwable t) {
			exito = false;
			throw new AppException("actualizaInfoCreditoVinculado(Error): Error al guardar la informaci�n en cedi_ifnb_aspirante",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("actualizaInfoCreditoVinculado():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		return exito;
	}

	/**
	 *Funci�n que nos regresa la clave del aspirante
	 * @param rfc
	 * @return
	 */
	public String getClaveAspirante(String rfc) {
		log.info("CediEJB::getClaveAspirante(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String IC_ASPIRANTE = "";
		try {
			con.conexionDB();

			String qryConsulta = " select IC_IFNB_ASPIRANTE  from cedi_ifnb_aspirante" + " where CG_RFC =TRIM(?)";
			varBind = new ArrayList();
			varBind.add(rfc);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				IC_ASPIRANTE = rs.getString("IC_IFNB_ASPIRANTE");


			}
			log.debug(" IC_ASPIRANTE :: " + IC_ASPIRANTE);
			rs.close();
			ps.close();


		} catch (Throwable t) {
			throw new AppException("getClaveAspirante(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getClaveAspirante():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getClaveAspirante(S)");
		}

		return IC_ASPIRANTE;
	}

	public List getStoreGridInfoFinanciera(String IC_SOLICITUD) {
		log.info("CediEJB::getStoreGridInfoFinanciera(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		HashMap datos = new HashMap();
		List registros = new ArrayList();
		String IC_ASPIRANTE = "";
	    List listColumna = new ArrayList();
		try {
			con.conexionDB();

			String qryConsulta =
						 " select * from cedi_informacion_financiera" + " where IC_SOLICITUD = ?" +
						 "	ORDER BY IC_ANIO desc";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
			    listColumna = new ArrayList();
				datos = new HashMap();
			   
				datos.put("IC_SOLICITUD", rs.getString("IC_SOLICITUD") == null ? "" : rs.getString("IC_SOLICITUD"));
			    listColumna.add(datos);
				datos = new HashMap();
				datos.put("IC_ANIO", rs.getString("IC_ANIO") == null ? "" : rs.getString("IC_ANIO"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_ACTIVOS_TOTALES",
						  rs.getString("FG_ACTIVOS_TOTALES") == null ? "" : rs.getString("FG_ACTIVOS_TOTALES"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_INVERSION_CAJA",
						  rs.getString("FG_INVERSION_CAJA") == null ? "" : rs.getString("FG_INVERSION_CAJA"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_CARTERA_VIGENTE",
						  rs.getString("FG_CARTERA_VIGENTE") == null ? "" : rs.getString("FG_CARTERA_VIGENTE"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_CARTERA_VENCIDA",
						  rs.getString("FG_CARTERA_VENCIDA") == null ? "" : rs.getString("FG_CARTERA_VENCIDA"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_CARTERA_TOTAL",
						  rs.getString("FG_CARTERA_TOTAL") == null ? "" : rs.getString("FG_CARTERA_TOTAL"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_ESTIMACION_RESERVAS",
						  rs.getString("FG_ESTIMACION_RESERVAS") == null ? "" : rs.getString("FG_ESTIMACION_RESERVAS"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_PASIVOS_BANCARIOS",
						  rs.getString("FG_PASIVOS_BANCARIOS") == null ? "" : rs.getString("FG_PASIVOS_BANCARIOS"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_PASIVOS_TOTALES",
						  rs.getString("FG_PASIVOS_TOTALES") == null ? "" : rs.getString("FG_PASIVOS_TOTALES"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_CAPITAL_SOCIAL",
						  rs.getString("FG_CAPITAL_SOCIAL") == null ? "" : rs.getString("FG_CAPITAL_SOCIAL"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_CAPITAL_CONTABLE",
						  rs.getString("FG_CAPITAL_CONTABLE") == null ? "" : rs.getString("FG_CAPITAL_CONTABLE"));
			    listColumna.add(datos);
			    datos = new HashMap();
				datos.put("FG_UTILIDAD_NETA",
						  rs.getString("FG_UTILIDAD_NETA") == null ? "" : rs.getString("FG_UTILIDAD_NETA"));
			    listColumna.add(datos);
				registros.add(listColumna);
			}

			log.debug(" registros ::rrr " + registros);
			rs.close();
			ps.close();


		} catch (Throwable t) {
			throw new AppException("getStoreGridInfoFinanciera(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getStoreGridInfoFinanciera():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getStoreGridInfoFinanciera(S)");
		}

		return registros;
	}

	/**
	 *Funci�n que nos guarda la informacion financiera de los �ltimos tres a�os
	 * @param con
	 * @param anio
	 * @param ic_solicitud
	 * @return
	 */
	public boolean guardaInfoAnio(String anio, String ic_solicitud) {
		log.info("guardaInfoAnio (E)");

		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		List varBind = new ArrayList();
		String existe_anio = "";
	    AccesoDB con = new AccesoDB();
		try {

		    con.conexionDB();
			strSQL = "	SELECT DECODE(COUNT(*),0,'N','S') AS EXISTE " + "	FROM cedi_informacion_financiera " +
						 "	WHERE IC_SOLICITUD = ? " + "	AND IC_ANIO = ?";

			varBind = new ArrayList();
			varBind.add(ic_solicitud);
			varBind.add(anio);
			log.debug("qryConsulta ::: " + strSQL);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(strSQL, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				existe_anio = rs.getString("EXISTE");
			}
			rs.close();
			ps.close();
			if (existe_anio.equals("N")) {
				strSQL = "  INSERT INTO cedi_informacion_financiera(IC_SOLICITUD,IC_ANIO) " + "  VALUES(?,?) ";
				varBind = new ArrayList();
				varBind.add(ic_solicitud);
				varBind.add(anio);

				log.debug("qry :: " + strSQL);
				log.debug("varBind :: " + varBind);
				ps = con.queryPrecompilado(strSQL, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();

			}

		} catch (Throwable t) {
			exito = false;
			throw new AppException("guardaInfoAnio(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("guardaInfoAnio():: Error al cerrar Recursos: " + t.getMessage());
			}
		    if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
		         con.cierraConexionDB();
			}

			log.info("CediEJB::guardaInfoAnio(S)");
		}
		return exito;
	}

	/**
	 *Funci�n que nos ayuda a guardar la informaci�n Financiera
	 * @param con se pasa la conexi�n ya que si existe algum error ya no guardara nada
	 * @param campo es el campo donde se guardara el dato
	 * @param dato es el dato que fue capturado por el ususario
	 * @param anio a�o en el que se guardara la informaci�n
	 * @param ic_solicitud en donde se guardara la informaci�n
	 * @return
	 */
	public boolean guardaInformacionFinanciera(String campo, String dato, String anio,
											   String ic_solicitud) {
		log.info("guardaInformacionFinanciera (E)");
	    AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		List varBind = new ArrayList();
		try {
		    con.conexionDB();
			if (!campo.equals("")) {
				strSQL = "  UPDATE cedi_informacion_financiera  " + "	SET " + campo + " = ?" + "	WHERE IC_SOLICITUD = ?" +
								"  AND IC_ANIO = ? ";
				varBind = new ArrayList();
				varBind.add(dato);
				varBind.add(ic_solicitud);
				varBind.add(anio);

				log.debug("qry :: " + strSQL);
				log.debug("varBind :: " + varBind);
				ps = con.queryPrecompilado(strSQL, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();
			}


		} catch (Throwable t) {
			exito = false;
			throw new AppException("guardaInformacionFinanciera(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("guardaInformacionFinanciera():: Error al cerrar Recursos: " + t.getMessage());
			}
		    if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
		        con.cierraConexionDB();
			}

			log.info("CediEJB::guardaInformacionFinanciera(S)");
		}
		return exito;
	}

	/**
	 *	Funci�n que nos ayuda a guardar los saldos de los acreditados
	 * @param IC_SOLICITUD clave de la solicitud
	 * @param tipo_acreditado tipo de afiliado V o I
	 * @param num_acreditado clave de los afiliados
	 * @param saldo  El saldo de los acreditados
	 * @param saldoPM  Saldo vigente de la persona Moral acreditada
	 * @param saldoPF	Saldo vigente de la persona F�sica acreditada
	 * @return
	 */
	public boolean guardarInformacionSaldos(String IC_SOLICITUD, String tipo_acreditado,
											String num_acreditado, String saldo) {
		log.info("CediEJB::guardarInformacionSaldos(E)");
	    AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String existe = "";
		boolean exito = true;
		try {
		    con.conexionDB();
			String qryConsulta =
						 " SELECT DECODE(COUNT(*),0,'N','S') AS EXISTE " + " FROM cedi_saldo_acreditado" +
						 " WHERE IC_SOLICITUD = ?" + " AND CC_TIPO_ACREDITADO = ?" + " AND IC_SALDO_ACREDITADO =?";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			varBind.add(tipo_acreditado);
			varBind.add(num_acreditado);

			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				existe = rs.getString("EXISTE");
			}
			rs.close();
			ps.close();
			if (existe.equals("N")) {
				String strSQL = "  INSERT INTO cedi_saldo_acreditado(IC_SOLICITUD,CC_TIPO_ACREDITADO,IC_SALDO_ACREDITADO,FG_SALDO)" +
								"  VALUES(?,?,?,?) ";
				varBind = new ArrayList();
				varBind.add(IC_SOLICITUD);
				varBind.add(tipo_acreditado);
				varBind.add(num_acreditado);
				varBind.add(saldo);
				log.debug("qry :: " + strSQL);
				log.debug("varBind :: " + varBind);
				ps = con.queryPrecompilado(strSQL, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();

			} else {
				String strSQL = "  UPDATE cedi_saldo_acreditado" + "  SET FG_SALDO = ? " + " WHERE IC_SOLICITUD = ?" +
								" AND CC_TIPO_ACREDITADO = ?" + " AND IC_SALDO_ACREDITADO =?";
				varBind = new ArrayList();
				varBind.add(saldo);
				varBind.add(IC_SOLICITUD);
				varBind.add(tipo_acreditado);
				varBind.add(num_acreditado);
				log.debug("qry :: " + strSQL);
				log.debug("varBind :: " + varBind);
				ps = con.queryPrecompilado(strSQL, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();
			}
			rs.close();
			ps.close();


		} catch (Throwable t) {
			exito = false;
			throw new AppException("actualizaInfoCreditoVinculado(Error): Error al guardar la informaci�n en cedi_ifnb_aspirante",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("actualizaInfoCreditoVinculado():: Error al cerrar Recursos: " + t.getMessage());
			}
		    if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
		        con.cierraConexionDB();
			}

		}

		return exito;
	}

	/**
	 * 	Funci�n que guarda los saldos Vigentes de la Persona Moral y F�sica
	 * @param con
	 * @param IC_SOLICITUD
	 * @param saldoPM
	 * @param saldoPF
	 * @return
	 */
	public boolean guardarSaldoPMF(String IC_SOLICITUD, String saldoPM, String saldoPF) {
		log.info("CediEJB::guardarSaldoPMF(E)");
	    AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		boolean exito = true;
		try {
		    con.conexionDB();
			String strSQL =
						 "     UPDATE cedi_solicitud " + "   SET FG_SALDO_VIGENTE_PRINC_PM  = ?, " +
						 "   FG_SALDO_VIGENTE_PRINC_PF = ?" + "   WHERE IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(saldoPM);
			varBind.add(saldoPF);
			varBind.add(IC_SOLICITUD);
			log.debug("qry :: " + strSQL);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(strSQL, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();

		} catch (Throwable t) {
		    exito = false;
			throw new AppException("guardarSaldoPMF(Error): Error al guardar la informaci�n en cedi_ifnb_aspirante", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("guardarSaldoPMF():: Error al cerrar Recursos: " + t.getMessage());
			}
		    if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
		        con.cierraConexionDB();
			}

		}

		return exito;
	}

	/**
	 * Funci�n que nos regresa el listado de los registros
	 * @param IC_SOLICITUD clave de la solicitud
	 * @param tipo_acreditado es el tipo de credito
	 * @return lista de registros
	 */
	public Registros getStoreGridAcreditado(String IC_SOLICITUD, String tipo_acreditado) {
		log.info("CediEJB::getStoreGridAcreditado(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		HashMap datos = new HashMap();
		List registros = new ArrayList();
		String IC_ASPIRANTE = "";
		try {
			con.conexionDB();

			String qryConsulta =
						 "  SELECT IC_SOLICITUD,CC_TIPO_ACREDITADO, IC_SALDO_ACREDITADO, 'Acreditado '||IC_SALDO_ACREDITADO  as CS_SALDO_ACREDITADO , FG_SALDO   FROM cedi_saldo_acreditado" +
						 "  WHERE IC_SOLICITUD = ?" + "  AND CC_TIPO_ACREDITADO = ?" +
						 "	AND IC_SALDO_ACREDITADO IN (1,2,3,4,5,6,7,8,9,10)"+
						"   ORDER BY FG_SALDO DESC";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			varBind.add(tipo_acreditado);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			Registros reg = con.consultarDB(qryConsulta, varBind);


			return reg;

		} catch (Throwable t) {
			throw new AppException("getStoreGridAcreditado(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getStoreGridAcreditado():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getStoreGridAcreditado(S)");
		}

	}

	/**
	 * FUNCI�N QUE NOS OBTIENE LOS RESULTADOS DE LOS INDICADORES FINANCIEROS
	 * @param IC_SOLICITUD es la clave de la solicitud
	 * @return
	 */
	public List getStoreIndicadorFinan(String IC_SOLICITUD) {
		log.info("CediEJB::getStoreIndicadorFinan(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		HashMap datos = new HashMap();
		List registros = new ArrayList();
		try {
			con.conexionDB();

			String qryConsulta =
						 "   SELECT IC_ANIO,ic_solicitud, round((((FG_CAPITAL_CONTABLE +(FG_ESTIMACION_RESERVAS-FG_CARTERA_VENCIDA)) - (0.05*FG_CARTERA_VIGENTE))/FG_CARTERA_TOTAL)*100,2) AS CAPITALIZACION, " +
						 "  round((decode(FG_CARTERA_VENCIDA,0,1,FG_CARTERA_VENCIDA)/FG_CARTERA_TOTAL)*100 ,2) AS MOROSIDAD," +
						 "  round((FG_ESTIMACION_RESERVAS/decode(FG_CARTERA_VENCIDA,0,1,FG_CARTERA_VENCIDA))*100,2) AS COVERTURA_RESERVA," +
						 " round((FG_UTILIDAD_NETA/FG_CAPITAL_CONTABLE)*100,2) AS ROE," +
						 "  round((FG_INVERSION_CAJA/FG_PASIVOS_TOTALES)*100,2) AS LIQUIDEZ," +
						 "  round((FG_PASIVOS_TOTALES/FG_CAPITAL_CONTABLE)*100,2) AS APALANCAMIENTO" +
						 "	FROM cedi_informacion_financiera" + "  WHERE IC_SOLICITUD = ?" +
						 "  ORDER BY IC_ANIO DESC";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				datos = new HashMap();
				datos.put("IC_ANIO", rs.getString("IC_ANIO") == null ? "" : rs.getString("IC_ANIO"));
				datos.put("CAPITALIZACION",
						  rs.getString("CAPITALIZACION") == null ? "" : rs.getString("CAPITALIZACION"));
				datos.put("MOROSIDAD", rs.getString("MOROSIDAD") == null ? "" : rs.getString("MOROSIDAD"));
				datos.put("COVERTURA_RESERVA",
						  rs.getString("COVERTURA_RESERVA") == null ? "" : rs.getString("COVERTURA_RESERVA"));
				datos.put("ROE", rs.getString("ROE") == null ? "" : rs.getString("ROE"));
				datos.put("LIQUIDEZ", rs.getString("LIQUIDEZ") == null ? "" : rs.getString("LIQUIDEZ"));
				datos.put("APALANCAMIENTO",
						  rs.getString("APALANCAMIENTO") == null ? "" : rs.getString("APALANCAMIENTO"));
				datos.put("ic_solicitud", rs.getString("ic_solicitud") == null ? "" : rs.getString("ic_solicitud"));
				registros.add(datos);
			}
			rs.close();
			ps.close();
			return registros;

		} catch (Throwable t) {
			throw new AppException("getStoreIndicadorFinan(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getStoreIndicadorFinan():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getStoreIndicadorFinan(S)");
		}

	}


	/**
	 *Funci�n que nos regresa la respuesta de la pregunta 4
	 * @param IC_SOLICITUD
	 * @param pregunta
	 * @return
	 */
	public List getDestinoRecSol(String IC_SOLICITUD, String pregunta) {
		log.info("CediEJB::getDestinoRecSol(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		HashMap datos = new HashMap();
		List registros = new ArrayList();
		try {
			con.conexionDB();

			String qryConsulta =
						 "  select cri.IC_SOLICITUD, crp.IC_PREGUNTA,   cri.IC_RESPUESTA_PARAMETRIZACION,crp.CG_OPCION_RESPUESTA,cri.CG_RESPUESTA_LIBRE, decode(crp.IC_RESPUESTA_PARAMETRIZACION,10,'> � = 9 millones UDI',11,'> � = 5 millones UDI','') AS INDICADOR_CUMPLIMIENTO" +
						 "  from cedi_respuesta_parametrizacion crp, cedi_respuesta_ifnb cri" +
						 "  where crp.IC_RESPUESTA_PARAMETRIZACION = cri.IC_RESPUESTA_PARAMETRIZACION" + "  and crp.IC_PREGUNTA = ?" +
						 "	AND cri.IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(pregunta);
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				datos = new HashMap();
				datos.put("IC_SOLICITUD", rs.getString("IC_SOLICITUD") == null ? "" : rs.getString("IC_SOLICITUD"));
				datos.put("IC_PREGUNTA", rs.getString("IC_PREGUNTA") == null ? "" : rs.getString("IC_PREGUNTA"));
				datos.put("IC_RESPUESTA_PARAMETRIZACION",
						  rs.getString("IC_RESPUESTA_PARAMETRIZACION") == null ? "" :
						  rs.getString("IC_RESPUESTA_PARAMETRIZACION"));
				datos.put("CG_OPCION_RESPUESTA",
						  rs.getString("CG_OPCION_RESPUESTA") == null ? "" : rs.getString("CG_OPCION_RESPUESTA"));
				datos.put("CG_RESPUESTA_LIBRE",
						  rs.getString("CG_RESPUESTA_LIBRE") == null ? "" : rs.getString("CG_RESPUESTA_LIBRE"));
				datos.put("INDICADOR_CUMPLIMIENTO",
						  rs.getString("INDICADOR_CUMPLIMIENTO") == null ? "" : rs.getString("INDICADOR_CUMPLIMIENTO"));
				registros.add(datos);
			}
			rs.close();
			ps.close();
			return registros;

		} catch (Throwable t) {
			throw new AppException("getDestinoRecSol(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getDestinoRecSol():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getDestinoRecSol(S)");
		}

	}

	/**
	 *Funci�n que nos dara la informaci�n de una solicitud
	 * @param IC_SOLICITUD
	 * @return
	 */
	public List getInfoFinanciera(String IC_SOLICITUD) {
		log.info("CediEJB::getInfoFinanciera(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		HashMap datos = new HashMap();
		List registros = new ArrayList();
		try {
			con.conexionDB();

			String qryConsulta =
						 "  select  cif.IC_ANIO," +
						 "  TRUNC((cif.FG_CAPITAL_CONTABLE/ (/* Formatted on 2015/10/03 16:28 (Formatter Plus v4.8.8) */ SELECT fn_valor_compra FROM com_tipo_cambio WHERE ic_moneda = 40 AND ROWID = (SELECT MAX (ROWID) FROM com_tipo_cambio  WHERE ic_moneda = 40))),2) AS CARTERA_PYME," +
						 "  TRUNC((cif.FG_CAPITAL_CONTABLE*0.30),2) as CONCENTRACION_ACRE_PM," +
						 "  TRUNC((cif.FG_CAPITAL_CONTABLE*0.10),2) as CONCENTRACION_ACRE_PF," +
						 "  TRUNC((cif.FG_CARTERA_TOTAL*0.35),2) as CONCENTRACION_CARTERA_VIGENTE" +
						 "  from cedi_informacion_financiera cif" + "  where cif.IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				datos = new HashMap();
				datos.put("IC_ANIO", rs.getString("IC_ANIO") == null ? "" : rs.getString("IC_ANIO"));
				datos.put("CARTERA_PYME", rs.getString("CARTERA_PYME") == null ? "" : rs.getString("CARTERA_PYME"));
				datos.put("CONCENTRACION_ACRE_PM",
						  rs.getString("CONCENTRACION_ACRE_PM") == null ? "" : rs.getString("CONCENTRACION_ACRE_PM"));
				datos.put("CONCENTRACION_ACRE_PF",
						  rs.getString("CONCENTRACION_ACRE_PF") == null ? "" : rs.getString("CONCENTRACION_ACRE_PF"));
				datos.put("CONCENTRACION_CARTERA_VIGENTE",
						  rs.getString("CONCENTRACION_CARTERA_VIGENTE") == null ? "" :
						  rs.getString("CONCENTRACION_CARTERA_VIGENTE"));
				registros.add(datos);
			}
			rs.close();
			ps.close();
			return registros;

		} catch (Throwable t) {
			throw new AppException("getInfoFinanciera(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getInfoFinanciera():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getInfoFinanciera(S)");
		}

	}

	/**
	 * Funcion que guardara en mes y a�o de la Informai�n financiera
	 * @param con
	 * @param clave_solicitud
	 * @param anio
	 * @param mes
	 * @return
	 */
	public boolean InsertarAnioInfFinaciera(String clave_solicitud, String anio, String mes) {
		log.info("InsertarAnioInfFinaciera(E)");
	    AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";

		try {
		    con.conexionDB();
			qry = "";
			qry = "  UPDATE cedi_solicitud " + "  set IG_ANIO_INF_FINANCIERA = ?, " + "  IG_MES_INF_FINANCIERA = ?" +
						 "  WHERE IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(anio);
			varBind.add(mes);
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();

		} catch (Throwable t) {
		    exito = false;
			throw new AppException("InsertarAnioInfFinaciera(Error): Error al guardar la informaci�n", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("InsertarAnioInfFinaciera():: Error al cerrar Recursos: " + t.getMessage());
			}
		    if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
		        con.cierraConexionDB();
			}

		}
		return exito;
	}

	/**
	 *Funci�n que calcula los indicadores Financieros y los guarda en la solicitud correspondiente
	 * @param con
	 * @param IC_SOLICITUD
	 * @param pregunta
	 * @param usuario
	 * @param estatus
	 * @return
	 */
	public boolean guardaIndicadoresFinancieros(String IC_SOLICITUD, String pregunta, String usuario,
												String estatus) {
		log.info("CediEJB::guardaIndicadoresFinancieros(E)");
	    AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		HashMap datos = new HashMap();
		List registros = new ArrayList();
		boolean exito = true;
		try {
		    con.conexionDB();
			String qryConsulta =
						 "  SELECT IC_ANIO,ic_solicitud, round((((FG_CAPITAL_CONTABLE +(FG_ESTIMACION_RESERVAS-FG_CARTERA_VENCIDA)) - (0.05*FG_CARTERA_VIGENTE))/FG_CARTERA_TOTAL)*100,2) AS CAPITALIZACION," +
						 "  round((decode(FG_CARTERA_VENCIDA,0,1,FG_CARTERA_VENCIDA)/FG_CARTERA_TOTAL)*100,2)  AS MOROSIDAD," +
						 "   round((FG_ESTIMACION_RESERVAS/decode(FG_CARTERA_VENCIDA,0,1,FG_CARTERA_VENCIDA))*100,2) AS COVERTURA_RESERVA," +
						 " round((FG_UTILIDAD_NETA/FG_CAPITAL_CONTABLE)*100,2) AS ROE," +
						 "  round((FG_INVERSION_CAJA/FG_PASIVOS_TOTALES)*100,2) AS LIQUIDEZ," +
						 "  round((FG_PASIVOS_TOTALES/FG_CAPITAL_CONTABLE)*100,2) AS APALANCAMIENTO" +
						 "  FROM cedi_informacion_financiera" + "  WHERE IC_SOLICITUD = ?" + "  ORDER BY IC_ANIO DESC";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			int cont = 0;
			while (rs.next()) {
				cont++;
				String aux = "";
				if(cont<=3){
					if (cont == 3) {
						aux = "	SET FG_CAPITALIZACION_ANIO1 = ?," + "	FG_MOROSIDAD_ANIO1  = ?," + "	FG_COBERTURA_RESERVAS_ANIO1 = ?," +
										   "	FG_ROE_ANIO1 = ?," + "	FG_LIQUIDEZ_ANIO1 = ?," +
										   "  FG_APALANCAMIENTO_ANIO1 = ?";
					} else if (cont == 2) {
						aux = "	SET FG_CAPITALIZACION_ANIO2 = ?," + "	FG_MOROSIDAD_ANIO2  = ?," + "	FG_COBERTURA_RESERVAS_ANIO2 = ?," +
										   "	FG_ROE_ANIO2 = ?," + "	FG_LIQUIDEZ_ANIO2 = ?," +
										   "  FG_APALANCAMIENTO_ANIO2 = ?";
					} else if (cont == 1) {
						aux = "	SET FG_CAPITALIZACION_ANIO3 = ?," + "	FG_MOROSIDAD_ANIO3  = ?," + "	FG_COBERTURA_RESERVAS_ANIO3 = ?," +
										   "	FG_ROE_ANIO3 = ?," + "	FG_LIQUIDEZ_ANIO3 = ?," +
										   "  FG_APALANCAMIENTO_ANIO3 = ?";
					}
					String qry = "  UPDATE cedi_solicitud " + aux + "  WHERE IC_SOLICITUD = ?";
					varBind = new ArrayList();
					varBind.add(rs.getString("CAPITALIZACION") == null ? "" : rs.getString("CAPITALIZACION"));
					varBind.add(rs.getString("MOROSIDAD") == null ? "" : rs.getString("MOROSIDAD"));
					varBind.add(rs.getString("COVERTURA_RESERVA") == null ? "" : rs.getString("COVERTURA_RESERVA"));
					varBind.add(rs.getString("ROE") == null ? "" : rs.getString("ROE"));
					varBind.add(rs.getString("LIQUIDEZ") == null ? "" : rs.getString("LIQUIDEZ"));
					varBind.add(rs.getString("APALANCAMIENTO") == null ? "" : rs.getString("APALANCAMIENTO"));
					varBind.add(IC_SOLICITUD);
	
					log.debug("qry :: " + qry);
					log.debug("varBind :: " + varBind);
					ps1 = con.queryPrecompilado(qry, varBind);
					ps1.executeUpdate();
					ps1.clearParameters();
					ps1.close();
				}

			}
			rs.close();
			ps.close();

			String tipo_cartera = this.getTipoCartera(IC_SOLICITUD, pregunta);

			// Valida la aprobaci�n de los indicadores financieros
			//boolean acepta_sol_Ind = this.validaAprobacion(con,IC_SOLICITUD,tipo_cartera);

			// Valida la aprobaci�n de la Informaci�n Cualitativa
			boolean aprobada = this.validaViabilidadCedula(con,IC_SOLICITUD,tipo_cartera);
			


			//Para calcular el  Capital Contable
			qryConsulta =
						 "  select  cif.IC_ANIO," +
						 "  round((cif.FG_CAPITAL_CONTABLE/ (/* Formatted on 2015/10/03 16:28 (Formatter Plus v4.8.8) */ SELECT fn_valor_compra FROM com_tipo_cambio WHERE ic_moneda = 40 AND ROWID = (SELECT MAX (ROWID) FROM com_tipo_cambio  WHERE ic_moneda = 40))),2) AS CARTERA_PYME," +
						 "  round((cif.FG_CAPITAL_CONTABLE*0.30),2) as CONCENTRACION_ACRE_PM," +
						 "  round((cif.FG_CAPITAL_CONTABLE*0.10),2) as CONCENTRACION_ACRE_PF," +
						 "  round((cif.FG_CARTERA_TOTAL*0.35),2) as CONCENTRACION_CARTERA_VIGENTE" +
						 "  from cedi_informacion_financiera cif, cedi_solicitud cs" + "  where cif.IC_SOLICITUD = ?" +
						 "	AND cif.IC_SOLICITUD = cs.IC_SOLICITUD" +
						 "	AND cif.IC_ANIO = cs.IG_ANIO_INF_FINANCIERA";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta Para calcular el  Capital Contable ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				String aux_cad = "";
				if (aprobada != true) {
					aux_cad = "  IC_ESTATUS_SOLICITUD = ?,";
				}
				String queryAux =
								"	UPDATE cedi_solicitud" + "	SET FG_CARTERA = ?," + "	CS_TIPO_CARTERA = ?," + aux_cad +
								"	FG_CONC_ACRED_PM = ?," + "	FG_CONC_ACRED_PF = ? ," + "	FG_CONC_CART_VIG = ?" +
								"	WHERE IC_SOLICITUD = ?";
				varBind = new ArrayList();
				varBind.add(rs.getString("CARTERA_PYME") == null ? "" : rs.getString("CARTERA_PYME"));
				varBind.add(tipo_cartera);
				if (aprobada != true) {
					varBind.add("7");
				}
				varBind.add(rs.getString("CONCENTRACION_ACRE_PM") == null ? "" : rs.getString("CONCENTRACION_ACRE_PM"));
				varBind.add(rs.getString("CONCENTRACION_ACRE_PF") == null ? "" : rs.getString("CONCENTRACION_ACRE_PF"));
				varBind.add(rs.getString("CONCENTRACION_CARTERA_VIGENTE") == null ? "" :
							rs.getString("CONCENTRACION_CARTERA_VIGENTE"));
				varBind.add(IC_SOLICITUD);

				log.debug("qry :: " + queryAux);
				log.debug("varBind :: " + varBind);
				ps1 = con.queryPrecompilado(queryAux, varBind);
				ps1.executeUpdate();
				ps1.clearParameters();
				ps1.close();
			}
			rs.close();
			ps.close();

		    boolean ban_inserta = this.insertaViabilidadCedula(con, IC_SOLICITUD, tipo_cartera, estatus, usuario);

			return exito;

		} catch (Throwable t) {
		    exito = false;
			throw new AppException("guardaIndicadoresFinancieros(Error): Error al guardar la informaci�n en cedi_ifnb_aspirante",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				if (ps1 != null)
					ps1.close();
			} catch (Exception t) {
				log.error("guardaIndicadoresFinancieros():: Error al cerrar Recursos: " + t.getMessage());
			}
		    if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
		        con.cierraConexionDB();
			}

		}

	}

	public String getTipoCartera(String IC_SOLICITUD, String pregunta) {
		log.info("getTipoCartera(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
	    PreparedStatement ps2 = null;
	    ResultSet rs2 = null;
		List varBind = new ArrayList();
		String qryConsulta = "";
		AccesoDB con = new AccesoDB();
		HashMap datos = new HashMap();
		List registros = new ArrayList();
		String opc_1 = "";
		String opc_2 = "";
		String opc_3 = "";
		String opc_1_r = "";
		String opc_2_r = "";

		String tipo_cartera = "";

		try {
			con.conexionDB();
		    List respP4 = new ArrayList();
			String subQueryP4 = "   select IC_RESPUESTA_PARAMETRIZACION  from cedi_respuesta_parametrizacion where IC_PREGUNTA = 4 order by IC_RESPUESTA_PARAMETRIZACION";
		    ps2 = con.queryPrecompilado(subQueryP4);
		    rs2 = ps2.executeQuery();
		    while (rs2.next()) {
		        respP4.add((String)rs2.getString("IC_RESPUESTA_PARAMETRIZACION"));
		    }
			rs2.close();
		    rs2.close();
			
			//Se obtiene el tipo de cartera
			HashMap datosTP = new HashMap();
			List registrosTP = new ArrayList();
			qryConsulta =
						 "  select cri.IC_RESPUESTA_PARAMETRIZACION, cri.CG_RESPUESTA_LIBRE" +
						 "  from cedi_respuesta_parametrizacion crp, cedi_respuesta_ifnb cri" +
						 "   where crp.IC_RESPUESTA_PARAMETRIZACION = cri.IC_RESPUESTA_PARAMETRIZACION" + "  and crp.IC_PREGUNTA = ?" +
						 "  AND cri.IC_SOLICITUD = ?";

			varBind = new ArrayList();
			varBind.add(pregunta);
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				datosTP = new HashMap();
				datosTP.put("IC_RESPUESTA_PARAMETRIZACION",
							rs.getString("IC_RESPUESTA_PARAMETRIZACION") == null ? "" :
							rs.getString("IC_RESPUESTA_PARAMETRIZACION"));
				datosTP.put("CG_RESPUESTA_LIBRE",
							rs.getString("CG_RESPUESTA_LIBRE") == null ? "" : rs.getString("CG_RESPUESTA_LIBRE"));
				registrosTP.add(datosTP);
			}
			rs.close();
			ps.close();

			int num_opc_elegidas = registrosTP.size();

			if (num_opc_elegidas > 0) {
				HashMap hmInfoR = (HashMap) registrosTP.get(0);
				Iterator it = hmInfoR.keySet().iterator();
			    int posi = 0;
				while (it.hasNext()) {
					String key = (String) it.next();
					if (num_opc_elegidas == 1) {
						if (key.equals("IC_RESPUESTA_PARAMETRIZACION")) {
							opc_1 = (String) hmInfoR.get(key);
						    log.info("opc_1 " + opc_1);
						    for(int i = 0; i < respP4.size(); i++){
							    
							    log.info("respP4 " + respP4.get(i));
								if (opc_1.equals(respP4.get(i))) {
									if(i==0){
									    tipo_cartera = "P";
									}else if(i==1){
									    tipo_cartera = "M";
									}else if(i==2){
									    tipo_cartera = "";
									}
								}
							}
							posi++;
							log.info("tipo_cartera " + tipo_cartera);
						}
					} else if (num_opc_elegidas == 2 || num_opc_elegidas == 3) {
						HashMap hmInfoR1 = (HashMap) registrosTP.get(1);
						if (key.equals("IC_RESPUESTA_PARAMETRIZACION")) {
							opc_1 = (String) hmInfoR.get(key);
							opc_2 = (String) hmInfoR1.get(key);
							log.info("opc_1 " + opc_1);
							log.info("opc_2 " + opc_2);
						} else if (key.equals("CG_RESPUESTA_LIBRE")) {
							opc_1_r = (String) hmInfoR.get(key);
							opc_2_r = (String) hmInfoR1.get(key);
							log.info("opc_1_r " + opc_1_r);
							log.info("opc_2_r " + opc_2_r);
						}


						if ((!opc_1.equals("") && !opc_2.equals("")) && (!opc_1_r.equals("") && !opc_2_r.equals(""))) {
							double valor1 = Double.parseDouble(opc_1_r);
							double valor2 = Double.parseDouble(opc_2_r);

							if (valor1 >= valor2) {
								tipo_cartera = "P";
							}else {
								tipo_cartera = "M";
							}
						}
						log.debug("tipo_cartera " + tipo_cartera);
					}
				}
			}
			log.debug("opc_1 " + opc_1);
			log.debug("opc_2 " + opc_2);
			log.debug("opc_3 " + opc_3);
			log.debug("tipo_cartera " + tipo_cartera);

		} catch (Throwable t) {
			throw new AppException("getTipoCartera(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getTipoCartera():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getTipoCartera(S)");
		}
		return tipo_cartera;
	}

	/**
	 * Funci�n donde obtenemos los resultados de los indicadores financieros
	 * @param IC_SOLICITUD
	 * @param pregunta
	 * @return
	 */
	public List getStoreIndicadorResultado(String IC_SOLICITUD, String pregunta) {
		log.info("CediEJB::getStoreIndicadorResultado(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
	    PreparedStatement ps1 = null;
	    ResultSet rs1 = null;
		List varBind = new ArrayList();
		HashMap datos = new HashMap();
		List registros = new ArrayList();

		boolean ban_aprobacion = true;
		double FG_CARTERA = 0;
		double CONC_ACRED_PM = 0;
		double CONC_ACRED_PF = 0;
		double CONC_CART_VIG = 0;
		double FG_SALDO_VIGENTE_PRINC_PM = 0;
		double FG_SALDO_VIGENTE_PRINC_PF = 0;
		double SUMA_10_PRINCIPALES = 0;

		try {
			con.conexionDB();
			String tipo_cartera = this.getTipoCartera(IC_SOLICITUD, pregunta);
			String qry = " select  cif.IC_ANIO,    " +
						 "   round((cif.FG_CAPITAL_CONTABLE/ (/* Formatted on 2015/10/03 16:28 (Formatter Plus v4.8.8) */ SELECT fn_valor_compra FROM com_tipo_cambio WHERE ic_moneda = 40 AND ROWID = (SELECT MAX (ROWID) FROM com_tipo_cambio  WHERE ic_moneda = 40))),2) AS CARTERA_PYME,  " +
						 "  round((cif.FG_CAPITAL_CONTABLE*0.30),2) as CONCENTRACION_ACRE_PM, cs.FG_SALDO_VIGENTE_PRINC_PM, " +
						 "  round((cif.FG_CAPITAL_CONTABLE*0.10),2) as CONCENTRACION_ACRE_PF,  cs.FG_SALDO_VIGENTE_PRINC_PF,   " +
						 "  round( (cif.FG_CARTERA_TOTAL*0.35),2) as CONCENTRACION_CARTERA_VIGENTE, (select SUM(FG_SALDO) from cedi_saldo_acreditado where ic_solicitud = ? AND CC_TIPO_ACREDITADO = 'V') as SUMA_10_PRINCIPALES " +
						 "  from cedi_informacion_financiera cif, cedi_solicitud cs " + "	where cif.IC_SOLICITUD = ? " +
						 "	AND cif.IC_SOLICITUD = cs.IC_SOLICITUD " +
						 "	AND cif.IC_ANIO = cs.IG_ANIO_INF_FINANCIERA ";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			varBind.add(IC_SOLICITUD);

			log.debug("qry ::  " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				FG_CARTERA = rs.getDouble("CARTERA_PYME");
				CONC_ACRED_PM = rs.getDouble("CONCENTRACION_ACRE_PM");
				CONC_ACRED_PF = rs.getDouble("CONCENTRACION_ACRE_PF");
				CONC_CART_VIG = rs.getDouble("CONCENTRACION_CARTERA_VIGENTE");

				FG_SALDO_VIGENTE_PRINC_PF = rs.getDouble("FG_SALDO_VIGENTE_PRINC_PF");
				FG_SALDO_VIGENTE_PRINC_PM = rs.getDouble("FG_SALDO_VIGENTE_PRINC_PM");
				SUMA_10_PRINCIPALES = rs.getDouble("SUMA_10_PRINCIPALES");


			}
			rs.close();
			ps.close();
			// Destino de Recursos solicitados:
			double cantidad_cartera = 0;
			int cont_opc =0;
			if (tipo_cartera.equals("P")) {
			    cantidad_cartera = 9000000;
			}else if (tipo_cartera.equals("M")) {
			    cantidad_cartera = 5000000;
			}
			
			
		    qry = "	select IC_RESPUESTA_PARAMETRIZACION " +
		                "  from cedi_respuesta_parametrizacion " +
		                 "  where ic_pregunta = ? " +
		                 "  ORDER BY IC_RESPUESTA_PARAMETRIZACION ";
		    varBind = new ArrayList();
		    varBind.add("4");

		    log.debug("qry ::  " + qry);
		    log.debug("varBind ::  " + varBind);
		    ps = con.queryPrecompilado(qry, varBind);
		    rs = ps.executeQuery();
		    while (rs.next()) {
		        cont_opc++;
		        String ic_resp = rs.getString("IC_RESPUESTA_PARAMETRIZACION") == null ? "" :rs.getString("IC_RESPUESTA_PARAMETRIZACION");
		        qry = " SELECT cri.ic_respuesta_parametrizacion " +
		                    "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
		                     " 	WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion AND crp.ic_pregunta = cp.ic_pregunta AND cri.ic_solicitud = ? AND crp.ic_pregunta = 4 " +
		                     "  and cri.ic_respuesta_parametrizacion = ? ";
		        varBind = new ArrayList();
		        varBind.add(IC_SOLICITUD);
		        varBind.add(ic_resp);

		        log.debug("qry ::  " + qry);
		        log.debug("varBind ::  " + varBind);
		        ps1 = con.queryPrecompilado(qry, varBind);
		        rs1 = ps1.executeQuery();
		        if (rs1.next()) {
		            String ic_resp_sel = rs1.getString("IC_RESPUESTA_PARAMETRIZACION") == null ? "" :rs1.getString("IC_RESPUESTA_PARAMETRIZACION");
					if(!ic_resp_sel.equals("")){
						if(cont_opc==1){
						    datos = new HashMap();
						    datos.put("RESULTADO", FG_CARTERA);
							datos.put("CRITERIO", "Cartera Pyme");
							datos.put("INDICADOR_CUMPLIMIENTO", "> � = 9 millones UDI");
											
							if (FG_CARTERA >= 9000000 ) {
								datos.put("INDICADOR", "S");
							} else {
								datos.put("INDICADOR", "N");
							}
						    registros.add(datos);
						}
					    if(cont_opc==2){
					        datos = new HashMap();
					        datos.put("RESULTADO", FG_CARTERA);
					        datos.put("CRITERIO", "Microcr�dito");
							datos.put("INDICADOR_CUMPLIMIENTO", "> � = 5 millones UDI");
					        if (FG_CARTERA >= 5000000) {
					           datos.put("INDICADOR", "S");
					        } else {
					          datos.put("INDICADOR", "N");
					        }
					        registros.add(datos);
					    }
					    if(cont_opc==3){
					        datos = new HashMap();
					        datos.put("RESULTADO", "--");
					        datos.put("CRITERIO", "Otro");
					        datos.put("INDICADOR_CUMPLIMIENTO", "--");
							datos.put("INDICADOR", "--");
					        registros.add(datos);
					    }
						
					}

		        }
		        rs1.close();
		        ps1.close();

		    }
		    rs.close();
		    ps.close();
			
			
		    
			// ******* 
			datos = new HashMap();
			datos.put("CRITERIO", "Concentraci�n m�xima de Acreditado Persona Moral");
			datos.put("INDICADOR_CUMPLIMIENTO", "< � = al 30% del capital contable");
			datos.put("RESULTADO", CONC_ACRED_PM);
		    if(CONC_ACRED_PM>0){
				if (FG_SALDO_VIGENTE_PRINC_PM > CONC_ACRED_PM) {
					datos.put("INDICADOR", "N");
				} else {
					datos.put("INDICADOR", "S");
				}
			}else{
				datos.put("INDICADOR", "N");	
			}
			registros.add(datos);

			datos = new HashMap();
			datos.put("CRITERIO", "Concentraci�n m�xima de Acreditado Persona F�sica");
			datos.put("INDICADOR_CUMPLIMIENTO", "< � = al 10% del capital contable");
			datos.put("RESULTADO", CONC_ACRED_PF);
			if(CONC_ACRED_PF>0){
				if (FG_SALDO_VIGENTE_PRINC_PF > CONC_ACRED_PF) {
					datos.put("INDICADOR", "N");
				} else {
					datos.put("INDICADOR", "S");
				}
			}else{
			    datos.put("INDICADOR", "N");
			}
			registros.add(datos);

			datos = new HashMap();
			datos.put("CRITERIO", "Concentraci�n de Cartera");
			datos.put("INDICADOR_CUMPLIMIENTO", "< � = al 35% de la suma de los 10 principales acreditados vigentes");
			datos.put("RESULTADO", CONC_CART_VIG);
		    if(CONC_CART_VIG>0){
				if (SUMA_10_PRINCIPALES > CONC_CART_VIG) {
	
					datos.put("INDICADOR", "N");
				} else {
					datos.put("INDICADOR", "S");
				}
			}else{
			    datos.put("INDICADOR", "N");
			}
			registros.add(datos);
			return registros;

		} catch (Throwable t) {
			throw new AppException("getStoreIndicadorResultado(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getStoreIndicadorResultado():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getStoreIndicadorResultado(S)");
		}

	}

	public String generarArchivoCedula(List datoGrales) {
		log.info(" generarArchivoCedula (E)");

		PreparedStatement ps = null;
		ResultSet rs = null;
		String nombreArchivo = "";
		ComunesPDF pdfDoc = new ComunesPDF();

		String pais = datoGrales.get(0).toString();
		String nombre = datoGrales.get(2).toString();
		String nombreUsua = datoGrales.get(3).toString();
		String logo = datoGrales.get(4).toString();
		String strDirectorioTemp = datoGrales.get(5).toString();
		String strDirectorioPublicacion = datoGrales.get(6).toString();
		String clave_solicitud = datoGrales.get(7).toString();
		String estatus = datoGrales.get(10).toString();
		String pantalla = datoGrales.get(11).toString();// 
		if(pantalla.equals("CALIFICATE")||pantalla.equals("OTRO_ARCHIVOS")){

			List listInfoAspirante = new ArrayList();
			List listInfoIndicador = new ArrayList();
			List listInfoCualitativa = new ArrayList();
			//String estatus = this.getEstatusSoli(clave_solicitud);
			HashMap hmAspirante = new HashMap();
			HashMap hmIndicador = new HashMap();
			HashMap hmCualitativa = new HashMap();
	
			try {
	
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(1, strDirectorioTemp + nombreArchivo);
	
				String meses[] = {
							 "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre",
							 "Noviembre", "Diciembre"
				};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0, 2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3, 5)) - 1];
				String anioActual = fechaActual.substring(6, 10);
				String horaActual = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
				pdfDoc.encabezadoConImagenes(pdfDoc, pais, "", "", "", "", logo, strDirectorioPublicacion);
	
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
							   " ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Resultado de elegibilidad", "formas", ComunesPDF.CENTER);
				listInfoCualitativa = this.getInformacionInfoCualitativa(clave_solicitud);
				if (estatus.equals("2")||estatus.toUpperCase().equals("SOLICITADA")||estatus.equals("3")||estatus.toUpperCase().equals("ASIGNADA-PREAN�LISIS")||estatus.equals("4")||estatus.toUpperCase().equals("EN PRE-AN�LISIS IF")||estatus.equals("5")||estatus.toUpperCase().equals("EN AN�LISIS")) {
					pdfDoc.addText("C�DULA ELECTR�NICA DE ACEPTACI�N", "formas", ComunesPDF.CENTER);
				} else {
					pdfDoc.addText("C�DULA ELECTR�NICA DE RECHAZO", "formas", ComunesPDF.CENTER);
				}
	
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				 
				listInfoAspirante = this.getInformacionAspirante(clave_solicitud, estatus);
				HashMap hmGenaral =(HashMap)this.getInformacionCualiPDF(clave_solicitud);
				HashMap credVinc = (HashMap)hmGenaral.get("P3");
				String leyeCred = "";
				String existe_credVin = (String)credVinc.get("CS_CREDITO_VINCULADO");
				if(existe_credVin.equals("S")){
					leyeCred = "El Intermediario Financieros SI cuenta con Cr�ditos Vinculados, se recomienda su revisi�n";
				}else{
					leyeCred = "El Intermediario NO cuenta con cr�ditos vinculados";
				}
				float[] tamanio = { 50, 50 };
				pdfDoc.setTable(2, 60, tamanio, 5);
				pdfDoc.setCell("Folio Solicitud ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(0);
				pdfDoc.setCell((String) hmAspirante.get("IC_SOLICITUD"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Raz�n Social", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(1);
				pdfDoc.setCell((String) hmAspirante.get("CG_RAZON_SOCIAL"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("R.F.C. ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(2);
				pdfDoc.setCell((String) hmAspirante.get("CG_RFC"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Estado ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(3);
				pdfDoc.setCell((String) hmAspirante.get("ESTADO"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Tel�fono ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(4);
				pdfDoc.setCell((String) hmAspirante.get("CG_TELEFONO"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Correo Electr�nico ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(5);
				pdfDoc.setCell((String) hmAspirante.get("CG_EMAIL"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Destino de los Recursos solicitados", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(6);
				pdfDoc.setCell((String) hmAspirante.get("CS_TIPO_CARTERA"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Fecha de registro ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(7);
				pdfDoc.setCell((String) hmAspirante.get("DF_MOVIMIENTO"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Hora de registro ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(8);
				pdfDoc.setCell((String) hmAspirante.get("HORA"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Usuario ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(9);
				pdfDoc.setCell((String) hmAspirante.get("IG_USUARIO")+"-"+nombreUsua, "formas", ComunesPDF.LEFT);
				pdfDoc.addTable();
				float[] tamaniom = { 100 };
				pdfDoc.setTable(1, 60, tamaniom, 0);
				if (estatus.equals("2")||estatus.toUpperCase().equals("SOLICITADA")||estatus.equals("3")||estatus.toUpperCase().equals("ASIGNADA-PREAN�LISIS")||estatus.equals("4")||estatus.toUpperCase().equals("EN PRE-AN�LISIS IF")||estatus.equals("5")||estatus.toUpperCase().equals("EN AN�LISIS")) {
					pdfDoc.setCell("La informaci�n que fue proporcionada cumple con los requisitos m�nimos solicitados, en breve un Ejecutivo se pondr� en contacto con usted para continuar con su ejercicio de viabilidad para formar parte de la red de Intermediarios de NAFINSA.\n" +
								   "Para cualquier duda o aclaraci�n favor de contactarnos al correo electr�nico cedi@nafin.gob.mx o al tel�fono 01 800 NAFINSA (623 4672).\n\n"+
									leyeCred+"\n");
					pdfDoc.setCell("*El resultado de esta c�dula es un ejercicio de elegibilidad, por lo que no es una autorizaci�n de cr�dito ni un monto de l�nea a otorgar. La solicitud requiere an�lisis experto y autorizaci�n del Comit� correspondiente en NAFINSA.",
								   "formasB", ComunesPDF.JUSTIFIED);
				}else{
					pdfDoc.setCell("El proyecto no es elegible para financiamiento.\n\n" + 
									"En respuesta a la informaci�n Financiera que amablemente requisit�, y con base al estudio pertinente realizado, el resultado no fue favorable para incorporarse a la Red de Intermediarios Financieros de Nacional Financiera.\n\n" +
								   "Para cualquier duda o aclaraci�n favor de contactarnos al correo electr�nico cedi@nafin.gob.mx o al tel�fono 01 800 NAFINSA (623 4672).\n\n"+
									"Agradecemos su inter�s en trabajar con Nacional Financiera.\n",
								   "formas", ComunesPDF.JUSTIFIED);
				}
				
				pdfDoc.addTable();
	
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Indicadores y Criterios Financieros ", "formas", ComunesPDF.LEFT);
				listInfoIndicador = this.getInformacionIndicadores(clave_solicitud,pantalla);
			   
				float[] tamanioInd = { 150, 80, 80, 600 };
				pdfDoc.setTable(4, 100, tamanioInd, 0);
				pdfDoc.setCell("Raz�n Financiera", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Su resultado", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Validaci�n", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Requisitos m�nimos para incorporaci�n", "celda01", ComunesPDF.CENTER);
				for (int i = 0; i < listInfoIndicador.size(); i++) {
					
					hmIndicador = (HashMap) listInfoIndicador.get(i);
					pdfDoc.setCell((String) hmIndicador.get("CG_NOMBRE"), "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String) hmIndicador.get("CS_RESULTADO"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String) hmIndicador.get("CS_VIABLE"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String) hmIndicador.get("CS_REQUISITO"), "formas", ComunesPDF.LEFT);
				}
				pdfDoc.addTable();
	
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Informaci�n Cualitativa", "formas", ComunesPDF.LEFT);
				
				float[] tamanioCual = { 600, 80, 80, 140 };
				pdfDoc.setTable(4, 100, tamanioCual, 0);
				pdfDoc.setCell("Raz�n Financiera", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Su resultado", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Validaci�n", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Requisitos m�nimos para incorporaci�n", "celda01", ComunesPDF.CENTER);
				for (int i = 0; i < listInfoCualitativa.size(); i++) {
					hmCualitativa = (HashMap) listInfoCualitativa.get(i);
					pdfDoc.setCell((String) hmCualitativa.get("CG_NOMBRE"), "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String) hmCualitativa.get("CS_RESULTADO"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String) hmCualitativa.get("CS_VIABLE"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String) hmCualitativa.get("CS_REQUISITO"), "formas", ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Paso 1. Datos Generales", "formas", ComunesPDF.LEFT);
				//tamanio = { 50, 50 };
				pdfDoc.setTable(2, 60, tamanio, 0);
				
			   // pdfDoc.setTable(2, 11, tamanio, 0);
				pdfDoc.setCell("Raz�n Social", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(1);
				pdfDoc.setCell((String) hmAspirante.get("CG_RAZON_SOCIAL"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("Nombre Comercial", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(11);
				pdfDoc.setCell((String) hmAspirante.get("CG_NOMBRE_COMERCIAL"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("R.F.C. empresarial ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(2);
				pdfDoc.setCell((String) hmAspirante.get("CG_RFC"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("Domicilio(Calle y N�mero) ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(12);
				pdfDoc.setCell((String) hmAspirante.get("CG_CALLE"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("Colonia ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(13);
				pdfDoc.setCell((String) hmAspirante.get("CG_COLONIA"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("Estado", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(3);
				pdfDoc.setCell((String) hmAspirante.get("ESTADO"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("Delegaci�n o Municipio", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(16);
				pdfDoc.setCell((String) hmAspirante.get("IC_MUNICIPIO"), "formas", ComunesPDF.LEFT);
				
				pdfDoc.setCell("C�digo Postal empresarial", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(17);
				pdfDoc.setCell((String) hmAspirante.get("CG_CP"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Nombre de Contacto", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(18);
				pdfDoc.setCell((String) hmAspirante.get("CG_CONTACTO"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Correo Electr�nico empresarial ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(5);
				pdfDoc.setCell((String) hmAspirante.get("CG_EMAIL"), "formas", ComunesPDF.LEFT);
				pdfDoc.setCell("Tel�fono empresarial ", "celda02", ComunesPDF.LEFT);
				hmAspirante = (HashMap) listInfoAspirante.get(4);
				pdfDoc.setCell((String) hmAspirante.get("CG_TELEFONO"), "formas", ComunesPDF.LEFT);
				pdfDoc.addTable();
				
				
				
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Paso 2. Informaci�n Cualitativa", "formas", ComunesPDF.LEFT);
				float[] tamanioP2 = { 600, 200 };
				pdfDoc.setTable(2, 100, tamanioP2, 0);
				List datoP2 = (ArrayList)hmGenaral.get("P2");
				for(int i = 0 ; i < datoP2.size(); i++){
					HashMap hmInfo = (HashMap)datoP2.get(i);
					pdfDoc.setCell((String)hmInfo.get("CG_PREGUNTA"), "celda02", ComunesPDF.LEFT);
					pdfDoc.setCell((String)hmInfo.get("RESPUESTA"), "formas", ComunesPDF.CENTER);
					
				}
				pdfDoc.addTable();
				
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Paso 3. Cr�dito Vinculado", "formas", ComunesPDF.LEFT);
				
				pdfDoc.setTable(2, 100, tamanio, 0);
				
				HashMap datoP3 = (HashMap)hmGenaral.get("P3");
				String credito_vinculado = (String)datoP3.get("CS_CREDITO_VINCULADO");
				pdfDoc.setCell((String)datoP3.get("CG_MONTO_CRED_VINCULADO"), "celda02", ComunesPDF.LEFT);
				if(credito_vinculado.equals("S")){
					pdfDoc.setCell("Si, 	"+(String)datoP3.get("FG_MONTO_CRED_VINCULADO"), "formas", ComunesPDF.CENTER);
				}else{
					pdfDoc.setCell("No ", "formas", ComunesPDF.CENTER);
				}
			   
				pdfDoc.addTable();
				
				
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Paso 4. Informaci�n Financiera", "formas", ComunesPDF.LEFT);
				
			   
				
				
				HashMap datoP4 = (HashMap)hmGenaral.get("P4AnioMes");
				int mes = Integer.parseInt((String)datoP4.get("IG_MES_INF_FINANCIERA"));
				int anio = Integer.parseInt((String)datoP4.get("IG_ANIO_INF_FINANCIERA"));
				pdfDoc.addText("Fecha de la informaci�n Financiera "+meses[mes-1]+" "+(String)datoP4.get("IG_ANIO_INF_FINANCIERA"), "formas", ComunesPDF.LEFT);
				float[] tamInfoFanan = { 200, 200, 200, 200 };
				pdfDoc.setTable(4, 100, tamInfoFanan, 0);
				List datoP4G = (ArrayList)hmGenaral.get("P4Grid");
				pdfDoc.setCell("Concepto", "celda01", ComunesPDF.LEFT);
				pdfDoc.setCell(String.valueOf((anio-2)), "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(String.valueOf((anio-1)), "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(meses[mes-1]+" "+String.valueOf((anio)), "celda01", ComunesPDF.CENTER);
				for(int i = 0; i < datoP4G.size(); i++){
					HashMap hmInfo = (HashMap)datoP4G.get(i);
					pdfDoc.setCell((String)hmInfo.get("CS_CONCEPTO"), "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)hmInfo.get("ANIO_1"), "formas", ComunesPDF.CENTER);	
					pdfDoc.setCell((String)hmInfo.get("ANIO_2"), "formas", ComunesPDF.CENTER); 
					pdfDoc.setCell((String)hmInfo.get("ANIO_3"), "formas", ComunesPDF.CENTER); 
					
				}
				pdfDoc.addTable();
				float[] tamanioSal = { 50, 50 };
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Paso 5. Saldos", "formas", ComunesPDF.LEFT);
				
				pdfDoc.addText("Cifras en pesos a "+meses[mes-1]+"-"+anio, "formas", ComunesPDF.CENTER);
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				
			
			   
				
				pdfDoc.setTable(2, 40, tamanioSal, 0);
				List P5Acreditados = (ArrayList)hmGenaral.get("P5Acreditados");
				pdfDoc.setCell("Acredtitados Vigentes", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo", "celda01", ComunesPDF.CENTER);
				
				for(int i = 0; i < P5Acreditados.size(); i++){
					HashMap hmInfo = (HashMap)P5Acreditados.get(i);
					pdfDoc.setCell((String)hmInfo.get("IC_SALDO_ACREDITADO"), "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)hmInfo.get("FG_SALDO"), "formas", ComunesPDF.CENTER);  
				   
					
				}
				pdfDoc.addTable();
			   
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				
				pdfDoc.setTable(2, 40, tamanioSal, 0);
				List P5AcreditadosInc = (ArrayList)hmGenaral.get("P5AcreditadosInc");
				pdfDoc.setCell("Acreditados en incumplimiento de pago", "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Saldo", "celda01", ComunesPDF.CENTER);
				
				for(int i = 0; i < P5AcreditadosInc.size(); i++){
					HashMap hmInfo = (HashMap)P5AcreditadosInc.get(i);
					pdfDoc.setCell((String)hmInfo.get("IC_SALDO_ACREDITADO"), "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)hmInfo.get("FG_SALDO"), "formas", ComunesPDF.CENTER);  
				   
					
				}
				pdfDoc.addTable();
			   
				HashMap P5Saldos = (HashMap)hmGenaral.get("P5Saldos");
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Saldo Vigente de la Principal Persona Moral acreditada con actividad Empresarial:", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
			   
				
				pdfDoc.setTable(2, 60, tamanio, 0);
				pdfDoc.setCell("Cifras en pesos a "+meses[mes-1]+"-"+anio, "celda02", ComunesPDF.LEFT);
			 
				pdfDoc.setCell((String)P5Saldos.get("FG_SALDO_VIGENTE_PRINC_PM"), "formas", ComunesPDF.CENTER);
			   
				pdfDoc.addTable();
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("Saldo Vigente de la Principal Persona F�sica acreditada con actividad Empresaria:", "formas", ComunesPDF.CENTER);
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				
				pdfDoc.setTable(2, 60, tamanio, 0);
				pdfDoc.setCell("Cifras en pesos a "+meses[mes-1]+"-"+anio, "celda02", ComunesPDF.LEFT);
				pdfDoc.setCell((String)P5Saldos.get("FG_SALDO_VIGENTE_PRINC_PF"), "formas", ComunesPDF.CENTER);
				
				pdfDoc.addTable();
				
				
				
				pdfDoc.endDocument();
				
				
				
			} catch (Throwable e) {
				log.error(" Error  generarArchivoCedula (S) " + e);
				throw new AppException("Error al generar el archivo ", e);
			} finally {
				try {
					if (rs != null)
						rs.close();
					if (ps != null)
						ps.close();
				} catch (Exception t) {
					log.error("generarArchivoCedula():: Error al cerrar Recursos: " + t.getMessage());
				}
				try {
				} catch (Exception e) {
				}
				log.info("  generarArchivoCedula (S) ");
			}
		}else{
		    nombreArchivo =  this.descargaArchivosEdoFinan(clave_solicitud, strDirectorioTemp, "CEDULA_COMPLETA");
		}
		return nombreArchivo;
	}


	/**
	 *Funci�n para insertar los movimientos de la solicitud
	 * @param con
	 * @param IC_SOLICITUD clave de la solicitud
	 * @param IG_USUARIO usuario que realiso el movimiento
	 * @param ESTATUS Nuevo estatus que tendra la solicitud
	 * @param MOVIMIENTO Accion que cada usuario realice como Registr de ususario, Registro de Solicitud, ect
	 * @return
	 */
	private boolean setMovimientoSolicitus(AccesoDB con, String IC_SOLICITUD, String IG_USUARIO, String ESTATUS,
										  String MOVIMIENTO, String Detalle) {
		log.info("setMovimientoSolicitus (E)");

		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		List varBind = new ArrayList();
		String existe_anio = "";
		try {

			String qry = " INSERT INTO cedi_solicitud_movimiento (IC_SOLICITUD_CAMBIO_EST,IC_SOLICITUD,IG_USUARIO,DF_MOVIMIENTO,IC_ESTATUS_SOLICITUD,IC_MOVIMIENTO,CS_DETALLE)" +
						 " VALUES((SELECT MAX (IC_SOLICITUD_CAMBIO_EST)+1 FROM cedi_solicitud_movimiento),?,?,sysdate,?,?,?)";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			varBind.add(IG_USUARIO);
			varBind.add(ESTATUS);
			varBind.add(MOVIMIENTO);
		    varBind.add(Detalle);
			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();


		} catch (Throwable t) {
			exito = false;
			throw new AppException("setMovimientoSolicitus(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("setMovimientoSolicitus():: Error al cerrar Recursos: " + t.getMessage());
			}

			log.info("CediEJB::setMovimientoSolicitus(S)");
		}
		return exito;
	}
	
	public boolean setMovimientoSolicitus(String IC_SOLICITUD, String IG_USUARIO, String ESTATUS,
										  String MOVIMIENTO, String Detalle) {
		log.info("setMovimientoSolicitus (E)");
	    AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		List varBind = new ArrayList();
		String existe_anio = "";
		try {
		    con.conexionDB();
			String qry = " INSERT INTO cedi_solicitud_movimiento (IC_SOLICITUD_CAMBIO_EST,IC_SOLICITUD,IG_USUARIO,DF_MOVIMIENTO,IC_ESTATUS_SOLICITUD,IC_MOVIMIENTO,CS_DETALLE)" +
						 " VALUES((SELECT MAX (IC_SOLICITUD_CAMBIO_EST)+1 FROM cedi_solicitud_movimiento),?,?,sysdate,?,?,?)";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			varBind.add(IG_USUARIO);
			varBind.add(ESTATUS);
			varBind.add(MOVIMIENTO);
			varBind.add(Detalle);
			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();


		} catch (Throwable t) {
			exito = false;
			throw new AppException("setMovimientoSolicitus(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("setMovimientoSolicitus():: Error al cerrar Recursos: " + t.getMessage());
			}
		    if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
		        con.cierraConexionDB();
			}

			log.info("CediEJB::setMovimientoSolicitus(S)");
		}
		return exito;
	}

	/**
	 * Funci�n que nos indica si la solicitud es viable validando  la informaci�n Cualitativa
	 * @param con
	 * @param clave_solicitud
	 * @return
	 */
	/*public boolean viabilidadInfoCualitativa(AccesoDB con, String clave_solicitud) {
		log.info("viabilidadInfoCualitativa(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";

		String ic_pregunta = "";
		String ic_resp = "";
		String opc_resp = "";
		String resp_libre = "";


		boolean ban_aprobacion = true;
		try {
			qry = "";
			qry = "  SELECT   crp.ic_pregunta, cri.ic_respuesta_parametrizacion,crp.cg_opcion_respuesta, cri.cg_respuesta_libre " +
						 "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
						 "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
						 "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
						 "  AND crp.ic_pregunta IN (7, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19, 20) ";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				ic_pregunta = rs.getString("ic_pregunta") == null ? "" : rs.getString("ic_pregunta");
				ic_resp = rs.getString("ic_respuesta_parametrizacion") == null ? "" :
						  rs.getString("ic_respuesta_parametrizacion");
				opc_resp = rs.getString("cg_opcion_respuesta") == null ? "" : rs.getString("cg_opcion_respuesta");
				resp_libre = rs.getString("cg_respuesta_libre") == null ? "" : rs.getString("cg_respuesta_libre");
				int anios = Integer.parseInt(resp_libre);
				if (ic_pregunta.equals("7")) {
					if (anios < 2) {
						ban_aprobacion = false;
					}
				} else {
					if (!opc_resp.equals("SI")) {
						ban_aprobacion = false;
					}
				}
			}
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();


		} catch (Throwable t) {
			throw new AppException("viabilidadInfoCualitativa(Error): Error al guardar la informaci�n", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("viabilidadInfoCualitativa():: Error al cerrar Recursos: " + t.getMessage());
			}
		}
		return ban_aprobacion;
	}*/

	/**
	 * Funci�n que guarda la informacion de la c�dula
	 * @param con
	 * @param IC_ELEMENTO clave del elemento que se valido
	 * @param VIABLE valor de la validaci�n
	 * @param CAMPO de la table que se modificara
	 */
	private void insertaActualizaCedula(AccesoDB con, String IC_SOLICITUD, String IC_ELEMENTO, String VIABLE,
									   String CAMPO) {
		log.info("insertaActualizaCedula (E)");

		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String existe = "";
		boolean exito = true;
		try {

			String qryConsulta =
						 " SELECT DECODE(COUNT(*),0,'N','S') AS EXISTE " + " FROM cedi_cedula" +
						 " WHERE IC_ELEMENTO_CEDULA = ?" + " AND IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(IC_ELEMENTO);
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				existe = rs.getString("EXISTE");
			}
			rs.close();
			ps.close();
			if (existe.equals("N")) {
				String strSQL = " INSERT INTO cedi_cedula(IC_ELEMENTO_CEDULA,IC_SOLICITUD, CS_VIABLE) " +
								"  VALUES(?,?,?) ";
				varBind = new ArrayList();
				varBind.add(IC_ELEMENTO);
				varBind.add(IC_SOLICITUD);
				varBind.add(VIABLE);
				log.debug("qry :: " + strSQL);
				log.debug("varBind :: " + varBind);
				ps = con.queryPrecompilado(strSQL, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();

			} else {
				String strSQL = "  	UPDATE cedi_cedula " + "  	SET CS_VIABLE = ? " + " 	WHERE IC_ELEMENTO_CEDULA = ?" +
								" 	AND IC_SOLICITUD =  ?";
				varBind = new ArrayList();
				varBind.add(VIABLE);
				varBind.add(IC_ELEMENTO);
				varBind.add(IC_SOLICITUD);
				log.debug("qry :: " + strSQL);
				log.debug("varBind :: " + varBind);
				ps = con.queryPrecompilado(strSQL, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();
			}


		} catch (Throwable t) {
			exito = false;
			throw new AppException("insertaActualizaCedula(Error): Error al guardar la informaci�n en cedi_ifnb_aspirante",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("insertaActualizaCedula():: Error al cerrar Recursos: " + t.getMessage());
			}

		}

	}

	private boolean insertaViabilidadCedula(AccesoDB con, String clave_solicitud, String tipo_cartera, String estatus,
										   String usuario) {
		log.info("insertaViabilidadCedula(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";

		String ic_pregunta = "";
		String ic_resp = "";
		String opc_resp = "";
		String resp_libre = "";
		boolean exito = true;
		boolean exito_movimiento = true;
	    boolean ban_movi = true;

		double CAPITALIZACION = 0;
		double MOROSIDAD = 0;
		double COBERTURA = 0;
		double ROE = 0;
		double LIQUIDEZ = 0;
		double APALANCAMIENTO = 0;
		double FG_CARTERA = 0;
		double CONC_ACRED_PM = 0;
		double CONC_ACRED_PF = 0;
		double CONC_CART_VIG = 0;
		double FG_SALDO_VIGENTE_PRINC_PM = 0;
		double FG_SALDO_VIGENTE_PRINC_PF = 0;
		double SUMA_10_PRINCIPALES = 0;

		try {
			// Inserta la viabilidad de la informaci�n Cualitativa
			qry = "";
			qry = "  SELECT   crp.ic_pregunta, cri.ic_respuesta_parametrizacion,crp.cg_opcion_respuesta, cri.cg_respuesta_libre " +
						 "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
						 "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
						 "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
						 "  AND crp.ic_pregunta IN (7, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19, 20) ";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				ic_pregunta = rs.getString("ic_pregunta") == null ? "" : rs.getString("ic_pregunta");
				ic_resp = rs.getString("ic_respuesta_parametrizacion") == null ? "" :
						  rs.getString("ic_respuesta_parametrizacion");
				opc_resp = rs.getString("cg_opcion_respuesta") == null ? "" : rs.getString("cg_opcion_respuesta");
				resp_libre = rs.getString("cg_respuesta_libre") == null ? "" : rs.getString("cg_respuesta_libre");

				if (ic_pregunta.equals("7")) {
					if (!resp_libre.equals("")) {
						int anios = Integer.parseInt(resp_libre.replace(" ", ""));
						if (anios >= 2) {
							this.insertaActualizaCedula(con, clave_solicitud, "18", "S", "CS_VIABLE");
						} else {
							this.insertaActualizaCedula(con, clave_solicitud, "18", "N", "CS_VIABLE");
							exito = false;
						
						}
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "18", "N", "CS_VIABLE");
						exito = false;
					
					}
				} else if (ic_pregunta.equals("8")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "19", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "19", "N", "CS_VIABLE");
						exito = false;
					
					}
				} else if (ic_pregunta.equals("9")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "20", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "20", "N", "CS_VIABLE");
						exito = false;
						
					}
				} else if (ic_pregunta.equals("10")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "21", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "21", "N", "CS_VIABLE");
						exito = false;
					
					}
				} else if (ic_pregunta.equals("11")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "22", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "22", "N", "CS_VIABLE");
						exito = false;
					}
				} else if (ic_pregunta.equals("12")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "23", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "23", "N", "CS_VIABLE");
						exito = false;
					}
				} else if (ic_pregunta.equals("13")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "24", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "24", "N", "CS_VIABLE");
						exito = false;
					}
				} else if (ic_pregunta.equals("16")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "27", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "27", "N", "CS_VIABLE");
						exito = false;
					}
				} else if (ic_pregunta.equals("17")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "28", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "28", "N", "CS_VIABLE");
						exito = false;
					}
				} else if (ic_pregunta.equals("18")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "29", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "29", "N", "CS_VIABLE");
						exito = false;
					}
				} else if (ic_pregunta.equals("19")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "30", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "30", "N", "CS_VIABLE");
						exito = false;
					}
				} else if (ic_pregunta.equals("20")) {
					if (opc_resp.toUpperCase().equals("SI")) {
						this.insertaActualizaCedula(con, clave_solicitud, "31", "S", "CS_VIABLE");
					} else {
						this.insertaActualizaCedula(con, clave_solicitud, "31", "N", "CS_VIABLE");
						exito = false;
					}
				}
			}
		   
			rs.close();
			ps.close();
			// *** para completar el resto de las preguntas que no son motivo de rechazo
		    this.insertaActualizaCedula(con, clave_solicitud, "12", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "13", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "14", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "15", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "16", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "17", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "25", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "26", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "32", "S", "CS_VIABLE");
			
			log.debug("Exito movimientos "+exito);
			if (exito == false) {
			   
			    boolean actualizaMov = this.setMovimientoSolicitus(con, clave_solicitud, usuario, "7", "9","Rechazo Calif�cate Paso2. Informaci�n Cualitativa");
			    ban_movi = false;
			}
			// Inserta el resultado de viabilidad de los indicadores

			qry = "";
			qry = "  SELECT cs.fg_capitalizacion_anio3, cs.fg_morosidad_anio3,cs.fg_cobertura_reservas_anio3, cs.fg_roe_anio3, fg_liquidez_anio3,cs.fg_apalancamiento_anio3, cs.fg_cartera, cs.fg_conc_acred_pm,cs.fg_conc_acred_pf, cs.fg_conc_cart_vig, SUM(csa.FG_SALDO) AS SUMA_10_PRINCIPALES, cs.FG_SALDO_VIGENTE_PRINC_PM, cs.FG_SALDO_VIGENTE_PRINC_PF  " +
						 "  FROM cedi_solicitud cs, cedi_saldo_acreditado csa " + "  WHERE cs.ic_solicitud = ?" +
						 "  AND cs.ic_solicitud = csa.ic_solicitud" + "  AND csa.cc_tipo_acreditado = 'V'" +
						 "  group by   cs.fg_capitalizacion_anio3, cs.fg_morosidad_anio3,cs.fg_cobertura_reservas_anio3, cs.fg_roe_anio3, fg_liquidez_anio3,cs.fg_apalancamiento_anio3, cs.fg_cartera, cs.fg_conc_acred_pm,cs.fg_conc_acred_pf, cs.fg_conc_cart_vig, cs.FG_SALDO_VIGENTE_PRINC_PM, cs.FG_SALDO_VIGENTE_PRINC_PF  ";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				CAPITALIZACION = rs.getDouble("FG_CAPITALIZACION_ANIO3");
				MOROSIDAD = rs.getDouble("FG_MOROSIDAD_ANIO3");
				COBERTURA = rs.getDouble("FG_COBERTURA_RESERVAS_ANIO3");
				ROE = rs.getDouble("FG_ROE_ANIO3");
				LIQUIDEZ = rs.getDouble("FG_LIQUIDEZ_ANIO3");
				APALANCAMIENTO = rs.getDouble("FG_APALANCAMIENTO_ANIO3");
				FG_CARTERA = rs.getDouble("FG_CARTERA");
				CONC_ACRED_PM = rs.getDouble("FG_CONC_ACRED_PM");
				CONC_ACRED_PF = rs.getDouble("FG_CONC_ACRED_PF");
				CONC_CART_VIG = rs.getDouble("FG_CONC_CART_VIG");
				FG_SALDO_VIGENTE_PRINC_PM = rs.getDouble("FG_SALDO_VIGENTE_PRINC_PM");
				FG_SALDO_VIGENTE_PRINC_PF = rs.getDouble("FG_SALDO_VIGENTE_PRINC_PF");
				SUMA_10_PRINCIPALES = rs.getDouble("SUMA_10_PRINCIPALES");

			}
			rs.close();
			ps.close();
			exito_movimiento = true;
			String cadena_indicador = "";
			if (CAPITALIZACION >= 12) {
				this.insertaActualizaCedula(con, clave_solicitud, "1", "S", "CS_VIABLE");
			} else {
				this.insertaActualizaCedula(con, clave_solicitud, "1", "N", "CS_VIABLE");
				exito = false;
				exito_movimiento = false;
				cadena_indicador += "Capitalizaci�n,";
			}
			if (MOROSIDAD < 5 && MOROSIDAD >= 0) {
				this.insertaActualizaCedula(con, clave_solicitud, "2", "S", "CS_VIABLE");

			} else {
				this.insertaActualizaCedula(con, clave_solicitud, "2", "N", "CS_VIABLE");

				exito = false;
				exito_movimiento = false;
				cadena_indicador += "Morosidad,";
			}
			if (COBERTURA >= 100) {
				this.insertaActualizaCedula(con, clave_solicitud, "3", "S", "CS_VIABLE");

			} else {
				this.insertaActualizaCedula(con, clave_solicitud, "3", "N", "CS_VIABLE");

				exito = false;
				exito_movimiento = false;
				cadena_indicador += "Cobertura de reservas,";
			}
			if (ROE > 4) {
				this.insertaActualizaCedula(con, clave_solicitud, "4", "S", "CS_VIABLE");

			} else {
				this.insertaActualizaCedula(con, clave_solicitud, "4", "N", "CS_VIABLE");

				exito = false;
				exito_movimiento = false;
				cadena_indicador += "ROE,";
			}
			if (LIQUIDEZ >= 1) {
				this.insertaActualizaCedula(con, clave_solicitud, "5", "S", "CS_VIABLE");

			} else {
				this.insertaActualizaCedula(con, clave_solicitud, "5", "N", "CS_VIABLE");

				exito = false;
				exito_movimiento = false;
				cadena_indicador += "Liquidez,";
			}
			if (APALANCAMIENTO > 2.5 && APALANCAMIENTO < 0) {
				
			    this.insertaActualizaCedula(con, clave_solicitud, "6", "N", "CS_VIABLE");

			    exito = false;
			    exito_movimiento = false;
			    cadena_indicador += "Apalancamiento,";

			} else{
			    this.insertaActualizaCedula(con, clave_solicitud, "6", "S", "CS_VIABLE");
			}
			if (tipo_cartera.equals("P")) {
				if (FG_CARTERA < 9000000) {
				    this.insertaActualizaCedula(con, clave_solicitud, "7", "N", "CS_VIABLE");
				    exito = false;
				    exito_movimiento = false;
				    cadena_indicador += "Cartera PYME,";
				} else {
					
				    this.insertaActualizaCedula(con, clave_solicitud, "7", "S", "CS_VIABLE");

				}

				this.eliminaActualizaCedula(con, clave_solicitud, "8");

			}
			if (tipo_cartera.equals("M")) {
				if (FG_CARTERA < 5000000) {
				    this.insertaActualizaCedula(con, clave_solicitud, "8", "N", "CS_VIABLE");
				    exito = false;
				    exito_movimiento = false;
				    cadena_indicador += "Cartera PYME,";
				} else {
					
				    this.insertaActualizaCedula(con, clave_solicitud, "8", "S", "CS_VIABLE");

				}
				this.eliminaActualizaCedula(con, clave_solicitud, "7");
			}
			if (tipo_cartera.equals("")) {
				this.eliminaActualizaCedula(con, clave_solicitud, "8");
				this.eliminaActualizaCedula(con, clave_solicitud, "7");
			}
			if(CONC_ACRED_PM>0){

				if (FG_SALDO_VIGENTE_PRINC_PM > CONC_ACRED_PM) {
					this.insertaActualizaCedula(con, clave_solicitud, "9", "N", "CS_VIABLE");
					exito = false;
					exito_movimiento = false;
					cadena_indicador += "Concentraci�n de acreditado Persona Moral,";
	
				} else {
					this.insertaActualizaCedula(con, clave_solicitud, "9", "S", "CS_VIABLE");
				}
			}else{
			    this.insertaActualizaCedula(con, clave_solicitud, "9", "N", "CS_VIABLE");
			    exito = false;
			    exito_movimiento = false;
			    cadena_indicador += "Concentraci�n de acreditado Persona Moral,";
			}
		    if(CONC_ACRED_PF>0){
				if (FG_SALDO_VIGENTE_PRINC_PF > CONC_ACRED_PF) {
					this.insertaActualizaCedula(con, clave_solicitud, "10", "N", "CS_VIABLE");
					exito = false;
					exito_movimiento = false;
					cadena_indicador += "Concentraci�n de acreditado Persona F�sica,";
				} else {
					this.insertaActualizaCedula(con, clave_solicitud, "10", "S", "CS_VIABLE");
				}
			}else{
			    this.insertaActualizaCedula(con, clave_solicitud, "10", "N", "CS_VIABLE");
			    exito = false;
			    exito_movimiento = false;
			    cadena_indicador += "Concentraci�n de acreditado Persona F�sica,";
			}
			if(CONC_CART_VIG>0){
				if (SUMA_10_PRINCIPALES > CONC_CART_VIG) {
					this.insertaActualizaCedula(con, clave_solicitud, "11", "N", "CS_VIABLE");
					exito = false;
					exito_movimiento = false;
					cadena_indicador += "Concentraci�n de Cartera vigente ,";
	
				} else {
					this.insertaActualizaCedula(con, clave_solicitud, "11", "S", "CS_VIABLE");
				}
			}else{
			    this.insertaActualizaCedula(con, clave_solicitud, "11", "N", "CS_VIABLE");
			    exito = false;
			    exito_movimiento = false;
			    cadena_indicador += "Concentraci�n de Cartera vigente ,";
			}
			String cadena_final = cadena_indicador.substring(0, cadena_indicador.length());
		   
			
			//***********  SE INSERTAN A LA C�DULA LA VIABILIDAD DE LAS PESTA�AS CALIFICATE ******* 
				//******** cr�dito vinculado
			this.insertaActualizaCedula(con, clave_solicitud, "33", "S", "CS_VIABLE");
			
				//******** Informaci�n Financiera
			this.insertaActualizaCedula(con, clave_solicitud, "34", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "35", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "36", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "37", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "38", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "39", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "40", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "41", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "42", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "43", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "44", "S", "CS_VIABLE");
			
		    this.insertaActualizaCedula(con, clave_solicitud, "45", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "46", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "47", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "48", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "49", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "50", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "51", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "52", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "53", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "54", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "55", "S", "CS_VIABLE");
			
		    this.insertaActualizaCedula(con, clave_solicitud, "56", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "57", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "58", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "59", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "60", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "61", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "62", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "63", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "64", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "65", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "66", "S", "CS_VIABLE");
			//****** SALDOS
		    this.insertaActualizaCedula(con, clave_solicitud, "67", "S", "CS_VIABLE");
		    this.insertaActualizaCedula(con, clave_solicitud, "68", "S", "CS_VIABLE");
				
			
			if (ban_movi == true && exito_movimiento ==false ) {
				
			    boolean actualizaMov = this.setMovimientoSolicitus(con, clave_solicitud, usuario, "7", "9","Rechazo Calif�cate " + cadena_final);
			}

		} catch (Throwable t) {
			throw new AppException("insertaViabilidadCedula(Error): Error al guardar la informaci�n", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("insertaViabilidadCedula():: Error al cerrar Recursos: " + t.getMessage());
			}
		}
		return exito;
	}

	public List getInformacionAspirante(String IC_SOLICITUD, String estatus) {
		log.info("CediEJB::getInformacionAspirante(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		PreparedStatement ps1 = null;
		ResultSet rs1 = null;

		List varBind = new ArrayList();
		HashMap datos = new HashMap();

		List listInfAsp = new ArrayList();


		try {
			con.conexionDB();
			
			String cartera = "";
		    String subQuery = "  SELECT crp.cg_opcion_respuesta" +
		                       "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
		                       "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
		                       "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
		                       "  AND crp.ic_pregunta = ?";

		    varBind = new ArrayList();
		    varBind.add(IC_SOLICITUD);
		    varBind.add("4");
		    ps1 = con.queryPrecompilado(subQuery, varBind);
		    rs1 = ps1.executeQuery();
		    while (rs1.next()) {
				String valor = rs1.getString("CG_OPCION_RESPUESTA");
		        cartera += rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :rs1.getString("CG_OPCION_RESPUESTA")+',';
				
		    }
		    rs1.close();
		    ps1.close();
			String cadena_final = "";
			if(!cartera.equals("")){
				cadena_final = cartera.substring(0, (cartera.length()-1));
			}
			String  cad_estatus="and IC_ESTATUS_SOLICITUD = "+estatus;
			
			// SE OBTIENE LA INFORMACI�N DE ASPIRANTE
			String qryConsulta =
						 " SELECT CS.IC_SOLICITUD,CIA.CG_RAZON_SOCIAL, CIA.CG_RFC,  ce.CD_NOMBRE AS ESTADO, CIA.CG_TELEFONO, CIA.CG_EMAIL,TO_CHAR (CSM.DF_MOVIMIENTO, 'dd/mm/yyyy') AS DF_MOVIMIENTO,TO_CHAR (CSM.DF_MOVIMIENTO, 'HH24:mi:ss') AS HORA, CSM.IG_USUARIO, cs.IC_ESTATUS_SOLICITUD," +
						 "	 CIA.CG_NOMBRE_COMERCIAL,CIA.CG_CALLE,CIA.CG_COLONIA,CIA.IC_PAIS,CIA.IC_ESTADO,cm.CD_NOMBRE as IC_MUNICIPIO,CIA.CG_CP,CIA.CG_CONTACTO 	"+
						 " FROM CEDI_SOLICITUD CS, CEDI_SOLICITUD_MOVIMIENTO CSM, CEDI_IFNB_ASPIRANTE CIA, COMCAT_MUNICIPIO cm, COMCAT_ESTADO ce" +
						 " WHERE CS.IC_IFNB_ASPIRANTE = CIA.IC_IFNB_ASPIRANTE" + " AND CS.IC_SOLICITUD = CSM.IC_SOLICITUD" +
						 " AND CS.IC_SOLICITUD  = ?" + " AND cm.IC_ESTADO = CIA.IC_ESTADO  AND cm.IC_ESTADO = ce.IC_ESTADO" + " AND cm.IC_PAIS = CIA.IC_PAIS" +
						 " AND cm.IC_MUNICIPIO = CIA.IC_MUNICIPIO" +
						 "	and csm.IC_SOLICITUD_CAMBIO_EST = (select max(IC_SOLICITUD_CAMBIO_EST ) from cedi_solicitud_movimiento where IC_SOLICITUD = ? )";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				datos = new HashMap();
				datos.put("IC_SOLICITUD", rs.getString("IC_SOLICITUD") == null ? "" : rs.getString("IC_SOLICITUD"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("CG_RAZON_SOCIAL",rs.getString("CG_RAZON_SOCIAL") == null ? "" : rs.getString("CG_RAZON_SOCIAL"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("CG_RFC", rs.getString("CG_RFC") == null ? "" : rs.getString("CG_RFC"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("ESTADO", rs.getString("ESTADO") == null ? "" : rs.getString("ESTADO"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("CG_TELEFONO", rs.getString("CG_TELEFONO") == null ? "" : rs.getString("CG_TELEFONO"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("CG_EMAIL", rs.getString("CG_EMAIL") == null ? "" : rs.getString("CG_EMAIL"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("CS_TIPO_CARTERA",cadena_final);
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("DF_MOVIMIENTO", rs.getString("DF_MOVIMIENTO") == null ? "" : rs.getString("DF_MOVIMIENTO"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("HORA", rs.getString("HORA") == null ? "" : rs.getString("HORA"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("IG_USUARIO", rs.getString("IG_USUARIO") == null ? "" : rs.getString("IG_USUARIO"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
				datos.put("IC_ESTATUS_SOLICITUD",
				rs.getString("IC_ESTATUS_SOLICITUD") == null ? "" : rs.getString("IC_ESTATUS_SOLICITUD"));
			    listInfAsp.add(datos);
				
				// *** 10
				datos = new HashMap();
				datos.put("CG_NOMBRE_COMERCIAL",
				rs.getString("CG_NOMBRE_COMERCIAL") == null ? "" : rs.getString("CG_NOMBRE_COMERCIAL"));
				listInfAsp.add(datos);
			    datos = new HashMap();
			    datos.put("CG_CALLE",
			    rs.getString("CG_CALLE") == null ? "" : rs.getString("CG_CALLE"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
			    datos.put("CG_COLONIA",
			    rs.getString("CG_COLONIA") == null ? "" : rs.getString("CG_COLONIA"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
			    datos.put("IC_PAIS",
			    rs.getString("IC_PAIS") == null ? "" : rs.getString("IC_PAIS"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
			    datos.put("IC_ESTADO",
			    rs.getString("IC_ESTADO") == null ? "" : rs.getString("IC_ESTADO"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
			    datos.put("IC_MUNICIPIO",
			    rs.getString("IC_MUNICIPIO") == null ? "" : rs.getString("IC_MUNICIPIO"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
			    datos.put("CG_CP",
			    rs.getString("CG_CP") == null ? "" : rs.getString("CG_CP"));
			    listInfAsp.add(datos);
			    datos = new HashMap();
			    datos.put("CG_CONTACTO",
			    rs.getString("CG_CONTACTO") == null ? "" : rs.getString("CG_CONTACTO"));
			    listInfAsp.add(datos);
			}
			log.debug(" registros :: "+listInfAsp);
			rs.close();
			ps.close();


		} catch (Throwable t) {
			throw new AppException("getInformacionAspirante(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				
			    if (rs1 != null)
			        rs1.close();
			    if (ps1 != null)
			        ps1.close();
			} catch (Exception t) {
				log.error("getInformacionAspirante():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getInformacionAspirante(S)");
		}

		return listInfAsp;
	}

	public List getInformacionIndicadores(String IC_SOLICITUD, String pantalla) {
		log.info("CediEJB::getInformacionIndicadores(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
	    PreparedStatement ps2 = null;
	    ResultSet rs2 = null;


		List varBind = new ArrayList();
		HashMap datos = new HashMap();

		List listResuIndica = new ArrayList();
		String qryConsulta = "";

		try {
			con.conexionDB();
			// SE OBTIENE LA INFORMACI�N DE LOS ELEMENTOS DE VIABILIDAD
			qryConsulta =
						 " SELECT CEC.IC_ELEMENTO_CEDULA, CEC.CG_NOMBRE, DECODE(CC.CS_VIABLE,'S','Viable','N','No viable') AS CS_VIABLE" +
						 " FROM CEDI_SOLICITUD CS, CEDI_CEDULA CC, CEDICAT_ELEMENTO_CEDULA CEC" +
						 " WHERE  CC.IC_SOLICITUD = CS.IC_SOLICITUD " + " AND CC.IC_ELEMENTO_CEDULA = CEC.IC_ELEMENTO_CEDULA" +
						 " AND CC.IC_SOLICITUD = ?" + " AND CEC.IC_ELEMENTO_CEDULA IN(1,2,3,4,5,6,7,8,9,10,11)";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				
				String num_elemento = rs.getString("IC_ELEMENTO_CEDULA") == null ? "" : rs.getString("IC_ELEMENTO_CEDULA");
				//datos.put("IC_ELEMENTO_CEDULA", num_elemento);
				
				

				if (num_elemento.equals("1")) {
				    datos = new HashMap();
				    datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));
				    datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
					String subQuery = "	SELECT FG_CAPITALIZACION_ANIO3 AS CAPITALIZACION FROM CEDI_SOLICITUD" +
									   "	WHERE IC_SOLICITUD = ?";
					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
					ResultSet rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CAPITALIZACION") == null ? "" :
								  rs1.getString("CAPITALIZACION") + " %");
						datos.put("CS_REQUISITO", "El porcentaje de capitalizaci�n debe ser mayor a 12%");
					}
					rs1.close();
					ps1.close();
				    listResuIndica.add(datos);
				} else if (num_elemento.equals("2")) {
				    datos = new HashMap();
				    datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));
				    datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
					String subQuery = "  SELECT FG_MOROSIDAD_ANIO3 AS morosidad FROM CEDI_SOLICITUD" +
									   "  WHERE IC_SOLICITUD = ?";
					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
					ResultSet rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("morosidad") == null ? "" : rs1.getString("morosidad") + " %");
						datos.put("CS_REQUISITO", "El porcentaje de morosidad debe ser menor a 5%");
					}
					rs1.close();
					ps1.close();
				    listResuIndica.add(datos);

				} else if (num_elemento.equals("3")) {
					
				    datos = new HashMap();
				    datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));
				    datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
					String subQuery = "  SELECT FG_COBERTURA_RESERVAS_ANIO3 AS FG_COBERTURA_RESERVAS_ANIO3 FROM CEDI_SOLICITUD" +
									   "  WHERE IC_SOLICITUD = ?";
					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
					ResultSet rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("FG_COBERTURA_RESERVAS_ANIO3") == null ? "" :
								  rs1.getString("FG_COBERTURA_RESERVAS_ANIO3") + " %");
						datos.put("CS_REQUISITO", "El valor de la cobertura de reservas debe ser mayor a 1");
					}
					rs1.close();
					ps1.close();
				    listResuIndica.add(datos);
				}else if (num_elemento.equals("4")) {
				    if(!pantalla.equals("CALIFICATE")){
						datos = new HashMap();
						datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));
						datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
						String subQuery = "  SELECT FG_ROE_ANIO3 AS FG_ROE_ANIO3 FROM CEDI_SOLICITUD" +
										   "  WHERE IC_SOLICITUD = ?";
						varBind = new ArrayList();
						varBind.add(IC_SOLICITUD);
						PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
						ResultSet rs1 = ps1.executeQuery();
						if (rs1.next()) {
							datos.put("CS_RESULTADO",
									  rs1.getString("FG_ROE_ANIO3") == null ? "" :
									  rs1.getString("FG_ROE_ANIO3") + " %");
							datos.put("CS_REQUISITO", "Mayor a 4 % ");
						}
						rs1.close();
						ps1.close();
						listResuIndica.add(datos);
					}
				}else if (num_elemento.equals("5")) {
				    if(!pantalla.equals("CALIFICATE")){
						datos = new HashMap();
						datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));
						datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
						String subQuery = "  SELECT FG_LIQUIDEZ_ANIO3 AS FG_LIQUIDEZ_ANIO3 FROM CEDI_SOLICITUD" +
										   "  WHERE IC_SOLICITUD = ?";
						varBind = new ArrayList();
						varBind.add(IC_SOLICITUD);
						PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
						ResultSet rs1 = ps1.executeQuery();
						if (rs1.next()) {
							datos.put("CS_RESULTADO",
									  rs1.getString("FG_LIQUIDEZ_ANIO3") == null ? "" :
									  rs1.getString("FG_LIQUIDEZ_ANIO3"));
							datos.put("CS_REQUISITO", "Al menos 1 vez");
						}
						rs1.close();
						ps1.close();
						listResuIndica.add(datos);
					}
				} else if (num_elemento.equals("6")) {
				    if(!pantalla.equals("CALIFICATE")){
						datos = new HashMap();
						datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));
						datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
						String subQuery = "  SELECT FG_APALANCAMIENTO_ANIO3 AS FG_APALANCAMIENTO_ANIO3 FROM CEDI_SOLICITUD" +
										   "  WHERE IC_SOLICITUD = ?";
						varBind = new ArrayList();
						varBind.add(IC_SOLICITUD);
						PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
						ResultSet rs1 = ps1.executeQuery();
						if (rs1.next()) {
							datos.put("CS_RESULTADO",
									  rs1.getString("FG_APALANCAMIENTO_ANIO3") == null ? "" :
									  rs1.getString("FG_APALANCAMIENTO_ANIO3"));
							datos.put("CS_REQUISITO", "Menor o igual a 2.5 veces");
						}
						rs1.close();
						ps1.close();
						listResuIndica.add(datos);
					}
				} else if (num_elemento.equals("7") || num_elemento.equals("8")) {
					List respP4 = new ArrayList();
				    String subQueryP4 = "	select IC_RESPUESTA_PARAMETRIZACION  from cedi_respuesta_parametrizacion where IC_PREGUNTA = 4 order by IC_RESPUESTA_PARAMETRIZACION";
					ps2 = con.queryPrecompilado(subQueryP4);
				    rs2 = ps2.executeQuery();
				    while (rs2.next()) {
					    respP4.add((String)rs2.getString("IC_RESPUESTA_PARAMETRIZACION"));
					}
				    rs2.close();
				    ps2.close();
					String subQuery = 	"  SELECT cri.IC_RESPUESTA_PARAMETRIZACION,crp.cg_opcion_respuesta,round((cif.FG_CAPITAL_CONTABLE/ (/* Formatted on 2015/10/03 16:28 (Formatter Plus v4.8.8) */ SELECT fn_valor_compra FROM com_tipo_cambio WHERE ic_moneda = 40 AND ROWID = (SELECT MAX (ROWID) FROM com_tipo_cambio  WHERE ic_moneda = 40))),2) AS CARTERA_PYME" +
										"  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri, cedi_informacion_financiera cif, cedi_solicitud cs"+
										"	WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion	"+
										"   AND cri.ic_solicitud =  cif.ic_solicitud "+
										"   AND cri.ic_solicitud =  cs.ic_solicitud "+
										"   and cif.IC_ANIO = cs.IG_ANIO_INF_FINANCIERA"+
										"   AND cri.ic_solicitud = ?"+
										"   AND crp.ic_pregunta = 4 ";
									  
						
					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
				    log.debug("qryConsulta w::: " + subQuery);
				    log.debug(" varBind w:: " + varBind);
					PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
					ResultSet rs1 = ps1.executeQuery();
					int posi = 0;
					while (rs1.next()) {
					    datos = new HashMap();
						String tipo_cartera = rs1.getString("IC_RESPUESTA_PARAMETRIZACION") == null ? "" :
											  rs1.getString("IC_RESPUESTA_PARAMETRIZACION");
					    double FG_CARTERA = rs1.getDouble("CARTERA_PYME");
					    log.debug(" tipo_cartera ::    eee " + tipo_cartera);
						datos.put("CS_RESULTADO",rs1.getString("CARTERA_PYME") == null ? "" : rs1.getString("CARTERA_PYME"));
						for(int i = 0; i < respP4.size(); i++){
							if (tipo_cartera.equals(respP4.get(i))) {
								
							    if (i==0) {
									datos.put("CG_NOMBRE", "Cartera PyME");
							        if (FG_CARTERA >= 9000000 ) {
							            datos.put("CS_VIABLE","Viable");
							        }else{
							                                     
							             datos.put("CS_VIABLE","No viable");
							        }
										  datos.put("CS_REQUISITO",
							                        " El valor del Capital Contable para Cartera PyME debe ser igual o mayor a 9 millones de UDIS(1 UDI = 5.270368) ");
				                  } else if (i==1) {
				                         datos.put("CG_NOMBRE", "Microcr�dito");
				                          if (FG_CARTERA >= 5000000) {
						                                        
				                           datos.put("CS_VIABLE","Viable");
					                      }else{
				                            datos.put("CS_VIABLE","No viable");
										   }
											datos.put("CS_REQUISITO",
							                                              "El valor del Capital Contable para Cartera Microcr�dito debe ser igual o mayor a 5 millones de UDIS(1 UDI = 5.270368) ");
					                }else if (i==2) {
										datos.put("CG_NOMBRE", "OTRO");
							            datos.put("CS_VIABLE","- " );
							                                    
							            datos.put("CS_REQUISITO",
							                          "-   ");
									}
							} 
						}
					    listResuIndica.add(datos);
					}
				    
				    
					rs1.close();
					ps1.close();
					
				} else if (num_elemento.equals("9")) {
				    datos = new HashMap();
				    datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));
				    datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
					String subQuery = "  SELECT FG_CONC_ACRED_PM AS FG_CONC_ACRED_PM FROM CEDI_SOLICITUD" +
									   "  WHERE IC_SOLICITUD = ?";
					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
					ResultSet rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("FG_CONC_ACRED_PM") == null ? "" : rs1.getString("FG_CONC_ACRED_PM"));
						datos.put("CS_REQUISITO",
								  "La concentraci�n m�xima de acreditado para persona moral no debe ser mayor al 30% del Capital Contable (considera la �ltima fecha de informaci�n financiera registrada). ");
					}
					rs1.close();
					ps1.close();
				    listResuIndica.add(datos);
				} else if (num_elemento.equals("10")) {
				    datos = new HashMap();
				    datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));
				    datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
					String subQuery = "  SELECT FG_CONC_ACRED_PF AS FG_CONC_ACRED_PF FROM CEDI_SOLICITUD" +
									   "  WHERE IC_SOLICITUD = ?";
					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
					ResultSet rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("FG_CONC_ACRED_PF") == null ? "" : rs1.getString("FG_CONC_ACRED_PF"));
						datos.put("CS_REQUISITO",
								  "La concentraci�n m�xima de acreditado para persona f�sica no debe ser mayor al 10% del Capital Contable (considera la �ltima fecha de informaci�n financiera registrada).");
					}
					rs1.close();
					ps1.close();
				    listResuIndica.add(datos);
				} else if (num_elemento.equals("11")) {
				    datos = new HashMap();
				    datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));
				    datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
					String subQuery = "  SELECT FG_CONC_CART_VIG AS FG_CONC_CART_VIG FROM CEDI_SOLICITUD" +
									   "  WHERE IC_SOLICITUD = ?";
					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					PreparedStatement ps1 = con.queryPrecompilado(subQuery, varBind);
					ResultSet rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("FG_CONC_CART_VIG") == null ? "" : rs1.getString("FG_CONC_CART_VIG"));
						datos.put("CS_REQUISITO",
								  "Se recomienda que la suma del saldo vigente de los 10 principales acreditados del Intermediario sea menor al 35% de la cartera. ");
					}
					rs1.close();
					ps1.close();
				    listResuIndica.add(datos);
				}
				

			}
			log.debug(" registros :: " + listResuIndica);
			rs.close();
			ps.close();


		} catch (Throwable t) {
			throw new AppException("getInformacionIndicadores(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
				
			    if (rs2 != null)
			        rs2.close();
			    if (ps2 != null)
			        ps2.close();
			} catch (Exception t) {
				log.error("getInformacionIndicadores():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getInformacionIndicadores(S)");
		}

		return listResuIndica;
	}

	public List getInformacionInfoCualitativa(String IC_SOLICITUD) {
		log.info("CediEJB::getInformacionInfoCualitativa(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		PreparedStatement ps1 = null;
		ResultSet rs1 = null;

		List varBind = new ArrayList();
		HashMap datos = new HashMap();

		List listResuInfFinan = new ArrayList();
		String qryConsulta = "";

		try {
			con.conexionDB();
			// SE OBTIENE LA INFORMACI�N DE LA INFORMACI�N CUALITATIVA

			qryConsulta =
						 " SELECT CEC.IC_ELEMENTO_CEDULA,CS.IC_ESTATUS_SOLICITUD, CEC.CG_NOMBRE, DECODE(CC.CS_VIABLE,'S','Viable','N','No viable') AS CS_VIABLE" +
						 " FROM CEDI_SOLICITUD CS, CEDI_CEDULA CC, CEDICAT_ELEMENTO_CEDULA CEC" +
						 " WHERE  CC.IC_SOLICITUD = CS.IC_SOLICITUD " + " AND CC.IC_ELEMENTO_CEDULA = CEC.IC_ELEMENTO_CEDULA" +
						 " AND CC.IC_SOLICITUD = ?" +
						 " AND CEC.IC_ELEMENTO_CEDULA IN(18,19,20,21,22,23,24,27,28,29,30,31)";
			varBind = new ArrayList();
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				datos = new HashMap();
				String num_elemento =
								rs.getString("IC_ELEMENTO_CEDULA") == null ? "" : rs.getString("IC_ELEMENTO_CEDULA");
				datos.put("IC_ELEMENTO_CEDULA",
						  rs.getString("IC_ELEMENTO_CEDULA") == null ? "" : rs.getString("IC_ELEMENTO_CEDULA"));
				datos.put("CG_NOMBRE", rs.getString("CG_NOMBRE") == null ? "" : rs.getString("CG_NOMBRE"));
				datos.put("CS_VIABLE", rs.getString("CS_VIABLE") == null ? "" : rs.getString("CS_VIABLE"));


				if (num_elemento.equals("18")) {

					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("7");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						String opcRes = rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :rs1.getString("CG_OPCION_RESPUESTA");
						if(opcRes.equals("SI")){
						    datos.put("CS_RESULTADO",
						              rs1.getString("CG_RESPUESTA_LIBRE") == null ? "" :
						              rs1.getString("CG_RESPUESTA_LIBRE"));
						}else if(opcRes.equals("NO")){
						    datos.put("CS_RESULTADO",
						              rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
						              rs1.getString("CG_OPCION_RESPUESTA"));
						}
						
						datos.put("CS_REQUISITO", "Debe ser igual o mayor a 2 a�os");
					}
					rs1.close();
					ps1.close();
				} else if (num_elemento.equals("19")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("8");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();

				} else if (num_elemento.equals("20")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("9");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();
				} else if (num_elemento.equals("21")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("10");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();
				} else if (num_elemento.equals("22")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("11");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();
				} else if (num_elemento.equals("23")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("12");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();

				} else if (num_elemento.equals("24")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("13");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();
				} else if (num_elemento.equals("27")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("16");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();
				} else if (num_elemento.equals("28")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("17");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();
				} else if (num_elemento.equals("29")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("18");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();
				} else if (num_elemento.equals("30")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("19");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();
				} else if (num_elemento.equals("31")) {
					String subQuery = "  SELECT crp.cg_opcion_respuesta, cri.cg_respuesta_libre" +
									   "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
									   "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
									   "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
									   "  AND crp.ic_pregunta = ?";

					varBind = new ArrayList();
					varBind.add(IC_SOLICITUD);
					varBind.add("20");
					ps1 = con.queryPrecompilado(subQuery, varBind);
					rs1 = ps1.executeQuery();
					if (rs1.next()) {
						datos.put("CS_RESULTADO",
								  rs1.getString("CG_OPCION_RESPUESTA") == null ? "" :
								  rs1.getString("CG_OPCION_RESPUESTA"));
						datos.put("CS_REQUISITO", "SI");
					}
					rs1.close();
					ps1.close();
				}


				listResuInfFinan.add(datos);
			}
			//log.debug(" registros :: "+registros);
			rs.close();
			ps.close();


		} catch (Throwable t) {
			throw new AppException("getInformacionInfoCualitativa(Error): Error al consultar si ya existe alg�n usuario con perfil IF CEDI",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getInformacionInfoCualitativa():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CediEJB::getInformacionInfoCualitativa(S)");
		}

		return listResuInfFinan;
	}

	/**
	 *Funci�n que realiza el envio de correo dirigido a todos los usuarios del GESTOR CEDI.
	 * @param registros lista de usuarios con perfil Gestor Cedi
	 * @param ic_solicitud clave de la solicitud
	 */
	public void enviaCorreoNotiCalificate(List registros, String ic_solicitud, String estatus_f) {
		log.info("enviaCorreoNotiCalificate(E)");
		Correo correo = new Correo();
	    correo.setCodificacion("charset=UTF-8");
	    StringBuffer tabla = new StringBuffer();
		String ccCorreo = "";
		StringBuffer asunto = new StringBuffer();
		StringBuffer textoA = new StringBuffer();
		StringBuffer textoB = new StringBuffer();
		String aQuien = "";
		String textoCorreo = "";
		List listInfoAspirante = new ArrayList();
		HashMap hmAspirante = new HashMap();
	    HashMap hmAspirante1 = new HashMap();
	    HashMap hmAspirante2 = new HashMap();
	    HashMap hmAspirante3 = new HashMap();
		try {

			listInfoAspirante = this.getInformacionAspirante(ic_solicitud,estatus_f);
			hmAspirante = (HashMap) listInfoAspirante.get(10);
			String estatus = (String) hmAspirante.get("IC_ESTATUS_SOLICITUD");
			
		    String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
		        "font-size: 10px; "+
		        "font-weight: bold;"+
		        "color: #FFFFFF;"+
		        "background-color: #4d6188;"+
		        "padding-top: 3px;"+
		        "padding-right: 1px;"+
		        "padding-bottom: 1px;"+
		        "padding-left: 3px;"+
		        "height: 25px;"+
		        "border: 1px solid #1a3c54;'";

		    String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
		        "color:#000066; "+
		        "padding-top:1px; "+
		        "padding-right:2px; "+
		        "padding-bottom:1px; "+
		        "padding-left:2px; "+
		        "height:22px; "+
		        "font-size:11px;'";
			
			
			//tabla.append("<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:10;'><br>");
			tabla.append("<table border=\"1\">");
			tabla.append(" <tr>" + 
						 "    <td align=\"center\" " + styleEncabezados + ">Raz&oacute;n Social</td>"+ 
						 "    <td align=\"center\" " + styleEncabezados + ">R.F.C</td>" + 
						 "    <td align=\"center\" " + styleEncabezados +">Folio Solicitud</td>" +
						 "    <td align=\"center\" " + styleEncabezados + ">Fecha de Registro</td>"+
						 " </tr>");
		    hmAspirante1 = (HashMap) listInfoAspirante.get(1);
		    hmAspirante2 = (HashMap) listInfoAspirante.get(2);
		    hmAspirante = (HashMap) listInfoAspirante.get(0);
		    hmAspirante3 = (HashMap) listInfoAspirante.get(7);
			String razon = (String) hmAspirante1.get("CG_RAZON_SOCIAL");
		   /* byte ptextRazon[] = razon.getBytes("ISO-8859-1");
		    String razonF = new String(ptextRazon, "UTF-8");*/
			tabla.append("   <tr> " +
							 " <td align=\"left\" " + style + "> " + razon + "</td>" +
							 " <td align=\"left\" " + style + "> " + (String) hmAspirante2.get("CG_RFC") + "</td>" +
							 " <td align=\"center\" " +style + "> " + (String) hmAspirante.get("IC_SOLICITUD") + "</td>" +
							 " <td align=\"center\" " + style + "> " +(String) hmAspirante3.get("DF_MOVIMIENTO") + "</td>" +
						 " </tr> ");
			tabla.append("</table>");
			//tabla.append("</form>");
			if (estatus.equals("7")) {
				asunto.append("Solicitud Rechazada-Calif\u00EDcate");
				textoA.append("<p>Estimado Gestor CEDI <br><br>Por este conducto, se le informa que la siguiente solicitud no ha sido elegible para su viabilidad de incorporaci&oacute;n a la red de intermediarios Financieros de Nacional Financiera:</p><br>");
			} else if (estatus.equals("2")) {
				asunto.append("Atenci\u00F3n de Solicitudes");
				textoA.append("<p>Estimado Gestor CEDI <br><br>Por este conducto, se le informa que la siguiente solicitud ha sido aceptada para validar su viabilidad de incorporaci&oacute;n a Nacional Financiera, favor de realizar el an&aacute;lisis correspondiente:</p><br>");
			}
			textoB.append("<br>Sin otro particular, reciba un cordial saludo." + "<br><br>ATENTAMENTE<br>" +
						 "Centro de Desarrollo de Intermediarios");
			
		   /* String totalCorreo = textoA.toString() +tabla + textoB.toString();
			byte ptextCorreo[] = totalCorreo.getBytes();
		    String correoF = new String(ptextCorreo, "UTF-8");*/
		                
		   
			
			for (int i = 0; i < registros.size(); i++) {
				HashMap solicitud = (HashMap) registros.get(i);
				String perfil = solicitud.get("PERFIL").toString();
				String login = solicitud.get("LOGIN").toString();
				String nombre = solicitud.get("NOMBRE").toString();
				String apellidoPaterno = solicitud.get("APELLIDO_PATERMO").toString();
				String apellidoMaterno = solicitud.get("APELLIDO_MATERNO").toString();
				aQuien = solicitud.get("CORREO").toString();
				textoCorreo = "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>" +textoA.toString() +tabla + textoB.toString() + "</form>";
				correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", aQuien, ccCorreo,  asunto.toString(), textoCorreo.toString(),
												   null, null);

			}
		} catch (Throwable t) {
			throw new AppException("enviaCorreoNotiCalificate() Error al enviar correo ", t);
		}
	}

	public boolean guardarArchEdoFinan(String ic_solicitud, String rutaArchivo, String tipoArchivo) {
		log.info("guardarArchEdoFinan (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "", campo = "", tabla = "cedi_solicitud";

		if (tipoArchivo.equals("Balance")) {
			campo = "bi_balance";
		}
		if (tipoArchivo.equals("Edo_Resultado")) {
			campo = "bi_edo_resultados";
		}
		if (tipoArchivo.equals("RFC")) {
			campo = "bi_rfc";
		}
	    if (tipoArchivo.equals("CEDULA_PREVIA")) {
	        campo = "BI_CEDULA_PREVIA";
	    }
	    if (tipoArchivo.equals("CEDULA_COMPLETA")) {
	        campo = "BI_CEDULA_COMPLETA";
	    }


		try {
			con.conexionDB();

			File archivo = new File(rutaArchivo);
			FileInputStream fileinputstream = new FileInputStream(archivo);

			strSQL = " Select count(*) AS numRegistros  from " + tabla + "  WHERE IC_SOLICITUD = ? ";
			log.debug("strSQL " + strSQL);
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(ic_solicitud));
			rs = ps.executeQuery();
			rs.next();
			boolean existeRegistro = (rs.getInt("numRegistros") == 0) ? false : true;
			rs.close();
			ps.close();

			log.debug("existeRegistro " + existeRegistro);


			log.debug(" rutaArchivo ---------------->" + rutaArchivo);
			log.debug(" archivo ---------------->" + archivo);
			log.debug("(int)archivo.length() ---------------->" + (int) archivo.length());
			log.debug(" ic_solicitud= " + ic_solicitud);
			log.debug(" campo = " + campo);


			strSQL = " UPDATE " + tabla + " SET " + campo + "  = ? " + " WHERE IC_SOLICITUD = ? ";
			log.debug("strSQL " + strSQL);
			ps = con.queryPrecompilado(strSQL);
			ps.setBinaryStream(1, fileinputstream, (int) archivo.length());
			ps.setString(2, ic_solicitud);
			ps.executeUpdate();
			ps.close();

		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("guardarArchEdoFinan  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  guardarArchEdoFinan (S) ");
			}
		}
		return exito;
	}

	public String descargaArchivosEdoFinan(String no_solicitud, String strDirectorioTemp, String tipoArchivo) {
		log.info("descargaArchivosSolic(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		FileOutputStream fileOutputStream = null;
		String pathname = "", nombreArchivo = "", campo = "";

		try {
			con.conexionDB();

			if (tipoArchivo.equals("balance")) {
				campo = "BI_BALANCE";
			}
			if (tipoArchivo.equals("resultados")) {
				campo = "BI_EDO_RESULTADOS";
			}
			if (tipoArchivo.equals("rfc")) {
				campo = "BI_RFC";
			}
		    if (tipoArchivo.equals("CEDULA_PREVIA")) {
		        campo = "BI_CEDULA_PREVIA";
		    }
		    if (tipoArchivo.equals("CEDULA_COMPLETA")) {
		        campo = "BI_CEDULA_COMPLETA";
		    }

			strSQL.append(" SELECT " + campo);
			strSQL.append(" FROM cedi_solicitud ");
			strSQL.append(" WHERE IC_SOLICITUD = ? ");
			varBind.add(no_solicitud);

			log.debug(strSQL.toString() + "   " + varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			if (rst.next()) {
				nombreArchivo = Comunes.cadenaAleatoria(8) + ".pdf";
				pathname = strDirectorioTemp + nombreArchivo;
				file = new File(pathname);
				fileOutputStream = new FileOutputStream(file);
				Blob blob = rst.getBlob(campo);
				InputStream inStream = blob.getBinaryStream();
				int size = (int) blob.length();
				byte[] buffer = new byte[size];
				int length = -1;

				while ((length = inStream.read(buffer)) != -1) {
					fileOutputStream.write(buffer, 0, length);
				}
				fileOutputStream.flush();
				fileOutputStream.close();
			}
			rst.close();
			pst.close();


		} catch (Exception e) {
			throw new AppException("Error al obtener el archivo de la base de datos: ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("descargaArchivosSolic(S)");
		}
		//return pathname;
		return nombreArchivo;
	}

	/**
	 * Funci�n que se usa para generar una nueva solicitud, cuando el aspirante ya realizo un proceso y fue rechazada, el aspirante debe existir
	 * @param rfc es el rfc del aspirante
	 * @return
	 */
	public String generaNuevoFolioAspirante(String rfc) {
		log.info("generaNuevoFolioAspirante(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//PreparedStatement ps1 = null;
		List varBind = new ArrayList();
		String qry = "";
		String qryId = "";
		String claveAsp = "";
		String claveSolic = "";
		String querySEQ = "";

		String IC_SOLICITUD = "";
		String IC_ESTATUS_SOLICITUD = "1";

		try {
			con.conexionDB();
			qryId = " SELECT ic_ifnb_aspirante  " + " FROM cedi_ifnb_aspirante" + " WHERE cg_rfc = TRIM(?)";
			varBind = new ArrayList();
			varBind.add(rfc);
			log.debug(" qryIdExist ::: " + qryId);
			log.debug(" varBindExist ::: " + varBind);
			ps = con.queryPrecompilado(qryId, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				claveAsp = rs.getString("IC_IFNB_ASPIRANTE");
			}
			rs.close();
			ps.close();

			querySEQ = "";
			querySEQ = "select SEQ_CEDI_SOLICITUD.NEXTVAL from dual";
			ps = con.queryPrecompilado(querySEQ);
			rs = ps.executeQuery();
			if (rs.next()) {
				claveSolic = rs.getString(1);
			}
			rs.close();
			ps.close();

			qry = "";
			qry = " INSERT INTO cedi_solicitud (IC_SOLICITUD,IC_IFNB_ASPIRANTE,IC_ESTATUS_SOLICITUD,DF_ALTA)" +
						 " VALUES(?,?,?,sysdate)";
			varBind = new ArrayList();
			varBind.add(claveSolic);
			varBind.add(claveAsp);
			varBind.add("1");
			log.debug("qrySolicitud11 :: " + qry);
			log.debug("varBind11 :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();


			qry = "";
			qry = " INSERT INTO cedi_solicitud_movimiento (IC_SOLICITUD_CAMBIO_EST,IC_SOLICITUD,DF_MOVIMIENTO,IC_ESTATUS_SOLICITUD,IC_MOVIMIENTO)" +
						 " VALUES((SELECT MAX(IC_SOLICITUD_CAMBIO_EST)+1 FROM cedi_solicitud_movimiento),?,sysdate,?,?)";
			varBind = new ArrayList();
			varBind.add(claveSolic);
			varBind.add(IC_ESTATUS_SOLICITUD);
			varBind.add("2");
			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
		} catch (Throwable t) {
			exito = false;
			throw new AppException("getPreguntasIFNB(Error): Error al consultar informaci�n de las preguntas", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getPreguntasIFNB():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		return claveSolic;

	}

	/**
	 * Funci�n que nos indica si la solicitud es viable o no.
	 * @param con
	 * @param clave_solicitud
	 * @param tipo_cartera
	 * @return
	 */
	private boolean validaViabilidadCedula(AccesoDB con, String clave_solicitud, String tipo_cartera) {
		log.info("validaViabilidadCedula(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
	    PreparedStatement ps2 = null;
	    ResultSet rs2 = null;
		List varBind = new ArrayList();
		String qry = "";

		String ic_pregunta = "";
		String ic_resp = "";
		String opc_resp = "";
		String resp_libre = "";
		boolean exito = true;

		double CAPITALIZACION = 0;
		double MOROSIDAD = 0;
		double COBERTURA = 0;
		double ROE = 0;
		double LIQUIDEZ = 0;
		double APALANCAMIENTO = 0;
		double FG_CARTERA = 0;
		double CONC_ACRED_PM = 0;
		double CONC_ACRED_PF = 0;
		double CONC_CART_VIG = 0;
		double FG_SALDO_VIGENTE_PRINC_PM = 0;
		double FG_SALDO_VIGENTE_PRINC_PF = 0;
		double SUMA_10_PRINCIPALES = 0;

		try {
			boolean existe_resp_info_cual= true;
			
			List causa_rechazo = new ArrayList();
		    causa_rechazo.add(4);
		    causa_rechazo.add(7);
		    causa_rechazo.add(8);
		    causa_rechazo.add(9);
		    causa_rechazo.add(10);
		    causa_rechazo.add(11);
		    causa_rechazo.add(12);
		    causa_rechazo.add(13);
		    causa_rechazo.add(16);
		    causa_rechazo.add(17);
		    causa_rechazo.add(18);
		    causa_rechazo.add(19);
		    causa_rechazo.add(20);
		    log.debug("ddddddddddddd " + causa_rechazo);
			for(int i = 0; i<causa_rechazo.size();i++){
				// Inserta la viabilidad de la informaci�n Cualitativa
				qry = "";
				qry = "  SELECT  count(*) total_opc_resp " +
							 "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
							 "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
							 "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ? " +
							 "  AND crp.ic_pregunta = ? ";
				varBind = new ArrayList();
			    varBind.add(clave_solicitud);
				varBind.add(causa_rechazo.get(i));
	
				log.debug("qry :: www " + qry);
				log.debug("varBind ::www " + varBind);
				ps = con.queryPrecompilado(qry, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
				    existe_resp_info_cual= true;
				}else{
				    existe_resp_info_cual= false;
				}
			}
		    log.debug("qry :: existe_resp_info_cual " + existe_resp_info_cual);
			// Inserta la viabilidad de la informaci�n Cualitativa
			if(existe_resp_info_cual==true){
				// Se obtiene los opciones de respuesta para la pregunta 4
			    List opc_respP4 = new ArrayList(); // Guarda las opciones de respuesta para la pregunta 4
				String subQueryP4 = "   select IC_RESPUESTA_PARAMETRIZACION  from cedi_respuesta_parametrizacion where IC_PREGUNTA = 4 order by IC_RESPUESTA_PARAMETRIZACION";
			    ps2 = con.queryPrecompilado(subQueryP4);
			    rs2 = ps2.executeQuery();
			    while (rs2.next()) {
			       opc_respP4.add((String)rs2.getString("IC_RESPUESTA_PARAMETRIZACION"));
			    }
			    rs2.close();
			    rs2.close();
				
			    List respP4 = new ArrayList();
			    HashMap datos = new HashMap();
			    qry = "";
			    qry = "  SELECT    cri.ic_respuesta_parametrizacion,cri.cg_respuesta_libre " +
			                 "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
			                 "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
			                 "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
			                 "  AND crp.ic_pregunta = 4 ";
			    varBind = new ArrayList();
			    varBind.add(clave_solicitud);
			    
			    log.debug("qry :: www " + qry);
			    log.debug("varBind ::www " + varBind);
			    ps2 = con.queryPrecompilado(qry, varBind);
			    rs2 = ps2.executeQuery();
			    
				
			    while (rs2.next()) {
					datos = new HashMap();
			        datos.put("clave_resp", rs2.getString("IC_RESPUESTA_PARAMETRIZACION") == null ? "" : rs2.getString("IC_RESPUESTA_PARAMETRIZACION"));
					datos.put("respuesta", rs2.getString("CG_RESPUESTA_LIBRE") == null ? "" : rs2.getString("CG_RESPUESTA_LIBRE"));
			        respP4.add(datos);
			    }
			    rs2.close();
			    rs2.close();
				if(respP4.size()>0){
				    
						if(respP4.size()==1){
							HashMap respuesta = (HashMap) respP4.get(0);
							double clave_resp = Double.parseDouble((String) respuesta.get("clave_resp"));
							double porc_resp = Double.parseDouble((String) respuesta.get("respuesta"));
							for(int i=0;i <opc_respP4.size();i++){
								double clave_opc = Double.parseDouble((String) opc_respP4.get(i));
								if(clave_opc==clave_resp){
									if(i==2){
										exito = false;
									}
								}
							}
						}else if(respP4.size()>1){
							double var1 = 0;
							double var2 = 0;
						    double var3 = 0;
						    double var1_por = 0;
						    double var2_por= 0;
						    double var3_por= 0;
							
							int pos_aux = -1;
							int tam_resp = respP4.size();
						    for(int x=0;x <respP4.size();x++){
								HashMap respuesta = (HashMap) respP4.get(x);
								double clave_resp = Double.parseDouble((String) respuesta.get("clave_resp"));
								double porc_resp = Double.parseDouble((String) respuesta.get("respuesta"));
								if(x==0){
								    var1 = clave_resp;
								    var1_por = porc_resp;
								}else if(x==1){
								    var2 = clave_resp;
								    var2_por = porc_resp;
								}else if(x==2){
								    var3 = clave_resp;
								    var3_por = porc_resp;
								}
								for(int i=0;i <opc_respP4.size();i++){
									double clave_opc = Double.parseDouble((String) opc_respP4.get(i));
									if(clave_opc==clave_resp){
										if(i==2){
											pos_aux = x;
										}
									}
								}
							}
							if(pos_aux!=-1){
							    HashMap respuesta = (HashMap) respP4.get(pos_aux);
							    double clave_resp = Double.parseDouble((String) respuesta.get("clave_resp"));
							    double porc_resp = Double.parseDouble((String) respuesta.get("respuesta"));
							    for(int x=0;x <respP4.size();x++){
								    HashMap respuesta_r = (HashMap) respP4.get(x);
								    double clave = Double.parseDouble((String) respuesta_r.get("clave_resp"));
								    double porc = Double.parseDouble((String) respuesta_r.get("respuesta"));
									if(porc_resp>porc){
									    exito = false;
									}
								}
								
								
							}
						}
				}
				log.debug("validsacion pregunta 4 exito "+exito);
				qry = "";
				qry = "  SELECT   crp.ic_pregunta, cri.ic_respuesta_parametrizacion,crp.cg_opcion_respuesta, cri.cg_respuesta_libre " +
							 "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
							 "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
							 "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
							 "  AND crp.ic_pregunta IN (4,7, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19, 20) ";
				varBind = new ArrayList();
				varBind.add(clave_solicitud);
	
				log.debug("qry :: " + qry);
				log.debug("varBind :: " + varBind);
				ps = con.queryPrecompilado(qry, varBind);
				rs = ps.executeQuery();
				while (rs.next()) {
					ic_pregunta = rs.getString("ic_pregunta") == null ? "" : rs.getString("ic_pregunta");
					ic_resp = rs.getString("ic_respuesta_parametrizacion") == null ? "" :
							  rs.getString("ic_respuesta_parametrizacion");
					opc_resp = rs.getString("cg_opcion_respuesta") == null ? "" : rs.getString("cg_opcion_respuesta");
					resp_libre = rs.getString("cg_respuesta_libre") == null ? "" : rs.getString("cg_respuesta_libre");
				    if (ic_pregunta.equals("7")) {
						if (!resp_libre.equals("")) {
							int anios = Integer.parseInt(resp_libre.replace(" ", ""));
							if (anios < 2) {
								exito = false;
							}
						} else {
							exito = false;
						}
					}else if (ic_pregunta.equals("8")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					}else if (ic_pregunta.equals("9")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("10")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("11")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("12")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("13")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("16")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("17")) {
						if (!opc_resp.equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("18")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("19")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("20")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					}
				}
				rs.close();
				ps.close();
				
			// Inserta el resultado de viabilidad de los indicadores
			}else{
			    exito = false;
			}
			qry = 
		    "   SELECT IC_ANIO,ic_solicitud, round((((FG_CAPITAL_CONTABLE +(FG_ESTIMACION_RESERVAS-FG_CARTERA_VENCIDA)) - (0.05*FG_CARTERA_VIGENTE))/FG_CARTERA_TOTAL)*100,2) AS CAPITALIZACION, " +
		    "  round((decode(FG_CARTERA_VENCIDA,0,1,FG_CARTERA_VENCIDA)/FG_CARTERA_TOTAL)*100 ,2) AS MOROSIDAD," +
		    "  round((FG_ESTIMACION_RESERVAS/decode(FG_CARTERA_VENCIDA,0,1,FG_CARTERA_VENCIDA))*100,2) AS COVERTURA_RESERVA," +
		    " round((FG_UTILIDAD_NETA/FG_CAPITAL_CONTABLE)*100,2) AS ROE," +
		    "  round((FG_INVERSION_CAJA/FG_PASIVOS_TOTALES)*100,2) AS LIQUIDEZ," +
		    "  round((FG_PASIVOS_TOTALES/FG_CAPITAL_CONTABLE)*100,2) AS APALANCAMIENTO" +
		    "  FROM cedi_informacion_financiera" + "  WHERE IC_SOLICITUD = ?" +
		    "  ORDER BY IC_ANIO DESC";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				CAPITALIZACION = rs.getDouble("CAPITALIZACION");
				MOROSIDAD = rs.getDouble("MOROSIDAD");
				COBERTURA = rs.getDouble("COVERTURA_RESERVA");
				ROE = rs.getDouble("ROE");
				LIQUIDEZ = rs.getDouble("LIQUIDEZ");
				APALANCAMIENTO = rs.getDouble("APALANCAMIENTO");
				
				
			}
			rs.close();
			ps.close();
			
		    qry = " select  cif.IC_ANIO,    " +
		                 "   round((cif.FG_CAPITAL_CONTABLE/ (/* Formatted on 2015/10/03 16:28 (Formatter Plus v4.8.8) */ SELECT fn_valor_compra FROM com_tipo_cambio WHERE ic_moneda = 40 AND ROWID = (SELECT MAX (ROWID) FROM com_tipo_cambio  WHERE ic_moneda = 40))),2) AS CARTERA_PYME,  " +
		                 "  round((cif.FG_CAPITAL_CONTABLE*0.30),2) as CONCENTRACION_ACRE_PM, cs.FG_SALDO_VIGENTE_PRINC_PM, " +
		                 "  round((cif.FG_CAPITAL_CONTABLE*0.10),2) as CONCENTRACION_ACRE_PF,  cs.FG_SALDO_VIGENTE_PRINC_PF,   " +
		                 "  round( (cif.FG_CARTERA_TOTAL*0.35),2) as CONCENTRACION_CARTERA_VIGENTE, (select SUM(FG_SALDO) from cedi_saldo_acreditado where ic_solicitud = ? AND CC_TIPO_ACREDITADO = 'V') as SUMA_10_PRINCIPALES " +
		                 "  from cedi_informacion_financiera cif, cedi_solicitud cs " + "   where cif.IC_SOLICITUD = ? " +
		                 "  AND cif.IC_SOLICITUD = cs.IC_SOLICITUD " +
		                 "  AND cif.IC_ANIO = cs.IG_ANIO_INF_FINANCIERA ";
		    varBind = new ArrayList();
		    varBind.add(clave_solicitud);
		    varBind.add(clave_solicitud);
		    ps = con.queryPrecompilado(qry, varBind);
		    rs = ps.executeQuery();
		    if (rs.next()) {
		        
		        FG_CARTERA = rs.getDouble("CARTERA_PYME");
		        CONC_ACRED_PM = rs.getDouble("CONCENTRACION_ACRE_PM");
		        CONC_ACRED_PF = rs.getDouble("CONCENTRACION_ACRE_PF");
		        CONC_CART_VIG = rs.getDouble("CONCENTRACION_CARTERA_VIGENTE");
		        FG_SALDO_VIGENTE_PRINC_PM = rs.getDouble("FG_SALDO_VIGENTE_PRINC_PM");
		        FG_SALDO_VIGENTE_PRINC_PF = rs.getDouble("FG_SALDO_VIGENTE_PRINC_PF");
		        SUMA_10_PRINCIPALES = rs.getDouble("SUMA_10_PRINCIPALES");

		    }

			if (CAPITALIZACION < 12) {
				exito = false;
				log.debug("CAPITALIZACION "+CAPITALIZACION);
			}
			if (MOROSIDAD >= 5 && MOROSIDAD < 0) {
				exito = false;
			    log.debug("MOROSIDAD "+MOROSIDAD);
			}
			if (COBERTURA < 100) {
				exito = false;
			    log.debug("COBERTURA "+COBERTURA);
			}
			/*if (ROE <= 4) {
				exito = false;
			    log.debug("ROE "+ROE);
			}
			if (LIQUIDEZ < 1) {
				exito = false;
			    log.debug("LIQUIDEZ "+LIQUIDEZ);
			}
			if (APALANCAMIENTO > 2.5 && APALANCAMIENTO < 0) {
				exito = false;
			    log.debug("APALANCAMIENTO "+APALANCAMIENTO);
			}*/
			if (tipo_cartera.equals("P")) {
				if (FG_CARTERA < 9000000) {
					exito = false;
				    log.debug("FG_CARTERA "+FG_CARTERA);
				}
			}
			if (tipo_cartera.equals("M")) {
				if (FG_CARTERA < 5000000) {
					exito = false;
				    log.debug("FG_CARTERA "+FG_CARTERA);
				}
			}
			if(CONC_ACRED_PM>0){
				if (FG_SALDO_VIGENTE_PRINC_PM > CONC_ACRED_PM) {
					exito = false;
				    log.debug("CONC_ACRED_PM "+CONC_ACRED_PM);
				}
			}else{
			    exito = false;
			}
		    if(CONC_ACRED_PF>0){
				if (FG_SALDO_VIGENTE_PRINC_PF > CONC_ACRED_PF) {
					exito = false;
				    log.debug("CONC_ACRED_PF "+CONC_ACRED_PF);
				}
			}else{
			    exito = false;
			}
			if(CONC_CART_VIG>0){
				if (SUMA_10_PRINCIPALES > CONC_CART_VIG) {
					exito = false;
				    log.debug("CONC_CART_VIG "+CONC_CART_VIG);
				}
			}else{
			    exito = false;
			}
		    log.debug("validaViabilidadCedula  "+exito);
		} catch (Throwable t) {
			throw new AppException("validaViabilidadCedula(Error): Error al guardar la informaci�n", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("validaViabilidadCedula():: Error al cerrar Recursos: " + t.getMessage());
			}
		}
		log.error("validaViabilidadCedula(S)");
		return exito;
	}

	public boolean validaViabilidadCedula( String clave_solicitud, String tipo_cartera) {
		log.info("validaViabilidadCedula(E)");
	    AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
	    PreparedStatement ps2 = null;
	    ResultSet rs2 = null;
		List varBind = new ArrayList();
		String qry = "";

		String ic_pregunta = "";
		String ic_resp = "";
		String opc_resp = "";
		String resp_libre = "";

		double CAPITALIZACION = 0;
		double MOROSIDAD = 0;
		double COBERTURA = 0;
		double ROE = 0;
		double LIQUIDEZ = 0;
		double APALANCAMIENTO = 0;
		double FG_CARTERA = 0;
		double CONC_ACRED_PM = 0;
		double CONC_ACRED_PF = 0;
		double CONC_CART_VIG = 0;
		double FG_SALDO_VIGENTE_PRINC_PM = 0;
		double FG_SALDO_VIGENTE_PRINC_PF = 0;
		double SUMA_10_PRINCIPALES = 0;

		try {
			
			// Inserta la viabilidad de la informaci�n Cualitativa
			con.conexionDB();
		    boolean existe_resp_info_cual= true;
		    
		    List causa_rechazo = new ArrayList();
		    causa_rechazo.add(4);
		    causa_rechazo.add(7);
		    causa_rechazo.add(8);
		    causa_rechazo.add(9);
		    causa_rechazo.add(10);
		    causa_rechazo.add(11);
		    causa_rechazo.add(12);
		    causa_rechazo.add(13);
		    causa_rechazo.add(16);
		    causa_rechazo.add(17);
		    causa_rechazo.add(18);
		    causa_rechazo.add(19);
		    causa_rechazo.add(20);
		    for(int i = 0; i<causa_rechazo.size();i++){
		        // Inserta la viabilidad de la informaci�n Cualitativa
		        qry = "";
		        qry = "  SELECT  count(*) total_opc_resp " +
		                     "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
		                     "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
		                     "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ? " +
		                     "  AND crp.ic_pregunta = ? ";
		        varBind = new ArrayList();
		        varBind.add(clave_solicitud);
		        varBind.add(causa_rechazo.get(i));
		    
		        log.debug("qry :: resp_preg " + qry);
		        log.debug("varBind ::resp_preg " + varBind);
		        ps = con.queryPrecompilado(qry, varBind);
		        rs = ps.executeQuery();
		        if (rs.next()) {
					String dato = rs.getString("total_opc_resp");
					if(dato.equals("0")){
						existe_resp_info_cual= false;
		            }else{
						existe_resp_info_cual= true;
					}
		        }
		        rs.close();
		        ps.close();
		    }
		    log.debug("qry ::existe_resp_info_cual " + existe_resp_info_cual);
		    if(existe_resp_info_cual==true){
			    // Se obtiene los opciones de respuesta para la pregunta 4
			    List opc_respP4 = new ArrayList(); // Guarda las opciones de respuesta para la pregunta 4
			    String subQueryP4 = "   select IC_RESPUESTA_PARAMETRIZACION  from cedi_respuesta_parametrizacion where IC_PREGUNTA = 4 order by IC_RESPUESTA_PARAMETRIZACION";
			    ps2 = con.queryPrecompilado(subQueryP4);
			    rs2 = ps2.executeQuery();
			    while (rs2.next()) {
			       opc_respP4.add((String)rs2.getString("IC_RESPUESTA_PARAMETRIZACION"));
			    }
			    rs2.close();
			    rs2.close();
			    
			    List respP4 = new ArrayList();
			    HashMap datos = new HashMap();
			    qry = "";
			    qry = "  SELECT    cri.ic_respuesta_parametrizacion,cri.cg_respuesta_libre " +
			                 "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
			                 "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
			                 "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
			                 "  AND crp.ic_pregunta = 4 ";
			    varBind = new ArrayList();
			    varBind.add(clave_solicitud);
			    
			    log.debug("qry :: www " + qry);
			    log.debug("varBind ::www " + varBind);
			    ps2 = con.queryPrecompilado(qry, varBind);
			    rs2 = ps2.executeQuery();
			    
			    
			    while (rs2.next()) {
			        datos = new HashMap();
			        datos.put("clave_resp", rs2.getString("IC_RESPUESTA_PARAMETRIZACION") == null ? "" : rs2.getString("IC_RESPUESTA_PARAMETRIZACION"));
			        datos.put("respuesta", rs2.getString("CG_RESPUESTA_LIBRE") == null ? "" : rs2.getString("CG_RESPUESTA_LIBRE"));
			        respP4.add(datos);
			    }
			    rs2.close();
			    rs2.close();
			    if(respP4.size()>0){
			        
			            if(respP4.size()==1){
			                HashMap respuesta = (HashMap) respP4.get(0);
			                double clave_resp = Double.parseDouble((String) respuesta.get("clave_resp"));
			                double porc_resp = Double.parseDouble((String) respuesta.get("respuesta"));
			                for(int i=0;i <opc_respP4.size();i++){
			                    double clave_opc = Double.parseDouble((String) opc_respP4.get(i));
			                    if(clave_opc==clave_resp){
			                        if(i==2){
			                            exito = false;
			                        }
			                    }
			                }
			            }else if(respP4.size()>1){
			                double var1 = 0;
			                double var2 = 0;
			                double var3 = 0;
			                double var1_por = 0;
			                double var2_por= 0;
			                double var3_por= 0;
			                
			                int pos_aux = -1;
			                int tam_resp = respP4.size();
			                for(int x=0;x <respP4.size();x++){
			                    HashMap respuesta = (HashMap) respP4.get(x);
			                    double clave_resp = Double.parseDouble((String) respuesta.get("clave_resp"));
			                    double porc_resp = Double.parseDouble((String) respuesta.get("respuesta"));
			                    if(x==0){
			                        var1 = clave_resp;
			                        var1_por = porc_resp;
			                    }else if(x==1){
			                        var2 = clave_resp;
			                        var2_por = porc_resp;
			                    }else if(x==2){
			                        var3 = clave_resp;
			                        var3_por = porc_resp;
			                    }
			                    for(int i=0;i <opc_respP4.size();i++){
			                        double clave_opc = Double.parseDouble((String) opc_respP4.get(i));
			                        if(clave_opc==clave_resp){
			                            if(i==2){
			                                pos_aux = x;
			                            }
			                        }
			                    }
			                }
			                if(pos_aux!=-1){
			                    HashMap respuesta = (HashMap) respP4.get(pos_aux);
			                    double clave_resp = Double.parseDouble((String) respuesta.get("clave_resp"));
			                    double porc_resp = Double.parseDouble((String) respuesta.get("respuesta"));
			                    for(int x=0;x <respP4.size();x++){
			                        HashMap respuesta_r = (HashMap) respP4.get(x);
			                        double clave = Double.parseDouble((String) respuesta_r.get("clave_resp"));
			                        double porc = Double.parseDouble((String) respuesta_r.get("respuesta"));
			                        if(porc_resp>porc){
			                            exito = false;
			                        }
			                    }
			                    
			                    
			                }
			            }
			    }
			    log.debug("validsacion pregunta 4 exito "+exito);
			    
				qry = "";
				qry = "  SELECT   crp.ic_pregunta, cri.ic_respuesta_parametrizacion,crp.cg_opcion_respuesta, cri.cg_respuesta_libre " +
							 "  FROM cedi_respuesta_parametrizacion crp,cedi_respuesta_ifnb cri,cedi_pregunta cp " +
							 "  WHERE crp.ic_respuesta_parametrizacion = cri.ic_respuesta_parametrizacion" +
							 "  AND crp.ic_pregunta = cp.ic_pregunta" + "  AND cri.ic_solicitud = ?" +
							 "  AND crp.ic_pregunta IN (7, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19, 20) ";
				varBind = new ArrayList();
				varBind.add(clave_solicitud);
	
				log.debug("qry :: " + qry);
				log.debug("varBind :: " + varBind);
				ps = con.queryPrecompilado(qry, varBind);
				rs = ps.executeQuery();
				while (rs.next()) {
					ic_pregunta = rs.getString("ic_pregunta") == null ? "" : rs.getString("ic_pregunta");
					ic_resp = rs.getString("ic_respuesta_parametrizacion") == null ? "" :
							  rs.getString("ic_respuesta_parametrizacion");
					opc_resp = rs.getString("cg_opcion_respuesta") == null ? "" : rs.getString("cg_opcion_respuesta");
					resp_libre = rs.getString("cg_respuesta_libre") == null ? "" : rs.getString("cg_respuesta_libre");
	
					if (ic_pregunta.equals("7")) {
						if (!resp_libre.equals("")) {
							int anios = Integer.parseInt(resp_libre.replace(" ", ""));
							if (anios < 2) {
								exito = false;
							}
						} else {
							exito = false;
						}
					}else if (ic_pregunta.equals("8")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					}else if (ic_pregunta.equals("9")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("10")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("11")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("12")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("13")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("16")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("17")) {
						if (!opc_resp.equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("18")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("19")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					} else if (ic_pregunta.equals("20")) {
						if (!opc_resp.toUpperCase().equals("SI")) {
							exito = false;
						}
					}
				}
				rs.close();
				ps.close();
			}else{
			    exito = false;
			}
			// Inserta el resultado de viabilidad de los indicadores

			qry = 
			"   SELECT IC_ANIO,ic_solicitud, round((((FG_CAPITAL_CONTABLE +(FG_ESTIMACION_RESERVAS-FG_CARTERA_VENCIDA)) - (0.05*FG_CARTERA_VIGENTE))/FG_CARTERA_TOTAL)*100,2) AS CAPITALIZACION, " +
			"  round((decode(FG_CARTERA_VENCIDA,0,1,FG_CARTERA_VENCIDA)/FG_CARTERA_TOTAL)*100 ,2) AS MOROSIDAD," +
			"  round((FG_ESTIMACION_RESERVAS/decode(FG_CARTERA_VENCIDA,0,1,FG_CARTERA_VENCIDA))*100,2) AS COVERTURA_RESERVA," +
			" round((FG_UTILIDAD_NETA/FG_CAPITAL_CONTABLE)*100,2) AS ROE," +
			"  round((FG_INVERSION_CAJA/FG_PASIVOS_TOTALES)*100,2) AS LIQUIDEZ," +
			"  round((FG_PASIVOS_TOTALES/FG_CAPITAL_CONTABLE)*100,2) AS APALANCAMIENTO" +
			"  FROM cedi_informacion_financiera" + "  WHERE IC_SOLICITUD = ?" +
			"  ORDER BY IC_ANIO DESC";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				CAPITALIZACION = rs.getDouble("CAPITALIZACION");
				MOROSIDAD = rs.getDouble("MOROSIDAD");
				COBERTURA = rs.getDouble("COVERTURA_RESERVA");
				ROE = rs.getDouble("ROE");
				LIQUIDEZ = rs.getDouble("LIQUIDEZ");
				APALANCAMIENTO = rs.getDouble("APALANCAMIENTO");
				
				
			}
			rs.close();
			ps.close();
			
			qry = " select  cif.IC_ANIO,    " +
						 "   round((cif.FG_CAPITAL_CONTABLE/ (/* Formatted on 2015/10/03 16:28 (Formatter Plus v4.8.8) */ SELECT fn_valor_compra FROM com_tipo_cambio WHERE ic_moneda = 40 AND ROWID = (SELECT MAX (ROWID) FROM com_tipo_cambio  WHERE ic_moneda = 40))),2) AS CARTERA_PYME,  " +
						 "  round((cif.FG_CAPITAL_CONTABLE*0.30),2) as CONCENTRACION_ACRE_PM, cs.FG_SALDO_VIGENTE_PRINC_PM, " +
						 "  round((cif.FG_CAPITAL_CONTABLE*0.10),2) as CONCENTRACION_ACRE_PF,  cs.FG_SALDO_VIGENTE_PRINC_PF,   " +
						 "  round( (cif.FG_CARTERA_TOTAL*0.35),2) as CONCENTRACION_CARTERA_VIGENTE, (select SUM(FG_SALDO) from cedi_saldo_acreditado where ic_solicitud = ? AND CC_TIPO_ACREDITADO = 'V') as SUMA_10_PRINCIPALES " +
						 "  from cedi_informacion_financiera cif, cedi_solicitud cs " + "   where cif.IC_SOLICITUD = ? " +
						 "  AND cif.IC_SOLICITUD = cs.IC_SOLICITUD " +
						 "  AND cif.IC_ANIO = cs.IG_ANIO_INF_FINANCIERA ";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);
			varBind.add(clave_solicitud);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				
				FG_CARTERA = rs.getDouble("CARTERA_PYME");
				CONC_ACRED_PM = rs.getDouble("CONCENTRACION_ACRE_PM");
				CONC_ACRED_PF = rs.getDouble("CONCENTRACION_ACRE_PF");
				CONC_CART_VIG = rs.getDouble("CONCENTRACION_CARTERA_VIGENTE");
				FG_SALDO_VIGENTE_PRINC_PM = rs.getDouble("FG_SALDO_VIGENTE_PRINC_PM");
				FG_SALDO_VIGENTE_PRINC_PF = rs.getDouble("FG_SALDO_VIGENTE_PRINC_PF");
				SUMA_10_PRINCIPALES = rs.getDouble("SUMA_10_PRINCIPALES");

			}

			if (CAPITALIZACION < 12) {
				exito = false;
				log.debug("CAPITALIZACION "+CAPITALIZACION);
			}
			if (MOROSIDAD >= 5 && MOROSIDAD < 0) {
				exito = false;
				log.debug("MOROSIDAD "+MOROSIDAD);
			}
			if (COBERTURA < 100) {
				exito = false;
				log.debug("COBERTURA "+COBERTURA);
			}
			/*if (ROE <= 4) {
				exito = false;
				log.debug("ROE "+ROE);
			}
			if (LIQUIDEZ < 1) {
				exito = false;
				log.debug("LIQUIDEZ "+LIQUIDEZ);
			}
			if (APALANCAMIENTO > 2.5 && APALANCAMIENTO < 0) {
				exito = false;
				log.debug("APALANCAMIENTO "+APALANCAMIENTO);
			}*/
			if (tipo_cartera.equals("P")) {
				if (FG_CARTERA < 9000000) {
					exito = false;
					log.debug("FG_CARTERA "+FG_CARTERA);
				}
			}
			if (tipo_cartera.equals("M")) {
				if (FG_CARTERA < 5000000) {
					exito = false;
					log.debug("FG_CARTERA "+FG_CARTERA);
				}
			}
			if(CONC_ACRED_PM>0){
				if (FG_SALDO_VIGENTE_PRINC_PM > CONC_ACRED_PM) {
					exito = false;
					log.debug("CONC_ACRED_PM "+CONC_ACRED_PM);
				}
			}else{
				exito = false;
			}
			if(CONC_ACRED_PF>0){
				if (FG_SALDO_VIGENTE_PRINC_PF > CONC_ACRED_PF) {
					exito = false;
					log.debug("CONC_ACRED_PF "+CONC_ACRED_PF);
				}
			}else{
				exito = false;
			}
			if(CONC_CART_VIG>0){
				if (SUMA_10_PRINCIPALES > CONC_CART_VIG) {
					exito = false;
					log.debug("CONC_CART_VIG "+CONC_CART_VIG);
				}
			}else{
				exito = false;
			}
			log.debug("validaViabilidadCedula  "+exito);
		} catch (Throwable t) {
		    exito = false;
			throw new AppException("validaViabilidadCedula(Error): Error al guardar la informaci�n", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("validaViabilidadCedula():: Error al cerrar Recursos: " + t.getMessage());
			}
		    if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
		        con.cierraConexionDB();
			}
		}
		log.error("validaViabilidadCedula(S)");
		return exito;
	}
	/**
	 *Funci�n que cambia el estatus de la solicitud
	 * @param registros
	 * @param usuario
	 * @param ic_solicitud
	 * @param ArchAdjunto
	 * @param estatus
	 * @return
	 */
	public boolean cambiaEstatusSolic(List registros, String usuario, String ic_solicitud, List ArchAdjunto,
									  String estatus) {
		log.info("cambiaEstatusSolic(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		//PreparedStatement ps1 = null;
		List varBind = new ArrayList();
		String qry = "";


		try {
			con.conexionDB();

			qry = "";
			qry = " 	UPDATE cedi_solicitud" + "	SET IC_ESTATUS_SOLICITUD = ?" + "  WHERE IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add("2");
			varBind.add(ic_solicitud);
			log.debug("qrySolicitud11 :: " + qry);
			log.debug("varBind11 :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
		    
			if(estatus.equals("1")){
				this.setMovimientoSolicitus(con, ic_solicitud, usuario, "2", "3","");
			}else if(estatus.equals("7")||estatus.equals("8")){
			    this.setMovimientoSolicitus(con, ic_solicitud, usuario, "2", "12","");
			}


		} catch (Throwable t) {
			exito = false;
			throw new AppException("cambiaEstatusSolic(Error): Error al consultar informaci�n de las preguntas", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("cambiaEstatusSolic():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		return exito;

	}

	/**
	 * Funci�n que envia correo de aceptac�n a los Usuarios del Gestor cedi
	 * @param registros
	 * @param ic_solicitud
	 * @param archivoAdjunto
	 */
	public void enviaCorreoNotiCalificateAceptacion(List registros, String ic_solicitud, ArrayList archivoAdjunto, String estatus_f) {
		log.info("enviaCorreoNotiCalificateAceptacion(E)");
		Correo correo = new Correo();
	    correo.setCodificacion("charset=UTF-8");
	    StringBuffer tabla = new StringBuffer();
		String ccCorreo = "";
	    StringBuffer asunto = new StringBuffer();
		StringBuffer textoA = new StringBuffer();
	    StringBuffer textoB = new StringBuffer();
	    String aQuien = "";
		String textoCorreo = "";
		List listInfoAspirante = new ArrayList();
		HashMap hmAspirante = new HashMap();
	    HashMap hmAspirante1 = new HashMap();
	    HashMap hmAspirante2 = new HashMap();
	    HashMap hmAspirante3 = new HashMap();
		try {

		    listInfoAspirante = this.getInformacionAspirante(ic_solicitud,estatus_f);
			hmAspirante = (HashMap) listInfoAspirante.get(10);
			String estatus = (String) hmAspirante.get("IC_ESTATUS_SOLICITUD");
		    
		    String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
		        "font-size: 10px; "+
		        "font-weight: bold;"+
		        "color: #FFFFFF;"+
		        "background-color: #4d6188;"+
		        "padding-top: 3px;"+
		        "padding-right: 1px;"+
		        "padding-bottom: 1px;"+
		        "padding-left: 3px;"+
		        "height: 25px;"+
		        "border: 1px solid #1a3c54;'";

		    String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
		        "color:#000066; "+
		        "padding-top:1px; "+
		        "padding-right:2px; "+
		        "padding-bottom:1px; "+
		        "padding-left:2px; "+
		        "height:22px; "+
		        "font-size:11px;'";	
			//tabla.append("<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:10;'><br>");
			tabla.append("<table border=\"1\">");
			tabla.append(" <tr>" + 
							 "    <td align=\"center\" " + styleEncabezados + ">Raz&oacute;n Social</td>" +
							 "    <td align=\"center\" " + styleEncabezados + ">R.F.C</td>" + 
							 "    <td align=\"center\" " + styleEncabezados +">Folio Solicitud</td>" + 
							 "    <td align=\"center\" " + styleEncabezados + ">Fecha de Registro</td>" +
						 " </tr>");
		    hmAspirante = (HashMap) listInfoAspirante.get(0);
			hmAspirante1 = (HashMap) listInfoAspirante.get(1);
		    hmAspirante2 = (HashMap) listInfoAspirante.get(2);
		    hmAspirante3 = (HashMap) listInfoAspirante.get(7);
		    String razon = (String) hmAspirante1.get("CG_RAZON_SOCIAL");
		   /* byte ptextRazon[] = razon.getBytes("ISO-8859-1");
		    String razonF = new String(ptextRazon, "UTF-8");*/
			tabla.append("   <tr> " +
							 " <td align=\"left\" " + style + "> " + razon + "</td>" +
							 " <td align=\"left\" " + style + "> " + (String) hmAspirante2.get("CG_RFC") + "</td>" + 
							 " <td align=\"center\" " +style + "> " + (String) hmAspirante.get("IC_SOLICITUD") + "</td>" +
							 " <td align=\"center\" " + style + "> " +(String) hmAspirante3.get("DF_MOVIMIENTO") + "</td>" + 
						 " </tr> ");
			tabla.append("</table>");
			//tabla.append("</form>");
			if (estatus.equals("7")) {
				asunto.append("Solicitud Rechazada-Calif\u00EDcate");
				textoA.append("Estimado Gestor CEDI <br><br>Por este conducto, se le informa que la siguiente solicitud no ha sido elegible para su viabilidad de incorporaci&oacute;n a la red de intermediarios Financieros de Nacional Financiera:<br><br>");
			} else if (estatus.equals("2")) {
				asunto.append("Atenci\u00F3n de Solicitudes");
				textoA.append("Estimado Gestor CEDI <br><br>Por este conducto, se le informa que la siguiente solicitud ha sido aceptada para validar su viabilidad de incorporaci&oacute;n a Nacional Financiera, favor de realizar el an&aacute;lisis correspondiente:<br><br>");
			}
			textoB.append("<br>Sin otro particular, reciba un cordial saludo." + "<br><br>ATENTAMENTE<br>" +
						 "Centro de Desarrollo de Intermediarios");
			/*String totalCorreo = textoA.toString() +tabla + textoB.toString();
		    byte ptextCorreo[] = totalCorreo.getBytes();
		    String correoF = new String(ptextCorreo, "UTF-8");*/
			
		   
			
			for (int i = 0; i < registros.size(); i++) {
				HashMap solicitud = (HashMap) registros.get(i);
				String perfil = solicitud.get("PERFIL").toString();
				String login = solicitud.get("LOGIN").toString();
				String nombre = solicitud.get("NOMBRE").toString();
				String apellidoPaterno = solicitud.get("APELLIDO_PATERMO").toString();
				String apellidoMaterno = solicitud.get("APELLIDO_MATERNO").toString();
				aQuien = solicitud.get("CORREO").toString();
				textoCorreo = "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>" + textoA.toString() +tabla + textoB.toString()+ "</form>";
				correo.enviaCorreoConDatosAdjuntos("no_response@nafin.gob.mx", aQuien, ccCorreo, asunto.toString(), textoCorreo.toString(),
												   null, null);

			}
		} catch (Throwable t) {
			throw new AppException("enviaCorreoNotiCalificateAceptacion() Error al enviar correo ", t);
		}
	}

	public boolean actualizaSaldos(String ic_solicitud) {

		log.info("actualizaSaldos(E)  ");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" SELECT count(*)  as total  FROM cedi_saldo_acreditado where IC_SOLICITUD =  ? ");
			lVarBind = new ArrayList();
			lVarBind.add(ic_solicitud);
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			rs = ps.executeQuery();
			rs.next();
			boolean existeRegistro = (rs.getInt("total") == 0) ? false : true;
			rs.close();
			ps.close();

			if (existeRegistro) {
				strSQL = new StringBuffer();
				strSQL.append(" delete cedi_saldo_acreditado where IC_SOLICITUD = ?   ");
				lVarBind = new ArrayList();
				lVarBind.add(ic_solicitud);
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();
			}

		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("actualizaSaldos  " + e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  actualizaSaldos (S) ");
			}
		}
		return exito;
	}

	public String getEstatusSoli(String ic_solicitud) {

		log.info("getEstatusSoli(E)  ");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		StringBuffer strSQL = new StringBuffer();
		List lVarBind = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String estatus = "";
		try {
			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append(" SELECT IC_ESTATUS_SOLICITUD  FROM cedi_solicitud where IC_SOLICITUD =  ? ");
			lVarBind = new ArrayList();
			lVarBind.add(ic_solicitud);
		    log.debug("strSQL :: "+strSQL);
		    log.debug("lVarBind :: "+lVarBind);
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				estatus = rs.getString("IC_ESTATUS_SOLICITUD");
			}
		    log.debug("estatus :: "+estatus);
			rs.close();
			ps.close();


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("getEstatusSoli  " + e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getEstatusSoli (S) ");
			}
		}
		return estatus;
	}


	/**
	 *
	 * @return
	 */

	public List getCatalogoTipoPregunta() {

		log.info("getCatalogoTipoPregunta(E)  ");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List reg = new ArrayList();
		HashMap datos = new HashMap();

		try {
			con.conexionDB();

			String qrySentencia =
						 "select IC_TIPO_RESPUESTA as clave, CD_TIPO_RESPUESTA as descripcion " +
						 " from comcat_tipo_respuesta " + " where  IC_TIPO_RESPUESTA in (1,2,3) ";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();

			while (rs.next()) {
				datos = new HashMap();
				datos.put("descripcion", rs.getString("DESCRIPCION") == null ? "" : rs.getString("DESCRIPCION"));
				datos.put("clave", rs.getString("CLAVE") == null ? "" : rs.getString("CLAVE"));
				reg.add(datos);
			}
			rs.close();
			ps.close();


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("getEstatusSoli  " + e);
			throw new AppException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getEstatusSoli (S) ");
			}
		}
		return reg;
	}

	private void eliminaActualizaCedula(AccesoDB con, String IC_SOLICITUD, String IC_ELEMENTO) {
		log.info("eliminaActualizaCedula (E)");

		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String existe = "";
		boolean exito = true;
		try {

			String qryConsulta =
						 " SELECT DECODE(COUNT(*),0,'N','S') AS EXISTE " + " FROM cedi_cedula" +
						 " WHERE IC_ELEMENTO_CEDULA = ?" + " AND IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(IC_ELEMENTO);
			varBind.add(IC_SOLICITUD);
			log.debug("qryConsulta ::: " + qryConsulta);
			log.debug(" varBind :: " + varBind);
			ps = con.queryPrecompilado(qryConsulta, varBind);
			rs = ps.executeQuery();
			if (rs.next()) {
				existe = rs.getString("EXISTE");
			}
			rs.close();
			ps.close();
			if (existe.equals("S")) {
				String strSQL = " delete cedi_cedula " + " WHERE IC_ELEMENTO_CEDULA = ? " + " AND IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(IC_ELEMENTO);
				varBind.add(IC_SOLICITUD);
				log.debug("qry :: " + strSQL);
				log.debug("varBind :: " + varBind);
				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				ps.executeUpdate();
				ps.close();

			}


		} catch (Throwable t) {
			exito = false;
			throw new AppException("eliminaActualizaCedula(Error): Error al guardar la informaci�n en cedi_ifnb_aspirante",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("eliminaActualizaCedula():: Error al cerrar Recursos: " + t.getMessage());
			}

		}

	}

	public boolean updateConfPreg(String ic_resp_para, String activo) {
		log.info("updateConfPreg (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();

		try {
			con.conexionDB();


			String strSQL =
						 " UPDATE cedi_respuesta_parametrizacion " + " SET CS_ACTIVO = ?  " +
						 " WHERE IC_RESPUESTA_PARAMETRIZACION = ? ";
			varBind = new ArrayList();
			varBind.add(activo);
			varBind.add(ic_resp_para);
			log.debug("strSQL " + strSQL);
			log.debug("varBind " + varBind);

			ps = con.queryPrecompilado(strSQL, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();


		} catch (Throwable e) {
			exito = false;
			e.printStackTrace();
			log.error("updateConfPreg  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  updateConfPreg (S) ");
			}
		}
		return exito;
	}
	
	/*public List mostrarBotonEstatus(AccesoDB con, String clave_solicitud) {
		log.info("mostrarBotonEstatus(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";

		String ESTATUS = "";
		String TOTAL_ESTATUS = "";
		String ESTATUS_FINAL = "";
	    String EXISTE_RECHAZO = "N";
	    Map mpesta = new HashMap();
		List lstEsta = new ArrayList();
	    String total = "";
		
	    List listResul = new ArrayList();
	    List mpResula = new ArrayList();
		try {
			qry = "";
			qry = "  select count(IC_ESTATUS_SOLICITUD) as total_estatus, IC_ESTATUS_SOLICITUD " +
						 "  from CEDI_SOLICITUD_MOVIMIENTO " +
						 "   where IC_SOLICITUD=?  " +
				  "		group  by IC_ESTATUS_SOLICITUD 	";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				ESTATUS = rs.getString("IC_ESTATUS_SOLICITUD") == null ? "" : rs.getString("IC_ESTATUS_SOLICITUD");
				TOTAL_ESTATUS = rs.getString("TOTAL_ESTATUS") == null ? "" :rs.getString("TOTAL_ESTATUS");
				if(ESTATUS.equals("1")||ESTATUS.equals("7")||ESTATUS.equals("2")||ESTATUS.equals("8")){
				    mpesta = new HashMap();
				    mpesta.put("TOTAL_ESTATUS", rs.getString("TOTAL_ESTATUS") == null ? "" : rs.getString("TOTAL_ESTATUS"));
				    mpesta.put("ESTATUS", rs.getString("IC_ESTATUS_SOLICITUD") == null ? "" : rs.getString("IC_ESTATUS_SOLICITUD"));
				    lstEsta.add(mpesta);
				}
				
			}
		    rs.close();
			ps.close();
			
		    qry = "";
		    qry = "  select IC_ESTATUS_SOLICITUD " +
		                 "  from CEDI_SOLICITUD " +
		                 "   where IC_SOLICITUD=?  " ;
		    varBind = new ArrayList();
		    varBind.add(clave_solicitud);

		    log.debug("qry :: " + qry);
		    log.debug("varBind :: " + varBind);
		    ps = con.queryPrecompilado(qry, varBind);
		    rs = ps.executeQuery();
		    if(rs.next()) {
		        ESTATUS_FINAL = rs.getString("IC_ESTATUS_SOLICITUD") == null ? "" : rs.getString("IC_ESTATUS_SOLICITUD");
		    }
		    rs.close();
		    ps.close();
		    mpesta.put("IC_ESTATUS_SOLICITUD",ESTATUS_FINAL);
		    
			for(int i = 0 ; i < lstEsta.size(); i ++){
			    mpesta = new HashMap();
			    mpesta = (HashMap)lstEsta.get(i);
			    Iterator it = mpesta.keySet().iterator();
				String estatus = "";
			   
			    while(it.hasNext()){
					String key =(String) it.next();	
					if(key.equals("ESTATUS")){
						estatus = (String)mpesta.get(key);	
					    if(estatus.equals(ESTATUS_FINAL)){
							total = (String)mpesta.get("TOTAL_ESTATUS");
						}
					    if(estatus.equals("7")){
							EXISTE_RECHAZO = "S";
						}
					                            
						
					}	
				}
			}
		    mpesta.put("TOTAL_ESTATUS",total);
		    listResul.add(mpesta);

		} catch (Throwable t) {
			throw new AppException("viabilidadInfoCualitativa(Error): Error al guardar la informaci�n", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("viabilidadInfoCualitativa():: Error al cerrar Recursos: " + t.getMessage());
			}
		}
		return listResul;
	}*/
	public List consultaBotonEstatus(String clave_solicitud) {
		log.info("consultaBotonEstatus(E)");
	    AccesoDB con = new AccesoDB();
		boolean exito = true;
	   
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";

		String ESTATUS = "";
		String TOTAL_ESTATUS = "";
		String ESTATUS_FINAL = "0";
	    String EXISTE_RECHAZO = "N";
		Map mpesta = new HashMap();
		List lstEsta = new ArrayList();
		String total_aceptada = "0";
	    String total_rechazo = "0";
	    String existe_cedula = "0";
		
		List listResul = new ArrayList();
		Map mpResula = new HashMap();
		String resultado = "";
		try {
		    con.conexionDB();
		    qry = "";
			qry = "  select IC_ESTATUS_SOLICITUD " +
		          "  from CEDI_SOLICITUD " +
		          "   where IC_SOLICITUD=?  " ;
		    varBind = new ArrayList();
		    varBind.add(clave_solicitud);

		    log.debug("qry :: " + qry);
		    log.debug("varBind :: " + varBind);
		    ps = con.queryPrecompilado(qry, varBind);
		    rs = ps.executeQuery();
		    if(rs.next()) {
				ESTATUS_FINAL = rs.getString("IC_ESTATUS_SOLICITUD") == null ? "" : rs.getString("IC_ESTATUS_SOLICITUD");
			}
			rs.close();
			ps.close();
			
			qry = "";
			qry = "  select count(IC_ESTATUS_SOLICITUD) as total_aceptada " +
						 "  from CEDI_SOLICITUD_MOVIMIENTO  " +
						 "   where IC_SOLICITUD=?   " +
				  "     AND IC_MOVIMIENTO = 3  "+
				  "	group  by IC_ESTATUS_SOLICITUD";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry ::   " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			while (rs.next()) {
				total_aceptada = rs.getString("total_aceptada") == null ? "0" : rs.getString("total_aceptada");
				
				
			}
		    rs.close();
			ps.close();
			
			qry = "";
			qry = "  select count(*) as total_rechazo " +
				 "  from CEDI_SOLICITUD_MOVIMIENTO  " +
				 "   where IC_SOLICITUD=?  " +
				  "	AND IC_ESTATUS_SOLICITUD = 7 or  IC_ESTATUS_SOLICITUD = 8"+
				  "	group  by IC_ESTATUS_SOLICITUD";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			if(rs.next()) {
				total_rechazo = rs.getString("total_rechazo") == null ? "0" : rs.getString("total_rechazo");
			}
		    rs.close();
		    ps.close();
			
		    qry = "";
		    qry = "  select count(*) existe_cedula " +
		                 "  from cedi_cedula  " +
		                 "   where IC_SOLICITUD=?   ";
		    varBind = new ArrayList();
		    varBind.add(clave_solicitud);

		    log.debug("qry ::   " + qry);
		    log.debug("varBind :: " + varBind);
		    ps = con.queryPrecompilado(qry, varBind);
		    rs = ps.executeQuery();
		    while (rs.next()) {
		        existe_cedula = rs.getString("EXISTE_CEDULA") == null ? "0" : rs.getString("EXISTE_CEDULA");
		        
		        
		    }
		    rs.close();
		    ps.close();
			
			int total_aceptada_valor = Integer.parseInt(total_aceptada);
		    int total_rechazo_valor = Integer.parseInt(total_rechazo);
			if(total_rechazo_valor>0){
				if(total_rechazo_valor==1&&total_aceptada_valor==0){
				    resultado = "1VR";  
				}else if(total_rechazo_valor>1&&total_aceptada_valor==0){
				    resultado = "2VR"; 
				}else if(total_aceptada_valor>0){
				    resultado = "2VA"; 
				}
				
			}else{
			    if(total_aceptada_valor>0){
					resultado = "1VA";	
				}
			}
			
		    mpResula.put("RESPUESTA",resultado);
		    mpResula.put("EXISTE_CEDULA",existe_cedula);
		    listResul.add(mpResula);

		} catch (Throwable e) {
		    try {
		        if (rs != null)
		            rs.close();
		        if (ps != null)
		            ps.close();
		    } catch (Exception t) {
		        log.error("consultaBotonEstatus():: Error al cerrar Recursos: " + t.getMessage());
		    }
			exito = false;
			e.printStackTrace();
			log.error("consultaBotonEstatus  " + e);
			throw new AppException("SIST0001");

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  consultaBotonEstatus (S) ");
			}
		}
		return listResul;
	}
	
	public Map getInformacionCualiPDF(String ic_solicitud) {

		log.info("getInformacionCualiPDF(E)  ");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
	    PreparedStatement ps1 = null;
	    ResultSet rs1 = null;
		String estatus = "";
		
		int numPregunta = 0;
		String tipo_pregunta = "";
		String  CG_NOMBRE_OTRO = "";
		String CG_OPCION_RESPUESTA = "";
		String CG_RESPUESTA_LIBRE = "";
	    String CG_PREGUNTA = "";
		
	    Map mpGeral = new HashMap();
	    String qryConsultaRes ="";
		try {
			con.conexionDB();
			String qry = "";
		    Map mpDataResp = new HashMap();
		    List lstRespPreg = new ArrayList();
		    List lstAcreditado = new ArrayList();
		    qry = "SELECT   ic_pregunta, cg_pregunta FROM cedi_pregunta where CS_ACTIVO = 'S' ORDER BY ic_pregunta ASC";
		    ps = con.queryPrecompilado(qry);
		    rs = ps.executeQuery();
		   
		    while (rs.next()) {
				numPregunta = Integer.parseInt(rs.getString("IC_PREGUNTA"));
		        CG_PREGUNTA = rs.getString("CG_PREGUNTA") == null ? "" : rs.getString("CG_PREGUNTA");
				 qryConsultaRes =" select cri.IC_TIPO_RESPUESTA, cp.IC_RESPUESTA_PARAMETRIZACION,cri.CG_OPCION_RESPUESTA , cp.CG_RESPUESTA_LIBRE as CG_RESPUESTA_LIBRE, cp.CG_NOMBRE_OTRO " +
		                " from cedi_respuesta_parametrizacion cri,cedi_respuesta_ifnb cp, cedi_pregunta preg   " +
						"	WHERE cri.ic_respuesta_parametrizacion = cp.ic_respuesta_parametrizacion	"+
						"	AND cri.ic_pregunta = preg.ic_pregunta"+
						"	and cri.IC_PREGUNTA = "+rs.getString("IC_PREGUNTA")+
						"   and cp.ic_solicitud = ? ";
		             	varBind = new ArrayList();
						varBind.add(ic_solicitud);

						log.debug("qry :: " + qryConsultaRes);
		                log.debug("varBind :: " + varBind);
						ps1 = con.queryPrecompilado(qryConsultaRes, varBind);
						rs1 = ps1.executeQuery();
						
						String cadena = "";
		                 while(rs1.next()) {
		                    CG_OPCION_RESPUESTA = rs1.getString("CG_OPCION_RESPUESTA") == null ? "" : rs1.getString("CG_OPCION_RESPUESTA");
							CG_RESPUESTA_LIBRE = rs1.getString("CG_RESPUESTA_LIBRE") == null ? "" : rs1.getString("CG_RESPUESTA_LIBRE");
		                    CG_NOMBRE_OTRO = rs1.getString("CG_NOMBRE_OTRO") == null ? "" : rs1.getString("CG_NOMBRE_OTRO");
		                     tipo_pregunta = rs1.getString("IC_TIPO_RESPUESTA") == null ? "" : rs1.getString("IC_TIPO_RESPUESTA");
							 if(tipo_pregunta.equals("2")&&!CG_RESPUESTA_LIBRE.equals("")){
								 if(CG_NOMBRE_OTRO.equals("")){
									cadena+= CG_OPCION_RESPUESTA +" "+CG_RESPUESTA_LIBRE +"% "+CG_NOMBRE_OTRO+"\n"; 
								 }else{
								     cadena+= CG_OPCION_RESPUESTA +" "+CG_NOMBRE_OTRO +" "+CG_RESPUESTA_LIBRE+"% \n"; 
								 }
							 }else{
							     cadena+= CG_OPCION_RESPUESTA +" "+CG_RESPUESTA_LIBRE +" "+CG_NOMBRE_OTRO+"\n";
							 }
		                    
		                 }
						rs1.close();
						ps1.close();
						mpDataResp = new HashMap();
						mpDataResp.put("numPregunta",numPregunta);
						mpDataResp.put("CG_PREGUNTA",CG_PREGUNTA);
						mpDataResp.put("RESPUESTA",cadena);
						lstRespPreg.add(mpDataResp);
						
				
		    }
		    rs.close();
		    ps.close();
		    
		    mpGeral.put("P2",lstRespPreg);   
		   qryConsultaRes =" select '�El Intermediario cuenta con alg'||chr(250)||'n cr'||chr(233)||'dito vinculado?' as CG_MONTO_CRED_VINCULADO, FG_MONTO_CRED_VINCULADO, CS_CREDITO_VINCULADO from  cedi_solicitud " +
		        " where IC_SOLICITUD = ?    ";
			varBind = new ArrayList();
		    varBind.add(ic_solicitud);

		    log.debug("qry :: " + qryConsultaRes);
		    log.debug("varBind :: " + varBind);
		    ps1 = con.queryPrecompilado(qryConsultaRes, varBind);
		    rs1 = ps1.executeQuery();
		            
			String cadena = "";
			if(rs1.next()) {
		        mpDataResp = new HashMap();
		        mpDataResp.put("CG_MONTO_CRED_VINCULADO",rs1.getString("CG_MONTO_CRED_VINCULADO") == null ? "" : rs1.getString("CG_MONTO_CRED_VINCULADO"));
		        mpDataResp.put("FG_MONTO_CRED_VINCULADO",rs1.getString("FG_MONTO_CRED_VINCULADO") == null ? "" : rs1.getString("FG_MONTO_CRED_VINCULADO"));
		        mpDataResp.put("CS_CREDITO_VINCULADO",rs1.getString("CS_CREDITO_VINCULADO") == null ? "" : rs1.getString("CS_CREDITO_VINCULADO"));
		    }
		    rs1.close();
		    ps1.close();
		    mpGeral.put("P3",mpDataResp); 
			
		    List datosReg = this.getStoreGridInfoFinanciera(ic_solicitud);
			HashMap datos = new HashMap();
		    List reg = new ArrayList();
			if(datosReg.size()>0){
		       List listColumna1 = (ArrayList)datosReg.get(2);
		       List listColumna2 = (ArrayList)datosReg.get(1);
		       List listColumna3 = (ArrayList)datosReg.get(0);
		       for(int i = 0; i< listColumna1.size(); i++){
		           HashMap hmInfo = (HashMap)listColumna1.get(i);
		           Iterator it = hmInfo.keySet().iterator();
		           HashMap hmInfo1 = (HashMap)listColumna2.get(i);
		           Iterator it1 = hmInfo1.keySet().iterator();
		           HashMap hmInfo2 = (HashMap)listColumna3.get(i);
		           Iterator it2 = hmInfo2.keySet().iterator();
		            if(i==2){
						String key = "FG_ACTIVOS_TOTALES";
		                datos = new HashMap();
		                datos.put("CAMPO","FG_ACTIVOS_TOTALES");
		                datos.put("CS_CONCEPTO","Activos Totales");
		                datos.put("ANIO_1",hmInfo.get(key));
		                datos.put("ANIO_2",hmInfo1.get(key));
		                datos.put("ANIO_3",hmInfo2.get(key));
		                reg.add(datos);
					}else if(i==3){
		                String key = "FG_INVERSION_CAJA";
		                datos = new HashMap();
		                datos.put("CAMPO","FG_INVERSION_CAJA");
		                datos.put("CS_CONCEPTO","Disponibles");
		                datos.put("ANIO_1",hmInfo.get(key));
		                datos.put("ANIO_2",hmInfo1.get(key));
		                datos.put("ANIO_3",hmInfo2.get(key));
		                reg.add(datos);
		            }else if(i==4){
						String key = "FG_CARTERA_VIGENTE";
		                datos = new HashMap();
		                datos.put("CAMPO","FG_CARTERA_VIGENTE");
		                datos.put("CS_CONCEPTO","Cartera Vigente");
		                datos.put("ANIO_1",hmInfo.get(key));
		                datos.put("ANIO_2",hmInfo1.get(key));
		                datos.put("ANIO_3",hmInfo2.get(key));
		                reg.add(datos);
					}else if(i==5){
		                 String key = "FG_CARTERA_VENCIDA";
		                 datos = new HashMap();
		                 datos.put("CAMPO","FG_CARTERA_VENCIDA");
		                 datos.put("CS_CONCEPTO","Cartera Vencida");
		                 datos.put("ANIO_1",hmInfo.get(key));
		                 datos.put("ANIO_2",hmInfo1.get(key));
		                 datos.put("ANIO_3",hmInfo2.get(key));
		                 reg.add(datos);
		            }else if(i==6){
		                 String key = "FG_CARTERA_TOTAL";
		                 datos = new HashMap();
		                 datos.put("CAMPO","FG_CARTERA_TOTAL");
		                 datos.put("CS_CONCEPTO","Cartera Total");
		                 datos.put("ANIO_1",hmInfo.get(key));
		                 datos.put("ANIO_2",hmInfo1.get(key));
		                 datos.put("ANIO_3",hmInfo2.get(key));
		                 reg.add(datos);
		            }else if(i==7){
		                 String key = "FG_ESTIMACION_RESERVAS";
		                 datos = new HashMap();
		                 datos.put("CAMPO","FG_ESTIMACION_RESERVAS");
		                 datos.put("CS_CONCEPTO","Estimaci�n de Reservas");
		                 datos.put("ANIO_1",hmInfo.get(key));
		                 datos.put("ANIO_2",hmInfo1.get(key));
		                 datos.put("ANIO_3",hmInfo2.get(key));
		                 reg.add(datos);
		            }else if(i==8){
		                 String key = "FG_PASIVOS_BANCARIOS";
		                 datos = new HashMap();
		                 datos.put("CAMPO","FG_PASIVOS_BANCARIOS");
		                 datos.put("CS_CONCEPTO","Pasivos Bancarios");
		                 datos.put("ANIO_1",hmInfo.get(key));
		                 datos.put("ANIO_2",hmInfo1.get(key));
		                 datos.put("ANIO_3",hmInfo2.get(key));
		                 reg.add(datos);
		            }else if(i==9){
		                  String key = "FG_PASIVOS_TOTALES";
						  datos = new HashMap();
		                  datos.put("CAMPO","FG_PASIVOS_TOTALES");
		                  datos.put("CS_CONCEPTO","Pasivos Totales");
		                  datos.put("ANIO_1",hmInfo.get(key));
		                  datos.put("ANIO_2",hmInfo1.get(key));
		                  datos.put("ANIO_3",hmInfo2.get(key));
		                  reg.add(datos);
					}else if(i==10){
		                  String key = "FG_CAPITAL_SOCIAL";
		                  datos = new HashMap();
		                  datos.put("CAMPO","FG_CAPITAL_SOCIAL");
		                  datos.put("CS_CONCEPTO","Capital Social");
		                  datos.put("ANIO_1",hmInfo.get(key));
		                  datos.put("ANIO_2",hmInfo1.get(key));
		                  datos.put("ANIO_3",hmInfo2.get(key));
		                  reg.add(datos);
					}else if(i==11){
		                  String key = "FG_CAPITAL_CONTABLE";
		                  datos = new HashMap();
		                  datos.put("CAMPO","FG_CAPITAL_CONTABLE");
		                  datos.put("CS_CONCEPTO","Capital Contable");
		                  datos.put("ANIO_1",hmInfo.get(key));
		                  datos.put("ANIO_2",hmInfo1.get(key));
		                  datos.put("ANIO_3",hmInfo2.get(key));
		                  reg.add(datos);
		             }else if(i==12){
		                  String key = "FG_UTILIDAD_NETA";
		                  datos = new HashMap();
		                  datos.put("CAMPO","FG_UTILIDAD_NETA");
		                  datos.put("CS_CONCEPTO","Utilidad Neta");
		                  datos.put("ANIO_1",hmInfo.get(key));
		                  datos.put("ANIO_2",hmInfo1.get(key));
		                  datos.put("ANIO_3",hmInfo2.get(key));
		                  reg.add(datos);
		             }
		                    
				}
		            
			}
		    mpGeral.put("P4Grid",reg);
		    qryConsultaRes =" select IG_ANIO_INF_FINANCIERA, IG_MES_INF_FINANCIERA  from cedi_solicitud " +
		        " where IC_SOLICITUD = ?    ";
		    varBind = new ArrayList();
		    varBind.add(ic_solicitud);

		    log.debug("qry :: " + qryConsultaRes);
		    log.debug("varBind :: " + varBind);
		    ps1 = con.queryPrecompilado(qryConsultaRes, varBind);
		    rs1 = ps1.executeQuery();
		    if(rs1.next()) {
		        mpDataResp = new HashMap();
		        mpDataResp.put("IG_ANIO_INF_FINANCIERA",rs1.getString("IG_ANIO_INF_FINANCIERA") == null ? "" : rs1.getString("IG_ANIO_INF_FINANCIERA"));
		        mpDataResp.put("IG_MES_INF_FINANCIERA",rs1.getString("IG_MES_INF_FINANCIERA") == null ? "" : rs1.getString("IG_MES_INF_FINANCIERA"));
		    }
		    rs1.close();
		    ps1.close();
		    mpGeral.put("P4AnioMes",mpDataResp);
			
		  
			
		    lstAcreditado = new ArrayList();
		    qryConsultaRes =" select 'Acreditado '||IC_SALDO_ACREDITADO as IC_SALDO_ACREDITADO,FG_SALDO  from CEDI_SALDO_ACREDITADO  " +
		        " where CC_TIPO_ACREDITADO = 'V'   "+
				" and IC_SOLICITUD = ?    ";
		    varBind = new ArrayList();
		    varBind.add(ic_solicitud);

		    log.debug("qry :: " + qryConsultaRes);
		    log.debug("varBind :: " + varBind);
		    ps1 = con.queryPrecompilado(qryConsultaRes, varBind);
		    rs1 = ps1.executeQuery();
		    while(rs1.next()) {
		        mpDataResp = new HashMap();
		        mpDataResp.put("IC_SALDO_ACREDITADO",rs1.getString("IC_SALDO_ACREDITADO") == null ? "" : rs1.getString("IC_SALDO_ACREDITADO"));
		        mpDataResp.put("FG_SALDO",rs1.getString("FG_SALDO") == null ? "" : rs1.getString("FG_SALDO"));
			    lstAcreditado.add(mpDataResp);
			}
		    rs1.close();
		    ps1.close();
		    mpGeral.put("P5Acreditados",lstAcreditado);
			
		    lstAcreditado = new ArrayList();
			
		    qryConsultaRes =" select 'Acreditado '||IC_SALDO_ACREDITADO as IC_SALDO_ACREDITADO,FG_SALDO  from CEDI_SALDO_ACREDITADO  " +
		        " where CC_TIPO_ACREDITADO = 'I'   "+
		        " and IC_SOLICITUD = ?    ";
		    varBind = new ArrayList();
		    varBind.add(ic_solicitud);

		    log.debug("qry :: " + qryConsultaRes);
		    log.debug("varBind :: " + varBind);
		    ps1 = con.queryPrecompilado(qryConsultaRes, varBind);
		    rs1 = ps1.executeQuery();
		    while(rs1.next()) {
		        mpDataResp = new HashMap();
		        mpDataResp.put("IC_SALDO_ACREDITADO",rs1.getString("IC_SALDO_ACREDITADO") == null ? "" : rs1.getString("IC_SALDO_ACREDITADO"));
		        mpDataResp.put("FG_SALDO",rs1.getString("FG_SALDO") == null ? "" : rs1.getString("FG_SALDO"));
		        lstAcreditado.add(mpDataResp);
		    }
		    rs1.close();
		    ps1.close();
		    mpGeral.put("P5AcreditadosInc",lstAcreditado);
			
			
		    qryConsultaRes =" select FG_SALDO_VIGENTE_PRINC_PM , FG_SALDO_VIGENTE_PRINC_PF from cedi_solicitud " +
		        " where IC_SOLICITUD = ?  ";
		    varBind = new ArrayList();
		    varBind.add(ic_solicitud);

		    log.debug("qry :: " + qryConsultaRes);
		    log.debug("varBind :: " + varBind);
		    ps1 = con.queryPrecompilado(qryConsultaRes, varBind);
		    rs1 = ps1.executeQuery();
		    while(rs1.next()) {
		        mpDataResp = new HashMap();
		        mpDataResp.put("FG_SALDO_VIGENTE_PRINC_PM",rs1.getString("FG_SALDO_VIGENTE_PRINC_PM") == null ? "" : rs1.getString("FG_SALDO_VIGENTE_PRINC_PM"));
		        mpDataResp.put("FG_SALDO_VIGENTE_PRINC_PF",rs1.getString("FG_SALDO_VIGENTE_PRINC_PF") == null ? "" : rs1.getString("FG_SALDO_VIGENTE_PRINC_PF"));
		    }
		    rs1.close();
		    ps1.close();
		    mpGeral.put("P5Saldos",mpDataResp);
			
	
		} catch (Throwable e) {
			
			exito = false;
			e.printStackTrace();
			log.error("getInformacionCualiPDF  " + e);
			throw new AppException("SIST0001");
		} finally {
		    try {
		        if (rs != null)
		            rs.close();
		        if (ps != null)
		            ps.close();
		        if (rs1 != null)
		            rs1.close();
		        if (ps1 != null)
		            ps1.close();
		    } catch (Exception t) {
		        log.error("getInformacionCualiPDF():: Error al cerrar Recursos: " + t.getMessage());
		    }
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.info("  getInformacionCualiPDF (S) ");
			}
		}
		return mpGeral;
	}
	
	public String existeRechazoMovimientos(String clave_solicitud) {
		log.info("existeRechazoMovimientos(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
	   
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";

		
		List listResul = new ArrayList();
		String existe_rechazo = "";
		try {
			con.conexionDB();
			qry = "  select count(IC_ESTATUS_SOLICITUD) as existe_rechazo " +
				  "  from CEDI_SOLICITUD_MOVIMIENTO " +
				  "   where IC_SOLICITUD=? " +
				  "	 and IC_ESTATUS_SOLICITUD  = 7"+
				  "	group by IC_ESTATUS_SOLICITUD ";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			if(rs.next()) {
				existe_rechazo = rs.getString("EXISTE_RECHAZO") == null ? "" : rs.getString("EXISTE_RECHAZO");
			}
			rs.close();
			ps.close();
			
			
		} catch (Throwable e) {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("existeRechazoMovimientos():: Error al cerrar Recursos: " + t.getMessage());
			}
			exito = false;
			e.printStackTrace();
			log.error("existeRechazoMovimientos  " + e);
			throw new AppException("SIST0001");

		} finally {
			
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  existeRechazoMovimientos (S) ");
			}
		}
		return existe_rechazo;
	}
	
	public boolean actualizaEstatusSolicitud(String ic_solicitud, String estatus) {
		log.info("actualizaEstatusSolicitud(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List varBind = new ArrayList();
		String qry = "";


		try {
			con.conexionDB();

			qry = "     UPDATE cedi_solicitud" + "  SET IC_ESTATUS_SOLICITUD = ?" + "  WHERE IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(estatus);
			varBind.add(ic_solicitud);
			log.debug("qrySolicitud11 :: " + qry);
			log.debug("varBind11 :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
		    log.debug("varBind11 ::actualizaEstatusSolicitud termino " + varBind);

		} catch (Throwable t) {
			exito = false;
			throw new AppException("actualizaEstatusSolicitud(Error): Error al consultar informaci�n de las preguntas", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("actualizaEstatusSolicitud():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		return exito;

	}
	
	public boolean actualizaDictamenConfirmacion(String ic_solicitud, String resultado_dictamen) {
		log.info("actualizaDictamenConfirmacion(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		List varBind = new ArrayList();
		String qry = "";


		try {
			con.conexionDB();

			qry = "     UPDATE cedi_solicitud" +
				  "  SET CS_INDICADOR_AUX = ?" + 
				  "  WHERE IC_SOLICITUD = ?";
			varBind = new ArrayList();
			varBind.add(resultado_dictamen);
			varBind.add(ic_solicitud);
			log.debug("qrySolicitud11 : : " + qry);
			log.debug("varBind11 :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			ps.executeUpdate();
			ps.clearParameters();
			ps.close();
			

		} catch (Throwable t) {
			exito = false;
			throw new AppException("actualizaDictamenConfirmacion(Error): Error al consultar informaci�n de las preguntas", t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("actualizaDictamenConfirmacion():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		return exito;

	}
	
	public String consultaDictamenSolicitud(String clave_solicitud) {
		log.info("consultaDictamenSolicitud(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
	   
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";

		String valor_dictamen = "";
		try {
			con.conexionDB();
			qry = "  select CS_DICTAMEN_CONFIRMADO " +
				  "  from cedi_solicitud " +
				  " where IC_SOLICITUD = ?  ";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				valor_dictamen = rs.getString("CS_DICTAMEN_CONFIRMADO") == null ? "" : rs.getString("CS_DICTAMEN_CONFIRMADO");
			}
			rs.close();
			ps.close();
			
			
		} catch (Throwable e) {
			
			exito = false;
			e.printStackTrace();
			log.error("consultaDictamenSolicitud  " + e);
			throw new AppException("SIST0001");

		} finally {
		    try {
		        if (rs != null)
		            rs.close();
		        if (ps != null)
		            ps.close();
		    } catch (Exception t) {
		        log.error("consultaDictamenSolicitud():: Error al cerrar Recursos: " + t.getMessage());
		    }
			
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  consultaDictamenSolicitud (S) ");
			}
		}
		return valor_dictamen;
	}
	
	public String existeCredVinculado(String clave_solicitud) {
		log.info("existeCredVinculado(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
	   
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String qry = "";

		String cred_vinculado = "";
		try {
			con.conexionDB();
			qry = "  select CS_CREDITO_VINCULADO " +
				  " from cedi_solicitud " +
				  " where IC_SOLICITUD = ?  ";
			varBind = new ArrayList();
			varBind.add(clave_solicitud);

			log.debug("qry :: " + qry);
			log.debug("varBind :: " + varBind);
			ps = con.queryPrecompilado(qry, varBind);
			rs = ps.executeQuery();
			
			if(rs.next()) {
				cred_vinculado = rs.getString("CS_CREDITO_VINCULADO") == null ? "" : rs.getString("CS_CREDITO_VINCULADO");
			}
			rs.close();
			ps.close();
			
			
		} catch (Throwable e) {
			
			exito = false;
			e.printStackTrace();
			log.error("existeCredVinculado  " + e);
			throw new AppException("SIST0001");

		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("existeCredVinculado():: Error al cerrar Recursos: " + t.getMessage());
			}
			
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
				log.debug("  existeCredVinculado (S) ");
			}
		}
		return cred_vinculado;
	}

	public boolean eliminarPregunta(String IC_PREGUNTA, String IC_TIPO_RESPUESTA) {

			log.info("eliminarPregunta(E)  ");

			AccesoDB con = new AccesoDB();
			boolean exito = true;
			StringBuffer strSQL = new StringBuffer();
			List lVarBind = new ArrayList();
			PreparedStatement ps = null;
			ResultSet rs = null;

			try {
				con.conexionDB();

				
				strSQL = new StringBuffer();
				strSQL.append("     delete cedi_pregunta where IC_PREGUNTA = ? and IC_TIPO_RESPUESTA = ?   ");
				lVarBind = new ArrayList();
				lVarBind.add(IC_PREGUNTA);
				lVarBind.add(IC_TIPO_RESPUESTA);
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();
				
				strSQL = new StringBuffer();
				strSQL.append("     delete cedi_respuesta_parametrizacion   where IC_PREGUNTA = ?   and IC_TIPO_RESPUESTA = ?   ");
				lVarBind = new ArrayList();
				lVarBind.add(IC_PREGUNTA);
				lVarBind.add(IC_TIPO_RESPUESTA);
				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();

			} catch (Throwable e) {
				exito = false;
				e.printStackTrace();
				log.error("eliminarPregunta  " + e);
				throw new AppException("SIST0001");
			} finally {
				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(exito);
					con.cierraConexionDB();
					log.info("  eliminarPregunta (S) ");
				}
			}
			return exito;
		}
	
	
	private boolean guardarInfoNuevaSolicitud(String ic_solicitud_ant, String ic_solicitud_nueva) {
		log.info("CediEJB::guardarInfoNuevaSolicitud(E) " );

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;

		PreparedStatement psR = null;
		ResultSet rsR = null;

		PreparedStatement psChk = null;
		ResultSet rsChk = null;

		HashMap hmInfPreguntas = new HashMap();
		List varBind = new ArrayList();
		
	    String qryConsulta = "";
		boolean exito = true;

		try {
			con.conexionDB();

			
	//******************
			
				qryConsulta =
							 " select IC_PREGUNTA,IC_TIPO_RESPUESTA,CG_PREGUNTA,IG_NUM_OPCIONES, CS_ACTIVO  " +
							 " from cedi_pregunta" + " ORDER BY IC_PREGUNTA ASC";
				ps = con.queryPrecompilado(qryConsulta);
				rs = ps.executeQuery();
				while (rs.next()) {
					
					String qryConsultaRes =
									" select IC_RESPUESTA_PARAMETRIZACION AS IC_RESP, IC_PREGUNTA,  IC_TIPO_RESPUESTA AS TIPO_RESP, CG_OPCION_RESPUESTA AS OPC_RESP, CG_PREGUNTAS_EXCLUIR AS PREG_EXC, DECODE(CG_PREGUNTAS_EXCLUIR,'','N','S') AS BAN_EXCLUIR,CS_LIBRE_OBLIGATORIA AS LIBRE_OBLIG, CS_RESPUESTA_ABIERTA_ASOC AS RESP_ABIERTA_ASO, CS_LIBRE_TIPO_VALIDACION AS LIBRE_TIPO_DATO,IG_LIBRE_LONGITUD_MAXIMA AS LIBRE_LONG,  CS_ACTIVO, CS_OPCION_OTROS AS OPCION_OTROS " +
									" from cedi_respuesta_parametrizacion" + " where IC_PREGUNTA = " +
									rs.getString("IC_PREGUNTA");
					psR = con.queryPrecompilado(qryConsultaRes);
					rsR = psR.executeQuery();
					
	
					while (rsR.next()) {
						
						String 	 qryCheckOpcion =
												" INSERT INTO cedi_respuesta_ifnb(IC_SOLICITUD,IC_RESPUESTA_PARAMETRIZACION,CG_RESPUESTA_LIBRE,CG_NOMBRE_OTRO)  "+
												
											   " SELECT "+ic_solicitud_nueva+" AS ic_solicitud, IC_RESPUESTA_PARAMETRIZACION, CG_RESPUESTA_LIBRE,CG_NOMBRE_OTRO" +
											   " FROM cedi_respuesta_ifnb " +
											   " WHERE IC_RESPUESTA_PARAMETRIZACION = ?"+
											   "    AND IC_SOLICITUD = ?    ";
							varBind = new ArrayList();
							varBind.add(rsR.getString("IC_RESP") == null ? "" : rsR.getString("IC_RESP"));
							varBind.add(ic_solicitud_ant);
							
							log.debug("qryCheckOpcion :::  "+qryCheckOpcion+"  varBind :::  "+varBind);
							
							psChk = con.queryPrecompilado(qryCheckOpcion, varBind);
							psChk.executeUpdate();
							psChk.clearParameters();
							psChk.close();
						
						
					}
					rsR.close();
					psR.close();
				}
				rs.close();
				ps.close();
				
				qryConsulta = "";
				qryConsulta =
							 " SELECT CS_CREDITO_VINCULADO, FG_MONTO_CRED_VINCULADO, IC_SOLICITUD  " + " FROM cedi_solicitud " +
							 " WHERE IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(ic_solicitud_ant);
				log.debug("qryConsulta ::: "+qryConsulta+"   varBind :: "+varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					   String  qryCheckOpcion =
												"  UPDATE cedi_solicitud " + "  set CS_CREDITO_VINCULADO = ?, " + "  FG_MONTO_CRED_VINCULADO = ?" +
												 "  WHERE IC_SOLICITUD = ?";
						varBind = new ArrayList();
						varBind.add(rs.getString("CS_CREDITO_VINCULADO") == null ? "" :rs.getString("CS_CREDITO_VINCULADO"));
						varBind.add(rs.getString("FG_MONTO_CRED_VINCULADO") == null ? "" :rs.getString("FG_MONTO_CRED_VINCULADO"));
						varBind.add(ic_solicitud_nueva);
						
						log.debug("qryCheckOpcion :::  "+qryCheckOpcion+"  varBind :::  "+varBind);
						
						psChk = con.queryPrecompilado(qryCheckOpcion, varBind);
						psChk.executeUpdate();
						psChk.clearParameters();
						psChk.close();
					
				}
	
				rs.close();
				ps.close();
				
			
				qryConsulta = "";
				qryConsulta =
							 " SELECT IG_ANIO_INF_FINANCIERA,IG_MES_INF_FINANCIERA  " + " FROM cedi_solicitud " +
							 " WHERE IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(ic_solicitud_ant);
				 log.debug("qryConsulta ::: "+qryConsulta +"  varBind :: "+varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					
					   String  qryCheckOpcion =
						"  UPDATE cedi_solicitud " + "  set IG_ANIO_INF_FINANCIERA = ?, " + "  IG_MES_INF_FINANCIERA = ?" +
												 "  WHERE IC_SOLICITUD = ?";
						varBind = new ArrayList();
						
						varBind.add(rs.getString("IG_ANIO_INF_FINANCIERA") == null ? "" :rs.getString("IG_ANIO_INF_FINANCIERA"));
						varBind.add(rs.getString("IG_MES_INF_FINANCIERA") == null ? "" :rs.getString("IG_MES_INF_FINANCIERA"));
						varBind.add(ic_solicitud_nueva);
						log.debug("qryCheckOpcion :::  "+qryCheckOpcion+"  varBind :::  "+varBind);
						psChk = con.queryPrecompilado(qryCheckOpcion, varBind);
						psChk.executeUpdate();
						psChk.clearParameters();
						psChk.close();
						
						
						qryCheckOpcion =
							" INSERT INTO cedi_informacion_financiera(IC_SOLICITUD,IC_ANIO,FG_ACTIVOS_TOTALES,FG_INVERSION_CAJA,FG_CARTERA_VIGENTE, FG_CARTERA_VENCIDA,FG_CARTERA_TOTAL,FG_ESTIMACION_RESERVAS, FG_PASIVOS_BANCARIOS, FG_PASIVOS_TOTALES,FG_CAPITAL_SOCIAL,\n" + 
							" FG_CAPITAL_CONTABLE, FG_UTILIDAD_NETA)  "+
							"   select "+ic_solicitud_nueva+" AS ic_solicitud,IC_ANIO,FG_ACTIVOS_TOTALES,FG_INVERSION_CAJA,FG_CARTERA_VIGENTE, FG_CARTERA_VENCIDA,FG_CARTERA_TOTAL,FG_ESTIMACION_RESERVAS, FG_PASIVOS_BANCARIOS, FG_PASIVOS_TOTALES,FG_CAPITAL_SOCIAL," +
							"   FG_CAPITAL_CONTABLE, FG_UTILIDAD_NETA from cedi_informacion_financiera " +
							"   where IC_SOLICITUD = ?";
						 varBind = new ArrayList();
						 varBind.add(ic_solicitud_ant);
						 log.debug("qryCheckOpcion :::  "+qryCheckOpcion+"  varBind :::  "+varBind);
						 psChk = con.queryPrecompilado(qryCheckOpcion, varBind);
						 psChk.executeUpdate();
						 psChk.clearParameters();
						 psChk.close();
						
						
				}
				rs.close();
				ps.close();
				
				qryConsulta = "";
				qryConsulta =
							 " SELECT FG_SALDO_VIGENTE_PRINC_PM ,FG_SALDO_VIGENTE_PRINC_PF  " + " FROM cedi_solicitud " +
							 " WHERE IC_SOLICITUD = ? ";
				varBind = new ArrayList();
				varBind.add(ic_solicitud_ant);
				log.debug("qryConsulta ::: " + qryConsulta + " varBind :: " + varBind);
				ps = con.queryPrecompilado(qryConsulta, varBind);
				rs = ps.executeQuery();
				if (rs.next()) {
					
					   String  qryCheckOpcion =
						"  UPDATE cedi_solicitud " + "  set FG_SALDO_VIGENTE_PRINC_PM = ?, " + "  FG_SALDO_VIGENTE_PRINC_PF = ?" +
												 "  WHERE IC_SOLICITUD = ?";
						varBind = new ArrayList();
						
						
						varBind.add(rs.getString("FG_SALDO_VIGENTE_PRINC_PM") == null ? "" :rs.getString("FG_SALDO_VIGENTE_PRINC_PM"));
						varBind.add(rs.getString("FG_SALDO_VIGENTE_PRINC_PF") == null ? "" :rs.getString("FG_SALDO_VIGENTE_PRINC_PF"));
						
						varBind.add(ic_solicitud_nueva);
						log.debug("qryCheckOpcion :::  "+qryCheckOpcion+"  varBind :::  "+varBind);
						psChk = con.queryPrecompilado(qryCheckOpcion, varBind);
						psChk.executeUpdate();
						psChk.clearParameters();
						psChk.close();
						
						qryCheckOpcion =
							"  INSERT INTO cedi_saldo_acreditado(IC_SOLICITUD,CC_TIPO_ACREDITADO,IC_SALDO_ACREDITADO,FG_SALDO)" + 
							"   SELECT "+ic_solicitud_nueva+" AS ic_solicitud, CC_TIPO_ACREDITADO, IC_SALDO_ACREDITADO, FG_SALDO   " +
							"   FROM cedi_saldo_acreditado " +
							"   where IC_SOLICITUD = ?";
						 varBind = new ArrayList();
						 varBind.add(ic_solicitud_ant);
						 log.debug("qryCheckOpcion :::  "+qryCheckOpcion+"  varBind :::  "+varBind);
						 psChk = con.queryPrecompilado(qryCheckOpcion, varBind);
						 psChk.executeUpdate();
						 psChk.clearParameters();
						 psChk.close();
						
					  
				}
				rs.close();
				ps.close();
				
			
				

		} catch (Throwable t) {
		    exito = false;
			throw new AppException("getInformacionInicialCalificate(Error): Error al consultar informaci�n de las preguntas",
								   t);
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();

				if (psR != null)
					psR.close();
				if (rsR != null)
					rsR.close();

				if (psChk != null)
					psChk.close();
				if (rsChk != null)
					rsChk.close();
			} catch (Exception t) {
				log.error("getInformacionInicialCalificate():: Error al cerrar Recursos: " + t.getMessage());
			}
			if (con.hayConexionAbierta()) {
			    con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("CediEJB::getInformacionInicialCalificate(S)");
		}

		return exito;
	}

		
}
