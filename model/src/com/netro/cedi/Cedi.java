
package com.netro.cedi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.usuarios.SolicitudArgus;

@Remote
public interface Cedi { 
	public Map getPreguntasIFNB ();
	public String modificacionEncuestaGral(List lstPreguntas, List lstRespuestas,String num_real_preg);	
	public String descargaArchivosSolic(String no_solicitud, String strDirectorioTemp, String tipoArchivo );


	public List  getDoctosCheckListSolicitud(String ic_solicitud  );  
	public String generarPDFCorreo(HttpServletRequest request, HashMap parametros );
	public boolean  cambioEstatusConfirma ( HashMap  parametros  );	
	public String desArchSolicCheckList(String no_solicitud, String strDirectorioTemp, String ic_doc_chekList, String extension ); 
	public List getCheckList ( String  solicitud , String csFijo  );
	
	public boolean guardarArchivoSolic(String no_solicitud,  String ic_doc_chekList , String rutaArchivo  );
	public boolean enviarArchivo(String no_solicitud, String ic_doc_chekList[], String  estatus, String strLogin  );
	public String desArchSolicPrevios(String no_solicitud, String strDirectorioTemp, String ic_doc_chekList, String extension );
	
	// san
	public SolicitudArgus generarSolicitudArgus(HttpServletRequest request);
	public boolean getExisteUsuarioIFCEDI(String rfc);
	public boolean guardaDatosIF(String razonSoc, String rfc, String usuario,String existeRFC);
	public Map getDatosInicialesCalificate(String rfc);
	public boolean completarInfoAsp(SolicitudArgus solicitud, String razonSocial,String nomCom ,String CG_CONTACTO, String CG_EMAIL, String ic_aspirante, String IC_SOLICITUD, String usuario, String estatus);
	public String getClaveSolicitud(String clave);
	public boolean actualizarInfoCualitativa(String ic_solicitud, List lstRespuestas,String usuario,String estatus);
	public Map getInformacionInicialCalificate(String rfc, String pestania);
	public boolean actualizaInfoCreditoVinculado( String clave_solicitud, String tieneCredVinc,String montoCredVinc,String usuario,String estatus);
	public String getClaveAspirante(String rfc);
	public List getStoreGridInfoFinanciera(String IC_SOLICITUD);
	public boolean guardaInfoAnio( String anio, String ic_solicitud);
	public boolean guardaInformacionFinanciera(String campo, String dato, String anio, String ic_solicitud);
	public Registros getStoreGridAcreditado(String IC_SOLICITUD, String tipo_acreditado);
	public boolean guardarInformacionSaldos(String IC_SOLICITUD, String tipo_acreditado, String num_acreditado, String saldo);
	public boolean guardarSaldoPMF(String IC_SOLICITUD, String saldoPM, String saldoPF);
	public List getStoreIndicadorFinan(String IC_SOLICITUD);
	public List getInfoFinanciera(String IC_SOLICITUD);
	public List getDestinoRecSol(String IC_SOLICITUD, String pregunta);
	public boolean InsertarAnioInfFinaciera(String clave_solicitud, String anio,String mes);
	public boolean guardaIndicadoresFinancieros(String IC_SOLICITUD, String pregunta, String usuario, String estatus);
	public List getStoreIndicadorResultado(String IC_SOLICITUD, String pregunta);
	public String generarArchivoCedula(List datoGrales);
	public boolean setMovimientoSolicitus( String IC_SOLICITUD,String IG_USUARIO,String ESTATUS, String MOVIMIENTO, String Detalle);
	//public boolean viabilidadInfoCualitativa(AccesoDB con,String clave_solicitud);
	//public void insertaActualizaCedula( AccesoDB con,String IC_SOLICITUD, String IC_ELEMENTO,String VIABLE,String CAMPO);
	public List getInformacionAspirante(String IC_SOLICITUD, String estatus);
	public List getInformacionIndicadores(String IC_SOLICITUD, String pantalla);
	public List getInformacionInfoCualitativa(String IC_SOLICITUD);
	public void enviaCorreoNotiCalificate(List registros,String ic_solicitud, String estatus_f);
	public boolean guardarArchEdoFinan(String ic_solicitud, String rutaArchivo, String tipoArchivo);
	public String descargaArchivosEdoFinan(String no_solicitud, String strDirectorioTemp, String tipoArchivo );
	public String generaNuevoFolioAspirante(String rfc);
	public String getTipoCartera(String IC_SOLICITUD,String pregunta);
	public boolean validaViabilidadCedula(String clave_solicitud,String tipo_cartera);
	public boolean cambiaEstatusSolic(List registros, String usuario,String ic_solicitud, List ArchAdjunto, String estatus);
	public void enviaCorreoNotiCalificateAceptacion(List registros,String ic_solicitud, ArrayList archivoAdjunto, String estatus_f );
	public boolean actualizaSaldos(String ic_solicitud);
	public String getEstatusSoli(String ic_solicitud);
	
	public List getCatalogoTipoPregunta();
	//public void eliminaActualizaCedula( AccesoDB con,String IC_SOLICITUD, String IC_ELEMENTO);
	public HashMap getDatosIF(String solicitud  ); 
	public boolean updateConfPreg(String  ic_resp_para,String  activo);
	//public boolean insertaViabilidadCedula(AccesoDB con,String clave_solicitud,String tipo_cartera,String estatus,String usuario);
	public Map informacionInicialTp(String clave_solicitud);
	//public List mostrarBotonEstatus(AccesoDB con, String clave_solicitud);
	public List consultaBotonEstatus(String clave_solicitud);
	public Map getInformacionCualiPDF(String ic_solicitud);
	public String existeRechazoMovimientos(String clave_solicitud);
	public boolean actualizaEstatusSolicitud(String ic_solicitud, String estatus);
	public String consultaDictamenSolicitud(String clave_solicitud);
	public boolean actualizaDictamenConfirmacion(String ic_solicitud, String resultado_dictamen);
	public String existeCredVinculado(String clave_solicitud);
	public boolean eliminarPregunta(String IC_PREGUNTA, String IC_TIPO_RESPUESTA);
	
}
