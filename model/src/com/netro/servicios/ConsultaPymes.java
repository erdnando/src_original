package com.netro.servicios;

import java.io.*;
import java.util.*;
import java.util.zip.*;
import java.sql.*;
import netropology.utilerias.*;
import netropology.utilerias.usuarios.*;
import com.netro.exception.*;

/**
 * Esta clase extrae permite realizar consultas que se muestran en combos, pero que no son un catalogo
 * @author Alberto Cruz Flores
 * @since 08/10/2008 F048 - 2008
 */
 
public class ConsultaPymes implements Serializable{
 
 /**
	* Constructor de la clase.
	*/
	public ConsultaPymes(){}

/**
 * Este m�todo permite generar la lista de pymes que se muestran dentro del combo de 
 * pyme en la pantalla de consulta de pymes para factoraje movil
 * @param claveUsuario es la clave de acceso del usuario pyme.
 * @param nafinElectronico es el numero de la pyme en nafin electr�nico.
 * @param razonSocial es el nombre o razon social de la pyme, dependiendo si es persona fisica o moral.
 * @param rfc es el rfc de la pyme.
 * @param numeroSirac es el numero de sirac de la pyme.
 * @return listaPymes una lista de objetos de tipo ElementoCatalogo que genera el combo de pymes de la pantalla.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public List getListaPymes(String claveUsuario, String nafinElectronico, String razonSocial, String rfc, String numeroSirac) throws NafinException{
		AccesoDB con = new AccesoDB();
		ResultSet rs	= null;
		StringBuffer strSQL	= new StringBuffer();
		List varBind = new ArrayList();
		List listaPymes = new ArrayList();
	
		try{
			con.conexionDB();
			
			strSQL.append(" SELECT pym.ic_pyme AS CLAVE,");
			strSQL.append(" crn.ic_nafin_electronico ||' '|| DECODE(pym.cs_tipo_persona, 'M', pym.cg_razon_social, 'F', pym.cg_appat || ' ' || pym.cg_apmat ||' '|| pym.cg_nombre) AS DESCRIPCION");
			strSQL.append(" FROM comcat_pyme pym,");
			strSQL.append(" comrel_nafin crn");
			strSQL.append(" WHERE pym.ic_pyme = crn.ic_epo_pyme_if");
			strSQL.append(" AND pym.cs_habilitado = ?");
			strSQL.append(" AND crn.cg_tipo = ?");
			
			varBind.add("S");
			varBind.add("P");

			if(!claveUsuario.equals("") && !claveUsuario.equals("*")) {
				UtilUsr utilUsr = new UtilUsr();
				Usuario usr = utilUsr.getUsuario(claveUsuario);
				
				String icPyme = usr.getClaveAfiliado();
				String tipoAfiliado = usr.getTipoAfiliado();
			
				if (tipoAfiliado != null && tipoAfiliado.equals("P") && tipoAfiliado != null && !tipoAfiliado.equals("")){
					strSQL.append("	AND pym.ic_pyme = ? ");
					
					varBind.add(new Long(icPyme));
				}
			}
						
			if(!nafinElectronico.equals("") && !nafinElectronico.equals("*")){
				strSQL.append(" AND crn.ic_nafin_electronico = ?");
				
				varBind.add(new Long(nafinElectronico));
			}
											
			if(!razonSocial.equals("")){
				strSQL.append(" AND pym.cg_razon_social LIKE NVL(REPLACE (?, '*', '%'), '%')");
									
				varBind.add(razonSocial.toUpperCase());
			}
			
			if(!rfc.equals("")){
				strSQL.append(" AND pym.cg_rfc LIKE NVL(REPLACE (?, '*', '%'), '%')");
									
				varBind.add(rfc.toUpperCase());
			}
			
			if(!numeroSirac.equals("") ){
				strSQL.append(" AND pym.IN_NUMERO_SIRAC = ? ");
				varBind.add(numeroSirac);
			}

/*
			if(!numeroSirac.equals("") && !numeroSirac.equals("*")){
				strSQL.append(" AND CPP.CS_CELULAR_MODIF = ?");

				varBind.add("S");
			}
*/			
			strSQL.append(" ORDER BY CLAVE");
			
			System.out.println("..:: strSQL : "+strSQL.toString());
			System.out.println("..:: varBind : "+varBind.toString());
			
			PreparedStatement preparedStatement = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = preparedStatement.executeQuery();
			
			while(rs.next()){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(rs.getString(1));
				elementoCatalogo.setDescripcion(rs.getString(2));
				listaPymes.add(elementoCatalogo);
			}
			
			rs.close();
			preparedStatement.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){				
				con.cierraConexionDB();
			}
		}
		return listaPymes;
	}
}