package com.netro.servicios;

import com.netro.descuento.ISeleccionDocumento;
import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.math.BigDecimal;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CorreoArchivoAdjunto;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.SMS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.usuarios.SolicitudArgus;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "ServiciosEJB" , mappedName = "ServiciosEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ServiciosBean implements Servicios {
    public ServiciosBean() {
    }

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ServiciosBean.class);

	/*************************************************************************************
	*   REGRESA UN VECTOR DE VECTORES CON LOS PORTLETS Y SUS POSICIONES CORRESPONDIENTES
	*   KE TIENE ASIGNADO UNA EPO DETERMINADA.
	*      ---  PANTALL MENUS Y DISE�OS
	*************************************************************************************/
	public Vector obtenPortletsxEpo( int liNumeroEpo )  throws NafinException {
		/*     [ IC_PORTLET, IN_POS ]    */

		boolean lbError = false;
		AccesoDB conexBD = new AccesoDB();
		Vector lvProtletEPo = new Vector();

		try {
			conexBD.conexionDB();
			String qryPortletsEPO = "";
			qryPortletsEPO  = "SELECT ic_portlet, in_pos FROM comrel_epo_portlet ";
			qryPortletsEPO += "WHERE ic_epo = " + String.valueOf(liNumeroEpo);

			ResultSet rsPortletsEPO = conexBD.queryDB( qryPortletsEPO );

			while(rsPortletsEPO.next())	{
				Vector lv_BanPos = new Vector();

				String m_sCvePortlet = "", m_sPosPortlet = "";
				m_sCvePortlet  =  String.valueOf( rsPortletsEPO.getInt("ic_portlet") );
				m_sPosPortlet  =  String.valueOf( rsPortletsEPO.getInt("in_pos") );

				lv_BanPos.addElement(m_sCvePortlet);
				lv_BanPos.addElement(m_sPosPortlet);
				lvProtletEPo.addElement(lv_BanPos);
			}

			rsPortletsEPO.close();
			return lvProtletEPo;
        } catch (Exception err) {
			lbError = true;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta())
				conexBD.cierraConexionDB();
		}
	}

	/*************************************************************************************
	*  GRABA LAS FECHAS DE PUBLICACION DE LOS BANNERS
	*     ---  PARA PANTALLA BANNER'S
	*************************************************************************************/
    public void modificaFechaBanner(String lsFchIni, String lsFchFin, String lsCveBanner) throws NafinException {
		System.out.print("\n Entrando a modificaFechaBanner() \n");
		AccesoDB conexBD = new AccesoDB();

		try{
			conexBD.conexionDB();

			String sSql = "UPDATE com_banner SET df_ini_banner = to_date('"+lsFchIni+"', 'yyyy-MM-dd'), ";
				sSql += "df_fin_banner = to_date('"+lsFchFin+"', 'yyyy-MM-dd') ";
				sSql += "WHERE ic_banner = '" + lsCveBanner + "'";
			conexBD.ejecutaSQL(sSql);

			conexBD.terminaTransaccion(true);

			System.out.print("\n Metodo 'modificaFechaBanner()' ejecutado con exito. \n");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo modificaFechaBanner. \n");
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta())
				conexBD.cierraConexionDB();
		}
	}

	/*************************************************************************************
	*
	*  GUARDA LOS MENSJES EN LA PATALLA DE MISMO NOMBRE
	*
	*************************************************************************************/

    public void guardaMensaje(String lsNafinElec, String lsNumEPO, String lsCategoria, String lsTitulo, String ldDesde,
                              String ldHasta, String lsContenido, String lsNombArchivo) throws NafinException {
		System.out.println("Entrando a metodo 'guardaMensaje() del EJB de SERVICIOS'");
		boolean lbSinError = true;
		AccesoDB			con	= new AccesoDB();
		PreparedStatement	ps	= null;
		ResultSet			rs  = null;
		int 				icMensaje = 0;
		try{
			con.conexionDB();

			String SQLmensaje = "";
            SQLmensaje = " select seq_com_mensaje_ic_mensaje.NEXTVAL from dual";
			ps = con.queryPrecompilado(SQLmensaje);
			rs = ps.executeQuery();
			if(rs.next())
				icMensaje = rs.getInt(1);
			ps.close();

			SQLmensaje  =
				"INSERT INTO com_mensaje (ic_mensaje, ic_nafin_electronico, ic_epo, ic_tipo_mensaje, " +
                "cg_titulo, df_ini_mensaje, df_fin_mensaje, tt_contenido, ci_imagen) " + "VALUES (?,?,?,?," +
                "?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY')," + "?,?)";
			ps = con.queryPrecompilado(SQLmensaje);
			ps.setInt(1,icMensaje);
			ps.setString(2,lsNafinElec);
			ps.setString(3,lsNumEPO);
			ps.setString(4,lsCategoria);
			ps.setString(5,lsTitulo);
			ps.setString(6,ldDesde);
			ps.setString(7,ldHasta);
			ps.setString(8,lsContenido.replace(',',' '));
			ps.setString(9,lsNombArchivo);
			ps.execute();
			ps.close();
			con.terminaTransaccion(true);
			System.out.print("\n Metodo 'guardaMensaje()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo guardaMensaje. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbSinError);
				con.cierraConexionDB();
			}
		}
	}

    public int guardaMensajeIni(String tipoAfiliado, String[] cadenaDestino, String[] ifDestino, String txtFechaDesde,
                                String txtFechaHasta, String txtTitulo, String txtContenido, String txtArchivo,
                                String tipo_info) throws NafinException {
		System.out.println("ServiciosBean.guardaMensajeIni (E)");
		AccesoDB 			con	= new AccesoDB();
		PreparedStatement	ps 	= null;
		ResultSet			rs	= null;
		String				qrySentencia	= "";
		int 				icMensajeIni	= 0;
		int i = 0;
		boolean	resultado = true;
		try{
			con.conexionDB();
            qrySentencia = " select nvl(max(ic_mensaje_ini),0)+1" + " from com_mensaje_ini";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next())
				icMensajeIni = rs.getInt(1);
			ps.close();
			qrySentencia =
					" insert into com_mensaje_ini"+
					" (ic_mensaje_ini,cg_titulo,df_ini_mensaje,df_fin_mensaje,cg_contenido,cg_imagen,cg_tipo_afiliado,cg_tipo_mensaje)"+
                " values(?,?,to_date(?,'dd/mm/yyyy')" + " ,to_date(?,'dd/mm/yyyy'),?,?,?,?)";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,icMensajeIni);
			ps.setString(2,txtTitulo);
			ps.setString(3,txtFechaDesde);
			ps.setString(4,txtFechaHasta);
			ps.setString(5,txtContenido);
			ps.setString(6,txtArchivo);
			ps.setString(7,tipoAfiliado);
			ps.setString(8,tipo_info);
			ps.execute();
			ps.close();
			if(("E".equals(tipoAfiliado)||"P".equals(tipoAfiliado))&&cadenaDestino!=null){
				qrySentencia =
                    "  INSERT INTO comrel_epo_mensaje_ini" + "  (ic_epo,ic_mensaje_ini)" +
				"  (SELECT ?,? FROM dual WHERE NOT EXISTS(SELECT 1 FROM comrel_epo_mensaje_ini WHERE ic_epo = ? AND ic_mensaje_ini = ? ))"  ;

				ps = con.queryPrecompilado(qrySentencia);
				for(i=0;i<cadenaDestino.length;i++){
					ps.setString(1,cadenaDestino[i]);
					ps.setInt(2,icMensajeIni);
					ps.setString(3,cadenaDestino[i]);
					ps.setInt(4,icMensajeIni);
					ps.execute();
				}
				ps.close();
			}
			if(("I".equals(tipoAfiliado)||"P".equals(tipoAfiliado))&&ifDestino!=null){
				if("I".equals(tipoAfiliado)){
					qrySentencia =
                        " insert into comrel_if_mensaje_ini" + " (ic_mensaje_ini,ic_if)" +
						"  (SELECT ?,? FROM dual WHERE NOT EXISTS(SELECT 1 FROM comrel_if_mensaje_ini WHERE ic_mensaje_ini = ? AND ic_if = ? ))"  ;
					ps = con.queryPrecompilado(qrySentencia);
					for(i=0;i<ifDestino.length;i++){
						ps.setInt(1,icMensajeIni);
						ps.setString(2,ifDestino[i]);
						ps.setInt(3,icMensajeIni);
						ps.setString(4,ifDestino[i]);
						ps.execute();
					}
					ps.close();
				}else if("P".equals(tipoAfiliado)){
					qrySentencia =
                        "  INSERT INTO comrel_epo_mensaje_ini" + "  (ic_mensaje_ini,ic_epo)" +
                        "  (SELECT ?,ic_epo FROM comrel_if_epo ie" + "  	WHERE ie.ic_if = ? " +
						"    AND ie.cs_vobo_nafin = 'S'"   +
						"    AND NOT EXISTS (SELECT 1 FROM comrel_epo_mensaje_ini WHERE ic_mensaje_ini = ? AND ic_epo = ie.ic_epo)"   +
						"  )"  ;
					ps = con.queryPrecompilado(qrySentencia);
					for(i=0;i<ifDestino.length;i++){
						ps.setInt(1,icMensajeIni);
						ps.setString(2,ifDestino[i]);
						ps.setInt(3,icMensajeIni);
						ps.execute();
					}
					ps.close();
				}
			}

		}catch(Exception e){
			resultado = false;
			System.out.println("ServiciosBean.guardaMensajeIni Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			System.out.println("ServiciosBean.guardaMensajeIni (S)");
		}
		return icMensajeIni;
	}


    public void modificacionMensajeIni(String icMensajeIni, String strDesde, String strHasta, String strTitulo,
                                       String strContenido) throws NafinException {
		System.out.println("ServiciosBean.modificacionMensajeIni (E)");
		AccesoDB 			con	= new AccesoDB();
		PreparedStatement	ps 	= null;
		String				qrySentencia	= "";
		boolean				resultado = true;
		try{
			con.conexionDB();
			qrySentencia =
                "  update com_mensaje_ini " + " set df_ini_mensaje = to_date(?,'dd/mm/yyyy')" +
                " ,df_fin_mensaje = to_date(?,'dd/mm/yyyy')" + " ,cg_titulo = ?" + " ,cg_contenido = ?" +
				" where ic_mensaje_ini = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,strDesde);
			ps.setString(2,strHasta);
			ps.setString(3,strTitulo);
			ps.setString(4,strContenido);
			ps.setString(5,icMensajeIni);
			ps.execute();
			ps.close();
		}catch(Exception e){
			resultado = false;
			System.out.println("ServiciosBean.modificacionMensajeIni Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			System.out.println("ServiciosBean.modificacionMensajeIni (S)");
		}
	}


	public void bajaMensajeIni(String icMensajeIni) throws NafinException{
		System.out.println("ServiciosBean.bajaMensajeIni (E)");
		AccesoDB 			con	= new AccesoDB();
		PreparedStatement	ps 	= null;
		String				qrySentencia	= "";
		boolean				resultado		= true;
		try{
			con.conexionDB();
      /*FODEA 0008 - 2009 ACF (I)*/
            qrySentencia = "delete com_estadistica_mensaje_ini" + " where ic_mensaje_ini = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,icMensajeIni);
			ps.execute();
			ps.close();
      /*FODEA 0008 - 2009 ACF (I)*/
            qrySentencia = "delete comrel_epo_mensaje_ini" + " where ic_mensaje_ini = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,icMensajeIni);
			ps.execute();
			ps.close();
            qrySentencia = "delete comrel_if_mensaje_ini" + " where ic_mensaje_ini = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,icMensajeIni);
			ps.execute();
			ps.close();
            qrySentencia = "delete com_mensaje_ini" + " where ic_mensaje_ini = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,icMensajeIni);
			ps.execute();
			ps.close();
		}catch(Exception e){
			resultado = false;
			System.out.println("ServiciosBean.bajaMensajeIni Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			System.out.println("ServiciosBean.bajaMensajeIni (S)");
		}
	}

	public Vector getMensajeIni(String icMensajeIni) throws NafinException{
		System.out.println("ServiciosBean.bajaMensajeIni (E)");
		AccesoDB 			con	= new AccesoDB();
		PreparedStatement	ps 	= null;
		ResultSet			rs 	= null;
		String				qrySentencia	= "";
		Vector				vecColumnas 	= new Vector();
		try{
			con.conexionDB();
			qrySentencia =
                " SELECT mi.ic_mensaje_ini" + "   ,to_char(mi.df_ini_mensaje,'dd/mm/yyyy') as fecha_ini" +
                "   ,to_char(mi.df_fin_mensaje,'dd/mm/yyyy') as fecha_fin" + "   ,mi.cg_titulo" +
                "   ,mi.cg_contenido   " + "  FROM com_mensaje_ini mi" + "  WHERE mi.ic_mensaje_ini = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,icMensajeIni);
			rs = ps.executeQuery();
			if(rs.next()){
				vecColumnas.add(0,(rs.getString("cg_titulo")==null)?"":rs.getString("cg_titulo"));
				vecColumnas.add(1,(rs.getString("fecha_ini")==null)?"":rs.getString("fecha_ini"));
				vecColumnas.add(2,(rs.getString("fecha_fin")==null)?"":rs.getString("fecha_fin"));
				vecColumnas.add(3,(rs.getString("cg_contenido")==null)?"":rs.getString("cg_contenido"));
			}
			ps.close();
		}catch(Exception e){
			System.out.println("ServiciosBean.bajaMensajeIni Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("ServiciosBean.bajaMensajeIni (S)");
		}
		return vecColumnas;
	}


	/*************************************************************************************
	*
	*  BORRA LOS CURSOS EN LA PATALLA DE MISMO NOMBRE
	*
	*************************************************************************************/
	public void borraMensaje(String lsNumEPO, String lsTitulo,String mensajesEpos)	throws NafinException {
		System.out.println("Entrando a metodo 'borraMensaje() del EJB de SERVICIOS'");
		AccesoDB con = new AccesoDB();
		boolean lbSinError = true;
		PreparedStatement ps = null;
		try{
			con.conexionDB();
			String SqlMensaje = "";
			String SqlRelMensaje = "";

			if("S".equals(mensajesEpos)){
				SqlRelMensaje =
                    " DELETE comrel_mensaje_usuario mu" + " WHERE EXISTS (" + " 	  SELECT 1 FROM com_mensaje m" +
                    " 	  WHERE m.cg_titulo =?" + " 	  AND m.ic_nafin_electronico = ?" +
					" 	  AND m.ic_mensaje = mu.ic_mensaje"   +
                    " 	  AND m.ic_nafin_electronico = mu.ic_nafin_electronico" + " )";
				ps = con.queryPrecompilado(SqlRelMensaje);
				ps.setString(1,lsTitulo);
				ps.setString(2,lsNumEPO);
				ps.execute();
				ps.close();
                SqlMensaje = "DELETE FROM com_mensaje " + " WHERE cg_titulo =?" + " AND ic_nafin_electronico = ?";
				ps = con.queryPrecompilado(SqlMensaje);
				ps.setString(1,lsTitulo);
				ps.setString(2,lsNumEPO);
				ps.execute();
				ps.close();
			}else{

				SqlRelMensaje =
                    " DELETE comrel_mensaje_usuario mu" + " WHERE EXISTS (" + " 	  SELECT 1 FROM com_mensaje m" +
                    " 	  WHERE m.cg_titulo =?" + " 	  AND m.ic_epo = ?" + " 	  AND m.ic_mensaje = mu.ic_mensaje" +
                    " 	  AND m.ic_nafin_electronico = mu.ic_nafin_electronico" + " )";
				ps = con.queryPrecompilado(SqlRelMensaje);
				ps.setString(1,lsTitulo);
				ps.setString(2,lsNumEPO);
				ps.execute();
				ps.close();
                SqlMensaje = "DELETE FROM com_mensaje " + " WHERE cg_titulo =?" + " AND ic_epo = ?";

				ps = con.queryPrecompilado(SqlMensaje);
				ps.setString(1,lsTitulo);
				ps.setString(2,lsNumEPO);
				ps.execute();
				ps.close();
			}
			//System.out.print("\n\t SqlMensaje: " + SqlMensaje + "\n\n");
			System.out.print("\n Metodo 'borraMensaje()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo borraMensaje. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbSinError);
				con.cierraConexionDB();
			}
		}
	}

	/*************************************************************************************
	*
	*  MODIFICA LOS MENSJES EN LA PATALLA DE MISMO NOMBRE
	*
	*************************************************************************************/
    public void modificaMensaje(String lsNumEPO, String lsTitulo1, String lsTitulo2, String ldDesde, String ldHasta,
                                String lsContenido, String mensajesEpos) throws NafinException {
		System.out.println("Entrando a metodo 'modificaMensaje() del EJB de SERVICIOS'");
		boolean lbSinError = true;
		AccesoDB conexBD = new AccesoDB();
		try{
			conexBD.conexionDB();

			String SQLmensaje = "";
			SQLmensaje  =
                "UPDATE com_mensaje " + " SET cg_titulo='" + lsTitulo1 + "'" + " ,df_ini_mensaje=TO_DATE('" + ldDesde +
                "','DD/MM/YYYY')" + " ,df_fin_mensaje=TO_DATE('" + ldHasta + "','DD/MM/YYYY')" + " ,TT_CONTENIDO='" +
                lsContenido + "'" + " WHERE cg_titulo='" + lsTitulo2 + "'";
			if("S".equals(mensajesEpos))
                SQLmensaje += " and ic_nafin_electronico = " + lsNumEPO;
			else
                SQLmensaje += " and ic_epo = " + lsNumEPO;


			System.out.print("\n\t SQLmensaje: " + SQLmensaje + "\n\n");

			conexBD.ejecutaSQL(SQLmensaje);
			conexBD.terminaTransaccion(true);
			System.out.print("\n Metodo 'modificaMensaje()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo modificaMensaje. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}

	/*************************************************************************************
	*
	*  GUARDA LOS CURSOS EN LA PATALLA DE MISMO NOMBRE
	*
	*************************************************************************************/
	public void guardaCurso(String lsCve_Curso,    String lsUsuario,  String lsCadena,  String lsTitulo,
                            String lsNombArchivo, String ldInicial, String ldFinal,
                            String lsContenido) throws NafinException {
		System.out.println("Entrando a metodo 'guardaCurso() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlCurso = "";
            SqlCurso =
                "INSERT INTO com_curso (ic_curso, ic_nafin_electronico, ic_epo,cg_titulo, ci_imagen, df_ini_pub, " +
                "df_fin_pub, tt_contenido) VALUES " + "(" + lsCve_Curso + ", '" + lsUsuario + "'," + lsCadena + ",'" +
                lsTitulo + "','" + lsNombArchivo + "',TO_DATE('" + ldInicial + "','DD/MM/YYYY'),TO_DATE('" + ldFinal +
                "','DD/MM/YYYY'),'" + lsContenido.replace('\'', ' ') + "')";

			System.out.print("\n\t SqlCurso: " + SqlCurso + "\n\n");

			conexBD.ejecutaSQL(SqlCurso);
			System.out.print("\n Metodo 'guardaCurso()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo guardaCurso. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}

	/*************************************************************************************
	*
	*  BORRA LOS CURSOS EN LA PATALLA DE MISMO NOMBRE
	*
	*************************************************************************************/
	public void borraCurso(String lsCve_Curso, String lsCadena) throws NafinException {
		System.out.println("Entrando a metodo 'borraCurso() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlCurso = "";
			SqlCurso  = "DELETE FROM com_curso WHERE ic_epo = " +lsCadena+ " AND ic_curso = "+ lsCve_Curso;

			System.out.print("\n\t SqlCurso: " + SqlCurso + "\n\n");

			conexBD.ejecutaSQL(SqlCurso);
			System.out.print("\n Metodo 'borraCurso()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo borraCurso. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}

	/*************************************************************************************
	*  MODIFICA LOS CURSOS EN LA PATALLA DE MISMO NOMBRE
	*************************************************************************************/
	public void modificaCurso(String lsCve_Curso,    String lsCadena,  String lsTitulo, String lsNombArchivo,
		String ldInicial,  String ldFinal,   String lsContenido)  throws NafinException {
		System.out.println("Entrando a metodo 'modificaCurso() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlCurso = "";
            SqlCurso =
                "UPDATE com_curso SET cg_titulo = '" + lsTitulo + "', ci_imagen = '" + lsNombArchivo +
				"', df_ini_pub = TO_DATE('"+ldInicial+"','DD/MM/YYYY'), df_fin_pub = TO_DATE('"+ldFinal+
                "','DD/MM/YYYY'), tt_contenido = '" + lsContenido.replace('\'', ' ') + "' where ic_epo = " + lsCadena +
                " and ic_curso = " + lsCve_Curso;

			System.out.print("\n\t SqlCurso: " + SqlCurso + "\n\n");

			conexBD.ejecutaSQL(SqlCurso);
			System.out.print("\n Metodo 'modificaCurso()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo modificaCurso. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if (conexBD.hayConexionAbierta())	{
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}

	/*************************************************************************************
	*  GUARDA SITIO DE INTERES
	*************************************************************************************/
	public void guardaSitioInteres(String lsCve_Sitio, String lsUsuario,  String lsCadena,  String lsTitulo,
                                   String lsSitio, String ldInicial, String ldFinal,
                                   String lsContenido) throws NafinException {
		System.out.println("Entrando a metodo 'guardaSitioInteres() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlSitio = "";
            SqlSitio =
                "INSERT INTO com_sitio_interes (ic_sitio_interes, ic_nafin_electronico, ic_epo, cg_titulo, " +
                "cg_liga, df_ini_sitio_int, df_fin_sitio_int, tt_contenido) VALUES (" + lsCve_Sitio + ",'" + lsUsuario +
                "'," + lsCadena + ",'" + lsTitulo + "','" + lsSitio + "',TO_DATE('" + ldInicial +
				"','DD/MM/YYYY'),TO_DATE('"+ldFinal+"','DD/MM/YYYY'),'"+lsContenido.replace('\'',' ')+"')";

			System.out.print("\n\t SqlCurso: " + SqlSitio + "\n\n");

			conexBD.ejecutaSQL(SqlSitio);
			System.out.print("\n Metodo 'guardaSitioInteres()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo guardaSitioInteres. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}


	/*************************************************************************************
	*
	*  BORRA LOS SITIOS DE INTERES DE UNA CIERTA CADENA
	*
	*************************************************************************************/
	public void borraSitioInteres(String lsCve_Sitio, String lsCadena) throws NafinException {
		System.out.println("Entrando a metodo 'borraSitioInteres() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlSitios = "";
            SqlSitios =
                "DELETE FROM com_sitio_interes WHERE ic_epo = " + lsCadena + " and ic_sitio_interes = " + lsCve_Sitio;

			System.out.print("\n\t SqlSitios: " + SqlSitios + "\n\n");

			conexBD.ejecutaSQL(SqlSitios);
			System.out.print("\n Metodo 'borraSitioInteres()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo borraSitioInteres. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}

		/*************************************************************************************
		*
		*  MODIFICA LOS SITIOS DE INTERES EN LA PATALLA DE MISMO NOMBRE
		*
		*************************************************************************************/

		public void modificaSitioInteres(String lsCve_Sitio,    String lsCadena,  String lsTitulo, String lsSitio,
									String ldInicial,  String ldFinal,   String lsContenido)  throws NafinException {
			System.out.println("ServiciosEJB::modificaSitioInteres(E)");
			AccesoDB conexBD = new AccesoDB();
			boolean lbSinError = true;
			try{
				conexBD.conexionDB();
				String SqlSitio = "";
            SqlSitio =
                "update com_sitio_interes set cg_titulo = '" + lsTitulo + "', cg_liga = '" + lsSitio +
					"', df_ini_sitio_int = TO_DATE('"+ldInicial+"','DD/MM/YYYY'), df_fin_sitio_int = TO_DATE('"+
					ldFinal+"','DD/MM/YYYY'), tt_contenido = '"+lsContenido.replace('\'',' ')+"' where ic_epo = "+
					lsCadena+" and ic_sitio_interes = "+lsCve_Sitio;

				conexBD.ejecutaSQL(SqlSitio);
        } catch (SQLException errSql) {
				System.out.print("\n Error: " + errSql + "\n");
				lbSinError = false;
				throw new NafinException("SIST0001");
        } catch (Exception err) {
				System.out.print("\nSe ha producido un error en el metodo modificaSitioInteres. \n");
				lbSinError = false;
				throw new NafinException("SIST0001");
        } finally {
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
				System.out.println("ServiciosEJB::modificaSitioInteres(S)");
			}
		}


       /*************************************************************************************
       *
       *  DEVUELVEL EL NUMERO DE SITIOS DE INTERES KE ESTAN GRABADOS
       *
       *************************************************************************************/

		public String obtenNumSitiosInteres()	throws NafinException	{
			String sqlNumSitios = "";
			int liNumSitios = 0;
			AccesoDB conexBD = new AccesoDB();
			try {
				sqlNumSitios = "SELECT MAX(ic_sitio_interes)+1 n_stio FROM com_sitio_interes";
				conexBD.conexionDB();
				ResultSet rsNunSitios = conexBD.queryDB( sqlNumSitios );

			    rsNunSitios.next();
				liNumSitios = rsNunSitios.getInt(1);
            if (liNumSitios == 0)
                liNumSitios = 1;
				return String.valueOf( liNumSitios );
        } catch (Exception err) {
				System.out.print("\nSe ha producido un error en el metodo obtenNumSitiosInteres. \n");
				throw new NafinException("SIST0001");
			}
		}


	/*************************************************************************************
	*
	*  GUARDA VIDEO CONFERENCIAS
	*
	*************************************************************************************/
    public void guardaVideoConferencia(String lsCve_Video, String lsUsuario, String lsCadena, String lsTitulo,
                                       String lsNombArchivo, String ldDesde, String ldHasta,
                                       String lsContenido) throws NafinException {
		System.out.println("Entrando a metodo 'guardaVideoConferencia() del EJB de SERVICIOS'");
		boolean lbSinError = true;
		AccesoDB conexBD = new AccesoDB();
		try{
			conexBD.conexionDB();

			String SQLVideo = "";
            SQLVideo =
                "INSERT INTO com_videoconferencia (ic_videoconferencia, ic_nafin_electronico , ic_epo, cg_titulo, " +
                "cg_archivo, df_ini_video, df_fin_video, tt_descripcion) VALUES (" + lsCve_Video + ",'" + lsUsuario +
                "'," + lsCadena + ",'" + lsTitulo + "','" + lsNombArchivo + "',TO_DATE('" + ldDesde +
                "','DD/MM/YYYY'),TO_DATE('" + ldHasta + "','DD/MM/YYYY'),'" + lsContenido.replace('\'', ' ') + "')";

			System.out.print("\n\t SQLVideo: " + SQLVideo + "\n\n");

			conexBD.ejecutaSQL(SQLVideo);
			conexBD.terminaTransaccion(true);
			System.out.print("\n Metodo 'guardaVideoConferencia()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo guardaVideoConferencia. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}


	/*************************************************************************************
	*
	*  BORRA LAS VIDEO CONFERENCIAS
	*
	*************************************************************************************/
	public void borraVideoConferencia(String lsCve_Video, String lsCadena) throws NafinException {
		System.out.println("Entrando a metodo 'borraVideoConferencia() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlVideo = "";
            SqlVideo =
                "DELETE FROM com_videoconferencia WHERE ic_epo = " + lsCadena + " AND ic_videoconferencia = " +
                lsCve_Video;

			System.out.print("\n\t SqlVideo: " + SqlVideo + "\n\n");

			conexBD.ejecutaSQL(SqlVideo);
			System.out.print("\n Metodo 'borraVideoConferencia()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo borraVideoConferencia. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}


	/*************************************************************************************
	*
	*  MODIFICA LAS VIDEOCONFERENCIAS
	*
	*************************************************************************************/
    public void modificaVideoConferencia(String lsCve_Video, String lsCadena, String lsTitulo, String lsNombArchivo,
                                         String ldInicial, String ldFinal, String lsContenido) throws NafinException {
		System.out.println("Entrando a metodo 'modificaVideoConferencia() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlVideo = "";
            SqlVideo =
                "UPDATE com_videoconferencia SET cg_titulo = '" + lsTitulo + "', cg_archivo = '" + lsNombArchivo +
                "', df_ini_video = TO_DATE('" + ldInicial + "','DD/MM/YYYY'), df_fin_video = TO_DATE('" + ldFinal +
                "','DD/MM/YYYY'), tt_descripcion = '" + lsContenido.replace('\'', ' ') + "' where ic_epo = " +
                lsCadena + " and ic_videoconferencia = " + lsCve_Video;

			System.out.print("\n\t SqlVideo: " + SqlVideo + "\n\n");

			conexBD.ejecutaSQL(SqlVideo);
			System.out.print("\n Metodo 'modificaVideoConferencia()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo modificaVideoConferencia. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}

	/*************************************************************************************
	*
	*  GUARDA ENCUESTA
	*
	*************************************************************************************/
    public String guardaEncuesta(String lsTitulo, String ldFechaIni, String ldFechaFin, String lsEPO,
                                 String lsAfiliados[], String lsInstrucciones, String lsNumPreguntas, String lsAfiliado,
                                 String obligatoria, String dias) throws NafinException {
		System.out.println("ServiciosEJB::guardaEncuesta(E)");

		boolean				lbSinError 		= true;
		AccesoDB 			conexBD 		= new AccesoDB();
		PreparedStatement	ps;
		ResultSet			rs;
		String				SQLEncuesta;
		String				ic_encuesta = "";
		String 				sqlTipoAfil = "";
		String 				paramTipoAfil = "";

		try{
			conexBD.conexionDB();

            SQLEncuesta = " SELECT seq_com_encuesta.nextval " + "   FROM dual";
			ps = conexBD.queryPrecompilado(SQLEncuesta);
			rs = ps.executeQuery();
			if(rs.next())
				ic_encuesta = (rs.getString(1)==null)?"":rs.getString(1);
			rs.close();
			ps.close();


			for(int i=0;i<lsAfiliados.length;i++) {
				if(i==0){
					SQLEncuesta =
                        " INSERT INTO com_encuesta" + "             (ic_encuesta, cg_titulo," +
						"              df_ini_encuesta, df_fin_encuesta, "   +
						" 			 tt_instrucciones, ig_num_preguntas, CS_OBLIGATORIO, IC_DIASPRE)"   +
                        "      VALUES (?, ?,  " + " 	 		 TO_DATE (?, 'DD/MM/YYYY'), TO_DATE (?, 'DD/MM/YYYY'), " +
						" 			 ?, ?,?, ?)"  ;

					ps = conexBD.queryPrecompilado(SQLEncuesta);
					ps.setLong(1,Long.parseLong(ic_encuesta));
					ps.setString(2, lsTitulo);
					ps.setString(3, ldFechaIni);
					ps.setString(4, ldFechaFin);
					ps.setString(5, lsInstrucciones.replace('\'',' '));
					ps.setInt(6, Integer.parseInt(lsNumPreguntas));
					ps.setString(7, obligatoria);
					ps.setInt(8, Integer.parseInt(dias));
					ps.executeUpdate();
					ps.clearParameters();
					ps.close();
				}


                if ("P".equals(lsAfiliado)) {
                    sqlTipoAfil = " ic_pyme, ic_epo";
                    paramTipoAfil = " ?, ?";
                }
                if ("I".equals(lsAfiliado)) {
                    sqlTipoAfil = " ic_if";
                    paramTipoAfil = " ?";
                }
                if ("E".equals(lsAfiliado)) {
                    sqlTipoAfil = " ic_epo";
                    paramTipoAfil = " ?";
                }

				SQLEncuesta =
                    " INSERT INTO comrel_encuesta_afiliado" + "             (ic_encuesta_afil, ic_encuesta, " +
                    sqlTipoAfil + ",cg_afiliado	)" + "      VALUES (seq_comrel_encuesta_afiliado.nextval, ?," +
                    paramTipoAfil + " ,?)";

					int p=0;
					ps = conexBD.queryPrecompilado(SQLEncuesta);
					ps.setLong(++p, Long.parseLong(ic_encuesta));
					if("P".equals(lsAfiliado)){
						ps.setLong(++p, Long.parseLong(lsAfiliados[i]));
						ps.setLong(++p, Long.parseLong(lsEPO));
					}else if("I".equals(lsAfiliado) || "E".equals(lsAfiliado)){
						ps.setLong(++p, Long.parseLong(lsAfiliados[i]));
					}
					ps.setString(++p, lsAfiliado);
					ps.executeUpdate();
					ps.clearParameters();
					ps.close();
			}//for(int i=0;i<lsAfiliados.length;i++)
			lbSinError = true;
        } catch (SQLException errSql) {
			lbSinError = false;
			System.out.println("ServiciosEJB::guardaEncuesta(SQLException) "+errSql);
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			lbSinError = false;
			System.out.println("ServiciosEJB::guardaEncuesta(Exception) "+err);
			throw new NafinException("SIST0001");
        } finally {
			conexBD.terminaTransaccion(lbSinError);
			if(conexBD.hayConexionAbierta()){
				conexBD.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::guardaEncuesta(S)");
		}
		return ic_encuesta;
	}


	/*************************************************************************************
	*
	*  GUARDA PREGUNTA
	*
	*************************************************************************************/

    public void guardaPregunta(String lsCveEncuesta, String lsEpo, String lsAfiliados[], List lsPreguntas,
                               List lsTipoRespuesta, List lsOpciones, List cverubro,
                               List cvenumeracion) throws NafinException {
		System.out.println("ServiciosEJB::guardaPregunta(E)");

		boolean				lbSinError 		= true;
		AccesoDB 			conexBD 		= new AccesoDB();
		PreparedStatement	ps;
		String				SQLPregunta;
		try{
			conexBD.conexionDB();


				for(int j=0;j<lsPreguntas.size();j++) {

				String ic_tipo_respuesta = (String)lsTipoRespuesta.get(j);
				String cg_pregunta = (String)lsPreguntas.get(j);
				String ig_num_opciones = (String)lsOpciones.get(j);
				String rubro = (String)cverubro.get(j);
				String numeracion =  (String)cvenumeracion.get(j);

				if(ig_num_opciones.equals("")){
					ig_num_opciones  = "0";
				}

				if(cg_pregunta.length()>300){
					cg_pregunta = cg_pregunta.substring(0,300);
				}


					SQLPregunta =
						" INSERT INTO com_pregunta"   +
						"             (ic_pregunta, ic_encuesta, ig_num_pregunta, ic_tipo_respuesta, cg_pregunta, ig_num_opciones, IC_RUBRO, IC_NOPREGUNTA)"   +
						"      VALUES (seq_com_pregunta.nextval,?, ?, ?, ?, ?, ?, ?)"  ;

					ps = conexBD.queryPrecompilado(SQLPregunta);
					ps.setInt(1, Integer.parseInt(lsCveEncuesta));
					ps.setInt(2, j+1);
					ps.setString(3, ic_tipo_respuesta);
					ps.setString(4, cg_pregunta);
					ps.setInt(5, Integer.parseInt(ig_num_opciones));
					ps.setInt(6, Integer.parseInt(rubro));
					ps.setInt(7, Integer.parseInt(numeracion));

					ps.executeUpdate();
					ps.clearParameters();
					ps.close();

				}//for(int j=0;j<lsPreguntas.length;j++)


        } catch (SQLException errSql) {
			lbSinError = false;
			System.out.println("ServiciosEJB::guardaPregunta(SQLException)");
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.println("ServiciosEJB::guardaPregunta(Exception)");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			conexBD.terminaTransaccion(lbSinError);
			if(conexBD.hayConexionAbierta()){
				conexBD.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::guardaPregunta(S)");
		}
	}


	/*************************************************************************************
	*
	*  GUARDA PREGUNTA
	*
	*************************************************************************************/
	public void guardaRespuesta(String lsCve_Encuesta, String lsCve_Pregunta, String lsTipoRespuesta,
						String lsCve_Respuesta,  String lsUsuario,       String lsCadena,
                                String lsContenido) throws NafinException {
		System.out.println("ServiciosEJB::guardaRespuesta(E)");
		boolean lbSinError = true;
		AccesoDB conexBD = new AccesoDB();
		try{
			conexBD.conexionDB();

			String SQLRespuesta = "";
            SQLRespuesta =
                "INSERT INTO com_respuesta (ic_respuesta, ic_encuesta, ic_pregunta, ic_tipo_respuesta,  " +
                " cg_respuesta) VALUES (seq_com_respuesta.nextval, " + lsCve_Encuesta + "," + lsCve_Pregunta + "," +
                lsTipoRespuesta + ",'" + lsContenido.replace('\'', ' ') + "')";

//System.out.print("\n\t SQLRespuesta: " + SQLRespuesta + "\n\n");

			conexBD.ejecutaSQL(SQLRespuesta);
			conexBD.terminaTransaccion(true);
			System.out.print("\n Metodo 'guardaRespuesta()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo guardaRespuesta. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::guardaRespuesta(S)");
		}
	}


	/*************************************************************************************
	*
	*  BORRA RESPUESTA
	*
	*************************************************************************************/
	public void borraRespuesta(String lsCve_Encuesta, String lsCadena) throws NafinException {
		System.out.println("Entrando a metodo 'borraRespuesta() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlRespuesta = "";
            SqlRespuesta =
                "DELETE FROM com_contesta_respuesta WHERE ic_epo = " + lsCadena + " AND ic_encuesta = " +
                lsCve_Encuesta;
			System.out.print("\n\t SqlRespuesta: " + SqlRespuesta + "\n\n");
			conexBD.ejecutaSQL(SqlRespuesta);

            SqlRespuesta =
                "DELETE FROM com_respuesta WHERE ic_epo = " + lsCadena + " AND ic_encuesta = " + lsCve_Encuesta;
			System.out.print("\n\t SqlRespuesta: " + SqlRespuesta + "\n\n");
			conexBD.ejecutaSQL(SqlRespuesta);

			System.out.print("\n Metodo 'borraRespuesta()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo borraRespuesta. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}

	/*************************************************************************************
	*
	*  BORRA PREGUNTA
	*
	*************************************************************************************/
	public void borraPregunta(String lsCve_Encuesta, String lsCadena) throws NafinException {
		System.out.println("Entrando a metodo 'borraPregunta() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlPregunta = "";

            SqlPregunta =
                "DELETE FROM com_pregunta WHERE ic_epo = " + lsCadena + " and ic_encuesta = " + lsCve_Encuesta;

			System.out.print("\n\t SqlRespuesta: " + SqlPregunta + "\n\n");
			conexBD.ejecutaSQL(SqlPregunta);

			System.out.print("\n Metodo 'borraPregunta()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo borraPregunta. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}

	/*************************************************************************************
	*
	*  BORRA ENCUESTA
	*
	*************************************************************************************/
	public void borraEncuesta(String lsCve_Encuesta, String lsCadena) throws NafinException {
		System.out.println("Entrando a metodo 'borraEncuesta() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		PreparedStatement ps = null;
		String SqlEncuesta = "";
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();


            SqlEncuesta =
                "DELETE com_contesta_respuesta " + "WHERE ic_respuesta IN ( " + "            SELECT cr.ic_respuesta " +
								"              FROM com_encuesta ce, com_respuesta cr " +
                "             WHERE ce.ic_encuesta = cr.ic_encuesta " + "             AND ce.ic_encuesta = ?) ";

			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(lsCve_Encuesta));
			ps.executeUpdate();
			ps.close();

			SqlEncuesta = "DELETE FROM com_respuesta WHERE ic_encuesta = ? ";
			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(lsCve_Encuesta));
			ps.executeUpdate();
			ps.close();

			SqlEncuesta = "DELETE FROM com_pregunta WHERE ic_encuesta = ? ";
			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(lsCve_Encuesta));
			ps.executeUpdate();
			ps.close();

			SqlEncuesta = "DELETE FROM comrel_encuesta_afiliado WHERE ic_encuesta = ? ";
			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(lsCve_Encuesta));
			ps.executeUpdate();
			ps.close();

			SqlEncuesta = "DELETE FROM com_encuesta WHERE ic_encuesta = ? ";
			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(lsCve_Encuesta));
			ps.executeUpdate();
			ps.close();

			System.out.print("\n Metodo 'borraEncuesta()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo borraEncuesta. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}


	/*************************************************************************************
	*
	*  MODIFICA LAS ENCUESTAS
	*
	*************************************************************************************/
    public void modificaEncuesta(String lsCve_Encuesta, String lsCadena, String lsTitulo, String ldDesde,
                                 String ldHasta, String lsContenido, String obligatoria,
                                 String dias) throws NafinException {
		System.out.println("Entrando a metodo 'modificaEncuesta() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();
			String SqlEncuesta = "";
            SqlEncuesta =
                " UPDATE com_encuesta  " + " SET cg_titulo = '" + lsTitulo + "'," +
                "	df_ini_encuesta = TO_DATE('" + ldDesde + "','DD/MM/YYYY'),  " + " df_fin_encuesta = TO_DATE('" +
                ldHasta + "','DD/MM/YYYY'), " + " tt_instrucciones = '" + lsContenido.replace('\'', ' ') + "' ," +
                "  CS_OBLIGATORIO = '" + obligatoria + "', " + "  IC_DIASPRE = " + dias + "  where ic_epo = " +
                lsCadena + " and ic_encuesta = " + lsCve_Encuesta;

			System.out.print("\n\t SqlEncuesta: " + SqlEncuesta + "\n\n");

			conexBD.ejecutaSQL(SqlEncuesta);
			System.out.print("\n Metodo 'modificaEncuesta()' ejecutado con exito. \n");
        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo modificaEncuesta. \n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}


/**
 * Almacena la respuesta a la Campa�a que se le presenta al proveedor.
 * @param sts Estatus S Si me interesa, N No me interesa
 * @param msgini ????
 * @param icepo Clave de la epo a la que pertenece la pyme
 * @param icpyme Clave de la pyme
 * @return Cadena con codigo de ejecuci�n: 0 Ok, 1 Error
 *
 *
 */
public String guardaEstatusCamp(String sts,String msgini,String icepo,String icpyme) throws NafinException{

		AccesoDB 			con	= new AccesoDB();
		String				qrySentencia	= "";
		boolean bOk = true;

		try{
			con.conexionDB();
            qrySentencia =
                "insert into com_estadistica_mensaje_ini values(" + msgini + "," + icepo + "," + icpyme + ",'" + sts +
                "',null,sysdate)";
			System.out.println("Query para insertar::: "+qrySentencia);
			con.ejecutaSQL(qrySentencia);
			return "0";
		}catch(Exception e){
			e.printStackTrace();
			bOk = false;
			return "1";
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
	}
//****************************************************************************** FODEA 032 - 2008 PROMOCION A PYMES	(I) ACF

/**
 * Determina si la pyme recibe promoci�n de los productos de N@E en algun medio
 * ya sea email o celular.
 * @param ic_pyme El numero de la pyme
 * @return String
 */
	public String recibePromociones(String icPyme, String bancoFondeo) throws NafinException{
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs	= null;
		String strSQL	= "";
		String recibePromocion = "";

		try{
			con.conexionDB();
			strSQL = " SELECT COUNT(ic_pyme) FROM com_promo_pyme WHERE ic_pyme = ? AND ic_banco_fondeo = ?";

			System.out.println("..:: strSQL : "+strSQL);

			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, Long.parseLong(icPyme));
			ps.setInt(2, Integer.parseInt(bancoFondeo));

			rs = ps.executeQuery();

			while(rs.next()){
				recibePromocion = rs.getString(1).trim();
			}

			rs.close();
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return recibePromocion;
	}

/**
 * Actualiza las preferencias de la pyme, para recibir promoci�n de los productos de N@E en algun medio
 * ya sea email o celular.
 * @param condiciones Un HashMap que contiene los criterios con los que la pyme parametriza la recepcion de
 * 				la informcaion acerca de los priductos de nafin.
 */
	public void actualizaPromociones(HashMap condiciones) throws NafinException{
		AccesoDB con = new AccesoDB();
		ResultSet rs	= null;
		String strSQL	= "";

		try{
			con.conexionDB();

			String correoPrevio = "";
			String celularPrevio = "";
			String cambiaCorreo = "";
			String cveUsuario = (String)condiciones.get("cveUsuario");
			String nombreUsuario = (String)condiciones.get("nombreUsuario");
			String ic_pyme = (String)condiciones.get("ic_pyme");
			String aceptaPromo = (String)condiciones.get("aceptaPromo");
			String celularChk = (String)condiciones.get("celularChk");
			String emailRegChk = (String)condiciones.get("emailRegChk");
			String confirmacionCelular = (String)condiciones.get("confirmacionCelular");
			String confirmacionCorreo = (String)condiciones.get("confirmacionCorreo");
			String nuevoCorreo = (String)condiciones.get("nuevoCorreo");
			String numeroCelular = (String)condiciones.get("numeroCelular");

			strSQL = " SELECT CS_PROMO_EMAIL, CS_PROMO_CELULAR FROM COM_PROMO_PYME WHERE IC_PYME = ?";

			System.out.println("..:: strSQL : "+strSQL);

			PreparedStatement preparedStatement = con.queryPrecompilado(strSQL);
			preparedStatement.setInt(1, Integer.parseInt(ic_pyme));

			rs = preparedStatement.executeQuery();

			while(rs.next()){
				correoPrevio = rs.getString(1).trim();
				celularPrevio = rs.getString(2).trim();
			}

			rs.close();
			preparedStatement.close();

			if (aceptaPromo.equals("S")){
				String email = "";
				String celular = "";

				if(emailRegChk.equals("S") || (!nuevoCorreo.equals("") && nuevoCorreo.equals(confirmacionCorreo))){
					email = "S";
				}else{
					email = "N";
				}

                if (celularChk.equals("S") &&
                    (!numeroCelular.equals("") && numeroCelular.equals(confirmacionCelular))) {
					celular = "S";
				}else{
					celular = "N";
				}

				if(!nuevoCorreo.equals("") && nuevoCorreo.equals(confirmacionCorreo)){
					cambiaCorreo = "S";
				}else{
					cambiaCorreo = "N";
				}

                strSQL =
                    " UPDATE COM_PROMO_PYME SET CS_PROMO_EMAIL = ?, CS_PROMO_CELULAR = ?, CG_CLAVE_USUARIO = ?, CG_NOMBRE_USUARIO = ?, DF_PROMOCION = SYSDATE, CS_EMAIL_MODIF = ?" +
								 " WHERE IC_PYME = ?";

				System.out.println("..:: strSQL : "+strSQL);

				PreparedStatement ps = con.queryPrecompilado(strSQL);

				ps.setString(1, email);
				ps.setString(2, celular);
				ps.setString(3, cveUsuario);
				ps.setString(4, nombreUsuario);
				ps.setString(5, cambiaCorreo);
				ps.setInt(6, Integer.parseInt(ic_pyme));

				ps.executeUpdate();

				ps.close();


				if(!emailRegChk.equals("S") && !nuevoCorreo.equals("") && nuevoCorreo.equals(confirmacionCorreo)){
					strSQL = " UPDATE COM_CONTACTO SET CG_EMAIL = ? WHERE IC_PYME = ? AND CS_PRIMER_CONTACTO = ?";

					System.out.println("..:: strSQL : "+strSQL);

					PreparedStatement ps1 = con.queryPrecompilado(strSQL);

					ps1.setString(1, nuevoCorreo);
					ps1.setInt(2, Integer.parseInt(ic_pyme));
					ps1.setString(3, "S");

					ps1.executeUpdate();

					ps1.close();

					UtilUsr utilUsr = new UtilUsr();
					SolicitudArgus solicitud = new SolicitudArgus();
					Usuario usr = new Usuario();
					usr.setLogin(cveUsuario);
					usr.setEmail(nuevoCorreo);
					solicitud.setUsuario(usr);

					utilUsr.actualizarUsuario(solicitud);
				}

				if(celularChk.equals("S") && !numeroCelular.equals("") && numeroCelular.equals(confirmacionCelular)){
					strSQL = " UPDATE COM_CONTACTO SET CG_CELULAR = ? WHERE IC_PYME = ? AND CS_PRIMER_CONTACTO = ?";

					System.out.println("..:: strSQL : "+strSQL);

					PreparedStatement ps1 = con.queryPrecompilado(strSQL);

					ps1.setString(1, numeroCelular);
					ps1.setInt(2, Integer.parseInt(ic_pyme));
					ps1.setString(3, "S");

					ps1.executeUpdate();

					ps1.close();
				}
			}else{
                strSQL =
                    " UPDATE COM_PROMO_PYME SET CS_PROMO_EMAIL = ?, CS_PROMO_CELULAR = ?, CG_CLAVE_USUARIO = ?, CG_NOMBRE_USUARIO = ?, DF_PROMOCION = SYSDATE, CS_EMAIL_MODIF = ?" +
								 " WHERE IC_PYME = ?";

				System.out.println("..:: strSQL : "+strSQL);

				PreparedStatement ps = con.queryPrecompilado(strSQL);

				ps.setString(1, "N");
				ps.setString(2, "N");
				ps.setString(3, cveUsuario);
				ps.setString(4, nombreUsuario);
				ps.setString(5, "N");
				ps.setInt(6, Integer.parseInt(ic_pyme));

				ps.executeUpdate();

				ps.close();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

/**
 * Obtiene el numero de celular, si la pyme tiene uno parmetrizado, de no ser asi, regresa una cadena vacia.
 * @param ic_pyme El numero de la pyme.
 * @return el numero celular como una cadena.
 */
	public HashMap consultaPromocionPyme(String ic_pyme) throws NafinException{
		AccesoDB con = new AccesoDB();
		HashMap resultado = new HashMap();
		PreparedStatement	ps = null;
		ResultSet rs	= null;
		String strSQL	= "";
		String numero_celular = "";
		String promo_email = "";
		String promo_cel  = "";

		try{
			con.conexionDB();

			strSQL = "SELECT CS_PROMO_EMAIL, CS_PROMO_CELULAR FROM COM_PROMO_PYME WHERE IC_PYME = ?";
			System.out.println("..:: strSQL : "+strSQL);
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(ic_pyme));

			rs = ps.executeQuery();

			while(rs.next()){
				promo_email = rs.getString(1).trim();
				promo_cel = rs.getString(2).trim();
			}

			rs.close();
			ps.close();

			if(promo_cel.equals("S")){
				strSQL = " SELECT CG_CELULAR FROM COM_CONTACTO WHERE IC_PYME = ? AND CS_PRIMER_CONTACTO = ?";
				System.out.println("..:: strSQL : "+strSQL);
				ps = con.queryPrecompilado(strSQL);
				ps.setInt(1, Integer.parseInt(ic_pyme));
				ps.setString(2, "S");

				rs = ps.executeQuery();

				while(rs.next()){
					numero_celular = (rs.getString(1) == null)?"":rs.getString(1).trim();
				}

				rs.close();
				ps.close();
			}

			resultado.put("promo_email", promo_email);
			resultado.put("promo_cel", promo_cel);
			resultado.put("numero_celular", numero_celular);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return resultado;
	}
//****************************************************************************** FODEA 032 - 2008 PROMOCION A PYMES	(F) ACF
//****************************************************************************** FODEA 048 - 2008 FACTORAJE MOVIL

/**
 * Este m�todo permite realizar una consulta de la pymes que reciben informaci�n de factoraje
 * a trav�s de email y/o celular.
 * @param condiciones HashMap que contiene las condiciones mediante las cuales se realiza la consulta.
 * @return resultado HashMap con el resultado de la consulta.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public HashMap consultaPromociones(HashMap condiciones) throws NafinException{
		AccesoDB con = new AccesoDB();
		HashMap resultado = new HashMap();
		ResultSet rs	= null;
		StringBuffer strSQL	= new StringBuffer();
		List varBind = new ArrayList();
		int i = 0;

		try{
			con.conexionDB();

			String numeroEpo = (String)condiciones.get("numeroEpo");
			String nafinElectronico = (String)condiciones.get("nafinElectronico");
			String icPyme = (String)condiciones.get("icPyme");
			String usuario = (String)condiciones.get("usuario");
			String fechaInicial = (String)condiciones.get("fechaInicial");
			String fechaFinal = (String)condiciones.get("fechaFinal");
			String tipoModificacion = (String)condiciones.get("tipoModificacion");

			strSQL.append(" SELECT CRN.IC_NAFIN_ELECTRONICO,");
			strSQL.append(" CCP.CG_RAZON_SOCIAL,");
			strSQL.append(" CPP.CG_CLAVE_USUARIO,");
			strSQL.append(" CPP.CG_NOMBRE_USUARIO,");
			strSQL.append(" TO_CHAR(CPP.DF_PROMOCION, 'DD/MM/YYYY HH:MI AM'),");
			strSQL.append(" CCN.CG_APPAT||' '||CCN.CG_APMAT||' '|| CCN.CG_NOMBRE,");
			strSQL.append(" CPP.CS_FACTORAJE_MOVIL,");
			strSQL.append(" CCN.CG_EMAIL,");
			strSQL.append(" CCN.CG_CELULAR");
			strSQL.append(" FROM COM_PROMO_PYME CPP,");
			strSQL.append(" COMREL_NAFIN CRN,");
			strSQL.append(" COMCAT_PYME CCP,");
			strSQL.append(" COM_CONTACTO CCN");

			if(!numeroEpo.equals("")){
				strSQL.append(", COMREL_PYME_EPO CPE");
			}

			strSQL.append(" WHERE CPP.IC_PYME = CRN.IC_EPO_PYME_IF");
			strSQL.append(" AND CPP.IC_PYME = CCP.IC_PYME");
			strSQL.append(" AND CPP.IC_PYME = CCN.IC_PYME");
			strSQL.append(" AND CCN.CS_PRIMER_CONTACTO = ?");
			strSQL.append(" AND CRN.CG_TIPO = ?");

			varBind.add("S");
			varBind.add("P");

			if(!numeroEpo.equals("")){
				strSQL.append(" AND CPP.IC_PYME = CPE.IC_PYME");
				strSQL.append(" AND CPE.IC_EPO = ?");

				varBind.add(new Long(numeroEpo));
			}

			if(!nafinElectronico.equals("") && !icPyme.equals("")){
				strSQL.append(" AND CRN.IC_NAFIN_ELECTRONICO = ?");
				strSQL.append(" AND CPP.IC_PYME = ?");

				varBind.add(new Long(nafinElectronico));
				varBind.add(new Long(icPyme));
			}

			if(!usuario.equals("")){
				strSQL.append(" AND CPP.CG_CLAVE_USUARIO = ?");

				varBind.add(usuario);
			}

			if(!fechaInicial.equals("") && !fechaFinal.equals("")){
				strSQL.append(" AND CPP.DF_PROMOCION BETWEEN TO_DATE( ?, 'DD/MM/YYYY') AND TO_DATE( ?, 'DD/MM/YYYY') + 1");

				varBind.add(fechaInicial);
				varBind.add(fechaFinal);
			}

			if(!tipoModificacion.equals("")){
				if(tipoModificacion.equals("EMAIL")){
					strSQL.append(" AND CPP.CS_EMAIL_MODIF = ?");
					varBind.add("S");
				}else if(tipoModificacion.equals("CELULAR")){
					strSQL.append(" AND CPP.CS_CELULAR_MODIF = ?");
					varBind.add("S");
				}
			}

			System.out.println("..:: strSQL : "+strSQL.toString());
			System.out.println("..:: varBind : "+varBind.toString());

			PreparedStatement preparedStatement = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = preparedStatement.executeQuery();

			while(rs.next()){
				resultado.put("nafinElectronico"+i, (rs.getString(1) == null)?"":rs.getString(1).trim());
				resultado.put("nombrePyme"+i, (rs.getString(2) == null)?"":rs.getString(2).trim());
				resultado.put("claveUsuario"+i, (rs.getString(3) == null)?"":rs.getString(3).trim());
				resultado.put("nombreUsuario"+i,(rs.getString(4) == null)?"":rs.getString(4).trim());
				resultado.put("fecha"+i, (rs.getString(5) == null)?"":rs.getString(5).trim());
				resultado.put("nombreContacto"+i, (rs.getString(6) == null)?"":rs.getString(6).trim());
				resultado.put("aceptaServicio"+i, (rs.getString(7) == null)?"":rs.getString(7).trim());
				resultado.put("email"+i, (rs.getString(8) == null)?"":rs.getString(8).trim());
				resultado.put("celular"+i, (rs.getString(9) == null)?"":rs.getString(9).trim());
				i++;
			}

			resultado.put("registros", Integer.toString(i));

			rs.close();
			preparedStatement.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return resultado;
	}

/**
 * Este m�todo permite realizar una consulta de los datos de la pyme seleccionada
 * @param icPyme la clave de la pyme de la que se obtienen los datos
 * @return resultado HashMap con el resultado de la consulta.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public HashMap getInformacionPyme(String icPyme) throws NafinException{
		AccesoDB con = new AccesoDB();
		HashMap resultado = new HashMap();
		ResultSet rs	= null;
		StringBuffer strSQL	= new StringBuffer();
		List varBind = new ArrayList();

		try{
			con.conexionDB();

			strSQL.append(" SELECT pym.cg_rfc,");
			strSQL.append(" dom.cg_calle||' '||dom.cg_numero_ext||' '||dom.cg_numero_int,");
			strSQL.append(" dom.cg_colonia,");
			strSQL.append(" dom.cn_cp,");
			strSQL.append(" dom.cg_telefono1");
			strSQL.append(" FROM comcat_pyme pym,");
			strSQL.append(" com_domicilio dom");
			strSQL.append(" WHERE pym.ic_pyme = dom.ic_pyme");
			strSQL.append(" AND pym.ic_pyme = ?");

			varBind.add(new Long(icPyme));

			System.out.println("..:: strSQL : "+strSQL.toString());
			System.out.println("..:: varBind : "+varBind.toString());

			PreparedStatement preparedStatement = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = preparedStatement.executeQuery();

			if(rs.next()){
				resultado.put("RFC", rs.getString(1) == null?"-":rs.getString(1).trim());
				resultado.put("CALLE", rs.getString(2) == null?"-":rs.getString(2).trim());
				resultado.put("COLONIA", rs.getString(3) == null?"-":rs.getString(3).trim());
				resultado.put("CODIGO_POSTAL",rs.getString(4) == null?"-":rs.getString(4).trim());
				resultado.put("TELEFONO", rs.getString(5) == null?"-":rs.getString(5).trim());
			}

			rs.close();
			preparedStatement.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return resultado;
	}

/**
 * Este m�todo permite consultar el correo y el celular registrados de la pyme
 * para agragarlos al servicio de factoraje movil.
 *
 * @param icPyme la clave de la pyme de la que se obtienen los datos
 * @return resultado lista con el resultado de la consulta.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public List getDatosPymeFactorajeMovil(String icPyme) throws NafinException{
		AccesoDB con = new AccesoDB();
		ResultSet rs	= null;
		StringBuffer strSQL	= new StringBuffer();
		List varBind = new ArrayList();
		List resultado = new ArrayList();

		try{
			con.conexionDB();

			strSQL.append(" SELECT cg_email, cg_celular FROM com_contacto WHERE ic_pyme = ? AND cs_primer_contacto = ?");

			varBind.add(new Long(icPyme));
			varBind.add("S");

			System.out.println("..:: strSQL : "+strSQL.toString());
			System.out.println("..:: varBind : "+varBind.toString());

			PreparedStatement preparedStatement = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = preparedStatement.executeQuery();

			if(rs.next()){
				resultado.add(rs.getString(1) == null?"":rs.getString(1));
				resultado.add(rs.getString(2) == null?"":rs.getString(2));
			}

			rs.close();
			preparedStatement.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return resultado;
	}

/**
 * Este m�todo permite guerdar la informaci�n del servicio de factoraje m�vil para la pyme
 * sin activar el servicio.
 *
 * @param datosRegistroFactorajeMovil lista con los par�metros del registro de la pyme
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public void guardarRegistroFactorajeMovil(List datosRegistroFactorajeMovil) throws NafinException{
		AccesoDB con = new AccesoDB();
		ResultSet rs	= null;
		StringBuffer strSQL	= new StringBuffer();
		List varBind = new ArrayList();
		boolean flag = true;

		try{
			con.conexionDB();

			String icPyme = (String)datosRegistroFactorajeMovil.get(0);
			String bancoFondeo = (String)datosRegistroFactorajeMovil.get(1);
			String recibeFactoraje = (String)datosRegistroFactorajeMovil.get(2);
			String nuevoCorreo = (String)datosRegistroFactorajeMovil.get(3);
			//String confirmacionCorreo = (String)datosRegistroFactorajeMovil.get(4);
			String nuevoCelular = (String)datosRegistroFactorajeMovil.get(5);
			//String confirmacionCelular = (String)datosRegistroFactorajeMovil.get(6);
			String nombreUsuario = (String)datosRegistroFactorajeMovil.get(7);
			String loginUsuario = (String)datosRegistroFactorajeMovil.get(8);
			String icTerminos = (String)datosRegistroFactorajeMovil.get(9);
			String desactivacion = (String)datosRegistroFactorajeMovil.get(10);

			String registroFactoraje = "";
			//======================================================================>> Consulta si la pyme ya esta registrada en el servicio de factoraje movil
			strSQL.append(" SELECT COUNT(ic_pyme) FROM com_promo_pyme WHERE ic_pyme = ?");
			varBind.add(new Long(icPyme));

			System.out.println("..:: strSQL : "+strSQL.toString());
			System.out.println("..:: varBind : "+varBind.toString());

			PreparedStatement pStatement = con.queryPrecompilado(strSQL.toString(), varBind);

			rs = pStatement.executeQuery();

            if (rs.next()) {
                registroFactoraje = rs.getString(1) == null ? "" : rs.getString(1);
            }

			rs.close();
			pStatement.close();

			if(!registroFactoraje.equals("0")){//===================================>> Si la pyme ya tiene registro
				String correoAnterior = "";
				String celularAnterior = "";
				String modifCorreo = "";
				String modifCelular = "";
				String registroBancoFondeo = "";

				strSQL = new StringBuffer();
				varBind = new ArrayList();
				//===================================>> Consulta el correo registrado de la pyme
				strSQL.append(" SELECT cg_email FROM com_contacto WHERE ic_pyme = ? AND cs_primer_contacto = ?");
				varBind.add(new Long(icPyme));
				varBind.add("S");

				System.out.println("..:: strSQL : "+strSQL.toString());
				System.out.println("..:: varBind : "+varBind.toString());

				PreparedStatement pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

				rs = pStatementS.executeQuery();

                if (rs.next()) {
                    correoAnterior = rs.getString(1) == null ? "" : rs.getString(1);
                }

				rs.close();
				pStatementS.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();
				//===================================>> Consulta el celular registrado de la pyme
				strSQL.append(" SELECT cg_celular FROM com_contacto WHERE ic_pyme = ? AND cs_primer_contacto = ?");
				varBind.add(new Long(icPyme));
				varBind.add("S");

				System.out.println("..:: strSQL : "+strSQL.toString());
				System.out.println("..:: varBind : "+varBind.toString());

				pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

				rs = pStatementS.executeQuery();

                if (rs.next()) {
                    celularAnterior = rs.getString(1) == null ? "" : rs.getString(1);
                }

				rs.close();
				pStatementS.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();
				//===================================>> Consulta si la pyme ya esta registrada en el servicio de factoraje movil con el banco de fondeo
				strSQL.append(" SELECT COUNT(ic_pyme) FROM com_promo_pyme WHERE ic_pyme = ? AND ic_banco_fondeo = ?");
				varBind.add(new Long(icPyme));
				varBind.add(new Integer(bancoFondeo));

				System.out.println("..:: strSQL : "+strSQL.toString());
				System.out.println("..:: varBind : "+varBind.toString());

				pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

				rs = pStatementS.executeQuery();

                if (rs.next()) {
                    registroBancoFondeo = rs.getString(1) == null ? "" : rs.getString(1);
                }

				rs.close();
				pStatementS.close();
				//===================================>> Verifica si se cambio el correo de la pyme
                if (!nuevoCorreo.equals("") && !correoAnterior.equals(nuevoCorreo)) {
                    modifCorreo = "S";
                } else {
                    modifCorreo = "N";
                }
				//===================================>> Verifica si se cambio el celular de la pyme
                if (!nuevoCelular.equals("") && !celularAnterior.equals(nuevoCelular)) {
                    modifCelular = "S";
                } else {
                    modifCelular = "N";
                }

				if(!registroBancoFondeo.equals("0")){//===================================>> Si la pyme ya tiene registro en factoraje movil con el banco de fondeo
					if(modifCorreo.equals("S")){//===================================>> Si se cambio el celular se actualiza el celular y se registra el cambio
						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" UPDATE com_contacto SET cg_email = ? WHERE ic_pyme = ? AND cs_primer_contacto = ?");
						varBind.add(nuevoCorreo);
						varBind.add(new Long(icPyme));
						varBind.add("S");

						System.out.println("..:: strSQL : "+strSQL.toString());
						System.out.println("..:: varBind : "+varBind.toString());

						pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

						pStatementS.executeUpdate();

						pStatementS.close();
					}

					if(modifCelular.equals("S")){//===================================>> Si se cambio el correo se actualiza el correo y se registra el cambio
						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" UPDATE com_contacto SET cg_celular = ? WHERE ic_pyme = ? AND cs_primer_contacto = ?");
						varBind.add(nuevoCelular);
						varBind.add(new Long(icPyme));
						varBind.add("S");

						System.out.println("..:: strSQL : "+strSQL.toString());
						System.out.println("..:: varBind : "+varBind.toString());

						pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

						pStatementS.executeUpdate();

						pStatementS.close();
					}


					strSQL = new StringBuffer();
					varBind = new ArrayList();

					if(desactivacion.equals("1")){
						//===================================>> Se actualizan los datos de la pyme registrada con el banco de fondeo
						strSQL.append(" UPDATE com_promo_pyme SET cg_clave_usuario = ?, cg_nombre_usuario = ?, df_desactivacion = SYSDATE, cs_email_modif = ?, cs_celular_modif = ?, cs_factoraje_movil = ?, cs_activado = ?, ic_terminos_sms = ?");
						strSQL.append(" WHERE ic_pyme = ?");
						strSQL.append(" AND ic_banco_fondeo = ?");
					}else{
						//===================================>> Se actualizan los datos de la pyme registrada con el banco de fondeo
						strSQL.append(" UPDATE com_promo_pyme SET cg_clave_usuario = ?, cg_nombre_usuario = ?, df_activacion = SYSDATE, cs_email_modif = ?, cs_celular_modif = ?, cs_factoraje_movil = ?, cs_activado = ?, ic_terminos_sms = ?");
						strSQL.append(" WHERE ic_pyme = ?");
						strSQL.append(" AND ic_banco_fondeo = ?");
					}

					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(modifCorreo);
					varBind.add(modifCelular);
					varBind.add(recibeFactoraje);
					varBind.add("N");
					varBind.add(new Integer(icTerminos));
					varBind.add(new Long(icPyme));
					varBind.add(new Integer(bancoFondeo));

					System.out.println("..:: strSQL : "+strSQL.toString());
					System.out.println("..:: varBind : "+varBind.toString());

					pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

					pStatementS.executeUpdate();

					pStatementS.close();
				}else{//===================================>> Si la pyme ya tiene registro en factoraje movil pero no tiene registrado un banco de fondeo
					if(modifCelular.equals("S")){//===================================>> Si se cambio el celular se actualiza el celular y se registra el cambio
						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" UPDATE com_contacto SET cg_celular = ? WHERE ic_pyme = ? AND cs_primer_contacto = ?");
						varBind.add(nuevoCelular);
						varBind.add(new Long(icPyme));
						varBind.add("S");

						System.out.println("..:: strSQL : "+strSQL.toString());
						System.out.println("..:: varBind : "+varBind.toString());

						pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

						pStatementS.executeUpdate();

						pStatementS.close();
					}

					if(modifCorreo.equals("S")){//===================================>> Si se cambio el correo se actualiza el correo y se registra el cambio
						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" UPDATE com_contacto SET cg_email = ? WHERE ic_pyme = ? AND cs_primer_contacto = ?");
						varBind.add(nuevoCorreo);
						varBind.add(new Long(icPyme));
						varBind.add("S");

						System.out.println("..:: strSQL : "+strSQL.toString());
						System.out.println("..:: varBind : "+varBind.toString());

						pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

						pStatementS.executeUpdate();

						pStatementS.close();
					}

					strSQL = new StringBuffer();
					varBind = new ArrayList();
					//===================================>> Se actualizan los datos de la pyme registrada agregando el banco de fondeo
					strSQL.append(" UPDATE com_promo_pyme SET ic_banco_fondeo = ?, cg_clave_usuario = ?, cg_nombre_usuario = ?, df_activacion = SYSDATE, cs_email_modif = ?, cs_celular_modif = ?, cs_factoraje_movil = ?, cs_activado = ?, ic_terminos_sms = ?");
					strSQL.append(" WHERE ic_pyme = ?");

					varBind.add(new Integer(bancoFondeo));
					varBind.add(loginUsuario);
					varBind.add(nombreUsuario);
					varBind.add(modifCorreo);
					varBind.add(modifCelular);
					varBind.add(recibeFactoraje);
					varBind.add("N");
					varBind.add(new Integer(icTerminos));
					varBind.add(new Long(icPyme));

					System.out.println("..:: strSQL : "+strSQL.toString());
					System.out.println("..:: varBind : "+varBind.toString());

					pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

					pStatementS.executeUpdate();

					pStatementS.close();
				}
			}else{//===================================>> Si la pyme no tiene registro en factoraje movil
				String correoAnterior = "";
				String celularAnterior = "";
				String modifCorreo = "";
				String modifCelular = "";

				strSQL = new StringBuffer();
				varBind = new ArrayList();
				//===================================>> Consulta el correo registrado de la pyme
				strSQL.append(" SELECT cg_email FROM com_contacto WHERE ic_pyme = ? AND cs_primer_contacto = ?");
				varBind.add(new Long(icPyme));
				varBind.add("S");

				System.out.println("..:: strSQL : "+strSQL.toString());
				System.out.println("..:: varBind : "+varBind.toString());

				PreparedStatement pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

				rs = pStatementS.executeQuery();

                if (rs.next()) {
                    correoAnterior = rs.getString(1) == null ? "" : rs.getString(1);
                }

				rs.close();
				pStatementS.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();
				//===================================>> Consulta el celular registrado de la pyme
				strSQL.append(" SELECT cg_celular FROM com_contacto WHERE ic_pyme = ? AND cs_primer_contacto = ?");
				varBind.add(new Long(icPyme));
				varBind.add("S");

				System.out.println("..:: strSQL : "+strSQL.toString());
				System.out.println("..:: varBind : "+varBind.toString());

				pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

				rs = pStatementS.executeQuery();

                if (rs.next()) {
                    celularAnterior = rs.getString(1) == null ? "" : rs.getString(1);
                }

				rs.close();
				pStatementS.close();
				//===================================>> Verifica si se cambio el correo de la pyme
                if (!nuevoCorreo.equals("") && !correoAnterior.equals(nuevoCorreo)) {
                    modifCorreo = "S";
                } else {
                    modifCorreo = "N";
                }
				//===================================>> Verifica si se cambio el celular de la pyme
                if (!nuevoCelular.equals("") && !celularAnterior.equals(nuevoCelular)) {
                    modifCelular = "S";
                } else {
                    modifCelular = "N";
                }

				if(modifCelular.equals("S")){//===================================>> Si se cambio el celular se actualiza el celular y se registra el cambio
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" UPDATE com_contacto SET cg_celular = ? WHERE ic_pyme = ? AND cs_primer_contacto = ?");
					varBind.add(nuevoCelular);
					varBind.add(new Long(icPyme));
					varBind.add("S");

					System.out.println("..:: strSQL : "+strSQL.toString());
					System.out.println("..:: varBind : "+varBind.toString());

					pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

					pStatementS.executeUpdate();

					pStatementS.close();
				}

				if(modifCorreo.equals("S")){//===================================>> Si se cambio el correo se actualiza el correo y se registra el cambio
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" UPDATE com_contacto SET cg_email = ? WHERE ic_pyme = ? AND cs_primer_contacto = ?");
					varBind.add(nuevoCorreo);
					varBind.add(new Long(icPyme));
					varBind.add("S");

					System.out.println("..:: strSQL : "+strSQL.toString());
					System.out.println("..:: varBind : "+varBind.toString());

					pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

					pStatementS.executeUpdate();

					pStatementS.close();
				}

				strSQL = new StringBuffer();
				varBind = new ArrayList();
				//===================================>> Se agregan los datos de la pyme sin registro previo, a�adiendo el banco de fondeo
				strSQL.append(" INSERT INTO com_promo_pyme(ic_pyme, ic_banco_fondeo, cs_promo_email, cs_promo_celular, cg_clave_usuario, cg_nombre_usuario, df_promocion, cs_email_modif, cs_celular_modif, cs_factoraje_movil, cs_activado, ic_terminos_sms)");
				strSQL.append(" VALUES(?, ?, ?, ?, ?, ?, SYSDATE, ?, ?, ?, ?, ?)");

				varBind.add(new Long(icPyme));
				varBind.add(new Integer(bancoFondeo));
				varBind.add("S");
				varBind.add("S");
				varBind.add(loginUsuario);
				varBind.add(nombreUsuario);
				varBind.add(modifCorreo);
				varBind.add(modifCelular);
				varBind.add(recibeFactoraje);
				varBind.add("N");
				varBind.add(new Integer(icTerminos));

				System.out.println("..:: strSQL : "+strSQL.toString());
				System.out.println("..:: varBind : "+varBind.toString());

				pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

				pStatementS.executeUpdate();

				pStatementS.close();
			}
		}catch(Exception e){
			flag = false;
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(flag);
				con.cierraConexionDB();
			}
		}
	}

/**
 * Este m�todo permite activar el servicio de factoraje m�vil para la pyme
 *
 * @param datosRegistroFactorajeMovil lista con los par�metros del registro de la pyme
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public void activarServicioFactorajeMovil(List datosActivacionFactorajeMovil) throws NafinException{
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL	= new StringBuffer();
		List varBind = new ArrayList();
		boolean flag = true;

		try{
			con.conexionDB();

			String icPyme = (String)datosActivacionFactorajeMovil.get(0);
			String bancoFondeo = (String)datosActivacionFactorajeMovil.get(1);
			String momentoEnvioS = (String)datosActivacionFactorajeMovil.get(2);
			String tipoInformacionS = (String)datosActivacionFactorajeMovil.get(3);
			String compCelular = (String)datosActivacionFactorajeMovil.get(4);

			strSQL.append(" UPDATE com_promo_pyme SET cs_momento_envio = ?, cs_info_envio = ?, cg_comp_celular = ?, cs_activado = ?, df_activacion = SYSDATE");
			strSQL.append(" WHERE ic_pyme = ?");
			strSQL.append(" AND ic_banco_fondeo = ?");

			varBind.add(momentoEnvioS);
			varBind.add(tipoInformacionS);
			varBind.add(compCelular);
			varBind.add("S");
			varBind.add(new Long(icPyme));
			varBind.add(new Integer(bancoFondeo));

			System.out.println("..:: strSQL : "+strSQL.toString());
			System.out.println("..:: varBind : "+varBind.toString());

			PreparedStatement pStatementS = con.queryPrecompilado(strSQL.toString(), varBind);

			pStatementS.executeUpdate();

			pStatementS.close();

		}catch(Exception e){
			flag = false;
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(flag);
				con.cierraConexionDB();
			}
		}
	}

  /**
   * Genera un archivo en el directorio temporal para ser consultado en la
	 * pantalla del expediente digitalizado de la pyme.
	 * @param icPyme Es la clave de la pyme de la que se consulta el archivo del expediente digitalizado.
   * @param icTipo Especifica el tipo de documento del expediente digitalizado que se va a descargar.
   * @param directorioTmp Es el directorio donde se genera de manera temporal el archivo.
	 * @return file El archivo obtenido de la base de datos, este archivo est� en formato PDF.
	 * @throws netropology.utilerias.AppException Al encontrar un error inesperado
   */
    public File getArchivoTerminosCondiciones(String bancoFondeo, String icTerminos,
                                              String directorioTmp) throws NafinException {

		AccesoDB con = new AccesoDB();
		File file = null;
    FileOutputStream fileOutputStream = null;

    try{
      con.conexionDB();
            String strSQL =
                " SELECT bi_terminos, cg_extension" + " FROM comcat_terminos_sms" + " WHERE ic_banco_fondeo = ?" +
											" AND ic_terminos_sms = ?";

      PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(bancoFondeo));
			ps.setInt(2, Integer.parseInt(icTerminos));
      ResultSet rs = ps.executeQuery();

			if(rs.next()){
                String pathname =
                    directorioTmp + "Terminos_y_Condiciones_" + Comunes.cadenaAleatoria(4) + "." +
                    rs.getString("cg_extension").toLowerCase();
        file = new File(pathname);
        fileOutputStream = new FileOutputStream(file);
        Blob blob = rs.getBlob("bi_terminos");
        InputStream inStream = blob.getBinaryStream();
        int size = (int)blob.length();
        byte[] buffer = new byte[size];
        int length = -1;

				while ((length = inStream.read(buffer)) != -1){
          fileOutputStream.write(buffer, 0, length);
        }
      }
    }catch(Exception e){
			e.printStackTrace();
    }finally{
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
    }
		return file;
  }

/**
 * Obtiene el banco de fondeo con el que trabaja la Epo a la que esta afiliada la pyme
 * @param icEpo El numero de la EPO
 * @return bancoFondeo es la clave del banco de fondeo.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public String getBancoFondeo(String icEpo) throws NafinException{
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs	= null;
		String bancoFondeo = "";

		try{
			con.conexionDB();
			String strSQL = " SELECT ic_banco_fondeo FROM comcat_epo WHERE ic_epo = ?";

			System.out.println("..:: strSQL : "+strSQL);

			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, Long.parseLong(icEpo));

			rs = ps.executeQuery();

			while(rs.next()){
				bancoFondeo = rs.getString(1).trim();
			}

			rs.close();
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return bancoFondeo;
	}

/**
 * Obtiene los dias previos al vencimiento de los documentos que tiene publicados una pyme
 * para enviarle una notificacions via sms.
 * @return diasPrevios es el numero de dias previos al vencimiento de los documentos.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public String getDiasPreviosVencimiento() throws NafinException{
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs	= null;
		String diasPrevios = "";

		try{
			con.conexionDB();
			String strSQL = " SELECT cg_valor FROM com_parametros_sms WHERE cc_param_sms = ?";

			System.out.println("..:: strSQL : "+strSQL);

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, "DIAS_PREVIOS_VENCIMIENTO");

			rs = ps.executeQuery();

            while (rs.next()) {
                diasPrevios = rs.getString(1).trim();
            }

			rs.close();
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return diasPrevios;
	}

/**
 * Obtiene la clave de la versi�n mas reciente del archivo de terminos y condiciones
 * del servicio nafinsa m�vil, dependiendo del banco de fondeo con el que trabaje la
 * EPO a la que esta afiliada la pyme
 * @param bancoFondeo es la clave del banco de fondeo con el que trabaja la EPO.
 * @return icVersion es la clave de la versi�n del archivo de terminos y condiciones.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public String getVersionTerminosCondiciones(String bancoFondeo) throws NafinException{
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs	= null;
		String icVersion = "";

		try{
			con.conexionDB();
			String fechaVersion = "";
            String strSQL =
                " SELECT TO_CHAR(MAX(df_alta), 'DD/MM/YYYY') FROM comcat_terminos_sms WHERE ic_banco_fondeo = ?";

			System.out.println("..:: strSQL : "+strSQL);

			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(bancoFondeo));

			rs = ps.executeQuery();

            while (rs.next()) {
                fechaVersion = rs.getString(1).trim();
            }

			rs.close();
			ps.close();

            strSQL =
                " SELECT ic_terminos_sms FROM comcat_terminos_sms WHERE ic_banco_fondeo = ? AND TO_CHAR(df_alta, 'DD/MM/YYYY') = ?";

			System.out.println("..:: strSQL : "+strSQL);

			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(bancoFondeo));
			ps.setString(2, fechaVersion);

			rs = ps.executeQuery();

            while (rs.next()) {
                icVersion = rs.getString(1).trim();
            }

			rs.close();
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return icVersion;
	}

/**
 * Este proceso se encarga de verificar si la pyme acept� o no el servicio de factoraje m�vil
 * @param icPyme Es la clave de la pyme.
 * @return servicioAceptado cadena que indica si la pyme ya acept� el servicio o no lo ha aceptado
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public String servicioAceptado(String icPyme, String bancoFondeo) throws NafinException{
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps = null;
		ResultSet rs	= null;
		String servicioAceptado = "";

		try{
			con.conexionDB();
			String strSQL = " SELECT cs_factoraje_movil FROM com_promo_pyme WHERE ic_pyme = ? AND ic_banco_fondeo = ?";

			System.out.println("..:: strSQL : "+strSQL);

			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, Long.parseLong(icPyme));
			ps.setInt(2, Integer.parseInt(bancoFondeo));

			rs = ps.executeQuery();

            while (rs.next()) {
                servicioAceptado = rs.getString(1) == null ? "" : rs.getString(1);
            }

			rs.close();
			ps.close();

            if (servicioAceptado.equals("")) {
                servicioAceptado = "N";
            }
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return servicioAceptado;
	}

/**
 * Este m�todo se invoca desde el proceso que se encarga de notificar a las pymes via
 * SMS de los documentos que tiene publicados. La informaci�n que se env�a en cada
 * mensaje depende de la parametrizaci�n que hizo cada pyme al momento de activar el servicio.
 *
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public void enviaSMSFactorajeMovilPublicacion() throws NafinException{
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		SMS sms = new SMS();
		PreparedStatement	pst1 = null;
		ResultSet rst1	= null;
		boolean trans_op = true;
		boolean flag_envio = false;

		log.info("enviaSMSFactorajeMovilPublicacion(I) ::..");

		try{
			con.conexionDB();
			/*Se obtiene el costo por mensaje sms*/
			String costo_sms = sms.getCostoSMS();

			/* Se obtienen las claves de las pymes que tienen el servicio de factoraje m�vil activado
			* y que reciben notificaci�n sms cuando tienen documentos publicados*/
			strSQL.append(" SELECT ic_pyme FROM com_promo_pyme WHERE cs_activado = ? AND (cs_momento_envio = ? OR cs_momento_envio = ?) ORDER BY ic_pyme");

			varBind.add("S");
			varBind.add("P");
			varBind.add("A");

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);

			rst1 = pst1.executeQuery();

			while(rst1.next()){
				String icPyme = rst1.getString(1);

				String numeroCelular = "";
				String tipoInformacion = "";
				String numeroDocumentos = "";
				String montoDocumentos = "";

				/*Se obtiene el numero celular que tiene registrado la pyme*/
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cg_celular FROM com_contacto WHERE ic_pyme = ? AND cs_primer_contacto = ?");

				varBind.add(new Long(icPyme));
				varBind.add("S");

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				PreparedStatement pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				ResultSet rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    numeroCelular = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				/*Se obtiene la parametrizaci�n del tipo de informaci�n que la pyme desea recibir*/
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cs_info_envio FROM com_promo_pyme WHERE ic_pyme = ?");

				varBind.add(new Long(icPyme));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    tipoInformacion = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				/*A continuaci�n se obtienen el n�mero y monto de documentos publicados a la pyme*/
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT COUNT(ic_documento)");
				strSQL.append(" FROM com_documento");
				strSQL.append(" WHERE ic_pyme = ?");
				strSQL.append(" AND ic_estatus_docto = ?");

				varBind.add(new Long(icPyme));
				varBind.add(new Integer(2));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    numeroDocumentos = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT SUM(fn_monto)");
				strSQL.append(" FROM com_documento");
				strSQL.append(" WHERE ic_pyme = ?");
				strSQL.append(" AND ic_estatus_docto = ?");

				varBind.add(new Long(icPyme));
				varBind.add(new Long(2));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    montoDocumentos = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				montoDocumentos = Comunes.formatoDecimal(montoDocumentos, 2);
				montoDocumentos = montoDocumentos.substring(0, montoDocumentos.lastIndexOf("."));

				if(tipoInformacion.equals("N")){/*Si la informacion del mensaje solo contiene el n�mero de documentos publicados*/
					/*Valida que el numero de documentos no sea cero para no enviar mensaje*/
					if(!numeroDocumentos.equals("0")){
                        try {
						flag_envio = true;
						sms.enviarNumeroDocumentosPublicados(numeroCelular, numeroDocumentos);
                        } catch(Exception e) {
                            flag_envio = false;
                            log.error("ServiciosBean::enviaSMSFactorajeMovilPublicacion(Exception):"+e.getMessage());
                            e.printStackTrace();
					}
					}
				}else if(tipoInformacion.equals("M")){/*Si la informacion del mensaje solo contiene el monto de los documentos publicados*/
					/*Valida que el monto de los documentos no sea cero para no enviar mensaje*/
					if(!montoDocumentos.equals("0")){
					    try {
						flag_envio = true;
						sms.enviarMontoDocumentosPublicados(numeroCelular, montoDocumentos);
					    } catch(Exception e) {
					        flag_envio = false;
					        log.error("ServiciosBean::enviaSMSFactorajeMovilPublicacion(Exception):"+e.getMessage());
					        e.printStackTrace();
					}
					}
				}else{/*Si la informacion del mensaje contiene el n�mero y el monto de los documentos publicados*/
					/*Valida que el monto y el numero de documentos no sea cero para no enviar mensaje*/
					if(!numeroDocumentos.equals("0") && !montoDocumentos.equals("0")){
					    try {
						flag_envio = true;
						sms.enviarNumeroMontoDoctosPublicados(numeroCelular, numeroDocumentos, montoDocumentos);
					    } catch(Exception e) {
					        flag_envio = false;
					        log.error("ServiciosBean::enviaSMSFactorajeMovilPublicacion(Exception):"+e.getMessage());
					        e.printStackTrace();
					}
				}
				}
				if(flag_envio){
					strSQL = new StringBuffer();
					varBind = new ArrayList();
					PreparedStatement pst = null;

                    if (montoDocumentos.indexOf(",") != -1) {
                        montoDocumentos = montoDocumentos.replaceAll(",", "");
                    }

					/*Se calcula el costo por documento notificado a partir del costo por mensaje y el numero de documentos*/
                    BigDecimal costo_notificacion =
                        new BigDecimal(costo_sms).divide(new BigDecimal(numeroDocumentos), 2, BigDecimal.ROUND_HALF_UP);

					/*Se inserta en la bitacora el detalle del documento, el monto y el costo de la notificaci�n en la bitacora*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO bit_promo_pyme (");
					strSQL.append(" ic_pyme,");
					strSQL.append(" ic_documento,");
					strSQL.append(" cg_estatus,");
					strSQL.append(" fn_monto_notificado,");
					strSQL.append(" fn_costo_mensaje)");
					strSQL.append(" SELECT ic_pyme, ic_documento, ?, fn_monto, ?");
					strSQL.append(" FROM com_documento");
					strSQL.append(" WHERE ic_pyme = ?");
					strSQL.append(" AND ic_estatus_docto = ?");

					varBind.add("N");
					varBind.add(new Double(costo_notificacion.toPlainString()));
					varBind.add(new Long(icPyme));
					varBind.add(new Long(2));

					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					flag_envio = false;
				}
			}

			rst1.close();
			pst1.close();
		}catch(Exception e){
			trans_op = false;
			log.info("enviaSMSFactorajeMovilPublicacion(ERROR) ::..");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("enviaSMSFactorajeMovilPublicacion(F) ::..");
	}

/**
 * Este m�todo se invoca desde el proceso que se encarga de notificar a las pymes via
 * SMS de los documentos que tiene publicados. La informaci�n que se env�a en cada
 * mensaje depende de la parametrizaci�n que hizo cada pyme al momento de activar el servicio.
 *
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public void enviaSMSFactorajeMovilVencimiento() throws NafinException{
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		SMS sms = new SMS();
		PreparedStatement	pst1 = null;
		ResultSet rst1	= null;
		boolean trans_op = true;
		boolean flag_envio = false;

		log.info("enviaSMSFactorajeMovilVencimiento(I) ::..");

		try{
			con.conexionDB();
			/*Se obtiene el costo por mensaje sms*/
			String costo_sms = sms.getCostoSMS();

			/* Se obtienen las claves de las pymes que tienen el servicio de factoraje m�vil activado
			* y que reciben notificaci�n sms cuando tienen documentos publicados*/
			strSQL.append(" SELECT ic_pyme FROM com_promo_pyme WHERE cs_activado = ? AND (cs_momento_envio = ? OR cs_momento_envio = ?) ORDER BY ic_pyme");

			varBind.add("S");
			varBind.add("V");
			varBind.add("A");

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);

			rst1 = pst1.executeQuery();

			while(rst1.next()){
				String icPyme = rst1.getString(1);
				String numeroCelular = "";
				String tipoInformacion = "";
				String fechaActual = "";
				String fechaVencimiento = "";
				String numeroDocumentos = "";
				String montoDocumentos = "";

				/*Se obtiene el numero celular que tiene registrado la pyme*/
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cg_celular FROM com_contacto WHERE ic_pyme = ? AND cs_primer_contacto = ?");

				varBind.add(new Long(icPyme));
				varBind.add("S");

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				PreparedStatement pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				ResultSet rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    numeroCelular = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				/*Se obtiene la fecha del dia a partir de la cual se cuentan los dias habiles previos al vencimiento*/
				strSQL = new StringBuffer();

				strSQL.append(" SELECT TO_CHAR(SYSDATE, 'DD/MM/YYYY') FROM DUAL");

				log.debug("..:: strSQL : "+strSQL);

				pst2 = con.queryPrecompilado(strSQL.toString());

				rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    fechaActual = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				int diasPrevios = Integer.parseInt(this.getDiasPreviosVencimiento());

				fechaVencimiento = Fecha.sumaFechaDiasHabiles(fechaActual, "dd/MM/yyyy", diasPrevios);

				log.debug("..:: Fecha de Vencimiento : " + fechaVencimiento);

				/*Se obtiene la parametrizaci�n del tipo de informaci�n que la pyme desea recibir*/
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cs_info_envio FROM com_promo_pyme WHERE ic_pyme = ?");

				varBind.add(new Long(icPyme));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    tipoInformacion = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				/*Se obtiene el n�mero y monto de los documentos que por vencer que tiene la pyme*/
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT COUNT(ic_documento)");
				strSQL.append(" FROM com_documento");
				strSQL.append(" WHERE ic_pyme = ?");
				strSQL.append(" AND ic_estatus_docto = ?");
				strSQL.append(" AND df_fecha_venc = TO_DATE(?, 'DD/MM/YYYY')");

				varBind.add(new Long(icPyme));
				varBind.add(new Integer(2));
				varBind.add(fechaVencimiento);

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    numeroDocumentos = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT SUM(fn_monto)");
				strSQL.append(" FROM com_documento");
				strSQL.append(" WHERE ic_pyme = ?");
				strSQL.append(" AND ic_estatus_docto = ?");
				strSQL.append(" AND df_fecha_venc = TO_DATE(?, 'DD/MM/YYYY')");

				varBind.add(new Long(icPyme));
				varBind.add(new Integer(2));
				varBind.add(fechaVencimiento);

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    montoDocumentos = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				montoDocumentos = Comunes.formatoDecimal(montoDocumentos, 2);
				montoDocumentos = montoDocumentos.substring(0, montoDocumentos.lastIndexOf("."));

				if(tipoInformacion.equals("N")){/*Si la informacion del mensaje solo contiene el n�mero de documentos por vencer*/
					/*Valida que el numero de documentos no sea cero para no enviar mensaje*/
					if(!numeroDocumentos.equals("0")){
					    try {
						flag_envio = true;
						sms.enviarNumeroDoctosPorVencer(numeroCelular, numeroDocumentos);
					    } catch(Exception e) {
					        flag_envio = false;
					        log.error("ServiciosBean::enviaSMSFactorajeMovilVencimiento(Exception):"+e.getMessage());
					        e.printStackTrace();
					}
					}
				}else if(tipoInformacion.equals("M")){/*Si la informacion del mensaje solo contiene el monto de los documentos por vencer*/
					/*Valida que el monto de los documentos no sea cero para no enviar mensaje*/
					if(!montoDocumentos.equals("0")){
					    try {
						flag_envio = true;
						sms.enviarMontoDoctosPorVencer(numeroCelular, montoDocumentos);
					    } catch(Exception e) {
					        flag_envio = false;
					        log.error("ServiciosBean::enviaSMSFactorajeMovilVencimiento(Exception):"+e.getMessage());
					        e.printStackTrace();
					}
					}
				}else{/*Si la informacion del mensaje contiene el n�mero y el monto de los documentos por vencer*/
					/*Valida que el monto y el numero de documentos no sea cero para no enviar mensaje*/
					if(!numeroDocumentos.equals("0") && !montoDocumentos.equals("0")){
                        try {
						flag_envio = true;
						sms.enviarNumeroMontoDoctosPorVencer(numeroCelular, numeroDocumentos, montoDocumentos);
					    } catch(Exception e) {
					        flag_envio = false;
					        log.error("ServiciosBean::enviaSMSFactorajeMovilVencimiento(Exception):"+e.getMessage());
					        e.printStackTrace();
					}
				}
				}
				if(flag_envio){
					strSQL = new StringBuffer();
					varBind = new ArrayList();
					PreparedStatement pst = null;

                    if (montoDocumentos.indexOf(",") != -1) {
                        montoDocumentos = montoDocumentos.replaceAll(",", "");
                    }

					/*Se calcula el costo por documento notificado a partir del costo por mensaje y el numero de documentos*/
                    BigDecimal costo_notificacion =
                        new BigDecimal(costo_sms).divide(new BigDecimal(numeroDocumentos), 2, BigDecimal.ROUND_HALF_UP);

					/*Se inserta en la bitacora el detalle del documento, el monto y el costo de la notificaci�n en la bitacora*/
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO bit_promo_pyme (");
					strSQL.append(" ic_pyme,");
					strSQL.append(" ic_documento,");
					strSQL.append(" cg_estatus,");
					strSQL.append(" fn_monto_notificado,");
					strSQL.append(" fn_costo_mensaje)");
					strSQL.append(" SELECT ic_pyme, ic_documento, ?, fn_monto, ?");
					strSQL.append(" FROM com_documento");
					strSQL.append(" WHERE ic_pyme = ?");
					strSQL.append(" AND ic_estatus_docto = ?");
					strSQL.append(" AND df_fecha_venc = TO_DATE(?, 'DD/MM/YYYY')");

					varBind.add("V");
					varBind.add(new Double(costo_notificacion.toPlainString()));
					varBind.add(new Long(icPyme));
					varBind.add(new Long(2));
					varBind.add(fechaVencimiento);

					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					flag_envio = false;
				}
			}

			rst1.close();
			pst1.close();
		}catch(Exception e){
			trans_op = false;
			log.info("enviaSMSFactorajeMovilVencimiento(ERROR) ::..");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("enviaSMSFactorajeMovilVencimiento(F) ::..");
	}

/**
 * Este m�todo se llama desde el proceso que genera la bitacora global de documentos,
 * la cual guarda un condensado de la informaci�n de los documentos negociables, por vencer, operados y
 * vencidos sin operar existentes en la tabla com_documento.
 * @author Alberto Cruz Flores
 * @since FODEA 028 - 2009 Reportes Nafinsa M�vil.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public void generaBitacoraGlobalDocumentos() throws NafinException{
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		PreparedStatement	pst1 = null;
		ResultSet rst1	= null;
		boolean trans_op = true;

		log.info("generaBitacoraGlobalDocumentos(I) ::..");

		try{
			con.conexionDB();

			String num_doctos_negociables = "0";
			String monto_doctos_negociables = "0";
			String num_doctos_por_vencer = "0";
			String monto_doctos_por_vencer = "0";
			String num_doctos_operados = "0";
			String monto_doctos_operados = "0";
			String num_doctos_vencidos = "0";
			String monto_doctos_vencidos = "0";
			String fecha_actual = "";
			String fecha_vencimiento = "";

			//QUERY PARA OBTENER LOS DOCUMENTOS NEGOCIABLES EXISTENTES EN COM_DOCUMENTO
			strSQL.append(" SELECT /*+ index(docto in_com_documento_04_nuk)*/ COUNT(docto.ic_documento), NVL(SUM(docto.fn_monto), 0)");
			strSQL.append(" FROM com_documento docto");
			strSQL.append(" WHERE docto.ic_estatus_docto = ?");

			varBind.add(new Integer(2));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);

			rst1 = pst1.executeQuery();

			while(rst1.next()){
				num_doctos_negociables = rst1.getString(1)==null?"0":rst1.getString(1);
				monto_doctos_negociables = rst1.getString(2)==null?"0":rst1.getString(2);
			}

			rst1.close();
			pst1.close();

			/*Se obtiene la fecha del dia a partir de la cual se cuentan los dias habiles previos al vencimiento*/
			strSQL = new StringBuffer();

			strSQL.append(" SELECT TO_CHAR(SYSDATE, 'DD/MM/YYYY') FROM DUAL");

			log.debug("..:: strSQL : "+strSQL);

			pst1 = con.queryPrecompilado(strSQL.toString());

			rst1 = pst1.executeQuery();

            while (rst1.next()) {
                fecha_actual = rst1.getString(1);
            }

			rst1.close();
			pst1.close();

			int diasPrevios = Integer.parseInt(this.getDiasPreviosVencimiento());

			fecha_vencimiento = Fecha.sumaFechaDiasHabiles(fecha_actual, "dd/MM/yyyy", diasPrevios);

			log.debug("..:: Fecha de Vencimiento : "+fecha_vencimiento);

			//QUERY PARA OBTENER LOS DOCUMENTOS NEGOCIABLES POR VENCER EN CON_DOCUMENTO
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT COUNT(ic_documento), NVL(SUM(fn_monto), 0)");
			strSQL.append(" FROM com_documento");
			strSQL.append(" WHERE ic_estatus_docto = ?");
			strSQL.append(" AND df_fecha_venc = TO_DATE(?, 'dd/mm/yyyy')");

			varBind.add(new Integer(2));
			varBind.add(fecha_vencimiento);

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);

			rst1 = pst1.executeQuery();

			while(rst1.next()){
				num_doctos_por_vencer = rst1.getString(1)==null?"0":rst1.getString(1);
				monto_doctos_por_vencer = rst1.getString(2)==null?"0":rst1.getString(2);
			}

			rst1.close();
			pst1.close();

			//QUERY PARA OBTENER LOS DOCUMENTOS OPERADOS EN COM_DOCUMENTO
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT /*+ index(docto in_com_documento_04_nuk)*/ COUNT(docto.ic_documento), NVL(SUM(docto.fn_monto), 0)");
			strSQL.append(" FROM com_documento docto");
			strSQL.append(" WHERE docto.ic_estatus_docto IN (?, ?, ?)");

			varBind.add(new Integer(4));
			varBind.add(new Integer(11));
			varBind.add(new Integer(12));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);

			rst1 = pst1.executeQuery();

			while(rst1.next()){
				num_doctos_operados = rst1.getString(1)==null?"0":rst1.getString(1);
				monto_doctos_operados = rst1.getString(2)==null?"0":rst1.getString(2);
			}

			rst1.close();
			pst1.close();

			//QUERY PARA OBTENER LOS DOCUMENTOS VENCIDOS SIN OPERAR EN COM_DOCUMENTO
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT /*+ index(docto in_com_documento_04_nuk)*/ COUNT(docto.ic_documento), NVL(SUM(docto.fn_monto), 0)");
			strSQL.append(" FROM com_documento docto");
			strSQL.append(" WHERE docto.ic_estatus_docto = ?");

			varBind.add(new Integer(9));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);

			rst1 = pst1.executeQuery();

			while(rst1.next()){
				num_doctos_vencidos = rst1.getString(1)==null?"0":rst1.getString(1);
				monto_doctos_vencidos = rst1.getString(2)==null?"0":rst1.getString(2);
			}

			rst1.close();
			pst1.close();

			/*INSERTA EN LA TABLA BIT_DOCTOS_GLOBAL LA INFORMACION*/
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" INSERT INTO bit_doctos_global (");
			strSQL.append(" ig_numero_neg,");
			strSQL.append(" fn_monto_neg,");
			strSQL.append(" ig_numero_por_vencer,");
			strSQL.append(" fn_monto_por_vencer,");
			strSQL.append(" ig_numero_operados,");
			strSQL.append(" fn_monto_operados,");
			strSQL.append(" ig_numero_vencidos,");
			strSQL.append(" fn_monto_vencidos");
			strSQL.append(") VALUES (");
			strSQL.append(" ?, ?, ?, ?, ?, ?, ?, ?");
			strSQL.append(")");

			varBind.add(new Long(num_doctos_negociables));
			varBind.add(new Double(monto_doctos_negociables));
			varBind.add(new Long(num_doctos_por_vencer));
			varBind.add(new Double(monto_doctos_por_vencer));
			varBind.add(new Long(num_doctos_operados));
			varBind.add(new Double(monto_doctos_operados));
			varBind.add(new Long(num_doctos_vencidos));
			varBind.add(new Double(monto_doctos_vencidos));

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
			pst1.executeUpdate();
			pst1.close();
		}catch(Exception e){
			trans_op = false;
			log.info("generaBitacoraGlobalDocumentos(ERROR) ::..");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("generaBitacoraGlobalDocumentos(F) ::..");
	}

/**
 * Este m�todo se llama desde el proceso que genera la bitacora de los documentos operados
 * que fueron notificados a la pyme a traves del servicio de nafinsa m�vil.
 * @author Alberto Cruz Flores
 * @since FODEA 028 - 2009 Reportes Nafinsa M�vil.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public void generaBitDoctosOperadosNotificados() throws NafinException{
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		PreparedStatement	pst1 = null;
		ResultSet rst1	= null;
		boolean trans_op = true;

		log.info("generaBitDoctosOperadosNotificados(I) ::..");

		try{
			con.conexionDB();

			String fecha_actual = "";

			//SE OBTIENE LA FECHA ACTUAL QUE SER� EL PARAMETRO FECHA DE OPERACI�N
			strSQL.append(" SELECT TO_CHAR(SYSDATE, 'DD/MM/YYYY') FROM DUAL");

			log.debug("..:: strSQL : "+strSQL);

			pst1 = con.queryPrecompilado(strSQL.toString());

			rst1 = pst1.executeQuery();

            while (rst1.next()) {
                fecha_actual = rst1.getString(1);
            }

			rst1.close();
			pst1.close();

			//SE OBTIENEN LAS PYMES QUE TIENE EL SERVICIO DE NAFINSA MOVIL ACTIVADO
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT pyme.ic_pyme ic_pyme,");
			strSQL.append(" pyme.cg_razon_social nombre_pyme,");
			strSQL.append(" crn.ic_nafin_electronico nafin_electronico");
			strSQL.append(" FROM com_promo_pyme cpp");
			strSQL.append(", comcat_pyme pyme");
			strSQL.append(", comrel_nafin crn");
			strSQL.append(" WHERE cpp.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND cpp.ic_pyme = crn.ic_epo_pyme_if");
			strSQL.append(" AND crn.cg_tipo = ?");
			strSQL.append(" AND cpp.cs_activado = ?");

			varBind.add("P");
			varBind.add("S");

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);

			rst1 = pst1.executeQuery();

			while(rst1.next()){
				String ic_pyme = rst1.getString("ic_pyme");
				String nombre_pyme = rst1.getString("nombre_pyme");
				String nafin_electronico = rst1.getString("nafin_electronico");

				BigDecimal monto_doctos_op = new BigDecimal("0.00");
				BigDecimal interes_doctos_op = new BigDecimal("0.00");
				BigDecimal costo_notificaciones = new BigDecimal("0.00");
				int num_doctos_op = 0;
				int num_notificaciones = 0;

				//PRIMERO SE OBTIENEN LOS DOCUMENTOS OPERADOS POR LA PYME
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT /*+use_nl(sol docto cds)*/ docto.ic_documento ic_documento,");
				strSQL.append(" docto.fn_monto monto_documento,");
				strSQL.append(" sol.df_operacion fecha_operacion,");
				strSQL.append(" cds.in_importe_interes monto_intereses");
				strSQL.append(" FROM com_documento docto");
				strSQL.append(", com_solicitud sol");
				strSQL.append(", com_docto_seleccionado cds");
				strSQL.append(" WHERE docto.ic_documento = sol.ic_documento");
				strSQL.append(" AND docto.ic_documento = cds.ic_documento");
				strSQL.append(" AND docto.ic_estatus_docto IN (?, ?, ?)");
				strSQL.append(" AND docto.ic_pyme = ?");
				strSQL.append(" AND sol.df_operacion >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1");

				varBind.add(new Integer(4));
				varBind.add(new Integer(11));
				varBind.add(new Integer(12));
				varBind.add(new Long(ic_pyme));
				varBind.add(fecha_actual);
				varBind.add(fecha_actual);

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				PreparedStatement pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				ResultSet rst2 = pst2.executeQuery();

				while(rst2.next()){
					String ic_documento = rst2.getString("ic_documento");
					String monto_documento = rst2.getString("monto_documento");
					String monto_intereses = rst2.getString("monto_intereses");
					boolean elimina_registros = false;

					//Se valida si los documentos operados fueron notificados de ser asi, se obtiene el total de notificaciones que tuvo el documento y el costo de las mismas
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT COUNT(ic_documento) numero_notificacion,");
					strSQL.append(" NVL(SUM(fn_costo_mensaje), 0) costo_notificacion");
					strSQL.append(" FROM bit_promo_pyme");
					strSQL.append(" WHERE ic_pyme = ?");
					strSQL.append(" AND ic_documento = ?");

					varBind.add(new Long(ic_pyme));
					varBind.add(new Long(ic_documento));

					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind);

					PreparedStatement pst3 = con.queryPrecompilado(strSQL.toString(), varBind);

					ResultSet rst3 = pst3.executeQuery();

					//Si entra dentro del while significa que eldocumento operado fue notificado
					while(rst3.next()){
						String numero_notificacion = rst3.getString(1)==null?"0":rst3.getString(1);
						String costo_notificacion = rst3.getString(2)==null?"0":rst3.getString(2);

                        monto_doctos_op =
                            monto_doctos_op.add(new BigDecimal(monto_documento)).setScale(2, BigDecimal.ROUND_HALF_UP);
                        interes_doctos_op =
                            interes_doctos_op.add(new BigDecimal(monto_intereses)).setScale(2,
                                                                                            BigDecimal.ROUND_HALF_UP);
                        costo_notificaciones =
                            costo_notificaciones.add(new BigDecimal(costo_notificacion)).setScale(2,
                                                                                                  BigDecimal.ROUND_HALF_UP);
						num_notificaciones += Integer.parseInt(numero_notificacion);
						num_doctos_op++;
						elimina_registros = true;
					}

					rst3.close();
					pst3.close();

					//Si se encontro informaci�n en bit_promo_pyme, se eliminan los registros una vez que fueron contados y sumados.
					if(elimina_registros){
						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" DELETE bit_promo_pyme");
						strSQL.append(" WHERE ic_pyme = ?");
						strSQL.append(" AND ic_documento = ?");

						varBind.add(new Long(ic_pyme));
						varBind.add(new Long(ic_documento));

						log.debug("..:: strSQL : "+strSQL.toString());
						log.debug("..:: varBind : "+varBind);

						pst3 = con.queryPrecompilado(strSQL.toString(), varBind);
						pst3.executeUpdate();
						pst3.close();
					}
				}

				rst2.close();
				pst2.close();

				//si se encontraron documentos operados notificados, se guarda en la bitacora la informaci�n
				if(num_doctos_op != 0){
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO bit_doctos_on (");
					strSQL.append(" ic_pyme,");
					strSQL.append(" ic_nafin_electronico,");
					strSQL.append(" cg_razon_social,");
					strSQL.append(" df_fecha_operacion,");
					strSQL.append(" ig_numero_operados,");
					strSQL.append(" fn_monto_operados,");
					strSQL.append(" fg_interes_operados,");
					strSQL.append(" ig_numero_notificaciones,");
					strSQL.append(" fn_costo_notificaciones");
					strSQL.append(") VALUES (");
					strSQL.append(" ?, ?, ?, TO_DATE(?, 'dd/mm/yyyy'), ?, ?, ?, ?, ?");
					strSQL.append(")");

					varBind.add(new Long(ic_pyme));
					varBind.add(new Long(nafin_electronico));
					varBind.add(nombre_pyme);
					varBind.add(fecha_actual);
					varBind.add(new Integer(num_doctos_op));
					varBind.add(new Double(monto_doctos_op.toPlainString()));
					varBind.add(new Double(interes_doctos_op.toPlainString()));
					varBind.add(new Integer(num_notificaciones));
					varBind.add(new Double(costo_notificaciones.toPlainString()));

					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind);

					pst2 = con.queryPrecompilado(strSQL.toString(), varBind);
					pst2.executeUpdate();
					pst2.close();
				}
			}

			rst1.close();
			pst1.close();
		}catch(Exception e){
			trans_op = false;
			log.info("generaBitDoctosOperadosNotificados(ERROR) ::..");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("generaBitDoctosOperadosNotificados(F) ::..");
	}

/**
 * Este m�todo se llama desde el proceso que genera la bitacora de los documentos vencidos
 * sin operar que fueron notificados a la pyme a traves del servicio de nafinsa m�vil.
 * @author Alberto Cruz Flores
 * @since FODEA 028 - 2009 Reportes Nafinsa M�vil.
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public void generaBitDoctosVencidosNotificados() throws NafinException{
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		PreparedStatement	pst1 = null;
		ResultSet rst1	= null;
		boolean trans_op = true;

		log.info("generaBitDoctosVencidosNotificados(I) ::..");

		try{
			con.conexionDB();

			//SE OBTIENEN LAS PYMES QUE TIENE EL SERVICIO DE NAFINSA MOVIL ACTIVADO
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append(" SELECT pyme.ic_pyme ic_pyme,");
			strSQL.append(" pyme.cg_razon_social nombre_pyme,");
			strSQL.append(" crn.ic_nafin_electronico nafin_electronico");
			strSQL.append(" FROM com_promo_pyme cpp");
			strSQL.append(", comcat_pyme pyme");
			strSQL.append(", comrel_nafin crn");
			strSQL.append(" WHERE cpp.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND cpp.ic_pyme = crn.ic_epo_pyme_if");
			strSQL.append(" AND crn.cg_tipo = ?");
			strSQL.append(" AND cpp.cs_activado = ?");

			varBind.add("P");
			varBind.add("S");

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);

			rst1 = pst1.executeQuery();

			while(rst1.next()){
				String ic_pyme = rst1.getString("ic_pyme");
				String nombre_pyme = rst1.getString("nombre_pyme");
				String nafin_electronico = rst1.getString("nafin_electronico");
				String fecha_vencimiento = "";

				BigDecimal monto_doctos_venc = new BigDecimal("0.00");
				BigDecimal costo_notificaciones = new BigDecimal("0.00");
				int num_doctos_venc = 0;
				int num_notificaciones = 0;

				//PRIMERO SE OBTIENEN LOS DOCUMENTOS OPERADOS POR LA PYME
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT docto.ic_documento ic_documento,");
				strSQL.append(" docto.fn_monto monto_documento,");
				strSQL.append(" TO_CHAR(docto.df_fecha_venc, 'dd/mm/yyyy') fecha_vencimiento");
				strSQL.append(" FROM com_documento docto");
				strSQL.append(" WHERE docto.ic_estatus_docto = ?");
				strSQL.append(" AND docto.ic_pyme = ?");

				varBind.add(new Integer(9));
				varBind.add(new Long(ic_pyme));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				PreparedStatement pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				ResultSet rst2 = pst2.executeQuery();

				while(rst2.next()){
					String ic_documento = rst2.getString("ic_documento");
					String monto_documento = rst2.getString("monto_documento");
					fecha_vencimiento = rst2.getString("fecha_vencimiento");
					boolean elimina_registros = false;

					//Se valida si los documentos vencidos fueron notificados de ser asi, se obtiene el total de notificaciones que tuvo el documento y el costo de las mismas
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" SELECT COUNT(ic_documento) numero_notificacion,");
					strSQL.append(" NVL(SUM(fn_costo_mensaje), 0) costo_notificacion");
					strSQL.append(" FROM bit_promo_pyme");
					strSQL.append(" WHERE ic_pyme = ?");
					strSQL.append(" AND ic_documento = ?");

					varBind.add(new Long(ic_pyme));
					varBind.add(new Long(ic_documento));

					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind);

					PreparedStatement pst3 = con.queryPrecompilado(strSQL.toString(), varBind);

					ResultSet rst3 = pst3.executeQuery();

					//Si entra dentro del while significa que eldocumento operado fue notificado
					while(rst3.next()){
						String numero_notificacion = rst3.getString(1)==null?"0":rst3.getString(1);
						String costo_notificacion = rst3.getString(2)==null?"0":rst3.getString(2);

                        monto_doctos_venc =
                            monto_doctos_venc.add(new BigDecimal(monto_documento)).setScale(2,
                                                                                            BigDecimal.ROUND_HALF_UP);
                        costo_notificaciones =
                            costo_notificaciones.add(new BigDecimal(costo_notificacion)).setScale(2,
                                                                                                  BigDecimal.ROUND_HALF_UP);
						num_notificaciones += Integer.parseInt(numero_notificacion);
						num_doctos_venc++;
						elimina_registros = true;
					}

					rst3.close();
					pst3.close();

					//Si se encontro informaci�n en bit_promo_pyme, se eliminan los registros una vez que fueron contados y sumados.
					if(elimina_registros){
						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" DELETE bit_promo_pyme");
						strSQL.append(" WHERE ic_pyme = ?");
						strSQL.append(" AND ic_documento = ?");

						varBind.add(new Long(ic_pyme));
						varBind.add(new Long(ic_documento));

						log.debug("..:: strSQL : "+strSQL.toString());
						log.debug("..:: varBind : "+varBind);

						pst3 = con.queryPrecompilado(strSQL.toString(), varBind);
						pst3.executeUpdate();
						pst3.close();
					}
				}

				rst2.close();
				pst2.close();

				//si se encontraron documentos operados notificados, se guarda en la bitacora la informaci�n
				if(num_doctos_venc != 0){
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO bit_doctos_vn (");
					strSQL.append(" ic_pyme,");
					strSQL.append(" ic_nafin_electronico,");
					strSQL.append(" cg_razon_social,");
					strSQL.append(" df_fecha_vencimiento,");
					strSQL.append(" ig_numero_vencidos,");
					strSQL.append(" fn_monto_vencidos,");
					strSQL.append(" ig_numero_notificaciones,");
					strSQL.append(" fn_costo_notificaciones");
					strSQL.append(") VALUES (");
					strSQL.append(" ?, ?, ?, TO_DATE(?, 'dd/mm/yyyy'), ?, ?, ?, ?");
					strSQL.append(")");

					varBind.add(new Long(ic_pyme));
					varBind.add(new Long(nafin_electronico));
					varBind.add(nombre_pyme);
					varBind.add(fecha_vencimiento);
					varBind.add(new Integer(num_doctos_venc));
					varBind.add(new Double(monto_doctos_venc.toPlainString()));
					varBind.add(new Integer(num_notificaciones));
					varBind.add(new Double(costo_notificaciones.toPlainString()));

					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind);

					pst2 = con.queryPrecompilado(strSQL.toString(), varBind);
					pst2.executeUpdate();
					pst2.close();
				}
			}

			rst1.close();
			pst1.close();
		}catch(Exception e){
			trans_op = false;
			log.info("generaBitDoctosVencidosNotificados(ERROR) ::..");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("generaBitDoctosVencidosNotificados(F) ::..");
	}

//FODEA 032 - 2010 ACF (I)

	/**
	 * Este m�todo se encarga de enviar el correo de notificaci�n de publicaci�n
	 * a los intermediarios financieros beneficiarios que tengan habilitado el servicio.
	 * @throws AppException Cuando ocurre alg�n error
	 */
	public void enviaCorreoBeneficiarioPublicacion(String strDirectorioPublicacion) throws AppException {
		log.info("enviaCorreoBeneficiarioPublicacion(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String strDirectorioTemp = strDirectorioPublicacion + "/00tmp/15cadenas/";
		File archivoAjunto = null;

		try {
			con.conexionDB();

			strSQL.append(" SELECT /*+use_nl(apb cif crn)*/ apb.ic_beneficiario AS ic_beneficiario,");
			strSQL.append(" cif.cg_razon_social AS nombre_beneficiario,");
			strSQL.append(" crn.ic_nafin_electronico AS nafin_electronico");
			strSQL.append(" FROM com_aviso_pub_benef apb");
			strSQL.append(", comcat_if cif");
			strSQL.append(", comrel_nafin crn");
			strSQL.append(" WHERE apb.ic_beneficiario = cif.ic_if");
			strSQL.append(" AND apb.ic_beneficiario = crn.ic_epo_pyme_if");
			strSQL.append(" AND apb.df_activacion = (SELECT MAX(df_activacion) FROM com_aviso_pub_benef WHERE ic_beneficiario = apb.ic_beneficiario)");
			strSQL.append(" AND crn.cg_tipo = ?");
			strSQL.append(" AND apb.cs_activo = ?");

			varBind.add("I");
			varBind.add("S");

			System.out.println("..:: strSQL: "+strSQL.toString());
			System.out.println("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
                String icBeneficiario =
                    rst.getString("ic_beneficiario") == null ? "" : rst.getString("ic_beneficiario");
                String nombreBeneficiario =
                    rst.getString("nombre_beneficiario") == null ? "" : rst.getString("nombre_beneficiario");
                String nafinElectronico =
                    rst.getString("nafin_electronico") == null ? "" : rst.getString("nafin_electronico");

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT COUNT(docto.ic_documento) AS doctos_publicados");
				strSQL.append(" FROM com_documento docto");
				strSQL.append(" WHERE docto.cs_dscto_especial IN (?, ?)");
				strSQL.append(" AND docto.df_alta >= TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy'), 'dd/mm/yyyy')");
				strSQL.append(" AND docto.df_alta < TO_DATE(TO_CHAR(SYSDATE + 1, 'dd/mm/yyyy'), 'dd/mm/yyyy')");
				strSQL.append(" AND docto.ic_beneficiario = ?");

				varBind.add("D");
				varBind.add("I");
				varBind.add(new Integer(icBeneficiario));

				System.out.println("..:: strSQL: "+strSQL.toString());
				System.out.println("..:: varBind: "+varBind);

				PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
				ResultSet rst1 = pst1.executeQuery();

				if (rst1.next()) {
					int documentosPublicados = rst1.getInt("doctos_publicados");
					if (documentosPublicados > 0) {
						StringBuffer cuerpoCorreo = new StringBuffer();
						String correoBeneficiario = "";
						strSQL = new StringBuffer();
						varBind = new ArrayList();

						strSQL.append(" SELECT cg_email AS email_representante FROM com_domicilio WHERE ic_if = ? AND cs_fiscal = ?");
						varBind.add(new Integer(icBeneficiario));
						varBind.add("S");

						log.debug("..:: strSQL: "+strSQL.toString());
						log.debug("..:: varBind: "+varBind);

						PreparedStatement pstEmail = con.queryPrecompilado(strSQL.toString(), varBind);
						ResultSet rstEmail = pstEmail.executeQuery();

						while (rstEmail.next()) {
                            correoBeneficiario =
                                rstEmail.getString("email_representante") == null ? "" :
                                rstEmail.getString("email_representante");
						}

						rstEmail.close();
						pstEmail.close();

						cuerpoCorreo.append("Estimado(a): " + nombreBeneficiario + "\n\n");
						cuerpoCorreo.append("El dia de hoy se registro en el SISTEMA DE CADENAS PRODUCTIVAS de Nacional Financiera, S.N.C. que una o mas EPOs dieron de alta ");
						cuerpoCorreo.append("documentos, los mismos que se anexan en el archivo PDF.\n\n");
						cuerpoCorreo.append("Para consultarlos favor de ingresar al sitio de Cadenas Productivas en http://www.nafin.com\n\n");
						cuerpoCorreo.append("Atentamente\n\n");
						cuerpoCorreo.append("Nacional Financiera, S.N.C.\n\n");

                        archivoAjunto =
                            getArchivoPubBeneficiario(strDirectorioPublicacion, icBeneficiario, nombreBeneficiario,
                                                      nafinElectronico);

                        CorreoArchivoAdjunto.AttachFile("cadenas@nafin.gob.mx", correoBeneficiario,
                                                        "Publicacion de Documentos", cuerpoCorreo.toString(),
                                                        archivoAjunto.getName(), strDirectorioTemp);
					}
				}

				rst1.close();
				pst1.close();
			}

			rst.close();
			pst.close();
		} catch (Exception e) {
			log.info("enviaCorreoBeneficiarioPublicacion(ERROR) ::..");
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("enviaCorreoBeneficiarioPublicacion(S) ::..");
		}
	}

  /**
   * Genera un archivo en el directorio temporal para ser consultado en la
	 * pantalla del expediente digitalizado de la pyme.
	 * @param icPyme Es la clave de la pyme de la que se consulta el archivo del expediente digitalizado.
   * @param icTipo Especifica el tipo de documento del expediente digitalizado que se va a descargar.
   * @param directorioTmp Es el directorio donde se genera de manera temporal el archivo.
	 * @return file El archivo obtenido de la base de datos, este archivo est� en formato PDF.
	 * @throws netropology.utilerias.AppException Al encontrar un error inesperado
   */
    private File getArchivoPubBeneficiario(String strDirectorioPublicacion, String icBeneficiario,
                                           String nombreBeneficiario, String nafinElectronico) throws AppException {
		log.info("getArchivoPubBeneficiario(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		File file = null;
		CreaArchivo archivo = new CreaArchivo();
		String strDirectorioTemp = strDirectorioPublicacion + "/00tmp/15cadenas/";
		HashMap registrosDocumentosMN = new HashMap();
		HashMap registrosDocumentosDL = new HashMap();
		BigDecimal montoDocumentosMN = new BigDecimal("0.00");
		BigDecimal montoDocumentosDL = new BigDecimal("0.00");
		int numRegistros = 0;

    try {
      con.conexionDB();
			/*SE OBTIENEN LOS DOCUMENTOS PUBLICADOS EN MONEDA NACIONAL*/
			strSQL.append(" SELECT /*+use_nl(docto epo pyme mon tfc cif bnf cmp) index(cmp cp_comcat_tipo_compra_pk)*/ epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" pyme.cg_razon_social AS nombre_pyme,");
			strSQL.append(" docto.ig_numero_docto AS numero_docto,");
			strSQL.append(" TO_CHAR(docto.df_fecha_docto, 'dd/mm/yyyy') AS fecha_emision,");
			strSQL.append(" TO_CHAR(docto.df_fecha_venc, 'dd/mm/yyyy') AS fecha_vencimiento,");
			strSQL.append(" mon.cd_nombre AS moneda,");
			strSQL.append(" tfc.cg_nombre AS tipo_factoraje,");
			strSQL.append(" docto.fn_monto AS monto_documento,");
			strSQL.append(" 100 AS porcentaje_descuento,");
			strSQL.append(" docto.fn_monto AS monto_descuento,");
			strSQL.append(" docto.ct_referencia AS referencia,");
			strSQL.append(" cif.cg_razon_social AS nombre_if,");
			strSQL.append(" bnf.cg_razon_social AS nombre_beneficiario,");
			strSQL.append(" docto.fn_porc_beneficiario AS porcentaje_beneficiario,");
			strSQL.append(" (docto.fn_monto * docto.fn_porc_beneficiario) / 100 AS monto_beneficiario,");
			strSQL.append(" TO_CHAR(docto.df_entrega, 'dd/mm/yyyy') AS df_entrega,");
			strSQL.append(" cmp.cg_descripcion AS tipo_compra,");
			strSQL.append(" docto.cg_clave_presupuestaria AS clasificador_gasto,");
			strSQL.append(" docto.cg_periodo AS plazo_maximo");
			strSQL.append(" FROM com_documento docto");
			strSQL.append(" , comcat_epo epo");
			strSQL.append(" , comcat_pyme pyme");
			strSQL.append(" , comcat_moneda mon");
			strSQL.append(" , comcat_tipo_factoraje tfc");
			strSQL.append(" , comcat_if cif");
			strSQL.append(" , comcat_if bnf");
			strSQL.append(" , comcat_tipo_compra cmp");
			strSQL.append(" WHERE docto.ic_epo = epo.ic_epo");
			strSQL.append(" AND docto.ic_pyme = pyme.ic_pyme");
			strSQL.append(" AND docto.ic_if = cif.ic_if(+)");
			strSQL.append(" AND docto.ic_beneficiario = bnf.ic_if");
			strSQL.append(" AND docto.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND docto.cs_dscto_especial = tfc.cc_tipo_factoraje");
			strSQL.append(" AND docto.cg_tipo_compra = cmp.cc_clave(+)");
			strSQL.append(" AND docto.cs_dscto_especial IN (?, ?)");
			strSQL.append(" AND docto.ic_moneda = ?");
			strSQL.append(" AND docto.df_alta >= TO_DATE(TO_CHAR(SYSDATE, 'dd/mm/yyyy'), 'dd/mm/yyyy')");
			strSQL.append(" AND docto.df_alta < TO_DATE(TO_CHAR(SYSDATE + 1, 'dd/mm/yyyy'), 'dd/mm/yyyy')");
			strSQL.append(" AND docto.ic_beneficiario = ?");

			varBind.add("D");//Distribuido
			varBind.add("I");//Vencimiento Infonavit
			varBind.add(new Integer(1));//Moneda nacional
			varBind.add(new Integer(icBeneficiario));//Clave beneficiario

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				HashMap registroDoctoMN = new HashMap();
                registroDoctoMN.put("nombreEpo",
                                    rst.getString("nombre_epo") == null ? "" : rst.getString("nombre_epo"));
                registroDoctoMN.put("nombrePyme",
                                    rst.getString("nombre_pyme") == null ? "" : rst.getString("nombre_pyme"));
                registroDoctoMN.put("numeroDocto",
                                    rst.getString("numero_docto") == null ? "" : rst.getString("numero_docto"));
                registroDoctoMN.put("fechaEmision",
                                    rst.getString("fecha_emision") == null ? "" : rst.getString("fecha_emision"));
                registroDoctoMN.put("fechaVencimiento",
                                    rst.getString("fecha_vencimiento") == null ? "" :
                                    rst.getString("fecha_vencimiento"));
				registroDoctoMN.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
                registroDoctoMN.put("tipoFactoraje",
                                    rst.getString("tipo_factoraje") == null ? "" : rst.getString("tipo_factoraje"));
                registroDoctoMN.put("montoDocumento",
                                    rst.getString("monto_documento") == null ? "0" : rst.getString("monto_documento"));
                registroDoctoMN.put("porcentajeDescuento",
                                    rst.getString("porcentaje_descuento") == null ? "0" :
                                    rst.getString("porcentaje_descuento"));
                registroDoctoMN.put("montoDescuento",
                                    rst.getString("monto_descuento") == null ? "0" : rst.getString("monto_descuento"));
                registroDoctoMN.put("referencia",
                                    rst.getString("referencia") == null ? "" : rst.getString("referencia"));
				registroDoctoMN.put("nombreIf", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
                registroDoctoMN.put("nombreBeneficiario",
                                    rst.getString("nombre_beneficiario") == null ? "" :
                                    rst.getString("nombre_beneficiario"));
                registroDoctoMN.put("porcentajeBeneficiario",
                                    rst.getString("porcentaje_beneficiario") == null ? "0" :
                                    rst.getString("porcentaje_beneficiario"));
                registroDoctoMN.put("montoBeneficiario",
                                    rst.getString("monto_beneficiario") == null ? "0" :
                                    rst.getString("monto_beneficiario"));
                registroDoctoMN.put("fechaEntrega",
                                    rst.getString("df_entrega") == null ? "" : rst.getString("df_entrega"));
                registroDoctoMN.put("tipoCompra",
                                    rst.getString("tipo_compra") == null ? "" : rst.getString("tipo_compra"));
                registroDoctoMN.put("clasificadorGasto",
                                    rst.getString("clasificador_gasto") == null ? "" :
                                    rst.getString("clasificador_gasto"));
                registroDoctoMN.put("plazoMaximo",
                                    rst.getString("plazo_maximo") == null ? "" : rst.getString("plazo_maximo"));
				registrosDocumentosMN.put("registroDoctoMN" + numRegistros, registroDoctoMN);
				numRegistros++;
			}

			registrosDocumentosMN.put("numRegistrosMN", Integer.toString(numRegistros));

			rst.close();
			pst.close();

			/*SE OBTIENEN LOS DOCUMENTOS PUBLICADOS EN DOLARES AMERICANOS*/
			numRegistros = 0;
			varBind = new ArrayList();
			varBind.add("D");//Distribuido
			varBind.add("I");//Vencimiento Infonavit
			varBind.add(new Integer(54));//Dolares americanos
			varBind.add(new Integer(icBeneficiario));//Clave beneficiario

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			while (rst.next()) {
				HashMap registroDoctoDL = new HashMap();
                registroDoctoDL.put("nombreEpo",
                                    rst.getString("nombre_epo") == null ? "" : rst.getString("nombre_epo"));
                registroDoctoDL.put("nombrePyme",
                                    rst.getString("nombre_pyme") == null ? "" : rst.getString("nombre_pyme"));
                registroDoctoDL.put("numeroDocto",
                                    rst.getString("numero_docto") == null ? "" : rst.getString("numero_docto"));
                registroDoctoDL.put("fechaEmision",
                                    rst.getString("fecha_emision") == null ? "" : rst.getString("fecha_emision"));
                registroDoctoDL.put("fechaVencimiento",
                                    rst.getString("fecha_vencimiento") == null ? "" :
                                    rst.getString("fecha_vencimiento"));
				registroDoctoDL.put("moneda", rst.getString("moneda")==null?"":rst.getString("moneda"));
                registroDoctoDL.put("tipoFactoraje",
                                    rst.getString("tipo_factoraje") == null ? "" : rst.getString("tipo_factoraje"));
                registroDoctoDL.put("montoDocumento",
                                    rst.getString("monto_documento") == null ? "0" : rst.getString("monto_documento"));
                registroDoctoDL.put("porcentajeDescuento",
                                    rst.getString("porcentaje_descuento") == null ? "0" :
                                    rst.getString("porcentaje_descuento"));
                registroDoctoDL.put("montoDescuento",
                                    rst.getString("monto_descuento") == null ? "0" : rst.getString("monto_descuento"));
                registroDoctoDL.put("referencia",
                                    rst.getString("referencia") == null ? "" : rst.getString("referencia"));
				registroDoctoDL.put("nombreIf", rst.getString("nombre_if")==null?"":rst.getString("nombre_if"));
                registroDoctoDL.put("nombreBeneficiario",
                                    rst.getString("nombre_beneficiario") == null ? "" :
                                    rst.getString("nombre_beneficiario"));
                registroDoctoDL.put("porcentajeBeneficiario",
                                    rst.getString("porcentaje_beneficiario") == null ? "0" :
                                    rst.getString("porcentaje_beneficiario"));
                registroDoctoDL.put("montoBeneficiario",
                                    rst.getString("monto_beneficiario") == null ? "0" :
                                    rst.getString("monto_beneficiario"));
                registroDoctoDL.put("fechaEntrega",
                                    rst.getString("df_entrega") == null ? "" : rst.getString("df_entrega"));
                registroDoctoDL.put("tipoCompra",
                                    rst.getString("tipo_compra") == null ? "" : rst.getString("tipo_compra"));
                registroDoctoDL.put("clasificadorGasto",
                                    rst.getString("clasificador_gasto") == null ? "" :
                                    rst.getString("clasificador_gasto"));
                registroDoctoDL.put("plazoMaximo",
                                    rst.getString("plazo_maximo") == null ? "" : rst.getString("plazo_maximo"));
				registrosDocumentosDL.put("registroDoctoDL" + numRegistros, registroDoctoDL);
				numRegistros++;
			}

			registrosDocumentosDL.put("numRegistrosDL", Integer.toString(numRegistros));

			rst.close();
			pst.close();
			//======================================================================== comienza la generaci�n del PDF
			String nombreArchivo = archivo.nombreArchivo() + ".pdf";

			ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioTemp + nombreArchivo, "", false, true, true);
            pdfDoc.setEncabezado("MEXICO", nafinElectronico, "", nombreBeneficiario, "", "nafin.gif",
                                 strDirectorioPublicacion, "");

			int numRegistrosMN = Integer.parseInt(registrosDocumentosMN.get("numRegistrosMN").toString());
			int numRegistrosDL = Integer.parseInt(registrosDocumentosDL.get("numRegistrosDL").toString());

			if (numRegistrosMN > 0) {
                float anchoCeldaMN[] = {
                    2.25f, 7.25f, 7.25f, 7.25f, 7.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f,
                    6.25f, 6.25f
                };
				pdfDoc.setTable(16, 100, anchoCeldaMN);

				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				pdfDoc.setCell("A", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Nombre EPO", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Nombre Proveedor", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("N�mero de Documento", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Fecha Emisi�n", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Fecha Vencimiento", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Moneda", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Tipo Factoraje", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Monto", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Porcentaje de Descuento", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Monto a Descontar", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Referencia", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Nombre IF", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Nombre Beneficiario", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Porcentaje Beneficiario", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Monto Beneficiario", "celda01", ComunesPDF.CENTER, 1);

				pdfDoc.setCell("B", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Fecha de Recepci�n de Bienes y Servicios", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Tipo de Compra (procedimiento)", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Clasificador por Objeto del Gasto", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Plazo M�ximo", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("", "celda01", ComunesPDF.CENTER, 11);

				pdfDoc.setHeaders();

				for (int i = 0; i < numRegistrosMN; i++) {
					HashMap registroDoctoMN = (HashMap)registrosDocumentosMN.get("registroDoctoMN" + i);
					pdfDoc.setCell("A", "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)registroDoctoMN.get("nombreEpo"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("nombrePyme"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("numeroDocto"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("fechaEmision"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("fechaVencimiento"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("moneda"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("tipoFactoraje"), "formas", ComunesPDF.CENTER, 1);
                    pdfDoc.setCell("$" + Comunes.formatoDecimal(registroDoctoMN.get("montoDocumento"), 2), "formas",
                                   ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("porcentajeDescuento"), "formas", ComunesPDF.CENTER, 1);
                    pdfDoc.setCell("$" + Comunes.formatoDecimal(registroDoctoMN.get("montoDescuento"), 2), "formas",
                                   ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("referencia"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("nombreIf"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("nombreBeneficiario"), "formas", ComunesPDF.CENTER, 1);
                    pdfDoc.setCell((String) registroDoctoMN.get("porcentajeBeneficiario"), "formas", ComunesPDF.CENTER,
                                   1);
                    pdfDoc.setCell("$" + Comunes.formatoDecimal(registroDoctoMN.get("montoBeneficiario"), 2), "formas",
                                   ComunesPDF.CENTER, 1);

					pdfDoc.setCell("B", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("fechaEntrega"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("tipoCompra"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("clasificadorGasto"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoMN.get("plazoMaximo"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 11);

                    montoDocumentosMN =
                        montoDocumentosMN.add(new BigDecimal((String) registroDoctoMN.get("montoDocumento")));
				}

				pdfDoc.addTable();
			}

			if (numRegistrosDL > 0) {
                float anchoCeldaDL[] = {
                    2.25f, 7.25f, 7.25f, 7.25f, 7.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f, 6.25f,
                    6.25f, 6.25f
                };
				pdfDoc.setTable(16, 100, anchoCeldaDL);

				pdfDoc.addText("\n","formas",ComunesPDF.CENTER);

				pdfDoc.setCell("A", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Nombre EPO", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Nombre Proveedor", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("N�mero de Documento", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Fecha Emisi�n", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Fecha Vencimiento", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Moneda", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Tipo Factoraje", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Monto", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Porcentaje de Descuento", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Monto a Descontar", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Referencia", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Nombre IF", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Nombre Beneficiario", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Porcentaje Beneficiario", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Monto Beneficiario", "celda01", ComunesPDF.CENTER, 1);

				pdfDoc.setCell("B", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Fecha de Recepci�n de Bienes y Servicios", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Tipo de Compra (procedimiento)", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Clasificador por Objeto del Gasto", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("Plazo M�ximo", "celda01", ComunesPDF.CENTER, 1);
				pdfDoc.setCell("", "celda01", ComunesPDF.CENTER, 11);

				pdfDoc.setHeaders();

				for (int i = 0; i < numRegistrosDL; i++) {
					HashMap registroDoctoDL = (HashMap)registrosDocumentosDL.get("registroDoctoDL" + i);
					pdfDoc.setCell("A", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("nombreEpo"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("nombrePyme"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("numeroDocto"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("fechaEmision"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("fechaVencimiento"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("moneda"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("tipoFactoraje"), "formas", ComunesPDF.CENTER, 1);
                    pdfDoc.setCell("$" + Comunes.formatoDecimal(registroDoctoDL.get("montoDocumento"), 2), "formas",
                                   ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("porcentajeDescuento"), "formas", ComunesPDF.CENTER, 1);
                    pdfDoc.setCell("$" + Comunes.formatoDecimal(registroDoctoDL.get("montoDescuento"), 2), "formas",
                                   ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("referencia"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("nombreIf"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("nombreBeneficiario"), "formas", ComunesPDF.CENTER, 1);
                    pdfDoc.setCell((String) registroDoctoDL.get("porcentajeBeneficiario"), "formas", ComunesPDF.CENTER,
                                   1);
                    pdfDoc.setCell("$" + Comunes.formatoDecimal(registroDoctoDL.get("montoBeneficiario"), 2), "formas",
                                   ComunesPDF.CENTER, 1);

					pdfDoc.setCell("B", "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("fechaEntrega"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("tipoCompra"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("clasificadorGasto"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell((String)registroDoctoDL.get("plazoMaximo"), "formas", ComunesPDF.CENTER, 1);
					pdfDoc.setCell("", "formas", ComunesPDF.CENTER, 11);

                    montoDocumentosDL =
                        montoDocumentosDL.add(new BigDecimal((String) registroDoctoDL.get("montoDocumento")));
				}

				pdfDoc.addTable();
			}

			if ((numRegistrosMN > 0) || (numRegistrosDL > 0)) {
				float anchoCeldaDL[] = {33.33f, 33.33f, 33.33f};
				pdfDoc.setTable(3, 50, anchoCeldaDL);

				pdfDoc.setCell("TOTAL MONEDA NACIONAL", "formas", ComunesPDF.RIGHT, 2);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(montoDocumentosMN.setScale(2, BigDecimal.ROUND_HALF_UP), 2),
                               "celda01", ComunesPDF.RIGHT, 1);
				pdfDoc.setCell("TOTAL DOCUMENTOS EN MONEDA NACIONAL", "formas", ComunesPDF.RIGHT, 2);
				pdfDoc.setCell(Integer.toString(numRegistrosMN), "formas", ComunesPDF.RIGHT, 1);
				pdfDoc.setCell("TOTAL MONEDA DOLAR", "formas", ComunesPDF.RIGHT, 2);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(montoDocumentosDL.setScale(2, BigDecimal.ROUND_HALF_UP), 2),
                               "celda01", ComunesPDF.RIGHT, 1);
				pdfDoc.setCell("TOTAL DOCUMENTOS EN MONEDA DOLAR", "formas", ComunesPDF.RIGHT, 2);
				pdfDoc.setCell(Integer.toString(numRegistrosDL), "formas", ComunesPDF.RIGHT, 1);

				pdfDoc.addTable();
			}

			pdfDoc.endDocument();

			file = new File(strDirectorioTemp + nombreArchivo);
    }catch(Exception e){
			log.info("getArchivoPubBeneficiario(ERROR) ::..");
			e.printStackTrace();
    }finally{
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getArchivoPubBeneficiario(S) ::..");
    }
		return file;
  }
//FODEA 032 - 2010 ACF (I)

    public void modificaEncuestaGral(String cveEncuesta, String titulo, String fec_ini, String fec_fin,
                                     String instrucciones, List preguntas, List tipoRespuesta, List numOpciones,
                                     List lstRespPreg, String obligatoria, String dias, List cverubro,
                                     List cvenumeracion) throws NafinException {
		System.out.println("Entrando a metodo 'modificaEncuesta() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String SqlEncuesta = "";
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();

			System.out.println("dias   "+dias);
			if(dias.equals("")){
				dias = "0";
			}
            SqlEncuesta =
                " UPDATE com_encuesta " + " SET cg_titulo = ? , " + " df_ini_encuesta = TO_DATE(? ,'DD/MM/YYYY'), " +
                " df_fin_encuesta = TO_DATE(?,'DD/MM/YYYY'), " + " tt_instrucciones =  '" +
                instrucciones.replace('\'', ' ') + "', " + " CS_OBLIGATORIO = '" + obligatoria + "', " +
                " IC_DIASPRE = ? " + " where ic_encuesta = ? ";

			//Clob clob = null;
			//clob.setString(1,instrucciones.replace('\'',' '));
			//SE ACTUALIZA INFO DE LA ENCUESTA

			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setString(1,titulo);
			ps.setString(2,fec_ini);
			ps.setString(3,fec_fin);
			//ps.setClob(4,clob);
			ps.setInt(4,Integer.parseInt(dias));
			ps.setLong(5,Long.parseLong(cveEncuesta));
			ps.executeUpdate();
			ps.close();

			//SE BORRA DETALLE DE OPCIONES DE RESPUESTA PARA LAS PREGUNTAS
			SqlEncuesta = " DELETE FROM com_respuesta WHERE ic_encuesta = ? ";
			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(cveEncuesta));
			ps.executeUpdate();
			ps.close();

			//SE BORRA PREGUNTAS DE LA SUBASTA
			SqlEncuesta = " DELETE FROM com_pregunta WHERE ic_encuesta = ? ";
			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(cveEncuesta));
			ps.executeUpdate();
			ps.close();

			//preguntas[],   String tipoRespuesta[], String numOpciones[]
			String cvePregunta = "";
			for(int x=0; x<preguntas.size();x++){


				SqlEncuesta = " SELECT seq_com_pregunta.nextval FROM dual ";

				ps = conexBD.queryPrecompilado(SqlEncuesta);
				rs = ps.executeQuery();
				if(rs.next()){
					cvePregunta = rs.getString(1);
				}
				rs.close();
				ps.close();

				String cg_pregunta = (String)preguntas.get(x);
				if(cg_pregunta.length()>300){
					cg_pregunta = cg_pregunta.substring(0,300);
				}

                SqlEncuesta =
                    " INSERT INTO com_pregunta" +
							"      	(ic_pregunta, ic_encuesta, ig_num_pregunta, ic_tipo_respuesta, cg_pregunta, ig_num_opciones, IC_RUBRO, IC_NOPREGUNTA)"   +
							"     	VALUES (?, ?, ?, ?, ?, ?, ? , ?)"  ;
				ps = conexBD.queryPrecompilado(SqlEncuesta);
				ps.setInt(1, Integer.parseInt(cvePregunta));
				ps.setInt(2, Integer.parseInt(cveEncuesta));
				ps.setInt(3, x+1);
				ps.setInt(4,  Integer.parseInt((String)tipoRespuesta.get(x)));
				ps.setString(5, cg_pregunta);
				ps.setInt(6, Integer.parseInt((String)numOpciones.get(x)));
				ps.setInt(7, Integer.parseInt((String)cverubro.get(x)));
				ps.setInt(8,  Integer.parseInt((String)cvenumeracion.get(x)));
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();


				//obtengo los  respuestas
				List datosO = (ArrayList)lstRespPreg.get(x);

				if(datosO.size()>0){
					for(int p=0; p<datosO.size(); p++) {

						String  valor = (String)datosO.get(p);

                        if (valor == null || valor.equals("null")) {
                            valor = "";
                        }

						System.out.println("valor "+valor);

                        SqlEncuesta =
                            "INSERT INTO com_respuesta (ic_respuesta, ic_encuesta, ic_pregunta, ic_tipo_respuesta, cg_respuesta) " +
												" VALUES (seq_com_respuesta.nextval, ?, ?, ?, ?)";

							ps = conexBD.queryPrecompilado(SqlEncuesta);
							ps.setLong(1, Long.parseLong(cveEncuesta));
							ps.setLong(2, Long.parseLong(cvePregunta));
							ps.setInt(3, Integer.parseInt((String)tipoRespuesta.get(x)));
							ps.setString(4,valor.replace('\'',' '));
							ps.executeUpdate();
							ps.clearParameters();
							ps.close();

						}

				}


			}


        } catch (SQLException errSql) {
			System.out.print("\n Error: " + errSql + "\n");
			lbSinError = false;
			throw new NafinException("SIST0001");
        } catch (Exception err) {
			System.out.print("\nSe ha producido un error en el metodo modificaEncuesta. \n");
			err.printStackTrace();
			lbSinError = false;
			throw new NafinException("SIST0001");
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}

/**
 * Este m�todo llena la informaci�n de la bitacora de documentos
 * operados que han sido notificados, negociables y por vencer con corte
 * al d�a inmediato anterior.
 * Elimina la informaci�n generada que corresponda al mes que se evalua
 * cada vez que se invoca el m�todo.
 * @author Janneth Rebeca Salda�a Waldo alias Rebos
 * @author Gilberto Aparicio
 * @since FODEA 052 - 2010 Reportes Nafinsa M�vil.
 * @param mes Mes a calcular. Si no es especificado es el mes del a�o actual
 * @param anio Anio del mes a calcular.
 */
	public void generaBitDoctosNotificados(String mes, String anio) {
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		PreparedStatement	ps = null, ps1 = null;
		ResultSet rs	= null;
		boolean trans_op = true;
		boolean periodoAnterior = false;

		log.info("generaBitDoctosNotificados(I) ::..");


		try{
				con.conexionDB();
				try {

					if (mes!=null && !mes.trim().equals("") && anio != null && !anio.trim().equals("")) {
						strSQL = new StringBuffer();
                    strSQL.append(" SELECT 'VALIDO' as fecha_valida " + " FROM DUAL " + " WHERE TO_DATE('01/" + mes +
                                  "/" + anio + "', 'dd/mm/yyyy') < TRUNC(SYSDATE -1, 'mm') ");
						ps = con.queryPrecompilado(strSQL.toString());
						rs = ps.executeQuery();
						boolean fechaParamValida = false;
						if (rs.next()) {  //Si el query regresa registro, la fecha especificada como parametro es valida para efectos de la generacion de info.
							fechaParamValida = true;
						}
						rs.close();
						ps.close();
						if (!fechaParamValida) {
							throw new Exception("La fecha especificada como parametro debe ser valida y menor al mes actual");
						}
						periodoAnterior = true;
					} else {
						periodoAnterior = false;
					}
				} catch(Exception e) {
                throw new AppException("Error en los parametros recibidos: " + e.getMessage() + "\n" + "mes=" + mes +
                                       "\n" + "anio=" + anio + "\n", e);
				}
				String condicionFechaIni = "";
				String condicionFechaFin = "";
				String fechaNotificacion = "";
				if (periodoAnterior) {	//Generacion de info de meses completos anteriores
					condicionFechaIni = " TO_DATE('01/" + mes + "/" + anio + "', 'dd/mm/yyyy') ";
					condicionFechaFin = " ADD_MONTHS( TO_DATE('01/" + mes + "/" + anio + "', 'dd/mm/yyyy'), 1) ";
					fechaNotificacion = " LAST_DAY(TO_DATE('01/" + mes + "/" + anio + "', 'dd/mm/yyyy')) ";
					log.info("generaBitDoctosNotificados() :: Generacion del Mes Completo: " + mes + "/" + anio);
				} else {
					// los SYSDATE - 1 en este metodo se deben a que la generaci�n de la info
					// se realiza al d�a anterior inmmediato.
					condicionFechaIni = " TRUNC(SYSDATE - 1, 'mm') ";
					condicionFechaFin = " TRUNC(SYSDATE - 1 ) + 1 ";
					fechaNotificacion = " SYSDATE - 1 ";
				}

				strSQL = new StringBuffer();
            strSQL.append(" DELETE FROM bit_doctos_notificados " + " WHERE df_fecha_notificacion >= " +
                          condicionFechaIni + " AND df_fecha_notificacion < " + condicionFechaFin);

				log.debug("generaBitDoctosNotificados() :: strSQL : "+strSQL.toString());

				ps = con.queryPrecompilado(strSQL.toString());
				ps.executeUpdate();
				ps.close();

				//SE INSERTAN LOS DOCUMENTOS NOTIFICADOS
				strSQL = new StringBuffer();

            strSQL.append(" INSERT INTO bit_doctos_notificados " +
						"            (ic_pyme, cg_estatus, ig_numero_doctos, fn_monto_notificado, " +
						"             df_fecha_notificacion) " +
						"   SELECT   x.ic_pyme pyme, x.cg_estatus estatus, " +
						"            COUNT (x.ic_documento) num_doctos_notif, " +
                          "            SUM (x.fn_monto_notificado) monto_notif, " + fechaNotificacion +
                          " fecha_notif " +
						"   FROM (SELECT DISTINCT ic_pyme, cg_estatus, ic_documento, fn_monto_notificado " +
                          "         FROM bit_promo_pyme " + "         WHERE df_fecha_notificacion >= " +
                          condicionFechaIni + "               AND df_fecha_notificacion < " + condicionFechaFin +
                          ") x " + "   GROUP BY x.ic_pyme, x.cg_estatus ");

				log.debug("generaBitDoctosNotificados() :: strSQL : "+strSQL.toString());
				ps = con.queryPrecompilado(strSQL.toString());
				ps.executeUpdate();
				ps.close();

				//SE BUSCAN DOCUMENTOS NOTIFICADOS OPERADOS Y SE INSERTAN EN BITACORA
				strSQL = new StringBuffer();
				varBind = new ArrayList();

            strSQL.append(" SELECT   x.ic_pyme pyme, x.cg_estatus estatus, COUNT (x.ic_documento) num_doctos_ope,  " +
						"          SUM (x.in_importe_recibir) monto_ope   " +
						" FROM (SELECT DISTINCT cd.ic_pyme, bpp.cg_estatus, cd.ic_documento,  " +
						"                           cds.in_importe_recibir  " +
						"                      FROM bit_promo_pyme bpp,  " +
						"                           com_documento cd,  " +
						"                           com_docto_seleccionado cds  " +
						"                     WHERE cd.ic_documento = bpp.ic_documento  " +
						"                       AND cd.ic_documento = cds.ic_documento  " +
						"                       AND cd.ic_estatus_docto IN (?, ?, ?) " +
						"                       AND cd.cs_dscto_especial != ? " +
						"                       AND bpp.df_fecha_notificacion >= " + condicionFechaIni +
						"                       AND bpp.df_fecha_notificacion < " + condicionFechaFin +
                          "                       ) x  " + " GROUP BY x.ic_pyme, x.cg_estatus  ");

				varBind.add(new Integer(4));
				varBind.add(new Integer(11));
				varBind.add(new Integer(12));
				varBind.add("C");

				log.debug("generaBitDoctosNotificados:: "+strSQL.toString());
				log.debug("generaBitDoctosNotificados:: varBind : "+varBind);

				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				rs = ps.executeQuery();

				while(rs.next()){
					String pyme2 = rs.getString("pyme");
					String estatus2 = rs.getString("estatus");
					String num_doctos_ope = rs.getString("num_doctos_ope");
					String monto_ope = rs.getString("monto_ope");

					//SI SE ENCUENTRAN DOCUMENTOS OPERADOS QUE HAN SIDO NOTIFICADOS SE ACTUALIZAN EN BITACORA
					strSQL = new StringBuffer();
					varBind = new ArrayList();

                strSQL.append(" UPDATE bit_doctos_notificados  " + "  SET ig_num_doctos_operados = ?,  " +
                              "        fg_monto_operado = ?  " + "  WHERE ic_pyme = ?  " + "    AND cg_estatus = ?  " +
							"    AND df_fecha_notificacion >= TRUNC(" + fechaNotificacion + ")  " +	//GEAG... no se si esto vaya o no... yo digo que si... :-)  porque si no actualizaria registros de meses pasados
							"    AND df_fecha_notificacion < TRUNC(" + fechaNotificacion + ") + 1  ");

					varBind.add(new Long(num_doctos_ope));
					varBind.add(new Double(monto_ope));
					varBind.add(new Long(pyme2));
					varBind.add(estatus2);

					log.debug("generaBitDoctosNotificados():: strSQL : "+strSQL.toString());
					log.debug("generaBitDoctosNotificados():: varBind : "+varBind);

					ps1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ps1.executeUpdate();
					ps1.close();
				}

				ps.close();
				rs.close();

				//SE BUSCAN DOCUMENTOS QUE SE HAN VENCIDO Y SE ACTUALIZA LA BITACORA
				strSQL = new StringBuffer();
				varBind = new ArrayList();

            strSQL.append(" SELECT   x.ic_pyme pyme, x.cg_estatus estatus,  " +
						"          COUNT (x.ic_documento) num_doctos_venc,  " +
						"          SUM (x.fn_monto) monto_venc  " +
						"     FROM (SELECT DISTINCT cd.ic_pyme, bpp.cg_estatus, cd.ic_documento,  " +
						"                           cd.fn_monto  " +
						"                      FROM bit_promo_pyme bpp, com_documento cd  " +
						"                     WHERE cd.ic_documento = bpp.ic_documento  " +
						"                       AND cd.ic_estatus_docto in (?,?) " +
						"                       AND bpp.df_fecha_notificacion >= " + condicionFechaIni +
						"                       AND bpp.df_fecha_notificacion < " + condicionFechaFin +
                          "                       ) x  " + " GROUP BY x.ic_pyme, x.cg_estatus ");

				varBind.add(new Integer(9));
				varBind.add(new Integer(10));

				log.debug("generaBitDoctosNotificados():: strSQL : "+strSQL.toString());
				log.debug("generaBitDoctosNotificados():: varBind : "+varBind);

				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				rs = ps.executeQuery();

				while(rs.next()){
					String pyme3 = rs.getString("pyme");
					String estatus3 = rs.getString("estatus");
					String num_doctos_venc = rs.getString("num_doctos_venc");
					String monto_venc = rs.getString("monto_venc");

					//SI SE ENCUENTRAN DOCUMENTOS VENCIDOS QUE HAN SIDO NOTIFICADOS SE ACTUALIZAN EN BITACORA
					strSQL = new StringBuffer();
					varBind = new ArrayList();

                strSQL.append(" UPDATE bit_doctos_notificados  " + "    SET ig_num_doctos_vencidos = ?,  " +
                              "        fg_monto_vencido = ?  " + "  WHERE ic_pyme = ?  " + "    AND cg_estatus = ?  " +
							"    AND df_fecha_notificacion >= TRUNC(" + fechaNotificacion + ")  " +	//GEAG... no se si esto vaya o no... yo digo que si... :-)  porque si no actualizaria registros de meses pasados
							"    AND df_fecha_notificacion < TRUNC(" + fechaNotificacion + ") + 1  ");

					varBind.add(new Long(num_doctos_venc));
					varBind.add(new Double(monto_venc));
					varBind.add(new Long(pyme3));
					varBind.add(estatus3);

					log.debug("generaBitDoctosNotificados():: strSQL : "+strSQL.toString());
					log.debug("generaBitDoctosNotificados():: varBind : "+varBind);

					ps1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ps1.executeUpdate();
					ps1.close();
				}
				rs.close();
				ps.close();

		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error en el proceso de llenado de la bitacora de documentos notificados", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("generaBitDoctosNotificados(F) ::..");
		}
	}//FODEA 052 - 2010 Rebos

/**
 * Este m�todo realizar� el corte por PyME de los documentos operados, negociables
 * y vencidos que han sido notificados hacinedo fecha de corte al dia anterior
 * inmediato.
 * Se elimina la dependecia de orden de ejecuci�n con el proceso generaBitDoctosNotificados
 * @author Janneth Rebeca Salda�a Waldo alias Rebos
 * @author Gilberto Aparicio
 * @since FODEA 052 - 2010 Reportes Nafinsa M�vil.
 * @param mes Mes a calcular de periodos anteriores, si viene vacio calcula el periodo que corresponde
 * @param anio A�o del mes calcular de periodos anteriores, si viene vacio calcula el que corresponde
 */
	public void generaBitCostoEIngreso(String mes, String anio){
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		PreparedStatement	ps = null, ps1 = null;
		ResultSet rs = null;
		boolean trans_op = true;
		boolean periodoAnterior = false;
		int icMensaje = 0;

		log.info("generaBitCostoEIngreso(I) ::..");
		// los SYSDATE - 1 en este metodo se deben a que la generaci�n de la info
		// se realiza al d�a anterior inmmediato.


		try{
				con.conexionDB();
				try {

					if (mes!=null && !mes.trim().equals("") && anio != null && !anio.trim().equals("")) {
						strSQL = new StringBuffer();
                    strSQL.append(" SELECT 'VALIDO' as fecha_valida " + " FROM DUAL " + " WHERE TO_DATE('01/" + mes +
                                  "/" + anio + "', 'dd/mm/yyyy') < TRUNC(SYSDATE -1, 'mm') ");
						ps = con.queryPrecompilado(strSQL.toString());
						rs = ps.executeQuery();
						boolean fechaParamValida = false;
						if (rs.next()) {  //Si el query regresa registro, la fecha especificada como parametro es valida para efectos de la generacion de info.
							fechaParamValida = true;
						}
						rs.close();
						ps.close();
						if (!fechaParamValida) {
							throw new Exception("La fecha especificada como parametro debe ser valida y menor al mes actual");
						}
						periodoAnterior = true;
					} else {
						periodoAnterior = false;
					}
				} catch(Exception e) {
                throw new AppException("Error en los parametros recibidos: " + e.getMessage() + "\n" + "mes=" + mes +
                                       "\n" + "anio=" + anio + "\n", e);
				}
				String condicionFechaIni = "";
				String condicionFechaFin = "";
				String fechaCorte = "";
				if (periodoAnterior) {	//Generacion de info de meses completos anteriores
					condicionFechaIni = " TO_DATE('01/" + mes + "/" + anio + "', 'dd/mm/yyyy') ";
					condicionFechaFin = " ADD_MONTHS( TO_DATE('01/" + mes + "/" + anio + "', 'dd/mm/yyyy'), 1) ";
					fechaCorte = " LAST_DAY(TO_DATE('01/" + mes + "/" + anio + "', 'dd/mm/yyyy')) ";
					log.info("generaBitCostoEIngreso() :: Generacion del Mes Completo: " + mes + "/" + anio);
				} else {
					// los SYSDATE - 1 en este metodo se deben a que la generaci�n de la info
					// se realiza al d�a anterior inmmediato.
					condicionFechaIni = " TRUNC(SYSDATE - 1, 'mm') ";
					condicionFechaFin = " TRUNC(SYSDATE - 1 ) + 1 ";
					fechaCorte = " SYSDATE - 1 ";
				}
				//SE ELIMINA LA INFORMACI�N DEL MES PARA GENERAR LA NUEVA
				strSQL = new StringBuffer();

            strSQL.append(" DELETE FROM bit_costo_benef  " + " WHERE df_fecha_corte >= " + condicionFechaIni +
						" AND df_fecha_corte <" + condicionFechaFin);

				log.debug("generaBitCostoEIngreso:: strSQL : " + strSQL.toString());

				ps = con.queryPrecompilado(strSQL.toString());
				ps.executeUpdate();
				ps.close();

				String SQLmensaje = " SELECT seq_bit_costo_benef.NEXTVAL FROM dual";

				ps = con.queryPrecompilado(SQLmensaje);
				rs = ps.executeQuery();
				if(rs.next())
					icMensaje = rs.getInt(1);
				ps.close();

				//SE OBTIENEN LAS PYMES NOTIFICADAS
				strSQL = new StringBuffer();

            strSQL.append(" INSERT INTO bit_costo_benef " + "            (ic_pyme, ic_notificacion, df_fecha_corte) " +
						"   SELECT DISTINCT (ic_pyme) AS pyme, ?, " + fechaCorte + " AS fecha_corte " +
                          "              FROM bit_promo_pyme " + "             WHERE df_fecha_notificacion >= " +
                          condicionFechaIni + "               AND df_fecha_notificacion < " + condicionFechaFin);

				varBind.add(new Integer(icMensaje));
				log.debug("generaBitCostoEIngreso:: strSQL : " + strSQL.toString());
				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				ps.executeUpdate();
				ps.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

				//DOCUMENTOS NOTIFICADOS QUE SE HAN OPERADO
            strSQL.append(" SELECT   x.ic_pyme pyme,  " + "                  COUNT(x.ic_documento) num_doctos_ope,  " +
						"                  SUM (x.in_importe_recibir) monto_ope,  " +
						"          SUM (x.in_importe_interes) int_doctos_ope  " +
						" FROM (SELECT DISTINCT bpp.ic_pyme, bpp.ic_documento,  " +
						"                           cds.in_importe_recibir, cds.in_importe_interes  " +
						"                      FROM bit_promo_pyme bpp,  " +
						"                           com_documento cd,  " +
						"                           com_docto_seleccionado cds  " +
						"                     WHERE bpp.ic_documento = cd.ic_documento  " +
						"                       AND cd.ic_documento = cds.ic_documento  " +
						"                       AND ic_estatus_docto IN (?, ?, ?) " +
						"                       AND bpp.df_fecha_notificacion >= " + condicionFechaIni  +
						"                       AND bpp.df_fecha_notificacion < " + condicionFechaFin  +
                          "                       ) x  " + " GROUP BY x.ic_pyme  ");
				log.debug("generaBitCostoEIngreso:: strSQL : " + strSQL.toString());
				varBind.add(new Integer(4));
				varBind.add(new Integer(11));
				varBind.add(new Integer(12));

				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				rs = ps.executeQuery();

				while(rs.next()){
					String pyme = rs.getString("pyme")==null?"0":rs.getString("pyme");
					String num_doctos_ope = rs.getString("num_doctos_ope")==null?"0":rs.getString("num_doctos_ope");
					String monto_ope = rs.getString("monto_ope")==null?"0":rs.getString("monto_ope");
					String int_doctos_ope = rs.getString("int_doctos_ope")==null?"0":rs.getString("int_doctos_ope");

					strSQL = new StringBuffer();
					varBind = new ArrayList();

                strSQL.append(" UPDATE bit_costo_benef  " + "    SET ig_num_doctos_oper = ?,  " +
                              "        fg_monto_oper = ?,  " + "        fg_int_doctos_oper = ?  " +
                              " WHERE ic_pyme = ?  " + " 	AND df_fecha_corte >= TRUNC (" + fechaCorte + ")  " +
							"  AND df_fecha_corte < TRUNC (" + fechaCorte + ") + 1 ");

					varBind.add(new Integer(num_doctos_ope));
					varBind.add(new Double(monto_ope));
					varBind.add(new Double(int_doctos_ope));
					varBind.add(new Long(pyme));

					log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
					log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

					ps1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ps1.executeUpdate();
					ps1.close();
				}

				ps.close();
				rs.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

            strSQL.append(" SELECT DISTINCT (bpp.ic_pyme) AS pyme, COUNT (bpp.ic_documento) AS num_not_ope, " +
						"   SUM (bpp.fn_costo_mensaje) AS costo_not_ope " +
                          " FROM bit_promo_pyme bpp, com_documento cd " + " WHERE bpp.ic_documento = cd.ic_documento " +
                          "    AND ic_estatus_docto IN (?, ?, ?) " + "    AND bpp.df_fecha_notificacion >= " +
                          condicionFechaIni + "    AND bpp.df_fecha_notificacion < " + condicionFechaFin +
						" GROUP BY bpp.ic_pyme ");


				varBind.add(new Integer(4));
				varBind.add(new Integer(11));
				varBind.add(new Integer(12));

				log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
				log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				rs = ps.executeQuery();

				while(rs.next()){
					String pyme = rs.getString("pyme")==null?"0":rs.getString("pyme");
					String num_not_ope = rs.getString("num_not_ope")==null?"0":rs.getString("num_not_ope");
					String costo_not_ope = rs.getString("costo_not_ope")==null?"0":rs.getString("costo_not_ope");

					strSQL = new StringBuffer();
					varBind = new ArrayList();

                strSQL.append(" UPDATE bit_costo_benef  " + "    SET ig_num_not_oper = ?,  " +
                              "        fg_costo_not_oper = ?  " + "  WHERE ic_pyme = ?  " +
							"     AND df_fecha_corte >= TRUNC(" + fechaCorte + ")  " +
							"   AND df_fecha_corte < TRUNC(" + fechaCorte + ") +1  ");

					varBind.add(new Integer(num_not_ope));
					varBind.add(new Double(costo_not_ope));
					varBind.add(new Long(pyme));

					log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
					log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

					ps1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ps1.executeUpdate();
					ps1.close();
				}

				ps.close();
				rs.close();

				//NOTIFICACIONES QUE SIGUEN NEGOCIABLES
				strSQL = new StringBuffer();
				varBind = new ArrayList();

            strSQL.append(" SELECT   x.ic_pyme pyme,  " + "        COUNT (x.ic_documento) num_doctos_neg,  " +
						"          SUM (x.fn_monto) monto_neg  " +
						"     FROM (SELECT DISTINCT bpp.ic_pyme, bpp.ic_documento, cd.fn_monto  " +
						"                      FROM bit_promo_pyme bpp, com_documento cd  " +
						"                     WHERE bpp.ic_documento = cd.ic_documento  " +
						"                       AND cd.ic_estatus_docto = ? " +
						"                       AND bpp.df_fecha_notificacion >= " + condicionFechaIni  +
						"                       AND bpp.df_fecha_notificacion < " + condicionFechaFin  +
                          "                       ) x  " + " GROUP BY x.ic_pyme  ");

				varBind.add(new Integer(2));

				log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
				log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				rs = ps.executeQuery();

				while(rs.next()){
					String pyme = rs.getString("pyme")==null?"0":rs.getString("pyme");
					String num_doctos_neg = rs.getString("num_doctos_neg")==null?"0":rs.getString("num_doctos_neg");
					String monto_neg = rs.getString("monto_neg")==null?"0":rs.getString("monto_neg");

					strSQL = new StringBuffer();
					varBind = new ArrayList();

                strSQL.append(" UPDATE bit_costo_benef  " + "    SET ig_num_doctos_neg = ?,  " +
                              "        fg_monto_neg = ?  " + "  WHERE ic_pyme = ?  " +
							"    AND df_fecha_corte >= TRUNC(" + fechaCorte + ")  " +
							"    AND df_fecha_corte < TRUNC(" + fechaCorte + ") + 1  ");

					varBind.add(new Integer(num_doctos_neg));
					varBind.add(new Double(monto_neg));
					varBind.add(new Long(pyme));

					log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
					log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

					ps1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ps1.executeUpdate();
					ps1.close();
				}

				ps.close();
				rs.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

            strSQL.append(" SELECT DISTINCT (bpp.ic_pyme) pyme,  " + "    COUNT (bpp.ic_documento) num_not_neg,  " +
						"    SUM (bpp.fn_costo_mensaje) costo_not_neg  " +
						" FROM bit_promo_pyme bpp, com_documento cd  " +
                          " WHERE bpp.ic_documento = cd.ic_documento  " + "       AND ic_estatus_docto = ? " +
						"       AND bpp.df_fecha_notificacion >= " + condicionFechaIni  +
                          "       AND bpp.df_fecha_notificacion < " + condicionFechaFin + " GROUP BY bpp.ic_pyme ");

				varBind.add(new Integer(2));

				log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
				log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				rs = ps.executeQuery();

				while(rs.next()){
					String pyme = rs.getString("pyme")==null?"0":rs.getString("pyme");
					String num_not_neg = rs.getString("num_not_neg")==null?"0":rs.getString("num_not_neg");
					String costo_not_neg = rs.getString("costo_not_neg")==null?"0":rs.getString("costo_not_neg");

					strSQL = new StringBuffer();
					varBind = new ArrayList();

                strSQL.append(" UPDATE bit_costo_benef  " + "    SET ig_num_not_neg = ?,  " +
                              "        fg_costo_not_neg = ?  " + "  WHERE ic_pyme = ?  " +
							"    AND df_fecha_corte >= TRUNC(" + fechaCorte + ")  " +
							"    AND df_fecha_corte < TRUNC(" + fechaCorte + ") + 1  ");

					varBind.add(new Integer(num_not_neg));
					varBind.add(new Double(costo_not_neg));
					varBind.add(new Long(pyme));

					log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
					log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

					ps1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ps1.executeUpdate();
					ps1.close();
				}

				ps.close();
				rs.close();

				//NOTIFICACIONES QUE SE HAN VENCIDO
				strSQL = new StringBuffer();
				varBind = new ArrayList();

            strSQL.append("SELECT   x.ic_pyme pyme,  " + "    COUNT (x.ic_documento) num_doctos_ven,  " +
						"    SUM (x.fn_monto) monto_ven  " +
						" FROM (SELECT DISTINCT bpp.ic_pyme, bpp.ic_documento, cd.fn_monto  " +
						"       FROM bit_promo_pyme bpp, com_documento cd  " +
						"       WHERE bpp.ic_documento = cd.ic_documento  " +
						"          AND cd.ic_estatus_docto in (?,?) " +
						"          AND bpp.df_fecha_notificacion >= " + condicionFechaIni  +
                          "          AND bpp.df_fecha_notificacion < " + condicionFechaFin + "      ) x  " +
						" GROUP BY x.ic_pyme  ");

				varBind.add(new Integer(9));
				varBind.add(new Integer(10));

				log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
				log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				rs = ps.executeQuery();

				while(rs.next()){
					String pyme = rs.getString("pyme")==null?"0":rs.getString("pyme");
                String num_doctos_ven = rs.getString("num_doctos_ven") == null ? "0" : rs.getString("num_doctos_ven");
                ;
					String monto_ven = rs.getString("monto_ven")==null?"0":rs.getString("monto_ven");

					strSQL = new StringBuffer();
					varBind = new ArrayList();

                strSQL.append(" UPDATE bit_costo_benef  " + "     SET ig_num_doctos_ven = ?,  " +
                              "        fg_monto_ven = ?  " + "  WHERE ic_pyme = ?  " +
							" 	   AND df_fecha_corte >= TRUNC(" + fechaCorte + ")  " +
							"     AND df_fecha_corte < TRUNC(" + fechaCorte + ") + 1  ");
					varBind.add(new Integer(num_doctos_ven));
					varBind.add(new Double(monto_ven));
					varBind.add(new Long(pyme));

					log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
					log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

					ps1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ps1.executeUpdate();
					ps1.close();
				}

				ps.close();
				rs.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();

            strSQL.append(" SELECT DISTINCT (bpp.ic_pyme) AS pyme,  " +
						"    COUNT (bpp.ic_documento) AS num_not_ven,  " +
						"    SUM (bpp.fn_costo_mensaje) AS costo_not_ven  " +
						" FROM bit_promo_pyme bpp, com_documento cd  " +
                          " WHERE bpp.ic_documento = cd.ic_documento  " + "    AND ic_estatus_docto in (?,?)  " +
						"    AND bpp.df_fecha_notificacion >= " + condicionFechaIni  +
                          "    AND bpp.df_fecha_notificacion < " + condicionFechaFin + " GROUP BY bpp.ic_pyme ");

				varBind.add(new Integer(9));
				varBind.add(new Integer(10));

				log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
				log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

				ps = con.queryPrecompilado(strSQL.toString(), varBind);
				rs = ps.executeQuery();

				while(rs.next()){
					String pyme = rs.getString("pyme")==null?"0":rs.getString("pyme");
					String num_not_ven = rs.getString("num_not_ven")==null?"0":rs.getString("num_not_ven");
					String costo_not_ven = rs.getString("costo_not_ven")==null?"0":rs.getString("costo_not_ven");

					strSQL = new StringBuffer();
					varBind = new ArrayList();

                strSQL.append(" UPDATE bit_costo_benef  " + "    SET ig_num_not_ven = ?,  " +
                              "        fg_costo_not_ven = ?  " + "  WHERE ic_pyme = ?  " +
							"     AND df_fecha_corte >= TRUNC(" + fechaCorte + ")  " +
							"     AND df_fecha_corte < TRUNC(" + fechaCorte + ") + 1  ");

					varBind.add(new Integer(num_not_ven));
					varBind.add(new Double(costo_not_ven));
					varBind.add(new Long(pyme));

					log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());
					log.debug("generaBitCostoEIngreso:: varBind : "+varBind);

					ps1 = con.queryPrecompilado(strSQL.toString(), varBind);
					ps1.executeUpdate();
					ps1.close();
				}

				ps.close();
				rs.close();

				strSQL = new StringBuffer();
				varBind = new ArrayList();
				//AL FINALIZAR ELIMINA LA BITACORA BIT_PROMO_PYME REGISTROS de mas de 365 dias de antig�edad
				//el 365 fue al feeling :-) porque en F052-2010 se supone que se deberia borrar toda la info
				//una vez que se procesaba el mes completo

				// ---- Dado que siguen saliendo observaciones, m�s vale comentar la eliminaci�n de los registros
				//----- de manera que en un caso extremo :-) se pueda volver a generar la informaci�n del reporte.
				// ---- En cuanto ya este m�s estable deber�a activarse la depuraci�n... (GEAG)
				//strSQL.append(" DELETE bit_promo_pyme WHERE df_fecha_notificacion < TRUNC(SYSDATE) - 365 ");

				//log.debug("generaBitCostoEIngreso:: strSQL : "+strSQL.toString());

				//ps = con.queryPrecompilado(strSQL.toString());
				//ps.executeUpdate();
				//ps.close();

		}catch(Exception e){
			trans_op = false;
			throw new AppException("Error en el proceso de generacion del reporte de Costo e Ingreso", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
			log.info("generaBitCostoEIngreso(F) ::..");
		}
	}//FODEA 052 - 2010 Rebos


	/**
	 * Obtiene la informaci�n de las encuestas a presentar al afiliado
	 * @param perfil Clave del perfil
	 * @param iNoCliente Clave del afiliado
	 * @param iNoEPO Clave de la epo relacionada (solo para pymes)
	 * @return
	 */
	public List  hayEncuestas( String strTipoUsuario, String iNoCliente, String iNoEPO) throws AppException{

		log.info("hayEncuestas(E)");

		AccesoDB con = new AccesoDB();

		boolean ok = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List datos = new ArrayList();
		List encuestas = new ArrayList();
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;

		PreparedStatement ps3 = null;
		ResultSet rs3 = null;
		String qry_cons = "";

		try{
			con.conexionDB();


			if(strTipoUsuario.equals("EPO") || strTipoUsuario.equals("IF") || strTipoUsuario.equals("ADMIN IF")) {
				if (strTipoUsuario.equals("IF") || strTipoUsuario.equals("ADMIN IF") ) {
                    qry_cons =
                        "SELECT   cea.ic_encuesta_afil, ce.cg_titulo,  TO_CHAR(ce.df_fin_encuesta, 'dd/mm/yyyy') AS df_fin_encuesta, " +
							"         ce.df_ini_encuesta, ce.df_publicacion, ce.ig_num_preguntas, " +
							"         dbms_lob.substr( ce.tt_instrucciones, 4000) as tt_instrucciones, ce.ic_encuesta , " +
							"        ce.CS_OBLIGATORIO, ce.IC_DIASPRE  "+
							"       , TO_char (antdiahabil (ce.df_fin_encuesta - ce.ic_diaspre, 2  ), 'dd/mm/yyyy')	 as fechaV "+
							"    FROM com_encuesta ce, comrel_encuesta_afiliado cea " +
                        "    WHERE ce.ic_encuesta = cea.ic_encuesta " + "    AND cea.cg_afiliado = 'I'      " +
                        "		 AND not exists(SELECT 1 " + "                      FROM com_contesta_respuesta " +
							"                     WHERE ic_encuesta_afil = cea.ic_encuesta_afil " +
							"                    ) "+
							" and exists( SELECT 1  FROM com_pregunta cp, com_respuesta cr , com_encuesta ce "+
                        " WHERE cea.ic_encuesta = cp.ic_encuesta " + " AND cp.ic_pregunta = cr.ic_pregunta   )  " +
                        "   AND cea.ic_if =  " + iNoCliente + "  AND  ce.df_fin_encuesta  >= trunc(sysdate)   " +
							" ORDER BY fechaV asc";

				} else if(strTipoUsuario.equals("EPO")) {
                    qry_cons =
                        "SELECT   cea.ic_encuesta_afil, ce.cg_titulo,  TO_CHAR(ce.df_fin_encuesta, 'dd/mm/yyyy') AS df_fin_encuesta, " +
							"         ce.df_ini_encuesta, ce.df_publicacion, ce.ig_num_preguntas, " +
							"         dbms_lob.substr( ce.tt_instrucciones, 4000) as tt_instrucciones, ce.ic_encuesta , " +
							"        ce.CS_OBLIGATORIO, ce.IC_DIASPRE  "+
							"       , TO_char (antdiahabil (ce.df_fin_encuesta - ce.ic_diaspre, 2  ), 'dd/mm/yyyy')	 as fechaV "+
							"    FROM com_encuesta ce, comrel_encuesta_afiliado cea " +
                        "    WHERE ce.ic_encuesta = cea.ic_encuesta " + "    AND cea.cg_afiliado = 'E'      " +
                        "		 AND not exists(SELECT 1 " + "                      FROM com_contesta_respuesta " +
							"                     WHERE ic_encuesta_afil = cea.ic_encuesta_afil " +
							"                    ) "+
							" and exists( SELECT 1  FROM com_pregunta cp, com_respuesta cr , com_encuesta ce "+
                        " WHERE cea.ic_encuesta = cp.ic_encuesta " + " AND cp.ic_pregunta = cr.ic_pregunta   )  " +
                        " AND cea.ic_epo =  " + iNoCliente + " AND  ce.df_fin_encuesta  >= trunc(sysdate)   " +
							" ORDER BY fechaV asc";
				}

				log.debug(" qry_cons "+qry_cons);

				ps = con.queryPrecompilado(qry_cons);
				rs = ps.executeQuery();


			} else if(strTipoUsuario.equals("PYME")) {
                qry_cons =
                    "SELECT   cea.ic_encuesta_afil, ce.cg_titulo, TO_CHAR(ce.df_fin_encuesta, 'dd/mm/yyyy') AS df_fin_encuesta, " +
						"         ce.df_ini_encuesta, ce.df_publicacion, ce.ig_num_preguntas, " +
						"         dbms_lob.substr( ce.tt_instrucciones, 4000) as tt_instrucciones, ce.ic_encuesta " +
						"       ,ce.CS_OBLIGATORIO, ce.IC_DIASPRE  "+
						"       , TO_char (antdiahabil (ce.df_fin_encuesta - ce.ic_diaspre, 2  ), 'dd/mm/yyyy')	as fechaV "+
						"    FROM com_encuesta ce, comrel_encuesta_afiliado cea " +
                    "   WHERE ce.ic_encuesta = cea.ic_encuesta " + "     AND cea.cg_afiliado = 'P' " +
                    "	AND not exists(SELECT 1 " + "                 FROM com_contesta_respuesta " +
                    "                 WHERE ic_encuesta_afil = cea.ic_encuesta_afil " + "                ) " +
						" and exists( SELECT 1  FROM com_pregunta cp, com_respuesta cr , com_encuesta ce "+
                    " WHERE cea.ic_encuesta = cp.ic_encuesta " + " AND cp.ic_pregunta = cr.ic_pregunta   )  " +
                    "     AND cea.ic_pyme = ? " + "     AND cea.ic_epo = ? " +
						"   AND  ce.df_fin_encuesta  >= trunc(sysdate)   "+
						" ORDER BY fechaV asc";	//El orden por fechaV es importante... no mover


				log.debug(" qry_cons "+qry_cons);
				log.debug(" iNoCliente "+iNoCliente);
				log.debug(" iNoEPO "+iNoEPO);

				ps = con.queryPrecompilado(qry_cons);
				ps.setLong(1,Long.parseLong(iNoCliente));
				ps.setLong(2,Long.parseLong(iNoEPO));
				//ps.setLong(3,Long.parseLong(iNoCliente));
				//ps.setLong(4,Long.parseLong(iNoEPO));
				rs = ps.executeQuery();
			}
			//out.print(qry_cons);


			// esto es  para obtener el Numero de Preguntas sin contar los opciones de Rubro
			String qry_consT =
                " SELECT count(*) total  " + "  FROM com_pregunta " + "  WHERE ic_tipo_respuesta != 4 " +
					" AND ic_encuesta = ?  ";
			ps2 = con.queryPrecompilado(qry_consT);

			String strSQL =
						" SELECT fn_num_dias_habiles( TRUNC(sysdate), TO_DATE(?, 'dd/mm/yyyy') ) AS num_dia_habiles " +
						" FROM dual ";
			PreparedStatement psNumHabiles = con.queryPrecompilado(strSQL);

            String qry_consF =
                "	SELECT TO_CHAR( ANTDIAHABIL( TRUNC (TO_DATE(?,'dd/mm/yyyy') - ?) ,2) ,'dd/mm/yyyy') as fechaV FROM DUAL";

			ps3 = con.queryPrecompilado(qry_consF);

			while(rs.next()){
				ps2.clearParameters();
				ps3.clearParameters();
				psNumHabiles.clearParameters();

				datos = new ArrayList();

				String encuesta = rs.getString("ic_encuesta");
				String fechaFinal = rs.getString("df_fin_encuesta");
				String dias  = (rs.getString("IC_DIASPRE")==null)?"0":rs.getString("IC_DIASPRE");
				String obligatorio   =  (rs.getString("CS_OBLIGATORIO")==null)?"N":rs.getString("CS_OBLIGATORIO");
				datos.add(rs.getString("ic_encuesta_afil"));
				datos.add(rs.getString("cg_titulo"));


				ps2.setLong(1,Long.parseLong(encuesta));
				rs2 = ps2.executeQuery();
				if(rs2.next()){
					datos.add(rs2.getString("total"));
				}
				rs2.close();

				if(!dias.equals("0") && obligatorio.equals("S")){
					ps3.setString(1, fechaFinal);
					ps3.setInt(2, Integer.parseInt(dias));
					rs3 = ps3.executeQuery();
					if(rs3.next()){
						datos.add(rs3.getString("fechaV"));
					}
					rs3.close();

				}else{
					datos.add(rs.getString("df_fin_encuesta"));
				}

				datos.add(rs.getString("tt_instrucciones"));
				datos.add( (rs.getString("CS_OBLIGATORIO")==null)?"N":rs.getString("CS_OBLIGATORIO"));

				psNumHabiles.setString(1, (String)datos.get(3));	//Fecha Limite de la encuesta
				ResultSet rsNumHabiles = psNumHabiles.executeQuery();
				rsNumHabiles.next();
				datos.add( new Integer(rsNumHabiles.getInt("num_dia_habiles")) );
				rsNumHabiles.close();

				encuestas.add(datos);
			} //fin while
			psNumHabiles.close();
			ps2.close();
			ps3.close();
			rs.close();
			ps.close();


			return encuestas;

		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new AppException("Error consultar si hay Encuestas parametrizadas para el Usuario ",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("hayEncuestas(S)");
		}
	}

	/**
	 * Obtiene la informaci�n de las encuestas a presentar al afiliado
	 * Solo presenta mensajes para afiliados con perfil ADMIN PYME, ADMIN IF
	 * y ADMIN EPO.
	 * @param perfil Tipo de Afiliado. E Epo, P Pyme, I If
	 * @param claveAfiliado Clave del afiliado (ic_epo, ic_pyme, ic_if)
	 * @param claveEpoRelacionada	Clave de la epo relacionada. aplica
	 * 		cuando tipoAfiliado = P
	 * @return objeto Registros con la informaci�n resumen de las encuestas
	 * 		pendientes por contestar
	 *
	 */
    public Registros obtenerEncuestas(String perfil, String claveAfiliado, String claveEpoRelacionada) {
		log.info("obtenerEncuestas(E)");

		Registros encuestas = new Registros();

		List lEncuestas = hayEncuestas(perfil, claveAfiliado, claveEpoRelacionada);
		List encabezados = new ArrayList();
		encabezados.add("IC_ENCUESTA_AFIL");
		encabezados.add("CG_TITULO");
		encabezados.add("TOTAL");
		encabezados.add("FECHA_LIMITE");
		encabezados.add("TT_INSTRUCCIONES");
		encabezados.add("CS_OBLIGATORIO");
		encabezados.add("NUM_DIA_HABILES");

		encuestas.setNombreColumnas(encabezados);
		encuestas.setData(lEncuestas);


		log.info("obtenerEncuestas(S)");
		return encuestas;
	}

	/**
	 *
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param iNoEPO
	 * @param iNoCliente
	 * @param perfil
	 */
	public int  hayEncuestasObligatoria( String perfil, String iNoCliente, String iNoEPO) throws AppException{

		log.info("hayEncuestasObligatoria(E)");

		AccesoDB con = new AccesoDB();

		boolean ok = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qry_cons = "";
		int total = 0;

		try{
			con.conexionDB();


			if(perfil.equals("ADMIN EPO") || perfil.equals("ADMIN IF")) {
				if (perfil.equals("ADMIN IF")) {
                    qry_cons =
                        "SELECT   count(*)  total  " + "    FROM com_encuesta ce, comrel_encuesta_afiliado cea " +
                        "    WHERE ce.ic_encuesta = cea.ic_encuesta " + "    AND cea.cg_afiliado = 'I'      " +
                        "		 AND not exists(SELECT 1 " + "                      FROM com_contesta_respuesta " +
							"                     WHERE ic_encuesta_afil = cea.ic_encuesta_afil " +
                        "                    ) " + "     AND cea.ic_if = ? " + " and ce.cs_obligatorio ='S' " +
                        " AND  ce.df_fin_encuesta  >= trunc(sysdate)  " + " ORDER BY  ce.df_fin_encuesta  ";

				} else if(perfil.equals("ADMIN EPO")) {
                    qry_cons =
                        "SELECT count(*)  total  " + "  FROM com_encuesta ce, comrel_encuesta_afiliado cea " +
                        " WHERE ce.ic_encuesta = cea.ic_encuesta " + "  AND cea.cg_afiliado = 'E' " +
                        "	AND not exists(SELECT 1 " + "                 FROM com_contesta_respuesta " +
                        "                 WHERE ic_encuesta_afil = cea.ic_encuesta_afil " + "                ) " +
                        "   AND cea.ic_epo = ? " + "   and ce.cs_obligatorio ='S' " +
                        " AND  ce.df_fin_encuesta  >= trunc(sysdate)  " + " ORDER BY ce.df_fin_encuesta ";
				}


				ps = con.queryPrecompilado(qry_cons);
				ps.setLong(1,Long.parseLong(iNoCliente));
				rs = ps.executeQuery();


			} else if(perfil.equals("ADMIN PYME")) {
                qry_cons =
                    "SELECT   count(*)  total  " + "    FROM com_encuesta ce, comrel_encuesta_afiliado cea " +
                    "   WHERE ce.ic_encuesta = cea.ic_encuesta " + "     AND cea.cg_afiliado = 'P' " +
                    "	AND not exists(SELECT 1 " + "                 FROM com_contesta_respuesta " +
                    "                 WHERE ic_encuesta_afil = cea.ic_encuesta_afil " + "                ) " +
                    "     AND cea.ic_pyme = ? " + "     AND cea.ic_epo = ? " + "     and ce.cs_obligatorio ='S' " +
                    " AND  ce.df_fin_encuesta  >= trunc(sysdate)  " + " ORDER BY ce.df_fin_encuesta  ";

				ps = con.queryPrecompilado(qry_cons);
				ps.setLong(1,Long.parseLong(iNoCliente));
				ps.setLong(2,Long.parseLong(iNoEPO));
				rs = ps.executeQuery();
			}
			//out.print(qry_cons);


			while(rs.next()){
				total = rs.getInt("total");
			}
			rs.close();
			ps.close();


			return total;

		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new AppException("Error  consultar si hay Encuestas Obligatorias parametrizadas para el Usuario ",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("hayEncuestasObligatoria(S)");
		}
	}


	/**
	 * Obtiene la informaci�n de las encuesta especificada
	 * @param claveEncuestaAfiliado clave de la encuesta del afiliado
	 * @return Mapa con la informacion de las encuestas
	 * 	"PREGUNTAS" objeto Registros con los datos de la encuesta
	 *    "OPCIONES" objeto Registros con los datos de las opciones de las preguntas
	 */
	public Map obtenerEncuesta(String claveEncuestaAfiliado) {
		log.info("obtenerEncuesta(E)");
		Long iClaveEncuestaAfiliado = null;
		try {
			if (claveEncuestaAfiliado == null || claveEncuestaAfiliado.equals("")) {
				throw new Exception("Los parametros son requeridos.");
			}
			iClaveEncuestaAfiliado = new Long(claveEncuestaAfiliado);
		} catch (Exception e ){
			throw new AppException("Error en los parametros recibidos", e);
		}

		AccesoDB con = new AccesoDB();

		try{
			con.conexionDB();

			String sqlP =
					" SELECT cea.ic_encuesta_afil, cp.ic_encuesta, cp.ic_pregunta, cp.cg_pregunta, " +
					"       ce.ig_num_preguntas, cp.ig_num_pregunta, cp.ic_tipo_respuesta, cp.ic_rubro " +
					"  FROM com_encuesta ce, comrel_encuesta_afiliado cea, com_pregunta cp " +
                " WHERE ce.ic_encuesta = cea.ic_encuesta " + "   AND cea.ic_encuesta = cp.ic_encuesta " +
					"	 AND cea.ic_encuesta_afil = ? "+
					" ORDER BY  cp.ig_num_pregunta ";	//El orden es importante. No MOVER
			List params = new ArrayList();
			params.add(iClaveEncuestaAfiliado);
			Registros regP = con.consultarDB(sqlP, params);

			String sqlO =
				" SELECT cr.ic_pregunta, cr.ic_respuesta, cr.ic_tipo_respuesta, cr.cg_respuesta, cr.cg_excluyentes, cr.ig_ponderacion " +
				"  FROM comrel_encuesta_afiliado cea, com_pregunta cp, com_respuesta cr " +
                " WHERE cea.ic_encuesta = cp.ic_encuesta " + "   AND cp.ic_pregunta = cr.ic_pregunta " +
				"   AND cea.ic_encuesta_afil = ? " +
				" ORDER BY  cp.ig_num_pregunta, cr.ic_respuesta  "; //El orden es importante. No MOVER
			params = new ArrayList();
			params.add(iClaveEncuestaAfiliado);
			Registros regO = con.consultarDB(sqlO, params);

			Map resultado = new HashMap();
			resultado.put("PREGUNTAS", regP);
			resultado.put("OPCIONES", regO);


			return resultado;

		}catch(Exception e){
			throw new AppException("Error obtener las preuntas y opciones de la encuesta", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("obtenerEncuesta(S)");
		}
	}

	/**
	 * Almacena las respuesta del afiliado de una encuesta en particular
	 * @param claveEncuestaAfiliado clave de la encuesta del afiliado
	 * @param strLogin Login del usuario que contesta la encuesta
	 * @param mRespuestas Map con los valores de las claves de respuesta y texto (para las respuesta libres)
	 */
    public void guardaRespuestaAfiliado(String claveEncuestaAfiliado, String strLogin, Map mRespuestas) {
		log.info("guardaRespuestaAfiliado(E)");
		boolean bOk = true;
		long iClaveEncuestaAfiliado = 0;
		try {
            if (claveEncuestaAfiliado == null || claveEncuestaAfiliado.equals("") || strLogin == null ||
                strLogin.equals("") || mRespuestas == null) {
				throw new Exception("Los parametros son requeridos.");
			}
			iClaveEncuestaAfiliado = Long.parseLong(claveEncuestaAfiliado);
		} catch (Exception e ){
			throw new AppException("Error en los parametros recibidos", e);
		}

		AccesoDB con = new AccesoDB();

		try{
			con.conexionDB();

			String sql =
					" INSERT INTO com_contesta_respuesta (ic_encuesta_afil,ic_respuesta,ic_usuario,cg_contesta_respuesta) " +
					" VALUES (?,?,?,?) ";
			PreparedStatement ps = con.queryPrecompilado(sql);


			Iterator it = mRespuestas.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry entry = (Map.Entry)it.next();

				ps.clearParameters();
				ps.setLong(1, iClaveEncuestaAfiliado);
				ps.setInt(2, Integer.parseInt((String)entry.getKey()));
				ps.setString(3, strLogin);
				ps.setString(4, (String)entry.getValue());
				ps.executeUpdate();
			}
			ps.close();
			bOk = true;
		}catch(Exception e){
			bOk = false;
			throw new AppException("Error al almacenar las respuestas del afiliado", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("guardaRespuestaAfiliado(S)");
		}
	}


	/**
	 * Obtiene los mensajes Iniciales parametrizados
	 * @param claveAfiliado Clave de Afiliado
	 * @param tipoAfiliado Tipo de Afiliado EPO Epo, IF If, PYME Pyme
	 * @param claveEpoRelacionada Clave de Epo Relacionada. Solo aplica
	 * si tipoAfiliado = PYME
	 * @param claveMensaje Si no viene especificada (ic_mensaje_ini) o viene nula, se obtienen
	 * todas los mensajes iniciales.
	 *
	 * @return objeto Registros con la informacion de los mensajes Iniciales
	 */
    public Registros obtenerMensajesIniciales(String claveAfiliado, String tipoAfiliado, String claveEpoRelacionada,
                                              String claveMensaje) {

		log.info("obtenerMensajesIniciales(E)");

		Integer iClaveAfiliado = null;
		Integer iClaveEpoRelacionada = null;
		Integer iClaveMensaje = null;
		try {
			if (claveAfiliado == null || claveAfiliado.equals("")) {
				throw new Exception("La clave de afiliado es requerida");
			}
			iClaveAfiliado = new Integer(claveAfiliado);
			if (tipoAfiliado == null || tipoAfiliado.equals("")) {
				throw new Exception("El tipo de afiliado es requerido");
			}
			if (claveEpoRelacionada !=null) {
				iClaveEpoRelacionada = new Integer(claveEpoRelacionada);
			}
			if (claveMensaje !=null && !claveMensaje.equals("")) {
				iClaveMensaje = new Integer(claveMensaje);
			}

		} catch(Exception e){
			throw new AppException("Error en los parametros recibidos", e);
		}

		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			Registros mensajes = new Registros();
			List params = new ArrayList();
			List idsMensajes = new ArrayList();
			if (iClaveMensaje != null) {
				//Si viene clave de mensaje solo trae ese... de lo contrario todos los que apliqueen
				idsMensajes.add(iClaveMensaje);
			} else {
				idsMensajes = this.getIdsMensajes(claveAfiliado, tipoAfiliado, claveEpoRelacionada);
			}

			StringBuffer sbSQL = new StringBuffer();
			if(idsMensajes.size() > 0){
                sbSQL.append("  SELECT mi.ic_mensaje_ini" + "  ,mi.cg_titulo" + "  ,mi.cg_contenido" +
                             "  ,mi.cg_imagen" + " ,NVL(mi.cg_tipo_mensaje,'M') cg_tipo_mensaje" +
                             "  FROM com_mensaje_ini mi" + "  WHERE mi.ic_mensaje_ini IN (" +
                             Comunes.repetirCadenaConSeparador("?", ",", idsMensajes.size()) + ")");
				params = new ArrayList();
				params.addAll(idsMensajes);

				if (tipoAfiliado.equals("PYME")) {
                    sbSQL.append("  AND NOT EXISTS " + " 		(SELECT emix.ic_mensaje_ini " +
							" 		FROM com_estadistica_mensaje_ini emix " +
							" 		WHERE emix.ic_mensaje_ini = mi.ic_mensaje_ini " +
							"   	AND ic_pyme = ? )");
					params.add(iClaveAfiliado);
				}
				log.debug("obtenerMensajesIniciales query=" + sbSQL);
				log.debug("obtenerMensajesIniciales params=" + params);

				mensajes = con.consultarDB(sbSQL.toString(), params);
			}
			return mensajes;

		} catch(Exception e) {
			throw new AppException("Error al obtener los mensajes iniciales" ,e);
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("obtenerMensajesIniciales(S)");
		}
	}

	/**
	 * Obtiene la lista de ids de mensajes parametrizados que deben mostrarsele
	 * al afiliado
	 * @param claveAfiliado clave del afiliado (ic_pyme, ic_epo, etc.)
	 * @param tipoAfiliado tipo de afiliado (IF, PYME, EPO)
	 * @param claveEpoRelacionada Clave de la epo relacionada
	 * @return Lista con los ids...
	 */
    public List getIdsMensajes(String claveAfiliado, String tipoAfiliado, String claveEpoRelacionada) {
		log.info("getIdsMensajes(E)");

		Integer iClaveAfiliado = null;
		Integer iClaveEpoRelacionada = null;
		try {
			if (claveAfiliado == null || claveAfiliado.equals("")) {
				throw new Exception("La clave de afiliado es requerida");
			}
			iClaveAfiliado = new Integer(claveAfiliado);
			if (tipoAfiliado == null || tipoAfiliado.equals("")) {
				throw new Exception("El tipo de afiliado es requerido");
			}
			if (claveEpoRelacionada !=null) {
				iClaveEpoRelacionada = new Integer(claveEpoRelacionada);
			}

		} catch(Exception e){
			throw new AppException("Error en los parametros recibidos", e);
		}

		List params = new ArrayList();
		List idsMensajes = new ArrayList();
		String qrySentencia = "";
		if("EPO".equals(tipoAfiliado)){
			qrySentencia	=
                "  SELECT mi.ic_mensaje_ini" + "  FROM com_mensaje_ini mi" + "  ,comrel_epo_mensaje_ini emi" +
                "  WHERE mi.ic_mensaje_ini = emi.ic_mensaje_ini" + "  AND emi.ic_epo = ? " +
				"  AND mi.cg_tipo_afiliado = ? "   +
				"  AND TRUNC(SYSDATE) BETWEEN TRUNC(mi.df_ini_mensaje) AND TRUNC(mi.df_fin_mensaje)"  ;
			params.add(iClaveAfiliado);
			params.add("E");
		}else if("IF".equals(tipoAfiliado)){
			qrySentencia	=
                "  SELECT mi.ic_mensaje_ini" + "  FROM com_mensaje_ini mi" + "  ,comrel_if_mensaje_ini emi" +
                "  WHERE mi.ic_mensaje_ini = emi.ic_mensaje_ini" + "  AND emi.ic_if = ?" +
				"  AND mi.cg_tipo_afiliado = ? "   +
				"  AND TRUNC(SYSDATE) BETWEEN TRUNC(mi.df_ini_mensaje) AND TRUNC(mi.df_fin_mensaje)"  ;
			params.add(iClaveAfiliado);
			params.add("I");
		}else if("PYME".equals(tipoAfiliado)){
			qrySentencia	=
                "  SELECT distinct mi.ic_mensaje_ini" + "  FROM com_mensaje_ini mi" + "  ,comrel_epo_mensaje_ini emi" +
                "  ,comrel_pyme_epo pe" + "  WHERE mi.ic_mensaje_ini = emi.ic_mensaje_ini" +
                "  AND emi.ic_epo = pe.ic_epo" + "  AND pe.ic_pyme = ?" + "  AND pe.cs_habilitado = ?" +
				"  AND mi.cg_tipo_afiliado = ?"   +
				"  AND TRUNC(SYSDATE) BETWEEN TRUNC(mi.df_ini_mensaje) AND TRUNC(mi.df_fin_mensaje)" +
				"  AND emi.ic_epo = ? ";
			params.add(iClaveAfiliado);
			params.add("S");
			params.add("P");
			params.add(iClaveEpoRelacionada);
		}

		log.debug("getIdsMensajes query=" + qrySentencia);
		log.debug("getIdsMensajes params=" + params);
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			Registros reg = con.consultarDB(qrySentencia, params);
			while(reg.next()) {
				idsMensajes.add(new Integer(reg.getString("ic_mensaje_ini")));
			}
			return idsMensajes;
		} catch(Exception e) {
			throw new AppException("Error al obtener los Ids de los mensajes", e);
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getIdsMensajes(S)");
		}
	}
    /*********************
    *  	CR�DITO PRODUCTIVO
    ***/

	/**
	 * Obtiene los datos a mostrar en el anuncio de Cr�dito Productivo
	 * @return Map EPO, Nombre, tel�fono y correo ejecutivo a contactar
	 * @param claveEPO Clave de la epo
	 * @param clavePyme Clave de la pyme
	 */
	 public Map getDatosCreditoProductivo(String claveEpo,String clavePyme){
		AccesoDB 		con				= new AccesoDB();
		StringBuffer 	qrySentencia;
		List				lVarBind			= null;
		Registros		registros		= null;
		Registros		registros2		= null;
		boolean			hayExito			= true;
		Map				map				= null;
		lVarBind								= new ArrayList();
		String			nomEpo			= "";
		String 			nomEjecutivo	= "";
		String 			telefono 		= "";
		String 			correoElec 		= "";
		String 			fg_monto			= "";
		try{
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT cg_razon_social FROM comcat_epo WHERE ic_epo = ?");
			lVarBind.add(new Integer(claveEpo));
			registros = con.consultarDB(qrySentencia.toString(),lVarBind,false);

			if(registros.next()){
				nomEpo = registros.getString("cg_razon_social");
			}else{
				nomEpo = "";
			}
			log.debug("qrySentencia= "+qrySentencia.toString());
			log.debug("lVarBind= "+lVarBind);

		}catch(Exception e){
			hayExito=false;
			throw new AppException("Error al obtener el nombre de la EPO", e);
		} finally{
				if(!hayExito && con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
		//OBTENER NOMBRE, TEL�FONO Y CORREO DE EJECUTIVO A CONTACTAR
			try{
				hayExito = true;
				log.debug("Nombre EPO: " + nomEpo);
				qrySentencia = new StringBuffer();
            qrySentencia.append("SELECT cg_ejecutivo_prom, cg_telefono, cg_correo" + ",fn_monto_credito" +
                                " FROM com_mensaje_pyme" + " WHERE ic_epo = ?" + " AND ic_pyme = ?" +
											" AND df_publicacion >= trunc(sysdate, 'mm') " +
											" AND df_publicacion < trunc(sysdate) + 1 ");
				lVarBind.clear();
				lVarBind.add(new Integer(claveEpo));
				lVarBind.add(new Integer(clavePyme));

				registros2 = con.consultarDB(qrySentencia.toString(),lVarBind);

				log.debug("qrySentencia Datos Ejecutivo: " + qrySentencia.toString());
				log.debug("lVarBind= "+lVarBind);

					if(registros2.next()){
                nomEjecutivo =
                    registros2.getString("cg_ejecutivo_prom") == null ? "--" :
                    registros2.getString("cg_ejecutivo_prom");
						telefono 			= registros2.getString("cg_telefono")==null?"--":registros2.getString("cg_telefono");
						correoElec 			= registros2.getString("cg_correo")==null?"--":registros2.getString("cg_correo");
                fg_monto =
                    registros2.getString("fn_monto_credito") == null ? "0" : registros2.getString("fn_monto_credito");
					}else{
						nomEjecutivo = "--";
						telefono = "--";
						correoElec = "--";
						fg_monto = "0";
					}
				log.debug("nomEjecutivo= "+nomEjecutivo);
				log.debug("telefono= "+telefono);
				log.debug("correoElec= "+correoElec);
				log.debug("fg_monto= "+fg_monto);

				map = new HashMap();
				map.put("NOMBRE_EPO",nomEpo);
				map.put("NOM_EJECUTIVO",nomEjecutivo);
				map.put("TELEFONO",telefono);
				map.put("CORREO",correoElec);
				map.put("MONTO",fg_monto);

			} catch (Exception e) {
					hayExito = false;
					throw new AppException("Error al obtener los datos", e);
			} finally {
				if(con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}
			return map;
	 }

	/**
	 * Actualiza cs_estatus_envio y cg_estatus_respuesta de com_mensaje_pyme
	 * @param estatusEnvio Estatus env�o acerca del cr�dito productivo
	 * @param estatusRespuesta Estatus de respuesta del cr�dito productivo
	 * @param claveEpo Clave de la Epo
	 * @param clavePyme Clave de la Pyme
	 */
    public void actualizaRespuestaCreditoProductivo(String estatusEnvio, String estatusRespuesta, String claveEpo,
                                                    String clavePyme) {
		log.info("actualizaRespuestaCreditoProductivo(E)");
		AccesoDB 		con					= new AccesoDB();
		List 				condiciones			= null;
		StringBuffer	qrySentencia;
							qrySentencia		= new StringBuffer();
							condiciones			= new ArrayList();
		boolean			hayExito				= true;
		try{
			con.conexionDB();
            qrySentencia.append(" UPDATE com_mensaje_pyme " + " SET cs_estatus_envio = ?, " +
                                " cg_estatus_respuesta = ?, " + " df_respuesta = sysdate " + " WHERE ic_epo = ? " +
                                " AND ic_pyme = ? " + " AND df_publicacion >= trunc(sysdate, 'mm') " +
					" AND df_publicacion < trunc(sysdate) + 1 ");
			List params = new ArrayList();
			params.add(estatusEnvio);
			params.add(estatusRespuesta);
			params.add(new Integer(claveEpo));
			params.add(new Integer(clavePyme));

			try{
				con.ejecutaUpdateDB(qrySentencia.toString(), params);
				log.debug("qrySentencia= " + qrySentencia.toString() + ":" + params);
			}catch(SQLException errorSQL){
				hayExito = false;
				throw new AppException("Error al actualizar lo datos", errorSQL);
			}
		}catch (Exception e) {
			log.error("actualizaRespuestaCreditoProductivo(Error)",e);
			hayExito = false;
			throw new AppException("Error al actualizar", e);
		} finally  {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(hayExito);
				con.cierraConexionDB();
			}
			log.info("actualizaRespuestaCreditoProductivo(S)");
		}
	}

	/**
	 * Determina si se mostrara mensaje de Credito o no
	 * @return true si es necesario mostrar el mensaje o false de lo contrario
	 * @param clavePyme clave de la Pyme (ic_pyme)
	 */
	public boolean mostrarMensajeCredito(String clavePyme, String claveEpoRelacionado) {
		log.info("mostrarMensajeCredito(E)");
		boolean mostrarMensaje = false;
		long iClavePyme = 0;
		int iClaveEpoRelacionada = 0;
		try {
			if (clavePyme == null || clavePyme.equals("")) {
				throw new Exception("La clave de la pyme es requerida");
			}
			iClavePyme = Long.parseLong(clavePyme);
			if (claveEpoRelacionado == null || claveEpoRelacionado.equals("")) {
				throw new Exception("La clave de la epo relacionada es requerida");
			}
			iClaveEpoRelacionada = Integer.parseInt(claveEpoRelacionado);
		} catch(Exception e){
			throw new AppException("Error en los parametros recibidos", e);
		}
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			String queryPropuesta =
                " SELECT count(*) " + "   FROM com_mensaje_pyme pro" + "  WHERE pro.cs_estatus_envio = 'A'" +
                "    AND pro.ic_pyme = ?" + "    AND pro.ic_epo = ?" +
                " 	  AND df_publicacion >= trunc(sysdate, 'mm') " + " 	  AND df_publicacion < trunc(sysdate) + 1 " +
					"    AND fn_monto_credito > 0 ";

			PreparedStatement pstmt = con.queryPrecompilado(queryPropuesta);
			pstmt.setLong(1, iClavePyme);
			pstmt.setInt(2, iClaveEpoRelacionada);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()) {
				mostrarMensaje = (rs.getInt(1)>0)?true:false;
			}
			rs.close();
			pstmt.close();
			return mostrarMensaje;
		} catch(Exception e) {
			throw new AppException("Error al determinar si se muestra mensaje de credito", e);
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("mostrarMensajeCredito(S)");
		}
	}


	/**
 * Este m�todo se invoca desde el proceso que se encarga de notificar a las pymes via
 * SMS cuando la pyme tiene una Oferta de Tasas Especiales para la seleccion de sus documentos.
 *
 * @throws NafinException cuando ocurre un error en la consulta.
 */
	public void enviaSMSFactorajeMovilOfertaTasas() throws NafinException{
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		SMS sms = new SMS();
		PreparedStatement	pst1 = null;
		ResultSet rst1	= null;
		boolean trans_op = true;
		boolean flag_envio = false;

		log.info("enviaSMSFactorajeMovilOfertaTasas(I) ::..");

		try{
			con.conexionDB();
			strSQL.append(" SELECT ic_pyme FROM com_promo_pyme WHERE cs_activado = ?  ORDER BY ic_pyme");

			varBind.add("S");

			log.debug("..:: strSQL : "+strSQL.toString());
			log.debug("..:: varBind : "+varBind);

			pst1 = con.queryPrecompilado(strSQL.toString(), varBind);

			rst1 = pst1.executeQuery();

			while(rst1.next()){
				String icPyme = rst1.getString(1);

				String numeroCelular = "";

				/*Se obtiene el numero celular que tiene registrado la pyme*/
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cg_celular FROM com_contacto WHERE ic_pyme = ? AND cs_primer_contacto = ?");

				varBind.add(new Long(icPyme));
				varBind.add("S");

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				PreparedStatement pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				ResultSet rst2 = pst2.executeQuery();

                while (rst2.next()) {
                    numeroCelular = rst2.getString(1);
                }

				rst2.close();
				pst2.close();

				/*Se obtiene la parametrizaci�n del tipo de informaci�n que la pyme desea recibir*/
				/*
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT cs_info_envio FROM com_promo_pyme WHERE ic_pyme = ?");

				varBind.add(new Long(icPyme));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst2 = con.queryPrecompilado(strSQL.toString(), varBind);

				rst2 = pst2.executeQuery();

				while(rst2.next()){tipoInformacion = rst2.getString(1);}

				rst2.close();
				pst2.close();
				*/
				//******

                ISeleccionDocumento seleccionDocto =
                    ServiceLocator.getInstance().lookup("SeleccionDocumentoEJB", ISeleccionDocumento.class);

				/*A continuaci�n se obtienen el n�mero y monto de documentos publicados a la pyme*/
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append("SELECT EPO.ic_epo, EPO.cg_razon_social ");
				strSQL.append(" FROM comrel_pyme_epo RPE, comcat_epo EPO ");
				strSQL.append(" WHERE RPE.ic_epo = EPO.ic_epo AND EPO.cs_habilitado = 'S' AND RPE.ic_pyme = ? ");

				varBind.add(new Long(icPyme));

				log.debug("..:: strSQL : "+strSQL.toString());
				log.debug("..:: varBind : "+varBind);

				pst2 = con.queryPrecompilado(strSQL.toString(), varBind);
				rst2 = pst2.executeQuery();

				while(rst2.next()){
					//flag_envio=false;
                    if (seleccionDocto.hayOfertaDeTasas(icPyme, "1", rst2.getString("cveEpo")) ||
                        seleccionDocto.hayOfertaDeTasas(icPyme, "54", rst2.getString("cveEpo"))) {
						flag_envio = true;
						sms.enviarNotificacionOfertaTasas(numeroCelular , rst2.getString("cg_razon_social"));
					}
				}

				rst2.close();
				pst2.close();

				/*
				if(flag_envio){
					strSQL = new StringBuffer();
					varBind = new ArrayList();
					PreparedStatement pst = null;
					ResultSet rst = null;

					if(montoDocumentos.indexOf(",") != -1){
						montoDocumentos = montoDocumentos.replaceAll(",", "");
					}

					/*Se calcula el costo por documento notificado a partir del costo por mensaje y el numero de documentos*/
					//BigDecimal costo_notificacion = new BigDecimal(costo_sms).divide(new BigDecimal(numeroDocumentos), 2, BigDecimal.ROUND_HALF_UP);

					/*Se inserta en la bitacora el detalle del documento, el monto y el costo de la notificaci�n en la bitacora*/
				/*
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO bit_promo_pyme (");
					strSQL.append(" ic_pyme,");
					strSQL.append(" ic_documento,");
					strSQL.append(" cg_estatus,");
					strSQL.append(" fn_monto_notificado,");
					strSQL.append(" fn_costo_mensaje)");
					strSQL.append(" SELECT ic_pyme, ic_documento, ?, fn_monto, ?");
					strSQL.append(" FROM com_documento");
					strSQL.append(" WHERE ic_pyme = ?");
					strSQL.append(" AND ic_estatus_docto = ?");

					varBind.add("N");
					varBind.add(new Double(costo_notificacion.toString()));
					varBind.add(new Long(icPyme));
					varBind.add(new Long(2));

					log.debug("..:: strSQL : "+strSQL.toString());
					log.debug("..:: varBind : "+varBind);

					pst = con.queryPrecompilado(strSQL.toString(), varBind);
					pst.executeUpdate();
					pst.close();

					flag_envio = false;
				}
				*/
			}

			rst1.close();
			pst1.close();
		}catch(Exception e){
			trans_op = false;
			log.info("enviaSMSFactorajeMovilOfertaTasas(ERROR) ::..");
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(trans_op);
				con.cierraConexionDB();
			}
		}
		log.info("enviaSMSFactorajeMovilOfertaTasas(F) ::..");
	}


    public String guardaEncuestaGral(HashMap DatosAltaEnc, List lstPreguntas, List lstRespuestas,
                                     String tipoAfiliado) throws AppException {
		System.out.println("ServiciosEJB::guardaEncuestaGral(E)");

		boolean				lbSinError 		= true;
		AccesoDB 			conexBD 		= new AccesoDB();
		PreparedStatement	ps;
		ResultSet			rs;
		String				SQLEncuesta;
		String				ic_encuesta = "";
		String 				sqlTipoAfil = "";
		String 				paramTipoAfil = "";

		try{
			conexBD.conexionDB();

            SQLEncuesta = " SELECT seq_com_encuesta.nextval " + "   FROM dual";
			ps = conexBD.queryPrecompilado(SQLEncuesta);
			rs = ps.executeQuery();
			if(rs.next())
				ic_encuesta = (rs.getString(1)==null)?"":rs.getString(1);
			rs.close();
			ps.close();

			//se obtien datos del alta de la encuesta
			String fp_titulo = (String)DatosAltaEnc.get("fp_titulo");
			String fp_fec_desde = (String)DatosAltaEnc.get("fp_fec_desde");
			String fp_fec_hasta = (String)DatosAltaEnc.get("fp_fec_hasta");
			String fp_instrucciones = (String)DatosAltaEnc.get("fp_instrucciones");
			String fp_cadena = (String)DatosAltaEnc.get("fp_cadena");
			//String fp_tipoAfiliado = (String)DatosAltaEnc.get("fp_tipoAfiliado");
			String fp_respObligada = (String)DatosAltaEnc.get("fp_respObligada");
			String fp_diasResp = (String)DatosAltaEnc.get("fp_diasResp");
			//String fp_numPreguntas = (String)DatosAltaEnc.get("fp_numPreguntas");
			String fp_selecAfiliados = (String)DatosAltaEnc.get("fp_selecAfiliados");

			String lsAfiliados[] = fp_selecAfiliados.split(",");

			for(int i=0;i<lsAfiliados.length;i++) {
				if(i==0){
					SQLEncuesta =
                        " INSERT INTO com_encuesta" + "             (ic_encuesta, cg_titulo," +
						"              df_ini_encuesta, df_fin_encuesta, "   +
						" 			 tt_instrucciones, ig_num_preguntas, CS_OBLIGATORIO, IC_DIASPRE)"   +
                        "      VALUES (?, ?,  " + " 	 		 TO_DATE (?, 'DD/MM/YYYY'), TO_DATE (?, 'DD/MM/YYYY'), " +
						" 			 ?, ?,?, ?)"  ;

					ps = conexBD.queryPrecompilado(SQLEncuesta);
					ps.setLong(1,Long.parseLong(ic_encuesta));
					ps.setString(2, fp_titulo);
					ps.setString(3, fp_fec_desde);
					ps.setString(4, fp_fec_hasta);
					ps.setString(5, fp_instrucciones.replace('\'',' '));
					ps.setInt(6, lstPreguntas.size());
					ps.setString(7, fp_respObligada);
					ps.setInt(8, Integer.parseInt(fp_diasResp));
					ps.executeUpdate();
					ps.clearParameters();
					ps.close();
				}


                if ("P".equals(tipoAfiliado)) {
                    sqlTipoAfil = " ic_pyme, ic_epo";
                    paramTipoAfil = " ?, ?";
                }
                if ("I".equals(tipoAfiliado)) {
                    sqlTipoAfil = " ic_if";
                    paramTipoAfil = " ?";
                }
                if ("E".equals(tipoAfiliado)) {
                    sqlTipoAfil = " ic_epo";
                    paramTipoAfil = " ?";
                }

				SQLEncuesta =
                    " INSERT INTO comrel_encuesta_afiliado" + "             (ic_encuesta_afil, ic_encuesta, " +
                    sqlTipoAfil + ",cg_afiliado	)" + "      VALUES (seq_comrel_encuesta_afiliado.nextval, ?," +
                    paramTipoAfil + " ,?)";

					int p=0;
					ps = conexBD.queryPrecompilado(SQLEncuesta);
					ps.setLong(++p, Long.parseLong(ic_encuesta));
					if("P".equals(tipoAfiliado)){
						ps.setLong(++p, Long.parseLong(lsAfiliados[i]));
						ps.setLong(++p, Long.parseLong(fp_cadena));
					}else if("I".equals(tipoAfiliado) || "E".equals(tipoAfiliado)){
						ps.setLong(++p, Long.parseLong(lsAfiliados[i]));
					}
					ps.setString(++p, tipoAfiliado);
					ps.executeUpdate();
					ps.clearParameters();
					ps.close();
			}//for(int i=0;i<lsAfiliados.length;i++)

			guardaPreguntaGral(conexBD, lstPreguntas, ic_encuesta);
			guardaRespuestaEnc(conexBD, lstRespuestas, ic_encuesta);
			lbSinError = true;

			return ic_encuesta;

		}catch(Throwable t ){
			lbSinError = false;
			throw new AppException("Error al guardar la encuesta", t);
		}finally{
			conexBD.terminaTransaccion(lbSinError);
			if(conexBD.hayConexionAbierta()){
				conexBD.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::guardaEncuestaGral(S)");
		}

	}


    private void guardaPreguntaGral(AccesoDB conexBD, List lstPreguntas, String cveEncuesta) throws AppException {
		System.out.println("ServiciosEJB::guardaPreguntaGral(E)");

		boolean				lbSinError 		= true;
		PreparedStatement	ps;
		String				SQLPregunta;
		try{

			if(lstPreguntas!=null && lstPreguntas.size()>0){

				for(int j=0;j<lstPreguntas.size();j++) {
					HashMap DatosPregEnc = (HashMap)lstPreguntas.get(j);

					String fpOrdenPreg = (String)DatosPregEnc.get("ORDENPREG");
					String fpPregunta = (String)DatosPregEnc.get("PREGUNTA");
					String fpTipoPreg = (String)DatosPregEnc.get("TIPOPREG");
					String fpTipoResp = (String)DatosPregEnc.get("TIPORESP");
					String fpNumOpc = (String)DatosPregEnc.get("NUMOPC");
					String fpNumRubro = (String)DatosPregEnc.get("NUMRUBRO");
					String fpNumPreg = (String)DatosPregEnc.get("NUMPREG");


					if(fpPregunta.length()>300){
						fpPregunta = fpPregunta.substring(0,300);
					}

					SQLPregunta =
						" INSERT INTO com_pregunta"   +
						"             (ic_pregunta, ic_encuesta, ig_num_pregunta, cs_tipo_pregunta, ic_tipo_respuesta, cg_pregunta, ig_num_opciones, ic_rubro, ic_nopregunta)"   +
						"      VALUES (seq_com_pregunta.nextval,?, ?, ?, ?, ?, ?, ?, ?)"  ;

					ps = conexBD.queryPrecompilado(SQLPregunta);
					ps.setInt(1, Integer.parseInt(cveEncuesta));
					ps.setInt(2, Integer.parseInt(fpOrdenPreg));
					ps.setString(3, fpTipoPreg);
					ps.setString(4, fpTipoResp);
					ps.setString(5, fpPregunta);
					ps.setInt(6, Integer.parseInt(fpNumOpc));
					ps.setInt(7, Integer.parseInt(fpNumRubro));
					ps.setInt(8, Integer.parseInt(fpNumPreg));

					ps.executeUpdate();
					ps.clearParameters();
					ps.close();

				}//for(int j=0;j<lsPreguntas.length;j++)
			}//end if()

		}catch(Throwable t){
			lbSinError = false;
			throw new AppException("guardaPreguntaGral(Error): Error al guardar preguntas de la encuesta", t);
        } finally {
			System.out.println("ServiciosEJB::guardaPreguntaGral(S)");
		}
	}

	private void guardaRespuestaEnc(AccesoDB con, List lstRespuestas, String cveEncuesta ) throws AppException {
		System.out.println("ServiciosEJB::guardaRespuesta(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qryUpdateResp = "";
		String qrySelect = "";

		try{
			HashMap dataRespuestas = null;

			String vNumPregunta = "";
			String vRespuesta = "";
			String vPregExclu = "";
			String vPonderac = "";
			String vTipoResp = "";
			String vNumOpc = "";
			String cvePregunta = "";

            qryUpdateResp =
                " INSERT INTO com_respuesta (ic_respuesta, ic_encuesta, ic_pregunta, ic_tipo_respuesta, cg_excluyentes, ig_ponderacion, cg_respuesta) " +
								" VALUES (seq_com_respuesta.nextval, ?, ?, ?, ?, ?, ? )";

            qrySelect =
                "SELECT ic_pregunta " + "  FROM com_pregunta " +
							" WHERE ic_encuesta = ? AND ic_nopregunta = ? AND ic_tipo_respuesta = ? ";

			if(lstRespuestas!=null && lstRespuestas.size()>0){
				int p = 0;
				for(int x=0; x<lstRespuestas.size(); x++ ){
					p=0;
					dataRespuestas = (HashMap)lstRespuestas.get(x);
					vNumPregunta = (String)dataRespuestas.get("NUMPREG");
					vRespuesta = (String)dataRespuestas.get("RESPUESTA");
					vPregExclu = (String)dataRespuestas.get("PREGEXCLU");
					vPonderac = (String)dataRespuestas.get("PONDERACION");
					vTipoResp = (String)dataRespuestas.get("TIPORESP");
					vNumOpc = (String)dataRespuestas.get("NUMOPC");
					cvePregunta = "";

					ps = con.queryPrecompilado(qrySelect);
					ps.setLong(1, Long.parseLong(cveEncuesta));
					ps.setLong(2, Long.parseLong(vNumPregunta));
					ps.setLong(3, Long.parseLong(vTipoResp));
					rs = ps.executeQuery();

					if(rs.next()){
						cvePregunta = rs.getString("ic_pregunta");
					}
					rs.close();
					ps.close();

					ps = con.queryPrecompilado(qryUpdateResp);
					ps.setLong(++p, Long.parseLong(cveEncuesta));
					ps.setLong(++p, Long.parseLong(cvePregunta));
					ps.setLong(++p, Long.parseLong(vTipoResp));
					ps.setString(++p, vPregExclu);
					ps.setLong(++p, Long.parseLong(vPonderac));
					ps.setString(++p, vRespuesta);
					ps.executeUpdate();
					ps.close();

				}
			}

        } catch (Throwable t) {
			throw new AppException("Error al guardar la paramterizacion de respuestas para la encuesta", t);
		}finally{
			log.info("ServiciosEJB::guardaRespuesta(S)");
		}
	}


	public List consultaEncuestas(String cveEncuesta, String cveEpo, String cveTipoAfil, String fechaIni,
				String fechaFin, String tipoAfiliado) throws AppException {
		System.out.println("ServiciosEJB::consultaEncuestas(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		List lstEncuestas = new ArrayList();

		try{
			con.conexionDB();
			String tablas = "";
			String condiciones = "";


			//CONDICIONES PARA CONSULTA DE ENCUESTAS PYME
			if("P".equals(cveTipoAfil)|| "D".equals(cveTipoAfil)){
				String cliente = "P".equals(cveTipoAfil)?"1":"2";
				tablas = ", comcat_pyme cp ";
                condiciones += " AND cea.ic_pyme = cp.ic_pyme " + " AND cp.ic_tipo_cliente =  " + cliente + " ";
			}else if(!"".equals(cveTipoAfil)){
				tablas = ", comcat_epo cep ";
                condiciones += " AND cea.ic_epo = cep.ic_epo " + " AND cep.ic_tipo_epo = " + cveTipoAfil + " ";
			}


            if (!"".equals(cveEpo))
                condiciones += " AND cea.ic_epo = " + cveEpo + " ";
			System.out.println("CadenasEpos==="+cveEpo);
			//

			//CONDICIONES PAERA CONSULTAS DE ENCUESTA IF Y GRAL
            if (!"".equals(cveEncuesta))
                condiciones += " AND ce.ic_encuesta = " + cveEncuesta;
            if (!"".equals(fechaIni))
                condiciones += " AND ce.df_publicacion >= trunc(to_date('" + fechaIni + "','dd/mm/yyyy'))  ";
            if (!"".equals(fechaFin))
                condiciones += " AND ce.df_publicacion < trunc(to_date('" + fechaFin + "','dd/mm/yyyy')+1) ";

			String qryConsulta =
				"SELECT DISTINCT (ce.ic_encuesta) cveencuesta, ce.cg_titulo titulo, " +
				"         TO_CHAR (ce.df_ini_encuesta, 'DD/MM/YYYY') fechaini, " +
				"         TO_CHAR (ce.df_fin_encuesta, 'DD/MM/YYYY') fechafin,  " +
				"         ce.ig_num_preguntas numpreguntas, ce.CS_OBLIGATORIO obligatorio " +
				"    FROM com_encuesta ce, comrel_encuesta_afiliado cea " +tablas+
                "   WHERE ce.ic_encuesta = cea.ic_encuesta " + "   AND cea.cg_afiliado =  '" + tipoAfiliado + "' " +
                condiciones + " ORDER BY ce.ic_encuesta ";

				System.out.println("qryConsulta======"+qryConsulta);
				ps = con.queryPrecompilado(qryConsulta);
				rs = ps.executeQuery();

				HashMap mpData = null;

				while(rs.next()){
					mpData = new HashMap();
					mpData.put("CVEENCUESTA",rs.getString("cveencuesta"));
					mpData.put("TITULO",rs.getString("titulo"));
					mpData.put("FECHAINI",rs.getString("fechaini"));
					mpData.put("FECHAFIN",rs.getString("fechafin"));
					mpData.put("OBLIGATORIO",rs.getString("obligatorio"));
					mpData.put("NUMPREGUNTAS",rs.getString("numpreguntas"));

                qryConsulta =
                    " SELECT tt_instrucciones FROM com_encuesta where  ic_encuesta = " + rs.getString("cveencuesta");

					ps2 = con.queryPrecompilado(qryConsulta);
					rs2 = ps2.executeQuery();
					String instrucciones = ".....";
					if (rs2.next()) {
						java.sql.Clob clob = rs2.getClob("tt_instrucciones");
                    if (clob != null)
                        instrucciones = clob.getSubString(1, 30) + ".....";
					}
					rs2.close();
					ps2.close();

					String numResp = "0";
                qryConsulta =
                    " SELECT COUNT(1) as valor " + "  FROM com_encuesta ce, " +
                    "       comrel_encuesta_afiliado cea, " + "       com_contesta_respuesta cc " +
                    " WHERE ce.ic_encuesta = cea.ic_encuesta " + "   AND cea.ic_encuesta_afil = cc.ic_encuesta_afil " +
							"   AND ce.ic_encuesta =  ? " ;

					ps2 = con.queryPrecompilado(qryConsulta);
					ps2.setLong(1, Long.parseLong(rs.getString("cveencuesta")));
					rs2 = ps2.executeQuery();
					if (rs2.next()) {
						numResp = rs2.getString("valor");
					}
					rs2.close();
					ps2.close();


					mpData.put("NUMRESPUESTAS",numResp);
					mpData.put("INSTRUCCIONES",instrucciones);

					lstEncuestas.add(mpData);
				}

				rs.close();
				ps.close();

			return lstEncuestas;

		}catch(Throwable t){
			throw new AppException("consultaEncuestas(Error): Error al consultar las encuestas", t);
        } finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::consultaEncuestas(S)");
		}
	}

    public String modificacionEncuestaGral(HashMap DatosAltaEnc, List lstPreguntas, List lstRespuestas,
                                           String cveEncuesta) throws AppException {
		System.out.println("ServiciosEJB::guardaEncuestaGral(E)");

		boolean				lbSinError 		= true;
		AccesoDB 			conexBD 		= new AccesoDB();
		PreparedStatement	ps;
		String				SQLEncuesta;

		try{
			conexBD.conexionDB();

			//se obtien datos del alta de la encuesta
			String fp_titulo = (String)DatosAltaEnc.get("fp_titulo");
			String fp_fec_desde = (String)DatosAltaEnc.get("fp_fec_desde");
			String fp_fec_hasta = (String)DatosAltaEnc.get("fp_fec_hasta");
			String fp_instrucciones = (String)DatosAltaEnc.get("fp_instrucciones");
			//String fp_cadena = (String)DatosAltaEnc.get("fp_cadena");
			//String fp_tipoAfiliado = (String)DatosAltaEnc.get("fp_tipoAfiliado");
			String fp_respObligada = (String)DatosAltaEnc.get("fp_respObligada");
			String fp_diasResp = (String)DatosAltaEnc.get("fp_diasResp");
			//String fp_numPreguntas = (String)DatosAltaEnc.get("fp_numPreguntas");
			//String fp_selecAfiliados = (String)DatosAltaEnc.get("fp_selecAfiliados");


			SQLEncuesta =
                "UPDATE com_encuesta " + "   SET cg_titulo = ?, " +
				"       df_ini_encuesta = TO_DATE (?, 'dd/mm/yyyy'), " +
                "       df_fin_encuesta = TO_DATE (?, 'dd/mm/yyyy'), " + "       tt_instrucciones = ?, " +
                "       cs_obligatorio = ?, " + "       ic_diaspre = ? " + " WHERE ic_encuesta = ? ";


			ps = conexBD.queryPrecompilado(SQLEncuesta);
			ps.setString(1, fp_titulo);
			ps.setString(2, fp_fec_desde);
			ps.setString(3, fp_fec_hasta);
			ps.setString(4, fp_instrucciones.replace('\'',' '));
			ps.setString(5, fp_respObligada);
			ps.setInt(6, Integer.parseInt(fp_diasResp));
			ps.setLong(7,Long.parseLong(cveEncuesta));
			ps.executeUpdate();
			ps.close();

			//SE BORRA DETALLE DE OPCIONES DE RESPUESTA PARA LAS PREGUNTAS
			SQLEncuesta = " DELETE FROM com_respuesta WHERE ic_encuesta = ? ";
			ps = conexBD.queryPrecompilado(SQLEncuesta);
			ps.setLong(1,Long.parseLong(cveEncuesta));
			ps.executeUpdate();
			ps.close();

			//SE BORRA PREGUNTAS DE LA SUBASTA
			SQLEncuesta = " DELETE FROM com_pregunta WHERE ic_encuesta = ? ";
			ps = conexBD.queryPrecompilado(SQLEncuesta);
			ps.setLong(1,Long.parseLong(cveEncuesta));
			ps.executeUpdate();
			ps.close();

			guardaPreguntaGral(conexBD, lstPreguntas, cveEncuesta);
			guardaRespuestaEnc(conexBD, lstRespuestas, cveEncuesta);
			lbSinError = true;

			return cveEncuesta;

		}catch(Throwable t ){
			lbSinError = false;
			throw new AppException("Error al modificar la encuesta", t);
		}finally{
			conexBD.terminaTransaccion(lbSinError);
			if(conexBD.hayConexionAbierta()){
				conexBD.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::modificacionEncuestaGral(S)");
		}

	}

	public List consultaRespuestas(String tipoAfiliado) throws AppException {
		log.info("ServiciosEJB::consultaRespuestas(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstRespuestas = new ArrayList();

		try{
			con.conexionDB();

			String qryConsulta =
				"SELECT DISTINCT cea.ic_encuesta AS ic_encuesta, ce.cg_titulo AS cg_titulo,  " +
					"                to_char(ce.df_fin_encuesta,'dd/mm/yyyy') AS df_fin_encuesta,  " +
					"                to_char(ce.df_ini_encuesta,'dd/mm/yyyy') AS df_ini_encuesta,  " +
					"                to_char(ce.df_publicacion,'dd/mm/yyyy') AS df_publicacion,  " +
					"                ce.ig_num_preguntas AS ig_num_preguntas,  " +
                "                NVL (totres.countresp, 0) AS contresp  " + "           FROM com_encuesta ce,  " +
					"                comrel_encuesta_afiliado cea,  " +
					"                (SELECT   COUNT (DISTINCT (ic_usuario)) AS countresp,  " +
					"                          cea.ic_encuesta AS ic_encuesta  " +
					"                     FROM com_contesta_respuesta ccr,  " +
					"                          comrel_encuesta_afiliado cea " +
					"                     where cea.ic_encuesta_afil = ccr.ic_encuesta_afil " +
					"                     and cea.cg_afiliado = ?  " +
					"                 GROUP BY cea.ic_encuesta) totres  " +
					"          WHERE ce.ic_encuesta = cea.ic_encuesta  " +
                "            AND cea.ic_encuesta = totres.ic_encuesta  " + "            AND cea.cg_afiliado = ? " +
					"       ORDER BY ce.cg_titulo  ";

				ps = con.queryPrecompilado(qryConsulta);
				ps.setString(1,tipoAfiliado);
				ps.setString(2,tipoAfiliado);
				rs = ps.executeQuery();

				HashMap mpData = null;

				while(rs.next()){
					mpData = new HashMap();
					mpData.put("CVEENCUESTA",rs.getString("ic_encuesta"));
					mpData.put("TITULO",rs.getString("cg_titulo"));
					mpData.put("FECHAINI",rs.getString("df_ini_encuesta"));
					mpData.put("FECHAFIN",rs.getString("df_fin_encuesta"));
					mpData.put("FECPUBLIC",rs.getString("df_publicacion"));
					mpData.put("NUMPREGUNTAS",rs.getString("ig_num_preguntas"));
					mpData.put("CONTRESP",rs.getString("contresp"));

					lstRespuestas.add(mpData);
				}

				rs.close();
				ps.close();

			return lstRespuestas;

		}catch(Throwable t){
			throw new AppException("consultaRespuestas(Error): Error al consultar respuestas de la encuesta", t);
        } finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("ServiciosEJB::consultaRespuestas(S)");
		}
	}

    public List consultaRespIndividual(String cveEncuesta, String cveAfiliado,
                                       String tipoAfiliado) throws AppException {
		log.info("ServiciosEJB::consultaRespuestas(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		List lstRespIndividual = new ArrayList();

		try{
			con.conexionDB();
			String condicion = "";
			if("I".equals(tipoAfiliado)){
				condicion = " AND cea.ic_if = ? ";
			} else if("P".equals(tipoAfiliado)){
				condicion = " AND cea.ic_pyme = ? ";
			} else if("E".equals(tipoAfiliado)){
				condicion = " AND cea.ic_epo = ? ";
			}

			String qryConsulta =
				"SELECT DISTINCT cea.ic_encuesta_afil AS ic_encuesta_afil,  " +
				"                cp.ic_pregunta AS ic_pregunta,   " +
				"                cp.cg_pregunta AS cg_pregunta,  " +
				"                cp.ic_tipo_respuesta AS tipo_resp,  " +
                "					  cp.ig_num_pregunta as ig_num_pregunta, " + "					  cp.ic_nopregunta as ic_nopregunta " +
                "           FROM com_encuesta ce,  " + "                comrel_encuesta_afiliado cea,  " +
                "                com_pregunta cp,  " + "                com_respuesta cr  " +
				"          WHERE ce.ic_encuesta = cea.ic_encuesta  " +
				"            AND cea.ic_encuesta = cp.ic_encuesta  " +
                "            AND cp.ic_pregunta = cr.ic_pregunta(+)  " + "            AND cea.ic_encuesta = ? " +
                condicion + "	       ORDER BY cp.ic_pregunta ";


				ps = con.queryPrecompilado(qryConsulta);
				ps.setLong(1,Long.parseLong(cveEncuesta));
				ps.setLong(2,Long.parseLong(cveAfiliado));
				rs = ps.executeQuery();

				HashMap mpData = null;
				List lstResp = null;
				HashMap mpDataResp = null;
				while(rs.next()){
					mpData = new HashMap();
                mpData.put("ICENCAFIL",
                           rs.getString("ic_encuesta_afil") == null ? "" : rs.getString("ic_encuesta_afil"));
					mpData.put("ICPREGUNTA",rs.getString("ic_pregunta")==null?"":rs.getString("ic_pregunta"));
					mpData.put("CGPREGUNTA",rs.getString("cg_pregunta")==null?"":rs.getString("cg_pregunta"));
					mpData.put("TIPORESP",rs.getString("tipo_resp")==null?"":rs.getString("tipo_resp"));
                mpData.put("IGORDENPREG",
                           rs.getString("ig_num_pregunta") == null ? "" : rs.getString("ig_num_pregunta"));
					mpData.put("IGNUMPREG",rs.getString("ic_nopregunta"));


                qryConsulta =
                    "SELECT cr.ic_tipo_respuesta AS ic_tipo_respuesta, " +
								"       ccr.ic_respuesta AS ic_respuesta, cr.cg_respuesta AS cg_respuesta, " +
								"       ccr.cg_contesta_respuesta AS cg_contesta_respuesta " +
								"  FROM com_respuesta cr, com_contesta_respuesta ccr " +
                    " WHERE cr.ic_respuesta = ccr.ic_respuesta(+) " + "   AND ccr.ic_encuesta_afil = ? " +
								"   AND cr.ic_pregunta = ? ";

					ps2 = con.queryPrecompilado(qryConsulta);
					ps2.setLong(1,Long.parseLong(rs.getString("ic_encuesta_afil")));
					ps2.setLong(2,Long.parseLong(rs.getString("ic_pregunta")));
					rs2 = ps2.executeQuery();

					lstResp = new ArrayList();
					while(rs2.next()){
						mpDataResp = new HashMap();
						mpDataResp.put("TIPORESP",rs2.getString("ic_tipo_respuesta"));
						mpDataResp.put("ICRESPUESTA",rs2.getString("ic_respuesta"));
						mpDataResp.put("CGRESPUESTA",rs2.getString("cg_respuesta"));
						mpDataResp.put("CGCONTESTARESP",rs2.getString("cg_contesta_respuesta"));

						lstResp.add(mpDataResp);
					}
					rs2.close();
					ps2.close();

					mpData.put("LSTRESP", lstResp);

					lstRespIndividual.add(mpData);
				}

				rs.close();
				ps.close();

			return lstRespIndividual;

		}catch(Throwable t){
			throw new AppException("consultaRespuestas(Error): Error al consultar respuestas de la encuesta", t);
        } finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("ServiciosEJB::consultaRespuestas(S)");
		}
	}

    public List consultaRespConsolidada(String cveEncuesta, String cveAfiliado,
                                        String tipoAfiliado) throws AppException {
		log.info("ServiciosEJB::consultaRespConsolidada(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		PreparedStatement ps3 = null;
		ResultSet rs3 = null;
		List lstRespConsolidada = new ArrayList();

		try{
			con.conexionDB();

			String qryConsulta =
				"SELECT cp.ic_pregunta AS ic_pregunta, cp.cg_pregunta AS cg_pregunta,  " +
				"       cp.ig_num_pregunta as ig_num_pregunta , cp.ic_tipo_respuesta AS tipo_resp  " +
				"       ,cp.ic_nopregunta  as ic_nopregunta, cp.ic_rubro as ic_rubro  " +
                "		  ,cp.cs_tipo_pregunta as tipo_preg " + "  FROM com_encuesta cea, com_pregunta cp  " +
                " WHERE cea.ic_encuesta = cp.ic_encuesta  " + "   AND cea.ic_encuesta = ? " +
                "   AND cp.ic_tipo_respuesta in(?,?,?,?)  " + "	ORDER BY cp.ic_pregunta ";


				ps = con.queryPrecompilado(qryConsulta);
				ps.setLong(1,Long.parseLong(cveEncuesta));
				ps.setLong(2,Long.parseLong("2"));
				ps.setLong(3,Long.parseLong("3"));
				ps.setLong(4,Long.parseLong("4"));
				ps.setLong(5,Long.parseLong("5"));
				rs = ps.executeQuery();

				HashMap mpData = null;
				List lstResp = null;
				HashMap mpDataResp = null;
				while(rs.next()){
					mpData = new HashMap();
					mpData.put("ICPREGUNTA",rs.getString("ic_pregunta"));
					mpData.put("CGPREGUNTA",rs.getString("cg_pregunta"));
					mpData.put("IGORDENPREG",rs.getString("ig_num_pregunta"));
					mpData.put("TIPOPREG",rs.getString("tipo_preg"));
					mpData.put("TIPORESP",rs.getString("tipo_resp"));
					mpData.put("IGNUMPREG",rs.getString("ic_nopregunta"));
					mpData.put("ICRUBRO",rs.getString("ic_rubro"));


                qryConsulta =
                    "SELECT cr.cg_respuesta AS cg_respuesta, NVL (totr.countresp, 0) AS countresp, cr.ic_tipo_respuesta ic_tipo_respuesta, cr.ic_respuesta ic_respuesta " +
                    "  FROM com_encuesta cea, " + "       com_pregunta cp, " + "       com_respuesta cr, " +
							"       (SELECT   COUNT (DISTINCT (ic_usuario)) AS countresp, " +
							"                 cea.ic_encuesta AS ic_encuesta, " +
							"                 ccr.ic_respuesta AS ic_respuesta " +
							"            FROM com_contesta_respuesta ccr, comrel_encuesta_afiliado cea " +
							"           WHERE ccr.ic_encuesta_afil = cea.ic_encuesta_afil " +
							"             AND cea.ic_encuesta = ? " +
							"        GROUP BY cea.ic_encuesta, ccr.ic_respuesta) totr " +
                    " WHERE cea.ic_encuesta = cp.ic_encuesta " + "   AND cr.ic_encuesta = totr.ic_encuesta(+) " +
                    "   AND cr.ic_respuesta = totr.ic_respuesta(+) " + "   AND cp.ic_pregunta = cr.ic_pregunta " +
                    "   AND cea.ic_encuesta = ? " + "   AND cr.ic_pregunta = ? " +
							"   AND cr.ic_tipo_respuesta IN (?, ?, ?)  ";


					ps2 = con.queryPrecompilado(qryConsulta);
					ps2.setLong(1,Long.parseLong(cveEncuesta));
					ps2.setLong(2,Long.parseLong(cveEncuesta));
					ps2.setLong(3,Long.parseLong(rs.getString("ic_pregunta")));
					ps2.setLong(4,Long.parseLong("2"));
					ps2.setLong(5,Long.parseLong("3"));
					ps2.setLong(6,Long.parseLong("5"));
					rs2 = ps2.executeQuery();

					lstResp = new ArrayList();
					while(rs2.next()){
						mpDataResp = new HashMap();
						mpDataResp.put("CGRESPUESTA",rs2.getString("cg_respuesta"));
						mpDataResp.put("CONTRESP",rs2.getString("countresp"));
						mpDataResp.put("TIPORESP",rs2.getString("ic_tipo_respuesta"));
						mpDataResp.put("CVERESP",rs2.getString("ic_respuesta"));


						if("5".equals(rs2.getString("ic_tipo_respuesta"))){
							//query para las respuestas con ponderacion
                        qryConsulta =
                            "SELECT   COUNT (DISTINCT (ic_usuario)) AS countresp,  " +
							"                 cea.ic_encuesta AS ic_encuesta,  " +
							"                 ccr.ic_respuesta AS ic_respuesta, " +
							"                 ccr.cg_contesta_respuesta as cg_contesta_respuesta " +
							"            FROM com_contesta_respuesta ccr, comrel_encuesta_afiliado cea  " +
							"           WHERE ccr.ic_encuesta_afil = cea.ic_encuesta_afil  " +
                            "             AND cea.ic_encuesta = ? " + "             AND ccr.ic_respuesta = ? " +
							"        GROUP BY cea.ic_encuesta, ccr.ic_respuesta, ccr.cg_contesta_respuesta ";

							ps3 = con.queryPrecompilado(qryConsulta);
							ps3.setLong(1, Long.parseLong(cveEncuesta));
							ps3.setLong(2, Long.parseLong(rs2.getString("ic_respuesta")));
							rs3 = ps3.executeQuery();

							List lstRespPond = new ArrayList();
							HashMap hmRespPond = null;
							while(rs3.next()){
								hmRespPond = new HashMap();
								hmRespPond.put("CONTRESP",rs3.getString("countresp"));
								hmRespPond.put("CONTESTARESP",rs3.getString("cg_contesta_respuesta"));
								lstRespPond.add(hmRespPond);
							}
							rs3.close();
							ps3.close();

							mpDataResp.put("LSTRESPPOND", lstRespPond);
						}

						lstResp.add(mpDataResp);
					}
					rs2.close();
					ps2.close();

					mpData.put("LSTRESP", lstResp);

					lstRespConsolidada.add(mpData);
				}

				rs.close();
				ps.close();

			return lstRespConsolidada;

		}catch(Throwable t){
            throw new AppException("consultaRespConsolidada(Error): Error al consultar respuestas consolidadas de la encuesta",
                                   t);
        } finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("ServiciosEJB::consultaRespConsolidada(S)");
		}
	}


	public HashMap consultaInfoEncuesta (String cveEncuesta, String tipoAfiliado) throws AppException {
		log.info("ServiciosEJB::consultaInfoEncuesta(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		HashMap hmResp = new HashMap();
		HashMap hmDataEncuesta = new HashMap();

		try{
			con.conexionDB();

			//SE OBTIENEN DATOS PRINCIPALES DE ENCUESTA
			String qryConsulta =
				"SELECT ic_encuesta, ig_num_preguntas, cg_titulo, " +
				"       TO_CHAR (df_ini_encuesta, 'DD/MM/YYYY') as df_ini_encuesta, " +
				"       TO_CHAR (df_fin_encuesta, 'DD/MM/YYYY') as df_fin_encuesta, " +
                "       tt_instrucciones, cs_obligatorio, ic_diaspre " + "  FROM com_encuesta " +
				" WHERE ic_encuesta = ? ";

				ps = con.queryPrecompilado(qryConsulta);
				ps.setLong(1,Long.parseLong(cveEncuesta));
				rs = ps.executeQuery();

				HashMap hmParamEncuesta = new HashMap();
				if(rs.next()){
					hmParamEncuesta.put("fp_cveEncuesta_m"+tipoAfiliado,rs.getString("ic_encuesta"));
					hmParamEncuesta.put("fp_igNumPreg"+tipoAfiliado,rs.getString("ig_num_preguntas"));
					hmParamEncuesta.put("fp_titulo_m"+tipoAfiliado,rs.getString("cg_titulo"));
					hmParamEncuesta.put("fp_fec_desde_m"+tipoAfiliado,rs.getString("df_ini_encuesta"));
					hmParamEncuesta.put("fp_fec_hasta_m"+tipoAfiliado,rs.getString("df_fin_encuesta"));
					hmParamEncuesta.put("fp_instrucciones_m"+tipoAfiliado,rs.getString("tt_instrucciones"));
					hmParamEncuesta.put("fp_respObligada_m"+tipoAfiliado,rs.getString("cs_obligatorio"));
					hmParamEncuesta.put("fp_diasResp_m"+tipoAfiliado,rs.getString("ic_diaspre"));
				}
				rs.close();
				ps.close();

				hmDataEncuesta.put("INFOGRAL",hmParamEncuesta);

				//SE OBTIENEN LAS REGUNTAS DE LA ENCUESTA

				qryConsulta =
					"SELECT DISTINCT cp.ic_pregunta AS ic_pregunta,   " +
					"                cp.cg_pregunta AS cg_pregunta,  " +
					"                cp.cs_tipo_pregunta AS tipo_preg,  " +
					"                cp.ic_tipo_respuesta AS tipo_resp,  " +
                "					  cp.ig_num_pregunta as ig_num_pregunta, " + "					  cp.ic_nopregunta as ic_nopregunta, " +
					"					  cp.ig_num_opciones as ig_num_opciones, ic_rubro as ic_rubro " +
                "           FROM com_encuesta ce,  " + "                com_pregunta cp  " +
                "          WHERE ce.ic_encuesta =  cp.ic_encuesta  " + "            AND ce.ic_encuesta = ? " +
					"	       ORDER BY cp.ic_pregunta ";

				ps = con.queryPrecompilado(qryConsulta);
				ps.setLong(1,Long.parseLong(cveEncuesta));
				rs = ps.executeQuery();

				HashMap mpDataPreg = null;
				List lstPreg = new ArrayList();
				while(rs.next()){
					mpDataPreg = new HashMap();
					mpDataPreg.put("ICPREGUNTA",rs.getString("ic_pregunta"));
					mpDataPreg.put("CGPREGUNTA",rs.getString("cg_pregunta"));
					mpDataPreg.put("TIPOPREG",rs.getString("tipo_preg"));
					mpDataPreg.put("TIPORESP",rs.getString("tipo_resp"));
					mpDataPreg.put("IGORDENPREG",rs.getString("ig_num_pregunta"));
					mpDataPreg.put("IGNUMPREG",rs.getString("ic_nopregunta"));
					mpDataPreg.put("IGNUMOPC",rs.getString("ig_num_opciones"));
					mpDataPreg.put("ICRUBRO",rs.getString("ic_rubro"));
					lstPreg.add(mpDataPreg);


					if(!"4".equals(rs.getString("tipo_resp"))){
						qryConsulta =
							"SELECT DISTINCT cp.ic_pregunta AS ic_pregunta, cp.cg_pregunta AS cg_pregunta, " +
							"                cp.ic_tipo_respuesta AS tipo_resp, " +
							"                cp.ig_num_pregunta AS ig_num_pregunta, " +
							"                cp.ic_nopregunta AS ic_nopregunta, " +
							"                cp.ig_num_opciones AS ig_num_opciones, ic_rubro AS ic_rubro, " +
							"                cr.ic_respuesta as ic_respuesta,  " +
							"                cr.cg_respuesta as cg_respuesta, " +
							"                cr.cg_excluyentes as cg_excluyentes, " +
							"                cr.ig_ponderacion as ig_ponderacion " +
							"           FROM com_encuesta ce, com_pregunta cp, com_respuesta cr " +
							"          WHERE ce.ic_encuesta = cp.ic_encuesta " +
							"            AND cp.ic_encuesta = cr.ic_encuesta " +
                        "            AND cp.ic_pregunta = cr.ic_pregunta " + "            AND ce.ic_encuesta = ? " +
                        "            AND cr.ic_pregunta = ? " + "       ORDER BY cp.ic_pregunta ";

						ps2 = con.queryPrecompilado(qryConsulta);
						ps2.setLong(1,Long.parseLong(cveEncuesta));
						ps2.setLong(2,Long.parseLong(rs.getString("ic_pregunta")));
						rs2 = ps2.executeQuery();

						HashMap mpDataResp = null;
						List lstRespPreg = new ArrayList();
						while(rs2.next()){
							mpDataResp = new HashMap();
							mpDataResp.put("ICPREGUNTA",rs2.getString("ic_pregunta"));
							mpDataResp.put("CGPREGUNTA",rs2.getString("cg_pregunta"));
							mpDataResp.put("TIPORESP",rs2.getString("tipo_resp"));
							mpDataResp.put("IGORDENPREG",rs2.getString("ig_num_pregunta"));
							mpDataResp.put("IGNUMPREG",rs2.getString("ic_nopregunta"));
							mpDataResp.put("IGNUMOPC",rs2.getString("ig_num_opciones"));
							mpDataResp.put("ICRUBRO",rs2.getString("ic_rubro"));
							mpDataResp.put("ICRESPUESTA",rs2.getString("ic_respuesta"));
							mpDataResp.put("CGRESPUESTA",rs2.getString("cg_respuesta"));
							mpDataResp.put("CGEXCLUTENTE",rs2.getString("cg_excluyentes"));
							mpDataResp.put("IGPONDERACION",rs2.getString("ig_ponderacion"));


							lstRespPreg.add(mpDataResp);
						}
						rs2.close();
						ps2.close();

						hmResp.put("p"+rs.getString("ic_nopregunta"),lstRespPreg);
					}


				}
				rs.close();
				ps.close();

				hmDataEncuesta.put("INFOPREGUNTAS",lstPreg);

				//SE OBTIENEN LAS RESPUESTA  DE LA ENCUESTA
				hmDataEncuesta.put("INFORESPUESTAS",hmResp);


			return hmDataEncuesta;

		}catch(Throwable t){
			throw new AppException("consultaInfoEncuesta(Error): Error al consultar informacion de la encuesta", t);
        } finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("ServiciosEJB::consultaInfoEncuesta(S)");
		}
	}


    public String guardaPlantillaGral(HashMap DatosAltaEnc, List lstPreguntas, List lstRespuestas,
                                      String tipoAfiliado) throws AppException {
		System.out.println("ServiciosEJB::guardaEncuestaGral(E)");

		boolean				lbSinError 		= true;
		AccesoDB 			conexBD 		= new AccesoDB();
		PreparedStatement	ps;
		ResultSet			rs;
		String				SQLEncuesta;
		String				ic_encuesta = "";

		try{
			conexBD.conexionDB();

            SQLEncuesta = " SELECT seq_com_plantilla.nextval " + "   FROM dual";
			ps = conexBD.queryPrecompilado(SQLEncuesta);
			rs = ps.executeQuery();
			if(rs.next())
				ic_encuesta = (rs.getString(1)==null)?"":rs.getString(1);
			rs.close();
			ps.close();

			//se obtien datos del alta de la encuesta
			String fp_titulo = (String)DatosAltaEnc.get("fp_titulo");
			//String fp_fec_desde = (String)DatosAltaEnc.get("fp_fec_desde");
			//String fp_fec_hasta = (String)DatosAltaEnc.get("fp_fec_hasta");
			String fp_instrucciones = (String)DatosAltaEnc.get("fp_instrucciones");
			//String fp_cadena = (String)DatosAltaEnc.get("fp_cadena");
			//String fp_tipoAfiliado = (String)DatosAltaEnc.get("fp_tipoAfiliado");
			//String fp_respObligada = (String)DatosAltaEnc.get("fp_respObligada");
			//String fp_diasResp = (String)DatosAltaEnc.get("fp_diasResp");
			//String fp_numPreguntas = (String)DatosAltaEnc.get("fp_numPreguntas");
			//String fp_selecAfiliados = (String)DatosAltaEnc.get("fp_selecAfiliados");
			String fp_usuario = (String)DatosAltaEnc.get("fp_usuario");


					SQLEncuesta =
						" INSERT INTO com_plantilla"   +
						"             (ic_plantilla, cg_titulo, tt_instrucciones, ig_num_preguntas, cg_usuario_alta) "   +
						"      VALUES (?, ?, ?, ?, ?)"  ;

					ps = conexBD.queryPrecompilado(SQLEncuesta);
					ps.setLong(1,Long.parseLong(ic_encuesta));
					ps.setString(2, fp_titulo);
					ps.setString(3, fp_instrucciones.replace('\'',' '));
					ps.setInt(4, lstPreguntas.size());
					ps.setString(5, fp_usuario);
					ps.executeUpdate();
					ps.clearParameters();
					ps.close();

			guardaPreguntaPlant(conexBD, lstPreguntas, ic_encuesta);
			guardaRespuestaPlant(conexBD, lstRespuestas, ic_encuesta);
			lbSinError = true;

			return ic_encuesta;

		}catch(Throwable t ){
			lbSinError = false;
			throw new AppException("Error al guardar la plantilla", t);
		}finally{
			conexBD.terminaTransaccion(lbSinError);
			if(conexBD.hayConexionAbierta()){
				conexBD.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::guardaEncuestaGral(S)");
		}
	}


    private void guardaPreguntaPlant(AccesoDB conexBD, List lstPreguntas, String cveEncuesta) throws AppException {
		System.out.println("ServiciosEJB::guardaPreguntaPlant(E)");

		boolean				lbSinError 		= true;
		PreparedStatement	ps;
		String				SQLPregunta;
		try{

			if(lstPreguntas!=null && lstPreguntas.size()>0){

				for(int j=0;j<lstPreguntas.size();j++) {
					HashMap DatosPregEnc = (HashMap)lstPreguntas.get(j);

					String fpOrdenPreg = (String)DatosPregEnc.get("ORDENPREG");
					String fpPregunta = (String)DatosPregEnc.get("PREGUNTA");
					String fpTipoPreg = (String)DatosPregEnc.get("TIPOPREG");
					String fpTipoResp = (String)DatosPregEnc.get("TIPORESP");
					String fpNumOpc = (String)DatosPregEnc.get("NUMOPC");
					String fpNumRubro = (String)DatosPregEnc.get("NUMRUBRO");
					String fpNumPreg = (String)DatosPregEnc.get("NUMPREG");


					if(fpPregunta.length()>300){
						fpPregunta = fpPregunta.substring(0,300);
					}

					SQLPregunta =
						" INSERT INTO com_plantilla_pregunta"   +
						"             (ic_pregunta, ic_plantilla, ig_num_pregunta, cs_tipo_pregunta, ic_tipo_respuesta, cg_pregunta, ig_num_opciones, ic_rubro, ic_nopregunta)"   +
						"      VALUES (seq_com_plantilla_pregunta.nextval,?, ?, ?, ?, ?, ?, ?, ?)"  ;

					ps = conexBD.queryPrecompilado(SQLPregunta);
					ps.setInt(1, Integer.parseInt(cveEncuesta));
					ps.setInt(2, Integer.parseInt(fpOrdenPreg));
					ps.setString(3, fpTipoPreg);
					ps.setString(4, fpTipoResp);
					ps.setString(5, fpPregunta);
					ps.setInt(6, Integer.parseInt(fpNumOpc));
					ps.setInt(7, Integer.parseInt(fpNumRubro));
					ps.setInt(8, Integer.parseInt(fpNumPreg));

					ps.executeUpdate();
					ps.clearParameters();
					ps.close();

				}//for(int j=0;j<lsPreguntas.length;j++)
			}//end if()

		}catch(Throwable t){
			lbSinError = false;
			throw new AppException("guardaPreguntaPlant(Error): Error al guardar preguntas de la plantilla", t);
        } finally {
			System.out.println("ServiciosEJB::guardaPreguntaPlant(S)");
		}
	}

	private void guardaRespuestaPlant(AccesoDB con, List lstRespuestas, String cveEncuesta ) throws AppException {
		System.out.println("ServiciosEJB::guardaRespuestaPlant(E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qryUpdateResp = "";
		String qrySelect = "";

		try{
			HashMap dataRespuestas = null;

			String vNumPregunta = "";
			String vRespuesta = "";
			String vPregExclu = "";
			String vPonderac = "";
			String vTipoResp = "";
			String vNumOpc = "";
			String cvePregunta = "";

            qryUpdateResp =
                " INSERT INTO com_plantilla_respuesta (ic_respuesta, ic_plantilla, ic_pregunta, ic_tipo_respuesta, cg_excluyentes, ig_ponderacion, cg_respuesta) " +
								" VALUES (seq_com_plantilla_respuesta.nextval, ?, ?, ?, ?, ?, ? )";

            qrySelect =
                "SELECT ic_pregunta " + "  FROM com_plantilla_pregunta " +
							" WHERE ic_plantilla = ? AND ic_nopregunta = ? AND ic_tipo_respuesta = ? ";

			if(lstRespuestas!=null && lstRespuestas.size()>0){
				int p = 0;
				for(int x=0; x<lstRespuestas.size(); x++ ){
					p=0;
					dataRespuestas = (HashMap)lstRespuestas.get(x);
					vNumPregunta = (String)dataRespuestas.get("NUMPREG");
					vRespuesta = (String)dataRespuestas.get("RESPUESTA");
					vPregExclu = (String)dataRespuestas.get("PREGEXCLU");
					vPonderac = (String)dataRespuestas.get("PONDERACION");
					vTipoResp = (String)dataRespuestas.get("TIPORESP");
					vNumOpc = (String)dataRespuestas.get("NUMOPC");
					cvePregunta = "";

					ps = con.queryPrecompilado(qrySelect);
					ps.setLong(1, Long.parseLong(cveEncuesta));
					ps.setLong(2, Long.parseLong(vNumPregunta));
					ps.setLong(3, Long.parseLong(vTipoResp));
					rs = ps.executeQuery();

					if(rs.next()){
						cvePregunta = rs.getString("ic_pregunta");
					}
					rs.close();
					ps.close();

					ps = con.queryPrecompilado(qryUpdateResp);
					ps.setLong(++p, Long.parseLong(cveEncuesta));
					ps.setLong(++p, Long.parseLong(cvePregunta));
					ps.setLong(++p, Long.parseLong(vTipoResp));
					ps.setString(++p, vPregExclu);
					ps.setLong(++p, Long.parseLong(vPonderac));
					ps.setString(++p, vRespuesta);
					ps.executeUpdate();
					ps.close();

				}
			}

        } catch (Throwable t) {
			throw new AppException("Error al guardar la paramterizacion de respuestas para la plantilla", t);
		}finally{
			log.info("ServiciosEJB::guardaRespuestaPlant(S)");
		}
	}

	public List consultaPlantillas(String cvePlantilla, String fechaIni, String fechaFin) throws AppException {
		System.out.println("ServiciosEJB::consultaPlantillas(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;
		List lstEncuestas = new ArrayList();

		try{
			con.conexionDB();
			String condiciones = "";


			//CONDICIONES PAERA CONSULTAS DE ENCUESTA IF Y GRAL
            if (!"".equals(cvePlantilla))
                condiciones += " AND ce.ic_plantilla = " + cvePlantilla;
            if (!"".equals(fechaIni))
                condiciones += " AND ce.df_publicacion >= trunc(to_date('" + fechaIni + "','dd/mm/yyyy'))  ";
            if (!"".equals(fechaFin))
                condiciones += " AND ce.df_modificacion < trunc(to_date('" + fechaFin + "','dd/mm/yyyy')+1) ";

			String qryConsulta =
				"SELECT DISTINCT (ce.ic_plantilla) cveplantilla, ce.cg_titulo titulo, " +
				"            ce.ig_num_preguntas numpreguntas, ce.cg_usuario_alta, " +
				"            TO_CHAR (ce.df_publicacion, 'DD/MM/YYYY') fecpublic, " +
                "            ce.cg_usuario_mod, " + "            TO_CHAR (ce.df_modificacion, 'DD/MM/YYYY') fecmod " +
                "       FROM com_plantilla ce where 1=1 " + condiciones + "   ORDER BY ce.ic_plantilla ";


				ps = con.queryPrecompilado(qryConsulta);
				rs = ps.executeQuery();

				HashMap mpData = null;

				while(rs.next()){
					mpData = new HashMap();
					mpData.put("CVEPLANTILLA",rs.getString("cveplantilla"));
					mpData.put("TITULO",rs.getString("titulo"));
					mpData.put("USUARIOALTA",rs.getString("cg_usuario_alta"));
					mpData.put("FECPUBLIC",rs.getString("fecpublic"));
					mpData.put("FECMOD",rs.getString("fecmod"));
					mpData.put("USUARIOMOD",rs.getString("cg_usuario_mod"));
					mpData.put("NUMPREGUNTAS",rs.getString("numpreguntas"));

                qryConsulta =
                    " SELECT tt_instrucciones FROM com_plantilla where  ic_plantilla = " + rs.getString("cveplantilla");

					ps2 = con.queryPrecompilado(qryConsulta);
					rs2 = ps2.executeQuery();
					String instrucciones = ".....";
					if (rs2.next()) {
						java.sql.Clob clob = rs2.getClob("tt_instrucciones");
                    if (clob != null)
                        instrucciones = clob.getSubString(1, 30) + ".....";
					}
					rs2.close();
					ps2.close();

					mpData.put("INSTRUCCIONES",instrucciones);

					lstEncuestas.add(mpData);
				}

				rs.close();
				ps.close();

			return lstEncuestas;

		}catch(Throwable t){
			throw new AppException("consultaPlantillas(Error): Error al consultar las plantillas", t);
        } finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::consultaPlantillas(S)");
		}
	}

	public void borraPlantilla(String cvePlantilla) throws AppException {
		System.out.println("Entrando a metodo 'borraPlantilla() del EJB de SERVICIOS'");
		AccesoDB conexBD = new AccesoDB();
		PreparedStatement ps = null;
		String SqlEncuesta = "";
		boolean lbSinError = true;
		try{
			conexBD.conexionDB();

			SqlEncuesta = "DELETE FROM com_plantilla_respuesta WHERE ic_plantilla = ? ";
			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(cvePlantilla));
			ps.executeUpdate();
			ps.close();

			SqlEncuesta = "DELETE FROM com_plantilla_pregunta WHERE ic_plantilla = ? ";
			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(cvePlantilla));
			ps.executeUpdate();
			ps.close();

			SqlEncuesta = "DELETE FROM com_plantilla WHERE ic_plantilla = ? ";
			ps = conexBD.queryPrecompilado(SqlEncuesta);
			ps.setLong(1,Long.parseLong(cvePlantilla));
			ps.executeUpdate();
			ps.close();

        } catch (Throwable t) {
			lbSinError = false;
			throw new AppException("Error al borrar plantilla", t);
        } finally {
			if(conexBD.hayConexionAbierta()){
				conexBD.terminaTransaccion(lbSinError);
				conexBD.cierraConexionDB();
			}
		}
	}


	public HashMap consultaInfoPlantilla (String cveEncuesta, String tipoAfiliado) throws AppException {
		log.info("ServiciosEJB::consultaInfoPlantilla(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps2 = null;
		ResultSet rs2 = null;
		HashMap hmResp = new HashMap();
		HashMap hmDataEncuesta = new HashMap();

		try{
			con.conexionDB();

			//SE OBTIENEN DATOS PRINCIPALES DE ENCUESTA
			String qryConsulta =
				"SELECT ic_plantilla, ig_num_preguntas, cg_titulo, " +
				"       TO_CHAR (df_publicacion, 'DD/MM/YYYY') as fecpub, " +
                "       TO_CHAR (df_modificacion, 'DD/MM/YYYY') as fecmod, " + "       tt_instrucciones " +
                "  FROM com_plantilla " + " WHERE ic_plantilla = ? ";

				ps = con.queryPrecompilado(qryConsulta);
				ps.setLong(1,Long.parseLong(cveEncuesta));
				rs = ps.executeQuery();

				HashMap hmParamEncuesta = new HashMap();
				if(rs.next()){
					hmParamEncuesta.put("fp_cveEncuesta_m"+tipoAfiliado,rs.getString("ic_plantilla"));
					hmParamEncuesta.put("fp_numPreguntas_m"+tipoAfiliado,rs.getString("ig_num_preguntas"));
					hmParamEncuesta.put("fp_titulo_m"+tipoAfiliado,rs.getString("cg_titulo"));
					hmParamEncuesta.put("fp_instrucciones_m"+tipoAfiliado,rs.getString("tt_instrucciones"));
					hmParamEncuesta.put("fp_fec_desde_m"+tipoAfiliado,rs.getString("fecpub"));
					hmParamEncuesta.put("fp_fec_hasta_m"+tipoAfiliado,rs.getString("fecmod"));
				}
				rs.close();
				ps.close();

				hmDataEncuesta.put("INFOGRAL",hmParamEncuesta);

				//SE OBTIENEN LAS REGUNTAS DE LA ENCUESTA

				qryConsulta =
					"SELECT DISTINCT cp.ic_pregunta AS ic_pregunta,   " +
					"                cp.cg_pregunta AS cg_pregunta,  " +
					"                cp.cs_tipo_pregunta AS tipo_preg,  " +
					"                cp.ic_tipo_respuesta AS tipo_resp,  " +
                "					  cp.ig_num_pregunta as ig_num_pregunta, " + "					  cp.ic_nopregunta as ic_nopregunta, " +
					"					  cp.ig_num_opciones as ig_num_opciones, ic_rubro as ic_rubro " +
                "           FROM com_plantilla ce,  " + "                com_plantilla_pregunta cp  " +
                "          WHERE ce.ic_plantilla =  cp.ic_plantilla  " + "            AND ce.ic_plantilla = ? " +
					"	       ORDER BY cp.ic_pregunta ";

				ps = con.queryPrecompilado(qryConsulta);
				ps.setLong(1,Long.parseLong(cveEncuesta));
				rs = ps.executeQuery();

				HashMap mpDataPreg = null;
				List lstPreg = new ArrayList();
				while(rs.next()){
					mpDataPreg = new HashMap();
					mpDataPreg.put("ICPREGUNTA",rs.getString("ic_pregunta"));
					mpDataPreg.put("CGPREGUNTA",rs.getString("cg_pregunta"));
					mpDataPreg.put("TIPOPREG",rs.getString("tipo_preg"));
					mpDataPreg.put("TIPORESP",rs.getString("tipo_resp"));
					mpDataPreg.put("IGORDENPREG",rs.getString("ig_num_pregunta"));
					mpDataPreg.put("IGNUMPREG",rs.getString("ic_nopregunta"));
					mpDataPreg.put("IGNUMOPC",rs.getString("ig_num_opciones"));
					mpDataPreg.put("ICRUBRO",rs.getString("ic_rubro"));
					lstPreg.add(mpDataPreg);


					if(!"4".equals(rs.getString("tipo_resp"))){
						qryConsulta =
							"SELECT DISTINCT cp.ic_pregunta AS ic_pregunta, cp.cg_pregunta AS cg_pregunta, " +
							"                cp.ic_tipo_respuesta AS tipo_resp, " +
							"                cp.ig_num_pregunta AS ig_num_pregunta, " +
							"                cp.ic_nopregunta AS ic_nopregunta, " +
							"                cp.ig_num_opciones AS ig_num_opciones, ic_rubro AS ic_rubro, " +
							"                cr.ic_respuesta as ic_respuesta,  " +
							"                cr.cg_respuesta as cg_respuesta, " +
							"                cr.cg_excluyentes as cg_excluyentes, " +
							"                cr.ig_ponderacion as ig_ponderacion " +
							"           FROM com_plantilla ce, com_plantilla_pregunta cp, com_plantilla_respuesta cr " +
							"          WHERE ce.ic_plantilla = cp.ic_plantilla " +
							"            AND cp.ic_plantilla = cr.ic_plantilla " +
                        "            AND cp.ic_pregunta = cr.ic_pregunta " + "            AND ce.ic_plantilla = ? " +
                        "            AND cr.ic_pregunta = ? " + "       ORDER BY cp.ic_pregunta ";

						ps2 = con.queryPrecompilado(qryConsulta);
						ps2.setLong(1,Long.parseLong(cveEncuesta));
						ps2.setLong(2,Long.parseLong(rs.getString("ic_pregunta")));
						rs2 = ps2.executeQuery();

						HashMap mpDataResp = null;
						List lstRespPreg = new ArrayList();
						while(rs2.next()){
							mpDataResp = new HashMap();
							mpDataResp.put("ICPREGUNTA",rs2.getString("ic_pregunta"));
							mpDataResp.put("CGPREGUNTA",rs2.getString("cg_pregunta"));
							mpDataResp.put("TIPORESP",rs2.getString("tipo_resp"));
							mpDataResp.put("IGORDENPREG",rs2.getString("ig_num_pregunta"));
							mpDataResp.put("IGNUMPREG",rs2.getString("ic_nopregunta"));
							mpDataResp.put("IGNUMOPC",rs2.getString("ig_num_opciones"));
							mpDataResp.put("ICRUBRO",rs2.getString("ic_rubro"));
							mpDataResp.put("ICRESPUESTA",rs2.getString("ic_respuesta"));
							mpDataResp.put("CGRESPUESTA",rs2.getString("cg_respuesta"));
							mpDataResp.put("CGEXCLUTENTE",rs2.getString("cg_excluyentes"));
							mpDataResp.put("IGPONDERACION",rs2.getString("ig_ponderacion"));


							lstRespPreg.add(mpDataResp);
						}
						rs2.close();
						ps2.close();

						hmResp.put("p"+rs.getString("ic_nopregunta"),lstRespPreg);
					}


				}
				rs.close();
				ps.close();

				hmDataEncuesta.put("INFOPREGUNTAS",lstPreg);

				//SE OBTIENEN LAS RESPUESTA  DE LA ENCUESTA
				hmDataEncuesta.put("INFORESPUESTAS",hmResp);


			return hmDataEncuesta;

		}catch(Throwable t){
			throw new AppException("consultaInfoPlantilla(Error): Error al consultar informacion de la encuesta", t);
        } finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("ServiciosEJB::consultaInfoPlantilla(S)");
		}
	}

    public String modificacionPlantillaGral(HashMap DatosAltaEnc, List lstPreguntas, List lstRespuestas,
                                            String cvePlantilla, String usuario,
                                            String guardarComo) throws AppException {
		System.out.println("ServiciosEJB::modificacionPlantillaGral(E)");

		boolean				lbSinError 		= true;
		AccesoDB 			conexBD 		= new AccesoDB();
		PreparedStatement	ps;
		ResultSet			rs;
		String				SQLEncuesta;
		String				cvePlantillaNva = "";

		try{
			conexBD.conexionDB();

			//se obtien datos del alta de la encuesta
			String fp_titulo = (String)DatosAltaEnc.get("fp_titulo");
			String fp_instrucciones = (String)DatosAltaEnc.get("fp_instrucciones");
			String fp_numPreguntas = (String)DatosAltaEnc.get("fp_numPreguntas");


			if(!"S".equals(guardarComo)){
				SQLEncuesta =
                    "UPDATE com_plantilla " + "   SET cg_titulo = ?, " + "       df_modificacion = sysdate, " +
                    "       tt_instrucciones = ?, " + "       ig_num_preguntas = ?, " + "       cg_usuario_mod = ? " +
					" WHERE ic_plantilla = ? ";


				ps = conexBD.queryPrecompilado(SQLEncuesta);
				ps.setString(1, fp_titulo);
				ps.setString(2, fp_instrucciones.replace('\'',' '));
				ps.setInt(3, Integer.parseInt(fp_numPreguntas));
				ps.setString(4, usuario);
				ps.setLong(5,Long.parseLong(cvePlantilla));
				ps.executeUpdate();
				ps.close();


				//SE BORRA DETALLE DE OPCIONES DE RESPUESTA PARA LAS PREGUNTAS
				SQLEncuesta = " DELETE FROM com_plantilla_respuesta WHERE ic_plantilla = ? ";
				ps = conexBD.queryPrecompilado(SQLEncuesta);
				ps.setLong(1,Long.parseLong(cvePlantilla));
				ps.executeUpdate();
				ps.close();

				//SE BORRA PREGUNTAS DE LA SUBASTA
				SQLEncuesta = " DELETE FROM com_plantilla_pregunta WHERE ic_plantilla = ? ";
				ps = conexBD.queryPrecompilado(SQLEncuesta);
				ps.setLong(1,Long.parseLong(cvePlantilla));
				ps.executeUpdate();
				ps.close();

			}else{
                SQLEncuesta = " SELECT seq_com_plantilla.nextval " + "   FROM dual";

				ps = conexBD.queryPrecompilado(SQLEncuesta);
				rs = ps.executeQuery();
				if(rs.next())
					cvePlantillaNva = (rs.getString(1)==null)?"":rs.getString(1);
				rs.close();
				ps.close();

				SQLEncuesta =
					" INSERT INTO com_plantilla"   +
					"             (ic_plantilla, cg_titulo, tt_instrucciones, ig_num_preguntas) "   +
					"      VALUES (?, ?, ?, ?)"  ;

				ps = conexBD.queryPrecompilado(SQLEncuesta);
				ps.setLong(1,Long.parseLong(cvePlantillaNva));
				ps.setString(2, fp_titulo);
				ps.setString(3, fp_instrucciones.replace('\'',' '));
				ps.setInt(4, lstPreguntas.size());
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();

				cvePlantilla = cvePlantillaNva;
			}


			guardaPreguntaPlant(conexBD, lstPreguntas, cvePlantilla);
			guardaRespuestaPlant(conexBD, lstRespuestas, cvePlantilla);
			lbSinError = true;

			return cvePlantilla;

		}catch(Throwable t ){
			lbSinError = false;
			throw new AppException("Error al modificar la plantilla", t);
		}finally{
			conexBD.terminaTransaccion(lbSinError);
			if(conexBD.hayConexionAbierta()){
				conexBD.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::modificacionPlantillaGral(S)");
		}

	}

    public String guardaEncuestaPlant(HashMap DatosAltaEnc, String cvePlantilla,
                                      String tipoAfiliado) throws AppException {
		System.out.println("ServiciosEJB::guardaEncuestaPlant(E)");

		boolean				lbSinError 		= true;
		AccesoDB 			conexBD 		= new AccesoDB();
		PreparedStatement	ps;
		ResultSet			rs;
		PreparedStatement	ps2;
		ResultSet			rs2;
		PreparedStatement	ps3;
		String				SQLEncuesta;
		String				ic_encuesta = "";
		String 				sqlTipoAfil = "";
		String 				paramTipoAfil = "";

		try{
			conexBD.conexionDB();

            SQLEncuesta = " SELECT seq_com_encuesta.nextval " + "   FROM dual";
			ps = conexBD.queryPrecompilado(SQLEncuesta);
			rs = ps.executeQuery();
			if(rs.next())
				ic_encuesta = (rs.getString(1)==null)?"":rs.getString(1);
			rs.close();
			ps.close();

			//se obtien datos del alta de la encuesta
			String fp_titulo = (String)DatosAltaEnc.get("fp_titulo");
			String fp_fec_desde = (String)DatosAltaEnc.get("fp_fec_desde");
			String fp_fec_hasta = (String)DatosAltaEnc.get("fp_fec_hasta");
			String fp_instrucciones = (String)DatosAltaEnc.get("fp_instrucciones");
			String fp_cadena = (String)DatosAltaEnc.get("fp_cadena");
			//String fp_tipoAfiliado = (String)DatosAltaEnc.get("fp_tipoAfiliado");
			String fp_respObligada = (String)DatosAltaEnc.get("fp_respObligada");
			String fp_diasResp = (String)DatosAltaEnc.get("fp_diasResp");
			//String fp_numPreguntas = (String)DatosAltaEnc.get("fp_numPreguntas");
			String fp_selecAfiliados = (String)DatosAltaEnc.get("fp_selecAfiliados");
			String lsAfiliados[] = fp_selecAfiliados.split(",");


			//OBTENER INFO PLANTILLA
			SQLEncuesta =
				"SELECT ic_plantilla, ig_num_preguntas, cg_titulo, " +
				"       TO_CHAR (df_publicacion, 'DD/MM/YYYY') as fecpub, " +
                "       TO_CHAR (df_modificacion, 'DD/MM/YYYY') as fecmod, " + "       tt_instrucciones " +
                "  FROM com_plantilla " + " WHERE ic_plantilla = ? ";

				ps = conexBD.queryPrecompilado(SQLEncuesta);
				ps.setLong(1,Long.parseLong(cvePlantilla));
				rs = ps.executeQuery();

				String numPregPlant = "";
				if(rs.next()){
					numPregPlant = rs.getString("ig_num_preguntas");
				}
				rs.close();
				ps.close();


			for(int i=0;i<lsAfiliados.length;i++) {
				if(i==0){
					SQLEncuesta =
                        " INSERT INTO com_encuesta" + "             (ic_encuesta, cg_titulo," +
						"              df_ini_encuesta, df_fin_encuesta, "   +
						" 			 tt_instrucciones, ig_num_preguntas, CS_OBLIGATORIO, IC_DIASPRE)"   +
                        "      VALUES (?, ?,  " + " 	 		 TO_DATE (?, 'DD/MM/YYYY'), TO_DATE (?, 'DD/MM/YYYY'), " +
						" 			 ?, ?,?, ?)"  ;

					ps = conexBD.queryPrecompilado(SQLEncuesta);
					ps.setLong(1,Long.parseLong(ic_encuesta));
					ps.setString(2, fp_titulo);
					ps.setString(3, fp_fec_desde);
					ps.setString(4, fp_fec_hasta);
					ps.setString(5, fp_instrucciones.replace('\'',' '));
					ps.setInt(6, Integer.parseInt(numPregPlant));
					ps.setString(7, fp_respObligada);
					ps.setInt(8, Integer.parseInt(fp_diasResp));
					ps.executeUpdate();
					ps.clearParameters();
					ps.close();
				}


                if ("P".equals(tipoAfiliado)) {
                    sqlTipoAfil = " ic_pyme, ic_epo";
                    paramTipoAfil = " ?, ?";
                }
                if ("I".equals(tipoAfiliado)) {
                    sqlTipoAfil = " ic_if";
                    paramTipoAfil = " ?";
                }
                if ("E".equals(tipoAfiliado)) {
                    sqlTipoAfil = " ic_epo";
                    paramTipoAfil = " ?";
                }

				SQLEncuesta =
                    " INSERT INTO comrel_encuesta_afiliado" + "             (ic_encuesta_afil, ic_encuesta, " +
                    sqlTipoAfil + ",cg_afiliado	)" + "      VALUES (seq_comrel_encuesta_afiliado.nextval, ?," +
                    paramTipoAfil + " ,?)";

					int p=0;
					ps = conexBD.queryPrecompilado(SQLEncuesta);
					ps.setLong(++p, Long.parseLong(ic_encuesta));
					if("P".equals(tipoAfiliado)){
						ps.setLong(++p, Long.parseLong(lsAfiliados[i]));
						ps.setLong(++p, Long.parseLong(fp_cadena));
					}else if("I".equals(tipoAfiliado) || "E".equals(tipoAfiliado)){
						ps.setLong(++p, Long.parseLong(lsAfiliados[i]));
					}
					ps.setString(++p, tipoAfiliado);
					ps.executeUpdate();
					ps.clearParameters();
					ps.close();
			}//for(int i=0;i<lsAfiliados.length;i++)


            SQLEncuesta =
                "SELECT  preg.ic_pregunta, preg.ic_plantilla, preg.ig_num_pregunta, preg.cs_tipo_pregunta, preg.ic_tipo_respuesta,  " +
				"        preg.cg_pregunta, preg.ig_num_opciones, preg.ic_rubro, preg.ic_nopregunta  " +
                "    FROM com_plantilla_pregunta preg " + "   WHERE preg.ic_plantilla = ? " +
				"ORDER BY preg.ic_pregunta ";

			ps = conexBD.queryPrecompilado(SQLEncuesta);
			ps.setLong(1, Long.parseLong(cvePlantilla));
			rs = ps.executeQuery();

			String cvePregunta = "";
			while(rs.next()){
				SQLEncuesta = "select seq_com_pregunta.NEXTVAL from dual ";
				ps2 = conexBD.queryPrecompilado(SQLEncuesta);
				rs2 = ps2.executeQuery();
				while(rs2.next()){
					cvePregunta = rs2.getString(1);
				}
				rs2.close();
				ps2.close();

                SQLEncuesta =
                    " INSERT INTO com_pregunta" +
				"             (ic_pregunta, ic_encuesta, ig_num_pregunta, cs_tipo_pregunta, ic_tipo_respuesta, cg_pregunta, ig_num_opciones, ic_rubro, ic_nopregunta)"   +
				"			VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?) ";

				ps2 = conexBD.queryPrecompilado(SQLEncuesta);
				ps2.setLong(1, Long.parseLong(cvePregunta));
				ps2.setLong(2, Long.parseLong(ic_encuesta));
				ps2.setLong(3, Long.parseLong(rs.getString("ig_num_pregunta")));
				ps2.setString(4, rs.getString("cs_tipo_pregunta"));
				ps2.setLong(5, Long.parseLong(rs.getString("ic_tipo_respuesta")));
				ps2.setString(6, rs.getString("cg_pregunta"));
				ps2.setLong(7, Long.parseLong(rs.getString("ig_num_opciones")));
				ps2.setLong(8, Long.parseLong(rs.getString("ic_rubro")));
				ps2.setLong(9, Long.parseLong(rs.getString("ic_nopregunta")));
				ps2.executeUpdate();
				ps2.close();

                SQLEncuesta =
                    "SELECT   resp.ic_respuesta ic_respuesta_r, resp.ic_plantilla ic_plantilla_r, " +
							"         resp.ic_pregunta ic_pregunta_r, " +
							"         resp.ic_tipo_respuesta ic_tipo_respuesta_r, " +
							"         resp.cg_excluyentes cg_excluyentes_r, " +
                    "         resp.ig_ponderacion ig_ponderacion_r, " + "         resp.cg_respuesta cg_respuesta_r " +
							"    FROM com_plantilla_respuesta resp " +
                    "   WHERE resp.ic_plantilla = ? AND resp.ic_pregunta = ? " + "ORDER BY resp.ic_respuesta ";

				ps2 = conexBD.queryPrecompilado(SQLEncuesta);
				ps2.setLong(1, Long.parseLong(cvePlantilla));
				ps2.setLong(2, Long.parseLong(rs.getString("ic_pregunta")));
				rs2 = ps2.executeQuery();
				while(rs2.next()){
                    SQLEncuesta =
                        " INSERT INTO com_respuesta (ic_respuesta, ic_encuesta, ic_pregunta, ic_tipo_respuesta, cg_excluyentes, ig_ponderacion, cg_respuesta) " +
								" VALUES (seq_com_respuesta.nextval, ?, ?, ?, ?, ?, ? )";

					ps3 = conexBD.queryPrecompilado(SQLEncuesta);
					ps3.setLong(1, Long.parseLong(ic_encuesta));
					ps3.setLong(2, Long.parseLong(cvePregunta));
					ps3.setLong(3, Long.parseLong(rs2.getString("ic_tipo_respuesta_r")));
					ps3.setString(4, rs2.getString("cg_excluyentes_r"));
					ps3.setString(5, rs2.getString("ig_ponderacion_r"));
					ps3.setString(6, rs2.getString("cg_respuesta_r"));
					ps3.executeUpdate();
					ps3.close();

				}
				rs2.close();
				ps2.close();

			}

			rs.close();
			ps.close();

			lbSinError = true;

			return ic_encuesta;

		}catch(Throwable t ){
			lbSinError = false;
			throw new AppException("Error al guardar la encuesta con plantilla", t);
		}finally{
			conexBD.terminaTransaccion(lbSinError);
			if(conexBD.hayConexionAbierta()){
				conexBD.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::guardaEncuestaPlant(S)");
		}

	}

	/**Pantalla Admin Nafin Administraci�n/Servicios/Encuestas
	 * es para guardar la Fecha que se captura en la consulta de encuestas IF, Pyme, EPO
	 * Fodea 036-2014
	 * @throws netropology.utilerias.AppException
	 * @param ic_encuesta
	 * @param fecha_final
	 */
	public void guardaFechaFinalEncuesta( String fecha_final, String ic_encuesta) throws AppException {
		System.out.println("ServiciosEJB::guardaEncuestaPlant(E)");

		boolean				lbSinError 		= true;
		AccesoDB 			con 		= new AccesoDB();
		PreparedStatement	ps;
		StringBuffer 	SQL = new StringBuffer();
		List varBind = new ArrayList();
		try{
			con.conexionDB();

				VectorTokenizer fecha_f = new VectorTokenizer(fecha_final, "|");
				Vector fecha_fi=fecha_f.getValuesVector();

				VectorTokenizer n_encues_ = new VectorTokenizer(ic_encuesta, "|");
				Vector n_encuesta=n_encues_.getValuesVector();


				SQL = new StringBuffer();
            SQL.append("UPDATE com_encuesta " + " set df_fin_encuesta  = TO_DATE (?, 'DD/MM/YYYY') " +
				" where ic_encuesta = ? ");

				for(int i=0; i<fecha_fi.size();i++) {

					String fecha = fecha_fi.get(i).toString();
					String no_encuesta = n_encuesta.get(i).toString();
					if (!fecha.equals("") && !no_encuesta.equals("") ){
						varBind = new ArrayList();
						varBind.add(fecha);
						varBind.add(no_encuesta);

						System.out.println("SQL "+SQL);
						System.out.println("varBind "+varBind);

						ps = con.queryPrecompilado(SQL.toString(), varBind);
						ps.executeUpdate();
						ps.close();
					}
				}


		}catch(Throwable t ){
			lbSinError = false;
			throw new AppException("Error al guardar la encuesta con plantilla", t);
		}finally{
			con.terminaTransaccion(lbSinError);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			System.out.println("ServiciosEJB::guardaFechaFinalEncuesta(S)");
		}

	}

    /**
     * Obtiene la lista de ids de mensajes parametrizados que deben mostrarsele
     * al afiliado
     * @param claveAfiliado clave del afiliado (ic_pyme, ic_epo, etc.)
     * @param tipoAfiliado tipo de afiliado (IF, PYME, EPO)
     * @param claveEpoRelacionada Clave de la epo relacionada
     * @return Lista con los ids...
     */


    public List getIdsMsjEdoCta(String claveAfiliado) {
        log.info("getIdsMsjEdoCta(E)");
        Integer iClaveAfiliado = null;

        try {
            if (claveAfiliado == null || claveAfiliado.equals("")) {
                throw new Exception("La clave de afiliado es requerida");
            }
            iClaveAfiliado = new Integer(claveAfiliado);
        } catch (Exception e) {
            throw new AppException("Error en los parametros recibidos", e);
        }

        List params = new ArrayList();
        List idsMensajes = new ArrayList();
        String qrySentencia =
            "select ic_notifica, df_fin_mes, ic_if, ic_proc_edo_cuenta " 
            + " from bi_notifica_edo_cta " 
            + " where cg_visto is null "
            + " and ic_if       =?" +
            " and DF_FIN_MES < trunc(sigfechahabil(sysdate, 6, '-')) " + 
            " and DF_FIN_MES > trunc(sysdate - 360) ";
        params.add(iClaveAfiliado);

        log.debug("getIdsMensajes query=" + qrySentencia);
        log.debug("getIdsMensajes params=" + params);
        AccesoDB con = new AccesoDB();
        try {
            con.conexionDB();
            Registros reg = con.consultarDB(qrySentencia, params);
            while (reg.next()) {
                idsMensajes.add(new Integer(reg.getString("ic_notifica")));
            }
            return idsMensajes;
        } catch (Exception e) {
            throw new AppException("Error al obtener los Ids de los mensajes", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("getIdsMsjEdoCta(S)");
        }
    }

    public Registros obtenerMensajesEdoCta(String claveAfiliado) {
        log.info("obtenerMensajesIniciales(E)");

        Integer iClaveAfiliado = null;
        try {
            if (claveAfiliado == null || claveAfiliado.equals("")) {
                throw new Exception("La clave de afiliado es requerida");
}
            iClaveAfiliado = new Integer(claveAfiliado);

        } catch (Exception e) {
            throw new AppException("Error en los parametros recibidos", e);
        }

        AccesoDB con = new AccesoDB();
        try {
            con.conexionDB();
            Registros mensajes = new Registros();
            List params = new ArrayList();
            List idsMensajes = new ArrayList();
            idsMensajes = this.getIdsMsjEdoCta(claveAfiliado);

            StringBuffer sbSQL = new StringBuffer();
            if (idsMensajes.size() > 0) {
                sbSQL.append("SELECT ic_notifica," + "  to_char(DF_FIN_MES,'dd/mm/yyyy') DF_FIN_MES," +
                             "  bi_notifica_edo_cta.ic_if ic_if," + "  ic_proc_edo_cuenta," +
                             "  comcat_if.cg_nombre_comercial cg_nombre_comercial" +
                             " FROM bi_notifica_edo_cta, comcat_if" +
                             " WHERE bi_notifica_edo_cta.ic_if=comcat_if.ic_if" + " and cg_visto IS NULL" +
                             " AND bi_notifica_edo_cta.ic_notifica IN (" +
                             Comunes.repetirCadenaConSeparador("?", ",", idsMensajes.size()) + ")");
                params = new ArrayList();
                params.addAll(idsMensajes);

                log.debug("obtenerMensajesIniciales query=" + sbSQL);
                log.debug("obtenerMensajesIniciales params=" + params);

                mensajes = con.consultarDB(sbSQL.toString(), params);
            }
            return mensajes;

        } catch (Exception e) {
            throw new AppException("Error al obtener los mensajes iniciales", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("obtenerMensajesIniciales(S)");
        }
    }

    public String guardaVistoEdoCta(String msgini, String gsCveUsuario) throws NafinException {
        AccesoDB con = new AccesoDB();
        String qrySentencia = "";
        boolean bOk = true;

        try {
            con.conexionDB();
            qrySentencia =
                "update bi_notifica_edo_cta set cg_visto='S', df_visto=sysdate, ic_usuario='"+gsCveUsuario+"' where ic_if="+msgini;
            System.out.println("Query para actualizar::: " + qrySentencia);
            con.ejecutaSQL(qrySentencia);
            return "0";
        } catch (Exception e) {
            e.printStackTrace();
            bOk = false;
            return "1";
        } finally {
            if (con.hayConexionAbierta()) {
                con.terminaTransaccion(bOk);
                con.cierraConexionDB();
            }
        }
    }
}


