
/*************************************************************************************
 *
 * Nombre de Clase o de Archivo:  Servicios.java
 *
 * Versi�n:  1.0
 *
 * Fecha Creaci�n:  06 / Marzo / 2002
 *
 * Autor:  Manuel Ramos Mendoza
 *
 * Fecha Ult. Modificaci�n: 06 / Marzo / 2002
 *
 * Descripci�n de Clase:  SERVICIOS
 *
 *************************************************************************************/


package com.netro.servicios;

import java.util.Map;

import com.netro.exception.*;

import netropology.utilerias.AppException;

import java.util.Vector;
import java.util.HashMap;
import java.util.List;

import java.io.File;

import netropology.utilerias.Registros;

import javax.ejb.Remote;

@Remote
public interface Servicios {

    /*********************
     *  	CR�DITO PRODUCTIVO
     ***/
    public Map getDatosCreditoProductivo(String claveEpo, String clavePyme);

    public void actualizaRespuestaCreditoProductivo(String estatusEnvio, String estatusRespuesta, String claveEpo,
                                                    String clavePyme);

    /*********************
     *  	MENSAJES
     ***/

    public abstract void guardaMensaje(String lsNafinElec, String lsNumEPO, String lsCategoria, String lsTitulo,
                                       String ldDesde, String ldHasta, String lsContenido,
                                       String lsNombArchivo) throws NafinException, Exception;


    public abstract void borraMensaje(String lsNumEPO, String lsTitulo, String mensajesEpos) throws NafinException,
                                                                                                    Exception;


    public abstract void modificaMensaje(String lsNumEPO, String lsTitulo1, String lsTitulo2, String ldDesde,
                                         String ldHasta, String lsContenido, String mensajesEpos) throws NafinException,
                                                                                                         Exception;


    /*********************
     *  	CURSOS
     ***/

    public abstract void guardaCurso(String lsCve_Curso, String lsUsuario, String lsCadena, String lsTitulo,
                                     String lsNombArchivo, String ldInicial, String ldFinal,
                                     String lsContenido) throws NafinException, Exception;


    public abstract void borraCurso(String lsCve_Curso, String lsCadena) throws NafinException, Exception;


    public abstract void modificaCurso(String lsCve_Curso, String lsCadena, String lsTitulo, String lsNombArchivo,
                                       String ldInicial, String ldFinal, String lsContenido) throws NafinException,
                                                                                                    Exception;


    /*********************
     *  	SITIOS DE INTERES
     ***/


    public abstract void guardaSitioInteres(String lsCve_Sitio, String lsUsuario, String lsCadena, String lsTitulo,
                                            String lsSitio, String ldInicial, String ldFinal,
                                            String lsContenido) throws NafinException, Exception;


    public abstract void borraSitioInteres(String lsCve_Sitio, String lsCadena) throws NafinException, Exception;


    public abstract void modificaSitioInteres(String lsCve_Curso, String lsCadena, String lsTitulo,
                                              String lsNombArchivo, String ldInicial, String ldFinal,
                                              String lsContenido) throws NafinException, Exception;


    public abstract String obtenNumSitiosInteres() throws NafinException, Exception;


    /*********************
     *  	VIDEO CONFERENCIAS
     ***/


    public abstract void guardaVideoConferencia(String lsCve_Video, String lsUsuario, String lsCadena, String lsTitulo,
                                                String lsNombArchivo, String ldDesde, String ldHasta,
                                                String lsContenido) throws NafinException, Exception;


    public abstract void borraVideoConferencia(String lsCve_Video, String lsCadena) throws NafinException, Exception;


    public abstract void modificaVideoConferencia(String lsCve_Video, String lsCadena, String lsTitulo,
                                                  String lsNombArchivo, String ldInicial, String ldFinal,
                                                  String lsContenido) throws NafinException, Exception;


    /*********************
     *  	ENCUESTAS
     ***/
    public abstract String guardaEncuesta(String lsTitulo, String ldFechaIni, String ldFechaFin, String lsEPO,
                                          String lsAfiliados[], String lsInstrucciones, String lsNumPreguntas,
                                          String lsAfiliado, String obligatoria, String dias) throws NafinException,
                                                                                                     Exception;


    public abstract void guardaPregunta(String lsCveEncuesta, String lsEpo, String lsAfiliados[], List lsPreguntas,
                                        List lsTipoRespuesta, List lsOpciones, List cverubro,
                                        List cvenumeracion) throws NafinException, Exception;


    public abstract void guardaRespuesta(String lsCve_Encuesta, String lsCve_Pregunta, String lsTipoRespuesta,
                                         String lsCve_Respuesta, String lsUsuario, String lsCadena,
                                         String lsContenido) throws NafinException, Exception;


    public abstract void borraRespuesta(String lsCve_Encuesta, String lsCadena) throws NafinException, Exception;


    public abstract void borraPregunta(String lsCve_Encuesta, String lsCadena) throws NafinException, Exception;


    public abstract void borraEncuesta(String lsCve_Encuesta, String lsCadena) throws NafinException, Exception;


    public abstract void modificaEncuesta(String lsCve_Encuesta, String lsCadena, String lsTitulo, String ldDesde,
                                          String ldHasta, String lsContenido, String obligatoria,
                                          String dias) throws NafinException, Exception;


    public abstract int guardaMensajeIni(String tipoAfiliado, String[] cadenaDestino, String[] ifDestino,
                                         String txtFechaDesde, String txtFechaHasta, String txtTitulo,
                                         String txtContenido, String txtArchivo,
                                         String tipo_info) throws NafinException;

    public abstract void modificacionMensajeIni(String icMensajeIni, String strDesde, String strHasta, String strTitulo,
                                                String strContenido) throws NafinException;


    public abstract void bajaMensajeIni(String icMensajeIni) throws NafinException;

    public abstract Vector getMensajeIni(String icMensajeIni) throws NafinException;

    public String guardaEstatusCamp(String sts, String msgini, String icepo, String icpyme) throws NafinException;

    //****************************************************************************** FODEA 032 - 2008 PROMOCION A PYMES	(I) ACF
    public String recibePromociones(String icPyme, String bancoFondeo) throws NafinException;

    public HashMap consultaPromocionPyme(String ic_pyme) throws NafinException;

    public void actualizaPromociones(HashMap condiciones) throws NafinException;

    public HashMap consultaPromociones(HashMap condiciones) throws NafinException;
    //****************************************************************************** FODEA 032 - 2008 PROMOCION A PYMES	(F) ACF
    //****************************************************************************** FODEA 048 - 2008 Factoraje M�vil	(I) ACF
    public HashMap getInformacionPyme(String icPyme) throws NafinException;

    public List getDatosPymeFactorajeMovil(String icPyme) throws NafinException;

    public void guardarRegistroFactorajeMovil(List datosRegistroFactorajeMovil) throws NafinException;

    public void activarServicioFactorajeMovil(List datosActivacionFactorajeMovil) throws NafinException;

    public String getBancoFondeo(String icEpo) throws NafinException;

    public String getDiasPreviosVencimiento() throws NafinException;

    public File getArchivoTerminosCondiciones(String bancoFondeo, String icTerminos,
                                              String directorioTmp) throws NafinException;

    public void enviaSMSFactorajeMovilPublicacion() throws NafinException;

    public void enviaSMSFactorajeMovilVencimiento() throws NafinException;

    public String getVersionTerminosCondiciones(String bancoFondeo) throws NafinException;

    public String servicioAceptado(String icPyme, String bancoFondeo) throws NafinException;
    //****************************************************************************** FODEA 048 - 2008 Factoraje M�vil	(F) ACF
    public void generaBitacoraGlobalDocumentos() throws NafinException; //FODEA 028 - 2009 ACF

    public void generaBitDoctosOperadosNotificados() throws NafinException; //FODEA 028 - 2009 ACF

    public void generaBitDoctosVencidosNotificados() throws NafinException; //FODEA 028 - 2009 ACF

    public void generaBitDoctosNotificados(String mes, String anio); //FODEA 052 - 2010 JRSW

    public void generaBitCostoEIngreso(String mes, String anio); //FODEA 052 - 2010 JRSW

    public void enviaCorreoBeneficiarioPublicacion(String strDirectorioPublicacion) throws AppException; //FODEA 032 - 2010 ACF

    //FODEA 056 FVR
    public void modificaEncuestaGral(String lsCve_Encuesta, String titulo, String fec_ini, String fec_fin,
                                     String instrucciones, List preguntas, List tipoRespuesta, List numOpciones,
                                     List lstRespPreg, String obligatoria, String dias, List cverubro,
                                     List cvenumeracion) throws NafinException;

    public List hayEncuestas(String strTipoUsuario, String iNoCliente,
                             String iNoEPO) throws AppException; //FODEA 00 - 2011 DLHC

    public int hayEncuestasObligatoria(String perfil, String iNoCliente,
                                       String iNoEPO) throws AppException; //FODEA 00 - 2011 DLHC

    public Registros obtenerMensajesIniciales(String claveAfiliado, String tipoAfiliado, String claveEpoRelacionada,
                                              String claveMensaje);

    public List getIdsMensajes(String claveAfiliado, String tipoAfiliado, String claveEpoRelacionada);

    public Registros obtenerEncuestas(String perfil, String claveAfiliado, String claveEpoRelacionada);

    public Map obtenerEncuesta(String claveEncuestaAfiliado);

    public void guardaRespuestaAfiliado(String claveEncuestaAfiliado, String strLogin, Map mRespuestas);

    public boolean mostrarMensajeCredito(String clavePyme, String claveEpoRelacionado);

    public void enviaSMSFactorajeMovilOfertaTasas() throws NafinException;

    public String guardaEncuestaGral(HashMap DatosAltaEnc, List lstPreguntas, List guardaEncuestaGral,
                                     String tipoAfiliado) throws AppException;

    public List consultaEncuestas(String cveEncuesta, String cveEpo, String cveTipoAfil, String fechaIni,
                                  String fechaFin, String tipoAfiliado) throws AppException;

    public List consultaRespuestas(String tipoAfiliado) throws AppException;

    public List consultaRespIndividual(String cveEncuesta, String cveAfiliado, String tipoAfiliado) throws AppException;

    public List consultaRespConsolidada(String cveEncuesta, String cveAfiliado,
                                        String tipoAfiliado) throws AppException;

    public HashMap consultaInfoEncuesta(String cveEncuesta, String tipoAfiliado) throws AppException;

    public String modificacionEncuestaGral(HashMap DatosAltaEnc, List lstPreguntas, List lstRespuestas,
                                           String cveEncuesta) throws AppException;

    public String guardaPlantillaGral(HashMap DatosAltaEnc, List lstPreguntas, List lstRespuestas,
                                      String tipoAfiliado) throws AppException;

    public List consultaPlantillas(String cvePlantilla, String fechaIni, String fechaFin) throws AppException;

    public void borraPlantilla(String cvePlantilla) throws AppException;

    public HashMap consultaInfoPlantilla(String cveEncuesta, String tipoAfiliado) throws AppException;

    public String modificacionPlantillaGral(HashMap DatosAltaEnc, List lstPreguntas, List lstRespuestas,
                                            String cvePlantilla, String usuario,
                                            String guardarComo) throws AppException;

    public String guardaEncuestaPlant(HashMap DatosAltaEnc, String cvePlantilla,
                                      String tipoAfiliado) throws AppException;

    public void guardaFechaFinalEncuesta(String fecha_final, String ic_encuesta) throws AppException; //Fodea 036-2014

    public List getIdsMsjEdoCta(String claveAfiliado);

    Registros obtenerMensajesEdoCta(String claveAfiliado);

    String guardaVistoEdoCta(String msgini, String gsCveUsuario) throws NafinException;
}
