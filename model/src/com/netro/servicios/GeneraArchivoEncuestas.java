package com.netro.servicios;

import com.netro.pdf.ComunesPDF;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.io.Serializable;


public class GeneraArchivoEncuestas  implements Serializable{
	public GeneraArchivoEncuestas() {	}
	
	
	public String generaRespIndPDF(HashMap hmSession,String path, String cveEncuesta, String cveAfiliado, String tipoAfiliado) throws AppException{
		String nombreArchivo	=	"";
		try{
			
			ServiciosBean servicios = new ServiciosBean();
			//Se obtien objeto List que contien info de la respuesta individual
			List lstRespInd = servicios.consultaRespIndividual(cveEncuesta, cveAfiliado, tipoAfiliado);
			
			//Inicia creacion del PDF
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			
			String strPais = (String)hmSession.get("strPais");
			String iNoNafinElectronico = (String)hmSession.get("iNoNafinElectronico");
			String sesExterno = (String)hmSession.get("sesExterno");
			String strNombre = (String)hmSession.get("strNombre");
			String strNombreUsuario = (String)hmSession.get("strNombreUsuario");
			String strLogo = (String)hmSession.get("strLogo");
			String strDirectorioPublicacion = (String)hmSession.get("strDirectorioPublicacion");
			
			
			pdfDoc.encabezadoConImagenes(pdfDoc,strPais, iNoNafinElectronico,sesExterno, strNombre,
													strNombreUsuario, strLogo,strDirectorioPublicacion);
			
			
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
			
			
			
			if(lstRespInd!=null && lstRespInd.size()>0){
				pdfDoc.setTable(2,100);
				pdfDoc.setCell("ENCUESTA","celda01rep",ComunesPDF.CENTER,2);
				
				for(int x=0; x<lstRespInd.size();x++){
					HashMap hmPreg = lstRespInd.get(x)==null?new HashMap():(HashMap)lstRespInd.get(x);
					String pregRubro = (String)hmPreg.get("CGPREGUNTA");
					String tipoResp = (String)hmPreg.get("TIPORESP");
					
					pdfDoc.setCell(pregRubro,"celda01",ComunesPDF.LEFT,2);
					
					if(!"4".equals(tipoResp)){
						List lstResp = (List)hmPreg.get("LSTRESP");
						if(lstResp!=null && lstResp.size()>0){
							int totResp = lstResp.size();
							for(int y=0; y<lstResp.size();y++){
								HashMap hmResp = lstResp.get(y)==null?new HashMap():(HashMap)lstResp.get(y);
								
								if("1".equals(tipoResp)){
									pdfDoc.setCell((String)hmResp.get("CGCONTESTARESP"),"formas",ComunesPDF.LEFT,2);
								}
								if(!"1".equals(tipoResp)){
									if("5".equals(tipoResp)){
										pdfDoc.setCell((String)hmResp.get("CGRESPUESTA")+" = "+(String)hmResp.get("CGCONTESTARESP"),"formas",ComunesPDF.LEFT,1);
									}else{
										pdfDoc.setCell((String)hmResp.get("CGRESPUESTA"),"formas",ComunesPDF.LEFT,1);
									}
									if(totResp%2!=0 && (totResp-1)==y){
										pdfDoc.setCell(" ","formas",ComunesPDF.LEFT,1);	
									}
								}
							}//for(int y=0; x<lstResp.size();y++)
						}//if(lstResp!=null && lstResp.size()>0)
					}//if(!"4".equals(tipoResp))
					
				}
				
				pdfDoc.addTable();
			}
			
			pdfDoc.endDocument();
			
			return nombreArchivo;
			
			
		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error al generar PDF de Respuesta Individual", t);
		}
	}
	
	
	public String generaRespIndCSV(HashMap hmSession,String path, String cveEncuesta, String cveAfiliado, String tipoAfiliado) throws AppException{
		String nombreArchivo	=	"";
		StringBuffer contenidoArchivo = new StringBuffer();
		try{
			
			ServiciosBean servicios = new ServiciosBean();
			//Se obtien objeto List que contien info de la respuesta individual
			List lstRespInd = servicios.consultaRespIndividual(cveEncuesta, cveAfiliado, tipoAfiliado);
			
			//Inicia creacion del PDF
			CreaArchivo archivo = new CreaArchivo();
			//nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			
			contenidoArchivo.append("\n"+"Nombre Afiliado "+"\n");
			contenidoArchivo.append("\n"+"ENCUESTA"+"\n");
			
			
			
			if(lstRespInd!=null && lstRespInd.size()>0){
				
				for(int x=0; x<lstRespInd.size();x++){
					HashMap hmPreg = lstRespInd.get(x)==null?new HashMap():(HashMap)lstRespInd.get(x);
					String pregRubro = (String)hmPreg.get("CGPREGUNTA");
					String tipoResp = (String)hmPreg.get("TIPORESP");
					
					contenidoArchivo.append("\n"+pregRubro.replaceAll("\\r"," ").replaceAll("\\n", " ").replace('\"', ' ').replace('\\', ' ')+"\n");
					
					if(!"4".equals(tipoResp)){
						List lstResp = (List)hmPreg.get("LSTRESP");
						if(lstResp!=null && lstResp.size()>0){
							int totResp = lstResp.size();
							int respeti = 0;
							for(int y=0; y<lstResp.size();y++){
								HashMap hmResp = lstResp.get(y)==null?new HashMap():(HashMap)lstResp.get(y);
								
								if("1".equals(tipoResp)){
									contenidoArchivo.append(","+((String)hmResp.get("CGCONTESTARESP")).replace(',', ' ') );
								}
								if(!"1".equals(tipoResp)){
									
									if("5".equals(tipoResp)){
										contenidoArchivo.append(","+((String)hmResp.get("CGRESPUESTA")).replace(',', ' ')+" = "+(String)hmResp.get("CGCONTESTARESP"));
									}else{
										contenidoArchivo.append(","+((String)hmResp.get("CGRESPUESTA")).replace(',', ' ') );
									}
									if(totResp%2!=0 && (totResp-1)==y){
										contenidoArchivo.append("\n");
									}else{
										respeti++;
										if(respeti==2){
											respeti=0;
											contenidoArchivo.append("\n");
										}
									}
								}
							}//for(int y=0; x<lstResp.size();y++)
						}//if(lstResp!=null && lstResp.size()>0)
					}//if(!"4".equals(tipoResp))
					
				}
				
			}
			
			if(!archivo.make(contenidoArchivo.toString(), path, ".csv")){
					nombreArchivo = "error";
			}else{
				nombreArchivo = archivo.nombre;
			}
			
			System.out.println("nombreArchivo======="+nombreArchivo);
			return nombreArchivo;
			
			
		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error al generar PDF de Respuesta Individual", t);
		}
	}
	
	
	
	public String generaRespConsPDF(HashMap hmSession,String path, String cveEncuesta, String cveAfiliado, String tipoAfiliado) throws AppException{
		String nombreArchivo	=	"";
		try{
			
			ServiciosBean servicios = new ServiciosBean();
			//Se obtien objeto List que contien info de la respuesta individual
			List lstRespInd = servicios.consultaRespConsolidada(cveEncuesta, cveAfiliado, tipoAfiliado);
			
			//Inicia creacion del PDF
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			
			String strPais = (String)hmSession.get("strPais");
			String iNoNafinElectronico = (String)hmSession.get("iNoNafinElectronico");
			String sesExterno = (String)hmSession.get("sesExterno");
			String strNombre = (String)hmSession.get("strNombre");
			String strNombreUsuario = (String)hmSession.get("strNombreUsuario");
			String strLogo = (String)hmSession.get("strLogo");
			String strDirectorioPublicacion = (String)hmSession.get("strDirectorioPublicacion");
			
			
			pdfDoc.encabezadoConImagenes(pdfDoc,strPais, iNoNafinElectronico,sesExterno, strNombre,
													strNombreUsuario, strLogo,strDirectorioPublicacion);
			
			
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
			
			
			
			if(lstRespInd!=null && lstRespInd.size()>0){
				pdfDoc.setTable(2,100);
				pdfDoc.setCell("ENCUESTA","celda01rep",ComunesPDF.CENTER,2);
				
				for(int x=0; x<lstRespInd.size();x++){
					HashMap hmPreg = lstRespInd.get(x)==null?new HashMap():(HashMap)lstRespInd.get(x);
					String pregRubro = (String)hmPreg.get("CGPREGUNTA");
					String tipoResp = (String)hmPreg.get("TIPORESP");
					
					pdfDoc.setCell(pregRubro,"celda01",ComunesPDF.LEFT,2);
					
					if(!"4".equals(tipoResp)){
						List lstResp = (List)hmPreg.get("LSTRESP");
						if(lstResp!=null && lstResp.size()>0){
							int totResp = lstResp.size();
							for(int y=0; y<lstResp.size();y++){
								HashMap hmResp = lstResp.get(y)==null?new HashMap():(HashMap)lstResp.get(y);
								
								String tipoRespPond = (String)hmResp.get("TIPORESP");
								List lstRespPond = (List)hmResp.get("LSTRESPPOND");
								if("5".equals(tipoRespPond)){
									if(lstRespPond!=null && lstRespPond.size()>0){
										//int totResp = lstResp.size();
										for(int z=0; z<lstRespPond.size();z++){
											HashMap hmRespPond = lstRespPond.get(z)==null?new HashMap():(HashMap)lstRespPond.get(z);
											if(z==0){
												pdfDoc.setCell((String)hmResp.get("CGRESPUESTA"),"formas",ComunesPDF.LEFT,2);
											}
											pdfDoc.setCell((String)hmRespPond.get("CONTESTARESP"),"formas",ComunesPDF.LEFT,1);
											pdfDoc.setCell((String)hmRespPond.get("CONTRESP"),"formas",ComunesPDF.LEFT,1);
										}
									}
								
								}else{								
									pdfDoc.setCell((String)hmResp.get("CGRESPUESTA"),"formas",ComunesPDF.LEFT,1);
									pdfDoc.setCell((String)hmResp.get("CONTRESP"),"formas",ComunesPDF.LEFT,1);
								}
								
							}//for(int y=0; x<lstResp.size();y++)
						}//if(lstResp!=null && lstResp.size()>0)
					}//if(!"4".equals(tipoResp))
					
				}
				
				pdfDoc.addTable();
			}
			
			pdfDoc.endDocument();
			
			return nombreArchivo;
			
			
		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error al generar PDF de Respuesta Individual", t);
		}
	}
	
	
	
	public String generaRespConsCSV(HashMap hmSession,String path, String cveEncuesta, String cveAfiliado, String tipoAfiliado) throws AppException{
		String nombreArchivo	=	"";
		StringBuffer contenidoArchivo = new StringBuffer();
		try{
			
			ServiciosBean servicios = new ServiciosBean();
			//Se obtien objeto List que contien info de la respuesta individual
			List lstRespInd = servicios.consultaRespConsolidada(cveEncuesta, cveAfiliado, tipoAfiliado);
			
			//Inicia creacion del PDF
			CreaArchivo archivo = new CreaArchivo();
			//nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			
			contenidoArchivo.append("\n"+"ENCUESTA"+"\n");
			
			
			
			if(lstRespInd!=null && lstRespInd.size()>0){
				
				for(int x=0; x<lstRespInd.size();x++){
					HashMap hmPreg = lstRespInd.get(x)==null?new HashMap():(HashMap)lstRespInd.get(x);
					String pregRubro = (String)hmPreg.get("CGPREGUNTA");
					String tipoResp = (String)hmPreg.get("TIPORESP");
					
					contenidoArchivo.append("\n"+pregRubro.replaceAll("\\r"," ").replaceAll("\\n", " ").replace('\"', ' ').replace('\\', ' ')+"\n");
					
					if(!"4".equals(tipoResp)){
						List lstResp = (List)hmPreg.get("LSTRESP");
						if(lstResp!=null && lstResp.size()>0){
							int totResp = lstResp.size();
							int respeti = 0;
							for(int y=0; y<lstResp.size();y++){
								HashMap hmResp = lstResp.get(y)==null?new HashMap():(HashMap)lstResp.get(y);
								
								String tipoRespPond = (String)hmResp.get("TIPORESP");
								List lstRespPond = (List)hmResp.get("LSTRESPPOND");
								if("5".equals(tipoRespPond)){
									if(lstRespPond!=null && lstRespPond.size()>0){
										//int totResp = lstResp.size();
										for(int z=0; z<lstRespPond.size();z++){
											HashMap hmRespPond = lstRespPond.get(z)==null?new HashMap():(HashMap)lstRespPond.get(z);
											if(z==0){
												contenidoArchivo.append(","+((String)hmResp.get("CGRESPUESTA")).replace(',', ' ') );
												contenidoArchivo.append("\n");
											}
											contenidoArchivo.append(","+((String)hmRespPond.get("CONTESTARESP")).replace(',', ' ') );
											contenidoArchivo.append(","+((String)hmRespPond.get("CONTRESP")).replace(',', ' ') );
											contenidoArchivo.append("\n");
										}
									}
								
								}else{
								
									contenidoArchivo.append(","+((String)hmResp.get("CGRESPUESTA")).replace(',', ' ') );
									contenidoArchivo.append(","+((String)hmResp.get("CONTRESP")).replace(',', ' ') );
									contenidoArchivo.append("\n");

								}
								
							
							}//for(int y=0; x<lstResp.size();y++)
						}//if(lstResp!=null && lstResp.size()>0)
					}//if(!"4".equals(tipoResp))
					
				}
				
			}
			
			if(!archivo.make(contenidoArchivo.toString(), path, ".csv")){
					nombreArchivo = "error";
			}else{
				nombreArchivo = archivo.nombre;
			}
			
			System.out.println("nombreArchivo======="+nombreArchivo);
			return nombreArchivo;
			
			
		}catch(Throwable t){
			t.printStackTrace();
			throw new AppException("Error al generar PDF de Respuesta Individual", t);
		}
	}
	
	
}