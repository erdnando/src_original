
/*************************************************************************************
*
* Nombre de Clase o de Archivo:  InteligenciaComercial.java
*
* Versi�n:  1.0
*
* Fecha Creaci�n:  19/05/2006
*
* Autor:  Ricardo Fuentes
*
* Descripci�n de Clase:  TEMPLATES
*
*************************************************************************************/



package com.netro.cadenas;

import com.netro.exception.NafinException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import netropology.utilerias.AppException;

@Remote
public interface InteligenciaComercial {


	public Map consultaParamEnvio(String action
		,String mailAgrega
		,String icEnvios[]
		,String origenes[]
		,String correos[]
		,String mensuales[]
		,String diarios[]
		,int icProductoNafin)
		throws NafinException;
		
	public void guardarParamEnvio(
		String envMensActivar
		,String envDiarioActivar
		,String icEnvios[]
		,String origenes[]
		,String correos[]
		,String mensuales[]
		,String diarios[]
		,int icProductoNafin) throws NafinException;


	public Map procesarArchivo(List lDatos,int anio,String mes)
		throws NafinException;
	
	public List getMensajesCargadosTmp(String icProceso)
		throws NafinException;
		
	public List procesarMensajesCargados(String icProceso,String anio,String mes)
		throws NafinException;

	public List consultaEposMensajes()		
		throws NafinException;
		
	public List consultaEstadisticas(String anio,String mes,String estatus,String icEpo)	
		throws NafinException;

	public List consultaEstadisticas(String anio,String mes,String estatus,String icEpos[])	
		throws NafinException;
		
	public List consultaResumenEst(String anio,String mes,String estatus,String icEpo)	
		throws NafinException;

	public List consultaResumenEst(String anio,String mes,String estatus,String icEpos[])	
		throws NafinException;		
		
	public List consultaDetallePorEpo(String icEpo,String respuesta,String anio,String mes)		
		throws NafinException;

	public List consultaDetallePorEpo(String icEpos[],String respuesta,String anio,String mes)		
		throws NafinException;
		
	public void cambiarEstatusEnvio(String icEpos[],String accion)
		throws NafinException;

	public String consultaClientesPotenciales(String rutaArchivo, String rutaVirtual);
		
	public Map consultaMesesAnios(String anio,String mes)
		throws NafinException;
    
  public List reporteOperacionesEnCadenas(String hashDatosEntrada, String banco_fondeo, String ic_epo, String fecha_inicial, String fecha_final)
    throws NafinException;//FODEA 013 - 2009 ACF
    

  public List reporteOperacionesDescuentoAutomatico(List parametros_reporte)
    throws NafinException;//FODEA 013 - 2009 ACF

  public List obtenerPymesDescuentoAutomatico(List parametros_detalle)
    throws NafinException;//FODEA 013 - 2009 ACF
    
  public List obtenerCatalogoBaseOperacion(String ic_if)
    throws NafinException;//FODEA 013 - 2009 ACF
    
  public List obtenerCatalogoIF(String base_operacion)
    throws NafinException;//FODEA 013 - 2009 ACF
    
  public List obtenerCatalogoTipoCredito(String ic_if, String base_operacion) 
    throws NafinException;//FODEA 013 - 2009 ACF
    
  public List reporteCreditoElectronicoBO(List parametros_reporte)
    throws NafinException;//FODEA 013 - 2009 ACF
    
  public List obtenerDetalleCreditoElectronico(List parametros_reporte)
    throws NafinException;//FODEA 013 - 2009 ACF
		
	public List reporteClientesRegNafinsaMovil(String num_nae, String claveAnioI, String claveAnioF, String claveMesI, String claveMesF, String claveAnioC, String claveMesC)
	  throws NafinException;//FODEA 052 - 2010 Rebos
		
	public List clientesNafinsaMovilDetalle(String anioDetalle, String numMes)
		throws NafinException;//FODEA 052 - 2010 Rebos
		
	public List reporteNegociablesNafinsaMovil(String tipo_docto, String claveAnioI, String claveAnioF, String claveMesI, String claveMesF)
		throws NafinException;//FODEA 052 - 2010 Rebos
		
	public List reporteNafinsaMovilDetalle(String tipo_docto, String anioDetalle, String numMes)
		throws NafinException;//FODEA 052 - 2010 Rebos
		
	public List reporteNafinsaMovilDetalleTotal(String tipo_docto, String anioDetalle, String numMes)
		throws NafinException;//FODEA 052 - 2010 Rebos	
	
	public List reporteCostoEIngresoNafinsaMovil(String claveAnioI, String claveAnioF, String claveMesI, String claveMesF)
		throws NafinException;//FODEA 052 - 2010 Rebos
		
	public List reporteCostoEIngresoDetalle(String anioDetalle, String numMes)
		throws NafinException;//FODEA 052 - 2010 Rebos
		
	public List reporteCostoEIngresoGlobal(String anioDetalle, String numMes)
		throws NafinException;//FODEA 052 - 2010 Rebos

	public List reporteOperadoNegociableNafinsaMovil(String fecha_operacion_ini, String fecha_operacion_fin) 
		throws NafinException;//FODEA 028 - 2009 eBa	
		
	public List reporteDoctosGlobalNafinsaMovil(String fecha_operacion_ini, String fecha_operacion_fin) 
		throws NafinException;//FODEA 028 - 2009 eBa	
		
	public List reportePymesGlobalNafinsaMovil() 
		throws NafinException;//FODEA 028 - 2009 eBa	
		
	public List reporteVencidosNotificadosNafinsaMovil(String fecha_vencimiento_ini, String fecha_vencimiento_fin) 
		throws NafinException;//FODEA 028 - 2009 eBa	
	
	public List obtenerCatalogoMeses() 
		throws AppException;//FODEA 001 - 2010 ACF
	
	public List obtenerCatalogoAnios() 
		throws AppException;//FODEA 001 - 2010 ACF
	
	public HashMap generaReporteOperacionPymesCadenasMes(HashMap datosConsulta)
		throws AppException;//FODEA 001 - 2010 ACF
	
	public String validaRelacionPymeIf(String clavePyme, String claveIf)
		throws AppException;//FODEA 033 - 2010 ACF
	
	public void guardarArchivoReporte(String nombreArchivo, String strDirectorioTemp, String origen);
	
	public String generarArchivo(String 	mesInicial,String 	anioInicial,String 	mesFinal,String 	anioFinal,String 	sPeriodo1,String 	sPeriodo2,String 	sPeriodo3,String 	sNoEposSelec,String 	strDirectorioTemp)
		throws AppException;
	
	public String generarArchNafinsaMovil(String strDirectorioPublicacion,String strDirectorioTemp,String num_nae,String claveAnioI,String claveAnioF,String claveMesI,String claveMesF ,String claveAnioC,String claveMesC)
		throws AppException;
	public String generarArchNafinsaMovilDetalle(String strDirectorioPublicacion,String strDirectorioTemp,String anioDetalle,String numMes,String mesDetalle)
		throws AppException;
	
	public void  getEnvioCorreoDiario(String rutaArchivo  , String  rutaVirtual ); 
}