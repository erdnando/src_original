package com.netro.cadenas;

import com.netro.exception.NafinException;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "EstadoCuentaPymeEJB" , mappedName = "EstadoCuentaPymeEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class EstadoCuentaPymeBean implements EstadoCuentaPyme {

	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>IHJ
	
  //Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(EstadoCuentaPymeBean.class);

  
    public List getAvisoPago() throws NafinException{
	System.out.println("EstadoCuentaPymeBean::getAvisoPago(E)");
	AccesoDB 	con 				= new AccesoDB();
	Registros 	consulta 		= null;
	String 		qrySentencia;
	
	 try
	 {
	 	con.conexionDB();
			  qrySentencia = "SELECT est.ig_codigo_cliente COGIDIGO_CLIENTE, "+
				" est.cg_desc_cliente DESC_CLIENTE, "+
				" est.cg_desc_producto_banco DESC_PRODUCTO_BANCO, "+
				" est.ig_numero_prestamo NUMERO_PRESTAMO, "+
				" TO_CHAR (est.df_fecha_apertura, 'dd/mm/yyyy') FECHA_APERTURA, "+
				" mon.cd_nombre MONEDA, "+
				" est.fn_monto_inicial MONTO_INICIAL, "+
				" TO_CHAR (ven.df_periodoinic, 'dd/mm/yyyy') DF_PERIODOINIC, "+
				" TO_CHAR (ven.df_periodofin, 'dd/mm/yyyy') DF_PERIODOFIN, "+
				" ven.fg_totalvencimiento TOTAL_VENCIMIENTO, "+
				" TO_CHAR (ven.com_fechaprobablepago, 'dd/mm/yyyy') COM_FECHAPROBABLEPAGO, "+
				" ven.ig_dias DIAS, "+
				" ven.fg_amortizacion AMORTIZACION, "+
				" ven.fg_interes INTERES "+
				" FROM com_estado_cuenta_diario est, com_vencimiento ven, comcat_moneda mon "+
				" WHERE est.ig_numero_prestamo = ven.ig_prestamo "+
				" AND est.ic_moneda = mon.ic_moneda ";


			consulta = con.consultarDB(qrySentencia , new ArrayList(), true);
	 }

	 catch (Exception e)
	 {	   System.out.println("EstadoCuentaPymeBean::getAvisoPago(E)" +e);
	       e.printStackTrace();
		   throw new NafinException("SIST0001");
	 }
	 finally{
	 	  if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getFechasAviso(S)");
	 }
	 return  new ArrayList();//consulta;
	}

	public List getFechasAviso() throws NafinException {
		System.out.println("EstadoCuentaPymeBean::getFechasAviso(E)");
		AccesoDB 		con				= new AccesoDB();
		List 			renglones		= new ArrayList();
		String			qrySentencia;
		try {
			con.conexionDB();
			
			qrySentencia = 
				" SELECT   SYSDATE fecha"   +
				"     FROM DUAL"   +
				" UNION ALL"   +
				" SELECT   ADD_MONTHS (SYSDATE, -1)"   +
				"     FROM DUAL"   +
				" UNION ALL"   +
				" SELECT   ADD_MONTHS (SYSDATE, -2)"   +
				"     FROM DUAL"   +
				" UNION ALL"   +
				" SELECT   ADD_MONTHS (SYSDATE, -3)"   +
				"     FROM DUAL"   +
				" ORDER BY 1 DESC"  ;
			Registros consulta = con.consultarDB(qrySentencia, new ArrayList(), true);
			while(consulta.next()) {
				List columnas = new ArrayList();
				
				Calendar calendar = Calendar.getInstance();
				
				Date date = new Date(((Timestamp)consulta.getObject("fecha")).getTime());
				
				calendar.setTime(date);
				System.out.println("getFechasAviso::calenda:: " + calendar);
				String fecha = Fecha.getFormatCalendar(calendar, "dd/MM/yyyy");
				columnas.add(fecha);
				columnas.add(Fecha.getMesNombre(fecha));
				calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
				columnas.add(Fecha.getFormatCalendar(calendar, "dd/MM/yyyy"));
				renglones.add(columnas);
			}//while(registros.next())
			
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getFechasAviso(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getFechasAviso(S)");
		}
		return renglones;
	}//getFechasAviso
	
	//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<IHJ
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>JSHD
	public HashMap getEstadoCuentaFinDeMes(String numeroCliente, String fecha) 
	 throws NafinException {
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		HashMap			resultados		= new HashMap();
		String			condicion = "", tabla = "";
		String			qryCondicionTabla = "";
		ResultSet		rs = null;
		
		System.out.println("EstadoCuentaPymeBean::getEstadoCuentaFinDeMes(E)");
		try 
		{	
			con.conexionDB();
			String existRegistro = "";
			boolean pyme_cliente = true;
			
			qryCondicionTabla = " select count(*) from comcat_pyme cp, com_edocta_cat cec "+
													" where cec.ig_codigo_cliente = cp.in_numero_sirac "+
													" and ig_codigo_cliente = "+numeroCliente;
													
			rs = con.queryDB(qryCondicionTabla);
			if(rs.next())	existRegistro = rs.getString(1);
			if(existRegistro.equals("0"))
			{
				qryCondicionTabla = 
							" select count(*) from comcat_clientes cp, com_edocta_cat cec "+
							" where cec.ig_codigo_cliente = cp.ic_cliente "+
							" and ig_codigo_cliente = "+numeroCliente;
				rs = con.queryDB(qryCondicionTabla);
				if(rs.next())	existRegistro = rs.getString(1);
				pyme_cliente = false;
			}
			if(rs!=null)rs.close();
			
			if(!pyme_cliente){
					tabla += " comcat_clientes cli ";
					condicion += " cli.ic_cliente ";						
			}else{				
					tabla += " comcat_pyme cli ";
					condicion += " cli.in_numero_sirac ";
			}
			
			qrySentencia =
					" SELECT   '01/' || TO_CHAR (cta.df_fecha_fin_mes, 'mm/yyyy') fecha_ini, " +
					" TO_CHAR (cta.df_fecha_fin_mes, 'dd/mm/yyyy') fecha_fin, TO_CHAR (cta.df_fecha_fin_mes, 'dd') dias, "+
					" mon.cd_nombre moneda, cta.ig_codigo_cliente cod_cliente, " +
					" TO_CHAR (SYSDATE, 'dd/mm/yyyy') fecha_emision,  " +
					"	DECODE (cli.cs_tipo_persona, " +
               "	'F', cli.cg_appat || ' ' || cli.cg_apmat || ' ' || cli.cg_nombre, " +
					"	'M', cli.cg_razon_social " +
					"	) nombre_razon_social, " +
					" cta.fn_saldo_anterior saldoanterior, cta.fn_monto_operado montooperado, cta.fn_capital_recuperado capitalrecuperado, " +
					" cta.fn_saldo_insoluto saldoinsoluto, cta.fn_comision comision " +

					" FROM com_edocta_cat cta, comcat_moneda mon,"+tabla+										
					" WHERE cta.ig_codigo_moneda = mon.ic_moneda " +
					" AND cta.ig_codigo_cliente = " +condicion+
					" AND ig_codigo_cliente = ? " + 
					" AND cta.df_fecha_fin_mes >= to_date(?, 'dd/mm/yyyy') " +
					" AND cta.df_fecha_fin_mes < to_date(?, 'dd/mm/yyyy')+1 " ;
						
			lVarBind.add(numeroCliente);
			lVarBind.add(fecha);
			lVarBind.add(fecha);
			System.out.println("qrySentencia:::"+qrySentencia);
			System.out.println("lVarBind:::"+lVarBind);
			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next() ){
				
				resultados.put("FECHA_INI",				registros.getString("fecha_ini"));
				resultados.put("FECHA_FIN",				registros.getString("fecha_fin"));
				resultados.put("DIAS",		  				registros.getString("dias"));
				resultados.put("MONEDA",					registros.getString("moneda"));
				resultados.put("FECHA_EMISION",			registros.getString("fecha_emision"));
				resultados.put("CODIG_CLIENTE",			registros.getString("cod_cliente"));
				resultados.put("NOMBRE_RAZON_SOCIAL",	registros.getString("nombre_razon_social"));
				resultados.put("SALDO_ANTERIOR",			Comunes.formatoDecimal(registros.getString("saldoanterior"),2,true));
				resultados.put("MONTO_OPERADO",			Comunes.formatoDecimal(registros.getString("montooperado"),2,true));
				resultados.put("CAPITAL_RECUPERADO",	Comunes.formatoDecimal(registros.getString("capitalrecuperado"),2,true));
				resultados.put("SALDO_INSOLUTO",			Comunes.formatoDecimal(registros.getString("saldoinsoluto"),2,true));
				resultados.put("COMISION",					Comunes.formatoDecimal(registros.getString("comision"),2,true));
				resultados.put("PERIODO", 					"1 al " + registros.getString("dias") + " de " + getMesyAnio(registros.getString("fecha_fin")).toLowerCase());
			}
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getEstadoCuentaFinDeMes(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getEstadoCuentaFinDeMes(S)");
		}	
		return resultados;
	}
	
	/*
	 * Datos sobre las disposiciones para el layout del estado de cuenta por prestamo
	 */
	public ArrayList getDisposicionEstadoCuenta(String numeroPrestamo, String numeroCliente, String fecha)
	 throws NafinException {
		System.out.println("EstadoCuentaPymeBean::getDisposicionEstadoCuenta(E)");
		AccesoDB con 					= new AccesoDB();
		HashMap campos     			= new HashMap();
		ArrayList list 				= new ArrayList();
		List lVarBind					= new ArrayList();
		Registros registros = null;
		
		try
		{
			con.conexionDB();	
			String qryConsulta = 
					" SELECT   ope.ig_numero_prestamo numero_prestamo, TO_CHAR(ope.df_fecha_disposicion, 'dd/mm/YYYY') fecha_disposicion, " +
					" 		ope.fn_monto_dispuesto monto_dispuesto, ope.fn_monto_total monto_total, " +
					"		ope.cg_descripcion_com descripcion_com,  TO_CHAR(ope.df_fecha_pago_comision, 'dd/mm/YYYY') fecha_pago_comision, " +
					"		ope.fn_importe_linea_aut importe_linea_aut, ope.fn_porc_comision porc_comision, " +
					"		ope.fn_importe_comision importe_comision, ope.fn_iva iva, ope.fn_total_pagado total_pagado " +       

					" FROM     com_edocta_cat cta, com_edocta_opera ope  " +
					" WHERE    cta.df_fecha_fin_mes = ope.df_fecha_fin_mes " +
					"		AND cta.ig_codigo_cliente = ope.ig_codigo_cliente " +
					"		AND cta.ig_codigo_moneda = ope.ig_codigo_moneda " +
					" 		AND cta.cg_codigo_base_operacion = ope.cg_codigo_base_operacion " +
					"		AND cta.ig_codigo_cliente = ? " + //condicion general
					"		AND cta.df_fecha_fin_mes >= to_date(?, 'dd/mm/yyyy') " +
					" 		AND cta.df_fecha_fin_mes < to_date(?, 'dd/mm/yyyy')+1 ";
					lVarBind.add(numeroCliente);
					lVarBind.add(fecha);
					lVarBind.add(fecha);
			
			if(!"".equals(numeroPrestamo)){
					qryConsulta += " 		AND ope.ig_numero_prestamo = ? " ; //condicion especifica
					lVarBind.add(numeroPrestamo);
			}
			
			System.out.println("qryConsulta::::::" + qryConsulta);
			System.out.println("lVarBind::::::" + lVarBind);
			registros = con.consultarDB(qryConsulta, lVarBind, false);  
			while(registros != null && registros.next())
			{
				campos = new HashMap();
				campos.put("NUM_PRESTAMO", 			registros.getString("numero_prestamo"));
				campos.put("FECHA_DISPOSICION", 		registros.getString("fecha_disposicion"));
				campos.put("MONTO_DISPUESTO", 		Comunes.formatoDecimal(registros.getString("monto_dispuesto"),2,true));
				campos.put("MONTO_TOTAL", 				Comunes.formatoDecimal(registros.getString("monto_total"),2,true));
				campos.put("DESCRIPCION_COM", 		registros.getString("descripcion_com"));
				campos.put("FECHA_PAGO_COMISION", 	registros.getString("fecha_pago_comision"));
				campos.put("IMPORTE_LINEA", 			Comunes.formatoDecimal(registros.getString("importe_linea_aut"),2,true));
				campos.put("PORC_COMISION", 			registros.getString("porc_comision"));
				campos.put("IMPORTE_COMISION", 		Comunes.formatoDecimal(registros.getString("importe_comision"),2,true));
				campos.put("IVA", 						Comunes.formatoDecimal(registros.getString("iva"),2,true));
				campos.put("TOTAL_PAGADO", 			Comunes.formatoDecimal(registros.getString("total_pagado"),2,true));
				list.add(campos);
			}
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getDisposicionEstadoCuenta(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getDisposicionEstadoCuenta(S)");
		}	
		return list;	
  }//getDisposicionEstadoCuenta
  
  /*
	 * Datos recuperado para el layout del estado de cuenta por prestamo
	 */
  public ArrayList getRecuperadoEstadoCuenta(String numeroPrestamo, String numeroCliente, String fecha)
	 throws NafinException {
		System.out.println("EstadoCuentaPymeBean::getRecuperadoEstadoCuenta(E)");
		AccesoDB con 					= new AccesoDB();
		HashMap campos     			= new HashMap();
		ArrayList list 				= new ArrayList();
		List lVarBind					= new ArrayList();
		Registros registros = null;
		
		try
		{
			con.conexionDB();	
			String qryConsulta = 
					" SELECT   rec.ig_numero_prestamo numero_prestamo, TO_CHAR(df_fecha_pago, 'dd/mm/YYYY')  fecha_pago,  " +
					"		rec.fn_capital capital, rec.fn_pre_pago_cap prepago_capital, rec.fn_int_ordinario interes_ordinario, " +
					" 		rec.fn_int_moratorio interes_moratorio, rec.fn_comisiones comisiones, rec.fn_iva iva,  " +
					" 		rec.fn_subsidio subsidio, rec.fn_total_pagado total_pagado, " +
					
					"		pres.fn_capital_vigente capital_vigente, pres.fn_capital_vencido capital_vencido, " +
					"		pres. fn_tasa_total tasa_total, pres.cg_descripcion_tasa descripcion_tasa, " +
					"		pres.fn_tasa_total_moratoria tasa_moratoria, pres.cg_descripcion_tasa_moratoria descripcion_moratoria, " +
					"		pres.ig_cuotas_vencidas cuotas_vencidas, pres.ig_cuotas_por_emitir cuotas_por_emitir " +
					" FROM     com_edocta_cat cta, com_edocta_recu rec, com_edocta_pres pres " +
					" WHERE    cta.df_fecha_fin_mes = rec.df_fecha_fin_mes " +
					"		AND cta.df_fecha_fin_mes = pres.df_fecha_fin_mes " +
					"		AND cta.ig_codigo_cliente = rec.ig_codigo_cliente " +
					" 		AND cta.ig_codigo_moneda = rec.ig_codigo_moneda " +
					"		AND cta.cg_codigo_base_operacion = rec.cg_codigo_base_operacion " +
					" 		AND cta.ig_codigo_cliente = ? " + //condicion general 
					"		AND cta.df_fecha_fin_mes >= to_date(?, 'dd/mm/yyyy') " +
					" 		AND cta.df_fecha_fin_mes < to_date(?, 'dd/mm/yyyy')+1 " +
					
					"		AND cta.ig_codigo_cliente = pres.ig_codigo_cliente " +
               "		AND cta.ig_codigo_moneda = pres.ig_codigo_moneda " +
               "		AND cta.cg_codigo_base_operacion = pres.cg_codigo_base_operacion " +
               "		AND rec.ig_numero_prestamo = pres.ig_numero_prestamo " ;
					lVarBind.add(numeroCliente);
					lVarBind.add(fecha);
					lVarBind.add(fecha);
			
			if(!"".equals(numeroPrestamo)){
					qryConsulta += " 		AND rec.ig_numero_prestamo = ? " ; //condicion especifica
					lVarBind.add(numeroPrestamo);
			}
			
			System.out.println("qryConsulta::::::" + qryConsulta);
			System.out.println("lVarBind::::::" + lVarBind);
			registros = con.consultarDB(qryConsulta, lVarBind, false);  
			while(registros != null && registros.next())
			{
				campos = new HashMap();
				campos.put("NUM_PRESTAMO", 		registros.getString("numero_prestamo"));
				campos.put("FECHA_PAGO", 			registros.getString("fecha_pago"));
				campos.put("CAPITAL", 				Comunes.formatoDecimal(registros.getString("capital"),2,true));
				campos.put("PREPAGO_CAPITAL", 	Comunes.formatoDecimal(registros.getString("prepago_capital"),2,true));
				campos.put("INT_ORDINARIO", 		Comunes.formatoDecimal(registros.getString("interes_ordinario"),2,true));
				campos.put("INT_MORATORIO", 		Comunes.formatoDecimal(registros.getString("interes_moratorio"),2,true));
				campos.put("COMISIONES", 			Comunes.formatoDecimal(registros.getString("comisiones"),2,true));
				campos.put("IVA", 					Comunes.formatoDecimal(registros.getString("iva"),2,true));
				campos.put("SUBSIDIO", 				Comunes.formatoDecimal(registros.getString("subsidio"),2,true));
				campos.put("TOTAL_PAGADO", 		Comunes.formatoDecimal(registros.getString("total_pagado"),2,true));
				
				campos.put("CAPITAL_VIGENTE", 		Comunes.formatoDecimal(registros.getString("capital_vigente"),2,true));
				campos.put("CAPITAL_VENCIDO", 		Comunes.formatoDecimal(registros.getString("capital_vencido"),2,true));
				campos.put("TASA_TOTAL", 				registros.getString("tasa_total"));
				campos.put("DESCRIPCION_TASA", 		registros.getString("descripcion_tasa"));
				campos.put("TASA_MORATORIA", 			registros.getString("tasa_moratoria"));
				campos.put("DESCRIPCION_MORATORIA", registros.getString("descripcion_moratoria"));
				campos.put("CUOTAS_VENCIDAS", 		registros.getString("cuotas_vencidas"));
				campos.put("CUOTAS_POR_EMITIR", 		registros.getString("cuotas_por_emitir"));
				
				list.add(campos);
			}
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getRecuperadoEstadoCuenta(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getRecuperadoEstadoCuenta(S)");
		}	
		return list;	
  }//getRecuperadoEstadoCuenta
  
  //pie de pagina del estado de cuenta
	public ArrayList getPieEstadoCuenta()
	 throws NafinException {
	 	System.out.println("EstadoCuentaPymeBean::getRecuperadoEstadoCuenta(E)");
		AccesoDB con 					= new AccesoDB();
		ArrayList list 				= new ArrayList();
		Registros registros = null;
		HashMap campos     			= new HashMap();
		try
		{
			con.conexionDB();	
			String qryConsulta = 
					" SELECT   rownum numero, cg_texto FROM com_pie_edocat_cat " ;
			registros = con.consultarDB(qryConsulta);  
			while(registros != null && registros.next())
			{	
				campos = new HashMap();
				campos.put("NUMERO",registros.getString("numero"));
				campos.put("TEXTO",registros.getString("cg_texto"));
				list.add(campos);
			}
	 
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getRecuperadoEstadoCuenta(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getRecuperadoEstadoCuenta(S)");
		}	
		return list;	
  }//getPieEstadoCuenta	 
	
	private String getMesyAnio(String strFecha){
		String []fecha = strFecha.split("/");
		String mes  = "DESCONOCIDO";
		String anio = fecha[2];
		
		int intMes = Integer.parseInt(fecha[1]);
		switch(intMes){
			case 1:  mes = "ENERO"; 		break;
			case 2:  mes = "FEBRERO"; 		break;
			case 3:  mes = "MARZO"; 		break;
			case 4:  mes = "ABRIL"; 		break;
			case 5:  mes = "MAYO"; 			break;
			case 6:  mes = "JUNIO"; 		break;
			case 7:  mes = "JULIO"; 		break;
			case 8:  mes = "AGOSTO"; 		break;
			case 9:  mes = "SEPTIEMBRE"; 	break;
			case 10: mes = "OCTUBRE"; 		break;
			case 11: mes = "NOVIEMBRE"; 	break;
			case 12: mes = "DICIEMBRE"; 	break;
		}
		return mes + " DE " + anio;
		
	}
	
	public HashMap getDireccionCliente(String numeroCliente) 
	 throws NafinException {
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		HashMap			resultados		= new HashMap();
		String qryCondicionTabla = "";
		ResultSet rs = null;
		
		if (numeroCliente == null || numeroCliente.equals(""))
			return resultados;
		
		// Obtener datos del cliente
		try {
			con.conexionDB();
			String existRegistro = "";
			boolean pyme_cliente = true;
			
			qryCondicionTabla = " select count(*) from comcat_pyme cp, com_edocta_cat cec "+
													" where cec.ig_codigo_cliente = cp.in_numero_sirac "+
													" and ig_codigo_cliente = "+numeroCliente;
													
			rs = con.queryDB(qryCondicionTabla);
			if(rs.next())	existRegistro = rs.getString(1);
			if(existRegistro.equals("0"))
			{
				qryCondicionTabla = 
							" select count(*) from comcat_clientes cp, com_edocta_cat cec "+
							" where cec.ig_codigo_cliente = cp.ic_cliente "+
							" and ig_codigo_cliente = "+numeroCliente;
				rs = con.queryDB(qryCondicionTabla);
				if(rs.next())	existRegistro = rs.getString(1);
				pyme_cliente = false;
			}
			if(rs!=null)rs.close();
			
			
			
			
			if(pyme_cliente){
			qrySentencia =
				"SELECT " +
					"pym.ic_pyme, pym.cg_rfc, " +
					"dom.cg_calle " +
					"|| ' ' " +
					"|| dom.cg_numero_ext " +
					"|| ' ' " +
					"|| dom.cg_numero_int calle, " +
					"dom.cg_colonia colonia, " +
					"mun.cd_nombre delegacion_mun, est.cd_nombre ciudad, dom.cn_cp codpos " +
				"FROM " +
					"comcat_pyme pym, " + 
					"com_domicilio dom, " +
					"comcat_municipio mun, " +
					"comcat_estado est " +
				"WHERE " + 
					"pym.ic_pyme = dom.ic_pyme AND " +
					"dom.ic_estado = mun.ic_estado AND " +
					"dom.ic_municipio = mun.ic_municipio AND " +
					"dom.ic_pais = mun.ic_pais AND " +
					"mun.ic_estado = est.ic_estado AND " + 
					"mun.ic_pais = est.ic_pais AND " +
					"pym.in_numero_sirac = ? ";
			}else{
				qrySentencia =
					" SELECT "+
					" cli.ic_cliente, cli.cg_rfc, "+
					" cli.cg_calle calle, "+
					" cli.cg_colonia colonia,  "+
					" mun.cd_nombre delegacion_mun, est.cd_nombre ciudad, cli.cn_cp codpos  "+
					" FROM  "+
					" comcat_clientes cli,  "+
					" comcat_municipio mun,  "+
					" comcat_estado est  "+
					" WHERE  "+
					" cli.ic_estado = mun.ic_estado AND  "+
					" cli.ic_municipio = mun.ic_municipio AND  "+
					" cli.ic_pais = mun.ic_pais AND  "+
					" mun.ic_estado = est.ic_estado AND  "+
					" mun.ic_pais = est.ic_pais AND  "+
					" cli.ic_cliente = ? ";
			}
			lVarBind.add(numeroCliente);
			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next() ){
				resultados.put("RFC",				registros.getString("CG_RFC"));
				resultados.put("CALLE",				registros.getString("CALLE").toUpperCase());
				resultados.put("COLONIA",			registros.getString("COLONIA").toUpperCase());
				resultados.put("MUNICIPIO",		registros.getString("DELEGACION_MUN").toUpperCase());
				resultados.put("CIUDAD",			registros.getString("CIUDAD").toUpperCase());
				resultados.put("CODIGO_POSTAL",	registros.getString("CODPOS"));
			}
				
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getSaldoFinDeMes(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getSaldoFinDeMes(S)");
		}	
		return resultados;
	}
	
public HashMap getEstadoDeCuentaAlDia(String numeroPrestamo, String numeroCliente) 
	 throws NafinException {
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		HashMap			resultados		= new HashMap();
		String qryCondicionTabla = "";
		ResultSet rs = null;
		boolean pyme_cliente = true;
		
		System.out.println("EstadoCuentaPymeBean::getEstadoDeCuentaAlDia(E)");
		
		System.out.println("numeroPrestamo "+numeroPrestamo);
		System.out.println("numeroCliente "+numeroCliente);
		
		
		try 
		{	
			con.conexionDB();
			qrySentencia =
				"SELECT " + 
					"est.ig_codigo_cliente, est.cg_desc_cliente, est.cg_desc_producto_banco, " + 
					"TO_CHAR (SYSDATE, 'dd/mm/yyyy') fecha_emision, est.ig_numero_prestamo, "  +
					"est.fn_monto_inicial, " + 
					"TO_CHAR (est.df_fecha_apertura, 'dd/mm/yyyy') df_fecha_apertura, " + 
					"est.fn_interes_vigente + est.fn_interes_vencido interes_normal, " + 
					"est.fn_moratorios + est.fn_sobretasa_moratoria interes_moratorio, " + 
					"est.fn_saldo_insoluto, est.cg_desc_valor_tasa_cartera, " + 
					"TO_CHAR (est.df_fecha_vencimiento, 'dd/mm/yyyy') df_fecha_vencimiento, " +
               "cp_cpy.cd_descripcion per_pago_interes, cper.cd_descripcion per_pago_capital " + 
					",est.fn_tasa_total tasa_total " + 
				"FROM " + 
					"com_estado_cuenta_diario est, comcat_periodicidad cper, comcat_periodicidad cp_cpy " +
				"WHERE " + 
					"est.ig_numero_prestamo    = ? AND " +
               "est.ig_frecuencia_capital = cper.ic_periodicidad(+)   AND " +
               "est.ig_frecuencia_interes = cp_cpy.ic_periodicidad(+) ";
			
			System.out.println("qrySentencia:::: " + qrySentencia);
			lVarBind.add(numeroPrestamo);
			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next() ){
				
				resultados.put("COD_CLIENTE",			registros.getString("IG_CODIGO_CLIENTE"));
				resultados.put("NOMBRE",				registros.getString("CG_DESC_CLIENTE").toUpperCase());
				resultados.put("FECHA_EMISION",		registros.getString("FECHA_EMISION"));
				resultados.put("NUM_PRESTAMO",		numeroPrestamo);
				resultados.put("MONTO_INICIAL",		registros.getString("FN_MONTO_INICIAL"));
				resultados.put("FECHA_OPERACION",	registros.getString("DF_FECHA_APERTURA"));
				resultados.put("INTERES_NORMAL",		registros.getString("INTERES_NORMAL"));
				resultados.put("INTERES_MORATORIO",	registros.getString("INTERES_MORATORIO"));
				resultados.put("SALDO_INSOLUTO",		registros.getString("FN_SALDO_INSOLUTO"));
				resultados.put("TASA_APLICADA",		registros.getString("CG_DESC_VALOR_TASA_CARTERA"));
				resultados.put("PER_PAGO_CAPITAL",	registros.getString("PER_PAGO_CAPITAL"));
				resultados.put("PER_PAGO_INTERES",	registros.getString("PER_PAGO_INTERES"));
				resultados.put("FECHA_ULTIMO_PAGO",	registros.getString("DF_FECHA_VENCIMIENTO"));
				resultados.put("TASA_TOTAL", 			Comunes.formatoDecimal(registros.getString("TASA_TOTAL"), 4, true));
				

				// Calcular total con la siguiente formula 
				// 	Total = Saldo Insoluto + interés normal + interés moratorio		
				BigDecimal Total 					= new BigDecimal("0");
				
				BigDecimal saldoInsoluto 		= new BigDecimal( (String)((resultados.get("SALDO_INSOLUTO").equals(""))		?"0":resultados.get("SALDO_INSOLUTO")));
				BigDecimal interesNormal 		= new BigDecimal( (String)((resultados.get("INTERES_NORMAL").equals(""))		?"0":resultados.get("INTERES_NORMAL")));   
				BigDecimal interesMoratorio 	= new BigDecimal( (String)((resultados.get("INTERES_MORATORIO").equals(""))	?"0":resultados.get("INTERES_MORATORIO")));
			
				Total = Total.add(saldoInsoluto);
				Total = Total.add(interesNormal);
				Total = Total.add(interesMoratorio);
				resultados.put("TOTAL",	Total.toPlainString());
				// Calcular la longitud de las celdas donde se desplegara el resultado
				String 	strNum 		= (String) resultados.get("MONTO_INICIAL");
				String 	[]numero 	= strNum.split("\\.");
				int 		numDigitos 	= numero[0].length();
				int 		widthCelda	= 0;
			
				widthCelda = numDigitos * 6 ;	// Calcular longitud de todos los caracteres; 6px por caracter	
				widthCelda += 15 ;				// Agregar longitud de los decimales(2 x 6px) y el punto (3px)
				if((numDigitos % 4) == 0 )		// Agregar longitud de las comas
					widthCelda += (numDigitos/4) * 3; // 3px por coma	
				widthCelda += 14;					// Agregar 12 px de offset
				if (widthCelda < 60 )			// La longitud calculada no puede ser menor que la longitud de la fecha
					widthCelda = 72;

				resultados.put("LONG_CELDA", widthCelda + "px");
			
				// Dar formato a los saldos e intereses 
				resultados.put("MONTO_INICIAL",		Comunes.formatoDecimal(resultados.get("MONTO_INICIAL"),2,true));
				resultados.put("SALDO_INSOLUTO",		Comunes.formatoDecimal(resultados.get("SALDO_INSOLUTO"),2,true));
				resultados.put("INTERES_NORMAL",		Comunes.formatoDecimal(resultados.get("INTERES_NORMAL"),2,true));
				resultados.put("INTERES_MORATORIO",	Comunes.formatoDecimal(resultados.get("INTERES_MORATORIO"),2,true));
				resultados.put("TOTAL",					Comunes.formatoDecimal(resultados.get("TOTAL"),2,true));
			
				// Obtener datos del cliente
				lVarBind.clear();
				String existRegistro = "";
				
				qryCondicionTabla = " select count(*) from comcat_pyme cp, com_edocta_cat cec "+
													" where cec.ig_codigo_cliente = cp.in_numero_sirac "+
													" and ig_codigo_cliente = "+numeroCliente;
													
				rs = con.queryDB(qryCondicionTabla);
				if(rs.next())	existRegistro = rs.getString(1);
				if(existRegistro.equals("0"))
				{
					qryCondicionTabla = 
								" select count(*) from comcat_clientes cp, com_edocta_cat cec "+
								" where cec.ig_codigo_cliente = cp.ic_cliente "+
								" and ig_codigo_cliente = "+numeroCliente;
					rs = con.queryDB(qryCondicionTabla);
					if(rs.next())	existRegistro = rs.getString(1);
					pyme_cliente = false;
				}
				if(rs!=null)rs.close();
				
				System.out.println("pyme_cliente:::: " + pyme_cliente);
				
				if(pyme_cliente){
				qrySentencia =
					"SELECT " +
						"pym.ic_pyme, pym.cg_rfc, " +
						"dom.cg_calle " +
						"|| ' ' " +
						"|| dom.cg_numero_ext " +
						"|| ' ' " +
						"|| dom.cg_numero_int calle, " +
						"dom.cg_colonia colonia, " +
						"mun.cd_nombre delegacion_mun, est.cd_nombre ciudad, dom.cn_cp codpos " +
					"FROM " +
						"comcat_pyme pym, " + 
						"com_domicilio dom, " +
						"comcat_municipio mun, " +
						"comcat_estado est " +
					"WHERE " + 
						"pym.ic_pyme = dom.ic_pyme AND " +
						"dom.ic_estado = mun.ic_estado AND " +
						"dom.ic_municipio = mun.ic_municipio AND " +
						"dom.ic_pais = mun.ic_pais AND " +
						"mun.ic_estado = est.ic_estado AND " + 
						"mun.ic_pais = est.ic_pais AND " +
						"pym.in_numero_sirac = ? ";
				}else
				{
					qrySentencia =
					" SELECT "+
					" cli.ic_cliente, cli.cg_rfc, "+
					" cli.cg_calle calle, "+
					" cli.cg_colonia colonia,  "+
					" mun.cd_nombre delegacion_mun, est.cd_nombre ciudad, cli.cn_cp codpos  "+
					" FROM  "+
					" comcat_clientes cli,  "+
					" comcat_municipio mun,  "+
					" comcat_estado est  "+
					" WHERE  "+
					" cli.ic_estado = mun.ic_estado AND  "+
					" cli.ic_municipio = mun.ic_municipio AND  "+
					" cli.ic_pais = mun.ic_pais AND  "+
					" mun.ic_estado = est.ic_estado AND  "+
					" mun.ic_pais = est.ic_pais AND  "+
					" cli.ic_cliente = ? ";
				}
				
				lVarBind.add(registros.getString("IG_CODIGO_CLIENTE"));
				registros = con.consultarDB(qrySentencia, lVarBind, false);
				
				System.out.println("qrySentencia:::: " + qrySentencia);
				System.out.println("lVarBind:::: " + lVarBind);
				
				if(registros != null && registros.next() ){
					resultados.put("RFC",				registros.getString("CG_RFC"));
					resultados.put("CALLE",				registros.getString("CALLE").toUpperCase());
					resultados.put("COLONIA",			registros.getString("COLONIA").toUpperCase());
					resultados.put("MUNICIPIO",		registros.getString("DELEGACION_MUN").toUpperCase());
					resultados.put("CIUDAD",			registros.getString("CIUDAD").toUpperCase());
					resultados.put("CODIGO_POSTAL",	registros.getString("CODPOS"));
				}	else{
					resultados.put("RFC",		"");
					resultados.put("CALLE",		"");
					resultados.put("COLONIA",	"");
					resultados.put("MUNICIPIO",	"");
					resultados.put("CIUDAD",			"");
					resultados.put("CODIGO_POSTAL",	"");
				}
			}
				
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getEstadoDeCuentaAlDia(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getEstadoDeCuentaAlDia(S)");
		}	
		return resultados;
	}
	
	public HashMap getAvisoDePago(String numeroPrestamo) 
	  throws NafinException {
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		HashMap			resultados		= new HashMap();
		
		System.out.println("EstadoCuentaPymeBean::getAvisoDePago(E)");
		try 
		{	
			con.conexionDB();
			qrySentencia =
				"SELECT DISTINCT "+
					"est.ig_codigo_cliente, est.cg_desc_cliente, est.cg_desc_producto_banco, " +
					"est.ig_numero_prestamo, " +
					"TO_CHAR (est.df_fecha_apertura, 'dd/mm/yyyy') df_fecha_apertura, " +
					"mon.cd_nombre moneda, est.fn_monto_inicial, " +
					"TO_CHAR (ven.df_periodoinic, 'dd/mm/yyyy') df_periodoinic, " +
					"TO_CHAR (ven.df_periodofin, 'dd/mm/yyyy') df_periodofin, " +
					"(est.fn_saldo_insoluto + est.fn_interes_vigente + est.fn_interes_vencido + est.fn_mora100 ) as fn_saldo_insoluto, " +//"ven.fg_totalvencimiento, " +
					"TO_CHAR (ven.com_fechaprobablepago, 'dd/mm/yyyy') com_fechaprobablepago, " +
					"ven.ig_dias, ven.fg_amortizacion, ven.fg_interes, " +
					"ven.fg_amortizacion + ven.fg_interes  monto_total, " +
					"est.ig_codigo_cliente referencia_pago, " +//"est.ig_codigo_cliente || '-' || 'X' referencia_pago, " + // Falta agregar algoritmo
					"ig_codigo_producto_banco producto_banco, ig_codigo_sub_aplicacion codigo_sub_aplicacion, " + //"FN_PAGO_MUESTRA_DATOS_PER(est.ig_numero_prestamo) muestra_datos_periodo, " +
					"mon.ic_moneda clave_moneda " +
				"FROM " +
					"com_estado_cuenta_diario est, com_vencimiento ven, comcat_moneda mon " +
				"WHERE  " +
					"est.ig_numero_prestamo = ven.ig_prestamo and " +
					"est.ic_moneda 			= mon.ic_moneda and " +
					"ven.ig_prestamo 			= ? "+
				"ORDER BY com_fechaprobablepago DESC"; 
			lVarBind.add(numeroPrestamo);
			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next() ){
				resultados.put("COD_CLIENTE",						registros.getString("IG_CODIGO_CLIENTE"));
				resultados.put("NOMBRE",							registros.getString("CG_DESC_CLIENTE").toUpperCase());
				resultados.put("NOMBRE_DEL_PROGRAMA",			registros.getString("CG_DESC_PRODUCTO_BANCO").toUpperCase());
				resultados.put("NUM_PRESTAMO",					numeroPrestamo);
				resultados.put("FECHA_OPERACION",				Fecha.getFechaConNombreAbbrDelMes(registros.getString("DF_FECHA_APERTURA"),'-'));
				resultados.put("MONEDA",							registros.getString("MONEDA"));
				resultados.put("MONTO_CREDITO_SOLICITADO",	Comunes.formatoDecimal(registros.getString("FN_MONTO_INICIAL"),2,true));
				resultados.put("FECHA_CORTE",						Fecha.getFechaConNombreAbbrDelMes(registros.getString("DF_PERIODOFIN"),'-'));
				resultados.put("SALDO_AL_CORTE",					Comunes.formatoDecimal(registros.getString("FN_SALDO_INSOLUTO"),2,true));
				resultados.put("FECHA_LIMITE_PAGO",				Fecha.getFechaConNombreAbbrDelMes(registros.getString("COM_FECHAPROBABLEPAGO"),'-'));
				resultados.put("PERIODO_INICIO",					registros.getString("DF_PERIODOINIC"));
				resultados.put("PERIODO_FIN",						registros.getString("DF_PERIODOFIN"));
				resultados.put("DIAS_PERIODO",					registros.getString("IG_DIAS"));
				resultados.put("CAPITAL_A_PAGAR",				Comunes.formatoDecimal(registros.getString("FG_AMORTIZACION"),2,true));
				resultados.put("INTERES_A_PAGAR",				Comunes.formatoDecimal(registros.getString("FG_INTERES"),2,true));
				resultados.put("MONTO_TOTAL",						Comunes.formatoDecimal(registros.getString("MONTO_TOTAL"),2,true));
				resultados.put("MONTO_TOTAL_LEYENDA",			Comunes.numeroEnLetras(registros.getString("MONTO_TOTAL"),registros.getString("CLAVE_MONEDA")));
				resultados.put("REFERENCIA_PAGO",				registros.getString("REFERENCIA_PAGO") + "-" + getDigitoVerificador(registros.getString("REFERENCIA_PAGO")));
				resultados.put("MUESTRA_DATOS_PERIODO",		esObraPublica(registros.getString("PRODUCTO_BANCO"),registros.getString("CODIGO_SUB_APLICACION")));
			}
				
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getAvisoDePago(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getAvisoDePago(S)");
		}	
		return resultados;
	}
	
	public HashMap getAvisoDePagoVencido(String numeroPrestamo) 
	  throws NafinException {
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		HashMap			resultados		= new HashMap();
		
		System.out.println("EstadoCuentaPymeBean::getAvisoDePagoVencido(E)");
		try 
		{	
			con.conexionDB();
			qrySentencia =
				"SELECT DISTINCT " +
					"est.ig_codigo_cliente, est.cg_desc_cliente, est.cg_desc_producto_banco, " +
					"est.ig_numero_prestamo, " +
					"TO_CHAR (est.df_fecha_apertura, 'dd/mm/yyyy') df_fecha_apertura, " +
					"mon.cd_nombre moneda, est.fn_monto_inicial, " +
					"TO_CHAR (SYSDATE, 'dd/mm/yyyy') df_periodofin, " + // "TO_CHAR (ven.df_periodofin, 'dd/mm/yyyy') df_periodofin, " +
					"(est.fn_saldo_insoluto + est.fn_interes_vigente + est.fn_interes_vencido + est.fn_mora100 ) as fn_saldo_insoluto, est.ig_cuotas_vencidas, " +//"est.fn_saldo_insoluto, est.ig_cuotas_vencidas, " + // "ven.fg_totalvencimiento, est.ig_cuotas_vencidas, " +
					"TO_CHAR (est.df_fecha_ultimo_pago, 'dd/mm/yyyy') df_fecha_ultimo_pago, " +
					"est.fn_capital_vencido, est.fn_interes_vencido, est.fn_mora100, " + //"est.fn_capital_vencido, est.fn_interes_vencido, est.fn_moratorios, " +
					"est.fn_cartera_vencida monto_total, " + //"est.fn_capital_vencido+est.fn_interes_vencido+est.fn_moratorios monto_total, " +
					"est.ig_codigo_cliente referencia_pago,  " +//"est.ig_codigo_cliente || '-' || 'X' referencia_pago,  " + // Falta agregar algoritmo
					"ig_codigo_producto_banco producto_banco, ig_codigo_sub_aplicacion codigo_sub_aplicacion, " + //"FN_PAGO_MUESTRA_DATOS_PER(est.ig_numero_prestamo) muestra_datos_vencimientos,  " +
					"mon.ic_moneda clave_moneda " +//"mon.ic_moneda clave_moneda, ven.com_fechaprobablepago " +
				"FROM " +
					"com_estado_cuenta_diario est, comcat_moneda mon " + //"com_estado_cuenta_diario est, com_vencimiento ven, comcat_moneda mon " +
				"WHERE " +
					"est.ic_moneda            = mon.ic_moneda AND " +
					"est.ig_numero_prestamo   = ? ";
				/*
					"est.ig_numero_prestamo   = ven.ig_prestamo(+) AND " +
					"est.ic_moneda            = mon.ic_moneda   AND " +
					"ven.ig_prestamo          = ? " +*/
			//qrySentencia +=
			//	"ORDER BY ven.com_fechaprobablepago ASC ";
				
			lVarBind.add(numeroPrestamo);
			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next() ){
				resultados.put("COD_CLIENTE", 						registros.getString("IG_CODIGO_CLIENTE"));
				resultados.put("NOMBRE",								registros.getString("CG_DESC_CLIENTE").toUpperCase());
				resultados.put("NOMBRE_DEL_PROGRAMA",				registros.getString("CG_DESC_PRODUCTO_BANCO").toUpperCase());
				resultados.put("NUM_PRESTAMO",						numeroPrestamo);
				resultados.put("FECHA_OPERACION",					Fecha.getFechaConNombreAbbrDelMes(registros.getString("DF_FECHA_APERTURA"),'-'));
				resultados.put("MONEDA",								registros.getString("MONEDA"));
				resultados.put("MONTO_CREDITO_SOLICITADO",		Comunes.formatoDecimal(registros.getString("FN_MONTO_INICIAL"),2,true));
				resultados.put("FECHA_CORTE",							Fecha.getFechaConNombreAbbrDelMes(registros.getString("DF_PERIODOFIN"),'-'));
				resultados.put("SALDO_AL_CORTE",						Comunes.formatoDecimal(registros.getString("FN_SALDO_INSOLUTO"),2,true));
				resultados.put("NUMERO_AMORTIZACIONES_VENCIDAS",registros.getString("IG_CUOTAS_VENCIDAS"));
				resultados.put("FECHA_ULTIMO_PAGO",					registros.getString("DF_FECHA_ULTIMO_PAGO"));
				resultados.put("CAPITAL_A_PAGAR",					Comunes.formatoDecimal(registros.getString("FN_CAPITAL_VENCIDO"),2,true));
				resultados.put("INTERES_A_PAGAR",					Comunes.formatoDecimal(registros.getString("FN_INTERES_VENCIDO"),2,true));
				resultados.put("INTERESES_MORATORIOS_A_PAGAR",	Comunes.formatoDecimal(registros.getString("FN_MORA100"),2,true));
				resultados.put("MONTO_TOTAL",							Comunes.formatoDecimal(registros.getString("MONTO_TOTAL"),2,true));
				resultados.put("MONTO_TOTAL_LEYENDA",				Comunes.numeroEnLetras(registros.getString("MONTO_TOTAL"),registros.getString("CLAVE_MONEDA")));
				resultados.put("REFERENCIA_PAGO",					registros.getString("REFERENCIA_PAGO") + "-" + getDigitoVerificador(registros.getString("REFERENCIA_PAGO")));
				resultados.put("MUESTRA_DATOS_VENCIMIENTOS",		esObraPublica(registros.getString("PRODUCTO_BANCO"),registros.getString("CODIGO_SUB_APLICACION")));	
			}
				
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getAvisoDePagoVencido(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getAvisoDePagoVencido(S)");
		}	
		return resultados;
	}
	
	public String getNumeroSIRAC(String numeroCliente)
	 throws NafinException {
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		String 			resultado		= null;
		try {	
				con.conexionDB();
				qrySentencia = 
					"select " +
						"in_numero_sirac " +
					"from " + 
						"comcat_pyme where ic_pyme =  ? ";
				lVarBind.add(numeroCliente);
				registros = con.consultarDB(qrySentencia, lVarBind, false);		
				if(registros != null && registros.next() ){
					resultado = registros.getString("in_numero_sirac");
				}	
			
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getSaldoFinDeMes(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getSaldoFinDeMes(S)");
		}
		return resultado;
	}
	
	/*
	 * Obtener los datos relacionados a la tabla de amortización
	 */
	public HashMap getDatosTablaAmortizacion(String numeroPrestamo) 
	  throws NafinException {
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= null;
		String 			qryTotales	 	= null;
		List 				lVarBind			= new ArrayList();
		Registros		registros		= null;
		Registros		registros2		= null;
		HashMap			resultados		= new HashMap();
		
		System.out.println("EstadoCuentaPymeBean::getDatosTablaAmortizacion(E)");
		try 
		{	
			con.conexionDB();
			qrySentencia =
				"SELECT DISTINCT " + 
					" est.ig_codigo_cliente AS CODIGOCLIENTE, tam.cg_desc_base_ope AS BASEOP, " +
					" tam.cg_desc_sub_aplica AS SUBAPLICA, tam.cg_desc_moneda AS MONEDA, Tam.cg_desc_cleinte AS EMPRESA, " +
					" tam.cg_desc_financiera AS BANCO, tam.fn_monto_operado AS MONTODISPUESTO, TO_CHAR(tam.df_fecha_operacion,'dd/mm/YYYY') AS FECHADISP, " +
					" tam.fn_tasa_total AS TASAANUAL, tam.ig_dia_pago DIAPAGO, " +
					" TO_CHAR(tam.df_fecha_primer_pago_comision, 'dd/mm/YYYY') AS PRIMERPAGOCOM, TO_CHAR(tam.df_fecha_primer_pago_capital, 'dd/mm/YYYY') AS PRIMERPAGOCAP, " +
					" round(MONTHS_BETWEEN(to_date(tam.df_fecha_vencimiento,'dd/mm/YYYY'), to_date(tam.df_fecha_operacion,'dd/mm/YYYY')),0) AS PLAZOCREDITO, " +
					" CASE " +
					"		WHEN tam.df_fecha_primer_pago_capital -tam.df_fecha_operacion > 30 THEN " +
					"			ROUND(months_between(tam.df_fecha_primer_pago_capital,     tam.df_fecha_operacion)) " +
					"		ELSE " +
					"			0 " +
					" END PLAZOGRACIA,  " +
					" est.ig_codigo_cliente REFERENCIA_PAGO " +
				" FROM     com_estado_cuenta_diario est, com_tabla_amortizacion tam " +
				" WHERE    est.ig_numero_prestamo    = ? " + 
				" AND  est.ig_numero_prestamo = tam.ig_numero_prestamo " ;
				
			System.out.println("qrySentencia:: " + qrySentencia);
			lVarBind.add(numeroPrestamo);
			registros = con.consultarDB(qrySentencia, lVarBind, false);
			if(registros != null && registros.next() ){
				resultados.put("CODIGOCLIENTE",							registros.getString("CODIGOCLIENTE"));
				resultados.put("BASEOP",									registros.getString("BASEOP"));
				resultados.put("SUBAPLICA",								registros.getString("SUBAPLICA"));
				resultados.put("MONEDA",									registros.getString("MONEDA"));
				resultados.put("EMPRESA",									registros.getString("EMPRESA"));
				resultados.put("BANCO",										registros.getString("BANCO"));
				resultados.put("MONTODISPUESTO",							Comunes.formatoDecimal(registros.getString("MONTODISPUESTO"),2,true));
				resultados.put("FECHADISP",								Fecha.getFechaConNombreAbbrDelMes(registros.getString("FECHADISP"),'-'));
				resultados.put("PRIMERPAGOCOM",							Fecha.getFechaConNombreAbbrDelMes(registros.getString("PRIMERPAGOCOM"),'-'));
				resultados.put("PRIMERPAGOCAP",							Fecha.getFechaConNombreAbbrDelMes(registros.getString("PRIMERPAGOCAP"),'-'));
				resultados.put("TASAANUAL",								registros.getString("TASAANUAL"));
				resultados.put("DIAPAGO",									registros.getString("DIAPAGO"));
				resultados.put("PLAZOCREDITO",							registros.getString("PLAZOCREDITO"));
				resultados.put("PLAZOGRACIA",								registros.getString("PLAZOGRACIA"));
				resultados.put("REFERENCIA_PAGO",						registros.getString("REFERENCIA_PAGO") + "-" + getDigitoVerificador(registros.getString("REFERENCIA_PAGO")));
			}	
			qryTotales =
				"SELECT sum(fn_pago_capital) AS TOTALPAGOCAPITAL, sum(fn_pago_interes) AS TOTALPAGOINTERES, sum(fn_pago_capital + fn_pago_interes) AS TOTALMONTOMENSUAL " +
				" FROM com_tabla_amortizacion " +
				" WHERE ig_numero_prestamo = ? " ;
			registros2 = con.consultarDB(qryTotales, lVarBind, false);
			if(registros2 != null && registros2.next() ){
				resultados.put("TOTALPAGOCAPITAL",						Comunes.formatoMN(registros2.getString("TOTALPAGOCAPITAL")));
				resultados.put("TOTALPAGOINTERES",						Comunes.formatoMN(registros2.getString("TOTALPAGOINTERES")));
				resultados.put("TOTALMONTOMENSUAL",						Comunes.formatoMN(registros2.getString("TOTALMONTOMENSUAL")));
			}
			System.out.println("qrySentencia:: " + qryTotales);
			//System.out.println("lVarBind:: " + lVarBind);
				
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getDatosTablaAmortizacion(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getDatosTablaAmortizacion(S)");
		}	
		return resultados;
	}
	
	/*
	 * Se obtiene la tabla como tal con el calendario de pagos y cantidades
	 */
	 
	public ArrayList getCalendarioPagos(String numeroPrestamo)
	 throws NafinException {
		System.out.println("EstadoCuentaPymeBean::getCalendarioPagos(E)");
		AccesoDB con 					= new AccesoDB();
		HashMap campos     			= new HashMap();
		ArrayList list 				= new ArrayList();
		List lVarBind					= new ArrayList();
		Registros registros = null;
		
		try
		{
			con.conexionDB();	
			String qryConsulta = 
				"	SELECT " + 
					" ig_numero_cuota AS NUMEROPAGO, TO_CHAR(df_fecha_real_pago, 'dd/mm/YYYY') AS FECHAPAGO, fn_pago_capital AS PAGOCAPITAL, fn_pago_interes AS PAGOINTERES, " +
					" (fn_pago_capital + fn_pago_interes) AS MONTOMENSUAL, fn_saldo_capital AS SALDOMENSUAL " +
				" FROM     com_tabla_amortizacion " +
				" WHERE    ig_numero_prestamo = ? " +
				" ORDER By fn_saldo_capital desc " ;
			lVarBind.add(numeroPrestamo);
			System.out.println(qryConsulta);
			registros = con.consultarDB(qryConsulta, lVarBind, false);  
			while(registros != null && registros.next())
			{
				campos = new HashMap();
				campos.put("NUMEROPAGO", registros.getString("NUMEROPAGO"));
				campos.put("FECHAPAGO", registros.getString("FECHAPAGO"));
				campos.put("PAGOCAPITAL", Comunes.formatoMN(registros.getString("PAGOCAPITAL")));
				campos.put("PAGOINTERES", Comunes.formatoMN(registros.getString("PAGOINTERES")));
				campos.put("MONTOMENSUAL", Comunes.formatoMN(registros.getString("MONTOMENSUAL")));
				campos.put("SALDOMENSUAL", Comunes.formatoMN(registros.getString("SALDOMENSUAL")));
				list.add(campos);
			}
		} catch(Exception e) {
			System.out.println("EstadoCuentaPymeBean::getCalendarioPagos(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001"); 
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("EstadoCuentaPymeBean::getCalendarioPagos(S)");
		}	
		return list;	
  }//getCalendarioPagos
	
	private String esObraPublica(String productoBanco,String codigo_sub_aplicacion){
		String	resultado 				= 	"true";
		int 		codigoSubAplicacion	=	0;
		
		try {
			codigoSubAplicacion = Integer.parseInt(codigo_sub_aplicacion);
		}catch(Exception e){
			codigoSubAplicacion = 0;
		}
		
		if(productoBanco.equals("24")){
			resultado = "false";
		}else if(codigoSubAplicacion >= 15001 && codigoSubAplicacion <= 15106){
			resultado = "false";
		}
		
		return resultado;
	}
	
	private int getDigitoVerificador(String numeroDeCuenta){
			System.out.println("::getDigitoVerificador(E)");
			int digitoVerificador = 0;
			
			try {
								
				// Extraer arreglo de numeros	
				String 	[]letras 		= numeroDeCuenta.split("");
				int		[]referencia 	= null;
				int		numeroDigitos	= letras.length-1;
				referencia = new int[numeroDigitos];
				for(int i=1;i<letras.length;i++){
					referencia[i-1]=Integer.parseInt(letras[i]);
				}
			
				// Generar secuencia de factores
				int []factores	= new int[numeroDigitos];
				for(int i=referencia.length,j=0;i>0;i--,j++){
					if(i%2 == 0){
						factores[j]=1;
					}else{
						factores[j]=2;
					}
				}
				
				// Generar productos
				int []productos = new int[numeroDigitos];
				for(int i=0;i<productos.length;i++){
					productos[i] = referencia[i] * factores[i];
				}
			
				// Suma de sus digitos
				int []sumas = new int[numeroDigitos];
				for(int i=0;i<sumas.length;i++){			
					String numero 		= Integer.toString(productos[i]);
					String []digitos 	= numero.split("");
				
					int total = 0;
					for(int j=1;j<digitos.length;j++){
						total += Integer.parseInt(digitos[j]);
					}
					sumas[i]=total;
				}
			
				// Calcular total
				int totalSumas = 0;
				for(int i=0;i<sumas.length;i++){
					totalSumas += sumas[i];
				}
							
				// Obtener digito verificador
				digitoVerificador = (totalSumas%10 == 0)?0:10-(totalSumas%10);
				
			}catch(Exception e){
				System.out.println("::getDigitoVerificador(Exception)");
				e.printStackTrace();
				System.out.println("::getDigitoVerificador(S)");
				return 0;
			}
			System.out.println("::getDigitoVerificador(S)");
			return digitoVerificador;				
	}
	
	/**
	 * metodo para hacer la busqueda de la Pyme en la pantalla de Resumen de Préstamos 
	 * @throws com.netro.exception.NafinException
	 * @return  regresa un objeto de tipo JSONArray
	 * @param nombre
	 * @param rfc
	 * @param numNafinElectronico
	 */
	public String  busquedaAvanzaResumenPrestamos(String nombre, String rfc, String numNafinElectronico) 	throws NafinException {
		log.info("busquedaAvanzaResumenPrestamos");
	
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String 	qrySentencia	= null;
		List 		lVarBind		= new ArrayList();			
		HashMap columnas = new HashMap();
		String infoRegresar ="";
		JSONArray registros = new JSONArray();
		try {
      con.conexionDB();
     	
			qrySentencia =
				"SELECT " + 
					"CPYME.IN_NUMERO_SIRAC NUMERO_SIRAC,  CNAFIN.IC_NAFIN_ELECTRONICO NUM_NAFIN_ELECTRONICO, CPYME.CG_RAZON_SOCIAL NOMBRE " +
				"FROM " + 
					"COMCAT_PYME CPYME, COMREL_NAFIN   CNAFIN " +
				"WHERE ";
				qrySentencia =  qrySentencia + ((!"".equals(nombre))? "CPYME.CG_RAZON_SOCIAL LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') AND " : "");
				qrySentencia =  qrySentencia + ((!"".equals(rfc))? "CPYME.CG_RFC LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') AND " : "");
				qrySentencia =  qrySentencia + ((!"".equals(numNafinElectronico))? "CNAFIN.IC_NAFIN_ELECTRONICO LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') AND " : "");
				qrySentencia =  qrySentencia + " CPYME.IN_NUMERO_SIRAC IS NOT NULL AND " + 
					"CNAFIN.IC_EPO_PYME_IF = CPYME.IC_PYME AND " +
					"CG_TIPO = 'P' " +
					" UNION ALL " +
				"SELECT CPYME.IC_CLIENTE NUMERO_SIRAC, CNAFIN.IC_NAFIN_ELECTRONICO NUM_NAFIN_ELECTRONICO, " + 
				" decode(cs_tipo_persona, 'M',CPYME.CG_RAZON_SOCIAL, CPYME.CG_NOMBRE || ' ' || CPYME.CG_APPAT || ' ' || CPYME.CG_APMAT ) NOMBRE " +
					"FROM COMCAT_CLIENTES CPYME, COMREL_NAFIN CNAFIN "+
					"WHERE ";
				qrySentencia =  qrySentencia + ((!"".equals(nombre))? "CPYME.CG_RAZON_SOCIAL LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') AND " : "");
				qrySentencia =  qrySentencia + ((!"".equals(rfc))? "CPYME.CG_RFC LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') AND " : "");
				qrySentencia =  qrySentencia + ((!"".equals(numNafinElectronico))? "CNAFIN.IC_NAFIN_ELECTRONICO LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') AND " : "");
				qrySentencia =  qrySentencia + " CNAFIN.IC_EPO_PYME_IF = CPYME.IC_CLIENTE AND " + 
					" CG_TIPO = 'L' " +
					"ORDER BY NOMBRE, NUM_NAFIN_ELECTRONICO";
				
				log.debug(qrySentencia);
				
				if (!"".equals(nombre)) lVarBind.add(nombre);
				if (!"".equals(rfc)) lVarBind.add(rfc);
				if (!"".equals(numNafinElectronico)) lVarBind.add(numNafinElectronico);
				if (!"".equals(nombre)) lVarBind.add(nombre);
				if (!"".equals(rfc)) lVarBind.add(rfc);
				if (!"".equals(numNafinElectronico)) lVarBind.add(numNafinElectronico);
				
		 
				ps = con.queryPrecompilado(qrySentencia, lVarBind);
				rs = ps.executeQuery();
				while(rs.next()){				
					String nafin  =(rs.getString("num_nafin_electronico")==null)?"":rs.getString("num_nafin_electronico");
					String nombre2  =(rs.getString("nombre")==null)?"":rs.getString("nombre");
					String descripcion = nafin+" "+ nombre2;
					columnas = new HashMap();				
					columnas.put("clave",(rs.getString("numero_sirac")==null)?"":rs.getString("numero_sirac"));
					columnas.put("descripcion",descripcion);
					registros.add(columnas);
				}
				rs.close();
				ps.close();
						
				infoRegresar = "({\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"})";
			
	 		 
		} catch(Exception e) { 
			log.error("busquedaAvanzaResumenPrestamos(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally { 
        con.cierraConexionDB();
			log.info(" busquedaAvanzaResumenPrestamos (S)");
		}
		
		return infoRegresar;
	}
	public String getNombreCliente(String numeroCliente,String numeroPrestamo)
	throws NafinException{
		System.out.println("EstadoCuentaPymeBean::getNombreCliente (E)");
		AccesoDB	con = new AccesoDB();
		PreparedStatement	ps	= null;
      ResultSet 	rs = null;
		StringBuffer 	qrySentencia;
		List 	conditions;
      String 		nombreCliente = "";
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
        try{
				con.conexionDB();
				qrySentencia.append(
					"SELECT /*+index(est ");
					if(numeroPrestamo!=null&&!"".equals(numeroPrestamo)) {
						qrySentencia.append("in_com_estado_cuenta_diario_02 ");
					}
					else if(numeroCliente!=null&&!"".equals(numeroCliente)){
						qrySentencia.append("in_com_estado_cuenta_diario_03 ");
					}
					qrySentencia.append(" ) use_nl(est tip)*/ " +
					"       est.cg_desc_cliente" +
					" FROM com_estado_cuenta_diario est, comcat_tipo_credito tip " +
					" WHERE est.ig_codigo_sub_aplicacion = tip.ic_tipo_credito AND tip.cs_info_pago = ? ");
					conditions.add("S");
					if(numeroCliente!=null&&!"".equals(numeroCliente)) {
						qrySentencia.append(" AND est.ig_codigo_cliente = ? ");
						conditions.add(new Integer(numeroCliente));
					}
					if(numeroPrestamo!=null&&!"".equals(numeroPrestamo)) {
						qrySentencia.append(" AND est.ig_numero_prestamo = ? ");
						conditions.add(new Integer(numeroPrestamo));
					}
					qrySentencia.append(" ORDER BY est.ig_codigo_cliente, est.df_fecha_vencimiento ");
			System.out.println("qrySentencia: "+qrySentencia);
			System.out.println("conditions: "+conditions);
					
					ps = con.queryPrecompilado(qrySentencia.toString(), conditions);
				   rs = ps.executeQuery(); 
               if(rs.next()){
						nombreCliente = (rs.getString("cg_desc_cliente")==null)?"":rs.getString("cg_desc_cliente");
               }
					rs.close();
					ps.close();
               con.cierraStatement();
			log.debug("nombreCliente "+nombreCliente);
        }catch(Exception e){
        	nombreCliente = "";
			log.error("getNombreCliente(Exception) "+e);
			e.printStackTrace();
               throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                	con.cierraConexionDB();
                }
		System.out.println("CargaDocDistBean::getNombreCliente (S)");
        }
	return nombreCliente;
	}
}//EstadoCuentaPymeBean