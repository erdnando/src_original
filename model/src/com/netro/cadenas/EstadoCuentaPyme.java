package com.netro.cadenas;


import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import javax.ejb.EJBObject;
import javax.ejb.SessionContext;
import com.netro.exception.NafinException;
import javax.ejb.Remote;

@Remote
public interface EstadoCuentaPyme {
    public List getFechasAviso() throws NafinException;

    public List getAvisoPago() throws NafinException;

    public HashMap getEstadoCuentaFinDeMes(String numeroCliente, String fecha) throws NafinException;

    public ArrayList getDisposicionEstadoCuenta(String numeroPrestamo, String numeroCliente, String fecha)  throws NafinException;

    public ArrayList getRecuperadoEstadoCuenta(String numeroPrestamo, String numeroCliente, String fecha) throws NafinException;

    public ArrayList getPieEstadoCuenta() throws NafinException;

    public HashMap getDireccionCliente(String numeroCliente) throws NafinException;

    public HashMap getEstadoDeCuentaAlDia(String numeroPrestamo, String numeroCliente) throws NafinException;

    public HashMap getAvisoDePago(String numeroPrestamo) throws NafinException;

    public HashMap getAvisoDePagoVencido(String numeroPrestamo) throws NafinException;

    public String getNumeroSIRAC(String numeroCliente) throws NafinException;

    public HashMap getDatosTablaAmortizacion(String numeroPrestamo) throws NafinException;

    public ArrayList getCalendarioPagos(String numeroPrestamo) throws NafinException;

    public String busquedaAvanzaResumenPrestamos(String nombre, String rfc, String numNafinElectronico) throws NafinException;

    public String getNombreCliente(String numeroCliente,String numeroPrestamo) throws NafinException;
}//EstadoCuentaPyme
