package com.netro.cadenas;

import java.io.Serializable;
import com.netro.exception.*;
import netropology.utilerias.*;
import java.sql.*;
import java.text.*;
import java.math.*;

/**
 * Clase que se encarga de invocar al metodo generarArchivo del 
 * Inteligencia Comercial Bean
 *
 * @author  Jesus Salim Hernandez David
 * @version 1.0.14
 */

public class GeneraArchivoConsultaThread implements Runnable, Serializable {
	
	private int 				numberOfRegisters;
	private boolean 			started;
	private boolean 			running;
	private int 				processedRegisters;
	private InteligenciaComercialBean inteligencia;
	private boolean			error;
	private String 			nombreArchivo;
	private String 			rutaFisica;
	private String 			rutaVirtual;
	
	private String 	mesInicial;
	private String 	anioInicial;
	private String 	mesFinal;
	private String 	anioFinal;
	private String 	sPeriodo1;
	private String 	sPeriodo2;
	private String 	sPeriodo3;
	private String 	sNoEposSelec; 
	
	

   public GeneraArchivoConsultaThread() {
	numberOfRegisters 	= 0;
	started 			   = false;
	running 			   = false;
	processedRegisters = 0;
	inteligencia			= new InteligenciaComercialBean();
	error					= false;
   }
	 
   protected void generaArchivoCsv() {
			
		try {
	
			nombreArchivo = inteligencia.generarArchivo(mesInicial, anioInicial, mesFinal, anioFinal, sPeriodo1, sPeriodo2, sPeriodo3, sNoEposSelec, rutaFisica);
					
	
		} catch(Exception e){
				setRunning(false);
				processedRegisters = numberOfRegisters ;
				setError(true);
				e.printStackTrace();
				System.out.println("Error en ValidaDatosPymeMasivaThread::validateRegisters al extraer numero de registro de la base de datos");
				// terminar el proceso si falla la funcion de validacion para algun registro
		}
		
   }
	
	public synchronized int getNumberOfRegisters() {
       return numberOfRegisters;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		return((processedRegisters == 0 && numberOfRegisters == 0)?false:(processedRegisters == numberOfRegisters));
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
	public synchronized void setError(boolean error) {
       this.error = error;
   }
	
	public synchronized boolean hasError() {
       return error;
   }
	
	public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }
	

   public void run() {
		 
		// Obtener numero de registros
		//AccesoDB 	con 	=	new AccesoDB();
		boolean		lbOK	=  true;
		
		// Realizar la Validacion de los datos
      try {
			
			//if(lbOK == false) 
				setRunning(true);
			
				while (isRunning() && nombreArchivo == null){
					if(lbOK && nombreArchivo == null){
						lbOK = false;
						generaArchivoCsv();
					}
				}
		
      } finally {
			setRunning(false);
      }

    }

	 public String getNombreArchivo(){ // Fodea 057 - 2010
		return nombreArchivo;
	 }
 
	 public void setNombreArchivo(String nombreArchivo){ // Fodea 057 - 2010
		this.nombreArchivo = nombreArchivo;
	 }


	public void setRutaFisica(String rutaFisica) {
		this.rutaFisica = rutaFisica;
	}


	public String getRutaFisica() {
		return rutaFisica;
	}


	public void setRutaVirtual(String rutaVirtual) {
		this.rutaVirtual = rutaVirtual;
	}


	public String getRutaVirtual() {
		return rutaVirtual;
	}

	public String getAnioFinal() {
		return anioFinal;
	}

	public void setAnioFinal(String anioFinal) {
		this.anioFinal = anioFinal;
	}

	public String getAnioInicial() {
		return anioInicial;
	}

	public void setAnioInicial(String anioInicial) {
		this.anioInicial = anioInicial;
	}

	public String getMesFinal() {
		return mesFinal;
	}

	public void setMesFinal(String mesFinal) {
		this.mesFinal = mesFinal;
	}

	public String getMesInicial() {
		return mesInicial;
	}

	public void setMesInicial(String mesInicial) {
		this.mesInicial = mesInicial;
	}

	public String getSPeriodo1() {
		return sPeriodo1;
	}

	public void setSPeriodo1(String sPeriodo1) {
		this.sPeriodo1 = sPeriodo1;
	}

	public String getSPeriodo2() {
		return sPeriodo2;
	}

	public void setSPeriodo2(String sPeriodo2) {
		this.sPeriodo2 = sPeriodo2;
	}

	public String getSPeriodo3() {
		return sPeriodo3;
	}

	public void setSPeriodo3(String sPeriodo3) {
		this.sPeriodo3 = sPeriodo3;
	}

	public String getSNoEposSelec() {
		return sNoEposSelec;
	}

	public void setSNoEposSelec(String sNoEposSelec) {
		this.sNoEposSelec = sNoEposSelec;
	}


}