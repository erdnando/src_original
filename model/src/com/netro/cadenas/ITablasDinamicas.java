/*************************************************************************************
*
* Nombre de Clase o Archivo: ITablasDinamicas.java
*
* Versi�n: 		  1.0
*
* Fecha Creaci�n: 11/febrero/2002
*
* Autor:          Cindy Ramos P�rez
*
* Fecha Ult. Modificaci�n:
*
* Descripci�n de Clase: Interface remota del EJB de Publicaci�n de Tablas Din�micas
*
*************************************************************************************/

package com.netro.cadenas;

import java.util.Vector;
import com.netro.exception.*;
import netropology.utilerias.*;
import javax.ejb.EJBObject;
import javax.ejb.Remote;

@Remote
public interface ITablasDinamicas {

	public Vector obtenListaPublicaciones( String cadena )
		throws NafinException;

	public void borraRegistros(String lsNumPublicacion, String lsCaracterCampo, String lsNumIdCampo)
		throws NafinException;

	public void borraPublicacion(String lsNumEpo,  String lsNumPublicacion, boolean mbExisteTabEdi, boolean lbExisteTabDet )
		throws NafinException;

	public boolean modificaPublicaciones(Vector lvQry, int liNumPublicacion)
		throws NafinException;

	public boolean modificaPublicaciones(String lsQry)
		throws NafinException;


	public boolean checaPseudollave(String lsNumPublicacion, String lsNumProvDis, String lsPrimerCampoFijo)
		throws NafinException;


	public boolean existeTabla( String lsNumPublicacion, String lsCaracterTabla )
		throws NafinException, Exception;

	public int guardaPublicacion(	String lsNumEpo, 			String lsTipoPublicacion, 	String lsNombreTabla,
									String lsFchIni, 			String lsFchFin, 			int    liNumColumnas)
		throws NafinException, Exception;


	public void modificaPublicacion(	String lsNumEpo,	String lsNombreTabla,	String lsFchIni,
										String lsFchFin, 	int    liNumColumnas,   int    liNumPub)
		throws NafinException, Exception;


	public boolean guardaCampos(int 	  liNumPub,           String[] lsNombres,       String[] lsTiposCampo,
								String[] lsLongitudesCampo,   String[] lsTotalesCampo,  String[] lsOrdenes,
								String   lsCaracterCampo)
		throws NafinException, Exception;



	public String obtenNombrePub(int liNumPublicacion)
		throws NafinException, Exception;

	public void guardaRespuestaPyme(String[] msQuerys, String[] msNumIds, String iNoUsuario, String lsNumPublicacion)
		throws NafinException;


	public void importaDatosFijos( String lsPathArchivo, String lsNumPublicacion, String lsNumEPO)
		throws NafinException;

	public void importaDatosDetalle( String lsPathArchivo, String lsNumPublicacion)
		throws NafinException;

	public boolean insertaCampoDetalle( Vector lVValoresDatos, Vector lVTiposDatos, String lsNumPublicacion )
		throws NafinException;

	public boolean insertaCampoFijo( Vector lVValoresDatos, Vector lVTiposDatos, String lsNumPublicacion, String lsNumEPO )
		throws NafinException;

	public Vector obtenTipoDatos( String lsNumPublicacion, String lsCaracterCampo )
		throws NafinException;

	public boolean hayUsuariosPub( String lsNumEPO, String lsNumPublicacion, String lsProveedor )
		throws NafinException;

	public void insertaUsuariosAPublicacion( String lsNumEPO, String lsNumPublicacion, String lsProveedor )
		throws NafinException;







	public void grabaPublicacion(	int    liNumEpo, 	int liTipoPublicacion, 	String lsNombreTabla,	String lsFchIni,
									String lsFchFin,	int liNumColumnas)
				throws NafinException;



	public int icrearTabla(int eiNumCampos, CUsrPublicacion eoUsrPublicacion, String esNombreCampos[],
							String esTipoDatos[], String esLongitudes[] )
				throws NafinException;

	public int icrearTabla(int eiNumCampos, String esNomTabla, String esTipoPub, String esUsuarios,
							String esFechaIni, String esFechaFin, int eiCveEPO, String esNombreCampos[],
							String esTipoCampos[], String esLongitudes[] )
				throws NafinException;

	public Vector ovconsultarTiposPub()
				throws NafinException;

	public Vector ovconsultarEPOs()
				throws NafinException;

	public Vector ovgetCampos(int eiCvePublicacion)
				throws NafinException;

	public Vector ovgetDatosTabla(int eiCvePublicacion)
				throws NafinException;

	public Vector ovgetDatosRegistro(int eiCvePublicacion, int eiCveRegistro)
				throws NafinException;

	public Vector ovgetInfTabla(int eiCvePublicacion, int eiCveEPO)
				throws NafinException;

	public Vector ovgetTablas(int eiCveEPO)
				throws NafinException;

	public Vector sinsertarRegistro( int eiCvePublicacion, String esNombreArchivo )
				throws NafinException ;

	public void vactualizarRegistro(int eiCvePublicacion, int eiCveRegistro, String esValores[], String esTipoDato[] )
			throws NafinException ;

	public void vborrarRegistro(int eiCvePublicacion, int eiCveRegistro )
		throws NafinException;

	public void vborrarTabla(String esClaveEpo, int eiCvePublicacion)
		throws NafinException, Exception;

	public void vinsertarRegistro(int eiCvePublicacion, String esValores[], String esTipoDato[] )
		throws NafinException;

	public void vlimpiarTabla(int eiCvePublicacion)
		throws NafinException;

	public void vsetDatosTabla(int eiCvePublicacion, CUsrPublicacion eoUsrPublicacion)
				throws NafinException;

	public void vsetDatosTabla(int eiCvePublicacion, String esCveEpo, String esNomTabla,
								String esFechaIni, String esFechaFin )
				throws NafinException;

	public boolean bvalidarTabla(int eiCveEPO, String esNomTabla )
				throws NafinException;

	public boolean guardaUsuarios(int liNumPublicacion,  String Usr)
				throws NafinException;
				
	public void guardaRespuestaPymeExt(String[] msQuerys, String[] msNumIds, String iNoUsuario, String lsNumPublicacion)
		throws NafinException;
		
}

