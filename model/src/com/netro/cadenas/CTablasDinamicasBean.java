package com.netro.cadenas;

import com.netro.exception.NafinException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.CUsrPublicacion;
import netropology.utilerias.Comunes;
import netropology.utilerias.VectorTokenizer;

/*************************************************************************************
 *
 * Nombre de Clase o de Archivo: CTablasDinamicasBean.java
 *
 * Versi�n: 		  1.0
 *
 * Fecha Creaci�n: 11/febrero/2002
 *
 * Autor:          Manuel Ramos Mendoza
 *
 * Fecha Ult. Modificaci�n:
 *
 * Descripci�n de Clase: Clase que implementa los m�todos para el EJB de Publicaci�n de Tablas Din�micas.
 *
 *************************************************************************************/
@Stateless(name = "TablasDinamicasEJB" , mappedName = "TablasDinamicasEJB")
@TransactionManagement(TransactionManagementType.BEAN)

public class CTablasDinamicasBean implements ITablasDinamicas {


	private String 	DELIMITADOR_REGISTRO = "\n",
					DELIMITADOR_CAMPO    = "|",
					CADENA_CONEXION       = "seguridadData";  //seguridadData

	private AccesoDB mobjConexion  = new AccesoDB();

	/*********************************************************************************************
	*
	*	    	OBTIENE LA LISTA DE PUBLICACIONES DE LA EPO
	*
	*********************************************************************************************/


	public Vector obtenListaPublicaciones( String cadena )
		throws NafinException	{

		String msQryPublicaciones = "SELECT distinct(ic_publicacion),cg_nombre,TO_CHAR(df_ini_pub,'DD/MM/YYYY'), " +
								   "	TO_CHAR(df_fin_pub,'DD/MM/YYYY'),in_columnas " +
								   "FROM com_publicacion " +
								   "WHERE ic_epo = " + cadena + " and ic_tipo_publicacion = 1 order by ic_publicacion";


		AccesoDB  lobjConexion = new AccesoDB();
		ResultSet lCurListaPub = null;
		Vector    lVecRegistro = new Vector(),
				  lVecListaPub = new Vector();



		try	{
			try {	lobjConexion.conexionDB();	}
			catch (Exception error)	{
				throw new NafinException("SIST0001");
			}

			lCurListaPub = lobjConexion.queryDB( msQryPublicaciones );

			while ( lCurListaPub.next() )	{

				lVecRegistro = new Vector();
				lVecRegistro.addElement( lCurListaPub.getString(1) );
				lVecRegistro.addElement( lCurListaPub.getString(2) );
				lVecRegistro.addElement( lCurListaPub.getString(3) );
				lVecRegistro.addElement( lCurListaPub.getString(4) );
				lVecRegistro.addElement( lCurListaPub.getString(5) );

				lVecListaPub.addElement( lVecRegistro );
			}
		}
		catch (NafinException Error){
			throw Error;
		}
		catch (Exception epError){
			throw new NafinException("PARM0001");    //No es Posible Publicar Tablas en este momento, intenta m�s tarde
		}
		finally {
			lobjConexion.cierraConexionDB();
		}

		return lVecListaPub;
	}




	/*********************************************************************************************
	*
	*	    	BORRA TODAS LAS TABLAS KE LE CORRESPONDE A UNA PUBLICACION
	*
	*********************************************************************************************/

	public void borraPublicacion(String lsNumEpo,  String lsNumPublicacion, boolean mbExisteTabEdi, boolean lbExisteTabDet )
		throws NafinException	{

		boolean lbNoError = false;
		String lsQry = "";

		AccesoDB lobjConexion = new AccesoDB();
		try	{
			lobjConexion.conexionDB();

				/*******************************************************************
				*	PREGUNTANDO SI LA PUBLICACION LE CORRESPONDE A LA EPO
				*******************************************************************/

				lsQry = "SELECT IC_PUBLICACION FROM com_publicacion " +
						"WHERE ic_publicacion = "+lsNumPublicacion+" AND ic_epo = " + lsNumEpo;
				ResultSet lCursor = lobjConexion.queryDB( lsQry );
				if ( lCursor.next() )	lbNoError = true;
				lCursor.close();

				if (lbNoError)	{
					lsQry = "DELETE FROM comrel_publicacion_usuario WHERE ic_publicacion = " + lsNumPublicacion;
					lobjConexion.ejecutaSQL( lsQry );

					lsQry = "DELETE FROM com_tabla_pub_atrib_d WHERE ic_publicacion = " + lsNumPublicacion;
					lobjConexion.ejecutaSQL( lsQry );

					lsQry = "DELETE FROM com_tabla_pub_atrib WHERE ic_publicacion = " + lsNumPublicacion;
					lobjConexion.ejecutaSQL( lsQry );

					lsQry = "DELETE FROM com_publicacion WHERE ic_publicacion = " + lsNumPublicacion + " AND ic_epo = " + lsNumEpo;
					lobjConexion.ejecutaSQL( lsQry );

					if ( lbExisteTabDet )	{
						lsQry = "DROP TABLE tabla_d_" + lsNumPublicacion;
						lobjConexion.ejecutaSQL( lsQry );
					}

					if ( mbExisteTabEdi )	{
						lsQry = "DROP TABLE tabla_edit_" + lsNumPublicacion;
						lobjConexion.ejecutaSQL( lsQry );
					}

					lsQry = "DROP TABLE tabla_" + lsNumPublicacion;
					lobjConexion.ejecutaSQL( lsQry );
				}
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
			lbNoError = false;
			throw new NafinException("PARM0001");    //No es Posible Publicar Tablas en este momento, intenta m�s tarde
		}
		finally	{
			if ( lobjConexion != null)	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}
	}




	/*********************************************************************************************
	*
	*	    	BORRA LOS REGISTROS DE LAS TABLAS
	*
	*********************************************************************************************/

	public void borraRegistros(String lsNumPublicacion, String lsCaracterCampo, String lsNumIdCampo)
		throws NafinException	{
		boolean  lbNoError = true;
		AccesoDB lobjConexion = new AccesoDB();

		try	{
			lobjConexion.conexionDB();

			/****************************************************
			* BORRANDO TODOS LOS REGISTROS Y RELACIONES
			****************************************************/

			if ( lsCaracterCampo.equals("F") ) {
				boolean	  lbTablaDeta    = false,
						  lbTablaEdita   = false;
				String    lsChkDetalles  = "SELECT * FROM com_tabla_pub_atrib_d WHERE ic_publicacion = " + lsNumPublicacion,
						  lsChkEditables = "SELECT * FROM com_tabla_pub_atrib WHERE cs_tipo_atrib = 'E' AND ic_publicacion = " + lsNumPublicacion,
						  lsBorraDeta    = "DELETE FROM tabla_d_" + lsNumPublicacion,
						  lsBorraEdita   = "DELETE FROM tabla_edit_" + lsNumPublicacion,
						  lsBorraFijos   = "DELETE FROM tabla_" + lsNumPublicacion;

				ResultSet lCursor = lobjConexion.queryDB( lsChkDetalles );
				if ( lCursor.next() )	lbTablaDeta = true;

				lCursor = lobjConexion.queryDB( lsChkEditables );
				if ( lCursor.next() )	lbTablaEdita = true;

				if ( lbTablaDeta )	lobjConexion.ejecutaSQL( lsBorraDeta );
				if ( lbTablaEdita )	lobjConexion.ejecutaSQL( lsBorraEdita );
				lobjConexion.ejecutaSQL( lsBorraFijos );
			}
			else if	( lsCaracterCampo.equals("D") ) {
				String lsBorraDeta    = "DELETE FROM tabla_d_" + lsNumPublicacion + " WHERE id = " + lsNumIdCampo;
				lobjConexion.ejecutaSQL( lsBorraDeta );
			}

		}
		/*catch (NafinException nafErr)	{
			throw nafErr;
		}*/
		catch(Exception err)	{
			System.out.print( err.getMessage() );
			lbNoError = false;
			throw new NafinException("PARM0001");    //No es Posible Publicar Tablas en este momento, intenta m�s tarde
		}
		finally	{
			if ( lobjConexion != null)	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}
	}




	/*********************************************************************************************
	*
	*	    	EJECUTA LOS QUERYS ARMADOS EN LA JSP PARA MODIFICACION DE DATOS
	*
	*********************************************************************************************/

	public boolean modificaPublicaciones(Vector lvQry, int liNumPublicacion)
		throws NafinException	{

		boolean  lbNoError = true;
		String   lsQry     = "";
		AccesoDB lobjConexion = new AccesoDB();

		try	{

			try {	lobjConexion.conexionDB();	}
			catch (Exception error)	{
				lbNoError = false;
				throw new NafinException("SIST0001");
			}

			for (int i=0; i<lvQry.size(); i++)	{
				lsQry = (String) lvQry.elementAt(i);
				lobjConexion.ejecutaSQL( lsQry );
			}
		}

		catch (NafinException Error){
			lbNoError = false;
			throw Error;
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
			lbNoError = false;
			throw new NafinException("PARM0001");    //No es Posible Publicar Tablas en este momento, intenta m�s tarde
		}
		finally	{
			if ( lobjConexion != null)	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}
		return lbNoError;
	}






	/*********************************************************************************************
	*
	*	    	EJECUTA EL QUERY ARMADOS EN LA JSP PARA MODIFICACION DE DATOS
	*
	*********************************************************************************************/

	public boolean modificaPublicaciones(String lsQry)
		throws NafinException	{

		boolean  lbNoError = true;
		AccesoDB lobjConexion = new AccesoDB();

		try	{

			try {	lobjConexion.conexionDB();	}
			catch (Exception error)	{
				lbNoError = false;
				throw new NafinException("SIST0001");
			}

			lobjConexion.ejecutaSQL( lsQry );
		}

		catch (NafinException Error){
			lbNoError = false;
			throw Error;
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
			lbNoError = false;
			throw new NafinException("PARM0001");    //No es Posible Publicar Tablas en este momento, intenta m�s tarde
		}
		finally	{
			if ( lobjConexion != null)	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}
		return lbNoError;
	}







	/****************************************************************************************
	*	CHECA KE NO SE REPITA EL CAMPO DE PROVEEDOR-DISTRIBUIDOR Y EL PRIMER CAMPO FIJO
	*****************************************************************************************/

	public boolean checaPseudollave(String lsNumPublicacion, String lsNumProvDis, String lsPrimerCampoFijo)
		throws NafinException	{
		System.out.print("CTablasDinamicasBean.checaPseudollave(E)");

		String lsQryChecPsLlav = "SELECT id FROM tabla_"+lsNumPublicacion+" WHERE campo0 = '"+lsNumProvDis+"' AND campo1 = '"+lsPrimerCampoFijo+"'";

		boolean  lbNoError = true;
		try	{
			if (mobjConexion.hayConexionAbierta() == false) {
				try {	mobjConexion.conexionDB();	}
				catch (Exception error)	{
					throw new NafinException("SIST0001");
				}
			}

			ResultSet lCursor = mobjConexion.queryDB( lsQryChecPsLlav );
			if ( lCursor.next() )
				lbNoError = false;
			lCursor.close();
			mobjConexion.cierraStatement();


		}
		catch (NafinException Error){
			System.out.print("CTablasDinamicasBean.checaPseudollave(NafinException):"+Error );
			lbNoError = false;
			throw Error;
		}
		catch(Exception err)	{
			System.out.print("CTablasDinamicasBean.checaPseudollave(Exception):"+err.getMessage() );
			lbNoError = false;
			throw new NafinException("SIST0001");
		}
		finally	{
			System.out.print("CTablasDinamicasBean.checaPseudollave(S)");
		}
		return lbNoError;
	}





	/************************************************************************************************
	*
	*	VERIFICA SI EXISTE UNA TALBA
	*
	************************************************************************************************/

	public boolean existeTabla( String lsNumPublicacion, String lsCaracterTabla )
		throws NafinException	{

		String 	 lsQryExisTabla = "";
		boolean  lbExiste       = false;

		if ( lsCaracterTabla.equals("D") )
			lsQryExisTabla = "SELECT * FROM com_tabla_pub_atrib_d WHERE ic_publicacion = " + lsNumPublicacion;
		else
			lsQryExisTabla = "SELECT * FROM com_tabla_pub_atrib " +
							 "WHERE cs_tipo_atrib = '"+lsCaracterTabla+"' AND ic_publicacion = " + lsNumPublicacion;


		AccesoDB lobjConexion = new AccesoDB();

		try	{

			try {	lobjConexion.conexionDB();	}
			catch (Exception error)	{
				throw new NafinException("SIST0001");
			}

			ResultSet lCurExiteTabla = lobjConexion.queryDB( lsQryExisTabla );
			if ( lCurExiteTabla.next() )	lbExiste = true;
			lCurExiteTabla.close();
			lobjConexion.cierraStatement();

			System.out.print("\t\n cerrando rs \n");

		}
		catch (NafinException Error){
			throw Error;
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en CTablasDinamicasBean de la funcion existeTabla.");
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("Se ha producido un error en CTablasDinamicasBean de la funcion existeTabla.");
			System.out.println("ERROR: " + err.getMessage());
		}
		finally	{
			System.out.print("\t\n EN FINALLY \n");
			lobjConexion.cerrarDB(true);

			if ( lobjConexion.hayConexionAbierta() ) {
				lobjConexion.cierraConexionDB();
			}
		}

		return lbExiste;
	}







	/*************************************************
	*
	*	GUARDA NUEVA PUBLICACION
	*
	***************************************************/

	public int guardaPublicacion(	String lsNumEpo, 			String lsTipoPublicacion, 	String lsNombreTabla,
									String lsFchIni, 			String lsFchFin, 			int    liNumColumnas)
		throws NafinException	{

		boolean lbNoError     =  true;
		int    	liCvePub      =  obtenNumPublicacionSig();

		String 	lsQryNuevaPub =  "INSERT INTO com_publicacion " +
								 "	(ic_publicacion, ic_epo, ic_tipo_publicacion, cg_nombre,  " +
								 "	df_ini_pub, df_fin_pub, in_columnas) " +
								 "VALUES ("+liCvePub+", "+lsNumEpo+", "+lsTipoPublicacion+", " +
								 "	'"+lsNombreTabla+"', TO_DATE('"+lsFchIni+"', 'dd/mm/yyyy'), " +
								 "	TO_DATE('"+lsFchFin+"', 'dd/mm/yyyy'), "+liNumColumnas+")";

		AccesoDB lobjConexion = new AccesoDB();

		try	{
			try {	lobjConexion.conexionDB();	}
			catch (Exception error)	{
				throw new NafinException("SIST0001");
			}

			lobjConexion.ejecutaSQL( lsQryNuevaPub );
		}
		catch (NafinException Error){
			lbNoError = false;
			throw Error;
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
			lbNoError = false;
		}
		finally	{
			if ( lobjConexion != null)	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}

		return liCvePub;
	}







	/*************************************************
	*	MODIFICA PUBLICACION
	***************************************************/

	public void modificaPublicacion(	String lsNumEpo,	String lsNombreTabla,	String lsFchIni,
										String lsFchFin, 	int    liNumColumnas,   int    liNumPub)
		throws NafinException	{

		boolean lbNoError     =  true;

		String 	lsQryNuevaPub =  "UPDATE com_publicacion SET " +
								 "	cg_nombre   = '"+lsNombreTabla+"' , " +
								 "	df_ini_pub  = TO_DATE('"+lsFchIni+"', 'dd/mm/yyyy'), " +
								 "	df_fin_pub  = TO_DATE('"+lsFchFin+"', 'dd/mm/yyyy'), " +
								 "	in_columnas = in_columnas + " + liNumColumnas + " " +
								 "WHERE ic_publicacion = " + liNumPub +
								 "	AND ic_epo = " + lsNumEpo;

		AccesoDB lobjConexion = new AccesoDB();

		try	{

			try {	lobjConexion.conexionDB();	}
			catch (Exception error)	{
				throw new NafinException("SIST0001");
			}
			System.out.println("\n En modificaPublicacion con lsQryNuevaPub: " + lsQryNuevaPub + "\n");
			lobjConexion.ejecutaSQL( lsQryNuevaPub );

		}
		catch (NafinException Error)	{
			lbNoError = false;
			throw Error;
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
			lbNoError = false;
		}
		finally	{
			if ( lobjConexion != null)	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}
	}








	/*************************************************
	*
	*	OBTENIENDO NUMERO DE PUBLICACION
	*
	***************************************************/

	private int obtenNumPublicacionSig()	{
		String 	lsQryNumMaxPub = "SELECT NVL(MAX (ic_publicacion),0) + 1 as num_pub FROM com_publicacion";
		int		liNumPublicacion = 0;

		AccesoDB lobjConexion = new AccesoDB();

		try	{

			try {	lobjConexion.conexionDB();	}
			catch (Exception error)	{
				throw new NafinException("SIST0001");
			}

			ResultSet lCurNumMaxPub = lobjConexion.queryDB( lsQryNumMaxPub );
			while ( lCurNumMaxPub.next() )
				liNumPublicacion = lCurNumMaxPub.getInt("num_pub");
			lCurNumMaxPub.close();
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally	{
			if ( lobjConexion != null)	lobjConexion.cierraConexionDB();
		}

		return liNumPublicacion;
	}







	/*************************************************
	*	GUARDA USUARIOS
	***************************************************/

	public boolean guardaUsuarios(int liNumPublicacion,  String Usr)
		throws NafinException	{

		boolean lbNoError  = true;
		Vector 	AregloUser = new Vector();
		AregloUser = Comunes.explode("|",Usr.trim());
		String 	lsQryUsuarios =  "";
		AccesoDB lobjConexion = new AccesoDB();

		try	{

			try {	lobjConexion.conexionDB();	}
			catch (Exception error)	{
				throw new NafinException("SIST0001");
			}

			for (int i=0; i<AregloUser.size(); i++)	{
				lsQryUsuarios = "INSERT INTO comrel_publicacion_usuario (ic_nafin_electronico, ic_publicacion) " +
								"VALUES ("+(String) AregloUser.elementAt(i)+", "+liNumPublicacion+")";
				lobjConexion.ejecutaSQL( lsQryUsuarios );
			}
		}
		catch (NafinException Error)	{
			lbNoError = false;
			throw Error;
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
			lbNoError = false;
			throw new NafinException("SIST0001");
		}
		finally	{
			if ( lobjConexion != null)	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}

		return true;
	}






	/*************************************************
	*
	*	GUARDA LA ESTRUCTURA DE LAS TABLAS
	*
	***************************************************/

	public boolean guardaCampos(int 	  liNumPub,           String[] lsNombres,       String[] lsTiposCampo,
								String[] lsLongitudesCampo,   String[] lsTotalesCampo,  String[] lsOrdenes,
								String   lsCaracterCampo)
		throws NafinException	{


		boolean lbNoError = false;

		if ( lsTiposCampo.length == lsNombres.length && lsOrdenes.length == lsNombres.length)	{
			lbNoError = true;
			String 	
					lsQryCampos  = "",
					lsLongDec    = "",
					lsLongEntero = "",
					lsTotal      = "";

			int     liContadorTotales = 0,
					liContadorLong    = 0,
					liOrden           = 0,
					liNumAtributo     = 0;

			AccesoDB lobjConexion = new AccesoDB();


			/*************************************************************
			*	BUSCANDO EL NUMERO MAYOR DE ATRIBUTO
			*************************************************************/
			String lsQryAtrib = "";
			if (lsCaracterCampo.equals("D"))
				lsQryAtrib = "SELECT NVL(MAX(ic_atributo_d)+1, 0) AS num_atributo FROM com_tabla_pub_atrib_d WHERE ic_publicacion = " + liNumPub;
			else
				lsQryAtrib = "SELECT NVL(MAX(ic_atributo)+1, 0) AS num_atributo FROM com_tabla_pub_atrib WHERE ic_publicacion = " + liNumPub;



			try	{

				try {	lobjConexion.conexionDB();	}
				catch (Exception error)	{
					throw new NafinException("SIST0001");
				}

				ResultSet cur = lobjConexion.queryDB(lsQryAtrib);
				while (cur.next())	{
					liNumAtributo = cur.getInt("num_atributo");
				}
				cur.close();
			}
			catch (NafinException Error)	{
				lbNoError = false;
				throw Error;
			}
			catch( SQLException err )	{
				System.out.print("SQL Error: " + err.getMessage());
				return false;
			}
			catch( Exception err )	{
				System.out.print("Error: " + err.getMessage());
				return false;
			}
			finally	{
				if ( lobjConexion != null)
					lobjConexion.cierraConexionDB();
			}
			/*************************************************************/


			for (int i=0; i<lsNombres.length; i++)	{
				if (!lbNoError) break;

				if (lsOrdenes[i].equals(""))	liOrden = 0;
				else 							liOrden = Integer.parseInt( lsOrdenes[i] );

				if ( lsTotalesCampo != null )	{
					if( Integer.parseInt(lsTotalesCampo[ liContadorTotales ]) == i )	{
						lsTotal = "S";
						if ( lsTotalesCampo.length-1 > liContadorTotales )	liContadorTotales++;
					}
					else
						lsTotal = "N";
				}
				else 	lsTotal = "N";

				lsLongEntero = lsLongitudesCampo[liContadorLong];
				if ( lsTiposCampo[i].equals("numerico") ) {
					liContadorLong++;
					lsLongDec = lsLongitudesCampo[liContadorLong];
				}
				else	lsLongDec = "0";
				liContadorLong++;
				int liLongEntero = Integer.parseInt(lsLongEntero) + Integer.parseInt(lsLongDec);

				if (lsCaracterCampo.equals("D"))
					lsQryCampos = "INSERT INTO com_tabla_pub_atrib_d (" +
						"	ic_atributo_d, ic_publicacion, cg_nombre_atrib, cg_tipo_dato, ig_long_atrib, " +
						"	cs_total, ig_ordenamiento, ig_long_atrib_d) "	+
						"VALUES ("+liNumAtributo+", "+ liNumPub +", '"+lsNombres[i]+"', '"+lsTiposCampo[i]+"', "+liLongEntero+", " +
						"	'"+lsTotal+"', 	"+liOrden+", "+lsLongDec+"	)";
				else
					lsQryCampos = "INSERT INTO com_tabla_pub_atrib (" +
						"	ic_atributo, ic_publicacion, cg_nombre_atrib, cg_tipo_dato, ig_long_atrib, cs_tipo_atrib, " +
						"	cs_total, ig_ordenamiento, ig_long_atrib_d) "	+
						"VALUES ("+liNumAtributo+", "+ liNumPub +", '"+lsNombres[i]+"', '"+lsTiposCampo[i]+"', "+liLongEntero+", " +
						"	'"+lsCaracterCampo+"', '"+lsTotal+"', 	"+liOrden+", "+lsLongDec+"	)";

				liNumAtributo++;


				try	{
					try {	lobjConexion.conexionDB();	}
					catch (Exception error)	{
						throw new NafinException("SIST0001");
					}

					lobjConexion.ejecutaSQL( lsQryCampos );
					System.out.println("se ha guardado un atributo mas.");

				}
				catch (NafinException Error)	{
					lbNoError = false;
					throw Error;
				}
				catch(Exception err)	{
					System.out.print("ERROR: " + err.getMessage() );
					lbNoError = false;
				}
				finally	{
					if ( lobjConexion != null)	{
						lobjConexion.terminaTransaccion(lbNoError);
						lobjConexion.cierraConexionDB();
					}
				}
			} //---------------------------------------------------------------------------- FIN DEL FOR
		}
		else	{
			System.out.print("Hay diferencia en le numero de campos a insertar");
		}

		return lbNoError;
	}









	/*************************************************
	*	OBTIENE EL NOMBRE DE LA PUBLICACION
	***************************************************/

	public String obtenNombrePub(int liNumPublicacion)
		throws NafinException	{

		String lsNombrePublicacion = "";
		String lsQryNomPub = "SELECT cg_nombre FROM com_publicacion WHERE ic_publicacion = " + liNumPublicacion;

		AccesoDB lobjConexion = new AccesoDB();

		try	{
			try {	lobjConexion.conexionDB();	}
			catch (Exception error)	{
				throw new NafinException("SIST0001");
			}

			ResultSet lCurNomPub = lobjConexion.queryDB( lsQryNomPub );
			while ( lCurNomPub.next() )
				lsNombrePublicacion = lCurNomPub.getString("cg_nombre");
			lCurNomPub.close();
		}

		catch (NafinException Error)	{
			throw Error;
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally	{
			if ( lobjConexion != null)	lobjConexion.cierraConexionDB();
		}

		return lsNombrePublicacion;
	}







	public void guardaRespuestaPyme(String[] msQuerys, String[] msNumIds, String iNoCliente, String lsNumPublicacion)
		throws NafinException	{

		boolean lbNoError = true;
		AccesoDB lobjConexion = new AccesoDB();

		try	{
			try 					{	lobjConexion.conexionDB();	}
			catch (Exception error)	{	lbNoError = false;  throw new NafinException("SIST0001");	}

			for (int i=0; i<msQuerys.length; i++  )	{
				if ( msQuerys[i].equals("") )	continue;   // ********* NO SE GRABA

				String lsQrTrunc = msQuerys[i].substring( 0, msQuerys[i].length()-2 ), /// ***** KITANDO LA COMA KE SOBRA
					   lsQry = "INSERT INTO tabla_edit_" + lsNumPublicacion +
							   " VALUES ("+msNumIds[i]+ ", " + iNoCliente + ", " + lsQrTrunc + ")";

				lobjConexion.ejecutaSQL( lsQry );
			}
		}
		catch (NafinException Error)	{
			lbNoError = false;
			throw Error;
		}
		catch(Exception err)	{
			System.out.print("ERROR: " + err.getMessage() );
			lbNoError = false;
			throw new NafinException("SIST0001");
		}
		finally	{
			if ( lobjConexion != null)	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}
	}




	/************************************************************************************************
	*
	*	IMPORTA DATOS DE TIPO FIJOS
	*
	************************************************************************************************/

	public void importaDatosFijos( String lsPathArchivo, String lsNumPublicacion, String lsNumEPO)
		throws NafinException	{

		boolean lbNoError = false;
		Vector 	lVTipoDatos = obtenTipoDatos( lsNumPublicacion, "F" ),
				lVValoresDa = new Vector();

		if ( lVTipoDatos.size() == 0 )	return;

		try	{
			InputStream fImportar          = new FileInputStream( lsPathArchivo );
		    int 		liNumBytes         = fImportar.available();
		    char[] 		laContenido        = new char[liNumBytes];
			String		lsContenidoArchImp = "",
						lsRegistro         = "",
						lsCampo            = "";

			StringTokenizer lstTramaRegis,
							lstTramaCampo;



		    for (int i=0; i<liNumBytes; i++)	laContenido[i] = (char) fImportar.read();

		    lsContenidoArchImp = String.valueOf(laContenido);

			mobjConexion  = new AccesoDB();
			try 					{	mobjConexion.conexionDB();	}
			catch (Exception error)	{	lbNoError = false;  throw new NafinException("SIST0001");	}

			/**************************************~
			*	Desentramando Registros
			******/

			lsContenidoArchImp = lsContenidoArchImp.trim();
			String lsUltCaracter = lsContenidoArchImp.substring( lsContenidoArchImp.length()-1);
			if (  !lsUltCaracter.equals(DELIMITADOR_CAMPO)  )	lsContenidoArchImp += DELIMITADOR_CAMPO;


			lstTramaRegis = new StringTokenizer(lsContenidoArchImp, DELIMITADOR_REGISTRO);
			while ( lstTramaRegis.hasMoreTokens() ) {

				lsRegistro = lstTramaRegis.nextToken();
				lsRegistro = lsRegistro.substring( 0, lsRegistro.length()-1 );
				lsUltCaracter = lsRegistro.substring( lsRegistro.length()-1);
				if (  !lsUltCaracter.equals(DELIMITADOR_CAMPO)  )	lsRegistro += DELIMITADOR_CAMPO;

				//System.out.println("\n lsRegistro: (" + lsRegistro + ")\n");

	    		/**************************************~
				*	Desentramando Campos
				******/
				lstTramaCampo = new StringTokenizer( lsRegistro, DELIMITADOR_CAMPO );
				lVValoresDa = new Vector();
				while ( lstTramaCampo.hasMoreTokens() )	{
					lsCampo = lstTramaCampo.nextToken();
					lVValoresDa.addElement( lsCampo );
				}

				lbNoError = insertaCampoFijo( lVValoresDa, lVTipoDatos, lsNumPublicacion, lsNumEPO );

				if ( !lbNoError )	throw new NafinException("ADMC0001");
			}
		}
		catch (NafinException Error)	{
			lbNoError = false;
			throw Error;
		}
		catch (Exception err)	{
			lbNoError = false;
			System.out.println("ERROR EN EJB: " + err);
			throw new NafinException("SIST0001");
		}
		finally	{
			if ( mobjConexion != null )	{
				mobjConexion.terminaTransaccion(lbNoError);
				mobjConexion.cierraConexionDB();
			}
		}
	}






	/************************************************************************************************
	*
	*	IMPORTA DATOS DE TIPO DETALLE
	*
	************************************************************************************************/

	public void importaDatosDetalle( String lsPathArchivo, String lsNumPublicacion)
		throws NafinException	{

		Vector 	lVTipoDatos = obtenTipoDatos( lsNumPublicacion, "D" ),
				lVValoresDa = new Vector();
		boolean lbNoError = false;

		if ( lVTipoDatos.size() == 0 )	return;

		try	{
			InputStream fImportar          = new FileInputStream( lsPathArchivo );
		    int 		liNumBytes         = fImportar.available();
		    char[] 		laContenido        = new char[liNumBytes];
			String		lsContenidoArchImp = "",
						lsRegistro         = "",
						lsCampo            = "";

			StringTokenizer lstTramaRegis,
							lstTramaCampo;

			mobjConexion  = new AccesoDB();
			try 					{	mobjConexion.conexionDB();	}
			catch (Exception error)	{	lbNoError = false;  throw new NafinException("SIST0001");	}


		    for (int i=0; i<liNumBytes; i++)	laContenido[i] = (char) fImportar.read();

		    lsContenidoArchImp = String.valueOf(laContenido);



			/**************************************~
			*	Desentramando Registros
			******/

			lsContenidoArchImp = lsContenidoArchImp.trim();
			String lsUltCaracter = lsContenidoArchImp.substring( lsContenidoArchImp.length()-1);
			if (  !lsUltCaracter.equals(DELIMITADOR_CAMPO)  )	lsContenidoArchImp += DELIMITADOR_CAMPO;

			System.out.println("\n\n lsContenidoArchImp: " + lsContenidoArchImp + "\n");

			lstTramaRegis = new StringTokenizer(lsContenidoArchImp, DELIMITADOR_REGISTRO);

			while ( lstTramaRegis.hasMoreTokens() ) {

				/**************************************~
				*	Desentramando Campos
				******/

				lsRegistro = lstTramaRegis.nextToken();

				lsRegistro = lsRegistro.substring( 0, lsRegistro.length()-1 );
				lsUltCaracter = lsRegistro.substring( lsRegistro.length()-1);
				if (  !lsUltCaracter.equals(DELIMITADOR_CAMPO)  )	lsRegistro += DELIMITADOR_CAMPO;

				System.out.println("\n\n lsRegistro: " + lsRegistro + "\n");

				lstTramaCampo = new StringTokenizer( lsRegistro, DELIMITADOR_CAMPO );
				lVValoresDa = new Vector();
				while ( lstTramaCampo.hasMoreTokens() )	{
					lsCampo = lstTramaCampo.nextToken();
					lVValoresDa.addElement( lsCampo );
				}

				lbNoError = insertaCampoDetalle( lVValoresDa, lVTipoDatos, lsNumPublicacion );
				if ( !lbNoError )	throw new NafinException("ADMC0001");
			}
		}
		catch (NafinException Error)	{
			lbNoError = false;
			throw Error;
		}
		catch (Exception err)	{
			System.out.println("ERROR EN EJB: " + err);
			lbNoError = false;
			throw new NafinException("SIST0001");
		}
		finally	{
			if ( mobjConexion != null )	{
				mobjConexion.terminaTransaccion(lbNoError);
				mobjConexion.cierraConexionDB();
			}
		}
	}




	/************************************************************************************************
	*
	*	INSERTA CAMPOS FIJOS
	*
	************************************************************************************************/

	public boolean insertaCampoFijo( Vector lVValoresDatos, Vector lVTiposDatos, String lsNumPublicacion, String lsNumEPO )
		throws NafinException	{

		System.out.print("\n Entrando a insertaCampoFijo \n\n");

		if ( lVValoresDatos.size() == 0 || lVTiposDatos.size() == 0 ) return false;
		if ( !checaPseudollave(lsNumPublicacion, (String)lVValoresDatos.elementAt(0), (String)lVValoresDatos.elementAt(1)) )	{
			System.out.println("LLAVE REPETIDA: '" + (String)lVValoresDatos.elementAt(0)  + "' y '" + (String)lVValoresDatos.elementAt(1) + "'");
			return false;
		}

		int		 liNumId       = obtenNumIdSig( lsNumPublicacion, "F", mobjConexion );
		boolean  lbNoError     = true;
		String 	 lsInsertDatos = "INSERT INTO tabla_"+lsNumPublicacion+" VALUES (" + liNumId + ", ",
				 lsCampo       = "";

		try	{

			/***********************************~
			*	ARMANDO INSERT
			******/

			for (int i = 0; i<=lVTiposDatos.size(); i++)	{

				lsCampo = (String) lVValoresDatos.elementAt(i);

				if (i == 0)	{	// EL PRIMER CAMPO CORRESPONDE AL NUM PROVEE-DISTRI
					lsInsertDatos += "'" + lsCampo + "'";

					if ( !hayUsuariosPub( lsNumEPO, lsNumPublicacion, lsCampo ) )	{
						insertaUsuariosAPublicacion( lsNumEPO, lsNumPublicacion, lsCampo );
					}
				}
				else	{
					if (   ((String)lVTiposDatos.elementAt(i-1)).equals("alfanumerico")
									||   ((String)lVTiposDatos.elementAt(i-1)).equals("seleccion")   )
						lsInsertDatos += "'" + lsCampo + "'";
					else if (   ((String)lVTiposDatos.elementAt(i-1)).equals("numerico")    )
						lsInsertDatos += lsCampo;
					else if (   ((String)lVTiposDatos.elementAt(i-1)).equals("hora")    )
						lsInsertDatos += "TO_DATE('" + lsCampo + "', 'hh24:mi')";
					else if (   ((String)lVTiposDatos.elementAt(i-1)).equals("fecha")    )
						lsInsertDatos += "TO_DATE('" + lsCampo + "', 'dd/mm/yyyy')";
				}

				if ( i < lVTiposDatos.size() )	// SI NO ES EL ULTIMO CAMPO SE PONE COMA..
						lsInsertDatos += ", ";

			}	// FIN DEL FOR
			lsInsertDatos += ")";

			//System.out.print("\n Query armado lsInsertDatos: " + lsInsertDatos + "\n");

			mobjConexion.ejecutaSQL( lsInsertDatos );
		}

		/*catch (NafinException Error)	{
			lbNoError = false;
			throw Error;
		}*/
		catch(SQLException errSql)	{
			lbNoError = false;
		}
		catch(Exception err)	{
			lbNoError = false;
			System.out.println("insertaCampoFijo: Exception: " + err);
			throw new NafinException("ADMC0001");
		}
		/*
		finally	{
			if ( lobjConexion != null )	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}*/

		return lbNoError;
	}





	/************************************************************************************************
	*
	*	INSERTA CAMPOS DE DETALLE     (POR REGISTRO)
	*
	************************************************************************************************/

	public boolean insertaCampoDetalle( Vector lVValoresDatos, Vector lVTiposDatos, String lsNumPublicacion )
		throws NafinException	{

		System.out.println("LLEGANDO A INSERTADETALLE CON lVValoresDatos: " + lVValoresDatos + "\n");

		if ( lVValoresDatos.size() == 0 || lVTiposDatos.size() == 0 ) return false;

		System.out.println("1");

		int		 liNumId       = obtenNumId( lsNumPublicacion, (String)lVValoresDatos.elementAt(0), (String)lVValoresDatos.elementAt(1), mobjConexion ),
				 liNumId_d     = obtenNumIdSig( lsNumPublicacion, liNumId, mobjConexion);
		boolean  lbNoError     = true;
		String 	 lsInsertDatos = "INSERT INTO tabla_d_"+lsNumPublicacion+" VALUES (" + liNumId + ", " + liNumId_d + ", ",
				 lsCampo       = "",
				 lsTipoDato    = "";
		System.out.println("3");

		if ( liNumId == -1 )	{
			/******************************************************************************
			*	SI REGRESA CERO ES PORKE NO HAY RELACION EN LA TABLA DE CAMPOS FIJOS
			********/
			System.out.println("NO HAY RELACION DE LOS CAMPOS (" + (String)lVValoresDatos.elementAt(0) + " Y " + (String)lVValoresDatos.elementAt(1) + ")");
			return false;
		}

		System.out.println("ENTRANDO AL TRY DESPUES DE LAS FUNCIONES \n");
		System.out.println("lVTiposDatos: " + lVTiposDatos + " \n");
		System.out.println("lVValoresDatos: " + lVValoresDatos + " \n");


		try	{
			/***********************************~
			*	ARMANDO INSERT
			******/

			for (int i = 0; i<lVTiposDatos.size(); i++)	{

				lsCampo    = (String) lVValoresDatos.elementAt(i+2);
				lsTipoDato = (String) lVTiposDatos.elementAt(i);

				if ( lsTipoDato.equals("alfanumerico")  ||  lsTipoDato.equals("seleccion")   )
					lsInsertDatos += "'" + lsCampo + "'";
				else if ( lsTipoDato.equals("numerico")    )
					lsInsertDatos += lsCampo;
				else if ( lsTipoDato.equals("hora")    )
					lsInsertDatos += "TO_DATE('" + lsCampo + "', 'hh24:mi')";
				else if ( lsTipoDato.equals("fecha")    )
					lsInsertDatos += "TO_DATE('" + lsCampo + "', 'dd/mm/yyyy')";

				if ( i < lVTiposDatos.size()-1 )	// SI NO ES EL ULTIMO CAMPO SE PONE COMA..
						lsInsertDatos += ", ";

			}	// FIN DEL FOR
			lsInsertDatos += ")";

			System.out.println("\n lsInsertDatos: " + lsInsertDatos + "\n");
			mobjConexion.ejecutaSQL( lsInsertDatos );
		}
		catch(SQLException errSql)	{
			System.out.println("Se ha producido un error en la clase CImportaReg en la clase insertaCampoDetalle.");
			System.out.println("ERROR: " + errSql.getMessage());
			lbNoError = false;
		}
		catch(Exception err)	{
			lbNoError = false;
			throw new NafinException("ADMC0001");
		}

		return lbNoError;
	}





	/************************************************************************************************
	*
	*	REGRESA EL NUMERO DE ID SIGUIENTE DE CAMPOS FIJOS O EDITABLES
	*
	************************************************************************************************/

	private int obtenNumIdSig( String lsNumPublicacion, String lsCaracter, AccesoDB  lobjConexion)
		throws NafinException	{

		String 	 lsQryNumId = "SELECT NVL(MAX(id)+1, 0) AS num_id_sig FROM tabla_" + lsNumPublicacion;
		int 	 liNumId = 0;

		try	{

			ResultSet lCurNumId = lobjConexion.queryDB( lsQryNumId );
			if ( lCurNumId.next() )	liNumId = lCurNumId.getInt("num_id_sig");
			lCurNumId.close();
		}
		/*catch (NafinException Error)	{
			throw Error;
		}*/
		catch(SQLException errSql)	{
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			throw new NafinException("ADMC0001");
		}

		return liNumId;
	}



	/************************************************************************************************
	*	REGRESA EL NUMERO DE ID QUE LE CORRESPONDE A LA PSEUDOLLAVE
	*	SE USA PARA RELACIONES CON LOS CAMPOS DE DETALLE
	************************************************************************************************/

	private int obtenNumId( String lsNumPublicacion, String lsNumProv, String lsCampo1, AccesoDB lobjConexion )
		throws NafinException	{

		String 	 lsQryNumId = "SELECT id FROM tabla_"+lsNumPublicacion+" WHERE campo0 = '"+lsNumProv+"' AND campo1 = '"+lsCampo1+"'";
		int 	 liNumId = -1;

		try	{

			ResultSet lCurNumId = lobjConexion.queryDB( lsQryNumId );
			if ( lCurNumId.next() )	liNumId = lCurNumId.getInt("id");
			lCurNumId.close();
		}
		catch(SQLException errSql)	{
			System.out.println("SQL ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("ERROR: " + err.getMessage());
			throw new NafinException("ADMC0001");
		}
		return liNumId;
	}



	/************************************************************************************************
	*
	*	REGRESA EL NUMERO DE ID SIGUIENTE DE CAMPOS DETALLE
	*
	************************************************************************************************/

	private int obtenNumIdSig( String lsNumPublicacion, int liId, AccesoDB  lobjConexion )
		throws NafinException	{

		String 	 lsQryNumId = "SELECT NVL(MAX(id_d)+1, 0) AS num_id_sig FROM tabla_d_"+lsNumPublicacion+" WHERE id = " + liId;
		int 	 liNumId = 0;

		try	{
			ResultSet lCurNumId = lobjConexion.queryDB( lsQryNumId );
			if ( lCurNumId.next() )	liNumId = lCurNumId.getInt("num_id_sig");
			lCurNumId.close();
		}
		catch(SQLException errSql)	{
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("ERROR: " + err.getMessage());
			throw new NafinException("ADMC0001");
		}

		return liNumId;
	}




	/************************************************************************************************
	*
	*	REGRESA UN VECTOR DE DE LOS TIPOS DE DATOS FIJOS
	*
	************************************************************************************************/

	public Vector obtenTipoDatos( String lsNumPublicacion, String lsCaracterCampo )
		throws NafinException	{

		String lsQryTipoDatos = "";
		Vector lVTipoDatos    = new Vector();
		AccesoDB lobjConexion = new AccesoDB();

		if ( lsCaracterCampo.equals("F") || lsCaracterCampo.equals("E"))
			lsQryTipoDatos = "SELECT cg_tipo_dato FROM com_tabla_pub_atrib " +
							"WHERE cs_tipo_atrib = '"+lsCaracterCampo+"' AND ic_publicacion = "+lsNumPublicacion+" " +
							"ORDER BY ic_atributo";
		else if ( lsCaracterCampo.equals("D") )
			lsQryTipoDatos = "SELECT cg_tipo_dato FROM com_tabla_pub_atrib_d " +
							"WHERE ic_publicacion = "+lsNumPublicacion+" " +
							"ORDER BY ic_atributo_d";

		try	{
			try 					{	lobjConexion.conexionDB();	}
			catch (Exception error)	{	throw new NafinException("SIST0001");	}

			ResultSet lCurTDatos = lobjConexion.queryDB( lsQryTipoDatos );
			while ( lCurTDatos.next() )
				lVTipoDatos.addElement( lCurTDatos.getString("cg_tipo_dato") );
			lCurTDatos.close();
		}
		catch (NafinException Error)	{
			throw Error;
		}
		catch(SQLException errSql)	{
			System.out.println("ERROR: " + errSql.getMessage());
		}
		catch(Exception err)	{
			System.out.println("ERROR: " + err.getMessage());
			throw new NafinException("ADMC0001");
		}
		finally	{
			if ( lobjConexion != null)	lobjConexion.cierraConexionDB();
		}

		return lVTipoDatos;
	}



	/*******************************************************************
	*
	*	VERIFICA SI HAY USUARIOS PARA ESTA PUBLICACION
	*
	*******************************************************************/

	public boolean hayUsuariosPub( String lsNumEPO, String lsNumPublicacion, String lsProveedor )
		throws NafinException	{
		System.out.print("CTablasDinamicasBean.hayUsuariosPub(E)");
		boolean  lbHayUsr = false;
		//AccesoDB lobjConexion = new AccesoDB();


		String   lsQryUsuarios = 
					" SELECT COUNT(*) AS num_usuarios, 'TablasDinamicasEJB::hayUsuariosPub()'"+
					" FROM comrel_publicacion_usuario"   +
					" WHERE ic_publicacion = ?"   +
					"     AND ic_nafin_electronico IN (SELECT ic_nafin_electronico FROM comrel_nafin"   +
					"                             WHERE ic_epo_pyme_if IN"   +
					"                                 (SELECT ic_pyme FROM comrel_pyme_epo"   +
					" 										WHERE cg_pyme_epo_interno = ?"   +
					" 											AND ic_epo = ? )"   +
					" 							AND cg_tipo = 'P'"   +
					" 						)"  ;



		try	{
			if (mobjConexion.hayConexionAbierta() == false) {
				try {	mobjConexion.conexionDB();	}
				catch (Exception error)	{
					throw new NafinException("SIST0001");
				}
			}

			PreparedStatement pstmt = null;
			pstmt = mobjConexion.queryPrecompilado(lsQryUsuarios);
			long numero1=Long.parseLong(lsNumPublicacion);
			pstmt.setLong(1,numero1);
			pstmt.setString(2,lsProveedor);
			long numero3=Long.parseLong(lsNumEPO);
			pstmt.setLong(3,numero3);
			ResultSet lCurNumUsuarios = pstmt.executeQuery();
			pstmt.clearParameters();

			/*ResultSet lCurNumUsuarios = lobjConexion.queryDB( lsQryUsuarios );
			*/

			if ( lCurNumUsuarios.next() )
				lbHayUsr = (lCurNumUsuarios.getInt("num_usuarios") > 0) ? true : false;
			else
				throw new NafinException("PARM0012");
			lCurNumUsuarios.close();
			if(pstmt != null) pstmt.close();
		}
		catch (NafinException Error)	{
			System.out.print("CTablasDinamicasBean.hayUsuariosPub(NafinException):"+Error );
			throw Error;
		}
		catch(Exception err)	{
			System.out.print("CTablasDinamicasBean.hayUsuariosPub(Exception):"+err.getMessage() );
			throw new NafinException("SIST0001");
		} finally {
			System.out.print("CTablasDinamicasBean.hayUsuariosPub(S)");
		}

		return lbHayUsr;
	}



	/*******************************************************************
	*
	*	AGREGA USUARIOS A LA PUBLICACION POR NUMERO DE PROVEEDOR
	*
	*******************************************************************/

	public void insertaUsuariosAPublicacion( String lsNumEPO, String lsNumPublicacion, String lsProveedor )
		throws NafinException	{

		boolean  lbOK     = true;
		System.out.print("CTablasDinamicasBean.insertaUsuariosAPublicacion(E)");

		//AccesoDB lobjConexion = new AccesoDB();
		String   lsQryUsuarios = "INSERT INTO comrel_publicacion_usuario (ic_nafin_electronico, ic_publicacion) " +
								 "	 (SELECT ic_nafin_electronico, "+lsNumPublicacion+" AS ic_publicacion FROM comrel_nafin " +
								 "	      WHERE ic_epo_pyme_if IN (SELECT ic_pyme FROM comrel_pyme_epo " +
								 "								WHERE cg_pyme_epo_interno = '"+lsProveedor+"' " +
								 "									AND ic_epo = "+lsNumEPO+")"+
								 "		 AND cg_tipo = 'P'"+
								 "	)";
		try	{
			if (mobjConexion.hayConexionAbierta() == false) {
				try {	mobjConexion.conexionDB();	}
				catch (Exception error)	{
					throw new NafinException("SIST0001");
				}
			}

			mobjConexion.ejecutaSQL( lsQryUsuarios );
		}
		catch (NafinException Error)	{
			System.out.print("CTablasDinamicasBean.insertaUsuariosAPublicacion(NafinException):"+Error );
			throw Error;
		}
		catch(Exception err)	{
			System.out.print("CTablasDinamicasBean.insertaUsuariosAPublicacion(Exception):"+err.getMessage() );
			lbOK = false;
			throw new NafinException("PARM0011");
		}
		finally	{
			if (mobjConexion.hayConexionAbierta() == true) {
				mobjConexion.terminaTransaccion(lbOK);
			}
			System.out.print("CTablasDinamicasBean.insertaUsuariosAPublicacion(S)");
		}
	}

	/****************************************************************************************************************/
















	/*********************************************************************************************
	*
	*	    	GRABA LA PUBLICACION
	*
	*********************************************************************************************/

	public void grabaPublicacion(	int    liNumEpo, 	int liTipoPublicacion, 	String lsNombreTabla,	String lsFchIni,
									String lsFchFin,	int liNumColumnas)	throws NafinException	{
		boolean lbError = false;
		int liNumPublicacion = 0;

		System.out.println("\n\n");
		System.out.println("Numero de la epo: " + liNumEpo);
		System.out.println("Tipo de Publcacion: " + liTipoPublicacion);
		System.out.println("Nombre de la Tabla: " + lsNombreTabla);
		System.out.println("Fecha inicio: " + lsFchIni);
		System.out.println("Fecha Fin: " + lsFchFin);
		System.out.println("Numero de columnas: " + liNumColumnas);


		String lsQryNumMaxPublicacion = "SELECT NVL(MAX (ic_publicacion),0) + 1 as num_pub FROM com_publicacion";
		AccesoDB lobjConexion = new AccesoDB();
		ResultSet lcurNumMaxPub = null;

		try {
			try {
				lobjConexion.conexionDB();
			} catch (Exception error){
				lbError = true;
				throw new NafinException("SIST0001");
			}

			lcurNumMaxPub = lobjConexion.queryDB( lsQryNumMaxPublicacion );

			if (lcurNumMaxPub.next())
				liNumPublicacion = lcurNumMaxPub.getInt("num_pub");
			else{
				lbError = true;
				throw new NafinException("PARM0001");  //Error al obtener el N�mero de Tabla
			}
			lcurNumMaxPub.close();

			System.out.println("\n Publicacion a Grabar: " + liNumPublicacion);

		}

		catch (NafinException Error){
			lbError = true;
			throw Error;
		}
		catch (Exception epError){
			lbError = true;
			throw new NafinException("PARM0001");    //No es Posible Publicar Tablas en este momento, intenta m�s tarde
		}
		finally {
			lobjConexion.terminaTransaccion(!lbError);
			lobjConexion.cierraConexionDB();
		}

	}




	/*********************************************************************************************
	*
	*	    int icrearTabla()
	*
	*********************************************************************************************/
	public int icrearTabla(int eiNumCampos, CUsrPublicacion eoUsrPublicacion, String esNombreCampos[],
							String esTipoCampos[], String esLongitudes[] )
		throws NafinException
	{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";
		String lsUsuarios  = "";
		String lsNomTabla  = "";
		String lsTipoPubl  = "";
		String lsFechaIni  = "";
		String lsFechaFin  = "";

		int liCvePublicacion = 0;
		int liCveEPO = 0;
		int i;

		Vector lovArregloUser = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::icrearTabla(E)");
		try {
			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				lsCodError = "SIST0001";
				throw new NafinException("SIST0001");
			}

			lsCadenaSQL = "SELECT NVL(MAX (IC_PUBLICACION),0) + 1 n_pub FROM com_publicacion";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				liCvePublicacion = lrsSel.getInt(1);
			else{
				lsCodError = "PARM0001";
				throw new NafinException("PARM0001");  //Error al obtener el N�mero de Tabla
			}
			lrsSel.close();

			eiNumCampos += 1;

			liCveEPO   = eoUsrPublicacion.getCveEpo();
			lsNomTabla = eoUsrPublicacion.getNomPublicacion();
			lsTipoPubl = eoUsrPublicacion.getTipoPublicacion();
			lsFechaIni = eoUsrPublicacion.getFechaIni();
			lsFechaFin = eoUsrPublicacion.getFechaFin();
			lsUsuarios = eoUsrPublicacion.getUsuarios();

			esNombreCampos[0] = "cve_dist_prov";
			esTipoCampos[0] = "Alfanum�rico";
			esLongitudes[0] = "25";

			lovArregloUser = Comunes.explode("|",lsUsuarios);

			for (i = 0; i < lovArregloUser.size(); i++) {

				lsUsuarios = lovArregloUser.get(i).toString();

				lsCadenaSQL  = "INSERT INTO com_publicacion";
				lsCadenaSQL += " (IC_PUBLICACION, IC_USUARIO, IC_EPO, IC_TIPO_PUBLICACION, CG_NOMBRE,";
				lsCadenaSQL += "  DF_INI_PUB, DF_FIN_PUB, TT_CONTENIDO, CI_IMAGEN, IN_COLUMNAS, CG_ARCHIVO)";
				lsCadenaSQL += " values ("+ liCvePublicacion +",'"+ lsUsuarios +"',"+ liCveEPO +",";
				lsCadenaSQL += lsTipoPubl +",'"+ lsNomTabla +"',TO_DATE('"+ lsFechaIni +"','DD/MM/YYYY'),";
				lsCadenaSQL += " TO_DATE('"+ lsFechaFin +"','DD/MM/YYYY'),null,null,"+ eiNumCampos +",null)";

				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}
			for (i = 0; i < eiNumCampos; i++) {
				lsCadenaSQL  = "INSERT INTO com_tabla_pub_atrib";
				lsCadenaSQL += " (IC_ATRIBUTO, IC_PUBLICACION, CG_NOMBRE_ATRIB, IG_LONG_ATRIB, CG_TIPO_DATO)";
				lsCadenaSQL += " VALUES ("+ String.valueOf(i) +","+ liCvePublicacion +",'";
				lsCadenaSQL +=  esNombreCampos[i] +"',"+ esLongitudes[i] +",'"+ esTipoCampos[i] +"')";

				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}

			lsCadenaSQL = "CREATE TABLE tabla_" + liCvePublicacion + " (id number,";
			for (i = 0; i < eiNumCampos; i++) {
				esTipoCampos[i] = (esTipoCampos[i].equals("Alfanum�rico")) ? "varchar2": "number";
				if (esLongitudes[i].equals(""))
					esLongitudes[i] = "1";
				lsCadenaSQL += "campo"+ String.valueOf(i) +" "+ esTipoCampos[i] +" ("+ esLongitudes[i] +"),";
			}
			lsCadenaSQL += " constraint tabla_"+ liCvePublicacion +" primary key (id))";

			lodbConexion.ejecutaSQL(lsCadenaSQL);

			return liCvePublicacion;
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "PARM0001";
			throw new NafinException("PARM0001");    //No es Posible Publicar Tablas en este momento, intenta m�s tarde
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" TablasDinamicasEJB::icrearTabla(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    int icrearTabla()
	*
	*********************************************************************************************/
	public int icrearTabla(int eiNumCampos, String esNomTabla, String esTipoPub, String esUsuarios,
							String esFechaIni, String esFechaFin, int eiCveEPO, String esNombreCampos[],
							String esTipoCampos[], String esLongitudes[] )
		throws NafinException
	{
		boolean lbOK = true;

		String lsCodError = "OK";
		String lsCadenaSQL = "";

		int liCvePublicacion = 0;
		int i;

		Vector lovArregloUser = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::icrearTabla(E)");

		System.out.println("\t eiNumCampos:" + eiNumCampos);
		System.out.println("\t esNomTabla:" + esNomTabla);
		System.out.println("\t esTipoPub:" + esTipoPub);
		System.out.println("\t esUsuarios:" + esUsuarios);
		System.out.println("\t esFechaIni:" + esFechaIni);
		System.out.println("\t esFechaFin:" + esFechaFin);
		System.out.println("\t eiCveEPO:" + eiCveEPO);
		System.out.println("\t esNombreCampos:" + esNombreCampos);
		System.out.println("\t esTipoCampos:" + esTipoCampos);
		System.out.println("\t esLongitudes:" + esLongitudes);

		try {
			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				lsCodError = "SIST0001";
				throw new NafinException("SIST0001");
			}

			lsCadenaSQL = "SELECT NVL(MAX (IC_PUBLICACION),0) + 1 n_pub FROM com_publicacion";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				liCvePublicacion = lrsSel.getInt(1);
			else{
				lsCodError = "PARM0001";
				throw new NafinException("PARM0001");  //Error al obtener el N�mero de Tabla
			}
			lrsSel.close();

			System.out.println("Clave de la Publicaci�n: "+ liCvePublicacion);

			eiNumCampos += 1;

			esNombreCampos[0] = "cve_dist_prov";
			esTipoCampos[0] = "Alfanum�rico";
			esLongitudes[0] = "25";

			lovArregloUser = Comunes.explode("|",esUsuarios);

			System.out.println("Inicia la inserci�n");
			for (i = 0; i < lovArregloUser.size(); i++) {

				esUsuarios = lovArregloUser.get(i).toString();

				lsCadenaSQL  = "INSERT INTO com_publicacion";
				lsCadenaSQL += " (IC_PUBLICACION, IC_USUARIO, IC_EPO, IC_TIPO_PUBLICACION, CG_NOMBRE,";
				lsCadenaSQL += "  DF_INI_PUB, DF_FIN_PUB, TT_CONTENIDO, CI_IMAGEN, IN_COLUMNAS, CG_ARCHIVO)";
				lsCadenaSQL += " values ("+ liCvePublicacion +",'"+ esUsuarios +"',"+ eiCveEPO +",";
				lsCadenaSQL += esTipoPub +",'"+ esNomTabla +"',TO_DATE('"+ esFechaIni +"','DD/MM/YYYY'),";
				lsCadenaSQL += "TO_DATE('"+ esFechaFin +"','DD/MM/YYYY'),null,null,"+ eiNumCampos +",null)";

				System.out.println(lsCadenaSQL);

				lodbConexion.ejecutaSQL(lsCadenaSQL);

				System.out.println("Inserto la Tabla en la Tabla de com_publicacion");
			}
			for (i = 0; i < eiNumCampos; i++) {
				lsCadenaSQL  = "INSERT INTO com_tabla_pub_atrib";
				lsCadenaSQL += " (IC_ATRIBUTO, IC_PUBLICACION, CG_NOMBRE_ATRIB, IG_LONG_ATRIB, CG_TIPO_DATO)";
				lsCadenaSQL += " VALUES ("+ String.valueOf(i) +","+ liCvePublicacion +",'";
				lsCadenaSQL +=  esNombreCampos[i] +"',"+ esLongitudes[i] +",'"+ esTipoCampos[i] +"')";

				lodbConexion.ejecutaSQL(lsCadenaSQL);
				System.out.println("\t Inserto los atributos de la tabla");
			}

			System.out.println("Inicia para Crear la tabla");

			lsCadenaSQL = "CREATE TABLE tabla_" + liCvePublicacion + " (id number,";
			for (i = 0; i < eiNumCampos; i++) {
				esTipoCampos[i] = (esTipoCampos[i].equals("Alfanum�rico")) ? "varchar2": "number";
				System.out.println("\t valor1:" + esTipoCampos[i]);
				if (esLongitudes[i].equals(""))
					esLongitudes[i] = "1";
				System.out.print("\t Long1:" + esLongitudes[i]);
				lsCadenaSQL += "campo"+ i +" "+ esTipoCampos[i] +" ("+ esLongitudes[i] +"),";
			}
			lsCadenaSQL += " constraint tabla_"+ liCvePublicacion +" primary key (id))";

			System.out.println("\t Cadena SQL ->" + lsCadenaSQL);

			lodbConexion.ejecutaSQL(lsCadenaSQL);
			System.out.println("Crea la Tabla F�sica");

			return liCvePublicacion;
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			lsCodError = "PARM0001";
			throw new NafinException("PARM0001");    //No es Posible Publicar Tablas en este momento, intenta m�s tarde
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" TablasDinamicasEJB::icrearTabla(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    Vector ovconsultarEPOs()
	*
	*********************************************************************************************/
	public Vector ovconsultarEPOs()
			throws NafinException
	{
		String lsCadenaSQL  = "";

		Vector lovRegistro = null;
		Vector lovDatosTabla = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::ovconsultarEPOs(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "SELECT IC_EPO AS CLAVE,CG_RAZON_SOCIAL AS DESCRIPCION ";
			lsCadenaSQL += " FROM comcat_epo";
			lsCadenaSQL += " ORDER BY DESCRIPCION";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				do {
					lovRegistro = new Vector();

					lovRegistro.addElement(lrsSel.getString(1));
					lovRegistro.addElement(lrsSel.getString(2));

					lovDatosTabla.addElement(lovRegistro);

				} while (lrsSel.next());
			else
				throw new NafinException("PARM0010");

			return lovDatosTabla;
		} catch (NafinException Error){
			throw Error;
		} catch (Exception epError){
			throw new NafinException("SIST0001");
		} finally {
				lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::ovconsultarEPOs(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    Vector ovconsultarTiposPub()
	*
	*********************************************************************************************/
	public Vector ovconsultarTiposPub()
			throws NafinException
	{
		String lsCadenaSQL  = "";

		Vector lovRegistro = null;
		Vector lovDatosTabla = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::ovconsultarTiposPub(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "SELECT ic_tipo_publicacion as CLAVE, cd_tipo_publicacion as DESCRIPCION ";
			lsCadenaSQL += " FROM comcat_tipo_publicacion";
			lsCadenaSQL += " ORDER BY ic_tipo_publicacion";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				do {
					lovRegistro = new Vector();

					lovRegistro.addElement(lrsSel.getString(1));
					lovRegistro.addElement(lrsSel.getString(2));

					lovDatosTabla.addElement(lovRegistro);

				} while (lrsSel.next());
			else
				throw new NafinException("PARM0010");

			return lovDatosTabla;
		} catch (NafinException Error){
			throw Error;
		} catch (Exception epError){
			throw new NafinException("SIST0001");
		} finally {
				lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::ovconsultarTiposPub(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    Vector ovgetCampos()
	*
	*********************************************************************************************/
	public Vector ovgetCampos(int eiCvePublicacion)
			throws NafinException
	{
		String lsCadenaSQL  = "";
		String lsNombreDato = "";
		String lsTipoDato   = "";
		String lsLongitud   = "";

		Vector lovRegistro = null;
		Vector lovDatosCampos = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::ovgetCampos(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL  = "SELECT NVL(cg_nombre_atrib, ''), NVL(ig_long_atrib,0), NVL(cg_tipo_dato, '')";
			lsCadenaSQL += " FROM com_tabla_pub_atrib";
			lsCadenaSQL += " WHERE ic_publicacion = " + eiCvePublicacion;
			lsCadenaSQL += " ORDER BY ic_atributo";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next()){
				do {
					lsNombreDato = lrsSel.getString(1);
					lsLongitud   = lrsSel.getString(2);
					lsTipoDato   = lrsSel.getString(3);

					lovRegistro = new Vector();

					lovRegistro.addElement(lsNombreDato);
					lovRegistro.addElement(lsLongitud);
					lovRegistro.addElement(lsTipoDato);

					lovDatosCampos.addElement(lovRegistro);		//Agrega el registro al vector de los datos de los campos
				}while (lrsSel.next());
			} else {
				throw new NafinException("PARM0006"); 		//No se encontraron los atributos de la tabla
			}
			lrsSel.close();
			lodbConexion.cierraConexionDB();

			return lovDatosCampos;
		} catch (NafinException error){
			throw error;
		} catch (Exception epError){
			throw new NafinException("SIST0001");
		} finally {
			if (lodbConexion.hayConexionAbierta() == true)
				lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::ovgetCampos(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    Vector ovgetDatosTabla()
	*
	*********************************************************************************************/
	public Vector ovgetDatosTabla(int eiCvePublicacion)
			throws NafinException
	{
		String lsCadenaSQL  = "";

		int liNCampos = 0;

		Vector lovRegistro = null;
		Vector lovDatosTabla = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::ovgetDatosTabla(E)");
		try {
			lodbConexion.conexionDB();

			liNCampos = iObtieneNumeroColumnas(eiCvePublicacion, lodbConexion);

			liNCampos++;

			lsCadenaSQL = "SELECT * FROM tabla_"+ eiCvePublicacion + " ORDER BY id";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				do {
					lovRegistro = new Vector();
					for (int i = 1; i <= liNCampos; i++)
						lovRegistro.addElement(lrsSel.getString(i));

					lovDatosTabla.addElement(lovRegistro);

				} while (lrsSel.next());

			return lovDatosTabla;
		} catch (Exception epError){
			throw new NafinException("PARM0010");
		} finally {
				lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::ovgetDatosTabla(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    Vector ovgetTablas()
	*
	*********************************************************************************************/
	public Vector ovgetTablas(int eiCveEPO)
			throws NafinException
	{
		String lsCadenaSQL  = "";
		String lsCvePub  = "";
		String lsNomPub  = "";
		String lsFechIni = "";
		String lsFechFin = "";
		String lsNColumna = "";

		Vector lovRegistro = null;
		Vector lovTablas = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::ovgetTablas(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL  = "SELECT distinct(ic_publicacion),cg_nombre,TO_CHAR(df_ini_pub,'DD/MM/YYYY'),";
			lsCadenaSQL += " TO_CHAR(df_fin_pub,'DD/MM/YYYY'),in_columnas ";
			lsCadenaSQL += " FROM com_publicacion";
			lsCadenaSQL += " WHERE ic_epo = " + eiCveEPO;
			lsCadenaSQL += " AND ic_tipo_publicacion = 1";
			lsCadenaSQL += " ORDER BY ic_publicacion";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				do {
					lsCvePub   = lrsSel.getString(1);
					lsNomPub   = lrsSel.getString(2);
					lsFechIni  = lrsSel.getString(3);
					lsFechFin  = lrsSel.getString(4);
					lsNColumna = lrsSel.getString(5);

					lovRegistro = new Vector();
					lovRegistro.addElement(lsCvePub);
					lovRegistro.addElement(lsNomPub);
					lovRegistro.addElement(lsFechIni);
					lovRegistro.addElement(lsFechFin);
					lovRegistro.addElement(lsNColumna);

					lovTablas.addElement(lovRegistro);

				} while (lrsSel.next());

			return lovTablas;
		} catch (Exception epError){
			throw new NafinException("PARM0010");
		} finally {
				lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::ovgetTablas(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    Vector ovgetDatosRegistro()
	*
	*********************************************************************************************/
	public Vector ovgetDatosRegistro(int eiCvePublicacion, int eiCveRegistro)
			throws NafinException
	{
		String lsCadenaSQL  = "";

		int liNCampos = 0;

		Vector lovRegistro = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::ovgetDatosRegistro(E)");
		try {
			lodbConexion.conexionDB();

			liNCampos = iObtieneNumeroColumnas(eiCvePublicacion, lodbConexion);

			liNCampos++;

			lsCadenaSQL  = "SELECT * FROM tabla_"+ eiCvePublicacion;
			lsCadenaSQL += " WHERE id = " + eiCveRegistro;
			lsCadenaSQL += " ORDER BY id";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next()){
				lovRegistro = new Vector();
				for (int i = 1; i <= liNCampos; i++)
					lovRegistro.addElement(lrsSel.getString(i));
				}
			else
				throw new NafinException("PARM0010");

			return lovRegistro;
		} catch (NafinException error){
			throw error;
		} catch (Exception epError){
			throw new NafinException("SIST0001");
		} finally {
				lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::ovgetDatosRegistro(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    Vector ovgetInfTabla()
	*
	*********************************************************************************************/
	public Vector ovgetInfTabla(int eiCvePublicacion, int eiCveEPO)
			throws NafinException
	{
		String lsCadenaSQL  = "";
		String lsNomTabla  = "";
		String lsFechaIni  = "";
		String lsFechaFin  = "";

		Vector lovRegistro = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::ovgetInfTabla(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL  = "SELECT cg_nombre, TO_CHAR(df_ini_pub,'DD/MM/YYYY'),";
			lsCadenaSQL += " TO_CHAR(df_fin_pub,'DD/MM/YYYY')";
			lsCadenaSQL += " FROM com_publicacion";
			lsCadenaSQL += " WHERE ic_publicacion = "+ eiCvePublicacion;
			lsCadenaSQL += " AND ic_epo = "+ eiCveEPO;

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next()){
				lsNomTabla = lrsSel.getString(1);
				lsFechaIni = lrsSel.getString(2);
				lsFechaFin = lrsSel.getString(3);

				lovRegistro.addElement(lsNomTabla);
				lovRegistro.addElement(lsFechaIni);
				lovRegistro.addElement(lsFechaFin);
			}
			else
				throw new NafinException("PARM0010");   ////No se encontraron datos
			return lovRegistro;
		} catch (NafinException error){
			throw error;
		} catch (Exception epError){
			throw new NafinException("SIST0001");
		} finally {
				lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::ovgetInfTabla(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    String sinsertarRegistro()   [ Carga Masiva ]
	*
	*********************************************************************************************/
	public Vector sinsertarRegistro( int eiCvePublicacion, String esNombreArchivo )
			throws NafinException
	{
		boolean lbOK    = true;
		boolean lbError = false;

		String lsCadenaSQL = "";
		String lsLinea     = "";
		String lsValor     = "";
		String lsError     = "";

		int liTotalReg = 0;
		int liNCampos  = 0;
		int liLong     = 0;
		int liTotalRech = 0;
		int liID  = 0;

		String laNombre_campo[] = null;
		String laLong_campo[]   = null;
		String laTipo_campo[]   = null;
		String laDato[]         = null;

		VectorTokenizer lvt = null;
		Vector lovDatos = null;

		Vector lovResultado = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::sinsertarRegistro(E)");
		try {
			lodbConexion.conexionDB();

			System.out.println("Va a abrir el Archivo:"+esNombreArchivo);
			File lofArchivo = new File(esNombreArchivo);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo)));

			System.out.println("Abrio el Archivo");

			liNCampos = iObtieneNumeroColumnas(eiCvePublicacion, lodbConexion);

			System.out.println("Se obtiene el n�mero de registros de la tabla");

			lsCadenaSQL = "SELECT cg_nombre_atrib,ig_long_atrib,cg_tipo_dato";
			lsCadenaSQL += " FROM com_tabla_pub_atrib";
			lsCadenaSQL += " WHERE ic_publicacion = " + eiCvePublicacion;
			lsCadenaSQL += " order by ic_atributo";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			System.out.println("Obtiene los atributos de la tabla");

			laNombre_campo = new String[liNCampos];
			laLong_campo   = new String[liNCampos];
			laTipo_campo   = new String[liNCampos];
			laDato         = new String[liNCampos];

			System.out.println("Llena los arreglos de los atributos");
			for (liNCampos = 0; lrsSel.next(); liNCampos++) {
				laNombre_campo[liNCampos] = (lrsSel.getString(1) == null) ? "" : lrsSel.getString(1);
				laLong_campo[liNCampos]   = (lrsSel.getString(2) == null) ? "" : lrsSel.getString(2);
				laTipo_campo[liNCampos]   = (lrsSel.getString(3) == null) ? "" : lrsSel.getString(3);
			}
			lrsSel.close();

			System.out.println("Empieza a Leer el Archivo");
			while((lsLinea = br.readLine()) != null) {
				lbOK = true;
				//El numero total de registros cargados en el archivo.
				liTotalReg++;
				if (!lsLinea.equals("")) {
					System.out.println("Linea:"+liTotalReg);
					lvt = new VectorTokenizer(lsLinea,"|");
					lovDatos = lvt.getValuesVector();

					if( liNCampos == lovDatos.size()) {
						System.out.println("Lee los campos.");
						for (int i = 0; i < lovDatos.size(); i++) {
							try {
								lsValor = (lovDatos.get(i) == null) ? "" : (String)lovDatos.get(i);
							} catch(ArrayIndexOutOfBoundsException aioobe) {
								System.out.println(aioobe);
								throw new NafinException("PARM0009");	//Error al leer el archivo
							}
							lsValor = lsValor.trim();
							liLong = lsValor.length();
							if (liLong >= 1) {
								lsValor = lsValor.replace('\'',' ');
								lsValor = lsValor.replace('\\',' ');
								if (laTipo_campo[i].equals("Alfanum�rico"))
									lsValor = "'" + lsValor + "'";
							}
							if (lsValor.equals("")) lsValor = "NULL";
							//Valida que el primer campo tenga un valor.
							if(i == 0 && liLong == 0) {
								lbOK = false;
								lsError += "Error en la Linea " + liTotalReg + " en el campo " + laNombre_campo[i] + " del archivo, campo obligatorio. \n";
							}
							//Valida la longitud del campo.
							if(liLong > Integer.parseInt(laLong_campo[i])) {
								lbOK = false;
								lsError += "Error en la Linea " + liTotalReg  + " en el campo " + laNombre_campo[i]  + " del archivo, longitud inv�lida. \n";
							}
							//Valida que sea entero.
							if(laTipo_campo[i].equals("Entero") || laTipo_campo[i].equals("number")) {
								if(Comunes.esNumero(lsValor) == false) {
									lbOK = false;
									lsError += "Error en la linea " + liTotalReg + " en el campo " + laNombre_campo[i] + " del archivo, el dato no es un entero. \n";
								}
							}
							laDato[i] = lsValor;
							System.out.println("Registro leido:"+ lsValor);
						} //for
					} else {
						lbOK = false;
						lsError += "Error en la linea " + liTotalReg + " n�mero de campos inv�lidos. \n";
					}
					lovDatos.removeAllElements();
				} else {
					lbOK = false;
					lsError += "Error en la linea " + liTotalReg + " linea vacia. \n";
				}
				System.out.println("Termino de leer la l�nea con:"+lbOK);
				if(lbOK == true) {
					System.out.println("L�nea Correcta, procede a Insertar el registro");
					lsCadenaSQL = "SELECT NVL(max(ID),0)+1 id from tabla_" + eiCvePublicacion;

					lrsSel = lodbConexion.queryDB(lsCadenaSQL);

					if (lrsSel.next())
						liID = lrsSel.getInt(1);
					lrsSel.close();
					lsCadenaSQL = "insert into tabla_" + eiCvePublicacion + " (id,";
					for (int n = 0; n < liNCampos; n++) {
						lsCadenaSQL += "campo" + n + ",";
					}

					lsCadenaSQL = lsCadenaSQL.substring(0,lsCadenaSQL.length()-1);
					lsCadenaSQL += ") values (" + liID + ",";
					for (int n = 0; n < liNCampos; n++) {
						lsCadenaSQL += laDato[n] + ",";
					}

					lsCadenaSQL = lsCadenaSQL.substring(0,lsCadenaSQL.length()-1);
					lsCadenaSQL += ")";
					try {
						lodbConexion.ejecutaSQL(lsCadenaSQL);
						lodbConexion.terminaTransaccion(true);
						lodbConexion.cierraStatement();
					} catch (Exception error1){
						System.out.println("Error al insertar el registro"+error1.getMessage());
						lodbConexion.terminaTransaccion(false);
						lodbConexion.cierraStatement();
						lsError += "Error al insertar la l�nea" + liTotalReg + ". \n";
						lbError = true;
						liTotalRech++;
					}
				} else {
					lbError = true;
					liTotalRech++;
				}
				System.out.println("Termino el registro:" );
			} //while externo
			lofArchivo.delete();

			lovResultado.addElement(new Integer(liTotalReg));
			lovResultado.addElement(new Integer(liTotalRech));
			lovResultado.addElement(lsError);

			System.out.println("Termino con : Reg="+liTotalReg+" Rech="+liTotalRech+"Errores"+lsError);
			return lovResultado;

		} catch (NafinException error){
			throw error;
		} catch (Exception epError){
			System.out.println("Error Inesperado:"+ epError.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::sinsertarRegistro(S)");
		}
	}

	/*********************************************************************************************
	*
	*	   void vactualizarRegistro()
	*
	*********************************************************************************************/
	public void vactualizarRegistro(int eiCvePublicacion, int eiCveRegistro, String esValores[],
									String esTipoDato[] )
			throws NafinException
	{
		boolean lbOK = true;
		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::vactualizarRegistro(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "UPDATE tabla_" + eiCvePublicacion + " SET";
			for (int i = 0; i < esValores.length; i++) {
				lsCadenaSQL += " campo" + String.valueOf(i) + " = ";
				if (!esValores[i].equals("")) {
					if (esTipoDato[i].equals("number"))
						lsCadenaSQL += " " + esValores[i] + ",";
					 else
						lsCadenaSQL += " '"+ esValores[i] + "',";
				} else {
					lsCadenaSQL += "null,";
				}
			}
			lsCadenaSQL = lsCadenaSQL.substring(0,lsCadenaSQL.length()-1);
			lsCadenaSQL += " WHERE id = "  + eiCveRegistro;

			System.out.println(lsCadenaSQL);
			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			} catch (Exception error){
				throw new NafinException("PARM0004");   ////No se pudo actualizar el registro
			}
		} catch (NafinException error){
			throw error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::vactualizarRegistro(S)");
		}
	}

	/*********************************************************************************************
	*
	*	   void vborrarRegistro()
	*
	*********************************************************************************************/
	public void vborrarRegistro(int eiCvePublicacion, int eiCveRegistro )
			throws NafinException
	{
		boolean lbOK = true;
		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::vborrarRegistro(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "DELETE FROM tabla_"+ eiCvePublicacion +" WHERE id = "+ eiCveRegistro;

			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			} catch (Exception error){
				throw new NafinException("PARM0005");   ////No se pudo borrar el registro
			}

		} catch (NafinException error){
			throw error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::vborrarRegistro(S)");
		}
	}

	/*********************************************************************************************
	*
	*	   void vborrarTabla()
	*
	*********************************************************************************************/
	public void vborrarTabla(String esClaveEpo, int eiCvePublicacion)
			throws NafinException
	{
		boolean lbOK = true;
		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::vborrarTabla(E)");
		try {

			try {
				lodbConexion.conexionDB();
			} catch (Exception error){
				throw new NafinException("SIST0001");
			}
			// Borra la tabla l�gica
			System.out.println("Borrar la tabla l�gica");
			lsCadenaSQL  = "DELETE FROM com_publicacion";
			lsCadenaSQL += " WHERE ic_epo = "+ esClaveEpo +" AND ic_publicacion = "+ eiCvePublicacion;

			System.out.println(lsCadenaSQL);
			lodbConexion.ejecutaSQL(lsCadenaSQL);

			System.out.println("Borra los atributos de la tabla l�gica");
			lsCadenaSQL  = "DELETE FROM com_tabla_pub_atrib";
			lsCadenaSQL += " WHERE ic_publicacion = "+ eiCvePublicacion;

			lodbConexion.ejecutaSQL(lsCadenaSQL);

			// Borra F�sicamente la tabla
			System.out.println("Borra la Tabla F�sica");
			lsCadenaSQL = "drop table tabla_"+ eiCvePublicacion;

			lodbConexion.ejecutaSQL(lsCadenaSQL);
		} catch (NafinException error){
			lbOK = false;
			throw error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("PARM0003");    //No es Posible Borrar Tablas en este momento.
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::vborrarTabla(S)");
		}
	}

	/*********************************************************************************************
	*
	*	   void vinsertarRegistro()
	*
	*********************************************************************************************/
	public void vinsertarRegistro(int eiCvePublicacion, String esValores[], String esTipoDato[] )
			throws NafinException
	{
		boolean lbOK = true;
		String lsCadenaSQL = "";
		int liID  = 0;

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::vinsertarRegistro(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "SELECT NVL(MAX(ID),0) + 1 id FROM tabla_"+ eiCvePublicacion;

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				liID = lrsSel.getInt(1);
			else
				throw new NafinException("PARM0002");	// Error al buscar el n�mero de registro
			lrsSel.close();

			lsCadenaSQL = "INSERT INTO tabla_"+ eiCvePublicacion +" values ("+ liID +",";
			System.out.println("Longitud del arreglo " + esValores.length);
			for (int i = 0; i < esValores.length; i++) {
				System.out.println(i + ": " + esValores[i]);
				if (!esValores[i].equals("")) {
					if (esTipoDato[i].equals("number")) {
						lsCadenaSQL += " "+ esValores[i] +",";
					} else {
						lsCadenaSQL += " '"+ esValores[i] + "',";
					}
				} else
					lsCadenaSQL += "null,";
			}
			lsCadenaSQL = lsCadenaSQL.substring(0,lsCadenaSQL.length()-1);
			lsCadenaSQL += ")";

			System.out.println(lsCadenaSQL);

			lodbConexion.ejecutaSQL(lsCadenaSQL);
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("PARM0002");    //Problemas al insertar el registro
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::vinsertarRegistro(S)");
		}
	}

	/*********************************************************************************************
	*
	*	   void vlimpiarTabla()
	*
	*********************************************************************************************/
	public void vlimpiarTabla(int eiCvePublicacion)
			throws NafinException
	{
		boolean lbOK = true;
		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::vlimpiarTabla(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "truncate table tabla_"+ eiCvePublicacion;

			lodbConexion.ejecutaSQL(lsCadenaSQL);

		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("PARM0008");    //No se pudo limpiar la tabla
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::vlimpiarTabla(S)");
		}
	}

	/*********************************************************************************************
	*
	*	   void vsetDatosTabla()
	*
	*********************************************************************************************/
	public void vsetDatosTabla(int eiCvePublicacion, CUsrPublicacion eoUsrPublicacion)
			throws NafinException
	{
		boolean lbOK = true;
		String lsCadenaSQL  = "";
		String lsNomTabla  = "";
		String lsFechaIni  = "";
		String lsFechaFin  = "";

		int liCveEpo = 0;

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::vsetDatosTabla(E)");
		try {
			lodbConexion.conexionDB();

			liCveEpo   = eoUsrPublicacion.getCveEpo();
			lsNomTabla = eoUsrPublicacion.getNomPublicacion();
			lsFechaIni = eoUsrPublicacion.getFechaIni();
			lsFechaFin = eoUsrPublicacion.getFechaFin();

			lsCadenaSQL = "UPDATE com_publicacion ";
			lsCadenaSQL += " SET cg_nombre = '" + lsNomTabla + "',";
			lsCadenaSQL += " df_ini_pub = TO_DATE('" + lsFechaIni + "','DD/MM/YYYY'),";
			lsCadenaSQL += " df_fin_pub = TO_DATE('" + lsFechaFin + "','DD/MM/YYYY') ";
			lsCadenaSQL += " WHERE ic_epo = " + liCveEpo;
			lsCadenaSQL += " AND ic_publicacion = " + eiCvePublicacion;

			lodbConexion.ejecutaSQL(lsCadenaSQL);
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("PARM0007");    //Problemas al actualizar los Datos de la Tabla
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::vsetDatosTabla(S)");
		}
	}

	/*********************************************************************************************
	*
	*	   void vsetDatosTabla()
	*
	*********************************************************************************************/
	public void vsetDatosTabla(int eiCvePublicacion, String esCveEpo, String esNomTabla,
								String esFechaIni, String esFechaFin )
			throws NafinException
	{
		boolean lbOK = true;
		String lsCadenaSQL  = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::vsetDatosTabla(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "UPDATE com_publicacion ";
			lsCadenaSQL += " SET cg_nombre = '" + esNomTabla + "',";
			lsCadenaSQL += " df_ini_pub = TO_DATE('" + esFechaIni + "','DD/MM/YYYY'),";
			lsCadenaSQL += " df_fin_pub = TO_DATE('" + esFechaFin + "','DD/MM/YYYY') ";
			lsCadenaSQL += " WHERE ic_epo = " + esCveEpo;
			lsCadenaSQL += " AND ic_publicacion = " + eiCvePublicacion;

			lodbConexion.ejecutaSQL(lsCadenaSQL);
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("PARM0007");    //Problemas al actualizar los Datos de la Tabla
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::vsetDatosTabla(S)");
		}
	}


	/*********************************************************************************************
	*
	*	    String sinsertarRegistro()   [ Carga Masiva ]
	*
	*********************************************************************************************/
	public void vinsertarRegistro( int eiCvePublicacion, String esLinea )
			throws NafinException
	{
		boolean lbOK    = true;
		boolean lbError = false;

		String lsCadenaSQL = "";
		String lsLinea     = "";
		String lsValor     = "";
		String lsError     = "";

		int liTotalReg = 0;
		int liNCampos  = 0;
		int liLong     = 0;
		int liTotalRech = 0;
		int liID  = 0;

		String laNombre_campo[] = null;
		String laLong_campo[]   = null;
		String laTipo_campo[]   = null;
		String laDato[]         = null;

		VectorTokenizer lvt = null;
		Vector lovDatos = null;

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::sinsertarRegistro(E)");
		try {
			lodbConexion.conexionDB();

			if (!lsLinea.equals("")) {
				System.out.println("Linea:"+liTotalReg);
				lvt = new VectorTokenizer(lsLinea,"|");
				lovDatos = lvt.getValuesVector();

				if( liNCampos == lovDatos.size()) {
					System.out.println("Lee los campos:"+ liNCampos);
					for (int i = 0; i < lovDatos.size(); i++) {
						try {
							lsValor = (lovDatos.get(i) == null) ? "" : (String)lovDatos.get(i);
						} catch(ArrayIndexOutOfBoundsException aioobe) {
							System.out.println(aioobe);
							throw new NafinException("PARM0009");	//Error al leer el archivo
						}
						lsValor = lsValor.trim();
						liLong = lsValor.length();
						if (liLong >= 1) {
							lsValor = lsValor.replace('\'',' ');
							lsValor = lsValor.replace('\\',' ');
							if (laTipo_campo[i].equals("Alfanum�rico"))
								lsValor = "'" + lsValor + "'";
						}
						if (lsValor.equals("")) lsValor = "NULL";
						//Valida que el primer campo tenga un valor.
						if(i == 0 && liLong == 0) {
							lbOK = false;
							lsError += "Error en la Linea " + liTotalReg + " en el campo " + laNombre_campo[i] + " del archivo, campo obligatorio. \n";
						}
						//Valida la longitud del campo.
						if(liLong > Integer.parseInt(laLong_campo[i])) {
							lbOK = false;
							lsError += "Error en la Linea " + liTotalReg  + " en el campo " + laNombre_campo[i]  + " del archivo, longitud inv�lida. \n";
						}
						//Valida que sea entero.
						if(laTipo_campo[i].equals("Entero") || laTipo_campo[i].equals("number")) {
							if(Comunes.esNumero(lsValor) == false) {
								lbOK = false;
								lsError += "Error en la linea " + liTotalReg + " en el campo " + laNombre_campo[i] + " del archivo, el dato no es un entero. \n";
							}
						}
						laDato[i] = lsValor;
						System.out.println("Registro leido:"+ lsValor);
					} //for
				} else {
					lbOK = false;
					lsError += "Error en la linea " + liTotalReg + " n�mero de campos inv�lidos. \n";
				}
				lovDatos.removeAllElements();
			} else {
				lbOK = false;
				lsError += "Error en la linea " + liTotalReg + " linea vacia. \n";
			}
			System.out.println("Termino de leer la l�nea con:"+lbOK);
			if(lbOK == true) {
				System.out.println("L�nea Correcta, procede a Insertar el registro");
				lsCadenaSQL = "SELECT NVL(max(ID),0)+1 id from tabla_" + eiCvePublicacion;

				lrsSel = lodbConexion.queryDB(lsCadenaSQL);

				if (lrsSel.next())
					liID = lrsSel.getInt(1);
				lrsSel.close();
				lsCadenaSQL = "insert into tabla_" + eiCvePublicacion + " (id,";
				for (int n = 0; n < liNCampos; n++) {
					lsCadenaSQL += "campo" + n + ",";
				}

				lsCadenaSQL = lsCadenaSQL.substring(0,lsCadenaSQL.length()-1);
				lsCadenaSQL += ") values (" + liID + ",";
				for (int n = 0; n < liNCampos; n++) {
					lsCadenaSQL += laDato[n] + ",";
				}

				lsCadenaSQL = lsCadenaSQL.substring(0,lsCadenaSQL.length()-1);
				lsCadenaSQL += ")";
				try {
					lodbConexion.ejecutaSQL(lsCadenaSQL);
					lodbConexion.terminaTransaccion(true);
				} catch (Exception error1){
					System.out.println("Error al insertar el registro"+error1.getMessage());
					lodbConexion.terminaTransaccion(false);
					lsError += "Error al insertar la l�nea" + liTotalReg + ". \n";
					lbError = true;
					liTotalRech++;
				}
			} else {
				lbError = true;
				liTotalRech++;
			}

		} catch (NafinException error){
			throw error;
		} catch (Exception epError){
			System.out.println("Error Inesperado:"+ epError.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::sinsertarRegistro(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    int iObtieneNumeroColumnas()
	*
	*********************************************************************************************/
	private int iObtieneNumeroColumnas( int eiCvePublicacion, AccesoDB eodbConexion)
			throws Exception
	{
		String lsCadenaSQL = "";
		int liNumeroColumnas = 0;
		ResultSet lrsSel = null;

		System.out.println(" TablasDinamicasEJB::iObtieneNumeroColumnas(E/S)");

		lsCadenaSQL  = "SELECT count(*)";
		lsCadenaSQL += " FROM com_tabla_pub_atrib";
		lsCadenaSQL += " WHERE ic_publicacion = " + eiCvePublicacion;

		lrsSel = eodbConexion.queryDB(lsCadenaSQL);

		if(lrsSel.next())
			liNumeroColumnas = lrsSel.getInt(1);

		return liNumeroColumnas;
	}

	/*********************************************************************************************
	*
	*	   void vvalidarTabla()
	*
	*********************************************************************************************/
	public boolean bvalidarTabla(int eiCveEPO, String esNomTabla )
			throws NafinException
	{
		int liValor = 0;
		String lsCadenaSQL  = "";
		ResultSet lrsSel = null;

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" TablasDinamicasEJB::bvalidarTabla(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "SELECT count(*) as n_tabla";
			lsCadenaSQL += " FROM com_publicacion";
			lsCadenaSQL += " WHERE ic_epo = "+ eiCveEPO;
			lsCadenaSQL += " AND cg_nombre = '" + esNomTabla + "'";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			System.out.println(lsCadenaSQL);

			if(lrsSel.next())
				liValor = lrsSel.getInt(1);

			return (liValor == 0) ? true: false;

		} catch (Exception epError){
			System.out.println("Error:" + epError);
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.cierraConexionDB();
			System.out.println(" TablasDinamicasEJB::bvalidarTabla(S)");
		}
	}


	public void guardaRespuestaPymeExt(String[] msQuerys, String[] msNumIds, String iNoCliente, String lsNumPublicacion)
		throws NafinException	{

		boolean lbNoError = true;
		AccesoDB lobjConexion = new AccesoDB();

		try	{
			try 					{	lobjConexion.conexionDB();	}
			catch (Exception error)	{	lbNoError = false;  throw new NafinException("SIST0001");	}

			for (int i=0; i<msQuerys.length; i++  )	{
				if ( msQuerys[i].equals("") )	continue;   // ********* NO SE GRABA

				String lsQrTrunc = msQuerys[i].substring( 0, msQuerys[i].length()-1 ), /// ***** KITANDO LA COMA KE SOBRA
					   lsQry = "INSERT INTO tabla_edit_" + lsNumPublicacion +
							   " VALUES ("+msNumIds[i]+ ", " + iNoCliente + ", " + lsQrTrunc + ")";

				System.out.println("lsQry :: "+lsQry);
				lobjConexion.ejecutaSQL( lsQry );
			}
		}
		catch (NafinException Error)	{
			lbNoError = false;
			throw Error;
		}
		catch(Exception err)	{
			System.out.print("ERROR: " + err.getMessage() );
			lbNoError = false;
			throw new NafinException("SIST0001");
		}
		finally	{
			if ( lobjConexion != null)	{
				lobjConexion.terminaTransaccion(lbNoError);
				lobjConexion.cierraConexionDB();
			}
		}
	}
	
}// Fin de la Clase