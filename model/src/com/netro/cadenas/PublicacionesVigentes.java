package com.netro.cadenas;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.CreaArchivo;
//import netropology.utilerias.AppException;

/**
 * Esta clase se encarga de generar los archivos csv de los reportes de publicaciones vigentes para proveedores
 * habilitados y no habilitados, para posteriormente comprimirlos en un archivo ZIP.
 * @author Alberto Cruz Flores
 */
public class PublicacionesVigentes {
	/**
	 * Constructor de la clase.
	 */
	public PublicacionesVigentes() {}
	
	/**
	 * M�todo que genera el archivo CSV de las publicaciones vigentes a proveedores habilitados.
	 * @param ruta
	 * 
	 */
	public String generaArchivoPublicacionesVigentes(String rutaApp) throws Exception {
		System.out.println("generaArchivoPublicacionesVigentes(E) ::..");
		CreaArchivo archivo = new CreaArchivo();
		File file = null;
		String nombreArchivoZip = "";
		String strDirectorioTemp = rutaApp + "/00tmp/15cadenas/";
		try {
			nombreArchivoZip = "PublicacionesVigentes_" + new SimpleDateFormat("ddMMyyyy").format(Calendar.getInstance().getTime()) + ".zip";
			
			StringBuffer publicacionesVigentesPH = this.obtenerPublicacionesVigentesProvHabilitados();
			archivo.make(publicacionesVigentesPH.toString(), strDirectorioTemp, "ProveedoresHabilitados", ".csv");
			
			StringBuffer publicacionesVigentesPNH = this.obtenerPublicacionesVigentesProvNoHabilitados();
			archivo.make(publicacionesVigentesPNH.toString(), strDirectorioTemp, "ProveedoresNoHabilitados", ".csv");
			
			BufferedInputStream bufferedInputStream = null;
			FileInputStream fileInputStream = null;
			ZipEntry zipEntry = null;
			int count;
			
			FileOutputStream fileOutputStream = new FileOutputStream(strDirectorioTemp + nombreArchivoZip);			
			ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(fileOutputStream));			
			byte[] data = new byte[8192];
			
			fileInputStream = new FileInputStream(strDirectorioTemp + "ProveedoresHabilitados.csv");
			bufferedInputStream = new BufferedInputStream(fileInputStream, 8192);			
			zipEntry = new ZipEntry("ProveedoresHabilitados.csv");
			zipOutputStream.putNextEntry(zipEntry);
			while ((count = bufferedInputStream.read(data, 0, 8192)) != -1) {
				zipOutputStream.write(data, 0, count);
			}
			bufferedInputStream.close();
			
			fileInputStream = new FileInputStream(strDirectorioTemp + "ProveedoresNoHabilitados.csv");
			bufferedInputStream = new BufferedInputStream(fileInputStream, 8192);			
			zipEntry = new ZipEntry("ProveedoresNoHabilitados.csv");
			zipOutputStream.putNextEntry(zipEntry);			
			while ((count = bufferedInputStream.read(data, 0, 8192)) != -1) {
				zipOutputStream.write(data, 0, count);
			}
			bufferedInputStream.close();
			
			zipOutputStream.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("generaArchivoPublicacionesVigentes(ERROR) ::..", e);
		} finally {
			System.out.println("generaArchivoPublicacionesVigentes(S) ::..");
		}
		return nombreArchivoZip;
	}

	/**
	 * M�todo que obtiene las publicaciones vigentes para proveedores habilitados.
	 * @author Alberto Cruz Flores.
	 * @return publicacionesVigentesPH StringBuffer con el resultado de la consulta.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	private StringBuffer obtenerPublicacionesVigentesProvHabilitados() throws Exception {
		System.out.println("obtenerPublicacionesVigentesProvHabilitados(E) ::..");
		AccesoDBOracle con = new AccesoDBOracle();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		StringBuffer publicacionesVigentesPH = new StringBuffer();
    
        try {
            con.conexionDB();
		
            strSQL.append(" SELECT /*+index(doc IN_COM_DOCUMENTO_04_NUK) use_nl(doc crn con cpe rpe pym epo mon sec cpn)*/");
            strSQL.append(" epo.ic_epo AS clave_epo,");
            strSQL.append(" epo.cg_razon_social AS nombre_epo,");
            strSQL.append(" crn.ic_nafin_electronico AS nafin_electronico,");
            strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
            strSQL.append(" pym.cg_appat_rep_legal AS ap_rep_legal_pyme,");
            strSQL.append(" pym.cg_apmat_legal AS am_rep_legal_pyme,");
            strSQL.append(" pym.cg_nombrerep_legal AS nombre_rep_legal_pyme,");
            strSQL.append(" pym.cg_rfc AS rfc_pyme,");
            strSQL.append(" sec.cd_nombre AS sector,");
            strSQL.append(" con.cg_tel AS numero_telefono_pyme,");
            strSQL.append(" con.cg_celular AS numero_celular_pyme,");
            strSQL.append(" con.cg_email AS email_pyme,");
            strSQL.append(" doc.ig_numero_docto AS numero_documento,");
            strSQL.append(" doc.fn_monto AS monto_documento,");
            strSQL.append(" mon.cd_nombre AS moneda,");
            strSQL.append(" TO_CHAR(doc.df_fecha_venc, 'dd/mm/yyyy') AS fecha_vencimiento,");
            strSQL.append(" TO_CHAR(doc.df_alta, 'dd/mm/yyyy') AS fecha_alta,");
            
            strSQL.append(" con.cg_appat AS ap_contacto_pyme,");
            strSQL.append(" con.cg_apmat AS am_contacto_pyme,");
            strSQL.append(" con.cg_nombre as nombre_contacto_pyme,");
            strSQL.append(" con.cg_tel as num_tel_contacto,");
            strSQL.append(" con.cg_celular as num_cel_contacto,");
            strSQL.append(" con.cg_email as correo_contacto");
            
            strSQL.append(" FROM com_documento doc");
            strSQL.append(", comrel_nafin crn");
            strSQL.append(", com_contacto con");
            strSQL.append(", comrel_pyme_epo cpe");
            strSQL.append(", comrel_producto_epo rpe");
            strSQL.append(", comcat_pyme pym");
            strSQL.append(", comcat_epo epo");
            strSQL.append(", comcat_moneda mon");
            strSQL.append(", comcat_sector_econ sec");
            strSQL.append(", comcat_producto_nafin cpn");
            strSQL.append(" WHERE doc.ic_pyme = crn.ic_epo_pyme_if");
            strSQL.append(" AND doc.ic_epo = cpe.ic_epo");
            strSQL.append(" AND doc.ic_pyme = cpe.ic_pyme");
            strSQL.append(" AND cpe.cs_aceptacion = ?");
            strSQL.append(" AND con.ic_pyme = pym.ic_pyme");
            strSQL.append(" AND con.cs_primer_contacto = ?");
            strSQL.append(" AND crn.cg_tipo = ?");
            strSQL.append(" AND doc.ic_pyme = pym.ic_pyme");            
            strSQL.append(" AND doc.ic_epo             = rpe.ic_epo");
            strSQL.append(" AND rpe.ic_producto_nafin  = cpn.ic_producto_nafin");
            strSQL.append(" AND cpn.ic_producto_nafin  = ?");
            strSQL.append(" AND doc.ic_epo = epo.ic_epo");
            strSQL.append(" AND doc.ic_moneda = mon.ic_moneda");
            strSQL.append(" AND pym.ic_sector_econ = sec.ic_sector_econ");
            strSQL.append(" AND doc.df_fecha_venc >= TRUNC(SYSDATE + NVL (rpe.ig_dias_minimo, cpn.in_dias_minimo))");
            strSQL.append(" AND doc.ic_estatus_docto = ?");
            
            varBind.add("H");
            varBind.add("S");
            varBind.add("P");
            varBind.add(new Integer(1));
            varBind.add(new Integer(2));
            
            System.out.println("..:: strSQL : "+strSQL.toString());
            System.out.println("..:: varBind : "+varBind.toString());
      
            pst = con.queryPrecompilado(strSQL.toString(), varBind);
            rst = pst.executeQuery();
            
            publicacionesVigentesPH.append("Clave de la EPO,");
            publicacionesVigentesPH.append("Raz�n Social de la EPO,");
            publicacionesVigentesPH.append("Nafin Electr�nico,");
            publicacionesVigentesPH.append("Raz�n Social de la PyME,");
            publicacionesVigentesPH.append("Apellido Paterno del Representante Legal,");
            publicacionesVigentesPH.append("Apellido Materno del Representante Legal,");
            publicacionesVigentesPH.append("Nombre del Representante Legal,");
            publicacionesVigentesPH.append("RFC de la PyME,");
            publicacionesVigentesPH.append("Sector al que pertenece la PyME,");
            publicacionesVigentesPH.append("N�mero Telef�nico,");
            publicacionesVigentesPH.append("N�mero Celular,");
            publicacionesVigentesPH.append("Correo Electr�nico,");
            publicacionesVigentesPH.append("N�mero de Documento Publicado,");
            publicacionesVigentesPH.append("Monto del Documento Publicado,");
            publicacionesVigentesPH.append("Tipo de Moneda,");
            publicacionesVigentesPH.append("Fecha de Vencimiento,");
            publicacionesVigentesPH.append("Fecha de Alta,");
           
            publicacionesVigentesPH.append("Apellido Paterno del Contacto,");
            publicacionesVigentesPH.append("Apellido Materno del Contacto,");            
            publicacionesVigentesPH.append("Nombre del Contacto,");
            publicacionesVigentesPH.append("Tel�fono del Contacto,");
            publicacionesVigentesPH.append("Celular del Contacto,");
            publicacionesVigentesPH.append("Correo Electr�nico del Contacto \n");
            

            while (rst.next()){
                publicacionesVigentesPH.append((rst.getString("clave_epo")==null?"":rst.getString("clave_epo")) + ",");
                publicacionesVigentesPH.append((rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo")).replaceAll(",", " ") + ",");
                publicacionesVigentesPH.append((rst.getString("nafin_electronico")==null?"":rst.getString("nafin_electronico")) + ",");
                publicacionesVigentesPH.append((rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme")).replaceAll(",", " ") + ",");
                publicacionesVigentesPH.append((rst.getString("ap_rep_legal_pyme")==null?"":rst.getString("ap_rep_legal_pyme")) + ",");
                publicacionesVigentesPH.append((rst.getString("am_rep_legal_pyme")==null?"":rst.getString("am_rep_legal_pyme")) + ",");
                publicacionesVigentesPH.append((rst.getString("nombre_rep_legal_pyme")==null?"":rst.getString("nombre_rep_legal_pyme")) + ",");
                publicacionesVigentesPH.append((rst.getString("rfc_pyme")==null?"":rst.getString("rfc_pyme")).replaceAll(",", "") + ",");
                publicacionesVigentesPH.append((rst.getString("sector")==null?"":rst.getString("sector")).replaceAll(",", "") + ",");
                publicacionesVigentesPH.append((rst.getString("numero_telefono_pyme")==null?"":rst.getString("numero_telefono_pyme")).replaceAll(",", " ") + ",");
                publicacionesVigentesPH.append((rst.getString("numero_celular_pyme")==null?"":rst.getString("numero_celular_pyme")).replaceAll(",", " ") + ",");
                publicacionesVigentesPH.append((rst.getString("email_pyme")==null?"":rst.getString("email_pyme")).replaceAll(",", " ") + ",");
                publicacionesVigentesPH.append((rst.getString("numero_documento")==null?"":rst.getString("numero_documento")).replaceAll(",", "") + ",");
                publicacionesVigentesPH.append((rst.getString("monto_documento")==null?"":rst.getString("monto_documento")).replaceAll(",", "") + ",");
                publicacionesVigentesPH.append((rst.getString("moneda")==null?"":rst.getString("moneda")) + ",");
                publicacionesVigentesPH.append((rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento")) + ",");
                publicacionesVigentesPH.append((rst.getString("fecha_alta")==null?"":rst.getString("fecha_alta")) + ",");
                
                publicacionesVigentesPH.append((rst.getString("ap_contacto_pyme")==null?"":rst.getString("ap_contacto_pyme")).replaceAll(",", " ")+",");
                publicacionesVigentesPH.append((rst.getString("am_contacto_pyme")==null?"":rst.getString("am_contacto_pyme")).replaceAll(",", " ")+",");
                publicacionesVigentesPH.append((rst.getString("nombre_contacto_pyme")==null?"":rst.getString("nombre_contacto_pyme")).replaceAll(",", " ")+",");
                publicacionesVigentesPH.append((rst.getString("num_tel_contacto")==null?"":rst.getString("num_tel_contacto"))+",");
                publicacionesVigentesPH.append((rst.getString("num_cel_contacto")==null?"":rst.getString("num_cel_contacto"))+",");
                publicacionesVigentesPH.append((rst.getString("correo_contacto")==null?"":rst.getString("correo_contacto")).replaceAll(",", " ")+",");
                
                publicacionesVigentesPH.append("\n");
            }
            rst.close();
            pst.close();
        } catch(Exception e) {
            e.printStackTrace();
            throw new Exception("obtenerPublicacionesVigentesProvHabilitados(ERROR) ::..", e);
        } finally {
            if(con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            System.out.println("obtenerPublicacionesVigentesProvHabilitados(S) ::..");
        }
        return publicacionesVigentesPH;
    }

	/**
	 * M�todo que obtiene las publicaciones vigentes para proveedores habilitados.
	 * @author Alberto Cruz Flores.
	 * @return publicacionesVigentesPNH StringBuffer con el resultado de la consulta.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */		
  private StringBuffer obtenerPublicacionesVigentesProvNoHabilitados() throws Exception {
		System.out.println("obtenerPublicacionesVigentesProvNoHabilitados(E) ::..");
    AccesoDBOracle con = new AccesoDBOracle();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    StringBuffer publicacionesVigentesPNH = new StringBuffer();
    
    try {
      con.conexionDB();
      
      strSQL.append(" SELECT /*+index(doc in_com_documento_09_nuk) use_nl(doc crn con cpe pym epo mon sec)*/");
			strSQL.append(" epo.ic_epo AS clave_epo,");
			strSQL.append(" epo.cg_razon_social AS nombre_epo,");
			strSQL.append(" crn.ic_nafin_electronico AS nafin_electronico,");
			strSQL.append(" pym.cg_razon_social AS nombre_pyme,");
			strSQL.append(" pym.cg_appat_rep_legal AS ap_rep_legal_pyme,");
			strSQL.append(" pym.cg_apmat_legal AS am_rep_legal_pyme,");
			strSQL.append(" pym.cg_nombrerep_legal AS nombre_rep_legal_pyme,");
			strSQL.append(" pym.cg_rfc AS rfc_pyme,");
			strSQL.append(" sec.cd_nombre AS sector,");
			strSQL.append(" con.cg_tel AS numero_telefono_pyme,");
			strSQL.append(" con.cg_celular AS numero_celular_pyme,");
			strSQL.append(" con.cg_email AS email_pyme,");
			strSQL.append(" doc.ig_numero_docto AS numero_documento,");
			strSQL.append(" doc.fn_monto AS monto_documento,");
			strSQL.append(" mon.cd_nombre AS moneda,");
			strSQL.append(" TO_CHAR(doc.df_fecha_venc, 'dd/mm/yyyy') AS fecha_vencimiento,");
			strSQL.append(" TO_CHAR(doc.df_alta, 'dd/mm/yyyy') AS fecha_alta, " +
					" cpe.cs_aceptacion AS estatus_pyme,");                       
                        
                        strSQL.append(" con.cg_appat AS ap_contacto_pyme,");
                        strSQL.append(" con.cg_apmat AS am_contacto_pyme,");
                        strSQL.append(" con.cg_nombre as nombre_contacto_pyme,");
                        strSQL.append(" con.cg_tel as num_tel_contacto,");
                        strSQL.append(" con.cg_celular as num_cel_contacto,");
                        strSQL.append(" con.cg_email as correo_contacto");        
			
                        strSQL.append(" FROM com_documento doc");
			strSQL.append(", comrel_nafin crn");
			strSQL.append(", com_contacto con");
			strSQL.append(", comrel_pyme_epo cpe");
			strSQL.append(", comcat_pyme pym");
			strSQL.append(", comcat_epo epo");
			strSQL.append(", comcat_moneda mon");
			strSQL.append(", comcat_sector_econ sec");
			strSQL.append(" WHERE doc.ic_pyme = crn.ic_epo_pyme_if");
			strSQL.append(" AND doc.ic_epo = cpe.ic_epo");
			strSQL.append(" AND doc.ic_pyme = cpe.ic_pyme");
			strSQL.append(" AND cpe.cs_aceptacion <> ?");
			strSQL.append(" AND con.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND con.cs_primer_contacto = ?");
			strSQL.append(" AND crn.cg_tipo = ?");
			strSQL.append(" AND doc.ic_pyme = pym.ic_pyme");
			strSQL.append(" AND doc.ic_epo = epo.ic_epo");
			strSQL.append(" AND doc.ic_moneda = mon.ic_moneda");
			strSQL.append(" AND pym.ic_sector_econ = sec.ic_sector_econ");
			strSQL.append(" AND doc.df_fecha_venc >= TRUNC(SYSDATE + 8)");
			strSQL.append(" AND doc.ic_estatus_docto = ?");

      varBind.add("H");
			varBind.add("S");
      varBind.add("P");
      varBind.add(new Integer(2));
      
      System.out.println("..:: strSQL : "+strSQL.toString());
      System.out.println("..:: varBind : "+varBind.toString());
      
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();
      
      int indice = 0;

			publicacionesVigentesPNH.append("Clave de la EPO,");
			publicacionesVigentesPNH.append("Raz�n Social de la EPO,");
			publicacionesVigentesPNH.append("Nafin Electr�nico,");
			publicacionesVigentesPNH.append("Raz�n Social de la PyME,");
			publicacionesVigentesPNH.append("Apellido Paterno del Representante Legal,");
			publicacionesVigentesPNH.append("Apellido Materno del Representante Legal,");
			publicacionesVigentesPNH.append("Nombre del Representante Legal,");
			publicacionesVigentesPNH.append("RFC de la PyME,");
			publicacionesVigentesPNH.append("Sector al que pertenece la PyME,");
			publicacionesVigentesPNH.append("N�mero Telef�nico,");
			publicacionesVigentesPNH.append("N�mero Celular,");
			publicacionesVigentesPNH.append("Correo Electr�nico,");
			publicacionesVigentesPNH.append("N�mero de Documento Publicado,");
			publicacionesVigentesPNH.append("Monto del Documento Publicado,");
			publicacionesVigentesPNH.append("Tipo de Moneda,");
			publicacionesVigentesPNH.append("Fecha de Vencimiento,");
			publicacionesVigentesPNH.append("Fecha de Alta,");
                        publicacionesVigentesPNH.append("Estatus Pyme,");
        
                        publicacionesVigentesPNH.append("Apellido Paterno del Contacto,");
                        publicacionesVigentesPNH.append("Apellido Materno del Contacto,");
                        publicacionesVigentesPNH.append("Nombre del Contacto,");                        
                        publicacionesVigentesPNH.append("Tel�fono del Contacto,");
                        publicacionesVigentesPNH.append("Celular del Contacto,");
                        publicacionesVigentesPNH.append("Correo Electr�nico del Contacto \n");


      while (rst.next()){
        publicacionesVigentesPNH.append((rst.getString("clave_epo")==null?"":rst.getString("clave_epo")).replaceAll(",", "") + ",");
        publicacionesVigentesPNH.append((rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo")).replaceAll(",", "") + ",");
        publicacionesVigentesPNH.append((rst.getString("nafin_electronico")==null?"":rst.getString("nafin_electronico")).replaceAll(",", "") + ",");
        publicacionesVigentesPNH.append((rst.getString("nombre_pyme")==null?"":rst.getString("nombre_pyme")).replaceAll(",", " ") + ",");
        publicacionesVigentesPNH.append((rst.getString("ap_rep_legal_pyme")==null?"":rst.getString("ap_rep_legal_pyme")).replaceAll(",", "") + ",");
        publicacionesVigentesPNH.append((rst.getString("am_rep_legal_pyme")==null?"":rst.getString("am_rep_legal_pyme")).replaceAll(",", "") + ",");
        publicacionesVigentesPNH.append((rst.getString("nombre_rep_legal_pyme")==null?"":rst.getString("nombre_rep_legal_pyme")).replaceAll(",", "") + ",");
        publicacionesVigentesPNH.append((rst.getString("rfc_pyme")==null?"":rst.getString("rfc_pyme")).replaceAll(",", "") + ",");
        publicacionesVigentesPNH.append((rst.getString("sector")==null?"":rst.getString("sector")).replaceAll(",", "") + ",");
        publicacionesVigentesPNH.append((rst.getString("numero_telefono_pyme")==null?"":rst.getString("numero_telefono_pyme")).replaceAll(",", " ") + ",");
        publicacionesVigentesPNH.append((rst.getString("numero_celular_pyme")==null?"":rst.getString("numero_celular_pyme")).replaceAll(",", " ") + ",");
        publicacionesVigentesPNH.append((rst.getString("email_pyme")==null?"":rst.getString("email_pyme")).replaceAll(",", " ") + ",");
        publicacionesVigentesPNH.append((rst.getString("numero_documento")==null?"":rst.getString("numero_documento")).replaceAll(",", " ") + ",");
				publicacionesVigentesPNH.append((rst.getString("monto_documento")==null?"":rst.getString("monto_documento")).replaceAll(",", " ") + ",");
        publicacionesVigentesPNH.append((rst.getString("moneda")==null?"":rst.getString("moneda")).replaceAll(",", " ") + ",");
        publicacionesVigentesPNH.append((rst.getString("fecha_vencimiento")==null?"":rst.getString("fecha_vencimiento")) + ",");
        publicacionesVigentesPNH.append((rst.getString("fecha_alta")==null?"":rst.getString("fecha_alta")) + ",");
		  publicacionesVigentesPNH.append((rst.getString("estatus_pyme")==null?"":rst.getString("estatus_pyme")) + ",");
          
          publicacionesVigentesPNH.append((rst.getString("ap_contacto_pyme")==null?"":rst.getString("ap_contacto_pyme")).replaceAll(",", " ")+",");
          publicacionesVigentesPNH.append((rst.getString("am_contacto_pyme")==null?"":rst.getString("am_contacto_pyme")).replaceAll(",", " ")+",");
          publicacionesVigentesPNH.append((rst.getString("nombre_contacto_pyme")==null?"":rst.getString("nombre_contacto_pyme")).replaceAll(",", " ")+",");
          publicacionesVigentesPNH.append((rst.getString("num_tel_contacto")==null?"":rst.getString("num_tel_contacto"))+",");
          publicacionesVigentesPNH.append((rst.getString("num_cel_contacto")==null?"":rst.getString("num_cel_contacto"))+",");
          publicacionesVigentesPNH.append((rst.getString("correo_contacto")==null?"":rst.getString("correo_contacto")).replaceAll(",", " ")+",");
          
          
          
				publicacionesVigentesPNH.append("\n");
      }

      rst.close();
      pst.close();
    }catch(Exception e){
      e.printStackTrace();
			throw new Exception("obtenerPublicacionesVigentesProvNoHabilitados(ERROR) ::..", e);
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
      System.out.println("obtenerPublicacionesVigentesProvNoHabilitados(S) ::..");
    }
    return publicacionesVigentesPNH;
	}

	/**
	 * M�todo que obtiene los correos electronicos de los destinatarios de la notificacion de 
	 * publicaciones vigentes a proveedores habilitados y no habilitados.
	 * @return correosDestinatarios Cadena con los correos de los destinatarios.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	public String obtenerDestinatariosPubVigentes() throws Exception {
		System.out.println("obtenerDestinatariosPubVigentes(E)");
		AccesoDBOracle con = new AccesoDBOracle();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String correosDestinatarios = "";
		String claveCorreosDestinatarios = "";
		try {
			con.conexionDB();

			strSQL.append(" SELECT MAX(ic_email_pub_vig) AS ic_email_pub_vig FROM com_email_pub_vig");
			System.out.println("..:: strSQL: "+strSQL.toString());
			pst = con.queryPrecompilado(strSQL.toString());
			rst = pst.executeQuery();
			if (rst.next()) {
				claveCorreosDestinatarios = rst.getString("ic_email_pub_vig")==null?"0":rst.getString("ic_email_pub_vig");
			}
			
			strSQL = new StringBuffer();
			strSQL.append(" SELECT cg_email_pub_vig AS correos_destinatarios FROM com_email_pub_vig WHERE ic_email_pub_vig = ?");
			varBind.add(new Integer(claveCorreosDestinatarios));
			System.out.println("..:: strSQL: "+strSQL.toString());
			System.out.println("..:: varBind: "+varBind);
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			if (rst.next()) {
				correosDestinatarios = rst.getString("correos_destinatarios")==null?"":rst.getString("correos_destinatarios");
			}

			correosDestinatarios = correosDestinatarios.replace('|', ',');

			rst.close();
			pst.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("obtenerDestinatariosPubVigentes(ERROR) ::..", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("obtenerDestinatariosPubVigentes(S)");
		}
		return correosDestinatarios;
	}
	
	public String getFechaConNombreDelMes(String fecha) {
		if (fecha == null) return fecha;
		int numMes = 0;
		
		String []datos=fecha.split("\\/");
		if(datos.length != 3) return fecha;
		
		String dia 	= datos[0];
		String mes 	= datos[1];
		String anio = datos[2];
		
		try {
			numMes = Integer.parseInt(mes);	
		} catch(Exception e) {
			return fecha;
		}
		
		switch(numMes){
			case 1:  mes = "enero"; break;
			case 2:  mes = "febrero"; break;
			case 3:  mes = "marzo"; break;
			case 4:  mes = "abril"; break;
			case 5:  mes = "mayo"; break;
			case 6:  mes = "junio"; break;
			case 7:  mes = "julio"; break;
			case 8:  mes = "agosto"; break;
			case 9:  mes = "septiembre"; break;
			case 10: mes = "octubre"; break;
			case 11: mes = "noviembre"; break;
			case 12: mes = "diciembre"; break;
			default: return fecha;
		}
		return dia + " de " + mes + " de " + anio; 
	}
}