package com.netro.cadenas;

import com.netro.exception.NafinException;
import com.netro.xls.ComunesXLS;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Clase para obtener informaci�n de Inteligencia Comercial.
 *
 * @author Ricardo Fuentes
 * @author Alberto Cruz
 *
 * @version 1.0
 *
 * @since: 19/05/2006
 *
 */
@Stateless(name = "InteligenciaComercialEJB" , mappedName = "InteligenciaComercialEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class InteligenciaComercialBean implements InteligenciaComercial {
	public InteligenciaComercialBean()  {   }

	//Variable para enviar mensajes al log.
	private static final Log LOG = ServiceLocator.getInstance().getLog(InteligenciaComercialBean.class);
	
	@Override
	public Map consultaParamEnvio(String action
		,String mailAgrega
		,String icEnvios[]
		,String origenes[]
		,String correos[]
		,String mensuales[]
		,String diarios[]
		,int icProductoNafin)
		throws NafinException{
		AccesoDB	con			= new AccesoDB();
		Map			mRetorno	= new HashMap();
		int			i= 0;
		try{
			PreparedStatement	ps				= null;
			ResultSet			rs				= null;
			String				qrySentencia	= null;
			List				lCorreos		= new ArrayList();
			con.conexionDB();

			qrySentencia  =
				" select cs_envio_mensual_clientes"   +
				" 	   ,cs_envio_diario_clientes"   +
				" from comcat_producto_nafin"   +
				" where ic_producto_nafin = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,icProductoNafin);
			rs = ps.executeQuery();
			if(rs.next()){
				mRetorno.put("cs_envio_mensual_clientes",(rs.getString("cs_envio_mensual_clientes")==null?"N":rs.getString("cs_envio_mensual_clientes")));
				mRetorno.put("cs_envio_diario_clientes",(rs.getString("cs_envio_diario_clientes")==null?"N":rs.getString("cs_envio_diario_clientes")));
			}
			rs.close();
			ps.close();

			if(correos!=null){
				for(i=0;i<correos.length;i++){
					if(!"nodespliega".equals(origenes[i])){
						Map mColumnas = new HashMap();
						mColumnas.put("ic_envio_clientes",icEnvios[i]);
						mColumnas.put("cg_email",correos[i] );
						mColumnas.put("cs_envio_diario",diarios[i]);
						mColumnas.put("cs_envio_mensual",mensuales[i]);
						mColumnas.put("origen",origenes[i]);
						lCorreos.add(mColumnas);
					}
				}
			}else{
				qrySentencia =
					" SELECT ic_envio_clientes"   +
					"  , cg_email"   +
					"  , cs_envio_diario"   +
					"  , cs_envio_mensual"   +
					"  , ic_producto_nafin"   +
					" FROM com_envio_clientes "+
					" WHERE ic_producto_nafin = ?";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,icProductoNafin);
				rs = ps.executeQuery();
				while(rs.next()){
					Map mColumnas = new HashMap();
					mColumnas.put("ic_envio_clientes",rs.getString("ic_envio_clientes"));
					mColumnas.put("cg_email",rs.getString("cg_email"));
					mColumnas.put("cs_envio_diario",rs.getString("cs_envio_diario"));
					mColumnas.put("cs_envio_mensual",rs.getString("cs_envio_mensual"));
					mColumnas.put("origen","tabla");
					lCorreos.add(mColumnas);
				}
				rs.close();
				ps.close();
			}
			if("A".equals(action)){		//agregar nuevo correo a los hiddens
				Map mColumnas = new HashMap();
				mColumnas.put("ic_envio_clientes","");
				mColumnas.put("cg_email",mailAgrega );
				mColumnas.put("cs_envio_diario","N");
				mColumnas.put("cs_envio_mensual","N");
				mColumnas.put("origen","agregar");
				lCorreos.add(mColumnas);
			}
			mRetorno.put("correos",lCorreos);
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return mRetorno;
	}

	@Override
	public void guardarParamEnvio(
		String envMensActivar
		,String envDiarioActivar
		,String icEnvios[]
		,String origenes[]
		,String correos[]
		,String mensuales[]
		,String diarios[]
		,int icProductoNafin) throws NafinException{
		AccesoDB	con		= new AccesoDB();
		int			i= 0;
		boolean		ok		= true;
		try{
			PreparedStatement	ps				= null;
			PreparedStatement	ps2				= null;
			PreparedStatement	ps3				= null;
			String				qrySentencia	= null;

			con.conexionDB();
			qrySentencia =
				" update comcat_producto_nafin"+
				" set cs_envio_mensual_clientes = ?"+
				" ,cs_envio_diario_clientes = ?"+
				" where ic_producto_nafin = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,envMensActivar);
			ps.setString(2,envDiarioActivar);
			ps.setInt(3,icProductoNafin);
			ps.execute();
			ps.close();

			if(icEnvios!=null&&origenes!=null){
				qrySentencia =
					" update com_envio_clientes"+
					" set cs_envio_diario = ?"+
					" ,cs_envio_mensual = ?"+
					" where ic_envio_clientes = ?";
				ps = con.queryPrecompilado(qrySentencia);

				qrySentencia =
					" insert into com_envio_clientes"+
					" (ic_envio_clientes,cg_email,cs_envio_diario,cs_envio_mensual,ic_producto_nafin)"+
					" select nvl(max(ic_envio_clientes),0)+1,?,?,?,?"   +
					" from com_envio_clientes"  ;
				ps2 = con.queryPrecompilado(qrySentencia);

				qrySentencia =
					" delete com_envio_clientes"+
					" where ic_envio_clientes = ?";
				ps3 = con.queryPrecompilado(qrySentencia);

				for(i=0;i<icEnvios.length;i++){
					if("tabla".equals(origenes[i])){
						ps.setString(1,diarios[i]);
						ps.setString(2,mensuales[i]);
						ps.setString(3,icEnvios[i]);
						ps.execute();
					}
					if("agregar".equals(origenes[i])){
						ps2.setString(1,correos[i]);
						ps2.setString(2,diarios[i]);
						ps2.setString(3,mensuales[i]);
						ps2.setInt(4,icProductoNafin);
						ps2.execute();
					}
					if("eliminar".equals(origenes[i])){
						ps3.setString(1,icEnvios[i]);
						ps3.execute();
					}
				} // for
				ps.close();
				ps2.close();
				ps3.close();
			}

		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		}
	}

	@Override
	public Map procesarArchivo(List lDatos,int anio,String mes) throws NafinException {

		LOG.info("procesarArchivo(E)");

		AccesoDB          con          = new AccesoDB();
		ResultSet         rs           = null;
		PreparedStatement ps           = null;
		String            qrySentencia = "";

		Map     mRetorno    = new HashMap();
		int     liLineadocs = 1;
		int     icProceso   = 0;
		int     contador    = 0; 
		String  mesExiste   = "N";
		boolean transaccion = true;

		try {

			con.conexionDB();

			List   lErrores       = new ArrayList();
			List   lCorrectos     = new ArrayList();
			Map    Error          = new HashMap();
			Map    Correcto       = null;
			String icEpo          = null;
			String icPyme         = null;
			String montoCredito   = null;
			String montoConsolida = null;
			String ejecutivo      = null;
			String telefono       = null;
			String correo         = null;

			if (lDatos.size() == 0){
				Error = new HashMap();
				Error.put("linea","000");
				Error.put("error","El archivo contiene m�s de una hoja de c&aacute;lculo</br> o el contenido es incorrecto.");
				Error.put("solucion","Inserte un archivo con una sola hoja de</br> c&aacute;lculo y verifique el layout.</a>");
				lErrores.add(Error);
			} else {
				List lovDatos = (ArrayList) lDatos.get(0);
				if (lovDatos.size() < 7){
					Error = new HashMap();
					Error.put("linea","000");
					Error.put("error","No coincide con el layout requerido.");
					Error.put("solucion","Verifique el n�mero de datos.");
					lErrores.add(Error);
				}
			}

			qrySentencia = " SELECT SEQ_COMTMP_MENSAJE_PYME.nextval from dual ";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				icProceso = rs.getInt(1);
			}
			rs.close();
			ps.close();

			qrySentencia =
				" select count(1) "+
				" from com_mensaje_pyme "+
				" where to_char(df_publicacion,'mm/yyyy') = ? ";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,mes+"/"+anio);
			rs = ps.executeQuery();
			if(rs.next()){
				if(rs.getInt(1)>0){
					mesExiste = "S";
				}
			}
			rs.close();
			ps.close();

			qrySentencia =
				"insert into comtmp_mensaje_pyme"+
				" (IC_PROC_MENSAJE,IC_EPO,IC_PYME,FN_MONTO_CREDITO,FG_MONTO_CONSOLIDADO"+
				" ,CG_EJECUTIVO_PROM,CG_TELEFONO,CG_CORREO,CS_ESTATUS_ENVIO"+
				" ,CG_ESTATUS_RESPUESTA,CS_MENSAJE,DF_RESPUESTA,DF_PUBLICACION)"+
				" values(?,?,?,?,?,?,?,?,'A','N','S',null,to_date(?,'mm/yyyy'))";
			ps = con.queryPrecompilado(qrySentencia);

			for(int i = 0; i < lDatos.size(); i++){
				List lovDatos = (List) lDatos.get(i);
				// Valida el n�mero de datos de la l�nea
				if(lovDatos.size() < 7) {
					Error = new HashMap();
					Error.put("linea",""+liLineadocs);
					Error.put("error","No coincide con el layout requerido.");
					Error.put("solucion","Verifique el n&uacute;mero de datos.");
					lErrores.add(Error);
				} else {
					boolean ok = true;
					icEpo          = (lovDatos.size()>=1)?Comunes.quitaComitasSimples(lovDatos.get(0).toString().trim()).trim():"";
					icPyme         = (lovDatos.size()>=2)?Comunes.quitaComitasSimples(lovDatos.get(1).toString().trim()).trim():"";
					montoCredito   = (lovDatos.size()>=3)?Comunes.quitaComitasSimples(lovDatos.get(2).toString().trim()).trim():"";
					montoConsolida = (lovDatos.size()>=4)?Comunes.quitaComitasSimples(lovDatos.get(3).toString().trim()).trim():"";
					ejecutivo      = (lovDatos.size()>=5)?Comunes.quitaComitasSimples(lovDatos.get(4).toString().trim()).trim():"";
					telefono       = (lovDatos.size()>=6)?Comunes.quitaComitasSimples(lovDatos.get(5).toString().trim()).trim():"";
					correo         = (lovDatos.size()>=7)?Comunes.quitaComitasSimples(lovDatos.get(6).toString().trim()).trim():"";

					icEpo    = icEpo.replaceAll(".0", "");
					icPyme   = icPyme.replaceAll(".0", "");
					telefono = telefono.replaceAll(".0", "");

					if(icEpo!=null&&icEpo.length()>3){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","La longitud de la clave de la EPO </br> excede el maximo permitido");
						Error.put("solucion","Verifique que no sea mayor a tres digitos.");
						lErrores.add(Error);
						ok = false;
					}else{
						try{
							Integer.parseInt(icEpo);
						}catch(Exception e){
							Error = new HashMap();
							Error.put("linea",""+liLineadocs);
							Error.put("error","El valor la clave de la EPO es incorrecto");
							Error.put("solucion","Verifique que este capturado y que sea un valor num&eacute;rico.");
							lErrores.add(Error);
							ok = false;
						}
					}

					if(icPyme!=null&&icPyme.length()>8){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","La longitud de la clave de la Pyme </br> excede el maximo permitido");
						Error.put("solucion","Verifique que no sea mayor a ocho digitos.");
						lErrores.add(Error);
						ok = false;
					}else{
						try{
							Integer.parseInt(icPyme);
						}catch(Exception e){
							Error = new HashMap();
							Error.put("linea",""+liLineadocs);
							Error.put("error","El valor de la clave de la Pyme es incorrecto");
							Error.put("solucion","Verifique que este capturado y que sea un valor num&eacute;rico.");
							lErrores.add(Error);
							ok = false;
						}
					}


					if(montoCredito!=null&&montoCredito.length()>14){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","La longitud del Monto Credito de ventas </br> excede el maximo permitido");
						Error.put("solucion","Verifique que no sea mayor a 14 caracteres.");
						lErrores.add(Error);
						ok = false;
					}else{
						try{
							Double.parseDouble(montoCredito);
						}catch(Exception e){
							Error = new HashMap();
							Error.put("linea",""+liLineadocs);
							Error.put("error","El valor del Monto Credito de ventas es incorrecto");
							Error.put("solucion","Verifique que este capturado y que sea </br> un valor num&eacute;rico.");
							lErrores.add(Error);
							ok = false;
						}
					}

					if(montoConsolida!=null&&montoConsolida.length()>14){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","La longitud del Monto Consolidado excede el maximo permitido");
						Error.put("solucion","Verifique que no sea mayor a 14 caracteres.");
						lErrores.add(Error);
						ok = false;
					}else{
						try{
							Double.parseDouble(montoConsolida);
						}catch(Exception e){
							Error = new HashMap();
							Error.put("linea",""+liLineadocs);
							Error.put("error","El valor del Monto Consolidado es incorrecto");
							Error.put("solucion","Verifique que este capturado y  </br>que sea un valor num&eacute;rico.");
							lErrores.add(Error);
							ok = false;
						}
					}

					if(ejecutivo!=null&&ejecutivo.length()>100){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","La longitud del ejecutivo excede </br> el maximo permitido");
						Error.put("solucion","Verifique que no sea mayor a cien caracteres.");
						lErrores.add(Error);
						ok = false;
					}

					if(telefono!=null&&telefono.length()>30){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","La longitud del telefono excede </br> el maximo permitido");
						Error.put("solucion","Verifique que no sea mayor a treinta caracteres.");
						lErrores.add(Error);
						ok = false;
					}

					if(correo!=null&&correo.length()>30){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","La longitud del correo excede </br> el maximo permitido");
						Error.put("solucion","Verifique que no sea mayor a treinta caracteres.");
						lErrores.add(Error);
						ok = false;
					}


					if (correo.indexOf("@")==-1||correo.indexOf(".")==-1){
						Error = new HashMap();
						Error.put("linea",""+liLineadocs);
						Error.put("error","El correo es incorrecto");
						Error.put("solucion","Verifique que contenga el caracter '@' </br> y que </br>la terminacion sea la adecuada. Ejemplo:nombre@empresa.com.");
						lErrores.add(Error);
						ok = false;
					}

					if(ok){
						ps.setInt(1,icProceso);
						ps.setString(2,icEpo);
						ps.setString(3,icPyme);
						ps.setDouble(4,Double.parseDouble(montoCredito));
						ps.setDouble(5,Double.parseDouble(montoConsolida));
						ps.setString(6,ejecutivo);
						ps.setString(7,telefono);
						ps.setString(8,correo);
						ps.setString(9,mes+"/"+anio);
						ps.execute();

						Correcto = new HashMap();
						Correcto.put("linea",""+liLineadocs);
						Correcto.put("numPyme",icPyme);
						lCorrectos.add(Correcto);
					}

				}
				liLineadocs++;
			}
			ps.close();
			mRetorno.put("errores",lErrores);
			mRetorno.put("correctos",lCorrectos);
			mRetorno.put("proceso",""+icProceso);
			mRetorno.put("existeMes",mesExiste);
		}catch(Exception e){
			transaccion = false;
			LOG.error("procesarArchivo(Exception)", e);
			throw new NafinException("PARM0009");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
			LOG.info(" procesarArchivo(S)");
		}
		return mRetorno;
	}

	@Override
	public List getMensajesCargadosTmp(String icProceso)
	throws NafinException	{
		LOG.info(" getMensajesCargadosTmp(E)");

		AccesoDB con = new AccesoDB();
		ResultSet			rs		= null;
		PreparedStatement	ps		= null;
		String		qrySentencia	= "";
		List		lRetorno 		= new ArrayList();
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT IC_EPO"   +
				" , IC_PYME"   +
				" , FN_MONTO_CREDITO"   +
				" , FG_MONTO_CONSOLIDADO"   +
				" , CG_EJECUTIVO_PROM"   +
				" , CG_TELEFONO"   +
				" , CG_CORREO"   +
				" FROM COMTMP_MENSAJE_PYME"   +
				" WHERE IC_PROC_MENSAJE = ?"+
				" AND (IC_EPO,IC_PYME,TO_CHAR(DF_PUBLICACION,'mm/yyyy'))"+
				" NOT IN(select ic_epo,ic_pyme,TO_CHAR(DF_PUBLICACION,'mm/yyyy')"+
				" 	     from com_mensaje_pyme where cg_estatus_respuesta = 'S') ";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,icProceso);
			rs = ps.executeQuery();
			while(rs.next()){
				Map mColumnas = new HashMap();
				mColumnas.put("IC_EPO",rs.getString("IC_EPO"));
				mColumnas.put("IC_PYME",rs.getString("IC_PYME"));
				mColumnas.put("MONTO_CREDITO",Comunes.formatoDecimal(rs.getDouble("FN_MONTO_CREDITO"),2));
				mColumnas.put("MONTO_CONSOLIDADO",Comunes.formatoDecimal(rs.getDouble("FG_MONTO_CONSOLIDADO"),2));
				mColumnas.put("EJECUTIVO",rs.getString("CG_EJECUTIVO_PROM"));
				mColumnas.put("TELEFONO",rs.getString("CG_TELEFONO"));
				mColumnas.put("CORREO",rs.getString("CG_CORREO"));
				lRetorno.add(mColumnas);
			}
			rs.close();
			ps.close();


		}catch(Exception e){
			LOG.error(" getMensajesCargadosTmp(Exception)", e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			LOG.info(" getMensajesCargadosTmp(S)");
		}
		return lRetorno;
	}

	@Override
	public List procesarMensajesCargados(String icProceso,String anio,String mes)
	throws NafinException	{
		LOG.info(" getMensajesCargadosTmp(E)");

		AccesoDB con = new AccesoDB();
		ResultSet			rs		= null;
		PreparedStatement	ps		= null;
		String		qrySentencia	= "";
		List		lRetorno 		= new ArrayList();
		boolean		ok 				= true;
		try {
			con.conexionDB();

			qrySentencia =
				" delete com_mensaje_pyme "+
				" where (ic_epo,ic_pyme) "+
				" in(select ic_epo,ic_pyme from comtmp_mensaje_pyme where ic_proc_mensaje = ?)"+
				" and cg_estatus_respuesta in('N','R')"+
				" and to_char(df_publicacion,'mm/yyyy') = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,icProceso);
			ps.setString(2,mes+"/"+anio);
			ps.execute();
			ps.close();

			qrySentencia =
				" insert into com_mensaje_pyme"   +
				"  (IC_MENSAJE_PYME, IC_EPO, IC_PYME, FN_MONTO_CREDITO, FG_MONTO_CONSOLIDADO"   +
				"  ,CG_EJECUTIVO_PROM, CG_TELEFONO, CG_CORREO, CS_ESTATUS_ENVIO, CG_ESTATUS_RESPUESTA"   +
				"  , CS_MENSAJE, DF_RESPUESTA, DF_PUBLICACION)"   +
				"  select seq_com_mensaje_pyme.nextval,ic_epo,ic_pyme,fn_monto_credito,fg_monto_consolidado"   +
				"  ,cg_ejecutivo_prom,cg_telefono,cg_correo,cs_estatus_envio,cg_estatus_respuesta,cs_mensaje,df_respuesta,to_date(?,'mm/yyyy')"   +
				"  from COMTMP_MENSAJE_PYME"   +
				"  where (ic_epo,ic_pyme,to_char(df_publicacion,'mm/yyyy'))"   +
				"  not in(select ic_epo,ic_pyme,to_char(df_publicacion,'mm/yyyy') from com_mensaje_pyme where to_char(df_publicacion,'mm/yyyy') = ?)"   +
				"  and ic_proc_mensaje = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,mes+"/"+anio);
			ps.setString(2,mes+"/"+anio);
			ps.setString(3,icProceso);
			ps.execute();


			qrySentencia =
				" SELECT IC_EPO"   +
				" , IC_PYME"   +
				" , FN_MONTO_CREDITO"   +
				" , FG_MONTO_CONSOLIDADO"   +
				" , CG_EJECUTIVO_PROM"   +
				" , CG_TELEFONO"   +
				" , CG_CORREO"   +
				" FROM COM_MENSAJE_PYME"   +
				" WHERE (ic_epo,ic_pyme,df_publicacion) in(select ic_epo,ic_pyme,df_publicacion from comtmp_mensaje_pyme where ic_proc_mensaje = ?)"+
				" and cg_estatus_respuesta = 'N'";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,icProceso);
			rs = ps.executeQuery();
			while(rs.next()){
				Map mColumnas = new HashMap();
				mColumnas.put("IC_EPO",rs.getString("IC_EPO"));
				mColumnas.put("IC_PYME",rs.getString("IC_PYME"));
				mColumnas.put("MONTO_CREDITO",Comunes.formatoDecimal(rs.getDouble("FN_MONTO_CREDITO"),2));
				mColumnas.put("MONTO_CONSOLIDADO",Comunes.formatoDecimal(rs.getDouble("FG_MONTO_CONSOLIDADO"),2));
				mColumnas.put("EJECUTIVO",rs.getString("CG_EJECUTIVO_PROM"));
				mColumnas.put("TELEFONO",rs.getString("CG_TELEFONO"));
				mColumnas.put("CORREO",rs.getString("CG_CORREO"));
				lRetorno.add(mColumnas);
			}
			rs.close();
			ps.close();
		}catch(Exception e){
			ok =false;
			lRetorno.clear();
			LOG.error(" getMensajesCargadosTmp(Exception)", e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			LOG.info(" getMensajesCargadosTmp(S)");
		}
		return lRetorno;
	}

	@Override
	public List consultaEposMensajes()
	throws NafinException	{
		LOG.info(" consultaEposMensajes(E)");

		AccesoDB con = new AccesoDB();
		ResultSet			rs		= null;
		PreparedStatement	ps		= null;
		String		qrySentencia	= "";
		List		lRetorno 		= new ArrayList();
		try {
			con.conexionDB();
			qrySentencia =
				"  select distinct e.ic_epo"   +
				"  	   ,e.cg_razon_social"   +
				"  from comcat_epo e"   +
				"  ,com_mensaje_pyme mp"   +
				"  where e.ic_epo = mp.ic_epo"   +
				"  order by cg_razon_social"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			while(rs.next()){
				Map mColumnas = new HashMap();
				mColumnas.put("IC_EPO",rs.getString("IC_EPO"));
				mColumnas.put("NOMBRE",rs.getString("CG_RAZON_SOCIAL"));
				lRetorno.add(mColumnas);
			}
			rs.close();
			ps.close();

		}catch(Exception e){
			LOG.error(" consultaEposMensajes(Exception)", e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			LOG.info(" consultaEposMensajes(S)");
		}
		return lRetorno;
	}

	@Override
	public List consultaEstadisticas(String anio,String mes,String estatus,String icEpo)
	throws NafinException	{
		return 	consultaEstadisticas(anio,mes,estatus,icEpo,null);
	}

	@Override
	public List consultaEstadisticas(String anio,String mes,String estatus,String icEpos[])
	throws NafinException	{
		return 	consultaEstadisticas(anio,mes,estatus,null,icEpos);
	}

	private List consultaEstadisticas(String anio,String mes,String estatus,String icEpo,String icEpos[])
	throws NafinException	{
		LOG.info(" consultaEstadisticos(E)");

		AccesoDB con = new AccesoDB();
		ResultSet			rs		= null;
		PreparedStatement	ps		= null;
		StringBuffer		qrySentencia	= new StringBuffer("");;
		List		lRetorno 		= new ArrayList();
		int			numParam		= 0;

		double		totPymes	= 0;
		double		totPymesMsg = 0;
		double		totPymesSi	= 0;
		double		totPymesNo	= 0;
		double		totSinResp	= 0;
		double		totSinMsg	= 0;
		try {
			con.conexionDB();
			qrySentencia.append(
				"   select mp.ic_epo"   +
				"   ,e.cg_razon_social as nombre_epo"   +
				"   ,vn.num_pymes"   +
				"   ,count(1) as num_pymes_msg"   +
				"   ,sum(case when cg_estatus_respuesta = 'S' then 1 else 0 end) as num_resp_si"   +
				"   ,sum(case when cg_estatus_respuesta = 'R' then 1 else 0 end) as num_resp_no"   +
				"   ,sum(case when cg_estatus_respuesta = 'N' then 1 else 0 end) as sin_responder"   +
				"   ,sum(case when cg_estatus_respuesta = 'S' then fn_monto_credito else 0 end) as monto_resp_si"   +
				"   ,sum(case when cg_estatus_respuesta = 'R' then fn_monto_credito else 0 end) as monto_resp_no"   +
				"   ,sum(case when cg_estatus_respuesta = 'N' then fn_monto_credito else 0 end) as monto_sin_responder"   +
				"   ,(vn.num_pymes - count(1)) as pymes_sin_msg"   +
				"   ,case when sum(case when to_char(df_publicacion,'mm/yyyy') = to_char(sysdate,'mm/yyyy') then 1 else 0 end)=0 then 'No Aplica' "   +
				"    when sum(case when cs_estatus_envio = 'A' then 1 else 0 end)>0 then 'Activo' "   +
				"    else 'Suspendido' end  as estatus_envio"   +
				"   from com_mensaje_pyme mp"   +
				"   ,comcat_epo e"   +
				"   ,(select ic_epo, count(1) as num_pymes"   +
				"     from comrel_pyme_epo pe"   +
				"     group by ic_epo) vn"   +
				"   where mp.ic_epo = e.ic_epo"   +
				"   and mp.ic_epo = vn.ic_epo"
			);

			if(anio!=null&&!anio.equals("")){
				if(mes==null||mes.equals("")){
					qrySentencia.append(" and df_publicacion between to_date('01/01/'||?,'dd/mm/yyyy') and to_date('31/12/'||?,'dd/mm/yyyy')");
				}else{
					qrySentencia.append(" and to_char(df_publicacion,'mm/yyyy') = ?||'/'||? ");
				}
			}
			if(estatus!=null&&!estatus.equals("")){
				if(!estatus.equals("NA"))
					qrySentencia.append(" and cs_estatus_envio = ?");
				else
					qrySentencia.append(" and to_char(df_publicacion,'mm/yyyy')!=to_char(sysdate,'mm/yyyy')");

			}
			if(icEpo!=null&&!icEpo.equals("")){
				qrySentencia.append(" and mp.ic_epo = ?");
			}

			if(icEpos!=null&&icEpos.length!=0){
				qrySentencia.append(" and mp.ic_epo in(");
				for(int j=0;j<icEpos.length;j++){
					if(j>0){
						qrySentencia.append(",");
					}
					qrySentencia.append("?");
				}
				qrySentencia.append(")");
			}

			qrySentencia.append(
				"  group by mp.ic_epo"   +
				"  ,e.cg_razon_social"   +
				"  ,vn.num_pymes") ;
			ps = con.queryPrecompilado(qrySentencia.toString());
			if(anio!=null&&!anio.equals("")){
				if(mes==null||mes.equals("")){
					ps.setString(++numParam,anio);
					ps.setString(++numParam,anio);
				}else{
					ps.setString(++numParam,mes);
					ps.setString(++numParam,anio);
				}
			}

			if(estatus!=null&&!estatus.equals("")){
				if(!estatus.equals("NA"))
					ps.setString(++numParam,estatus);
			}
			if(icEpo!=null&&!icEpo.equals("")){
				ps.setString(++numParam,icEpo);
			}

			if(icEpos!=null&&icEpos.length!=0){
				for(int j=0;j<icEpos.length;j++){
					ps.setString(++numParam,icEpos[j]);
				}
			}

			rs = ps.executeQuery();
			while(rs.next()){
				Map mColumnas = new HashMap();
				mColumnas.put("IC_EPO",rs.getString("IC_EPO"));
				mColumnas.put("NOMBRE_EPO",rs.getString("NOMBRE_EPO"));
				mColumnas.put("NUM_PYMES",Comunes.formatoDecimal(rs.getString("NUM_PYMES"),0));
				mColumnas.put("NUM_PYMES_MSG",Comunes.formatoDecimal(rs.getString("NUM_PYMES_MSG"),0));
				mColumnas.put("NUM_RESP_SI",Comunes.formatoDecimal(rs.getString("NUM_RESP_SI"),0));
				mColumnas.put("NUM_RESP_NO",Comunes.formatoDecimal(rs.getString("NUM_RESP_NO"),0));
				mColumnas.put("SIN_RESPONDER",Comunes.formatoDecimal(rs.getString("SIN_RESPONDER"),0));
				mColumnas.put("PYMES_SIN_MSG",Comunes.formatoDecimal(rs.getString("PYMES_SIN_MSG"),0));
				mColumnas.put("ESTATUS_ENVIO",rs.getString("ESTATUS_ENVIO"));
				mColumnas.put("MONTO_RESP_SI",Comunes.formatoDecimal(rs.getString("MONTO_RESP_SI"),2));
				mColumnas.put("MONTO_RESP_NO",Comunes.formatoDecimal(rs.getString("MONTO_RESP_NO"),2));
				mColumnas.put("MONTO_SIN_RESPONDER",Comunes.formatoDecimal(rs.getString("MONTO_SIN_RESPONDER"),2));

				totPymes	+= rs.getDouble("NUM_PYMES");
				totPymesMsg += rs.getDouble("NUM_PYMES_MSG");
				totPymesSi	+= rs.getDouble("NUM_RESP_SI");
				totPymesNo	+= rs.getDouble("NUM_RESP_NO");
				totSinResp	+= rs.getDouble("SIN_RESPONDER");
				totSinMsg	+= rs.getDouble("PYMES_SIN_MSG");

				mColumnas.put("TOT_PYMES",Comunes.formatoDecimal(totPymes,0));
				mColumnas.put("TOT_PYMES_MSG",Comunes.formatoDecimal(totPymesMsg,0));
				mColumnas.put("TOT_PYMES_SI",Comunes.formatoDecimal(totPymesSi,0));
				mColumnas.put("TOT_PYMES_NO",Comunes.formatoDecimal(totPymesNo,0));
				mColumnas.put("TOT_SIN_RESP",Comunes.formatoDecimal(totSinResp,0));
				mColumnas.put("TOT_SIN_MSG",Comunes.formatoDecimal(totSinMsg,0));
				lRetorno.add(mColumnas);
			}
			rs.close();
			ps.close();


		}catch(Exception e){
			LOG.error(" consultaEstadisticos(Exception)", e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			LOG.info(" consultaEstadisticos(S)");
		}
		return lRetorno;
	}

	@Override
	public List consultaDetallePorEpo(String icEpo,String respuesta,String anio,String mes)
	throws NafinException	{
		return consultaDetallePorEpo(icEpo,null,respuesta,anio,mes);
	}

	@Override
	public List consultaDetallePorEpo(String icEpos[],String respuesta,String anio,String mes)
	throws NafinException	{
		return consultaDetallePorEpo(null,icEpos,respuesta,anio,mes);
	}

	private List consultaDetallePorEpo(String icEpo,String icEpos[],String respuesta,String anio,String mes)
	throws NafinException	{
		LOG.info(" consultaDetallePorEpo(E)");

		AccesoDB con = new AccesoDB();
		ResultSet			rs		= null;
		PreparedStatement	ps		= null;
		StringBuffer		qrySentencia	= new StringBuffer("");
		List		lRetorno 		= new ArrayList();
		int			numParam		= 1;
		try {
			con.conexionDB();
			qrySentencia.append(
				" select mp.ic_pyme"   +
				"  ,p.in_numero_sirac as NUMERO_SIRAC"   +
				"  ,p.cg_razon_social AS NOMBRE_PYME"   +
				"  ,case when pep.cs_habilitado in('S','H') then 'YA' else 'NO' end as credCad"   +
				"  ,es.cd_nombre AS ESTADO"   +
				"  ,dom.cn_cp"   +
				"  ,dom.cg_telefono1"   +
				"  ,mp.fn_monto_credito"   +
				"  ,mp.fg_monto_consolidado"   +
				"  ,case when cg_estatus_respuesta = 'S' then 'SI' when cg_estatus_respuesta = 'R' then 'NO' when cg_estatus_respuesta = 'N' then 'No contestada' end as respuesta"   +
				"  ,to_char(df_respuesta,'dd/mm/yyyy') as fecha_respuesta"   +
				"  ,e.cg_razon_social AS NOMBRE_EPO"   +
				" from com_mensaje_pyme mp "   +
				"  ,comcat_pyme p"   +
				"  ,comcat_epo e"+
				"  ,comrel_pyme_epo_x_producto pep"   +
				"  ,com_domicilio dom"   +
				"  ,comcat_estado es"   +
				" where mp.ic_pyme = p.ic_pyme"   +
				" and mp.ic_epo = pep.ic_epo(+)"   +
				" and mp.ic_pyme = pep.ic_pyme(+)"   +
				" and mp.ic_epo = e.ic_epo"+
				" and p.ic_pyme = dom.ic_pyme"   +
				" and dom.ic_estado = es.ic_estado"+
				" and pep.ic_producto_nafin(+) = 5"   +
				" and to_char(df_publicacion,'mm/yyyy') = ?");
			if(icEpo!=null&&!icEpo.equals("")){
				qrySentencia.append(" and mp.ic_epo = ?");
			}
			if(respuesta!=null&&!respuesta.equals("")){
				qrySentencia.append(" and mp.cg_estatus_respuesta = ?");
			}

			if(icEpos!=null&&icEpos.length!=0){
				qrySentencia.append(" and mp.ic_epo in(");
				for(int j=0;j<icEpos.length;j++){
					if(j>0){
						qrySentencia.append(",");
					}
					qrySentencia.append("?");
				}
				qrySentencia.append(")");
			}
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setString(1,mes+"/"+anio);

			if(icEpo!=null&&!icEpo.equals("")){
				ps.setString(++numParam,icEpo);
			}
			if(respuesta!=null&&!respuesta.equals("")){
				ps.setString(++numParam,respuesta);
			}

			if(icEpos!=null&&icEpos.length!=0){
				for(int j=0;j<icEpos.length;j++){
					ps.setString(++numParam,icEpos[j]);
				}
			}

			rs = ps.executeQuery();
			while(rs.next()){
				Map mColumnas = new HashMap();
				mColumnas.put("IC_PYME",rs.getString("IC_PYME"));
				mColumnas.put("NUMERO_SIRAC",(rs.getString("NUMERO_SIRAC")==null)?" ":rs.getString("NUMERO_SIRAC"));
				mColumnas.put("NOMBRE_PYME",rs.getString("NOMBRE_PYME"));
				mColumnas.put("CREDCAD",rs.getString("CREDCAD"));
				mColumnas.put("ESTADO",rs.getString("ESTADO"));
				mColumnas.put("CP",rs.getString("CN_CP"));
				mColumnas.put("TELEFONO",rs.getString("CG_TELEFONO1"));
				mColumnas.put("MONTO_CREDITO",rs.getString("FN_MONTO_CREDITO"));
				mColumnas.put("MONTO_CONSOLIDADO",rs.getString("FG_MONTO_CONSOLIDADO"));
				mColumnas.put("RESPUESTA",rs.getString("RESPUESTA"));
				mColumnas.put("FECHA_RESPUESTA",(rs.getString("FECHA_RESPUESTA")==null)?" ":rs.getString("FECHA_RESPUESTA"));
				mColumnas.put("NOMBRE_EPO",rs.getString("NOMBRE_EPO"));
				lRetorno.add(mColumnas);
			}
			rs.close();
			ps.close();

		}catch(Exception e){
			LOG.error(" consultaDetallePorEpo(Exception)", e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			LOG.info(" consultaDetallePorEpo(S)");
		}
		return lRetorno;
	}

	@Override
	public void cambiarEstatusEnvio(String icEpos[],String accion)
		throws NafinException{
		boolean	 ok = true;
		AccesoDB con = new AccesoDB();
		try{
			PreparedStatement ps = null;
			String qrySentencia = null;
			con.conexionDB();

			if(accion.equals("E")){
				qrySentencia =
					" update com_mensaje_pyme"+
					" set cs_estatus_envio = 'A'"+
					" ,cg_estatus_respuesta = 'N'"+
					" where ic_epo = ?";
			}
			if(accion.equals("D")){
				qrySentencia =
					" update com_mensaje_pyme"+
					" set cs_estatus_envio = 'N'"+
					" where ic_epo = ?";
			}
			if(icEpos!=null){
				ps = con.queryPrecompilado(qrySentencia);
				for(int i=0;i<icEpos.length;i++){
					ps.setString(1,icEpos[i]);
					ps.execute();
				}
				ps.close();
			}

		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
		}
	}

	@Override
	public List consultaResumenEst(String anio,String mes,String estatus,String icEpo)
	throws NafinException	{
		return 	consultaResumenEst(anio,mes,estatus,icEpo,null);
	}

	@Override
	public List consultaResumenEst(String anio,String mes,String estatus,String icEpos[])
	throws NafinException	{
		return 	consultaResumenEst(anio,mes,estatus,null,icEpos);
	}

	private List consultaResumenEst(String anio,String mes,String estatus,String icEpo,String icEpos[])
	throws NafinException	{
		LOG.info(" consultaEstadisticos(E)");

		AccesoDB con = new AccesoDB();
		ResultSet			rs		= null;
		PreparedStatement	ps		= null;
		StringBuffer		qrySentencia	= new StringBuffer("");;
		List		lRetorno 		= new ArrayList();
		int			numParam		= 0;
		try {
			con.conexionDB();
			qrySentencia.append(
				" select sum(vn.num_pymes) as num_pymes"   +
				" ,count(1) as num_pymes_msg"   +
				" ,sum(case when cg_estatus_respuesta = 'S' then 1 else 0 end) as num_resp_si"   +
				" ,sum(case when cg_estatus_respuesta = 'R' then 1 else 0 end) as num_resp_no"   +
				" ,sum(case when cg_estatus_respuesta = 'N' then 1 else 0 end) as sin_responder"   +
				" ,sum(case when cg_estatus_respuesta = 'S' then fn_monto_credito else 0 end) as monto_resp_si"   +
				" ,sum(case when cg_estatus_respuesta = 'R' then fn_monto_credito else 0 end) as monto_resp_no"   +
				" ,sum(case when cg_estatus_respuesta = 'N' then fn_monto_credito else 0 end) as monto_sin_responder"   +
				" ,(sum(vn.num_pymes) - count(1)) as pymes_sin_msg"   +
				" from com_mensaje_pyme mp"   +
				" ,comcat_epo e"   +
				" ,(select ic_epo, count(1) as num_pymes "   +
				"   from comrel_pyme_epo pe"   +
				"   group by ic_epo) vn"   +
				" where mp.ic_epo = e.ic_epo"   +
				" and mp.ic_epo = vn.ic_epo");

			if(anio!=null&&!anio.equals("")){
				if(mes==null||mes.equals("")){
					qrySentencia.append(" and df_publicacion between to_date('01/01/'||?,'dd/mm/yyyy') and to_date('31/12/'||?,'dd/mm/yyyy')");
				}else{
					qrySentencia.append(" and to_char(df_publicacion,'mm/yyyy') = ?||'/'||? ");
				}
			}
			if(estatus!=null&&!estatus.equals("")){
				if(!estatus.equals("NA"))
					qrySentencia.append(" and cs_estatus_envio = ?");
				else
					qrySentencia.append(" and to_char(df_publicacion,'mm/yyyy')!=to_char(sysdate,'mm/yyyy')");

			}
			if(icEpo!=null&&!icEpo.equals("")){
				qrySentencia.append(" and mp.ic_epo = ?");
			}

			if(icEpos!=null&&icEpos.length!=0){
				qrySentencia.append(" and mp.ic_epo in(");
				for(int j=0;j<icEpos.length;j++){
					if(j>0){
						qrySentencia.append(",");
					}
					qrySentencia.append("?");
				}
				qrySentencia.append(")");
			}

			ps = con.queryPrecompilado(qrySentencia.toString());
			if(anio!=null&&!anio.equals("")){
				if(mes==null||mes.equals("")){
					ps.setString(++numParam,anio);
					ps.setString(++numParam,anio);
				}else{
					ps.setString(++numParam,mes);
					ps.setString(++numParam,anio);
				}
			}

			if(estatus!=null&&!estatus.equals("")){
				if(!estatus.equals("NA"))
					ps.setString(++numParam,estatus);
			}
			if(icEpo!=null&&!icEpo.equals("")){
				ps.setString(++numParam,icEpo);
			}

			if(icEpos!=null&&icEpos.length!=0){
				for(int j=0;j<icEpos.length;j++){
					ps.setString(++numParam,icEpos[j]);
				}
			}

			rs = ps.executeQuery();
			while(rs.next()){
				Map mColumnas = new HashMap();
				mColumnas.put("NUM_PYMES",Comunes.formatoDecimal(rs.getString("NUM_PYMES"),0));
				mColumnas.put("NUM_PYMES_MSG",Comunes.formatoDecimal(rs.getString("NUM_PYMES_MSG"),0));
				mColumnas.put("NUM_RESP_SI",Comunes.formatoDecimal(rs.getString("NUM_RESP_SI"),0));
				mColumnas.put("NUM_RESP_NO",Comunes.formatoDecimal(rs.getString("NUM_RESP_NO"),0));
				mColumnas.put("SIN_RESPONDER",Comunes.formatoDecimal(rs.getString("SIN_RESPONDER"),0));
				mColumnas.put("PYMES_SIN_MSG",Comunes.formatoDecimal(rs.getString("PYMES_SIN_MSG"),0));
				mColumnas.put("MONTO_RESP_SI",Comunes.formatoDecimal(rs.getString("MONTO_RESP_SI"),2));
				mColumnas.put("MONTO_RESP_NO",Comunes.formatoDecimal(rs.getString("MONTO_RESP_NO"),2));
				mColumnas.put("MONTO_SIN_RESPONDER",Comunes.formatoDecimal(rs.getString("MONTO_SIN_RESPONDER"),2));
				lRetorno.add(mColumnas);
			}
			rs.close();
			ps.close();

		}catch(Exception e){
			LOG.error(" consultaEstadisticos(Exception)", e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			LOG.info(" consultaEstadisticos(S)");
		}
		return lRetorno;
	}

	/**
	 * Este m�todo genera el reporte de los clientes que aceptan la promoci�n a trav�s de
	 * un ejecutivo en el sistema Nafin Electr�nico.
	 * @param rutaArchivo Directorio donde se colocar� el archivo generado del reporte.
	 * 	Recordar que este archivo generado solo se crear� en el servidor donde se ejecute
	 *    el proceso.
	 * @param rutaVirtual Ruta virtual en donde se puede accesar al archivo via web
	 * @return Cadena con el detalle de la ejecucion:
	 * 	PARAMETRO NO ACTIVO. El parametro no esta activo. Se interrumpe la ejecuci�n
	 */
	@Override
	public String consultaClientesPotenciales(String rutaArchivo, String rutaVirtual) throws AppException {
		LOG.info("consultaClientesPotenciales(E) ::..");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement	ps = null;
		StringBuffer qrySentencia	= new StringBuffer("");
		StringBuffer resumenEjecucion = new StringBuffer();

		int i = 0;

		//Esto es para los criterios de la consulta.
		String sMes = new SimpleDateFormat("MM").format(new java.util.Date());
		String sNoAnio = new SimpleDateFormat("yyyy").format(new java.util.Date());
		String nombreArchivo = "CredCad_"+sMes+"_"+sNoAnio;
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;

		SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
		//SimpleDateFormat sdfAnio = new SimpleDateFormat("yyyy");
		//SimpleDateFormat sdfMes = new SimpleDateFormat("MM");
		Calendar gcFechaFinal = new GregorianCalendar(Integer.parseInt(sNoAnio), Integer.parseInt(sMes), 1);
		gcFechaFinal.add(Calendar.MONTH, -1);
		String sFechaFin = sdfFecha.format(gcFechaFinal.getTime());
		//String sAnioFin = sdfAnio.format(gcFechaFinal.getTime());
		//String sMesFin = sdfMes.format(gcFechaFinal.getTime());
		Calendar gcFechaInicial = new GregorianCalendar(Integer.parseInt(sNoAnio)-1, Integer.parseInt(sMes) - 1, 1);
		gcFechaInicial.add(Calendar.MONTH, -1);
		String sFechaIni = sdfFecha.format(gcFechaInicial.getTime());
		//String sAnioIni = sdfAnio.format(gcFechaInicial.getTime());
		//String sMesIni = sdfMes.format(gcFechaInicial.getTime());
		
		List	lCorreos  = new ArrayList();
		String servidor   = "";
		try {
			String activo = null;
			con.conexionDB();

			qrySentencia = new StringBuffer(" SELECT cg_ip_smtp FROM com_param_gral");
			LOG.debug("..:: qrySentencia: "+qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				servidor = rs.getString(1);
			}
			rs.close();
			ps.close();

			qrySentencia = new StringBuffer();
			qrySentencia.append(
					" SELECT cs_envio_mensual_clientes" +
					" FROM comcat_producto_nafin" +
					" WHERE ic_producto_nafin = ?");
			
			LOG.debug("..:: qrySentencia: "+qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, 5);
			rs = ps.executeQuery();
			if(rs.next()){
				activo = rs.getString(1);
			}
			rs.close();
			ps.close();
			if(!"S".equals(activo)){
				resumenEjecucion.append("PARAMETRO NO ACTIVO");
				return resumenEjecucion.toString();
			}

			qrySentencia = new StringBuffer();
			qrySentencia.append(
					" SELECT cg_email" +
					" FROM com_envio_clientes" +
					" WHERE cs_envio_mensual = ? " +
					" AND ic_producto_nafin = ? ");
			LOG.debug("..:: qrySentencia: "+qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setString(1, "S");
			ps.setInt(2, 5);
			rs = ps.executeQuery();
			while(rs.next()){
				lCorreos.add(rs.getString("cg_email"));
			}
			rs.close();
			ps.close();

			writer = new OutputStreamWriter(new FileOutputStream(rutaArchivo+nombreArchivo+".csv"), "ISO-8859-1");
			buffer = new BufferedWriter(writer, 1024 * 16);	//Buffer de 16 Kb. 
			
			//List lColumnas = new ArrayList();
			buffer.write("IC_EPO,EPO,NUM. PYME,CRED. CAD,NUM. SIRAC,PYME,ESTADO" +
					"C.P.,TEL 1,FECHA VENCIMIENTO,FECHA AUTORIZACION,RFC");

			ArrayList alMes = new ArrayList();
			ArrayList alAnio = new ArrayList();
			String fechaIniAux = sdfFecha.format(gcFechaInicial.getTime());
			i = 0;
			while (!fechaIniAux.equals(sFechaFin)) {
				String[] arrFecha = fechaIniAux.split("/");
				int mesNumero = Integer.parseInt(arrFecha[1]);
				String mesAnioLetra = "";

				if ("01".equals(arrFecha[1])) {
					mesAnioLetra = "ENE-"+arrFecha[2];
				} else if ("02".equals(arrFecha[1])) {
					mesAnioLetra = "FEB-"+arrFecha[2];
				} else if ("03".equals(arrFecha[1])) {
					mesAnioLetra = "MAR-"+arrFecha[2];
				} else if ("04".equals(arrFecha[1])) {
					mesAnioLetra = "ABR-"+arrFecha[2];
				} else if ("05".equals(arrFecha[1])) {
					mesAnioLetra = "MAY-"+arrFecha[2];
				} else if ("06".equals(arrFecha[1])) {
					mesAnioLetra = "JUN-"+arrFecha[2];
				} else if ("07".equals(arrFecha[1])) {
					mesAnioLetra = "JUL-"+arrFecha[2];
				} else if ("08".equals(arrFecha[1])) {
					mesAnioLetra = "AGO-"+arrFecha[2];
				} else if ("09".equals(arrFecha[1])) {
					mesAnioLetra = "SEP-"+arrFecha[2];
				} else if ("10".equals(arrFecha[1])) {
					mesAnioLetra = "OCT-"+arrFecha[2];
				} else if ("11".equals(arrFecha[1])) {
					mesAnioLetra = "NOV-"+arrFecha[2];
				} else {
					mesAnioLetra = "DIC-"+arrFecha[2];
				}

				alMes.add(i, Integer.toString(mesNumero));
				alAnio.add(i, arrFecha[2]);
				buffer.write("," + mesAnioLetra);
				i++;

				gcFechaInicial.add(Calendar.MONTH, 1);
				fechaIniAux = sdfFecha.format(gcFechaInicial.getTime());
			}

			buffer.write(",MONTO CREDITO VENTAS 3 MESES");
			buffer.write(",TIPO DE CRITERIO");
			//lRetorno.add(lColumnas);
			
			
			StringBuffer strSQL = new StringBuffer(
					" SELECT /*+ INDEX(cie in_comrel_if_epo_02_nuk) USE_NL(cie cif) */  " +
					" 	DISTINCT cif.ic_if AS ic_if, " +
					" 	cif.cg_razon_social AS nombre_if " +
					" FROM comrel_if_epo cie, comcat_if cif, comcat_epo e " +
					" WHERE cif.ic_if = cie.ic_if " +
					" 	AND cie.ic_epo = e.ic_epo " +
					" 	AND cie.cs_vobo_nafin = 'S' " +
					" 	AND cie.cs_aceptacion = 'S' " +
					" 	AND cif.cs_habilitado = 'S' " +
					"  AND e.cs_habilitado = 'S' " +
					" ORDER BY cif.cg_razon_social ");
			List ifsAfiliados = new ArrayList();

			PreparedStatement psIFs = con.queryPrecompilado(strSQL.toString());
			
			rs = psIFs.executeQuery();
			while (rs.next()) {
				ifsAfiliados.add(rs.getString("ic_if"));
				String nombreIf = (rs.getString("nombre_if") == null)?"":rs.getString("nombre_if").replace(',',' ');
				buffer.write("," + nombreIf);
			}
			rs.close();
			psIFs.close();
			buffer.write("\n");

 
			qrySentencia = new StringBuffer(10000);	//Espacio inicial reservado para el String
			qrySentencia.append(
					" SELECT /*+ use_nl( mto, p, e, dom, es, se, sub, ram)*/" + 
					" e.ic_epo AS ic_epo," + 
					" e.cg_razon_social AS nomEpo," + 
					" p.ic_pyme AS numPyme," + 
					" mto.credcad AS credcad," + 
					" p.in_numero_sirac AS numSirac," + 
					" REPLACE(p.cg_razon_social, ',', '') AS nomPyme," + 
					" es.cd_nombre AS nomEstado," + 
					" dom.cn_cp AS CP," + 
					" dom.cg_telefono1 AS tel1," + 
					" dom.cg_telefono2 AS tel2," + 
					" p.cg_rfc AS rfcPyme," + 
					" se.cd_nombre AS sector," + 
					" sub.cd_nombre AS subsector," + 
					" ram.cd_nombre AS rama," + 
					" NVL(linea.fecha_venc, ' ') AS fecha_venc," + 
					" NVL(linea.fecha_aut, ' ') AS fecha_aut,");
			for (i = 0; i < alMes.size(); i++) {
				qrySentencia.append(" mto.mes" + (i + 1) + ",");
			}
			qrySentencia.append(" CASE WHEN(INSTR((");
			for (i = 6; i < alMes.size(); i++) {
				qrySentencia.append(" DECODE(mto.mes"+ (i + 1) + ", 0, 0, 1) || '' ||");
			}
			qrySentencia.delete(qrySentencia.length() - 9, qrySentencia.length());
			qrySentencia.append(
					"), '111111') <> 0) THEN 'SI' ELSE 'NO' END AS SeisConsec," +
					" CASE WHEN((");
			for( i = 0; i < alMes.size(); i++) {
				qrySentencia.append(" DECODE(mto.mes"+ (i + 1) + ", 0, 0, 1)+");
			}
			qrySentencia.delete(qrySentencia.length() - 1, qrySentencia.length());
			qrySentencia.append(
					") >= 8) THEN 'SI' ELSE 'NO' END AS OchoDeDoce" + 
					" FROM comcat_pyme p," + 
					" comcat_epo e," + 
					" com_domicilio dom," + 
					" comcat_estado es," + 
					" comcat_sector_econ se," + 
					" comcat_subsector sub," + 
					" comcat_rama ram," + 
					" (" + 
					" SELECT m.ic_epo," + 
					" m.ic_pyme," + 
					" m.credcad,");
			for (i = 0; i < alMes.size(); i++) {
				qrySentencia.append(" SUM(m.m" + (i + 1) + ") AS mes" + (i + 1) + ",");
			}
			qrySentencia.delete(qrySentencia.length() - 1, qrySentencia.length());
			qrySentencia.append(
					" FROM" +
					" (SELECT /*+ index(a in_com_acuse1_02_nuk) use_nl(a, d, pexp)*/" +
					" d.ic_epo," +
					" d.ic_pyme," +
					" DECODE(pexp.cs_habilitado, 'S', 'YA', 'H', 'YA', 'NO') AS credcad,");
			for (i = 0; i < alMes.size(); i++) {
				qrySentencia.append(" CASE WHEN (" + alMes.get(i).toString() + " = TO_NUMBER(TO_CHAR(D.df_alta,'MM'))) AND (" + alAnio.get(i).toString() + " = TO_NUMBER(TO_CHAR(D.df_alta,'YYYY'))) THEN SUM(D.fn_monto) ELSE 0 END AS m" + (i + 1) + ",");
			}
			qrySentencia.delete(qrySentencia.length() - 1, qrySentencia.length());
			
			qrySentencia.append(
					" FROM com_acuse1 a, com_documento D, comrel_pyme_epo_x_producto PEXP" + 
					" WHERE a.cc_acuse = d.cc_acuse " + 
					" AND D.ic_epo = PEXP.ic_epo(+)" + 
					" AND D.ic_pyme = PEXP.ic_pyme(+)" + 
					" AND a.DF_FECHAHORA_CARGA >= TO_DATE('" + sFechaIni + "', 'DD/MM/YYYY') " +
					" AND a.DF_FECHAHORA_CARGA < TO_DATE('" + sFechaFin + "', 'DD/MM/YYYY') " + // < que el 1er dia del mes en curso
					" AND D.ic_moneda = 1" + 
					" AND D.ic_estatus_docto <> 1" + 
					" AND PEXP.ic_producto_nafin(+) = 5" + 
					" GROUP BY d.ic_epo, d.ic_pyme, TO_NUMBER(TO_CHAR(D.df_alta,'YYYY')), TO_NUMBER(TO_CHAR(D.df_alta,'MM'))" + 
					", DECODE (pexp.cs_habilitado, 'S', 'YA', 'H', 'YA', 'NO')) M" + 
					" GROUP BY M.ic_epo, M.ic_pyme, m.credcad" + 
					" ) MTO" + 
					", (SELECT ic_pyme," + 
					" TO_CHAR(MAX(df_vencimiento_adicional), 'dd/mm/yyyy') AS fecha_venc," + 
					" TO_CHAR(MAX(df_autorizacion), 'dd/mm/yyyy') AS fecha_aut" + 
					" FROM com_linea_credito" + 
					" WHERE ic_producto_nafin = 5" + 
					" AND ic_estatus_linea = 5 " + 
					" GROUP BY ic_pyme) linea" + 
					" WHERE E.ic_epo = mto.ic_epo" + 
					" AND P.ic_pyme = mto.ic_pyme" + 
					" AND P.ic_pyme = DOM.ic_pyme" + 
					" AND p.ic_sector_econ = se.ic_sector_econ" + 
					" AND p.ic_sector_econ = sub.ic_sector_econ" + 
					" AND p.ic_subsector = sub.ic_subsector" + 
					" AND p.ic_sector_econ = ram.ic_sector_econ" + 
					" AND p.ic_subsector = ram.ic_subsector" + 
					" AND p.ic_rama = ram.ic_rama" + 
					" AND DOM.ic_estado = ES.ic_estado" + 
					" AND P.ic_pyme = linea.ic_pyme(+)" + 
					" GROUP BY E.ic_epo,E.cg_razon_social, P.ic_pyme," + 
					" mto.credcad," + 
					" P.in_numero_sirac, P.in_numero_sirac, P.cg_razon_social, ES.cd_nombre," + 
					" DOM.cn_cp, DOM.cg_telefono1, DOM.cg_telefono2, P.cg_rfc," + 
					" se.cd_nombre," + 
					" sub.cd_nombre," + 
					" ram.cd_nombre," + 
					" linea.fecha_venc," + 
					" linea.fecha_aut,");
			for (i = 0; i < alMes.size(); i++) {
				qrySentencia.append(" MTO.mes" + (i + 1) + ",");
			}
			qrySentencia.delete(qrySentencia.length() - 1, qrySentencia.length());

			LOG.debug("El query de clientes potenciales = "+qrySentencia.toString());

			String qryIfHabilitado = 
					" SELECT /*+ INDEX(cie in_comrel_if_epo_02_nuk) USE_NL(cie cif) */  " +
					"     DISTINCT cif.ic_if AS ic_if, " +
					"     CASE WHEN EXISTS( " +
					"        SELECT 1 " +
					"         FROM comrel_cuenta_bancaria ccb " +
					"        , comrel_pyme_if cpi " +
					"         WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria " +
					"         AND cpi.ic_if = cie.ic_if " +
					"         AND ccb.ic_pyme = ? " +
					"         AND ccb.ic_moneda = ? " +
					"         AND cpi.cs_vobo_if = ? " +
					"         AND cpi.cs_borrado = ? " +
					"     ) THEN 'SI HABILITADA' ELSE 'NO habilitada' END AS pyme_if_habilitada " +
					" FROM comrel_if_epo cie, comcat_if cif " +
					" WHERE cif.ic_if = cie.ic_if " +
					"     AND cie.ic_epo IN ( ? ) " +
					"     AND cie.cs_vobo_nafin = ? " +
					"     AND cie.cs_aceptacion = ? " +
					"     AND cif.cs_habilitado = ? ";
			PreparedStatement psIfHabilitadas = con.queryPrecompilado(qryIfHabilitado);

			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				//lColumnas = new ArrayList();
				double	montoCredito = 0;
				String tipoCriterio = "Ninguno";
				int claveEpo = rs.getInt("ic_epo");
				int clavePyme = rs.getInt("numPyme"); 
				
				buffer.write( claveEpo + "," +
				rs.getString("nomEpo").replace(',',' ') + "," +
				clavePyme + "," +
				rs.getString("credCad").replace(',',' ') + "," +
				(rs.getString("numSirac")==null?"":rs.getString("numSirac")) + "," +
				(rs.getString("nomPyme")==null?"":rs.getString("nomPyme").replace(',',' ')) + "," +
				(rs.getString("nomEstado")==null?"":rs.getString("nomEstado").replace(',',' ')) + "," +
				rs.getString("CP") + "," +
				(rs.getString("tel1")==null?"":rs.getString("tel1").replace(',',' ')) + "," +
				(rs.getString("fecha_venc")==null?"":rs.getString("fecha_venc")) + "," +
				(rs.getString("fecha_aut")==null?"":rs.getString("fecha_aut")) + "," +
				rs.getString("rfcPyme") + ",");
				
				for(i=0; i<alMes.size(); i++)
					buffer.write(rs.getString("mes"+(i+1)) + ",");

				if("SI".equals(rs.getString("SeisConsec"))){
					tipoCriterio = "�ltimos seis meses consecutivos";
					for(i=6; i<alMes.size(); i++){
						montoCredito += rs.getDouble("mes"+(i+1));
					}
					montoCredito = (montoCredito / 6) * 3;
				}else if("SI".equals(rs.getString("OchoDeDoce"))){
					tipoCriterio = "Ocho de Doce";
				}
				if(!"SI".equals(rs.getString("SeisConsec"))){
					int numMesesConPub = 0;
					for(i=alMes.size()-1; i>=0; i--){
						if(rs.getDouble("mes"+(i+1))>0){
							numMesesConPub++;
							if(numMesesConPub<=6){
								montoCredito += rs.getDouble("mes"+(i+1));
							}
						}
					}
					montoCredito = (montoCredito / (numMesesConPub*1.0)) * 3;
				}
				buffer.write(
						montoCredito + "," +
						tipoCriterio);
				
				//Determinar si esta habilitado o no un banco (o no aplica)
				psIfHabilitadas.clearParameters();
				psIfHabilitadas.setInt(1, clavePyme);
				psIfHabilitadas.setInt(2, 1);	//Moneda = 1
				psIfHabilitadas.setString(3, "S");
				psIfHabilitadas.setString(4, "N");
				psIfHabilitadas.setInt(5, claveEpo);
				psIfHabilitadas.setString(6, "S");
				psIfHabilitadas.setString(7, "S");
				psIfHabilitadas.setString(8, "S");
				ResultSet rsIfH = psIfHabilitadas.executeQuery();
				Map mIfH = new HashMap();
				while(rsIfH.next()) {
					mIfH.put(rsIfH.getString("ic_if"), rsIfH.getString("pyme_if_habilitada"));
				}
				rsIfH.close();
				//No cerrar aqui psIfHabilitadas, ya que se reutiliza 
				
				for (int j = 0; j < ifsAfiliados.size(); j++) {
					String habilitado = "";
					String claveAfiliadoEncabezado = (String)ifsAfiliados.get(j);
					if (mIfH.containsKey(claveAfiliadoEncabezado)) {
						habilitado = (String)mIfH.get(claveAfiliadoEncabezado);
					} else {
						habilitado = "N/A";
					}
					buffer.write("," + habilitado);
				}
				buffer.write("\n");	//Salta al sig. registro.
				//lRetorno.add(lColumnas);
			} //Fin while
			rs.close();
			ps.close();
			psIfHabilitadas.close();//Ya no se necesita este PS
			buffer.flush();
			buffer.close();	//Termina de escribir el archivo
			
			this.guardarArchivoReporte(nombreArchivo+".csv",rutaArchivo, "EnvioMensualEJBCliente.sh");
			
			String mensaje =
				" A quien corresponda:\n"+
				"\tPor este conducto le informamos que la informaci�n correspondiente al reporte de\n"+
				" Clientes Susceptibles de Credicadenas ya se encuentra disponible en ubicaci�n,"+
				" con el nombre de: "+nombreArchivo+
				"\n\n\tPuede usted descargarlo en la siguiente direccion: "+rutaVirtual+"/DescargaArchivoProCre?nombreArchivo="+nombreArchivo+".csv";

			for(i=0; i<lCorreos.size(); i++) {
				Correo.enviarMensajeTexto(
						(String)lCorreos.get(i),
						"ahgarfias@nafin.gob.mx",
						"ENVIO MENSUAL DE CLIENTES SUSCEPTIBLES DE CREDICADENAS",
						mensaje);
			}
			resumenEjecucion.append("Correos Enviados a: " + lCorreos + "\n" +
					"El proceso termina normalmente: " + "\n");
			return resumenEjecucion.toString();
		} catch(Exception e) {
			LOG.error("consultaClientesPotenciales(ERROR)", e);
			throw new AppException("Error en el proceso de envio mensual de Inteligencia Comercial", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			LOG.info("consultaClientesPotenciales(S) ::..");
		}
	}




	private Map consultaEnvioDiario()
	throws Exception	{
		LOG.info("consultaEnvioDiario(E)");

		AccesoDB con = new AccesoDB();
		ResultSet			rs		= null;
		PreparedStatement	ps		= null;
		StringBuffer		qrySentencia	= new StringBuffer("");
		Map 		mRetorno 		= new HashMap();
		List		lRetorno 		= new ArrayList();
		// Esto es para los criterios de la consulta.
		String servidor = "";
		List	lCorreos  = new ArrayList();
		try {
			String activo = null;
			con.conexionDB();

			qrySentencia = new StringBuffer(" SELECT cg_ip_smtp FROM com_param_gral");
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				servidor = rs.getString(1);
			}
			rs.close();
			ps.close();


			qrySentencia = new StringBuffer(
				" SELECT cs_envio_diario_clientes "+
				" FROM comcat_producto_nafin "+
				" WHERE ic_producto_nafin = 5 ");
			
			LOG.debug(" qrySentencia  "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				activo = rs.getString(1);
			}
			rs.close();
			ps.close();
			LOG.debug(" activo  "+activo);
			
			if(!"S".equals(activo)){
				LOG.info("consultaEnvioDiario:: PARAMETRO NO ACTIVO");
				return mRetorno;
			}

			qrySentencia = new StringBuffer(
				" SELECT cg_email"+
				" FROM com_envio_clientes"+
				" WHERE cs_envio_diario = 'S'"+
				" AND ic_producto_nafin = 5");
			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				lCorreos.add(rs.getString("cg_email"));
			}
			rs.close();
			ps.close();

			List lColumnas = new ArrayList();
			lColumnas.add("Mes de Publicaci�n");
			lColumnas.add("IC_EPO");
			lColumnas.add("EPO");
			lColumnas.add("Num. Pyme");
			lColumnas.add("Nombre de la Pyme");
			lColumnas.add("Ejecutivo Promoci�n");
			lColumnas.add("Tel�fono");
			lColumnas.add("Correo");
			lColumnas.add("Monto de L�nea Consolidado");
			lColumnas.add("Monto Cr�dito Ventas de Tres meses");
			lColumnas.add("Estatus de respuesta");
			lColumnas.add("Fecha de Respuesta");


			lRetorno.add(lColumnas);

			//INICIA EL QUERY DE CONSULTA
			qrySentencia = new StringBuffer(
				" SELECT TO_CHAR(mp.df_publicacion,'mm/yyyy') AS mesPub "   +
				"   ,mp.ic_epo"   +
				"   ,e.cg_razon_social AS nombre_epo"   +
				"   ,mp.ic_pyme"   +
				"   ,p.cg_razon_social AS nombre_pyme"   +
				"   ,mp.cg_ejecutivo_prom"   +
				"   ,mp.cg_telefono"   +
				"   ,mp.cg_correo"   +
				"   ,mp.fn_monto_credito"   +
				"   ,mp.fg_monto_consolidado"   +
				"   ,to_char(df_respuesta,'dd/mm/yyyy') AS fecha_resp"   +
				" FROM com_mensaje_pyme mp"   +
				"  , comcat_epo e"   +
				"  , comcat_pyme p"   +
				" WHERE mp.ic_epo = e.ic_epo"   +
				" AND mp.ic_pyme = p.ic_pyme "   +
				" AND cg_estatus_respuesta = 'S'"   +
				" AND df_publicacion BETWEEN TO_DATE(TO_CHAR(SYSDATE,'mm/yyyy'),'mm/yyyy') AND TO_DATE(TO_CHAR(SYSDATE,'mm/yyyy'),'mm/yyyy')+31"
			);


			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			while(rs.next()){
				lColumnas = new ArrayList();
				lColumnas.add(rs.getString("mesPub"));
				lColumnas.add(rs.getString("ic_epo"));
				lColumnas.add((rs.getString("nombre_epo")==null?"":rs.getString("nombre_epo")));
				lColumnas.add(rs.getString("ic_pyme"));
				lColumnas.add((rs.getString("nombre_pyme")==null?"":rs.getString("nombre_pyme")));
				lColumnas.add((rs.getString("cg_ejecutivo_prom")==null?"":rs.getString("cg_ejecutivo_prom")));
				lColumnas.add((rs.getString("cg_telefono")==null?"":rs.getString("cg_telefono")));
				lColumnas.add((rs.getString("cg_correo")==null?"":rs.getString("cg_correo")));
				lColumnas.add((rs.getString("fn_monto_credito")==null?"":rs.getString("fn_monto_credito")));
				lColumnas.add((rs.getString("fg_monto_consolidado")==null?"":rs.getString("fg_monto_consolidado")));
				lColumnas.add("SI");
				lColumnas.add((rs.getString("fecha_resp")==null?"":rs.getString("fecha_resp")));
				lRetorno.add(lColumnas);
			}
			rs.close();
			ps.close();
			if(lRetorno.size()<=1){
				return mRetorno;
			}
			mRetorno.put("datos",lRetorno);
			mRetorno.put("correos",lCorreos);
			mRetorno.put("servidorsmtp",servidor);
			return mRetorno;
		}catch(Exception e){
			LOG.error("consultaEnvioDiario(Exception)", e);
			throw new AppException("Error Envio Diario", e);
		}finally{
			LOG.info("consultaEnvioDiario(S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}

	@Override
	public Map consultaMesesAnios(String anio,String mes)
		throws NafinException{
		AccesoDB	con			= new AccesoDB();
		Map			mRetorno	= new HashMap();
		try{
			PreparedStatement	ps				= null;
			ResultSet			rs				= null;
			String				qrySentencia	= null;
			List				lAnios			= new ArrayList();
			List				lMeses			= new ArrayList();
			con.conexionDB();
			qrySentencia =
				" select distinct to_char(df_publicacion,'yyyy') as anio "   +
				" from com_mensaje_pyme"  ;
			ps  = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			while(rs.next()){
				lAnios.add(rs.getString("anio"));
			}
			rs.close();
			ps.close();

			if(!"".equals(anio)){
				qrySentencia =
					" select distinct to_char(df_publicacion,'mm') as mes "   +
					" from com_mensaje_pyme"+
					" where to_char(df_publicacion,'yyyy') = ?";
				ps  = con.queryPrecompilado(qrySentencia);
				ps.setString(1,anio);
				rs = ps.executeQuery();
				while(rs.next()){
					lMeses.add(rs.getString("mes"));
				}
				rs.close();
				ps.close();
			}

			mRetorno.put("meses",lMeses);
			mRetorno.put("anios",lAnios);
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return mRetorno;
	}

/**
 * M�todo que genera el reporte de las operaciones en cadenas para una o mas EPOs.
 * @param hashDatosEntrada Codigo hash resultante de la cadena que contiene los datos de entrada.
 * @param banco_fondeo La clave del banco de fondeo con el que se realizaron las operaciones en cadenas
 * @param ic_epo La clave de la EPO para la que se requiere generar el reporte, si no se especifica (cadena vac�a) el reporte se genera para todas las epos habilitadas.
 * @param fecha_inicial La fecha de inicio del periodo en el que se genera el reporte.
 * @param fecha_final La fecha de termino del periodo en el que se genera el reporte.
 * @return reporteOperacionesCadenas Lista con la informaci�n del reporte.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 013 - 2009 Reportes
 * @author Alberto Cruz Flores
 */
	@Override
	public List reporteOperacionesEnCadenas(String hashDatosEntrada, String banco_fondeo, String ic_epo, String fecha_inicial, String fecha_final) throws NafinException{
		LOG.info("reporteOperacionesEnCadenas(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean operacion_exitosa = true;
		List reporteOperacionesCadenas = new ArrayList();

		try{
			con.conexionDB();

			strSQL.append(" SELECT COUNT(ic_reporte_cadenas) registros_reporte");
			strSQL.append(" FROM comtmp_rep_operaciones_cadenas");
			strSQL.append(" WHERE ic_reporte_cadenas = ?");
			varBind.add(new Integer(hashDatosEntrada));

			LOG.debug("..:: strSQL : "+strSQL.toString());
			LOG.debug("..:: varBind : "+varBind.toString());

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();

			String registros_reporte = "";

			while(rst.next()){registros_reporte = rst.getString("registros_reporte");}

			rst.close();
			pst.close();

			if(registros_reporte.equals("0")){
				strSQL =  new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT epos.ic_epo ic_epo,");
				strSQL.append(" epos.pymes_afiliadas pymes_afiliadas,");
				strSQL.append(" epos.nombre_epo nombre_epo,");
				strSQL.append(" epos.pymes_habilitadas pymes_habilitadas,");
				strSQL.append(" NVL(php.cant_pyme_hab_pub, 0) cant_pyme_hab_pub,");
				strSQL.append(" NVL(php.monto_pyme_hab_pub, 0) monto_pyme_hab_pub,");
				strSQL.append(" NVL((epos.pymes_habilitadas - NVL(php.cant_pyme_hab_pub, 0)), 0) cant_pyme_hab_spub,");
				strSQL.append(" NVL(pho.cant_pyme_hab_ope, 0) cant_pyme_hab_ope,");
				strSQL.append(" NVL(pho.monto_pyme_hab_ope, 0) monto_pyme_hab_ope,");
				strSQL.append(" epos.pymes_no_habilitadas pymes_no_habilitadas,");
				strSQL.append(" NVL(pnhp.cant_pyme_nh_pub, 0) cant_pyme_nh_pub,");
				strSQL.append(" NVL(pnhp.monto_pyme_nh_pub, 0) monto_pyme_nh_pub,");
				strSQL.append(" NVL((epos.pymes_no_habilitadas - NVL(pnhp.cant_pyme_nh_pub, 0)), 0) cant_pyme_nh_spub");
				strSQL.append(" FROM (");
				strSQL.append(" SELECT /*+use_nl(epo cpe pyme) index(epo cp_comcat_epo_pk)*/");
				strSQL.append(" epo.ic_epo AS ic_epo,");
				strSQL.append(" COUNT(cpe.ic_pyme) AS pymes_afiliadas,");
				strSQL.append(" epo.cg_razon_social AS nombre_epo,");
				strSQL.append(" SUM(CASE WHEN cpe.cs_aceptacion = 'H' THEN 1 ELSE 0 END) AS pymes_habilitadas,");
				strSQL.append(" SUM(CASE WHEN cpe.cs_aceptacion <> 'H' THEN 1 ELSE 0 END) AS pymes_no_habilitadas");
				strSQL.append(" FROM comrel_pyme_epo cpe, comcat_epo epo, comcat_pyme pyme");
				strSQL.append(" WHERE cpe.ic_epo = epo.ic_epo");
				strSQL.append(" AND cpe.ic_pyme = pyme.ic_pyme");
				strSQL.append(" AND pyme.cs_habilitado = ?");
				strSQL.append(" AND epo.cs_habilitado = ?");
				strSQL.append(" AND epo.ic_banco_fondeo = ?");
				if(!ic_epo.equals("")){strSQL.append(" AND epo.ic_epo = ?");}
				strSQL.append(" GROUP BY epo.ic_epo, epo.cg_razon_social) epos,");
				strSQL.append(" (SELECT ic_epo, COUNT(1) AS cant_pyme_hab_pub, SUM(monto) monto_pyme_hab_pub");
				strSQL.append(" FROM (");
				strSQL.append(" SELECT /*+use_nl(docto pe p)*/ pe.ic_epo, pe.ic_pyme,SUM(docto.fn_monto) AS monto");
				strSQL.append(" FROM comrel_pyme_epo pe, com_documento docto, comcat_pyme p");
				strSQL.append(" WHERE pe.ic_epo = docto.ic_epo");
				strSQL.append(" AND pe.ic_pyme = docto.ic_pyme");
				strSQL.append(" AND pe.ic_pyme = p.ic_pyme");
				strSQL.append(" AND p.cs_habilitado = ?");
				strSQL.append(" AND pe.cs_aceptacion = ?");
				strSQL.append(" AND docto.df_alta >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND docto.df_alta < TO_DATE(?, 'dd/mm/yyyy') + 1");
				if(!ic_epo.equals("")){strSQL.append(" AND pe.ic_epo = ?");}
				strSQL.append(" GROUP BY pe.ic_epo, pe.ic_pyme");
				strSQL.append(" )");
				strSQL.append(" GROUP BY ic_epo");
				strSQL.append(" ORDER BY ic_epo) php,");
				strSQL.append(" (SELECT ic_epo, COUNT(1) AS cant_pyme_hab_ope, SUM(monto) monto_pyme_hab_ope");
				strSQL.append(" FROM (");
				strSQL.append(" SELECT /*+use_nl(sol docto pyme)*/ docto.ic_epo, docto.ic_pyme, SUM(docto.fn_monto) AS monto");
				strSQL.append(" FROM com_solicitud sol, com_documento docto, comcat_pyme pyme");
				strSQL.append(" WHERE sol.ic_documento = docto.ic_documento");
				strSQL.append(" AND docto.ic_pyme = pyme.ic_pyme");
				strSQL.append(" AND pyme.cs_habilitado = ?");
				strSQL.append(" AND sol.ic_banco_fondeo = ?");
				strSQL.append(" AND docto.ic_estatus_docto IN (?, ?, ?)");
				strSQL.append(" AND sol.df_operacion >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1");
				if(!ic_epo.equals("")){strSQL.append(" AND docto.ic_epo = ?");}
				strSQL.append(" GROUP BY docto.ic_epo, docto.ic_pyme");
				strSQL.append(" )");
				strSQL.append(" GROUP BY ic_epo");
				strSQL.append(" ORDER BY ic_epo) pho,");
				strSQL.append(" (SELECT ic_epo, COUNT(1) AS cant_pyme_nh_pub, SUM(monto) monto_pyme_nh_pub");
				strSQL.append(" FROM (");
				strSQL.append(" SELECT /*+use_nl(docto pe p)*/ pe.ic_epo, pe.ic_pyme, SUM(docto.fn_monto) monto");
				strSQL.append(" FROM comrel_pyme_epo pe, com_documento docto, comcat_pyme p");
				strSQL.append(" WHERE pe.ic_epo = docto.ic_epo");
				strSQL.append(" AND pe.ic_pyme = docto.ic_pyme");
				strSQL.append(" AND pe.ic_pyme = p.ic_pyme");
				strSQL.append(" AND p.cs_habilitado = ?");
				strSQL.append(" AND pe.cs_aceptacion <> ?");
				strSQL.append(" AND docto.df_alta >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND docto.df_alta < TO_DATE(?, 'dd/mm/yyyy') + 1");
				if(!ic_epo.equals("")){strSQL.append(" AND pe.ic_epo = ?");}
				strSQL.append(" GROUP BY pe.ic_epo, pe.ic_pyme");
				strSQL.append(" )");
				strSQL.append(" GROUP BY ic_epo");
				strSQL.append(" ORDER BY ic_epo) pnhp");
				strSQL.append(" WHERE epos.ic_epo = php.ic_epo(+)");
				strSQL.append(" AND epos.ic_epo = pho.ic_epo(+)");
				strSQL.append(" AND epos.ic_epo = pnhp.ic_epo(+)");
				strSQL.append(" ORDER BY epos.nombre_epo");

				varBind.add("S");
				varBind.add("S");
				varBind.add(new Integer(banco_fondeo));
				if(!ic_epo.equals("")){varBind.add(new Long(ic_epo));}
				varBind.add("S");
				varBind.add("H");
				varBind.add(fecha_inicial);
				varBind.add(fecha_final);
				if(!ic_epo.equals("")){varBind.add(new Long(ic_epo));}
				varBind.add("S");
				varBind.add(new Integer(banco_fondeo));
				varBind.add(new Integer(4));
				varBind.add(new Integer(11));
				varBind.add(new Integer(12));
				varBind.add(fecha_inicial);
				varBind.add(fecha_final);
				if(!ic_epo.equals("")){varBind.add(new Long(ic_epo));}
				varBind.add("S");
				varBind.add("H");
				varBind.add(fecha_inicial);
				varBind.add(fecha_final);
				if(!ic_epo.equals("")){varBind.add(new Long(ic_epo));}

				LOG.debug("..:: strSQL : "+strSQL.toString());
				LOG.debug("..:: varBind : "+varBind.toString());

				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				rst = pst.executeQuery();

				while(rst.next()){
					List operacionesCadenasEpo = new ArrayList();
					operacionesCadenasEpo.add(rst.getString("pymes_afiliadas")==null?"":rst.getString("pymes_afiliadas"));
					operacionesCadenasEpo.add(rst.getString("nombre_epo")==null?"":rst.getString("nombre_epo"));
					operacionesCadenasEpo.add(rst.getString("pymes_habilitadas")==null?"":rst.getString("pymes_habilitadas"));
					operacionesCadenasEpo.add(rst.getString("cant_pyme_hab_pub")==null?"":rst.getString("cant_pyme_hab_pub"));
					operacionesCadenasEpo.add(rst.getString("monto_pyme_hab_pub")==null?"":rst.getString("monto_pyme_hab_pub"));
					operacionesCadenasEpo.add(rst.getString("cant_pyme_hab_spub")==null?"":rst.getString("cant_pyme_hab_spub"));
					operacionesCadenasEpo.add(rst.getString("cant_pyme_hab_ope")==null?"":rst.getString("cant_pyme_hab_ope"));
					operacionesCadenasEpo.add(rst.getString("monto_pyme_hab_ope")==null?"":rst.getString("monto_pyme_hab_ope"));
					operacionesCadenasEpo.add(rst.getString("pymes_no_habilitadas")==null?"":rst.getString("pymes_no_habilitadas"));
					operacionesCadenasEpo.add(rst.getString("cant_pyme_nh_pub")==null?"":rst.getString("cant_pyme_nh_pub"));
					operacionesCadenasEpo.add(rst.getString("monto_pyme_nh_pub")==null?"":rst.getString("monto_pyme_nh_pub"));
					operacionesCadenasEpo.add(rst.getString("cant_pyme_nh_spub")==null?"":rst.getString("cant_pyme_nh_spub"));

					reporteOperacionesCadenas.add(operacionesCadenasEpo);
				}

				rst.close();
				pst.close();

				if(!reporteOperacionesCadenas.isEmpty()){
					strSQL = new StringBuffer();
					varBind = new ArrayList();

					strSQL.append(" INSERT INTO comtmp_rep_operaciones_cadenas (");
					strSQL.append(" ic_reporte_cadenas,");
					strSQL.append(" in_pymes_afiliadas,");
					strSQL.append(" cg_nombre_epo,");
					strSQL.append(" in_pymes_habilitadas,");
					strSQL.append(" in_pymes_hab_cpub,");
					strSQL.append(" fn_monto_hab_cpub,");
					strSQL.append(" in_pymes_hab_spub,");
					strSQL.append(" in_pymes_hab_cope,");
					strSQL.append(" fn_monto_hab_cope,");
					strSQL.append(" in_pymes_no_habilitadas,");
					strSQL.append(" in_pymes_no_hab_cpub,");
					strSQL.append(" fn_monto_no_hab_cpub,");
					strSQL.append(" in_pymes_no_hab_spub");
					strSQL.append(") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

					LOG.debug("..:: strSQL : "+strSQL.toString());

					pst = con.queryPrecompilado(strSQL.toString());

					for(int i = 0; i < reporteOperacionesCadenas.size(); i++){
						List operacionesCadenasEpo = (List)reporteOperacionesCadenas.get(i);
						pst.setInt(1, Integer.parseInt(hashDatosEntrada));
						pst.setInt(2, Integer.parseInt((String)operacionesCadenasEpo.get(0)));
						pst.setString(3, (String)operacionesCadenasEpo.get(1));
						pst.setInt(4, Integer.parseInt((String)operacionesCadenasEpo.get(2)));
						pst.setInt(5, Integer.parseInt((String)operacionesCadenasEpo.get(3)));
						pst.setDouble(6, Double.parseDouble((String)operacionesCadenasEpo.get(4)));
						pst.setInt(7, Integer.parseInt((String)operacionesCadenasEpo.get(5)));
						pst.setInt(8, Integer.parseInt((String)operacionesCadenasEpo.get(6)));
						pst.setDouble(9, Double.parseDouble((String)operacionesCadenasEpo.get(7)));
						pst.setInt(10, Integer.parseInt((String)operacionesCadenasEpo.get(8)));
						pst.setInt(11, Integer.parseInt((String)operacionesCadenasEpo.get(9)));
						pst.setDouble(12, Double.parseDouble((String)operacionesCadenasEpo.get(10)));
						pst.setInt(13, Integer.parseInt((String)operacionesCadenasEpo.get(11)));
						pst.executeUpdate();
						pst.clearParameters();
					}
					pst.close();
				}
			}else{
				strSQL = new StringBuffer();
				varBind = new ArrayList();

				strSQL.append(" SELECT in_pymes_afiliadas,");
				strSQL.append(" cg_nombre_epo,");
				strSQL.append(" in_pymes_habilitadas,");
				strSQL.append(" in_pymes_hab_cpub,");
				strSQL.append(" fn_monto_hab_cpub,");
				strSQL.append(" in_pymes_hab_spub,");
				strSQL.append(" in_pymes_hab_cope,");
				strSQL.append(" fn_monto_hab_cope,");
				strSQL.append(" in_pymes_no_habilitadas,");
				strSQL.append(" in_pymes_no_hab_cpub,");
				strSQL.append(" fn_monto_no_hab_cpub,");
				strSQL.append(" in_pymes_no_hab_spub");
				strSQL.append(" FROM comtmp_rep_operaciones_cadenas");
				strSQL.append(" WHERE ic_reporte_cadenas = ?");
				varBind.add(new Integer(hashDatosEntrada));

				LOG.debug("..:: strSQL : "+strSQL.toString());
				LOG.debug("..:: varBind : "+varBind.toString());

				pst = con.queryPrecompilado(strSQL.toString(), varBind);
				rst = pst.executeQuery();

				while(rst.next()){
					List operacionesCadenasEpo = new ArrayList();
					operacionesCadenasEpo.add(rst.getString(1));
					operacionesCadenasEpo.add(rst.getString(2));
					operacionesCadenasEpo.add(rst.getString(3));
					operacionesCadenasEpo.add(rst.getString(4));
					operacionesCadenasEpo.add(rst.getString(5));
					operacionesCadenasEpo.add(rst.getString(6));
					operacionesCadenasEpo.add(rst.getString(7));
					operacionesCadenasEpo.add(rst.getString(8));
					operacionesCadenasEpo.add(rst.getString(9));
					operacionesCadenasEpo.add(rst.getString(10));
					operacionesCadenasEpo.add(rst.getString(11));
					operacionesCadenasEpo.add(rst.getString(12));
					reporteOperacionesCadenas.add(operacionesCadenasEpo);
				}
				rst.close();
				pst.close();
			}
		}catch(Exception e){
			operacion_exitosa = false;
			LOG.error("reporteOperacionesEnCadenas(ERROR)", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(operacion_exitosa);
				con.cierraConexionDB();
			}
		}
		LOG.info("reporteOperacionesEnCadenas(F)");
		return reporteOperacionesCadenas;
	}

/**
 * M�todo que genera el reporte de las operaciones de Descuento Autom�tico que generan una o m�s EPOs.
 * @param parametros_reporte Lista que contiene los parametros de entrada para generar el reporte
 *        los parametros vienen en el siguiente orden:
 *        0)  Cadena con el c�digo hash resultante obtenida a partir de los datos de entrada.
 *        1)  La clave del banco de fondeo con el que se realizaron las operaciones en cadenas
 *        2)  La clave de la EPO para la que se requiere generar el reporte, si no se especifica (cadena vac�a) el reporte se genera para todas las epos habilitadas.
 *        3)  La clave del IF para la que se requiere generar el reporte, si no se especifica (cadena vac�a) el reporte se genera para todas las ifs asociadas a la epo.
 *        4)  La clave de la versi�n de convenio asociado a las pymes.
 *        5)  La fecha de inicio del periodo en el que se genera el reporte.
 *        6)  La fecha de t�rmino del periodo en el que se genera el reporte.
 * @return reporteOperacionesCadenas Lista con la informaci�n del reporte.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 013 - 2009 Reportes
 * @author Alberto Cruz Flores
 */
	@Override
	public List reporteOperacionesDescuentoAutomatico(List parametros_reporte) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    boolean operacion_exitosa = true;
    List reporteOperacionesDescuentoAutomatico = new ArrayList();

    String hashDatosEntrada = (String)parametros_reporte.get(0);
    String banco_fondeo = (String)parametros_reporte.get(1);
    String ic_epo = (String)parametros_reporte.get(2);
    String ic_if = (String)parametros_reporte.get(3);
    String ic_version_convenio = (String)parametros_reporte.get(4);
    String fecha_inicial = (String)parametros_reporte.get(5);
    String fecha_final = (String)parametros_reporte.get(6);

    LOG.info("..:: reporteOperacionesDescuentoAutomatico(I)");
    try {
      con.conexionDB();

      strSQL.append(" SELECT COUNT(ic_reporte_desc_auto) registros_reporte");
      strSQL.append(" FROM comtmp_rep_oper_desc_auto");
      strSQL.append(" WHERE ic_reporte_desc_auto = ?");
      varBind.add(new Integer(hashDatosEntrada));
      //LOG.debug("..:: strSQL : "+strSQL.toString());
      //LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      String registros_reporte = "0";

      while(rst.next()){registros_reporte = rst.getString("registros_reporte");}

      rst.close();
      pst.close();

      if(registros_reporte.equals("0")){
        strSQL =  new StringBuffer();
        varBind = new ArrayList();

        strSQL.append(" SELECT /*+use_nl(cpi epo cif) index(pyme cp_comcat_pyme_pk) index(ccb in_comrel_cuent_ban_01_nuk)+*/ epo.ic_epo,");
        strSQL.append(" cif.ic_if,");
        strSQL.append(" epo.cg_razon_social nombre_epo,");
        strSQL.append(" cif.cg_razon_social nombre_if,");
        strSQL.append(" conv.cd_version_convenio version_convenio,");
        strSQL.append(" count(ccb.ic_pyme) total_pymes");
        strSQL.append(" FROM comrel_pyme_if cpi");
        strSQL.append(" ,comrel_cuenta_bancaria ccb");
        strSQL.append(" ,comcat_epo epo");
        strSQL.append(" ,comcat_if cif");
        strSQL.append(" ,comcat_version_convenio conv");
        strSQL.append(" ,comcat_pyme pyme");
        strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
        strSQL.append(" AND cpi.ic_epo = epo.ic_epo");
        strSQL.append(" AND cpi.ic_if = cif.ic_if");
        strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
        strSQL.append(" AND pyme.ic_version_convenio = conv.ic_version_convenio");
        strSQL.append(" AND ccb.ic_moneda = ?");
        strSQL.append(" AND ccb.cs_borrado = ?");
        strSQL.append(" AND cpi.cs_vobo_if = ?");
        strSQL.append(" AND cpi.cs_borrado = ?");
        strSQL.append(" AND pyme.cs_dscto_automatico IN (?, ?)");
        strSQL.append(" AND epo.ic_banco_fondeo = ?");
        if(!ic_epo.equals("")){strSQL.append(" AND epo.ic_epo = ?");}
        if(!ic_if.equals("")){strSQL.append(" AND cif.ic_if = ?");}
        strSQL.append(" AND conv.ic_version_convenio = ?");
        strSQL.append(" GROUP BY epo.ic_epo, cif.ic_if, epo.cg_razon_social, cif.cg_razon_social, conv.cd_version_convenio");
        strSQL.append(" ORDER BY epo.cg_razon_social, cif.cg_razon_social, conv.cd_version_convenio");

        varBind.add(new Integer(1));
        varBind.add("N");
        varBind.add("S");
        varBind.add("N");
        varBind.add("S");
        varBind.add("N");
        varBind.add(new Integer(banco_fondeo));
        if(!ic_epo.equals("")){varBind.add(new Long(ic_epo));}
        if(!ic_if.equals("")){varBind.add(new Long(ic_if));}
        varBind.add(new Integer(ic_version_convenio));
        //LOG.debug("..:: strSQL : "+strSQL.toString());
        //LOG.debug("..:: varBind : "+varBind.toString());
        pst = con.queryPrecompilado(strSQL.toString(), varBind);
        rst = pst.executeQuery();

        while(rst.next()){
          List operacionesDescuentoAutomatico = new ArrayList();
          String clave_epo = rst.getString(1)==null?"":rst.getString(1);
          String clave_if = rst.getString(2)==null?"":rst.getString(2);
          String nombre_epo = rst.getString(3)==null?"":rst.getString(3);
          String nombre_if = rst.getString(4)==null?"":rst.getString(4);
          String version_convenio = rst.getString(5)==null?"":rst.getString(5);
          int numero_pymes_afiliadas = rst.getInt(6);

          int numero_pymes_con_desc = 0;
          int numero_pymes_sin_desc = 0;
          int numero_doctos_pub_con_desc = 0;
          int numero_doctos_ope_con_desc = 0;
          int numero_doctos_pub_sin_desc = 0;
          int numero_doctos_ope_sin_desc = 0;
          BigDecimal monto_publicado_con_desc = new BigDecimal("0.00");
          BigDecimal monto_operado_con_desc = new BigDecimal("0.00");
          BigDecimal monto_publicado_sin_desc = new BigDecimal("0.00");
          BigDecimal monto_operado_sin_desc = new BigDecimal("0.00");

          //SE OBTIENE EL TOTAL DE PYMES CON DESCUENTO PARA LA RELACION EPO-IF-CONVENIO
          strSQL =  new StringBuffer();
          varBind = new ArrayList();

          strSQL.append(" SELECT COUNT(ccb.ic_pyme) numero_pymes_cd");
          strSQL.append(" FROM comrel_pyme_if cpi");
          strSQL.append(" ,comrel_cuenta_bancaria ccb");
          strSQL.append(" ,comcat_epo epo");
          strSQL.append(" ,comcat_if cif");
          strSQL.append(" ,comcat_version_convenio conv");
          strSQL.append(" ,comcat_pyme pyme");
          strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
          strSQL.append(" AND cpi.ic_epo = epo.ic_epo");
          strSQL.append(" AND cpi.ic_if = cif.ic_if");
          strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
          strSQL.append(" AND pyme.ic_version_convenio = conv.ic_version_convenio");
          strSQL.append(" AND ccb.ic_moneda = ?");
          strSQL.append(" AND ccb.cs_borrado = ?");
          strSQL.append(" AND cpi.cs_vobo_if = ?");
          strSQL.append(" AND cpi.cs_borrado = ?");
          strSQL.append(" AND pyme.cs_dscto_automatico = ?");
          strSQL.append(" AND epo.ic_banco_fondeo = ?");
          strSQL.append(" AND epo.ic_epo = ?");
          strSQL.append(" AND cif.ic_if = ?");
          strSQL.append(" AND conv.ic_version_convenio = ?");
          strSQL.append(" GROUP BY epo.ic_epo, epo.cg_razon_social, cif.ic_if, cif.cg_razon_social, conv.cd_version_convenio");
          strSQL.append(" ORDER BY epo.cg_razon_social, cif.cg_razon_social, conv.cd_version_convenio");

          varBind.add(new Integer(1));
          varBind.add("N");
          varBind.add("S");
          varBind.add("N");
          varBind.add("S");
          varBind.add(new Integer(banco_fondeo));
          varBind.add(new Long(clave_epo));
          varBind.add(new Long(clave_if));
          varBind.add(new Integer(ic_version_convenio));
          //LOG.debug("..:: strSQL : "+strSQL.toString());
          //LOG.debug("..:: varBind : "+varBind.toString());

          PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
          ResultSet rst1 = pst1.executeQuery();

          while(rst1.next()){
            numero_pymes_con_desc = rst1.getInt(1);
          }

          rst1.close();
          pst1.close();

          //Se obtiene el numero y monto de los documentos publicados a pymes con descuento automatico
          strSQL =  new StringBuffer();
          varBind = new ArrayList();

          strSQL.append(" SELECT COUNT(docto.ic_documento) numero_doctos_pcd, SUM(docto.fn_monto) monto_doctos_pcd");
          strSQL.append(" FROM comrel_pyme_if cpi");
          strSQL.append(" ,comrel_cuenta_bancaria ccb");
          strSQL.append(" ,comcat_epo epo");
          strSQL.append(" ,comcat_if cif");
          strSQL.append(" ,comcat_version_convenio conv");
          strSQL.append(" ,comcat_pyme pyme");
          strSQL.append(" ,com_documento docto");
          strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
          strSQL.append(" AND cpi.ic_epo = epo.ic_epo");
          strSQL.append(" AND cpi.ic_if = cif.ic_if");
          strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
          strSQL.append(" AND pyme.ic_version_convenio = conv.ic_version_convenio");
          strSQL.append(" AND docto.ic_epo = epo.ic_epo");
          strSQL.append(" AND docto.ic_if = cif.ic_if");
          strSQL.append(" AND docto.ic_pyme = pyme.ic_pyme");
          //strSQL.append(" AND docto.ic_estatus_docto NOT IN (?, ?, ?)");
          strSQL.append(" AND ccb.ic_moneda = ?");
          strSQL.append(" AND ccb.cs_borrado = ?");
          strSQL.append(" AND cpi.cs_vobo_if = ?");
          strSQL.append(" AND cpi.cs_borrado = ?");
          strSQL.append(" AND pyme.cs_dscto_automatico = ?");
          strSQL.append(" AND epo.ic_banco_fondeo = ?");
          strSQL.append(" AND epo.ic_epo = ?");
          strSQL.append(" AND cif.ic_if = ?");
          strSQL.append(" AND conv.ic_version_convenio = ?");
          strSQL.append(" AND docto.df_alta BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE (?, 'dd/mm/yyyy') + 1");
          strSQL.append(" GROUP BY epo.ic_epo, epo.cg_razon_social, cif.ic_if, cif.cg_razon_social, conv.cd_version_convenio");
          strSQL.append(" ORDER BY epo.cg_razon_social, cif.cg_razon_social, conv.cd_version_convenio");

          //varBind.add(new Integer(4));
          //varBind.add(new Integer(11));
          //varBind.add(new Integer(12));
          varBind.add(new Integer(1));
          varBind.add("N");
          varBind.add("S");
          varBind.add("N");
          varBind.add("S");
          varBind.add(new Integer(banco_fondeo));
          varBind.add(new Long(clave_epo));
          varBind.add(new Long(clave_if));
          varBind.add(new Integer(ic_version_convenio));
          varBind.add(fecha_inicial);
          varBind.add(fecha_final);
          //LOG.debug("..:: strSQL : "+strSQL.toString());
          //LOG.debug("..:: varBind : "+varBind.toString());

          pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
          rst1 = pst1.executeQuery();

          while(rst1.next()){
            numero_doctos_pub_con_desc = rst1.getInt(1);
            monto_publicado_con_desc = new BigDecimal(rst1.getString(2)==null?"0.00":rst1.getString(2));
          }

          rst1.close();
          pst1.close();

          //Se obtiene el numero y monto de los documentos operados por pymes con descuento automatico
          strSQL =  new StringBuffer();
          varBind = new ArrayList();

          strSQL.append(" SELECT COUNT(docto.ic_documento) numero_doctos_ocd, SUM(docto.fn_monto) monto_doctos_ocd");
          strSQL.append(" FROM comrel_pyme_if cpi");
          strSQL.append(" ,comrel_cuenta_bancaria ccb");
          strSQL.append(" ,comcat_epo epo");
          strSQL.append(" ,comcat_if cif");
          strSQL.append(" ,comcat_version_convenio conv");
          strSQL.append(" ,comcat_pyme pyme");
          strSQL.append(" ,com_documento docto");
          strSQL.append(" ,com_solicitud sol");
          strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
          strSQL.append(" AND cpi.ic_epo = epo.ic_epo");
          strSQL.append(" AND cpi.ic_if = cif.ic_if");
          strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
          strSQL.append(" AND pyme.ic_version_convenio = conv.ic_version_convenio");
          strSQL.append(" AND docto.ic_epo = epo.ic_epo");
          strSQL.append(" AND docto.ic_if = cif.ic_if");
          strSQL.append(" AND docto.ic_pyme = pyme.ic_pyme");
          strSQL.append(" AND docto.ic_documento = sol.ic_documento");
          strSQL.append(" AND epo.ic_banco_fondeo = sol.ic_banco_fondeo");
          strSQL.append(" AND docto.ic_estatus_docto IN (?, ?, ?)");
          strSQL.append(" AND ccb.ic_moneda = ?");
          strSQL.append(" AND ccb.cs_borrado = ?");
          strSQL.append(" AND cpi.cs_vobo_if = ?");
          strSQL.append(" AND cpi.cs_borrado = ?");
          strSQL.append(" AND pyme.cs_dscto_automatico = ?");
          strSQL.append(" AND epo.ic_banco_fondeo = ?");
          strSQL.append(" AND epo.ic_epo = ?");
          strSQL.append(" AND cif.ic_if = ?");
          strSQL.append(" AND conv.ic_version_convenio = ?");
          strSQL.append(" AND sol.df_operacion BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE (?, 'dd/mm/yyyy') + 1");
          strSQL.append(" GROUP BY epo.ic_epo, epo.cg_razon_social, cif.ic_if, cif.cg_razon_social, conv.cd_version_convenio");
          strSQL.append(" ORDER BY epo.cg_razon_social, cif.cg_razon_social, conv.cd_version_convenio");

          varBind.add(new Integer(4));
          varBind.add(new Integer(11));
          varBind.add(new Integer(12));
          varBind.add(new Integer(1));
          varBind.add("N");
          varBind.add("S");
          varBind.add("N");
          varBind.add("S");
          varBind.add(new Integer(banco_fondeo));
          varBind.add(new Long(clave_epo));
          varBind.add(new Long(clave_if));
          varBind.add(new Integer(ic_version_convenio));
          varBind.add(fecha_inicial);
          varBind.add(fecha_final);
          //LOG.debug("..:: strSQL : "+strSQL.toString());
          //LOG.debug("..:: varBind : "+varBind.toString());

          pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
          rst1 = pst1.executeQuery();

          while(rst1.next()){
            numero_doctos_ope_con_desc = rst1.getInt(1);
            monto_operado_con_desc = new BigDecimal(rst1.getString(2)==null?"0.00":rst1.getString(2));
          }

          rst1.close();
          pst1.close();

          //Se obtiene el total de pymes sin descuento para la relacion epo-if-convenio
          strSQL =  new StringBuffer();
          varBind = new ArrayList();

          strSQL.append(" SELECT COUNT(ccb.ic_pyme) numero_pymes_sd");
          strSQL.append(" FROM comrel_pyme_if cpi");
          strSQL.append(" ,comrel_cuenta_bancaria ccb");
          strSQL.append(" ,comcat_epo epo");
          strSQL.append(" ,comcat_if cif");
          strSQL.append(" ,comcat_version_convenio conv");
          strSQL.append(" ,comcat_pyme pyme");
          strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
          strSQL.append(" AND cpi.ic_epo = epo.ic_epo");
          strSQL.append(" AND cpi.ic_if = cif.ic_if");
          strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
          strSQL.append(" AND pyme.ic_version_convenio = conv.ic_version_convenio");
          strSQL.append(" AND ccb.ic_moneda = ?");
          strSQL.append(" AND ccb.cs_borrado = ?");
          strSQL.append(" AND cpi.cs_vobo_if = ?");
          strSQL.append(" AND cpi.cs_borrado = ?");
          strSQL.append(" AND pyme.cs_dscto_automatico = ?");
          strSQL.append(" AND epo.ic_banco_fondeo = ?");
          strSQL.append(" AND epo.ic_epo = ?");
          strSQL.append(" AND cif.ic_if = ?");
          strSQL.append(" AND conv.ic_version_convenio = ?");
          strSQL.append(" GROUP BY epo.ic_epo, epo.cg_razon_social, cif.ic_if, cif.cg_razon_social, conv.cd_version_convenio");
          strSQL.append(" ORDER BY epo.cg_razon_social, cif.cg_razon_social, conv.cd_version_convenio");

          varBind.add(new Integer(1));
          varBind.add("N");
          varBind.add("S");
          varBind.add("N");
          varBind.add("N");
          varBind.add(new Integer(banco_fondeo));
          varBind.add(new Long(clave_epo));
          varBind.add(new Long(clave_if));
          varBind.add(new Integer(ic_version_convenio));
          //LOG.debug("..:: strSQL : "+strSQL.toString());
          //LOG.debug("..:: varBind : "+varBind.toString());

          pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
          rst1 = pst1.executeQuery();

          while(rst1.next()){
            numero_pymes_sin_desc = rst1.getInt(1);
          }

          rst1.close();
          pst1.close();

          //Se obtiene el numero y monto de los documentos publicados a pymes sin descuento automatico
          strSQL =  new StringBuffer();
          varBind = new ArrayList();

          strSQL.append(" SELECT COUNT(docto.ic_documento) numero_doctos_psd, SUM(docto.fn_monto) monto_doctos_psd");
          strSQL.append(" FROM comrel_pyme_if cpi");
          strSQL.append(" ,comrel_cuenta_bancaria ccb");
          strSQL.append(" ,comcat_epo epo");
          strSQL.append(" ,comcat_if cif");
          strSQL.append(" ,comcat_version_convenio conv");
          strSQL.append(" ,comcat_pyme pyme");
          strSQL.append(" ,com_documento docto");
          strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
          strSQL.append(" AND cpi.ic_epo = epo.ic_epo");
          strSQL.append(" AND cpi.ic_if = cif.ic_if");
          strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
          strSQL.append(" AND pyme.ic_version_convenio = conv.ic_version_convenio");
          strSQL.append(" AND docto.ic_epo = epo.ic_epo");
          strSQL.append(" AND docto.ic_if = cif.ic_if");
          strSQL.append(" AND docto.ic_pyme = pyme.ic_pyme");
          //strSQL.append(" AND docto.ic_estatus_docto NOT IN (?, ?, ?)");
          strSQL.append(" AND ccb.ic_moneda = ?");
          strSQL.append(" AND ccb.cs_borrado = ?");
          strSQL.append(" AND cpi.cs_vobo_if = ?");
          strSQL.append(" AND cpi.cs_borrado = ?");
          strSQL.append(" AND pyme.cs_dscto_automatico = ?");
          strSQL.append(" AND epo.ic_banco_fondeo = ?");
          strSQL.append(" AND epo.ic_epo = ?");
          strSQL.append(" AND cif.ic_if = ?");
          strSQL.append(" AND conv.ic_version_convenio = ?");
          strSQL.append(" AND docto.df_alta BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE (?, 'dd/mm/yyyy') + 1");
          strSQL.append(" GROUP BY epo.ic_epo, epo.cg_razon_social, cif.ic_if, cif.cg_razon_social, conv.cd_version_convenio");
          strSQL.append(" ORDER BY epo.cg_razon_social, cif.cg_razon_social, conv.cd_version_convenio");

          //varBind.add(new Integer(4));
          //varBind.add(new Integer(11));
          //varBind.add(new Integer(12));
          varBind.add(new Integer(1));
          varBind.add("N");
          varBind.add("S");
          varBind.add("N");
          varBind.add("N");
          varBind.add(new Integer(banco_fondeo));
          varBind.add(new Long(clave_epo));
          varBind.add(new Long(clave_if));
          varBind.add(new Integer(ic_version_convenio));
          varBind.add(fecha_inicial);
          varBind.add(fecha_final);
          //LOG.debug("..:: strSQL : "+strSQL.toString());
          //LOG.debug("..:: varBind : "+varBind.toString());

          pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
          rst1 = pst1.executeQuery();

          while(rst1.next()){
            numero_doctos_pub_sin_desc = rst1.getInt(1);
            monto_publicado_sin_desc = new BigDecimal(rst1.getString(2)==null?"0.00":rst1.getString(2));
          }

          rst1.close();
          pst1.close();

          //Se obtiene el numero y monto de los documentos operados por pymes sin descuento automatico
          strSQL =  new StringBuffer();
          varBind = new ArrayList();

          strSQL.append(" SELECT COUNT(docto.ic_documento) numero_doctos_osd, SUM(docto.fn_monto) monto_doctos_osd");
          strSQL.append(" FROM comrel_pyme_if cpi");
          strSQL.append(" ,comrel_cuenta_bancaria ccb");
          strSQL.append(" ,comcat_epo epo");
          strSQL.append(" ,comcat_if cif");
          strSQL.append(" ,comcat_version_convenio conv");
          strSQL.append(" ,comcat_pyme pyme");
          strSQL.append(" ,com_documento docto");
          strSQL.append(" ,com_solicitud sol");
          strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
          strSQL.append(" AND cpi.ic_epo = epo.ic_epo");
          strSQL.append(" AND cpi.ic_if = cif.ic_if");
          strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
          strSQL.append(" AND pyme.ic_version_convenio = conv.ic_version_convenio");
          strSQL.append(" AND docto.ic_epo = epo.ic_epo");
          strSQL.append(" AND docto.ic_if = cif.ic_if");
          strSQL.append(" AND docto.ic_pyme = pyme.ic_pyme");
          strSQL.append(" AND docto.ic_documento = sol.ic_documento");
          strSQL.append(" AND epo.ic_banco_fondeo = sol.ic_banco_fondeo");
          strSQL.append(" AND docto.ic_estatus_docto IN (?, ?, ?)");
          strSQL.append(" AND ccb.ic_moneda = ?");
          strSQL.append(" AND ccb.cs_borrado = ?");
          strSQL.append(" AND cpi.cs_vobo_if = ?");
          strSQL.append(" AND cpi.cs_borrado = ?");
          strSQL.append(" AND pyme.cs_dscto_automatico = ?");
          strSQL.append(" AND epo.ic_banco_fondeo = ?");
          strSQL.append(" AND epo.ic_epo = ?");
          strSQL.append(" AND cif.ic_if = ?");
          strSQL.append(" AND conv.ic_version_convenio = ?");
          strSQL.append(" AND sol.df_operacion BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE (?, 'dd/mm/yyyy') + 1");
          strSQL.append(" GROUP BY epo.ic_epo, epo.cg_razon_social, cif.ic_if, cif.cg_razon_social, conv.cd_version_convenio");
          strSQL.append(" ORDER BY epo.cg_razon_social, cif.cg_razon_social, conv.cd_version_convenio");

          varBind.add(new Integer(4));
          varBind.add(new Integer(11));
          varBind.add(new Integer(12));
          varBind.add(new Integer(1));
          varBind.add("N");
          varBind.add("S");
          varBind.add("N");
          varBind.add("N");
          varBind.add(new Integer(banco_fondeo));
          varBind.add(new Long(clave_epo));
          varBind.add(new Long(clave_if));
          varBind.add(new Integer(ic_version_convenio));
          varBind.add(fecha_inicial);
          varBind.add(fecha_final);
          //LOG.debug("..:: strSQL : "+strSQL.toString());
          //LOG.debug("..:: varBind : "+varBind.toString());

          pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
          rst1 = pst1.executeQuery();

          while(rst1.next()){
            numero_doctos_ope_sin_desc = rst1.getInt(1);
            monto_operado_sin_desc = new BigDecimal(rst1.getString(2)==null?"0.00":rst1.getString(2));
          }

          rst1.close();
          pst1.close();

          //Se insertan los valores obtenidos en la tabla temporal
          strSQL =  new StringBuffer();

          strSQL.append(" INSERT INTO comtmp_rep_oper_desc_auto(ic_reporte_desc_auto, ic_epo, ic_if, cg_nombre_epo, cg_nombre_if, cg_convenio, in_pymes_afiliadas,");
          strSQL.append(" in_pymes_cdesc, in_doctos_cpub, fn_monto_cpub, in_doctos_cope, fn_monto_cope, in_pymes_sdesc, in_doctos_spub, fn_monto_spub,");
          strSQL.append(" in_doctos_sope, fn_monto_sope) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

          pst1 = con.queryPrecompilado(strSQL.toString());
          //LOG.debug("..:: strSQL : "+strSQL.toString());
          pst1.setInt(1, Integer.parseInt(hashDatosEntrada));
          pst1.setInt(2, Integer.parseInt(clave_epo));
          pst1.setInt(3, Integer.parseInt(clave_if));
          pst1.setString(4, nombre_epo);
          pst1.setString(5, nombre_if);
          pst1.setString(6, version_convenio);
          pst1.setInt(7, numero_pymes_afiliadas);
          pst1.setInt(8, numero_pymes_con_desc);
          pst1.setInt(9, numero_doctos_pub_con_desc);
          pst1.setDouble(10, Double.parseDouble(monto_publicado_con_desc.toString()));
          pst1.setInt(11, numero_doctos_ope_con_desc);
          pst1.setDouble(12, Double.parseDouble(monto_operado_con_desc.toString()));
          pst1.setInt(13, numero_pymes_sin_desc);
          pst1.setInt(14, numero_doctos_pub_sin_desc);
          pst1.setDouble(15, Double.parseDouble(monto_publicado_sin_desc.toString()));
          pst1.setInt(16, numero_doctos_ope_sin_desc);
          pst1.setDouble(17, Double.parseDouble(monto_operado_sin_desc.toString()));

          pst1.executeUpdate();

          pst1.close();

          operacionesDescuentoAutomatico.add(clave_epo);
          operacionesDescuentoAutomatico.add(clave_if);
          operacionesDescuentoAutomatico.add(nombre_epo);
          operacionesDescuentoAutomatico.add(nombre_if);
          operacionesDescuentoAutomatico.add(version_convenio);
          operacionesDescuentoAutomatico.add(Integer.toString(numero_pymes_afiliadas));
          operacionesDescuentoAutomatico.add(Integer.toString(numero_pymes_con_desc));
          operacionesDescuentoAutomatico.add(Integer.toString(numero_doctos_pub_con_desc));
          operacionesDescuentoAutomatico.add(monto_publicado_con_desc.toString());
          operacionesDescuentoAutomatico.add(Integer.toString(numero_doctos_ope_con_desc));
          operacionesDescuentoAutomatico.add(monto_operado_con_desc.toString());
          operacionesDescuentoAutomatico.add(Integer.toString(numero_pymes_sin_desc));
          operacionesDescuentoAutomatico.add(Integer.toString(numero_doctos_pub_sin_desc));
          operacionesDescuentoAutomatico.add(monto_publicado_sin_desc.toString());
          operacionesDescuentoAutomatico.add(Integer.toString(numero_doctos_ope_sin_desc));
          operacionesDescuentoAutomatico.add(monto_operado_sin_desc.toString());

          reporteOperacionesDescuentoAutomatico.add(operacionesDescuentoAutomatico);
        }
        rst.close();
        pst.close();
      } else {
        strSQL =  new StringBuffer();
        varBind = new ArrayList();

        strSQL.append(" SELECT /*+index(comtmp_rep_oper_desc_auto comtmp_rep_op_dsc_aut_01_nuk)+*/ ic_epo, ic_if, cg_nombre_epo, cg_nombre_if, cg_convenio, in_pymes_afiliadas,");
        strSQL.append(" in_pymes_cdesc, in_doctos_cpub, fn_monto_cpub, in_doctos_cope, fn_monto_cope, in_pymes_sdesc, in_doctos_spub, fn_monto_spub,");
        strSQL.append(" in_doctos_sope, fn_monto_sope FROM comtmp_rep_oper_desc_auto WHERE ic_reporte_desc_auto = ?");

        varBind.add(new Integer(hashDatosEntrada));
        //LOG.debug("..:: strSQL : "+strSQL.toString());
        //LOG.debug("..:: varBind : "+varBind.toString());
        pst = con.queryPrecompilado(strSQL.toString(), varBind);
        rst = pst.executeQuery();  

        while(rst.next()){
          List operacionesDescuentoAutomatico = new ArrayList();
          operacionesDescuentoAutomatico.add(rst.getString(1)==null?"":rst.getString(1));
          operacionesDescuentoAutomatico.add(rst.getString(2)==null?"":rst.getString(2));
          operacionesDescuentoAutomatico.add(rst.getString(3)==null?"":rst.getString(3));
          operacionesDescuentoAutomatico.add(rst.getString(4)==null?"":rst.getString(4));
          operacionesDescuentoAutomatico.add(rst.getString(5)==null?"":rst.getString(5));
          operacionesDescuentoAutomatico.add(rst.getString(6)==null?"":rst.getString(6));
          operacionesDescuentoAutomatico.add(rst.getString(7)==null?"":rst.getString(7));
          operacionesDescuentoAutomatico.add(rst.getString(8)==null?"":rst.getString(8));
          operacionesDescuentoAutomatico.add(rst.getString(9)==null?"":rst.getString(9));
          operacionesDescuentoAutomatico.add(rst.getString(10)==null?"":rst.getString(10));
          operacionesDescuentoAutomatico.add(rst.getString(11)==null?"":rst.getString(11));
          operacionesDescuentoAutomatico.add(rst.getString(12)==null?"":rst.getString(12));
          operacionesDescuentoAutomatico.add(rst.getString(13)==null?"":rst.getString(13));
          operacionesDescuentoAutomatico.add(rst.getString(14)==null?"":rst.getString(14));
          operacionesDescuentoAutomatico.add(rst.getString(15)==null?"":rst.getString(15));
          operacionesDescuentoAutomatico.add(rst.getString(16)==null?"":rst.getString(16));
          reporteOperacionesDescuentoAutomatico.add(operacionesDescuentoAutomatico);
        }

        rst.close();
        pst.close();
      }
    }catch(Exception e){
      operacion_exitosa = false;
      LOG.info("..:: reporteOperacionesDescuentoAutomatico(ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.terminaTransaccion(operacion_exitosa);
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteOperacionesDescuentoAutomatico(F)");
    return reporteOperacionesDescuentoAutomatico;
	} 
/**
 * M�todo obtiene las pymes que operanc con o sin de Descuento Autom�tico que con una EPO.
 * @param parametros_detalle Lista que contiene los parametros de entrada para generar el detalle
 *        los parametros vienen en el siguiente orden:
 *        0)  Cadena con el c�digo hash resultante obtenida a partir de los datos de entrada.
 *        1)  La clave del banco de fondeo con el que se realizaron las operaciones en cadenas
 *        2)  La clave de la EPO para la que se requiere generar el reporte, si no se especifica (cadena vac�a) el reporte se genera para todas las epos habilitadas.
 *        3)  La clave del IF para la que se requiere generar el reporte, si no se especifica (cadena vac�a) el reporte se genera para todas las ifs asociadas a la epo.
 *        4)  La clave de la versi�n de convenio asociado a las pymes.
 *        5)  La fecha de inicio del periodo en el que se genera el reporte.
 *        6)  La fecha de t�rmino del periodo en el que se genera el reporte.
 *        7)  Cadena para indicar con o sin descuento automatico, valores S (con DA) o N (sin DA)
 * @return reporteOperacionesCadenas Lista con la informaci�n del reporte.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 013 - 2009 Reportes
 * @author Alberto Cruz Flores
 */
	@Override
	public List obtenerPymesDescuentoAutomatico(List parametros_detalle) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List detallePymesConDescuentoAutomatico = new ArrayList();

    String banco_fondeo = (String)parametros_detalle.get(1);
    String ic_epo = (String)parametros_detalle.get(2);
    String ic_if = (String)parametros_detalle.get(3);
    String ic_version_convenio = (String)parametros_detalle.get(4);
    String cs_descuento = (String)parametros_detalle.get(7); 

    LOG.info("..:: obtenerPymesDescuentoAutomatico(I)");
    try {
      con.conexionDB();

      strSQL.append(" SELECT pyme.ic_pyme, crn.ic_nafin_electronico, pyme.cg_razon_social, pyme.cg_rfc");
      strSQL.append(" FROM comrel_pyme_if cpi");
      strSQL.append(" ,comrel_cuenta_bancaria ccb");
      strSQL.append(" ,comcat_epo epo");
      strSQL.append(" ,comcat_if cif");
      strSQL.append(" ,comcat_version_convenio conv");
      strSQL.append(" ,comcat_pyme pyme");
      strSQL.append(" ,comrel_nafin crn");
      strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
      strSQL.append(" AND cpi.ic_epo = epo.ic_epo");
      strSQL.append(" AND cpi.ic_if = cif.ic_if");
      strSQL.append(" AND ccb.ic_pyme = pyme.ic_pyme");
      strSQL.append(" AND pyme.ic_version_convenio = conv.ic_version_convenio");
      strSQL.append(" AND pyme.ic_pyme = crn.ic_epo_pyme_if");
      strSQL.append(" AND crn.cg_tipo = ?");
      strSQL.append(" AND ccb.ic_moneda = ?");
      strSQL.append(" AND ccb.cs_borrado = ?");
      strSQL.append(" AND cpi.cs_vobo_if = ?");
      strSQL.append(" AND cpi.cs_borrado = ?");
      strSQL.append(" AND pyme.cs_dscto_automatico = ?");
      strSQL.append(" AND epo.ic_banco_fondeo = ?");
      strSQL.append(" AND epo.ic_epo = ?");
      strSQL.append(" AND cif.ic_if = ?");
      strSQL.append(" AND conv.ic_version_convenio = ?");
      strSQL.append(" GROUP BY pyme.ic_pyme, crn.ic_nafin_electronico, pyme.cg_razon_social, pyme.cg_rfc");
      strSQL.append(" ORDER BY pyme.cg_razon_social");

      varBind.add("P");
      varBind.add(new Integer(1));
      varBind.add("N");
      varBind.add("S");
      varBind.add("N");
      varBind.add(cs_descuento);
      varBind.add(new Integer(banco_fondeo));
      varBind.add(new Long(ic_epo));
      varBind.add(new Long(ic_if));
      varBind.add(new Integer(ic_version_convenio));

      //LOG.debug("..:: strSQL : "+strSQL.toString());
      //LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List pymesConDescuentoAutomatico = new ArrayList();
        pymesConDescuentoAutomatico.add(rst.getString(2) == null?"":rst.getString(2));
        pymesConDescuentoAutomatico.add(rst.getString(3) == null?"":rst.getString(3));
        pymesConDescuentoAutomatico.add(rst.getString(4) == null?"":rst.getString(4));
        detallePymesConDescuentoAutomatico.add(pymesConDescuentoAutomatico);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: obtenerPymesDescuentoAutomatico(ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: obtenerPymesDescuentoAutomatico(F)");
    return detallePymesConDescuentoAutomatico;
	}

/**
 * M�todo obtiene el catalogo de bases de operaci�n parametrizadas para generar el combo en pantalla
 * de reporte de Cr�dito Electr�nico.
 * @param ic_if Cadena con la clave de if, si la cadena esta vacia se genera para todos los IF.
 * @return catalogoBaseOperacion Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 013 - 2009 Reportes
 * @author Alberto Cruz Flores
 */
	@Override
  public List obtenerCatalogoBaseOperacion(String ic_if) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List catalogoBaseOperacion = new ArrayList();

    LOG.info("..:: obtenerCatalogoBaseOperacion(I)");
    try {
      con.conexionDB();

      strSQL.append(" SELECT DISTINCT cbo.ig_codigo_base clave, cbo.ig_codigo_base ||' '|| cbo.cg_descripcion descripcion");
      strSQL.append(" FROM comcat_base_operacion cbo, com_base_op_credito boc");
      strSQL.append(" WHERE cbo.ig_codigo_base = boc.ig_codigo_base");
      if(!ic_if.equals("")){strSQL.append(" AND boc.ic_if = ?");}
      strSQL.append(" ORDER BY cbo.ig_codigo_base");

      if(!ic_if.equals("")){varBind.add(new Long(ic_if));}
      //LOG.debug("..:: strSQL : "+strSQL.toString());
      //LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(rst.getString("CLAVE"));
				elementoCatalogo.setDescripcion(rst.getString("DESCRIPCION"));
        catalogoBaseOperacion.add(elementoCatalogo);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: obtenerCatalogoBaseOperacion (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: obtenerCatalogoBaseOperacion(F)");
    return catalogoBaseOperacion;
	}

/**
 * M�todo obtiene el catalogo de IF parametrizadas para generar el combo en pantalla
 * de reporte de Cr�dito Electr�nico.
 * @param base_operacion Cadena con la clave de if, si la cadena esta vacia se genera para todos los IF.
 * @return catalogoBaseOperacion Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 013 - 2009 Reportes
 * @author Alberto Cruz Flores
 */
	@Override
  public List obtenerCatalogoIF(String base_operacion) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List catalogoIF = new ArrayList();

    LOG.info("..:: obtenerCatalogoBaseOperacion(I)");
    try {
      con.conexionDB();

      strSQL.append(" SELECT DISTINCT cif.ic_if clave, cif.ic_financiera, cif.ic_financiera ||' '|| cif.cg_razon_social descripcion");
      strSQL.append(" FROM comcat_if cif, com_base_op_credito boc");
      strSQL.append(" WHERE boc.ic_if = cif.ic_if");
      if(!base_operacion.equals("")){strSQL.append(" AND boc.ig_codigo_base = ?");}
      strSQL.append(" ORDER BY cif.ic_financiera");

      if(!base_operacion.equals("")){varBind.add(new Long(base_operacion));}
      //LOG.debug("..:: strSQL : "+strSQL.toString());
      //LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(rst.getString("CLAVE"));
				elementoCatalogo.setDescripcion(rst.getString("DESCRIPCION"));
        catalogoIF.add(elementoCatalogo);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: obtenerCatalogoBaseOperacion (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: obtenerCatalogoBaseOperacion(F)");
    return catalogoIF;
	}

/**
 * M�todo obtiene el catalogo de tipo de credito parametrizadas por un IF o una Base de Operacion para generar el combo en pantalla
 * de reporte de Cr�dito Electr�nico. Este combo se genera ya se a a partir de la clave de IF o la clave de Base de Operacion, nunca con ambas.
 * @param ic_if Cadena con la clave de if para la que se obtienen los tipos de credito.
 * @param base_operacion Cadena con la clave de la base de oepracion para la que se obtienen los tipos de credito.
 * @return catalogoTipoCredito Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 013 - 2009 Reportes
 * @author Alberto Cruz Flores
 */
	@Override
  public List obtenerCatalogoTipoCredito(String ic_if, String base_operacion) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List catalogoTipoCredito = new ArrayList();

    LOG.info("..:: obtenerCatalogoTipoCredito(I)");
    try {
      con.conexionDB();

      strSQL.append(" SELECT DISTINCT ctc.ic_tipo_credito clave, ctc.ic_tipo_credito ||' '|| ctc.cd_descripcion descripcion");
      strSQL.append(" FROM comcat_tipo_credito ctc, com_base_op_credito boc");
      strSQL.append(" WHERE boc.ic_tipo_credito = ctc.ic_tipo_credito");
      if(!ic_if.equals("")){strSQL.append(" AND boc.ic_if = ?");}
      if(!base_operacion.equals("")){strSQL.append(" AND boc.ig_codigo_base = ?");}
      strSQL.append(" ORDER BY ctc.ic_tipo_credito");

      if(!ic_if.equals("")){varBind.add(new Long(ic_if));}
      if(!base_operacion.equals("")){varBind.add(new Long(base_operacion));}
      //LOG.debug("..:: strSQL : "+strSQL.toString());
      //LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(rst.getString("CLAVE"));
				elementoCatalogo.setDescripcion(rst.getString("DESCRIPCION"));
        catalogoTipoCredito.add(elementoCatalogo);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: obtenerCatalogoTipoCredito (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: obtenerCatalogoTipoCredito(F)");
    return catalogoTipoCredito;
	}

/**
 * M�todo que genera el reporte de Cr�dito Electr�nico a partir de la Base de Operaci�n.
 * @param parametros_reporte Lista con los siguientes parametros:
 *        0) base_operacion Base de operaci�n para la que se quiere obtener el reporte de credito.
 *        1) clave_if Clave del IF que tiene esa base de operacion
 *        2) tipo_credito Tipo de credito parametrizado para el IF.
 *        3) clave_estado Clave del estado del pa�s al que pertenece un acreditado.
 *        4) fecha_ini Fecha inicial del periodo a reportar.
 *        5) fecha_fin Fecha final del periodo a reportar.
 * @return catalogoTipoCredito Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 013 - 2009 Reportes
 * @author Alberto Cruz Flores
 */
	@Override
  public List reporteCreditoElectronicoBO(List parametros_reporte) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteCreditoElectronicoBO = new ArrayList();

    LOG.info("..:: reporteCreditoElectronicoBO(I)");
    try {
      con.conexionDB();

      String base_operacion = (String)parametros_reporte.get(0);
      String clave_if = (String)parametros_reporte.get(1);
      String tipo_credito = (String)parametros_reporte.get(2);
      String clave_estado = (String)parametros_reporte.get(3);
      String fecha_ini = (String)parametros_reporte.get(4);
      String fecha_fin = (String)parametros_reporte.get(5);
      String reporte_credito = (String)parametros_reporte.get(6);

      if(reporte_credito.equals("BASE_OPER")){
        strSQL.append(" SELECT /*+index(csp in_com_solic_portal_04_nuk) index(boc cp_com_base_op_credito_pk)*/ cbo.ig_codigo_base || ' ' || cbo.cg_descripcion base_operacion,");
        strSQL.append(" cif.cg_razon_social intermediario_financiero,");
      }else{
        strSQL.append(" SELECT /*+index(csp in_com_solic_portal_04_nuk) index(boc cp_com_base_op_credito_pk)*/ cif.cg_razon_social intermediario_financiero,");
        strSQL.append(" cbo.ig_codigo_base || ' ' || cbo.cg_descripcion base_operacion,");
      }

      strSQL.append(" ctc.ic_tipo_credito ||' - '|| ctc.cd_descripcion tipo_credito,");
      strSQL.append(" SUM(csp.fn_importe_docto) monto_solicitado,");
      strSQL.append(" SUM(csp.fn_importe_dscto) monto_descuento,");
      strSQL.append(" edo.ic_estado ||' - '|| edo.cd_nombre estado_acreditado,");
      strSQL.append(" cbo.ig_codigo_base ig_codigo_base,");
      strSQL.append(" cif.ic_if ic_if,");
      strSQL.append(" ctc.ic_tipo_credito ic_tipo_credito,");
      strSQL.append(" edo.ic_estado ic_estado");
      strSQL.append(" FROM com_solic_portal csp,");
      strSQL.append(" comcat_base_operacion cbo,");
      //strSQL.append(" com_base_op_credito boc,");
      strSQL.append(" comcat_if cif,");
      strSQL.append(" comcat_tipo_credito ctc,");
      strSQL.append(" comcat_estado edo");
      strSQL.append(" WHERE csp.ig_base_operacion = cbo.ig_codigo_base");
      strSQL.append(" AND csp.ic_if = cif.ic_if ");
      strSQL.append(" AND csp.ic_tipo_credito = ctc.ic_tipo_credito");
      strSQL.append(" AND csp.ic_estado = edo.ic_estado");
      //strSQL.append(" AND csp.ic_if = boc.ic_if");
      //strSQL.append(" and csp.ig_base_operacion = boc.ig_codigo_base");
      strSQL.append(" AND edo.ic_pais = ?");
      if(!base_operacion.equals("")){strSQL.append(" AND csp.ig_base_operacion = ?");}
      if(!clave_if.equals("")){strSQL.append(" AND csp.ic_if = ?");}
      if(!tipo_credito.equals("")){strSQL.append(" AND csp.ic_tipo_credito = ?");}
      if(!clave_estado.equals("")){strSQL.append(" AND csp.ic_estado = ?");}
      strSQL.append(" AND csp.df_carga >= TO_DATE(?, 'dd/mm/yyyy')");
		strSQL.append(" AND csp.df_carga < TO_DATE(?, 'dd/mm/yyyy') + 1");

      if(reporte_credito.equals("BASE_OPER")){
        strSQL.append(" GROUP BY cbo.ig_codigo_base,");
        strSQL.append(" cbo.cg_descripcion,");
        strSQL.append(" cif.ic_if,");
        strSQL.append(" cif.cg_razon_social,");
        strSQL.append(" ctc.ic_tipo_credito,");
        strSQL.append(" ctc.cd_descripcion,");
        strSQL.append(" edo.ic_estado,");
        strSQL.append(" edo.cd_nombre");
        strSQL.append(" ORDER BY cbo.cg_descripcion, cif.cg_razon_social, ctc.cd_descripcion, edo.cd_nombre");
      }else{
        strSQL.append(" GROUP BY cif.ic_if,");
        strSQL.append(" cif.cg_razon_social,");
        strSQL.append(" cbo.ig_codigo_base,");
        strSQL.append(" cbo.cg_descripcion,");
        strSQL.append(" ctc.ic_tipo_credito,");
        strSQL.append(" ctc.cd_descripcion,");
        strSQL.append(" edo.ic_estado,");
        strSQL.append(" edo.cd_nombre");
        strSQL.append(" ORDER BY cif.cg_razon_social, cbo.cg_descripcion, ctc.cd_descripcion, edo.cd_nombre");
      }

      varBind.add(new Integer(24));
      if(!base_operacion.equals("")){varBind.add(new Long(base_operacion));}
      if(!clave_if.equals("")){varBind.add(new Long(clave_if));}
      if(!tipo_credito.equals("")){varBind.add(new Integer(tipo_credito));}
      if(!clave_estado.equals("")){varBind.add(new Integer(clave_estado));}
      varBind.add(fecha_ini);
      varBind.add(fecha_fin);
      //LOG.debug("..:: strSQL : "+strSQL.toString());
      //LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List registroReporteCredito = new ArrayList();
        registroReporteCredito.add(rst.getString(1)==null?"":rst.getString(1));
        registroReporteCredito.add(rst.getString(2)==null?"":rst.getString(2));
        registroReporteCredito.add(rst.getString(3)==null?"":rst.getString(3));
        registroReporteCredito.add(rst.getString(4)==null?"":rst.getString(4));
        registroReporteCredito.add(rst.getString(5)==null?"":rst.getString(5));
        registroReporteCredito.add(rst.getString(6)==null?"":rst.getString(6));
        //registroReporteCredito.add(rst.getString(7)==null?"":rst.getString(7));

        String ig_codigo_base = rst.getString(7)==null?"":rst.getString(7);
        String ic_if = rst.getString(8)==null?"":rst.getString(8);
        String ic_tipo_credito = rst.getString(9)==null?"":rst.getString(9);
        String ic_estado = rst.getString(10)==null?"":rst.getString(10);

        strSQL =  new StringBuffer();
        varBind = new ArrayList();

        strSQL.append(" SELECT COUNT(tabla_pf.clave_sirac) FROM (");
        strSQL.append(" SELECT /*+index(csp in_com_solic_portal_04_nuk)*/ DISTINCT csp.ig_clave_sirac clave_sirac");
        strSQL.append(" FROM com_solic_portal csp,");
        strSQL.append(" comcat_base_operacion cbo,");
        strSQL.append(" comcat_if cif,");
        strSQL.append(" comcat_tipo_credito ctc,");
        strSQL.append(" comcat_estado edo");
        strSQL.append(" WHERE csp.ig_base_operacion = cbo.ig_codigo_base");
        strSQL.append(" AND csp.ic_if = cif.ic_if ");
        strSQL.append(" AND csp.ic_tipo_credito = ctc.ic_tipo_credito");
        strSQL.append(" AND csp.ic_estado = edo.ic_estado");
        strSQL.append(" AND edo.ic_pais = ?");
        if(!ig_codigo_base.equals("")){strSQL.append(" AND csp.ig_base_operacion = ?");}
        if(!ic_if.equals("")){strSQL.append(" AND csp.ic_if = ?");}
        if(!ic_tipo_credito.equals("")){strSQL.append(" AND csp.ic_tipo_credito = ?");}
        if(!ic_estado.equals("")){strSQL.append(" AND csp.ic_estado = ?");}
		  strSQL.append(" AND csp.df_carga >= TO_DATE(?, 'dd/mm/yyyy')");
		  strSQL.append(" AND csp.df_carga < TO_DATE(?, 'dd/mm/yyyy') + 1");
        strSQL.append(" AND csp.cs_tipo_persona = ?");
        strSQL.append(") tabla_pf");

        varBind.add(new Integer(24));
        if(!ig_codigo_base.equals("")){varBind.add(new Long(ig_codigo_base));}
        if(!ic_if.equals("")){varBind.add(new Long(ic_if));}
        if(!ic_tipo_credito.equals("")){varBind.add(new Integer(ic_tipo_credito));}
        if(!ic_estado.equals("")){varBind.add(new Integer(ic_estado));}
        varBind.add(fecha_ini);
        varBind.add(fecha_fin);
        varBind.add("F");
        //LOG.debug("..:: strSQL : "+strSQL.toString());
        //LOG.debug("..:: varBind : "+varBind.toString());
        PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
        ResultSet rst1 = pst1.executeQuery();

        int personas_fisicas = 0;

        while(rst1.next()){
          personas_fisicas = rst1.getInt(1);
          registroReporteCredito.add(rst1.getString(1));
        }

        rst1.close();
        pst1.close();

        strSQL =  new StringBuffer();
        varBind = new ArrayList();

        strSQL.append(" SELECT COUNT(tabla_pm.clave_sirac) FROM (");
        strSQL.append(" SELECT /*+index(csp in_com_solic_portal_04_nuk)*/ DISTINCT csp.ig_clave_sirac clave_sirac");
        strSQL.append(" FROM com_solic_portal csp,");
        strSQL.append(" comcat_base_operacion cbo,");
        strSQL.append(" comcat_if cif,");
        strSQL.append(" comcat_tipo_credito ctc,");
        strSQL.append(" comcat_estado edo");
        strSQL.append(" WHERE csp.ig_base_operacion = cbo.ig_codigo_base");
        strSQL.append(" AND csp.ic_if = cif.ic_if ");
        strSQL.append(" AND csp.ic_tipo_credito = ctc.ic_tipo_credito");
        strSQL.append(" AND csp.ic_estado = edo.ic_estado");
        strSQL.append(" AND edo.ic_pais = ?");
        if(!ig_codigo_base.equals("")){strSQL.append(" AND csp.ig_base_operacion = ?");}
        if(!ic_if.equals("")){strSQL.append(" AND csp.ic_if = ?");}
        if(!ic_tipo_credito.equals("")){strSQL.append(" AND csp.ic_tipo_credito = ?");}
        if(!ic_estado.equals("")){strSQL.append(" AND csp.ic_estado = ?");}
        strSQL.append(" AND csp.df_carga >= TO_DATE(?, 'dd/mm/yyyy')");
		  strSQL.append(" AND csp.df_carga < TO_DATE(?, 'dd/mm/yyyy') + 1");
        strSQL.append(" AND csp.cs_tipo_persona = ?");
        strSQL.append(") tabla_pm");

        varBind.add(new Integer(24));
        if(!ig_codigo_base.equals("")){varBind.add(new Long(ig_codigo_base));}
        if(!ic_if.equals("")){varBind.add(new Long(ic_if));}
        if(!ic_tipo_credito.equals("")){varBind.add(new Integer(ic_tipo_credito));}
        if(!ic_estado.equals("")){varBind.add(new Integer(ic_estado));}
        varBind.add(fecha_ini);
        varBind.add(fecha_fin);
        varBind.add("M");
        //LOG.debug("..:: strSQL : "+strSQL.toString());
        //LOG.debug("..:: varBind : "+varBind.toString());
        pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
        rst1 = pst1.executeQuery();

        int personas_morales = 0;

        while(rst1.next()){
          personas_morales = rst1.getInt(1);
          registroReporteCredito.add(rst1.getString(1));
        }

        rst1.close();
        pst1.close();

        int total_registros = personas_fisicas + personas_morales;

        registroReporteCredito.add(Integer.toString(total_registros));
        registroReporteCredito.add(ig_codigo_base);
        registroReporteCredito.add(ic_if);
        registroReporteCredito.add(ic_tipo_credito);
        registroReporteCredito.add(ic_estado);

        reporteCreditoElectronicoBO.add(registroReporteCredito);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: reporteCreditoElectronicoBO (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteCreditoElectronicoBO(F)");
    return reporteCreditoElectronicoBO;
	}

/**
 * M�todo que genera el reporte de Cr�dito Electr�nico a partir de la Base de Operaci�n.
 * @param parametros_reporte Lista con los siguientes parametros:
 *        0) base_operacion Base de operaci�n para la que se quiere obtener el reporte de credito.
 *        1) clave_if Clave del IF que tiene esa base de operacion
 *        2) tipo_credito Tipo de credito parametrizado para el IF.
 *        3) clave_estado Clave del estado del pa�s al que pertenece un acreditado.
 *        4) fecha_ini Fecha inicial del periodo a reportar.
 *        5) fecha_fin Fecha final del periodo a reportar.
 * @return catalogoTipoCredito Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 013 - 2009 Reportes
 * @author Alberto Cruz Flores
 */
	@Override
  public List obtenerDetalleCreditoElectronico(List parametros_reporte) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List detalleCreditoElectronico = new ArrayList();

    LOG.info("..:: obtenerDetalleCreditoElectronico(I)");
    try {
      con.conexionDB();

      String base_operacion = (String)parametros_reporte.get(0);
      String clave_if = (String)parametros_reporte.get(1);
      String tipo_credito = (String)parametros_reporte.get(2);
      String clave_estado = (String)parametros_reporte.get(3);
      String fecha_ini = (String)parametros_reporte.get(4);
      String fecha_fin = (String)parametros_reporte.get(5);
      String tipo_persona = (String)parametros_reporte.get(6);

      strSQL.append(" SELECT DISTINCT csp.ig_clave_sirac clave_sirac");
      strSQL.append(" FROM com_solic_portal csp,");
      strSQL.append(" comcat_base_operacion cbo,");
      strSQL.append(" comcat_if cif,");
      strSQL.append(" comcat_tipo_credito ctc,");
      strSQL.append(" comcat_estado edo");
      strSQL.append(" WHERE csp.ig_base_operacion = cbo.ig_codigo_base");
      strSQL.append(" AND csp.ic_if = cif.ic_if ");
      strSQL.append(" AND csp.ic_tipo_credito = ctc.ic_tipo_credito");
      strSQL.append(" AND csp.ic_estado = edo.ic_estado");
      strSQL.append(" AND edo.ic_pais = ?");
      strSQL.append(" AND csp.ig_base_operacion = ?");
      strSQL.append(" AND csp.ic_if = ?");
      strSQL.append(" AND csp.ic_tipo_credito = ?");
      strSQL.append(" AND csp.ic_estado = ?");
      strSQL.append(" AND csp.df_carga BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') + 1");
      strSQL.append(" AND csp.cs_tipo_persona = ?");

      varBind.add(new Integer(24));
      varBind.add(new Long(base_operacion));
      varBind.add(new Long(clave_if));
      varBind.add(new Integer(tipo_credito));
      varBind.add(new Integer(clave_estado));
      varBind.add(fecha_ini);
      varBind.add(fecha_fin);
      if(tipo_persona.equals("F")){varBind.add("F");}
      else{varBind.add("M");}
      //LOG.debug("..:: strSQL : "+strSQL.toString());
      //LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        String clave_sirac = rst.getString(1)==null?"":rst.getString(1);

        strSQL =  new StringBuffer();
        varBind = new ArrayList();

        strSQL.append(" SELECT pym.in_numero_sirac, pym.cg_rfc, pym.cg_razon_social, edo.ic_estado || ' - ' || edo.cd_nombre, mun.ic_municipio || ' - ' || mun.cd_nombre");
        strSQL.append(" FROM comcat_pyme pym, com_domicilio dom, comcat_estado edo, comcat_municipio mun");
        strSQL.append(" WHERE dom.ic_pyme = pym.ic_pyme");
        strSQL.append(" AND dom.ic_estado = edo.ic_estado");
        strSQL.append(" AND dom.ic_municipio = mun.ic_municipio");
        strSQL.append(" AND mun.ic_estado = edo.ic_estado");
        strSQL.append(" AND edo.ic_pais = ?");
        strSQL.append(" AND dom.cs_fiscal = ?");
        strSQL.append(" AND pym.in_numero_sirac = ?");
        strSQL.append(" ORDER BY pym.cg_razon_social");

        varBind.add(new Integer(24));
        varBind.add("S");
        varBind.add(new Long(clave_sirac));
        //LOG.debug("..:: strSQL : "+strSQL.toString());
        //LOG.debug("..:: varBind : "+varBind.toString());
        PreparedStatement pst1 = con.queryPrecompilado(strSQL.toString(), varBind);
        ResultSet rst1 = pst1.executeQuery();

        while(rst1.next()){
          List registroReporteCredito = new ArrayList();
          registroReporteCredito.add(rst1.getString(1)==null?"":rst1.getString(1));
          registroReporteCredito.add(rst1.getString(2)==null?"":rst1.getString(2));
          registroReporteCredito.add(rst1.getString(3)==null?"":rst1.getString(3));
          registroReporteCredito.add(rst1.getString(4)==null?"":rst1.getString(4));
          registroReporteCredito.add(rst1.getString(5)==null?"":rst1.getString(5));
          detalleCreditoElectronico.add(registroReporteCredito);
        }

        rst1.close();
        pst1.close();
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: obtenerDetalleCreditoElectronico (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: obtenerDetalleCreditoElectronico(F)");
    return detalleCreditoElectronico;
	}


/**
	 * M�todo que genera el Resumen del reporte de Clientes Registrados de Nafinsa Movil a partir de la fecha de promocion.
 * @param
 *        1) num_nae Numero de Nafin Electronico de la Pyme.
	 * 2) claveAnioI Anio inicial de promocion.
	 * 3) claveAnioF Anio final de promocion.
	 * 4) claveMesI Mes inicial de promocion.
	 * 5) claveMesF Mes final de promocion.
 * @return reporteClientesRegNafinsaMovil Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 052 - 2010 Reportes Nafinsa M�vil
	 * @author Janneth Rebeca Salda�a Waldo
	 */
	@Override
  public List reporteClientesRegNafinsaMovil(String num_nae, String claveAnioI, String claveAnioF, String claveMesI, String claveMesF, String claveAnioC, String claveMesC) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteClientesRegNafinsaMovil = new ArrayList();

		SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calI = Calendar.getInstance();
		Calendar calF = Calendar.getInstance();
		calI.set(Integer.parseInt(claveAnioC), Integer.parseInt(claveMesC), 1);
		calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), 1);
		calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), calF.getActualMaximum(Calendar.DAY_OF_MONTH));

		String fecha_registro_ini = sdfFecha.format(calI.getTime());
		String fecha_registro_fin = sdfFecha.format(calF.getTime());

    LOG.info("..:: reporteClientesRegNafinsaMovil(I)");
    try {
      con.conexionDB();

      strSQL.append(" SELECT   afiliadas.anio, afiliadas.mes, afiliadas.afiliadas, ");
      strSQL.append("          habilitadas.habilitadas, altas.altas, inactivas.inactivas, afiliadas.nummes ");
      strSQL.append("     FROM (SELECT   TO_CHAR (df_carta_bajo, 'yyyy') anio, ");
      strSQL.append("                    CASE ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 1 THEN 'ENERO' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 2 THEN 'FEBRERO' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 3 THEN 'MARZO' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 4 THEN 'ABRIL' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 5 THEN 'MAYO' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 6 THEN 'JUNIO' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 7 THEN 'JULIO' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 8 THEN 'AGOSTO' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 9 THEN 'SEPTIEMBRE' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 10 THEN 'OCTUBRE' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 11 THEN 'NOVIEMBRE' ");
      strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 12 THEN 'DICIEMBRE' ");
      strSQL.append("                    END AS mes, ");
      strSQL.append("                    TO_CHAR (df_carta_bajo, 'mm') nummes, ");
      strSQL.append("                    COUNT (DISTINCT ic_pyme) afiliadas ");
      strSQL.append("               FROM comrel_pyme_epo ");
      strSQL.append("              WHERE cs_aceptacion != ? ");
      strSQL.append("                AND df_carta_bajo >= TO_DATE (?, 'dd/mm/yyyy') ");
      strSQL.append("                AND df_carta_bajo < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
      strSQL.append("           GROUP BY TO_CHAR (df_carta_bajo, 'yyyy'), ");
      strSQL.append("                    TO_CHAR (df_carta_bajo, 'MONTH'), ");
      strSQL.append("                    TO_CHAR (df_carta_bajo, 'mm')) afiliadas, ");
      strSQL.append("          (SELECT   TO_CHAR (df_aceptacion, 'yyyy') anio, ");
      strSQL.append("                    TO_CHAR (df_aceptacion, 'mm') nummes, ");
      strSQL.append("                    COUNT (DISTINCT ic_pyme) habilitadas ");
      strSQL.append("               FROM comrel_pyme_epo ");
      strSQL.append("              WHERE cs_aceptacion = ? ");
      strSQL.append("                AND df_aceptacion >= TO_DATE (?, 'dd/mm/yyyy') ");
      strSQL.append("                AND df_aceptacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
      strSQL.append("           GROUP BY TO_CHAR (df_aceptacion, 'yyyy'), ");
      strSQL.append("                    TO_CHAR (df_aceptacion, 'MONTH'), ");
      strSQL.append("                    TO_CHAR (df_aceptacion, 'mm')) habilitadas, ");
      strSQL.append("          (SELECT   TO_CHAR (df_activacion, 'yyyy') anio, ");
      strSQL.append("                    TO_CHAR (df_activacion, 'mm') nummes, ");
      strSQL.append("                    COUNT (df_activacion) altas ");
      strSQL.append("               FROM com_promo_pyme ");
      strSQL.append("              WHERE df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
      strSQL.append("                AND df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
      strSQL.append("                AND (   df_activacion > df_desactivacion ");
      strSQL.append("                     OR df_desactivacion IS NULL ");
      strSQL.append("                    ) ");
      strSQL.append("           GROUP BY TO_CHAR (df_activacion, 'yyyy'), ");
      strSQL.append("                    TO_CHAR (df_activacion, 'mm')) altas, ");
      strSQL.append("          (SELECT   TO_CHAR (df_desactivacion, 'yyyy') anio, ");
      strSQL.append("                    TO_CHAR (df_desactivacion, 'mm') nummes, ");
      strSQL.append("                    COUNT (df_desactivacion) inactivas ");
      strSQL.append("               FROM com_promo_pyme ");
      strSQL.append("              WHERE df_desactivacion >= TO_DATE (?, 'dd/mm/yyyy') ");
      strSQL.append("                AND df_desactivacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
      strSQL.append("                AND df_desactivacion > df_activacion ");
      strSQL.append("           GROUP BY TO_CHAR (df_desactivacion, 'yyyy'), ");
      strSQL.append("                    TO_CHAR (df_desactivacion, 'mm')) inactivas ");
      strSQL.append("    WHERE afiliadas.anio = habilitadas.anio(+) ");
      strSQL.append("      AND afiliadas.nummes = habilitadas.nummes(+) ");
      strSQL.append("      AND afiliadas.anio = altas.anio(+) ");
      strSQL.append("      AND afiliadas.nummes = altas.nummes(+) ");
      strSQL.append("      AND afiliadas.anio = inactivas.anio(+) ");
      strSQL.append("      AND afiliadas.nummes = inactivas.nummes(+) ");
      strSQL.append(" ORDER BY afiliadas.anio, afiliadas.nummes ");
		
		varBind.add("H");
		varBind.add(fecha_registro_ini);
		varBind.add(fecha_registro_fin);
		varBind.add("H");
		varBind.add(fecha_registro_ini);
		varBind.add(fecha_registro_fin);
		varBind.add(fecha_registro_ini);
		varBind.add(fecha_registro_fin);
		varBind.add(fecha_registro_ini);
		varBind.add(fecha_registro_fin);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List registroReporteClientesReg = new ArrayList();
        registroReporteClientesReg.add(rst.getString(1)==null?"":rst.getString(1));
        registroReporteClientesReg.add(rst.getString(2)==null?"":rst.getString(2));
        registroReporteClientesReg.add(rst.getString(3)==null?"":rst.getString(3));
        registroReporteClientesReg.add(rst.getString(4)==null?"":rst.getString(4));
        registroReporteClientesReg.add(rst.getString(5)==null?"":rst.getString(5));
        registroReporteClientesReg.add(rst.getString(6)==null?"":rst.getString(6));
				registroReporteClientesReg.add(rst.getString(7)==null?"":rst.getString(7));
        reporteClientesRegNafinsaMovil.add(registroReporteClientesReg);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: reporteClientesRegNafinsaMovil (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteClientesRegNafinsaMovil(F)");
    return reporteClientesRegNafinsaMovil;
	}

/**
	 * M�todo que genera el reporte de Clientes Registrados de Nafinsa Movil a partir de la fecha de promocion.
 * @param
 *        1) anioDetalle anio de la promoci�n.
 *        2) numMes Mes en n�mero de la promoci�n.
	 * @return clientesNafinsaMovilDetalle Lista con la informaci�n del catalogo.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 052 - 2010 Reportes Nafinsa M�vil
	 * @author Janneth Rebeca Salda�a Waldo
	 */
	@Override
  public List clientesNafinsaMovilDetalle(String anioDetalle, String numMes) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null, ps = null;
    ResultSet rst = null, rs = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List clientesNafinsaMovilDetalle = new ArrayList();

		/*SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calIA = Calendar.getInstance();
		Calendar calFA = Calendar.getInstance();
		Calendar calIM = Calendar.getInstance();
		Calendar calFM = Calendar.getInstance();

		calIA.set(Integer.parseInt("2008"), Integer.parseInt("0"), 1);
		calFA.set(Integer.parseInt(anioDetalle), Integer.parseInt(numMes)-2, calFA.getActualMaximum(calFA.DAY_OF_MONTH)-1);

		calIM.set(Integer.parseInt(anioDetalle), Integer.parseInt(numMes)-1, 1);
		calFM.set(Integer.parseInt(anioDetalle), Integer.parseInt(numMes)-1, calFM.getMaximum(calFM.DAY_OF_MONTH)-1);*/

		String fecha_ini_act = "01/01/2008";
//		String fecha_fin_act = sdfFecha.format(calFA.getTime());
		String fecha_ini_mes = "01/" + numMes + "/" + anioDetalle;
//		String fecha_fin_mes = sdfFecha.format(calFM.getTime());

		//LOG.info("..:: fecha_ini_act: " + fecha_ini_act + ", fecha_fin_act: " + fecha_fin_act + ", fecha_ini_mes: " + fecha_ini_mes + ", fecha_fin_mes: " + fecha_fin_mes);

    LOG.info("..:: clientesNafinsaMovilDetalle(I)");
    try {
      con.conexionDB();

			strSQL.append("SELECT rel.ic_nafin_electronico no_nafin, ");
			strSQL.append("			  rel.ic_epo_pyme_if no_pyme, ");
			strSQL.append("       pym.cg_razon_social razon_social, ");
			strSQL.append("       con.cg_email email, ");
			strSQL.append("       con.cg_celular no_celular, ");
			strSQL.append("       TO_CHAR (promo.df_promocion, 'dd/mm/yyyy') fecha_registro, ");
			strSQL.append("       TO_CHAR (promo.df_desactivacion, 'dd/mm/yyyy') fecha_desactivacion, ");
			strSQL.append("       TO_CHAR (promo.df_activacion, 'dd/mm/yyyy') fecha_activacion, ");
			strSQL.append("       NVL (afiliadas.total, 0) epos_afiliadas, ");
			strSQL.append("       NVL (noafiliadas.total, 0) epos_no_afiliadas, ");
			strSQL.append("       ce.cd_nombre estado, ");
			strSQL.append("       CASE ");
			strSQL.append("              WHEN promo.df_desactivacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("              AND promo.df_desactivacion < ADD_MONTHS( TO_DATE (?, 'dd/mm/yyyy'), 1) ");
			strSQL.append("              AND promo.df_desactivacion > promo.df_activacion ");
			strSQL.append("                 THEN 'BAJA' ");
			strSQL.append("              WHEN promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("              AND promo.df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
      strSQL.append("       			 AND (promo.df_activacion > promo.df_desactivacion OR promo.df_desactivacion IS NULL) ");
			strSQL.append("              		THEN 'ACTIVA' ");
			strSQL.append("              WHEN promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("              AND promo.df_activacion < ADD_MONTHS( TO_DATE (?, 'dd/mm/yyyy'), 1) ");
      strSQL.append("       			 AND (promo.df_activacion > promo.df_desactivacion OR promo.df_desactivacion IS NULL) ");
			strSQL.append("                 THEN 'ALTA' ");
			strSQL.append("       END AS estatus ");
			strSQL.append("  FROM com_promo_pyme promo, ");
			strSQL.append("       comrel_nafin rel, ");
			strSQL.append("       comcat_pyme pym, ");
			strSQL.append("       com_contacto con, ");
			strSQL.append("       com_domicilio cd, ");
			strSQL.append("       comcat_estado ce, ");
			strSQL.append("       (SELECT   COUNT (1) total, relpe.ic_pyme ");
			strSQL.append("            FROM comrel_pyme_epo relpe, comrel_nafin relna ");
			strSQL.append("           WHERE relpe.cs_habilitado = ? ");
			strSQL.append("             AND relpe.cs_aceptacion = ? ");
			strSQL.append("             AND relpe.ic_pyme = relna.ic_epo_pyme_if ");
			strSQL.append("        GROUP BY relpe.ic_pyme) afiliadas, ");
			strSQL.append("       (SELECT   COUNT (1) total, relpe.ic_pyme ");
			strSQL.append("            FROM comrel_pyme_epo relpe, comrel_nafin relna ");
			strSQL.append("           WHERE relpe.cs_habilitado = ? ");
			strSQL.append("             AND relpe.cs_aceptacion != ? ");
			strSQL.append("             AND relpe.ic_pyme = relna.ic_epo_pyme_if ");
			strSQL.append("        GROUP BY relpe.ic_pyme) noafiliadas ");
			strSQL.append(" WHERE promo.ic_pyme = rel.ic_epo_pyme_if ");
			strSQL.append("   AND rel.cg_tipo = ? ");
			strSQL.append("   AND promo.ic_pyme = pym.ic_pyme ");
			strSQL.append("   AND promo.ic_pyme = con.ic_pyme ");
			strSQL.append("   AND con.cs_primer_contacto = ? ");
			strSQL.append("   AND pym.ic_pyme = afiliadas.ic_pyme(+) ");
			strSQL.append("   AND pym.ic_pyme = noafiliadas.ic_pyme(+) ");
			strSQL.append("   AND promo.ic_pyme = cd.ic_pyme ");
			strSQL.append("   AND ce.ic_estado = cd.ic_estado ");
			strSQL.append("   AND ce.ic_pais = cd.ic_pais ");
			strSQL.append("   AND cd.cs_fiscal = ? ");
			strSQL.append("   AND (   (    promo.df_desactivacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("            AND promo.df_desactivacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("            AND promo.df_desactivacion > promo.df_activacion ");
			strSQL.append("              ) ");
			strSQL.append("           OR (    promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("               AND promo.df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("        			  AND (promo.df_activacion > promo.df_desactivacion OR promo.df_desactivacion IS NULL) ");
			strSQL.append("              ) ");
			strSQL.append("           OR (    promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("               AND promo.df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("               AND (   promo.df_activacion > promo.df_desactivacion ");
			strSQL.append("                    OR promo.df_desactivacion IS NULL ");
			strSQL.append("                   ) ");
			strSQL.append("              ) ");
			strSQL.append("          ) ");


			varBind.add(fecha_ini_mes);
			varBind.add(fecha_ini_mes);
			varBind.add(fecha_ini_act);
			varBind.add(fecha_ini_mes);
			varBind.add(fecha_ini_mes);
			varBind.add(fecha_ini_mes);
			varBind.add("S");
			varBind.add("H");
			varBind.add("S");
			varBind.add("H");
			varBind.add("P");
			varBind.add("S");
			varBind.add("S");
			varBind.add(fecha_ini_mes);
			varBind.add(fecha_ini_mes);
			varBind.add(fecha_ini_act);
			varBind.add(fecha_ini_mes);
			varBind.add(fecha_ini_mes);
			varBind.add(fecha_ini_mes);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List registroReporteClientesReg = new ArrayList();
        registroReporteClientesReg.add(rst.getString(1)==null?"":rst.getString(1));
        registroReporteClientesReg.add(rst.getString(2)==null?"":rst.getString(2));
        registroReporteClientesReg.add(rst.getString(3)==null?"":rst.getString(3));
        registroReporteClientesReg.add(rst.getString(4)==null?"":rst.getString(4));
        registroReporteClientesReg.add(rst.getString(5)==null?"":rst.getString(5));
        registroReporteClientesReg.add(rst.getString(6)==null?"":rst.getString(6));
        registroReporteClientesReg.add(rst.getString(7)==null?"":rst.getString(7));
        registroReporteClientesReg.add(rst.getString(8)==null?"":rst.getString(8));
				registroReporteClientesReg.add(rst.getString(9)==null?"":rst.getString(9));
				registroReporteClientesReg.add(rst.getString(10)==null?"":rst.getString(10));
				registroReporteClientesReg.add(rst.getString(11)==null?"":rst.getString(11));
				registroReporteClientesReg.add(rst.getString(12)==null?"":rst.getString(12));

				StringBuffer strSQL1 = new StringBuffer();
				List varBind1 = new ArrayList();

				strSQL1.append("SELECT distinct ");
				strSQL1.append("    CASE ");
				strSQL1.append("        WHEN ce.ic_tipo_epo IN(1,2,3,4) THEN 'AMBAS' ");
				strSQL1.append("        WHEN ce.ic_tipo_epo IN(1,2) THEN 'PUBLICAS' ");
				strSQL1.append("        WHEN ce.ic_tipo_epo IN(3,4) THEN 'PRIVADAS' END AS tipoEPO ");
				strSQL1.append("  FROM comcat_epo ce, comrel_pyme_epo cpe ");
				strSQL1.append(" WHERE cpe.ic_epo = ce.ic_epo ");
				strSQL1.append("   AND ce.ic_tipo_epo is not null ");
				strSQL1.append("   AND cpe.cs_aceptacion = ? ");
				strSQL1.append("   AND cpe.cs_habilitado = ? ");
   			strSQL1.append("   AND cpe.ic_pyme = ? ");

        varBind1.add("H");
				varBind1.add("S");
				varBind1.add(rst.getString(2));

			//	LOG.debug("..:: strSQL1 : "+strSQL1.toString());
			//	LOG.debug("..:: varBind1 : "+varBind1.toString());

				ps = con.queryPrecompilado(strSQL1.toString(), varBind1);
				rs = ps.executeQuery();

				if(rs.next()){
					registroReporteClientesReg.add(rs.getString(1)==null?"":rs.getString(1));
				}else{
					registroReporteClientesReg.add("");
				}
				clientesNafinsaMovilDetalle.add(registroReporteClientesReg);

				rs.close();
				ps.close();
			}
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: clientesNafinsaMovilDetalle (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: clientesNafinsaMovilDetalle(F)");
    return clientesNafinsaMovilDetalle;
	}

/**
	 * M�todo que genera el reporte de Documentos Negociables y Por Vencer de Nafinsa Movil a partir de la fecha de promocion.
 * @param
 *        1) tipo_docto Tipo de reporte Negociable 'N' o Por Vencer 'V'
	 * 2) claveAnioI A�o inicial de promocion.
	 * 3) claveAnioF A�o final de promocion.
	 * 4) claveMesI Mes inicial de promocion.
	 * 5) claveMesF Mes final de promocion.
 * @return reporteNegociablesNafinsaMovil Lista con la informaci�n del cat�logo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 028 - 2009 Reportes Nafinsa M�vil
	 * @author Erik Barba Alatriste
	 * @Modify Janneth Rebeca Salda�a Waldo FODEA 052 - 2010
	 */
	@Override
  public List reporteNegociablesNafinsaMovil(String tipo_docto, String claveAnioI, String claveAnioF, String claveMesI, String claveMesF) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteNegociablesNafinsaMovil = new ArrayList();

		SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calI = Calendar.getInstance();
		Calendar calF = Calendar.getInstance();
		calI.set(Integer.parseInt(claveAnioI), Integer.parseInt(claveMesI), 1);
		calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), 1);
		calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), calF.getActualMaximum(Calendar.DAY_OF_MONTH));

		String fecha_notificacion_ini = sdfFecha.format(calI.getTime());
		String fecha_notificacion_fin = sdfFecha.format(calF.getTime());

		LOG.info("..:: reporteNegociablesNafinsaMovil(I)");

    try {
      con.conexionDB();

			strSQL.append(" SELECT   TO_CHAR (df_fecha_notificacion, 'yyyy') anio_notificacion, ");
			strSQL.append("          CASE WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 1 THEN 'ENERO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 2 THEN 'FEBRERO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 3 THEN 'MARZO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 4 THEN 'ABRIL' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 5 THEN 'MAYO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 6 THEN 'JUNIO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 7 THEN 'JULIO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 8 THEN 'AGOSTO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 9 THEN 'SEPTIEMBRE' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 10 THEN 'OCTUBRE' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 11 THEN 'NOVIEMBRE' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_notificacion, 'mm') = 12 THEN 'DICIEMBRE' END AS mes_notificacion, ");
			strSQL.append("          COUNT (ic_pyme) pymes, SUM (ig_numero_doctos) doctos_notificados, ");
			strSQL.append("          SUM (fn_monto_notificado) monto_notificado, ");
			strSQL.append("          TO_CHAR (df_fecha_notificacion, 'mm') num_mes ");
			strSQL.append("     FROM bit_doctos_notificados ");
			strSQL.append("    WHERE cg_estatus = ? ");
			strSQL.append("      AND df_fecha_notificacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("      AND df_fecha_notificacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append(" GROUP BY TO_CHAR (df_fecha_notificacion, 'yyyy'), ");
			strSQL.append("          TO_CHAR (df_fecha_notificacion, 'MONTH'), ");
			strSQL.append("          TO_CHAR (df_fecha_notificacion, 'mm') ");
			strSQL.append(" ORDER BY anio_notificacion, num_mes DESC ");

			varBind.add(tipo_docto); // estatus de bitdocumento = N egociable , V por Vencer
      varBind.add(fecha_notificacion_ini);
      varBind.add(fecha_notificacion_fin);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List registroReporteNegociablesReg = new ArrayList();
        registroReporteNegociablesReg.add(rst.getString(1)==null?"":rst.getString(1));//anio_notificacion
        registroReporteNegociablesReg.add(rst.getString(2)==null?"":rst.getString(2));//mes_notificacion
        registroReporteNegociablesReg.add(rst.getString(3)==null?"":rst.getString(3));//pymes
        registroReporteNegociablesReg.add(rst.getString(4)==null?"":rst.getString(4));//doctos_notificados
				registroReporteNegociablesReg.add(rst.getString(5)==null?"":rst.getString(5));//monto_notificado
				registroReporteNegociablesReg.add(rst.getString(6)==null?"":rst.getString(6));//num_mes
        reporteNegociablesNafinsaMovil.add(registroReporteNegociablesReg);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: reporteNegociablesNafinsaMovil (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteNegociablesNafinsaMovil(F)");
    return reporteNegociablesNafinsaMovil;
	}

/**
	 * M�todo que genera el reporte de Documentos Negociables y Por Vencer - Detalle de Nafinsa Movil a partir de la fecha de promocion.
 * @param
 *        1) tipo_docto Tipo de reporte Negociable 'N' o Por Vencer 'V'
 *        2) anioDetalle A�o de promocion.
 *        3) mesDetalle Mes de promocion.
	 * @return reporteNafinsaMovilDetalle Lista con la informaci�n del cat�logo.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 052 - 2010 Reportes Nafinsa M�vil
	 * @author Janneth Rebeca Salda�a Waldo
	 */
	@Override
  public List reporteNafinsaMovilDetalle(String tipo_docto, String anioDetalle, String numMes) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteNafinsaMovilDetalle = new ArrayList();

		//SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
		//Calendar calI = Calendar.getInstance();
		//Calendar calF = Calendar.getInstance();
		//calI.set(Integer.parseInt(anioDetalle), Integer.parseInt(numMes)-1, 1);
		//calF.set(Integer.parseInt(anioDetalle), Integer.parseInt(numMes)-1, calF.getActualMaximum(calF.DAY_OF_MONTH));

		String fecha_notificacion_ini = "01/" + numMes+ "/" + anioDetalle;

		LOG.info("..:: reporteNafinsaMovilDetalle(I)");

    try {
      con.conexionDB();

			strSQL.append("SELECT crn.ic_nafin_electronico nafin_electronico, ");
			strSQL.append("       ccp.ic_pyme ic_pyme, ");
			strSQL.append("       ccp.cg_rfc rfc, ");
			strSQL.append("       ccp.cg_razon_social nombre_pyme, ");
			strSQL.append("       cc.cg_celular num_cel, ");
			strSQL.append("       bdn.ig_numero_doctos doctos_notificados, ");
			strSQL.append("       bdn.fn_monto_notificado monto_notificado, ");
			strSQL.append("       bdn.ig_num_doctos_operados doctos_operados, ");
			strSQL.append("       bdn.fg_monto_operado monto_operado, ");
			strSQL.append("       bdn.ig_num_doctos_vencidos doctos_vencidos, ");
			strSQL.append("       bdn.fg_monto_vencido monto_vencido");
			strSQL.append("  FROM com_promo_pyme cpp, ");
			strSQL.append("       comrel_nafin crn, ");
			strSQL.append("       comcat_pyme ccp, ");
			strSQL.append("       com_contacto cc, ");
			strSQL.append("       bit_doctos_notificados bdn ");
			strSQL.append(" WHERE cpp.ic_pyme = ccp.ic_pyme ");
			strSQL.append("   AND cpp.ic_pyme = crn.ic_epo_pyme_if ");
			strSQL.append("   AND cpp.ic_pyme = cc.ic_pyme ");
			strSQL.append("   AND cpp.ic_pyme = bdn.ic_pyme ");
			strSQL.append("   AND crn.cg_tipo = ? ");
			strSQL.append("   AND cc.cs_primer_contacto = ? ");
			strSQL.append("   AND bdn.cg_estatus = ? ");
			strSQL.append("   AND bdn.df_fecha_notificacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("   AND bdn.df_fecha_notificacion < ADD_MONTHS( TO_DATE(?, 'dd/mm/yyyy'), 1) ");

			varBind.add("P"); 
			varBind.add("S");
			varBind.add(tipo_docto); // estatus de bitdocumento = N egociable , V por Vencer
			varBind.add(fecha_notificacion_ini);
			varBind.add(fecha_notificacion_ini);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

		while(rst.next()){
			List registroReporteNegociablesReg = new ArrayList();
			registroReporteNegociablesReg.add(rst.getString(1)==null?"":rst.getString(1));//nafin_electronico
			registroReporteNegociablesReg.add(rst.getString(2)==null?"":rst.getString(2));//ic_pyme
			registroReporteNegociablesReg.add(rst.getString(3)==null?"":rst.getString(3));//rfc
			registroReporteNegociablesReg.add(rst.getString(4)==null?"":rst.getString(4));//nombre_pyme
			registroReporteNegociablesReg.add(rst.getString(5)==null?"":rst.getString(5));//num_cel
			registroReporteNegociablesReg.add(rst.getString(6)==null?"":rst.getString(6));//doctos_notificados
			registroReporteNegociablesReg.add(rst.getString(7)==null?"":rst.getString(7));//monto_notificado
			registroReporteNegociablesReg.add(rst.getString(8)==null?"":rst.getString(8));//doctos_operados
			registroReporteNegociablesReg.add(rst.getString(9)==null?"":rst.getString(9));//monto_operado
			registroReporteNegociablesReg.add(rst.getString(10)==null?"":rst.getString(10));//doctos_vencidos
			registroReporteNegociablesReg.add(rst.getString(11)==null?"":rst.getString(11));//monto_vencido
			reporteNafinsaMovilDetalle.add(registroReporteNegociablesReg);
		}

		rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: reporteNafinsaMovilDetalle (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteNafinsaMovilDetalle(F)");
    return reporteNafinsaMovilDetalle;
	}

	/**
	 * M�todo que genera el total del reporte de Documentos Negociables y Por Vencer de Nafinsa Movil a partir de la fecha de promocion.
 * @param
 *        1) tipo_docto Tipo de reporte Negociable 'N' o Por Vencer 'V'
 *        2) anioDetalle A�o de promocion.
 *        3) numMes Mes de promocion.
	 * @return reporteNafinsaMovilDetalleTotal Lista con la informaci�n del cat�logo.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 052 - 2010 Reportes Nafinsa M�vil
	 * @author Janneth Rebeca Salda�a Waldo
	 */
	@Override
  public List reporteNafinsaMovilDetalleTotal(String tipo_docto, String anioDetalle, String numMes) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteNafinsaMovilDetalleTotal = new ArrayList();

		/*SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calI = Calendar.getInstance();
		Calendar calF = Calendar.getInstance();
		calI.set(Integer.parseInt(anioDetalle), Integer.parseInt(numMes)-1, 1);
		calF.set(Integer.parseInt(anioDetalle), Integer.parseInt(numMes)-1, calF.getActualMaximum(calF.DAY_OF_MONTH));*/

		String fecha_notificacion_ini = "01/" + numMes + "/" + anioDetalle;
		//String fecha_notificacion_fin = sdfFecha.format(calF.getTime());

		LOG.info("..:: reporteNafinsaMovilDetalleTotal(I)");

    try {
      con.conexionDB();

			strSQL.append("SELECT   COUNT (ic_pyme) no_pymes, ");
			strSQL.append("         SUM (ig_numero_doctos) doctos_notificados, ");
			strSQL.append("         SUM (fn_monto_notificado) monto_notificado, ");
			strSQL.append("         SUM (ig_num_doctos_operados) doctos_operados, ");
			strSQL.append("         SUM (fg_monto_operado) monto_operado, ");
			strSQL.append("         SUM (ig_num_doctos_vencidos) doctos_vencidos, ");
			strSQL.append("         SUM (fg_monto_vencido) monto_vencido ");
			strSQL.append("    FROM bit_doctos_notificados ");
			strSQL.append("   WHERE cg_estatus = ? ");
			strSQL.append("     AND df_fecha_notificacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("     AND df_fecha_notificacion < ADD_MONTHS( TO_DATE (?, 'dd/mm/yyyy'), 1) ");

			varBind.add(tipo_docto); // estatus de bitdocumento = N egociable , V por Vencer
			varBind.add(fecha_notificacion_ini);
			varBind.add(fecha_notificacion_ini);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List registroReporteRegTotal = new ArrayList();
        registroReporteRegTotal.add(rst.getString(1)==null?"":rst.getString(1));//no_pymes
        registroReporteRegTotal.add(rst.getString(2)==null?"":rst.getString(2));//doctos_notificados
        registroReporteRegTotal.add(rst.getString(3)==null?"":rst.getString(3));//monto_notificado
        registroReporteRegTotal.add(rst.getString(4)==null?"":rst.getString(4));//doctos_operados
				registroReporteRegTotal.add(rst.getString(5)==null?"":rst.getString(5));//monto_operado
				registroReporteRegTotal.add(rst.getString(6)==null?"":rst.getString(6));//doctos_vencidos
				registroReporteRegTotal.add(rst.getString(7)==null?"":rst.getString(7));//monto_vencido
        reporteNafinsaMovilDetalleTotal.add(registroReporteRegTotal);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: reporteNafinsaMovilDetalleTotal (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteNafinsaMovilDetalleTotal(F)");
    return reporteNafinsaMovilDetalleTotal;
	}

/**
	 * M�todo que genera el reporte de Costo e ingreso de las Notificaciones de Nafinsa Movil a partir de la fecha de promocion.
 * @param
 *        1) claveAnioI A�o inicial de promocion.
	 * 2) claveAnioF A�o final de promocion.
	 * 3) claveMesI Mes inicial de promocion.
	 * 4) claveMesF Mes final de promocion.
 * @return reporteNegociablesNafinsaMovil Lista con la informaci�n del cat�logo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 052 - 2009 Reportes Nafinsa M�vil
	 * @author Janneth Rebeca Salda�a Waldo
	 */
	@Override
  public List reporteCostoEIngresoNafinsaMovil(String claveAnioI, String claveAnioF, String claveMesI, String claveMesF) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteCostoEIngresoNafinsaMovil = new ArrayList();

		SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calI = Calendar.getInstance();
		Calendar calF = Calendar.getInstance();
		calI.set(Integer.parseInt(claveAnioI), Integer.parseInt(claveMesI), 1);
		calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), 1);
		calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), calF.getActualMaximum(Calendar.DAY_OF_MONTH));

		String fecha_notificacion_ini = sdfFecha.format(calI.getTime());
		String fecha_notificacion_fin = sdfFecha.format(calF.getTime());

		LOG.info("..:: reporteCostoEIngresoNafinsaMovil(I)");

    try {
      con.conexionDB();

			strSQL.append(" SELECT   TO_CHAR (df_fecha_corte, 'yyyy') anio_corte, ");
			strSQL.append(" CASE WHEN TO_CHAR (df_fecha_corte, 'mm') = 1 THEN 'ENERO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 2 THEN 'FEBRERO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 3 THEN 'MARZO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 4 THEN 'ABRIL' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 5 THEN 'MAYO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 6 THEN 'JUNIO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 7 THEN 'JULIO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 8 THEN 'AGOSTO' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 9 THEN 'SEPTIEMBRE' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 10 THEN 'OCTUBRE' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 11 THEN 'NOVIEMBRE' ");
			strSQL.append("               WHEN TO_CHAR (df_fecha_corte, 'mm') = 12 THEN 'DICIEMBRE' END AS mes_corte, ");
			strSQL.append("          COUNT (ic_pyme) pymes, ");
			strSQL.append("          SUM (ig_num_doctos_oper) doctos_oper, ");
			strSQL.append("          SUM (fg_monto_oper) monto_oper,");
			strSQL.append("          SUM (ig_num_not_oper) num_not_oper,");
			strSQL.append("          SUM (fg_costo_not_oper) costo_not_oper,");
			strSQL.append("          SUM (fg_int_doctos_oper) intereses,");
			strSQL.append("          SUM (ig_num_doctos_neg) doctos_neg,");
			strSQL.append("          SUM (fg_monto_neg) monto_neg,");
			strSQL.append("          SUM (ig_num_not_neg)num_not_neg, ");
			strSQL.append("          SUM (fg_costo_not_neg) costo_not_neg, ");
			strSQL.append("          SUM (ig_num_doctos_ven) num_doctos_ven, ");
			strSQL.append("          SUM (fg_monto_ven) monto_ven, ");
			strSQL.append("          SUM (ig_num_not_ven) num_not_ven, ");
			strSQL.append("          SUM (fg_costo_not_ven) costo_not_ven, ");
			strSQL.append("          TO_CHAR (df_fecha_corte, 'mm') num_mes ");
			strSQL.append("     FROM bit_costo_benef ");
			strSQL.append("    WHERE df_fecha_corte >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("      AND df_fecha_corte < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append(" GROUP BY TO_CHAR (df_fecha_corte, 'yyyy'), ");
			strSQL.append("          TO_CHAR (df_fecha_corte, 'MONTH'), ");
			strSQL.append("          TO_CHAR (df_fecha_corte, 'mm') ");
			strSQL.append(" ORDER BY anio_corte, mes_corte DESC ");

      varBind.add(fecha_notificacion_ini);
      varBind.add(fecha_notificacion_fin);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List registroReporteCostoEIngresoReg = new ArrayList();
        registroReporteCostoEIngresoReg.add(rst.getString(1)==null?"":rst.getString(1));//anio_corte
        registroReporteCostoEIngresoReg.add(rst.getString(2)==null?"":rst.getString(2));//mes_corte
        registroReporteCostoEIngresoReg.add(rst.getString(3)==null?"":rst.getString(3));//pymes
        registroReporteCostoEIngresoReg.add(rst.getString(4)==null?"":rst.getString(4));//doctos_oper
				registroReporteCostoEIngresoReg.add(rst.getString(5)==null?"":rst.getString(5));//monto_oper
				registroReporteCostoEIngresoReg.add(rst.getString(6)==null?"":rst.getString(6));//num_not_oper
				registroReporteCostoEIngresoReg.add(rst.getString(7)==null?"":rst.getString(7));//costo_not_oper
				registroReporteCostoEIngresoReg.add(rst.getString(8)==null?"":rst.getString(8));//intereses
				registroReporteCostoEIngresoReg.add(rst.getString(9)==null?"":rst.getString(9));//doctos_neg
				registroReporteCostoEIngresoReg.add(rst.getString(10)==null?"":rst.getString(10));//monto_neg
				registroReporteCostoEIngresoReg.add(rst.getString(11)==null?"":rst.getString(11));//num_not_neg
				registroReporteCostoEIngresoReg.add(rst.getString(12)==null?"":rst.getString(12));//costo_not_neg
				registroReporteCostoEIngresoReg.add(rst.getString(13)==null?"":rst.getString(13));//num_doctos_ven
				registroReporteCostoEIngresoReg.add(rst.getString(14)==null?"":rst.getString(14));//monto_ven
				registroReporteCostoEIngresoReg.add(rst.getString(15)==null?"":rst.getString(15));//num_not_ven
				registroReporteCostoEIngresoReg.add(rst.getString(16)==null?"":rst.getString(16));//costo_not_ven
				registroReporteCostoEIngresoReg.add(rst.getString(17)==null?"":rst.getString(17));//costo_not_ven
        reporteCostoEIngresoNafinsaMovil.add(registroReporteCostoEIngresoReg);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: reporteCostoEIngresoNafinsaMovil (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteCostoEIngresoNafinsaMovil(F)");
    return reporteCostoEIngresoNafinsaMovil;
	}

/**
	 * M�todo que genera el reporte Costo e ingreso de las notificaciones - Detalle de Nafinsa Movil a partir de la fecha de promocion.
 * @param
 *        2) anioDetalle A�o de promocion.
 *        3) numMes Mes de promocion.
	 * @return reporteNafinsaMovilDetalle Lista con la informaci�n del cat�logo.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 052 - 2010 Reportes Nafinsa M�vil
	 * @author Janneth Rebeca Salda�a Waldo
	 */
	@Override
  public List reporteCostoEIngresoDetalle(String anioDetalle, String numMes) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null, ps = null;
    ResultSet rst = null, rs = null;
    StringBuffer strSQL = new StringBuffer();
		StringBuffer strSQL1 = new StringBuffer();
    List varBind = new ArrayList();
		List varBind1 = new ArrayList();
    List reporteCostoEIngresoDetalle = new ArrayList();
		
		String fecha_notificacion_ini = "01/" + numMes+ "/" + anioDetalle;

		LOG.info("..:: reporteCostoEIngresoDetalle(I)");

    try {
      con.conexionDB();
			strSQL = new StringBuffer();
			strSQL.append("SELECT /*+ use_nl (bcb crn cp cc ce cd)*/ ");
			strSQL.append("       crn.ic_nafin_electronico no_nafin, ");
			strSQL.append("       bcb.ic_pyme pyme, ");
			strSQL.append("       cp.cg_rfc rfc, ");
			strSQL.append("       cp.cg_razon_social razon_social, ");
			strSQL.append("       cc.cg_celular celular, ");
			strSQL.append("       ce.cd_nombre estado, ");
			strSQL.append("       bcb.ig_num_doctos_oper doctos_oper, ");
			strSQL.append("       bcb.fg_monto_oper monto_oper, ");
			strSQL.append("       bcb.ig_num_not_oper num_not_oper, ");
			strSQL.append("       bcb.fg_costo_not_oper costo_not_oper, ");
			strSQL.append("       bcb.fg_int_doctos_oper intereses, ");
			strSQL.append("       bcb.ig_num_doctos_neg doctos_neg, ");
			strSQL.append("       bcb.fg_monto_neg monto_neg, ");
			strSQL.append("       bcb.ig_num_not_neg num_not_neg, ");
			strSQL.append("       bcb.fg_costo_not_neg costo_not_neg, ");
			strSQL.append("       bcb.ig_num_doctos_ven num_doctos_ven, ");
			strSQL.append("       bcb.fg_monto_ven monto_ven, ");
			strSQL.append("       bcb.ig_num_not_ven num_not_ven, ");
			strSQL.append("       bcb.fg_costo_not_ven costo_not_ven ");
			strSQL.append("  FROM bit_costo_benef bcb, ");
			strSQL.append("       comrel_nafin crn, ");
			strSQL.append("       comcat_pyme cp, ");
			strSQL.append("       com_contacto cc, ");
			strSQL.append("       comcat_estado ce, ");
			strSQL.append("       com_domicilio cd ");
			strSQL.append(" WHERE bcb.df_fecha_corte >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("   AND bcb.df_fecha_corte < ADD_MONTHS( TO_DATE (?, 'dd/mm/yyyy'), 1) ");
			strSQL.append("   AND crn.ic_epo_pyme_if = bcb.ic_pyme ");
			strSQL.append("   AND crn.cg_tipo = ? ");
			strSQL.append("   AND cc.cs_primer_contacto = ? ");
			strSQL.append("   AND cp.ic_pyme = bcb.ic_pyme ");
			strSQL.append("   AND cc.ic_pyme = bcb.ic_pyme ");
			strSQL.append("   AND cd.ic_pyme = bcb.ic_pyme ");
			strSQL.append("   AND cd.ic_pais = ce.ic_pais ");
			strSQL.append("   AND cd.ic_estado = ce.ic_estado ");

			varBind.add(fecha_notificacion_ini);
			varBind.add(fecha_notificacion_ini);
			varBind.add("P");
			varBind.add("S");

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

		strSQL1.append("SELECT distinct ");
		strSQL1.append("    CASE ");
		strSQL1.append("        WHEN ce.ic_tipo_epo IN(1,2,3,4) THEN 'AMBAS' ");
		strSQL1.append("        WHEN ce.ic_tipo_epo IN(1,2) THEN 'PUBLICAS' ");
		strSQL1.append("        WHEN ce.ic_tipo_epo IN(3,4) THEN 'PRIVADAS' END AS tipoEPO ");
		strSQL1.append("  FROM comcat_epo ce, comrel_pyme_epo cpe ");
		strSQL1.append(" WHERE cpe.ic_epo = ce.ic_epo ");
		strSQL1.append("   AND ce.ic_tipo_epo is not null ");
		strSQL1.append("   AND cpe.cs_aceptacion = ? ");
		strSQL1.append("   AND cpe.cs_habilitado = ? ");
		strSQL1.append("   AND cpe.ic_pyme = ? ");

      while(rst.next()){
        List registroReporteCostoEIngresoReg = new ArrayList();
        registroReporteCostoEIngresoReg.add(rst.getString(1)==null?"":rst.getString(1));//no_nafin
        registroReporteCostoEIngresoReg.add(rst.getString(2)==null?"":rst.getString(2));//pyme
        registroReporteCostoEIngresoReg.add(rst.getString(3)==null?"":rst.getString(3));//rfc
        registroReporteCostoEIngresoReg.add(rst.getString(4)==null?"":rst.getString(4));//razon_social
				registroReporteCostoEIngresoReg.add(rst.getString(5)==null?"":rst.getString(5));//celular
				registroReporteCostoEIngresoReg.add(rst.getString(6)==null?"0":rst.getString(6));//estado
				registroReporteCostoEIngresoReg.add(rst.getString(7)==null?"0":rst.getString(7));//doctos_oper
				registroReporteCostoEIngresoReg.add(rst.getString(8)==null?"0":rst.getString(8));//monto_oper
				registroReporteCostoEIngresoReg.add(rst.getString(9)==null?"0":rst.getString(9));//num_not_oper
				registroReporteCostoEIngresoReg.add(rst.getString(10)==null?"0":rst.getString(10));//costo_not_oper
				registroReporteCostoEIngresoReg.add(rst.getString(11)==null?"0":rst.getString(11));//intereses
				registroReporteCostoEIngresoReg.add(rst.getString(12)==null?"0":rst.getString(12));//doctos_neg
				registroReporteCostoEIngresoReg.add(rst.getString(13)==null?"0":rst.getString(13));//monto_neg
				registroReporteCostoEIngresoReg.add(rst.getString(14)==null?"0":rst.getString(14));//num_not_neg
				registroReporteCostoEIngresoReg.add(rst.getString(15)==null?"0":rst.getString(15));//costo_not_neg
				registroReporteCostoEIngresoReg.add(rst.getString(16)==null?"0":rst.getString(16));//num_doctos_ven
				registroReporteCostoEIngresoReg.add(rst.getString(17)==null?"0":rst.getString(17));//monto_ven
				registroReporteCostoEIngresoReg.add(rst.getString(18)==null?"0":rst.getString(18));//num_not_ven
				registroReporteCostoEIngresoReg.add(rst.getString(19)==null?"0":rst.getString(19));//costo_not_ven

				varBind1.clear();
				varBind1.add("H");
				varBind1.add("S");
				varBind1.add(rst.getString(2));

				LOG.debug("..:: strSQL1 : "+strSQL1.toString());
				LOG.debug("..:: varBind1 : "+varBind1.toString());

				ps = con.queryPrecompilado(strSQL1.toString(), varBind1);
				rs = ps.executeQuery();

				if(rs.next()){
					registroReporteCostoEIngresoReg.add(rs.getString(1)==null?"":rs.getString(1));
				}else if(!rs.next()){
					registroReporteCostoEIngresoReg.add("");
				}
				reporteCostoEIngresoDetalle.add(registroReporteCostoEIngresoReg);

				rs.close();
				ps.close();
      }

		rst.close();
      pst.close();

    }catch(Exception e){
      LOG.info("..:: reporteCostoEIngresoDetalle (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteCostoEIngresoDetalle(F)");
    return reporteCostoEIngresoDetalle;
	}

/**
	 * M�todo que genera el reporte Costo e ingreso de las notificaciones - Global de Nafinsa Movil a partir de la fecha de promocion.
 * @param
 *        2) anioDetalle A�o de promocion.
 *        3) mesDetalle Mes de promocion.
	 * @return reporteCostoEIngresoGlobal Lista con la informaci�n del cat�logo.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 052 - 2010 Reportes Nafinsa M�vil
	 * @author Janneth Rebeca Salda�a Waldo
	 */
	@Override
  public List reporteCostoEIngresoGlobal(String anioDetalle, String numMes) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteCostoEIngresoGlobal = new ArrayList();

		String fecha_notificacion_ini = "01/" + numMes+ "/" + anioDetalle;

		LOG.info("..:: reporteCostoEIngresoGlobal(I)");

    try {
      con.conexionDB();

			strSQL.append("SELECT SUM (ig_numero_neg) num_neg, SUM (fn_monto_neg) monto_neg, ");
			strSQL.append("       SUM (ig_numero_operados) num_ope, SUM (fn_monto_operados) monto_ope, ");
			strSQL.append("       SUM (ig_numero_vencidos) num_ven, SUM (fn_monto_vencidos) monto_ven ");
			strSQL.append("  FROM bit_doctos_global ");
			strSQL.append(" WHERE df_fecha_proc >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("   AND df_fecha_proc < TO_DATE (?, 'dd/mm/yyyy') + 1 ");

			varBind.add(fecha_notificacion_ini);
			varBind.add(fecha_notificacion_ini);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

		while(rst.next()){
			List registroReporteCostoEIngresoRegGlobal = new ArrayList();
			registroReporteCostoEIngresoRegGlobal.add(rst.getString(1)==null?"":rst.getString(1));//num_neg
			registroReporteCostoEIngresoRegGlobal.add(rst.getString(2)==null?"":rst.getString(2));//monto_neg
			registroReporteCostoEIngresoRegGlobal.add(rst.getString(3)==null?"":rst.getString(3));//num_ope
			registroReporteCostoEIngresoRegGlobal.add(rst.getString(4)==null?"":rst.getString(4));//monto_ope
			registroReporteCostoEIngresoRegGlobal.add(rst.getString(5)==null?"":rst.getString(5));//num_ven
			registroReporteCostoEIngresoRegGlobal.add(rst.getString(6)==null?"":rst.getString(6));//monto_ven
			reporteCostoEIngresoGlobal.add(registroReporteCostoEIngresoRegGlobal);
		}

		rst.close();
      pst.close();

    }catch(Exception e){
      LOG.info("..:: reporteCostoEIngresoGlobal (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteCostoEIngresoGlobal(F)");
		return reporteCostoEIngresoGlobal;
	}

/**
	 * M�todo que genera el reporte de Documentos Operados VS Negociables.
 * @param 1) fecha_operacion_ini Fecha inicial de operacion.
 * @param 2) fecha_operacion_fin Fecha final de operacion.
	 * @return resultado Lista con la informaci�n del catalogo y sus totales.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 028 - 2009 Reportes Nafinsa M�vil
	 * @author Erik Barba Alatriste
	 */
	@Override
  public List reporteOperadoNegociableNafinsaMovil(String fecha_operacion_ini, String fecha_operacion_fin) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteOperadoNegociablesNafinsaMovil = new ArrayList();
		List reporteTotalOperadoNegociablesNafinsaMovil = new ArrayList();
		List resultado = new ArrayList();

    LOG.info("..:: reporteOperadoNegociableNafinsaMovil(I)");
    try {
      con.conexionDB();
			strSQL.append("SELECT TO_CHAR (docon.df_fecha_operacion, 'dd/mm/yyyy')," );
			strSQL.append("       docon.ic_nafin_electronico, docon.ic_pyme, docon.cg_razon_social," );
      strSQL.append("       docon.ig_numero_operados, docon.fn_monto_operados," );
			strSQL.append("       docon.ig_numero_notificaciones, docon.fn_costo_notificaciones," );
			strSQL.append("       docon.fg_interes_operados" );
			strSQL.append("  FROM bit_doctos_on docon" );
			strSQL.append("   WHERE docon.DF_FECHA_OPERACION BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy') ");
			strSQL.append("  order by 1" );

      varBind.add(fecha_operacion_ini);
      varBind.add(fecha_operacion_fin);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List registroReporteOperadoNegociables = new ArrayList();
        registroReporteOperadoNegociables.add(rst.getString(1)==null?"":rst.getString(1));
        registroReporteOperadoNegociables.add(rst.getString(2)==null?"":rst.getString(2));
        registroReporteOperadoNegociables.add(rst.getString(3)==null?"":rst.getString(3));
        registroReporteOperadoNegociables.add(rst.getString(4)==null?"":rst.getString(4));
        registroReporteOperadoNegociables.add(rst.getString(5)==null?"":rst.getString(5));
        registroReporteOperadoNegociables.add(rst.getString(6)==null?"":rst.getString(6));
        registroReporteOperadoNegociables.add(rst.getString(7)==null?"":rst.getString(7));
        registroReporteOperadoNegociables.add(rst.getString(8)==null?"":rst.getString(8));
        registroReporteOperadoNegociables.add(rst.getString(9)==null?"":rst.getString(9));
        reporteOperadoNegociablesNafinsaMovil.add(registroReporteOperadoNegociables);
      }
      rst.close();
      pst.close();
			resultado.add(reporteOperadoNegociablesNafinsaMovil);

			//Sacando los totales
			strSQL = new StringBuffer();
			varBind = new ArrayList();
			strSQL.append("	SELECT    count(distinct ic_pyme), SUM(IG_NUMERO_OPERADOS), sum(FN_MONTO_OPERADOS), sum(IG_NUMERO_NOTIFICACIONES), sum(FN_COSTO_NOTIFICACIONES), SUM(FG_INTERES_OPERADOS)" );
			strSQL.append(" 	FROM bit_doctos_on docon" );
			strSQL.append(" WHERE docon.df_fecha_operacion BETWEEN TO_DATE (?, 'dd/mm/yyyy') AND TO_DATE (?, 'dd/mm/yyyy') " );
      varBind.add(fecha_operacion_ini);
      varBind.add(fecha_operacion_fin);
      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();
      while(rst.next()){
        List registroReporteOperadoNegociables = new ArrayList();
        registroReporteOperadoNegociables.add(rst.getString(1)==null?"":rst.getString(1));
        registroReporteOperadoNegociables.add(rst.getString(2)==null?"":rst.getString(2));
        registroReporteOperadoNegociables.add(rst.getString(3)==null?"":rst.getString(3));
        registroReporteOperadoNegociables.add(rst.getString(4)==null?"":rst.getString(4));
        registroReporteOperadoNegociables.add(rst.getString(5)==null?"":rst.getString(5));
				registroReporteOperadoNegociables.add(rst.getString(6)==null?"":rst.getString(6));
        reporteTotalOperadoNegociablesNafinsaMovil.add(registroReporteOperadoNegociables);
      }
      rst.close();
      pst.close();
			resultado.add(reporteTotalOperadoNegociablesNafinsaMovil);
    }catch(Exception e){
      LOG.info("..:: reporteOperadoNegociableNafinsaMovil (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteOperadoNegociableNafinsaMovil(F)");
    return resultado;
	}

/**
	 * M�todo que genera el reporte Layout 2 de Documentos Operados VS Negociables.
 * @param 1) fecha_operacion_ini Fecha inicial de operacion.
 * @param 2) fecha_operacion_fin Fecha final de operacion.
	 * @return reporteDoctosGlobal Lista con la informaci�n del catalogo.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 028 - 2009 Reportes Nafinsa M�vil
	 * @author Erik Barba Alatriste
	 */
	@Override
  public List reporteDoctosGlobalNafinsaMovil(String fecha_operacion_ini, String fecha_operacion_fin) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteDoctosGlobal = new ArrayList();

    LOG.info("..:: reporteDoctosGlobalNafinsaMovil(I)");
    try {
      con.conexionDB();
			strSQL.append("SELECT     Round((SUM (ig_numero_neg))  / fn_dias_entre2_fechas (?, ?),0), " );
			strSQL.append("           (SUM (fn_monto_neg))   / fn_dias_entre2_fechas (?, ?), " );
			strSQL.append("           Round((SUM (ig_numero_operados))  / fn_dias_entre2_fechas (?, ?),0), " );
			strSQL.append("           (SUM (fn_monto_operados))   / fn_dias_entre2_fechas (?, ?), " );
			strSQL.append("           Round((SUM (ig_numero_vencidos))  / fn_dias_entre2_fechas (?, ?),0), " );
			strSQL.append("           (SUM (fn_monto_vencidos))   / fn_dias_entre2_fechas (?, ?) " );
			strSQL.append("    FROM bit_doctos_global " );
			strSQL.append("   WHERE df_fecha_proc BETWEEN TO_DATE (?, 'dd/mm/yyyy') AND TO_DATE (?, 'dd/mm/yyyy') + 1" );

      varBind.add(fecha_operacion_ini);
      varBind.add(fecha_operacion_fin);
      varBind.add(fecha_operacion_ini);
      varBind.add(fecha_operacion_fin);
			varBind.add(fecha_operacion_ini);
      varBind.add(fecha_operacion_fin);
			varBind.add(fecha_operacion_ini);
      varBind.add(fecha_operacion_fin);
      varBind.add(fecha_operacion_ini);
      varBind.add(fecha_operacion_fin);
			varBind.add(fecha_operacion_ini);
      varBind.add(fecha_operacion_fin);
			varBind.add(fecha_operacion_ini);
      varBind.add(fecha_operacion_fin);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List registroReporteDoctosGlobal = new ArrayList();
        registroReporteDoctosGlobal.add(rst.getString(1)==null?"":rst.getString(1));
        registroReporteDoctosGlobal.add(rst.getString(2)==null?"":rst.getString(2));
        registroReporteDoctosGlobal.add(rst.getString(3)==null?"":rst.getString(3));
        registroReporteDoctosGlobal.add(rst.getString(4)==null?"":rst.getString(4));
        registroReporteDoctosGlobal.add(rst.getString(5)==null?"":rst.getString(5));
        registroReporteDoctosGlobal.add(rst.getString(6)==null?"":rst.getString(6));
        reporteDoctosGlobal.add(registroReporteDoctosGlobal);
      }
      rst.close();
      pst.close();

    }catch(Exception e){
      LOG.info("..:: reporteDoctosGlobalNafinsaMovil (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteDoctosGlobalNafinsaMovil(F)");
    return reporteDoctosGlobal;
	}

/**
 * M�todo que genera el reporte Layout 3 de Documentos Operados VS Negociables Pymes.
 * @return reportePymesGlobal Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 028 - 2009 Reportes Nafinsa M�vil
 * @author Erik Barba Alatriste
 */
	@Override
  public List reportePymesGlobalNafinsaMovil() throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List reporteDoctosGlobal = new ArrayList();

    LOG.info("..:: reportePymesGlobalNafinsaMovil(I)");
    try {
      con.conexionDB();
			//No. Pymes Afiliadas
			strSQL.append(" SELECT COUNT (1) ");
			strSQL.append("   FROM comrel_pyme_epo");
			strSQL.append("  WHERE cs_habilitado = 'S'  ");
      LOG.debug("..:: strSQL : "+strSQL.toString());
      pst = con.queryPrecompilado(strSQL.toString());
      rst = pst.executeQuery();
      if(rst.next()){
        reporteDoctosGlobal.add(rst.getString(1)==null?"":rst.getString(1));
      }
      rst.close();
      pst.close();

			strSQL = new StringBuffer();
			//No. Pymes Afiliadas Habilitadas
			strSQL.append(" SELECT COUNT (1) ");
			strSQL.append("   FROM comrel_pyme_epo");
			strSQL.append("  WHERE cs_habilitado = 'S' AND cs_aceptacion = 'H' ");
      LOG.debug("..:: strSQL : "+strSQL.toString());
      pst = con.queryPrecompilado(strSQL.toString());
      rst = pst.executeQuery();
      if(rst.next()){
        reporteDoctosGlobal.add(rst.getString(1)==null?"":rst.getString(1));
      }
      rst.close();
      pst.close();

			strSQL = new StringBuffer();
			//No. Pymes con el servicio nafinsa Movil
			strSQL.append(" SELECT /*+ use_nl(rel prom) index(prom IN_COM_PROMO_PYME_01_NUK) */ COUNT (DISTINCT prom.ic_pyme) ");
			strSQL.append("   FROM comrel_pyme_epo rel, com_promo_pyme prom ");
			strSQL.append("  WHERE rel.cs_habilitado = 'S' ");
			strSQL.append("    AND rel.cs_aceptacion = 'H' ");
			strSQL.append("    AND rel.ic_pyme = prom.ic_pyme ");
			strSQL.append("    AND prom.cs_activado = 'S' ");

      LOG.debug("..:: strSQL : "+strSQL.toString());
      pst = con.queryPrecompilado(strSQL.toString());
      rst = pst.executeQuery();
      if(rst.next()){
        reporteDoctosGlobal.add(rst.getString(1)==null?"":rst.getString(1));
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("..:: reportePymesGlobalNafinsaMovil (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reportePymesGlobalNafinsaMovil(F)");
    return reporteDoctosGlobal;
	}

/**
	 * M�todo que genera el reporte de Documentos Vencidos VS Notificados.
 * @param 1) fecha_vencimiento_ini Fecha inicial de operacion.
 * @param 2) fecha_vencimiento_fin Fecha final de operacion.
	 * @return resultado Lista con la informaci�n del catalogo y sus totales.
	 * @throws NafinException Excepcion lanzada cuando ocurre un error.
	 * @since FODEA 028 - 2009 Reportes Nafinsa M�vil
	 * @author Erik Barba Alatriste
	 */
	@Override
  public List reporteVencidosNotificadosNafinsaMovil(String fecha_vencimiento_ini, String fecha_vencimiento_fin) throws NafinException{
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    List reporteVencidoNotificadoNafinsaMovil = new ArrayList();
		List reporteTotalVencidoNotificadoNafinsaMovil = new ArrayList();
		List resultado = new ArrayList();

    LOG.info("..:: reporteVencidosNotificadosNafinsaMovil(I)");
    try {
      con.conexionDB();

			strSQL.append("SELECT to_char(df_fecha_vencimiento,'dd/mm/yyyy'), ic_nafin_electronico, ic_pyme, cg_razon_social, " );
			strSQL.append("       ig_numero_vencidos, fn_monto_vencidos, ig_numero_notificaciones, " );
			strSQL.append("       fn_costo_notificaciones " );
			strSQL.append("  FROM bit_doctos_vn " );
			strSQL.append("   WHERE df_fecha_vencimiento BETWEEN TO_DATE(?, 'dd/mm/yyyy') AND TO_DATE(?, 'dd/mm/yyyy')  ");
			strSQL.append("  order by 1 " );

			varBind.add(fecha_vencimiento_ini);
      varBind.add(fecha_vencimiento_fin);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      while(rst.next()){
        List registroReporteVencidoNotifiado = new ArrayList();
        registroReporteVencidoNotifiado.add(rst.getString(1)==null?"":rst.getString(1));
        registroReporteVencidoNotifiado.add(rst.getString(2)==null?"":rst.getString(2));
        registroReporteVencidoNotifiado.add(rst.getString(3)==null?"":rst.getString(3));
        registroReporteVencidoNotifiado.add(rst.getString(4)==null?"":rst.getString(4));
        registroReporteVencidoNotifiado.add(rst.getString(5)==null?"":rst.getString(5));
        registroReporteVencidoNotifiado.add(rst.getString(6)==null?"":rst.getString(6));
        registroReporteVencidoNotifiado.add(rst.getString(7)==null?"":rst.getString(7));
				registroReporteVencidoNotifiado.add(rst.getString(8)==null?"":rst.getString(8));
        reporteVencidoNotificadoNafinsaMovil.add(registroReporteVencidoNotifiado);
      }
      rst.close();
      pst.close();
			resultado.add(reporteVencidoNotificadoNafinsaMovil);

			//Sacando los totales
			strSQL = new StringBuffer();
			varBind = new ArrayList();

			strSQL.append("SELECT COUNT (DISTINCT ic_pyme), SUM (ig_numero_vencidos), " );
			strSQL.append("       SUM (fn_monto_vencidos), SUM (ig_numero_notificaciones), " );
			strSQL.append("       SUM (fn_costo_notificaciones) " );
			strSQL.append("  FROM bit_doctos_vn " );
			strSQL.append(" WHERE df_fecha_vencimiento BETWEEN TO_DATE (?, 'dd/mm/yyyy') AND TO_DATE (?, 'dd/mm/yyyy') " );
      varBind.add(fecha_vencimiento_ini);
      varBind.add(fecha_vencimiento_fin);
      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();
      while(rst.next()){
        List registroReporteVencidoNotificado = new ArrayList();
        registroReporteVencidoNotificado.add(rst.getString(1)==null?"":rst.getString(1));
        registroReporteVencidoNotificado.add(rst.getString(2)==null?"":rst.getString(2));
        registroReporteVencidoNotificado.add(rst.getString(3)==null?"":rst.getString(3));
        registroReporteVencidoNotificado.add(rst.getString(4)==null?"":rst.getString(4));
        registroReporteVencidoNotificado.add(rst.getString(5)==null?"":rst.getString(5));
        reporteTotalVencidoNotificadoNafinsaMovil.add(registroReporteVencidoNotificado);
      }
      rst.close();
      pst.close();
			resultado.add(reporteTotalVencidoNotificadoNafinsaMovil);
    }catch(Exception e){
      LOG.info("..:: reporteVencidosNotificadosNafinsaMovil (ERROR) : ");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("..:: reporteVencidosNotificadosNafinsaMovil(F)");
    return resultado;
	}

/**
 * M�todo genera el catalogo de Meses para la pantalla de reporte de operacion de pymes en cadenas por mes.
 * @return catalogoMeses Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 001 - 2010
 * @author Alberto Cruz Flores
 */
	@Override
  public List obtenerCatalogoMeses() throws AppException{
    LOG.info("obtenerCatalogoMeses(I) ::..");
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List catalogoMeses = new ArrayList();

    try {
      con.conexionDB();

      strSQL.append(" SELECT '01' AS clave, 'Enero' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '02' AS clave, 'Febrero' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '03' AS clave, 'Marzo' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '04' AS clave, 'Abril' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '05' AS clave, 'Mayo' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '06' AS clave, 'Junio' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '07' AS clave, 'Julio' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '08' AS clave, 'Agosto' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '09' AS clave, 'Septiembre' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '10' AS clave, 'Octubre' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '11' AS clave, 'Noviembre' AS descripcion FROM dual UNION");
      strSQL.append(" SELECT '12' AS clave, 'Diciembre' AS descripcion FROM dual");
      //LOG.debug("..:: strSQL : "+strSQL.toString());
      //LOG.debug("..:: varBind : "+varBind.toString());
      pst = con.queryPrecompilado(strSQL.toString());
      rst = pst.executeQuery();

      while(rst.next()){
        ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(rst.getString("CLAVE"));
				elementoCatalogo.setDescripcion(rst.getString("DESCRIPCION"));
        catalogoMeses.add(elementoCatalogo);
      }
      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("obtenerCatalogoMeses(Error) ::..");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
    LOG.info("obtenerCatalogoMeses(F) ::..");
    return catalogoMeses;
	}

/**
 * M�todo genera el catalogo de A�os para la pantalla de reporte de operacion de pymes en cadenas por mes.
 * @return catalogoAnios Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 001 - 2010
 * @author Alberto Cruz Flores
 */
	@Override
  public List obtenerCatalogoAnios() throws AppException{
    LOG.info("obtenerCatalogoAnios(I) ::..");
    List catalogoAnios = new ArrayList();

    try {
      SimpleDateFormat formato = new SimpleDateFormat("yyyy");
      Calendar calendario = Calendar.getInstance();
      String sAnio = formato.format(calendario.getTime());
      int iAnio = Integer.parseInt(sAnio);

      for (int i = 0; i < 3; i++) {
        ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(Integer.toString(iAnio));
				elementoCatalogo.setDescripcion(Integer.toString(iAnio));
        catalogoAnios.add(elementoCatalogo);
        iAnio -= 1;
      }
    }catch(Exception e){
      LOG.info("obtenerCatalogoAnios(Error) ::..");
      e.printStackTrace();
    }finally{
      LOG.info("obtenerCatalogoAnios(F) ::..");
    }
    return catalogoAnios;
	}

/**
 * M�todo genera el catalogo de Meses para la pantalla de reporte de operacion de pymes en cadenas por mes.
 * @return catalogoMeses Lista con la informaci�n del catalogo.
 * @throws NafinException Excepcion lanzada cuando ocurre un error.
 * @since FODEA 001 - 2010
 * @author Alberto Cruz Flores
 */
	@Override
  public HashMap generaReporteOperacionPymesCadenasMes(HashMap datosConsulta) throws AppException{
    LOG.info("generaReporteOperacionPymesCadenasMes(I) ::..");
    AccesoDB con = new AccesoDB();
    PreparedStatement pst = null;
    ResultSet rst = null;
    StringBuffer strSQL = new StringBuffer();
    List varBind = new ArrayList();
    HashMap reporteOperacionPymesCadenasMes = new HashMap();

    try {
      con.conexionDB();

      String banco_fondeo = datosConsulta.get("banco_fondeo") == null?"":(String)datosConsulta.get("banco_fondeo");
      String mesOperacion = datosConsulta.get("mesOperacion") == null?"":(String)datosConsulta.get("mesOperacion");
      String anioOperacion = datosConsulta.get("anioOperacion") == null?"":(String)datosConsulta.get("anioOperacion");

      String fechaInicial = "01/" + mesOperacion + "/" + anioOperacion;
      int diasDelMes = Fecha.getNumeroDeDiasDelMes(fechaInicial, "dd/MM/yyyy");
      String fechaFinal = diasDelMes + "/" + mesOperacion + "/" + anioOperacion;

      strSQL.append(" SELECT pu.pymes_unicas AS pymes_unicas_afiliadas,");
      strSQL.append(" pup.pymes_con_pub AS pymes_con_publicacion,");
      strSQL.append(" (pup.pymes_con_pub * 100 / pu.pymes_unicas) AS pc_pymes_con_pub,");
      strSQL.append(" pup.monto_pub AS monto_publicado,");
      strSQL.append(" pup.doctos_publicados AS numero_doctos_pub,");
      strSQL.append(" puo.monto_ope AS monto_operado,");
      strSQL.append(" puo.doctos_operados AS numero_doctos_ope");
      strSQL.append(" FROM (");
      strSQL.append(" SELECT COUNT(pymes) AS pymes_con_pub, SUM(doctos_pub) AS doctos_publicados, SUM(monto) AS monto_pub");
      strSQL.append(" FROM(");
      strSQL.append(" SELECT /*+ordered use_nl(docto pe pyme)*/");
      strSQL.append(" pe.ic_pyme pymes,");
      strSQL.append(" COUNT(docto.ic_documento) AS doctos_pub,");
      strSQL.append(" SUM (docto.fn_monto) AS monto");
      strSQL.append(" FROM com_documento docto,");
      strSQL.append(" comrel_pyme_epo pe,");
      strSQL.append(" comcat_pyme pyme");
      strSQL.append(" WHERE pe.ic_epo = docto.ic_epo");
      strSQL.append(" AND pe.ic_pyme = docto.ic_pyme");
      strSQL.append(" AND pe.ic_pyme = pyme.ic_pyme");
      strSQL.append(" AND pyme.cs_habilitado = ?");
      strSQL.append(" AND docto.df_alta >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND docto.df_alta < TO_DATE(?, 'dd/mm/yyyy') + 1");
      strSQL.append(" GROUP BY pe.ic_pyme) pymes_unicas_pub) pup,");
      strSQL.append(" (SELECT SUM(pymes_afiliadas) AS pymes_unicas");
      strSQL.append(" FROM (");
      strSQL.append(" SELECT /*+use_nl(epo cpe pyme)*/");
      strSQL.append(" epo.ic_epo AS ic_epo,");
      strSQL.append(" COUNT (cpe.ic_pyme) AS pymes_afiliadas");
      strSQL.append(" FROM comrel_pyme_epo cpe, comcat_epo epo, comcat_pyme pyme");
      strSQL.append(" WHERE cpe.ic_epo = epo.ic_epo");
      strSQL.append(" AND cpe.ic_pyme = pyme.ic_pyme");
      strSQL.append(" AND pyme.cs_habilitado = ?");
      strSQL.append(" AND epo.cs_habilitado = ?");
      if (!banco_fondeo.equals("")) {
        strSQL.append(" AND epo.ic_banco_fondeo = ?");
      }
      strSQL.append(" GROUP BY epo.ic_epo) pymes_unicas) pu,");
      strSQL.append(" (SELECT COUNT(ic_pyme) AS pymes_con_ope, SUM(doctos_ope) AS doctos_operados, SUM(monto) monto_ope");
      strSQL.append(" FROM (");
      strSQL.append(" SELECT /*+use_nl(sol docto pyme)*/");
      strSQL.append(" docto.ic_pyme,");
      strSQL.append(" COUNT(docto.ic_documento) AS doctos_ope,");
      strSQL.append(" SUM(docto.fn_monto) AS monto");
      strSQL.append(" FROM com_solicitud sol, com_documento docto, comcat_pyme pyme");
      strSQL.append(" WHERE sol.ic_documento = docto.ic_documento");
      strSQL.append(" AND docto.ic_pyme = pyme.ic_pyme");
      strSQL.append(" AND pyme.cs_habilitado = ?");
      strSQL.append(" AND docto.ic_estatus_docto IN (?, ?, ?)");
      if (!banco_fondeo.equals("")) {
        strSQL.append(" AND sol.ic_banco_fondeo = ?");
      }
      strSQL.append(" AND sol.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')");
      strSQL.append(" AND sol.df_operacion < TO_DATE(?, 'dd/mm/yyyy') + 1");
      strSQL.append(" GROUP BY docto.ic_pyme) pymes_unicas_ope) puo");

      varBind.add("S");
      varBind.add(fechaInicial);//Fecha de alta de docto
      varBind.add(fechaFinal);
      varBind.add("S");
      varBind.add("S");
      if (!banco_fondeo.equals("")) {
        varBind.add(new Integer(banco_fondeo));//banco de fondeo
      }
      varBind.add("S");
      varBind.add(new Integer(4));//estatus docto
      varBind.add(new Integer(11));//estatus docto
      varBind.add(new Integer(12));//estatus docto
      if (!banco_fondeo.equals("")) {
        varBind.add(new Integer(banco_fondeo));//banco de fondeo
      }
      varBind.add(fechaInicial);//Fecha de alta de operacion docto
      varBind.add(fechaFinal);

      LOG.debug("..:: strSQL : "+strSQL.toString());
      LOG.debug("..:: varBind : "+varBind.toString());

      pst = con.queryPrecompilado(strSQL.toString(), varBind);
      rst = pst.executeQuery();

      int indice = 0;

      while(rst.next()){
        HashMap operacionPorMes = new HashMap();
        operacionPorMes.put("pymesUnicasAfiliadas", rst.getString("pymes_unicas_afiliadas")==null?"":rst.getString("pymes_unicas_afiliadas"));
        operacionPorMes.put("pymesConPublicacion", rst.getString("pymes_con_publicacion")==null?"":rst.getString("pymes_con_publicacion"));
        operacionPorMes.put("pcPymesConPub", rst.getString("pc_pymes_con_pub")==null?"":rst.getString("pc_pymes_con_pub"));
        operacionPorMes.put("montoPublicado", rst.getString("monto_publicado")==null?"":rst.getString("monto_publicado"));
        operacionPorMes.put("numeroPoctosPub", rst.getString("numero_doctos_pub")==null?"":rst.getString("numero_doctos_pub"));
        operacionPorMes.put("montoOperado", rst.getString("monto_operado")==null?"":rst.getString("monto_operado"));
        operacionPorMes.put("numeroDoctosOpe", rst.getString("numero_doctos_ope")==null?"":rst.getString("numero_doctos_ope"));
        reporteOperacionPymesCadenasMes.put("operacionPorMes" + indice, operacionPorMes);
        indice++;
      }

      reporteOperacionPymesCadenasMes.put("numeroRegistros", Integer.toString(indice));

      rst.close();
      pst.close();
    }catch(Exception e){
      LOG.info("generaReporteOperacionPymesCadenasMes(Error) ::..");
      e.printStackTrace();
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
      LOG.info("generaReporteOperacionPymesCadenasMes(F) ::..");
    }
    return reporteOperacionPymesCadenasMes;
	}

	/**
	 * Este m�todo valida si una pyme ya se encuentra afiliada habilitada con un IF.
	 * @param clavePyme clave interna de la Pyme a validar
	 * @param claveIf clave interna del if a validar
	 * @return relacionPymeIf cadena con el estatus de la relaci�n pyme - IF: SI HABILITADA � NO.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	@Override
	public String validaRelacionPymeIf(String clavePyme, String claveIf) throws AppException {
		LOG.info("validaRelacionPymeIf(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		String relacionPymeIf = "NO";
		try {
			con.conexionDB();

			strSQL.append(" SELECT DECODE(COUNT(ccb.ic_pyme), 0, 'N', 'S') AS pyme_if_habilitada");
			strSQL.append(" FROM comrel_cuenta_bancaria ccb");
			strSQL.append(", comrel_pyme_if cpi");
			strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
			strSQL.append(" AND cpi.ic_if = ?");
			strSQL.append(" AND ccb.ic_pyme = ?");
			strSQL.append(" AND ccb.ic_moneda = ?");
			strSQL.append(" AND cpi.cs_vobo_if = ?");
			strSQL.append(" AND cpi.cs_borrado = ?");

			varBind.add(new Long(claveIf));
			varBind.add(new Long(clavePyme));
			varBind.add(new Integer(1));
			varBind.add("S");
			varBind.add("N");
			//LOG.debug("..:: strSQL: "+strSQL.toString());
			//LOG.debug("..:: varBind: "+varBind);
			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			if (rst.next()) {
				if (rst.getString("pyme_if_habilitada").equals("S")) {
					relacionPymeIf = "SI HABILITADA";
				} else {
					relacionPymeIf = "NO";
				}
			}

			rst.close();
			pst.close();
		} catch (Exception e) {
			LOG.info("validaRelacionPymeIf(ERROR)");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			LOG.info("validaRelacionPymeIf(S)");
		}
		return relacionPymeIf;
	}
	
	/**
	 * Guarda en la base el reporte genardo Diario o Mensual para que pueda ser consultado posteriormente.
	 * @param strDirectorioTemp
	 * @throws AppException
	 * @param nombreZip
	 * @param cveIf
	 * @param Fcorte
	 */
	@Override
	public void guardarArchivoReporte(String nombreArchivo, String strDirectorioTemp, String origen) {
	    LOG.info("guardarArchivoReporte(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";
		boolean commit = true;
		try{
			con.conexionDB();
			int contador = 0;
			query = "SELECT count(*) " +
					"  FROM com_archivos_procesos " +
					" WHERE cg_nombre_archivo = ? ";
			
			ps = con.queryPrecompilado(query);
			ps.setString(1,nombreArchivo);
			rs = ps.executeQuery();
			
			if(rs.next()){
				contador = rs.getInt(1);
			}
			rs.close();
			ps.close();
			
			if(contador>0){
				query = "DELETE com_archivos_procesos " +
						"  WHERE cg_nombre_archivo = ? ";
				
				ps = con.queryPrecompilado(query);
				ps.setString(1,nombreArchivo);
				ps.executeUpdate();
				ps.close();
			}
			
			File fzip = new File(strDirectorioTemp+nombreArchivo);
			FileInputStream fis = new FileInputStream(fzip);
			query = "INSERT INTO com_archivos_procesos (cg_nombre_archivo, bi_archivo, cg_origen ) " +
					"     VALUES (?, ?, ?) ";

			ps = con.queryPrecompilado(query);
			ps.setString(1, nombreArchivo);
			ps.setBinaryStream(2, fis, fzip.length());
		    ps.setString(3, origen);
			ps.executeUpdate();
			ps.close();
			
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
		   LOG.error("Error al guardar el archivo  "+ e);
			throw new AppException("Error al guardar el archivo zip generado para la parte de Informacion operativa", e);
		   
		} finally {
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		   LOG.info("guardarArchivoReporte(S)");
		}
	}
	
	/**
	 * Funci�n que crea el archivo CSV.
	 * @param mesInicial
	 * @param anioInicial
	 * @param mesFinal
	 * @param anioFinal
	 * @param sPeriodo1
	 * @param sPeriodo2
	 * @param sPeriodo3
	 * @param sNoEposSelec
	 * @param strDirectorioTemp
	 * @return
	 */
	@Override
	public String generarArchivo(String 	mesInicial,String 	anioInicial,String 	mesFinal,String 	anioFinal,String 	sPeriodo1,String 	sPeriodo2,String 	sPeriodo3,String 	sNoEposSelec,String 	strDirectorioTemp){	 
			
			LOG.info("generarArchivo (E)");
			AccesoDB 	con 		= new AccesoDB();
			String nombreArchivo = "";
			Registros 		rs 			= null;
			List lVarBind = new ArrayList();
			OutputStreamWriter writer = null;
			BufferedWriter buffer = null;
			int total = 0;	
			StringBuffer strSQL = new StringBuffer();
			boolean bSeis = false;
			boolean bOcho = false;
			boolean bTodos = false;
			StringBuffer sbContenido = new StringBuffer(); 
			try {
				con.conexionDB();
					
				String sNoEposAux = "";
				String sNoEpos1 = sNoEposAux;
				StringTokenizer st = new StringTokenizer(sNoEposSelec, ",");
				while (st.hasMoreTokens()) {
					LOG.debug("st:*** rrrr"+st.nextToken());
					if("".equals(sNoEposAux))
						sNoEposAux += "?";
					else
						sNoEposAux += ",?";
				}
				
				if (sPeriodo1.equals("true")) {
					bSeis = true;
				}
				if (sPeriodo2.equals("true")) {
					bOcho = true;
				}
				if (sPeriodo3.equals("true")) {
					bTodos = true;
				}
				
				if (sNoEpos1.lastIndexOf(",") != -1) {
					int indiceUltimaComa = sNoEpos1.lastIndexOf(",");
					if (indiceUltimaComa == (sNoEpos1.length() - 1)) {
						sNoEpos1 = sNoEpos1.substring(0, sNoEpos1.lastIndexOf(","));
					}
				}
				
				SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat sdfAnio = new SimpleDateFormat("yyyy");
				SimpleDateFormat sdfMes = new SimpleDateFormat("MM");
				
				Calendar gcFechaFinal = new GregorianCalendar(Integer.parseInt(anioFinal), Integer.parseInt(mesFinal), 1);
				String sFechaFin = sdfFecha.format(gcFechaFinal.getTime());
				String sAnioFin = sdfAnio.format(gcFechaFinal.getTime());
				String sMesFin = sdfMes.format(gcFechaFinal.getTime());
				
				Calendar gcFechaInicial = new GregorianCalendar(Integer.parseInt(anioInicial), Integer.parseInt(mesInicial) - 1, 1);
				String sFechaIni = sdfFecha.format(gcFechaInicial.getTime());
				String sAnioIni = sdfAnio.format(gcFechaInicial.getTime());
				String sMesIni = sdfMes.format(gcFechaInicial.getTime());
				
				sbContenido = new StringBuffer("IC_EPO,EPO,NUM. PYME,CRED. CAD,NUM. SIRAC,PYME,ESTADO,C.P.,TEL 1,RFC,SECTOR,SUBSECTOR,RAMA,");
	
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				LOG.debug("strDirectorioTemp *** "+strDirectorioTemp);
				writer = new OutputStreamWriter(new FileOutputStream(strDirectorioTemp + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				
				
				strSQL.append("	 SELECT MES.ic_mes AS mes, ANO.ic_anio AS anio, MES.nombre||'-'||ANO.ic_anio as nomMesAnio");
				strSQL.append(" FROM ( ");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual UNION");
				strSQL.append(" SELECT ? AS ic_mes, ? AS nombre FROM dual) MES,");
				strSQL.append(" (SELECT ? AS ic_anio from dual UNION");
				strSQL.append(" SELECT ? AS ic_anio from dual) ANO");
				strSQL.append(" WHERE (MES.ic_mes >= ? AND ANO.ic_anio = ?) OR (MES.ic_mes < ? AND ANO.ic_anio = ?)");
				strSQL.append(" ORDER BY ANO.ic_anio, MES.ic_mes");
				LOG.debug("==========>> strSQL: "+strSQL);
				ArrayList alMes = new ArrayList();
				ArrayList alAnio = new ArrayList();
				int i = 0;
				lVarBind = new ArrayList();
				lVarBind.add(new Integer(1));
				lVarBind.add("ENE");
				lVarBind.add(new Integer(2));
				lVarBind.add("FEB");
				lVarBind.add(new Integer(3));
				lVarBind.add("MAR");
				lVarBind.add(new Integer(4));
				lVarBind.add("ABR");
				lVarBind.add(new Integer(5));
				lVarBind.add("MAY");
				lVarBind.add(new Integer(6));
				lVarBind.add("JUN");
				lVarBind.add(new Integer(7));
				lVarBind.add("JUL");
				lVarBind.add(new Integer(8));
				lVarBind.add("AGO");
				lVarBind.add(new Integer(9));
				lVarBind.add("SEP");
				lVarBind.add(new Integer(10));
				lVarBind.add("OCT");
				lVarBind.add(new Integer(11));
				lVarBind.add("NOV");
				lVarBind.add(new Integer(12));
				lVarBind.add("DIC");
			    lVarBind.add(new Integer(sAnioIni));
				lVarBind.add(new Integer(sAnioFin));
			    lVarBind.add(new Integer(sMesIni));
			    lVarBind.add(new Integer(sAnioIni));
			    lVarBind.add(new Integer(sMesFin));
			    lVarBind.add(new Integer(sAnioFin));
				rs = con.consultarDB(strSQL.toString(), lVarBind);
				while (rs.next()) {
					alMes.add(i, rs.getString("mes"));
					alAnio.add(i, rs.getString("anio"));
					sbContenido.append(rs.getString("nomMesAnio")+",");
					i++;
				}						
				
				sbContenido.append("TIPO DE CRITERIO,");	
				
				strSQL = new StringBuffer();
				HashMap ifsAfiliados = new HashMap();
				i = 0;
				strSQL.append(" SELECT DISTINCT cif.ic_if AS ic_if,");
				strSQL.append(" cif.cg_razon_social AS nombre_if");
				strSQL.append(" FROM comrel_if_epo cie");
				strSQL.append(", comcat_if cif");
				strSQL.append(" WHERE cif.ic_if = cie.ic_if");
				strSQL.append(" AND cie.ic_epo IN (" + sNoEposAux + ")");
				strSQL.append(" AND cie.cs_vobo_nafin = ?");
				strSQL.append(" AND cie.cs_aceptacion = ?");
				strSQL.append(" AND cif.cs_habilitado = ?");
				strSQL.append(" ORDER BY cif.cg_razon_social");
				lVarBind = new ArrayList();
				st = new StringTokenizer(sNoEposSelec, ",");
				while (st.hasMoreTokens()) {
					lVarBind.add(new Integer(st.nextToken()));
				}
				lVarBind.add("S");
				lVarBind.add("S");
				lVarBind.add("S");
				LOG.debug("==========>> strSQL: "+strSQL.toString()+" "+lVarBind);
				rs = con.consultarDB(strSQL.toString(), lVarBind, true);
				while (rs.next()) {
					ifsAfiliados.put("ic_if" + i, rs.getString("ic_if"));
					sbContenido.append(rs.getString("nombre_if").toUpperCase().replaceAll(",", "") + ",");
					i++;
				}
				ifsAfiliados.put("numeroRegistros", Integer.toString(i));
				
				sbContenido.append("\r\n");
				
				lVarBind = new ArrayList();
				strSQL = new StringBuffer();
				strSQL.append(" SELECT /*+index(se) index(sub) index(es) index(ram) use_nl(mto p e dom es se sub ram)*/ ");	                       
				strSQL.append(" e.ic_epo AS ic_epo,");
				strSQL.append(" e.cg_razon_social AS nomEpo,");
				strSQL.append(" p.ic_pyme AS numPyme,");
				strSQL.append(" mto.credcad AS credcad,");
				strSQL.append(" p.in_numero_sirac AS numSirac,");
				strSQL.append(" REPLACE(p.cg_razon_social, ',', '') AS nomPyme,");
				strSQL.append(" es.cd_nombre AS nomEstado,");
				strSQL.append(" dom.cn_cp AS CP,");
				strSQL.append(" dom.cg_telefono1 AS tel1,");
				strSQL.append(" dom.cg_telefono2 AS tel2,");
				strSQL.append(" p.cg_rfc AS rfcPyme,");
				strSQL.append(" se.cd_nombre AS sector,");
				strSQL.append(" sub.cd_nombre AS subsector,");
				strSQL.append(" ram.cd_nombre AS rama,");
				for (i = 0; i < alMes.size(); i++) {
					strSQL.append(" mto.mes" + (i + 1) + ",");
				}
				strSQL.append(" CASE WHEN(INSTR((");
				for (i = 6; i < alMes.size(); i++) {
					strSQL.append(" DECODE(mto.mes"+ (i + 1) + ", ?, ?, ?) || '' ||");
					lVarBind.add(new Integer(0));
					lVarBind.add(new Integer(0));
					lVarBind.add(new Integer(1));
				}
				strSQL.delete(strSQL.length() - 9, strSQL.length());
				strSQL.append("), ?) <> ?) THEN ? ELSE ? END AS SeisConsec,");
				lVarBind.add("111111");
				lVarBind.add(new Integer(0));
				lVarBind.add("SI");
				lVarBind.add("NO");
				strSQL.append(" CASE WHEN((");	
				for( i = 0; i < alMes.size(); i++) {
					strSQL.append(" DECODE(mto.mes"+ (i + 1) + ", ?, ?, ?)+");
					lVarBind.add(new Integer(0));
					lVarBind.add(new Integer(0));
					lVarBind.add(new Integer(1));
				}
				strSQL.delete(strSQL.length() - 1, strSQL.length());
				strSQL.append(") >= ?) THEN ? ELSE ? END AS OchoDeDoce");
				lVarBind.add(new Integer(8));
				lVarBind.add("SI");
				lVarBind.add("NO");	
				strSQL.append(" FROM comcat_pyme p,");
				strSQL.append(" comcat_epo e,");
				strSQL.append(" com_domicilio dom,");
				strSQL.append(" comcat_estado es,");
				strSQL.append(" comcat_sector_econ se,");
				strSQL.append(" comcat_subsector sub,");
				strSQL.append(" comcat_rama ram,");
				strSQL.append(" (");
				strSQL.append(" SELECT m.ic_epo,");
				strSQL.append(" m.ic_pyme,");
				strSQL.append(" m.credcad,");
				for (i = 0; i < alMes.size(); i++) {
					strSQL.append(" SUM(m.m" + (i + 1) + ") AS mes" + (i + 1) + ",");
				}
				strSQL.delete(strSQL.length() - 1, strSQL.length());
				strSQL.append(" FROM");
				strSQL.append(" (SELECT /*+use_nl(d a pexp)*/");
				strSQL.append(" d.ic_epo,");
				strSQL.append(" d.ic_pyme,");
				strSQL.append(" DECODE(pexp.cs_habilitado, 'S', 'YA', 'H', 'YA', 'NO') AS credcad,");
				for (i = 0; i < alMes.size(); i++) {
					strSQL.append(" CASE WHEN ( ? = TO_NUMBER(TO_CHAR(D.df_alta,'MM'))) AND ( ? = TO_NUMBER(TO_CHAR(D.df_alta,'YYYY'))) THEN SUM(D.fn_monto) ELSE ? END AS m" + (i + 1) + ",");
					lVarBind.add(new Integer(alMes.get(i).toString()));
					lVarBind.add(new Integer(alAnio.get(i).toString()));
					lVarBind.add(new Integer(0));
				}
				strSQL.delete(strSQL.length() - 1, strSQL.length());
				strSQL.append(" FROM com_documento D, com_acuse1 a, comrel_pyme_epo_x_producto PEXP ");	
				strSQL.append(" WHERE a.cc_acuse = d.cc_acuse ");
				strSQL.append(" AND D.ic_epo = PEXP.ic_epo(+)");
				strSQL.append(" AND D.ic_pyme = PEXP.ic_pyme(+)");
				strSQL.append(" AND a.DF_FECHAHORA_CARGA >= TO_DATE(?, 'DD/MM/YYYY')");
				lVarBind.add(sFechaIni);
				strSQL.append(" AND a.DF_FECHAHORA_CARGA < TO_DATE(?, 'DD/MM/YYYY')");
				lVarBind.add(sFechaFin);
				strSQL.append(" AND D.ic_moneda = ?");
				lVarBind.add(new Integer(1));
				strSQL.append(" AND D.ic_estatus_docto <> ?");
				lVarBind.add(new Integer(1));
				strSQL.append(" AND D.ic_epo IN (" + sNoEposAux + ")");
				st = new StringTokenizer(sNoEposSelec, ",");
				while (st.hasMoreTokens()) {
					lVarBind.add(new Integer(st.nextToken()));
				}
				strSQL.append(" AND PEXP.ic_producto_nafin(+) = ?");
				lVarBind.add(new Integer(5));
				strSQL.append(" GROUP BY d.ic_epo, d.ic_pyme, TO_NUMBER(TO_CHAR(D.df_alta,'YYYY')), TO_NUMBER(TO_CHAR(D.df_alta,'MM'))");
				strSQL.append(", DECODE (pexp.cs_habilitado, 'S', 'YA', 'H', 'YA', 'NO')) M");
				strSQL.append(" GROUP BY M.ic_epo, M.ic_pyme, m.credcad");
				strSQL.append(" ) MTO");
				strSQL.append(" WHERE E.ic_epo = mto.ic_epo");
				strSQL.append(" AND P.ic_pyme = mto.ic_pyme");
				strSQL.append(" AND P.ic_pyme = DOM.ic_pyme");
				strSQL.append(" AND p.ic_sector_econ = se.ic_sector_econ");
				strSQL.append(" AND p.ic_sector_econ = sub.ic_sector_econ");
				strSQL.append(" AND p.ic_subsector = sub.ic_subsector");
				strSQL.append(" AND p.ic_sector_econ = ram.ic_sector_econ");
				strSQL.append(" AND p.ic_subsector = ram.ic_subsector");
				strSQL.append(" AND p.ic_rama = ram.ic_rama");
				strSQL.append(" AND DOM.ic_estado = ES.ic_estado");
				if (!bTodos) {
					if (bSeis && bOcho) {
						strSQL.append(" AND ( ( INSTR( ( ");
						for (i = 6; i < alMes.size(); i++) {
							strSQL.append(" DECODE(mto.mes"+ (i + 1) + ", ?, ?, ?) || '' ||");
							lVarBind.add(new Integer(0));
							lVarBind.add(new Integer(0));
							lVarBind.add(new Integer(1));
						}
						strSQL.delete(strSQL.length() - 9, strSQL.length());
						strSQL.append(" ), ?) <> ?)");
						lVarBind.add("111111");
						lVarBind.add(new Integer(0));
						strSQL.append(" OR ( ");
						for (i = 0; i < alMes.size(); i++) {
							strSQL.append(" DECODE(mto.mes" + (i + 1) + ", ?, ?, ?)+");
							lVarBind.add(new Integer(0));
							lVarBind.add(new Integer(0));
							lVarBind.add(new Integer(1));
						}
						strSQL.delete(strSQL.length() - 1, strSQL.length());
						strSQL.append(" >= ?) )");
						lVarBind.add(new Integer(8));
					} else if (bSeis) {
						strSQL.append(" AND INSTR( (");
						for (i = 6; i < alMes.size(); i++) {
							strSQL.append(" DECODE(mto.mes" + (i + 1) + ", ?, ?, ?) || '' ||");
							lVarBind.add(new Integer(0));
							lVarBind.add(new Integer(0));
							lVarBind.add(new Integer(1));
						}
						strSQL.delete(strSQL.length() - 9, strSQL.length());
						strSQL.append(" ), ?) <> ? ");
						lVarBind.add("111111");
						lVarBind.add(new Integer(0));
					} else if (bOcho) {
						strSQL.append(" AND (");
						for (i = 0; i < alMes.size(); i++) {
							strSQL.append(" DECODE(mto.mes" + (i + 1) + ", ?, ?, ?)+");
							lVarBind.add(new Integer(0));
							lVarBind.add(new Integer(0));
							lVarBind.add(new Integer(1));
						}
						strSQL.delete(strSQL.length() - 1, strSQL.length());
						strSQL.append(" ) >= ? ");
						lVarBind.add(new Integer(8));
					}
				}
				strSQL.append(" GROUP BY E.ic_epo,E.cg_razon_social, P.ic_pyme, ");
				strSQL.append(" mto.credcad,");
				strSQL.append(" P.in_numero_sirac, P.in_numero_sirac, P.cg_razon_social, ES.cd_nombre,");
				strSQL.append(" DOM.cn_cp, DOM.cg_telefono1, DOM.cg_telefono2, P.cg_rfc,");
				strSQL.append(" se.cd_nombre,");
				strSQL.append(" sub.cd_nombre,");
				strSQL.append(" ram.cd_nombre,");
				for (i = 0; i < alMes.size(); i++) {
					strSQL.append(" MTO.mes" + (i + 1) + ",");
				}
				strSQL.delete(strSQL.length() - 1, strSQL.length());
				//LOG.debug("==========>> strSQL: "+strSQL.toString()+" "+lVarBind);
			//<<==============================================================================		
				boolean bHayReg = false;
				//rs = con.queryDB(sQuery);
				rs = con.consultarDB(strSQL.toString(), lVarBind, true);
				while(rs.next()) {
					String tipoCriterio = "Ninguno";
					sbContenido.append(
						rs.getString("ic_epo")+
						","+(rs.getString("nomEpo")==null?"":rs.getString("nomEpo").replace(',',' '))+
						","+rs.getString("numPyme")+
						","+rs.getString("credCad")+
						","+(rs.getString("numSirac")==null?"":rs.getString("numSirac"))+
						","+(rs.getString("nomPyme")==null?"":rs.getString("nomPyme"))+
						","+(rs.getString("nomEstado")==null?"":rs.getString("nomEstado").replace(',',' '))+
						","+rs.getString("CP")+
						","+(rs.getString("tel1")==null?"":rs.getString("tel1"))+
						","+rs.getString("rfcPyme")+
						","+(rs.getString("sector")==null?"":rs.getString("sector").replace(',',' '))+
						","+(rs.getString("subsector")==null?"":rs.getString("subsector").replace(',',' '))+
						","+(rs.getString("rama")==null?"":rs.getString("rama").replace(',',' '))+
						",");
					for(i=0; i<alMes.size(); i++)
						sbContenido.append(rs.getString("mes"+(i+1))+",");
					/*if(bTodos || (bSeis && bOcho))
						sbContenido.append(rs.getString("SeisConsec")+","+rs.getString("OchoDeDoce"));*/
					if("SI".equals(rs.getString("SeisConsec"))&&(bSeis||bTodos)){
						tipoCriterio = "�ltimos seis meses consecutivos";
					}else if("SI".equals(rs.getString("OchoDeDoce"))&&(bOcho||bTodos)){
						tipoCriterio = "Ocho de Doce";
					}
					sbContenido.append(tipoCriterio);
					sbContenido.append(",");
			//<<============================================================================
					int numIfsAfil = Integer.parseInt(ifsAfiliados.get("numeroRegistros").toString());
					for (int j = 0; j < numIfsAfil; j++) {
						strSQL = new StringBuffer();
						
						strSQL.append(" SELECT DECODE(COUNT(ccb.ic_pyme), ?, ?, ?) AS pyme_if_habilitada");
						strSQL.append(" FROM comrel_cuenta_bancaria ccb");
						strSQL.append(", comrel_pyme_if cpi");
						strSQL.append(" WHERE cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
						strSQL.append(" AND cpi.ic_epo = ? ");
						strSQL.append(" AND cpi.ic_if = ? ");
						strSQL.append(" AND ccb.ic_pyme = ? ");
						strSQL.append(" AND ccb.ic_moneda = ?");
						strSQL.append(" AND cpi.cs_vobo_if = ?");
						strSQL.append(" AND cpi.cs_borrado = ?");
						//LOG.debug("==========>> strSQL: "+strSQL.toString());
						lVarBind = new ArrayList();
						lVarBind.add(new Integer(0));
						lVarBind.add("N");
						lVarBind.add("S");
						lVarBind.add(new Integer(rs.getString("ic_epo")));
						lVarBind.add(new Integer(ifsAfiliados.get("ic_if" + j).toString()));
						lVarBind.add(new Integer(rs.getString("numPyme")));
						lVarBind.add(new Integer(1));
						lVarBind.add("S");
						lVarBind.add("N");
						Registros registros = con.consultarDB(strSQL.toString(), lVarBind, true);
						if (registros.next()) {
							if (registros.getString("pyme_if_habilitada").equals("S")) {
								sbContenido.append("SI HABILITADO,");
							} else {
								sbContenido.append("NO,");
							}
						}//if (registros.next())
					}//for (int j = 0; j < numIfsAfil; j++)
			//<<==============================================================================
					sbContenido.append("\r\n");
					bHayReg = true;
				}//while(rs.next())
				
			
				total++;
				LOG.debug("sbContenido.toString() WW "+sbContenido.toString());
				if(total==1000){					
					total=0;	
					buffer.write(sbContenido.toString());
					sbContenido = new StringBuffer();//Limpio  
				}	
				buffer.write(sbContenido.toString());
				buffer.close();	
				sbContenido = new StringBuffer();//Limpio
			} catch(Throwable e) {
				throw new AppException("Error al generar el  archivo PDF", e);
			} finally {
			    if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}		
			
			LOG.info("generarArchivo (S) "  + nombreArchivo);
			return nombreArchivo;
		}
	/**
	 * Metodo que genera el archivo Xls de la consulta realizada.
	 * @param strDirectorioPublicacion
	 * @param strDirectorioTemp
	 * @param num_nae
	 * @param claveAnioI
	 * @param claveAnioF
	 * @param claveMesI
	 * @param claveMesF
	 * @param claveAnioC
	 * @param claveMesC
	 * @return
	 */
	@Override
		public String generarArchNafinsaMovil(String strDirectorioPublicacion,String strDirectorioTemp,String num_nae,String claveAnioI,String claveAnioF,String claveMesI,String claveMesF ,String claveAnioC,String claveMesC){
			
		CreaArchivo archivo = new CreaArchivo();
		ComunesXLS xls = null;
		String nombreArchivo = archivo.nombreArchivo() + ".xls";
		
		xls = new ComunesXLS(strDirectorioTemp+nombreArchivo);
		
		LOG.info("getStreamInfo(I) ::..");
		
		try{
		  
					
			List reporteClientesRegNafinsaMovil = this.reporteClientesRegNafinsaMovil(num_nae, claveAnioI, claveAnioF, claveMesI, claveMesF, claveAnioC, claveMesC);
							
				BigDecimal numActivas = new BigDecimal("0");                                
				BigDecimal Acn = null;
				BigDecimal Aln = null;
				BigDecimal Abn = null;                                                                              
					
			xls.setTabla(7);
			
			xls.setCelda("A�o / Mes", "celda01", ComunesXLS.CENTER, 2);
			xls.setCelda("PyMES en Cadenas", "celda01", ComunesXLS.CENTER, 2);
					xls.setCelda("PyMES en Nafinsa M�vil", "celda01", ComunesXLS.CENTER, 3);                                
					xls.setCelda("", "celda01", ComunesXLS.CENTER, 2);
			xls.setCelda("Afiliadas", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Afiliadas Habilitadas", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Altas", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Activas", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Bajas", "celda01", ComunesXLS.CENTER, 1);
							
			for(int i= 0; i < reporteClientesRegNafinsaMovil.size(); i++){
			  List registroReporteClienteReg = (List)reporteClientesRegNafinsaMovil.get(i);                 
						numActivas = new BigDecimal("0");
						
						String activas = (String)registroReporteClienteReg.get(4)==null||"".equals(registroReporteClienteReg.get(4))?"0":(String)registroReporteClienteReg.get(4);
						String altas = (String)registroReporteClienteReg.get(4)==null||"".equals(registroReporteClienteReg.get(4))?"0":(String)registroReporteClienteReg.get(4);
						String bajas = (String)registroReporteClienteReg.get(5)==null||"".equals(registroReporteClienteReg.get(5))?"0":(String)registroReporteClienteReg.get(5);
									
						if(i==0){
							Acn = new BigDecimal (activas);
							Aln = new BigDecimal (altas);
							Abn = new BigDecimal (bajas);
						}else{                                      
							Aln = new BigDecimal (altas);
							Abn = new BigDecimal (bajas);                               
							Acn = Acn.add(Aln).subtract(Abn);                                   
						}                                                                                                                       
						numActivas = Acn;
						if(Integer.parseInt((String)registroReporteClienteReg.get(6)) >= Integer.parseInt(claveMesI) && Integer.parseInt((String)registroReporteClienteReg.get(0)) >= Integer.parseInt(claveAnioI)){          
							xls.setCelda(registroReporteClienteReg.get(0), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(registroReporteClienteReg.get(1), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(Comunes.formatoDecimal((String)registroReporteClienteReg.get(2), 0), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(Comunes.formatoDecimal((String)registroReporteClienteReg.get(3), 0), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(Comunes.formatoDecimal((String)registroReporteClienteReg.get(4), 0), "formas", ComunesXLS.CENTER, 1);                  
							xls.setCelda(numActivas.toString(), "formas", ComunesXLS.CENTER, 1);
							xls.setCelda(Comunes.formatoDecimal((String)registroReporteClienteReg.get(5), 0), "formas", ComunesXLS.CENTER, 1);
						}
			}
			
			xls.cierraTabla();
			xls.cierraXLS(); 
			  
		}catch(Exception e){
				e.printStackTrace();
			throw new AppException("generarArchivosCuenta(Error...) "+ e.getMessage());
			
		 }
		 return nombreArchivo;
		}
		
		public String generarArchNafinsaMovilDetalle(String strDirectorioPublicacion,String strDirectorioTemp,String anioDetalle,String numMes,String mesDetalle){
			
		CreaArchivo archivo = new CreaArchivo();
		ComunesXLS xls = null;
		String nombreArchivo = archivo.nombreArchivo() + ".xls";
		
		xls = new ComunesXLS(strDirectorioTemp+nombreArchivo);
		
		LOG.info("generarArchNafinsaMovilDetalle(I) ::..");
		
		try{
			
					LOG.debug("------->  numMes: " + numMes + ", anioDetalle: " + anioDetalle + ", mesDetalle: " + mesDetalle);
							
					List clientesDocsRegNafinsaMovilDetalle = this.clientesNafinsaMovilDetalle(anioDetalle, numMes);        
			
			xls.setTabla(13);
			
					xls.setCelda(mesDetalle + " " + anioDetalle, "celda01", ComunesXLS.CENTER, 13);
					xls.setCelda("No. N@E", "celda01", ComunesXLS.CENTER, 1);        
			xls.setCelda("No. PYME", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Raz�n Social", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Email", "celda01", ComunesXLS.CENTER, 1);         
			xls.setCelda("No. Celular", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Fecha de Registro", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Fecha de �ltima desactivaci�n", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Fecha de �ltima activaci�n", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("EPOS Afiliadas", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("EPOS por Afiliar", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Tipo de EPO", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Estado", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda("Estatus en Nafinsa M�vil", "celda01", ComunesXLS.CENTER, 1);
			
			for(int i= 0; i < clientesDocsRegNafinsaMovilDetalle.size(); i++){
			  List registroReporteClienteReg = (List)clientesDocsRegNafinsaMovilDetalle.get(i);
			  xls.setCelda(registroReporteClienteReg.get(0), "formas", ComunesXLS.CENTER, 1);
			  xls.setCelda(registroReporteClienteReg.get(1), "formas", ComunesXLS.CENTER, 1);
			  xls.setCelda(registroReporteClienteReg.get(2), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(registroReporteClienteReg.get(3), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(registroReporteClienteReg.get(4), "formas", ComunesXLS.CENTER, 1);                 
			  xls.setCelda(registroReporteClienteReg.get(5), "formas", ComunesXLS.CENTER, 1);
			  xls.setCelda(registroReporteClienteReg.get(6), "formas", ComunesXLS.CENTER, 1);
			  xls.setCelda(registroReporteClienteReg.get(7), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(registroReporteClienteReg.get(8), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(registroReporteClienteReg.get(9), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(registroReporteClienteReg.get(12), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(registroReporteClienteReg.get(10), "formas", ComunesXLS.CENTER, 1);
						xls.setCelda(registroReporteClienteReg.get(11), "formas", ComunesXLS.CENTER, 1);                                        
			}
			
			xls.cierraTabla();
			xls.cierraXLS();        

		}catch(Exception e){
				e.printStackTrace();
			throw new AppException("generarArchNafinsaMovilDetalle(Error...) "+ e.getMessage());
			
		 }
		 return nombreArchivo;
		}
	
	/**
	 * Fodea 02-2015. 
	 * @param rutaArchivo Ruta fisica del archivo a crear
	 * @param rutaVirtual Ruta virtual
	 */
	@Override
	public void  getEnvioCorreoDiario(String rutaArchivo, String  rutaVirtual) {
		LOG.info("getEnvioCorreoDiario (E)");   
		int i = 0;
	   Correo email = new Correo();	  
		List lColumnas = null;
		
		try {
		
			String dia  = new SimpleDateFormat("dd").format(new java.util.Date());
			String mes  = new SimpleDateFormat("MM").format(new java.util.Date());
			String anio = new SimpleDateFormat("yyyy").format(new java.util.Date());      
			
						
			Map  mContenido   = this.consultaEnvioDiario();
			if (mContenido == null || mContenido.isEmpty()) {
				//Nada que hacer
				return;
			}
			List lContenido   = (List)mContenido.get("datos");
			List lCorreos  = (List)mContenido.get("correos");
					
			String nombreArchivo = "CredCad_"+dia+"_"+mes+"_"+anio+".csv";
			StringBuffer contenidoArchivo = new StringBuffer("");
		   OutputStreamWriter writer = null;
			BufferedWriter buffer = null;
			
			for(i=0;i<lContenido.size();i++){
				lColumnas = (List)lContenido.get(i);
				for(int j=0;j<lColumnas.size();j++){
					String valor = (lColumnas.get(j)==null)?"":lColumnas.get(j).toString().replace(',',' ');
					contenidoArchivo.append(valor+",");
				}
				contenidoArchivo.append("\n");
			}
		  
		   writer = new OutputStreamWriter(new FileOutputStream(rutaArchivo+nombreArchivo), "ISO-8859-1");
			buffer = new BufferedWriter(writer, 1024 * 16); //Buffer de 16 Kb. 
		            
			buffer.write(  contenidoArchivo.toString());
		   buffer.flush();
			buffer.close();   //Termina de escribir el archivo
				
		   String mensaje = " Estimado Promotor:\n"+
		      "\tA continuacion le enviamos el archivo con la informaci�n de las Pymes\n"+
		      " a las que les fue enviado el mensaje de promoci�n y que se encuentran interesadas\n"+
		      " en el producto de Credicadenas. La informaci�n comprende del 01/"+mes+"/"+anio+"\n"+
		      " al "+dia+"/"+mes+"/"+anio+
		      "\n\n\tPuede usted descargarlo en la siguiente direccion: "+rutaVirtual+"/DescargaArchivoProCre?nombreArchivo="+nombreArchivo;

						
			this.guardarArchivoReporte(nombreArchivo,rutaArchivo, "EnvioDiarioEJBCliente.sh");
			String lbTitulo = "ENVIO DIARIO DE CLIENTES INTERESADOS EN CREDICADENAS";
				
			ArrayList listaDeArchivos = new ArrayList();
			HashMap archivoAdjunto = new HashMap();
			String rutaArch = rutaArchivo +nombreArchivo;	
			archivoAdjunto.put("FILE_FULL_PATH", rutaArch);
			listaDeArchivos.add(archivoAdjunto);
		 
			for(i=0;i<lCorreos.size();i++){
				String correoPara = (String)lCorreos.get(i);	
			   email.setCodificacion("charset=UTF-8");
				email.enviaCorreoConDatosAdjuntos("ahgarfias@nafin.gob.mx", correoPara, "", lbTitulo, mensaje, null, listaDeArchivos);			   
			}			
			
		} catch (Exception e) {    
			LOG.error("getEnvioCorreoDiario(Exception)",e);
		   throw new AppException("Error en el proceso de envio Diario Inteligencia Comercial", e);
		}finally{
		   LOG.info("getEnvioCorreoDiario(S)");      
		} 
	}
}