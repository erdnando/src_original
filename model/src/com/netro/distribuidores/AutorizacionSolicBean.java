package com.netro.distribuidores;

import com.netro.distribuidores.ws.DocumentoDistIFWS;
import com.netro.distribuidores.ws.ProcesoIFDistWSInfo;
import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CrearArchivosAcuses;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "AutorizacionSolicEJB" , mappedName = "AutorizacionSolicEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class AutorizacionSolicBean implements AutorizacionSolic {

private final static Log log = ServiceLocator.getInstance().getLog(AutorizacionSolicBean.class);


public Vector getLineasConDoctos(String ic_epo,String ic_if,String ic_documento,String fecha_seleccion_de,String fecha_seleccion_a)
	throws NafinException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String condicion    = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();

                if(!"".equals(ic_documento)&&ic_documento!=null)
                	condicion += " AND LC.IC_LINEA_CREDITO_DM "+
                        	     " IN(SELECT IC_LINEA_CREDITO_DM FROM DIS_DOCUMENTO "+
                                     " WHERE IC_DOCUMENTO = "+ic_documento+
                                     " AND IC_EPO = "+ic_epo+
                                     ")";
                if(!"".equals(fecha_seleccion_de)&&fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&&fecha_seleccion_a!=null)
                	condicion += "  AND LC.IC_LINEA_CREDITO_DM IN("   +
				     "  SELECT D.IC_LINEA_CREDITO_DM FROM DIS_DOCUMENTO D,DIS_DOCTO_SELECCIONADO DS"   +
                                     "  WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                                     "  AND TRUNC(DS.DF_FECHA_SELECCION)"   +
                                     "  between TRUNC(TO_DATE('"+fecha_seleccion_de+"','DD/MM/YYYY'))"   +
                                     "  and TRUNC(TO_DATE('"+fecha_seleccion_a+"','DD/MM/YYYY'))"   +
                                     "  AND D.ic_epo =  "  +ic_epo+
                                     " )"  ;

                qrySentencia = "    SELECT  E.cg_razon_social"   +
					"    		 ,E.ic_epo"   +
					"    		 ,LC.ic_linea_credito_dm"   +
					"   		 ,M.cd_nombre as MONEDA"   +
					"   		 ,M.ic_moneda"   +
					"  		 ,LC.fn_monto_autorizado_total as SALDO_INICIAL"   +
					"  		 , (LC.fn_monto_autorizado_total-LC.fn_saldo_total) as MONTO_SELECCIONADO"   +
					"   		 ,VC.num_doctos_selec"   +
					"  		 ,LC.fn_saldo_total AS SALDO_DISPONIBLE"   +
					"    FROM DIS_LINEA_CREDITO_DM LC"   +
					"    ,COMCAT_EPO E"   +
					"    ,COMCAT_MONEDA M"   +
					"    ,(select count(d.ic_documento) as num_doctos_selec,ic_linea_credito_dm"   +
					"      from dis_documento d,dis_docto_seleccionado ds"   +
					"      where d.ic_epo = " +ic_epo+
					" 	 and d.ic_documento = ds.ic_documento"   +
					"      group by ic_linea_credito_dm) vc"   +
					"    ,COMREL_IF_EPO IE"   +
					"    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"    WHERE LC.ic_epo = E.ic_epo"   +
					"    AND LC.ic_moneda = M.ic_moneda"   +
					"    AND E.cs_habilitado = 'S'"   +
					"    AND LC.ic_linea_credito_dm = vc.ic_linea_credito_dm"   +
					"    AND E.IC_EPO = IE.IC_EPO"   +
					"    AND LC.IC_IF = IE.IC_IF"   +
					"    AND IE.CS_DISTRIBUIDORES = 'S'"   +
					"    AND IEP. IC_EPO = IE.IC_EPO"   +
					"    AND IEP.IC_IF = IE.IC_IF"   +
					"    AND IEP.IC_PRODUCTO_NAFIN = 4"   +
					"	 AND LC.ic_estatus_linea = 12"+
					"    AND IE.ic_if = " +ic_if+
					"    AND IE.ic_epo = " +ic_epo+
					condicion;
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	vecColumnas = new Vector();
/*0*/                    vecColumnas.add((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
/*1*/                    vecColumnas.add((rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO"));
/*2*/                    vecColumnas.add((rs.getString("IC_LINEA_CREDITO_DM")==null)?"":rs.getString("IC_LINEA_CREDITO_DM"));
/*3*/                    vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*4*/                    vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*5*/                    vecColumnas.add((rs.getString("SALDO_INICIAL")==null)?"":rs.getString("SALDO_INICIAL"));
/*6*/                    vecColumnas.add((rs.getString("MONTO_SELECCIONADO")==null)?"":rs.getString("MONTO_SELECCIONADO"));
/*7*/                    vecColumnas.add((rs.getString("NUM_DOCTOS_SELEC")==null)?"":rs.getString("NUM_DOCTOS_SELEC"));
/*8*/                    vecColumnas.add((rs.getString("SALDO_DISPONIBLE")==null)?"":rs.getString("SALDO_DISPONIBLE"));
				vecFilas.add(vecColumnas);
                }
                con.cierraStatement();

        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}


public Vector getLineasConDoctosCCC(String ic_epo,String ic_pyme,String ic_if,String ic_documento,String fecha_seleccion_de,String fecha_seleccion_a)
	throws NafinException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String condicion    = "";
        String condicionP1  = "";
        String condicionP2  = "";
        String condicionP3  = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();

                if(!"".equals(ic_pyme)&&ic_pyme!=null){
                	condicionP1 = " AND IC_PYME = "+ic_pyme;
                        condicionP2 = "	AND D.ic_pyme = "+ic_pyme;
                        condicionP3 = "	AND LC.ic_pyme = "+ic_pyme;
                }

                if(!"".equals(ic_documento)&&ic_documento!=null)
                	condicion += " AND LC.IC_LINEA_CREDITO "+
                        	     " IN(SELECT IC_LINEA_CREDITO FROM DIS_DOCUMENTO "+
                                     " WHERE IC_DOCUMENTO = "+ic_documento+
                                     " AND IC_EPO = "+ic_epo+
                                     condicionP1+
                                     ")";
                if(!"".equals(fecha_seleccion_de)&&fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&&fecha_seleccion_a!=null)
                	condicion += "  AND LC.IC_LINEA_CREDITO IN("   +
				     "  SELECT D.IC_LINEA_CREDITO FROM DIS_DOCUMENTO D,DIS_DOCTO_SELECCIONADO DS"   +
                                     "  WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                                     "  AND TRUNC(DS.DF_FECHA_SELECCION)"   +
                                     "  between TRUNC(TO_DATE('"+fecha_seleccion_de+"','DD/MM/YYYY'))"   +
                                     "  and TRUNC(TO_DATE('"+fecha_seleccion_a+"','DD/MM/YYYY'))"   +
                                     "  AND D.ic_epo =  "  +ic_epo+
                                     condicionP2+
                                     " )"  ;

                qrySentencia = "    SELECT  P.cg_razon_social"   +
					"    		 ,P.ic_pyme"   +
					"    		 ,LC.cg_tipo_solicitud||nvl(P.in_numero_sirac,'')||LC.ic_linea_credito as folio_armado"   +
					"   		 ,M.cd_nombre as MONEDA"   +
					"   		 ,M.ic_moneda"   +
					"  		 ,LC.fn_monto_autorizado_total as SALDO_INICIAL"   +
					"  		 ,(LC.fn_monto_autorizado_total-LC.fn_saldo_linea_total) as MONTO_SELECCIONADO"   +
					"   		 ,VC.num_doctos_selec"   +
					"  		 ,LC.fn_saldo_linea_total AS SALDO_DISPONIBLE"   +
					"    FROM COM_LINEA_CREDITO LC"   +
					"    ,COMCAT_PYME P"   +
					"    ,COMCAT_MONEDA M"   +
					"    ,COMREL_PYME_EPO PE"+
					"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					"    ,(select count(d.ic_documento) as num_doctos_selec,ic_linea_credito"   +
					"      from dis_documento d,dis_docto_seleccionado ds"   +
					"      where d.ic_epo = " +ic_epo+
					condicionP2+
					" 	 and d.ic_documento = ds.ic_documento"   +
					"      group by ic_linea_credito) vc"   +
					"    WHERE LC.ic_pyme = P.ic_pyme"   +
					"    AND LC.ic_moneda = M.ic_moneda"   +
					"    AND LC.ic_linea_credito = vc.ic_linea_credito"   +
					"    AND P.IC_PYME = LC.IC_PYME"   +
					"    AND PE.CS_DISTRIBUIDORES in('S','R')"   +
					"    AND PEP. IC_EPO = PE.IC_EPO"   +
					"    AND PEP.IC_PYME = PE.IC_PYME"   +
					"    AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					"    AND LC.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"+
					"    AND PE.IC_PYME = LC.IC_PYME"+
					"    AND PE.IC_EPO = "+ic_epo+
					"    AND LC.ic_if = " +ic_if+
					"	 AND LC.ic_estatus_linea = 12 "+
					"	 AND LC.cs_factoraje_con_rec = 'N' "+
					condicionP3+
					condicion;
//                System.out.println("\n\n\\\\\\\\"+qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	vecColumnas = new Vector();
/*0*/                    vecColumnas.add((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
/*1*/                    vecColumnas.add((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*2*/                    vecColumnas.add((rs.getString("FOLIO_ARMADO")==null)?"":rs.getString("FOLIO_ARMADO"));
/*3*/                    vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*4*/                    vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*5*/                    vecColumnas.add((rs.getString("SALDO_INICIAL")==null)?"":rs.getString("SALDO_INICIAL"));
/*6*/                    vecColumnas.add((rs.getString("MONTO_SELECCIONADO")==null)?"":rs.getString("MONTO_SELECCIONADO"));
/*7*/                    vecColumnas.add((rs.getString("NUM_DOCTOS_SELEC")==null)?"":rs.getString("NUM_DOCTOS_SELEC"));
/*8*/                    vecColumnas.add((rs.getString("SALDO_DISPONIBLE")==null)?"":rs.getString("SALDO_DISPONIBLE"));
		vecFilas.add(vecColumnas);
                }
                con.cierraStatement();

        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}

/*
 *Modificaciones realizadas para obtener las lineas de factoraje con recurso
 * By Hugo Vargas
*/
public Vector getLineasConDoctosFF(String ic_epo,String ic_pyme,String ic_if,String ic_documento,String fecha_seleccion_de,String fecha_seleccion_a)
	throws AppException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String condicion    = "";
        String condicionP1  = "";
        String condicionP2  = "";
        String condicionP3  = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();

                if(!"".equals(ic_pyme)&&ic_pyme!=null){
                	condicionP1 = " AND IC_PYME = "+ic_pyme;
                        condicionP2 = "	AND D.ic_pyme = "+ic_pyme;
                        condicionP3 = "	AND LC.ic_pyme = "+ic_pyme;
                }

                if(!"".equals(ic_documento)&&ic_documento!=null)
                	condicion += " AND LC.IC_LINEA_CREDITO "+
                        	     " IN(SELECT IC_LINEA_CREDITO FROM DIS_DOCUMENTO "+
                                     " WHERE IC_DOCUMENTO = "+ic_documento+
                                     " AND IC_EPO = "+ic_epo+
                                     condicionP1+
                                     ")";
                if(!"".equals(fecha_seleccion_de)&&fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&&fecha_seleccion_a!=null)
                	condicion += "  AND LC.IC_LINEA_CREDITO IN("   +
				     "  SELECT D.IC_LINEA_CREDITO FROM DIS_DOCUMENTO D,DIS_DOCTO_SELECCIONADO DS"   +
                                     "  WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                                     "  AND TRUNC(DS.DF_FECHA_SELECCION)"   +
                                     "  between TRUNC(TO_DATE('"+fecha_seleccion_de+"','DD/MM/YYYY'))"   +
                                     "  and TRUNC(TO_DATE('"+fecha_seleccion_a+"','DD/MM/YYYY'))"   +
                                     "  AND D.ic_epo =  "  +ic_epo+
                                     condicionP2+
                                     " )"  ;

                qrySentencia = "    SELECT  P.cg_razon_social"   +
					"    		 ,P.ic_pyme"   +
					"    		 ,LC.cg_tipo_solicitud||nvl(P.in_numero_sirac,'')||LC.ic_linea_credito as folio_armado"   +
					"   		 ,M.cd_nombre as MONEDA"   +
					"   		 ,M.ic_moneda"   +
					"  		 ,LC.fn_monto_autorizado_total as SALDO_INICIAL"   +
					"  		 ,(LC.fn_monto_autorizado_total-LC.fn_saldo_linea_total) as MONTO_SELECCIONADO"   +
					"   		 ,VC.num_doctos_selec"   +
					"  		 ,LC.fn_saldo_linea_total AS SALDO_DISPONIBLE"   +
					"    FROM COM_LINEA_CREDITO LC"   +
					"    ,COMCAT_PYME P"   +
					"    ,COMCAT_MONEDA M"   +
					"    ,COMREL_PYME_EPO PE"+
					"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					"    ,(select count(d.ic_documento) as num_doctos_selec,ic_linea_credito"   +
					"      from dis_documento d,dis_docto_seleccionado ds"   +
					"      where d.ic_epo = " +ic_epo+
					condicionP2+
					" 	 and d.ic_documento = ds.ic_documento"   +
					"      group by ic_linea_credito) vc"   +
					"    WHERE LC.ic_pyme = P.ic_pyme"   +
					"    AND LC.ic_moneda = M.ic_moneda"   +
					"    AND LC.ic_linea_credito = vc.ic_linea_credito"   +
					"    AND P.IC_PYME = LC.IC_PYME"   +
					"    AND PE.CS_DISTRIBUIDORES in('S','R')"   +
					"    AND PEP. IC_EPO = PE.IC_EPO"   +
					"    AND PEP.IC_PYME = PE.IC_PYME"   +
					"    AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					"    AND LC.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"+
					"    AND PE.IC_PYME = LC.IC_PYME"+
					"    AND PE.IC_EPO = "+ic_epo+
					"    AND LC.ic_if = " +ic_if+
					"	 AND LC.ic_estatus_linea = 12 "+
					"	 AND LC.cs_factoraje_con_rec = 'S' "+
					condicionP3+
					condicion;
//                System.out.println("\n\n\\\\\\\\"+qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	vecColumnas = new Vector();
/*0*/                    vecColumnas.add((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));
/*1*/                    vecColumnas.add((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*2*/                    vecColumnas.add((rs.getString("FOLIO_ARMADO")==null)?"":rs.getString("FOLIO_ARMADO"));
/*3*/                    vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*4*/                    vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*5*/                    vecColumnas.add((rs.getString("SALDO_INICIAL")==null)?"":rs.getString("SALDO_INICIAL"));
/*6*/                    vecColumnas.add((rs.getString("MONTO_SELECCIONADO")==null)?"":rs.getString("MONTO_SELECCIONADO"));
/*7*/                    vecColumnas.add((rs.getString("NUM_DOCTOS_SELEC")==null)?"":rs.getString("NUM_DOCTOS_SELEC"));
/*8*/                    vecColumnas.add((rs.getString("SALDO_DISPONIBLE")==null)?"":rs.getString("SALDO_DISPONIBLE"));
		vecFilas.add(vecColumnas);
                }
                con.cierraStatement();

        }catch(Exception e){
		//System.out.println("Excepcion "+e);
                throw new AppException("Error al obtener las l�neas de factoraje con recurso"+e);
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}


public Vector getDoctosSeleccionados(String ic_epo,String ic_if,String ic_pyme,String ic_documento,String fecha_seleccion_de,String fecha_seleccion_a,String in,String cg_tipo_credito)
	throws NafinException{
   	Vector retorno = null;
	try{
        	if("D".equals(cg_tipo_credito))
                	retorno =  getDoctosSeleccionados(ic_epo,ic_if,ic_documento,fecha_seleccion_de,fecha_seleccion_a,in);
                else if("C".equals(cg_tipo_credito))
                	retorno =  getDoctosSeleccionadosCCC(ic_epo,ic_pyme,ic_if,ic_documento,fecha_seleccion_de,fecha_seleccion_a,in);
					else if("F".equals(cg_tipo_credito))
						retorno =  getDoctosSeleccionadosFF(ic_epo,ic_pyme,ic_if,ic_documento,fecha_seleccion_de,fecha_seleccion_a,in);
	}catch(NafinException ne){
        	throw ne;
        }
   return retorno;
}
public Vector getDoctosSeleccionados(String ic_epo,String ic_if,String ic_documento,String fecha_seleccion_de,String fecha_seleccion_a,String in)
	throws NafinException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String condicion    = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();

                if(!"".equals(ic_documento)&&ic_documento!=null)
                	condicion += " AND D.ic_documento = "+ic_documento;
                if(!"".equals(fecha_seleccion_de)&&fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&&fecha_seleccion_a!=null)
                	condicion += "  AND TRUNC(DS.DF_FECHA_SELECCION)"+
                                     "  between TRUNC(TO_DATE('"+fecha_seleccion_de+"','DD/MM/YYYY'))"+
                                     "  and TRUNC(TO_DATE('"+fecha_seleccion_a+"','DD/MM/YYYY'))";
                if(!"".equals(in)&&in!=null)
                        condicion += " AND D.IC_DOCUMENTO IN("+in+")";


                qrySentencia =  "  SELECT  D.ig_numero_docto"   +
						"  		,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
						" 		,P.cg_razon_social as DISTRIBUIDOR"   +
                        "		,P.ic_pyme"+
						" 		,M.cd_nombre as MONEDA"   +
                        "		,D.ic_moneda"+
						" 		,D.fn_monto"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPO_CONVERSION"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
						" 		,CT.CD_NOMBRE  ||'' ||  ds.cg_rel_mat || ' ' ||ds.FN_PUNTOS AS REFERENCIA_INT		 "   +
						" 		,DECODE(DS.CG_REL_MAT"+
                        "               ,'+',DS.fn_valor_tasa + DS.fn_puntos"+
                        "               ,'-',DS.fn_valor_tasa - DS.fn_puntos"+
                        "               ,'*',DS.fn_valor_tasa * DS.fn_puntos"+
                        "               ,'/',DS.fn_valor_tasa / DS.fn_puntos"+
                        "               ,0) AS VALOR_TASA"   +
                        " 		,D.IG_PLAZO_CREDITO"   +
                        " 		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO		"   +
                        " 		,TCI.ic_tipo_cobro_interes"   +
                        "               ,TCI.cd_descripcion as tipo_cobro_interes"+
                        "               ,DS.fn_importe_interes as MONTO_INTERES"+
                        "               ,D.ic_documento" +
                        "               ,E.cg_razon_social as EPO"+
                        "               ,nvl(D.fn_porc_descuento,0) as fn_porc_descuento"+
                        "		,LCD.ic_moneda as MONEDA_LINEA"+
						" 		,DS.fn_importe_recibir"   +
						"       ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa, "+
						"       NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver, "+
						"       d.fn_tipo_cambio as tipo_cambio, "+
						"       NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes, "+
						"       NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as tipo_cobranza "+
						"		,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5 "+
						"		,DS.fn_valor_tasa"+
                        "  FROM DIS_DOCUMENTO D"   +
                        "    ,COMCAT_PYME P"   +
                        "    ,COMREL_PRODUCTO_EPO PE"   +
       					"	 ,comrel_pyme_epo cpe "+
                        "    ,COMCAT_PRODUCTO_NAFIN PN"   +
                        "    ,COMCAT_MONEDA M"   +
                        "    ,COM_TIPO_CAMBIO TC"   +
                        "    ,DIS_LINEA_CREDITO_DM LCD"   +
                        "    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
                        "    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
                        "    ,COMCAT_TASA CT"   +
                        "    ,COMCAT_EPO E"+
                        "    ,DIS_DOCTO_SELECCIONADO DS"   +
                        "  WHERE D.IC_EPO = PE.IC_EPO"   +
                        "    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND PN.IC_PRODUCTO_NAFIN = 4"   +
                        "    AND P.IC_PYME = D.IC_PYME"   +
                        "    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                        "    AND D.IC_EPO = PE.IC_EPO"   +
						"    AND cpe.ic_epo = d.ic_epo "+
						"    AND cpe.ic_pyme = d.ic_pyme "+
                        "    AND LCD.IC_MONEDA = M.IC_MONEDA"   +
                        "    AND D.IC_MONEDA = TC.IC_MONEDA"   +
                        "    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = D.IC_MONEDA)"   +
                        "    AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
                        "    AND LCD.IC_IF = IEP.IC_IF"   +
                        "    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
                        "    AND D.ic_estatus_docto = 3"   +
                        "    AND IEP.IC_EPO = D.IC_EPO"   +
                        "    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND DS.IC_TASA = CT.IC_TASA "  +
                        "    AND D.IC_EPO = E.IC_EPO "+
                        "    AND D.IC_EPO = "+ic_epo+
                        "    AND LCD.IC_IF = "+ic_if+
						"	 AND LCD.ic_estatus_linea = 12 "+
                        "    AND D.ic_documento not in(select ic_documento from dis_solicitud)" +
                        condicion +
                        "  ORDER BY D.IG_NUMERO_DOCTO"  ;
				System.out.println("\nqrySentencia: "+qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	  vecColumnas = new Vector();
/*0*/                     vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*1*/                     vecColumnas.add((rs.getString("DF_FECHA_SELECCION")==null)?"":rs.getString("DF_FECHA_SELECCION"));
/*2*/                     vecColumnas.add((rs.getString("DISTRIBUIDOR")==null)?"":rs.getString("DISTRIBUIDOR"));
/*3*/                     vecColumnas.add((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*4*/                     vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*5*/                     vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*6*/                     vecColumnas.add((rs.getString("FN_MONTO")==null)?"":rs.getString("FN_MONTO"));
/*7*/                     vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*8*/                     vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*9*/                     vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*10*/                    vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*11*/                    vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*12*/                    vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*13*/                    vecColumnas.add((rs.getString("TIPO_COBRO_INTERES")==null)?"":rs.getString("TIPO_COBRO_INTERES"));
/*14*/                    vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*15*/                    vecColumnas.add((rs.getString("MONTO_INTERES")==null)?"":rs.getString("MONTO_INTERES"));
/*16*/                    vecColumnas.add((rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO"));
/*17*/                    vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*18*/                    vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO"));
/*19*/                    vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"0":rs.getString("MONEDA_LINEA"));
/*20*/                    vecColumnas.add((rs.getString("FN_IMPORTE_RECIBIR")==null)?"0":rs.getString("FN_IMPORTE_RECIBIR"));
/*21*/                    vecColumnas.add((rs.getString("numdist")==null)?"":rs.getString("numdist"));
/*22*/                    vecColumnas.add((rs.getString("cve_tasa")==null)?"":rs.getString("cve_tasa"));
/*23*/                    vecColumnas.add((rs.getString("tipo_conver")==null)?"":rs.getString("tipo_conver"));
/*24*/                    vecColumnas.add((rs.getString("tipo_cambio")==null)?"":rs.getString("tipo_cambio"));
/*25*/                    vecColumnas.add((rs.getString("resp_interes")==null)?"":rs.getString("resp_interes"));
/*26*/                    vecColumnas.add((rs.getString("tipo_cobranza")==null)?"":rs.getString("tipo_cobranza"));
/*27*/                    vecColumnas.add((rs.getString("ct_referencia")==null)?"":rs.getString("ct_referencia"));
/*28*/                    vecColumnas.add((rs.getString("cg_campo1")==null)?"":rs.getString("cg_campo1"));
/*29*/                    vecColumnas.add((rs.getString("cg_campo2")==null)?"":rs.getString("cg_campo2"));
/*30*/                    vecColumnas.add((rs.getString("cg_campo3")==null)?"":rs.getString("cg_campo3"));
/*31*/                    vecColumnas.add((rs.getString("cg_campo4")==null)?"":rs.getString("cg_campo4"));
/*32*/                    vecColumnas.add((rs.getString("cg_campo5")==null)?"":rs.getString("cg_campo5"));
/*33*/                    vecColumnas.add((rs.getString("fn_valor_tasa")==null)?"":rs.getString("fn_valor_tasa"));
                          vecFilas.add(vecColumnas);
                }
                con.cierraStatement();

        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}


public Vector getDoctosSeleccionadosCCC(String ic_epo,String ic_pyme,String ic_if,String ic_documento,String fecha_seleccion_de,String fecha_seleccion_a,String in)
	throws NafinException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String condicion    = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();

                if(!"".equals(ic_documento)&&ic_documento!=null)
                	condicion += " AND D.IC_DOCUMENTO = "+ic_documento;
                if(!"".equals(fecha_seleccion_de)&&fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&&fecha_seleccion_a!=null)
                	condicion += "  AND TRUNC(DS.DF_FECHA_SELECCION)"+
                                     "  between TRUNC(TO_DATE('"+fecha_seleccion_de+"','DD/MM/YYYY'))"+
                                     "  and TRUNC(TO_DATE('"+fecha_seleccion_a+"','DD/MM/YYYY'))";
                if(!"".equals(in)&&in!=null)
                        condicion += " AND D.IC_DOCUMENTO IN("+in+")";
                if(!"".equals(ic_pyme))
                	condicion += "    AND D.IC_PYME = "+ic_pyme;

                qrySentencia =  "  SELECT  D.ig_numero_docto"   +
						"  		,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
						" 		,P.cg_razon_social as DISTRIBUIDOR"   +
                        "		,P.ic_pyme"+
						" 		,M.cd_nombre as MONEDA"   +
                        "		,D.ic_moneda"+
						" 		,D.fn_monto"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPO_CONVERSION"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
						" 		,CT.CD_NOMBRE||' '||DS.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT		 "   +
						" 		,DECODE(DS.CG_REL_MAT"+
                        "			,'+',DS.fn_valor_tasa + DS.fn_puntos"+
                        "			,'-',DS.fn_valor_tasa - DS.fn_puntos"+
                        "			,'*',DS.fn_valor_tasa * DS.fn_puntos"+
                        "			,'/',DS.fn_valor_tasa / DS.fn_puntos"+
                        "			,0) AS VALOR_TASA"   +
                        " 		,D.IG_PLAZO_CREDITO"   +
                        " 		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO		"   +
                        " 		,TCI.ic_tipo_cobro_interes"   +
                        "		,TCI.cd_descripcion as tipo_cobro_interes"+
                        "		,DS.fn_importe_interes as MONTO_INTERES"+
                        "		,D.ic_documento" +
                        "		,E.cg_razon_social as EPO"+
                        "		,nvl(D.fn_porc_descuento,0) as fn_porc_descuento"+
                        "		,LC.ic_moneda as MONEDA_LINEA"+
												"		,DS.fn_importe_recibir"   +
												"		,DS.FN_VALOR_TASA"   +
                        "   ,cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5 "+
												"   , lc.cg_numero_cuenta  as cg_numero_cuenta, "+										  
								" d.ig_tipo_pago AS TIPOPAGO,   "+
								" d.IG_MESES_INTERES  as MESES_INTERESES "+ 
										  
                        "  FROM DIS_DOCUMENTO D"   +
                        "    ,COMCAT_PYME P"   +
                        "    ,COMREL_PRODUCTO_EPO PE"   +
                        "    ,COMCAT_PRODUCTO_NAFIN PN"   +
                        "    ,COMCAT_MONEDA M"   +
                        "    ,COM_TIPO_CAMBIO TC"   +
                        "    ,COM_LINEA_CREDITO LC"   +
                        "    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
                        "    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
                        "    ,COMCAT_TASA CT"   +
                        "    ,COMCAT_EPO E"+
                        "    ,DIS_DOCTO_SELECCIONADO DS"   +
                        "  WHERE D.IC_EPO = PE.IC_EPO"   +
                        "    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND PN.IC_PRODUCTO_NAFIN = 4"   +
                        "    AND P.IC_PYME = D.IC_PYME"   +
                        "    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                        "    AND D.IC_EPO = PE.IC_EPO"   +
                        "    AND LC.IC_MONEDA = M.IC_MONEDA"   +
                        "    AND D.IC_MONEDA = TC.IC_MONEDA"   +
                        "    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                        "    AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
                        "    AND LC.IC_IF = IEP.IC_IF"   +
                        "    AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
                        "    AND D.ic_estatus_docto = 3"   +
                        "    AND IEP.IC_EPO = D.IC_EPO"   +
                        "    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND DS.IC_TASA = CT.IC_TASA "  +
                        "    AND D.IC_EPO = E.IC_EPO "+
                        "    AND D.IC_EPO = "+ic_epo+
                        "    AND LC.IC_IF = "+ic_if+
						"	 AND LC.ic_estatus_linea = 12"+
						"	 AND LC.cs_factoraje_con_rec = 'N' "+
                        "    AND D.ic_documento not in(select ic_documento from dis_solicitud)" +
                        condicion +
                        "  ORDER BY D.IG_NUMERO_DOCTO"  ;
                rs = con.queryDB(qrySentencia);
                System.out.println("qrySentencia "+qrySentencia);
                while(rs.next()){
                	  vecColumnas = new Vector();
/*0*/                     vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*1*/                     vecColumnas.add((rs.getString("DF_FECHA_SELECCION")==null)?"":rs.getString("DF_FECHA_SELECCION"));
/*2*/                     vecColumnas.add((rs.getString("DISTRIBUIDOR")==null)?"":rs.getString("DISTRIBUIDOR"));
/*3*/                     vecColumnas.add((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*4*/                     vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*5*/                     vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*6*/                     vecColumnas.add((rs.getString("FN_MONTO")==null)?"":rs.getString("FN_MONTO"));
/*7*/                     vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*8*/                     vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*9*/                     vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*10*/                    vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*11*/                    vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*12*/                    vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*13*/                    vecColumnas.add((rs.getString("TIPO_COBRO_INTERES")==null)?"":rs.getString("TIPO_COBRO_INTERES"));
/*14*/                    vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*15*/                    vecColumnas.add((rs.getString("MONTO_INTERES")==null)?"":rs.getString("MONTO_INTERES"));
/*16*/                    vecColumnas.add((rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO"));
/*17*/                    vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*18*/                    vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO"));
/*19*/                    vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"0":rs.getString("MONEDA_LINEA"));
/*20*/                    vecColumnas.add((rs.getString("FN_IMPORTE_RECIBIR")==null)?"0":rs.getString("FN_IMPORTE_RECIBIR"));
/*21*/                    vecColumnas.add((rs.getString("FN_VALOR_TASA")==null)?"0":rs.getString("FN_VALOR_TASA"));
/*22*/                    vecColumnas.add((rs.getString("CG_CAMPO1")==null)?"0":rs.getString("CG_CAMPO1"));
/*23*/                    vecColumnas.add((rs.getString("CG_CAMPO2")==null)?"0":rs.getString("CG_CAMPO2"));
/*24*/                    vecColumnas.add((rs.getString("CG_CAMPO3")==null)?"0":rs.getString("CG_CAMPO3"));
/*25*/                    vecColumnas.add((rs.getString("CG_CAMPO4")==null)?"0":rs.getString("CG_CAMPO4"));
/*26*/                    vecColumnas.add((rs.getString("CG_CAMPO5")==null)?"0":rs.getString("CG_CAMPO5"));
/*27*/  						  vecColumnas.add((rs.getString("cg_numero_cuenta")==null)?"0":rs.getString("cg_numero_cuenta"));
/*28*/                    vecColumnas.add((rs.getString("MESES_INTERESES")==null)?"0":rs.getString("MESES_INTERESES"));
/*29*/                    vecColumnas.add((rs.getString("TIPOPAGO")==null)?"1":rs.getString("TIPOPAGO")); 

                          /* for(int i=1;i<=numCamposAd;i++)
                            {
                            vecColumnas.addElement((rs.getString("CG_CAMPO"+i)==null)?"":rs.getString("CG_CAMPO"+i));
                            }   */

                          vecFilas.add(vecColumnas);
                }
                System.out.println("vecFilas "+vecFilas);
                con.cierraStatement();

        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}

public Vector getDoctosSeleccionadosFF(String ic_epo,String ic_pyme,String ic_if,String ic_documento,String fecha_seleccion_de,String fecha_seleccion_a,String in)
	throws AppException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String condicion    = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();

                if(!"".equals(ic_documento)&&ic_documento!=null)
                	condicion += " AND D.IC_DOCUMENTO = "+ic_documento;
                if(!"".equals(fecha_seleccion_de)&&fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&&fecha_seleccion_a!=null)
                	condicion += "  AND TRUNC(DS.DF_FECHA_SELECCION)"+
                                     "  between TRUNC(TO_DATE('"+fecha_seleccion_de+"','DD/MM/YYYY'))"+
                                     "  and TRUNC(TO_DATE('"+fecha_seleccion_a+"','DD/MM/YYYY'))";
                if(!"".equals(in)&&in!=null)
                        condicion += " AND D.IC_DOCUMENTO IN("+in+")";
                if(!"".equals(ic_pyme))
                	condicion += "    AND D.IC_PYME = "+ic_pyme;

                qrySentencia =  "  SELECT  D.ig_numero_docto"   +
						"  		,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
						" 		,P.cg_razon_social as DISTRIBUIDOR"   +
                        "		,P.ic_pyme"+
						" 		,M.cd_nombre as MONEDA"   +
                        "		,D.ic_moneda"+
						" 		,D.fn_monto"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPO_CONVERSION"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
						" 		,CT.CD_NOMBRE||' '||DS.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT		 "   +
						" 		,DECODE(DS.CG_REL_MAT"+
                        "			,'+',DS.fn_valor_tasa + DS.fn_puntos"+
                        "			,'-',DS.fn_valor_tasa - DS.fn_puntos"+
                        "			,'*',DS.fn_valor_tasa * DS.fn_puntos"+
                        "			,'/',DS.fn_valor_tasa / DS.fn_puntos"+
                        "			,0) AS VALOR_TASA"   +
                        " 		,D.IG_PLAZO_CREDITO"   +
                        " 		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO		"   +
                        " 		,TCI.ic_tipo_cobro_interes"   +
                        "		,TCI.cd_descripcion as tipo_cobro_interes"+
                        "		,DS.fn_importe_interes as MONTO_INTERES"+
                        "		,D.ic_documento" +
                        "		,E.cg_razon_social as EPO"+
                        "		,nvl(D.fn_porc_descuento,0) as fn_porc_descuento"+
                        "		,LC.ic_moneda as MONEDA_LINEA"+
												"		,DS.fn_importe_recibir"   +
												"		,DS.FN_VALOR_TASA"   +
                        "   ,cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5 "+
												"   , lc.cg_numero_cuenta  as cg_numero_cuenta "+
                        "  FROM DIS_DOCUMENTO D"   +
                        "    ,COMCAT_PYME P"   +
                        "    ,COMREL_PRODUCTO_EPO PE"   +
                        "    ,COMCAT_PRODUCTO_NAFIN PN"   +
                        "    ,COMCAT_MONEDA M"   +
                        "    ,COM_TIPO_CAMBIO TC"   +
                        "    ,COM_LINEA_CREDITO LC"   +
                        "    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
                        "    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
                        "    ,COMCAT_TASA CT"   +
                        "    ,COMCAT_EPO E"+
                        "    ,DIS_DOCTO_SELECCIONADO DS"   +
                        "  WHERE D.IC_EPO = PE.IC_EPO"   +
                        "    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND PN.IC_PRODUCTO_NAFIN = 4"   +
                        "    AND P.IC_PYME = D.IC_PYME"   +
                        "    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                        "    AND D.IC_EPO = PE.IC_EPO"   +
                        "    AND LC.IC_MONEDA = M.IC_MONEDA"   +
                        "    AND D.IC_MONEDA = TC.IC_MONEDA"   +
                        "    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                        "    AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
                        "    AND LC.IC_IF = IEP.IC_IF"   +
                        "    AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
                        "    AND D.ic_estatus_docto = 3"   +
                        "    AND IEP.IC_EPO = D.IC_EPO"   +
                        "    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND DS.IC_TASA = CT.IC_TASA "  +
                        "    AND D.IC_EPO = E.IC_EPO "+
                        "    AND D.IC_EPO = "+ic_epo+
                        "    AND LC.IC_IF = "+ic_if+
						"	 AND LC.ic_estatus_linea = 12"+
						"	 AND LC.cs_factoraje_con_rec = 'S' "+
                        "    AND D.ic_documento not in(select ic_documento from dis_solicitud)" +
                        condicion +
                        "  ORDER BY D.IG_NUMERO_DOCTO"  ;
                rs = con.queryDB(qrySentencia);
                System.out.println("qrySentencia "+qrySentencia);
                while(rs.next()){
                	  vecColumnas = new Vector();
/*0*/                     vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*1*/                     vecColumnas.add((rs.getString("DF_FECHA_SELECCION")==null)?"":rs.getString("DF_FECHA_SELECCION"));
/*2*/                     vecColumnas.add((rs.getString("DISTRIBUIDOR")==null)?"":rs.getString("DISTRIBUIDOR"));
/*3*/                     vecColumnas.add((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*4*/                     vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*5*/                     vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*6*/                     vecColumnas.add((rs.getString("FN_MONTO")==null)?"":rs.getString("FN_MONTO"));
/*7*/                     vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*8*/                     vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*9*/                     vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*10*/                    vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*11*/                    vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*12*/                    vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*13*/                    vecColumnas.add((rs.getString("TIPO_COBRO_INTERES")==null)?"":rs.getString("TIPO_COBRO_INTERES"));
/*14*/                    vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*15*/                    vecColumnas.add((rs.getString("MONTO_INTERES")==null)?"":rs.getString("MONTO_INTERES"));
/*16*/                    vecColumnas.add((rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO"));
/*17*/                    vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*18*/                    vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO"));
/*19*/                    vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"0":rs.getString("MONEDA_LINEA"));
/*20*/                    vecColumnas.add((rs.getString("FN_IMPORTE_RECIBIR")==null)?"0":rs.getString("FN_IMPORTE_RECIBIR"));
/*21*/                    vecColumnas.add((rs.getString("FN_VALOR_TASA")==null)?"0":rs.getString("FN_VALOR_TASA"));
/*22*/                    vecColumnas.add((rs.getString("CG_CAMPO1")==null)?"0":rs.getString("CG_CAMPO1"));
                          vecColumnas.add((rs.getString("CG_CAMPO2")==null)?"0":rs.getString("CG_CAMPO2"));
                          vecColumnas.add((rs.getString("CG_CAMPO3")==null)?"0":rs.getString("CG_CAMPO3"));
                          vecColumnas.add((rs.getString("CG_CAMPO4")==null)?"0":rs.getString("CG_CAMPO4"));
                          vecColumnas.add((rs.getString("CG_CAMPO5")==null)?"0":rs.getString("CG_CAMPO5"));
												  vecColumnas.add((rs.getString("cg_numero_cuenta")==null)?"0":rs.getString("cg_numero_cuenta"));

                          /* for(int i=1;i<=numCamposAd;i++)
                            {
                            vecColumnas.addElement((rs.getString("CG_CAMPO"+i)==null)?"":rs.getString("CG_CAMPO"+i));
                            }   */

                          vecFilas.add(vecColumnas);
                }
                //System.out.println("vecFilas "+vecFilas);
                con.cierraStatement();

        }catch(Exception e){
		//System.out.println("Excepcion "+e);
                throw new AppException("Error al obtener informaci�n"+e);
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}

   private void insertaAcuse(
	String  acuse,
   	String	totalMontoMN,
	String	totalInteresMN,
	String	totalMontoDescMN,
	String	totalMontoUSD,
	String	totalInteresUSD,
	String	totalMontoDescUSD,
	String	icUsuario,
	String	cgReciboElectronico,
        AccesoDB con)
   throws NafinException{
        String qrySentencia = "";
   	try{
		qrySentencia =	" INSERT INTO COM_ACUSE3"+
                		" (cc_acuse,in_monto_mn,in_monto_int_mn,in_monto_dscto_mn"+
                                " ,in_monto_dl,in_monto_int_dl,in_monto_dscto_dl"+
                                " ,ic_usuario,df_fecha_hora,cg_recibo_electronico,ic_producto_nafin)"+
                                " values('"+acuse.toString()+"',"+totalMontoMN+","+totalInteresMN+","+totalMontoDescMN +
                                " ,"+totalMontoUSD+","+totalInteresUSD+","+totalMontoDescUSD+
                                " ,'"+icUsuario+"',sysdate,'"+cgReciboElectronico+"',4)";
                con.ejecutaSQL(qrySentencia);
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }

   }

 public String confirmaSolic(
	String		totalMontoMN,
	String		totalInteresMN,
	String		totalMontoDescMN,
	String		totalMontoUSD,
	String		totalInteresUSD,
	String		totalMontoDescUSD,
	String		icUsuario,
	String		cgReciboElectronico,
	ArrayList	listaEpos,
	String		cg_tipo_credito,
	HashMap		doctosPorEpo,
	HashMap		catalogoTipoFondeoPorEPO,
	ArrayList		li_responsable_int,
	ArrayList		li_tipo_cobranza,
	ArrayList		li_tipo_cobro_int  	)
 throws NafinException{

	 	  System.out.println("confirmaSolic(E)");

        AccesoDB 		con 				= null;
        String 		qrySentencia 	= "";
        Acuse       	acuse 			= null;

        String 							ic_estatus_solic 	= "1";

        boolean 							ok 					= true;
        try{
			  		 // 1. Validar que la EPO tenga definido el tipo de Fondeo
					 for(int k=0;k<listaEpos.size();k++){
						 String claveEpo 		= (String) 	listaEpos.get(k);
						 String csTipoFondeo = (String)	catalogoTipoFondeoPorEPO.get(claveEpo);
						 if ("".equals(csTipoFondeo) || csTipoFondeo == null){
							 ok = false;
							 throw new AppException("No se encuentra definido el tipo de Fondeo de la operacion, no es posible continuar. Informe al administrador.");
						 }
					 }

                con = new AccesoDB();
                con.conexionDB();

					 // Insertar acuse
                acuse = new Acuse(3,"4","com",con);
                //String acuseFormateado  = acuse.formatear();
                insertaAcuse(acuse.toString(),totalMontoMN,totalInteresMN,totalMontoDescMN,totalMontoUSD,totalInteresUSD,totalMontoDescUSD,icUsuario,cgReciboElectronico,con);

					 // Para cada EPO:
					 for(int k=0;k<listaEpos.size();k++){

						 // Determinar el estatus de la solicitud en base al tipo de fondeo de la EPO
						 String claveEpo 		= (String) 	listaEpos.get(k);
						 String csTipoFondeo = (String)	catalogoTipoFondeoPorEPO.get(claveEpo);
						 ic_estatus_solic = (("P".equals(csTipoFondeo)))?"10":"1"; // Si es fondeo Nafin = 1, Propio = 10	//Modificacion a peticion de Edgar.
						 //ic_estatus_solic 	= (("N".equals(csTipoFondeo)))?"1":"10"; // Si es fondeo Nafin = 1, Propio = 10

						 ArrayList doctos 	= (ArrayList)doctosPorEpo.get(claveEpo);
						 for(int j=0;j<doctos.size();j++){

							 // 1. Revisar si el docto ya se encuentra en comrel_producto_docto
							 qrySentencia = 	" select count(*) "+
		                        			" from comrel_producto_docto PD"+
													" ,dis_documento D"+
                                       " where PD.ic_epo = D.ic_epo"+
                                       " and PD.ic_producto_nafin = 4"+
                                       " and D.ic_documento = "+doctos.get(j);
							 ResultSet rs = con.queryDB(qrySentencia);
                      int aux = 0;
                      if(rs.next()){
                       	aux = rs.getInt(1);
                      }
                      con.cierraStatement();
                      if(aux==0){
                       	throw new NafinException("DIST0012");
                      }

							String  cg_representante_int =  li_responsable_int.get(0).toString();
							String  cg_tipo_cobranza =  li_tipo_cobranza.get(0).toString();
							String  cg_tipo_interes = li_tipo_cobro_int .get(0).toString();

							log.debug("cg_representante_int =="+cg_representante_int +"-----cg_tipo_cobranza == "+cg_tipo_cobranza+"-----cg_tipo_interes == "+cg_tipo_interes);


							 // 2. Insertar el documento
                      if("D".equals(cg_tipo_credito)){
                                      qrySentencia =  " insert into dis_solicitud" +
                                                    " (ic_documento,ic_estatus_solic,cc_acuse,ic_clase_docto"+
                                                    " ,cg_rmif,in_stif,df_v_credito,df_ppc,df_ppi,in_dia_pago,ic_tasaif,ic_tipo_credito, ic_if, df_fecha_solicitud  " +

																	 " ,CG_RESPONSABLE_INTERES ,  CG_TIPO_COBRANZA ,  IC_TIPO_COBRO_INTERES )  "+

																	 " select D.ic_documento,"+ic_estatus_solic+",'"+acuse.toString()+"',PD.ic_clase_docto" +
                                                    " ,'+',0,D.df_fecha_venc_credito,D.df_fecha_venc_credito,D.df_fecha_venc_credito,to_number(to_char(D.df_fecha_venc_credito,'dd')),DS.ic_tasa" +
																	 " ,decode(LC.ic_moneda,1,211,54,221), LC.ic_if, sysdate, '"+cg_representante_int+"', '"+cg_tipo_cobranza+"','"+cg_tipo_interes+"'"+
                                                    " from dis_documento D, comrel_producto_docto PD, dis_docto_seleccionado DS" +
																	 " ,dis_linea_credito_dm LC"+
                                                    " where D.ic_epo = PD.ic_epo" +
                                                    " and D.ic_producto_nafin = PD.ic_producto_nafin"+
                                                    " and D.ic_documento = DS.ic_documento"+
																	 " and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"+
                                                    " and D.ic_documento ="+doctos.get(j);

                        }else if("C".equals(cg_tipo_credito)){
                                      qrySentencia =  " insert into dis_solicitud" +
                                                    " (ic_documento,ic_estatus_solic,cc_acuse,ic_clase_docto"+
                                                    " ,cg_rmif,in_stif,df_v_credito,df_ppc,df_ppi,in_dia_pago,ic_tasaif,ic_tipo_credito, ic_if, df_fecha_solicitud, cs_factoraje_con_rec)"+
                                                    " select D.ic_documento,"+ic_estatus_solic+",'"+acuse.toString()+"',PD.ic_clase_docto" +
                                                    " ,'+',0,D.df_fecha_venc_credito,D.df_fecha_venc_credito,D.df_fecha_venc_credito,to_number(to_char(D.df_fecha_venc_credito,'dd')),DS.ic_tasa" +
																	 " ,decode(LC.ic_moneda,1,211,54,221), LC.ic_if, sysdate, 'N' "+
                                                    " from dis_documento D, comrel_producto_docto PD, dis_docto_seleccionado DS" +
																	 " ,com_linea_credito LC"+
                                                    " where D.ic_epo = PD.ic_epo" +
                                                    " and D.ic_producto_nafin = PD.ic_producto_nafin"+
                                                    " and D.ic_documento = DS.ic_documento"+
																	 " and D.ic_linea_credito = LC.ic_linea_credito"+
                                                    " and D.ic_documento ="+doctos.get(j);
                        }else if("F".equals(cg_tipo_credito)){
                                      qrySentencia =  " insert into dis_solicitud" +
                                                    " (ic_documento,ic_estatus_solic,cc_acuse,ic_clase_docto"+
                                                    " ,cg_rmif,in_stif,df_v_credito,df_ppc,df_ppi,in_dia_pago,ic_tasaif,ic_tipo_credito, ic_if, df_fecha_solicitud, cs_factoraje_con_rec)"+
                                                    " select D.ic_documento,"+ic_estatus_solic+",'"+acuse.toString()+"',PD.ic_clase_docto" +
                                                    " ,'+',0,D.df_fecha_venc_credito,D.df_fecha_venc_credito,D.df_fecha_venc_credito,to_number(to_char(D.df_fecha_venc_credito,'dd')),DS.ic_tasa" +
																	 " ,decode(LC.ic_moneda,1,211,54,221), LC.ic_if, sysdate, 'S'"+
                                                    " from dis_documento D, comrel_producto_docto PD, dis_docto_seleccionado DS" +
																	 " ,com_linea_credito LC"+
                                                    " where D.ic_epo = PD.ic_epo" +
                                                    " and D.ic_producto_nafin = PD.ic_producto_nafin"+
                                                    " and D.ic_documento = DS.ic_documento"+
																	 " and D.ic_linea_credito = LC.ic_linea_credito"+
                                                    " and D.ic_documento ="+doctos.get(j);
								}

								System.out.println("\n\n\nLA SENTENCIA DEL INSERT "+qrySentencia);
                        con.ejecutaSQL(qrySentencia);
								// 3. Actualizamos el estatus del documento:
								//  	3.1 Si es primer piso queda como seleccionado pyme
								// 	3.2 Si es segundo piso queda como operado
								if("D".equals(cg_tipo_credito)){
                                      qrySentencia =  " update dis_documento"+
                                                      " set ic_estatus_docto = "+
                                                      " (select decode(I.ig_tipo_piso,1,3,2,4)"+
                                                      "  from comcat_if I" +
                                                      "  ,dis_documento D" +
                                                      "  ,dis_linea_credito_dm LC" +
                                                      "  where D.ic_linea_credito_dm = LC.ic_linea_credito_dm" +
                                                      "  and D.ic_documento = "+doctos.get(j)+
                                                      "  and LC.ic_if = I.ic_if) " +
                                                      " where ic_documento = "+doctos.get(j);
                        }else if("C".equals(cg_tipo_credito) || "F".equals(cg_tipo_credito)){
                                      qrySentencia =  " update dis_documento"+
                                                      " set ic_estatus_docto = "+
                                                      " (select decode(I.ig_tipo_piso,1,3,2,4)"+
                                                      "  from comcat_if I" +
                                                      "  ,dis_documento D" +
                                                      "  ,com_linea_credito LC" +
                                                      "  where D.ic_linea_credito = LC.ic_linea_credito" +
                                                      "  and D.ic_documento = " +doctos.get(j)+
                                                      "  and LC.ic_if = I.ic_if) " +
                                                      " where ic_documento = "+doctos.get(j);
                        }
								//   System.out.println("\n\n\nLA SENTENCIA DEL UPDATE "+qrySentencia);
                        con.ejecutaSQL(qrySentencia);

						 }

					 }

        }catch(NafinException ne){
			  System.out.println("confirmaSolic(Exception)");
			  System.out.println("confirmaSolic(Exception).totalMontoMN 				= <"+totalMontoMN+">");
			  System.out.println("confirmaSolic(Exception).totalInteresMN 				= <"+totalInteresMN+">");
			  System.out.println("confirmaSolic(Exception).totalMontoDescMN 			= <"+totalMontoDescMN+">");
			  System.out.println("confirmaSolic(Exception).totalMontoUSD 				= <"+totalMontoUSD+">");
			  System.out.println("confirmaSolic(Exception).totalInteresUSD 			= <"+totalInteresUSD+">");
			  System.out.println("confirmaSolic(Exception).totalMontoDescUSD 			= <"+totalMontoDescUSD+">");
			  System.out.println("confirmaSolic(Exception).icUsuario 					= <"+icUsuario+">");
			  System.out.println("confirmaSolic(Exception).cgReciboElectronico 		= <"+cgReciboElectronico+">");
			  System.out.println("confirmaSolic(Exception).listaEpos 					= <"+listaEpos+">");
			  System.out.println("confirmaSolic(Exception).cg_tipo_credito 			= <"+cg_tipo_credito+">");
			  System.out.println("confirmaSolic(Exception).doctosPorEpo 				= <"+doctosPorEpo+">");
			  System.out.println("confirmaSolic(Exception).catalogoTipoFondeoPorEPO = <"+catalogoTipoFondeoPorEPO+">");
			  ok = false;
           ne.printStackTrace();
			  throw ne;
        }catch(Exception e){
			  System.out.println("confirmaSolic(Exception)");
			  System.out.println("confirmaSolic(Exception).totalMontoMN 				= <"+totalMontoMN+">");
			  System.out.println("confirmaSolic(Exception).totalInteresMN 				= <"+totalInteresMN+">");
			  System.out.println("confirmaSolic(Exception).totalMontoDescMN 			= <"+totalMontoDescMN+">");
			  System.out.println("confirmaSolic(Exception).totalMontoUSD 				= <"+totalMontoUSD+">");
			  System.out.println("confirmaSolic(Exception).totalInteresUSD 			= <"+totalInteresUSD+">");
			  System.out.println("confirmaSolic(Exception).totalMontoDescUSD 			= <"+totalMontoDescUSD+">");
			  System.out.println("confirmaSolic(Exception).icUsuario 					= <"+icUsuario+">");
			  System.out.println("confirmaSolic(Exception).cgReciboElectronico 		= <"+cgReciboElectronico+">");
			  System.out.println("confirmaSolic(Exception).listaEpos 					= <"+listaEpos+">");
			  System.out.println("confirmaSolic(Exception).cg_tipo_credito 			= <"+cg_tipo_credito+">");
			  System.out.println("confirmaSolic(Exception).doctosPorEpo 				= <"+doctosPorEpo+">");
			  System.out.println("confirmaSolic(Exception).catalogoTipoFondeoPorEPO = <"+catalogoTipoFondeoPorEPO+">");
			  ok = false;
           e.printStackTrace();
			  System.out.println("Excepcion "+e);
        }finally{
			  if(con.hayConexionAbierta()){
				  con.terminaTransaccion(ok);
              con.cierraConexionDB();
			  }
			  System.out.println("confirmaSolic(S)");
        }

		  return acuse.toString();

   }

  public String confirmaSolic(
	String	totalMontoMN,
	String	totalInteresMN,
	String	totalMontoDescMN,
	String	totalMontoUSD,
	String	totalInteresUSD,
	String	totalMontoDescUSD,
	String	icUsuario,
	String	cgReciboElectronico,
	String	ic_documento[],
	String	cg_tipo_credito,
  String  csTipoFondeo
        )
   throws NafinException{

        AccesoDB con = null;
        String qrySentencia = "";
        Acuse       acuse = null;
        String ic_estatus_solic = "1";

        boolean ok = true;
        try{
                if ("".equals(csTipoFondeo) || csTipoFondeo == null){
                  ok = false;
                  throw new AppException("No se encuentra definido el tipo de Fondeo de la operacion, no es posible continuar. Informe al administrador.");
                }
                con = new AccesoDB();
					 ic_estatus_solic = (("P".equals(csTipoFondeo)))?"10":"1"; // Si es fondeo Nafin = 1, Propio = 10
                //ic_estatus_solic = (("N".equals(csTipoFondeo)))?"1":"10"; // Si es fondeo Nafin = 1, Propio = 10	//version anterior, //Modificacion a peticion de Edgar.
                con.conexionDB();
                acuse = new Acuse(3,"4","com",con);
                //String acuseFormateado  = acuse.formatear();
                insertaAcuse(acuse.toString(),totalMontoMN,totalInteresMN,totalMontoDescMN,totalMontoUSD,totalInteresUSD,totalMontoDescUSD,icUsuario,cgReciboElectronico,con);
                for(int i=0;i<ic_documento.length;i++){
                        qrySentencia = 	" select count(*) "+
		                        		" from comrel_producto_docto PD"+
		                        		" ,dis_documento D"+
                                        " where PD.ic_epo = D.ic_epo"+
                                        " and PD.ic_producto_nafin = 4"+
                                        " and D.ic_documento = "+ic_documento[i];
                        ResultSet rs = con.queryDB(qrySentencia);
                        int aux = 0;
                        if(rs.next()){
                        	aux = rs.getInt(1);
                        }
                        con.cierraStatement();
                        if(aux==0){
                        	throw new NafinException("DIST0012");
                        }
                        //insersion del credito
                        if("D".equals(cg_tipo_credito)){
                                      qrySentencia =  " insert into dis_solicitud" +
                                                    " (ic_documento,ic_estatus_solic,cc_acuse,ic_clase_docto"+
                                                    " ,cg_rmif,in_stif,df_v_credito,df_ppc,df_ppi,in_dia_pago,ic_tasaif,ic_tipo_credito, ic_if, df_fecha_solicitud)"+
                                                    " select D.ic_documento,"+ic_estatus_solic+",'"+acuse.toString()+"',PD.ic_clase_docto" +
                                                    " ,'+',0,D.df_fecha_venc_credito,D.df_fecha_venc_credito,D.df_fecha_venc_credito,to_number(to_char(D.df_fecha_venc_credito,'dd')),DS.ic_tasa" +
                                " ,decode(LC.ic_moneda,1,211,54,221), LC.ic_if, sysdate"+
                                                    " from dis_documento D, comrel_producto_docto PD, dis_docto_seleccionado DS" +
                                " ,dis_linea_credito_dm LC"+
                                                    " where D.ic_epo = PD.ic_epo" +
                                                    " and D.ic_producto_nafin = PD.ic_producto_nafin"+
                                                    " and D.ic_documento = DS.ic_documento"+
                                " and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"+
                                                    " and D.ic_documento =" +ic_documento[i];

                        }else if("C".equals(cg_tipo_credito)){
                                      qrySentencia =  " insert into dis_solicitud" +
                                                    " (ic_documento,ic_estatus_solic,cc_acuse,ic_clase_docto"+
                                                    " ,cg_rmif,in_stif,df_v_credito,df_ppc,df_ppi,in_dia_pago,ic_tasaif,ic_tipo_credito, ic_if, df_fecha_solicitud)"+
                                                    " select D.ic_documento,"+ic_estatus_solic+",'"+acuse.toString()+"',PD.ic_clase_docto" +
                                                    " ,'+',0,D.df_fecha_venc_credito,D.df_fecha_venc_credito,D.df_fecha_venc_credito,to_number(to_char(D.df_fecha_venc_credito,'dd')),DS.ic_tasa" +
                                " ,decode(LC.ic_moneda,1,211,54,221), LC.ic_if, sysdate"+
                                                    " from dis_documento D, comrel_producto_docto PD, dis_docto_seleccionado DS" +
                                " ,com_linea_credito LC"+
                                                    " where D.ic_epo = PD.ic_epo" +
                                                    " and D.ic_producto_nafin = PD.ic_producto_nafin"+
                                                    " and D.ic_documento = DS.ic_documento"+
                                " and D.ic_linea_credito = LC.ic_linea_credito"+
                                                    " and D.ic_documento =" +ic_documento[i];
                        }

                        System.out.println("\n\n\nLA SENTENCIA DEL INSERT "+qrySentencia);
                        con.ejecutaSQL(qrySentencia);
                        //actualizamos el estatus del documento:
                        //si es primer piso queda como seleccionado pyme
                        // si es segundo piso queda como operado
                        if("D".equals(cg_tipo_credito)){
                                      qrySentencia =  " update dis_documento"+
                                                      " set ic_estatus_docto = "+
                                                      " (select decode(I.ig_tipo_piso,1,3,2,4)"+
                                                      "  from comcat_if I" +
                                                      "  ,dis_documento D" +
                                                      "  ,dis_linea_credito_dm LC" +
                                                      "  where D.ic_linea_credito_dm = LC.ic_linea_credito_dm" +
                                                      "  and D.ic_documento = " +ic_documento[i]+
                                                      "  and LC.ic_if = I.ic_if) " +
                                                      " where ic_documento = "+ic_documento[i];
                                      }else if("C".equals(cg_tipo_credito)){
                                      qrySentencia =  " update dis_documento"+
                                                      " set ic_estatus_docto = "+
                                                      " (select decode(I.ig_tipo_piso,1,3,2,4)"+
                                                      "  from comcat_if I" +
                                                      "  ,dis_documento D" +
                                                      "  ,com_linea_credito LC" +
                                                      "  where D.ic_linea_credito = LC.ic_linea_credito" +
                                                      "  and D.ic_documento = " +ic_documento[i]+
                                                      "  and LC.ic_if = I.ic_if) " +
                                                      " where ic_documento = "+ic_documento[i];
                                      }
              //                        System.out.println("\n\n\nLA SENTENCIA DEL UPDATE "+qrySentencia);
                                con.ejecutaSQL(qrySentencia);
                              }
        }catch(NafinException ne){
        	ok = false;
          throw ne;
        }catch(Exception e){
        	ok = false;
          System.out.println("Excepcion "+e);
          //throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                	con.terminaTransaccion(ok);
                 	con.cierraConexionDB();
          }
        }
   return acuse.toString();
   }

public Vector getDoctosSeleccionadosIF(String cc_acuse,String cg_tipo_credito)
	throws NafinException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();
		if("D".equals(cg_tipo_credito)){
        	    qrySentencia =  "  SELECT  D.ig_numero_docto"   +
						"  		,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
						" 		,P.cg_razon_social as DISTRIBUIDOR"   +
						"		,P.ic_pyme"+
						" 		,M.cd_nombre as MONEDA"   +
						"		,D.ic_moneda"+
						" 		,D.fn_monto"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPO_CONVERSION"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
						//" 		,CT.CD_NOMBRE AS REFERENCIA_INT		 "   +
						"		,CT.CD_NOMBRE||' '||DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REFERENCIA_INT " +
						" 		,DECODE(DS.CG_REL_MAT"+
                        "		,'+',DS.fn_valor_tasa + DS.fn_puntos"+
                        "		,'-',DS.fn_valor_tasa - DS.fn_puntos"+
                        "		,'*',DS.fn_valor_tasa * DS.fn_puntos"+
                        "		,'/',DS.fn_valor_tasa / DS.fn_puntos"+
                        "		,0) AS VALOR_TASA"   +
                        " 		,D.IG_PLAZO_CREDITO"   +
                        " 		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO		"   +
                        " 		,TCI.ic_tipo_cobro_interes"   +
                        "		,TCI.cd_descripcion as tipo_cobro_interes"+
                        "		,DS.fn_importe_interes as MONTO_INTERES"+
                        "		,D.ic_documento" +
                        "		,E.cg_razon_social as EPO"+
                        "		,nvl(D.fn_porc_descuento,0) as fn_porc_descuento"+
                        "		, to_char(ca.df_fecha_hora,'dd/mm/yyyy') as fecha_carga"   +
                        "		, to_char(ca.df_fecha_hora,'hh:mi') as hora_carga"   +
                        "		, ' ' as usuario"   +
                        "		,LCD.ic_moneda as MONEDA_LINEA"+
						" 		,DS.fn_importe_recibir"   +
						"       ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa, "+
						"       NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver, "+
						"       d.fn_tipo_cambio as tipo_cambio, "+
						"       NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes, "+
						"       NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as tipo_cobranza "+
						"		,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5 "+
						"		,DS.fn_valor_tasa"+
						"  , lcd.CG_NUM_CUENTA_IF as cg_numero_cuenta "+
                  "  , D.IC_EPO as ic_epo "  +
                        "  FROM DIS_DOCUMENTO D"   +
                        "    ,COMCAT_PYME P"   +
                        "    ,COMREL_PRODUCTO_EPO PE"   +
       					"	 ,comrel_pyme_epo cpe "+
                        "    ,COMCAT_PRODUCTO_NAFIN PN"   +
                        "    ,COMCAT_MONEDA M"   +
                        "    ,COM_TIPO_CAMBIO TC"   +
                        "    ,DIS_LINEA_CREDITO_DM LCD"   +
                        "    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
                        "    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
                        "    ,COMCAT_TASA CT"   +
                        "    ,COMCAT_EPO E"+
                        "    ,DIS_DOCTO_SELECCIONADO DS"   +
                        "    ,DIS_SOLICITUD S"+
                        "    ,COM_ACUSE3 CA"+
						" 	 ,COMCAT_IF I"+
                        "  WHERE D.IC_EPO = PE.IC_EPO"   +
                        "    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND PN.IC_PRODUCTO_NAFIN = 4"   +
                        "    AND P.IC_PYME = D.IC_PYME"   +
                        "    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                        "    AND D.IC_EPO = PE.IC_EPO"   +
						"    AND cpe.ic_epo = d.ic_epo "+
						"    AND cpe.ic_pyme = d.ic_pyme "+
//                        "    AND D.IC_MONEDA = M.IC_MONEDA"   +
                        "    AND M.IC_MONEDA = TC.IC_MONEDA"   +
                        "    AND TC.IC_MONEDA = LCD.IC_MONEDA"   +
                        "    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                        "    AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
                        "    AND LCD.IC_IF = IEP.IC_IF"   +
                        "    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
                        "    AND D.ic_estatus_docto = decode(I.ig_tipo_piso,1,3,2,4)"   +
                        "    AND IEP.IC_EPO = D.IC_EPO"   +
                        "    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND DS.IC_TASA = CT.IC_TASA "  +
                        "    AND D.IC_EPO = E.IC_EPO "+
                        "    AND D.IC_DOCUMENTO = S.IC_DOCUMENTO"+
                        "    AND S.CC_ACUSE = '"+cc_acuse+"'"+
                        "    AND S.CC_ACUSE = CA.CC_ACUSE"+
						"	 AND I.IC_IF = IEP.IC_IF"+
                        "  ORDER BY D.IG_NUMERO_DOCTO";
                    }else if("C".equals(cg_tipo_credito) || "F".equals(cg_tipo_credito)){
		       qrySentencia = "  SELECT  D.ig_numero_docto"   +
						"  		,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
						" 		,P.cg_razon_social as DISTRIBUIDOR"   +
			                        "               ,P.ic_pyme"+
						" 		,M.cd_nombre as MONEDA"   +
			                        "               ,D.ic_moneda"+
						" 		,D.fn_monto"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPO_CONVERSION"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
						//" 		,CT.CD_NOMBRE AS REFERENCIA_INT		 "   +
						"		,CT.CD_NOMBRE||' '||DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REFERENCIA_INT " +
						" 		,DECODE(DS.CG_REL_MAT"+
                        "               ,'+',DS.fn_valor_tasa + DS.fn_puntos"+
                        "               ,'-',DS.fn_valor_tasa - DS.fn_puntos"+
                        "               ,'*',DS.fn_valor_tasa * DS.fn_puntos"+
                        "               ,'/',DS.fn_valor_tasa / DS.fn_puntos"+
                        "               ,0) AS VALOR_TASA"   +
                        " 		,D.IG_PLAZO_CREDITO"   +
                        " 		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO		"   +
                        " 		,TCI.ic_tipo_cobro_interes"   +
                        "               ,TCI.cd_descripcion as tipo_cobro_interes"+
                        "               ,DS.fn_importe_interes as MONTO_INTERES"+
                        "               ,D.ic_documento" +
                        "               ,E.cg_razon_social as EPO"+
                        "               ,nvl(D.fn_porc_descuento,0) as fn_porc_descuento"+
                        "               , to_char(ca.df_fecha_hora,'dd/mm/yyyy') as fecha_carga"   +
                        "               , to_char(ca.df_fecha_hora,'hh:mi') as hora_carga"   +
                        "               , ' ' as usuario"   +
                        "		,LC.ic_moneda as MONEDA_LINEA"+
								"		,DS.fn_importe_recibir"+
								"		,DS.fn_valor_tasa"+
								"     ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa "+ //F003-2012
								"     ,NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver "+ //F003-2012
								"     ,d.fn_tipo_cambio as tipo_cambio "+ //F003-2012
								"     ,NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes "+ //F003-2012
								"     ,NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as tipo_cobranza "+ //F003-2012
								"		,ct_referencia " + //F003-2012
            "		,cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5 "+
						"  , lc.cg_numero_cuenta as cg_numero_cuenta " +
                  "  ,  D.IC_EPO " +									 
							  " , d.ig_tipo_pago AS TIPOPAGO "+ //F09-2015									 
                        "  FROM DIS_DOCUMENTO D"   +
                        "    ,COMCAT_PYME P"   +
								"	  ,COMREL_PYME_EPO CPE "+
                        "    ,COMREL_PRODUCTO_EPO PE"   +
                        "    ,COMCAT_PRODUCTO_NAFIN PN"   +
                        "    ,COMCAT_MONEDA M"   +
                        "    ,COM_TIPO_CAMBIO TC"   +
                        "    ,COM_LINEA_CREDITO LC"   +
                        "    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
                        "    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
                        "    ,COMCAT_TASA CT"   +
                        "    ,COMCAT_EPO E"+
                        "    ,DIS_DOCTO_SELECCIONADO DS"   +
                        "    ,DIS_SOLICITUD S"+
                        "    ,COM_ACUSE3 CA"+
						"	 ,COMCAT_IF I"+
                        "  WHERE D.IC_EPO = PE.IC_EPO"   +
                        "    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND PN.IC_PRODUCTO_NAFIN = 4"   +
                        "    AND P.IC_PYME = D.IC_PYME"   +
                        "    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                        "    AND D.IC_EPO = PE.IC_EPO"   +
								"    AND CPE.IC_EPO = D.IC_EPO "+
								"    AND CPE.IC_PYME = D.IC_PYME "+
//                        "    AND D.IC_MONEDA = M.IC_MONEDA"   +
                        "    AND M.IC_MONEDA = TC.IC_MONEDA"   +
                        "    AND TC.IC_MONEDA = LC.IC_MONEDA"   +
                        "    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                        "    AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
                        "    AND LC.IC_IF = IEP.IC_IF"   +
                        "    AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
                        "    AND D.ic_estatus_docto = decode(I.ig_tipo_piso,1,3,2,4)"   +
                        "    AND IEP.IC_EPO = D.IC_EPO"   +
                        "    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                        "    AND DS.IC_TASA = CT.IC_TASA "  +
                        "    AND D.IC_EPO = E.IC_EPO "+
                        "    AND D.IC_DOCUMENTO = S.IC_DOCUMENTO"+
                        "    AND S.CC_ACUSE = '"+cc_acuse+"'"+
                        "    AND S.CC_ACUSE = CA.CC_ACUSE"+
						"	 AND I.IC_IF = IEP.IC_IF"+
                        "  ORDER BY D.IG_NUMERO_DOCTO"  ;
                    }

System.out.println("******* \n qrySentencia: "+qrySentencia);

                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	  vecColumnas = new Vector();
/*0*/						vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*1*/						vecColumnas.add((rs.getString("DF_FECHA_SELECCION")==null)?"":rs.getString("DF_FECHA_SELECCION"));
/*2*/						vecColumnas.add((rs.getString("DISTRIBUIDOR")==null)?"":rs.getString("DISTRIBUIDOR"));
/*3*/						vecColumnas.add((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*4*/						vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*5*/						vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*6*/						vecColumnas.add((rs.getString("FN_MONTO")==null)?"":rs.getString("FN_MONTO"));
/*7*/						vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*8*/						vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*9*/						vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*10*/					vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*11*/					vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*12*/					vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*13*/					vecColumnas.add((rs.getString("TIPO_COBRO_INTERES")==null)?"":rs.getString("TIPO_COBRO_INTERES"));
/*14*/					vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*15*/					vecColumnas.add((rs.getString("MONTO_INTERES")==null)?"":rs.getString("MONTO_INTERES"));
/*16*/					vecColumnas.add((rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO"));
/*17*/					vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*18*/					vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO"));
							String  acuseFormateado  = cc_acuse.substring(0,1)+"-"+cc_acuse.substring(1,7)+"-"+cc_acuse.substring(7,9)+"-"+cc_acuse.substring(9,15);
/*19*/					vecColumnas.add(acuseFormateado);
/*20*/					vecColumnas.add((rs.getString("FECHA_CARGA")==null)?"":rs.getString("FECHA_CARGA"));
/*21*/					vecColumnas.add((rs.getString("HORA_CARGA")==null)?"":rs.getString("HORA_CARGA"));
/*22*/					vecColumnas.add((rs.getString("USUARIO")==null)?"":rs.getString("USUARIO"));
/*23*/					vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*24*/					vecColumnas.add((rs.getString("FN_IMPORTE_RECIBIR")==null)?"":rs.getString("FN_IMPORTE_RECIBIR"));
						if("D".equals(cg_tipo_credito)){
/*25*/					vecColumnas.add((rs.getString("numdist")==null)?"":rs.getString("numdist"));
/*26*/					vecColumnas.add((rs.getString("cve_tasa")==null)?"":rs.getString("cve_tasa"));
/*27*/					vecColumnas.add((rs.getString("tipo_conver")==null)?"":rs.getString("tipo_conver"));
/*28*/					vecColumnas.add((rs.getString("tipo_cambio")==null)?"":rs.getString("tipo_cambio"));
/*29*/					vecColumnas.add((rs.getString("resp_interes")==null)?"":rs.getString("resp_interes"));
/*30*/					vecColumnas.add((rs.getString("tipo_cobranza")==null)?"":rs.getString("tipo_cobranza"));
/*31*/					vecColumnas.add((rs.getString("ct_referencia")==null)?"":rs.getString("ct_referencia"));
						}

/*32 o 25*/			vecColumnas.add((rs.getString("cg_campo1")==null)?"":rs.getString("cg_campo1"));
/*33 o 26*/			vecColumnas.add((rs.getString("cg_campo2")==null)?"":rs.getString("cg_campo2"));
/*34 o 27*/			vecColumnas.add((rs.getString("cg_campo3")==null)?"":rs.getString("cg_campo3"));
/*35 o 28*/			vecColumnas.add((rs.getString("cg_campo4")==null)?"":rs.getString("cg_campo4"));
/*36 o 29*/			vecColumnas.add((rs.getString("cg_campo5")==null)?"":rs.getString("cg_campo5"));

/*37 O 30*/			vecColumnas.add((rs.getString("fn_valor_tasa")==null)?"":rs.getString("fn_valor_tasa"));
/*38 O 31*/			vecColumnas.add((rs.getString("cg_numero_cuenta")==null)?"":rs.getString("cg_numero_cuenta"));
/*39 O 32*/			vecColumnas.add((rs.getString("ic_epo")==null)?"":rs.getString("ic_epo"));

						if("C".equals(cg_tipo_credito)	||	"F".equals(cg_tipo_credito)){
/*33*/					vecColumnas.add((rs.getString("numdist")==null)?"":rs.getString("numdist"));
/*34*/            	vecColumnas.add((rs.getString("cve_tasa")==null)?"":rs.getString("cve_tasa"));
/*35*/               vecColumnas.add((rs.getString("tipo_conver")==null)?"":rs.getString("tipo_conver"));
/*36*/               vecColumnas.add((rs.getString("tipo_cambio")==null)?"":rs.getString("tipo_cambio"));
/*37*/               vecColumnas.add((rs.getString("resp_interes")==null)?"":rs.getString("resp_interes"));
/*38*/               vecColumnas.add((rs.getString("tipo_cobranza")==null)?"":rs.getString("tipo_cobranza"));
/*39*/               vecColumnas.add((rs.getString("ct_referencia")==null)?"":rs.getString("ct_referencia"));
/*40*/               vecColumnas.add((rs.getString("TIPOPAGO")==null)?"":rs.getString("TIPOPAGO"));
						}



                          vecFilas.add(vecColumnas);
                }
                con.cierraStatement();

        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}


public Vector getDoctosSeleccionadosNafin(String ic_if,String ic_tipo_cobro_int,String df_fecha_operacion_de,String df_fecha_operacion_a,String ic_documento,String ig_numero_docto,String ic_moneda,String ic_estatus_solic,String tipoLineaCredito,String in)
	throws NafinException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String qrySentenciaDM = "";
        String qrySentenciaCCC = "";
        String condicion    = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();

                if(!"".equals(ic_if)&&ic_if!=null)
                       condicion += " AND LCD.IC_IF = "+ic_if;

                if(!"".equals(ic_documento)&&ic_documento!=null)
                       condicion += " AND D.IC_DOCUMENTO = "+ic_documento;

                if(!"".equals(ic_moneda)&&ic_moneda!=null)
                       condicion += " AND D.IC_MONEDA = "+ic_moneda;

                if(!"".equals(ic_estatus_solic)&&ic_estatus_solic!=null)
                       condicion += " AND S.IC_ESTATUS_SOLIC = "+ic_estatus_solic;

                if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
                       condicion += " AND TCI.ic_tipo_cobro_interes = "+ic_tipo_cobro_int;

                if(!"".equals(ig_numero_docto)&&ig_numero_docto!=null)
                	condicion += " AND D.IG_NUMERO_DOCTO = '"+ig_numero_docto+"'";

                if(!"".equals(df_fecha_operacion_de)&&df_fecha_operacion_de!=null&&!"".equals(df_fecha_operacion_a)&&df_fecha_operacion_a!=null)
                	condicion += "  AND TRUNC(CA.DF_FECHA_HORA)"+
                                     "  between TRUNC(TO_DATE('"+df_fecha_operacion_de+"','DD/MM/YYYY'))"+
                                     "  and TRUNC(TO_DATE('"+df_fecha_operacion_a+"','DD/MM/YYYY'))";
                if(!"".equals(in)&&in!=null)
                        condicion = " AND D.IC_DOCUMENTO IN("+in+")";


/*                if("".equals(condicion))
                        condicion += "  AND TRUNC(CA.DF_FECHA_HORA) = TRUNC(SYSDATE)";*/

                if("".equals(ic_estatus_solic))
                	condicion += "  AND S.IC_ESTATUS_SOLIC = 1";

                qrySentenciaDM =  "  SELECT  E.cg_razon_social as EPO"+
						"		,P.cg_razon_social as DISTRIBUIDOR"   +
						"		,D.ig_numero_docto"   +
						"		,to_char(D.DF_FECHA_EMISION,'dd/mm/yyyy') as DF_FECHA_EMISION "   +
						"		,to_char(D.DF_FECHA_VENC,'dd/mm/yyyy') as DF_FECHA_VENC "   +
						"		,P.ic_pyme"+
						"		,D.ig_plazo_docto"+
						"		,M.ic_moneda"+
						"		,M.cd_nombre as MONEDA"   +
						"		,D.fn_monto"   +
						"		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
						"		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
						"		,D.IG_PLAZO_DESCUENTO"   +
						"		,decode(D.ic_tipo_financiamiento,1,nvl(D.fn_porc_descuento,0),0) as fn_porc_descuento"+
						"		,TF.cd_descripcion as MODO_PLAZO"+
						"		,TCI.cd_descripcion as tipo_cobro_interes"+
						"		,ES.cd_descripcion as ESTATUS"+
						"		,D.ic_documento" +
						"		,I.ic_if"+
						"		,I.cg_razon_social as NOMBRE_IF"+
						"		,PEP.cg_tipo_credito AS TIPO_LINEA_CREDITO"+
						"		,TO_CHAR(CA.df_fecha_hora,'dd/mm/yyyy') as df_fecha_seleccion"   +
						"		,DS.fn_importe_interes as MONTO_INTERES"+
						"		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO	 "   +
						"		,CT.CD_NOMBRE AS REFERENCIA_INT		 "   +
						"		,DECODE(DS.CG_REL_MAT"+
						"			,'+',DS.fn_valor_tasa + DS.fn_puntos"+
						"			,'-',DS.fn_valor_tasa - DS.fn_puntos"+
						"			,'*',DS.fn_valor_tasa * DS.fn_puntos"+
						"			,'/',DS.fn_valor_tasa / DS.fn_puntos"+
						"		,0) AS VALOR_TASA"   +
						" 		,TCI.ic_tipo_cobro_interes"   +
						"		,decode(nvl(PE.cg_tipo_cobranza,PN.cg_tipo_cobranza),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
						"		,I.ig_tipo_piso"+
						"		,D.ig_plazo_credito"+
						"		,decode(PEP.cg_tipo_credito,'D','Descuento Mercantil','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
						"		,LCD.ic_moneda as MONEDA_LINEA"+
						"		,DS.fn_importe_recibir as MONTO_CREDITO"+
						"		,Ds.FN_VALOR_TASA"+
						"  FROM DIS_DOCUMENTO D"   +
						"    ,COMCAT_PYME P"   +
						"    ,COMREL_PRODUCTO_EPO PE"   +
						"    ,COMCAT_PRODUCTO_NAFIN PN"   +
						"    ,COMCAT_MONEDA M"   +
						"    ,COM_TIPO_CAMBIO TC"   +
						"    ,DIS_LINEA_CREDITO_DM LCD"   +
						"    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
						"    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
						"    ,COMCAT_TASA CT"   +
						"    ,COMCAT_EPO E"+
						"    ,COMCAT_IF I"+
						"    ,DIS_DOCTO_SELECCIONADO DS"   +
						"    ,DIS_SOLICITUD S"  +
						"    ,COMCAT_ESTATUS_SOLIC ES"+
						"    ,COM_ACUSE3 CA"+
						"    ,COMCAT_TIPO_FINANCIAMIENTO TF"+
						"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
						"  WHERE D.IC_EPO = PE.IC_EPO"   +
						"    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
						"    AND PN.IC_PRODUCTO_NAFIN = 4"   +
						"    AND PEP.IC_EPO = E.IC_EPO"+
						"    AND PEP.IC_PYME = P.IC_PYME"+
						"    AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
						"    AND P.IC_PYME = D.IC_PYME"   +
						"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
						"    AND D.IC_EPO = PE.IC_EPO"   +
						"    AND D.IC_MONEDA = M.IC_MONEDA"   +
						"    AND M.IC_MONEDA = TC.IC_MONEDA"   +
						"    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
						"    AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
						"    AND LCD.IC_IF = IEP.IC_IF"   +
						"    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
//						"    AND D.ic_estatus_docto IN(3,4)"   +
						"    AND IEP.IC_EPO = D.IC_EPO"   +
						"    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
						"    AND DS.IC_TASA = CT.IC_TASA "  +
						"    AND D.IC_EPO = E.IC_EPO "+
						"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"+
						"    AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC"+
						"	 AND S.IC_BLOQUEO = 1"+
						"    AND I.IC_IF = LCD.IC_IF"+
						"    AND S.CC_ACUSE = CA.CC_ACUSE"+
						"    AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
                        condicion;

                qrySentenciaCCC =  "  SELECT  E.cg_razon_social as EPO"+
						" 		,P.cg_razon_social as DISTRIBUIDOR"   +
						"       ,D.ig_numero_docto"   +
						" 		,to_char(D.DF_FECHA_EMISION,'dd/mm/yyyy') as DF_FECHA_EMISION "   +
						" 		,to_char(D.DF_FECHA_VENC,'dd/mm/yyyy') as DF_FECHA_VENC "   +
						"       ,P.ic_pyme"+
						"       ,D.ig_plazo_docto"+
						"       ,M.ic_moneda"+
						" 		,M.cd_nombre as MONEDA"   +
						" 		,D.fn_monto"   +
						"		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
						" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
						" 		,D.IG_PLAZO_DESCUENTO"   +
						"		,decode(D.ic_tipo_financiamiento,1,nvl(D.fn_porc_descuento,0),0) as fn_porc_descuento"+
						"       ,TF.cd_descripcion as MODO_PLAZO"+
						"       ,TCI.cd_descripcion as tipo_cobro_interes"+
						"       ,ES.cd_descripcion as ESTATUS"+
						"       ,D.ic_documento" +
						"       ,I.ic_if"+
						"       ,I.cg_razon_social as NOMBRE_IF"+
						"       ,PEP.cg_tipo_credito AS TIPO_LINEA_CREDITO"+
						"  		,TO_CHAR(CA.df_fecha_hora,'dd/mm/yyyy') as df_fecha_seleccion"   +
						"       ,DS.fn_importe_interes as MONTO_INTERES"+
						" 		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO	 "   +
						" 		,CT.CD_NOMBRE AS REFERENCIA_INT		 "   +
						" 		,DECODE(DS.CG_REL_MAT"+
						"       ,'+',DS.fn_valor_tasa + DS.fn_puntos"+
						"       ,'-',DS.fn_valor_tasa - DS.fn_puntos"+
						"       ,'*',DS.fn_valor_tasa * DS.fn_puntos"+
						"       ,'/',DS.fn_valor_tasa / DS.fn_puntos"+
						"       ,0) AS VALOR_TASA"   +
						" 		,TCI.ic_tipo_cobro_interes"   +
						"       ,decode(nvl(PE.cg_tipo_cobranza,PN.cg_tipo_cobranza),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
						"       ,I.ig_tipo_piso"+
						"       ,D.ig_plazo_credito"+
						"		,decode(PEP.cg_tipo_credito,'D','Descuento Mercantil','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
						"		,LCD.ic_moneda as MONEDA_LINEA"+
						"		,DS.fn_importe_recibir as MONTO_CREDITO"+
						"		,DS.fn_valor_tasa"+
						"  FROM DIS_DOCUMENTO D"   +
						"    ,COMCAT_PYME P"   +
						"    ,COMREL_PRODUCTO_EPO PE"   +
						"    ,COMCAT_PRODUCTO_NAFIN PN"   +
						"    ,COMCAT_MONEDA M"   +
						"    ,COM_TIPO_CAMBIO TC"   +
						"    ,COM_LINEA_CREDITO LCD"   +
						"    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
						"    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
						"    ,COMCAT_TASA CT"   +
						"    ,COMCAT_EPO E"+
						"    ,COMCAT_IF I"+
						"    ,DIS_DOCTO_SELECCIONADO DS"   +
						"    ,DIS_SOLICITUD S"  +
						"    ,COMCAT_ESTATUS_SOLIC ES"+
						"    ,COM_ACUSE3 CA"+
						"    ,COMCAT_TIPO_FINANCIAMIENTO TF"+
						"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
						"  WHERE D.IC_EPO = PE.IC_EPO"   +
						"    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
						"    AND PN.IC_PRODUCTO_NAFIN = 4"   +
						"    AND PEP.IC_EPO = E.IC_EPO"+
						"    AND PEP.IC_PYME = P.IC_PYME"+
						"    AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
						"    AND P.IC_PYME = D.IC_PYME"   +
						"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
						"    AND D.IC_EPO = PE.IC_EPO"   +
						"    AND D.IC_MONEDA = M.IC_MONEDA"   +
						"    AND M.IC_MONEDA = TC.IC_MONEDA"   +
						"    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
						"    AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
						"    AND LCD.IC_IF = IEP.IC_IF"   +
						"    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
						//                        "    AND D.ic_estatus_docto IN(3,4)"   +
						"    AND IEP.IC_EPO = D.IC_EPO"   +
						"    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
						"    AND DS.IC_TASA = CT.IC_TASA "  +
						"    AND D.IC_EPO = E.IC_EPO "+
						"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"+
						"    AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC"+
						"	 AND S.IC_BLOQUEO = 1"+
						"    AND I.IC_IF = LCD.IC_IF"+
						"    AND S.CC_ACUSE = CA.CC_ACUSE"+
						"    AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
						condicion;
                qrySentencia = " SELECT * FROM (";
        	if("D".equals(tipoLineaCredito)){
                	qrySentencia += qrySentenciaDM;
                }else if("C".equals(tipoLineaCredito)){
                	qrySentencia += qrySentenciaCCC;
                }else{
                	qrySentencia += qrySentenciaDM +" UNION ALL "+qrySentenciaCCC;
                }
                qrySentencia += "  )ORDER BY 1,3";
                System.out.println(qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){

                	 vecColumnas = new Vector();
/*0*/                    vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*1*/                    vecColumnas.add((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));
/*2*/                    vecColumnas.add((rs.getString("DISTRIBUIDOR")==null)?"":rs.getString("DISTRIBUIDOR"));
/*3*/                    vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*4*/                    vecColumnas.add((rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION"));
/*5*/                    vecColumnas.add((rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC"));
/*6*/                    vecColumnas.add((rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO"));
/*7*/                    vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*8*/                    vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*9*/                    vecColumnas.add((rs.getString("FN_MONTO")==null)?"":rs.getString("FN_MONTO"));
/*10*/                   vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*11*/                   vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*12*/                   vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/                   vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO"));
/*14*/                   vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/                   vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*16*/                   vecColumnas.add((rs.getString("TIPO_COBRO_INTERES")==null)?"":rs.getString("TIPO_COBRO_INTERES"));
/*17*/                   vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*18*/                   vecColumnas.add((rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO"));
/*19*/                   vecColumnas.add((rs.getString("IC_IF")==null)?"":rs.getString("IC_IF"));
/*20*/                   vecColumnas.add((rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF"));
/*21*/                   vecColumnas.add((rs.getString("TIPO_LINEA_CREDITO")==null)?"":rs.getString("TIPO_LINEA_CREDITO"));
/*22*/                   vecColumnas.add((rs.getString("DF_FECHA_SELECCION")==null)?"":rs.getString("DF_FECHA_SELECCION"));
/*23*/                   vecColumnas.add((rs.getString("MONTO_INTERES")==null)?"":rs.getString("MONTO_INTERES"));
/*24*/                   vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*25*/                   vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*26*/                   vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*27*/                   vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*28*/                   vecColumnas.add((rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA"));
/*29*/                   vecColumnas.add((rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO"));
/*30*/                   vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*31*/                   vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*32*/                   vecColumnas.add((rs.getString("FN_VALOR_TASA")==null)?"":rs.getString("FN_VALOR_TASA"));
                         vecFilas.add(vecColumnas);
                }
                con.cierraStatement();
        }catch(SQLException sqle){
                System.out.println(qrySentencia);
                System.out.println("SQLException "+sqle);
                throw new NafinException("SIST0001");
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}




public Vector getDatosArchivoSirac(String in)
	throws NafinException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();
                qrySentencia ="   SELECT P.in_numero_sirac"   +
				"  	  , P.cg_razon_social"   +
				"  	  , P.cg_rfc"   +
				"  	  , (N3.ic_nafin_electronico||' / '||I.cg_razon_social) IFB"   +
				"  	  , (DOM.ic_estado||' / '||ES.cd_nombre) ESTADO"   +
				"  	  , DOM.cg_municipio"   +
				"  	  , (DOM.cg_calle||' Ext. '||DOM.cg_numero_ext ||' Int. '|| DOM.cg_numero_int) DOMICILIO"   +
				"  	  , DOM.cg_colonia"   +
				"  	  , DOM.cn_cp"   +
				"  	  , TO_CHAR(P.df_constitucion, 'DD/MM/YYYY') FECHA_CONST"   +
				"  	  , (P.ic_estrato||' / '||EST.cd_nombre) ESTRATO"   +
				"  	  , P.fn_ventas_net_tot"   +
				"  	  , P.fn_ventas_net_exp"   +
				"  	  , (P.ic_sector_econ||' / '||SEC.cd_nombre) SECTOR"   +
				"  	  , P.cg_productos"   +
				"  	  , N1.ic_nafin_electronico NE_EPO"   +
				"  	  , N2.ic_nafin_electronico NE_PYME"   +
				"  	  , D.ig_numero_docto"   +
				"  	  , (DS.FN_VALOR_TASA) TNUF"   +
				"  	  , (S.ic_oficina||' / '||OES.cd_descripcion) OFICINA"   +
				"  	  , (A3.ic_usuario)  FUNCIONARIO"   +
				"  	  , E.cg_razon_social EMISOR"   +
				"  	  , (D.ic_moneda ||' / '||MON.cd_nombre) MONEDA"   +
				"  	  , (DS.fn_importe_recibir)	   IMPORTE_CREDITO"   +
				"  	  , (D.IC_PRODUCTO_NAFIN ||' / '||PN.ic_nombre) TIPO_PRODUCTO"   +
				"  	  , decode(PEP.cg_tipo_credito,'D','1 / DM','C','2 / CCC') TIPO_CREDITO"   +
				"     	  , (DS.ic_tasa ||' / '|| TA2.cd_nombre) TIPO_TASA"   +
				"  	  , D.ig_plazo_credito"   +
				"  	  , TO_CHAR(D.df_fecha_venc_credito,'dd/mm/yyyy') FECHA_VENCIMIENTO"   +
				"  	  , P1.cd_descripcion FPAGCAP"   +
				"  	  , P2.cd_descripcion FPAGINT"   +
				"  	  , TO_CHAR(S.df_ppc, 'DD/MM/YYYY') FECHAPPC"   +
				"  	  , TO_CHAR(S.df_ppi, 'DD/MM/YYYY') FECHAPPI"   +
				"  	  , (S.ic_tasaif ||' / '||TA.cd_nombre) TASAIF"   +
				"  	  , S.cg_rmif RMIF"   +
				"  	  , S.in_stif  STIF"   +
				"  	  , (S.ic_tasaif ||' / '|| TA.cd_nombre) TASAUF"   +
				"  	  , DS.cg_rel_mat as RMUF"   +
				"  	  , DS.fn_puntos as STUF"   +
				"  	  , (S.ic_tipo_credito||' / '||TC.cd_descripcion) DESTINO_CREDITO"   +
				"  	  , (S.ic_tabla_amort ||' / '|| TAM.cd_descripcion) TABLA_AMORT"   +
				"     FROM COMREL_NAFIN N1 ,"   +
				"          DIS_SOLICITUD S,"   +
				"          COMCAT_ESQUEMA_AMORT EA,"   +
				"          COMCAT_TIPO_CREDITO TC ,"   +
				"          COMCAT_TABLA_AMORT TAM,"   +
				"          COMCAT_ESTATUS_SOLIC ESS ,"   +
				"          COMCAT_TASA TA,"   +
				"          COMCAT_TASA TA2,"   +
				"          COMCAT_OFICINA_ESTATAL OES,"   +
				"          COM_ACUSE3 A3 ,"   +
				"          DIS_DOCTO_SELECCIONADO DS,"   +
				"          COMCAT_IF I,"   +
				//"          SEG_ USUARIO U ,"   +
				"          DIS_DOCUMENTO D,"   +
				"          COMCAT_MONEDA MON,"   +
				"          COMCAT_PYME P ,"   +
				"          COMCAT_ESTRATO EST,"   +
				"          COMCAT_SECTOR_ECON SEC,"   +
				"          COM_DOMICILIO DOM ,"   +
				"          COMCAT_ESTADO ES,"   +
				"          COMREL_NAFIN N2,"   +
				"          COMREL_NAFIN N3,"   +
				"          (SELECT IC_LINEA_CREDITO_DM AS IC_LINEA_CREDITO,IC_IF,'D' AS CG_TIPO_CREDITO FROM DIS_LINEA_CREDITO_DM WHERE IC_PRODUCTO_NAFIN = 4 AND CG_TIPO_SOLICITUD IN('I','R')"   +
				" 		 UNION ALL"   +
				" 		 SELECT IC_LINEA_CREDITO,IC_IF, 'C' AS CG_TIPO_CREDITO FROM COM_LINEA_CREDITO WHERE IC_PRODUCTO_NAFIN = 4 AND CG_TIPO_SOLICITUD IN('I','R')) LC,"   +
				"  	  COMCAT_EPO E,"   +
				"  	  COMCAT_PRODUCTO_NAFIN PN,"   +
				"  	  COMREL_PYME_EPO_X_PRODUCTO PEP,"   +
				"  	  COMCAT_PERIODICIDAD P1,"   +
				"  	  COMCAT_PERIODICIDAD P2"   +
				"    WHERE D.ic_documento = DS.ic_documento"   +
				"      	AND DS.ic_documento = S.ic_documento"   +
				"      	AND D.ic_pyme = P.ic_pyme"   +
				"     	AND P.ic_pyme = DOM.ic_pyme"   +
				"  	AND D.IC_EPO = E.IC_EPO"   +
				"  	AND D.IC_EPO = PEP.IC_EPO"   +
				"  	AND D.IC_PYME = PEP.IC_PYME"   +
				"  	AND D.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"   +
				" 	AND PN.ic_producto_nafin = D.ic_producto_nafin"   +
				"      	AND UPPER(DOM.cs_fiscal) = 'S'"   +
				"      	AND DOM.ic_estado = ES.ic_estado"   +
				"      	AND P.ic_estrato = EST.ic_estrato (+)"   +
				"      	AND P.ic_sector_econ = SEC.ic_sector_econ (+)"   +
				"      	AND D.ic_moneda = MON.ic_moneda"   +
				"      	AND S.ic_estatus_solic = ESS.ic_estatus_solic"   +
				"     	AND S.ic_tabla_amort = TAM.ic_tabla_amort"   +
				"      	AND LC.ic_linea_credito = DECODE(PEP.CG_TIPO_CREDITO,'D',D.ic_linea_credito_dm,'C',D.IC_LINEA_CREDITO)"   +
				"	AND LC.CG_TIPO_CREDITO = PEP.CG_TIPO_CREDITO"   +
				"      	AND LC.ic_if = I.ic_if"   +
				"      	AND S.ic_tipo_credito = TC.ic_tipo_credito"   +
				"      	AND S.ic_esquema_amort = EA.ic_esquema_amort"   +
				"      	AND (D.ic_epo = N1.ic_epo_pyme_if AND UPPER(N1.cg_tipo) = 'E')"   +
				"      	AND (D.ic_pyme = N2.ic_epo_pyme_if AND UPPER(N2.cg_tipo) = 'P')"   +
				"      	AND (LC.ic_if = N3.ic_epo_pyme_if AND UPPER(N3.cg_tipo) = 'I')"   +
				"      	AND S.ic_oficina = OES.ic_oficina"   +
				"      	AND S.cc_acuse = A3.cc_acuse"   +
				//"      	AND A3.ic_usuario = U.ic_usuario"   +
				"      	AND S.ic_tasaif = TA.ic_tasa"   +
				"  	AND DS.ic_tasa = TA2.ic_tasa"   +
				"  	AND S.ic_periodicidad_c = P1.ic_periodicidad"   +
				"  	AND S.ic_periodicidad_i = P2.ic_periodicidad" +
				"     	AND S.ic_documento IN("+in+")"  ;
                rs = con.queryDB(qrySentencia);
                while(rs.next()){

                 vecColumnas = new Vector();
/*0*/             vecColumnas.add((rs.getString("in_numero_sirac")==null)?"":rs.getString("in_numero_sirac"));
/*1*/             vecColumnas.add((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
/*2*/             vecColumnas.add((rs.getString("cg_rfc")==null)?"":rs.getString("cg_rfc"));
/*3*/             vecColumnas.add((rs.getString("IFB")==null)?"":rs.getString("IFB"));
/*4*/             vecColumnas.add((rs.getString("ESTADO")==null)?"":rs.getString("ESTADO"));
/*5*/             vecColumnas.add((rs.getString("cg_municipio")==null)?"":rs.getString("cg_municipio"));
/*6*/             vecColumnas.add((rs.getString("DOMICILIO")==null)?"":rs.getString("DOMICILIO"));
/*7*/             vecColumnas.add((rs.getString("cg_colonia")==null)?"":rs.getString("cg_colonia"));
/*8*/             vecColumnas.add((rs.getString("cn_cp")==null)?"":rs.getString("cn_cp"));
/*9*/             vecColumnas.add((rs.getString("FECHA_CONST")==null)?"":rs.getString("FECHA_CONST"));
/*10*/            vecColumnas.add((rs.getString("ESTRATO")==null)?"":rs.getString("ESTRATO"));
/*11*/            vecColumnas.add((rs.getString("fn_ventas_net_tot")==null)?"":rs.getString("fn_ventas_net_tot"));
/*12*/            vecColumnas.add((rs.getString("fn_ventas_net_exp")==null)?"":rs.getString("fn_ventas_net_exp"));
/*13*/            vecColumnas.add((rs.getString("SECTOR")==null)?"":rs.getString("SECTOR"));
/*14*/            vecColumnas.add((rs.getString("cg_productos")==null)?"":rs.getString("cg_productos"));
/*15*/            vecColumnas.add((rs.getString("NE_EPO")==null)?"":rs.getString("NE_EPO"));
/*16*/            vecColumnas.add((rs.getString("NE_PYME")==null)?"":rs.getString("NE_PYME"));
/*17*/            vecColumnas.add((rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto"));
/*18*/            vecColumnas.add((rs.getString("TNUF")==null)?"":rs.getString("TNUF"));
/*19*/            vecColumnas.add((rs.getString("OFICINA")==null)?"":rs.getString("OFICINA"));
/*20*/            vecColumnas.add((rs.getString("FUNCIONARIO")==null)?"":rs.getString("FUNCIONARIO"));
/*21*/            vecColumnas.add((rs.getString("EMISOR")==null)?"":rs.getString("EMISOR"));
/*22*/            vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*23*/            vecColumnas.add((rs.getString("IMPORTE_CREDITO")==null)?"":rs.getString("IMPORTE_CREDITO"));
/*24*/            vecColumnas.add((rs.getString("TIPO_PRODUCTO")==null)?"":rs.getString("TIPO_PRODUCTO"));
/*25*/            vecColumnas.add((rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO"));
/*26*/            vecColumnas.add((rs.getString("TIPO_TASA")==null)?"":rs.getString("TIPO_TASA"));
/*27*/            vecColumnas.add((rs.getString("ig_plazo_credito")==null)?"":rs.getString("ig_plazo_credito"));
/*28*/            vecColumnas.add((rs.getString("FECHA_VENCIMIENTO")==null)?"":rs.getString("FECHA_VENCIMIENTO"));
/*29*/            vecColumnas.add((rs.getString("FPAGCAP")==null)?"":rs.getString("FPAGCAP"));
/*30*/            vecColumnas.add((rs.getString("FPAGINT")==null)?"":rs.getString("FPAGINT"));
/*31*/            vecColumnas.add((rs.getString("FECHAPPC")==null)?"":rs.getString("FECHAPPC"));
/*32*/            vecColumnas.add((rs.getString("FECHAPPI")==null)?"":rs.getString("FECHAPPI"));
/*33*/            vecColumnas.add((rs.getString("TASAIF")==null)?"":rs.getString("TASAIF"));
/*34*/            vecColumnas.add((rs.getString("RMIF")==null)?"":rs.getString("RMIF"));
/*35*/            vecColumnas.add((rs.getString("STIF")==null)?"":rs.getString("STIF"));
/*36*/            vecColumnas.add((rs.getString("TASAUF")==null)?"":rs.getString("TASAUF"));
/*37*/            vecColumnas.add((rs.getString("RMUF")==null)?"":rs.getString("RMUF"));
/*38*/            vecColumnas.add((rs.getString("STUF")==null)?"":rs.getString("STUF"));
/*39*/            vecColumnas.add((rs.getString("DESTINO_CREDITO")==null)?"":rs.getString("DESTINO_CREDITO"));
/*40*/            vecColumnas.add((rs.getString("TABLA_AMORT")==null)?"":rs.getString("TABLA_AMORT"));
		vecFilas.add(vecColumnas);
                }
                con.cierraStatement();
        }catch(SQLException sqle){
                System.out.println(qrySentencia);
                System.out.println("SQLException "+sqle);
                throw new NafinException("SIST0001");
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}

public void generaReporteSirac(String in)
       throws NafinException{

   	AccesoDB    con          = null;
        String      qrySentencia = "";
        boolean     ok           = true;
   	try{
        	con = new AccesoDB();
                con.conexionDB();
                qrySentencia  =  " update dis_solicitud " +
                                 " set ic_estatus_solic = 2 " +
                                 " ,df_operacion = sysdate "+
                                 " where ic_documento in("+in+") " +
                                 " and ic_estatus_solic = 1";
                con.ejecutaSQL(qrySentencia);
        }catch(SQLException sqle){
                ok = false;
		System.out.println(qrySentencia);
		System.out.println("AutorizacionSolicBean::GenerareporteSirac SQLExcepcion: "+sqle);
                throw new NafinException("SIST0001");
        }catch(Exception e){
                ok = false;
		System.out.println("AutorizacionSolicBean::GenerareporteSirac Excepcion: "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                        con.terminaTransaccion(ok);
                 	con.cierraConexionDB();
                }
        }
}

public String getCuentaAutorizada(String ic_epo,String ic_if,String ic_pyme,String ic_moneda)
	throws NafinException {
   	AccesoDB con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	String qrySentencia = "";
	String cg_numero_cuenta = "";

   	try{
		con = new AccesoDB();
		con.conexionDB();
		qrySentencia =
			" SELECT cg_numero_cuenta"   +
			"  FROM comrel_cuenta_bancaria_x_prod cbp, com_cuenta_aut_x_prod cap "   +
			" WHERE cbp.ic_cuenta_bancaria = cap.ic_cuenta_bancaria "   +
			" AND cbp.ic_epo = ? "   +
			" AND cbp.ic_pyme = ? "   +
			" AND cbp.ic_if = ? "   +
			" AND ic_moneda = ? "  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1, Integer.parseInt(ic_epo));
		ps.setInt(2, Integer.parseInt(ic_pyme));
		ps.setInt(3, Integer.parseInt(ic_if));
		ps.setInt(4, Integer.parseInt(ic_moneda));

		rs = ps.executeQuery();
		if(rs.next())
			cg_numero_cuenta = rs.getString(1)==null?"":rs.getString(1);

		ps.clearParameters();
		rs.close();
		if(ps != null) ps.close();


	}catch(Exception e){
		System.out.println("Excepcion "+e);
		throw new NafinException("SIST0001");
	}finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
 return cg_numero_cuenta;
}
public Vector getDatosCuentaAutorizada(String ic_epo,String ic_if,String ic_pyme,String ic_moneda)
	throws NafinException {
   	AccesoDB con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	String qrySentencia = "";
	Vector	vecFilas    = new Vector();
	Vector	vecColumnas = null;
   	try{
		con = new AccesoDB();
		con.conexionDB();
		qrySentencia =
			" SELECT pym.cg_razon_social AS nompyme,"   +
			"     crn.IC_NAFIN_ELECTRONICO AS nepyme,"   +
			"     pym.cg_rfc AS pymerfc,"   +
			"    cbp.cg_numero_cuenta AS nocuenta,"   +
			"    cbp.cg_sucursal AS sucursal,"   +
			"    decode (plz.cd_descripcion, NULL, ' ', plz.cd_descripcion||', '||plz.cg_estado) AS plaza "   +
			" FROM comrel_cuenta_bancaria_x_prod cbp, "   +
			"     comcat_pyme pym, comcat_plaza plz,comrel_nafin crn "   +
			" WHERE cbp.ic_pyme = pym.ic_pyme "   +
			" AND cbp.ic_plaza = plz.ic_plaza "   +
			" AND cbp.ic_pyme = crn.IC_EPO_PYME_IF "   +
			" AND crn.CG_TIPO = 'P' "   +
			" AND cbp.ic_epo = ? "   +
			" AND cbp.ic_pyme = ? "   +
			" AND cbp.ic_if = ? "   +
			" AND ic_moneda = ? "  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1, Integer.parseInt(ic_epo));
		ps.setInt(2, Integer.parseInt(ic_pyme));
		ps.setInt(3, Integer.parseInt(ic_if));
		ps.setInt(4, Integer.parseInt(ic_moneda));
		rs = ps.executeQuery();
		if(rs.next()) {
			vecColumnas = new Vector();
/*0*/		vecColumnas.add((rs.getString("nompyme")==null)?"":rs.getString("nompyme"));
/*1*/		vecColumnas.add((rs.getString("nepyme")==null)?"":rs.getString("nepyme"));
/*2*/		vecColumnas.add((rs.getString("pymerfc")==null)?"":rs.getString("pymerfc"));
/*3*/		vecColumnas.add((rs.getString("nocuenta")==null)?"":rs.getString("nocuenta"));
/*4*/		vecColumnas.add((rs.getString("sucursal")==null)?"":rs.getString("sucursal"));
/*5*/		vecColumnas.add((rs.getString("plaza")==null)?"":rs.getString("plaza"));
			vecFilas.add(vecColumnas);
		}
		ps.clearParameters();
		rs.close();
		if(ps != null) ps.close();
	}catch(Exception e){
		System.out.println("Excepcion "+e);
		throw new NafinException("SIST0001");
	}finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
	}
 return vecFilas;
}
/**
   *
   * @throws com.netro.exception.NafinException
   * @return
   * @param fecha_seleccion_a
   * @param fecha_seleccion_de
   * @param ic_documento
   * @param ic_if
   * @param ic_pyme
   * @param ic_epo
   */
public Vector getDoctosSelCCCVariableFijos(String ic_epo,String ic_pyme,String ic_if,String ic_documento,String fecha_seleccion_de,String fecha_seleccion_a)

	throws NafinException{
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas    = new Vector();
        Vector	vecColumnas = null;
        System.out.println("getDoctosSelCCCVariableFijos" );
   	try{
        	con = new AccesoDB();
                con.conexionDB();

qrySentencia = " SELECT  d.ic_documento AS ic_documento, "+
         " p.ic_pyme AS ic_pyme,"+
         " p.cg_razon_social AS cg_razon_social, "+
         " e.ic_epo as ic_epo,"+
         " e.cg_razon_social AS epo, "+
         " TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS df_fecha_seleccion, "+
         " d.ic_moneda AS ic_moneda, "+
         " m.cd_nombre AS nombremoneda,  "+
         " ds.fn_importe_recibir as fn_importe_recibir,"+
         " d.fn_monto AS monto, "+
         " d.ig_plazo_credito AS ig_plazo_credito, "+
         " TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS df_fecha_venc_credito, "+
         " ds.fn_importe_interes AS monto_interes,  "+
         " ct.ic_tasa as ic_tasa, "+
         " ds.fn_valor_tasa AS fn_valor_tasa,          "+
         " DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', 'Sin conversion','P', 'Peso-Dolar','') AS tipo_conversion, "+
         " ds.fn_importe_recibir AS fn_importe_recibir,  "+
         " DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, "+
         //" ct.cd_nombre AS referencia_int,            "+
			" d.ct_referencia as ct_referencia,	"+
         " cg_campo1 AS campo1, cg_campo2 AS campo2, cg_campo3 AS campo3, cg_campo4 AS campo4,  cg_campo5 AS campo5 "+
         " ,d.IG_NUMERO_DOCTO  as IG_NUMERO_DOCTO "+
			" , lc.cg_numero_cuenta  as cg_numero_cuenta	 "+
			" ,TCI.ic_tipo_cobro_interes"   +
			" ,TCI.cd_descripcion as tipo_cobro_interes"+
			" ,ct.cd_nombre||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT		 "   +
			" ,NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes "+
			" ,NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as tipo_cobranza "+
			" ,cpe.cg_num_distribuidor as numdist "+
			" , d.ig_tipo_pago AS TIPOPAGO  "+
					
         " FROM dis_documento d, "+
         " comcat_pyme p, "+
			" comrel_pyme_epo cpe, "+
         " comrel_producto_epo pe, "+
         " comcat_producto_nafin pn, "+
         " comcat_moneda m, "+
         " com_tipo_cambio tc, "+
         " com_linea_credito lc, "+
         " comcat_tipo_cobro_interes tci, "+
         " comrel_if_epo_x_producto iep, "+
         " comcat_tasa ct, "+
         " comcat_epo e, "+
         " dis_docto_seleccionado ds "+
         " WHERE d.ic_epo = pe.ic_epo "+
         " AND pe.ic_producto_nafin = pn.ic_producto_nafin "+
         " AND pn.ic_producto_nafin = 4 "+
         " AND p.ic_pyme = d.ic_pyme  "+
         " AND d.ic_documento = ds.ic_documento "+
			" and cpe.ic_epo = d.ic_epo "+
			" and cpe.ic_pyme = d.ic_pyme "+
			" AND d.ic_epo = pe.ic_epo "+
			" AND lc.ic_moneda = m.ic_moneda "+
			" AND d.ic_moneda = tc.ic_moneda "+
			" AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
			" FROM com_tipo_cambio "+
			" WHERE ic_moneda = m.ic_moneda) "+
			" AND d.ic_linea_credito = lc.ic_linea_credito "+
			" AND lc.ic_if = iep.ic_if "+
			" AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
			" AND d.ic_estatus_docto = 3 "+
			" AND iep.ic_epo = d.ic_epo "+
			" AND iep.ic_producto_nafin = pn.ic_producto_nafin "+
			" AND ds.ic_tasa = ct.ic_tasa "+
			" AND d.ic_epo = e.ic_epo "+
			" AND lc.ic_estatus_linea = 12 "+
		  " AND d.ic_documento NOT IN (SELECT ic_documento FROM dis_solicitud) ";


              if (!ic_epo.equals("")) {
                  qrySentencia +=" AND d.ic_epo = "+ic_epo;
              }
              if (!ic_if.equals("")) {
                  qrySentencia +=" AND lc.ic_if = "+ic_if;
              }
              if (!fecha_seleccion_de.equals("") && !fecha_seleccion_a.equals("")) {

              qrySentencia +=" AND TRUNC (ds.df_fecha_seleccion) BETWEEN TRUNC (TO_DATE ('"+fecha_seleccion_de+"','DD/MM/YYYY')) "+
                              " AND TRUNC (TO_DATE ('"+fecha_seleccion_a+"','DD/MM/YYYY')) ";
              }

              if (!ic_pyme.equals("")) {
                 qrySentencia +="AND d.ic_pyme = "+ic_pyme;

              }

              if (!ic_documento.equals("")) {
                 qrySentencia +="AND d.ic_documento = "+ic_documento;

              }


              qrySentencia +=" ORDER BY d.ig_numero_docto ";


               rs = con.queryDB(qrySentencia);
                System.out.println("qrySentencia "+qrySentencia);
                while(rs.next()){
                	  vecColumnas = new Vector();
/*0*/                     vecColumnas.add((rs.getString("ic_documento")==null)?"":rs.getString("ic_documento"));
/*1*/                     vecColumnas.add((rs.getString("ic_pyme")==null)?"":rs.getString("ic_pyme"));
/*2*/                     vecColumnas.add((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
/*3*/                     vecColumnas.add((rs.getString("ic_epo")==null)?"":rs.getString("ic_epo"));
/*4*/                     vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*5*/                     vecColumnas.add((rs.getString("df_fecha_seleccion")==null)?"":rs.getString("df_fecha_seleccion"));
/*6*/                     vecColumnas.add((rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda"));
/*7*/                     vecColumnas.add((rs.getString("nombremoneda")==null)?"":rs.getString("nombremoneda"));
/*8*/                     vecColumnas.add((rs.getString("fn_importe_recibir")==null)?"":rs.getString("fn_importe_recibir"));
/*9*/                     vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*10*/                     vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*11*/                     vecColumnas.add((rs.getString("MONTO_INTERES")==null)?"":rs.getString("MONTO_INTERES"));
/*12*/                     vecColumnas.add((rs.getString("ic_tasa")==null)?"":rs.getString("ic_tasa"));
/*13*/                     vecColumnas.add((rs.getString("fn_valor_tasa")==null)?"":rs.getString("fn_valor_tasa"));
/*14*/                     vecColumnas.add((rs.getString("tipo_conversion")==null)?"":rs.getString("tipo_conversion"));
/*15*/                     vecColumnas.add((rs.getString("monto")==null)?"":rs.getString("monto"));
/*16*/                     vecColumnas.add((rs.getString("tipo_cambio")==null)?"":rs.getString("tipo_cambio"));
/*17*/                     vecColumnas.add((rs.getString("resp_interes")==null)?"":rs.getString("resp_interes"));
/*18*/                     vecColumnas.add((rs.getString("referencia_int")==null)?"":rs.getString("referencia_int"));
/*29*/                     vecColumnas.add((rs.getString("campo1")==null)?"":rs.getString("campo1"));
/*20*/                     vecColumnas.add((rs.getString("campo2")==null)?"":rs.getString("campo2"));
/*21*/                     vecColumnas.add((rs.getString("campo3")==null)?"":rs.getString("campo3"));
/*22*/                     vecColumnas.add((rs.getString("campo4")==null)?"":rs.getString("campo4"));
/*23*/                     vecColumnas.add((rs.getString("campo5")==null)?"":rs.getString("campo5"));
/*24*/                     vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*25*/                     vecColumnas.add((rs.getString("cg_numero_cuenta")==null)?"":rs.getString("cg_numero_cuenta"));
/*26*/                     vecColumnas.add((rs.getString("ic_tipo_cobro_interes")==null)?"":rs.getString("ic_tipo_cobro_interes"));
/*27*/                     vecColumnas.add((rs.getString("tipo_cobro_interes")==null)?"":rs.getString("tipo_cobro_interes"));
/*28*/                     vecColumnas.add((rs.getString("resp_interes")==null)?"":rs.getString("resp_interes"));
/*29*/                     vecColumnas.add((rs.getString("tipo_cobranza")==null)?"":rs.getString("tipo_cobranza"));
/*30*/                     vecColumnas.add((rs.getString("ct_referencia")==null)?"":rs.getString("ct_referencia"));
/*31*/							vecColumnas.add((rs.getString("numdist")==null)?"":rs.getString("numdist"));
/*32*/                     vecColumnas.add((rs.getString("TIPOPAGO")==null)?"":rs.getString("TIPOPAGO"));
 




                          vecFilas.add(vecColumnas);
                }
                System.out.println("vecFilas "+vecFilas);
                con.cierraStatement();

        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
 return vecFilas;
}


/**
	 * Obtiene Los documentos Seleccionados Pyme, para su operaci�n de
	 * Descuento.
	 *
	 * El m�todo est� pensado para ser invocado via Web Service DescuentoIFWS.
	 *
	 * Al momento de regresar los documentos que se encuentran seleccionados
	 * por la Pyme, estos se pasan autom�ticamente a estatus 24 (En proceso
	 * de Autorizacion IF)
	 *
	 * Los resultados de este m�todo, dependen de la parametrizacion del IF
	 * "TIPO_MONEDA" en la Base de Datos. Si este es "A" Regresa MN y USD.
	 * Si es "MN" solo regresa los documentos de Moneda Nacional (ic_moneda = 1)
	 * y si es "DA" solo regresa Dolares Americanos (ic_moneda = 54)
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 * 		del proceso mas los objetos DocumentoIFWS con los datos de los
	 *    documentos seleccionados
	 */
	public ProcesoIFDistWSInfo getDoctosSelecPymeDisWS(String claveUsuario,
			String password, String claveIF) {
		log.info("getDoctosSelecPymeDisWS(E)");

		ProcesoIFDistWSInfo resultadoProceso = new ProcesoIFDistWSInfo();
		List lDocumentos = new ArrayList();
		String nombreIF  ="";

		StringBuffer erroresInesperados = new StringBuffer();  //para ir concatenando los errores Inesperadoa

		//Los ERRORES INESPERADOS se relanzan para que el Catch m�s externo
		//sea el encargado de considerarlos. Agregando al resumen de ejecucion
		//ERROR INESPERADO|
		//Los errores inesperados incluyen por ejemplo a las fallas de
		// BD, de conexion al OID, etc...

		/*
				String icMoneda			=	"1";
				double monto				=	0.0;
				double tipoCambio			=	0.0;
				double montoValuado		=	0.0;
				double montoInteres 		=	0.0;
				String monedaLinea		= 	"1";

				int totalDoctosMN 		= 0;
				double totalMontoMN 		= 0.0;
				double totalMontoValuadoMN = 0.0;
				double totalMontoIntMN		= 0.0;

				int totalDoctosUSD		= 0;
				double totalMontoUSD 	= 0.0;
				double totalMontoValuadoUSD = 0.0;
				double totalMontoIntUSD	= 0.0;

				int totalDoctosConv		= 0;
				double totalMontoConv	= 0.0;
				double totalMontoConvMN = 0.0;
				double totalMontoIntConv = 0.0;
				*/

		int iClaveIF = 0;
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveIF == null || claveIF.equals("") ) {
				throw new Exception("Los parametros son requeridos");
			}

			iClaveIF = Integer.parseInt(claveIF);

		} catch(Exception e) {
			log.error("getDoctosSelecPymeDisWS(Error)(" + claveIF + "_" + claveUsuario + ") ", e);
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password + "," + claveIF + ")" + "Error=" + e.getMessage());
			return resultadoProceso;
		}
		//****************************************************************************************


		AccesoDB con = null;
		boolean updateOk = true;
		try {
			try {
				con = new AccesoDB();
			} catch(Throwable t) {
				throw new Exception("Error al generar el objeto de acceso a BD", t);
			}
			UtilUsr utilUsr = new UtilUsr();
			List cuentasIF = null;
			try {
				cuentasIF = utilUsr.getUsuariosxAfiliado(claveIF, "I");
			} catch(Throwable t) {
				throw new Exception("Error al obtener la lista de usuarios del IF: " + claveIF + ".", t);
			}

			log.debug("getDoctosSelecPymeDisWS(" + claveIF + "_" + claveUsuario + ")::Validando claveIF/usuario");

			if (!cuentasIF.contains(claveUsuario)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("ERROR|El usuario " + claveUsuario + " no existe para el IF especificado");
				return resultadoProceso;
			}

			log.debug("getDoctosSelecPymeDisWS(" + claveIF + "_" + claveUsuario + ")::Validando usuario/passwd");
			try {
				if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
					resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
					resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del usuario " + claveUsuario);
					return resultadoProceso;
				}
			} catch(Throwable t) {
				throw new Exception("Error en el componente de validacion de usuario. ", t);
			}

			try {
				con.conexionDB();
			} catch(Throwable t) {
				throw new Exception ("Error al realizar la conexion a la BD. ", t);
			}

			PreparedStatement psI = null;
			try {

				ResultSet rsI = null;
				String strSQLIF = " select cg_razon_social as NOMBREIF  from comcat_if  where  ic_if =  ?";
				psI = con.queryPrecompilado(strSQLIF);
				psI.setInt(1, iClaveIF);
				rsI = psI.executeQuery();
				if(rsI.next()) {
					nombreIF = rsI.getString("NOMBREIF");
				}
				rsI.close();
				psI.close();

				//DESCUENTO MERCANTIL
				List lDocumentosDM = this.getDoctosSeleccionadosWSDM(claveIF, con);

				//CREDITO EN CUENTA CORRIENTE
				List lDocumentosCCC = this.getDoctosSeleccionadosWSCCC(claveIF, con);

				//Se genera arreglo de todos los tipo de credito
				for(int i=0; i<lDocumentosDM.size(); i++){
					lDocumentos.add((DocumentoDistIFWS)lDocumentosDM.get(i));
				}
				for(int i=0; i<lDocumentosCCC.size(); i++){
					lDocumentos.add((DocumentoDistIFWS)lDocumentosCCC.get(i));
				}

				updateOk = true;

				//insertaAcuse(acuse.toString(),String.valueOf(totalMontoMN), String.valueOf(totalMontoIntMN), String.valueOf(totalMontoValuadoMN),
				//				 String.valueOf(totalMontoUSD), String.valueOf(totalMontoIntUSD), String.valueOf(totalMontoValuadoUSD),"","",con);

			} catch(Throwable t) {
				throw t;
			}
			//*********************************************
			//throw new Exception("Error al precompilar la consulta de documentos. ", t);
			//throw new Exception("Error al precompilar la actualizacion de documentos.", t);
			//throw new Exception("Error al obtener/actualizar los documentos.", t);
			//Notificaci�n de sobregiro de lineas.
			//StringBuffer mensajeCorreo = new StringBuffer();


			DocumentoDistIFWS[] arrDocumentos = null;
			if (lDocumentos.size() > 0) {
				arrDocumentos = new DocumentoDistIFWS[lDocumentos.size()];
				lDocumentos.toArray(arrDocumentos);
			}
			log.info("getDoctosSelecPymeDisWS(" + claveIF + "_" + claveUsuario + "):: " + lDocumentos.size() + " Documentos a regresar");
			resultadoProceso.setDocumentos(arrDocumentos);
			resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito
		} catch( Throwable  t) { //en esta secci�n caen todos los errores inesperados
			updateOk = false;
			resultadoProceso.setCodigoEjecucion(new Integer(2));	//2 = Error
			resultadoProceso.setResumenEjecucion("ERROR INESPERADO|" + t.getMessage());
			log.error("getDoctosSelecPymeDisWS(Error)", t);

			erroresInesperados.append(resultadoProceso.getResumenEjecucion()+"\r\n");
			log.error("getDoctosSelecPymeDisWS(Error)(" + claveIF + "_" + claveUsuario + ")::Hubo Errores Inesperados "+ erroresInesperados.toString());

					/* llamado del metodo para enviar correo de Errores Inesperados siempre y cuando hayan*/
			try {

				if(!erroresInesperados.toString().equals("")){
					erroresInesperados.append("\nError causado al procesar: Clave IF " +
							claveIF + ", Usuario: " + claveUsuario);
					enviodeCorreoWS(erroresInesperados, "S");
				}
			} catch(Throwable e) {
				//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
				log.error("Error al Enviar el correo de Errores Inesperados. ");
			}

			return resultadoProceso;
		} finally {
			log.info("getDoctosSelecPymeDisWS(S)");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(updateOk);
				con.cierraConexionDB();
			}
		}


		return resultadoProceso;
	}



	private List getDoctosSeleccionadosWSDM(String iClaveIF, AccesoDB con)
	throws NafinException{
		//AccesoDB con = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
      ResultSet rs = null;
		ResultSet rs2 = null;
      String qrySentencia = "";
		List lDocumentos = new ArrayList();
   	try{


			// OBTENGO LAS EPOS Y  VALIDO SI TIENEN  LIMITE SOBREGIRADO
				String strSQLEPO =
						"SELECT DISTINCT e.ic_epo AS clave, e.cg_razon_social AS descripcion " +
						"           FROM comcat_epo e, " +
						"                comrel_if_epo ie, " +
						"                comrel_if_epo_x_producto iep, " +
						"                comrel_producto_epo pre, " +
						"                comcat_producto_nafin pn, " +
						"                dis_documento d " +
						"          WHERE ie.ic_epo = e.ic_epo " +
						"            AND e.ic_epo = pre.ic_epo " +
						"            AND pre.ic_producto_nafin = pn.ic_producto_nafin " +
						"            AND ie.ic_if = iep.ic_if " +
						"            AND ie.ic_epo = iep.ic_epo " +
						"            AND d.ic_epo = e.ic_epo " +
						"            AND e.cs_habilitado = ? " +
						"            AND ie.cs_distribuidores IN (?, ?) " +
						"            AND iep.cs_habilitado = ? " +
						"            AND iep.ic_producto_nafin = ? " +
						"            AND pn.ic_producto_nafin = ? " +
						"            AND ie.ic_if = ? " +
						"            AND NVL (pre.cg_tipos_credito, pn.cg_tipos_credito) IN (?, ?) " +
						"            AND d.ic_estatus_docto IN (?) " +
						"            AND d.ic_linea_credito_dm IS NOT NULL " +
						"            AND d.ic_linea_credito_dm IN (SELECT ic_linea_credito_dm " +
						"                                            FROM dis_linea_credito_dm " +
						"                                           WHERE ic_if = ?) " +
						"       ORDER BY 1 ";


				try{
					List lParams = new ArrayList();
					lParams.add("S");
					lParams.add("S");
					lParams.add("R");
					lParams.add("S");
					lParams.add(new Integer(4));
					lParams.add(new Integer(4));
					lParams.add(new Integer(iClaveIF));
					lParams.add("A");
					lParams.add("D");
					lParams.add(new Integer(3));//Estatus del Documento 3= Seleccionado Pyme
					lParams.add(new Integer(iClaveIF));


					log.debug("getDoctosSelecPymeDisWS(" + iClaveIF + "_:: strSQLEPO=" + strSQLEPO);
					log.debug("getDoctosSelecPymeDisWS(" + iClaveIF + "_:: strSQLEPO Params=" + lParams);


					ps = con.queryPrecompilado(strSQLEPO, lParams);
					rs = ps.executeQuery();
				} catch(Throwable t) {
					throw new NafinException("Error al realizar la consulta para obtener las EPOS DM ");
				}



				//Acuse acuse = new Acuse(3,"4","com",con);
				//acuseFormateado  = acuse.formatear();
				int iter = 0;
				String cveEpos = "";
				while (rs.next()) {
					cveEpos += iter==0?rs.getString("clave"):(","+rs.getString("clave"));
					iter++;
				}
				rs.close();
				ps.close();

				if(!"".equals(cveEpos)){
					qrySentencia = "  SELECT  d.ic_epo CLAVEEPO "   +
									"     ,E.cg_razon_social as NOMBREEPO "+
									"		,P.ic_pyme as CLAVEDISTRIBUIDOR "+
									" 		,P.cg_razon_social as NOMBREDISTRIBUIDOR "   +
									"     ,cpe.cg_num_distribuidor as NUMERODISTRIBUIDOR " +
									" 		,P.in_numero_sirac as NUMSIRACDISTRIBUIDOR "   +
									" 	   ,D.CC_ACUSE as ACUSECARGA "   +
									" 		,D.ig_numero_docto as NUMERODOCTOINI "   +
									" 	   ,TO_CHAR(D.DF_CARGA,'dd/mm/yyyy') as FECHAPUBLICACION"   +
									" 	   ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHAEMISION"   +
									" 	   ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHAVENCIINI"   +
									" 	   ,D.IG_PLAZO_DOCTO as PLAZODOCTOINI "   +
									" 	   ,D.fn_monto as MONTODOCTO "   +
									" 	   ,D.IG_PLAZO_DESCUENTO as PLAZODESCUENTO "   +
									//setPorcentajeDescuento
									"     ,nvl(D.fn_porc_descuento,0) as PORDESCUENTO"+
									" 	   ,TF.IC_TIPO_FINANCIAMIENTO AS CLAVEMODPLAZO "   +
									" 	   ,TF.CD_DESCRIPCION AS MODALIDADPLAZO "   +
									"     ,D.ic_documento as NUMDOCTOFIN " +
									"  	,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHAOPERACION "   +//setFechaOperacion
									"		,D.ic_moneda as CLAVEMONEDA "+
									" 		,M.cd_nombre as NOMBREMONEDA"   +
									" 		,D.IG_PLAZO_CREDITO as PLAZOCREDITO"   +
									" 		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as FECHAVENCICREDITO		"   +
									"     ,DS.fn_importe_interes as MONTOINTERES "+
									"		,DS.fn_valor_tasa as TASAINTERES "+
									" 		,TCI.ic_tipo_cobro_interes as CLAVETIPOINTERES "   +
									"     ,TCI.cd_descripcion as TIPOINTERES"+
									" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPOCONVERSION"   +
									" 		,D.fn_monto as MONTOCREDITO"   +
									" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPOCAMBIO "   +
									"     ,NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as RESPONPAGOINTERES "+
									"     ,NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as TIPOCOBRANZA "+
									" 		,CT.CD_NOMBRE  ||'' ||  ds.cg_rel_mat || ' ' ||ds.FN_PUNTOS AS REFERENCIA "   +
									"		,cg_campo1 as CAMPOADICIONAL1 " +
									"		,cg_campo2 as CAMPOADICIONAL2 " +
									"		,cg_campo3 as CAMPOADICIONAL3 " +
									"		,cg_campo4 as CAMPOADICIONAL4 " +
									"		,cg_campo5 as CAMPOADICIONAL5 " +
									" 		,DECODE(DS.CG_REL_MAT"+
									"               ,'+',DS.fn_valor_tasa + DS.fn_puntos"+
									"               ,'-',DS.fn_valor_tasa - DS.fn_puntos"+
									"               ,'*',DS.fn_valor_tasa * DS.fn_puntos"+
									"               ,'/',DS.fn_valor_tasa / DS.fn_puntos"+
									"               ,0) AS VALOR_TASA"   +
									"		,LCD.ic_moneda as MONEDA_LINEA"+
									" 		,DS.fn_importe_recibir as MONTORECIBIR"   +
									"		,ds.ic_tasa as CLAVETASA "+
									"     , NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as TIPOCONVERSION2 "+
									"     ,d.fn_tipo_cambio as TIPOCAMBIO2 "+
									"		,ct_referencia " +
								    "   ,  D.ic_estatus_docto  as ICESTATUS "+
								   "	, es.CD_DESCRIPCION  as NOMBRE_ESTATUS "+								   
                                                                       " ,p.CG_RFC  as rfcPyme " +  //QC-DLHC
                                                                       " , e.cg_rfc as rfcEPO  "+ //QC-DLHC
                                            
									"  FROM DIS_DOCUMENTO D"   +
									"    ,COMCAT_PYME P"   +
									"    ,COMREL_PRODUCTO_EPO PE"   +
									" 	  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
									"	  ,COMREL_PYME_EPO CPE "+
									"    ,COMCAT_PRODUCTO_NAFIN PN"   +
									"    ,COMCAT_MONEDA M"   +
									"    ,COM_TIPO_CAMBIO TC"   +
									"    ,DIS_LINEA_CREDITO_DM LCD"   +
									"    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
									"    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
									"    ,COMCAT_TASA CT"   +
									"    ,COMCAT_EPO E"+
									"    ,DIS_DOCTO_SELECCIONADO DS"   +
								   "   ,  COMCAT_ESTATUS_DOCTO es   "+		
								   
									"  WHERE D.IC_EPO = PE.IC_EPO"   +
									"    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
									"    AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO "   +
									"    AND PN.IC_PRODUCTO_NAFIN = 4"   +
									"    AND P.IC_PYME = D.IC_PYME"   +
									"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
									"    AND D.IC_EPO = PE.IC_EPO"   +
							"    AND cpe.ic_epo = d.ic_epo "+
							"    AND cpe.ic_pyme = d.ic_pyme "+
									"    AND LCD.IC_MONEDA = M.IC_MONEDA"   +
									"    AND D.IC_MONEDA = TC.IC_MONEDA"   +
									"    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = D.IC_MONEDA)"   +
									"    AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
									"    AND LCD.IC_IF = IEP.IC_IF"   +
									"    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
									"    AND D.ic_estatus_docto = 3"   +
									"    AND IEP.IC_EPO = D.IC_EPO"   +
									"    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
									"    AND DS.IC_TASA = CT.IC_TASA "  +
									"    AND D.IC_EPO = E.IC_EPO "+
									"    AND D.IC_EPO in("+cveEpos+") "+
									"    AND LCD.IC_IF = "+iClaveIF+
									"	 AND LCD.ic_estatus_linea = 12 "+
									"    AND D.ic_documento not in(select ic_documento from dis_solicitud)" +
									"    AND ES.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO   "+	
									"  ORDER BY d.ic_epo, D.IG_NUMERO_DOCTO"  ;
						System.out.println("\nqrySentencia: "+qrySentencia);
						//rs = con.queryDB(qrySentencia);
						ps = con.queryPrecompilado(qrySentencia);
						rs = ps.executeQuery();
						while(rs.next()){
							DocumentoDistIFWS documento = new DocumentoDistIFWS();
							documento.setClaveEpo((rs.getString("CLAVEEPO")==null)?"":rs.getString("CLAVEEPO"));
							documento.setNombreEpo((rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO"));
							documento.setClaveDistribuidor((rs.getString("CLAVEDISTRIBUIDOR")==null)?"":rs.getString("CLAVEDISTRIBUIDOR"));
							documento.setNombreDistribuidor((rs.getString("NOMBREDISTRIBUIDOR")==null)?"":rs.getString("NOMBREDISTRIBUIDOR"));
							documento.setNumeroDistribuidor((rs.getString("NUMERODISTRIBUIDOR")==null)?"":rs.getString("NUMERODISTRIBUIDOR"));
							documento.setNumeroSiracDistribuidor((rs.getString("NUMSIRACDISTRIBUIDOR")==null)?"":rs.getString("NUMSIRACDISTRIBUIDOR"));
							documento.setAcuseCarga((rs.getString("ACUSECARGA")==null)?"":rs.getString("ACUSECARGA"));
							documento.setNumDoctoIni((rs.getString("NUMERODOCTOINI")==null)?"":rs.getString("NUMERODOCTOINI"));
							documento.setFechaPublicacion((rs.getString("FECHAPUBLICACION")==null)?"":rs.getString("FECHAPUBLICACION"));
							documento.setFechaEmision((rs.getString("FECHAEMISION")==null)?"":rs.getString("FECHAEMISION"));
							documento.setFechaVenciIni((rs.getString("FECHAVENCIINI")==null)?"":rs.getString("FECHAVENCIINI"));
							documento.setPlazoDoctoIni((rs.getString("PLAZODOCTOINI")==null)?"":rs.getString("PLAZODOCTOINI"));
							documento.setPlazoDescuento((rs.getString("PLAZODESCUENTO")==null)?"":rs.getString("PLAZODESCUENTO"));							
							documento.setPorcentajeDescuento((rs.getString("PORDESCUENTO")==null)?"":rs.getString("PORDESCUENTO"));							
							documento.setClaveModPlazo((rs.getString("CLAVEMODPLAZO")==null)?"":rs.getString("CLAVEMODPLAZO"));
							documento.setModalidadPlazo((rs.getString("MODALIDADPLAZO")==null)?"":rs.getString("MODALIDADPLAZO"));
							documento.setNumDoctoFin((rs.getString("NUMDOCTOFIN")==null)?"":rs.getString("NUMDOCTOFIN"));
							documento.setFechaOperacion((rs.getString("FECHAOPERACION")==null)?"":rs.getString("FECHAOPERACION"));
							documento.setClaveMoneda((rs.getString("CLAVEMONEDA")==null)?"":rs.getString("CLAVEMONEDA"));
							documento.setNombreMoneda((rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA"));
							documento.setMontoCredito((rs.getString("MONTOCREDITO")==null)?"":rs.getString("MONTOCREDITO"));
							documento.setPlazoCredito((rs.getString("PLAZOCREDITO")==null)?"":rs.getString("PLAZOCREDITO"));
							documento.setFechaVenciCredito((rs.getString("FECHAVENCICREDITO")==null)?"":rs.getString("FECHAVENCICREDITO"));
							documento.setMontoInteres((rs.getString("MONTOINTERES")==null)?"":rs.getString("MONTOINTERES"));
							documento.setTasaInteres((rs.getString("TASAINTERES")==null)?"":rs.getString("TASAINTERES"));
							documento.setClaveTipoInteres((rs.getString("CLAVETIPOINTERES")==null)?"":rs.getString("CLAVETIPOINTERES"));
							documento.setTipoInteres((rs.getString("TIPOINTERES")==null)?"":rs.getString("TIPOINTERES"));
							documento.setTipoConversion((rs.getString("TIPOCONVERSION")==null)?"":rs.getString("TIPOCONVERSION"));
							if("1".equals(documento.getClaveMoneda())){
								documento.setMontoDocto((rs.getString("MONTODOCTO")==null)?"":rs.getString("MONTODOCTO"));
								documento.setMontoDoctoDL("0.0");
							}else if("54".equals(documento.getClaveMoneda())){
								documento.setMontoDocto("0.0");
								documento.setMontoDoctoDL((rs.getString("MONTODOCTO")==null)?"":rs.getString("MONTODOCTO"));
							}
							documento.setMontoDescuento(  String.valueOf((Double.parseDouble(documento.getMontoDocto()))*((Double.parseDouble(documento.getPorcentajeDescuento()))/100))) ;
							documento.setTipoCambio((rs.getString("TIPOCAMBIO")==null)?"":rs.getString("TIPOCAMBIO"));
							documento.setResponPagoInteres((rs.getString("RESPONPAGOINTERES")==null)?"":rs.getString("RESPONPAGOINTERES"));
							documento.setTipoCobranza((rs.getString("TIPOCOBRANZA")==null)?"":rs.getString("TIPOCOBRANZA"));
							documento.setReferencia((rs.getString("REFERENCIA")==null)?"":rs.getString("REFERENCIA"));
							documento.setCampoAdicional1((rs.getString("CAMPOADICIONAL1")==null)?"":rs.getString("CAMPOADICIONAL1"));
							documento.setCampoAdicional2((rs.getString("CAMPOADICIONAL2")==null)?"":rs.getString("CAMPOADICIONAL2"));
							documento.setCampoAdicional3((rs.getString("CAMPOADICIONAL3")==null)?"":rs.getString("CAMPOADICIONAL3"));
							documento.setCampoAdicional4((rs.getString("CAMPOADICIONAL4")==null)?"":rs.getString("CAMPOADICIONAL4"));
							documento.setCampoAdicional5((rs.getString("CAMPOADICIONAL5")==null)?"":rs.getString("CAMPOADICIONAL5"));
							documento.setValorTasa((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
							documento.setMonedaLinea((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
							documento.setMontoRecibir((rs.getString("MONTORECIBIR")==null)?"":rs.getString("MONTORECIBIR"));
							documento.setClaveTasa((rs.getString("CLAVETASA")==null)?"":rs.getString("CLAVETASA"));
							
							documento.setClaveDocumento((rs.getString("NUMDOCTOFIN")==null)?"":rs.getString("NUMDOCTOFIN"));
							documento.setClaveEstatus((rs.getString("ICESTATUS")==null)?"":rs.getString("ICESTATUS"));
							documento.setNombreEstatus((rs.getString("NOMBRE_ESTATUS")==null)?"":rs.getString("NOMBRE_ESTATUS")); 
							
							documento.setModalidadRiesgo("E"); //Riesgo EPO(E)  QC-DLHC 
							documento.setRfcEpo((rs.getString("rfcEPO")==null)?"":rs.getString("rfcEPO")); //QC-DLHC 
							documento.setRfcDistribuidor((rs.getString("rfcPyme")==null)?"":rs.getString("rfcPyme")); //QC-DLHC 
                                                        
							lDocumentos.add(documento);

							qrySentencia =
								" select count(*) "+
								" from comrel_producto_docto PD"+
								" ,dis_documento D"+
								" where PD.ic_epo = D.ic_epo"+
								" and PD.ic_producto_nafin = 4"+
								" and D.ic_documento = ? ";

							ps2 = con.queryPrecompilado(qrySentencia);
							ps2.setLong(1, Long.parseLong(documento.getNumDoctoFin()));
							rs2 = ps2.executeQuery();
							int aux = 0;
							if(rs2.next()){
								aux = rs2.getInt(1);
							}
							rs2.close();
							ps2.close();

							if(aux==0){
								throw new NafinException("DIST0012");
							}

							try{
								qrySentencia =  " update dis_documento"+
											" set ic_estatus_docto = ? "+
											" where ic_documento = ? ";

								ps2 = con.queryPrecompilado(qrySentencia);
								ps2.setLong(1, Long.parseLong("24"));
								ps2.setLong(2, Long.parseLong(documento.getNumDoctoFin()));
								ps2.executeUpdate();
								ps2.close();

							} catch(Throwable t) {
								throw new NafinException("Error al precompilar la actualizacion o actualizar los documentos ");
							}
						}
						rs.close();
						ps.close();
					}

					//EposIn.append(rsE.getString("claveEpo")+",");
					//Horario.validarHorario(4, strTipoUsuario, ic_epo, iNoCliente);


					/*SUMATORIA DE MONTOS PARA EL ACUSE*/
					/*
						icMoneda			=	documento.getClaveMoneda();
						monto				=	Double.parseDouble((String)hmColumna.get("MONTORECIBIR"));
						tipoCambio			=	Double.parseDouble(documento.getTipoCambio());
						montoValuado		=	monto*tipoCambio;
						montoInteres 		=	Double.parseDouble(documento.getMontoInteres());
						monedaLinea		= 	(String)hmColumna.get("MONEDA_LINEA");

						if("1".equals(icMoneda)){
							totalDoctosMN 			++;
							totalMontoMN 			+= monto;
							totalMontoValuadoMN 	+= montoValuado;
							totalMontoIntMN		+= montoInteres;
						}

						if("54".equals(icMoneda)&&"54".equals(monedaLinea)){
							totalDoctosUSD			++;
							totalMontoUSD 			+= monto;
							totalMontoValuadoUSD	+= montoValuado;
							totalMontoIntUSD		+= montoInteres;
						}

						if("54".equals(icMoneda)&&"1".equals(monedaLinea)){
							totalDoctosConv		++;
							totalMontoConv			+= monto;
							totalMontoConvMN		+= montoValuado;
							totalMontoIntConv		+= montoInteres;
						}
						*/
					/* TERMINA SUMATORIA DE MONTOS PARA EL ACUSE*/


			} catch(NafinException ne) {
				throw ne;
			} catch(Throwable t) {
				t.printStackTrace();
				throw new NafinException("Error al precompilar la consulta o consultar los documentos ");
			}
				//*********************************************

 return lDocumentos;
}

private List getDoctosSeleccionadosWSCCC(String iClaveIF, AccesoDB con)
	throws NafinException{
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
      ResultSet rs = null;
		ResultSet rs2 = null;
      String qrySentencia = "";
		List lDocumentos = new ArrayList();
   	try{


			// OBTENGO LAS EPOS Y  VALIDO SI TIENEN  LIMITE SOBREGIRADO
				String strSQLEPO =
						"SELECT DISTINCT e.ic_epo AS clave, e.cg_razon_social AS descripcion " +
						"           FROM comcat_epo e, " +
						"                comrel_if_epo ie, " +
						"                comrel_if_epo_x_producto iep, " +
						"                comrel_producto_epo pre, " +
						"                comcat_producto_nafin pn, " +
						"                dis_documento d " +
						"          WHERE ie.ic_epo = e.ic_epo " +
						"            AND e.ic_epo = pre.ic_epo " +
						"            AND pre.ic_producto_nafin = pn.ic_producto_nafin " +
						"            AND ie.ic_if = iep.ic_if " +
						"            AND ie.ic_epo = iep.ic_epo " +
						"            AND d.ic_epo = e.ic_epo " +
						"            AND e.cs_habilitado = ? " +
						"            AND ie.cs_distribuidores IN (?, ?) " +
						"            AND iep.cs_habilitado = ? " +
						"            AND iep.ic_producto_nafin = ? " +
						"            AND pn.ic_producto_nafin = ? " +
						"            AND ie.ic_if = ? " +
						"            AND NVL (pre.cg_tipos_credito, pn.cg_tipos_credito) IN (?, ?) " +
						"            AND d.ic_estatus_docto IN (?) " +
						"            AND d.ic_linea_credito_dm IS NULL " +
						"            AND d.ic_linea_credito IN (SELECT ic_linea_credito " +
						"                                         FROM com_linea_credito " +
						"                                        WHERE ic_if = ?) " +
						"       ORDER BY 2 ";


				String cveEpos = "";

				try{
					List lParams = new ArrayList();
					lParams.add("S");
					lParams.add("S");
					lParams.add("R");
					lParams.add("S");
					lParams.add(new Integer(4));
					lParams.add(new Integer(4));
					lParams.add(new Integer(iClaveIF));
					lParams.add("A");
					lParams.add("C");
					lParams.add(new Integer(3));//Estatus del Documento 3= Seleccionado Pyme
					lParams.add(new Integer(iClaveIF));


					log.debug("getDoctosSeleccionadosWSCCC(" + iClaveIF + "_:: strSQLEPO=" + strSQLEPO);
					log.debug("getDoctosSeleccionadosWSCCC(" + iClaveIF + "_:: strSQLEPO Params=" + lParams);
					ps = con.queryPrecompilado(strSQLEPO, lParams);

					rs = ps.executeQuery();

					//Acuse acuse = new Acuse(3,"4","com",con);
					//acuseFormateado  = acuse.formatear();
					int iter = 0;
					while (rs.next()) {
						cveEpos += iter==0?rs.getString("clave"):(","+rs.getString("clave"));
						iter++;
					}
					rs.close();
					ps.close();

				} catch(Throwable t) {
					throw new NafinException("Error al realizar la consulta para obtener las EPOS ");
				}

				if(!"".equals(cveEpos)){
					qrySentencia =
									"  SELECT  d.ic_epo CLAVEEPO  " +
									"     ,E.cg_razon_social as NOMBREEPO  " +
									"        ,P.ic_pyme as CLAVEDISTRIBUIDOR  " +
									"         ,P.cg_razon_social as NOMBREDISTRIBUIDOR  " +
									"     ,cpe.cg_num_distribuidor as NUMERODISTRIBUIDOR  " +
									"         ,P.in_numero_sirac as NUMSIRACDISTRIBUIDOR  " +
									"        ,D.CC_ACUSE as ACUSECARGA  " +
									"         ,D.ig_numero_docto as NUMERODOCTOINI  " +
									"        ,TO_CHAR(D.DF_CARGA,'dd/mm/yyyy') as FECHAPUBLICACION " +
									"        ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHAEMISION " +
									"        ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHAVENCIINI " +
									"        ,D.IG_PLAZO_DOCTO as PLAZODOCTOINI  " +
									"        ,D.fn_monto as MONTODOCTO  " +
									"        ,D.IG_PLAZO_DESCUENTO as PLAZODESCUENTO  " +
									"        ,nvl(D.fn_porc_descuento,0) as PORDESCUENTO " +
									"        ,TF.IC_TIPO_FINANCIAMIENTO AS CLAVEMODPLAZO  " +
									"        ,TF.CD_DESCRIPCION AS MODALIDADPLAZO  " +
									"     ,D.ic_documento as NUMDOCTOFIN  " +
									"      ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHAOPERACION  " +
									"        ,D.ic_moneda as CLAVEMONEDA  " +
									"         ,M.cd_nombre as NOMBREMONEDA " +
									"         ,D.IG_PLAZO_CREDITO as PLAZOCREDITO " +
									"         ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as FECHAVENCICREDITO         " +
									"     ,DS.fn_importe_interes as MONTOINTERES  " +
									"        ,DS.fn_valor_tasa as TASAINTERES  " +
									"         ,TCI.ic_tipo_cobro_interes as CLAVETIPOINTERES  " +
									"     ,TCI.cd_descripcion as TIPOINTERES " +
									"         ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPOCONVERSION " +
									"         ,D.fn_monto as MONTOCREDITO " +
									"         ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPOCAMBIO  " +
									"     ,NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as RESPONPAGOINTERES  " +
									"     ,NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as TIPOCOBRANZA  " +
									"         ,CT.CD_NOMBRE  ||'' ||  ds.cg_rel_mat || ' ' ||ds.FN_PUNTOS AS REFERENCIA  " +
									"        ,cg_campo1 as CAMPOADICIONAL1  " +
									"        ,cg_campo2 as CAMPOADICIONAL2  " +
									"        ,cg_campo3 as CAMPOADICIONAL3  " +
									"        ,cg_campo4 as CAMPOADICIONAL4  " +
									"        ,cg_campo5 as CAMPOADICIONAL5  " +
									"        ,DS.fn_valor_tasa AS VALOR_TASA " +									
									"        , DS.fn_valor_tasa  - DS.fn_puntos  AS  VALOR_TASA_INTERES  "+
									"        ,LC.ic_moneda as MONEDA_LINEA " +
									"         ,DS.fn_importe_recibir as MONTORECIBIR " +
									"        ,ds.ic_tasa as CLAVETASA  " +
									"     , NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as TIPOCONVERSION2  " +
									"     ,d.fn_tipo_cambio as TIPOCAMBIO2  " +
									"        ,ct_referencia  " +
									"   ,  D.ic_estatus_docto  as ICESTATUS "+
									" , es.CD_DESCRIPCION  as NOMBRE_ESTATUS "+                                                                        
                                                                        "  ,p.CG_RFC  as rfcPyme "+ //QC-DLHC
                                                                        "  ,e.cg_rfc as rfcEPO "+ //QC-DLHC
									
									"  FROM DIS_DOCUMENTO D " +
									"    ,COMCAT_PYME P " +
									"    ,COMREL_PRODUCTO_EPO PE " +
									"    ,COMCAT_PRODUCTO_NAFIN PN " +
									"    ,COMCAT_MONEDA M " +
									"    ,COM_TIPO_CAMBIO TC " +
									"    ,COM_LINEA_CREDITO LC " +
									"    ,COMCAT_TIPO_COBRO_INTERES TCI " +
									"    ,COMCAT_TIPO_FINANCIAMIENTO TF " +
									"    ,COMREL_IF_EPO_X_PRODUCTO IEP " +
									"    ,COMREL_PYME_EPO CPE  " +
									"    ,COMCAT_TASA CT " +
									"    ,COMCAT_EPO E " +
									"    ,DIS_DOCTO_SELECCIONADO DS " +
									"   ,  COMCAT_ESTATUS_DOCTO es  "+
									
									"  WHERE D.IC_EPO = PE.IC_EPO " +
									"    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
									"    AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO " +
									"    AND CPE.IC_EPO = D.IC_EPO  " +
									"    AND CPE.IC_PYME = D.IC_PYME  " +
									"    AND PN.IC_PRODUCTO_NAFIN = 4 " +
									"    AND P.IC_PYME = D.IC_PYME " +
									"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
									"    AND D.IC_EPO = PE.IC_EPO " +
									"    AND LC.IC_MONEDA = M.IC_MONEDA " +
									"    AND D.IC_MONEDA = TC.IC_MONEDA " +
									"    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
									"    AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO " +
									"    AND LC.IC_IF = IEP.IC_IF " +
									"    AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES " +
									"    AND D.ic_estatus_docto = 3 " +
									"    AND IEP.IC_EPO = D.IC_EPO " +
									"    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
									"    AND DS.IC_TASA = CT.IC_TASA  " +
									"    AND D.IC_EPO = E.IC_EPO  " +
									"    AND D.IC_EPO in("+cveEpos+") "+
									"    AND LC.IC_IF = "+iClaveIF+
									"     AND LC.ic_estatus_linea = 12 " +
									"     AND LC.cs_factoraje_con_rec = 'N'  " +
									"    AND D.ic_documento not in(select ic_documento from dis_solicitud) " +
									"   AND ES.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO   "+ 
									"  ORDER BY D.IC_EPO, D.IC_PYME, D.IG_NUMERO_DOCTO ";

						System.out.println("\nqrySentencia: "+qrySentencia);

						ps = con.queryPrecompilado(qrySentencia);
						rs = ps.executeQuery();
						while(rs.next()){
							DocumentoDistIFWS documento = new DocumentoDistIFWS();
							documento.setClaveEpo((rs.getString("CLAVEEPO")==null)?"":rs.getString("CLAVEEPO"));
							documento.setNombreEpo((rs.getString("NOMBREEPO")==null)?"":rs.getString("NOMBREEPO"));
							documento.setClaveDistribuidor((rs.getString("CLAVEDISTRIBUIDOR")==null)?"":rs.getString("CLAVEDISTRIBUIDOR"));
							documento.setNombreDistribuidor((rs.getString("NOMBREDISTRIBUIDOR")==null)?"":rs.getString("NOMBREDISTRIBUIDOR"));
							documento.setNumeroDistribuidor((rs.getString("NUMERODISTRIBUIDOR")==null)?"":rs.getString("NUMERODISTRIBUIDOR"));
							documento.setNumeroSiracDistribuidor((rs.getString("NUMSIRACDISTRIBUIDOR")==null)?"":rs.getString("NUMSIRACDISTRIBUIDOR"));
							documento.setAcuseCarga((rs.getString("ACUSECARGA")==null)?"":rs.getString("ACUSECARGA"));
							documento.setNumDoctoIni((rs.getString("NUMERODOCTOINI")==null)?"":rs.getString("NUMERODOCTOINI"));
							documento.setFechaPublicacion((rs.getString("FECHAPUBLICACION")==null)?"":rs.getString("FECHAPUBLICACION"));
							documento.setFechaEmision((rs.getString("FECHAEMISION")==null)?"":rs.getString("FECHAEMISION"));
							documento.setFechaVenciIni((rs.getString("FECHAVENCIINI")==null)?"":rs.getString("FECHAVENCIINI"));
							documento.setPlazoDoctoIni((rs.getString("PLAZODOCTOINI")==null)?"":rs.getString("PLAZODOCTOINI"));							
							documento.setPlazoDescuento((rs.getString("PLAZODESCUENTO")==null)?"":rs.getString("PLAZODESCUENTO"));
							documento.setPorcentajeDescuento((rs.getString("PORDESCUENTO")==null)?"":rs.getString("PORDESCUENTO"));							
							documento.setClaveModPlazo((rs.getString("CLAVEMODPLAZO")==null)?"":rs.getString("CLAVEMODPLAZO"));
							documento.setModalidadPlazo((rs.getString("MODALIDADPLAZO")==null)?"":rs.getString("MODALIDADPLAZO"));
							documento.setNumDoctoFin((rs.getString("NUMDOCTOFIN")==null)?"":rs.getString("NUMDOCTOFIN"));
							documento.setFechaOperacion((rs.getString("FECHAOPERACION")==null)?"":rs.getString("FECHAOPERACION"));
							documento.setClaveMoneda((rs.getString("CLAVEMONEDA")==null)?"":rs.getString("CLAVEMONEDA"));
							documento.setNombreMoneda((rs.getString("NOMBREMONEDA")==null)?"":rs.getString("NOMBREMONEDA"));
							documento.setMontoCredito((rs.getString("MONTOCREDITO")==null)?"":rs.getString("MONTOCREDITO"));
							documento.setPlazoCredito((rs.getString("PLAZOCREDITO")==null)?"":rs.getString("PLAZOCREDITO"));
							documento.setFechaVenciCredito((rs.getString("FECHAVENCICREDITO")==null)?"":rs.getString("FECHAVENCICREDITO"));
							documento.setMontoInteres((rs.getString("MONTOINTERES")==null)?"":rs.getString("MONTOINTERES"));
						//	documento.setTasaInteres((rs.getString("MONTOINTERES")==null)?"":rs.getString("MONTOINTERES"));
							documento.setTasaInteres((rs.getString("VALOR_TASA_INTERES")==null)?"":rs.getString("VALOR_TASA_INTERES"));
							documento.setClaveTipoInteres((rs.getString("CLAVETIPOINTERES")==null)?"":rs.getString("CLAVETIPOINTERES"));
							documento.setTipoInteres((rs.getString("TIPOINTERES")==null)?"":rs.getString("TIPOINTERES"));
							documento.setTipoConversion((rs.getString("TIPOCONVERSION")==null)?"":rs.getString("TIPOCONVERSION"));
						
							if("1".equals(documento.getClaveMoneda())){
								documento.setMontoDocto((rs.getString("MONTODOCTO")==null)?"":rs.getString("MONTODOCTO"));
								documento.setMontoDoctoDL("0.0");
							}else if("54".equals(documento.getClaveMoneda())){
								documento.setMontoDocto("0.0");
								documento.setMontoDoctoDL((rs.getString("MONTODOCTO")==null)?"":rs.getString("MONTODOCTO"));
							}
							documento.setMontoDescuento(  String.valueOf((Double.parseDouble(documento.getMontoDocto()))*((Double.parseDouble(documento.getPorcentajeDescuento()))/100))) ;
							documento.setTipoCambio((rs.getString("TIPOCAMBIO")==null)?"":rs.getString("TIPOCAMBIO"));
							documento.setResponPagoInteres((rs.getString("RESPONPAGOINTERES")==null)?"":rs.getString("RESPONPAGOINTERES"));
							documento.setTipoCobranza((rs.getString("TIPOCOBRANZA")==null)?"":rs.getString("TIPOCOBRANZA"));
							documento.setReferencia((rs.getString("REFERENCIA")==null)?"":rs.getString("REFERENCIA"));
							documento.setCampoAdicional1((rs.getString("CAMPOADICIONAL1")==null)?"":rs.getString("CAMPOADICIONAL1"));
							documento.setCampoAdicional2((rs.getString("CAMPOADICIONAL2")==null)?"":rs.getString("CAMPOADICIONAL2"));
							documento.setCampoAdicional3((rs.getString("CAMPOADICIONAL3")==null)?"":rs.getString("CAMPOADICIONAL3"));
							documento.setCampoAdicional4((rs.getString("CAMPOADICIONAL4")==null)?"":rs.getString("CAMPOADICIONAL4"));
							documento.setCampoAdicional5((rs.getString("CAMPOADICIONAL5")==null)?"":rs.getString("CAMPOADICIONAL5"));
							documento.setValorTasa((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
							documento.setMonedaLinea((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
							documento.setMontoRecibir((rs.getString("MONTORECIBIR")==null)?"":rs.getString("MONTORECIBIR"));
							documento.setClaveTasa((rs.getString("CLAVETASA")==null)?"":rs.getString("CLAVETASA"));
							
							documento.setClaveDocumento((rs.getString("NUMDOCTOFIN")==null)?"":rs.getString("NUMDOCTOFIN"));
							documento.setClaveEstatus((rs.getString("ICESTATUS")==null)?"":rs.getString("ICESTATUS"));
							documento.setNombreEstatus((rs.getString("NOMBRE_ESTATUS")==null)?"":rs.getString("NOMBRE_ESTATUS"));	
							
							documento.setModalidadRiesgo("D"); //Distribuidor(D)  QC-DLHC 
							documento.setRfcEpo((rs.getString("rfcEPO")==null)?"":rs.getString("rfcEPO")); //QC-DLHC 
							documento.setRfcDistribuidor((rs.getString("rfcPyme")==null)?"":rs.getString("rfcPyme")); //QC-DLHC 
                                                    
							lDocumentos.add(documento);

							qrySentencia =
								" select count(*) "+
								" from comrel_producto_docto PD"+
								" ,dis_documento D"+
								" where PD.ic_epo = D.ic_epo"+
								" and PD.ic_producto_nafin = 4"+
								" and D.ic_documento = ? ";

							ps2 = con.queryPrecompilado(qrySentencia);
							ps2.setLong(1, Long.parseLong(documento.getNumDoctoFin()));
							rs2 = ps2.executeQuery();
							int aux = 0;
							if(rs2.next()){
								aux = rs2.getInt(1);
							}
							rs2.close();
							ps2.close();

							if(aux==0){
								throw new NafinException("DIST0012");
							}

							try{
								qrySentencia =  " update dis_documento"+
											" set ic_estatus_docto = ? "+
											" where ic_documento = ? ";

								ps2 = con.queryPrecompilado(qrySentencia);
								ps2.setLong(1, Long.parseLong("24"));
								ps2.setLong(2, Long.parseLong(documento.getNumDoctoFin()));
								ps2.executeUpdate();
								ps2.close();

							} catch(Throwable t) {
								t.printStackTrace();
								throw new NafinException("Error al precompilar la actualizacion o actualizar los documentos ");
							}
						}
						rs.close();
						ps.close();

				}

					//Horario.validarHorario(4, strTipoUsuario, ic_epo, iNoCliente);







					/*SUMATORIA DE MONTOS PARA EL ACUSE*/
					/*
						icMoneda			=	documento.getClaveMoneda();
						monto				=	Double.parseDouble((String)hmColumna.get("MONTORECIBIR"));
						tipoCambio			=	Double.parseDouble(documento.getTipoCambio());
						montoValuado		=	monto*tipoCambio;
						montoInteres 		=	Double.parseDouble(documento.getMontoInteres());
						monedaLinea		= 	(String)hmColumna.get("MONEDA_LINEA");

						if("1".equals(icMoneda)){
							totalDoctosMN 			++;
							totalMontoMN 			+= monto;
							totalMontoValuadoMN 	+= montoValuado;
							totalMontoIntMN		+= montoInteres;
						}

						if("54".equals(icMoneda)&&"54".equals(monedaLinea)){
							totalDoctosUSD			++;
							totalMontoUSD 			+= monto;
							totalMontoValuadoUSD	+= montoValuado;
							totalMontoIntUSD		+= montoInteres;
						}

						if("54".equals(icMoneda)&&"1".equals(monedaLinea)){
							totalDoctosConv		++;
							totalMontoConv			+= monto;
							totalMontoConvMN		+= montoValuado;
							totalMontoIntConv		+= montoInteres;
						}
						*/
					/* TERMINA SUMATORIA DE MONTOS PARA EL ACUSE*/


			} catch(NafinException ne) {
				throw ne;
			} catch(Throwable t) {
				t.printStackTrace();
				throw new NafinException("Error al precompilar la consulta o consultar los documentos ");
			}
				//*********************************************

 return lDocumentos;
}


/**
	 * metodo que envia los errores Inesperados en el proceso de WS
	 * a los correos que existen en la tabla de comcat_correoWS
	 * esto es enviado solo si hay Errores Inesperados
	 * @param errorInesperado Datos del error Inesperado. Ser� el cuerpo del correo
	 * @param metodo Proceso que genera el error. Con base en este par�metro se
	 * 	selecciona el titulo del correo.
	 * @return
	 */
	private String enviodeCorreoWS(StringBuffer errorInesperado, String metodo) {
		log.info("EnviodeCorreoWS (E)");

		StringBuffer  qrySentencia = new StringBuffer();
		PreparedStatement  ps	= null;
		ResultSet  rs	= null;
		List correos = new ArrayList();
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;

		String respuesta ="";

		String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
				"color:#000066; "+
				"padding-top:1px; "+
				"padding-right:2px; "+
				"padding-bottom:1px; "+
				"padding-left:2px; "+
				"height:22px; "+
				"font-size:11px;'";

		String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
				"font-size: 10px; "+
				"font-weight: bold;"+
				"color: #FFFFFF;"+
				"background-color: #4d6188;"+
				"padding-top: 3px;"+
				"padding-right: 1px;"+
				"padding-bottom: 1px;"+
				"padding-left: 3px;"+
				"height: 25px;"+
				"border: 1px solid #1a3c54;'";


		String titulo ="";
		if(metodo.equals("S")){
			titulo = "ERRORES INESPERADOS EN LA SELECCION DE DOCUMENTOS PYME WS";
		}else if (metodo.equals("C")){
			titulo = "ERRORES INESPERADOS EN LA CONFIRMACION DE DOCUMENTOS WS ";
		}else if (metodo.equals("CTAS")){
			titulo = "ERRORES INESPERADOS EN LA SELECCION DE CUENTAS PYME PENDIENTES POR AUTORIZAR WS ";
		}else if (metodo.equals("CTAS_A")){
			titulo = "ERRORES INESPERADOS EN LA SELECCION DE CUENTAS PYME AUTORIZADAS WS ";
		}else if (metodo.equals("C_CTAS")){
			titulo = "ERRORES INESPERADOS EN LA CONFIRMACION DE CUENTAS PYME WS ";
		}

		log.debug(errorInesperado);
		try {
			con.conexionDB();

			if(!errorInesperado.toString().equals("")) {
				qrySentencia.append(" SELECT cg_email FROM comcat_correoWS ");
				ps = con.queryPrecompilado(qrySentencia.toString());
				rs = ps.executeQuery();

				while(rs.next()){
					correos.add(rs.getString("CG_EMAIL"));
				}
				rs.close();
				ps.close();

				for(int i=0;i< correos.size();i++){

					String correoPara =(String)correos.get(i);

					log.debug(correoPara);

					StringBuffer mensajeCorreo =  new StringBuffer();
					mensajeCorreo.append("<html>");
					mensajeCorreo.append("<head>");
					mensajeCorreo.append("<title>");
					mensajeCorreo.append(""+titulo+"");
					mensajeCorreo.append("</title>");
					mensajeCorreo.append("</head>");
					mensajeCorreo.append("<body>");

					mensajeCorreo.append("<table cellspacing='0' cellpadding='0' border='0' align='left' width='400'>");
					mensajeCorreo.append("<tr><td "+style+" "+styleEncabezados+" align='left'><b>ERRORES INESPERADOS: :<b></td></tr>");
					mensajeCorreo.append("<tr><td "+style+" width='400'>"+errorInesperado+"\r\n"+"<br></td></tr>");

					mensajeCorreo.append("<p>");
					mensajeCorreo.append("<p>");
					mensajeCorreo.append("<tr><td "+style+" width='400'>"+"Atentamente"+"</td></tr>");
					mensajeCorreo.append("<tr><td "+style+" width='400'>"+"Nacional Financiera  S.N.C."+"</td></tr>");
					mensajeCorreo.append("</table>");
					mensajeCorreo.append("</body>");
					mensajeCorreo.append("</html>");

					Correo correo = new Correo();
					correo.enviarTextoHTML("cadenas@nafin.gob.mx", correoPara, titulo, mensajeCorreo.toString());
				}
				con.terminaTransaccion(true);
			}

			return respuesta;
		} catch(Exception e) {
			log.error("EnviodeCorreoWS (Exception) " + e);
			throw new AppException("Error al enviar el correo de '" + titulo + "'",  e);
		} finally {
			log.info("EnviodeCorreoWS (S)");
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene la cadena a firmar por parte del IF, usando la autorizacion via
	 * WebService.
	 * La cadena est� confirmado unicamente por los atributos que tengan valor.
	 * @param documentos Arreglo de documentos que ser�n utilizados en el proceso
	 * de cambio de estatus v�a WebService.
	 * @return Cadena a firmar
	 */
	public String getCadenaFirmarDistWS(DocumentoDistIFWS[] documentos) {
		log.info("getCadenaFirmarWS(E)");
		StringBuffer str = new StringBuffer();
		if (documentos != null && documentos.length > 0) {
			for (int i = 0; i < documentos.length; i++) {
				if (documentos[i].getClaveDocumento() != null && !documentos[i].getClaveDocumento().equals("")) {
					str.append(documentos[i].getClaveDocumento());
				} else {
					throw new AppException("La Clave de Documento es requerida para cada uno de los documentos");
				}
				if (documentos[i].getClaveEstatus() != null && !documentos[i].getClaveEstatus().equals("")) {
					str.append("|"+documentos[i].getClaveEstatus());
				} else {
					throw new AppException("La Clave del Estatus es requerida para cada uno de los documentos");
				}
				//Si el documento se pasa a negociable (se rechaza la autorizacion del descuento) es
				//obligatorio poner la clave de rechazo, si no est�, lanza exepcion.
				if ( "2".equals(documentos[i].getClaveEstatus()) && (
						documentos[i].getClaveRechazo() == null || documentos[i].getClaveRechazo().length() == 0) ) {
					throw new AppException("La Clave de Rechazo es requerida si el estatus es 2 (Negociable) para cada uno de los documentos");
				}
				if (documentos[i].getClaveRechazo() != null && !documentos[i].getClaveRechazo().equals("")) {
					str.append("|"+documentos[i].getClaveRechazo());
				}
				if (documentos[i].getClaveEpo() != null && !documentos[i].getClaveEpo().equals("")) {
					str.append("|"+documentos[i].getClaveEpo());
				}
				if (documentos[i].getNombreEpo() != null && !documentos[i].getNombreEpo().equals("")) {
					str.append("|"+documentos[i].getNombreEpo());
				}
				if (documentos[i].getClaveDistribuidor() != null && !documentos[i].getClaveDistribuidor().equals("")) {
					str.append("|"+documentos[i].getClaveDistribuidor());
				}
				if (documentos[i].getNombreDistribuidor() != null && !documentos[i].getNombreDistribuidor().equals("")) {
					str.append("|"+documentos[i].getNombreDistribuidor());
				}
				if (documentos[i].getNumeroSiracDistribuidor() != null && !documentos[i].getNumeroSiracDistribuidor().equals("")) {
					str.append("|"+documentos[i].getNumeroSiracDistribuidor());
				}
				if (documentos[i].getAcuseCarga() != null && !documentos[i].getAcuseCarga().equals("")) {
					str.append("|"+documentos[i].getAcuseCarga());
				}
				if (documentos[i].getNumDoctoIni() != null && !documentos[i].getNumDoctoIni().equals("")) {
					str.append("|"+documentos[i].getNumDoctoIni());
				}
				if (documentos[i].getFechaPublicacion() != null && !documentos[i].getFechaPublicacion().equals("")) {
					str.append("|"+documentos[i].getFechaPublicacion());
				}
				if (documentos[i].getFechaEmision() != null && !documentos[i].getFechaEmision().equals("")) {
					str.append("|"+documentos[i].getFechaEmision());
				}
				if (documentos[i].getFechaVenciIni() != null && !documentos[i].getFechaVenciIni().equals("")) {
					str.append("|"+documentos[i].getFechaVenciIni());
				}
				if (documentos[i].getPlazoDoctoIni() != null && !documentos[i].getPlazoDoctoIni().equals("")) {
					str.append("|"+documentos[i].getPlazoDoctoIni());
				}
				if (documentos[i].getMontoDocto() != null && !documentos[i].getMontoDocto().equals("")) {
					str.append("|"+documentos[i].getMontoDocto());
				}
				if (documentos[i].getPlazoDescuento() != null && !documentos[i].getPlazoDescuento().equals("")) {
					str.append("|"+documentos[i].getPlazoDescuento());
				}
				if (documentos[i].getPorcentajeDescuento() != null && !documentos[i].getPorcentajeDescuento().equals("")) {
					str.append("|"+documentos[i].getPorcentajeDescuento());
				}
				if (documentos[i].getMontoDescuento() != null && !documentos[i].getMontoDescuento().equals("")) {
					str.append("|"+documentos[i].getMontoDescuento());
				}
				if (documentos[i].getClaveModPlazo() != null && !documentos[i].getClaveModPlazo().equals("")) {
					str.append("|"+documentos[i].getClaveModPlazo());
				}
				if (documentos[i].getModalidadPlazo() != null && !documentos[i].getModalidadPlazo().equals("")) {
					str.append("|"+documentos[i].getModalidadPlazo());
				}
				if (documentos[i].getNumDoctoFin() != null && !documentos[i].getNumDoctoFin().equals("")) {
					str.append("|"+documentos[i].getNumDoctoFin());
				}
				if (documentos[i].getFechaOperacion() != null && !documentos[i].getFechaOperacion().equals("")) {
					str.append("|"+documentos[i].getFechaOperacion());
				}
				if (documentos[i].getClaveMoneda() != null && !documentos[i].getClaveMoneda().equals("")) {
					str.append("|"+documentos[i].getClaveMoneda());
				}
				if (documentos[i].getNombreMoneda() != null && !documentos[i].getNombreMoneda().equals("")) {
					str.append("|"+documentos[i].getNombreMoneda());
				}
				if (documentos[i].getMontoCredito() != null && !documentos[i].getMontoCredito().equals("")) {
					str.append("|"+documentos[i].getMontoCredito());
				}
				if (documentos[i].getPlazoCredito() != null && !documentos[i].getPlazoCredito().equals("")) {
					str.append("|"+documentos[i].getPlazoCredito());
				}
				if (documentos[i].getFechaVenciCredito() != null && !documentos[i].getFechaVenciCredito().equals("")) {
					str.append("|"+documentos[i].getFechaVenciCredito());
				}
				if (documentos[i].getMontoInteres() != null && !documentos[i].getMontoInteres().equals("")) {
					str.append("|"+documentos[i].getMontoInteres());
				}
				if (documentos[i].getTasaInteres() != null && !documentos[i].getTasaInteres().equals("")) {
					str.append("|"+documentos[i].getTasaInteres());
				}
				if (documentos[i].getClaveTipoInteres() != null && !documentos[i].getClaveTipoInteres().equals("")) {
					str.append("|"+documentos[i].getClaveTipoInteres());
				}
				if (documentos[i].getTipoInteres () != null && !documentos[i].getTipoInteres().equals("")) {
					str.append("|"+documentos[i].getTipoInteres());
				}
				if (documentos[i].getTipoConversion() != null && !documentos[i].getTipoConversion().equals("")) {
					str.append("|"+documentos[i].getTipoConversion());
				}
				if (documentos[i].getMontoDoctoDL() != null && !documentos[i].getMontoDoctoDL().equals("")) {
					str.append("|"+documentos[i].getMontoDoctoDL());
				}
				if (documentos[i].getTipoCambio() != null && !documentos[i].getTipoCambio().equals("")) {
					str.append("|"+documentos[i].getTipoCambio());
				}
				if (documentos[i].getResponPagoInteres() != null && !documentos[i].getResponPagoInteres().equals("")) {
					str.append("|"+documentos[i].getResponPagoInteres());
				}
				if (documentos[i].getTipoCobranza() != null && !documentos[i].getTipoCobranza().equals("")) {
					str.append("|"+documentos[i].getTipoCobranza());
				}
				if (documentos[i].getReferencia() != null && !documentos[i].getReferencia().equals("")) {
					str.append("|"+documentos[i].getReferencia());
				}
				if (documentos[i].getCampoAdicional1() != null && !documentos[i].getCampoAdicional1().equals("")) {
					str.append("|"+documentos[i].getCampoAdicional1());
				}
				if (documentos[i].getCampoAdicional2 () != null && !documentos[i].getCampoAdicional2().equals("")) {
					str.append("|"+documentos[i].getCampoAdicional2());
				}
				if (documentos[i].getCampoAdicional3 () != null && !documentos[i].getCampoAdicional3().equals("")) {
					str.append("|"+documentos[i].getCampoAdicional3());
				}
				if (documentos[i].getCampoAdicional4() != null && !documentos[i].getCampoAdicional4().equals("")) {
					str.append("|"+documentos[i].getCampoAdicional4());
				}
				if (documentos[i].getCampoAdicional5() != null && !documentos[i].getCampoAdicional5().equals("")) {
					str.append("|"+documentos[i].getCampoAdicional5());
				}
				str.append("\n");
			} //fin del for
		}//fin if hay datos.
		log.info("getCadenaFirmarWS(S)");
		return str.toString();
	}

	public ProcesoIFDistWSInfo getAvisosNotificacionDisWS (String claveUsuario, String password, 	String claveEpo, String clavePyme, String claveIF,
																			String fecOperacionIni, String fecOperacionFin) {
		log.info("getAvisosNotificacionDisWS(E)");
		int iClaveIF = 0;
		int iClaveEPO = 0;
		boolean existeRangofechas = false;
		List lstDocument = new ArrayList();
		AccesoDB con = new AccesoDB();
		PreparedStatement psPrincipal = null;
		ResultSet rsPrincipal = null;
		ProcesoIFDistWSInfo resultadoProceso = new ProcesoIFDistWSInfo();
		String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		UtilUsr utilUsr = new UtilUsr();


		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveUsuario == null || claveUsuario.equals("") ||
					password == null || password.equals("") ||
					claveIF == null || claveIF.equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveIF = Integer.parseInt(claveIF);


			if(claveEpo != null && !claveEpo.equals(""))
				iClaveEPO = Integer.parseInt(claveEpo);



			if(fecOperacionIni!=null && !fecOperacionIni.equals("")){
				if(!Comunes.esFechaValida(fecOperacionIni,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
				existeRangofechas = true;
			}
			if(fecOperacionFin!=null && !fecOperacionFin.equals("")){
				if(!Comunes.esFechaValida(fecOperacionFin,"dd/MM/yyyy")) throw new Exception("Formato de Fechas Incorrecto");
				existeRangofechas = true;
			}

			//if(!existeRangofechas) throw new Exception("Se requiere al menos un rango de fechas");

		} catch(Exception e) {
			log.error("avisosNotificacionWS(Error) ", e);

			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso

			resultadoProceso.setResumenEjecucion("Error en la validacion de parametros: " +
					"(" + claveUsuario + "," + password + "," + claveEpo +"," + clavePyme +
					"," + claveIF + "," + fecOperacionIni + "," + fecOperacionFin +
					")" + "Error=" + e.getMessage());
			return resultadoProceso;
		}


		//******************************Validacion de fechas*********************************
		try{

			//boolean limitarRangoFechas = this.getLimitarRangoFechas();

			if(fecOperacionIni!=null && !fecOperacionIni.equals("")){
				if(fecOperacionFin==null || fecOperacionFin.equals("")) throw new Exception("Se requiere fecha de Notificacion final");
				else{
					if(!Comunes.esComparacionFechaValida(fecOperacionFin,"ge",fecOperacionIni,"dd/MM/yyyy"))
						throw new Exception("La fecha de operacion final debe ser mayor o igual a la inicial");

					//if(limitarRangoFechas && Fecha.restaFechas(fecNotificacionIni,fecNotificacionFin)>7)
					//	throw new Exception("El rango de fechas no debe ser mayor a 7 dias");

				}
			}


		} catch(Exception e) {
			log.error("getAvisosNotificacionDisWS(Error) " +  e.getMessage());

			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso

			resultadoProceso.setResumenEjecucion("Error en rango de Fechas: " +
					"(" + fecOperacionIni + "," + fecOperacionFin +")" + "Error=" + e.getMessage());
			return resultadoProceso;
		}

		try{
						//Validacion del usuario y Password y que sea del IF especificado
			List cuentasIF = utilUsr.getUsuariosxAfiliado(claveIF, "I");

			log.debug("getAvisosNotificacionDisWS::Validando claveIF/usuario");

			if (!cuentasIF.contains(claveUsuario)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("El usuario no existe para el IF especificado");
				return resultadoProceso; //Termina Proceso
			}

			log.debug("getAvisosNotificacionDisWS::Validando usuario/passwd");
			if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("Fallo la autenticacion del usuario");
				return resultadoProceso; //Termina Proceso
			}

			con.conexionDB();

//**********************************VALIDACIONES PARA EL QUERY**************************
			StringBuffer condicion = new StringBuffer();

			if (claveEpo!=null && !claveEpo.equals("")){
				condicion.append( " AND D.ic_epo = ? ");
			}


			if (clavePyme!=null && !clavePyme.equals("")){
				condicion.append( " AND D.ic_pyme = ? ");
			}

			if(fecOperacionIni!= null && !fecOperacionIni.equals(""))
			{
				if(fecOperacionFin!= null && !fecOperacionFin.equals(""))
					condicion.append(
						" AND (c3.df_fecha_hora >= trunc(to_date(?,'dd/mm/yyyy')) " +
						" AND  c3.df_fecha_hora < trunc(to_date(?,'dd/mm/yyyy')+1)) ");
			}

			if( (claveEpo ==null || claveEpo.equals("")) &&
					(fecOperacionIni==null || fecOperacionIni.equals("")) &&
					(fecOperacionFin==null || fecOperacionFin.equals("")) ){// && tipoFactoraje.equals("")
				condicion.append(
					"   AND c3.df_fecha_hora >= TO_DATE (?, 'dd/mm/yyyy') " +
					"   AND c3.df_fecha_hora < (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
			}

			//monedaParametrizadaWS = getParametrizacionMonedaWS(iClaveIF);

			//if (monedaParametrizadaWS.equals("MN") ||
			//		monedaParametrizadaWS.equals("DA")) {
			//	condicion.append(" AND d.ic_moneda = ? ");
			//}


			String sentenciaSQL =
					"SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF,CPE)   index(d cp_dis_documento_pk)   index(m cp_comcat_moneda_pk)*/ " +
					"		  d.ic_epo AS claveepo,  " +
					"       e.cg_razon_social AS nombreepo, " +
					"       p.ic_pyme AS clavedistribuidor, " +
					"       p.cg_razon_social AS nombredistribuidor, " +
					"       cpe.cg_num_distribuidor AS numerodistribuidor, " +
					"       p.in_numero_sirac AS numsiracdistribuidor,  " +
					"       d.cc_acuse AS acusecarga, " +
					"       d.ig_numero_docto AS numerodoctoini, " +
					"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fechapublicacion, " +
					"       TO_CHAR (d.df_fecha_emision, 'dd/mm/yyyy') AS fechaemision, " +
					"       TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavenciini, " +
					"       d.ig_plazo_docto AS plazodoctoini,  " +
					"       d.fn_monto AS montodocto, " +
					"       d.ig_plazo_descuento AS plazodescuento, " +
					"       NVL (d.fn_porc_descuento, 0) AS montodescuento, " +
					"       tf.ic_tipo_financiamiento AS clavemodplazo, " +
					"       tf.cd_descripcion AS modalidadplazo,  " +
					"       d.ic_documento AS numdoctofin, " +
					"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fechaoperacion, " +
					"       d.ic_moneda AS clavemoneda,  " +
					"       m.cd_nombre AS nombremoneda, " +
					"       d.ig_plazo_credito AS plazocredito, " +
					"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fechavencicredito, " +
					"       ds.fn_importe_interes AS montointeres,  " +
					"       ds.fn_valor_tasa AS tasainteres, " +
					"       tci.ic_tipo_cobro_interes AS clavetipointeres, " +
					"       tci.cd_descripcion AS tipointeres, " +
					"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"               'N', 'Sin conversion', " +
					"               'P', 'Peso-Dolar', " +
					"               '' " +
					"              ) AS tipoconversion, " +
					"       d.fn_monto AS montodocto, " +
					"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"               'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), " +
					"               '1' " +
					"              ) AS tipocambio, " +
					"       NVL (pe.cg_responsable_interes, " +
					"            pn.cg_responsable_interes " +
					"           ) AS responpagointeres, " +
					"       NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) AS tipocobranza, " +
					"       ct.cd_nombre || '' || ds.cg_rel_mat || ' ' " +
					"       || ds.fn_puntos AS referencia, " +
					"       cg_campo1 AS campoadicional1, cg_campo2 AS campoadicional2, " +
					"       cg_campo3 AS campoadicional3, cg_campo4 AS campoadicional4, " +
					"       cg_campo5 AS campoadicional5, " +
					"       DECODE (ds.cg_rel_mat, " +
					"               '+', ds.fn_valor_tasa + ds.fn_puntos, " +
					"               '-', ds.fn_valor_tasa - ds.fn_puntos, " +
					"               '*', ds.fn_valor_tasa * ds.fn_puntos, " +
					"               '/', ds.fn_valor_tasa / ds.fn_puntos, " +
					"               0 " +
					"              ) AS valor_tasa, " +
					"       lcd.ic_moneda AS moneda_linea, ds.fn_importe_recibir AS montorecibir, " +
					"       ds.ic_tasa AS clavetasa, " +
					"       NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) AS tipoconversion2, " +
					"       D.fn_monto as MONTOCREDITO, " +
					"       d.fn_tipo_cambio AS tipocambio2, ct_referencia, " +
					"       d.ic_estatus_docto AS claveestatus, ced.cd_descripcion as nombreestatus " +
					"      , ( d.IG_MESES_INTERES * 30 ) as MESES_INTERESES "+ //F09-2015
		         "      , d.ig_tipo_pago as tipoPago   "+ //F09-2015
					
					"  FROM dis_documento d, " +
					"       dis_docto_seleccionado ds, " +
					"       dis_solicitud s, " +
					"       com_acuse3 c3, " +
					"       dis_linea_credito_dm lcd, " +
					"       comcat_pyme p, " +
					"       comrel_producto_epo pe, " +
					"       comcat_producto_nafin pn, " +
					"       comcat_moneda m, " +
					"       com_tipo_cambio tc, " +
					"       comcat_tipo_cobro_interes tci, " +
					"       comrel_if_epo_x_producto iep, " +
					"       comcat_tasa ct, " +
					"       comcat_epo e, " +
					"       comcat_estatus_docto ed, " +
					"       comcat_if ci, " +
					"       comcat_tipo_financiamiento tf, " +
					"       comrel_pyme_epo cpe, " +
					"       comcat_estatus_docto ced " +
					" WHERE d.ic_epo = pe.ic_epo " +
					"   AND d.ic_pyme = p.ic_pyme " +
					"   AND d.ic_epo = cpe.ic_epo " +
					"   AND d.ic_pyme = cpe.ic_pyme " +
					"   AND d.ic_estatus_docto = ced.ic_estatus_docto " +
					"   AND d.ic_documento = ds.ic_documento " +
					"   AND d.ic_linea_credito_dm = lcd.ic_linea_credito_dm " +
					"   AND d.ic_epo = pe.ic_epo " +
					"   AND d.ic_moneda = m.ic_moneda " +
					"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
					"   AND d.ic_epo = e.ic_epo " +
					"   AND d.ic_estatus_docto = ed.ic_estatus_docto " +
					"   AND d.ic_epo = iep.ic_epo " +
					"   AND ds.ic_documento = s.ic_documento " +
					"   AND ds.ic_tasa = ct.ic_tasa " +
					"   AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
					"   AND iep.ic_producto_nafin = pn.ic_producto_nafin " +
					"   AND lcd.ic_if = iep.ic_if " +
					"   AND lcd.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes " +
					"   AND s.cc_acuse = c3.cc_acuse " +
					"   AND lcd.ic_if = ci.ic_if " +
					"   AND m.ic_moneda = tc.ic_moneda " +
					"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
					"                         FROM com_tipo_cambio " +
					"                        WHERE ic_moneda = m.ic_moneda) " +
					"   AND lcd.ic_if = ? " +
					"   AND e.cs_habilitado = 'S' " +
					"   AND d.ic_estatus_docto IN (4, 11) " +
					condicion.toString()+
					"UNION ALL " +
					"SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF,CPE)   index(d cp_dis_documento_pk)   index(m cp_comcat_moneda_pk)*/ " +
					"       d.ic_epo AS claveepo,  " +
					"       e.cg_razon_social AS nombreepo, " +
					"       p.ic_pyme AS clavedistribuidor, " +
					"       p.cg_razon_social AS nombredistribuidor, " +
					"       cpe.cg_num_distribuidor AS numerodistribuidor, " +
					"       p.in_numero_sirac AS numsiracdistribuidor,  " +
					"       d.cc_acuse AS acusecarga, " +
					"       d.ig_numero_docto AS numerodoctoini, " +
					"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fechapublicacion, " +
					"       TO_CHAR (d.df_fecha_emision, 'dd/mm/yyyy') AS fechaemision, " +
					"       TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavenciini, " +
					"       d.ig_plazo_docto AS plazodoctoini,  " +
					"       d.fn_monto AS montodocto, " +
					"       d.ig_plazo_descuento AS plazodescuento, " +
					"       NVL (d.fn_porc_descuento, 0) AS montodescuento, " +
					"       tf.ic_tipo_financiamiento AS clavemodplazo, " +
					"       tf.cd_descripcion AS modalidadplazo,  " +
					"       d.ic_documento AS numdoctofin, " +
					"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fechaoperacion, " +
					"       d.ic_moneda AS clavemoneda,  " +
					"       m.cd_nombre AS nombremoneda, " +
					"       d.ig_plazo_credito AS plazocredito, " +
					"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fechavencicredito, " +
					"       ds.fn_importe_interes AS montointeres,  " +
					"       ds.fn_valor_tasa AS tasainteres, " +
					"       tci.ic_tipo_cobro_interes AS clavetipointeres, " +
					"       tci.cd_descripcion AS tipointeres, " +
					"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"               'N', 'Sin conversion', " +
					"               'P', 'Peso-Dolar', " +
					"               '' " +
					"              ) AS tipoconversion, " +
					"       d.fn_monto AS montodocto, " +
					"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"               'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), " +
					"               '1' " +
					"              ) AS tipocambio, " +
					"       NVL (pe.cg_responsable_interes, " +
					"            pn.cg_responsable_interes " +
					"           ) AS responpagointeres, " +
					"       NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) AS tipocobranza, " +
					"       ct.cd_nombre || '' || ds.cg_rel_mat || ' ' " +
					"       || ds.fn_puntos AS referencia, " +
					"       cg_campo1 AS campoadicional1, cg_campo2 AS campoadicional2, " +
					"       cg_campo3 AS campoadicional3, cg_campo4 AS campoadicional4, " +
					"       cg_campo5 AS campoadicional5, " +
					"       DECODE (ds.cg_rel_mat, " +
					"               '+', ds.fn_valor_tasa + ds.fn_puntos, " +
					"               '-', ds.fn_valor_tasa - ds.fn_puntos, " +
					"               '*', ds.fn_valor_tasa * ds.fn_puntos, " +
					"               '/', ds.fn_valor_tasa / ds.fn_puntos, " +
					"               0 " +
					"              ) AS valor_tasa, " +
					"       lcd.ic_moneda AS moneda_linea, ds.fn_importe_recibir AS montorecibir, " +
					"       ds.ic_tasa AS clavetasa, " +
					"       NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) AS tipoconversion2, " +
					"       D.fn_monto as MONTOCREDITO, " +
					"       d.fn_tipo_cambio AS tipocambio2, ct_referencia, " +
					"       d.ic_estatus_docto AS claveestatus, ced.cd_descripcion as nombreestatus " +
					"      , ( d.IG_MESES_INTERES * 30 ) as MESES_INTERESES "+ //F09-2015
		         "      , d.ig_tipo_pago as tipoPago   "+ //F09-2015
					
					"  FROM dis_documento d, " +
					"       dis_docto_seleccionado ds, " +
					"       dis_solicitud s, " +
					"       com_acuse3 c3, " +
					"       com_linea_credito lcd, " +
					"       comcat_pyme p, " +
					"       comrel_producto_epo pe, " +
					"       comcat_producto_nafin pn, " +
					"       comcat_moneda m, " +
					"       com_tipo_cambio tc, " +
					"       comcat_tipo_cobro_interes tci, " +
					"       comrel_if_epo_x_producto iep, " +
					"       comcat_tasa ct, " +
					"       comcat_epo e, " +
					"       comcat_estatus_docto ed, " +
					"       comcat_if ci, " +
					"       comcat_tipo_financiamiento tf, " +
					"       comrel_pyme_epo cpe,  " +
					"       comcat_estatus_docto ced  " +
					" WHERE d.ic_epo = pe.ic_epo " +
					"   AND d.ic_pyme = p.ic_pyme " +
					"   AND d.ic_epo = cpe.ic_epo " +
					"   AND d.ic_pyme = cpe.ic_pyme " +
					"   AND d.ic_estatus_docto = ced.ic_estatus_docto " +
					"   AND d.ic_documento = ds.ic_documento " +
					"   AND d.ic_linea_credito = lcd.ic_linea_credito " +
					"   AND d.ic_epo = pe.ic_epo " +
					"   AND d.ic_moneda = m.ic_moneda " +
					"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
					"   AND d.ic_epo = e.ic_epo " +
					"   AND d.ic_estatus_docto = ed.ic_estatus_docto " +
					"   AND d.ic_epo = iep.ic_epo " +
					"   AND ds.ic_documento = s.ic_documento " +
					"   AND ds.ic_tasa = ct.ic_tasa " +
					"   AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
					"   AND iep.ic_producto_nafin = pn.ic_producto_nafin " +
					"   AND lcd.ic_if = iep.ic_if " +
					"   AND lcd.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes " +
					"   AND s.cc_acuse = c3.cc_acuse " +
					"   AND lcd.ic_if = ci.ic_if " +
					"   AND m.ic_moneda = tc.ic_moneda " +
					"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
					"                         FROM com_tipo_cambio " +
					"                        WHERE ic_moneda = m.ic_moneda) " +
					"   AND lcd.ic_if = ? " +
					"   AND e.cs_habilitado = 'S' " +
					"   AND s.cs_factoraje_con_rec = 'N' " +
					"   AND d.ic_estatus_docto IN (4, 11) " +
					//"   AND TRUNC (c3.df_fecha_hora) BETWEEN TO_DATE ('05/05/2014', 'DD/MM/YYYY') " +
					//"                                    AND TO_DATE ('06/06/2014', 'DD/MM/YYYY') ";
					condicion.toString();

			//log.debug("avisosNotificacionWS:sentenciaSQL:>>>>"+sentenciaSQL);
			List params = new ArrayList();

			//---->>>>Pasar mas bajo   psPrincipal= con.queryPrecompilado(sentenciaSQL);
			params.add(new Integer(claveIF));

			if (claveEpo!=null && !claveEpo.equals("")){
				params.add(new Integer(claveEpo));
			}

			if (clavePyme!=null && !clavePyme.equals("")){
				params.add(new Integer(clavePyme));
			}

			if(fecOperacionIni!=null && !fecOperacionIni.equals(""))
				if(fecOperacionFin!=null && !fecOperacionFin.equals("")){
					params.add(fecOperacionIni);
					params.add(fecOperacionFin);
				}

			if( (claveEpo==null || claveEpo.equals("")) &&
					(fecOperacionIni==null || fecOperacionIni.equals("")) &&
					(fecOperacionFin==null || fecOperacionFin.equals("")) ) {
				params.add(fechaActual);
				params.add(fechaActual);
			}

			//SEGUNDO QUERY
			params.add(new Integer(claveIF));

			if (claveEpo!=null && !claveEpo.equals("")){
				params.add(new Integer(claveEpo));
			}

			if (clavePyme!=null && !clavePyme.equals("")){
				params.add(new Integer(clavePyme));
			}

			if(fecOperacionIni!=null && !fecOperacionIni.equals(""))
				if(fecOperacionFin!=null && !fecOperacionFin.equals("")){
					params.add(fecOperacionIni);
					params.add(fecOperacionFin);
				}

			if( (claveEpo==null || claveEpo.equals("")) &&
					(fecOperacionIni==null || fecOperacionIni.equals("")) &&
					(fecOperacionFin==null || fecOperacionFin.equals("")) ) {
				params.add(fechaActual);
				params.add(fechaActual);
			}

			//if (monedaParametrizadaWS.equals("MN")) {
			//	params.add(new Integer(1));	//1.- Moneda Nacional
			//} else if (monedaParametrizadaWS.equals("DA")) {
			//	params.add(new Integer(54));	//54.- Dolares Americanos
			//}
			log.debug("getAvisosNotificacionDisWS: qry=" + sentenciaSQL);
			log.debug("getAvisosNotificacionDisWS: params=" + params);

			psPrincipal= con.queryPrecompilado(sentenciaSQL, params);
			rsPrincipal = psPrincipal.executeQuery();
			if(rsPrincipal!=null){
				while(rsPrincipal.next()){
					DocumentoDistIFWS documento = new DocumentoDistIFWS();
				   String tipoPago =(rsPrincipal.getString("tipoPago")==null)?"":rsPrincipal.getString("tipoPago"); //F09-2015 
					
					documento.setClaveEpo((rsPrincipal.getString("CLAVEEPO")==null)?"":rsPrincipal.getString("CLAVEEPO"));
					documento.setNombreEpo((rsPrincipal.getString("NOMBREEPO")==null)?"":rsPrincipal.getString("NOMBREEPO"));
					documento.setClaveDistribuidor((rsPrincipal.getString("CLAVEDISTRIBUIDOR")==null)?"":rsPrincipal.getString("CLAVEDISTRIBUIDOR"));
					documento.setNombreDistribuidor((rsPrincipal.getString("NOMBREDISTRIBUIDOR")==null)?"":rsPrincipal.getString("NOMBREDISTRIBUIDOR"));
					documento.setNumeroDistribuidor((rsPrincipal.getString("NUMERODISTRIBUIDOR")==null)?"":rsPrincipal.getString("NUMERODISTRIBUIDOR"));
					documento.setNumeroSiracDistribuidor((rsPrincipal.getString("NUMSIRACDISTRIBUIDOR")==null)?"":rsPrincipal.getString("NUMSIRACDISTRIBUIDOR"));
					documento.setAcuseCarga((rsPrincipal.getString("ACUSECARGA")==null)?"":rsPrincipal.getString("ACUSECARGA"));
					documento.setNumDoctoIni((rsPrincipal.getString("NUMERODOCTOINI")==null)?"":rsPrincipal.getString("NUMERODOCTOINI"));
					documento.setFechaPublicacion((rsPrincipal.getString("FECHAPUBLICACION")==null)?"":rsPrincipal.getString("FECHAPUBLICACION"));
					documento.setFechaEmision((rsPrincipal.getString("FECHAEMISION")==null)?"":rsPrincipal.getString("FECHAEMISION"));
					documento.setFechaVenciIni((rsPrincipal.getString("FECHAVENCIINI")==null)?"":rsPrincipal.getString("FECHAVENCIINI"));
					documento.setPlazoDoctoIni((rsPrincipal.getString("PLAZODOCTOINI")==null)?"":rsPrincipal.getString("PLAZODOCTOINI"));
					documento.setMontoDocto((rsPrincipal.getString("MONTODOCTO")==null)?"":rsPrincipal.getString("MONTODOCTO"));
				   if("2".equals(tipoPago)) {
						documento.setPlazoCredito((rsPrincipal.getString("PLAZOCREDITO")==null)?"":rsPrincipal.getString("PLAZOCREDITO") +" (M) ");
					}else  {
						documento.setPlazoCredito((rsPrincipal.getString("PLAZOCREDITO")==null)?"":rsPrincipal.getString("PLAZOCREDITO"));
					}
					
					documento.setPlazoDescuento((rsPrincipal.getString("PLAZODESCUENTO")==null)?"":rsPrincipal.getString("PLAZODESCUENTO"));
					documento.setPorcentajeDescuento((rsPrincipal.getString("MONTODESCUENTO")==null)?"":rsPrincipal.getString("MONTODESCUENTO"));
					documento.setMontoDescuento(  String.valueOf((Double.parseDouble(documento.getMontoDocto()))*((Double.parseDouble(documento.getPorcentajeDescuento()))/100))) ;
					documento.setClaveModPlazo((rsPrincipal.getString("CLAVEMODPLAZO")==null)?"":rsPrincipal.getString("CLAVEMODPLAZO"));
					documento.setModalidadPlazo((rsPrincipal.getString("MODALIDADPLAZO")==null)?"":rsPrincipal.getString("MODALIDADPLAZO"));
					documento.setNumDoctoFin((rsPrincipal.getString("NUMDOCTOFIN")==null)?"":rsPrincipal.getString("NUMDOCTOFIN"));
					documento.setFechaOperacion((rsPrincipal.getString("FECHAOPERACION")==null)?"":rsPrincipal.getString("FECHAOPERACION"));
					documento.setClaveMoneda((rsPrincipal.getString("CLAVEMONEDA")==null)?"":rsPrincipal.getString("CLAVEMONEDA"));
					documento.setNombreMoneda((rsPrincipal.getString("NOMBREMONEDA")==null)?"":rsPrincipal.getString("NOMBREMONEDA"));
					documento.setMontoCredito((rsPrincipal.getString("MONTOCREDITO")==null)?"":rsPrincipal.getString("MONTOCREDITO"));
					documento.setPlazoCredito((rsPrincipal.getString("PLAZOCREDITO")==null)?"":rsPrincipal.getString("PLAZOCREDITO"));
					documento.setFechaVenciCredito((rsPrincipal.getString("FECHAVENCICREDITO")==null)?"":rsPrincipal.getString("FECHAVENCICREDITO"));
					documento.setMontoInteres((rsPrincipal.getString("MONTOINTERES")==null)?"":rsPrincipal.getString("MONTOINTERES"));
					documento.setTasaInteres((rsPrincipal.getString("TASAINTERES")==null)?"":rsPrincipal.getString("TASAINTERES"));
					documento.setClaveTipoInteres((rsPrincipal.getString("CLAVETIPOINTERES")==null)?"":rsPrincipal.getString("CLAVETIPOINTERES"));
					documento.setTipoInteres((rsPrincipal.getString("TIPOINTERES")==null)?"":rsPrincipal.getString("TIPOINTERES"));
					documento.setTipoConversion((rsPrincipal.getString("TIPOCONVERSION")==null)?"":rsPrincipal.getString("TIPOCONVERSION"));
					//documento.setMontoDoctoDL((rsPrincipal.getString("MONTODOCTO")==null)?"":rsPrincipal.getString("MONTODOCTO"));
					documento.setTipoCambio((rsPrincipal.getString("TIPOCAMBIO")==null)?"":rsPrincipal.getString("TIPOCAMBIO"));
					documento.setResponPagoInteres((rsPrincipal.getString("RESPONPAGOINTERES")==null)?"":rsPrincipal.getString("RESPONPAGOINTERES"));
					documento.setTipoCobranza((rsPrincipal.getString("TIPOCOBRANZA")==null)?"":rsPrincipal.getString("TIPOCOBRANZA"));
					documento.setReferencia((rsPrincipal.getString("REFERENCIA")==null)?"":rsPrincipal.getString("REFERENCIA"));
					documento.setClaveEstatus((rsPrincipal.getString("CLAVEESTATUS")==null)?"":rsPrincipal.getString("CLAVEESTATUS"));
					documento.setNombreEstatus((rsPrincipal.getString("NOMBREESTATUS")==null)?"":rsPrincipal.getString("NOMBREESTATUS"));


					String campoAdicional1 = rsPrincipal.getString("CAMPOADICIONAL1");
					campoAdicional1 = (campoAdicional1!=null)?campoAdicional1.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";
					String campoAdicional2 = rsPrincipal.getString("CAMPOADICIONAL2");
					campoAdicional2 = (campoAdicional2!=null)?campoAdicional2.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";
					String campoAdicional3 = rsPrincipal.getString("CAMPOADICIONAL3");
					campoAdicional3 = (campoAdicional3!=null)?campoAdicional3.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";
					String campoAdicional4 = rsPrincipal.getString("CAMPOADICIONAL4");
					campoAdicional4 = (campoAdicional4!=null)?campoAdicional4.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";
					String campoAdicional5 = rsPrincipal.getString("CAMPOADICIONAL5");
					campoAdicional5 = (campoAdicional5!=null)?campoAdicional5.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", ""):"";


					documento.setCampoAdicional1(campoAdicional1);
					documento.setCampoAdicional2(campoAdicional2);
					documento.setCampoAdicional3(campoAdicional3);
					documento.setCampoAdicional4(campoAdicional4);
					documento.setCampoAdicional5(campoAdicional5);
					//documento.setValorTasa((rsPrincipal.getString("VALOR_TASA")==null)?"":rsPrincipal.getString("VALOR_TASA"));
					//documento.setMonedaLinea((rsPrincipal.getString("MONEDA_LINEA")==null)?"":rsPrincipal.getString("MONEDA_LINEA"));
					//documento.setMontoRecibir((rsPrincipal.getString("MONTORECIBIR")==null)?"":rsPrincipal.getString("MONTORECIBIR"));
					//documento.setClaveTasa((rsPrincipal.getString("CLAVETASA")==null)?"":rsPrincipal.getString("CLAVETASA"));
					if("2".equals(tipoPago)) {
						documento.setPlazo((rsPrincipal.getString("MESES_INTERESES")==null)?"":rsPrincipal.getString("MESES_INTERESES")+" (M)");//F09-2015
					}else {
						documento.setPlazo(""); 
					}
					lstDocument.add(documento);
				}
			}

			if(rsPrincipal!=null)rsPrincipal.close();
			if(psPrincipal!=null)psPrincipal.close();
			//ps.close();

			DocumentoDistIFWS[] arrDocumentos = null;
			if (lstDocument.size() > 0) {
				arrDocumentos = new DocumentoDistIFWS[lstDocument.size()];
				lstDocument.toArray(arrDocumentos);
			}

			resultadoProceso.setDocumentos(arrDocumentos);
			resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito

		} catch (Throwable t) {

			log.error("getAvisosNotificacionDisWS(Error) ", t);
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			resultadoProceso.setResumenEjecucion("Error Inesperado. Proceso Cancelado:" + t.getMessage());

		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}//finaliza  try inicial

		log.info("getAvisosNotificacionDisWS(S)");
		return resultadoProceso;
	}//cierre avisoIFWS


	/**
 * Versi�n del WS de confirmaci�n de Documentos IF Distribuidores.
 * @param claveUsuario Clave del usuario
 * @param password contrase�a
 * @param claveIF Clave del IF (ic_if)
 * @param documentos arreglo de documentos a confirmar
 * @param pkcs7 Cadena firmada en formato PKCS7
 * @param serial Serial del certificado
 */
public ProcesoIFDistWSInfo confirmacionDoctosDistIFWS (String claveUsuario,
		String password, 	String claveIF,
		DocumentoDistIFWS[] documentos, String pkcs7, String serial) {
	log.info("confirmacionDoctosDistIFWS(E)");

	ProcesoIFDistWSInfo resultadoProceso = new ProcesoIFDistWSInfo();
	List lDocumentosError = new ArrayList();
	List lDocumentosV2 = new ArrayList();
	
	int i = 0; //numero de registros procesados

	int iClaveIF = 0;
	StringBuffer erroresInesperados = new StringBuffer();  //para ir concatenando los errores Inesperadoa

	//**********************************Validaci�n de parametros:*****************************
	try {
		if (claveUsuario == null || claveUsuario.equals("") ||
				password == null || password.equals("") ||
				claveIF == null || claveIF.equals("")  ||
				documentos == null ||
				pkcs7 == null || pkcs7.equals("") ||
				serial == null || serial.equals("") ) {
			throw new Exception("Los parametros son requeridos");
		}
		iClaveIF = Integer.parseInt(claveIF);

	} catch(Exception e) {
		log.error("confirmacionDoctosDistIFWS(Error) ", e);
		// Como no entr� al ciclo, se regresan todos los documentos recibidos (si existen)
		if (documentos != null) {
			for (i=0; i < documentos.length; i++) {
				DocumentoDistIFWS doc = documentos[i];
				doc.setErrorDetalle("ERROR|Registro no procesado");
				doc.setTipoError(new Integer(1));
			}
		}
		resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
		resultadoProceso.setResumenEjecucion("ERROR|Error en la validacion de parametros: " +
				"(" + claveUsuario + "," + password + "," + claveIF +
				"," + documentos + "," + pkcs7 + "," + serial + ")" + ". Error=" + e.getMessage());
		resultadoProceso.setDocumentos(documentos);
		return resultadoProceso;
	}
	//****************************************************************************************


	if (documentos == null) {
		//No hay documentos por procesar. Termina
		resultadoProceso.setCodigoEjecucion(new Integer(1)); //Exito
		resultadoProceso.setResumenEjecucion("No hay documentos por procesar");
		return resultadoProceso;

	}

	AccesoDB con = null;
	boolean exito = true;
	//boolean error = false;
	String acuse = "";

	//PreparedStatement psParamNotasCred = null;
	PreparedStatement psRegistroSolicitud = null;
	PreparedStatement psRegistroSolicitudCC = null;
	PreparedStatement psRechazo = null;
	PreparedStatement psDoc = null;
	PreparedStatement psSolicitud = null;
	PreparedStatement psUpdate = null;
	//PreparedStatement psAmortizacion = null;


	try {
		try {
			con = new AccesoDB();
		} catch(Throwable t) {
			throw new Exception("Error al generar el objeto de acceso a BD", t);
		}
		//Validacion del usuario y Password y que sea del IF especificado
		UtilUsr utilUsr = new UtilUsr();
		List cuentasIF = null;
		try {
			cuentasIF = utilUsr.getUsuariosxAfiliado(claveIF, "I");
		} catch(Throwable t) {
			throw new Exception("Error al obtener la lista de usuarios del IF: " + claveIF + ".", t);
		}
		log.debug("confirmacionDoctosDistIFWS::Validando claveIF/usuario");

		if (!cuentasIF.contains(claveUsuario)) {
			//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
			for (i=0; i < documentos.length; i++) {
				DocumentoDistIFWS doc = documentos[i];
				doc.setErrorDetalle("ERROR|Registro no procesado");
				doc.setTipoError(new Integer(1));
			}
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|El usuario " + claveUsuario + " no existe para el IF especificado");
			resultadoProceso.setDocumentos(documentos);
			return resultadoProceso; //Termina Proceso
		}

		log.debug("confirmacionDoctosDistIFWS::Validando usuario/passwd");
		try {
			if (!utilUsr.esUsuarioValido(claveUsuario, password)) {
				//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
				for (i=0; i < documentos.length; i++) {
					DocumentoDistIFWS doc = documentos[i];
					doc.setErrorDetalle("ERROR|Registro no procesado");
					doc.setTipoError(new Integer(1));
				}
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del usuario " + claveUsuario);
				resultadoProceso.setDocumentos(documentos);
				return resultadoProceso; //Termina Proceso
			}
		} catch(Throwable t) {
			throw new Exception("Error en el componente de validacion de usuario. ", t);
		}

		try {
			con.conexionDB();
		} catch(Throwable t) {
			throw new Exception ("Error al realizar la conexion a la BD. ", t);
		}

		PreparedStatement ps = null;
		String qrySentencia = null;
		try {
			//Creo que este alter es para que funcione adecuadamente la funcion sigfechahabilxepo
			qrySentencia = "ALTER SESSION SET NLS_TERRITORY = MEXICO";
			ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();
			con.terminaTransaccion(true);
		} catch(Throwable t) {
			throw new Exception("Error al establecer la informacion de idioma en BD");
		}



		//----------------------- Definicion de queries que se ocupan dentro del ciclo ---------------

		try {
			String strSQL =
					"SELECT d.ic_epo claveepo, e.cg_razon_social AS nombreepo, " +
					"       p.ic_pyme AS clavedistribuidor, " +
					"       p.cg_razon_social AS nombredistribuidor, " +
					"       cpe.cg_num_distribuidor AS numerodistribuidor, " +
					"       p.in_numero_sirac AS numsiracdistribuidor, d.cc_acuse AS acusecarga, " +
					"       d.ig_numero_docto AS numerodoctoini, " +
					"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fechapublicacion, " +
					"       TO_CHAR (d.df_fecha_emision, 'dd/mm/yyyy') AS fechaemision, " +
					"       TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavenciini, " +
					"       d.ig_plazo_docto AS plazodoctoini, d.fn_monto AS montodocto, " +
					"       d.ig_plazo_descuento AS plazodescuento, " +
					"       NVL (d.fn_porc_descuento, 0) AS montodescuento, " +
					"       tf.ic_tipo_financiamiento AS clavemodplazo, " +
					"       tf.cd_descripcion AS modalidadplazo, d.ic_documento AS numdoctofin, " +
					"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fechaoperacion, " +
					"       d.ic_moneda AS clavemoneda, m.cd_nombre AS nombremoneda, " +
					"       d.ig_plazo_credito AS plazocredito, " +
					"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fechavencicredito, " +
					"       ds.fn_importe_interes AS montointeres, ds.fn_valor_tasa AS tasainteres, " +
					"       tci.ic_tipo_cobro_interes AS clavetipointeres, " +
					"       tci.cd_descripcion AS tipointeres, " +
					"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"               'N', 'Sin conversion', " +
					"               'P', 'Peso-Dolar', " +
					"               '' " +
					"              ) AS tipoconversion, " +
					"       d.fn_monto AS montocredito, " +
					"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"               'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), " +
					"               '1' " +
					"              ) AS tipocambio, " +
					"       NVL (pe.cg_responsable_interes, " +
					"            pn.cg_responsable_interes " +
					"           ) AS responpagointeres, " +
					"       NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) AS tipocobranza, " +
					"       ct.cd_nombre || '' || ds.cg_rel_mat || ' ' " +
					"       || ds.fn_puntos AS referencia, " +
					"       cg_campo1 AS campoadicional1, cg_campo2 AS campoadicional2, " +
					"       cg_campo3 AS campoadicional3, cg_campo4 AS campoadicional4, " +
					"       cg_campo5 AS campoadicional5, " +
					"       DECODE (ds.cg_rel_mat, " +
					"               '+', ds.fn_valor_tasa + ds.fn_puntos, " +
					"               '-', ds.fn_valor_tasa - ds.fn_puntos, " +
					"               '*', ds.fn_valor_tasa * ds.fn_puntos, " +
					"               '/', ds.fn_valor_tasa / ds.fn_puntos, " +
					"               0 " +
					"              ) AS valor_tasa, " +
					"       lcd.ic_moneda AS moneda_linea, ds.fn_importe_recibir AS montorecibir, " +
					"       ds.ic_tasa AS clavetasa, " +
					"       NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) AS tipoconversion2, " +
					"       d.fn_tipo_cambio AS tipocambio2, ct_referencia, 'D' as tipocredito, d.ic_estatus_docto estatusDoctoActual " +
					"  FROM dis_documento d, " +
					"       comcat_pyme p, " +
					"       comrel_producto_epo pe, " +
					"       comcat_tipo_financiamiento tf, " +
					"       comrel_pyme_epo cpe, " +
					"       comcat_producto_nafin pn, " +
					"       comcat_moneda m, " +
					"       com_tipo_cambio tc, " +
					"       dis_linea_credito_dm lcd, " +
					"       comcat_tipo_cobro_interes tci, " +
					"       comrel_if_epo_x_producto iep, " +
					"       comcat_tasa ct, " +
					"       comcat_epo e, " +
					"       dis_docto_seleccionado ds " +
					" WHERE d.ic_epo = pe.ic_epo " +
					"   AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
					"   AND tf.ic_tipo_financiamiento = d.ic_tipo_financiamiento " +
					"   AND pn.ic_producto_nafin = 4 " +
					"   AND p.ic_pyme = d.ic_pyme " +
					"   AND d.ic_documento = ds.ic_documento " +
					"   AND d.ic_epo = pe.ic_epo " +
					"   AND cpe.ic_epo = d.ic_epo " +
					"   AND cpe.ic_pyme = d.ic_pyme " +
					"   AND lcd.ic_moneda = m.ic_moneda " +
					"   AND d.ic_moneda = tc.ic_moneda " +
					"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
					"                         FROM com_tipo_cambio " +
					"                        WHERE ic_moneda = d.ic_moneda) " +
					"   AND d.ic_linea_credito_dm = lcd.ic_linea_credito_dm " +
					"   AND lcd.ic_if = iep.ic_if " +
					"   AND tci.ic_tipo_cobro_interes = lcd.ic_tipo_cobro_interes " +
					"   AND d.ic_estatus_docto = 24 " +
					"   AND iep.ic_epo = d.ic_epo " +
					"   AND iep.ic_producto_nafin = pn.ic_producto_nafin " +
					"   AND ds.ic_tasa = ct.ic_tasa " +
					"   AND d.ic_epo = e.ic_epo " +
					"   AND lcd.ic_if = ? " +
					"   AND lcd.ic_estatus_linea = 12 " +
					"   AND d.ic_documento = ? " +
					"   AND d.ic_documento NOT IN (SELECT ic_documento " +
					"                                FROM dis_solicitud) " +
					"UNION ALL " +
					"SELECT d.ic_epo claveepo, e.cg_razon_social AS nombreepo, " +
					"       p.ic_pyme AS clavedistribuidor, " +
					"       p.cg_razon_social AS nombredistribuidor, " +
					"       cpe.cg_num_distribuidor AS numerodistribuidor, " +
					"       p.in_numero_sirac AS numsiracdistribuidor, d.cc_acuse AS acusecarga, " +
					"       d.ig_numero_docto AS numerodoctoini, " +
					"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fechapublicacion, " +
					"       TO_CHAR (d.df_fecha_emision, 'dd/mm/yyyy') AS fechaemision, " +
					"       TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fechavenciini, " +
					"       d.ig_plazo_docto AS plazodoctoini, d.fn_monto AS montodocto, " +
					"       d.ig_plazo_descuento AS plazodescuento, " +
					"       NVL (d.fn_porc_descuento, 0) AS montodescuento, " +
					"       tf.ic_tipo_financiamiento AS clavemodplazo, " +
					"       tf.cd_descripcion AS modalidadplazo, d.ic_documento AS numdoctofin, " +
					"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fechaoperacion, " +
					"       d.ic_moneda AS clavemoneda, m.cd_nombre AS nombremoneda, " +
					"       d.ig_plazo_credito AS plazocredito, " +
					"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fechavencicredito, " +
					"       ds.fn_importe_interes AS montointeres, ds.fn_valor_tasa AS tasainteres, " +
					"       tci.ic_tipo_cobro_interes AS clavetipointeres, " +
					"       tci.cd_descripcion AS tipointeres, " +
					"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"               'N', 'Sin conversion', " +
					"               'P', 'Peso-Dolar', " +
					"               '' " +
					"              ) AS tipoconversion, " +
					"       d.fn_monto AS montocredito, " +
					"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"               'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), " +
					"               '1' " +
					"              ) AS tipocambio, " +
					"       NVL (pe.cg_responsable_interes, " +
					"            pn.cg_responsable_interes " +
					"           ) AS responpagointeres, " +
					"       NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) AS tipocobranza, " +
					"       ct.cd_nombre || '' || ds.cg_rel_mat || ' ' " +
					"       || ds.fn_puntos AS referencia, " +
					"       cg_campo1 AS campoadicional1, cg_campo2 AS campoadicional2, " +
					"       cg_campo3 AS campoadicional3, cg_campo4 AS campoadicional4, " +
					"       cg_campo5 AS campoadicional5, " +
					"       DECODE (ds.cg_rel_mat, " +
					"               '+', ds.fn_valor_tasa + ds.fn_puntos, " +
					"               '-', ds.fn_valor_tasa - ds.fn_puntos, " +
					"               '*', ds.fn_valor_tasa * ds.fn_puntos, " +
					"               '/', ds.fn_valor_tasa / ds.fn_puntos, " +
					"               0 " +
					"              ) AS valor_tasa, " +
					"       lc.ic_moneda AS moneda_linea, ds.fn_importe_recibir AS montorecibir, " +
					"       ds.ic_tasa AS clavetasa, " +
					"       NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) AS tipoconversion2, " +
					"       d.fn_tipo_cambio AS tipocambio2, ct_referencia, 'C' as tipocredito, d.ic_estatus_docto estatusDoctoActual  " +
					"  FROM dis_documento d, " +
					"       comcat_pyme p, " +
					"       comrel_producto_epo pe, " +
					"       comcat_producto_nafin pn, " +
					"       comcat_moneda m, " +
					"       com_tipo_cambio tc, " +
					"       com_linea_credito lc, " +
					"       comcat_tipo_cobro_interes tci, " +
					"       comcat_tipo_financiamiento tf, " +
					"       comrel_if_epo_x_producto iep, " +
					"       comrel_pyme_epo cpe, " +
					"       comcat_tasa ct, " +
					"       comcat_epo e, " +
					"       dis_docto_seleccionado ds " +
					" WHERE d.ic_epo = pe.ic_epo " +
					"   AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
					"   AND tf.ic_tipo_financiamiento = d.ic_tipo_financiamiento " +
					"   AND cpe.ic_epo = d.ic_epo " +
					"   AND cpe.ic_pyme = d.ic_pyme " +
					"   AND pn.ic_producto_nafin = 4 " +
					"   AND p.ic_pyme = d.ic_pyme " +
					"   AND d.ic_documento = ds.ic_documento " +
					"   AND d.ic_epo = pe.ic_epo " +
					"   AND lc.ic_moneda = m.ic_moneda " +
					"   AND d.ic_moneda = tc.ic_moneda " +
					"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
					"                         FROM com_tipo_cambio " +
					"                        WHERE ic_moneda = m.ic_moneda) " +
					"   AND d.ic_linea_credito = lc.ic_linea_credito " +
					"   AND lc.ic_if = iep.ic_if " +
					"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes " +
					"   AND d.ic_estatus_docto = 24 " +
					"   AND iep.ic_epo = d.ic_epo " +
					"   AND iep.ic_producto_nafin = pn.ic_producto_nafin " +
					"   AND ds.ic_tasa = ct.ic_tasa " +
					"   AND d.ic_epo = e.ic_epo " +
					"   AND lc.ic_if = ? " +
					"   AND lc.ic_estatus_linea = 12 " +
					"   AND lc.cs_factoraje_con_rec = 'N' " +
					"   AND d.ic_documento = ? " +
					"   AND d.ic_documento NOT IN (SELECT ic_documento " +
					"                                FROM dis_solicitud) ";


			psDoc = con.queryPrecompilado(strSQL);
		} catch (Throwable t) {
			throw new Exception("Error al precompilar la consulta de documentos.",t);
		}

		try {
			String querySolicitud =
					" SELECT COUNT(*) as numRegistros " +
					" FROM dis_solicitud "+
					" WHERE ic_documento = ? ";
			psSolicitud = con.queryPrecompilado(querySolicitud);
		} catch(Throwable t) {
			throw new Exception("Error al precompilar la consulta de cantidad de solicitudes.",t);
		}

		try {
			String strSQLRechazo =
					" SELECT cg_descripcion AS causaRechazo " +
					" FROM com_error_if     " +
					" WHERE cc_error_if = ? " +
					"   AND ic_if       = ? ";

			psRechazo = con.queryPrecompilado(strSQLRechazo);
		} catch(Throwable t) {
			throw new Exception("Error al precompilar la consulta de errores de IF.",t);
		}

		try{
			String queryUpdate =
					" UPDATE dis_documento " +
					" SET ic_estatus_docto = ? " +
					" WHERE ic_documento = ? ";
			psUpdate = con.queryPrecompilado(queryUpdate);
		} catch(Throwable t) {
			throw new Exception("Error al precompilar la actualizacion del cambio de estatus del documento.",t);
		}

		try {
			String queryRegistroSolicitudDM =
					" insert into dis_solicitud" +
					" (ic_documento,ic_estatus_solic,cc_acuse,ic_clase_docto"+
					" ,cg_rmif,in_stif,df_v_credito,df_ppc,df_ppi,in_dia_pago,ic_tasaif,ic_tipo_credito, ic_if, df_fecha_solicitud, CG_RESPONSABLE_INTERES, CG_TIPO_COBRANZA, IC_TIPO_COBRO_INTERES)"+
					" select D.ic_documento, ?, ?, PD.ic_clase_docto" +
					" ,'+',0,D.df_fecha_venc_credito,D.df_fecha_venc_credito,D.df_fecha_venc_credito,to_number(to_char(D.df_fecha_venc_credito,'dd')),DS.ic_tasa" +
					" ,decode(LC.ic_moneda,1,211,54,221), LC.ic_if, sysdate,"+
					" NVL (pe.cg_responsable_interes, pn.cg_responsable_interes), " +
					" NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), " +
					" pe.ic_tipo_cobro_interes " +
					" from dis_documento D, comrel_producto_docto PD, dis_docto_seleccionado DS" +
					" ,dis_linea_credito_dm LC, comrel_producto_epo pe, comcat_producto_nafin pn " +
					" where D.ic_epo = PD.ic_epo" +
					" AND d.ic_epo = pe.ic_epo " +
					" AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
					" and D.ic_producto_nafin = PD.ic_producto_nafin"+
					" and D.ic_documento = DS.ic_documento"+
					" and D.ic_linea_credito_dm = LC.ic_linea_credito_dm "+
					" and pn.ic_producto_nafin = 4 " +
               " and D.ic_documento = ? ";

			String queryRegistroSolicitudCC =
					" insert into dis_solicitud" +
					" (ic_documento,ic_estatus_solic,cc_acuse,ic_clase_docto"+
					" ,cg_rmif,in_stif,df_v_credito,df_ppc,df_ppi,in_dia_pago,ic_tasaif,ic_tipo_credito, ic_if, df_fecha_solicitud, CG_RESPONSABLE_INTERES, CG_TIPO_COBRANZA, IC_TIPO_COBRO_INTERES)"+
					" select D.ic_documento, ?, ?, PD.ic_clase_docto" +
					" ,'+',0,D.df_fecha_venc_credito,D.df_fecha_venc_credito,D.df_fecha_venc_credito,to_number(to_char(D.df_fecha_venc_credito,'dd')),DS.ic_tasa" +
					" ,decode(LC.ic_moneda,1,211,54,221), LC.ic_if, sysdate,"+
					" NVL (pe.cg_responsable_interes, pn.cg_responsable_interes), " +
					" NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), " +
					" pe.ic_tipo_cobro_interes " +
					" from dis_documento D, comrel_producto_docto PD, dis_docto_seleccionado DS, comrel_producto_epo pe, comcat_producto_nafin pn " +
					" ,com_linea_credito LC"+
					" where D.ic_epo = PD.ic_epo" +
					" and d.ic_epo = pe.ic_epo " +
					" and pe.ic_producto_nafin = pn.ic_producto_nafin " +
					" and D.ic_producto_nafin = PD.ic_producto_nafin"+
					" and D.ic_documento = DS.ic_documento"+
					" and D.ic_linea_credito = LC.ic_linea_credito"+
					" and pn.ic_producto_nafin = 4 " +
					" and D.ic_documento = ? ";


			psRegistroSolicitud = con.queryPrecompilado(queryRegistroSolicitudDM);
			psRegistroSolicitudCC = con.queryPrecompilado(queryRegistroSolicitudCC);
		} catch(Throwable t) {
			throw new Exception("Error al precompilar la insercion de la solicitud.",t);
		}




		//----------------------- ----------------------------------------------- ---------------


		//-------------------------- VALIDACION De FIRMA PKCS7 ----------------------------

		log.info("confirmacionDoctosDistIFWS()::Validando que el certificado sea del usuario especificado");
		PreparedStatement psSerial = null;
		String strSerial = "";

		try {
			String qryTraeSerial =
					" SELECT dn_user " +
					" FROM users_seguridata "+
					" WHERE UPPER(trim(table_uid)) = UPPER(?) ";
			psSerial = con.queryPrecompilado(qryTraeSerial);
			psSerial.setString(1, claveUsuario);
			ResultSet rsSerial = psSerial.executeQuery();

			if(rsSerial.next()) {
				strSerial = rsSerial.getString(1).trim();
			}
			rsSerial.close();
			psSerial.close();
		} catch(Throwable t) {
			throw new Exception("Error al intentar verificar el numero de serie del certificado.",t);
		} finally {
			con.terminaTransaccion(true); //Se realiza un commit dado que se accesa una tabla remota
		}

		if (!serial.equals(strSerial)) {
			//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
			for (i=0; i < documentos.length; i++) {
				DocumentoDistIFWS doc = documentos[i];
				doc.setErrorDetalle("ERROR|Registro no procesado");
				doc.setTipoError(new Integer(1));
			}
			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|El certificado con numero de serie " + serial + " no corresponde al usuario " + claveUsuario);
			resultadoProceso.setDocumentos(documentos);
			return resultadoProceso;
		}
		String cadenaOriginalFirmada = "";

		try {
			cadenaOriginalFirmada = this.getCadenaFirmarDistWS(documentos);
		} catch(AppException e) {
			//Si es AppException significa que es un error normal de aplicaci�n al intentar obetener la cadena Original
			for (i=0; i < documentos.length; i++) {
				DocumentoDistIFWS doc = documentos[i];
				doc.setErrorDetalle("ERROR|Registro no procesado");
				doc.setTipoError(new Integer(1));
			}

			resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
			resultadoProceso.setResumenEjecucion("ERROR|Error al intentar obtener la cadena original antes de la firma." + e.getMessage() );
			resultadoProceso.setDocumentos(documentos);
			return resultadoProceso;
		}catch(Throwable t) {
			throw new Exception("Error al intentar obtener la cadena original antes de la firma.",t);
		}
		netropology.utilerias.Seguridad s = null;

		try {
			s = new netropology.utilerias.Seguridad();
		} catch(Throwable t) {
			throw new Exception("Error al inicializar el componente de Seguridad.",t);
		}
		char getReceipt = 'Y';
		String receipt = "";


		log.info("confirmacionDoctosIFWS()::Autenticando Firma Digital");
		boolean autenticarMensaje = false;
		Acuse acuseA = new Acuse(3,"4","com",con);
		acuse = acuseA.toString();
		try {
			autenticarMensaje = s.autenticar(acuse.toString(), serial, pkcs7, cadenaOriginalFirmada, getReceipt);
			if (autenticarMensaje) {
				receipt = s.getAcuse();
			} else {
				log.info("Error en la autentificaci�n del Mensaje :: "+ s.mostrarError());
				//Como aun no entra al ciclo, no hay ningun docto procesado y por lo tanto se regresan todos los que se recibieron
				for (i=0; i < documentos.length; i++) {
					DocumentoDistIFWS doc = documentos[i];
					doc.setErrorDetalle("ERROR|Registro no procesado");
					doc.setTipoError(new Integer(1));
				}
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //Error del proceso
				resultadoProceso.setResumenEjecucion("ERROR|Fallo la autenticacion del mensaje");
				resultadoProceso.setDocumentos(documentos);
				//BorraAcuse(acuse,con); //Borra el Acuse y realiza commit
				return resultadoProceso; //Termina Proceso
			}
		} catch(Throwable t) {
			throw new Exception("Error en el proceso de autenticacion del mensaje.",t);
		}

		//Se realiza la creacion de un acuse vacio y se inserta definitivamente (GeneraAcuse da commit)
		try {
			//acuse = this.GeneraAcuse(claveIF, "ws_confirmacion_" + claveUsuario, con);

			insertaAcuse(acuse,claveUsuario,receipt,con);
			//la autenticacion del mensaje requiere este numero de acuse
		} catch(Throwable t) {
			throw new Exception("Error al generar el acuse de la operacion.",t);
		}

		// Una vez que se entra al ciclo, aun los errores inesperados no interrumpen
		// la ejecuci�n del procesos...
		int numOperados = 0;
		//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
		//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv Inicio de Ciclo vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
		//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
		for (i=0; i < documentos.length; i++) {
			DocumentoDistIFWS doc = null;
			StringBuffer detalleError = null;
			String folio="";
			try { //Como se permiten cargas parciales, se debe cachar cualquier error y continuar con el siguiente registro
				doc = documentos[i];
				log.debug("confirmacionDoctosDistIFWS():: Documento Recibido[" + i + "]=" + doc);
				detalleError = new StringBuffer();

				//psParamNotasCred.clearParameters();
				psRegistroSolicitud.clearParameters();
				psUpdate.clearParameters();
				psRechazo.clearParameters();
				psSolicitud.clearParameters();
				psDoc.clearParameters();
				//psAmortizacion.clearParameters();

				//************************Validaci�n de atributos de documentos:**************************
				long lClaveDocumento = 0;
				try {
					lClaveDocumento = Long.parseLong(doc.getClaveDocumento());
				} catch(NumberFormatException e) {
					detalleError.append("El valor de la clave del documento no es un numero valido\n");
				}


				int iClaveEstatusDoctoOriginal = 0;		//En esta variable se guarda el estatus que tiene actualmente el documento en BD.
				String tipoCredito = "";
				String claveEpo = "";
				boolean hayFondeoPropio = false;

				//Valida la existencia de la clave del documento (ic_documento)
				ResultSet rs = null;
				try {
					psDoc.setInt(1, iClaveIF);
					psDoc.setLong(2, lClaveDocumento);
					psDoc.setInt(3, iClaveIF);
					psDoc.setLong(4, lClaveDocumento);
					rs = psDoc.executeQuery();
					if (rs.next()) {
						iClaveEstatusDoctoOriginal = rs.getInt("estatusDoctoActual");
						tipoCredito = rs.getString("tipocredito");

					} else {
						detalleError.append("El valor de la clave del documento no existe o no pertenece al intermediario\n");
					}
					rs.close();
				} catch(Throwable t) {
					throw new Exception("Error al obtener los datos del documento.",t);
				}

				//Los documentos que pueden ser cambiados de estatus en este proceso son los que actualmente tengan 24 (En proceso de autorizacion IF)
				if (iClaveEstatusDoctoOriginal != 24) {
					detalleError.append("El documento especificado no se encuentra en estatus " +
							" en proceso de Autorizacion IF. Estatus Actual=" + iClaveEstatusDoctoOriginal );
				}

				int iClaveEstatusAsignar = 0;
				try {
					iClaveEstatusAsignar = Integer.parseInt(doc.getClaveEstatus());
				} catch(NumberFormatException e) {
					detalleError.append("El valor de la clave del estatus no es un numero valido");
				}
				//Bancomer va a mandar documentos de 24 a 24 :-) porque as� est� su aplicacion
				//Se van a ignorar...
				if (iClaveEstatusDoctoOriginal == 24 && iClaveEstatusAsignar != 2 && iClaveEstatusAsignar != 4 && iClaveEstatusAsignar != 24) {
					detalleError.append("El valor de la Clave de Estatus a asignar no es valido. " +
							"Valores validos: 2 (Rechazado), 4 (Operado), 24 (En proceso de autorizacion IF) ");
				}

				String claveRechazo = doc.getClaveRechazo();
				String causaRechazo = "";

				if (iClaveEstatusAsignar == 2 && (claveRechazo == null || claveRechazo.trim().equals("")) ) {
					//Estatus 2 en realidad es Negociable, pero en este proceso significa que el IF lo rechazo
					// y por lo tanto se requiere la clave de rechazo
					detalleError.append("El valor de la Clave de Rechazo es obligatorio si el estatus a asignar es 2 (Rechazado). ");
				} else if (iClaveEstatusAsignar == 2 && claveRechazo.length() > 8 ) {
					detalleError.append("El valor de la Clave de Rechazo no es valido. ");
				} else if (iClaveEstatusAsignar == 2 && claveRechazo.length() <= 8 ) {

					try {

						System.out.println("claveRechazo ==== "+claveRechazo);
						System.out.println("claveIF ==== "+claveIF);
						psRechazo.setString(1, claveRechazo);
						psRechazo.setInt(2, Integer.parseInt(claveIF));
						rs = psRechazo.executeQuery();

						if (rs.next()) {
							causaRechazo = rs.getString("causaRechazo");
						} else {
							detalleError.append("El valor de la Clave de Rechazo no es una clave existente. ");
						}
						rs.close();

					} catch(NumberFormatException e) {
						detalleError.append("El formato de la Clave de Rechazo no es valido\n");
					} catch(Throwable t) {
						throw new Exception("Error al consultar la clave de rechazo.",t);
					}

				}

				if (detalleError.length()>0) { //Si hay Errores
					doc.setErrorDetalle("ERROR|" + detalleError.toString());
					doc.setTipoError(new Integer(1));
					lDocumentosError.add(doc);
					continue;		//Continua con el siguiente Documento
				} else {
					//error = false;
					doc.setErrorDetalle("");
					doc.setTipoError(new Integer(0));
				}

				//if (!error) {	//Si no hay error de validaci�n de los valores de atributos
				//codigo para procesar el registro
				if (iClaveEstatusAsignar == 2) {	//Se regresa a negociable (Rechazo por parte del IF)
					try {
						String query =

								" INSERT INTO DIS_CAMBIO_ESTATUS ( " +
								" dc_fecha_cambio, ic_documento, " +
								" ic_cambio_estatus, ct_cambio_motivo) " +
								" VALUES (sysdate,?,?,?)";

						PreparedStatement psCambioEstatus = con.queryPrecompilado(query);
						psCambioEstatus.setLong(1,lClaveDocumento);

						int cambioEstatus = 0;
						/*if (iClaveEstatusDoctoOriginal == 3) { //Si pasa de Seleccionado Pyme a Negociable
							cambioEstatus = 2;
						} else */
						if (iClaveEstatusDoctoOriginal == 24) {	//Si pasa de En proceso de Autorizacion IF a Negociable
							cambioEstatus = 23;
						}

						//Registra el cambio de estatus del Documento
						psCambioEstatus.setInt(2, cambioEstatus );
						psCambioEstatus.setString(3, causaRechazo );
						//psCambioEstatus.setString(4, claveUsuario );

						psCambioEstatus.executeUpdate();
						psCambioEstatus.close();

					} catch(Throwable t) {
						throw new Exception("Error al registrar el cambio de estatus del documento en bitacora.",t);
					}
					try {
						// Modifica el estatus del documento
						psUpdate.setInt(1, 2);	//Negociable
						psUpdate.setLong(2, lClaveDocumento);
						psUpdate.executeUpdate();
					} catch(Throwable t) {
						throw new Exception("Error al realizar el cambio de estatus del documento.",t);
					}
					try {
						//Dado que se permiten cargas parciales, se realiza el commit de
						//la transaccion x registro
						con.terminaTransaccion(true);
					} catch(Throwable t) {
						throw new Exception("Error al realizar el commit de los cambios del documento.",t);
					}

				} else if (iClaveEstatusAsignar == 24) {  //En proceso de Autorizacion IF
					//Se ignora debido a que los documentos ya estan en estatus 24...
					/*try {
						// Modifica el estatus del documento
						psUpdate.setInt(1, 24); //En proceso de Autorizacion IF
						psUpdate.setLong(2,lClaveDocumento);
						psUpdate.executeUpdate();
					} catch(Throwable t) {
						throw new Exception("Error al realizar el cambio de estatus del documento.",t);
					}
					try {
						//Dado que se permiten cargas parciales, se realiza el commit de
						//la transaccion x registro
						con.terminaTransaccion(true);
					} catch(Throwable t) {
						throw new Exception("Error al realizar el commit de los cambios del documento.",t);
					}*/
				} else if (iClaveEstatusAsignar == 4) { //Operada
					numOperados++;
					int estatusSolic = 0;
					try {

						qrySentencia = 	" select count(*) "+
		                        			" from comrel_producto_docto PD"+
													" ,dis_documento D"+
                                       " where PD.ic_epo = D.ic_epo"+
                                       " and PD.ic_producto_nafin = 4"+
                                       " and D.ic_documento = ? ";
						 PreparedStatement psIn = con.queryPrecompilado(qrySentencia);
						 psIn.setLong(1, lClaveDocumento);
						 ResultSet rsIn = psIn.executeQuery();
						 int aux = 0;
						 if(rsIn.next()){
							aux = rsIn.getInt(1);
						 }
						 rsIn.close();
						 psIn.close();
						 con.cierraStatement();
						 if(aux==0){
							doc.setErrorDetalle("ERROR|La EPO no tiene definido el tipo de documento para este producto");
							doc.setTipoError(new Integer(1));
							lDocumentosError.add(doc);
							continue; //Continua con el siguiente documento
						 }

					} catch(Throwable t) {
						throw new Exception("Error al verificar la clase de documento.",t);
					}


					try {
						hayFondeoPropio = hayFondeoPropio(claveIF,claveEpo);
					} catch(Throwable t) {
						throw new Exception("Error al obtener parametrizacion de Fondeo Propio.",t);
					}


					if (hayFondeoPropio) {
						estatusSolic = 10; //Operada con fondeo propio
					} else {
						estatusSolic = 1;	//Seleccionado IF
					}

					
					//Verifica que la Solicitud no exista previamente

					ResultSet rsSolic = null;
					boolean existeSolicitud = false;
					try {
						psSolicitud.setLong(1,lClaveDocumento);
						rsSolic = psSolicitud.executeQuery();
						rsSolic.next();
						existeSolicitud = (rsSolic.getInt("numRegistros") > 0)?true:false;
					} catch(Throwable t) {
						throw new Exception("Error al realizar la verificacion de la existencia de la solicitud.",t);
					} finally {
						rsSolic.close();
					}

					if (existeSolicitud) {
						doc.setErrorDetalle("ERROR|El documento ya tiene registrada una solicitud de descuento previa");
						doc.setTipoError(new Integer(1));
						lDocumentosError.add(doc);
						continue; //Continua con el siguiente documento
					}

					log.debug("confirmacionDoctosDistIFWS():: Validaciones OK. Se prepara la generacion de la solicitud");

					try {
						log.debug("estatusSolic = "+ estatusSolic);
						log.debug("acuse.toString() = "+ acuse.toString());
						log.debug("lClaveDocumento = "+ lClaveDocumento);

						if("D".equals(tipoCredito)){
							psRegistroSolicitud.setInt(1, estatusSolic);
							psRegistroSolicitud.setString(2, acuse.toString());
							psRegistroSolicitud.setLong(3,lClaveDocumento);
							psRegistroSolicitud.executeUpdate();
						}


						if("C".equals(tipoCredito)){
							psRegistroSolicitudCC.setInt(1, estatusSolic);
							psRegistroSolicitudCC.setString(2, acuse.toString());
							psRegistroSolicitudCC.setLong(3,lClaveDocumento);
							psRegistroSolicitudCC.executeUpdate();
						}


					} catch(Throwable t) {
						throw new Exception("Error al registrar la solicitud " + folio + " en BD.",t);
					}
					log.debug("confirmacionDoctosDistIFWS():: Solicitud: " + folio + " insertada");

					/*
					try {
						// Inserci�n de la amortizacion en com_amortizacion
						psAmortizacion.setString(1, folio);
						psAmortizacion.setDouble(2, Double.parseDouble(importeRecibir));
						psAmortizacion.setString(3, fechaVencimiento);
						psAmortizacion.executeUpdate();
					} catch(Throwable t) {
						throw new Exception("Error al registrar la amortizacion.",t);
					}
					*/

					log.debug("confirmacionDoctosDistIFWS():: Amortizacion:  insertada");
					try {
						//Actualizamos al documento con el estatus Operada (4)
						// Modifica el estatus del documento
						psUpdate.setInt(1, 4);	//Documento Operado
						psUpdate.setLong(2,lClaveDocumento);
						psUpdate.executeUpdate();
					} catch(Throwable t) {
						throw new Exception("Error al realizar el cambio de estatus (a operado) del documento.",t);
					}

					log.debug("confirmacionDoctosDistIFWS():: Estatus del documento actualizado");
					/*
					PreparedStatement psNC = null;
					try {
						if(bOperaNotasCredito) {
							//Actualiza las Notas de credito asociadas a la solicitud procesada
							qrySentencia =
									" UPDATE COM_DOCUMENTO SET ic_estatus_docto = ? " +
									" WHERE ic_docto_asociado = ? ";
							psNC = con.queryPrecompilado(qrySentencia);
							psNC.setInt(1,4);	//Documento operado
							psNC.setLong(2,lClaveDocumento);
							psNC.executeUpdate();
							log.debug("confirmacionDoctosDistIFWS():: Notas de credito asociadas actualizadas");
						} //Opera Notas de credito
					}catch(Throwable t) {
						con.terminaTransaccion(false);
						throw new Exception("Error al actualizar las notas de credito asociadas a la solicitud procesada", t);
					} finally {
						if(psNC != null)  {
						psNC.close();
						}
					}
					*/
//-----------------------------------------------------------------------------------------------------------
					//Debido a que hay cargas parciales, aquellos registros que lleguen hasta aqui
					//deben ser insertados definitivamente. Por ello se realiza el commit por documento valido
					con.terminaTransaccion(true);

				} //Fin de procesamiento estatus de documento 4 (operado)
			} catch(Throwable t) {
				//Error inesperado en el procesamiento del documento.
				//Debe continuar con el siguiente documento.
				doc.setErrorDetalle("ERROR INESPERADO|" + detalleError.toString() + "\nDetalle: " + t.getMessage());
				doc.setTipoError(new Integer(2));
				lDocumentosError.add(doc);
				erroresInesperados.append(doc.getErrorDetalle()+"\r\n");
				log.error("Hubo errores Inesperados "+erroresInesperados);

				log.error("confirmacionDoctosDistIFWS(Error) ", t);
				try {
					if (con.hayConexionAbierta()) {
						//en caso de error inesperado realiza un rollback.
						con.terminaTransaccion(false);
					}
				} catch(Throwable t1) {
					log.error("confirmacionDoctosDistIFWS(Error) ", t1);
				}
				continue;
			} finally{
				
				//if(banderaV2){
					if(doc.getErrorDetalle().equals("")){
						//doc.setFolio(folio);
						doc.setAcuseConfirmacion(acuse);
						SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
						java.util.Date fechaDate = new java.util.Date();
						//System.out.println("\n\naqui pase.\n\n");
						String fecha=formateador.format(fechaDate);
						doc.setFechaAltaSolicitud(fecha);
					}
					lDocumentosV2.add(doc);

				//}
				
			}//fin del try-catch del ciclo
		} //fin for
		//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		//---------------------------------- Fin del ciclo -----------------------------------
		//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

		//Se actualizan los datos del acuse con base en los registros insertados en com_solicitud (Solicitudes generadas)
		//Si no hay registros o existe un error al actualizar el acuse, se elimina el acuse
		boolean acuseNoActualizado = true;
		if (numOperados > 0) {
			try {
				acuseNoActualizado = this.ActualizaAcuse(acuse, receipt,  con);		//ActualizaAcuse realiza commit   (No lanza excepci�n solo el valor de retorno ser�a false)
			} catch (Throwable t) {
				throw new Exception("Error al actualizar los datos del acuse", t);
			}
		} else {
			acuseNoActualizado = true;
		}

		if (acuseNoActualizado && !acuse.equals("") ) {
			//Si el acuse no se actualizo porque No se registraron solicitudes (no se operaron documentos)
			//, se elimina el acuse
			BorraAcuse(acuse,con); //BorraAcuse realiza commit
			acuse = null;	//Dado que el acuse se borra se establece en Null, para identificar que finalmente no hubo acuse.
		}

		exito=true;

		DocumentoDistIFWS[] arrDocumentosError = null;
		if (lDocumentosError.size() > 0) {
			arrDocumentosError = new DocumentoDistIFWS[lDocumentosError.size()];
			lDocumentosError.toArray(arrDocumentosError);
		}

		resultadoProceso.setDocumentos(arrDocumentosError);

		//NOTA: Que el codigo de ejecuci�n sea 1, no significa que no haya errores en los documentos
		//Ver documentaci�n del M�todo
		resultadoProceso.setCodigoEjecucion(new Integer(1)); //1=Exito

		String datosAcuse = "";
		try {
			if (acuse != null) {	//Si hay acuse... porque hubo registros procesados
				String qryAcuse =
						" SELECT in_monto_mn,in_monto_int_mn," +
						" in_monto_dscto_mn,in_monto_dl, " +
						" in_monto_int_dl,in_monto_dscto_dl " +
						" FROM com_acuse3 " +
						" WHERE cc_acuse = ? ";
				PreparedStatement psAcuse = con.queryPrecompilado(qryAcuse);
				psAcuse.setString(1, acuse);
				ResultSet rsAcuse = psAcuse.executeQuery();
				if (rsAcuse.next()) {
					String montoTotalMN = rsAcuse.getString("in_monto_mn");
					String montoInteresTotalMN = rsAcuse.getString("in_monto_int_mn");
					//String montoDescuentoTotalMN = rsAcuse.getString("in_monto_dscto_mn");
					String montoTotalDL = rsAcuse.getString("in_monto_dl");
					String montoInteresTotalDL = rsAcuse.getString("in_monto_int_dl");
					//String montoDescuentoTotalDL = rsAcuse.getString("in_monto_dscto_dl");

					datosAcuse =
							"Acuse: " + acuse + "\n" +
							"Numero de Documentos recibidos: " + documentos.length + "\n" +
							"Documentos con Error: " + lDocumentosError.size() + "\n" +
							"Monto MXP: " + montoTotalMN + "\n" +
							"Monto Interes MXP: " + montoInteresTotalMN+ "\n" +
							//"Monto Descuento MXP: " + montoDescuentoTotalMN + "\n" +
							"Monto USD: " + montoTotalDL + "\n" +
							"Monto Interes USD: " + montoInteresTotalDL + "\n";
							//"Monto Descuento USD: " + montoDescuentoTotalDL + "\n";
				}
				rsAcuse.close();
				psAcuse.close();
			}
			resultadoProceso.setResumenEjecucion("PROCESO FINALIZADO\n" + datosAcuse);
		   
			if (acuse != null) {
					CrearArchivosAcuses arch = new CrearArchivosAcuses();

					arch.setAcuse(acuse);
					arch.setUsuario(claveUsuario);
					arch.setTipoUsuario("IF");
					arch.setVersion("WEBSERVICE");

					try {
						arch.setResumenEjecucion(resultadoProceso.getResumenEjecucion());
						arch.setRecibo(receipt);
						arch.guardarArchivo();

					} catch (Exception exception) {

						java.io.StringWriter outSW = new java.io.StringWriter();
						exception.printStackTrace(new java.io.PrintWriter(outSW));
						String stackTrace = outSW.toString();
						arch.setDesError(stackTrace);
						arch.guardaBitacoraArch();
						throw new NafinException("SIST0001");
					}

				}
			
			
			
		} catch (Throwable t) {
			throw new Exception("Error al obtener los datos del acuse", t);
		}

	} catch (Throwable t) { //Aqui llegan todos los errores inesperados que puedan ocurrir.
		exito = false;
		DocumentoDistIFWS[] arrDocumentosError = null;
		log.error("confirmacionDoctosDistIFWS(Error) ", t);
		// Si los errores son antes de que empiece el ciclo de procesamiento de documentos
		// significa que no proceso ningun registro, por lo cual se debe regresar
		// todos los documentos recibidos, como documentos con error...
		if (i == 0) { //si i es 0 significa que no entro al ciclo para procesar docto
			for (i=0; i < documentos.length; i++) {
				DocumentoDistIFWS doc = documentos[i];
				doc.setErrorDetalle("ERROR INESPERADO|Registro no procesado");
				doc.setTipoError(new Integer(2));

				erroresInesperados.append(doc.getErrorDetalle()+"\r\n");
				log.error("Hubo errores Inesperados "+erroresInesperados);

			}
			arrDocumentosError = documentos;
			//borra el acuse ya que no se proceso ningun registro
		} else {
			// Si el proceso es despues del ciclo de procesamiento de documentos,
			// si hubo errores, ya sea esperados o inesperados, estos estan especificados
			// en el detalle de Error del objeto del documento
			if (lDocumentosError.size() > 0) {
				arrDocumentosError = new DocumentoDistIFWS[lDocumentosError.size()];
				lDocumentosError.toArray(arrDocumentosError);
			}
		}

		resultadoProceso.setDocumentos(arrDocumentosError);
		resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
		resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Proceso Interrumpido: " + t.getMessage());

		erroresInesperados.append(resultadoProceso.getResumenEjecucion()+"\r\n");
		log.error("Hubo errores Inesperados "+erroresInesperados);


		/* llamado del metodo para enviar correo de Errores Inesperados siempre y cuando hayan*/
		try {
			if(!erroresInesperados.toString().equals("")){
				erroresInesperados.append("\nError causado al procesar: Clave IF " +
						claveIF + ", Usuario: " + claveUsuario);
				enviodeCorreoWS(erroresInesperados,"C");
			}
		} catch(Throwable e) {
			//throw new Exception ("Error al Enviar el correo de Errores Inesperados. ", t);
		}

	} finally {
		// se incorporan los documentos que estan bien 			
		if(lDocumentosV2.size()>0){
			DocumentoDistIFWS[] arrDocumentosError = new DocumentoDistIFWS[lDocumentosV2.size()];
			lDocumentosV2.toArray(arrDocumentosError);
			resultadoProceso.setDocumentos(arrDocumentosError);		
		} 
		
		if (con.hayConexionAbierta()) {
			try {
				//psParamNotasCred.close();
				psRegistroSolicitud.close();
				psUpdate.close();
				psRechazo.close();
				psSolicitud.close();
				psDoc.close();
				//psAmortizacion.close();
				con.terminaTransaccion(exito);
				//Si hubo un error inesperado que interrumpio el proceso (exito = false) y hay acuse
				//entonces se borra dicho acuse.
				if ( !exito && !acuse.equals("") ) {
					//dado que BorraAcuse da commit, se coloca hasta el final despues
					//de con.terminaTransaccion(exito); para que no afecte la transaccion principal
					BorraAcuse(acuse, con);
				}
				con.cierraConexionDB();
			} catch(Exception e1) {
				log.error("confirmacionDoctosDistIFWS(Error)::" + e1.getMessage());
			}
		}
	}

	log.info("confirmacionDoctosDistIFWS(S)");
	return resultadoProceso;
}


/**
	 * Determina si esta parametrizado el fondeo propio para la EPO
	 * con el IF especificado.
	 * por motivos de compatibilidad, ic_epo puede trae la cadena NULL en lugar
	 * de la clave del ic_epo
	 *
	 *
	 * @param claveIF Clave del IF
	 * @param ic_epo Clave de la Epo
	 * @return true si maneja fondeo propio o false de lo contrario
	 * @throws com.netro.exception.NafinException
	 */
	private boolean hayFondeoPropio(String claveIF,String ic_epo)
			throws NafinException {

		AccesoDB con  = new AccesoDB();
		String qrySentencia = null;
		ResultSet rs = null;
		String tipoFondeo = "";
		boolean resultado = false;

		try {
			if(ic_epo!=null&&!"".equals(ic_epo)) {
				con.conexionDB();
				qrySentencia =
						" SELECT CS_TIPO_FONDEO " +
						" FROM COMREL_IF_EPO_X_PRODUCTO" +
						" WHERE IC_IF = ? " +
						" AND IC_EPO =  ? " +
						" AND IC_PRODUCTO_NAFIN = ?";

				PreparedStatement ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(claveIF));
				if (ic_epo.equalsIgnoreCase("NULL")) {
					ps.setNull(2,Types.NUMERIC);
				} else {
					ps.setInt(2,Integer.parseInt(ic_epo));
				}
				ps.setInt(3,4);	//4=Distribuidores

				rs = ps.executeQuery();

				if(rs.next()){
					tipoFondeo = rs.getString("CS_TIPO_FONDEO");
					if("P".equals(tipoFondeo))
						resultado = true;
				}
				con.cierraStatement();
			}
		} catch (Exception e) {
			throw new AppException("Error al determinar el manejo de fondeo propio", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return resultado;
	}

	private void insertaAcuse(
		String  acuse,
		String	icUsuario,
		String	cgReciboElectronico,
		AccesoDB con)
   throws NafinException{
		PreparedStatement ps = null;
		String qrySentencia = "";
   	try{
			qrySentencia =	" INSERT INTO COM_ACUSE3"+
                		" (cc_acuse,in_monto_mn,in_monto_int_mn,in_monto_dscto_mn"+
							  " ,in_monto_dl,in_monto_int_dl,in_monto_dscto_dl"+
							  " ,ic_usuario,df_fecha_hora,cg_recibo_electronico,ic_producto_nafin)"+
							  " values(?, 0, 0, 0, 0, 0, 0,?,sysdate, ?,4)";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,acuse);
			ps.setString(2,icUsuario);
			ps.setString(3,cgReciboElectronico);
			ps.executeUpdate();
			ps.close();

			con.terminaTransaccion(true);
        }catch(Exception e){
				System.out.println("Excepcion "+e);
            throw new NafinException("SIST0001");
        }
   }


	/**
	 * Realiza la actualizaci�n de los datos del acuse (montos, num. registros
	 *  procesados,etc.).
	 *
	 * @param acuse clave del Acuse
	 * @param reciboFirma Recibo regresado por el proceso de firma digital
	 * @param con Conexion de Base de datos (debe estar establecida la conexion)
	 * @return true Si exiti� un error al realizar la actualizaci�n o false de lo contrario.
	 * @throws com.netro.exception.NafinException
	 */
	private boolean ActualizaAcuse(String acuse, String reciboFirma, AccesoDB con)
			throws NafinException {
		log.info("ActualizaAcuse(E)");
		boolean error=false;

		/* Actualiza el acuse con los datos de las solicitudes */


		String qrybuscadatos=
				" SELECT nvl(sum(decode(d.ic_moneda,1,d.fn_monto,0)),0) monto_mn,  " +
				"		nvl(sum(decode(d.ic_moneda,54,d.fn_monto,0)),0) monto_dl,  " +
				"		nvl(sum(d.fn_monto),0) monto_total,  " +
				"		nvl(sum(decode(d.ic_moneda,1,DS.fn_importe_interes,0)),0) monto_int_mn,  " +
				"		nvl(sum(decode(d.ic_moneda,54,DS.fn_importe_interes,0)),0) monto_int_dl,  " +
				"		nvl(sum(DS.fn_importe_interes),0) monto_int_total,  " +
				"		nvl(sum(decode(d.ic_moneda,1,ds.fn_importe_recibir,0)),0) monto_d_mn,  " +
				"		nvl(sum(decode(d.ic_moneda,54,ds.fn_importe_recibir,0)),0) monto_d_dl,  " +
				"		nvl(sum(ds.fn_importe_recibir),0) monto_total,  " +
				"		nvl(sum(1),0) total_reg  " +
				" FROM dis_solicitud s,  " +
				"      dis_documento d,  " +
				"      dis_docto_seleccionado ds  " +
				" WHERE s.ic_documento=d.ic_documento  " +
				"   and s.ic_documento=ds.ic_documento  " +
				"   and d.ic_documento=ds.ic_documento  " +
				"  	and s.cc_acuse= ? ";

		ResultSet rs=null;
		PreparedStatement ps = null;
		try {
			ps = con.queryPrecompilado(qrybuscadatos);
			ps.setString(1, acuse);
			rs = ps.executeQuery();
			if (rs.next()) {
				//String in_monto_mn=rs.getString("MONTO_MN");
				//String in_monto_dl=rs.getString("MONTO_DL");
				String in_monto_int_mn=rs.getString("MONTO_INT_MN");
				String in_monto_int_dl=rs.getString("MONTO_INT_DL");
				String in_monto_recib_mn=rs.getString("MONTO_D_MN");
				String in_monto_recib_dl=rs.getString("MONTO_D_DL");
				String total_reg=rs.getString("TOTAL_REG");
				rs.close();
				con.cierraStatement();
				if (!(total_reg.equals("0"))) {
					String qryactualiza =
							" UPDATE com_acuse3 SET "+
							"IN_MONTO_MN = ?, "+
							"IN_MONTO_INT_MN = ?, "+
							"IN_MONTO_DSCTO_MN = ?, "+
							"IN_MONTO_DL = ?, "+
							"IN_MONTO_INT_DL = ?, "+
							"IN_MONTO_DSCTO_DL = ?, "+
							"cg_recibo_electronico = ? " +
							"WHERE cc_acuse = ? ";
					PreparedStatement psUpdate = con.queryPrecompilado(qryactualiza);
					psUpdate.setDouble(1,Double.parseDouble(in_monto_recib_mn));
					psUpdate.setDouble(2,Double.parseDouble(in_monto_int_mn));
					psUpdate.setDouble(3,Double.parseDouble("0.0"));
					psUpdate.setDouble(4,Double.parseDouble(in_monto_recib_dl));
					psUpdate.setDouble(5,Double.parseDouble(in_monto_int_dl));
					psUpdate.setDouble(6,Double.parseDouble("0.0"));
					psUpdate.setString(7,reciboFirma);
					psUpdate.setString(8,acuse);
					psUpdate.executeUpdate();
					psUpdate.close();
				} else {
					error=true; /* No existen registros que pertenezcan al acuse */
					log.info("ActualizaAcuse():: NO existen registros que pertenezcan al acuse");
				}
			} else {
				error=true;
			}
		} catch (Exception e) {
			log.error("ActualizaAcuse(Error): ", e);
			error=true;
			if ( !"".equals(reciboFirma) ) {		//Si reciboFirma no es vacio es una invocaci�n directa desde este metodo por lo cual se puede lanzar la excepci�n
				throw new AppException("Error al actualizar el acuse", e);
			}
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs!=null) {
						rs.close();
					}
					if (ps!=null) {
						ps.close();
					}
				}catch(Exception e) {
					log.error("ActualizaAcuse(Error)" + e.getMessage(), e);
				}

				/* Realiza un commit o un rollback */
				if (error == true) {
					con.terminaTransaccion(false);
				} else {
					con.terminaTransaccion(true);
				}
			} /* If cuando hay conexion */
			log.info("ActualizaAcuse(S)");
		}
		return error;
	}

	public void BorraAcuse(String acuse,AccesoDB con)
		throws NafinException
    {	/* Regresar los documentos a en proceso de autorizacion IF */
    	log.info("BorraAcuse(E):"+acuse);
		String qryborra =
			"UPDATE dis_documento " +
			"   SET ic_estatus_docto = 24 " +
			" WHERE ic_documento IN (SELECT sol.ic_documento " +
			"                          FROM dis_solicitud sol " +
			"                         WHERE sol.cc_acuse = '"+acuse+"') ";
        try
        {	con.ejecutaSQL(qryborra);


        	qryborra = "DELETE dis_solicitud s WHERE s.cc_acuse = '"+acuse+"' ";
        	con.ejecutaSQL(qryborra);

			qryborra = "DELETE com_acuse3 WHERE cc_acuse = '"+acuse+"' ";
        	con.ejecutaSQL(qryborra);
			con.terminaTransaccion(true);
		/* Manejo de exceptiones */
		} catch (Exception e) {
			if (con.hayConexionAbierta())
			{
				con.terminaTransaccion(false);
			}
			throw new NafinException("DSCT0069");
		} finally {
				log.info("BorraAcuse(S)");
		}
	}
}