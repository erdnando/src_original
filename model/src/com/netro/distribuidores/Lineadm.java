package com.netro.distribuidores;

import com.netro.exception.NafinException;

import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.AccesoDB;

@Remote
public interface Lineadm {

    public abstract Vector getValores(String ic_epo, String solicitud, String ic_moneda, String ic_tipo_cobro_interes,  String vencimiento, String ic_financiera) throws NafinException;

    public abstract Vector getTotalAfiliados(String ic_epo) throws NafinException;

    public abstract String getDetalle(String ic_if) throws NafinException;

    public abstract Vector getDetalleInicial(String ic_linea_credito_dm) throws NafinException;

    public abstract String getMontoAutorizado(String ic_if, String ic_epo,String folio) throws NafinException;

    public Vector getMontoAutFechaVto(String ic_if, String ic_epo,String folio) throws NafinException;  

    public abstract Vector getComboBanco() throws NafinException;

    public abstract Vector getComboEPO_RS() throws NafinException;

    public abstract Vector getComboSolicitudRelacionada(String ic_if,String ic_epo, String solicitud, String moneda) throws NafinException;

    public abstract boolean getLineaExistente(String moneda,String ic_if,String ic_epo) throws NafinException;

    public abstract Vector setLineasCredito(String iNoUsuario,String in_total_lineas,String in_total_monto,String in_total_lineasDol,String in_total_montoDol,String anoNafinE[],String ic_epos[],String asolic[],String afolio[],String amoneda[],String amontoAuto[],String aplaz[],String acobint[],String avencimiento[],String acliAfil[],String anCtaEpo[],String aifBanco[],String anCtaIf[], String ic_if) throws NafinException;

    public abstract Vector getSolicRelac(String ic_linea_credito_dm) throws NafinException;

    public abstract Vector getSolicitudes(String ic_if, String tipoLinCre,String fechaVenc1,String fechaVenc2,String epo,String tipoCoInt,String distrib,String numCred,String fechaOper1,String fechaOper2,String monto1,String monto2) throws NafinException;

    public abstract void setDisDocumento(String ic_documento[],String ic_estatus_docto[],String ct_cambio_motivo[]) throws NafinException;

    public Vector getConsultaBajaDocs(String ic_epo,String distribuidor,String moneda,String estatusdocto,String monto1,String monto2,String numdoc,String cdescuento,String numaccuse,String fechaemision1,String fechaemision2,String fechavenc1,String fechavenc2,String fechapub1,String fechapub2,String modplazo,String NOnegociable ) throws NafinException;

    public Vector getCambiosDocto(String ic_documento) throws NafinException;

    public void setDisDocumentoBaja(String ic_documento[],String selecc[],String ic_estatus_docto,String ct_cambio_motivo[]) throws NafinException;

    public void validaLinea(String moneda,String ic_if,String ic_epo,String fechaVenc,String tipoSolic) throws NafinException;

}
