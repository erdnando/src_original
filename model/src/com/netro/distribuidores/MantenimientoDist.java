package com.netro.distribuidores;

import netropology.utilerias.*;
import java.util.*;
import java.math.*;
import java.io.*;
import com.netro.exception.*;
import javax.ejb.Remote;

@Remote
public interface MantenimientoDist {
	public Combos getComboModalidad()
		throws NafinException;

	 public Vector getVecModalidad(String epo,String pyme)	//Sustituye a getComboModalidad
		throws NafinException;
		
	public Vector consultaDoctos(String ic_epo,String ic_pyme, String ic_moneda, String modo_plazo, String ic_estatus_docto, String ig_numero_docto, String cc_acuse, String fecha_emision_de, String fecha_emision_a, String fecha_vto_de, String fecha_vto_a, String fecha_publicacion_de, String fecha_publicacion_a, String fn_monto_de, String fn_monto_a, String monto_con_descuento, String solo_cambio,String in,boolean incluyeCambio,String NOnegociable)
		throws NafinException;

   	public void guardaCambios(
   			String ic_documento[],
   			String nueva_fecha_docto[],
   			String nueva_fecha_vto[],
   			String nuevo_monto[],
   			String nuevo_modo_plazo[],
   			String fecha_lim_desc[], 
   			String cg_fecha_porc_desc,
   			String ic_estatus_docto[],
				String anterior_monto[],
				String ses_ic_epo,
				String tipoCredito,
				String ventaCartera,
				String descuentoAutomatico
				)
		throws NafinException;
   
	public Vector consultaPagoCredito(String lsNumEPO,
               String lsNumCredito,
               String lsFchOperIni,
               String lsFchOperFin,
               double ldMontoCredIni,
               double ldMontoCredFin,
               String lsFchVenciIni,
               String lsFchVenciFin,
               String lsEstatus,
               String lsTipoCobroInte,
               String lsIcIF,
               String lsTipoCredito,
               String lsNumPyme )
		throws NafinException;
	
	public Vector consultaPagosCreditos(String lsNumEPO,       String lsNumPYME,       String lsNumCredito,   String lsFchOperIni,  
										String lsFchOperFin,   double ldMontoCredIni,  double ldMontoCredFin, String lsFchVenciIni, 
										String lsFchVenciFin,  String lsTipoCobroInte, String lsEstatus,      String lsFchPagoIni,
										String lsFchPagoFin )
		throws NafinException;
	
	public Vector consultaPagosCreditos(String lsNumIF,        String lsNumCredito,  String lsMoneda,      double ldMontoCredIni,  
										double ldMontoCredFin, String lsFchVenciIni, String lsFchVenciFin, String lsTipoCobroInte, 
										String lsFchPagoIni,   String lsFchPagoFin,String lsEstatus)
		throws NafinException;
		
	public void guardaPagosRechazados( Vector lVecPagosRecha )	
		throws NafinException;
		
	public abstract String getDiasAmpFechaVen(String ic_epo)		
		throws NafinException;
		
	public abstract List getDatosSirac(String ig_numero_prestamo)
		throws NafinException;
		
		
	public abstract List getDistribuidores(String ic_if, String cg_num_distribuidor, String cg_rfc, String cg_razon_social)		
		throws NafinException;
		
	public abstract void setEstatusSuspen(String ic_pyme, String cg_num_distribuidor, String cs_estatus)
		throws NafinException;
		
	public abstract List getHistorico(String ic_pyme) 
		throws NafinException;
		
	public abstract void setClientesSusp(String df_suspension)
		throws NafinException;
		
 	public abstract void setClientesReactivos(String df_reactivacion)
		throws NafinException;
		
	public abstract void operaPlazoAmpliadoPemex(String ic_epo) 
		throws NafinException;
		
	public HashMap validarMantenimientoMasivo(String strRegistros, String ic_epo)
		throws NafinException;
		
	public void guardaCambiosMasivo(String strPreacuse, String totaldoc, String totaldocdl, String mtodoc, String mtodocdl, String acuse, String recibo, String icusuario)
		throws NafinException;
	
	public Vector getVecModalidad(String epo,String pyme, String tipoFinanciamiento)	throws NafinException;
	
		
}