package com.netro.distribuidores;

import java.util.List;

public interface DistribuidorEnlace {

	public abstract String getTablaDocumentos();
	
	public abstract String getTablaAcuse();
	
	public abstract String getTablaErrores();
	
	public abstract String getDocuments();
	
	//public abstract String getInsertaErrores(ErroresEnl err);
	
	//public abstract String getUpdateAcuse(AcuseEnl acu);
	
	//public abstract String getInsertAcuse(AcuseEnl acu);
	
	public abstract String getCondicionQuery();
  
  public abstract void addErrores(ErroresEnl err);
  
  public abstract List getErrores();
  
  //public String bCamposDetalle();
	
}//Distribuidor Enlace