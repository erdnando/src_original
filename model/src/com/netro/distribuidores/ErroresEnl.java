package com.netro.distribuidores;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class ErroresEnl {

	private	String	ig_numero_docto 		= null;
	private	String	cg_pyme_epo_interno		= null;
	private String	ig_numero_error			= null;
	private String	cg_error				= null;
	private String	in_a_ejercicio			= null;
	private String	in_sociedad				= null;
	private String	cc_acuse				= null;
	
	//getters
	public String getIgNumeroDocto() {
		return ig_numero_docto;
	}
	public String getCg_pyme_epo_interno() {
		return cg_pyme_epo_interno;
	}
	public String getIgNumeroError(){
		return ig_numero_error;
	}
	public String getCgError(){
		return cg_error;
	}
	public String getInAEjercicio(){
		return in_a_ejercicio;
	}
	public String getInSociedad(){
		return in_sociedad;
	}
	public String getCcAcuse(){
		return cc_acuse;
	}
	//setters
	public void setIgNumeroDocto(String ig_numero_docto){
		this.ig_numero_docto = ig_numero_docto;
	}
	public void setCg_pyme_epo_interno(String cg_pyme_epo_interno) {
		this.cg_pyme_epo_interno = cg_pyme_epo_interno;
	}
	public void setIgNumeroError(String ig_numero_error){
		this.ig_numero_error = ig_numero_error;
	}
	public void setCgError(String cg_error){
		this.cg_error = cg_error;
	}
	public void setInAEjercicio(String in_a_ejercicio){
		this.in_a_ejercicio = in_a_ejercicio;
	}
	public void setInSociedad(String in_sociedad){
		this.in_sociedad = in_sociedad;
	}
	public void setCcAcuse(String cc_acuse){
		this.cc_acuse = cc_acuse;
	}

}
