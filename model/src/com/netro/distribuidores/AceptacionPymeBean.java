package com.netro.distribuidores;

import com.netro.anticipos.CapturaTasas;
import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;
import com.netro.seguridadbean.SeguException;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AccesoDBOracle;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.Bitacora;
import netropology.utilerias.Combos;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "AceptacionPymeEJB" , mappedName = "AceptacionPymeEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class AceptacionPymeBean implements AceptacionPyme {
   private String	  acuseFormateado  = "";

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(AceptacionPymeBean.class);


    public Vector consultaDoctos(String cc_acuse)
   throws NafinException{
	System.out.println("AceptacionPymeBean::consultaDoctos (E)");
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas = new Vector();
        Vector	vecFilas1 = new Vector();
        Vector	vecFilas2 = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();
				qrySentencia = "  SELECT E.CG_RAZON_SOCIAL AS EPO"   +
                             "    ,D.IG_NUMERO_DOCTO"   +
                             "    ,DS.CC_ACUSE"   +
                             "    ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION"   +
                             "    ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC"   +
                             "    ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION"   +
                             "    ,D.IG_PLAZO_DOCTO AS PLAZO"   +
                             "    ,M.CD_NOMBRE AS MONEDA"   +
                             "    ,D.FN_MONTO AS MONTO"   +
                             "    ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                             "    ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,''),'') as TIPO_CAMBIO"   +
                             "    ,'' as MONTO_VALUADO"   +
                             "    ,D.ig_plazo_descuento"   +
							"     ,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento"   +
                             "    ,TF.cd_descripcion as MODO_PLAZO"   +
                             "    ,ED.CD_DESCRIPCION AS ESTATUS"   +
                             "    ,'' AS CAMBIOS"   +
                             "    ,D.IC_DOCUMENTO AS NUMERO_CREDITO"   +
                             "    ,I.CG_RAZON_SOCIAL AS IF"   +
                             "    ,DECODE(LCD.CG_TIPO_SOLICITUD,'I','Inicial','A','Ampliacion','R','Renovacion') as TIPO_SOLICITUD"   +
                             "    ,'' AS FECHA_OPERACION"   +
                             "    ,'' AS MONTO_CREDITO"   +
                             "    ,D.IG_PLAZO_CREDITO"   +
                             "    ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
                             "    ,CT.CD_NOMBRE AS REFERENCIA_INT"   +
                             "    ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
                             "    , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES"   +
                             "    ,decode(TF.ic_tipo_financiamiento,2,0,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100)) as MONTO_DESCUENTO"   +
                             "    ,CMT.FN_VALOR AS VALOR_TASA"   +
                             "    ,DS.CG_REL_MAT AS REL_MAT"   +
                             "    ,DS.FN_PUNTOS AS PUNTOS"   +
                             "    ,I.IG_TIPO_PISO"   +
                             "    ,TCI.ic_tipo_cobro_interes"   +
                             "    ,M.IC_MONEDA"   +
                             "    ,CT.ic_tasa"   +
                             "  , to_char(ca.df_fecha_hora,'dd/mm/yyyy') as fecha_carga"   +
                             "  , to_char(ca.df_fecha_hora,'hh:mi') as hora_carga"   +
                             "  , '' as usuario "   +
                             "	,LCD.ic_moneda as MONEDA_LINEA "+
							 "	,TF.ic_tipo_financiamiento "+
							 "	,M2.cd_nombre as NOMBRE_MLINEA " +
							 "	,DS.FN_VALOR_TASA"+
							"     ,  (SELECT fn_valor_aforo     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS descuento_aforo  "+

                             "  FROM DIS_DOCUMENTO D"   +
                             "    ,COMCAT_EPO E"   +
                             "    ,COMREL_PRODUCTO_EPO PE"   +
                             "    ,COMCAT_PRODUCTO_NAFIN PN"   +
                             "    ,COMCAT_MONEDA M"   +
                             "    ,COM_TIPO_CAMBIO TC"   +
                             "    ,COMCAT_ESTATUS_DOCTO ED"   +
                             "    ,DIS_LINEA_CREDITO_DM LCD"   +
                             "    ,COMCAT_IF I"   +
                             "    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
                             "    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
                             "    ,COMCAT_TASA CT"   +
                             "    ,COMCAT_PLAZO CP"   +
                             "    ,COM_TASA_IF_EPO CTG"   +
                             "    ,COM_MANT_TASA CMT"   +
                             "    ,DIS_DOCTO_SELECCIONADO DS"   +
                             "    ,COM_ACUSE2 CA"   +
                             //FODA 006"  , s"   +
                             "	  ,COMCAT_TIPO_FINANCIAMIENTO TF"+
							 "	  ,COMCAT_MONEDA M2"+
                             "  WHERE E.IC_EPO = PE.IC_EPO"   +
                             "    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                             "    AND PN.IC_PRODUCTO_NAFIN = 4"   +
                             "    AND D.IC_EPO = PE.IC_EPO"   +
                             "    AND D.IC_MONEDA = M.IC_MONEDA"   +
                             "    AND M.IC_MONEDA = TC.IC_MONEDA"   +
                             "    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                             "    AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
                             "    AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
                             "    AND CTG.IC_IF = LCD.IC_IF"   +
                             "    AND CTG.IC_EPO = LCD.IC_EPO"   +
                             "    AND LCD.IC_IF = I.IC_IF"   +
							 "	  AND M2.IC_MONEDA = LCD.IC_MONEDA"+
                             "    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
                             "    AND D.ic_estatus_docto = 3"   +
                             "    AND IEP.IC_IF = I.IC_IF"   +
                             "    AND IEP.IC_EPO = E.IC_EPO"   +
                             "	  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
                             "	  AND D.IC_TIPO_FINANCIAMIENTO != 5 "+
                             "    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                             "    AND CT.IC_PLAZO = CP.IC_PLAZO"   +
                             "    AND CT.CS_DISPONIBLE = 'S'"   +
                             "    AND CTG.IC_TASA = CT.IC_TASA"   +
                             "    AND CTG.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                             "    AND CMT.IC_TASA = CT.IC_TASA"   +
							 "	  AND CT.IC_TASA = DS.IC_TASA"+
                             "    AND CMT.DC_FECHA IN("   +
                             "    	  SELECT MAX(DC_FECHA) FROM COM_MANT_TASA"   +
                             "    	  WHERE IC_TASA = CT.IC_TASA)"   +
                             "    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                             "    AND DS.CC_ACUSE = CA.CC_ACUSE"   +
                             "    AND DS.CC_ACUSE = '"+cc_acuse+"'"   +
                             //FODA 006"    and ca.ic_usuario = s.ic_usuario"   +
                             "  ORDER BY D.IG_NUMERO_DOCTO"  ;

                System.out.println("consultaDoctos: "+qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*10*/			vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*11*/			vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
/*12*/			vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/			vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*14*/			vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/			vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*16*/			vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
/*17*/			vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
/*18*/			vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
/*19*/			vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*20*/			vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*21*/			vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*22*/			vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*23*/			vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*24*/			vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*25*/			vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
/*26*/			vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
/*27*/			vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
/*28*/			vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
/*29*/			vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*30*/			vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
/*31*/			vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*32*/			vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*33*/			vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
/*34*/			vecColumnas.add(cc_acuse);
/*35*/			vecColumnas.add((rs.getString("FECHA_CARGA")==null)?"":rs.getString("FECHA_CARGA"));
/*36*/			vecColumnas.add((rs.getString("HORA_CARGA")==null)?"":rs.getString("HORA_CARGA"));
/*37*/			vecColumnas.add((rs.getString("USUARIO")==null)?"":rs.getString("USUARIO"));
/*38*/			vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*39*/			vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
/*40*/			vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"":rs.getString("NOMBRE_MLINEA"));
/*41*/			vecColumnas.add((rs.getString("FN_VALOR_TASA")==null)?"":rs.getString("FN_VALOR_TASA"));
/*42*/			vecColumnas.add((rs.getString("descuento_aforo")==null)?"":rs.getString("descuento_aforo"));




                        if("E".equals(rs.getString("RESPONSABLE_INTERES")))
							vecFilas1.add(vecColumnas);
                        else
                        	vecFilas2.add(vecColumnas);
                }
                con.cierraStatement();
                vecFilas.add(vecFilas1);
                vecFilas.add(vecFilas2);
        }catch(Exception e){
				System.out.println("Excepcion "+e);
				e.printStackTrace();
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("AceptacionPymeBean::consultaDoctos (S)");
        }
   return vecFilas;
   }


 //DEPRECATED METHOD 09/06/2003
public Combos getComboLineasCCC(String ic_pyme)
	throws NafinException{
		System.out.println("AceptacionPymeBean::getComboLineasCCC (E) DEPRECATED: USAR getLineasCCC");
		AccesoDB 	con = null;
		Combos 		comboLineas = null;
		String    qrySentencia = "";
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =	" SELECT LCD.IC_LINEA_CREDITO "+
                                        " ,DECODE(LCD.CG_NUMERO_CUENTA,'',LCD.CG_NUMERO_CUENTA_IF,LCD.CG_NUMERO_CUENTA)"+
					" ||' - '||I.CG_RAZON_SOCIAL"+
		                        " FROM COM_LINEA_CREDITO LCD"+
                                        " ,COMCAT_IF I"+
                                        " WHERE LCD.IC_IF = I.IC_IF"+
                                        " AND LCD.DF_VENCIMIENTO_ADICIONAL > SYSDATE"+
                                        " AND LCD.CG_TIPO_SOLICITUD IN('I','R')"+
                                        " AND LCD.IC_PYME = "+ic_pyme+
                                        " ORDER BY IC_LINEA_CREDITO";

			comboLineas = new Combos(qrySentencia,con);
		}	catch(Exception e){
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		System.out.println("AceptacionPymeBean::getComboLineasCCC (S)");
		}
		return comboLineas;
}

//SUSTITUYE A getComboLineasCCC
	public Vector getLineasCCC(String ic_pyme)
			throws NafinException{
		Vector				vecFilas		= new Vector();
		try {
			vecFilas = getLineasCCC(ic_pyme, "", "");
		}	catch(Exception e){
			throw new NafinException("SIST0001");
		}
		return vecFilas;
	}

	public Vector getLineasCCC(String ic_pyme, String ic_if, String ic_moneda)
			throws NafinException{
		System.out.println("AceptacionPymeBean::getLineasCCC (E)");
		AccesoDB			con				= null;
		ResultSet			rs 				= null;
		PreparedStatement	ps				= null;
		Vector				vecFilas		= new Vector();
		Vector				vecColumnas		= new Vector();
		String				qrySentencia	= "";
		String				condicion		= "";
		try {
			con = new AccesoDB();
			con.conexionDB();
			if(!"".equals(ic_if))
				condicion += "	AND LCD.IC_IF = ? ";
			if(!"".equals(ic_moneda))
				condicion += "	AND LCD.IC_MONEDA = ? ";
			qrySentencia =
				" SELECT LCD.IC_LINEA_CREDITO "+
				" ,LCD.ic_linea_credito"+
				" ||' - '||I.CG_RAZON_SOCIAL||DECODE(LCD.IC_MONEDA,1,' M.N.',54,' USD')"+
				" FROM COM_LINEA_CREDITO LCD"+
				"	 ,COMCAT_IF I"+
				" WHERE LCD.IC_IF = I.IC_IF"+
				"	AND LCD.DF_VENCIMIENTO_ADICIONAL > SYSDATE"+
				"	AND LCD.CG_TIPO_SOLICITUD = 'I'"+
				"	AND LCD.IC_ESTATUS_LINEA = 12"+
				"	AND LCD.IC_PYME = ? "+
				condicion+
				" ORDER BY IC_LINEA_CREDITO";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_pyme));
			int cont = 1;
			if(!"".equals(ic_if)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_if));
			}
			if(!"".equals(ic_moneda)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_moneda));
			}
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()){
				vecColumnas = new Vector();
				vecColumnas.add((rs.getString(1)==null)?"":rs.getString(1));
				vecColumnas.add((rs.getString(2)==null)?"":rs.getString(2));
				vecFilas.add(vecColumnas);
			}
			ps.close(); rs.close();
		}	catch(Exception e){
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AceptacionPymeBean::getLineasCCC (S)");
		}
		return vecFilas;
	}


   public Vector consultaDoctos(String ic_pyme
        ,String ic_epo
   	,String ic_moneda
        , String ig_numero_docto
        ,String fn_monto_de
        ,String fn_monto_a
        ,String cc_acuse
        ,String monto_desc
        ,String fecha_emision_de
        ,String fecha_emision_a
        ,String con_cambio
        ,String fecha_vto_de
        ,String fecha_vto_a
        ,String	modo_plazo
        ,String fecha_publicacion_de
        ,String fecha_publicacion_a
        ,String in
   )

   throws NafinException{
	System.out.println("AceptacionPymeBean::consultaDoctos (E)");
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas = new Vector();
        Vector	vecFilas1 = new Vector();
        Vector	vecFilas2 = new Vector();
        Vector	vecColumnas = null;
        String 	condicion = "";
   	try{
        	con = new AccesoDB();
                con.conexionDB();

			String bloqueo =   this.getValidaBloqueo ( ic_pyme   , ic_epo , ic_moneda    ,  ig_numero_docto
        , fn_monto_de  , fn_monto_a    , cc_acuse , monto_desc    , fecha_emision_de, fecha_emision_a  , con_cambio    , fecha_vto_de
        , fecha_vto_a     ,	modo_plazo    , fecha_publicacion_de , fecha_publicacion_a     , in  );


                if(!"".equals(ic_epo)&&ic_epo!=null)
                	condicion += " and D.ic_epo = "+ic_epo;
                if(!"".equals(modo_plazo)&&modo_plazo!=null)
                	condicion += " and TF.ic_tipo_financiamiento = "+modo_plazo;
                else
                	condicion += " and TF.ic_tipo_financiamiento in (1,2,3,4)";
                if(!"".equals(ic_moneda)&&ic_moneda!=null)
                	condicion += " and D.ic_moneda = "+ic_moneda;
                if(!"".equals(ig_numero_docto)&&ig_numero_docto!=null)
                	condicion += " and D.ig_numero_docto = '"+ig_numero_docto+"'";
                if(!"".equals(cc_acuse)&&cc_acuse!=null)
                	condicion += " and D.cc_acuse = '"+cc_acuse+"'";
                if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&!"S".equals(monto_desc))
                	condicion += " and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a;
                if("S".equals(con_cambio))
                	condicion += " and D.ic_documento in(select ic_documento from dis_cambio_estatus where trunc(dc_fecha_cambio)= trunc(sysdate))";
                if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&"S".equals(monto_desc))
                	condicion += " and D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) between "+fn_monto_de+" and "+fn_monto_a;
				if(!"".equals(fecha_emision_de)&&fecha_emision_de!=null&&!"".equals(fecha_emision_a)&&fecha_emision_a!=null)
                	condicion += " and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))";
				if(!"".equals(fecha_vto_de)&&fecha_vto_de!=null&&!"".equals(fecha_vto_a)&&fecha_vto_a!=null)
	               	condicion += " and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))";
				if(!"".equals(fecha_publicacion_de)&&fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&&fecha_publicacion_a!=null)
                	condicion += " and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))";
                if(!"".equals(in)&&in!=null)
                	condicion += " and D.ic_documento in("+in+")";

							qrySentencia = "   SELECT E.CG_RAZON_SOCIAL AS EPO"   +
							"     ,D.IG_NUMERO_DOCTO"   +
							"     ,D.CC_ACUSE"   +
							"     ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION"   +
							"     ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC"   +
							"     ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION"   +
							"     ,D.IG_PLAZO_DOCTO AS PLAZO"   +
							"     ,M.CD_NOMBRE AS MONEDA"   +
							"     ,D.FN_MONTO AS MONTO"   +
							"     ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
							"     ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
							"     ,'' as MONTO_VALUADO"   +
							"     ,D.ig_plazo_descuento"   +
							"     ,D.fn_porc_descuento as fn_porc_descuento"   +
							"     ,TF.cd_descripcion as MODO_PLAZO"   +
							"     ,ED.CD_DESCRIPCION AS ESTATUS"   +
							"     ,'' AS CAMBIOS"   +
							"     ,D.IC_DOCUMENTO AS NUMERO_CREDITO"   +
							"     ,I.CG_RAZON_SOCIAL AS IF"   +
							"     ,DECODE(LCD.CG_TIPO_SOLICITUD,'I','Inicial','A','Ampliacion','R','Renovacion','') as TIPO_SOLICITUD"   +
							"     ,'' AS FECHA_OPERACION"   +
							"     ,'' AS MONTO_CREDITO"   +
							"     ,DECODE(D.ic_tipo_financiamiento,1,trunc(d.df_fecha_venc)-trunc(sysdate),4,trunc(d.df_fecha_venc)-trunc(sysdate),D.IG_PLAZO_CREDITO) as IG_PLAZO_CREDITO"   +
							"     ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
							"     ,''  AS REFERENCIA_INT"   +
							"     ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
							"     , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES"   +
                             "    ,fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
							"     ,'' AS VALOR_TASA"   +
							"     ,''  AS REL_MAT"   +
							"     ,''  AS PUNTOS"   +
							"     ,I.IG_TIPO_PISO"   +
							"     ,TCI.ic_tipo_cobro_interes"   +
							"     ,M.IC_MONEDA"   +
							"     ,'' as ic_tasa"   +
							" 	  ,TF.ic_tipo_financiamiento"   +
							" 	  ,LCD.ic_moneda as MONEDA_LINEA"   +
							"  	  ,nvl(pe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO"   +
//							"  	  ,nvl(pep.ig_dias_maximo,nvl(pe.ig_dias_maximo,pn.in_dias_maximo))  as DIAS_MAXIMO"   +
							"  	  ,nvl(pe.ig_dias_maximo,pn.in_dias_maximo)  as DIAS_MAXIMO"   +
							" 	  ,I.ic_if"   +
							"	  ,LCD.ic_linea_credito_dm"+
							"	  ,M2.cd_nombre as NOMBRE_MLINEA,clas.cg_descripcion as CATEGORIA"+
							"  	  ,nvl(clas.ig_plazo_aut,nvl(pep.ig_dias_maximo,nvl(pe.ig_dias_amp_fecha_venc,pn.ig_dias_amp_fecha_venc))) as DIAS_MAXIMO_VENC"   +
							"     ,TO_CHAR(D.DF_FECHA_VENC+nvl(clas.ig_plazo_aut,nvl(pep.ig_dias_maximo,nvl(pe.ig_dias_amp_fecha_venc,pn.ig_dias_amp_fecha_venc))),'dd/mm/yyyy') as FECHA_VENC_MAX"   +
							"  ,PE.IC_TIPO_COBRO_INTERES as TIPO_COBRO_INTERES,  " +
							" TO_CHAR (sysdate, 'dd/mm/yyyy') AS fecha_hoy "+

							" ,  E.IC_EPO AS IC_EPO   "+
							" , I.IC_IF AS IC_IF   "+
							//" , ( SELECT cs_activo FROM comrel_pyme_epo_if_x_producto   WHERE ic_pyme = d.ic_pyme  and ic_epo = d.ic_epo  and ic_if = i.ic_if   AND ic_producto_nafin = 4  )  as PYME_BLOQUEADA " + //F05-2014
					      " ,( select fN_VALOR_AFORO  From dis_aforo_epo  WHERE  IC_IF  = i.ic_if  and  ic_epo = d.ic_epo  and  ic_moneda =  d.ic_moneda  ) as DESCUENTO_AFORO  "+  //F05-2014
						 	" ,( select FN_POR_LINEA  from dis_sublimites  WHERE ic_if =  i.ic_if   and ic_epo =  d.ic_epo  and ic_pyme =  d.ic_pyme  and ic_moneda  =  d.ic_moneda )   as SUB_LIMITE  " +//F05-2014
							" ,  nvl(LCD.FN_MONTO_AUTORIZADO,0)  as MONTO_LINEA  " +  // F05-2014
							" ,  nvl(LCD.FN_SALDO_TOTAL,0)  as SALDO_DISP_LINEA  " +  // F05-2014
							" , pi.CG_LINEA_CREDITO AS CG_LINEA_CREDITO "+//F20-2015
							" ,to_char(lcd.DF_VENCIMIENTO_ADICIONAL,'dd/mm/yyyy') as FECHA_VENC_LINEA  "+ //F20-2015 
														
							"   FROM DIS_DOCUMENTO D"   +
							"     ,COMCAT_EPO E"   +
							"     ,COMREL_PRODUCTO_EPO PE"   +
							"     ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
							"     ,COMCAT_PRODUCTO_NAFIN PN"   +
							"     ,COMCAT_MONEDA M"   +
							"     ,COM_TIPO_CAMBIO TC"   +
							"     ,COMCAT_ESTATUS_DOCTO ED"   +
							"     ,DIS_LINEA_CREDITO_DM LCD"   +
							"     ,COMCAT_IF I"   +
							"     ,COMCAT_TIPO_COBRO_INTERES TCI"   +
							"     ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
							"     ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
							"	  ,COMCAT_MONEDA M2,comrel_clasificacion clas "+
							"   , comrel_producto_if pi  "+
												
							"   WHERE E.IC_EPO = PE.IC_EPO"   +
							"     AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
							"     AND PEP.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
							"     AND D.IC_EPO = PE.IC_EPO"   +
							"     AND D.IC_EPO = PEP.IC_EPO"   +
							"     AND D.IC_PYME = PEP.IC_PYME"   +
							"     AND D.IC_MONEDA = M.IC_MONEDA"   +
							"     AND M.IC_MONEDA = TC.IC_MONEDA"   +
							"	  AND M2.IC_MONEDA = LCD.IC_MONEDA"+
							"     AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
							"     AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
							"     AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
							"     AND LCD.IC_IF = I.IC_IF"   +
							"     AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
							"     AND IEP.IC_IF = I.IC_IF"   +
							"     AND IEP.IC_EPO = E.IC_EPO"   +
							" 	  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
							" 	  AND D.IC_TIPO_FINANCIAMIENTO != 5 "   +
							"     AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
							" 	  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
							"     AND D.ic_estatus_docto = 2"   +
							"     AND PN.IC_PRODUCTO_NAFIN = 4"   +
							"	  AND PE.IC_PRODUCTO_NAFIN = 4"+
							"	  AND LCD.ic_estatus_linea = 12"+
							"   and  d.CG_VENTACARTERA is null "+
                            "    AND D.IC_PYME = "+ic_pyme+
							condicion+
							"  and  pi.ic_if = lcd.ic_if  "+  //F020-2015
							"  AND pi.ic_producto_nafin = 4  "+//F020-2015
										  
							"  ORDER BY IG_NUMERO_DOCTO"  ;


     System.out.println("qrySentencia------------------------> "+qrySentencia);

                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*10*/			vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*11*/			vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
/*12*/			vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/			vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*14*/			vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/			vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*16*/			vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
/*17*/			vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
/*18*/			vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
/*19*/			vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*20*/			vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*21*/			vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*22*/			vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*23*/			vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*24*/			vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*25*/			vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
/*26*/			vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
/*27*/			vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
/*28*/			vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
/*29*/			vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*30*/			vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
/*31*/			vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*32*/			vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*33*/			vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
/*34*/			vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
/*35*/			vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*36*/			vecColumnas.add((rs.getString("DIAS_MINIMO")==null)?"0":rs.getString("DIAS_MINIMO"));
/*37*/			vecColumnas.add((rs.getString("DIAS_MAXIMO")==null)?"0":rs.getString("DIAS_MAXIMO"));
/*38*/			vecColumnas.add((rs.getString("IC_IF")==null)?"0":rs.getString("IC_IF"));
/*39*/			vecColumnas.add((rs.getString("IC_LINEA_CREDITO_DM")==null)?"":rs.getString("IC_LINEA_CREDITO_DM"));
/*40*/			vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"":rs.getString("NOMBRE_MLINEA"));
/*41*/			vecColumnas.add((rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA"));
/*42*/			vecColumnas.add((rs.getString("DIAS_MAXIMO_VENC")==null)?"":rs.getString("DIAS_MAXIMO_VENC"));
/*43*/			vecColumnas.add((rs.getString("FECHA_VENC_MAX")==null)?"":rs.getString("FECHA_VENC_MAX"));
/*44*/			vecColumnas.add((rs.getString("TIPO_COBRO_INTERES")==null)?"":rs.getString("TIPO_COBRO_INTERES"));
/*45*/			vecColumnas.add((rs.getString("fecha_hoy")==null)?"":rs.getString("fecha_hoy"));
/*46*/			vecColumnas.add(bloqueo);
/*47*/			vecColumnas.add((rs.getString("DESCUENTO_AFORO")==null)?"0":rs.getString("DESCUENTO_AFORO"));
/*48*/			vecColumnas.add((rs.getString("SUB_LIMITE")==null)?"0":rs.getString("SUB_LIMITE"));
/*49*/			vecColumnas.add((rs.getString("MONTO_LINEA")==null)?"0":rs.getString("MONTO_LINEA"));
/*50*/			vecColumnas.add((rs.getString("SALDO_DISP_LINEA")==null)?"0":rs.getString("SALDO_DISP_LINEA"));
/*51*/         vecColumnas.add((rs.getString("CG_LINEA_CREDITO")==null)?"0":rs.getString("CG_LINEA_CREDITO"));
/*52*/         vecColumnas.add((rs.getString("FECHA_VENC_LINEA")==null)?"0":rs.getString("FECHA_VENC_LINEA"));

				if("E".equals(rs.getString("RESPONSABLE_INTERES")))
					vecFilas1.add(vecColumnas);
				else
					vecFilas2.add(vecColumnas);
                }

                con.cierraStatement();
                vecFilas.add(vecFilas1);
                vecFilas.add(vecFilas2);
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("AceptacionPymeBean::consultaDoctos (S)");
        }
   return vecFilas;
   }


	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param in
	 * @param ic_linea_credito
	 * @param fecha_publicacion_a
	 * @param fecha_publicacion_de
	 * @param modo_plazo
	 * @param fecha_vto_a
	 * @param fecha_vto_de
	 * @param con_cambio
	 * @param fecha_emision_a
	 * @param fecha_emision_de
	 * @param monto_desc
	 * @param cc_acuse
	 * @param fn_monto_a
	 * @param fn_monto_de
	 * @param ig_numero_docto
	 * @param ic_moneda
	 * @param ic_epo
	 * @param ic_pyme
	 */
   public Vector consultaDoctosCCC(String ic_pyme
        ,String ic_epo
	   	,String ic_moneda
        , String ig_numero_docto
        ,String fn_monto_de
        ,String fn_monto_a
        ,String cc_acuse
        ,String monto_desc
        ,String fecha_emision_de
        ,String fecha_emision_a
        ,String con_cambio
        ,String fecha_vto_de
        ,String fecha_vto_a
        ,String	modo_plazo
        ,String fecha_publicacion_de
        ,String fecha_publicacion_a
        ,String ic_linea_credito
        ,String in
   )

   throws NafinException{
	System.out.println("AceptacionPymeBean::consultaDoctosCCC (E)");
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas = new Vector();
        Vector	vecColumnas = null;
        String 	condicion = "";
        boolean ok = true;
   	try{
        	con = new AccesoDB();
          con.conexionDB();

					/*se establece zona mexico*/
					String strAlterMex = " alter session set NLS_TERRITORY = 'MEXICO' ";
					con.ejecutaUpdateDB(strAlterMex);
					/*fin */

                if(!"".equals(ic_epo)&&ic_epo!=null)
                	condicion += " and D.ic_epo = "+ic_epo;
                if(!"".equals(modo_plazo)&&modo_plazo!=null)
                	condicion += " and TF.ic_tipo_financiamiento = "+modo_plazo;
/*                else
                	condicion += " and TF.ic_tipo_financiamiento in (1,2,3,4)";*/
                if(!"".equals(ic_moneda)&&ic_moneda!=null)
                	condicion += " and D.ic_moneda = "+ic_moneda;
                if(!"".equals(ig_numero_docto)&&ig_numero_docto!=null)
                	condicion += " and D.ig_numero_docto = '"+ig_numero_docto+"'";
                if(!"".equals(cc_acuse)&&cc_acuse!=null)
                	condicion += " and D.cc_acuse = '"+cc_acuse+"'";
                if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&!"S".equals(monto_desc))
                	condicion += " and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a;
                if("S".equals(con_cambio))
                	condicion += " and D.ic_documento in(select ic_documento from dis_cambio_estatus where trunc(dc_fecha_cambio)= trunc(sysdate))";
                if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&"S".equals(monto_desc))
                	condicion += " and D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) between "+fn_monto_de+" and "+fn_monto_a;
				if(!"".equals(fecha_emision_de)&&fecha_emision_de!=null&&!"".equals(fecha_emision_a)&&fecha_emision_a!=null)
                	condicion += " and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))";
				if(!"".equals(fecha_vto_de)&&fecha_vto_de!=null&&!"".equals(fecha_vto_a)&&fecha_vto_a!=null)
                	condicion += " and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))";
				if(!"".equals(fecha_publicacion_de)&&fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&&fecha_publicacion_a!=null)
                	condicion += " and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))";
                if(!"".equals(ic_linea_credito)&&ic_linea_credito!=null)
                	condicion += " and LC.ic_linea_credito = "+ic_linea_credito;
                if(!"".equals(in)&&in!=null)
                	condicion += " and D.ic_documento in("+in+")";

		/*	ESTE QUERY LLEVA RESTRICCI�N DE LA TASA CON EL PLAZO, ESO SE HAR� CON PROGRAMACION, POR ESO SE COMENT�, Y SE DEJ� COMO APARECE ABAJO	JRFH*/

		qrySentencia =
					"    SELECT /*+index(d IN_DIS_DOCUMENTO_02_NUK)*/ "+
					"		distinct E.CG_RAZON_SOCIAL AS EPO"   +
					"      ,D.IG_NUMERO_DOCTO"   +
					"      ,D.CC_ACUSE"   +
					"      ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION"   +
					"      ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC"   +
					"      ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION"   +
					"      ,D.IG_PLAZO_DOCTO AS PLAZO"   +
					"      ,M.CD_NOMBRE AS MONEDA"   +
					"      ,D.FN_MONTO AS MONTO"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,''),'') as TIPO_CAMBIO"   +
					"      ,'' as MONTO_VALUADO"   +
					"      ,D.ig_plazo_descuento"   +
					"      ,D.fn_porc_descuento"   +
					"      ,TF.cd_descripcion as MODO_PLAZO"   +
					"      ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"      ,'' AS CAMBIOS"   +
					"      ,D.IC_DOCUMENTO AS NUMERO_CREDITO"   +
					"      ,I.CG_RAZON_SOCIAL AS IF"   +
					"      ,DECODE(LC.CG_TIPO_SOLICITUD,'I','Inicial','A','Ampliacion','R','Renovacion','') as TIPO_SOLICITUD"   +
					"      ,'' AS FECHA_OPERACION"   +
					"      ,'' AS MONTO_CREDITO"   +
					"      ,DECODE(D.ic_tipo_financiamiento,1,trunc(d.df_fecha_venc)-trunc(sysdate),D.IG_PLAZO_CREDITO) as IG_PLAZO_CREDITO"   +
					"      ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
					"      ,'' AS REFERENCIA_INT"   +
					"      ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
					"      , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES"   +
					"	   ,decode(TF.ic_tipo_financiamiento,2,0,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100)) as MONTO_DESCUENTO"   +
					"      ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
					"      ,'' AS VALOR_TASA"   +
					"      ,'' AS REL_MAT"   +
					"      ,'' AS PUNTOS"   +
					"      ,I.IG_TIPO_PISO"   +
					"      ,TCI.ic_tipo_cobro_interes"   +
					"      ,M.IC_MONEDA"   +
					"      ,'' as IC_TASA"   +
					"      ,TF.ic_tipo_financiamiento"   +
					"      ,LC.ic_moneda as MONEDA_LINEA"   +
					"      ,nvl(pe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO"   +
					"      ,nvl(pe.ig_dias_maximo,pn.in_dias_maximo) as DIAS_MAXIMO"   +
					"      ,LC.ic_linea_credito as LINEA_CREDITO"   +
					" 	  , I.ic_if"   +
					"	  , M2.cd_nombre as NOMBRE_MLINEA"+
					"	  , PYM.CG_RAZON_SOCIAL AS NOMBREPYME "+
          "    , e.ic_epo as ic_epo "+
         // "  , TO_CHAR (SIGFECHAHABIL(d.df_fecha_venc+NVL(pep.ig_plazo_pyme, pe.ig_dias_maximo),0,'-'), 'DD/MM/YYYY') AS fechaVencimiento "+
          "  , TO_CHAR (SIGFECHAHABIL(d.df_fecha_venc+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-'), 'DD/MM/YYYY') AS fechaVencimiento "+
          "   ,pep.ig_plazo_pyme as plazopyme, pe.ig_dias_maximo as plazoepo, pn.in_dias_maximo as plazonafin "+
          "		, (select (SIGFECHAHABIL(d.df_fecha_venc+cpe.ig_plazo_pyme,0,'-')-d.df_fecha_venc) "+//obtiene plazo valido
					"		 from comrel_pyme_epo_x_producto cpe "+
					"		 where pym.ic_pyme = cpe.ic_pyme"+
					"		 and e.ic_epo = cpe.ic_epo"+
					"		 and cpe.ic_producto_nafin=4) AS PLAZO_VALIDO"+//se obtiene plazo valido en caso de que el plazo real especificado sea un dia inhabil--(02/05/2008)
					"		, PEP.ig_plazo_pyme AS PLAZO_PYME"		+//se ontiene plazo real estblecido por IF--(02/05/2008)
          "    ,tf.ic_tipo_financiamiento as plazofinanciamiento, "+
          "     tci.IC_TIPO_COBRO_INTERES  as interes,"+
          "     pe.CG_TIPOS_CREDITO as credito,  "+
			 "     TO_CHAR (sigfechahabil (  Sysdate-1 , nvl(pe.IG_DIAS_OPERACION, 0) ,'+'),  'DD/MM/YYYY')  as FECHA_DIAS_OPERACION,  "+
			  "    nvl(pe.IG_DIAS_OPERACION, 0)  as  DIAS_OPERACION,  "+
			  "    d.IG_MESES_INTERES as MESES_INTERESES,  "+ //F09-2015
			  "    d.ig_tipo_pago AS TIPOPAGO   , "+//F09-2015
			  "    TO_char (ADD_MONTHS (TRUNC(SYSDATE), d.ig_meses_interes ), 'DD/MM/YYYY')  AS FECHA_VEN_MESES,  " +
			  "   fn_dias_entre2_fechas (  ADD_MONTHS (TRUNC(SYSDATE), d.ig_meses_interes ) , TRUNC(SYSDATE)  )   AS PLAZO_VEN_MESES " + 
			  " ,to_char(lc.DF_VENCIMIENTO_ADICIONAL,'dd/mm/yyyy') as FECHA_VENC_LINEA  "+ //F20-2015 
			  " , pi.CG_LINEA_CREDITO as CG_LINEA_CREDITO  "+ //F020-2015     	
			
			
			  	"    FROM DIS_DOCUMENTO D"   + 
					"      ,COMCAT_PYME PYM "   +
					"      ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"      ,COMREL_PRODUCTO_EPO PE"   +
					"      ,COM_LINEA_CREDITO LC"   +
					"      ,COMCAT_EPO E"   +
					"      ,COM_TIPO_CAMBIO TC"   +
					"      ,COMCAT_IF I"   +
					"      ,COMCAT_MONEDA M"   +
					"	   ,COMCAT_MONEDA M2"+
					"      ,COMCAT_ESTATUS_DOCTO ED"   +
					"      ,COMCAT_PRODUCTO_NAFIN PN"   +
					"      ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"      ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"			 ,COMREL_PYME_EPO_X_PRODUCTO PEP"		+//se agraga tabla para traer el plazo(02/05/2008)
					"      , comrel_producto_if  pi  "+ //F020-2015
					
        	"    WHERE E.IC_EPO = PE.IC_EPO"   +
					"			 AND PYM.IC_PYME = PEP.IC_PYME"		+//se agraga condicion para traer el plazo(02/05/2008)
					"			 AND E.IC_EPO = PEP.IC_EPO"		+//se agraga condicion para traer el plazo(02/05/2008)
					//"			 AND PEP.IG_PLAZO_PYME IS NOT NULL"		+//se agraga condicion para considerar plazos no nulos(05/05/2008)
					//"			 AND PEP.IG_PLAZO_PYME > 0"		+//se agraga condicion para traer plazos > 0 --(05/05/2008)
				   " 			 AND PEP.ic_producto_nafin = 4 "+
					"      AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"      AND PYM.IC_PYME = D.IC_PYME"   +
					"      AND D.IC_EPO = PE.IC_EPO"   +
					"      AND D.IC_MONEDA = M.IC_MONEDA"   +
					"      AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"	   AND M2.IC_MONEDA = LC.IC_MONEDA"	+
					"      AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"      AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"      AND LC.IC_IF = I.IC_IF"   +
					"      AND LC.IC_EPO = E.IC_EPO"   +
					"      AND LC.IC_PYME = D.IC_PYME"   +
					"      AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
					"      AND D.ic_estatus_docto = 2"   +
					"      AND IEP.IC_IF = I.IC_IF"   +
					"      AND IEP.IC_EPO = E.IC_EPO"   +
					"      AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"      AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" 	   AND ("   +		//SI LA LINEA ES EN DOLARES, SOLO TRAE DOCUMENTOS EN DOLARES, PERO SI ES M.N. TRAE TODOS
					" 	 	 LC.IC_MONEDA = D.IC_MONEDA"   +
					" 		 OR "   +
					" 		 LC.IC_MONEDA = 1"   +
					" 	 	 )"  +
					"      AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.CG_VENTACARTERA is null "+
					"      AND D.IC_PYME = "+ic_pyme+
          			condicion+
					"  AND lc.ic_if = pi.ic_if  "+
					"  AND pi.ic_producto_nafin = 4  "+  
          "  ORDER BY D.IG_NUMERO_DOCTO"  ;


          System.out.println("qrySentencia::: ----->  "+qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*10*/			vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*11*/			vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
/*12*/			vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/			vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*14*/			vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/			vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*16*/			vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
/*17*/			vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
/*18*/			vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
/*19*/			vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*20*/			vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*21*/			vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*22*/			vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*23*/			vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*24*/			vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*25*/			vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
/*26*/			vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
/*27*/			vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
/*28*/			vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
/*29*/			vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*30*/			vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
/*31*/			vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*32*/			vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*33*/			vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
/*34*/			vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
/*35*/			vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*36*/			vecColumnas.add((rs.getString("DIAS_MINIMO")==null)?"0":rs.getString("DIAS_MINIMO"));
/*37*/			vecColumnas.add((rs.getString("DIAS_MAXIMO")==null)?"0":rs.getString("DIAS_MAXIMO"));
/*38*/			vecColumnas.add((rs.getString("LINEA_CREDITO")==null)?"0":rs.getString("LINEA_CREDITO"));
/*39*/			vecColumnas.add((rs.getString("IC_IF")==null)?"0":rs.getString("IC_IF"));
/*40*/			vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"0":rs.getString("NOMBRE_MLINEA"));
/*41*/			vecColumnas.add((rs.getString("NOMBREPYME")==null)?"0":rs.getString("NOMBREPYME"));
/*42*/			vecColumnas.add((rs.getString("PLAZO_VALIDO")==null)?"0":rs.getString("PLAZO_VALIDO"));
/*43*/			vecColumnas.add((rs.getString("PLAZO_PYME")==null)?"0":rs.getString("PLAZO_PYME"));
/*44*/			vecColumnas.add((rs.getString("plazofinanciamiento")==null)?"0":rs.getString("plazofinanciamiento"));
/*45*/			vecColumnas.add((rs.getString("interes")==null)?"0":rs.getString("interes"));
/*46*/			vecColumnas.add((rs.getString("credito")==null)?"0":rs.getString("credito"));
/*47*/			vecColumnas.add((rs.getString("fechaVencimiento")==null)?"0":rs.getString("fechaVencimiento"));
/*48*/			vecColumnas.add((rs.getString("ic_epo")==null)?"0":rs.getString("ic_epo"));
/*49*/			vecColumnas.add((rs.getString("plazopyme")==null)?"0":rs.getString("plazopyme"));
/*50*/			vecColumnas.add((rs.getString("plazoepo")==null)?"0":rs.getString("plazoepo"));
/*51*/			vecColumnas.add((rs.getString("plazonafin")==null)?"0":rs.getString("plazonafin"));
/*52*/			vecColumnas.add((rs.getString("FECHA_DIAS_OPERACION")==null)?"0":rs.getString("FECHA_DIAS_OPERACION"));
/*53*/			vecColumnas.add((rs.getString("DIAS_OPERACION")==null)?"0":rs.getString("DIAS_OPERACION"));
/*54*/			vecColumnas.add((rs.getString("MESES_INTERESES")==null)?"0":rs.getString("MESES_INTERESES"));
/*55*/			vecColumnas.add((rs.getString("TIPOPAGO")==null)?"1":rs.getString("TIPOPAGO")); 						 
/*56*/			vecColumnas.add((rs.getString("FECHA_VEN_MESES")==null)?"":rs.getString("FECHA_VEN_MESES")); 
/*57*/         vecColumnas.add((rs.getString("CG_LINEA_CREDITO")==null)?"":rs.getString("CG_LINEA_CREDITO")); 
/*58*/         vecColumnas.add((rs.getString("FECHA_VENC_LINEA")==null)?"":rs.getString("FECHA_VENC_LINEA")); 
/*59*/			vecColumnas.add((rs.getString("PLAZO_VEN_MESES")==null)?"":rs.getString("PLAZO_VEN_MESES")); 
		  	   			 

            vecFilas.add(vecColumnas);
						//System.out.println("plazo_valido::::::::::::"+rs.getString("PLAZO_VALIDO"));
            }
 
                con.cierraStatement();
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                  con.terminaTransaccion(ok);
                 	con.cierraConexionDB();
                }
		System.out.println("AceptacionPymeBean::consultaDoctosCCC (S)");
        }
   return vecFilas;
   }


   public Vector consultaDoctosCCC(String cc_acuse)
   throws NafinException{
	System.out.println("AceptacionPymeBean::consultaDoctosCCC (E)");
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();

                qrySentencia =   "    SELECT E.CG_RAZON_SOCIAL AS EPO"   +
					"      ,D.IG_NUMERO_DOCTO"   +
					"      ,D.CC_ACUSE"   +
					"      ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION"   +
					"      ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC"   +
					"      ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION"   +
					"      ,D.IG_PLAZO_DOCTO AS PLAZO"   +
					"      ,M.CD_NOMBRE AS MONEDA"   +
					"      ,D.FN_MONTO AS MONTO"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,''),'') as TIPO_CAMBIO"   +
					"      ,'' as MONTO_VALUADO"   +
					"      ,D.ig_plazo_descuento"   +
					"      ,D.fn_porc_descuento"   +
					"      ,TF.cd_descripcion as MODO_PLAZO"   +
					"      ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"      ,'' AS CAMBIOS"   +
					"      ,D.IC_DOCUMENTO AS NUMERO_CREDITO"   +
					"      ,I.CG_RAZON_SOCIAL AS IF"   +
					"      ,DECODE(LC.CG_TIPO_SOLICITUD,'I','Inicial','A','Ampliacion','R','Renovacion','') as TIPO_SOLICITUD"   +
					"      ,'' AS FECHA_OPERACION"   +
					"      ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"      ,D.IG_PLAZO_CREDITO"   +
					"      ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
					"      ,CT.CD_NOMBRE AS REFERENCIA_INT"   +
					"      ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
					"      , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES"   +
					"      ,decode(TF.ic_tipo_financiamiento,2,0,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100)) as MONTO_DESCUENTO"   +
					"      ,ds.FN_VALOR_TASA AS VALOR_TASA"   +
					"      ,ds.CG_REL_MAT AS REL_MAT"   +
					"      ,ds.FN_PUNTOS AS PUNTOS"   +
					"      ,I.IG_TIPO_PISO"   +
					"      ,TCI.ic_tipo_cobro_interes"   +
					"      ,M.IC_MONEDA"   +
					"      ,CT.ic_tasa"   +
					"      ,TF.ic_tipo_financiamiento"   +
					"      ,LC.ic_moneda as MONEDA_LINEA"   +
					"      ,nvl(pe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO"   +
					"      ,nvl(pe.ig_dias_maximo,pn.in_dias_maximo) as DIAS_MAXIMO"   +
					"      ,LC.ic_linea_credito as LINEA_CREDITO"   +
					"      ,DS.fn_importe_interes as MONTO_INTERES"   +
					"      , to_char(ca.df_fecha_hora,'dd/mm/yyyy') as fecha_carga"   +
					"      , to_char(ca.df_fecha_hora,'hh:mi') as hora_carga"   +
					"      , ca.ic_usuario as usuario"   +
					"	   , M2.cd_nombre as NOMBRE_MLINEA"+
					"	   , PYM.CG_RAZON_SOCIAL AS NOMBREPYME, "+
											
					"    d.IG_MESES_INTERES as MESES_INTERESES,  "+ //F09-2015
					"    d.ig_tipo_pago AS TIPOPAGO   , "+//F09-2015
				  "    TO_char (ADD_MONTHS (TRUNC(SYSDATE), d.ig_meses_interes ), 'DD/MM/YYYY')  AS FECHA_VEN_MESES,  " +
				  "   fn_dias_entre2_fechas (  ADD_MONTHS (TRUNC(SYSDATE), d.ig_meses_interes ) , TRUNC(SYSDATE)  )   AS PLAZO_VEN_MESES " + 
						    
						  
					"    FROM DIS_DOCUMENTO D"   +
					"      ,DIS_DOCTO_SELECCIONADO DS"   +
					"      ,COMCAT_PYME PYM"   +
					"      ,COMCAT_EPO E"   +
					"      ,COMREL_PRODUCTO_EPO PE"   +
					"      ,COMCAT_PRODUCTO_NAFIN PN"   +
					"      ,COMCAT_MONEDA M"   +
					"      ,COM_TIPO_CAMBIO TC"   +
					"      ,COMCAT_ESTATUS_DOCTO ED"   +
					"      ,COM_LINEA_CREDITO LC"   +
					"      ,COMCAT_IF I"   +
					"      ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"      ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"      ,COMCAT_TASA CT"   +
					"      ,COMCAT_PLAZO CP "   +
					"      ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"      ,COM_ACUSE2 CA"   +
					"	   ,COMCAT_MONEDA M2"+
					"    WHERE E.IC_EPO = PE.IC_EPO"   +
					"      AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
					"      AND PYM.IC_PYME = D.IC_PYME"   +
					"      AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"      AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.IC_EPO = PE.IC_EPO"   +
					"      AND D.IC_MONEDA = M.IC_MONEDA"   +
					"      AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"	   AND M2.IC_MONEDA = LC.IC_MONEDA"+
					"      AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"      AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"      AND LC.IC_IF = I.IC_IF"   +
					"      AND LC.IC_EPO = E.IC_EPO"   +
					"      AND LC.IC_PYME = D.IC_PYME"   +
					"      AND LC.IC_LINEA_CREDITO = D.IC_LINEA_CREDITO"   +
					"      AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
					"      AND IEP.IC_IF = I.IC_IF"   +
					"      AND IEP.IC_EPO = E.IC_EPO"   +
					"      AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"      AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"      AND CT.IC_PLAZO = CP.IC_PLAZO"   +
					"      AND CT.IC_MONEDA = LC.IC_MONEDA"   +
					"      AND CT.CS_DISPONIBLE = 'S'"   +
					"      AND DS.IC_TASA = CT.IC_TASA"   +
					"      AND DS.CC_ACUSE = CA.CC_ACUSE"   +
					"     AND DS.CC_ACUSE = '"+cc_acuse+"'"+
					"  ORDER BY D.IG_NUMERO_DOCTO"  ;


                System.out.println(qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*10*/			vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*11*/			vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
/*12*/			vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/			vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*14*/			vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/			vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*16*/			vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
/*17*/			vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
/*18*/			vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
/*19*/			vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*20*/			vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*21*/			vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*22*/			vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*23*/			vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*24*/			vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*25*/			vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
/*26*/			vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
/*27*/			vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
/*28*/			vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
/*29*/			vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*30*/			vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
/*31*/			vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*32*/			vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*33*/			vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
/*34*/			vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
/*35*/			vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*36*/			vecColumnas.add((rs.getString("DIAS_MINIMO")==null)?"0":rs.getString("DIAS_MINIMO"));
/*37*/			vecColumnas.add((rs.getString("DIAS_MAXIMO")==null)?"0":rs.getString("DIAS_MAXIMO"));
/*38*/			vecColumnas.add((rs.getString("LINEA_CREDITO")==null)?"0":rs.getString("LINEA_CREDITO"));
/*39*/			vecColumnas.add((rs.getString("MONTO_INTERES")==null)?"0":rs.getString("MONTO_INTERES"));
/*40*/			vecColumnas.add(acuseFormateado);
/*41*/			vecColumnas.add((rs.getString("FECHA_CARGA")==null)?"":rs.getString("FECHA_CARGA"));
/*42*/			vecColumnas.add((rs.getString("HORA_CARGA")==null)?"":rs.getString("HORA_CARGA"));
/*43*/			vecColumnas.add((rs.getString("USUARIO")==null)?"":rs.getString("USUARIO"));
/*44*/			vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"":rs.getString("NOMBRE_MLINEA"));
/*45*/			vecColumnas.add((rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME"));
/*46*/         vecColumnas.add((rs.getString("MESES_INTERESES")==null)?"":rs.getString("MESES_INTERESES"));
/*47*/         vecColumnas.add((rs.getString("TIPOPAGO")==null)?"":rs.getString("TIPOPAGO"));
						 
                vecFilas.add(vecColumnas);
                }
                con.cierraStatement();
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("AceptacionPymeBean::consultaDoctosCCC (S)");
        }
   return vecFilas;
   }





   private void insertaAcuse(
	String  acuse,
   	String	totalMontoMN,
	String	totalInteresMN,
	String	totalMontoImpMN,
	String	totalMontoUSD,
	String	totalInteresUSD,
	String	totalMontoImpUSD,
	String	icUsuario,
	String	cgReciboElectronico,
        AccesoDB con)
   throws NafinException{
		System.out.println("AceptacionPymeBean::insertaAcuse (E)");
        String qrySentencia = "";
   	try{
		qrySentencia =	" INSERT INTO COM_ACUSE2"+
                		" (cc_acuse,fn_total_monto_mn,fn_total_int_mn,fn_total_imp_mn"+
                                " ,fn_total_monto_dl,fn_total_int_dl,fn_total_imp_dl"+
                                " ,ic_usuario,df_fecha_hora,cg_recibo_electronico,ic_producto_nafin)"+
                                " values('"+acuse.toString()+"',"+totalMontoMN+","+totalInteresMN+","+totalMontoImpMN +
                                " ,"+totalMontoUSD+","+totalInteresUSD+","+totalMontoImpUSD+
                                " ,'"+icUsuario+"',sysdate,'"+cgReciboElectronico+"',4)";
                con.ejecutaSQL(qrySentencia);
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }
		System.out.println("AceptacionPymeBean::insertAcuse (S)");
   }

   public String confirmaPedido(
		String	totalMontoMN,
		String	totalInteresMN,
		String	totalMontoImpMN,
		String	totalMontoUSD,
		String	totalInteresUSD,
		String	totalMontoImpUSD,
		String	icUsuario,
		String	cgReciboElectronico,
		String	ic_documento[],
		String	ic_tasa[],
		String	cg_rel_mat[],
		String	fn_puntos[],
		String	fn_valor_tasa[],
		String	fn_importe_interes[],
		String	fn_importe_recibir[],
	    String 	plazo_credito[],
	    String	fecha_vto[],
		String	tipo_tasa[],
		String	puntos_pref[] ,
		String	fn_montoValuado [] )  throws NafinException{
		System.out.println("AceptacionPymeBean::confirmaPedido (E)");
		AccesoDB con = null;
		String qrySentencia = "";
		Acuse       	acuse = null;
		ResultSet   	rs = null;
		int             aux = 0;
		boolean ok = true;
		try{
			con = new AccesoDB();
			con.conexionDB();
			acuse = new Acuse(2,"4","com",con);
			acuseFormateado  = acuse.formatear();
			insertaAcuse(acuse.toString(),totalMontoMN,totalInteresMN,totalMontoImpMN,totalMontoUSD,totalInteresUSD,totalMontoImpUSD,icUsuario,cgReciboElectronico,con);
			for(int i=0;i<ic_documento.length;i++) {
				aux = 0;
				qrySentencia =
					" select count(1) from dis_docto_seleccionado"+
					" where ic_documento = "+ic_documento[i];
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					aux = rs.getInt(1);
				con.cierraStatement();
					String Puntos ="";
				if(puntos_pref[i]==null) {  Puntos ="";  }

				if(aux==0) {
					qrySentencia =
						" insert into dis_docto_seleccionado"+
						" (ic_documento,cc_acuse,ic_tasa,cg_rel_mat"+
						" ,fn_puntos,fn_valor_tasa,fn_importe_interes"+
						" ,fn_importe_recibir,df_fecha_seleccion,cg_tipo_tasa,fn_puntos_pref, fn_monto_valuado )"+
						" values("+ic_documento[i]+",'"+acuse.toString()+"',"+ic_tasa[i]+",'"+cg_rel_mat[i]+"'"+
						" ,"+fn_puntos[i]+","+fn_valor_tasa[i]+","+fn_importe_interes[i]+","+fn_importe_recibir[i]+
						" ,sysdate,'"+tipo_tasa[i]+"','"+Puntos+"', "+fn_montoValuado[i] +")";
						System.out.println("El insert = \n"+ qrySentencia);
				} else {
					qrySentencia =
						" update dis_docto_seleccionado"+
						" set cc_acuse = '"+acuse.toString()+"'"+
						" ,ic_tasa = "+ic_tasa[i]+
						" ,cg_rel_mat = '"+cg_rel_mat[i]+"'"+
						" ,fn_puntos = "+fn_puntos[i]+
						" ,fn_valor_tasa = "+fn_valor_tasa[i]+
						" ,fn_importe_interes = "+fn_importe_interes[i]+
						" ,fn_importe_recibir = "+fn_importe_recibir[i]+
						" ,df_fecha_seleccion = sysdate"+
						" ,cg_tipo_tasa = '"+tipo_tasa[i]+"'"+
						" ,fn_puntos_pref = '"+Puntos+"'"+
						" , fn_monto_valuado =   "+fn_montoValuado[i]+
						" where ic_documento = "+ic_documento[i];
						System.out.println("El update dis_docto_seleccionado = \n"+ qrySentencia);
				}
				con.ejecutaSQL(qrySentencia);

				qrySentencia =
					"UPDATE dis_documento " +
					"   SET (ic_estatus_docto, ig_plazo_credito, df_fecha_venc_credito, fn_tipo_cambio) = " +
					"          (SELECT 3, "+plazo_credito[i]+", "+"to_date('"+fecha_vto[i]+"','dd/mm/yyyy')"+", DECODE (ic_moneda, 1, 1, fn_valor_compra) "+
					"             FROM com_tipo_cambio tc " +
					"            WHERE ic_moneda = (SELECT ic_moneda " +
					"                                 FROM dis_documento " +
					"                                WHERE ic_documento = "+ic_documento[i]+") AND dc_fecha = (SELECT MAX (dc_fecha) " +
					"                                                                            FROM com_tipo_cambio " +
					"                                                                           WHERE ic_moneda = tc.ic_moneda)) " +
					" WHERE ic_documento = "+ic_documento[i];
/*					"update dis_documento set "+
					" (ic_estatus_docto,ig_plazo_credito,df_fecha_venc_credito,fn_tipo_cambio) = "+
					" ( select 3,"+plazo_credito[i]+",to_date('"+fecha_vto[i]+"','dd/mm/yyyy')"+
					" ,decode(ic_moneda,1,1,fn_valor_compra) from com_tipo_cambio tc where ic_moneda =(select ic_moneda from dis_documento where ic_documento ="+ic_documento[i]+")"+
					" and dc_fecha = (select max(dc_fecha) from com_tipo_cambio where ic_moneda = tc.ic_moneda)"+
					") where ic_documento = "+ic_documento[i];*/
System.out.println("El update dis_documento  = \n"+ qrySentencia);
					con.ejecutaSQL(qrySentencia);
			}

		  } catch (SQLException sqle) {
			 System.out.println("sqle.getErrorCode() ---->"+sqle.getErrorCode());
				if (sqle.getErrorCode()==20001) {
				System.out.println("Excepcion "+sqle.getMessage());
					throw new NafinException(sqle.getMessage());
				}
		  } catch(Exception e){
        	ok = false;
			System.out.println("Excepcion "+e);
			throw new NafinException("Hubo error en la confirmaci�n del documento");
        }finally{
        	if(con.hayConexionAbierta()){
                	con.terminaTransaccion(ok);
                 	con.cierraConexionDB();
                }
			System.out.println("AceptacionPymeBean::confirmaPedido (S) ");
        }
		return acuse.toString();
	}

	public	String confirmaDoctoCCC(
		String totalMontoMN,
		String totalInteresMN,
		String totalMontoImpMN,
		String totalMontoUSD,
		String totalInteresUSD,
		String totalMontoImpUSD,
		String icUsuario,
		String cgReciboElectronico,
		String ic_documento[],
		String ic_tasa[],
		String cg_rel_mat[],
		String fn_puntos[],
		String fn_valor_tasa[],
		String fn_importe_interes[],
		String fn_importe_recibir[],
		String plazo_credito[],
		String fecha_vto[],
		String ic_linea_credito[]
	) throws NafinException{
		String strAcuse = "";
		try{
			strAcuse = confirmaDoctoCCC(
						totalMontoMN,
						totalInteresMN,
						totalMontoImpMN,
						totalMontoUSD,
						totalInteresUSD,
						totalMontoImpUSD,
						icUsuario,
						cgReciboElectronico,
						ic_documento,
						ic_tasa,
						cg_rel_mat,
						fn_puntos,
						fn_valor_tasa,
						fn_importe_interes,
						fn_importe_recibir,
						plazo_credito,
						fecha_vto,
						ic_linea_credito, "");

        }catch(Exception e){
			System.out.println("Excepcion "+e);
			e.printStackTrace();
        }
		return strAcuse;
	}

	public	String confirmaDoctoCCC(
		String totalMontoMN,
		String totalInteresMN,
		String totalMontoImpMN,
		String totalMontoUSD,
		String totalInteresUSD,
		String totalMontoImpUSD,
		String icUsuario,
		String cgReciboElectronico,
		String ic_documento[],
		String ic_tasa[],
		String cg_rel_mat[],
		String fn_puntos[],
		String fn_valor_tasa[],
		String fn_importe_interes[],
		String fn_importe_recibir[],
		String plazo_credito[],
		String fecha_vto[],
		String ic_linea_credito[],
		String tipoUsuario
	) throws NafinException{
		String strAcuse = "";
		try{
			strAcuse = confirmaDoctoCCC(
						totalMontoMN,
						totalInteresMN,
						totalMontoImpMN,
						totalMontoUSD,
						totalInteresUSD,
						totalMontoImpUSD,
						icUsuario,
						cgReciboElectronico,
						ic_documento,
						ic_tasa,
						cg_rel_mat,
						fn_puntos,
						fn_valor_tasa,
						fn_importe_interes,
						fn_importe_recibir,
						plazo_credito,
						fecha_vto,
						ic_linea_credito,
						tipoUsuario, "",null);

        }catch(Exception e){
			System.out.println("Excepcion "+e);
			e.printStackTrace();
        }
		return strAcuse;
	}

	public   String confirmaDoctoCCC(
		String totalMontoMN,
		String totalInteresMN,
		String totalMontoImpMN,
		String totalMontoUSD,
		String totalInteresUSD,
		String totalMontoImpUSD,
		String icUsuario,
		String cgReciboElectronico,
		String ic_documento[],
		String ic_tasa[],
		String cg_rel_mat[],
		String fn_puntos[],
		String fn_valor_tasa[],
		String fn_importe_interes[],
		String fn_importe_recibir[],
		String plazo_credito[],
		String fecha_vto[],
		String ic_linea_credito[],
		String tipoUsuario,
		String ordenPagoTC,
		HashMap hmInfoTransTC)
			throws NafinException{
		System.out.println("AceptacionPymeBean::confirmaDoctoCCC (E)");
		AccesoDB con = null;
		String qrySentencia = "";
		Acuse          acuse = null;
	 Acuse         acuse3 = null;
		ResultSet      rs = null;
		int             aux = 0;
		boolean ok = true;
		try{
			con = new AccesoDB();
			con.conexionDB();
			if("IF".equals(tipoUsuario))
				acuse = new Acuse(6,"4","com",con);
			else
				acuse = new Acuse(2,"4","com",con);
			acuseFormateado  = acuse.formatear();
			insertaAcuse(acuse.toString(),totalMontoMN,totalInteresMN,totalMontoImpMN,totalMontoUSD,totalInteresUSD,totalMontoImpUSD,icUsuario,cgReciboElectronico,con);
		if(!"".equals(ordenPagoTC)){
		  acuse3 = new Acuse(3,"4","com",con);
		  insertaAcuse3(acuse3.toString(),totalMontoMN,totalInteresMN,totalMontoImpMN,totalMontoUSD,totalInteresUSD,totalMontoImpUSD,icUsuario,cgReciboElectronico,con);
		}
			for(int i=0;i<ic_documento.length;i++){
				aux = 0;
				qrySentencia =
					" select count(*) from dis_docto_seleccionado"+
					" where ic_documento = "+ic_documento[i];
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					aux = rs.getInt(1);
				con.cierraStatement();
				if("".equals(ordenPagoTC)){
				if(aux==0) {
					qrySentencia =
						" insert into dis_docto_seleccionado"+
						" (ic_documento,cc_acuse,ic_tasa,cg_rel_mat"+
						" ,fn_puntos,fn_valor_tasa,fn_importe_interes"+
						" ,fn_importe_recibir,df_fecha_seleccion)"+
						" values("+ic_documento[i]+",'"+acuse.toString()+"',"+ic_tasa[i]+",'"+cg_rel_mat[i]+"'"+
						" ,"+fn_puntos[i]+","+fn_valor_tasa[i]+","+fn_importe_interes[i]+","+fn_importe_recibir[i]+
						" ,sysdate)";
				} else {
					qrySentencia =
						" update dis_docto_seleccionado"+
						" set cc_acuse = '"+acuse.toString()+"'"+
						" ,ic_tasa = "+ic_tasa[i]+
						" ,cg_rel_mat = '"+cg_rel_mat[i]+"'"+
						" ,fn_puntos = "+fn_puntos[i]+
						" ,fn_valor_tasa = "+fn_valor_tasa[i]+
						" ,fn_importe_interes = "+fn_importe_interes[i]+
						" ,fn_importe_recibir = "+fn_importe_recibir[i]+
						" ,df_fecha_seleccion = sysdate"+
						" where ic_documento = "+ic_documento[i];
				}
				con.ejecutaSQL(qrySentencia);
				}else if(!"".equals(ordenPagoTC)){
					String fnComision = "";
					String ic_epo = "";
					
					qrySentencia =
						" select ic_epo from dis_documento "+
						" where ic_documento = "+ic_documento[i];
					rs = con.queryDB(qrySentencia);
					if(rs.next())
						ic_epo = rs.getString(1);
					con.cierraStatement();
					
					qrySentencia =
						" select FN_TASA_DESCUENTO from comrel_producto_epo "+
						" where IC_PRODUCTO_NAFIN = 4 "+
						" and ic_epo = "+ic_epo;
					rs = con.queryDB(qrySentencia);
					if(rs.next())
						fnComision = rs.getString(1);
					con.cierraStatement();
					
					if(aux==0) {
						qrySentencia =
							" insert into dis_docto_seleccionado"+
							" (ic_documento,cc_acuse, fn_importe_recibir,df_fecha_seleccion, fn_porc_comision_apli, ic_tasa)"+
							" values("+ic_documento[i]+",'"+acuse.toString()+"',"+fn_importe_recibir[i]+
							" ,sysdate, "+ fnComision+", 999)";
					} else {
						qrySentencia =
							" update dis_docto_seleccionado"+
							" set cc_acuse = '"+acuse.toString()+"'"+
							" ,ic_tasa = 999 "+
							" ,cg_rel_mat = null "+
							" ,fn_puntos = null "+
							" ,fn_valor_tasa = null "+
							" ,fn_importe_interes = null "+
							" ,fn_importe_recibir = "+fn_importe_recibir[i]+
							" ,df_fecha_seleccion = sysdate"+
							" ,fn_porc_comision_apli = "+fnComision+
							" where ic_documento = "+ic_documento[i];
					}
					con.ejecutaSQL(qrySentencia);
				}

				System.out.println("\n qrySentencia: "+qrySentencia);
				if("".equals(ordenPagoTC)){
					qrySentencia =
					"update dis_documento set "+
					" ( ic_estatus_docto,ig_plazo_credito,df_fecha_venc_credito,fn_tipo_cambio,ic_linea_credito) = "+
					" ( select  3,"+plazo_credito[i]+",to_date('"+fecha_vto[i]+"','dd/mm/yyyy')"+
					" ,decode(ic_moneda,1,1,fn_valor_compra),"+ic_linea_credito[i]+" from com_tipo_cambio tc where ic_moneda =(select ic_moneda from dis_documento where ic_documento ="+ic_documento[i]+")"+
					" and dc_fecha = (select max(dc_fecha) from com_tipo_cambio where ic_moneda = tc.ic_moneda)"+
					") where ic_documento = "+ic_documento[i];
				}else if(!"".equals(ordenPagoTC)){
					qrySentencia =
										"update dis_documento set "+
										" (ig_tipo_pago, ic_estatus_docto,ig_plazo_credito,df_fecha_venc_credito,fn_tipo_cambio, ic_bins, ic_linea_credito, ic_orden_pago_tc) = "+
										" ( select 3, 32,"+plazo_credito[i]+",to_date('"+fecha_vto[i]+"','dd/mm/yyyy')"+
										" ,decode(ic_moneda,1,1,fn_valor_compra),"+ic_linea_credito[i]+", 0, '"+ordenPagoTC+"' from com_tipo_cambio tc where ic_moneda =(select ic_moneda from dis_documento where ic_documento ="+ic_documento[i]+")"+
										" and dc_fecha = (select max(dc_fecha) from com_tipo_cambio where ic_moneda = tc.ic_moneda)"+
										") where ic_documento = "+ic_documento[i]+
										" and ic_orden_pago_tc = "+ordenPagoTC;
				}
				System.out.println("\n qrySentencia: "+qrySentencia);
				con.ejecutaSQL(qrySentencia);
				
				if(!"".equals(ordenPagoTC)){
					//acuse3 = new Acuse(3,"4","com",con);
					//insertaAcuse3(acuse.toString(),totalMontoMN,totalInteresMN,totalMontoImpMN,totalMontoUSD,totalInteresUSD,totalMontoImpUSD,icUsuario,cgReciboElectronico,con);
					qrySentencia = "INSERT INTO dis_solicitud " +
							"            (ic_documento, ic_estatus_solic, cc_acuse, ic_clase_docto, " +
							"             cg_rmif, in_stif, df_v_credito, df_ppc, df_ppi, in_dia_pago, " +
							"             ic_tasaif, ic_tipo_credito, ic_if, df_fecha_solicitud, " +
							"             cs_factoraje_con_rec, df_operacion) " +
							"   SELECT d.ic_documento, 14, '"+acuse3.toString()+"', pd.ic_clase_docto, '+', 0, " +
							"          d.df_fecha_venc_credito, d.df_fecha_venc_credito, " +
							"          d.df_fecha_venc_credito, " +
							"          TO_NUMBER (TO_CHAR (d.df_fecha_venc_credito, 'dd')), ds.ic_tasa, " +
							"          DECODE (cbi.ic_moneda, 1, 211, 54, 221), cbi.ic_if, SYSDATE, 'N', SYSDATE " +
							"     FROM dis_documento d, " +
							"          comrel_producto_docto pd, " +
							"          dis_docto_seleccionado ds, " +
							"          com_bins_if cbi " +
							"    WHERE d.ic_epo = pd.ic_epo " +
							"      AND d.ic_producto_nafin = pd.ic_producto_nafin " +
							"      AND d.ic_documento = ds.ic_documento " +
							"      AND d.ic_bins = cbi.ic_bins " +
							"      AND d.ic_documento = "+ic_documento[i];
					
					System.out.println("\n qrySentencia: "+qrySentencia);
					con.ejecutaSQL(qrySentencia);
				}
				
			}
			 
			//actuailza datos de la transaccion Pago TC
			if(!"".equals(ordenPagoTC)){
					String infoTCmonto = (String)hmInfoTransTC.get("MONTO_TOTAL");
					String infoTCtransaccion = (String)hmInfoTransTC.get("ID_TRANSACCION");
					String infoTCcodResp = (String)hmInfoTransTC.get("CODIGO_RESPUESTA");
					String infoTCcodDesc = (String)hmInfoTransTC.get("CODIGO_DESC");
					String infoTCcodAut = (String)hmInfoTransTC.get("CODIGO_AUTORIZACION");
					String infoTCestatus = (String)hmInfoTransTC.get("ESTATUS_TRANSACCION");
			 String infoTCdigits = (String)hmInfoTransTC.get("ULTIMOS_DIGIT_TC");
			
				aux = 0;
				qrySentencia = "SELECT count(1) " +
									"  FROM dis_doctos_pago_tc " +
									" WHERE ic_orden_pago_tc = '"+ordenPagoTC+"'";
				
				rs = con.queryDB(qrySentencia);
				if(rs.next())
					aux = rs.getInt(1);
				con.cierraStatement();
				if(aux>0){

					qrySentencia = "UPDATE dis_doctos_pago_tc " +
									"   SET fn_monto_total = "+totalMontoImpMN+", " +
									"       cg_transaccion_id = '"+infoTCtransaccion+"'," +
									"       cg_codigo_autorizacion = '"+infoTCcodAut+"'," +
									"       cg_codigo_resp = '"+infoTCcodResp+"'," +
									"       cg_codigo_desc = '"+infoTCcodDesc+"'," +
									"       df_fecha_autorizacion = SYSDATE, " +
									"       cs_estatus_transaccion = '"+infoTCestatus+"', "+
						"       cg_num_tarjeta = '"+infoTCdigits+"' "+
									" WHERE ic_orden_pago_tc = '"+ordenPagoTC+"'";
					
					System.out.println("\n qrySentencia: "+qrySentencia);
					con.ejecutaSQL(qrySentencia);
				}
			}

			
		  }catch(Exception e){
			ok = false;
		System.out.println("Excepcion "+e);
		  }finally{
			if(con.hayConexionAbierta()){
						con.terminaTransaccion(ok);
						con.cierraConexionDB();
					 }
		System.out.println("AceptacionPymeBean::confirmaDoctoCCC (S)");
		  }
		return acuse.toString();
	}



   public void cambiaTipoFinanciamiento(String ic_pyme)
   throws NafinException{
	System.out.println("AceptacionPymeBean::cambiaTipoFinanciamiento (E)");
   	AccesoDB 	con = null;
        String		qrySentencia = "";
        int    		numVencidos = 0;
        boolean		ok = true;
        ResultSet 	rs =  null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();
                qrySentencia =
					" UPDATE dis_documento"   +
					"    SET ic_tipo_financiamiento = 2,"   +
					"        fn_porc_descuento = 0"   +
					"  WHERE ic_documento IN"   +
					"           (SELECT ic_documento"   +
					"              FROM dis_documento doc, comrel_producto_epo cpe"   +
					"             WHERE doc.ic_epo = cpe.ic_epo"   +
					"               AND ic_tipo_financiamiento IN (1,  3)"   +
					"               AND ic_estatus_docto = 2"   +
					"               AND ic_pyme = "+ic_pyme+" "+
					"               AND TRUNC (DECODE (cpe.cg_fecha_porc_desc, 'E', doc.df_fecha_emision, 'P', doc.df_carga)) +"   +
					"                   (doc.ig_plazo_descuento - 1) <"   +
					"                      TRUNC (SYSDATE)"   +
					"               AND cpe.ic_producto_nafin = 4)"  ;
                con.ejecutaSQL(qrySentencia);
                qrySentencia =
					" UPDATE dis_documento"   +
					"    SET ic_estatus_docto = 9"   +
					"  WHERE ic_documento IN"   +
					"           (SELECT ic_documento"   +
					"              FROM dis_documento doc, comrel_producto_epo cpe"   +
					"             WHERE doc.ic_epo = cpe.ic_epo"   +
					"               AND ic_tipo_financiamiento IN (4)"   +
					"               AND ic_estatus_docto = 2"   +
					" 				AND ic_pyme = "+ic_pyme+" "+
					"               AND TRUNC (DECODE (cpe.cg_fecha_porc_desc, 'E', doc.df_fecha_emision, 'P', doc.df_carga)) +"   +
					"                   doc.ig_plazo_descuento <"   +
					"                      TRUNC (SYSDATE)"   +
					"               AND cpe.ic_producto_nafin = 4)"  ;
                con.ejecutaSQL(qrySentencia);
                qrySentencia =
					" select count(*) from dis_documento "+
					" where ic_estatus_docto = 22"+
					" and ic_pyme = "+ic_pyme;
                rs = con.queryDB(qrySentencia);
                if(rs.next())
                	numVencidos = rs.getInt(1);
                con.cierraStatement();
                if(numVencidos > 0)
                	throw new NafinException("DIST0020");

        }catch(NafinException ne){
        	throw ne;
        }catch(Exception e){
        	ok = false;
		System.out.println("aceptacionPyme::CambiaTipoFinanciamiento Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                	con.terminaTransaccion(ok);
                 	con.cierraConexionDB();
                }
		System.out.println("AceptacionPymeBean::cambiaTipoFinanciamiento (S)");
        }
   }


   public String getDiasInhabiles(String ic_pyme,String ic_epo)
   throws NafinException{
	System.out.println("AceptacionPymeBean::getDiasInhabiles (E)");
   	AccesoDB 	con = null;
        String		qrySentencia = "";
        ResultSet 	rs =  null;
        String		retorno = "";
   	try{
        	con = new AccesoDB();
                con.conexionDB();
                qrySentencia =	" select cg_dia_inhabil from comcat_dia_inhabil where cg_dia_inhabil is not null ";
                if(!"".equals(ic_epo)){
			qrySentencia += " union "   +
				" select cg_dia_inhabil from comrel_dia_inhabil_x_epo"   +
				" where ic_epo = "+ic_epo+
				" and cg_dia_inhabil is not null";
                }
       System.out.println("qrySentencia::: "+qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	if(!"".equals(retorno))
                        	retorno += ",";
                        retorno += rs.getString(1);
                }
                con.cierraStatement();
        }catch(Exception e){
		System.out.println("aceptacionPyme::getDiasInHabiles Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("AceptacionPymeBean::getDiasInhabiles (S)");
        }
   return retorno;
   }



	public void validarUsuario(String esClavePyme, String esLogin, String esPassword,
		String esIP, String esHost, String esSistema )
        throws NafinException {
		System.out.println("SelecionPYME::validarUsuario(E)");
		AccesoDB conexion = new AccesoDB();

		try {
			com.netro.seguridadbean.Seguridad objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB",com.netro.seguridadbean.Seguridad.class);
			UtilUsr utils = new UtilUsr();
			boolean usaurioOK = utils.esUsuarioValido(esLogin, esPassword);
			if(!usaurioOK)
				throw new NafinException("DSCT0009");
			Usuario usuario = utils.getUsuario(esLogin);
			String perfil = usuario.getPerfil();

			try{
				objSeguridad.validaFacultad( (String) esLogin, (String) perfil, "PPYMDAUTOR" , "24PYME24AUTORIZA" , (String) esSistema, (String) esIP, (String) esHost );
			}catch( SeguException ErrorSeg){
				System.out.println("ERROR: validaFacultad(" + esLogin + "," +
						perfil + ",\"PPYMDAUTOR\",\"24PYME24AUTORIZA\"," +
						esSistema + "," +  esIP + "," + esHost+")");
				ErrorSeg.printStackTrace();
				throw new NafinException("DSCT0014");
			}

/*
			con = (SeguridadHome)  PortableRemoteObject.narrow(
							(new InitialContext()).lookup("SeguridadEJB"), SeguridadHome.class );
			objSeguridad = con.create();
			//Valida el Usuario por medio de Seguridad, para cargar en Memoria el usuario, si es el caso
			objSeguridad.validaUsuario(esLogin, esPassword, esIP, esHost );

			conexion.conexionDB();
            String perfil = "";
			lsQrySentencia = "SELECT cc_perfil ";
			lsQrySentencia += " FROM  U,  UP ";
			lsQrySentencia += " WHERE U.cg_login = '"+esLogin+"' ";
			lsQrySentencia += " AND U.cg_password = encripta_pwd('"+esLogin+"','"+esPassword+"') ";
			lsQrySentencia += " AND U.ic_usuario = UP.ic_usuario ";
			lsQrySentencia += " AND U.ic_pyme = " + esClavePyme;
			ResultSet rsSentencia = conexion.queryDB( lsQrySentencia );
			if(rsSentencia.next()) {
				perfil = rsSentencia.getString(1);
			} else {
				throw new NafinException("DSCT0009");
			}
			try{
				objSeguridad.validaFacultad( (String) esLogin, (String) perfil, "PPYMDAUTOR" , "24PYME24AUTORIZA" , (String) esSistema, (String) esIP, (String) esHost );
			}catch( SeguException ErrorSeg){
				throw new NafinException("DSCT0014");
			}
*/
		}catch( SeguException ErrorSeg){
			ErrorSeg.printStackTrace();
				throw new NafinException("DSCT0009");
		}catch (NafinException ne) {
			ne.printStackTrace();
			throw ne;
		}catch (Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraConexionDB();
			}
			System.out.println("AceptacionPyme::validarUsuario(S)");
		}
	}

    public void operaTipoCredito(String ic_pyme,String tipo_credito,int ic_producto_nafin,String ic_epo)
        throws NafinException{
		System.out.println("AceptacionPymeBean::operaTipoCredito (E)");
		AccesoDB con = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		String qrySentencia = "";
		int cuenta = 0;
   		try{
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =	" select count(*)"+
				" from comrel_pyme_epo_x_producto"+
				" where ic_pyme = ?"+
				" and ic_producto_nafin = ?"+
				" and cg_tipo_credito = ?";
			if(ic_epo!=null)
				qrySentencia += " and ic_epo = ?";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_pyme);
			ps.setInt(2,ic_producto_nafin);
			ps.setString(3,tipo_credito);
			if(ic_epo!=null)
				ps.setString(4,ic_epo);

			rs = ps.executeQuery();
			if(rs.next())
				cuenta = rs.getInt(1);

			ps.close();
			if(cuenta==0)
				throw new NafinException("DIST0015");

			}catch(NafinException ne){
                	throw ne;
        	}catch(Exception e){
			System.out.println("Excepcion "+e);
                        throw new NafinException("SIST0001");
        	}finally{
        		if(con.hayConexionAbierta()){
               		  	con.cierraConexionDB();
               	}
			System.out.println("AceptacionPymeBean::operaTipoCredito (S)");
        	}
     }

	public Vector monitorLineas(String ic_epo)
			throws NafinException{
		Vector vecFilas = new Vector();
		try {
			vecFilas = monitorLineas(ic_epo,"","", "");
		} catch(Exception e){
			System.out.println("AceptacionPymeBean::monitorLineas Exception "+e);
			throw new NafinException("SIST0001");
		}
		return vecFilas;
	}

	public Vector monitorLineas(String ic_epo, String ic_if, String ic_moneda, String ic_pyme )
			throws NafinException{
		System.out.println("AceptacionPymeBean::monitorLineas (E)");
		AccesoDB			con				= null;
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;		
		Vector				vecFilas 		= new Vector();
		Vector				vecColumnas 	= null;
	   List varBind =  new ArrayList();
		StringBuffer   qrySentencia = new StringBuffer();   
	      
	   try {
			con = new AccesoDB();
	      con.conexionDB();
	        
		   qrySentencia = new StringBuffer();   
		            
		         qrySentencia.append( 
		            " SELECT lcd.ic_linea_credito,"   +
		            "        lcd.ic_linea_credito || ' - ' || i.cg_razon_social || ' ' || DECODE (ic_moneda, 1, 'M.N.', 54, 'USD') AS descripcion,"   +
		            "        lcd.fn_monto_autorizado_total, lcd.fn_saldo_linea_total, lcd.ic_moneda"   +
		            "   FROM com_linea_credito lcd, comcat_if i"   +
		            "  WHERE lcd.ic_if = i.ic_if"   +
		            "    AND lcd.df_vencimiento_adicional > SYSDATE"   +
		            "    AND lcd.cg_tipo_solicitud = 'I'"   +
		            "    AND lcd.ic_estatus_linea = 12"   +
		            "    AND lcd.ic_producto_nafin = 4"  ); 
		            
		            if(!"".equals(ic_epo)){
		               qrySentencia.append( " AND lcd.ic_epo = ? ");
		               varBind.add( ic_epo );
		            }
		            
		            if(!"".equals(ic_if)){
		               qrySentencia.append( " AND lcd.ic_if = ? ");
		               varBind.add( ic_if );
		            }
		            
		            if(!"".equals(ic_moneda)){
		               qrySentencia.append(" AND lcd.ic_moneda = ? ");
		               varBind.add(ic_moneda);
		            }
		            if(!"".equals(ic_pyme)){
		               qrySentencia.append(" AND lcd.ic_pyme = ? ");
		               varBind.add(ic_pyme);
		            }
		            qrySentencia.append( "  ORDER BY lcd.ic_linea_credito "); ;
		            
		            
		         System.out.println ("================================== ");
		         System.out.println ("qrySentencia   "+qrySentencia);
		         System.out.println ("varBind   "+varBind);
		         System.out.println ("================================== "); 
		   ps = con.queryPrecompilado(qrySentencia.toString(),varBind );           
		   rs = ps.executeQuery();      
			
			while(rs.next()){
				vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("IC_LINEA_CREDITO")==null)?"":rs.getString("IC_LINEA_CREDITO"));
/*1*/			vecColumnas.add((rs.getString("DESCRIPCION")==null)?"":rs.getString("DESCRIPCION"));
/*2*/			vecColumnas.add((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
/*3*/			vecColumnas.add((rs.getString("FN_SALDO_LINEA_TOTAL")==null)?"":rs.getString("FN_SALDO_LINEA_TOTAL"));
/*4*/			vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
				vecFilas.add(vecColumnas);
			}
			rs.close();ps.close();
		} catch(Exception e){
			System.out.println("AceptacionPymeBean::monitorLineas Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AceptacionPymeBean::monitorLineas (S)");
		}
		return vecFilas;
	}


 public String getTipoCambio(String ic_pyme)
 throws NafinException{
	System.out.println("AceptacionPymeBean::getTipoCambio (E)");
 	AccesoDB	con = null;
        ResultSet 	rs = null;
        String          qrySentencia = "";
        String 		tipoCambio = "";
        try{
        	con = new AccesoDB();
                con.conexionDB();
               	qrySentencia = 	   " SELECT round(fn_valor_compra, 2) FROM COM_TIPO_CAMBIO " +
				   " WHERE ic_moneda = 54" +
				   " AND dc_fecha = (Select max(TC.dc_fecha) " +
				   " FROM COM_TIPO_CAMBIO TC " +
				   " WHERE TC.ic_moneda = 54)";
                rs = con.queryDB(qrySentencia);
               	if(rs.next())
	               	tipoCambio = rs.getString(1);
               	con.cierraStatement();
        }catch(Exception e){
        	tipoCambio = "";
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                	con.cierraConexionDB();
                }
		System.out.println("AceptacionPymeBean::getTipoCambio (S)");
        }
 return tipoCambio;
 }

	/*********************************************************************************************
	*
	* 	    void descuentoAutomaticoDist()
	*		Integraci�n del Proceso de Descuento Autom�tico de Distribuidores.
	*
	*********************************************************************************************/
	public void descuentoAutomaticoDist(String ruta)
			throws NafinException {
		AccesoDB lodbConexion = new AccesoDB();
		System.out.println(" AceptacionPymeEJB::descuentoAutomaticoDist(E)");
		boolean ejecutar = true;
		ResultSet rsCveProc=null;
		String lsCveProc="";
		try {
			// Abre la conexi�n a la base de datos
			lodbConexion.conexionDB();
			while (ejecutar) {
				validaDiaHabil(lodbConexion);
				String lsQuery =
					" SELECT cs_cierre_servicio, 'AceptacionPymeEJB::descuentoAutomaticoDist()'"   +
					"   FROM comcat_producto_nafin"   +
					"  WHERE ic_producto_nafin = 4"  ;
				PreparedStatement ps = lodbConexion.queryPrecompilado(lsQuery);
				ResultSet lrs3 = ps.executeQuery();
				if (lrs3.next()) {
					// Prosigue solo si no se encuentra cerrado el servicio
					if (lrs3.getString(1).equals("N")) {
						try {
							rsCveProc=lodbConexion.queryDB(" SELECT SEQ_COMCAT_RECHAZO_DSCTO.NEXTVAL FROM DUAL ");
							if (rsCveProc.next())
								lsCveProc=(rsCveProc.getString(1)==null)?"":rsCveProc.getString(1);
							if (lsCveProc.equals(""))
								throw new NafinException("DSCT0082");
							rsCveProc.close();
							lodbConexion.cierraStatement();
						} catch (SQLException sqle) {
							System.out.println("Error 1 en descuentoAutomaticoDist::"+sqle.getMessage());
							throw new NafinException("DSCT0082");
						} catch (NafinException ne) {
							throw ne;
						}
						selecAutoPymeDist(lsCveProc, lodbConexion,ruta);
						selecAutoPymeDistCCC(lsCveProc, lodbConexion,ruta);
						selecAutoPymeDistVDC(lsCveProc, lodbConexion,ruta);
						ejecutar = false;
					} else {
						System.out.println("Favor de Esperar, no esta abierto el horario de servicio, no existe tasa");
						Thread.sleep(1000*60*1);	//Dormir 1 minuto
						continue;	//Vuelve a evaluar la condici�n del while(ejecutar)
					}
				}
				lrs3.close();
				if (ps != null) ps.close();
				//lodbConexion.cierraStatement();
			} //fin del while(ejecutar)
			System.out.println("El proceso de Desc. Auto. Dist. ha terminado normalmente");
		} catch (NafinException Error) {
			System.out.println(" AceptacionPymeEJB::descuentoAutomaticoDist(NafinException)"+ Error);
			throw Error;
		} catch (Exception epError) {
			epError.printStackTrace();
			System.out.println(" AceptacionPymeEJB::descuentoAutomaticoDist(Exception)"+ epError);
			throw new NafinException("SIST0001");
			//throw epError;
		} finally {
			System.out.println(" AceptacionPymeEJB::descuentoAutomaticoDist(S)");
			if (lodbConexion.hayConexionAbierta() == true)
				lodbConexion.cierraConexionDB();
		}

	}//Fin del M�todo descuentoAutomaticoDist()

	/*********************************************************************************************
	*
	* 	    void validaDiaHabil()
	*		Verifica que no sea Dia Inhabil, de lo contrario arroja una NafinException
	*********************************************************************************************/

	private static void validaDiaHabil(AccesoDB eodbConexion)
			throws NafinException {
		System.out.println(" AceptacionPymeEJB::validaDiaHabil(E)");
		try {
			String sentenciaSQL =
				" SELECT COUNT (*) AS diainhabil"   +
				"   FROM comcat_dia_inhabil"   +
				"  WHERE cg_dia_inhabil = TO_CHAR (SYSDATE, 'dd/mm')"  ;
			ResultSet rs = eodbConexion.queryDB(sentenciaSQL);
			rs.next();
			int diaInhabil = rs.getInt("diainhabil");
			rs.close();
			eodbConexion.cierraStatement();
			if (diaInhabil!=0) {	//Es d�a inhabil?
				System.out.println("El dia es inhabil\nPrograma finalizado");
				throw new NafinException("DSCT0046");
			}
			Calendar fecha = Calendar.getInstance();
        	int dia = fecha.get(Calendar.DAY_OF_WEEK);
			if (dia == Calendar.SUNDAY || dia == Calendar.SATURDAY) { // Es sabado o domingo
				System.out.println("El dia es sabado o domingo, \nPrograma finalizado");
				throw new NafinException("DSCT0046");
			}
		} catch (NafinException ne)	{
			System.out.println(" AceptacionPymeEJB::validaDiaHabil(NafinException): "+ne);
			throw ne;
		} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::validaDiaHabil(Exception): "+e);
			throw new NafinException("SIST0001");
		} finally {
			System.out.println(" AceptacionPymeEJB::validaDiaHabil(S)");
		}
	}




	/**
	 * Obtiene una lista con las pymes bloqueadas para Distribuidores
	 * @return Lista con las clave de las pymes bloqueadas. Si no hay pymes
	 * 		bloqueadas regresa una Lista vacia.
	 */
	private static List pymesBloqueadas() throws SQLException, NamingException {

		AccesoDB con = new AccesoDB();
		List pymesBloqueadas = new ArrayList();
		try {
			con.conexionDB();

			String strSQL = "SELECT ic_pyme FROM comrel_bloqueo_pyme_x_producto " +
					" WHERE ic_producto_nafin = 4 AND cs_bloqueo='B' " +
					"union " +
					" select ic_pyme " +
					" from comrel_pyme_epo_x_producto " +
					" where cs_activo = 'S' " +
					" and ic_producto_nafin = 4 " +
					" ORDER BY ic_pyme ";

			ResultSet rs = con.queryDB(strSQL);
			while (rs.next()) {
				pymesBloqueadas.add(rs.getString("ic_pyme"));
			}
			rs.close();
			con.cierraStatement();
			return pymesBloqueadas;
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


	/**
	 * Realiza el proceso de seleccion de documentos
	 * automaticamente para Distribuidores
	 * @param cveProc Clave del proceso
	 * @param con Referencia a la conexi�n de BD.
	 */

	private void selecAutoPymeDist(String cveProc, AccesoDB con, String ruta)
			throws NafinException {
		System.out.println(" AceptacionPymeEJB::selecAutoPymeDist(E)");
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		PreparedStatement 	ps2				= null;
		ResultSet			rs2				= null;

		List pymesBloqueadas = new ArrayList();
		List lstCorreosEnviar = new ArrayList();

        Acuse	acuse				= null;
		String	rs_documento		= "",
				rs_epo 				= "",
				rs_pyme 			= "",
				rs_usuario 			= "",
				rs_monto			= "",
				rs_moneda_docto		= "",
				rs_por_desc			= "",
				rs_monto_descuento	= "",
				rs_tipo_conversion	= "",
				rs_tipo_cambio		= "",
				rs_if				= "",
				rs_tipo_piso		= "",
				rs_linea_credito_dm	= "",
				rs_moneda_linea		= "",
				rs_saldo_disponible	= "",
				rs_plazo 			= "",
				rs_fecha_venc_cred	= "",
				rs_moneda_auto		= "";

		String	rs_tasa				= "",
				rs_rel_mat			= "",
				rs_puntos			= "",
				rs_tasa_aplicar		= "",
				rs_tipo_tasa		= "",
				rs_puntos_pref		= "";

		String envioCorreoEpo = "";
		try {
			pymesBloqueadas = pymesBloqueadas();
			qrySentencia =
				" SELECT   /*+index(doc) use_nl(doc pep)*/"   +
				"           pep.ic_epo, pep.ic_pyme, 'dscto_aut' AS ic_usuario, pep.cs_moneda_auto "   +
				"     FROM dis_documento doc, comrel_pyme_epo_x_producto pep"   +
				"    WHERE doc.ic_epo = pep.ic_epo"   +
				"      AND doc.ic_pyme = pep.ic_pyme"   +
				"      AND doc.ic_producto_nafin = pep.ic_producto_nafin"   +
				"      AND doc.ic_linea_credito_dm IS NOT NULL"   +
				"      AND doc.ic_tipo_financiamiento IN (1, 4, 2)"   +
				"      AND doc.ic_estatus_docto = 2"   +
				"      AND pep.cs_dscto_auto = 'S'"   +
					//Inicia  Fodea 015-2014
				" AND  doc.ic_pyme  not in   (  SELECT bo.ic_pyme FROM COMREL_BLOQUEO_PYME_X_EPO  bo  "+
					" WHERE bo.CS_BLOQUEO = 'B'   " +
					" and bo.IC_PRODUCTO_NAFIN  = 4  "+
					"and  bo.ic_epo  = doc.ic_epo   ) "+
				//Termina  Fodea 015-2014
				" GROUP BY pep.ic_pyme, 'dscto_aut', pep.ic_epo, pep.cs_moneda_auto"  ;

			log.debug("qrySentencia ------------->  "+qrySentencia);

			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			int doctosinsert;
			while(rs.next()) {
				double 	total_monto		= 0;
				double 	total_int		= 0;
				double 	total_credito	= 0;
				double 	total_monto_dl	= 0;
				double 	total_int_dl	= 0;
				double 	total_credito_dl= 0;
				acuse = null;
				//rs_epo = ""; rs_pyme = "";
				rs_usuario = ""; //rs_moneda_auto = "";
				doctosinsert = 0;
				rs_epo				= rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				rs_pyme				= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				rs_usuario			= rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
				rs_moneda_auto		= rs.getString("cs_moneda_auto")==null?"":rs.getString("cs_moneda_auto");
				boolean vHorarioEpo = vHorarioEpo(rs_epo,con);

/*System.out.println("rs_epo:: "+rs_epo);
System.out.println("rs_pyme:: "+rs_pyme);
System.out.println("rs_moneda_auto:: "+rs_moneda_auto);*/
				Vector doctos = getDoctosDesctoAuto(rs_epo,rs_pyme,rs_moneda_auto,con);
System.out.println("getDoctosDesctoAuto:: "+doctos);
				for (int doc=0;doc<doctos.size();doc++) {
					rs_documento = ""; 	rs_monto = ""; 	rs_moneda_docto = "";
					rs_por_desc = ""; rs_monto_descuento = ""; rs_tipo_conversion = "";
					rs_tipo_cambio = ""; rs_if = ""; rs_tipo_piso = "";
					rs_linea_credito_dm = ""; rs_moneda_linea = ""; rs_saldo_disponible = "";
					rs_plazo = ""; rs_fecha_venc_cred = "";
					rs_tasa = ""; rs_rel_mat = ""; rs_puntos = "";
					rs_tasa_aplicar = ""; rs_tipo_tasa = ""; rs_puntos_pref = "";
					Vector datosDocumento = (Vector)doctos.get(doc);
					rs_documento		= (String)datosDocumento.get( 0);
					rs_monto			= (String)datosDocumento.get( 1);
					rs_moneda_docto		= (String)datosDocumento.get( 2);
					rs_por_desc			= (String)datosDocumento.get( 3);
					rs_monto_descuento	= (String)datosDocumento.get( 4);
					rs_tipo_conversion	= (String)datosDocumento.get( 5);
					rs_tipo_cambio 		= (String)datosDocumento.get( 6);
					rs_if				= (String)datosDocumento.get( 7);
					rs_tipo_piso		= (String)datosDocumento.get( 8);
					rs_linea_credito_dm	= (String)datosDocumento.get( 9);
					rs_moneda_linea		= (String)datosDocumento.get(10);
					rs_saldo_disponible	= (String)datosDocumento.get(11);
					rs_plazo 			= (String)datosDocumento.get(12);
					rs_fecha_venc_cred	= (String)datosDocumento.get(13);
					if(vHorarioEpo) {

						//GEAG F048-2005
						//Verifica si la pyme en turno no est� bloqueada
						if (pymesBloqueadas.contains(rs_pyme)) {
							//Como la pyme est� bloqueada no se hace el descuento
							setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 94, con);
							continue; //Continua con el docto
						}
           //GEAG FIN F048-2005

						if(!"".equals(rs_linea_credito_dm)){
							Vector tasaDocumento = validaPlazoDocto(rs_epo,rs_pyme,rs_if,rs_moneda_linea,rs_plazo,con);
							if(tasaDocumento.size()==1) {
								Vector datosTasa = (Vector)tasaDocumento.get(0);
								rs_tasa			= (String)datosTasa.get(0);
								rs_rel_mat		= (String)datosTasa.get(1);
								rs_puntos		= (String)datosTasa.get(2);
								rs_tasa_aplicar	= (String)datosTasa.get(3);
								rs_tipo_tasa	= (String)datosTasa.get(4);
								rs_puntos_pref	= (String)datosTasa.get(5);
								double mtoDocFinal = 0;
								double montoTasaInt = 0;
								double montoInt = 0;

								//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
								if("54".equals(rs_moneda_docto)&&"1".equals(rs_moneda_linea)&&!"".equals(rs_tipo_conversion)) {
									double montoValuado	= "".equals(rs_tipo_cambio)?0:(Double.parseDouble(rs_tipo_cambio)*(Double.parseDouble(rs_monto) - Double.parseDouble(rs_monto_descuento)));
									mtoDocFinal	= montoValuado;
								} else
									mtoDocFinal = Double.parseDouble(rs_monto) - Double.parseDouble(rs_monto_descuento);
								if("1".equals(rs_tipo_piso)){
									montoTasaInt = (double)((mtoDocFinal*(Double.parseDouble(rs_tasa_aplicar)/100)/360)*100)/100;
								}else{
									montoTasaInt = (mtoDocFinal*(Double.parseDouble(rs_tasa_aplicar)/100)/360);
								}
								montoInt = Double.parseDouble(Comunes.formatoDecimal(montoTasaInt*Double.parseDouble(rs_plazo),2,false));

System.out.println("mtoDocFinal "+mtoDocFinal);
System.out.println("rs_saldo_disponible "+rs_saldo_disponible);

								if(mtoDocFinal<Double.parseDouble(rs_saldo_disponible)) {

									try {

										if(doctosinsert==0&&acuse==null) {
							                acuse = new Acuse(7,"4","com",con);
											creaAcuseDstoAuto(acuse.toString(), rs_usuario, con);
										}

										confirmaDoctoDsctoAtuto(
											rs_documento, acuse.toString(), mtoDocFinal, montoInt,
											rs_plazo, rs_fecha_venc_cred,rs_tipo_cambio,
											rs_tasa, rs_rel_mat, rs_puntos, rs_tasa_aplicar,
											rs_tipo_tasa, rs_puntos_pref,con);

										doctosinsert++;

										if("1".equals(rs_moneda_linea)) {
											total_monto		+= Double.parseDouble(rs_monto);
											total_int		+= montoInt;
											total_credito	+= mtoDocFinal;
										} else if("54".equals(rs_moneda_linea)) {
											total_monto_dl	+= Double.parseDouble(rs_monto);
											total_int_dl	+= montoInt;
											total_credito_dl+= mtoDocFinal;
										}

									} catch (NafinException nexc) {
										if("DIST0044".equals(nexc.getCodError())) {
											/**Limite Pyme insuficiente**************************/
											setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 99, con);
										}
									}
								} else {
									/**Saldo de l�nea insuficiente**************************/
									setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 91, con);
								}//else if(mtoDocFinal>Double.parseDouble(rs_saldo_disponible))
							} else {
								/**No existe tasa para ese  plazo**************************/
								setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 86, con);
							} //else if(tasaDocumento.size()>0)
						} else {
							/**La L�nea no esta vigente**************************/
							setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 90, con);
						} //else if(!"".equals(rs_linea_credito_dm)
					} else {
						/**Epo fuera de horario de servicio**************************/
						setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 83, con);
					} // if(vHorarioEpo) else
				}//for (int doc=0;doc<doctos.size();doc++
				if(doctosinsert>0){
					List lstCorreo = new ArrayList();
					String correo_c = "";
					envioCorreoEpo = "";
					setAcuseDstoAuto(acuse.toString(),total_monto, total_int, total_credito, total_monto_dl, total_int_dl, total_credito_dl, con);
					String qrymail = "select cg_email from com_contacto  " +
										"where ic_pyme = ? " +
										"and cs_primer_contacto = 'S' ";

					ps2 = con.queryPrecompilado(qrymail);
					ps2.setString(1,rs_pyme);
					rs2 = ps.executeQuery();
					if(rs2!=null && rs2.next()){
						correo_c = rs2.getString(1);
					}
					if(ps2!=null)ps2.close();
					if(rs2!=null)rs2.close();

					String qryEpoCorreo = "select cs_envio_correo_elec from comrel_producto_epo " +
											"where ic_epo = ? " +
											"and ic_producto_nafin =4 " +
											"and cs_habilitado = 'S' ";
					System.out.println("rs_epo>>>>>>>>>>>>>>>>"+rs_epo);
					ps2 = con.queryPrecompilado(qryEpoCorreo);
					ps2.setLong(1,Long.parseLong(rs_epo));
					rs2 = ps2.executeQuery();
					if(rs2!=null && rs2.next()){
						envioCorreoEpo  = rs2.getString("cs_envio_correo_elec");
					}
					if(ps2!=null)ps2.close();
					if(rs2!=null)rs2.close();

					if(envioCorreoEpo==null || "".equals(envioCorreoEpo)){
					 qryEpoCorreo = "SELECT cs_envio_correo_elec " +
						"  FROM comcat_producto_nafin " +
						" WHERE ic_producto_nafin = 4 ";
						ps2 = con.queryPrecompilado(qryEpoCorreo);
						rs2 = ps2.executeQuery();
						if(rs2!=null && rs2.next()){
							envioCorreoEpo  = rs2.getString("cs_envio_correo_elec");
						}
						if(ps2!=null)ps2.close();
						if(rs2!=null)rs2.close();
					}



					lstCorreo.add(acuse.toString());
					lstCorreo.add(correo_c);
					lstCorreo.add(envioCorreoEpo);
					lstCorreosEnviar.add(lstCorreo);
					//con.terminaTransaccion(true);
				}con.terminaTransaccion(true);
			}//while

      System.out.println("*********************");
      System.out.println("rs_epo:: "+rs_epo);
      System.out.println("rs_pyme:: "+rs_pyme);
      System.out.println("rs_moneda_auto:: "+rs_moneda_auto);

      if (!rs_epo.equals("") && !rs_pyme.equals("") && !rs_moneda_auto.equals("") ) {
        Vector nofueromProcesadas = new Vector();
        nofueromProcesadas =  nofueromProcesadas( rs_epo, rs_pyme, rs_moneda_auto,con);
        System.out.println("nofueromProcesadas::" +nofueromProcesadas);
        }
  		/*nuevo codigo para mandar correo con pdf*/
			if(lstCorreosEnviar!=null && lstCorreosEnviar.size()>0 && "S".equals(envioCorreoEpo)){
				con.terminaTransaccion(true);
				String strDirectorioTemp = ruta;
				String strDirectorioFisico = strDirectorioTemp+"00tmp/24finandist/";
				for(int z=0; z<lstCorreosEnviar.size(); z++){
					List lstCorreosGen = (List)lstCorreosEnviar.get(z);
					if("S".equals((String)lstCorreosGen.get(2))){
						System.out.println("antes de generar pdf...................................");
            System.out.println("Datos Correo 2--- "+lstCorreosGen);
            System.out.println("(String)lstCorreosGen.get(0)"+(String)lstCorreosGen.get(0));
            System.out.println("(String)lstCorreosGen.get(1)"+(String)lstCorreosGen.get(1));
            System.out.println("(String)lstCorreosGen.get(2)"+(String)lstCorreosGen.get(2));
            System.out.println("strDirectorioTemp"+strDirectorioTemp);
            System.out.println("strDirectorioFisico"+strDirectorioFisico);

						//String nombreArchivo = generaPDFDocDist((String)lstCorreosGen.get(0),"",strDirectorioTemp,strDirectorioFisico,"DIST_DESC_AUT_"+acuse.toString());
            String nombreArchivo = generaPDFDocDist((String)lstCorreosGen.get(0),"",strDirectorioTemp,strDirectorioFisico,"DIST_DESC_AUT_"+(String)lstCorreosGen.get(0));
            System.out.println("despues de generar pdf...................."+nombreArchivo);
						String mensajeCorreo 	= 	"<html>															"  +
											"       <head>													"  +
											"               <title>DESCUENTO ELECTRONICO NAFIN</title>	"  +
											"       </head>												"  +
											"       <body>													"	+
											"			<table><tr><td>DATOS ADJUNTOS PROCESO DESCUENTO AUTOMATICO DISTRIBUIDORES</td></tr></table>		" +
											"	</body>														" 	+
											"</html>															";
						ArrayList listaDeImagenes = null;
						ArrayList listaDeArchivos = new ArrayList();

						nombreArchivo = strDirectorioFisico+nombreArchivo;
						HashMap 			archivoAdjunto 		 = new HashMap();
						archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );//applicationPath + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif");

						listaDeArchivos.add(archivoAdjunto);
						Correo correo = new Correo();
						System.out.println("antes de enviar correo....................");
						correo.enviaCorreoConDatosAdjuntos("cadenas@nafin.gob.mx",(String)lstCorreosGen.get(1),"","DESCUENTO ELECTRONICO NAFIN",mensajeCorreo,listaDeImagenes,listaDeArchivos);
						System.out.println("despues de enviar correo....................");
					}
				}
			}
			/*------------*/

			rs.close();
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::selecAutoPymeDist(Exception): "+e);
			con.terminaTransaccion(false);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::selecAutoPymeDist(S)");
    		con.terminaTransaccion(true);
    	}
	} // fin del m�todo procesoSelecPyme.

	private static boolean vHorarioEpo(String ic_epo, AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::vHorarioEpo(E)");
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		boolean 			existe			= false;
		try {
			qrySentencia =
				" SELECT hor1.existe, hor2.existehor"   +
				"   FROM (SELECT /*+index(che1 CP_COMREL_HORARIO_X_EPO_PK)*/ COUNT (1) AS existe"   +
				"           FROM comrel_horario_x_epo che1"   +
				"          WHERE ic_epo = ?"   +
				"            AND ic_producto_nafin = 4) hor1,"   +
				"        (SELECT /*+index(che2 CP_COMREL_HORARIO_X_EPO_PK)*/ COUNT (1) AS existehor"   +
				"           FROM comrel_horario_x_epo che2"   +
				"          WHERE ic_epo = ?"   +
				"            AND ic_producto_nafin = 4"   +
				"            AND TO_DATE (cg_horario_inicio, 'hh24:mi') <= TO_DATE (TO_CHAR (SYSDATE, 'hh24:mi'), 'hh24:mi')"   +
				"            AND TO_DATE (cg_horario_fin, 'hh24:mi') >= TO_DATE (TO_CHAR (SYSDATE, 'hh24:mi'), 'hh24:mi')) hor2"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_epo));
			ps.setInt(2,Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			int contexiste = 0;
			int contexistehor = 0;

			if(rs.next()) {
				contexiste = rs.getInt(1);
				contexistehor = rs.getInt(2);
			}
			rs.close();
			if(ps!=null) ps.close();
			if(contexiste>0) {
				if(contexistehor>0)
					existe = true;
			}
			else
				existe = true;
  	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::vHorarioEpo(Exception): "+e);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::vHorarioEpo(S)");
    	}
    	return existe;
	} // fin del m�todo vHorarioEpo.

	private static void creaAcuseDstoAuto(String cc_acuse, String ic_usuario, AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::creaAcuseDstoAuto(E)");
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		try {
			qrySentencia =
				" INSERT INTO com_acuse2 (cc_acuse, "   +
				"                         ic_usuario, df_fecha_hora,  ic_producto_nafin)"   +
				"      VALUES(?, "   +
				"             ?, SYSDATE, 4)"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, cc_acuse);
			ps.setString(2, ic_usuario);
			ps.executeUpdate();
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::creaAcuseDstoAuto(Exception): "+e);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::creaAcuseDstoAuto(S)");
    	}
	} // fin del m�todo creaAcuseDstoAuto.

	private static void setAcuseDstoAuto(String cc_acuse,
								double total_monto_mn, double total_int_mn, double total_imp_mn,
								double total_monto_dl, double total_int_dl, double total_imp_dl,
								AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::setAcuseDstoAuto(E)");
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		try {
			qrySentencia =
				" UPDATE com_acuse2"   +
				"    SET fn_total_monto_mn = ?,"   +
				"        fn_total_int_mn = ?,"   +
				"        fn_total_imp_mn = ?,"   +
				"        fn_total_monto_dl = ?,"   +
				"        fn_total_int_dl = ?,"   +
				"        fn_total_imp_dl = ?"   +
				"  WHERE cc_acuse = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setDouble( 1, total_monto_mn);
			ps.setDouble( 2, total_int_mn);
			ps.setDouble( 3, total_imp_mn);
			ps.setDouble( 4, total_monto_dl);
			ps.setDouble( 5, total_int_dl);
			ps.setDouble( 6, total_imp_dl);
			ps.setString( 7, cc_acuse);
			ps.executeUpdate();
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::setAcuseDstoAuto(Exception): "+e);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::setAcuseDstoAuto(S)");
    	}
	} // fin del m�todo creaAcuseDstoAuto.


	private static void setRechazosDsctoAuto(String cveProc, String ic_documento, String ic_if, int causa_rechazo, AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::setRechazosDsctoAuto(E)");
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		try {
			qrySentencia =
				" INSERT INTO com_dsctoerror ("   +
				"                            ic_dsctoerror,"   +
				"                            ic_documento,"   +
				"                            ic_if,"   +
				"                            df_proceso,"   +
				"                            ic_causa_rechazo,"   +
				"                            ic_producto_nafin)"   +
				"      VALUES("   +
				"         ?,"   +
				"         ?,"   +
				"         ?,"   +
				"         SYSDATE,"   +
				"         ?,"   +
				"         4)"  ;
			System.out.println(" qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(cveProc));
			ps.setInt( 2, Integer.parseInt(ic_documento));
			if(!"".equals(ic_if))
				ps.setInt( 3, Integer.parseInt(ic_if));
			else
				ps.setNull( 3, Types.INTEGER);
			ps.setInt( 4, causa_rechazo);
			ps.executeUpdate();
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::setRechazosDsctoAuto(Exception): "+e);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::setRechazosDsctoAuto(S)");
    	}
	} // fin del m�todo creaAcuseDstoAuto.

	private static Vector getDoctosDesctoAuto(String ic_epo,String ic_pyme,String moneda_auto, AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::getDoctosDesctoAuto(E)");
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector	 			datosDocto		= new Vector();
		Vector				columnas		= null;
		try {
			if(!"".equals(moneda_auto)) {
				if("N".equals(moneda_auto))
					moneda_auto = "1";
				else if("D".equals(moneda_auto))
					moneda_auto = "54";
				else if("A".equals(moneda_auto))
					moneda_auto = "1,54";
			}//if(!"".equals(rs_moneda_auto))

			qrySentencia =
				" SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(cif CP_COMCAT_IF_PK) use_nl(doc cif lcd lc)*/"   +
				"        doc.ic_documento, doc.fn_monto, doc.ic_moneda AS moneda_docto, doc.fn_porc_descuento,"   +
				"        fn_monto * (NVL (doc.fn_porc_descuento, 0) / 100) AS monto_descuento,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), 'N', '', 'P', 'Dolar-Peso', '') AS tipo_conversion,"   +
				"        DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),"   +
				"           'P', DECODE (doc.ic_moneda, 54, tc.fn_valor_compra, '1'), '1' ) AS tipo_cambio,"   +
				"        lc.ic_if, cif.ig_tipo_piso, lc.ic_linea_credito_dm, lc.ic_moneda AS moneda_linea,"   +
				"         lc.saldo_disponible, " +

				" decode(doc.ic_tipo_financiamiento , 2, decode(doc.ic_moneda, 1,     (SIGFECHAHABIL(TRUNC (SYSDATE)+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-')-TRUNC (SYSDATE)),  "+
				"                                                              54,    (SIGFECHAHABIL(TRUNC (SYSDATE+1)+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-')-TRUNC (SYSDATE+1))   )  "+
				"                                     ,doc.ig_plazo_docto ) AS ig_plazo_pyme,   "+
            " decode(doc.ic_tipo_financiamiento , 2, decode(doc.ic_moneda, 1, TO_CHAR (SIGFECHAHABIL(TRUNC (SYSDATE)+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-'), 'DD/MM/YYYY'),  "+
				"																				  54, TO_CHAR (SIGFECHAHABIL(TRUNC (SYSDATE+1)+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-'), 'DD/MM/YYYY')  )  "+
				"                                      ,TO_CHAR (doc.df_fecha_venc, 'DD/MM/YYYY') ) AS fecha_venc_cred    "+
				"   FROM dis_documento doc,"   +
				"        comcat_if cif,"   +
				"        comrel_producto_epo pe,"   +
				"        comcat_producto_nafin pn,"   +
				"        com_tipo_cambio tc,"   +
				"        dis_linea_credito_dm lcd,"   +
				"        COMREL_PYME_EPO_X_PRODUCTO pep ,  "+
            "        com_parametrizacion_epo pa, "+
				"        (SELECT ic_linea_credito_dm, ic_if, ic_moneda, ic_tipo_cobro_interes,"   +
				"                (fn_monto_autorizado_total - fn_saldo_financiado - fn_monto_x_autorizar) AS saldo_disponible"   +
				"           FROM dis_linea_credito_dm"   +
				"          WHERE TRUNC (df_vencimiento_adicional) > TRUNC (SYSDATE)"   +
				"            AND (fn_monto_autorizado_total - fn_saldo_financiado - fn_monto_x_autorizar) > 0"   +
				"            AND cg_tipo_solicitud = 'I'"   +
				"            AND ic_estatus_linea = 12) lc"   +
				"  WHERE doc.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)"   +
				"    AND doc.ic_linea_credito_dm = lcd.ic_linea_credito_dm"   +
				"    AND lc.ic_if = cif.ic_if (+)"   +
				"    AND doc.ic_epo = pe.ic_epo"   +
				"    AND doc.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND pe.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND doc.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha)"   +
				"                          FROM com_tipo_cambio"   +
				"                         WHERE ic_moneda = doc.ic_moneda)"   +
				"    AND lcd.ic_moneda IN ("+moneda_auto+")"   +
				"    AND lcd.ic_tipo_cobro_interes = 2"   +
				"    AND doc.ic_tipo_financiamiento IN (1,4, 2)"   +
				"    AND doc.ic_estatus_docto = 2"   +
				"    AND doc.ic_epo = ?"   +
				"    AND doc.ic_pyme = ?"+
        "    AND doc.ic_epo = pa.ic_epo "+
        "    AND pa.CC_PARAMETRO_EPO = 'PUB_EPO_DESC_AUTOMATICO' "+
        "    AND pa.CG_VALOR ='S'"+
		  "   AND doc.ic_epo = pep.ic_epo  "+
		  "   AND doc.ic_pyme = pep.ic_pyme   " +
		  "   AND pep.ic_producto_nafin = 4   ";


System.out.println("qrySentencia:------ "+qrySentencia);
//System.out.println("ic_epo: "+ic_epo);
//System.out.println("ic_pyme: "+ic_pyme);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(ic_epo));
			ps.setInt( 2,Integer.parseInt(ic_pyme));

			rs = ps.executeQuery();
			while(rs.next()) {
				columnas = new Vector();
				columnas.addElement((rs.getString("ic_documento")==null)?"":rs.getString("ic_documento"));
				columnas.addElement((rs.getString("fn_monto")==null)?"":rs.getString("fn_monto"));
				columnas.addElement((rs.getString("moneda_docto")==null)?"":rs.getString("moneda_docto"));
				columnas.addElement((rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento"));
				columnas.addElement((rs.getString("monto_descuento")==null)?"":rs.getString("monto_descuento"));
				columnas.addElement((rs.getString("tipo_conversion")==null)?"":rs.getString("tipo_conversion"));
				columnas.addElement((rs.getString("tipo_cambio")==null)?"":rs.getString("tipo_cambio"));
				columnas.addElement((rs.getString("ic_if")==null)?"":rs.getString("ic_if"));
				columnas.addElement((rs.getString("ig_tipo_piso")==null)?"":rs.getString("ig_tipo_piso"));
				columnas.addElement((rs.getString("ic_linea_credito_dm")==null)?"":rs.getString("ic_linea_credito_dm"));
				columnas.addElement((rs.getString("moneda_linea")==null)?"":rs.getString("moneda_linea"));
				columnas.addElement((rs.getString("saldo_disponible")==null)?"":rs.getString("saldo_disponible"));
				columnas.addElement((rs.getString("ig_plazo_pyme")==null)?"":rs.getString("ig_plazo_pyme"));
				columnas.addElement((rs.getString("fecha_venc_cred")==null)?"":rs.getString("fecha_venc_cred"));
		      datosDocto.addElement(columnas);
			}//while
			rs.close();
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::getDoctosDesctoAuto(Exception): "+e );
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::getDoctosDesctoAuto(S)");
    	}
    	return datosDocto;
	} // fin del m�todo getDoctosDesctoAuto.

	private static Vector validaPlazoDocto(String ic_epo,String ic_pyme,String ic_if,String ic_moneda,String plazo, AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::validaPlazoDocto(E)");
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		ResultSet			rs				= null;
		Vector	 			datosTasa		= new Vector();
		Vector				columnas		= null;
		try {
			qrySentencia =
				" SELECT   /*+index(mt CP_COM_MANT_TASA_PK) index(tie CP_COM_TASA_IF_EPO_PK) use_nl(mt ct tie tp cp pfr neg)*/"   +
				"        mt.ic_tasa, tie.cg_rel_mat, tie.fn_puntos,"   +
				"        NVL (neg.fn_valor,"   +
				"           DECODE ("   +
				"              tie.cg_rel_mat,"   +
				"              '+', (mt.fn_valor + tie.fn_puntos) - NVL (pfr.fn_puntos, 0),"   +
				"              '-', (mt.fn_valor - tie.fn_puntos) - NVL (pfr.fn_puntos, 0),"   +
				"              '*', (mt.fn_valor * tie.fn_puntos) - NVL (pfr.fn_puntos, 0),"   +
				"              '/', (mt.fn_valor / tie.fn_puntos) - NVL (pfr.fn_puntos, 0),"   +
				"              0 ) ) AS tasaaplicar,"   +
				"        CASE WHEN SIGN(NVL(neg.fn_valor, - 1)) > 0 THEN 'N'"   +
				"         ELSE CASE WHEN SIGN(NVL (pfr.fn_puntos, - 1)) > 0 THEN 'P'"   +
				"         ELSE 'B'"   +
				"         END"   +
				"        END AS tipo_tasa, pfr.fn_puntos as puntos_pref"   +
				"   FROM com_mant_tasa mt, comcat_tasa ct, com_tasa_if_epo tie, comrel_tasa_producto tp, comcat_plazo cp,"   +
				"        (SELECT   /*+index(tpn CP_DIS_TASA_PREFER_NEGO_PK)*/"   +
				"                ic_epo, fn_puntos"   +
				"           FROM dis_tasa_prefer_nego tpn"   +
				"          WHERE ic_pyme = ?"   +
				"            AND ic_producto_nafin = 4"   +
				"            AND ic_epo = ?"   +
				"            AND ic_if = ?"   +
				"            AND ic_moneda = ?"   +
				"            AND cg_tipo = 'P') pfr,"   +
				"        (SELECT   /*+index(tpn CP_DIS_TASA_PREFER_NEGO_PK)*/"   +
				"                ic_tasa, fn_valor"   +
				"           FROM dis_tasa_prefer_nego tpn"   +
				"          WHERE ic_pyme = ?"   +
				"            AND ic_producto_nafin = 4"   +
				"            AND ic_epo = ?"   +
				"            AND ic_if = ?"   +
				"            AND ic_moneda = ?"   +
				"            AND cg_tipo = 'N'"   +
				"            AND TRUNC (df_fecha) = TRUNC (SYSDATE)) neg"   +
				"  WHERE mt.ic_tasa = ct.ic_tasa"   +
				"    AND ct.ic_tasa = tie.ic_tasa"   +
				"    AND tie.ic_tasa = tp.ic_tasa"   +
				"    AND tie.ic_producto_nafin = tp.ic_producto_nafin"   +
				"    AND ct.ic_tasa = neg.ic_tasa (+)"   +
				"    AND tie.ic_epo = pfr.ic_epo (+)"   +
				"    AND mt.dc_fecha = (SELECT   /*+ordered use_nl(mi ti ctp)*/"   +
				"                              MAX (mi.dc_fecha)"   +
				"                         FROM com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp"   +
				"                        WHERE mi.ic_tasa = mt.ic_tasa"   +
				"                          AND ti.ic_tasa = mi.ic_tasa"   +
				"                          AND ti.ic_tasa = ctp.ic_tasa"   +
				"                          AND ctp.ic_producto_nafin = tp.ic_producto_nafin)"   +
				"    AND tp.ic_producto_nafin = cp.ic_producto_nafin"   +
				"    AND ct.ic_plazo = cp.ic_plazo"   +
				"    AND cp.in_plazo_dias >= ?"   +
				"    AND cp.in_plazo_dias IN (SELECT   /*+ index(pl CP_COMCAT_PLAZO_PK)*/"   +
				"                                    MIN (in_plazo_dias)"   +
				"                               FROM comcat_plazo pl"   +
				"                              WHERE pl.in_plazo_dias >= ?"   +
				"                                AND pl.ic_producto_nafin = 4)"   +
				"    AND tp.ic_producto_nafin = 4"   +
				"    AND tie.ic_epo = ?"   +
				"    AND tie.ic_if = ?"   +
				"    AND ct.ic_moneda = ?"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(ic_pyme));
			ps.setInt( 2,Integer.parseInt(ic_epo));
			ps.setInt( 3,Integer.parseInt(ic_if));
			ps.setInt( 4,Integer.parseInt(ic_moneda));
			ps.setInt( 5,Integer.parseInt(ic_pyme));
			ps.setInt( 6,Integer.parseInt(ic_epo));
			ps.setInt( 7,Integer.parseInt(ic_if));
			ps.setInt( 8,Integer.parseInt(ic_moneda));
			ps.setInt( 9,Integer.parseInt(plazo));
			ps.setInt(10,Integer.parseInt(plazo));
			ps.setInt(11,Integer.parseInt(ic_epo));
			ps.setInt(12,Integer.parseInt(ic_if));
			ps.setInt(13,Integer.parseInt(ic_moneda));
			rs = ps.executeQuery();
			while(rs.next()) {
				columnas = new Vector();
				columnas.addElement((rs.getString("ic_tasa")==null)?"":rs.getString("ic_tasa"));
				columnas.addElement((rs.getString("cg_rel_mat")==null)?"":rs.getString("cg_rel_mat"));
				columnas.addElement((rs.getString("fn_puntos")==null)?"":rs.getString("fn_puntos"));
				columnas.addElement((rs.getString("tasaaplicar")==null)?"":rs.getString("tasaaplicar"));
				columnas.addElement((rs.getString("tipo_tasa")==null)?"":rs.getString("tipo_tasa"));
				columnas.addElement((rs.getString("puntos_pref")==null)?"":rs.getString("puntos_pref"));
		      	datosTasa.addElement(columnas);
			}//while
			rs.close();
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::validaPlazoDocto(Exception): "+e);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::validaPlazoDocto(S)");
    	}
    	return datosTasa;
	} // fin del m�todo validaPlazoDocto.


	private static void confirmaDoctoDsctoAtuto(
										String ic_documento, String cc_acuse, double mtoDocFinal, double montoTasaInt,
										String ig_plazo, String df_fecha_venc_cred,String tipo_cambio,
										String ic_tasa, String cg_rel_mat, String fn_puntos, String tasa_aplicar, String tipo_tasa, String fn_puntos_pref,AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::confirmaDoctoDsctoAtuto(E)");
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		try {

		qrySentencia= " delete  from  dis_docto_seleccionado where ic_documento = "+ic_documento;
		ps = con.queryPrecompilado(qrySentencia);
		ps.executeUpdate();
		if(ps!=null) ps.close();

			qrySentencia =
				" INSERT INTO dis_docto_seleccionado ("   +
				"                            ic_documento, cc_acuse,"   +
				"                            fn_importe_recibir,"   +
				"                            fn_importe_interes,"   +
				"                            ic_tasa, cg_rel_mat,"   +
				"                            fn_puntos, fn_valor_tasa,"   +
				"                            cg_tipo_tasa, fn_puntos_pref,"   +
				"                            df_fecha_seleccion)"   +
				"      VALUES(?, ?, ?, ?, ?, ?, ?, ?, ? , ?, SYSDATE)"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_documento));
			ps.setString( 2, cc_acuse);
			ps.setDouble( 3, mtoDocFinal);
			ps.setDouble( 4, montoTasaInt);
			ps.setInt( 5, Integer.parseInt(ic_tasa));
			ps.setString(6, cg_rel_mat);
			ps.setDouble( 7, Double.parseDouble(fn_puntos));
			ps.setDouble( 8, Double.parseDouble(tasa_aplicar));
			ps.setString( 9, tipo_tasa);
			if(!"".equals(fn_puntos_pref))
				ps.setDouble(10, Double.parseDouble(fn_puntos_pref));
			else
					ps.setNull(10, Types.FLOAT);
			ps.executeUpdate();
			if(ps!=null) ps.close();

			try {

				qrySentencia =
					" UPDATE dis_documento"   +
					"    SET ic_estatus_docto = 3,"   +
					"        ig_plazo_credito = ?,"   +
					"        df_fecha_venc_credito = TO_DATE (?, 'dd/mm/yyyy'),"   +
					"        fn_tipo_cambio = ?"   +
					"  WHERE ic_documento = ?"  ;
System.out.println("\n qrySentencia: "+qrySentencia);
System.out.println("\n ig_plazo: "+ig_plazo);
System.out.println("\n df_fecha_venc_cred: "+df_fecha_venc_cred);
System.out.println("\n tipo_cambio: "+tipo_cambio);
System.out.println("\n ic_documento: "+ic_documento);

				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt( 1, Integer.parseInt(ig_plazo));
				ps.setString( 2, df_fecha_venc_cred);
				ps.setDouble( 3, Double.parseDouble(tipo_cambio));
				ps.setInt( 4, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				if(ps!=null) ps.close();

			} catch (SQLException sqle) {
				System.out.println("sqle.getErrorCode():"+sqle.getErrorCode());
				sqle.printStackTrace();
				if (sqle.getErrorCode()==20001) {
					throw new NafinException("DIST0042");
				}
			}

    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::confirmaDoctoDsctoAtuto(Exception): "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::confirmaDoctoDsctoAtuto(S)");
    	}
	} // fin del m�todo creaAcuseDstoAuto.


	public int setLimiteAcumPyme(String ic_epo, String ic_pyme, String fg_limite_pyme_acum)
			throws NafinException {
		System.out.println("AceptacionPymeEJB::setLimiteAcumPyme(E)");
		AccesoDB 			con 			= new AccesoDB();
		String 				qrySentencia 	= "";
		List 				lVarBind 		= new ArrayList();
		int					registros		= 0;
		boolean				bOK				= true;
		try {
			con.conexionDB();
			qrySentencia =
				" UPDATE comrel_pyme_epo_x_producto"   +
				"    SET fg_limite_pyme_acum = ?"   +
				"  WHERE ic_producto_nafin = ?"   +
				"    AND ic_epo = ?"   +
				"    AND ic_pyme = ?"  ;
			lVarBind = new ArrayList();
			if(!"".equals(fg_limite_pyme_acum))
				lVarBind.add(new Double(fg_limite_pyme_acum));
			else
				lVarBind.add(Double.TYPE);
			lVarBind.add(new Integer(4));
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer(ic_pyme));
			registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);
		} catch(Exception e){
			bOK = false;
			System.out.println("AceptacionPymeEJB::setLimiteAcumPyme(Exception)"+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AceptacionPymeEJB::setLimiteAcumPyme(S)");
		}
		return registros;
	}//setLimiteAcumPyme

  /**
	 * Realiza el proceso de seleccion de documentos
	 * automaticamente para Distribuidores
	 * @param cveProc Clave del proceso
	 * @param con Referencia a la conexi�n de BD.
	 */

	private void selecAutoPymeDistCCC(String cveProc, AccesoDB con, String ruta)
			throws NafinException {
		System.out.println(" AceptacionPymeEJB::selecAutoPymeDistCCC(E)");
		String	qrySentencia	= "";
		PreparedStatement ps	= null;
		ResultSet rs					= null;
		PreparedStatement ps2	= null;
		ResultSet rs2				= null;

		List pymesBloqueadas 		= new ArrayList();

		List lstCorreosEnviar = new ArrayList();

    Acuse	acuse							= null;
		String	rs_documento		= "",
		rs_epo 									= "",
		rs_pyme 								= "",
		rs_usuario 							= "",
		rs_monto								= "",
		rs_moneda_docto					= "",
		rs_por_desc							= "",
		rs_monto_descuento			= "",
		rs_tipo_conversion			= "",
		rs_tipo_cambio					= "",
		rs_if										= "",
		rs_tipo_piso						= "",
		rs_linea_credito				= "",
		rs_moneda_linea					= "",
		rs_saldo_disponible			= "",
		rs_plazo 								= "",
		rs_fecha_venc_cred			= "",
		rs_moneda_auto					= "";

		String	rs_tasa					= "",
		rs_rel_mat							= "",
		rs_puntos								= "",
		rs_tasa_aplicar					= "",
		rs_tipo_tasa						= "",
		rs_puntos_pref					= "",
		rs_moneda								= "",
		rs_plazo_dias						= "";

    String rs_epoA		= "",
    rs_pymeA					= "";
    int rs_plazoHabil =0;
		String envioCorreoEpo = "N";
		String fechahabil = "",   operalineaCreditoIF = "",  fechaVencimientoLinea = ""; //F020-2015
		try {
			CapturaTasas tasasBean = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);

			pymesBloqueadas = pymesBloqueadas();
			qrySentencia =
				" SELECT   /*+index(doc) use_nl(doc pep)*/"   +
				"           pep.ic_epo, pep.ic_pyme, 'dscto_aut' AS ic_usuario, pep.cs_moneda_auto "   +
				"     FROM dis_documento doc, comrel_pyme_epo_x_producto pep, com_linea_credito clc"   +
				"    WHERE doc.ic_epo = pep.ic_epo"   +
				"      AND doc.ic_pyme = pep.ic_pyme"   +
				"	AND doc.ic_pyme = clc.ic_pyme"	+
				"	AND doc.ic_epo = clc.ic_epo"	+
				"      AND doc.ic_producto_nafin = pep.ic_producto_nafin"   +
				"      AND clc.ic_linea_credito IS NOT NULL"	+
				"      AND doc.ic_tipo_financiamiento IN (2)"   +
				"      AND doc.ic_estatus_docto = 2"   +
					//Inicia  Fodea 015-2014
				" AND  doc.ic_pyme  not in   (  SELECT bo.ic_pyme FROM COMREL_BLOQUEO_PYME_X_EPO  bo  "+
					" WHERE bo.CS_BLOQUEO = 'B'   " +
					" and bo.IC_PRODUCTO_NAFIN  = 4  "+
					"and  bo.ic_epo  = doc.ic_epo   ) "+
				//Termina  Fodea 015-2014
				" GROUP BY pep.ic_pyme, 'dscto_aut', pep.ic_epo, pep.cs_moneda_auto"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			int doctosinsert;
			while(rs.next()) {
				double 	total_monto				= 0;
				double 	total_int					= 0;
				double 	total_credito			= 0;
				double 	total_monto_dl		= 0;
				double 	total_int_dl			= 0;
				double 	total_credito_dl	= 0;
				String inhabilEpoPyme = "";
				acuse 						= null;
				rs_epo 						= "";
				rs_pyme 					= "";
				rs_usuario				= "";
				rs_moneda_auto		= "";
				doctosinsert 			= 0;
				rs_epo						= rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				rs_pyme						= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				rs_usuario				= rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
				rs_moneda_auto		= rs.getString("cs_moneda_auto")==null?"N":rs.getString("cs_moneda_auto");
				boolean vHorarioEpo = vHorarioEpo(rs_epo,con);

				System.out.println("rs_epo:: "+rs_epo);
				System.out.println("rs_pyme:: "+rs_pyme);
				System.out.println("rs_moneda_auto:: "+rs_moneda_auto);
				Vector doctos = getDoctosDesctoAutoCCC(rs_epo,rs_pyme,rs_moneda_auto,con);
				System.out.println("getDoctosDesctoAutoCCC:: "+doctos);
				if(doctos.size()>0){
					for (int doc=0;doc<doctos.size();doc++) {
						rs_documento 				= "";	rs_monto						= "";
						rs_moneda_docto			= "";	rs_por_desc					= "";
						rs_monto_descuento	= ""; rs_tipo_conversion	= "";
						rs_tipo_cambio 			= ""; rs_if								= "";
						rs_tipo_piso 				= "";	rs_linea_credito		= "";
						rs_moneda_linea 		= ""; rs_saldo_disponible = "";
						rs_plazo 						= ""; rs_fecha_venc_cred 	= "";
						rs_tasa 						= ""; rs_rel_mat					= "";
						rs_puntos 					= ""; rs_tasa_aplicar			= "";
						rs_tipo_tasa				= ""; rs_puntos_pref 			= "";
					   operalineaCreditoIF = "";  fechaVencimientoLinea = ""; //F020-2015
						String in						= "";
						int contMN					=0;
						int contUS					=0;
						int plazoAuxiliar				= 9999;

						Vector datosDocumento = (Vector)doctos.get(doc);

						rs_documento				= (String)datosDocumento.get( 0);
						rs_monto						= (String)datosDocumento.get( 1);
						rs_moneda_docto			= (String)datosDocumento.get( 2);
						rs_por_desc					= (String)datosDocumento.get( 3);
						rs_monto_descuento	= (String)datosDocumento.get( 4);
						rs_tipo_conversion	= (String)datosDocumento.get( 5);
						rs_tipo_cambio 			= (String)datosDocumento.get( 6);
						rs_if								= (String)datosDocumento.get( 7);
						rs_tipo_piso				= (String)datosDocumento.get( 8);
						rs_linea_credito		= (String)datosDocumento.get( 9);
						rs_moneda_linea			= (String)datosDocumento.get(10);
						rs_saldo_disponible	= (String)datosDocumento.get(11);
						rs_plazo 						= (String)datosDocumento.get(12);
						rs_fecha_venc_cred	= (String)datosDocumento.get(13);
            rs_epoA = (String)datosDocumento.get(14);
            rs_pymeA = (String)datosDocumento.get(15);
            fechahabil = (String)datosDocumento.get(16);
						
					   operalineaCreditoIF = (String)datosDocumento.get(17);//F020-2015
					   fechaVencimientoLinea = (String)datosDocumento.get(18); //F020-2015
						
						if(vHorarioEpo) {


							//GEAG F048-2005
							//Verifica si la pyme en turno no est� bloqueada
							if (pymesBloqueadas.contains(rs_pyme)) {
								//Como la pyme est� bloqueada no se hace el descuento
								setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 94, con);
								continue; //Continua con el docto
							}
							//GEAG FIN F048-2005

							if(!"".equals(rs_linea_credito)){
								Vector tasaDocumento 	= tasasBean.ovgetTasasxPyme(4,rs_pyme,rs_if,"1");
								
								//--- Fodea 05-2014 
								Vector tasaValP =  tasasBean.getTasasPreferNego("4", "P",  rs_if, rs_epo, rs_pyme, rs_moneda_docto, "2"  );
								Vector tasaValN =  tasasBean.getTasasPreferNego("4", "N",  rs_if, rs_epo, rs_pyme, rs_moneda_docto ,  "2" );
								
								if(tasaValP.size()>0 && tasaValN.size()>0)  { 	tasaDocumento = tasaValN; 	}
								if(tasaValP.size()>0 && tasaValN.size()==0)  { 	tasaDocumento = tasaValP; 	}
								if(tasaValP.size()==0 && tasaValN.size()>0)  { 	tasaDocumento = tasaValN; 	}
								//--- Fodea 05-2014

								Vector datosTasa 			= new Vector();
								Vector tasasFinales 	= new Vector();
								Vector tasaAcum 			= new Vector();

								if(tasaValP.size()==0 && tasaValN.size()==0)  {

									//Vector tasaDocumento = validaPlazoDocto(rs_epo,rs_pyme,rs_if,rs_moneda_linea,rs_plazo,con);

									if(tasaDocumento.size()>=1) {
										for(int x =0;x<tasaDocumento.size();x++) {
											tasaAcum	= new Vector();
											datosTasa = (Vector)tasaDocumento.get(x);

											rs_tasa					= (String)datosTasa.get(2);
											rs_rel_mat			= (String)datosTasa.get(5);
											rs_puntos				= (String)datosTasa.get(6);
											rs_tasa_aplicar	= (String)datosTasa.get(7);
											rs_moneda				= (String)datosTasa.get(9)==null?"":(String)datosTasa.get(9);
											rs_plazo_dias		= (String)datosTasa.get(10);
											//rs_tipo_tasa		= (String)datosTasa.get(1);

											tasaAcum.add((String)datosTasa.get(2));
											tasaAcum.add((String)datosTasa.get(5));
											tasaAcum.add((String)datosTasa.get(6));
											tasaAcum.add((String)datosTasa.get(7));
											tasaAcum.add((String)datosTasa.get(9));
											tasaAcum.add((String)datosTasa.get(10));

											tasasFinales.add(tasaAcum);

											if(rs_moneda.equals("1"))
													contMN++;
											if(rs_moneda.equals("54"))
													contUS++;
										}
									}
									if(contMN==0 || contUS==0){
										if(contMN==0)	in +="1";
										if(contUS==0){
												if(!in.equals(""))in += ",";
											in +="54";
										}

										tasaDocumento = tasasBean.ovgetTasas(4,in);

										for(int x =0;x<tasaDocumento.size();x++) {
											tasaAcum	= new Vector();
											datosTasa = (Vector)tasaDocumento.get(x);

											tasaAcum.add((String)datosTasa.get(2));
											tasaAcum.add((String)datosTasa.get(5));
											tasaAcum.add((String)datosTasa.get(6));
											tasaAcum.add((String)datosTasa.get(7));
											tasaAcum.add((String)datosTasa.get(10));
											tasaAcum.add((String)datosTasa.get(11));

											tasasFinales.add(tasaAcum);
										}

										System.out.println("num reg----"+tasasFinales.size());

										for(int x=0; x<tasasFinales.size();x++){
											datosTasa = (Vector)tasasFinales.get(x);

											if(Integer.parseInt(rs_plazo)<=Integer.parseInt((String)datosTasa.get(5))
													&& ((String)datosTasa.get(4)).equals(rs_moneda_linea)
													&& Integer.parseInt((String)datosTasa.get(5))<=plazoAuxiliar)
											{

												rs_tasa						= (String)datosTasa.get(0);
												rs_rel_mat				= (String)datosTasa.get(1);
												rs_puntos					= (String)datosTasa.get(2);
												rs_tasa_aplicar		= (String)datosTasa.get(3);
												rs_moneda					= (String)datosTasa.get(4)==null?"":(String)datosTasa.get(4);
												rs_plazo_dias			= (String)datosTasa.get(5);
												//rs_tipo_tasa		= (String)datosTasa.get(1);

												plazoAuxiliar =Integer.parseInt((String)datosTasa.get(5));
											}
										}
									}

								}else  {
										//--- Fodea 05-2014
									for(int x =0;x<tasaDocumento.size();x++) {
										tasaAcum	= new Vector();
										datosTasa = (Vector)tasaDocumento.get(x);

										rs_tasa					= (String)datosTasa.get(2);
										rs_rel_mat			= (String)datosTasa.get(5);
										rs_puntos				= (String)datosTasa.get(6);
										rs_tasa_aplicar	= (String)datosTasa.get(4);
										rs_moneda				= (String)datosTasa.get(13)==null?"":(String)datosTasa.get(13);
										rs_plazo_dias		= (String)datosTasa.get(14);

										plazoAuxiliar =Integer.parseInt((String)datosTasa.get(14));

									}
									//--- Fodea 05-2014
									System.out.println("monto rs_tasa_aplicar---"+rs_tasa_aplicar);
								}



								if(datosTasa.size()>=1 && plazoAuxiliar!=9999){
									double mtoDocFinal	= 0;
									double montoTasaInt	= 0;
									double montoInt		= 0;

									//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN

									if("54".equals(rs_moneda_docto)&&"1".equals(rs_moneda_linea)&&!"".equals(rs_tipo_conversion)) {
										double montoValuado	= "".equals(rs_tipo_cambio)?0:(Double.parseDouble(rs_tipo_cambio)*(Double.parseDouble(rs_monto) - Double.parseDouble(rs_monto_descuento)));
										mtoDocFinal	= montoValuado;
									} else
										mtoDocFinal = Double.parseDouble(rs_monto) - Double.parseDouble(rs_monto_descuento);
									if("1".equals(rs_tipo_piso)){
										montoTasaInt = (double)((mtoDocFinal*(Double.parseDouble(rs_tasa_aplicar)/100)/360)*100)/100;
									}else{
										System.out.println("monto final---"+mtoDocFinal);
										montoTasaInt = (mtoDocFinal*(Double.parseDouble(rs_tasa_aplicar)/100)/360);
									}
									montoInt = Double.parseDouble(Comunes.formatoDecimal(montoTasaInt*Double.parseDouble(rs_plazo),2,false));

  System.out.println("mtoDocFinal::"+mtoDocFinal);
  System.out.println("rs_saldo_disponible::"+rs_saldo_disponible);

									if(mtoDocFinal<Double.parseDouble(rs_saldo_disponible)) {

										try {

											if(doctosinsert==0&&acuse==null) {
																acuse = new Acuse(7,"4","com",con);
												creaAcuseDstoAuto(acuse.toString(), rs_usuario, con);
											}


                       //Fodea 014-Distribuidores Fase II 2010                     inhabilEpoPyme.equals("")
                            boolean Inhabil = false;
                            String  diahabil = "";
                            diahabil = rs_fecha_venc_cred;
                            Inhabil  = DiaInhabil(diahabil);
                            inhabilEpoPyme =getDiasInhabilesDes(rs_pymeA,rs_epoA,diahabil);
                            int no_dia_semana = 0;

                            Calendar gcDiaFecha = new GregorianCalendar();
                            java.util.Date fechaVencNvo = Comunes.parseDate(diahabil);
                            gcDiaFecha.setTime(fechaVencNvo);
                            no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);


                            if(no_dia_semana==Calendar.SATURDAY ) { // 7 Sabado
                             if(Inhabil==true || !inhabilEpoPyme.equals(""))  {
                               //menosdias= Fecha.restaFechas(rs_fecha_venc_cred,  fechahabil);
                               rs_plazoHabil = Integer.parseInt(rs_plazo) -1;
                               rs_plazo=String.valueOf(rs_plazoHabil);
                               diahabil  =  Fecha.sumaFechaDias(diahabil, -1);
                             }
                            }

                            if(no_dia_semana==Calendar.SUNDAY) { // domingo
                             if (Inhabil= true || !inhabilEpoPyme.equals("") ) {
                             // menosdias= Fecha.restaFechas(rs_fecha_venc_cred,  fechahabil);
                              rs_plazoHabil = Integer.parseInt(rs_plazo) -2;
                              rs_plazo=String.valueOf(rs_plazoHabil);
                              diahabil  =  Fecha.sumaFechaDias(diahabil, -2);
                             }
                            }
                            System.out.println("diahabil: "+diahabil);

                            if(no_dia_semana!=Calendar.SATURDAY && no_dia_semana!=Calendar.SUNDAY) {
                              if (!inhabilEpoPyme.equals("")) {

                                //menosdias= Fecha.restaFechas(rs_fecha_venc_cred, fechahabil);
                                rs_plazoHabil = Integer.parseInt(rs_plazo)-1;
                                rs_plazo=String.valueOf(rs_plazoHabil);
                                diahabil  =  Fecha.sumaFechaDias(rs_fecha_venc_cred, -1);

                                Inhabil  = DiaInhabil(diahabil);
                                inhabilEpoPyme =getDiasInhabilesDes(rs_pymeA,rs_epoA,diahabil);

                              System.out.println("diahabil: "+diahabil);

                                java.util.Date fechaVencNvo1 = Comunes.parseDate(diahabil);
                                gcDiaFecha.setTime(fechaVencNvo1);
                                no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);

                             System.out.println(no_dia_semana+"----"+diahabil+"------"+inhabilEpoPyme);

                              if(no_dia_semana==Calendar.SATURDAY ) { // 7 Sabado
                               if(Inhabil==true || !inhabilEpoPyme.equals(""))  {
                                 //menosdias= Fecha.restaFechas(rs_fecha_venc_cred,  fechahabil);
                                 rs_plazoHabil = Integer.parseInt(rs_plazo) -1;
                                 rs_plazo=String.valueOf(rs_plazoHabil);
                                 diahabil  =  Fecha.sumaFechaDias(diahabil, -1);
                                 System.out.println("Paso Porqui Sabado::::");
                               }
                              }


                              if(no_dia_semana==Calendar.SUNDAY ) { // domingo
                               if (Inhabil= true || !inhabilEpoPyme.equals("") ) {
                               // menosdias= Fecha.restaFechas(rs_fecha_venc_cred,  fechahabil);
                                rs_plazoHabil = Integer.parseInt(rs_plazo) -2;
                                rs_plazo=String.valueOf(rs_plazoHabil);
                                diahabil  =  Fecha.sumaFechaDias(diahabil, -2);
                                System.out.println("Paso Porqui domingo::::");
                               }
                              }
                              }
                            }

                             if (Inhabil= false && inhabilEpoPyme.equals("")){
                               rs_plazo = rs_plazo;
                               diahabil = rs_fecha_venc_cred;
                             }

                            System.out.println(rs_plazoHabil +"----"+ diahabil);


                            if (Inhabil= true || !inhabilEpoPyme.equals("") ){
                               System.out.println(" Fecha de Vencimiento del documento es un dia inhabil:::::"+rs_documento);
                            }
                      //Fodea 014-Distribuidores Fase II 2010

							 //Foda 020-2015
							 java.util.Date  fechaDenDocto = Comunes.parseDate(diahabil);
							 java.util.Date  fechaVenLinea = Comunes.parseDate(fechaVencimientoLinea);                                
							 // fechaVenDocto es menor a fechaVenLinea =  -1 
							 // fechaVenDocto es igual a fechaVenLinea = 0 
							 // fechaVenDocto es mayor a fechaVenLinea =  1                             
							 if (operalineaCreditoIF.trim().equals("S")  && fechaDenDocto.compareTo(fechaVenLinea)==1  ){
									 System.out.println("El documento no puede vencer el d�a "+diahabil+" por que la L�nea de Cr�dito que tiene con el Intermediario vence el d�a "+fechaVencimientoLinea);                             
							 }else {

											confirmaDoctoDsctoAtutoCCC(
												rs_documento, acuse.toString(), mtoDocFinal, montoInt,
												rs_plazo, diahabil,rs_tipo_cambio,
												rs_tasa, rs_rel_mat, rs_puntos, rs_tasa_aplicar,
												rs_tipo_tasa, rs_puntos_pref,rs_linea_credito,con);



											doctosinsert++;

											if("1".equals(rs_moneda_linea)) {
												total_monto		+= Double.parseDouble(rs_monto);
												total_int		+= montoInt;
												total_credito	+= mtoDocFinal;
											} else if("54".equals(rs_moneda_linea)) {
												total_monto_dl	+= Double.parseDouble(rs_monto);
												total_int_dl	+= montoInt;
												total_credito_dl+= mtoDocFinal;
											}
										}

										} catch (NafinException nexc) {
											if("DIST0044".equals(nexc.getCodError())) {
												/**Limite Pyme insuficiente**************************/
											setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 99, con);
											}
										}
									} else {
										/**Saldo de l�nea insuficiente**************************/
										setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 91, con);
									}//else if(mtoDocFinal>Double.parseDouble(rs_saldo_disponible))
								} else {//-----------------------------------------------------------------cierre de cambio
									/**No existe tasa para ese  plazo**************************/
									setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 86, con);
								} //else if(tasaDocumento.size()>0)
							} else {
								/**La L�nea no esta vigente**************************/
								setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 90, con);
							} //else if(!"".equals(rs_linea_credito)
						} else {
							/**Epo fuera de horario de servicio**************************/
							setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 83, con);
						} // if(vHorarioEpo) else
					}//for (int doc=0;doc<doctos.size();doc++
				}//if(doctos.size()>0)
				if(doctosinsert>0){
					List lstCorreo = new ArrayList();
					String correo_c = "";
					envioCorreoEpo = "N";
					setAcuseDstoAuto(acuse.toString(),total_monto, total_int, total_credito, total_monto_dl, total_int_dl, total_credito_dl, con);
					String qrymail = "select cg_email from com_contacto  " +
										"where ic_pyme = ? " +
										"and cs_primer_contacto = 'S' ";

					System.out.println("rs_pyme>>>>>>>>>>>>>>>>"+rs_pyme);
					ps2 = con.queryPrecompilado(qrymail);
					ps2.setLong(1,Long.parseLong(rs_pyme));
					rs2 = ps2.executeQuery();
					if(rs2!=null && rs2.next()){
						correo_c = rs2.getString("cg_email");
					}
					if(ps2!=null)ps2.close();
					if(rs2!=null)rs2.close();

					String qryEpoCorreo = "select cs_envio_correo_elec from comrel_producto_epo " +
											"where ic_epo = ? " +
											"and ic_producto_nafin =4 " +
											"and cs_habilitado = 'S' ";
					System.out.println("rs_epo>>>>>>>>>>>>>>>>"+rs_epo);
					ps2 = con.queryPrecompilado(qryEpoCorreo);
					ps2.setLong(1,Long.parseLong(rs_epo));
					rs2 = ps2.executeQuery();
					if(rs2!=null && rs2.next()){
						envioCorreoEpo  = rs2.getString("cs_envio_correo_elec");
					}
					if(ps2!=null)ps2.close();
					if(rs2!=null)rs2.close();

					if((envioCorreoEpo==null || "".equals(envioCorreoEpo))){
						qryEpoCorreo = "SELECT cs_envio_correo_elec " +
										"  FROM comcat_producto_nafin " +
										" WHERE ic_producto_nafin = 4 ";
						ps2 = con.queryPrecompilado(qryEpoCorreo);
						rs2 = ps2.executeQuery();
						if(rs2!=null && rs2.next()){
							envioCorreoEpo  = rs2.getString("cs_envio_correo_elec");
						}
						if(ps2!=null)ps2.close();
						if(rs2!=null)rs2.close();
					}


					lstCorreo.add(acuse.toString());
					lstCorreo.add(correo_c);
					lstCorreo.add(envioCorreoEpo);
					lstCorreosEnviar.add(lstCorreo);
					//con.terminaTransaccion(true);


				}con.terminaTransaccion(true);
			}//while

      if (!rs_epo.equals("") && !rs_pyme.equals("") && !rs_moneda_auto.equals("") ) {
      Vector nofueromProcesadasCCC = new Vector();
      nofueromProcesadasCCC =  nofueromProcesadasCCC( rs_epo, rs_pyme, rs_moneda_auto,con);
      System.out.println("nofueromProcesadasCCC::" +nofueromProcesadasCCC);
      }
			/*codigo nuevo para correo*/
			if(lstCorreosEnviar!=null && lstCorreosEnviar.size()>0 && "S".equals(envioCorreoEpo)){
				con.terminaTransaccion(true);
				String strDirectorioTemp = ruta;
				String strDirectorioFisico = strDirectorioTemp+"00tmp/24finandist/";
				for(int z=0; z<lstCorreosEnviar.size(); z++){
					List lstCorreosGen = (List)lstCorreosEnviar.get(z);
					if("S".equals((String)lstCorreosGen.get(2))){
						System.out.println("antes de generar pdf...................................");
            System.out.println("Datos Correo  "+lstCorreosGen);
            System.out.println("(String)lstCorreosGen.get(0) "+(String)lstCorreosGen.get(0));
            System.out.println("(String)lstCorreosGen.get(1) "+(String)lstCorreosGen.get(1));
            System.out.println("(String)lstCorreosGen.get(2) "+(String)lstCorreosGen.get(2));
            System.out.println("CCC");
            System.out.println("strDirectorioTemp "+strDirectorioTemp);
            System.out.println("strDirectorioFisico "+strDirectorioFisico);
            String nombreArchivo = generaPDFDocDist((String)lstCorreosGen.get(0),"CCC",strDirectorioTemp,strDirectorioFisico,"DIST_DESC_AUT_"+(String)lstCorreosGen.get(0));

						System.out.println("despues de generar pdf...................."+nombreArchivo);
						String mensajeCorreo 	= 	"<html>															"  +
											"       <head>													"  +
											"               <title>DESCUENTO ELECTRONICO NAFIN</title>	"  +
											"       </head>												"  +
											"       <body>													"	+
											"			<table><tr><td>DATOS ADJUNTOS PROCESO DESCUENTO AUTOMATICO DISTRIBUIDORES</td></tr></table>		" +
											"	</body>														" 	+
											"</html>															";
						ArrayList listaDeImagenes = null;
						ArrayList listaDeArchivos = new ArrayList();

						nombreArchivo = strDirectorioFisico+nombreArchivo;
						HashMap 			archivoAdjunto 		 = new HashMap();
						archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );//applicationPath + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif");

						listaDeArchivos.add(archivoAdjunto);
						Correo correo = new Correo();
						System.out.println("antes de enviar correo...................."+(String)lstCorreosGen.get(1));
						correo.enviaCorreoConDatosAdjuntos("cadenas@nafin.gob.mx",(String)lstCorreosGen.get(1),"","DESCUENTO ELECTRONICO NAFIN",mensajeCorreo,listaDeImagenes,listaDeArchivos);
						System.out.println("despues de enviar correo....................");
					}//ciere if(se enviara correo?)
				}
			}
			/*codigo nuevo para correo*/
			rs.close();
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::selecAutoPymeDistCCC(Exception): "+e);
			e.printStackTrace();
			con.terminaTransaccion(false);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::selecAutoPymeDistCCC(S)");
    		con.terminaTransaccion(true);
    	}
	} // fin del m�todo procesoSelecPyme.


private static Vector getDoctosDesctoAutoCCC(String ic_epo,String ic_pyme,String moneda_auto, AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::getDoctosDesctoAutoCCC(E)");
		String qrySentencia	= "";
		StringBuffer  linea_credito = new StringBuffer();
		PreparedStatement ps	= null;
		ResultSet rs		= null;
		Vector datosDocto	= new Vector();
		Vector columnas		= null;
		try {
			if(!"".equals(moneda_auto)) {
				if("N".equals(moneda_auto))
					moneda_auto = "1";
				else if("D".equals(moneda_auto))
					moneda_auto = "54";
				else if("A".equals(moneda_auto))
					moneda_auto = "1,54";
			}//if(!"".equals(rs_moneda_auto))

			qrySentencia =
				" select ic_linea_credito from com_linea_credito "+
			 	" where ic_epo = ? "+
				" and ic_pyme = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(ic_epo));
			ps.setInt( 2,Integer.parseInt(ic_pyme));

			rs = ps.executeQuery();

			while(rs.next()) {
				linea_credito.append(rs.getString(1)==null?"":rs.getString(1)+",");
			}

			if(!linea_credito.toString().equals("")) {
				linea_credito.deleteCharAt(linea_credito.length()-1);
			}

			String strAlterMex = " alter session set NLS_TERRITORY = 'MEXICO' ";
			con.ejecutaUpdateDB(strAlterMex);

			qrySentencia =
			 " SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(cif CP_COMCAT_IF_PK) use_nl(doc cif clc)*/ "+
			         " doc.ic_documento, doc.fn_monto, doc.ic_moneda AS moneda_docto, doc.fn_porc_descuento, "+
			         " fn_monto * (NVL (doc.fn_porc_descuento, 0) / 100) AS monto_descuento, "+
			         " DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), 'N', '', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
			         " DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			         " 'P', DECODE (doc.ic_moneda, 54, tc.fn_valor_compra, '1'), '1' ) AS tipo_cambio, "+
			         " lc.ic_if, cif.ig_tipo_piso, lc.ic_linea_credito, lc.ic_moneda AS moneda_linea, "+
			         " lc.saldo_disponible, "+
						" decode(doc.ic_moneda, 1,     (SIGFECHAHABIL(TRUNC (SYSDATE)+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-')-TRUNC (SYSDATE)),  "+
						" 								54,    (SIGFECHAHABIL(TRUNC (SYSDATE+1)+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-')-TRUNC (SYSDATE+1))   ) AS ig_plazo_pyme,  "+
						" decode(doc.ic_moneda, 1, TO_CHAR (SIGFECHAHABIL(TRUNC (SYSDATE)+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-'), 'DD/MM/YYYY'), " +
						"                       54,  TO_CHAR (SIGFECHAHABIL(TRUNC (SYSDATE+1)+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-'), 'DD/MM/YYYY')  )  AS fecha_venc_cred  ,  "+

	               " TO_CHAR (ANTDIAHABIL(doc.df_fecha_venc+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),1), 'DD/MM/YYYY') AS fechaHabil, "+
               " doc.ic_epo as epo, doc.ic_pyme  as pyme "+
			 
					" , pi.cg_linea_credito AS cg_linea_credito "+
	 	         " ,  TO_CHAR (lc.df_vencimiento_adicional, 'dd/mm/yyyy') AS fecha_venc_linea "+                   
	 	                 
			    " FROM dis_documento doc, "+
			         " comcat_if cif, "+
			         " comrel_producto_epo pe, "+
			         " comcat_producto_nafin pn, "+
			         " com_tipo_cambio tc, "+
			         " comrel_pyme_epo_x_producto pep,  "+
               " com_parametrizacion_epo pa, "+
					" comrel_producto_if pi,  "+  
			         " (SELECT ic_linea_credito, ic_if, ic_moneda, ic_tipo_cobro_interes, "+
			           " fn_monto_autorizado_total, fn_saldo_linea_total, "+
			           " (fn_saldo_linea_total) AS saldo_disponible ,  df_vencimiento_adicional  "+
			           " FROM com_linea_credito "+
			           " WHERE TRUNC (df_vencimiento_adicional) > TRUNC (SYSDATE) "+
			           " AND (fn_saldo_linea_total ) > 0 "+
			           " AND cg_tipo_solicitud = 'I' "+
			           " AND ic_linea_credito in( "+linea_credito.toString() +" )" +
			           " AND ic_estatus_linea = 12)lc "+
			   " WHERE  lc.ic_if = cif.ic_if  "+
			     " AND doc.ic_epo = pe.ic_epo "+
			     " AND doc.ic_producto_nafin = pe.ic_producto_nafin "+
			     " AND pe.ic_producto_nafin = pn.ic_producto_nafin "+
			     " AND doc.ic_moneda = tc.ic_moneda "+
			     " AND doc.ic_epo = pep.ic_epo   "+
			     " AND doc.ic_pyme = pep.ic_pyme "+
			     " AND pep.ic_producto_nafin = 4 "+
					 " AND doc.DF_FECHA_VENC >=trunc(SYSDATE) "+
					 " AND doc.DF_FECHA_VENC <trunc(SYSDATE+1) "+
			     " AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
			                          " FROM com_tipo_cambio "+
			                          " WHERE ic_moneda = doc.ic_moneda) "+
			     //" AND lc.ic_moneda IN (1) "+
			     " AND lc.ic_tipo_cobro_interes = 2 "+
			     " AND doc.ic_tipo_financiamiento IN (2) "+
			     " AND doc.ic_estatus_docto = 2 "+
			     " AND doc.ic_epo = ? "+
    			  " AND doc.ic_pyme = ? "+
				  " AND doc.ic_epo = pa.ic_epo "+
				  " AND pa.CC_PARAMETRO_EPO = 'PUB_EPO_DESC_AUTOMATICO' "+
				  " AND pa.CG_VALOR ='S'"+
				  " and lc.ic_moneda = doc.ic_moneda  "+
				  " AND pi.ic_if = lc.ic_if "+
	 	        " AND pi.ic_producto_nafin = 4 "+
				  " and doc.IG_TIPO_PAGO = 1 ";  //Solo documentos Financiamiento con intereses 

System.out.println("qrySentencia: "+qrySentencia);
System.out.println("ic_epo: "+ic_epo);
System.out.println("ic_pyme: "+ic_pyme);
System.out.println("linea_credito: "+linea_credito);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(ic_epo));
			ps.setInt( 2,Integer.parseInt(ic_pyme));

			rs = ps.executeQuery();
			while(rs.next()) {
				columnas = new Vector();
				columnas.addElement((rs.getString("ic_documento")==null)?"":rs.getString("ic_documento"));
				columnas.addElement((rs.getString("fn_monto")==null)?"":rs.getString("fn_monto"));
				columnas.addElement((rs.getString("moneda_docto")==null)?"":rs.getString("moneda_docto"));
				columnas.addElement((rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento"));
				columnas.addElement((rs.getString("monto_descuento")==null)?"":rs.getString("monto_descuento"));
				columnas.addElement((rs.getString("tipo_conversion")==null)?"":rs.getString("tipo_conversion"));
				columnas.addElement((rs.getString("tipo_cambio")==null)?"":rs.getString("tipo_cambio"));
				columnas.addElement((rs.getString("ic_if")==null)?"":rs.getString("ic_if"));
				columnas.addElement((rs.getString("ig_tipo_piso")==null)?"":rs.getString("ig_tipo_piso"));
				columnas.addElement((rs.getString("ic_linea_credito")==null)?"":rs.getString("ic_linea_credito"));
				columnas.addElement((rs.getString("moneda_linea")==null)?"":rs.getString("moneda_linea"));
				columnas.addElement((rs.getString("saldo_disponible")==null)?"":rs.getString("saldo_disponible"));
				columnas.addElement((rs.getString("ig_plazo_pyme")==null)?"":rs.getString("ig_plazo_pyme"));
				columnas.addElement((rs.getString("fecha_venc_cred")==null)?"":rs.getString("fecha_venc_cred"));
		    columnas.addElement((rs.getString("epo")==null)?"":rs.getString("epo"));
		    columnas.addElement((rs.getString("pyme")==null)?"":rs.getString("pyme"));
        columnas.addElement((rs.getString("fechaHabil")==null)?"":rs.getString("fechaHabil"));
			columnas.addElement((rs.getString("cg_linea_credito")==null)?"":rs.getString("cg_linea_credito"));
			columnas.addElement((rs.getString("fecha_venc_linea")==null)?"":rs.getString("fecha_venc_linea")); 
			         
        datosDocto.addElement(columnas);
			}//while
			rs.close();
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::getDoctosDesctoAutoCCC(Exception): "+e);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::getDoctosDesctoAutoCCC(S)");
    	}
    	return datosDocto;
	} // fin del m�todo getDoctosDesctoAuto.

		private static void confirmaDoctoDsctoAtutoCCC(
										String ic_documento, String cc_acuse, double mtoDocFinal, double montoTasaInt,
										String ig_plazo, String df_fecha_venc_cred,String tipo_cambio,
										String ic_tasa, String cg_rel_mat, String fn_puntos, String tasa_aplicar, String tipo_tasa, String fn_puntos_pref, String linea_credito ,AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::confirmaDoctoDsctoAtutoCCC(E)");
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		try {

		qrySentencia= " delete  from  dis_docto_seleccionado where ic_documento = "+ic_documento;
		ps = con.queryPrecompilado(qrySentencia);
		ps.executeUpdate();
		if(ps!=null) ps.close();


		qrySentencia =
				" INSERT INTO dis_docto_seleccionado ("   +
				"                            ic_documento, cc_acuse,"   +
				"                            fn_importe_recibir,"   +
				"                            fn_importe_interes,"   +
				"                            ic_tasa, cg_rel_mat,"   +
				"                            fn_puntos, fn_valor_tasa,"   +
				"                            cg_tipo_tasa, fn_puntos_pref,"   +
				"                            df_fecha_seleccion)"   +
				"      VALUES(?, ?, ?, ?, ?, ?, ?, ?, ? , ?, SYSDATE)"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_documento));
			ps.setString( 2, cc_acuse);
			ps.setDouble( 3, mtoDocFinal);
			ps.setDouble( 4, montoTasaInt);
			ps.setInt( 5, Integer.parseInt(ic_tasa));
			ps.setString(6, cg_rel_mat);
			ps.setDouble( 7, Double.parseDouble(fn_puntos));
			ps.setDouble( 8, Double.parseDouble(tasa_aplicar));
			if(tipo_tasa!= null && !"".equals(tipo_tasa))
				ps.setString( 9, tipo_tasa);
			else
					ps.setNull(9, Types.VARCHAR);

			if(fn_puntos_pref!= null && !"".equals(fn_puntos_pref))
				ps.setDouble(10, Double.parseDouble(fn_puntos_pref));
			else
					ps.setNull(10, Types.FLOAT);

			System.out.println("\n inicial_ic_documento: "+ic_documento);
			ps.executeUpdate();
			if(ps!=null) ps.close();

      System.out.println("qrySentencia: "+qrySentencia);
			try {

				qrySentencia =
					" UPDATE dis_documento"   +
					"    SET ic_estatus_docto = 3,"   +
					"        ig_plazo_credito = ?,"   +
					"        df_fecha_venc_credito = TO_DATE (?, 'dd/mm/yyyy'),"   +
					"        ic_linea_credito = ?,"   +
					"        fn_tipo_cambio = ?"   +
					"  WHERE ic_documento = ?"  ;

				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt( 1, Integer.parseInt(ig_plazo));
				ps.setString( 2, df_fecha_venc_cred);
				ps.setInt( 3, Integer.parseInt(linea_credito));
				ps.setDouble( 4, Double.parseDouble(tipo_cambio));
				ps.setInt( 5, Integer.parseInt(ic_documento));
				ps.executeUpdate();
				if(ps!=null) ps.close();

  System.out.println("qrySentencia: "+qrySentencia);

			} catch (SQLException sqle) {
				System.out.println("sqle.getErrorCode():"+sqle.getErrorCode());
				sqle.printStackTrace();
				if (sqle.getErrorCode()==20001) {
					throw new NafinException("DIST0042");
				}
			}

    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::confirmaDoctoDsctoAtutoCCC(Exception): "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::confirmaDoctoDsctoAtutoCCC(S)");
    	}
	} // fin del m�todo creaAcuseDstoAuto.

	private String generaPDFDocDist(String acuse,String tipoCred,String strDirectorioTemp,String strDirectorioPublicacion, String fileName)throws NafinException{
		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();

			String 			nombreArchivo 		= fileName+".pdf";
			List vecFilas = new ArrayList();

			String fechaHoy 	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			//VARIABLES TOTALES
			int	totalDoctosMN	= 0;
			double	totalMontoMN	= 0.0;
			double	totalMontoDescMN	= 0.0;
			double	totalInteresMN		= 0.0;
			double	totalMontoImpMN	= 0.0;
			int	totalDoctosUSD		= 0;
			double	totalMontoUSD		= 0.0;
			double	totalMontoDescUSD	= 0.0;
			double	totalInteresUSD	= 0.0;
			double	totalMontoImpUSD	= 0.0;
			int	totalDoctosConv	= 0;
			double	totalMontosConv	= 0.0;
			double	totalMontoDescConv = 0.0;
			double	totalMontoImpConv	= 0.0;
			double	totalMontoIntConv	= 0.0;


			//variable de espliego
			String icMoneda 				= "";
			String nombreEpo				= "";
			String igNumeroDocto			= "";
			String ccAcuse 				= "";
			String dfFechaEmision		= "";
			String dfFechaVencimiento	= "";
			String dfFechaPublicacion 	= "";
			String igPlazoDocto			= "";
			String moneda 					= "";
			double fnMonto 				= 0;
			String cgTipoConv				= "";
			String tipoCambio				= "";
			double montoValuado 			= 0;
			String igPlazoDescuento		= "";
			String fnPorcDescuento		= "";
			double montoDescuento 		= 0;
			String modoPlazo				= "";
			String estatus 				= "";
			String cambios					= "";
			String numeroCredito			= "";
			String nombreIf				= "";
			String tipoLinea				= "";
			String fechaOperacion		= "";
			double montoCredito			= 0;
			String referencia				= "";
			String plazoCredito			= "";
			String fechaVencCredito		= "";
			double valorTasaInt			= 0;
			double montoTasaInt			= 0;
			double fnPuntos				= 0;
			String relMat					= "";
			String tipoCobroInt			= "";
			String  tipoPiso				= "";
			String icTipoCobroInt		= "";
			String icTasa					= "";
			String acuseFormateado		= "";
			String usuario					= "";
			String fechaCarga				= "";
			String horaCarga				= "";
			String icLineaCredito		= "";
			String monedaLinea			= "";
			double montoCapitalInt		= 0;
			String nombreMLinea			= "";
			String icTipoFinanciamiento = "";
			//fin variables de despliegue

			//String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			//String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			//String diaActual    	= fechaActual.substring(0,2);
			//String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			//String anioActual   	= fechaActual.substring(6,10);
			//String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			//String strEncabezado = "M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
			//			  " ----------------------------- " + horaActual;
			//Fin encabezado

			ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioPublicacion+nombreArchivo, "", false, true, true);
			pdfDoc.setEncabezado("Mexico", "", "", "proc_autom", "proc_automatico", "nafin.gif", strDirectorioTemp, "");

			if("CCC".equals(tipoCred))
				vecFilas = consultaDoctosCCC(acuse);
			else
				vecFilas = consultaDoctos(acuse);
			System.out.println("vecFilas.size()>>>>>"+vecFilas.size());

			for(int i=0;i<vecFilas.size();i++){
				List vecColumnas = (Vector)vecFilas.get(i);
				String monedaLinea_ = "";
				String icTipoFinanciamiento_ = "";
				double fnMonto_		= Double.parseDouble(vecColumnas.get(8).toString());
				double montoDescuento_ 		= Double.parseDouble(vecColumnas.get(28).toString());
				//double montoValuado_ 	= "".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
				double montoCredito_		= Double.parseDouble(vecColumnas.get(21).toString());
				double montoTasaInt_		= 0;
				String monenda 		= (vecColumnas.get(32)!=null)?(String)vecColumnas.get(32):"";
				double valorTasaInt_	= Double.parseDouble(vecColumnas.get(24).toString());
				//String relMat_			= (String)vecColumnas.get(25);
				//double fnPuntos_		= Double.parseDouble(vecColumnas.get(26).toString());
				//String tipoCambio_	= (String)vecColumnas.get(10);
				//String cgTipoConv_	= (String)vecColumnas.get(9);
				String tipoPiso_		= (String)vecColumnas.get(30);

				if("CCC".equals(tipoCred)){
					monedaLinea_ = (vecColumnas.get(35)!=null)?(String)vecColumnas.get(35):"";
					icTipoFinanciamiento_ = (vecColumnas.get(34)!=null)?(String)vecColumnas.get(34):"";
					montoTasaInt_ = Double.parseDouble(vecColumnas.get(39).toString());

				}else{
					monedaLinea_ = (vecColumnas.get(38)!=null)?(String)vecColumnas.get(38):"";
					icTipoFinanciamiento_ = (vecColumnas.get(39)!=null)?(String)vecColumnas.get(39):"";
					if("1".equals(tipoPiso_)){
						montoTasaInt_ = (double)Math.round((montoCredito_*(valorTasaInt_/100)/360)*100)/100;
					}else{
						montoTasaInt_ = (montoCredito_*(valorTasaInt_/100)/360);
					}
				}


				if("1".equals(monenda)){
					totalDoctosMN++;
					totalMontoMN += fnMonto_;
					totalMontoDescMN += montoDescuento_;
					totalInteresMN += montoTasaInt_;
					totalMontoImpMN += montoCredito_;
				}
				if("54".equals(moneda)&&"54".equals(monedaLinea_)){
					totalDoctosUSD++;
					totalMontoUSD += fnMonto_;
					totalMontoDescUSD += montoDescuento_;
					totalInteresUSD += montoTasaInt_;
					totalMontoImpUSD += montoCredito_;
				}
				if("54".equals(moneda)&&"1".equals(monedaLinea_)){
					totalDoctosConv++;
					totalMontosConv	+= fnMonto_;
					totalMontoDescConv += montoDescuento_;
					totalMontoImpConv	+= montoCredito_;
					totalMontoIntConv	+= montoTasaInt_;
				}
			}

			float anchos0[]={8f,9f,8f,9f,8f,9f,8f,8f,8f,9f,8f,8f};
				pdfDoc.setTable(12,100,anchos0);
				pdfDoc.setCell("Moneda Inicial", "formasmenB", ComunesPDF.CENTER,4);
				pdfDoc.setCell("Dolares", "formasmenB", ComunesPDF.CENTER,4);
				pdfDoc.setCell("Doctos en DLL financiados en M.N.", "formasmenB", ComunesPDF.CENTER,4);

				pdfDoc.setCell("Num. Total de Doctos.", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Iniciales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Finales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Intereses", "formasmenB", ComunesPDF.CENTER);

				pdfDoc.setCell("Num. Total de Doctos.", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Iniciales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Finales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Intereses", "formasmenB", ComunesPDF.CENTER);

				pdfDoc.setCell("Num. Total de Doctos.", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Iniciales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Finales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Intereses", "formasmenB", ComunesPDF.CENTER);

				pdfDoc.setCell(String.valueOf(totalDoctosMN),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoMN,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoImpMN,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalInteresMN,2),"formasmen", ComunesPDF.CENTER);

				pdfDoc.setCell(String.valueOf(totalDoctosUSD),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoUSD,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoImpUSD,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalInteresUSD,2),"formasmen", ComunesPDF.CENTER);

				pdfDoc.setCell(String.valueOf(totalDoctosConv),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontosConv,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoImpConv,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoIntConv,2),"formasmen", ComunesPDF.CENTER);

				pdfDoc.setCell(
				"Al transmitir este mensaje de datos  y para todos los efectos legales Usted, bajo su responsabilidad, manifiesta su aceptaci�n para que sea(n) sustituido(s) el(los) DOCUMENTO(S)  INICIAL(ES) que ha seleccionado por el(los) DOCUMENTO(S) FINAL(ES) y estos �ltimos sean descontados a favor de la EMPRESA DE PRIMER ORDEN."+
				"Dicho(s)  DOCUMENTO(S)  FINALE(S) contiene(n) los Derechos de Cobro a su cargo por lo que Usted   acepta pagar el 100% de su valor al INTERMEDIARIO FINANCIERO en su fecha de vencimiento."+
				"Manifiesta tambi�n su obligaci�n de cubrir  al INTERMEDIARIO FINANCIERO,  los intereses  que se detallan en esta pantalla, en la fecha de su operaci�n, por haber aceptado la sustituci�n."
				,"formasmen", ComunesPDF.CENTER,12);

				pdfDoc.addTable();
				pdfDoc.addText(" ");

			float anchos[]={6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.6f,6.6f,6.6f,6.6f,6.6f};

			pdfDoc.setTable(15,100,anchos);

			for(int i=0;i<vecFilas.size();i++){
				List vecColumnas = (Vector)vecFilas.get(i);
				nombreEpo				= (String)vecColumnas.get(0);
				igNumeroDocto			= (String)vecColumnas.get(1);
				ccAcuse					= (String)vecColumnas.get(2);
				dfFechaEmision			= (String)vecColumnas.get(3);
				dfFechaVencimiento	= (String)vecColumnas.get(4);
				dfFechaPublicacion 	= (String)vecColumnas.get(5);
				igPlazoDocto			= (String)vecColumnas.get(6);
				moneda 					= (String)vecColumnas.get(7);
				fnMonto					= Double.parseDouble(vecColumnas.get(8).toString());/**************1*/
				cgTipoConv				= (String)vecColumnas.get(9);
				tipoCambio				= (String)vecColumnas.get(10);
				montoValuado 			= "".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
				igPlazoDescuento		= (String)vecColumnas.get(12);
				fnPorcDescuento		= (String)vecColumnas.get(13);
				montoDescuento 		= Double.parseDouble(vecColumnas.get(28).toString());/*********************1a*/
				modoPlazo				= (String)vecColumnas.get(14);
				estatus 					= (String)vecColumnas.get(15);
				cambios					= (String)vecColumnas.get(16);
				//creditos por concepto de intereses
				numeroCredito			= (String)vecColumnas.get(17);
				nombreIf					= (String)vecColumnas.get(18);
				tipoLinea				= (String)vecColumnas.get(19);
				fechaOperacion			= (String)vecColumnas.get(20);
				montoCredito			= Double.parseDouble(vecColumnas.get(21).toString());
				plazoCredito			= (String)vecColumnas.get(22);
				fechaVencCredito		= (String)vecColumnas.get(23);
				valorTasaInt			= Double.parseDouble(vecColumnas.get(24).toString());/************/
				relMat					= (String)vecColumnas.get(25);
				fnPuntos					= Double.parseDouble(vecColumnas.get(26).toString());
				tipoCobroInt			= (String)vecColumnas.get(27);
				referencia				= (String)vecColumnas.get(29);
				tipoPiso					= (String)vecColumnas.get(30);
				icTipoCobroInt			= (String)vecColumnas.get(31);
				icMoneda					= (String)vecColumnas.get(32);
				icTasa					= (String)vecColumnas.get(33);
				if("CCC".equals(tipoCred)){
					icTipoFinanciamiento =(String)vecColumnas.get(34);
					monedaLinea				= (String)vecColumnas.get(35);/*******************/
					icLineaCredito			= (String)vecColumnas.get(38);
					montoTasaInt			= Double.parseDouble(vecColumnas.get(39).toString());
					acuseFormateado 		= (String)vecColumnas.get(40);
					fechaCarga				= (String)vecColumnas.get(41);
					horaCarga				= (String)vecColumnas.get(42);
					usuario					= (String)vecColumnas.get(43);
					nombreMLinea			= (String)vecColumnas.get(44);

					if(icMoneda.equals(monedaLinea)){
						cgTipoConv		= "";
						tipoCambio		= "";
						montoValuado	= fnMonto;
					}
					if("1".equals(icTipoFinanciamiento)){
						//plazoCredito = igPlazoDocto;
						fechaVencCredito = dfFechaVencimiento;
					}
					if("+".equals(relMat))
						valorTasaInt += fnPuntos;
					else if("-".equals(relMat))
						valorTasaInt -= fnPuntos;
					else if("*".equals(relMat))
						valorTasaInt *= fnPuntos;
					else if("/".equals(relMat))
						valorTasaInt /= fnPuntos;

					//montoValuadoLinea = fnMonto;

					plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;

					if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
						montoCredito = fnMonto-montoDescuento;
					else if("2".equals(icTipoFinanciamiento))
						montoCredito = fnMonto;

					//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN
					if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
						if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
							montoCredito = montoValuado;
						else if("2".equals(icTipoFinanciamiento))
							montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
					}


					if("1".equals(tipoPiso)){
						montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
					}else{
						montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
					}
				}else{
					acuseFormateado	=	(String)vecColumnas.get(34);
					usuario				= 	(String)vecColumnas.get(37);
					fechaCarga			= 	(String)vecColumnas.get(35);
					horaCarga			= 	(String)vecColumnas.get(36);
					icTipoFinanciamiento =(String)vecColumnas.get(39);
					monedaLinea			= 	(String)vecColumnas.get(38);
					nombreMLinea		= 	(String)vecColumnas.get(40);

					if("+".equals(relMat))
						valorTasaInt += fnPuntos;
					else if("-".equals(relMat))
						valorTasaInt -= fnPuntos;
					else if("*".equals(relMat))
						valorTasaInt *= fnPuntos;
					else if("/".equals(relMat))
						valorTasaInt /= fnPuntos;
					plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;

					montoCredito = fnMonto -montoDescuento;

					if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
						if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
							montoCredito = montoValuado;
						else if("2".equals(icTipoFinanciamiento))
							montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
					}

					if("1".equals(tipoPiso)){
						montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
					}else{
						montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
					}
					montoCapitalInt = montoCredito +(montoTasaInt*Double.parseDouble(plazoCredito));
				}


				//inicia pdf  27

				if(i==0){
					System.out.println("encabezado de pdf");
					/*A*/pdfDoc.setCell("A", "formasmenB", ComunesPDF.CENTER);
					/*1*/pdfDoc.setCell("Epo", "formasmenB", ComunesPDF.CENTER);
					/*2*/pdfDoc.setCell("Num. docto. inicial", "formasmenB", ComunesPDF.CENTER);
					/*3*/pdfDoc.setCell("Num. acuse", "formasmenB", ComunesPDF.CENTER);
					/*4*/pdfDoc.setCell("Fecha de emisi�n", "formasmenB", ComunesPDF.CENTER);
					/*5*/pdfDoc.setCell("Fecha vencimiento", "formasmenB", ComunesPDF.CENTER);
					/*6*/pdfDoc.setCell("Fecha de publicaci�n", "formasmenB", ComunesPDF.CENTER);
					/*7*/pdfDoc.setCell("Plazo docto.", "formasmenB", ComunesPDF.CENTER);
					/*8*/pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
					/*9*/pdfDoc.setCell("Monto", "formasmenB", ComunesPDF.CENTER);
					/*10*/pdfDoc.setCell("Plazo para descuento en dias", "formasmenB", ComunesPDF.CENTER);
					/*11*/pdfDoc.setCell("% de descuento", "formasmenB", ComunesPDF.CENTER);
					/*12*/pdfDoc.setCell("Monto % de descuento", "formasmenB", ComunesPDF.CENTER);
					/*13*/pdfDoc.setCell("Tipo conv.", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER);
					/*B*/pdfDoc.setCell("B", "formasmenB", ComunesPDF.CENTER);
					/*14*/pdfDoc.setCell("Tipo cambio", "formasmenB", ComunesPDF.CENTER);
					/*15*/pdfDoc.setCell("Monto valuado", "formasmenB", ComunesPDF.CENTER);
					/*16*/pdfDoc.setCell("Modalidad de plazo", "formasmenB", ComunesPDF.CENTER);

					//	Cr�dito por concepto de intereses

					/*17*/pdfDoc.setCell("Num. de docto final", "formasmenB", ComunesPDF.CENTER);
					/*18*/pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
					/*19*/pdfDoc.setCell("Monto cr�dito", "formasmenB", ComunesPDF.CENTER);
					/*20*/pdfDoc.setCell("Plazo", "formasmenB", ComunesPDF.CENTER);
					/*21*/pdfDoc.setCell("Fecha de vencimiento", "formasmenB", ComunesPDF.CENTER);
					/*22*/pdfDoc.setCell("Fecha de operaci�n", "formasmenB", ComunesPDF.CENTER);
					/*23*/pdfDoc.setCell("IF", "formasmenB", ComunesPDF.CENTER);
					/*24*/pdfDoc.setCell("Referencia tasa de inter�s", "formasmenB", ComunesPDF.CENTER);
					/*25*/pdfDoc.setCell("Valor tasa de inter�s", "formasmenB", ComunesPDF.CENTER);
					/*26*/pdfDoc.setCell("Monto de intereses", "formasmenB", ComunesPDF.CENTER);
					/*27*/pdfDoc.setCell("Monto Total de Capital e Inter�s", "formasmenB", ComunesPDF.CENTER);
				}
				System.out.println("igNumeroDocto>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+igNumeroDocto);
				/*A*/pdfDoc.setCell("A", "formasmen", ComunesPDF.CENTER);
				/*1*/pdfDoc.setCell(nombreEpo,"formasmen", ComunesPDF.LEFT);
				/*2*/pdfDoc.setCell(igNumeroDocto,"formasmen", ComunesPDF.LEFT);
				/*3*/pdfDoc.setCell(ccAcuse,"formasmen", ComunesPDF.LEFT);
				/*4*/pdfDoc.setCell(dfFechaEmision,"formasmen", ComunesPDF.LEFT);
				/*5*/pdfDoc.setCell(dfFechaVencimiento,"formasmen", ComunesPDF.LEFT);
				/*6*/pdfDoc.setCell(dfFechaPublicacion,"formasmen", ComunesPDF.LEFT);
				/*7*/pdfDoc.setCell(igPlazoDocto,"formasmen", ComunesPDF.LEFT);
				/*8*/pdfDoc.setCell(moneda,"formasmen", ComunesPDF.LEFT);
				/*9*/pdfDoc.setCell(Comunes.formatoDecimal(fnMonto,2),"formasmen", ComunesPDF.LEFT);
				/*10*/pdfDoc.setCell(igPlazoDescuento,"formasmen", ComunesPDF.LEFT);
				/*11*/pdfDoc.setCell(fnPorcDescuento+(("".equals(fnPorcDescuento))?"":" %"),"formasmen", ComunesPDF.LEFT);
				/*12*/pdfDoc.setCell(Comunes.formatoDecimal(montoDescuento,2),"formasmen", ComunesPDF.LEFT);
				if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
					/*13*/pdfDoc.setCell(cgTipoConv,"formasmen", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
					/*B*/pdfDoc.setCell("B", "formasmen", ComunesPDF.CENTER);
					/*14*/pdfDoc.setCell(tipoCambio,"formasmen", ComunesPDF.LEFT);
					/*15*/pdfDoc.setCell(Comunes.formatoDecimal(montoValuado,2),"formasmen", ComunesPDF.LEFT);

				}else{
					/*13*/pdfDoc.setCell("","formasmen", ComunesPDF.LEFT);
					pdfDoc.setCell("", "formasmen", ComunesPDF.CENTER);
					/*B*/pdfDoc.setCell("B", "formasmen", ComunesPDF.CENTER);
					/*14*/pdfDoc.setCell("","formasmen", ComunesPDF.LEFT);
					/*15*/pdfDoc.setCell("","formasmen", ComunesPDF.LEFT);
				}
				/*16*/pdfDoc.setCell(modoPlazo,"formasmen", ComunesPDF.LEFT);

				//Cr�dito por concepto de intereses

				/*17*/pdfDoc.setCell(numeroCredito,"formasmen", ComunesPDF.LEFT);
				/*18*/pdfDoc.setCell(nombreMLinea,"formasmen", ComunesPDF.LEFT);
				/*19*/pdfDoc.setCell(Comunes.formatoDecimal(montoCredito,2),"formasmen", ComunesPDF.LEFT);
				/*20*/pdfDoc.setCell(plazoCredito,"formasmen", ComunesPDF.LEFT);
				/*21*/pdfDoc.setCell(fechaVencCredito,"formasmen", ComunesPDF.LEFT);
				/*22*/pdfDoc.setCell(fechaHoy,"formasmen", ComunesPDF.LEFT);
				/*23*/pdfDoc.setCell(nombreIf,"formasmen", ComunesPDF.LEFT);
				/*24*/pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":referencia+" "+relMat+" "+fnPuntos,"formasmen", ComunesPDF.LEFT);
				/*25*/pdfDoc.setCell(Comunes.formatoDecimal(valorTasaInt,2)+"%","formasmen", ComunesPDF.LEFT);
				/*26*/pdfDoc.setCell(Comunes.formatoDecimal(montoTasaInt,2),"formasmen", ComunesPDF.LEFT);
				/*27*/pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoCapitalInt,2),"formasmen", ComunesPDF.LEFT);
			}//cierre for
			pdfDoc.addTable();
			//datos de cifra de control
			pdfDoc.addText(" ");
			float anchos2[]={20f,20f};
			pdfDoc.setTable(2,40,anchos2);
			pdfDoc.setCell("Datos de cifras de control","formasmenB", ComunesPDF.CENTER,2);
			pdfDoc.setCell("Num. acuse","formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell(acuse,"formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell("Fecha","formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell(fechaCarga,"formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell("Hora","formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell(horaCarga,"formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell("Nombre y n�mero de usuario","formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell("proc_automatico","formasmen", ComunesPDF.LEFT);
			pdfDoc.addTable();

			pdfDoc.endDocument();
			return nombreArchivo;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

	}

private static Vector nofueromProcesadas(String ic_epo,String ic_pyme,String moneda_auto, AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::nofueromProcesadas(E)");

    System.out.println(" ic_epo::  "+ic_epo);
    System.out.println(" ic_pyme::  "+ic_pyme);
     System.out.println(" moneda_auto::  "+moneda_auto);

		//String condicion	= "";
		String qrySentencia	= "";
		String linea_credito	= "0";
		PreparedStatement ps	= null;
		ResultSet rs		= null;
		Vector datosDocto	= new Vector();
		Vector columnas		= null;
		try {
			if(!"".equals(moneda_auto)) {
				if("N".equals(moneda_auto))
					moneda_auto = "1";
				else if("D".equals(moneda_auto))
					moneda_auto = "54";
				else if("A".equals(moneda_auto))
					moneda_auto = "1,54";
			}//if(!"".equals(rs_moneda_auto))

			qrySentencia =
				" select ic_linea_credito from com_linea_credito "+
			 	" where ic_epo = ? "+
				" and ic_pyme = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(ic_epo));
			ps.setInt( 2,Integer.parseInt(ic_pyme));

			rs = ps.executeQuery();

			if(rs.next()) {
				linea_credito = rs.getString(1)==null?"":rs.getString(1);
			}

			String strAlterMex = " alter session set NLS_TERRITORY = 'MEXICO' ";
			con.ejecutaUpdateDB(strAlterMex);

			qrySentencia =
			 " SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(cif CP_COMCAT_IF_PK) use_nl(doc cif clc)*/ "+
			         " doc.ic_epo, doc.ic_pyme,  doc.ic_documento "+
               " FROM dis_documento doc, "+
			         " comcat_if cif, "+
			         " comrel_producto_epo pe, "+
			         " comcat_producto_nafin pn, "+
			         " com_tipo_cambio tc, "+
			         " comrel_pyme_epo_x_producto pep,  "+
               " com_parametrizacion_epo pa, "+
			         " (SELECT ic_linea_credito, ic_if, ic_moneda, ic_tipo_cobro_interes, "+
			           " fn_monto_autorizado_total, fn_saldo_linea_total, "+
			           " (fn_saldo_linea_total) AS saldo_disponible "+
			           " FROM com_linea_credito "+
			           " WHERE TRUNC (df_vencimiento_adicional) > TRUNC (SYSDATE) "+
			           " AND (fn_saldo_linea_total ) > 0 "+
			           " AND cg_tipo_solicitud = 'I' "+
			           " AND ic_linea_credito = ? "+
			           " AND ic_estatus_linea = 12)lc "+
			   " WHERE  lc.ic_if = cif.ic_if  "+
			     " AND doc.ic_epo = pe.ic_epo "+
			     " AND doc.ic_producto_nafin = pe.ic_producto_nafin "+
			     " AND pe.ic_producto_nafin = pn.ic_producto_nafin "+
			     " AND doc.ic_moneda = tc.ic_moneda "+
			     " AND doc.ic_epo = pep.ic_epo   "+
			     " AND doc.ic_pyme = pep.ic_pyme "+
			     " AND pep.ic_producto_nafin = 4 "+
					 " AND doc.DF_FECHA_VENC >=trunc(SYSDATE) "+
					 " AND doc.DF_FECHA_VENC <trunc(SYSDATE+1) "+
			     " AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
			                          " FROM com_tipo_cambio "+
			                          " WHERE ic_moneda = doc.ic_moneda) "+
			     " AND lc.ic_moneda IN (1) "+
			     " AND lc.ic_tipo_cobro_interes = 2 "+
			     " AND doc.ic_tipo_financiamiento IN (2) "+
			     " AND doc.ic_estatus_docto = 2 "+
			     " AND doc.ic_epo = ? "+
    			 " AND doc.ic_pyme = ? "+
           " AND doc.ic_epo = pa.ic_epo "+
           " AND pa.CC_PARAMETRO_EPO = 'PUB_EPO_DESC_AUTOMATICO' "+
           " AND pa.CG_VALOR ='S'";

System.out.println("qrySentencia: "+qrySentencia);
System.out.println("ic_epo: "+ic_epo);
System.out.println("ic_pyme: "+ic_pyme);
System.out.println("linea_credito: "+linea_credito);

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(linea_credito));
			ps.setInt( 2,Integer.parseInt(ic_epo));
			ps.setInt( 3,Integer.parseInt(ic_pyme));

			rs = ps.executeQuery();
			while(rs.next()) {
				columnas = new Vector();
				columnas.addElement((rs.getString("ic_pyme")==null)?"":rs.getString("ic_pyme"));
				columnas.addElement((rs.getString("ic_epo")==null)?"":rs.getString("ic_epo"));
            columnas.addElement((rs.getString("ic_documento")==null)?"":rs.getString("ic_documento"));
        datosDocto.addElement(columnas);
			}//while
			rs.close();
     //System.out.println("Pymes que no fueron Procesadas: "+datosDocto);
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::nofueromProcesadas(Exception): "+e);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::nofueromProcesadas(S)");
    	}
    	return datosDocto;
	} // fin del m�todo nofueromProcesadas.


private static Vector nofueromProcesadasCCC(String ic_epo,String ic_pyme,String moneda_auto, AccesoDB con) throws NafinException {
		System.out.println(" AceptacionPymeEJB::nofueromProcesadasCCC(E)");

     System.out.println(" ic_epo::  "+ic_epo);
     System.out.println(" ic_pyme::  "+ic_pyme);
     System.out.println(" moneda_auto::  "+moneda_auto);

		String qrySentencia	= "";
		String linea_credito	= "";
		PreparedStatement ps	= null;
		ResultSet rs		= null;
		Vector datosDocto	= new Vector();
		Vector columnas		= null;
		try {
			if(!"".equals(moneda_auto)) {
				if("N".equals(moneda_auto))
					moneda_auto = "1";
				else if("D".equals(moneda_auto))
					moneda_auto = "54";
				else if("A".equals(moneda_auto))
					moneda_auto = "1,54";
			}//if(!"".equals(rs_moneda_auto))

			qrySentencia =
				" select ic_linea_credito from com_linea_credito "+
			 	" where ic_epo = ? "+
				" and ic_pyme = ? ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(ic_epo));
			ps.setInt( 2,Integer.parseInt(ic_pyme));

			rs = ps.executeQuery();

			if(rs.next()) {
				linea_credito = rs.getString(1)==null?"":rs.getString(1);
			}

			String strAlterMex = " alter session set NLS_TERRITORY = 'MEXICO' ";
			con.ejecutaUpdateDB(strAlterMex);

			qrySentencia =
			 " SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(cif CP_COMCAT_IF_PK) use_nl(doc cif clc)*/ "+
			         " doc.ic_documento,  doc.ic_pyme,  doc.ic_epo "+
							 " FROM dis_documento doc, "+
			         " comcat_if cif, "+
			         " comrel_producto_epo pe, "+
			         " comcat_producto_nafin pn, "+
			         " com_tipo_cambio tc, "+
			         " comrel_pyme_epo_x_producto pep,  "+
               " com_parametrizacion_epo pa, "+
			         " (SELECT ic_linea_credito, ic_if, ic_moneda, ic_tipo_cobro_interes, "+
			           " fn_monto_autorizado_total, fn_saldo_linea_total, "+
			           " (fn_saldo_linea_total) AS saldo_disponible "+
			           " FROM com_linea_credito "+
			           " WHERE TRUNC (df_vencimiento_adicional) > TRUNC (SYSDATE) "+
			           " AND (fn_saldo_linea_total ) > 0 "+
			           " AND cg_tipo_solicitud = 'I' "+
			           " AND ic_linea_credito = ? "+
			           " AND ic_estatus_linea = 12)lc "+
			   " WHERE  lc.ic_if = cif.ic_if  "+
			     " AND doc.ic_epo = pe.ic_epo "+
			     " AND doc.ic_producto_nafin = pe.ic_producto_nafin "+
			     " AND pe.ic_producto_nafin = pn.ic_producto_nafin "+
			     " AND doc.ic_moneda = tc.ic_moneda "+
			     " AND doc.ic_epo = pep.ic_epo   "+
			     " AND doc.ic_pyme = pep.ic_pyme "+
			     " AND pep.ic_producto_nafin = 4 "+
					 " AND doc.DF_FECHA_VENC >=trunc(SYSDATE) "+
					 " AND doc.DF_FECHA_VENC <trunc(SYSDATE+1) "+
			     " AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
			                          " FROM com_tipo_cambio "+
			                          " WHERE ic_moneda = doc.ic_moneda) "+
			     " AND lc.ic_moneda IN (1) "+
			     " AND lc.ic_tipo_cobro_interes = 2 "+
			     " AND doc.ic_tipo_financiamiento IN (2) "+
			     " AND doc.ic_estatus_docto = 2 "+
			     " AND doc.ic_epo = ? "+
    			 " AND doc.ic_pyme = ? "+
           " AND doc.ic_epo = pa.ic_epo "+
           " AND pa.CC_PARAMETRO_EPO = 'PUB_EPO_DESC_AUTOMATICO' "+
           " AND pa.CG_VALOR ='S'";

/*System.out.println("qrySentencia: "+qrySentencia);
System.out.println("ic_epo: "+ic_epo);
System.out.println("ic_pyme: "+ic_pyme);
*/
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(linea_credito));
			ps.setInt( 2,Integer.parseInt(ic_epo));
			ps.setInt( 3,Integer.parseInt(ic_pyme));

			rs = ps.executeQuery();
			while(rs.next()) {
				columnas = new Vector();
				columnas.addElement((rs.getString("ic_pyme")==null)?"":rs.getString("ic_pyme"));
				columnas.addElement((rs.getString("ic_epo")==null)?"":rs.getString("ic_epo"));
        columnas.addElement((rs.getString("ic_documento")==null)?"":rs.getString("ic_documento"));
        datosDocto.addElement(columnas);
			}//while
			rs.close();
      //System.out.println("Pymes que no fueron Procesadas CCC: "+datosDocto);
			if(ps!=null) ps.close();
    	} catch (Exception e) {
			System.out.println(" AceptacionPymeEJB::nofueromProcesadasCCC(Exception): "+e);
			throw new NafinException("SIST0001");
    	} finally {
    		System.out.println(" AceptacionPymeEJB::nofueromProcesadasCCC(S)");
    	}
    	return datosDocto;
	} // fin del m�todo nofueromProcesadas.

 /*********************************************************************************************
*	    boolean DiaInhabil()
*********************************************************************************************/
	public boolean DiaInhabil(String esFecha)
    {
    	boolean lbInhabil = false;
      //boolean ok = true;
      int diaInhabil = 0;
      AccesoDB eodbCon = new AccesoDB();
		try	{
        eodbCon.conexionDB();

      StringBuffer sentenciaSQL = new StringBuffer();

      sentenciaSQL.append("SELECT ES_INHABIL(TO_DATE('"+esFecha+"','DD/MM/YYYY')) as INHABIL FROM DUAL");

      System.out.println("sentenciaSQL"+sentenciaSQL);
      ResultSet rs = eodbCon.queryDB(sentenciaSQL.toString());
			if(rs!=null && rs.next()){
			 diaInhabil = rs.getInt(1);
      }
			rs.close();

			if (diaInhabil!=0) {
        lbInhabil = true;
			}
    System.out.println("sentenciaSQL"+sentenciaSQL +"    "+diaInhabil+"    "+esFecha);

		} catch (Exception e) {
			e.printStackTrace();
      System.out.println("Error en DiaInhabil = "+e);
    }finally{
        	if(eodbCon.hayConexionAbierta()){
           	eodbCon.cierraConexionDB();
          }
    }

        return lbInhabil;
	}


 public String getDiasInhabilesDes(String ic_pyme,String ic_epo, String fecha )
   throws NafinException{
	System.out.println("AceptacionPymeBean::getDiasInhabilesDes (E)");


     	AccesoDB 	con = null;
        String		qrySentencia = "";
        ResultSet 	rs =  null;
        String		retorno = "";
        String fecha1 =  fecha.substring(0,5);

        System.out.println(fecha1);
   	try{
        	con = new AccesoDB();
                con.conexionDB();
                qrySentencia =	" select cg_dia_inhabil from comcat_dia_inhabil"+
                " where  cg_dia_inhabil = '"+fecha1+"' and cg_dia_inhabil is not null ";
                if(!"".equals(ic_epo)){
			qrySentencia += " union "   +
				" select cg_dia_inhabil from comrel_dia_inhabil_x_epo"   +
				" where ic_epo = "+ic_epo+
        " and cg_dia_inhabil = '"+fecha1+"' and cg_dia_inhabil is not null";

                }
       System.out.println("***********************");
       System.out.println("ic_epo::: "+ic_epo);
       System.out.println("qrySentencia::: "+qrySentencia);
       System.out.println("***********************");
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	if(!"".equals(retorno))
                        	retorno += ",";
                        retorno += rs.getString(1);
                }
                con.cierraStatement();
        }catch(Exception e){
		System.out.println("aceptacionPyme::getDiasInHabiles Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("AceptacionPymeBean::getDiasInhabiles (S)");
        }
   return retorno;
   }


  /**
	 * Realiza el proceso de seleccion de documentos
	 * automaticamente para Distribuidores de venta de Cartera
	 * cuando la epo esta parametizada con Venta de Cartera
	 * y opera  Descuento Automatico
	 * @param cveProc Clave del proceso
	 * @param con Referencia a la conexi�n de BD.
	 */

	private void selecAutoPymeDistVDC(String cveProc, AccesoDB con, String ruta)
			throws NafinException {
    log.info("selecAutoPymeDistVDC (E)");

		String	qrySentencia	= "";
		PreparedStatement ps	= null;
		ResultSet rs					= null;
		PreparedStatement ps2	= null;
		ResultSet rs2			  	= null;
		List pymesBloqueadas 		= new ArrayList();
		List lstCorreosEnviar   = new ArrayList();
    Acuse	acuse							= null;
		String	rs_documento		= "",	 rs_epo 	        = "", 	rs_pyme     = "", 	rs_usuario          = "",
		rs_monto	              = "",  rs_moneda_docto	= "",	  rs_por_desc	= "",   rs_monto_descuento	= "",
    rs_tipo_conversion			= "",  rs_tipo_cambio	  = "",	  rs_if	      = "",	  rs_tipo_piso	      = "",
    rs_linea_credito				= "",	 rs_moneda_linea	= "",   rs_rel_mat	= "",   rs_plazo 				    = "",
    rs_saldo_disponible		  = "",  rs_fecha_venc_cred	 = "",	 rs_moneda_auto   = "";


		String rs_puntos		= "", 	rs_tasa_aplicar	= "", 	rs_tipo_tasa		= "",
    rs_puntos_pref			= "", 	rs_moneda				= "", 	rs_plazo_dias		= "",
    rs_epoA		          = "",   rs_pymeA				= "",   rs_tasa ="";

    int rs_plazoHabil =0;
		String envioCorreoEpo = "N";
		String fechahabil = "";

		try {
			CapturaTasas tasasBean = ServiceLocator.getInstance().lookup("CapturaTasasEJB", CapturaTasas.class);
			pymesBloqueadas = pymesBloqueadas();
			qrySentencia =
				" SELECT   /*+index(doc) use_nl(doc pep)*/"   +
				"           pep.ic_epo, pep.ic_pyme, 'dscto_aut' AS ic_usuario, pep.cs_moneda_auto "   +
				"     FROM dis_documento doc, comrel_pyme_epo_x_producto pep, DIS_LINEA_CREDITO_DM clc"   +
				"    WHERE doc.ic_epo = pep.ic_epo"   +
				"      AND doc.ic_pyme = pep.ic_pyme"   +
				"	AND doc.ic_epo = clc.ic_epo"	+
				"      AND doc.ic_producto_nafin = pep.ic_producto_nafin"   +
				"      AND clc.ic_linea_credito_dm IS NOT NULL"	+
				"      AND doc.ic_tipo_financiamiento IN (2)"   +
				"      AND doc.ic_estatus_docto = 2"   +
					//Inicia  Fodea 015-2014
				" AND  doc.ic_pyme  not in   (  SELECT bo.ic_pyme FROM COMREL_BLOQUEO_PYME_X_EPO  bo  "+
					" WHERE bo.CS_BLOQUEO = 'B'   " +
					" and bo.IC_PRODUCTO_NAFIN  = 4  "+
					"and  bo.ic_epo  = doc.ic_epo   ) "+
				//Termina  Fodea 015-2014
				" GROUP BY pep.ic_pyme, 'dscto_aut', pep.ic_epo, pep.cs_moneda_auto"  ;
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			int doctosinsert;
			while(rs.next()) {
				double 	total_monto				= 0;
				double 	total_int					= 0;
				double 	total_credito			= 0;
				double 	total_monto_dl		= 0;
				double 	total_int_dl			= 0;
				double 	total_credito_dl	= 0;
				String inhabilEpoPyme = "";
				acuse 						= null;
				rs_epo 						= "";
				rs_pyme 					= "";
				rs_usuario				= "";
				rs_moneda_auto		= "";
				doctosinsert 			= 0;
				rs_epo						= rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				rs_pyme						= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				rs_usuario				= rs.getString("ic_usuario")==null?"":rs.getString("ic_usuario");
				rs_moneda_auto		= rs.getString("cs_moneda_auto")==null?"N":rs.getString("cs_moneda_auto");
				boolean vHorarioEpo = vHorarioEpo(rs_epo,con);

				log.debug("rs_epo:: "+rs_epo);
				log.debug("rs_pyme:: "+rs_pyme);
				log.debug("rs_moneda_auto:: "+rs_moneda_auto);
				Vector doctos = getDoctosDesctoAutoCDV(rs_epo,rs_pyme,rs_moneda_auto,con);
				log.debug("getDoctosDesctoAutoCDV:: "+doctos);

				if(doctos.size()>0){
					for (int doc=0;doc<doctos.size();doc++) {
						rs_documento 				= "";	rs_monto						= "";
						rs_moneda_docto			= "";	rs_por_desc					= "";
						rs_monto_descuento	= ""; rs_tipo_conversion	= "";
						rs_tipo_cambio 			= ""; rs_if								= "";
						rs_tipo_piso 				= "";	rs_linea_credito		= "";
						rs_moneda_linea 		= ""; rs_saldo_disponible = "";
						rs_plazo 						= ""; rs_fecha_venc_cred 	= "";
						rs_tasa 						= ""; rs_rel_mat					= "";
						rs_puntos 					= ""; rs_tasa_aplicar			= "";
						rs_tipo_tasa				= ""; rs_puntos_pref 			= "";
						String in						= "";
						int contMN					=0;
						int contUS					=0;
						int plazoAuxiliar				= 9999;
					   String operalineaCreditoIF ="", fechaVencimientoLinea ="";
						
						Vector datosDocumento = (Vector)doctos.get(doc);

						rs_documento				= (String)datosDocumento.get( 0);
						rs_monto						= (String)datosDocumento.get( 1);
						rs_moneda_docto			= (String)datosDocumento.get( 2);
						rs_por_desc					= (String)datosDocumento.get( 3);
						rs_monto_descuento	= (String)datosDocumento.get( 4);
						rs_tipo_conversion	= (String)datosDocumento.get( 5);
						rs_tipo_cambio 			= (String)datosDocumento.get( 6);
						rs_if								= (String)datosDocumento.get( 7);
						rs_tipo_piso				= (String)datosDocumento.get( 8);
						rs_linea_credito		= (String)datosDocumento.get( 9);
						rs_moneda_linea			= (String)datosDocumento.get(10);
						rs_saldo_disponible	= (String)datosDocumento.get(11);
						rs_plazo 						= (String)datosDocumento.get(12);
						rs_fecha_venc_cred	= (String)datosDocumento.get(13);
            rs_epoA = (String)datosDocumento.get(14);
            rs_pymeA = (String)datosDocumento.get(15);
            fechahabil = (String)datosDocumento.get(16);
						
					   operalineaCreditoIF = (String)datosDocumento.get(17);//F020-2015
						fechaVencimientoLinea = (String)datosDocumento.get(18); //F020-2015
						
						if(vHorarioEpo) {


							//GEAG F048-2005
							//Verifica si la pyme en turno no est� bloqueada
							if (pymesBloqueadas.contains(rs_pyme)) {
								//Como la pyme est� bloqueada no se hace el descuento
								setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 94, con);
								continue; //Continua con el docto
							}
							//GEAG FIN F048-2005


							//Fodea 05-2014
							System.out.println(" --rs_documento -------"+rs_documento);
							if(getPymeBloqueada(rs_epo, rs_pyme , rs_if ).equals("S"))   {
								System.out.println("Por el momento no es posible seleccionar documentos, su servicio ha sido bloqueado por el Intermediario Financiero "+rs_documento);
								continue; //Continua con el docto
							}
							//Fodea 05-2014
							System.out.println(" --rs_documento -------"+rs_documento);


              if(!"".equals(rs_linea_credito)){
								Vector tasaDocumento 	= tasasBean.ovgetTasasxPyme(4,rs_pyme,rs_if,"1");
								Vector datosTasa 			= new Vector();
								Vector tasasFinales 	= new Vector();
								Vector tasaAcum 			= new Vector();
								//Vector tasaDocumento = validaPlazoDocto(rs_epo,rs_pyme,rs_if,rs_moneda_linea,rs_plazo,con);
								if(tasaDocumento.size()>=1) {
									for(int x =0;x<tasaDocumento.size();x++) {
										tasaAcum	= new Vector();
										datosTasa = (Vector)tasaDocumento.get(x);

										rs_tasa					= (String)datosTasa.get(2);
										rs_rel_mat			= (String)datosTasa.get(5);
										rs_puntos				= (String)datosTasa.get(6);
										rs_tasa_aplicar	= (String)datosTasa.get(7);
										rs_moneda				= (String)datosTasa.get(9)==null?"":(String)datosTasa.get(9);
										rs_plazo_dias		= (String)datosTasa.get(10);
										//rs_tipo_tasa		= (String)datosTasa.get(1);

										tasaAcum.add((String)datosTasa.get(2));
										tasaAcum.add((String)datosTasa.get(5));
										tasaAcum.add((String)datosTasa.get(6));
										tasaAcum.add((String)datosTasa.get(7));
										tasaAcum.add((String)datosTasa.get(9));
										tasaAcum.add((String)datosTasa.get(10));

										tasasFinales.add(tasaAcum);

										if(rs_moneda.equals("1"))
												contMN++;
										if(rs_moneda.equals("54"))
												contUS++;
									}
								}
								if(contMN==0 || contUS==0){
								/* if(contMN==0)  in +="1";
									if(contUS==0){
										if(!in.equals(""))in += ",";
			                        in +="54";
	                         }
								 */
								                              
								if(contMN==0 || contMN!=0  )  { in +="1"; }
								if(!in.equals(""))in += ",";
								if(contUS==0 ||  contUS!=0 )  { in +="54"; }
								
								log.debug("moneda Tasas----"+in);
									tasaDocumento = tasasBean.ovgetTasas(4,in);

									for(int x =0;x<tasaDocumento.size();x++) {
										tasaAcum	= new Vector();
										datosTasa = (Vector)tasaDocumento.get(x);

										tasaAcum.add((String)datosTasa.get(2));
										tasaAcum.add((String)datosTasa.get(5));
										tasaAcum.add((String)datosTasa.get(6));
										tasaAcum.add((String)datosTasa.get(7));
										tasaAcum.add((String)datosTasa.get(10));
										tasaAcum.add((String)datosTasa.get(11));

										tasasFinales.add(tasaAcum);
									}

										log.debug("num reg----"+tasasFinales.size());

									for(int x=0; x<tasasFinales.size();x++){
										datosTasa = (Vector)tasasFinales.get(x);

										if(Integer.parseInt(rs_plazo)<=Integer.parseInt((String)datosTasa.get(5))
												&& ((String)datosTasa.get(4)).equals(rs_moneda_linea)
												&& Integer.parseInt((String)datosTasa.get(5))<=plazoAuxiliar)
										{

											rs_tasa						= (String)datosTasa.get(0);
											rs_rel_mat				= (String)datosTasa.get(1);
											rs_puntos					= (String)datosTasa.get(2);
											rs_tasa_aplicar		= (String)datosTasa.get(3);
											rs_moneda					= (String)datosTasa.get(4)==null?"":(String)datosTasa.get(4);
											rs_plazo_dias			= (String)datosTasa.get(5);
											//rs_tipo_tasa		= (String)datosTasa.get(1);

											plazoAuxiliar =Integer.parseInt((String)datosTasa.get(5));
										}
									}
								}
								if(datosTasa.size()>=1 && plazoAuxiliar!=9999){
									double mtoDocFinal	= 0;
									double montoTasaInt	= 0;
									double montoInt		= 0;

									//TIPO DE CONVERSION, TIPO DE CAMBIO PARA DOCUMENTOS EN DLL FINANCIADOS EN MN

									if("54".equals(rs_moneda_docto)&&"1".equals(rs_moneda_linea)&&!"".equals(rs_tipo_conversion)) {
										double montoValuado	= "".equals(rs_tipo_cambio)?0:(Double.parseDouble(rs_tipo_cambio)*(Double.parseDouble(rs_monto) - Double.parseDouble(rs_monto_descuento)));
										mtoDocFinal	= montoValuado;
									} else
										mtoDocFinal = Double.parseDouble(rs_monto) - Double.parseDouble(rs_monto_descuento);
									if("1".equals(rs_tipo_piso)){
										montoTasaInt = (double)((mtoDocFinal*(Double.parseDouble(rs_tasa_aplicar)/100)/360)*100)/100;
									}else{
											log.debug("monto final---"+mtoDocFinal);
										montoTasaInt = (mtoDocFinal*(Double.parseDouble(rs_tasa_aplicar)/100)/360);
									}
									montoInt = Double.parseDouble(Comunes.formatoDecimal(montoTasaInt*Double.parseDouble(rs_plazo),2,false));

  	log.debug("mtoDocFinal::"+mtoDocFinal);
  	log.debug("rs_saldo_disponible::"+rs_saldo_disponible);

									if(mtoDocFinal<Double.parseDouble(rs_saldo_disponible)) {

										try {

											if(doctosinsert==0&&acuse==null) {
																acuse = new Acuse(7,"4","com",con);
												creaAcuseDstoAuto(acuse.toString(), rs_usuario, con);
											}


                       //Fodea 014-Distribuidores Fase II 2010                     inhabilEpoPyme.equals("")
                            boolean Inhabil = false;
                            String  diahabil = "";
                            diahabil = rs_fecha_venc_cred;
                            Inhabil  = DiaInhabil(diahabil);
                            inhabilEpoPyme =getDiasInhabilesDes(rs_pymeA,rs_epoA,diahabil);
                            int no_dia_semana = 0;

                            Calendar gcDiaFecha = new GregorianCalendar();
                            java.util.Date fechaVencNvo = Comunes.parseDate(diahabil);
                            gcDiaFecha.setTime(fechaVencNvo);
                            no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);


                            if(no_dia_semana==Calendar.SATURDAY ) { // 7 Sabado
                             if(Inhabil==true || !inhabilEpoPyme.equals(""))  {
                               //menosdias= Fecha.restaFechas(rs_fecha_venc_cred,  fechahabil);
                               rs_plazoHabil = Integer.parseInt(rs_plazo) -1;
                               rs_plazo=String.valueOf(rs_plazoHabil);
                               diahabil  =  Fecha.sumaFechaDias(diahabil, -1);
                             }
                            }

                            if(no_dia_semana==Calendar.SUNDAY ) { // domingo
                             if (Inhabil== true || !inhabilEpoPyme.equals("") ) {
                             // menosdias= Fecha.restaFechas(rs_fecha_venc_cred,  fechahabil);
                              rs_plazoHabil = Integer.parseInt(rs_plazo) -2;
                              rs_plazo=String.valueOf(rs_plazoHabil);
                              diahabil  =  Fecha.sumaFechaDias(diahabil, -2);
                             }
                            }
                            System.out.println("diahabil: "+diahabil);

                            if(no_dia_semana!=Calendar.SATURDAY && no_dia_semana!=Calendar.SUNDAY ) {
                              if (!inhabilEpoPyme.equals("")) {

                                //menosdias= Fecha.restaFechas(rs_fecha_venc_cred, fechahabil);
                                rs_plazoHabil = Integer.parseInt(rs_plazo)-1;
                                rs_plazo=String.valueOf(rs_plazoHabil);
                                diahabil  =  Fecha.sumaFechaDias(rs_fecha_venc_cred, -1);

                                Inhabil  = DiaInhabil(diahabil);
                                inhabilEpoPyme =getDiasInhabilesDes(rs_pymeA,rs_epoA,diahabil);

                             	log.debug("diahabil: "+diahabil);

                                //Calendar gcDiaFecha1 = new GregorianCalendar();
                                java.util.Date fechaVencNvo1 = Comunes.parseDate(diahabil);
                                gcDiaFecha.setTime(fechaVencNvo1);
                                no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);

                             	log.debug(no_dia_semana+"----"+diahabil+"------"+inhabilEpoPyme);

                              if(no_dia_semana==Calendar.SATURDAY ) { // 7 Sabado
                               if(Inhabil==true || !inhabilEpoPyme.equals(""))  {
                                 rs_plazoHabil = Integer.parseInt(rs_plazo) -1;
                                 rs_plazo=String.valueOf(rs_plazoHabil);
                                 diahabil  =  Fecha.sumaFechaDias(diahabil, -1);
                                 System.out.println("Paso Porqui Sabado::::");
                               }
                              }


                              if(no_dia_semana==Calendar.SUNDAY) { // domingo
                               if (Inhabil= true || !inhabilEpoPyme.equals("") ) {
                                 rs_plazoHabil = Integer.parseInt(rs_plazo) -2;
                                rs_plazo=String.valueOf(rs_plazoHabil);
                                diahabil  =  Fecha.sumaFechaDias(diahabil, -2);
                                System.out.println("Paso Porqui domingo::::");
                               }
                              }
                              }
                            }

                             if (Inhabil= false && inhabilEpoPyme.equals("")){
                               rs_plazo = rs_plazo;
                               diahabil = rs_fecha_venc_cred;
                             }

                            	log.debug(rs_plazoHabil +"----"+ diahabil);


                            if (Inhabil= true || !inhabilEpoPyme.equals("") ){
                               System.out.println(" Fecha de Vencimiento del documento es un dia inhabil:::::"+rs_documento);
                            }
                      //Fodea 014-Distribuidores Fase II 2010
							
							 //Foda 020-2015
							java.util.Date  fechaVenDocto = Comunes.parseDate(diahabil);
							java.util.Date  fechaVenLinea = Comunes.parseDate(fechaVencimientoLinea);                                
														 
									// fechaVenDocto es menor a fechaVenLinea =  -1 
									// fechaVenDocto es igual a fechaVenLinea = 0 
									// fechaVenDocto es mayor a fechaVenLinea =  1                             
									if (operalineaCreditoIF.trim().equals("S")  && fechaVenDocto.compareTo(fechaVenLinea)==1  ){
										System.out.println("El documento no puede vencer el d�a "+diahabil+" por que la L�nea de Cr�dito que tiene con el Intermediario vence el d�a "+fechaVencimientoLinea);                             
									}else {


											confirmaDoctoDsctoAtutoCCC(
												rs_documento, acuse.toString(), mtoDocFinal, montoInt,
												rs_plazo, diahabil,rs_tipo_cambio,
												rs_tasa, rs_rel_mat, rs_puntos, rs_tasa_aplicar,
												rs_tipo_tasa, rs_puntos_pref,rs_linea_credito,con);



											doctosinsert++;

											if("1".equals(rs_moneda_linea)) {
												total_monto		+= Double.parseDouble(rs_monto);
												total_int		+= montoInt;
												total_credito	+= mtoDocFinal;
											} else if("54".equals(rs_moneda_linea)) {
												total_monto_dl	+= Double.parseDouble(rs_monto);
												total_int_dl	+= montoInt;
												total_credito_dl+= mtoDocFinal;
											}

										}
										} catch (NafinException nexc) {
											if("DIST0044".equals(nexc.getCodError())) {
												System.out.println("/**Limite Pyme insuficiente**************************/");
											setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 99, con);
											}
										}
									} else {
										System.out.println("/**Saldo de l�nea insuficiente**************************/");
										setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 91, con);
									}//else if(mtoDocFinal>Double.parseDouble(rs_saldo_disponible))
								} else {//-----------------------------------------------------------------cierre de cambio
									System.out.println("/**No existe tasa para ese  plazo**************************/");
									setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 86, con);
								} //else if(tasaDocumento.size()>0)
							} else {
								System.out.println("/**La L�nea no esta vigente**************************/");
								setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 90, con);
							} //else if(!"".equals(rs_linea_credito)
						} else {
							System.out.println("/**Epo fuera de horario de servicio**************************/");
							setRechazosDsctoAuto(cveProc, rs_documento, rs_if, 83, con);
						} // if(vHorarioEpo) else
					}//for (int doc=0;doc<doctos.size();doc++
				}//if(doctos.size()>0)
				if(doctosinsert>0){
					List lstCorreo = new ArrayList();
					String correo_c = "";
					envioCorreoEpo = "N";
					setAcuseDstoAuto(acuse.toString(),total_monto, total_int, total_credito, total_monto_dl, total_int_dl, total_credito_dl, con);
					String qrymail = "select cg_email from com_contacto  " +
										"where ic_epo = ? " +
										"and cs_primer_contacto = 'S' ";

						log.debug("rs_pyme>>>>>>>>>>>>>>>>"+rs_pyme);
					ps2 = con.queryPrecompilado(qrymail);
					ps2.setLong(1,Long.parseLong(rs_epo));
					rs2 = ps2.executeQuery();
					if(rs2!=null && rs2.next()){
						correo_c = rs2.getString("cg_email");
					}
					if(ps2!=null)ps2.close();
					if(rs2!=null)rs2.close();

					String qryEpoCorreo = "select cs_envio_correo_elec from comrel_producto_epo " +
											"where ic_epo = ? " +
											"and ic_producto_nafin =4 " +
											"and cs_habilitado = 'S' ";
						log.debug("rs_epo>>>>>>>>>>>>>>>>"+rs_epo);
					ps2 = con.queryPrecompilado(qryEpoCorreo);
					ps2.setLong(1,Long.parseLong(rs_epo));
					rs2 = ps2.executeQuery();
					if(rs2!=null && rs2.next()){
						envioCorreoEpo  = rs2.getString("cs_envio_correo_elec");
					}
					if(ps2!=null)ps2.close();
					if(rs2!=null)rs2.close();

					if((envioCorreoEpo==null || "".equals(envioCorreoEpo))){
						qryEpoCorreo = "SELECT cs_envio_correo_elec " +
										"  FROM comcat_producto_nafin " +
										" WHERE ic_producto_nafin = 4 ";
						ps2 = con.queryPrecompilado(qryEpoCorreo);
						rs2 = ps2.executeQuery();
						if(rs2!=null && rs2.next()){
							envioCorreoEpo  = rs2.getString("cs_envio_correo_elec");
						}
						if(ps2!=null)ps2.close();
						if(rs2!=null)rs2.close();
					}


					lstCorreo.add(acuse.toString());
					lstCorreo.add(correo_c);
					lstCorreo.add(envioCorreoEpo);
					lstCorreosEnviar.add(lstCorreo);
					//con.terminaTransaccion(true);


				}con.terminaTransaccion(true);
			}//while

			/*codigo nuevo para correo*/
			if(lstCorreosEnviar!=null && lstCorreosEnviar.size()>0 && "S".equals(envioCorreoEpo)){
				con.terminaTransaccion(true);
				String strDirectorioTemp = ruta;
				String strDirectorioFisico = strDirectorioTemp+"00tmp/24finandist/";
				for(int z=0; z<lstCorreosEnviar.size(); z++){
					List lstCorreosGen = (List)lstCorreosEnviar.get(z);
					if("S".equals((String)lstCorreosGen.get(2))){
							log.debug("antes de generar pdf...................................");
            	log.debug("Datos Correo  "+lstCorreosGen);
            	log.debug("(String)lstCorreosGen.get(0) "+(String)lstCorreosGen.get(0));
            	log.debug("(String)lstCorreosGen.get(1) "+(String)lstCorreosGen.get(1));
            	log.debug("(String)lstCorreosGen.get(2) "+(String)lstCorreosGen.get(2));
            	log.debug("strDirectorioTemp "+strDirectorioTemp);
           	  log.debug("strDirectorioFisico "+strDirectorioFisico);

             String nombreArchivo = generaPDFDoVCartera((String)lstCorreosGen.get(0),"CDV",strDirectorioTemp,strDirectorioFisico,"DIST_DESC_AUT_"+(String)lstCorreosGen.get(0));

							log.debug("despues de generar pdf...................."+nombreArchivo);
						String mensajeCorreo 	= 	"<html>															"  +
											"       <head>													"  +
											"               <title>DESCUENTO ELECTRONICO NAFIN</title>	"  +
											"       </head>												"  +
											"       <body>													"	+
											"			<table><tr><td>DATOS ADJUNTOS PROCESO DESCUENTO AUTOMATICO DISTRIBUIDORES</td></tr></table>		" +
											"	</body>														" 	+
											"</html>															";
						ArrayList listaDeImagenes = null;
						ArrayList listaDeArchivos = new ArrayList();

						nombreArchivo = strDirectorioFisico+nombreArchivo;
						HashMap 			archivoAdjunto 		 = new HashMap();
						archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );//applicationPath + "00archivos/15cadenas/15archcadenas/logos/nafinsa.gif");

						listaDeArchivos.add(archivoAdjunto);
						Correo correo = new Correo();
							log.debug("antes de enviar correo...................."+(String)lstCorreosGen.get(1));
						correo.enviaCorreoConDatosAdjuntos("cadenas@nafin.gob.mx",(String)lstCorreosGen.get(1),"","DESCUENTO ELECTRONICO NAFIN",mensajeCorreo,listaDeImagenes,listaDeArchivos);
							log.debug("despues de enviar correo....................");
					}//ciere if(se enviara correo?)
				}
			}
			/*codigo nuevo para correo*/
			rs.close();
			if(ps!=null) ps.close();
    	} catch (Exception e) {
				log.error("selecAutoPymeDistVDC(Exception): "+e);
			e.printStackTrace();
			con.terminaTransaccion(false);
			throw new NafinException("SIST0001");
    	} finally {
    			log.info("selecAutoPymeDistVDC (S)");
    		con.terminaTransaccion(true);
    	}
	} // fin del m�todo procesoSelecPyme.



 private static Vector getDoctosDesctoAutoCDV(String ic_epo,String ic_pyme,String moneda_auto, AccesoDB con) throws NafinException {

		log.info("getDoctosDesctoAutoCDV (E)");

		String qrySentencia	= "";
		String linea_credito	= "";
		PreparedStatement ps	= null;
		ResultSet rs		= null;
		Vector datosDocto	= new Vector();
		Vector columnas		= null;
    PreparedStatement ps1	= null;
		ResultSet rs1		= null;
		String valorVentaCartera ="N";

		try {

			if(!"".equals(moneda_auto)) {
				if("N".equals(moneda_auto))
					moneda_auto = "1";
				else if("D".equals(moneda_auto))
					moneda_auto = "54";
				else if("A".equals(moneda_auto))
					moneda_auto = "1,54";
			}//if(!"".equals(rs_moneda_auto))

			qrySentencia =
				" select ic_linea_credito_dm from DIS_LINEA_CREDITO_DM "+
			 	" where ic_epo = ? "+
				" and  ic_producto_nafin =  4  "+
				" AND cg_tipo_solicitud = 'I'  "+
				" AND ic_estatus_linea = 12 ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(ic_epo));

			rs = ps.executeQuery();

			if(rs.next()) {
				linea_credito = rs.getString(1)==null?"":rs.getString(1);
			}

			String strAlterMex = " alter session set NLS_TERRITORY = 'MEXICO' ";
			con.ejecutaUpdateDB(strAlterMex);

			//Para Saber si esta parametrizada por venta de cartera
			qrySentencia = " select cg_valor from com_parametrizacion_epo " +
										 "  where CC_PARAMETRO_EPO ='PUB_EPO_VENTA_CARTERA' "+
										 "   and  ic_epo = "+ic_epo;

			ps1 = con.queryPrecompilado(qrySentencia);
			rs1 = ps1.executeQuery();
			while(rs1.next()) {
				valorVentaCartera = (rs1.getString("cg_valor")==null)?"N":rs1.getString("cg_valor");
			}
			rs1.close();
			if(ps1!=null) ps1.close();


			if (valorVentaCartera.equals("S")) {

			qrySentencia =
			 " SELECT   /*+index(doc CP_DIS_DOCUMENTO_PK) index(cif CP_COMCAT_IF_PK) use_nl(doc cif clc)*/ "+
			         " doc.ic_documento, doc.fn_monto, doc.ic_moneda AS moneda_docto, doc.fn_porc_descuento, "+
			         " fn_monto * (NVL (doc.fn_porc_descuento, 0) / 100) AS monto_descuento, "+
			         " DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), 'N', '', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
			         " DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			         " 'P', DECODE (doc.ic_moneda, 54, tc.fn_valor_compra, '1'), '1' ) AS tipo_cambio, "+
			         " lc.ic_if, cif.ig_tipo_piso, lc.ic_linea_credito_dm, lc.ic_moneda AS moneda_linea, "+
			         " lc.saldo_disponible, "+
               " (SIGFECHAHABIL(doc.df_fecha_venc+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-')-doc.df_fecha_venc) AS ig_plazo_pyme, "+
               " TO_CHAR (SIGFECHAHABIL(doc.df_fecha_venc+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-'), 'DD/MM/YYYY') AS fecha_venc_cred, "+
               " TO_CHAR (ANTDIAHABIL(doc.df_fecha_venc+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),1), 'DD/MM/YYYY') AS fechaHabil, "+
               " doc.ic_epo as epo, doc.ic_pyme  as pyme "+
					"  , pi.CG_LINEA_CREDITO AS CG_LINEA_CREDITO "+
			      " ,to_char(lc.DF_VENCIMIENTO_ADICIONAL,'dd/mm/yyyy') as FECHA_VENC_LINEA  "+
				
				 " FROM dis_documento doc, "+
			         " comcat_if cif, "+
			         " comrel_producto_epo pe, "+
			         " comcat_producto_nafin pn, "+
			         " com_tipo_cambio tc, "+
			         " comrel_pyme_epo_x_producto pep,  "+
               " com_parametrizacion_epo pa, "+
			   " comrel_producto_if pi,  "+


					"  (SELECT ic_linea_credito_DM, ic_if, ic_moneda, ic_tipo_cobro_interes,  "+
					" FN_SALDO_TOTAL, FN_MONTO_AUTORIZADO, "+				
				  "(FN_MONTO_AUTORIZADO) AS saldo_disponible , DF_VENCIMIENTO_ADICIONAL "+
			 
					" FROM DIS_LINEA_CREDITO_DM  "+
					"  WHERE  TRUNC (df_vencimiento_adicional) > TRUNC (SYSDATE)  "+
					" AND (FN_MONTO_AUTORIZADO ) >0 "+
					" AND cg_tipo_solicitud = 'I'  "+
					" AND ic_linea_credito_DM = ?  "+
					" AND ic_estatus_linea = 12)lc  "+

			   " WHERE  lc.ic_if = cif.ic_if  "+
			     " AND doc.ic_epo = pe.ic_epo "+
			     " AND doc.ic_producto_nafin = pe.ic_producto_nafin "+
			     " AND pe.ic_producto_nafin = pn.ic_producto_nafin "+
			     " AND doc.ic_moneda = tc.ic_moneda "+
			     " AND doc.ic_epo = pep.ic_epo   "+
			     " AND doc.ic_pyme = pep.ic_pyme "+
			     " AND pep.ic_producto_nafin = 4 "+
			     " AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
			                          " FROM com_tipo_cambio "+
			                          " WHERE ic_moneda = doc.ic_moneda) "+
			     " AND lc.ic_moneda IN (1) "+
			     //" AND lc.ic_tipo_cobro_interes = 2 "+
			     " AND doc.ic_tipo_financiamiento IN (2) "+
			     " AND doc.ic_estatus_docto = 2 "+
			     " AND doc.ic_epo = ? "+
    			 " AND doc.ic_pyme = ? "+
           " AND doc.ic_epo = pa.ic_epo "+
					 " AND DOC.FN_MONTO <= LC.FN_SALDO_TOTAL "+
           " AND pa.CC_PARAMETRO_EPO ='PUB_EPO_DESC_AUTOMATICO' "+
           " AND pa.CG_VALOR ='S'"+
			"   and  pi.ic_if = lc.ic_if  "+ //F020-2015
			"  AND pi.ic_producto_nafin = 4 "; //F020-2015

      log.debug("qrySentencia: "+qrySentencia);
      log.debug("ic_epo: "+ic_epo);
      log.debug("ic_pyme: "+ic_pyme);
      log.debug("linea_credito: "+linea_credito);

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1,Integer.parseInt(linea_credito));
			ps.setInt( 2,Integer.parseInt(ic_epo));
			ps.setInt( 3,Integer.parseInt(ic_pyme));

			rs = ps.executeQuery();
			while(rs.next()) {
				columnas = new Vector();
				columnas.addElement((rs.getString("ic_documento")==null)?"":rs.getString("ic_documento"));
				columnas.addElement((rs.getString("fn_monto")==null)?"":rs.getString("fn_monto"));
				columnas.addElement((rs.getString("moneda_docto")==null)?"":rs.getString("moneda_docto"));
				columnas.addElement((rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento"));
				columnas.addElement((rs.getString("monto_descuento")==null)?"":rs.getString("monto_descuento"));
				columnas.addElement((rs.getString("tipo_conversion")==null)?"":rs.getString("tipo_conversion"));
				columnas.addElement((rs.getString("tipo_cambio")==null)?"":rs.getString("tipo_cambio"));
				columnas.addElement((rs.getString("ic_if")==null)?"":rs.getString("ic_if"));
				columnas.addElement((rs.getString("ig_tipo_piso")==null)?"":rs.getString("ig_tipo_piso"));
				columnas.addElement((rs.getString("ic_linea_credito_dm")==null)?"":rs.getString("ic_linea_credito_dm"));
				columnas.addElement((rs.getString("moneda_linea")==null)?"":rs.getString("moneda_linea"));
				columnas.addElement((rs.getString("saldo_disponible")==null)?"":rs.getString("saldo_disponible"));
				columnas.addElement((rs.getString("ig_plazo_pyme")==null)?"":rs.getString("ig_plazo_pyme"));
				columnas.addElement((rs.getString("fecha_venc_cred")==null)?"":rs.getString("fecha_venc_cred"));
		    columnas.addElement((rs.getString("epo")==null)?"":rs.getString("epo"));
		    columnas.addElement((rs.getString("pyme")==null)?"":rs.getString("pyme"));
        columnas.addElement((rs.getString("fechaHabil")==null)?"":rs.getString("fechaHabil"));
			columnas.addElement((rs.getString("CG_LINEA_CREDITO")==null)?"":rs.getString("CG_LINEA_CREDITO"));//17
			columnas.addElement((rs.getString("FECHA_VENC_LINEA")==null)?"":rs.getString("FECHA_VENC_LINEA")); //18
        datosDocto.addElement(columnas);
			}//while
			rs.close();
			if(ps!=null) ps.close();

			}
    	} catch (Exception e) {
			log.error(" getDoctosDesctoAutoCDV  (Exception): "+e);
			throw new NafinException("SIST0001");
    	} finally {
    		log.debug("getDoctosDesctoAutoCDV (S)");
    	}
    	return datosDocto;
	} // fin del m�todo getDoctosDesctoAutoCDV.


/**
	 * Genera PDf de venta de Cartera
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param fileName
	 * @param strDirectorioPublicacion
	 * @param strDirectorioTemp
	 * @param tipoCred
	 * @param acuse
	 */

private String generaPDFDoVCartera(String acuse,String tipoCred,String strDirectorioTemp,String strDirectorioPublicacion, String fileName)throws NafinException{
		log.info ("generaPDFDoVCartera (E)  ");

		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();

			String 			nombreArchivo 		= fileName+".pdf";
			List vecFilas = new ArrayList();

			String fechaHoy 	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			//VARIABLES TOTALES
			int	totalDoctosMN	= 0;
			double	totalMontoMN	= 0.0;
			double	totalMontoDescMN	= 0.0;
			double	totalInteresMN		= 0.0;
			double	totalMontoImpMN	= 0.0;
			int	totalDoctosUSD		= 0;
			double	totalMontoUSD		= 0.0;
			double	totalMontoDescUSD	= 0.0;
			double	totalInteresUSD	= 0.0;
			double	totalMontoImpUSD	= 0.0;
			int	totalDoctosConv	= 0;
			double	totalMontosConv	= 0.0;
			double	totalMontoDescConv = 0.0;
			double	totalMontoImpConv	= 0.0;
			double	totalMontoIntConv	= 0.0;


			//variable de espliego
			String icMoneda 				= "";
			String nombreEpo				= "";
			String igNumeroDocto			= "";
			String ccAcuse 				= "";
			String dfFechaEmision		= "";
			String dfFechaVencimiento	= "";
			String dfFechaPublicacion 	= "";
			String igPlazoDocto			= "";
			String moneda 					= "";
			double fnMonto 				= 0;
			String cgTipoConv				= "";
			String tipoCambio				= "";
			double montoValuado 			= 0;
			String igPlazoDescuento		= "";
			String fnPorcDescuento		= "";
			double montoDescuento 		= 0;
			String modoPlazo				= "";
			String estatus 				= "";
			String cambios					= "";
			String numeroCredito			= "";
			String nombreIf				= "";
			String tipoLinea				= "";
			String fechaOperacion		= "";
			double montoCredito			= 0;
			String referencia				= "";
			String plazoCredito			= "";
			String fechaVencCredito		= "";
			double valorTasaInt			= 0;
			double montoTasaInt			= 0;
			double fnPuntos				= 0;
			String relMat					= "";
			String tipoCobroInt			= "";
			String  tipoPiso				= "";
			String icTipoCobroInt		= "";
			String icTasa					= "";
			String acuseFormateado		= "";
			String usuario					= "";
			String fechaCarga				= "";
			String horaCarga				= "";
			String monedaLinea			= "";
			double montoCapitalInt		= 0;
			String nombreMLinea			= "";
			String icTipoFinanciamiento = "";
			String nombrePyme ="";
			//fin variables de despliegue

			//String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			//String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			//String diaActual    	= fechaActual.substring(0,2);
			//String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			//String anioActual   	= fechaActual.substring(6,10);
			//String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			//String strEncabezado = "M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual +
			//			  " ----------------------------- " + horaActual;
			//Fin encabezado

			ComunesPDF pdfDoc = new ComunesPDF(2, strDirectorioPublicacion+nombreArchivo, "", false, true, true);
			pdfDoc.setEncabezado("Mexico", "", "", "proc_autom", "proc_automatico", "nafin.gif", strDirectorioTemp, "");


			vecFilas = consDoctosVCartera(acuse);

			log.debug("vecFilas.size()>>>>>"+vecFilas.size());

			for(int i=0;i<vecFilas.size();i++){
				List vecColumnas = (Vector)vecFilas.get(i);
				String monedaLinea_ = "";
				String icTipoFinanciamiento_ = "";
				double fnMonto_		= Double.parseDouble(vecColumnas.get(8).toString());
				double montoDescuento_ 		= Double.parseDouble(vecColumnas.get(28).toString());
				//double montoValuado_ 	= "".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
				double montoCredito_		= Double.parseDouble(vecColumnas.get(21).toString());
				double montoTasaInt_		= 0;
				String monenda 		= (vecColumnas.get(32)!=null)?(String)vecColumnas.get(32):"";
				double valorTasaInt_	= Double.parseDouble(vecColumnas.get(24).toString());
				//String relMat_			= (String)vecColumnas.get(25);
				//double fnPuntos_		= Double.parseDouble(vecColumnas.get(26).toString());
				//String tipoCambio_	= (String)vecColumnas.get(10);
				//String cgTipoConv_	= (String)vecColumnas.get(9);
				String tipoPiso_		= (String)vecColumnas.get(30);

					monedaLinea_ = (vecColumnas.get(38)!=null)?(String)vecColumnas.get(38):"";
					icTipoFinanciamiento_ = (vecColumnas.get(39)!=null)?(String)vecColumnas.get(39):"";
					if("1".equals(tipoPiso_)){
						montoTasaInt_ = (double)Math.round((montoCredito_*(valorTasaInt_/100)/360)*100)/100;
					}else{
						montoTasaInt_ = (montoCredito_*(valorTasaInt_/100)/360);
					}


				if("1".equals(monenda)){
					totalDoctosMN++;
					totalMontoMN += fnMonto_;
					totalMontoDescMN += montoDescuento_;
					totalInteresMN += montoTasaInt_;
					totalMontoImpMN += montoCredito_;
				}
				if("54".equals(moneda)&&"54".equals(monedaLinea_)){
					totalDoctosUSD++;
					totalMontoUSD += fnMonto_;
					totalMontoDescUSD += montoDescuento_;
					totalInteresUSD += montoTasaInt_;
					totalMontoImpUSD += montoCredito_;
				}
				if("54".equals(moneda)&&"1".equals(monedaLinea_)){
					totalDoctosConv++;
					totalMontosConv	+= fnMonto_;
					totalMontoDescConv += montoDescuento_;
					totalMontoImpConv	+= montoCredito_;
					totalMontoIntConv	+= montoTasaInt_;
				}
			}

			float anchos0[]={8f,9f,8f,9f,8f,9f,8f,8f,8f,9f,8f,8f};
				pdfDoc.setTable(12,100,anchos0);
				pdfDoc.setCell("Moneda Inicial", "formasmenB", ComunesPDF.CENTER,4);
				pdfDoc.setCell("Dolares", "formasmenB", ComunesPDF.CENTER,4);
				pdfDoc.setCell("Doctos en DLL financiados en M.N.", "formasmenB", ComunesPDF.CENTER,4);

				pdfDoc.setCell("Num. Total de Doctos.", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Iniciales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Finales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Intereses", "formasmenB", ComunesPDF.CENTER);

				pdfDoc.setCell("Num. Total de Doctos.", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Iniciales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Finales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Intereses", "formasmenB", ComunesPDF.CENTER);

				pdfDoc.setCell("Num. Total de Doctos.", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Iniciales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Total de Doctos. Finales", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Intereses", "formasmenB", ComunesPDF.CENTER);

				pdfDoc.setCell(String.valueOf(totalDoctosMN),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoMN,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoImpMN,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalInteresMN,2),"formasmen", ComunesPDF.CENTER);

				pdfDoc.setCell(String.valueOf(totalDoctosUSD),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoUSD,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoImpUSD,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalInteresUSD,2),"formasmen", ComunesPDF.CENTER);

				pdfDoc.setCell(String.valueOf(totalDoctosConv),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontosConv,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoImpConv,2),"formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalMontoIntConv,2),"formasmen", ComunesPDF.CENTER);

				pdfDoc.setCell(
				"Al transmitir este mensaje de datos  y para todos los efectos legales Usted, bajo su responsabilidad, manifiesta su aceptaci�n para que sea(n) sustituido(s) el(los) DOCUMENTO(S)  INICIAL(ES) que ha seleccionado por el(los) DOCUMENTO(S) FINAL(ES) y estos �ltimos sean descontados a favor de la EMPRESA DE PRIMER ORDEN."+
				"Dicho(s)  DOCUMENTO(S)  FINALE(S) contiene(n) los Derechos de Cobro a su cargo por lo que Usted   acepta pagar el 100% de su valor al INTERMEDIARIO FINANCIERO en su fecha de vencimiento."+
				"Manifiesta tambi�n su obligaci�n de cubrir  al INTERMEDIARIO FINANCIERO,  los intereses  que se detallan en esta pantalla, en la fecha de su operaci�n, por haber aceptado la sustituci�n."
				,"formasmen", ComunesPDF.CENTER,12);

				pdfDoc.addTable();
				pdfDoc.addText(" ");

			float anchos[]={6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.5f,6.6f,6.6f,6.6f,6.6f};

			pdfDoc.setTable(14,100,anchos);

			for(int i=0;i<vecFilas.size();i++){
				List vecColumnas    = (Vector)vecFilas.get(i);
				nombreEpo				    = (String)vecColumnas.get(0);
				igNumeroDocto			  = (String)vecColumnas.get(1);
				ccAcuse					    = (String)vecColumnas.get(2);
				dfFechaEmision			= (String)vecColumnas.get(3);
				dfFechaVencimiento	= (String)vecColumnas.get(4);
				dfFechaPublicacion 	= (String)vecColumnas.get(5);
				igPlazoDocto			  = (String)vecColumnas.get(6);
				moneda 					    = (String)vecColumnas.get(7);
				fnMonto					    = Double.parseDouble(vecColumnas.get(8).toString());/**************1*/
				cgTipoConv				  = (String)vecColumnas.get(9);
				tipoCambio				  = (String)vecColumnas.get(10);
				montoValuado 			  = "".equals(tipoCambio)?fnMonto:(Double.parseDouble(tipoCambio)*fnMonto);
				igPlazoDescuento		= (String)vecColumnas.get(12);
				fnPorcDescuento		  = (String)vecColumnas.get(13);
				montoDescuento 		  = Double.parseDouble(vecColumnas.get(28).toString());/*********************1a*/
				modoPlazo				    = (String)vecColumnas.get(14);
				estatus 					  = (String)vecColumnas.get(15);
				cambios					    = (String)vecColumnas.get(16);
				//creditos por concepto de intereses
				numeroCredito			  = (String)vecColumnas.get(17);
				nombreIf					  = (String)vecColumnas.get(18);
				tipoLinea				    = (String)vecColumnas.get(19);
				fechaOperacion			= (String)vecColumnas.get(20);
				montoCredito			  = Double.parseDouble(vecColumnas.get(21).toString());
				plazoCredito			  = (String)vecColumnas.get(22);
				fechaVencCredito		= (String)vecColumnas.get(23);
				valorTasaInt			  = Double.parseDouble(vecColumnas.get(24).toString());/************/
				relMat					    = (String)vecColumnas.get(25);
				fnPuntos					  = Double.parseDouble(vecColumnas.get(26).toString());
				tipoCobroInt			  = (String)vecColumnas.get(27);
				referencia				  = (String)vecColumnas.get(29);
				tipoPiso					  = (String)vecColumnas.get(30);
				icTipoCobroInt			= (String)vecColumnas.get(31);
				icMoneda					  = (String)vecColumnas.get(32);
				icTasa					    = (String)vecColumnas.get(33);
				nombrePyme		      =	(String)vecColumnas.get(45);
				acuseFormateado	    =	(String)vecColumnas.get(34);
				usuario				      =	(String)vecColumnas.get(37);
				fechaCarga			    =	(String)vecColumnas.get(41);
				horaCarga			      = (String)vecColumnas.get(42);
				icTipoFinanciamiento =(String)vecColumnas.get(39);
				monedaLinea			     =(String)vecColumnas.get(38);
				nombreMLinea		     = (String)vecColumnas.get(40);

					if("+".equals(relMat))
						valorTasaInt += fnPuntos;
					else if("-".equals(relMat))
						valorTasaInt -= fnPuntos;
					else if("*".equals(relMat))
						valorTasaInt *= fnPuntos;
					else if("/".equals(relMat))
						valorTasaInt /= fnPuntos;
					plazoCredito = "".equals(plazoCredito)?"0":plazoCredito;

					montoCredito = fnMonto -montoDescuento;

					if("54".equals(icMoneda)&&"1".equals(monedaLinea)&&!"".equals(cgTipoConv)){
						if("1".equals(icTipoFinanciamiento)||"3".equals(icTipoFinanciamiento))
							montoCredito = montoValuado;
						else if("2".equals(icTipoFinanciamiento))
							montoCredito = (Double.parseDouble(tipoCambio)*fnMonto);
					}

					if("1".equals(tipoPiso)){
						montoTasaInt = (double)Math.round((montoCredito*(valorTasaInt/100)/360)*100)/100;
					}else{
						montoTasaInt = (montoCredito*(valorTasaInt/100)/360);
					}

				montoCapitalInt = montoCredito - (montoTasaInt*Double.parseDouble(plazoCredito));


				//inicia pdf  27

				if(i==0){
					System.out.println("encabezado de pdf");
					/*A*/pdfDoc.setCell("A", "formasmenB", ComunesPDF.CENTER);
					/*1*/pdfDoc.setCell("Pyme", "formasmenB", ComunesPDF.CENTER);
					/*2*/pdfDoc.setCell("Num. docto. inicial", "formasmenB", ComunesPDF.CENTER);
					/*3*/pdfDoc.setCell("Num. acuse", "formasmenB", ComunesPDF.CENTER);
					/*4*/pdfDoc.setCell("Fecha de emisi�n", "formasmenB", ComunesPDF.CENTER);
					/*5*/pdfDoc.setCell("Fecha vencimiento", "formasmenB", ComunesPDF.CENTER);
					/*6*/pdfDoc.setCell("Fecha de publicaci�n", "formasmenB", ComunesPDF.CENTER);
					/*7*/pdfDoc.setCell("Plazo docto.", "formasmenB", ComunesPDF.CENTER);
					/*8*/pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
					/*9*/pdfDoc.setCell("Monto", "formasmenB", ComunesPDF.CENTER);
					/*10*/pdfDoc.setCell("Tipo conv.", "formasmenB", ComunesPDF.CENTER);
					/*11*/pdfDoc.setCell("Tipo cambio", "formasmenB", ComunesPDF.CENTER);
					/*12*/pdfDoc.setCell("Monto valuado", "formasmenB", ComunesPDF.CENTER);
					/*13*/pdfDoc.setCell("Estatus", "formasmenB", ComunesPDF.CENTER);



					/*B*/pdfDoc.setCell("B", "formasmenB", ComunesPDF.CENTER);
					//	Cr�dito por concepto de intereses
					/*14*/pdfDoc.setCell("Num. de docto final", "formasmenB", ComunesPDF.CENTER);
					/*15*/pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
					/*16*/pdfDoc.setCell("Monto cr�dito", "formasmenB", ComunesPDF.CENTER);
					/*17*/pdfDoc.setCell("Plazo", "formasmenB", ComunesPDF.CENTER);
					/*18*/pdfDoc.setCell("Fecha de operaci�n", "formasmenB", ComunesPDF.CENTER);
					/*19*/pdfDoc.setCell("IF", "formasmenB", ComunesPDF.CENTER);
					/*20*/pdfDoc.setCell("Referencia tasa de inter�s", "formasmenB", ComunesPDF.CENTER);
					/*21*/pdfDoc.setCell("Valor tasa de inter�s", "formasmenB", ComunesPDF.CENTER);
					/*22*/pdfDoc.setCell("Monto de intereses", "formasmenB", ComunesPDF.CENTER);
					/*23*/pdfDoc.setCell("Monto Total de Capital menos Inter�s", "formasmenB", ComunesPDF.CENTER);
				  /*24*/pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER);
					/*25*/pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER);
          /*26*/pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER);
				}
				System.out.println("igNumeroDocto>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+igNumeroDocto);

				/*A*/pdfDoc.setCell("A", "formasmen", ComunesPDF.CENTER);
				/*1*/pdfDoc.setCell(nombrePyme,"formasmen", ComunesPDF.LEFT);
				/*2*/pdfDoc.setCell(igNumeroDocto,"formasmen", ComunesPDF.CENTER);
				/*3*/pdfDoc.setCell(ccAcuse,"formasmen", ComunesPDF.CENTER);
				/*4*/pdfDoc.setCell(dfFechaEmision,"formasmen", ComunesPDF.CENTER);
				/*5*/pdfDoc.setCell(dfFechaVencimiento,"formasmen", ComunesPDF.CENTER);
				/*6*/pdfDoc.setCell(dfFechaPublicacion,"formasmen", ComunesPDF.CENTER);
				/*7*/pdfDoc.setCell(igPlazoDocto,"formasmen", ComunesPDF.CENTER);
				/*8*/pdfDoc.setCell(moneda,"formasmen", ComunesPDF.CENTER);
				/*9*/pdfDoc.setCell(Comunes.formatoDecimal(fnMonto,2),"formasmen", ComunesPDF.CENTER);
				/*10*/pdfDoc.setCell(cgTipoConv,"formasmen", ComunesPDF.CENTER);
				/*11*/pdfDoc.setCell(tipoCambio,"formasmen", ComunesPDF.CENTER);
				/*12*/pdfDoc.setCell(Comunes.formatoDecimal(montoValuado,2),"formasmen", ComunesPDF.CENTER);
				/*13*/pdfDoc.setCell(estatus,"formasmen", ComunesPDF.CENTER);


					/*B*/pdfDoc.setCell("B", "formasmen", ComunesPDF.CENTER);
				//Cr�dito por concepto de intereses

				/*14*/pdfDoc.setCell(numeroCredito,"formasmen", ComunesPDF.CENTER);
				/*15*/pdfDoc.setCell(nombreMLinea,"formasmen", ComunesPDF.CENTER);
				/*16*/pdfDoc.setCell(Comunes.formatoDecimal(montoCredito,2),"formasmen", ComunesPDF.CENTER);
				/*17*/pdfDoc.setCell(plazoCredito,"formasmen", ComunesPDF.CENTER);
				/*18*/pdfDoc.setCell(fechaHoy,"formasmen", ComunesPDF.CENTER);
				/*19*/pdfDoc.setCell(nombreIf,"formasmen", ComunesPDF.CENTER);
				/*20*/pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":referencia+" "+relMat+" "+fnPuntos,"formasmen", ComunesPDF.CENTER);
				/*21*/pdfDoc.setCell(Comunes.formatoDecimal(valorTasaInt,2)+"%","formasmen", ComunesPDF.CENTER);
				/*22*/pdfDoc.setCell(Comunes.formatoDecimal(montoTasaInt,2),"formasmen", ComunesPDF.CENTER);
				/*23*/pdfDoc.setCell(("S".equals(icTipoCobroInt))?"":"$"+Comunes.formatoDecimal(montoCapitalInt,2),"formasmen", ComunesPDF.CENTER);
				/*24*/pdfDoc.setCell("","formasmen", ComunesPDF.CENTER);
				/*25*/pdfDoc.setCell("","formasmen", ComunesPDF.CENTER);
				/*26*/pdfDoc.setCell("","formasmen", ComunesPDF.CENTER);


			}//cierre for
			pdfDoc.addTable();
			//datos de cifra de control
			pdfDoc.addText(" ");
			float anchos2[]={20f,20f};
			pdfDoc.setTable(2,40,anchos2);
			pdfDoc.setCell("Datos de cifras de control","formasmenB", ComunesPDF.CENTER,2);
			pdfDoc.setCell("Num. acuse","formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell(acuse,"formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell("Fecha","formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell(fechaCarga,"formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell("Hora","formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell(horaCarga,"formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell("Nombre y n�mero de usuario","formasmen", ComunesPDF.LEFT);
			pdfDoc.setCell("proc_automatico","formasmen", ComunesPDF.LEFT);
			pdfDoc.addTable();

			pdfDoc.endDocument();
			return nombreArchivo;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

	}
 /**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param in
	 * @param fecha_publicacion_a
	 * @param fecha_publicacion_de
	 * @param fecha_vto_a
	 * @param fecha_vto_de
	 * @param con_cambio
	 * @param fecha_emision_a
	 * @param fecha_emision_de
	 * @param monto_desc
	 * @param cc_acuse
	 * @param fn_monto_a
	 * @param fn_monto_de
	 * @param ig_numero_docto
	 * @param ic_moneda
	 * @param ic_pyme
	 * @param ic_epo
	 */


  public Vector consultaDoctosVentaCartera(String ic_epo,String ic_pyme
	   	,String ic_moneda
        , String ig_numero_docto
        ,String fn_monto_de
        ,String fn_monto_a
        ,String cc_acuse
        ,String monto_desc
        ,String fecha_emision_de
        ,String fecha_emision_a
        ,String con_cambio
        ,String fecha_vto_de
        ,String fecha_vto_a
        ,String fecha_publicacion_de
        ,String fecha_publicacion_a
        ,String in
   )

   throws NafinException{
		log.info("consultaDoctosVentaCartera (S)");
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas = new Vector();
        Vector	vecColumnas = null;
        String 	condicion = "";

        boolean ok = true;
   	try{
        	con = new AccesoDB();
          con.conexionDB();

		/*se establece zona mexico*/
					String strAlterMex = " alter session set NLS_TERRITORY = 'MEXICO' ";
					con.ejecutaUpdateDB(strAlterMex);
					/*fin */

			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion += " and D.ic_pyme = "+ic_pyme;
			if(!"0".equals(ic_moneda))
				condicion += " and D.ic_moneda ="+ic_moneda;
			if("0".equals(ic_moneda))
				condicion += " and D.ic_moneda in (1,54)";
      if(!"".equals(ig_numero_docto)&&ig_numero_docto!=null)
				condicion += " and D.ig_numero_docto = '"+ig_numero_docto+"'";
      if(!"".equals(cc_acuse)&&cc_acuse!=null)
				condicion += " and D.cc_acuse = '"+cc_acuse+"'";
      if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&!"S".equals(monto_desc))
				condicion += " and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a;
			if("S".equals(con_cambio))
				condicion += " and D.ic_documento in(select ic_documento from dis_cambio_estatus where trunc(dc_fecha_cambio)= trunc(sysdate))";
      if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&"S".equals(monto_desc))
				condicion += " and D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) between "+fn_monto_de+" and "+fn_monto_a;
			if(!"".equals(fecha_emision_de)&&fecha_emision_de!=null&&!"".equals(fecha_emision_a)&&fecha_emision_a!=null)
				condicion += " and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_vto_de)&&fecha_vto_de!=null&&!"".equals(fecha_vto_a)&&fecha_vto_a!=null)
				condicion += " and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_publicacion_de)&&fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&&fecha_publicacion_a!=null)
				condicion += " and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))";
      if(!"".equals(in)&&in!=null)
				condicion += " and D.ic_documento in("+in+")";

		/*	ESTE QUERY LLEVA RESTRICCI�N DE LA TASA CON EL PLAZO, ESO SE HAR� CON PROGRAMACION, POR ESO SE COMENT�, Y SE DEJ� COMO APARECE ABAJO	JRFH*/

		qrySentencia =
					"      SELECT /*+index(d IN_DIS_DOCUMENTO_02_NUK)*/ "+
					"		   distinct PYM.CG_RAZON_SOCIAL AS pyme"   +
					"      ,D.IG_NUMERO_DOCTO"   +
					"      ,D.CC_ACUSE"   +
					"      ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION"   +
					"      ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC"   +
					"      ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION"   +
					"      ,D.IG_PLAZO_DOCTO AS PLAZO"   +
					"      ,M.CD_NOMBRE AS MONEDA"   +
					"      ,D.FN_MONTO AS MONTO"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,''),'') as TIPO_CAMBIO"   +
					"      ,'' as MONTO_VALUADO"   +
					"      ,D.ig_plazo_descuento"   +
					"      ,D.fn_porc_descuento"   +
					"      ,TF.cd_descripcion as MODO_PLAZO"   +
					"      ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"      ,'' AS CAMBIOS"   +
					"      ,D.IC_DOCUMENTO AS NUMERO_CREDITO"   +
					"      ,I.CG_RAZON_SOCIAL AS IF"   +
					"      ,DECODE(LC.CG_TIPO_SOLICITUD,'I','Inicial','A','Ampliacion','R','Renovacion','') as TIPO_SOLICITUD"   +
					"      ,'' AS FECHA_OPERACION"   +
					"      ,'' AS MONTO_CREDITO"   +
					"      ,DECODE(D.ic_tipo_financiamiento,1,trunc(d.df_fecha_venc)-trunc(sysdate),D.IG_PLAZO_CREDITO) as IG_PLAZO_CREDITO"   +
					"      ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
					"      ,'' AS REFERENCIA_INT"   +
					"      ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
					"      , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES"   +
					"	     ,decode(TF.ic_tipo_financiamiento,2,0,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100)) as MONTO_DESCUENTO"   +
					"      ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
					"      ,'' AS VALOR_TASA"   +
					"      ,'' AS REL_MAT"   +
					"      ,'' AS PUNTOS"   +
					"      ,I.IG_TIPO_PISO"   +
					"      ,TCI.ic_tipo_cobro_interes"   +
					"      ,M.IC_MONEDA"   +
					"      ,'' as IC_TASA"   +
					"      ,TF.ic_tipo_financiamiento"   +
					"      ,LC.ic_moneda as MONEDA_LINEA"   +
					"      ,nvl(pe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO"   +
					"      ,nvl(pe.ig_dias_maximo,pn.in_dias_maximo) as DIAS_MAXIMO"   +
					"      ,LC.ic_linea_credito_dm as LINEA_CREDITO"   +
					" 	   , I.ic_if"   +
					"	     , M2.cd_nombre as NOMBRE_MLINEA"+
					"	     , PYM.CG_RAZON_SOCIAL AS NOMBREPYME "+
          "      , e.ic_epo as ic_epo "+
          "      , TO_CHAR (SIGFECHAHABIL(d.df_fecha_venc+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-'), 'DD/MM/YYYY') AS fechaVencimiento "+
          "      ,pep.ig_plazo_pyme as plazopyme, pe.ig_dias_maximo as plazoepo, pn.in_dias_maximo as plazonafin "+
          "		   , (select (SIGFECHAHABIL(d.df_fecha_venc+cpe.ig_plazo_pyme,0,'-')-d.df_fecha_venc) "+//obtiene plazo valido
					"	     from comrel_pyme_epo_x_producto cpe "+
					"	     where pym.ic_pyme = cpe.ic_pyme"+
					"		   and e.ic_epo = cpe.ic_epo"+
					"		   and cpe.ic_producto_nafin=4) AS PLAZO_VALIDO"+
					"		   ,PEP.ig_plazo_pyme AS PLAZO_PYME"		+
          "      ,tf.ic_tipo_financiamiento as plazofinanciamiento, "+
          "      tci.IC_TIPO_COBRO_INTERES  as interes,"+
          "      pe.CG_TIPOS_CREDITO as credito "+

					 ", d.ic_epo as ic_epo "+
					 ", i.ic_if as ic_if "+
					 " , ( SELECT cs_activo FROM comrel_pyme_epo_if_x_producto   WHERE ic_pyme = d.ic_pyme  and ic_epo = d.ic_epo  and ic_if = i.ic_if   AND ic_producto_nafin = 4  )  as PYME_BLOQUEADA " +  //F05-2014
					 " ,( select fN_VALOR_AFORO  From dis_aforo_epo  WHERE  IC_IF  = i.ic_if  and  ic_epo = d.ic_epo  and  ic_moneda =  d.ic_moneda  ) as DESCUENTO_AFORO  "+  //F05-2014
					 " ,( select FN_POR_LINEA  from dis_sublimites  WHERE ic_if =  i.ic_if   and ic_epo =  d.ic_epo  and ic_pyme =  d.ic_pyme  and ic_moneda  =  d.ic_moneda )   as SUB_LIMITE  " +//F05-2014
					 " ,  nvl(lc.FN_MONTO_AUTORIZADO,0)  as MONTO_LINEA  " +  // F05-2014

					 " ,pi.CG_LINEA_CREDITO AS CG_LINEA_CREDITO  "+
					 " ,to_char(lc.DF_VENCIMIENTO_ADICIONAL,'dd/mm/yyyy') as FECHA_VENC_LINEA  "+
					
					"  FROM DIS_DOCUMENTO D"   +
					"      ,COMCAT_PYME PYM "   +
					"      ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"      ,COMREL_PRODUCTO_EPO PE"   +
					"		, DIS_LINEA_CREDITO_DM lc 	"+
					"      ,COMCAT_EPO E"   +
					"      ,COM_TIPO_CAMBIO TC"   +
					"      ,COMCAT_IF I"   +
					"      ,COMCAT_MONEDA M"   +
					"	     ,COMCAT_MONEDA M2"+
					"      ,COMCAT_ESTATUS_DOCTO ED"   +
					"      ,COMCAT_PRODUCTO_NAFIN PN"   +
					"      ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"      ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"		 ,COMREL_PYME_EPO_X_PRODUCTO PEP"		+
					" , comrel_producto_if pi  "+
					" WHERE E.IC_EPO = PE.IC_EPO"   +
					"			 AND PYM.IC_PYME = PEP.IC_PYME"		+
					"			 AND E.IC_EPO = PEP.IC_EPO"		+
				  "	  	 AND PEP.ic_producto_nafin = 4 "+
					"      AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"      AND PYM.IC_PYME = D.IC_PYME"   +
					"      AND D.IC_EPO = PE.IC_EPO"   +
					"      AND D.IC_MONEDA = M.IC_MONEDA"   +
					"      AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"	     AND M2.IC_MONEDA = LC.IC_MONEDA"	+
					"      AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"      AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"      AND LC.IC_IF = I.IC_IF"   +
					"      AND LC.IC_EPO = E.IC_EPO"   +
					"		 AND LC.ic_linea_credito_dm = D.IC_LINEA_CREDITO_dm "+
					"      AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
					"      AND D.ic_estatus_docto = 2"   +
					"      AND IEP.IC_IF = I.IC_IF"   +
					"      AND IEP.IC_EPO = E.IC_EPO"   +
					"      AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"      AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" 	   AND ("   +
					" 	 	 LC.IC_MONEDA = D.IC_MONEDA"   +
					" 		 OR "   +
					" 		 LC.IC_MONEDA = 1"   +
					" 	 	 )"  +
					"      AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.CG_VENTACARTERA = 'S' "   +
					"      AND D.IC_EPO = "+ic_epo+
          			condicion+
			  " AND  pi.ic_if = lc.ic_if  "+
				" AND pi.ic_producto_nafin = 4  "+  
          "  ORDER BY D.IG_NUMERO_DOCTO"  ;


          log.debug("qrySentencia:::  "+qrySentencia);

          rs = con.queryDB(qrySentencia);
          while(rs.next()){
          vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("PYME")==null)?"":rs.getString("PYME"));
/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*10*/		vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*11*/		vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
/*12*/		vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/		vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*14*/		vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/		vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*16*/		vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
/*17*/		vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
/*18*/		vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
/*19*/		vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*20*/		vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*21*/		vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*22*/		vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*23*/		vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*24*/		vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*25*/		vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
/*26*/		vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
/*27*/		vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
/*28*/		vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
/*29*/		vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*30*/		vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
/*31*/		vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*32*/		vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*33*/		vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
/*34*/		vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
/*35*/		vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*36*/		vecColumnas.add((rs.getString("DIAS_MINIMO")==null)?"0":rs.getString("DIAS_MINIMO"));
/*37*/		vecColumnas.add((rs.getString("DIAS_MAXIMO")==null)?"0":rs.getString("DIAS_MAXIMO"));
/*38*/		vecColumnas.add((rs.getString("LINEA_CREDITO")==null)?"0":rs.getString("LINEA_CREDITO"));
/*39*/		vecColumnas.add((rs.getString("IC_IF")==null)?"0":rs.getString("IC_IF"));
/*40*/		vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"0":rs.getString("NOMBRE_MLINEA"));
/*41*/		vecColumnas.add((rs.getString("NOMBREPYME")==null)?"0":rs.getString("NOMBREPYME"));
/*42*/		vecColumnas.add((rs.getString("PLAZO_VALIDO")==null)?"0":rs.getString("PLAZO_VALIDO"));
/*43*/		vecColumnas.add((rs.getString("PLAZO_PYME")==null)?"0":rs.getString("PLAZO_PYME"));
/*44*/		vecColumnas.add((rs.getString("plazofinanciamiento")==null)?"0":rs.getString("plazofinanciamiento"));
/*45*/		vecColumnas.add((rs.getString("interes")==null)?"0":rs.getString("interes"));
/*46*/		vecColumnas.add((rs.getString("credito")==null)?"0":rs.getString("credito"));
/*47*/		vecColumnas.add((rs.getString("fechaVencimiento")==null)?"0":rs.getString("fechaVencimiento"));
/*48*/		vecColumnas.add((rs.getString("ic_epo")==null)?"0":rs.getString("ic_epo"));
/*49*/		vecColumnas.add((rs.getString("plazopyme")==null)?"0":rs.getString("plazopyme"));
/*50*/		vecColumnas.add((rs.getString("plazoepo")==null)?"0":rs.getString("plazoepo"));
/*51*/		vecColumnas.add((rs.getString("plazonafin")==null)?"0":rs.getString("plazonafin"));
/*52*/		vecColumnas.add((rs.getString("PYME_BLOQUEADA")==null)?"N":rs.getString("PYME_BLOQUEADA"));  //F05-2014
/*53*/		vecColumnas.add((rs.getString("DESCUENTO_AFORO")==null)?"0":rs.getString("DESCUENTO_AFORO"));  //F05-2014
/*54*/		vecColumnas.add((rs.getString("SUB_LIMITE")==null)?"100":rs.getString("SUB_LIMITE"));  //F05-2014
/*55*/		vecColumnas.add((rs.getString("MONTO_LINEA")==null)?"0":rs.getString("MONTO_LINEA"));  //F05-2014
/*56*/		vecColumnas.add((rs.getString("CG_LINEA_CREDITO")==null)?"N":rs.getString("CG_LINEA_CREDITO"));  //F20-2015
/*57*/		vecColumnas.add((rs.getString("FECHA_VENC_LINEA")==null)?"":rs.getString("FECHA_VENC_LINEA"));  //F20-2015
          vecFilas.add(vecColumnas);

      }

      con.cierraStatement();

        }catch(Exception e){
		log.error("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                  con.terminaTransaccion(ok);
                 	con.cierraConexionDB();
                }
		log.info(":consultaDoctosVentaCartera (S)");
        }
   return vecFilas;
   }

/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param in
	 * @param ic_linea_credito
	 * @param ic_epo
	 */
  public Vector consDoctosVentaCartera(String ic_epo ,String ic_linea_credito ,String in)  throws NafinException{

	log.info("consDoctosVentaCartera (E)");

				AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas = new Vector();
        Vector	vecColumnas = null;
        String 	condicion = "";
        boolean ok = true;

				try{
        	con = new AccesoDB();
          con.conexionDB();

					/*se establece zona mexico*/
					String strAlterMex = " alter session set NLS_TERRITORY = 'MEXICO' ";
					con.ejecutaUpdateDB(strAlterMex);
					/*fin */


          if(!"".equals(ic_linea_credito)&&ic_linea_credito!=null)
						condicion += " and LC.ic_linea_credito = "+ic_linea_credito;
          if(!"".equals(in)&&in!=null)
						condicion += " and D.ic_documento in("+in+")";

		/*	ESTE QUERY LLEVA RESTRICCI�N DE LA TASA CON EL PLAZO, ESO SE HAR� CON PROGRAMACION, POR ESO SE COMENT�, Y SE DEJ� COMO APARECE ABAJO	JRFH*/

		qrySentencia =
					"    SELECT /*+index(d IN_DIS_DOCUMENTO_02_NUK)*/ "+
					"		distinct PYM.CG_RAZON_SOCIAL AS PYME"   +
					"      ,D.IG_NUMERO_DOCTO"   +
					"      ,D.CC_ACUSE"   +
					"      ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION"   +
					"      ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC"   +
					"      ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION"   +
					"      ,D.IG_PLAZO_DOCTO AS PLAZO"   +
					"      ,M.CD_NOMBRE AS MONEDA"   +
					"      ,D.FN_MONTO AS MONTO"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,''),'') as TIPO_CAMBIO"   +
					"      ,'' as MONTO_VALUADO"   +
					"      ,D.ig_plazo_descuento"   +
					"      ,D.fn_porc_descuento"   +
					"      ,TF.cd_descripcion as MODO_PLAZO"   +
					"      ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"      ,'' AS CAMBIOS"   +
					"      ,D.IC_DOCUMENTO AS NUMERO_CREDITO"   +
					"      ,I.CG_RAZON_SOCIAL AS IF"   +
					"      ,DECODE(LC.CG_TIPO_SOLICITUD,'I','Inicial','A','Ampliacion','R','Renovacion','') as TIPO_SOLICITUD"   +
					"      ,'' AS FECHA_OPERACION"   +
					"      ,'' AS MONTO_CREDITO"   +
					"      ,DECODE(D.ic_tipo_financiamiento,1,trunc(d.df_fecha_venc)-trunc(sysdate),D.IG_PLAZO_CREDITO) as IG_PLAZO_CREDITO"   +
					"      ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
					"      ,'' AS REFERENCIA_INT"   +
					"      ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
					"      , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES"   +
					"	     ,decode(TF.ic_tipo_financiamiento,2,0,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100)) as MONTO_DESCUENTO"   +
					"      ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
					"      ,'' AS VALOR_TASA"   +
					"      ,'' AS REL_MAT"   +
					"      ,'' AS PUNTOS"   +
					"      ,I.IG_TIPO_PISO"   +
					"      ,TCI.ic_tipo_cobro_interes"   +
					"      ,M.IC_MONEDA"   +
					"      ,'' as IC_TASA"   +
					"      ,TF.ic_tipo_financiamiento"   +
					"      ,LC.ic_moneda as MONEDA_LINEA"   +
					"      ,nvl(pe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO"   +
					"      ,nvl(pe.ig_dias_maximo,pn.in_dias_maximo) as DIAS_MAXIMO"   +
					"      ,LC.ic_linea_credito_dm as LINEA_CREDITO"   +
					" 	   , I.ic_if"   +
					"	     , M2.cd_nombre as NOMBRE_MLINEA"+
					"	     , PYM.CG_RAZON_SOCIAL AS NOMBREPYME "+
          "      , e.ic_epo as ic_epo "+
          "      , TO_CHAR (SIGFECHAHABIL(d.df_fecha_venc+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-'), 'DD/MM/YYYY') AS fechaVencimiento "+
          "      ,pep.ig_plazo_pyme as plazopyme, pe.ig_dias_maximo as plazoepo, pn.in_dias_maximo as plazonafin "+
          "		   , (select (SIGFECHAHABIL(d.df_fecha_venc+cpe.ig_plazo_pyme,0,'-')-d.df_fecha_venc) "+
					"		   from comrel_pyme_epo_x_producto cpe "+
					"		   where pym.ic_pyme = cpe.ic_pyme"+
					"		   and e.ic_epo = cpe.ic_epo"+
					"		   and cpe.ic_producto_nafin=4) AS PLAZO_VALIDO"+
					"		   ,PEP.ig_plazo_pyme AS PLAZO_PYME"		+
          "      ,tf.ic_tipo_financiamiento as plazofinanciamiento, "+
          "      tci.IC_TIPO_COBRO_INTERES  as interes,"+
          "      pe.CG_TIPOS_CREDITO as credito "+
					" FROM DIS_DOCUMENTO D"   +
					"      ,COMCAT_PYME PYM "   +
					"      ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"      ,COMREL_PRODUCTO_EPO PE"   +
				//	"      ,COM_LINEA_CREDITO LC"   +
				 "       ,dis_linea_credito_dm  lc"+
					"      ,COMCAT_EPO E"   +
					"      ,COM_TIPO_CAMBIO TC"   +
					"      ,COMCAT_IF I"   +
					"      ,COMCAT_MONEDA M"   +
					"	     ,COMCAT_MONEDA M2"+
					"      ,COMCAT_ESTATUS_DOCTO ED"   +
					"      ,COMCAT_PRODUCTO_NAFIN PN"   +
					"      ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"      ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"			 ,COMREL_PYME_EPO_X_PRODUCTO PEP"		+
        	"  WHERE E.IC_EPO = PE.IC_EPO"   +
					"			 AND PYM.IC_PYME = PEP.IC_PYME"		+
					"			 AND E.IC_EPO = PEP.IC_EPO"		+
			    " 		 AND PEP.ic_producto_nafin = 4 "+
					"      AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"      AND PYM.IC_PYME = D.IC_PYME"   +
					"      AND D.IC_EPO = PE.IC_EPO"   +
					"      AND D.IC_MONEDA = M.IC_MONEDA"   +
					"      AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"	     AND M2.IC_MONEDA = LC.IC_MONEDA"	+
					"      AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"      AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"      AND LC.IC_IF = I.IC_IF"   +
					"      AND LC.IC_EPO = E.IC_EPO"   +
					//"      AND LC.IC_PYME = D.IC_PYME"   +
					"      AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
					"      AND D.ic_estatus_docto  = 2"   +
					"      AND IEP.IC_IF = I.IC_IF"   +
					"      AND IEP.IC_EPO = E.IC_EPO"   +
					"      AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"      AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" 	   AND ("   +
					" 	 	 LC.IC_MONEDA = D.IC_MONEDA"   +
					" 		 OR "   +
					" 		 LC.IC_MONEDA = 1"   +
					" 	 	 )"  +
					"      AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.IC_EPO = "+ic_epo+
          			condicion+
          "  ORDER BY D.IG_NUMERO_DOCTO"  ;


          log.debug("qrySentencia:::  "+qrySentencia);

          rs = con.queryDB(qrySentencia);

          while(rs.next()){
                	vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("PYME")==null)?"":rs.getString("PYME"));
/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*10*/		vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*11*/		vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
/*12*/		vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/		vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*14*/		vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/		vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*16*/		vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
/*17*/		vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
/*18*/		vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
/*19*/		vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*20*/		vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*21*/		vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*22*/		vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*23*/		vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*24*/		vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*25*/		vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
/*26*/		vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
/*27*/		vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
/*28*/		vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
/*29*/		vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*30*/		vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
/*31*/		vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*32*/		vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*33*/		vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
/*34*/		vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
/*35*/		vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*36*/		vecColumnas.add((rs.getString("DIAS_MINIMO")==null)?"0":rs.getString("DIAS_MINIMO"));
/*37*/		vecColumnas.add((rs.getString("DIAS_MAXIMO")==null)?"0":rs.getString("DIAS_MAXIMO"));
/*38*/		vecColumnas.add((rs.getString("LINEA_CREDITO")==null)?"0":rs.getString("LINEA_CREDITO"));
/*39*/		vecColumnas.add((rs.getString("IC_IF")==null)?"0":rs.getString("IC_IF"));
/*40*/		vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"0":rs.getString("NOMBRE_MLINEA"));
/*41*/		vecColumnas.add((rs.getString("NOMBREPYME")==null)?"0":rs.getString("NOMBREPYME"));
/*42*/		vecColumnas.add((rs.getString("PLAZO_VALIDO")==null)?"0":rs.getString("PLAZO_VALIDO"));
/*43*/		vecColumnas.add((rs.getString("PLAZO_PYME")==null)?"0":rs.getString("PLAZO_PYME"));
/*44*/		vecColumnas.add((rs.getString("plazofinanciamiento")==null)?"0":rs.getString("plazofinanciamiento"));
/*45*/		vecColumnas.add((rs.getString("interes")==null)?"0":rs.getString("interes"));
/*46*/		vecColumnas.add((rs.getString("credito")==null)?"0":rs.getString("credito"));
/*47*/		vecColumnas.add((rs.getString("fechaVencimiento")==null)?"0":rs.getString("fechaVencimiento"));
/*48*/		vecColumnas.add((rs.getString("ic_epo")==null)?"0":rs.getString("ic_epo"));
/*49*/		vecColumnas.add((rs.getString("plazopyme")==null)?"0":rs.getString("plazopyme"));
/*50*/		vecColumnas.add((rs.getString("plazoepo")==null)?"0":rs.getString("plazoepo"));
/*51*/		vecColumnas.add((rs.getString("plazonafin")==null)?"0":rs.getString("plazonafin"));
           vecFilas.add(vecColumnas);

        }
				con.cierraStatement();

        }catch(Exception e){
		log.error("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                  con.terminaTransaccion(ok);
                 	con.cierraConexionDB();
                }
		log.info("consDoctosVentaCartera (S)");
        }
   return vecFilas;
   }

/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @param esSistema
	 * @param esHost
	 * @param esIP
	 * @param esPassword
	 * @param esLogin
	 * @param esClavePyme
	 */
	public void validarUsuarioEPO(String esClavePyme, String esLogin, String esPassword,
		String esIP, String esHost, String esSistema )
        throws NafinException {

		log.info("validarUsuarioEPO  (E)");

		String lsQrySentencia = null;
		AccesoDB conexion = new AccesoDB();
		try {
			com.netro.seguridadbean.Seguridad objSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
			UtilUsr utils = new UtilUsr();
			boolean usaurioOK = utils.esUsuarioValido(esLogin, esPassword);

			if(!usaurioOK)
				throw new NafinException("DSCT0009");
			Usuario usuario = utils.getUsuario(esLogin);
			String perfil = usuario.getPerfil();

			try{
				objSeguridad.validaFacultad( (String) esLogin, (String) perfil, "PEPODCAPTA" , "29SVenCartera" , (String) esSistema, (String) esIP, (String) esHost );
			}catch( SeguException ErrorSeg){
				log.error("ERROR: validarUsuarioEPO(" + esLogin + "," +
						perfil + ",\"PEPODCAPTA\",\"29SVenCartera\"," +
						esSistema + "," +  esIP + "," + esHost+")");
				ErrorSeg.printStackTrace();
				throw new NafinException("DSCT0014");

			}


		}catch( SeguException ErrorSeg){
			ErrorSeg.printStackTrace();
				throw new NafinException("DSCT0009");
		}catch (NafinException ne) {
			ne.printStackTrace();
			throw ne;
		}catch (Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (conexion.hayConexionAbierta() == true) {
				conexion.cierraConexionDB();
			}
			log.info("validarUsuarioEPO (S)");
		}
	}
/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param cc_acuse
	 */
  public Vector consDoctosVCartera(String cc_acuse)   throws NafinException{

	log.info("consDoctosVCartera (E)");

   	AccesoDB con = null;
    ResultSet rs = null;
    String qrySentencia = "";
    Vector	vecFilas = new Vector();
    Vector	vecColumnas = null;

   	try{
        	con = new AccesoDB();
                con.conexionDB();

                qrySentencia =   "    SELECT E.CG_RAZON_SOCIAL AS EPO"   +
					"      ,D.IG_NUMERO_DOCTO"   +
					"      ,D.CC_ACUSE"   +
					"      ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION"   +
					"      ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC"   +
					"      ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION"   +
					"      ,D.IG_PLAZO_DOCTO AS PLAZO"   +
					"      ,M.CD_NOMBRE AS MONEDA"   +
					"      ,D.FN_MONTO AS MONTO"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,''),'') as TIPO_CAMBIO"   +
					"      ,'' as MONTO_VALUADO"   +
					"      ,D.ig_plazo_descuento"   +
					"      ,D.fn_porc_descuento"   +
					"      ,TF.cd_descripcion as MODO_PLAZO"   +
					"      ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"      ,'' AS CAMBIOS"   +
					"      ,D.IC_DOCUMENTO AS NUMERO_CREDITO"   +
					"      ,I.CG_RAZON_SOCIAL AS IF"   +
					"      ,DECODE(LC.CG_TIPO_SOLICITUD,'I','Inicial','A','Ampliacion','R','Renovacion','') as TIPO_SOLICITUD"   +
					"      ,'' AS FECHA_OPERACION"   +
					"      ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"      ,D.IG_PLAZO_CREDITO"   +
					"      ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
					"      ,CT.CD_NOMBRE AS REFERENCIA_INT"   +
					"      ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
					"      , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES"   +
					"      ,decode(TF.ic_tipo_financiamiento,2,0,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100)) as MONTO_DESCUENTO"   +
					"      ,ds.FN_VALOR_TASA AS VALOR_TASA"   +
					"      ,ds.CG_REL_MAT AS REL_MAT"   +
					"      ,ds.FN_PUNTOS AS PUNTOS"   +
					"      ,I.IG_TIPO_PISO"   +
					"      ,TCI.ic_tipo_cobro_interes"   +
					"      ,M.IC_MONEDA"   +
					"      ,CT.ic_tasa"   +
					"      ,TF.ic_tipo_financiamiento"   +
					"      ,LC.ic_moneda as MONEDA_LINEA"   +
					"      ,nvl(pe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO"   +
					"      ,nvl(pe.ig_dias_maximo,pn.in_dias_maximo) as DIAS_MAXIMO"   +
					"      ,LC.ic_linea_credito_dm as LINEA_CREDITO"   +
					"      ,DS.fn_importe_interes as MONTO_INTERES"   +
					"      , to_char(ca.df_fecha_hora,'dd/mm/yyyy') as fecha_carga"   +
					"      , to_char(ca.df_fecha_hora,'hh:mi') as hora_carga"   +
					"      , ca.ic_usuario as usuario"   +
					"	     , M2.cd_nombre as NOMBRE_MLINEA"+
					"	     , PYM.CG_RAZON_SOCIAL AS NOMBREPYME"+
					"     ,( select fN_VALOR_AFORO  From dis_aforo_epo  where  IC_IF  = i.ic_if  and  ic_epo = d.ic_epo  and  ic_moneda =  d.ic_moneda  ) as DESCUENTO_AFORO  "+  //F05-2014

					" FROM DIS_DOCUMENTO D"   +
					"      ,DIS_DOCTO_SELECCIONADO DS"   +
					"      ,COMCAT_PYME PYM"   +
					"      ,COMCAT_EPO E"   +
					"      ,COMREL_PRODUCTO_EPO PE"   +
					"      ,COMCAT_PRODUCTO_NAFIN PN"   +
					"      ,COMCAT_MONEDA M"   +
					"      ,COM_TIPO_CAMBIO TC"   +
					"      ,COMCAT_ESTATUS_DOCTO ED"   +
					"      ,DIS_LINEA_CREDITO_DM LC"   +
					"      ,COMCAT_IF I"   +
					"      ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"      ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"      ,COMCAT_TASA CT"   +
					"      ,COMCAT_PLAZO CP "   +
					"      ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"      ,COM_ACUSE2 CA"   +
					"	     ,COMCAT_MONEDA M2"+
					" WHERE E.IC_EPO = PE.IC_EPO"   +
					"      AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
					"      AND PYM.IC_PYME = D.IC_PYME"   +
					"      AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"      AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.IC_EPO = PE.IC_EPO"   +
					"      AND D.IC_MONEDA = M.IC_MONEDA"   +
					"      AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"	     AND M2.IC_MONEDA = LC.IC_MONEDA"+
					"      AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"      AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"      AND LC.IC_IF = I.IC_IF"   +
					"      AND LC.IC_EPO = E.IC_EPO"   +
					"      AND LC.ic_linea_credito_dm = D.IC_LINEA_CREDITO_dm"   +
					"      AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
					"      AND IEP.IC_IF = I.IC_IF"   +
					"      AND IEP.IC_EPO = E.IC_EPO"   +
					"      AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"      AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"      AND CT.IC_PLAZO = CP.IC_PLAZO"   +
					//"      AND CT.IC_MONEDA = LC.IC_MONEDA"   +
					"      AND CT.CS_DISPONIBLE = 'S'"   +
					"      AND DS.IC_TASA = CT.IC_TASA"   +
					"      AND DS.CC_ACUSE = CA.CC_ACUSE"   +
					"     AND DS.CC_ACUSE = '"+cc_acuse+"'"+
					"  ORDER BY D.IG_NUMERO_DOCTO"  ;

        log.debug("qrySentencia:::   "+qrySentencia);

         rs = con.queryDB(qrySentencia);
        while(rs.next()){
         	vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*10*/		vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*11*/		vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
/*12*/		vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/		vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*14*/		vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/		vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*16*/		vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
/*17*/		vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
/*18*/		vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
/*19*/		vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*20*/		vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*21*/		vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*22*/		vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*23*/		vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*24*/		vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*25*/		vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
/*26*/		vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
/*27*/		vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
/*28*/		vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
/*29*/		vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*30*/		vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
/*31*/		vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*32*/		vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*33*/		vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
/*34*/		vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
/*35*/		vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*36*/		vecColumnas.add((rs.getString("DIAS_MINIMO")==null)?"0":rs.getString("DIAS_MINIMO"));
/*37*/		vecColumnas.add((rs.getString("DIAS_MAXIMO")==null)?"0":rs.getString("DIAS_MAXIMO"));
/*38*/		vecColumnas.add((rs.getString("LINEA_CREDITO")==null)?"0":rs.getString("LINEA_CREDITO"));
/*39*/		vecColumnas.add((rs.getString("MONTO_INTERES")==null)?"0":rs.getString("MONTO_INTERES"));
/*40*/		vecColumnas.add(acuseFormateado);
/*41*/		vecColumnas.add((rs.getString("FECHA_CARGA")==null)?"":rs.getString("FECHA_CARGA"));
/*42*/		vecColumnas.add((rs.getString("HORA_CARGA")==null)?"":rs.getString("HORA_CARGA"));
/*43*/		vecColumnas.add((rs.getString("USUARIO")==null)?"":rs.getString("USUARIO"));
/*44*/		vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"":rs.getString("NOMBRE_MLINEA"));
/*45*/		vecColumnas.add((rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME"));
/*46*/		vecColumnas.add((rs.getString("DESCUENTO_AFORO")==null)?"":rs.getString("DESCUENTO_AFORO"));


          vecFilas.add(vecColumnas);
        }
        con.cierraStatement();

        }catch(Exception e){
		log.error("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		log.info("consDoctosVCartera (S)");
        }
   return vecFilas;
   }

/*********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
**********************************************************************************************
*********************************************************************************************/
	public Vector monitorLineasdm(String ic_epo, String ic_if, String ic_moneda)
			throws NafinException{
		System.out.println("AceptacionPymeBean::monitorLineas (E)");
		AccesoDB			con				= null;
		ResultSet			rs				= null;
		PreparedStatement	ps				= null;
		String				qrySentencia	= "";
		String				condicion		= "";
		Vector				vecFilas 		= new Vector();
		Vector				vecColumnas 	= null;
		try {
			con = new AccesoDB();
			con.conexionDB();
			if(!"".equals(ic_if))
				condicion += " AND lcd.ic_if = ? ";
			if(!"".equals(ic_moneda))
				condicion += " AND lcd.ic_moneda = ? ";
			qrySentencia =
				" SELECT lcd.IC_LINEA_CREDITO_DM,"   +
				"        lcd.IC_LINEA_CREDITO_DM || ' - ' || i.cg_razon_social || ' ' || DECODE (ic_moneda, 1, 'M.N.', 54, 'USD') AS descripcion,"   +
				"        lcd.fn_monto_autorizado_total, lcd.FN_MONTO_AUTORIZADO_TOTAL, lcd.ic_moneda, lcd.fn_saldo_total"   +
				"   FROM dis_linea_credito_dm lcd, comcat_if i"   +
				"  WHERE lcd.ic_if = i.ic_if"   +
				"    AND lcd.df_vencimiento_adicional > SYSDATE"   +
				"    AND lcd.cg_tipo_solicitud = 'I'"   +
				"    AND lcd.ic_estatus_linea = 12"   +
				"    AND lcd.ic_producto_nafin = 4"   +
				"    AND lcd.ic_epo = ?"   +
				condicion+
				"  ORDER BY lcd.IC_LINEA_CREDITO_DM"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			int cont = 1;
			if(!"".equals(ic_if)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_if));
			}
			if(!"".equals(ic_moneda)) {
				cont++;ps.setInt(cont, Integer.parseInt(ic_moneda));
			}
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()){
				vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("IC_LINEA_CREDITO_DM")==null)?"":rs.getString("IC_LINEA_CREDITO_DM"));
/*1*/			vecColumnas.add((rs.getString("DESCRIPCION")==null)?"":rs.getString("DESCRIPCION"));
/*2*/			vecColumnas.add((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
/*3*/			vecColumnas.add((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
/*4*/			vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*5*/			vecColumnas.add((rs.getString("FN_SALDO_TOTAL")==null)?"":rs.getString("FN_SALDO_TOTAL"));

				vecFilas.add(vecColumnas);
			}
			rs.close();ps.close();
		} catch(Exception e){
			System.out.println("AceptacionPymeBean::monitorLineas Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("AceptacionPymeBean::monitorLineas (S)");
		}
		return vecFilas;
	}

	/**
	 * Obtiene resumen de publicacion por epo, para la pyme especificada
	 *
	 * @param clavePyme Clave de la pyme (ic_pyme)
	 * @return Objeto Registros con la informaci�n del resumen.
	 */
	public Registros getResumenPublicacionXEpo(String clavePyme) {
		log.info("getResumenPublicacionXEpo(E)");

		AccesoDB con = new AccesoDB();
		List params  = null;
		try {
			con.conexionDB();

			String strSQL =
					" SELECT epo.ic_epo,epo.cg_razon_social, COUNT (d.ic_epo) AS numDoctos,  " +
					" CASE WHEN COUNT (d.ic_epo) >0 THEN 'S' ELSE 'N' END AS conPublicacion, " +
					" COUNT (decode(d.ic_moneda, 1, d.ic_epo)) AS numDoctosMN, " +
					" COUNT (decode(d.ic_moneda, 54, d.ic_epo)) AS numDoctosUSD,   " +
					" NVL( SUM(CASE WHEN d.ic_moneda = 1 THEN fn_monto ELSE 0 END) , 0) AS montoMN, " +
					" NVL( SUM(CASE WHEN d.ic_moneda = 54 THEN fn_monto ELSE 0 END) , 0) AS montoUSD " +
					" FROM comrel_pyme_epo rpe, comcat_epo epo, dis_documento d " +
					" WHERE rpe.ic_epo = epo.ic_epo  " +
					" AND epo.cs_habilitado = ?  " +
					" AND rpe.ic_pyme = ? " +
					" AND rpe.ic_epo = d.ic_epo(+) " +
					" AND rpe.ic_pyme = d.ic_pyme(+) " +
					" AND d.ic_estatus_docto (+) = ? " +
					" AND rpe.cs_distribuidores in (?,?) " +
					" AND (d.ic_documento IS NULL OR (d.ic_documento IS NOT NULL AND d.ic_tipo_financiamiento IS NOT NULL) ) "+
					" GROUP BY epo.ic_epo,epo.cg_razon_social " +
					" ORDER BY conPublicacion DESC, epo.cg_razon_social ";

			params = new ArrayList();
			params.add("S");
			params.add(new Integer(clavePyme));
			params.add(new Integer(2));	//Negociable
			params.add("S");
			params.add("R");
			log.debug(strSQL);
			log.debug(params);
			Registros reg = con.consultarDB(strSQL, params);
			return reg;
		} catch (Exception e) {
			throw new AppException("Error al general el resumen.", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getResumenPublicacionXEpo(S)");
		}
	}
	/**
	 *Resumen de Publicaci�npara Factoraje con Recurso	 *
	 * @return
	 * @param clavePyme
	 */
	public Registros getResumenPublicacionXEpoFacRec(String clavePyme) {
		log.info("getResumenPublicacionXEpoFacRec(E)");

		AccesoDB con = new AccesoDB();
		List params  = null;
		try {
			con.conexionDB();

			String strSQL =
					" SELECT epo.ic_epo,epo.cg_razon_social, COUNT (d.ic_epo) AS numDoctos,  " +
					" CASE WHEN COUNT (d.ic_epo) >0 THEN 'S' ELSE 'N' END AS conPublicacion, " +
					" COUNT (decode(d.ic_moneda, 1, d.ic_epo)) AS numDoctosMN, " +
					" COUNT (decode(d.ic_moneda, 54, d.ic_epo)) AS numDoctosUSD,   " +
					" NVL( SUM(CASE WHEN d.ic_moneda = 1 THEN fn_monto ELSE 0 END) , 0) AS montoMN, " +
					" NVL( SUM(CASE WHEN d.ic_moneda = 54 THEN fn_monto ELSE 0 END) , 0) AS montoUSD " +
					" FROM comrel_pyme_epo rpe, comcat_epo epo, dis_documento d, comrel_producto_epo cpe  " +
					" WHERE rpe.ic_epo = epo.ic_epo  " +
					" AND epo.cs_habilitado = ?  " +
					" AND rpe.ic_pyme = ? " +
					" AND rpe.ic_epo = d.ic_epo(+) " +
					" AND rpe.ic_pyme = d.ic_pyme(+) " +
					" AND d.ic_estatus_docto (+) = ? " +
					" AND rpe.cs_distribuidores in (?,?) " +
					" and cpe.cg_tipos_credito  =? "+
					" and cpe.ic_epo = d.ic_epo "+
					" and cpe.ic_epo =rpe.ic_epo "+
					" and d.IC_TIPO_FINANCIAMIENTO is null "+
					" GROUP BY epo.ic_epo,epo.cg_razon_social " +
					" ORDER BY conPublicacion DESC, epo.cg_razon_social ";

			params = new ArrayList();
			params.add("S");
			params.add(new Integer(clavePyme));
			params.add(new Integer(2));	//Negociable
			params.add("S");
			params.add("R");
			params.add("F");

			log.debug(strSQL);
			log.debug(params);
			Registros reg = con.consultarDB(strSQL, params);
			return reg;
		} catch (Exception e) {
			throw new AppException("Error al general el resumen.", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getResumenPublicacionXEpo(S)");
		}
	}


	/**
	 * Fodea 019-2012
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param in
	 * @param ic_linea_credito
	 * @param fecha_publicacion_a
	 * @param fecha_publicacion_de
	 * @param fecha_vto_a
	 * @param fecha_vto_de
	 * @param fecha_emision_a
	 * @param fecha_emision_de
	 * @param cc_acuse
	 * @param fn_monto_a
	 * @param fn_monto_de
	 * @param ig_numero_docto
	 * @param ic_moneda
	 * @param ic_epo
	 * @param ic_pyme
	 */
	public Vector consultaDoctosFactRecurso(String ic_pyme, String ic_epo ,String ic_moneda  , String ig_numero_docto
        ,String fn_monto_de  ,String fn_monto_a   ,String cc_acuse  ,String fecha_emision_de
        ,String fecha_emision_a  ,String fecha_vto_de   ,String fecha_vto_a     ,String fecha_publicacion_de
        ,String fecha_publicacion_a  ,String ic_linea_credito  ,String in  )  throws NafinException{

		System.out.println("AceptacionPymeBean::consultaDoctosFactRecurso (E)");
   	AccesoDB con = null;
      ResultSet rs = null;
      String qrySentencia = "";
      Vector	vecFilas = new Vector();
      Vector	vecColumnas = null;
      String 	condicion = "";
      boolean ok = true;
   	try{
			con = new AccesoDB();
         con.conexionDB();
			/*se establece zona mexico*/
			String strAlterMex = " alter session set NLS_TERRITORY = 'MEXICO' ";
			con.ejecutaUpdateDB(strAlterMex);

			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion += " and D.ic_epo = "+ic_epo;
         if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion += " and D.ic_moneda = "+ic_moneda;
         if(!"".equals(ig_numero_docto)&&ig_numero_docto!=null)
				condicion += " and D.ig_numero_docto = '"+ig_numero_docto+"'";
         if(!"".equals(cc_acuse)&&cc_acuse!=null)
				condicion += " and D.cc_acuse = '"+cc_acuse+"'";
         if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null)
				condicion += " and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a;
         if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null)
				condicion += " and D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) between "+fn_monto_de+" and "+fn_monto_a;
			if(!"".equals(fecha_emision_de)&&fecha_emision_de!=null&&!"".equals(fecha_emision_a)&&fecha_emision_a!=null)
				condicion += " and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_vto_de)&&fecha_vto_de!=null&&!"".equals(fecha_vto_a)&&fecha_vto_a!=null)
				condicion += " and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_publicacion_de)&&fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&&fecha_publicacion_a!=null)
				condicion += " and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))";
         if(!"".equals(ic_linea_credito)&&ic_linea_credito!=null)
				condicion += " and LC.ic_linea_credito = "+ic_linea_credito;
         if(!"".equals(in)&&in!=null)
				condicion += " and D.ic_documento in("+in+")";
			/*	ESTE QUERY LLEVA RESTRICCI�N DE LA TASA CON EL PLAZO, ESO SE HAR� CON PROGRAMACION, POR ESO SE COMENT�, Y SE DEJ� COMO APARECE ABAJO	JRFH*/

			qrySentencia =
					"    SELECT /*+index(d IN_DIS_DOCUMENTO_02_NUK)*/ "+
					"		distinct E.CG_RAZON_SOCIAL AS EPO"   +
					"      ,D.IG_NUMERO_DOCTO"   +
					"      ,D.CC_ACUSE"   +
					"      ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION"   +
					"      ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC"   +
					"      ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION"   +
					"      ,D.IG_PLAZO_DOCTO AS PLAZO"   +
					"      ,M.CD_NOMBRE AS MONEDA"   +
					"      ,D.FN_MONTO AS MONTO"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,''),'') as TIPO_CAMBIO"   +
					"      ,'' as MONTO_VALUADO"   +
					"      ,D.ig_plazo_descuento"   +
					"      ,D.fn_porc_descuento"   +
					"      ,'' as MODO_PLAZO"   +
					"      ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"      ,'' AS CAMBIOS"   +
					"      ,D.IC_DOCUMENTO AS NUMERO_CREDITO"   +
					"      ,I.CG_RAZON_SOCIAL AS IF"   +
					"      ,DECODE(LC.CG_TIPO_SOLICITUD,'I','Inicial','A','Ampliacion','R','Renovacion','') as TIPO_SOLICITUD"   +
					"      ,'' AS FECHA_OPERACION"   +
					"      ,'' AS MONTO_CREDITO"   +
					"      ,DECODE(D.ic_tipo_financiamiento,1,trunc(d.df_fecha_venc)-trunc(sysdate),D.IG_PLAZO_CREDITO) as IG_PLAZO_CREDITO"   +
					"      ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
					"      ,'' AS REFERENCIA_INT"   +
					"      ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
					"      , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES"   +
					"      ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
					"      ,'' AS VALOR_TASA"   +
					"      ,'' AS REL_MAT"   +
					"      ,'' AS PUNTOS"   +
					"      ,I.IG_TIPO_PISO"   +
					"      ,TCI.ic_tipo_cobro_interes"   +
					"      ,M.IC_MONEDA"   +
					"      ,'' as IC_TASA"   +
					"      ,'' as ic_tipo_financiamiento"   +
					"      ,LC.ic_moneda as MONEDA_LINEA"   +
					"      ,nvl(pe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO"   +
					"      ,nvl(pe.ig_dias_maximo,pn.in_dias_maximo) as DIAS_MAXIMO"   +
					"      ,LC.ic_linea_credito as LINEA_CREDITO"   +
					" 	  , I.ic_if"   +
					"	  , M2.cd_nombre as NOMBRE_MLINEA"+
					"	  , PYM.CG_RAZON_SOCIAL AS NOMBREPYME "+
					"    , e.ic_epo as ic_epo "+
					"  , TO_CHAR (SIGFECHAHABIL(d.df_fecha_venc+NVL(pep.ig_plazo_pyme,NVL(pe.ig_dias_maximo,pn.in_dias_maximo)),0,'-'), 'DD/MM/YYYY') AS fechaVencimiento "+
					"   ,pep.ig_plazo_pyme as plazopyme, pe.ig_dias_maximo as plazoepo, pn.in_dias_maximo as plazonafin "+
					"		, (select (SIGFECHAHABIL(d.df_fecha_venc+cpe.ig_plazo_pyme,0,'-')-d.df_fecha_venc) "+//obtiene plazo valido
					"		 from comrel_pyme_epo_x_producto cpe "+
					"		 where pym.ic_pyme = cpe.ic_pyme"+
					"		 and e.ic_epo = cpe.ic_epo"+
					"		 and cpe.ic_producto_nafin=4) AS PLAZO_VALIDO"+//se obtiene plazo valido en caso de que el plazo real especificado sea un dia inhabil--(02/05/2008)
					"		, PEP.ig_plazo_pyme AS PLAZO_PYME"		+//se ontiene plazo real estblecido por IF--(02/05/2008)
					"    , '' as plazofinanciamiento, "+
					"    tci.IC_TIPO_COBRO_INTERES  as interes,"+
					"     pe.CG_TIPOS_CREDITO as credito "+
					" 		,lc.ig_cuenta_bancaria AS CG_NUMERO_CUENTA "+
					"    FROM DIS_DOCUMENTO D"   +
					"      ,COMCAT_PYME PYM "   +
					"      ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"      ,COMREL_PRODUCTO_EPO PE"   +
					"      ,COM_LINEA_CREDITO LC"   +
					"      ,COMCAT_EPO E"   +
					"      ,COM_TIPO_CAMBIO TC"   +
					"      ,COMCAT_IF I"   +
					"      ,COMCAT_MONEDA M"   +
					"	   ,COMCAT_MONEDA M2"+
					"      ,COMCAT_ESTATUS_DOCTO ED"   +
					"      ,COMCAT_PRODUCTO_NAFIN PN"   +
					"      ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"			 ,COMREL_PYME_EPO_X_PRODUCTO PEP"		+
					"    WHERE E.IC_EPO = PE.IC_EPO"   +
					"			 AND PYM.IC_PYME = PEP.IC_PYME"		+
					"			 AND E.IC_EPO = PEP.IC_EPO"		+
					" 			 AND PEP.ic_producto_nafin = 4 "+
					"      AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"      AND PYM.IC_PYME = D.IC_PYME"   +
					"      AND D.IC_EPO = PE.IC_EPO"   +
					"      AND D.IC_MONEDA = M.IC_MONEDA"   +
					"      AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"	   AND M2.IC_MONEDA = LC.IC_MONEDA"	+
					"      AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"      AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"      AND LC.IC_IF = I.IC_IF"   +
					"      AND LC.IC_EPO = E.IC_EPO"   +
					"      AND LC.IC_PYME = D.IC_PYME"   +
					"      AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
					"      AND D.ic_estatus_docto = 2"   +
					"      AND IEP.IC_IF = I.IC_IF"   +
					"      AND IEP.IC_EPO = E.IC_EPO"   +
					"      AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" 	   AND ("   +		//SI LA LINEA ES EN DOLARES, SOLO TRAE DOCUMENTOS EN DOLARES, PERO SI ES M.N. TRAE TODOS
					" 	 	 LC.IC_MONEDA = D.IC_MONEDA"   +
					" 		 OR "   +
					" 		 LC.IC_MONEDA = 1"   +
					" 	 	 )"  +
					"      AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.IC_PRODUCTO_NAFIN = 4"   +
					"      AND D.CG_VENTACARTERA is null "+
					"		 AND  d.IC_TIPO_FINANCIAMIENTO is null  "+
					"		 AND lc.cg_tipo_solicitud = 'I' "+
					"		 AND lc.cs_factoraje_con_rec = 'S' "+
					"      AND D.IC_PYME = "+ic_pyme+
          			condicion+
          "  ORDER BY D.IG_NUMERO_DOCTO"  ;


				System.out.println("qrySentencia:::  "+qrySentencia);
				rs = con.queryDB(qrySentencia);
            while(rs.next()){
					vecColumnas = new Vector();
	/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
	/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
	/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
	/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
	/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
	/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
	/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
	/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
	/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
	/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
	/*10*/		vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
	/*11*/		vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
	/*12*/		vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
	/*13*/		vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
	/*14*/		vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
	/*15*/		vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
	/*16*/		vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
	/*17*/		vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
	/*18*/		vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
	/*19*/		vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
	/*20*/		vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
	/*21*/		vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
	/*22*/		vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
	/*23*/		vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
	/*24*/		vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
	/*25*/		vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
	/*26*/		vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
	/*27*/		vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
	/*28*/		vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
	/*29*/		vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
	/*30*/		vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
	/*31*/		vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
	/*32*/		vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
	/*33*/		vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
	/*34*/		vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
	/*35*/		vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
	/*36*/		vecColumnas.add((rs.getString("DIAS_MINIMO")==null)?"0":rs.getString("DIAS_MINIMO"));
	/*37*/		vecColumnas.add((rs.getString("DIAS_MAXIMO")==null)?"0":rs.getString("DIAS_MAXIMO"));
	/*38*/		vecColumnas.add((rs.getString("LINEA_CREDITO")==null)?"0":rs.getString("LINEA_CREDITO"));
	/*39*/		vecColumnas.add((rs.getString("IC_IF")==null)?"0":rs.getString("IC_IF"));
	/*40*/		vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"0":rs.getString("NOMBRE_MLINEA"));
	/*41*/		vecColumnas.add((rs.getString("NOMBREPYME")==null)?"0":rs.getString("NOMBREPYME"));
	/*42*/		vecColumnas.add((rs.getString("PLAZO_VALIDO")==null)?"0":rs.getString("PLAZO_VALIDO"));
	/*43*/		vecColumnas.add((rs.getString("PLAZO_PYME")==null)?"0":rs.getString("PLAZO_PYME"));
	/*44*/		vecColumnas.add((rs.getString("plazofinanciamiento")==null)?"0":rs.getString("plazofinanciamiento"));
	/*45*/		vecColumnas.add((rs.getString("interes")==null)?"0":rs.getString("interes"));
	/*46*/		vecColumnas.add((rs.getString("credito")==null)?"0":rs.getString("credito"));
	/*47*/		vecColumnas.add((rs.getString("fechaVencimiento")==null)?"0":rs.getString("fechaVencimiento"));
	/*48*/		vecColumnas.add((rs.getString("ic_epo")==null)?"0":rs.getString("ic_epo"));
	/*49*/		vecColumnas.add((rs.getString("plazopyme")==null)?"0":rs.getString("plazopyme"));
	/*50*/		vecColumnas.add((rs.getString("plazoepo")==null)?"0":rs.getString("plazoepo"));
	/*51*/		vecColumnas.add((rs.getString("plazonafin")==null)?"0":rs.getString("plazonafin"));
	/*52*/		vecColumnas.add((rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA"));

					vecFilas.add(vecColumnas);
            }

			con.cierraStatement();
		}catch(Exception e){
			System.out.println("Excepcion "+e);
         throw new NafinException("SIST0001");
      }finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
            con.cierraConexionDB();
         }
			System.out.println("AceptacionPymeBean::consultaDoctosCCC (S)");
      }
		return vecFilas;
   }

	/**
	 * confirmacion de seleccion de  documento con el tipo de credito Factoraje con recurso
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_linea_credito
	 * @param fecha_vto
	 * @param plazo_credito
	 * @param fn_importe_recibir
	 * @param fn_importe_interes
	 * @param fn_valor_tasa
	 * @param fn_puntos
	 * @param cg_rel_mat
	 * @param ic_tasa
	 * @param ic_documento
	 * @param cgReciboElectronico
	 * @param icUsuario
	 * @param totalMontoImpUSD
	 * @param totalInteresUSD
	 * @param totalMontoUSD
	 * @param totalMontoImpMN
	 * @param totalInteresMN
	 * @param totalMontoMN
	 */
	public	String confirmaDoctoFactoRec(
		String totalMontoMN,
		String totalInteresMN,
		String totalMontoImpMN,
		String totalMontoUSD,
		String totalInteresUSD,
		String totalMontoImpUSD,
		String icUsuario,
		String cgReciboElectronico,
		String ic_documento[],
		String ic_tasa[],
		String cg_rel_mat[],
		String fn_puntos[],
		String fn_valor_tasa[],
		String fn_importe_interes[],
		String fn_importe_recibir[],
		String plazo_credito[],
		String fecha_vto[],
		String ic_linea_credito[]	) throws NafinException{
		log.info("confirmaDoctoFactoRec (E)");

		AccesoDB con = null;
		String qrySentencia = "";
		Acuse  	acuse = null;
		ResultSet rs = null;
		int   aux = 0;
		boolean ok = true;

		try{
        	con = new AccesoDB();

			con.conexionDB();

			acuse = new Acuse(2,"4","com",con);
			acuseFormateado  = acuse.formatear();
			insertaAcuse(acuse.toString(),totalMontoMN,totalInteresMN,totalMontoImpMN,totalMontoUSD,totalInteresUSD,totalMontoImpUSD,icUsuario,cgReciboElectronico,con);

			for(int i=0;i<ic_documento.length;i++){
				aux = 0;
				qrySentencia =	" select count(*) from dis_docto_seleccionado where ic_documento = "+ic_documento[i];
				rs = con.queryDB(qrySentencia);
				if(rs.next()) {
					aux = rs.getInt(1);
				}
				con.cierraStatement();

				if(aux==0) {
					qrySentencia =	" insert into dis_docto_seleccionado"+
										" (ic_documento,cc_acuse,ic_tasa,cg_rel_mat"+
										" ,fn_puntos,fn_valor_tasa,fn_importe_interes"+
										" ,fn_importe_recibir,df_fecha_seleccion)"+
										" values("+ic_documento[i]+",'"+acuse.toString()+"',"+ic_tasa[i]+",'"+cg_rel_mat[i]+"'"+
										" ,"+fn_puntos[i]+","+fn_valor_tasa[i]+","+fn_importe_interes[i]+","+fn_importe_recibir[i]+
										" ,sysdate)";
				} else {
					qrySentencia =	" update dis_docto_seleccionado"+
						" set cc_acuse = '"+acuse.toString()+"'"+
						" ,ic_tasa = "+ic_tasa[i]+
						" ,cg_rel_mat = '"+cg_rel_mat[i]+"'"+
						" ,fn_puntos = "+fn_puntos[i]+
						" ,fn_valor_tasa = "+fn_valor_tasa[i]+
						" ,fn_importe_interes = "+fn_importe_interes[i]+
						" ,fn_importe_recibir = "+fn_importe_recibir[i]+
						" ,df_fecha_seleccion = sysdate"+
						" where ic_documento = "+ic_documento[i];
				}
				con.ejecutaSQL(qrySentencia);

				System.out.println("\n qrySentencia: "+qrySentencia);
				qrySentencia =
									"update dis_documento set "+
									" (ic_estatus_docto,ig_plazo_credito,df_fecha_venc_credito,fn_tipo_cambio,ic_linea_credito) = "+
									" ( select 3,"+plazo_credito[i]+",to_date('"+fecha_vto[i]+"','dd/mm/yyyy')"+
									" ,decode(ic_moneda,1,1,fn_valor_compra),"+ic_linea_credito[i]+" from com_tipo_cambio tc where ic_moneda =(select ic_moneda from dis_documento where ic_documento ="+ic_documento[i]+")"+
									" and dc_fecha = (select max(dc_fecha) from com_tipo_cambio where ic_moneda = tc.ic_moneda)"+
									") where ic_documento = "+ic_documento[i];

				System.out.println("\n qrySentencia: "+qrySentencia);
				con.ejecutaSQL(qrySentencia);
			}

        }catch(Exception e){
        	ok = false;
			System.out.println("Excepcion "+e);
        }finally{
				if(con.hayConexionAbierta()){
					con.terminaTransaccion(ok);
               con.cierraConexionDB();
            }
			log.info("confirmaDoctoFactoRec (S)");
        }
		return acuse.toString();
	}

	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param cc_acuse
	 */
	public Vector consultaDoctosFactoRec(String cc_acuse)  throws NafinException{
		log.info("consultaDoctosFactoRec (E)");
		AccesoDB con = null;
      ResultSet rs = null;
      String qrySentencia = "";
      Vector vecFilas = new Vector();
      Vector vecColumnas = null;

   	try{

			con = new AccesoDB();
         con.conexionDB();
			qrySentencia = "  SELECT distinct E.CG_RAZON_SOCIAL AS EPO"   +
                             "    ,D.IG_NUMERO_DOCTO"   +
                             "    ,DS.CC_ACUSE"   +
                             "    ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION"   +
                             "    ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC"   +
                             "    ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION"   +
                             "    ,D.IG_PLAZO_DOCTO AS PLAZO"   +
                             "    ,M.CD_NOMBRE AS MONEDA"   +
                             "    ,D.FN_MONTO AS MONTO"   +
                             "    ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                             "    ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,''),'') as TIPO_CAMBIO"   +
                             "    ,'' as MONTO_VALUADO"   +
                             "    ,D.ig_plazo_descuento"   +
									 "     ,D.fn_porc_descuento as fn_porc_descuento"   +
                             "    ,'' as MODO_PLAZO"   +
                             "    ,ED.CD_DESCRIPCION AS ESTATUS"   +
                             "    ,'' AS CAMBIOS"   +
                             "    ,D.IC_DOCUMENTO AS NUMERO_CREDITO"   +
                             "    ,I.CG_RAZON_SOCIAL AS IF"   +
                             "    ,DECODE(LC.CG_TIPO_SOLICITUD,'I','Inicial','A','Ampliacion','R','Renovacion') as TIPO_SOLICITUD"   +
                             "    ,'' AS FECHA_OPERACION"   +
                             "    ,'' AS MONTO_CREDITO"   +
                             "    ,D.IG_PLAZO_CREDITO"   +
                             "    ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
                             "    ,CT.CD_NOMBRE AS REFERENCIA_INT"   +
                             "    ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
                             "    , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES"   +
                             "    ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
                             "    ,CMT.FN_VALOR AS VALOR_TASA"   +
                             "    ,DS.CG_REL_MAT AS REL_MAT"   +
                             "    ,DS.FN_PUNTOS AS PUNTOS"   +
                             "    ,I.IG_TIPO_PISO"   +
                             "    ,TCI.ic_tipo_cobro_interes"   +
                             "    ,M.IC_MONEDA"   +
                             "    ,CT.ic_tasa"   +
                             "  , to_char(ca.df_fecha_hora,'dd/mm/yyyy') as fecha_carga"   +
                             "  , to_char(ca.df_fecha_hora,'hh:mi') as hora_carga"   +
                             "  , '' as usuario "   +
                             "	,LC.ic_moneda as MONEDA_LINEA "+
									  "	,'' as ic_tipo_financiamiento "+
									  "	,M2.cd_nombre as NOMBRE_MLINEA " +
									  "	,DS.FN_VALOR_TASA"+
									  " 	,lc.CG_NUMERO_CUENTA AS CG_NUMERO_CUENTA "+

									  "  FROM DIS_DOCUMENTO D"   +
                             "    ,COMCAT_EPO E"   +
                             "    ,COMREL_PRODUCTO_EPO PE"   +
                             "    ,COMCAT_PRODUCTO_NAFIN PN"   +
                             "    ,COMCAT_MONEDA M"   +
                             "    ,COM_TIPO_CAMBIO TC"   +
                             "    ,COMCAT_ESTATUS_DOCTO ED"   +
                             "    ,COM_LINEA_CREDITO LC"   +
                             "    ,COMCAT_IF I"   +
                             "    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
                             "    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
                             "    ,COMCAT_TASA CT"   +
                             "    ,COMCAT_PLAZO CP"   +
                             "    ,COM_TASA_IF_EPO CTG"   +
                             "    ,COM_MANT_TASA CMT"   +
                             "    ,DIS_DOCTO_SELECCIONADO DS"   +
                             "    ,COM_ACUSE2 CA"   +
									  "	  ,COMCAT_MONEDA M2"+
                             "  WHERE E.IC_EPO = PE.IC_EPO"   +
                             "    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                             "    AND PN.IC_PRODUCTO_NAFIN = 4"   +
                             "    AND D.IC_EPO = PE.IC_EPO"   +
                             "    AND D.IC_MONEDA = M.IC_MONEDA"   +
                             "    AND M.IC_MONEDA = TC.IC_MONEDA"   +
                             "    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                             "    AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
                             "    AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
                             "    AND CTG.IC_IF = LC.IC_IF"   +
                             "    AND LC.IC_IF = I.IC_IF"   +
									  "	  AND M2.IC_MONEDA = LC.IC_MONEDA"+
                             "    AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
                             "    AND D.ic_estatus_docto = 3"   +
                             "    AND IEP.IC_IF = I.IC_IF"   +
                             "    AND IEP.IC_EPO = E.IC_EPO"   +
                             "    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                             "    AND CT.IC_PLAZO = CP.IC_PLAZO"   +
                             "    AND CT.CS_DISPONIBLE = 'S'"   +
                             "    AND CTG.IC_TASA = CT.IC_TASA"   +
                             "    AND CTG.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
                             "    AND CMT.IC_TASA = CT.IC_TASA"   +
									  "	  AND CT.IC_TASA = DS.IC_TASA"+
                             "    AND CMT.DC_FECHA IN( SELECT MAX(DC_FECHA) FROM COM_MANT_TASA  WHERE IC_TASA = CT.IC_TASA)"   +
                             "    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
                             "    AND DS.CC_ACUSE = CA.CC_ACUSE"   +
                             "    AND DS.CC_ACUSE = '"+cc_acuse+"'"   +
                             "  ORDER BY D.IG_NUMERO_DOCTO"  ;

         log.debug("consultaDoctosFactoRec: "+qrySentencia);

         rs = con.queryDB(qrySentencia);
         while(rs.next()){
				vecColumnas = new Vector();
				/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
				/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
				/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
				/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
				/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
				/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
				/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
				/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
				/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
				/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
				/*10*/		vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
				/*11*/		vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
				/*12*/		vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
				/*13*/		vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
				/*14*/		vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
				/*15*/		vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
				/*16*/		vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
				/*17*/		vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
				/*18*/		vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
				/*19*/		vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
				/*20*/		vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
				/*21*/		vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
				/*22*/		vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
				/*23*/		vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
				/*24*/		vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
				/*25*/		vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
				/*26*/		vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
				/*27*/		vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
				/*28*/		vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
				/*29*/		vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
				/*30*/		vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
				/*31*/		vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
				/*32*/		vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
				/*33*/		vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
				/*34*/		vecColumnas.add(cc_acuse);
				/*35*/		vecColumnas.add((rs.getString("FECHA_CARGA")==null)?"":rs.getString("FECHA_CARGA"));
				/*36*/		vecColumnas.add((rs.getString("HORA_CARGA")==null)?"":rs.getString("HORA_CARGA"));
				/*37*/		vecColumnas.add((rs.getString("USUARIO")==null)?"":rs.getString("USUARIO"));
				/*38*/		vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
				/*39*/		vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
				/*40*/		vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"":rs.getString("NOMBRE_MLINEA"));
				/*41*/		vecColumnas.add((rs.getString("FN_VALOR_TASA")==null)?"":rs.getString("FN_VALOR_TASA"));
				/*42*/		vecColumnas.add((rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA"));
								vecFilas.add(vecColumnas);
         }

         con.cierraStatement();
      }catch(Exception e){
			System.out.println("Excepcion "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
         }
			log.info("consultaDoctosFactoRec (S)");
      }
		return vecFilas;
   }

	/**
	 * sumatoria de los Montos de los documentos en estatus Seleccionada Pyme, En Proceso de Autorizaci�n IF y Operada
	 * para la pantala de
	 * Admin EPO  Capturas / Selecci�n de Doctos venta de cartera
	 * Admin PYME  Seleccion de Doctos. Modalidad 1 (Riesgo Empresa de Primer Orden)
	 * Admin PYME Distribuidores / Lineas de Credito / Autorizaci�n de L�neas de Cr�dito EPO
	 * Admin PYME Distribuidores / Lineas de Credito / Autorizaci�n de L�neas de Cr�dito Distribuidor
	 * Fodea 05-2014
	 * @return
	 * @param ic_moneda
	 * @param ic_pyme
	 * @param ic_epo
	 */
	public  String   getMontoTotalMocumentos  (String ic_epo, String ic_moneda  , String ic_if, String  modalidad, String ic_pyme, String lineaCredito  ){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer strQry = new StringBuffer();
		List lVarBind		= new ArrayList();
		String totalMonto  ="0";
		log.info("getMontoTotalMocumentos (E)");


		 try{
			con.conexionDB();



			strQry = new StringBuffer();
			strQry.append("  select  sum (fn_monto ) as totalMonto   "+
			" From dis_documento d,  comrel_if_epo ie  ");

			if(modalidad.equals("1") )  {
				strQry.append(" , dis_linea_credito_dm l  ");
			}else  {
				strQry.append(" , com_linea_credito  l  ");
			}

		strQry.append(" where d.ic_estatus_docto in ( ?, ?, ?   )    "+
			" and d.ic_epo   = ?   "+
			" and d.ic_moneda  = ?   "+
			" and d.ic_epo =  ie.ic_epo "+
			" and ie.ic_if  = ?  "+
			" and l.ic_if = ?  "+
			" and d.ic_moneda = l.ic_moneda  "+
			" and l.ic_if = ie.ic_if  "+
			"  and l.ic_epo = d.ic_epo   ");

			if(!ic_pyme.equals("") )  {
				strQry.append(" and d.ic_pyme = ? "  );
			}

			if(modalidad.equals("1") )  {
				strQry.append(" AND d.ic_linea_credito_dm = l.ic_linea_credito_dm  ");
			}else  {
				strQry.append("  AND d.ic_linea_credito = l.ic_linea_credito  ");
			}

			if(modalidad.equals("1")  &&  !lineaCredito.equals("") ){
				strQry.append(" and  d.ic_linea_credito_dm = ?   ");
			}
			if(modalidad.equals("2")  &&  !lineaCredito.equals("") ){
				strQry.append(" and  d.ic_linea_credito = ?    ");
			}

			lVarBind		= new ArrayList();
			lVarBind.add( "3"); //Seleccionada Pyme
			lVarBind.add( "4");  // Operada
			lVarBind.add( "24"); // En Proceso de Autorizaci�n IF
			lVarBind.add(ic_epo);
			lVarBind.add(ic_moneda );
			lVarBind.add(ic_if  );
			lVarBind.add(ic_if  );
			if(!ic_pyme.equals("") )  {
				lVarBind.add(ic_pyme );
			}
			if(!lineaCredito.equals("") ){
				lVarBind.add(lineaCredito );
			}
             
			log.trace("strQry  " +strQry);
			log.trace("lVarBind  " +lVarBind);
             
            ps = con.queryPrecompilado(strQry.toString(),lVarBind );
            rs = ps.executeQuery();
             
			if(rs.next()) {
                totalMonto= (rs.getString("totalMonto")==null?"0":rs.getString("totalMonto"));
            }
             rs.close();
             ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getMontoTotalMocumentos (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getMontoTotalMocumentos (S)");
      return totalMonto;
    }


	/**
	 * Fodea 05-2014  valido si la pyme se encuentra bloqueada
	 * @return
	 * @param ic_if
	 * @param ic_pyme
	 * @param ic_epo
	 */
	private   String  getPymeBloqueada  (String ic_epo, String ic_pyme , String ic_if    ){
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
		StringBuffer strQry = new StringBuffer();
		List lVarBind		= new ArrayList();
		String bloqueda  ="N";
		log.info("getPymeBloqueada (E)");


		 try{
			con.conexionDB();

			strQry = new StringBuffer();
			strQry.append(" SELECT cs_activo FROM comrel_pyme_epo_if_x_producto    "+
			"  WHERE ic_pyme = ?   "+
			" and ic_epo = ?   "+
			" and ic_if = ?     " +
			" AND ic_producto_nafin = ?   ");

			lVarBind		= new ArrayList();
			lVarBind.add( ic_pyme);
			lVarBind.add(ic_epo);
			lVarBind.add( ic_if );
			lVarBind.add("4");

			log.info("strQry  " +strQry);
			log.info("lVarBind  " +lVarBind);


			ps = con.queryPrecompilado(strQry.toString(),lVarBind );
         rs = ps.executeQuery();

			if(rs.next()){
				bloqueda= (rs.getString("cs_activo")==null?"N":rs.getString("cs_activo"));
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getPymeBloqueada (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getPymeBloqueada (S)");
      return bloqueda;
    }


    	/**
	 * Metodo para validar el bloqueo de la pyme
	 * Fodea 05-2014
	 * @return
	 * @param ic_pyme
	 * @param ic_epo
	 * @param ic_if
	 */

    private String getValidaBloqueo (String ic_pyme
            ,String ic_epo ,String ic_moneda    , String ig_numero_docto
            ,String fn_monto_de  ,String fn_monto_a    ,String cc_acuse
            ,String monto_desc    ,String fecha_emision_de
            ,String fecha_emision_a  ,String con_cambio    ,String fecha_vto_de
            ,String fecha_vto_a     ,String	modo_plazo    ,String fecha_publicacion_de
            ,String fecha_publicacion_a     ,String in  )  {
    		log.info("getValidaBloqueo");
    		String	qrySentencia	 	= "";
    		AccesoDB con = new AccesoDB();
    		String bloqueo = "N";
    		List varBind = new ArrayList();
    		String  ic_if  =   "";



    		try {
    			con.conexionDB();

    			qrySentencia = "   SELECT distinct  I.IC_IF AS IC_IF   "+
    								"   FROM DIS_DOCUMENTO D"   +
    								"     ,COMCAT_EPO E"   +
    								"     ,COMREL_PRODUCTO_EPO PE"   +
    								"     ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
    								"     ,COMCAT_PRODUCTO_NAFIN PN"   +
    								"     ,COMCAT_MONEDA M"   +
    								"     ,COM_TIPO_CAMBIO TC"   +
    								"     ,COMCAT_ESTATUS_DOCTO ED"   +
    								"     ,DIS_LINEA_CREDITO_DM LCD"   +
    								"     ,COMCAT_IF I"   +
    								"     ,COMCAT_TIPO_COBRO_INTERES TCI"   +
    								"     ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
    								"     ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
    								"	  ,COMCAT_MONEDA M2,comrel_clasificacion clas "+
    								"   WHERE E.IC_EPO = PE.IC_EPO"   +
    								"     AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
    								"     AND PEP.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
    								"     AND D.IC_EPO = PE.IC_EPO"   +
    								"     AND D.IC_EPO = PEP.IC_EPO"   +
    								"     AND D.IC_PYME = PEP.IC_PYME"   +
    								"     AND D.IC_MONEDA = M.IC_MONEDA"   +
    								"     AND M.IC_MONEDA = TC.IC_MONEDA"   +
    								"	  AND M2.IC_MONEDA = LCD.IC_MONEDA"+
    								"     AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
    								"     AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
    								"     AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
    								"     AND LCD.IC_IF = I.IC_IF"   +
    								"     AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
    								"     AND IEP.IC_IF = I.IC_IF"   +
    								"     AND IEP.IC_EPO = E.IC_EPO"   +
    								" 	  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
    								" 	  AND D.IC_TIPO_FINANCIAMIENTO != 5 "   +
    								"     AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
    								" 	  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
    								"     AND D.ic_estatus_docto = 2"   +
    								"     AND PN.IC_PRODUCTO_NAFIN = 4"   +
    								"	  AND PE.IC_PRODUCTO_NAFIN = 4"+
    								"	  AND LCD.ic_estatus_linea = 12"+
    								"   and  d.CG_VENTACARTERA is null "+
    								"    AND D.IC_PYME = "+ic_pyme;

    				if(!"".equals(ic_epo)&&ic_epo!=null)
    					qrySentencia += " and D.ic_epo = "+ic_epo;
    				if(!"".equals(modo_plazo)&&modo_plazo!=null)
    					qrySentencia += " and TF.ic_tipo_financiamiento = "+modo_plazo;
    				else
    				  qrySentencia += " and TF.ic_tipo_financiamiento in (1,2,3,4)";
    				if(!"".equals(ic_moneda)&&ic_moneda!=null)
    					qrySentencia += " and D.ic_moneda = "+ic_moneda;
    				if(!"".equals(ig_numero_docto)&&ig_numero_docto!=null)
    					qrySentencia += " and D.ig_numero_docto = '"+ig_numero_docto+"'";
    				if(!"".equals(cc_acuse)&&cc_acuse!=null)
    					qrySentencia += " and D.cc_acuse = '"+cc_acuse+"'";
    				if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&!"S".equals(monto_desc))
    					qrySentencia += " and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a;
    				if("S".equals(con_cambio))
    					qrySentencia += " and D.ic_documento in(select ic_documento from dis_cambio_estatus where trunc(dc_fecha_cambio)= trunc(sysdate))";
    				if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&"S".equals(monto_desc))
    					qrySentencia += " and D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) between "+fn_monto_de+" and "+fn_monto_a;
    				if(!"".equals(fecha_emision_de)&&fecha_emision_de!=null&&!"".equals(fecha_emision_a)&&fecha_emision_a!=null)
    					qrySentencia += " and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))";
    				if(!"".equals(fecha_vto_de)&&fecha_vto_de!=null&&!"".equals(fecha_vto_a)&&fecha_vto_a!=null)
    					qrySentencia += " and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))";
    				if(!"".equals(fecha_publicacion_de)&&fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&&fecha_publicacion_a!=null)
    					qrySentencia += " and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))";
    				if(!"".equals(in)&&in!=null)
    					qrySentencia += " and D.ic_documento in("+in+")";

    			log.debug("qrySentencia  "+qrySentencia);
    			ResultSet rs = con.queryDB(qrySentencia);
    			while(rs.next()){
    				 ic_if  += (rs.getString("IC_IF")==null)?"":rs.getString("IC_IF")+",";
    			}
    			rs.close();


    			log.debug("ic_if  "+ic_if);
    			if(!ic_if.equals("")) {

    			int tamanio2 =	ic_if.length();
    				String val =  ic_if.substring(tamanio2-1, tamanio2);
    				if(val.equals(",")){
    					ic_if =ic_if.substring(0,tamanio2-1);
    				}

    			log.debug("ic_if  "+ic_if);


    				StringBuffer qrySentenciaI = new StringBuffer();
    				qrySentenciaI.append(" select count(*) total from  comrel_pyme_epo_if_x_producto  "+
    										  " where  ic_if in ( " +ic_if+")"+
    										  " and ic_pyme  =  ? "+
    										  " and CS_ACTIVO = ? ");

    				varBind.add(ic_pyme);
    				varBind.add("S");

    				log.debug("qrySentencia  "+qrySentenciaI);
    				log.debug("varBind  "+varBind);

    				PreparedStatement ps1 = con.queryPrecompilado(qrySentenciaI.toString(), varBind);
    				ResultSet rs1 = ps1.executeQuery();
    				if(rs1.next()){
    					int valor = rs1.getInt("total");
    					if(valor>0){
    						bloqueo = "S";
    					}
    				}
    				rs1.close();
    				ps1.close();

    			}

    		} catch(Exception e) {
    			log.error("getValidaBloqueo(Exception) "+e);
    			throw new AppException("Error al verificar el bloqueo ",e);
    		} finally {
    			con.cierraConexionDB();
    			log.info(" getValidaBloqueo");
    		}

    		return bloqueo;
	}



	 /**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param in
	 * @param ic_linea_credito
	 * @param fecha_publicacion_a
	 * @param fecha_publicacion_de
	 * @param modo_plazo
	 * @param fecha_vto_a
	 * @param fecha_vto_de
	 * @param con_cambio
	 * @param fecha_emision_a
	 * @param fecha_emision_de
	 * @param monto_desc
	 * @param cc_acuse
	 * @param fn_monto_a
	 * @param fn_monto_de
	 * @param ig_numero_docto
	 * @param ic_moneda
	 * @param ic_epo
	 * @param ic_pyme
	 */
   public Vector consultaDoctosCccTC(String ic_pyme
		,String ic_epo
		,String ic_moneda
		,String ig_numero_docto
		,String fn_monto_de
		,String fn_monto_a
		,String cc_acuse
		,String monto_desc
		,String fecha_emision_de
		,String fecha_emision_a
		,String con_cambio
		,String fecha_vto_de
		,String fecha_vto_a
		,String	modo_plazo
		,String fecha_publicacion_de
		,String fecha_publicacion_a
		,String ic_bins
		,String in
   )
   throws AppException{
		log.info("AceptacionPymeBean::consultaDoctosCccTC (E)");
		AccesoDB con = null;
		ResultSet rs = null;
		String qrySentencia = "";
		Vector	vecFilas = new Vector();
		Vector	vecColumnas = null;
		String 	condicion = "";

		boolean ok = true;
   	try{
			con = new AccesoDB();
			con.conexionDB();

			/*se establece zona mexico*/
			String strAlterMex = " alter session set NLS_TERRITORY = 'MEXICO' ";
			con.ejecutaUpdateDB(strAlterMex);
			/*fin */

			if(!"".equals(ic_epo)&&ic_epo!=null)
			condicion += " and D.ic_epo = "+ic_epo;
			if(!"".equals(modo_plazo)&&modo_plazo!=null)
			condicion += " and TF.ic_tipo_financiamiento = "+modo_plazo;
			/*                else
			condicion += " and TF.ic_tipo_financiamiento in (1,2,3,4)";*/
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
			condicion += " and D.ic_moneda = "+ic_moneda;
			if(!"".equals(ig_numero_docto)&&ig_numero_docto!=null)
			condicion += " and D.ig_numero_docto = '"+ig_numero_docto+"'";
			if(!"".equals(cc_acuse)&&cc_acuse!=null)
			condicion += " and D.cc_acuse = '"+cc_acuse+"'";
			if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&!"S".equals(monto_desc))
			condicion += " and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a;
			if("S".equals(con_cambio))
			condicion += " and D.ic_documento in(select ic_documento from dis_cambio_estatus where trunc(dc_fecha_cambio)= trunc(sysdate))";
			if(!"".equals(fn_monto_de)&&fn_monto_de!=null&&!"".equals(fn_monto_a)&&fn_monto_a!=null&&"S".equals(monto_desc))
			condicion += " and D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) between "+fn_monto_de+" and "+fn_monto_a;
			if(!"".equals(fecha_emision_de)&&fecha_emision_de!=null&&!"".equals(fecha_emision_a)&&fecha_emision_a!=null)
			condicion += " and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_vto_de)&&fecha_vto_de!=null&&!"".equals(fecha_vto_a)&&fecha_vto_a!=null)
			condicion += " and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_publicacion_de)&&fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&&fecha_publicacion_a!=null)
			condicion += " and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))";
			if(!"".equals(ic_bins)&&ic_bins!=null)
			condicion += " and cbi.ic_bins = "+ic_bins;
			if(!"".equals(in)&&in!=null)
			condicion += " and D.ic_documento in("+in+")";

			/*	ESTE QUERY LLEVA RESTRICCI�N DE LA TASA CON EL PLAZO, ESO SE HAR� CON PROGRAMACION, POR ESO SE COMENT�, Y SE DEJ� COMO APARECE ABAJO	JRFH*/

			qrySentencia =
					"SELECT          /*+index(d IN_DIS_DOCUMENTO_02_NUK)*/ " +
					"       DISTINCT e.cg_razon_social AS epo, d.ig_numero_docto, d.cc_acuse, " +
					"                TO_CHAR (d.df_fecha_emision, 'dd/mm/yyyy') AS fecha_emision, " +
					"                TO_CHAR (d.df_fecha_venc, 'dd/mm/yyyy') AS fecha_venc, " +
					"                TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, " +
					"                d.ig_plazo_docto AS plazo, m.cd_nombre AS moneda, " +
					"                d.fn_monto AS monto, " +
					"                DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"                        'N', '', " +
					"                        'P', 'Dolar-Peso', " +
					"                        '' " +
					"                       ) AS tipo_conversion, " +
					"                DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					"                        'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, ''), " +
					"                        '' " +
					"                       ) AS tipo_cambio, " +
					"                '' AS monto_valuado, d.ig_plazo_descuento, " +
					"                d.fn_porc_descuento, tf.cd_descripcion AS modo_plazo, " +
					"                ed.cd_descripcion AS estatus, '' AS cambios, " +
					"                d.ic_documento AS numero_credito, i.cg_razon_social AS IF, " +
					"                '' AS fecha_operacion, '' AS monto_credito, " +
					"                DECODE (d.ic_tipo_financiamiento, " +
					"                        1, TRUNC (d.df_fecha_venc) - TRUNC (SYSDATE), " +
					"                        d.ig_plazo_credito " +
					"                       ) AS ig_plazo_credito, " +
					"                TO_CHAR (d.df_fecha_venc_credito, " +
					"                         'dd/mm/yyyy' " +
					"                        ) AS df_fecha_venc_credito, " +
					"                '' AS referencia_int, " +
					"                NVL (pe.cg_responsable_interes, " +
					"                     pn.cg_responsable_interes " +
					"                    ) AS responsable_interes, " +
					"                DECODE (tf.ic_tipo_financiamiento, " +
					"                        2, 0, " +
					"                        d.fn_monto * (NVL (d.fn_porc_descuento, 0) / 100) " +
					"                       ) AS monto_descuento, " +
					"                  d.fn_monto " +
					"                * (NVL (d.fn_porc_descuento, 0) / 100) AS monto_descuento, " +
					"                '' AS valor_tasa, '' AS rel_mat, '' AS puntos, i.ig_tipo_piso, " +
					"                m.ic_moneda, '' AS ic_tasa, tf.ic_tipo_financiamiento, " +
					"                NVL (pe.ig_dias_minimo, pn.in_dias_minimo) AS dias_minimo, " +
					"                NVL (pe.ig_dias_maximo, pn.in_dias_maximo) AS dias_maximo, " +
					"                i.ic_if, pym.cg_razon_social AS nombrepyme, " +
					"                e.ic_epo AS ic_epo, " +
					"                TO_CHAR " +
					"                   (sigfechahabil (  d.df_fecha_venc " +
					"                                   + NVL (pep.ig_plazo_pyme, " +
					"                                          NVL (pe.ig_dias_maximo, " +
					"                                               pn.in_dias_maximo " +
					"                                              ) " +
					"                                         ), " +
					"                                   0, " +
					"                                   '-' " +
					"                                  ), " +
					"                    'DD/MM/YYYY' " +
					"                   ) AS fechavencimiento, " +
					"                pep.ig_plazo_pyme AS plazopyme, pe.ig_dias_maximo AS plazoepo, " +
					"                pn.in_dias_maximo AS plazonafin, " +
					"                (SELECT (  sigfechahabil (d.df_fecha_venc + cpe.ig_plazo_pyme, " +
					"                                          0, " +
					"                                          '-' " +
					"                                         ) " +
					"                         - d.df_fecha_venc " +
					"                        ) " +
					"                   FROM comrel_pyme_epo_x_producto cpe " +
					"                  WHERE pym.ic_pyme = cpe.ic_pyme " +
					"                    AND e.ic_epo = cpe.ic_epo " +
					"                    AND cpe.ic_producto_nafin = 4) AS plazo_valido, " +
					"                pep.ig_plazo_pyme AS plazo_pyme, " +
					"                tf.ic_tipo_financiamiento AS plazofinanciamiento, " +
					"                pe.cg_tipos_credito AS credito, " +
					"                TO_CHAR " +
					"                   (sigfechahabil (SYSDATE - 1, " +
					"                                   NVL (pe.ig_dias_operacion, 0), " +
					"                                   '+' " +
					"                                  ), " +
					"                    'DD/MM/YYYY' " +
					"                   ) AS fecha_dias_operacion, " +
					"                NVL (pe.ig_dias_operacion, 0) AS dias_operacion, " +
					"                cbi.IC_BINS clave_bin, " +
					"                cbi.CG_DESCRIPCION desc_bin, " +
					"                cbi.CG_CODIGO_BIN codigo_bin, " +
					"                cbi.FN_COMISION comision_bin, " +
					"                cbi.IC_MONEDA moneda_bin " +
					"           FROM dis_documento d, " +
					"                comcat_pyme pym, " +
					"                comrel_if_epo_x_producto iep, " +
					"                comrel_producto_epo pe, " +
					"                comcat_epo e, " +
					"                com_tipo_cambio tc, " +
					"                comcat_if i, " +
					"                comcat_moneda m, " +
					"                comcat_estatus_docto ed, " +
					"                comcat_producto_nafin pn, " +
					"                comcat_tipo_financiamiento tf, " +
					"                comrel_pyme_epo_x_producto pep, " +
					"                com_bins_if cbi " +
					"          WHERE e.ic_epo = pe.ic_epo " +
					"            AND pym.ic_pyme = pep.ic_pyme " +
					"            AND e.ic_epo = pep.ic_epo " +
					"            AND pep.ic_producto_nafin = 4 " +
					"            AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
					"            AND pym.ic_pyme = d.ic_pyme " +
					"            AND d.ic_epo = pe.ic_epo " +
					"            AND d.ic_moneda = m.ic_moneda " +
					"            AND m.ic_moneda = tc.ic_moneda " +
          "            AND trunc(sysdate) <= d.df_fecha_venc " +
					"            AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
					"                                  FROM com_tipo_cambio " +
					"                                 WHERE ic_moneda = m.ic_moneda) " +
					"            AND d.ic_estatus_docto = ed.ic_estatus_docto " +
					"            AND d.ic_estatus_docto = 2 " +
					"            AND iep.ic_if = i.ic_if " +
					"            AND iep.ic_epo = e.ic_epo " +
					"            AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
					"            AND iep.ic_producto_nafin = pn.ic_producto_nafin " +
					"            AND cbi.ic_if = i.ic_if " +
					"            AND cbi.ic_moneda = d.ic_moneda " +
					"            AND pn.ic_producto_nafin = 4 " +
					"            AND d.ic_producto_nafin = 4 " +
					"            AND d.cg_ventacartera IS NULL " +
					"            AND d.ic_pyme =  "+ic_pyme+
					"            AND D.ig_tipo_pago = 1 "+ //F09-2015  solo Financiamiento con intereses
          		condicion+
					"  ORDER BY D.IG_NUMERO_DOCTO" ;


			System.out.println("qrySentencia:::  "+qrySentencia);
			rs = con.queryDB(qrySentencia);
			while(rs.next()){
            vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*10*/		vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*11*/		vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
/*12*/		vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/		vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*14*/		vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/		vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*16*/		vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
/*17*/		vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
/*18*/		vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
///*19*/		vecColumnas.add((rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*19*/		vecColumnas.add("");
/*20*/		vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*21*/		vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*22*/		vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*23*/		vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*24*/		vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*25*/		vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
/*26*/		vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
///*27*/		vecColumnas.add((rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
/*27*/		vecColumnas.add("");
/*28*/		vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
/*29*/		vecColumnas.add((rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*30*/		vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
///*31*/		vecColumnas.add((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*31*/		vecColumnas.add("");
/*32*/		vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*33*/		vecColumnas.add((rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
/*34*/		vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
///*35*/		vecColumnas.add((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*35*/		vecColumnas.add("");
/*36*/		vecColumnas.add((rs.getString("DIAS_MINIMO")==null)?"0":rs.getString("DIAS_MINIMO"));
/*37*/		vecColumnas.add((rs.getString("DIAS_MAXIMO")==null)?"0":rs.getString("DIAS_MAXIMO"));
///*38*/		vecColumnas.add((rs.getString("LINEA_CREDITO")==null)?"0":rs.getString("LINEA_CREDITO"));
/*38*/		vecColumnas.add("0");
/*39*/		vecColumnas.add((rs.getString("IC_IF")==null)?"0":rs.getString("IC_IF"));
///*40*/		vecColumnas.add((rs.getString("NOMBRE_MLINEA")==null)?"0":rs.getString("NOMBRE_MLINEA"));
/*40*/		vecColumnas.add("0");
/*41*/		vecColumnas.add((rs.getString("NOMBREPYME")==null)?"0":rs.getString("NOMBREPYME"));
/*42*/		vecColumnas.add((rs.getString("PLAZO_VALIDO")==null)?"0":rs.getString("PLAZO_VALIDO"));
/*43*/		vecColumnas.add((rs.getString("PLAZO_PYME")==null)?"0":rs.getString("PLAZO_PYME"));
/*44*/		vecColumnas.add((rs.getString("plazofinanciamiento")==null)?"0":rs.getString("plazofinanciamiento"));
///*45*/		vecColumnas.add((rs.getString("interes")==null)?"0":rs.getString("interes"));
/*45*/		vecColumnas.add("0");
/*46*/		vecColumnas.add((rs.getString("credito")==null)?"0":rs.getString("credito"));
/*47*/		vecColumnas.add((rs.getString("fechaVencimiento")==null)?"0":rs.getString("fechaVencimiento"));
/*48*/		vecColumnas.add((rs.getString("ic_epo")==null)?"0":rs.getString("ic_epo"));
/*49*/		vecColumnas.add((rs.getString("plazopyme")==null)?"0":rs.getString("plazopyme"));
/*50*/		vecColumnas.add((rs.getString("plazoepo")==null)?"0":rs.getString("plazoepo"));
/*51*/		vecColumnas.add((rs.getString("plazonafin")==null)?"0":rs.getString("plazonafin"));
/*52*/		vecColumnas.add((rs.getString("FECHA_DIAS_OPERACION")==null)?"0":rs.getString("FECHA_DIAS_OPERACION"));
/*53*/		vecColumnas.add((rs.getString("DIAS_OPERACION")==null)?"0":rs.getString("DIAS_OPERACION"));

/*54*/		vecColumnas.add((rs.getString("clave_bin")==null)?"0":rs.getString("clave_bin"));
/*55*/		vecColumnas.add((rs.getString("desc_bin")==null)?"":rs.getString("desc_bin"));
/*56*/		vecColumnas.add((rs.getString("codigo_bin")==null)?"":rs.getString("codigo_bin"));
/*57*/		vecColumnas.add((rs.getString("comision_bin")==null)?"0":rs.getString("comision_bin"));
/*58*/		vecColumnas.add((rs.getString("moneda_bin")==null)?"1":rs.getString("moneda_bin"));



            vecFilas.add(vecColumnas);
				//System.out.println("plazo_valido::::::::::::"+rs.getString("PLAZO_VALIDO"));
         }

         rs.close();
			con.cierraStatement();
		}catch(Throwable e){
			log.error("ERROR::consultaDoctosCccTC", e);
			throw new AppException("Error al consultar documentos de Cred en Cuenta Corriente para Pago con Tarjeta de Cred", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("AceptacionPymeBean::consultaDoctosCccTC (S)");
		}
		return vecFilas;
	}


	public void preGuardadoIdOrdenTC(String[]ic_documento, String idNumOrden, String montoToTal) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String strSQL = "";
		boolean commit = true;
		try{
			con.conexionDB();

			strSQL = "UPDATE dis_documento " +
						"   SET ic_orden_pago_tc = ? " +
						" WHERE ic_documento = ? ";


			for(int x=0; x<ic_documento.length;x++){
				ps = con.queryPrecompilado(strSQL);
				ps.setString(1, idNumOrden);
				ps.setLong(2, Long.parseLong(ic_documento[x]));
				ps.executeUpdate();
				ps.close();
			}

			strSQL = "INSERT INTO dis_doctos_pago_tc " +
						"            (ic_orden_pago_tc, fn_monto_total, cs_estatus_transaccion " +
						"            ) " +
						"     VALUES (?, ?, ? " +
						"            ) ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, idNumOrden);
			ps.setDouble(2, Double.parseDouble(montoToTal));
			ps.setString(3, "P");
			ps.executeUpdate();


		}catch(Throwable t){
			commit = false;
			throw new AppException("Error al pre guardar el numero de orden generado para el pago con tarjeta de credito", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}

	public void rechazoIdOrdenTC(HashMap hmData, String[] numero_docto) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		String strSQL = "";
		boolean commit = true;
		try{
			con.conexionDB();
			StringBuffer strDocumentos = new StringBuffer("");



			String orderId = (String)hmData.get("OrderId");
			String responseCode = (String)hmData.get("ResponseCode");
			String responseMessage = (String)hmData.get("ResponseMessage");
			String iNoNafinElectronico = (String)hmData.get("iNoNafinElectronico");
			String iNoUsuario = (String)hmData.get("iNoUsuario");
			String cveEpo = (String)hmData.get("cveEpo");
			String cveIf = (String)hmData.get("cveIf");

			strDocumentos.append("ic_epo = ").append(cveEpo);
			strDocumentos.append(", ic_if = ").append(cveIf);
			strDocumentos.append(", \ncodigo respuesta = ").append(responseCode);
			strDocumentos.append(", descripcion respuesta = ").append(responseMessage).append(", \n");

			for(int i=0; i<numero_docto.length;i++ ){
				strDocumentos.append(" no. docto = ").append(numero_docto[i]);
			}

			strSQL = "UPDATE dis_doctos_pago_tc " +
						"  set cs_estatus_transaccion = ?, " +
						"  	cg_codigo_resp = ?, " +
						"  	cg_codigo_desc = ? " +
						"  WHERE ic_orden_pago_tc = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, "R");
			ps.setString(2, responseCode);
			ps.setString(3, responseMessage);
			ps.setString(4, orderId);
			ps.executeUpdate();

			Bitacora.grabarEnBitacora(con, "SELEC_DIST_CCC", "D",iNoNafinElectronico, iNoUsuario, "", strDocumentos.toString());

		}catch(Throwable t){
			commit = false;
			throw new AppException("Error al rechazar el numero de orden generado para el pago con tarjeta de credito", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
	}

	public Vector consultaDoctosCCCTC(String cc_acuse)
   throws NafinException{
	System.out.println("AceptacionPymeBean::consultaDoctosCCCTC (E)");
   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas = new Vector();
        Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();

                qrySentencia =   "SELECT E.CG_RAZON_SOCIAL AS EPO " +
											"      ,D.IG_NUMERO_DOCTO " +
											"      ,D.CC_ACUSE " +
											"      ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION " +
											"      ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENC " +
											"      ,to_char(D.df_carga,'dd/mm/yyyy') AS FECHA_PUBLICACION " +
											"      ,D.IG_PLAZO_DOCTO AS PLAZO " +
											"      ,M.CD_NOMBRE AS MONEDA " +
											"      ,D.FN_MONTO AS MONTO " +
											"      ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION " +
											"      ,'' as MONTO_VALUADO " +
											"      ,D.ig_plazo_descuento " +
											"      ,D.fn_porc_descuento " +
											"      ,TF.cd_descripcion as MODO_PLAZO " +
											"      ,ED.CD_DESCRIPCION AS ESTATUS " +
											"      ,'' AS CAMBIOS " +
											"      ,D.IC_DOCUMENTO AS NUMERO_CREDITO " +
											"      ,I.CG_RAZON_SOCIAL AS IF " +
											"      ,'' AS FECHA_OPERACION " +
											"      ,DS.fn_importe_recibir AS MONTO_CREDITO " +
											"      ,D.IG_PLAZO_CREDITO " +
											"      ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO " +
											"      , nvl(pe.cg_responsable_interes,pn.cg_responsable_interes) as RESPONSABLE_INTERES " +
											"      ,decode(TF.ic_tipo_financiamiento,2,0,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100)) as MONTO_DESCUENTO " +
											"      ,ds.FN_VALOR_TASA AS VALOR_TASA " +
											"      ,ds.CG_REL_MAT AS REL_MAT " +
											"      ,ds.FN_PUNTOS AS PUNTOS " +
											"      ,I.IG_TIPO_PISO " +
											"      ,M.IC_MONEDA " +
											"      ,TF.ic_tipo_financiamiento " +
											"      ,nvl(pe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO " +
											"      ,nvl(pe.ig_dias_maximo,pn.in_dias_maximo) as DIAS_MAXIMO " +
											"      ,DS.fn_importe_interes as MONTO_INTERES " +
											"      , to_char(ca.df_fecha_hora,'dd/mm/yyyy') as fecha_carga " +
											"      , to_char(ca.df_fecha_hora,'hh:mi') as hora_carga " +
											"      , ca.ic_usuario as usuario " +
											"      , PYM.CG_RAZON_SOCIAL AS NOMBREPYME " +
											"      , CBI.CG_DESCRIPCION as DESC_BINS " +
                      "      , CBI.CG_CODIGO_BIN as CODIGO_BIN " +
											"      , DTC.IC_ORDEN_PAGO_TC as ID_ORDEN_TRANS " +
											"      , DTC.CG_CODIGO_RESP as CODIGO_RESP " +
											"      , DTC.CG_CODIGO_DESC as CODIGO_DESC " +
											"      , DTC.CG_TRANSACCION_ID as ID_TRANSACCION " +
											"      , DTC.CG_CODIGO_AUTORIZACION as CODIGO_AUTORIZA " +
                      "      , DTC.cg_num_tarjeta as NUM_TARJETA " +
											"		 , DTC.FN_MONTO_TOTAL as MONTO_TOT_TRANS "+
											"      , TO_CHAR(DTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_AUT_TRANS " +
											"    FROM DIS_DOCUMENTO D " +
											"      ,DIS_DOCTO_SELECCIONADO DS " +
											"      ,COMCAT_PYME PYM " +
											"      ,COMCAT_EPO E " +
											"      ,COMREL_PRODUCTO_EPO PE " +
											"      ,COMCAT_PRODUCTO_NAFIN PN " +
											"      ,COMCAT_MONEDA M " +
											"      ,COMCAT_ESTATUS_DOCTO ED " +
											"      ,COMCAT_IF I " +
											"      ,COMREL_IF_EPO_X_PRODUCTO IEP  " +
											"      ,COMCAT_TIPO_FINANCIAMIENTO TF " +
											"      ,COM_ACUSE2 CA, " +
											"       COM_BINS_IF CBI, " +
											"       DIS_DOCTOS_PAGO_TC DTC " +
											"    WHERE E.IC_EPO = PE.IC_EPO " +
											"      AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO " +
											"      AND PYM.IC_PYME = D.IC_PYME " +
											"      AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
											"      AND PN.IC_PRODUCTO_NAFIN = 4 " +
											"      AND D.IC_EPO = PE.IC_EPO " +
											"      AND D.IC_MONEDA = M.IC_MONEDA " +
											"      AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
											"      AND IEP.IC_IF = I.IC_IF " +
											"      AND IEP.IC_EPO = E.IC_EPO " +
											"      AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
											"      AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
											"      AND D.IC_MONEDA = CBI.IC_MONEDA " +
											"      AND D.IC_BINS = CBI.IC_BINS " +
											"     AND CBI.IC_IF = I.IC_If " +
											"      AND D.IC_ORDEN_PAGO_TC = DTC.IC_ORDEN_PAGO_TC " +
											"      AND DS.CC_ACUSE = CA.CC_ACUSE " +
											"     AND DS.CC_ACUSE = '"+cc_acuse+"' " +
											"  ORDER BY D.IG_NUMERO_DOCTO ";



                System.out.println(qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*1*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*2*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*3*/			vecColumnas.add((rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION"));
/*4*/			vecColumnas.add((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*5*/			vecColumnas.add((rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION"));
/*6*/			vecColumnas.add((rs.getString("PLAZO")==null)?"":rs.getString("PLAZO"));
/*7*/			vecColumnas.add((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"));
/*8*/			vecColumnas.add((rs.getString("MONTO")==null)?"":rs.getString("MONTO"));
/*9*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*10*/			vecColumnas.add("");//(rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*11*/			vecColumnas.add((rs.getString("MONTO_VALUADO")==null)?"":rs.getString("MONTO_VALUADO"));
/*12*/			vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*13*/			vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*14*/			vecColumnas.add((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));
/*15*/			vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*16*/			vecColumnas.add((rs.getString("CAMBIOS")==null)?"":rs.getString("CAMBIOS"));
/*17*/			vecColumnas.add((rs.getString("NUMERO_CREDITO")==null)?"":rs.getString("NUMERO_CREDITO"));
/*18*/			vecColumnas.add((rs.getString("IF")==null)?"":rs.getString("IF"));
/*19*/			vecColumnas.add("");//(rs.getString("TIPO_SOLICITUD")==null)?"":rs.getString("TIPO_SOLICITUD"));
/*20*/			vecColumnas.add((rs.getString("FECHA_OPERACION")==null)?"":rs.getString("FECHA_OPERACION"));
/*21*/			vecColumnas.add((rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO"));
/*22*/			vecColumnas.add((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));
/*23*/			vecColumnas.add((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));
/*24*/			vecColumnas.add((rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA"));
/*25*/			vecColumnas.add((rs.getString("REL_MAT")==null)?"":rs.getString("REL_MAT"));
/*26*/			vecColumnas.add((rs.getString("PUNTOS")==null)?"":rs.getString("PUNTOS"));
/*27*/			vecColumnas.add("");//(rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"));
/*28*/			vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));
/*29*/			vecColumnas.add("");//(rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT"));
/*30*/			vecColumnas.add((rs.getString("IG_TIPO_PISO")==null)?"":rs.getString("IG_TIPO_PISO"));
/*31*/			vecColumnas.add("");//(rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
/*32*/			vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*33*/			vecColumnas.add("");//(rs.getString("IC_TASA")==null)?"":rs.getString("IC_TASA"));
/*34*/			vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
/*35*/			vecColumnas.add("");//(rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));
/*36*/			vecColumnas.add((rs.getString("DIAS_MINIMO")==null)?"0":rs.getString("DIAS_MINIMO"));
/*37*/			vecColumnas.add((rs.getString("DIAS_MAXIMO")==null)?"0":rs.getString("DIAS_MAXIMO"));
/*38*/			vecColumnas.add("0");//(rs.getString("LINEA_CREDITO")==null)?"0":rs.getString("LINEA_CREDITO"));
/*39*/			vecColumnas.add((rs.getString("MONTO_INTERES")==null)?"0":rs.getString("MONTO_INTERES"));
/*40*/			vecColumnas.add(acuseFormateado);
/*41*/			vecColumnas.add((rs.getString("FECHA_CARGA")==null)?"":rs.getString("FECHA_CARGA"));
/*42*/			vecColumnas.add((rs.getString("HORA_CARGA")==null)?"":rs.getString("HORA_CARGA"));
/*43*/			vecColumnas.add((rs.getString("USUARIO")==null)?"":rs.getString("USUARIO"));
/*44*/			vecColumnas.add("");//(rs.getString("NOMBRE_MLINEA")==null)?"":rs.getString("NOMBRE_MLINEA"));
/*45*/			vecColumnas.add((rs.getString("NOMBREPYME")==null)?"":rs.getString("NOMBREPYME"));

/*46*/			vecColumnas.add((rs.getString("DESC_BINS")==null)?"":rs.getString("DESC_BINS"));
/*47*/			vecColumnas.add((rs.getString("ID_ORDEN_TRANS")==null)?"":rs.getString("ID_ORDEN_TRANS"));
/*48*/			vecColumnas.add((rs.getString("CODIGO_RESP")==null)?"":rs.getString("CODIGO_RESP"));
/*49*/			vecColumnas.add((rs.getString("CODIGO_DESC")==null)?"":rs.getString("CODIGO_DESC"));
/*50*/			vecColumnas.add((rs.getString("ID_TRANSACCION")==null)?"":rs.getString("ID_TRANSACCION"));
/*51*/			vecColumnas.add((rs.getString("CODIGO_AUTORIZA")==null)?"":rs.getString("CODIGO_AUTORIZA"));
/*52*/			vecColumnas.add((rs.getString("FECHA_AUT_TRANS")==null)?"":rs.getString("FECHA_AUT_TRANS"));
/*53*/			vecColumnas.add((rs.getString("MONTO_TOT_TRANS")==null)?"":rs.getString("MONTO_TOT_TRANS"));
/*54*/			vecColumnas.add((rs.getString("CODIGO_BIN")==null)?"":rs.getString("CODIGO_BIN"));
/*55*/			vecColumnas.add((rs.getString("NUM_TARJETA")==null)?"":rs.getString("NUM_TARJETA"));


                vecFilas.add(vecColumnas);
                }
                con.cierraStatement();
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("AceptacionPymeBean::consultaDoctosCCCTC (S)");
        }
   return vecFilas;
   }

	private void insertaAcuse3(
		String  acuse,
		String	totalMontoMN,
		String	totalInteresMN,
		String	totalMontoDescMN,
		String	totalMontoUSD,
		String	totalInteresUSD,
		String	totalMontoDescUSD,
		String	icUsuario,
		String	cgReciboElectronico,
			  AccesoDB con)
		throws NafinException{
			  String qrySentencia = "";
			try{
			qrySentencia =	" INSERT INTO COM_ACUSE3"+
								" (cc_acuse,in_monto_mn,in_monto_int_mn,in_monto_dscto_mn"+
											  " ,in_monto_dl,in_monto_int_dl,in_monto_dscto_dl"+
											  " ,ic_usuario,df_fecha_hora,cg_recibo_electronico,ic_producto_nafin)"+
											  " values('"+acuse.toString()+"',"+totalMontoMN+","+totalInteresMN+","+totalMontoDescMN +
											  " ,"+totalMontoUSD+","+totalInteresUSD+","+totalMontoDescUSD+
											  " ,'"+icUsuario+"',sysdate,'"+cgReciboElectronico+"',4)";
						 con.ejecutaSQL(qrySentencia);
			  }catch(Exception e){
			System.out.println("Excepcion "+e);
						 throw new NafinException("SIST0001");
			  }

   }

	/**
		 *
		 * Fodea 05-2014
		 * @throws netropology.utilerias.AppException
		 * @param claveEpos
		 * @param ruta
		 */
		public void procesoCambioEstatus (String ruta)	throws NafinException	{
			log.info("procesoCambioEstatus (E)");
			PreparedStatement ps = null;
			ResultSet rs        = null;
			AccesoDBOracle	 con 	     =   new AccesoDBOracle();
			StringBuffer qrySentencia = new StringBuffer();
			List  lVarBind 		= new ArrayList();
			boolean resultado = true;
			HashMap hmArchivoAdjunto = new HashMap();
			HashMap archivoAdjunto 		 = new HashMap();
			Correo correo = new Correo();
			ArrayList listaDeImagenes = null;
			ArrayList listaDeArchivos = new ArrayList();
			StringBuffer mensajeCorreo = new StringBuffer();

			String strDirectorioTemp = ruta;
			//String strDirectorioFisico = strDirectorioTemp+"/nafin-web/00tmp/24finandist/";
			String de ="cadenas@nafin.gob.mx";
			String tituloMensaje ="Documentos con cambio de estatus Negociable";
			String ic_documentos ="",  num_pymes  ="",  loginUsuario ="",  destinatario =  "";


			try {
				con.conexionDB();

				UtilUsr utilUsr = new UtilUsr();

				//Obtengo los documentos que fueron cambiados a estatus 2 el dia de hoy
				qrySentencia = new StringBuffer();
				qrySentencia.append(" select ic_documento    from  DIS_CAMBIO_ESTATUS   "  +
				"  WHERE   IC_CAMBIO_ESTATUS   in ( ? , ? )  " +
				"  and  TRUNC (dc_fecha_cambio) = TRUNC (SYSDATE)  ");

				lVarBind.add("2");
				lVarBind.add("41");

				log.debug("qrySentencia.toString()  "+qrySentencia.toString());
				log.debug("lVarBind  "+lVarBind );

				ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
				rs = ps.executeQuery();

				while (rs.next()) {
					ic_documentos += rs.getString("ic_documento")==null?"":rs.getString("ic_documento")+",";
				}
				rs.close();
				ps.close();


				if(!ic_documentos.equals(""))  {

				int tamanio2 =	ic_documentos.length();
				String valor =  ic_documentos.substring(tamanio2-1, tamanio2);
				if(valor.equals(",")){
					ic_documentos =ic_documentos.substring(0,tamanio2-1);
				}

				// ************************Correo que se envia a los Distribuodores  NAFIN  *************************+
					//Obtengo todos las PYMES
					qrySentencia = new StringBuffer();
					qrySentencia.append(" select distinct  ic_pyme  from  dis_documento  " +
											 "   where ic_documento in ( "+ic_documentos+") " +
											 " and  ic_estatus_docto = 2  "+
											 " order by ic_pyme    desc " );

					log.debug("qrySentencia.toString()  "+qrySentencia.toString());
					ps = con.queryPrecompilado(qrySentencia.toString());
					rs = ps.executeQuery();

					//se envia correo por cada pyme
					while (rs.next()) {

						//Limpia Variables
						hmArchivoAdjunto = new HashMap();
						archivoAdjunto 		 = new HashMap();
						listaDeImagenes = null;
						listaDeArchivos = new ArrayList();
						mensajeCorreo = new StringBuffer();

						String  ic_pyme = rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
						num_pymes += rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme")+",";

						String nombrePyme  =  getNombrePyme(ic_pyme); //obtengo el nombre de la pyme

						// obtengo el correo electronico de la pyme
						 List cuentas = utilUsr.getUsuariosxAfiliado(ic_pyme, "P");
						  System.out.println("ic_pyme ----------->"+ic_pyme);
						 System.out.println("cuentas ----------->"+cuentas);

						 if(cuentas.size()>0)  {
							 Iterator itCuentas = cuentas.iterator();
							 if (itCuentas.hasNext()) {
								loginUsuario = (String) itCuentas.next();
							}
							Usuario usuarioObj = utilUsr.getUsuario(loginUsuario);
							destinatario = usuarioObj.getEmail();
						 }
						 System.out.println("destinatario ----------->"+destinatario);
						//metodo que regresa el archivo
						String nombreArchivo =  CorreoprocesoCambioEstatusXPYME (strDirectorioTemp, ic_pyme , ic_documentos );

						if(!nombreArchivo.equals("")) {
							archivoAdjunto 		 = new HashMap();
							archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );
							mensajeCorreo = new StringBuffer();
							mensajeCorreo.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">");
							mensajeCorreo.append("Estimado Usuario <b>"+nombrePyme+ ".</b> \n ");
							mensajeCorreo.append("<br> Por medio del presente se le notifica que los siguientes documentos fueron retornados al estatus <b>Negociable</b>, por lo que los recursos no ser�n abonados. "+
														"<br>Usted puede consultar las causas del Rechazo en la columna <b>Causa</b> del archivo adjunto \n </br>" );

							mensajeCorreo.append("<br>Para mayor informaci�n lo invitamos a consultar el portal de Cadenas Productivas <b> (http://cadenas.nafin.com.mx)</b> en el m�dulo de 'Distribuidores'.\r \n ");

							mensajeCorreo.append("<br>ATENTAMENTE \n ");
							mensajeCorreo.append("<br>Nacional Financiera, S.N.C.</br> \r\n ");

							mensajeCorreo.append("Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr�nicos de entrada. Por favor no responsa a este mensaje \n ");
							mensajeCorreo.append( "</span></span>");

							listaDeArchivos.add(archivoAdjunto);

							try { //Si el env�o del correo falla se ignora.
									correo.enviaCorreoConDatosAdjuntos(de,destinatario,"",tituloMensaje,mensajeCorreo.toString(),listaDeImagenes,listaDeArchivos);
								} catch(Throwable t) {
									log.error("Error al enviar correo " + t.getMessage());
								}
							}

					}
					rs.close();
					ps.close();

					System.out.println("num_pymes ----------->"+num_pymes);
				// ************************Correo que se envia al Adminitrador NAFIN  *************************+
				if(!num_pymes.equals("")) {

					int tamanio3 =	num_pymes.length();
					String valor1 =  num_pymes.substring(tamanio3-1, tamanio3);
					if(valor1.equals(",")){
						num_pymes =num_pymes.substring(0,tamanio3-1);
					}
				System.out.println("num_pymes ----------->"+num_pymes);

					//metodo que regresa el archivo
					String nombreArchivoN =  CorreoCambioEstatusparaNafin (strDirectorioTemp, num_pymes , ic_documentos);
					String  destinatarioNafin = getCorreoNotiNafin();

					System.out.println("nombreArchivoN ----------->"+nombreArchivoN);

					if(!nombreArchivoN.equals("")) {

						archivoAdjunto 		 = new HashMap();
						archivoAdjunto.put("FILE_FULL_PATH", nombreArchivoN );
						mensajeCorreo = new StringBuffer();
						mensajeCorreo.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">");
						mensajeCorreo.append("Estimado Adminitrador NAFIN. \n ");
						mensajeCorreo.append("<br>  Por medio del presente se le adjunta el concentrado de todos los documentos que fueron retornados a estatus <b>Negociable </b> mismos que han sido notificados a los Distribuidores.\r \n ");

						mensajeCorreo.append("<br><br>ATENTAMENTE \n ");
						mensajeCorreo.append("<br> Nacional Financiera, S.N.C. </br>\r\n ");

						mensajeCorreo.append("<br> Nota: Este mensaje fue enviado desde una cuenta de notificaciones que no puede aceptar correos electr�nicos de entrada. Por favor no responsa a este mensaje \n ");
						mensajeCorreo.append( "</span></span>");

						listaDeArchivos = new ArrayList();
						listaDeArchivos.add(archivoAdjunto);
						System.out.println("destinatarioNafin ----------->"+destinatarioNafin);
						try { //Si el env�o del correo falla se ignora.
							correo.enviaCorreoConDatosAdjuntos(de,destinatarioNafin,"",tituloMensaje,mensajeCorreo.toString(),listaDeImagenes,listaDeArchivos);
						} catch(Throwable t) {
							log.error("Error al enviar correo " + t.getMessage());
						}
					}
				}
			}else  {
				System.out.println("  No hay documentos retornados a �Negociable� a notificar  ");
			}

		}catch(Exception e){
				resultado = false;
				log.error("procesoCambioEstatus (Exception) " + e);
				throw new AppException("procesoCambioEstatus(Exception) ", e);
			}finally{
				if(con.hayConexionAbierta()){
					con.terminaTransaccion(resultado);
					con.cierraConexionDB();
				}
			}
			log.info("procesoCambioEstatus (S)");

		}


		/**
		 *  metodo para obtener los documentos que fueron procesados  para su cambio de estatus
		 *  solo los de la PYME
		 *   Fodea 05-2014
		 * @throws netropology.utilerias.AppException
		 * @return
		 * @param strDirectorioTemp
		 * @param documentos
		 */
		private static String  CorreoprocesoCambioEstatusXPYME ( String strDirectorioTemp, String ic_pyme, String ic_documento )	throws AppException	{

			PreparedStatement ps = null;
			ResultSet rs        = null;
			AccesoDBOracle	 con 	     =   new AccesoDBOracle();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			BufferedWriter 	out 								= null;
			String nombreArchivo = "";
			String	fecha	 = new java.text.SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date());
			OutputStreamWriter writer = null;
			List  lVarBind 		= new ArrayList();

			log.info("CorreoprocesoCambioEstatusXPYME (E)");
			nombreArchivo = strDirectorioTemp+"DocumentosCambioEstatus"+fecha+".csv";

			try {
				con.conexionDB();

				log.debug("ic_pyme "+ic_pyme  );

				// Obtengo ls registros que fueron modificados y los envio en un excel  por correo electronico
				qrySentencia = new StringBuffer();
				qrySentencia.append("  SELECT distinct d.ic_pyme,     e.cg_razon_social  as nombreEPO,  "+
				"  d.IG_NUMERO_DOCTO as NumeroDocto,  "+
				" to_char(d.DF_FECHA_VENC , 'dd/mm/yyyy')  as fechaVenci,   "+
				" to_char(c.DC_FECHA_CAMBIO , 'dd/mm/yyyy')  as fechaCambioEstatus,   "+
				" m.CD_NOMBRE  as nombreMoneda , " +
				" d.FN_MONTO as monto, " +
				" ec.CD_DESCRIPCION as estatusCambio,  "+
				" c.CT_CAMBIO_MOTIVO    as causa  " +
				" from  dis_documento d, " +
				" DIS_CAMBIO_ESTATUS c ,   " +
				" comcat_epo e ,  " +
				" comcat_moneda m, " +
				" COMCAT_CAMBIO_ESTATUS ec  " +
				" Where d.ic_documento = c.ic_documento " +
				" and  d.ic_moneda = m.ic_moneda " +
				" and c.ic_cambio_estatus  = ec.ic_cambio_estatus " +
				" and d.ic_epo = e.ic_epo " +
				" and ec.ic_cambio_estatus = ?  " +
			" and  TRUNC (dc_fecha_cambio) = TRUNC (SYSDATE)  ");

				lVarBind.add("2");


				if(!ic_pyme.equals("") )  {
					qrySentencia.append(" and d.ic_pyme = ?  ");
					lVarBind.add(ic_pyme );
				}

				if(!ic_documento.equals("") )  {
					qrySentencia.append(" and d.ic_documento  in ("+ic_documento+")" );
				}

				log.debug("qrySentencia " +qrySentencia);
				log.debug("lVarBind " +lVarBind);

				ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind );
				rs = ps.executeQuery();


				writer  = new OutputStreamWriter(new FileOutputStream(nombreArchivo),"Cp1252");
				out   = new BufferedWriter(writer);


				out.write("	EPO " +",");
				out.write("	N�mero de Documento " +",");
				out.write("	Fecha de Vencimiento " +",");
				out.write("	Fecha Cambio de Esatus  " +",");
				out.write("	Moneda " +",");
				out.write("	Monto " +",");
				out.write("	Tipo de Cambio de estatus " +",");
				out.write("	Causa " +",");
				out.write("\r\n");

				int numero =0;
				while (rs.next()) {

					String nombreEPO = rs.getString("nombreEPO")==null?"":rs.getString("nombreEPO").replace(',',' ');
					String fechaVenci = rs.getString("fechaVenci")==null?"":rs.getString("fechaVenci").replace(',',' ');
					String fechaCambioEstatus = rs.getString("fechaCambioEstatus")==null?"":rs.getString("fechaCambioEstatus").replace(',',' ');
					String nombreMoneda = rs.getString("nombreMoneda")==null?"":rs.getString("nombreMoneda").replace(',',' ');
					String monto = rs.getString("monto")==null?"":rs.getString("monto").replace(',',' ');
					String estatusCambio = rs.getString("estatusCambio")==null?"":rs.getString("estatusCambio").replace(',',' ');
					String causa = rs.getString("causa")==null?"":rs.getString("causa").replace(',',' ');
					String numero_docto = rs.getString("NumeroDocto")==null?"":rs.getString("NumeroDocto").replace(',',' ');

					out.write(nombreEPO+",");
					out.write(numero_docto+",");
					out.write(fechaVenci+",");
					out.write(fechaCambioEstatus+",");
					out.write(nombreMoneda+",");
					out.write(monto+",");
					out.write(estatusCambio+",");
					out.write(causa+",");
					out.write("\r\n");
						numero++;
					}
					if(numero==0){
						nombreArchivo ="";
					}


			}catch(Exception e){
				log.error("CorreoprocesoCambioEstatusXPYME (Exception) " + e);
				throw new AppException("CorreoprocesoCambioEstatusXPYME(Exception) ", e);
			}finally{
				if (rs 	!= null) try { rs.close(); 	} catch(SQLException e) {}
				if (ps 	!= null) try { ps.close(); 	} catch(SQLException e) {}
				if (out 	!= null )  try { out.close();   } catch(Exception    e) {}
				if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB();

				if(con.hayConexionAbierta()){
					con.terminaTransaccion(resultado);
					con.cierraConexionDB();
				}
			}
			log.info("CorreoprocesoCambioEstatusXPYME (S)");
			return nombreArchivo;
		}
			/**
		 * metodo que arma el arhivo para Nafin que se enviara el administrado Nafin
		 *  Fodea 05-2014
		 * @throws netropology.utilerias.AppException
		 * @return
		 * @param ic_documentos
		 * @param ic_pymes
		 * @param strDirectorioTemp
		 */
		private static String  CorreoCambioEstatusparaNafin  ( String strDirectorioTemp, String ic_pymes, String ic_documentos )	throws AppException	{

			PreparedStatement ps = null;
			ResultSet rs        = null;
			AccesoDBOracle	 con 	     =   new AccesoDBOracle();
			boolean	 resultado = true;
			StringBuffer qrySentencia = new StringBuffer();
			BufferedWriter 	out 								= null;
			String nombreArchivo = "";
			String	fecha	 = new java.text.SimpleDateFormat("dd-MM-yyyy").format(new java.util.Date());
			OutputStreamWriter writer = null;

			log.info("CorreoCambioEstatusparaNafin (E)");
			nombreArchivo = strDirectorioTemp+"ConcentradoCambioEstatus"+fecha+".csv";

			try {
				con.conexionDB();

				// Obtengo ls registros que fueron modificados y los envio en un excel  por correo electronico
				qrySentencia = new StringBuffer();
				qrySentencia.append("   SELECT  distinct  " +
				" p.cg_razon_social  as nombrePYME,   " +
				" e.cg_razon_social  as nombreEPO,  " +
				" i.cg_razon_social  as nombreIF, " +
				" d.IG_NUMERO_DOCTO as numeroDocto,  " +
				" d.fn_monto as monto " +
				" from  dis_documento d, " +
				" DIS_LINEA_CREDITO_DM  s,     "+
				" comcat_epo e ,   "+
				" comcat_pyme  p , "+
				" comcat_if i" +
				" Where d.ic_epo = e.ic_epo "+
				" and  d.ic_pyme= p.ic_pyme " +
				" and s.ic_if = i.ic_if  " +
				" and d.ic_epo = s.ic_epo   "+
				" and d.ic_estatus_docto = 2  " +
				" and IC_ESTATUS_LINEA = 12 "+
				" and s.ic_moneda = d.ic_moneda  "+
				" AND s.cg_tipo_solicitud = 'I'  "+
				" and TRUNC (s.df_vencimiento_adicional) > TRUNC (SYSDATE)  "+
				" AND D.IC_LINEA_CREDITO_DM = s.IC_LINEA_CREDITO_DM "+
				" and d.ic_documento  in ("+ic_documentos+")" +
				" and d.ic_pyme   in ("+ic_pymes+")"+
				"  UNION ALL  "+
				" SELECT DISTINCT p.cg_razon_social AS nombrepyme,  "+
				"					 e.cg_razon_social AS nombreepo, i.cg_razon_social AS nombreif,  "+
				"					 d.ig_numero_docto AS numerodocto, d.fn_monto AS monto  "+
				"			  FROM dis_documento d,  "+
				"					 COM_LINEA_CREDITO s,  "+
				"					 comcat_epo e,  "+
				"					 comcat_pyme p,  "+
				"					 comcat_if i  "+
				"			 WHERE d.ic_epo = e.ic_epo  "+
				"				AND d.ic_pyme = p.ic_pyme  "+
				"				AND s.ic_if = i.ic_if  "+
				"			  AND d.ic_epo = s.ic_epo  "+
				"			  AND d.ic_estatus_docto = 2  "+
				"			  AND ic_estatus_linea = 12  "+
				"			  AND s.ic_moneda = d.ic_moneda   "+
				"			 AND s.cg_tipo_solicitud = 'I'  "+
				"			 AND TRUNC (s.df_vencimiento_adicional) > TRUNC (SYSDATE)  "+
				"				AND d.ic_linea_credito = s.ic_linea_credito "+
				"				AND d.ic_documento IN   ("+ic_documentos+")" +
				"				AND d.ic_pyme IN ("+ic_pymes+")");

				log.debug("qrySentencia " +qrySentencia);

				ps = con.queryPrecompilado(qrySentencia.toString() );
				rs = ps.executeQuery();


				writer  = new OutputStreamWriter(new FileOutputStream(nombreArchivo),"Cp1252");
				out   = new BufferedWriter(writer);

				out.write("	DISTRIBUIDOR " +",");
				out.write("	EPO  " +",");
				out.write("	IF " +",");
				out.write("	No. de Documento  " +",");
				out.write("	Monto " +",");
				out.write("\r\n");


				int numero =0;
				while (rs.next()) {

					String nombrePYME = rs.getString("nombrePYME")==null?"":rs.getString("nombrePYME").replace(',',' ');
					String nombreEPO = rs.getString("nombreEPO")==null?"":rs.getString("nombreEPO").replace(',',' ');
					String nombreIF = rs.getString("nombreIF")==null?"":rs.getString("nombreIF").replace(',',' ');
					String numeroDocto = rs.getString("numeroDocto")==null?"":rs.getString("numeroDocto").replace(',',' ');
					String monto = rs.getString("monto")==null?"":rs.getString("monto").replace(',',' ');

					out.write(nombrePYME+",");
					out.write(nombreEPO+",");
					out.write(nombreIF+",");
					out.write(numeroDocto+",");
					out.write(monto+",");
					out.write("\r\n");
						numero++;
					}
					if(numero==0){
						nombreArchivo ="";
					}


			}catch(Exception e){
				log.error("CorreoCambioEstatusparaNafin (Exception) " + e);
				throw new AppException("CorreoCambioEstatusparaNafin(Exception) ", e);
			}finally{
				if (rs 	!= null) try { rs.close(); 	} catch(SQLException e) {}
				if (ps 	!= null) try { ps.close(); 	} catch(SQLException e) {}
				if (out 	!= null )  try { out.close();   } catch(Exception    e) {}
				if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB();

				if(con.hayConexionAbierta()){
					con.terminaTransaccion(resultado);
					con.cierraConexionDB();
				}
			}
			log.info("CorreoCambioEstatusparaNafin (S)");
			return nombreArchivo;
		}

	/**
	 * Obtengo la razon social de la PYME  Fodea 05-2014
	 * @return
	 * @param ic_pyme
	 */
	private static  String getNombrePyme (String  ic_pyme ){
      AccesoDBOracle 	      con   =  new AccesoDBOracle();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      List lVarBind		= new ArrayList();
		log.info("getNombrePyme (E)");
		String nombrePYME ="";

		 try{
         con.conexionDB();

         String strQry = " SELECT CG_RAZON_SOCIAL  as nombrePYME   FROM comcat_pyme where ic_pyme =  ?   ";
			lVarBind		= new ArrayList();
			lVarBind.add(ic_pyme);
        	ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next() && rs!=null){
				nombrePYME= (rs.getString("nombrePYME")==null?"-1":rs.getString("nombrePYME"));
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getNombrePyme (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getNombrePyme (S)");
      return nombrePYME;
    }

		/**
	 * Obtengo el correo que se parametriza en la pantalla    Fodea 05-2014
	 * Administraci�n / Parametrizaci�n / Otros Par�metros
	 * E-mail Notificaciones Nafin
	 * @return
	 */
	private static  String getCorreoNotiNafin (){
      AccesoDBOracle 	      con   =  new AccesoDBOracle();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;
      List lVarBind		= new ArrayList();
		log.info("getCorreoNotiNafin (E)");
		String correoNafin ="";

		 try{
         con.conexionDB();

         String strQry = "  SELECT cg_email_notificaciones_nafin  as correo " +
			"   FROM comcat_producto_nafin   " +
			"  WHERE ic_producto_nafin = ?  ";

			lVarBind		= new ArrayList();
			lVarBind.add("1");
        	ps = con.queryPrecompilado(strQry,lVarBind );
         rs = ps.executeQuery();

         if(rs.next() && rs!=null){
				correoNafin= (rs.getString("correo")==null?"":rs.getString("correo"));
         }
			rs.close();
       	ps.close();

      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getCorreoNotiNafin (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
      }
      log.info("getCorreoNotiNafin (S)");
      return correoNafin;
    }

    //METODOS QUE NO PERTENECEN A LA OPERACION DE NAFIN
    public AceptacionPymeBean(){}


	/**
		 * //F09-2015
		 * @throws com.netro.exception.NafinException
		 * @return 
		 */
		public String getDiasInhabilesXanio()   throws NafinException{
			log.info(" getDiasInhabilesXanio (E)");
			AccesoDB    con = null;
			String      qrySentencia = "";
			int         numVencidos = 0;
			boolean     ok = true;
			ResultSet   rs =  null;
			String      retorno = "";
			try{
				
				con = new AccesoDB();
				con.conexionDB();
				
				qrySentencia = " SELECT   TO_CHAR (df_dia_inhabil, 'dd/mm/yyyy') AS fecha  "+
						 "  FROM comcat_dia_inhabil "+
						 "  WHERE df_dia_inhabil IS NOT NULL  "+
						 "  order by df_dia_inhabil asc ";
						 
				
				log.debug("qrySentencia::: "+qrySentencia);
				
				rs = con.queryDB(qrySentencia);
				while(rs.next()){
					if(!"".equals(retorno))
						retorno += ",";
						retorno += rs.getString(1);
				}
				con.cierraStatement();
			}catch(Exception e){
				log.error("getDiasInhabilesXanio Excepcion "+e);
				throw new NafinException("SIST0001");
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
			log.info(" getDiasInhabilesXanio (S)");
			  }
		return retorno;
		}



	/**
		 * 
		 * @throws com.netro.exception.NafinException
		 * @return 
		 */
		public boolean getEsDiasInhabilesXanio(String fecha)   throws NafinException{ 
			log.info(" getEsDiasInhabilesXanio (E)");
			AccesoDB    con = null;     
			boolean     ok = true;
			ResultSet   rs =  null;
			PreparedStatement ps    =  null;  
			boolean     diaInhabil = false;
			List  lVarBind    = new ArrayList();    
			try{
				
				con = new AccesoDB();
				con.conexionDB();
				
				String qrySentencia =   " SELECT   TO_CHAR (df_dia_inhabil, 'dd/mm/yyyy') AS fecha  "+
						 "  FROM comcat_dia_inhabil "+
						 "  WHERE df_dia_inhabil IS NOT NULL  "+
						 " and  df_dia_inhabil =  TO_date ( ? , 'dd/mm/yyyy') "+
						 "  order by df_dia_inhabil asc ";
						 
				lVarBind    = new ArrayList();         
				lVarBind.add(fecha);
				
				log.debug("qrySentencia::: "+qrySentencia);
				log.debug("lVarBind::: "+lVarBind);
				
				ps = con.queryPrecompilado(qrySentencia,lVarBind );   
				rs = ps.executeQuery();
			 
				if(rs!=null && rs.next()){
					diaInhabil = true;
				}
				rs.close();
				ps.close(); 
				
			}catch(Exception e){
				log.error("getEsDiasInhabilesXanio Excepcion "+e);
				throw new NafinException("SIST0001");
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
			log.info(" getEsDiasInhabilesXanio (S)");
			  }
		return diaInhabil;
		}		
		
	/**
	* Metodo que obtiene la cantidad de documentos del If  que estan en estatus Seleccionado PYME
	* del Modulo de Distribuidores
	* @throws netropology.utilerias.AppException
	*/
	public void  getDoctosNotifiIF_Distribuidores() throws AppException{          
	log.info("getDoctosNotifiIF_Distribuidores (E)");  
	
	 AccesoDB         con   =  new AccesoDB();
	List varBind = new ArrayList();
	StringBuffer   SQL = new StringBuffer();   
	
	StringBuffer contenido = new StringBuffer(); 
	
	String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
				"font-size: 10px; "+
				"font-weight: bold;"+
				"color: #FFFFFF;"+
				"background-color: #4d6188;"+
				"padding-top: 3px;"+
				"padding-right: 1px;"+
				"padding-bottom: 1px;"+
				"padding-left: 3px;"+
				"height: 25px;"+
				"border: 1px solid #1a3c54;'";
			
		String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
				"color:#000066; "+
				"padding-top:1px; "+
				"padding-right:2px; "+
				"padding-bottom:1px; "+
				"padding-left:2px; "+
				"height:22px; "+
				"font-size:11px;'";
	
		Correo correo = new Correo();
	String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
	String asunto =  "Documentos Seleccionada Pyme � Financiamiento a Distribuidores "+fechaActual;
	
	
	try {
	
		con.conexionDB();
		
		// Obtengo los IF parametrizados que tiene parametrizado la opci�n de  Activar Notificaci�n Doctos. �Seleccionada Pyme� por e-mail 
		SQL = new StringBuffer();   
		SQL.append( "  SELECT i.ic_if as IC_IF ,  "+
		" i.CG_RAZON_SOCIAL as NOMBRE_IF ,  "+
		"  r.CS_MAIL_NOTIFICA_DOCTOS as CORREO  "+
		"  FROM comrel_producto_if  r,  comcat_if i  "+
		"  WHERE  r.ic_if = i.ic_if  "+
		" and r.ic_producto_nafin = 4  "+
		"  and r.CG_NOTIFICA_DOCTOS = 'S'  "+
		" order by  i.ic_if asc "); 

		
		PreparedStatement ps1 = con.queryPrecompilado(SQL.toString() );   
		ResultSet   rs1 = ps1.executeQuery();          
				
		while(rs1.next() ){   
				String ic_if =  (rs1.getString("IC_IF")==null)?"":rs1.getString("IC_IF");
				String nombreIF =  (rs1.getString("NOMBRE_IF")==null)?"":rs1.getString("NOMBRE_IF");         
				String email =  (rs1.getString("CORREO")==null)?"":rs1.getString("CORREO");         
		
			contenido = new StringBuffer(); 
			contenido.append( "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'>"+                     
									" <BR> <b>"+nombreIF + " </b>  <BR> "+
									" <BR> Se le informa que la(s) siguiente(s) Cadena(s) Productiva(s), cuenta(s) con documento(s) en estatus <b>Seleccionada Pyme</b>  para Financiamiento a Distribuidores : <BR>"+           
									" <BR>&nbsp;&nbsp; </BR>"+
									" <table border=\"1\">"+
									"  <tr>" +
									"     <td align=\"center\" "+styleEncabezados+">Cadena Productiva</td>" +
									"     <td align=\"center\" "+styleEncabezados+">Total Documentos</td>" +
									"  </tr>");
			
			//Obtengo las EPo que  que tiene    documentos en Seleccionada Pyme  
			SQL = new StringBuffer();   
			
			SQL.append( "  SELECT  distinct  a.NOMBRE_EPO  AS  NOMBRE_EPO  , sum( a.TOTAL )   AS TOTAL   FROM  "+          
							"( select distinct  e.CG_RAZON_SOCIAL as NOMBRE_EPO , count(*) AS TOTAL   "+
							" from dis_documento d,    "+
							" dis_docto_seleccionado  s ,   "+
							" comcat_epo e, "+
							" com_linea_credito lc "+
							" where  d.ic_documento = s.ic_documento  "+ 
							" and d.ic_epo = e.ic_epo "+
							" and d.ic_estatus_docto = ? "+
							" and lc.ic_if = ? "+
							" and d.ic_linea_credito = lc.ic_linea_credito "+
							" and lc.ic_estatus_linea = ? "+
							"  group by e.CG_RAZON_SOCIAL   "+
							" UNION ALL  "+
							" select distinct e.CG_RAZON_SOCIAL as NOMBRE_EPO , count(*) AS TOTAL  "+
							" from  dis_documento d,    "+
							" dis_docto_seleccionado  s ,   "+
							" comcat_epo e, "+
							"  dis_linea_credito_dm lc "+
							" where  d.ic_documento = s.ic_documento "+
							" and d.ic_epo = e.ic_epo "+
							" and d.ic_estatus_docto = ? "+
							" and lc.ic_if =? "+
							" and d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
							" and lc.ic_estatus_linea = ?  "+
							" group by e.CG_RAZON_SOCIAL  "+
							" ) a group by a.NOMBRE_EPO " );
							
			varBind = new ArrayList();
			varBind.add("3");
			varBind.add(ic_if);
			varBind.add("12");
			varBind.add("3");
			varBind.add(ic_if);
			varBind.add("12");
			
			log.debug("SQL " +SQL);
			log.debug("varBind " +varBind);
			
				
			PreparedStatement ps2 = con.queryPrecompilado(SQL.toString(),varBind ); 
			ResultSet   rs2 = ps2.executeQuery();
			int hayDocumentos = 0;
			
			while(rs2.next()){   
				String nombreEPO =  (rs2.getString("NOMBRE_EPO")==null)?"":rs2.getString("NOMBRE_EPO");
				String total =  (rs2.getString("TOTAL")==null)?"":rs2.getString("TOTAL");        
				hayDocumentos++;
				
				contenido.append(  "<tr> " +   
										 " <td align=\"left\" "+style+"> " + nombreEPO +"</td>" +    
										 " <td align=\"center\" "+style+"> " + total +"</td>" +
										 " </tr> ");
			}
			rs2.close();
			ps2.close();  
			
			contenido.append("</table>");       
			
			contenido.append( " <BR>Para mayor detalle, ingrese al portal de NAFINSA en la secci�n de Cadenas Productivas. <BR>" +
				 " <BR> Atte. Nacional Financiera, S.N.C.");
			
			 
		if(hayDocumentos>0) {               
				correo.enviarTextoHTML("no_response@nafin.gob.mx",email,asunto, contenido.toString());
				System.out.println("Notificaci�n doctos Seleccionada Pyme exitosa.");               
			}else{
				System.out.println("No hay documentos en estatus Seleccionada Pyme a notificar para ning�n IF. ");
			}
				 
		}
		rs1.close();
		ps1.close();   
		
	} catch (Exception e) {
		con.terminaTransaccion(false);
		System.out.println("Exception: " + e.getCause());// Debug info
		e.printStackTrace();
	}finally{
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(true);
			con.cierraConexionDB();
		}
	} 
							
	log.info("getDoctosNotifiIF_Distribuidores (S)");                    
	
	}
	
	
	
}


