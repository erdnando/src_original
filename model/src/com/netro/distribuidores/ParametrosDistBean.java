package com.netro.distribuidores;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

@Stateless(name = "ParametrosDistEJB" , mappedName = "ParametrosDistEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ParametrosDistBean implements ParametrosDist {

  //Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ParametrosDistBean.class);


	public Vector getParamEpo(String ic_epo) {

		log.info("getParamEpo (E)");

		String qrySentencia      = "";
		String qryParame         = "";
		String qryModalidadPlazo = "";
		String valor             = "0";

		AccesoDB		con = null;
		ResultSet	rs1 = null;
		ResultSet	rs  = null;
		ResultSet	rs2 = null;

		Vector vecParametros  = new Vector();
		Vector ModalidadPlazo = new Vector();
		Vector parametros     = new Vector();

		log.info("ic_epo "+ic_epo);

    	try {
        con = new AccesoDB();
        con.conexionDB();
        qrySentencia =
          "SELECT cpe.cg_tipos_credito, cpe.cg_responsable_interes, "+
          "       cpe.cg_tipo_cobranza, cpe.ic_tipo_cobro_interes, "+
          "       cetf.ic_tipo_financiamiento, cpe.cs_negociable_a_baja, "+
			 "			cpe.cg_tipo_pago, cpe.CS_OPERA_FIRMA_MANC, "+
          "      cpe.CS_OPERA_SIN_CESION "+ //nuevo parametro 2017 jage
          "  FROM comrel_producto_epo cpe, comrel_epo_tipo_financiamiento cetf "+
          " WHERE cetf.ic_producto_nafin (+) = cpe.ic_producto_nafin "+
          "   AND cetf.ic_epo (+) = cpe.ic_epo "+
          "   AND cpe.ic_producto_nafin = 4 "+
          "   AND cpe.ic_epo = "+ic_epo;
        rs = con.queryDB(qrySentencia);

		  log.debug("--->qrySentencia nueva: "+qrySentencia);

        qryParame ="  select cc_parametro_epo, CG_VALOR from  com_parametrizacion_epo "+
                   " where CC_PARAMETRO_EPO in( " +
                   "'PUB_EPO_DESC_AUTOMATICO', " +
                   "'PUB_EPO_VENTA_CARTERA', " +
                   "'PUB_LIMITE_LINEA_CREDITO', " +
                   "'PUB_EPO_NO_NEGOCIABLES')" +
                   " and ic_epo = "+ic_epo;

        rs1 = con.queryDB(qryParame);
        while(rs1.next()) {
            parametros.addElement((rs1.getString("CG_VALOR")==null)?"N":rs1.getString("CG_VALOR"));
         }

        log.debug("qrySentencia"+qrySentencia);
        log.debug("qryParame"+qryParame);
        log.debug("parametros"+parametros);

        //query para traer las modalidades que se cargaron en la parametrizaci�n
        qryModalidadPlazo ="  select ic_tipo_financiamiento from comrel_epo_tipo_financiamiento   "+
               " WHERE ic_producto_nafin = 4  "+
               " AND ic_epo = "+ic_epo;

        rs2 = con.queryDB(qryModalidadPlazo);

        while(rs2.next()) 	{
           ModalidadPlazo.addElement((rs2.getString("ic_tipo_financiamiento")==null)?"":rs2.getString("ic_tipo_financiamiento"));
        }
        if(ModalidadPlazo.size()<1) {
          ModalidadPlazo.addElement(valor);
        }else if(ModalidadPlazo.size()==1) {
          ModalidadPlazo.addElement(valor);
          ModalidadPlazo.addElement(valor);
          ModalidadPlazo.addElement(valor);
        }else if(ModalidadPlazo.size()==2) {
         ModalidadPlazo.addElement(valor);
         ModalidadPlazo.addElement(valor);
        }else if(ModalidadPlazo.size()==3) {
         ModalidadPlazo.addElement(valor);
        }

        log.debug("ModalidadPlazo::  "+ModalidadPlazo);

        if(rs.next()) {
              vecParametros.addElement((rs.getString("CG_TIPOS_CREDITO")==null)?"":rs.getString("CG_TIPOS_CREDITO"));
              vecParametros.addElement((rs.getString("CG_RESPONSABLE_INTERES")==null)?"":rs.getString("CG_RESPONSABLE_INTERES"));
              vecParametros.addElement((rs.getString("CG_TIPO_COBRANZA")==null)?"":rs.getString("CG_TIPO_COBRANZA"));
              vecParametros.addElement((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));
              vecParametros.addElement((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
              vecParametros.addElement(parametros);
              vecParametros.addElement(ModalidadPlazo);
				  vecParametros.addElement((rs.getString("CS_NEGOCIABLE_A_BAJA")==null)?"":rs.getString("CS_NEGOCIABLE_A_BAJA"));
				  vecParametros.addElement((rs.getString("CG_TIPO_PAGO")==null)?"":rs.getString("CG_TIPO_PAGO"));		//Fodea 13-2014
				  vecParametros.addElement((rs.getString("CS_OPERA_FIRMA_MANC")==null)?"N":rs.getString("CS_OPERA_FIRMA_MANC")); //Fodea 32-2014
				  vecParametros.addElement((rs.getString("CS_OPERA_SIN_CESION")==null)?"":rs.getString("CS_OPERA_SIN_CESION")); //NUEVO PARAMETRO 2017 JAGE
        }
        else {
			vecParametros.addElement("");
			vecParametros.addElement("");
			vecParametros.addElement("");
			vecParametros.addElement("");
			vecParametros.addElement("");
			vecParametros.addElement("");
			vecParametros.addElement("");	//Fodea 13-2014
			vecParametros.addElement(""); //Fodea 13-2014
			vecParametros.addElement(""); //Fodea 13-2014
			vecParametros.addElement(""); //Fodea 32-2014
			vecParametros.addElement("");//Nuevo parametros JAGE
        }
        con.cierraStatement();
      }catch(Exception e){
      			con.terminaTransaccion(false);
          System.out.println("Exception.Parametros::ConsultaParametros: "+e);
      } finally {
    			con.cierraConexionDB();
      }
        log.info("getParamEpo (S)");
        return vecParametros;
    }

    public Vector getParamEpoa(String ic_epo) {
        log.info("ParametrosDistBean::getParamEpo(E)");
        Vector              vecParametros = new Vector();
        String              qrySentencia  = "";
        ResultSet           rs = null;
        PreparedStatement   ps = null;
        AccesoDB            con = null;        
        try {
            con = new AccesoDB();
            con.conexionDB();
            qrySentencia =
            " SELECT p.ig_dias_minimo, p.ig_dias_maximo, p.cg_tipo_conversion, p.cs_cifras_control, "          +
            "        p.ig_plazo_dscto,  p.fn_porcentaje_dscto, e.cg_razon_social, p.cg_tipos_credito, "        +
            "        p.cg_tipo_cobranza, p.ig_dias_aviso_venc, p.cg_fecha_porc_desc,p.ig_dias_amp_fecha_venc, "+
            "        p.cs_envio_correo_elec, p.IG_DIAS_OPERACION, p.CG_TIPO_PAGO, p.fn_tasa_descuento,  IG_MESES_INTERES  "         +
            "   FROM comrel_producto_epo p, comcat_epo e "                                                     +
            "  WHERE p.ic_producto_nafin = ? "                                                                 +
            "    AND p.ic_epo = e.ic_epo "                                                                     +
            "    AND p.ic_epo = ? ";            
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.valueOf("4"));
            ps.setInt(2, Integer.valueOf(ic_epo));
            rs = ps.executeQuery();
            ps.clearParameters();
            log.debug("Consulta: " + qrySentencia );
            if (rs.next()) {
                /*00*/        vecParametros.addElement((rs.getString("IG_DIAS_MINIMO")         ==null)?"":rs.getString("IG_DIAS_MINIMO")         );
                /*01*/        vecParametros.addElement((rs.getString("IG_DIAS_MAXIMO")         ==null)?"":rs.getString("IG_DIAS_MAXIMO")         );
                /*02*/        vecParametros.addElement((rs.getString("CG_TIPO_CONVERSION")     ==null)?"":rs.getString("CG_TIPO_CONVERSION")     );
                /*03*/        vecParametros.addElement((rs.getString("CS_CIFRAS_CONTROL")      ==null)?"":rs.getString("CS_CIFRAS_CONTROL")      );
                /*04*/        vecParametros.addElement((rs.getString("IG_PLAZO_DSCTO")         ==null)?"":rs.getString("IG_PLAZO_DSCTO")         );
                /*05*/        vecParametros.addElement((rs.getString("FN_PORCENTAJE_DSCTO")    ==null)?"":rs.getString("FN_PORCENTAJE_DSCTO")    );
                /*06*/        vecParametros.addElement((rs.getString("CG_RAZON_SOCIAL")        ==null)?"":rs.getString("CG_RAZON_SOCIAL")        );
                /*07*/        vecParametros.addElement((rs.getString("CG_TIPOS_CREDITO")       ==null)?"":rs.getString("CG_TIPOS_CREDITO")       );
                /*08*/        vecParametros.addElement((rs.getString("CG_TIPO_COBRANZA")       ==null)?"":rs.getString("CG_TIPO_COBRANZA")       );
                /*09*/        vecParametros.addElement((rs.getString("IG_DIAS_AVISO_VENC")     ==null)?"":rs.getString("IG_DIAS_AVISO_VENC")     );
                /*10*/        vecParametros.addElement((rs.getString("cg_fecha_porc_desc")     ==null)?"":rs.getString("cg_fecha_porc_desc")     );
                /*11*/        vecParametros.addElement((rs.getString("ig_dias_amp_fecha_venc") ==null)?"":rs.getString("ig_dias_amp_fecha_venc") );
                /*12*/        vecParametros.addElement((rs.getString("cs_envio_correo_elec")   ==null)?"":rs.getString("cs_envio_correo_elec")   );
                /*13*/        vecParametros.addElement((rs.getString("IG_DIAS_OPERACION")      ==null)?"":rs.getString("IG_DIAS_OPERACION")      );
                /*14*/        vecParametros.addElement((rs.getString("CG_TIPO_PAGO")           ==null)?"":rs.getString("CG_TIPO_PAGO")           );
                /*15*/        vecParametros.addElement((rs.getString("fn_tasa_descuento")      ==null)?"":rs.getString("fn_tasa_descuento")      );
                /*16*/        vecParametros.addElement((rs.getString("IG_MESES_INTERES")      ==null)?"":rs.getString("IG_MESES_INTERES")      ); // F09-2015
            } else{
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
                vecParametros.addElement("");
            }
            rs.close();ps.close();
            //con.cierraStatement();
        } catch(Exception e) {
            log.error("ParametrosDistBean::getParamEpo(Exception) "+e);
            con.terminaTransaccion(false);
            e.printStackTrace();
        } finally { 
            try {
                if (rs!=null) rs.close();
                if (ps!=null) ps.close();
            } catch(Exception e) {
                log.error("ParametrosDistBean::getParamEpoa(Exception)" + e.getMessage(), e);
            }
            if(con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("ParametrosDistBean::getParamEpo(S)");
        }
        return vecParametros;
    }

	public Vector getParamIF(String ic_if) throws NafinException{
		Vector    vecParametros	= new Vector();
		String    qrySentencia	= "";
		ResultSet rs     			= null;
		AccesoDB  con				= null;
    	try {
			con = new AccesoDB();
			con.conexionDB();

			if (!("".equals(ic_if))) {
				qrySentencia =
				"SELECT cg_razon_social "+
				" FROM comcat_if "+
				" WHERE ic_if = "+ic_if+" ";
				rs = con.queryDB(qrySentencia);
				if(rs.next()){
					vecParametros.add((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
				}
				rs.close();
				con.cierraStatement();
				qrySentencia =
				"SELECT CG_TIPO_LIQUIDACION, FN_LIMITE_PUNTOS, CG_OPERA_TARJETA, CG_LINEA_CREDITO, CG_NOTIFICA_DOCTOS, CS_MAIL_NOTIFICA_DOCTOS "+
				" FROM COMREL_PRODUCTO_IF "+
				" WHERE IC_IF = "+ic_if+" "+
				" AND IC_PRODUCTO_NAFIN = 4 ";
				rs = con.queryDB(qrySentencia);
				if(rs.next()){
					vecParametros.add((rs.getString("CG_TIPO_LIQUIDACION")     ==null)?"" :rs.getString("CG_TIPO_LIQUIDACION"));     //1
					vecParametros.add((rs.getString("FN_LIMITE_PUNTOS")        ==null)?"" :rs.getString("FN_LIMITE_PUNTOS"));        //2
					vecParametros.add((rs.getString("CG_OPERA_TARJETA")        ==null)?"" :rs.getString("CG_OPERA_TARJETA"));        //3
					vecParametros.add((rs.getString("CG_LINEA_CREDITO")        ==null)?"" :rs.getString("CG_LINEA_CREDITO"));        //4
					vecParametros.add((rs.getString("CG_NOTIFICA_DOCTOS")      ==null)?"N":rs.getString("CG_NOTIFICA_DOCTOS"));      //5
					vecParametros.add((rs.getString("CS_MAIL_NOTIFICA_DOCTOS") ==null)?"" :rs.getString("CS_MAIL_NOTIFICA_DOCTOS")); //6
				}
				rs.close();
				con.cierraStatement();
			}
		} catch(Exception e){
			con.terminaTransaccion(false);
			System.out.println("Exception.Parametros::ConsultaParametros: "+e);
			throw new NafinException("SIST0001");
		} finally {
			con.cierraConexionDB();
		}
		return vecParametros;
	}

  	public   Vector  getParamGral()
  	{
		Vector vecParametros = new Vector();
    String		qrySentencia	= "";
    ResultSet	rs						= null;
    AccesoDB 	con						= null;
    	try {
      	con = new AccesoDB();
        con.conexionDB();


        qrySentencia =" SELECT " +
				      				" cg_tipos_credito, " +
							      	" cg_responsable_interes, " +
								      " cg_tipo_cobranza, " +
								      " ic_tipo_cobro_interes  " +

								    " from comcat_producto_nafin "+

								     " WHERE ic_producto_nafin = 4 ";


						rs = con.queryDB(qrySentencia);

						if(rs.next())
							{
              vecParametros.addElement((rs.getString("CG_TIPOS_CREDITO")==null)?"":rs.getString("CG_TIPOS_CREDITO"));
              vecParametros.addElement((rs.getString("CG_RESPONSABLE_INTERES")==null)?"":rs.getString("CG_RESPONSABLE_INTERES"));
              vecParametros.addElement((rs.getString("CG_TIPO_COBRANZA")==null)?"":rs.getString("CG_TIPO_COBRANZA"));
              vecParametros.addElement((rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES"));

              }
            else
            	{
              vecParametros.addElement("");
							vecParametros.addElement("");
              vecParametros.addElement("");
							vecParametros.addElement("");
              }
          con.cierraStatement();
      }catch(Exception e){
      			con.terminaTransaccion(false);
          System.out.println("Exception.Parametros::ConsultaParametros: "+e);
      } finally {
    			con.cierraConexionDB();
      }
		return vecParametros;
    }

  	public   Vector  getParamGrala()
  	{
	Vector vecParametros = new Vector();
    String		qrySentencia	= "";
    ResultSet	rs						= null;
    AccesoDB 	con						= null;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia =
			" SELECT in_dias_minimo, "   +
			"        in_dias_maximo, "   +
			"        cg_tipo_conversion, "   +
			"        cs_cifras_control, "   +
			"        fn_minimo_financiamiento, "   +
			"        ig_dias_aviso_venc, "   +
			"        ig_dias_amp_fecha_venc, "   +
			"        cs_envio_correo_elec "   +
			"   FROM comcat_producto_nafin"   +
			"  WHERE ic_producto_nafin = 4"  ;
			rs = con.queryDB(qrySentencia);
		if(rs.next()){
/*0*/			vecParametros.addElement((rs.getString("IN_DIAS_MINIMO")==null)?"":rs.getString("IN_DIAS_MINIMO"));
/*1*/			vecParametros.addElement((rs.getString("IN_DIAS_MAXIMO")==null)?"":rs.getString("IN_DIAS_MAXIMO"));
/*2*/			vecParametros.addElement((rs.getString("CG_TIPO_CONVERSION")==null)?"":rs.getString("CG_TIPO_CONVERSION"));
/*3*/			vecParametros.addElement((rs.getString("CS_CIFRAS_CONTROL")==null)?"":rs.getString("CS_CIFRAS_CONTROL"));
/*4*/			vecParametros.addElement((rs.getString("FN_MINIMO_FINANCIAMIENTO")==null)?"":rs.getString("FN_MINIMO_FINANCIAMIENTO"));
/*5*/			vecParametros.addElement((rs.getString("IG_DIAS_AVISO_VENC")==null)?"":rs.getString("IG_DIAS_AVISO_VENC"));
/*6*/			vecParametros.addElement((rs.getString("ig_dias_amp_fecha_venc")==null)?"":rs.getString("ig_dias_amp_fecha_venc"));
/*7*/			vecParametros.addElement((rs.getString("cs_envio_correo_elec")==null)?"":rs.getString("cs_envio_correo_elec"));

		}else{
/*0*/			vecParametros.addElement("");
/*1*/			vecParametros.addElement("");
/*2*/			vecParametros.addElement("");
/*3*/			vecParametros.addElement("");
/*4*/			vecParametros.addElement("");
/*5*/			vecParametros.addElement("");
/*6*/			vecParametros.addElement("");
/*7*/			vecParametros.addElement("");
		}
		con.cierraStatement();
      }catch(Exception e){
			con.terminaTransaccion(false);
			System.out.println("Exception.Parametros::ConsultaParametrosa: "+e);
      } finally {
    			con.cierraConexionDB();
      }
		return vecParametros;
    }

	public int vmodificaParamGrales(int diasmin, int plazomax, String tipoconv, String control,String plazominlc, String proxvenc, String diasFechaVenc, String envCorreo){
		System.out.println("Parametros::vmodificaParamGrales (entra)");
		int actualiza ;
		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			String lsQry  = "";
			lsQry  = " update comcat_producto_nafin   set  "+
					"  IN_DIAS_MINIMO     =  " + diasmin +
					" ,IN_DIAS_MAXIMO   =   " + plazomax+
					" ,CG_TIPO_CONVERSION       =   '" + tipoconv  +"'" +
					" ,CS_CIFRAS_CONTROL     =  '" + control  +"'" +
					" ,FN_MINIMO_FINANCIAMIENTO = "+plazominlc+
					" ,IG_DIAS_AVISO_VENC       = "+proxvenc+
					" ,ig_dias_amp_fecha_venc       = "+diasFechaVenc+
					" ,cs_envio_correo_elec       = '"+envCorreo+"' "+//se agrega por F029-2009
					"  where ic_producto_nafin = 4 ";

			//System.out.println("************\nvmodificaParamGrales: " + lsQry + "\n******************\n");
			con.ejecutaSQL(lsQry);
			actualiza=1;
			con.terminaTransaccion(true);
		}catch (Exception e) {
			con.terminaTransaccion(false);
			actualiza=0;
			System.out.println("Exception.Parametros::vmodificaParamGrales: "+e);
		}finally{
			con.cierraConexionDB();
			System.out.println("Parametros::vmodificaParamGrales(finally)");
		}
		return actualiza;
	}

	public int vmodificaParametros(String tipocred, String respint, String tipocob, int esqint  )
			//throws NafinException
	{

		System.out.println("Parametros::ActualizaParametros(entra)");
    int actualiza ;
		AccesoDB con = new AccesoDB();

		try{
			con.conexionDB();


		String lsQry  = "";

/*
			lsQry  = " update comcat_producto_nafin   set  "  ;
if (!tipocred.equals(""))
			lsQry +=	"  CG_TIPOS_CREDITO     =  '" + tipocred  +"'" ;
if (!respint.equals(""))
			 lsQry +=		", CG_RESPONSABLE_INTERES       =   '" + respint  +"'" ;
if (!tipocob.equals("")){
			 lsQry +=		" ,CG_TIPO_COBRANZA       =   '" + tipocob  +"'" ;
       }
if(esqint!= 0)
				lsQry +=	" ,IC_TIPO_COBRO_INTERES     =  " + esqint  ;
 else
    		//lsQry +=	", IC_TIPO_COBRO_INTERES     =  " + esqint  ;
				lsQry +=	"  where ic_producto_nafin =     4 "	    ;
*/

			lsQry  = " update comcat_producto_nafin   set  "  ;

			lsQry +=	"  CG_TIPOS_CREDITO     =  '" + tipocred  +"'" ;

			 lsQry +=		", CG_RESPONSABLE_INTERES       =   '" + respint  +"'" ;

			 lsQry +=		" ,CG_TIPO_COBRANZA       =   '" + tipocob  +"'" ;

if(esqint!= 0)
				lsQry +=	" ,IC_TIPO_COBRO_INTERES     =  " + esqint  ;
 //else
    		//lsQry +=	", IC_TIPO_COBRO_INTERES     =  " + esqint  ;
				lsQry +=	"  where ic_producto_nafin =     4 "	    ;

		System.out.println("************\nParametrosGral: " + lsQry + "\n******************\n");
		con.ejecutaSQL(lsQry);
    actualiza=1;
		con.terminaTransaccion(true);

		}catch (Exception e) {
			con.terminaTransaccion(false);
      actualiza=0;
			System.out.println("Exception.Parametros::ActualizaParametros: "+e);
		}finally{
			con.cierraConexionDB();
			System.out.println("Parametros::ActualizaParametros(finally)");
		}
    return actualiza;
	}
/**
   * Metodo para actualizar los parametros de la Epo
   * en  Financiamiento de Distribuidores
   * @return
   * @param ventaCartera
   * @param lineaCredito
   * @param noNegociables
   * @param desautomatico
   * @param ic_producto_nafin
   * @param modplazo
   * @param sepo
   * @param esqint
   * @param tipocob
   * @param respint
   * @param tipocred
   */
	public int vmodificaParamEpo(String tipocred, String respint, String tipocob, int esqint,  String sepo, String modplazo,int ic_producto_nafin, 
  String desautomatico,String noNegociables, String lineaCredito,String ventaCartera, String negociableabaja, String tipoPago, String firmaMancomunada,
  String mesesIntereses, String  cmbMeses, String bajocontrato)
			//throws NafinException
	{

	    log.info("Parametros::ActualizaParamEPO 1(entra)");
    
	    int actualiza ;
            String antRespInt	= "";
            String antTipoCred	= "";
	    AccesoDB con = new AccesoDB();
	    int total=0;
      int plazo =0;
		try{
		    con.conexionDB();
                    String lsQry  = "";

    lsQry =
    	"SELECT COUNT (*) "+
        "  FROM dis_documento "+
        " WHERE ic_estatus_docto IN (2,  3) "+
        "   AND ic_epo = "+sepo+" "+
        "   AND ic_producto_nafin = "+ic_producto_nafin+" ";

      System.out.println(lsQry);
      ResultSet rs = con.queryDB(lsQry);
      total=0;
      if (rs.next())
        total = rs.getInt(1);
      con.cierraStatement();
      lsQry = " select cg_responsable_interes,cg_tipos_credito"+
      		" from comrel_producto_epo"+
                " where ic_epo = "+sepo+
                " and ic_producto_nafin = "+ic_producto_nafin;
      log.debug("-----"+lsQry);         
      rs = con.queryDB(lsQry);
      if(rs.next()){
      	antRespInt = rs.getString("CG_RESPONSABLE_INTERES");
        antTipoCred = rs.getString("CG_TIPOS_CREDITO");
      }
      con.cierraStatement();
     // if (total>0&&(!antRespInt.equals(respint)||(!antTipoCred.equals(tipocred)&&!"A".equals(tipocred))) ) {
       //return 2;
        //  log.debug("paso por aqui:::   *");
			lsQry  =	"  UPDATE COMREL_PRODUCTO_EPO SET ";
			lsQry +=	"  CS_NEGOCIABLE_A_BAJA = '"   + negociableabaja  +"'";
			lsQry +=	", CG_TIPOS_CREDITO = '"       + tipocred         +"'";
			lsQry +=	", CG_RESPONSABLE_INTERES = '" + respint          +"'";
			lsQry +=	", CG_TIPO_COBRANZA  = '"      + tipocob          +"'";
			lsQry +=	", CG_TIPO_PAGO  = '"          + tipoPago         +"'";
			lsQry +=	", CS_OPERA_FIRMA_MANC  = '"   + firmaMancomunada +"'";	
		        lsQry +=	",CS_OPERA_SIN_CESION = '"+bajocontrato+"' ";

          if(esqint!= 0){
              lsQry +=	", IC_TIPO_COBRO_INTERES     =  '" + esqint +"'"  ;              
          }
			 lsQry +=	"  where ic_producto_nafin = "+ic_producto_nafin+
                              " and ic_epo = " + sepo ;
          
      log.debug("paso por aqui:::   ***");
      System.out.println("************\nActualizaParamEPO: " + lsQry + "\n******************\n");
      con.ejecutaSQL(lsQry);
     // }  
      
    lsQry =
      "SELECT COUNT (*) "+
      "  FROM comrel_epo_tipo_financiamiento "+
      " WHERE ic_epo = "+sepo+" "+
      "   AND ic_producto_nafin = 4 ";
      System.out.println(lsQry);
			rs = con.queryDB(lsQry);
      total=0;
      if (rs.next()) {
        total = rs.getInt(1);
      } 
            
      lsQry = " DELETE from comrel_epo_tipo_financiamiento " +
								" WHERE ic_producto_nafin = '4' "+
                " AND ic_epo = '"+sepo + "'";
      
      log.debug("lsQry:::   "+lsQry); 
      con.ejecutaSQL(lsQry);      
 
      String[] nuevo=new String[modplazo.length()]; 
       for(int i=0; i<modplazo.length(); i++) { 
             nuevo[i]=String.valueOf(modplazo.charAt(i)); 
            lsQry =      
              "INSERT INTO comrel_epo_tipo_financiamiento "+
              "            (ic_producto_nafin, ic_epo, ic_tipo_financiamiento) "+
              "     VALUES("+ic_producto_nafin+", "+sepo+", "+nuevo[i]+") ";
              
               con.ejecutaSQL(lsQry);
               log.debug("-----"+lsQry);               
         }  //for       
      
    ParaAutomaticoEpo(sepo, desautomatico,noNegociables,lineaCredito,ventaCartera, mesesIntereses);
	 
	 //esto es solo si la epo OPera Factoraje con recuerso
		if(tipocred.equals("F")) {
		
			String  qrySentencia = " update comrel_producto_epo "+
											  " set  ig_dias_maximo =30 "+
											  ", ig_dias_minimo =0 "+  
											  " where ic_epo = "+sepo+											  
											  " AND ic_producto_nafin = "+ic_producto_nafin;               
			PreparedStatement	ps = con.queryPrecompilado(qrySentencia);
			ps.executeUpdate();
			ps.close();  
			con.cierraStatement();
			
		}
		
			
		
		actualiza=1;
		con.terminaTransaccion(true);

		}catch (Exception e) {
			con.terminaTransaccion(false);
      actualiza=0;
			log.error("Exception.Parametros::ActualizaParamEPO: "+e);
		}finally{
			con.cierraConexionDB();
			log.info("Parametros::ActualizaParamEPO(finally)");
		}
    return actualiza;
	}


	/**
	 * metodo que actualiza los parametros de la epo
	 * en  Financiamiento de Distribuidores
	 * @return 
	 * @param ventaCartera
	 * @param lineaCredito
	 * @param noNegociables
	 * @param desautomatico
	 * @param ic_epo
	 */
		 private  String  ParaAutomaticoEpo(String ic_epo, String desautomatico, 
				 String noNegociables, String lineaCredito,String ventaCartera, String mesesIntereses)     {
	  
		 log.info("ParaAutomaticoEpo (E) ");    
		
		  String qrySentencia = "";
		 String qryInsert = "";
		 String qryUpdate = "";
		 
		  ResultSet  rs          = null;
		  AccesoDB   con            = null;
		 String valor = "";
		 PreparedStatement ps = null;
		 boolean transaccion = true;
		 
		 try {
			con = new AccesoDB();
			con.conexionDB();
			
			qrySentencia = " DELETE com_parametrizacion_epo Where "+
					  " CC_PARAMETRO_EPO in( " +
					  "'PUB_EPO_DESC_AUTOMATICO', " +
					  "'PUB_EPO_VENTA_CARTERA', " +
					  "'PUB_LIMITE_LINEA_CREDITO', " +
					  "'PUB_EPO_NO_NEGOCIABLES'  )" +
					  " AND IC_EPO = " + ic_epo;
					 
		  ps = con.queryPrecompilado(qrySentencia);
			  ps.executeUpdate();
			  ps.close();        

		  log.debug("qrySentencia"  +qrySentencia);
		  
		  if (desautomatico.equals("S")|| desautomatico.equals("N")) {   //Descuento Automatico       
				qryInsert = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)  "+
								 " VALUES("+ic_epo+", 'PUB_EPO_DESC_AUTOMATICO', '"+desautomatico+"'"+")";         
			  
				ps = con.queryPrecompilado(qryInsert);
				ps.executeUpdate();
				ps.close();
				 log.debug("desautomatico::   "+qryInsert);
		 }
		 
			if (noNegociables.equals("S")|| noNegociables.equals("N")) {    //Documentos No Negociables       
				qryInsert = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)  "+
								 " VALUES("+ic_epo+", 'PUB_EPO_NO_NEGOCIABLES', '"+noNegociables+"'"+")";         
			  
				ps = con.queryPrecompilado(qryInsert);
				ps.executeUpdate();
				ps.close();
				 log.debug("noNegociables::   "+qryInsert);
		 }
		 
		  if (lineaCredito.equals("S")|| lineaCredito.equals("N")) {    //Limite de Linea de Credito       
				qryInsert = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)  "+
								 " VALUES("+ic_epo+", 'PUB_LIMITE_LINEA_CREDITO', '"+lineaCredito+"'"+")";         
			  
				ps = con.queryPrecompilado(qryInsert);
				ps.executeUpdate();
				ps.close();
				 log.debug("lineaCredito::   "+qryInsert);
		 }
		 
		  if (ventaCartera.equals("S")|| ventaCartera.equals("N")) {    //venta de Cartera       
				qryInsert = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)  "+
								 " VALUES("+ic_epo+", 'PUB_EPO_VENTA_CARTERA', '"+ventaCartera+"'"+")";         
			  
				ps = con.queryPrecompilado(qryInsert);
				ps.executeUpdate();
				ps.close();
			  log.debug("ventaCartera::   "+qryInsert);
		 }

			con.cierraStatement();
		 }catch(Exception e){
					transaccion = false;
					 e.printStackTrace();     
			  log.error("ParaAutomaticoEpo "+e);
		 } finally {
			  if(con.hayConexionAbierta()){
				  con.terminaTransaccion(transaccion);
				  con.cierraConexionDB();
			  }
		 }
			log.info("ParaAutomaticoEpo (S) ");
		 return valor;
	  }
	
	public int vmodificaParamEpoa(int diasmin, int plazomax, String tipoconv, String control,int plazo, int porcent, String epo, int proxvenc )
			//throws NafinException
	{

		System.out.println("Parametros::vmodificaParamEpoa (entra)");
		int actualiza ;
		AccesoDB con = new AccesoDB();

		try{
			con.conexionDB();
			String lsQry  = "";

			lsQry  =
				" UPDATE comrel_producto_epo"   +
				"    SET ig_dias_minimo = "+diasmin+","   +
				"        ig_dias_maximo = "+plazomax+","   +
				"        cg_tipo_conversion = '"+tipoconv+"',"   +
				"        cs_cifras_control = '"+control+"',"   +
				"        ig_plazo_dscto = "+plazo+","   +
				"        fn_porcentaje_dscto = "+porcent+","   +
				"        ig_dias_aviso_venc = "+proxvenc+" "   +
				"  WHERE ic_producto_nafin = 4"   +
				"    AND ic_epo = "+epo+" "  ;
			System.out.println("************\nvmodificaParamEpoa: " + lsQry + "\n******************\n");
			con.ejecutaSQL(lsQry);
			actualiza=1;
			con.terminaTransaccion(true);
		}catch (Exception e) {
			con.terminaTransaccion(false);
      actualiza=0;
			System.out.println("Exception.Parametros::vmodificaParamEpoa: "+e);
		}

    finally{
			con.cierraConexionDB();
			System.out.println("Parametros::vmodificaParamEpoa(finally)");
		}
    return actualiza;
	}

	/**
	 * Modifica los datos de la pantalla Productos/Distribuidores/Capturas/Par�metros por EPO (Seccion 2).
	 * En cado de que la modalidad sea 2 y el tipo de pago sea H o TC, los cambios tambien se registran en bit�cora.
	 * @return
	 * @param noAfiliacionComer
	 * @param nombreUsuario
	 * @param claveUsuario
	 * @param tipoPago
	 * @param tipoCred
	 * @param tasaDescuento
	 * @param diasoperacion
	 * @param txtEnvCorreo
	 * @param txtDiasFechaVenc
	 * @param cg_fecha_porc_desc
	 * @param proxvenc
	 * @param epo
	 * @param porcent
	 * @param plazo
	 * @param control
	 * @param tipoconv
	 * @param plazomax
	 * @param diasmin
	 */
	public int vmodificaParamEpoa(int diasmin, int plazomax, String tipoconv, String control,int plazo, double porcent, String epo, int proxvenc, String cg_fecha_porc_desc, 
	String txtDiasFechaVenc, String txtEnvCorreo, int diasoperacion, double tasaDescuento, String tipoCred, String tipoPago, String claveUsuario, 
	String nombreUsuario, String noAfiliacionComer, String mesesIntereses, String  cmbMeses )   { 
	
	   System.out.println("Parametros::vmodificaParamEpoa (entra)");
	   
	   int actualiza         = 0;
	   int nafinElec         = 0;                      //OBTIENE EL IC_NAFIN ELECTRONICO CUANDO SE REQUIERA INSERTAR EN BITACORA
	   StringBuffer consulta = new StringBuffer();     //QUERY PARA LOS DATOS QUE SE INSERTARAN EN BITACORA
	   StringBuffer consNaf  = new StringBuffer();     //QUERY PARA EL IC_NAFIN ELECTRONICO QUE SE INSERTARA EN BITACORA
	   StringBuffer insetBit = new StringBuffer();     //QUERY PARA INSERTAR EN BITACORA
	   AccesoDB con          = new AccesoDB();
	   ResultSet rs          = null;
	   PreparedStatement ps1 = null;
	   String cadenaAnt      = "";                     //CADENA DE DATOS A INSERTAR EN BITACORA
	   String cadenaAct      = "";                     //CADENA DE DATOS A INSERTAR EN BITACORA
	   
	   try{
	      con.conexionDB();
	      String lsQry  = "";
	      String condicion = "";
	      String condicionTasaDescuento = "";
	      
	      if(txtEnvCorreo!=null && !txtEnvCorreo.equals("")){
	         condicion = " ,cs_envio_correo_elec = '"+txtEnvCorreo+"' ";
	      }
	      
	      /***** Si es > 0 entonces se registra en bit�cora. Las consultas son para obtener los datos anteriores *****/ 
	      if(tasaDescuento > 0){
	         condicionTasaDescuento = " , FN_TASA_DESCUENTO = " + tasaDescuento;
	         
	         consulta.append(
	               "SELECT IC_EPO, CG_TIPOS_CREDITO, CG_TIPO_PAGO, FN_TASA_DESCUENTO "  +
	               "FROM COMREL_PRODUCTO_EPO " +
	               "WHERE IC_PRODUCTO_NAFIN = 4 AND IC_EPO = "+ epo
	         );
	         
	         rs = con.queryDB(consulta.toString());
	         if(rs.next()){
	            cadenaAnt = "IC_EPO= " + rs.getInt("IC_EPO") + " ,CG_TIPOS_CREDITO= " + rs.getString("CG_TIPOS_CREDITO") + 
	                        " ,CG_TIPO_PAGO= " + rs.getString("CG_TIPO_PAGO") + " ,FN_TASA_DESCUENTO= " + rs.getDouble("FN_TASA_DESCUENTO") ;
	         } else{
	            cadenaAnt = "";
	         }
	         System.out.println("Consulta (CADENA_ANTERIOR): " + consulta.toString());
	         
	         rs = null;
	         consNaf.append("SELECT IC_NAFIN_ELECTRONICO FROM COMREL_NAFIN WHERE IC_EPO_PYME_IF = "+ epo +" AND CG_TIPO =  'E'");
	         rs = con.queryDB(consNaf.toString());
	         if(rs.next()) {
	            nafinElec = rs.getInt("IC_NAFIN_ELECTRONICO");
	         } else{
	            nafinElec = 0;
	         }
	                     
	      }
	      
	      /***** Realiza el update normalmente *****/
	      lsQry =
	         " UPDATE COMREL_PRODUCTO_EPO "       +
	         "    SET IG_DIAS_MINIMO = "          + diasmin            + ","  +
	         "        IG_DIAS_MAXIMO = "          + plazomax           + ","  +
	         "        CG_TIPO_CONVERSION = '"     + tipoconv           + "'," +
	         "        CS_CIFRAS_CONTROL = '"      + control            + "'," +
	         "        IG_PLAZO_DSCTO = "          + plazo              + ","  +
	         "        FN_PORCENTAJE_DSCTO = "     + porcent            + ","  +
	         "        IG_DIAS_AVISO_VENC = "      + proxvenc           + ","  +
	         "        CG_FECHA_PORC_DESC = '"     + cg_fecha_porc_desc + "'," +
	         "        IG_DIAS_OPERACION = "       + diasoperacion      + ","  +
	         "        IG_DIAS_AMP_FECHA_VENC = '" + txtDiasFechaVenc   + "', "  +
	         "        IG_MESES_INTERES = '" + cmbMeses   + "' "  +          
	         condicion +
	         condicionTasaDescuento +
	         "  WHERE IC_PRODUCTO_NAFIN = 4" +
	         "    AND IC_EPO = " + epo;
	      System.out.println("************\nvmodificaParamEpoa: " + lsQry + "\n******************\n");
	      con.ejecutaSQL(lsQry);
	      actualiza=1;
	      con.terminaTransaccion(true);
	      
	      
	      //Fodea 092015 
	      if (mesesIntereses.equals("S") || mesesIntereses.equals("N")) {    
	       
	       PreparedStatement ps = null;
	       String qryDele = " DELETE com_parametrizacion_epo Where "+
	             " CC_PARAMETRO_EPO = 'CS_MESES_SIN_INTERESES' " + 
	             " AND IC_EPO = " + epo;
	            
	       ps = con.queryPrecompilado(qryDele);
	       ps.executeUpdate();
	       ps.close();        

	       
	        String qryInsert = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)  "+
	          " VALUES("+epo+", 'CS_MESES_SIN_INTERESES', '"+mesesIntereses+"'"+")";         
	       
	        ps = con.queryPrecompilado(qryInsert);
	        ps.executeUpdate();
	        ps.close();
	        log.debug("meses sin interese::   "+qryInsert);
	   }
	   
	      
	      
	      /***** Actualizo el  Numero de Afiliacion de Comercio *****/
	      setAfiliadoComercio(epo, noAfiliacionComer);
	      
	      /***** Si el update fue correcto y cunmple con las condiciones del if anterior, se procede a guardar en bit�cora *****/
	      if(tasaDescuento > 0 && actualiza == 1){
	         cadenaAct = "IC_EPO= " + epo + " ,CG_TIPOS_CREDITO= " + tipoCred +" ,CG_TIPO_PAGO= " + tipoPago + " ,FN_TASA_DESCUENTO= " + tasaDescuento;
	         
	         insetBit.append(
	            "INSERT INTO BIT_CAMBIOS_GRAL " + 
	            "(IC_CAMBIO, CC_PANTALLA_BITACORA, CG_CLAVE_AFILIADO, IC_NAFIN_ELECTRONICO, DF_CAMBIO, IC_USUARIO, CG_NOMBRE_USUARIO, CG_ANTERIOR, CG_ACTUAL, IC_EPO) " +
	            "VALUES (SEQ_BIT_CAMBIOS_GRAL.NextVal, 'PARAM_EPO', 'E', ?, SYSDATE, ?, ?, ?, ?, ?) " 
	         );
	         
	         try{
	            ps1 = con.queryPrecompilado(insetBit.toString());
	            ps1.setInt   (1, nafinElec          );
	            ps1.setString(2, claveUsuario       );
	            ps1.setString(3, nombreUsuario      );
	            ps1.setString(4, cadenaAnt          );
	            ps1.setString(5, cadenaAct          );
	            ps1.setString(6, epo                );
	            
	            ps1.executeUpdate(); 
	            con.terminaTransaccion(true);
	            System.out.println("Inserta Bit�cora: " + insetBit.toString());
	            
	         } catch(Exception e){
	            System.err.println("Exception.Parametros::vmodificaParamEpoa: (Error al insertar en bit�cora) " +  e);
	            if( ps1 != null ){ try{ ps1.close(); } catch(Exception e1){} }
	            con.terminaTransaccion(false);
	         }
	      }
	      
	   }catch (Exception e) {
	      con.terminaTransaccion(false);
	      actualiza=0;
	      System.out.println("Exception.Parametros::vmodificaParamEpoa: "+e);
	   }
	   finally{
	      if( ps1 != null ){ try{ ps1.close(); } catch(Exception e){} }
	      con.cierraConexionDB();
	      System.out.println("Parametros::vmodificaParamEpoa(finally)");
	   }
	   return actualiza;
	}

public int vmodificaEpoIF( String porccobra[], String mtobloq[], String porgaran[], String icif[] ,int elementos,String epo)
{

		System.out.println("Parametros::vmodificaEpoIF  (entra)");
    int actualiza ;
		AccesoDB con = new AccesoDB();

		try{
			con.conexionDB();

      for(int i=0;i<elementos;i++){

           String  porcentcob = porccobra[i];
           String  montobloq = mtobloq[i];
           String  porcentgar = porgaran[i];
           String  cveif = icif[i];

            String lsQry2 ="";
           // System.out.println("porcentcob: " + porcentcob);
            lsQry2 = "update comrel_if_epo_x_producto set " ;
            lsQry2 +=" fn_porc_cobranza_del = " + porcentcob ;
            lsQry2 +=", fn_monto_bloqueo_linea = " + montobloq ;
            lsQry2 +=", fn_porc_garantia = " + porcentgar ;
            lsQry2 +=" where ic_producto_nafin = 4 " ;
            lsQry2 +=" and ic_epo = " + epo  ;
            lsQry2 +=" and ic_if = " + cveif ;


             //porcentcob = (porccobra[i].equals(""))?"0":porccobra[i];

            //System.out.println("datos a update: " + porcentcob +", "+montobloq+", "+porcentgar+", "+cveif+" , " + epo);

        con.ejecutaSQL(lsQry2);
		System.out.println("************\nvmodificaEpoIF  segunda parte: " + lsQry2 + "\n******************\n");

      }

    actualiza=1;
		con.terminaTransaccion(true);

		}catch (Exception e) {
			con.terminaTransaccion(false);
      actualiza=0;
			System.out.println("Exception.Parametros::vmodificaEpoIF : "+e);
		}
    finally{
			con.cierraConexionDB();
			System.out.println("Parametros::vmodificaEpoIF(finally)");
		}
    return actualiza;
	}

  	public   Vector  getParamEpo_IF(String ic_epo)
  	{
		Vector vecParametros = new Vector();
		Vector vecIF = new Vector();
    String		qrySentencia	= "";
    ResultSet	rs						= null;
    AccesoDB 	con						= null;
    	try {
      	con = new AccesoDB();
        con.conexionDB();


        qrySentencia =" select I.cg_razon_social, X.fn_porc_cobranza_del, " +
                      " X.fn_monto_bloqueo_linea,X.fn_porc_garantia " +
                      "  ,sum(C.fn_monto_autorizado_total*decode(C.ic_moneda,1,1,54,TC.fn_valor_compra)) as fn_monto_autorizado_total" +
                      " ,R.ic_if, P.cg_tipos_credito, P.cg_tipo_cobranza" +
                      " from comrel_if_epo R, comcat_if I, comrel_if_epo_x_producto X, dis_linea_credito_dm C " +
                      " ,com_tipo_cambio TC ,comrel_producto_epo P "  +
                      " where R.cs_distribuidores ='S' " +
                      " and R.ic_if = I.ic_if " +
                      " and R.ic_if = X.ic_if " +
                      " and R.ic_epo = X.ic_epo " +
                      " and C.cg_tipo_solicitud in('I','R') " +
                      " and C.df_vencimiento_adicional > sysdate " +
                      " and R.ic_if = C.ic_if " +
                      " and R.ic_epo = C.ic_epo " +
                      " and R.ic_epo = P.ic_epo " +
                      " and P.ic_producto_nafin =4 " +
                      " and C.ic_moneda = TC.ic_moneda " +
                      " and TC.dc_fecha in(select max(dc_fecha) " +
                      " from com_tipo_cambio where ic_moneda = C.ic_moneda) " +
                      " and X.ic_producto_nafin =4 " +
                      " and C.ic_producto_nafin =4 " +
                      " and R.ic_epo = " + ic_epo +
                      " group by R.ic_if,R.ic_epo,I.cg_razon_social, " +
                      " X.fn_porc_cobranza_del,X.fn_monto_bloqueo_linea,X.fn_porc_garantia, P.cg_tipos_credito, P.cg_tipo_cobranza ";


            System.out.println(": "+qrySentencia);

						rs = con.queryDB(qrySentencia);

						while(rs.next())
							{
              vecParametros = new Vector();
              vecParametros.addElement((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
              vecParametros.addElement((rs.getString("fn_porc_cobranza_del")==null)?"":rs.getString("fn_porc_cobranza_del"));
              vecParametros.addElement((rs.getString("fn_monto_bloqueo_linea")==null)?"":rs.getString("fn_monto_bloqueo_linea"));
              vecParametros.addElement((rs.getString("fn_porc_garantia")==null)?"":rs.getString("fn_porc_garantia"));
              vecParametros.addElement((rs.getString("fn_monto_autorizado_total")==null)?"":rs.getString("fn_monto_autorizado_total"));
              vecParametros.addElement((rs.getString("ic_if")==null)?"":rs.getString("ic_if"));
              vecParametros.addElement((rs.getString("cg_tipos_credito")==null)?"":rs.getString("cg_tipos_credito"));
              vecParametros.addElement((rs.getString("cg_tipo_cobranza")==null)?"":rs.getString("cg_tipo_cobranza"));
              vecIF.addElement(vecParametros);
              }

          con.cierraStatement();
      }catch(Exception e){
      			con.terminaTransaccion(false);
          System.out.println("Exception.Parametros::getParamEpo_IF: "+e);
      } finally {
    			con.cierraConexionDB();
      }
		return vecIF;
    }

	public boolean updateParametros(String ic_if, String cg_tipo_solicitud, String ic_epo[], String autorizacion[], String fn_limite_puntos, String operaTarjeta, String cg_linea_c) throws NafinException {
	AccesoDB lobdConexion = new AccesoDB();
	String lsQry ="";
	String lsQry2 ="";
	boolean lbOK = true; 
		try {
			lobdConexion.conexionDB();
      lsQry =
        "UPDATE	COMREL_PRODUCTO_IF "+
        "   		SET CG_TIPO_LIQUIDACION = '"	+cg_tipo_solicitud+"' "+
        "   		, FN_LIMITE_PUNTOS = "			+fn_limite_puntos+
		  "   		, CG_OPERA_TARJETA = '"			+operaTarjeta+"' "+
		  "         , CG_LINEA_CREDITO  = '"+cg_linea_c+"'"+
        " WHERE	IC_IF = " 							+ic_if+
        "  			AND IC_PRODUCTO_NAFIN = 4 ";
      //System.out.println(lsQry);
			try{
				lobdConexion.ejecutaSQL(lsQry);
	if(ic_epo!=null){
	        for (int x=0;x<ic_epo.length;x++) {
	          lsQry2 =
	            "UPDATE comrel_if_epo_x_producto "+
	            "   SET cs_autorizado_tasa = '"+autorizacion[x]+"' "+
	            " WHERE ic_producto_nafin = 4 "+
	            "   AND ic_if = "+ic_if+" "+
                    "   AND ic_epo = "+ic_epo[x]+" ";
		//          System.out.println(lsQry2);
	          lobdConexion.ejecutaSQL(lsQry2);
	        }
        }

			} catch (SQLException sqle){
				lbOK = false;
				throw new NafinException("ANTI0022");
			}
	   	} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en actualizaComRelProductoIf(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
	return lbOK;
	}

  public Vector getEposxIf(String ic_if) 	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    Vector		  vecColumnas		= new Vector();
    Vector		  vecFilas			= new Vector();

    try{
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia =
        "SELECT ce.ic_epo, ce.cg_razon_social, iep.cs_autorizado_tasa "+
        "  FROM comcat_epo ce, comrel_if_epo_x_producto iep "+
        " WHERE ce.ic_epo IN (SELECT ic_epo "+
        "                       FROM dis_linea_credito_dm "+
        "                      WHERE cg_tipo_solicitud IN ('I',  'R') "+
        "                        AND TRUNC (df_vencimiento_adicional) >= TRUNC (SYSDATE) "+
        "                        AND ic_if = "+ic_if+") "+
        "   AND iep.ic_if = "+ic_if+" "+
        "   AND iep.ic_epo = ce.ic_epo "+
        "   AND iep.ic_producto_nafin = 4";
//      System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);

      while(rs.next()){
        vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString("ic_epo")==null)?"":rs.getString("ic_epo"));
				vecColumnas.addElement((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
				vecColumnas.addElement((rs.getString("cs_autorizado_tasa")==null)?"":rs.getString("cs_autorizado_tasa"));
      	vecFilas.addElement(vecColumnas);
      }
      con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
   return vecFilas;
  }//getEposxIf

	public Vector getParamDesctoAuto(String ic_pyme)
  		throws NafinException{
		AccesoDB			con           	= null;
		ResultSet			rs          	= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		Vector				vecColumnas		= new Vector();
		Vector				vecFilas		= new Vector();
		try{
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT   /*+index(pep CP_COMREL_PYME_EPO_X_PROD_PK) use_nl(e lc pe pep)*/"   +
				"        e.ic_epo, e.cg_razon_social, pep.cs_dscto_auto, pep.cs_moneda_auto, v.ic_linea_credito_dm"   +
				"   FROM comcat_epo e, comrel_pyme_epo pe, comrel_pyme_epo_x_producto pep,"   +
				"        (SELECT /*+index(lc CP_DIS_LINEA_CREDITO_DM_PK)*/ ic_epo, ic_linea_credito_dm"   +
				"           FROM dis_linea_credito_dm lc"   +
				"          WHERE lc.df_vencimiento_adicional > SYSDATE"   +
				"            AND (lc.fn_monto_autorizado_total - lc.fn_saldo_financiado - lc.fn_monto_x_autorizar) > 0"   +
				"            AND lc.cg_tipo_solicitud = 'I'"   +
				"            AND lc.ic_estatus_linea = 12"   +
				"            AND lc.ic_moneda = 54) v"   +
				"  WHERE pe.ic_epo = e.ic_epo"   +
				"    AND e.ic_epo = v.ic_epo (+)"   +
				"    AND pep.ic_epo = pe.ic_epo"   +
				"    AND pep.ic_pyme = pe.ic_pyme"   +
				"    AND e.cs_habilitado = 'S'"   +
				"    AND pe.ic_pyme = ?"   +
				"    AND pe.cs_distribuidores IN ('S',  'R')"   +
				"    AND pep.ic_producto_nafin = 4"  ;
//		System.out.println("\n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_pyme));
			rs = ps.executeQuery();
			while(rs.next()) {
				vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString("ic_epo")==null)?"":rs.getString("ic_epo"));
				vecColumnas.addElement((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
				vecColumnas.addElement((rs.getString("cs_dscto_auto")==null)?"":rs.getString("cs_dscto_auto"));
				vecColumnas.addElement((rs.getString("cs_moneda_auto")==null)?"":rs.getString("cs_moneda_auto"));
				vecColumnas.addElement((rs.getString("ic_linea_credito_dm")==null)?"":rs.getString("ic_linea_credito_dm"));
				vecFilas.addElement(vecColumnas);
			}
			rs.close();
			if(ps!=null) ps.close();
		}catch(Exception e){
			throw new NafinException("SIST0001");
		}finally{
		  if(con.hayConexionAbierta())
		    con.cierraConexionDB();
		}
		return vecFilas;
	}//getParamDesctoAuto

	public boolean setParamDesctoAuto(String ic_pyme,String cs_descto_auto,String monedaAutoN[],String monedaAutoD[])
			throws NafinException {
		System.out.println("ParamDistEJB::setParamDesctoAuto (E)");
		AccesoDB			con           	= null;
		String				qrySentencia	= "";
		PreparedStatement 	ps				= null;
		boolean lbOK = true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			StringTokenizer monedaEpoN;
			StringTokenizer monedaEpoD;
			int numElementos = monedaAutoN.length;
			for(int m=0;m<numElementos;m++){
				String ic_epo = "";
				String cs_moneda = "";
				String ic_epo_d = "";
				String cs_moneda_d = "";

				monedaEpoN = new StringTokenizer(monedaAutoN[m],"|");
				monedaEpoD = new StringTokenizer(monedaAutoD[m],"|");

				if(monedaEpoN.hasMoreTokens()){
					cs_moneda	= monedaEpoN.nextToken().toString();
					ic_epo		= monedaEpoN.nextToken().toString();
				}//if(monedaEpo.hasMoreTokens())
				if(monedaEpoD.hasMoreTokens()){
					cs_moneda_d	= monedaEpoD.nextToken().toString();
					ic_epo_d	= monedaEpoD.nextToken().toString();
				}//if(monedaEpo.hasMoreTokens())

				if("XN".equals(cs_moneda)&&"XD".equals(cs_moneda_d))
					cs_moneda = "";
				else  if("XN".equals(cs_moneda)&&"D".equals(cs_moneda_d))
					cs_moneda = "D";
				else if("N".equals(cs_moneda)&&"XD".equals(cs_moneda_d))
					cs_moneda = "N";
				else if("N".equals(cs_moneda)&&"D".equals(cs_moneda_d))
					cs_moneda = "A";

				qrySentencia =
					" UPDATE comrel_pyme_epo_x_producto"   +
					"    SET cs_moneda_auto = ?"   +
					"		 ,cs_dscto_auto = ?"   +
					"  WHERE ic_pyme = ?"   +
					"    AND ic_epo = ?"   +
					"    AND ic_producto_nafin = 4"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,cs_moneda);
				if(!"".equals(cs_descto_auto))
					ps.setString(2,cs_descto_auto);
				else
					ps.setNull(2, Types.VARCHAR);
				ps.setInt(3,Integer.parseInt(ic_pyme));
				ps.setInt(4,Integer.parseInt(ic_epo));
				ps.execute();
			}//for(int m=0;m<numElementos;m++)

		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en setParamDesctoAuto(). "+e);
			throw new NafinException("SIST0001");
		}
		finally	{
			con.terminaTransaccion(lbOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParamDistEJB::setParamDesctoAuto (S)");
		}
		return lbOK;
	}

	//**************
	//
	//		boolean insertaClasificacion()
	//		Inserta clasificaciones de clientes. Agregado 05/03/04
	//
	//***************

	public boolean insertaClasificacion(String clave,String epo,int prodnaf,String desc, String plazo)
		throws NafinException {

		String qrySentencia = null;
		boolean stat 		= true;

		System.out.println("Afiliacion::insertaClasificacion(E)");

		AccesoDB con  = new AccesoDB();

		try {

			con.conexionDB();

			qrySentencia = " INSERT into comrel_clasificacion " +
							" (IC_CLASIFICACION,CG_CLAVE,IC_EPO,IC_PRODUCTO_NAFIN,CG_DESCRIPCION, ig_plazo_aut) " +
							" SELECT NVL (MAX (ic_clasificacion), 0) + 1, '" + clave + "'," + epo + "," + prodnaf +",'" + desc + "'," + ((plazo.equals(""))?null:plazo) +
          					" FROM comrel_clasificacion ";

			System.out.println("qrySentencia: " + qrySentencia);

			con.ejecutaSQL(qrySentencia);

			System.out.println("qrySentencia ejecutada");

		} catch(Exception e) {
			stat = false;
			System.out.println("AfiliacionBean::insertaClasificacion() "+e.getMessage());
			throw new NafinException("DSCT0010");
		} finally {
           	con.terminaTransaccion(stat);
           	if(con.hayConexionAbierta())
           		con.cierraConexionDB();
			System.out.println("Afiliacion::insertaClasificacion(S)");
      }
      return stat;
	} // fin insertaClasificacion()

	//**************
	//
	//		boolean eliminaClasificacion()
	//		Elimina clasificaciones de clientes. Agregado 05/03/04
	//
	//***************

	public boolean eliminaClasificacion(String clave,String epo,int prodnaf,int flag)
		throws NafinException {

		String qrySentencia = null;
		boolean stat 		= true;

		System.out.println("Afiliacion::eliminaClasificacion(E)");

		AccesoDB con  = new AccesoDB();

		try {

			con.conexionDB();

			qrySentencia = " UPDATE comrel_pyme_epo_x_producto " +
							" SET ic_clasificacion = null " +
							" WHERE ic_epo = " + epo +
							" AND ic_producto_nafin = " + prodnaf;

			if (flag == 1)
				qrySentencia +=	" AND ic_clasificacion = "+ clave;
			else
				qrySentencia += " AND ic_pyme = " + clave;


			System.out.println("update qrySentencia: " + qrySentencia);
			con.ejecutaSQL(qrySentencia);

			if (flag == 1){
				qrySentencia = " DELETE from dis_plazo_descuento " +
								" WHERE ic_clasificacion = '"+ clave + "'";

				System.out.println("delete 1 qrySentencia: " + qrySentencia);
				con.ejecutaSQL(qrySentencia);

				qrySentencia = " DELETE from comrel_clasificacion " +
								" WHERE ic_clasificacion = '"+ clave + "'";

				System.out.println("delete 2 qrySentencia: " + qrySentencia);
				con.ejecutaSQL(qrySentencia);
			}

		} catch(Exception e) {
			stat = false;
			System.out.println("AfiliacionBean::eliminaClasificacion() "+e.getMessage());
			throw new NafinException("DSCT0010");
		} finally {
           	con.terminaTransaccion(stat);
           	if(con.hayConexionAbierta())
           		con.cierraConexionDB();
			System.out.println("Afiliacion::eliminaClasificacion(S)");
      }
      return stat;
	} // fin eliminaClasificacion()

	//**************
	//
	//		boolean cambiaClasificacion()
	//		Modifica clasificaciones de clientes. Agregado 05/03/04
	//
	//***************

	public boolean cambiaClasificacion(String idclas,String clave,String desc, String plazo)
		throws NafinException {

		String qrySentencia = null;
		boolean stat 		= true;

		System.out.println("Afiliacion::cambiaClasificacion(E)");

		AccesoDB con  = new AccesoDB();

		try {

			con.conexionDB();

			qrySentencia = " UPDATE comrel_clasificacion " +
							" SET cg_clave = '" + clave + "'," +
							"		cg_descripcion = '" + desc + "', " +
							"	 	ig_plazo_aut= " + plazo +
							" WHERE ic_clasificacion = "+ idclas;

			System.out.println("update qrySentencia: " + qrySentencia);
			con.ejecutaSQL(qrySentencia);

		} catch(Exception e) {
			stat = false;
			System.out.println("AfiliacionBean::cambiaClasificacion() "+e.getMessage());
			throw new NafinException("DSCT0010");
		} finally {
           	con.terminaTransaccion(stat);
           	if(con.hayConexionAbierta())
           		con.cierraConexionDB();
			System.out.println("Afiliacion::cambiaClasificacion(S)");
      }
      return stat;
	} // fin cambiaClasificacion()



	//**************
	//
	//		boolean relacionaClasificacion()
	//		Relaciona los clientes con una clasificaci�n. Agregado 08/03/04
	//
	//***************

	public boolean relacionaClasificacion(String epo, String pyme, int prodnaf, String idclas, String ig_dias_maximo)
		throws NafinException {

		String 		qrySentencia	= null;
		ResultSet   rs				= null;
		boolean stat 		= true;

		System.out.println("Afiliacion::relacionaClasificacion(E)");

		AccesoDB con = new AccesoDB();

		try {

			con.conexionDB();

			qrySentencia =
				" UPDATE comrel_pyme_epo_x_producto"   +
				"    SET ic_clasificacion = "+idclas+","   +
				"        ig_dias_maximo = "+ig_dias_maximo+" "   +
				"  WHERE ic_producto_nafin = 4"   +
				"    AND ic_epo = "+epo+" "   +
				"    AND ic_pyme = "+pyme+" "  ;
			System.out.println("update qrySentencia: " + qrySentencia);
			con.ejecutaSQL(qrySentencia);

			qrySentencia =
				" SELECT ic_documento, to_char(df_fecha_emision,'dd/mm/yyyy') as fechaemision"   +
				"   FROM dis_documento"   +
				"  WHERE ic_estatus_docto = 2"   +
				"    AND cs_plazo_descuento = 'N'"+
				"    AND ic_epo = "+epo+" "+
				"    AND ic_pyme = "+pyme+" ";
			System.out.println("\nqrySentencia: "+qrySentencia);
			rs = con.queryDB(qrySentencia);
			while(rs.next()){
				String n_plazo = "0";
				String n_porc = "0";
				String rs_documento = rs.getString("ic_documento")==null?"":rs.getString("ic_documento");
				String rs_fecha_emision = rs.getString("fechaemision")==null?"":rs.getString("fechaemision");
				ResultSet rs1 = null;
				qrySentencia =
					" SELECT ig_plazo_final, fg_porcentaje"   +
					"   FROM comrel_pyme_epo_x_producto pep, dis_plazo_descuento pds"   +
					"  WHERE pep.ic_clasificacion = pds.ic_clasificacion"   +
					"    AND (TRUNC (SYSDATE) - TRUNC (TO_DATE ('"+rs_fecha_emision+"', 'dd/mm/yyyy'))) BETWEEN ig_plazo_inicial AND ig_plazo_final"   +
					"    AND pep.ic_pyme = "+pyme+" "   +
					"    AND pep.ic_epo = "+epo+" "  ;
				System.out.println(qrySentencia);
				rs1 = con.queryDB(qrySentencia);
				if(rs1.next()){
					n_plazo	= rs1.getString("ig_plazo_final")==null?"":rs1.getString("ig_plazo_final");
					n_porc	= rs1.getString("fg_porcentaje")==null?"":rs1.getString("fg_porcentaje");
				}
				rs1.close();
				qrySentencia =
					" UPDATE dis_documento"   +
					"    SET ig_plazo_descuento = "+n_plazo+","   +
					"        fn_porc_descuento = "+n_porc+", "   +
					"        cs_plazo_descuento = 'S' "   +
					"  WHERE ic_documento = "+rs_documento+" "  ;
				System.out.println(qrySentencia);
				con.ejecutaSQL(qrySentencia);
			}
			rs.close();
		} catch(Exception e) {
			stat = false;
			System.out.println("AfiliacionBean::relacionaClasificacion() "+e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
           	con.terminaTransaccion(stat);
           	if(con.hayConexionAbierta())
           		con.cierraConexionDB();
			System.out.println("Afiliacion::relacionaClasificacion(S)");
      }
      return stat;
	} // fin relacionaClasificacion()

//**************
	//
	//		boolean insertaPlazo()
	//		Inserta plazos de las clasificaciones de clientes. Agregado 09/03/04
	//
	//***************

	public boolean insertaPlazo(String idclas,String pi,String pf,String porc)
		throws NafinException {

		String qrySentencia = null;
		boolean stat 		= true;

		System.out.println("Afiliacion::insertaPlazo(E)");

		AccesoDB con  = new AccesoDB();

		try {

			con.conexionDB();

			qrySentencia = " INSERT into dis_plazo_descuento " +
							" (ic_plazo_descuento,ic_clasificacion,ig_plazo_inicial, " +
							" ig_plazo_final,fg_porcentaje) " +
							" SELECT NVL (MAX (ic_plazo_descuento), 0) + 1," + idclas + "," + pi + "," + pf +"," + porc + " " +
          					" FROM dis_plazo_descuento ";

			System.out.println("qrySentencia: " + qrySentencia);

			con.ejecutaSQL(qrySentencia);

			System.out.println("qrySentencia ejecutada");

		} catch(Exception e) {
			stat = false;
			System.out.println("AfiliacionBean::insertaPlazo() "+e.getMessage());
			throw new NafinException("DSCT0010");
		} finally {
           	con.terminaTransaccion(stat);
           	if(con.hayConexionAbierta())
           		con.cierraConexionDB();
			System.out.println("Afiliacion::insertaPlazo(S)");
      }
      return stat;
	} // fin insertaPlazo()

	//**************
	//
	//		boolean eliminaPlazo()
	//		Elimina plazos de las clasificaciones de clientes. Agregado 09/03/04
	//
	//***************

	public boolean eliminaPlazo(String clave)
		throws NafinException {

		String qrySentencia = null;
		boolean stat 		= true;

		System.out.println("Afiliacion::eliminaPlazo(E)");

		AccesoDB con  = new AccesoDB();

		try {

			con.conexionDB();

				qrySentencia = " DELETE from dis_plazo_descuento " +
								" WHERE ic_plazo_descuento = '"+ clave + "'";

				System.out.println("delete qrySentencia: " + qrySentencia);
				con.ejecutaSQL(qrySentencia);

		} catch(Exception e) {
			stat = false;
			System.out.println("AfiliacionBean::eliminaPlazo() "+e.getMessage());
			throw new NafinException("DSCT0010");
		} finally {
           	con.terminaTransaccion(stat);
           	if(con.hayConexionAbierta())
           		con.cierraConexionDB();
			System.out.println("Afiliacion::eliminaPlazo(S)");
      }
      return stat;
	} // fin eliminaPlazo()

	//**************
	//
	//		boolean modificaPlazo()
	//		Modifica plazos de las clasificaciones de clientes. Agregado 09/03/04
	//
	//***************

	public boolean modificaPlazo(String icplazo,String icplazo_ant,String icplazo_sig,
								String p_ini,String p_fin,String porc)
		throws NafinException {

		String qrySentencia = null;
		boolean stat 		= true;
		int ini = Integer.parseInt(p_ini);
		int fin = Integer.parseInt(p_fin);

		System.out.println("Afiliacion::modificaPlazo(E)");

		AccesoDB con  = new AccesoDB();

		try {

			con.conexionDB();

			qrySentencia = " UPDATE dis_plazo_descuento " +
							" SET 	ig_plazo_inicial = " + p_ini + "," +
							" 		ig_plazo_final = " + p_fin + "," +
							" 		fg_porcentaje = "  + porc +
							" WHERE ic_plazo_descuento = "+ icplazo;

			System.out.println("update 1 qrySentencia: " + qrySentencia);
			con.ejecutaSQL(qrySentencia);

			ini--;
			fin++;

			if(!icplazo_ant.equals("0")){
				qrySentencia = " UPDATE dis_plazo_descuento " +
								" SET 	ig_plazo_final = " + ini +
								" WHERE ic_plazo_descuento = "+ icplazo_ant;

				System.out.println("update 2 qrySentencia: " + qrySentencia);
				con.ejecutaSQL(qrySentencia);
			}

			if(!icplazo_sig.equals("0")){
				qrySentencia = " UPDATE dis_plazo_descuento " +
								" SET 	ig_plazo_inicial = " + fin +
								" WHERE ic_plazo_descuento = "+ icplazo_sig;

				System.out.println("update 3 qrySentencia: " + qrySentencia);
				con.ejecutaSQL(qrySentencia);
			}

		} catch(Exception e) {
			stat = false;
			System.out.println("AfiliacionBean::modificaPlazo() "+e.getMessage());
			throw new NafinException("DSCT0010");
		} finally {
           	con.terminaTransaccion(stat);
           	if(con.hayConexionAbierta())
           		con.cierraConexionDB();
			System.out.println("Afiliacion::modificaPlazo(S)");
      }
      return stat;
	} // fin modificaPlazo()

	public int setLimitePyme(String ic_epo, String ic_pyme, String cg_num_distribuidor, String fg_limite_pyme,
											String cg_mensaje_terminacion, String nombreUsuario, String ic_usuario, String acuse,
											String recibo, String fg_limite_pyme_usd)
			throws NafinException {
		System.out.println("ParametrosDistBean::setLimitePyme(E)");
		AccesoDB 			con 			= new AccesoDB();
		String 				qrySentencia 	= "";
		List 				lVarBind 		= new ArrayList();
		List 				lDatos 			= new ArrayList();
		int					registros		= 0;
		boolean				bOK				= true;
		try {
			con.conexionDB();
			if("".equals(ic_pyme)) {
				qrySentencia =
					" SELECT ic_pyme"   +
					"   FROM comrel_pyme_epo"   +
					"  WHERE ic_epo = ?"   +
					"    AND cg_num_distribuidor = ?"  ;
				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(cg_num_distribuidor);
				lDatos = con.consultaRegDB(qrySentencia, lVarBind, false);
				ic_pyme = lDatos.get(0).toString();
			}
			qrySentencia =
				" UPDATE comrel_pyme_epo_x_producto"   +
				"    SET fg_limite_pyme = ? "+
				" ,fg_limite_pyme_usd = ? "+
				" ,cg_mensaje_terminacion = ?, ic_usuario = ?, cg_nombre_usuario = ? , df_fecha_hora_modificacion = sysdate	" +
				"  WHERE ic_producto_nafin = ?"   +
				"    AND ic_epo = ?"   +
				"    AND ic_pyme = ?"  ;
			lVarBind = new ArrayList();
			lVarBind.add(fg_limite_pyme);
			lVarBind.add(fg_limite_pyme_usd);
			lVarBind.add(cg_mensaje_terminacion);
			lVarBind.add(ic_usuario);
			lVarBind.add(nombreUsuario);
			lVarBind.add(new Integer(4));
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer(ic_pyme));

			System.out.println("qrySentencia"+qrySentencia);
			System.out.println("lVarBind "+lVarBind);
			registros = con.ejecutaUpdateDB(qrySentencia, lVarBind);

			qrySentencia = "insert into com_acuse4(cc_acuse,cg_recibo_electronico, ic_usuario, ic_producto_nafin, df_fecha_hora_carga, c_facultad) " +
				" values (?,?,?,?,sysdate,?)";
			lVarBind= new ArrayList();
			lVarBind.add(acuse);
			lVarBind.add(recibo);
			lVarBind.add(ic_usuario);
			lVarBind.add("4");
			lVarBind.add("24EPO24FORMA8");
			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e){
			bOK = false;
			System.out.println("ParametrosDistBean::setLimitePyme(Exception)"+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDistBean::setLimitePyme(S)");
		}
		return registros;
	}//getLimitePyme

	public List getLimitePyme(String ic_epo, String ic_pyme, String cg_num_distribuidor)
			throws NafinException {
		System.out.println("ParametrosDistBean::getLimitePyme(E)");
		AccesoDB 			con 			= new AccesoDB();
		String 				qrySentencia 	= "";
		List 				lVarBind 		= new ArrayList();
		List 				lDatos 			= new ArrayList();
		try {
			con.conexionDB();
			qrySentencia =
				" SELECT pep.ic_pyme, pym.cg_razon_social, pep.fg_limite_pyme,"   +
				"        NVL (pep.fg_limite_pyme_acum, 0) fg_limite_pyme_acum, pep.cg_mensaje_terminacion mensaje, " +
				"		 to_char(pep.df_fecha_hora_modificacion,'dd/mm/yyyy hh:mi:ss') fecha, pep.ic_usuario icusuario, pep.cg_nombre_usuario nombre,"   +
				"		'' ic_moneda, pep.fg_limite_pyme_usd,  NVL (pep.fg_limite_pyme_acum_usd, 0) fg_limite_pyme_acum_usd" +
				"   FROM comrel_pyme_epo_x_producto pep, comrel_pyme_epo cpe, comcat_pyme pym"   +
				"  WHERE pep.ic_epo = cpe.ic_epo"   +
				"    AND pep.ic_pyme = cpe.ic_pyme"   +
				"    AND cpe.ic_pyme = pym.ic_pyme"   +
				"    AND pep.fg_limite_pyme IS NOT NULL"   +
				"    AND pep.ic_producto_nafin = ?"   +
				"    AND pep.ic_epo = ?"  ;
			lVarBind.add(new Integer(4));
			lVarBind.add(new Integer(ic_epo));
			if(!"".equals(ic_pyme)) {
				qrySentencia += "    AND pep.ic_pyme = ?";
				lVarBind.add(new Integer(ic_pyme));
			} else if(!"".equals(cg_num_distribuidor)) {
				qrySentencia += "    AND cpe.cg_num_distribuidor = ?"  ;
				lVarBind.add(cg_num_distribuidor);
			}
			lDatos = con.consultaDB(qrySentencia, lVarBind, false);

			System.out.println("qrySentencia "+qrySentencia);
			System.out.println("lVarBind "+lVarBind);
		} catch(Exception e){
			System.out.println("ParametrosDistBean::getLimitePyme(Exception)"+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDistBean::getLimitePyme(S)");
		}
		return lDatos;
	}//getLimitePyme

	public List getPymesBloqueo(String ic_if, String ic_epo, String ic_pyme)throws NafinException{
		System.out.println("ParametrosDistBean::getPymesBloqueo(E)");
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		List lVarBind = new ArrayList();
		List lDatos = new ArrayList();
		try {
			con.conexionDB();
			qrySentencia = "select n.ic_nafin_electronico ne, " +
										" p.cg_razon_social rz, " +
										" p.cg_rfc rfc, " +
										" to_char(pexp.df_bloqueo_desbloq,'dd/mm/yyyy hh24:mi') fecha, " +
										" pexp.cg_causa_bloq_desbloq causa, " +
										" pexp.cs_activo bloqueado, " +
										" p.ic_pyme icpyme, " +
										" pe.ic_epo icepo, " +
										" pexp.cg_usuario log_usuario " +
										" from comrel_if_epo ie, comrel_if_epo_x_producto iexp " +
										" , comrel_pyme_epo pe, comrel_pyme_epo_if_x_producto pexp " +
										" , comcat_pyme p, comrel_nafin n " +
										" where ie.ic_if = ? " +
										" and ie.ic_if = iexp.ic_if " +
										" and ie.cs_distribuidores in ('S','R') " +
										" and iexp.cs_habilitado = 'S' " +
										" and iexp.ic_producto_nafin = 4 " +
										" and ie.ic_epo = iexp.ic_epo " +
										" and ie.ic_epo = pe.ic_epo " +
										" and ie.ic_epo = pexp.ic_epo " +
										" and iexp.ic_epo = pe.ic_epo " +
										" and iexp.ic_epo = pexp.ic_epo " +
										" and pe.ic_epo = pexp.ic_epo " +
										" and pe.ic_pyme = pexp.ic_pyme " +
										" and pexp.ic_producto_nafin = 4 " +
										" and iexp.ic_producto_nafin = pexp.ic_producto_nafin " +
										" and pe.cs_distribuidores in('S','R') " +
									//	" and pexp.cg_tipo_credito = 'D' " +
										" and p.ic_pyme = pexp.ic_pyme " +
										" and p.ic_pyme = pe.ic_pyme " +
										" and n.ic_epo_pyme_if = p.ic_pyme " +
										" and n.ic_epo_pyme_if = p.ic_pyme " +
										" and n.ic_epo_pyme_if = pexp.ic_pyme " +
										" and n.cg_tipo = 'P'";
			if(!ic_epo.equals(""))
				qrySentencia += "  and ie.ic_epo = ? ";

			if(!ic_pyme.equals(""))
				qrySentencia += "  and p.ic_pyme = ? ";

			System.out.println("Qry getPymesBloqueo::: "+qrySentencia);

			lVarBind.add(ic_if);

			if(!ic_epo.equals(""))
				lVarBind.add(ic_epo);

			if(!ic_pyme.equals(""))
				lVarBind.add(ic_pyme);

			qrySentencia += " order by n.ic_epo_pyme_if";

			lDatos = con.consultaDB(qrySentencia, lVarBind, false);
		}catch(Exception e){
				System.out.println("ParametrosDistBean::getPymesBloqueo(Exception)"+e);
				throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDistBean::getPymesBloqueo(S)");
		}
		return lDatos;
	}

	public void setPymesBloqueo(String cadena, String ic_usuario, String acuse, String recibo) throws NafinException {
		System.out.println("ParametrosDistBean::setPymesBloqueo(E)");
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		String registro = "";
		StringTokenizer st = null;
		VectorTokenizer vt = null;
		List lVarBind = new ArrayList();
		boolean	bOK = true;
		try {
			con.conexionDB();
			qrySentencia = " UPDATE comrel_pyme_epo_x_producto"   +
				" SET cs_activo = ?, df_bloqueo_desbloq = sysdate, cg_causa_bloq_desbloq = ?" +
				" Where ic_pyme = ? and ic_epo = ? and ic_producto_nafin = ?";
			st = new StringTokenizer(cadena, "@");
			while (st.hasMoreTokens()){

				registro = st.nextToken();
				System.out.println("registro::: "+registro);
				vt = new VectorTokenizer(registro,"|");
				Vector vecColumnas = vt.getValuesVector();
				System.out.println("vecColumnas.size()::: "+vecColumnas.size());
				lVarBind = new ArrayList();
				lVarBind.add(vecColumnas.get(3).toString());
				lVarBind.add(vecColumnas.get(4).toString());
				lVarBind.add(vecColumnas.get(1).toString());
				lVarBind.add(vecColumnas.get(2).toString());
				lVarBind.add("4");
				System.out.println(":::query update:::"+qrySentencia+"varBind"+lVarBind);
				con.ejecutaUpdateDB(qrySentencia, lVarBind);
			}
			qrySentencia = "insert into com_acuse4(cc_acuse,cg_recibo_electronico, ic_usuario, ic_producto_nafin, df_fecha_hora_carga, c_facultad) " +
				" values (?,?,?,?,sysdate,?)";
			lVarBind= new ArrayList();
			lVarBind.add(acuse);
			lVarBind.add(recibo);
			lVarBind.add(ic_usuario);
			lVarBind.add("4");
			lVarBind.add("24IF24FORMA9");
			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e){
			bOK = false;
			System.out.println("ParametrosDistBean::setPymesBloqueo(Exception)"+e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDistBean::setPymesBloqueo(S)");
		}
	}


	public void setPymesBloqueoDistEpoIf(String cadena, String ic_usuario, String acuse, String recibo) throws NafinException {
		System.out.println("setPymesBloqueoDistEpoIf(E)");
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		String qryExiste = "";
		String strSQLD = "";
		String 		strSQLCondicion		= "";
		String registro = "";
		StringTokenizer st = null;
		VectorTokenizer vt = null;
		List lVarBind = new ArrayList();
		boolean	bOK = true;
		ResultSet existe = null;
		try {
			con.conexionDB();
			st = new StringTokenizer(cadena, "@");
			while (st.hasMoreTokens()){
				registro = st.nextToken();
				vt = new VectorTokenizer(registro,"|");
				Vector vecColumnas = vt.getValuesVector();
				String IF=vecColumnas.get(0).toString();
				String  PYME = vecColumnas.get(1).toString();
				String EPO = vecColumnas.get(2).toString();
				String CHCK = vecColumnas.get(3).toString();
				String CAUSA = vecColumnas.get(4).toString();
				String usu = vecColumnas.get(5).toString();
				qryExiste="SELECT * FROM comrel_pyme_epo_if_x_producto WHERE IC_IF= "+IF+" AND IC_EPO = "+EPO+"  AND IC_PYME = "+PYME;
				log.info("qryExiste :::: "+qryExiste);
				existe = con.queryDB(qryExiste);
				if(!existe.next() ){
					if( !CAUSA.equals("")){
						strSQLD=" INSERT INTO comrel_pyme_epo_if_x_producto( IC_IF, IC_EPO, IC_PYME, DF_BLOQUEO_DESBLOQ, CG_CAUSA_BLOQ_DESBLOQ, CS_ACTIVO, IC_PRODUCTO_NAFIN, CG_USUARIO )"+
									" VALUES( "+IF+", "+EPO+", "+PYME+",sysdate,  '"+CAUSA+"', '"+CHCK+"', 4,'"+usu+"' ) ";
						log.info("strSQLD :::: "+strSQLD);
						con.ejecutaSQL(strSQLD);
					}
				}else{

						lVarBind = new ArrayList();
						if(usu.equals("")){
							strSQLCondicion = " UPDATE comrel_pyme_epo_if_x_producto"   +
													" SET cs_activo ='"+CHCK+"',df_bloqueo_desbloq = sysdate, cg_causa_bloq_desbloq = '"+CAUSA+"'" +
													" Where ic_pyme = "+PYME+" and ic_epo = "+EPO+" and ic_producto_nafin = 4  and ic_if = "+IF+" ";
							log.info("strSQLCondicion :::: "+strSQLCondicion);
							con.ejecutaUpdateDB(strSQLCondicion);
						}else{

							qrySentencia = " UPDATE comrel_pyme_epo_if_x_producto"   +
												" SET cs_activo = '"+CHCK+"', cg_usuario = '"+usu+"',df_bloqueo_desbloq = sysdate, cg_causa_bloq_desbloq = '"+CAUSA+"'" +
												" Where ic_pyme = "+PYME+" and ic_epo = "+EPO+" and ic_producto_nafin = 4 and ic_if = "+IF+" ";
							log.info("qrySentencia :::: "+qrySentencia);
							con.ejecutaUpdateDB(qrySentencia);
						}

					}

			}
			qrySentencia = "insert into com_acuse4(cc_acuse,cg_recibo_electronico, ic_usuario, ic_producto_nafin, df_fecha_hora_carga, c_facultad) " +
				" values (?,?,?,?,sysdate,?)";
			lVarBind= new ArrayList();
			lVarBind.add(acuse);
			lVarBind.add(recibo);
			lVarBind.add(ic_usuario);
			lVarBind.add("4");
			lVarBind.add("24IF24FORMA9");
			log.info("qrySentencia :::: "+qrySentencia);
			log.info("lVarBind :::: "+lVarBind);
			con.ejecutaUpdateDB(qrySentencia, lVarBind);

		} catch(Exception e){
			bOK = false;
			System.out.println("ParametrosDistBean::setPymesBloqueoDistEpoIf(Exception)"+e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOK);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDistBean::setPymesBloqueoDistEpoIf(S)");
		}
	}


	public boolean pymeBloqueadaDist(String esClavePyme, String esClaveEpo) throws NafinException {
		System.out.println("ParametrosDistBean::pymeBloqueadaDist(E)");
		int liPyme = 0; int liEpo = 0;
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (esClavePyme == null || esClaveEpo == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			liPyme = Integer.parseInt(esClavePyme);
			liEpo = Integer.parseInt(esClaveEpo);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" esClavePyme=" + esClavePyme +
					" esClaveEpo=" + esClaveEpo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		AccesoDB con = new AccesoDB();
		boolean pymeBloqueada = false;
		try {
			con.conexionDB();

			String strSQL = "SELECT cs_activo FROM comrel_pyme_epo_x_producto " +
					" WHERE ic_pyme = ? and ic_epo = ? AND ic_producto_nafin = ? ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, liPyme);
			ps.setInt(2, liEpo);
			ps.setInt(3, 4);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String bloqueo = (rs.getString("cs_activo") == null)?"":rs.getString("cs_activo");
				pymeBloqueada = (bloqueo.equals("S"))?true:false;
			}
			rs.close();
			ps.close();
		} catch (NamingException e) {
			throw new NafinException("SIST0001");
		} catch (SQLException e) {
			throw new NafinException("SIST0001");
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("ParametrosDistBean::pymeBloqueadaDist(S)");
		}
		return pymeBloqueada;
	}

	public boolean validarDistribuidor(String ic_epo, String cg_num_distribuidor) throws NafinException {
		System.out.println("ParametrosDistBean::validarDistribuidor(E)");
		AccesoDB 			con 			= new AccesoDB();
		String 				qrySentencia 	= "";
		List 				lVarBind 		= new ArrayList();
		List 				lDatos 			= new ArrayList();
		boolean				bOK				= false;
		try {
			con.conexionDB();
				qrySentencia =
					" SELECT ic_pyme"   +
					"   FROM comrel_pyme_epo"   +
					"  WHERE ic_epo = ?"   +
					"    AND cg_num_distribuidor = ?"  ;
				lVarBind.add(new Integer(ic_epo));
				lVarBind.add(cg_num_distribuidor);
				lDatos = con.consultaRegDB(qrySentencia, lVarBind, false);
				if(lDatos.size()>0){
					bOK = false;
				}
		} catch(Exception e){
			System.out.println("ParametrosDistBean::validarDistribuidor(Exception)"+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("ParametrosDistBean::validarDistribuidor(S)");
		}
		return bOK;
	}

	/**
	 * Parametriza Pyme estableciendo su plazo en dias
	 * @throws com.netro.exception.NafinException
	 * @param plazos --plazo en dias por pyme
	 * @param ic_pyme -- num identificacion de pyme
	 * @param ic_epo -- num identificacion por epo
	 */
	public void setPlazosPyme(String ic_epo, String[] ic_pyme, String[] plazos )
		throws  NafinException
	{
		AccesoDB con = new AccesoDB();
		String strInsertPlazos="";

		try
		{
			con.conexionDB();

			if(ic_pyme != null && ic_pyme.length > 0){
				for(int x=0; x < ic_pyme.length; x++){
					strInsertPlazos = " update comrel_pyme_epo_x_producto "+
												" set ig_plazo_pyme = " +plazos[x]+
												" where ic_epo = "+ic_epo+
												" and ic_pyme = "+ic_pyme[x];

					con.ejecutaUpdateDB(strInsertPlazos);
				}//cierre for
			}// cierre if

		con.terminaTransaccion(true);
		}catch(Exception e)
		{
			e.printStackTrace();
			con.terminaTransaccion(false);
			con.cierraConexionDB();
			throw new NafinException("SIST0001");
		}finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}

	/**
	 * Obtiene pymes con sus plazos ya establecidos
	 * @throws com.netro.exception.NafinException
	 * @return List
	 * @param ic_if clave del intermediario financiero
	 * @param ic_epo clave de la cadena productiva
	 */
	public List getPlazosPymeIfEpo(String ic_epo, String ic_if )
		throws  NafinException
	{
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		List colPlazoPyme = new ArrayList();
		try
		{
			con.conexionDB();



			String qryPlazosPyme =
					" SELECT pexp.ic_pyme, pexp.ig_plazo_pyme, cp.cg_razon_social "+
					" FROM comcat_pyme cp, comrel_pyme_epo cpe,  "+
					" comrel_pyme_epo_x_producto pexp, com_linea_credito lc "+
					" WHERE cp.ic_pyme = cpe.ic_pyme  "+
					" AND lc.ic_pyme = cp.ic_pyme "+
					" AND cpe.cs_distribuidores in('S','R') "+
					" AND cp.ic_pyme = pexp.ic_pyme "+
					" AND pexp.ic_producto_nafin = 4  "+
					" AND cp.cs_invalido != 'S'  "+
					" AND cpe.ic_epo = pexp.ic_epo    "+
					" AND pexp.cs_habilitado in ('S','N') "+
					" AND pexp.cg_tipo_credito = 'C' "+
					" AND lc.ic_producto_nafin = 4 "+
					" AND TRUNC (lc.df_vencimiento_adicional) > TRUNC (SYSDATE) "+
					" AND lc.cg_tipo_solicitud IN ('I',  'R') "+
					" AND lc.ic_if = "+ ic_if+
					" AND pexp.ic_epo = "+ ic_epo+
					" GROUP BY pexp.ic_pyme, pexp.ig_plazo_pyme, cp.cg_razon_social "+
					" ORDER BY 2 ";

			rs = con.queryDB(qryPlazosPyme);

				while(rs.next())
				{
					List regPyme = new ArrayList();

					regPyme.add(rs.getString(1));
					regPyme.add(rs.getString(2));
					regPyme.add(rs.getString(3));

					colPlazoPyme.add(regPyme);
				}



		}catch(Exception e)
		{
			e.printStackTrace();
			con.cierraConexionDB();
			throw new NafinException("SIST0001");
		}finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

		return colPlazoPyme;
	}

	public List validarLimitexMoneda(String ic_epo) throws NafinException {
		log.info("ParametrosDistBean::validarLimitexMoneda(E)");
		AccesoDB 		con	 			= new AccesoDB();
		StringBuffer	qrySentencia;
		List				lVarBind 		= new ArrayList();
		List 				lDatos 			= new ArrayList();
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

			qrySentencia.append(
				" SELECT FN_MONTO_AUTORIZADO_TOTAL, IC_MONEDA" +
				 " FROM DIS_LINEA_CREDITO_DM " +
				 " WHERE  DF_VENCIMIENTO_ADICIONAL > SYSDATE" +
				 " AND CG_TIPO_SOLICITUD IN('I')" +
				 " AND IC_EPO = ?" +
				 " AND IC_PRODUCTO_NAFIN = ?" +
				 " AND IC_ESTATUS_LINEA = ?" +
				 " ORDER BY IC_MONEDA ASC");
			lVarBind.add(new Integer(ic_epo));
			lVarBind.add(new Integer("4"));
			lVarBind.add(new Integer("12"));
			lDatos = con.consultaRegDB(qrySentencia.toString(), lVarBind, false);
		} catch(Exception e){
			log.error("ParametrosDistBean::validarLimitexMoneda(Exception)"+e);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDistBean::validarLimitexMoneda(S)");
		}
		return lDatos;
	}

 /**
  *M�todo que regresa si el epo est� o no parametrizada como
  *Opera Descuento Autom�tico y como Venta de Cartera
	*o cualquier otro par�metro correspondiente a la
	*tabla de  com_parametrizacion_epo
	*Fodea 010 Distribuidores 2010
	*@return "S" si se encuentra parametrizado el parametro
	* "N" si este no esta parametrizado
   *@param ic_epo numero de la Epo
   *@param parametro a buscar por la Epo
   */
    public String DesAutomaticoEpo(String ic_epo, String parametro) {
        log.info("ParametrosDistBean::DesAutomaticoEpo(E)");
        String		qrySentencia	= "";
        ResultSet	rs				= null;
        AccesoDB 	con				= null;
        PreparedStatement   ps      = null;
        String valor = "";
        try {
            con = new AccesoDB();
            con.conexionDB();
            qrySentencia =
                " SELECT cg_valor as valor  "+
                " FROM com_parametrizacion_epo  "+
                " WHERE  cc_parametro_epo = ? "+
                " AND ic_epo =  ? ";
            log.debug("qrySentencia"  +qrySentencia);
            ps = con.queryPrecompilado(qrySentencia);
            log.trace("parametro:"+parametro);
            ps.setString(1, parametro);
            log.trace("ic_epo:"+ic_epo);
            ps.setInt(2, Integer.valueOf(ic_epo));
            rs = ps.executeQuery();
            ps.clearParameters();
            if(rs.next()) {
                valor = ((rs.getString("valor")==null)?"N":rs.getString("valor"));
            } else {
                valor = "N";
            }
            rs.close();ps.close();
            log.debug("valor:"  +valor);
        } catch(Exception e) {
            log.error("ParametrosDistBean::DesAutomaticoEpo(Exception) "+e);
            con.terminaTransaccion(false);
        } finally {
            try {
                if (rs!=null) rs.close();
                if (ps!=null) ps.close();
            } catch(Exception e) {
                log.error("CargaDocDistBean::getCamposAdicionales(Exception)" + e.getMessage(), e);
            }
            if(con.hayConexionAbierta())
                con.cierraConexionDB();
        }
        log.info("ParametrosDistBean::DesAutomaticoEpo(S)");
        return valor;
    }
 
  /**
   * metodo que actualiza los parametros de la epo
   * en  Financiamiento de Distribuidores
   * @return
   * @param ventaCartera
   * @param lineaCredito
   * @param noNegociables
   * @param desautomatico
   * @param ic_epo
   */
   	private  String  ParaAutomaticoEpo(String ic_epo, String desautomatico,
            String noNegociables, String lineaCredito,String ventaCartera)  	{

      log.info("ParaAutomaticoEpo (E) ");

		String qrySentencia	= "";
		String qryInsert = "";
		AccesoDB 	con				= null;
      String valor = "";
      PreparedStatement ps = null;
      boolean transaccion = true;

    	try {
        con = new AccesoDB();
        con.conexionDB();

        qrySentencia = " DELETE com_parametrizacion_epo Where "+
                " CC_PARAMETRO_EPO in( " +
                "'PUB_EPO_DESC_AUTOMATICO', " +
                "'PUB_EPO_VENTA_CARTERA', " +
                "'PUB_LIMITE_LINEA_CREDITO', " +
                "'PUB_EPO_NO_NEGOCIABLES')" +
                " AND IC_EPO = " + ic_epo;

       ps = con.queryPrecompilado(qrySentencia);
			 ps.executeUpdate();
			 ps.close();

       log.debug("qrySentencia"  +qrySentencia);

       if (desautomatico.equals("S")|| desautomatico.equals("N")) {   //Descuento Automatico
           qryInsert = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)  "+
                        " VALUES("+ic_epo+", 'PUB_EPO_DESC_AUTOMATICO', '"+desautomatico+"'"+")";

           ps = con.queryPrecompilado(qryInsert);
           ps.executeUpdate();
           ps.close();
            log.debug("desautomatico::   "+qryInsert);
      }

        if (noNegociables.equals("S")|| noNegociables.equals("N")) {    //Documentos No Negociables
           qryInsert = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)  "+
                        " VALUES("+ic_epo+", 'PUB_EPO_NO_NEGOCIABLES', '"+noNegociables+"'"+")";

           ps = con.queryPrecompilado(qryInsert);
           ps.executeUpdate();
           ps.close();
            log.debug("noNegociables::   "+qryInsert);
      }

       if (lineaCredito.equals("S")|| lineaCredito.equals("N")) {    //Limite de Linea de Credito
           qryInsert = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)  "+
                        " VALUES("+ic_epo+", 'PUB_LIMITE_LINEA_CREDITO', '"+lineaCredito+"'"+")";

           ps = con.queryPrecompilado(qryInsert);
           ps.executeUpdate();
           ps.close();
            log.debug("lineaCredito::   "+qryInsert);
      }

       if (ventaCartera.equals("S")|| ventaCartera.equals("N")) {    //venta de Cartera
           qryInsert = " INSERT INTO com_parametrizacion_epo  (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)  "+
                        " VALUES("+ic_epo+", 'PUB_EPO_VENTA_CARTERA', '"+ventaCartera+"'"+")";

           ps = con.queryPrecompilado(qryInsert);
           ps.executeUpdate();
           ps.close();
          log.debug("ventaCartera::   "+qryInsert);
      }

        con.cierraStatement();
      }catch(Exception e){
      		  transaccion = false;
			      e.printStackTrace();
          log.error("ParaAutomaticoEpo "+e);
      } finally {
    		 if(con.hayConexionAbierta()){
				 con.terminaTransaccion(transaccion);
				 con.cierraConexionDB();
			 }
      }
        log.info("ParaAutomaticoEpo (S) ");
		return valor;
    }
   /**
   * Metodo que obtiene el plazo parametrizado por la epo   *
   * Fodea 014-2010 Distribuidores Fase II
   * @return  el valor del plazo capturado en la parametizaci�n de al EPO
   * @param ic_epo
   */
  public  String  plazoPorEpo(String ic_epo)  	{

      log.info("plazoPorEpo (E) ");

		String qrySentencia	= "";
		ResultSet	rs				= null;
		AccesoDB 	con				= null;
		String valor = "";
		boolean transaccion = true;

    	try {
        con = new AccesoDB();
        con.conexionDB();
        qrySentencia =
         " SELECT ig_dias_maximo " +
         " FROM comrel_producto_epo   "+
         " WHERE ic_producto_nafin = 4   "+
         " AND ic_epo = "+ic_epo;

        log.debug("qrySentencia"  +qrySentencia);

        rs = con.queryDB(qrySentencia);
        if(rs.next()) {
          valor=   ((rs.getString(1)==null)?"":rs.getString(1));
         }
        con.cierraStatement();

        if (valor.equals("")|| valor.equals("0")) { // cuando no tiene plazo la epo

         qrySentencia =" select IN_DIAS_MAXIMO from comcat_producto_nafin "+
                        " where ic_producto_nafin = 4 ";
        log.debug("qrySentencia"  +qrySentencia);

        rs = con.queryDB(qrySentencia);
        if(rs.next()) {
          valor=   ((rs.getString(1)==null)?"":rs.getString(1));
         }
        con.cierraStatement();
        }


      }catch(Exception e){
      		  transaccion = false;
			      e.printStackTrace();
          log.error("plazoPorEpo "+e);
      } finally {
    		 if(con.hayConexionAbierta()){
				 con.terminaTransaccion(transaccion);
				 con.cierraConexionDB();
			 }
      }
        log.info("plazoPorEpo (S) ");
		return valor;
    }

     /**
   * Metodo que obtiene si la epo opera
   * CCC (Cr�dito en Cuenta Corriente).   *
   * Fodea 014-2010 Distribuidores Fase II
   * @return  el valor del tipo de credito
   * C = Credito en Cuenta Corriente
   * D = Descuento/Factoraje
   * A = Ambos
   * @param ic_epo
   */
  public  String  obtieneTipoCredito(String ic_epo)  	{

      log.info("obtieneTipoCredito (E) ");

		String qrySentencia	= "";
		ResultSet	rs				= null;
		AccesoDB 	con				= null;
		String valor = "";
      boolean transaccion = true;

    	try {
        con = new AccesoDB();
        con.conexionDB();
        qrySentencia =
         " SELECT CG_TIPOS_CREDITO " +
         " FROM comrel_producto_epo   "+
         " WHERE ic_producto_nafin = 4   "+
         " AND ic_epo = "+ic_epo;

        log.debug("qrySentencia"  +qrySentencia);

        rs = con.queryDB(qrySentencia);
        if(rs.next()) {
          valor=   ((rs.getString(1)==null)?"":rs.getString(1));
         }

        con.cierraStatement();
      }catch(Exception e){
      		  transaccion = false;
			      e.printStackTrace();
          log.error("obtieneTipoCredito "+e);
      } finally {
    		 if(con.hayConexionAbierta()){
				 con.terminaTransaccion(transaccion);
				 con.cierraConexionDB();
			 }
      }
        log.info("obtieneTipoCredito (S) ");
		return valor;
    }

	//**********************Fodea 017-2011 (E)***********************************

	/**
	 *
	 * @return
	 * @param ic_epo
	 */
	public  String  obtieneResponsable(String ic_epo)  	{
		log.info("obtieneResponsable (E) ");

	  ResultSet	rs				= null;
    PreparedStatement ps = null;
	  AccesoDB 	con				= null;
    String responsable = "";
    boolean transaccion = true;

    try {
			con = new AccesoDB();
      con.conexionDB();

			String	qrySentencia =	" select nvl(a.cg_responsable_interes,b.cg_responsable_interes) as cg_responsable_interes" +
				" from comrel_producto_epo a,comcat_producto_nafin b " +
				" where a.ic_epo = ? "+
				" and a.ic_producto_nafin = ? " +
				" and a.ic_producto_nafin = b.ic_producto_nafin ";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_epo));
			ps.setInt(2,4);
			rs = ps.executeQuery();
			if(rs.next()){
				responsable = (rs.getString("cg_responsable_interes")==null)?"":rs.getString("cg_responsable_interes");
			}
			rs.close();
			ps.close();



    }catch(Exception e){
			transaccion = false;
			e.printStackTrace();
      log.error("obtieneResponsable "+e);
    } finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
    }

    log.info("obtieneResponsable (S) ");
			return responsable;

	}
	 /**
	 *
	 * @return
	 * @param ic_pyme
	 */
 	public  String  obtieneTiposCreditoA(String ic_pyme)  	{
		log.info("obtieneTiposCreditoA (E) ");

		ResultSet	rs				= null;
		PreparedStatement ps = null;
		AccesoDB 	con				= null;
		boolean transaccion = true;
		String tiposCredito = "";

    try {
			con = new AccesoDB();
      con.conexionDB();

		String qrySentencia = 	"  SELECT CG_TIPO_CREDITO,COUNT(*)" +
						"  FROM COMREL_PYME_EPO_X_PRODUCTO" +
						"  WHERE IC_PYME = ? "+
						"  AND IC_PRODUCTO_NAFIN = 4"+
						"  GROUP BY CG_TIPO_CREDITO";

		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_pyme));
		rs = ps.executeQuery();
		while(rs.next()){
			if(!"".equals(tiposCredito)&&rs.getInt(2)>0)
				tiposCredito = "A";
			if("".equals(tiposCredito)&&rs.getInt(2)>0)
				tiposCredito = (rs.getString(1)==null)?"":rs.getString(1);
		}

    }catch(Exception e){
			transaccion = false;
			e.printStackTrace();
      log.error("obtieneTiposCreditoA "+e);
    } finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
    }
    log.info("obtieneTiposCreditoA (S) ");
		return tiposCredito;
	}


 /**
	 *
	 * @return
	 * @param ic_pyme
	 */
	public  String  obtieneTipoConversion(String ic_pyme)  	{
		log.info("obtieneTipoConversion (E) ");

		ResultSet	rs				= null;
		PreparedStatement ps = null;
		AccesoDB 	con				= null;
		boolean transaccion = true;
		String cgTipoConversion = "";

    try {
			con = new AccesoDB();
      con.conexionDB();

		String qrySentencia = 	"  SELECT nvl(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION)" +
						"  FROM COMREL_PYME_EPO_X_PRODUCTO PEP" +
						"  ,COMREL_PRODUCTO_EPO PE"+
						"  ,COMCAT_PRODUCTO_NAFIN PN"+
						"  WHERE PEP.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
						"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
						"  AND PE.IC_EPO = PEP.IC_EPO"+
						"  AND PEP.IC_PYME = ? "+
						"  AND PEP.IC_PRODUCTO_NAFIN = ? "+
						"  AND PE.IC_PRODUCTO_NAFIN = ? ";
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_pyme));
		ps.setInt(2,4);
		ps.setInt(3,4);

		rs = ps.executeQuery();
		while(rs.next()){
			if("P".equals(rs.getString(1))){
				cgTipoConversion = (rs.getString(1)==null)?"":rs.getString(1);
			}
		}

    }catch(Exception e){
			transaccion = false;
			e.printStackTrace();
      log.error("obtieneTipoConversion "+e);
    } finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}

    }

    log.info("obtieneTipoConversion (S) ");
		return cgTipoConversion;

	}
 	//**********************Fodea 017-2011 (E)***********************************

	/**
	 * Obtiene los parametros necesarios para inicializar el m�dulo de
	 * distribuidores
	 * @param claveAfiliado Clave del Afiliado (ic_pyme, ic_epo, etc.)
	 * @param tipoAfiliado Tipo de Afiliado (P Pyme, E Epo, I If)
	 * @param claveEpoRelacionada Clave de la epo relacionada. (aplica si tipoAfiliado=P)
	 */
	public Map getParametrosModuloDistribuidores(String claveAfiliado,
			String tipoAfiliado, String claveEpoRelacionada) {
		log.info("getParametrosModuloDistribuidores(E)");
		Map mParamsModulo = new HashMap();


		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveAfiliado==null || claveAfiliado.equals("") ||
					tipoAfiliado == null || tipoAfiliado.equals("") ) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			Integer.parseInt(claveAfiliado);
			if (claveEpoRelacionada != null && !claveEpoRelacionada.equals("")) {
				Integer.parseInt(claveEpoRelacionada);
			}

		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos", e);
		}
		//***************************************************************************************


		AccesoDB con = new AccesoDB();
		List params = null;
		try {
			if (tipoAfiliado.equals("P")) {

				con.conexionDB();

				String strSQL =
						" SELECT pep.cs_aceptacion_pyme "+
						" ,pep.cs_habilitado "+
						" ,pep.cs_contrato_firmado "+
						" FROM comrel_pyme_epo_x_producto pep "+
						" ,comrel_pyme_epo pe "+
						" WHERE pep.ic_producto_nafin = ? "+
						" and pep.ic_pyme = pe.ic_pyme "+
						" and pep.ic_epo = pe.ic_epo "+
						" and pe.cs_distribuidores in(?,?) "+
						" and pe.ic_pyme = ? "+
						" and pe.ic_epo = ? ";
				params = new ArrayList();
				params.add(new Integer(4));
				params.add("R");
				params.add("S");
				params.add(new Integer(claveAfiliado));
				params.add(new Integer(claveEpoRelacionada));


				Registros reg = con.consultarDB(strSQL,params);
				if (reg.next()) {
					mParamsModulo.put("cs_aceptacion_pyme", reg.getString("cs_aceptacion_pyme"));
				}

				if ("S".equals(mParamsModulo.get("cs_aceptacion_pyme")) ) {
					strSQL =
							" SELECT COUNT(*) AS numRelacionesPymeEpo " +
							" FROM comrel_pyme_epo_x_producto " +
							" WHERE ic_pyme = ? " +
							" AND ic_producto_nafin = ? "+
							" AND cs_habilitado = ? ";
					params = new ArrayList();
					params.add(new Integer(claveAfiliado));
					params.add(new Integer(4));
					params.add("S");

					reg = con.consultarDB(strSQL,params);
					if (reg.next()) {
						mParamsModulo.put("bPymeRelacionada", ((Integer.parseInt(reg.getString(1))>1)?"S":"N"));
					}
				}

			} //Fin pyme
			return mParamsModulo;
		} catch(Exception e) {
			throw new AppException("Error al inicializar el modulo de Descuento");
		} finally {
			log.info("getParametrosModuloDistribuidores(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}



/**
	 *
	 * @return
	 * @param ic_epo
	 */
	public  List  obtieneParametrosAviso(String ic_epo)  	{
		log.info("obtieneParametrosAviso (E) ");

		ResultSet	rs				= null;
		PreparedStatement ps = null;
		AccesoDB 	con				= null;
		boolean transaccion = true;
		List 	datos  = new ArrayList();

    try {
			con = new AccesoDB();
      con.conexionDB();
    	String   qrySentencia =	" select nvl(a.cg_responsable_interes,b.cg_responsable_interes) as cg_responsable_interes " +
					" ,nvl(a.cg_tipos_credito,b.cg_tipos_credito) as tipos_credito , "+
					" DECODE (NVL (a.cg_tipo_conversion, b.cg_tipo_conversion), 'N', '', 'P', 'Dolar-Peso','')  as tipo_conversion " +
					" from comrel_producto_epo a,comcat_producto_nafin b " +
					" where a.ic_epo = ? " +
					" and a.ic_producto_nafin = ? " +
					" and a.ic_producto_nafin = b.ic_producto_nafin ";


		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_epo));
		ps.setInt(2,4);
		rs = ps.executeQuery();
		while(rs.next()){
			datos.add((rs.getString("cg_responsable_interes")==null)?"":rs.getString("cg_responsable_interes"));
			datos.add((rs.getString("tipos_credito")==null)?"":rs.getString("tipos_credito"));
			datos.add((rs.getString("tipo_conversion")==null)?"":rs.getString("tipo_conversion"));
		}

    }catch(Exception e){
			transaccion = false;
			e.printStackTrace();
      log.error("obtieneParametrosAviso "+e);
    } finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
		}

   }

    log.info("obtieneParametrosAviso (S) ");
		return datos;

	}

	public List validarLimitexMoneda2(String ic_epo){
		log.info("ParametrosDistBean::validarLimitexMoneda2(E)");
		AccesoDB 			con 			= new AccesoDB();
		StringBuffer	qrySentencia;
		ResultSet rs = null;
		PreparedStatement ps = null;
		List 				lDatos 			= new ArrayList();
		List 				lDatosaux 			= null;
		try {
			con.conexionDB();

			qrySentencia = new StringBuffer();

			qrySentencia.append(
					" SELECT sum(FN_MONTO_AUTORIZADO_TOTAL) , IC_MONEDA" +
					 " FROM DIS_LINEA_CREDITO_DM " +
					 " WHERE  DF_VENCIMIENTO_ADICIONAL > SYSDATE" +
					 " AND CG_TIPO_SOLICITUD IN(?)" +
					 " AND IC_EPO = ? " +
					 " AND IC_PRODUCTO_NAFIN = ? " +
					 " AND IC_ESTATUS_LINEA = ? " +
					 " group by ic_moneda" +
					 " ORDER BY IC_MONEDA ASC");

				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setString(1,"I");
				ps.setInt(2,Integer.parseInt(ic_epo));
				ps.setInt(3,4);
				ps.setInt(4,12);
				rs = ps.executeQuery();

				while(rs.next()){
					lDatosaux = new ArrayList();
					lDatosaux.add(rs.getString(1));
					lDatosaux.add(rs.getString(2));
					lDatos.add(lDatosaux);
				}
				rs.close();
				ps.close();

		} catch(Exception e){
			log.error("ParametrosDistBean::validarLimitexMoneda2(Exception)"+e);
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("ParametrosDistBean::validarLimitexMoneda2(S)");
		}
		return lDatos;
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante.
	 * @param ic_pyme
	 */
	public Registros getClasificacionClientes(String ic_pyme)	{
		log.info("getClasificacionClientes");
		StringBuffer qrySentencia;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

				qrySentencia.append(
					" SELECT ic_clasificacion,cg_clave, cg_descripcion,ig_plazo_aut " +
					" FROM comrel_clasificacion " +
					" WHERE ic_producto_nafin = ? " +
					" AND ic_epo = ? " +
					" ORDER BY cg_clave");

			log.debug("qrySentencia= " + qrySentencia.toString());
			List params = new ArrayList();
			params.add(new Integer(4));
			params.add(ic_pyme);
			Registros reg = con.consultarDB(qrySentencia.toString(), params);
			return reg;
		} catch(Exception e) {
			log.error("getClasificacionClientes(Exception) "+e);
			throw new AppException("Error al obtener clasificacion de clientes",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getClasificacionClientes");
		}
	}//fin metodo

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante.
	 * @param ic_pyme y/o ic_epo
	 */
	public Registros getClasificacionClientes2(String ic_epo,String ic_pyme, String categoria, String plazo)	{
		log.info("getClasificacionClientes2");
		StringBuffer qrySentencia;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

				qrySentencia.append(
					" select " +
					"	    py.ic_pyme, " +
					"	    py.cg_razon_social, " +
					"	    clas.cg_descripcion, " +
					"	    clas.ic_clasificacion, " +
					"	    pep.ig_dias_maximo, " +
					"	    clas.ig_plazo_aut " +
					"	from  " +
					"	    comrel_pyme_epo_x_producto pep, " +
					"	    comcat_pyme py, " +
					"	    comrel_clasificacion clas " +
					"	where py.ic_pyme = pep.ic_pyme " +
					"	    and pep.ic_epo = clas.ic_epo (+)" +
					"	    and pep.ic_producto_nafin = clas.ic_producto_nafin (+)" +
					"	    and pep.ic_clasificacion = clas.ic_clasificacion (+)" +
					" 		and pep.ic_producto_nafin = ? " +
					" 		and pep.ic_epo = ? ");

			List params = new ArrayList();
			params.add(new Integer(4));
			params.add(ic_epo);

			if (!categoria.equals("")) {
				qrySentencia.append(" AND pep.ic_clasificacion = ? ");
				params.add(categoria);
			}

			if (!ic_pyme.equals("") && !ic_pyme.equals("T")){
				qrySentencia.append(" AND pep.ic_pyme = ? ");
				params.add(ic_pyme);
			}
			if (!plazo.equals("")){
				qrySentencia.append("  AND pep.ig_dias_maximo = ? ");
				params.add(plazo);
			}
			qrySentencia.append(" AND (   pep.ic_clasificacion IS NOT NULL OR pep.ig_dias_maximo IS NOT NULL) ");
			qrySentencia.append(" ORDER BY cg_clave");

			log.debug("qrySentencia= " + qrySentencia.toString());
			Registros reg = con.consultarDB(qrySentencia.toString(), params);
			return reg;
		} catch(Exception e) {
			log.error("getClasificacionClientes2(Exception) "+e);
			throw new AppException("Error al obtener clasificacion de clientes ",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getClasificacionClientes2");
		}
	}//fin metodo

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante.
	 * @param ic_pyme y/o ic_epo
	 */
	public Registros getPlazosClasificacionClientes(String ic_clas)	{
		log.info("getPlazosClasificacionClientes");
		StringBuffer qrySentencia;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

				qrySentencia.append(
					" SELECT * FROM dis_plazo_descuento WHERE ic_clasificacion =  ? " +
					" ORDER BY IC_PLAZO_DESCUENTO ");

			List params = new ArrayList();
			params.add(ic_clas);
			log.debug("qrySentencia= " + qrySentencia.toString());
			Registros reg = con.consultarDB(qrySentencia.toString(), params);
			return reg;
		} catch(Exception e) {
			log.error("getPlazosClasificacionClientes(Exception) "+e);
			throw new AppException("Error al obtener el plazo de la clasificacion de clientes ",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getPlazosClasificacionClientes");
		}
	}//fin metodo

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante.
	 * @param ic_pyme
	 */
	public Registros getDistribuidorClasificacion(String ic_pyme)	{
		log.info("getDistribuidorClasificacion");
		StringBuffer qrySentencia;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

			qrySentencia.append(
				" SELECT pe.cg_num_distribuidor, py.cg_razon_social, pep.ig_dias_maximo"   +
				"   FROM comcat_pyme py, comrel_pyme_epo pe, comrel_pyme_epo_x_producto pep"   +
				"  WHERE py.ic_pyme = pe.ic_pyme"   +
				"    AND pep.ic_pyme = pe.ic_pyme"   +
				"    AND pep.ic_epo = pe.ic_epo"   +
				"    AND pep.ic_producto_nafin = ? "   +
				"    AND py.ic_pyme = ? " );

			log.debug("qrySentencia= " + qrySentencia.toString());
			List params = new ArrayList();
			params.add(new Integer(4));
			params.add(ic_pyme);
			Registros reg = con.consultarDB(qrySentencia.toString(), params);
			return reg;
		} catch(Exception e) {
			log.error("getDistribuidorClasificacion(Exception) "+e);
			throw new AppException("Error al obtener el distribuidor de la clasficacion",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getDistribuidorClasificacion");
		}
	}//fin metodo


	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante.
	 * @param ic_pyme
	 */
	public String validaDiasCategorias(String ic_pyme, String id_clas)	{
		log.info("validaDiasCategorias");
		String igDias = "";
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

				qrySentencia.append(
					" SELECT max(pep.ig_dias_maximo) AS igDias" +
					" FROM comrel_pyme_epo_x_producto pep, comrel_clasificacion c " +
					" WHERE pep.ic_epo = ? " +
					" AND pep.ic_producto_nafin = ? " +
					" AND pep.ic_producto_nafin = c.ic_producto_nafin " +
					" AND pep.ic_epo = c.ic_epo " +
					" AND c.ic_clasificacion = ? " +
					" AND pep.ic_clasificacion = c.ic_clasificacion"
				);

			log.debug("qrySentencia= " + qrySentencia.toString());

			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,Integer.parseInt(ic_pyme));
			ps.setInt(2,4);
			ps.setString(3,id_clas);
			rs = ps.executeQuery();
			if (rs.next()){
				igDias = rs.getString(1)==null?"":rs.getString(1);
			}
		} catch(Exception e) {
			log.error("validaDiasCategorias(Exception) "+e);
			throw new AppException("Error al validar el numero de dias",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" validaDiasCategorias");
		}
		return igDias;
	}//fin metodo

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante.
	 * @param ic_pyme, cve_cat
	 */
	public String validaAgregaCategoria(String ic_pyme, String cve_cat)	{
		log.info("validaAgregaCategoria");
		String resultado = "";
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

				qrySentencia.append(
					" SELECT 1 " +
					" FROM COMREL_CLASIFICACION " +
					" WHERE IC_PRODUCTO_NAFIN = ? " +
					" AND ic_epo = ? " +
					" AND cg_clave = ? "
				);

			log.debug("qrySentencia= " + qrySentencia.toString());

			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,4);
			ps.setInt(2,Integer.parseInt(ic_pyme));
			ps.setString(3,cve_cat);
			rs = ps.executeQuery();
			if (rs.next()){
				resultado = "true";
			}
		} catch(Exception e) {
			log.error("validaAgregaCategoria(Exception) "+e);
			throw new AppException("Error al validar la clave de clasificacion",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" validaAgregaCategoria");
		}
		return resultado;
	}//fin metodo

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante.
	 * @param ic_docto
	 */
	public Registros getDetalleCambiosDoctos(String ic_docto)	{
		log.info("getDetalleCambiosDoctos");
		StringBuffer qrySentencia;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

			qrySentencia.append(
					"SELECT distinct TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, "+
					"       TO_CHAR (ce.df_fecha_emision_anterior, 'dd/mm/yyyy') AS fecha_emi_ant, "+
					"       ce.ct_cambio_motivo, ce.fn_monto_anterior, ce.fn_monto_nuevo, "+
					"       TO_CHAR (ce.df_fecha_emision_nueva, 'dd/mm/yyyy') AS fecha_emi_new, "+
					"       TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy') AS fecha_venc_ant, "+
					"       TO_CHAR (ce.df_fecha_venc_nueva, 'dd/mm/yyyy') AS fecha_venc_new, "+
					"       cce.cd_descripcion "+
					"		  ,TFA.cd_descripcion as modo_plazo_anterior"+
					"		  ,TFN.cd_descripcion as modo_plazo_nuevo"+
					"  FROM dis_cambio_estatus ce, comcat_cambio_estatus cce "+
					" 	,comcat_tipo_financiamiento TFA,comcat_tipo_financiamiento TFN"+
					" WHERE ic_documento = ? "+
					"   AND cce.ic_cambio_estatus = ce.ic_cambio_estatus "+
					"	  AND ce.ic_tipo_finan_ant = TFA.ic_tipo_financiamiento(+)"+
					"	  AND ce.ic_tipo_finan_nuevo = TFN.ic_tipo_financiamiento(+)" );

			log.debug("qrySentencia= " + qrySentencia.toString());
			List params = new ArrayList();
			params.add(ic_docto);
			Registros reg = con.consultarDB(qrySentencia.toString(), params);
			return reg;
		} catch(Exception e) {
			log.error("getDetalleCambiosDoctos(Exception) "+e);
			throw new AppException("Error al obtener el detalle de cambios de documentos",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getDetalleCambiosDoctos");
		}
	}//fin metodo

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante.
	 * @param ic_epo
	 * @param cg_rfc
	 * @param ic_nafinElec
	 * @param ic_pyme
	 */

	public Registros getInformacionDistribuidor(String ic_epo,String cg_rfc,String ic_nafinElec,String ic_pyme,String ic_if)	{
		log.info("getInformacionDistribuidor");
		String query;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();

			query =
				" SELECT cp.ic_pyme, cp.cg_rfc, crn.ic_nafin_electronico "   +
				" FROM comcat_pyme cp, comrel_pyme_epo cpe, comrel_pyme_epo_x_producto pexp, comrel_nafin crn "   +
				" WHERE cp.ic_pyme = cpe.ic_pyme "   +
				" AND cp.ic_pyme = pexp.ic_pyme "   +
				" AND cpe.ic_epo = pexp.ic_epo "   +
				" AND crn.IC_EPO_PYME_IF = cp.ic_pyme "   +
				" AND cpe.cs_distribuidores IN ('S', 'R') "   +
				" AND pexp.cs_habilitado in ('S','N') "   +
				" AND pexp.cg_tipo_credito = 'D' "   +
				" AND pexp.ic_producto_nafin = 4 "   +
				" AND pexp.ic_epo = "+ic_epo+" "+
				" AND crn.CG_TIPO = 'P' "   +
				" AND cp.ic_pyme NOT IN ( SELECT cbp.ic_pyme "   +
				" FROM comrel_cuenta_bancaria_x_prod cbp, com_cuenta_aut_x_prod cap "   +
				" WHERE cbp.ic_cuenta_bancaria = cap.ic_cuenta_bancaria "   +
				" AND cbp.ic_epo = pexp.ic_epo  ";
			query += !"".equals(ic_if)?" AND cbp.ic_if = "+ic_if+" ":"";
			query +=")";
			query += (!"".equals(cg_rfc))?" AND cp.cg_rfc = '"+cg_rfc+"' ":"";
			query += (!"".equals(ic_pyme))?" AND cp.ic_pyme = "+ic_pyme+" ":"";
			query += (!"".equals(ic_nafinElec))?" AND crn.ic_nafin_electronico = "+ic_nafinElec+" ":"";

			log.debug("qrySentencia= " + query);


			Registros reg = con.consultarDB(query);
			return reg;
		} catch(Exception e) {
			log.error("getInformacionDistribuidor(Exception) "+e);
			throw new AppException("Error al obtener la informacion del distribuidor",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getInformacionDistribuidor");
		}
	}//fin metodo

	/**
	 * metodo para validar si la epo esta habilitada
	 * para el producto de Distribuidores
	 * @return
	 * @param ic_epo
	 */
	public String epohabilitada(String ic_epo)	{
		log.info("epohabilitada");
		String resultado = "";
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

			qrySentencia.append(" Select cs_habilitado from comrel_producto_epo where ic_producto_nafin = ?  and ic_epo = ? ");

			log.debug("qrySentencia= " + qrySentencia.toString());

			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,4);
			ps.setInt(2,Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			if (rs.next()){
				resultado = rs.getString(1);
			}
		} catch(Exception e) {
			log.error("epohabilitada(Exception) "+e);
			throw new AppException("Error  al validar si la epo esta Habilitada para Distribuidores ",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" epohabilitada");
		}
		return resultado;
	}//fin metodo


	/**
	 * metodo para Insertar, Actualizar y Eliminar cuentas bancarias
	 * retorna un boleano si la accion se realizo correcta true, de lo
	 * contrario false
	 * @return boolean
	 * @param ic_epo
	 */
	public String[] actualizaCuentaBancaria(String ic_financiera,String ic_if,String ic_moneda,String ic_epo,
	 String cg_rfc,String ic_nafinElec,String ic_pyme,String CtasCapturadas,String cg_sucursal,String cg_numcta,String ic_plaza){
		log.info("actualizarCuenta");
		String query;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String ic_cuenta_b="";

		String strMensaje = "";
		boolean CuentaOK	 	= false;
		boolean existeError 	= true;
		PreparedStatement ps1 = null;
		try {
			con.conexionDB();

			String ic_if_tipo1 = "";
			query =
				" SELECT cif.ic_financiera "   +
				" FROM comcat_if cif, comcat_financiera fin "   +
				" WHERE cif.ic_financiera = fin.ic_financiera "   +
				" AND fin.ic_tipo_financiera = 1 "   +
				" AND cif.ic_if = ?"  ;
			ps = con.queryPrecompilado(query);
			log.debug("qrySentencia= " + query);
			ps.setInt(1, Integer.parseInt(ic_if));
			rs = ps.executeQuery();
			if(rs.next()) {
				ic_if_tipo1 = rs.getString(1);
				ic_financiera = ic_if_tipo1;
			}


			if(!"".equals(ic_epo)&&!"".equals(ic_pyme)&&!"".equals(ic_moneda)&&!"".equals(ic_financiera)) {
			query =
				" SELECT ic_cuenta_bancaria,cg_numero_cuenta,cg_sucursal,ic_plaza "   +
				" FROM comrel_cuenta_bancaria_x_prod "   +
				" WHERE ic_epo = ? "   +
				" AND ic_pyme = ? "   +
				" AND ic_financiera = ? "   +
				" AND ic_moneda = ?"  ;
			ps = con.queryPrecompilado(query);
			log.debug("qrySentencia= " + query);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, Integer.parseInt(ic_pyme));
			ps.setInt(3, Integer.parseInt(ic_financiera));
			ps.setInt(4, Integer.parseInt(ic_moneda));
			rs = ps.executeQuery();
			if(rs.next()) {
				ic_cuenta_b = rs.getString(1)==null?"":rs.getString(1);
				cg_numcta	= rs.getString(2)==null?"":rs.getString(2);
				cg_sucursal	= rs.getString(3)==null?"":rs.getString(3);
				ic_plaza	= rs.getString(4)==null?"":rs.getString(4);
				CtasCapturadas=ic_cuenta_b;
			}
			else {
				ic_cuenta_b	= "";
			}
		}
		try {
						String qryAltaCuenta =
							" INSERT INTO com_cuenta_aut_x_prod ("   +
							"    ic_cuenta_bancaria,"   +
							"    ic_if,"   +
							"    ic_epo,"   +
							"    ic_producto_nafin,"   +
							"    cs_vobo_if,"   +
							"    cs_borrado,"   +
							"    df_vobo_if"   +
							" ) SELECT ic_cuenta_bancaria,"   +
							"    ic_if,"   +
							"    ic_epo,"   +
							"    4,"   +
							"    'S',"   +
							"    'N',"   +
							"    TRUNC(SYSDATE) "   +
							" FROM comrel_cuenta_bancaria_x_prod "   +
							" WHERE ic_cuenta_bancaria IN ("+CtasCapturadas+") "   +
							" ORDER BY 1 "  ;
						log.debug("qrySentencia= " + qryAltaCuenta);
						con.ejecutaSQL(qryAltaCuenta);
						con.terminaTransaccion(true);
						CuentaOK = true;
						existeError = false;
						strMensaje = "La autorizaci�n se ha realizado satisfactoriamente.";

						if(CuentaOK) {
							boolean req9 = false;
							boolean req3_6 = false;
							boolean req4 = false;
							String query1 =
								" SELECT  ic_epo,ic_pyme FROM comrel_cuenta_bancaria_x_prod "   +
								" WHERE ic_cuenta_bancaria IN ("+CtasCapturadas+") ";
	//out.println("<br>query1: "+query1);
							ps1 = con.queryPrecompilado(query1);
							ResultSet rs1 = ps1.executeQuery();
							while(rs1.next()) {
								ic_epo = rs1.getString("ic_epo")==null?"":rs1.getString("ic_epo");
								ic_pyme = rs1.getString("ic_pyme")==null?"":rs1.getString("ic_pyme");
								query =
									" SELECT  cs_habilitado FROM comrel_pyme_epo_x_producto "   +
									" WHERE ic_pyme = ? "   +
									" AND ic_epo = ? "   +
									" AND ic_producto_nafin = 4"  ;
	//out.println("<br>query: "+query+" "+ic_pyme+" "+ic_epo);
								ps = con.queryPrecompilado(query);
								ps.setInt(1, Integer.parseInt(ic_pyme));
								ps.setInt(2, Integer.parseInt(ic_epo));
								rs = ps.executeQuery();
								if(rs.next())
									if("S".equals(rs.getString(1)))
											req9 = true;
								rs.close();
								if(ps != null) ps.close();
								if(!req9) {
									query =
										" SELECT cs_aceptacion_pyme"   +
										"  FROM dis_linea_credito_dm lc, comrel_pyme_epo_x_producto pe "   +
										" WHERE TRUNC(lc.df_vencimiento) >= TRUNC(SYSDATE) "   +
										" AND lc.cg_tipo_solicitud IN ('I', 'A', 'R') "   +
										" AND pe.ic_epo = lc.ic_epo "   +
										" AND lc.ic_producto_nafin = 4 "   +
										" AND lc.ic_epo = ? "   +
										" AND pe.ic_pyme = ? "   +
										" AND pe.ic_producto_nafin = 4 "   +
										" AND pe.cg_tipo_credito = 'D'"  ;
	//out.println("<br>query: "+query+" "+ic_pyme+" "+ic_epo);
									ps = con.queryPrecompilado(query);
									ps.setInt(1, Integer.parseInt(ic_epo));
									ps.setInt(2, Integer.parseInt(ic_pyme));
									rs = ps.executeQuery();
									if(rs.next()) {
										if(rs.getString(1).equals("S"))
												req3_6 = true;
									}
									rs.close();
									if(ps != null) ps.close();
									if(req3_6) {
										query =
											" SELECT TO_CHAR(MAX(te.df_captura), 'dd/mm/yyyy') fecha_captura "   +
											" FROM com_tasa_if_epo te "   +
											" WHERE te.ic_producto_nafin = 4 "   +
											" AND te.ic_epo = ? UNION ALL "   +
											" SELECT 's/f' fecha_captura "   +
											" FROM comcat_epo e "   +
											" WHERE NOT EXISTS (SELECT 1"   +
											"  FROM com_tasa_if_epo ie, comcat_epo e2 "   +
											" WHERE ie.ic_epo = e2.ic_epo "   +
											" AND e.ic_epo = e2.ic_epo) "   +
											" AND e.ic_epo = ?"  ;
	//out.println("<br>query: "+query+" "+ic_pyme+" "+ic_epo);
										ps = con.queryPrecompilado(query);
										ps.setInt(1, Integer.parseInt(ic_epo));
										ps.setInt(2, Integer.parseInt(ic_epo));
										rs = ps.executeQuery();
										if(rs.next())
												req4 = true;
										rs.close();
										if(ps != null) ps.close();
									}//if(req3_6)
									if(req3_6&&req4) {
										try {
											query =
												" UPDATE comrel_pyme_epo_x_producto "   +
												" SET cs_habilitado = 'S', "   +
												"     df_habilitado = SYSDATE "   +
												" WHERE ic_pyme = "+ic_pyme+" "   +
												" AND ic_epo = "+ic_epo+" "   +
												" AND ic_producto_nafin = 4"  ;
											log.debug("qrySentencia= " + query);											con.ejecutaSQL(query);
											con.terminaTransaccion(true);
										}catch(Exception e){
											con.terminaTransaccion(false);
											existeError = true;
											strMensaje = "Hubo un error al dar de alta la cuenta";
										}

									}//if(req3_6&&req4)
								}//if(!req9)
							}
							rs1.close();
							if(ps1 != null) ps1.close();
						}

						CtasCapturadas = "";
					}catch(Exception e){
						con.terminaTransaccion(false);
						e.printStackTrace();
						strMensaje = "Hubo un error al dar de alta la cuenta";
					}
			}catch(Exception e){
			//String strErr = e.toString();

		}finally{
			con.cierraConexionDB();
		}
			String arr[]={strMensaje,existeError+"",ic_cuenta_b+""};

			return arr;
	 }

	  public String[] aceptaCuenta(String ic_cuenta_b,String cg_sucursal,String cg_numcta,String ic_plaza){
		log.info("InsertarCuenta");
		AccesoDB con = new AccesoDB();
		String strMensaje = "";
		boolean CuentaOK	 	= false;
		boolean existeError 	= true;

	 try {
							con.conexionDB();
							String qryAltaCuenta =
								" Update comrel_cuenta_bancaria_x_prod "   +
								" Set	cg_sucursal = "+cg_sucursal+","   +
								"		cg_numero_cuenta = "+cg_numcta+","   +
								"		ic_plaza="+ic_plaza+" "   +
								" Where ic_cuenta_bancaria in ("+ic_cuenta_b+")";
							log.debug("qrySentencia= " + qryAltaCuenta);
							con.ejecutaSQL(qryAltaCuenta);
							con.terminaTransaccion(true);
							CuentaOK = true;
							existeError = false;
							strMensaje = "La cuenta ha sido actualizada";
						}catch(Exception e){
							con.terminaTransaccion(false);
							strMensaje = "Hubo un error al dar de alta la cuenta";
						}
			String arr[]={strMensaje,existeError+"",ic_cuenta_b+""};
			return arr;
		}


	/**
	 * metodo para Insertar cuentas bancarias
	 * retorna un boleano si la accion se realizo correcta true, de lo
	 * contrario false
	 * @return boolean
	 * @param ic_epo
	 */

	 public String[] insertaCuentaBancaria(String ic_financiera,String ic_if,String ic_moneda,String ic_epo,
	 String cg_rfc,String ic_nafinElec,String ic_pyme,String cg_sucursal,String cg_numcta,String ic_plaza){
		log.info("InsertarCuenta");
		String query;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		String CtasCapturadas="";
		AccesoDB con = new AccesoDB();
		String ic_cuenta_b="";
		String CveCtaB="";
		String strMensaje = "";
		boolean existeCuenta 	= false;
		boolean CuentaOK	 	= false;
		boolean existeError 	= true;
		PreparedStatement ps1 = null;
		try {
			con.conexionDB();

			if(!"".equals(ic_epo)&&!"".equals(ic_pyme)&&!"".equals(ic_moneda)&&!"".equals(ic_financiera)) {
			query =
				" SELECT ic_cuenta_bancaria,cg_numero_cuenta,cg_sucursal,ic_plaza "   +
				" FROM comrel_cuenta_bancaria_x_prod "   +
				" WHERE ic_epo = ? "   +
				" AND ic_pyme = ? "   +
				" AND ic_financiera = ? "   +
				" AND ic_moneda = ?"  ;
			ps = con.queryPrecompilado(query);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, Integer.parseInt(ic_pyme));
			ps.setInt(3, Integer.parseInt(ic_financiera));
			ps.setInt(4, Integer.parseInt(ic_moneda));
			rs = ps.executeQuery();
			if(rs.next()) {
				ic_cuenta_b = rs.getString(1)==null?"":rs.getString(1);
			}	else {
				ic_cuenta_b	= "";
			}
		}

		if("".equals(ic_cuenta_b)) {
						String qryTraeCuentas2 =
							" SELECT COUNT( * ),'15admnafinctabancpyme' "   +
							" FROM comrel_cuenta_bancaria_x_prod"   +
							" WHERE ic_moneda = ? "   +
							" AND ic_pyme = ? "   +
							" AND ic_epo = ? "   ;
						qryTraeCuentas2 += (!"".equals(ic_if))?" AND ic_if = ? ":" AND ic_financiera = ? ";
						qryTraeCuentas2 +=
							" AND ic_producto_nafin = 4"   +
							" AND cs_borrado = 'N'"  ;
						ps1 = con.queryPrecompilado(qryTraeCuentas2);
						ps1.setInt(1, Integer.parseInt(ic_moneda));
						ps1.setInt(2, Integer.parseInt(ic_pyme));
						ps1.setInt(3, Integer.parseInt(ic_epo));
						ps1.setInt(4, Integer.parseInt((!"".equals(ic_if))?ic_if:ic_financiera));

						ResultSet rsTraeCuentas2 = ps1.executeQuery();
						if(rsTraeCuentas2.next())
							if(rsTraeCuentas2.getInt(1)>0)
									existeCuenta = true;
						rsTraeCuentas2.close();
						if(ps1 != null) ps1.close();
						if (existeCuenta)
							strMensaje = "Ya existe una cuenta para ese Distribuidor";
						else {
							 CveCtaB = "0";
							String qryCveCtaB =
								" SELECT MAX(ic_cuenta_bancaria) AS cve "   +
								" FROM comrel_cuenta_bancaria_x_prod"  ;
							ResultSet rsCveCtaB = con.queryDB(qryCveCtaB);
							if(rsCveCtaB.next())
								CveCtaB = Integer.toString(rsCveCtaB.getInt(1) + 1);
							rsCveCtaB.close();
							if("".equals(CtasCapturadas))
								CtasCapturadas = CveCtaB;
							else
								CtasCapturadas += ","+CveCtaB;
							try {
								String qryAltaCuenta =
									" INSERT INTO comrel_cuenta_bancaria_x_prod ("   +
									"    ic_cuenta_bancaria,"   +
									"    ic_producto_nafin,"   +
									"    ic_epo,"   +
									"    ic_pyme,"   ;
								qryAltaCuenta += (!"".equals(ic_if))?"    ic_if,":"";
								qryAltaCuenta +=
									"    ic_financiera,"+
									"    cg_sucursal,"   +
									"    ic_moneda,"   +
									"    cg_numero_cuenta,"   +
									"    ic_plaza,"   +
									"    cs_borrado"   +
									" ) "   +
									" VALUES("   +
									"    "+CveCtaB+","   +
									"    4,"   +
									"    "+ic_epo+","   +
									"    "+ic_pyme+","   ;
								qryAltaCuenta += (!"".equals(ic_if))?"    "+ic_if+",":"";
								qryAltaCuenta +=
									"    "+ic_financiera+","+
									"    '"+cg_sucursal+"',"   +
									"    "+ic_moneda+","   +
									"    '"+cg_numcta+"',"   +
									"    "+ic_plaza+","   +
									"    'N'"   +
									" )"  ;
								log.debug("qrySentencia= " + qryAltaCuenta);
								con.ejecutaSQL(qryAltaCuenta);
								con.terminaTransaccion(true);
								existeError = false;
								strMensaje = "La cuenta ha sido dada de alta";
							}catch(Exception e){
								con.terminaTransaccion(false);
								strMensaje = "Hubo un error al dar de alta la cuenta";
							}
						}//if (existeCuenta)
					} //if("".equals(ic_cuenta_b))
					else {
					 CveCtaB = ic_cuenta_b;
						if("".equals(CtasCapturadas))
							CtasCapturadas = CveCtaB;
						else
							CtasCapturadas += ","+CveCtaB;
						try {
							String qryAltaCuenta =
								" Update comrel_cuenta_bancaria_x_prod "   +
								" Set	cg_sucursal = "+cg_sucursal+","   +
								"		cg_numero_cuenta = "+cg_numcta+","   +
								"		ic_plaza="+ic_plaza+" "   +
								" Where ic_cuenta_bancaria in ("+ic_cuenta_b+")";
							log.debug("qrySentencia= " + qryAltaCuenta);
							con.ejecutaSQL(qryAltaCuenta);
							con.terminaTransaccion(true);
							CuentaOK = true;
							existeError = false;
							strMensaje = "La cuenta ha sido actualizada";
						}catch(Exception e){
							con.terminaTransaccion(false);
							strMensaje = "Hubo un error al dar de alta la cuenta";
						}
					}//else if("".equals(ic_cuenta_b))
		}catch(Exception e){
			//String strErr = e.toString();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info(strMensaje);
		}
		String arr[]={strMensaje,existeError+"",CveCtaB+""};
			return arr;

	 }



	 public String[] eliminarCuenta(String ic_financiera,String ic_if,String ic_moneda,String ic_epo,
	 String cg_rfc,String ic_nafinElec,String ic_pyme,String CtasCapturadas,String cg_sucursal,String cg_numcta,String ic_plaza)	{

		log.info("accionCuenta");
		String query;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String ic_cuenta_b="";
		String strMensaje = "";
		boolean CuentaOK	 	= false;
		boolean existeError 	= true;
		try {
			con.conexionDB();


			ic_cuenta_b=CtasCapturadas;



		try {
					query =
						" SELECT count(*)"   +
						" FROM com_cuenta_aut_x_prod "   +
						" WHERE ic_cuenta_bancaria = ? "  ;
						log.debug("qrySentencia= " + query);
					ps = con.queryPrecompilado(query);
					ps.setInt(1, Integer.parseInt(ic_cuenta_b));
					rs = ps.executeQuery();
					int cuentauto = 0;
					if(rs.next())
						cuentauto = rs.getInt(1);
					rs.close();
					if(ps!=null) ps.close();
					if(cuentauto==0) {
						String cadaux = "";
						StringTokenizer st = new StringTokenizer(CtasCapturadas,",");
						CtasCapturadas = "";
						while (st.hasMoreTokens()) {
							cadaux = st.nextToken();
							if(!ic_cuenta_b.equals(cadaux)) {
								if("".equals(CtasCapturadas))
									CtasCapturadas = cadaux;
								else
									CtasCapturadas += ","+cadaux;
							}
						}
						query =
							" DELETE FROM com_cuenta_aut_x_prod "   +
							" WHERE ic_cuenta_bancaria = "+ic_cuenta_b+" "  ;
						con.ejecutaSQL(query);
						log.debug("qrySentencia= " + query);
						con.terminaTransaccion(true);
						query =
							" DELETE FROM comrel_cuenta_bancaria_x_prod "   +
							" WHERE ic_cuenta_bancaria = "+ic_cuenta_b+" "  ;
						con.ejecutaSQL(query);
						log.debug("qrySentencia= " + query);
						con.terminaTransaccion(true);
						strMensaje = "La cuenta ha sido Eliminada";
					}
					else
						strMensaje = "No puede borrar una cuenta que ya ha sido autorizada.";
					CuentaOK = true;
					existeError = false;



					}catch(Exception e){
						con.terminaTransaccion(false);
						strMensaje = "Hubo un error al eliminar la cuenta";
					}
			}catch(Exception e){
			//String strErr = e.toString();

		}finally{
			con.cierraConexionDB();
		}
		String arr[]={strMensaje,existeError+"",ic_cuenta_b+""};
			return arr;
	 }

	/**
	 *
	 * @return
	 * @param ic_epo
	 * @param ic_pyme
	 */
	public String getTiposCredito(String ic_pyme, String ic_epo)	{
	log.info("getTiposCredito");

		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String cgTiposCredito = "";

		try {
      con.conexionDB();
		qrySentencia = new StringBuffer();
      qrySentencia.append("	SELECT CG_TIPO_CREDITO as VALOR	FROM COMREL_PYME_EPO_X_PRODUCTO" +
								  "  WHERE IC_PRODUCTO_NAFIN = ? ");
		if(!ic_pyme.equals("")) {
			qrySentencia.append("  AND IC_PYME =  "+Integer.parseInt(ic_pyme));
		}
		if(!ic_epo.equals("")) {
			qrySentencia.append("  AND IC_EPO =  "+Integer.parseInt(ic_epo));
		}

		log.debug("qrySentencia  "+qrySentencia);

		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, 4);
		rs = ps.executeQuery();
		while(rs.next()){
			 cgTiposCredito = (rs.getString("VALOR")==null)?"":rs.getString("VALOR");
		}
		rs.close();
      ps.close();

		} catch(Exception e) {
			log.error("getTiposCredito(Exception) "+e);
			throw new AppException("Error al obtener el tipo de cr�dito",e);
		} finally {
			con.cierraConexionDB();
			log.info(" getTiposCredito");
		}

		return cgTiposCredito;
	}

	/**
	 * Obtiene los parametros por epo para distribuidores dependiendo de la seccion
	 * @throws netropology.utilerias.AppException
	 * @return HashMap
	 * @param cveSeccion - indica la seccion de la que se obtendran los parametros
	 * @param cveEpo - clave que identifica a la epo de la que se obtendram los parametros
	 */
	public List getParametrosEpo(String cveEpo, String cveSeccion) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		List lstParams = new ArrayList();
		HashMap hmParams = new HashMap();
		String strSQL = "";
		try{
			con.conexionDB();

			if("1".equals(cveSeccion)){
				strSQL = "SELECT pe.cg_tipos_credito AS tipo_credito_operar, " +
							"       DECODE (pe.cg_responsable_interes, " +
							"               'E', 'EPO', " +
							"               'D', 'Distribuidor' " +
							"              ) AS resposabilidad_pago_interes, " +
							"       DECODE (pe.cg_tipo_cobranza, " +
							"               'D', 'Delegada', " +
							"               'N', 'No Delegada' " +
							"              ) AS tipo_cobranza, " +
							"       tci.cd_descripcion AS esquema_pago_interes, " +
							"       vtf.cd_descripcion AS modalidad_plazo_operar " +
							"  FROM comrel_producto_epo pe, " +
							"       comcat_tipo_cobro_interes tci, " +
							"       (SELECT tf.cd_descripcion, etf.ic_epo, etf.ic_producto_nafin " +
							"          FROM comcat_tipo_financiamiento tf, " +
							"               comrel_epo_tipo_financiamiento etf " +
							"         WHERE etf.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
							"           AND etf.ic_producto_nafin = 4) vtf " +
							" WHERE pe.ic_producto_nafin = 4 " +
							"   AND pe.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes(+) " +
							"   AND pe.ic_producto_nafin = vtf.ic_producto_nafin(+) " +
							"   AND pe.ic_epo = vtf.ic_epo(+) " +
							"   AND pe.ic_epo = ? ";

				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1, Long.parseLong(cveEpo));
				rs = ps.executeQuery();

				if(rs.next()){
					String tipoCreditoOperar = (rs.getString("TIPO_CREDITO_OPERAR")==null)?" ":(rs.getString("TIPO_CREDITO_OPERAR"));
					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "Tipo de cr�dito a operar");
					hmParams.put("TIPODATO", "C");
					if("D".equals(tipoCreditoOperar)){
						hmParams.put("VALORES", "Descuento y/o Factoraje");
					}
					if("C".equals(tipoCreditoOperar)){
						hmParams.put("VALORES", "Cr�dito en cuenta corriente");
					}
					if("A".equals(tipoCreditoOperar)){
						hmParams.put("VALORES", "Descuento");
					}

					lstParams.add(hmParams);

					if(tipoCreditoOperar.equals("D")||tipoCreditoOperar.equals("A")){
						hmParams = new HashMap();
						hmParams.put("NOMBREPARAM", "Responsabilidad de Pago de Inter�s(Solo si opera DM)");
						hmParams.put("TIPODATO", "C");
						hmParams.put("VALORES", (rs.getString("RESPOSABILIDAD_PAGO_INTERES")==null)?" ":(rs.getString("RESPOSABILIDAD_PAGO_INTERES")));
						lstParams.add(hmParams);
					}

					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "Modalidad de plazo a operar");
					hmParams.put("TIPODATO", "C");
					hmParams.put("VALORES", (rs.getString("MODALIDAD_PLAZO_OPERAR")==null)?" ":(rs.getString("MODALIDAD_PLAZO_OPERAR")));
					lstParams.add(hmParams);

					if(tipoCreditoOperar.equals("D")||tipoCreditoOperar.equals("A")){
						hmParams = new HashMap();
						hmParams.put("NOMBREPARAM", "Tipo de cobranza(Solo si opera DM)");
						hmParams.put("TIPODATO", "C");
						hmParams.put("VALORES", (rs.getString("TIPO_COBRANZA")==null)?" ":(rs.getString("TIPO_COBRANZA")));
						lstParams.add(hmParams);
					}

					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "Esquema de pago de intereses");
					hmParams.put("TIPODATO", "C");
					hmParams.put("VALORES", (rs.getString("ESQUEMA_PAGO_INTERES")==null)?" ":(rs.getString("ESQUEMA_PAGO_INTERES")));
					lstParams.add(hmParams);
				}
				rs.close();
				ps.close();

			}else if("2".equals(cveSeccion)){

				strSQL = "SELECT pe.ig_dias_minimo AS dias_minimos, " +
							"       pe.cg_tipos_credito AS tipo_credito_operar, " +
							"       pe.ig_dias_maximo AS dias_maximos, " +
							"       DECODE (pe.cg_tipo_conversion, " +
							"               'P', 'Peso - Dolar', " +
							"               '', 'Sin conversion' " +
							"              ) AS tipo_conversion, " +
							"       DECODE (pe.cs_cifras_control, " +
							"               'S', 'Si mostrar', " +
							"               'N', 'No mostrar' " +
							"              ) AS pantalla_cifras_control, " +
							"       pe.ig_plazo_dscto, pe.fn_porcentaje_dscto, " +
							"       pe.ig_dias_aviso_venc AS ig_dias_aviso_venc, pe.ig_dias_amp_fecha_venc " +
							"  FROM comrel_producto_epo pe " +
							" WHERE pe.ic_producto_nafin = 4 AND pe.ic_epo = ? ";

				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1, Long.parseLong(cveEpo));
				rs = ps.executeQuery();

				if(rs.next()){
					String plazoDscto = (rs.getString("IG_PLAZO_DSCTO")==null)?" ":(rs.getString("IG_PLAZO_DSCTO"));
					String porcentDscto = (rs.getString("FN_PORCENTAJE_DSCTO")==null)?" ":(rs.getString("FN_PORCENTAJE_DSCTO"));

					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "Plazo m�nimo del financiamiento");
					hmParams.put("TIPODATO", "N");
					hmParams.put("VALORES", (rs.getString("DIAS_MINIMOS")==null)?" ":(rs.getString("DIAS_MINIMOS")));
					lstParams.add(hmParams);

					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "Plazo m�ximo del financiamiento");
					hmParams.put("TIPODATO", "N");
					hmParams.put("VALORES", (rs.getString("DIAS_MAXIMOS")==null)?" ":(rs.getString("DIAS_MAXIMOS")));
					lstParams.add(hmParams);

					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "D�as M�ximos para Ampliar la Fecha de Vencimiento");
					hmParams.put("TIPODATO", "N");
					hmParams.put("VALORES", (rs.getString("IG_DIAS_AMP_FECHA_VENC")==null)?" ":(rs.getString("IG_DIAS_AMP_FECHA_VENC")));
					lstParams.add(hmParams);

					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "Tipo de conversi�n");
					hmParams.put("TIPODATO", "C");
					hmParams.put("VALORES", (rs.getString("TIPO_CONVERSION")==null)?" ":(rs.getString("TIPO_CONVERSION")));
					lstParams.add(hmParams);

					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "Operar con pantalla de cifras de control");
					hmParams.put("TIPODATO", "C");
					hmParams.put("VALORES", (rs.getString("PANTALLA_CIFRAS_CONTROL")==null)?" ":(rs.getString("PANTALLA_CIFRAS_CONTROL")));
					lstParams.add(hmParams);

					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "Plazo m�ximo para tomar el descuento, Pocentaje de descuento que otorga la EPO");
					hmParams.put("TIPODATO", "N");
					hmParams.put("VALORES", "Plazo: "+plazoDscto+"  Porcentaje: "+porcentDscto+"%" );
					lstParams.add(hmParams);

					hmParams = new HashMap();
					hmParams.put("NOMBREPARAM", "Aviso de pr�ximos vencimientos en:");
					hmParams.put("TIPODATO", "N");
					hmParams.put("VALORES", ((rs.getString("IG_DIAS_AVISO_VENC")==null)?" ":(rs.getString("IG_DIAS_AVISO_VENC"))) +" d�as h�biles");
					lstParams.add(hmParams);

				}
				rs.close();
				ps.close();
			}

			return lstParams;

		}catch(Throwable t){
			throw new AppException("Error al obtener parametros por EPO por seccion", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}
	/**
	 * Funci�n que guarda  el valor de aforo en la tabla dis_aforo_epo y lo inserta en la BIT_CAMBIOS_GRAL
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param strNombreUsuario
	 * @param iNoNafinElectronico
	 * @param strLogin
	 * @param aforo
	 * @param ic_moneda
	 * @param ic_epo
	 * @param ic_if
	 */

	public boolean guardaAforoPorEPO(String ic_if, String ic_epo,String ic_moneda, String aforo, String strLogin, Long iNoNafinElectronico, String strNombreUsuario) throws AppException {
		AccesoDB con = new AccesoDB();
		String 		strSQLCondicion		= "";
		String 		strSQL		= "";
		ResultSet existe = null;
		StringBuffer insertBitacoraGral = null;
		log.debug("guardaAforoPorEPO(E)");
		boolean commit = true;
		try{
			con.conexionDB();
			strSQLCondicion=" SELECT  * FROM dis_aforo_epo WHERE IC_IF = "+ic_if+"  AND IC_EPO =  "+ic_epo+" AND IC_MONEDA = "+ic_moneda+" ";
			existe = con.queryDB(strSQLCondicion);
			if(existe.next()){
				strSQL ="UPDATE dis_aforo_epo  "+
						  "SET  FN_VALOR_AFORO= "+aforo+" ,"+
						  " CG_USUARIO_MODIFICA = '"+strNombreUsuario+"' ,"+
						  " DF_MODIFICA = sysdate  "+
						  " WHERE IC_IF = "+ic_if+" AND IC_EPO = "+ic_epo+" AND IC_MONEDA = "+ic_moneda+" ";
				log.debug("query: : "+strSQL);
				con.ejecutaSQL(strSQL);
				insertBitacoraGral = new StringBuffer();
				insertBitacoraGral.append(
					"\n INSERT INTO BIT_CAMBIOS_GRAL (  " +
					"\n IC_CAMBIO,   " +
					"\n CC_PANTALLA_BITACORA,   " +
					"\n CG_CLAVE_AFILIADO,  " +
					"\n IC_NAFIN_ELECTRONICO,  " +
					"\n DF_CAMBIO,  " +
					"\n IC_USUARIO,  " +
					"\n CG_NOMBRE_USUARIO,  " +
					"\n CG_ANTERIOR,  " +
					"\n CG_ACTUAL )   " +
					"\n VALUES(   " +
					"\n SEQ_BIT_CAMBIOS_GRAL.nextval,  " +
					"\n 'AFORO_EPO',  " +
					"\n 'I',  " +
					"\n "+iNoNafinElectronico+",  " +
					"\n SYSDATE,  " +
					"\n '"+strLogin+"',   " +
					"\n '"+strNombreUsuario+"',  " +
					"\n ' "+ ic_epo+" - "+  ic_moneda +" -  Aforo = 80',   " +
					"\n '"+ ic_epo +" - "+   ic_moneda +" - Aforo = 90')   "
					);
				log.debug("insertBitacoraGral  : "+insertBitacoraGral.toString());
				con.ejecutaSQL(insertBitacoraGral.toString());
			}else{
				strSQL ="INSERT INTO dis_aforo_epo(  IC_IF, IC_EPO, IC_MONEDA, FN_VALOR_AFORO, CG_USUARIO_ALTA, DF_ALTA, CG_USUARIO_MODIFICA, DF_MODIFICA     ) "+
							"VALUES ( "+ic_if+" , "+ic_epo+" , "+ic_moneda+" ,  "+aforo+"  , '"+strNombreUsuario+"' ,sysdate,'' ,'')";
				log.debug("strSQL  : "+strSQL);
				con.ejecutaSQL(strSQL);
				insertBitacoraGral = new StringBuffer();
				insertBitacoraGral.append(
					"\n INSERT INTO BIT_CAMBIOS_GRAL (  " +
					"\n IC_CAMBIO,   " +
					"\n CC_PANTALLA_BITACORA,   " +
					"\n CG_CLAVE_AFILIADO,  " +
					"\n IC_NAFIN_ELECTRONICO,  " +
					"\n DF_CAMBIO,  " +
					"\n IC_USUARIO,  " +
					"\n CG_NOMBRE_USUARIO,  " +
					"\n CG_ANTERIOR,   " +
					"\n CG_ACTUAL )   " +
					"\n VALUES(    " +
					"\n SEQ_BIT_CAMBIOS_GRAL.nextval,   " +
					"\n 'AFORO_EPO',   " +
					"\n 'I',   " +
					"\n "+iNoNafinElectronico+",   " +
					"\n SYSDATE,   " +
					"\n '"+strLogin+"',    " +
					"\n '"+strNombreUsuario+"',   " +
					"\n ' ',   " +
					"\n '"+ic_epo +" - "+ ic_moneda +" - Aforo = 80 ') "
				);
				log.debug("insertBitacoraGral alta : "+insertBitacoraGral.toString());
				con.ejecutaSQL(insertBitacoraGral.toString());
			}
		}catch(Exception e){
			commit= false;
			e.printStackTrace();
			throw new AppException("Error al confirmar carga de liberacion masiva de ctas pyme",e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
		}
		log.debug("guardaAforoPorEPO(S)");
		return commit;
	}


	public List getBinsIFxEpo(String cveEpo, String cveMoneda)throws AppException {
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer qrySQL = new StringBuffer();
		List lstBins = new ArrayList();
		try{
			con.conexionDB();

			qrySQL.append("SELECT DISTINCT ci.ic_if AS clave, ci.cg_razon_social AS descripcion, ");
			qrySQL.append("                cbi.ic_bins cveBin, cbi.cg_descripcion descBin, cbi.cg_codigo_bin as cg_codigo_bin ");
			qrySQL.append("           FROM comcat_if ci, ");
			qrySQL.append("                comrel_if_epo rie, ");
			qrySQL.append("                comrel_if_epo_x_producto iexp, ");
			qrySQL.append("                com_bins_if cbi ");
			qrySQL.append("          WHERE ci.ic_if = rie.ic_if ");
			qrySQL.append("            AND iexp.ic_if = ci.ic_if ");
			qrySQL.append("            AND iexp.ic_epo = rie.ic_epo ");
			qrySQL.append("            AND rie.ic_if = cbi.ic_if ");
			qrySQL.append("            AND rie.ic_epo = ? ");
			qrySQL.append("            AND iexp.ic_producto_nafin = ? ");
			qrySQL.append("            AND rie.cs_distribuidores = ? ");
			qrySQL.append("            AND cbi.cg_estatus = ? ");
			qrySQL.append("            AND cbi.ic_moneda = ? ");
			qrySQL.append("       ORDER BY ci.cg_razon_social, cbi.cg_descripcion ");

			ps = con.queryPrecompilado(qrySQL.toString());
			ps.setLong(1, Long.parseLong(cveEpo));
			ps.setLong(2, Long.parseLong("4"));
			ps.setString(3, "S");
			ps.setString(4, "A");
			ps.setLong(5, Long.parseLong(cveMoneda));
			rs = ps.executeQuery();

			while(rs.next()){
				HashMap hmBins = new HashMap();
				hmBins.put("clave",rs.getString("cveBin")==null?"":rs.getString("cveBin"));
				hmBins.put("descripcion",rs.getString("descBin")==null?"":(rs.getString("cg_codigo_bin")+"-"+rs.getString("descBin")));
				hmBins.put("claveIf",rs.getString("clave")==null?"":rs.getString("clave"));
				hmBins.put("descripcionIf",rs.getString("descripcion")==null?"":rs.getString("descripcion"));
				lstBins.add(hmBins);
			}

			rs.close();
			ps.close();

			return lstBins;
		}catch(Throwable t){
			throw new AppException("Error al consultar  Tarjetras de Credito de IFS(Bin's)...", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

	}

	/**
	 *
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param cvePyme
	 * @param cveEpo
	 */
	public String  getLimiteAcumulado(String cveEpo, String cvePyme)throws AppException {
		log.info("getLimiteAcumulado(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer qrySQL = new StringBuffer();
		List lVarBind		= new ArrayList();
		String totalAcumulado ="0";
		try{
			con.conexionDB();

			qrySQL.append(" select  sum(sel.fn_importe_recibir + sel.fn_importe_interes) as total   "+
                " FROM dis_documento doc, "+
					 " dis_docto_seleccionado sel  "+
                " WHERE doc.ic_documento  = sel.ic_documento   "+
					 " AND doc.ic_estatus_docto  in ( ?, ?  )   "+
                "  and doc.ic_epo =  ? "+
					 " and doc.ic_pyme = ?  ");

			lVarBind		= new ArrayList();
			lVarBind.add("3");
         lVarBind.add("4");
			lVarBind.add(cveEpo);
			lVarBind.add(cvePyme);

			log.debug("qrySQL----->"+qrySQL);
			log.debug("lVarBind----->"+lVarBind);

			ps = con.queryPrecompilado(qrySQL.toString(), lVarBind);
			rs = ps.executeQuery();

			if(rs.next()){
				totalAcumulado= 	rs.getString("total")==null?"0":rs.getString("total");
			}

			rs.close();
			ps.close();


		  }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en   (S)" +e);
         throw new RuntimeException(e);
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			log.info("getLimiteAcumulado(S)");
      }
			return totalAcumulado;
	}


	/**
	 * Fodea 032-204
	 * @return
	 * @param ic_epo
	 */
	public String getFirmaMancomunada(String ic_epo)	{
		log.info("getFirmaMancomunada");
		String resultado = "";
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

			qrySentencia.append(" Select CS_OPERA_FIRMA_MANC " +
										" from COMREL_PRODUCTO_EPO "+
										"where ic_producto_nafin = ?  and ic_epo = ? ");

			log.debug("qrySentencia= " + qrySentencia.toString());

			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,4);
			ps.setInt(2,Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			if (rs.next()){
				resultado = (rs.getString("CS_OPERA_FIRMA_MANC")==null)?"N":rs.getString("CS_OPERA_FIRMA_MANC");
			}
		} catch(Exception e) {
			log.error("getFirmaMancomunada(Exception) "+e);
			throw new AppException("Error  al validar si la epo opera Firma Mancomunada ",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getFirmaMancomunada");
		}
		return resultado;
	}//fin metodo

	/**
	 * Obtiene el Numero de Afiliacion de Comercio de la tabla com_parametrizacion_epo
	 * @return CG_AFILIADO_COMERCIO
	 * @param ic_epo
	 */
	public String getAfiliadoComercio(String ic_epo){

		log.info("getAfiliadoComercio (E)");
		StringBuffer qrySentencia = new StringBuffer("");
		String valor = "";
		ResultSet rs = null;
		AccesoDB con = null;

		try{
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia.append(" SELECT CG_VALOR");
			qrySentencia.append(" FROM com_parametrizacion_epo");
			qrySentencia.append(" WHERE  cc_parametro_epo = 'CG_AFILIADO_COMERCIO'");
			qrySentencia.append(" AND ic_epo = " + ic_epo);

			log.debug("Sentencia: " + qrySentencia.toString());

			rs = con.queryDB(qrySentencia.toString());
			if(rs.next()){
				valor = ((rs.getString("CG_VALOR")==null)?"":rs.getString("CG_VALOR"));
			} else{
				valor = "";
			}
			log.debug("valor"  +valor);
			con.cierraStatement();
		} catch(Exception e){
			con.terminaTransaccion(false);
			log.error("getAfiliadoComercio(Exception): " + e);
		} finally{
			con.cierraConexionDB();
		}
		log.info("getAfiliadoComercio (S)");
		return valor;
	}

	/**
	 * Actualiza el Numero de Afiliacion de Comercio en la tabla com_parametrizacion_epo
	 * @return true: operaci�n exitosa. false: caso contrario
	 * @param valor
	 * @param ic_epo
	 */
	public boolean setAfiliadoComercio(String ic_epo, String valor){

		log.info("setAfiliadoComercio (E)");
		StringBuffer qryDelete = new StringBuffer("");
		StringBuffer qryInsert = new StringBuffer("");
		AccesoDB con           = null;
		PreparedStatement ps   = null;
		boolean transaccion    = true;

		try{
			con = new AccesoDB();
			con.conexionDB();

			qryDelete.append(" DELETE com_parametrizacion_epo");
			qryDelete.append(" WHERE  cc_parametro_epo = 'CG_AFILIADO_COMERCIO'");
			qryDelete.append(" AND ic_epo = " + ic_epo);
			log.debug("Delete: " + qryDelete.toString());
			ps = con.queryPrecompilado(qryDelete.toString());
			ps.executeUpdate();
			ps.close();
			con.cierraStatement();

			qryInsert.append(" INSERT INTO com_parametrizacion_epo");
			qryInsert.append(" (IC_EPO, CC_PARAMETRO_EPO, CG_VALOR)");
			qryInsert.append(" VALUES(" + ic_epo + ", 'CG_AFILIADO_COMERCIO', '" + valor + "'" + ")");
			log.debug("Insert: " + qryInsert.toString());

			ps = con.queryPrecompilado(qryInsert.toString());
			ps.executeUpdate();
			ps.close();
			con.cierraStatement();

		} catch(Exception e){
			transaccion = false;
			log.error("setAfiliadoComercio(Exception): " + e);
		} finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
		}
		log.info("setAfiliadoComercio (S)");
		return transaccion;
	}

  public String getParamXepo(String ic_epo, String parametro)throws AppException	{
		log.info("getFirmaMancomunada");
		String resultado = "";
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();

			qrySentencia.append("SELECT cc_parametro_epo, cg_valor " +
                        "  FROM com_parametrizacion_epo " +
                        " WHERE cc_parametro_epo = ? AND ic_epo = ? "
                        );

			log.debug("qrySentencia= " + qrySentencia.toString());

			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setString(1, parametro);
			ps.setInt(2,Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			if (rs.next()){
				resultado = (rs.getString("cg_valor")==null)?"":rs.getString("cg_valor");
			}
      return resultado;
		} catch(Exception e) {
			log.error("getParamXepo(Exception) "+e);
			throw new AppException("Error  al validar si la epo tiene activo un parametro",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getParamXepo");
		}
	}//fin metodo

	public boolean updateParametros(String ic_if, String cg_tipo_solicitud, String ic_epo[], String autorizacion[], String fn_limite_puntos, String operaTarjeta, String cg_linea_c, String notificaDoctos, String emailNotificaDoctos) throws NafinException {
		AccesoDB lobdConexion = new AccesoDB();
		StringBuffer lsQry = new StringBuffer();
		String lsQry2 ="";
		boolean lbOK = true;
		try {
			lobdConexion.conexionDB();
			lsQry.append("UPDATE COMREL_PRODUCTO_IF "                                      );
			lsQry.append("       SET CG_TIPO_LIQUIDACION = '"   + cg_tipo_solicitud   + "'");
			lsQry.append("       , FN_LIMITE_PUNTOS = "         + fn_limite_puntos         );
			lsQry.append("       , CG_OPERA_TARJETA = '"        + operaTarjeta        + "'");
			lsQry.append("       , CG_LINEA_CREDITO  = '"       + cg_linea_c          + "'");
			lsQry.append("       , CG_NOTIFICA_DOCTOS = '"      + notificaDoctos      + "'");
			lsQry.append("       , CS_MAIL_NOTIFICA_DOCTOS = '" + emailNotificaDoctos + "'");
			lsQry.append(" WHERE IC_IF = "                      + ic_if                    );
			lsQry.append("       AND IC_PRODUCTO_NAFIN = 4 "                               );
			System.out.println(lsQry.toString());
			try{
				lobdConexion.ejecutaSQL(lsQry.toString());
				if(ic_epo!=null){
					for (int x=0;x<ic_epo.length;x++) {
						lsQry2 =
							"UPDATE comrel_if_epo_x_producto "+
							"   SET cs_autorizado_tasa = '"+autorizacion[x]+"' "+
							" WHERE ic_producto_nafin = 4 "+
							"   AND ic_if = "+ic_if+" "+
							"   AND ic_epo = "+ic_epo[x]+" ";
							//System.out.println(lsQry2);
						lobdConexion.ejecutaSQL(lsQry2);
					}
				}

			} catch (SQLException sqle){
				lbOK = false;
				throw new NafinException("ANTI0022");
			}
		} catch (NafinException ne){
			throw ne;
		} catch(Exception e){
			lbOK = false;
			System.out.println("Exception ocurrida en actualizaComRelProductoIf(). "+e);
			throw new NafinException("SIST0001");
		}
		finally {
			lobdConexion.terminaTransaccion(lbOK);
			if(lobdConexion.hayConexionAbierta()) lobdConexion.cierraConexionDB();
		}
		return lbOK;
	}
        
        
    /**
     * 
     * @param ic_epo
     * @return
     */
    public  String  obtieneOperaSinCesion(String ic_epo)    throws NafinException{

        log.info("obtieneOperaSinCesion (E) ");

                  String qrySentencia     = "";
                  ResultSet       rs                              = null;
                  AccesoDB        con                             = null;
                  String valor = "";
        boolean transaccion = true;

          try {
          con = new AccesoDB();
          con.conexionDB();
          qrySentencia =
           " SELECT CS_OPERA_SIN_CESION " +
           " FROM comrel_producto_epo   "+
           " WHERE ic_producto_nafin = 4   "+
           " AND ic_epo = "+ic_epo;

          log.debug("qrySentencia"  +qrySentencia);

          rs = con.queryDB(qrySentencia);
          if(rs.next()) {
            valor=   ((rs.getString(1)==null)?"":rs.getString(1));
           }

          con.cierraStatement();
        }catch(Exception e){
                    transaccion = false;
                                e.printStackTrace();
            log.error("obtieneTipoCredito "+e);
        } finally {
                   if(con.hayConexionAbierta()){
                                   con.terminaTransaccion(transaccion);
                                   con.cierraConexionDB();
                           }
        }
          log.info("obtieneTipoCredito (S) ");
                  return valor;
      }

}// Fin de la Clase
