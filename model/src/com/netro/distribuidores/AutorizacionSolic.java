package com.netro.distribuidores;

import netropology.utilerias.*;
import java.util.*;
import java.math.*;
import java.io.*;
import com.netro.exception.*;
import com.netro.distribuidores.ws.*;
import javax.ejb.Remote;

@Remote
public interface AutorizacionSolic {

    public Vector getLineasConDoctos(String ic_epo,String ic_if,String ig_numero_docto,String fecha_seleccion_de,String fecha_seleccion_a) throws NafinException;

    public Vector getLineasConDoctosCCC(String ic_epo,String ic_pyme,String ic_if,String ig_numero_docto,String fecha_seleccion_de,String fecha_seleccion_a) throws NafinException;

    public Vector getLineasConDoctosFF(String ic_epo,String ic_pyme,String ic_if,String ig_numero_docto,String fecha_seleccion_de,String fecha_seleccion_a);

    public Vector getDoctosSeleccionados(String ic_epo,String ic_if,String ig_numero_docto,String fecha_seleccion_de,String fecha_seleccion_a,String in) throws NafinException;

    public Vector getDoctosSeleccionadosCCC(String ic_epo,String ic_pyme,String ic_if,String ig_numero_docto,String fecha_seleccion_de,String fecha_seleccion_a,String in) throws NafinException;        

    public Vector getDoctosSeleccionadosFF(String ic_epo,String ic_pyme,String ic_if,String ig_numero_docto,String fecha_seleccion_de,String fecha_seleccion_a,String in);

    public Vector getDoctosSeleccionadosIF(String cc_acuse,String cg_tipo_credito) throws NafinException;

    public Vector getDoctosSeleccionadosNafin(String ic_if,String ic_tipo_cobro_int,String df_fecha_operacion_de,String df_fecha_operacion_a,String ic_documento,String ig_numero_docto,String ic_moneda,String ic_estatus_solic,String tipoLineaCredito,String in) throws NafinException;

    public String confirmaSolic(
	    String totalMontoMN,
	    String totalInteresMN,
	    String totalMontoDescMN,
	    String totalMontoUSD,
	    String totalInteresUSD,
	    String totalMontoDescUSD,
	    String icUsuario,
	    String cgReciboElectronico,
	    String ic_documento[],
	    String cg_tipo_credito,
	    String csTipoFondeo
    ) throws NafinException;

    public void generaReporteSirac(String in) throws NafinException;

    public Vector getDatosArchivoSirac(String in) throws NafinException;

    public Vector getDoctosSeleccionados(String ic_epo,String ic_if,String ic_pyme,String ig_numero_docto,String fecha_seleccion_de,String fecha_seleccion_a,String in,String cg_tipo_credito) throws NafinException;

    public String getCuentaAutorizada(String ic_epo,String ic_if,String ic_pyme,String ic_moneda) throws NafinException;

    public Vector getDatosCuentaAutorizada(String ic_epo,String ic_if,String ic_pyme,String ic_moneda) throws NafinException;

    public Vector getDoctosSelCCCVariableFijos(String ic_epo,String ic_pyme,String ic_if,String ic_documento,String fecha_seleccion_de,String fecha_seleccion_a) throws NafinException;

    public String confirmaSolic(
	    String totalMontoMN,
	    String totalInteresMN,
	    String totalMontoDescMN,
	    String totalMontoUSD,
	    String totalInteresUSD,
	    String totalMontoDescUSD,
	    String icUsuario,
	    String cgReciboElectronico,
	    ArrayList listaEpos,
	    String cg_tipo_credito,
	    HashMap doctosPorEpo,
	    HashMap catalogoTipoFondeoPorEPO,
	    ArrayList li_responsable_int,
	    ArrayList li_tipo_cobranza,
	    ArrayList li_tipo_cobro_int
    ) throws NafinException;

    public ProcesoIFDistWSInfo getDoctosSelecPymeDisWS(String claveUsuario, String password, String claveIF);

    public String getCadenaFirmarDistWS(DocumentoDistIFWS[] documentos);

    public ProcesoIFDistWSInfo getAvisosNotificacionDisWS (String claveUsuario, String password, String claveEpo, String clavePyme, String claveIF, String fecOperacionIni, String fecOperacionFin);

    public ProcesoIFDistWSInfo confirmacionDoctosDistIFWS (String claveUsuario, String password, String claveIF, DocumentoDistIFWS[] documentos, String pkcs7, String serial);

}