package com.netro.distribuidores;


import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Combos;
import netropology.utilerias.Comunes;
import netropology.utilerias.VectorTokenizer;

@Stateless(name = "MantenimientoDistEJB" , mappedName = "MantenimientoDistEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class MantenimientoDistBean implements MantenimientoDist {

	private String CADENA_CONEXION = "distribuidoresDS";


   public Combos getComboModalidad()
   throws NafinException{
	Combos comboModalidad = null;
        AccesoDB con  = null;
   	try{
        	con = new AccesoDB();
                con.conexionDB();
        	comboModalidad = new Combos("SELECT IC_TIPO_FINANCIAMIENTO,CD_DESCRIPCION FROM COMCAT_TIPO_FINANCIAMIENTO WHERE IC_TIPO_FINANCIAMIENTO",con);
        }catch(Exception e){
        	throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta())
                	con.cierraConexionDB();
        }

   return comboModalidad;
   }

	public Vector getVecModalidad(String epo,String pyme)	//Sustituye a getComboModalidad
		throws NafinException{
		System.out.println("MantenimientoDist::getVecModalidad (E)");
		Vector vecFilas = new Vector();
		Vector vecColumnas = null;
		String credito = "";
		String valida = "";
		AccesoDB con  = null;
		ResultSet rs = null;
		try{
			con = new AccesoDB();
			con.conexionDB();
			rs = con.queryDB("select cg_tipo_credito "+
							  	"from comrel_pyme_epo_x_producto pep "+
								"where   pep.ic_pyme = "+ pyme +
								"    and pep.ic_epo = "+ epo +
								"    and pep.ic_producto_nafin = 4");
			if(rs.next())
				credito = rs.getString(1);
			rs.close();
			if ("C".equals(credito))
				valida = ",5";
			rs = con.queryDB("SELECT IC_TIPO_FINANCIAMIENTO,CD_DESCRIPCION "+
					"FROM COMCAT_TIPO_FINANCIAMIENTO "+
					"WHERE IC_TIPO_FINANCIAMIENTO IN (1,2,3,4"+
					valida + ")");
			System.out.println("Valida: "+ valida + "; Cr�dito: "+ credito + "; Pyme: " + pyme);
			while(rs.next()){
				vecColumnas = new Vector();
				vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
				vecColumnas.add((rs.getString("CD_DESCRIPCION")==null)?"":rs.getString("CD_DESCRIPCION"));
				vecFilas.add(vecColumnas);
			}
			con.cierraStatement();
		}catch(Exception e){
			System.out.println("MantenimientoDist::getVecModalidad Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
			con.cierraConexionDB();
			System.out.println("MantenimientoDist::getVecModalidad (S)");
		}
		return vecFilas;
	}

	public Vector consultaDoctos(String ic_epo,String ic_pyme, String ic_moneda, String modo_plazo, String ic_estatus_docto, String ig_numero_docto, String cc_acuse, String fecha_emision_de, String fecha_emision_a, String fecha_vto_de, String fecha_vto_a, String fecha_publicacion_de, String fecha_publicacion_a, String fn_monto_de, String fn_monto_a, String monto_con_descuento, String solo_cambio,String in,boolean incluyeCambio, String NOnegociable)
			throws NafinException{
		AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String condicion    = "";
        Vector	vecFilas = new Vector();
        Vector	vecColumnas = null;
   		try {
			con = new AccesoDB();
			con.conexionDB();

			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion += " and D.ic_pyme = "+ic_pyme;
			if(!"".equals(ic_estatus_docto)&&ic_estatus_docto!=null)
				condicion += " and D.ic_estatus_docto = "+ic_estatus_docto;

      if("".equals(ic_estatus_docto)&& NOnegociable.equals("N")){
      	condicion += " and D.ic_estatus_docto in (2, 9)";
      }else  if("".equals(ic_estatus_docto)&& NOnegociable.equals("S")){ //Fodea 029-2010 Distribuidores Fase III
        condicion += " and D.ic_estatus_docto in (2, 9,1)";
      }
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion += " and M.ic_moneda = "+ic_moneda;
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion += " and TF.ic_tipo_financiamiento = "+modo_plazo;
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion += " and D.ig_numero_docto = '"+ig_numero_docto+"'";
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion += " and D.cc_acuse = '"+cc_acuse+"'";
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion += " and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion += " and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion += " and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))";
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion += " and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a;
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion += " and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a;
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion += " and D.ic_documento in (select ic_documento from dis_cambio_estatus)";
			if(!"".equals(in)&&in!=null)
				condicion += " and D.ic_documento in("+in+")";



                qrySentencia =	" SELECT P.cg_razon_social as NOMBRE_PYME "+
					" ,D.ig_numero_docto"+
					" ,D.ic_documento"+
					" ,D.cc_acuse"+
					" ,to_char(D.df_fecha_emision,'dd/mm/yyyy') as DF_FECHA_EMISION"+
					" ,to_char(D.df_carga,'dd/mm/yyyy') as DF_CARGA"+
					" ,to_char(D.df_fecha_venc,'dd/mm/yyyy') as DF_FECHA_VENC"+
					" ,D.ig_plazo_docto"+
					" ,M.ic_moneda"+
					" ,M.cd_nombre AS NOMBRE_MONEDA"+
					" ,D.fn_monto"+
					" ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					" ,D.ig_plazo_descuento"+
					" ,D.fn_porc_descuento"+
					" ,D.fn_monto * (fn_porc_descuento/100) as MONTO_DESCUENTO"+
					" ,TF.ic_tipo_financiamiento"+
					" ,TF.cd_descripcion as TIPO_FINANCIAMIENTO"+
					" ,ED.ic_estatus_docto "+
					" ,ED.cd_descripcion as ESTATUS"+
					", d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" ,to_char(sysdate,'dd/mm/yyyy') as FECHA_MODIFICACION,clas.cg_descripcion as CATEGORIA";
                if(incluyeCambio)
                	qrySentencia += " ,to_char(df_fecha_emision_anterior,'dd/mm/yyyy') as df_fecha_emision_anterior"+
						" ,to_char(df_fecha_emision_nueva,'dd/mm/yyyy') as df_fecha_emision_nueva"+
						" ,to_char(df_fecha_venc_anterior,'dd/mm/yyyy') as df_fecha_venc_anterior"+
						" ,to_char(df_fecha_venc_nueva,'dd/mm/yyyy') as df_fecha_venc_nueva"+
						" ,fn_monto_anterior"+
						" ,fn_monto_nuevo"+
						" ,TF2.cd_descripcion as modo_plazo_anterior"+
						" ,TF.cd_descripcion as modo_plazo_nuevo";
		else
                	qrySentencia += " ,'' as df_fecha_emision_anterior"+
						" ,'' as df_fecha_emision_nueva"+
						" ,'' as df_fecha_venc_anterior"+
						" ,'' as df_fecha_venc_nueva"+
						" ,'' as fn_monto_anterior"+
						" ,'' as fn_monto_nuevo"+
						" ,'' as modo_plazo_anterior"+
						" ,'' as modo_plazo_nuevo";

                qrySentencia += " FROM dis_documento D"+
						" ,comcat_pyme P"+
						" ,comcat_moneda M"+
						" ,comrel_producto_epo PE"+
						" ,comcat_producto_nafin PN"+
						" ,com_tipo_cambio TC"+
						" ,comcat_tipo_financiamiento TF,comrel_clasificacion clas "+
						" ,comcat_estatus_docto ED";
        	if(incluyeCambio)
                	qrySentencia += ",dis_cambio_estatus CE,comcat_tipo_financiamiento TF2";
                qrySentencia += " WHERE D.ic_pyme = P.ic_pyme"+
					" AND D.ic_moneda = M.ic_moneda"+
					" AND PE.ic_epo = D.ic_epo"+
					" AND PN.ic_producto_nafin = PE.ic_producto_nafin"+
					" AND PN.ic_producto_nafin = 4"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					" AND M.ic_moneda = TC.ic_moneda"   +
					" AND TF.ic_tipo_financiamiento = D.ic_tipo_financiamiento"+
					" AND D.ic_estatus_docto = ED.ic_estatus_docto"+
//					" AND D.ic_estatus_docto = 2"+
					" AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
					" AND D.IC_EPO = "+ic_epo;
                if(incluyeCambio)
                	qrySentencia += " AND D.IC_DOCUMENTO = CE.IC_DOCUMENTO"+
						" AND CE.DC_FECHA_CAMBIO IN(SELECT MAX(DC_FECHA_CAMBIO) FROM DIS_CAMBIO_ESTATUS WHERE IC_DOCUMENTO = D.IC_DOCUMENTO)"+
						" AND CE.IC_TIPO_FINAN_ANT = TF2.IC_TIPO_FINANCIAMIENTO(+)";
                qrySentencia += condicion;
				System.out.println("MantenimientoDistBean.consultaDoctos(Query)" + qrySentencia);
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
                	vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO"));
/*1*/			vecColumnas.add((rs.getString("NOMBRE_PYME")==null)?"":rs.getString("NOMBRE_PYME"));
/*2*/			vecColumnas.add((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));
/*3*/			vecColumnas.add((rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE"));
/*4*/			vecColumnas.add((rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION"));
/*5*/			vecColumnas.add((rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA"));
/*6*/			vecColumnas.add((rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC"));
/*7*/			vecColumnas.add((rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO"));
/*8*/			vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*9*/			vecColumnas.add((rs.getString("NOMBRE_MONEDA")==null)?"":rs.getString("NOMBRE_MONEDA"));
/*10*/			vecColumnas.add((rs.getString("FN_MONTO")==null)?"0":rs.getString("FN_MONTO"));
/*11*/			vecColumnas.add((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));
/*12*/			vecColumnas.add((rs.getString("TIPO_CAMBIO")==null)?"1":rs.getString("TIPO_CAMBIO"));
/*13*/			vecColumnas.add((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));
/*14*/			vecColumnas.add((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));
/*15*/			vecColumnas.add((rs.getString("MONTO_DESCUENTO")==null)?"0":rs.getString("MONTO_DESCUENTO"));
/*16*/			vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
/*17*/			vecColumnas.add((rs.getString("TIPO_FINANCIAMIENTO")==null)?"":rs.getString("TIPO_FINANCIAMIENTO"));
/*18*/			vecColumnas.add((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"));
/*19*/			vecColumnas.add((rs.getString("FECHA_MODIFICACION")==null)?"":rs.getString("FECHA_MODIFICACION"));
/*20*/			vecColumnas.add((rs.getString("DF_FECHA_EMISION_ANTERIOR")==null)?"":rs.getString("DF_FECHA_EMISION_ANTERIOR"));
/*21*/			vecColumnas.add((rs.getString("DF_FECHA_EMISION_NUEVA")==null)?"":rs.getString("DF_FECHA_EMISION_NUEVA"));
/*22*/			vecColumnas.add((rs.getString("DF_FECHA_VENC_ANTERIOR")==null)?"":rs.getString("DF_FECHA_VENC_ANTERIOR"));
/*23*/			vecColumnas.add((rs.getString("DF_FECHA_VENC_NUEVA")==null)?"":rs.getString("DF_FECHA_VENC_NUEVA"));
/*24*/			vecColumnas.add((rs.getString("FN_MONTO_ANTERIOR")==null)?"":rs.getString("FN_MONTO_ANTERIOR"));
/*25*/			vecColumnas.add((rs.getString("FN_MONTO_NUEVO")==null)?"":rs.getString("FN_MONTO_NUEVO"));
/*26*/			vecColumnas.add((rs.getString("MODO_PLAZO_ANTERIOR")==null)?"":rs.getString("MODO_PLAZO_ANTERIOR"));
/*27*/			vecColumnas.add((rs.getString("MODO_PLAZO_NUEVO")==null)?"":rs.getString("MODO_PLAZO_NUEVO"));
/*28*/			vecColumnas.add((rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA"));
/*29*/			vecColumnas.add((rs.getString("ic_estatus_docto")==null)?"":rs.getString("ic_estatus_docto"));
/*30*/			vecColumnas.add((rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA"));
					vecFilas.add(vecColumnas);
           }


        }catch(Exception e){
		System.out.println("MantenimientoDistBean::consultaDoctos Excepcion "+e);
        	throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
        }
   return vecFilas;
   }


   public void guardaCambios(String ic_documento[],String nueva_fecha_docto[],
	 String nueva_fecha_vto[],String nuevo_monto[], String nuevo_modo_plazo[],
	 String fecha_lim_desc[], String cg_fecha_porc_desc, String ic_estatus_docto[],
	 String anterior_monto[],String ses_ic_epo, String tipoCredito,
	 String ventaCartera,	String descuentoAutomatico 	)
   throws NafinException{

   	AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String set = "";
        String values = "";
        int i = 0;
        int dif = 0;
        int min=0,max=0;
        boolean ok = true;
		String	plazoDscto = "";
		String 	porcDscto = "";
		String tipoFinan = "";
		String      qrySentencia2 = "";
		PreparedStatement ps = null;
		double monto_anterior =0;
		double monto_nuevo =0;
		double monto_total =0;
		double monto_total2 =0;

   	try{
        	con = new AccesoDB();
            con.conexionDB();
			validaFechas(nueva_fecha_vto,nueva_fecha_docto,ic_documento,con);
  			System.out.println("MantenimientoDist::guardaCambios(E)");
            for(i=0;i<ic_documento.length;i++){
					set	= "";
                    values  = "";
						if(!"".equals(nueva_fecha_docto[i])){
							set += " SET df_fecha_emision = to_date('"+nueva_fecha_docto[i]+"','dd/mm/yyyy')";
							values += "df_fecha_emision,to_date('"+nueva_fecha_docto[i]+"','dd/mm/yyyy')";
						}else {
							values += "NULL,NULL";
						}
						if(!"".equals(nueva_fecha_vto[i])){
							set += ("".equals(set)? " SET":", ")+ " df_fecha_venc = to_date('"+nueva_fecha_vto[i]+"','dd/mm/yyyy')";
							values += ",df_fecha_venc,to_date('"+nueva_fecha_vto[i]+"','dd/mm/yyyy')";
						}else{
							values += ",NULL,NULL";
						}
						if(!"".equals(nuevo_monto[i])){
							set += ("".equals(set)? " SET":", ")+ " fn_monto = "+nuevo_monto[i];
							values += ",fn_monto,"+nuevo_monto[i];
						}else{
							values += ",NULL,NULL";
						}
						if(!"".equals(nuevo_modo_plazo[i])){
							if(!"3".equals(nuevo_modo_plazo[i])){
								if("2".equals(nuevo_modo_plazo[i])){
									qrySentencia =
									" select nvl(d.ig_plazo_descuento,pe.ig_plazo_dscto) as plazo_descuento"   +
										" ,d.ic_tipo_financiamiento as tipofinan"   +
										" from dis_documento d"   +
										" ,comrel_producto_epo pe"   +
										" where pe.ic_epo = d.ic_epo "   +
										" and pe.ic_producto_nafin = d.ic_producto_nafin"   +
										" and d.ic_documento = "+ic_documento[i];

									rs = con.queryDB(qrySentencia);
									if(rs.next()){
										plazoDscto	= rs.getString("PLAZO_DESCUENTO");
										tipoFinan	= rs.getString("tipofinan");

										if(tipoFinan.equals("3")){
											set += ("".equals(set)? " SET":", ")+ " ig_plazo_descuento = null"+
											" ,fn_porc_descuento = null"+
											" ,cs_plazo_descuento = 'N' ";
										}

									}
									rs.close();
									con.cierraStatement();
								}else{
									qrySentencia =
										" select nvl(d.ig_plazo_descuento,pe.ig_plazo_dscto) as plazo_descuento"   +
										" ,nvl(d.fn_porc_descuento,pe.fn_porcentaje_dscto) as porc_descuento"   +
										" from dis_documento d"   +
										" ,comrel_producto_epo pe"   +
										" where pe.ic_epo = d.ic_epo "   +
										" and pe.ic_producto_nafin = d.ic_producto_nafin"   +
										" and d.ic_documento = "+ic_documento[i];

									rs = con.queryDB(qrySentencia);
									if(rs.next()){
										plazoDscto	= rs.getString("PLAZO_DESCUENTO");
										porcDscto	= rs.getString("PORC_DESCUENTO");

										set += ("".equals(set)? " SET":", ")+ " ig_plazo_descuento ="+plazoDscto+
											" ,fn_porc_descuento = "+porcDscto+
											" ,cs_plazo_descuento = 'N' ";
									}
									con.cierraStatement();
								}
							}
							set += ("".equals(set)? " SET":", ")+ " ic_tipo_financiamiento = "+nuevo_modo_plazo[i];
							values += ",ic_tipo_financiamiento,"+nuevo_modo_plazo[i];
						} else{
							values+=",NULL,NULL";
						}
						if("9".equals(ic_estatus_docto[i])  && !"".equals(nueva_fecha_vto[i])) {
							set += ("".equals(set)? " SET":", ")+ " ic_estatus_docto = 1";
						}
                        if(!"".equals(set)){
            				qrySentencia =
								" INSERT INTO DIS_CAMBIO_ESTATUS"+
								" (dc_fecha_cambio,ic_documento,ic_cambio_estatus,df_fecha_emision_anterior,df_fecha_emision_nueva,df_fecha_venc_anterior,df_fecha_venc_nueva,fn_monto_anterior,fn_monto_nuevo,ic_tipo_finan_ant,ic_tipo_finan_nuevo)"+
								" select sysdate,ic_documento,21,"+values+
								" from dis_documento where ic_documento = "+ic_documento[i];
                  			System.out.println(qrySentencia);
	                        con.ejecutaSQL(qrySentencia);

                         	qrySentencia =
                         		" UPDATE DIS_DOCUMENTO "+set+
								" WHERE IC_DOCUMENTO = "+ic_documento[i];
                  			System.out.println(qrySentencia);
	                        con.ejecutaSQL(qrySentencia);

        					qrySentencia =
								" select D.df_fecha_venc-D.df_fecha_emision as diferencia"+
								" ,nvl(pe.ig_dias_minimo,pn.in_dias_minimo) as dias_minimo"+
								" ,nvl(pe.ig_dias_maximo,pn.in_dias_maximo) as dias_maximo"+
								" from dis_documento D"+
								" ,comrel_producto_epo PE"+
								" ,comcat_producto_nafin PN"+
								" where D.ic_documento = "+ic_documento[i]+
								" and D.ic_epo = PE.ic_epo"+
								" and PE.ic_producto_nafin = PN.ic_producto_nafin "+
								" and PN.ic_producto_nafin = 4";

	                   			System.out.println(qrySentencia);

                                rs = con.queryDB(qrySentencia);
                                if(rs.next()){
									dif = rs.getInt("DIFERENCIA");
									min = rs.getInt("DIAS_MINIMO");
									max = rs.getInt("DIAS_MAXIMO");
                                }
                                con.cierraStatement();
                                if(dif<0)
                                	throw new NafinException("DIST0004");
                                if(dif<min)
                                	throw new NafinException("DIST0005");
                                if(dif>max)
                                	throw new NafinException("DIST0006");
								qrySentencia =
									"update dis_documento "+
									" set ig_plazo_docto = df_fecha_venc-df_fecha_emision"+
									" where ic_documento = "+ic_documento[i];
								System.out.println(qrySentencia);
								con.ejecutaSQL(qrySentencia);
            			}
            			if("9".equals(ic_estatus_docto[i]) && !"".equals(nueva_fecha_vto[i])) {
                         	qrySentencia =
                         		" UPDATE DIS_DOCUMENTO set ic_estatus_docto = 2"+
								" WHERE IC_DOCUMENTO = "+ic_documento[i]+
								" AND IC_ESTATUS_DOCTO = 1";
                  			System.out.println(qrySentencia);
	                        con.ejecutaSQL(qrySentencia);
            			}
					if(!"".equals(fecha_lim_desc[i])){
						String plazo = "";
						String plazo_nuevo = "";

						if("E".equals(cg_fecha_porc_desc))
							plazo_nuevo = ", TRUNC (TO_DATE ('" + fecha_lim_desc[i] + "', 'dd/mm/yyyy')) - TRUNC (DF_FECHA_EMISION)";
						else
							if("P".equals(cg_fecha_porc_desc))
							plazo_nuevo = ", TRUNC (TO_DATE ('" + fecha_lim_desc[i] + "', 'dd/mm/yyyy')) - TRUNC (DF_CARGA)";

						qrySentencia =
							" select ig_plazo_descuento "+ plazo_nuevo +
							" from dis_documento D"+
							" where D.ic_documento = "+ic_documento[i];

                   		System.out.println(qrySentencia);

                        rs = con.queryDB(qrySentencia);

                        if(rs.next()){
                        	plazo = rs.getString(1);
                        	plazo_nuevo = rs.getString(2);
                        }

                    	if(!"".equals(set)){
							qrySentencia =
								" UPDATE DIS_CAMBIO_ESTATUS"+
								" set ig_plazo_descuento_ant= " + plazo + ",ig_plazo_descuento_nuevo = "+ plazo_nuevo +
								" where dc_fecha_cambio = sysdate and ic_documento = "+ic_documento[i];

						} else {
							qrySentencia =
								" INSERT INTO DIS_CAMBIO_ESTATUS"+
								" (dc_fecha_cambio,ic_documento,ic_cambio_estatus,df_fecha_emision_anterior," +
								" df_fecha_emision_nueva,df_fecha_venc_anterior,df_fecha_venc_nueva,fn_monto_anterior,"+
								" fn_monto_nuevo,ic_tipo_finan_ant,ic_tipo_finan_nuevo,ig_plazo_descuento_ant,"+
								" ig_plazo_descuento_nuevo)"+
								" select sysdate,ic_documento,21,"+values+","+plazo+","+plazo_nuevo+
								" from dis_documento where ic_documento = "+ic_documento[i];
	              		}

                    	System.out.println(qrySentencia);
                    	con.ejecutaSQL(qrySentencia);
             			qrySentencia =
							"update dis_documento "+
							" set ig_plazo_descuento = "+ plazo_nuevo +
							" ,cs_plazo_descuento = 'N' "+
							" where ic_documento = "+ic_documento[i];
                    	System.out.println(qrySentencia);
                   		con.ejecutaSQL(qrySentencia);
                    }

							if ( (tipoCredito.equals("D") &&  ventaCartera.equals("S") &&  descuentoAutomatico.equals("S") )
							||  (tipoCredito.equals("C") && descuentoAutomatico.equals("S") ) ) {

								if(!nuevo_monto[i].equals("")) {
									 
									 String saldo="0";
									 
									 if ( !ses_ic_epo.equals("")) {  
										saldo= validaMontoComprometido(ses_ic_epo);
									 }
									 String  montoA = anterior_monto[i];
									 String  montoN = nuevo_monto[i];

									 System.out.println("montoA:: "		+montoA);
									 System.out.println("montoN:: "		+montoN);

									 monto_anterior =Double.parseDouble(montoA);
									 monto_nuevo =Double.parseDouble(montoN);

										System.out.println("monto_anterior:: "		+monto_anterior);
										System.out.println("monto_nuevo:: "		+monto_nuevo);

										if (monto_nuevo > monto_anterior){
												monto_total = monto_nuevo - monto_anterior;
												monto_total2 = Double.parseDouble(saldo) + monto_total;
										}
										if (monto_nuevo < monto_anterior){
												monto_total = monto_anterior - monto_nuevo;
												monto_total2 = Double.parseDouble(saldo) - monto_total;
										}

										System.out.println("monto_total2:: "		+monto_total2);

										qrySentencia2 ="	update DIS_LINEA_CREDITO_DM    "+
																		" set FN_SALDO_COMPROMETIDO = "+monto_total2+
																		"  WHERE  ic_epo =  "+ses_ic_epo+
																		" AND ic_producto_nafin = 4 "+
																		" AND df_vencimiento_adicional > SYSDATE  "+
																		" AND cg_tipo_solicitud = 'I' "+
																		" AND ic_estatus_linea = 12 ";


									System.out.println("qrySentencia2:: "		+qrySentencia2);
											ps = con.queryPrecompilado(qrySentencia2);
											ps.executeUpdate();
											ps.close();
								} //if(!nuevo_monto[i].equals("")) {
							}

          }//for

	}catch(NafinException ne){
		System.out.println("MantenimientoDistBean::guardaCambios SQLExcepcion "+ne+"\n"+qrySentencia);

        	ok = false;
		throw ne;
        }catch(SQLException e){
        	ok = false;
		System.out.println("MantenimientoDistBean::guardaCambios SQLExcepcion "+e+"\n"+qrySentencia);
                throw new NafinException("DIST0011");
        }catch(Exception e){
        	ok = false;
		System.out.println("MantenimientoDistBean::guardaCambios Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{

 		System.out.println("MantenimientoDist::guardaCambios(C)");
       	if(con.hayConexionAbierta()){
                	con.terminaTransaccion(ok);
                 	con.cierraConexionDB();
                }
        }
   }

	public String getDiasAmpFechaVen(String ic_epo)
			throws NafinException{
		System.out.println("MantenimientoDist::getDiasAmpFechaVen (E)");
		AccesoDB con  = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String qrySentencia = "";
		String paramentro = "";
		try{
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT nvl(cpe.ig_dias_amp_fecha_venc,cpn.ig_dias_amp_fecha_venc) "   +
				"   FROM comrel_producto_epo cpe, comcat_producto_nafin cpn"   +
				"  WHERE cpe.ic_producto_nafin = cpn.ic_producto_nafin"   +
				"    AND cpn.ic_producto_nafin = 4"   +
				"    AND ic_epo = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				paramentro	= rs.getString(1)==null?"":rs.getString(1);
			}//while
			if(ps != null) ps.close();
			rs.close();
		}catch(Exception e){
			System.out.println("MantenimientoDist::getDiasAmpFechaVen Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDist::getDiasAmpFechaVen (S)");
		}
		return paramentro;
	}

   private void validaFechas(String df_fecha_venc[],String df_fecha_emision[],String ic_documento[],AccesoDB con)
    throws NafinException
      {
      String    qrySentencia  = "";
      ResultSet rs            = null;
      java.util.Date fechaVenc = null;
      java.util.Date fechaEmision = null;
      //SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
      Vector	diames_inhabil = new Vector();
      String 	dm_inhabil = "";
      int i=0;
      try
        {
	 /****************************************************/

	qrySentencia = "select cg_dia_inhabil from comcat_dia_inhabil where cg_dia_inhabil is not null UNION select cg_dia_inhabil from comrel_dia_inhabil_x_epo where cg_dia_inhabil is not null and ic_epo = (select ic_epo from dis_documento where ic_documento = "+ic_documento[0]+")";
	rs = con.queryDB(qrySentencia);
	while (rs.next())
		diames_inhabil.addElement(rs.getString(1).trim());
	con.cierraStatement();
	for(i=0;i<ic_documento.length;i++){
	        if(!"".equals(df_fecha_venc[i])&&df_fecha_venc[i]!=null){
               		fechaVenc = Comunes.parseDate(df_fecha_venc[i]);
                        if(fechaVenc.getDay()==0||fechaVenc.getDay()==6) //domingo o sabado
					throw new NafinException("DIST0010");
			for(int fv=0; fv<diames_inhabil.size(); fv++) {
				dm_inhabil=diames_inhabil.get(fv).toString();
		        	if(df_fecha_venc[i].substring(0,5).equals(dm_inhabil)) {
					throw new NafinException("DIST0010");
				}
			}
	        }
	        if(!"".equals(df_fecha_emision[i])&&df_fecha_emision[i]!=null){
               		fechaEmision = Comunes.parseDate(df_fecha_emision[i]);
                        if(fechaEmision.getDay()==0||fechaEmision.getDay()==6) //domingo o sabado
					throw new NafinException("DIST0013");
			for(int fv=0; fv<diames_inhabil.size(); fv++) {
				dm_inhabil=diames_inhabil.get(fv).toString();
		        	if(df_fecha_emision[i].substring(0,5).equals(dm_inhabil)) {
					throw new NafinException("DIST0013");
				}
			}
	        }
        }


       }catch(NafinException ne){
            throw ne;
       }catch(Exception e){
          System.out.println("HUBO UNA EXCEPCION EN VALIDA FECHAS "+e);
          throw new NafinException("SIST0001");
      }
    }







	/*********************************************************************************************************************
	*
	*	REGRESA UN VECTOR CON LA CONSULTA DE PAGOS CREDITO
	*		---	LOS ULTIMOS DOS REGISTRO SON LOS TOTALES DE LA CONSULTA (PESOS Y DOLLAR) ---
	*********************************************************************************************************************/

	public Vector consultaPagoCredito(String lsNumEPO,
        	String lsNumCredito,
                String lsFchOperIni,
                String lsFchOperFin,
		double ldMontoCredIni,
                double ldMontoCredFin,
                String lsFchVenciIni,
                String lsFchVenciFin,
                String lsEstatus,
                String lsTipoCobroInte,
                String lsIcIf,
                String lsTipoCredito,
                String lsNumPyme)
	throws NafinException	{

		System.out.println("*****************************************************************************");
		System.out.println("*****  ENTRANDO A consultaPagoCredito DEL BEAN MantenimientoDistBean: \t Si hay algun problema preguntale a Manuel");
		System.out.println("Cachando los valores:");
		System.out.println("lsNumEPO: "       + lsNumEPO);
		System.out.println("lsNumCredito: "   + lsNumCredito);
		System.out.println("lsFchOperIni: "   + lsFchOperIni);
		System.out.println("lsFchOperFin: "   + lsFchOperFin);
		System.out.println("ldMontoCredIni: " + ldMontoCredIni);
		System.out.println("ldMontoCredFin: " + ldMontoCredFin);
		System.out.println("lsFchVenciIni: "  + lsFchVenciIni);
		System.out.println("lsFchVenciFin: "  + lsFchVenciFin);

		AccesoDB  lobjConexion = new AccesoDB();
		Vector    lVecConsulta = new Vector(),
				  lVecTotales  = new Vector();
		ResultSet lCurConsulta;
                String	lsMensajeError		= "";
                String	lsCamposQuery 		= "";
                String	lsTotCampos		= "";
                String	lsCondicionesQuery	= "";
                String	lsQryPagoCredito	= "";
                String	lsQryTotPagCrePeso	= "";
                String	lsQryTotPagCreDollar	= "";
                String	COM_LINEA_CREDITO	= "";
                String	IC_LINEA_CREDITO	= "";

                if("D".equals(lsTipoCredito)){
                	COM_LINEA_CREDITO = "dis_linea_credito_dm";
                        IC_LINEA_CREDITO  = "ic_linea_credito_dm";
                }else if("C".equals(lsTipoCredito)){
                	COM_LINEA_CREDITO = "com_linea_credito";
                        IC_LINEA_CREDITO = "ic_linea_credito";
                }

		lsMensajeError   = "ERROR EN EL BEAN (LineadmBean): \t EN EL METODO consultaPagoCredito: \t ERROR: ";
                lsCamposQuery    = "" +
					"SELECT docto.ic_documento           			AS num_credito,   " +
					"       epo.cg_razon_social          			AS nombre_epo,    " +
					"       pyme.cg_razon_social         			AS nombre_pyme,   " +
					"       moneda.cd_nombre AS tipo_moneda, " +
					"       docto_sel.fn_importe_recibir   			AS monto,         " +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', " +
					"			'Sin Conversion')        			AS tipo_conversion, " +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio, " +
					"       docto.fn_monto * docto.fn_tipo_cambio  	AS monto_valuado, " +
					"       NVL((SELECT cd_nombre FROM comcat_tasa WHERE ic_tasa =  docto_sel.ic_tasa), '') AS ref_tasa, " +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-', " +
					"             docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', " +
					"			  docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/', " +
					"             docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes, " +
					"       docto.ig_plazo_credito                  AS plazo, " +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY')    AS fch_vencimiento, " +
					"       docto_sel.fn_importe_interes            AS monto_interes, " +
					"       (SELECT cd_descripcion FROM comcat_tipo_cobro_interes WHERE ic_tipo_cobro_interes = " +
					"	            lin_cred.ic_tipo_cobro_interes ) AS tipo_cobro_inte, " +
					"       (SELECT cd_descripcion FROM comcat_estatus_docto WHERE ic_estatus_docto = docto.ic_estatus_docto ) " +
					"				AS estatus_soli ";
                lsTotCampos = "" +
					"SELECT SUM(docto.fn_monto)               			AS monto,         " +
					"       SUM(docto.fn_monto * docto.fn_tipo_cambio) 	AS monto_valuado, " +
					"       SUM(docto_sel.fn_importe_interes)           AS monto_interes  ";
		lsCondicionesQuery = "" +
					"FROM comcat_epo             epo, " +
					"     comcat_pyme            pyme, " +
					"     dis_documento          docto, " +
					"     comcat_moneda          moneda, " +
					"     comrel_producto_epo    prod_epo, " +
					"     comcat_producto_nafin  prod_nafin, " +
					"     dis_solicitud          soli, " +
					"     dis_docto_seleccionado docto_sel, " +
					"     "+COM_LINEA_CREDITO+"   lin_cred " +
					"WHERE docto.ic_epo = epo.ic_epo " +
					"      AND docto.ic_pyme = pyme.ic_pyme " +
					"      AND lin_cred.ic_moneda = moneda.ic_moneda " +
					"      AND prod_nafin.ic_producto_nafin = 4 " +
					"      AND prod_epo.ic_producto_nafin   = prod_nafin.ic_producto_nafin " +
					"      AND prod_epo.ic_epo = docto.ic_epo " +
					"      AND docto.ic_documento = soli.ic_documento(+) " +
					"      AND docto.ic_documento = docto_sel.ic_documento " +
					" 	   AND epo.cs_habilitado = 'S' " +
					"      AND docto."+IC_LINEA_CREDITO+" = lin_cred."+IC_LINEA_CREDITO+" "+
                                        "      AND lin_cred.ic_if = "+lsIcIf;
                                if(!"".equals(lsNumEPO))
					lsCondicionesQuery += "      AND epo.ic_epo = " + lsNumEPO + " ";
                                if(!"".equals(lsNumPyme))
                                	lsCondicionesQuery += "	     AND pyme.ic_pyme = "+lsNumPyme;
				lsQryPagoCredito     = lsCamposQuery + lsCondicionesQuery;
				lsQryTotPagCrePeso   = lsTotCampos   + lsCondicionesQuery +
					"      AND docto.ic_moneda = 1";
				lsQryTotPagCreDollar = lsTotCampos   + lsCondicionesQuery +
					"      AND docto.ic_moneda = 54";



		/*******************************
		* Agregando Filtros al query
		*****/

		if (!lsNumCredito.equals("") )	{
			lsQryPagoCredito     += " AND docto.ic_documento = " + lsNumCredito;
			lsQryTotPagCrePeso   += " AND docto.ic_documento = " + lsNumCredito;
			lsQryTotPagCreDollar += " AND docto.ic_documento = " + lsNumCredito;
		}

		if (!lsFchOperIni.equals("") )	{
			lsQryPagoCredito     += " AND soli.df_operacion >= TO_DATE('"+lsFchOperIni+"', 'DD/MM/YYYY') ";
			lsQryTotPagCrePeso   += " AND soli.df_operacion >= TO_DATE('"+lsFchOperIni+"', 'DD/MM/YYYY') ";
			lsQryTotPagCreDollar += " AND soli.df_operacion >= TO_DATE('"+lsFchOperIni+"', 'DD/MM/YYYY') ";
		}
		if (!lsFchOperFin.equals("") )	{
			lsQryPagoCredito     += " AND soli.df_operacion >= TO_DATE('"+lsFchOperFin+"', 'DD/MM/YYYY') ";
			lsQryTotPagCrePeso   += " AND soli.df_operacion >= TO_DATE('"+lsFchOperFin+"', 'DD/MM/YYYY') ";
			lsQryTotPagCreDollar += " AND soli.df_operacion >= TO_DATE('"+lsFchOperFin+"', 'DD/MM/YYYY') ";
		}

		if ( ldMontoCredIni > 0 )	{
			lsQryPagoCredito     += " AND docto.fn_monto >= " + ldMontoCredIni;
			lsQryTotPagCrePeso   += " AND docto.fn_monto >= " + ldMontoCredIni;
			lsQryTotPagCreDollar += " AND docto.fn_monto >= " + ldMontoCredIni;
		}
		if ( ldMontoCredFin > 0 )	{
			lsQryPagoCredito     += " AND docto.fn_monto >= " + ldMontoCredFin;
			lsQryTotPagCrePeso   += " AND docto.fn_monto >= " + ldMontoCredFin;
			lsQryTotPagCreDollar += " AND docto.fn_monto >= " + ldMontoCredFin;
		}

		if (!lsFchVenciIni.equals(""))	{
			lsQryPagoCredito     += " AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ";
			lsQryTotPagCrePeso   += " AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ";
			lsQryTotPagCreDollar += " AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ";
		}
		if (!lsFchVenciFin.equals(""))	{
			lsQryPagoCredito     += " AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ";
			lsQryTotPagCrePeso   += " AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ";
			lsQryTotPagCreDollar += " AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ";
		}

		if ( !lsTipoCobroInte.equals("") )	{
			lsQryPagoCredito     += " AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte;
			lsQryTotPagCrePeso   += " AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte;
			lsQryTotPagCreDollar += " AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte;
		}

		if (!lsEstatus.equals("") )	{
			lsQryPagoCredito     += " AND docto.ic_estatus_docto = " + lsEstatus;
			lsQryTotPagCrePeso   += " AND docto.ic_estatus_docto = " + lsEstatus;
			lsQryTotPagCreDollar += " AND docto.ic_estatus_docto = " + lsEstatus;
		}

		lsQryPagoCredito += " ORDER BY docto.ic_documento";

		try {
			try	{	lobjConexion.conexionDB();	}
			catch (	Exception err )	{	throw new NafinException("SIST0001");	}

			System.out.println( "\n" + lsQryPagoCredito + "\n" );
			System.out.println( "\n" + lsQryTotPagCrePeso + "\n" );
			System.out.println( "\n" + lsQryTotPagCreDollar + "\n" );

			lCurConsulta = lobjConexion.queryDB( lsQryPagoCredito );
			while ( lCurConsulta.next() )	{
				Vector lVecTem = new Vector();
				lVecTem.add( lCurConsulta.getString("num_credito") );
				lVecTem.add( lCurConsulta.getString("nombre_epo") );
				lVecTem.add( lCurConsulta.getString("nombre_pyme") );
				lVecTem.add( lCurConsulta.getString("tipo_moneda") );
				lVecTem.add( lCurConsulta.getString("monto") );
				lVecTem.add( lCurConsulta.getString("tipo_conversion") );
				lVecTem.add( lCurConsulta.getString("tipo_cambio") );
				lVecTem.add( lCurConsulta.getString("monto_valuado") );
				lVecTem.add( lCurConsulta.getString("ref_tasa") );
				lVecTem.add( lCurConsulta.getString("tasa_interes") );
				lVecTem.add( lCurConsulta.getString("plazo") );
				lVecTem.add( lCurConsulta.getString("fch_vencimiento") );
				lVecTem.add( lCurConsulta.getString("monto_interes") );
				lVecTem.add( lCurConsulta.getString("tipo_cobro_inte") );
				lVecTem.add( lCurConsulta.getString("estatus_soli") );
				lVecConsulta.add( lVecTem );
			}

			lCurConsulta = lobjConexion.queryDB( lsQryTotPagCrePeso );
			if ( lCurConsulta.next() )	{
				lVecTotales.add( lCurConsulta.getString("monto") );
				lVecTotales.add( lCurConsulta.getString("monto_valuado") );
				lVecTotales.add( lCurConsulta.getString("monto_interes") );
				lVecConsulta.add( lVecTotales );
			}

			lCurConsulta = lobjConexion.queryDB( lsQryTotPagCreDollar );
			if ( lCurConsulta.next() )	{
				Vector lVecTotalesD = new Vector();
				lVecTotalesD.add( lCurConsulta.getString("monto") );
				lVecTotalesD.add( lCurConsulta.getString("monto_valuado") );
				lVecTotalesD.add( lCurConsulta.getString("monto_interes") );
				lVecConsulta.add( lVecTotalesD );
			}

			lCurConsulta.close();
		}
		catch(Exception err)	{
			System.out.println(lsMensajeError + err.getMessage());
		}
		finally	{
			if ( lobjConexion.hayConexionAbierta() )	lobjConexion.cierraConexionDB();
		}

		return lVecConsulta;
	}





 	/*********************************************************************************************************************
	*
	*	REGRESA UN VECTOR CON LA CONSULTA DE PAGOS CREDITO
	*		---	LOS ULTIMOS DOS REGISTRO SON LOS TOTALES DE LA CONSULTA (PESOS Y DOLLAR) ---
	*********************************************************************************************************************/

	public Vector consultaPagosCreditos(String lsNumEPO,       String lsNumPYME,       String lsNumCredito,   String lsFchOperIni,
										String lsFchOperFin,   double ldMontoCredIni,  double ldMontoCredFin, String lsFchVenciIni,
										String lsFchVenciFin,  String lsTipoCobroInte, String lsEstatus,      String lsFchPagoIni,
										String lsFchPagoFin )
		throws NafinException	{

		System.out.println("*****************************************************************************");
		System.out.println("*****  ENTRANDO A consultaPagosCreditos DEL BEAN MantenimientoDistBean: \t Si hay algun problema preguntale a Manuel");
		System.out.println("Cachando los valores:");
		System.out.println("lsNumEPO: "        + lsNumEPO);
		System.out.println("lsNumPYME: "       + lsNumPYME);
		System.out.println("lsNumCredito: "    + lsNumCredito);
		System.out.println("lsFchOperIni: "    + lsFchOperIni);
		System.out.println("lsFchOperFin: "    + lsFchOperFin);
		System.out.println("ldMontoCredIni: "  + ldMontoCredIni);
		System.out.println("ldMontoCredFin: "  + ldMontoCredFin);
		System.out.println("lsFchVenciIni: "   + lsFchVenciIni);
		System.out.println("lsFchVenciFin: "   + lsFchVenciFin);
		System.out.println("lsTipoCobroInte: " + lsTipoCobroInte);
		System.out.println("lsEstatus: "       + lsEstatus);
		System.out.println("lsFchPagoIni: "    + lsFchPagoIni);
		System.out.println("lsFchPagoFin: "    + lsFchPagoFin);

		AccesoDB  lobjConexion = new AccesoDB();
		Vector    lVecConsulta = new Vector();

		String 	lsMensajeError   = "ERROR EN EL BEAN (MantenimientoDistBean): \t EN EL METODO consultaPagosCreditos: \t ERROR: ",
				lsQryPagoCredito = "" +
				   "SELECT  docto.ic_documento     AS num_credito, " +
				   "		epo.cg_razon_social    AS epo, " +
				   "        pyme.cg_razon_social   AS distribuidor, " +
				   "        'Descuento Mercantil'  AS tipo_credito, " +
				   "        DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') " +
				   "            AS responsable_interes, " +
				   "        DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') " +
				   "                               AS tipo_cobranza, " +
				   "        TO_CHAR(ca3.df_fecha_hora, 'DD/MM/YYYY') " +
				   "                               AS fecha_operacion, " +
				   "        moneda.cd_nombre 	   AS tipo_moneda, " +
				   "        docto_sel.fn_importe_recibir AS monto, " +
				   "        DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  " +
				   "                               AS tipo_conversion, " +
				   "       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio, " +
				   //"        docto.fn_tipo_cambio   AS tipo_cambio, " +
				   "        docto.fn_monto * docto.fn_tipo_cambio  " +
				   "                              AS monto_valuado, " +
				   "        NVL((SELECT cd_nombre FROM comcat_tasa WHERE ic_tasa =  soli.ic_tasaif), 'No Autorizado') " +
				   "                              AS ref_tasa, " +
				   "        DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',  " +
				   "              docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',  " +
				   "              docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes, " +
				   "        docto.ig_plazo_credito AS plazo, " +
				   "        TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY')  " +
				   "                               AS fch_vencimiento, " +
				   "        docto_sel.fn_importe_interes AS monto_interes, " +
				   "        (SELECT cd_descripcion FROM comcat_tipo_cobro_interes WHERE ic_tipo_cobro_interes = lin_cred.ic_tipo_cobro_interes) " +
				   "                                     AS tipo_cobro_inte, " +
				   "        (SELECT cd_descripcion FROM comcat_estatus_docto WHERE ic_estatus_docto = docto.ic_estatus_docto ) AS estatus_soli, " +
				   "		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago, " +
        		   "		camb_estatus.cg_numero_pago  AS num_pago," +
        		   "		moneda.ic_moneda    " +
				   " FROM dis_documento          docto, " +
				   "     dis_solicitud          soli, " +
				   "     dis_docto_seleccionado docto_sel, " +
				   "     comcat_epo             epo, " +
				   "     comcat_pyme            pyme, " +
				   "     comrel_producto_epo    prod_epo, " +
				   "     comcat_producto_nafin  prod_nafin, " +
				   "     comcat_moneda          moneda, " +
				   "     dis_linea_credito_dm   lin_cred, " +
				   "	dis_cambio_estatus     camb_estatus, " +
				   "	com_acuse3				ca3 "+
				   " WHERE docto.ic_epo = epo.ic_epo " +
				   "      AND docto.ic_pyme = pyme.ic_pyme " +
				   "      AND docto.ic_epo  = prod_epo.ic_epo " +
				   "      AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin " +
				   "      AND docto.ic_documento = soli.ic_documento " +
				   "      AND lin_cred.ic_moneda = moneda.ic_moneda " +
				   "      AND docto.ic_documento = docto_sel.ic_documento " +
				   "      AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm " +
				   "	  AND docto.ic_documento = camb_estatus.ic_documento "+
				   "	  AND soli.cc_acuse = ca3.cc_acuse"+
				   "	  AND camb_estatus.dc_fecha_cambio in(select max(dc_fecha_cambio) from dis_cambio_estatus where ic_documento = docto.ic_documento)"+
				   "      AND prod_nafin.ic_producto_nafin = 4 ";

		/*******************************
		* Agregando Filtros al query
		*****/

		if (!lsNumPYME.equals("") )	lsQryPagoCredito += " AND docto.ic_pyme = " + lsNumPYME;

		if (!lsNumEPO.equals("") )	lsQryPagoCredito += " AND docto.ic_epo  = " + lsNumEPO;

		if (!lsNumCredito.equals("") )	lsQryPagoCredito += " AND docto.ic_documento = " + lsNumCredito;

		if (!lsFchOperIni.equals("") )	lsQryPagoCredito += " AND soli.df_operacion >= TO_DATE('"+lsFchOperIni+"', 'DD/MM/YYYY') ";

		if (!lsFchOperFin.equals("") )	lsQryPagoCredito += " AND soli.df_operacion >= TO_DATE('"+lsFchOperFin+"', 'DD/MM/YYYY') ";

		if ( ldMontoCredIni > 0 )		lsQryPagoCredito += " AND docto.fn_monto >= " + ldMontoCredIni;

		if ( ldMontoCredFin > 0 )		lsQryPagoCredito += " AND docto.fn_monto >= " + ldMontoCredFin;

		if (!lsFchVenciIni.equals(""))	lsQryPagoCredito += " AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ";

		if (!lsFchVenciFin.equals(""))	lsQryPagoCredito += " AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ";

		if (!lsTipoCobroInte.equals(""))lsQryPagoCredito += " AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte;

		if (!lsEstatus.equals("") )		lsQryPagoCredito += " AND docto.ic_estatus_docto = " + lsEstatus;

		if (!lsFchPagoIni.equals("") )	lsQryPagoCredito += " AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoIni+"', 'DD/MM/YYYY') ";

		if (!lsFchPagoFin.equals("") )	lsQryPagoCredito += " AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoFin+"', 'DD/MM/YYYY') ";

		lsQryPagoCredito += " ORDER BY docto.ic_documento";

		try {
			try	{	lobjConexion.conexionDB();	}
			catch (	Exception err )	{	throw new NafinException("SIST0001");	}

			System.out.println( "\n" + lsQryPagoCredito + "\n" );

			ResultSet lCurConsulta = lobjConexion.queryDB( lsQryPagoCredito );
			while ( lCurConsulta.next() )	{
				Vector lVecTem = new Vector();
				lVecTem.add( lCurConsulta.getString("num_credito") );
				lVecTem.add( lCurConsulta.getString("epo") );
				lVecTem.add( lCurConsulta.getString("distribuidor") );
				lVecTem.add( lCurConsulta.getString("tipo_credito") );
				lVecTem.add( lCurConsulta.getString("responsable_interes") );
				lVecTem.add( lCurConsulta.getString("tipo_cobranza") );
				lVecTem.add( lCurConsulta.getString("fecha_operacion") );
				lVecTem.add( lCurConsulta.getString("tipo_moneda") );
				lVecTem.add( lCurConsulta.getString("monto") );
				lVecTem.add( lCurConsulta.getString("tipo_conversion") );
				lVecTem.add( lCurConsulta.getString("tipo_cambio") );
				lVecTem.add( lCurConsulta.getString("monto_valuado") );
				lVecTem.add( lCurConsulta.getString("ref_tasa") );
				lVecTem.add( lCurConsulta.getString("tasa_interes") );
				lVecTem.add( lCurConsulta.getString("plazo") );
				lVecTem.add( lCurConsulta.getString("fch_vencimiento") );
				lVecTem.add( lCurConsulta.getString("monto_interes") );
				lVecTem.add( lCurConsulta.getString("tipo_cobro_inte") );
				lVecTem.add( lCurConsulta.getString("estatus_soli") );
				lVecTem.add( lCurConsulta.getString("fecha_pago") );
				lVecTem.add( lCurConsulta.getString("num_pago") );
				lVecTem.add( lCurConsulta.getString("ic_moneda") );
				lVecConsulta.add( lVecTem );
			}

			lCurConsulta.close();
		}
		catch(Exception err)	{
			System.out.println(lsMensajeError + err.getMessage());
		}
		finally	{
			if ( lobjConexion.hayConexionAbierta() )	lobjConexion.cierraConexionDB();
		}

		return lVecConsulta;
	}





 	/*********************************************************************************************************************
	*
	*	REGRESA UN VECTOR CON LA CONSULTA DE PAGOS CREDITO POR IF PARA CONSULTA DE NAFIN
	*
	*********************************************************************************************************************/

	public Vector consultaPagosCreditos(String lsNumIF,        String lsNumCredito,  String lsMoneda,      double ldMontoCredIni,
										double ldMontoCredFin, String lsFchVenciIni, String lsFchVenciFin, String lsTipoCobroInte,
										String lsFchPagoIni,   String lsFchPagoFin, String lsEstatus)
		throws NafinException	{

		System.out.println("*****************************************************************************");
		System.out.println("*****  ENTRANDO A consultaPagosCreditos DEL BEAN MantenimientoDistBean: \t Si hay algun problema preguntale a Manuel");
		System.out.println("Cachando los valores:");
		System.out.println("lsNumCredito: "    + lsNumCredito);
		System.out.println("ldMontoCredIni: "  + ldMontoCredIni);
		System.out.println("ldMontoCredFin: "  + ldMontoCredFin);
		System.out.println("lsFchVenciIni: "   + lsFchVenciIni);
		System.out.println("lsFchVenciFin: "   + lsFchVenciFin);
		System.out.println("lsTipoCobroInte: " + lsTipoCobroInte);
		System.out.println("lsFchPagoIni: "    + lsFchPagoIni);
		System.out.println("lsFchPagoFin: "    + lsFchPagoFin);

		AccesoDB  lobjConexion = new AccesoDB();
		Vector    lVecConsulta = new Vector();
		ResultSet lCurConsulta;

		String 	lsMensajeError   = "ERROR EN EL BEAN (MantenimientoDistBean): \t EN EL METODO consultaPagosCreditos: \t ERROR: ",
				lsQryPagoCredito = "" +
				   " SELECT  docto.ic_documento     AS num_credito, " +
				   "		epo.cg_razon_social    AS epo, " +
				   "        pyme.cg_razon_social   AS distribuidor, " +
				   "		(SELECT cg_razon_social FROM comcat_if WHERE ic_if = " + lsNumIF + ") " +
				   "							   AS Intermediario, " +
				   "        'Descuento Mercantil'  AS tipo_credito, " +
				   "        DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') " +
				   "            AS responsable_interes, " +
				   "        DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') " +
				   "                               AS tipo_cobranza, " +
				   "        TO_CHAR(soli.df_operacion, 'DD/MM/YYYY') " +
				   "                               AS fecha_operacion, " +
				   "        moneda.cd_nombre	   AS tipo_moneda, " +
				   "        docto_sel.fn_importe_recibir         AS monto, " +
				   "        DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  " +
				   "                               AS tipo_conversion, " +
				   "        docto.fn_tipo_cambio   AS tipo_cambio, " +
				   "        docto.fn_monto * docto.fn_tipo_cambio  " +
				   "                              AS monto_valuado, " +
				   "        NVL((SELECT cd_nombre FROM comcat_tasa WHERE ic_tasa =  soli.ic_tasaif), 'No Autorizado') " +
				   "                              AS ref_tasa, " +
				   "        DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',  " +
				   "              docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',  " +
				   "              docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes, " +
				   "        docto.ig_plazo_credito AS plazo, " +
				   "        TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY')  " +
				   "                               AS fch_vencimiento, " +
				   "        docto_sel.fn_importe_interes AS monto_interes, " +
				   "        (SELECT cd_descripcion FROM comcat_tipo_cobro_interes WHERE ic_tipo_cobro_interes = lin_cred.ic_tipo_cobro_interes) " +
				   "                                     AS tipo_cobro_inte, " +
				   "        (SELECT cd_descripcion FROM comcat_estatus_docto WHERE ic_estatus_docto = docto.ic_estatus_docto ) AS estatus_soli, " +
        		   "		camb_estatus.cg_numero_pago  AS num_pago, " +
				   "		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago " +
				   "		,moneda.ic_moneda"+
				   " FROM dis_documento          docto, " +
				   "     dis_solicitud          soli, " +
				   "     dis_docto_seleccionado docto_sel, " +
				   "     comcat_epo             epo, " +
				   "     comcat_pyme            pyme, " +
				   "     comrel_producto_epo    prod_epo, " +
				   "     comcat_producto_nafin  prod_nafin, " +
				   "     comcat_moneda          moneda, " +
				   "     dis_linea_credito_dm   lin_cred, " +
				   "	dis_cambio_estatus     camb_estatus " +
				   " WHERE docto.ic_epo = epo.ic_epo " +
				   "      AND docto.ic_pyme = pyme.ic_pyme " +
				   "      AND docto.ic_epo  = prod_epo.ic_epo " +
				   "      AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin " +
				   "      AND docto.ic_documento = soli.ic_documento " +
				   "      AND lin_cred.ic_moneda = moneda.ic_moneda " +
				   "      AND docto.ic_documento = docto_sel.ic_documento " +
				   "      AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm " +
				   "	  AND docto.ic_documento = camb_estatus.ic_documento " +
				   "	  AND camb_estatus.dc_fecha_cambio in(select max(dc_fecha_cambio) from dis_cambio_estatus where ic_documento = docto.ic_documento)"+
				   "      AND prod_nafin.ic_producto_nafin = 4 " +
				   "      AND lin_cred.ic_if = " + lsNumIF;


		/*******************************
		* Agregando Filtros al query
		*****/

		if (!lsNumCredito.equals("") )	lsQryPagoCredito += " AND docto.ic_documento = " + lsNumCredito;

		if (!lsMoneda.equals("") )		lsQryPagoCredito += " AND docto.ic_moneda = " + lsMoneda;

		if ( ldMontoCredIni > 0 )		lsQryPagoCredito += " AND docto.fn_monto >= " + ldMontoCredIni;

		if ( ldMontoCredFin > 0 )		lsQryPagoCredito += " AND docto.fn_monto >= " + ldMontoCredFin;

		if (!lsFchVenciIni.equals(""))	lsQryPagoCredito += " AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ";

		if (!lsFchVenciFin.equals(""))	lsQryPagoCredito += " AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ";

		if (!lsTipoCobroInte.equals(""))lsQryPagoCredito += " AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte;

		if (!lsFchPagoIni.equals("") )	lsQryPagoCredito += " AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoIni+"', 'DD/MM/YYYY') ";

		if (!lsFchPagoFin.equals("") )	lsQryPagoCredito += " AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoFin+"', 'DD/MM/YYYY') ";

		if (!lsEstatus.equals("") )		lsQryPagoCredito += " AND soli.ic_estatus_solic =  "+lsEstatus;
		else 							lsQryPagoCredito += " AND soli.ic_estatus_solic in(5,11)";

		lsQryPagoCredito += " ORDER BY docto.ic_documento, docto.ic_epo, docto.ic_pyme";

		try {
			try	{	lobjConexion.conexionDB();	}
			catch (	Exception err )	{	throw new NafinException("SIST0001");	}

			System.out.println( "\n" + lsQryPagoCredito + "\n" );

			lCurConsulta = lobjConexion.queryDB( lsQryPagoCredito );
			while ( lCurConsulta.next() )	{
				Vector lVecTem = new Vector();
/*0*/				lVecTem.add( lCurConsulta.getString("num_credito") );
/*1*/				lVecTem.add( lCurConsulta.getString("epo") );
/*2*/				lVecTem.add( lCurConsulta.getString("distribuidor") );
/*3*/				lVecTem.add( lCurConsulta.getString("Intermediario") );
/*4*/				lVecTem.add( lCurConsulta.getString("tipo_credito") );
/*5*/				lVecTem.add( lCurConsulta.getString("responsable_interes") );
/*6*/				lVecTem.add( lCurConsulta.getString("tipo_cobranza") );
/*7*/				lVecTem.add( lCurConsulta.getString("fecha_operacion") );
/*8*/				lVecTem.add( lCurConsulta.getString("tipo_moneda") );
/*9*/				lVecTem.add( lCurConsulta.getString("monto") );
/*10*/				lVecTem.add( lCurConsulta.getString("tipo_conversion") );
/*11*/				lVecTem.add( lCurConsulta.getString("tipo_cambio") );
/*12*/				lVecTem.add( lCurConsulta.getString("monto_valuado") );
/*13*/				lVecTem.add( lCurConsulta.getString("ref_tasa") );
/*14*/				lVecTem.add( lCurConsulta.getString("tasa_interes") );
/*15*/				lVecTem.add( lCurConsulta.getString("plazo") );
/*16*/				lVecTem.add( lCurConsulta.getString("fch_vencimiento") );
/*17*/				lVecTem.add( lCurConsulta.getString("monto_interes") );
/*18*/				lVecTem.add( lCurConsulta.getString("tipo_cobro_inte") );
/*19*/				lVecTem.add( lCurConsulta.getString("estatus_soli") );
/*20*/				lVecTem.add( lCurConsulta.getString("num_pago") );
/*21*/				lVecTem.add( lCurConsulta.getString("fecha_pago") );
/*22*/				lVecTem.add( lCurConsulta.getString("ic_moneda") );
				lVecConsulta.add( lVecTem );
			}

			lCurConsulta.close();
		}
		catch(Exception err)	{
			System.out.println(lsMensajeError + err.getMessage());
		}
		finally	{
			if ( lobjConexion.hayConexionAbierta() )	lobjConexion.cierraConexionDB();
		}

		return lVecConsulta;
	}





	/*********************************************************************************************************************
	*
	*	GUARDA LOS PAGOS RECHAZADOS DE PAGOS CREDITO
	*
	*********************************************************************************************************************/

	public void guardaPagosRechazados( Vector lVecPagosRecha )
		throws NafinException	{

		System.out.println("*****************************************************************************");
		System.out.println("*****  ENTRANDO A guardaPagosRechazados DEL BEAN MantenimientoDistBean: \t Si hay algun problema preguntale a Manuel");
		System.out.println("Cachando los valores:");
		System.out.println("Vector de entrada: "       + lVecPagosRecha);

		// El vector trae registros de tres elementos: [numero de credito] [Estatus X Asignar] [Numero Pago Referenciado]

		AccesoDB  lobjConexion = new AccesoDB();
		boolean   lbTodoOK     = true;

		String 	lsMensajeError = "ERROR EN EL BEAN (MantenimientoDistBean): \t EN EL METODO guardaPagosRechazados: \t ERROR: ",
				lsInsertPagos  = "",
				lsUpdateDocto  = "",
				lsUpdateSolic  = "";

		try	{
			try	{	lobjConexion.conexionDB();	}
			catch(Exception err)	{	throw new NafinException("SIST0001");	}
			String lsEstatusSolic = "";
			for (int i=0; i<lVecPagosRecha.size(); i++)	{
				Vector lVecTemporal = new Vector();
				lVecTemporal = (Vector) lVecPagosRecha.elementAt(i);
				String 	lsNumCredito    = (String) lVecTemporal.elementAt(0),
						lsEstatusXAsig  = (String) lVecTemporal.elementAt(1),
						lsNumPagoRef    = (String) lVecTemporal.elementAt(2),
						lsCambioEstatus = "";

				if ( lsEstatusXAsig.equals("11")){
                                	lsCambioEstatus = "19";
                                        lsEstatusSolic 	= "5";
                                }else{
                                	lsCambioEstatus = "18";
                                        lsEstatusSolic = "11";
                                }

				lsInsertPagos = "INSERT INTO dis_cambio_estatus (dc_fecha_cambio, ic_documento, ic_cambio_estatus, cg_numero_pago) " +
								"VALUES (SYSDATE, "+lsNumCredito+", "+lsCambioEstatus+", '"+lsNumPagoRef+"')";

				lsUpdateDocto = "UPDATE dis_documento SET ic_estatus_docto = "+lsEstatusXAsig+" WHERE ic_documento = " +lsNumCredito;

				lsUpdateSolic = "UPDATE dis_solicitud SET ic_estatus_solic = "+lsEstatusXAsig+" WHERE ic_documento = " +lsNumCredito+
								"	AND ic_estatus_solic in (11, 22)";


				/*System.out.println("Querys a ejecutar:\n");
				System.out.println(lsInsertPagos + "\n");
				System.out.println(lsUpdateDocto + "\n");
				System.out.println(lsUpdateSolic + "\n");*/

				lobjConexion.ejecutaSQL( lsInsertPagos );
				lobjConexion.ejecutaSQL( lsUpdateDocto );
				lobjConexion.ejecutaSQL( lsUpdateSolic );
			}
		}
		catch (Exception err)	{
			System.out.println(lsMensajeError + err.getMessage() + "\n");
			lbTodoOK = false;
			throw new NafinException("SIST0001");
		}
		finally{
        	if( lobjConexion.hayConexionAbierta() )	{
				lobjConexion.terminaTransaccion(lbTodoOK);
				lobjConexion.cierraConexionDB();
            }
        }

	}


	public List getDatosSirac(String ig_numero_prestamo) throws NafinException	{
		System.out.println("MantenimientoDistEJB::getDatosSirac(E)");
		AccesoDB 			con				= null;
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		String				qrySentencia	= "";
		List				renglones		= new ArrayList();
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT   /*+ index(spagos PR_PK_SAL_PLA_PAG)*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'C', "   +
				"         DECODE (tip.estatus_saldo, 1, spagos.valor, /*Capital Vigente*/"   +
				"                                    2, spagos.valor, 0), 0), 0)), 0) AS capital_total, /*Capital Vencido*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'C', "   +
				"         DECODE (tip.estatus_saldo, 1, spagos.valor_pagado, /*Capital Vigente*/"   +
				"                                    2, spagos.valor_pagado, 0), 0), 0)), 0) AS capital_pagado, /*Capital Vencido*/"   +
				"        NVL (SUM (NVL ("   +
				"         DECODE (tip.tipo_abono, 'I', DECODE (tip.estatus_saldo, 1, spagos.valor, /*Interes Vigente*/"   +
				"                                                                 2, spagos.valor, 0), 0), 0)), 0) AS int_normal, /*Interes Vencido*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'I', DECODE (tip.estatus_saldo, 1, spagos.valor_pagado, /*Interes Vigente*/"   +
				"                                                                               2, spagos.valor_pagado, 0), 0), 0)), 0) AS int_normal_pagado, /*Interes Vencido*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'I', "   +
				"         DECODE (tip.estatus_saldo, 3, spagos.valor, /*Moratorio*/"   +
				"                                    4, spagos.valor, 0), 0), 0)), 0) AS int_moratorio, /*Sobretasa Moraratoria*/"   +
				"        NVL (SUM (NVL (DECODE (tip.tipo_abono, 'I', "   +
				"         DECODE (tip.estatus_saldo, 3, spagos.valor_pagado, /*Moratorio*/"   +
				"                                    4, spagos.valor_pagado, 0), 0), 0)), 0) AS int_moratorio_pagado, /*Sobretasa Moraratoria*/"   +
				"        TO_CHAR (pre.fecha_apertura, 'dd/mm/yyyy') AS fecha_apertura"   +
				"   FROM pr_saldos_plan_pago spagos, pr_tipos_saldo tip, pr_prestamos pre"   +
				"  WHERE spagos.codigo_tipo_saldo = tip.codigo_tipo_saldo"   +
				"    AND spagos.codigo_empresa = pre.codigo_empresa"   +
				"    AND spagos.codigo_agencia = pre.codigo_agencia"   +
				"    AND spagos.codigo_sub_aplicacion = pre.codigo_sub_aplicacion"   +
				"    AND spagos.numero_prestamo = pre.numero_prestamo"   +
				"    AND pre.numero_prestamo = ?"   +
				"  GROUP BY TO_CHAR (pre.fecha_apertura, 'dd/mm/yyyy')"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setLong(1, Long.parseLong(ig_numero_prestamo));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()){
				String rs_capitalTotal 		= rs.getString("capital_total")==null?"0":rs.getString("capital_total");
				String rs_capitalPagado 	= rs.getString("capital_pagado")==null?"0":rs.getString("capital_pagado");
				String rs_intNormal 		= rs.getString("int_normal")==null?"0":rs.getString("int_normal");
				String rs_intNormalPagado 	= rs.getString("int_normal_pagado")==null?"0":rs.getString("int_normal_pagado");
				String rs_intMor 			= rs.getString("int_moratorio")==null?"0":rs.getString("int_moratorio");
				String rs_intMorPagado 		= rs.getString("int_moratorio_pagado")==null?"0":rs.getString("int_moratorio_pagado");

				renglones.add(rs_capitalTotal);
				renglones.add(rs_capitalPagado);
				renglones.add(rs_intNormal);
				renglones.add(rs_intNormalPagado);
				renglones.add(rs_intMor);
				renglones.add(rs_intMorPagado);
			}
			rs.close();
			ps.close();
		}catch(Exception e){
			System.out.println("MantenimientoDistEJB::getDatosSirac(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDistEJB::getDatosSirac(S)");
		}
		return renglones;
	}//getDatosSirac


 /*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>IHJ*/

 	public void setClientesSusp(String df_suspension)
			throws NafinException {
		System.out.println("MantenimientoDistEJB::setClientesSusp(E)");

		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;

		boolean				bOk				= true;
		int					totalRegistros	= 0;
		try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia =
				" SELECT   ic_pyme, TO_CHAR (MAX (df_suspension), 'dd/mm/yyyy hh24:mi:ss') as fechaSusp"   +
				"     FROM dis_clientes_susp"   +
				"    WHERE df_suspension >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"      AND df_suspension < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
				" GROUP BY ic_pyme"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, df_suspension);
			ps.setString(2, df_suspension);
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				String rs_ic_pyme 		= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				String rs_fecha_susp 	= rs.getString("fechaSusp")==null?"":rs.getString("fechaSusp");
				System.out.println("\n rs_ic_pyme: "+rs_ic_pyme);
				System.out.println("\n rs_fecha_susp: "+rs_fecha_susp);
				totalRegistros += setSuspendidos(rs_ic_pyme, rs_fecha_susp, con);
			}//if(rs.next())
			rs.close(); ps.close();

			qrySentencia =
				" INSERT INTO dis_cc_clientes_susp_p02"   +
				"             (df_fecha_suspension, in_total_clientes_susp, df_alta)"   +
				"      VALUES(TO_DATE (?, 'dd/mm/yyyy'), ?, SYSDATE)"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, df_suspension);
			ps.setInt( 2, totalRegistros);
			ps.executeUpdate();
			ps.close();

		} catch(Exception e) {
			bOk = false;
			System.out.println("MantenimientoDistEJB::setClientesSusp(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDistEJB::setClientesSusp(S)");
		}
	}//setClientesSusp

 	private int setSuspendidos(String ic_pyme, String df_suspension, AccesoDB con)
			throws NafinException {
		System.out.println("MantenimientoDistEJB::setSuspendidos(E)");


		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		int					registros 		= 0;
		try {

			qrySentencia =
				" INSERT INTO dis_clientes_susp_p02"   +
				"             (cg_num_distribuidor, df_fecha_suspension, df_alta)"   +
				"    SELECT cg_num_distribuidor, df_suspension, SYSDATE"   +
				"      FROM dis_clientes_susp cs"   +
				"     WHERE cs_estatus = 'S'"   +
				"       AND ic_pyme = ?"   +
				"       AND TO_CHAR (df_suspension, 'dd/mm/yyyy hh24:mi:ss') = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_pyme));
			ps.setString(2, df_suspension);
			registros = ps.executeUpdate();
			ps.close();

			qrySentencia =
				" UPDATE dis_clientes_susp"   +
				"    SET df_registro = SYSDATE"   +
				"  WHERE cs_estatus = 'S'"   +
				"    AND ic_pyme = ?"   +
				"    AND TO_CHAR (df_suspension, 'dd/mm/yyyy hh24:mi:ss') = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_pyme));
			ps.setString(2, df_suspension);
			ps.executeUpdate();
			ps.close();
		} catch(Exception e) {
			System.out.println("MantenimientoDistEJB::setSuspendidos(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("MantenimientoDistEJB::setSuspendidos(S)");
		}
		return registros;
	}//setSuspendidos


 	public void setClientesReactivos(String df_reactivacion)
			throws NafinException {
		System.out.println("MantenimientoDistEJB::setClientesReactivos(E)");

		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;

		boolean				bOk				= true;
		int					totalRegistros	= 0;
		try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia =
				" SELECT   ic_pyme, TO_CHAR (MAX (df_suspension), 'dd/mm/yyyy hh24:mi:ss') as fechaReac"   +
				"     FROM dis_clientes_susp"   +
				"    WHERE df_suspension >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"      AND df_suspension < (TO_DATE (?, 'dd/mm/yyyy') + 1)"   +
				" GROUP BY ic_pyme"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, df_reactivacion);
			ps.setString(2, df_reactivacion);
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				String rs_ic_pyme 			= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				String rs_df_reactivacion 	= rs.getString("fechaReac")==null?"":rs.getString("fechaReac");
				totalRegistros += setReactivos(rs_ic_pyme, rs_df_reactivacion, con);
			}//if(rs.next())
			rs.close(); ps.close();

			qrySentencia =
				" INSERT INTO dis_cc_clientes_reac_p02"   +
				"             (df_reactivacion, in_total_clientes_reac, df_alta)"   +
				"      VALUES(TO_DATE (?, 'dd/mm/yyyy'), ?, SYSDATE)"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, df_reactivacion);
			ps.setInt( 2, totalRegistros);
			ps.executeUpdate();
			ps.close();

		} catch(Exception e) {
			bOk = false;
			System.out.println("MantenimientoDistEJB::setClientesReactivos(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDistEJB::setClientesReactivos(S)");
		}
	}//setClientesSusp

 	private int setReactivos(String ic_pyme, String df_reactivacion, AccesoDB con)
			throws NafinException {
		System.out.println("MantenimientoDistEJB::setReactivos(E)");


		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		int					registros 		= 0;
		try {

			qrySentencia =
				" INSERT INTO dis_clientes_reac_p02"   +
				"             (cg_num_distribuidor, df_reactivacion, df_alta)"   +
				"    SELECT cg_num_distribuidor, df_suspension, SYSDATE"   +
				"      FROM dis_clientes_susp cs"   +
				"     WHERE cs_estatus = 'R'"   +
				"       AND ic_pyme = ?"   +
				"       AND TO_CHAR (df_suspension, 'dd/mm/yyyy hh24:mi:ss') = ?"  ;
System.out.println("\n qrySentencia: "+qrySentencia);
System.out.println("\n ic_pyme: "+ic_pyme);
System.out.println("\n df_reactivacion: "+df_reactivacion);

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_pyme));
			ps.setString(2, df_reactivacion);
			registros = ps.executeUpdate();
			ps.close();

			qrySentencia =
				" UPDATE dis_clientes_susp"   +
				"    SET df_registro = SYSDATE"   +
				"  WHERE cs_estatus = 'R'"   +
				"    AND ic_pyme = ?"   +
				"    AND TO_CHAR (df_suspension, 'dd/mm/yyyy hh24:mi:ss') = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_pyme));
			ps.setString(2, df_reactivacion);
			ps.executeUpdate();
			ps.close();
		} catch(Exception e) {
			System.out.println("MantenimientoDistEJB::setReactivos(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("MantenimientoDistEJB::setReactivos(S)");
		}
		return registros;
	}//setSuspendidos

	public List getDistribuidores(String ic_if, String cg_num_distribuidor, String cg_rfc, String cg_razon_social)
			throws NafinException {
		System.out.println("MantenimientoDistEJB::getDistribuidores(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		String				condicion	 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;
		List				renglones		= new ArrayList();
		List				columnas		= null;

		cg_rfc = cg_rfc.replace('*', '%');
		cg_razon_social = cg_razon_social.replace('*', '%');

		if(!"".equals(cg_num_distribuidor)) {
			condicion += "      AND crn.ic_nafin_electronico = ?";
		}
		if(!"".equals(cg_rfc)) {
			condicion += "      AND pym.cg_rfc like ?";
		}
		if(!"".equals(cg_razon_social)) {
			condicion += "      AND pym.cg_razon_social like ?";
		}

		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT cpe.cg_num_distribuidor, pym.cg_razon_social, pym.cg_rfc,"   +
				"        crn.ic_nafin_electronico, pym.ic_pyme"   +
				"   FROM comcat_pyme pym,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        comrel_nafin crn,"   +
				"        comrel_pyme_epo cpe,"   +
				"        comrel_if_epo_x_producto iep,"   +
				"        comrel_epo_tipo_financiamiento etf"   +

				"  WHERE pym.ic_pyme = pep.ic_pyme"   +
				"    AND pym.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND pep.ic_epo = cpe.ic_epo"   +
				"    AND pep.ic_pyme = cpe.ic_pyme"   +
				"    AND iep.ic_epo = cpe.ic_epo"   +
				"    AND iep.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_producto_nafin = etf.ic_producto_nafin"   +
				"    AND cpe.ic_epo = etf.ic_epo"   +
				"    AND cpe.cg_num_distribuidor IS NOT NULL"   +
				"    AND etf.ic_tipo_financiamiento = 5"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pep.cg_tipo_credito = 'C'"   +
				"    AND pep.cs_habilitado IN ('S', 'N')"   +
				"    AND cpe.cs_distribuidores IN ('S', 'R')"   +
				"    AND iep.cs_habilitado = 'S'"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND iep.ic_if = ?"   +
				condicion;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_if));
			int cont = 1;
			if(!"".equals(cg_num_distribuidor)) {
				cont++;ps.setInt(cont, Integer.parseInt(cg_num_distribuidor));
			}
			if(!"".equals(cg_rfc)) {
				cont++;ps.setString(cont, cg_rfc);
			}
			if(!"".equals(cg_razon_social)) {
				cont++;ps.setString(cont, cg_razon_social);
			}

			rs = ps.executeQuery();
			while(rs.next()){
				String rs_num_distribuidor 	= rs.getString("cg_num_distribuidor")==null?"":rs.getString("cg_num_distribuidor");
				String rs_pyme 				= rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social");
				String rs_rfc 				= rs.getString("cg_rfc")==null?"":rs.getString("cg_rfc");
				String rs_nafin_electronico	= rs.getString("ic_nafin_electronico")==null?"":rs.getString("ic_nafin_electronico");
				String rs_ic_pyme			= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");

				List lDatos = getEstatusSuspen(rs_ic_pyme);
				String rs_fecha_susp		= lDatos.get(0).toString();
				String rs_historico			= lDatos.get(1).toString();
				String rs_estatus			= lDatos.get(2).toString();

				columnas = new ArrayList();
				columnas.add(rs_num_distribuidor);
				columnas.add(rs_pyme);
				columnas.add(rs_rfc);
				columnas.add(rs_nafin_electronico);
				columnas.add(rs_fecha_susp);
				columnas.add(rs_estatus);
				columnas.add(rs_historico);
				columnas.add(rs_ic_pyme);
				renglones.add(columnas);
			}
			rs.close();ps.close();
		} catch(Exception e) {
			System.out.println("MantenimientoDistEJB::getDistribuidores(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDistEJB::getTrimestresEnRango(S)");
		}
		return renglones;
	}//getDistribuidores

	private List getEstatusSuspen(String ic_pyme)
			throws NafinException {
		System.out.println("MantenimientoDistEJB::setEstatusSuspen(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;

		List				renglones		= new ArrayList();

		String rs_fecha = "", rs_registros = "", rs_estatus = "";

		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT TO_CHAR (MAX (df_suspension), 'dd/mm/yyyy hh24:mi:ss') as fechaSusp, COUNT (1) as total"   +
				"   FROM dis_clientes_susp"   +
				"  WHERE ic_pyme = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_pyme));
			rs = ps.executeQuery();
			if(rs.next()){
				rs_fecha = rs.getString("fechaSusp")==null?"":rs.getString("fechaSusp");
				rs_registros = rs.getString("total")==null?"0":rs.getString("total");
			}//if(rs.next())
			rs.close();	ps.close();
			if(!"".equals(rs_fecha)) {
				qrySentencia =
					" SELECT cs_estatus, to_char(df_suspension, 'dd/mm/yyyy') as fechaSusp"   +
					"   FROM dis_clientes_susp"   +
					"  WHERE df_suspension >= TO_DATE (?, 'dd/mm/yyyy hh24:mi:ss')"  +
					"   AND ic_pyme = ?"  ;
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString( 1, rs_fecha);
				ps.setInt( 2, Integer.parseInt(ic_pyme));
				rs = ps.executeQuery();
				if(rs.next()){
					rs_estatus = rs.getString("cs_estatus")==null?"":rs.getString("cs_estatus");
					rs_fecha = rs.getString("fechaSusp")==null?"":rs.getString("fechaSusp");
				}//if(rs.next())
				rs.close();	ps.close();
			}//if(!"".equals(rs_fecha))
			renglones.add(rs_fecha);
			renglones.add(rs_registros);
			renglones.add(rs_estatus);
		} catch(Exception e) {
			System.out.println("MantenimientoDistEJB::setEstatusSuspen(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDistEJB::setEstatusSuspen(S)");
		}
		return renglones;
	}//getDistribuidores

	public void setEstatusSuspen(String ic_pyme, String cg_num_distribuidor, String cs_estatus)
			throws NafinException {
		System.out.println("MantenimientoDistEJB::setEstatusSuspen(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		boolean				bOk				= true;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" INSERT INTO dis_clientes_susp"   +
				"             (ic_pyme, cg_num_distribuidor, df_suspension, cs_estatus, df_registro)"   +
				"      VALUES(?, ?, SYSDATE, ?, NULL)"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_pyme));
			ps.setString( 2, cg_num_distribuidor);
			ps.setString( 3, cs_estatus);
			ps.executeUpdate();
			ps.close();
		} catch(Exception e) {
			bOk = false;
			System.out.println("MantenimientoDistEJB::setEstatusSuspen(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDistEJB::setEstatusSuspen(S)");
		}
	}//getDistribuidores


	public List getHistorico(String ic_pyme)
			throws NafinException {
		System.out.println("MantenimientoDistEJB::setEstatusSuspen(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;

		List				renglones		= new ArrayList();
		List 				columnas		= null;
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT cs_estatus, decode(cs_estatus,'S','Suspendido','R','Reactivado','') as estatus,TO_CHAR (df_suspension, 'dd/mm/yyyy') AS fechasusp"   +
				"   FROM dis_clientes_susp"   +
				"  WHERE ic_pyme = ?"  +
				"  ORDER BY 3"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_pyme));
			rs = ps.executeQuery();
			while(rs.next()){
				String rs_estatus 		= rs.getString("cs_estatus")==null?"":rs.getString("cs_estatus");
				String rs_auxEstatus 	= rs.getString("estatus")==null?"":rs.getString("estatus");
				String rs_fecha 		= rs.getString("fechasusp")==null?"":rs.getString("fechasusp");
				columnas = new ArrayList();
				columnas.add(rs_auxEstatus);
				if("S".equals(rs_estatus))
					columnas.add(rs_fecha);
				else
					columnas.add("");
				if("R".equals(rs_estatus))
					columnas.add(rs_fecha);
				else
					columnas.add("");
				renglones.add(columnas);
			}//if(rs.next())
			rs.close();	ps.close();
		} catch(Exception e) {
			System.out.println("MantenimientoDistEJB::setEstatusSuspen(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDistEJB::setEstatusSuspen(S)");
		}
		return renglones;
	}//getDistribuidores

	public void operaPlazoAmpliadoPemex(String ic_epo)
			throws NafinException {
		System.out.println("MantenimientoDistEJB::operaPlazoAmpliadoPemex(E)");
		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;

		int existe = 0;

		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM comrel_epo_tipo_financiamiento"   +
				"  WHERE ic_producto_nafin = 4"   +
				"    AND ic_tipo_financiamiento = 5"   +
				"    AND ic_epo = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt( 1, Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			if(rs.next())
				existe = rs.getInt(1);
			rs.close();	ps.close();
			if(existe==0)
				throw new NafinException("DIST0041");
		}catch(NafinException ne){
               	throw ne;
		} catch(Exception e) {
			System.out.println("MantenimientoDistEJB::operaPlazoAmpliadoPemex(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDistEJB::operaPlazoAmpliadoPemex(S)");
		}
	}//getDistribuidores

	public HashMap validarMantenimientoMasivo(String strRegistros, String ic_epo) throws NafinException{
		System.out.println("MantenimientoDistEJB::validarMantenimientoMasivo(E)");
		HashMap hretorno = new HashMap();
		StringTokenizer st = null;
		VectorTokenizer vt = null;
		String slinea = "", slineasbien = "", slineaserror = "";
		int inolinea = 1, cont = 0, contbienmn = 0, contbiendl = 0;
		StringBuffer strberrores = new StringBuffer();
		StringBuffer strbsinerrores = new StringBuffer();
		AccesoDB con = new AccesoDB();
		String qrySentencia = "", icdocumento = "",icdoctos = "", icpyme = "", fechaEmision = "";
		String moneda = "", tipoFinan = "", fechaVenc = "", strPreacuse = "", strErrores = "";
		double monto = 0, sumamonto = 0, montomax = 0, montomin  = 0;
		double sumamontodl = 0, montomaxdl = 0, montomindl  = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean bandok = true;
		try{
			con.conexionDB();
			st = new StringTokenizer(strRegistros, "\n");
			while(st.hasMoreTokens()){
				slinea = st.nextToken();
				System.out.println("slinea::: "+slinea);
				vt = new VectorTokenizer(slinea, "|");
				Vector vlinea = vt.getValuesVector();
				if(vlinea.size() != 8){
					strberrores.append("Error en la linea "+inolinea+", No contiene el n�mero de columnas correcto.\n");
				}else{
					// debe contener un numero de distribuidor valido

					if(vlinea.get(0).toString().equals("")){
						strberrores.append("Error en la linea "+inolinea+", El n�mero de distribuidor es obligatorio.\n");
						bandok = false;
					}else{
						qrySentencia = "select pe.cg_num_distribuidor, pe.ic_pyme " +
												" from comrel_pyme_epo pe, comrel_pyme_epo_x_producto pexp " +
												" where pe.ic_epo = ? " +
												" and pe.ic_epo = pexp.ic_epo " +
												" and pe.cs_distribuidores IN ('S', 'R') " +
												" and pexp.ic_producto_nafin = ? " +
												" and pe.cg_num_distribuidor = ? " +
												" and pe.ic_pyme = pexp.ic_pyme";
						ps = con.queryPrecompilado(qrySentencia);
						ps.setInt( 1, Integer.parseInt(ic_epo));
						ps.setInt( 2, Integer.parseInt("4"));
						ps.setString( 3, vlinea.get(0).toString());
						rs = ps.executeQuery();
						if(!rs.next()){
							strberrores.append("Error en la linea "+inolinea+", El n�mero de la PYME no existe, para la EPO.\n");
							bandok = false;
						}
						rs.close();	ps.close();
					}

					//la fecha debe tener el formato dd/mm/aaaa
					if(!Comunes.checaFecha(vlinea.get(2).toString())){
						strberrores.append("Error en la linea "+inolinea+", La fecha debe tener el siguiente formato: dd/mm/aaaa.\n");
						bandok = false;
					}

					//la posicion 3 contiene la clave de la moneda 1 o 54
					if(!"1".equals(vlinea.get(3).toString()) && !"54".equals(vlinea.get(3).toString())){
						strberrores.append("Error en la linea "+inolinea+", La clave de la moneda no es correcta.\n");
						bandok = false;
					}

					//la posicion 4 debe de contener las iniciales MP
					if(!"MP".equals(vlinea.get(4).toString().toUpperCase())){
						strberrores.append("Error en la linea "+inolinea+", Faltan las iniciales MP.\n");
						bandok = false;
					}

					// la posicion 5 debe contener el numero 3
					if(!"3".equals(vlinea.get(5).toString())){
						strberrores.append("Error en la linea "+inolinea+", La clave de la modalidad anterior no es correcta.\n");
						bandok = false;
					}

					// la posicion 6 debe contener el numero 2
					if(!"2".equals(vlinea.get(6).toString())){
						strberrores.append("Error en la linea "+inolinea+", La clave de la modalidad nueva no es correcta.\n");
						bandok = false;
					}

					// la posicion 7 debe contener la causa
					if("".equals(vlinea.get(7).toString())){
						strberrores.append("Error en la linea "+inolinea+", La causa es obligatoria.\n");
						bandok = false;
					}

					// validar que el documento exista con todos los parametros dados
					if(bandok){
						qrySentencia = "select ic_documento, ic_pyme, to_char(df_fecha_emision,'dd/mm/yyyy') fecha, ic_moneda, " +
												" ic_tipo_financiamiento, fn_monto, to_char(df_fecha_venc,'dd/mm/yyyy') fechavenc " +
												" from dis_documento d " +
												" where d.ic_epo = ? " +
												" and d.ig_numero_docto = ? " +
												" and d.ic_producto_nafin = ?";
						System.out.println("qry::: "+qrySentencia);
						ps = con.queryPrecompilado(qrySentencia);
						ps.setInt(1, Integer.parseInt(ic_epo));
						ps.setString( 2, vlinea.get(1).toString());
						ps.setString( 3, "4");
						rs = ps.executeQuery();
						if(rs.next()){
							icdocumento = rs.getString("ic_documento");
							icpyme = rs.getString("ic_pyme");
							fechaEmision = rs.getString("fecha");
							moneda = rs.getString("ic_moneda");
							tipoFinan = rs.getString("ic_tipo_financiamiento");
							monto = rs.getDouble("fn_monto");
							fechaVenc = rs.getString("fechavenc");

							//validar los datos que vienen en el archivo
							if(!fechaEmision.equals(vlinea.get(2).toString())){
								strberrores.append("Error en la linea "+inolinea+", La fecha es incorrecta.\n");
								bandok = false;
							}
							if(!moneda.equals(vlinea.get(3).toString())){
								strberrores.append("Error en la linea "+inolinea+", La moneda es incorrecta.\n");
								bandok = false;
							}
							if(!tipoFinan.equals("3")){
								strberrores.append("Error en la linea "+inolinea+", La clave de modalidad del documento no es correcta.\n");
								bandok = false;
							}
						}else{
							strberrores.append("Error en la linea "+inolinea+", El Documento no existe.\n");
							bandok = false;
						}
						rs.close();	ps.close();
					}

					if(bandok){
						// query para el nombre de la pyme de acuerdo al documento y numero de distriubidor
						qrySentencia = "select p.cg_razon_social as razonsocial " +
													" from comcat_pyme p " +
													" where p.ic_pyme = ?";
						ps = con.queryPrecompilado(qrySentencia);
						ps.setInt( 1, Integer.parseInt(icpyme));
						rs = ps.executeQuery();
						if(rs.next()){
							String npyme = rs.getString("razonsocial");
							strbsinerrores.append("Linea "+inolinea+": No. Documento "+vlinea.get(1).toString()+" - PyME "+vlinea.get(0).toString()+" - "+npyme+"\n");
							slineasbien += inolinea+",";
							icdoctos += icdocumento+",";
							strPreacuse += vlinea.get(0).toString()+","+vlinea.get(1).toString()+","+monto+","+fechaVenc+","+vlinea.get(7).toString()+","+icdocumento+"|";
							if(moneda.equals("1")){
								sumamonto += monto;
								//monto maximo
								if(montomax < monto)
									montomax = monto;
								//monto minimo
								if(montomin == 0)
									montomin = monto;
								else if(montomin > monto)
									montomin = monto;
								contbienmn++;
							}else{
								sumamontodl += monto;
								//monto maximo
								if(montomaxdl < monto)
									montomaxdl = monto;
								//monto minimo
								if (montomindl == 0)
									montomindl = monto;
								if(montomindl > monto)
									montomindl = monto;
								contbiendl++;
							}
						}
						rs.close();	ps.close();
					}else{
						slineaserror += inolinea+",";
						strErrores += slinea+"\n";
					}

				} // fin else
				bandok = true;
				cont++;
				inolinea++;
			}
			hretorno.put("errores",strberrores.toString());
			hretorno.put("correctos",strbsinerrores.toString());
			hretorno.put("lineasb",slineasbien);
			hretorno.put("linease",slineaserror);
			hretorno.put("totaldoctos",new Integer(cont));
			hretorno.put("sumamonto",new Double(sumamonto));
			hretorno.put("montomax",new Double(montomax));
			hretorno.put("montomin",new Double(montomin));
			hretorno.put("sumamontodl",new Double(sumamontodl));
			hretorno.put("montomaxdl",new Double(montomaxdl));
			hretorno.put("montomindl",new Double(montomindl));
			hretorno.put("totalbienmn",new Integer(contbienmn));
			hretorno.put("totalbiendl",new Integer(contbiendl));
			hretorno.put("icDoctos",icdoctos);
			hretorno.put("strPreacuse",strPreacuse);
			hretorno.put("strErrores",strErrores);

		} catch(Exception e) {
			System.out.println("MantenimientoDistEJB::validarMantenimientoMasivo(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoDistEJB::validarMantenimientoMasivo(S)");
		}
		return hretorno;
	}


	public void guardaCambiosMasivo(String strPreacuse, String totaldoc, String totaldocdl, String mtodoc, String mtodocdl, String acuse, String recibo, String icusuario) throws NafinException{
		System.out.println("MantenimientoDist::guardaCambiosMasivo(E)");
   		AccesoDB con = null;
   		PreparedStatement psSentencia = null;
   		PreparedStatement ps = null;
        String qrySentencia = "Update dis_documento set ic_tipo_financiamiento = ?, fn_porc_descuento = null, ig_plazo_descuento = null where ic_documento = ?";
        String qry = "insert into dis_cambio_estatus"+
								" (dc_fecha_cambio,ic_documento,ic_cambio_estatus,ic_tipo_finan_ant,ic_tipo_finan_nuevo, ct_cambio_motivo)"+
								" values (sysdate, ?,?,?,?,?)";
        String slinea = "";
        StringTokenizer st = null;
        VectorTokenizer vt = null;
		boolean ok = true;
	   	try{
	        	con = new AccesoDB();
	            con.conexionDB();
	            psSentencia = con.queryPrecompilado(qrySentencia);
	            ps = con.queryPrecompilado(qry);
				st = new StringTokenizer(strPreacuse, "|");
				while(st.hasMoreTokens()){
					slinea = st.nextToken();
					vt = new VectorTokenizer(slinea, ",");
					Vector vlinea = vt.getValuesVector();

					psSentencia.setInt(1,Integer.parseInt("2"));
					psSentencia.setInt(2,Integer.parseInt(vlinea.get(5).toString()));
					psSentencia.executeUpdate();
					System.out.println("Se actualizo el documento::: "+vlinea.get(5).toString());

					ps.setInt(1,Integer.parseInt(vlinea.get(5).toString()));
					ps.setInt(2,Integer.parseInt("21"));
					ps.setInt(3,Integer.parseInt("3"));
					ps.setInt(4,Integer.parseInt("2"));
					ps.setString(5,vlinea.get(4).toString());
					ps.executeUpdate();
					System.out.println("Se inserto en dis_cambio_estatus::: "+vlinea.get(5).toString());
				}
				psSentencia.close();
				ps.close();

				//insertar el acuse

				qry = "insert into com_acuse4 values (?,?,?,?,?,?,?,?,sysdate,?)";
				ps = con.queryPrecompilado(qry);
				ps.setString(1,acuse);
				ps.setInt(2,Integer.parseInt(icusuario));
				ps.setInt(3,Integer.parseInt(totaldoc));
				ps.setDouble(4,(new Double(mtodoc)).doubleValue());
				ps.setInt(5,Integer.parseInt(totaldocdl));
				ps.setDouble(6,(new Double(mtodocdl)).doubleValue());
				ps.setInt(7,Integer.parseInt(recibo));
				ps.setInt(8,Integer.parseInt("4"));
				ps.setString(9,"24EPO24FORMA9");
				ps.executeUpdate();
				ps.close();
				System.out.println("Se inserto acuse ");

	        }catch(SQLException e){
	        	ok = false;
				System.out.println("MantenimientoDistBean::guardaCambiosMasivo SQLExcepcion "+e+"\n"+qrySentencia);
	               throw new NafinException("DIST0011");
	        }catch(Exception e){
	        	ok = false;
				System.out.println("MantenimientoDistBean::guardaCambiosMasivo Excepcion "+e);
	            throw new NafinException("SIST0001");
	        }finally{
	 			System.out.println("MantenimientoDist::guardaCambiosMasivo(C)");
	       		if(con.hayConexionAbierta()){
	            	con.terminaTransaccion(ok);
	                con.cierraConexionDB();
				}
	        }
	}
					/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_epo
	 */
	private String validaMontoComprometido(String ic_epo) 	throws NafinException {
	System.out.println("validaMontoComprometido (E)");

		String	qrySentencia 	= "";
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String monto = "";

		try {
      con.conexionDB();

      qrySentencia = 	" SELECT FN_SALDO_COMPROMETIDO "+
                      " FROM DIS_LINEA_CREDITO_DM "+
                      " WHERE  ic_epo = "+ic_epo+
                      " AND ic_producto_nafin = 4 "+
											" AND df_vencimiento_adicional > SYSDATE "+
											" AND cg_tipo_solicitud = 'I' "+
											"  AND ic_estatus_linea = 12" ;

				System.out.println("qrySentencia" +qrySentencia);

      ps = con.queryPrecompilado(qrySentencia);
      rs = ps.executeQuery();
      ps.clearParameters();

      if(rs.next())	{
        monto = rs.getString("FN_SALDO_COMPROMETIDO")==null?"":rs.getString("FN_SALDO_COMPROMETIDO");
      }
      rs.close();
      ps.close();


	  	System.out.println("monto linea de Credito" +monto);

		} catch(Exception e) {
				System.out.println("validaMontoComprometido(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
        con.cierraConexionDB();
				System.out.println(" validaMontoComprometido(S)");
		}

		return monto;
	}


	public Vector getVecModalidad(String epo,String pyme, String tipoFinanciamiento)	//Sustituye a getComboModalidad
		throws NafinException{
		System.out.println("MantenimientoDist::getVecModalidad (E)");
		Vector vecFilas = new Vector();
		Vector vecColumnas = null;
		String credito = "";
		String valida = "";
		AccesoDB con  = null;
		ResultSet rs = null;
		try{
			con = new AccesoDB();
			con.conexionDB();
			rs = con.queryDB("select cg_tipo_credito "+
							  	"from comrel_pyme_epo_x_producto pep "+
								"where   pep.ic_pyme = "+ pyme +
								"    and pep.ic_epo = "+ epo +
								"    and pep.ic_producto_nafin = 4");
			if(rs.next())
				credito = rs.getString(1);
			rs.close();
			if ("C".equals(credito))
				valida = ",5";
			rs = con.queryDB("SELECT IC_TIPO_FINANCIAMIENTO,CD_DESCRIPCION "+
					"FROM COMCAT_TIPO_FINANCIAMIENTO "+
					"WHERE IC_TIPO_FINANCIAMIENTO IN ("+tipoFinanciamiento+valida + ")");
			System.out.println("Valida: "+ valida + "; Cr�dito: "+ credito + "; Pyme: " + pyme);
			while(rs.next()){
				vecColumnas = new Vector();
				vecColumnas.add((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));
				vecColumnas.add((rs.getString("CD_DESCRIPCION")==null)?"":rs.getString("CD_DESCRIPCION"));
				vecFilas.add(vecColumnas);
			}
			con.cierraStatement();
		}catch(Exception e){
			System.out.println("MantenimientoDist::getVecModalidad Exception "+e);
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta())
			con.cierraConexionDB();
			System.out.println("MantenimientoDist::getVecModalidad (S)");
		}
		return vecFilas;
	}

}

