package com.netro.distribuidores;

import com.nafin.ws.CargaDocDistribuidoresWSInfo;

import com.netro.exception.NafinException;
import com.netro.exception.NafinetException;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.Combos;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CrearArchivosAcuses;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

@Stateless(name = "CargaDocDistEJB" , mappedName = "CargaDocDistEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class CargaDocDistBean implements CargaDocDist {

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CargaDocDistBean.class);


private Hashtable setFechas(String ic_epo,AccesoDB con) throws NafinException
  {
	System.out.println("CargaDocDistBean::setFechas (E)");
	Hashtable		fechas				= new Hashtable();
	String fechaVencMin  ="", fechaVencMax ="";

	  String qrySentencia =
			" SELECT TO_CHAR (SYSDATE + ig_dias_minimo, 'dd/mm/yyyy') AS fechavencmin, "   +
			"        TO_CHAR (SYSDATE + ig_dias_maximo, 'dd/mm/yyyy') AS fechavencmax"   +
			"   FROM comrel_producto_epo"   +
			"  WHERE ic_producto_nafin = 4"   +
			"    AND ic_epo = "+ ic_epo;
	  ResultSet rs = null;



  try {
		rs = con.queryDB(qrySentencia);
		if (rs.next())
	  	{
  		fechaVencMin = rs.getString("FECHAVENCMIN");
  		fechaVencMax = rs.getString("FECHAVENCMAX");
  		}
		con.cierraStatement();
// Si la epo no especifico valores de dias minimos y/o m�ximos se toman los default
		if (fechaVencMin==null || fechaVencMax==null)
		{
			qrySentencia=
				" SELECT TO_CHAR (SYSDATE + in_dias_minimo, 'dd/mm/yyyy') AS fechavencmin, "   +
				"        TO_CHAR (SYSDATE + in_dias_maximo, 'dd/mm/yyyy') AS fechavencmax"   +
				"   FROM comcat_producto_nafin"   +
				"  WHERE ic_producto_nafin = 4"  ;
//			out.print(qrySentencia);
			rs = con.queryDB(qrySentencia);

			if (rs.next())
			  {
				if (fechaVencMin == null)
					fechaVencMin = rs.getString("FECHAVENCMIN");
				if (fechaVencMax == null)
					fechaVencMax = rs.getString("FECHAVENCMAX");
        }
      con.cierraStatement();
      }
		fechas.put("fechaVencMin",fechaVencMin);
		fechas.put("fechaVencMax",fechaVencMax);


    }catch(Exception e){
    throw new NafinException("SIST0001");
    }
	System.out.println("CargaDocDistBean::setFechas (S)");
		return fechas;
  }   //    FIN SET FECHAS


  public Vector getFechas(String ic_epo)
  	throws NafinException
  	{
	System.out.println("CargaDocDistBean::getFechas (E)");
		AccesoDB	con	= null;
		Vector		vecFilas	= new Vector();
		Hashtable		fechas				= new Hashtable();

		  try {
  	  	con = new AccesoDB();
    	  con.conexionDB();

       fechas =   setFechas(ic_epo,con);

		 String fechaVencMin  = fechas.get("fechaVencMin").toString();
		 String fechaVencMax  = fechas.get("fechaVencMax").toString();

				vecFilas.addElement(fechaVencMin);
 				vecFilas.addElement(fechaVencMax);
		  } catch(Exception e){
  	  		throw new NafinException("SIST0001");
    	} finally {
	    	if(con.hayConexionAbierta())
  	    	con.cierraConexionDB();
		System.out.println("CargaDocDistBean::getFechas (S)");
    	}
    return vecFilas;
    }
 	// Revisa si el numero de pyme existe en la relacion pyme_epo y la obtine para insertarla en la tabla temporal.
    private String[] validaRelPymeEpo(String ic_epo,String cg_num_distribuidor,AccesoDB con) throws NafinException {
        log.info("CargaDocDistBean::validaRelPymeEpo(E)");
        String qrySentencia = "";
        ResultSet rs	= null;
        PreparedStatement ps = null;
        String no_pyme	= "";
        String ic_pyme	= "";
        String aceptacion	= "";
        String retorno[] = new String[2];
        try {
            qrySentencia = 
                "SELECT rel.ic_pyme, "+
                "       pym.cg_razon_social, "+
                "       rel.cs_distribuidores "+
                "FROM comrel_pyme_epo rel, "+
                "     comcat_pyme pym "+
                "WHERE rel.ic_pyme = pym.ic_pyme "+
                "    AND rel.ic_epo = ? "+
                "    AND rel.cg_num_distribuidor = ? "+
                "    AND rel.cs_distribuidores IN (?,?)";
            log.debug("qrySentencia:"+qrySentencia);
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.valueOf(ic_epo));
            ps.setString(2, cg_num_distribuidor);
            ps.setString(3, "S");
            ps.setString(4, "R");
            rs = ps.executeQuery();
            ps.clearParameters();
            while(rs.next()) {
                ic_pyme = rs.getString(1);
                aceptacion = rs.getString(3);
            }
            rs.close();ps.close();
            //con.cierraStatement();
            retorno[0] = ic_pyme;
            
            String habilitado = "";
            if("".equals(ic_pyme)) {
                throw new NafinException("ANTI0010");
            } else {
                qrySentencia =	
                    "SELECT rel.ic_pyme, "+
                    "       relx.cs_habilitado, "+
                    "       relx.cg_tipo_credito "+
                    "FROM comrel_pyme_epo rel, "+
                    "     comrel_pyme_epo_x_producto relx "+
                    "WHERE rel.ic_epo = relx.ic_epo "+
                    "    AND rel.ic_pyme = relx.ic_pyme "+
                    "    AND relx.ic_producto_nafin = ? "+
                    "    AND rel.ic_epo = ? "+
                    "    AND rel.cg_num_distribuidor = ?";
                log.debug("qrySentencia:"+qrySentencia);
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, Integer.valueOf("4"));
                ps.setInt(2, Integer.valueOf(ic_epo));
                ps.setString(3, cg_num_distribuidor);
                rs = ps.executeQuery();
                ps.clearParameters();
                no_pyme="";
                while(rs.next()) {
                    no_pyme = rs.getString(1);
                    habilitado = rs.getString(2);
                    retorno[1] = rs.getString(3);
                }
                rs.close();ps.close();
                //con.cierraStatement();
                //Modificado EGB Foda 002-2004 Varios
                /*if("N".equals(habilitado)){
                    throw new NafinException("ANTI0009");
                }*/
            }   //else
        } catch(NafinException ne) {
            log.error(ne.getMsgError());
            throw ne;
        } catch(Exception e) {
            log.error("CargaDoctoBean::validaRelPymeEpo(Exception) " +e);
            throw new NafinException("SIST0001");
        } finally {
            try {
                if (rs!=null) rs.close();
                if (ps!=null) ps.close();
            } catch(Exception e) {
                log.error("CargaDoctoBean::validaRelPymeEpo(Exception)" + e.getMessage(), e);
            }
        }
        log.info("CargaDocDistBean::validaRelPymeEpo(S)");
        return retorno;
    } //VALIDA RELPYME EPO

  public  void borraTmpDoctoDetalle(String id)
    throws NafinException
      {
	  System.out.println("CargaDocDistBean::borraTmpDoctoDetalle (E)");
      boolean   resultado     = true;
      String    qrySentencia  = "";
      AccesoDB  con           = null;
      try
        {
          con = new AccesoDB();
          con.conexionDB();
    			qrySentencia="delete from distmp_docto_detalle where ic_consecutivo="+id;
          //            out.println(qrySentencia);
    			con.ejecutaSQL(qrySentencia);
          }catch(Exception e){
          resultado = false;
          throw new NafinException("SIST0001");
          }finally{
          con.terminaTransaccion(resultado);
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
			System.out.println("CargaDocDistBean::borraTmpDoctoDetalle (S)");
          }
      }

  public  void insertaTmpDoctoDetalle(String icProcDocto,String icPyme,String igNumeroDocto,Vector cg_campo)
    throws NafinException
     {
		System.out.println("CargaDocDistBean::insertaTmpDoctoDetalle (E)");
      boolean   resultado     = true;
      String    qrySentencia  = "";
      AccesoDB  con           = null;
      try
        {
     			if (cg_campo.get(0)!=null) cg_campo.set(0,"'"+cg_campo.get(0)+"'");else cg_campo.set(0,"NULL");
					if (cg_campo.get(1)!=null) cg_campo.set(1,"'"+cg_campo.get(1)+"'");else cg_campo.set(1,"NULL");
					if (cg_campo.get(2)!=null) cg_campo.set(2,"'"+cg_campo.get(2)+"'");else cg_campo.set(2,"NULL");
					if (cg_campo.get(3)!=null) cg_campo.set(3,"'"+cg_campo.get(3)+"'");else cg_campo.set(3,"NULL");
					if (cg_campo.get(4)!=null) cg_campo.set(4,"'"+cg_campo.get(4)+"'");else cg_campo.set(4,"NULL");
					if (cg_campo.get(5)!=null) cg_campo.set(5,"'"+cg_campo.get(5)+"'");else cg_campo.set(5,"NULL");
					if (cg_campo.get(6)!=null) cg_campo.set(6,"'"+cg_campo.get(6)+"'");else cg_campo.set(6,"NULL");
					if (cg_campo.get(7)!=null) cg_campo.set(7,"'"+cg_campo.get(7)+"'");else cg_campo.set(7,"NULL");
					if (cg_campo.get(8)!=null) cg_campo.set(8,"'"+cg_campo.get(8)+"'");else cg_campo.set(8,"NULL");
					if (cg_campo.get(9)!=null) cg_campo.set(9,"'"+cg_campo.get(9)+"'");else cg_campo.set(9,"NULL");
          con = new AccesoDB();
          con.conexionDB();
    			qrySentencia="insert into distmp_docto_detalle (ic_proc_docto, ic_pyme, "+
    				" ig_numero_docto, ic_consecutivo, cg_campo1, cg_campo2, cg_campo3, "+
    				" cg_campo4, cg_campo5, cg_campo6, cg_campo7, cg_campo8, cg_campo9, "+
    				" cg_campo10)"+
    				" values ("+icProcDocto+", "+icPyme+", '"+igNumeroDocto+"'"+
    				" , seq_comtmp_ped_det_ic_consec.NEXTVAL"+
    				" , "+cg_campo.get(0)+", "+cg_campo.get(1)+", "+cg_campo.get(2)+", "+cg_campo.get(3)+", "+cg_campo.get(4)+
    				" , "+cg_campo.get(5)+", "+cg_campo.get(6)+", "+cg_campo.get(7)+", "+cg_campo.get(8)+", "+cg_campo.get(9)+")";
          //            out.println(qrySentencia);
    			con.ejecutaSQL(qrySentencia);
          }catch(Exception e){
          resultado = false;
          throw new NafinException("SIST0001");
          }finally{
          con.terminaTransaccion(resultado);
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
			System.out.println("CargaDocDistBean::insertaTmpDoctoDetalle (S)");
          }
    }


  private void insertaTemporales(String ic_proc_docto,String ic_pyme,String ig_numero_docto,String df_fecha_emision,String df_fecha_venc,String ic_moneda,String fn_monto,String ct_referencia,Vector cg_campo,String ic_epo,String ic_tipo_financiamiento,String ig_plazo_dscto,String fn_porc_dscto,String ic_linea_credito_dm,String cs_plazo_descuento,String ic_clasificacion,AccesoDB con,String negociable,String operaVentaCartera,boolean isWS, String receipt,  String ic_tipo_Pago, String ic_pediodo)
    throws NafinException
    {
		log.info("CargaDocDistBean::insertaTemporales (E)");
		log.debug("CargaDocDistBean::ic_linea_credito_dm "+ic_linea_credito_dm);
		String      qrySentencia   = "";
		PreparedStatement   ps = null;
		ResultSet   rs             = null;
		int	    ic_consecutivo = 1;
		String ic_clasificacion1 = "";
		String campoReceipt = "";
		String valReceipt = "";
        if (ic_clasificacion.equals("NULL")) {  ic_clasificacion1 = "0";
        }else {  ic_clasificacion1 = ic_clasificacion;   }
        try{
        	//LLENAMOS LOS CAMPOS ADICIONALES PREVIAMENTE INSERTADOS EN EL METODO GETCAMPOS ADICIONALES
			String tabla="";
					if(isWS){
						 WSEnlace dist = new WSEnlace();
						tabla=	dist.getTablaDocumentos();
						campoReceipt = ", cg_receipt";
						valReceipt = ",'"+receipt+"'";
					}else{
						tabla = "distmp_proc_docto";

					}
                qrySentencia =
					"select max(nvl(ic_consecutivo,0))+1"+
					" from "+ tabla +
					" where ic_proc_docto = ? ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1, Integer.valueOf(ic_proc_docto));
				rs = ps.executeQuery();
				ps.clearParameters();
                if(rs.next())
                	ic_consecutivo = rs.getInt(1);
				rs.close();ps.close();
                //con.cierraStatement();

        	qrySentencia =
				"insert into "+ tabla+
				" (ic_consecutivo,ic_proc_docto,ic_epo,ic_pyme,ic_producto_nafin"+
				"  ,ig_numero_docto,df_fecha_emision,df_fecha_venc"+
				"  ,ic_moneda,ct_referencia,fn_porc_descuento"+
				"  ,ig_plazo_descuento,fn_monto,ic_tipo_financiamiento,ic_estatus_docto"+
				"  ,ic_linea_credito_dm"+
				"  ,cg_campo1,cg_campo2 ,cg_campo3, cg_campo4, cg_campo5,CS_PLAZO_DESCUENTO,ic_clasificacion, CG_VENTACARTERA,  ig_tipo_pago, IG_MESES_INTERES "+campoReceipt+")"+
				" values("+ic_consecutivo+","+ic_proc_docto+","+ic_epo+","+ic_pyme+",4"+
				" ,'"+ig_numero_docto+"',to_date('"+df_fecha_emision+"','dd/mm/yyyy'),to_date('"+df_fecha_venc+"','dd/mm/yyyy')"+
				" ,"+ic_moneda+",'"+ct_referencia+"',"+(("".equals(fn_porc_dscto))?"NULL":fn_porc_dscto)+
				" ,"+(("".equals(ig_plazo_dscto))?"NULL":ig_plazo_dscto)+","+fn_monto+",'"+ic_tipo_financiamiento+"',"+negociable+
				" ,"+ic_linea_credito_dm+
				",'" +cg_campo.get(0).toString().replace('\'',' ')+"',' "+cg_campo.get(1).toString().replace('\'',' ')+
				"',' "+cg_campo.get(2).toString().replace('\'',' ')+"','"+cg_campo.get(3).toString().replace('\'',' ')+
				"','"+cg_campo.get(4).toString().replace('\'',' ')+"','"+cs_plazo_descuento+"','"+ic_clasificacion1+				
				 "','"+operaVentaCartera+"', '"+ic_tipo_Pago+"','"+ic_pediodo+"'"+valReceipt+")";  
            log.debug(qrySentencia);
            con.ejecutaSQL(qrySentencia);
		}catch(Exception e){
			log.error("CargaDoctoBean::insertaTemporales Exception " +e);
			throw new NafinException("SIST0001");
		}
		log.info("CargaDocDistBean::insertaTemporales (S)");
    } //Inserta Temporales

  public String getDiasInhabiles()  throws NafinException
  {
	System.out.println("CargaDocDistBean::getDiasInhabiles (E)");
    AccesoDB con = null;
    String diasInhabiles = "";
   	String qrySentencia = "SELECT cg_dia_inhabil FROM comcat_dia_inhabil where cg_dia_inhabil is not null ";
    ResultSet rs = null;
    try{
      con = new AccesoDB();
      con.conexionDB();
    	rs = con.queryDB(qrySentencia);
    	while(rs.next()) { diasInhabiles += rs.getString(1)+";"; }
        con.cierraStatement();
    }catch(Exception e){
      throw new NafinException("SIST0001");
    }finally{
       if(con.hayConexionAbierta())
          con.cierraConexionDB();
	System.out.println("CargaDocDistBean::getDiasInhabiles (S)");
    }
   return diasInhabiles;
  }

        public String getClaveDoctoTmp() throws NafinException {
            log.info("CargaDocDistBean::getClaveDoctoTmp(E)");
            String maxClaveDocto = "1";
            String qrySentencia = 
                "select NVL(max(ic_proc_docto),?)+? as claveProceso"+
                " from distmp_proc_docto";
            AccesoDB con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                con = new AccesoDB();
                con.conexionDB();
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1,Integer.parseInt("0"));
                ps.setInt(2,Integer.parseInt("1"));
                rs = ps.executeQuery();
                ps.clearParameters();
                if(rs.next())
                    maxClaveDocto = rs.getString("CLAVEPROCESO");
                if(maxClaveDocto == null)
                    maxClaveDocto = "1";
                rs.close();ps.close();
            } catch(Exception e) {
                log.error("CargaDocDistBean::getClaveDoctoTmp(Exception) "+e);
                throw new NafinException("SIST0001");
            } finally {
                if(con.hayConexionAbierta())
                    con.cierraConexionDB();
                log.info("CargaDocDistBean::getClaveDoctoTmp(S)");
            }
            return maxClaveDocto;
        }//fin getClaveDoctoTmp


        public String getClaveDoctoTmpWS() throws NafinException { 
            log.info("CargaDocDistBean::getClaveDoctoTmpWS(E)");
            String maxClaveDocto = "1";
            String qrySentencia = 
                "select NVL(max(ic_proc_docto),?)+? as claveProceso"+
                " from DISTMP_PROC_DOCTO_WS";
            AccesoDB con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {            
                con = new AccesoDB();
                con.conexionDB();
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1,Integer.parseInt("0"));
                ps.setInt(2,Integer.parseInt("1"));
                rs = ps.executeQuery();
                ps.clearParameters();
                if(rs.next())
                    maxClaveDocto = rs.getString("CLAVEPROCESO");
                if(maxClaveDocto == null)
                    maxClaveDocto = "1";
                rs.close();ps.close();
            } catch(Exception e) {
                log.info("CargaDocDistBean::getClaveDoctoTmpWS(Exception) "+e);
                throw new NafinException("SIST0001");
            } finally {
                if(con.hayConexionAbierta())
                    con.cierraConexionDB();
                log.info("CargaDocDistBean::getClaveDoctoTmpWS(S)");
            }
            return maxClaveDocto;
        }//fin getClaveDoctoTmp


    private boolean existeDocto(String ic_epo,String ic_proc_docto,String ig_numero_docto,AccesoDB con)
      throws NafinException
      {
		log.info("CargaDocDistBean::existeDocto (E)");
       String qrySentencia="";
       boolean resultado = false;
       ResultSet rs = null;
       PreparedStatement ps = null;
       int numRegistros = 0;
       try
          {
          if(ic_proc_docto!=null && !"".equals(ic_proc_docto)){
           qrySentencia = "select count(1) as numRegistros from distmp_proc_docto"+
  	   			" where ic_proc_docto=?"+
    				" and ig_numero_docto=?"+
                                " and ic_epo=?";
           log.debug("qrySentencia:"+qrySentencia);
           ps = con.queryPrecompilado(qrySentencia);
           ps.setInt(1, Integer.valueOf(ic_proc_docto));
           log.trace("ic_proc_docto:"+ic_proc_docto);
           ps.setString(2, ig_numero_docto);
           log.trace("ig_numero_docto:"+ig_numero_docto);
           ps.setInt(3, Integer.valueOf(ic_epo));
           log.trace("ic_epo:"+ic_epo);
           rs = ps.executeQuery();
           ps.clearParameters();
           if(rs.next())
             numRegistros=rs.getInt("NUMREGISTROS");
           ps.close();rs.close();
           //con.cierraStatement();
          }

          qrySentencia=	"select count(1) as numRegistros from dis_documento"+
			" where ig_numero_docto= ? "+
                        " and ic_epo = ? "+
								" and ic_estatus_docto <> ? ";
          log.debug("qrySentencia:"+qrySentencia);
          ps = con.queryPrecompilado(qrySentencia);
          ps.setString(1, ig_numero_docto);
          log.trace("ig_numero_docto:"+ig_numero_docto);
          ps.setInt(2, Integer.valueOf(ic_epo));
          log.trace("ic_epo:"+ic_epo);
          ps.setInt(3, Integer.valueOf("5"));
          log.trace("ic_estatus_docto:"+5);
          rs = ps.executeQuery();
          ps.clearParameters();
          if(rs.next())
            numRegistros += rs.getInt("NUMREGISTROS");
          ps.close();rs.close();
          //con.cierraStatement();
          if(numRegistros==0)
            resultado = true;
          else
            throw new NafinException("DIST0003");
          }catch(NafinException ne) {
		System.out.println(ne.getMsgError());
            throw ne;
          }catch(Exception e){
            System.out.println("CargaDocDistBean::existeDocto Exception " +e);
            throw new NafinException("SIST0001");
       } finally { 
               try {
                   if (rs!=null) rs.close();
                   if (ps!=null) ps.close();
               } catch(Exception e) {
                   log.error("CargaDocDistBean::getParamEpoa(Exception)" + e.getMessage(), e);
               }               
           }
       
		log.info("CargaDocDistBean::existeDocto (S)");
       return resultado;
      }   //fin existe docto

    private boolean existeDocto(String ic_epo,String ic_proc_docto,AccesoDB con)
      throws NafinException
      {
	   log.info("CargaDocDistBean::existeDocto (E)");
       String qrySentencia="";
       boolean resultado = false;
       ResultSet rs = null;
       PreparedStatement ps = null;
       int numRegistros = 0;
       try
          {
	    		qrySentencia="select count(*) as numRegistros from dis_documento"+
		    	  	" where ig_numero_docto in("+
				" 	select ig_numero_docto from distmp_proc_docto"+
				" 	where ic_proc_docto=?"+
				" 	and ic_epo=?"+
				"	}"+
      				" and ic_epo=?";
			    log.debug("qrySentencia:"+qrySentencia);
			    ps = con.queryPrecompilado(qrySentencia);
			    ps.setInt(1, Integer.valueOf(ic_proc_docto));
			    log.trace("ic_proc_docto:"+ic_proc_docto);
			    ps.setInt(2, Integer.valueOf(ic_epo));
			    ps.setInt(3, Integer.valueOf(ic_epo));
			    log.trace("ic_epo:"+ic_epo);
			    rs = ps.executeQuery();
			    ps.clearParameters();
			    if(rs.next())
      			numRegistros += rs.getInt("NUMREGISTROS");
    			rs.close();ps.close();
    			//con.cierraStatement();
          if(numRegistros==0)
            resultado = true;
          else
            throw new NafinException("DIST0003");
          }catch(NafinException ne){
            throw ne;
          }catch(Exception e) {
            log.error("CargaDocDistBean::existeDocto Exception " +e);
            throw new NafinException("SIST0001");
       } finally {
               try {
                   if (rs!=null) rs.close();
                   if (ps!=null) ps.close();
               } catch(Exception e) {
                   log.error("CargaDocDistBean::existeDocto(Exception)" + e.getMessage(), e);
               }
               
           }
		log.info("CargaDocDistBean::existeDocto (S)");
       return resultado;
      }   //fin existe docto


 	 public HashMap tempIndividual(String df_fecha_venc,String df_fecha_emision,String ic_epo,String ic_proc_docto,String ig_numero_docto,String cg_pyme_epo_interno,String ic_moneda,String fn_monto,String ct_referencia,Vector cg_campo,String ic_tipo_financiamiento,String ig_plazo_dscto,String fn_porc_dscto, String cs_plazo_descuento, String ic_clasificacion, String cg_fecha_porc_desc, String negociable, String operaVentaCartera,  String ic_tipo_Pago, String ic_pediodo)
      throws NafinException{
		System.out.println("CargaDocDistBean::tempIndividual (E)");
		HashMap hmResult = new HashMap();
      boolean		resultado	= true;
      AccesoDB	con     	= null;
		boolean isWS=false;
      String		ic_pyme 	= "";
      String		validaDist	= "";
      String		validaMonto = "";
      String		acumulado	= "";
		String 		arreglo[] 	= new String[2];
		String		fin_selec	= ic_tipo_financiamiento;
		String qrySentencia  ="";
		ResultSet rs1 = null;
		PreparedStatement	ps1	= null;
		String ic_linea_credito_dm ="";
		String msgError ="";

      try{
			con = new AccesoDB();
         con.conexionDB();

         resultado = existeDocto(ic_epo,ic_proc_docto,ig_numero_docto,con);

			arreglo = validaRelPymeEpo(ic_epo, cg_pyme_epo_interno, con);
			ic_pyme = arreglo[0];

            if(resultado) {
              	validaFechas(df_fecha_venc, df_fecha_emision, ic_epo, ic_pyme, con);
            }
			if(!ic_tipo_financiamiento.equals("")) {
				ic_tipo_financiamiento = getTipoFinDefault(ic_epo,ic_tipo_financiamiento,con);
				if("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento)) {
					ig_plazo_dscto = getPlazoFinDefault(ic_epo,ic_tipo_financiamiento,ig_plazo_dscto,con);
					fn_porc_dscto =  getPorcFinDefault(ic_epo,ic_tipo_financiamiento,fn_porc_dscto,con);
					/*Validacion de fecha de publicacion*/
					if(resultado){
						resultado = validaPlazoDescto(ig_plazo_dscto,df_fecha_venc,df_fecha_emision,cg_fecha_porc_desc,con);
						if(!resultado){
							msgError = "El plazo para descuento no puede ser mayor a la Fecha de vencimiento del documento";
							hmResult.put("MSGERROR", msgError);
						}
					}

				} else {
						ig_plazo_dscto = "";
						 fn_porc_dscto = "";
				}
			}

			if ("5".equals(fin_selec)){
				validaDist = validaDistribuidor(ic_epo,cg_pyme_epo_interno,df_fecha_emision);
				if("S".equals(validaDist))
					throw new NafinException("DIST0038");
				acumulado = getMontoAcumulado(ic_epo,cg_pyme_epo_interno,ic_moneda,ic_proc_docto);
				validaMonto = validaSaldoDisponible(ic_epo,cg_pyme_epo_interno,ic_moneda,fn_monto,acumulado);
				if("N".equals(validaMonto))
					throw new NafinException("DIST0039");
				if("NL".equals(validaMonto))
					throw new NafinException("DIST0040");
			}

		log.debug("ic_linea_credito_dm  "+ic_linea_credito_dm);


			if(operaVentaCartera.equals("S")){
			qrySentencia = 	" SELECT ic_linea_credito_dm "+
                      " FROM DIS_LINEA_CREDITO_DM "+
                      " WHERE  ic_epo = ? "+
                      " AND ic_producto_nafin =  ? "+
											" AND ic_moneda =  ? "+
											" AND df_vencimiento_adicional > SYSDATE  "+
											" AND ic_estatus_linea = 12 "+
											" AND cg_tipo_solicitud = 'I' ";

      ps1 = con.queryPrecompilado(qrySentencia);
      ps1.setInt(1,Integer.parseInt(ic_epo));
      ps1.setInt(2,4);
			ps1.setInt(3,Integer.parseInt(ic_moneda));
      rs1 = ps1.executeQuery();
      ps1.clearParameters();

      if(rs1.next())	{
        ic_linea_credito_dm = rs1.getString("ic_linea_credito_dm")==null?"null":rs1.getString("ic_linea_credito_dm");
      }
      rs1.close();
      ps1.close();

			if(ic_linea_credito_dm.equals("")){ ic_linea_credito_dm = null; }

			}else{
			ic_linea_credito_dm = "null";
			}

			log.debug("ic_linea_credito_dm  "+ic_linea_credito_dm);


			insertaTemporales(ic_proc_docto,ic_pyme,ig_numero_docto,df_fecha_emision,df_fecha_venc,ic_moneda,fn_monto,
		              ct_referencia,cg_campo,ic_epo,ic_tipo_financiamiento,ig_plazo_dscto,fn_porc_dscto,ic_linea_credito_dm,cs_plazo_descuento,
		              ic_clasificacion,con,negociable,operaVentaCartera,isWS, "", ic_tipo_Pago ,  ic_pediodo );
		             

		  }catch(NafinException ne){
            resultado = false;
            System.out.println("ENTRANDO A NAFINEXCEPTION");
           // msgError = ne.getMsgError();
				hmResult.put("MSGERROR", ne.getMsgError());
        }catch(Exception e){
          System.out.println("CargaDoctoBean::tempIndividual Exception "+e);
			 System.out.println("--------"+e.getMessage());
			  hmResult.put("MSGERROR",e.getMessage());
          resultado = false;
          throw new NafinException("SIST0001");
        }finally{
          hmResult.put("RESULTADO",new Boolean(resultado));
			 con.terminaTransaccion(resultado);
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
			System.out.println("CargaDocDistBean::tempIndividual (S)");
        }
       //return resultado;

		 return hmResult;
      }
/**
   * metodo que valida la carga masiva de documentos de distribuidores
   * @throws com.netro.exception.NafinException
   * @return
   * @param limiteLineaCredito
   * @param TipoCredito
   * @param DescuentoAutomatico
   * @param fn_porc_descuento
   * @param ig_plazo_descuento
   * @param cg_clave
   * @param df_limite_dscto
   * @param ic_tipo_financiamiento
   * @param cs_cifras_control
   * @param ic_pyme
   * @param pipesporlinea
   * @param vecCampos
   * @param linea
   * @param txtmtomaxdocdol
   * @param txtmtomindocdol
   * @param txtmtomaxdoc
   * @param txtmtomindoc
   * @param fn_monto
   * @param ic_moneda
   * @param ig_numero_docto
   * @param df_fecha_emision
   * @param df_fecha_venc
   * @param ic_epo
   */
public String   validacionesMasiva(
          String ic_epo,
          String df_fecha_venc,
          String df_fecha_emision,
          String ig_numero_docto,
          String ic_moneda,
          String fn_monto,
          BigDecimal txtmtomindoc,
          BigDecimal txtmtomaxdoc,
          BigDecimal txtmtomindocdol,
          BigDecimal txtmtomaxdocdol,
          int linea,
          Vector vecCampos,
          int pipesporlinea,
          String ic_pyme,
          String cs_cifras_control,
          String ic_tipo_financiamiento,
          String df_limite_dscto,
          String cg_clave,
          String ig_plazo_descuento,
          String fn_porc_descuento,
          String DescuentoAutomatico,
          String TipoCredito,
          String limiteLineaCredito,
          String negociable,
			 String ventaCartera,
			 String strPerfil, //F032-2014
			 String tipo_Pago,  //F09-2015
			 String pediodo , //F09-2015
			 String meseSinIntereses,  //F09-2015
			 String cmbMeses //F09-2015
          )
      throws NafinException, NumberFormatException
      {
		log.info("CargaDocDistBean::validacionesMasiva (E)");
	    boolean resultado = true;
	   	AccesoDB con = null;
		ResultSet rs = null;
		String qrySentencia = "";
        String Parametr = "";
        BigDecimal monto = null;
        Vector vecColumnas = null;
        String NomFinancimiento ="";
        String qryModalidadPlazo ="";
        ResultSet rs2 = null;
        String valorFinanciamiento ="";
        String MontoLineaCredito = "";

        double MontoLineaCredito2 =0;
        String NOnegociable ="";
        String inhabilEpo ="";
        String fechaDeHoy = null;
        int no_dia_semana = 0;
        double porLineaCredito = 0;
        String validaLinea ="";
        
        String  msgError   = "";
        BigDecimal monto_mn=new BigDecimal("0.0");
        BigDecimal monto_dolar=new BigDecimal("0.0");
        int        totdoc_mn=0;
        int        totdoc_dolar=0;
        
        try {
            con = new AccesoDB();
            con.conexionDB();
            
		    double fn_monto2 =Double.parseDouble((String)fn_monto);
         /*Fodea 014-2010 Distrbuidores Fase II ini */
          if ("1".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Pronto Pago";     }
          if ("2".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Ampliado"; }
          if ("3".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Mixto";     }
          if ("4".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Pronto Pago Fijo"; }
          if ("5".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Ampliado Pemex"; }

          if (!ic_tipo_financiamiento.equals("") ){
                qryModalidadPlazo ="  select f.ic_tipo_financiamiento "+
                                   " from comrel_epo_tipo_financiamiento f,   "+
                                   "  comrel_producto_epo  e "+
                       " WHERE f.ic_producto_nafin = 4  "+
                       " AND f.ic_epo = "+ic_epo+""+
                       " AND f.ic_tipo_financiamiento = "+ic_tipo_financiamiento+""+
                       " and e.ic_epo = f.ic_epo "+
                       " and e.ic_producto_nafin = 4 ";
                rs2 = con.queryDB(qryModalidadPlazo);

                while(rs2.next()) 	{
                   valorFinanciamiento = rs2.getString("ic_tipo_financiamiento");
                 }
                 rs2.close();
                 con.cierraStatement();

                 if (!ic_tipo_financiamiento.equals(valorFinanciamiento) ){
                   msgError = "Error en la l&iacute;nea "+linea+",La Epo No opera el tipo de Modalidad de  "+NomFinancimiento+" \n";
                  resultado =  false;
                  }
         }
        /*Fodea 014-2010 Distrbuidores Fase II  */

        //Fodea  29-2010 Distribuidores Fase III inicia*/
			 if ( TipoCredito.equals("D")&& ventaCartera.equals("S") ) {
						validaLinea = validaLineCredito(ic_epo);
						if(validaLinea.equals("") || validaLinea == null || validaLinea.equals("null")) {
						msgError = "Error en la l&iacute;nea "+linea+", La Epo no tiene linea de Credito";
            resultado =  false;
					}
			 }


      if ((DescuentoAutomatico.equals("S") &&  limiteLineaCredito.equals("S") &&  TipoCredito.equals("C"))||(DescuentoAutomatico.equals("S") &&  limiteLineaCredito.equals("S") &&  TipoCredito.equals("D")&& ventaCartera.equals("S"))) {
         MontoLineaCredito = montoLineaCreCaptura(ic_epo);
         MontoLineaCredito2 =  Double.parseDouble((String)MontoLineaCredito);

         log.debug("fn_monto   "+fn_monto2+" MontoLineaCredito   "+MontoLineaCredito2);

         porLineaCredito =  (fn_monto2 * 100)/MontoLineaCredito2;

         if(!fn_monto.equals("") ) {
           if (fn_monto2 > MontoLineaCredito2){
            msgError = "Error en la l&iacute;nea "+linea+", El monto capturado rebasa el l�mite de su l�nea de cr�dito. Porcentaje Utilizado "+Comunes.formatoDecimal(porLineaCredito,2)+" %";
            resultado =  false;
					}
			}
    }
		if("ADMIN EPO".equals(strPerfil) ||  "ADMIN EPO GRUPO".equals(strPerfil) ||  "ADMIN EPO FON".equals(strPerfil)  || "ADMIN EPO DISTR".equals(strPerfil)  ) { //F032-2014 
		 if(negociable.equals("NS")){
			msgError = "Error en la l&iacute;nea "+linea+", El valor de Negociable no puede estar vacio";
			 resultado =  false;
		 }

		  if(!negociable.equals("NS") ){
			 NOnegociable =  DesAutomaticoEpo(ic_epo,"PUB_EPO_NO_NEGOCIABLES");
			  if( NOnegociable.equals("N") && negociable.equals("N")) {
					 msgError = "Error en la l&iacute;nea "+linea+", La EPO no se encuentra parametrizada para publicar documentos No Negociables";
					resultado =  false;
			  }
			}
		}
      //Fodea  29-2010 Distribuidores Fase III final*/

      qrySentencia = 	" select 1 from comrel_epo_tipo_financiamiento " +
								" where ic_tipo_financiamiento = '5'" +
								" and ic_epo = "+ic_epo;
				log.debug("\n qrySentencia: "+qrySentencia);
                rs = con.queryDB(qrySentencia);
            if(rs.next())
            	Parametr = "1";
            rs.close();
            con.cierraStatement();
            if(!("".equals(ic_tipo_financiamiento)||"1".equals(ic_tipo_financiamiento)||"2".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento)||"1".equals(Parametr)))
            	throw new NafinException("DIST0009");
            int moneda=0;

			if(!"".equals(ic_moneda)&&ic_moneda!=null) {
				if(Comunes.esNumero(ic_moneda)==false)
                  throw new NafinException("GRAL0014");
				else {
					moneda=Integer.parseInt(ic_moneda);
					if(fn_monto == null || "".equals(fn_monto))
			            throw new NafinException("GRAL0011");
					else if(!Comunes.esDecimal(fn_monto))
			            throw new NafinException("GRAL0012");
					else if(Double.parseDouble(fn_monto)<=0)
						throw new NafinException("GRAL0019");
					if((moneda != 1) && (moneda != 54))
		                    throw new NafinException("GRAL0015");
					else {
						monto = new BigDecimal(fn_monto);
						if(moneda == 1) {	//Si es Moneda Nacional
							if(monto.compareTo(txtmtomindoc) < 0 && "S".equals(cs_cifras_control)){
								msgError = "Error en la l&iacute;nea "+linea+", el Monto M&iacute;nimo de Entrada en M.N. "+txtmtomindoc.toPlainString()+" es Mayor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Mayor. \n";
								resultado =  false;
							}else if(monto.compareTo(txtmtomaxdoc) > 0 && "S".equals(cs_cifras_control)){
								msgError = "Error en la l&iacute;nea "+linea+", el Monto Maximo de Entrada en M.N. "+txtmtomaxdoc.toPlainString()+" es Menor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Menor. \n";
								resultado = false;
							}
						monto_mn = monto_mn.add(monto);
						totdoc_mn++;
						}
						if(moneda == 54) {	//Si son Dolares.
							if(monto.compareTo(txtmtomindocdol) < 0 && "S".equals(cs_cifras_control)){
								msgError = "Error en la l&iacute;nea "+linea+", el Monto M&iacute;nimo de Entrada en D&oacute;lares "+txtmtomindocdol.toPlainString()+" es Mayor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Mayor. \n";
								resultado = false;
							}else if(monto.compareTo(txtmtomaxdocdol) > 0 && "S".equals(cs_cifras_control)){
								msgError = "Error en la l&iacute;nea "+linea+", el Monto M&aacute;ximo de Entrada en D&oacute;lares "+txtmtomaxdocdol.toPlainString()+" es Menor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Menor. \n";
								resultado = false;
							}
							monto_dolar = monto_dolar.add(monto);
							totdoc_dolar++;
						}
					}   //fin else
				} // fin else           (TIPO DE MONEDA)
			}
			else
			   throw new NafinException("GRAL0013");

          //VALIDAMOS QUE NO SE EXCEDA EL NUMERO DE CAMPOS PERMITIDOS
			 int  numCamposAd = vecCampos.size();
          for(int i=0;i<vecCampos.size();i++){
            if(!"".equals((String)vecCampos.get(i))&&(i>=numCamposAd)) {
		msgError = "El Campo Din�mico No."+(i+1)+" no est� definido por la EPO";
		resultado =false;
            }
          }

			  Vector vecCamposAd=  getCamposAdicionales(ic_epo,false);

          for(int i=0;i<vecCamposAd.size();i++){
              vecColumnas = (Vector)vecCamposAd.get(i);
              if("N".equals((String)vecColumnas.get(2))&&!"".equals((String)vecCampos.get(i))&&vecCampos.get(i)!=null) {
              	if(!Comunes.esNumero((String)vecCampos.get(i))) {
					msgError = "El Campo Din�mico No."+(i+1)+" debe de ser Num�rico";
					resultado = false;
              }
          }
          if(Integer.parseInt((String)vecColumnas.get(3)) < vecCampos.get(i).toString().length()) {
		msgError = "El Campo Din�mico No."+(i+1)+" excede la longitud definida por su epo";
          }
	}

          //Validamos que los campos definidos por la epo esten capturados
        pipesporlinea = pipesporlinea -6;
        if(numCamposAd > pipesporlinea)
          {
          msgError = "Faltan Campos";
          resultado = false;
          }

            if("".equals(ig_numero_docto)||ig_numero_docto==null)
              throw new NafinException("ANTI0013");
            if("".equals(df_fecha_emision) || df_fecha_emision ==null)
              throw new NafinException("ANTI0014");
            if(!Comunes.checaFecha(df_fecha_emision))
              throw new NafinException("GRAL0010");
            if("".equals(df_fecha_venc) || df_fecha_venc ==null)
              throw new NafinException("ANTI0015");
            if(!Comunes.checaFecha(df_fecha_venc))
              throw new NafinException("GRAL0010");
            if(!"".equals(df_limite_dscto))
            	if(!Comunes.checaFecha(df_limite_dscto))
              		throw new NafinException("GRAL0010");
            if(pipesporlinea<0)
              throw new NafinException("GRAL0016");
			if("".equals(ic_pyme) || ic_pyme==null)
              throw new NafinException("GRAL0017");
			else if(validaPymeXEpoBloqXNumProv(ic_pyme, ic_epo, "4")){
				msgError = "No es posible realizar la carga del documento , el Proveedor est� bloqueado \n";
				resultado =  false;
			}

			if(!"".equals(cg_clave) && (!"".equals(ig_plazo_descuento) || !"".equals(fn_porc_descuento) || !"".equals(df_limite_dscto)))
					throw new NafinException("DIST0032");
			if(!"".equals(ig_plazo_descuento) && "".equals(fn_porc_descuento))
					throw new NafinException("DIST0033");
			if("".equals(ig_plazo_descuento) && !"".equals(fn_porc_descuento))
					throw new NafinException("DIST0033");

		/*Para Validar la fecha de Emision	Fodea 029-2010 Distribuidores Fase III	 */
		Calendar gcDiaFecha = new GregorianCalendar();
    java.util.Date fechaEmisionNvo = Comunes.parseDate(df_fecha_emision);
    gcDiaFecha.setTime(fechaEmisionNvo);
    no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
		inhabilEpo = getDiasInhabilesDes(ic_epo, df_fecha_emision);

		fechaDeHoy 	= Fecha.getFechaActual();

		log.debug("no_dia_semana   " +no_dia_semana+"   inhabilEpo  "+inhabilEpo);

			SimpleDateFormat formatEntrada = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date df_fecha_emision2 = formatEntrada.parse(df_fecha_emision);
			java.util.Date  fechaDeHoy2 = Comunes.parseDate(fechaDeHoy);

		if ( TipoCredito.equals("D") && ventaCartera.equals("S")) {	 // se quita la condici�n de validar Descuento Automatica determinado por Edgar ya que no se usa
			if (  fechaDeHoy2.compareTo(df_fecha_emision2)<0  )  {
					msgError = "Error en la l&iacute;nea "+linea+",La Fecha de Emisi�n debe ser igual o menor al d�a de hoy  \n";
					resultado =  false;
			}
		}
		/*Para Validar la fecha de Emision		 */

			//Fodea 09-2015 (E)
			if (meseSinIntereses.equals("S")) {

				if ("".equals(tipo_Pago) || tipo_Pago == null) {
					msgError = "Error en la l&iacute;nea " + linea + ",El campo Tipo de Pago es obligatorio  \n";
					resultado = false;
				}
				if (!"".equals(tipo_Pago) && tipo_Pago != null) {

					if (Comunes.esNumero(tipo_Pago) == false) {
						msgError = "Error en la l&iacute;nea " + linea + ",El campo Tipo de Pago debe ser num�rico \n";
						resultado = false;

					} else if (Comunes.esNumero(tipo_Pago) == true) {
						if (!"1".equals(tipo_Pago) && !"2".equals(tipo_Pago)) {
							msgError = "Error en la l&iacute;nea " + linea +
												  ",El campo Tipo de Pago s�lo acepta las opciones 1 (Financiamiento con intereses) o 2 (Meses sin intereses). \n";
							resultado = false;
						}
					}
				}

				if ("2".equals(tipo_Pago)) {

					if (!"2".equals(ic_tipo_financiamiento)) {
						msgError = "Error en la l&iacute;nea " + linea + ", el tipo de Modalidad  " + NomFinancimiento +
											" no corresponde para los documentos que ser�n financiados a meses sin intereses  \n";
						resultado = false;
					}

					if ("".equals(pediodo) || pediodo == null) {
						msgError = "Error en la l&iacute;nea " + linea +
											",El campo Periodo es obligatorio si los documentos ser�n financiados a meses sin intereses  \n";
						resultado = false;
					}

					if (!"".equals(pediodo) && pediodo != null) {
						if (Comunes.esNumero(pediodo) == false) {
							msgError = "Error en la l&iacute;nea " + linea + ",El campo Periodo debe ser num�rico \n";
							resultado = false;

						} else if (Comunes.esNumero(pediodo) == true) {
							if (Integer.parseInt(pediodo) < 2) {
								msgError = "Error en la l&iacute;nea " + linea +
														 ",El campo Periodo no debe de ser menor a 2 meses \n";
								resultado = false;
							}
							if (Integer.parseInt(pediodo) > Integer.parseInt(cmbMeses)) {
								msgError = "Error en la l&iacute;nea " + linea +
														 ",El campo Periodo excede los meses de acuerdo a lo parametrizado \n";
								resultado = false;
							}
						}

					}
				}
			}
			//Fodea 09-2015 (S)

		} catch(NafinException ne) {
            log.error("CargaDocDistBean::validacionesMasiva(NafinException) "+ne);
            msgError = ne.getMsgError();
            throw ne;
        } catch(NumberFormatException nfe) {
            log.error("CargaDocDistBean::validacionesMasiva(NumberFormatException) "+nfe);
            msgError = "Se esperaba un valor num�rico,"+nfe.getMessage();
//            throw nfe;
        } catch(Exception e) {
            log.error("CargaDocDistBean::validacionesMasiva(Exception) "+e);
            msgError = "El layout es incorrecto, por favor verif&iacute;quelo";
            throw new NafinException("SIST0001");
        } finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CargaDocDistBean::validacionesMasiva (S)");
		}
        return msgError;
    }//validacionesMasiva


/**
   * metodo que valida la carga masiva de documentos de distribuidores
   * @throws com.netro.exception.NafinException
   * @return
   * @param limiteLineaCredito
   * @param TipoCredito
   * @param DescuentoAutomatico
   * @param fn_porc_descuento
   * @param ig_plazo_descuento
   * @param cg_clave
   * @param df_limite_dscto
   * @param ic_tipo_financiamiento
   * @param cs_cifras_control
   * @param ic_pyme
   * @param pipesporlinea
   * @param vecCampos
   * @param linea
   * @param txtmtomaxdocdol
   * @param txtmtomindocdol
   * @param txtmtomaxdoc
   * @param txtmtomindoc
   * @param fn_monto
   * @param ic_moneda
   * @param ig_numero_docto
   * @param df_fecha_emision
   * @param df_fecha_venc
   * @param ic_epo
   */
	/*
public boolean validacionesMasivaWS( WSEnlace disEnlace ) throws NafinException{
	boolean retorno=false;
	System.out.println("CargaDocDistBean::validacionesMasiva (E)");
	boolean resultado = true;
	AccesoDB con = null;
	ResultSet rs = null;

	String qrySentencia = "";
	String Parametr = "";
	BigDecimal monto = null;
	Vector vecColumnas = null;
	String NomFinancimiento ="";
	String qryModalidadPlazo ="";
	ResultSet rs2 = null;
	String valorFinanciamiento ="";
	String MontoLineaCredito = "";
	double fn_monto2;
	double MontoLineaCredito2 =0;
	String NOnegociable ="";
	String inhabilEpo ="";
	String fechaDeHoy = null;
	int no_dia_semana = 0;
	double porLineaCredito = 0;
	String validaLinea ="";
	StringBuffer noError=new StringBuffer();

	try{
		con = new AccesoDB();
		con.conexionDB();

		int hayreg=0;
		String query = " SELECT COUNT (1), 'CargaDocumentoBean::proceso()' origenquery "   +
							" FROM "+disEnlace.getTablaDocumentos()+" "   +
							disEnlace.getCondicionQuery();

		log.debug("query: " + query);
		rs = con.queryDB(query);
		if (rs.next()) {
			hayreg = rs.getInt(1);
		}
		rs.close(); con.cierraStatement();

		if(hayreg > 0){
			Acuse acuse=new Acuse(Acuse.ACUSE_EPO);
			AcuseEnl	acuEnl	= new AcuseEnl();
			acuEnl.setCcAcuse(acuse.toString());

			String ic_epo="";
			String df_fecha_venc="";
			String df_fecha_emision="";
			String ig_numero_docto="";
			String ic_moneda="";
			String fn_monto="";
			int linea =0;
			Vector vecCampos;
			//int pipesporlinea="";
			String ic_pyme="";
			//String cs_cifras_control,
			String ic_tipo_financiamiento="";
			String df_limite_dscto="";
			String cg_clave="";
			String ig_plazo_descuento="";
			String fn_porc_descuento="";
			String referencia="";
			String ic_linea_credito_dm="";
			String cs_plazo_descuento="";
			String ic_clasificacion="";
			String cg_ventacartera="";

			String negociable="";
			String ventaCartera="";
			String cd1,cd2,cd3,cd4,cd5;


			ParametrosDistHome parametrosDistHome = (ParametrosDistHome)ServiceLocator.getInstance().getEJBHome("ParametrosDistEJB", ParametrosDistHome.class);
			ParametrosDist BeanParametros = parametrosDistHome.create();

			NafinetExceptionHome nafinExceptionHome = (NafinetExceptionHome)ServiceLocator.getInstance().getEJBHome("NafinetExceptionEJB", NafinetExceptionHome.class);
			NafinetException nafException = nafinExceptionHome.create();

			String	DescuentoAutomatico =BeanParametros.DesAutomaticoEpo(disEnlace.getIc_epo(),"PUB_EPO_DESC_AUTOMATICO" );
			String limiteLineaCredito =BeanParametros.DesAutomaticoEpo(disEnlace.getIc_epo(),"PUB_LIMITE_LINEA_CREDITO");//Fodea 029-2010 Distribuodores Fase III
			String TipoCredito =  BeanParametros.obtieneTipoCredito (disEnlace.getIc_epo()); //Fodea 029-2010 Distribuodores Fase III
			//String ventaCartera =BeanParametros.DesAutomaticoEpo(disEnlace.getIc_epo(),"PUB_EPO_VENTA_CARTERA");



			String noUsuario = "enlac_aut";
			String noRecibo = "";
			String cargaEnlace = "";
			if(disEnlace instanceof WSEnlace) {
				noUsuario 	= ((WSEnlace)disEnlace).getIc_usuario();
				noRecibo 	= ((WSEnlace)disEnlace).getCg_receipt();
				cargaEnlace = "S";
			}

			PreparedStatement	ps		= null;
			qrySentencia =	" insert into dis_documento"+
				"(ic_documento,ic_epo,ic_pyme,ic_producto_nafin,cc_acuse,ig_numero_docto"+
					" ,df_fecha_emision,df_fecha_venc,ic_moneda,ct_referencia,fn_porc_descuento"+
					" ,ig_plazo_descuento,fn_monto,ic_tipo_financiamiento,ic_estatus_docto"+
					" ,ic_linea_credito_dm"+
					" ,cg_campo1,cg_campo2,cg_campo3,cg_campo4,cg_campo5,CS_PLAZO_DESCUENTO,ic_clasificacion, CG_VENTACARTERA)"+
					"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

				ps = con.queryPrecompilado(qrySentencia);
			// Inserta los datos del Acuse.
			acuEnl.setCcAcuse(acuse.toString());
			int totdocmn=0, totdocdol=0, linedoctos=0, itotacepmn=0, itotrechmn=0, itotacepdol=0, itotrechdol=0;
			BigDecimal mtodocmn=new BigDecimal(0.0); BigDecimal mtodocdol=new BigDecimal(0.0);
			query =
				" INSERT INTO com_acuse1"   +
				"             (cc_acuse, in_total_docto_mn, fn_total_monto_mn,"   +
				"              df_fechahora_carga, in_total_docto_dl, fn_total_monto_dl,"   +
				"              ic_usuario, cg_recibo_electronico, cs_carga_ws)"   +
				"      VALUES ('"+acuEnl.getCcAcuse()+"', "+acuEnl.getInTotalDoctoMn()+", "+acuEnl.getFnTotalMontoMn()+","   +
				"              SYSDATE, "+acuEnl.getInTotalDoctoDl()+", "+acuEnl.getFnTotalMontoDl()+","   +
				"              '"+noUsuario+"', '"+noRecibo+"', '"+cargaEnlace+"')"  ;
			log.debug(query);
			try {
				con.ejecutaSQL(query);
			} catch(Exception e) {
				//sblog.append("Error al insertar a com_acuse1.\n");
			}
			//if(!con.ejecutaSQL(query)) { log.error("Error al insertar a com_acuse1.\n"); }

			// GENERAL

			String queri = disEnlace.getDocuments() ;
			log.debug(queri);
			rs = con.queryDB(queri);
			ic_epo=disEnlace.getIc_epo();
			while(rs.next()) {

			//ic_epo = rs.getString("ic_epo").trim();
			df_fecha_venc = rs.getString("df_fecha_venc")==null?"":rs.getString("df_fecha_venc").trim();
			df_fecha_emision= rs.getString("df_fecha_emision")==null?"":rs.getString("df_fecha_emision").trim();
			ig_numero_docto= rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto").trim();
			ic_moneda= rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda").trim();
			fn_monto= rs.getString("fn_monto")==null?"":rs.getString("fn_monto").trim();
			//pipesporlinea= rs.getString(1).trim();
			referencia = rs.getString("ct_referencia")==null?"":rs.getString("ct_referencia");
			ic_pyme= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme").trim();
			ic_tipo_financiamiento= rs.getString("ic_tipo_financiamiento")==null?"":rs.getString("ic_tipo_financiamiento").trim();
			//df_limite_dscto= rs.getString(1).trim();
			//cg_clave= rs.getString(1).trim();
			ig_plazo_descuento= rs.getString("ig_plazo_descuento")==null?"":rs.getString("ig_plazo_descuento").trim();
			fn_porc_descuento= rs.getString("fn_porc_descuento")==null?"":rs.getString("fn_porc_descuento").trim();
			negociable= rs.getString("ic_estatus_docto")==null?"":rs.getString("ic_estatus_docto").trim();
			ic_linea_credito_dm = rs.getString("ic_linea_credito_dm")==null?"":rs.getString("ic_linea_credito_dm").trim();
			cs_plazo_descuento = rs.getString("cs_plazo_descuento")==null?"":rs.getString("cs_plazo_descuento").trim();
			ic_clasificacion = rs.getString("ic_clasificacion")==null?"":rs.getString("ic_clasificacion").trim();
			cg_ventacartera= rs.getString("cg_ventacartera")==null?"":rs.getString("cg_ventacartera").trim();

			cd1 				= (rs.getString( "cg_campo1")==null)?"":rs.getString( "cg_campo1");
			cd2 				= (rs.getString("cg_campo2")==null)?"":rs.getString("cg_campo2");
			cd3 				= (rs.getString("cg_campo3")==null)?"":rs.getString("cg_campo3");
			cd4 				= (rs.getString("cg_campo4")==null)?"":rs.getString("cg_campo4");
			cd5 				= (rs.getString("cg_campo5")==null)?"":rs.getString("cg_campo5");

			vecCampos = new Vector();
			vecCampos.addElement(cd1);
			vecCampos.addElement(cd2);
			vecCampos.addElement(cd3);
			vecCampos.addElement(cd4);
			vecCampos.addElement(cd5);


			fn_monto2 =Double.parseDouble((String)fn_monto);

         //Fodea 014-2010 Distrbuidores Fase II ini
          if ("1".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Pronto Pago";     }
          if ("2".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Ampliado"; }
          if ("3".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Mixto";     }
          if ("4".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Pronto Pago Fijo"; }
          if ("5".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Ampliado Pemex"; }

          if (!ic_tipo_financiamiento.equals("") ){
                qryModalidadPlazo ="  select f.ic_tipo_financiamiento "+
                                   " from comrel_epo_tipo_financiamiento f,   "+
                                   "  comrel_producto_epo  e "+
                       " WHERE f.ic_producto_nafin = 4  "+
                       " AND f.ic_epo = "+ic_epo+""+
                       " AND f.ic_tipo_financiamiento = "+ic_tipo_financiamiento+""+
                       " and e.ic_epo = f.ic_epo "+
                       " and e.ic_producto_nafin = 4 ";
                rs2 = con.queryDB(qryModalidadPlazo);

                while(rs2.next()) 	{
                   valorFinanciamiento = rs2.getString("ic_tipo_financiamiento");
                 }

                 if (!ic_tipo_financiamiento.equals(valorFinanciamiento) ){
                   msgError = "Error en la l&iacute;nea "+linea+",La Epo No opera el tipo de Modalidad de  "+NomFinancimiento+" \n";
						 noError.append("17\n");
                  resultado =  false;
                  }
         }
        //Fodea 014-2010 Distrbuidores Fase II

        //Fodea  29-2010 Distribuidores Fase III inicia
			 if ( TipoCredito.equals("D")&& ventaCartera.equals("S") ) {
					validaLinea = validaLineCredito(ic_epo);
					if(validaLinea.equals("") || validaLinea == null || validaLinea.equals("null")) {
						msgError = "Error en la linea "+linea+", La Epo no tiene linea de Credito";
						noError.append("22\n");
						resultado =  false;
					}
			 }


      if ((DescuentoAutomatico.equals("S") &&  limiteLineaCredito.equals("S") &&  TipoCredito.equals("C"))||(DescuentoAutomatico.equals("S") &&  limiteLineaCredito.equals("S") &&  TipoCredito.equals("D")&& ventaCartera.equals("S"))) {
         MontoLineaCredito = montoLineaCreCaptura(ic_epo);
         MontoLineaCredito2 =  Double.parseDouble((String)MontoLineaCredito);

         log.debug("fn_monto   "+fn_monto2+" MontoLineaCredito   "+MontoLineaCredito2);

         porLineaCredito =  (fn_monto2 * 100)/MontoLineaCredito2;

         if(!fn_monto.equals("") ) {
           if (fn_monto2 > MontoLineaCredito2){
            msgError = "Error en la l&iacute;nea "+linea+", El monto capturado rebasa el l�mite de su l�nea de cr�dito. Porcentaje Utilizado "+Comunes.formatoDecimal(porLineaCredito,2)+" %";
				noError.append("13\n");
            resultado =  false;
					}
			}
    }

		if(negociable.equals("NS")){
			msgError = "Error en la l&iacute;nea "+linea+", El valor de Negociable no puede estar vacio";
			noError.append("13\n");
			resultado =  false;
		}

		if(!negociable.equals("NS") ){
			NOnegociable =  DesAutomaticoEpo(ic_epo,"PUB_EPO_NO_NEGOCIABLES");
			if( NOnegociable.equals("N") && negociable.equals("N")) {
				 msgError = "Error en la l&iacute;nea "+linea+", La EPO no se encuentra parametrizada para publicar documentos No Negociables";
				 noError.append("28\n");
				resultado =  false;
			}
		}
      //Fodea  29-2010 Distribuidores Fase III final

      qrySentencia = 	" select 1 from comrel_epo_tipo_financiamiento " +
								" where ic_tipo_financiamiento = '5'" +
								" and ic_epo = "+ic_epo;
				System.out.println("\n qrySentencia: "+qrySentencia);
                rs = con.queryDB(qrySentencia);
            if(rs.next())
            	Parametr = "1";
            if(!("".equals(ic_tipo_financiamiento)||"1".equals(ic_tipo_financiamiento)||"2".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento)||"1".equals(Parametr))){
					msgError = nafException.getMensajeError("DIST0009", "ES");
					noError.append("29\n");
					resultado =  false;
					//throw new NafinException("DIST0009");
				}
            int moneda=0;

			if(!"".equals(ic_moneda)&&ic_moneda!=null) {
				if(Comunes.esNumero(ic_moneda)==false){
               msgError = nafException.getMensajeError("GRAL0014", "ES");
					noError.append("GRAL0014\n");
					resultado =  false;
						//throw new NafinException("GRAL0014");
				}
				else {
					moneda=Integer.parseInt(ic_moneda);
					if(fn_monto == null || "".equals(fn_monto)){
							msgError = nafException.getMensajeError("GRAL0011", "ES");
							noError.append("GRAL0011\n");
							resultado =  false;
					      //throw new NafinException("GRAL0011");
					}
					else if(!Comunes.esDecimal(fn_monto)){
			            msgError = nafException.getMensajeError("GRAL0012", "ES");
							noError.append("GRAL0012\n");
							resultado =  false;
							//throw new NafinException("GRAL0012");
					}
					else if(Double.parseDouble(fn_monto)<=0){
						msgError = nafException.getMensajeError("GRAL0019", "ES");
						noError.append("GRAL0019\n");
						resultado =  false;
						//throw new NafinException("GRAL0019");
					}
					if((moneda != 1) && (moneda != 54)){
		            msgError = nafException.getMensajeError("GRAL0015", "ES");
						noError.append("GRAL0015\n");
						resultado =  false;
						//        throw new NafinException("GRAL0015");
					}
					else {
						monto = new BigDecimal(fn_monto);
						if(moneda == 1) {	//Si es Moneda Nacional

						monto_mn = monto_mn.add(monto);
						totdoc_mn++;
						}
						if(moneda == 54) {	//Si son Dolares.

							monto_dolar = monto_dolar.add(monto);
							totdoc_dolar++;
						}
					}   //fin else
				} // fin else           (TIPO DE MONEDA)
			}
			else{

			 msgError = nafException.getMensajeError("GRAL0013", "ES");
			 noError.append("GRAL0013\n");
			 resultado =  false;
			 //  throw new NafinException("GRAL0013");
			}
          //VALIDAMOS QUE NO SE EXCEDA EL NUMERO DE CAMPOS PERMITIDOS
			 int  numCamposAd = vecCampos.size();
          for(int i=0;i<vecCampos.size();i++){
            if(!"".equals((String)vecCampos.get(i))&&(i>=numCamposAd)) {
					msgError = "El Campo Din�mico No."+(i+1)+" no est� definido por la EPO";
					noError.append("24\n");
					resultado =false;
            }
          }

			 Vector vecCamposAd=  getCamposAdicionales(ic_epo,false);

          for(int i=0;i<vecCamposAd.size();i++){
              vecColumnas = (Vector)vecCamposAd.get(i);
              if("N".equals((String)vecColumnas.get(2))&&!"".equals((String)vecCampos.get(i))&&vecCampos.get(i)!=null) {
              	if(!Comunes.esNumero((String)vecCampos.get(i))) {
					msgError = "El Campo Din�mico No."+(i+1)+" debe de ser Num�rico";
					noError.append("25\n");
					resultado = false;
              }
          }
          if(Integer.parseInt((String)vecColumnas.get(3)) < vecCampos.get(i).toString().length()) {
				msgError = "El Campo Din�mico No."+(i+1)+" excede la longitud definida por su epo";
				noError.append("26\n");
				resultado = false;
          }
	}

          //Validamos que los campos definidos por la epo esten capturados
       // pipesporlinea = pipesporlinea -6;
        //if(numCamposAd > pipesporlinea)
         // {
         // msgError = "Faltan Campos";
         // resultado = false;
         // }

            if("".equals(ig_numero_docto)||ig_numero_docto==null){
					msgError = nafException.getMensajeError("ANTI0013", "ES");
					noError.append("ANTI0013\n");
					resultado =  false;
					// throw new NafinException("ANTI0013");
				}
            if("".equals(df_fecha_emision) || df_fecha_emision ==null){
              msgError = nafException.getMensajeError("ANTI0014", "ES");
				  noError.append("ANTI0014\n");
					resultado =  false;
				  //throw new NafinException("ANTI0014");
				}
            if(!Comunes.checaFecha(df_fecha_emision)){
             msgError = nafException.getMensajeError("GRAL0010", "ES");
				 noError.append("GRAL0010\n");
				 resultado =  false;
				 // throw new NafinException("GRAL0010");
				}
            if("".equals(df_fecha_venc) || df_fecha_venc ==null){
              msgError = nafException.getMensajeError("ANTI0015", "ES");
				  noError.append("ANTI0015\n");
				 resultado =  false;
				  //throw new NafinException("ANTI0015");
				}
            if(!Comunes.checaFecha(df_fecha_venc)){
              msgError = nafException.getMensajeError("GRAL0010", "ES");
				  noError.append("GRAL0010\n");
				 resultado =  false;
				  //throw new NafinException("GRAL0010");
				}
            if(!"".equals(df_limite_dscto))
            	if(!Comunes.checaFecha(df_limite_dscto)){
						msgError = nafException.getMensajeError("GRAL0010", "ES");
						noError.append("GRAL0010\n");
						resultado =  false;
				//		throw new NafinException("GRAL0010");
					}
            //if(pipesporlinea<0)
              //throw new NafinException("GRAL0016");
			if("".equals(ic_pyme) || ic_pyme==null){

				msgError = nafException.getMensajeError("GRAL0017", "ES");
				noError.append("GRAL0017\n");
				resultado =  false;
            //  throw new NafinException("GRAL0017");
			}
			if(!"".equals(cg_clave) && (!"".equals(ig_plazo_descuento) || !"".equals(fn_porc_descuento) || !"".equals(df_limite_dscto))){
					msgError = nafException.getMensajeError("DIST0032", "ES");
					noError.append("DIST0032\n");
					resultado =  false;
					//throw new NafinException("DIST0032");
			}
			if(!"".equals(ig_plazo_descuento) && "".equals(fn_porc_descuento)){
					msgError = nafException.getMensajeError("DIST0033", "ES");
					noError.append("DIST0033\n");
					resultado =  false;
					//throw new NafinException("DIST0033");
			}
			if("".equals(ig_plazo_descuento) && !"".equals(fn_porc_descuento)){
				msgError = nafException.getMensajeError("DIST0033", "ES");
				noError.append("DIST0033\n");
				resultado =  false;
					//throw new NafinException("DIST0033");
			}

		// Para Validar la fecha de Emision	Fodea 029-2010 Distribuidores Fase III
		Calendar gcDiaFecha = new GregorianCalendar();
		java.util.Date fechaEmisionNvo = Comunes.parseDate(df_fecha_emision);
		gcDiaFecha.setTime(fechaEmisionNvo);
		no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
		inhabilEpo = getDiasInhabilesDes(ic_epo, df_fecha_emision);

		fechaDeHoy 	= Fecha.getFechaActual();

		log.debug("no_dia_semana   " +no_dia_semana+"   inhabilEpo  "+inhabilEpo);

			SimpleDateFormat formatEntrada = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date df_fecha_emision2 = formatEntrada.parse(df_fecha_emision);
			java.util.Date  fechaDeHoy2 = Comunes.parseDate(fechaDeHoy);

		if ( TipoCredito.equals("D") && ventaCartera.equals("S") ) { // se quita la condici�n de validar Descuento Automatica determinado por Edgar ya que no se usa
			if (  fechaDeHoy2.compareTo(df_fecha_emision2)<0  )  {
					msgError = "Error en la l&iacute;nea "+linea+",La Fecha de Emisi�n debe ser igual o menor al d�a de hoy \n";
					noError.append("6\n");
					resultado =  false;
				}
			}

			if(moneda == 1) {
				mtodocmn = mtodocmn.add(monto);
				totdocmn++;
				if(resultado) itotacepmn++; else itotrechmn++;
			} else if(moneda == 54) {
				mtodocdol = mtodocdol.add(monto);
				totdocdol++;
				if(resultado) itotacepdol++; else itotrechdol++;
			}

				//acuse = new Acuse(1,"4","com",con);
				//creaAcuse(acuse.toString(),ic_usuario,totalDoctoMN,totalDoctoUSD,totalMontoMN,totalMontoUSD,reciboElectronico,con);
	        //ic_documento = getClaveDocto(con);
			  if (resultado){
			  String claveDocto=getClaveDocto(con);

									//resultado

								ps.setString(	1, claveDocto);
								ps.setString(	2, disEnlace.getIc_epo());
								ps.setString(	3, ic_pyme);
								ps.setString(	4, "4");
								ps.setString(	5, acuEnl.getCcAcuse());
								ps.setString(	6, ig_numero_docto);
								ps.setString(	7, df_fecha_emision);
								ps.setString(	8, df_fecha_venc);
								ps.setInt(		9,	Integer.parseInt(ic_moneda));
								ps.setString(	10, referencia);
								ps.setDouble(	11, Double.parseDouble(fn_porc_descuento));
								ps.setInt(		12, Integer.parseInt(ig_plazo_descuento));
								ps.setDouble(	13, Double.parseDouble(fn_monto));
								ps.setString(	14, ic_tipo_financiamiento);
								ps.setString(	15, negociable);
								ps.setString(	16, ic_linea_credito_dm);
								ps.setString(	17, cd1);
								ps.setString(	18, cd2);
								ps.setString(	19, cd3);
								ps.setString(	20, cd4);
								ps.setString(	21, cd5);
								ps.setString(	22, cs_plazo_descuento);
								ps.setString(	23, ic_clasificacion);
								ps.setString(	24, cg_ventacartera);

								ps.executeUpdate();
								ps.clearParameters();
								ps.close();
			  }
			if (resultado == false){
				ErroresEnl err = new ErroresEnl();
				err.setIgNumeroDocto(ig_numero_docto);
				err.setIgNumeroError(0+"");
				err.setCgError(msgError+" Linea: "+linea);

				query = disEnlace.getInsertaErrores(err);

//						query = "insert into "+epoEnl.getTablaErrores()+" (ig_numero_docto, ig_numero_error, cg_error) "+
//								" values('"+st.nextToken()+"',"+st2.nextToken()+",'"+linea+"')";
			            try{
			              con.ejecutaSQL(query);
			            }catch(Exception e){
			              log.error("Error al insertar en "+disEnlace.getTablaErrores()+".\n");
							  e.printStackTrace();
			              log.error("Error al insertar en "+disEnlace.getTablaErrores()+".\n");
			            }
				resultado=true;

				}
			}//FIN WHILE registros
			retorno=totdocmn>0&&totdocdol>0;
			if(totdocmn>0&&totdocdol>0){
				query =
					" UPDATE com_acuse1"   +
					"    SET in_total_docto_mn = "+itotacepmn+","   +
					"        fn_total_monto_mn = "+mtodocmn+","   +
					"        in_total_docto_dl = "+itotacepdol+","   +
					"        fn_total_monto_dl = "+mtodocdol+" "   +
					"  WHERE cc_acuse = '"+acuEnl.getCcAcuse()+"'"  ;


				if( limiteLineaCredito.equals("S")) {
				double monto_comprometidoTotal = 0;
				double saldo_total =0;
				double monto_comprometido =Double.parseDouble(mtodocmn.toString());
						 String saldo= 	validaMontoComprometido( ic_epo);
						 saldo_total =  Double.parseDouble((String)saldo);

						monto_comprometidoTotal = saldo_total + monto_comprometido;

						String qrySentencia2 ="	update DIS_LINEA_CREDITO_DM    "+
															" set FN_SALDO_COMPROMETIDO = "+monto_comprometidoTotal+
															"  WHERE  ic_epo =  "+ic_epo+
															" AND ic_producto_nafin = 4 "+
															" AND df_vencimiento_adicional > SYSDATE  "+
															" AND cg_tipo_solicitud = 'I' "+
															" AND ic_estatus_linea = 12 ";

							log.debug("monto Coprometido::: "+qrySentencia2);

							ps = con.queryPrecompilado(qrySentencia2);
							ps.executeUpdate();
							ps.close();
				 }
			}
		}//FIN if(hayreg > 0)
		//Para Validar la fecha de Emision


	      }catch(NafinException ne){
				ne.printStackTrace();
            msgError = ne.getMsgError();
            throw ne;
        }catch(Exception e){
				e.printStackTrace();
				System.out.println("excepcion "+e);
				msgError = "El layout es incorrecto, por favor verif&iacute;quelo";
				throw new NafinException("SIST0001");
        } finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("CargaDocDistBean::validacionesMasiva (S)");
		}
      return retorno;
    }

*/

    private String getTipoFinDefault(String ic_epo,String ic_tipo_financiamiento,AccesoDB con) throws NafinException {
        log.info("CargaDocDistBean::getTipoFinDefault(E)");
        String		qrySentencia	= "";
        PreparedStatement   ps = null;
        ResultSet	rs		= null;
        String          tipoDef		= "";
        try {
		    if("".equals(ic_tipo_financiamiento)||ic_tipo_financiamiento ==null){
	        	qrySentencia =	
                    " select ic_tipo_financiamiento"   +
                    "  from comrel_epo_tipo_financiamiento "   +
                    "  where ic_producto_nafin = ? "   +
                    "  and ic_epo = ? ";
                log.debug("qrySentencia:"+qrySentencia);
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, Integer.valueOf("4"));
                ps.setInt(2, Integer.valueOf(ic_epo));
                rs = ps.executeQuery();
                ps.clearParameters();
                if(rs.next())
                	tipoDef = rs.getString(1);
                rs.close();ps.close();
                //con.cierraStatement();
            } else {
            	tipoDef = ic_tipo_financiamiento;
            }
            log.trace("EL TIPO DE FINANCIAMIENTO POR DEFAULT ES "+tipoDef);
            if("".equals(tipoDef)||tipoDef ==null)
                throw new NafinException("DIST0007");
        } catch(NafinException ne) {
            log.error(ne.getMsgError());
            throw ne;
        } catch(Exception e) {
            log.error("CargaDocDist.getTipoFinaDefault::Exception "+e);
        } finally {
            try {
                if (rs!=null) rs.close();
                if (ps!=null) ps.close();
            } catch(Exception e) {
                log.error("CargaDocDist::getTipoFinaDefault(Exception)" + e.getMessage(), e);
            }            
        }
        log.info("CargaDocDistBean::getTipoFinDefault(S)");
        return tipoDef;
    }

    private String getPlazoFinDefault(String ic_epo,String ic_tipo_financiamiento,String ig_plazo_dscto,AccesoDB con) throws NafinException{
        log.info("CargaDocDistBean::getPlazoFinDefault (E)");
        String		qrySentencia	= "";
        PreparedStatement   ps = null;
        ResultSet	rs		= null;
        String          plazoDef	= "";
        try {
            if(("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento))&&("".equals(ig_plazo_dscto)||ig_plazo_dscto ==null)) {
                qrySentencia =  
                    " select nvl(pe.ig_plazo_dscto,null)"   +
                    " from comrel_producto_epo pe"   +
                    " where  pe.ic_producto_nafin = ?"   +
                    " and pe.ic_epo = ? ";
                log.debug("qrySentencia:"+qrySentencia);
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, Integer.valueOf("4"));
                ps.setInt(2, Integer.valueOf(ic_epo));
                rs = ps.executeQuery();
                ps.clearParameters();
                if(rs.next()) {
                    plazoDef = rs.getString(1);
                    log.debug("ENTRO EN EL NEXT "+plazoDef);
                }
                rs.close();ps.close();
                //con.cierraStatement();
            } else {
                plazoDef = ig_plazo_dscto;
                log.debug("ENTRO EN EL PRIMER ELSE "+plazoDef);
            }
            if(("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento))&&("".equals(plazoDef)||plazoDef ==null))
                throw new NafinException("DIST0008");
            else if(plazoDef==null||"".equals(plazoDef)) {
                plazoDef = "null";
                log.debug("ENTRO EN EL SEGUNDO ELSE "+plazoDef);
            }
        } catch(NafinException ne) {
            log.error(ne.getMsgError());
            throw ne;
        } catch(Exception e) {
            log.error("CargaDocDist.getTipoFinaDefault::Exception "+e);
        }
        log.info("CargaDocDistBean::getPlazoFinDefault (S)");
        return plazoDef;
    }


    private boolean validaPlazoDescto(
    		String ig_plazo_dscto,
    		String df_fecha_venc,
    		String df_fecha_emision,
    		String cg_fecha_porc_desc,
    		AccesoDB con) throws NafinException{
		log.info("CargaDocDist::validaPlazoDescto(E)");
		Calendar 	cal	 			= new GregorianCalendar();
        boolean resultado = false;
		  String msgError =  "";
        try {
            String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            if("E".equals(cg_fecha_porc_desc))
                fechaHoy = df_fecha_emision;
            cal.set(Integer.parseInt(fechaHoy.substring(6, 10)), Integer.parseInt(fechaHoy.substring(3, 5))-1, Integer.parseInt(fechaHoy.substring(0, 2)));
            cal.add(Calendar.DATE, Integer.parseInt(ig_plazo_dscto));	//incremento de la fecha de publicacion m�s el plazo
            if((cal.getTime()).compareTo(Comunes.parseDate(df_fecha_venc))>0) {
                resultado = false;
                msgError = "El plazo para descuento no puede ser mayor a la Fecha de vencimiento del documento";
                log.debug("msgError "+msgError);
            } else
                resultado = true;
        }catch(Exception e){
             log.error("CargaDocDist.validaPlazoDescto(Exception) "+e);
        }
		log.info("CargaDocDistBean::validaPlazoDescto(S)");
    	return resultado;
    }

    private String getPorcFinDefault(String ic_epo,String ic_tipo_financiamiento,String fn_porc_dscto,AccesoDB con) throws NafinException{
		log.info("CargaDocDistBean::getPorcFinDefault(E)");
    	String		qrySentencia	= "";
        PreparedStatement   ps = null;
        ResultSet	rs		= null;
        String          porcDef	= "";
        //ESL cambi� <=0 por =0 en fn_porc_dscto
        try{
            if("".equals(fn_porc_dscto)){
                //no hacer nada
            } else if(!Comunes.esDecimal(fn_porc_dscto)) {
                fn_porc_dscto = "";
            } else if(Double.parseDouble(fn_porc_dscto)<0||Double.parseDouble(fn_porc_dscto)>100) {
                throw new NafinException("DIST0024");
            }
    
            if(("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento))&&("".equals(fn_porc_dscto)||fn_porc_dscto ==null)){
                qrySentencia =	
                    " select nvl(pe.fn_porcentaje_dscto,null)"   +
                    " from comrel_producto_epo pe"   +
                    " where  pe.ic_producto_nafin = ?"   +
                    " and pe.ic_epo = ? ";
                log.debug("qrySentencia:"+qrySentencia);
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, Integer.valueOf("4"));
                ps.setInt(2, Integer.valueOf(ic_epo));
                rs = ps.executeQuery();
                ps.clearParameters();
                if(rs.next()){
                    porcDef = rs.getString(1);
                        log.trace("ENTRO AL NEXT "+porcDef);
                }
                rs.close();ps.close();
                //con.cierraStatement();
            } else {
                porcDef = fn_porc_dscto;
                log.debug("ENTRO AL PORCDEF" +porcDef);
            }
           if(("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento))&&("".equals(porcDef)||porcDef ==null))
                throw new NafinException("DIST0008");
            else if(porcDef==null||"".equals(porcDef)) {
                porcDef = "null";
                log.debug("ENTRO EN EL ELSE A NULL "+porcDef);
            }
        }catch(NafinException ne){
            log.error(ne.getMsgError());
            throw ne;
        }catch(Exception e){
            log.error("CargaDocDist.getPorcFinDefault(Exception) "+e);
        }
        log.info("CargaDocDistBean::getPorcFinDefault(S)");
        return porcDef;
    }

    public boolean tempMasiva(String df_fecha_venc,String df_fecha_emision,String ic_epo,String ic_proc_docto,String ig_numero_docto,String cg_pyme_epo_interno,String ic_moneda,String fn_monto,String ct_referencia,Vector cg_campo,String ic_tipo_financiamiento,String ig_plazo_dscto,String fn_porc_dscto,String ic_linea_credito_dm,String fecha_lim_dscto,String cg_fecha_porc_desc,String cg_clave, String negociable, String operaVentaCartera, String tipo_Pago, String pediodo)
      throws NafinException {
		log.info("CargaDocDistBean::tempMasiva (E)");
        boolean		resultado	= true;
        AccesoDB	con			= null;
		String		arreglo[]	= new String[2];
        String		ic_pyme		= "";
        String 		validaDist 	= "";
        String 		validaSaldo = "";
        String 		acumulado	= "";
		String 		ic_clasificacion = "NULL";
		String		fin_selec	= ic_tipo_financiamiento;
		String qrySentencia ="";
		ResultSet rs1 = null;
		PreparedStatement	ps1	= null;
		String  cs_plazo_descuento = "";

        try{
            con = new AccesoDB();
            con.conexionDB();
            resultado = existeDocto(ic_epo,ic_proc_docto,ig_numero_docto,con);

	      	arreglo = validaRelPymeEpo(ic_epo,cg_pyme_epo_interno,con);
				ic_pyme = arreglo[0];

            if(resultado) {
              	validaFechas(df_fecha_venc,df_fecha_emision,ic_epo,ic_pyme,con);
            }

				String tipoCredito = this.obtieneTipoCredito(ic_epo);
				if(!tipoCredito.equals("F") ){
					ic_tipo_financiamiento = getTipoFinDefault(ic_epo,ic_tipo_financiamiento,con);
				}

			if(!"".equals(cg_clave) || ("".equals(cg_clave) && "".equals(ig_plazo_dscto) && "".equals(fn_porc_dscto)))
				ic_clasificacion = getCategoria(ic_epo,"4",cg_clave,ic_pyme);

			if("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento)) {
				if(!"".equals(ig_plazo_dscto) || !"".equals(fecha_lim_dscto)){
					ig_plazo_dscto = validaFechaLimite(df_fecha_emision,ig_plazo_dscto,fecha_lim_dscto, cg_fecha_porc_desc, con);
					log.trace("primer metodo ig_plazo_descuento::ig_plazo_dscto == "+ig_plazo_dscto);
				}

				Vector vectoraux = new Vector();
				String aux_ig_plazo_dscto	= "";
				String aux_fn_porc_dscto	= "";
				String aux_tipo_param		= "";

				vectoraux = getParametrosClasific(ic_epo,ic_pyme,df_fecha_emision,cg_fecha_porc_desc,ic_clasificacion,con);

				if("".equals(ic_clasificacion))
					ic_clasificacion = "NULL";

				String rs_plazo_dscto	= (String)vectoraux.get(0);
				String rs_porc_dscto	= (String)vectoraux.get(1);
				String rs_tipo_param	= (String)vectoraux.get(2);
				if("E".equals(cg_fecha_porc_desc) && !"NULL".equals(ic_clasificacion)){
					String rs_plazo_dscto_max	= "";
					if(vectoraux.size()>3)
						rs_plazo_dscto_max = (String)vectoraux.get(3);
					if("".equals(rs_plazo_dscto_max))
						rs_plazo_dscto_max = rs_plazo_dscto;

					int restaFechas = getRestaFechas(df_fecha_emision,rs_plazo_dscto_max);
					if(restaFechas<0) {
						ig_plazo_dscto = "0";
						fn_porc_dscto = "0";
					}
				}

				if("".equals(ig_plazo_dscto)) {
					aux_ig_plazo_dscto = rs_plazo_dscto;
					aux_tipo_param = rs_tipo_param;
				}
				else {
					aux_ig_plazo_dscto = ig_plazo_dscto;
					if(ig_plazo_dscto.equals(rs_plazo_dscto))
						aux_tipo_param = rs_tipo_param;
					else
						aux_tipo_param = "U";
				}

				if("".equals(fn_porc_dscto)) {
					aux_fn_porc_dscto = rs_porc_dscto;
					if(!"U".equals(aux_tipo_param))
						aux_tipo_param = rs_tipo_param;
				}
				else {
					aux_fn_porc_dscto	= fn_porc_dscto;
					if(fn_porc_dscto.equals(rs_porc_dscto)) {
						if(!"U".equals(aux_tipo_param))
							aux_tipo_param = rs_tipo_param;
					}
					else
						aux_tipo_param = "U";
				}

				ig_plazo_dscto = getPlazoFinDefault(ic_epo,ic_tipo_financiamiento,aux_ig_plazo_dscto,con);
				fn_porc_dscto =  getPorcFinDefault(ic_epo,ic_tipo_financiamiento,aux_fn_porc_dscto,con);
				log.trace("primer metodo ig_plazo_descuento::ig_plazo_dscto == "+ig_plazo_dscto);


				if(!aux_ig_plazo_dscto.equals(ig_plazo_dscto))
						cs_plazo_descuento = "N";
				else
					if(!aux_fn_porc_dscto.equals(fn_porc_dscto))
						cs_plazo_descuento = "N";
					else
						if("G".equals(aux_tipo_param) || "U".equals(aux_tipo_param))
							cs_plazo_descuento = "N";
						else
							if("NULL".equals(ic_clasificacion) || "0".equals(ic_clasificacion))
								cs_plazo_descuento = "N";
				/*Validacion de fecha de publicacion*/
				if(resultado)
					resultado = validaPlazoDescto(ig_plazo_dscto,df_fecha_venc,df_fecha_emision,cg_fecha_porc_desc,con);
			}
			else {
              	ig_plazo_dscto = "";
                fn_porc_dscto = "";
                cs_plazo_descuento = "N";
			}
			if(!"D".equals(arreglo[1])){
				ic_linea_credito_dm = "null";
			}
			/*else{
				if(ic_linea_credito_dm!=null&&!"".equals(ic_linea_credito_dm)){
					if(!Comunes.esNumero(ic_linea_credito_dm))
						throw new NafinException("DIST0025");
					validaLineaCredito(ic_epo,ic_moneda,ic_linea_credito_dm,con);
				}else{
					ic_linea_credito_dm = "null";
				}
			}*/

			if("D".equals(arreglo[1]) && ic_linea_credito_dm.equals("")){
				 qrySentencia = 	
					" SELECT ic_linea_credito_dm "+
					" FROM DIS_LINEA_CREDITO_DM "+
					" WHERE  ic_epo = ? "+
					" AND ic_producto_nafin =  ? ";
				log.debug("qrySentencia:"+qrySentencia);
				ps1 = con.queryPrecompilado(qrySentencia);
				ps1.setInt(1,Integer.parseInt(ic_epo));
				ps1.setInt(2,4);
				rs1 = ps1.executeQuery();
				ps1.clearParameters();
				log.trace("ic_epo"+ic_epo);

				if(rs1.next())	{
					ic_linea_credito_dm = rs1.getString("ic_linea_credito_dm")==null?"null":rs1.getString("ic_linea_credito_dm");
				}
				rs1.close();ps1.close();
			}
			if(ic_linea_credito_dm.equals("")){
				ic_linea_credito_dm =null;
			}
			log.debug("ic_linea_credito_dm"+ic_linea_credito_dm);
			log.debug("ic_clasificacion = " + ic_clasificacion);

			if("5".equals(fin_selec)){
	            validaDist = validaDistribuidor(ic_epo,cg_pyme_epo_interno,df_fecha_emision);
				if("S".equals(validaDist))
	              throw new NafinException("DIST0038");
				acumulado = getMontoAcumulado(ic_epo,cg_pyme_epo_interno,ic_moneda,ic_proc_docto);
				validaSaldo = validaSaldoDisponible(ic_epo,cg_pyme_epo_interno,ic_moneda,fn_monto,acumulado);
				if("N".equals(validaSaldo))
	              throw new NafinException("DIST0039");
				if("NL".equals(validaSaldo))
					throw new NafinException("DIST0040");
        	}
            insertaTemporales(ic_proc_docto,ic_pyme,ig_numero_docto,df_fecha_emision,df_fecha_venc,ic_moneda,fn_monto,ct_referencia,cg_campo,ic_epo,ic_tipo_financiamiento,ig_plazo_dscto,fn_porc_dscto,ic_linea_credito_dm,cs_plazo_descuento,ic_clasificacion,con, negociable, operaVentaCartera,false, "",  tipo_Pago, pediodo);
				cs_plazo_descuento = "S";
        }catch(NafinException ne){
            resultado = false;
            log.error("ENTRANDO A NAFINEXCEPTION"+ ne.getMsgError());
            throw ne;
        }catch(Exception e){
          log.error("CargaDocDistBean::tempMasiva Exception "+e);
			 e.printStackTrace();
          resultado = false;
          throw new NafinException("SIST0001");
        }finally{
          con.terminaTransaccion(resultado);
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
          log.info("CargaDocDistBean::tempMasiva (S)");
        }
       return resultado;
      }


		/*
		public boolean tempMasivaWS(String receipt, String df_fecha_venc,String df_fecha_emision,String ic_epo,String ic_proc_docto,String ig_numero_docto,String cg_pyme_epo_interno,String ic_moneda,String fn_monto,String ct_referencia,Vector cg_campo,String ic_tipo_financiamiento,String ig_plazo_dscto,String fn_porc_dscto,String ic_linea_credito_dm,String fecha_lim_dscto,String cg_fecha_porc_desc,String cg_clave, String negociable, String operaVentaCartera,AccesoDB	con)
      throws NafinException{
		System.out.println("CargaDocDistBean::tempMasiva (E)");
        boolean		resultado	= true;

		  boolean isWS = true;
		String		arreglo[]	= new String[2];
		String		ic_pyme		= "";
		String 		validaDist 	= "";
		String 		validaSaldo = "";
		String 		acumulado	= "";
		String 		fechaActual	=(new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
		String 		ic_clasificacion = "NULL";
		String		fin_selec	= ic_tipo_financiamiento;
		String qrySentencia ="";
		ResultSet rs1 = null;
		PreparedStatement	ps1	= null;
		String  cs_plazo_descuento = "";

        try{

            con.conexionDB();
            resultado = existeDocto(ic_epo,ic_proc_docto,ig_numero_docto,con);

	      	arreglo = validaRelPymeEpo(ic_epo,cg_pyme_epo_interno,con);
				ic_pyme = arreglo[0];

            if(resultado) {
              	validaFechas(df_fecha_venc,df_fecha_emision,ic_epo,ic_pyme,con);
            }

				String tipoCredito = this.obtieneTipoCredito(ic_epo);
				if(!tipoCredito.equals("F") ){
					ic_tipo_financiamiento = getTipoFinDefault(ic_epo,ic_tipo_financiamiento,con);
				}

			if(!"".equals(cg_clave) || ("".equals(cg_clave) && "".equals(ig_plazo_dscto) && "".equals(fn_porc_dscto)))
				ic_clasificacion = getCategoria(ic_epo,"4",cg_clave,ic_pyme);

			if("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento)) {
				if(!"".equals(ig_plazo_dscto) || !"".equals(fecha_lim_dscto))
					ig_plazo_dscto = validaFechaLimite(df_fecha_emision,ig_plazo_dscto,fecha_lim_dscto, cg_fecha_porc_desc, con);

				Vector vectoraux = new Vector();
				String aux_ig_plazo_dscto	= "";
				String aux_fn_porc_dscto	= "";
				String aux_tipo_param		= "";

				vectoraux = getParametrosClasific(ic_epo,ic_pyme,df_fecha_emision,cg_fecha_porc_desc,ic_clasificacion,con);

				if("".equals(ic_clasificacion))
					ic_clasificacion = "NULL";

				String rs_plazo_dscto	= (String)vectoraux.get(0);
				String rs_porc_dscto	= (String)vectoraux.get(1);
				String rs_tipo_param	= (String)vectoraux.get(2);
				if("E".equals(cg_fecha_porc_desc) && !"NULL".equals(ic_clasificacion)){
					String rs_plazo_dscto_max	= "";
					if(vectoraux.size()>3)
						rs_plazo_dscto_max = (String)vectoraux.get(3);
					if("".equals(rs_plazo_dscto_max))
						rs_plazo_dscto_max = rs_plazo_dscto;

					int restaFechas = getRestaFechas(df_fecha_emision,rs_plazo_dscto_max);
					if(restaFechas<0) {
						ig_plazo_dscto = "0";
						fn_porc_dscto = "0";
					}
				}

				if("".equals(ig_plazo_dscto)) {
					aux_ig_plazo_dscto = rs_plazo_dscto;
					aux_tipo_param = rs_tipo_param;
				}
				else {
					aux_ig_plazo_dscto = ig_plazo_dscto;
					if(ig_plazo_dscto.equals(rs_plazo_dscto))
						aux_tipo_param = rs_tipo_param;
					else
						aux_tipo_param = "U";
				}

				if("".equals(fn_porc_dscto)) {
					aux_fn_porc_dscto = rs_porc_dscto;
					if(!"U".equals(aux_tipo_param))
						aux_tipo_param = rs_tipo_param;
				}
				else {
					aux_fn_porc_dscto	= fn_porc_dscto;
					if(fn_porc_dscto.equals(rs_porc_dscto)) {
						if(!"U".equals(aux_tipo_param))
							aux_tipo_param = rs_tipo_param;
					}
					else
						aux_tipo_param = "U";
				}

				ig_plazo_dscto = getPlazoFinDefault(ic_epo,ic_tipo_financiamiento,aux_ig_plazo_dscto,con);
				fn_porc_dscto =  getPorcFinDefault(ic_epo,ic_tipo_financiamiento,aux_fn_porc_dscto,con);

				if(!aux_ig_plazo_dscto.equals(ig_plazo_dscto))
						cs_plazo_descuento = "N";
				else
					if(!aux_fn_porc_dscto.equals(fn_porc_dscto))
						cs_plazo_descuento = "N";
					else
						if("G".equals(aux_tipo_param) || "U".equals(aux_tipo_param))
							cs_plazo_descuento = "N";
						else
							if("NULL".equals(ic_clasificacion) || "0".equals(ic_clasificacion))
								cs_plazo_descuento = "N";
				//Validacion de fecha de publicacion
				if(resultado)
					resultado = validaPlazoDescto(ig_plazo_dscto,df_fecha_venc,df_fecha_emision,cg_fecha_porc_desc,con);
			}
			else {
              	ig_plazo_dscto = "";
                fn_porc_dscto = "";
                cs_plazo_descuento = "N";
			}
			if(!"D".equals(arreglo[1])){
				ic_linea_credito_dm = "null";
			}
			//else{
			//	if(ic_linea_credito_dm!=null&&!"".equals(ic_linea_credito_dm)){
			//		if(!Comunes.esNumero(ic_linea_credito_dm))
			//			throw new NafinException("DIST0025");
			//		validaLineaCredito(ic_epo,ic_moneda,ic_linea_credito_dm,con);
			//	}else{
			//		ic_linea_credito_dm = "null";
			//	}
			//}

			if("D".equals(arreglo[1]) && ic_linea_credito_dm.equals("")){
				 qrySentencia = 	" SELECT ic_linea_credito_dm "+
												" FROM DIS_LINEA_CREDITO_DM "+
												" WHERE  ic_epo = ? "+
												" AND ic_producto_nafin =  ? ";

				ps1 = con.queryPrecompilado(qrySentencia);
				ps1.setInt(1,Integer.parseInt(ic_epo));
				ps1.setInt(2,4);
				rs1 = ps1.executeQuery();
				ps1.clearParameters();
				log.debug("qrySentencia----> "+qrySentencia);
				log.debug("ic_epo"+ic_epo);

				if(rs1.next())	{
					ic_linea_credito_dm = rs1.getString("ic_linea_credito_dm")==null?"null":rs1.getString("ic_linea_credito_dm");
				}
				rs1.close();
				ps1.close();
			}
			if(ic_linea_credito_dm.equals("")){
				ic_linea_credito_dm =null;
			}
			log.debug("ic_linea_credito_dm"+ic_linea_credito_dm);
			System.out.println("ic_clasificacion = " + ic_clasificacion);

			if("5".equals(fin_selec)){
	            validaDist = validaDistribuidor(ic_epo,cg_pyme_epo_interno,df_fecha_emision);
				if("S".equals(validaDist))
	              throw new NafinException("DIST0038");
				acumulado = getMontoAcumulado(ic_epo,cg_pyme_epo_interno,ic_moneda,ic_proc_docto);
				validaSaldo = validaSaldoDisponible(ic_epo,cg_pyme_epo_interno,ic_moneda,fn_monto,acumulado);
				if("N".equals(validaSaldo))
	              throw new NafinException("DIST0039");
				if("NL".equals(validaSaldo))
					throw new NafinException("DIST0040");
        	}
            insertaTemporales(ic_proc_docto,ic_pyme,ig_numero_docto,df_fecha_emision,df_fecha_venc,ic_moneda,fn_monto,ct_referencia,cg_campo,ic_epo,ic_tipo_financiamiento,ig_plazo_dscto,fn_porc_dscto,ic_linea_credito_dm,cs_plazo_descuento,ic_clasificacion,con, negociable, operaVentaCartera,isWS,receipt);
				cs_plazo_descuento = "S";
        }catch(NafinException ne){
            resultado = false;
            System.out.println("ENTRANDO A NAFINEXCEPTION");
            msgError = ne.getMsgError();
            throw ne;
        }catch(Exception e){
          System.out.println("CargaDoctoBean::tempMasiva Exception "+e);
          resultado = false;
          throw new NafinException("SIST0001");
        }finally{

			System.out.println("CargaDocDistBean::tempMasiva (S)");
        }
       return resultado;
      }

	*/
	private void validaFechas(String df_fecha_venc, String df_fecha_emision, String ic_epo, String ic_pyme, AccesoDB con)
			throws NafinException {
		log.info("CargaDocDistBean::validaFechas(E)");
		String    qrySentencia  = "";
		ResultSet rs            = null;
	    PreparedStatement ps    = null;
		int       fmin          = 0;
		int       fmax          = 0;
		int       fdmin         = 0;
		int       fdmax         = 0;
		String    fechaMinima   = "";
		String    fechaMaxima   = "";
		int       difFechas     = 0;
		java.util.Date FechavMin    = null;
		java.util.Date FechavMax    = null;
		java.util.Date fechaEmision  = null;
		java.util.Date fechaVenc = null;
		java.util.Date FechaHoy     = new java.util.Date();
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		Vector	diames_inhabil = new Vector();
		String 	dm_inhabil = "";
		try {
			/****************************************************/
			qrySentencia =
				" SELECT cg_dia_inhabil"   +
				"   FROM comcat_dia_inhabil"   +
				"	where cg_dia_inhabil is not null "+
				" UNION"   +
				" SELECT cg_dia_inhabil"   +
				"   FROM comrel_dia_inhabil_x_epo"   +
				"  WHERE ic_epo = ? "  +
				"	and cg_dia_inhabil is not null ";
            log.debug("qrySentencia:"+qrySentencia);
		    ps = con.queryPrecompilado(qrySentencia);
		    ps.setInt(1, Integer.valueOf(ic_epo));
		    rs = ps.executeQuery();
		    ps.clearParameters();
			while (rs.next())
				diames_inhabil.addElement(rs.getString(1).trim());
            rs.close();ps.close();
			//con.cierraStatement();
            
			for(int fv=0; fv<diames_inhabil.size(); fv++) {
				dm_inhabil=diames_inhabil.get(fv).toString();
			    	if(df_fecha_venc.substring(0,5).equals(dm_inhabil)) {
					throw new NafinException("DIST0010");
				}
			    	/*if(df_fecha_emision.substring(0,5).equals(dm_inhabil)) {
					throw new NafinException("DIST0013");
				} */
			}

			qrySentencia =
				" SELECT   TO_DATE (?, 'DD/MM/YYYY')"   +
				"        - (TO_DATE (?, 'DD/MM/YYYY') + NVL (ig_dias_minimo, ?)),"   +
				"          TO_DATE (?, 'DD/MM/YYYY')"   +
				"        - (TO_DATE (?, 'DD/MM/YYYY') + NVL (ig_dias_maximo, ?)),"   +
				"        ig_dias_minimo, ig_dias_maximo,"   +
				"        TO_CHAR ((TO_DATE (?, 'DD/MM/YYYY') + NVL (ig_dias_minimo, ?)), 'dd/mm/yyyy'),"   +
				"        TO_CHAR ((TO_DATE (?, 'DD/MM/YYYY') + NVL (ig_dias_maximo, ?)), 'dd/mm/yyyy'),"   +
				"        TO_DATE (?, 'DD/MM/YYYY') - (TO_DATE (?, 'DD/MM/YYYY'))"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_epo = ? "   +
				"    AND ic_producto_nafin = ? "   +
				"    AND cs_habilitado = ? "  ;

/*				" SELECT TO_DATE ('"+df_fecha_venc+"', 'DD/MM/YYYY') -"   +
				"            (TO_DATE ('"+df_fecha_emision+"', 'DD/MM/YYYY') + NVL (cpe.ig_dias_minimo, 0)),"   +
				"        TO_DATE ('"+df_fecha_venc+"', 'DD/MM/YYYY') -"   +
				"            (TO_DATE ('"+df_fecha_emision+"', 'DD/MM/YYYY') + NVL (pep.ig_dias_maximo, NVL (cpe.ig_dias_maximo, 0))),"   +
				"        cpe.ig_dias_minimo,"   +
				"        NVL (pep.ig_dias_maximo, cpe.ig_dias_maximo) ig_dias_maximo,"   +
				"        TO_CHAR ((TO_DATE ('"+df_fecha_emision+"', 'DD/MM/YYYY') + NVL (cpe.ig_dias_minimo, 0)), 'dd/mm/yyyy'),"   +
				"        TO_CHAR ((TO_DATE ('"+df_fecha_emision+"', 'DD/MM/YYYY') + "   +
				"            NVL (pep.ig_dias_maximo, NVL (cpe.ig_dias_maximo, 0))),'dd/mm/yyyy'),"   +
				"        TO_DATE ('"+df_fecha_venc+"', 'DD/MM/YYYY') - (TO_DATE ('"+df_fecha_emision+"', 'DD/MM/YYYY'))"   +
				"   FROM comrel_producto_epo cpe, comrel_pyme_epo_x_producto pep"   +
				"  WHERE cpe.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND cpe.ic_epo = pep.ic_epo"   +
				"    AND cpe.ic_producto_nafin = 4"   +
				"    AND cpe.cs_habilitado = 'S'"   +
				"    AND pep.ic_pyme = "+ic_pyme+" "   +
				"    AND cpe.ic_epo = "+ic_epo+" "  ;*/

		    log.debug("qrySentencia:"+qrySentencia);
		    ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, df_fecha_venc);
		    ps.setString(2, df_fecha_emision);
		    ps.setInt(3, Integer.valueOf("0"));
		    ps.setString(4, df_fecha_venc);
		    ps.setString(5, df_fecha_emision);
		    ps.setInt(6, Integer.valueOf("0"));
		    ps.setString(7, df_fecha_emision);
		    ps.setInt(8, Integer.valueOf("0"));
		    ps.setString(9, df_fecha_emision);
		    ps.setInt(10, Integer.valueOf("0"));
		    ps.setString(11, df_fecha_venc);
		    ps.setString(12, df_fecha_emision);
		    ps.setInt(13, Integer.valueOf(ic_epo));
		    ps.setInt(14, Integer.valueOf("4"));
		    ps.setString(15, "S");
		    rs = ps.executeQuery();
		    ps.clearParameters();
			while (rs.next()) {
				fmin 		= rs.getInt(1);
				fmax 		= rs.getInt(2);
				fdmin 		= rs.getInt(3);
				fdmax 		= rs.getInt(4);
				fechaMinima = rs.getString(5);
				fechaMaxima = rs.getString(6);
				difFechas  	= rs.getInt(7);
			}
            rs.close();ps.close();
			//con.cierraStatement();
            
			if ((fdmax==0) || (fdmin==0)) {
				qrySentencia =
					" SELECT TO_DATE (?, 'DD/MM/YYYY') - (TO_DATE (?, 'DD/MM/YYYY') + NVL (in_dias_minimo, ?)),"   +
					"        TO_DATE (?, 'DD/MM/YYYY') - (TO_DATE (?, 'DD/MM/YYYY') + NVL (in_dias_maximo, ?)),"   +
					"        in_dias_minimo, in_dias_maximo,"   +
					"        TO_CHAR (TO_DATE (?, 'DD/MM/YYYY') + NVL (in_dias_minimo, ?), 'DD/MM/YYYY'),"   +
					"        TO_CHAR (TO_DATE (?, 'DD/MM/YYYY') + NVL (in_dias_maximo, ?), 'DD/MM/YYYY')"   +
					"   FROM comcat_producto_nafin"   +
					"  WHERE ic_producto_nafin = ?"  ;
			    log.debug("qrySentencia:"+qrySentencia);
			    ps = con.queryPrecompilado(qrySentencia);
			    ps.setString(1, df_fecha_venc);
			    ps.setString(2, df_fecha_emision);
			    ps.setInt(3, Integer.valueOf("0"));
			    ps.setString(4, df_fecha_venc);
			    ps.setString(5, df_fecha_emision);                
			    ps.setInt(6, Integer.valueOf("0"));
			    ps.setString(7, df_fecha_venc);
			    ps.setInt(8, Integer.valueOf("0"));
			    ps.setString(9, df_fecha_venc);
			    ps.setInt(10, Integer.valueOf("0"));
			    ps.setInt(10, Integer.valueOf("4"));
			    rs = ps.executeQuery();
			    ps.clearParameters();
				while (rs.next()) {
					if (fdmin == 0) {
						fmin 		= rs.getInt(1);
						fdmin 		= rs.getInt(3);
						fechaMinima = rs.getString(5);
					}
					if (fdmax == 0) {
						fmax 		= rs.getInt(2);
						fdmax 		= rs.getInt(4);
						fechaMaxima = rs.getString(6);
					}
				}
			    rs.close();ps.close();
				//con.cierraStatement();
			}
			FechavMin 		= Comunes.parseDate(fechaMinima);
			FechavMax 		= Comunes.parseDate(fechaMaxima);
			fechaEmision 	= Comunes.parseDate(df_fecha_emision);
			fechaVenc 		= Comunes.parseDate(df_fecha_venc);
			Calendar cal = Calendar.getInstance();
			cal.setTime(fechaVenc);
			int diaDeLaSemana = cal.get(Calendar.DAY_OF_WEEK);
			if(diaDeLaSemana==Calendar.SATURDAY||diaDeLaSemana==Calendar.SUNDAY) //domingo o sabado
				throw new NafinException("DIST0010");
			if(fechaEmision.compareTo(fechaVenc)>0)
				throw new NafinException("DIST0004");

	       	//VALIDA QUE LA FECHA DE EMISION NO SEA MAYOR A HOY Y QUE LA FECHA DE VENCIMIENTO NO SEA MENOR A HOY
					//Se deshabilito para el Fodea 029-2010 Distribuidores Fase III
	       /* if(fechaEmision.compareTo(FechaHoy)>0)
				if(!sdf.format(fechaEmision).equals(sdf.format(FechaHoy)))
					throw new NafinException("DIST0016");*/
			if(FechaHoy.compareTo(fechaVenc)>0)
				if(!sdf.format(fechaEmision).equals(sdf.format(FechaHoy)))
					throw new NafinException("DIST0017");
			//ic_estatus_docto = "2";
			//habilitaPyme = true;
			if (fmin < 0){
				throw new NafinException("DIST0005");
			}
			if (fmax >= 0){
				throw new NafinException("DIST0006");
			}
		} catch(NafinException ne) {
		    log.error("NafinException "+ne.getMessage());
			//msgError = ne.getMsgError();
			throw ne;
		} catch(Exception e) {
			log.error("CargaDocDistBean::validaFechas(Exception) "+e);
			throw new NafinException("SIST0001");
		}
		log.info("CargaDocDistBean::validaFechas(S)");
    }

    private String getClaveDocto(AccesoDB con) throws NafinException {
        log.info("CargaDocDistBean::getClaveDocto(E)");
        String maxClaveDocto = "";
        String qrySentencia = 
            "select NVL(max(ic_documento),?)+? as maxClaveDocto"+
            " from dis_documento";
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.valueOf("0"));
            ps.setInt(2, Integer.valueOf("1"));
            rs = ps.executeQuery();
            ps.clearParameters();
            if(rs.next())
                maxClaveDocto = rs.getString("MAXCLAVEDOCTO");
            ps.close();rs.close();
            //con.cierraStatement();
        } catch(Exception e) {
            log.error("CargaDocDistBean::getClaveDocto(Exception) "+e);
            throw new NafinException("SIST0001");
        }
        log.info("CargaDocDistBean::getClaveDocto(S)");
        return maxClaveDocto;
    }

    private String getClavePago(AccesoDB con) throws NafinException
      {
		System.out.println("CargaDocDistBean::getClavePago (E)");
      	String maxClavePago = "";
		String qrySentencia="select NVL(max(ic_pago),0)+1 as maxClavePago "+
							"from dis_pagos_ant";
	    ResultSet rs = null;
	    try{
			rs = con.queryDB(qrySentencia);
			if(rs.next())
			maxClavePago = rs.getString("MAXCLAVEPAGO");
        	System.out.println("Clave de Pago: "+maxClavePago);
          	con.cierraStatement();
        }catch(Exception e){
          throw new NafinException("SIST0001");
        }
		System.out.println("CargaDocDistBean::getClavePago (S)");
        return maxClavePago;
      }

    private String insertaDoctos(String ic_epo, String cve_acuse,String strAforo,String icProcDocto,AccesoDB con)
      throws NafinException
      {
	System.out.println("CargaDocDistBean::insertaDoctos (E)");
	String qrySentencia = "";
      String maxClaveDocto = getClaveDocto(con);
      try
        {
       	 if(existeDocto(ic_epo,icProcDocto,con)){
	        qrySentencia = "insert into dis_documento (ic_documento, ic_pyme, ic_epo, ic_producto_nafin "+
							" , cc_acuse, ig_numero_docto, df_fecha_emision"+
							" , df_fecha_venc, ic_moneda, fn_monto"+
							" , ic_estatus_docto, ct_referencia,ig_plazo_docto"+
							" , cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5,df_alta,CS_PLAZO_DESCUENTO)"+
          	  " select "+maxClaveDocto+"+ROWNUM,ic_pyme,"+ic_epo+",2"+
	            " ,'"+cve_acuse+"',ig_numero_docto"+
  	 					" , df_fecha_emision"+
   						" , df_fecha_venc"+
   						" , ic_moneda,fn_monto,ic_estatus_docto"+
			       	        	" , ct_referencia,ROUND(df_fecha_venc-sysdate,0)"+
   						" , cg_campo1, cg_campo2, cg_campo3"+
   						" , cg_campo4, cg_campo5, sysdate,CS_PLAZO_DESCUENTO "+
	   					" from distmp_proc_docto"+
  	 					" where ic_proc_docto="+icProcDocto+
    	        " and ic_epo="+ic_epo;
                System.out.println(qrySentencia);
      		con.ejecutaSQL(qrySentencia);
        	}
        }catch(NafinException ne){
          throw ne;
        }catch(Exception e){
          throw new NafinException("SIST0001");
        }
		System.out.println("CargaDocDistBean::insertaDoctos (S)");
      return maxClaveDocto;
      }

  //ELIMINA LOS DATOS DE LAS TABLAS TEMPORALES UNA VEZ QUE SE HAYAN INSERTADO
    //  EN LAS TABLAS PERMANENTES O QUE SE HAYA CANCELADO EL PROCESO
    private void borraTemporales(String icProcDocto,String icConsecutivo,String ic_epo, AccesoDB con)
      throws NafinException
      {
		System.out.println("CargaDocDistBean::borraTemporales (E)");
      String qrySentencia = "";
      try{
	qrySentencia  = "delete from distmp_proc_docto"+
	              			  " where ic_proc_docto="+icProcDocto+
                        " and ic_epo="+ic_epo;
        if(!("".equals(icConsecutivo)||icConsecutivo==null))
          qrySentencia += " and ic_consecutivo = "+icConsecutivo;
  			con.ejecutaSQL(qrySentencia);

     }catch(Exception e){
          throw new NafinException("SIST0001");
     }
	System.out.println("CargaDocDistBean::borraTemporales (S)");
 }

    public String insertaCargaIndividual(
        String cve_acuse,
        String numDoctosMN,
        String montoTotalMN,
        String numDoctosDL,
        String montoTotalDL,
        String ic_usuario,
        String seg_acuse,
        String ic_epo,
        String strAforo,
        String icProcDocto) throws NafinException
      {
		System.out.println("CargaDocDistBean::insertaCargaIndividual (E)");
        boolean resultado = true;
        AccesoDB con = new AccesoDB();
        String maxClaveDocto = "";
        try{
          con.conexionDB();
          insertaAcuse(cve_acuse,numDoctosMN,montoTotalMN,numDoctosDL,montoTotalDL,ic_usuario,seg_acuse,con);
         maxClaveDocto = insertaDoctos(ic_epo,cve_acuse,strAforo,icProcDocto,con);
//         insertaDoctoDetalle(icProcDocto,maxClaveDocto,ic_epo,con);
//         borraTemporales(icProcDocto,con);
        }catch(NafinException ne){
          throw ne;
        }catch(Exception e){
          System.out.println("CargaDoctoBean::conexionDB Exception "+e);
          resultado = false;
          throw new NafinException("SIST0001");
        }finally
        {
        	try	{
						con.terminaTransaccion(resultado);
             if(con.hayConexionAbierta())
			          con.cierraConexionDB();
        	}catch(Exception e){
          	throw new NafinException("SIST0001");
          }
		System.out.println("CargaDocDistBean::insertaCargaIndividual (S)");
        }
      return maxClaveDocto;
      }

    //Abre una conexion y elimina los datos de las tablas temporales
    //  sin insertar en las tablas permanentes
    // cuando se cancela un proceso,
    public void cancelaCarga(String icProcDocto,String icConsecutivo,String ic_epo) throws NafinException
      {
		System.out.println("CargaDocDistBean::cancelaCarga (E)");
        boolean   resultado = true;
        AccesoDB  con = null;
        try{
            con = new AccesoDB();
            con.conexionDB();
            borraTemporales(icProcDocto,icConsecutivo,ic_epo,con);
        }catch(NafinException ne){
          throw ne;
        }catch(Exception e){
          System.out.println("CargaDoctoBean::cancelaCarga Exception "+e);
          resultado = false;
        }finally{
        con.terminaTransaccion(resultado);
        if(con.hayConexionAbierta())
          con.cierraConexionDB();
		System.out.println("CargaDocDistBean::cancelaCarga (S)");
        }
      }

    //DEVUELVE UN VECTOR PARA MOSTRAR LOS CAMPOS ADICIONALES
    // DE UNA EPO
    public Vector getCamposAdicionales(String ic_epo,boolean esDetalle) throws NafinException {
		log.info("CargaDocDistBean::getCamposAdicionales(E)");
        boolean   resultado     = true;
        AccesoDB  con           = new AccesoDB();
        ResultSet rs            = null;
        PreparedStatement ps    = null;
        String    qrySentencia  = "";
        Vector    vecFilas      = new Vector();
        Vector    vecColumnas   = new Vector();
        String    strDetalle    = "";

        if(esDetalle)
          strDetalle = "_detalle";
        try{
            con.conexionDB();
            qrySentencia = 
                "select initcap(cg_nombre_campo) as CG_NOMBRE_CAMPO,ic_no_campo "+
                " , cg_tipo_dato, ig_longitud"+
                " from comrel_visor"+strDetalle+
                "	where ic_epo = ? "+
                " and ic_producto_nafin=?"+
                "	order by ic_no_campo";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.valueOf(ic_epo));
            ps.setInt(2, Integer.valueOf(4));
            rs = ps.executeQuery();
            ps.clearParameters();
            while(rs.next()) {
              vecFilas = new Vector();
              vecFilas.addElement(rs.getString("CG_NOMBRE_CAMPO"));    // 0
              vecFilas.addElement(rs.getString("IC_NO_CAMPO"));        // 1
              vecFilas.addElement(rs.getString("CG_TIPO_DATO"));       // 2
              vecFilas.addElement(rs.getString("IG_LONGITUD"));        // 3
              vecColumnas.addElement(vecFilas);
            }
            rs.close();ps.close();
            //con.cierraStatement();
        } catch(Exception e) {
          log.error("CargaDocDistBean::getCamposAdicionales(Exception) "+e);
          resultado = false;
          throw new NafinException("SIST0001");
        } finally {
            try {
                if (rs!=null) rs.close();
                if (ps!=null) ps.close();
            } catch(Exception e) {
                log.error("CargaDocDistBean::getCamposAdicionales(Exception)" + e.getMessage(), e);
            }
            if(con.hayConexionAbierta())
                con.cierraConexionDB();
            log.info("CargaDocDistBean::getCamposAdicionales(S)");
        }
        return vecColumnas;
      }

    public int    getNumCamposAdicionales(String ic_epo,boolean esDetalle) throws NafinException
      {
	  System.out.println("CargaDocDistBean::getNumCamposAdicionales (E)");
      boolean   ejecutaQuery  = false;
      String    strDetalle    = esDetalle?"_detalle":"";
      ResultSet rs = null;
      AccesoDB con = null;
      int       retorno = 0;

		Vector camposA=  getCamposAdicionales(ic_epo,esDetalle);

      if(esDetalle){
			retorno = camposA.size();
      }else{
        retorno = camposA.size();
		}
      if(retorno == 0)
        ejecutaQuery = true;
      if(ejecutaQuery)
        {
        try{
          con = new AccesoDB();
          con.conexionDB();
          String qrySentencia =  "select count(*) as numCampos"+
              		" from comrel_visor"+strDetalle+
              		"	where ic_epo = "+ic_epo+
              		" and ic_producto_nafin=4";
          rs = con.queryDB(qrySentencia);
            if(rs.next())
              retorno = rs.getInt("numCampos");
        }catch(Exception e){
          System.out.println("CargaDocDistBean::getNumCamposAdicionales Exception" +e);
          throw new NafinException("SIST0001");
        }finally{
          if(con.hayConexionAbierta())
            con.cierraConexionDB();
         }
		System.out.println("CargaDocDistBean::getNumCamposAdicionales (S)");
        }//fin if ejecuta query
      return retorno;
      }


    private int    getNumCamposAdicionales(String ic_epo,boolean esDetalle,AccesoDB con)
    throws NafinException
      {
		System.out.println("CargaDocDistBean::getNumCamposAdicionales (E)");
      boolean   ejecutaQuery  = false;
      String    strDetalle    = esDetalle?"_detalle":"";
      ResultSet rs = null;
      int       retorno = 0;

		Vector camposA=  getCamposAdicionales(ic_epo,esDetalle);

      if(esDetalle){
        retorno = camposA.size();
      }else{
        retorno = camposA.size();
		}
      if(retorno == 0)
        ejecutaQuery = true;
      if(ejecutaQuery)
        {
        try{
          String qrySentencia =  "select count(*) as numCampos"+
              		" from comrel_visor"+strDetalle+
              		"	where ic_epo = "+ic_epo+
              		" and ic_producto_nafin=4";
          rs = con.queryDB(qrySentencia);
            if(rs.next())
              retorno = rs.getInt("numCampos");

        }catch(Exception e){
          System.out.println("CargaDocDistBean::getNumCamposAdicionales Exception" +e);
          throw new NafinException("SIST0001");
         }
        }//fin if ejecuta query
	System.out.println("CargaDocDistBean::getNumCamposAdicionales (S)");
      return retorno;
      }


    //DEVUELVE UN VECTOR PARA MOSTRAR LA CONSULTA DE LOS DATOS INSERTADOS
    // EN LAS TABLAS PERMANENTES
    public Vector getDoctoInsertado(String ccAcuse,String tipoCredito)
      throws NafinException
      {
		System.out.println("CargaDocDistBean::getDoctoInsertado (E)");
        boolean   resultado        = true;
        AccesoDB  con              = new AccesoDB();
        ResultSet rs               = null;
        String    qrySentencia     = "";
        Vector    vecRenglones     = new Vector();
        Vector    vecColumnas      = new Vector();
        String 	  condicion 	   = "";
      try{
        con.conexionDB();

			if(!tipoCredito.equals("F")){
				if(!"".equals(tipoCredito)&&tipoCredito!=null)
								condicion += " and pp.cg_tipo_credito = '"+tipoCredito+"'";

				qrySentencia=	"select pd.ig_numero_docto"+
						"  , TO_CHAR(pd.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
						"  , TO_CHAR(pd.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
						"  , pd.fn_monto, pd.ct_referencia, py.cg_razon_social"   +
						"  , m.ic_moneda, m.cd_nombre,ROWNUM AS ic_consecutivo,py.ic_pyme,pe.cg_num_distribuidor"   +
						"  , pd.cg_campo1, pd.cg_campo2, pd.cg_campo3, pd.cg_campo4"   +
						"  , pd.cg_campo5"   +
						"  , pp.cg_tipo_credito"   +
						"  , pd.fn_porc_descuento"   +
						"  , pd.ig_plazo_descuento"   +
						"  , decode(nvl(cpe.cg_tipo_conversion,pn.cg_tipo_conversion),'P','Dolar-Peso','Sin conversion') as tipo_conversion"   +
						"  , to_number(decode(nvl(cpe.cg_tipo_conversion,pn.cg_tipo_conversion),'P',tc.fn_valor_compra,'')) as tipo_cambio"   +
						"  , decode(nvl(cpe.cg_responsable_interes,pn.cg_responsable_interes),'E','EPO','Distribuidor') as responsable_interes"   +
						"  , decode(nvl(cpe.cg_tipo_cobranza,pn.cg_tipo_cobranza),'D','Delegada','No Delegada') as tipo_cobranza"   +
						"  , pd.ic_linea_credito_dm"   +
						"  , pd.ig_plazo_credito"   +
						"  , to_char(nvl(pd.df_fecha_venc_credito,sysdate),'dd/mm/yyyy') as df_fecha_venc_credito"   +
						"  , to_char(ca.df_fechahora_carga,'dd/mm/yyyy') as fecha_carga"   +
						"  , to_char(ca.df_fechahora_carga,'hh:mi') as hora_carga"   +
						"  , ca.ic_usuario as usuario"   +
						"  ,vlc.cg_razon_social as nombre_if"   +
						"  ,pd.ig_plazo_docto"   +
						"  ,tf.cd_descripcion as MODO_PLAZO"   +
						"  ,vlc.ic_moneda as MONEDA_LINEA,clas.cg_descripcion  as CATEGORIA"+
				 ", pd.ic_estatus_docto as estatus "+
						" ,pd.CG_VENTACARTERA as CG_VENTACARTERA "+
						" ,e.CD_DESCRIPCION as DESESTATUS  "+ //F032-2014
						" ,pd.ic_epo as IC_EPO  "+ //F032-2014
						", DECODE (pd.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ' ) AS TIPOPAGO "+ //F09-2015
						                  
						"  from dis_documento pd, comcat_pyme py"   +
						"  ,comcat_moneda m,comrel_pyme_epo pe"   +
						"  ,comrel_pyme_epo_x_producto pp"   +
						"  ,comrel_producto_epo cpe,comcat_producto_nafin pn"   +
						"  , com_tipo_cambio tc"   +
						"  ,comcat_tipo_financiamiento tf"   +
						"  ,com_acuse1 ca"   +
						"  ,comrel_clasificacion clas "+
						"  ,(select lc.ic_moneda,i.cg_razon_social,lc.ic_linea_credito_dm,i.ic_if from comcat_if i,dis_linea_credito_dm lc where lc.ic_if = i.ic_if) vlc"   +
						" , COMCAT_ESTATUS_DOCTO  e  "+ //F032-2014

						"  where pd.ic_pyme=py.ic_pyme and pd.ic_moneda=m.ic_moneda"   +
						"  and pe.ic_pyme = py.ic_pyme"   +
						"  and pd.ic_epo=pe.ic_epo"   +
						"  and pe.ic_epo = pp.ic_epo"   +
						"  and pe.ic_pyme = pp.ic_pyme"   +
						"  and pp.ic_producto_nafin = 4"   +
						"  and cpe.ic_epo = pe.ic_epo"   +
						"  and cpe.ic_producto_nafin = pp.ic_producto_nafin"   +
						"  and pn.ic_producto_nafin = pp.ic_producto_nafin"   +
						"  and m.ic_moneda = tc.ic_moneda"   +
						"  and tc.dc_fecha IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
						"  and tf.ic_tipo_financiamiento = pd.ic_tipo_financiamiento"   +
						"  and ca.cc_acuse = pd.cc_acuse"   +
						"  and pd.ic_clasificacion = clas.ic_clasificacion (+) "+
						"  and vlc.ic_linea_credito_dm(+) = pd.ic_linea_credito_dm"  +
						"  and e.ic_estatus_docto = pd.ic_estatus_docto "+ //F032-2014
						"  and pd.cc_acuse = '"+ccAcuse+"'"+
														 condicion;

			}else if(tipoCredito.equals("F")){
				qrySentencia=	"select pd.ig_numero_docto"+
						"  , TO_CHAR(pd.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
						"  , TO_CHAR(pd.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
						"  , pd.fn_monto, pd.ct_referencia, py.cg_razon_social"   +
						"  , m.ic_moneda, m.cd_nombre,ROWNUM AS ic_consecutivo,py.ic_pyme,pe.cg_num_distribuidor"   +
						"  , pd.cg_campo1, pd.cg_campo2, pd.cg_campo3, pd.cg_campo4"   +
						"  , pd.cg_campo5"   +
						"  , cpe.cg_tipos_credito"   +
						"  , pd.fn_porc_descuento"   +
						"  , pd.ig_plazo_descuento"   +
						"  , decode(nvl(cpe.cg_tipo_conversion,pn.cg_tipo_conversion),'P','Dolar-Peso','Sin conversion') as tipo_conversion"   +
						"  , to_number(decode(nvl(cpe.cg_tipo_conversion,pn.cg_tipo_conversion),'P',tc.fn_valor_compra,'')) as tipo_cambio"   +
						"  , decode(nvl(cpe.cg_responsable_interes,pn.cg_responsable_interes),'E','EPO','Distribuidor') as responsable_interes"   +
						"  , decode(nvl(cpe.cg_tipo_cobranza,pn.cg_tipo_cobranza),'D','Delegada','No Delegada') as tipo_cobranza"   +
						"  , pd.ic_linea_credito_dm"   +
						"  , pd.ig_plazo_credito"   +
						"  , to_char(nvl(pd.df_fecha_venc_credito,sysdate),'dd/mm/yyyy') as df_fecha_venc_credito"   +
						"  , to_char(ca.df_fechahora_carga,'dd/mm/yyyy') as fecha_carga"   +
						"  , to_char(ca.df_fechahora_carga,'hh:mi') as hora_carga"   +
						"  , ca.ic_usuario as usuario"   +
						"  ,vlc.cg_razon_social as nombre_if"   +
						"  ,pd.ig_plazo_docto"   +
						"  ,'NA' as MODO_PLAZO"   +
						"  ,vlc.ic_moneda as MONEDA_LINEA, 'NA'  as CATEGORIA"+
				 ", pd.ic_estatus_docto as estatus "+
						" ,pd.CG_VENTACARTERA as CG_VENTACARTERA "+
						" ,e.CD_DESCRIPCION as DESESTATUS "+ //F032-2014
						" ,pd.ic_epo as IC_EPO  "+ //F032-2014
						", DECODE (pd.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ' ) AS TIPOPAGO "+ //F09-2015
						                  
												
						"  from dis_documento pd, comcat_pyme py"   +
						"  ,comcat_moneda m,comrel_pyme_epo pe"   +
						"  ,comrel_producto_epo cpe,comcat_producto_nafin pn"   +
						"  , com_tipo_cambio tc"   +
						"  ,com_acuse1 ca"   +
						"  ,(select lc.ic_moneda,i.cg_razon_social,lc.ic_linea_credito_dm,i.ic_if from comcat_if i,dis_linea_credito_dm lc where lc.ic_if = i.ic_if) vlc"   +
						" ,  COMCAT_ESTATUS_DOCTO  e "+

						"  where pd.ic_pyme=py.ic_pyme and pd.ic_moneda=m.ic_moneda"   +
						"  and pe.ic_pyme = py.ic_pyme"   +
						"  and pd.ic_epo=pe.ic_epo"   +
						"  and pe.ic_epo = cpe.ic_epo"   +
						"  and cpe.ic_producto_nafin = 4"   +
						"  and cpe.ic_epo = pe.ic_epo"   +
						"  and pn.ic_producto_nafin = cpe.ic_producto_nafin"   +
						"  and m.ic_moneda = tc.ic_moneda"   +
						"  and tc.dc_fecha IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
						"  and ca.cc_acuse = pd.cc_acuse"   +
						"  and vlc.ic_linea_credito_dm(+) = pd.ic_linea_credito_dm"  +
						" and pd.cc_acuse = '"+ccAcuse+"'"+
						" and cpe.cg_tipos_credito = '"+tipoCredito+"'"+
						" and e.ic_estatus_docto = pd.ic_estatus_docto "; //F032-2014
		}

          rs = con.queryDB(qrySentencia);
    System.out.println("qrySentencia"+qrySentencia);
            while(rs.next())
              {
              vecColumnas = new Vector();
/*0*/               vecColumnas.addElement((rs.getString("CG_NUM_DISTRIBUIDOR")==null)?"":rs.getString("CG_NUM_DISTRIBUIDOR"));// 0
/*1*/               vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));    // 1 nombre de la pyme
/*2*/               vecColumnas.addElement((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));   // 2
/*3*/               vecColumnas.addElement((rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION"));    // 3
/*4*/               vecColumnas.addElement((rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC"));   // 4
/*5*/               vecColumnas.addElement((rs.getString("CD_NOMBRE")==null)?"":rs.getString("CD_NOMBRE"));          // 5 nombre de la moneda
/*6*/               vecColumnas.addElement((rs.getString("FN_MONTO")==null)?"":rs.getString("FN_MONTO"));           // 6
/*7*/               vecColumnas.addElement((rs.getString("CT_REFERENCIA")==null)?"":rs.getString("CT_REFERENCIA"));      // 7
/*8*/               vecColumnas.addElement((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));          // 8
/*9*/               vecColumnas.addElement((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));            // 9
/*10*/              vecColumnas.addElement((rs.getString("IC_CONSECUTIVO")==null)?"":rs.getString("IC_CONSECUTIVO"));     // 10
						  if(!tipoCredito.equals("F")){
/*11*/             	 vecColumnas.addElement((rs.getString("CG_TIPO_CREDITO")==null)?"":rs.getString("CG_TIPO_CREDITO"));      // 11
						  }else 	if(tipoCredito.equals("F")){
/*11*/             	 vecColumnas.addElement((rs.getString("CG_TIPOS_CREDITO")==null)?"":rs.getString("CG_TIPOS_CREDITO"));      // 11
						  }
/*12*/              vecColumnas.addElement((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));      // 12
/*13*/              vecColumnas.addElement((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));      // 11
/*14*/              vecColumnas.addElement((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));      // 11
/*15*/              vecColumnas.addElement((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));      // 17
/*16*/              vecColumnas.addElement((rs.getString("RESPONSABLE_INTERES")==null)?"":rs.getString("RESPONSABLE_INTERES"));      // 16
/*17*/              vecColumnas.addElement((rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA"));      // 17
/*18*/              vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO_DM")==null)?"":rs.getString("IC_LINEA_CREDITO_DM"));      // 18
/*19*/              vecColumnas.addElement((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));      // 19
/*20*/              vecColumnas.addElement((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));      // 20
/*21*/              vecColumnas.addElement((rs.getString("FECHA_CARGA")==null)?"":rs.getString("FECHA_CARGA"));      // 21
/*22*/              vecColumnas.addElement((rs.getString("HORA_CARGA")==null)?"":rs.getString("HORA_CARGA"));      // 22
String acuseFormateado =  ccAcuse.substring(0,1)+"-"+ccAcuse.substring(1,2)+"-"+ccAcuse.substring(2,8)+"-"+ccAcuse.substring(8,10)+"-"+ccAcuse.substring(10,15);
/*23*/              vecColumnas.addElement(acuseFormateado);      // 23
/*24*/              vecColumnas.addElement((rs.getString("USUARIO")==null)?"":rs.getString("USUARIO"));      // 24
/*25*/              vecColumnas.addElement((rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF"));      // 25
/*26*/              vecColumnas.addElement((rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO"));      // 26
/*27*/              vecColumnas.addElement((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));      // 26
/*28*/              vecColumnas.addElement((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));      // 27
/*29*/              vecColumnas.addElement((rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA"));      // 27
/*30*/              vecColumnas.addElement((rs.getString("estatus")==null)?"":rs.getString("estatus"));      // 27
/*31*/              vecColumnas.addElement((rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA"));      // 27
/*32*/              vecColumnas.addElement((rs.getString("DESESTATUS")==null)?"":rs.getString("DESESTATUS"));      //F032-2014
/*33*/              vecColumnas.addElement((rs.getString("TIPOPAGO")==null)?"":rs.getString("TIPOPAGO"));      //F032-2014 

						  String ic_epo =  (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO");
						  Vector camposA=  getCamposAdicionales(ic_epo,false);
						  int  numCamposAd =  camposA.size();

              for(int i=1;i<=numCamposAd;i++)
                {
                vecColumnas.addElement((rs.getString("CG_CAMPO"+i)==null)?"":rs.getString("CG_CAMPO"+i));
                }
              vecRenglones.addElement(vecColumnas);
              }    //while
              rs.close();
            con.cierraStatement();
        }catch(Exception e){
          System.out.println("CargaDocDistBean::getDoctoInsertado Exception "+e);
          resultado = false;
          throw new NafinException("SIST0001");
        }finally{
        con.cierraConexionDB();
		System.out.println("CargaDocDistBean::getDoctoInsertado (S)");
        }
        return vecRenglones;
      }



    //DEVUELVE UN VECTOR PARA MOSTRAR LA CONSULTA DE LOS DATOS INSERTADOS
    // EN LAS TABLAS PERMANENTES
    public Vector getDoctoInsertado(String icProcDocto,String icEpo,String icConsecutivo,String tipoCredito)
      throws NafinException
      {
		System.out.println("CargaDocDistBean::getDoctoInsertado (E)");
        boolean   resultado        = true;
        AccesoDB  con              = new AccesoDB();
        ResultSet rs               = null;
        String    qrySentencia     = "";
        Vector    vecRenglones     = new Vector();
        Vector    vecColumnas      = new Vector();
        String 	  condicion 	   = "";
      try{
        con.conexionDB();
			getNumCamposAdicionales(icEpo,false,con);

			if("F".equals(tipoCredito)){

				if(!"".equals(tipoCredito)&&tipoCredito!=null)
					condicion += " and cpe.cg_tipos_credito = '"+tipoCredito+"'";
				if(icProcDocto!=null&&!"".equals(icProcDocto))
					condicion += " and pd.ic_proc_docto="+icProcDocto;
				if(icConsecutivo!=null&&!"".equals(icConsecutivo))
					condicion += " and pd.ic_consecutivo ="+icConsecutivo;

				qrySentencia="select pd.ig_numero_docto"+
							" , TO_CHAR(pd.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"+
							" , TO_CHAR(pd.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"+
							" , pd.fn_monto, pd.ct_referencia, py.cg_razon_social"+
							" , m.ic_moneda, m.cd_nombre,pd.ic_consecutivo,py.ic_pyme,pe.cg_num_distribuidor"+
							" , pd.cg_campo1, pd.cg_campo2, pd.cg_campo3, pd.cg_campo4"+
							" , pd.cg_campo5 "+
							" , cpe.cg_tipos_credito"+
							" , pd.fn_porc_descuento"+
							" , pd.ig_plazo_descuento"+
							" , decode(nvl(cpe.cg_tipo_conversion,pn.cg_tipo_conversion),'P','Dolar-Peso','Sin conversion') as tipo_conversion"+
							" , decode(nvl(cpe.cg_tipo_conversion,pn.cg_tipo_conversion),'P',tc.fn_valor_compra,'') as tipo_cambio"  +
							" , decode(nvl(cpe.cg_responsable_interes,pn.cg_responsable_interes),'E','EPO','Distribuidor') as responsable_interes"+
							" , decode(nvl(cpe.cg_tipo_cobranza,pn.cg_tipo_cobranza),'D','Delegada','No Delegada') as tipo_cobranza"+
							" , pd.ic_linea_credito_dm"+
							" , pd.ig_plazo_credito"+
							" , to_char(nvl(pd.df_fecha_venc_credito,sysdate),'dd/mm/yyyy') as df_fecha_venc_credito"+
							" , ROUND(pd.df_fecha_venc-pd.df_fecha_emision,0) as IG_PLAZO_DOCTO"+
							" , 'N/A' as MODO_PLAZO "+
							" , nvl(cpe.cg_responsable_interes,pn.cg_responsable_interes) as cg_responsable_interes"+
							" , lc.ic_moneda as MONEDA_LINEA"+
							" , decode(cpe.cg_tipos_credito,'D','Descuento Mercantil','C','Credito en Cuenta Corriente', 'F', 'Factoraje con Recuerso') as TIPO_CREDITO"+
							" , pd.fn_monto * (pd.fn_porc_descuento/100) as MONTO_DESCUENTO"+
							" , '' as  ic_tipo_financiamiento "+
							" ,nvl(cpe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO"+
							" ,nvl(cpe.ig_dias_maximo,pn.in_dias_maximo) as DIAS_MAXIMO,clas.cg_descripcion as CATEGORIA,clas.ic_clasificacion as IC_CATEGORIA"+
					  " ,pd.ic_estatus_docto as estatus "+
							" ,pd.CG_VENTACARTERA as CG_VENTACARTERA "+
							",  e.CD_DESCRIPCION AS DESESTATUS "+ //F032-2014
							",  pd.IC_EPO AS IC_EPO "+ //F032-2014
							", DECODE (pd.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ' ) AS TIPOPAGO "+ //F09-2015
							                     
							" from distmp_proc_docto pd, comcat_pyme py"+
							" ,comcat_moneda m,comrel_pyme_epo pe"+
							" ,comrel_producto_epo cpe,comcat_producto_nafin pn"+
							" ,com_tipo_cambio tc"+
							" ,dis_linea_credito_dm lc,comrel_clasificacion clas"+
							", COMCAT_ESTATUS_DOCTO e  "+ //F032-2014


					  " where pd.ic_pyme=py.ic_pyme and pd.ic_moneda=m.ic_moneda"+
							" and pe.ic_pyme = py.ic_pyme"+
							" and pe.ic_epo="+icEpo+
							" and pd.ic_epo=pe.ic_epo"+
							" and cpe.ic_producto_nafin = 4"+
							" and cpe.ic_epo = pe.ic_epo"+
							" and cpe.ic_producto_nafin = cpe.ic_producto_nafin"+
							" and pn.ic_producto_nafin = cpe.ic_producto_nafin"+
							" and m.ic_moneda = tc.ic_moneda"+
							" and pd.ic_linea_credito_dm = lc.ic_linea_credito_dm(+)"+
							" AND pd.ic_clasificacion = clas.ic_clasificacion (+) "+
							" and tc.dc_fecha IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"+
							" and pd.IC_ESTATUS_DOCTO = e.IC_ESTATUS_DOCTO " + //F032-2014
							condicion;
			}else {

				  System.out.println("paso por Aqui ");
				if(!"".equals(tipoCredito)&&tipoCredito!=null)
					condicion += " and pp.cg_tipo_credito = '"+tipoCredito+"'";
				if(icProcDocto!=null&&!"".equals(icProcDocto))
					condicion += " and pd.ic_proc_docto="+icProcDocto;
				if(icConsecutivo!=null&&!"".equals(icConsecutivo))
					condicion += " and pd.ic_consecutivo ="+icConsecutivo;

				qrySentencia="select pd.ig_numero_docto"+
							" , TO_CHAR(pd.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"+
							" , TO_CHAR(pd.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"+
							" , pd.fn_monto, pd.ct_referencia, py.cg_razon_social"+
							" , m.ic_moneda, m.cd_nombre,pd.ic_consecutivo,py.ic_pyme,pe.cg_num_distribuidor"+
							" , pd.cg_campo1, pd.cg_campo2, pd.cg_campo3, pd.cg_campo4"+
							" , pd.cg_campo5 "+
							" , pp.cg_tipo_credito"+
							" , pd.fn_porc_descuento"+
							" , pd.ig_plazo_descuento"+
							" , decode(nvl(cpe.cg_tipo_conversion,pn.cg_tipo_conversion),'P','Dolar-Peso','Sin conversion') as tipo_conversion"+
							" , decode(nvl(cpe.cg_tipo_conversion,pn.cg_tipo_conversion),'P',tc.fn_valor_compra,'') as tipo_cambio"  +
							" , decode(nvl(cpe.cg_responsable_interes,pn.cg_responsable_interes),'E','EPO','Distribuidor') as responsable_interes"+
							" , decode(nvl(cpe.cg_tipo_cobranza,pn.cg_tipo_cobranza),'D','Delegada','No Delegada') as tipo_cobranza"+
							" , pd.ic_linea_credito_dm"+
							" , pd.ig_plazo_credito"+
							" , to_char(nvl(pd.df_fecha_venc_credito,sysdate),'dd/mm/yyyy') as df_fecha_venc_credito"+
							" , ROUND(pd.df_fecha_venc-pd.df_fecha_emision,0) as IG_PLAZO_DOCTO"+
							" , tf.cd_descripcion as MODO_PLAZO"+
							" , nvl(cpe.cg_responsable_interes,pn.cg_responsable_interes) as cg_responsable_interes"+
							" , lc.ic_moneda as MONEDA_LINEA"+
							" , decode(pp.cg_tipo_credito,'D','Descuento Mercantil','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
							" , pd.fn_monto * (pd.fn_porc_descuento/100) as MONTO_DESCUENTO"+
							" , tf.ic_tipo_financiamiento "+
							" ,nvl(cpe.ig_dias_minimo,pn.in_dias_minimo) as DIAS_MINIMO"+
							" ,nvl(cpe.ig_dias_maximo,pn.in_dias_maximo) as DIAS_MAXIMO,clas.cg_descripcion as CATEGORIA,clas.ic_clasificacion as IC_CATEGORIA"+
					  " ,pd.ic_estatus_docto as estatus "+
							" ,pd.CG_VENTACARTERA as CG_VENTACARTERA "+
							",  e.CD_DESCRIPCION AS DESESTATUS "+ //F032-2014
							",  pd.IC_EPO AS IC_EPO "+ //F032-2014
							", DECODE (pd.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ' ) AS TIPOPAGO "+ //F09-2015							                     
														
							" from distmp_proc_docto pd, comcat_pyme py"+
							" ,comcat_moneda m,comrel_pyme_epo pe"+
							" ,comrel_pyme_epo_x_producto pp"+
							" ,comrel_producto_epo cpe,comcat_producto_nafin pn"+
							" ,com_tipo_cambio tc"+
							" ,comcat_tipo_financiamiento tf"+
							" ,dis_linea_credito_dm lc,comrel_clasificacion clas"+
							", COMCAT_ESTATUS_DOCTO e  "+ //F032-2014

					  " where pd.ic_pyme=py.ic_pyme and pd.ic_moneda=m.ic_moneda"+
							" and pe.ic_pyme = py.ic_pyme"+
							" and pe.ic_epo="+icEpo+
							" and pd.ic_epo=pe.ic_epo"+
							" and pe.ic_epo = pp.ic_epo"+
							" and pe.ic_pyme = pp.ic_pyme"+
							" and pp.ic_producto_nafin = 4"+
							" and cpe.ic_epo = pe.ic_epo"+
							" and cpe.ic_producto_nafin = pp.ic_producto_nafin"+
							" and pn.ic_producto_nafin = pp.ic_producto_nafin"+
							" and m.ic_moneda = tc.ic_moneda"+
							" and pd.ic_linea_credito_dm = lc.ic_linea_credito_dm(+)"+
							" AND pd.ic_clasificacion = clas.ic_clasificacion (+) "+
							" and tc.dc_fecha IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"+
							" and tf.ic_tipo_financiamiento = pd.ic_tipo_financiamiento"+
							" and pd.IC_ESTATUS_DOCTO = e.IC_ESTATUS_DOCTO " + //F032-2014
							condicion;

			}

			System.out.println("--------------------");
			System.out.println("qrySentencia"+qrySentencia);
			System.out.println("----------------------");

			rs = con.queryDB(qrySentencia);


            while(rs.next()) {
              vecColumnas = new Vector();
/*0*/               vecColumnas.addElement((rs.getString("CG_NUM_DISTRIBUIDOR")==null)?"":rs.getString("CG_NUM_DISTRIBUIDOR"));// 0
/*1*/               vecColumnas.addElement((rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL"));    // 1 nombre de la pyme
/*2*/               vecColumnas.addElement((rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO"));   // 2
/*3*/               vecColumnas.addElement((rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION"));    // 3
/*4*/               vecColumnas.addElement((rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC"));   // 4
/*5*/               vecColumnas.addElement((rs.getString("CD_NOMBRE")==null)?"":rs.getString("CD_NOMBRE"));          // 5 nombre de la moneda
/*6*/               vecColumnas.addElement((rs.getString("FN_MONTO")==null)?"":rs.getString("FN_MONTO"));           // 6
/*7*/               vecColumnas.addElement((rs.getString("CT_REFERENCIA")==null)?"":rs.getString("CT_REFERENCIA"));      // 7
/*8*/               vecColumnas.addElement((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));          // 8
/*9*/               vecColumnas.addElement((rs.getString("IC_PYME")==null)?"":rs.getString("IC_PYME"));            // 9
/*10*/              vecColumnas.addElement((rs.getString("IC_CONSECUTIVO")==null)?"":rs.getString("IC_CONSECUTIVO"));     // 10
							if(!"F".equals(tipoCredito)){
/*11*/	              vecColumnas.addElement((rs.getString("CG_TIPO_CREDITO")==null)?"":rs.getString("CG_TIPO_CREDITO"));      // 11
							}else if("F".equals(tipoCredito)){
								vecColumnas.addElement((rs.getString("CG_TIPOS_CREDITO")==null)?"":rs.getString("CG_TIPOS_CREDITO"));      // 11
							}
/*12*/              vecColumnas.addElement((rs.getString("FN_PORC_DESCUENTO")==null)?"":rs.getString("FN_PORC_DESCUENTO"));      // 12
/*13*/              vecColumnas.addElement((rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO"));      // 11
/*14*/              vecColumnas.addElement((rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION"));      // 11
/*15*/              vecColumnas.addElement((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));      // 17
/*16*/              vecColumnas.addElement((rs.getString("RESPONSABLE_INTERES")==null)?"":rs.getString("RESPONSABLE_INTERES"));      // 16
/*17*/              vecColumnas.addElement((rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA"));      // 17
/*18*/              vecColumnas.addElement((rs.getString("IC_LINEA_CREDITO_DM")==null)?"":rs.getString("IC_LINEA_CREDITO_DM"));      // 18
/*19*/              vecColumnas.addElement((rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO"));      // 19
/*20*/              vecColumnas.addElement((rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO"));      // 20
/*21*/ 	            vecColumnas.addElement((rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO"));                     //26
/*22*/ 	            vecColumnas.addElement((rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO"));                     //22
/*23*/              vecColumnas.addElement((rs.getString("CG_RESPONSABLE_INTERES")==null)?"":rs.getString("CG_RESPONSABLE_INTERES"));      // 23
/*24*/              vecColumnas.addElement((rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA"));      // 24
/*25*/              vecColumnas.addElement((rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO"));      // 25
/*26*/              vecColumnas.addElement((rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO"));      // 26
/*27*/              vecColumnas.addElement((rs.getString("IC_TIPO_FINANCIAMIENTO")==null)?"":rs.getString("IC_TIPO_FINANCIAMIENTO"));      // 27
/*28*/              vecColumnas.addElement((rs.getString("DIAS_MINIMO")==null)?"":rs.getString("DIAS_MINIMO"));      // 28
/*29*/              vecColumnas.addElement((rs.getString("DIAS_MAXIMO")==null)?"":rs.getString("DIAS_MAXIMO"));      // 29
/*30*/              vecColumnas.addElement("36");      // 36  INCREMENTAR ESTE NUMERO CADA QUE SE AGREGUE UN CAMPO, EL CAMPO  SE DEBE AGREGAR DESPUES DE ESTE
/*31*/              vecColumnas.addElement((rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA"));      // 29
/*32*/              vecColumnas.addElement((rs.getString("IC_CATEGORIA")==null)?"":rs.getString("IC_CATEGORIA"));      // 32
/*33*/              vecColumnas.addElement((rs.getString("estatus")==null)?"":rs.getString("estatus"));      // 32
/*34*/              vecColumnas.addElement((rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA"));      // 33
/*35*/              vecColumnas.addElement((rs.getString("DESESTATUS")==null)?"":rs.getString("DESESTATUS"));      //  //F032-2014
/*36*/              vecColumnas.addElement((rs.getString("TIPOPAGO")==null)?"":rs.getString("TIPOPAGO"));      //  //F032-2014

							String ic_epo = (rs.getString("IC_EPO")==null)?"":rs.getString("IC_EPO");
						  Vector camposA=  getCamposAdicionales(ic_epo,false);
						  int  numCamposAd =  camposA.size();

              for(int i=1;i<=numCamposAd;i++)  {
                vecColumnas.addElement((rs.getString("CG_CAMPO"+i)==null)?"":rs.getString("CG_CAMPO"+i));
               }
              vecRenglones.addElement(vecColumnas);
              }    //while
            con.cierraStatement();


				System.out.println("vecColumnas "+vecColumnas);
				System.out.println("vecRenglones "+vecRenglones);

        }catch(Exception e){
          System.out.println("CargaDocDistBean::getDoctoInsertado Exception ----------"+e);
          resultado = false;
          throw new NafinException("SIST0001");
        }finally{
        con.cierraConexionDB();
//        vecColumnas.removeAllElements();
		System.out.println("CargaDocDistBean::getDoctoInsertado (S)");
        }
        return vecRenglones;

      }

//Despliega la consulta de documentos detalle temporales
    public Vector getDetalleTmp(String icProcDocto,String igNumeroDocto,String icConsecutivo)
      throws NafinException
      {
		System.out.println("CargaDocDistBean::getDetalleTmp (E)");
        boolean   resultado        = true;
        AccesoDB  con              = new AccesoDB();
        ResultSet rs               = null;
        String    qrySentencia     = "";
        Vector    vecRenglones     = new Vector();
        Vector    vecColumnas      = new Vector();
      try{
        con.conexionDB();
         String    strAux = "";
	       qrySentencia="select ic_consecutivo, cg_campo1, cg_campo2, cg_campo3, cg_campo4,"+
        		" cg_campo5, cg_campo6, cg_campo7, cg_campo8, cg_campo9, cg_campo10"+
        		" from distmp_docto_detalle";



          if(icConsecutivo!=null&&!"".equals(icConsecutivo))
            qrySentencia += " where ic_consecutivo ="+icConsecutivo;
          else
            {
            qrySentencia += 		" where ic_proc_docto="+icProcDocto+
                            		" and ig_numero_docto = '"+igNumeroDocto+"'";
            }
          rs = con.queryDB(qrySentencia);
            while(rs.next())
              {
              vecColumnas = new Vector();
              strAux = (rs.getString("IC_CONSECUTIVO")==null)?"":rs.getString("IC_CONSECUTIVO");
              vecColumnas.addElement(strAux);
              for(int i=1;i<=vecColumnas.size();i++)
                {
                strAux = (rs.getString("CG_CAMPO"+i)==null)?"":rs.getString("CG_CAMPO"+i);
                vecColumnas.addElement(strAux);
                }
              vecRenglones.addElement(vecColumnas);
              }    //while
            con.cierraStatement();
        }catch(Exception e){
          System.out.println("CargaDocDistBean::getDetalleTmp Exception "+e);
          resultado = false;
          throw new NafinException("SIST0001");
        } finally{
	        con.cierraConexionDB();
			System.out.println("CargaDocDistBean::getDetalleTmp (S)");
        }
	        return vecRenglones;
      }




    /* Inserta los datos del acuse generado    */
    private void insertaAcuse(
        String cve_acuse,
        String numDoctosMN,
        String montoTotalMN,
        String numDoctosDL,
        String montoTotalDL,
        String ic_usuario,
        String seg_acuse,
        AccesoDB con)
        throws NafinException
        {
		System.out.println("CargaDocDistBean::insertaAcuse (E)");
        String qrySentencia = "";
 				qrySentencia= "insert into com_acuse1 (cc_acuse, in_total_docto_mn"+
  					          " , fn_total_monto_mn, in_total_docto_dl, fn_total_monto_dl"+
	  				          " , df_fechahora_carga, ic_usuario,cg_recibo_electronico, ic_producto_nafin)"+
		  			          " values ('"+cve_acuse+"', "+numDoctosMN+"+0, "+montoTotalMN+
			    	          " +0, "+numDoctosDL+"+0, "+montoTotalDL+"+0, sysdate"+
                      " , '"+ic_usuario+"','"+seg_acuse+"', 2)";
        try
          {
          con.ejecutaSQL(qrySentencia);
          }
        catch(Exception e)
          {
          System.out.println("CargaDocDistBean::insertaAcuse Exception "+e);
          throw new NafinException("SIST0001");
          }
		System.out.println("CargaDocDistBean::insertaAcuse (S)");
        }


/*public void validarUsuario(String esClaveEpo, String esLogin, String esPassword,
String esIP, String esHost, String esSistema )
throws NafinException
{
	System.out.println("CargaDocDistBean::validarUsuario (E)");
  String lsQrySentencia = null;
  AccesoDB conexion = new AccesoDB();

  SeguridadHome con;
  com.netro.seguridadbean.Seguridad objSeguridad = null;

  try {
      con = (SeguridadHome)  PortableRemoteObject.narrow(
      (new InitialContext()).lookup("SeguridadEJB"), SeguridadHome.class );

      objSeguridad = con.create();

// Valida el Usuario por medio de Seguridad, para cargar en Memoria el usuario, si es el caso
       objSeguridad.validaUsuario(esLogin, esPassword, esIP, esHost );

       conexion.conexionDB();
       String perfil = "";

      lsQrySentencia = "SELECT cc_perfil ";
      lsQrySentencia += " FROM seg _usuario U, seg _rel_usuario_perfil UP ";
      lsQrySentencia += " WHERE U.cg_login = '"+esLogin+"' ";
      lsQrySentencia += " AND U.cg_password = '"+esPassword+"' ";
      lsQrySentencia += " AND U.ic_usuario = UP.ic_usuario ";
      lsQrySentencia += " AND U.ic_epo = " + esClaveEpo;

      ResultSet rsSentencia = conexion.queryDB( lsQrySentencia );
      System.out.println(lsQrySentencia);
      if(rsSentencia.next()) {
          perfil = rsSentencia.getString(1);
      } else {
          throw new NafinException("DSCT0009");
      }

      try{
        objSeguridad.validaFacultad( (String) esLogin, (String) perfil,
        "PEPOAAUTOR", "19EPO19AUTORIZA", (String) esSistema, (String) esIP, (String)
        esHost );
      }catch( SeguException ErrorSeg){
        throw new NafinException("DSCT0014");
      }
   }catch( SeguException ErrorSeg){
      throw new NafinException("DSCT0009");
   }catch (NafinException ne) {
      throw ne;
   }catch (Exception e) {
      throw new NafinException("SIST0001");
   } finally {
      if (conexion.hayConexionAbierta() == true) {
        conexion.cierraConexionDB();
	System.out.println("CargaDocDistBean::validarUsuario (S)");
   }
}

}
*/

public boolean validaDetalleMasivo(BufferedReader br,String ic_epo)
  throws NafinException
  {
	System.out.println("CargaDocDistBean::validaDetalleMasivo (E)");
  boolean ok = true;
  boolean hayDocto = false;
  String linea="", los_campos_det="", los_valores_det="", valcamp="";
  String ig_numero_docto="", consecutivo="", campdet1="", campdet2="", campdet3="", campdet4="", campdet5="", campdet6="", campdet7="", campdet8="", campdet9="", campdet10="";
  int no_pipes=0, totlinead=1, d=0;

  Vector numCamposDet=  getCamposAdicionales(ic_epo,true);
  int no_campos_detalle = numCamposDet.size();
  String query = "";
  VectorTokenizer vtd=null; Vector vecdet=null;
  AccesoDB con = null;
  ResultSet rs = null;
  String msgError = "";
  int noCamposDet[]=new int[11];  String nombreCampoDet[]=new String[11];
  int longitudDet[]=new int[11];  String tipoDatoDet[]=new String[11];
  Vector vecColumnas = null;
   Vector nums_doctos=new Vector();
  try
    {
    getCamposAdicionales(ic_epo,true);
    con = new AccesoDB();
    con.conexionDB();

	 Vector vecCamposDet=  getCamposAdicionales(ic_epo,true);

    for(int i=0;i<vecCamposDet.size();i++) {
      vecColumnas = (Vector)vecCamposDet.get(i);
      noCamposDet[i+1] = Integer.parseInt((String)vecColumnas.get(1));
      nombreCampoDet[i+1] = (String)vecColumnas.get(0);
      tipoDatoDet[i+1] = (String)vecColumnas.get(2);
      longitudDet[i+1] = Integer.parseInt((String)vecColumnas.get(3));
      }

    while ((linea=br.readLine())!=null)
      {
      vtd=new VectorTokenizer(linea,"|");
      vecdet=vtd.getValuesVector();
      if(no_campos_detalle > vecdet.size()) {
        no_pipes=no_campos_detalle-vecdet.size();
        msgError += " @Error en la linea "+totlinead+", debe de poner "+no_pipes+" separador(es) | si usted no desea poner un valor. \n";
        ok=false;
        }
      ig_numero_docto=(vecdet.size()>=1)?(String)vecdet.get(0):"";    ig_numero_docto=Comunes.quitaComitasSimples(ig_numero_docto);
      consecutivo=(vecdet.size()>=2)?(String)vecdet.get(1):"";        consecutivo=Comunes.quitaComitasSimples(consecutivo);
      campdet1=(vecdet.size()>=3)?(String)vecdet.get(2):"";           campdet1=Comunes.quitaComitasSimples(campdet1);
      campdet2=(vecdet.size()>=4)?(String)vecdet.get(3):"";           campdet2=Comunes.quitaComitasSimples(campdet2);
      campdet3=(vecdet.size()>=5)?(String)vecdet.get(4):"";           campdet3=Comunes.quitaComitasSimples(campdet3);
      campdet4=(vecdet.size()>=6)?(String)vecdet.get(5):"";           campdet4=Comunes.quitaComitasSimples(campdet4);
      campdet5=(vecdet.size()>=7)?(String)vecdet.get(6):"";           campdet5=Comunes.quitaComitasSimples(campdet5);
      campdet6=(vecdet.size()>=8)?(String)vecdet.get(7):"";           campdet6=Comunes.quitaComitasSimples(campdet6);
      campdet7=(vecdet.size()>=9)?(String)vecdet.get(8):"";           campdet7=Comunes.quitaComitasSimples(campdet7);
      campdet8=(vecdet.size()>=10)?(String)vecdet.get(9):"";          campdet8=Comunes.quitaComitasSimples(campdet8);
      campdet9=(vecdet.size()>=11)?(String)vecdet.get(10):"";         campdet9=Comunes.quitaComitasSimples(campdet9);
      campdet10=(vecdet.size()>=12)?(String)vecdet.get(11):"";        campdet10=Comunes.quitaComitasSimples(campdet10);
      vecdet.removeAllElements();
      if("".equals(ig_numero_docto)||ig_numero_docto==null) {
          msgError += " @Error en la linea "+totlinead+", el valor del NUMERO DOCUMENTO no puede estar vacio, debe de tener un valor. \n";
          ok = false;
      } else if(ig_numero_docto.length()>15) {
          msgError += " @Error en la linea "+totlinead+", el valor del NUMERO DOCUMENTO excede la longitud definida por su epo que es de 15. \n";
          ok = false;
      }
    /* La Validacion del valor del Consecutivo. */
      if("".equals(consecutivo)||consecutivo==null) {
          msgError += " @Error en la linea "+totlinead+", el valor del CONSECUTIVO no puede estar vacio, debe de tener un valor. \n";
          ok = false;
      } else {
          if (!Comunes.esNumero(consecutivo)) {
              msgError += " @Error en la linea "+totlinead+", el valor del CONSECUTIVO debe de ser Numerico. \n";
              ok = false;
          }
      }
      for(d=1; d<noCamposDet.length; d++) {
        switch(noCamposDet[d]) {
           case 1: valcamp=campdet1; los_valores_det+=",'"+campdet1+"'"; los_campos_det+=", CG_CAMPO1"; break;
           case 2: valcamp=campdet2; los_valores_det+=",'"+campdet2+"'"; los_campos_det+=", CG_CAMPO2"; break;
           case 3: valcamp=campdet3; los_valores_det+=",'"+campdet3+"'"; los_campos_det+=", CG_CAMPO3"; break;
           case 4: valcamp=campdet4; los_valores_det+=",'"+campdet4+"'"; los_campos_det+=", CG_CAMPO4"; break;
           case 5: valcamp=campdet5; los_valores_det+=",'"+campdet5+"'"; los_campos_det+=", CG_CAMPO5"; break;
           case 6: valcamp=campdet6; los_valores_det+=",'"+campdet6+"'"; los_campos_det+=", CG_CAMPO6"; break;
           case 7: valcamp=campdet7; los_valores_det+=",'"+campdet7+"'"; los_campos_det+=", CG_CAMPO7"; break;
           case 8: valcamp=campdet8; los_valores_det+=",'"+campdet8+"'"; los_campos_det+=", CG_CAMPO8"; break;
           case 9: valcamp=campdet9; los_valores_det+=",'"+campdet9+"'"; los_campos_det+=", CG_CAMPO9"; break;
           case 10: valcamp=campdet10; los_valores_det+=",'"+campdet10+"'"; los_campos_det+=", CG_CAMPO10"; break;
        }
        if(noCamposDet[d] == d){
          if((tipoDatoDet[d].equalsIgnoreCase("N")) && (!valcamp.equals(""))) {
            if (!Comunes.esDecimal(valcamp)) {
              msgError += " @Error en la linea "+totlinead+", el Tipo de Dato del Campo "+nombreCampoDet[d]+" debe de ser Numerico. \n";
        			ok=false;
            }
          }
          if(longitudDet[d] < valcamp.length()) {
            msgError += " @Error en la linea "+totlinead+", la Longitud del Campo "+nombreCampoDet[d]+" es mayor que la definida por su Epo, por favor reviselo. \n";
        	  ok=false;
          }
        }
      } //for
      int actualiza=0;
      hayDocto = false;
      try {
          existeDocto(ic_epo,null,ig_numero_docto,con);
      } catch(NafinException ne) {
        hayDocto = true;   //esta funcion lanzo la excepcion indicando que si existe el documento
      }

      if(hayDocto) {
        int ic_documento=0; String numero_docto="";
        query = "select ic_documento, ig_numero_docto from dis_documento where ic_epo="+ic_epo+
            "and ig_numero_docto='"+ig_numero_docto+"'";
        rs = con.queryDB(query);
        if(rs.next()) {
          ic_documento = rs.getInt(1);
          numero_docto = rs.getString(2);
          }
        con.cierraStatement();
        query = "select count(*) from dis_docto_detalle "+
                "where ic_documento="+ic_documento+" and ic_docto_detalle="+consecutivo;
        rs = con.queryDB(query);
        if(rs.next()) {
            actualiza=rs.getInt(1);
        }
        con.cierraStatement();
        if(actualiza > 0) {
          query = "UPDATE dis_docto_detalle set CG_CAMPO1='"+campdet1+"', CG_CAMPO2='"+campdet2+"', "+
              "CG_CAMPO3='"+campdet3+"', CG_CAMPO4='"+campdet4+"', CG_CAMPO5='"+campdet5+"', CG_CAMPO6='"+campdet6+"', "+
              "CG_CAMPO7='"+campdet7+"', CG_CAMPO8='"+campdet8+"', CG_CAMPO9='"+campdet9+"', CG_CAMPO10='"+campdet10+"' "+
              "where IC_DOCumento="+ic_documento+" and IC_docto_DETALLE="+consecutivo ;
        } else {
          query = "INSERT INTO dis_docto_detalle(IC_documento, IC_docto_DETALLE "+los_campos_det+")"+
              "VALUES("+ic_documento+", "+consecutivo+" "+los_valores_det+")";
        } //else
        los_valores_det=""; los_campos_det="";
        if(ok) {
          try{
					  con.ejecutaSQL(query);
            }catch(Exception e){
                  msgError += "Error en la linea: "+totlinead+", los valores sobrepasan los limites permitidos, verifique sus longitudes. \n";
                  ok = false;
            }
          } //ok
        } else {
              msgError += " @Error en la linea "+totlinead+", no existe ningun documento en la Base de Datos para el numero de documento "+ig_numero_docto+", por favor reviselo. \n";
          ok=false;
      } // else
      con.terminaTransaccion(ok);
      totlinead++;
      nums_doctos.addElement(ig_numero_docto);
    } //while externo.
    //numDoctos = nums_doctos;
/*  }catch(NafinException ne){
      msgError += " @"+ne.getMsgError();
      ok = false;*/
  } catch(Exception e){
      ok = false;
      throw new NafinException("SIST0001");
  } finally{
    if(con.hayConexionAbierta())
      con.cierraConexionDB();
	System.out.println("CargaDocDistBean::validaDetalleMasivo (S)");
  }
return ok;
} //fin del m�todo

/*
public Vector getNumDoctos()
  {
  return numDoctos;
  }
  */


public boolean esParametrizado(String ic_epo)
 throws NafinException{
	System.out.println("CargaDocDistBean::esParametrizado (E)");
        AccesoDB  con              = new AccesoDB();
        ResultSet rs               = null;
        String    qrySentencia     = "";
				boolean		parametro 			 = false;
      try{
        con.conexionDB();
				qrySentencia = " select nvl(pe.cs_automatico_susceptible,pn.cs_automatico_susceptible)"+
								" from comrel_producto_epo pe"+
								"	,comcat_producto_nafin pn"+
								" where pe.ic_epo = "+ic_epo+
								" and pn.ic_producto_nafin = 2"+
								" and pn.ic_producto_nafin = pe.ic_producto_nafin";
        rs = con.queryDB(qrySentencia);
							if(rs.next())
								parametro = "S".equals(rs.getString(1));
							con.cierraStatement();

      }catch(Exception e){
          System.out.println("CargaDocDistBean::esParametrizado Exception "+e);
          throw new NafinException("SIST0001");
      }finally{
        	if(con.hayConexionAbierta())
		        con.cierraConexionDB();
		System.out.println("CargaDocDistBean::esParametrizado (S)");
      }
 return parametro;
 }

 public void operaCifrasControl(String ses_ic_epo)
 throws NafinException{
		System.out.println("CargaDocDistBean::operaCifrasControl (E)");
        AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String cs_cifras_control = "";
        try{
        	con = new AccesoDB();
                con.conexionDB();
                qrySentencia =	" SELECT NVL(PE.cs_cifras_control,PN.cs_cifras_control) "+
                		" FROM COMCAT_PRODUCTO_NAFIN PN,COMREL_PRODUCTO_EPO PE"+
                		" WHERE PN.IC_PRODUCTO_NAFIN = 4"+
                                " AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
                                " AND PE.IC_EPO = "+ses_ic_epo;
                rs = con.queryDB(qrySentencia);
                if(rs.next())
                	cs_cifras_control = (rs.getString(1)==null)?"":rs.getString(1);
                con.cierraStatement();
                if("N".equals(cs_cifras_control))
                	throw new NafinException("DIST0002");
        }catch(NafinException ne){
		throw ne;
        }catch(Exception e){
        	System.out.println("CargaDocDistBean::operaCifrasControl Exception "+e);
        }finally{
        	if(con.hayConexionAbierta())
                	con.cierraConexionDB();
		System.out.println("CargaDocDistBean::operaCifrasControl (S)");
        }
 }


 public void operaDistribuidores(String ses_ic_epo)
 throws NafinException{
		System.out.println("CargaDocDistBean::operaDistribuidores (E)");
        AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String cs_habilitado = "";
        try{
        	con = new AccesoDB();
                con.conexionDB();
                qrySentencia =  " SELECT CS_HABILITADO FROM COMREL_PRODUCTO_EPO PE"+
                		" WHERE PE.IC_PRODUCTO_NAFIN = 4"+
                                " AND PE.IC_EPO = "+ses_ic_epo;
                rs = con.queryDB(qrySentencia);
                if(rs.next())
                	cs_habilitado = (rs.getString(1)==null)?"":rs.getString(1);
                con.cierraStatement();
                if("N".equals(cs_habilitado))
                	throw new NafinException("DIST0014");
        }catch(NafinException ne){
		throw ne;
        }catch(Exception e){
        	System.out.println("CargaDocDistBean::operaCifrasControl Exception "+e);
        }finally{
        	if(con.hayConexionAbierta())
                	con.cierraConexionDB();
		System.out.println("CargaDocDistBean::operaDistribuidores (S)");
        }
 }

 public String getTipoCambio(String ic_epo)
 throws NafinException{
		System.out.println("CargaDocDistBean::getTipoCambio (E)");
	 	AccesoDB	con = null;
        ResultSet 	rs = null;
        String          qrySentencia = "";
        String 		tipoCambio = "";
        String		aux = "";
        try{
        	con = new AccesoDB();
                con.conexionDB();
                qrySentencia =	" SELECT NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION)"+
                		" FROM COMREL_PRODUCTO_EPO PE,COMCAT_PRODUCTO_NAFIN PN"+
                                " WHERE PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
                                " AND PE.IC_EPO = "+ic_epo+
                                " AND PN.IC_PRODUCTO_NAFIN = 4";
                rs = con.queryDB(qrySentencia);
                if(rs.next()){
			aux = rs.getString(1);
                }
                con.cierraStatement();
                if(!"N".equals(aux)){
                	qrySentencia = 	   " SELECT round(fn_valor_compra, 2) FROM COM_TIPO_CAMBIO " +
					   " WHERE ic_moneda = 54" +
					   " AND dc_fecha = (Select max(TC.dc_fecha) " +
					   " FROM COM_TIPO_CAMBIO TC " +
					   " WHERE TC.ic_moneda = 54)";
                        rs = con.queryDB(qrySentencia);
                        	if(rs.next())
                                	tipoCambio = rs.getString(1);
                	con.cierraStatement();
                }
        }catch(Exception e){
        	tipoCambio = "";
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                	con.cierraConexionDB();
                }
		System.out.println("CargaDocDistBean::getTipoCambio (S)");
        }
 return tipoCambio;
 }

public Combos getPlazos()
throws NafinException{
		System.out.println("CargaDocDistBean::getPlazos (E)");
		AccesoDB 	con = null;
		Combos 		comboPlazos = null;
		String    qrySentencia = "";
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =	" SELECT IN_PLAZO_DIAS,IN_PLAZO_DIAS FROM COMCAT_PLAZO --WHERE ic_plazo in (11)";
			comboPlazos = new Combos(qrySentencia,con);
		}	catch(Exception e){
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		System.out.println("CargaDocDistBean::getPlazos (S)");
		}
		return comboPlazos;
}

//Deprecated: Usar getLineas
public Combos getComboLineas(String ic_epo)
	throws NafinException{
		System.out.println("CargaDocDistBean::getComboLineas (E) Deprecated: Usar getLineas");
		AccesoDB 	con = null;
		Combos 		comboLineas = null;
		String    qrySentencia = "";
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =	" SELECT LCD.IC_LINEA_CREDITO_DM "+
							" ,DECODE(LCD.CG_NUM_CUENTA_EPO,'',LCD.CG_NUM_CUENTA_IF,LCD.CG_NUM_CUENTA_EPO)"+
							" ||' - '||I.CG_RAZON_SOCIAL,"+
							" (LCD.fn_monto_autorizado_total-LCD.fn_saldo_financiado-LCD.fn_monto_x_autorizar) as saldo_disponible"+
							" FROM DIS_LINEA_CREDITO_DM LCD"+
							" ,COMCAT_IF I"+
							" WHERE LCD.IC_IF = I.IC_IF"+
							" AND LCD.DF_VENCIMIENTO_ADICIONAL > SYSDATE"+
							" AND LCD.CG_TIPO_SOLICITUD IN('I','R')"+
							" AND LCD.IC_EPO = "+ic_epo+
							" AND (LCD.fn_monto_autorizado_total-LCD.fn_saldo_financiado-LCD.fn_monto_x_autorizar)>0"+
							" ORDER BY SALDO_DISPONIBLE DESC, IC_LINEA_CREDITO_DM";

			comboLineas = new Combos(qrySentencia,con);
		}	catch(Exception e){
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("CargaDocDistBean::getComboLineas (S)");
		}
		return comboLineas;
}

//SUSTITUYE A getComboLineas
public Vector getLineas(String ic_epo)
	throws NafinException{
		System.out.println("CargaDocDistBean::getLineas (E)");
		AccesoDB 	con = null;
		ResultSet	rs = null;
		Vector		vecFilas = new Vector();
		Vector		vecColumnas = new Vector();
		String		qrySentencia = "";
		try {
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =	" SELECT LCD.IC_LINEA_CREDITO_DM "+
					" ,LCD.IC_LINEA_CREDITO_DM"+
					" ||' - '||I.CG_RAZON_SOCIAL||DECODE(LCD.IC_MONEDA,1,' MONEDA NACIONAL',54,' DOLARES'),"+
					" (LCD.fn_monto_autorizado_total-LCD.fn_saldo_financiado-LCD.fn_monto_x_autorizar) as saldo_disponible"+
					" FROM DIS_LINEA_CREDITO_DM LCD"+
					" ,COMCAT_IF I"+
					" WHERE LCD.IC_IF = I.IC_IF"+
					" AND LCD.DF_VENCIMIENTO_ADICIONAL > SYSDATE"+
					" AND (LCD.fn_monto_autorizado_total-LCD.fn_saldo_financiado-LCD.fn_monto_x_autorizar)>0"+
					" AND LCD.CG_TIPO_SOLICITUD = 'I' "+
					" AND LCD.ic_estatus_linea = 12"+
					" AND LCD.IC_EPO = "+ic_epo+
					" ORDER BY SALDO_DISPONIBLE DESC,IC_LINEA_CREDITO_DM";
			rs = con.queryDB(qrySentencia);
			while(rs.next()){
				vecColumnas = new Vector();
				vecColumnas.add((rs.getString(1)==null)?"":rs.getString(1));
				vecColumnas.add((rs.getString(2)==null)?"":rs.getString(2));
				vecFilas.add(vecColumnas);
			}
			con.cierraStatement();
		}	catch(Exception e){
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		System.out.println("CargaDocDistBean::getLineas (S)");
		}
		return vecFilas;
}


public Vector monitorLineas(String ic_epo)
	throws NafinException{
	System.out.println("CargaDocDistBean::monitorLineas (E)");
	AccesoDB 	con = null;
	ResultSet	rs = null;
	String    	qrySentencia = "";
	Vector      vecFilas = new Vector();
	Vector 		vecColumnas = null;
	try {
		con = new AccesoDB();
		con.conexionDB();
		qrySentencia =	" SELECT LCD.IC_LINEA_CREDITO_DM "+
			" ,LCD.IC_LINEA_CREDITO_DM"+
			" ||' - '||I.CG_RAZON_SOCIAL||' '||decode(ic_moneda,1,'MONEDA NACIONAL',54,'DOLARES') as DESCRIPCION"+
			" ,LCD.FN_MONTO_AUTORIZADO_TOTAL"+
			" ,LCD.FN_SALDO_TOTAL"+
			" ,LCD.IC_MONEDA"+
			" ,LCD.FN_SALDO_FINANCIADO"+
			" ,LCD.FN_MONTO_X_AUTORIZAR"+
			" ,(LCD.fn_monto_autorizado_total-LCD.fn_saldo_financiado-LCD.fn_monto_x_autorizar) AS SALDO_DISPONIBLE"+
			" ,LCD.FN_MONTO_NEGOCIABLE"+
			" ,(LCD.fn_monto_autorizado_total-LCD.fn_saldo_financiado-LCD.fn_monto_x_autorizar)-LCD.fn_monto_negociable as SALDO_POTENCIAL"+
			" FROM DIS_LINEA_CREDITO_DM LCD"+
			" ,COMCAT_IF I"+
			" WHERE LCD.IC_IF = I.IC_IF"+
			" AND LCD.DF_VENCIMIENTO_ADICIONAL > SYSDATE"+
			" AND LCD.CG_TIPO_SOLICITUD IN('I')"+
			" AND LCD.IC_EPO = "+ic_epo+
			" AND LCD.IC_PRODUCTO_NAFIN = 4"+
			" AND LCD.IC_ESTATUS_LINEA = 12"+
			" ORDER BY SALDO_DISPONIBLE DESC,LCD.IC_LINEA_CREDITO_DM";
		rs = con.queryDB(qrySentencia);
		while(rs.next()){
			vecColumnas = new Vector();
/*0*/		vecColumnas.add((rs.getString("IC_LINEA_CREDITO_DM")==null)?"":rs.getString("IC_LINEA_CREDITO_DM"));
/*1*/		vecColumnas.add((rs.getString("DESCRIPCION")==null)?"":rs.getString("DESCRIPCION"));
/*2*/		vecColumnas.add((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
/*3*/		vecColumnas.add((rs.getString("FN_SALDO_TOTAL")==null)?"":rs.getString("FN_SALDO_TOTAL"));
/*4*/		vecColumnas.add((rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"));
/*5*/		vecColumnas.add((rs.getString("FN_SALDO_FINANCIADO")==null)?"":rs.getString("FN_SALDO_FINANCIADO"));
/*6*/		vecColumnas.add((rs.getString("FN_MONTO_X_AUTORIZAR")==null)?"":rs.getString("FN_MONTO_X_AUTORIZAR"));
/*7*/		vecColumnas.add((rs.getString("SALDO_DISPONIBLE")==null)?"":rs.getString("SALDO_DISPONIBLE"));
/*8*/		vecColumnas.add((rs.getString("FN_MONTO_NEGOCIABLE")==null)?"":rs.getString("FN_MONTO_NEGOCIABLE"));
/*9*/		vecColumnas.add((rs.getString("SALDO_POTENCIAL")==null)?"":rs.getString("SALDO_POTENCIAL"));
			vecFilas.add(vecColumnas);
		}
			con.cierraStatement();
	}	catch(Exception e){
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())
			con.cierraConexionDB();
		System.out.println("CargaDocDistBean::monitorLineas (S)");
	}
	return vecFilas;
}


public void actualizaDetalle(String ic_epo,String ic_proc_docto,String lineaTodos,String chkTodos,String []lineaReg,String []plazo,String []fecha_vto,String []igNumeroDocto)
throws NafinException{
	System.out.println("CargaDocDistBean::actualizaDetalle (E)");
	AccesoDB 	con = null;
	String      qrySentencia = "";
    boolean		ok = true;
    try{
    	con = new AccesoDB();
        con.conexionDB();
        for(int i=0;i<igNumeroDocto.length;i++){
	       	qrySentencia = 	" update distmp_proc_docto"+
        			" set ic_linea_credito_dm = "+ (("T".equals(chkTodos))?lineaTodos:lineaReg[i]);
                if(!"".equals(plazo[i])&&plazo[i]!=null)
                            qrySentencia += " ,ig_plazo_credito = "+plazo[i];
		if(!"".equals(fecha_vto[i])&&fecha_vto[i]!=null)
                            qrySentencia += " ,df_fecha_venc_credito = to_date('"+fecha_vto[i]+"','dd/mm/yyyy')";
                qrySentencia += " where ic_proc_docto = "+ic_proc_docto+
                            " and ig_numero_docto = '"+igNumeroDocto[i]+"'";
                System.out.println("ejecutando "+qrySentencia);
        	con.ejecutaSQL(qrySentencia);
        }

    }catch(Exception e){
    	ok = false;
    	throw new NafinException("SIST0001");
    }finally{
    	if(con.hayConexionAbierta()){
        	con.terminaTransaccion(ok);
            con.cierraConexionDB();
        }
	System.out.println("CargaDocDistBean::actualizaDetalle (S)");
    }

}

public String confirmaCarga(String ic_epo,String ic_proc_docto,String ic_usuario,String totalDoctoMN,String totalDoctoUSD,String totalMontoMN,String totalMontoUSD,String reciboElectronico, String limiteLineaCredito)
throws NafinException{
	System.out.println("CargaDocDistBean::confirmaCarga (E)");
    AccesoDB 	con = null;
    String      qrySentencia = "";
    boolean	ok = true;
    Acuse       acuse = null;
    String 	ic_documento = "";
		double monto_comprometidoTotal = 0;
		double saldo_total =0;
		double monto_comprometido =Double.parseDouble((String)totalMontoMN);
		String      qrySentencia2 = "";
		PreparedStatement ps = null;

    try{
    	con = new AccesoDB();
        con.conexionDB();
        acuse = new Acuse(1,"4","com",con);
		creaAcuse(acuse.toString(),ic_usuario,totalDoctoMN,totalDoctoUSD,totalMontoMN,totalMontoUSD,reciboElectronico,con);
        ic_documento = getClaveDocto(con);
		qrySentencia =	" insert into dis_documento"+
        		"(ic_documento,ic_epo,ic_pyme,ic_producto_nafin,cc_acuse,ig_numero_docto"+
                        " ,df_fecha_emision,df_fecha_venc,ic_moneda,ct_referencia,fn_porc_descuento"+
                        " ,ig_plazo_descuento,fn_monto,ic_tipo_financiamiento,ic_estatus_docto"+
                        " ,ic_linea_credito_dm,ig_plazo_credito,df_fecha_venc_credito,ig_plazo_docto"+
                        " ,cg_campo1,cg_campo2,cg_campo3,cg_campo4,cg_campo5,CS_PLAZO_DESCUENTO,ic_clasificacion, CG_VENTACARTERA, ig_tipo_pago, IG_MESES_INTERES )"+
        		" SELECT "+ic_documento+"+ic_consecutivo,ic_epo,ic_pyme,ic_producto_nafin,'"+acuse.toString()+"',ig_numero_docto"+
                        " ,df_fecha_emision,df_fecha_venc,ic_moneda,ct_referencia,fn_porc_descuento"+
                        " ,ig_plazo_descuento,fn_monto,ic_tipo_financiamiento,ic_estatus_docto"+
                        " ,ic_linea_credito_dm,ig_plazo_credito,df_fecha_venc_credito,ROUND(df_fecha_venc-df_fecha_emision,0)"+
                        " ,cg_campo1,cg_campo2,cg_campo3,cg_campo4,cg_campo5,CS_PLAZO_DESCUENTO,ic_clasificacion,CG_VENTACARTERA, ig_tipo_pago, IG_MESES_INTERES  "+
                        " from distmp_proc_docto where ic_proc_docto = "+ic_proc_docto+
                        " and ic_epo = "+ic_epo;
    	con.ejecutaSQL(qrySentencia);

		 if( limiteLineaCredito.equals("S")) {
				 String saldo= 	validaMontoComprometido( ic_epo);
				 saldo_total =  Double.parseDouble((String)saldo);

				monto_comprometidoTotal = saldo_total + monto_comprometido;

				qrySentencia2 ="	update DIS_LINEA_CREDITO_DM    "+
													" set FN_SALDO_COMPROMETIDO = "+monto_comprometidoTotal+
													"  WHERE  ic_epo =  "+ic_epo+
													" AND ic_producto_nafin = 4 "+
													" AND df_vencimiento_adicional > SYSDATE  "+
													" AND cg_tipo_solicitud = 'I' "+
													" AND ic_estatus_linea = 12 ";

					log.debug("monto Coprometido::: "+qrySentencia2);

						ps = con.queryPrecompilado(qrySentencia2);
						ps.executeUpdate();
						ps.close();
		 }//if( limiteLineaCredito.equals("S")) {

        //String  acuseFormateado = acuse.formatear();
    }catch(Exception e){
    	ok = false;
    	throw new NafinException("SIST0001");
    }finally{
    	if(con.hayConexionAbierta()){
        	con.terminaTransaccion(ok);
            	con.cierraConexionDB();
        }
	System.out.println("CargaDocDistBean::confirmaCarga (S)");
    }
    return acuse.toString();
}

private void creaAcuse(String acuse,String ic_usuario, AccesoDB con)
throws NafinException{
	System.out.println("CargaDocDistBean::creaAcuse (E)");
    try{
		creaAcuse(acuse, ic_usuario, "0", "0", "0", "0", "NULL", con);
    }catch(Exception e){

    	throw new NafinException("SIST0001");
    }
	System.out.println("CargaDocDistBean::creaAcuse (S)");
}

private void creaAcuse(String acuse,String ic_usuario,String totalDoctoMN,String totalDoctoUSD,String totalMontoMN,String totalMontoUSD,String reciboElectronico,AccesoDB con)
throws NafinException{
	System.out.println("CargaDocDistBean::creaAcuse (E)");
    String      qrySentencia = "";
    boolean	ok = true;
    try{
       	qrySentencia = 	" insert into com_acuse1"+
        		"(cc_acuse,in_total_docto_mn,fn_total_monto_mn,in_total_docto_dl,fn_total_monto_dl"+
                        " ,df_fechahora_carga,ic_usuario,cg_recibo_electronico,ic_producto_nafin)"+
                        "values('"+acuse+"',"+totalDoctoMN+","+totalMontoMN+","+totalDoctoUSD+","+totalMontoUSD+
                        " ,sysdate,'"+ic_usuario+"','"+reciboElectronico+"',4)";
	con.ejecutaSQL(qrySentencia);
    }catch(Exception e){
    	ok = false;
    	throw new NafinException("SIST0001");
    }
	System.out.println("CargaDocDistBean::creaAcuse (S)");
}

public String confirmaPagoAnt(
	String totalPagos,
	String totalMonto,
	String ic_usuario,
	String reciboElectronico,
	String txt_ic_epo[],
	String txt_ic_pyme[],
	String txt_importe[],
	String txt_fec_aplicacion[],
	String chk_aplica[])
throws NafinException{
	System.out.println("CargaDocDistBean::confirmaPagoAnt (E)");
    AccesoDB 	con = null;
    String      qrySentencia = "";
    boolean		ok = true;
    Acuse       acuse = null;
    String 		ic_pago = "", acuseFormateado ="";
    int registros = Integer.parseInt(totalPagos);


    try{
    	con = new AccesoDB();
        con.conexionDB();
        acuse = new Acuse(1,"4","com",con);
		insertaAcusePago(acuse.toString(),ic_usuario,totalPagos,totalMonto,reciboElectronico,con);
        for(int j=0;j<registros;j++){
        	ic_pago = getClavePago(con);
			qrySentencia =	"insert into dis_pagos_ant "+
        					"(ic_pago,df_registro,ic_epo,ic_pyme,ic_moneda,fn_importe,"+
        					"df_fecha_aplicacion,cs_dispersion,cc_acuse) values ("+
        					ic_pago+",sysdate,"+txt_ic_epo[j]+","+txt_ic_pyme[j]+",1,"+
        					txt_importe[j]+",to_date('"+txt_fec_aplicacion[j]+
        					"','dd/mm/yyyy'),'N','"+acuse.toString()+"')";
        	System.out.println("qrySentencia: "+qrySentencia);
    		con.ejecutaSQL(qrySentencia);
    	}
        acuseFormateado = acuse.formatear();
    }catch(Exception e){
    	ok = false;
    	throw new NafinException("SIST0001");
    }finally{
    	if(con.hayConexionAbierta()){
        	con.terminaTransaccion(ok);
            	con.cierraConexionDB();
        }
	System.out.println("CargaDocDistBean::confirmaPagoAnt (S)");
    }
    return acuse.toString();
}

private void insertaAcusePago(
	String  acuse,
	String	icUsuario,
	String	totalPagos,
   	String	totalMonto,
	String	cgReciboElectronico,
    AccesoDB con)
   throws NafinException{
		System.out.println("CargaDocDistBean::insertaAcusePago (E)");
        String qrySentencia = "";
   	try{
		qrySentencia =	" INSERT INTO COM_ACUSE2"+
                		" (cc_acuse,fn_total_monto_mn,ic_usuario,df_fecha_hora,"+
        				" cg_recibo_electronico,ic_producto_nafin,in_total_docto_mn)"+
                        " values('"+acuse+"',"+totalMonto+",'"+icUsuario+"',sysdate,'"+
                        cgReciboElectronico+"',4,"+totalPagos+")";
       	System.out.println("qrySentencia: "+qrySentencia);
        con.ejecutaSQL(qrySentencia);
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }
		System.out.println("CargaDocDistBean::insertaAcusePago (S)");
   }

public Vector consultaPagosAnt(String cc_acuse)
   throws NafinException{
	System.out.println("CargaDocDistBean::consultaPagosAnt (E)");
   		AccesoDB con = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas = new Vector();
        Vector	vecColumnas = null;

   		try{
        	con = new AccesoDB();
            con.conexionDB();
			qrySentencia =  " select e.cg_razon_social as EPO,p.cg_razon_social as PYME, "+
							" dpa.fn_importe as IMPORTE, "+
							" to_char(dpa.df_fecha_aplicacion,'dd/mm/yyyy') as FEC_APLICACION, "+
    						" to_char(dpa.df_registro,'dd/mm/yyyy') as FEC_RECEPCION,"+
    						" to_char(dpa.df_registro,'hh:mi am') as HOR_RECEPCION"+
							" from comcat_pyme p, comcat_epo e, dis_pagos_ant dpa "+
							" where e.ic_epo = dpa.ic_epo "+
    						" and p.ic_pyme = dpa.ic_pyme "+
							" and dpa.cc_acuse = '"+cc_acuse+"'"+
							" order by dpa.ic_pago"  ;
			System.out.println(qrySentencia);
            rs = con.queryDB(qrySentencia);
            while(rs.next()){
                vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*1*/			vecColumnas.add((rs.getString("PYME")==null)?"":rs.getString("PYME"));
/*2*/			vecColumnas.add((rs.getString("IMPORTE")==null)?"":rs.getString("IMPORTE"));
/*3*/			vecColumnas.add((rs.getString("FEC_APLICACION")==null)?"":rs.getString("FEC_APLICACION"));
/*4*/			vecColumnas.add((rs.getString("FEC_RECEPCION")==null)?"":rs.getString("FEC_RECEPCION"));
/*5*/			vecColumnas.add((rs.getString("HOR_RECEPCION")==null)?"":rs.getString("HOR_RECEPCION"));
	String acuseFormateado =  cc_acuse.substring(0,1)+"-"+cc_acuse.substring(1,2)+"-"+cc_acuse.substring(2,8)+"-"+cc_acuse.substring(8,10)+"-"+cc_acuse.substring(10,15);
/*6*/			vecColumnas.add(acuseFormateado);

                vecFilas.add(vecColumnas);
            }
            con.cierraStatement();
        }catch(Exception e){
		System.out.println("Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("CargaDocDistBean::consultaPagosAnt (S)");
        }
   return vecFilas;
   }

public boolean operaConversion(String ic_epo)
	throws NafinException{
	System.out.println("CargaDocDistBean::operaConversion (E)");
   	AccesoDB con = null;
	ResultSet rs = null;
	String qrySentencia = "";
	boolean retorno = false;
   	try{
        	con = new AccesoDB();
                con.conexionDB();
                qrySentencia = 	" select nvl(PE.cg_tipo_conversion,PN.cg_tipo_conversion)"+
					"	,nvl(PE.cg_tipos_credito,PN.cg_tipos_credito)"+
					" from comrel_producto_epo PE,comcat_producto_nafin PN"+
					" where PE.ic_producto_nafin = PN.ic_producto_nafin"+
					" and PE.ic_producto_nafin = 4"+
					" and PN.ic_producto_nafin = 4"+
					" and PE.ic_epo = "+ic_epo;
                rs = con.queryDB(qrySentencia);
                while(rs.next()){
					retorno = ("P".equals(rs.getString(1))&&("A".equals(rs.getString(2))||"D".equals(rs.getString(2))));
                }
				con.cierraStatement();
        }catch(SQLException e){
		System.out.println("CargaDocDist::operaConversion SQLExcepcion \n"+qrySentencia);
                throw new NafinException("SIST0001");
        }catch(Exception e){
		System.out.println("CargaDocDist::operaConversion Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("CargaDocDistBean::operaConversion (S)");
        }
return retorno;
}



public Vector getTipoFinanciamientoXEpo(String ic_epo,String ic_pyme,String selected)
	throws NafinException{
	System.out.println("CargaDocDistBean::getTipoFinanciamientoXEpo (E)");
   	AccesoDB con = null;
	ResultSet rs = null;
	String  qrySentencia = "";
	Vector	vecFilas = new Vector();
	Vector	vecColumnas = null;
   	try{
        	con = new AccesoDB();
            con.conexionDB();
           /* qrySentencia = 	" select 1 "+
							"	from comrel_pyme_epo pe, "+
							"		 comrel_pyme_epo_x_producto pep, "+
							"	     comrel_epo_tipo_financiamiento etf "+
  							"	where   pe.ic_epo = pep.ic_epo "+
							"	    and pe.ic_pyme = pep.ic_pyme "+
							"	    and pe.ic_epo = etf.ic_epo "+
							"	    and pep.ic_producto_nafin = etf.ic_producto_nafin "+
							"	    and pep.ic_epo = "+ ic_epo +
							"	    and pe.cg_num_distribuidor = '"+ ic_pyme +"'"+
							"	    and pep.ic_producto_nafin = 4 "+
							"	    and pep.cg_tipo_credito = 'C' "+
							"	    and etf.ic_tipo_financiamiento = 5 ";
			System.out.println("\n qrySentencia: "+qrySentencia);
            rs = con.queryDB(qrySentencia);
            if(rs.next())
            	esParametrizado = "S";
            else
            	esParametrizado = "N";
					*/
            qrySentencia = 	"  select TF.ic_tipo_financiamiento"   +
				"  	,TF.cd_descripcion"   +
				" 	,decode(ETF.ic_tipo_financiamiento,null,'','selected')"   +
				"  from comcat_tipo_financiamiento TF"   +
				"  	  ,comrel_epo_tipo_financiamiento ETF ";
			//	"  where ETF.ic_tipo_financiamiento in(1,2,3,4";
			//if ("S".equals(esParametrizado))
				//qrySentencia += ",5";
			qrySentencia +=  " where tf.ic_tipo_financiamiento = etf.ic_tipo_financiamiento"   +
				"  and etf.ic_producto_nafin = 4"   +
				"  and etf.ic_epo = "+ic_epo;
			
			if(!"".equals(selected) ) {
				qrySentencia +=  " AND   etf.ic_tipo_financiamiento =  "+selected;
			}
			
			System.out.println("\n qrySentencia: "+qrySentencia);
            rs = con.queryDB(qrySentencia);
            while(rs.next()){
                	vecColumnas = new Vector();
/*0*/                        vecColumnas.add((rs.getString(1)==null)?"":rs.getString(1));
/*1*/                        vecColumnas.add((rs.getString(2)==null)?"":rs.getString(2));
			     if(!"".equals(selected)&&selected!=null)
/*2*/	                        vecColumnas.add(selected.equals(rs.getString(1).toString())?"selected":"");
			     else
/*2*/	                        vecColumnas.add((rs.getString(3)==null)?"":rs.getString(3));
			vecFilas.add(vecColumnas);
                }
        }catch(SQLException e){
		System.out.println("CargaDocDist::getTipoFinanciamientoXEpo SQLExcepcion \n"+qrySentencia);
                throw new NafinException("SIST0001");
        }catch(Exception e){
		System.out.println("CargaDocDist::getTipoFinanciamientoXEpo Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("CargaDocDistBean::getTipoFinanciamientoXEPO (S)");
        }
return vecFilas;
}

public Vector getParametrosClasific(String ic_epo,String ic_pyme, String df_fecha_emision, String cg_fecha_porc_desc, String ic_clasificacion)
	throws NafinException{
	System.out.println("CargaDocDistBean::getParametrosClasific (E)");
	AccesoDB con = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	String qrySentencia = "";
	Vector	vecFilas = new Vector();
	String		arreglo[]	= new String[2];
	try{
		con = new AccesoDB();
		con.conexionDB();
		if("".equals(ic_clasificacion)) {
		    arreglo = validaRelPymeEpo(ic_epo,ic_pyme,con);
			ic_pyme = arreglo[0];
			if("E".equals(cg_fecha_porc_desc)) {
				qrySentencia =
					" SELECT MIN (ic_plazo_descuento), ig_plazo_final, fg_porcentaje, 'C'"   +
					"   FROM comrel_pyme_epo_x_producto pep, dis_plazo_descuento pds"   +
					"  WHERE pep.ic_clasificacion = pds.ic_clasificacion"   +
					"    AND pep.ic_pyme = ?"   +
					"    AND pep.ic_epo = ?"   +
					"    AND pep.ic_producto_nafin = 4"   +
					"    AND ((TRUNC (SYSDATE) - TRUNC (TO_DATE (?, 'dd/mm/yyyy'))) + 1) BETWEEN"   +
					"           ig_plazo_inicial AND ig_plazo_final"   +
					"  GROUP BY ig_plazo_final,  fg_porcentaje"   +
					" UNION ALL "   +
					" SELECT ic_plazo_descuento, ig_plazo_final, fg_porcentaje, 'C'"   +
					"   FROM comrel_pyme_epo_x_producto pep, dis_plazo_descuento pds"   +
					"  WHERE pep.ic_clasificacion = pds.ic_clasificacion"   +
					"    AND pep.ic_pyme = ?"   +
					"    AND pep.ic_epo = ?"   +
					"    AND pep.ic_producto_nafin = 4"   +
					"    AND pds.ic_plazo_descuento IN (SELECT MAX (ic_plazo_descuento)"   +
					"                                     FROM dis_plazo_descuento pd2"   +
					"                                    WHERE pds.ic_clasificacion = pd2.ic_clasificacion)"  ;
			}
			else {
				if("P".equals(cg_fecha_porc_desc)) {
					qrySentencia =
						" SELECT MIN (ic_plazo_descuento), ig_plazo_final, fg_porcentaje, 'C'"   +
						"   FROM comrel_pyme_epo_x_producto pep, dis_plazo_descuento pds"   +
						"  WHERE pep.ic_clasificacion = pds.ic_clasificacion"   +
						"    AND pep.ic_pyme = ? "   +
						"    AND pep.ic_epo = ?"   +
						"    AND pep.ic_producto_nafin = 4"   +
						"    AND pds.ig_plazo_inicial = 1"   +
						"  GROUP BY ig_plazo_final,  fg_porcentaje"  ;
				}
			}
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_pyme));
			ps.setInt(2,Integer.parseInt(ic_epo));
			if("E".equals(cg_fecha_porc_desc)) {
				ps.setString(3,df_fecha_emision);
				ps.setInt(4,Integer.parseInt(ic_pyme));
				ps.setInt(5,Integer.parseInt(ic_epo));
			}

		}
		else {
			if("E".equals(cg_fecha_porc_desc)) {
				qrySentencia =
					" SELECT ic_plazo_descuento, ig_plazo_final, fg_porcentaje, 'C'"   +
					"   FROM dis_plazo_descuento pds"   +
					"  WHERE pds.ic_clasificacion = ?"   +
					"    AND ((TRUNC (SYSDATE) - TRUNC (TO_DATE (?, 'dd/mm/yyyy'))) + 1) BETWEEN"   +
					"           ig_plazo_inicial AND ig_plazo_final"   +
					" UNION ALL"   +
					" SELECT pds.ic_plazo_descuento, pds.ig_plazo_final, pds.fg_porcentaje, 'C'"   +
					"   FROM dis_plazo_descuento pds"   +
					"  WHERE pds.ic_clasificacion = ?"   +
					"    AND pds.ic_plazo_descuento IN (SELECT MAX (pd2.ic_plazo_descuento)"   +
					"                                     FROM dis_plazo_descuento pd2"   +
					"                                    WHERE pds.ic_clasificacion = pd2.ic_clasificacion)"  ;
			}
			else {
				if("P".equals(cg_fecha_porc_desc)) {
					qrySentencia =
						" SELECT MIN (ic_plazo_descuento), ig_plazo_final, fg_porcentaje, 'C'"   +
						"   FROM dis_plazo_descuento pds"   +
						"  WHERE pds.ic_clasificacion = ?"   +
						"    AND pds.ig_plazo_inicial = 1"   +
						"  GROUP BY ig_plazo_final,  fg_porcentaje"  ;
				}
			}
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_clasificacion));
			if("E".equals(cg_fecha_porc_desc)) {
				ps.setString(2,df_fecha_emision);
				ps.setInt(3,Integer.parseInt(ic_clasificacion));
			}

		}
System.out.println("\n ********************* \n qrySentencia: "+qrySentencia);
		rs = ps.executeQuery();
		if(rs.next()){
/*0*/		vecFilas.add((rs.getString(2)==null)?"":rs.getString(2));
/*1*/		vecFilas.add((rs.getString(3)==null)?"":rs.getString(3));
/*2*/		vecFilas.add((rs.getString(4)==null)?"":rs.getString(4));
			if("E".equals(cg_fecha_porc_desc) && rs.next()) {
/*3*/			vecFilas.add((rs.getString(2)==null)?"":rs.getString(2));
/*4*/			vecFilas.add((rs.getString(3)==null)?"":rs.getString(3));
/*5*/			vecFilas.add((rs.getString(4)==null)?"":rs.getString(4));
			}
		}
		else {
			rs.close();ps.close();
			qrySentencia =
				" SELECT ig_plazo_dscto, fn_porcentaje_dscto, 'G'"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_epo = ?"   +
				"    AND ic_producto_nafin = 4"  ;
System.out.println("\n ********************* \n qrySentencia: "+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			if(rs.next()){
/*0*/			vecFilas.add((rs.getString(1)==null)?"":rs.getString(1));
/*1*/			vecFilas.add((rs.getString(2)==null)?"":rs.getString(2));
/*2*/			vecFilas.add((rs.getString(3)==null)?"":rs.getString(3));
			}
		}
		rs.close();ps.close();
	}catch(SQLException e){
		System.out.println("CargaDocDist::getParametrosClasific SQLExcepcion \n"+qrySentencia);
		throw new NafinException("SIST0001");
	}catch(Exception e){
		System.out.println("CargaDocDist::getParametrosClasific Excepcion "+e);
		throw new NafinException("SIST0001");
	}finally{
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
		System.out.println("CargaDocDistBean::getParametrosClasific (S)");
	}
	return vecFilas;
}


    private Vector getParametrosClasific(String ic_epo,String ic_pyme, String df_fecha_emision, String cg_fecha_porc_desc, String ic_clasificacion,AccesoDB con) throws NafinException {
        log.info("CargaDocDistBean::Private getParametrosClasific (E)");
        //AccesoDB con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        Vector	vecFilas = new Vector();
        try {
            //con = new AccesoDB();
            //con.conexionDB();
            if("NULL".equals(ic_clasificacion))
            ic_clasificacion = "";
            if("".equals(ic_clasificacion)) {
    			if("E".equals(cg_fecha_porc_desc)) {
        			qrySentencia =
                        " SELECT MIN (ic_plazo_descuento), ig_plazo_final, fg_porcentaje, 'C'"   +
                        "   FROM comrel_pyme_epo_x_producto pep, dis_plazo_descuento pds"   +
                        "  WHERE pep.ic_clasificacion = pds.ic_clasificacion"   +
                        "    AND pep.ic_pyme = ?"   +
                        "    AND pep.ic_epo = ?"   +
                        "    AND pep.ic_producto_nafin = ?"   +
                        "    AND ((TRUNC (SYSDATE) - TRUNC (TO_DATE (?, 'dd/mm/yyyy'))) + ?) BETWEEN"   +
                        "           ig_plazo_inicial AND ig_plazo_final"   +
                        "  GROUP BY ig_plazo_final,  fg_porcentaje"   +
                        " UNION ALL "   +
                        " SELECT ic_plazo_descuento, ig_plazo_final, fg_porcentaje, 'C'"   +
                        "   FROM comrel_pyme_epo_x_producto pep, dis_plazo_descuento pds"   +
                        "  WHERE pep.ic_clasificacion = pds.ic_clasificacion"   +
                        "    AND pep.ic_pyme = ?"   +
                        "    AND pep.ic_epo = ?"   +
                        "    AND pep.ic_producto_nafin = ?"   +
                        "    AND pds.ic_plazo_descuento IN (SELECT MAX (ic_plazo_descuento)"   +
                        "                                     FROM dis_plazo_descuento pd2"   +
                        "                                    WHERE pds.ic_clasificacion = pd2.ic_clasificacion)"  ;
			}
			else {
				if("P".equals(cg_fecha_porc_desc)) {
					qrySentencia =
						" SELECT MIN (ic_plazo_descuento), ig_plazo_final, fg_porcentaje, 'C'"   +
						"   FROM comrel_pyme_epo_x_producto pep, dis_plazo_descuento pds"   +
						"  WHERE pep.ic_clasificacion = pds.ic_clasificacion"   +
						"    AND pep.ic_pyme = ? "   +
						"    AND pep.ic_epo = ?"   +
						"    AND pep.ic_producto_nafin = ?"   +
						"    AND pds.ig_plazo_inicial = ?"   +
						"  GROUP BY ig_plazo_final,  fg_porcentaje"  ;
				}
			}
            log.debug("qrySentencia:"+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			if("E".equals(cg_fecha_porc_desc)) {
			    ps.setInt(1,Integer.parseInt(ic_pyme));
			    ps.setInt(2,Integer.parseInt(ic_epo));
			    ps.setInt(3,Integer.parseInt("4"));
				ps.setString(4,df_fecha_emision);
			    ps.setInt(5,Integer.parseInt("1"));
				ps.setInt(6,Integer.parseInt(ic_pyme));
				ps.setInt(7,Integer.parseInt(ic_epo));
			    ps.setInt(8,Integer.parseInt("4"));
			} else if("P".equals(cg_fecha_porc_desc)) {
                ps.setInt(1,Integer.parseInt(ic_pyme));
                ps.setInt(2,Integer.parseInt(ic_epo));
                ps.setInt(3,Integer.parseInt("4"));
                ps.setInt(4,Integer.parseInt("1"));
            }
		}
		else {
			if("E".equals(cg_fecha_porc_desc)) {
				qrySentencia =
					" SELECT ic_plazo_descuento, ig_plazo_final, fg_porcentaje, 'C'"   +
					"   FROM dis_plazo_descuento pds"   +
					"  WHERE pds.ic_clasificacion = ?"   +
					"    AND ((TRUNC (SYSDATE) - TRUNC (TO_DATE (?, 'dd/mm/yyyy'))) + 1) BETWEEN"   +
					"           ig_plazo_inicial AND ig_plazo_final"   +
					" UNION ALL"   +
					" SELECT pds.ic_plazo_descuento, pds.ig_plazo_final, pds.fg_porcentaje, 'C'"   +
					"   FROM dis_plazo_descuento pds"   +
					"  WHERE pds.ic_clasificacion = ?"   +
					"    AND pds.ic_plazo_descuento IN (SELECT MAX (pd2.ic_plazo_descuento)"   +
					"                                     FROM dis_plazo_descuento pd2"   +
					"                                    WHERE pds.ic_clasificacion = pd2.ic_clasificacion)"  ;
			}
			else {
				if("P".equals(cg_fecha_porc_desc)) {
					qrySentencia =
						" SELECT MIN (ic_plazo_descuento), ig_plazo_final, fg_porcentaje, 'C'"   +
						"   FROM comrel_pyme_epo_x_producto pep, dis_plazo_descuento pds"   +
						"  WHERE pep.ic_clasificacion = pds.ic_clasificacion"   +
						"    AND pds.ic_clasificacion = ? "   +
						"    AND pep.ic_producto_nafin = ?"   +
						"    AND pds.ig_plazo_inicial = ?"   +
						"  GROUP BY ig_plazo_final,  fg_porcentaje"  ;
				}
			}
		    log.debug("qrySentencia:"+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			if("E".equals(cg_fecha_porc_desc)) {
			    ps.setInt(1,Integer.parseInt(ic_clasificacion));
				ps.setString(2,df_fecha_emision);
				ps.setInt(3,Integer.parseInt(ic_clasificacion));
			} else if("P".equals(cg_fecha_porc_desc)) {
                ps.setInt(1,Integer.parseInt(ic_clasificacion));
                ps.setInt(2,Integer.parseInt("4"));
                ps.setInt(3,Integer.parseInt("1"));
            }
		}
		rs = ps.executeQuery();
	    ps.clearParameters();
		if(rs.next()){
/*0*/		vecFilas.add((rs.getString(2)==null)?"":rs.getString(2));
/*1*/		vecFilas.add((rs.getString(3)==null)?"":rs.getString(3));
/*2*/		vecFilas.add((rs.getString(4)==null)?"":rs.getString(4));
			if("E".equals(cg_fecha_porc_desc) && rs.next()) {
/*3*/			vecFilas.add((rs.getString(2)==null)?"":rs.getString(2));
/*4*/			vecFilas.add((rs.getString(3)==null)?"":rs.getString(3));
/*5*/			vecFilas.add((rs.getString(4)==null)?"":rs.getString(4));
			}
		}
		else {
			rs.close();ps.close();
			qrySentencia =
				" SELECT ig_plazo_dscto, fn_porcentaje_dscto, 'G'"   +
				"   FROM comrel_producto_epo"   +
				"  WHERE ic_epo = ?"   +
				"    AND ic_producto_nafin = ?"  ;
		    log.debug("qrySentencia:"+qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1,Integer.parseInt(ic_epo));
		    ps.setInt(2,Integer.parseInt("4"));
			rs = ps.executeQuery();
		    ps.clearParameters();
			if(rs.next()){
/*0*/			vecFilas.add((rs.getString(1)==null)?"":rs.getString(1));
/*1*/			vecFilas.add((rs.getString(2)==null)?"":rs.getString(2));
/*2*/			vecFilas.add((rs.getString(3)==null)?"":rs.getString(3));
			}
		}
		rs.close();ps.close();
	}catch(SQLException e){
		log.error("CargaDocDist::Private getParametrosClasific SQLExcepcion \n"+qrySentencia);
		throw new NafinException("SIST0001");
	}catch(Exception e){
		log.error("CargaDocDist::Private getParametrosClasific Excepcion "+e);
		throw new NafinException("SIST0001");
	}finally{
		//if(con.hayConexionAbierta()){			con.cierraConexionDB();		}
		log.info("CargaDocDistBean::Private getParametrosClasific (S)");
	}
	return vecFilas;
}

    private String validaFechaLimite(String df_fecha_emision,String ig_plazo_dscto,String fecha_lim_dscto, String cg_fecha_porc_desc, AccesoDB con) throws NafinException {
        log.info("CargaDocDistBean::validaFechaLimite(E)");
        String dia_		= "";
        String mes_		= "";
        String anyo_	= "";
        int plazo_aux = 0;
        String plazo_dscto = ig_plazo_dscto;
        //PreparedStatement ps = null;		ResultSet rs = null;	String qrySentencia = "";
        try {
            Calendar aux_fechaEmision = new GregorianCalendar();
            Calendar aux_fechaLimite = new GregorianCalendar();
            String fechaAuto = "";
            if("E".equals(cg_fecha_porc_desc))
                fechaAuto = df_fecha_emision;
            else
                if("P".equals(cg_fecha_porc_desc))
            fechaAuto = (new SimpleDateFormat("dd/MM/yyyy")).format(new java.util.Date());
            
            if(!"".equals(fecha_lim_dscto)) {
                dia_	= fechaAuto.substring(0, 2);
                mes_	= fechaAuto.substring(3, 5);
                anyo_	= fechaAuto.substring(6, 10);
                aux_fechaEmision.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));
                dia_	= fecha_lim_dscto.substring(0, 2);
                mes_	= fecha_lim_dscto.substring(3, 5);
                anyo_	= fecha_lim_dscto.substring(6, 10);
                aux_fechaLimite.set(Integer.parseInt(anyo_), Integer.parseInt(mes_)-1, Integer.parseInt(dia_));
                java.util.Date FEMISION = aux_fechaEmision.getTime();
                java.util.Date FLIMITE = aux_fechaLimite.getTime();
                long resta= FLIMITE.getTime() - FEMISION.getTime();
                double division = (double)resta/(double)86400000;
                plazo_aux = (int)Math.round(division);
            }
            if("".equals(plazo_dscto))
                plazo_dscto = new Integer(plazo_aux).toString();
            else {
                if((Integer.parseInt(plazo_dscto) != plazo_aux) && !"".equals(fecha_lim_dscto))
                    throw new NafinException("DIST0031");
            }
        } finally {
            //if(con.hayConexionAbierta()){			con.cierraConexionDB();		}
            log.info("CargaDocDistBean::validaFechaLimite(S)");
        }
        return plazo_dscto;
    }

        public String getParamFechaPorc(String ic_epo) throws NafinException {
            log.info("CargaDocDistBean::getParamFechaPorc(E)");
            AccesoDB con = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            String qrySentencia = "";
            String cg_fecha_porc_desc = "";
            try {
                con = new AccesoDB();
                con.conexionDB();
                qrySentencia =
                    " SELECT cg_fecha_porc_desc"   +
                    "   FROM comrel_producto_epo"   +
                    "  WHERE ic_producto_nafin = ?"   +
                    "    AND ic_epo = ?"  ;
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1,Integer.parseInt("4"));
                ps.setInt(2,Integer.parseInt(ic_epo));
                rs = ps.executeQuery();
                ps.clearParameters();
                if(rs.next())
                    cg_fecha_porc_desc = (rs.getString(1)==null)?"":rs.getString(1);
                rs.close();ps.close();
            } catch(SQLException e) {
                log.error("CargaDocDistBean::getParamFechaPorc(Exception) "+e);
                log.error("CargaDocDistBean::getParamFechaPorc(SQLExcepcion) \n"+qrySentencia);
                throw new NafinException("SIST0001");
            } catch(Exception e) {
                log.error("CargaDocDistBean::getParamFechaPorc(Exception) "+e);
                throw new NafinException("SIST0001");
            } finally {
                if(con.hayConexionAbierta()) {
                    con.cierraConexionDB();
                }
                log.info("CargaDocDistBean::getParamFechaPorc(S)");
            }
            return cg_fecha_porc_desc;
        }

    private String getCategoria(String ic_epo, String ic_producto_nafin, String cg_clave,String ic_pyme) throws NafinException {
        log.info("CargaDocDistBean::getCategoria(E)");
        AccesoDB con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        String tablas = "";
        String condicion = "";
        String ic_clasificacion = "NULL";
        try {
            con = new AccesoDB();
            con.conexionDB();
            if("".equals(cg_clave)) {
                tablas = 
                    "        ,comrel_pyme_epo_x_producto pep";
                condicion =
                    "    AND clas.ic_epo = pep.ic_epo"   +
                    "    AND clas.ic_producto_nafin = pep.ic_producto_nafin"   +
                    "    AND clas.ic_clasificacion = pep.ic_clasificacion"   +
                    "    AND pep.ic_pyme = ?";
            } else {
                condicion = "    AND clas.cg_clave = ? ";
            }
            qrySentencia =
                " SELECT clas.ic_clasificacion"   +
                "   FROM comrel_clasificacion clas"+tablas+" "+
                "  WHERE clas.ic_epo = ?"   +
                "    AND clas.ic_producto_nafin = ? "+condicion+" ";
            ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1,Integer.parseInt(ic_epo));
            ps.setInt(2,Integer.parseInt(ic_producto_nafin));
            if(!"".equals(cg_clave))
                ps.setString(3,cg_clave);
            else
                ps.setInt(3,Integer.parseInt(ic_pyme));
            rs = ps.executeQuery();
            if(rs.next())
                ic_clasificacion = (rs.getString(1)==null)?"NULL":rs.getString(1);
            else if(!"".equals(cg_clave))
                throw new NafinException("DIST0034");
            rs.close();ps.close();
            log.debug("qrySentencia::: "+qrySentencia);
        } catch(SQLException e) {
            e.printStackTrace();
            log.error("CargaDocDist::getCategoria(SQLException) "+e);
            log.error("CargaDocDist::getCategoria SQLExcepcion \n"+qrySentencia);
            throw new NafinException("SIST0001");
        } catch(NafinException ne) {
            ne.printStackTrace();
            log.error("CargaDocDist::getCategoria(NafinException) "+ne);
            throw ne;
        } catch(Exception e) {
            e.printStackTrace();
            log.error("CargaDocDist::getCategoria(Exception) "+e);
            throw new NafinException("SIST0001");
        } finally {
            try {
                if (rs!=null) rs.close();
                if (ps!=null) ps.close();
            } catch(Exception e) {
                log.error("CargaDocDistBean::getCategoria(Exception)" + e);
            }
            if(con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("CargaDocDistBean::getCategoria(S)");
        }
        return ic_clasificacion;
    }
    
    private int getRestaFechas(String fechaEmision, String plazoMax) throws NafinException {
        log.info("CargaDocDistBean::gettRestaFechas (E)");
        AccesoDB con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String qrySentencia = "";
        int resta = 0;
	try{
		con = new AccesoDB();
		con.conexionDB();
		qrySentencia =
			" SELECT (TO_DATE (?, 'dd/mm/yyyy') + ?) - TRUNC(SYSDATE) as RESTA"   +
			"   FROM dual"  ;
		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,fechaEmision);
		ps.setInt(2,Integer.parseInt(plazoMax));
		rs = ps.executeQuery();
        ps.clearParameters();
		if(rs.next())
			resta = rs.getInt(1);
		rs.close();ps.close();
	}catch(SQLException e){
	    log.error("CargaDocDist::gettRestaFechas(SQLException) "+e);
		log.error("CargaDocDist::gettRestaFechas SQLExcepcion \n"+qrySentencia);
		throw new NafinException("SIST0001");
	}catch(Exception e){
		log.error("CargaDocDist::gettRestaFechas Excepcion "+e);
		throw new NafinException("SIST0001");
	}finally {
		if(con.hayConexionAbierta()){
			con.cierraConexionDB();
		}
		log.info("CargaDocDistBean::gettRestaFechas (S)");
	}
	return resta;
 }


public Vector getCategoriasXEpo(String ic_epo, String cg_num_distribuidor, String ic_producto_nafin, String selected)
		throws NafinException{
	System.out.println("CargaDocDistBean::getCategoriasXEpo (E)");
   	AccesoDB con = null;
	ResultSet rs = null;
	PreparedStatement ps = null;
	String qrySentencia = "";
	Vector	vecFilas = new Vector();
	Vector	vecColumnas = null;
   	try{
		con = new AccesoDB();
		con.conexionDB();
		qrySentencia =
			" SELECT cla.ic_clasificacion, cla.cg_clave || ' - ' || cla.cg_descripcion AS descripcion,"   +
			"        DECODE (pep.ic_clasificacion, cla.ic_clasificacion, 'selected', '') AS selected"   +
			"   FROM comrel_clasificacion cla, comrel_pyme_epo_x_producto pep, comrel_pyme_epo cpe"   +
			"  WHERE cla.ic_epo = pep.ic_epo"   +
			"    AND cpe.ic_epo = cla.ic_epo"   +
			"    AND cpe.ic_pyme = pep.ic_pyme"   +
			"    AND cla.ic_producto_nafin = pep.ic_producto_nafin"   +
			"    AND cla.ic_epo = ?"   +
			"    AND cla.ic_producto_nafin = ?"   +
			"    AND cpe.cg_num_distribuidor = ?"   +
			"  ORDER BY 1"  ;
		System.out.println("\n qrySentencia: "+qrySentencia);
		System.out.println("Dist: " + cg_num_distribuidor + "; Selected: " + selected);
		ps = con.queryPrecompilado(qrySentencia);
		ps.setInt(1,Integer.parseInt(ic_epo));
		ps.setInt(2,Integer.parseInt(ic_producto_nafin));
		ps.setString(3,cg_num_distribuidor);
		rs = ps.executeQuery();
		while(rs.next()){
			vecColumnas = new Vector();
/*0*/		vecColumnas.add((rs.getString(1)==null)?"":rs.getString(1));
/*1*/		vecColumnas.add((rs.getString(2)==null)?"":rs.getString(2));
			if(!"".equals(selected)&&selected!=null)
/*2*/			vecColumnas.add(selected.equals(rs.getString(1).toString())?"selected":"");
			else
/*2*/			vecColumnas.add((rs.getString(3)==null)?"":rs.getString(3));
			vecFilas.add(vecColumnas);
		}
		rs.close();
		if(ps!=null) ps.close();
	}catch(SQLException e){
		System.out.println("CargaDocDist::getCategoriasXEpo SQLExcepcion \n"+qrySentencia);
		e.printStackTrace();
	    throw new NafinException("SIST0001");
	}catch(Exception e){
		System.out.println("CargaDocDist::getCategoriasXEpo Excepcion "+e);
	    throw new NafinException("SIST0001");
	}finally{
		if(con.hayConexionAbierta()){
	     	con.cierraConexionDB();
	    }
		System.out.println("CargaDocDistBean::getCategoriasXEpo (S)");
	}
	return vecFilas;
}

    public String validaDistribuidor (String epo,String dist,String fecha_emision) throws NafinException{
        log.info("CargaDocDistBean::validaDistribuidor(E)");
        AccesoDB con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        String  qrySentencia = "";
        String   Pyme = "";
        String  Dist = "";
        try {
	        con = new AccesoDB();
	        con.conexionDB();
	        qrySentencia = 
                "select ic_pyme "+
                "           from comrel_pyme_epo "+
                "           where ic_epo = ? "+
                "           and cg_num_distribuidor = ? ";
	        log.debug("\n qrySentencia: "+qrySentencia);
	        ps = con.queryPrecompilado(qrySentencia);
	        ps.setInt(1, Integer.parseInt(epo));
	        ps.setString(2, dist);
	        rs = ps.executeQuery();
	        ps.clearParameters();
	        if(rs.next())
	        	Pyme = rs.getString(1);
	        else Pyme = "0";
            rs.close();ps.close();

        	qrySentencia = 
                "select cs_estatus "+
                "           from dis_clientes_susp "+
                "           where df_suspension in "+
                "           (   select max(df_suspension) "+
                "               from dis_clientes_susp "+
                "               where ic_pyme = ? ) "+
                "           and df_suspension <= to_date(?,'dd/mm/yyyy')";
           	log.debug("\n qrySentencia: "+qrySentencia);
            ps = con.queryPrecompilado(qrySentencia);
           	ps.setInt(1, Integer.parseInt(Pyme));
           	ps.setString(2, fecha_emision);
           	rs = ps.executeQuery();
           	ps.clearParameters();
           	if(rs.next())
            	Dist = rs.getString(1);
            else Dist = "N";
            rs.close();ps.close();
        } catch(SQLException e) {
            log.error("CargaDocDist::validaDistribuidor(SQLExcepcion) "+e);
            log.error("CargaDocDist::validaDistribuidor SQLExcepcion \n"+qrySentencia);
            throw new NafinException("SIST0001");
        } catch(Exception e) {
            System.out.println("CargaDocDist::validaDistribuidor(Excepcion) "+e);
            throw new NafinException("SIST0001");
        } finally {
            if(con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("CargaDocDistBean::validaDistribuidor (S): " + Dist);
        }
        return Dist;
    }


    public String validaSaldoDisponible (String epo,String dist,String moneda,String monto_docto,String monto_temporal) throws NafinException {
        log.info("CargaDocDistBean::validaSaldoDisponible(E)");
        AccesoDB con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        String  qrySentencia = "";
        String	Pyme = "";
        BigDecimal saldo = new BigDecimal("0.0");
        String  valida = "N";
        try {
        	con = new AccesoDB();
            con.conexionDB();
	        qrySentencia = 
                "select ic_pyme "+
                "           from comrel_pyme_epo "+
                "           where ic_epo = ? "+
                "           and cg_num_distribuidor = ? ";
	        log.debug("\n qrySentencia: "+qrySentencia);
	        ps = con.queryPrecompilado(qrySentencia);
	        ps.setInt(1, Integer.parseInt(epo));
	        ps.setString(2, dist);
	        rs = ps.executeQuery();
	        ps.clearParameters();
	        if(rs.next())
	        	Pyme = rs.getString(1);
	        else Pyme = "0";
            rs.close();ps.close();

            qrySentencia = 	
                "select saldo_disponible - ( monto_comprometido + ? + ? ) from "+
                "	(select sum(fn_monto) as monto_comprometido "+
                "	from dis_documento "+
                "	where ic_epo = ? "+
                "	and ic_pyme = ? "+
                "	and ic_moneda = ? "+
                "	and ic_estatus_docto = ?), "+
                "	(select fn_saldo_linea_total as saldo_disponible "+
                "	from com_linea_credito lcd, comcat_if i "+
                "	where lcd.ic_if = i.ic_if "+
                "	and ic_producto_nafin = ? "+
                "	and lcd.df_vencimiento_adicional > SYSDATE "+
                "	and lcd.cg_tipo_solicitud = ? "+
                "	and i.ig_tipo_piso = ? "+
                "	and lcd.ic_estatus_linea = ? "+
                "	and ic_epo = ? "+
                "	and ic_pyme = ? "+
                "	and ic_moneda = ? )";
			log.debug("\n qrySentencia: "+qrySentencia);
	        ps = con.queryPrecompilado(qrySentencia);
	        ps.setBigDecimal(1,new BigDecimal(monto_docto));
	        ps.setBigDecimal(2,new BigDecimal(monto_temporal));
            ps.setInt(3, Integer.parseInt(epo));
            ps.setInt(4, Integer.parseInt(Pyme));
            ps.setInt(5, Integer.parseInt(moneda));
            ps.setInt(6, Integer.valueOf(2));
            ps.setInt(7, Integer.valueOf(4));
            ps.setString(8, "I");
            ps.setInt(9, Integer.valueOf(1));
            ps.setInt(10, Integer.valueOf(12));
            ps.setInt(11, Integer.parseInt(epo));
            ps.setInt(12, Integer.parseInt(Pyme));
            ps.setInt(13, Integer.parseInt(moneda));
	        rs = ps.executeQuery();
	        ps.clearParameters();
			if(rs.next()){
                saldo = rs.getBigDecimal(1)==null?new BigDecimal("0.0"):rs.getBigDecimal(1);
                if(saldo.signum()>0)
                    valida = "S";
		  	} else
		    	valida = "NL";
	        rs.close();ps.close();
        } catch(SQLException e) {
            log.error("CargaDocDist::validaSaldoDisponible(SQLExcepcion) "+e);
            log.error("CargaDocDist::validaSaldoDisponible SQLExcepcion \n"+qrySentencia);
            throw new NafinException("SIST0001");
        } catch(Exception e) {
            log.error("CargaDocDist::validaSaldoDisponible(Excepcion) "+e);
            throw new NafinException("SIST0001");
        } finally {
        	if(con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.error("CargaDocDistBean::validaSaldoDisponible(S) "+valida);
        }
        return valida;
    }

public String getMontoAcumulado (String epo,String dist,String moneda,String proc_docto)
	throws NafinException{
	System.out.println("CargaDocDistBean::getMontoAcumulado (E)");
   	AccesoDB con = null;
	ResultSet rs = null;
	String  qrySentencia = "";
	String	Pyme = "";
	String  valida = "0";
   	try{
        	con = new AccesoDB();
            con.conexionDB();

            qrySentencia = 	"select ic_pyme "+
							"	from comrel_pyme_epo "+
							"	where ic_epo = "+ epo +
							"	and cg_num_distribuidor = '"+ dist +"'";

			System.out.println("\n qrySentencia: "+qrySentencia);
            rs = con.queryDB(qrySentencia);
			if(rs.next())
	            Pyme = rs.getString(1);
	        else Pyme = "0";
	        rs.close();

            qrySentencia = 	"select sum(fn_monto) "+
							"from distmp_proc_docto "+
							"where ic_proc_docto = "+ proc_docto +
							"	and ic_epo = "+ epo +
							"	and ic_pyme = "+ Pyme +
							"	and ic_moneda = "+ moneda;

			System.out.println("\n qrySentencia: "+qrySentencia);
            rs = con.queryDB(qrySentencia);
			if(rs.next())
				valida = rs.getString(1);
			if (valida == null)
				valida = "0";
	        rs.close();
	        con.cierraStatement();

        }catch(SQLException e){
		System.out.println("CargaDocDist::getMontoAcumulado SQLExcepcion \n"+qrySentencia);
                throw new NafinException("SIST0001");
        }catch(Exception e){
		System.out.println("CargaDocDist::getMontoAcumulado Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("CargaDocDistBean::getMontoAcumulado(S): " + valida);
        }

	return valida;
}

public int esInhabil (String chk[],String fechas[],int registros)
	throws NafinException{
	System.out.println("CargaDocDistBean::esInhabil (E)");
   	AccesoDB con = null;
	ResultSet rs = null;
    PreparedStatement ps = null;
    String  qrySentencia = "";
	int valida = -1;
   	try{
        	con = new AccesoDB();
            con.conexionDB();
            System.out.println("Registros: "+registros);
            for(int i=0;i<registros;i++){
            	int j = Integer.parseInt(chk[i]);

	            qrySentencia = "select es_inhabil(to_date(?, 'dd/mm/yyyy')) from dual";

				System.out.println("\n qrySentencia: "+qrySentencia+" Fecha: " + fechas[j]);
	           	ps = con.queryPrecompilado(qrySentencia);
	           	ps.setString(1,fechas[j]);
	           	rs = ps.executeQuery();
	           	ps.clearParameters();
	           	if(rs.next())
		        	if(rs.getInt(1)==1){
		        		valida = j;
		        		throw new NafinException("TOTC0011");
		        	}
	            rs.close();ps.close();
			}
	        //con.cierraStatement();

        }catch(NafinException ne){
            System.out.println("ENTRANDO A NAFINEXCEPTION"+ne.getMsgError()) ;
        }catch(SQLException e){
		System.out.println("CargaDocDist::esInhabil SQLExcepcion \n"+qrySentencia);
                throw new NafinException("SIST0001");
        }catch(Exception e){
		System.out.println("CargaDocDist::esInhabil Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
		System.out.println("CargaDocDistBean::esInhabil(S): " + valida);
        }

	return valida;
}

	public void setCargaEnlacePEMEX(String ic_epo, String df_fecha_emision)
			throws NafinException {
		System.out.println("CargaDocDistBean::setCargaEnlacePEMEX(E)");

		AccesoDB 			con				= null;
		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;

		boolean				bOk				= true;

		//CIFRAS DE CONTROL
		int	registros = 0;
		double montoTotal = 0;

		int	registrosCC = 0;
		double montoTotalCC = 0;

		Acuse	acuse 		= null;
		int 	totDocMN 	= 0;
		double 	totMtoMN 	= 0;
		int 	totDocUSD 	= 0;
		double 	totMtoUSD 	= 0;

		int 	totDocAcep 	= 0;
		double 	totMtoAcep 	= 0;
		int 	totDocRech 	= 0;
		double 	totMtoRech 	= 0;

		try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia =
				" SELECT COUNT (1), SUM (fn_monto)"   +
				"   FROM distmp_doctos_pub_p02"   +
				"  WHERE df_fecha_emision >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"    AND df_fecha_emision < (TO_DATE (?, 'dd/mm/yyyy') + 1)"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, df_fecha_emision);
			ps.setString(2, df_fecha_emision);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				registros = rs.getInt(1);
				montoTotal = rs.getDouble(2);
			}//if(rs.next())
			rs.close(); ps.close();

			qrySentencia =
				" SELECT in_total_docto, fn_total_monto"   +
				"   FROM distmp_cc_doctos_pub_p02"   +
				"  WHERE df_fecha_emision >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"    AND df_fecha_emision < (TO_DATE (?, 'dd/mm/yyyy') + 1)"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, df_fecha_emision);
			ps.setString(2, df_fecha_emision);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				registrosCC = rs.getInt(1);
				montoTotalCC = rs.getDouble(2);
			}//if(rs.next())
			rs.close(); ps.close();

			qrySentencia =
				" SELECT ig_numero_docto, cpe.ic_pyme, tmp.cg_num_distribuidor,"   +
				"        TO_CHAR (df_fecha_emision, 'dd/mm/yyyy') AS fechaemision,"   +
				"        TO_CHAR (df_fecha_venc, 'dd/mm/yyyy') AS fechavenc, ic_moneda,"   +
				"        fn_monto, ic_tipo_financiamiento,"   +
				"        TO_CHAR (df_alta, 'dd/mm/yyyy') AS fechaalta,"   +
				"        TRUNC (df_fecha_venc - df_fecha_emision) AS diferencia,"   +
				"        es_inhabil (df_fecha_venc) AS DIAINHABIL"   +
				"   FROM distmp_doctos_pub_p02 tmp, comrel_pyme_epo cpe"   +
				"  WHERE cpe.ic_epo = ?"   +
				"    AND cpe.cg_num_distribuidor = tmp.cg_num_distribuidor"   +
				"    AND df_fecha_emision >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"    AND df_fecha_emision < (TO_DATE (?, 'dd/mm/yyyy') + 1)"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setString(2, df_fecha_emision);
			ps.setString(3, df_fecha_emision);
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()) {
				String rs_numero_docto 	= rs.getString("ig_numero_docto")==null?"":rs.getString("ig_numero_docto");
				String rs_pyme		 	= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme");
				String rs_num_dist	 	= rs.getString("cg_num_distribuidor")==null?"":rs.getString("cg_num_distribuidor");
				String rs_fecha_emision	= rs.getString("fechaemision")==null?"":rs.getString("fechaemision");
				String rs_fecha_venc	= rs.getString("fechavenc")==null?"":rs.getString("fechavenc");
				String rs_moneda		= rs.getString("ic_moneda")==null?"":rs.getString("ic_moneda");
				String rs_monto			= rs.getString("fn_monto")==null?"0":rs.getString("fn_monto");
				String rs_tipo_finan	= rs.getString("ic_tipo_financiamiento")==null?"0":rs.getString("ic_tipo_financiamiento");
				//String rs_fecha_alta	= rs.getString("fechaalta")==null?"":rs.getString("fechaalta");
				String rs_diferencia	= rs.getString("diferencia")==null?"":rs.getString("diferencia");
				String rs_diainhabil	= rs.getString("DIAINHABIL")==null?"":rs.getString("DIAINHABIL");

				if(registros==registrosCC && montoTotal==montoTotalCC) {
					try {
						 operaTipoCredito(rs_pyme,"C",4,ic_epo,con);
					} catch(NafinException ne){
						//Registra Rechazo 099
						setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "099", con);
						totDocRech ++;
						totMtoRech += Double.parseDouble(rs_monto);
						continue;
					}

					String clienteSusp = validaDistribuidor(ic_epo, rs_num_dist, rs_fecha_emision);

					if(!"S".equals(clienteSusp)) {
						double saldoDisponible = validaSaldos(ic_epo, rs_pyme, rs_moneda, con);
						if(saldoDisponible > Double.parseDouble(rs_monto)) {
							int existe = getDoctoExistente(ic_epo, rs_pyme, rs_numero_docto, con);
							if(existe==0) {
								if(Integer.parseInt(rs_diferencia)>0) {
									if(Integer.parseInt(rs_diainhabil)==0) {
										if(Double.parseDouble(rs_monto)>0) {
											if("5".equals(rs_tipo_finan)) {

												if(totDocMN == 0 && totDocUSD==0) {
													acuse = new Acuse(1,"4","com",con);
													creaAcuse(acuse.toString(), "enlac_aut", con);
												}

												if("1".equals(rs_moneda)) {
													totDocMN++;
													totMtoMN += Double.parseDouble(rs_monto);
												} else if("54".equals(rs_moneda)) {
													totDocUSD++;
													totMtoUSD += Double.parseDouble(rs_monto);
												}
												totDocAcep ++;
												totMtoAcep += Double.parseDouble(rs_monto);

												setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "000", con);

												setDocumentosEnlacePemex(ic_epo, rs_pyme, acuse.toString(), rs_numero_docto, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_diferencia, con);

											} else {
												//Registra Rechazo 099
												setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "099", con);
												totDocRech ++;
												totMtoRech += Double.parseDouble(rs_monto);
											}
										} else {
											//Registra Rechazo 009
											setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "009", con);
											totDocRech ++;
											totMtoRech += Double.parseDouble(rs_monto);
										}
									} else {
										//Registra Rechazo 008
										setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "008", con);
										totDocRech ++;
										totMtoRech += Double.parseDouble(rs_monto);

									}
								} else {
									//Registra Rechazo 099
									setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "099", con);
									totDocRech ++;
									totMtoRech += Double.parseDouble(rs_monto);
								}
							} else {
								//Registra Rechazo 001
								setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "001", con);
								totDocRech ++;
								totMtoRech += Double.parseDouble(rs_monto);
							}
						} else {
							//Registra Rechazo 099
							setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "099", con);
							totDocRech ++;
							totMtoRech += Double.parseDouble(rs_monto);
						}
					} else {
						//Registra Rechazo 012
						setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "012", con);
						totDocRech ++;
						totMtoRech += Double.parseDouble(rs_monto);
					}
				} else {
					//Registra Rechazo 011
					setDoctosPubAcuse(rs_numero_docto, rs_num_dist, rs_fecha_emision, rs_fecha_venc, rs_moneda, rs_monto, rs_tipo_finan, "011", con);
					totDocRech ++;
					totMtoRech += Double.parseDouble(rs_monto);
				}

			}//while(rs.next())
			rs.close(); ps.close();



			if(totDocMN > 0 || totDocUSD > 0) {
				actualizaAcuse(acuse.toString(), totDocMN, totDocUSD, totMtoMN, totMtoUSD, con);
			}
			setAcuseDoctosPubAcuse(df_fecha_emision, totDocAcep, totMtoAcep, totDocRech, totMtoRech, con);

		} catch(Exception e) {
			bOk = false;
			System.out.println("CargaDocDistBean::setCargaEnlacePEMEX(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(bOk);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("CargaDocDistBean::setCargaEnlacePEMEX(S)");
		}
	}//setCargaEnlacePEMEX


    private void operaTipoCredito(String ic_pyme, String tipo_credito, int ic_producto_nafin, String ic_epo, AccesoDB con)
        throws NafinException{
		System.out.println("CargaDocDistBean::operaTipoCredito (E)");
		ResultSet rs = null;
		PreparedStatement ps = null;
		String qrySentencia = "";
		int cuenta = 0;
   		try{
			qrySentencia =	" select count(*)"+
				" from comrel_pyme_epo_x_producto"+
				" where ic_pyme = ?"+
				" and ic_producto_nafin = ?"+
				" and cg_tipo_credito = ?";
			if(ic_epo!=null)
				qrySentencia += " and ic_epo = ?";

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_pyme);
			ps.setInt(2,ic_producto_nafin);
			ps.setString(3,tipo_credito);
			if(ic_epo!=null)
				ps.setString(4,ic_epo);

			rs = ps.executeQuery();
			if(rs.next())
				cuenta = rs.getInt(1);

			ps.close();
			if(cuenta==0)
				throw new NafinException("DIST0015");

			}catch(NafinException ne){
                	throw ne;
        	}catch(Exception e){
				System.out.println("CargaDocDistBean::operaTipoCredito(Excepcion) "+e);
                        throw new NafinException("SIST0001");
        	}finally{
				System.out.println("CargaDocDistBean::operaTipoCredito (S)");
        	}
     }

	private void actualizaAcuse(String acuse, int totalDoctoMN,int totalDoctoUSD,double totalMontoMN,double totalMontoUSD, AccesoDB con)
		throws NafinException{
		System.out.println("CargaDocDistBean::creaAcuse (E)");
	    String      qrySentencia = "";
	    PreparedStatement	ps				= null;
	    boolean	ok = true;
	    try{

			qrySentencia =
				" UPDATE com_acuse1"   +
				"    SET in_total_docto_mn = ?,"   +
				"        fn_total_monto_mn = ?,"   +
				"        in_total_docto_dl = ?,"   +
				"        fn_total_monto_dl = ?"   +
				"  WHERE cc_acuse = ?"   +
				"    AND ic_producto_nafin = 4"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, totalDoctoMN);
			ps.setDouble(2, totalMontoMN);
			ps.setInt(3, totalDoctoUSD);
			ps.setDouble(4, totalMontoUSD);
			ps.setString(5, acuse);

			ps.executeUpdate();
			ps.clearParameters();
			ps.close();

	    }catch(Exception e){
	    	ok = false;
	    	throw new NafinException("SIST0001");
	    }
		System.out.println("CargaDocDistBean::creaAcuse (S)");
	}


	private void setDocumentosEnlacePemex(String ic_epo, String ic_pyme, String cc_acuse, String ig_numero_docto, String df_fecha_emision, String df_fecha_venc, String ic_moneda, String fn_monto, String ig_plazo_docto, AccesoDB con)
	throws NafinException{
		System.out.println("CargaDocDistBean::setDocumentosEnlacePemex (E)");
	    String      qrySentencia = "";
	    PreparedStatement	ps				= null;
	    boolean	ok = true;
	    try{

			qrySentencia =

				" INSERT INTO dis_documento"   +
				"             (ic_documento, ic_epo, ic_pyme, ic_producto_nafin, cc_acuse,"   +
				"              ig_numero_docto, df_fecha_emision, df_fecha_venc, ic_moneda,"   +
				"              ct_referencia, fn_porc_descuento, ig_plazo_descuento, fn_monto,"   +
				"              ic_tipo_financiamiento, ic_estatus_docto, ic_linea_credito_dm,"   +
				"              ig_plazo_credito, df_fecha_venc_credito, ig_plazo_docto,"   +
				"              cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5,"   +
				"              cs_plazo_descuento, ic_clasificacion)"   +
				"    SELECT MAX (ic_documento) + 1, ?, ?, 4, ?, "   +
				"           ?, to_date(?, 'dd/mm/yyyy'), to_date(?, 'dd/mm/yyyy'), ?,"   +
				"           NULL, NULL, NULL, ?, "   +
				"           5, 2, NULL,"   +
				"           NULL, NULL, ?,"   +
				"           NULL, NULL, NULL, NULL, NULL,"   +
				"           'N', NULL"   +
				"      FROM dis_documento"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(		1, Integer.parseInt(ic_epo));
			ps.setInt(		2, Integer.parseInt(ic_pyme));
			ps.setString(	3, cc_acuse);
			ps.setString(	4, ig_numero_docto);
			ps.setString(	5, df_fecha_emision);
			ps.setString(	6, df_fecha_venc);
			ps.setInt(		7, Integer.parseInt(ic_moneda));
			ps.setDouble(	8, Double.parseDouble(fn_monto));
			ps.setInt(		9, Integer.parseInt(ig_plazo_docto));

			ps.executeUpdate();
			ps.clearParameters();
			ps.close();

	    }catch(Exception e){
	    	e.printStackTrace();
	    	ok = false;
	    	throw new NafinException("SIST0001");
	    }finally{
			System.out.println("CargaDocDistBean::setDocumentosEnlacePemex (S)");
	    }
	}//setDocumentosEnlacePemex


	private void setAcuseDoctosPubAcuse(String df_fecha_emision, int totDoctoAcep, double totMontoAcep, int totDocroRech, double totMontoRech, AccesoDB con)
	throws NafinException{
		System.out.println("CargaDocDistBean::setDocumentosEnlacePemex (E)");
	    String      qrySentencia = "";
	    PreparedStatement	ps				= null;
	    boolean	ok = true;
	    try{

			qrySentencia =
				" INSERT INTO dis_doctos_pub_acu_p02"   +
				"             (df_fecha_emision, in_total_doctos_acep, fn_total_monto_acep,"   +
				"              in_total_doctos_rech, fn_total_monto_rech, df_fechahora_carga)"   +
				"      VALUES (to_date(?, 'dd/mm/yyyy'), ?, ?,"   +
				"              ?, ?, SYSDATE)"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, df_fecha_emision);
			ps.setInt(2, totDoctoAcep);
			ps.setDouble(3, totMontoAcep);
			ps.setInt(4, totDocroRech);
			ps.setDouble(5, totMontoRech);

			ps.executeUpdate();
			ps.clearParameters();
			ps.close();

	    }catch(Exception e){
	    	ok = false;
	    	throw new NafinException("SIST0001");
	    }finally{
			System.out.println("CargaDocDistBean::setDocumentosEnlacePemex (S)");
	    }
	}//setDocumentosEnlacePemex

	private void setDoctosPubAcuse(String ig_numero_docto, String ig_numero_dist, String df_fecha_emision, String df_fecha_venc, String ic_moneda, String fn_monto, String ig_tipo_finan, String sError, AccesoDB con)
			throws NafinException {
		System.out.println("CargaDocDistBean::setDoctosPubAcuse(E)");

		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;

		try {
			qrySentencia =
				" INSERT INTO dis_doctos_pub_acu_det_p02"   +
				"             (ig_numero_docto, cg_num_distribuidor, df_fecha_emision,"   +
				"              df_fecha_venc, ic_moneda, fn_monto, ic_tipo_financiamiento,"   +
				"              ig_numero_catalogo, ig_numero_error, df_aut_rech,"   +
				"              df_fechahora_carga"   +
				"             )"   +
				"      VALUES (?, ?, to_date(?, 'dd/mm/yyyy'),"   +
				"              to_date(?, 'dd/mm/yyyy'), ?, ?, ?,"   +
				"              ?, ?, SYSDATE,"   +
				"              SYSDATE"   +
				"             )"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1, ig_numero_docto);
			ps.setString(2, ig_numero_dist);
			ps.setString(3, df_fecha_emision);
			ps.setString(4, df_fecha_venc);
			ps.setInt(5, Integer.parseInt(ic_moneda));
			ps.setDouble(6, Double.parseDouble(fn_monto));
			ps.setInt(7, Integer.parseInt(ig_tipo_finan));
			ps.setInt(8, 24); //Valor DEFAULT
			ps.setString(9, sError);

			ps.executeUpdate();
			ps.clearParameters();
			ps.close();

		} catch(Exception e) {
			System.out.println("CargaDocDistBean::setDoctosPubAcuse(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {

			System.out.println("CargaDocDistBean::setDoctosPubAcuse(S)");
		}
	}//setDoctosPubAcuse

	private double validaSaldos(String ic_epo, String ic_pyme, String ic_moneda, AccesoDB con)
			throws NafinException {
		System.out.println("CargaDocDistBean::validaSaldos(E)");

		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;

		double montoTotalNeg = 0;
		double saldoLineaTotal = 0;

		double saldoDisponible = 0;

		try {

			qrySentencia =
				" SELECT SUM (fn_monto)"   +
				"   FROM dis_documento"   +
				"  WHERE ic_epo = ?"   +
				"    AND ic_pyme = ?"   +
				"    AND ic_moneda = ?"   +
				"    AND ic_estatus_docto = 2"   +
				"    AND ic_producto_nafin = 4"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, Integer.parseInt(ic_pyme));
			ps.setInt(3, Integer.parseInt(ic_moneda));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next())
				montoTotalNeg = rs.getDouble(1);
			rs.close(); ps.close();

			qrySentencia =
				" SELECT   fn_saldo_linea_total"   +
				"     FROM com_linea_credito lcd, comcat_if i"   +
				"    WHERE lcd.ic_if = i.ic_if"   +
				"      AND lcd.df_vencimiento_adicional > SYSDATE"   +
				"      AND lcd.cg_tipo_solicitud = 'I'"   +
				"      AND lcd.ic_estatus_linea = 12"   +
				"      AND lcd.ic_pyme = ?"   +
				"      AND lcd.ic_moneda = ?"   +
				"      AND lcd.ic_producto_nafin = 4"   +
				"      AND i.ig_tipo_piso = 1"   +
				" ORDER BY ic_linea_credito"  ;

			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_pyme));
			ps.setInt(2, Integer.parseInt(ic_moneda));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next())
				saldoLineaTotal = rs.getDouble(1);
			rs.close(); ps.close();

			saldoDisponible = saldoLineaTotal - montoTotalNeg;
		} catch(Exception e) {
			System.out.println("CargaDocDistBean::validaSaldos(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {

			System.out.println("CargaDocDistBean::validaSaldos(S)");
		}

		return saldoDisponible;
	}//setCargaEnlacePEMEX


	private int getDoctoExistente(String ic_epo, String ic_pyme, String ig_numero_docto, AccesoDB con)
			throws NafinException {
		System.out.println("CargaDocDistBean::validaSaldos(E)");

		String				qrySentencia 	= "";
		PreparedStatement	ps				= null;
		ResultSet			rs				= null;

		int existe = 0;
		try {

			qrySentencia =
				" SELECT COUNT (1)"   +
				"   FROM dis_documento"   +
				"  WHERE ic_epo = ?"   +
				"    AND ic_pyme = ?"   +
				"    AND ic_producto_nafin = 4"   +
				"    AND ig_numero_docto = ?"  ;
			ps = con.queryPrecompilado(qrySentencia);
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, Integer.parseInt(ic_pyme));
			ps.setString(3, ig_numero_docto);
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next())
				existe = rs.getInt(1);
			rs.close(); ps.close();
		} catch(Exception e) {
			System.out.println("CargaDocDistBean::validaSaldos(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("CargaDocDistBean::validaSaldos(S)");
		}

		return existe;
	}//setCargaEnlacePEMEX

/**
   * metodod para obtener el monto de la linea de Credito
   * @throws com.netro.exception.NafinException
   * @return
   * @param ic_epo
   */

public String validaMontolineaCredito(String ic_epo) 	throws NafinException {
	log.info("validaMontolineaCredito (E)");

		String	qrySentencia 	= "";
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String monto = "";

		try {
      con.conexionDB();

      qrySentencia = 	" SELECT FN_SALDO_TOTAL "+
                      " FROM DIS_LINEA_CREDITO_DM "+
                      " WHERE  ic_epo = ? "+
                      " AND ic_producto_nafin = ? "+
											" AND df_vencimiento_adicional > SYSDATE "+
											" AND cg_tipo_solicitud = 'I' "+
											"  AND ic_estatus_linea = 12" ;

      ps = con.queryPrecompilado(qrySentencia);
      ps.setInt(1,Integer.parseInt(ic_epo));
      ps.setInt(2,4);
      rs = ps.executeQuery();
      ps.clearParameters();

      if(rs.next())	{
        monto = rs.getString("FN_SALDO_TOTAL")==null?"0":rs.getString("FN_SALDO_TOTAL");
      }
      rs.close();
      ps.close();

  log.debug("qrySentencia" +qrySentencia);
  log.debug("ic_epo" +ic_epo);
	  log.debug("monto linea de Credito" +monto);

		} catch(Exception e) {
			log.error("validaMontolineaCredito(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
        con.cierraConexionDB();
			log.info(" validaMontolineaCredito(S)");
		}

		return monto;
	}
  /**
   *
   *@@@ Metodo que regresa si el epo esta o no parametrizada como
   *@@@ Opera Descuento Automatico Fodea 010 Distribuidores 2010
   *@return
   *@param ic_epo
   * @param parametro
   */
    public String DesAutomaticoEpo(String ic_epo, String parametro)  	{
        log.info("CargaDocDistBean::DesAutomaticoEpo(E)");
        String		qrySentencia	= "";
        ResultSet	rs				= null;
        PreparedStatement   ps      = null;
        AccesoDB 	con				= null;
        String      valor           = "";
    	try {
            con = new AccesoDB();
            con.conexionDB();
            qrySentencia =
                " SELECT cg_valor as valor  "+
                " FROM com_parametrizacion_epo  "+
                " WHERE  cc_parametro_epo = ? "+
                " AND ic_epo = ?";
            log.debug("qrySentencia"  +qrySentencia);
            ps = con.queryPrecompilado(qrySentencia);
            ps.setString(1, parametro);
            ps.setInt(2, Integer.valueOf(ic_epo));
            rs = ps.executeQuery();
            ps.clearParameters();
            if(rs.next()) {
                valor = ((rs.getString("valor")==null)?"":rs.getString("valor"));
            } else {
                valor = "";
            }
            rs.close();ps.close();
            //con.cierraStatement();
        } catch(Exception e) {
            con.terminaTransaccion(false);
            log.error("CargaDocDistBean::DesAutomaticoEpo(Exception) "+e);
        } finally {
            con.cierraConexionDB();
        }
        log.info("CargaDocDistBean::DesAutomaticoEpo(S)");
        return valor;
    }

	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param fecha
	 * @param ic_epo
	 */
  public String getDiasInhabilesDes(String ic_epo, String fecha )
   throws NafinException{
	 log.info("CargaDocDistBean::getDiasInhabilesDes (E)");

		AccesoDB 	con = null;
    String		qrySentencia = "";
    ResultSet 	rs =  null;
    String		retorno = "";
    String fecha1 =  fecha.substring(0,5);


   	try{
        	con = new AccesoDB();
                con.conexionDB();
    qrySentencia =	" select cg_dia_inhabil from comcat_dia_inhabil"+
                    " where  cg_dia_inhabil = '"+fecha1+"'"+
                    " and cg_dia_inhabil is not null ";
    if(!"".equals(ic_epo)){
			qrySentencia += " union "   +
				" select cg_dia_inhabil from comrel_dia_inhabil_x_epo"   +
				" where ic_epo = "+ic_epo+
        " and cg_dia_inhabil = '"+fecha1+"'"+
        " and cg_dia_inhabil is not null ";

    }
       log.debug("***********************");
       log.debug("ic_epo::: "+ic_epo);
       log.debug("qrySentencia::: "+qrySentencia);
       log.debug("***********************");
			rs = con.queryDB(qrySentencia);
				while(rs.next()){
					if(!"".equals(retorno))
						retorno += ",";
							retorno += rs.getString(1);
          }
          con.cierraStatement();

					log.debug("retorno::: "+retorno);

        }catch(Exception e){
			log.error("CargaDocDistBean::getDiasInhabilesDes Excepcion "+e);
			throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
			log.info("CargaDocDistBean::getDiasInhabilesDes (S)");
        }
   return retorno;
   }

   /**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_epo
	 */
	public String getMontoComprometido(String ic_epo)
   throws NafinException{
	 log.info("getMontoComprometido: (E)");

		AccesoDB 	con = null;
    String		qrySentencia = "";
		String		qrySentencia2 = "";
    ResultSet 	rs =  null;
    String		monto = "";
    PreparedStatement ps = null;
    con = new AccesoDB();
		String montoCaptura ="";

   	try{
    con.conexionDB();


		montoCaptura = montoLineaCreCaptura(ic_epo);

		log.debug("montoCaptura::: "+montoCaptura);

		if (montoCaptura.equals("") ) {

    qrySentencia =" Select sum(d.FN_MONTO) as monto from dis_documento d "+
									" where d.ic_estatus_docto = 2 "+
									" and d.IC_LINEA_CREDITO_DM in ( "+
									" SELECT l.IC_LINEA_CREDITO_DM "+
									" FROM DIS_LINEA_CREDITO_DM l , dis_documento d "+
									" WHERE  l.ic_epo = "+ic_epo+
									" AND l.ic_producto_nafin = 4 "+
									" AND l.df_vencimiento_adicional > SYSDATE  "+
									" AND l.cg_tipo_solicitud = 'I'  "+
									" AND l.ic_estatus_linea = 12  )";

       log.debug("qrySentencia::: "+qrySentencia);
				ps = con.queryPrecompilado(qrySentencia);
				rs = ps.executeQuery();

				if(rs.next())	{
					 monto = rs.getString(1);
				}
				rs.close();
				ps.close();

				log.debug("----------------------------------------------- ");
				log.debug("monto::: "+monto);
		    log.debug("----------------------------------------------- ");

				if (!monto.equals("")){
					qrySentencia2 ="	update DIS_LINEA_CREDITO_DM    "+
													" set FN_SALDO_COMPROMETIDO = "+monto+
													"  WHERE  ic_epo =  "+ic_epo+
													" AND ic_producto_nafin = 4 "+
													" AND df_vencimiento_adicional > SYSDATE  "+
													" AND cg_tipo_solicitud = 'I' "+
													" AND ic_estatus_linea = 12 ";

						log.debug("monto Coprometido::: "+qrySentencia2);

						ps = con.queryPrecompilado(qrySentencia2);
						ps.executeUpdate();
						ps.close();
				} //if (!monto.equals("")){

				}	//if (montoCaptura.equals("0") ) {

        }catch(Exception e){
				log.error(" getMontoComprometido Excepcion "+e);
                throw new NafinException("SIST0001");
        }finally{
        	if(con.hayConexionAbierta()){
                 	con.cierraConexionDB();
                }
						log.info(" getMontoComprometido(S)");
        }
   return monto;
   }
/**
	 * este monto es para la captura del documento
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_epo
	 */
	public String montoLineaCreCaptura(String ic_epo) 	throws NafinException {
        log.info("CargaDocDistBean::montoLineaCreCaptura(E)");
        String	qrySentencia 	= "";
        PreparedStatement	ps	= null;
        ResultSet rs	= null;
        AccesoDB 	con = null;;
        con = new AccesoDB();
        double monto = 0;
        String disponible ="0";
        String compometido ="0" ;
        String montoTotal = "0";
		try {
            con.conexionDB();
            qrySentencia = 	
                " SELECT fn_saldo_total,  FN_SALDO_COMPROMETIDO "+
                " FROM DIS_LINEA_CREDITO_DM "+
                " WHERE  ic_epo = ? "+
                " AND ic_producto_nafin = ? "+
                " AND df_vencimiento_adicional > SYSDATE "+
                " AND cg_tipo_solicitud = ? "+
                "  AND ic_estatus_linea = ?" ;
            ps = con.queryPrecompilado(qrySentencia);
		    ps.setInt(1, Integer.valueOf(ic_epo));
		    ps.setInt(2, Integer.valueOf(4));
            ps.setString(3, "I");
		    ps.setInt(4, Integer.valueOf(12));
            rs = ps.executeQuery();
            ps.clearParameters();
            if(rs.next()) {
                disponible = rs.getString("fn_saldo_total")==null?"0":rs.getString("fn_saldo_total");
                compometido = rs.getString("FN_SALDO_COMPROMETIDO")==null?"0":rs.getString("FN_SALDO_COMPROMETIDO");
            }
            rs.close();ps.close();            
            monto =  Double.parseDouble(disponible)- Double.parseDouble(compometido);
            montoTotal = Double.toString (monto) ;
            log.debug("qrySentencia" +qrySentencia);
            log.debug("monto linea de Credito" +montoTotal);
		} catch(Exception e) {
			log.error("CargaDocDistBean::montoLineaCreCaptura(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
            if(con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
            log.info("CargaDocDistBean::montoLineaCreCaptura(S)");
		}
		return montoTotal;
	}
/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_epo
	 */
        private String validaMontoComprometido(String ic_epo) 	throws NafinException {
            log.info("CargaDocDistBean::validaMontoComprometido(E)");
            String	qrySentencia 	= "";
            PreparedStatement	ps	= null;
            ResultSet rs	= null;
            AccesoDB con = new AccesoDB();
            String monto = "";
            try {
                con.conexionDB();
                qrySentencia = 	
                    " SELECT FN_SALDO_COMPROMETIDO "+
                    " FROM DIS_LINEA_CREDITO_DM "+
                    " WHERE  ic_epo = ? "+
                    " AND ic_producto_nafin = ? "+
                    " AND df_vencimiento_adicional > SYSDATE "+
                    " AND cg_tipo_solicitud = ? "+
                    "  AND ic_estatus_linea = ?" ;                
                log.debug("qrySentencia" +qrySentencia);                
                ps = con.queryPrecompilado(qrySentencia);
                ps.setInt(1, Integer.valueOf(ic_epo));
                ps.setInt(2, Integer.valueOf("4"));
                ps.setString(3, "I");
                ps.setInt(2, Integer.valueOf("12"));
                rs = ps.executeQuery();
                ps.clearParameters();
                if(rs.next()) {
                    monto = rs.getString("FN_SALDO_COMPROMETIDO")==null?"0":rs.getString("FN_SALDO_COMPROMETIDO");
                }
                rs.close();ps.close();
                log.debug("monto linea de Credito" +monto);
		} catch(Exception e) {
			log.error("CargaDocDistBean::validaMontoComprometido(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
            con.cierraConexionDB();
		    log.info("CargaDocDistBean::validaMontoComprometido(S)");
		}
		return monto;
    }
	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param ic_epo
	 */
	public String validaLineCredito(String ic_epo) 	throws NafinException {
        log.info("CargaDocDistBean::validaLineCredito(E)");

		String	qrySentencia 	= "";
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String validaLinea = "";

		try {
            con.conexionDB();
            qrySentencia = 	
                " SELECT ic_linea_credito_dm "+
                " FROM DIS_LINEA_CREDITO_DM "+
                " WHERE  ic_epo = ? "+
                " AND ic_producto_nafin = ? "+
                " AND df_vencimiento_adicional > SYSDATE "+
                " AND cg_tipo_solicitud = ? "+
                "  AND ic_estatus_linea = ?" ;
			log.debug("qrySentencia" +qrySentencia);
            ps = con.queryPrecompilado(qrySentencia);
		    ps.setInt(1, Integer.valueOf(ic_epo));
		    ps.setInt(2, Integer.valueOf(4));
		    ps.setString(3, "I");
		    ps.setInt(4, Integer.valueOf(12));            
            rs = ps.executeQuery();
            ps.clearParameters();
            if(rs.next()) {
                validaLinea = rs.getString("ic_linea_credito_dm")==null?"":rs.getString("ic_linea_credito_dm");
            }
            rs.close();ps.close();
			log.debug("validaLinea" +validaLinea);
		} catch(Exception e) {
			log.error("CargaDocDistBean::validaLineCredito(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
            con.cierraConexionDB();
			log.info("CargaDocDistBean::validaLineCredito(S)");
		}
		return validaLinea;
	}

    public CargaDocDistBean(){}

		//Fodea 051 Distribuidores

/**
	 * metodo que  valida si los datos capturados en el archivo
	 * a procesar para Recahzo de Soclicitudes sean correctos
	 * @throws netropology.utilerias.AppException
	 * @return
	 * @param noLinea
	 * @param causa
	 * @param estatus
	 * @param documento
	 */
		public List ValidaLineaRechazo(String documento, String estatus, String causa, int noLinea) throws AppException{

		log.debug(" ValidaLineaRechazo (E)");

		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		boolean ok = true;
		String existe ="";
		String icEstatusDocto ="";
		List errores = new ArrayList();
		int conError = 0;
		List aprocesar = new ArrayList();
		List respuesta	 = new ArrayList();

		try {
     con.conexionDB();


								if(Comunes.esNumero(documento)){
									//valida que exista el documento
									StringBuffer	query = new StringBuffer();
									//query.append("  select ic_documento from dis_documento where ic_estatus_docto = ? and ic_documento = ? ");
									query.append("  select ic_documento, ic_estatus_docto from dis_documento where ic_estatus_docto in (?, ?) and ic_documento = ? ");

									ps = con.queryPrecompilado(query.toString());
									ps.setString(1, "3");
									ps.setString(2, "24");
									ps.setString(3, documento);
									rs = ps.executeQuery();

									if(rs.next()){
										existe = rs.getString("ic_documento");
										icEstatusDocto = rs.getString("ic_estatus_docto");
									}else{
										existe="";
										icEstatusDocto = "";
									}
									rs.close();
									ps.close();

								}

								 errores = new ArrayList();
								 //campos de numero de documento Final
								if(documento.equals("")){
									errores.add(Integer.toString(noLinea));
									errores.add(documento);
									errores.add("El n�mero de documento final es un campo obligatorio.");
								}	else	if(!Comunes.esNumero(documento)){
									errores.add(Integer.toString(noLinea));
									errores.add(documento);
									errores.add("El n�mero de documento final contiene caracteres no permitidos.");
							  } else if(existe.equals("")){
									errores.add(Integer.toString(noLinea));
									errores.add(documento);
									errores.add("El n�mero de documento final no corresponde a alguno existente.");
								}else if(documento.length()>10){
									errores.add(Integer.toString(noLinea));
									errores.add(documento);
									errores.add("El n�mero de documento final excede la longitud m�xima permitida.");
								}	else 	//campo de estatus
								if("3".equals(icEstatusDocto) && !estatus.equals("2") ){
									errores.add(Integer.toString(noLinea));
									errores.add(documento);
									errores.add("El estatus es incorrecto puede ser 2 (Negociable).");
								}if("24".equals(icEstatusDocto) && !estatus.equals("2") && !estatus.equals("3")){
									errores.add(Integer.toString(noLinea));
									errores.add(documento);
									errores.add("El estatus es incorrecto puede ser 2 (Negociable) � 3 (Seleccionada Pyme).");
								}else if(!Comunes.esNumero(estatus)){
										errores.add(Integer.toString(noLinea));
										errores.add(documento);
										errores.add("El estatus contiene caracteres no permitidos.");
								}else if(estatus.length()>2){
										errores.add(Integer.toString(noLinea));
										errores.add(documento);
										errores.add("El estatus excede la longitud m�xima permitida.");
								}else if(estatus.equals("")){
									errores.add(Integer.toString(noLinea));
									errores.add(documento);
									errores.add("El estatus es un campo obligatorio.");
								}else if(causa.equals("")){
									errores.add(Integer.toString(noLinea));
									errores.add(causa);
									errores.add("El causa es un campo obligatorio.");
								}else	if(causa.length()>255){
								  errores.add(Integer.toString(noLinea));
									errores.add(documento);
									errores.add("La causa excede la longitud m�xima permitida.");
								}


				log.debug(" errores "+errores);

					if(errores.size()>0){
							conError ++;
						}else{
								aprocesar = new ArrayList();
								aprocesar.add(Integer.toString(noLinea));
								aprocesar.add(documento);
								aprocesar.add(estatus);
								aprocesar.add(causa);
						}

				respuesta = new ArrayList();
				respuesta.add(errores);
				respuesta.add(aprocesar);


			return respuesta;
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new AppException("Error ValidaLineaRechazo", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("ValidaLineaRechazo(S)");
		}
	}


/**
	 *Metodo que procesa las solicitudes
	 * @throws netropology.utilerias.AppException
	 * @param archivo
	 */

	public List procesarRechazosMasivo(List procesar, String nombreArchivo, String total) throws AppException{

		log.debug(" procesarRechazosMasivo (E)");

		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		boolean ok = true;
		List datosAcuse = new ArrayList();
	  StringBuffer	query = new StringBuffer();
		int procesados = procesar.size();
		String ic_rechazo ="";
		try {
      con.conexionDB();

		//cambia el estatus de las solicitudes  de estatus
		// 3-Seleccionado Pyme
		// 9-Vencido sin Operar � 20 rechazado IF
		// y en la tabla de Bitacora
			log.debug("procesar  "+procesar);
			if(procesar.size()>0){
				for(int i= 0; i<procesar.size(); i++) {

					List datos = (List)procesar.get(i);

					String documento =(String)datos.get(1);
					String estatus =(String)datos.get(2);
					String causa =(String)datos.get(3);

					log.debug("estatus  "+estatus);
					log.debug(" documento "+documento);
					log.debug("causa "+causa);

					query = new StringBuffer();
					query.append(" UPDATE DIS_DOCUMENTO ");
					query.append(" SET ic_estatus_docto = ? ");
					query.append(" WHERE ic_documento = ? ");

					ps = con.queryPrecompilado(query.toString());
					ps.setString(1, estatus);
					ps.setString(2, documento);
					ps.executeUpdate();
					ps.close();

					if(estatus.equals("9")){ 	estatus ="30";	} //Seleccionada Pyme a Vencido sin Operar
					if(estatus.equals("20")){  estatus ="14";	} //Seleccionada Pyme a Rechazado IF

					if(estatus.equals("3")){ 	estatus ="34";	} //En Proceso de Autorizacion IF a Seleccionada Pyme
					if(estatus.equals("2")){  estatus ="2";	} //Negociable

					query = new StringBuffer();
					query.append(" INSERT INTO DIS_CAMBIO_ESTATUS ");
					query.append(" ( dc_fecha_cambio,ic_documento,ic_cambio_estatus,ct_cambio_motivo)");
					query.append(" VALUES (sysdate,?, ?, ? )");

					ps = con.queryPrecompilado(query.toString());
					ps.setString(1, documento);
					ps.setString(2, estatus);
					ps.setString(3, causa);
					ps.executeUpdate();
					ps.close();

				}

		}
			// obtiene el numero consecutivo
			query = new StringBuffer();
			query.append(" select SEQ_DIS_RECHAZO_SOLIC.NEXTVAL	AS IC_RECHAZO  ");
			query.append(" from DUAL ");
			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			while(rs.next()){
			ic_rechazo = rs.getString("IC_RECHAZO");
			}
			rs.close();
			ps.close();

			log.debug("ic_rechazo  "+ic_rechazo);

			// se inserta en la tabla de dis_rechazo_solic los datos correspondiente
			// para el acuse de la carga
			query = new StringBuffer();
			query.append(" INSERT INTO DIS_RECHAZO_SOLIC ");
      query.append(" ( IC_RECHAZO,  DF_CARGA, CG_NOMARCHIVO, FN_TOTAL, FN_PROCESADOS)");
			query.append(" VALUES (?,sysdate,?, ?, ?  )");

			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, ic_rechazo);
			ps.setString(2, nombreArchivo);
			ps.setString(3, total);
			ps.setInt(4, procesados);
			ps.executeUpdate();
			ps.close();


			//seleccion de los datos capturados para ser mostrados en el Acuse
			query = new StringBuffer();
			query.append(" select IC_RECHAZO,  ");
			query.append(" TO_CHAR(DF_CARGA ,'dd/mm/yyyy') as DF_CARGA,");
			query.append(" CG_NOMARCHIVO, FN_TOTAL, FN_PROCESADOS  ");
			query.append(" from DIS_RECHAZO_SOLIC ");
			query.append(" where IC_RECHAZO = ? ");


			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, ic_rechazo);
			rs = ps.executeQuery();
			while(rs.next()){

				datosAcuse.add(rs.getString("FN_TOTAL"));
				datosAcuse.add(rs.getString("FN_PROCESADOS"));
				datosAcuse.add(rs.getString("IC_RECHAZO"));
				datosAcuse.add(rs.getString("CG_NOMARCHIVO"));
				datosAcuse.add(rs.getString("DF_CARGA"));

			}
			rs.close();
			ps.close();

			log.debug("datosAcuse------"+datosAcuse);


			return datosAcuse;
		}catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new AppException("Error procesarRechazosMasivo", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("procesarRechazosMasivo(S)");
		}
	}
	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return
	 * @param ic_epo, ic_pyme
	 */
	public String getTipoCredito(String ic_epo, String ic_pyme)	{
	log.info("getTipoCredito");

		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String cgTipoCredito = "";

		try {
      con.conexionDB();
		qrySentencia = new StringBuffer();
		qrySentencia.append("SELECT CG_TIPO_CREDITO" +
									" FROM COMREL_PYME_EPO_X_PRODUCTO " +
									" WHERE IC_PRODUCTO_NAFIN = ?" +
									" AND IC_EPO = ? ");
		if(ic_pyme != null && !ic_pyme.equals("")){
			qrySentencia.append(" AND IC_PYME = ? ");
		}

		log.debug("qrySentencia " +qrySentencia.toString());
		log.debug("ic_pyme: "+ic_pyme);
		log.debug("ic_epo:"+ic_epo);

		ps = 	con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, 4); //4=Financiamiento a Distribuidores
		ps.setInt(2, Integer.parseInt(ic_epo));
		if(ic_pyme != null && !ic_pyme.equals("")){
			ps.setInt(3, Integer.parseInt(ic_pyme));
		}

      rs = ps.executeQuery();
      ps.clearParameters();

      if(rs.next())	{
        cgTipoCredito = rs.getString("CG_TIPO_CREDITO")==null?"":rs.getString("CG_TIPO_CREDITO");
      }
		rs.close();
      ps.close();

			log.debug("cgTipoCredito" +cgTipoCredito);

		} catch(Exception e) {
			log.error("getTipoCredito(Exception) "+e);
			throw new AppException("Error al obtener el tipo de cr�dito",e);
		} finally {
			con.cierraConexionDB();
			log.info(" getTipoCredito");
		}

		return cgTipoCredito;
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return
	 * @param ic_pyme
	 */
	public String getTiposCredito(String ic_pyme, String tiposCredito)	{
	log.info("getTiposCredito");

		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String cgTiposCredito = "";

		try {
      con.conexionDB();
		qrySentencia = new StringBuffer();
      qrySentencia.append("	SELECT CG_TIPO_CREDITO,COUNT(*)	FROM COMREL_PYME_EPO_X_PRODUCTO" +
						"  WHERE IC_PYME = ? AND IC_PRODUCTO_NAFIN = ?	GROUP BY CG_TIPO_CREDITO");
		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, Integer.parseInt(ic_pyme));
		ps.setInt(2, 4); //4=Financiamiento a Distribuidores
		rs = ps.executeQuery();
		while(rs.next()){
			if(!"".equals(tiposCredito)&&rs.getInt(2)>0)
				cgTiposCredito = "A";
			if("".equals(tiposCredito)&&rs.getInt(2)>0)
				cgTiposCredito = rs.getString(1);
		}
		rs.close();
      ps.close();
		log.debug("cgTiposCredito" +cgTiposCredito);
		} catch(Exception e) {
			log.error("getTiposCredito(Exception) "+e);
			throw new AppException("Error al obtener el tipo de cr�dito",e);
		} finally {
			con.cierraConexionDB();
			log.info(" getTiposCredito");
		}

		return cgTiposCredito;
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return
	 * @param ic_pyme
	 */
	public String getTipoConversion(String ic_pyme)	{
	log.info("getTipoConversion");

		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String cgTipoConversion = "";

		try {
      con.conexionDB();
		qrySentencia = new StringBuffer();
      qrySentencia.append("	SELECT nvl(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION)" +
						"  FROM COMREL_PYME_EPO_X_PRODUCTO PEP" +
						"  ,COMREL_PRODUCTO_EPO PE"+
						"  ,COMCAT_PRODUCTO_NAFIN PN"+
						"  WHERE PEP.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
						"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
						"  AND PE.IC_EPO = PEP.IC_EPO"+
						"  AND PEP.IC_PYME = ? " +
						"  AND PEP.IC_PRODUCTO_NAFIN = ?"+
						"  AND PE.IC_PRODUCTO_NAFIN = ?");
		log.debug("qrySentencia= " + qrySentencia);
		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, Integer.parseInt(ic_pyme));
		ps.setInt(2, 4); //4=Financiamiento a Distribuidores
		ps.setInt(3, 4); //4=Financiamiento a Distribuidores
		rs = ps.executeQuery();

		while(rs.next()){
			if("P".equals(rs.getString(1)))
				cgTipoConversion = rs.getString(1);
		}
		log.debug("cgTiposConverion" +cgTipoConversion);
		rs.close();
		ps.close();
		} catch(Exception e) {
			log.error("getTipoConversion(Exception) "+e);
			throw new AppException("Error al obtener parametro tipo conversion",e);
		} finally {
        con.cierraConexionDB();
			log.info(" getTipoConversion");
		}

		return cgTipoConversion;
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return
	 * @param ic_epo
	 */
	public String getEpoTipoCobroInt(String ic_epo)	{
	log.info("getEpoTipoCobroInt");

		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String rs_epo_tipo_cobro_int = "";

		try {
      con.conexionDB();
		qrySentencia = new StringBuffer();
      qrySentencia.append("	SELECT NVL (cpe.ic_tipo_cobro_interes, cpn.ic_tipo_cobro_interes) "   +
									"	FROM comrel_producto_epo cpe, comcat_producto_nafin cpn"   +
									"	WHERE cpe.ic_producto_nafin = cpn.ic_producto_nafin"   +
									"	AND cpe.ic_producto_nafin = ? "   +
									"	AND cpe.ic_epo = ? ");
		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, 4); //4=Financiamiento a Distribuidores
		ps.setInt(2, Integer.parseInt(ic_epo));
		rs = ps.executeQuery();

		if (rs.next())	{
			rs_epo_tipo_cobro_int = rs.getString(1)==null?"":rs.getString(1).trim();
		}

		rs.close();
		ps.close();

		log.debug("rs_epo_tipo_cobro_int" +rs_epo_tipo_cobro_int);
		} catch(Exception e) {
			log.error("getEpoTipoCobroInt(Exception) "+e);
			throw new AppException("Error al obtener tipo cobro de inter�s",e);
		} finally {
			con.cierraConexionDB();
			log.info("getEpoTipoCobroInt");
		}
		return rs_epo_tipo_cobro_int;
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return parametro
	 * @param ic_epo
	 */
	public String getEpoTipoCobroInt_b(String ic_epo)	{
	log.info("getEpoTipoCobroInt_b");

		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String rs_epo_tipo_cobro_int = "";

		try {
      con.conexionDB();
		qrySentencia = new StringBuffer();
      qrySentencia.append("	SELECT NVL (cpe.cg_responsable_interes, cpn.cg_responsable_interes) "   +
									"	FROM comrel_producto_epo cpe, comcat_producto_nafin cpn"   +
									"	WHERE cpe.ic_producto_nafin = cpn.ic_producto_nafin"   +
									"	AND cpe.ic_producto_nafin = ? "   +
									"	AND cpe.ic_epo = ? ");
		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, 4); //4=Financiamiento a Distribuidores
		ps.setInt(2, Integer.parseInt(ic_epo));
		rs = ps.executeQuery();

		if (rs.next())	{
			rs_epo_tipo_cobro_int = rs.getString(1)==null?"":rs.getString(1).trim();
		}

		rs.close();
		ps.close();

		log.debug("rs_epo_tipo_cobro_int" +rs_epo_tipo_cobro_int);
		} catch(Exception e) {
			log.error("getEpoTipoCobroInt_b(Exception) "+e);
			throw new AppException("Error al obtener tipo cobro de inter�s",e);
		} finally {
			con.cierraConexionDB();
			log.info("getEpoTipoCobroInt_b");
		}
		return rs_epo_tipo_cobro_int;
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return
	 * @param ic_epo,ic_pyme,
	 */
	public String getValidaIn(String ic_epo, String ic_pyme)	{
	log.info("getValidaIn");

		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		boolean epo = false;
		String credito = "";
		String validaIn = "1,2,3,4";

		try {
      con.conexionDB();
		qrySentencia = new StringBuffer();

      qrySentencia.append("  SELECT cg_tipo_credito FROM comrel_pyme_epo_x_producto pep "	+
									" WHERE pep.ic_producto_nafin = ?");
		if(ic_epo != null && !"".equals(ic_epo)){
			qrySentencia.append(" AND pep.ic_epo = ? ");
			epo = true;
		}
		if(ic_pyme != null && !"".equals(ic_pyme)){
			qrySentencia.append(" AND pep.ic_pyme = ? ");
		}
		log.debug("qrySentencia " +qrySentencia.toString());
		log.debug("ic_pyme: "+ic_pyme);
		log.debug("ic_epo:"+ic_epo);

		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, 4);
		if(epo){
			ps.setInt(2, Integer.parseInt(ic_epo));
		}
		if(ic_pyme != null && !"".equals(ic_pyme)){
			if (epo){
				ps.setInt(3, Integer.parseInt(ic_pyme));
			}else{
				ps.setInt(2, Integer.parseInt(ic_pyme));
			}
		}

		rs = ps.executeQuery();
		if (rs.next()){
			credito = rs.getString(1)==null?"":rs.getString(1).trim();
		}
		if ("C".equals(credito))	{
			validaIn = "1,2,3,4,5";
		}
		rs.close();
		ps.close();

		log.debug("validaIn" +validaIn);
		} catch(Exception e) {
			log.error("getValidaIn(Exception) "+e);
			throw new AppException("Error al obtener los valores de Modalidad de plazo",e);
		} finally {
        con.cierraConexionDB();
			log.info("getValidaIn");
		}
		return validaIn;
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return
	 * @param ic_pyme, ic_if
	 */
	public Hashtable getNombres_pyme_if(String ic_pyme, String ic_if)	{
	log.info("getNombres_pyme_if");
	Hashtable datos = new Hashtable();

	StringBuffer qrySentencia;
	PreparedStatement	ps	= null;
	ResultSet rs	= null;
	AccesoDB con	= new AccesoDB();
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append("	SELECT py.cg_razon_social AS nombre_pyme, ci.cg_razon_social AS nombre_if"+
										"	FROM comcat_pyme py,comcat_if ci WHERE py.ic_pyme = ? ");
			if (!ic_if.equals("")){
				qrySentencia.append(" AND ci.ic_if = ? ");
			}
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, Integer.parseInt(ic_pyme));
			ps.setInt(2, Integer.parseInt(ic_if));
			rs = ps.executeQuery();
			if (rs.next()){
				datos.put("NOMBRE_PYME",rs.getString("NOMBRE_PYME")==null?"":rs.getString("NOMBRE_PYME").trim());
				datos.put("NOMBRE_IF",rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF").trim());
			}
			rs.close();
			if(ps!=null){ ps.close();}
		} catch(Exception e) {
			log.error("getNombres_pyme_if(Exception) "+e);
			throw new AppException("Error al obtener la informacion del cliente",e);
		} finally {
        con.cierraConexionDB();
		}
		return datos;
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return
	 * @param ic_pyme, ic_if, ic_moneda	(requeridos)
	 */
	public Vector getLineaCredito(String ic_pyme, String ic_if, String ic_moneda)	{
	log.info("getLineaCredito");
    Vector 		vecFilas	= new Vector();
    Vector		vecColumnas	= null;

	StringBuffer qrySentencia;
	PreparedStatement	ps	= null;
	ResultSet rs	= null;
	AccesoDB con	= new AccesoDB();
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append("SELECT DISTINCT lc.ic_linea_credito_dm, cif.cg_razon_social AS banco_ref, "+
										"                lc.fn_monto_autorizado_total AS monto_auto, "+
										"                TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') "+
										"                      AS fecha_venc, "+
										"                cm.cd_nombre AS moneda, "+
										"                lc.fn_monto_autorizado_total - lc.fn_saldo_total AS dispuesto, "+
										"                lc.fn_saldo_total AS disponible, el.cd_descripcion "+
										"	FROM	dis_linea_credito_dm lc, "+
										"  		comcat_if cif, "+
										"			comcat_moneda cm, "+
										"			comcat_estatus_linea el "+
										"	WHERE cif.ic_if = lc.ic_if "+
										"	AND cm.ic_moneda = lc.ic_moneda "+
										"	AND lc.ic_epo = ? "+
										"  AND lc.ic_if = ? "+
										"	AND lc.cg_tipo_solicitud IN (?, ?) "+
										"	AND lc.ic_estatus_linea = el.ic_estatus_linea "+
										"	AND lc.ic_linea_credito_dm NOT IN (SELECT ic_linea_credito_dm_padre "+
										"                                        FROM dis_linea_credito_dm "+
										"                                       WHERE cg_tipo_solicitud = ? ) ");
			if (ic_moneda != null && !ic_moneda.equals("")){
				qrySentencia.append("	AND lc.ic_moneda = ? ");
			}
			log.info("El query es- - - - - - -  "+qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, Integer.parseInt(ic_pyme));
			ps.setInt(2, Integer.parseInt(ic_if));
			ps.setString(3,"I");
			ps.setString(4,"R");
			ps.setString(5,"R");
			if (ic_moneda != null && !ic_moneda.equals("")){
				ps.setInt(6, Integer.parseInt(ic_moneda));
			}

			rs = ps.executeQuery();
			while(rs.next()){
					vecColumnas = new Vector();
					  vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
					  vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
					  vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
					  vecColumnas.addElement((rs.getString(4)==null)?"":rs.getString(4));
					vecColumnas.addElement((rs.getString(5)==null)?"":rs.getString(5));
					  vecColumnas.addElement((rs.getString(6)==null)?"":rs.getString(6));
					  vecColumnas.addElement((rs.getString(7)==null)?"":rs.getString(7));
					  vecColumnas.addElement((rs.getString(8)==null)?"":rs.getString(8));
					  vecFilas.addElement(vecColumnas);
			}
			rs.close();
			if(ps!=null){ ps.close();}
		} catch(Exception e) {
			log.error("getLineaCredito(Exception) "+e);
			throw new AppException("Error al obtener la informacion de la linea de credito de la Cliente",e);
		} finally {
        con.cierraConexionDB();
		}
		return vecFilas;
	}

	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param noPyme
	 */
	public List repCambioEstatus(String noPyme, String noEstatus, String tiposCredito) throws NafinException {
		log.info("repCambioEstatus (E)");
		PreparedStatement	ps23	= null;
		ResultSet rs23	= null;
		PreparedStatement	ps2	= null;
		ResultSet rs2	= null;
		PreparedStatement	ps22	= null;
		ResultSet rs22	= null;
		PreparedStatement	ps21	= null;
		ResultSet rs21	= null;
		PreparedStatement	ps24	= null;
		ResultSet rs24	= null;

		AccesoDB con = new AccesoDB();
		List reportes =  new ArrayList();
		String qrySentenciaCCC = "";
		String qrySentenciaDM = "";
		String strQuery 		= "";
		JSONArray registros23 = new JSONArray();
		HashMap columnas23 = null;
		JSONArray registros2 = new JSONArray();
		HashMap columnas2 = null;
		JSONArray registros22 = new JSONArray();
		HashMap columnas22 = null;
		JSONArray registros21 = new JSONArray();
		HashMap columnas21 = null;
		JSONArray registros24 = new JSONArray();
		HashMap columnas24 = null;
		List conditions;
		conditions 		= new ArrayList();
		//totales
		int		totalDoctosMN		= 0;
		int 	totalDoctosUSD		= 0;
		double	totalMontoMN		= 0;
		double	totalMontoUSD		= 0;
		double	totalMontoValuadoMN	= 0;
		double	totalMontoValuadoUSD= 0;
		double	totalMontoCreditoMN	= 0;
		double	totalMontoCreditoUSD= 0;
		String icMoneda	= "";
		HashMap totales22t = null;
		HashMap totales14t = null;
		HashMap totales2t = null;
		HashMap totales21t = null;
		HashMap totales24t = null;
		JSONArray totales22r = new JSONArray();
		JSONArray totales14r = new JSONArray();
		JSONArray totales2r= new JSONArray();
		JSONArray totales21r = new JSONArray();
		JSONArray totales24r = new JSONArray();

		try {
      con.conexionDB();


			if(noEstatus.equals("22") ||"".equals(noEstatus)) { //Negociable a Vencido sin Operar
					totalDoctosMN		= 0;
				 	totalDoctosUSD		= 0;
					totalMontoMN		= 0;
					totalMontoUSD		= 0;
					totalMontoValuadoMN	= 0;
					totalMontoValuadoUSD= 0;
					totalMontoCreditoMN	= 0;
					totalMontoCreditoUSD= 0;


			strQuery =	" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,D.cc_acuse "+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND D.IC_ESTATUS_DOCTO = 9"+
					" AND CE.IC_CAMBIO_ESTATUS = 22"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+
					//" and d.ic_documento in(4361)"+
					" AND D.IC_PYME = "+new Integer(noPyme);

					ps22 = con.queryPrecompilado(strQuery.toString());
					rs22 = ps22.executeQuery();
					while(rs22.next()){
						double monto 				= rs22.getDouble("FN_MONTO");
						double tipoCambio			= rs22.getDouble("TIPO_CAMBIO");
						String porcDescuento		= (rs22.getString("FN_PORC_DESCUENTO")==null)?"0":rs22.getString("FN_PORC_DESCUENTO");
						double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
						double montoValuado		= monto*tipoCambio;
						String bandeVentaCartera			= (rs22.getString("CG_VENTACARTERA")==null)?"":rs22.getString("CG_VENTACARTERA");
						String modoPlazo			= (rs22.getString("MODO_PLAZO")==null)?"":rs22.getString("MODO_PLAZO");
						if (bandeVentaCartera.equals("S") ) {
							modoPlazo ="";
						}

						 icMoneda	= (rs22.getString("IC_MONEDA")==null)?"":rs22.getString("IC_MONEDA");

						if("1".equals(icMoneda)){
							totalDoctosMN ++;
							totalMontoMN += monto;
							totalMontoValuadoMN += montoValuado;
						}else if("54".equals(icMoneda)){
							totalDoctosUSD ++;
							totalMontoUSD += monto;
							totalMontoValuadoUSD += montoValuado;
						}


						columnas22 = new HashMap();
						columnas22.put("NOMBRE_EPO",(rs22.getString("NOMBRE_EPO")==null)?"":rs22.getString("NOMBRE_EPO"));
						columnas22.put("CC_ACUSE",(rs22.getString("CC_ACUSE")==null)?"":rs22.getString("CC_ACUSE"));
						columnas22.put("IG_NUMERO_DOCTO",(rs22.getString("IG_NUMERO_DOCTO")==null)?"":rs22.getString("IG_NUMERO_DOCTO"));
						columnas22.put("DF_FECHA_EMISION",(rs22.getString("DF_FECHA_EMISION")==null)?"":rs22.getString("DF_FECHA_EMISION"));
						columnas22.put("DF_FECHA_VENC",(rs22.getString("DF_FECHA_VENC")==null)?"":rs22.getString("DF_FECHA_VENC"));
						columnas22.put("DF_CARGA",(rs22.getString("DF_CARGA")==null)?"":rs22.getString("DF_CARGA"));
						columnas22.put("IG_PLAZO_DOCTO",(rs22.getString("IG_PLAZO_DOCTO")==null)?"":rs22.getString("IG_PLAZO_DOCTO"));
						columnas22.put("MONEDA",(rs22.getString("MONEDA")==null)?"":rs22.getString("MONEDA"));
						columnas22.put("FN_MONTO",(rs22.getString("FN_MONTO")==null)?"":rs22.getString("FN_MONTO"));
						columnas22.put("IG_PLAZO_DESCUENTO",(rs22.getString("IG_PLAZO_DESCUENTO")==null)?"":rs22.getString("IG_PLAZO_DESCUENTO"));
						columnas22.put("FN_PORC_DESCUENTO",(rs22.getString("FN_PORC_DESCUENTO")==null)?"":rs22.getString("FN_PORC_DESCUENTO"));
						columnas22.put("MONTO_PORC_DESCUENTO",(Double.toString (montoDescontar)));
						columnas22.put("TIPO_CONVERSION",(rs22.getString("TIPO_CONVERSION")==null)?"":rs22.getString("TIPO_CONVERSION"));
						columnas22.put("TIPO_CAMBIO",(rs22.getString("TIPO_CAMBIO")==null)?"":rs22.getString("TIPO_CAMBIO"));
						columnas22.put("MONT_VALUADO",Double.toString (montoValuado));
						columnas22.put("MODO_PLAZO",(modoPlazo));
						columnas22.put("FECHA_CAMBIO",(rs22.getString("FECHA_CAMBIO")==null)?"":rs22.getString("FECHA_CAMBIO"));
						registros22.add(columnas22);
					}
					rs22.close();
					ps22.close();

					if(totalDoctosMN >0 ){
								totales22t = new HashMap();
								totales22t.put("MONEDA","MONEDA NACIONAL");
								totales22t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
								totales22t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
								totales22t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));
						}
						if(totalDoctosUSD >0 ){
								totales22t = new HashMap();
								totales22t.put("MONEDA","MONEDA DOLAR");
								totales22t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
								totales22t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
								totales22t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));
						}
						totales22r.add(totales22t);
			}


			if( (noEstatus.equals("23"))  || ( noEstatus.equals("34")  ) ) {
				totalDoctosMN		= 0;
				totalDoctosUSD		= 0;
				totalMontoMN		= 0;
				totalMontoUSD		= 0;
				totalMontoValuadoMN	= 0;
				totalMontoValuadoUSD= 0;
				totalMontoCreditoMN	= 0;
				totalMontoCreditoUSD= 0;

			qrySentenciaDM =
				"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
				"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
				"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
				"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
				"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
				"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
				"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
				"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
				"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
				"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
				"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
				"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
				"       i.cg_razon_social AS nombre_if, "+
				"       DECODE (pp.cg_tipo_credito, "+
				"          'D', 'Descuento y/o Factoraje', 'C', 'Credito en Cuenta Corriente') AS tipo_credito, "+
				"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
				"       tci.cd_descripcion AS tipo_cobro_int, "+
				"  		DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || ds.CG_REL_MAT||' '||ds.FN_PUNTOS) as referencia_int,"+
				"       DECODE ( ds.cg_rel_mat, "+
				"          '+', fn_valor_tasa + fn_puntos, "+
				"          '-', fn_valor_tasa - fn_puntos, "+
				"          '*', fn_valor_tasa * fn_puntos, "+
				"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
				"       ds.fn_importe_interes AS monto_tasa_int, d.cc_acuse, "+
				"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
				"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
				"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_seleccion,"+
				"       TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio,"+
				"		ce.ct_cambio_motivo as causa"+
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"  FROM dis_documento d, "+
				"       comcat_pyme py, "+
				"       comcat_moneda m, "+
				"       comrel_producto_epo pe, "+
				"       comcat_producto_nafin pn, "+
				"       com_tipo_cambio tc, "+
				"       comcat_tipo_financiamiento tf, "+
				"       comcat_estatus_docto ed, "+
				"       dis_linea_credito_dm lc, "+
				"       comcat_epo e, "+
				"       dis_docto_seleccionado ds, "+
				"       comcat_if i, "+
				"       comrel_pyme_epo_x_producto pp, "+
				"       comcat_tipo_cobro_interes tci, "+
				"       comcat_tasa ct, "+
				"       dis_cambio_estatus ce "+
				" WHERE d.ic_pyme = py.ic_pyme "+
				"   AND ds.ic_documento = d.ic_documento "+
				"   AND ce.ic_documento = d.ic_documento "+
				"   AND d.ic_moneda = m.ic_moneda "+
				"   AND pn.ic_producto_nafin = 4 "+
				"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
				"   AND pe.ic_epo = d.ic_epo "+
				"   AND i.ic_if = lc.ic_if "+
				"   AND e.ic_epo = d.ic_epo "+
				"   AND pp.ic_epo = e.ic_epo "+
				"   AND pp.ic_pyme = py.ic_pyme "+
				"   AND pp.ic_producto_nafin = 4 "+
				"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
				"                         FROM com_tipo_cambio "+
				"                        WHERE ic_moneda = m.ic_moneda) "+
				"   AND m.ic_moneda = tc.ic_moneda "+
				"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
				"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
				"   AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
				"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
				"   AND ct.ic_tasa = ds.ic_tasa "+
				"   AND ce.ic_cambio_estatus ="+noEstatus+
				"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
				" AND D.IC_PYME = "+new Integer(noPyme);



			qrySentenciaCCC = " SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
			"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
			"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
			"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
			"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
			"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
			"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
			"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
			"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
			"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
			"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
			"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
			"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
			"       i.cg_razon_social AS nombre_if, "+
			"       DECODE (pp.cg_tipo_credito, "+
			"          'D', 'Descuento y/o Factoraje', 'C', 'Credito en Cuenta Corriente') AS tipo_credito, "+
			"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
			"       tci.cd_descripcion AS tipo_cobro_int, "+
			"       ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos  AS referencia_int, "+
			"       DECODE ( ds.cg_rel_mat, "+
			"          '+', fn_valor_tasa + fn_puntos, "+
			"          '-', fn_valor_tasa - fn_puntos, "+
			"          '*', fn_valor_tasa * fn_puntos, "+
			"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
			"       ds.fn_importe_interes AS monto_tasa_int, d.cc_acuse, "+
			"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
			"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
			"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_seleccion,"+
			"       TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio,"+
			"		ce.ct_cambio_motivo as causa"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"  FROM dis_documento d, "+
			"       comcat_pyme py, "+
			"       comcat_moneda m, "+
			"       comrel_producto_epo pe, "+
			"       comcat_producto_nafin pn, "+
			"       com_tipo_cambio tc, "+
			"       comcat_tipo_financiamiento tf, "+
			"       comcat_estatus_docto ed, "+
			"       com_linea_credito lc, "+
			"       comcat_epo e, "+
			"       dis_docto_seleccionado ds, "+
			"       comcat_if i, "+
			"       comrel_pyme_epo_x_producto pp, "+
			"       comcat_tipo_cobro_interes tci, "+
			"       comcat_tasa ct, "+
			"       dis_cambio_estatus ce "+
			" WHERE d.ic_pyme = py.ic_pyme "+
			"   AND ds.ic_documento = d.ic_documento "+
			"   AND ce.ic_documento = d.ic_documento "+
			"   AND d.ic_moneda = m.ic_moneda "+
			"   AND pn.ic_producto_nafin = 4 "+
			"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
			"   AND pe.ic_epo = d.ic_epo "+
			"   AND i.ic_if = lc.ic_if "+
			"   AND e.ic_epo = d.ic_epo "+
			"   AND pp.ic_epo = e.ic_epo "+
			"   AND pp.ic_pyme = py.ic_pyme "+
			"   AND pp.ic_producto_nafin = 4 "+
			"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
			"                         FROM com_tipo_cambio "+
			"                        WHERE ic_moneda = m.ic_moneda) "+
			"   AND m.ic_moneda = tc.ic_moneda "+
			"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
			"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
			"   AND d.ic_linea_credito = lc.ic_linea_credito "+
			"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
			"   AND ct.ic_tasa = ds.ic_tasa "+
			"   AND ce.ic_cambio_estatus ="+noEstatus+
			"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
			//"and d.ic_documento in(3915,3985)"+
			" AND D.IC_PYME =  "+new Integer(noPyme);


				if("D".equals(tiposCredito)){
					strQuery = qrySentenciaDM;
				}else if("C".equals(tiposCredito)){
					strQuery = qrySentenciaCCC;
				}else {
						strQuery = qrySentenciaDM +" UNION ALL "+qrySentenciaCCC;
				}
			System.out.println("strQuery:::::::::..."+strQuery);
			ps23 = con.queryPrecompilado(strQuery.toString());
			rs23 = ps23.executeQuery();
			while(rs23.next()){
				double monto 				= rs23.getDouble("FN_MONTO");
				double tipoCambio			= rs23.getDouble("TIPO_CAMBIO");
				String porcDescuento		= (rs23.getString("FN_PORC_DESCUENTO")==null)?"0":rs23.getString("FN_PORC_DESCUENTO");
				double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				double montoValuado		= monto*tipoCambio;
				String bandeVentaCartera			= (rs23.getString("CG_VENTACARTERA")==null)?"":rs23.getString("CG_VENTACARTERA");
				String modoPlazo			= (rs23.getString("MODO_PLAZO")==null)?"":rs23.getString("MODO_PLAZO");
				if (bandeVentaCartera.equals("S") ) {
					modoPlazo ="";
				}
				double montoCredito		= rs23.getDouble("MONTO_CREDITO");
				double montoTasaInt		= rs23.getDouble("MONTO_TASA_INT");
				double MontoTotalTasaInte = montoCredito+montoTasaInt;
				icMoneda	= (rs23.getString("IC_MONEDA")==null)?"":rs23.getString("IC_MONEDA");
				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += montoCredito;
				}else if("54".equals(icMoneda)){
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += montoCredito;
				}
				columnas23 = new HashMap();
				columnas23.put("NOMBRE_EPO",(rs23.getString("NOMBRE_EPO")==null)?"":rs23.getString("NOMBRE_EPO"));
				columnas23.put("CC_ACUSE",(rs23.getString("CC_ACUSE")==null)?"":rs23.getString("CC_ACUSE"));
				columnas23.put("IG_NUMERO_DOCTO",(rs23.getString("IG_NUMERO_DOCTO")==null)?"":rs23.getString("IG_NUMERO_DOCTO"));
				columnas23.put("DF_FECHA_EMISION",(rs23.getString("DF_FECHA_EMISION")==null)?"":rs23.getString("DF_FECHA_EMISION"));
				columnas23.put("DF_FECHA_VENC",(rs23.getString("DF_FECHA_VENC")==null)?"":rs23.getString("DF_FECHA_VENC"));
				columnas23.put("DF_CARGA",(rs23.getString("DF_CARGA")==null)?"":rs23.getString("DF_CARGA"));
				columnas23.put("IG_PLAZO_DOCTO",(rs23.getString("IG_PLAZO_DOCTO")==null)?"":rs23.getString("IG_PLAZO_DOCTO"));
				columnas23.put("MONEDA",(rs23.getString("MONEDA")==null)?"":rs23.getString("MONEDA"));
				columnas23.put("FN_MONTO",(rs23.getString("FN_MONTO")==null)?"":rs23.getString("FN_MONTO"));
				columnas23.put("IG_PLAZO_DESCUENTO",(rs23.getString("IG_PLAZO_DESCUENTO")==null)?"":rs23.getString("IG_PLAZO_DESCUENTO"));
				columnas23.put("FN_PORC_DESCUENTO",(rs23.getString("FN_PORC_DESCUENTO")==null)?"":rs23.getString("FN_PORC_DESCUENTO"));
				columnas23.put("MONTO_PORC_DESCUENTO",(Double.toString (montoDescontar)));
				columnas23.put("TIPO_CONVERSION",(rs23.getString("TIPO_CONVERSION")==null)?"":rs23.getString("TIPO_CONVERSION"));
				columnas23.put("TIPO_CAMBIO",(rs23.getString("TIPO_CAMBIO")==null)?"":rs23.getString("TIPO_CAMBIO"));
				columnas23.put("MONT_VALUADO",Double.toString (montoValuado));
				columnas23.put("MODO_PLAZO",(rs23.getString("MODO_PLAZO")==null)?"":rs23.getString("MODO_PLAZO"));
				columnas23.put("IC_DOCUMENTO",(rs23.getString("IC_DOCUMENTO")==null)?"":rs23.getString("IC_DOCUMENTO"));
				columnas23.put("MONTO_CREDITO",(rs23.getString("MONTO_CREDITO")==null)?"":rs23.getString("MONTO_CREDITO"));
				columnas23.put("IG_PLAZO_CREDITO",(rs23.getString("IG_PLAZO_CREDITO")==null)?"":rs23.getString("IG_PLAZO_CREDITO"));
				columnas23.put("FECHA_VENC_CREDITO",(rs23.getString("FECHA_VENC_CREDITO")==null)?"":rs23.getString("FECHA_VENC_CREDITO"));
				columnas23.put("FECHA_SELECCION",(rs23.getString("FECHA_SELECCION")==null)?"":rs23.getString("FECHA_SELECCION"));
				columnas23.put("NOMBRE_IF",(rs23.getString("NOMBRE_IF")==null)?"":rs23.getString("NOMBRE_IF"));
				columnas23.put("REFERENCIA_INT",(rs23.getString("REFERENCIA_INT")==null)?"":rs23.getString("REFERENCIA_INT"));
				columnas23.put("VALOR_TASA_INT",(rs23.getString("VALOR_TASA_INT")==null)?"":rs23.getString("VALOR_TASA_INT"));
				columnas23.put("MONTO_TASA_INT",(rs23.getString("MONTO_TASA_INT")==null)?"":rs23.getString("MONTO_TASA_INT"));
				columnas23.put("MONTOTOTAL_TASA_INT",Double.toString (MontoTotalTasaInte));
				columnas23.put("FECHA_CAMBIO",(rs23.getString("FECHA_CAMBIO")==null)?"":rs23.getString("FECHA_CAMBIO"));
				columnas23.put("CAUSA",(rs23.getString("CAUSA")==null)?"":rs23.getString("CAUSA"));
				registros23.add(columnas23);
			}
			rs23.close();
			ps23.close();

				if(totalDoctosMN >0 ){
					totales14t = new HashMap();
					totales14t.put("MONEDA","MONEDA NACIONAL");
					totales14t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
					totales14t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
					totales14t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));
					totales14t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoMN));
				}
				if(totalDoctosUSD >0 ){
					totales14t = new HashMap();
					totales14t.put("MONEDA","MONEDA DOLAR");
					totales14t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
					totales14t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
					totales14t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));
					totales14t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoUSD));
				}

				totales14r.add(totales14t);

		}

		if(noEstatus.equals("2") || "".equals(noEstatus) ) { //Seleccionado Pyme a Negociable
				totalDoctosMN		= 0;
				totalDoctosUSD		= 0;
				totalMontoMN		= 0;
				totalMontoUSD		= 0;
				totalMontoValuadoMN	= 0;
				totalMontoValuadoUSD= 0;
				totalMontoCreditoMN	= 0;
				totalMontoCreditoUSD= 0;

			qrySentenciaDM =
				"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
				"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
				"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
				"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
				"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
				"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
				"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
				"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
				"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
				"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
				"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
				"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
				"       i.cg_razon_social AS nombre_if, "+
				"       DECODE (pp.cg_tipo_credito, "+
				"          'D', 'Descuento y/o Factoraje', 'C', 'Credito en Cuenta Corriente') AS tipo_credito, "+
				"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
				"       tci.cd_descripcion AS tipo_cobro_int, "+
				"  		DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || ds.CG_REL_MAT||' '||ds.FN_PUNTOS) as referencia_int,"+
				"       DECODE ( ds.cg_rel_mat, "+
				"          '+', fn_valor_tasa + fn_puntos, "+
				"          '-', fn_valor_tasa - fn_puntos, "+
				"          '*', fn_valor_tasa * fn_puntos, "+
				"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
				"       ds.fn_importe_interes AS monto_tasa_int, d.cc_acuse, "+
				"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
				"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
				"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_seleccion,"+
				"       TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio,"+
				"		ce.ct_cambio_motivo as causa"+
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+ //F05-2014

				"  FROM dis_documento d, "+
				"       comcat_pyme py, "+
				"       comcat_moneda m, "+
				"       comrel_producto_epo pe, "+
				"       comcat_producto_nafin pn, "+
				"       com_tipo_cambio tc, "+
				"       comcat_tipo_financiamiento tf, "+
				"       comcat_estatus_docto ed, "+
				"       dis_linea_credito_dm lc, "+
				"       comcat_epo e, "+
				"       dis_docto_seleccionado ds, "+
				"       comcat_if i, "+
				"       comrel_pyme_epo_x_producto pp, "+
				"       comcat_tipo_cobro_interes tci, "+
				"       comcat_tasa ct, "+
				"       dis_cambio_estatus ce "+
				" WHERE d.ic_pyme = py.ic_pyme "+
				"   AND ds.ic_documento = d.ic_documento "+
				"   AND ce.ic_documento = d.ic_documento "+
				"   AND d.ic_moneda = m.ic_moneda "+
				"   AND pn.ic_producto_nafin = 4 "+
				"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
				"   AND pe.ic_epo = d.ic_epo "+
				"   AND i.ic_if = lc.ic_if "+
				"   AND e.ic_epo = d.ic_epo "+
				"   AND pp.ic_epo = e.ic_epo "+
				"   AND pp.ic_pyme = py.ic_pyme "+
				"   AND pp.ic_producto_nafin = 4 "+
				"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
				"                         FROM com_tipo_cambio "+
				"                        WHERE ic_moneda = m.ic_moneda) "+
				"   AND m.ic_moneda = tc.ic_moneda "+
				"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
				"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
				"   AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
				"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
				"   AND ct.ic_tasa = ds.ic_tasa "+
				"   AND ce.ic_cambio_estatus = 2 "+
				"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
				" AND D.IC_PYME = "+new Integer(noPyme);

		qrySentenciaCCC =
			"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
			"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
			"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
			"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
			"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
			"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
			"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
			"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
			"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
			"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
			"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
			"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
			"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
			"       i.cg_razon_social AS nombre_if, "+
			"       DECODE (pp.cg_tipo_credito, "+
			"          'D', 'Descuento y/o Factoraje', 'C', 'Credito en Cuenta Corriente') AS tipo_credito, "+
			"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
			"       tci.cd_descripcion AS tipo_cobro_int, "+
			"       ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos  AS referencia_int, "+
			"       DECODE ( ds.cg_rel_mat, "+
			"          '+', fn_valor_tasa + fn_puntos, "+
			"          '-', fn_valor_tasa - fn_puntos, "+
			"          '*', fn_valor_tasa * fn_puntos, "+
			"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
			"       ds.fn_importe_interes AS monto_tasa_int, d.cc_acuse, "+
			"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
			"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
			"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_seleccion,"+
			"       TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio,"+
			"		ce.ct_cambio_motivo as causa"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			" ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO   "+ //F05-2014

			"  FROM dis_documento d, "+
			"       comcat_pyme py, "+
			"       comcat_moneda m, "+
			"       comrel_producto_epo pe, "+
			"       comcat_producto_nafin pn, "+
			"       com_tipo_cambio tc, "+
			"       comcat_tipo_financiamiento tf, "+
			"       comcat_estatus_docto ed, "+
			"       com_linea_credito lc, "+
			"       comcat_epo e, "+
			"       dis_docto_seleccionado ds, "+
			"       comcat_if i, "+
			"       comrel_pyme_epo_x_producto pp, "+
			"       comcat_tipo_cobro_interes tci, "+
			"       comcat_tasa ct, "+
			"       dis_cambio_estatus ce "+
			" WHERE d.ic_pyme = py.ic_pyme "+
			"   AND ds.ic_documento = d.ic_documento "+
			"   AND ce.ic_documento = d.ic_documento "+
			"   AND d.ic_moneda = m.ic_moneda "+
			"   AND pn.ic_producto_nafin = 4 "+
			"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
			"   AND pe.ic_epo = d.ic_epo "+
			"   AND i.ic_if = lc.ic_if "+
			"   AND e.ic_epo = d.ic_epo "+
			"   AND pp.ic_epo = e.ic_epo "+
			"   AND pp.ic_pyme = py.ic_pyme "+
			"   AND pp.ic_producto_nafin = 4 "+
			"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
			"                         FROM com_tipo_cambio "+
			"                        WHERE ic_moneda = m.ic_moneda) "+
			"   AND m.ic_moneda = tc.ic_moneda "+
			"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
			"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
			"   AND d.ic_linea_credito = lc.ic_linea_credito "+
			"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
			"   AND ct.ic_tasa = ds.ic_tasa "+
			"   AND ce.ic_cambio_estatus = 2 "+
			"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
			//" and d.ic_documento in(4096, 4087) "+
			" AND D.IC_PYME = "+new Integer(noPyme);

			if("D".equals(tiposCredito))
				strQuery = qrySentenciaDM;
			else if("C".equals(tiposCredito))
				strQuery = qrySentenciaCCC;
			else
				strQuery = qrySentenciaDM +" UNION ALL "+qrySentenciaCCC;

		log.debug("strQuery--->"+strQuery);

			ps2 = con.queryPrecompilado(strQuery.toString());
			rs2 = ps2.executeQuery();
			while(rs2.next()){
				double monto 		= rs2.getDouble("FN_MONTO");
				double tipoCambio			= rs2.getDouble("TIPO_CAMBIO");
				String porcDescuento		= (rs2.getString("FN_PORC_DESCUENTO")==null)?"0":rs2.getString("FN_PORC_DESCUENTO");
				double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				double montoValuado		= monto*tipoCambio;
				String bandeVentaCartera			= (rs2.getString("CG_VENTACARTERA")==null)?"":rs2.getString("CG_VENTACARTERA");
				String modoPlazo			= (rs2.getString("MODO_PLAZO")==null)?"":rs2.getString("MODO_PLAZO");
				if (bandeVentaCartera.equals("S") ) {
					modoPlazo ="";
				}
				double montoCredito		= rs2.getDouble("MONTO_CREDITO");
				double montoTasaInt		= rs2.getDouble("MONTO_TASA_INT");
				double MontoTotalTasaInte = montoCredito+montoTasaInt;


				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += montoCredito;
				}else if("54".equals(icMoneda)){
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += montoCredito;
				}

				columnas2 = new HashMap();
				columnas2.put("NOMBRE_EPO",(rs2.getString("NOMBRE_EPO")==null)?"":rs2.getString("NOMBRE_EPO"));
				columnas2.put("CC_ACUSE",(rs2.getString("CC_ACUSE")==null)?"":rs2.getString("CC_ACUSE"));
				columnas2.put("IG_NUMERO_DOCTO",(rs2.getString("IG_NUMERO_DOCTO")==null)?"":rs2.getString("IG_NUMERO_DOCTO"));
				columnas2.put("DF_FECHA_EMISION",(rs2.getString("DF_FECHA_EMISION")==null)?"":rs2.getString("DF_FECHA_EMISION"));
				columnas2.put("DF_FECHA_VENC",(rs2.getString("DF_FECHA_VENC")==null)?"":rs2.getString("DF_FECHA_VENC"));
				columnas2.put("DF_CARGA",(rs2.getString("DF_CARGA")==null)?"":rs2.getString("DF_CARGA"));
				columnas2.put("IG_PLAZO_DOCTO",(rs2.getString("IG_PLAZO_DOCTO")==null)?"":rs2.getString("IG_PLAZO_DOCTO"));
				columnas2.put("MONEDA",(rs2.getString("MONEDA")==null)?"":rs2.getString("MONEDA"));
				columnas2.put("FN_MONTO",(rs2.getString("FN_MONTO")==null)?"":rs2.getString("FN_MONTO"));
				columnas2.put("IG_PLAZO_DESCUENTO",(rs2.getString("IG_PLAZO_DESCUENTO")==null)?"":rs2.getString("IG_PLAZO_DESCUENTO"));
				columnas2.put("FN_PORC_DESCUENTO",(rs2.getString("FN_PORC_DESCUENTO")==null)?"":rs2.getString("FN_PORC_DESCUENTO"));
				columnas2.put("MONTO_PORC_DESCUENTO",(Double.toString (montoDescontar)));
				columnas2.put("TIPO_CONVERSION",(rs2.getString("TIPO_CONVERSION")==null)?"":rs2.getString("TIPO_CONVERSION"));
				columnas2.put("TIPO_CAMBIO",(rs2.getString("TIPO_CAMBIO")==null)?"":rs2.getString("TIPO_CAMBIO"));
				columnas2.put("MONT_VALUADO",Double.toString (montoValuado));
				columnas2.put("MODO_PLAZO",(rs2.getString("MODO_PLAZO")==null)?"":rs2.getString("MODO_PLAZO"));
				columnas2.put("IC_DOCUMENTO",(rs2.getString("IC_DOCUMENTO")==null)?"":rs2.getString("IC_DOCUMENTO"));
				columnas2.put("MONTO_CREDITO",(rs2.getString("MONTO_CREDITO")==null)?"":rs2.getString("MONTO_CREDITO"));
				columnas2.put("IG_PLAZO_CREDITO",(rs2.getString("IG_PLAZO_CREDITO")==null)?"":rs2.getString("IG_PLAZO_CREDITO"));
				columnas2.put("FECHA_VENC_CREDITO",(rs2.getString("FECHA_VENC_CREDITO")==null)?"":rs2.getString("FECHA_VENC_CREDITO"));
				columnas2.put("FECHA_SELECCION",(rs2.getString("FECHA_SELECCION")==null)?"":rs2.getString("FECHA_SELECCION"));
				columnas2.put("NOMBRE_IF",(rs2.getString("NOMBRE_IF")==null)?"":rs2.getString("NOMBRE_IF"));
				columnas2.put("REFERENCIA_INT",(rs2.getString("REFERENCIA_INT")==null)?"":rs2.getString("REFERENCIA_INT"));
				columnas2.put("VALOR_TASA_INT",(rs2.getString("VALOR_TASA_INT")==null)?"":rs2.getString("VALOR_TASA_INT"));
				columnas2.put("MONTO_TASA_INT",(rs2.getString("MONTO_TASA_INT")==null)?"":rs2.getString("MONTO_TASA_INT"));
				columnas2.put("MONTOTOTAL_TASA_INT",Double.toString (MontoTotalTasaInte));
				columnas2.put("FECHA_CAMBIO",(rs2.getString("FECHA_CAMBIO")==null)?"":rs2.getString("FECHA_CAMBIO"));
				columnas2.put("CAUSA",(rs2.getString("CAUSA")==null)?"":rs2.getString("CAUSA"));
				columnas2.put("POR_AFRO",(rs2.getString("POR_AFRO")==null)?"":rs2.getString("POR_AFRO")); //F05-2014
				registros2.add(columnas2);
			}
			rs2.close();
			ps2.close();

			if(totalDoctosMN >0 ){
					totales2t = new HashMap();
					totales2t.put("MONEDA","MONEDA NACIONAL");
					totales2t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
					totales2t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
					totales2t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));
					totales2t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoMN));
				}
				if(totalDoctosUSD >0 ){
					totales2t = new HashMap();
					totales2t.put("MONEDA","MONEDA DOLAR");
					totales2t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
					totales2t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
					totales2t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));
					totales2t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoUSD));
				}
				totales2r.add(totales2t);
		}

			if(noEstatus.equals("21") ||"".equals(noEstatus)) { //Modificaciones

					totalDoctosMN		= 0;
				 	totalDoctosUSD		= 0;
					totalMontoMN		= 0;
					totalMontoUSD		= 0;
					totalMontoValuadoMN	= 0;
					totalMontoValuadoUSD= 0;
					totalMontoCreditoMN	= 0;
					totalMontoCreditoUSD= 0;

				strQuery =	" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,D.cc_acuse "+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					"	,to_char(CE.DF_FECHA_EMISION_ANTERIOR,'dd/mm/yyyy') as FECHA_EMISION_ANT"+
					"	,to_char(CE.DF_FECHA_VENC_ANTERIOR,'dd/mm/yyyy') as FECHA_VENC_ANT"+
					"	,CE.fn_monto_anterior"+
					"	,TF2.cd_descripcion AS MODO_PLAZO_ANT"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF2"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND CE.IC_CAMBIO_ESTATUS = 21"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+
					//	" and d.ic_documento in(3900, 3991) "+
					" AND TF2.IC_TIPO_FINANCIAMIENTO(+) = CE.IC_TIPO_FINAN_ANT"+
					" AND D.IC_PYME = "+new Integer(noPyme);

					ps21= con.queryPrecompilado(strQuery.toString());
					rs21 = ps21.executeQuery();

					while(rs21.next()){

						double monto 				= rs21.getDouble("FN_MONTO");
						double tipoCambio			= rs21.getDouble("TIPO_CAMBIO");
						String porcDescuento		= (rs21.getString("FN_PORC_DESCUENTO")==null)?"0":rs21.getString("FN_PORC_DESCUENTO");
						double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
						double montoValuado		= monto*tipoCambio;
						String bandeVentaCartera			= (rs21.getString("CG_VENTACARTERA")==null)?"":rs21.getString("CG_VENTACARTERA");
						String modoPlazo			= (rs21.getString("MODO_PLAZO")==null)?"":rs21.getString("MODO_PLAZO");
						if (bandeVentaCartera.equals("S") ) {
							modoPlazo ="";
						}
						if("1".equals(icMoneda)){
							totalDoctosMN ++;
							totalMontoMN += monto;
							totalMontoValuadoMN += montoValuado;
						}else if("54".equals(icMoneda)){
							totalDoctosUSD ++;
							totalMontoUSD += monto;
							totalMontoValuadoUSD += montoValuado;
						}

						columnas21 = new HashMap();
						columnas21.put("NOMBRE_EPO",(rs21.getString("NOMBRE_EPO")==null)?"":rs21.getString("NOMBRE_EPO"));
						columnas21.put("CC_ACUSE",(rs21.getString("CC_ACUSE")==null)?"":rs21.getString("CC_ACUSE"));
						columnas21.put("IG_NUMERO_DOCTO",(rs21.getString("IG_NUMERO_DOCTO")==null)?"":rs21.getString("IG_NUMERO_DOCTO"));
						columnas21.put("DF_FECHA_EMISION",(rs21.getString("DF_FECHA_EMISION")==null)?"":rs21.getString("DF_FECHA_EMISION"));
						columnas21.put("DF_FECHA_VENC",(rs21.getString("DF_FECHA_VENC")==null)?"":rs21.getString("DF_FECHA_VENC"));
						columnas21.put("DF_CARGA",(rs21.getString("DF_CARGA")==null)?"":rs21.getString("DF_CARGA"));
						columnas21.put("IG_PLAZO_DOCTO",(rs21.getString("IG_PLAZO_DOCTO")==null)?"":rs21.getString("IG_PLAZO_DOCTO"));
						columnas21.put("MONEDA",(rs21.getString("MONEDA")==null)?"":rs21.getString("MONEDA"));
						columnas21.put("FN_MONTO",(rs21.getString("FN_MONTO")==null)?"":rs21.getString("FN_MONTO"));
						columnas21.put("IG_PLAZO_DESCUENTO",(rs21.getString("IG_PLAZO_DESCUENTO")==null)?"":rs21.getString("IG_PLAZO_DESCUENTO"));
						columnas21.put("FN_PORC_DESCUENTO",(rs21.getString("FN_PORC_DESCUENTO")==null)?"":rs21.getString("FN_PORC_DESCUENTO"));
						columnas21.put("MONTO_PORC_DESCUENTO",(Double.toString (montoDescontar)));
						columnas21.put("TIPO_CONVERSION",(rs21.getString("TIPO_CONVERSION")==null)?"":rs21.getString("TIPO_CONVERSION"));
						columnas21.put("TIPO_CAMBIO",(rs21.getString("TIPO_CAMBIO")==null)?"":rs21.getString("TIPO_CAMBIO"));
						columnas21.put("MONT_VALUADO",Double.toString (montoValuado));
						columnas21.put("MODO_PLAZO",(modoPlazo));
						columnas21.put("FECHA_CAMBIO",(rs21.getString("FECHA_CAMBIO")==null)?"":rs21.getString("FECHA_CAMBIO"));
						columnas21.put("FECHA_EMISION_ANT",(rs21.getString("FECHA_EMISION_ANT")==null)?"":rs21.getString("FECHA_EMISION_ANT"));
						columnas21.put("FECHA_VENC_ANT",(rs21.getString("FECHA_VENC_ANT")==null)?"":rs21.getString("FECHA_VENC_ANT"));
						columnas21.put("FECHA_CAMBIO",(rs21.getString("FECHA_CAMBIO")==null)?"":rs21.getString("FECHA_CAMBIO"));
						columnas21.put("FN_MONTO_ANTERIOR",(rs21.getString("FN_MONTO_ANTERIOR")==null)?"":rs21.getString("FN_MONTO_ANTERIOR"));
						columnas21.put("MODO_PLAZO_ANT",(rs21.getString("MODO_PLAZO_ANT")==null)?"":rs21.getString("MODO_PLAZO_ANT"));
						registros21.add(columnas21);
					}
					rs21.close();
					ps21.close();

					if(totalDoctosMN >0 ){
					totales21t = new HashMap();
					totales21t.put("MONEDA","MONEDA NACIONAL");
					totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
					totales21t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
					totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));
					totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoMN));
				}
				if(totalDoctosUSD >0 ){
					totales21t = new HashMap();
					totales21t.put("MONEDA","MONEDA DOLAR");
					totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
					totales21t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
					totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));
					totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoUSD));
				}
				totales21r.add(totales21t);


		}

		if(noEstatus.equals("24") ||"".equals(noEstatus)) { //No Negociable a Baja
					totalDoctosMN		= 0;
				 	totalDoctosUSD		= 0;
					totalMontoMN		= 0;
					totalMontoUSD		= 0;
					totalMontoValuadoMN	= 0;
					totalMontoValuadoUSD= 0;
					totalMontoCreditoMN	= 0;
					totalMontoCreditoUSD= 0;

		strQuery =	" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,D.cc_acuse "+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
          "	,ce.ct_cambio_motivo as causa"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
          " ,comcat_estatus_docto ed "+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					"  AND ed.ic_estatus_docto = d.ic_estatus_docto "+
					" AND CE.IC_CAMBIO_ESTATUS = 24"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+
					//" and d.ic_documento in(4327, 4330)"+
					" AND D.IC_PYME = "+new Integer(noPyme);


					ps24 = con.queryPrecompilado(strQuery.toString());
					rs24 = ps24.executeQuery();
					while(rs24.next()){
						double monto 				= rs24.getDouble("FN_MONTO");
						double tipoCambio			= rs24.getDouble("TIPO_CAMBIO");
						String porcDescuento		= (rs24.getString("FN_PORC_DESCUENTO")==null)?"0":rs24.getString("FN_PORC_DESCUENTO");
						double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
						double montoValuado		= monto*tipoCambio;
						String bandeVentaCartera			= (rs24.getString("CG_VENTACARTERA")==null)?"":rs24.getString("CG_VENTACARTERA");
						String modoPlazo			= (rs24.getString("MODO_PLAZO")==null)?"":rs24.getString("MODO_PLAZO");
						if (bandeVentaCartera.equals("S") ) {
							modoPlazo ="";
						}

						if("1".equals(icMoneda)){
							totalDoctosMN ++;
							totalMontoMN += monto;
							totalMontoValuadoMN += montoValuado;
						}else if("54".equals(icMoneda)){
							totalDoctosUSD ++;
							totalMontoUSD += monto;
							totalMontoValuadoUSD += montoValuado;
					}

						columnas24 = new HashMap();
						columnas24.put("NOMBRE_EPO",(rs24.getString("NOMBRE_EPO")==null)?"":rs24.getString("NOMBRE_EPO"));
						columnas24.put("CC_ACUSE",(rs24.getString("CC_ACUSE")==null)?"":rs24.getString("CC_ACUSE"));
						columnas24.put("IG_NUMERO_DOCTO",(rs24.getString("IG_NUMERO_DOCTO")==null)?"":rs24.getString("IG_NUMERO_DOCTO"));
						columnas24.put("DF_FECHA_EMISION",(rs24.getString("DF_FECHA_EMISION")==null)?"":rs24.getString("DF_FECHA_EMISION"));
						columnas24.put("DF_FECHA_VENC",(rs24.getString("DF_FECHA_VENC")==null)?"":rs24.getString("DF_FECHA_VENC"));
						columnas24.put("DF_CARGA",(rs24.getString("DF_CARGA")==null)?"":rs24.getString("DF_CARGA"));
						columnas24.put("IG_PLAZO_DOCTO",(rs24.getString("IG_PLAZO_DOCTO")==null)?"":rs24.getString("IG_PLAZO_DOCTO"));
						columnas24.put("MONEDA",(rs24.getString("MONEDA")==null)?"":rs24.getString("MONEDA"));
						columnas24.put("FN_MONTO",(rs24.getString("FN_MONTO")==null)?"":rs24.getString("FN_MONTO"));
						columnas24.put("IG_PLAZO_DESCUENTO",(rs24.getString("IG_PLAZO_DESCUENTO")==null)?"":rs24.getString("IG_PLAZO_DESCUENTO"));
						columnas24.put("FN_PORC_DESCUENTO",(rs24.getString("FN_PORC_DESCUENTO")==null)?"":rs24.getString("FN_PORC_DESCUENTO"));
						columnas24.put("MONTO_PORC_DESCUENTO",(Double.toString (montoDescontar)));
						columnas24.put("TIPO_CONVERSION",(rs24.getString("TIPO_CONVERSION")==null)?"":rs24.getString("TIPO_CONVERSION"));
						columnas24.put("TIPO_CAMBIO",(rs24.getString("TIPO_CAMBIO")==null)?"":rs24.getString("TIPO_CAMBIO"));
						columnas24.put("MONT_VALUADO",Double.toString (montoValuado));
						columnas24.put("MODO_PLAZO",(modoPlazo));
						columnas24.put("FECHA_CAMBIO",(rs24.getString("FECHA_CAMBIO")==null)?"":rs24.getString("FECHA_CAMBIO"));
						columnas24.put("CAUSA",(rs24.getString("CAUSA")==null)?"":rs24.getString("CAUSA"));
						registros24.add(columnas24);

					}
					rs24.close();
					ps24.close();

					if(totalDoctosMN >0 ){
					totales24t = new HashMap();
					totales24t.put("MONEDA","MONEDA NACIONAL");
					totales24t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
					totales24t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
					totales24t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));

				}
				if(totalDoctosUSD >0 ){
					totales24t = new HashMap();
					totales24t.put("MONEDA","MONEDA DOLAR");
					totales24t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
					totales24t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
					totales24t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));
				}
				totales24r.add(totales24t);

			}

				reportes.add(registros23);
				reportes.add(registros2);
				reportes.add(registros22);
				reportes.add(registros24);
				reportes.add(registros21);

				reportes.add(totales14r);
				reportes.add(totales2r);
				reportes.add(totales22r);
				reportes.add(totales24r);
				reportes.add(totales21r);




	} catch(Exception e) {
			log.error("repCambioEstatus(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
        con.cierraConexionDB();
			log.info(" repCambioEstatus");
		}
		return reportes;

	}


	/**
	 *
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param tiposCredito
	 * @param noEstatus
	 * @param tipoArchivo
	 * @param strDirectorioTemp
	 * @param noPyme
	 */
public String repCambioEstatusCSVPDF(List datos)   throws NafinException {
		log.info(" repCambioEstatusCSVPDF (E)");
		String qrySentenciaCCC = "";
		String qrySentenciaDM = "";
		String strQuery 		= "";
		PreparedStatement	ps14	= null;
		ResultSet rs14	= null;
		PreparedStatement	ps2	= null;
		ResultSet rs2	= null;
		PreparedStatement	ps22	= null;
		ResultSet rs22	= null;
		PreparedStatement	ps21	= null;
		ResultSet rs21	= null;
		PreparedStatement	ps24	= null;
		ResultSet rs24	= null;
		AccesoDB	 con 	     =   new AccesoDB();
		boolean	 resultado = true;
		BufferedWriter 	out 								= null;
		String nombreArchivoCSV = "";
		String nombreArchivo = "";
		OutputStreamWriter writer = null;
		ComunesPDF pdfDoc = new ComunesPDF();
		String  nombreArchivoPDF  ="";


		String pais  = (String)datos.get(0);
		String nafinElectronico = (String)datos.get(1);
		String noExterno = (String)datos.get(2);
		String nombre =  (String)datos.get(3);
		String nombreUsua = (String)datos.get(4);
		String logo = (String)datos.get(5);
		String strDirectorioTemp =  (String)datos.get(6);
		String  noPyme = (String)datos.get(7);
		String tipoArchivo = (String)datos.get(8);
		String noEstatus = (String)datos.get(9);
		String tiposCredito = (String)datos.get(10);
		String strDirectorioPublicacion  = (String)datos.get(11);

				//totales
		int		totalDoctosMN		= 0;
		int 	totalDoctosUSD		= 0;
		double	totalMontoMN		= 0;
		double	totalMontoUSD		= 0;
		double	totalMontoValuadoMN	= 0;
		double	totalMontoValuadoUSD= 0;
		double	totalMontoCreditoMN	= 0;
		double	totalMontoCreditoUSD= 0;
		String   icMoneda ="";


		try {
			con.conexionDB();
			if(tipoArchivo.equals("csv")){
				nombreArchivoCSV = Comunes.cadenaAleatoria(16)+"."+tipoArchivo;
				writer  = new OutputStreamWriter(new FileOutputStream(strDirectorioTemp+nombreArchivoCSV),"Cp1252");
				out   = new BufferedWriter(writer);

			}else if(tipoArchivo.equals("pdf")){

				nombreArchivoPDF = Comunes.cadenaAleatoria(16)+".pdf";
				pdfDoc = new ComunesPDF(2,strDirectorioTemp+nombreArchivoPDF);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,pais,nafinElectronico,noExterno, nombre, nombreUsua, logo, strDirectorioPublicacion);

				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("  ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" Cambios de Estatus ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("  ","formas",ComunesPDF.CENTER);

			}

			if(noEstatus.equals("22") || "".equals(noEstatus)) { //Negociable a Vencido a Baja
					totalDoctosMN = 0;
					totalMontoMN = 0;
					totalMontoValuadoMN = 0;
					totalDoctosUSD = 0;
					totalMontoUSD = 0;
					totalMontoValuadoUSD = 0;
					icMoneda ="";


			strQuery =	" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,D.cc_acuse "+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND D.IC_ESTATUS_DOCTO = 9"+
					" AND CE.IC_CAMBIO_ESTATUS = 22"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+
					//" and d.ic_documento in(4361)"+
					" AND D.IC_PYME = "+new Integer(noPyme);



					ps22 = con.queryPrecompilado(strQuery.toString());
					rs22 = ps22.executeQuery();

					if(tipoArchivo.equals("csv")){
						out.write("Negociable a Vencido sin Operar\n ");
						out.write(" EPO,N�mero de Acuse de Carga,Num. docto.inicial,Fecha de emisi�n,Fecha de vencimiento,Fecha de Publicaci�n,Plazo docto.");
						out.write(",Moneda,Monto,Plazo para descuento en dias, % de descuento,Monto% de descuento,Tipo conv.,Tipo cambio,Monto valuado,Modalidad de plazo,Fecha Cambio\n");

					}else if(tipoArchivo.equals("pdf")){
							pdfDoc.setTable(10, 100);
							pdfDoc.setCell("Negociable a Vencido sin Operar","celda01",ComunesPDF.LEFT,10);
							pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("N�mero de Acuse de Carga","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Num. docto.inicial","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha de emisi�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha de Publicaci�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Plazo docto.","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Monto","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("Plazo para descuento en dias","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" % de descuento","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Monto % de descuento","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Tipo conv.","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Tipo cambio","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Monto valuado","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Modalidad de plazo","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Fecha Cambio","celda01",ComunesPDF.CENTER,2);



					}
					while(rs22.next()){

						double monto 				= rs22.getDouble("FN_MONTO");
						double tipoCambio			= rs22.getDouble("TIPO_CAMBIO");
						String porcDescuento		= (rs22.getString("FN_PORC_DESCUENTO")==null)?"0":rs22.getString("FN_PORC_DESCUENTO");
						double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
						double montoValuado		= monto*tipoCambio;
						String bandeVentaCartera			= (rs22.getString("CG_VENTACARTERA")==null)?"":rs22.getString("CG_VENTACARTERA");
						String modoPlazo			= (rs22.getString("MODO_PLAZO")==null)?"":rs22.getString("MODO_PLAZO");
						if (bandeVentaCartera.equals("S") ) {
							modoPlazo ="";
						}
						icMoneda	= (rs22.getString("IC_MONEDA")==null)?"":rs22.getString("IC_MONEDA");

						if("1".equals(icMoneda)){
							totalDoctosMN ++;
							totalMontoMN += monto;
							totalMontoValuadoMN += montoValuado;
						}else if("54".equals(icMoneda)){
							totalDoctosUSD ++;
							totalMontoUSD += monto;
							totalMontoValuadoUSD += montoValuado;
						}

						if(tipoArchivo.equals("csv")){
							out.write((rs22.getString("NOMBRE_EPO")==null)?"":rs22.getString("NOMBRE_EPO").toString().replace(',',' ')+",");
							out.write((rs22.getString("CC_ACUSE")==null)?"":rs22.getString("CC_ACUSE")+",");
							out.write((rs22.getString("IG_NUMERO_DOCTO")==null)?"":rs22.getString("IG_NUMERO_DOCTO")+",");
							out.write((rs22.getString("DF_FECHA_EMISION")==null)?"":rs22.getString("DF_FECHA_EMISION")+",");
							out.write((rs22.getString("DF_FECHA_VENC")==null)?" ":rs22.getString("DF_FECHA_VENC")+",");
							out.write((rs22.getString("DF_CARGA")==null)?"":rs22.getString("DF_CARGA")+",");
							out.write((rs22.getString("IG_PLAZO_DOCTO")==null)?"0":rs22.getString("IG_PLAZO_DOCTO")+",");
							out.write((rs22.getString("MONEDA")==null)?"":rs22.getString("MONEDA")+",");
							out.write("\"$"+Comunes.formatoDecimal((rs22.getString("FN_MONTO")==null)?"0":rs22.getString("FN_MONTO"),2,true)+"\",");
							out.write((rs22.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs22.getString("IG_PLAZO_DESCUENTO")+",");
							out.write((rs22.getString("FN_PORC_DESCUENTO")==null)?"0":rs22.getString("FN_PORC_DESCUENTO")+"%,");
							out.write("\"$"+Comunes.formatoDecimal(montoDescontar,2,true)+"\",");
							out.write((rs22.getString("TIPO_CONVERSION")==null)?" ":rs22.getString("TIPO_CONVERSION")+",");
							out.write((rs22.getString("TIPO_CAMBIO")==null)?" ":rs22.getString("TIPO_CAMBIO")+",");
							out.write("\"$"+Comunes.formatoDecimal(montoValuado,2,true)+"\",");
							out.write(modoPlazo+",");
							out.write((rs22.getString("FECHA_CAMBIO")==null)?" ":rs22.getString("FECHA_CAMBIO")+"\n");

						}else 	if(tipoArchivo.equals("pdf")){
							pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("NOMBRE_EPO")==null)?" ":rs22.getString("NOMBRE_EPO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("CC_ACUSE")==null)?" ":rs22.getString("CC_ACUSE"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("IG_NUMERO_DOCTO")==null)?" ":rs22.getString("IG_NUMERO_DOCTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("DF_FECHA_EMISION")==null)?" ":rs22.getString("DF_FECHA_EMISION"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("DF_FECHA_VENC")==null)?" ":rs22.getString("DF_FECHA_VENC"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("DF_CARGA")==null)?" ":rs22.getString("DF_CARGA"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("IG_PLAZO_DOCTO")==null)?"0":rs22.getString("IG_PLAZO_DOCTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("MONEDA")==null)?"":rs22.getString("MONEDA"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal((rs22.getString("FN_MONTO")==null)?"0":rs22.getString("FN_MONTO"),2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs22.getString("IG_PLAZO_DESCUENTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("FN_PORC_DESCUENTO")==null)?"0":rs22.getString("FN_PORC_DESCUENTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (montoDescontar)),2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell((rs22.getString("TIPO_CONVERSION")==null)?" ":rs22.getString("TIPO_CONVERSION"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("TIPO_CAMBIO")==null)?" ":rs22.getString("TIPO_CAMBIO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(Double.toString (montoValuado),2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs22.getString("FECHA_CAMBIO")==null)?" ":rs22.getString("FECHA_CAMBIO"),"formas",ComunesPDF.CENTER,2);

						}
					}
					rs22.close();
					ps22.close();
					if(tipoArchivo.equals("pdf")){
						if("1".equals(icMoneda)){
						pdfDoc.setCell("Total Documentos M.N. ","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total Monto M.N. ","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total Monto Valuado M.N. ","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoMN,2),"formas",ComunesPDF.CENTER,2);
						}else if("54".equals(icMoneda)){
						pdfDoc.setCell("Total Documentos Dolares ","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total Monto Dolares","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("Total Monto Valuado Dolares","formas",ComunesPDF.CENTER,2);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.CENTER,2);
						}
					}
					if(tipoArchivo.equals("pdf")){ 			pdfDoc.addTable(); 			}

			}

			if(noEstatus.equals("23") ||noEstatus.equals("34")) { //Seleccionado Pyme a Rechazado IF

					totalDoctosMN = 0;
					totalMontoMN = 0;
					totalMontoValuadoMN = 0;
					totalDoctosUSD = 0;
					totalMontoUSD = 0;
					totalMontoValuadoUSD = 0;
					icMoneda ="";

			qrySentenciaDM =
				"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
				"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
				"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
				"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
				"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
				"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
				"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
				"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
				"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
				"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
				"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
				"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
				"       i.cg_razon_social AS nombre_if, "+
				"       DECODE (pp.cg_tipo_credito, "+
				"          'D', 'Descuento y/o Factoraje', 'C', 'Credito en Cuenta Corriente') AS tipo_credito, "+
				"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
				"       tci.cd_descripcion AS tipo_cobro_int, "+
				"  		DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || ds.CG_REL_MAT||' '||ds.FN_PUNTOS) as referencia_int,"+
				"       DECODE ( ds.cg_rel_mat, "+
				"          '+', fn_valor_tasa + fn_puntos, "+
				"          '-', fn_valor_tasa - fn_puntos, "+
				"          '*', fn_valor_tasa * fn_puntos, "+
				"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
				"       ds.fn_importe_interes AS monto_tasa_int, d.cc_acuse, "+
				"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
				"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
				"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_seleccion,"+
				"       TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio,"+
				"		ce.ct_cambio_motivo as causa"+
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"  FROM dis_documento d, "+
				"       comcat_pyme py, "+
				"       comcat_moneda m, "+
				"       comrel_producto_epo pe, "+
				"       comcat_producto_nafin pn, "+
				"       com_tipo_cambio tc, "+
				"       comcat_tipo_financiamiento tf, "+
				"       comcat_estatus_docto ed, "+
				"       dis_linea_credito_dm lc, "+
				"       comcat_epo e, "+
				"       dis_docto_seleccionado ds, "+
				"       comcat_if i, "+
				"       comrel_pyme_epo_x_producto pp, "+
				"       comcat_tipo_cobro_interes tci, "+
				"       comcat_tasa ct, "+
				"       dis_cambio_estatus ce "+
				" WHERE d.ic_pyme = py.ic_pyme "+
				"   AND ds.ic_documento = d.ic_documento "+
				"   AND ce.ic_documento = d.ic_documento "+
				"   AND d.ic_moneda = m.ic_moneda "+
				"   AND pn.ic_producto_nafin = 4 "+
				"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
				"   AND pe.ic_epo = d.ic_epo "+
				"   AND i.ic_if = lc.ic_if "+
				"   AND e.ic_epo = d.ic_epo "+
				"   AND pp.ic_epo = e.ic_epo "+
				"   AND pp.ic_pyme = py.ic_pyme "+
				"   AND pp.ic_producto_nafin = 4 "+
				"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
				"                         FROM com_tipo_cambio "+
				"                        WHERE ic_moneda = m.ic_moneda) "+
				"   AND m.ic_moneda = tc.ic_moneda "+
				"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
				"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
				"   AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
				"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
				"   AND ct.ic_tasa = ds.ic_tasa "+
				"   AND ce.ic_cambio_estatus ="+noEstatus+
				"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
				" AND D.IC_PYME = "+new Integer(noPyme);

			qrySentenciaCCC = " SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
			"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
			"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
			"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
			"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
			"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
			"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
			"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
			"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
			"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
			"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
			"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
			"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
			"       i.cg_razon_social AS nombre_if, "+
			"       DECODE (pp.cg_tipo_credito, "+
			"          'D', 'Descuento y/o Factoraje', 'C', 'Credito en Cuenta Corriente') AS tipo_credito, "+
			"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
			"       tci.cd_descripcion AS tipo_cobro_int, "+
			"       ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos  AS referencia_int, "+
			"       DECODE ( ds.cg_rel_mat, "+
			"          '+', fn_valor_tasa + fn_puntos, "+
			"          '-', fn_valor_tasa - fn_puntos, "+
			"          '*', fn_valor_tasa * fn_puntos, "+
			"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
			"       ds.fn_importe_interes AS monto_tasa_int, d.cc_acuse, "+
			"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
			"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
			"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_seleccion,"+
			"       TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio,"+
			"		ce.ct_cambio_motivo as causa"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"  FROM dis_documento d, "+
			"       comcat_pyme py, "+
			"       comcat_moneda m, "+
			"       comrel_producto_epo pe, "+
			"       comcat_producto_nafin pn, "+
			"       com_tipo_cambio tc, "+
			"       comcat_tipo_financiamiento tf, "+
			"       comcat_estatus_docto ed, "+
			"       com_linea_credito lc, "+
			"       comcat_epo e, "+
			"       dis_docto_seleccionado ds, "+
			"       comcat_if i, "+
			"       comrel_pyme_epo_x_producto pp, "+
			"       comcat_tipo_cobro_interes tci, "+
			"       comcat_tasa ct, "+
			"       dis_cambio_estatus ce "+
			" WHERE d.ic_pyme = py.ic_pyme "+
			"   AND ds.ic_documento = d.ic_documento "+
			"   AND ce.ic_documento = d.ic_documento "+
			"   AND d.ic_moneda = m.ic_moneda "+
			"   AND pn.ic_producto_nafin = 4 "+
			"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
			"   AND pe.ic_epo = d.ic_epo "+
			"   AND i.ic_if = lc.ic_if "+
			"   AND e.ic_epo = d.ic_epo "+
			"   AND pp.ic_epo = e.ic_epo "+
			"   AND pp.ic_pyme = py.ic_pyme "+
			"   AND pp.ic_producto_nafin = 4 "+
			"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
			"                         FROM com_tipo_cambio "+
			"                        WHERE ic_moneda = m.ic_moneda) "+
			"   AND m.ic_moneda = tc.ic_moneda "+
			"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
			"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
			"   AND d.ic_linea_credito = lc.ic_linea_credito "+
			"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
			"   AND ct.ic_tasa = ds.ic_tasa "+
			"   AND ce.ic_cambio_estatus ="+noEstatus+
			"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
			//	"and d.ic_documento in(3915,3985)"+
			" AND D.IC_PYME =  "+new Integer(noPyme);

				if("D".equals(tiposCredito)){
					strQuery = qrySentenciaDM;
				}else if("C".equals(tiposCredito)){
					strQuery = qrySentenciaCCC;
				}else {
						strQuery = qrySentenciaDM +" UNION ALL "+qrySentenciaCCC;
				}

			System.out.println(":strQuery:::"+strQuery);
			ps14 = con.queryPrecompilado(strQuery.toString());
			rs14 = ps14.executeQuery();
			if(tipoArchivo.equals("csv")){
				if(noEstatus.equals("23")){
					out.write("\n En Proceso de Autorizacion IF a Negociable \n ");
				}else if(noEstatus.equals("34")){
					out.write("\n En Proceso de Autorizacion IF a Seleccionado Pyme \n ");
				}

				out.write(" EPO,N�mero de Acuse de carga,N�mero de documento inicial,Fecha de emisi�n,Fecha de vencimiento,Fecha de publicaci�n,Plazo de documento");
				out.write(",Moneda,Monto,Plazo para descuento,Porcentaje de descuento,Monto porcentaje de descuento,Tipo conversi�n,Tipo cambio,Monto valuado en pesos,Modalidad de plazo");
				out.write(",N�mero de documento final,Monto,Plazo,Fecha de vencimiento, IF,Referencia tasa de inter�s,Valor tasa de inter�s,Monto de Inter�s,Monto Total de Capital e Inter�s");
				out.write(",Fecha de cambio, Causa \n");

			}else if(tipoArchivo.equals("pdf")){

				pdfDoc.setTable(11, 100);

				if(noEstatus.equals("23")){
					pdfDoc.setCell(" En Proceso de Autorizacion IF a Negociable","celda01",ComunesPDF.LEFT,11);

				}else if(noEstatus.equals("34")){
					pdfDoc.setCell(" En Proceso de Autorizacion IF a Seleccionado Pyme","celda01",ComunesPDF.LEFT,11);
				}
				pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell(" EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de Acuse de carga","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de documento inicial","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de publicaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo de documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo para descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell(" Porcentaje de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto porcentaje de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo conversi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo cambio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto valuado en pesos","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Modalidad de plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de documento final ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("C","celda01rep",ComunesPDF.CENTER);
			//	pdfDoc.setCell(" Fecha de Operaci�n Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Referencia tasa de inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Valor tasa de inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto de Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto Total de Capital e Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de cambio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Causa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER);

			}
			while(rs14.next()){
				double monto 				= rs14.getDouble("FN_MONTO");
				double tipoCambio			= rs14.getDouble("TIPO_CAMBIO");
				String porcDescuento		= (rs14.getString("FN_PORC_DESCUENTO")==null)?"0":rs14.getString("FN_PORC_DESCUENTO");
				double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				double montoValuado		= monto*tipoCambio;
				String bandeVentaCartera			= (rs14.getString("CG_VENTACARTERA")==null)?"":rs14.getString("CG_VENTACARTERA");
				String modoPlazo			= (rs14.getString("MODO_PLAZO")==null)?"0":rs14.getString("MODO_PLAZO");
				if (bandeVentaCartera.equals("S") ) {
					modoPlazo ="";
				}
				double montoCredito		= rs14.getDouble("MONTO_CREDITO");
				double montoTasaInt		= rs14.getDouble("MONTO_TASA_INT");
				double MontoTotalTasaInte = montoCredito+montoTasaInt;

				icMoneda	= (rs14.getString("IC_MONEDA")==null)?"":rs14.getString("IC_MONEDA");
				String IG_PLAZO_DESCUENTO = (rs14.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs14.getString("IG_PLAZO_DESCUENTO");
				String	FN_PORC_DESCUENTO  = (rs14.getString("FN_PORC_DESCUENTO")==null)?"0":rs14.getString("FN_PORC_DESCUENTO");

					if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += montoCredito;
					}else if("54".equals(icMoneda)){
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += montoCredito;
				}


				if(tipoArchivo.equals("csv")){
					out.write((rs14.getString("NOMBRE_EPO")==null)?" ":rs14.getString("NOMBRE_EPO").toString().replace(',',' ')+",");
					out.write((rs14.getString("CC_ACUSE")==null)?" ":rs14.getString("CC_ACUSE")+",");
					out.write((rs14.getString("IG_NUMERO_DOCTO")==null)?" ":rs14.getString("IG_NUMERO_DOCTO")+",");
					out.write((rs14.getString("DF_FECHA_EMISION")==null)?" ":rs14.getString("DF_FECHA_EMISION")+",");
					out.write((rs14.getString("DF_FECHA_VENC")==null)?" ":rs14.getString("DF_FECHA_VENC")+",");
					out.write((rs14.getString("DF_CARGA")==null)?" ":rs14.getString("DF_CARGA")+",");
					out.write((rs14.getString("IG_PLAZO_DOCTO")==null)?"0":rs14.getString("IG_PLAZO_DOCTO")+",");
					out.write((rs14.getString("MONEDA")==null)?" ":rs14.getString("MONEDA")+",");
					out.write("\"$"+Comunes.formatoDecimal((rs14.getString("FN_MONTO")==null)?"0":rs14.getString("FN_MONTO"),2, true)+"\",");
					out.write(IG_PLAZO_DESCUENTO+",");
					out.write(FN_PORC_DESCUENTO+"%,");
					out.write("\"$"+Comunes.formatoDecimal(montoDescontar,2,true)+"\",");
					out.write((rs14.getString("TIPO_CONVERSION")==null)?" ":rs14.getString("TIPO_CONVERSION")+",");
					out.write((rs14.getString("TIPO_CAMBIO")==null)?" ":rs14.getString("TIPO_CAMBIO")+",");
					out.write("\"$"+Comunes.formatoDecimal(montoValuado,2,true)+"\",");
					out.write((rs14.getString("MODO_PLAZO")==null)?" ":rs14.getString("MODO_PLAZO")+",");
					out.write((rs14.getString("IC_DOCUMENTO")==null)?" ":rs14.getString("IC_DOCUMENTO")+",");
					out.write("\"$"+Comunes.formatoDecimal((rs14.getString("MONTO_CREDITO")==null)?"0":rs14.getString("MONTO_CREDITO"),2,true)+"\",");
					out.write((rs14.getString("IG_PLAZO_CREDITO")==null)?"0":rs14.getString("IG_PLAZO_CREDITO")+",");
					out.write((rs14.getString("FECHA_VENC_CREDITO")==null)?" ":rs14.getString("FECHA_VENC_CREDITO")+",");
					//out.write((rs14.getString("FECHA_SELECCION")==null)?" ":rs14.getString("FECHA_SELECCION")+",");
					out.write((rs14.getString("NOMBRE_IF")==null)?" ":rs14.getString("NOMBRE_IF").toString().replace(',',' ')+",");
					out.write((rs14.getString("REFERENCIA_INT")==null)?" ":rs14.getString("REFERENCIA_INT")+",");
					String valor_tasa_int = (rs14.getString("VALOR_TASA_INT")==null)?"0":rs14.getString("VALOR_TASA_INT");
					out.write(valor_tasa_int +"%,");
					out.write("\"$"+Comunes.formatoDecimal((rs14.getString("MONTO_TASA_INT")==null)?"0":rs14.getString("MONTO_TASA_INT"),2,true)+"\",");
					out.write("\"$"+Comunes.formatoDecimal(MontoTotalTasaInte,2,true)+"\",");
					out.write((rs14.getString("FECHA_CAMBIO")==null)?" ":rs14.getString("FECHA_CAMBIO")+",");
					out.write((rs14.getString("CAUSA")==null)?" ":rs14.getString("CAUSA").toString().replace(',',' '));
					out.write("\n");

				}else if(tipoArchivo.equals("pdf")){
					pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("NOMBRE_EPO")==null)?" ":rs14.getString("NOMBRE_EPO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("CC_ACUSE")==null)?" ":rs14.getString("CC_ACUSE"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("IG_NUMERO_DOCTO")==null)?" ":rs14.getString("IG_NUMERO_DOCTO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("DF_FECHA_EMISION")==null)?" ":rs14.getString("DF_FECHA_EMISION"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("DF_FECHA_VENC")==null)?" ":rs14.getString("DF_FECHA_VENC"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("DF_CARGA")==null)?" ":rs14.getString("DF_CARGA"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("IG_PLAZO_DOCTO")==null)?"0":rs14.getString("IG_PLAZO_DOCTO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("MONEDA")==null)?" ":rs14.getString("MONEDA"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((rs14.getString("FN_MONTO")==null)?"0":rs14.getString("FN_MONTO"),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell((rs14.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs14.getString("IG_PLAZO_DESCUENTO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("FN_PORC_DESCUENTO")==null)?"0":rs14.getString("FN_PORC_DESCUENTO")+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (montoDescontar)),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell((rs14.getString("TIPO_CONVERSION")==null)?" ":rs14.getString("TIPO_CONVERSION"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("TIPO_CAMBIO")==null)?" ":rs14.getString("TIPO_CAMBIO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(Double.toString (montoValuado),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell((rs14.getString("MODO_PLAZO")==null)?" ":rs14.getString("MODO_PLAZO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("IC_DOCUMENTO")==null)?" ":rs14.getString("IC_DOCUMENTO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((rs14.getString("MONTO_CREDITO")==null)?"0":rs14.getString("MONTO_CREDITO"),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell((rs14.getString("IG_PLAZO_CREDITO")==null)?"0":rs14.getString("IG_PLAZO_CREDITO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("FECHA_VENC_CREDITO")==null)?" ":rs14.getString("FECHA_VENC_CREDITO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("C","celda01rep",ComunesPDF.CENTER);
					//pdfDoc.setCell((rs14.getString("FECHA_SELECCION")==null)?" ":rs14.getString("FECHA_SELECCION"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("NOMBRE_IF")==null)?" ":rs14.getString("NOMBRE_IF"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("REFERENCIA_INT")==null)?" ":rs14.getString("REFERENCIA_INT"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("VALOR_TASA_INT")==null)?"0":rs14.getString("VALOR_TASA_INT")+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((rs14.getString("MONTO_TASA_INT")==null)?"0":rs14.getString("MONTO_TASA_INT"),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (MontoTotalTasaInte)),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell((rs14.getString("FECHA_CAMBIO")==null)?" ":rs14.getString("FECHA_CAMBIO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs14.getString("CAUSA")==null)?" ":rs14.getString("CAUSA"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);


				}
			}
			rs14.close();
			ps14.close();
			if(tipoArchivo.equals("pdf")){
				if("1".equals(icMoneda)){
					pdfDoc.setCell("Total Documentos M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell("Total Monto M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,3);

					pdfDoc.setCell("Total Monto Valuado M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoMN,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell("Total Monto Cr�dito M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,3);

				}else if("54".equals(icMoneda)){
					pdfDoc.setCell("Total Documentos Dolares ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.CENTER,2);
					pdfDoc.setCell("Total Monto Dolares","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.CENTER,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,3);

					pdfDoc.setCell("Total Monto Valuado Dolares","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell("Total Monto Cr�dito Dolares","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,3);
					}
				}
				if(tipoArchivo.equals("pdf")){ 			pdfDoc.addTable(); 			}

		}

		if(noEstatus.equals("2") || "".equals(noEstatus) ) { //Seleccionado Pyme a Negociable
					totalDoctosMN = 0;
					totalMontoMN = 0;
					totalMontoValuadoMN = 0;
					totalDoctosUSD = 0;
					totalMontoUSD = 0;
					totalMontoValuadoUSD = 0;
					icMoneda ="";
			qrySentenciaDM =
				"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
				"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
				"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
				"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
				"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
				"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
				"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
				"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
				"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
				"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
				"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
				"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
				"       i.cg_razon_social AS nombre_if, "+
				"       DECODE (pp.cg_tipo_credito, "+
				"          'D', 'Descuento y/o Factoraje', 'C', 'Credito en Cuenta Corriente') AS tipo_credito, "+
				"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
				"       tci.cd_descripcion AS tipo_cobro_int, "+
				"  		DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || ds.CG_REL_MAT||' '||ds.FN_PUNTOS) as referencia_int,"+
				"       DECODE ( ds.cg_rel_mat, "+
				"          '+', fn_valor_tasa + fn_puntos, "+
				"          '-', fn_valor_tasa - fn_puntos, "+
				"          '*', fn_valor_tasa * fn_puntos, "+
				"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
				"       ds.fn_importe_interes AS monto_tasa_int, d.cc_acuse, "+
				"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
				"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
				"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_seleccion,"+
				"       TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio,"+
				"		ce.ct_cambio_motivo as causa"+
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"    ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO   "+

				"  FROM dis_documento d, "+
				"       comcat_pyme py, "+
				"       comcat_moneda m, "+
				"       comrel_producto_epo pe, "+
				"       comcat_producto_nafin pn, "+
				"       com_tipo_cambio tc, "+
				"       comcat_tipo_financiamiento tf, "+
				"       comcat_estatus_docto ed, "+
				"       dis_linea_credito_dm lc, "+
				"       comcat_epo e, "+
				"       dis_docto_seleccionado ds, "+
				"       comcat_if i, "+
				"       comrel_pyme_epo_x_producto pp, "+
				"       comcat_tipo_cobro_interes tci, "+
				"       comcat_tasa ct, "+
				"       dis_cambio_estatus ce "+
				" WHERE d.ic_pyme = py.ic_pyme "+
				"   AND ds.ic_documento = d.ic_documento "+
				"   AND ce.ic_documento = d.ic_documento "+
				"   AND d.ic_moneda = m.ic_moneda "+
				"   AND pn.ic_producto_nafin = 4 "+
				"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
				"   AND pe.ic_epo = d.ic_epo "+
				"   AND i.ic_if = lc.ic_if "+
				"   AND e.ic_epo = d.ic_epo "+
				"   AND pp.ic_epo = e.ic_epo "+
				"   AND pp.ic_pyme = py.ic_pyme "+
				"   AND pp.ic_producto_nafin = 4 "+
				"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
				"                         FROM com_tipo_cambio "+
				"                        WHERE ic_moneda = m.ic_moneda) "+
				"   AND m.ic_moneda = tc.ic_moneda "+
				"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
				"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
				"   AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
				"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
				"   AND ct.ic_tasa = ds.ic_tasa "+
				"   AND ce.ic_cambio_estatus = 2 "+
				"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
				" AND D.IC_PYME = "+new Integer(noPyme);

		qrySentenciaCCC =
			"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
			"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
			"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
			"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
			"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
			"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
			"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
			"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
			"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
			"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
			"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
			"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
			"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
			"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
			"       i.cg_razon_social AS nombre_if, "+
			"       DECODE (pp.cg_tipo_credito, "+
			"          'D', 'Descuento y/o Factoraje', 'C', 'Credito en Cuenta Corriente') AS tipo_credito, "+
			"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
			"       tci.cd_descripcion AS tipo_cobro_int, "+
			"       ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos  AS referencia_int, "+
			"       DECODE ( ds.cg_rel_mat, "+
			"          '+', fn_valor_tasa + fn_puntos, "+
			"          '-', fn_valor_tasa - fn_puntos, "+
			"          '*', fn_valor_tasa * fn_puntos, "+
			"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
			"       ds.fn_importe_interes AS monto_tasa_int, d.cc_acuse, "+
			"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
			"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
			"       TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_seleccion,"+
			"       TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio,"+
			"		ce.ct_cambio_motivo as causa"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+

			"  FROM dis_documento d, "+
			"       comcat_pyme py, "+
			"       comcat_moneda m, "+
			"       comrel_producto_epo pe, "+
			"       comcat_producto_nafin pn, "+
			"       com_tipo_cambio tc, "+
			"       comcat_tipo_financiamiento tf, "+
			"       comcat_estatus_docto ed, "+
			"       com_linea_credito lc, "+
			"       comcat_epo e, "+
			"       dis_docto_seleccionado ds, "+
			"       comcat_if i, "+
			"       comrel_pyme_epo_x_producto pp, "+
			"       comcat_tipo_cobro_interes tci, "+
			"       comcat_tasa ct, "+
			"       dis_cambio_estatus ce "+
			" WHERE d.ic_pyme = py.ic_pyme "+
			"   AND ds.ic_documento = d.ic_documento "+
			"   AND ce.ic_documento = d.ic_documento "+
			"   AND d.ic_moneda = m.ic_moneda "+
			"   AND pn.ic_producto_nafin = 4 "+
			"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
			"   AND pe.ic_epo = d.ic_epo "+
			"   AND i.ic_if = lc.ic_if "+
			"   AND e.ic_epo = d.ic_epo "+
			"   AND pp.ic_epo = e.ic_epo "+
			"   AND pp.ic_pyme = py.ic_pyme "+
			"   AND pp.ic_producto_nafin = 4 "+
			"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
			"                         FROM com_tipo_cambio "+
			"                        WHERE ic_moneda = m.ic_moneda) "+
			"   AND m.ic_moneda = tc.ic_moneda "+
			"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
			"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
			"   AND d.ic_linea_credito = lc.ic_linea_credito "+
			"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
			"   AND ct.ic_tasa = ds.ic_tasa "+
			"   AND ce.ic_cambio_estatus = 2 "+
			"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
		//	 " and d.ic_documento in(4096, 4087) "+
			" AND D.IC_PYME = "+new Integer(noPyme);

			if("D".equals(tiposCredito))
				strQuery = qrySentenciaDM;
			else if("C".equals(tiposCredito))
				strQuery = qrySentenciaCCC;
			else
				strQuery = qrySentenciaDM +" UNION ALL "+qrySentenciaCCC;

			ps2 = con.queryPrecompilado(strQuery.toString());
			rs2 = ps2.executeQuery();

			if(tipoArchivo.equals("csv")){
				out.write("\n Seleccionado Pyme a Negociable \n");
				out.write(" EPO,N�mero de Acuse de Carga,Num. docto.inicial,Fecha de emisi�n,Fecha de vencimiento,Fecha de publicaci�n,Plazo docto.");
				out.write(",Moneda,Monto, % de Descuento Aforo, 	Monto a Descontar,  Plazo para descuento en dias,% de descuento,Monto % de descuento,Tipo conv.,Tipo cambio,Monto valuado en pesos,Modalidad de plazo");
				out.write(",Num.docto. final,Monto,Plazo,Fecha de Vencimiento,Fecha de Operaci�n Distribuidor,IF,Referencia tasa de inter�s,Valor tasa de inter�s,Monto de Intereses,Monto Total de Capital e Inter�s");
				out.write(",Fecha de Rechazo, Causa \n");

			}else if(tipoArchivo.equals("pdf")){
				pdfDoc.setTable(13, 100);
				pdfDoc.setCell("Seleccionado Pyme a Negociable","celda01",ComunesPDF.LEFT,13);
				pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell(" EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de Acuse de Carga","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Num. docto.inicial","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de publicaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo docto.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" % de Descuento Aforo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto a Descontar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo para descuento en dias","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell(" % de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto % de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo conv.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo cambio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto valuado en pesos","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Num.docto. final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("C","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Operaci�n Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Referencia tasa de inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Valor tasa de inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto de Intereses","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto Total de Capital e Inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Causa ","celda01",ComunesPDF.CENTER,5);

			}

			while(rs2.next()){
				double monto 		= rs2.getDouble("FN_MONTO");
				double tipoCambio			= rs2.getDouble("TIPO_CAMBIO");
				String porcDescuento		= (rs2.getString("FN_PORC_DESCUENTO")==null)?"0":rs2.getString("FN_PORC_DESCUENTO");
				double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
				double montoValuado		= monto*tipoCambio;
				String bandeVentaCartera			= (rs2.getString("CG_VENTACARTERA")==null)?"":rs2.getString("CG_VENTACARTERA");
				String modoPlazo			= (rs2.getString("MODO_PLAZO")==null)?"":rs2.getString("MODO_PLAZO");
				if (bandeVentaCartera.equals("S") ) {
					modoPlazo ="";
				}
				double montoCredito		= rs2.getDouble("MONTO_CREDITO");
				double montoTasaInt		= rs2.getDouble("MONTO_TASA_INT");
				double MontoTotalTasaInte = montoCredito+montoTasaInt;

				icMoneda	= (rs2.getString("IC_MONEDA")==null)?"":rs2.getString("IC_MONEDA");
				String IG_PLAZO_DESCUENTO = (rs2.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs2.getString("IG_PLAZO_DESCUENTO");

				if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += montoCredito;
					}else if("54".equals(icMoneda)){
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += montoCredito;
				}
				String porc_Aforo	= (rs2.getString("POR_AFRO")==null)?"0":rs2.getString("POR_AFRO"); //F05-2014
				if (porc_Aforo.equals("") ) {     porc_Aforo = "0"; }
				BigDecimal  por_Aforo= new BigDecimal(porc_Aforo);  //F05-2014
				BigDecimal  montoD= new BigDecimal(monto);  //F05-2014 /
				BigDecimal   MontoDesconta = montoD.multiply(por_Aforo.divide(new BigDecimal("100"),10,BigDecimal.ROUND_HALF_UP)); //F05-2014



				if(tipoArchivo.equals("csv")){
					out.write((rs2.getString("NOMBRE_EPO")==null)?" ":rs2.getString("NOMBRE_EPO").toString().replace(',',' ')+",");
					out.write((rs2.getString("CC_ACUSE")==null)?" ":rs2.getString("CC_ACUSE")+",");
					out.write((rs2.getString("IG_NUMERO_DOCTO")==null)?" ":rs2.getString("IG_NUMERO_DOCTO")+",");
					out.write((rs2.getString("DF_FECHA_EMISION")==null)?" ":rs2.getString("DF_FECHA_EMISION")+",");
					out.write((rs2.getString("DF_FECHA_VENC")==null)?" ":rs2.getString("DF_FECHA_VENC")+",");
					out.write((rs2.getString("DF_CARGA")==null)?" ":rs2.getString("DF_CARGA")+",");
					out.write((rs2.getString("IG_PLAZO_DOCTO")==null)?"0":rs2.getString("IG_PLAZO_DOCTO")+",");
					out.write((rs2.getString("MONEDA")==null)?" ":rs2.getString("MONEDA")+",");
					out.write("\"$"+Comunes.formatoDecimal((rs2.getString("FN_MONTO")==null)?"0":rs2.getString("FN_MONTO"),2,true)+"\",");
					out.write(porc_Aforo+",");
					out.write("\"$"+Comunes.formatoDecimal(MontoDesconta, 2,true)+"\",");
					out.write(IG_PLAZO_DESCUENTO+",");
					out.write(porcDescuento+"%,");
					out.write("\"$"+Comunes.formatoDecimal(montoDescontar,2,true)+"\",");
					out.write((rs2.getString("TIPO_CONVERSION")==null)?" ":rs2.getString("TIPO_CONVERSION")+",");
					out.write((rs2.getString("TIPO_CAMBIO")==null)?" ":rs2.getString("TIPO_CAMBIO")+",");
					out.write("\"$"+Comunes.formatoDecimal(Double.toString (montoValuado),2,true)+"\",");
					out.write((rs2.getString("MODO_PLAZO")==null)?" ":rs2.getString("MODO_PLAZO")+",");
					out.write((rs2.getString("IC_DOCUMENTO")==null)?" ":rs2.getString("IC_DOCUMENTO")+",");
					out.write("\"$"+Comunes.formatoDecimal((rs2.getString("MONTO_CREDITO")==null)?"0":rs2.getString("MONTO_CREDITO"),2,true)+"\",");
					out.write((rs2.getString("IG_PLAZO_CREDITO")==null)?"0":rs2.getString("IG_PLAZO_CREDITO")+",");
					out.write((rs2.getString("FECHA_VENC_CREDITO")==null)?" ":rs2.getString("FECHA_VENC_CREDITO")+",");
					out.write((rs2.getString("FECHA_SELECCION")==null)?" ":rs2.getString("FECHA_SELECCION")+",");
					out.write((rs2.getString("NOMBRE_IF")==null)?" ":rs2.getString("NOMBRE_IF").toString().replace(',',' ')+",");
					out.write((rs2.getString("REFERENCIA_INT")==null)?" ":rs2.getString("REFERENCIA_INT")+",");
					out.write((rs2.getString("VALOR_TASA_INT")==null)?"0":rs2.getString("VALOR_TASA_INT")+",");
					out.write("\"$"+Comunes.formatoDecimal((rs2.getString("MONTO_TASA_INT")==null)?"0":rs2.getString("MONTO_TASA_INT"),2)+"\",");
					out.write("\"$"+Comunes.formatoDecimal(MontoTotalTasaInte,2,true)+"\",");
					out.write((rs2.getString("FECHA_CAMBIO")==null)?" ":rs2.getString("FECHA_CAMBIO")+",");
					out.write((rs2.getString("CAUSA")==null)?" ":rs2.getString("CAUSA").toString().replace(',',' '));
					out.write("\n");

				}else if(tipoArchivo.equals("pdf")){
					pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("NOMBRE_EPO")==null)?" ":rs2.getString("NOMBRE_EPO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("CC_ACUSE")==null)?" ":rs2.getString("CC_ACUSE"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("IG_NUMERO_DOCTO")==null)?" ":rs2.getString("IG_NUMERO_DOCTO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("DF_FECHA_EMISION")==null)?" ":rs2.getString("DF_FECHA_EMISION"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("DF_FECHA_VENC")==null)?" ":rs2.getString("DF_FECHA_VENC"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("DF_CARGA")==null)?" ":rs2.getString("DF_CARGA"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("IG_PLAZO_DOCTO")==null)?"0":rs2.getString("IG_PLAZO_DOCTO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("MONEDA")==null)?"":rs2.getString("MONEDA"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((rs2.getString("FN_MONTO")==null)?"0":rs2.getString("FN_MONTO"),2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(porc_Aforo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(MontoDesconta, 2,true),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs2.getString("IG_PLAZO_DESCUENTO"),"formas",ComunesPDF.CENTER);

					pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("FN_PORC_DESCUENTO")==null)?"0":rs2.getString("FN_PORC_DESCUENTO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (montoDescontar)),2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("TIPO_CONVERSION")==null)?" ":rs2.getString("TIPO_CONVERSION"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("TIPO_CAMBIO")==null)?" ":rs2.getString("TIPO_CAMBIO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (montoValuado)),2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("MODO_PLAZO")==null)?" ":rs2.getString("MODO_PLAZO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("IC_DOCUMENTO")==null)?" ":rs2.getString("IC_DOCUMENTO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((rs2.getString("MONTO_CREDITO")==null)?"0":rs2.getString("MONTO_CREDITO"),2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("IG_PLAZO_CREDITO")==null)?"0":rs2.getString("IG_PLAZO_CREDITO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("FECHA_VENC_CREDITO")==null)?" ":rs2.getString("FECHA_VENC_CREDITO"),"formas",ComunesPDF.CENTER);

					pdfDoc.setCell("C","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("FECHA_SELECCION")==null)?" ":rs2.getString("FECHA_SELECCION"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("NOMBRE_IF")==null)?" ":rs2.getString("NOMBRE_IF"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("REFERENCIA_INT")==null)?" ":rs2.getString("REFERENCIA_INT"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("VALOR_TASA_INT")==null)?"0":rs2.getString("VALOR_TASA_INT"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((rs2.getString("MONTO_TASA_INT")==null)?"0":rs2.getString("MONTO_TASA_INT"),2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (MontoTotalTasaInte)),2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("FECHA_CAMBIO")==null)?" ":rs2.getString("FECHA_CAMBIO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs2.getString("CAUSA")==null)?" ":rs2.getString("CAUSA"),"formas",ComunesPDF.CENTER,5);

				}
			}
			rs2.close();
			ps2.close();

			if(tipoArchivo.equals("pdf")){
				if("1".equals(icMoneda)){
					pdfDoc.setCell("Total Documentos M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell("Total Monto M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);
					pdfDoc.setCell("Total Monto Valuado M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoMN,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell("Total Monto Cr�dito M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoMN,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);

				}else if("54".equals(icMoneda)){
					pdfDoc.setCell("Total Documentos Dolares ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell("Total Monto Dolares","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);
					pdfDoc.setCell("Total Monto Valuado Dolares","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell("Total Monto Cr�dito Dolares","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoUSD,2),"formas",ComunesPDF.RIGHT,2);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,5);
					}
				}
				if(tipoArchivo.equals("pdf")){ 			pdfDoc.addTable(); 			}

		}


		if(noEstatus.equals("24") ||"".equals(noEstatus)) { //No Negociable a Baja
				totalDoctosMN = 0;
					totalMontoMN = 0;
					totalMontoValuadoMN = 0;
					totalDoctosUSD = 0;
					totalMontoUSD = 0;
					totalMontoValuadoUSD = 0;
					icMoneda ="";

		strQuery =	" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,D.cc_acuse "+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
          "	,ce.ct_cambio_motivo as causa"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
          " ,comcat_estatus_docto ed "+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					"  AND ed.ic_estatus_docto = d.ic_estatus_docto "+
					" AND CE.IC_CAMBIO_ESTATUS = 24"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+
					// " and d.ic_documento in(4327, 4330)"+
					" AND D.IC_PYME = "+new Integer(noPyme);




					ps24 = con.queryPrecompilado(strQuery.toString());
					rs24 = ps24.executeQuery();
					if(tipoArchivo.equals("csv")){
						out.write("\n  No Negociable a Baja \n ");
						out.write( " EPO,N�mero de Acuse de Carga,Num. docto.inicial,Fecha de emisi�n,Fecha de vencimiento,Fecha de Publicaci�n,Plazo docto.");
						out.write( ",Moneda,Monto,Plazo para descuento en dias,% de descuento,Monto% de descuento,Tipo conv.,Tipo cambio,Monto valuado,Modalidad de plazo,Fecha Cambio, Causa \n");
					}else if(tipoArchivo.equals("pdf")){

						pdfDoc.setTable(10, 100);
						pdfDoc.setCell(" No Negociable a Baja","celda01",ComunesPDF.LEFT,10);
						pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell(" EPO","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" N�mero de Acuse de Carga","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Num. docto.inicial","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Fecha de emisi�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Fecha de vencimiento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Fecha de Publicaci�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Plazo docto.","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell(" Plazo para descuento en dias","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" % de descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Monto% de descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Tipo conv.","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Tipo cambio","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Monto valuado","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Modalidad de plazo","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Fecha Cambio","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Causa","celda01",ComunesPDF.CENTER);

					}

					while(rs24.next()){
						double monto 				= rs24.getDouble("FN_MONTO");
						double tipoCambio			= rs24.getDouble("TIPO_CAMBIO");
						String porcDescuento		= (rs24.getString("FN_PORC_DESCUENTO")==null)?"0":rs24.getString("FN_PORC_DESCUENTO");
						double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
						double montoValuado		= monto*tipoCambio;
						String bandeVentaCartera			= (rs24.getString("CG_VENTACARTERA")==null)?"":rs24.getString("CG_VENTACARTERA");
						String modoPlazo			= (rs24.getString("MODO_PLAZO")==null)?"":rs24.getString("MODO_PLAZO");
						if (bandeVentaCartera.equals("S") ) {
							modoPlazo =" ";
						}

						icMoneda	= (rs24.getString("IC_MONEDA")==null)?"":rs24.getString("IC_MONEDA");

						if("1".equals(icMoneda)){
							totalDoctosMN ++;
							totalMontoMN += monto;
							totalMontoValuadoMN += montoValuado;
							}else if("54".equals(icMoneda)){
							totalDoctosUSD ++;
							totalMontoUSD += monto;
							totalMontoValuadoUSD += montoValuado;
						}

						if(tipoArchivo.equals("csv")){
							out.write((rs24.getString("NOMBRE_EPO")==null)?" ":rs24.getString("NOMBRE_EPO").toString().replace(',',' ')+",");
							out.write((rs24.getString("CC_ACUSE")==null)?" ":rs24.getString("CC_ACUSE")+",");
							out.write((rs24.getString("IG_NUMERO_DOCTO")==null)?" ":rs24.getString("IG_NUMERO_DOCTO")+",");
							out.write((rs24.getString("DF_FECHA_EMISION")==null)?" ":rs24.getString("DF_FECHA_EMISION")+",");
							out.write((rs24.getString("DF_FECHA_VENC")==null)?" ":rs24.getString("DF_FECHA_VENC")+",");
							out.write((rs24.getString("DF_CARGA")==null)?" ":rs24.getString("DF_CARGA")+",");
							out.write((rs24.getString("IG_PLAZO_DOCTO")==null)?"0":rs24.getString("IG_PLAZO_DOCTO")+",");
							out.write((rs24.getString("MONEDA")==null)?" ":rs24.getString("MONEDA")+",");
							out.write("\"$"+Comunes.formatoDecimal((rs24.getString("FN_MONTO")==null)?"0":rs24.getString("FN_MONTO"),2,true)+"\",");
							out.write((rs24.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs24.getString("IG_PLAZO_DESCUENTO")+",");
							out.write((rs24.getString("FN_PORC_DESCUENTO")==null)?"0":rs24.getString("FN_PORC_DESCUENTO")+",");
							out.write("\"$"+Comunes.formatoDecimal((Double.toString (montoDescontar)),2,true)+"\",");
							out.write((rs24.getString("TIPO_CONVERSION")==null)?" ":rs24.getString("TIPO_CONVERSION")+",");
							out.write((rs24.getString("TIPO_CAMBIO")==null)?" ":rs24.getString("TIPO_CAMBIO")+",");
							out.write("\"$"+Comunes.formatoDecimal(Double.toString (montoValuado),2,true)+"\",");
							out.write(modoPlazo+" "+",");
							out.write((rs24.getString("FECHA_CAMBIO")==null)?" ":rs24.getString("FECHA_CAMBIO")+",");
							out.write((rs24.getString("CAUSA")==null)?" ":rs24.getString("CAUSA").toString().replace(',',' ')+"\n");

						}else if(tipoArchivo.equals("pdf")){
							pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("NOMBRE_EPO")==null)?" ":rs24.getString("NOMBRE_EPO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("CC_ACUSE")==null)?" ":rs24.getString("CC_ACUSE"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("IG_NUMERO_DOCTO")==null)?" ":rs24.getString("IG_NUMERO_DOCTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("DF_FECHA_EMISION")==null)?" ":rs24.getString("DF_FECHA_EMISION"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("DF_FECHA_VENC")==null)?" ":rs24.getString("DF_FECHA_VENC"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("DF_CARGA")==null)?" ":rs24.getString("DF_CARGA"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("IG_PLAZO_DOCTO")==null)?"0":rs24.getString("IG_PLAZO_DOCTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("MONEDA")==null)?" ":rs24.getString("MONEDA"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal((rs24.getString("FN_MONTO")==null)?"0":rs24.getString("FN_MONTO"),2),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs24.getString("IG_PLAZO_DESCUENTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("FN_PORC_DESCUENTO")==null)?"0":rs24.getString("FN_PORC_DESCUENTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (montoDescontar)),2),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("TIPO_CONVERSION")==null)?" ":rs24.getString("TIPO_CONVERSION"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("TIPO_CAMBIO")==null)?" ":rs24.getString("TIPO_CAMBIO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (montoValuado)),2),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("FECHA_CAMBIO")==null)?" ":rs24.getString("FECHA_CAMBIO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs24.getString("CAUSA")==null)?" ":rs24.getString("CAUSA").toString().replace(',',' '),"formas",ComunesPDF.CENTER);

						}
					}
					rs24.close();
					ps24.close();
						if(tipoArchivo.equals("pdf")){
				if("1".equals(icMoneda)){
					pdfDoc.setCell("Total Documentos M.N. ","formas",ComunesPDF.CENTER,2);
					pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto Valuado M.N. ","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoMN,2),"formas",ComunesPDF.RIGHT,2);

				}else if("54".equals(icMoneda)){
					pdfDoc.setCell("Total Documentos Dolares ","formas",ComunesPDF.CENTER,2);
					pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto Dolares","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("Total Monto Valuado Dolares","formas",ComunesPDF.LEFT,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.RIGHT,2);
					}
				}

					if(tipoArchivo.equals("pdf")){ 	pdfDoc.addTable(); 	}

			}

			if(noEstatus.equals("21") ||"".equals(noEstatus)) { //Modificaciones

				strQuery =	" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,D.cc_acuse "+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					"	,to_char(CE.DF_FECHA_EMISION_ANTERIOR,'dd/mm/yyyy') as FECHA_EMISION_ANT"+
					"	,to_char(CE.DF_FECHA_VENC_ANTERIOR,'dd/mm/yyyy') as FECHA_VENC_ANT"+
					"	,CE.fn_monto_anterior"+
					"	,TF2.cd_descripcion AS MODO_PLAZO_ANT"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF2"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND CE.IC_CAMBIO_ESTATUS = 21"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+
					" AND TF2.IC_TIPO_FINANCIAMIENTO(+) = CE.IC_TIPO_FINAN_ANT"+
					//" and d.ic_documento in(3900, 3991) "+
					" AND D.IC_PYME = "+new Integer(noPyme);

					ps21= con.queryPrecompilado(strQuery.toString());
					rs21 = ps21.executeQuery();

				if(tipoArchivo.equals("csv") ){
					out.write("\n Modificaci�n \n");
					out.write(" EPO,N�mero de Acuse de Carga,Num. docto.inicial,Fecha de emisi�n,Fecha de vencimiento,Fecha de Publicaci�n,Plazo docto.,Monto,");
					out.write(" Moneda,Plazo para descuento en dias,% de descuento,Monto % de descuento,Tipo conv.,Tipo cambio,Monto valuado en pesos,Modalidad de plazo,Fecha Cambio");
					out.write(",Anterior Fecha de Emisi�n ,Anterior Fecha de Vencimiento,Anterior Monto del Documento,Anterior Modalidad de Plazo \n");

				}else if(tipoArchivo.equals("pdf") ){
					pdfDoc.setTable(12, 100);
					pdfDoc.setCell(" Modificaci�n","celda01",ComunesPDF.LEFT,12);
					pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell(" EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" N�mero de Acuse de Carga","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Num. docto.inicial","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Fecha de emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Fecha de vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Fecha de Publicaci�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Plazo docto.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Monto","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Plazo para descuento en dias,","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" % de descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell(" Monto % de descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Tipo conv.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Tipo cambio,","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Monto valuado en pesos,","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Modalidad de plazo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Fecha Cambio","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Anterior Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Anterior Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Anterior Monto del Documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell(" Anterior Modalidad de Plazo","celda01",ComunesPDF.CENTER,2);


				}
					while(rs21.next()){

						double monto 				= rs21.getDouble("FN_MONTO");
						double tipoCambio			= rs21.getDouble("TIPO_CAMBIO");
						String porcDescuento		= (rs21.getString("FN_PORC_DESCUENTO")==null)?"0":rs21.getString("FN_PORC_DESCUENTO");
						double montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
						double montoValuado		= monto*tipoCambio;
						String bandeVentaCartera			= (rs21.getString("CG_VENTACARTERA")==null)?"":rs21.getString("CG_VENTACARTERA");
						String modoPlazo			= (rs21.getString("MODO_PLAZO")==null)?"":rs21.getString("MODO_PLAZO");
						if (bandeVentaCartera.equals("S") ) {
							modoPlazo =" ";
						}

						icMoneda	= (rs21.getString("IC_MONEDA")==null)?"":rs21.getString("IC_MONEDA");
						String IG_PLAZO_DESCUENTO = (rs21.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs21.getString("IG_PLAZO_DESCUENTO");
						//String FECHA_CAMBIO = (rs21.getString("FECHA_CAMBIO")==null)?" ":rs21.getString("FECHA_CAMBIO");
						String FECHA_EMISION_ANT = (rs21.getString("FECHA_EMISION_ANT")==null)?" ":rs21.getString("FECHA_EMISION_ANT");
						String FECHA_VENC_ANT  = (rs21.getString("FECHA_VENC_ANT")==null)?" ":rs21.getString("FECHA_VENC_ANT");


						if("1".equals(icMoneda)){
							totalDoctosMN ++;
							totalMontoMN += monto;
							totalMontoValuadoMN += montoValuado;
							}else if("54".equals(icMoneda)){
							totalDoctosUSD ++;
							totalMontoUSD += monto;
							totalMontoValuadoUSD += montoValuado;
						}

						if(tipoArchivo.equals("csv")){
							out.write((rs21.getString("NOMBRE_EPO")==null)?" ":rs21.getString("NOMBRE_EPO").toString().replace(',',' ')+",");
							out.write((rs21.getString("CC_ACUSE")==null)?" ":rs21.getString("CC_ACUSE")+",");
							out.write((rs21.getString("IG_NUMERO_DOCTO")==null)?" ":rs21.getString("IG_NUMERO_DOCTO")+",");
							out.write((rs21.getString("DF_FECHA_EMISION")==null)?" ":rs21.getString("DF_FECHA_EMISION")+",");
							out.write((rs21.getString("DF_FECHA_VENC")==null)?" ":rs21.getString("DF_FECHA_VENC")+",");
							out.write((rs21.getString("DF_CARGA")==null)?" ":rs21.getString("DF_CARGA")+",");
							out.write((rs21.getString("IG_PLAZO_DOCTO")==null)?"0":rs21.getString("IG_PLAZO_DOCTO").toString().replace(',',' ')+",");
							out.write("\"$"+Comunes.formatoDecimal((rs21.getString("FN_MONTO")==null)?"0":rs21.getString("FN_MONTO"),2,true)+"\",");
							out.write((rs21.getString("MONEDA")==null)?" ":rs21.getString("MONEDA")+",");
							out.write(IG_PLAZO_DESCUENTO+",");
							out.write((rs21.getString("FN_PORC_DESCUENTO")==null)?"0":rs21.getString("FN_PORC_DESCUENTO")+"%,");
							out.write("\"$"+Comunes.formatoDecimal(montoDescontar,2,true)+"\",");
							out.write((rs21.getString("TIPO_CONVERSION")==null)?" ":rs21.getString("TIPO_CONVERSION")+",");
							out.write((rs21.getString("TIPO_CAMBIO")==null)?" ":rs21.getString("TIPO_CAMBIO")+",");
							out.write("\"$"+Comunes.formatoDecimal(montoValuado,2,true)+"\",");
							out.write(modoPlazo+",");
							out.write((rs21.getString("FECHA_CAMBIO")==null)?" ":rs21.getString("FECHA_CAMBIO")+",");
							out.write(FECHA_EMISION_ANT+",");
							out.write(FECHA_VENC_ANT+",");
							out.write("\"$"+Comunes.formatoDecimal((rs21.getString("FN_MONTO_ANTERIOR")==null)?"0":rs21.getString("FN_MONTO_ANTERIOR"),2,true)+"\",");
							out.write((rs21.getString("MODO_PLAZO_ANT")==null)?" ":rs21.getString("MODO_PLAZO_ANT"));
							out.write("\n");

						}else if(tipoArchivo.equals("pdf")){
							pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("NOMBRE_EPO")==null)?" ":rs21.getString("NOMBRE_EPO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("CC_ACUSE")==null)?" ":rs21.getString("CC_ACUSE"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("IG_NUMERO_DOCTO")==null)?"":rs21.getString("IG_NUMERO_DOCTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("DF_FECHA_EMISION")==null)?"":rs21.getString("DF_FECHA_EMISION"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("DF_FECHA_VENC")==null)?"":rs21.getString("DF_FECHA_VENC"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("DF_CARGA")==null)?"":rs21.getString("DF_CARGA"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("IG_PLAZO_DOCTO")==null)?"0":rs21.getString("IG_PLAZO_DOCTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal((rs21.getString("FN_MONTO")==null)?"0":rs21.getString("FN_MONTO"),2),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("MONEDA")==null)?"":rs21.getString("MONEDA"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("IG_PLAZO_DESCUENTO")==null)?"0":rs21.getString("IG_PLAZO_DESCUENTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("FN_PORC_DESCUENTO")==null)?"0":rs21.getString("FN_PORC_DESCUENTO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (montoDescontar)),2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell((rs21.getString("TIPO_CONVERSION")==null)?"":rs21.getString("TIPO_CONVERSION"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("TIPO_CAMBIO")==null)?"":rs21.getString("TIPO_CAMBIO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal((Double.toString (montoValuado)),2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("FECHA_CAMBIO")==null)?"":rs21.getString("FECHA_CAMBIO"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("FECHA_EMISION_ANT")==null)?"":rs21.getString("FECHA_EMISION_ANT"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell((rs21.getString("FECHA_VENC_ANT")==null)?"":rs21.getString("FECHA_VENC_ANT"),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal((rs21.getString("FN_MONTO_ANTERIOR")==null)?"0":rs21.getString("FN_MONTO_ANTERIOR"),2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell((rs21.getString("MODO_PLAZO_ANT")==null)?"":rs21.getString("MODO_PLAZO_ANT"),"formas",ComunesPDF.CENTER,2);
						}
					}
					rs21.close();
					ps21.close();

					if(tipoArchivo.equals("pdf")){
						if("1".equals(icMoneda)){
							pdfDoc.setCell("Total Documentos M.N. ","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("Total Monto M.N. ","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoMN,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("Total Monto Valuado M.N. ","formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoMN,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell(" ","formas",ComunesPDF.RIGHT,3);

						}else if("54".equals(icMoneda)){
							pdfDoc.setCell("Total Documentos Dolares ","formas",ComunesPDF.LEFT,2);
							pdfDoc.setCell(String.valueOf(totalDoctosMN),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("Total Monto Dolares","formas",ComunesPDF.LEFT,2);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoUSD,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("Total Monto Valuado Dolares","formas",ComunesPDF.LEFT,2);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoUSD,2),"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell(" ","formas",ComunesPDF.RIGHT,3);
							}
				}

			if(tipoArchivo.equals("pdf")){  pdfDoc.addTable(); 			}

		}
		if(tipoArchivo.equals("pdf")){
			pdfDoc.endDocument();
			nombreArchivo  = nombreArchivoPDF;
		}
		if(tipoArchivo.equals("csv")){
			nombreArchivo  = nombreArchivoCSV;
		}

	 	}catch(Exception e){
			log.info("repCambioEstatusCSVPDF (Error )" +e);
			throw new AppException("repCambioEstatusCSVPDF(Exception) ", e);
		}finally{
			if (out 	!= null )  try { out.close();   } catch(Exception    e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB();
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
			log.info("repCambioEstatusCSVPDF (S)");
		}


		return nombreArchivo;
	}
//Reportes Admin EPO - Documentos por estatus
	public String getTipoCredito(String claveEpo)	{
	log.info("getTiposCredito");

		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String cgTiposCredito = "";

		try {
      con.conexionDB();
		qrySentencia = new StringBuffer();
      qrySentencia.append("SELECT NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) AS tipos_credito" +
										" FROM comrel_producto_epo pe, comcat_producto_nafin pn" +
									" WHERE pe.ic_producto_nafin = pn.ic_producto_nafin" +
										" AND pe.ic_epo = ?" +
										" AND pn.ic_producto_nafin = ?");
		ps = con.queryPrecompilado(qrySentencia.toString());
		ps.setInt(1, Integer.parseInt(claveEpo));
		ps.setInt(2, 4);//4=Financiamiento a Distribuidores
		rs = ps.executeQuery();
		while(rs.next()){
			cgTiposCredito  = (rs.getString("tipos_credito")!=null)?rs.getString("tipos_credito"):"";
		}
		rs.close();
      ps.close();
		log.debug("qrySentencia= " + qrySentencia.toString());
		log.debug("cgTiposCredito" +cgTiposCredito);
		} catch(Exception e) {
			log.error("getTipoCredito(Exception) "+e);
			throw new AppException("Error al obtener el tipo de cr�dito",e);
		} finally {
			con.cierraConexionDB();
			log.info(" getTipoCredito");
		}
		return cgTiposCredito;
	}


 /** para la pantalla de Esquema de Financiamiento por Cliente
	 * @return
	 * @param claveEpo
	 */

 	public List esquemaCliente(String claveEpo) {
		log.info("esquemaCliente(E)");
		boolean bOk = true;
		AccesoDB con = new AccesoDB();
		StringBuffer sQuery	= new StringBuffer();
		List datos  = new ArrayList();

		try {
			con.conexionDB();

    	sQuery.append(" 	SELECT DECODE (cg_responsable_interes, 'D', 'Distribuidor', 'E', 'EPO') as responsable, "+
		"       tf.cd_descripcion  as descripcion "+
		"  FROM comrel_producto_epo pe, "+
		"       comrel_epo_tipo_financiamiento etf, "+
		"       comcat_tipo_financiamiento tf "+
		" WHERE etf.ic_epo = ?  "+
		"   AND etf.ic_producto_nafin = 4 "+
		"   AND etf.ic_epo = pe.ic_epo "+
		"   AND etf.ic_producto_nafin = pe.ic_producto_nafin "+
		"   AND etf.ic_tipo_financiamiento = tf.ic_tipo_financiamiento ");

			log.debug("sQuery.toString()" +sQuery.toString());
			log.debug("claveEpo" +claveEpo);

			PreparedStatement ps = con.queryPrecompilado(sQuery.toString());
			ps.setInt(1, Integer.parseInt(claveEpo));
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				datos.add(rs.getString("responsable")==null?"":rs.getString("responsable"));
				datos.add(rs.getString("descripcion")==null?"":rs.getString("descripcion"));
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			bOk = false;
			throw new AppException("esquemaCliente.", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("esquemaCliente(S)");
		}
		return datos;

	}

	/**
	 * este metodo muestra el grid Principal de
	 * @return
	 * @param claveEpo
	 */

		public HashMap esquemaCliente2(String claveEpo) {
		log.info("esquemaCliente2(E)");
		boolean bOk = true;
		AccesoDB con = new AccesoDB();
		StringBuffer sQuery	= new StringBuffer();
		HashMap datos   = new HashMap();
		HashMap datosFi = new HashMap();

		try {
			con.conexionDB();

			sQuery.append(" SELECT cif.cg_razon_social as NOMBREIF , "+
			" lc.ic_linea_credito_dm  as  NUMLINEA , "+
			" cm.cd_nombre as MONEDA, "+
			"       TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS FECHA_AUTORIZACION, "+
			"       lc.ig_plazo as PLAZO ,"+
			" lc.fn_monto_autorizado_total  as MONTO_LINEA ,"+
			" tci.cd_descripcion as ESQUEMA "+
			"  FROM dis_linea_credito_dm lc, "+
			"       comcat_if cif, "+
			"       comcat_moneda cm, "+
			"       dis_acuse4 ac4, "+
			"       comcat_tipo_cobro_interes tci "+
			" WHERE lc.ic_epo =  ?"+
			"   AND lc.ic_producto_nafin = 4 "+
			"   AND lc.ic_if = cif.ic_if "+
			"   AND lc.ic_moneda = cm.ic_moneda "+
			"   AND lc.cc_acuse = ac4.cc_acuse "+
			"   AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes ");

			log.debug("sQuery.toString()" +sQuery.toString());
			log.debug("claveEpo" +claveEpo);

			PreparedStatement ps = con.queryPrecompilado(sQuery.toString());
			ps.setInt(1, Integer.parseInt(claveEpo));
			ResultSet rs = ps.executeQuery();
			int numeroRegistros = 0;
			while(rs.next()) {
				datos = new HashMap();
				datos.put("NOMBREIF",rs.getString("NOMBREIF")==null?"":rs.getString("NOMBREIF"));
				datos.put("NUMLINEA", rs.getString("NUMLINEA")==null?"":rs.getString("NUMLINEA"));
				datos.put("MONEDA",rs.getString("MONEDA")==null?"":rs.getString("MONEDA"));
				datos.put("FECHA_AUTORIZACION",rs.getString("FECHA_AUTORIZACION")==null?"":rs.getString("FECHA_AUTORIZACION"));
				datos.put("PLAZO",rs.getString("PLAZO")==null?"":rs.getString("PLAZO"));
				datos.put("MONTO_LINEA",rs.getString("MONTO_LINEA")==null?"":rs.getString("MONTO_LINEA"));
				datos.put("ESQUEMA",rs.getString("ESQUEMA")==null?"":rs.getString("ESQUEMA"));
			  datosFi.put("REGISTROS" + numeroRegistros, datos);
				numeroRegistros++;
			}
			rs.close();
			ps.close();

			return datosFi;

		} catch (Exception e) {
			bOk = false;
			throw new AppException("esquemaCliente2", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("esquemaCliente2(S)");
		}

	}


	/*  para la pantalla de Esquema de Financiamiento por Cliente
	 *  muestra el tipo de credito y la razon social de la pyme seleccionada
	 * @return
	 * @param clavePymme
	 * @param claveEpo
	 */

 	public List esquemaCliente3(String claveEpo, String  clavePymme) {
		log.info("esquemaCliente3(E)");
		boolean bOk = true;
		AccesoDB con = new AccesoDB();
		StringBuffer sQuery	= new StringBuffer();
		List datos  = new ArrayList();

		try {
			con.conexionDB();

    	sQuery.append(" SELECT pexp.cg_tipo_credito as credito, cpy.cg_razon_social  as razonSocial"+
		"  FROM comrel_pyme_epo_x_producto pexp, comcat_pyme cpy "+
		" WHERE pexp.ic_pyme = ? "+
		"   AND pexp.ic_epo = ? "+
		"   AND pexp.ic_producto_nafin = 4 "+
		"   AND pexp.ic_pyme = cpy.ic_pyme ");

			log.debug("sQuery.toString()" +sQuery.toString());
			log.debug("claveEpo" +claveEpo);
			log.debug("clavePymme" +clavePymme);

			PreparedStatement ps = con.queryPrecompilado(sQuery.toString());
			ps.setInt(1, Integer.parseInt(clavePymme));
			ps.setInt(2, Integer.parseInt(claveEpo));
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				datos.add(rs.getString("credito")==null?"":rs.getString("credito"));
				datos.add(rs.getString("razonSocial")==null?"":rs.getString("razonSocial"));
			}
			rs.close();
			ps.close();

		} catch (Exception e) {
			bOk = false;
			throw new AppException("esquemaCliente.", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("esquemaCliente3(S)");
		}
		return datos;

	}


	/*
	 * para la pantalla de Esquema de Financiamiento por Cliente
	 * para cuando la epo opera CCC o Ambos (CCC y Mercantil)
	 * y muestra el segundo grid cuando se selecciona el la Pyme
	 * @return
	 * @param claveEpo
	 */


	public HashMap esquemaCliente4(String claveEpo, String clavePyme) {
		log.info("esquemaCliente4(E)");
		boolean bOk = true;
		AccesoDB con = new AccesoDB();
		StringBuffer sQuery	= new StringBuffer();
		HashMap datos  = new HashMap();
		HashMap datosFi = new HashMap();

		try {
			con.conexionDB();

    sQuery.append(" SELECT cif.cg_razon_social as NOMBREIF   , cm.cd_nombre as MONEDA  , tci.cd_descripcion  as ESQUEMA"+
		"  FROM com_linea_credito lc, "+
		"       comcat_if cif, "+
		"       comcat_moneda cm, "+
		"       comcat_tipo_cobro_interes tci "+
		" WHERE lc.ic_epo = ? "+
		"   AND lc.ic_pyme = ? "+
		"   AND lc.ic_producto_nafin = 4 "+
		"   AND lc.ic_if = cif.ic_if "+
		"   AND lc.ic_moneda = cm.ic_moneda "+
		"   AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes ");

			log.debug("sQuery.toString()" +sQuery.toString());
			log.debug("claveEpo" +claveEpo);
			log.debug("clavePyme" +clavePyme);

			PreparedStatement ps = con.queryPrecompilado(sQuery.toString());
			ps.setInt(1, Integer.parseInt(claveEpo));
			ps.setInt(2, Integer.parseInt(clavePyme));
			ResultSet rs = ps.executeQuery();
			int numeroRegistros = 0;
			while(rs.next()) {
				datos = new HashMap();
				datos.put("NOMBREIF",rs.getString("NOMBREIF")==null?"":rs.getString("NOMBREIF"));
				datos.put("MONEDA", rs.getString("MONEDA")==null?"":rs.getString("MONEDA"));
				datos.put("ESQUEMA",rs.getString("ESQUEMA")==null?"":rs.getString("ESQUEMA"));
			  datosFi.put("REGISTROS" + numeroRegistros, datos);
				numeroRegistros++;
			}
			rs.close();
			ps.close();

			return datosFi;

		} catch (Exception e) {
			bOk = false;
			throw new AppException("esquemaCliente4", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("esquemaCliente4(S)");
		}

	}

	/** datos de grid de esquema de parametros (24consulta08Ext.data.jsp)
	 * @return
	 * @param claveEpo
	 */
		public List esquemasOperacion(String claveEpo){

		boolean bOk = true;
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		List datos  = new ArrayList();

		try {
			con.conexionDB();

    qrySentencia="select PE.cg_tipos_credito as TIPO_CREDITO_OPERAR" +
" , decode(PE.cg_responsable_interes, 'E', 'EPO', 'D', 'Distribuidor') as RESPOSABILIDAD_PAGO_INTERES" +
" , decode(PE.cg_tipo_cobranza, 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA" +
" , TCI.cd_descripcion as ESQUEMA_PAGO_INTERES" +
" , VTF.cd_descripcion as MODALIDAD_PLAZO_OPERAR" +

" from comrel_producto_epo PE, comcat_tipo_cobro_interes TCI" +
"    ,(select tf.cd_descripcion,etf.ic_epo,etf.ic_producto_nafin from comcat_tipo_financiamiento tf, comrel_epo_tipo_financiamiento etf" +
"     where etf.ic_tipo_financiamiento = tf.ic_tipo_financiamiento" +
"     and etf.ic_producto_nafin = 4 ) vtf" +
" where PE.ic_producto_nafin = 4" +
" and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes(+)" +
" and PE.ic_producto_nafin = VTF.ic_producto_nafin(+)" +
" and PE.ic_epo = VTF.ic_epo(+)";

	if(!"".equals(claveEpo))
		qrySentencia += " and PE.ic_epo = " + claveEpo;

			ResultSet rs  = con.queryDB(qrySentencia);
			while(rs.next()) {
				datos.add((rs.getString("TIPO_CREDITO_OPERAR")==null)?"":(rs.getString("TIPO_CREDITO_OPERAR")));
				datos.add((rs.getString("RESPOSABILIDAD_PAGO_INTERES")==null)?"":(rs.getString("RESPOSABILIDAD_PAGO_INTERES")));
				datos.add((rs.getString("TIPO_COBRANZA")==null)?"":(rs.getString("TIPO_COBRANZA")));
				datos.add((rs.getString("ESQUEMA_PAGO_INTERES")==null)?"":(rs.getString("ESQUEMA_PAGO_INTERES")));
				datos.add((rs.getString("MODALIDAD_PLAZO_OPERAR")==null)?"":(rs.getString("MODALIDAD_PLAZO_OPERAR")));
			}
			rs.close();


		} catch (Exception e) {
			bOk = false;
			throw new AppException("esquemaCliente.", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("esquemaCliente(S)");
		}
		return datos;

	}
	/** datos de grid de Parametros (24consulta08Ext.data.jsp)
	 * @return
	 * @param claveEpo
	 */
		public List parametros(String claveEpo){

		boolean bOk = true;
		AccesoDB con = new AccesoDB();
		String qrySentencia = "";
		List datos  = new ArrayList();

		try {
			con.conexionDB();

    qrySentencia="select pe.ig_dias_minimo as DIAS_MINIMOS" +
	", pe.cg_tipos_credito as TIPO_CREDITO_OPERAR" +
	", pe.ig_dias_maximo as  DIAS_MAXIMOS" +
	", decode(pe.cg_tipo_conversion, 'P', 'Peso - Dolar', '', 'Sin conversion') as TIPO_CONVERSION" +
	", decode(pe.cs_cifras_control, 'S', 'Si mostrar', 'N', 'No mostrar') as   PANTALLA_CIFRAS_CONTROL" +
	", pe.ig_plazo_dscto,pe.fn_porcentaje_dscto"+
	", pe.IG_DIAS_AVISO_VENC as  IG_DIAS_AVISO_VENC, pe.ig_dias_amp_fecha_venc " +
	" from comrel_producto_epo pe" +

	" where pe.ic_producto_nafin = 4";

	if(!"".equals(claveEpo))
		qrySentencia += " and pe.ic_epo = " + claveEpo;
		ResultSet rs  = con.queryDB(qrySentencia);
			if(rs.next()) {
			datos.add((rs.getString("DIAS_MINIMOS")==null)?"":(rs.getString("DIAS_MINIMOS")));
			datos.add((rs.getString("DIAS_MAXIMOS")==null)?"":(rs.getString("DIAS_MAXIMOS")));
			datos.add((rs.getString("TIPO_CONVERSION")==null)?"":(rs.getString("TIPO_CONVERSION")));
			datos.add((rs.getString("PANTALLA_CIFRAS_CONTROL")==null)?"":(rs.getString("PANTALLA_CIFRAS_CONTROL")));
			datos.add((rs.getString("IG_PLAZO_DSCTO")==null)?"":(rs.getString("IG_PLAZO_DSCTO")));
			datos.add((rs.getString("FN_PORCENTAJE_DSCTO")==null)?"":(rs.getString("FN_PORCENTAJE_DSCTO")));
			datos.add((rs.getString("IG_DIAS_AVISO_VENC")==null)?"":(rs.getString("IG_DIAS_AVISO_VENC")));
			datos.add((rs.getString("ig_dias_amp_fecha_venc")==null)?"":(rs.getString("ig_dias_amp_fecha_venc")));
			}
			rs.close();


		} catch (Exception e) {
			bOk = false;
			throw new AppException("esquemaCliente.", e);
		} finally {
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
			log.info("esquemaCliente(S)");
		}
		return datos;

	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa el tipo de credito EPO
	 * @param ic_epo
	 */
	public String getTipoCreditoEpo(String ic_epo)	{
		log.info("getTipoCreditoEpo");
		StringBuffer qrySentencia;
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		AccesoDB con = new AccesoDB();
		String tipoCreditoEpo = "";

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) "+
									"	FROM comrel_producto_epo pe, comcat_producto_nafin pn "+
									"	WHERE pe.ic_epo = ?"+
									"  AND pn.ic_producto_nafin = ? "+
									"  AND pe.ic_producto_nafin = pn.ic_producto_nafin");
			log.debug("qrySentencia= " + qrySentencia.toString());
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, Integer.parseInt(ic_epo));
			ps.setInt(2, 4);
			rs = ps.executeQuery();

			while(rs.next()){
					tipoCreditoEpo = rs.getString(1);
			}
			log.debug("getTipoCreditoEpo" +tipoCreditoEpo);
			rs.close();
			ps.close();
		} catch(Exception e) {
			log.error("getTipoCreditoEpo(Exception) "+e);
			throw new AppException("Error al obtener parametro tipo credito",e);
		} finally {
        con.cierraConexionDB();
			log.info(" getTipoCreditoEpo");
		}

		return tipoCreditoEpo;
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante.
	 * @param folio, tipo_credito
	 */
	public Registros getHistoricoCredito(String folio, String tipo_credito)	{
		log.info("getHistoricoCredito");
		StringBuffer qrySentencia;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			if ("D".equals(tipo_credito)){
				qrySentencia.append(
					" SELECT lcact.ic_linea_credito_dm as folio"+
					" 	   ,'Descuento y/o Factoraje' as tipoCredito"   +
					"      ,DECODE (lchis.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol"   +
					"      ,TO_CHAR (cn.ic_nafin_electronico) as nEpo"   +
					" 	   , ce.cg_razon_social as EPO"   +
					" 	   ,'N/A' as NPyme"   +
					" 	   ,'N/A' as Pyme"   +
					" 	   , TO_CHAR (ac4his.df_acuse, 'DD/MM/YYYY') AS fecha_auto_his"   +
					" 	   , TO_CHAR (ac4act.df_acuse, 'DD/MM/YYYY') AS fecha_auto_act"   +
					" 	   , cm.cd_nombre as moneda"   +
					"      ,'' AS causas"   +
					"      ,TO_CHAR (lchis.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc_his"   +
					"      ,TO_CHAR (lcact.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc_act	   "   +
					"      ,lchis.fn_monto_autorizado_total as monto_auto_his ,lchis.FN_SALDO_TOTAL as saldo_total_his"   +
					"      ,lcact.fn_monto_autorizado_total as monto_auto_act ,lcact.FN_SALDO_TOTAL as saldo_total_act "   +
					"   FROM dis_linea_credito_dm lcact,"   +
					" 		dis_linea_credito_dm lchis,"   +
					"        comrel_nafin cn,"   +
					"        comcat_epo ce,"   +
					"        dis_acuse4 ac4his,"   +
					"        dis_acuse4 ac4act,"   +
					"        comcat_moneda cm"   +
					"  WHERE lchis.ic_epo = cn.ic_epo_pyme_if"   +
					"    AND cn.cg_tipo = 'E'"   +
					"    AND lchis.ic_epo = ce.ic_epo"   +
					"    AND lchis.cc_acuse = ac4his.cc_acuse"   +
					"    AND lcact.cc_acuse = ac4act.cc_acuse"   +
					"    AND lchis.ic_moneda = cm.ic_moneda"   +
					"    AND lchis.ic_linea_credito_dm_padre = lcact.ic_linea_credito_dm"   +
					"    AND lcact.ic_linea_credito_dm = ?");

			}else if("C".equals(tipo_credito)){
				qrySentencia.append(
					"  SELECT  lcact.ic_linea_credito as folio"   +
					"  	  ,'Credito en Cuenta Corriente' as tipoCredito"   +
					"       ,DECODE (lchis.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol"   +
					"       ,'N/A' as nEpo"   +
					"  	  ,'N/A' as EPO"   +
					"       ,TO_CHAR (cn.ic_nafin_electronico) as NPyme"   +
					"  	  ,cp.cg_razon_social as Pyme"   +
					"  	  ,TO_CHAR (lchis.df_autorizacion_if, 'DD/MM/YYYY') AS fecha_auto_his"   +
					"  	  ,TO_CHAR (lcact.df_autorizacion_if, 'DD/MM/YYYY') AS fecha_auto_act"   +
					"  	  ,cm.cd_nombre as moneda"   +
					"       ,'' AS causas"   +
					"       ,TO_CHAR (lchis.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc_his"   +
					"       ,TO_CHAR (lcact.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc_act"   +
					"       ,lchis.fn_monto_autorizado_total as monto_auto_his ,lchis.FN_SALDO_LINEA_TOTAL as saldo_total_his"   +
					"       ,lcact.fn_monto_autorizado_total as monto_auto_act ,lcact.FN_SALDO_LINEA_TOTAL as saldo_total_act "   +
					"    FROM com_linea_credito lcact,"   +
					"  		com_linea_credito lchis,"   +
					"         comrel_nafin cn,"   +
					"         comcat_pyme cp,"   +
					"         comcat_moneda cm"   +
					"   WHERE lcact.ic_pyme = cn.ic_epo_pyme_if"   +
					"     AND cn.cg_tipo = 'P'"   +
					"     AND lcact.ic_pyme = cp.ic_pyme"   +
					"     AND lchis.ic_moneda = cm.ic_moneda"   +
					"     AND lchis.ic_linea_credito_padre = lcact.ic_linea_credito"   +
					"     AND lcact.ic_linea_credito = ?" ) ;
			}

			log.debug("qrySentencia= " + qrySentencia.toString());
			List params = new ArrayList();
			params.add(new Long(folio));
			Registros reg = con.consultarDB(qrySentencia.toString(), params);
			return reg;
		} catch(Exception e) {
			log.error("getHistoricoCredito(Exception) "+e);
			throw new AppException("Error al obtener historico de credito",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getHistoricoCredito");
		}
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante de la informacion del layout factoraje vencimiento
	 * @param clave_if
	 */
	public Registros getLayoutFactorajeVencimiento(String clave_if){
		log.info("getLayoutFactorajeVencimiento");
		StringBuffer qrySentencia;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append(
					"SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST, D.IG_NUMERO_DOCTO, D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,d.FN_TIPO_CAMBIO,'1'),'1') as TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
					"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,E.cg_razon_social as NOMBRE_EPO"   +
					" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"   +
					" 	,I.cg_razon_social as NOMBRE_IF"   +
					" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
					" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
					" 	,D.ig_plazo_credito"   +
					" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
					"  ,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
					" 	,DS.fn_valor_tasa as VALOR_TASA_INT"   +
					" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
					"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
					"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
					"	,LC.ic_moneda as MONEDA_LINEA"+
					"	, '24finandist/24if/24reporte01ext.jsp' as pantalla "+
					"FROM DIS_DOCUMENTO D"   +
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4 "   +
					"    union all"   +
					"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4 ) LC"   +
					"  ,COMCAT_EPO E"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,COMCAT_IF I"   +
					"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMCAT_TASA CT "   +
					"WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ?"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PE.IC_EPO = D.IC_EPO"   +
					"  AND I.IC_IF = LC.IC_IF"   +
					"  AND E.IC_EPO = D.IC_EPO"   +
					"  AND PP.IC_EPO = E.IC_EPO"   +
					"  AND PP.IC_PYME = PY.IC_PYME"   +
					"  AND PP.CG_TIPO_CREDITO = ? "  +
					"  AND PP.IC_PRODUCTO_NAFIN = ?"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
					"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
					"  AND CT.IC_TASA = DS.IC_TASA"   +
					"  AND D.IC_ESTATUS_DOCTO = ?"   +
					"  AND LC.IC_IF = ? "   +
					"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
					"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO "
					//+ "  AND DS.DF_FECHA_SELECCION >= TRUNC(SYSDATE)"  +
					//"  AND DS.DF_FECHA_SELECCION < TRUNC(SYSDATE+1)"
			);
			List params = new ArrayList();
			params.add(new Integer(4));
			params.add("C");
			params.add(new Integer(4));
			params.add(new Integer(3));
			params.add(new Long(clave_if));
			log.debug("qrySentencia= " + qrySentencia.toString());
			log.debug("params= " + params);
			Registros reg = con.consultarDB(qrySentencia.toString(), params);
			return reg;
		} catch(Exception e) {
			log.error("getLayoutFactorajeVencimiento(Exception) "+e);
			throw new AppException("Error al obtener layout factoraje vencimiento",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getLayoutFactorajeVencimiento");
		}
	}

	/**
	 *
	 * @throws com.netro.exception.AppException
	 * @return Regresa los registros del query resultante de la informacion del layout factoraje vencimiento para el tipo de cr�dito Factoraje
	 * @param clave_if, tipoCredito
	 */
	public Registros getLayoutFactorajeVencimiento(String clave_if, String tipoCredito){
		log.info("getLayoutFactorajeVencimiento");
		StringBuffer qrySentencia;
		AccesoDB con = new AccesoDB();

		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append(
					"SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST, D.IG_NUMERO_DOCTO, D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,d.FN_TIPO_CAMBIO,'1'),'1') as TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
					"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,E.cg_razon_social as NOMBRE_EPO"   +
					" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"   +
					" 	,I.cg_razon_social as NOMBRE_IF"   +
					" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','F','Factoraje con Recurso') as TIPO_CREDITO"   +
					" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
					" 	,D.ig_plazo_credito"   +
					" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
					"  ,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
					" 	,DS.fn_valor_tasa as VALOR_TASA_INT"   +
					" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
					"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
					"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
					"	,LC.ic_moneda as MONEDA_LINEA"+
					"	, '24finandist/24if/24reporte01ext.jsp' as pantalla "+
					"FROM DIS_DOCUMENTO D"   +
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'F' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4 "   +
					"    union all"   +
					"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4 ) LC"   +
					"  ,COMCAT_EPO E"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,COMCAT_IF I"   +
					"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMCAT_TASA CT "   +
					"WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ?"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PE.IC_EPO = D.IC_EPO"   +
					"  AND I.IC_IF = LC.IC_IF"   +
					"  AND E.IC_EPO = D.IC_EPO"   +
					"  AND PP.IC_EPO = E.IC_EPO"   +
					"  AND PP.IC_PYME = PY.IC_PYME"   +
					"  AND PP.CG_TIPO_CREDITO = ? "  +
					"  AND PP.IC_PRODUCTO_NAFIN = ?"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
					"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
					"  AND CT.IC_TASA = DS.IC_TASA"   +
					"  AND D.IC_ESTATUS_DOCTO = ?"   +
					"  AND LC.IC_IF = ? "   +
					"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
					"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'F',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO "
					//+ "  AND DS.DF_FECHA_SELECCION >= TRUNC(SYSDATE)"  +
					//"  AND DS.DF_FECHA_SELECCION < TRUNC(SYSDATE+1)"
			);
			List params = new ArrayList();
			params.add(new Integer(4));
			if(!"".equals(tipoCredito)){
				params.add("F");
			}else{
				params.add("F");
			}
			params.add(new Integer(4));
			params.add(new Integer(3));
			params.add(new Long(clave_if));
			log.debug("qrySentencia= " + qrySentencia.toString());
			log.debug("params= " + params);
			Registros reg = con.consultarDB(qrySentencia.toString(), params);
			return reg;
		} catch(Exception e) {
			log.error("getLayoutFactorajeVencimiento(Exception) "+e);
			throw new AppException("Error al obtener layout factoraje vencimiento",e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info(" getLayoutFactorajeVencimiento");
		}
	}

	/**
	 *Fodea 019-2012
	 * Fa
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param negociable
	 * @param TipoCredito
	 * @param ic_tipo_financiamiento
	 * @param cs_cifras_control
	 * @param ic_pyme
	 * @param pipesporlinea
	 * @param vecCampos
	 * @param linea
	 * @param txtmtomaxdocdol
	 * @param txtmtomindocdol
	 * @param txtmtomaxdoc
	 * @param txtmtomindoc
	 * @param fn_monto
	 * @param ic_moneda
	 * @param ig_numero_docto
	 * @param df_fecha_emision
	 * @param df_fecha_venc
	 * @param ic_epo
	 */
public String validacionesMasivaF(
	String ic_epo,   String df_fecha_venc,
   String df_fecha_emision,  String ig_numero_docto,
   String ic_moneda,  String fn_monto,
   BigDecimal txtmtomindoc,   BigDecimal txtmtomaxdoc,
   BigDecimal txtmtomindocdol,  BigDecimal txtmtomaxdocdol,
   int linea,   Vector vecCampos,
   int pipesporlinea,  String ic_pyme,
   String cs_cifras_control, String ic_tipo_financiamiento ,  String TipoCredito,
   String negociable )   throws NafinException      {

	log.debug(":validacionesMasivaF (E)");
	boolean resultado = true;
	AccesoDB con = null;

	String NOnegociable ="", inhabilEpo ="",  fechaDeHoy = null,   msgError ="";

	BigDecimal monto = null;
   Vector vecColumnas = null;
   //double fn_monto2 =Double.parseDouble((String)fn_monto);
   int no_dia_semana = 0;

	BigDecimal monto_mn=new BigDecimal("0.0");
	BigDecimal monto_dolar=new BigDecimal("0.0");
	int totdoc_mn=0;
	int totdoc_dolar=0;


	try{
		con = new AccesoDB();
      con.conexionDB();

		if(ic_tipo_financiamiento.equals("")){
			msgError = "Error en la l�nea "+linea+", El valor de la Modalidad de plazo  no puede estar vacio";
			resultado =  false;
		}
		if(!ic_tipo_financiamiento.equals("NA")){
			msgError = "Error en la l�nea "+linea+", El valor de la Modalidad de plazo no es el correcto ";
			resultado =  false;
		}
		if(negociable.equals("NS")){
			msgError = "Error en la l�nea "+linea+", El valor de Negociable no puede estar vacio";
			resultado =  false;
		}

		if(!negociable.equals("NS") ){
			NOnegociable =  DesAutomaticoEpo(ic_epo,"PUB_EPO_NO_NEGOCIABLES");
			if( NOnegociable.equals("N") && negociable.equals("N")) {
				msgError = "Error en la l�nea "+linea+", La EPO no se encuentra parametrizada para publicar documentos No Negociables";
				resultado =  false;
        }
      }

      int moneda=0;
		if(!"".equals(ic_moneda)&&ic_moneda!=null) {
			if(Comunes.esNumero(ic_moneda)==false){
				throw new NafinException("GRAL0014");
			}else {
				moneda=Integer.parseInt(ic_moneda);
				if(fn_monto == null || "".equals(fn_monto))
					throw new NafinException("GRAL0011");
				else if(!Comunes.esDecimal(fn_monto))
					throw new NafinException("GRAL0012");
				else if(Double.parseDouble(fn_monto)<=0)
					throw new NafinException("GRAL0019");
					if((moneda != 1) && (moneda != 54))
						throw new NafinException("GRAL0015");
					else {
						monto = new BigDecimal(fn_monto);
						if(moneda == 1) {	//Si es Moneda Nacional
							if(monto.compareTo(txtmtomindoc) < 0 && "S".equals(cs_cifras_control)){
								msgError = "Error en la l&iacute;nea "+linea+", el Monto M&iacute;nimo de Entrada en M.N. "+txtmtomindoc.toPlainString()+" es Mayor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Mayor. \n";
								resultado =  false;
							}else if(monto.compareTo(txtmtomaxdoc) > 0 && "S".equals(cs_cifras_control)){
								msgError = "Error en la l&iacute;nea "+linea+", el Monto Maximo de Entrada en M.N. "+txtmtomaxdoc.toPlainString()+" es Menor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Menor. \n";
								resultado = false;
							}
							monto_mn = monto_mn.add(monto);
							totdoc_mn++;
						}
						if(moneda == 54) {	//Si son Dolares.
							if(monto.compareTo(txtmtomindocdol) < 0 && "S".equals(cs_cifras_control)){
								msgError = "Error en la l&iacute;nea "+linea+", el Monto M&iacute;nimo de Entrada en D&oacute;lares "+txtmtomindocdol.toPlainString()+" es Mayor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Mayor. \n";
								resultado = false;
							}else if(monto.compareTo(txtmtomaxdocdol) > 0 && "S".equals(cs_cifras_control)){
								msgError = "Error en la l&iacute;nea "+linea+", el Monto M&aacute;ximo de Entrada en D&oacute;lares "+txtmtomaxdocdol.toPlainString()+" es Menor al Monto del Archivo de Carga que es de: "+fn_monto+" cuando deber&iacute;a de ser Menor. \n";
								resultado = false;
							}
							monto_dolar = monto_dolar.add(monto);
							totdoc_dolar++;
						}
					}   //fin else
				} // fin else           (TIPO DE MONEDA)
			}
			else
			   throw new NafinException("GRAL0013");

          //VALIDAMOS QUE NO SE EXCEDA EL NUMERO DE CAMPOS PERMITIDOS
			 int numCamposAd =  vecCampos.size();
          for(int i=0;i<vecCampos.size();i++){
            if(!"".equals((String)vecCampos.get(i))&&(i>=numCamposAd)) {
					msgError = "El Campo Din�mico No."+(i+1)+" no est� definido por la EPO";
					resultado =false;
            }
          }
			 Vector vecCamposAd=  getCamposAdicionales(ic_epo,false);

          for(int i=0;i<vecCamposAd.size();i++){
				vecColumnas = (Vector)vecCamposAd.get(i);
            if("N".equals((String)vecColumnas.get(2))&&!"".equals((String)vecCampos.get(i))&&vecCampos.get(i)!=null) {
              	if(!Comunes.esNumero((String)vecCampos.get(i))) {
						msgError = "El Campo Din�mico No."+(i+1)+" debe de ser Num�rico";
						resultado = false;
					}
				}
				if(Integer.parseInt((String)vecColumnas.get(3)) < vecCampos.get(i).toString().length()) {
					msgError = "El Campo Din�mico No."+(i+1)+" excede la longitud definida por su epo";
				}
			}
         //Validamos que los campos definidos por la epo esten capturados
			pipesporlinea = pipesporlinea -6;
			if(numCamposAd > pipesporlinea)  {
          msgError = "Faltan Campos";
          resultado = false;
         }

         if("".equals(ig_numero_docto)||ig_numero_docto==null)
				throw new NafinException("ANTI0013");
         if("".equals(df_fecha_emision) || df_fecha_emision ==null)
				throw new NafinException("ANTI0014");
         if(!Comunes.checaFecha(df_fecha_emision))
				throw new NafinException("GRAL0010");
         if("".equals(df_fecha_venc) || df_fecha_venc ==null)
				throw new NafinException("ANTI0015");
         if(!Comunes.checaFecha(df_fecha_venc))
				throw new NafinException("GRAL0010");
         if(pipesporlinea<0)
				throw new NafinException("GRAL0016");
			if("".equals(ic_pyme) || ic_pyme==null)
				throw new NafinException("GRAL0017");

			/*Para Validar la fecha de Emision	Fodea 029-2010 Distribuidores Fase III	 */
			Calendar gcDiaFecha = new GregorianCalendar();
			java.util.Date fechaEmisionNvo = Comunes.parseDate(df_fecha_emision);
			gcDiaFecha.setTime(fechaEmisionNvo);
			no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
			inhabilEpo = getDiasInhabilesDes(ic_epo, df_fecha_emision);
			fechaDeHoy 	= Fecha.getFechaActual();
			log.debug("no_dia_semana   " +no_dia_semana+"   inhabilEpo  "+inhabilEpo);

			//SimpleDateFormat formatEntrada = new SimpleDateFormat("dd/MM/yyyy");
			//java.util.Date df_fecha_emision2 = formatEntrada.parse(df_fecha_emision);
			//java.util.Date  fechaDeHoy2 = Comunes.parseDate(fechaDeHoy);
			/*Para Validar la fecha de Emision		 */

		}catch(NafinException ne){
			msgError = ne.getMsgError();
         throw ne;
      }catch(Exception e){
			System.out.println("excepcion "+e);
         msgError = "El layout es incorrecto, por favor verif&iacute;quelo";
         throw new NafinException("SIST0001");
      } finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.debug(":validacionesMasivaF (S)");
		}
      return msgError;
    }


    private  String obtieneTipoCredito(String ic_epo) {
        log.info("CargaDocDistBean::obtieneTipoCredito(E)");
        String qrySentencia	= "";
        ResultSet	rs				= null;
        PreparedStatement   ps = null;
        AccesoDB 	con				= null;
        String valor = "";
        boolean transaccion = true;
        try {
            con = new AccesoDB();
            con.conexionDB();
            qrySentencia =
                " SELECT CG_TIPOS_CREDITO " +
                " FROM comrel_producto_epo   "+
                " WHERE ic_producto_nafin = ?   "+
                " AND ic_epo = ? ";
            log.debug("qrySentencia"  +qrySentencia);
            ps = con.queryPrecompilado(qrySentencia);
            log.debug("4:"  +4);
            ps.setInt(1, Integer.valueOf("4"));
            log.debug("ic_epo:"  +ic_epo);
            ps.setInt(2, Integer.valueOf(ic_epo));
            rs = ps.executeQuery();
            ps.clearParameters();
            if(rs.next()) {
                valor = ((rs.getString(1)==null)?"":rs.getString(1));
            }
            rs.close();ps.close();
            //con.cierraStatement();
        } catch(Exception e) {
            log.error("CargaDocDistBean::obtieneTipoCredito(Exception) "+e);
            transaccion = false;
            e.printStackTrace();
        } finally {
            try {
                if (rs!=null) rs.close();
                if (ps!=null) ps.close();
            } catch(Exception e) {
                log.error("CargaDocDistBean::obtieneTipoCredito(Exception)" + e.getMessage(), e);
            }
            if(con.hayConexionAbierta()) {
                con.terminaTransaccion(transaccion);
                con.cierraConexionDB();
            }
        }
        log.info("CargaDocDistBean::obtieneTipoCredito(S)");
        return valor;
    }

	private String generaFormatoCorreoWS(String nombreEpo, String codError,String estatusCarga, String resumen){
		String textoCorreo = "";
		try{

			String msgEstatusCarga = "";
			if("2".equals(estatusCarga)){
				msgEstatusCarga = "Carga parcial";
			}else if("2".equals(estatusCarga)){
				msgEstatusCarga = "Toda la Carga tuvo Error";
			}

			String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
				"font-size: 10px; "+
				"font-weight: bold;"+
				"color: #FFFFFF;"+
				"background-color: #4d6188;"+
				"padding-top: 3px;"+
				"padding-right: 1px;"+
				"padding-bottom: 1px;"+
				"padding-left: 3px;"+
				"height: 25px;"+
				"border: 1px solid #1a3c54;'";

			String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
				"color:#000066; "+
				"padding-top:1px; "+
				"padding-right:2px; "+
				"padding-bottom:1px; "+
				"padding-left:2px; "+
				"height:22px; "+
				"font-size:11px;'";

			String tabla1 = "<table border=\"1\">";
			tabla1 += "	<tr>" +
					 "		<td align=\"center\" "+styleEncabezados+">Resumen</td>" +
					 "		<td align=\"center\" "+styleEncabezados+">Valor</td>" +
					 "		<td align=\"center\" "+styleEncabezados+">Descripcion</td>" +
					 "	</tr>";

			tabla1 += "	<tr>" +
				"		<td align=\"center\" "+style+">Codigo de Error</td>" +
				"		<td align=\"center\" "+style+">" + codError + "</td>" +
				"		<td align=\"center\" "+style+">Error en el Proceso</td>" +
				"	</tr>";
			tabla1 += "	<tr>" +
				"		<td align=\"center\" "+style+">Estatus Carga de Documentos</td>" +
				"		<td align=\"center\" "+style+">" + estatusCarga + "</td>" +
				"		<td align=\"center\" "+style+">"+ msgEstatusCarga+"</td>" +
				"	</tr>";
			tabla1 += "	<tr>" +
				"		<td align=\"center\" "+style+">Resumen de Ejecucion</td>" +
				"		<td align=\"center\" "+style+">Error Inesperado</td>" +
				"		<td align=\"center\" "+style+">"+resumen+"</td>" +
				"	</tr>";
			tabla1 += "</table><br><br>";

			textoCorreo = "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'><p> Nombre EPO: "+nombreEpo+"<br><br></p>"+tabla1+"</form>";

		}catch(Throwable t){
			log.error("Error al enviar correo " + t.getMessage());
		}

		return textoCorreo;

	}

	/**
	 * Realiza la carga de documentos de distribuidores.
	 * El proceso en la primera etapa, recibe el archivo (cadena) y
	 * verifica layout y que no haya lineas duplicadas.
	 * La segunda etapa es la carga en N@E pero �nicamente de aquellos
	 * documentos que cumplan con las reglas de negocio establecidas por Nafin
	 * (Se emplea un enlace)
	 * @param claveEPO Clave de la EPO que envia los documentos
	 * @param usuario Clave del usuario en N@E
	 * @param password Password del usuario en N@E
	 * @param cadenaArchivo Cadena con el contenido del archivo
	 * @param pkcs7 Cadena con el contenido del documento firmado
	 * @param String serial Serial del certificado.
	 *
	 * @return CargaDocDistribuidoresWSInfo que contiene informaci�n acerca de la
	 * 		ejecuci�n del proceso y de la carga de documentos
	 *		Para el estatus del proceso:
	 * 			1.-Sin errores
	 * 			2.-Error en el proceso
	 *		Para el estatus de la carga:
	 *			1.-Sin errores.
	 * 			2.-Carga con errores
	 * 			3.-Sin registros por procesar
	 */

	public CargaDocDistribuidoresWSInfo procesarDocDistribuidoresWS(
			String claveEPO, String usuario, String password,
			String cadenaArchivo, String pkcs7, String serial) {

		log.info("CargaDocDistBean::procesarDocDistribuidoresWS(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps =  null;
		ResultSet rs = null;
		boolean error = false;
		boolean errorCarga = false;
		String receipt = null;
		//String tituloMensaje = "Ejecucion procesarDocDistribuidoresWS";
		StringBuffer resumenEjecucion = null;
		String tablaResHtml = "";
		String txtCorreoHtml = "";
		String nombreEpo = "";
		String strSQL = "";
		int estatusCarga = 3;
		SimpleDateFormat formatoHora = new SimpleDateFormat ("yyyyMMdd_HHmmss");
		CargaDocDistribuidoresWSInfo info = new CargaDocDistribuidoresWSInfo();
		Correo correo = new Correo();

		String csCaracterEspecial = "N";
	   String  acuse ="";
		
		try {
			con.conexionDB();
			if (claveEPO == null || claveEPO.equals("")) {
				error = true;
				info.setCodigoEjecucion(new Integer(2)); //Error del proceso
				info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
				info.setResumenEjecucion("El par�metro -Clave de EPO- debe ser especificado");
				txtCorreoHtml = this.generaFormatoCorreoWS("Clave EPO incorrecta", String.valueOf(info.getCodigoEjecucion()), String.valueOf(info.getEstatusCarga()), info.getResumenEjecucion());

			}
			if (claveEPO != null && !claveEPO.equals("")) {
				Integer.parseInt(claveEPO);

				ps = con.queryPrecompilado("select cg_razon_social from comcat_epo where ic_epo = ? ");
				ps.setLong(1,Long.parseLong(claveEPO));
				rs = ps.executeQuery();
				if(rs.next()){
					nombreEpo = rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social");
				}
				rs.close();
				ps.close();
			}

			UtilUsr utilUsr = new UtilUsr();
			List cuentasEPO = utilUsr.getUsuariosxAfiliado(claveEPO, "E");



			log.info("procesarDocDistribuidoresWS::Validando claveEPO/usuario");
			if (!error && !cuentasEPO.contains(usuario)) {
				error = true;
				info.setCodigoEjecucion(new Integer(2)); //Error del proceso
				info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
				info.setResumenEjecucion("El usuario no existe para la EPO especificada");
				txtCorreoHtml = this.generaFormatoCorreoWS(nombreEpo, String.valueOf(info.getCodigoEjecucion()), String.valueOf(info.getEstatusCarga()), info.getResumenEjecucion());
			}

			log.info("procesarDocDistribuidoresWS::Validando usuario/passwd");
			if (!error && !utilUsr.esUsuarioValido(usuario, password)) {
				error = true;
				info.setCodigoEjecucion(new Integer(2)); //Error del proceso
				info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
				info.setResumenEjecucion("Fallo la autenticaci�n del usuario");
				txtCorreoHtml = this.generaFormatoCorreoWS(nombreEpo, String.valueOf(info.getCodigoEjecucion()), String.valueOf(info.getEstatusCarga()), info.getResumenEjecucion());
			}

			if (!error) {
				log.info("procesarDocDistribuidoresWS::Validando que el certificado sea del usuario especificado");
				String qryTraeSerial =
						" SELECT dn_user " +
						" FROM users_seguridata "+
						" WHERE UPPER(trim(table_uid)) = UPPER(?) ";
				PreparedStatement psSerial = con.queryPrecompilado(qryTraeSerial);
				psSerial.setString(1, usuario);
				ResultSet rsSerial = psSerial.executeQuery();
				String strSerial = "";
				if(rsSerial.next()) {
					strSerial = rsSerial.getString(1).trim();
				}
				rsSerial.close();
				psSerial.close();

				if (!serial.equals(strSerial)) {
					error = true;
					info.setCodigoEjecucion(new Integer(2)); //Error del proceso
					info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
					info.setResumenEjecucion("El certificado no corresponde al usuario");
					txtCorreoHtml = this.generaFormatoCorreoWS(nombreEpo, String.valueOf(info.getCodigoEjecucion()), String.valueOf(info.getEstatusCarga()), info.getResumenEjecucion());
				}
			}


			if (!error) {

				log.info("procesarDocDistribuidoresWS::Validando firma mensaje");
				netropology.utilerias.Seguridad s = new netropology.utilerias.Seguridad();
				char getReceipt = 'Y';
				String folio = claveEPO + "WS" + String.valueOf(new java.util.Date().getTime());

				boolean autenticarMensaje =
						s.autenticar(folio, serial, pkcs7,
								cadenaArchivo, getReceipt);

				if (autenticarMensaje) {
					receipt = s.getAcuse();
				} else {
					log.debug(s.mostrarError());
					error = true;
					info.setCodigoEjecucion(new Integer(2)); //Error del proceso
					info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
					info.setResumenEjecucion("Fallo la autenticaci�n del mensaje");
					txtCorreoHtml = this.generaFormatoCorreoWS(nombreEpo, String.valueOf(info.getCodigoEjecucion()), String.valueOf(info.getEstatusCarga()), info.getResumenEjecucion());
				}
			}



			errorCarga = false;
			resumenEjecucion = new StringBuffer();

			if (!error) {
				log.info("procesarDocDistribuidoresWS::Inicia proceso de archivo");
				List registros = new VectorTokenizer(cadenaArchivo, "\n").getValuesVector();

				//ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class);

				//Variables necesarias para el proceso
				Vector vecFilas 	= null;
				Vector vecdat=null;
				String ln	=	"",  cg_num_distribuidor	=	"",  numero_docto	=	"", fecha_emision	=	"", fecha_venc =	"",
					ic_moneda	=	"", fn_monto	=	"", ic_tipo_financiamiento =	"", categoria	=	"",
					ig_plazo_descuento	=	"", fn_porc_descuento	=	"", ic_linea_credito_dm	=	"", fecha_lim_dscto		=	"",
					ct_referencia		=	"", campo_ad1="", campo_ad2="", campo_ad3="", campo_ad4="",
					campo_ad5="",  nombre_pyme = "", NOnegociable = "" , 
					arma_error="", tipoPago ="", periodo ="";

				boolean ok=true, ok_fechas=true;
				int pipesporlinea=0,lineadocs=1;
				Integer.parseInt(this.getClaveDoctoTmp());
				VectorTokenizer vtd = null;
				Vector vecdet = null;
				String linea=	"";

				for(int i = 0; i < registros.size(); i++){
					String registroEnTurno = (String)registros.get(i);
					if (registroEnTurno.trim().equals("")) {
						//Linea vacia... Ignorar
						continue;
					}
					
					if(Comunes.tieneCaracterControlNoPermitidoEnXML1(registroEnTurno)){
						csCaracterEspecial = "S";
					}
					
					//List camposRegistroEnTurno =
							//new VectorTokenizer(registroEnTurno, "|").getValuesVector();
					vecdat=new VectorTokenizer(registroEnTurno, "|").getValuesVector();

					vtd		= new VectorTokenizer(linea,"|");
					vecdet	= vtd.getValuesVector();
					arma_error = "";
					ok=true;
					ok_fechas=true;
					pipesporlinea=0; linea=linea.trim();
					for(int n=0; n<linea.length(); n++) {
							ln=linea.substring(n,n+1);
					  if(ln.equals("|"))
								pipesporlinea++;
					  }

					/*if(camposRegistroEnTurno.size() < 7) {
						errorCarga = true; //con errores
						resumenEjecucion.append("Linea " + (i + 1) + ":" +
								"El registro no contiene el numero minimo de campos" + "\n");
						continue;
					}*/

					try {
						cg_num_distribuidor 	= (vecdat.size()>=1)?(String)vecdat.get(0):"";
						numero_docto 			= (vecdat.size()>=2)?(String)vecdat.get(1):"";
						fecha_emision 			= (vecdat.size()>=3)?(String)vecdat.get(2):"";
						fecha_venc 				= (vecdat.size()>=4)?(String)vecdat.get(3):"";
						ic_moneda 				= (vecdat.size()>=5)?(String)vecdat.get(4):"";
						fn_monto 				= (vecdat.size()>=6)?(String)vecdat.get(5):"";
						ic_tipo_financiamiento	= (vecdat.size()>=7)?(String)vecdat.get(6):"";
						categoria				= (vecdat.size()>=8)?(String)vecdat.get(7):"";
						if("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento)) {
							ig_plazo_descuento	= (vecdat.size()>=9)?(String)vecdat.get(8):"";
							fecha_lim_dscto		= (vecdat.size()>=10)?(String)vecdat.get(9):"";
							fn_porc_descuento	= (vecdat.size()>=11)?(String)vecdat.get(10):"";
                        } else {
							ic_linea_credito_dm = (vecdat.size()>=12)?(String)vecdat.get(11):"";
                        }
						ct_referencia			= (vecdat.size()>=13)?(String)vecdat.get(12):"";
						campo_ad1 				= (vecdat.size()>=14)?(String)vecdat.get(13):"";
						campo_ad2 				= (vecdat.size()>=15)?(String)vecdat.get(14):"";
						campo_ad3 				= (vecdat.size()>=16)?(String)vecdat.get(15):"";
						campo_ad4 				= (vecdat.size()>=17)?(String)vecdat.get(16):"";
						campo_ad5 				= (vecdat.size()>=18)?(String)vecdat.get(17):"";
						NOnegociable 			= (vecdat.size()>=19)?(String)vecdat.get(18):"";
                        tipoPago                = (vecdat.size()>=20)?(String)vecdat.get(19):""; //F09-2015
                        periodo                 = (vecdat.size()>=21)?(String)vecdat.get(20):""; //F09-2015
                        
                        vecdat.removeAllElements();
                        
						cg_num_distribuidor     = Comunes.quitaComitasSimples(cg_num_distribuidor);
						numero_docto            = Comunes.quitaComitasSimples(numero_docto);
						fecha_emision           = Comunes.quitaComitasSimples(fecha_emision);
						fecha_venc              = Comunes.quitaComitasSimples(fecha_venc);
					    ic_moneda               = Comunes.quitaComitasSimples(ic_moneda);
					    fn_monto                = Comunes.quitaComitasSimples(fn_monto.trim());
						ic_tipo_financiamiento  = Comunes.quitaComitasSimples(ic_tipo_financiamiento);
					    categoria               = Comunes.quitaComitasSimples(categoria);
					    ig_plazo_descuento      = Comunes.quitaComitasSimples(ig_plazo_descuento);
					    fecha_lim_dscto         = Comunes.quitaComitasSimples(fecha_lim_dscto);
					    fn_porc_descuento       = Comunes.quitaComitasSimples(fn_porc_descuento);
						ic_linea_credito_dm     = Comunes.quitaComitasSimples(ic_linea_credito_dm);
						ct_referencia           = Comunes.quitaComitasSimples(ct_referencia);
						campo_ad1               = Comunes.quitaComitasSimples(campo_ad1.trim());
						campo_ad2               = Comunes.quitaComitasSimples(campo_ad2.trim());
						campo_ad3               = Comunes.quitaComitasSimples(campo_ad3.trim());
						campo_ad4               = Comunes.quitaComitasSimples(campo_ad4.trim());
						campo_ad5               = Comunes.quitaComitasSimples(campo_ad5.trim());
					    NOnegociable            = Comunes.quitaComitasSimples(NOnegociable.trim());
					    tipoPago                = Comunes.quitaComitasSimples(tipoPago.trim());
					    periodo                 = Comunes.quitaComitasSimples(periodo.trim());
                        
						log.trace(" Numero de Distribuidor:"+cg_num_distribuidor);
						log.trace(" No de Documento:"+numero_docto);
						log.trace(" Fecha de Emisi�n del docto:"+fecha_emision);
						log.trace(" Fecha de Vencimiento del docto:"+fecha_venc);
						log.trace(" Moneda:"+ic_moneda);
						log.trace(" Monto del docto:"+fn_monto);
						log.trace(" Tipo Financiamiento:"+ic_tipo_financiamiento);
						log.trace(" categoria:"+categoria);
						log.trace(" Plazo para Descuento:"+ig_plazo_descuento);
						log.trace(" Fecha de L�mite para Descuento:"+fecha_lim_dscto);
						log.trace(" % del Descuento:"+fn_porc_descuento);
						log.trace(" Linea de Cr�dito:"+ic_linea_credito_dm);
						log.trace(" Referencia o Comentario:"+ct_referencia);
						log.trace(" Campo Adicional 1:"+campo_ad1);
						log.trace(" Campo Adicional 2:"+campo_ad2);
						log.trace(" Campo Adicional 3:"+campo_ad3);
						log.trace(" Campo Adicional 4:"+campo_ad4);
						log.trace(" Campo Adicional 5:"+campo_ad5);
						log.trace(" negociable: "+NOnegociable);
                        log.trace(" tipoPago: "+tipoPago);
						log.trace(" periodo: "+periodo);
                        
						nombre_pyme = "";
						vecFilas = new Vector();
						vecFilas.addElement(campo_ad1);
						vecFilas.addElement(campo_ad2);
						vecFilas.addElement(campo_ad3);
						vecFilas.addElement(campo_ad4);
						vecFilas.addElement(campo_ad5);
					} catch(ArrayIndexOutOfBoundsException aioobe) {
						arma_error = "El layout es incorrecto";
						//ok = false;
					} catch(Exception e){
						arma_error = "El layout es incorrecto";
						//ok = false;
					}


					//if (NOnegociable.equalsIgnoreCase("") ) {
					//	NOnegociable="NS";
					//}

					try {

						//INSERCION EN TABLA TEMPORAL

						/*int ic_consecutivo = 0;
						strSQL = "select max(nvl(ic_consecutivo,0))+1"+
								" from distmp_proc_docto_ws" +
								" where ic_proc_docto = "+ic_proc_docto;

						ps = con.queryPrecompilado(strSQL);
						rs = ps.executeQuery();
						if(rs.next()){
							ic_consecutivo = rs.getInt(1);
						}
						rs.close();
						ps.close();
                */

						strSQL =
						   "INSERT INTO distmp_proc_docto_ws " +
						   "            (ic_epo, cg_num_distribuidor, ig_numero_docto, df_fecha_emision, " +
						   "             df_fecha_venc, ic_moneda, fn_monto, ic_tipo_financiamiento, " +
						   "             ic_clasificacion, ig_plazo_descuento, df_fecha_venc_credito, " +
						   "             fn_porc_descuento, ic_linea_credito_dm, ct_referencia, " +
						   "             cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5, " +
						   "             cs_negociable, cg_receipt, cs_caracter_especial, ig_tipo_pago ,  ig_meses_interes " +
						   "            ) " +
						   "     VALUES (?, ?, ?, ?, " +
						   "             ?, ?, ?, ?, " +
						   "             ?, ?, ?, " +
						   "             ?, ?, ?, " +
						   "             ?, ?, ?, ?, ?, " +
						   "             ?, ?, ?,  " +
						   "             ?, ?  " +
						   "            ) ";
                        
						int p=0;

					    log.debug(" strSQL: "+strSQL);
                        
						ps = con.queryPrecompilado(strSQL);
                        
						ps.setLong(++p, Long.parseLong(claveEPO));
						ps.setString(++p, cg_num_distribuidor);
						ps.setString(++p, numero_docto);
						if (fecha_emision.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, fecha_emision);
						}
						if (fecha_venc.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, fecha_venc);
						}
						if (ic_moneda.equals("")) {
							ps.setNull(++p, java.sql.Types.NUMERIC);
						} else {
						    ps.setLong(++p, Long.parseLong(ic_moneda));
						}
						if (fn_monto.equals("")) {
							ps.setNull(++p, java.sql.Types.NUMERIC);
						} else {
						    ps.setDouble(++p, Double.parseDouble(fn_monto));
						}
						if (ic_tipo_financiamiento.equals("")) {
							ps.setNull(++p, java.sql.Types.NUMERIC);
						} else {
							ps.setString(++p, ic_tipo_financiamiento);
						}
						if (categoria.equals("")) {
							ps.setNull(++p, java.sql.Types.NUMERIC);
						} else {
							ps.setLong(++p, Long.parseLong(categoria));
						}
						if (ig_plazo_descuento.equals("")) {
							ps.setNull(++p, java.sql.Types.NUMERIC);
						} else {
							ps.setLong(++p, Long.parseLong(ig_plazo_descuento));
						}
						if (fecha_lim_dscto.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, fecha_lim_dscto);
						}
						if (fn_porc_descuento.equals("")) {
							ps.setNull(++p, java.sql.Types.NUMERIC);
						} else {
							ps.setDouble(++p, Double.parseDouble(fn_porc_descuento));
						}
						if (ic_linea_credito_dm.equals("")) {
							ps.setNull(++p, java.sql.Types.NUMERIC);
						} else {
							ps.setLong(++p, Long.parseLong(ic_linea_credito_dm));
						}
						if (ct_referencia.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, ct_referencia);
						}
						if (campo_ad1.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, campo_ad1);
						}
						if (campo_ad2.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, campo_ad2);
						}
						if (campo_ad3.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, campo_ad3);
						}
						if (campo_ad4.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, campo_ad4);
						}
						if (campo_ad5.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, campo_ad5);
						}
						if (NOnegociable.equals("")) {
							ps.setNull(++p, java.sql.Types.VARCHAR);
						} else {
							ps.setString(++p, NOnegociable);
						}
						ps.setString(++p, receipt);
						ps.setString(++p, csCaracterEspecial);
					   // F09-2015
						if (tipoPago.equals("")) {
							ps.setNull(++p, java.sql.Types.NUMERIC);
						} else {
						    ps.setLong(++p, Long.parseLong(tipoPago));
						}					   
						if (periodo.equals("")) {
							ps.setNull(++p, java.sql.Types.NUMERIC);
						} else {
						    ps.setLong(++p, Long.parseLong(periodo));
						}
                        
						ps.executeUpdate();
						ps.close();
                        
						//FINALIZA INSERCION tmp
                        
					} catch(Exception e){
						e.printStackTrace();
						String fechaError = formatoHora.format(new java.util.Date());

						log.error(fechaError + "::" + e.getMessage());
						//sqle.printStackTrace();
						//Si no se puede insertar en la tabla
						error = true; //con errores
						resumenEjecucion.append("Linea " + (i + 1) + ":" +
								"Verifique layout del registro y asegurese " +
								"que el numero de documento no se repita." +
								" (" + fechaError + ")" + "\n");
					}

					lineadocs++;
			}//fin for

			//Si alguno de los registros del archivo no se pudo insertar en
			//la tabla temporal de publicaci�n de documentos, entonces se
			//realiza un rollback sobre toda la operaci�n.
			if (error) {
				con.terminaTransaccion(false);
			} else {
				con.terminaTransaccion(true);
			}

			log.info("...");
			log.info("...");
			log.info("...");
			log.info("procesarDocDistribuidoresWS::Finaliza proceso de archivo");

				if (!error) {

					log.info("...");
					log.info("...");
					log.info("...");
					log.info("procesarDocDistribuidoresWS::Inicia proceso de carga de documentos");



					/*

					int moneda=0;
					//fodea 029-2010
					if (NOnegociable.equalsIgnoreCase("S") ) {
						estatusNegociable = "2";
					}else  if (NOnegociable.equalsIgnoreCase("N")) {
						estatusNegociable = "1";
					}


					moneda=Integer.parseInt(ic_moneda);
					String	descuentoAutomatico =BeanParametros.DesAutomaticoEpo(claveEPO,"PUB_EPO_DESC_AUTOMATICO" );
					String limiteLineaCredito =BeanParametros.DesAutomaticoEpo(claveEPO,"PUB_LIMITE_LINEA_CREDITO");//Fodea 029-2010 Distribuodores Fase III
					String TipoCredito =  BeanParametros.obtieneTipoCredito (claveEPO); //Fodea 029-2010 Distribuodores Fase III
					String ventaCartera =BeanParametros.DesAutomaticoEpo(claveEPO,"PUB_EPO_VENTA_CARTERA"); //Fodea 029-2010 Distribuodores Fase III

					if(ventaCartera.equals("S")) {
						operaVentaCartera = "S";
					}

					String lineaCredito = this.validaMontolineaCredito(claveEPO); //Fodea 029-2010 Distribuidores Fase III
					String hid_fecha_porc_desc = this.getParamFechaPorc(claveEPO);

					if(!this.tempMasivaWS(receipt, fecha_venc,fecha_emision,claveEPO,ic_proc_docto+"",numero_docto,cg_num_distribuidor,ic_moneda,fn_monto,ct_referencia,vecFilas,ic_tipo_financiamiento,ig_plazo_descuento,fn_porc_descuento,ic_linea_credito_dm,fecha_lim_dscto,hid_fecha_porc_desc,categoria,estatusNegociable,operaVentaCartera,con)){
						arma_error = this.getMsgError();
						ok = false;
					}
					*/

					//this.validacionesMasivaWS( new WSEnlace(claveEPO, receipt, usuario));

				   this.procesoCargaDoctosWS( new WSEnlace(claveEPO, receipt, usuario));  // AQUI HACE LAS VALIDACIONES  DE LA CARGA 


					String strSQLAcuse =
							" SELECT cc_acuse, in_total_proc, " +
							" 	fn_total_monto_mn, in_total_acep_mn, in_total_rech_mn, " +
							" 	fn_total_monto_dl, in_total_acep_dl, in_total_rech_dl, " +
							" 	df_fechahora_carga, cs_estatus " +
							" FROM dis_doctos_pub_acu_ws " +
							" WHERE ic_epo = ? " +
							" 	AND cg_receipt = ? ";
					PreparedStatement psAcuse = con.queryPrecompilado(strSQLAcuse);

					psAcuse.setInt(1, Integer.parseInt(claveEPO));
					psAcuse.setString(2, receipt);
					ResultSet rsAcuse = psAcuse.executeQuery();
					StringBuffer sbAcuse = new StringBuffer();

					String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
						"font-size: 10px; "+
						"font-weight: bold;"+
						"color: #FFFFFF;"+
						"background-color: #4d6188;"+
						"padding-top: 3px;"+
						"padding-right: 1px;"+
						"padding-bottom: 1px;"+
						"padding-left: 3px;"+
						"height: 25px;"+
						"border: 1px solid #1a3c54;'";

					String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
						"color:#000066; "+
						"padding-top:1px; "+
						"padding-right:2px; "+
						"padding-bottom:1px; "+
						"padding-left:2px; "+
						"height:22px; "+
						"font-size:11px;'";

					while (rsAcuse.next()) {
						acuse = rsAcuse.getString("cc_acuse");
						String totalProcesados = rsAcuse.getString("in_total_proc");
						String totalMontoMN = rsAcuse.getString("fn_total_monto_mn");
						String totalAceptadosMN = rsAcuse.getString("in_total_acep_mn");
						String totalRechazadosMN = rsAcuse.getString("in_total_rech_mn");
						String totalMontoDL = rsAcuse.getString("fn_total_monto_dl");
						String totalAceptadosDL = rsAcuse.getString("in_total_acep_dl");
						String totalRechazadosDL = rsAcuse.getString("in_total_rech_dl");
						String fechaHoraCarga = rsAcuse.getString("df_fechahora_carga");
						estatusCarga = Integer.parseInt(rsAcuse.getString("cs_estatus"));


						sbAcuse.append(
								"Acuse," + acuse + "\n" +
								"Numero Total de Documentos Procesados," + totalProcesados + "\n" +
								"Nombre de la EPO," + acuse + "\n" +
								"Monto Total M.N. Documentos Aceptados," + totalMontoMN + "\n" +
								"Numero Total de Documentos Aceptados M.N.," + totalAceptadosMN + "\n" +
								"Numero Total de Documentos Rechazados M.N.," + totalRechazadosMN + "\n" +
								"Monto Total DL Documentos Aceptados," + totalMontoDL + "\n" +
								"Numero Total de Documentos Aceptados DL," + totalAceptadosDL + "\n" +
								"Numero Total de Documentos Rechazados DL," + totalRechazadosDL + "\n" +
								"Fecha y hora de carga," + fechaHoraCarga + "\n" );

						txtCorreoHtml += "";
						tablaResHtml += "<table border=\"1\">";
						tablaResHtml += "	<tr>" +
								 "		<td align=\"center\" "+styleEncabezados+">Detalle</td>" +
								 "		<td align=\"center\" "+styleEncabezados+">Acuse: "+acuse+"</td>" +
								 "	</tr>";

						tablaResHtml += "	<tr>" +
							"		<td align=\"left\" "+style+">Numero Total de Documentos Procesados</td>" +
							"		<td align=\"left\" "+style+">"+totalProcesados+"</td>" +
							"	</tr> " +
							"	<tr>" +
							"		<td align=\"left\" "+style+">Nombre de la EPO</td>" +
							"		<td align=\"left\" "+style+">"+nombreEpo+"</td>" +
							"	</tr> " +
							"	<tr>" +
							"		<td align=\"left\" "+style+">Monto Total M.N. Documentos Aceptados</td>" +
							"		<td align=\"left\" "+style+">"+totalMontoMN+"</td>" +
							"	</tr> " +
							"	<tr>" +
							"		<td align=\"left\" "+style+">Numero Total de Documentos Aceptados M.N.</td>" +
							"		<td align=\"left\" "+style+">"+totalAceptadosMN+"</td>" +
							"	</tr> " +
							"	<tr>" +
							"		<td align=\"center\" "+style+">Numero Total de Documentos Rechazados M.N.</td>" +
							"		<td align=\"left\" "+style+">"+totalRechazadosMN+"</td>" +
							"	</tr> " +

							"	<tr>" +
							"		<td align=\"left\" "+style+">Monto Total DL Documentos Aceptados</td>" +
							"		<td align=\"left\" "+style+">"+totalMontoDL+"</td>" +
							"	</tr> " +
							"	<tr>" +
							"		<td align=\"left\" "+style+">Numero Total de Documentos Aceptados DL</td>" +
							"		<td align=\"left\" "+style+">"+totalAceptadosDL+"</td>" +
							"	</tr> " +
							"	<tr>" +
							"		<td align=\"left\" "+style+">Numero Total de Documentos Rechazados DL</td>" +
							"		<td align=\"left\" "+style+">"+totalRechazadosDL+"</td>" +
							"	</tr> " +
							"	<tr>" +
							"		<td align=\"left\" "+style+">Fecha y hora de carga</td>" +
							"		<td align=\"left\" "+style+">"+fechaHoraCarga+"</td>" +
							"	</tr> " ;
						tablaResHtml += "</table><br><br>";


					}
					rsAcuse.close();
					psAcuse.close();
                    
					if (sbAcuse.length() > 0 ) {
						resumenEjecucion.append(sbAcuse + "\n\n\n");
					}
                    
					String strSQLError =
							" SELECT ig_numero_docto, ig_numero_error, " +
							" cg_error " +
							" FROM dis_doctos_err_pub_ws " +
							" WHERE ic_epo = ? " +
							" 	AND cg_receipt = ? ";
					PreparedStatement psError = con.queryPrecompilado(strSQLError);                    
					log.trace("strSQLError =======  "+ strSQLError);
					log.trace("claveEPO oOOOOo =======  "+ claveEPO);
					log.trace("receipt oOOOOo =======  "+ receipt);
                    
					psError.setInt(1, Integer.parseInt(claveEPO));
					psError.setString(2, receipt);
					ResultSet rsError = psError.executeQuery();
					StringBuffer sbError = new StringBuffer();
          //String digitoIdentificador = "";
					log.trace("Llega aqui ...  ");
					int ierror  = 0;
					while (rsError.next()) {
						String numDocto = rsError.getString("ig_numero_docto");
						String codigoError = rsError.getString("ig_numero_error");
						String errorDocto = rsError.getString("cg_error");

						errorCarga = true; //con errores
						sbError.append(numDocto + ", " +
								codigoError + ", " +
								errorDocto.replace(',',' ') +
						"\n");

						if(ierror==0){
							tablaResHtml += "Errores Carga de Documentos<br><table border=\"1\">";
							tablaResHtml += "	<tr>" +
									 "		<td align=\"center\" "+styleEncabezados+">Numero de Documento</td>" +
									 "		<td align=\"center\" "+styleEncabezados+">Codigo de Error</td>" +
									 "		<td align=\"center\" "+styleEncabezados+">Descripcion de Error</td>" +
									 "	</tr>";
						}

						tablaResHtml += "	<tr>" +
							"		<td align=\"center\" "+style+">"+numDocto+"</td>" +
							"		<td align=\"center\" "+style+">"+codigoError+"</td>" +
							"		<td align=\"center\" "+style+">"+errorDocto+"</td>" +
							"	</tr>";

						ierror++;
					}
					rsError.close();
					psError.close();

					if(ierror>0){
						tablaResHtml += "</table><br><br>";
					}

					if (sbError.length() > 0 ) {
						resumenEjecucion.append(
								"\nNumero de Documento, Codigo Error, " +
								"Descripcion Error" +
                "\n" +
								sbError + "\n\n\n");
					}


					log.info("...");
					log.info("...");
					log.info("...");
					log.info("procesarDocDistribuidoresWS::Finaliza proceso de carga de documentos");
				}

				log.debug("resumenEjecucion.toString()======== "+resumenEjecucion.toString());
				info.setCodigoEjecucion((!error)?new Integer(1):new Integer(2));
				info.setEstatusCarga(new Integer(estatusCarga));
				info.setResumenEjecucion(resumenEjecucion.toString());

				txtCorreoHtml = "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'><p> Nombre EPO: "+nombreEpo+"<br><br>Codigo Ejecucion: "+String.valueOf(info.getCodigoEjecucion())+"<br>"+
									"Estatus Carga de Documento: "+String.valueOf(info.getEstatusCarga())+"<br>Resumen de ejecucion</p><br>"+tablaResHtml+"</form>";
			
			
			   if (!error)  {
					//Crea y Guarda el XML de respuesta 
			      CrearArchivosAcuses  arc = new CrearArchivosAcuses();
			      arc.setAcuse(acuse );
			      arc.setUsuario(usuario );
			      arc.setTipoUsuario("EPO");
			      arc.setVersion("WEBSERVICE"); 
			                     
			      try {
						arc.setCodigoEjecucion( String.valueOf(info.getCodigoEjecucion())  );
						arc.setEstatusCarga(  String.valueOf(estatusCarga)   );
			         arc.setResumenEjecucion( resumenEjecucion.toString()    );
			         arc.guardarArchivo(); 
					} catch (Exception exception) {
						java.io.StringWriter outSW = new java.io.StringWriter();
			         exception.printStackTrace(new java.io.PrintWriter(outSW));
			         String stackTrace = outSW.toString();
			         arc.setDesError(stackTrace);  
			         arc.guardaBitacoraArch();  
						throw new NafinException("SIST0001");
			      } 
				}
			
			}

		} catch(Exception e) {
			info.setCodigoEjecucion(new Integer(2)); //Error del proceso
			info.setEstatusCarga(new Integer(3)); //No se realiz� la carga por error en el proceso
			String fechaError = formatoHora.format(new java.util.Date());
			log.error("CargaDocDistBean::procesarDocDistribuidoresWS()::" +
					"ERROR INESPERADO " + fechaError);
			info.setResumenEjecucion("ERROR INESPERADO " + fechaError);
			txtCorreoHtml = this.generaFormatoCorreoWS(nombreEpo, String.valueOf(info.getCodigoEjecucion()), String.valueOf(info.getEstatusCarga()), info.getResumenEjecucion());
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}


		try { //Si el env�o del correo falla se ignora.
			String to = this.getCuentasWS(claveEPO);
			if (to != null) {
				//String from = this.getCuentaWSFrom();

				correo.enviarTextoHTML("No_response@nafin.gob.mx", to, "Ejecuci�n ProcesarDocumentosWS", txtCorreoHtml);
			}
		} catch(Throwable t) {
			log.error("Error al enviar correo " + t.getMessage());
		}

		log.info("CargaDocDistBean::procesarDocDistribuidoresWS(S)");
		return info;
	}

	public boolean validaPymeXEpoBloqueda(String cveEpo, String cvePyme, String cveProducto) throws AppException{
		log.info("CargaDocumentoBean::validaPymeXEpoBloqueda(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		boolean sibloqueda = false;
		try{
			con.conexionDB();

			strSQL = "SELECT cs_bloqueo " +
						"  FROM comrel_bloqueo_pyme_x_epo " +
						" WHERE ic_pyme = ? AND ic_epo = ? AND ic_producto_nafin = ? ";

			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1,Long.parseLong(cvePyme));
			ps.setLong(2,Long.parseLong(cveEpo));
			ps.setLong(3,Long.parseLong(cveProducto));

			rs = ps.executeQuery();
			if(rs.next()){
				if("B".equals(rs.getString("cs_bloqueo")))
					sibloqueda = true;
			}
			rs.close();
			ps.close();

			return sibloqueda;
		}catch(Throwable t){
			throw new AppException("Error al consultar si la pyme esta bloqueda para la epo", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("CargaDocumentoBean::validaPymeXEpoBloqueda(S)");
		}
	}

	public boolean validaPymeXEpoBloqXNumProv(String numDist, String cveEpo, String cveProducto) throws AppException{
		log.info("CargaDocumentoBean::validaPymeXEpoBloqXNumProv(E)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		boolean sibloqueda = false;
		try{
			con.conexionDB();
			String cvePyme = "";

			strSQL = "SELECT ic_epo, ic_pyme " +
						"  FROM comrel_pyme_epo " +
						" WHERE CG_NUM_DISTRIBUIDOR = ? " +
						" AND ic_epo = ? ";


			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, numDist);
			ps.setLong(2, Long.parseLong(cveEpo));

			rs = ps.executeQuery();
			if(rs.next()){
				cveEpo = rs.getString("ic_epo");
				cvePyme = rs.getString("ic_pyme");
			}
			rs.close();
			ps.close();

			if(!"".equals(cveEpo) && !"".equals(cvePyme)){
				strSQL = "SELECT cs_bloqueo " +
						"  FROM comrel_bloqueo_pyme_x_epo " +
						" WHERE ic_pyme = ? AND ic_epo = ? AND ic_producto_nafin = ? ";

				ps = con.queryPrecompilado(strSQL);
				ps.setLong(1,Long.parseLong(cvePyme));
				ps.setLong(2,Long.parseLong(cveEpo));
				ps.setLong(3,Long.parseLong(cveProducto));

				rs = ps.executeQuery();
				if(rs.next()){
					if("B".equals(rs.getString("cs_bloqueo")))
						sibloqueda = true;
				}
				rs.close();
				ps.close();
			}

			return sibloqueda;
		}catch(Throwable t){
			throw new AppException("Error al consultar si la pyme esta bloqueda para la epo", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("CargaDocumentoBean::validaPymeXEpoBloqXNumProv(S)");
		}
	}

	public String getAvisosNotificacionDisWS(String claveEPO, String usuario, String password, String fechaOperacionIni, String fechaOperacionFin){
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		StringBuffer sbArchivo = new StringBuffer();
		String nombreEpo = "";
		try{
			con.conexionDB();

			String tituloMensaje = "Ejecucion getAvisosNotificacionDisWS";
			String descMsgError = "";
			//**********************************Validaci�n de parametros:*****************************
			try {
				if (claveEPO == null || claveEPO.equals("")) {
					throw new Exception("El par�metro -Clave de EPO- debe ser especificado");
				}
				if (claveEPO != null && !claveEPO.equals("")) {
					Integer.parseInt(claveEPO);

					ps = con.queryPrecompilado("select cg_razon_social from comcat_epo where ic_epo = ? ");
					ps.setLong(1,Long.parseLong(claveEPO));
					rs = ps.executeQuery();
					if(rs.next()){
						nombreEpo = rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social");
					}
					rs.close();
					ps.close();
				}
				System.out.println("fechaOperacionIni ====== "+fechaOperacionIni);
				System.out.println("fechaOperacionFin ====== "+fechaOperacionFin);
				if (fechaOperacionIni != null && !fechaOperacionIni.equals("")) {
					Comunes.parseDate(fechaOperacionIni);
				}else{
					fechaOperacionIni	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				}
				if (fechaOperacionFin != null && !fechaOperacionFin.equals("")) {
					Comunes.parseDate(fechaOperacionFin);
				}else{
					fechaOperacionFin	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				}


				UtilUsr utilUsr = new UtilUsr();
				List cuentasEPO = utilUsr.getUsuariosxAfiliado(claveEPO, "E");

				log.info("getAvisosNotificacionWS::Validando claveEPO/usuario");

				if (!cuentasEPO.contains(usuario)) {
					throw new Exception("El usuario no es de la epo especificada");
				}

				log.info("getAvisosNotificacionWS::Validando usuario/passwd");
				if (!utilUsr.esUsuarioValido(usuario, password)) {
					throw new Exception("Fall� la autenticaci�n del usuario");
				}

			}catch(Throwable t){
				t.printStackTrace();
				descMsgError = t.getMessage();
				String error =
					"Error en los parametros recibidos. " + t.getMessage() + "\n" +
					"claveEPO=" + claveEPO + "\n" +
					"usuario=" + usuario + "\n" +
					"password=********" + "\n" +
					"fechaOperacionIni=" + fechaOperacionIni + "\n" +
					"fechaOperacionFin=" + fechaOperacionFin + "\n";

					log.error(error);

				try { //Si el env�o del correo falla se ignora.
					String to = this.getCuentasWS(claveEPO);
					if (to != null) {
						//String from = this.getCuentaWSFrom();

						String styleEncabezados = " style='font-family: Verdana, Arial, Helvetica, sans-serif; "+
							"font-size: 10px; "+
							"font-weight: bold;"+
							"color: #FFFFFF;"+
							"background-color: #4d6188;"+
							"padding-top: 3px;"+
							"padding-right: 1px;"+
							"padding-bottom: 1px;"+
							"padding-left: 3px;"+
							"height: 25px;"+
							"border: 1px solid #1a3c54;'";

						String style = "style='font-family:Verdana, Arial, Helvetica, sans-serif;"+
							"color:#000066; "+
							"padding-top:1px; "+
							"padding-right:2px; "+
							"padding-bottom:1px; "+
							"padding-left:2px; "+
							"height:22px; "+
							"font-size:11px;'";

						String tabla1 = "<table border=\"1\">";
						tabla1 += "	<tr>" +
								 "		<td align=\"center\" "+styleEncabezados+">Parametros</td>" +
								 "		<td align=\"center\" "+styleEncabezados+">Valores</td>" +
								 "		<td align=\"center\" "+styleEncabezados+">Descripcion del Error</td>" +
								 "	</tr>";

						tabla1 += "	<tr>" +
							"		<td align=\"center\" "+style+">claveEPO</td>" +
							"		<td align=\"center\" "+style+">" + claveEPO + "</td>" +
							"		<td align=\"center\" "+style+" rowspan=5>" + descMsgError + "</td>" +
							"	</tr>";
						tabla1 += "	<tr>" +
							"		<td align=\"center\" "+style+">usuario</td>" +
							"		<td align=\"center\" "+style+">" + usuario + "</td>" +
							"	</tr>";
						tabla1 += "	<tr>" +
							"		<td align=\"center\" "+style+">password</td>" +
							"		<td align=\"center\" "+style+">********</td>" +
							"	</tr>";
						tabla1 += "	<tr>" +
							"		<td align=\"center\" "+style+">fechaOperacionIni</td>" +
							"		<td align=\"center\" "+style+">" + fechaOperacionIni + "</td>" +
							"	</tr>";
						tabla1 += "	<tr>" +
							"		<td align=\"center\" "+style+">fechaOperacionFin</td>" +
							"		<td align=\"center\" "+style+">" + fechaOperacionFin + "</td>" +
							"	</tr>";
						tabla1 += "</table><br><br>";

						String textoCorreo = "<form style='font-family: calibri, verdana, courier, arial, sans-serif; font-size:14;'><p> Nombre EPO: "+nombreEpo+"<br><br>Error en los parametros recibidos.<br><br><br></p>"+tabla1+"</form>";

						Correo correo = new Correo();
						correo.enviarTextoHTML("No_response@nafin.gob.mx", to, tituloMensaje, textoCorreo);
					}
				} catch(Throwable t2) {
					t2.printStackTrace();
					log.error("Error al enviar correo " + t2.getMessage());
				}

				return error;
			}


			//INICIA CODIGO PARA CONSULTA
			StringBuffer qrySentencia = new StringBuffer();
			StringBuffer qryDM			  = new StringBuffer();
			StringBuffer qryCCC		    = new StringBuffer();
			StringBuffer qryFdR			= new StringBuffer();
			String tipo_credito_ini = "";
			String fn_porcentaje_iva = "";

			strSQL = " select nvl(a.cg_tipos_credito,b.cg_tipos_credito) as tipos_credito "+
					" from comrel_producto_epo a,comcat_producto_nafin b " +
					" where a.ic_epo = ? " +
					" and a.ic_producto_nafin = ? " +
					" and a.ic_producto_nafin = b.ic_producto_nafin ";

			ps = con.queryPrecompilado(strSQL);
			ps.setLong(1, Long.parseLong(claveEPO));
			ps.setInt(2, 4);
			rs = ps.executeQuery();
			if(rs.next()){
				tipo_credito_ini = rs.getString("tipos_credito")==null?"":rs.getString("tipos_credito");
			}
			rs.close();
			ps.close();

      strSQL = " SELECT fn_porcentaje_iva " +
            "  FROM comcat_iva " +
            " WHERE df_aplicacion IN (SELECT MAX (df_aplicacion) " +
            "                           FROM comcat_iva) ";

			ps = con.queryPrecompilado(strSQL);
			rs = ps.executeQuery();
			if(rs.next()){
				fn_porcentaje_iva = rs.getString("fn_porcentaje_iva")==null?"":rs.getString("fn_porcentaje_iva");
			}
			rs.close();
			ps.close();
			   
			ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
			            
			 String meseSinIntereses =BeanParametros.DesAutomaticoEpo(claveEPO,"CS_MESES_SIN_INTERESES"); // Fodea 09-2015 
			            
			            


			qryDM.append(
				"   SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
				"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
				"   index (lcd CP_DIS_LINEA_CREDITO_DM_PK) "   +
				"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
				"   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
				"   index (m CP_COMCAT_MONEDA_PK)"   +
				"   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
				"   index (e CP_COMCAT_EPO_PK)"   +
				"   index (ci CP_COMCAT_IF_PK ) "   +
				"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
				"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
				"   P.cg_razon_social as DISTRIBUIDOR"   +
				"  ,D.ig_numero_docto"   +
				"  ,D.cc_acuse"   +
				"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
				"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
				"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
				"  ,D.IG_PLAZO_DOCTO"   +
				"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
				"  ,M.cd_nombre as MONEDA"   +
				"  ,M.ic_moneda"   +
				"  ,D.fn_monto"   +
				"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
				"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
				"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
				"  ,D.IG_PLAZO_DESCUENTO"   +
				"  ,D.FN_MONTO"   +
				"  ,D.fn_porc_descuento"   +
				"  ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
				"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
				"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
				"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
				"  ,D.IG_PLAZO_CREDITO"   +
				"  ,DS.fn_importe_recibir as MONTO_CREDITO"   +
				"  ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
				"  ,TCI.ic_tipo_cobro_interes"   +
				"        ,TCI.cd_descripcion as tipo_cobro_interes"   +
				"        ,DS.fn_importe_interes as MONTO_INTERES"   +
				"        ,D.ic_documento"   +
				"        ,E.cg_razon_social as EPO"   +
				"  , D.ic_documento"   +
				"  ,CI.cg_razon_social as DIF"   +
				"  ,TF.cd_descripcion as MODO_PLAZO"   +
				"  ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
				"  ,'Descuento' as tipo_credito"   +
				"  ,LCD.ic_moneda as moneda_linea"   +
				"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"	 ,ds.FN_PORC_COMISION_APLI as COMISION_APLICABLE " +
				"	 , 'N/A' as CG_CODIGO_RESP " +
				"	 , 'N/A' as CG_CODIGO_DESC " +
				"   , 'N/A' as IC_ORDEN_PAGO_TC " +
				"   , 'N/A' as CG_TRANSACCION_ID " +
				"   , 0  as FN_MONTO_TOTAL " +
            "   , 'N/A' as CG_NUM_TARJETA " +
			    ", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ', '3', 'Tarjeta de Credito' ) AS TIPOPAGO "+ //F09-2015			           
				", d.IG_MESES_INTERES as MESES_INTERESES "+//F09-2015
				" , d.ig_tipo_pago as ig_tipo_pago "+ //F09-2015
			           
				"   FROM DIS_DOCUMENTO D"   +
				"  ,DIS_DOCTO_SELECCIONADO DS"   +
				"  ,DIS_SOLICITUD S"   +
				"  ,COM_ACUSE3 C3"   +
				"  ,COMCAT_PYME P"   +
				"  ,DIS_LINEA_CREDITO_DM LCD"   +
				"  ,COMCAT_PRODUCTO_NAFIN PN"   +
				"  ,COMREL_PRODUCTO_EPO PE"   +
				"  ,COMCAT_TASA CT"   +
				"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
				"  ,COMCAT_MONEDA M"   +
				"  ,COM_TIPO_CAMBIO TC"   +
				"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
				"  ,COMCAT_EPO E"   +
				"  ,COMCAT_IF CI"   +
				"  ,COMCAT_ESTATUS_DOCTO ED"   +
				"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"  WHERE "   +
				"  D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
				"  AND D.IC_EPO = PE.IC_EPO"   +
				"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
				"  AND P.IC_PYME = D.IC_PYME"   +
				"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
				"  AND D.IC_EPO = PE.IC_EPO"   +
				"  AND D.IC_MONEDA = M.IC_MONEDA"   +
				"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
				"  AND LCD.IC_IF = IEP.IC_IF"   +
				"  AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
				"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
				"  AND IEP.IC_EPO = D.IC_EPO"   +
				"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"  AND DS.IC_TASA = CT.IC_TASA"   +
				"  AND D.IC_EPO = E.IC_EPO"   +
				"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
				"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
				"  AND LCD.IC_IF = CI.IC_IF"  +
				"  AND D.IC_EPO = ? " +
				" AND D.ic_estatus_docto in(4,11)  " +
				" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') "  );

			qryCCC.append(
					"  SELECT  /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
					"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
					"   index (lcd CP_COM_LINEA_CREDITO_PK) "   +
					"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
					"   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
					"   index (m CP_COMCAT_MONEDA_PK)"   +
					"   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
					"   index (e CP_COMCAT_EPO_PK)"   +
					"   index (ci CP_COMCAT_IF_PK ) "   +
					"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
					"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
					"    P.cg_razon_social as DISTRIBUIDOR"   +
					"   ,D.ig_numero_docto"   +
					"   ,D.cc_acuse"   +
					"   ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
					"   ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
					"   ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
					"   ,D.IG_PLAZO_DOCTO"   +
					"   ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
					"   ,M.cd_nombre as MONEDA"   +
					"   ,M.ic_moneda"   +
					"   ,D.fn_monto"   +
					"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"   ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
					"   ,D.IG_PLAZO_DESCUENTO"   +
					"   ,D.FN_MONTO"   +
					"   ,D.fn_porc_descuento"   +
					"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
					"   ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"   ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"   ,DECODE(DS.CG_REL_MAT"   +
					"   ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"   ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"   ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"   ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"   ,0) AS VALOR_TASA"   +
					"   ,D.IG_PLAZO_CREDITO"   +
					"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
					"   ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
					"   ,TCI.ic_tipo_cobro_interes"   +
					"   ,TCI.cd_descripcion as tipo_cobro_interes"   +
					"   ,DS.fn_importe_interes as MONTO_INTERES"   +
					"   ,D.ic_documento"   +
					"   ,E.cg_razon_social as EPO"   +
					"   , D.ic_documento"   +
					"   ,CI.cg_razon_social as DIF"   +
					"   ,TF.cd_descripcion as MODO_PLAZO"   +
					"   ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
					"   ,'Credito en Cuenta Corriente' as tipo_credito"   +
					"   ,LCD.ic_moneda as moneda_linea"   +
					"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					"	 ,ds.FN_PORC_COMISION_APLI as COMISION_APLICABLE " +
					"	 , 'N/A' as CG_CODIGO_RESP " +
					"	 , 'N/A' as CG_CODIGO_DESC " +
					"   , 'N/A' as IC_ORDEN_PAGO_TC " +
					"   , 'N/A' as CG_TRANSACCION_ID " +
					"   , 0  as FN_MONTO_TOTAL " +
					"   , 'N/A' as CG_NUM_TARJETA " +
			      ", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ', '3', 'Tarjeta de Credito' ) AS TIPOPAGO "+ //F09-2015                    
			      ", d.IG_MESES_INTERES as MESES_INTERESES "+//F09-2015
			      " , d.ig_tipo_pago as ig_tipo_pago "+ //F09-2015
							  
					"    FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   ,COMCAT_PYME P"   +
					"   ,COM_LINEA_CREDITO LCD"   +
					"   ,COMCAT_PRODUCTO_NAFIN PN"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMCAT_TASA CT"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"   ,COMCAT_EPO E"   +
					"   ,COMCAT_IF CI"   +
					"   ,COMCAT_ESTATUS_DOCTO ED"   +
					"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"   AND P.IC_PYME = D.IC_PYME"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"   AND LCD.IC_IF = IEP.IC_IF"   +
					"   AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
					"   AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO"   +
					"   AND IEP.IC_EPO = D.IC_EPO"   +
					"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND DS.IC_TASA = CT.IC_TASA"   +
					"   AND D.IC_EPO = E.IC_EPO"   +
					"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"   AND LCD.IC_IF = CI.IC_IF"  +
					"    AND D.IC_EPO = ? " +
					"	  AND D.ic_estatus_docto in(4,11)  " +
					"	  AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");

					qryCCC.append(	"UNION ALL " +
							" SELECT  /*+ index (d CP_DIS_DOCUMENTO_PK)  " +
							"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)  " +
							"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK) " +
							"   index (pe CP_COMREL_PRODUCTO_EPO_PK) " +
							"   index (m CP_COMCAT_MONEDA_PK) " +
							"   index (tc CP_COM_TIPO_CAMBIO_PK) " +
							"   index (e CP_COMCAT_EPO_PK) " +
							"   index (ci CP_COMCAT_IF_PK )  " +
							"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK )  " +
							"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */   " +
							"    P.cg_razon_social as DISTRIBUIDOR " +
							"   ,D.ig_numero_docto " +
							"   ,D.cc_acuse " +
							"   ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision " +
							"   ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion " +
							"   ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc " +
							"   ,D.IG_PLAZO_DOCTO " +
							"   ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion " +
							"   ,M.cd_nombre as MONEDA " +
							"   ,M.ic_moneda " +
							"   ,D.fn_monto " +
							"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION " +
							"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO " +
							"   ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO " +
							"   ,D.IG_PLAZO_DESCUENTO " +
							"   ,D.FN_MONTO " +
							"   ,D.fn_porc_descuento " +
							"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO " +
							"   ,ED.CD_DESCRIPCION AS ESTATUS " +
							"   ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT " +
							"   ,DECODE(DS.CG_REL_MAT " +
							"   ,'+',DS.fn_valor_tasa + DS.fn_puntos " +
							"   ,'-',DS.fn_valor_tasa - DS.fn_puntos " +
							"   ,'*',DS.fn_valor_tasa * DS.fn_puntos " +
							"   ,'/',DS.fn_valor_tasa / DS.fn_puntos " +
							"   ,0) AS VALOR_TASA " +
							"   ,D.IG_PLAZO_CREDITO " +
							"   ,DS.fn_importe_recibir as MONTO_CREDITO " +
							"   ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO " +
							"   ,0 as ic_tipo_cobro_interes " +
							"   ,'' as tipo_cobro_interes " +
							"   ,DS.fn_importe_interes as MONTO_INTERES " +
							"   ,D.ic_documento " +
							"   ,E.cg_razon_social as EPO " +
							"   , D.ic_documento " +
							"   ,CI.cg_razon_social as DIF " +
							"   ,TF.cd_descripcion as MODO_PLAZO " +
							"   ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora " +
							"   ,'Credito en Cuenta Corriente' as tipo_credito " +
							"   ,CBI.ic_moneda as moneda_linea " +
							"   ,d.CG_VENTACARTERA as CG_VENTACARTERA  " +
							"	 ,ds.FN_PORC_COMISION_APLI as COMISION_APLICABLE " +
							"	 ,dpt.CG_CODIGO_RESP as CG_CODIGO_RESP " +
							"	 ,dpt.CG_CODIGO_DESC as CG_CODIGO_DESC " +
							"   ,dpt.IC_ORDEN_PAGO_TC as IC_ORDEN_PAGO_TC " +
							"   ,dpt.CG_TRANSACCION_ID as CG_TRANSACCION_ID " +
							"   ,dpt.FN_MONTO_TOTAL as FN_MONTO_TOTAL " +
						   "  ,dpt.CG_NUM_TARJETA as CG_NUM_TARJETA " +
							", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ', '3', 'Tarjeta de Credito' ) AS TIPOPAGO "+ //F09-2015                    
							 " , d.IG_MESES_INTERES as MESES_INTERESES "+//F09-2015
							 " , d.ig_tipo_pago as ig_tipo_pago "+ //F09-2015
										
							"    FROM DIS_DOCUMENTO D " +
							"   ,DIS_DOCTO_SELECCIONADO DS " +
							"   ,DIS_SOLICITUD S " +
							"   ,COM_ACUSE3 C3 " +
							"   ,COMCAT_PYME P " +
							"   ,COM_BINS_IF CBI " +
							"   ,COMCAT_PRODUCTO_NAFIN PN " +
							"   ,COMREL_PRODUCTO_EPO PE " +
							"   ,COMCAT_TASA CT " +
							"   ,COMREL_IF_EPO_X_PRODUCTO IEP " +
							"   ,COMCAT_MONEDA M " +
							"   ,COM_TIPO_CAMBIO TC " +
							"   ,COMCAT_EPO E " +
							"   ,COMCAT_IF CI " +
							"   ,COMCAT_ESTATUS_DOCTO ED " +
							"   ,COMCAT_TIPO_FINANCIAMIENTO TF " +
							"   ,DIS_DOCTOS_PAGO_TC DPT " +
							"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
							"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
							"   AND D.IC_ORDEN_PAGO_TC = DPT.IC_ORDEN_PAGO_TC(+) " +
							"   AND D.IC_EPO = PE.IC_EPO " +
							"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
							"   AND PN.IC_PRODUCTO_NAFIN = 4 " +
							"   AND P.IC_PYME = D.IC_PYME " +
							"   AND D.IC_EPO = PE.IC_EPO " +
							"   AND D.IC_MONEDA = M.IC_MONEDA " +
							"   AND M.IC_MONEDA = TC.IC_MONEDA(+) " +
							"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
							"   AND D.IC_BINS = CBI.IC_BINS " +
							"   AND CBI.IC_IF = IEP.IC_IF " +
							"   AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO " +
							"   AND IEP.IC_EPO = D.IC_EPO " +
							"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
							"   AND DS.IC_TASA = CT.IC_TASA(+) " +
							"   AND D.IC_EPO = E.IC_EPO " +
							"   AND S.CC_ACUSE = C3.CC_ACUSE " +
							"   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
							"   AND CBI.IC_IF = CI.IC_IF " +
							"   AND D.IC_EPO = ? " +
							"   AND D.ic_estatus_docto in(32)   " +
							"   AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY')  ");


				qryFdR.append(
					"  SELECT  /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
					"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
					"   index (lcd CP_COM_LINEA_CREDITO_PK) "   +
					"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
					"   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
					"   index (m CP_COMCAT_MONEDA_PK)"   +
					"   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
					"   index (e CP_COMCAT_EPO_PK)"   +
					"   index (ci CP_COMCAT_IF_PK ) "   +
					"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
					"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
					"    P.cg_razon_social as DISTRIBUIDOR"   +
					"   ,D.ig_numero_docto"   +
					"   ,D.cc_acuse"   +
					"   ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
					"   ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
					"   ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
					"   ,D.IG_PLAZO_DOCTO"   +
					"   ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
					"   ,M.cd_nombre as MONEDA"   +
					"   ,M.ic_moneda"   +
					"   ,D.fn_monto"   +
					"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"   ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
					"   ,D.IG_PLAZO_DESCUENTO"   +
					"   ,D.FN_MONTO"   +
					"   ,D.fn_porc_descuento"   +
					"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
					"   ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"   ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"   ,DECODE(DS.CG_REL_MAT"   +
					"   ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"   ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"   ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"   ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"   ,0) AS VALOR_TASA"   +
					"   ,D.IG_PLAZO_CREDITO"   +
					"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
					"   ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
					"   ,TCI.ic_tipo_cobro_interes"   +
					"   ,TCI.cd_descripcion as tipo_cobro_interes"   +
					"   ,DS.fn_importe_interes as MONTO_INTERES"   +
					"   ,D.ic_documento"   +
					"   ,E.cg_razon_social as EPO"   +
					"   , D.ic_documento"   +
					"   ,CI.cg_razon_social as DIF"   +
					"   ,'NA' as MODO_PLAZO"   +
					"   ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
					"   ,'Factoraje con Recurso' as tipo_credito"   +
					"   ,LCD.ic_moneda as moneda_linea"   +
					"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					"	 ,ds.FN_PORC_COMISION_APLI as COMISION_APLICABLE " +
					"	 , 'N/A' as CG_CODIGO_RESP " +
					"	 , 'N/A' as CG_CODIGO_DESC " +
					"   , 'N/A' as IC_ORDEN_PAGO_TC " +
					"   , 'N/A' as CG_TRANSACCION_ID " +
					"   , 0  as FN_MONTO_TOTAL " +
          "   , 'N/A' as CG_NUM_TARJETA " +
					" ,lcd.ig_cuenta_bancaria as CG_NUMERO_CUENTA "+			  
			   ", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ', '3', 'Tarjeta de Credito' ) AS TIPOPAGO "+ //F09-2015                    			   
			   ", d.IG_MESES_INTERES as MESES_INTERESES "+//F09-2015
			   " , d.ig_tipo_pago as ig_tipo_pago "+ //F09-2015
					
					"    FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   ,COMCAT_PYME P"   +
					"   ,COM_LINEA_CREDITO LCD"   +
					"   ,COMCAT_PRODUCTO_NAFIN PN"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMCAT_TASA CT"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"   ,COMCAT_EPO E"   +
					"   ,COMCAT_IF CI"   +
					"   ,COMCAT_ESTATUS_DOCTO ED"   +
					"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO "   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"   AND P.IC_PYME = D.IC_PYME"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"   AND LCD.IC_IF = IEP.IC_IF"   +
					"   AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
					"   AND TF.IC_TIPO_FINANCIAMIENTO(+) = D.IC_TIPO_FINANCIAMIENTO"   +
					"   AND IEP.IC_EPO = D.IC_EPO"   +
					"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND DS.IC_TASA = CT.IC_TASA"   +
					"   AND D.IC_EPO = E.IC_EPO"   +
					"   AND S.CC_ACUSE = C3.CC_ACUSE "   +
					"   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"   AND LCD.IC_IF = CI.IC_IF"  +
					"   AND d.IC_TIPO_FINANCIAMIENTO is null "+
					"	 AND LCD.cg_tipo_solicitud = 'I' "+
					"	 AND LCD.cs_factoraje_con_rec = 'S' "+
					"   AND D.IC_EPO = ? " +
					"	 AND D.ic_estatus_docto in(4,11)  " +
					"	 AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");


			if("D".equals(tipo_credito_ini)){
				qrySentencia.append(qryDM.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setLong(1, Long.parseLong(claveEPO));
				ps.setString(2, fechaOperacionIni);
				ps.setString(3, fechaOperacionFin);
			}else if("C".equals(tipo_credito_ini)){
				qrySentencia.append(qryCCC.toString());
				System.out.println("qrySentencia.toString() ======= "+qrySentencia.toString());
				System.out.println("qrySentencia.claveEPO() ======= "+claveEPO);
				System.out.println("qrySentencia.fechaOperacionIni() ======= "+fechaOperacionIni);
				System.out.println("qrySentencia.fechaOperacionFin() ======= "+fechaOperacionFin);
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setLong(1, Long.parseLong(claveEPO));
				ps.setString(2, fechaOperacionIni);
				ps.setString(3, fechaOperacionFin);

				ps.setLong(4, Long.parseLong(claveEPO));
				ps.setString(5, fechaOperacionIni);
				ps.setString(6, fechaOperacionFin);
			}else if("F".equals(tipo_credito_ini)){
				qrySentencia.append(qryFdR.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setLong(1, Long.parseLong(claveEPO));
				ps.setString(2, fechaOperacionIni);
				ps.setString(3, fechaOperacionFin);
			}else{
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.setLong(1, Long.parseLong(claveEPO));
				ps.setString(2, fechaOperacionIni);
				ps.setString(3, fechaOperacionFin);
				ps.setLong(4, Long.parseLong(claveEPO));
				ps.setString(5, fechaOperacionIni);
				ps.setString(6, fechaOperacionFin);
			}

			rs = ps.executeQuery();
			int i = 0;
			while(rs.next()){

				String distribuidor = rs.getString("DISTRIBUIDOR")==null?"":rs.getString("DISTRIBUIDOR");
				String ig_numero_docto = rs.getString("IG_NUMERO_DOCTO")==null?"":rs.getString("IG_NUMERO_DOCTO");
				String cc_acuse = rs.getString("CC_ACUSE")==null?"":rs.getString("CC_ACUSE");
				String df_fecha_emision = rs.getString("DF_FECHA_EMISION")==null?"":rs.getString("DF_FECHA_EMISION");
				String df_fecha_publicacion = rs.getString("DF_FECHA_PUBLICACION")==null?"":rs.getString("DF_FECHA_PUBLICACION");
				String df_fecha_venc = rs.getString("DF_FECHA_VENC")==null?"":rs.getString("DF_FECHA_VENC");
				String ig_plazo_docto = rs.getString("IG_PLAZO_DOCTO")==null?"":rs.getString("IG_PLAZO_DOCTO");
				//String df_fecha_seleccion = rs.getString("DF_FECHA_SELECCION")==null?"":rs.getString("DF_FECHA_SELECCION");
				String moneda = rs.getString("MONEDA")==null?"":rs.getString("MONEDA");
				//String ic_moneda = rs.getString("IC_MONEDA")==null?"":rs.getString("IC_MONEDA");
				String fn_monto = rs.getString("FN_MONTO")==null?"":rs.getString("FN_MONTO");
				//String tipo_conversion = rs.getString("TIPO_CONVERSION")==null?"":rs.getString("TIPO_CONVERSION");
				//String tipo_cambio = rs.getString("TIPO_CAMBIO")==null?"":rs.getString("TIPO_CAMBIO");
				//String monto_valuado = rs.getString("MONTO_VALUADO")==null?"":rs.getString("MONTO_VALUADO");
				String ig_plazo_descuento = rs.getString("IG_PLAZO_DESCUENTO")==null?"":rs.getString("IG_PLAZO_DESCUENTO");
				String fn_porc_descuento = rs.getString("FN_PORC_DESCUENTO")==null?"":rs.getString("FN_PORC_DESCUENTO");
				String monto_descuento = rs.getString("MONTO_DESCUENTO")==null?"":rs.getString("MONTO_DESCUENTO");
				String estatus = rs.getString("ESTATUS")==null?"":rs.getString("ESTATUS");
				String referencia_int = rs.getString("REFERENCIA_INT")==null?"":rs.getString("REFERENCIA_INT");
				String valor_tasa = rs.getString("VALOR_TASA")==null?"":rs.getString("VALOR_TASA");
				String ig_plazo_credito = rs.getString("IG_PLAZO_CREDITO")==null?"":rs.getString("IG_PLAZO_CREDITO");
				String monto_credito = rs.getString("MONTO_CREDITO")==null?"":rs.getString("MONTO_CREDITO");
				String df_fecha_venc_credito = rs.getString("DF_FECHA_VENC_CREDITO")==null?"":rs.getString("DF_FECHA_VENC_CREDITO");
				String ic_tipo_cobro_interes = rs.getString("IC_TIPO_COBRO_INTERES")==null?"":rs.getString("IC_TIPO_COBRO_INTERES");
				String tipo_cobro_interes = rs.getString("TIPO_COBRO_INTERES")==null?"":rs.getString("TIPO_COBRO_INTERES");
				String monto_interes = rs.getString("MONTO_INTERES")==null?"":rs.getString("MONTO_INTERES");
				String ic_documento = rs.getString("IC_DOCUMENTO")==null?"":rs.getString("IC_DOCUMENTO");
				//String epo = rs.getString("EPO")==null?"":rs.getString("EPO");
				String dif = rs.getString("DIF")==null?"":rs.getString("DIF");
				//String modo_plazo = rs.getString("MODO_PLAZO")==null?"":rs.getString("MODO_PLAZO");
				String df_fecha_hora = rs.getString("DF_FECHA_HORA")==null?"":rs.getString("DF_FECHA_HORA");
				String tipo_credito = rs.getString("TIPO_CREDITO")==null?"":rs.getString("TIPO_CREDITO");
				//String moneda_linea = rs.getString("MONEDA_LINEA")==null?"":rs.getString("MONEDA_LINEA");
				//String cg_ventacartera = rs.getString("CG_VENTACARTERA")==null?"":rs.getString("CG_VENTACARTERA");


				String comision_aplicable = rs.getString("COMISION_APLICABLE")==null?"0.0":rs.getString("COMISION_APLICABLE");
				BigDecimal bdmontoComision = (((new BigDecimal(monto_credito)).multiply(new BigDecimal(comision_aplicable))).divide(new BigDecimal(100),2, 3));
				bdmontoComision = bdmontoComision.add(  (bdmontoComision.multiply(new BigDecimal(fn_porcentaje_iva))).divide(new BigDecimal(100),2, 3)   );

				String monto_comision = bdmontoComision.toPlainString();
				String monto_depositar = (new BigDecimal(monto_credito).subtract(new BigDecimal(monto_comision))).toPlainString();
				String cg_codigo_resp = rs.getString("CG_CODIGO_RESP")==null?"":rs.getString("CG_CODIGO_RESP");
				String cg_codigo_desc = rs.getString("CG_CODIGO_DESC")==null?"":rs.getString("CG_CODIGO_DESC");
				String ic_orden_pago_tc = rs.getString("IC_ORDEN_PAGO_TC")==null?"":rs.getString("IC_ORDEN_PAGO_TC");
				String cg_transaccion_id = rs.getString("CG_TRANSACCION_ID")==null?"":rs.getString("CG_TRANSACCION_ID");
				//String fn_monto_total = rs.getString("FN_MONTO_TOTAL")==null?"":rs.getString("FN_MONTO_TOTAL");
				String cg_num_tarjeta = rs.getString("CG_NUM_TARJETA")==null?"":rs.getString("CG_NUM_TARJETA");
				String codRespCompuesta = "";
			   String tipo_Pago = rs.getString("TIPOPAGO")==null?"":rs.getString("TIPOPAGO"); //F09-2015
				String meses = rs.getString("MESES_INTERESES")==null?"":rs.getString("MESES_INTERESES"); //F09-2015
				String ig_tipo_pago = rs.getString("ig_tipo_pago")==null?"":rs.getString("ig_tipo_pago"); //F09-2015      
				           

				if ("".equals(cg_codigo_resp) || "N/A".equals(cg_codigo_resp)){
					comision_aplicable = "N/A";
					monto_comision = "N/A";
					monto_depositar = "N/A";
          ic_orden_pago_tc = "N/A";
					cg_transaccion_id = "N/A";
					codRespCompuesta = "N/A";
          cg_num_tarjeta = "N/A";
				}else{
					df_fecha_venc_credito = "N/A";
					ig_plazo_docto = "N/A";
					df_fecha_venc = "N/A";
					monto_interes = "N/A";
					tipo_cobro_interes = "N/A";
					ic_tipo_cobro_interes = "N/A";
          ig_plazo_credito = "N/A";
          referencia_int = "N/A";
          valor_tasa = "N/A";

          //if(!"N/A".equals(cg_codigo_resp)){
          codRespCompuesta = cg_codigo_resp+"-"+cg_codigo_desc;
           cg_num_tarjeta = "xxxx-xxxx-xxxx-"+cg_num_tarjeta;
          //}else{
            //codRespCompuesta = cg_codigo_resp;
          //}

				}

				if(i==0){
					sbArchivo.append(
							"IF documento inicial, Distribuidor documento inicial, No. Docto Inicial, No. Acuse Carga documento inicial, " +
							"Fecha Emisi�n documento inicial, Fecha Pub. documento inicial, Fecha Venc. documento inicial, Fecha Oper. documento inicial, " +
							"Plazo docto. documento inicial, Moneda documento inicial, Monto  documento inicial, Plazo Dscto D�as documento inicial, " +
							"% Dscto documento inicial, Monto % Dscto documento inicial, Estatus  documento inicial, Tipo Cred. documento final, " +
							"No. Docto Final documento final, Monto documento final, % Comision Aplicable de Terceros, Monto Comision de Terceros (IVA Incluido), Monto Depositar por Operacion, Plazo documento final ");
					
				   if ( meseSinIntereses.equals("S"))  {  //F09-2015
				    sbArchivo.append(", Tipo de Pago,   Periodo ");
				    }
					sbArchivo.append(", Fecha Venc. documento final, Ref. Tasa Interes documento final, " +
							"Valor Tasa de Interes documento final, Monto Interes documento final, ID Orden enviado, Respuesta de Operacion, Codigo de Autorizacion, Numero Tarjeta de Credito \n");
				}

				sbArchivo.append(
							dif.replace(',',' ')+", " +
							distribuidor.replace(',',' ')+", " +
							ig_numero_docto+", " +
							cc_acuse.replace(',',' ')+", " +
							df_fecha_emision+", " +
							df_fecha_publicacion+", " +
							df_fecha_venc+", " +
							df_fecha_hora+", " +
							ig_plazo_docto+", " +
							moneda+", " +
							Comunes.formatoDecimal(fn_monto,2,false)+", " +
							ig_plazo_descuento+", " +
							fn_porc_descuento+", " +
							Comunes.formatoDecimal(monto_descuento,2,false)+", " +
							estatus+", " +
							tipo_credito+", " +
							ic_documento+", " +
							Comunes.formatoDecimal(monto_credito,2,false)+", " +
							comision_aplicable+", " +
							monto_comision+", " +
							monto_depositar+", " +
							ig_plazo_credito );

						if (meseSinIntereses.equals("S")) { //F09-2015
							sbArchivo.append(", " + tipo_Pago);
							if (ig_tipo_pago.equals("2")) { //F09-2015
								sbArchivo.append(", " + meses + "(M) ");
							} else {
								sbArchivo.append(",  ");
							}
						}
		
						sbArchivo.append(", " + df_fecha_venc_credito);
		
						if (ig_tipo_pago.equals("2")) {
							sbArchivo.append(",  N/A  ");
						} else {
							sbArchivo.append(", " + referencia_int);
						}
			                     
						
						sbArchivo.append(", " +valor_tasa+", " +
			         Comunes.formatoDecimal(monto_interes,2,false)  +", " +
							ic_orden_pago_tc +", " +
							cg_transaccion_id +", " +
							codRespCompuesta+", " +
              cg_num_tarjeta +
							//fn_monto_total +", " +
							//tipo_conversion+", " +
							//tipo_cambio+", " +
							//Comunes.formatoDecimal(monto_valuado,2,false)+", " +
							//modalidad_plazo.replace(',',' ')+", " +
							//cg_numero_cuenta+", " +
				  "\n");
				i++;
			}
			rs.close();
			ps.close();



			/*
			try { //Si el env�o del correo falla se ignora.
				String to = this.getCuentasWS(claveEPO);
				if (to != null) {
					String from = this.getCuentaWSFrom();
					Correo.enviarMensajeTexto(to, from, tituloMensaje, sbArchivo.toString());
				}
			} catch(Throwable t) {
				log.error("Error al enviar correo " + t.getMessage());
			}
			*/
			System.out.println("sbArchivo ====== "+sbArchivo);
			} catch (Exception e){
				e.printStackTrace();
				sbArchivo.append("ERROR. " + e.getMessage());
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}

			return sbArchivo.toString();
	}


private boolean procesoCargaDoctosWS( WSEnlace disEnlace ) throws AppException, NafinException{
	boolean retorno=false;
	log.info("CargaDocDistBean::procesoCargaDoctosWS(E)");
	boolean resultado = true;
	boolean regErroneos = false;
	AccesoDB con = null;
	ResultSet rs = null;

	StringBuffer sblog = new StringBuffer("");
	StringBuffer error=new StringBuffer();
	StringBuffer noError=new StringBuffer();
	String strError = "";
	StringTokenizer st,st2;

	String qrySentencia = "";
	String Parametr = "";
	BigDecimal monto = null;
	Vector vecColumnas = null;
	String NomFinancimiento ="";
	String qryModalidadPlazo ="";
	ResultSet rs2 = null;
    PreparedStatement ps2 = null;
	String valorFinanciamiento ="";
	String MontoLineaCredito = "";
	double fn_monto2 = 0.0;
	double MontoLineaCredito2 =0;
	String NOnegociable ="";
	double porLineaCredito = 0;
	String validaLinea ="", msgError = "";
	BigDecimal monto_mn=new BigDecimal("0.0");
	BigDecimal monto_dolar=new BigDecimal("0.0");
	int totdoc_mn=0;
	int totdoc_dolar=0;
	String csCaracterEspecial = "N";

	try{
		con = new AccesoDB();
		con.conexionDB();

		int hayreg=0;
		String query = 
            " SELECT COUNT (1), 'CargaDocDistBean::procesoCargaDoctosWS()' origenquery "   +
            " FROM "+disEnlace.getTablaDocumentos()+" "   +
            disEnlace.getCondicionQuery();
        log.debug("query: " + query);
        rs = con.queryDB(query);
		if (rs.next()) {
			hayreg = rs.getInt(1);
		}
		rs.close(); con.cierraStatement();
        log.debug("procesoCargaDoctosWS::hayreg("+hayreg+")");
        
		if(hayreg > 0) {

			Acuse acuse=new Acuse(Acuse.ACUSE_EPO);
			AcuseEnl	acuEnl	= new AcuseEnl();
			acuEnl.setCcAcuse(acuse.toString());

			String ic_epo="";
			String df_fecha_venc="";
			String df_fecha_emision="";
			String ig_numero_docto="";
			String ic_moneda="";
			String fn_monto="";
			int linea =0;
			Vector vecCampos;
			String ic_pyme="";
			String ic_tipo_financiamiento="";
			String df_limite_dscto="";
			String ig_plazo_descuento="";
			String fn_porc_descuento="";
			String referencia="";
			String ic_linea_credito_dm="";
			String cs_plazo_descuento="";
			String ic_clasificacion="";
			String cg_ventacartera="";
			String negociable="";
			String cd1,cd2,cd3,cd4,cd5;

            String cg_num_distribuidor = "";
            String operaVentaCartera = "";
            String tipo_Pago ="",periodo ="";  
			
			ParametrosDist BeanParametros = ServiceLocator.getInstance().lookup("ParametrosDistEJB",ParametrosDist.class);
			NafinetException nafException = ServiceLocator.getInstance().lookup("NafinetExceptionEJB",NafinetException.class);

            String DescuentoAutomatico = BeanParametros.DesAutomaticoEpo(disEnlace.getIc_epo(),"PUB_EPO_DESC_AUTOMATICO" );
            String limiteLineaCredito = BeanParametros.DesAutomaticoEpo(disEnlace.getIc_epo(),"PUB_LIMITE_LINEA_CREDITO");//Fodea 029-2010 Distribuodores Fase III
            String TipoCredito = BeanParametros.obtieneTipoCredito (disEnlace.getIc_epo()); //Fodea 029-2010 Distribuodores Fase III
            String ventaCartera = BeanParametros.DesAutomaticoEpo(disEnlace.getIc_epo(),"PUB_EPO_VENTA_CARTERA");
            String meseSinIntereses = BeanParametros.DesAutomaticoEpo(disEnlace.getIc_epo(),"CS_MESES_SIN_INTERESES"); // Fodea 09-2015
            Vector vecParametros = BeanParametros.getParamEpoa(disEnlace.getIc_epo()); // Fodea 09-2015 
            String cmbMeses = (vecParametros.get(16)==null)?"": vecParametros.get(16).toString().trim(); // Fodea 09-2015
            String epo_NOnegociable = DesAutomaticoEpo(disEnlace.getIc_epo(),"PUB_EPO_NO_NEGOCIABLES");
		    String epo_tipoCredito = this.obtieneTipoCredito(disEnlace.getIc_epo());
            
			if(ventaCartera.equals("S")) {
				operaVentaCartera = "S";
			}
            
			String cg_fecha_porc_desc = this.getParamFechaPorc(disEnlace.getIc_epo());
			//String lineaCredito = this.validaMontolineaCredito(disEnlace.getIc_epo()); //Fodea 029-2010 Distribuidores Fase III
            
			String noUsuario = "enlac_aut";
			String noRecibo = "";
			String cargaEnlace = "";
			if(disEnlace instanceof WSEnlace) {
				noUsuario 	= ((WSEnlace)disEnlace).getIc_usuario();
				noRecibo 	= ((WSEnlace)disEnlace).getCg_receipt();
				cargaEnlace = "S";
			}
            
            PreparedStatement ps = null;
            qrySentencia = 
                " insert into dis_documento"+
                "(ic_documento,ic_epo,ic_pyme,ic_producto_nafin,cc_acuse,ig_numero_docto"+
                " ,df_fecha_emision,df_fecha_venc,ic_moneda,ct_referencia,fn_porc_descuento"+
                " ,ig_plazo_descuento,fn_monto,ic_tipo_financiamiento,ic_estatus_docto"+
                " ,ic_linea_credito_dm"+
                " ,cg_campo1,cg_campo2,cg_campo3,cg_campo4,cg_campo5,CS_PLAZO_DESCUENTO,ic_clasificacion, CG_VENTACARTERA,  ig_tipo_pago, IG_MESES_INTERES )"+
                "VALUES (?,?,?,?,?,?,to_date(?,'dd/mm/yyyy'),to_date(?,'dd/mm/yyyy'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
			ps = con.queryPrecompilado(qrySentencia);
			// Inserta los datos del Acuse.
			acuEnl.setCcAcuse(acuse.toString());
			int totdocmn=0, totdocdol=0, itotacepmn=0, itotrechmn=0, itotacepdol=0, itotrechdol=0;
			BigDecimal mtodocmn=new BigDecimal(0.0); BigDecimal mtodocdol=new BigDecimal(0.0);
			query =
				" INSERT INTO com_acuse1"   +
				"             (cc_acuse, in_total_docto_mn, fn_total_monto_mn,"   +
				"              df_fechahora_carga, in_total_docto_dl, fn_total_monto_dl,"   +
				"              ic_usuario, cg_recibo_electronico, cs_carga_ws)"   +
				"      VALUES ('"+acuEnl.getCcAcuse()+"', "+acuEnl.getInTotalDoctoMn()+", "+acuEnl.getFnTotalMontoMn()+","   +
				"              SYSDATE, "+acuEnl.getInTotalDoctoDl()+", "+acuEnl.getFnTotalMontoDl()+","   +
				"              '"+noUsuario+"', '"+noRecibo+"', '"+cargaEnlace+"')"  ;
			log.debug(query);

			try {
				con.ejecutaSQL(query);
			} catch(Exception e) {
				sblog.append("Error al insertar a com_acuse1.\n");
			}
			//if(!con.ejecutaSQL(query)) { log.error("Error al insertar a com_acuse1.\n"); }

			try {
				query =
					" DELETE "+disEnlace.getTablaAcuse() +
					disEnlace.getCondicionQuery();

				log.debug(query);
				con.ejecutaSQL(query);


				if(disEnlace instanceof WSEnlace) {
					query =
						" DELETE "+disEnlace.getTablaErrores()+
						"  WHERE cg_receipt in (SELECT cg_receipt FROM "+disEnlace.getTablaAcuse()+
						" WHERE df_fechahora_carga < trunc(SYSDATE))";
					log.debug(query);
					con.ejecutaSQL(query);
					
					query =
						" DELETE "+disEnlace.getTablaDocumentos()+
						"  WHERE cg_receipt in (SELECT cg_receipt FROM "+disEnlace.getTablaAcuse()+
						" WHERE df_fechahora_carga < trunc(SYSDATE))";
					log.debug(query);
					con.ejecutaSQL(query);
					
					query =
						" DELETE "+disEnlace.getTablaAcuse()+
						"  WHERE df_fechahora_carga < TRUNC(SYSDATE)";
					log.debug(query);
					con.ejecutaSQL(query);
				}

			}catch(Exception e){
				sblog.append("Error al borrar a "+disEnlace.getTablaAcuse()+".\n");
			}


			query = disEnlace.getInsertAcuse(acuEnl);
			try{
				con.ejecutaSQL(query);
			}catch(Exception e){
				sblog.append("Error al insertar a "+disEnlace.getTablaAcuse()+".\n");
			}

			///////********************* GENERAL *************************////////

			String queri = disEnlace.getDocuments() ;
			log.debug(queri);
			rs = con.queryDB(queri);
			ic_epo=disEnlace.getIc_epo();

			//Vector vecFilas = this.getCamposAdicionales(ic_epo,false);
			Vector vecCamposAd = getCamposAdicionales(ic_epo, false);
            
			int iTotalError = 0;
			while(rs.next()) {
                //regErroneos = false;
                linea++;
                log.trace("linea++    =============    " + linea);
                resultado =  true;
                //ic_epo = rs.getString("ic_epo").trim()                
                cg_num_distribuidor = rs.getString("CG_NUM_DISTRIBUIDOR")==null?"":rs.getString("CG_NUM_DISTRIBUIDOR").trim();
                ig_numero_docto= rs.getString("IG_NUMERO_DOCTO")==null?"":rs.getString("IG_NUMERO_DOCTO").trim();
                df_fecha_emision= rs.getString("DF_FECHA_EMISION")==null?"":rs.getString("DF_FECHA_EMISION").trim();
                df_fecha_venc = rs.getString("DF_FECHA_VENC")==null?"":rs.getString("DF_FECHA_VENC").trim();
                ic_moneda= rs.getString("IC_MONEDA")==null?"":rs.getString("IC_MONEDA").trim();
                fn_monto= rs.getString("FN_MONTO")==null?"":rs.getString("FN_MONTO").trim();
                ic_tipo_financiamiento= rs.getString("IC_TIPO_FINANCIAMIENTO")==null?"":rs.getString("IC_TIPO_FINANCIAMIENTO").trim();
                ic_clasificacion = rs.getString("IC_CLASIFICACION")==null?"":rs.getString("IC_CLASIFICACION").trim();
                ig_plazo_descuento= rs.getString("IG_PLAZO_DESCUENTO")==null?"":rs.getString("IG_PLAZO_DESCUENTO").trim();
                df_limite_dscto= rs.getString("DF_FECHA_VENC_CREDITO")==null?"":rs.getString("DF_FECHA_VENC_CREDITO").trim();
                fn_porc_descuento= rs.getString("FN_PORC_DESCUENTO")==null?"":rs.getString("FN_PORC_DESCUENTO").trim();
                ic_linea_credito_dm = rs.getString("ic_linea_credito_dm")==null?"":rs.getString("ic_linea_credito_dm").trim();
                referencia = rs.getString("CT_REFERENCIA")==null?"":rs.getString("CT_REFERENCIA");
                cd1 = (rs.getString( "cg_campo1")==null)?"":rs.getString( "cg_campo1");
                cd2 = (rs.getString("cg_campo2")==null)?"":rs.getString("cg_campo2");
                cd3 = (rs.getString("cg_campo3")==null)?"":rs.getString("cg_campo3");
                cd4 = (rs.getString("cg_campo4")==null)?"":rs.getString("cg_campo4");
                cd5 = (rs.getString("cg_campo5")==null)?"":rs.getString("cg_campo5");
                NOnegociable = rs.getString("CS_NEGOCIABLE")==null?"":rs.getString("CS_NEGOCIABLE");
                tipo_Pago = rs.getString("ig_tipo_pago")==null?"":rs.getString("ig_tipo_pago");
                periodo = rs.getString("IG_MESES_INTERES")==null?"":rs.getString("IG_MESES_INTERES");			            
                //pipesporlinea= rs.getString(1).trim();
                String		fin_selec	= ic_tipo_financiamiento;
                //ic_pyme= rs.getString("ic_pyme")==null?"":rs.getString("ic_pyme").trim();
                //cs_plazo_descuento = rs.getString("cs_plazo_descuento")==null?"":rs.getString("cs_plazo_descuento").trim();
                //cg_ventacartera= rs.getString("cg_ventacartera")==null?"":rs.getString("cg_ventacartera").trim();
                vecCampos = new Vector();
                vecCampos.addElement(cd1);
                vecCampos.addElement(cd2);
                vecCampos.addElement(cd3);
                vecCampos.addElement(cd4);
                vecCampos.addElement(cd5);

                if(!fn_monto.equals("") ) {
                    fn_monto2 =Double.parseDouble(fn_monto);
                }


			//INICIAN VALIDADCIONES------------------------------------------------------------------------------------------
			//Realiza el proceso del archivo desde el EJB
			String estatusNegociable = "2";
			int moneda=0;
			//fodea 029-2010
			if (NOnegociable.equalsIgnoreCase("S") ) {
				estatusNegociable = "2";
			}else  if (NOnegociable.equalsIgnoreCase("N")) {
				estatusNegociable = "1";
			}
			if (NOnegociable.equalsIgnoreCase("") ) {
				NOnegociable="NS";
			}

			negociable = NOnegociable;

			/*Fodea 014-2010 Distrbuidores Fase II ini */
			if ("1".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Pronto Pago";     }
			if ("2".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Ampliado"; }
			if ("3".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Mixto";     }
			if ("4".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Pronto Pago Fijo"; }
			if ("5".equals(ic_tipo_financiamiento) ) { NomFinancimiento = "Plazo Ampliado Pemex"; }


			if(disEnlace instanceof WSEnlace) {
				csCaracterEspecial = rs.getString("cs_caracter_especial");
				if("S".equals(csCaracterEspecial)){
					error.append("Se detectaron caracteres de control no permitidos (excluyendo tabulador (\\t), salto de l�nea (\\n) y retorno de carro (\\r))\n");
					msgError = "Se detectaron caracteres de control no permitidos (excluyendo tabulador (\\t), salto de l�nea (\\n) y retorno de carro (\\r))\n";
					noError.append("45\n");
					strError = "45";
					resultado =  false;
					regErroneos = true;
				}
			}
			
			if (!ic_tipo_financiamiento.equals("") && Comunes.esNumero(ic_tipo_financiamiento)){
				qryModalidadPlazo =
                    "SELECT f.ic_tipo_financiamiento "+
                    "FROM   comrel_epo_tipo_financiamiento f, "+
                    "       comrel_producto_epo e "+
                    "WHERE  f.ic_producto_nafin = ? "+
                    "       AND f.ic_epo = ? "+
                    "       AND f.ic_tipo_financiamiento = ? "+
                    "       AND e.ic_epo = f.ic_epo "+
                    "       AND e.ic_producto_nafin = ? ";
                ps2 = con.queryPrecompilado(qryModalidadPlazo);
                ps2.setInt(1, Integer.valueOf(4));
                ps2.setInt(2, Integer.valueOf(ic_epo));
                ps2.setInt(3, Integer.valueOf(ic_tipo_financiamiento));
                ps2.setInt(4, Integer.valueOf(4));
                rs2 = ps2.executeQuery();
                ps2.clearParameters();                
                while(rs2.next()) {
                    valorFinanciamiento = rs2.getString("ic_tipo_financiamiento");
                }
                rs2.close();ps2.close();

				if (!ic_tipo_financiamiento.equals(valorFinanciamiento) ){
					error.append("Error en la linea "+linea+",La Epo No opera el tipo de Modalidad de  "+NomFinancimiento+" \n");
					msgError = "Error en la linea "+linea+",La Epo No opera el tipo de Modalidad de  "+NomFinancimiento+" \n";
					noError.append("3\n");
					strError = "3";
					resultado =  false;
					regErroneos = true;
				}
         }
        /*Fodea 014-2010 Distrbuidores Fase II  */

        //Fodea  29-2010 Distribuidores Fase III inicia*/
			 if ( TipoCredito.equals("D")&& ventaCartera.equals("S") ) {
					validaLinea = validaLineCredito(ic_epo);
					if(validaLinea.equals("") || validaLinea == null || validaLinea.equals("null")) {
						error.append("Error en la linea "+linea+", La Epo no tiene linea de Credito\n");
						msgError = "Error en la linea "+linea+", La Epo no tiene linea de Credito";
						noError.append("22\n");
						strError = "22";
						resultado =  false;
						regErroneos = true;
					}
			 }


      if ((DescuentoAutomatico.equals("S") &&  limiteLineaCredito.equals("S") &&  TipoCredito.equals("C"))||(DescuentoAutomatico.equals("S") &&  limiteLineaCredito.equals("S") &&  TipoCredito.equals("D")&& ventaCartera.equals("S"))) {
            MontoLineaCredito = montoLineaCreCaptura(ic_epo);
            MontoLineaCredito2 = Double.parseDouble(MontoLineaCredito);
            log.debug("fn_monto   "+fn_monto2+" MontoLineaCredito   "+MontoLineaCredito2);

         if(!fn_monto.equals("") ) {
				porLineaCredito =  (fn_monto2 * 100)/MontoLineaCredito2;
           if (fn_monto2 > MontoLineaCredito2){
            error.append("Error en la linea "+linea+", El monto capturado rebasa el l�mite de su l�nea de cr�dito. Porcentaje Utilizado "+Comunes.formatoDecimal(porLineaCredito,2)+" %\n");
				msgError = "Error en la linea "+linea+", El monto capturado rebasa el l�mite de su l�nea de cr�dito. Porcentaje Utilizado "+Comunes.formatoDecimal(porLineaCredito,2)+" %";
				noError.append("10\n");
				strError = "10";
            resultado =  false;
				regErroneos = true;
					}
			}
		}

		if(negociable.equals("NS")){
			error.append("Error en la linea "+linea+", El valor de Negociable no puede estar vacio\n");
			msgError = "Error en la linea "+linea+", El valor de Negociable no puede estar vacio";
			noError.append("8\n");
			strError = "8";
			resultado =  false;
			regErroneos = true;
		}

		if(!negociable.equals("NS") ){
            NOnegociable = epo_NOnegociable;
			if( NOnegociable.equals("N") && negociable.equals("N")) {
                error.append("Error en la linea "+linea+", La EPO no se encuentra parametrizada para publicar documentos No Negociables\n");
                msgError = "Error en la linea "+linea+", La EPO no se encuentra parametrizada para publicar documentos No Negociables";
                noError.append("9\n");
                strError = "9";
                resultado =  false;
                regErroneos = true;
			}
		}
                
        //Fodea  29-2010 Distribuidores Fase III final*/
        /*
         * se elimina acceso a la base para evitar consultas adicionales (IHJ)
        
        qrySentencia = 	
            " select 1 from comrel_epo_tipo_financiamiento " +
            " where ic_tipo_financiamiento = ?" +
            " and ic_epo = ?";
        ResultSet rs3 = null;
        log.debug("\n qrySentencia: "+qrySentencia);
        ps2 = con.queryPrecompilado(qrySentencia);
        ps2.setInt(1, Integer.valueOf(5));
        ps2.setInt(2, Integer.valueOf(ic_epo));
        rs3 = ps.executeQuery();
        ps2.clearParameters();
        if(rs3.next())
            Parametr = "1";
        ps2.close();rs3.close();
        
        */
                if("5".equals(valorFinanciamiento)) {
                    Parametr = "1";
                }
                
            if(!("".equals(ic_tipo_financiamiento)||"1".equals(ic_tipo_financiamiento)||"2".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento)||"1".equals(Parametr))){
					error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("DIST0009", "ES")+"\n");
					msgError = nafException.getMensajeError("DIST0009", "ES");
					noError.append("4\n");
					strError = "4";
					resultado =  false;
					regErroneos = true;
					//throw new NafinException("DIST0009");
				}
            //int
			moneda=0;

			if(!"".equals(ic_moneda)&&ic_moneda!=null) {
				if(Comunes.esNumero(ic_moneda)==false){
                    error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0014", "ES")+"\n");
                    msgError = nafException.getMensajeError("GRAL0014", "ES");
                    noError.append("GRAL0014\n");
                    strError = "30";
                    resultado =  false;
                    regErroneos = true;
                    //throw new NafinException("GRAL0014");
				} else {
					moneda=Integer.parseInt(ic_moneda);
					if(fn_monto == null || "".equals(fn_monto)){
							error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0011", "ES")+"\n");
							msgError = nafException.getMensajeError("GRAL0011", "ES");
							noError.append("GRAL0011\n");
							strError = "31";
							resultado =  false;
							regErroneos = true;
					      //throw new NafinException("GRAL0011");
					}
					else if(!Comunes.esDecimal(fn_monto)){
			            error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0012", "ES")+"\n");
							msgError = nafException.getMensajeError("GRAL0012", "ES");
							noError.append("GRAL0012\n");
							strError = "32";
							resultado =  false;
							regErroneos = true;
							//throw new NafinException("GRAL0012");
					}
					else if(Double.parseDouble(fn_monto)<=0){
						error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0019", "ES")+"\n");
						msgError = nafException.getMensajeError("GRAL0019", "ES");
						noError.append("GRAL0019\n");
						strError = "33";
						resultado =  false;
						regErroneos = true;
						//throw new NafinException("GRAL0019");
					}

					if((moneda != 1) && (moneda != 54)){
		            error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0015", "ES")+"\n");
						msgError = nafException.getMensajeError("GRAL0015", "ES");
						noError.append("GRAL0015\n");
						strError = "34";
						resultado =  false;
						regErroneos = true;
						//        throw new NafinException("GRAL0015");
					}
					else {
						if(fn_monto == null || "".equals(fn_monto))
							fn_monto = "0";
						monto = new BigDecimal(fn_monto);
						if(moneda == 1) {	//Si es Moneda Nacional

						monto_mn = monto_mn.add(monto);
						totdoc_mn++;
						}
						if(moneda == 54) {	//Si son Dolares.

							monto_dolar = monto_dolar.add(monto);
							totdoc_dolar++;
						}
					}   //fin else
				} // fin else           (TIPO DE MONEDA)
			} else{
                error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0013", "ES")+"\n");
                msgError = nafException.getMensajeError("GRAL0013", "ES");
                noError.append("GRAL0013\n");
                strError = "35";
                resultado =  false;
                regErroneos = true;
                //  throw new NafinException("GRAL0013");
			}
          //VALIDAMOS QUE NO SE EXCEDA EL NUMERO DE CAMPOS PERMITIDOS
            int numCamposAd = vecCampos.size();
            for(int i=0;i<vecCampos.size();i++){
                if(!"".equals((String)vecCampos.get(i))&&(i>=numCamposAd)) {
                    error.append("Error en la linea "+linea+", El Campo Din�mico No."+(i+1)+" no est� definido por la EPO\n");
                    msgError = "El Campo Din�mico No."+(i+1)+" no est� definido por la EPO";
                    noError.append("5\n");
                    strError = "5";
                    resultado =false;
                    regErroneos = true;
                }
            }
                
            //Vector vecCamposAd = getCamposAdicionales(ic_epo,false);
                
            for(int i=0;i<vecCamposAd.size();i++) {
                vecColumnas = (Vector)vecCamposAd.get(i);
                if("N".equals((String)vecColumnas.get(2))&&!"".equals((String)vecCampos.get(i))&&vecCampos.get(i)!=null) {
                    if(!Comunes.esNumero((String)vecCampos.get(i))) {
                        error.append("Error en la linea "+linea+", El Campo Din�mico No."+(i+1)+" debe de ser Num�rico\n");
                        msgError = "El Campo Din�mico No."+(i+1)+" debe de ser Num�rico";
                        noError.append("6\n");
                        strError = "6";
                        resultado = false;
                        regErroneos = true;
                    }
                }
                if(Integer.parseInt((String)vecColumnas.get(3)) < vecCampos.get(i).toString().length()) {
                    error.append("Error en la linea "+linea+", El Campo Din�mico No."+(i+1)+" excede la longitud definida por su epo\n");
                    msgError = "El Campo Din�mico No."+(i+1)+" excede la longitud definida por su epo";
                    noError.append("7\n");
                    strError = "7";
                    resultado = false;
                    regErroneos = true;
                }
            }

          //Validamos que los campos definidos por la epo esten capturados
       /* pipesporlinea = pipesporlinea -6;
        if(numCamposAd > pipesporlinea)
          {
          msgError = "Faltan Campos";
          resultado = false;
          }*/

            if("".equals(ig_numero_docto)||ig_numero_docto==null){
					//error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("ANTI0013", "ES")+"\n");
					error.append("Error en la linea "+linea+", No debe dejar el campo en blanco. Por favor capture el N�m. Documento inicial\n");
					msgError = nafException.getMensajeError("ANTI0013", "ES");
					noError.append("ANTI0013\n");
					strError = "36";
					resultado =  false;
					regErroneos = true;
					// throw new NafinException("ANTI0013");
				}
            if("".equals(df_fecha_emision) || df_fecha_emision ==null){
              error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("ANTI0014", "ES")+"\n");
				  msgError = nafException.getMensajeError("ANTI0014", "ES");
				  noError.append("ANTI0014\n");
				  strError = "37";
					resultado =  false;
					regErroneos = true;
				  //throw new NafinException("ANTI0014");
				}
            if(!Comunes.checaFecha(df_fecha_emision)){
             error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0010", "ES")+"\n");
				 msgError = nafException.getMensajeError("GRAL0010", "ES");
				 noError.append("GRAL0010\n");
				 strError = "38";
				 resultado =  false;
				 regErroneos = true;
				 // throw new NafinException("GRAL0010");
				}
            if("".equals(df_fecha_venc) || df_fecha_venc ==null){
              error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("ANTI0015", "ES")+"\n");
				  msgError = nafException.getMensajeError("ANTI0015", "ES");
				  noError.append("ANTI0015\n");
				  strError = "39";
				 resultado =  false;
				 regErroneos = true;
				  //throw new NafinException("ANTI0015");
				}
            if(!Comunes.checaFecha(df_fecha_venc)){
                error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0010", "ES")+"\n");
                msgError = nafException.getMensajeError("GRAL0010", "ES");
                noError.append("GRAL0010\n");
                strError = "40";
                resultado =  false;
                regErroneos = true;
                //throw new NafinException("GRAL0010");
            }
            if(!"".equals(df_limite_dscto))
            	if(!Comunes.checaFecha(df_limite_dscto)){
						error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0010", "ES")+"\n");
						msgError = nafException.getMensajeError("GRAL0010", "ES");
						noError.append("GRAL0010\n");
						strError = "40";
						resultado =  false;
						regErroneos = true;
				//		throw new NafinException("GRAL0010");
					}
            //if(pipesporlinea<0)
              //throw new NafinException("GRAL0016");
			if("".equals(cg_num_distribuidor) || cg_num_distribuidor==null){
				error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("GRAL0017", "ES")+"\n");
				msgError = nafException.getMensajeError("GRAL0017", "ES");
				noError.append("GRAL0017\n");
				strError = "GRAL0017";
				//strError = "41";
				resultado =  false;
				regErroneos = true;
            //  throw new NafinException("GRAL0017");
			}
			if(!"".equals(ic_clasificacion) && (!"".equals(ig_plazo_descuento) || !"".equals(fn_porc_descuento) || !"".equals(df_limite_dscto))){
					error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("DIST0032", "ES")+"\n");
					msgError = nafException.getMensajeError("DIST0032", "ES");
					noError.append("DIST0032\n");
					strError = "42";
					strError = "";
					resultado =  false;
					regErroneos = true;
					//throw new NafinException("DIST0032");
			}
			if(!"".equals(ig_plazo_descuento) && "".equals(fn_porc_descuento)){
					error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("DIST0033", "ES")+"\n");
					msgError = nafException.getMensajeError("DIST0033", "ES");
					noError.append("DIST0033\n");
					strError = "43";
					resultado =  false;
					regErroneos = true;
					//throw new NafinException("DIST0033");
			}
			if("".equals(ig_plazo_descuento) && !"".equals(fn_porc_descuento)){
				error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("DIST0033", "ES")+"\n");
				msgError = nafException.getMensajeError("DIST0033", "ES");
				noError.append("DIST0033\n");
				strError = "44";
				resultado =  false;
				regErroneos = true;
					//throw new NafinException("DIST0033");
			}

		/*Para Validar la fecha de Emision	Fodea 029-2010 Distribuidores Fase III	 */
		/*
		Calendar gcDiaFecha = new GregorianCalendar();
		java.util.Date fechaEmisionNvo = Comunes.parseDate(df_fecha_emision);
		gcDiaFecha.setTime(fechaEmisionNvo);
		no_dia_semana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);
		inhabilEpo = getDiasInhabilesDes(ic_epo, df_fecha_emision);

		fechaDeHoy 	= Fecha.getFechaActual();

		log.debug("no_dia_semana   " +no_dia_semana+"   inhabilEpo  "+inhabilEpo);

		SimpleDateFormat formatEntrada = new SimpleDateFormat("dd/MM/yyyy");
		java.util.Date df_fecha_emision2 = formatEntrada.parse(df_fecha_emision);
		java.util.Date  fechaDeHoy2 = Comunes.parseDate(fechaDeHoy);

	if ( TipoCredito.equals("D") && ventaCartera.equals("S") &&  DescuentoAutomatico.equals("S")) {
		if ( df_fecha_emision2.compareTo(fechaDeHoy2)==0  || df_fecha_emision2.compareTo(fechaDeHoy2)<0
			|| no_dia_semana==7 ||  no_dia_semana==1 || !inhabilEpo.equals("") )  {
			error.append("Error en la linea "+linea+", La Fecha de Emisi�n debe ser igual o mayor a la fecha del siguiente d�a h�bil  "+df_fecha_emision+" \n");
			msgError = "Error en la linea "+linea+",La Fecha de Emisi�n debe ser igual o mayor a la fecha del siguiente d�a h�bil  "+df_fecha_emision+" \n";
			noError.append("6\n");
			strError = "6";
			resultado =  false;
			regErroneos = true;
		}
	}
	*/


	if(this.existeDoctoWS(ic_epo,ig_numero_docto,con)) {
		error.append("Error en la linea "+linea+", El Numero de Documento ya existe \n");
		msgError = "Error en la linea "+linea+",El Numero de Documento ya existe \n";
		noError.append("2\n");
		strError = "2";
		resultado =  false;
		regErroneos = true;
	}//else{
        
		String		arreglo[]	= new String[2];
		String 		validaDist 	= "";
		String 		acumulado	= "";
		String 		validaSaldo = "";
                
		try{
			arreglo = validaRelPymeEpo(ic_epo,cg_num_distribuidor,con);
			ic_pyme = arreglo[0];
		} catch(NafinException ne) {
			ic_pyme = "0";
			error.append("Error en la linea "+linea+", "+ nafException.getMensajeError("ANTI0010", "ES")+"\n");
			msgError  = "Error en la linea "+linea+", "+ nafException.getMensajeError("ANTI0010", "ES");
			noError.append("1\n");
			strError = "1";
			resultado =  false;
			regErroneos = true;
		}

		if(resultado) {
			try{
				validaFechas(df_fecha_venc,df_fecha_emision,ic_epo,ic_pyme,con);
			} catch(NafinException ne ) {
				error.append("Error en la linea "+linea+", "+ ne.getMsgError()+"\n");
				msgError  = "Error en la linea "+linea+", "+ ne.getMsgError();
				noError.append(ne.getCodError()+"\n");
				strError = "1";
				resultado =  false;
				regErroneos = true;
			}
            
			//String tipoCredito = this.obtieneTipoCredito(ic_epo);
			String tipoCredito = epo_tipoCredito;
			if(!tipoCredito.equals("F") ){
				ic_tipo_financiamiento = getTipoFinDefault(ic_epo,ic_tipo_financiamiento,con);
			}

				if(!"".equals(ic_clasificacion) || ("".equals(ic_clasificacion) && "".equals(ig_plazo_descuento) && "".equals(fn_porc_descuento)))
                log.trace("ic_epo ==== "+ ic_epo);
                log.trace("ic_clasificacion ==== "+ ic_clasificacion);
                log.trace("ic_pyme ==== "+ ic_pyme);
                ic_clasificacion = getCategoria(ic_epo,"4",ic_clasificacion,ic_pyme);
                    
				if("1".equals(ic_tipo_financiamiento)||"3".equals(ic_tipo_financiamiento)||"4".equals(ic_tipo_financiamiento)) {
					if(!"".equals(ig_plazo_descuento) || !"".equals(df_limite_dscto)){
                        ig_plazo_descuento = validaFechaLimite(df_fecha_emision,ig_plazo_descuento,df_limite_dscto, cg_fecha_porc_desc, con);
                        log.debug("primer metodo validaFechaLimite::ig_plazo_descuento == "+ig_plazo_descuento);
					}

					Vector vectoraux = new Vector();
					String aux_ig_plazo_dscto	= "";
					String aux_fn_porc_dscto	= "";
					String aux_tipo_param		= "";

					log.trace("ic_clasificacion ==== "+ ic_clasificacion);
					log.trace("cg_fecha_porc_desc ==== "+ cg_fecha_porc_desc);
					vectoraux = getParametrosClasific(ic_epo,ic_pyme,df_fecha_emision,cg_fecha_porc_desc,ic_clasificacion,con);

					if("".equals(ic_clasificacion))
						ic_clasificacion = "NULL";

					String rs_plazo_dscto	= (String)vectoraux.get(0);
					String rs_porc_dscto	= (String)vectoraux.get(1);
					String rs_tipo_param	= (String)vectoraux.get(2);
					if("E".equals(cg_fecha_porc_desc) && !"NULL".equals(ic_clasificacion)){
						String rs_plazo_dscto_max	= "";
						if(vectoraux.size()>3)
							rs_plazo_dscto_max = (String)vectoraux.get(3);
						if("".equals(rs_plazo_dscto_max))
							rs_plazo_dscto_max = rs_plazo_dscto;

						int restaFechas = getRestaFechas(df_fecha_emision,rs_plazo_dscto_max);
						if(restaFechas<0) {
							ig_plazo_descuento = "0";
							fn_porc_descuento = "0";
						}
					}

					if("".equals(fn_porc_descuento)) {
						aux_ig_plazo_dscto = rs_plazo_dscto;
						aux_tipo_param = rs_tipo_param;
					}
					else {
						aux_ig_plazo_dscto = ig_plazo_descuento;
						if(ig_plazo_descuento.equals(rs_plazo_dscto))
							aux_tipo_param = rs_tipo_param;
						else
							aux_tipo_param = "U";
					}

					if("".equals(fn_porc_descuento)) {
						aux_fn_porc_dscto = rs_porc_dscto;
						if(!"U".equals(aux_tipo_param))
							aux_tipo_param = rs_tipo_param;
					}
					else {
						aux_fn_porc_dscto	= fn_porc_descuento;
						if(fn_porc_descuento.equals(rs_porc_dscto)) {
							if(!"U".equals(aux_tipo_param))
								aux_tipo_param = rs_tipo_param;
						}
						else
							aux_tipo_param = "U";
					}

					ig_plazo_descuento = getPlazoFinDefault(ic_epo,ic_tipo_financiamiento,aux_ig_plazo_dscto,con);
					fn_porc_descuento =  getPorcFinDefault(ic_epo,ic_tipo_financiamiento,aux_fn_porc_dscto,con);
					log.trace("primer metodo ig_plazo_descuento::ig_plazo_descuento == "+ig_plazo_descuento);

					if(!aux_ig_plazo_dscto.equals(ig_plazo_descuento))
							cs_plazo_descuento = "N";
					else
						if(!aux_fn_porc_dscto.equals(fn_porc_descuento))
							cs_plazo_descuento = "N";
						else
							if("G".equals(aux_tipo_param) || "U".equals(aux_tipo_param))
								cs_plazo_descuento = "N";
							else
								if("NULL".equals(ic_clasificacion) || "0".equals(ic_clasificacion))
									cs_plazo_descuento = "N";
					/*Validacion de fecha de publicacion*/
					if(resultado){
						resultado = validaPlazoDescto(ig_plazo_descuento,df_fecha_venc,df_fecha_emision,cg_fecha_porc_desc,con);
						if(!resultado){
							msgError = "";
							error.append("Error en la linea "+linea+", El plazo para descuento no puede ser mayor a la Fecha de vencimiento del documento\n");
							msgError  = "Error en la linea "+linea+", El plazo para descuento no puede ser mayor a la Fecha de vencimiento del documento";
							noError.append("10\n");
						}
					}
				}
				else {
						ig_plazo_descuento = "";
						 fn_porc_descuento = "";
						 cs_plazo_descuento = "N";
				}
				if(!"D".equals(arreglo[1])){
					ic_linea_credito_dm = "";
				}
				/*else{
					if(ic_linea_credito_dm!=null&&!"".equals(ic_linea_credito_dm)){
						if(!Comunes.esNumero(ic_linea_credito_dm))
							throw new NafinException("DIST0025");
						validaLineaCredito(ic_epo,ic_moneda,ic_linea_credito_dm,con);
					}else{
						ic_linea_credito_dm = "null";
					}
				}*/

				if("D".equals(arreglo[1]) && ic_linea_credito_dm.equals("")){
					ResultSet rs1 = null;
					PreparedStatement ps1 = null;
					qrySentencia = 	
                        " SELECT ic_linea_credito_dm "+
                        " FROM DIS_LINEA_CREDITO_DM "+
                        " WHERE  ic_epo = ? "+
                        " AND ic_producto_nafin =  ? ";
					ps1 = con.queryPrecompilado(qrySentencia);
					ps1.setInt(1,Integer.parseInt(ic_epo));
					ps1.setInt(2,4);
					rs1 = ps1.executeQuery();
					ps1.clearParameters();
					log.debug("qrySentencia----> "+qrySentencia);
					log.trace("ic_epo"+ic_epo);
					if(rs1.next())	{
						ic_linea_credito_dm = rs1.getString("ic_linea_credito_dm")==null?"":rs1.getString("ic_linea_credito_dm");
					}
					rs1.close();ps1.close();
				}
				if(ic_linea_credito_dm.equals("")){
					ic_linea_credito_dm ="";
				}
				log.trace("ic_linea_credito_dm"+ic_linea_credito_dm);
				log.trace("ic_clasificacion = " + ic_clasificacion);

			}

					log.trace(" meseSinIntereses: " + meseSinIntereses);
					log.trace(" tipo_Pago:---" + tipo_Pago + "--");
					log.trace(" ic_tipo_financiamiento:---" + ic_tipo_financiamiento + "--");
					log.trace(" periodo:---" + periodo + "--");

					//Fodea 09-2015 (E)
					if (meseSinIntereses.equals("S")) {

						if ("".equals(tipo_Pago)) {
							error.append("Error en la linea " + linea + ",El campo Tipo de Pago es obligatorio  \n");
							msgError = "El campo Tipo de Pago es obligatorio  \n";
							noError.append("0");
							resultado = false;

						} else if (!"".equals(tipo_Pago)) {
							if (Comunes.esNumero(tipo_Pago) == false) {
								error.append("Error en la linea " + linea + ",El campo Tipo de Pago debe ser num�rico \n");
								msgError = "El campo Tipo de Pago debe ser num�rico \n";
								noError.append("0");
								resultado = false;
							}
							if (!"1".equals(tipo_Pago) && !"2".equals(tipo_Pago)) {
								error.append("Error en la linea " + linea +",El campo Tipo de Pago solo acepta las opciones 1 (Financiamiento con intereses) o 2 (Meses sin intereses). \n");
								msgError = "El campo Tipo de Pago solo acepta las opciones 1 (Financiamiento con intereses) o 2 (Meses sin intereses). \n";
								noError.append("0");
								resultado = false;
							}
						}

						if ("2".equals(tipo_Pago)) {

							if (!"2".equals(ic_tipo_financiamiento)) {

								error.append("Error en la linea " + linea + ", el tipo de Modalidad  " + NomFinancimiento +
												 " no corresponde para los documentos que seran financiados a meses sin intereses  \n");
								msgError = "El tipo de Modalidad  " + NomFinancimiento +
														 " no corresponde para los documentos que seran financiados a meses sin intereses  \n";
								noError.append("0");
								resultado = false;
							}

							if ("".equals(periodo)) {
								error.append("Error en la linea " + linea +
												 ",El campo Periodo es obligatorio si los documentos seran financiados a meses sin intereses  \n");
								msgError = " El campo Periodo es obligatorio si los documentos seran financiados a meses sin intereses  \n";
								noError.append("0");
								resultado = false;
							}

							if (!"".equals(periodo)) {
								if (Comunes.esNumero(periodo) == false) {
									error.append("Error en la linea " + linea + ",El campo Periodo debe ser numerico \n");
									msgError = "El campo Periodo debe ser numerico \n";
									noError.append("0");
									resultado = false;
								}
								if (Integer.parseInt(periodo) < 2) {
									error.append("Error en la linea " + linea +
													 ",El campo Periodo no debe de ser menor a 2 meses \n");
									msgError = " El campo Periodo no debe de ser menor a 2 meses \n";
									noError.append("0");
									resultado = false;
								}
								if (Integer.parseInt(periodo) > Integer.parseInt(cmbMeses)) {
									error.append("Error en la linea " + linea +
													 ",El campo Periodo excede los meses de acuerdo a lo parametrizado \n");
									msgError = " El campo Periodo excede los meses de acuerdo a lo parametrizado \n";
									noError.append("0");
									resultado = false;
								}

							}
						}
					} else if (meseSinIntereses.equals("N")) {
						if (!"".equals(tipo_Pago) || !"".equals(periodo)) {
							error.append("Error en la linea " + linea + ", El layout es incorrecto, por favor verifiquelo \n");
							msgError = "  El layout es incorrecto, por favor verifiquelo \n";
							noError.append("0");
							resultado = false;
						}

					}

					//Fodea 09-2015 (S)
			try{
				if("5".equals(fin_selec)){
						validaDist = validaDistribuidor(ic_epo,cg_num_distribuidor,df_fecha_emision);
					if("S".equals(validaDist))
						  throw new NafinException("DIST0038");
					//acumulado = getMontoAcumulado(ic_epo,cg_num_distribuidor,ic_moneda,ic_proc_docto);
					validaSaldo = validaSaldoDisponible(ic_epo,cg_num_distribuidor,ic_moneda,fn_monto,acumulado);
					if("N".equals(validaSaldo))
						  throw new NafinException("DIST0039");
					if("NL".equals(validaSaldo))
						throw new NafinException("DIST0040");
				}
			}catch(NafinException ne){
				error.append("Error en la linea "+linea+", "+ ne.getMsgError()+"\n");
				msgError  = "Error en la linea "+linea+", "+ ne.getMsgError();
				noError.append(ne.getCodError()+"\n");
				//strError = "1";
				resultado =  false;
				regErroneos = true;
			}

	//}
	
	   
	//FIN DE VALIDACIONES--------------------------------------------------------
			if(moneda == 1) {
				mtodocmn = mtodocmn.add(monto);
				totdocmn++;
				if(resultado) itotacepmn++; else itotrechmn++;
			} else if(moneda == 54) {
				mtodocdol = mtodocdol.add(monto);
				totdocdol++;
				if(resultado) itotacepdol++; else itotrechdol++;
			}

				//acuse = new Acuse(1,"4","com",con);
				//creaAcuse(acuse.toString(),ic_usuario,totalDoctoMN,totalDoctoUSD,totalMontoMN,totalMontoUSD,reciboElectronico,con);
	        //ic_documento = getClaveDocto(con);

		if (resultado){
			  String claveDocto=getClaveDocto(con);

					//resultado
                    log.trace("claveDocto == " + claveDocto);
					log.trace("disEnlace.getIc_epo() == " + disEnlace.getIc_epo());
					log.trace("ic_pyme == " + ic_pyme);
					log.trace("acuEnl.getCcAcuse() == " + acuEnl.getCcAcuse());
					log.trace("ig_numero_docto == " + ig_numero_docto);
					log.trace("df_fecha_emision == " + df_fecha_emision);
					log.trace("df_fecha_venc == " + df_fecha_venc);
					log.trace("Integer.parseInt(ic_moneda) == " + ic_moneda);
					log.trace("referencia == " + referencia);
					log.trace("Double.parseDouble(fn_porc_descuento) == " + fn_porc_descuento);
					log.trace("Integer.parseInt(ig_plazo_descuento) == " + ig_plazo_descuento);
					log.trace("Double.parseDouble(fn_monto) == " + fn_monto);
					log.trace("ic_tipo_financiamiento == " + ic_tipo_financiamiento);
					log.trace("estatusNegociable == " + estatusNegociable);
					log.trace("ic_linea_credito_dm == " + ic_linea_credito_dm);
					log.trace("cd1 == " + cd1);
					log.trace("cd2 == " + cd2);
					log.trace("cd3 == " + cd3);
					log.trace("cd4 == " + cd4);
					log.trace("cd5 == " + cd5);
					log.trace("cs_plazo_descuento == " + cs_plazo_descuento);
					log.trace("ic_clasificacion == " + ic_clasificacion);
					log.trace("cg_ventacartera == " + cg_ventacartera);

					ic_clasificacion = "NULL".equals(ic_clasificacion)?"":ic_clasificacion;

                    qrySentencia = 
                        " insert into dis_documento"+
                        "(ic_documento,ic_epo,ic_pyme,ic_producto_nafin,cc_acuse,ig_numero_docto"+
                        " ,df_fecha_emision,df_fecha_venc,ig_plazo_docto, ic_moneda,ct_referencia,fn_porc_descuento"+
                        " ,ig_plazo_descuento,fn_monto,ic_tipo_financiamiento,ic_estatus_docto"+
                        " ,ic_linea_credito_dm"+
                        " ,cg_campo1,cg_campo2,cg_campo3,cg_campo4,cg_campo5,CS_PLAZO_DESCUENTO,ic_clasificacion, CG_VENTACARTERA, ig_tipo_pago, IG_MESES_INTERES )"+
                        "VALUES (?,?,?,?,?,?,to_date(?,'dd/mm/yyyy'),to_date(?,'dd/mm/yyyy'),ROUND(to_date(?,'dd/mm/yyyy')-to_date(?,'dd/mm/yyyy'),0),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    
					ps = con.queryPrecompilado(qrySentencia);
					ps.setString(	1, claveDocto);
					ps.setString(	2, disEnlace.getIc_epo());
					ps.setString(	3, ic_pyme);
					ps.setString(	4, "4");
					ps.setString(	5, acuEnl.getCcAcuse());
					ps.setString(	6, ig_numero_docto);
					ps.setString(	7, df_fecha_emision);
					ps.setString(	8, df_fecha_venc);
					ps.setString(	9, df_fecha_venc);
					ps.setString(	10, df_fecha_emision);
					ps.setInt(	11,	Integer.parseInt(ic_moneda));

					if (referencia.equals("")) {
						ps.setNull(12, java.sql.Types.VARCHAR);
					} else {
						ps.setString(12, referencia);
					}
					if (fn_porc_descuento.equals("")) {
						ps.setNull(13, java.sql.Types.NUMERIC);
					} else {
						ps.setDouble(13, Double.parseDouble(fn_porc_descuento));
					}
					if (ig_plazo_descuento.equals("")) {
						ps.setNull(14, java.sql.Types.NUMERIC);
					} else {
						ps.setInt(14, Integer.parseInt(ig_plazo_descuento));
					}
					if (fn_monto.equals("")) {
						ps.setNull(15, java.sql.Types.NUMERIC);
					} else {
						ps.setDouble(	15, Double.parseDouble(fn_monto));
					}
					if (ic_tipo_financiamiento.equals("")) {
						ps.setNull(16, java.sql.Types.NUMERIC);
					} else {
						ps.setString(	16, ic_tipo_financiamiento);
					}
					if (estatusNegociable.equals("")) {
						ps.setNull(17, java.sql.Types.VARCHAR);
					} else {
						ps.setString(	17, estatusNegociable);
					}
					if (ic_linea_credito_dm.equals("")) {
						ps.setNull(18, java.sql.Types.NUMERIC);
					} else {
						ps.setString(	18, ic_linea_credito_dm);
					}
					if (cd1.equals("")) {
						ps.setNull(19, java.sql.Types.VARCHAR);
					} else {
						ps.setString(	19, cd1);
					}
					if (cd2.equals("")) {
						ps.setNull(20, java.sql.Types.VARCHAR);
					} else {
						ps.setString(	20, cd2);
					}
					if (cd3.equals("")) {
						ps.setNull(21, java.sql.Types.VARCHAR);
					} else {
						ps.setString(	21, cd3);
					}
					if (cd4.equals("")) {
						ps.setNull(22, java.sql.Types.VARCHAR);
					} else {
						ps.setString(	22, cd4);
					}
					if (cd5.equals("")) {
						ps.setNull(23, java.sql.Types.VARCHAR);
					} else {
						ps.setString(	23, cd5);
					}
					if (cs_plazo_descuento.equals("")) {
						ps.setNull(24, java.sql.Types.VARCHAR);
					} else {
						ps.setString(	24, cs_plazo_descuento);
					}
					if (ic_clasificacion.equals("")) {
						ps.setNull(25, java.sql.Types.NUMERIC);
					} else {
						ps.setString(	25, ic_clasificacion);
					}

					if (cg_ventacartera.equals("")) {
						ps.setNull(26, java.sql.Types.VARCHAR);
					} else {
						ps.setString(	26, cg_ventacartera);
					}

					if (tipo_Pago.equals("")) {
						ps.setNull(27, java.sql.Types.NUMERIC);
					} else {
						ps.setDouble(  27, Integer.parseInt(tipo_Pago));
					}
					
					if ( periodo.equals("") && !tipo_Pago.equals("2") ) {
						ps.setNull(28, java.sql.Types.NUMERIC);
					} else {
						 ps.setDouble(  28, Integer.parseInt(periodo));
					}
		                  
								
					ps.executeUpdate();
					//ps.clearParameters();
					ps.close();
		}
        
		log.trace("resultado ==  "+resultado);
		if(fn_monto!=null && !"".equals(fn_monto) && ic_moneda!=null && !"".equals(ic_moneda))
			acuEnl.add(Double.parseDouble(fn_monto), Integer.parseInt(ic_moneda), resultado, "");
				if (resultado == false){
					//if(regErroneos) {
						String lineaError="";
						String numError = "";


						log.trace("error.toString() ==== "+error.toString());
						log.trace("noError.toString() ==== "+noError.toString());
						st=new StringTokenizer(error.toString(),"\n");
						st2=new StringTokenizer(noError.toString(),"\n");
						cd1 = (cd1.equals("")|| cd1.equals("null"))?"null":"'"+cd1+"'";
						cd2 = (cd2.equals("")|| cd2.equals("null"))?"null":"'"+cd2+"'";
						cd3 = (cd3.equals("")|| cd3.equals("null"))?"null":"'"+cd3+"'";
						cd4 = (cd4.equals("")|| cd4.equals("null"))?"null":"'"+cd4+"'";
						cd5 = (cd5.equals("")|| cd5.equals("null"))?"null":"'"+cd5+"'";

						while(st2.hasMoreTokens()) {
							ErroresEnl err = new ErroresEnl();
							err.setCcAcuse(acuEnl.getCcAcuse());
							numError = st2.nextToken();
							iTotalError++;
							lineaError=st.nextToken().trim();
							lineaError=(lineaError.length()>120)?lineaError.substring(0,120):lineaError.substring(0,lineaError.length());
							err.setIgNumeroDocto(ig_numero_docto);
							err.setIgNumeroError(numError);
							err.setCgError(lineaError);

							err.setInSociedad(cd2);
							//if(!(epoEnl instanceof EpoEnlaceAll)){
								query = disEnlace.getInsertaErrores(err);

								try{
									log.debug("Inserta Registro Con Error:: " + query);
									con.ejecutaSQL(query);
								}catch(Exception e){
									log.error("CargaDocumentoBean::procesoCargaDoctosWS(Exception):\n\t\tError al insertar en "+disEnlace.getTablaErrores()+". Seccion 1.\n");
									e.printStackTrace();
									sblog.append("Error al insertar en "+disEnlace.getTablaErrores()+".\n");
								}
							//}
							//if(!con.ejecutaSQL(query)) { log.error("Error al insertar en com_doctos_err_pub_imss.\n"); }
						}//while(st.hasMoreTokens())

					//}//if(!okReg)
					error.delete(0,error.length());
					noError.delete(0,noError.length());


				}


			}//FIN WHILE registros


			String csestatus = "";
			csestatus=(regErroneos)?"02":"01";
			if(hayreg==0){
				csestatus="03";
			}
			acuEnl.setCsEstatus(csestatus);
			query =
                " UPDATE com_acuse1"   +
                "    SET in_total_docto_mn = "+acuEnl.getInTotalAcepMn()+","   +
                "        fn_total_monto_mn = "+acuEnl.getFnTotalMontoAcepMn()+","   +
                "        in_total_docto_dl = "+acuEnl.getInTotalAcepDl()+","   +
                "        fn_total_monto_dl = "+acuEnl.getFnTotalMontoDl()+" "   +
                "  WHERE cc_acuse = '"+acuse+"'"  ;
			try {
				con.ejecutaSQL(query);
			} catch(Exception e) {
				sblog.append("Error al actualizar en com_acuse1.\n");
			}
			query = disEnlace.getUpdateAcuse(acuEnl);
			try{
				con.ejecutaSQL(query);
			}catch(Exception e){
				sblog.append("Error al actualizar en "+disEnlace.getTablaAcuse()+".\n");
			}
            
			con.terminaTransaccion(true);
			//Agrupacion de errores-----------------------------------------------
            
			retorno=totdocmn>0&&totdocdol>0;
			if(totdocmn>0&&totdocdol>0){
				query =
					" UPDATE com_acuse1"   +
					"    SET in_total_docto_mn = "+itotacepmn+","   +
					"        fn_total_monto_mn = "+mtodocmn.toPlainString()+","   +
					"        in_total_docto_dl = "+itotacepdol+","   +
					"        fn_total_monto_dl = "+mtodocdol.toPlainString()+" "   +
					"  WHERE cc_acuse = '"+acuEnl.getCcAcuse()+"'"  ;
				if( limiteLineaCredito.equals("S")) {
                    double monto_comprometidoTotal = 0;
                    double saldo_total =0;
                    double monto_comprometido =Double.parseDouble(mtodocmn.toPlainString());
                    String saldo = validaMontoComprometido( ic_epo);
                    saldo_total =  Double.parseDouble((String)saldo);
                    monto_comprometidoTotal = saldo_total + monto_comprometido;
                    String qrySentencia2 =
                        "	update DIS_LINEA_CREDITO_DM    "+
                        " set FN_SALDO_COMPROMETIDO = "+monto_comprometidoTotal+
                        "  WHERE  ic_epo =  ? "+
                        " AND ic_producto_nafin = ? "+
                        " AND df_vencimiento_adicional > SYSDATE  "+
                        " AND cg_tipo_solicitud = ? "+
                        " AND ic_estatus_linea = ? ";
                    log.debug("monto Coprometido::: "+qrySentencia2);
                    ps = con.queryPrecompilado(qrySentencia2);
                    ps.setInt(1, Integer.valueOf(ic_epo));
                    ps.setInt(2, Integer.valueOf("4"));
                    ps.setString(3, "I");
                    ps.setInt(4, Integer.valueOf("12"));
                    ps.executeUpdate();
                    ps.close();
                }
                con.terminaTransaccion(true);
			}

			/*query =	" DELETE "+disEnlace.getTablaDocumentos() +
					disEnlace.getCondicionQuery();
			try{
				con.ejecutaSQL(query);
			}catch(Exception e){
				sblog.append("Error al borrar "+disEnlace.getTablaDocumentos()+".\n");
			}*/
		}//FIN if(hayreg > 0)
		/*Para Validar la fecha de Emision		 */


        } catch(NafinException ne) {
            ne.printStackTrace();
            msgError = ne.getMsgError();
            throw ne;
        } catch(Exception e) {
            e.printStackTrace();
            log.error("CargaDocDistBean::procesoCargaDoctosWS(Exception) "+e);
            msgError = "El layout es incorrecto, por favor verif&iacute;quelo";
            throw new NafinException("SIST0001");
        } finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("CargaDocDistBean::procesoCargaDoctosWS(S)");
		}
        return retorno;
    }

	private boolean existeDoctoWS(String ic_epo,String ig_numero_docto,AccesoDB con) {
        log.info("CargaDocDistBean::existeDoctoWS(E)");
        String qrySentencia="";
        boolean resultado = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int numRegistros = 0;
		try {
			qrySentencia=	
                " select count(ic_documento) as numRegistros from dis_documento"+
                " where ic_estatus_docto != ? "+
                " and ig_numero_docto = ? " +
                " and ic_epo = ? " +
                " and ic_producto_nafin = ? ";
			ps = con.queryPrecompilado(qrySentencia);
            ps.setInt(1, Integer.valueOf(5));
			ps.setString(2, ig_numero_docto);
            ps.setInt(3, Integer.valueOf(ic_epo));
            ps.setInt(4, Integer.valueOf(4));
			rs = ps.executeQuery();
		    ps.clearParameters();
			if(rs.next())
				numRegistros = rs.getInt("NUMREGISTROS");
			rs.close();ps.close();
			if(numRegistros==0)
				resultado = false;
			else
				resultado = true;
        } catch(Exception e) {
            log.error("CargaDocDistBean::existeDoctoWS(Exception) "+e);
            //throw new NafinException("SIST0001");
		}
        log.info("CargaDocDistBean::existeDoctoWS (S)");
        return resultado;
	}//fin existe docto

	/**
 * Obtiene las cuentas de correo parametrizadas para la epo especificada,
 * m�s las cuentas generales parametrizadas (ic_epo IS NULL)
 * Si no hay cuentas parametrizadas para el Web Service regresa null.
 * @param claveEpo Clave de la epo
 *
 * @return Cadena con las cuenta de correo parametrizadas, separadas
 * 		por coma en caso de que sean dos o m�s.
 *
 */
	private String getCuentasWS(String claveEpo) {
		AccesoDB con = new AccesoDB();
		String cuentasCorreo = null;

		try {
			con.conexionDB();
			StringBuffer sb = new StringBuffer();
			String strSQL =
					" SELECT cg_mail " +
					" FROM com_correo_ws " +
					" WHERE ic_epo = ? OR ic_epo IS NULL ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(claveEpo));
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String correo = rs.getString("cg_mail");
				if (correo != null  && !correo.equals("")) {
					sb.append(((sb.length() > 0)?",":"") + correo);
				}
			}
			rs.close();
			ps.close();

			cuentasCorreo = (sb.length() > 0)?sb.toString():null;


		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return cuentasCorreo;
	}


	// *****************  funciones para el F032  *******************
	public String getTipoCreditoMantPendNeg(String ic_epo) throws NafinException{
		AccesoDB		con = null;
		ResultSet 	rs = null;
		String      qrySentencia = "";
		String		aux = "";
		String		aux2 = "";
		log.debug("getTipoCreditoMantPendNeg (E)");
		try{
        	con = new AccesoDB();
         con.conexionDB();
         qrySentencia =	" SELECT decode(NVL(PE.CG_TIPOS_CREDITO,PN.CG_TIPOS_CREDITO),'D','S','N') as tipo_credito"+
								" FROM COMREL_PRODUCTO_EPO PE,COMCAT_PRODUCTO_NAFIN PN"+
                        " WHERE PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
                        " AND PE.IC_EPO = "+ic_epo+
                        " AND PN.IC_PRODUCTO_NAFIN = 4";
			log.debug("qrySentencia :: "+qrySentencia);
          rs = con.queryDB(qrySentencia);
          if(rs.next()){
				aux = rs.getString(1);
          }
			 rs = null; qrySentencia = "";
			 qrySentencia ="  select CG_VALOR from com_parametrizacion_epo"+
                        "  WHERE ic_epo = "+ic_epo+
                        "  AND CC_PARAMETRO_EPO = 'PUB_EPO_VENTA_CARTERA'";
			log.debug("qrySentencia :: "+qrySentencia);
          rs = con.queryDB(qrySentencia);
          if(rs.next()){
				aux2 = rs.getString(1);
          }
			 if(aux.equals("S")&&aux2.equals("S")){
				aux = "N";
			 }else if(aux.equals("S")&&aux2.equals("N")){
				aux = "S";
			 }else if(aux.equals("N")){
				 aux = "N";
			 }
          con.cierraStatement();
        }catch(Exception e){
                throw new NafinException("SIST0001");
       }finally{
        	if(con.hayConexionAbierta()){
                	con.cierraConexionDB();
                }
		 }
		 log.debug("getTipoCreditoMantPendNeg (S)");
		 return aux;
	}

	public void eliminaPendientes(String ic_epo, String cc_acuse,String status) throws NafinException{
		AccesoDB con = null;
		boolean exito = true;
		String strSQL ="";
		ResultSet rs = null;
		PreparedStatement ps = null;
		boolean existenDoctos = false;
		log.debug("eliminaPendientes (E)");
		try {
		 	con = new AccesoDB();
			con.conexionDB();
			strSQL = " DELETE FROM dis_documento" +
						" WHERE cc_acuse = ? " +
						" AND ic_epo=? " +
						" AND ic_estatus_docto in (?)";
			log.debug("strSQL :: "+strSQL);
			log.debug("cc_acuse :: "+cc_acuse +" :: ic_epo :: "+ic_epo+" :: status :: "+status);
			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);
			ps.setInt(2, Integer.parseInt(ic_epo));
			ps.setInt(3, Integer.parseInt(status));
			ps.executeUpdate();
			if(ps!=null)ps.close();
			//valida si aun existe documentos para el acuse en cuestion
			strSQL = " select count(1) numDocs from  dis_documento" +
						" WHERE cc_acuse = ? " +
						" AND ic_epo=? ";
			log.debug("strSQL :: "+strSQL);
			log.debug("cc_acuse :: "+cc_acuse +" :: ic_epo :: "+ic_epo);
			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, cc_acuse);
			ps.setInt(2, Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				if(!"0".equals(rs.getString("numDocs"))){
					existenDoctos = true;
				}
			}
			if(rs!=null)rs.close();
			if(ps!=null)ps.close();

			if(!existenDoctos){
				strSQL = " DELETE FROM com_acuse1" +
							" WHERE cc_acuse = ?" ;
				log.debug("strSQL :: "+strSQL);
				log.debug("cc_acuse :: "+cc_acuse );
				ps = con.queryPrecompilado(strSQL);
				ps.setString(1, cc_acuse);
				ps.executeUpdate();

			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			exito = false;
			System.out.println("MantenimientoBean::eliminaPendientes Exception "+e);
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(exito);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			System.out.println("MantenimientoBean::eliminaPendientes (S)");
		}
		log.debug("eliminaPendientes (S)");
	}
	/**
	 * Funci�n que regresa la linea de credito seleccionada por la EPO
	 */
	public String lineaCreditoEpo(String ic_epo, String cc_acuse) throws NafinException {   // funcion que pasara igual
		AccesoDB 	con = new AccesoDB();
		ResultSet rs = null;
		boolean		commit = true;
		String resultado ="";
		log.debug("lineaCreditoEpo (E)");
		try{
			con.conexionDB();
			String query =
				" select max( d.IC_LINEA_CREDITO_DM ) as linea_credito"   +
				"   from dis_documento d"   +
				"  WHERE d.ic_epo ="+ic_epo+
				"   and d.cc_acuse = '"+cc_acuse+"'";

			try {
				log.debug("query :: "+query);
				rs = con.queryDB(query);
				if (rs.next())
					resultado = rs.getString("LINEA_CREDITO");

			} catch(SQLException sqle) { throw new NafinException("DSCT0037"); }
			con.cierraStatement();

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error LineaCreditoEpo "+e);
		}
		log.debug("lineaCreditoEpo (S)");
		return resultado;
	}
	public void actualizaLineaCredito(String ic_IF,String[] ic_documento, String cc_acuse, String epo)
		throws NafinException{
		System.out.println("CargaDocDistBean::actualizaLineaCredito (E)");
		AccesoDB 	con = null;
		String      qrySentencia = "";
     try{
		con = new AccesoDB();
		con.conexionDB();
		if(!ic_documento[0].equals("T")){
			for(int i=0;i<ic_documento.length;i++){
	       	qrySentencia = 	" update dis_documento"+
        			" set IC_LINEA_CREDITO_DM = "+ic_IF ;
            qrySentencia += " where ic_documento = "+ic_documento[i];
            System.out.println("ejecutando "+qrySentencia);
				con.ejecutaSQL(qrySentencia);
			}
		  }else{
				qrySentencia = 	" update dis_documento"+
        			" set IC_LINEA_CREDITO_DM = "+ic_IF ;
            qrySentencia += " where cc_acuse = '"+cc_acuse+"'";
				qrySentencia += " and IC_EPO = "+epo;
            System.out.println("ejecutando "+qrySentencia);
				con.ejecutaSQL(qrySentencia);
		  }
		con.cierraStatement();
    }catch(Exception e) {
		e.printStackTrace();
		System.out.println(e.getMessage());
		System.out.println("MantenimientoBean::actualizaLineaCredito Exception "+e);
		throw new NafinException("SIST0001");
	 }
 }



	public Vector mostrarDocumentosPendientes(String sNoCliente,String sEstatus,String cc_acuse, String numDoctos) throws NafinException {  //parasa idual
		AccesoDB 	con = new AccesoDB();
		ResultSet rs = null;
		boolean		commit = true;
		StringBuffer query = new StringBuffer();
		Vector vDoctos = new Vector();
		Vector vd = null;
		log.debug("mostrarDocumentosPendientes (E)");
		try{
			con.conexionDB();
			query.append(
				"		SELECT   pe.cg_num_distribuidor, py.cg_razon_social AS nombre_dist, d.ig_numero_docto,d.ct_referencia, " +
				"		TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision," +
				"		TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,d.cg_campo1,d.cg_campo2,d.cg_campo3,d.cg_campo4,d.cg_campo5,pp.cg_tipo_credito," +
				"		d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,m.ic_moneda,clas.cg_descripcion AS categoria,clas.ic_clasificacion AS ic_categoria," +
				"		NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,d.ig_plazo_credito,ROUND (d.df_fecha_venc - d.df_fecha_emision, 0) AS ig_plazo_docto," +
				"		NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, ca.cc_acuse acuse,d.ic_estatus_docto AS ic_estatus, tf.cd_descripcion AS modo_plazo,DECODE (pp.cg_tipo_credito,'D', 'Descuento Mercantil','C', 'Credito en Cuenta Corriente') AS tipo_credito," +
				"		d.fn_monto*(NVL(d.fn_porc_descuento,0)/100) AS monto_descontar,TO_CHAR (NVL (d.df_fecha_venc_credito, SYSDATE),'dd/mm/yyyy' ) AS df_fecha_venc_credito, py.CG_RFC, ed.cd_descripcion AS estatus" +
		      " , DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+  
		               
				"    FROM dis_documento d,"+//
				"		comcat_pyme py,"+//
				"		comcat_moneda m,"+//
				"		comrel_pyme_epo pe,"+//
				"		comcat_producto_nafin pn,"+//
				"		comrel_clasificacion clas,"+//
				"		comcat_tipo_financiamiento tf,"+//
				"		comcat_estatus_docto ed,"+
				"		comrel_pyme_epo_x_producto pp,"+//
				"		com_acuse1 ca"+
				"	WHERE d.ic_pyme = py.ic_pyme"+
				"		and d.cc_acuse = ca.cc_acuse"+
				"		AND d.ic_moneda = m.ic_moneda"+
				"		AND pe.ic_pyme = py.ic_pyme"+
				"		AND d.ic_epo = pe.ic_epo"+
				"		AND pe.ic_epo = pp.ic_epo"+
				"		AND pe.ic_pyme = pp.ic_pyme"+
				"		AND pp.ic_producto_nafin = 4"+
				"		AND pn.ic_producto_nafin = pp.ic_producto_nafin"+
				"		AND d.ic_clasificacion = clas.ic_clasificacion(+)"+
				"		AND d.ic_estatus_docto = ed.ic_estatus_docto"+/////////
				"		AND tf.ic_tipo_financiamiento = d.ic_tipo_financiamiento "+
				"    AND d.ic_epo ="+sNoCliente+
				"    AND ca.cc_acuse="+"'"+cc_acuse+"'"+
				"    AND d.ic_estatus_docto ="+sEstatus
				);

			if(!("").equals(numDoctos))
				query.append(" AND d.ig_numero_docto in ( "+numDoctos+") ");
			System.out.println(" query -->> "+query);
			rs = con.queryDB(query.toString());
			while(rs.next()) {
				vd = new Vector();
				vd.add((rs.getString("CG_NUM_DISTRIBUIDOR")==null?"":rs.getString("CG_NUM_DISTRIBUIDOR")));//0
				vd.add((rs.getString("NOMBRE_DIST")==null?"":rs.getString("NOMBRE_DIST")));//1
				vd.add((rs.getString("IG_NUMERO_DOCTO")==null?"":rs.getString("IG_NUMERO_DOCTO")));//2
				vd.add((rs.getString("CT_REFERENCIA")==null?"0":rs.getString("CT_REFERENCIA")));//3
				vd.add((rs.getString("DF_FECHA_EMISION")==null?"":rs.getString("DF_FECHA_EMISION")));//4
				vd.add((rs.getString("DF_FECHA_VENC")==null?"":rs.getString("DF_FECHA_VENC")));//5
				vd.add((rs.getString("CG_CAMPO1")==null?"":rs.getString("CG_CAMPO1")));//6
				vd.add((rs.getString("CG_CAMPO2")==null?"":rs.getString("CG_CAMPO2")));//7
				vd.add((rs.getString("CG_CAMPO3")==null?"":rs.getString("CG_CAMPO3")));//8
				vd.add((rs.getString("CG_CAMPO4")==null?"":rs.getString("CG_CAMPO4")));//9
				vd.add((rs.getString("CG_CAMPO5")==null?"":rs.getString("CG_CAMPO5")));//10
				vd.add((rs.getString("CG_TIPO_CREDITO")==null?"":rs.getString("CG_TIPO_CREDITO")));//11
				vd.add((rs.getString("IG_PLAZO_DOCTO")==null?"":rs.getString("IG_PLAZO_DOCTO")));//12
				vd.add((rs.getString("MONEDA")==null?"":rs.getString("MONEDA")));//13
				vd.add((rs.getString("FN_MONTO")==null?"":rs.getString("FN_MONTO")));//14
				vd.add((rs.getString("IC_MONEDA")==null?"":rs.getString("IC_MONEDA")));//15
				vd.add((rs.getString("CATEGORIA")==null?"":rs.getString("CATEGORIA")));//16
				vd.add((rs.getString("IC_CATEGORIA")==null?"":rs.getString("IC_CATEGORIA")));//17
				vd.add((rs.getString("IG_PLAZO_DESCUENTO")==null?"":rs.getString("IG_PLAZO_DESCUENTO")));//18
				vd.add((rs.getString("IG_PLAZO_CREDITO")==null?"":rs.getString("IG_PLAZO_CREDITO")));//19
				vd.add((rs.getString("FN_PORC_DESCUENTO")==null?"0":rs.getString("FN_PORC_DESCUENTO")));//20
				vd.add((rs.getString("ACUSE")==null?"":rs.getString("ACUSE")));//21
				vd.add((rs.getString("IC_ESTATUS")==null?"":rs.getString("ESTATUS")));//22
				vd.add((rs.getString("MODO_PLAZO")==null?"":rs.getString("MODO_PLAZO")));//23
				vd.add((rs.getString("TIPO_CREDITO")==null?"":rs.getString("TIPO_CREDITO")));//24
				vd.add((rs.getString("MONTO_DESCONTAR")==null?"":rs.getString("MONTO_DESCONTAR")));//25
				vd.add((rs.getString("DF_FECHA_VENC_CREDITO")==null?"":rs.getString("DF_FECHA_VENC_CREDITO")));//26
				vd.add((rs.getString("CG_RFC")==null?"":rs.getString("CG_RFC")));//27
				vd.add((rs.getString("ESTATUS")==null?"":rs.getString("ESTATUS")));//28
				vd.add((rs.getString("TIPOPAGO")==null?"":rs.getString("TIPOPAGO")));//29  

				vDoctos.add(vd);
			}
			rs.close();
			con.cierraStatement();
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error mostrarDocumentosPendientes "+e);
		}
		log.debug("mostrarDocumentosPendientes (S)");
		return vDoctos;
	}

	/**
	 * Se autorizan los documentos con estatus "Pendiente Negociable" y pasa "Pre Negociable"
	 * @throws com.netro.exception.NafinException
	 * @return
	 * @param numDoctos -> Contiene valores de los documentos a autorizar
	 * @param ic_estatus ->Se indica el estatus de documentos a considerar
	 * @param cc_acuse ->  identificador asigna a una carga en especifica
	 * @param ic_epo -> Clave de la cadena que realizo la carga
	 */

	public boolean bactualizaEstatusPendiente(String ic_epo, String cc_acuse,String ic_estatus,String auxPerfil, String numDoctos) throws NafinException {
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		boolean lbActualizo = true;
		String estatus="";
		boolean		commit = true;
		StringBuffer query = new StringBuffer();
		StringBuffer condicion= new StringBuffer();
		log.debug("bactualizaEstatusPendiente (E)");
		if(auxPerfil.equals("EPO MAN1 DISTR")){
			if("30".equals(ic_estatus)){
				estatus="28";
				condicion.append("   AND ic_estatus_docto= 30  ");
			}
		}if(auxPerfil.equals("EPO MAN2 DISTR")){
			if("28".equals(ic_estatus)){
				estatus="2";
				condicion.append("   AND ic_estatus_docto= 28 ");
			}
		}
		try{
			con.conexionDB();
			query.append("  update dis_documento "+
							 "  set ic_estatus_docto ="+estatus+
							 "  where cc_acuse =" +"'"+cc_acuse+"'"+
							"  AND ic_epo="+ic_epo+ condicion.toString()
			);
			if(!("").equals(numDoctos))
				query.append(" AND ig_numero_docto in ("+numDoctos+") ");

			System.out.println(" query -->> "+query);
			ps = con.queryPrecompilado(query.toString());
			ps.executeUpdate();
			if(ps!=null)ps.close();
			con.terminaTransaccion(lbActualizo);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}catch(Exception e){
			commit = false;
			lbActualizo = false;
			e.printStackTrace();
			log.error("Error bactualizaEstatusPendiente "+e);
		}
		log.debug("bactualizaEstatusPendiente (S)");
		return lbActualizo;
	}


	/**
	 * Fodea 09-2015
	 * Metodos para  Liberar l�nea de cr�dito de los Distribuidores con documentos financiados a meses sin intereses.
	 * @throws netropology.utilerias.AppException
	 * @return
	 */
	public boolean setActLineaCredDist() throws AppException {
		log.info("setActLineaCredDist (E)");
		PreparedStatement ps = null;
		ResultSet rs = null;
		AccesoDB con = new AccesoDB();
		boolean resultado = true;
		StringBuffer qrySentencia = new StringBuffer();

		try {

			con.conexionDB();

			//Obtengo los documentos en estatus Operado registrados con Financiamieno a Meses sin intereses, que no han vencido  y los cuales aun no se generan las fechas de pago
			qrySentencia = new StringBuffer();
			qrySentencia.append(" select  d.ic_documento,    " + " (d.FN_MONTO/d.IG_MESES_INTERES)as MontoXMes,  " +
									  " d.IG_MESES_INTERES as meses,  " + " d.ic_linea_credito as lineaCred,  " +
									  " TO_CHAR(d.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as fechaVencimiento,    " +
									  " TO_CHAR(ds.DF_FECHA_SELECCION,'dd/mm/yyyy')  as fechaOperacion , " +
									  " TO_CHAR(SYSDATE,'DD/MM/YYYY')  as fecha_actual  " + " from dis_documento d  , " +
									  " dis_docto_seleccionado ds, " + " com_linea_credito l, " + " dis_solicitud  s " +
									
									  " where d.ic_documento = ds.ic_documento " + " and s.ic_documento = ds.ic_documento " +
									  " and  l.ic_linea_credito  = d.ic_linea_credito " + " and  ic_estatus_docto = 4 " +
									  " and d.IG_TIPO_PAGO = 2  " + " and ds.CG_FECHAS_PAGO = 'N' " +
									  " and  d.DF_FECHA_VENC_CREDITO >= trunc(to_date(Sysdate,'dd/mm/yyyy'))  ");

			log.debug("qrySentencia " + qrySentencia.toString());

			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();
			int total = 0;

			// StringBuffer fechasPago = new StringBuffer();

			while (rs.next()) {
				String ic_documento = rs.getString("ic_documento") == null ? "" : rs.getString("ic_documento");
				String fechaOperacion = rs.getString("fechaOperacion") == null ? "" : rs.getString("fechaOperacion");
				String fechaVencimiento = rs.getString("fechaVencimiento") == null ? "" : rs.getString("fechaVencimiento");
				String lineaCred = rs.getString("lineaCred") == null ? "" : rs.getString("lineaCred");
				String MontoXMes = rs.getString("MontoXMes") == null ? "" : rs.getString("MontoXMes");
				int meses = rs.getInt("meses");

				//fechasPago = new StringBuffer();
				for (int i = 1; i <= meses - 1; i++) {
					String fechaCorte = Fecha.sumaFechaPlazo(fechaOperacion, i); // obtengo siguiente fecha de corte

					//se insertan las fechas de pago del documento
					qrySentencia = new StringBuffer();
					qrySentencia.append(" INSERT  INTO COM_AMORTI_DISDOCTO   " +
											  "  (  IC_AMORTI_DISDOCTO,  IC_DOCUMENTO ,  IC_LINEA_CREDITO , CG_MESES,  DF_FECHA_PAGO ,  FN_MONTO  , DF_FECHA_VENC  ) " +
											  " VALUES (  SEQ_COM_AMORTI_DISDOCTO.nextval , " + ic_documento + "," + lineaCred + ",'" + meses +
											  "', to_date('" + fechaCorte + "','dd/mm/yyyy')," + MontoXMes + ",to_date('" + fechaVencimiento +
											  "','dd/mm/yyyy') ) ");

					//log.debug("qrySentencia "+qrySentencia.toString());
					ps = con.queryPrecompilado(qrySentencia.toString());
					ps.executeUpdate();
					ps.close();
				}

				//este es para agregar la fecha de Vencimiento del documento  como ultimo pago
				qrySentencia = new StringBuffer();
				qrySentencia.append(" INSERT  INTO COM_AMORTI_DISDOCTO   " +
										  "  (  IC_AMORTI_DISDOCTO,  IC_DOCUMENTO ,  IC_LINEA_CREDITO , CG_MESES,  DF_FECHA_PAGO ,  FN_MONTO  , DF_FECHA_VENC  ) " +
										  " VALUES (  SEQ_COM_AMORTI_DISDOCTO.nextval , " + ic_documento + "," + lineaCred + ",'" + meses +
										  "', to_date('" + fechaVencimiento + "','dd/mm/yyyy')," + MontoXMes + ",to_date('" + fechaVencimiento +
										  "','dd/mm/yyyy') ) ");

				//log.debug("qrySentencia "+qrySentencia.toString());
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.executeUpdate();
				ps.close();

				//actualiza bandera  al documento para identificar que ya se generaron las fechas de pago del documento ya fueron generadas
				qrySentencia = new StringBuffer();
				qrySentencia.append(" Update dis_docto_seleccionado " + "  set CG_FECHAS_PAGO  = 'S' " +
										  " where IC_DOCUMENTO  =  " + ic_documento);

				//log.debug(" qrySentencia "+qrySentencia);
				ps = con.queryPrecompilado(qrySentencia.toString());
				ps.executeUpdate();
				ps.close();


			}

			// Obtengo los registros  que aun no ha sigo pagados
			qrySentencia = new StringBuffer();
			qrySentencia.append(" select IC_AMORTI_DISDOCTO, IC_DOCUMENTO , IC_LINEA_CREDITO, " +
									  " TO_CHAR(DF_FECHA_PAGO,'dd/mm/yyyy')  as DF_FECHA_PAGO  , " + " FN_MONTO , " + " DF_FECHA_VENC,   " +
									  " TO_CHAR(SYSDATE,'DD/MM/YYYY')  as fecha_actual " + " from com_amorti_disdocto " +
									  "  where CG_PAGO =  'N' " + " and  DF_FECHA_PAGO < TRUNC (SYSDATE) ");
			log.debug("qrySentencia " + qrySentencia.toString());

			ps = con.queryPrecompilado(qrySentencia.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				String lineaCred = rs.getString("IC_LINEA_CREDITO") == null ? "" : rs.getString("IC_LINEA_CREDITO");
				String fechasPago1 = rs.getString("DF_FECHA_PAGO") == null ? "" : rs.getString("DF_FECHA_PAGO");
				String MontoXMes = rs.getString("FN_MONTO") == null ? "" : rs.getString("FN_MONTO");
				String fechaVenc = rs.getString("DF_FECHA_VENC") == null ? "" : rs.getString("DF_FECHA_VENC");
				String fechaHoy = rs.getString("fecha_actual") == null ? "" : rs.getString("fecha_actual");
				String ic_documento = rs.getString("IC_DOCUMENTO") == null ? "" : rs.getString("IC_DOCUMENTO");
				String ic_amorti_disdocto =
							  rs.getString("IC_AMORTI_DISDOCTO") == null ? "" : rs.getString("IC_AMORTI_DISDOCTO");

				java.util.Date fechaPago = Comunes.parseDate(fechasPago1);
				java.util.Date fechaHoy2 = Comunes.parseDate(fechaHoy);

				log.debug("fechaPago  ===" + fechaPago + " ===   fechaHoy2  ===" + fechaHoy2);

				// Fechapago es menor a fechaHoy2 =  -1
				// FechaPago es igual a fechaHoy2 = 0
				// FechaPago es mayor a fechaHoy2 =  1

				//Verifico si la fecha de pago es menor al dia actual si es asi realizo la liberaci�n de la linea de credito
				if (fechaPago.compareTo(fechaHoy2) == -1) {

					qrySentencia = new StringBuffer();
					qrySentencia.append(" Update com_linea_credito " +
											  "  set fn_monto_autorizado_total  = fn_monto_autorizado_total  - " + MontoXMes + ", " +
											  " fn_saldo_linea_total    = fn_saldo_linea_total  +  " + MontoXMes + " where ic_linea_credito  =  " +
											  lineaCred);

					log.debug(" qrySentencia " + qrySentencia);
					log.debug(" fecha de pago mensual se actualizar� l�nea de Cr�dito del Distribuidor ");
					ps = con.queryPrecompilado(qrySentencia.toString());
					ps.executeUpdate();
					ps.close();

					// se actualiza  los campos CG_PAGO, DF_APLICA_PAGO para indicar que el pago ya se realizo y la fecha en la que se hizo
					qrySentencia = new StringBuffer();
					qrySentencia.append(" Update COM_AMORTI_DISDOCTO " + "  set CG_PAGO  = 'S', " + " DF_APLICA_PAGO  = Sysdate " +
											  " where ic_documento  = " + ic_documento + " and IC_AMORTI_DISDOCTO  = " +
											  ic_amorti_disdocto);

					//log.debug(" qrySentencia "+qrySentencia);
					ps = con.queryPrecompilado(qrySentencia.toString());
					ps.executeUpdate();
					ps.close();

				}

			}


		} catch (Exception e) {
			resultado = false;
			log.error("setActLineaCredDist (Exception) " + e);
			throw new AppException("setActLineaCredDist(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}

		log.info("setActLineaCredDist (S)");

		return resultado;
	}


}
