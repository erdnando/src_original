package com.netro.distribuidores;


import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

public class WSEnlace  implements DistribuidorEnlace, Serializable { 

	private String ic_epo 		= "";
	private String cg_receipt 	= "";
	private String ic_usuario 	= "";
		
	public WSEnlace (String ic_epo, String cg_receipt, String ic_usuario) {
		this.ic_epo 		= ic_epo;
		this.cg_receipt 	= cg_receipt;
		this.ic_usuario 	= ic_usuario;
	}//WSEnlace
	
	public WSEnlace () {

	}//WSEnlace
	
	public String getTablaDocumentos() {
		return "distmp_proc_docto_ws";
	}//getTablaDocumentos
	
	public String getTablaAcuse() {
		return "dis_doctos_pub_acu_ws";
	}//getTablaAcuse
	
	public String getTablaErrores() {
		return "dis_doctos_err_pub_ws";
	}//getTablaErrores
	
	public String getIc_usuario() {
		return this.ic_usuario;
	}//getIc_usuario
	
	public String getCg_receipt() {
		return this.cg_receipt;
	}//getIc_usuario
		
	public String getDocuments() {
		String querySentencia = 
			" SELECT IC_EPO,CG_NUM_DISTRIBUIDOR,IG_NUMERO_DOCTO, " +
			" DF_FECHA_EMISION, DF_FECHA_VENC,IC_MONEDA, " +
			"FN_MONTO,IC_TIPO_FINANCIAMIENTO,IC_CLASIFICACION, " +
			"IG_PLAZO_DESCUENTO,DF_FECHA_VENC_CREDITO,FN_PORC_DESCUENTO, " +
			"IC_LINEA_CREDITO_DM,CT_REFERENCIA,CG_CAMPO1, " +
			"CG_CAMPO2,CG_CAMPO3,CG_CAMPO4, " +
			"CG_CAMPO5,CS_NEGOCIABLE,CG_RECEIPT, CS_CARACTER_ESPECIAL, " +
	      " ig_tipo_pago, IG_MESES_INTERES  " +
			"   FROM "+getTablaDocumentos()+" "   +
			getCondicionQuery();
		
		return querySentencia;
	}//getDocuments
	
	public String getInsertaErrores(ErroresEnl err) {
		String querySentencia = 
			" INSERT INTO "+getTablaErrores()+" "   +
			"             (ic_epo, cg_receipt, ig_numero_docto, ig_numero_error, cg_error ) "+
			"      VALUES ("+ic_epo+", '"+cg_receipt+"', '"+err.getIgNumeroDocto()+"', '"+err.getIgNumeroError()+"', '"+err.getCgError()+"')";
		
		return querySentencia;
	}//getInsertaErrores
	
	public String getInsertAcuse(AcuseEnl acu) {
		String querySentencia = 
			" INSERT INTO "+getTablaAcuse()+" ("   +
			"              cc_acuse, ic_epo, cg_receipt, in_total_proc, fn_total_monto_mn,"   +
			"              in_total_acep_mn, in_total_rech_mn, fn_total_monto_dl,"   +
			"              in_total_acep_dl, in_total_rech_dl, df_fechahora_carga, cs_estatus)"   +
			"      VALUES ("   +
			"              '"+acu.getCcAcuse()+"', "+ic_epo+", '"+cg_receipt+"', "+acu.getInTotalProc()+", "+acu.getFnTotalMontoAcepMn()+","   +
			"              "+acu.getInTotalAcepMn()+", "+acu.getInTotalRechMn()+", "+acu.getFnTotalMontoAcepDl()+","   +
			"              "+acu.getInTotalAcepDl()+", "+acu.getInTotalRechDl()+", SYSDATE, "+acu.getCsEstatus()+")"  ;
			
		return querySentencia;
	}//getInsertAcuse

	public String getUpdateAcuse(AcuseEnl acu) {
		String querySentencia = 
			" UPDATE "+getTablaAcuse()+" "   +
			"    SET in_total_proc 		= "+acu.getInTotalProc()+" ,"   +
			"        fn_total_monto_mn 	= "+acu.getFnTotalMontoAcepMn()+" ,"   +
			"        in_total_acep_mn 	= "+acu.getInTotalAcepMn()+" ,"   +
			"        in_total_rech_mn 	= "+acu.getInTotalRechMn()+" ,"   +
			"        fn_total_monto_dl 	= "+acu.getFnTotalMontoAcepDl()+" ,"   +
			"        in_total_acep_dl 	= "+acu.getInTotalAcepDl()+" ,"   +
			"        in_total_rech_dl 	= "+acu.getInTotalRechDl()+" ,"   +
			"        cs_estatus 		= '"+acu.getCsEstatus()+"'"   +
			getCondicionQuery()+
			"    AND cc_acuse = '"+acu.getCcAcuse()+"' "  ;
			
		return querySentencia;
	}//getUpdateAcuse
	
	public String getCondicionQuery() {
		String condicion = 
			"  WHERE ic_epo = "+ic_epo+" "   +
			"    AND cg_receipt = '"+cg_receipt+"' "  ;
		return condicion;
	}//getCondicionQuery
	public void addErrores(ErroresEnl err){}
  public List getErrores(){
    ArrayList lista = new ArrayList();
    return lista;
  }


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setCg_receipt(String cg_receipt) {
		this.cg_receipt = cg_receipt;
	}


	public String get_cg_receipt() {
		return cg_receipt;
	}


	public void setIc_usuario(String ic_usuario) {
		this.ic_usuario = ic_usuario;
	}


	public String get_ic_usuario() {
		return ic_usuario;
	}
  
}//WSEnlace
