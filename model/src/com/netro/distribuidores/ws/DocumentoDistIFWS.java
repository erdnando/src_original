package com.netro.distribuidores.ws;


/**
 * La clase representa a un documento seleccionado para la operaci�n de
 * Distribuidores.
 * @author Fabian Valenzuela
 */
public class DocumentoDistIFWS implements java.io.Serializable {

	private String claveEpo;
	private String nombreEpo;
	private String claveDistribuidor;
	private String nombreDistribuidor;
	private String numeroDistribuidor;
	private String numeroSiracDistribuidor;
	private String acuseCarga;
	private String NumDoctoIni;
	private String fechaPublicacion;
	private String fechaEmision;
	private String fechaVenciIni;
	private String plazoDoctoIni;
	private String montoDocto;
	private String plazoDescuento;
	private String porcentajeDescuento;
	private String montoDescuento;
	private String claveModPlazo;
	private String modalidadPlazo;
	private String numDoctoFin;
	private String fechaOperacion;
	private String claveMoneda;
	private String nombreMoneda;
	private String montoCredito;
	private String plazoCredito;
	private String fechaVenciCredito;
	private String montoInteres;
	private String tasaInteres;
	private String claveTipoInteres;
	private String tipoInteres;
	private String tipoConversion;
	private String montoDoctoDL;
	private String tipoCambio;
	private String responPagoInteres;
	private String tipoCobranza;
	private String referencia;
	private String campoAdicional1;
	private String campoAdicional2;
	private String campoAdicional3;
	private String campoAdicional4;
	private String campoAdicional5;
	private String valorTasa;
	private String monedaLinea;
	private String montoRecibir;
	private String claveTasa;
	private String nombreEstatus;
	//GPG
	private String claveDocumento;
	private String claveEstatus;
	private String claveRechazo;
	private String errorDetalle;
	private String acuseConfirmacion;
	private String folio;
	private String fechaAltaSolicitud;
	
	private Integer tipoError;
	private String plazo;
        
        private String modalidadRiesgo; //QC-DLHC
        private String rfcEpo; //QC-DLHC
        private String rfcDistribuidor; //QC-DLHC
           
    
	public DocumentoDistIFWS() {
	}
	
	

	/**
	 * En caso de que exista un error en el procesamiento del Documento
	 * se regresa un c�digo que indica si el error es normal debido a las
	 * reglas de la l�gica de negocio � si es un error de sistema (BD, memoria, etc)
	 * @param tipoError Clave del tipo de error
	 * 	0.- Sin error
	 *    1.- Error normal (por validaci�n de negocio)
	 *    2.- Error inesperado (BD, memoria, etc.)
	 *    
	 */
	


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setNombreEpo(String nombreEpo) {
		this.nombreEpo = nombreEpo;
	}


	public String getNombreEpo() {
		return nombreEpo;
	}


	public void setClaveDistribuidor(String claveDistribuidor) {
		this.claveDistribuidor = claveDistribuidor;
	}


	public String getClaveDistribuidor() {
		return claveDistribuidor;
	}


	public void setNombreDistribuidor(String nombreDistribuidor) {
		this.nombreDistribuidor = nombreDistribuidor;
	}


	public String getNombreDistribuidor() {
		return nombreDistribuidor;
	}


	public void setNumeroDistribuidor(String numeroDistribuidor) {
		this.numeroDistribuidor = numeroDistribuidor;
	}


	public String getNumeroDistribuidor() {
		return numeroDistribuidor;
	}


	public void setNumeroSiracDistribuidor(String numeroSiracDistribuidor) {
		this.numeroSiracDistribuidor = numeroSiracDistribuidor;
	}


	public String getNumeroSiracDistribuidor() {
		return numeroSiracDistribuidor;
	}


	public void setAcuseCarga(String acuseCarga) {
		this.acuseCarga = acuseCarga;
	}


	public String getAcuseCarga() {
		return acuseCarga;
	}


	public void setNumDoctoIni(String NumDoctoIni) {
		this.NumDoctoIni = NumDoctoIni;
	}


	public String getNumDoctoIni() {
		return NumDoctoIni;
	}


	public void setFechaPublicacion(String fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}


	public String getFechaPublicacion() {
		return fechaPublicacion;
	}


	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}


	public String getFechaEmision() {
		return fechaEmision;
	}


	public void setFechaVenciIni(String fechaVenciIni) {
		this.fechaVenciIni = fechaVenciIni;
	}


	public String getFechaVenciIni() {
		return fechaVenciIni;
	}


	public void setPlazoDoctoIni(String plazoDoctoIni) {
		this.plazoDoctoIni = plazoDoctoIni;
	}


	public String getPlazoDoctoIni() {
		return plazoDoctoIni;
	}


	public void setMontoDocto(String montoDocto) {
		this.montoDocto = montoDocto;
	}


	public String getMontoDocto() {
		return montoDocto;
	}


	public void setPlazoDescuento(String plazoDescuento) {
		this.plazoDescuento = plazoDescuento;
	}


	public String getPlazoDescuento() {
		return plazoDescuento;
	}


	public void setPorcentajeDescuento(String porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}


	public String getPorcentajeDescuento() {
		return porcentajeDescuento;
	}


	public void setMontoDescuento(String montoDescuento) {
		this.montoDescuento = montoDescuento;
	}


	public String getMontoDescuento() {
		return montoDescuento;
	}


	public void setClaveModPlazo(String claveModPlazo) {
		this.claveModPlazo = claveModPlazo;
	}


	public String getClaveModPlazo() {
		return claveModPlazo;
	}


	public void setModalidadPlazo(String modalidadPlazo) {
		this.modalidadPlazo = modalidadPlazo;
	}


	public String getModalidadPlazo() {
		return modalidadPlazo;
	}


	public void setNumDoctoFin(String numDoctoFin) {
		this.numDoctoFin = numDoctoFin;
	}


	public String getNumDoctoFin() {
		return numDoctoFin;
	}


	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}


	public String getFechaOperacion() {
		return fechaOperacion;
	}


	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}


	public String getClaveMoneda() {
		return claveMoneda;
	}


	public void setNombreMoneda(String nombreMoneda) {
		this.nombreMoneda = nombreMoneda;
	}


	public String getNombreMoneda() {
		return nombreMoneda;
	}


	public void setMontoCredito(String montoCredito) {
		this.montoCredito = montoCredito;
	}


	public String getMontoCredito() {
		return montoCredito;
	}


	public void setPlazoCredito(String plazoCredito) {
		this.plazoCredito = plazoCredito;
	}


	public String getPlazoCredito() {
		return plazoCredito;
	}


	public void setFechaVenciCredito(String fechaVenciCredito) {
		this.fechaVenciCredito = fechaVenciCredito;
	}


	public String getFechaVenciCredito() {
		return fechaVenciCredito;
	}


	public void setMontoInteres(String montoInteres) {
		this.montoInteres = montoInteres;
	}


	public String getMontoInteres() {
		return montoInteres;
	}


	public void setTasaInteres(String tasaInteres) {
		this.tasaInteres = tasaInteres;
	}


	public String getTasaInteres() {
		return tasaInteres;
	}


	public void setClaveTipoInteres(String claveTipoInteres) {
		this.claveTipoInteres = claveTipoInteres;
	}


	public String getClaveTipoInteres() {
		return claveTipoInteres;
	}


	public void setTipoInteres(String tipoInteres) {
		this.tipoInteres = tipoInteres;
	}


	public String getTipoInteres() {
		return tipoInteres;
	}


	public void setTipoConversion(String tipoConversion) {
		this.tipoConversion = tipoConversion;
	}


	public String getTipoConversion() {
		return tipoConversion;
	}


	public void setTipoCambio(String tipoCambio) {
		this.tipoCambio = tipoCambio;
	}


	public String getTipoCambio() {
		return tipoCambio;
	}


	public void setResponPagoInteres(String responPagoInteres) {
		this.responPagoInteres = responPagoInteres;
	}


	public String getResponPagoInteres() {
		return responPagoInteres;
	}


	public void setTipoCobranza(String tipoCobranza) {
		this.tipoCobranza = tipoCobranza;
	}


	public String getTipoCobranza() {
		return tipoCobranza;
	}


	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}


	public String getReferencia() {
		return referencia;
	}


	public void setCampoAdicional1(String campoAdicional1) {
		this.campoAdicional1 = campoAdicional1;
	}


	public String getCampoAdicional1() {
		return campoAdicional1;
	}


	public void setCampoAdicional2(String campoAdicional2) {
		this.campoAdicional2 = campoAdicional2;
	}


	public String getCampoAdicional2() {
		return campoAdicional2;
	}


	public void setCampoAdicional3(String campoAdicional3) {
		this.campoAdicional3 = campoAdicional3;
	}


	public String getCampoAdicional3() {
		return campoAdicional3;
	}


	public void setCampoAdicional4(String campoAdicional4) {
		this.campoAdicional4 = campoAdicional4;
	}


	public String getCampoAdicional4() {
		return campoAdicional4;
	}


	public void setCampoAdicional5(String campoAdicional5) {
		this.campoAdicional5 = campoAdicional5;
	}


	public String getCampoAdicional5() {
		return campoAdicional5;
	}


	public void setAcuseConfirmacion(String acuseConfirmacion) {
		this.acuseConfirmacion = acuseConfirmacion;
	}


	public String getAcuseConfirmacion() {
		return acuseConfirmacion;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getFolio() {
		return folio;
	}


	public void setFechaAltaSolicitud(String fechaAltaSolicitud) {
		this.fechaAltaSolicitud = fechaAltaSolicitud;
	}


	public String getFechaAltaSolicitud() {
		return fechaAltaSolicitud;
	}


	public void setValorTasa(String valorTasa) {
		this.valorTasa = valorTasa;
	}


	public String getValorTasa() {
		return valorTasa;
	}


	public void setMonedaLinea(String monedaLinea) {
		this.monedaLinea = monedaLinea;
	}


	public String getMonedaLinea() {
		return monedaLinea;
	}


	public void setMontoRecibir(String montoRecibir) {
		this.montoRecibir = montoRecibir;
	}


	public String getMontoRecibir() {
		return montoRecibir;
	}


	public void setClaveTasa(String claveTasa) {
		this.claveTasa = claveTasa;
	}


	public String getClaveTasa() {
		return claveTasa;
	}


	public void setClaveDocumento(String claveDocumento) {
		this.claveDocumento = claveDocumento;
	}


	public String getClaveDocumento() {
		return claveDocumento;
	}


	public void setClaveEstatus(String claveEstatus) {
		this.claveEstatus = claveEstatus;
	}


	public String getClaveEstatus() {
		return claveEstatus;
	}


	public void setClaveRechazo(String claveRechazo) {
		this.claveRechazo = claveRechazo;
	}


	public String getClaveRechazo() {
		return claveRechazo;
	}


	public void setMontoDoctoDL(String montoDoctoDL) {
		this.montoDoctoDL = montoDoctoDL;
	}


	public String getMontoDoctoDL() {
		return montoDoctoDL;
	}


	public void setErrorDetalle(String errorDetalle) {
		this.errorDetalle = errorDetalle;
	}


	public String getErrorDetalle() {
		return errorDetalle;
	}


	public void setTipoError(Integer tipoError) {
		this.tipoError = tipoError;
	}


	public Integer getTipoError() {
		return tipoError;
	}


	public void setNombreEstatus(String nombreEstatus) {
		this.nombreEstatus = nombreEstatus;
	}


	public String getNombreEstatus() {
		return nombreEstatus;
	}
	
	public String getPlazo() {
		return plazo;
	}

	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}
	
        /**
         *QC-DLHC
         * @param modalidadRiesgo
         */
        public void setModalidadRiesgo(String modalidadRiesgo) {
                this.modalidadRiesgo = modalidadRiesgo;
        }
        
        /**
         *QC-DLHC
         * @return
         */
        public String getModalidadRiesgo() {
                return modalidadRiesgo;
        }
        
        
        /**
         *QC-DLHC
         * @param rfcEpo
        */
        public void setRfcEpo(String rfcEpo) {
                this.rfcEpo = rfcEpo;
        }
        
        /**
         *QC-DLHC
         * @return
         */
        public String getRfcEpo() {
                return rfcEpo;
        }
        
        /**
         *QC-DLHC
         * @param rfcDistribuidor
         */
        public void setRfcDistribuidor(String rfcDistribuidor) {
                this.rfcDistribuidor = rfcDistribuidor;
        }
        
        /**
         *QC-DLHC
         * @return
         */
        public String getRfcDistribuidor() {
                return rfcDistribuidor;
        }

}