package com.netro.distribuidores;


import com.netro.exception.NafinException;

import java.util.HashMap;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.AppException;
import netropology.utilerias.Combos;
import netropology.utilerias.Registros;

@Remote
public interface AceptacionPyme {

    public Vector consultaDoctos(String cc_acuse) throws NafinException;

    public Vector consultaDoctos(
	String ic_pyme,
	String ic_epo,
	String ic_moneda,
	String ig_numero_docto,
	String fn_monto_de,
	String fn_monto_a,
	String cc_acuse,
	String monto_desc,
	String fecha_emision_de,
	String fecha_emision_a,
	String con_cambio,
	String fecha_vto_de,
	String fecha_vto_a,
	String modo_plazo,
	String fecha_publicacion_de,
	String fecha_publicacion_a,
	String in
    ) throws NafinException;

    public String confirmaPedido(
	String totalMontoMN,
	String totalInteresMN,
	String totalMontoImpMN,
	String totalMontoUSD,
	String totalInteresUSD,
	String totalMontoImpUSD,
	String icUsuario,
	String cgReciboElectronico,
	String ic_documento[],
	String ic_tasa[],
	String cg_rel_mat[],
	String fn_puntos[],
	String fn_valor_tasa[],
	String fn_importe_interes[],
	String fn_importe_recibir[],
	String plazo_credito[],
	String fecha_vto[],
	String tipo_tasa[],
	String puntos_pref[],
	String fn_montoValuado[]
    ) throws NafinException;

    public String confirmaDoctoCCC(
	String totalMontoMN,
	String totalInteresMN,
	String totalMontoImpMN,
	String totalMontoUSD,
	String totalInteresUSD,
	String totalMontoImpUSD,
	String icUsuario,
	String cgReciboElectronico,
	String ic_documento[],
	String ic_tasa[],
	String cg_rel_mat[],
	String fn_puntos[],
	String fn_valor_tasa[],
	String fn_importe_interes[],
	String fn_importe_recibir[],
	String plazo_credito[],
	String fecha_vto[],
	String ic_linea_credito[]
    ) throws NafinException;
   
    public String confirmaDoctoCCC(
	String totalMontoMN,
	String totalInteresMN,
	String totalMontoImpMN,
	String totalMontoUSD,
	String totalInteresUSD,
	String totalMontoImpUSD,
	String icUsuario,
	String cgReciboElectronico,
	String ic_documento[],
	String ic_tasa[],
	String cg_rel_mat[],
	String fn_puntos[],
	String fn_valor_tasa[],
	String fn_importe_interes[],
	String fn_importe_recibir[],
	String plazo_credito[],
	String fecha_vto[],
	String ic_linea_credito[],
	String tipoUsuario
    ) throws NafinException;

    public Vector consultaDoctosCCC(
	String ic_pyme,
	String ic_epo,
	String ic_moneda,
	String ig_numero_docto,
	String fn_monto_de,
	String fn_monto_a,
	String cc_acuse,
	String monto_desc,
	String fecha_emision_de,
	String fecha_emision_a,
	String con_cambio,
	String fecha_vto_de,
	String fecha_vto_a,
	String modo_plazo,
	String fecha_publicacion_de,
	String fecha_publicacion_a,
	String ic_linea_credito,
	String in
    ) throws NafinException;

    public Vector consultaDoctosCCC(String cc_acuse) throws NafinException;

    public void cambiaTipoFinanciamiento(String ic_pyme) throws NafinException;

    public void validarUsuario(String esClavePyme, String esLogin, String esPassword, String esIP, String esHost, String esSistema) throws NafinException;

    public void operaTipoCredito(String ic_pyme,String tipo_credito,int ic_producto_nafin,String ic_epo) throws NafinException;

    public abstract Vector monitorLineas(String ic_epo) throws NafinException;

    public abstract Vector monitorLineas(String ic_epo, String ic_if, String ic_moneda, String ic_pyme ) throws NafinException;

    public String getTipoCambio(String ic_pyme) throws NafinException;

    public Combos getComboLineasCCC(String ic_pyme) throws NafinException;

    public Vector getLineasCCC(String ic_pyme) throws NafinException;

    public Vector getLineasCCC(String ic_pyme, String ic_if, String ic_moneda) throws NafinException;

    public String getDiasInhabiles(String ic_pyme,String ic_epo) throws NafinException;

    public abstract void descuentoAutomaticoDist(String ruta) throws NafinException;

    public int setLimiteAcumPyme(String ic_epo, String ic_pyme, String fg_limite_pyme_acum) throws NafinException;

    public boolean DiaInhabil(String esFecha) throws NafinException;

    public String getDiasInhabilesDes(String ic_pyme,String ic_epo, String fecha) throws NafinException;
    
    public Vector consultaDoctosVentaCartera(
	String ic_epo,
	String ic_pyme,
	String ic_moneda,
	String ig_numero_docto,
	String fn_monto_de,
	String fn_monto_a,
	String cc_acuse,
	String monto_desc,
	String fecha_emision_de,
	String fecha_emision_a,
	String con_cambio,
	String fecha_vto_de,
	String fecha_vto_a,
	String fecha_publicacion_de,
	String fecha_publicacion_a,
	String in
    ) throws NafinException;

    public Vector consDoctosVentaCartera(String ic_epo ,String ic_linea_credito ,String in) throws NafinException;

    public void validarUsuarioEPO(String esClavePyme, String esLogin, String esPassword,String esIP, String esHost, String esSistema) throws NafinException;

    public Vector consDoctosVCartera(String cc_acuse) throws NafinException;   

    public Vector monitorLineasdm(String ic_epo, String ic_if, String ic_moneda) throws NafinException;

    public Registros getResumenPublicacionXEpo(String clavePyme);

    // Fodea 019-2012
    public Registros getResumenPublicacionXEpoFacRec(String clavePyme);

    public Vector consultaDoctosFactRecurso(
	String ic_pyme,
	String ic_epo ,
	String ic_moneda,
	String ig_numero_docto,
	String fn_monto_de,
	String fn_monto_a,
	String cc_acuse,
	String fecha_emision_de,
	String fecha_emision_a,
	String fecha_vto_de,
	String fecha_vto_a,
	String fecha_publicacion_de,
	String fecha_publicacion_a,
	String ic_linea_credito,
	String in
    ) throws NafinException;

    public String confirmaDoctoFactoRec(
	String totalMontoMN,
	String totalInteresMN,
	String totalMontoImpMN,
	String totalMontoUSD,
	String totalInteresUSD,
	String totalMontoImpUSD,
	String icUsuario,
	String cgReciboElectronico,
	String ic_documento[],
	String ic_tasa[],
	String cg_rel_mat[],
	String fn_puntos[],
	String fn_valor_tasa[], 
	String fn_importe_interes[],
	String fn_importe_recibir[],
	String plazo_credito[],
	String fecha_vto[],
	String ic_linea_credito[]
    ) throws NafinException;

    public Vector consultaDoctosFactoRec(String cc_acuse) throws NafinException;

    public String getMontoTotalMocumentos(
	String ic_epo,
	String ic_moneda ,
	String ic_if,
	String modalidad,
	String ic_pyme,
	String lineaCredito
    ) throws NafinException; //F05-2014

    public Vector consultaDoctosCccTC(
	String ic_pyme,
	String ic_epo,
	String ic_moneda,
	String ig_numero_docto,
	String fn_monto_de,
	String fn_monto_a,
	String cc_acuse,
	String monto_desc,
	String fecha_emision_de,
	String fecha_emision_a,
	String con_cambio,
	String fecha_vto_de,
	String fecha_vto_a,
	String	modo_plazo,
	String fecha_publicacion_de,
	String fecha_publicacion_a,
	String ic_bins,
	String in
   ) throws AppException;

    public void preGuardadoIdOrdenTC(String[]ic_documento, String idNumOrden, String montoToTal) throws AppException;

    public void rechazoIdOrdenTC(HashMap hmData, String[] numero_docto) throws AppException;

    public Vector consultaDoctosCCCTC(String cc_acuse) throws NafinException;

    public String confirmaDoctoCCC(
	String totalMontoMN,
	String totalInteresMN,
	String totalMontoImpMN,
	String totalMontoUSD,
	String totalInteresUSD,
	String totalMontoImpUSD,
	String icUsuario,
	String cgReciboElectronico,
	String ic_documento[],
	String ic_tasa[],
	String cg_rel_mat[],
	String fn_puntos[],
	String fn_valor_tasa[],
	String fn_importe_interes[],
	String fn_importe_recibir[],
	String plazo_credito[],
	String fecha_vto[],
	String ic_linea_credito[],
	String tipoUsuario,
	String ordenPagoTC,
	HashMap hmInfoTransTC
    ) throws NafinException;

    public abstract void procesoCambioEstatus (String ruta) throws NafinException;
	public String getDiasInhabilesXanio()   throws NafinException; //F09-2015
	public boolean getEsDiasInhabilesXanio(String fecha )   throws NafinException;  //F09-2015
	public void  getDoctosNotifiIF_Distribuidores()throws NafinException; //F027-2015
} 