package com.netro.distribuidores;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Calendar;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "LineadmEJB" , mappedName = "LineadmEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class LineadmBean implements Lineadm {
	private final static Log log = ServiceLocator.getInstance().getLog(LineadmBean.class);

  public Vector getValores(String ic_epo, String solicitud, String ic_moneda, String ic_tipo_cobro_interes, String vencimiento, String ic_financiera)
  	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    Vector		  vecColumnas		= new Vector();
    Vector		  vecFilas			= new Vector();

    try{
      con = new AccesoDB();
      con.conexionDB();
      //CONSULTA
      if ("".equals(ic_financiera)) {
      qrySentencia =
        "SELECT A.cg_razon_social"+
        " ,B.cd_nombre"+
        " ,C.cd_descripcion"+
        " ,TRUNC (TO_DATE ('"+vencimiento+"','dd/mm/yyyy')) - TRUNC (SYSDATE) AS plazo "+
        " ,D.ic_nafin_electronico"+
        " FROM comcat_epo A, comcat_moneda B, comcat_tipo_cobro_interes C, comrel_nafin D "+
        " WHERE A.ic_epo  = "+ic_epo+" "+
        " AND B.ic_moneda = "+ic_moneda+" "+
        " AND C.ic_tipo_cobro_interes = "+ic_tipo_cobro_interes+" "+
        " AND A.ic_epo = D.ic_epo_pyme_if"+
        " AND D.cg_tipo = 'E'";
      }
      else {
      qrySentencia =
        "SELECT A.cg_razon_social"+
        " ,B.cd_nombre"+
        " ,C.cd_descripcion"+
        " ,TRUNC (TO_DATE ('"+vencimiento+"','dd/mm/yyyy')) - TRUNC (SYSDATE) AS plazo "+
        " ,D.ic_nafin_electronico"+
        " ,E.cd_nombre as banco"+
        " FROM comcat_epo A, comcat_moneda B, comcat_tipo_cobro_interes C, comrel_nafin D,comcat_financiera E "+
        " WHERE A.ic_epo  = "+ic_epo+" "+
        " AND B.ic_moneda = "+ic_moneda+" "+
        " AND C.ic_tipo_cobro_interes = "+ic_tipo_cobro_interes+" "+
        " AND A.ic_epo = D.ic_epo_pyme_if"+
        " AND D.cg_tipo = 'E'"+
        " AND E.ic_financiera = "+ic_financiera+" ";
      }
      System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
        vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
        if ("I".equals(solicitud))
          solicitud="INICIAL";
        else
          if ("A".equals(solicitud))
            solicitud="AMPLIACIÓN/REDUCCIÓN";
          else
            solicitud="RENOVACIÓN";
				vecColumnas.addElement(solicitud);
				vecColumnas.addElement((rs.getString("cd_nombre")==null)?"":rs.getString("cd_nombre"));
				vecColumnas.addElement((rs.getString("cd_descripcion")==null)?"":rs.getString("cd_descripcion"));
				vecColumnas.addElement(rs.getString("plazo"));
				vecColumnas.addElement(rs.getString("ic_nafin_electronico"));
				if ("".equals(ic_financiera))
          vecColumnas.addElement("");
        else
          vecColumnas.addElement(rs.getString("banco"));
      	vecFilas.addElement(vecColumnas);
      }
      con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
   return vecFilas;
  }//getValores

  public Vector getTotalAfiliados(String ic_epo)
  	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    Vector		  vecColumnas		= new Vector();
    Vector		  vecFilas			= new Vector();

    try{
      con = new AccesoDB();
      con.conexionDB();

      //CONSULTA
      qrySentencia =
        "SELECT COUNT(*) AS totClientAfil"+
        " FROM comrel_pyme_epo_x_producto"+
        " WHERE ic_epo = "+ic_epo+
        " AND ic_producto_nafin = 4"+
        " AND cs_habilitado = 'S'";
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
        vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString("totClientAfil").equals("0"))?"0":rs.getString("totClientAfil"));
      	vecFilas.addElement(vecColumnas);
      }
      con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
   return vecFilas;
  }//getTotalAfiliados

  public String getDetalle(String ic_if)
  	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    String      aux          = "";
    try{
      con = new AccesoDB();
      con.conexionDB();
      //CONSULTA
      qrySentencia =
        "SELECT CG_TIPO_LIQUIDACION"+
        " FROM COMREL_PRODUCTO_IF"+
        " WHERE IC_IF = "+ic_if+" "+
        " AND IC_PRODUCTO_NAFIN=4";
//      System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next())
        aux = (rs.getString("CG_TIPO_LIQUIDACION")==null)?"":rs.getString("CG_TIPO_LIQUIDACION");
      con.cierraStatement();
      if("".equals(aux))
      	throw new NafinException("DIST0023");
	}catch (NafinException ne){
		throw ne;
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
   return aux;
  }//getDetalle

  public Vector getDetalleInicial(String ic_linea_credito_dm)
  	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    try{
      con = new AccesoDB();
      con.conexionDB();
      qrySentencia =
        "SELECT a.cg_num_cuenta_if, a.ic_financiera, b.cd_nombre,a.cg_num_cuenta_epo "+
        "  FROM dis_linea_credito_dm a, comcat_financiera b "+
        " WHERE b.ic_financiera (+) = a.ic_financiera "+
        "   AND a.ic_linea_credito_dm = "+ic_linea_credito_dm+" ";
      System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString("CG_NUM_CUENTA_IF")==null)?"":rs.getString("CG_NUM_CUENTA_IF"));
	        vecColumnas.addElement((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
	        vecColumnas.addElement((rs.getString("CD_NOMBRE")==null)?"":rs.getString("CD_NOMBRE"));
	        vecColumnas.addElement((rs.getString("cg_num_cuenta_epo")==null)?"":rs.getString("cg_num_cuenta_epo"));
	        vecFilas.addElement(vecColumnas);
      }
      con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
		return vecFilas;
  }//getDetalleInicial

  public String getMontoAutorizado(String ic_if, String ic_epo,String folio)
  	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    String aux="";
    try{
      con = new AccesoDB();
      con.conexionDB();
      //CONSULTA
      qrySentencia =
        "SELECT FN_MONTO_AUTORIZADO_TOTAL"+
        " FROM dis_linea_credito_dm"+
        " WHERE ic_epo = "+ic_epo+" "+
        " AND ic_if = "+ic_if+" "+
        " AND IC_LINEA_CREDITO_DM = "+folio+" ";
        System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
        aux = (rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL");
      }
      con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
		return aux;
  }//getMontoAutorizado


  public Vector getMontoAutFechaVto(String ic_if, String ic_epo,String folio)
  	throws NafinException{
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    Vector		aux=new Vector();
    try{
      con = new AccesoDB();
      con.conexionDB();
      //CONSULTA
      qrySentencia =
        "SELECT FN_MONTO_AUTORIZADO_TOTAL"+
		" ,to_char(DF_VENCIMIENTO_ADICIONAL,'dd/mm/yyyy') as DF_VENCIMIENTO_ADICIONAL"+
        " FROM dis_linea_credito_dm"+
        " WHERE ic_epo = "+ic_epo+" "+
        " AND ic_if = "+ic_if+" "+
        " AND IC_LINEA_CREDITO_DM = "+folio+" ";
        System.out.println(qrySentencia);
      rs = con.queryDB(qrySentencia);
      if(rs.next()){
        aux.add((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
		aux.add((rs.getString("DF_VENCIMIENTO_ADICIONAL")==null)?"":rs.getString("DF_VENCIMIENTO_ADICIONAL"));
      }
      con.cierraStatement();
    }catch(Exception e){
    	throw new NafinException("SIST0001");
    }finally{
      if(con.hayConexionAbierta()){
        con.cierraConexionDB();
      }
    }
	return aux;
  }//getMontoAutorizado


 public Vector getComboBanco()
 throws NafinException
 {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
          "SELECT IC_FINANCIERA,CD_NOMBRE"+
          " FROM COMCAT_FINANCIERA"+
          " WHERE IC_TIPO_FINANCIERA = 1";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString("IC_FINANCIERA")==null)?"":rs.getString("IC_FINANCIERA"));
	        vecColumnas.addElement((rs.getString("CD_NOMBRE")==null)?"":rs.getString("CD_NOMBRE"));
	        vecFilas.addElement(vecColumnas);
	      }
      con.cierraStatement();
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
         	con.cierraConexionDB();
      }
		return vecFilas;
    }

 public Vector getComboEPO_RS() throws NafinException {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
          "SELECT A.ic_epo, A.cg_razon_social"+
          " FROM COMCAT_EPO A, COMREL_PRODUCTO_EPO PE, COMCAT_PRODUCTO_NAFIN PN"+
          " WHERE PE.ic_producto_nafin = PN.ic_producto_nafin"+
          " AND NVL(PE.cg_tipos_credito,PN.cg_tipos_credito) IN ('A','D')"+
          " AND PE.ic_producto_nafin = 4"+
          " AND A.ic_epo = PE.ic_epo";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        while(rs.next()) {
  	      vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString("ic_epo")==null)?"":rs.getString("ic_epo"));
	        vecColumnas.addElement((rs.getString("cg_razon_social")==null)?"":rs.getString("cg_razon_social"));
	        vecFilas.addElement(vecColumnas);
	      }
      con.cierraStatement();
    } catch(Exception e){
      throw new NafinException("SIST0001");
    } finally {
      if(con.hayConexionAbierta())
        con.cierraConexionDB();
      }
	return vecFilas;
 }

public Vector getComboSolicitudRelacionada(String ic_if,String ic_epo, String solicitud,String moneda)
 throws NafinException
 {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        if (solicitud.equals("A")) {
          qrySentencia  =
            "SELECT ic_linea_credito_dm "+
            "  FROM dis_linea_credito_dm "+
            " WHERE ic_if = "+ic_if+" "+
            "   AND ic_epo = "+ic_epo+" "+
            "   AND cg_tipo_solicitud = 'I' "+
			"	AND ic_estatus_linea = 12 " +
            "   AND TRUNC (df_vencimiento_adicional) > TRUNC (SYSDATE) ";
        }
        else if (solicitud.equals("R")) {
            qrySentencia  =
              "SELECT ic_linea_credito_dm "+
              "  FROM dis_linea_credito_dm "+
              " WHERE ic_if = "+ic_if+" "+
              "   AND ic_epo = "+ic_epo+" "+
              "   AND cg_tipo_solicitud = 'I' "+
			  "	  AND ic_estatus_linea in(11,12) "+
              "   AND TRUNC (df_vencimiento_adicional) < TRUNC (SYSDATE+90) ";
/*              "   AND ic_linea_credito_dm NOT IN (SELECT ic_linea_credito_dm_padre "+
              "                                     FROM dis_linea_credito_dm "+
              "                                    WHERE cg_tipo_solicitud IN ('I',  'R') "+
              "			                             AND ic_linea_credito_dm_padre IS NOT NULL) ";*/
        }
        qrySentencia += " AND ic_moneda = "+moneda+" ";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString("ic_linea_credito_dm")==null)?"":rs.getString("ic_linea_credito_dm"));
	        vecFilas.addElement(vecColumnas);
	      }
      con.cierraStatement();
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
    }

 public boolean getLineaExistente(String moneda,String ic_if,String ic_epo)	//DEPRECATED USE validaLinea
 throws NafinException
 {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    boolean bandera=false;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
          "SELECT ic_linea_credito_dm"+
          " FROM DIS_LINEA_CREDITO_DM"+
          " WHERE cg_tipo_solicitud='I'"+
          " AND ic_if = "+ic_if+" "+
          " AND ic_epo = "+ic_epo+" "+
          " AND ic_moneda = "+moneda+" ";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        if (rs.next())
          bandera = true;
        con.cierraStatement();
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return bandera;
    }


 public Vector setLineasCredito(String iNoUsuario,String in_total_lineas,String in_total_monto,String in_total_lineasDol,String in_total_montoDol,String anoNafinE[],String ic_epos[],String asolic[],String afolio[],String amoneda[],String amontoAuto[],String aplaz[],String acobint[],String avencimiento[],String acliAfil[],String anCtaEpo[],String aifBanco[],String anCtaIf[], String ic_if)
 throws NafinException
 {
    AccesoDB    con           = null;
    ResultSet   rs            = null;
    String      qrySentencia  = "";
    Vector		  vecColumnas		= new Vector();
    Vector		  vecFilas			= new Vector();
    String cc_acuse;
    boolean     ok = true;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        Acuse acuse = new Acuse(4,"4","dis",con);
        cc_acuse=acuse.toString();
        qrySentencia  =
          "INSERT INTO dis_acuse4 (cc_acuse,df_acuse,ic_usuario)"+
          "VALUES('"+cc_acuse+"',sysdate,'"+iNoUsuario+"')";
        System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
        System.out.println("\nAcuse creado...\n");
        setDisRelAcuse4(cc_acuse,in_total_lineas,in_total_monto,in_total_lineasDol,in_total_montoDol,con);
        setDisLineasCreditoDM(cc_acuse,anoNafinE,ic_epos,asolic,afolio,amoneda,amontoAuto,aplaz,acobint,
				avencimiento,acliAfil,anCtaEpo,aifBanco,anCtaIf,ic_if,con);
        qrySentencia =
          "SELECT to_char(sysdate,'dd/mm/yyyy') as fecha, to_char(sysdate,'HH:MI') as hora,"+
          " ' '  as nomUsuario"+
          " FROM dual ";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        if(rs.next()){
          vecColumnas = new Vector();
	  			vecColumnas.addElement(cc_acuse);
	  			vecColumnas.addElement(acuse.formatear());
  				vecColumnas.addElement((rs.getString("fecha")==null)?"":rs.getString("fecha"));
	  			vecColumnas.addElement((rs.getString("hora")==null)?"":rs.getString("hora"));
	  			vecColumnas.addElement((rs.getString("nomUsuario")==null)?"":rs.getString("nomUsuario"));
        	vecFilas.addElement(vecColumnas);
        }
        con.cierraStatement();
      } catch(Exception e){
          ok = false;
					log.error("setLineasCredito(Exception)", e);
					throw new AppException("Error al guardar las Lineas de Credito", e);
      } finally {
      	if(con.hayConexionAbierta())
          con.terminaTransaccion(ok);
        	con.cierraConexionDB();
      }
		return vecFilas;
    }

private void setDisRelAcuse4(String cc_acuse, String in_total_lineas, String in_total_monto,String in_total_lineasDol, String in_total_montoDol, AccesoDB con)
 throws NafinException
  {
    String      qrySentencia  = "";
    boolean     ok = true;
    	try {
        qrySentencia  =
          "INSERT INTO disrel_acuse4_moneda (CC_ACUSE,IC_MONEDA,IN_TOTAL_LINEAS,FN_TOTAL_MONTO) "+
          "VALUES('"+cc_acuse+"',1,"+Integer.parseInt(in_total_lineas)+","+Double.parseDouble(in_total_monto)+")";
        System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
          System.out.println("\n...agregar\n");
        qrySentencia  =
          "INSERT INTO disrel_acuse4_moneda (CC_ACUSE,IC_MONEDA,IN_TOTAL_LINEAS,FN_TOTAL_MONTO) "+
          "VALUES('"+cc_acuse+"',54,"+Integer.parseInt(in_total_lineasDol)+","+Double.parseDouble(in_total_montoDol)+")";
        System.out.println(qrySentencia);
        con.ejecutaSQL(qrySentencia);
          System.out.println("\n...agregar\n");
      } catch(Exception e){
          ok = false;
					throw new NafinException("SIST0001");
      }
  }

	private void setDisLineasCreditoDM(String cc_acuse, String anoNafinE[],
			String ic_epos[],String asolic[],String afolio[],String amoneda[],
			String amontoAuto[],String aplaz[],String acobint[],
			String avencimiento[],String acliAfil[],String anCtaEpo[],
			String aifBanco[],String anCtaIf[],String ic_if, AccesoDB con) {
		log.info("setDisLineasCreditoDM(E)");
		String      qrySentencia  = "";
		Vector		  vecColumnas		= new Vector();
		Vector		  vecFilas			= new Vector();
		boolean     ok = true;
		try {
			for (int i=0;i<anoNafinE.length;i++){
				if ("I".equals(asolic[i])){ 	//I = Inicial
					qrySentencia  =
							"INSERT INTO dis_linea_credito_dm (IC_LINEA_CREDITO_DM,CC_ACUSE,CG_TIPO_SOLICITUD,IC_MONEDA,FN_MONTO_AUTORIZADO,FN_MONTO_AUTORIZADO_TOTAL,IG_PLAZO,DF_VENCIMIENTO,DF_VENCIMIENTO_ADICIONAL,FN_SALDO_TOTAL,IC_TIPO_COBRO_INTERES,IC_PRODUCTO_NAFIN,IC_IF,IC_EPO,CG_NUM_CUENTA_EPO,CG_NUM_CUENTA_IF,IC_FINANCIERA,IC_LINEA_CREDITO_DM_PADRE,IC_ESTATUS_LINEA) "+
							"select nvl(max(ic_linea_credito_dm),0)+1,'"+
							cc_acuse+"','"+asolic[i]+"','"+
							amoneda[i]+"',"+
							amontoAuto[i]+","+amontoAuto[i]+","+
							aplaz[i]+",to_date('"+avencimiento[i]+"','dd/mm/yyyy'),to_date('"+
							avencimiento[i]+"','dd/mm/yyyy'),"+amontoAuto[i]+","+
							acobint[i]+",4,"+ic_if+","+
							ic_epos[i]+",'"+anCtaEpo[i]+"','"+
							anCtaIf[i]+"',";
					if ("".equals(aifBanco[i]))
						qrySentencia = qrySentencia+"null";
					else
						qrySentencia = qrySentencia+""+aifBanco[i]+"";
					qrySentencia = qrySentencia+",'',12 "+
					" from dis_linea_credito_dm";
					con.ejecutaSQL(qrySentencia);
				}else if ("A".equals(asolic[i])){		//A = Ampliacion
					vecFilas=getSolicRelac(afolio[i]);
					vecColumnas = (Vector)vecFilas.get(0);
					//String montAuto = (String)vecColumnas.get(0);
					//String saldtot = (String)vecColumnas.get(1);
					String fechaVenc = (String)vecColumnas.get(2);

					Calendar f1 = Calendar.getInstance();
					Calendar f2 = Calendar.getInstance();
					f1.set(Integer.parseInt(fechaVenc.substring(6,10)),Integer.parseInt(fechaVenc.substring(3,5))-1,Integer.parseInt(fechaVenc.substring(0,2)));
					java.util.Date dFechaVenc1 = f1.getTime();
					f2.set(Integer.parseInt(avencimiento[i].substring(6,10)),Integer.parseInt(avencimiento[i].substring(3,5))-1,Integer.parseInt(avencimiento[i].substring(0,2)));
					java.util.Date dFechaVenc2 = f2.getTime();
					int res = dFechaVenc1.compareTo(dFechaVenc2);
					//                System.out.println("\n******"+dFechaVenc1+" "+dFechaVenc2+" "+res);
					if (res==-1)
					fechaVenc = avencimiento[i];
					//Historico
					qrySentencia  =
							" INSERT INTO dis_linea_credito_dm (IC_LINEA_CREDITO_DM,CC_ACUSE,CG_TIPO_SOLICITUD,IC_MONEDA,FN_MONTO_AUTORIZADO,FN_MONTO_AUTORIZADO_TOTAL,IG_PLAZO,DF_VENCIMIENTO,DF_VENCIMIENTO_ADICIONAL,FN_SALDO_TOTAL,IC_TIPO_COBRO_INTERES,IC_PRODUCTO_NAFIN,IC_IF,IC_EPO,CG_NUM_CUENTA_EPO,CG_NUM_CUENTA_IF,IC_FINANCIERA,IC_LINEA_CREDITO_DM_PADRE,IC_ESTATUS_LINEA) "+
							" select nvl(max(lc1.ic_linea_credito_dm),0)+1 "+
							" ,'"+cc_acuse+"','A',lc2.IC_MONEDA"+
							" ,lc2.FN_MONTO_AUTORIZADO,lc2.FN_MONTO_AUTORIZADO_TOTAL,lc2.IG_PLAZO,lc2.DF_VENCIMIENTO,lc2.DF_VENCIMIENTO_ADICIONAL,lc2.FN_SALDO_TOTAL,lc2.IC_TIPO_COBRO_INTERES,lc2.IC_PRODUCTO_NAFIN"+
							" ,lc2.IC_IF,lc2.IC_EPO,lc2.CG_NUM_CUENTA_EPO,lc2.CG_NUM_CUENTA_IF,lc2.IC_FINANCIERA,lc2.IC_LINEA_CREDITO_DM,lc2.IC_ESTATUS_LINEA"+
							" from dis_linea_credito_dm lc1,dis_linea_credito_dm lc2"+
							" where lc2.ic_linea_credito_dm = "+afolio[i]+
							" group by lc2.IC_MONEDA,lc2.FN_MONTO_AUTORIZADO,lc2.FN_MONTO_AUTORIZADO_TOTAL,lc2.IG_PLAZO,lc2.DF_VENCIMIENTO,lc2.DF_VENCIMIENTO_ADICIONAL,lc2.FN_SALDO_TOTAL,lc2.IC_TIPO_COBRO_INTERES,lc2.IC_PRODUCTO_NAFIN"+
							" ,lc2.IC_IF,lc2.IC_EPO,lc2.CG_NUM_CUENTA_EPO,lc2.CG_NUM_CUENTA_IF,lc2.IC_FINANCIERA,lc2.IC_LINEA_CREDITO_DM,lc2.IC_ESTATUS_LINEA";
					con.ejecutaSQL(qrySentencia);
					qrySentencia =
							" UPDATE dis_linea_credito_dm  SET "+
							" FN_MONTO_AUTORIZADO = "+amontoAuto[i]+
							" ,FN_MONTO_AUTORIZADO_TOTAL = "+amontoAuto[i]+
							" ,FN_SALDO_TOTAL = "+amontoAuto[i]+"-(FN_MONTO_AUTORIZADO_TOTAL-FN_SALDO_TOTAL)"+
				         ", DF_VENCIMIENTO_ADICIONAL = to_date('"+avencimiento[i]+"','dd/mm/yyyy') "+  
							" WHERE ic_linea_credito_dm = "+afolio[i];
					con.ejecutaSQL(qrySentencia);
				}else if ("R".equals(asolic[i])){	//R = Renovacion

					vecFilas=getSolicRelac(afolio[i]);
					vecColumnas = (Vector)vecFilas.get(0);
					String fechaVenc = (String)vecColumnas.get(2);
					String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());

					Calendar f1 = Calendar.getInstance();
					Calendar f2 = Calendar.getInstance();
					f1.set(Integer.parseInt(fechaVenc.substring(6,10)),Integer.parseInt(fechaVenc.substring(3,5))-1,Integer.parseInt(fechaVenc.substring(0,2)));
					java.util.Date dFechaVenc1 = f1.getTime();
					f2.set(Integer.parseInt(fechaHoy.substring(6,10)),Integer.parseInt(fechaHoy.substring(3,5))-1,Integer.parseInt(fechaHoy.substring(0,2)));
					java.util.Date dFechaVenc2 = f2.getTime();
					int res = dFechaVenc1.compareTo(dFechaVenc2);
					//                System.out.println("\n******"+dFechaVenc1+" "+dFechaVenc2+" "+res);
					if (res>0)
						fechaVenc = fechaHoy;
					//Historico

					qrySentencia  =
							" INSERT INTO dis_linea_credito_dm (IC_LINEA_CREDITO_DM,CC_ACUSE,CG_TIPO_SOLICITUD,IC_MONEDA,FN_MONTO_AUTORIZADO,FN_MONTO_AUTORIZADO_TOTAL,IG_PLAZO,DF_VENCIMIENTO,DF_VENCIMIENTO_ADICIONAL,FN_SALDO_TOTAL,IC_TIPO_COBRO_INTERES,IC_PRODUCTO_NAFIN,IC_IF,IC_EPO,CG_NUM_CUENTA_EPO,CG_NUM_CUENTA_IF,IC_FINANCIERA,IC_LINEA_CREDITO_DM_PADRE,IC_ESTATUS_LINEA) "+
							" select nvl(max(lc1.ic_linea_credito_dm),0)+1 "+
							" ,'"+cc_acuse+"','R',lc2.IC_MONEDA"+
							" ,lc2.FN_MONTO_AUTORIZADO,lc2.FN_MONTO_AUTORIZADO_TOTAL,lc2.IG_PLAZO,lc2.DF_VENCIMIENTO,lc2.DF_VENCIMIENTO_ADICIONAL,lc2.FN_SALDO_TOTAL,lc2.IC_TIPO_COBRO_INTERES,lc2.IC_PRODUCTO_NAFIN"+
							" ,lc2.IC_IF,lc2.IC_EPO,lc2.CG_NUM_CUENTA_EPO,lc2.CG_NUM_CUENTA_IF,lc2.IC_FINANCIERA,lc2.IC_LINEA_CREDITO_DM,lc2.IC_ESTATUS_LINEA"+
							" from dis_linea_credito_dm lc1,dis_linea_credito_dm lc2"+
							" where lc2.ic_linea_credito_dm = "+afolio[i]+
							" group by lc2.IC_MONEDA,lc2.FN_MONTO_AUTORIZADO,lc2.FN_MONTO_AUTORIZADO_TOTAL,lc2.IG_PLAZO,lc2.DF_VENCIMIENTO,lc2.DF_VENCIMIENTO_ADICIONAL,lc2.FN_SALDO_TOTAL,lc2.IC_TIPO_COBRO_INTERES,lc2.IC_PRODUCTO_NAFIN"+
							" ,lc2.IC_IF,lc2.IC_EPO,lc2.CG_NUM_CUENTA_EPO,lc2.CG_NUM_CUENTA_IF,lc2.IC_FINANCIERA,lc2.IC_LINEA_CREDITO_DM,lc2.IC_ESTATUS_LINEA";
					con.ejecutaSQL(qrySentencia);
					qrySentencia =
							" UPDATE dis_linea_credito_dm  SET "+
							" FN_MONTO_AUTORIZADO = "+amontoAuto[i]+
							" ,FN_MONTO_AUTORIZADO_TOTAL = "+amontoAuto[i]+
							" ,FN_SALDO_TOTAL = "+amontoAuto[i]+"-(FN_MONTO_AUTORIZADO_TOTAL-FN_SALDO_TOTAL)"+
							" ,DF_VENCIMIENTO_ADICIONAL = TO_DATE('"+avencimiento[i]+"','dd/mm/yyyy') "+
							" ,IC_ESTATUS_LINEA = 12"+
							" WHERE ic_linea_credito_dm = "+afolio[i];
					con.ejecutaSQL(qrySentencia);

				}//else Inicial
			}//for
		} catch(Exception e){
			ok = false;
			throw new AppException("Error al establecer las lineas de credito de distribuidores", e);
		} finally {
			log.info("setDisLineasCreditoDM(S)");
		}
	}


 public Vector getSolicRelac(String ic_linea_credito_dm)
 throws NafinException
 {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
          "select IC_LINEA_CREDITO_DM,"+
          " FN_MONTO_AUTORIZADO_TOTAL,"+
          " FN_SALDO_TOTAL,"+
          " to_char(DF_VENCIMIENTO_ADICIONAL,'dd/mm/yyyy') AS VENCIMIENTO"+
          " from dis_linea_credito_dm"+
          " where IC_LINEA_CREDITO_DM="+ic_linea_credito_dm+" ";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
	 while(rs.next()) {
  	    	vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
	        vecColumnas.addElement((rs.getString("FN_SALDO_TOTAL")==null)?"":rs.getString("FN_SALDO_TOTAL"));
	        vecColumnas.addElement((rs.getString("VENCIMIENTO")==null)?"":rs.getString("VENCIMIENTO"));
	        vecFilas.addElement(vecColumnas);
	      }
      con.cierraStatement();
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
      }
		return vecFilas;
    }

 public Vector getSolicitudes(String ic_if, String tipoLinCre,String fechaVenc1,String fechaVenc2,String epo,String tipoCoInt,String distrib,String numCred,String fechaOper1,String fechaOper2,String monto1,String monto2)
  throws NafinException {
    String		qrySentenciaDM 	= "";
    String		qrySentenciaCCC	= "";
    String 		qrySentencia 	= "";
    String		condicion	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
   	try {
      	con = new AccesoDB();
        con.conexionDB();

       if (!("".equals(tipoLinCre)))
          condicion += "    AND x.cg_tipo_credito = '"+tipoLinCre+"' ";
        if (!("".equals(fechaVenc1)) && !("".equals(fechaVenc2)))
          condicion += "    AND TRUNC(DOC.df_fecha_venc_credito) BETWEEN TRUNC(TO_DATE('"+fechaVenc1+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+fechaVenc2+"','dd/mm/yyyy')) ";
        if (!("".equals(epo)))
          condicion += "    AND DOC.ic_epo = "+epo+" ";
        if (!("".equals(tipoCoInt)))
          condicion += "    AND TC.ic_tipo_cobro_interes = "+tipoCoInt+" ";
        if (!("".equals(distrib)))
          condicion += "    AND DOC.ic_pyme = "+distrib+" ";
        if (!("".equals(numCred)))
          condicion += "    AND DOC.ic_documento = "+numCred;
        if (!("".equals(fechaOper1)) && !("".equals(fechaOper2)))
          condicion += "    AND TRUNC(DS.df_fecha_seleccion) BETWEEN TRUNC(TO_DATE('"+fechaOper1+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+fechaOper2+"','dd/mm/yyyy')) ";
        if (!("".equals(monto1)) && !("".equals(monto2)))
	  condicion += "    AND DS.fn_importe_recibir BETWEEN "+monto1+" AND "+monto2+" ";
        qrySentenciaDM  =
	        "SELECT doc.ic_documento, doc.ig_numero_docto, e.cg_razon_social AS epo, "+
	        "       p.cg_razon_social AS pyme, x.cg_tipo_credito, "+
	        "       DECODE (NVL (pe.cg_responsable_interes, pn.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable, "+
	        "       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS cobranza, "+
	        "       TO_CHAR (DS.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_oper, "+
	        "       m.cd_nombre, doc.fn_monto, "+
        	"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', 'Sin Conversion','P', 'Peso - Dolar','') AS conversion, "+
	        "       doc.fn_tipo_cambio AS tipo_cambio, "+
	        "       DECODE (doc.ic_moneda,1, doc.fn_monto,54, doc.fn_monto * doc.fn_tipo_cambio) AS monto_val, "+
	        "       doc.ct_referencia, "+
	        "       DS.FN_VALOR_TASA AS tasa_int, "+
	        "       doc.ig_plazo_descuento, "+
	        "       TO_CHAR (doc.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc, "+
	        "       DS.fn_importe_interes AS monto_int, tc.cd_descripcion AS tipo_cob_int, "+
	        "       ed.cd_descripcion, m.ic_moneda, "+
			"		ds.fn_importe_recibir,"+
			"		ct.cd_nombre||' '||ds.cg_rel_mat||' '||ds.fn_puntos as referencia_tasa,"+
			"		doc.ig_plazo_credito ,"+
			"     doc.ic_estatus_docto "+
	        "  FROM dis_documento doc, "+
	        "       comcat_epo e, "+
	        "       comcat_pyme p, "+
	        "       comrel_pyme_epo_x_producto x, "+
	        "       comcat_producto_nafin pn, "+
	        "       comrel_producto_epo pe, "+
	        "       comcat_moneda m, "+
	        "       comcat_estatus_docto ed, "+
	        "       dis_linea_credito_dm lc, "+
	        "       comcat_tipo_cobro_interes tc, "+
	        "       dis_docto_seleccionado ds, "+
			"		comcat_tasa ct "+
			" WHERE doc.ic_estatus_docto in(3,24)"   +//MOD +(in(3,24))
	        "   AND doc.ic_epo = e.ic_epo "+
	        "   AND doc.ic_pyme = p.ic_pyme "+
	        "   AND x.ic_epo = doc.ic_epo "+
	        "   AND x.ic_pyme = doc.ic_pyme "+
	        "   AND x.ic_producto_nafin = doc.ic_producto_nafin "+
	        "   AND doc.ic_producto_nafin = pn.ic_producto_nafin "+
	        "   AND e.ic_epo = pe.ic_epo "+
	        "   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
	        "   AND pn.ic_producto_nafin = 4 "+
	        "   AND lc.ic_moneda = m.ic_moneda "+
	        "   AND doc.ic_estatus_docto = ed.ic_estatus_docto "+
	        "   AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
	        "   AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+
	        "   AND ds.ic_documento = doc.ic_documento "+
	        "   AND lc.ic_epo = doc.ic_epo "+
			"	AND DS.ic_tasa = CT.ic_tasa"+
            "   AND doc.ic_documento not in(select ic_documento from dis_solicitud)" +
        	"   AND lc.ic_if = "+ic_if+" "+
        	" 	AND e.cs_habilitado = 'S' " +
	          condicion;
        qrySentenciaCCC =
        	" SELECT doc.ic_documento, doc.ig_numero_docto, e.cg_razon_social AS epo,"   +
		"        p.cg_razon_social AS pyme, x.cg_tipo_credito,"   +
		"        DECODE (NVL (pe.cg_responsable_interes, pn.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable,"   +
		"        DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS cobranza,"   +
		"        TO_CHAR (DS.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_oper,"   +
		"        m.cd_nombre, doc.fn_monto,"   +
		"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', 'Sin Conversion','P', 'Peso - Dolar','') AS conversion,"   +
		"        doc.fn_tipo_cambio AS tipo_cambio,"   +
		"        DECODE (doc.ic_moneda,1, doc.fn_monto,54, doc.fn_monto * doc.fn_tipo_cambio) AS monto_val,"   +
		"        doc.ct_referencia,"   +
		"        DS.FN_VALOR_TASA AS tasa_int,"   +
		"        doc.ig_plazo_descuento,"   +
		"        TO_CHAR (doc.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc,"   +
		"        DS.fn_importe_interes AS monto_int, tc.cd_descripcion AS tipo_cob_int,"   +
		"        ed.cd_descripcion, m.ic_moneda,"   +
		"		ds.fn_importe_recibir,"+
		"		ct.cd_nombre||' '||ds.cg_rel_mat||' '||ds.fn_puntos as referencia_tasa,"+
		"		doc.ig_plazo_credito ,"+
		"     doc.ic_estatus_docto "+
		"   FROM dis_documento doc,"   +
		"        comcat_epo e,"   +
		"        comcat_pyme p,"   +
		"      	 comrel_pyme_epo_x_producto x,"   +
		"        comcat_producto_nafin pn,"   +
		"        comrel_producto_epo pe,"   +
		"        comcat_moneda m,"   +
		"        comcat_estatus_docto ed,"   +
		"        com_linea_credito lc,"   +
		"        comcat_tipo_cobro_interes tc,"   +
		"        dis_docto_seleccionado ds,"   +
		"		 comcat_tasa ct"+
		"  WHERE doc.ic_estatus_docto in(3,24)"   +//= 3 MOD +(in(3,24))
		"    AND doc.ic_epo = e.ic_epo"   +
		"    AND doc.ic_pyme = p.ic_pyme"   +
		"    AND x.ic_epo = doc.ic_epo"   +
		"    AND x.ic_pyme = doc.ic_pyme"   +
		"    AND x.ic_producto_nafin = doc.ic_producto_nafin"   +
		"    AND doc.ic_producto_nafin = pn.ic_producto_nafin"   +
		"    AND e.ic_epo = pe.ic_epo"   +
		"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
		"    AND pn.ic_producto_nafin = 4"   +
		"    AND lc.ic_moneda = m.ic_moneda"   +
		"    AND doc.ic_estatus_docto = ed.ic_estatus_docto"   +
		"    AND doc.ic_linea_credito = lc.ic_linea_credito"   +
		"    AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes"   +
		"    AND ds.ic_documento = doc.ic_documento"   +
		"    AND lc.ic_epo = doc.ic_epo"   +
		"	 AND DS.ic_tasa = CT.ic_tasa"+
		"    AND lc.ic_if = "+ic_if+
		" 	 AND e.cs_habilitado = 'S' " +
		condicion;
        qrySentencia =	"select * from(	";
        	if("D".equals(tipoLinCre))
                	qrySentencia += qrySentenciaDM;
                else if("C".equals(tipoLinCre))
                	qrySentencia += qrySentenciaCCC;
                else
                	qrySentencia += qrySentenciaDM + " UNION ALL " +qrySentenciaCCC;
        qrySentencia +=	"  )";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        while(rs.next()) {
//System.out.println("Entro al RS");
  	      vecColumnas = new Vector();
/*0*/	        vecColumnas.addElement((rs.getString("ic_documento")==null)?"":rs.getString("ic_documento"));
/*1*/	        vecColumnas.addElement((rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto"));
/*2*/	        vecColumnas.addElement((rs.getString("EPO")==null)?"":rs.getString("EPO"));
/*3*/	        vecColumnas.addElement((rs.getString("PYME")==null)?"":rs.getString("PYME"));
/*4*/	        vecColumnas.addElement((rs.getString("cg_tipo_credito")==null)?"":rs.getString("cg_tipo_credito"));
/*5*/	        vecColumnas.addElement((rs.getString("RESPONSABLE")==null)?"":rs.getString("RESPONSABLE"));
/*6*/	        vecColumnas.addElement((rs.getString("COBRANZA")==null)?"":rs.getString("COBRANZA"));
/*7*/	        vecColumnas.addElement((rs.getString("fecha_oper")==null)?"":rs.getString("fecha_oper"));
/*8*/	        vecColumnas.addElement((rs.getString("cd_nombre")==null)?"":rs.getString("cd_nombre"));
/*9*/	        vecColumnas.addElement((rs.getString("fn_monto")==null)?"":rs.getString("fn_monto"));
/*10*/	        vecColumnas.addElement((rs.getString("CONVERSION")==null)?"":rs.getString("CONVERSION"));
/*11*/	        vecColumnas.addElement((rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO"));
/*12*/	        vecColumnas.addElement((rs.getString("MONTO_VAL")==null)?"":rs.getString("MONTO_VAL"));
/*13*/	        vecColumnas.addElement((rs.getString("ct_referencia")==null)?"":rs.getString("ct_referencia"));
/*14*/         	vecColumnas.addElement((rs.getString("TASA_INT")==null)?"":rs.getString("TASA_INT"));
/*15*/	        vecColumnas.addElement((rs.getString("ig_plazo_descuento")==null)?"":rs.getString("ig_plazo_descuento"));
/*16*/	        vecColumnas.addElement((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"));
/*17*/	        vecColumnas.addElement((rs.getString("MONTO_INT")==null)?"":rs.getString("MONTO_INT"));
/*18*/	        vecColumnas.addElement((rs.getString("TIPO_COB_INT")==null)?"":rs.getString("TIPO_COB_INT"));
/*19*/	        vecColumnas.addElement((rs.getString("cd_descripcion")==null)?"":rs.getString("cd_descripcion"));
/*20*/	        vecColumnas.addElement((rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda"));
/*21*/	        vecColumnas.addElement((rs.getString("fn_importe_recibir")==null)?"":rs.getString("fn_importe_recibir"));
/*22*/	        vecColumnas.addElement((rs.getString("referencia_tasa")==null)?"":rs.getString("referencia_tasa"));
/*23*/	        vecColumnas.addElement((rs.getString("ig_plazo_credito")==null)?"":rs.getString("ig_plazo_credito"));
/*24*/	        vecColumnas.addElement((rs.getString("ic_estatus_docto")==null)?"":rs.getString("ic_estatus_docto"));
	        vecFilas.addElement(vecColumnas);
	      }
      con.cierraStatement();
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
 }
		return vecFilas;
 }//getSolicitudes

 public void setDisDocumento(String ic_documento[],String ic_estatus_docto[],String ct_cambio_motivo[])
 throws NafinException
 {
    AccesoDB    con           = null;
    String      qrySentencia  = "";
    String      aux           = "";
    boolean     ok = true;
    	try {
      	con = new AccesoDB();
        con.conexionDB();
        for (int j=0;j<ic_documento.length;j++) {
          if (!("".equals(ic_estatus_docto[j]))) {
            qrySentencia  =
              "UPDATE DIS_DOCUMENTO"+
              " SET ic_estatus_docto = "+ic_estatus_docto[j]+" "+
              " WHERE ic_documento = "+ic_documento[j]+" ";
            System.out.println(qrySentencia);
            con.ejecutaSQL(qrySentencia);


            if ("20".equals(ic_estatus_docto[j])){
					aux="14";
				}else if ("9".equals(ic_estatus_docto[j])){
					aux="30";
				}else if("2".equals(ic_estatus_docto[j])){
					aux="2";
				}else if("3".equals(ic_estatus_docto[j])){
					aux="34";
				}



            qrySentencia  =
              "INSERT INTO"+
              " DIS_CAMBIO_ESTATUS(dc_fecha_cambio,ic_documento,ic_cambio_estatus,ct_cambio_motivo)"+
              " VALUES (SYSDATE,"+ic_documento[j]+","+aux+",'"+ct_cambio_motivo[j]+"')";
            System.out.println(qrySentencia);
            con.ejecutaSQL(qrySentencia);
          }
    		}
      } catch(Exception e){
          ok = false;
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
          con.terminaTransaccion(ok);
       	con.cierraConexionDB();
      }
 }//setDisDocumento

	public void setDisDocumentoBaja(String ic_documento[],String selecc[],String ic_estatus_docto,String ct_cambio_motivo[]) throws NafinException{
		AccesoDB con           = null;
		String   qrySentencia  = "";
		String   aux           = "";
		boolean  ok            = true;
		try{
			con = new AccesoDB();
			con.conexionDB();
			for (int j=0;j<ic_documento.length;j++) {
				if ("ok".equals(selecc[j])){
					if ("4".equals(ic_estatus_docto) || "24".equals(ic_estatus_docto) || "37".equals(ic_estatus_docto) || "44".equals(ic_estatus_docto)){
						aux = "5";
					}
					qrySentencia =
						"UPDATE DIS_DOCUMENTO"+
						" SET ic_estatus_docto = "+aux+" "+
						" WHERE ic_documento = "+ic_documento[j]+" ";
					System.out.println(qrySentencia);
					con.ejecutaSQL(qrySentencia);
					qrySentencia =
						"INSERT INTO"+
						" DIS_CAMBIO_ESTATUS(dc_fecha_cambio,ic_documento,ic_cambio_estatus,ct_cambio_motivo)"+
						" VALUES (SYSDATE,"+ic_documento[j]+","+ic_estatus_docto+",'"+ct_cambio_motivo[j]+"')";
					System.out.println(qrySentencia);
					con.ejecutaSQL(qrySentencia);
				}
			}
		} catch(Exception e){
			ok = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally{
			if(con.hayConexionAbierta())
				con.terminaTransaccion(ok);
			con.cierraConexionDB();
		}
	}//setDisDocumento

 public Vector getConsultaBajaDocs(String ic_epo,String distribuidor,String moneda,String estatusdocto,String monto1,String monto2,String numdoc,String cdescuento,String numaccuse,String fechaemision1,String fechaemision2,String fechavenc1,String fechavenc2,String fechapub1,String fechapub2,String modplazo, String NOnegociable)
  throws NafinException {
    String		qrySentencia 	= "";
    String		condicion    	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
   	try {
      	con = new AccesoDB();
        con.conexionDB();
        if (!("".equals(distribuidor)))
          condicion +="    AND DOC.ic_pyme = "+distribuidor+" ";
        if (!("".equals(moneda)))
          condicion +="    AND DOC.ic_moneda = "+moneda+" ";
        if (!("".equals(monto1)) && !("".equals(monto2)) && "".equals(cdescuento))
          condicion +="    AND DOC.fn_monto BETWEEN "+monto1+" AND "+monto2+" ";
        if (!("".equals(monto1)) && !("".equals(monto2)) && "condesc".equals(cdescuento))
          condicion +="    AND DOC.fn_monto*(DOC.fn_porc_descuento/100) BETWEEN "+monto1+" AND "+monto2+" ";
        if (!("".equals(numdoc)))
          condicion +="    AND DOC.ig_numero_docto = '"+numdoc+"' ";
        if (!("".equals(modplazo)))
          condicion +="    AND doc.ic_tipo_financiamiento = '"+modplazo+"' ";
        if (!("".equals(numaccuse)))
          condicion +="    AND DOC.cc_acuse = '"+numaccuse+"' ";
        if (!("".equals(fechaemision1)) && !("".equals(fechaemision2)))
          condicion +="    AND TRUNC(DOC.df_fecha_emision) BETWEEN TRUNC(TO_DATE('"+fechaemision1+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+fechaemision2+"','dd/mm/yyyy')) ";
        if (!("".equals(fechapub1)) && !("".equals(fechapub2)))
          condicion +="    AND TRUNC(DOC.df_carga) BETWEEN TRUNC(TO_DATE('"+fechapub1+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+fechapub2+"','dd/mm/yyyy')) ";
        if (!("".equals(fechavenc1)) && !("".equals(fechavenc2)))
          condicion +="    AND TRUNC(DOC.df_fecha_venc) BETWEEN TRUNC(TO_DATE('"+fechavenc1+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+fechavenc2+"','dd/mm/yyyy')) ";
        if (!("".equals(estatusdocto)))
          condicion +="    AND DOC.ic_estatus_docto = "+estatusdocto+" ";
        if ("".equals(estatusdocto) && NOnegociable.equals("N")){ //Fodea 029-2010 Distribuidores Fase II
          condicion +="    AND DOC.ic_estatus_docto = 2 ";
        }else if ("".equals(estatusdocto) && NOnegociable.equals("S")){
         condicion +="    AND DOC.ic_estatus_docto in( 2, 1 )";
        }
        qrySentencia  =
          "SELECT doc.ic_documento AS clave, p.cg_razon_social AS pyme, "+
          "       doc.ig_numero_docto AS numdoc, doc.cc_acuse AS acuse, "+
          "       TO_CHAR (doc.df_fecha_emision, 'dd/mm/yyyy') AS fecha_emision, "+
          "       TO_CHAR (doc.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
          "       TO_CHAR (doc.df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc, "+
          "       doc.ig_plazo_docto AS plazodoc, m.cd_nombre AS moneda, "+
          "       doc.fn_monto AS monto, "+
          "       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
          "          'N', '', "+
          "          'P', 'Dolar-Peso','') AS conversion, "+
          "       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
          "          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), "+
          "          '1') AS tipo_cambio, "+
          "       DECODE (doc.ic_moneda, "+
          "          1, doc.fn_monto, "+
          "          54, doc.fn_monto * tc.fn_valor_compra) AS monto_valuado, "+
          "       doc.ig_plazo_descuento AS plazdesc, "+
          "       doc.fn_porc_descuento AS porcdesc, "+
          "       (doc.fn_monto * (doc.fn_porc_descuento) / 100) AS monto_con_desc, "+
          "       tf.cd_descripcion AS modalidad_plazo, "+
          "       ed.cd_descripcion AS estatus, m.ic_moneda "+
					"       ,ed.IC_ESTATUS_DOCTO AS Noestatus "+
					"      ,doc.CG_VENTACARTERA as CG_VENTACARTERA "+
          "  FROM comcat_pyme p, "+
          "       dis_documento doc, "+
          "       comcat_moneda m, "+
          "       comcat_producto_nafin pn, "+
          "       comrel_producto_epo pe, "+
          "       comrel_pyme_epo_x_producto x, "+
          "       comcat_epo e, "+
          "       comcat_estatus_docto ed, "+
          "       com_tipo_cambio tc, "+
          "       comcat_tipo_financiamiento tf "+
          " WHERE doc.ic_epo = "+ic_epo+" "+
          "   AND p.ic_pyme = doc.ic_pyme "+
          "   AND doc.ic_moneda = m.ic_moneda "+
          "   AND x.ic_epo = doc.ic_epo "+
          "   AND x.ic_pyme = doc.ic_pyme "+
          "   AND x.ic_producto_nafin = doc.ic_producto_nafin "+
          "   AND doc.ic_producto_nafin = pn.ic_producto_nafin "+
          "   AND e.ic_epo = pe.ic_epo "+
          "   AND pe.ic_epo = doc.ic_epo "+
          "   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
          "   AND pn.ic_producto_nafin = 4 "+
          "   AND doc.ic_estatus_docto = ed.ic_estatus_docto "+
          "   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
          "                         FROM com_tipo_cambio "+
          "                        WHERE ic_moneda = m.ic_moneda) "+
          "   AND m.ic_moneda = tc.ic_moneda "+
          "   AND tf.ic_tipo_financiamiento = doc.ic_tipo_financiamiento "+condicion;
        System.out.println("qrySentencia----->"+qrySentencia);
        rs = con.queryDB(qrySentencia);
        while(rs.next()) {
  	      vecColumnas = new Vector();
	        vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
	        vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
	        vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
	        vecColumnas.addElement((rs.getString(4)==null)?"":rs.getString(4));
	        vecColumnas.addElement((rs.getString(5)==null)?"":rs.getString(5));
	        vecColumnas.addElement((rs.getString(6)==null)?"":rs.getString(6));
	        vecColumnas.addElement((rs.getString(7)==null)?"":rs.getString(7));
	        vecColumnas.addElement((rs.getString(8)==null)?"":rs.getString(8));
	        vecColumnas.addElement((rs.getString(9)==null)?"":rs.getString(9));
	        vecColumnas.addElement((rs.getString(10)==null)?"":rs.getString(10));
	        vecColumnas.addElement((rs.getString(11)==null)?"":rs.getString(11));
	        vecColumnas.addElement((rs.getString(12)==null)?"":rs.getString(12));
	        vecColumnas.addElement((rs.getString(13)==null)?"":rs.getString(13));
	        vecColumnas.addElement((rs.getString(14)==null)?"":rs.getString(14));
	        vecColumnas.addElement((rs.getString(15)==null)?"":rs.getString(15));
	        vecColumnas.addElement((rs.getString(16)==null)?"":rs.getString(16));
	        vecColumnas.addElement((rs.getString(17)==null)?"":rs.getString(17));
	        vecColumnas.addElement((rs.getString(18)==null)?"":rs.getString(18));
	        vecColumnas.addElement((rs.getString(19)==null)?"":rs.getString(19));
					vecColumnas.addElement((rs.getString(20)==null)?"":rs.getString(20));
					vecColumnas.addElement((rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA"));

	        vecFilas.addElement(vecColumnas);
	      }
      con.cierraStatement();
      } catch(Exception e){
          System.out.println("Error en getConsultaBajaDocs(...);");
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
   }
		return vecFilas;
 }//getConsultaBajaDocs

 public Vector getCambiosDocto(String ic_documento) throws NafinException {
    String		qrySentencia 	= "";
    AccesoDB	con           = null;
    ResultSet	rs 						= null;
    Vector 		vecFilas			= new Vector();
    Vector		vecColumnas		= new Vector();
   	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
          "SELECT TO_CHAR(dc_fecha_cambio,'dd/mm/yyyy') AS fech_cambio,"+
          "    TO_CHAR(df_fecha_emision_anterior,'dd/mm/yyyy') AS fech_emi_ant,"+
          "    ct_cambio_motivo,"+
          "    fn_monto_anterior,"+
          "    fn_monto_nuevo,"+
          "    TO_CHAR(df_fecha_emision_nueva,'dd/mm/yyyy') AS fech_emi_new,"+
          "    TO_CHAR(df_fecha_venc_anterior,'dd/mm/yyyy') AS fech_venc_ant,"+
          "    TO_CHAR(df_fecha_venc_nueva,'dd/mm/yyyy') AS fech_venc_new "+
          "FROM DIS_CAMBIO_ESTATUS "+
          "WHERE ic_documento="+ic_documento+" "+
          " AND ic_cambio_estatus = 8";
        System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        while(rs.next()) {
  	      vecColumnas = new Vector();
          vecColumnas.addElement((rs.getString("fech_cambio")==null)?"":rs.getString("fech_cambio"));
          vecColumnas.addElement((rs.getString("ct_cambio_motivo")==null)?"":rs.getString("ct_cambio_motivo"));
	        vecColumnas.addElement((rs.getString("fech_emi_ant")==null)?"":rs.getString("fech_emi_ant"));
	        vecColumnas.addElement((rs.getString("fech_emi_new")==null)?"":rs.getString("fech_emi_new"));
	        vecColumnas.addElement((rs.getString("fn_monto_anterior")==null)?"":rs.getString("fn_monto_anterior"));
	        vecColumnas.addElement((rs.getString("fn_monto_nuevo")==null)?"":rs.getString("fn_monto_nuevo"));
	        vecColumnas.addElement((rs.getString("fech_venc_ant")==null)?"":rs.getString("fech_venc_ant"));
	        vecColumnas.addElement((rs.getString("fech_venc_new")==null)?"":rs.getString("fech_venc_new"));
	        vecFilas.addElement(vecColumnas);
	      }
      con.cierraStatement();
      } catch(Exception e){
					throw new NafinException("SIST0001");
      } finally {
      	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
 }
		return vecFilas;
 }//getCambiosDocto

 //SUSTITUYE A GETLINEAEXISTENTE: Verifica si ya existe una linea inicial para una epo y una moneda, y valida que la fecha de vencimiento sea mayor al parametro de dias minimos
 public void validaLinea(String moneda,String ic_if,String ic_epo,String fechaVenc,String tipoSolic)
  throws NafinException{
	System.out.println("LineadmBean::validaLinea(E) ");
	String				qrySentencia	= "";
	AccesoDB			con				= null;
	ResultSet			rs				= null;
	PreparedStatement	ps				= null;
	int					diferencia 		= 0;
	try {
		con = new AccesoDB();
		con.conexionDB();
		if("I".equals(tipoSolic)){
			qrySentencia  =
				"SELECT ic_linea_credito_dm"+
				" FROM DIS_LINEA_CREDITO_DM"+
				" WHERE cg_tipo_solicitud='I'"+
				" AND ic_if = ? "+
				" AND ic_epo = ? "+
				" AND ic_moneda = ? ";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			ps.setString(2,ic_epo);
			ps.setString(3,moneda);
			//System.out.println(qrySentencia);
			rs = ps.executeQuery();
			if (rs.next()){
				throw new NafinException("DIST0028");
			}
			ps.close();
			rs.close();
		}
		qrySentencia = "SELECT TO_DATE('"+fechaVenc+"','dd/mm/yyyy')-(sysdate+fn_minimo_financiamiento)"+
			" FROM comcat_producto_nafin "+
			" WHERE ic_producto_nafin = 4";
		rs = con.queryDB(qrySentencia);
		if(rs.next()){
			diferencia = rs.getInt(1);
		}
		con.cierraStatement();
		if(diferencia <= 0)
			throw new NafinException("DIST0029");
	} catch(NafinException ne){
		throw ne;
	} catch(Exception e){
		System.out.println("LineadmBean::validaLinea Exception "+e);
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta())
		con.cierraConexionDB();
	System.out.println("LineadmBean::validaLinea(S)");
	}
 }
}
