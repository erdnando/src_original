package com.netro.distribuidores;


import com.nafin.ws.CargaDocDistribuidoresWSInfo;

import com.netro.exception.NafinException;

import java.io.BufferedReader;

import java.math.BigDecimal;

import java.rmi.RemoteException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.AppException;
import netropology.utilerias.Combos;
import netropology.utilerias.Registros;

@Remote
public interface CargaDocDist {

    public abstract void cancelaCarga(String icProcDocto,String icConsecutivo,String ic_epo)
        throws NafinException;

    public abstract String insertaCargaIndividual(
        String cve_acuse,
        String numDoctosMN,
        String montoTotalMN,
        String numDoctosDL,
        String montoTotalDL, 
        String ic_usuario,
        String seg_acuse,
        String ic_epo,
        String strAforo,
        String icProcDocto)
        throws NafinException;

    public abstract Vector getCamposAdicionales(String ic_epo,boolean esDetalle)
      throws NafinException;

    public abstract Vector getDoctoInsertado(String icProcDocto,String icEpo,String icConsecutivo,String tipoCredito)
      throws NafinException;

    public Vector getDoctoInsertado(String ccAcuse,String tipoCredito)
      throws NafinException;

   
    public HashMap tempIndividual(String df_fecha_venc,String df_fecha_emision,String ic_epo,String ic_proc_docto,String ig_numero_docto,String cg_pyme_epo_interno,String ic_moneda,String fn_monto,String ct_referencia,Vector cg_campo,String ic_tipo_financiamiento,String ig_plazo_dscto,String fn_porc_dscto, String cs_plazo_descuento, String ic_clasificacion, String cg_fecha_porc_desc,String negociable, String operaVentaCartera, String ic_tipo_Pago, String ic_pediodo)
      throws NafinException;
    public boolean tempMasiva(String df_fecha_venc,String df_fecha_emision,String ic_epo,String ic_proc_docto,String ig_numero_docto,String cg_pyme_epo_interno,String ic_moneda,String fn_monto,String ct_referencia,Vector cg_campo,String ic_tipo_financiamiento,String ig_plazo_dscto,String fn_porc_dscto,String ic_linea_credito_dm,String fecha_lim_dscto, String cg_fecha_porc_desc,String cg_clave, String negociable,String operaVentaCartera, String ic_tipo_Pago, String ic_pediodo)
      throws NafinException;

    public abstract String getClaveDoctoTmp()
      throws NafinException;

    public abstract int getNumCamposAdicionales(String ic_epo,boolean esDetalle)
      throws NafinException;

    public abstract String getDiasInhabiles()
      throws NafinException;

  public  abstract void insertaTmpDoctoDetalle(String icProcDocto,String icPyme,String igNumeroDocto,Vector cg_campo)
    throws NafinException;

  public  abstract void borraTmpDoctoDetalle(String id)
    throws NafinException;

  public abstract Vector getDetalleTmp(String icProcDocto,String igNumeroDocto,String icConsecutivo)
      throws NafinException;

/*  public abstract void validarUsuario(String esClaveEpo, String esLogin, String esPassword,
    String esIP, String esHost, String esSistema )
  throws NafinException;
*/
   
    public String  validacionesMasiva(    
          String ic_epo,
          String df_fecha_venc,
          String df_fecha_emision,
          String ig_numero_docto,
          String ic_moneda,
          String fn_monto,
          BigDecimal txtmtomindoc,
          BigDecimal txtmtomaxdoc,
          BigDecimal txtmtomindocdol,
          BigDecimal txtmtomaxdocdol,
          int linea,
          Vector vecCampos,
          int pipesporlinea,
          String ic_pyme,
          String cs_cifras_control,
          String ic_tipo_financiamiento,
          String df_limite_dscto,
          String cg_clave,
          String ig_plazo_descuento,
          String fn_porc_descuento,
          String DescuentoAutomatico,
          String TipoCredito, 
          String limiteLineaCredito,
          String negociable,
			 String ventaCartera,
			 String strPerfil, //F032-2014,
			 String tipo_Pago,  //F09-2015
			 String pediodo , //F09-2015
			 String meseSinIntereses , //F09-2015
			 String cmbMeses //F09-2015
          )
      throws NafinException;

  public abstract Vector getFechas(String ic_epo)
  	throws NafinException;

    public abstract boolean esParametrizado(String ic_epo)
    throws NafinException;

    public abstract void operaCifrasControl(String ses_ic_epo)
    throws NafinException;

    public String getTipoCambio(String ic_epo)
    throws NafinException;

    public Combos getPlazos()
    throws NafinException;
	//DEPRECATED METHOD
    public Combos getComboLineas(String ic_epo)
    throws NafinException;

    public Vector getLineas(String ic_epo)
    throws NafinException;	
	
    public Vector monitorLineas(String ic_epo)
    throws NafinException;

    public void actualizaDetalle(String ic_epo,String ic_proc_docto,String lineaTodos,String chkTodos,String []lineaReg,String []plazo,String []fecha_vto,String []igNumeroDocto)
    throws NafinException;

    public String confirmaCarga(String ic_epo,String ic_proc_docto,String ic_usuario,String totalDoctoMN,String totalDoctoUSD,String totalMontoMN,String totalMontoUSD,String reciboElectronico, String limiteLineaCredito)
    throws NafinException;

    public void operaDistribuidores(String ses_ic_epo)
     throws NafinException;

    public Vector getTipoFinanciamientoXEpo(String ic_epo,String ic_pyme,String selected)
    throws NafinException;
	
	public abstract boolean operaConversion(String ic_epo)
	throws NafinException;

	public Vector getParametrosClasific(String ic_epo,String ic_pyme, String df_fecha_emision, String cg_fecha_porc_desc, String ic_clasificacion)
	throws NafinException;

	public abstract String getParamFechaPorc(String ic_epo)	
	throws NafinException;
	
	public Vector getCategoriasXEpo(String ic_epo,String cg_num_distribuidor,String ic_producto_nafin, String selected)
	throws NafinException;
	
    
	public String validaDistribuidor (String epo,String dist,String fecha_emision)	
	throws NafinException;

	public String validaSaldoDisponible (String epo,String dist,String moneda,String monto_docto,String monto_temporal)
	throws NafinException;

	public String getMontoAcumulado (String epo,String dist,String moneda,String proc_docto)
	throws NafinException;

	public String confirmaPagoAnt(
		String totalPagos,
		String totalMonto,
		String ic_usuario,
		String reciboElectronico,
		String txt_ic_epo[],
		String txt_ic_pyme[],
		String txt_importe[],
		String txt_fec_aplicacion[],
		String chk_aplica[])
	throws NafinException;
	
	public Vector consultaPagosAnt(String cc_acuse)
	throws NafinException;

	public int esInhabil (String chk[],String fechas[],int registros)
	throws NafinException;
	
	public abstract void setCargaEnlacePEMEX(String ic_epo, String df_fecha_emision)
	throws NafinException;
  
  public String validaMontolineaCredito(String ic_epo) 	throws NafinException;
  
	public String getDiasInhabilesDes(String ic_epo, String fecha )	throws NafinException;
  
	public String montoLineaCreCaptura(String ic_epo) 	throws NafinException;
  public String getMontoComprometido(String ic_epo) throws NafinException;
  public String validaLineCredito(String ic_epo)  throws NafinException;

	public List procesarRechazosMasivo(List procesar, String nombreArchivo, String total) throws NafinException; //Fodea 051-2010
	public List ValidaLineaRechazo(String documento, String estatus, String causa, int noLinea)  throws NafinException; //Fodea 051-2010
  
	public String getTipoCredito(String ic_epo, String ic_pyme);
	public String getTipoCredito(String ic_epo);
	public String getTiposCredito(String ic_pyme, String tipoCredito);
	public String getTipoConversion(String ic_pyme);
	public String getEpoTipoCobroInt(String ic_epo);
	public String getEpoTipoCobroInt_b(String ic_epo);
	public String getValidaIn(String ic_epo, String ic_pyme);
	public Hashtable getNombres_pyme_if(String ic_pyme, String ic_if);
	public Vector getLineaCredito(String ic_pyme, String ic_if, String ic_moneda);
	public List repCambioEstatus(String noPyme, String noEstatus, String tiposCredito)  throws NafinException; //017-2011
 	public String repCambioEstatusCSVPDF(List datos)  throws NafinException; //017-2011
	public List esquemaCliente(String claveEpo) throws NafinException;
	
	public HashMap esquemaCliente2(String claveEpo) throws NafinException;
	public List esquemaCliente3(String claveEpo, String  clavePymme)throws NafinException;
	public HashMap esquemaCliente4(String claveEpo, String  clavePymme)throws NafinException;
	public List esquemasOperacion(String claveEpo)throws NafinException;
	public List parametros(String claveEpo)throws NafinException;
	public String getTipoCreditoEpo(String ic_epo);
	public Registros getHistoricoCredito(String folio, String tipo_credito);
	public Registros getLayoutFactorajeVencimiento(String clave_if);
	public Registros getLayoutFactorajeVencimiento(String clave_if, String tipoCredito);

	public String validacionesMasivaF(     
		String ic_epo,   String df_fecha_venc,
		String df_fecha_emision,  String ig_numero_docto,
		String ic_moneda,  String fn_monto,
		BigDecimal txtmtomindoc,   BigDecimal txtmtomaxdoc,
		BigDecimal txtmtomindocdol,  BigDecimal txtmtomaxdocdol,
		int linea,   Vector vecCampos,
		int pipesporlinea,  String ic_pyme,
		String cs_cifras_control, String ic_tipo_financiamiento ,  String TipoCredito,   
		String negociable ) throws NafinException;
		   
	public CargaDocDistribuidoresWSInfo procesarDocDistribuidoresWS(
			String claveEPO, String usuario, String password,
			String cadenaArchivo, String pkcs7, String serial)throws NafinException;
		
	public boolean validaPymeXEpoBloqueda(String cveEpo, String cvePyme, String cveProducto) throws AppException;
	public boolean validaPymeXEpoBloqXNumProv(String numDist, String cveEpo, String cveProducto) throws AppException;
	public String getAvisosNotificacionDisWS(String claveEPO, String usuario, String password, String fechaOperacionIni, String fechaOperacionFin);
	
	public String getTipoCreditoMantPendNeg(String ic_epo) throws  NafinException;
	public void eliminaPendientes(String ic_epo, String cc_acuse,String status) throws  NafinException;
	public String lineaCreditoEpo(String ic_epo, String cc_acuse) throws  NafinException;
	public void actualizaLineaCredito(String ic_IF,String[] ic_documento, String cc_acuse, String epo) throws  NafinException;
	public Vector mostrarDocumentosPendientes(String sNoCliente,String sEstatus,String cc_acuse, String numDoctos) throws  NafinException;
	public boolean bactualizaEstatusPendiente(String ic_epo, String cc_acuse,String ic_estatus,String auxPerfil, String numDoctos)   throws  NafinException;
	
	public boolean  setActLineaCredDist ( ) throws NafinException;  //F09-2015 
		  
}
  