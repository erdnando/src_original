package com.netro.distribuidores;  
 
//import netropology.utilerias.Acuse;
import java.util.Vector;
import com.netro.exception.*;

import netropology.utilerias.*;
import java.util.*;  
import java.math.*;
import java.io.*;
import javax.ejb.SessionContext;
import javax.ejb.Remote;

@Remote
public interface ParametrosDist {
	public abstract Vector getParamEpo(String ic_epo)throws  Exception ;
 	public abstract Vector getParamEpoa(String ic_epo)throws  Exception ;
 	public   Vector  getParamGral()throws  Exception ;
 	public   Vector  getParamGrala()throws  Exception ;
	public int vmodificaParametros(String tipocred, String respint, String tipocob, int esqint  )throws  Exception ;
 	public int vmodificaParamEpo(String tipocred, String respint, String tipocob, int esqint,  String sepo,String modplazo,int ic_producto_nafin, String desautomatico, String noNegociables, String lineaCredito,String ventaCartera, String negociableabaja, String tipoPago, String firmaMancomunada,  String mesesIntereses, String  cmbMeses, String bajocontrato )throws  Exception ;

	public int vmodificaParamEpoa(int diasmin, int plazomax, String tipoconv, String control,int plazo, int porcent, String epo, int proxvenc)
		throws  Exception ;

	public abstract int vmodificaParamEpoa(int diasmin, int plazomax, String tipoconv, String control,int plazo, double porcent, String epo, int proxvenc, String cg_fecha_porc_desc, String txtDiasFechaVenc, String txtEnvCorreo, int diasoperacion, double tasaDescuento, String tipoCred, String tipoPago, String claveUsuario, String nombreUsuario, String noAfiliacionComer, String mesesIntereses, String  cmbMeses)
		throws  Exception ;
	
	public int vmodificaEpoIF( String porccobra[], String mtobloq[], String porgaran[], String icif[] ,int elementos,String epo)throws  Exception ;

	public abstract int vmodificaParamGrales(int diasmin, int plazomax, String tipoconv, String control,String plazominlc, String proxvenc, String diasFechaVenc, String envCorreo) 
		throws  Exception ;
	
 	public   Vector  getParamEpo_IF(String ic_epo)throws  Exception ;

	public boolean updateParametros(String ic_if, String cg_tipo_solicitud, String ic_epo[], String autorizacion[], String fn_limite_puntos, String operaTarjeta, String cg_linea_c ) throws NafinException;
	public Vector getEposxIf(String ic_if) 	throws NafinException;
	public   Vector  getParamIF(String ic_if) throws NafinException;
	
	public abstract Vector getParamDesctoAuto(String ic_pyme)  throws NafinException;
	public abstract boolean setParamDesctoAuto(String ic_pyme,String cs_descto_auto,String monedaAutoN[],String monedaAutoD[])   throws NafinException;
	
	public boolean insertaClasificacion(String clave,String epo,int prodnaf,String desc, String plazo)
		throws NafinException;

	public boolean eliminaClasificacion(String clave,String epo,int prodnaf,int flag)
		throws NafinException;

	public boolean cambiaClasificacion(String idclas,String clave,String desc, String plazo)
		throws NafinException;
		
	public boolean relacionaClasificacion(String epo, String pyme, int prodnaf, String idclas, String ig_dias_maximo)
		throws NafinException;

	public boolean insertaPlazo(String idclas,String pi,String pf,String porc)
		throws NafinException;

	public boolean eliminaPlazo(String clave)
		throws NafinException;
  
	public boolean modificaPlazo(String icplazo,String icplazo_ant,String icplazo_sig,
								String p_ini,String p_fin,String porc)
		throws NafinException;  
		
		
	public int setLimitePyme(String ic_epo, String ic_pyme, String cg_num_distribuidor, String fg_limite_pyme, 
											String cg_mensaje_terminacion, String nombreUsuario, String ic_usuario, String acuse, 
											String recibo, String ic_moneda)
		throws NafinException;
	

	public List getLimitePyme(String ic_epo, String ic_pyme, String cg_num_distribuidor)
		throws NafinException;
		
	public List getPymesBloqueo(String ic_if, String ic_epo, String ic_pyme)
		throws NafinException;
		
	public void setPymesBloqueo(String cadena, String ic_usuario, String acuse, String recibo)
		throws NafinException;
		
	public void setPymesBloqueoDistEpoIf(String cadena, String ic_usuario, String acuse, String recibo)
		throws NafinException;   
		
	public boolean pymeBloqueadaDist(String esClavePyme, String esClaveEpo)
		throws NafinException;
		
	public boolean validarDistribuidor(String ic_epo, String cg_num_distribuidor)
		throws NafinException;
	
	public void setPlazosPyme(String ic_epo, String[] ic_pyme, String[] plazos ) //pruction fabo
		throws NafinException;
	
	public List getPlazosPymeIfEpo(String ic_epo, String ic_if )	//production fabo (22/05/08)
		throws NafinException;
    
  public String  DesAutomaticoEpo(String ic_epo, String parametro) throws NafinException; //Fodea 010 Distibuidores 2010
  public  String  plazoPorEpo(String ic_epo)  throws NafinException; //Fodea 014-2010 Distribuidores Fase II
  public  String  obtieneTipoCredito(String ic_epo)  throws NafinException;  //Fodea 014-2010 Distribuidores Fase II
 
	//***********Fodea 017-2011 Migracion EXTJS******************  
	public  String  obtieneTiposCreditoA(String ic_pyme)  throws NafinException;
	public  String  obtieneResponsable(String ic_epo)  throws NafinException;
	public  String  obtieneTipoConversion(String ic_pyme)  throws NafinException;
	//***********Fodea 017-2011 Migracion EXTJS****************** 
	public Map getParametrosModuloDistribuidores(String claveAfiliado, 
			String tipoAfiliado, String claveEpoRelacionada);
			
			
	public  List  obtieneParametrosAviso(String ic_epo) throws NafinException;
	public List validarLimitexMoneda2(String ic_epo) throws NafinException;
	public Registros getClasificacionClientes(String ic_pyme);
	public Registros getClasificacionClientes2(String ic_epo,String ic_pyme,String categoria, String plazo);
	public Registros getDistribuidorClasificacion(String ic_pyme);
	public Registros getPlazosClasificacionClientes(String ic_clas);
	public String validaDiasCategorias(String ic_pyme, String id_clas);
	public String validaAgregaCategoria(String ic_pyme, String cve_cat);
	public Registros getDetalleCambiosDoctos(String ic_docto);
	public String epohabilitada(String ic_epo);
	public Registros getInformacionDistribuidor(String ic_epo,String cg_rfc,String ic_nafinElec,String ic_pyme,String ic_if);
	public String[] eliminarCuenta(String ic_financiera,String ic_if,String ic_moneda,String ic_epo,
	String cg_rfc,String ic_nafinElec,String ic_pyme,String CtasCapturadas,String cg_sucursal,String cg_numcta,String ic_plaza) ;
	public String[] insertaCuentaBancaria(String ic_financiera,String ic_if,String ic_moneda,String ic_epo,
	String cg_rfc,String ic_nafinElec,String ic_pyme,String cg_sucursal,String cg_numcta,String ic_plaza);
	public String[] aceptaCuenta(String ic_cuenta_b,String cg_sucursal,String cg_numcta,String ic_plaza);
	public String[] actualizaCuentaBancaria(String ic_financiera,String ic_if,String ic_moneda,String ic_epo,
	String cg_rfc,String ic_nafinElec,String ic_pyme,String CtasCapturadas,String cg_sucursal,String cg_numcta,String ic_plaza);
	public String getTiposCredito(String ic_pyme, String ic_epo); 
	
	public List  getParametrosEpo(String cveEpo, String cveSeccion) throws AppException;
	public boolean guardaAforoPorEPO(String ic_if, String ic_epo,String ic_moneda, String aforo, String strLogin, Long iNoNafinElectronico, String strNombreUsuario) throws AppException; //Fodea 021-2011
	public List getBinsIFxEpo(String cveEpo, String cveMoneda)throws AppException;
	public String  getLimiteAcumulado(String cveEpo, String cvePyme) throws AppException;
	
	public String getFirmaMancomunada(String ic_epo)	throws AppException; //Fodea 032-204
	public String getAfiliadoComercio(String ic_epo) throws AppException;
	public boolean setAfiliadoComercio(String ic_epo, String valor) throws AppException;
	public String getParamXepo(String ic_epo, String parametro) throws AppException;
	public boolean updateParametros(String ic_if, String cg_tipo_solicitud, String ic_epo[], String autorizacion[], String fn_limite_puntos, String operaTarjeta, String cg_linea_c, String notificaDoctos, String emailNotificaDoctos) throws NafinException;
	public  String  obtieneOperaSinCesion(String ic_epo)throws NafinException;	
} 
