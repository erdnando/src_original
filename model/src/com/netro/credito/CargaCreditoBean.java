package com.netro.credito;

import com.netro.exception.NafinException;

import java.math.BigDecimal;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Acuse;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;

@Stateless(name = "CargaCreditoEJB" , mappedName = "CargaCreditoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class CargaCreditoBean implements CargaCredito {

	private int 		campoError 		= 0;
	private boolean 	TA 				= false;

	public void procesaCargaMasivaCE(EnlInterCargaCE enlace, String NoIf) throws NafinException	{
		System.out.println("CargaCreditoEJB::procesaCargaMasivaCE(E)");

		String 	claveSirac 		= "", noSucBanco 		= "", moneda 		= "", numDocto 			= "",
				importeDocto 	= "", importeDscto 		= "", emisor 		= "", tipoCredito 		= "",
				ppc 			= "", ppi 				= "", claseDocto 	= "", domicilioPago 	= "",
				tasaInteresUF 	= "", relMatUF 			= "", sobreTasaUF 	= "", tasaInteresI 		= "",
				relMatI 		= "", sobreTasaI 		= "", tablaAmort 	= "", numAmort 			= "",
				fppc 			= "", fppi 				= "", bieneServi 	= "", fechVencDocto 	= "",
				fechVencDscto 	= "", diaPago 			= "", fetc 			= "", lugarFirma 		= "",
				amortAjuste 	= "", amortImporte 		= "", amortFecha 	= "", icEmisor 			= "",
				icTipoCredito 	= "", icTipoPPC 		= "", icTipoPPI 	= "", icClaseDocto 		= "",
				icTasaUF 		= "", icTasaIntermed 	= "", icTablaAmort 	= "", tipoRenta 		= "",
				numAmortRec 	= "", plazoMensual 		= "", TipoPlazo		= "", codigoBaseOpDinamica = "";


		boolean lbOK 			= true;

		StringBuffer lsCadenaSQL 	= new StringBuffer();
		StringBuffer log 			= new StringBuffer();

		String sTomoPlazoGral 	= "N";
		String idRegistro		= "";
		String codigoError		= "";
		String estatusAcu		= "";

		int PlazoFPPC 	 	= 0;
		int PlazoFPPI 		= 0;
		int PlazoFVD 		= 0;
		int iPlazoMaxBO 	= 0;
		boolean	codigoBaseOpDinamicaHabilitado	= false;
		boolean  existeCodigoBaseOpDinamica 		= false;

		Vector 				lovTablaAmort	= null;
		List 				lTmpBO 			= new ArrayList();;

		String 				qrySentencia 	= "";
		AccesoDB 			con 			= null;
		List				lVarBind 		= new ArrayList();
		List				lDatos			= new ArrayList();
		List 				lAmortizaciones = new ArrayList();

		int iTipoPiso 	= 0;
		int existenReg 	= 0;
		int existe 		= 0;

		EnlAcuCargaCE	acuseEnl			= null;
		Acuse 			acuse 			= null;
		String 			icSolicPortal 	= "";
		
		List				encabezados		= null;
		
		try {
			con = new AccesoDB();
			con.conexionDB();

			// 1. Depurar los registros con error en la tabla COM_SOLIC_PUB_ACU_RESUL_FIDE y que ya hayan sido
			// leidos por el FIDE( COM_SOLIC_PUB_ACU_FIDE.IG_ESTATUS = 2 )
System.out.println("CargaCreditoEJB::procesaCargaMasivaCE(Ejecutando SP_DEPURA_PUB_FIDE)");
			CallableStatement callableSt = con.ejecutaSP(enlace.getProcesoDepuracion());
			callableSt.executeUpdate();
			callableSt.close();
 
System.out.println("CargaCreditoEJB::procesaCargaMasivaCE(Procesar Documentos Publicados)");
			// 2. Traer los encabezados de los registros que estan listos para ser procesados
			qrySentencia = 
				"SELECT      "  +
				" 	 IC_ENC   "  +
				"FROM        "  + 
					" " + enlace.getTablaEncabezadoDocumentos() + " " +
				"WHERE       "  +
				"	IC_PROCESO = ? AND "  + // 1 - Disposiciones - 4 Registro Subsidio
				"	IG_ESTATUS = ?     ";   // 0 - Listo para ser procesado
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(enlace.getProceso()));
			lVarBind.add(new Integer("0"));					
			encabezados = con.consultaDB(qrySentencia, lVarBind, false);					
			encabezados = encabezados == null?new ArrayList():encabezados;
			
			// 3. Por cada encabezado:
			for(int indice=0;indice<encabezados.size();indice++){
				
				List  	registro  	= (ArrayList) encabezados.get(indice);
				String 	encabezado 	= (String) registro.get(0);
				
System.out.println("Procesando registros asociados al encabezado: " + encabezado);
				
				// 3.1 Generar el Acuse correspondiente 	
				acuse 		= new Acuse(Acuse.ACUSE_IF, "1");
				acuseEnl		= new EnlAcuCargaCE();
				acuseEnl.setCc_acuse(acuse.toString());
				
				// 3.2 Insertar Acuse generado
				qrySentencia 	= enlace.getInsertAcuse(acuseEnl);
				lVarBind 		= enlace.getVariablesBind();
				existe 			= con.ejecutaUpdateDB(qrySentencia, lVarBind);
				con.terminaTransaccion(true);

				//3.3 Obtiene el numero de registros a procesar
				qrySentencia =
					" SELECT COUNT (1), 'CargaCreditoBean::procesaCargaMasivaCE()'"   +
					"   FROM "+enlace.getTablaDocumentos()+" WHERE IC_ENC = ? ";
				lVarBind = new ArrayList();
				lVarBind.add(new Long(encabezado));
				existenReg = con.existeDB(qrySentencia, lVarBind);

System.out.println("qrySentencia(conteo registros): "+qrySentencia);
System.out.println("qrySentencia.encabezado:        "+encabezado);
System.out.println("existenReg:                     "+existenReg);

				if(existenReg>0) {

					//BORRA RESULTADOS SE DEFINIO QUE ELLOS BORRARAN LOS RESULTADOS
					//qrySentencia =
					//	" DELETE "+enlace.getTablaErrores()+" "  ;
					//existe = con.ejecutaUpdateDB(qrySentencia);
	
					//BORRA ACUSE
					//qrySentencia =
					//	" DELETE "+enlace.getTablaAcuse()+" "  ;
					//existe = con.ejecutaUpdateDB(qrySentencia);

					iTipoPiso = getTipoPisoIF(NoIf, con);

					// 3.4 Obtiene los registros a procesar
					qrySentencia = enlace.getDocuments();
					lVarBind = new ArrayList();
					lVarBind.add(new Long(encabezado));
					lDatos = con.consultaDB(qrySentencia,lVarBind);
					
System.out.println("qrySentencia(registros a procesar): "+qrySentencia);
System.out.println("qrySentencia.encabezado:            "+encabezado);

					for(int i=0;i<lDatos.size();i++) {

						List lRegistro 		= (ArrayList)lDatos.get(i);
	
						claveSirac				= lRegistro.get( 0).toString();
						noSucBanco				= lRegistro.get( 1).toString();
						moneda					= lRegistro.get( 2).toString();
						numDocto					= lRegistro.get( 3).toString();
						importeDocto			= lRegistro.get( 4).toString();
						importeDscto			= lRegistro.get( 5).toString();
						emisor 					= lRegistro.get( 6).toString();
						tipoCredito				= lRegistro.get( 7).toString();
						ppc						= lRegistro.get( 8).toString();
						ppi 						= lRegistro.get( 9).toString();
						bieneServi 				= lRegistro.get(10).toString();
						claseDocto 				= lRegistro.get(11).toString();
						domicilioPago 			= lRegistro.get(12).toString();
						tasaInteresUF 			= lRegistro.get(13).toString();
						relMatUF 				= lRegistro.get(14).toString();
						sobreTasaUF				= lRegistro.get(15).toString();
						tasaInteresI			= lRegistro.get(16).toString();
						numAmort					= lRegistro.get(17).toString();
						tablaAmort 				= lRegistro.get(18).toString();
						fppc						= lRegistro.get(19).toString();
						fppi						= lRegistro.get(20).toString();
						diaPago					= lRegistro.get(21).toString();
						fechVencDocto			= lRegistro.get(22).toString();
						fechVencDscto			= lRegistro.get(23).toString();
						fetc						= lRegistro.get(24).toString();
						lugarFirma				= lRegistro.get(25).toString();
						amortAjuste	 			= lRegistro.get(26).toString();
						amortImporte 			= lRegistro.get(27).toString();
						tipoRenta				= lRegistro.get(28).toString();
						TipoPlazo				= lRegistro.get(29).toString();
						// clavePrograma = lRegistro.get(30).toString(); // Este campo no se utiliza 
						codigoBaseOpDinamica = lRegistro.get(30).toString(); // Por el momento solo se utiliza
																							  // para codigos de operacion dinamicos
						
						System.out.println("01:"+claveSirac);
						System.out.println("02:"+noSucBanco);
						System.out.println("03:"+moneda);
						System.out.println("04:"+numDocto);
						System.out.println("05:"+importeDocto);
						System.out.println("06:"+importeDscto);
						System.out.println("07:"+emisor);
						System.out.println("08:"+tipoCredito);
						System.out.println("09:"+ppc);
						System.out.println("10:"+ppi);
						System.out.println("11:"+bieneServi);
						System.out.println("12:"+claseDocto);
						System.out.println("13:"+domicilioPago);
						System.out.println("14:"+tasaInteresUF);
						System.out.println("15:"+relMatUF);
						System.out.println("16:"+sobreTasaUF);
						System.out.println("17:"+tasaInteresI);
						System.out.println("18:"+numAmort);
						System.out.println("19:"+tablaAmort);
						System.out.println("20:"+fppc);
						System.out.println("21:"+fppi);
						System.out.println("22:"+diaPago);
						System.out.println("23:"+fechVencDocto);
						System.out.println("24:"+fechVencDscto);
						System.out.println("25:"+fetc);
						System.out.println("26:"+lugarFirma);
						System.out.println("27:"+amortAjuste);
						System.out.println("28:"+amortImporte);
						System.out.println("29:"+tipoRenta);
						System.out.println("30:"+TipoPlazo);
						System.out.println("31:"+codigoBaseOpDinamica);

						relMatI			= "";
						sobreTasaI		= "";
						numAmortRec 	= "";
						plazoMensual	= "S";
	
						idRegistro 		= "Clave Sirac:"+claveSirac+" Documento:"+numDocto+": ";
						lbOK				= true;

						try {
							
							//Validaci�n de la Clave de Sirac. Campo 1
							if (TipoPlazo.equals("")) {
								log.append(idRegistro+"Campo 30: El tipo de operacion es requerido.");
								throw new NafinException("CRED0116");
							}
	
							if (claveSirac.equals("")) {
								log.append(idRegistro+"Campo 1: La Clave de Sirac es un campo requerido.");
								throw new NafinException("CRED0014");
							} else {
								if (claveSirac.length()>12) {
									log.append(idRegistro+"Campo 1: La Clave de Sirac excede la longitud permitida (7).");
									throw new NafinException("CRED0015");
								}
								if (!Comunes.esNumero(claveSirac)) {
									log.append(idRegistro+"Campo 1: La Clave de Sirac no es v�lida.");
									throw new NafinException("CRED0016");
								}
							}

							//Validaci�n para el N�mero de Sucursal del Banco. Campo 2
//noSucBanco = "null";
							if (!noSucBanco.equals("")) {
								if (!Comunes.esNumero(noSucBanco)) {
									log.append(idRegistro+"Campo 2: El N�mero de Sucursal del Banco no es v�lido.");
									throw new NafinException("CRED0017");
								} else if(noSucBanco.length()>10) {
									log.append(idRegistro+"Campo 2: El N�mero de Sucursal del Banco excede la longitud permitida (10).");
									throw new NafinException("CRED0018");
								}
							}

							//Validaci�n para el tipo de Moneda. Campo 3
							if (moneda.equals("")) {
								log.append(idRegistro+"Campo 3: La Clave de la Moneda es un campo requerido.");
								throw new NafinException("CRED0019");
							} else if (!("1".equals(moneda)||"54".equals(moneda))) {
								log.append(idRegistro+"Campo 3: No es V�lido el tipo de Moneda (1,54).");
								throw new NafinException("CRED0020");
							}

							//Validaci�n para el N�mero de Documento. Campo 4
							if ("".equals(numDocto)) {
								log.append(idRegistro+"Campo 4: El N�mero de Documento es un campo requerido.");
								throw new NafinException("CRED0021");
							} else if (!Comunes.esNumero(numDocto)) {
								log.append(idRegistro+"Campo 4: No es v�lido el N�mero de Documento.");
								throw new NafinException("CRED0022");
							} else if(numDocto.length()>10) {
								log.append(idRegistro+"Campo 4: El N�mero de Documento excede la longitud permitida.");
								throw new NafinException("CRED0023");
							}

							//Valida si existe el N�mero de Documento. Campo 4
							existeNoDocto(numDocto, NoIf, claveSirac, con);

							//Validaci�n para el Importe de Documento. Campo 5
							if (importeDocto.equals("")) {
								log.append(idRegistro+"Campo 5: El Importe de Documento es un campo requerido.");
								throw new NafinException("CRED0024");
							} else {
								if (!Comunes.esDecimal(importeDocto)) {
									log.append(idRegistro+"Campo 5: El Importe del Documento no es un valor num�rico decimal v�lido.");
									throw new NafinException("CRED0025");
								} else if(importeDocto.length() > 22) {
									log.append(idRegistro+"Campo 5: El Importe de Documento excede la longitud permitida (22).");
									throw new NafinException("CRED0026");
								} else if(importeDocto.indexOf(".")!=-1) {
									if (importeDocto.substring(0, importeDocto.indexOf(".")).length() > 19 || importeDocto.substring(importeDocto.indexOf(".")+1,importeDocto.length()).length() > 2) {
										log.append(idRegistro+"Campo 5: El Importe del Documento no es un valor num�rico decimal v�lido (19,2).");
										throw new NafinException("CRED0027");
									}
								}
								if(Double.parseDouble(importeDocto)<=0) {
									log.append(idRegistro+"Campo 5: El Importe del Documento debe ser mayor a cero.");
									throw new NafinException("CRED0028");
								}
							}

							//Validaci�n para el Importe de Descuento. Campo 6
							if (importeDscto.equals("")) {
								log.append(idRegistro+"Campo 6: El Importe de Descuento es un campo requerido.");
								throw new NafinException("CRED0029");
							} else {
								if (!Comunes.esDecimal(importeDscto)) {
									log.append(idRegistro+"Campo 6: El Importe del Descuento no es un valor num�rico decimal v�lido.");
									throw new NafinException("CRED0030");
								} else if(importeDscto.length() > 22) {
									log.append(idRegistro+"Campo 6: El Importe de Descuento excede la longitud permitida (22).");
									throw new NafinException("CRED0031");
								} else if(importeDscto.indexOf(".")!=-1) {
									if (importeDscto.substring(0, importeDscto.indexOf(".")).length() > 19 || importeDscto.substring(importeDscto.indexOf(".")+1,importeDscto.length()).length() > 2) {
										log.append(idRegistro+"Campo 6: El Importe del Descuento no es un valor num�rico decimal v�lido (19,2).");
										throw new NafinException("CRED0032");
									}
								}
								if(Double.parseDouble(importeDscto)<=0) {
									log.append(idRegistro+"Campo 6: El Importe del Descuento debe ser mayor a cero.");
									throw new NafinException("CRED0033");
								} else if((Double.parseDouble(importeDscto) > Double.parseDouble(importeDocto))) {
									log.append(idRegistro+"Campo 6: El Importe de Descuento no puede ser mayor al Importe de Documento.");
									throw new NafinException("CRED0034");
								}
							}

							//Validaci�n para la Clave del Emisor. Campo 7
							icEmisor = "";
							if (!emisor.equals("")) {
								if(!Comunes.esNumero(emisor)) {
									log.append(idRegistro+"Campo 7: La Clave del Emisor no es v�lida.");
									throw new NafinException("CRED0035");
								} else {
									try {
										icEmisor = sValidaEmisor(emisor, con);
									  } catch (Exception e){
										log.append(idRegistro+"Campo 7: La Clave del Emisor no existe en el Cat�logo.");
										throw new NafinException("CRED0036");
										 }
								}
							}

							//Validaci�n para el Tipo de Cr�dito. Campo 8
							if(tipoCredito.equals("")) {
								log.append(idRegistro+"Campo 8: La Clave del Tipo de Cr�dito es un campo requerido.");
								throw new NafinException("CRED0037");
							} else {
								if(!Comunes.esNumero(tipoCredito)) {
									log.append(idRegistro+"Campo 8: La Clave del Tipo de Cr�dito no es v�lida.");
									throw new NafinException("CRED0038");
								} else {
									try {
										icTipoCredito = sValidaTipoCredito(tipoCredito, con);
									  } catch (Exception e){
										log.append(idRegistro+"Campo 8: La Clave del Tipo de Cr�dito no existe en el Cat�logo.");
										throw new NafinException("CRED0039");
									  }
								}
							}

							//Validaci�n para la Periodicidad del Pago de Capital. Campo 9
							if(ppc.equals("")) {
								log.append(idRegistro+"Campo 9: La Clave de la Periodicidad del Pago de Capital es un campo requerido.");
								throw new NafinException("CRED0040");
							} else {
								if(!Comunes.esNumero(ppc)) {
									log.append(idRegistro+"Campo 9: La Clave de la Periodicidad del Pago de Capital no es v�lida.");
									throw new NafinException("CRED0041");
								} else {
									try {
										icTipoPPC = sValidaPeriodicidadPP(ppc, con);
										 } catch (Exception e) {
										log.append(idRegistro+"Campo 9: La Clave de la Periodicidad del Pago de Capital no existe en el Cat�logo.");
										throw new NafinException("CRED0042");
										 }
								}
							}

							//Validaci�n para la Periodicidad del Pago de Intereses. Campo 10
							if(ppi.equals("")) {
								log.append(idRegistro+"Campo 10: La Clave de la Periodicidad del Pago de Intereses es un campo requerido.");
								throw new NafinException("CRED0043");
							} else {
								if(!Comunes.esNumero(ppi)) {
									log.append(idRegistro+"Campo 10: La Clave de la Periodicidad del Pago de Intereses no es v�lida.");
									throw new NafinException("CRED0044");
								} else {
									try {
										icTipoPPI = sValidaPeriodicidadPP(ppi, con);
										 } catch (Exception e){
										log.append(idRegistro+"Campo 10: La Clave de la Periodicidad del Pago de Intereses no existe en el Cat�logo.");
										throw new NafinException("CRED0045");
										 }
								}
							}

							//Validaci�n de los Bienes y Servicios. Campo 11
							if(bieneServi.equals("")) {
								log.append(idRegistro+"Campo 11: La Descripci�n de los Bienes y Servicios es un campo requerido.");
								throw new NafinException("CRED0046");
							} else {
								bieneServi=(bieneServi.length()>60)?bieneServi.substring(0,59):bieneServi;
							}

							//Validaci�n de la Clase de Documento. Campo 12
							if(claseDocto.equals("")) {
								log.append(idRegistro+"Campo 12: La Clave de la Clase de Documento es un campo requerido.");
								throw new NafinException("CRED0047");
							} else {
								if(!Comunes.esNumero(claseDocto)) {
									log.append(idRegistro+"Campo 12: La Clave de la Clase de Documento no es v�lida.");
									throw new NafinException("CRED0048");
								} else {
									try {
										icClaseDocto = sValidaClaseDocto(claseDocto, con);
										 } catch (Exception e){
										log.append(idRegistro+"Campo 12: La Clave de la Clase de Documento no existe en el Cat�logo.");
										throw new NafinException("CRED0049");
										 }
								}
							}

							//Validaci�n para el Domicilio de Pago. Campo 13
							if (domicilioPago.equals("")) {
								log.append(idRegistro+"Campo 13: El Domicilio de Pago es un campo requerido.");
								throw new NafinException("CRED0050");
							} else {
								domicilioPago=(domicilioPago.length()>60)?domicilioPago.substring(0,59):domicilioPago;
							}

							//Validaci�n para la Tasa de Interes del Usuario Final. Campo 14
							if(tasaInteresUF.equals("")&&TipoPlazo.equals("C")){
								log.append(idRegistro+"Campo 14: La Clave de la Tasa de Inter�s del Usuario Final es un campo requerido.");
								throw new NafinException("CRED0051");
							} else if(tasaInteresUF.equals("")) {
icTasaUF = "";
							} else {
								if(!Comunes.esNumero(tasaInteresUF)) {
									log.append(idRegistro+"Campo 14: La Clave de la Tasa de Inter�s del Usuario Final no es v�lida.");
									throw new NafinException("CRED0052");
								} else {
									try {
										icTasaUF = sValidaTasaInteresIntermed(tasaInteresUF,"N",con);
										 } catch (Exception e) {
										log.append(idRegistro+"Campo 14: La Clave de la Tasa de Inter�s del Usuario Final no existe en el Cat�logo.");
										throw new NafinException("CRED0053");
										 }
								}
							}

							//Validaci�n para la Relaci�n Matem�tica del Usuario Final. Campo 15
							if(relMatUF.equals("")&&TipoPlazo.equals("C")) {
								log.append(idRegistro+"Campo 15: La Relaci�n Matem�tica del Usuario Final es un campo requerido.");
								throw new NafinException("CRED0054");
							} else if(relMatUF.equals("")) {
relMatUF = "";
							} else if(!relMatUF.equals("+")) {
								log.append(idRegistro+"Campo 15: La Relaci�n Matem�tica del Usuario Final no es v�lida.");
								throw new NafinException("CRED0055");
							}
//						} else {
//relMatUF = "'"+relMatUF+"'";
//						}

							//Validaci�n para la SobreTasa del Usuario Final. Campo 16
							if(sobreTasaUF.equals("")&&TipoPlazo.equals("C")) {
								log.append(idRegistro+"Campo 16: La SobreTasa del Usuario Final es un campo requerido.");
								throw new NafinException("CRED0056");
							} else if(sobreTasaUF.equals("")) {
sobreTasaUF = "";
							} else {
								if(!Comunes.esDecimal(sobreTasaUF)) {
									log.append(idRegistro+"Campo 16: La SobreTasa del Usuario Final no es v�lida.");
									throw new NafinException("CRED0057");
								} else if(sobreTasaUF.length() > 7) {
									log.append(idRegistro+"Campo 16: La SobreTasa del Usuario Final excede la longitud permitida.");
									throw new NafinException("CRED0058");
								} else if(sobreTasaUF.indexOf(".")==-1) {
									if(sobreTasaUF.length() > 2) {
										log.append(idRegistro+"Campo 16: La SobreTasa del Usuario Final excede la longitud permitida de enteros (2).");
										throw new NafinException("CRED0059");
									}
								} else if(sobreTasaUF.substring(0,sobreTasaUF.indexOf(".")).length() > 2 || sobreTasaUF.substring(sobreTasaUF.indexOf(".")+1, sobreTasaUF.length()).length() > 4) {
									log.append(idRegistro+"Campo 16: La SobreTasa del Usuario Final excede la longitud permitida (2,4).");
									throw new NafinException("CRED0060");
								}
							}

							//Validaci�n para la Tasa de Interes del Intermediario. Campo 17
							if(tasaInteresI.equals("")) {
								log.append(idRegistro+"Campo 17: La Tasa de Interes del Intermediario es un campo requerido.");
								throw new NafinException("CRED0061");
							} else {
								if(!Comunes.esNumero(tasaInteresI)) {
									log.append(idRegistro+"Campo 17: La Clave de la Tasa de Interes del Intermediario no es v�lida.");
									throw new NafinException("CRED0062");
								} else {
									try {
										icTasaIntermed = sValidaTasaInteresIntermed(tasaInteresI,"S",con);
										 } catch (Exception e) {
										log.append(idRegistro+"Campo 17: La Clave de la Tasa de Inter�s del Intermediario no existe en el Cat�logo.");
										throw new NafinException("CRED0063");
										 }
								}
							}

							//Validaci�n para el N�mero de Amortizaciones. Campo 18
							if(numAmort.equals("")) {
								log.append(idRegistro+"Campo 18: El N�mero de Amortizaciones es un campo requerido.");
								throw new NafinException("CRED0064");
							} else if(!Comunes.esNumero(numAmort)) {
								log.append(idRegistro+"Campo 18: El N�mero de Amortizaciones no es v�lido.");
								throw new NafinException("CRED0065");
							} else if(numAmort.length() > 3) {
								log.append(idRegistro+"Campo 18: El N�mero de Amortizaciones excede la longitud permitida (3).");
								throw new NafinException("CRED0066");
							}

							//Validaci�n para el Tipo de Amortizaci�n. Campo 19
							if(tablaAmort.equals("")) {
								log.append(idRegistro+"Campo 19: La Clave del Tipo de Amortizaci�n es un campo requerido.");
								throw new NafinException("CRED0067");
							} else {
								if(!Comunes.esNumero(tablaAmort)) {
									log.append(idRegistro+"Campo 19: La Clave del Tipo de Amortizaci�n no es v�lido.");
									throw new NafinException("CRED0068");
								} else {
									try {
										icTablaAmort = sValidaTablaAmortizacion(tablaAmort,con);
										 } catch (Exception e) {
										log.append(idRegistro+"Campo 19: La Clave del Tipo de Amortizaci�n no existe en el Cat�logo.");
										throw new NafinException("CRED0069");
										 }
								}
							}

							//Para validaciones de fechas.
							java.util.Date fechaHoy = new java.util.Date();
							SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
							//Validaci�n para la Fecha del Primer Pago de Capital. Campo 20
							if(fppc.equals("")) {
								log.append(idRegistro+"Campo 20: La Fecha del Primer Pago de Capital es un campo requerido.");
								throw new NafinException("CRED0070");
							} else {
								if(!Comunes.checaFecha(fppc)) {
									log.append(idRegistro+"Campo 20: La Fecha del Primer Pago de Capital no es una fecha correcta.");
									throw new NafinException("CRED0071");
								} else {
									java.util.Date fPPC=Comunes.parseDate(fppc);
									int iComp = fPPC.compareTo(fechaHoy);
									if (iComp < 0 && !fppc.equals(sdf.format(fechaHoy)) ) {
										log.append(idRegistro+"Campo 20: La Fecha de Primer Pago de Capital "+fppc+" es menor a la Fecha de Hoy.");
										throw new NafinException("CRED0072");
									}
								}
							}

							//Validaci�n para la Fecha de Primer Pago de Interes. Campo 21
							if(fppi.equals("")) {
								log.append(idRegistro+"Campo 21: La Fecha del Primer Pago de Inter�s es un campo requerido.");
								throw new NafinException("CRED0073");
							} else {
								if(!Comunes.checaFecha(fppi)) {
									log.append(idRegistro+"Campo 21: La Fecha del Primer Pago de Inter�s no es una fecha correcta (dd/mm/yyyy).");
									throw new NafinException("CRED0074");
								} else {
									java.util.Date fPPI=Comunes.parseDate(fppi);
									int iComp = fPPI.compareTo(fechaHoy);
									if (iComp < 0 && !fppi.equals(sdf.format(fechaHoy)) ) {
										log.append(idRegistro+"Campo 21: La Fecha de Primer Pago de Inter�s "+fppi+" es menor a la Fecha de Hoy.");
										throw new NafinException("CRED0075");
									}
								}
							}

							//Validaci�n para el D�a de Pago. Campo 22
							if(diaPago.equals("")) {
								fppc.substring(0,2);
							} else {
								if(!Comunes.esNumero(diaPago)) {
									log.append(idRegistro+"Campo 22: El D�a de Pago no es v�lido.");
									throw new NafinException("CRED0076");
								} else if(diaPago.length()>2) {
									log.append(idRegistro+"Campo 22: El D�a de Pago excede la longitud permitida (2).");
									throw new NafinException("CRED0077");
								}
							}

							//Validaci�n para la Fecha de Vencimiento del Documento. Campo 23
							if(fechVencDocto.equals("")){
fechVencDocto = "";
							} else if(!Comunes.checaFecha(fechVencDocto)) {
								log.append(idRegistro+"Campo 23: La Fecha de Vencimiento del Documento no es una fecha correcta.");
								throw new NafinException("CRED0078");
							} else {
								java.util.Date fechaVenc=Comunes.parseDate(fechVencDocto);
								int iComp = fechaVenc.compareTo(fechaHoy);
								if (iComp < 0 && !fechVencDocto.equals(sdf.format(fechaHoy)) ) {
									log.append(idRegistro+"Campo 23: La Fecha de Vencimiento del Documento "+fechVencDocto+" es menor a la Fecha de Hoy.");
									throw new NafinException("CRED0079");
								}
							}

							//Validaci�n para la Fecha de Vencimiento del Descuento. Campo 24
							if(fechVencDscto.equals("")) {
								log.append(idRegistro+"Campo 24: La Fecha de Vencimiento del Descuento es un campo requerido.");
								throw new NafinException("CRED0080");
							} else {
								if(!Comunes.checaFecha(fechVencDscto)) {
									log.append(idRegistro+"Campo 24: La Fecha de Vencimiento del Descuento no es una fecha correcta.");
									throw new NafinException("CRED0081");
								} else {
									java.util.Date fechaVenc=Comunes.parseDate(fechVencDscto);
									int iComp = fechaVenc.compareTo(fechaHoy);
									if (iComp < 0 && !fechVencDscto.equals(sdf.format(fechaHoy)) ) {
										log.append(idRegistro+"Campo 24: La Fecha de Vencimiento del Descuento "+fechVencDscto+" es menor a la Fecha de Hoy.");
										throw new NafinException("CRED0082");
									}
								}
							}

							//Validaci�n para la Fecha de Emisi�n del T�tulo de Cr�dito. Campo 25
							if(fetc.equals("")) {
								log.append(idRegistro+"Campo 25: La Fecha de Emisi�n del T�tulo de Cr�dito es un campo requerido.");
								throw new NafinException("CRED0083");
							} else  {
								if(!Comunes.checaFecha(fetc)) {
									log.append(idRegistro+"Campo 25: La Fecha de Emisi�n del T�tulo de Cr�dito no es una fecha correcta.");
									throw new NafinException("CRED0084");
								}
							}

							//Validaci�n para el Lugar de Firma. Campo 26
							if(lugarFirma.equals("")) {
								log.append(idRegistro+"Campo 26: El Lugar de Firma es un campo requerido.");
								throw new NafinException("CRED0085");
							} else {
								lugarFirma = (lugarFirma.length()>50)?lugarFirma.substring(0,49):lugarFirma;
							}

							//Validaci�n para la Amortizaci�n de Ajuste. Campo 27
							//amortImporte=""; 
							amortFecha		= "";
							lovTablaAmort 	= null;

		               if(tablaAmort.equals("3") && !amortAjuste.equals("")) {
								
		               	if(!(amortAjuste.equals("P")||amortAjuste.equals("U"))){
									
										log.append(idRegistro+"Campo 27: El Tipo de Amortizaci�n de Ajuste no es v�lido (P-Primero, U-Ultimo).");
										throw new NafinException("CRED0086");
										
			               } else {
									
									try {
/*									
									lovTablaAmort = ovValidaAmortizacion(amortAjuste, claveSirac, numDocto, con);
			                        if (lovTablaAmort.size() > 0){
			                        	amortAjuste  = lovTablaAmort.elementAt(0).toString();
			                            amortImporte = lovTablaAmort.elementAt(1).toString();
			                            amortFecha 	 = lovTablaAmort.elementAt(2).toString();
			                        }
*/			                        
										if((amortImporte.equals(""))) {
											log.append(idRegistro+"Campo 27: Los Campos Din�micos de las Amortizaciones no est�n completos.");
											throw new NafinException("CRED0088");
										}
									}catch(Exception e){
										log.append(idRegistro+"Campo 27: Las Amortizaciones no est�n completas.");
										throw new NafinException("CRED0087");
				               }
									
								}
		               
							} else {
amortImporte 	= "";
amortAjuste 	= "";
							}

							//Validaci�n para el Tipo de Renta. Campo 28
							if(tablaAmort.equals("5")){
								if((!tipoRenta.equals(""))){
											if(!(tipoRenta.equals("N")||tipoRenta.equals("S"))){
										log.append(idRegistro+"Campo 28: El Tipo de Renta no es v�lido (S-Si, N-No).");
										throw new NafinException("CRED0089");
											}
								} else {
									log.append(idRegistro+"Campo 28: El Tipo de Renta es un campo requerido.");
									throw new NafinException("CRED0090");
								}
							}

							//Validaciones en Bases de Operaci�n
							try{
								lTmpBO = sGetDatosBO(NoIf,iTipoPiso,TipoPlazo,con);
								if(lTmpBO.size()==0) {
									log.append(idRegistro+"No existen registros parametrizados en Bases de Operaci�n para el Tipo de Plazo elegido.");
									throw new NafinException("CRED0091");
								}
								List lEmisor 		= (ArrayList)lTmpBO.get(0);
								List lTipoCredito 	= (ArrayList)lTmpBO.get(1);
								List lTasa 			= (ArrayList)lTmpBO.get(2);
								List lTablaAmort 	= (ArrayList)lTmpBO.get(3);
	
								if(!lEmisor.contains(icEmisor)) {
									log.append(idRegistro+"Campo 7: La Clave del Emisor no existe para el Tipo de Plazo elegido");
									throw new NafinException("CRED0092");
								}
	
								if(!lTipoCredito.contains(icTipoCredito)) {
									log.append(idRegistro+"Campo 8: La Clave del Tipo de Cr�dito no existe para el Tipo de Plazo elegido y la Clave de Emisor capturada");
									throw new NafinException("CRED0093");
								}
	
								if(!lTasa.contains(icTasaIntermed)) {
									log.append(idRegistro+"Campo 17: La Clave de la Tasa de Inter�s del Intermediario no existe para el Tipo de Plazo elegido y las Claves de Emisor y de Tipo de Cr�dito capturadas");
									throw new NafinException("CRED0094");
								}
	
								if(!lTablaAmort.contains(icTablaAmort)) {
									log.append(idRegistro+"Campo 19: La Clave del Tipo de Amortizaci�n no existe para el Tipo de Plazo elegido y las Claves de Emisor, Tipo de Cr�dito y Tasa de Inter�s del Intermediario capturadas");
									throw new NafinException("CRED0095");
								}
	
							} catch(Exception e) {
								e.printStackTrace();
								log.append(idRegistro+"No existen registros parametrizados en Bases de Operaci�n para el Tipo de Plazo elegido.");
								throw new NafinException("CRED0096");
							}

							//Validaci�n de la Fecha de Primer Pago de Capital, Fecha de Primer Pago de Inter�s
							//y Fecha de Vencimiento del Descuento con las Bases de Operaci�n. Campos 20,21,24
							qrySentencia =
								" SELECT TRUNC (TO_DATE ('"+fppc+"', 'dd/mm/yyyy')) - TRUNC (SYSDATE),"   +
								"        TRUNC (TO_DATE ('"+fppi+"', 'dd/mm/yyyy')) - TRUNC (SYSDATE),"   +
								"        TRUNC (TO_DATE ('"+fechVencDscto+"', 'dd/mm/yyyy')) - TRUNC (SYSDATE)"   +
								"   FROM DUAL"  ;
	
							List lDiferencias = con.consultaRegDB(qrySentencia);
							if(lDiferencias.size()>0) {
								PlazoFPPC = Integer.parseInt(lDiferencias.get(0).toString());
								PlazoFPPI = Integer.parseInt(lDiferencias.get(1).toString());
								PlazoFVD  = Integer.parseInt(lDiferencias.get(2).toString());
							}

							// Validamos la parametrizacion de la base de operaci�n para cr�dito electr�nico, foda 49.
							Hashtable hPlazoBO = getPlazoBO(TipoPlazo, iTipoPiso, NoIf, tasaInteresI, tipoCredito, tablaAmort, emisor, moneda, ppc, tipoRenta, con);
							iPlazoMaxBO = Integer.parseInt(hPlazoBO.get("plazoMax").toString());
	
							if(TipoPlazo.equals("C")) {	// Cr�dito.
								if(iPlazoMaxBO==0) { // Si no hay alg�n plazo en la b.o. se manda el mensaje.
									if(iTipoPiso==1||iTipoPiso==2) {
										log.append(idRegistro+"Campo 24: No existe Plazo M�ximo.");
										throw new NafinException("CRED0097");
									}
								} else if(PlazoFPPC > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
									log.append(idRegistro+"Campo 20: La Fecha de Primer Pago de Capital es mayor al plazo permitido");
									throw new NafinException("CRED0098");
								} else if(PlazoFPPI > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
									log.append(idRegistro+"Campo 21: La Fecha de Primer Pago de Inter�s es mayor al plazo permitido");
									throw new NafinException("CRED0099");
								} else if(PlazoFVD > iPlazoMaxBO) {	// Aplica tanto para 1ro y 2do piso.
									log.append(idRegistro+"Campo 24: La Fecha de Vencimiento del Descuento es mayor al plazo permitido");
									throw new NafinException("CRED0100");
								}
							} else if(TipoPlazo.equals("F")) {	//	Factoraje.
								if(iPlazoMaxBO==0 && iTipoPiso==1) {
									log.append(idRegistro+"Campo 24: No existe Plazo M�ximo.");
									throw new NafinException("CRED0101");
								}
								if(PlazoFPPC > iPlazoMaxBO) {
									log.append(idRegistro+"Campo 20: La Fecha de Primer Pago de Capital es mayor al plazo permitido");
									throw new NafinException("CRED0102");
								}
								if(PlazoFPPI > iPlazoMaxBO) {
									log.append(idRegistro+"Campo 21: La Fecha de Primer Pago de Inter�s es mayor al plazo permitido");
									throw new NafinException("CRED0103");
								}
								if(PlazoFVD > iPlazoMaxBO) {	// Si el plazo de operaci�n es mayor al plazo m�ximo defino.
									log.append(idRegistro+"Campo 24: La Fecha de Vencimiento del Descuento es mayor al plazo permitido");
									throw new NafinException("CRED0104");
								} // Plazo > iPlazoMaxBO
							} // TipoPlazo.equals("F")

							// Validar C�digo de Operaci�n Din�mica
							codigoBaseOpDinamicaHabilitado 	= ( (Boolean) hPlazoBO.get("codigoBaseOpDinamicaHabilitado") ).booleanValue(); 
							existeCodigoBaseOpDinamica			= codigoBaseOpDinamica != null && !"".equals(codigoBaseOpDinamica.trim())?true:false;
							if(         !codigoBaseOpDinamicaHabilitado && existeCodigoBaseOpDinamica  ){
								log.append(idRegistro+"Campo 31: El C�digo de la Base de Operaci�n Din�mica fue incluido, pero no es requerido.");
								throw new NafinException("CRED0117");
							}
							
							//Validaci�n de Fecha M�xima de Amortizaci�n Recortada. Campo 28 = R
							String fechaMax = getFechaMaxRecortado(TipoPlazo, iTipoPiso, NoIf, tasaInteresI, tipoCredito, tablaAmort, emisor, moneda, con);
							//System.out.println("=====LA FECHA MAX = -"+ fechaMax +"-");
							//System.out.println("tablaAmort: "+tablaAmort+ "lbOK: "+lbOK);
System.out.println("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
System.out.println("tablaAmort:"+tablaAmort);

							if("5".equals(tablaAmort)){
								
								List 		vecCampos 	= new ArrayList();
								String 	fecMaxVenc 	= "";
								
								try {
									
									qrySentencia =
										" SELECT TO_CHAR (SYSDATE + "+iPlazoMaxBO+", 'dd/mm/yyyy')"   +
										"   FROM DUAL"  ;
									lDiferencias = con.consultaRegDB(qrySentencia);
System.out.println("lDiferencias:"+lDiferencias);

									if(lDiferencias.size()>0) {
										fecMaxVenc = lDiferencias.get(0).toString();
									}

									qrySentencia =
										" SELECT   ic_amort_portal, fn_monto, to_char(df_vencimiento, 'dd/mm/yyyy'), cg_tipo_amor"   +
										"     FROM "   + enlace.getTablaAmort()+
										"    WHERE ig_clave_sirac  = ? "   +
										"      AND ig_numero_docto = ? "   +
										"		 AND ic_enc				= ? "  +
										" ORDER BY 1"  ;
									lVarBind = new ArrayList();
									lVarBind.add(new Long(claveSirac));
									lVarBind.add(new Long(numDocto));
									lVarBind.add(new Long(encabezado));
System.out.println("qrySentencia:"+qrySentencia);
System.out.println("claveSirac:"+claveSirac);
System.out.println("numDocto:"+numDocto);
System.out.println("encabezado:"+encabezado);
									lAmortizaciones = con.consultaDB(qrySentencia, lVarBind, false);
System.out.println("lAmortizaciones:"+lAmortizaciones);
									vecCampos = ovObtenerCamposDeAmort(lAmortizaciones,fechaMax,fecMaxVenc,iPlazoMaxBO);
									
								} catch (NafinException ne){
									log.append(idRegistro+ne.getMsgError());
									throw ne;
								}
								
								if("N".equals(tipoRenta)){
									
									fppc				= (String)vecCampos.get(0);
									fppi				= (String)vecCampos.get(1);
									fechVencDscto	= (String)vecCampos.get(2);
									diaPago			= (String)vecCampos.get(3);
	
									fppc				= Comunes.strtr(fppc,"\"","");
									fppi				= Comunes.strtr(fppi,"\"","");
									fechVencDscto	= Comunes.strtr(fechVencDscto,"\"","");
									diaPago			= Comunes.strtr(diaPago,"\"","");

								} else {
									
									String diappi	= fppi.substring(0,2);
									String diavenc	= fechVencDscto.substring(0,2);
									if(!diappi.equals(diavenc)){
										log.append(idRegistro+"Campo 24: Los d�as de pago deben ser iguales para todas las amortizaciones");
										throw new NafinException("CRED0105");
									}
									if("14".equals(ppc)){
										log.append(idRegistro+"Campo 9: No se puede seleccionar Periodicidad de Pago catorcenal para el Tipo de Amortizaci�n y Tipo de Renta capturados.");
										throw new NafinException("CRED0106");
									}
									if("14".equals(ppi)){
										log.append(idRegistro+"Campo 10: No se puede seleccionar Periodicidad de Pago catorcenal para el Tipo de Amortizaci�n y Tipo de Renta capturados.");
										throw new NafinException("CRED0107");
									}
									
								}
								
								numAmortRec		= (String)vecCampos.get(4);
								plazoMensual	= (String)vecCampos.get(5);
	
								numAmortRec		= Comunes.strtr(numAmortRec,"\"","");
								plazoMensual	= Comunes.strtr(plazoMensual,"\"","");
								
							} else {
								tipoRenta = "";
							}

							// Obtener consecutivo de IC_SOLIC_PORTAL
							icSolicPortal = "";
					
							lsCadenaSQL.delete(0, lsCadenaSQL.length());
							lsCadenaSQL.append(
							" SELECT SEQ_COM_SOLIC_PORTAL.NEXTVAL "   +
							"   FROM DUAL ");

							List lSolic = con.consultaRegDB(lsCadenaSQL.toString());
							if(lSolic.size()>0)
								icSolicPortal = lSolic.get(0).toString();
							
							// Insertar registro en COM_SOLIC_PORTAL
							lsCadenaSQL.delete(0, lsCadenaSQL.length());
							lsCadenaSQL.append(
								" INSERT INTO com_solic_portal ("   +
								"              ic_solic_portal, ic_if, ig_clave_sirac, ig_sucursal, ic_moneda,"   +
								"              ig_numero_docto, fn_importe_docto, fn_importe_dscto, ic_emisor,"   +
								"              ic_tipo_credito, ic_periodicidad_c, ic_periodicidad_i,"   +
								"              cg_bienes_servicios, ic_clase_docto, cg_domicilio_pago, ic_tasauf,"   +
								"              cg_rmuf, fg_stuf, ic_tasaif, cg_rmif, fg_stif, in_numero_amort,"   +
								"              ic_tabla_amort, df_ppc, df_ppi, in_dia_pago, df_v_documento,"   +
								"              df_v_descuento, ig_plazo, df_etc, cg_lugar_firma, cs_amort_ajuste,"   +
								"              fg_importe_ajuste, cs_tipo_plazo, cs_plazo_maximo_factoraje,"   +
								"              cs_tipo_renta, in_numero_amort_rec, cs_plazo_mensual,"   +
								"              ic_estatus_solic, cc_acuse, ic_producto_nafin,"   +
								"              ic_solicitud_equipo, ig_numero_solic_troya, cc_llave, ig_base_operacion )"   +
								"      VALUES ("   +
								"              ?, ?, ?, ?, ?,"   +
								"              ?, ?, ?, ?,"   +
								"              ?, ?, ?,"   +
								"              ?, ?, ?, ?,"   +
								"              ?, ?, ?, ?, ?, ?,"   +
								"              ?, to_date(?, 'dd/mm/yyyy'), to_date(?, 'dd/mm/yyyy'), ?, to_date(?, 'dd/mm/yyyy'),"   +
								"              to_date(?, 'dd/mm/yyyy'), ?, to_date(?, 'dd/mm/yyyy'), ?, ?,"   +
								"              ?, ?, ?,"   +
								"              ?, ?, ?,"   +
								"              ?, ?, ?,"   +
								"              ?, ?, ?, ? )");
							
							try {
								
								lVarBind = new ArrayList();
	
								lVarBind.add(new Long(icSolicPortal));
								lVarBind.add(new Integer(NoIf));
								lVarBind.add(new Long(claveSirac));
								if(!"".equals(noSucBanco))
									lVarBind.add(new Long(noSucBanco));
								else
									lVarBind.add(Long.TYPE);
								lVarBind.add(new Integer(moneda));
								lVarBind.add(new Long(numDocto));
	
								lVarBind.add(new Double(importeDocto));
								lVarBind.add(new Double(importeDscto));
								if(!"".equals(icEmisor))
									lVarBind.add(new Integer(icEmisor));
								else
									lVarBind.add(Integer.TYPE);
								lVarBind.add(new Integer(icTipoCredito));
								lVarBind.add(new Integer(icTipoPPC));
	
								lVarBind.add(new Integer(icTipoPPI));
								lVarBind.add(bieneServi);
								lVarBind.add(new Integer(icClaseDocto));
								lVarBind.add(domicilioPago);
								lVarBind.add(new Integer(icTasaUF));
								lVarBind.add(relMatUF);
	
								lVarBind.add(new Double(sobreTasaUF));
								lVarBind.add(new Integer(icTasaIntermed));
								lVarBind.add(relMatI);
								if(!"".equals(sobreTasaI))
									lVarBind.add(new Double(sobreTasaI));
								else
									lVarBind.add(Double.TYPE);
								lVarBind.add(new Integer(numAmort));
								lVarBind.add(new Integer(icTablaAmort));
								lVarBind.add(fppc);
								lVarBind.add(fppi);
	
								lVarBind.add(new Integer(diaPago));
								lVarBind.add(fechVencDocto);
								lVarBind.add(fechVencDscto);
								lVarBind.add(new Integer(PlazoFVD));
								lVarBind.add(fetc);
								lVarBind.add(lugarFirma);
								lVarBind.add(amortAjuste);								
	
								if(!"".equals(amortImporte))
									lVarBind.add(new Double(amortImporte));
								else
									lVarBind.add(Double.TYPE);
								lVarBind.add(TipoPlazo);
								lVarBind.add(sTomoPlazoGral);
								lVarBind.add(tipoRenta);
								if(!"".equals(numAmortRec))
									lVarBind.add(new Integer(numAmortRec));
								else
									lVarBind.add(Integer.TYPE);
	
								lVarBind.add(plazoMensual);
	
								int estatusSolicitud = 1;
								if(esIfBloqueado(NoIf, "0", con))
									estatusSolicitud = 12;
	
								lVarBind.add(new Integer(estatusSolicitud));
								lVarBind.add(acuse.toString());
								lVarBind.add(Integer.TYPE);
	
								lVarBind.add(Integer.TYPE);
								lVarBind.add(Integer.TYPE);
	
								String fechaLlave = (new java.text.SimpleDateFormat("ddMMyyyy")).format(new java.util.Date());
								String cc_llave = NoIf + claveSirac + numDocto + fechaLlave;
								lVarBind.add(cc_llave);
								
								// Codigo Base Operacion Dinamica
								if(existeCodigoBaseOpDinamica)
									lVarBind.add(new Integer(codigoBaseOpDinamica));
								else
									lVarBind.add(Integer.TYPE);
								
								con.ejecutaUpdateDB(lsCadenaSQL.toString(), lVarBind);
								
		               } catch (Exception error) {
								
		                	error.printStackTrace();
								log.append(idRegistro+"Error de Concurrencia, hay registros Repetidos.");
								throw new NafinException("CRED0108");
								
							}

						// Valida e inserta las amortizaciones
System.out.println("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_");
System.out.println("tipoRenta:"+tipoRenta);

							if("N".equals(tipoRenta)) {
								try{
System.out.println("icSolicPortal:"+icSolicPortal);
System.out.println("lAmortizaciones:"+lAmortizaciones);
System.out.println("fechaMax:"+fechaMax);

									ovGenerarAmortizaciones(
												icSolicPortal,
												Integer.parseInt(numAmort),
												Double.parseDouble(importeDocto),
												Double.parseDouble(importeDscto),
												lAmortizaciones,
												fechaMax,
												con);
								} catch(NafinException ne){
									if(TA){
										log.append(idRegistro+"Campo "+campoError+": El Tipo de Amortizaci�n no es v�lido");
										throw ne;
									} else {
										log.append(idRegistro+"Campo "+campoError+": "+ne.getMsgError());
										throw ne;
									}
								}
							}

							// FXXX-2010: YA NO SE INSERTARAN REGISTROS SIN ERROR EN LA TABLA: COM_SOLIC_PUB_ACU_RESUL_FIDE
							/*
							//SE ALMACENA EL EL FOLIO RESULTANTE
							EnlErrCargaCE enlErrCargaCE = new EnlErrCargaCE();
							enlErrCargaCE.setCc_acuse(acuse.toString());
							enlErrCargaCE.setIg_clave_sirac(claveSirac);
							enlErrCargaCE.setIg_numero_docto(numDocto);
							//enlErrCargaCE.setIg_solic_portal(icSolicPortal);
							enlErrCargaCE.setCc_error("0");
	
							qrySentencia = enlace.getInsertaErrores(enlErrCargaCE);
							lVarBind = enlace.getVariablesBind();
							existe = con.ejecutaUpdateDB(qrySentencia, lVarBind);
							*/
							
						} catch(NafinException ne) {
con.terminaTransaccion(false);
							lbOK = false;
							codigoError = ne.getCodError();
							EnlErrCargaCE enlErrCargaCE = new EnlErrCargaCE();
							enlErrCargaCE.setCc_acuse(acuse.toString());
							enlErrCargaCE.setIg_clave_sirac(claveSirac);
							enlErrCargaCE.setIg_numero_docto(numDocto);
							//enlErrCargaCE.setIg_solic_portal("");
							enlErrCargaCE.setIcEstatusSolic("4"); // Rechazada NAFIN
							enlErrCargaCE.setCc_error(codigoError);
							
							qrySentencia = enlace.getInsertaErrores(enlErrCargaCE);
							lVarBind = enlace.getVariablesBind();
							existe = con.ejecutaUpdateDB(qrySentencia, lVarBind);

con.terminaTransaccion(true);

						} catch(Exception exc) {
							exc.printStackTrace();
con.terminaTransaccion(false);
							lbOK = false;
							EnlErrCargaCE enlErrCargaCE = new EnlErrCargaCE();
							enlErrCargaCE.setCc_acuse(acuse.toString());
							enlErrCargaCE.setIg_clave_sirac(claveSirac);
							enlErrCargaCE.setIg_numero_docto(numDocto);
							//enlErrCargaCE.setIg_solic_portal("");
							enlErrCargaCE.setIcEstatusSolic("4"); // Rechazada NAFIN
							enlErrCargaCE.setCc_error("SIST001");
	
							qrySentencia = enlace.getInsertaErrores(enlErrCargaCE);
							lVarBind = enlace.getVariablesBind();
							existe = con.ejecutaUpdateDB(qrySentencia, lVarBind);
con.terminaTransaccion(true);
						} finally {
							
							con.terminaTransaccion(lbOK);
							
						}
						
System.out.println("lbOK:"+lbOK);
						acuseEnl.add(lbOK);
System.out.println("lbOK:"+lbOK);

					}//for(int i=0;i<lDatos.size();i++)

					// BORRAR AMORTIZACIONES
					qrySentencia =
						" DELETE FROM "+enlace.getTablaAmort()+" " +
						" WHERE IC_ENC = ?  ";
					lVarBind = new ArrayList();
					lVarBind.add(new Long(encabezado));
					existe = con.ejecutaUpdateDB(qrySentencia,lVarBind);
					
					// BORRAR OPERACIONES 
					qrySentencia =
						" DELETE FROM "+enlace.getTablaDocumentos()+" " +
						" WHERE IC_ENC = ? ";
					lVarBind = new ArrayList();
					lVarBind.add(new Long(encabezado));
					existe = con.ejecutaUpdateDB(qrySentencia,lVarBind);
					
					// BORRAR "ENCABEZADO" DE LOS DOCUMENTOS 
					qrySentencia =
						" DELETE FROM "+enlace.getTablaEncabezadoDocumentos()+" " +
						" WHERE IC_ENC = ? ";
					lVarBind = new ArrayList();
					lVarBind.add(new Long(encabezado));
					existe = con.ejecutaUpdateDB(qrySentencia,lVarBind);
 
				}//if(existenReg>0)
				else {
					estatusAcu = "03";
				}

System.out.println("acuseEnl:"+acuseEnl.toString());
				acuseEnl.setIgEstatus(1); // Registro(s) procesado(s) 
				qrySentencia = enlace.getUpdateAcuse(acuseEnl);
				lVarBind     = enlace.getVariablesBind();

System.out.println("qrySentencia:"+qrySentencia);
System.out.println("lVarBind:"+lVarBind);

				existe = con.ejecutaUpdateDB(qrySentencia, lVarBind);
		
System.out.println("existe:"+existe);

			}// for(int indice=0;indice<encabezados.size();indice++)
		
		} catch(NafinException ne){
			System.out.println("CargaCreditoEJB::procesaCargaMasivaCE(NafinException)");
			ne.printStackTrace();
			throw ne;
		} catch (Exception e){
			System.out.println("CargaCreditoEJB::procesaCargaMasivaCE(Exception) "+e);
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			con.terminaTransaccion(true);
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("CargaCreditoEJB::procesaCargaMasivaCE(S)");
		}
		
	}

	private int getTipoPisoIF(String sNoIF, AccesoDB con) throws NafinException {
		System.out.println("CargaCreditoEJB::getTipoPisoIF(E)");
		int iTipoPiso = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.queryPrecompilado("select ig_tipo_piso from comcat_if where ic_if = ?");
			ps.setInt(1, Integer.parseInt(sNoIF.trim()));
			rs = ps.executeQuery();
			ps.clearParameters();
			if(rs.next()) {
				iTipoPiso = rs.getInt("ig_tipo_piso");
			}
			rs.close();
			if(ps!=null) ps.close();
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("CargaCreditoEJB::getTipoPisoIF(Exception). "+e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			System.out.println("CargaCreditoEJB::getTipoPisoIF(S)");
		}
		return iTipoPiso;
	}

	private void existeNoDocto(String NoDocto,String NoIF,String claveSirac,AccesoDB con)
		throws NafinException{
		System.out.println("CargaCreditoEJB::existeNoDocto (E)");
		PreparedStatement	ps 		= null;
		ResultSet			rs 		= null;
		String		qrySentencia	= "";
			try{
				int numCols = 0;

				String fechaHoy = (new java.text.SimpleDateFormat("ddMMyyyy")).format(new java.util.Date());

				qrySentencia =
					" select count(1) from com_solic_portal" +
					" where cc_llave = ?";


				ps = con.queryPrecompilado(qrySentencia);

				String cc_llave = NoIF + claveSirac + NoDocto + fechaHoy;


				ps.setString(1, cc_llave);
				rs = ps.executeQuery();
				if(rs.next()){
					numCols = rs.getInt(1);
				}
				rs.close();ps.close();


				if(numCols>0) {
					throw new NafinException("CRED0009");
				}

			}catch(NafinException ne){
				System.out.println("CargaCreditoEJB::existeNoDocto(NafinException) "+ne);
				throw ne;
			}catch(Exception e){
				System.out.println("CargaCreditoEJB::existeNoDocto(Exception) "+e);
				throw new NafinException("SIST0001");
			}finally{
				System.out.println("CargaCreditoEJB::existeNoDocto (S)");
			}
	}

    private String sValidaEmisor( String esEmisor, AccesoDB con)
    	throws Exception {
		System.out.println("CargaCreditoEJB::sValidaEmisor (E)");
    	StringBuffer lsSQL 	= new StringBuffer();
    	PreparedStatement ps = null;
        String licEmisor 	= null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_emisor from comcat_emisor ");
		lsSQL.append(" where ic_emisor = ?");

		ps = con.queryPrecompilado(lsSQL.toString());
		ps.setInt(1, Integer.parseInt(esEmisor));
		lrsSel = ps.executeQuery();
		if (lrsSel.next())
			licEmisor = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar el emisor");
        }
        lrsSel.close();ps.close();
        lsSQL = null;
		System.out.println("CargaCreditoEJB::sValidaEmisor (S)");
        return licEmisor;
    }


    private String sValidaPeriodicidadPP( String esppc, AccesoDB con)
    	throws Exception {
		System.out.println("CargaCreditoEJB::sValidaPeriodicidadPP (E)");
    	StringBuffer lsSQL 	= new StringBuffer();
    	PreparedStatement ps = null;
        String licTipoPPC = null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_periodicidad from comcat_periodicidad ");
		lsSQL.append(" where ic_periodicidad = ?");

		ps = con.queryPrecompilado(lsSQL.toString());
		ps.setInt(1, Integer.parseInt(esppc));
		lrsSel = ps.executeQuery();

		if (lrsSel.next())
			licTipoPPC = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar la Periodicidad del Pago de Capital");
        }
        lrsSel.close();ps.close();
        lsSQL = null;
		System.out.println("CargaCreditoEJB::sValidaPeriodicidadPP (S)");
        return licTipoPPC;
    }

    private String sValidaClaseDocto( String esClaseDocto, AccesoDB con)
    	throws Exception {
		System.out.println("CargaCreditoEJB::sValidaClaseDocto (E)");
    	StringBuffer lsSQL 	= new StringBuffer();
    	PreparedStatement ps = null;
        String licClaseDocto = null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_clase_docto from comcat_clase_docto ");
		lsSQL.append(" where ic_clase_docto = ?");

		ps = con.queryPrecompilado(lsSQL.toString());
		ps.setInt(1, Integer.parseInt(esClaseDocto));
		lrsSel = ps.executeQuery();

		if (lrsSel.next())
			licClaseDocto = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar la Clase del Documento");
        }
        lrsSel.close();ps.close();
        lsSQL = null;
		System.out.println("CargaCreditoEJB::sValidaClaseDocto (S)");
        return licClaseDocto;
    }

    private String sValidaTablaAmortizacion( String establaAmortizacion, AccesoDB con)
    	throws Exception {
		System.out.println("CargaCreditoEJB::sValidaTablaAmortizacion (E)");
    	StringBuffer lsSQL 	= new StringBuffer();
    	PreparedStatement ps = null;
        String licTablaAmort = null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_tabla_amort from comcat_tabla_amort ");
		lsSQL.append(" where ic_tabla_amort = ?");

		ps = con.queryPrecompilado(lsSQL.toString());
		ps.setInt(1, Integer.parseInt(establaAmortizacion));
		lrsSel = ps.executeQuery();

		if (lrsSel.next())
			licTablaAmort = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar la Tabla de Amortizaci�n");
        }
        lrsSel.close();ps.close();
        lsSQL = null;
		System.out.println("CargaCreditoEJB::sValidaTablaAmortizacion (S)");
        return licTablaAmort;
    }

    private String sValidaTasaInteresIntermed( String estasaInteresIntermed, String habilitado, AccesoDB con)
    	throws Exception {
		System.out.println("CargaCreditoEJB::sValidaTasaInteresIntermed (E)");
    	StringBuffer lsSQL 	= new StringBuffer();
    	PreparedStatement ps = null;
        String licTasaIntermed = null;
        ResultSet lrsSel 	= null;

		lsSQL.append(" select ic_tasa from comcat_tasa ");
		lsSQL.append(" where ic_tasa = ?");

		if (habilitado.equals("S"))
			lsSQL.append(" and cs_creditoelec = ?");

		ps = con.queryPrecompilado(lsSQL.toString());
		ps.setInt(1, Integer.parseInt(estasaInteresIntermed));

		if (habilitado.equals("S"))
			ps.setString(2, "S");

		lrsSel = ps.executeQuery();

		if (lrsSel.next())
			licTasaIntermed = lrsSel.getString(1);
        else {
			throw new Exception("Error al validar la Tasa de Interes clave del Intermediario");
        }
        lrsSel.close();ps.close();
        lsSQL = null;
		System.out.println("CargaCreditoEJB::sValidaTasaInteresIntermed (S)");
        return licTasaIntermed;
    }

    private String sValidaTipoCredito( String esTipoCredito, AccesoDB con)
    	throws NafinException, Exception {
		System.out.println("CargaCreditoEJB::sValidaTipoCredito (E)");

    	StringBuffer lsSQL 	= new StringBuffer();
    	PreparedStatement ps = null;
        String licTipoCredito = null;
        ResultSet lrsSel 	= null;
        try {
			lsSQL.append(" select ic_tipo_credito from comcat_tipo_credito ");
			lsSQL.append(" where ic_tipo_credito = ? ");
			lsSQL.append(" and cs_creditoelec = ?");
			System.out.println("valor de tipo de credito:::::::::::::::"+esTipoCredito);
			ps = con.queryPrecompilado(lsSQL.toString());
			ps.setInt(1, Integer.parseInt(esTipoCredito));
			ps.setString(2, "S");

			lrsSel = ps.executeQuery();

			if (lrsSel.next())
				licTipoCredito = lrsSel.getString(1);
	        else {
				throw new Exception("Error al validar el Tipo cr�dito");
	        }
	        lrsSel.close();ps.close();
	        lsSQL = null;
        } catch(Exception e) {
        	e.printStackTrace();
        	throw new NafinException("SIST0001");
        } finally {
			System.out.println("CargaCreditoEJB::sValidaTipoCredito (S)");
        }
        return licTipoCredito;
    }


    private List sGetDatosBO(String NoIF, int TipoPiso, String TipoPlazo, AccesoDB con) throws Exception {
		System.out.println("CargaCreditoEJB::sGetDatosBO (E)");

		System.out.println("NoIF: "+NoIF);
		System.out.println("TipoPiso: "+TipoPiso);
		System.out.println("TipoPlazo: "+TipoPlazo);

    	List 	lDatos 			= new ArrayList();
    	List 	lEmisor 		= new ArrayList();
    	List 	lTipoCredito 	= new ArrayList();
    	List 	lTasa 			= new ArrayList();
    	List 	lTablaAmort 	= new ArrayList();
    	List 	lRenglones 		= new ArrayList();
    	List 	lVarBind 		= new ArrayList();
    	int 	registros		= 0;

    	String 	qrySentencia =
			" SELECT ic_emisor, ic_tipo_credito, ic_tasa, ic_tabla_amort"   +
			"   FROM comvis_base_op_credele"   +
			"  WHERE ic_if = ?"   +
			"    AND ig_tipo_cartera = ?"   +
			"    AND cs_tipo_plazo = ?"  ;
		lVarBind.add(new Integer(NoIF));
		lVarBind.add(new Integer(TipoPiso));
		lVarBind.add(TipoPlazo);
		lDatos = con.consultaDB(qrySentencia, lVarBind, false);
		for(int i=0;i<lDatos.size();i++) {
			List lRegistros = (ArrayList)lDatos.get(i);
			String ic_emisor 		= lRegistros.get(0).toString();
			String ic_tipo_credito 	= lRegistros.get(1).toString();
			String ic_tasa 			= lRegistros.get(2).toString();
			String ic_tabla_amort 	= lRegistros.get(3).toString();
			lEmisor.add(ic_emisor);
			lTipoCredito.add(ic_tipo_credito);
			lTasa.add(ic_tasa);
			lTablaAmort.add(ic_tabla_amort);
			registros++;
		}//for(int i=0;i<lDatos.size();i++)

		if(registros>0) {
			lRenglones.add(lEmisor);
			lRenglones.add(lTipoCredito);
			lRenglones.add(lTasa);
			lRenglones.add(lTablaAmort);

		}
		System.out.println("CargaCreditoEJB::sGetDatosBO (S)");
        return lRenglones;
    }

	private Hashtable getPlazoBO(String TipoPlazo, int iTipoPiso, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda, String iPeriodicidad, String iTipoRenta,
							AccesoDB con ) throws NafinException {
	System.out.println("CargaCreditoEJB::getPlazoBO (E)");
	PreparedStatement ps = null;
	ResultSet rs = null;
	int plazoMax = 0;
	Hashtable hPlazoBO = new Hashtable();
	String query = "", sTomoPlazoGral = "N";
	boolean codigoBaseOpDinamicaHabilitado = false;
	boolean plazoEncontrado = false;
		try{
			/*
						" select nvl(max(boc.ig_plazo_maximo),0) as plazoMax "+
						"   from com_base_op_credito boc "+
						" where boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?" and boc.ic_emisor is null ":"	and boc.ic_emisor = ? "
			*/
			
			// Nota: Se toma cs_base_op_dinamica del registro que tiene el plazo mas grande (ig_plazo_maximo) y en caso de plazos empatados
			// se toma el mas reciente: ic_base_op_credito
			query = " select nvl(boc.ig_plazo_maximo,0) as plazoMax, boc.cs_base_op_dinamica as cs_base_op_dinamica "+
						"   from com_base_op_credito boc "+
						" where boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?       " and boc.ic_emisor       is null ":" and boc.ic_emisor       = ? ")+
						(iPeriodicidad.equals("")?" and boc.ic_periodicidad is null ":" and boc.ic_periodicidad = ? ")+//Se agrega condicion por F014-2012 JSHD
						(iTipoRenta.equals("")?   " and boc.cs_tipo_renta   is null ":" and boc.cs_tipo_renta   = ? ")+//Se agrega condicion por F014-2012 JSHD
						" order by plazoMax desc, boc.ic_base_op_credito desc ";
			try {
				
				ps = con.queryPrecompilado(query);
				ps.setInt(1, iTipoPiso);
				ps.setInt(2, Integer.parseInt(NoIF.trim()));
				ps.setInt(3, Integer.parseInt(TasaI.trim()));
				ps.setInt(4, Integer.parseInt(TipoCredito.trim()));
				ps.setInt(5, Integer.parseInt(Amortizacion.trim()));
				ps.setObject(6, TipoPlazo);
				int idx = 7;
				if(!Emisor.equals("")) 			ps.setInt(idx++, 		Integer.parseInt(Emisor.trim()));
				if(!iPeriodicidad.equals("")) ps.setInt(idx++, 		Integer.parseInt(iPeriodicidad.trim()));
				if(!iTipoRenta.equals("")) 	ps.setString(idx++, 	iTipoRenta.trim());
				rs = ps.executeQuery();
				if(rs.next()){
					plazoMax 								= rs.getInt("plazoMax");
					codigoBaseOpDinamicaHabilitado 	= "S".equals(rs.getString("cs_base_op_dinamica"))?true:false;
					plazoEncontrado						= true;
				} else {
					plazoMax         						= 0;
					codigoBaseOpDinamicaHabilitado	= false;
					plazoEncontrado						= false;
				}
				ps.clearParameters();
 
				if( !plazoEncontrado ){ // Si el plazo no fue encontrado
					
					rs.close();
					ps.close();
 
					query = 
						" select nvl(boc.ig_plazo_maximo,0) as plazoMax, boc.cs_base_op_dinamica as cs_base_op_dinamica "+
						"   from com_base_op_credito boc "+
						" where boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?       " and boc.ic_emisor       is null ":" and boc.ic_emisor       = ? ")+
						" and boc.ic_periodicidad is null " +//Se agrega condicion por F014-2012 JSHD
						" and boc.cs_tipo_renta   is null " +//Se agrega condicion por F014-2012 JSHD
						" order by plazoMax desc, boc.ic_base_op_credito desc ";
						
					ps = con.queryPrecompilado(query);
					ps.setInt(1, iTipoPiso);
					ps.setInt(2, Integer.parseInt(NoIF.trim()));
					ps.setInt(3, Integer.parseInt(TasaI.trim()));
					ps.setInt(4, Integer.parseInt(TipoCredito.trim()));
					ps.setInt(5, Integer.parseInt(Amortizacion.trim()));
					ps.setObject(6, TipoPlazo);
					if(!Emisor.equals("")) ps.setInt(7, Integer.parseInt(Emisor.trim()));
					rs = ps.executeQuery();
					if(rs.next()){
						plazoMax 								= rs.getInt("plazoMax");
						codigoBaseOpDinamicaHabilitado 	= "S".equals(rs.getString("cs_base_op_dinamica"))?true:false;
						plazoEncontrado						= true;
					} else {
						plazoMax         						= 0;
						codigoBaseOpDinamicaHabilitado	= false;
						plazoEncontrado						= false;
					}
					
				}
				
				// En caso de ser Factoraje y que no tenga parametrizada un B.O. se toma el plazo generico de la moneda.
				if(TipoPlazo.equals("F") && plazoMax==0 && iTipoPiso==2) {
					
					rs.close();
					ps.close();
 
					query =
						" SELECT DECODE ("   +
						"           ?,"   +
						"           1, ig_plazo_maximo_factoraje,"   +
						"           54, ig_plazo_maximo_factoraje_dl"   +
						"        ) AS plazomax"   +
						"   FROM comcat_producto_nafin"   +
						"  WHERE ic_producto_nafin = ?"  ;
					ps = con.queryPrecompilado(query);
					ps.setInt(1, Integer.parseInt(Moneda.trim()));
					ps.setInt(2, 1);
					rs = ps.executeQuery();
					if(rs.next()) {
						plazoMax = rs.getInt("plazoMax");
						sTomoPlazoGral = "S";
					}
					ps.clearParameters();
				}

			} catch(SQLException sqle) {
				throw new NafinException("DSCT0085");
			}
		} catch(NafinException ne) {
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getPlazoBO(). "+e.getMessage());
			throw new NafinException("SIST0001");
		} finally {
			
			if( rs != null){ try { rs.close(); }catch(Exception e){} }
			if( ps != null){ try { ps.close(); }catch(Exception e){} }
			
			hPlazoBO.put("plazoMax", 								new Integer(plazoMax)								);
			hPlazoBO.put("sTomoPlazoGral", 						sTomoPlazoGral											);
			hPlazoBO.put("codigoBaseOpDinamicaHabilitado",	new Boolean(codigoBaseOpDinamicaHabilitado) 	);
			
			System.out.println("CargaCreditoEJB::getPlazoBO (S)");
		}
		
		return hPlazoBO;
		
	}
	
	private String getFechaMaxRecortado(String TipoPlazo, int iTipoPiso, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String Moneda, AccesoDB con)
		throws NafinException {
	System.out.println("CargaCreditoEJB::getFechaMaxRecortado (E)");
	PreparedStatement ps = null;
	ResultSet rs = null;
	String query = "";
	String fechaMax = "";
		try{
			query = " select to_char(sysdate + dias_minimos_primer_pago,'dd/mm/yyyy') "+
						"   from com_base_op_credito boc "+
						"	,no_bases_de_operacion nbo"+
						" where boc.ig_codigo_base = nbo.codigo_base_operacion"+
						"	and boc.ig_tipo_cartera = ? "+
						"   and boc.ic_if = ? "+
						"	and boc.ic_tasa = ? "+
						"   and boc.ic_tipo_credito = ? "+
						"	and boc.ic_tabla_amort = ? "+
						"	and boc.cs_tipo_plazo = ? "+
						(Emisor.equals("")?" and boc.ic_emisor is null ":"	and boc.ic_emisor = ? ");

				ps = con.queryPrecompilado(query);
				ps.setInt(1, iTipoPiso);								System.out.println("***TIPO PISO = "+iTipoPiso);
				ps.setInt(2, Integer.parseInt(NoIF.trim()));			System.out.println("***IF = "+NoIF);
				ps.setInt(3, Integer.parseInt(TasaI.trim()));			System.out.println("***TASA IF = "+TasaI);
				ps.setInt(4, Integer.parseInt(TipoCredito.trim()));		System.out.println("***TIPO CREDITO = "+TipoCredito);
				ps.setInt(5, Integer.parseInt(Amortizacion.trim()));	System.out.println("***AMORTIZACION = "+Amortizacion);
				ps.setObject(6, TipoPlazo);								System.out.println("***TIPO PLAZO = "+TipoPlazo);
				if(!Emisor.equals("")) ps.setInt(7, Integer.parseInt(Emisor.trim()));	System.out.println("***EMISOR = "+Emisor);
				rs = ps.executeQuery();
				if(rs.next()){
					fechaMax = (rs.getString(1)==null)?"":rs.getString(1);
				}
				if(ps!=null) ps.close();

				if("".equals(fechaMax))
					throw new NafinException("CRED0004");
		}catch(NafinException ne){
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getFechaMinRecortado(). "+e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("CargaCreditoEJB::getFechaMaxRecortado (S)");
		}
	return fechaMax;
	}

	private List ovObtenerCamposDeAmort(List eovDatos,String fechaMax,String fecMaxVenc,int PlazoMax)
		throws NafinException{
		System.out.println("CargaCreditoEJB.ovObtenerCamposDeAmort (E)");
		List vecRetorno = new ArrayList();
		int i = 0;
		String fppc		= "";
		String fppi		= "";
		String fvencdes	= "";
		String diappc	= "";
		int		numAmortRec = 0;
		java.util.Date 		dFecMax			= Comunes.parseDate(fechaMax);
		java.util.Date		dFecAmortAnt	= Comunes.parseDate(new SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()));
		java.util.Date 		fMaxVenc		= Comunes.parseDate(fecMaxVenc);
		java.util.Date 		fechaHoy		= new java.util.Date();
		String	plazoMensual 	= "S";
		try{
System.out.println("eovDatos: "+eovDatos);
			for(i=0;i<eovDatos.size();i++){
				List lRegistro = (ArrayList)eovDatos.get(i);
				String montoAmort = lRegistro.get(1).toString().trim();
				String fechaAmort = lRegistro.get(2).toString().trim();
				String tipoAmort  = lRegistro.get(3).toString().trim();

System.out.println("montoAmort: "+montoAmort);
System.out.println("fechaAmort: "+fechaAmort);
System.out.println("tipoAmort: "+tipoAmort);

				if(montoAmort.equals("")){
					throw new NafinException("CRED0109");
//					error = "El Importe de Amortizaci�n no est� capturado.";
				} else if(!Comunes.esDecimal(montoAmort)) {
					throw new NafinException("CRED0110");
//					error = "El Importe de Amortizaci�n no es un valor v�lido.";
				}
				if(fechaAmort.equals("")){
					throw new NafinException("CRED0111");
//					error = "La Fecha de Amortizaci�n no est� capturada.";
				} else {
					if(!Comunes.checaFecha(fechaAmort)) {
						throw new NafinException("CRED0112");
//						error = "La Fecha de Amortizaci�n no es una fecha correcta";
					} else {
						java.util.Date fecAmorttmp = Comunes.parseDate(fechaAmort);
						int iComp = fecAmorttmp.compareTo(fechaHoy);
						if(iComp < 0){
							throw new NafinException("CRED0113");
//							error = "La Fecha de Amortizaci�n es menor a la Fecha de Hoy";
						}
						iComp = fecAmorttmp.compareTo(fMaxVenc);
						if(iComp > 0){
							throw new NafinException("CRED0114");
//							error = "La Fecha de Amortizaci�n es mayor al plazo permitido";
						}
					}
				}
				if(!(tipoAmort.equals("A")||tipoAmort.equals("I")||tipoAmort.equals("R"))){
					throw new NafinException("CRED0115");
//					error = "El Tipo de Amortizaci�n no es v�lido";
				}

				java.util.Date dFecAmort	= Comunes.parseDate(fechaAmort);
				if(dFecAmortAnt!=null){
					long lfecAmort = dFecAmort.getTime();
					long lfecAmortAnt = dFecAmortAnt.getTime();
					double diferencia = (lfecAmort - lfecAmortAnt)/86400000;

					System.out.println("\n\n\n\nLINEA 4113, LA DIFERENCIA = "+diferencia);

					if((int)diferencia>31){
						plazoMensual = "N";
					}
				}
				//System.out.println("=====FECHA AMORT = -"+dFecAmort.toString()+"-");
				//System.out.println("=====LA FECHA MAX = -"+dFecMax.toString()+"-");
				//System.out.println("=====LA COMPARACION = "+dFecMax.compareTo(dFecAmort));

				if(dFecMax.compareTo(dFecAmort)>=0)
					tipoAmort = "R";

				if("A".equals(tipoAmort)){
					if("".equals(fppc))
						fppc = fechaAmort;
					fvencdes = fechaAmort;
				}
				if(("I".equals(tipoAmort)||"A".equals(tipoAmort))&&"".equals(fppi)){
					fppi = fechaAmort;
				}
				if("R".equals(tipoAmort)&&Double.parseDouble(montoAmort)>0){
					numAmortRec++;
				}
				dFecAmortAnt = Comunes.parseDate(fechaAmort);
			}
			if(!"".equals(fppc))
				diappc	= fppc.substring(0,2);

/*0*/		vecRetorno.add(fppc);
/*1*/		vecRetorno.add(fppi);
/*2*/		vecRetorno.add(fvencdes);
/*3*/		vecRetorno.add(diappc);
/*4*/		vecRetorno.add(numAmortRec+"");
/*5*/		vecRetorno.add(plazoMensual);

		}catch(NafinException ne){
			throw ne;
		}catch(Exception e){
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("CargaCreditoEJB.ovObtenerCamposDeAmort (S)");
		}
		return vecRetorno;
	}

	private void ovGenerarAmortizaciones(String icSolicPortal,int numAmort,double importeDocto, double importeDscto, List vecAmortizaciones,String fechaMax,AccesoDB con)
		throws NafinException{
		System.out.println("CargaCreditoEJB.ovGenerarAmortizaciones (E)");
		try{
			PreparedStatement	ps				= null;
			String				qrySentencia	= "";
			boolean				recortada		= false;
			boolean				interes			= false;
			boolean				capitalInt		= false;
			int 				i = 0, j = 0;
			BigDecimal			totalImpAmort	= new BigDecimal(0.0);
			BigDecimal			totalImpAmNoRec	= new BigDecimal(0.0);
			java.util.Date 		dFecMax			= Comunes.parseDate(fechaMax);
			java.util.Date		dFecAmortAnt	= new java.util.Date();

			qrySentencia =
				" INSERT INTO com_amort_portal"   +
				"             (ic_amort_portal, ic_solic_portal, cg_tipo_amort, fn_monto,"   +
				"              df_vencimiento)"   +
				"    SELECT NVL (MAX (ic_amort_portal), 0) + 1, ?, ?, ?,"   +
				"           TO_DATE (?, 'dd/mm/yyyy')"   +
				"      FROM com_amort_portal"   +
				"     WHERE ic_solic_portal = ?"  ;

			ps = con.queryPrecompilado(qrySentencia);
			for(i=0,j=0; i<vecAmortizaciones.size(); i++,j++){
				List lRegistro = (ArrayList)vecAmortizaciones.get(i);
				String strImpAmort = lRegistro.get(1).toString().trim();
				String fechaAmort = lRegistro.get(2).toString().trim();
				String tipoAmort = lRegistro.get(3).toString().trim();
				double importeAmort = 0;

				strImpAmort = Comunes.strtr(strImpAmort,"\"","");
				strImpAmort = Comunes.strtr(strImpAmort,",","");

				importeAmort = Double.parseDouble(strImpAmort);



				java.util.Date dFecAmort	= Comunes.parseDate(fechaAmort);

				/* revisa que la fecha no sea menor a la anterior*/
				if(i>0){
					if(dFecAmortAnt.compareTo(dFecAmort)>=0){
						campoError = 28 + i + 3;
						throw new NafinException("CRED0006");
					}
				}
				if(dFecMax.compareTo(dFecAmort)>=0)
					tipoAmort = "R";

				/*Valida los tipos de amortizaciones*/
				if("A".equals(tipoAmort)){
					capitalInt = true;
					totalImpAmNoRec = totalImpAmNoRec.add(new BigDecimal(importeAmort));
					if(importeAmort==0){
						campoError = 28 + i + 2;
						throw new NafinException("CRED0012");
					}
					if(importeAmort<0){
						campoError = 28 + i + 2;
						throw new NafinException("CRED0013");
					}
				}else if("I".equals(tipoAmort)){
					importeAmort = 0;
					interes = true;
					/*if(capitalInt)
						throw new NafinException("CRED0002");*/
				}else if("R".equals(tipoAmort)){
					recortada = true;
					if(capitalInt||interes){
						campoError = 28 + i + 4;
						throw new NafinException("CRED0001");
					}

					System.out.println("LA FECHA MAXIMA PERMITIDA = "+dFecMax);
					System.out.println("LA FECHA DE LA AMORTIZACION = "+dFecAmort);
					System.out.println("LA COMPARACION = "+dFecMax.compareTo(dFecAmort));
					if(dFecMax.compareTo(dFecAmort)<0){
						System.out.println("INCREIBLEMENTE SI PASO POR AQUI");
						campoError = 28 + i + 3;
						throw new NafinException("CRED0005");
					}

				} else {
					campoError = 28 + i + 4;
					TA = true;
					return;
				}
				dFecAmortAnt 	= dFecAmort;
				totalImpAmort	= totalImpAmort.add(new BigDecimal(importeAmort));
				//ejecutamos el insert en las tablas temporales para cada amortizacion
				if(("R".equals(tipoAmort)&&importeAmort>0)||!"R".equals(tipoAmort)){

					tipoAmort = Comunes.strtr(tipoAmort,"\"","");
					fechaAmort = Comunes.strtr(fechaAmort,"\"","");

					ps.setLong(1, new Long(icSolicPortal).longValue());
					ps.setString(2,tipoAmort);
					ps.setDouble(3, new Double(importeAmort).doubleValue());
					ps.setString(4,fechaAmort);
					ps.setLong(5, new Long(icSolicPortal).longValue());
					ps.execute();
					ps.clearParameters();
				}
			} //for
			ps.close();
			if(j!=numAmort){
				campoError = 18;
				throw new NafinException("CRED0003");
			}

			double tmpTotImpAmort = (double)Math.round(totalImpAmort.doubleValue()*100)/100;

			System.out.println("TOTAL IMPORTE AMORTIZACIONES = "+tmpTotImpAmort);
			System.out.println("TOTAL IMPORTE DOCTOS = "+importeDocto);

			if(tmpTotImpAmort!=importeDocto){
				campoError = 5;
				throw new NafinException("CRED0007");
			}

			double tmpTotImpAmNoRec = (double)Math.round(totalImpAmNoRec.doubleValue()*100)/100;

			System.out.println("TOTAL IMPORTE AMORT NO REC = "+tmpTotImpAmNoRec);
			System.out.println("TOTAL IMPORTE DSCTO = "+importeDscto);

			if(tmpTotImpAmNoRec!=importeDscto){
				campoError = 6;
				throw new NafinException("CRED0008");
			}

			System.out.println(" El vector de amortizaciones = "+vecAmortizaciones);
		}catch(NafinException ne){

			throw ne;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Error Inesperado, AutorizacionSolicitudEJB.ovGenerarAmortizaciones(): "+ e.getMessage());
			throw new NafinException("SIST0001");
		}finally{
			System.out.println("CargaCreditoEJB.ovGenerarAmortizaciones (S)");
		}
	}

	private boolean esIfBloqueado(String claveIf, String claveProducto, AccesoDB con)
			throws NafinException {

		int ic_if = 0;
		int ic_producto_nafin = 0;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (claveIf == null || claveProducto == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			ic_if = Integer.parseInt(claveIf);
			ic_producto_nafin = Integer.parseInt(claveProducto);
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos. " + e.getMessage() +
					" claveIf=" + claveIf +
					" claveProducto=" + claveProducto);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************


		boolean bloqueado = false;
		String strSQL =
				" SELECT  " +
				" 	count(*) as registroBloqueado  " +
				" FROM " +
				" 	comrel_bloqueo_if_x_producto " +
				" WHERE " +
				" 	ic_if = ? " +
				" 	AND ic_producto_nafin = ? " +
				" 	AND cs_bloqueo = ? ";
		try {
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, ic_if);
			ps.setInt(2, ic_producto_nafin);
			ps.setString(3, "B");
			ResultSet rs = ps.executeQuery();
			rs.next();
			if (rs.getInt("registroBloqueado") > 0) {
				bloqueado = true;
			} else {
				bloqueado = false;
			}
			rs.close();
			ps.close();

			return bloqueado;

		} catch (Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}
	}


	public StringBuffer procesoSolicitudes(IFEnlOperados ifEnlOper) throws NafinException{
		
		System.out.println("CargaCreditoBean::procesoSolicitudes(E)************\n");
 		
		StringBuffer 		log 						= new StringBuffer();
		AccesoDB 			con 						= new AccesoDB();
		String 				qrySentencia 			= "";
		List					lVarBind 				= new ArrayList();
		boolean 				bOk 						= true;
		Acuse 				acuse 					= null;
		EnlAcuOperadosCE	acuseEnl					= new EnlAcuOperadosCE();
		int 					existe					= 0;
		
		int 					numRegistrosOperados 	= 0;
		int 					numRegistrosRechazados 	= 0;
		int 					totalRegistros				= 0;
		
		List					registros									= null;
		boolean				entroEnSeccionInsercionRegistros 	= false;				
		
		try {
			
			con.conexionDB();
			
			// 1.0 Validar si el proceso ya ha sido ejecutado el Dia de Hoy
			boolean			hayAcuseDoctosOperados = false;
			qrySentencia 	= ifEnlOper.getQueryBuscarAcuseOperados();
			lVarBind 		= new ArrayList();
			
			registros 		= con.consultaDB(qrySentencia, lVarBind, false);					
			registros 		= registros == null?new ArrayList():registros;

			if(registros.size() > 0){
				List  	renglon  		= (ArrayList) registros.get(0);
				hayAcuseDoctosOperados 	= "true".equals((String)renglon.get(0))?true:false;
			}
			
			if(!hayAcuseDoctosOperados){
				
				entroEnSeccionInsercionRegistros = true;
				
				// 1.1 Generar el Acuse correspondiente 	
				acuse 	= new Acuse(Acuse.ACUSE_IF, "1");
				acuseEnl.setCc_acuse(acuse.toString());
	 
				// 1.2 Insertar acuse generado
				qrySentencia 	= ifEnlOper.getInsertAcuse(acuseEnl);
				lVarBind 		= ifEnlOper.getVariablesBind();
				try {
					existe 			= con.ejecutaUpdateDB(qrySentencia, lVarBind);
				}catch(SQLException sqle){
					log.append("Error al Insertar acuse: "+qrySentencia+", lVarBind: "+lVarBind+".\n");
					throw sqle;
				}
				
				// 1.3 Insertar Registros operados
				qrySentencia = ifEnlOper.getInsertSolicitudesOperadas(acuseEnl);
				try {
					numRegistrosOperados = con.ejecutaUpdateDB(qrySentencia, new ArrayList());
				} catch (SQLException sqle){
					log.append("Error al Insertar(registros operados): "+qrySentencia+".\n");
					throw sqle;
				}
				
				// 1.4 Insertar registros rechazados
				qrySentencia = ifEnlOper.getInsertSolicitudesRechazadas(acuseEnl);
				try {
					numRegistrosRechazados = con.ejecutaUpdateDB(qrySentencia, new ArrayList());
				} catch (SQLException sqle){
					log.append("Error al Insertar(registros rechazados): "+qrySentencia+".\n");
					throw sqle;
				}
	 
				// 1.5 Actualizar total de registros insertados
				totalRegistros = numRegistrosOperados + numRegistrosRechazados;
				
				// 1.6 Actualizar tabla de acuse con el conteo de los registros
				acuseEnl.setIn_total_acep(numRegistrosOperados);
				acuseEnl.setIn_total_rech(numRegistrosRechazados);
				acuseEnl.setIn_total_proc(totalRegistros);
				acuseEnl.setIgEstatus(1); // Registro(s) procesado(s) 
				qrySentencia = ifEnlOper.getUpdateAcuse(acuseEnl);
				lVarBind     = ifEnlOper.getVariablesBind();
				
				System.out.println("acuseEnl:     "+acuseEnl.toString());
				System.out.println("qrySentencia: "+qrySentencia);
				System.out.println("lVarBind:     "+lVarBind);
	
				existe = con.ejecutaUpdateDB(qrySentencia, lVarBind);
				
				System.out.println("existe:"+existe);
				
			}else{
				// El proceso ya se ejecuto e inserto registros para el dia de hoy para el enlace correspondiente, por lo que se suspende su ejecucion para
				// evitar duplicidad de registros.
				log.append(
					" El Proceso de Solicitudes para el Enlace: "+ifEnlOper.getClass().getName()+", ya ha sido ejecutado y ha insertado registros,\n" +
					"por lo que se suspende su ejecucion, para evitar que se vuelvan a reinsertar los registros en la tabla de \n" +
					"acuse: "+ifEnlOper.getTablaAcuse()+" y en la tabla de acuse de resultados: "+ifEnlOper.getTablaAcuseResultados()+".\n");
			}	
			
		} catch(SQLException sqle) {
			
			bOk = false;
			log.append(sqle.getMessage());
			sqle.printStackTrace();
			throw new NafinException("SIST0001");
			
		} catch(Exception e) {
			
			bOk = false;
			log.append(e.getMessage());
			e.printStackTrace();
			throw new NafinException("SIST0001");
			
		} finally {
			
			if(entroEnSeccionInsercionRegistros) con.terminaTransaccion(bOk);
			
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			
			if(bOk)
				log.append("Proceso llevado a cabo con exito. \n");
			
			System.out.println(log.toString());
			System.out.println("CargaCreditoBean::procesoSolicitudes(S)************\n");
			
		}
		return log;

	}

	public List getPlazo(String clave_if){
		String 	qrySentencia 	= null;
		PreparedStatement ps		= null;
		ResultSet			rs		= null;
		AccesoDB con =new AccesoDB();
		String plazo = "";
		List resultado = new ArrayList();
		try{	
			con.conexionDB();
						
			qrySentencia = " SELECT distinct cs_tipo_plazo "+
							" FROM com_base_op_credito bo, comcat_tasa ta, comcat_tipo_credito tc " +
							" WHERE ic_if = ? "+
							" AND bo.ic_tasa = ta.ic_tasa "+
							" AND bo.ic_tipo_credito = tc.ic_tipo_credito "+
							" AND ta.cs_creditoelec = ? "+
							" AND tc.cs_creditoelec = ? "+
							" AND bo.cs_tipo_plazo IN (?,?)" +
							" ORDER BY 1 ";								
							
				ps = con.queryPrecompilado(qrySentencia);
				ps.setInt(1,Integer.parseInt(clave_if));
				ps.setString(2,"S");
				ps.setString(3,"S");
				ps.setString(4,"F");
				ps.setString(5,"C");
				rs = ps.executeQuery();
				while(rs.next()){
					plazo = rs.getString("cs_tipo_plazo")==null?"":rs.getString("cs_tipo_plazo");
					resultado.add(plazo);
				}
				rs.close();
				ps.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()) 
				con.cierraConexionDB();	
		}
		return resultado;
	}

	public Hashtable getPlazoBaseOperacion(String TipoPlazo, String iTipoCartera, String NoIF, String TasaI, String TipoCredito,
							String Amortizacion, String Emisor, String iPeriodicidad, String iTipoRenta){

		System.out.println("CargaCreditoEJB::getPlazoBaseOperacion (E)");
		AccesoDB con =new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int idx=0;
		Hashtable hPlazoBO = new Hashtable();
		StringBuffer query = new StringBuffer();
		String puedeUsarCodigoBaseOperacionDinamica	= "false";
		boolean plazoEncontrado = false;
		String FecVenDMax = "",	plazoMax = "";
		try{
		con.conexionDB();
		query.append(
			" SELECT " +
			" 	 TO_CHAR(SYSDATE+nvl(ig_plazo_maximo,0),'dd/mm/yyyy') AS fecVenDMax,        "  +
			" 	 nvl(ig_plazo_maximo,0)                               AS plazoMaximo,       "  +
			"	 decode(ig_plazo_maximo,null,'S','N')                 AS plazoMaximoEsNull, "  + 
			" 	 cs_base_op_dinamica 	"  +
			" FROM                     "  +
			"	 com_base_op_credito    "  +
			" WHERE                    "  +
			"	 ic_if            = ? " +
			"	 AND cs_tipo_plazo = ? " +
			"	 AND ic_tipo_credito  = ? ");
			if("0".equals(Emisor)){
				query.append("AND ic_emisor IS NULL ");
			}else{
				query.append("AND ic_emisor = ? ");
			}
		query.append(" AND ic_tasa = ? "+
				" AND ic_tabla_amort = ? " +
				" AND ig_tipo_cartera = ? " +
				(iPeriodicidad.equals("")?"  AND ic_periodicidad IS NULL ":" AND ic_periodicidad = ? ")+//Se agrega condicion por F014-2012 JSHD
				(iTipoRenta.equals("")?" AND cs_tipo_renta IS NULL ":" AND cs_tipo_renta = ? ")//Se agrega condicion por F014-2012 JSHD
		);
		query.append(" ORDER BY plazoMaximo DESC, ic_base_op_credito DESC ");

			try {
				System.out.println("El query es - - - - - - - - -     "+query.toString());
				ps = con.queryPrecompilado(query.toString());
				ps.setInt(1, Integer.parseInt(NoIF.trim()));
				ps.setString(2, TipoPlazo);
				ps.setInt(3, Integer.parseInt(TipoCredito.trim()));
				idx = 4;
				if(!Emisor.equals("0")) 	ps.setInt(idx++, 		Integer.parseInt(Emisor.trim()));
				ps.setInt(idx++, Integer.parseInt(TasaI.trim()));
				ps.setInt(idx++, Integer.parseInt(Amortizacion.trim()));
				ps.setInt(idx++, Integer.parseInt(iTipoCartera.trim()));
				if(!iPeriodicidad.equals("")) ps.setInt(idx++, 		Integer.parseInt(iPeriodicidad.trim()));
				if(!iTipoRenta.equals("")) 	ps.setString(idx++, 	iTipoRenta.trim());

				rs = ps.executeQuery();
				if(rs.next()){
					FecVenDMax 										= rs.getString("fecVenDMax").trim();
					plazoMax   										= rs.getString("plazoMaximo").trim();
					puedeUsarCodigoBaseOperacionDinamica 	= "S".equals(rs.getString("cs_base_op_dinamica"))?"true":"false";
	
					// Realizar ajustes a los valores leidos
					if("S".equals(rs.getString("plazoMaximoEsNull"))){
						FecVenDMax = "";
						plazoMax   = "";
					}
					plazoEncontrado 								= true;
				}else{
					FecVenDMax 										= "";
					plazoMax   										= "";
					puedeUsarCodigoBaseOperacionDinamica 	= "false";
					plazoEncontrado 								= false;
				}
				ps.clearParameters();
	
				if( !plazoEncontrado ){ // Si el plazo no fue encontrado

					rs.close();
					ps.close();

					query = new StringBuffer();

					query.append(
						" SELECT " +
						" 	 to_char(sysdate+nvl(ig_plazo_maximo,0),'dd/mm/yyyy') AS fecVenDMax,        "  +
						" 	 nvl(ig_plazo_maximo,0)                               AS plazoMaximo,       "  +
						"	 decode(ig_plazo_maximo,null,'S','N')                 AS plazoMaximoEsNull, "  + 
						" 	 cs_base_op_dinamica 						"  +
						" FROM                                    "  +
						"	 com_base_op_credito                   "  +
						" WHERE                                   "  +
						"	 ic_if            = ? " +
						"	 AND cs_tipo_plazo = ? " +
						"	 AND ic_tipo_credito  = ? ");
						if("0".equals(Emisor)){
							query.append("AND ic_emisor IS NULL ");
						}else{
							query.append("AND ic_emisor = ? ");
						}
					query.append(" AND ic_tasa = ? "+
							" AND ic_tabla_amort = ? " +
							" AND ig_tipo_cartera = ? " +
							" AND ic_periodicidad IS NULL "+//Se agrega condicion por F014-2012 JSHD
							" AND cs_tipo_renta   IS NULL ");//Se agrega condicion por F014-2012 JSHD
	
					query.append(" ORDER BY plazoMaximo DESC, ic_base_op_credito DESC ");
	
					ps = con.queryPrecompilado(query.toString());
					ps.setInt(1, Integer.parseInt(NoIF.trim()));
					ps.setString(2, TipoPlazo);
					ps.setInt(3, Integer.parseInt(TipoCredito.trim()));
					idx = 4;
					if(!Emisor.equals("0")) 	ps.setInt(idx++, 		Integer.parseInt(Emisor.trim()));
					ps.setInt(idx++, Integer.parseInt(TasaI.trim()));
					ps.setInt(idx++, Integer.parseInt(Amortizacion.trim()));
					ps.setInt(idx++, Integer.parseInt(iTipoCartera.trim()));
					rs = ps.executeQuery();
					if(rs.next()){

						FecVenDMax 										= rs.getString("fecVenDMax").trim();
						plazoMax   										= rs.getString("plazoMaximo").trim();
						puedeUsarCodigoBaseOperacionDinamica 	= "S".equals(rs.getString("cs_base_op_dinamica"))?"true":"false";

						// Realizar ajustes a los valores leidos
						if("S".equals(rs.getString("plazoMaximoEsNull"))){
							FecVenDMax = "";
							plazoMax   = "";
						}
						plazoEncontrado 								= true;

					} else {

						FecVenDMax 										= "";
						plazoMax   										= "";
						puedeUsarCodigoBaseOperacionDinamica 	= "false";
						plazoEncontrado 								= false;

					}
				}
				
			} catch(SQLException sqle) {
				throw new AppException("Error al obtener el plazo base de operaci�n", sqle);
			}
		} catch(AppException ne) {
			throw ne;
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception en getPlazoBaseOperacion(). "+e.getMessage());
		} finally {
			
			if( rs != null){ try { rs.close(); }catch(Exception e){} }
			if( ps != null){ try { ps.close(); }catch(Exception e){} }
			
			hPlazoBO.put("Valida", 											"S"								);
			hPlazoBO.put("FecVenDMax", 									FecVenDMax						);
			hPlazoBO.put("plazoMax", 										plazoMax		);
			hPlazoBO.put("puedeUsarCodigoBaseOperacionDinamica",	puedeUsarCodigoBaseOperacionDinamica 	);
			
			System.out.println("CargaCreditoEJB::getPlazoBaseOperacion (S)");
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}
		
		return hPlazoBO;
		
	}
 
}//CargaCreditoBean
