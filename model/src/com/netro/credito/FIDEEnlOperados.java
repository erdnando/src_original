package com.netro.credito;
import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class FIDEEnlOperados implements IFEnlOperados, Serializable  {

	private String ic_if 		= "";
	private List 	lVarBind 	= new ArrayList();

	
	public FIDEEnlOperados(String ic_if){
		this.ic_if = ic_if;
	}
	
	public String getInsertAcuse(EnlAcuOperadosCE acu){
		lVarBind = new ArrayList();
		String qrySentencia =
			" INSERT INTO "+getTablaAcuse()+" ("   +
			"              cc_acuse, in_reg_proc, in_reg_ok, in_reg_rech, ig_estatus, ig_tipo_acuse)"   +
			"      VALUES (?, ?, ?, ?, ?, ?)"  ;
		lVarBind.add(acu.getCc_acuse());
		lVarBind.add(new Integer(acu.getIn_total_proc()));
		lVarBind.add(new Integer(acu.getIn_total_acep()));
		lVarBind.add(new Integer(acu.getIn_total_rech()));
		lVarBind.add(new Integer(acu.getIgEstatus()));
		lVarBind.add(new Integer(acu.getIgTipoAcuse()));
		return qrySentencia;
	}

	public List getVariablesBind() {
		return this.lVarBind;
	}
	
	public String getTablaAcuse(){
		return "com_solic_pub_acu_fide";	
	}
	
	public String getTablaAcuseResultados(){
		return "com_solic_pub_acu_resul_fide";	
	}
	
	public String getTablaDocumentos(){
		return "com_solic_portal";	
	}
	
	public String getInsertSolicitudesOperadas(EnlAcuOperadosCE	acuseEnl){
		return 
			"  INSERT INTO "+getTablaAcuseResultados()+" ( cc_acuse, ig_clave_sirac, ig_numero_docto, ig_numero_prestamo, df_operacion, ic_estatus_solic, FN_IMPORTE_DSCTO) " +
			"  SELECT '"+acuseEnl.getCc_acuse()+"', ig_clave_sirac, ig_numero_docto, ig_numero_prestamo, df_operacion, ic_estatus_solic, FN_IMPORTE_DSCTO"   +
			"   FROM "+ getTablaDocumentos() + " " +
			"   WHERE df_operacion >= trunc(sysdate)"   +
			"  AND df_operacion < trunc(sysdate+1)"   +
			"  AND ic_estatus_solic in (3)" +
			"  AND ic_if = "+ic_if;
		
	}
	
	public String getInsertSolicitudesRechazadas(EnlAcuOperadosCE	acuseEnl){
		return 
			"  INSERT INTO "+getTablaAcuseResultados()+" ( cc_acuse, ig_clave_sirac, ig_numero_docto, df_operacion, ic_estatus_solic,  cc_error) " +
			"  SELECT '"+acuseEnl.getCc_acuse()+"', ig_clave_sirac, ig_numero_docto, df_operacion, ic_estatus_solic, cc_codigo_aplicacion || ic_error_proceso " +
			"   FROM "+ getTablaDocumentos() + " " +
			"   WHERE df_operacion >= trunc(sysdate)"   +
			"  AND df_operacion < trunc(sysdate+1)"   +
			"  AND ic_estatus_solic in (4)" +
			"  AND ic_if = "+ic_if;
	}
	
	public String getUpdateAcuse(EnlAcuOperadosCE acu){
		lVarBind = new ArrayList();
		String qrySentencia =
			" UPDATE "+getTablaAcuse()+"  "  +
			"    SET in_reg_proc = ?,"   +
			"        in_reg_ok = ?,"   +
			"        in_reg_rech = ?, "  +
			"        ig_estatus  = ?  "  +
			"  WHERE cc_acuse = ?"  ;
		lVarBind.add(new Integer(acu.getIn_total_proc()));
		lVarBind.add(new Integer(acu.getIn_total_acep()));
		lVarBind.add(new Integer(acu.getIn_total_rech()));
		lVarBind.add(new Integer(acu.getIgEstatus()));
		lVarBind.add(acu.getCc_acuse());
		return qrySentencia;
	}
 
	public String getQueryBuscarAcuseOperados(){
		
		String qrySentencia =
			"SELECT "  + 
			"  DECODE(COUNT(1),0,'false','true') AS HAY_ACUSE "  +
			"FROM "  + 
			"  "+getTablaAcuse()+" "  +
			"WHERE "  + 
			"  DF_CARGA          >=  TRUNC(sysdate) "  +
			"  AND DF_CARGA      <   TRUNC(sysdate+1) "  +
			"  AND IG_TIPO_ACUSE =   2"; // Acuse del Proceso de Documentos Operados
		return qrySentencia;
		
	}
}
