package com.netro.credito;

import java.io.*;
import java.util.*;


public class FIDECargaCE implements EnlInterCargaCE, Serializable {

	private List 	lVarBind 		= new ArrayList();
	private String	lProceso = "1";  // 1 - Proceso Disposiciones - 4 - Registro del Subsidio

	public FIDECargaCE() { }

	public FIDECargaCE(String ic_proceso) { 
		this.lProceso = ic_proceso;
	}

	public String getTablaEncabezadoDocumentos() {
		return "comtmp_alta_enc_proc_fide";
	}
	
	public String getProcesoDepuracion(){
		return "SP_DEPURA_PUB_FIDE";
	}
	
	public String getTablaDocumentos() {
		return "comtmp_solic_fide";
	}

	public String getTablaAmort() {
		// return "comtmp_solic_pagos_fide";
		return "comtmp_solic_amort_fide";
	}

	public String getTablaAcuse() {
		return "com_solic_pub_acu_fide";
	}

	public String getTablaErrores() {
		return "com_solic_pub_acu_resul_fide";
	}

	public String getDocuments(){
		String qrySentencia =
/*01*/		" SELECT ig_clave_sirac,"   +
/*02*/		"        ig_sucursal,"   +
/*03*/		"        ic_moneda,"   +
/*04*/		"        ig_numero_docto,"   +
/*05*/		"        fn_importe_docto,"   +
/*06*/		"        fn_importe_dscto,"   +
/*07*/		"        ic_emisor,"   +
/*08*/		"        ic_tipo_credito,"   +
/*09*/		"        ic_periodicidad_c,"   +
/*10*/		"        ic_periodicidad_i,"   +
/*11*/		"        cg_bienes_servicios,"   +
/*12*/		"        ic_clase_docto,"   +
/*13*/		"        cg_domicilio_pago,"   +
/*14*/		"        ic_tasauf,"   +
/*15*/		"        cg_rmuf,"   +
/*16*/		"        fg_stuf,"   +
/*17*/		"        ic_tasaif,"   +
/*18*/		"        in_numero_amort,"   +
/*19*/		"        ic_tabla_amort,"   +
/*20*/		"        TO_CHAR (df_ppc, 'dd/mm/yyyy'),"   +
/*21*/		"        TO_CHAR (df_ppi, 'dd/mm/yyyy'),"   +
/*22*/		"        in_dia_pago,"   +
/*23*/		"        TO_CHAR (df_v_documento, 'dd/mm/yyyy'),"   +
/*24*/		"        TO_CHAR (df_v_descuento, 'dd/mm/yyyy'),"   +
/*25*/		"        TO_CHAR (df_etc, 'dd/mm/yyyy'),"   +
/*26*/		"        cg_lugar_firma,"   +
/*27*/		"        cs_amort_ajuste,"   +
/*28*/		"        fg_importe_ajuste, "   +
/*29*/		"        cs_tipo_renta,"   +
/*30*/		"        cs_tipo_operacion, "   +
/*31*/		"        ig_base_operacion "   +
			   "   FROM "+getTablaDocumentos()+" " +
				"	 WHERE        "  +
				"	 	ic_enc = ? ";

		return  qrySentencia;
	}

	public String getInsertaErrores(EnlErrCargaCE err) {
		lVarBind = new ArrayList();
		String qrySentencia =
			" INSERT INTO com_solic_pub_acu_resul_fide("   +
			"              cc_acuse, ig_clave_sirac, ig_numero_docto, cc_error, ic_estatus_solic)"   +
			"      VALUES (?, ?, ?, ?, ?)"  ;
		lVarBind.add(err.getCc_acuse());
		lVarBind.add(new Integer(err.getIg_clave_sirac()));
		lVarBind.add(new Long(err.getIg_numero_docto()));
		lVarBind.add(err.getCc_error());
		lVarBind.add(err.getIcEstatusSolic());
		return qrySentencia;
	}

	public String getUpdateAcuse(EnlAcuCargaCE acu){
		lVarBind = new ArrayList();
		String qrySentencia =
			" UPDATE com_solic_pub_acu_fide"   +
			"    SET in_reg_proc = ?,"   +
			"        in_reg_ok = ?,"   +
			"        in_reg_rech = ?, "  +
			"        ig_estatus  = ?  "  +
			"  WHERE cc_acuse = ?"  ;
		lVarBind.add(new Integer(acu.getIn_total_proc()));
		lVarBind.add(new Integer(acu.getIn_total_acep()));
		lVarBind.add(new Integer(acu.getIn_total_rech()));
		lVarBind.add(new Integer(acu.getIgEstatus()));
		lVarBind.add(acu.getCc_acuse());
		return qrySentencia;
	}

	public String getInsertAcuse(EnlAcuCargaCE acu){
		lVarBind = new ArrayList();
		String qrySentencia =
			" INSERT INTO com_solic_pub_acu_fide ("   +
			"              cc_acuse, in_reg_proc, in_reg_ok, in_reg_rech, ig_estatus, ig_tipo_acuse)"   +
			"      VALUES (?, ?, ?, ?, ?, ?)"  ;
		lVarBind.add(acu.getCc_acuse());
		lVarBind.add(new Integer(acu.getIn_total_proc()));
		lVarBind.add(new Integer(acu.getIn_total_acep()));
		lVarBind.add(new Integer(acu.getIn_total_rech()));
		lVarBind.add(new Integer(acu.getIgEstatus()));
		lVarBind.add(new Integer(acu.getIgTipoAcuse()));
		return qrySentencia;
	}

	public String getCondicionQuery(){
		return "";
	}
	

	//SETS


	//GETS
	
	public List getVariablesBind() {
		return this.lVarBind;
	}

	public String getProceso() {
		return this.lProceso;
	}



}//FIDECargaCE
