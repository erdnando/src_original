package com.netro.credito;

import java.util.List;

public interface IFEnlOperados extends java.io.Serializable {
		
	public String getInsertAcuse(EnlAcuOperadosCE acu);
	public List   getVariablesBind();
	public String getTablaAcuse();
	public String getTablaAcuseResultados();
	public String getTablaDocumentos();
	public String getInsertSolicitudesOperadas(EnlAcuOperadosCE	acuseEnl);
	public String getInsertSolicitudesRechazadas(EnlAcuOperadosCE	acuseEnl);
	public String getUpdateAcuse(EnlAcuOperadosCE acu);
	public String getQueryBuscarAcuseOperados();

}
