package com.netro.credito;

import com.netro.exception.NafinException;

import java.util.Hashtable;
import java.util.List;

import javax.ejb.Remote;

@Remote
public interface CargaCredito{

    public void procesaCargaMasivaCE(EnlInterCargaCE enlace, String NoIf) throws NafinException;

    public StringBuffer procesoSolicitudes(IFEnlOperados ifEnlOper) throws NafinException;

    public List getPlazo(String clave_if);

    public Hashtable getPlazoBaseOperacion(
	    String TipoPlazo,
	    String iTipoCartera,
	    String NoIF,
	    String TasaI,
	    String TipoCredito,
	    String Amortizacion,
	    String Emisor,
	    String iPeriodicidad,
	    String iTipoRenta
    );
}

