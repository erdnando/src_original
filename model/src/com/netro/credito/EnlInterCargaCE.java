package com.netro.credito;

import java.util.List;

public interface EnlInterCargaCE extends java.io.Serializable {

	public abstract String getTablaEncabezadoDocumentos();
	
	public abstract String getProcesoDepuracion();
	
	public abstract String getTablaDocumentos();

	public abstract String getTablaAmort();

	public abstract String getTablaAcuse();

	public abstract String getTablaErrores();

	public abstract String getDocuments();

	public abstract String getInsertaErrores(EnlErrCargaCE err);

	public abstract String getUpdateAcuse(EnlAcuCargaCE acu);

	public abstract String getInsertAcuse(EnlAcuCargaCE acu);

	public abstract String getCondicionQuery();

	public abstract List getVariablesBind();

	public abstract String getProceso();

}//EnlInterCargaCE
