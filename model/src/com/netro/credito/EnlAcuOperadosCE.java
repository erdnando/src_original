package com.netro.credito;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class EnlAcuOperadosCE {

	private String	cc_acuse					= "";
	private int 	in_total_proc			= 0;
	private int 	in_total_acep			= 0;
	private int 	in_total_rech			= 0;
	private int 	igEstatus				= -1;

	public static final int TIPO_ACUSE	= 2; // Acuse del Proceso de Documentos Operados
	
	public void add(boolean aceptado) {
		in_total_proc++;
		if(aceptado)
			in_total_acep++;
		else
			in_total_rech++;
	}

	public void setCc_acuse(String cc_acuse)	{
		this.cc_acuse = cc_acuse;
	}
	
	public String getCc_acuse(){
		return this.cc_acuse;
	}
	
	public int 	getIn_total_proc(){
		return this.in_total_proc;
	}
	
	public int 	getIn_total_acep(){
		return this.in_total_acep;
	}
	
	public int 	getIn_total_rech(){
		return this.in_total_rech;
	}

	
	
	public String toString(){
		String strAcuse =
			"\n[\nAcuse     = "+cc_acuse+
			",\nTotal Proc  = "+in_total_proc+
			",\nTotal Acep  = "+in_total_acep+
			",\nTotal Rech  = "+in_total_rech+
			",\nigEstatus   = "+igEstatus+
			",\nigTipoAcuse = "+TIPO_ACUSE+
			"\n]";
		return strAcuse;
	}

	public int getIgEstatus(){
		return this.igEstatus;
	}
	
	public int getIgTipoAcuse(){
		return TIPO_ACUSE;
	}
	
	public void setIgEstatus(int igEstatus){
		this.igEstatus = igEstatus;
	}
	
	public void setIn_total_proc(int in_total_proc){
		this.in_total_proc = in_total_proc;
	}
	
	public void setIn_total_acep(int in_total_acep){
		this.in_total_acep = in_total_acep;
	}
	
	public void setIn_total_rech(int in_total_rech){
		this.in_total_rech = in_total_rech;
	}
	
}
