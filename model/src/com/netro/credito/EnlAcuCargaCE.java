package com.netro.credito;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class EnlAcuCargaCE {

	private String	cc_acuse					= "";
	private int 	in_total_proc			= 0;
	private int 	in_total_acep			= 0;
	private int 	in_total_rech			= 0;
	private int 	igEstatus				= -1;
	
	public static final int TIPO_ACUSE	= 1;// Acuse del Proceso de Carga

	public void add(boolean aceptado) {
		in_total_proc++;
		if(aceptado)
			in_total_acep++;
		else
			in_total_rech++;
	}

	public void setCc_acuse(String cc_acuse)	{
		this.cc_acuse = cc_acuse;
	}
	
	public String getCc_acuse(){
		return this.cc_acuse;
	}
	
	public int 	getIn_total_proc(){
		return this.in_total_proc;
	}
	
	public int 	getIn_total_acep(){
		return this.in_total_acep;
	}
	
	public int 	getIn_total_rech(){
		return this.in_total_rech;
	}

	public String toString(){
		String strAcuse =
			"\n[\nAcuse     = "+cc_acuse+
			",\nTotal Proc  = "+in_total_proc+
			",\nTotal Acep  = "+in_total_acep+
			",\nTotal Rech  = "+in_total_rech+
			",\nigEstatus   = "+igEstatus+
			",\nigTipoAcuse = "+TIPO_ACUSE+
			"\n]";
		return strAcuse;
	}

	public int getIgEstatus(){
		return this.igEstatus;
	}
	
	public void setIgEstatus(int igEstatus){
		this.igEstatus = igEstatus;
	}
	
	public int getIgTipoAcuse(){
		return TIPO_ACUSE;
	}
		
}
