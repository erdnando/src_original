package com.netro.credito;

import java.util.*;

public interface EnlTorSubsidioCargaCE {

	public abstract String getTablaDocumentos();

	public abstract String getTablaAmort();

	public abstract String getTablaAcuse();

	public abstract String getTablaErrores();

	public abstract String getDocuments();

	public abstract String getInsertaErrores(EnlErrCargaCE err);

	public abstract String getUpdateAcuse(EnlAcuCargaCE acu);

	public abstract String getInsertAcuse(EnlAcuCargaCE acu);

	public abstract String getCondicionQuery();

	public abstract List getVariablesBind();
	
	public abstract String getInsertaSolicitudes();
	
	public abstract String getIc_if();

}//EnlTorSubsidioCargaCE
