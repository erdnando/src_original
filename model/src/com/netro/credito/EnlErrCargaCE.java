package com.netro.credito;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.math.*;

public class EnlErrCargaCE {
	
	private String cc_acuse 			= "";
	private String ig_clave_sirac 		= "";	
	private String ig_numero_docto 		= "";	
	private	String ig_solic_portal 		= "";
	private String cc_error 			= "";
	private String icEstatusSolic		= "";
		
	//GETS
	
	public String getCc_acuse(){
		return cc_acuse;
	}
	
	public String getIg_clave_sirac(){
		return ig_clave_sirac;
	}
	
	public String getIg_numero_docto(){
		return ig_numero_docto;
	}
	
	public String getIg_solic_portal(){
		return ig_solic_portal;
	}
	
	public String getCc_error(){
		return cc_error;
	}
	
	public String getIcEstatusSolic(){
		return icEstatusSolic;
	}

	//SETS
	
	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}
	
	public void setIg_clave_sirac(String ig_clave_sirac) {
		this.ig_clave_sirac = ig_clave_sirac;
	}
	
	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}
	
	public void setIg_solic_portal(String ig_solic_portal) {
		this.ig_solic_portal = ig_solic_portal;
	}
	
	public void setCc_error(String cc_error) {
		this.cc_error = cc_error;
	}

	public void setIcEstatusSolic(String icEstatusSolic){
		this.icEstatusSolic = icEstatusSolic;
	}
	
}//EnlErrCargaCE
