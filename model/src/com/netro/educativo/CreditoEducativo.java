package com.netro.educativo;

import netropology.utilerias.AppException;
import java.util.*;  
import java.math.*;
import netropology.utilerias.*;
import com.netro.exception.*;
import javax.ejb.Remote;

@Remote
public interface CreditoEducativo {

	public ResultadosGarEducativo procesarAltaGarantiaEducativo(String strDirectorio,String esNombreArchivo,String num_registros,String sum_montos,String ic_if);
	public ResultadosGarEducativo procesarAltaCreditoEducativo(
			String icProceso,
			String ic_if,
			String num_registros,
			String impte_total,
			String claveUniversidad,
			String ic_usuario,
			String strDirectorio
	);
	
	public String paramIF_Univ(String ic_if , String universidades[] ) ; 
	public String paramUniv_IF(String ic_if [] , String universidad ) ; 
	
	public String existeBO(String  ic_operacion); 
	public String agregarBO(List parametros);	
	public List consBaseOperacion(String  ic_operacion, String icUniversidad); 
	public String  cambioEstatus(String ic_base [], String estatus[]); 
	public String ModificarBO(List parametros);
	public Registros getTotalesResultados(String intermediario,String universidad, String grupo, 
	String estatus,String folio, String fechaValidaIni, String fechaValidaFin,String fechaOperaIni, String fechaOperaFin);
	public Registros getTotalesResultadosEstudiantes(String estatusReg,String folio);
	
	public Registros getFoliosConfirmar(String claveUniversidad, String fechaIni, String fechaFin, String folioOpera, String claveIf);
	public Registros getDatosCarga(String folio_carga);
	public boolean eliminaTemporalesCarga(String icProceso);
	public void setValidaAportaciones(String[] folios,String[] aceptados,String[] rechazados,
												String[] montoGaran, String[] causaRechazo,String fechaValidacion);

	public void setValidaGarantias(String folio,	String estatus, String causaRechazo,
								String fechaDeposito, String importe, String Banco,	String referencia);

	public String  transEducativoaGarantia(List parametros, String regAceptados[],  String regRechazados[], String causaRechazo[], String acuse, String no_disposicion )  ;       
	public String  baseOperacionIF(String ic_if );
	public void inacRelIF_Univ( String ic_if , String universidad, String estatus );
	public Registros getBasesOperaIf(String claveIf);
	public boolean envioCorreoUniversidad(String strDirectorio, List llaves);

}