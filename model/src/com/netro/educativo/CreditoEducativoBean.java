package com.netro.educativo;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

@Stateless(name = "CreditoEducativoEJB" , mappedName = "CreditoEducativoEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class CreditoEducativoBean implements CreditoEducativo {

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(CreditoEducativoBean.class);

	public static final int GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH = 2000;
	public static final int CLAVE_DEL_PROGRAMA_LENGTH 					= 5;
	/**
	 * Lee el archivo especificado, el cual debe cumplir con el layout de
	 * carga de Alta de Credito Educativo.
	 * @param strDirectorio Directorio donde se encuentra el archivo a procesar
	 * @param esNombreArchivo Nombre del archivo a procesar, puede ser
	 * 	un archivo de texto(.txt) o un archivo comprimido(.zip)
	 * @param num_registros Numero de registros contenidos en el archivo a procesar
	 * @param sum_montos Sumatoria del monto de los registros del archivo a procesar
	 * @param ic_if Clave del intermediario financiero
	 * @return Objeto ResultadosGarEducativo con el resultado del procesamiento.
	 * @throws com.netro.exception.AppException
	 */
	public ResultadosGarEducativo procesarAltaGarantiaEducativo(String strDirectorio,	String esNombreArchivo,String num_registros,	String sum_montos,String ic_if){
		log.info("CreditoEducativoBean::procesarAltaGarantiaEducativo(E)");
		ResultadosGarEducativo retorno = new ResultadosGarEducativo(ResultadosGarEducativo.CARGA_MASIVA);
		AccesoDB con			= new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet		  rs 	= null;
		String qrySentencia	= "";

		try{

			int ctrl_num_registros		= Integer.parseInt(num_registros);
			BigDecimal ctrl_sum_montos	= new BigDecimal(sum_montos.replaceAll(",", ""));
			String extension				= esNombreArchivo.substring(esNombreArchivo.length()-3,esNombreArchivo.length());

			if("zip".equalsIgnoreCase(extension)){
				try{
					esNombreArchivo	= descomprimirArchivo(strDirectorio,esNombreArchivo);
					extension			= esNombreArchivo.substring(esNombreArchivo.length()-3,esNombreArchivo.length());
					if(!"txt".equalsIgnoreCase(extension)){
						retorno.appendErrores("Error: El archivo comprimido no es txt\n");
					}
				}catch(Exception e){
					retorno.appendErrores("Error: El archivo comprimido no es txt\n");
				}
			}

			java.io.File lofArchivo = new java.io.File(esNombreArchivo);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo),"ISO-8859-1"));
			VectorTokenizer vt = null;
			Vector lovDatos = null;
			String lsLinea = "";
			int ifSiag  = 0;
			int lineas = 0;
			int numLinea = 1;
			int ctrl_linea = 0;
			int iNumRegTrans = 0;
			BigDecimal dbl_sum_montos = new BigDecimal(0.00);

			con.conexionDB();

			qrySentencia =
				"SELECT ic_if_siag FROM comcat_if"+
				" WHERE ic_if = ?";
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,ic_if);
			rs = ps.executeQuery();
			if(rs.next()){
				ifSiag = rs.getInt("ic_if_siag");
			}
			rs.close();
			ps.close();

			retorno.setIcProceso(calculaIcProceso());
			qrySentencia =
				"INSERT INTO cetmp_contenido_arc"+
				"(ic_proceso,	ic_linea,	cg_contenido)"+
				"VALUES(?,?,?)";
			ps = con.queryPrecompilado(qrySentencia);

			if(retorno.getErrores().length()==0){
				for (lineas = 0,numLinea = 1; (lsLinea = br.readLine()) != null;	lineas++,	numLinea++) {
					BigDecimal tmpMonto = new BigDecimal(0);
					retorno.setErrorLinea(false);
					lsLinea = lsLinea.trim();
					if (lsLinea.length()==0){
						continue;
					}else if( lsLinea.length() >= GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH){
						lsLinea = lsLinea.substring(0,GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH);
						ctrl_linea++;
					}else{
						ctrl_linea++;
					}
					vt = new VectorTokenizer(lsLinea,"@");
					lovDatos = vt.getValuesVector();

					this.validarAltaGarantias(ifSiag, numLinea, lsLinea,	ic_if, retorno);
					try {
						tmpMonto = new BigDecimal(((String)lovDatos.get(23)).trim());
					} catch(Throwable e) {
						tmpMonto = new BigDecimal(0);
					}

					if(!retorno.getErrorLinea()){
						if(lineas==0){
							retorno.appendCorrectos("Claves de financiamiento:\n");
						}
						//retorno.appendCorrectos("Linea:"+numLinea+" Monto del Financiamiento="+Comunes.formatoDecimal((String)lovDatos.get(23),2)+" Acreditado="+lovDatos.get(0)+".\n");
						retorno.appendCorrectos(""+(String)lovDatos.get(9)+"\n");
						retorno.incNumRegOk();
						retorno.addSumRegOk(tmpMonto.doubleValue());

						//Si no hay errores insertar en tabla temporal: GTITMP_CONTENIDO_ARCH
						if(retorno.getErrores().length()==0 && retorno.getCifras().length()==0){
							ps.setInt(1,retorno.getIcProceso());
							ps.setInt(2,numLinea);
							ps.setString(3,lsLinea);
							ps.execute();
						}

					}else{
						retorno.incNumRegErr();
						retorno.addSumRegErr(tmpMonto.doubleValue());
					}
					dbl_sum_montos = dbl_sum_montos.add(tmpMonto);
					iNumRegTrans++;
				} // for
			}
			ps.close();
			if(ctrl_linea!=ctrl_num_registros)
				retorno.appendCifras("El numero de registros capturado no corresponde al contenido en el archivo.\n");
			if(dbl_sum_montos.doubleValue()!=ctrl_sum_montos.doubleValue()){
				log.debug("La sumatoria de los montos = "+dbl_sum_montos.doubleValue());
				log.debug("La cifra de control capturada = "+ctrl_sum_montos.doubleValue());
				retorno.appendCifras("La sumatoria de los montos no corresponde al contenido en el archivo.\n");
			}

		}catch(Exception e){
			log.error("CreditoEducativoBean::procesarAltaGarantiaEducativo(Exception)");
			e.printStackTrace();
			throw new AppException("Error al procesar el archivo");
		}finally{
			if(con.hayConexionAbierta()){
				if(retorno.getErrores().length() > 0||retorno.getCifras().length()>0)
					con.terminaTransaccion(false);
				else
					con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			log.info("CreditoEducativoBean::procesarAltaGarantiaEducativo(S)");
		}
		return retorno;
	}

	/**
	 * Lee el archivo especificado, el cual debe cumplir con el layout de
	 * carga de Alta de Credito Educativo.
	 * @param strDirectorio Directorio donde se encuentra el archivo a procesar
	 * @param esNombreArchivo Nombre del archivo a procesar, puede ser
	 * 	un archivo de texto(.txt) o un archivo comprimido(.zip)
	 * @param num_registros Numero de registros contenidos en el archivo a procesar
	 * @param sum_montos Sumatoria del monto de los registros del archivo a procesar
	 * @param ic_if Clave del intermediario financiero
	 * @return Objeto ResultadosGarEducativo con el resultado del procesamiento.
	 * @throws com.netro.exception.AppException
	 */
	public ResultadosGarEducativo procesarAltaCreditoEducativo(
			String icProceso,
			String ic_if,
			String num_registros,
			String impte_total,
			String claveUniversidad,
			String ic_usuario,
			String strDirectorio
	){
		log.info("CreditoEducativoBean::procesarAltaCreditoEducativo(E)");

		ResultadosGarEducativo retorno = new ResultadosGarEducativo();
		String _fechaMsg = (new SimpleDateFormat ("yyyyMMdd_HHmmss")).format(new java.util.Date());

		AccesoDB          con    = new AccesoDB();
		PreparedStatement ps     = null;
		PreparedStatement ps1    = null;
		PreparedStatement ps2    = null;
		ResultSet         rs     = null;
		ResultSet         rs1    = null;
		ResultSet         rs2    = null;
		StringBuffer      query  = new StringBuffer(256);
		StringBuffer      query2 = new StringBuffer(256);
		StringBuffer      query3 = new StringBuffer(256);
		String            query4 = "";
		List         listaLlaves = new ArrayList();
		boolean           ok     = true;
		String            icUniv = "";

		try{
			con.conexionDB();

			BigDecimal folio						= null;
			BigDecimal ic_nafin_electronico	= null;
			Integer ic_if_siag					= null;

			//int consecutivo				= 0;
			//int ic_if_siag				= 0;
			//int ic_nafin_electronico	= 0;
			String fecha				= "";
			String hora					= "";

			String anio 				= "";
			String trim_calif			= "";
			int ic_contragarante		= 0;

			Registros reg = this.getDatosFolio(ic_if);

			if (reg.next()) {

				folio						= new BigDecimal(reg.getString("folio"));
				ic_nafin_electronico	= new BigDecimal(reg.getString("ic_nafin_electronico"));

				try {
					ic_if_siag = new Integer(reg.getString("ic_if_siag"));
				} catch (Exception e ) {
					ic_if_siag = new Integer(0);
				}
				fecha			= reg.getString("fecha");
				hora			= reg.getString("hora");
				anio			= reg.getString("anio");
				trim_calif	= reg.getString("trimestre");
			}

			ic_contragarante = this.getClaveContragarante(ic_if);//FODEA 002 - 2011 ACF

			query3.append(
				"INSERT INTO											"+
				"	CE_CONTENIDO_ARC									"+
				"	(														"+
				"	IC_FOLIO_CARGA,									"+
				"	IC_REGISTRO,										"+
				"  IC_IF,												"+
				"	CG_CONTENIDOE,										"+
				"	CG_CONTENIDOG,										"+
				"	IC_IF_SIAG,											"+
				"	IG_PROGRAMA											"+
				"	)	 													"+
				"	VALUES 												"+
				"	( 														"+
				"	  ?,?,?,?,?,?,?									"+
				"	) 														"
			);

			query2.append(
				"INSERT INTO 											"  +
				"	CE_CONTENIDO_EDUCATIVO 							"  +
				"	(	 													"  +
				"  IC_REGISTRO,										"  +
				"  CG_RAZON_SOCIAL,   								"  +
				"  CG_REGISTRO,   									"  +
				"  CG_CALLE,   										"  +
				"  CG_COLONIA,   										"  +
				"  CG_CODIGOPOSTAL,   								"  +
				"  CG_CIUDAD,   										"  +
				"  IC_MUNICIPIO,   									"  +
				"  IC_ESTADO,   										"  +
				"  CG_GIRO,   											"  +
				"  CG_FINANCIAMIENTO,   							"  +
				"  CG_SECTOR,   										"  +
				"  CG_FONDEO,   										"  +
				"  CG_RIESGO,   										"  +
				"  CG_PORCENTAJE_REG,   							"  +
				"  IC_PERSONAL,   									"  +
				"  FN_VENTAS_ANUAL,   								"  +
				"  CG_PROPOSITO,   									"  +
				"  IC_TIPO_FINANCIAMIENTO,   						"  +
				"  CC_CLAVE_MONTO_GARANTIA, 						"  +
				"  CC_RECURSOS,   									"  +
				"  FN_PORC_MER_INTERNO,   							"  +
				"  FN_PORC_MER_EXTERNO,   							"  +
				"  IC_AMORTIZACION,   								"  +
				"  FN_FINANCIAMIENTO,   							"  +
				"  DF_DISPOSICION_RIES,   							"  +
				"  FN_IMP_DISPOSICION,   							"  +
				"  CG_MONEDA,   										"  +
				"  CG_MESES_PLAZO,   								"  +
				"  CG_MESES_FINANCIAMIENTO,   					"  +
				"  CG_SOBRETASA,   									"  +
				"  IC_FUNCIONARIO_IF,   							"  +
				"  IC_IF_SIAG,   										"  +
				"  IC_AUTORIZACION,   								"  +
				"  IC_PROGRAMA,   									"  +
				"  IC_TASA,   											"  +
				"  CG_PLAZO_DIAS,   									"  +
				"  CG_CALIFICACION,   								"  +
				"  CG_ANTI_CLI,   									"  +
				"  CG_NOMCONTRATO,   								"  +
				"  CG_TELEFONO,   									"  +
				"  CG_CORREO,   										"  +
				"  CG_DEPENDENCIA,   								"  +
				"  CG_MATRICULA,   									"  +
				"  CG_DESC_ESTUDIOS,   								"  +
				"  DF_INGRESO,   										"  +
				"  IC_PERIODO,   										"  +
				"  CG_ANIO,   											"  +
				"  CG_MODALIDAD,   									"  +
				"  DF_DISPOSICION,   								"  +
				"  IC_NUM_DISPOSICION,   							"  +
				"  FN_SALDOLINEAS,   								"  +
				"  IN_REGISTROS,   									"  +
				"  IC_FOLIO_CARGA,   								"  +
				"  IC_ESTATUS_CRED_EDUCA,   						"  +
				"  IC_UNIVERSIDAD,   								"  +
				"  IC_IF ,													"  +	//61 campos
				"  CG_TIPOPERSONA , 										"  +	//F024-2013
				"  CG_SEXOACREDITADO								      "  +	//F024-2013
				"		) 													"  +
				"	VALUES 												"  +
				"	( 														"  +
				"		?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?"+
				"	  ,?,?,?,?,?,to_date(?,'yyyymmdd'),?,?,?,?,?,?,?,?,?,?,?,?,?,?"+
				"	  ,?,?,?,?,?,to_date(?,'ddmmyyyy'),?,?,?,to_date(?,'ddmmyyyy'),?,?,?,?"+
				"	  ,?,?,?  , ? , ? 	) 	"	);

			query		= new StringBuffer(256);
			query.append(
				"INSERT INTO											"+
				"	CE_ACUSE_EDUCATIVO								"+
				"	(														"+
				"	IC_FOLIO_CARGA,									"+
				"	IN_REGISTROS_ACEP,								"+
				"  IN_REGISTROS,										"+
				"	FN_MONTO_TOTAL,									"+
				"	IC_USUARIO,											"+
				"	CG_TIPO_OPERACION									"+
				"	)	 													"+
				"	VALUES 												"+
				"	( 														"+
				"	  ?,?,?,?,?,?										"+
				"	) 														"
			);
			String cg_tipo_operacion = "CREDITO EDUCATIVO";
			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setBigDecimal(1,	folio									);	//IC_FOLIO_CARGA
			ps.setBigDecimal(2,	getBigDecimal(num_registros)	);	//IN_REGISTROS_ACEP
			ps.setBigDecimal(3,	getBigDecimal(num_registros)	);	//IN_REGISTROS
			ps.setBigDecimal(4,	getBigDecimal(impte_total)		);	//FN_MONTO_TOTAL
			ps.setString(5,		ic_usuario							);	//IC_USUARIO
			ps.setString(6,		cg_tipo_operacion					);	//CG_TIPO_OPERACION
			int numRegistrosInsertadosAcuse = ps.executeUpdate();
			ps.close();

			// Imprmir mensaje de Depuracion
			log.debug("procesarAltaCreditoEducativo(" + _fechaMsg + "_" + icProceso + ")::Inserciones ce_acuse_educativo ...OK("+numRegistrosInsertadosAcuse+")");

			query		= new StringBuffer(256);
			query.append(
				" SELECT cg_contenido FROM cetmp_contenido_arc"+
				" WHERE ic_proceso = ? "+
				" ORDER BY ic_linea "
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, Integer.parseInt(icProceso));
			rs = ps.executeQuery();

			VectorTokenizer vt = null;
			Vector layout = null;
			String estatus = "1";

			String seqNEXTVAL	= "0";

			while (rs.next()) {
				String cg_contenido = rs.getString("cg_contenido")==null?"":rs.getString("cg_contenido");
				vt = new VectorTokenizer(cg_contenido,"@");
				layout = vt.getValuesVector();

				ps1 = con.queryPrecompilado("SELECT seq_ce_contenido_educativo.NEXTVAL FROM dual");
				rs1 = ps1.executeQuery();
				if(rs1.next()){
					seqNEXTVAL = rs1.getString(1)==null?"1":rs1.getString(1);
				}
				ps1.close();
				rs1.close();

				/***** Obtengo el IC_UNIVERSIDAD para utilizarlo en el insert *****/

				query4 = " SELECT IC_UNIVERSIDAD FROM  CECAT_BASES_OPERA WHERE IC_CLAVE = " +(String)layout.get(33);
				log.debug("Sentencia IC_UNIVERSIDAD: " + query4);
				ps2 = con.queryPrecompilado(query4);
				rs2 = ps2.executeQuery();
				if(rs2.next()){
					icUniv = rs2.getString("IC_UNIVERSIDAD")==null?"":rs2.getString("IC_UNIVERSIDAD");
				}
				ps2.close();
				rs2.close();

				boolean correoVacio = esCampoVacio((String)layout.get(40));
				int offset = correoVacio?0:1;

				ps = con.queryPrecompilado(query3.toString());
				ps.clearParameters();
				String lsLineaG= "";

				char _toCompare='@';
				int veces		=0;
				int tamG			=0;
				char []caracteres=cg_contenido.toCharArray();
				for(int i=0;i<=caracteres.length-1;i++){
					if(veces == (44+offset)){
						tamG = i;
						break;
					}
					if(_toCompare ==caracteres[i]){
						veces++;
					}
				}
				lsLineaG = cg_contenido.substring(0, tamG);

				ps.setBigDecimal(1, folio                         ); //IC_FOLIO_CARGA
				ps.setBigDecimal(2, getBigDecimal(seqNEXTVAL)     ); //IC_REGISTRO
				ps.setString(    3, ic_if                         ); //IC_IF
				ps.setString(    4, cg_contenido                  ); //CG_CONTENIDOE
				ps.setString(    5, lsLineaG                      ); //CG_CONTENIDOG
				ps.setInt(       6, getInt((String)layout.get(31))); //IC_IF_SIAG
				ps.setInt(       7, getInt((String)layout.get(33))); //TIPO_PROGRAMA
				int numRegistrosInsertadosContArch = ps.executeUpdate();
				ps.close();

				// Imprmir mensaje de Depuracion
				log.debug("procesarAltaCreditoEducativo(" + _fechaMsg + "_" + icProceso + ")::Inserciones ce_contenido_arc...OK(" + numRegistrosInsertadosContArch + ")");

				ps = con.queryPrecompilado(query2.toString());
				ps.clearParameters();
				ps.setBigDecimal(1, getBigDecimal(seqNEXTVAL));               //IC_REGISTRO
				ps.setString(    2, getCadena((String)layout.get(0)).trim()); //CG_RAZON_SOCIAL
				ps.setString(    3, getCadena((String)layout.get(1)).trim()); //CG_REGISTRO
				ps.setString(    4, getCadena((String)layout.get(2)).trim()); //CG_CALLE
				ps.setString(    5, getCadena((String)layout.get(3)).trim()); //CG_COLONIA
				ps.setString(    6, getCadena((String)layout.get(4)).trim()); //CG_CODIGOPOSTAL
				ps.setString(    7, getCadena((String)layout.get(5)).trim()); //CG_CIUDAD
				ps.setInt(       8, getInt((String)layout.get(6))          ); //IC_MUNICIPIO
				ps.setInt(       9, getInt((String)layout.get(7))          ); //IC_ESTADO
				ps.setString(   10, getCadena((String)layout.get(8)).trim()); //CG_GIRO
				ps.setString(   11, getCadena((String)layout.get(9)).trim()); //CG_FINANCIAMIENTO
				ps.setString(   12, getCadena((String)layout.get(10)).trim()); //CG_SECTOR
				ps.setString(   13, getCadena((String)layout.get(11)).trim()); //CG_FONDEO
				ps.setString(   14, getCadena((String)layout.get(12)).trim()); //CG_RIESGO
				ps.setString(   15, getCadena((String)layout.get(13)).trim()); //CG_PORCENTAJE_REG
				ps.setInt(      16, getInt((String)layout.get(14))          ); //IC_PERSONAL

				if(layout.get(15) == null  || "".equals((String)layout.get(15))){
					ps.setNull(17,	Types.NUMERIC);											//FN_VENTAS_ANUAL
				}else{
					ps.setBigDecimal(17,	getBigDecimal((String)layout.get(15))	);	//FN_VENTAS_ANUAL
				}
				ps.setString(18, getCadena((String)layout.get(16)).trim()); //CG_PROPOSITO
				ps.setInt(   19, getInt((String)layout.get(17))          ); //IC_TIPO_FINANCIAMIENTO
				ps.setString(20, getCadena((String)layout.get(18)).trim()); //CC_CLAVE_MONTO_GARANTIA
				ps.setString(21, getCadena((String)layout.get(19)).trim()); //CC_RECURSOS

				if(layout.get(20) == null  || "".equals((String)layout.get(20))){
					ps.setNull(22,	Types.NUMERIC);											//FN_PORC_MER_INTERNO
				}else{
					ps.setBigDecimal(22,	getBigDecimal((String)layout.get(20))	);	//FN_PORC_MER_INTERNO
				}
				if(layout.get(21) == null  || "".equals((String)layout.get(21))){
					ps.setNull(23,	Types.NUMERIC);											//FN_PORC_MER_EXTERNO
				}else{
					ps.setBigDecimal(23,	getBigDecimal((String)layout.get(21))	);	//FN_PORC_MER_EXTERNO
				}
				ps.setInt(24,			getInt((String)layout.get(22))			);		//IC_AMORTIZACION

				if(layout.get(23) == null  || "".equals((String)layout.get(23))){
					ps.setNull(25,	Types.NUMERIC);											//FN_FINANCIAMIENTO
				}else{
					ps.setBigDecimal(25,	getBigDecimal((String)layout.get(23))	);	//FN_FINANCIAMIENTO
				}
				if(layout.get(24) == null	||	"".equals((String)layout.get(24))){
					ps.setNull(26,	Types.DATE);
				}else{
					ps.setString(26,	getCadena((String)layout.get(24)).trim());	//DF_DISPOSICION_RIES
				}
				if(layout.get(25) == null  || "".equals((String)layout.get(25))){
					ps.setNull(27,	Types.NUMERIC);											//FN_IMP_DISPOSICION
				}else{
					ps.setBigDecimal(27,	getBigDecimal((String)layout.get(25))	);	//FN_IMP_DISPOSICION
				}
				ps.setString(28, getCadena((String)layout.get(26)).trim()); //CG_MONEDA
				ps.setString(29, getCadena((String)layout.get(27)).trim()); //CG_MESES_PLAZO
				ps.setString(30, getCadena((String)layout.get(28)).trim()); //CG_MESES_FINANCIAMIENTO
				ps.setString(31, getCadena((String)layout.get(29)).trim()); //CG_SOBRETASA
				ps.setInt(   32, getInt((String)layout.get(30))          ); //IC_FUNCIONARIO_IF
				ps.setInt(   33, getInt((String)layout.get(31))          ); //IC_IF_SIAG
				ps.setInt(   34, getInt((String)layout.get(32))          ); //IC_AUTORIZACION
				ps.setInt(   35, getInt((String)layout.get(33))          ); //IC_PROGRAMA
				ps.setInt(   36, getInt((String)layout.get(34))          ); //IC_TASA
				ps.setString(37, getCadena((String)layout.get(35)).trim()); //CG_PLAZO_DIAS
				ps.setString(38, getCadena((String)layout.get(36)).trim()); //CG_CALIFICACION
				ps.setString(39, getCadena((String)layout.get(37)).trim()); //CG_ANTI_CLI
				ps.setString(40, getCadena((String)layout.get(38)).trim()); //CG_NOMCONTRATO
				ps.setString(41, getCadena((String)layout.get(39)).trim()); //CG_TELEFONO

				if(correoVacio){
					ps.setString(42,(String)layout.get(40));								//CG_CORREO
				}else{
					String email = (String)layout.get(40) + "@" + (String)layout.get(41);
					ps.setString(42,						email);								//CG_CORREO
				}

				ps.setString(43, getCadena((String)layout.get(41+offset)).trim()); //CG_DEPENDENCIA
				ps.setString(44, getCadena((String)layout.get(44+offset)).trim()); //CG_MATRICULA
				ps.setString(45, getCadena((String)layout.get(45+offset)).trim()); //CG_DESC_ESTUDIOS
				ps.setString(46, getCadena((String)layout.get(46+offset)).trim()); //DF_INGRESO
				ps.setString(47, getCadena((String)layout.get(47+offset)).trim()); //CG_ANIO
				ps.setString(48, getCadena((String)layout.get(48+offset)).trim()); //IC_PERIODO
				ps.setString(49, getCadena((String)layout.get(49+offset)).trim()); //CG_MODALIDAD
				if((String)layout.get(50+offset) == null	||	"".equals((String)layout.get(50+offset))){
					ps.setNull(50,		Types.DATE													);	//DF_DISPOSICION
				}else{
					ps.setString(50,		getCadena((String)layout.get(50+offset)).trim());//DF_DISPOSICION
				}
				ps.setInt(       51, getInt((String)layout.get(51+offset))          ); //IC_NUM_DISPOSICION
				ps.setBigDecimal(52, getBigDecimal((String)layout.get(52+offset))   );//FN_SALDOLINEAS
				ps.setString(    53, num_registros                                  ); //IN_REGISTROS
				ps.setBigDecimal(54, folio                                          ); //IC_FOLIO_CARGA
				ps.setString(    55, estatus                                        ); //IC_ESTATUS_CRED_EDUCA
				ps.setString(    56, icUniv                                         ); //IC_UNIVERSIDAD  claveUniversidad
				ps.setBigDecimal(57, getBigDecimal(ic_if)                           ); //IC_IF

				ps.setString(    58, getCadena((String)layout.get(42+offset)).trim()); //CG_TIPOPERSONA
				ps.setString(    59, getCadena((String)layout.get(43+offset)).trim()); //CG_SEXOACREDITADDO

				int numRegistrosInsertadosCreditoEducativo = ps.executeUpdate();
				ps.close();

				listaLlaves.add(seqNEXTVAL);

				// Imprmir mensaje de Depuracion
				log.debug("procesarAltaCreditoEducativo(" + _fechaMsg + "_" + icProceso + ")::Inserciones ce_contenido_educativo ...OK("+numRegistrosInsertadosCreditoEducativo+")");

			}

			this.eliminaTemporalesCarga(icProceso);

			/***** Se manda a llamar el m�todo que genera el archivo y env�a el correo *****/
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
			}
			boolean exito = false;
			if(listaLlaves.size() > 0 && ok == true){
				exito = envioCorreoUniversidad(strDirectorio, listaLlaves);
			}

			retorno.setFolio(folio.toString());
			retorno.setFecha(fecha);
			retorno.setHora(hora);

		}catch(Exception e){
			e.printStackTrace();
			ok = false;
			try{
				this.eliminaTemporalesCarga(icProceso);
			}catch(Exception ea){
				throw new AppException("Error al eliminar temporales"+ea.getMessage());
			}
			log.error("CreditoEducativoBean::transmitirAltaGarantiaEducativo(Exception)");
			throw new AppException("Error al guardar la informacion"+e.getMessage());
		}finally{
			if(con.hayConexionAbierta()){
				//con.terminaTransaccion(false);
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("CreditoEducativoBean::transmitirAltaGarantiaEducativo(S)");
		}
		return retorno;
	}

	public boolean eliminaTemporalesCarga(String icProceso){
		log.info("CreditoEducativoBean::eliminaTemporalesCarga(E)");
		try {
			if (icProceso == null || icProceso.equals("")) {
				throw new Exception("El folio de carga es requerido");
			}
		} catch(Exception e) {
			String error =
					"ERROR. Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"icProceso=" + icProceso + "\n";
			throw new AppException(error);
		}
		AccesoDB con= new AccesoDB();
		boolean 	ok	= true;
		try {
			con.conexionDB();
			PreparedStatement ps = null;


			StringBuffer strSQL = new StringBuffer(
				"DELETE cetmp_contenido_arc"+
				" WHERE ic_proceso = ?"
			);
			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1,icProceso);
			ps.execute();
			ps.close();

		} catch(Exception e) {
			ok = false;
			throw new AppException("Error al eliminar temporales", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("CreditoEducativoBean::eliminaTemporalesCarga(S)");
		}
		return ok;
	}

	public Registros getDatosCarga(String folio_carga){
		log.info("CreditoEducativoBean::getDatosCarga(E)");
		try {
			if (folio_carga == null || folio_carga.equals("")) {
				throw new Exception("El folio de carga es requerido");
			}
		} catch(Exception e) {
			String error =
					"ERROR. Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"folio_carga=" + folio_carga + "\n";
			throw new AppException(error);
		}
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();

			StringBuffer strSQL = new StringBuffer(
				"SELECT								" +
				"ic_registro,cg_razon_social,cg_registro,cg_calle,cg_colonia,cg_codigopostal,"+
				"cg_ciudad,ic_municipio,ic_estado,cg_giro,cg_financiamiento,cg_sector,cg_fondeo,cg_riesgo,"+
				"cg_porcentaje_reg,ic_personal,fn_ventas_anual,cg_proposito,ic_tipo_financiamiento,cc_clave_monto_garantia,"+
				"cc_recursos,fn_porc_mer_interno,fn_porc_mer_externo,ic_amortizacion,fn_financiamiento,"+
				"TO_CHAR(df_disposicion_ries,'dd/mm/yyyy') AS df_disposicion_ries,fn_imp_disposicion,cg_moneda,cg_meses_plazo,cg_meses_financiamiento,"+
				"cg_sobretasa,ic_funcionario_if,ic_if_siag,ic_autorizacion,ic_programa,ic_tasa,cg_plazo_dias,cg_calificacion,"+
				"cg_anti_cli,cg_nomcontrato,cg_telefono,cg_correo,cg_dependencia,"+
				"cg_matricula,cg_desc_estudios,TO_CHAR(df_ingreso,'dd/mm/yyyy') AS df_ingreso,cg_anio,ic_periodo,cg_modalidad,"+
				"TO_CHAR(df_disposicion,'dd/mm/yyyy') AS df_disposicion,ic_num_disposicion,fn_saldolineas,in_registros_acep,in_registros_rech,"+
				"in_registros,ic_folio_carga,TO_CHAR(fn_fecha_carga,'dd/mm/yyyy') AS fn_fecha_carga,ic_estatus_cred_educa,ic_universidad "+
				", CG_TIPOPERSONA ,  CG_SEXOACREDITADO "+
				" FROM ce_contenido_educativo " +
				" WHERE ic_folio_carga = ? "
			);

			List params = new ArrayList();
			params.add(folio_carga);

			log.debug("getDatosCarga:: strSQL=" + strSQL.toString() );
			log.debug("getDatosCarga:: params=" + params );

			Registros reg = con.consultarDB(strSQL.toString(), params);

			return reg;

		} catch(Exception e) {
			throw new AppException("Error al obtener los datos de acuse", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CreditoEducativoBean::getDatosCarga(S)");
		}

	}

	/**
	 * Validaciones de alta de garantias.
	 * PRECAUCION: Las validaciones son comunes tanto a la carga por archivo
	 * como a la carga de garantias autom�ticas.
	 * Son las validaciones de los campos que componen una linea en el archivo de carga.
	 * El m�todo no regresa ningun valor pero se modifica el estado del objeto
	 * ResultadosGarEducativo, el cual llevara el registro de los errores de validaci�n.
	 *
	 * @param ifSiag clave SIAG del Intermediario Financiero
	 * @param numLinea Numero de linea que se est� validando
	 * @param strLinea linea de valores del layout de alta de garantias.
	 * @param retorno Objeto de ResultadosGarEducativo, que contiene los detalles de la validaci�n
	 *
	 * @author Gilberto Aparicio
	 *
	 * Copiado de GarantiasBean por HVC
	 *
	 */
	private void validarAltaGarantias(int ifSiag, int numLinea,
		String strLinea,	String claveIf, ResultadosGarEducativo retorno) {

		if(strLinea.length() >= GTI_CONTENIDO_ARCH_CG_CONTENIDO_LENGTH){
			retorno.appendErrores("Error en la linea:"+numLinea+" El tama�o del registro es demasiado grande para ser procesado. \n");
			retorno.setErrorLinea(true);
			return;
		}

		VectorTokenizer 	vt 		= new VectorTokenizer(strLinea,"@");
		List 					layout 	= vt.getValuesVector();

		boolean  existeCampoCorreo 		= layout.size() >= 41;
		boolean 	correoVacio 				= existeCampoCorreo?esCampoVacio((String)layout.get(40)):true;

		// Determinar el numero de elementos esperados
		int 		numElementosEsperados	= 0;
		int 		recibidos					= 0;

		//numElementosEsperados 	= 50 + 1 +(existeCampoCorreo && !correoVacio?1:0) ;//+1  y +1: es por la arroba del correo dominio del correo y por el ultimo arroba que lleva al final de cada registro
		numElementosEsperados 	= 53 + 1 +(existeCampoCorreo && !correoVacio?1:0) ;//+1  y +1: es por la arroba del correo dominio del correo y por el ultimo arroba que lleva al final de cada registro
		recibidos					= layout.size() - 1 - (existeCampoCorreo && !correoVacio?1:0);

		if( layout.size() != numElementosEsperados ){
			retorno.appendErrores("Error en la linea:"+numLinea+" El numero de campos es incorrecto. Esperados(53) Recibidos("+ recibidos +") \n");
			retorno.setErrorLinea(true);
			return;
		}

		if((strLinea.charAt(strLinea.length()-1)) != '@'){
			retorno.appendErrores("Error en la linea:"+numLinea+" La cadena termina incorrectamente; debe llevar '@' al final \n");
			retorno.setErrorLinea(true);
			return;
		}

		int longitudClaveFinanciamiento = 20;

		// VALIDACIONES COMUNES
		//----------------------------------------------------------------------------------------------------------------
		boolean campoValido = true;

		boolean primerasPerdidas	= false;
		boolean validaClaveFinan	= false;
		boolean validaNumDiposicion= false;
		boolean validaIntermediarioSiag		= false;

		// 1.  Nombre o Raz�n Social del acreditado										CHAR	   80						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(0), "Nombre o Raz�n Social del acreditado",   	 	retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(0), "Nombre o Raz�n Social del acreditado", 		 	retorno, 80);

		// 2.  Registro Federal de Causante del acreditado								CHAR	   13						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(1), "Registro Federal de Causante del acreditado",	retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(1), "Registro Federal de Causante del acreditado",	retorno, 13);
		if(campoValido)               validaRFC_v2(           numLinea, (String)layout.get(1), "Registro Federal de Causante del acreditado",	retorno    );

		// 3.  Calle y N�mero del acreditado												CHAR	   80						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(2), "Calle y N�mero del acreditado",   					retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(2), "Calle y N�mero del acreditado",        			retorno, 80);

		// 4.  Colonia del acreditado															CHAR	   80						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(3), "Colonia del acreditado",   							retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(3), "Colonia del acreditado",              			retorno, 80);

		// 5.  C�digo Postal																		CHAR		 5						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(4), "C�digo Postal",   										retorno    );
		if(campoValido) campoValido = validarLongitud( 			numLinea, (String)layout.get(4), "C�digo Postal",                   					retorno,  5);

		// 6.  Ciudad																				CHAR		80						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(5), "Ciudad",   												retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(5), "Ciudad", 													retorno, 80);

		// 7.  Clave del Municipio o Delegaci�n del acreditado						NUMBER	 4		Cat�logo A	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(6), "Clave del Municipio o Delegaci�n del acreditado",   		retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(6), "Clave del Municipio o Delegaci�n del acreditado",       retorno,  4);
		if(campoValido)               validaEntero(         	numLinea, (String)layout.get(6), "Clave del Municipio o Delegaci�n del acreditado",       retorno    );

		// 8.  Clave del Estado o Entidad Federativa del acreditado					NUMBER	 4		Cat�logo B	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(7), "Clave del Estado o Entidad Federativa del acreditado",  retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(7), "Clave del Estado o Entidad Federativa del acreditado",  retorno,  4);
		if(campoValido)               validaEntero(         	numLinea, (String)layout.get(7), "Clave del Estado o Entidad Federativa del acreditado",  retorno    );

		// 9.  Giro de la Actividad a la que se dedica el acreditado				CHAR	  300						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(8), "Giro de la Actividad a la que se dedica el acreditado", retorno     );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(8), "Giro de la Actividad a la que se dedica el acreditado", retorno, 300);

//Validar repetidos		//10.  Clave del financiamiento														CHAR	   20/30(para SHF)	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(9), "Clave del financiamiento",   						retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(9), "Clave del financiamiento",               		retorno, longitudClaveFinanciamiento);
		if(campoValido) campoValido = validaClaveFinan(			numLinea, (String)layout.get(9),	"Clave del financiamiento",							retorno		);
		if(campoValido){
			HashMap map = retorno.getMapaCredito();
			map.put(new Integer(numLinea),(String)layout.get(9));
			retorno.setMapaCredito(map);
			validaClaveFinan = true;
		}

		//11.  Sector de Actividad Econ�mica												CHAR		 8		Cat�logo C	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(10), "Sector de Actividad Econ�mica",   				retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(10), "Sector de Actividad Econ�mica",         		retorno,  8);

		//12.  Solicita Fondeo de NAFIN														CHAR		 1		S o N	      Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(11), "Solicita Fondeo de NAFIN",   						retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(11), "Solicita Fondeo de NAFIN",         				retorno,  1);
		if(campoValido)               validaSN(					numLinea, (String)layout.get(11), "Solicita Fondeo de NAFIN",							retorno	  );

		//13.  Solicita la Participaci�n en el Riesgo de NAFIN						CHAR		 1		S o N	      Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(12), "Solicita la Participaci�n en el Riesgo de NAFIN",		retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(12), "Solicita la Participaci�n en el Riesgo de NAFIN",  		retorno,  1);
		if(campoValido)               validaSN(					numLinea, (String)layout.get(12), "Solicita la Participaci�n en el Riesgo de NAFIN",		retorno    );

		//14.  Porcentaje de Participaci�n en el Riesgo Solicitado.					NUMBER	 5,2					Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(13), "Porcentaje de Participaci�n en el Riesgo Solicitado",	retorno      );
		if(campoValido) campoValido = validaPrecisionDecimal( numLinea, (String)layout.get(13), "Porcentaje de Participaci�n en el Riesgo Solicitado",  retorno, 5, 2);
		if(campoValido)               validaFlotanteV2(			numLinea, (String)layout.get(13), "Porcentaje de Participaci�n en el Riesgo Solicitado",	retorno	    );

		//15.  Personal Ocupado (n�mero)														NUMBER	10						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(14), "Personal Ocupado (n�mero)",   									retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(14), "Personal Ocupado (n�mero)",  									retorno, 10);
		if(campoValido)               validaEntero(         	numLinea, (String)layout.get(14), "Personal Ocupado (n�mero)",										retorno    );

		//16.  Promedio de Ventas Anual (en pesos)										NUMBER 13,2						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(15), "Promedio de Ventas Anual (en pesos)",   					retorno       );
		if(campoValido) campoValido = validaPrecisionDecimal( numLinea, (String)layout.get(15), "Promedio de Ventas Anual (en pesos)",  						retorno, 13, 2);
		if(campoValido)               validaFlotanteV2(       numLinea, (String)layout.get(15), "Promedio de Ventas Anual (en pesos)",						retorno	     );

		//17.  Prop�sito del Proyecto															CHAR	  2*N	   Cat�logo D	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(16), "Prop�sito del Proyecto",   										retorno    );
		if(campoValido) campoValido = validaLongitudMultiplo( numLinea, (String)layout.get(16), "Prop�sito del Proyecto",  										retorno,  2);
		if(campoValido)               validaProposito(        numLinea, (String)layout.get(16), "Prop�sito del Proyecto",											retorno    );

		//18.  Clave del Tipo de financiamiento											NUMBER	 6		Cat�logo E	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(17), "Clave del Tipo de financiamiento",   						retorno    );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(17), "Clave del Tipo de financiamiento",  							retorno,  6);
		if(campoValido)               validaEntero(				numLinea, (String)layout.get(17), "Clave del Tipo de financiamiento",							retorno	  );

		//19. Garant�a Otorgada por Acreditado y Monto de la Garant�a Otorgada.
		//19a. Clave Garant�a Otorgada por el Acreditado								NUMBER	 2		Cat�logo F	Obligatorio
		//19b. Monto de la garant�a Otorgada												NUMBER 12,2
		// Nota: Como es un campo compuesto no se permiten espacios al inicio o al final.
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(18), "Clave y Monto de la garantia otorgada",   					retorno    );
		if(campoValido) campoValido = validaLongitudMultiplo( numLinea, (String)layout.get(18), "Clave y Monto de la garantia otorgada",  					retorno, 14);
		if(campoValido)               validaClaveMonto(       numLinea, (String)layout.get(18), "Clave y Monto de la garantia otorgada",                retorno    );

		//20.  Destino de los Recursos.
		//20a. Clave del Tipo de Destino de los Recursos								NUMBER	 2		Cat�logo G	Obligatorio
		//20b. Porcentaje Parcial																NUMBER	 3
		//20c. Porcentaje de origen nacional												NUMBER	 3
		//20d. Porcentaje de importaci�n														NUMBER	 3
		// Nota: Como es un campo compuesto no se permiten espacios al inicio o al final
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(19), "Destino de los Recursos",   									retorno    );
		if(campoValido) campoValido = validaLongitudMultiplo( numLinea, (String)layout.get(19), "Destino de los Recursos",  										retorno, 11);
		if(campoValido)               validaDestino(          numLinea, (String)layout.get(19), 																			retorno    );

		//21.  Porcentaje de la Producci�n destinada al Mercado Interno			NUMBER  5,2						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(20), "Porcentaje de la Producci�n destinada al Mercado Interno",	retorno      );
		if(campoValido) campoValido = validaPrecisionDecimal( numLinea, (String)layout.get(20), "Porcentaje de la Producci�n destinada al Mercado Interno",  	retorno, 5, 2);
		if(campoValido)               validaFlotanteV2(       numLinea, (String)layout.get(20), "Porcentaje de la Producci�n destinada al Mercado Interno",   retorno      );

		//22.  Porcentaje de la Producci�n destinada al Mercado Externo			NUMBER  5,2						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(21), "Porcentaje de la Producci�n destinada al Mercado Externo",   retorno      );
		if(campoValido) campoValido = validaPrecisionDecimal( numLinea, (String)layout.get(21), "Porcentaje de la Producci�n destinada al Mercado Externo",  	retorno, 5, 2);
		if(campoValido)               validaFlotanteV2(       numLinea, (String)layout.get(21), "Porcentaje de la Producci�n destinada al Mercado Externo",	retorno      );

		//23.  Forma de Amortizaci�n															NUMBER	 2		Cat�logo H	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(22), "Forma de Amortizaci�n",   										retorno      );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(22), "Forma de Amortizaci�n",  										retorno,    2);
		if(campoValido)               validaEntero(         	numLinea, (String)layout.get(22), "Forma de Amortizaci�n",											retorno      );

//Monto total		//24.  Monto del Financiamiento														NUMBER 17,2						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(23), "Monto del Financiamiento",   									retorno 	    );
		if(campoValido) campoValido = validaPrecisionDecimal( numLinea, (String)layout.get(23), "Monto del Financiamiento",  									retorno,17, 2);
		if(campoValido)               validaFlotanteV2(       numLinea, (String)layout.get(23), "Monto del Financiamiento",										retorno	    );

		//25.  Fecha de Disposici�n y de Participaci�n en el Riesgo					DATE	    8						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(24), "Fecha de Disposici�n y de Participaci�n en el Riesgo", retorno      );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(24), "Fecha de Disposici�n y de Participaci�n en el Riesgo", retorno,    8);
		if(campoValido)               validaFecha(            numLinea, (String)layout.get(24), "Fecha de Disposici�n y de Participaci�n en el Riesgo", retorno      );

		//26.  Importe de Disposici�n															NUMBER 17,2						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(25), "Importe de Disposici�n",   										retorno      );
		if(campoValido) campoValido = validaPrecisionDecimal( numLinea, (String)layout.get(25), "Importe de Disposici�n",  										retorno,17, 2);
		if(campoValido)               validaFlotanteV2(       numLinea, (String)layout.get(25), "Importe de Disposici�n",											retorno      );

		//27.  Moneda																				CHAR	    7		Cat�logo I	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(26), "Moneda",   															retorno      );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(26), "Moneda",  															retorno,    7);
		if(campoValido)               validaMoneda(				numLinea, (String)layout.get(26), 																			retorno      );

		//28.  N�mero de Meses de Plazo														NUMBER	 6						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(27), "N�mero de Meses de Plazo",   									retorno   	 );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(27), "N�mero de Meses de Plazo",  									retorno,    6);
		if(campoValido)               validaEntero(				numLinea, (String)layout.get(27), "N�mero de Meses de Plazo",										retorno		 );

		//29.  N�mero de Meses de la Gracia del Financiemiento						NUMBER	 6						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(28), "N�mero de Meses de la Gracia del Financiemiento",   	retorno      );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(28), "N�mero de Meses de la Gracia del Financiemiento",		retorno,    6);
		if(campoValido)               validaEntero(         	numLinea, (String)layout.get(28), "N�mero de Meses de la Gracia del Financiemiento",      retorno      );

		//30.  Sobretasa																			NUMBER  7,4						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(29), "Sobretasa",   														retorno      );
		if(campoValido) campoValido = validaPrecisionDecimal( numLinea, (String)layout.get(29), "Sobretasa",  														retorno, 7, 4);
		if(campoValido)               validaFlotanteV2(       numLinea, (String)layout.get(29), "Sobretasa",															retorno		 );

		//31.  Clave del Funcionario Facultado del Intermediario Financiero 		NUMBER	 8		Cat�logo J	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(30), "Clave del Funcionario Facultado del Intermediario Financiero", retorno   );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(30), "Clave del Funcionario Facultado del Intermediario Financiero", retorno, 8);
		if(campoValido)               validaEntero(         	numLinea, (String)layout.get(30), "Clave del Funcionario Facultado del Intermediario Financiero", retorno   );

		//32.  Clave del Intermediario Financiero.										NUMBER	 5		Cat�logo K	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(31), "Clave del Intermediario Financiero",   						retorno     );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(31), "Clave del Intermediario Financiero", 						retorno,   5);
		if(campoValido) campoValido = validaIntermediario(    numLinea, (String)layout.get(31), "Clave del Intermediario Financiero", ifSiag,				retorno, ""	);
		if(campoValido) validaIntermediarioSiag = true;

		//33.  Tipo de Autorizaci�n															NUMBER	 6		Cat�logo L	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(32), "Tipo de Autorizaci�n",   										retorno     );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(32), "Tipo de Autorizaci�n", 											retorno,   6);
		if(campoValido)               validaEntero(         	numLinea, (String)layout.get(32), "Tipo de Autorizaci�n",											retorno		);

		//34.  Tipo Programa																		NUMBER	 5		Cat�logo M	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(33), "Tipo Programa",   													retorno     );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(33), "Tipo Programa", 													retorno,   CLAVE_DEL_PROGRAMA_LENGTH); // CLAVE_DEL_PROGRAMA_LENGTH = 5
		if(campoValido) campoValido = validaEntero(         	numLinea, (String)layout.get(33), "Tipo programa",												retorno, "" ); //FODEA 017 - 2009 ACF
		if(campoValido) primerasPerdidas = validaTipoPrograma(		numLinea, (String)layout.get(33), "Tipo programa",						claveIf,				retorno		);
		//primerasPerdidas

		//35.  Tipo Tasa																			CHAR	    6		Cat�logo N	Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(34), "Tipo Tasa",   														retorno     );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(34), "Tipo Tasa", 															retorno,   6);

		//36.  Plazo en n�mero de d�as														NUMBER	 2						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio( numLinea, (String)layout.get(35), "Plazo en n�mero de d�as",   									retorno     );
		if(campoValido) campoValido = validarLongitud(        numLinea, (String)layout.get(35), "Plazo en n�mero de d�as", 										retorno,   2);
		if(campoValido)               validaEntero(         	numLinea, (String)layout.get(35), "Plazo en n�mero de d�as",										retorno     );

		//37.  Calificaci�n Inicial															CHAR	    4
		//validarCampoConEspacioEnBlanco(								numLinea, (String)layout.get(36),"Calificaci�n Inicial",											retorno		);
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio(    numLinea, (String)layout.get(36), "Calificaci�n Inicial",   					retorno     );
		if(campoValido) campoValido = validarLongitud(           numLinea, (String)layout.get(36), "Calificaci�n Inicial", 						retorno,   4);

		//38.  Antig�edad como cliente en meses											NUMBER	 4						Obligatorio
		campoValido = true;
		if(campoValido) campoValido = validaCampoObligatorio(    numLinea, (String)layout.get(37), "Antig�edad como cliente en meses",   					retorno     );
		if(campoValido) campoValido = validarLongitud(           numLinea, (String)layout.get(37), "Antig�edad como cliente en meses", 						retorno,   4);
		if(campoValido)               validaEntero(            	numLinea, (String)layout.get(37), "Antig�edad como cliente en meses",						retorno     );

		//39.  Nombre del Contacto y/o Representante										CHAR	   80
		if(!esVacio((String)layout.get(38))){
			campoValido = true;
			if(campoValido) campoValido = validarLongitud(           numLinea, (String)layout.get(38), "Nombre del Contacto y/o Representante", 				retorno,  80);
			// validarNombreContacto(     numLinea, (String)layout.get(38), "Nombre del Contacto y/o Representante",             retorno     );
		}

		//40. Tel�fonos del Contacto.
		//40a. Clave del Tipo de Tel�fono													NUMBER	 2	   Cat�logo O
		//40b. Tel�fono con clave lada														NUMBER	10
		//40c. Extensi�n																			NUMBER	 6
		//Nota: Se requiere al menos un telefono de oficina y un telefono celular.
		//Nota: Como es un campo compuesto no se permiten espacios al inicio o al final.
		//if(!esEspacioEnBlanco((String)layout.get(39))){
		if(!esVacio((String)layout.get(39))){
			campoValido = true;
			if(campoValido) campoValido = validaLongitudMultiplo(    numLinea, (String)layout.get(39), "Tel�fonos del Contacto",  									retorno, 18);
			if(campoValido)               validarTelefonoPyme(    	numLinea, (String)layout.get(39), "Tel�fonos del Contacto", 									retorno    );
		}

		//41.  Correo Electr�nico																CHAR	   40
		int offset  = 0;
		if(!esVacio((String)layout.get(40))){
			// Si el campo 40 viene vac�o, se asume que no se especific� correo, en caso contrario,
			// Se asume que el campo 40 y 41 son corresponden al correo
			campoValido = true;
			if(campoValido) campoValido = validarLongitud(  numLinea, (String)layout.get(40) + "@" + (String)layout.get(41), "Correo Electr�nico", retorno, 40);
			if(campoValido)               validarCorreo(    numLinea, (String)layout.get(40) + "@" + (String)layout.get(41), "Correo Electr�nico", retorno    );
			offset = 1;
		}

		//42.  Dependencia a la que vende													NUMBER	 4	   Cat�logo	P
		if(!esVacio((String)layout.get(41+offset))){
			campoValido = true;
			if(campoValido) campoValido = validarLongitud(    numLinea, (String)layout.get(41+offset),"Dependencia a la que vende",         retorno,  4);
			if(campoValido)               validarDependencia( numLinea, (String)layout.get(41+offset),"Dependencia a la que vende",         retorno    );
		}

		// Las siguientes lineas ya no van a validarse, ya no deben de ir el el layout de carga...
		 //43.  Tipo de Persona																	CHAR		 1		F				Obligatorio
		 //validarCampoRequeridoVacio(	numLinea, (String)layout.get(42+offset),"Tipo de Persona", retorno                );
		 campoValido = true;
		 if(campoValido) campoValido = validaCampoObligatorio(	numLinea,	(String)layout.get(42+offset),	"Tipo de Persona",	retorno);
		 if(campoValido) campoValido = validarLongitud(   numLinea,	(String)layout.get(42+offset),	"Tipo de Persona", retorno, 1				);
		 if(campoValido) validaTipoPersonaC(numLinea, (String)layout.get(42+offset), "Tipo de Persona",	retorno		);


		// Las siguientes lineas ya no van a validarse, ya no deben de ir el el layout de carga...
		 //44.  Sexo del Acreditado															CHAR		 1		H=Hombre M = Mujer			Obligatorio
		// validarCampoRequeridoVacio(	numLinea, (String)layout.get(43+offset),"Sexo del Acreditado", retorno                );
		 campoValido = true;
		 if( ((String)layout.get(42+offset)).equals("F") ){
			 if(campoValido) campoValido = validaCampoObligatorio(	numLinea,	(String)layout.get(43+offset),	"G�nero del Acreditado",	retorno);
			 if(campoValido) campoValido = validarLongitud(   numLinea,	(String)layout.get(43+offset),	"G�nero  del Acreditado", retorno, 1				);
			 if(campoValido) validaTipoSexo(numLinea, (String)layout.get(43+offset), "G�nero del Acreditado",	retorno		);

		}else  if( ((String)layout.get(42+offset)).equals("M") ){
			if(campoValido) validarCampoRequeridoVacio(	numLinea, (String)layout.get(43+offset),"G�nero del Acreditado", retorno                );
		}
		/*
		//
		 //44.  Clave �nica de Registro de Poblaci�n										CHAR		18						Obligatorio
		 validarCampoRequeridoVacio(	numLinea, (String)layout.get(43+offset),"Clave �nica de Registro de Poblaci�n", retorno                );

		//45.  Registro Patronal del IMSS
		validarCampoRequeridoVacio(	numLinea, (String)layout.get(44+offset),"Registro Patronal del IMSS", retorno                );

		//46.  Nuevos Empleos Generados														NUMBER	 3						Obligatorio
		validarCampoRequeridoVacio(	numLinea, (String)layout.get(45+offset),"No. de Empleos Adicionaless", retorno                );

		///*	Validaciones para credito educativo	*///

		//45. Id Matricula																		CHAR		20						Obligatorio

		if(!esVacio((String)layout.get(44+offset))){
			campoValido           = true;
			//if(campoValido) campoValido =	validaCampoObligatorio(	numLinea,	(String)layout.get(44+offset),	"Id Matricula",				retorno							);
			if(campoValido) campoValido = validarLongitud(        numLinea,	(String)layout.get(44+offset),	"Id Matricula",				retorno,		20					);
			//if(campoValido)					validaIdMatricula(		numLinea,	(String)layout.get(44+offset),	"Id Matricula",				retorno							);
			if(campoValido){
				HashMap map = retorno.getMapaMatricula();
				map.put(new Integer(numLinea),(String)layout.get(44+offset));
				retorno.setMapaMatricula(map);
			}
		}

		//46. Descripci�n estudios																CHAR		200					Obligatorio
		if(!esVacio((String)layout.get(45+offset))){
			campoValido           = true;
			//if(campoValido) campoValido =	validaCampoObligatorio(	numLinea,	(String)layout.get(45+offset),	"Descripci�n de estudios",	retorno							);
			if(campoValido) campoValido = validarLongitud(        numLinea,	(String)layout.get(45+offset),	"Descripci�n de estudios",	retorno,		200				);
		}

		//47. Fecha Ingreso																		DATE								Obligatorio
		if(!esVacio((String)layout.get(46+offset))){
			campoValido           = true;
			//if(campoValido) campoValido =	validaCampoObligatorio(	numLinea,	(String)layout.get(46+offset),	"Fecha Ingreso",				retorno);
			if(campoValido) campoValido = validarLongitud(        numLinea,	(String)layout.get(46+offset),	"Fecha Ingreso",				retorno,			8				);
			if(campoValido)					validaFechaEducativo(	numLinea,	(String)layout.get(46+offset),	"Fecha Ingreso",				retorno);
		}

		//48. A�o Colocaci�n																		NUMBER	4						Obligatorio
		if(!esVacio((String)layout.get(47+offset))){
			campoValido           = true;
			//if(campoValido) campoValido =	validaCampoObligatorio(	numLinea,	(String)layout.get(47+offset),	"A�o colocaci�n",				retorno							);
			if(campoValido) campoValido = validaEntero(           numLinea,	(String)layout.get(47+offset),	"A�o colocaci�n",				retorno			,""			);
			if(campoValido)					validaAnioTrimEducativo(numLinea,	(String)layout.get(47+offset),	"A�o colocaci�n",				retorno							);
		}

		//49. Id Periodo educativo																NUMBER	10						Obligatorio
		if(!esVacio((String)layout.get(48+offset))){
			campoValido           = true;
			//if(campoValido) campoValido =	validaCampoObligatorio(	numLinea,	(String)layout.get(48+offset),	"Id periodo educativo",		retorno							);
			if(campoValido) campoValido = validarLongitud(        numLinea,	(String)layout.get(48+offset),	"Id periodo educativo",		retorno,		10					);
			if(campoValido)					validaEntero(				numLinea,	(String)layout.get(48+offset),	"Id periodo educativo",		retorno							);
		}

		//50. Modalidad programa																CHAR		200					Obligatorio
		if(!esVacio((String)layout.get(49+offset))){
			campoValido           = true;
			//if(campoValido) campoValido =	validaCampoObligatorio(	numLinea,	(String)layout.get(49+offset),	"Modalidad programa",		retorno							);
			if(campoValido) campoValido = validarLongitud(        numLinea,	(String)layout.get(49+offset),	"Modalidad programa",		retorno,		200				);
		}

		//51. Fecha �ltima disposici�n														DATE		8
		//if(!esEspacioEnBlanco((String)layout.get(52))){
		if(!esVacio((String)layout.get(50+offset))){
			campoValido           = true;
			if(campoValido) campoValido = validarLongitud(		numLinea,	(String)layout.get(50+offset),	"Fecha �ltima disposici�n",retorno,		8					);
			if(campoValido)					validaFechaEducativo(numLinea,	(String)layout.get(50+offset),	"Fecha �ltima disposicion",retorno							);
		}

		//52. N�mero disposici�n																NUMBER	200					Obligatorio
		campoValido           = true;
		if(campoValido) campoValido =	validaCampoObligatorio(	numLinea,	(String)layout.get(51+offset),	"N�mero Disposici�n",		retorno							);
		if(campoValido) campoValido = validarLongitud(        numLinea,	(String)layout.get(51+offset),	"N�mero Disposici�n",		retorno,		200				);
		if(campoValido) campoValido = validaEntero(				numLinea,	(String)layout.get(51+offset),	"N�mero Disposici�n",		retorno,		""					);
		if(campoValido) validaNumDiposicion = true;

		//53. Saldo en l�neas																	NUMBER 22,2						Obligatorio
		if(!esVacio((String)layout.get(52+offset))){
			campoValido           = true;
			//if(campoValido) campoValido =	validaCampoObligatorio(	numLinea,	(String)layout.get(52+offset),	"Saldo en l�neas",					retorno					);
			if(campoValido) campoValido = validaPrecisionDecimal( numLinea,	(String)layout.get(52+offset),	"Saldo en l�neas",					retorno, 22, 2			);
			if(campoValido)               validaFlotanteV2(       numLinea,	(String)layout.get(52+offset),	"Saldo en l�neas",					retorno		 			);
		}

		if (primerasPerdidas	&&	validaClaveFinan	&&	validaNumDiposicion && validaIntermediarioSiag){

			String claveFin = (String)layout.get(9);
			String numDispo = (String)layout.get(51+offset);
			String if_siag  = (String)layout.get(31);
			validaPrimerasPerdidas(numLinea,	claveFin,	numDispo,	if_siag,	retorno);

		}
	}

	/**
	 * Revisa si el String proporcionado en el parametro: <tt>campo</tt> viene vacio.
	 * @param campo <tt>String</tt> a validar.
	 * @return <tt>boolean</tt> con valor <tt>true</tt> si el campo viene vacio o tiene espacios en blanco;
	 *         regresa <tt>false</tt> en caso contrario.
	 * @since Fodea 025 - 2011
	 * @author Salim Hernandez
	 *
	 * Copiado de GarantiasBean por HVC
	 *
	 */
	private boolean esCampoVacio(String campo){
		log.info("esCampoVacio(E)");
		boolean resultado = false;
		if(campo == null || campo.trim().equals("")){
			resultado = true;
		}
		log.info("esCampoVacio(S)");
		return resultado;
	}

	private String descomprimirArchivo(String strDirectorio,String esNombreArchivo)
		throws Exception{
		InputStream inStream = new FileInputStream(esNombreArchivo);
		ZipInputStream zipInStream = new ZipInputStream(inStream);
		ZipEntry zipEntry = null;
		byte arrByte[] = new byte[4096];
		OutputStream outstream = null;
		while ((zipEntry = zipInStream.getNextEntry()) != null) {
			if(zipEntry.isDirectory()) {
				continue; /*Ignora Directorios*/
			}
			String rutaArchivo = null;
			rutaArchivo = zipEntry.getName();
			if(outstream==null){
				outstream =
						new BufferedOutputStream(
								new FileOutputStream(strDirectorio + rutaArchivo));
				esNombreArchivo = strDirectorio + rutaArchivo;
			}
			int i = 0;
			while((i = zipInStream.read(arrByte, 0, arrByte.length)) != -1) {
				outstream.write(arrByte, 0, i);
			}
			outstream.close();
		}
		zipInStream.close();
		return esNombreArchivo;
	}

	/**
	 * Calcula el siguiente numero de proceso.
	 * Anteriormente, el valor se calculaba con una max+1, pero con concurrencia
	 * podia regresar el mismo valor. Se ajusta para que use una secuencia,
	 * la cual al llegar a su valor maximo volver� a comenzar, por lo cual
	 * es importante que la tabla se depure adecuadamente.
	 * @author Gilberto Aparicio
	 * @return Numero de proceso
 	 * @throws com.netro.exception
	 *
	 * Copiado de GarantiasBean por HVC
	 *
	 */
	private int calculaIcProceso() {
		log.info("getIcProceso(E)");
		AccesoDB con = new AccesoDB();
		int retorno = 0;
		boolean exito = true;
		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			String qrySentencia =
				" SELECT seq_gtitmp_contenido_arch.nextval " +
				" FROM dual ";
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			if(rs.next()){
				retorno = rs.getInt(1);
			}
			rs.close();
			ps.close();
			log.debug("retorno--->"+retorno);
			return retorno;
		}catch(Exception e){
		   exito = false;
			throw new AppException("getIcProceso(Exception):: Error al calcular el numero de proceso." , e);
		}finally{
			if (con.hayConexionAbierta()) {
				//con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.debug("getIcProceso(S)");
		}
	}

	/**
	 *
	 * Valida que un campo, obtenido de la linea de un archivo de carga,
	 * sea flotante; no permite notacion cientifica.
	 *
	 * @param numLinea    Numero de linea del archivo
	 * @param campo       Valor del campo a validar
	 * @param nombreCampo Nombre del campo que se valida
	 * @param rg          Objeto que contiene los resultados del procesamiento
	 *
	 */
	private boolean validaFlotanteV2(
		int           numLinea,
		String        campo,
		String        nombreCampo,
		ResultadosGarEducativo rg
	){

		boolean error = false;
		try{
			campo = campo.trim();
			if( !campo.matches("[\\-]?[\\d]+[\\.]?") && !campo.matches("[\\-]?[\\d]*[\\.][\\d]+") ){
				error = true;
			}
		}catch(Exception e){
			error = true;
		}
		if( error ){
			if("Monto total".equals(nombreCampo)){
				rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo '"+nombreCampo+"' debe ser un n�mero entero de m�ximo 22 enteros y 2 decimales. \n");
			}else if ("Saldo en l�neas".equals(nombreCampo)){
				rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo '"+nombreCampo+"' debe ser un n�mero entero de m�ximo 22 enteros y 2 decimales. \n");
			}else{
				rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo '"+nombreCampo+"' es incorrecto. \n");
			}
			rg.setErrorLinea(true);
		}
		return !error;

	}

	private void validaRFC_v2( int numLinea, String rfc, String nombreCampo, ResultadosGarEducativo rg){
		try{
			if(rfc.indexOf('-')>0||rfc.indexOf(' ')>0){
				rg.appendErrores("Error en la linea:"+numLinea+" El campo "+nombreCampo+" no debe tener guiones ni espacios en blanco. \n");
				rg.setErrorLinea(true);
			}

		}catch(Exception e){
			rg.appendErrores("Error en la linea:"+numLinea+" El campo "+nombreCampo+" es incorrecto. \n");
			rg.setErrorLinea(true);
		}
	}

	private void validaEntero(int numLinea,String campo,String nombreCampo,ResultadosGarEducativo rg){
		try{
			Long.parseLong(campo.trim());
		}catch(Exception e){
			rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo '"+nombreCampo+"'  debe ser un numero entero. \n");
			rg.setErrorLinea(true);
		}
	}

	private boolean validaEntero(int numLinea,String campo,String nombreCampo,ResultadosGarEducativo rg, String credito){
		boolean valida = true;
		try{
			Long.parseLong(campo.trim());
		}catch(Exception e){
			valida = false;
			rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo '"+nombreCampo+"'  debe ser n�merico. \n");
			rg.setErrorLinea(true);
		}
		return valida;
	}

	private boolean validaIntermediario(int numLinea,String campo,String nombreCampo,int ifSiag,ResultadosGarEducativo rg, String credito){
		boolean valida = true;
		try{
			if(Integer.parseInt(campo.trim())!=ifSiag){
				rg.appendErrores("Error en la linea:"+numLinea+" La clave del intermediario financiero es incorrecta.\n");
				rg.setErrorLinea(true);
				valida = false;
			}
		}catch(Exception e){
			rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo '"+nombreCampo+"'  debe ser un numero entero. \n");
			rg.setErrorLinea(true);
			valida = false;
		}
		return valida;
	}

	private void validaSN(int numLinea,String campo,String nombreCampo,ResultadosGarEducativo rg){
			if(!"S".equals(campo.trim())&&!"N".equals(campo)){
				rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo '"+nombreCampo+"'  debe ser S o N. \n");
				rg.setErrorLinea(true);
			}
	}


	private void validaProposito(int numLinea,String valor,String nombreCampo,ResultadosGarEducativo rg){
		boolean error = false;
		String numeros = "0123456789";
		try{

			for(int i=0;i<valor.length();i+=2){
				String tmp = valor.substring(i,i+2);
				//if(numeros.indexOf(tmp.charAt(0))<0&&tmp.charAt(0)!='_'){
				if(numeros.indexOf(tmp.charAt(0))<0&&tmp.charAt(0)!=' '){
					error = true;
					break;
				}
				if(numeros.indexOf(tmp.charAt(1))<0){
					error = true;
					break;
				}
			}

		}catch(Exception e){
			error = true;
		}
		if(error){
			rg.setErrorLinea(true);
			rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo '"+nombreCampo+"' es incorrecto. \n");
		}
	}


	private void validaClaveMonto(int numLinea,String valor,String nombreCampo,ResultadosGarEducativo rg){
		boolean error = false;
		String numeros = "0123456789";
		try{

			for(int i=0;i<valor.length();i+=14){

				String tmp   = valor.substring(i,  i+2 );
				String monto = valor.substring(i+2,i+14);

				monto        = monto.replaceFirst("^[0]+",""); // Suprimir los ceros a la izquierda que no nos interesan,

				//errores.append("clave = "+tmp);
				//errores.append("\nmonto = "+monto+"\n");
				//if(numeros.indexOf(tmp.charAt(0))<0&&tmp.charAt(0)!='_'){
				if(numeros.indexOf(tmp.charAt(0))<0&&tmp.charAt(0)!=' '){
					error = true;
					break;
				}
				if(numeros.indexOf(tmp.charAt(1))<0){
					error = true;
					break;
				}

				// Validar que el monto sea un numero valido
				if( !monto.matches("[\\d]+[\\.]?") && !monto.matches("[\\d]*[\\.][\\d]+") ){
					error = true;
					break;
				}

				// Validar precision del monto
				String[] componentes  = monto.split("\\.",-1);
				String   parteEntera  = componentes.length > 0?componentes[0]:"";
				String   parteDecimal = componentes.length > 1?componentes[1]:"";
				if (        parteDecimal.length() > 2        ) { // 2 decimales
					//Error. El numero de decimales es mayor al especificado
					error = true;
					break;
				} else if ( parteEntera.length()  > 11 - 2   ) { // 9 enteros y no 10 porque se toma uno para el punto decimal
					//Error. El numero de enteros es mayor al permitido
					error = true;
					break;
				}


			}

		}catch(Exception e){
			error = true;
		}
		if(error){
			rg.setErrorLinea(true);
			rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo '"+nombreCampo+"' es incorrecto. \n");
		}
	}


	private void validaDestino(int numLinea,String valor,ResultadosGarEducativo rg){
		try{

			for(int i=0;i<valor.length();i+=11){

				String claveTipoDestino  		= valor.substring(i,  i+2);
				String porcentajeParcial 		= valor.substring(i+2,i+5);
				String porcentajeNacional		= valor.substring(i+5,i+8);
				String porcentajeImportacion 	= valor.substring(i+8,i+11);

				// Suprimir los guion a la izquierda
				claveTipoDestino = claveTipoDestino.replaceFirst("^[ ]","");

				// Validar Clave Tipo Destino
				if(!claveTipoDestino.matches("[\\d]+")){
					rg.setErrorLinea(true);;
					rg.appendErrores("Error en la linea:"+numLinea+" La clave del destino de los recursos es incorrecta \n");
					break;
				}
				// Validar Porcentaje Parcial
				if(!porcentajeParcial.matches("[\\d]+")){
					rg.setErrorLinea(true);;
					rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo Destino de los recursos es incorrecto. \n");
					break;
				}
				// Validar Porcentaje de origen nacional
				if(!porcentajeNacional.matches("[\\d]+")){
					rg.setErrorLinea(true);;
					rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo Destino de los recursos es incorrecto. \n");
					break;
				}
				// Validar porcentaje de importacion
				if(!porcentajeImportacion.matches("[\\d]+")){
					rg.setErrorLinea(true);;
					rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo Destino de los recursos es incorrecto. \n");
					break;
				}
				// Validar que la suma de porcentajes sea 100
				int sumatoriaPorcentajes = Integer.parseInt(porcentajeNacional) + Integer.parseInt(porcentajeImportacion);
				if( sumatoriaPorcentajes != 100 ){
					System.out.println("El valor de la suma de Porc Nacional y de Importacion = "+sumatoriaPorcentajes);
					rg.setErrorLinea(true);;
					rg.appendErrores("Error en la linea:"+numLinea+" La suma de Porcentaje Nacional y Porcentaje Importaci�n debe ser 100. \n");
					break;
				}

			}

		}catch(Exception e){
			rg.setErrorLinea(true);
			rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo Destino de los recursos es incorrecto. \n");
		}
	}


	/**
	 * Realiza la validacion de que el valor de &quot;campo&quot; sea una fecha
	 * en el formato DDMMAAAA
	 * @param numLinea Numero de linea que le corrersponde en el archivo que se esta procesando
	 * @param campo Valor a validar
	 * @param nombreCampo nombre del campo a validar
	 * @param rg Objeto de resultado de garantias.
	 */
	private void validaFechaEducativo(int numLinea,String campo,String nombreCampo,ResultadosGarEducativo rg) {
		int dia=0, mes=0, ano=0;
		try {
			if(campo != null && !campo.trim().equals("")) {
				dia = Integer.parseInt(campo.substring(0,2));
				mes = Integer.parseInt(campo.substring(2,4))-1;
				ano = Integer.parseInt(campo.substring(4,8));
				Calendar fecha = new GregorianCalendar(ano, mes, dia);
				int year = fecha.get(Calendar.YEAR);
				int month = fecha.get(Calendar.MONTH);
				int day = fecha.get(Calendar.DAY_OF_MONTH);

				if(campo.length()>8){
					rg.setErrorLinea(true);
					rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser una fecha valida en formato DDMMAAAA. \n");
				}	else if(!Comunes.esNumero(campo)) {
					rg.setErrorLinea(true);
					rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser una fecha valida en formato DDMMAAAA. \n");
				}

				if((dia!=day) || (mes!=month) || (ano!=year)){
					rg.setErrorLinea(true);
					rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser una fecha valida en formato DDMMAAAA. \n");
				}
			}
		}catch(Exception e) {
			rg.setErrorLinea(true);
			rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser una fecha valida en formato DDMMAAAA. \n");
		}
	}

	/**
	 * Realiza la validacion de que el valor de &quot;campo&quot; sea una fecha
	 * en el formato AAAAMMDD
	 * @param numLinea Numero de linea que le corrersponde en el archivo que se esta procesando
	 * @param campo Valor a validar
	 * @param nombreCampo nombre del campo a validar
	 * @param rg Objeto de resultado de garantias.
	 */
	private void validaFecha(int numLinea,String campo,String nombreCampo,ResultadosGarEducativo rg) {
		int dia=0, mes=0, ano=0;
		try {
			ano = Integer.parseInt(campo.substring(0,4));
			mes = Integer.parseInt(campo.substring(4,6))-1;
			dia = Integer.parseInt(campo.substring(6,8));
			Calendar fecha = new GregorianCalendar(ano, mes, dia);
			int year = fecha.get(Calendar.YEAR);
			int month = fecha.get(Calendar.MONTH);
			int day = fecha.get(Calendar.DAY_OF_MONTH);

			if(campo.length()>8){
				rg.setErrorLinea(true);
				rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser una fecha valida. \n");
			}	else if(!Comunes.esNumero(campo)) {
				rg.setErrorLinea(true);
				rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser una fecha valida. \n");
			}

			if((dia!=day) || (mes!=month) || (ano!=year)){
				rg.setErrorLinea(true);
				rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser una fecha valida. \n");
			}
		}catch(Exception e) {
			rg.setErrorLinea(true);
			rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser una fecha valida  en formato AAAAMMDD. \n");
		}
	}

	private void validaAnioTrimEducativo(int numLinea,String campo,String nombreCampo ,ResultadosGarEducativo rg){
		int ano=0;
		try{
			ano = Integer.parseInt(campo.trim());
			Calendar fecha = new GregorianCalendar(ano, 1, 1);
			int year = fecha.get(Calendar.YEAR);

			if(campo.length()>4){
				rg.setErrorLinea(true);
				rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser un a�o valido. \n");
			}	else if(!Comunes.esNumero(campo)) {
				rg.setErrorLinea(true);
				rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser un a�o valido. \n");
			}
			if((ano!=year)){
				rg.setErrorLinea(true);
				rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"'  debe ser un a�o valido. \n");
			}
		}catch(Exception e){
			rg.setErrorLinea(true);
			rg.appendErrores("Error en la linea:"+numLinea+" El valor del "+nombreCampo+" es incorrecto. \n");
		}
	}

	private void validaMoneda(int numLinea,String campo,ResultadosGarEducativo rg){
		try{
			campo = campo.trim();
			if(!"PESOS".equalsIgnoreCase(campo)&&!"DOLARES".equalsIgnoreCase(campo)){
					rg.setErrorLinea(true);
					rg.appendErrores("Error en la linea:"+numLinea+" El valor de la moneda debe ser PESOS o DOLARES. \n");
			}
		}catch(Exception e){
			rg.setErrorLinea(true);
			rg.appendErrores("Error en la linea:"+numLinea+" El valor de la moneda debe ser PESOS o DOLARES. \n");
		}
	}

	// Valida Campos Obligatorios SMJ 06/07/2006
	// Modified By JSHD 15/01/2013
	private boolean validaCampoObligatorio(int numLinea,String campo,String nombreCampo,ResultadosGarEducativo rg){

		boolean valido = true;
		if( campo == null || campo.matches("\\s*") ){
			rg.appendErrores("Error en la linea:"+numLinea+" El valor del campo "+nombreCampo+" es obligatorio. \n");
			rg.setErrorLinea(true);
			valido = false;
		}
		return valido;

	}

	/**
	 * Validacion del telefono de la pyme en formato de 18 digitos por cada telefono.
	 * (espacio 1 � 2 mas 10 digitos del telefono mas 6 digitos de la extension)
	 * @param numLinea - numero de la linea que se esta validando
	 * @param campo - valor que se validara
	 * @param nombreCampo - nombre del campo a validar
	 * @param ResultadosGarEducativo - objeto que guarda info de la validacion
	 * @since F059-2009
	 * @author Fabian Valenzuela/Gilberto Aparicio
	 */
	private void validarTelefonoPyme(int numLinea, String campo,
			String nombreCampo, ResultadosGarEducativo rg){
		try {

			log.info("validarTelefonoPyme(E)");

			final String TELEFONO_OFICINA = "1";
			final String TELEFONO_CELULAR = "2";

			int 		numTelefonos 			= campo.length() / 18;
			boolean 	hayTelefonoFijo 		= false;
			boolean 	hayTelefonoCelular	= false;

			for(int i=0; i< numTelefonos; i++){

				String claveTipoTelefono = campo.substring(i*18,   i*18+2 );
				String telefono			 = campo.substring(i*18+2, i*18+12);
				String extension         = campo.substring(i*18+12,i*18+18);

				// Suprimir los '_' a la izquierda.
				claveTipoTelefono = claveTipoTelefono.replaceFirst( "^[ ]+", "" );
				telefono				= telefono.replaceFirst(          "^[ ]+", "" );
				extension			= extension.replaceFirst(         "^[ ]+", "" );

				// Revisar el tipo de telefono especificado
				if(        TELEFONO_OFICINA.equals( claveTipoTelefono ) ){
					hayTelefonoFijo    = true;
				} else if( TELEFONO_CELULAR.equals( claveTipoTelefono ) ){
					hayTelefonoCelular = true;
				} else {
					throw new Exception("El identificador del telefono solo permite 1(Oficina) y 2(Celular)");
				}

				if(        !claveTipoTelefono.matches("[\\d]+") ){
					throw new Exception("Error de formato.");
				} else if( !telefono.matches("[\\d]+")          ){
					throw new Exception("Error de formato.");
				} else if( TELEFONO_CELULAR.equals(claveTipoTelefono) && !"".equals(extension) ){
					throw new Exception("Para los telefonos celulares, se requiere que la extension venga en \"blanco\".");
				} else if( !extension.matches("[\\d]*")         ){
					throw new Exception("Error de formato.");
				}

			}

			//if( !hayTelefonoFijo || !hayTelefonoCelular ){
			//	throw new Exception("Se deben registrar al menos dos Telefonos (Oficina y Celular).");
			if( !hayTelefonoFijo && !hayTelefonoCelular ){
				throw new Exception("Se debe registrar al menos un Tel�fono, ya sea de Oficina � Celular.");
			}

		} catch (Exception e) {
			log.error("validarTelefonoPyme(Exception)");
			rg.appendErrores("Error en la linea:"+numLinea+" el campo '"+nombreCampo+"' no es correcto. " + e.getMessage() + "\n");
			rg.setErrorLinea(true);
		} finally {
			log.info("validarTelefonoPyme(S)");
		}
	}

	/**
	 * Validacion del formato del correo electronico de la pyme.
	 * @param numLinea - numero de la linea que se esta validando
	 * @param campo - valor que se validara
	 * @param nombreCampo - nombre del campo a validar
	 * @param ResultadosGarEducativo - objeto que guarda info de la validacion
	 * @since F059-2009
	 * @author Fabian Valenzuela
	 */
	private void validarCorreo(int numLinea, String campo,
			String nombreCampo, ResultadosGarEducativo rg){
		try {
			log.info("validarCorreo(E)");
			if(campo != null && !"".equals(campo.trim()) ) {
				if(!Comunes.validaEmail(campo))
					throw new Exception();
			}
			if(campo.length()>40){
				throw new Exception();
			}
		} catch (Exception e) {
			log.error("validarCorreo(Exception)");
			rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo+"' no es valido  \n");
			rg.setErrorLinea(true);
		} finally {
			log.info("validarCorreo(S)");
		}
	}


	/**
	 * Validacion de la dependencia.
	 * @param numLinea - numero de la linea que se esta validando
	 * @param campo - valor que se validara
	 * @param nombreCampo - nombre del campo a validar
	 * @param ResultadosGarEducativo - objeto que guarda info de la validacion
	 * @since F025-2011
	 * @author Salim Hernandez
	 */
	private void validarDependencia(int numLinea, String campo, String nombreCampo, ResultadosGarEducativo rg){

		log.info("validarDependencia(E)");
		String msg_error = "";

		try {

			if(campo != null && !"".equals(campo.trim())) {
				msg_error=" no es un numero valido ";
				Long.parseLong(campo.trim());
			}

			if(campo != null && !"".equals(campo.trim()) && campo.length()>4){
				msg_error=" no es valido; longitud max(4) ";
				throw new Exception();
			}

		} catch (Exception e) {

			log.error("validarDependencia(Exception)");
			rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo +"'"+ msg_error+" \n");
			rg.setErrorLinea(true);

		} finally {
			log.info("validarDependencia(S)");
		}
	}

	/**
	 *
	 * Revisa si el String proporcionado en el parametro: <tt>campo</tt> corresponde a un espacio en blanco o
	 * a una cadena vac�a.
	 *
	 * @param campo <tt>String</tt> a validar.
	 * @return <tt>boolean</tt> con valor <tt>true</tt> si se trata de un campo vac�o;
	 *         regresa <tt>false</tt> en caso contrario.
	 *
	 * @since Fodea 004 - 2013 ( 11/03/2013 06:19:12 p.m. )
	 * @author Salim Hernandez
	 *Copiado por HVC el 14-03-2013
	 */
	private boolean esVacio(String campo){
		log.info("esVacio(E)");
		boolean espacioEnBlanco = false;
		if( campo == null || campo.matches("\\s*") ){
			espacioEnBlanco = true;
		}
		log.info("esVacio(S)");
		return espacioEnBlanco;
	}

	/**
	 *
	 * Este m�todo valida que el valor del campo proporcionado venga vac�o:
	 * con cadena vac�a o con espacios en blanco.
    *
	 * @param campo <tt>String</tt> con el contenido del campo a validar.
	 * @since Fodea 003,004 - 2013 ( 12/03/2013 03:54:32 p.m. )
	 * @author Salim Hernandez
	 * Copiado por HVC el 14-03-2013
	 */
	 private void validarCampoRequeridoVacio(int numLinea, String campo, String nombreCampo, ResultadosGarEducativo rg){

		log.info("validarCampoRequeridoVacio(E)");

		String 	msg_error 	= "";
		boolean 	hayError 	= false;

		if( campo != null && !campo.matches("\\s*") ){
			msg_error="El campo "+nombreCampo+" no requiere informaci�n, por lo que debe venir vac�o. ";
			hayError = true;
		}

		if(hayError){
			rg.appendErrores("Error en la linea:"+numLinea+" "+ msg_error+"\n");
			rg.setErrorLinea(true);
		}

		log.info("validarCampoRequeridoVacio(S)");

	}

	/**
	 * M�todo que se encarga de obtener de las tablas del SIAG la clave de contragarante
	 * asociado a un intermediario financiero.
	 * @param claveIf Clave interna del intermediario financiero.
	 * @return claveContragarante Clave del contragarante.
	 * @throws AppException Cuando ocurre un error en la consulta.
	 */
	private int getClaveContragarante(String claveIf) throws AppException{
		log.info("getClaveContragarante(E) ::..");
		AccesoDB con = new AccesoDB();
		PreparedStatement pst = null;
		ResultSet rst = null;
		StringBuffer strSQL = new StringBuffer();
		List varBind = new ArrayList();
		boolean transactionOk = true;
		int claveContragarante = 0;
		try {
			con.conexionDB();

			strSQL.append(" SELECT fso_clave AS clave_contragarante");
			strSQL.append(" FROM siag_fiso_intermediarios sfi");
			strSQL.append(" , comcat_if cif");
			strSQL.append(" WHERE sfi.clave = cif.ic_if_siag");
			strSQL.append(" And cif.ic_if = ?");

			varBind.add(new Integer(claveIf));

			log.debug("..:: strSQL: "+strSQL.toString());
			log.debug("..:: varBind: "+varBind);

			pst = con.queryPrecompilado(strSQL.toString(), varBind);
			rst = pst.executeQuery();
			if (rst.next()) {
				claveContragarante = rst.getInt("clave_contragarante");
			}
			rst.close();
			pst.close();
		} catch(Exception e) {
			transactionOk = false;
			e.printStackTrace();
			throw new AppException("getClaveContragarante(ERROR) ::..", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();
			}
			log.info("getClaveContragarante(S) ::..");
		}
		return claveContragarante;
	}


	/**
	 * Obtiene el numero de folio a partir de la clave de if especificada
	 * Tambien obtiene la clave siag para el IF, y el numero electronico
	 * @throws com.netro.exception.NafinException
	 * @return Objeto Registros con los datos del folio, N@E, SIAG, Fecha, Hora, A�o, trimestre
	 */
	private Registros getDatosFolio(String claveIF){
		return getDatosFolio(claveIF, false, "0");
	}

	/** Esta funcion devuelve la fecha y hora del dia actual, si a la hora de hacer
	 *  la consulta esta es menor o igual a la hora parametrizada (siag_cat_tipooperacion.hora_limite_desem), en caso contrario
	 *  devolvera la fecha del dia siguiente con la hora actual.
	 *	 @throws com.netro.exception.NafinException
	 *	 @return Una cadena de texto con el siguiente formato de fechas de Oracle:
	 *   			'DD/MM/YYYY HH24:MI'
	 *  @author JSHD
	 */
	public Hashtable getFechaConAdecuaciones(String sTipoOperacion) {
		log.info("CreditoEducativoBean::getFechaConAdecuaciones(E)");

		String 	FECHA 							= "";
		boolean  calcularDiaHabilSiguiente 	= false;
    Hashtable htFechas = new Hashtable();

		AccesoDB con = new AccesoDB();
		String qrySentencia =
					"SELECT 										" +
					"	CASE WHEN 									" +
					"		SYSDATE > to_date(to_char((select sysdate from DUAL_SIAG), 'dd/mm/yyyy ') || to_char(hora_limite_desem, 'hh24:mi:ss'), 'dd/mm/yyyy hh24:mi:ss')  	" +
					"	THEN 										" +
					"		'true' 									" +
					"  ELSE											" +
					"		'false'									" +
					"	END 										" +
					"		AS	CALCULAR_DIA_HABIL_SIGUIENTE,	" +
					"  TO_CHAR(SYSDATE,'DD/MM/YYYY')			" +
					"		AS FECHA, 							" +
					"  TO_CHAR(SYSDATE,'HH24:MI')				" +
					"		AS HORA 							" +
					" FROM siag_cat_tipooperacion  				" +
					" WHERE TIPO_OPERACION = ?";

		try {
			con.conexionDB();
			List varBind = new ArrayList();
			varBind.add(sTipoOperacion);
			Registros reg = con.consultarDB(qrySentencia, varBind);

			if(reg != null && reg.next()){
				calcularDiaHabilSiguiente 	= (reg.getString("CALCULAR_DIA_HABIL_SIGUIENTE") != null && reg.getString("CALCULAR_DIA_HABIL_SIGUIENTE").equals("true"))?true:false;
				FECHA 							= reg.getString("FECHA");

				if(calcularDiaHabilSiguiente){
					FECHA = Fecha.sumaFechaDiasHabilesSIAG(FECHA, "dd/MM/yyyy", 1);
				}

				//FECHA_Y_HORA = FECHA + " " + HORA;
        htFechas.put("FECHA_CARGA", reg.getString("FECHA"));
        htFechas.put("HORA_CARGA", reg.getString("HORA"));
        htFechas.put("FECHA_VALOR", FECHA);
			}

		}catch(Exception e) {
			log.error("CreditoEducativoBean::getFechaConAdecuaciones(Exception)");
			throw new AppException("Error al obtener la fecha valor", e);
		} finally {
			if(con.hayConexionAbierta()){
        con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			log.info("CreditoEducativoBean::getFechaConAdecuaciones(S)");
		}
		return htFechas;
	}

	/**
	 * Obtiene el siguiente valor consecutivo para formar el folio de la solicitud
	 * La funcionalidad, ser� EQUIVALENTE a la usada mediante secuencias de Oracle.
	 * Es decir, una vez que se regrese el valor por medio de este metodo en la
	 * tabla ser� actualizado el valor, con la finalidad de que no sea
	 * reusado el numero
	 *
	 * Esta implementaci�n maneja los consecutivos por d�a, es decir cada
	 * d�a empezara con el numero de folio 1
	 *
	 * @return Cadena con el numero de consecutivo del d�a.
	 */
	private String getConsecutivo() throws Exception {
    return getConsecutivo("");

	}

	private String getConsecutivo(String fecha) throws Exception {
		AccesoDB con = new AccesoDB();
		boolean exito = false;

		try {
			con.conexionDB();
			String strSQL =
					" SELECT MAX(ig_consecutivo) + 1 as consecutivo " +
					" FROM gti_consecutivo_folio ";
      if ("".equals(fecha)){
        strSQL += " WHERE dc_consecutivo = trunc(sysdate) ";
      } else {
        strSQL += " WHERE dc_consecutivo = to_date('"+fecha+"', 'dd/mm/yyyy') ";
      }

			String consecutivo = "";
			Registros reg = con.consultarDB(strSQL);
			if(reg.next()) {
				consecutivo = reg.getString("consecutivo");
			}
			if (consecutivo.equals("")) {
				String strSQLInsert =
						" INSERT INTO gti_consecutivo_folio (dc_consecutivo, ig_consecutivo) ";
        if ("".equals(fecha)){
					strSQLInsert += " VALUES (trunc(sysdate),?) ";
        } else {
          strSQLInsert += " VALUES (to_date('"+fecha+"', 'dd/mm/yyyy'),?) ";
        }
				consecutivo = "1";
				List varBind = new ArrayList();
				varBind.add(new BigDecimal(consecutivo));
				con.ejecutaUpdateDB(strSQLInsert, varBind);
			} else {
				String strSQLUpdate =
						" UPDATE gti_consecutivo_folio " +
						" SET ig_consecutivo = ? ";
          if ("".equals(fecha)){
						strSQLUpdate += " WHERE dc_consecutivo = trunc(sysdate) ";
          } else {
            strSQLUpdate += " WHERE dc_consecutivo = to_date('"+fecha+"', 'dd/mm/yyyy') ";
          }
				List varBind = new ArrayList();
				varBind.add(new BigDecimal(consecutivo));
				con.ejecutaUpdateDB(strSQLUpdate, varBind);
			}
			exito = true;
			return consecutivo;

		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}

	/**
	 * Obtiene el numero de folio a partir de la clave de if especificada
	 * Tambien obtiene la clave siag para el IF, y el numero electronico.
	 * @param usarCorreccionDeFecha 	indica si la fecha sera modificada
	 *											para que se tome la del dia siguiente,
	 *											en caso de que la fecha del sistema a la
	 *											hora de realizar la consulta supere la
	 *											hora parametrizada por tipo de operacion.
	 * @throws com.netro.exception.NafinException
	 * @return Objeto Registros con los datos del folio, N@E, SIAG, Fecha, Hora, A�o, trimestre
	 *
	 */
	private Registros getDatosFolio(String claveIF, boolean usarCorreccionDeFecha, String tipoOperacion) {
		log.info("CreditoEducativoBean::getFolio(E)");
		AccesoDB con 				= new AccesoDB();
		Hashtable htFechas 	= new Hashtable();
		String 	qrySentencia 	= "";

		try {
			if(usarCorreccionDeFecha){
				htFechas 	= getFechaConAdecuaciones(tipoOperacion);
				qrySentencia 	=
					"SELECT cn.ic_nafin_electronico "+
					" ,cn.ic_nafin_electronico || TO_CHAR(to_date(?,'DD/MM/YYYY'),'yyyymmdd') || ? AS FOLIO"+
					" ,i.ic_if_siag"+
					" ,? as FECHA_VALOR"+
					" ,? as HORA"+
					" ,TO_CHAR(to_date(?,'DD/MM/YYYY'),'yyyy') as ANIO_VALOR"+
					" ,case"+
					"	when to_char(to_date(?,'DD/MM/YYYY'),'mm') in('01','02','03') then 1"   +
					"	when to_char(to_date(?,'DD/MM/YYYY'),'mm') in('04','05','06') then 2"   +
					"	when to_char(to_date(?,'DD/MM/YYYY'),'mm') in('07','08','09') then 3"   +
					"	when to_char(to_date(?,'DD/MM/YYYY'),'mm') in('10','11','12') then 4"   +
					"  end as TRIMESTRE"  +
					" ,? as FECHA"+
					" ,TO_CHAR(to_date(?,'DD/MM/YYYY'),'yyyy') as ANIO"+
					" FROM " +
					"	COMREL_NAFIN 	CN, "+
					" 	COMCAT_IF 		I   "+
					" WHERE " +
					"	CN.IC_EPO_PYME_IF = I.IC_IF 	AND "+
					" 	CN.CG_TIPO 			= ? 			AND "+
					"  CN.IC_EPO_PYME_IF = ?";
			}else{
				qrySentencia 	=
					"SELECT cn.ic_nafin_electronico "+
					" ,cn.ic_nafin_electronico || TO_CHAR(sysdate,'yyyymmdd') || ? AS FOLIO"+
					" ,i.ic_if_siag"+
					" ,TO_CHAR(sysdate,'dd/mm/yyyy') as FECHA"+
					" ,TO_CHAR(sysdate,'HH24:MI') as HORA"+
					" ,TO_CHAR(sysdate,'yyyy') as ANIO"+
					" ,case"+
					"	when to_char(sysdate,'mm') in('01','02','03') then 1"   +
					"	when to_char(sysdate,'mm') in('04','05','06') then 2"   +
					"	when to_char(sysdate,'mm') in('07','08','09') then 3"   +
					"	when to_char(sysdate,'mm') in('10','11','12') then 4"   +
					"  end as TRIMESTRE"  +
					" FROM COMREL_NAFIN CN"+
					" ,COMCAT_IF I"+
					" WHERE CN.ic_epo_pyme_if = i.ic_if"+
					" and CN.cg_tipo = ?"+
					" and CN.ic_epo_pyme_if = ?";
			}


				log.debug("qrySentencia--->"+qrySentencia);


			con.conexionDB();

			List varBind = new ArrayList();

			if(usarCorreccionDeFecha){
				varBind.add(new String(htFechas.get("FECHA_VALOR").toString()));
				varBind.add(this.getConsecutivo(htFechas.get("FECHA_VALOR").toString()));
				varBind.add(new String(htFechas.get("FECHA_VALOR").toString()));
				varBind.add(new String(htFechas.get("HORA_CARGA").toString()));
				varBind.add(new String(htFechas.get("FECHA_VALOR").toString()));
				varBind.add(new String(htFechas.get("FECHA_VALOR").toString()));
				varBind.add(new String(htFechas.get("FECHA_VALOR").toString()));
				varBind.add(new String(htFechas.get("FECHA_VALOR").toString()));
				varBind.add(new String(htFechas.get("FECHA_VALOR").toString()));
				varBind.add(new String(htFechas.get("FECHA_CARGA").toString()));
				varBind.add(new String(htFechas.get("FECHA_CARGA").toString()));
				varBind.add("I");
				varBind.add(new Integer(claveIF));
			}else{
				varBind.add(this.getConsecutivo());
				varBind.add("I");
				varBind.add(new Integer(claveIF));
			}

			Registros reg = con.consultarDB(qrySentencia, varBind);

			log.debug("varBind--->"+varBind);

			return reg;
		}catch(Exception e) {
			log.error("CreditoEducativoBean::getFolio(). Exception: " + e.getMessage());
			throw new AppException("Error al obtener el folio de la solicitud", e);
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("CreditoEducativoBean::getFolio(S)");
		}
	}

	private int getInt(String valor){
		return (valor==null?0:Integer.parseInt(valor.trim()));
	}
	private String getCadena(String valor){
		return (valor == null?"":valor);
	}
	private BigDecimal getBigDecimal(String valor){
		return (valor==null||("".equals(valor))?new BigDecimal("0.00"):new BigDecimal(valor.trim()));
	}

	private boolean validaTipoPrograma(int numLinea, String campo, String nombreCampo, String claveIf,	ResultadosGarEducativo rg){

		log.info("validaTipoPrograma(E)");
		String msg_error = " no es valido; ";
		AccesoDB con = new AccesoDB();
		boolean perdida = false;

		try {

			con.conexionDB();

			String qrySentencia =
					" SELECT ic_clave, cs_opera_credito, ic_if, cg_modalidad "+
					" FROM cecat_bases_opera "+
					" WHERE ic_clave = ? ";

			List varBind = new ArrayList();
			varBind.add(new Integer(campo.trim()));

			Registros reg = con.consultarDB(qrySentencia, varBind);

			String cs_opera_credito	=	"";
			String ic_if				=	"";
			String cg_modalidad		=	"";

			if(reg.next()){
				cs_opera_credito	= reg.getString("cs_opera_credito");
				ic_if					= reg.getString("ic_if");
				cg_modalidad		= reg.getString("cg_modalidad");
			}else{
				msg_error += " (Base de Operaci�n) no est� registrada para el Intermediaro Financiero.";
				throw new Exception();
			}
			if("N".equals(cs_opera_credito)){
				msg_error += " (Base de Operaci�n) no opera bajo la modalidad de Cr�dito Educativo.";
				throw new Exception();
			}
			if(!claveIf.equals(ic_if)){
				msg_error += " (Base de Operaci�n) no est� registrada para el Intermediaro Financiero.";
				throw new Exception();
			}

			if(cg_modalidad.equals("2")){
				perdida = true;
			}

		} catch (Exception e) {

			log.error("validaTipoPrograma(Exception)");
			rg.appendErrores("Error en la linea:"+numLinea+" El campo '"+nombreCampo +"'"+ msg_error+" \n");
			rg.setErrorLinea(true);

		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("validaTipoPrograma(S)");
		}
		return true;
	}

	private void validaPrimerasPerdidas(int numLinea, String claveFin, String noDispo, String ic_if_siag,	ResultadosGarEducativo rg){
		log.info("validaPrimerasPerdidas(E)");
		String msg_error = "";
		Registros reg = new Registros();
		List varBind = new ArrayList();
		AccesoDB con = new AccesoDB();
		boolean flagContinue = true;
		String qrySentencia = "";
		int contador_b = 0;

		try {

			con.conexionDB();

			qrySentencia =
				"SELECT   cce.cg_financiamiento,	cce.ic_num_disposicion, "+
				"         cce.ic_folio_operacion, cce.ic_estatus_cred_educa, cg_aceptado_siag, "+
				"         (SELECT gti.ic_estatus_cred_educa "+
				"            FROM gti_estatus_solic gti "+
				"           WHERE gti.ic_folio = cce.ic_folio_operacion  "+
				"             AND gti.cg_programa = ? ) AS estatus_gti,  "+
				"        (SELECT gtii.ic_situacion "+
				"				FROM gti_estatus_solic gtii "+
				"				WHERE gtii.ic_folio = cce.ic_folio_operacion  "+
				"				AND gtii.cg_programa = ? ) AS ic_situacion, "+
				" 			cce.fn_fecha_carga "+
				" FROM     ce_contenido_educativo cce "+
				"   WHERE cce.cg_financiamiento = ? "+
				"   AND cce.ic_if_siag = ? "+
				" ORDER BY cce.fn_fecha_carga DESC ";

			log.debug("validaPrimerasPerdidas.Sentencia: " + qrySentencia);

			varBind = new ArrayList();
			varBind.add("E");
			varBind.add("E");
			varBind.add(claveFin);
			varBind.add(ic_if_siag);

			reg = new Registros();
			reg = con.consultarDB(qrySentencia, varBind);

			int contador = 0;
			while(reg.next()){
				String num_dispo					= (reg.getString("ic_num_disposicion")   ==null)?"0":reg.getString("ic_num_disposicion");
				String estatus_gti				= (reg.getString("estatus_gti")          ==null)?"" :reg.getString("estatus_gti");
				String ic_estatus_cred_educa	= (reg.getString("ic_estatus_cred_educa")==null)?"" :reg.getString("ic_estatus_cred_educa");
				String cg_aceptado_siag			= (reg.getString("cg_aceptado_siag")     ==null)?"" :reg.getString("cg_aceptado_siag");
				String ic_situacion				= (reg.getString("ic_situacion")         ==null)?"" :reg.getString("ic_situacion");

				if (	num_dispo.equals(noDispo)	){	//En el caso en que se desee guardar una misma Disposici�n.

					if(	estatus_gti.equals("3")	||	estatus_gti.equals("5")	||	estatus_gti.equals("7")	){	//Se verifica si el estatus en gti es rechazado . . .
						contador++;
						break;
					}else{
						if(	ic_estatus_cred_educa.equals("3")	&&	estatus_gti.equals("")	){
							contador++;
							break;
						}
						if(	(estatus_gti.equals("2")	||	estatus_gti.equals("4")	||	estatus_gti.equals("6"))	&&	cg_aceptado_siag.equals("N")	){
							contador++;
							break;
						}
						msg_error += " La disposici�n que se desea cargar para el Cr�dito: "+claveFin+" ya se registr� en el sistema. Favor de verificar. ";
						throw new Exception();
					}

				} else{
					//se verificar� que dicha disposici�n se trate de una posterior a la �ltima registrada en el sistema
					int old_disposicion = Integer.parseInt(num_dispo);
					int new_disposicion = Integer.parseInt(noDispo);

					int tot = new_disposicion - old_disposicion;

					if(tot != 1){
						msg_error += " Favor de verificar que el n�mero de disposici�n sea posterior a la �ltima registrada en el sistema";
						throw new Exception();
					}

					//Verificamos que se cargue disposicion 2
					if(	!"2".equals(noDispo)	){

						//se verifica que la disposici�n anterior haya sido aceptada por el SIAG y que se encuentre con el estatus �CONTRAGARANTIA ACEPTADA"
						if(	!estatus_gti.equals("6")	){
							msg_error += " La disposici�n que se desea cargar para el Cr�dito: "+claveFin+" no se puede cargar. verifique que la anterior haya sido aceptada. ";
							throw new Exception();
						}

						if(	estatus_gti.equals("6")	&&	cg_aceptado_siag.equals("N")	){
							msg_error += " La disposici�n que se desea cargar para el Cr�dito: "+claveFin+" no se puede cargar. verifique que la anterior haya sido aceptada. ";
							throw new Exception();
						}

					}else{

						if (cg_aceptado_siag.equals("N")){
							msg_error += " La disposici�n que se desea cargar para el Cr�dito: "+claveFin+" no se puede cargar. verifique que la anterior haya sido aceptada por el SIAG. ";
							throw new Exception();
						}else if(	cg_aceptado_siag.equals("S")	){
							if(	ic_estatus_cred_educa.equals("1")   || (	ic_estatus_cred_educa.equals("2") &&	estatus_gti.equals("2") && ic_situacion.equals("1")	)	){
								msg_error += " La disposici�n que se desea cargar para el Cr�dito: "+claveFin+" no se puede cargar. verifique que la anterior haya sido aceptada. ";
								throw new Exception();
							}
						}
					}
				}
				contador++;
				flagContinue = false;	//Si entra en este bloque, no continuar...
				break;
			}

			if(flagContinue){

				qrySentencia = "SELECT ic_num_disposicion FROM ce_contenido_aux WHERE ic_if_siag = ? AND cg_financiamiento = ? ";

				varBind = new ArrayList();
				varBind.add(ic_if_siag);
				varBind.add(claveFin);
				reg = con.consultarDB(qrySentencia, varBind);

				while(reg.next()){
					String num_dispo_b	= (reg.getString("ic_num_disposicion")==null)?"0":reg.getString("ic_num_disposicion");
					int old_disposicion_b= Integer.parseInt(num_dispo_b);
					int new_disposicion_b= Integer.parseInt(noDispo);

					int tot_b = new_disposicion_b - old_disposicion_b;

					if(tot_b != 1){
						msg_error += "Favor de verificar que el n�mero de disposici�n sea posterior a la �ltima: "+num_dispo_b;
						throw new Exception();
					}
					contador_b++;
					break;
				}
			}//End- flagContinue

			if(contador == 0 && contador_b == 0){
				if(	!"1".equals(noDispo)	){
					msg_error += " La disposici�n que se desea cargar para el Cr�dito: "+claveFin+" no se puede cargar. verifique que la anterior haya sido aceptada. ";
					throw new Exception();
				}
			}

		} catch (Exception e) {

			log.error("validaPrimerasPerdidas(Exception)");
			rg.appendErrores("Error en la linea: "+numLinea + msg_error+" \n");
			rg.setErrorLinea(true);

		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("validaPrimerasPerdidas(S)");
		}
	}

/**
	 * pantalla Parametrizaci�n IF- Universidad
	 * @return
	 * @param universidades
	 * @param ic_if
	 */
public String paramIF_Univ(String ic_if , String universidades[] ){
	log.info("paramIF_Univ (E)");
	AccesoDB con =new AccesoDB();
	StringBuffer  strSQL = new StringBuffer();
	StringBuffer  strSQLCos = new StringBuffer();
	String respuesta ="Fallo", existe ="0";
	List		lVarBind		= new ArrayList();
	List		lVarBindCos		= new ArrayList();
	PreparedStatement ps	= null;
	ResultSet  rs = null;


	try{
		con.conexionDB();


		for(int i=0; i < universidades.length; i++) {

			Vector lovDatos = new Vector();
			lovDatos = Comunes.explode(",", universidades[i]);
			String univ	= (String) lovDatos.get(0);

			// se verifica si existe relaci�n  entre el IF y la Universidad
			strSQLCos = new StringBuffer();
			strSQLCos.append( " select count(*)  EXISTE  from  cerel_if_universidad "+
							" where ic_if = ?  "+
							" and ic_universidad= ? ");
			lVarBindCos		= new ArrayList();
			lVarBindCos.add(ic_if);
			lVarBindCos.add(univ);

			ps = con.queryPrecompilado(strSQLCos.toString(), lVarBindCos);
			rs = ps.executeQuery();
			if(rs.next()) {
				existe= rs.getString("EXISTE")==null?"0":rs.getString("EXISTE");
			}
			ps.close();
			rs.close();

			if(existe.equals("0"))  {

				// se agregan todos los registros relacionados con el IF
					strSQL = new StringBuffer();
					strSQL.append( " Insert into  cerel_if_universidad "+
										"(ic_if , ic_universidad, df_operacion) "+
										" values( ? , ? , Sysdate ) ");

				lVarBind		= new ArrayList();
				lVarBind.add(ic_if);
				lVarBind.add(univ);

				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();
			}
		}//for

	} catch(Exception e) {
			respuesta ="Fallo";
		e.printStackTrace();
		log.error("error en la parametrizaci�n de relacion IF - Unversidades " +e);
		throw new AppException("paramIF_Univ(Exception) ", e);
	} finally {
		respuesta ="Correcto";
		con.terminaTransaccion(true);
		if(con.hayConexionAbierta())  con.cierraConexionDB();
	}
		log.info("paramIF_Univ (S)");
		return respuesta;
}

/**
	 * Pantalla Parametrizaci�n IF- Universidad
	 * @return
	 * @param universidad
	 * @param ic_if
	 */
public String paramUniv_IF(String ic_if [] , String universidad ){
	log.info("paramUniv_IF (E)");
	AccesoDB con =new AccesoDB();
	StringBuffer  strSQL = new StringBuffer();
	StringBuffer  strSQLCos = new StringBuffer();

	String respuesta ="Fallo", existe ="0";
	List		lVarBind		= new ArrayList();
	List		lVarBindCos		= new ArrayList();
	PreparedStatement ps	= null;
	ResultSet  rs = null;

	try{
		con.conexionDB();


		for(int i=0; i < ic_if.length; i++) {
			Vector lovDatos = new Vector();
			lovDatos = Comunes.explode(",", ic_if[i]);
			String icif	= (String) lovDatos.get(0);

			// se verifica si existe relaci�n  entre el IF y la Universidad
			strSQLCos = new StringBuffer();
			strSQLCos.append( " select count(*)  EXISTE  from  cerel_if_universidad "+
							" where ic_if = ?  "+
							" and ic_universidad= ? ");
			lVarBindCos		= new ArrayList();
			lVarBindCos.add(icif);
			lVarBindCos.add(universidad);

			ps = con.queryPrecompilado(strSQLCos.toString(), lVarBindCos);
			rs = ps.executeQuery();
			if(rs.next()) {
				existe= rs.getString("EXISTE")==null?"0":rs.getString("EXISTE");
			}
			ps.close();
			rs.close();

			if(existe.equals("0"))  {

				// se agregan todos los registros relacionados con el IF
				strSQL = new StringBuffer();
				strSQL.append( " Insert into  cerel_if_universidad "+
									"(ic_if , ic_universidad, df_operacion) "+
									" values( ? , ? , Sysdate ) ");
					lVarBind		= new ArrayList();
					lVarBind.add(icif);
					lVarBind.add(universidad);

					ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
					ps.executeUpdate();
					ps.close();
			}
		}

	} catch(Exception e) {
			respuesta ="Fallo";
		e.printStackTrace();
		log.error("error en la parametrizaci�n de relacion Unversidades  - IF  " +e);
		throw new AppException("insertSolicitud(Exception) ", e);
	} finally {
			respuesta ="Correcto";
			con.terminaTransaccion(true);
		if(con.hayConexionAbierta())  con.cierraConexionDB();
	}
		log.info("paramUniv_IF (S)");
		return respuesta;
}


	/**
	 *	verifica que clave de B.O. no exista en la base
	 * @return
	 * @param ic_operacion
	 */
	public String existeBO(String  ic_operacion){
		log.info("existeBO (E)");
		AccesoDB con =new AccesoDB();
		StringBuffer  strSQL = new StringBuffer();
		String respuesta ="Fallo", existe="";
		List		lVarBind		= new ArrayList();
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		try{
			con.conexionDB();

			strSQL = new StringBuffer();
			strSQL.append( " select count(*) as EXISTE from  CECAT_BASES_OPERA   where IC_CLAVE = ?  ");
			lVarBind		= new ArrayList();
			lVarBind.add(ic_operacion);
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			rs = ps.executeQuery();
			if(rs.next()) {
				existe= rs.getString("EXISTE")==null?"":rs.getString("EXISTE");
			}
			ps.close();
			rs.close();
			if(!existe.equals("0"))  {	respuesta ="Existe";   }

			System.out.println("existe  "+existe);
			System.out.println("ic_operacion  "+ic_operacion);

		} catch(Exception e) {
			e.printStackTrace();
			log.error("error verifica que clave de B.O. no exista en la base  " +e);
			throw new AppException("insertSolicitud(Exception) ", e);
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())  con.cierraConexionDB();
		}
			log.info("existeBO (S)");
			return respuesta;
	}


/**
	 * Agrega la parametrizaci�n de la Base de Operaci�n
	 * @return
	 * @param parametros
	 */
	public String agregarBO(List parametros){

		log.info("agregarBO (E)");

		AccesoDB con         = new AccesoDB();
		StringBuffer strSQL  = new StringBuffer();
		String respuesta     = "Fallo";
		List lVarBind        = new ArrayList();
		PreparedStatement ps = null;

		try{
			con.conexionDB();

			String ic_if        =  parametros.get(0).toString();
			String ic_operacion =  parametros.get(1).toString();
			String descripcion  =  parametros.get(2).toString();
			String operaCredito =  parametros.get(3).toString();
			String porcentaje   =  parametros.get(4).toString();
			String modalidad    =  parametros.get(5).toString();
			String usuario      =  parametros.get(6).toString();
			String universidad  =  parametros.get(9).toString(); //Fodea 44

			respuesta = existeBO(ic_operacion);

			if(!respuesta.equals("Existe")){

				strSQL = new StringBuffer();
				strSQL.append(" Insert into  CECAT_BASES_OPERA ");
				strSQL.append("(IC_BASE_OPERACION, IC_IF, IC_CLAVE, CG_DESCRIPCION, CS_OPERA_CREDITO, FG_PORCENTAJE_APORTACION, CG_MODALIDAD , CS_ACTIVO, CG_USUARIO, DF_ALTA, IC_UNIVERSIDAD ) ");
				strSQL.append("values( SEQ_CECAT_BASES_OPERA.nextval,  ? , ? , ? , ? , ? , ?, ?, ?, Sysdate, ? )");

				lVarBind = new ArrayList();
				lVarBind.add(ic_if);
				lVarBind.add(ic_operacion);
				lVarBind.add(descripcion);
				lVarBind.add(operaCredito);
				lVarBind.add(porcentaje);
				lVarBind.add(modalidad);
				lVarBind.add("A"); //Activo
				lVarBind.add(usuario);
				lVarBind.add(universidad); // Fodea 44

				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();
				respuesta = "Correcto";
			}

		} catch(Exception e){
			respuesta = "Fallo";
			e.printStackTrace();
			con.terminaTransaccion(true);
			log.error("Error al dar de alta la Base de Operaci�n. " +e);
			throw new AppException("agregarBO(Exception) ", e);
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		log.info("agregarBO (S)");
		return respuesta;
	}

		/**
		 * realiza consulta para editar  la parametrizaci�n de la Base de Operaci�n
		 * @return
		 * @param ic_operacion
		 */

	public List consBaseOperacion(String ic_operacion, String icUniversidad){

		log.info("consBaseOperacion (E)");

		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs         = null;
		StringBuffer strSQL  = new StringBuffer();
		List lVarBind        = new ArrayList();
		List respuesta       = new ArrayList();
		String nombreIF      = "";
		String claveOpera    = "";
		String descripcion   = "";
		String operaCredito  = "";
		String porcentaje    = "";
		String modalidad     = "";
		String consecutivo   = "";
		String universidad   = "";

		try{
			con.conexionDB();

			strSQL = new StringBuffer();
/*
			strSQL.append( " SELECT bo.ic_base_operacion  as CONSECUTIVO , "+
								" bo.ic_clave  as CLAVE , " +
								" i.cg_razon_social as NOMBRE_IF,  " +
								" bo.cg_descripcion as DESCRIPCION ,  " +
								" bo.cs_opera_credito as OPERA_CREDITO , " +
								" bo.fg_porcentaje_aportacion as POR_APORTACION , " +
								" bo.cg_modalidad  as MODALIDAD   " +
								" from cecat_bases_opera bo," +
								" comcat_if i " +
								" where i.ic_if  = bo.ic_if" +
								" and  bo.ic_base_operacion   =?  ");
*/
			strSQL.append(" SELECT BO.IC_BASE_OPERACION AS CONSECUTIVO,");
			strSQL.append(" BO.IC_CLAVE AS CLAVE,");
			strSQL.append(" I.CG_RAZON_SOCIAL AS NOMBRE_IF,");
			strSQL.append(" BO.CG_DESCRIPCION AS DESCRIPCION,");
			strSQL.append(" BO.CS_OPERA_CREDITO AS OPERA_CREDITO,");
			strSQL.append(" BO.FG_PORCENTAJE_APORTACION AS POR_APORTACION,");
			strSQL.append(" BO.CG_MODALIDAD AS MODALIDAD,");
			strSQL.append(" U.CG_RAZON_SOCIAL AS UNIVERSIDAD");
			strSQL.append(" FROM CECAT_BASES_OPERA BO,");
			strSQL.append(" COMCAT_IF I,");
			strSQL.append(" COMCAT_UNIVERSIDAD U,");
			strSQL.append(" CEREL_IF_UNIVERSIDAD R");
			strSQL.append(" WHERE I.IC_IF = BO.IC_IF");
			strSQL.append(" AND I.IC_IF = R.IC_IF");
			strSQL.append(" AND U.IC_UNIVERSIDAD = R.IC_UNIVERSIDAD");
			strSQL.append(" AND BO.IC_IF = R.IC_IF");
			strSQL.append(" AND BO.IC_BASE_OPERACION = ?");
			if(!icUniversidad.equals("")){
				strSQL.append(" AND U.IC_UNIVERSIDAD = ?");
			}

			log.debug("ic_operacion: "+ ic_operacion + " . icUniversidad: " + icUniversidad);
			log.debug("Sentencia: "+ strSQL.toString());

			lVarBind = new ArrayList();
			lVarBind.add(ic_operacion);
			lVarBind.add(icUniversidad);
			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			rs = ps.executeQuery();

			if(rs.next()) {

				consecutivo  = rs.getString("CONSECUTIVO")    ==null?"": rs.getString("CONSECUTIVO");
				nombreIF     = rs.getString("NOMBRE_IF")      ==null?"": rs.getString("NOMBRE_IF");
				claveOpera   = rs.getString("CLAVE")          ==null?"": rs.getString("CLAVE");
				descripcion  = rs.getString("DESCRIPCION")    ==null?"": rs.getString("DESCRIPCION");
				operaCredito = rs.getString("OPERA_CREDITO")  ==null?"": rs.getString("OPERA_CREDITO");
				porcentaje   = rs.getString("POR_APORTACION") ==null?"": rs.getString("POR_APORTACION");
				modalidad    = rs.getString("MODALIDAD")      ==null?"": rs.getString("MODALIDAD");
				universidad  = rs.getString("UNIVERSIDAD")    ==null?"": rs.getString("UNIVERSIDAD");

				respuesta.add(nombreIF);
				respuesta.add(claveOpera);
				respuesta.add(descripcion);
				respuesta.add(operaCredito);
				respuesta.add(porcentaje);
				respuesta.add(modalidad);
				respuesta.add(consecutivo);
				respuesta.add(universidad);
			}
			ps.close();
			rs.close();

		} catch(Exception e){

			e.printStackTrace();
			log.error("error al consultar la Base de Operaci�n  " +e);
			throw new AppException("insertSolicitud(Exception) ", e);
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())  con.cierraConexionDB();
		}
			log.info("consBaseOperacion (S)");
			return respuesta;
	}

	/**
	 * Modifica  la parametrizaci�n de la Base de Operaci�n
	 * @return
	 * @param parametros
	 */
	public String ModificarBO(List parametros){

		log.info("ModificarBO (E)");

		AccesoDB con         = new AccesoDB();
		List lVarBind        = new ArrayList();
		StringBuffer strSQL  = new StringBuffer();
		PreparedStatement ps = null;
		String respuesta     = "Fallo";

		try{
			con.conexionDB();

			//String ic_if        = parametros.get(0).toString();
			String ic_operacion = parametros.get(1).toString();
			String descripcion  = parametros.get(2).toString();
			String operaCredito = parametros.get(3).toString();
			String porcentaje   = parametros.get(4).toString();
			String modalidad    = parametros.get(5).toString();
			String usuario      = parametros.get(6).toString();
			String claveAnt     = parametros.get(7).toString();
			String ic_base      = parametros.get(8).toString();

			if(!claveAnt.equals(ic_operacion)){
				respuesta= existeBO(ic_operacion);
			}

			if(!respuesta.equals("Existe")){

				strSQL = new StringBuffer();
				strSQL.append("UPDATE CECAT_BASES_OPERA "     );
				strSQL.append("SET  IC_CLAVE = ?, "           );
				strSQL.append("CG_DESCRIPCION =?, "           );
				strSQL.append("CS_OPERA_CREDITO = ?, "        );
				strSQL.append("FG_PORCENTAJE_APORTACION = ?, ");
				strSQL.append("CG_MODALIDAD = ?, "            );
				strSQL.append("CG_USUARIO_MOD = ?, "          );
				strSQL.append("DF_MODIFICACION = Sysdate "   );
				strSQL.append("WHERE IC_BASE_OPERACION = ? "  );

				log.debug("Sentencia sql:" + strSQL.toString());

				lVarBind = new ArrayList();
				lVarBind.add(ic_operacion);
				lVarBind.add(descripcion);
				lVarBind.add(operaCredito);
				lVarBind.add(porcentaje);
				lVarBind.add(modalidad);
				lVarBind.add(usuario);
				lVarBind.add(ic_base);

				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();
				respuesta ="Correcto";
			}

		} catch(Exception e) {
				respuesta ="Fallo";
			e.printStackTrace();
			log.error("error al modificar la Base de Operaci�n  " +e);
			throw new AppException("insertSolicitud(Exception) ", e);
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())  con.cierraConexionDB();
		}
		log.info("ModificarBO (S)");
		return respuesta;
	}

	/**
	 * realiza cambio de estatus de  la parametrizaci�n de la Base de Operaci�n
	 * @return
	 * @param estatus
	 * @param ic_base
	 */
	public String  cambioEstatus(String ic_base[], String estatus[])   {
		log.info("cambioEstatus (E)");
		AccesoDB con =new AccesoDB();
		StringBuffer  strSQL = new StringBuffer();
		String respuesta ="Fallo", estatus2 ="", ic_base1 ="", estatus1="";
		List		lVarBind		= new ArrayList();
		PreparedStatement ps	= null;

		try{
			con.conexionDB();

			if(ic_base !=null &&  estatus !=null ){
				for(int i=0;i<ic_base.length;i++){
					ic_base1  = ic_base[i].toString();
					estatus1  = estatus[i].toString();

					if(estatus1.equals("A")) estatus2="I";
					if(estatus1.equals("I")) estatus2="A";


					strSQL = new StringBuffer();
					strSQL.append( " Update  CECAT_BASES_OPERA "+
											" SET  CS_ACTIVO = ? "+
											" where IC_BASE_OPERACION  =  ?");
						lVarBind		= new ArrayList();
						lVarBind.add(estatus2);
						lVarBind.add(ic_base1);

						System.out.println("strSQL "+strSQL.toString());

						System.out.println("lVarBind "+lVarBind);
						ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
						ps.executeUpdate();
						ps.close();
				}
			}
			respuesta ="Correcto";

		} catch(Exception e) {
			respuesta ="Fallo";
			e.printStackTrace();
			log.error("error al cambiar el estatus de  la Base de Operaci�n  " +e);
			throw new AppException("insertSolicitud(Exception) ", e);
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())  con.cierraConexionDB();
		}
		log.info("cambioEstatus (S)");
		return respuesta;
	}
	public Registros getTotalesResultados(String intermediario,String universidad, String grupo,
	String estatus,String folio, String fechaValidaIni, String fechaValidaFin,String fechaOperaIni, String fechaOperaFin){
		log.info("getTotalesResultados (E)");
		AccesoDB con =new AccesoDB();
		StringBuffer  qrySentencia = new StringBuffer();
		List		conditions		= new ArrayList();
		Registros reg=null;
		try{
			con.conexionDB();
			conditions 		= new ArrayList();
			qrySentencia.append(" select sum(cce.fn_financiamiento) as total_contra, "+
			 " sum (ccm.FN_MONTO_CONTRAGAN) as total_valido,cece.cd_descripcion as estatus , "+
				" count (cd.FN_IMPORTE) as registros "+
				" from comcat_if cif,\n" +
				" COMCAT_UNIVERSIDAD cuni,\n" +
				" CE_CONTENIDO_EDUCATIVO cce,\n" +
				" gti_estatus_solic ges,\n" +
				" CECAT_ESTATUS_CRED_EDUCA cece,\n" +
				" CE_DEPOSITO cd,\n" +
				" ce_confirma_monto  ccm\n" +
				" where \n" +
				" cif.IC_IF= cce.ic_if\n" +
				" and cuni.ic_universidad=cce.ic_Universidad\n" +
				" and ges.ic_folio=cce.IC_FOLIO_OPERACION\n" +
				" and cece.ic_estatus_cred_educa=cce.ic_ESTATUS_CRED_EDUCA\n" +
				" and cd.ic_folio_operacion=ges.ic_folio\n" +
				" and ccm.ic_folio_operacion=ges.ic_folio");


			if(!intermediario.equals("")){
				qrySentencia.append(" and cce.ic_if = ? ");
				conditions.add(intermediario);
			}
			if(!universidad.equals("")){
				qrySentencia.append(" and cuni.ic_universidad=? ");
				conditions.add(universidad);
			}
			if(!grupo.equals("")){
				qrySentencia.append(" and cuni.ic_grupo=? ");
				conditions.add(grupo);
			}
			if(!estatus.equals("")){
				qrySentencia.append(" and cece.ic_estatus_cred_educa = ? ");
				conditions.add(estatus);
			}
			if(!folio.equals("")){
				qrySentencia.append(" and cd.ic_folio=? ");
				conditions.add(folio);
			}
			if(!fechaValidaIni.equals("")&&!fechaValidaFin.equals("")){
				qrySentencia.append(" and ccm.DF_HR_COMFMONTO >=  to_date(?, 'dd/mm/yyyy')  ") ;
				qrySentencia.append("and ccm.DF_HR_COMFMONTO < to_date(?, 'dd/mm/yyyy')+1  ") ;
				conditions.add(fechaValidaIni);
				conditions.add(fechaValidaFin);
			}
			if(!fechaOperaIni.equals("")&&!fechaOperaFin.equals("")){
				qrySentencia.append(" and ges.df_autorizacion >=  to_date(?, 'dd/mm/yyyy')  ") ;
				qrySentencia.append("and ges.DF_AUTORIZACION < to_date(?, 'dd/mm/yyyy')+1  ") ;
				conditions.add(fechaOperaIni);
				conditions.add(fechaOperaFin);
			}

			qrySentencia.append(" group by cce.ic_ESTATUS_CRED_EDUCA ");

			reg=con.consultarDB(qrySentencia.toString(),conditions);

		} catch(Exception e) {

			e.printStackTrace();
			log.error("error al cambiar el estatus de  la Base de Operaci�n  " +e);
			throw new AppException("insertSolicitud(Exception) ", e);
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())  con.cierraConexionDB();
		}
		log.info("cambioEstatus (S)");



		return reg;
		}

	public Registros getTotalesResultadosEstudiantes(String estatusReg,String folio){
		log.info("getTotalesResultados (E)");
		AccesoDB con =new AccesoDB();
		StringBuffer  qrySentencia = new StringBuffer();
		List		conditions		= new ArrayList();
		Registros reg=null;
		try{
			con.conexionDB();
			conditions 		= new ArrayList();
			qrySentencia.append("select count(cce.ic_registro) as registros,sum(cce.fn_saldolineas)"+
			" as total_importe, sum(cce.fn_total) as total_monto,sum(cce.fn_saldolineas) "+
			"as total_saldo,sum(cce.fn_monto_garantia ) as total_contra "+
			" from "+
			" CE_CONTENIDO_EDUCATIVO cce,\n" +
			" cecat_bases_opera cbo "+
			" where \n" +
			" cce.ic_programa=cbo.ic_base_operacion \n");

		if(!estatusReg.equals("")){
			qrySentencia.append(" and cce.cg_aceptado_siar = ? ");
			conditions.add(estatusReg);
		}
		if(!folio.equals("")){
			qrySentencia.append(" and cce.ic_folio_operacion = ? ");
			conditions.add(folio);
		}


			reg=con.consultarDB(qrySentencia.toString(),conditions);

		} catch(Exception e) {

			e.printStackTrace();
			log.error("error al cambiar el estatus de  la Base de Operaci�n  " +e);
			throw new AppException("insertSolicitud(Exception) ", e);
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())  con.cierraConexionDB();
		}
		log.info("cambioEstatus (S)");

		return reg;
	}

	/***
	 *Metodo que obtiene la informaci�n para validar las aportaciones
	 **/
	public Registros getFoliosConfirmar(String claveUniversidad, String fechaIni, String fechaFin, String folioOpera, String claveIf){
		log.info("CreditoEducativo::getFoliosConfirmar (E)");
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();

			StringBuffer strSQL = new StringBuffer();

			List params = new ArrayList();
			params.add("S");
			params.add(new Integer(2));
			params.add(new Integer(1));
			params.add("E");
			params.add(new Integer(2));
			params.add(new Integer(5));

			strSQL.append(
				"SELECT gti.ic_folio AS folio_operacion,"+
				"		 cif.cg_razon_social AS nombre_if,"+
				"		 cif.ic_if AS clave_if, gti.ic_if_siag AS clave_if_siag,"+
				"		 TO_CHAR (gti.df_registro, 'dd/mm/yyyy HH24:MI:SS') AS fecha_hora,"+
				"		 gti.fn_monto_valido AS monto_valido, 'false' AS aceptados,"+
				"		 'false' AS rechazados, gti.in_registros_acep AS num_aceptadas,"+
				"		 gti.in_registros_rech AS num_rechazadas,"+
				"		 gti.in_registros AS total_operaciones, '' AS causa_rechazo,"+
				"		 (SELECT SUM("+
				"						ROUND(CASE"+
				"							 WHEN bo.cg_modalidad = 1"+
				"								 THEN ((  edu.fn_financiamiento * bo.fg_porcentaje_aportacion)/ 100 )"+
				"							 WHEN bo.cg_modalidad = 2"+
				"								 THEN ((  edu.fn_imp_disposicion* bo.fg_porcentaje_aportacion)/ 100)"+
				"							 ELSE 0"+
				"						 END,2)) AS monto_garantia"+
				"			 FROM cecat_bases_opera bo,"+
				"					ce_contenido_educativo edu"+
				"			WHERE edu.ic_programa = bo.ic_clave"+
				"			  AND edu.ic_if = bo.ic_if"+
				"			  AND edu.cg_aceptado_siag = ? "+
				"			  AND gti.ic_estatus_cred_educa = ?"+
				"			  AND gti.ic_folio = edu.ic_folio_operacion"+
				"			  AND gti.ic_if_siag = edu.ic_if_siag) AS monto_garantia"+
				" FROM gti_estatus_solic gti, comcat_if cif"+
				" WHERE cif.ic_if_siag = gti.ic_if_siag"+
				"  AND gti.ic_tipo_operacion = ? "+
				"	AND gti.cg_programa = ? "+
				"	AND gti.ic_estatus_cred_educa = ? "+
				"	AND gti.ic_situacion = ? "
			);
			if(claveUniversidad != null	&&	!claveUniversidad.equals("")){
				strSQL.append("	AND gti.ic_universidad = ? ");
				params.add(claveUniversidad);
			}
			if(claveIf != null	&&	!claveIf.equals("")){
				strSQL.append("	AND cif.ic_if = ? ");
				params.add(claveIf);
			}
			if(folioOpera != null	&&	!folioOpera.equals("")){
				strSQL.append("	AND gti.ic_folio = ? ");
				params.add(folioOpera);
			}
			if (fechaIni != null && !fechaIni.equals("")   && fechaFin != null && !fechaFin.equals("") ) {
				strSQL.append("	AND gti.df_registro >= TO_DATE (?, 'dd/mm/yyyy')	AND gti.df_registro < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
				params.add(fechaIni);
				params.add(fechaFin);
			}

			log.debug("getFoliosConfirmar:: strSQL=" + strSQL.toString() );
			log.debug("getFoliosConfirmar:: params=" + params );

			Registros reg = con.consultarDB(strSQL.toString(), params);

			return reg;

		} catch(Exception e) {
			throw new AppException("Error al obtener los datos a confirmar", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CreditoEducativo::getFoliosConfirmar (S)");
		}
	}

	/**
	 * Metodo que establece las aportaciones aceptadas o rechazadas
	 * */
	 public void setValidaAportaciones(String[] folios,String[] aceptados,String[] rechazados,
													String[] montoGaran, String[] causaRechazo,String fechaValidacion){
		log.info("CreditoEducativo::setValidaAportaciones (E)");

	   try {
	      if (	(fechaValidacion == null || fechaValidacion.equals(""))	) {
	         throw new Exception("Los parametros no pueden ser nulos");
	      }
	   } catch(Exception e) {
	      log.info("CreditoEducativo::setValidaAportaciones(Error) "+"Error en los parametros recibidos. "
						+ e.getMessage() + "\n fechaValidacion=" + fechaValidacion + "\n" );
	      throw new AppException("Error Inesperado", e);
	   }

	     AccesoDB          con    = new AccesoDB();
	     PreparedStatement ps     = null;
	     PreparedStatement ps1    = null;
	     ResultSet         rs     = null;
	     StringBuffer      query  = new StringBuffer(256);
	     StringBuffer      query2 = new StringBuffer(256);
	     StringBuffer      query3 = new StringBuffer(256);
	     StringBuffer      query4 = new StringBuffer(256);
	     boolean           ok     = true;

		try{
			con.conexionDB();

		    query.append(
		            "INSERT INTO                            "+
		            "ce_confirma_monto                      "+
		            "(                                      "+
		            " ic_folio_operacion,                   "+
		            " df_hr_comfmonto,                      "+
		            " fn_monto_contragan,                   "+
		            " cg_causa_rechazo_monto                "+
		            ")                                      "+
		            " VALUES                                "+
		            " (                                     "+
		            " ?,TO_DATE(?, 'DD/MM/YYYY HH24:MI:SS') "+
		            ",?,?                                   "+
		            ")                                      "
		    );
			query2.append(
				"UPDATE gti_estatus_solic SET ic_estatus_cred_educa = ? WHERE ic_folio = ? "
			);
			query3.append(
				"SELECT ce.ic_registro, ce.ic_folio_operacion, bo.fg_porcentaje_aportacion,"+
				"		 ce.ic_programa, bo.ic_clave, ce.fn_financiamiento,"+
				"		 ce.fn_imp_disposicion, bo.cg_modalidad,"+
				"		 ROUND(CASE"+
				"			 WHEN bo.cg_modalidad = 1"+
				"				 THEN ((ce.fn_financiamiento * bo.fg_porcentaje_aportacion)/100)"+
				"			 WHEN bo.cg_modalidad = 2"+
				"				 THEN ((ce.fn_imp_disposicion * bo.fg_porcentaje_aportacion)/100)"+
				"			 ELSE 0"+
				"		 END,2) AS monto_garantia"+
				" FROM cecat_bases_opera bo, ce_contenido_educativo ce, gti_estatus_solic ges"+
				" WHERE ce.ic_programa = bo.ic_clave"+
				"	AND ce.ic_if = bo.ic_if"+
				"	AND ce.cg_aceptado_siag = ? "+
				"	AND ges.ic_estatus_cred_educa = ? "+
				"	AND ges.ic_folio = ce.ic_folio_operacion"+
				"	AND ges.ic_if_siag = ce.ic_if_siag "+
				"	AND ges.ic_folio = ? "
			);
			query4.append(
				"UPDATE ce_contenido_educativo SET fn_monto_garantia = ? WHERE ic_registro = ?"
			);


		    for (int j=0; j < folios.length; j++) {
		            if(!folios[j].equals("")){

		                    ps = con.queryPrecompilado(query.toString());
		                    ps.clearParameters();
                                    System.out.println("" + new BigDecimal(folios[j]));
		                    ps.setBigDecimal(1, new BigDecimal(folios[j]) );
		                    ps.setString(    2, fechaValidacion           );

		                    if("true".equals(aceptados[j])){
		                            ps.setBigDecimal( 3, new BigDecimal(montoGaran[j]));
		                    } else{
		                            ps.setNull( 3, Types.NUMERIC);
		                    }
		                    if("true".equals(rechazados[j])){
		                            ps.setString( 4, causaRechazo[j]);
		                    }else{
		                            ps.setNull( 4, Types.VARCHAR);
		                    }
		                    ps.execute();
		                    ps.close();

		                    ps = con.queryPrecompilado(query2.toString());
		                    ps.clearParameters();
		                    if("true".equals(aceptados[j])){
		                            ps.setString(1, "4"); //ESTATUS 4-> "CONTRAGARANT�A CONFIRMADA-UNIVERSIDAD�
		                    }else{
		                            ps.setString(1, "5"); //ESTATUS 5-> "CONTRAGARANT�A RECHAZADA-UNIVERSIDAD�
		                    }
		                    ps.setBigDecimal( 2, new BigDecimal(folios[j]));
		                    ps.execute();
		                    ps.close();

		                    if("true".equals(aceptados[j])){

		                            ps1 = con.queryPrecompilado(query3.toString());
		                            ps1.clearParameters();
		                            ps1.setString( 1, "S"); //cg_aceptado_siag
		                            ps1.setString( 2, "4"); //ESTATUS 4-> "CONTRAGARANT�A CONFIRMADA-UNIVERSIDAD�
		                            ps1.setBigDecimal( 3, new BigDecimal(folios[j]));
		                            rs = ps1.executeQuery();
		                            BigDecimal monto_garant = new BigDecimal(0.00);
		                            String icRegistro = "";
		                            while(rs.next()){
		                                    monto_garant = rs.getBigDecimal("MONTO_GARANTIA");
		                                    icRegistro = rs.getString("IC_REGISTRO");
		                                    ps = con.queryPrecompilado(query4.toString());
		                                    ps.clearParameters();
		                                    ps.setBigDecimal( 1, monto_garant);
		                                    ps.setBigDecimal( 2, new BigDecimal(icRegistro));
		                                    ps.execute();
		                                    ps.close();
		                            }
		                            rs.close();
		                            ps1.close();

		                    }

		            }else{
		                    throw new AppException("Error: No existe informacion a procesar");
		            }
		    }

		}catch(Exception e){
			ok = false;
			log.error("CreditoEducativoBean::setValidaAportaciones(Exception)");
			throw new AppException("Error al guardar la informacion"+e.getMessage());
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("CreditoEducativoBean::setValidaAportaciones(S)");
		}
		//En caso de requerir una variable de retorno: return "";
	 }

	/**
	 * Metodo para la validaci�n de los registros de al Universidad
	 * @return
	 * @param acuse
	 * @param causaRechazo
	 * @param regRechazados
	 * @param regAceptados
	 * @param parametros
	 */
public String  transEducativoaGarantia(List parametros, String regAceptados[],  String regRechazados[], String causaRechazo[] ,String acuse, String no_disposicion )  {

		log.info("transEducativoaGarantia (E)");
		AccesoDB con =new AccesoDB();
		StringBuffer  strSQL = new StringBuffer();
		String respuesta ="Fallo";
		List		lVarBind		= new ArrayList();
		PreparedStatement ps	= null;
		ResultSet  rs = null;
		int li=0;
		String folio= "", ic_nafin_electronico="", 
				 anio="", trim_calif ="", ic_if_siag ="";

		try{
			con.conexionDB();

			String ic_if = parametros.get(0).toString();
			String nombreIF = parametros.get(1).toString();
			String regisAceptados= parametros.get(2).toString();
			String noRegRechazados = parametros.get(3).toString();
			String noRegistros = parametros.get(4).toString();
			String usuario= parametros.get(5).toString();
			String fechaHora = parametros.get(6).toString();
			String fecha = parametros.get(7).toString();
			String hora = parametros.get(8).toString();
			String impte_total = parametros.get(9).toString();
			String registosC = parametros.get(10).toString();
			String nombreUsuario = parametros.get(11).toString();
			String ic_universidad = parametros.get(12).toString();

			// REGISTRO VALIDO-UNIVERSIDAD
			if(!regisAceptados.equals("0")){

				int ic_contragarante =  this.getClaveContragarante(ic_if);

				//OBTENGO EL NUMERO DE NAFIN ELECTORNICO Y EL NUMERO DE IC_IF_SIAG QUE LE CORRESPONDE AL IF
				strSQL = new StringBuffer();
				strSQL.append( " SELECT  i.ic_if_siag  AS IFSIAG , n.ic_nafin_electronico as NAFIN   " +
									" FROM  comcat_if i  , COMREL_NAFIN n "+
									"  WHERE  i.ic_if = n.ic_epo_pyme_if "+
									"  and n.cg_tipo = ? " +
									" and ic_if = ? ");

				lVarBind		= new ArrayList();
				lVarBind.add("I");
				lVarBind.add(new Integer(ic_if));

				log.debug("strSQL "+strSQL.toString());
				log.debug("lVarBind "+lVarBind);

				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				rs = ps.executeQuery();
				while(rs.next()) {
					ic_if_siag= rs.getString("IFSIAG")==null?"":rs.getString("IFSIAG");
					ic_nafin_electronico= rs.getString("NAFIN")==null?"":rs.getString("NAFIN");

				}
				rs.close();
				ps.close();

				log.debug("ic_if_siag "+ic_if_siag);
				log.debug("ic_nafin_electronico "+ic_nafin_electronico);


				// obtengo el folio  con respecto a la Universidad
				strSQL = new StringBuffer();
				strSQL.append(
						" SELECT "+
							" cn.ic_nafin_electronico || TO_CHAR(sysdate,'yyyymmdd') || ? AS FOLIO"+
							" ,TO_CHAR(sysdate,'dd/mm/yyyy') as FECHA"+
							" ,TO_CHAR(sysdate,'HH24:MI') as HORA"+
							" ,TO_CHAR(sysdate,'yyyy') as ANIO"+
							" ,case"+
								"	when to_char(sysdate,'mm') in('01','02','03') then 1"   +
								"	when to_char(sysdate,'mm') in('04','05','06') then 2"   +
								"	when to_char(sysdate,'mm') in('07','08','09') then 3"   +
								"	when to_char(sysdate,'mm') in('10','11','12') then 4"   +
							"  end as TRIMESTRE"  +
						" FROM COMREL_NAFIN CN"+
						" ,COMCAT_UNIVERSIDAD  u"+
							" WHERE CN.ic_epo_pyme_if = u.ic_universidad"+
							" and CN.cg_tipo = ?"+
							" and CN.ic_epo_pyme_if = ?") ;

				lVarBind		= new ArrayList();
				lVarBind.add(this.getConsecutivo());
				lVarBind.add("U");
				lVarBind.add(new Integer(ic_universidad));

				log.debug("strSQL "+strSQL.toString());
				log.debug("lVarBind "+lVarBind);

				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				rs = ps.executeQuery();
				while(rs.next()) {
					folio= rs.getString("FOLIO")==null?"":rs.getString("FOLIO");
					fecha= rs.getString("FECHA")==null?"":rs.getString("FECHA");
					hora= rs.getString("HORA")==null?"":rs.getString("HORA");
					anio= rs.getString("ANIO")==null?"":rs.getString("ANIO");
					trim_calif= rs.getString("TRIMESTRE")==null?"":rs.getString("TRIMESTRE");
				}
				rs.close();
				ps.close();

				//INSERTO LOS DATOS A GARANTIAS
				strSQL = new StringBuffer();
				strSQL.append(	"insert into gti_estatus_solic"+
									"(ic_folio,ic_if_siag,ic_nafin_electronico,df_fecha_hora" +
									" ,in_registros,fn_impte_total,ic_usuario_facultado,ic_situacion"+
									" ,ic_sit_transfer,ic_tipo_operacion,ic_contragarante, cg_programa, ic_estatus_cred_educa, ic_universidad, CG_PPERD_DISP_POST, IN_REGISTROS_ACEP, FN_MONTO_VALIDO )"+
									" values(?,?,?,to_date(?,'dd/mm/yyyy HH24:MI'),?,?"+
									" ,?,?,?,?,?, ?, ?, ?, ? , ? , ?  )   ");



				lVarBind		= new ArrayList();
				lVarBind.add(new BigDecimal(folio));
				lVarBind.add(new Integer (ic_if_siag));
				lVarBind.add(new Integer (ic_nafin_electronico));
				lVarBind.add(fechaHora);
				lVarBind.add(new Integer(regisAceptados));
				lVarBind.add(impte_total);
				lVarBind.add(usuario);
				if(no_disposicion.equals("1"))lVarBind.add(new Integer(1));
				if(!no_disposicion.equals("1"))lVarBind.add(new Integer(5));
				lVarBind.add(new Integer(0));
				lVarBind.add(new Integer(1));
				lVarBind.add(new Integer(ic_contragarante));
				lVarBind.add("E");
				lVarBind.add(new Integer(2));
				lVarBind.add(ic_universidad);
				if(no_disposicion.equals("1"))  { lVarBind.add("N"); }
				if(!no_disposicion.equals("1")) { lVarBind.add("S"); }
				if(no_disposicion.equals("1")) { lVarBind.add(""); }
				if(!no_disposicion.equals("1")){ lVarBind.add(new Integer(regisAceptados));	}
				lVarBind.add(impte_total);

				log.debug("strSQL "+strSQL.toString());
				log.debug("lVarBind "+lVarBind);

				ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
				ps.executeUpdate();
				ps.close();


				//OBTENGO  LA LINEA CORRESPONDIENTE A GARANTIAS  PARA  ALTA  INSERTARLA EN  LAS TABLAS DE GARANTIAS

				strSQL = new StringBuffer();
				strSQL.append( " SELECT cg_contenidog as contenido , ic_if_siag as ifSiag,  ic_if as ifNaf , ic_registro as ic_reg, ig_programa as programa "+
									" FROM  ce_contenido_arc "+
									" WHERE  ic_registro in( "+registosC+")");

				log.debug("strSQL "+strSQL.toString());

				ps = con.queryPrecompilado(strSQL.toString());
				rs = ps.executeQuery();

				while(rs.next()) {
					li++;
					String contenido= rs.getString("contenido")==null?"":rs.getString("contenido");
					String ifSiag= rs.getString("ifSiag")==null?"":rs.getString("ifSiag");
					String ifNaf= rs.getString("ifNaf")==null?"":rs.getString("ifNaf");
					String ic_reg= rs.getString("ic_reg")==null?"":rs.getString("ic_reg");
					String programa= rs.getString("programa")==null?"":rs.getString("programa");


					strSQL = new StringBuffer();
					strSQL.append( " insert into gti_contenido_arch"+
						"(ic_folio, ic_linea, cg_contenido , ic_if_siag,  ic_if, ic_registro, ig_programa )"+
						"values(?,?,?,?,?,?, ?)");

					lVarBind		= new ArrayList();
					lVarBind.add(new BigDecimal(folio));
					lVarBind.add(new Integer(li));
					lVarBind.add(contenido);
					lVarBind.add(ifSiag);
					lVarBind.add(ifNaf);
					lVarBind.add(ic_reg);
					lVarBind.add(programa);

					log.debug("strSQL "+strSQL.toString());
					log.debug("lVarBind "+lVarBind);

					ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
					ps.executeUpdate();
					ps.close();
				}
				rs.close();
				ps.close();

				//ACTUALIZO EL ESTATUS DE LOS DOCUMENTOS VALIDADOS A  (2)REGISTRO VALIDO-UNIVERSIDAD
				for(int i=0;i<regAceptados.length;i++){
					strSQL = new StringBuffer();
					strSQL.append(" UPDATE ce_contenido_educativo "+
					" set ic_estatus_cred_educa =  ? ,"+
					" df_hr_validacion  = Sysdate,  "+
					" ic_folio_operacion =?, "+
					" cc_acuse = ? "+
					" where ic_registro = ?  ");

					lVarBind		= new ArrayList();
					lVarBind.add(new Integer(2));
					lVarBind.add(new BigDecimal(folio));
					lVarBind.add(acuse);
					lVarBind.add(regAceptados[i] );

					log.debug("strSQL "+strSQL.toString());
					log.debug("lVarBind "+lVarBind);

					ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
					ps.executeUpdate();
					ps.close();
				}

			}
			// //ACTUALIZO EL ESTATUS DE LOS DOCUMENTOS RECHAZADOS A  (3) REGISTRO RECHAZADO-UNIVERSIDAD
			if(!noRegRechazados.equals("0")){

				for(int i=0;i<regRechazados.length;i++){

					strSQL = new StringBuffer();
					strSQL.append(" UPDATE ce_contenido_educativo "+
					" set  cg_causa_rechazo = ? ,"+
					" ic_estatus_cred_educa =  ?, "+
					" df_hr_validacion  = Sysdate,  "+
					" cc_acuse = ? "+
					" where ic_registro = ?  ");

					lVarBind		= new ArrayList();
					lVarBind.add(causaRechazo[i] );
					lVarBind.add(new Integer(3));
					lVarBind.add(acuse);
					lVarBind.add(regRechazados[i] );

					log.debug("strSQL "+strSQL.toString());
					log.debug("lVarBind "+lVarBind);

					ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
					ps.executeUpdate();
					ps.close();
				}
			}

			// GUARGO UN ACUSE YA QUE PIDE CERTIFICADO SOBRE TODO  PARA  TENER LOS DATOS DE LAS CIFRAS DE CONTROL

			strSQL = new StringBuffer();
					strSQL.append(" insert into CE_ACUSE_EDUCATIVO_VAL  "+
					" ( cc_acuse, ic_folio_operacion, ic_if, cg_razon_social, in_registros_acep, in_registros_rech, in_registros, df_operacion, ic_usuario, cg_tipo_operacion, cg_nombre_usuario )  "+
					"  Values ( ?,?,?,?,?,?,?,Sysdate,?, ?, ? )");

			lVarBind		= new ArrayList();
			lVarBind.add(acuse);
			lVarBind.add(folio);
			lVarBind.add(ic_if );
			lVarBind.add(nombreIF );
			lVarBind.add(regisAceptados );
			lVarBind.add(noRegRechazados );
			lVarBind.add(noRegistros );
			lVarBind.add(usuario);
			lVarBind.add("CREDITO EDUCATIVO");
			lVarBind.add(nombreUsuario);

			log.debug("strSQL "+strSQL.toString());
			log.debug("lVarBind "+lVarBind);

			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();

			respuesta= folio;

		} catch(Exception e) {
			respuesta ="Fallo";
			con.terminaTransaccion(false);
			e.printStackTrace();
			log.error("error al transmitir datos de Educativo a Garantia  " +e);
			throw new AppException("transEducativoaGarantia(Exception) ", e);
		} finally {
			con.terminaTransaccion(true);
			if(con.hayConexionAbierta())  con.cierraConexionDB();
		}

		log.info("transEducativoaGarantia (S)");
		return respuesta;
	}

	/**
	 * metodo para obtener las bases Parametizadas por el IF
	 * @return
	 * @param ic_if
	 */
	public String  baseOperacionIF(String ic_if ){
		System.out.println("baseOperacionIF (E)");
		AccesoDB con					= new AccesoDB();
		PreparedStatement	ps			= null;
		ResultSet			rs			= null;
		StringBuffer 		query		= new StringBuffer(256);
		boolean 				ok			= true;
		StringBuffer base = new StringBuffer();
		try{

			con.conexionDB();
			query.append(
				"SELECT  ic_clave AS CLAVEBASE "+
				" FROM  cecat_bases_opera "+
				" WHERE ic_if = "+ic_if);

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();

			while(rs.next()) {
				 base.append(rs.getString("CLAVEBASE")+",");
			}

			if(base.length()>0){
				base.deleteCharAt(base.length()-1);
			}

		}catch(Exception e){
			ok = false;
			System.out.println("baseOperacionIF(Exception)");
			throw new AppException("Error al guardar la informacion"+e.getMessage());
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
				System.out.println("baseOperacionIF (E)");
		}
		return base.toString();

	 }


	 /**
	 * Pantalla Parametrizaci�n IF- Universidad
	 * para inactivar la relaci�n de IF yla Universidad
	 * @return
	 * @param universidad
	 * @param ic_if
	 */
public void inacRelIF_Univ( String ic_if , String universidad, String estatus ){
	log.info("inacRelIF_Univ (E)");
	AccesoDB con =new AccesoDB();
	StringBuffer  strSQL = new StringBuffer();
	boolean  ok = false;
	List		lVarBind		= new ArrayList();
	PreparedStatement ps	= null;

	try{
		con.conexionDB();

		// se agregan todos los registros relacionados con el IF
			strSQL = new StringBuffer();
			strSQL.append( " UPDATE  cerel_if_universidad "+
				" set cg_activo = ? "+
				" where   ic_if = ?"+
				" and ic_universidad	 = ? ");

			lVarBind		= new ArrayList();
			lVarBind.add(estatus);
			lVarBind.add(ic_if);
			lVarBind.add(universidad);

			log.info("strSQL  "+strSQL);
			log.info("lVarBind "+lVarBind);

			ps = con.queryPrecompilado(strSQL.toString(), lVarBind);
			ps.executeUpdate();
			ps.close();


	} catch(Exception e) {
			ok = false;
		e.printStackTrace();
		log.error("error en inactivar la  parametrizaci�n de relacion Unversidades  - IF  " +e);
		throw new AppException("inacRelIF_Univ(Exception) ", e);
	} finally {
			ok = true;
			con.terminaTransaccion(ok);
		if(con.hayConexionAbierta())  con.cierraConexionDB();
	}
		log.info("inacRelIF_Univ (S)");
}

	/**
	 * Metodo que establece las garantias aceptadas o rechazadas
	 * @autor Hugo Vargas
	 * */
	 public void setValidaGarantias(String folio,	String estatus, String causaRechazo,
								String fechaDeposito, String importe, String Banco,	String referencia){
		log.info("CreditoEducativo::setValidaGarantias (E)");

	   try {
	      if (	(folio == null || folio.equals("")) &&	(estatus == null || estatus.equals("")) &&
				(fechaDeposito == null || fechaDeposito.equals(""))	) {
	         throw new Exception("Los parametros no pueden ser nulos");
	      }
	   } catch(Exception e) {
	      log.info("CreditoEducativo::setValidaGarantias(Error) "+"Error en los parametros recibidos. "
						+ e.getMessage() + "\n folio=" + folio + "\n estatus=" + estatus + "\n fechaDeposito=" + fechaDeposito + "\n" );
	      throw new AppException("Error Inesperado", e);
	   }

		AccesoDB con					= new AccesoDB();
		PreparedStatement	ps			= null;
		StringBuffer 		query		= new StringBuffer(256);
		StringBuffer 		query2	= new StringBuffer(256);
		boolean 				ok			= true;

		try{
			con.conexionDB();

			query.append(
				"INSERT INTO											"+
				"	ce_deposito											"+
				"	(														"+
				"	ic_folio_operacion,								"+
				"  df_deposito,										"+
				"	fn_importe,											"+
				"	cg_nombre_banco,									"+
				"	cg_clave_referencia,								"+
				"	cg_causa_rechazo									"+
				"	)	 													"+
				"	VALUES 												"+
				"	( ?, 														"
			);

			if (	"6".equals(estatus)	)				{

				query.append("	  TO_DATE(?, 'DD/MM/YYYY')	");

			}else if (	"7".equals(estatus)	){

				query.append("	? ");
			}
			query.append("	 ,?,?,?,? ) ");

			query2.append(
				"UPDATE gti_estatus_solic SET ic_estatus_cred_educa = ? WHERE ic_folio = ? "
			);


			List var = new ArrayList();
			var.add(new BigDecimal(folio));
			var.add(fechaDeposito);
			var.add(importe);
			var.add(Banco);
			var.add(referencia);
			var.add(causaRechazo);
			log.debug("setValidaGarantias.query - -  "+query.toString());
			log.debug("setValidaGarantias.consitions - -  "+var);


			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setBigDecimal(	1,	new BigDecimal(folio)				);

			if(	"6".equals(estatus)							){
				ps.setString(		2,	fechaDeposito						);
				ps.setBigDecimal(	3,	new BigDecimal(importe.replaceAll(",", ""))			);
				ps.setString(		4,	Banco									);
				ps.setString(		5,	referencia							);
				ps.setNull(			6,	Types.VARCHAR						);
			}else if (	"7".equals(estatus)	)				{
				ps.setNull(			2,	Types.DATE							);
				ps.setNull(			3,	Types.NUMERIC						);
				ps.setNull(			4,	Types.VARCHAR						);
				ps.setNull(			5,	Types.VARCHAR						);
				ps.setString(		6,	causaRechazo						);
			}else{
				throw new AppException("Error: Ocurrio un error al procesar la informaci�n");
			}

			ps.execute();
			ps.close();

			ps = con.queryPrecompilado(query2.toString());
			ps.clearParameters();
			if(	"6".equals(estatus)							){

				ps.setString(1, estatus);	//ESTATUS 6-> "CONTRAGARANT�A ACEPTADA"

			}else if (	"7".equals(estatus)	)				{

				ps.setString(1, estatus);	//ESTATUS 7-> "CONTRAGARANT�A RECHAZADA"

			}else{
				throw new AppException("Error: Ocurrio un error al procesar la informaci�n");
			}
			ps.setBigDecimal(	2,	new BigDecimal(folio)	);
			ps.execute();
			ps.close();

		}catch(Exception e){
			ok = false;
			log.error("CreditoEducativoBean::setValidaAportaciones(Exception)");
			throw new AppException("Error al guardar la informacion"+e.getMessage());
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(ok);
				con.cierraConexionDB();
			}
			log.info("CreditoEducativoBean::setValidaAportaciones(S)");
		}
		//En caso de requerir una variable de retorno: return "";
	 }

	/**
	 *
	 * Valida la longitud de un campo entero o decimal proveniente de una linea de archivo
	 *
	 * Nota: Una precision de 5 y una escala de 2, permiten tener 3 enteros y 2 decimales
	 *
	 * @debugInfo Se consideran dentro de la parte entera los signos: '-' '+', lo podr�a
	 * ocasionar una validacion erronea de la precion
	 *
	 * @param numLinea    Numero de linea en el archivo
	 * @param campo       Valor del campo a Validar
	 * @param nombreCampo Descripcion del campo que se valida
	 * @param rg          objeto que almacena los resultados del procesamiento
	 * @param precision   Numero de digitos que conforman el valor
	 * @param decimales   Numero de digitos permitidos como decimales
	 *
	 * @return            <tt>false</tt> si cualquiera de las partes del n�mero excede la precisi�n
	 *                    especificada. <tt>true</tt> en caso contrario.
	 *
	 */
	private boolean validaPrecisionDecimal(
			int           numLinea,
			String        campo,
			String        nombreCampo,
			ResultadosGarEducativo rg,
			int           precision,
			int           decimales
	){

		boolean validacion    = true;
		campo                 = campo == null?"":campo;

		String[] componentes  = campo.split("\\.",-1);

		String   parteEntera  = componentes.length > 0?componentes[0]:"";
		String   parteDecimal = componentes.length > 1?componentes[1]:"";

		if (        parteDecimal.length() > decimales              ) {
			//Error. El numero de decimales es mayor al especificado
			validacion = false;
		} else if ( parteEntera.length() > precision - decimales   ) {
			//Error. El numero de enteros es mayor al permitido
			validacion = false;
		}

		if (!validacion) {

			rg.appendErrores("Error en la linea:");
			rg.appendErrores(String.valueOf(numLinea));
			rg.appendErrores(" El valor del campo ");
			rg.appendErrores(nombreCampo);
			rg.appendErrores(" no cumple con la longitud requerida \n");
			rg.setErrorLinea(true);
			validacion = false;

		}
		return validacion;

	}

	/**
	 *
	 * Valida la longitud de un campo de caracteres proveniente de una linea de archivo
	 *
	 * @param numLinea     Numero de linea en el archivo
	 * @param campo        Valor del campo a Validar
	 * @param nombreCampo  Descripcion del campo que se valida
	 * @param rg           objeto que almacena los resultados del procesamiento
	 * @param longitudMax  Longitud M�xima de la cadena
	 *
	 * @return            <tt>false</tt> si la cadena proporcionada no cumple con la
	 *                    longitud maxima permitida. <tt>true</tt> en caso contrario.
	 *
	 */
	private boolean validarLongitud(
			int           numLinea,
			String        campo,
			String        nombreCampo,
			ResultadosGarEducativo rg,
			int           longitudMax
	){

		boolean validacion = true;
		if (campo!=null && campo.length() > longitudMax) {

			rg.appendErrores("Error en la linea:");
			rg.appendErrores(String.valueOf(numLinea));
			rg.appendErrores(" El valor del campo ");
			rg.appendErrores(nombreCampo);
			rg.appendErrores(" no cumple con la longitud m�xima permitida: "+longitudMax+". \n");
			rg.setErrorLinea(true);
			validacion = false;

		}
		return validacion;

	}

	/**
	 *
	 * Valida que la longitud de un campo de caracteres proveniente de una linea de archivo
	 * sea multiplo del numero indicado en el campo: <tt>longitud</tt>.
	 *
	 * @param numLinea     		Numero de linea en el archivo.
	 * @param campo        		Valor del campo a Validar.
	 * @param nombreCampo  		Descripcion del campo que se valida.
	 * @param rg           		Objeto que almacena los resultados del procesamiento.
	 * @param longitud 			La longitud del campo debe ser multiplo de este valor.
	 *
	 * @return            <tt>false</tt> si la cadena proporcionada no cumple con la
	 *                    longitud o un multiplo de esta. <tt>true</tt> en caso contrario.
	 *
	 */
	private boolean validaLongitudMultiplo(
			int           numLinea,
			String        campo,
			String        nombreCampo,
			ResultadosGarEducativo rg,
			int           longitudMultiplo
	){

		boolean validacion = true;
		if ( campo == null || campo.length() == 0 || ( campo.length() % longitudMultiplo ) != 0 ) {

			rg.appendErrores("Error en la linea:");
			rg.appendErrores(String.valueOf(numLinea));
			rg.appendErrores(" El valor del campo ");
			rg.appendErrores(nombreCampo);
			rg.appendErrores(" no cumple con la longitud requerida: ");
			rg.appendErrores(String.valueOf(longitudMultiplo));
			rg.appendErrores(". \n");
			rg.setErrorLinea(true);
			validacion = false;

		}
		return validacion;

	}

	/**
	 * Metodo que valida un Id Credito repetido ( Cave del financiamiento )
	 * */
	private boolean validaClaveFinan(int numLinea,String campo,String nombreCampo,ResultadosGarEducativo rg){
		log.info("validaClaveFinan(E)");
		HashMap mapa = new HashMap();
		String 	msg_error 	= "";
		boolean 	hayError 	= false;

		mapa = rg.getMapaCredito();

		Set s	=	mapa.entrySet();

		Iterator it=s.iterator();
		while(it.hasNext()){

			Map.Entry entry = (Map.Entry)it.next();

			String key=	String.valueOf(entry.getKey());
			String value=(String)entry.getValue();

			if(campo.equals(value)){
				msg_error="El campo Clave del financiamiento se encuentra repetido en la l�nea: "+key;
				hayError = true;
				break;
			}
			//it.remove();
		}

		if(hayError){
			rg.appendErrores("Error en la linea:"+numLinea+" "+ msg_error+"\n");
			rg.setErrorLinea(true);
		}
		log.info("validaClaveFinan(S)");
		return !hayError;
	}

	public Registros getBasesOperaIf(String claveIf){
		log.info("CreditoEducativoBean::getBasesOperaIf(E)");
		try {
			if (claveIf == null || claveIf.equals("")) {
				throw new Exception("La clave de If es requerida");
			}
		} catch(Exception e) {
			String error =
					"ERROR. Error en los parametros recibidos. " + e.getMessage() + "\n" +
					"folio_carga=" + claveIf + "\n";
			throw new AppException(error);
		}
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();

			StringBuffer strSQL = new StringBuffer(
				"SELECT   ic_clave AS clave, cg_descripcion AS descripcion,"+
				"         DECODE (cg_modalidad, "+
				"                 1, 'Pari Passu', "+
				"                 2, 'Primeras Perdidas' "+
				"                ) AS modalidad "+
				"    FROM cecat_bases_opera "+
				"   WHERE ic_if = ? "+
				"ORDER BY ic_clave "
			);

			List params = new ArrayList();
			params.add(claveIf);

			log.debug("getBasesOperaIf:: strSQL=" + strSQL.toString() );
			log.debug("getBasesOperaIf:: params=" + params );

			Registros reg = con.consultarDB(strSQL.toString(), params);

			return reg;

		} catch(Exception e) {
			throw new AppException("Error al obtener los datos de acuse", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("CreditoEducativoBean::getBasesOperaIf(S)");
		}

	}
/*

*/
	/**
	 *
	 * @param rg
	 * @param nombreCampo
	 * @param tipoPersona
	 * @param numLinea
	 */
	private void validaTipoPersonaC( int numLinea, String tipoPersona, String nombreCampo, ResultadosGarEducativo rg){
		try{
			if(!tipoPersona.toUpperCase().equals("F") && !tipoPersona.toUpperCase().equals("M") ){
				rg.appendErrores("Error en la linea:"+numLinea+" El campo "+nombreCampo+" es incorrecto. \n");
				rg.setErrorLinea(true);
			}

		}catch(Exception e){
			rg.appendErrores("Error en la linea:"+numLinea+" El campo "+nombreCampo+" es incorrecto. \n");
			rg.setErrorLinea(true);
		}
	}
	/**
	 * @param rg
	 * @param nombreCampo
	 * @param tipoSexo
	 * @param numLinea
	 */
	private void validaTipoSexo( int numLinea, String tipoSexo, String nombreCampo, ResultadosGarEducativo rg){
		try{
			if(!tipoSexo.toUpperCase().equals("H") &&  !tipoSexo.toUpperCase().equals("M") ){
				rg.appendErrores("Error en la linea:"+numLinea+" El campo "+nombreCampo+" es incorrecto. \n");
				rg.setErrorLinea(true);
			}

		}catch(Exception e){
			rg.appendErrores("Error en la linea:"+numLinea+" El campo "+nombreCampo+" es incorrecto. \n");
			rg.setErrorLinea(true);
		}
	}

	/**
	 * Se generan las consultas para armar el archivo por unniversidad, y obtener el email de la misma.
	 * @return true: proceso exitoso
	 * @param llaves
	 * @param strDirectorio
	 */
	public boolean envioCorreoUniversidad(String strDirectorio, List llaves){

		boolean exito        = true;
		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		PreparedStatement ps1= null;
		ResultSet rs         = null;
		ResultSet rs1        = null;
		StringBuffer queryU  = new StringBuffer(); //Contiene el query para obtener los ic_universidad
		StringBuffer queryA  = new StringBuffer(); //Contiene el query para obtener los datos del archivo por universidad
		StringBuffer queryE  = new StringBuffer(); //Contiene el query para obtener el email de cada universidad
		List registros       = new ArrayList();
		List icUniversidades = new ArrayList();
		HashMap mapRegistros = new HashMap();
		String email         = "";
		String nafinElect    = "";

		log.info("envioCorreoUniversidad (E)");

		/***** Realizo la consulta para armar el archivo *****/
		try{
			con.conexionDB();
			/***** Primero obtengo las veces que aparece cada universidad *****/
			queryU = new StringBuffer();
			queryU.append(" SELECT DISTINCT(IC_UNIVERSIDAD) AS UNIVERSIDAD FROM CE_CONTENIDO_EDUCATIVO WHERE IC_REGISTRO IN( ");
			for(int i = 0; i < llaves.size(); i++){
				if(i > 0){
					queryU.append(", ");
				}
				queryU.append(llaves.get(i).toString());
			}
			queryU.append(" ) ");
			queryU.append(" GROUP BY IC_UNIVERSIDAD ");
			ps = con.queryPrecompilado(queryU.toString());
			log.info("Sentencia U: " + queryU.toString());
			rs = ps.executeQuery();

			while (rs.next()){
				icUniversidades.add(rs.getString("UNIVERSIDAD"));
			}
			/***** Limpio las variables *****/
			queryU = new StringBuffer();
			rs = null;
			ps = null;

			if(icUniversidades.size() > 0){
				for(int a = 0; a < icUniversidades.size(); a++){
					log.debug("icUniversidades: " + icUniversidades.get(a));
					queryA = new StringBuffer();
					registros = new ArrayList();
					queryA.append("SELECT CCE.IC_UNIVERSIDAD, "                                          );
					queryA.append("CCE.IC_IF, "                                                          );
					queryA.append("CU.CG_RAZON_SOCIAL AS UNIVERSIDAD, "                                  );
					queryA.append("CU.CG_NOMBRE_CAMPUS AS CAMPUS, "                                      );
					queryA.append("CI.CG_RAZON_SOCIAL AS INTERMEDIARIO_FINANCIERO, "                     );
					queryA.append("TO_CHAR(CCE.FN_FECHA_CARGA, 'dd/mm/yyyy HH24:MI:SS') AS FECHA_CARGA, ");
					queryA.append("CCE.CG_FINANCIAMIENTO AS CREDITO, "                                   );
					queryA.append("CCE.CG_RAZON_SOCIAL AS NOMBRE_ACREDITADO, "                           );
					queryA.append("CCE.CG_REGISTRO AS RFC, "                                             );
					queryA.append("CCE.CG_MATRICULA AS MATRICULA, "                                      );
					queryA.append("CCE.CG_DESC_ESTUDIOS AS ESTUDIOS, "                                   );
					queryA.append("TO_CHAR(CCE.DF_INGRESO, 'dd/mm/yyyy HH24:MI:SS') AS FECHA_INGRESO, "  );
					queryA.append("CCE.CG_ANIO AS ANIO_COLOCACION, "                                     );
					queryA.append("CCE.IC_PERIODO, "                                                     );
					queryA.append("CCE.CG_MODALIDAD AS MODALIDAD_PROGRAMA, "                             );
					queryA.append("TO_CHAR(CCE.DF_DISPOSICION, 'dd/mm/yyyy') AS FECHA_DISPOSICION, "     );
					queryA.append("CCE.IC_NUM_DISPOSICION AS NUM_DISPOSICION, "                          );
					queryA.append("CCE.FN_IMP_DISPOSICION AS IMP_DISPOSICION, "                          );
					queryA.append("CCE.FN_FINANCIAMIENTO AS MONTO_TOTAL, "                               );
					queryA.append("CCE.FN_SALDOLINEAS AS SALDO_LINEA, "                                  );
					queryA.append("DECODE (CBO.CG_MODALIDAD, "                                           );
					queryA.append("1, 'PARI PASSU', "                                                    );
					queryA.append("2, 'PRIMERAS PERDIDAS' "                                              );
					queryA.append(") AS MODALIDAD "                                                      );
					queryA.append("FROM CE_CONTENIDO_EDUCATIVO CCE, COMCAT_UNIVERSIDAD CU, "             );
					queryA.append("COMCAT_IF CI, CECAT_BASES_OPERA CBO "                                 );
					queryA.append("WHERE CCE.IC_UNIVERSIDAD = CU.IC_UNIVERSIDAD "                        );
					queryA.append("AND CCE.IC_IF = CI.IC_IF "                                            );
					queryA.append("AND CCE.IC_IF = CBO.IC_IF "                                           );
					queryA.append("AND CCE.IC_PROGRAMA = CBO.IC_CLAVE "                                  );
					queryA.append("AND CCE.IC_UNIVERSIDAD = " + icUniversidades.get(a) + " "             );
					queryA.append("AND CCE.IC_REGISTRO IN( "                                             );
					if(llaves.size() > 0){
						for(int i = 0; i < llaves.size(); i++){
							if(i > 0){
								queryA.append( ", ");
							}
							queryA.append(llaves.get(i).toString());
						}
					}
					queryA.append(" ) ");
					ps = con.queryPrecompilado(queryA.toString());
					log.info("Sentencia A: " + queryA.toString());
					rs = ps.executeQuery();
					while (rs.next()){

						mapRegistros = new HashMap();
						mapRegistros.put("universidad"       ,rs.getString("UNIVERSIDAD")              == null ?"": rs.getString("UNIVERSIDAD")             );
						mapRegistros.put("campus"            ,rs.getString("CAMPUS")                   == null ?"": rs.getString("CAMPUS")                  );
						mapRegistros.put("intermFinanciero"  ,rs.getString("INTERMEDIARIO_FINANCIERO") == null ?"": rs.getString("INTERMEDIARIO_FINANCIERO"));
						mapRegistros.put("fechaCarga"        ,rs.getString("FECHA_CARGA")              == null ?"": rs.getString("FECHA_CARGA")             );
						mapRegistros.put("credito"           ,rs.getString("CREDITO")                  == null ?"": rs.getString("CREDITO")                 );
						mapRegistros.put("nombreAcreditado"  ,rs.getString("NOMBRE_ACREDITADO")        == null ?"": rs.getString("NOMBRE_ACREDITADO")       );
						mapRegistros.put("rfc"               ,rs.getString("RFC")                      == null ?"": rs.getString("RFC")                     );
						mapRegistros.put("matricula"         ,rs.getString("MATRICULA")                == null ?"": rs.getString("MATRICULA")               );
						mapRegistros.put("estudios"          ,rs.getString("ESTUDIOS")                 == null ?"": rs.getString("ESTUDIOS")                );
						mapRegistros.put("fechaIngreso"      ,rs.getString("FECHA_INGRESO")            == null ?"": rs.getString("FECHA_INGRESO")           );
						mapRegistros.put("anioColocacion"    ,rs.getString("ANIO_COLOCACION")          == null ?"": rs.getString("ANIO_COLOCACION")         );
						mapRegistros.put("periodo"           ,rs.getString("IC_PERIODO")               == null ?"": rs.getString("IC_PERIODO")              );
						mapRegistros.put("modalidadPrograma" ,rs.getString("MODALIDAD_PROGRAMA")       == null ?"": rs.getString("MODALIDAD_PROGRAMA")      );
						mapRegistros.put("fechaDisposicion"  ,rs.getString("FECHA_DISPOSICION")        == null ?"": rs.getString("FECHA_DISPOSICION")       );
						mapRegistros.put("numDisposicion"    ,rs.getString("NUM_DISPOSICION")          == null ?"": rs.getString("NUM_DISPOSICION")         );
						mapRegistros.put("impDisposicion"    ,rs.getString("IMP_DISPOSICION")          == null ?"": rs.getString("IMP_DISPOSICION")         );
						mapRegistros.put("montoTotal"        ,rs.getString("MONTO_TOTAL")              == null ?"": rs.getString("MONTO_TOTAL")             );
						mapRegistros.put("saldoLinea"        ,rs.getString("SALDO_LINEA")              == null ?"": rs.getString("SALDO_LINEA")             );
						mapRegistros.put("modalidad"         ,rs.getString("MODALIDAD")                == null ?"": rs.getString("MODALIDAD")               );
						registros.add(mapRegistros);
					} // FIN DEL WHILE

					/***** Obtengo el email y el nafin electronico de la universidad *****/
					queryE = new StringBuffer();
					queryE.append("SELECT DISTINCT(D.IC_UNIVERSIDAD), "             );
					queryE.append("D.CG_EMAIL AS EMAIL, "                           );
					queryE.append("N.IC_NAFIN_ELECTRONICO AS NAFIN_ELECTRONICO "    );
					queryE.append("FROM COM_DOMICILIO D, COMREL_NAFIN N "           );
					queryE.append("WHERE D.IC_UNIVERSIDAD = N.IC_EPO_PYME_IF "      );
					queryE.append("AND N.CG_TIPO = 'U' "                            );
					queryE.append("AND D.IC_UNIVERSIDAD = " + icUniversidades.get(a));
					ps1 = con.queryPrecompilado(queryE.toString());
					log.info("Sentencia E: " + queryE.toString());
					rs1 = ps1.executeQuery();
					while(rs1.next()){
						email = rs1.getString("EMAIL") == null ?"": rs1.getString("EMAIL");
						nafinElect = rs1.getString("NAFIN_ELECTRONICO") == null ?"": rs1.getString("NAFIN_ELECTRONICO");
					}
					if(!nafinElect.equals("")){
						nafinElect = "-" + nafinElect;
					}
					/***** Mando a armar el archivo y lo envio por email *****/
					exito = generaCorreo(registros, strDirectorio, email, nafinElect);

				} // FIN DEL FOR
			} else{
				log.info("No se obtuvieron las universidades en la consulta.");
				exito = false;
			}

		} catch(Exception e){
			exito = false;
			log.warn("envioCorreoUniversidad.Exception. Error al realizar la consulta. " + e);
		} finally{
			try{
				rs.close();
				ps.close();
			} catch(Exception e){
				log .warn("Error al cerrar el PreparedStatement y el ResultSet. " + e);
			}
			try{
				rs1.close();
				ps1.close();
			} catch(Exception e){
				log .warn("Error al cerrar el PreparedStatement y el ResultSet. " + e);
			}
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.info("envioCorreoUniversidad (S)");
		return exito;
	}

	/**
	 * Genera un archivo csv y env�a un correo a la univarsidad indicada en el metodo envioCorreoUniversidad
	 * @return true: si se gener� el archivo y se envi� el correo exitsamente. false, alguno de los dos no se gener� correctamente
	 * @param nafinElectronico
	 * @param email
	 * @param strDirectorio
	 * @param registros
	 */
	public boolean generaCorreo(List registros, String strDirectorio, String email, String nafinElectronico){

		boolean exito = true;

		StringBuffer cuerpo  = new StringBuffer();
		StringBuffer asunto  = new StringBuffer();
		String nombreArchivo = "";
		String fechaActual   = "";

		HashMap mapaTemp          = new HashMap();
		StringBuffer contenido    = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer     = null;

		String universidad       = "";
		String campus            = "";
		String intermFinanciero  = "";
		String fechaCarga        = "";
		String credito           = "";
		String nombreAcreditado  = "";
		String rfc               = "";
		String matricula         = "";
		String estudios          = "";
		String fechaIngreso      = "";
		String anioColocacion    = "";
		String periodo           = "";
		String modalidadPrograma = "";
		String fechaDisposicion  = "";
		String numDisposicion    = "";
		String impDisposicion    = "";
		String montoTotal        = "";
		String saldoLinea        = "";
		String modalidad         = "";

		log.info("generaCorreo (E)");

		try{
			fechaActual = new java.text.SimpleDateFormat("ddMMyyyy").format(new java.util.Date());
			nombreArchivo = strDirectorio + "ConcentradoCreditoEducativo"+ nafinElectronico + "-" + fechaActual + ".csv";
			log.debug("nombreArchivo: " + nombreArchivo);

			/***** Si el archivo ya existe se elimina *****/
			try{
				File f = new File(nombreArchivo);
				if (f.delete())
					log.info("El archivo " + nombreArchivo + " ha sido borrado correctamente.");
				else
					log.info("El archivo " + nombreArchivo + " no se ha podido borrar.");
			} catch(Exception e){
				log.warn("generaCorreo.Exception( Error al intentar eliminar el archivo ): " + e);
			}

			/***** Se empieza a crear el archivo *****/
			writer = new OutputStreamWriter(new FileOutputStream(nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);

			contenido = new StringBuffer();
			contenido.append("Universidad,"                );
			contenido.append("Campus,"                     );
			contenido.append("Intermediario Financiero,"   );
			contenido.append("Fecha y Hora de Carga,"      );
			contenido.append("Credito,"                    );
			contenido.append("Nombre del acreditado,"      );
			contenido.append("RFC,"                        );
			contenido.append("Matricula,"                  );
			contenido.append("Estudios,"                   );
			contenido.append("Fecha de Ingreso,"           );
			contenido.append("A�o de colocaci�n,"          );
			contenido.append("Periodo educativo,"          );
			contenido.append("Modalidad programa,"         );
			contenido.append("Fecha ultima de disposici�n,");
			contenido.append("No. disposici�n,"            );
			contenido.append("Importe disposici�n,"        );
			contenido.append("Monto total,"                );
			contenido.append("Saldo de L�nea,"             );
			contenido.append("Modalidad, \n"               );

			for(int i = 0; i < registros.size(); i++){

				mapaTemp          = (HashMap)registros.get(i);
				universidad       = (String)mapaTemp.get("universidad");
				campus            = (String)mapaTemp.get("campus");
				intermFinanciero  = (String)mapaTemp.get("intermFinanciero");
				fechaCarga        = (String)mapaTemp.get("fechaCarga");
				credito           = (String)mapaTemp.get("credito");
				nombreAcreditado  = (String)mapaTemp.get("nombreAcreditado");
				rfc               = (String)mapaTemp.get("rfc");
				matricula         = (String)mapaTemp.get("matricula");
				estudios          = (String)mapaTemp.get("estudios");
				fechaIngreso      = (String)mapaTemp.get("fechaIngreso");
				anioColocacion    = (String)mapaTemp.get("anioColocacion");
				periodo           = (String)mapaTemp.get("periodo");
				modalidadPrograma = (String)mapaTemp.get("modalidadPrograma");
				fechaDisposicion  = (String)mapaTemp.get("fechaDisposicion");
				numDisposicion    = (String)mapaTemp.get("numDisposicion");
				impDisposicion    = (String)mapaTemp.get("impDisposicion");
				montoTotal        = (String)mapaTemp.get("montoTotal");
				saldoLinea        = (String)mapaTemp.get("saldoLinea");
				modalidad         = (String)mapaTemp.get("modalidad");

				contenido.append(universidad.replace(',',' ')      +", " );
				contenido.append(campus.replace(',',' ')           +", " );
				contenido.append(intermFinanciero.replace(',',' ') +", " );
				contenido.append(fechaCarga.replace(',',' ')       +", " );
				contenido.append(credito                           +", " );
				contenido.append(nombreAcreditado.replace(',',' ') +", " );
				contenido.append(rfc.replace(',',' ')              +", " );
				contenido.append(matricula.replace(',',' ')        +", " );
				contenido.append(estudios.replace(',',' ')         +", " );
				contenido.append(fechaIngreso.replace(',',' ')     +", " );
				contenido.append(anioColocacion.replace(',',' ')   +", " );
				contenido.append(periodo.replace(',',' ')          +", " );
				contenido.append(modalidadPrograma.replace(',',' ')+", " );
				contenido.append(fechaDisposicion.replace(',',' ') +", " );
				contenido.append(numDisposicion.replace(',',' ')   +", " );
				contenido.append(impDisposicion                    +", " );
				contenido.append(montoTotal                        +", " );
				contenido.append(saldoLinea                        +", " );
				contenido.append(modalidad                         +",\n");
			}
			buffer.write(contenido.toString());
			buffer.close();
			contenido = new StringBuffer();
			exito = true;
		} catch(Exception e){
			exito = false;
			log.warn("generaCorreo.Exception. Error al crear el archivo. " + e);
		}

		/***** Se arma el correo electr�nico *****/
		if(exito =true){
			try{
				log.debug("email: " + email);
				log.debug("archivoAdjunto: " + nombreArchivo);

				asunto.append("Cr�ditos Educativos");

				cuerpo.append("<span style=\"font-size:11pt;\"><span style=\"font-family: 'Calibri' \">");
				cuerpo.append("<br>"+ universidad +"<br><br>");
				cuerpo.append("Por medio del presente se adjunta el reporte anal&iacute;tico de todos los cr&eacute;ditos que ");
				cuerpo.append("fueron registrados por el Intermediario Financiero <b>"+ intermFinanciero +"</b> ");
				cuerpo.append("en el Programa Nacional de Financiamiento a la Educaci&oacute;n Superior.  <br><br>");
				cuerpo.append("Para validar la informaci&oacute;n deber&aacute;n ingresar al portal Nafinet:  <br>");
				cuerpo.append("http://www.nafin.com/portalnf/content/productos-y-servicios/programas-empresariales/credito-universitario.html  <br>");
				cuerpo.append("<br> ATENTAMENTE.  <br>");
				cuerpo.append("<b>Nacional Financiera, S.C.N.</b>  <br>");

				cuerpo.append("<br><b> Nota </b>: Este mensaje fue enviado desde una cuenta de notificaciones que no puede ");
				cuerpo.append("aceptar correos electr&oacute;nicos de entrada.<br> Por favor no responda a este mensaje.");

				log.debug("Cuerpo del correo: " + cuerpo.toString());

				Correo correoE = new Correo();
				ArrayList archivosAdjuntos = new ArrayList();
				HashMap hmArchivoAdjunto = new HashMap();
				hmArchivoAdjunto.put("FILE_FULL_PATH", nombreArchivo);
				archivosAdjuntos.add(hmArchivoAdjunto);

				correoE.enviaCorreoConDatosAdjuntos("info@nafin.gob.mx", email, "", asunto.toString(), cuerpo.toString(), null, archivosAdjuntos);
				exito = true;

			} catch(Exception e){
				exito = false;
				log.warn("generaCorreo.Exception. Error al enviar el correo. " + e);
			}
		}
		log.info("generaCorreo (S)");
		return exito;
	}

}