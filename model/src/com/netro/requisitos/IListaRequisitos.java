/*************************************************************************************
*
* Nombre de Clase o Archivo: IListaRequisitos.java
*
* Versi�n: 		  1.0
*
* Fecha Creaci�n: 11/febrero/2002
*
* Autor:          Ricardo Jim�nez Vargas
*
* Fecha Ult. Modificaci�n:
*
* Descripci�n de Clase: Stringerface remota del EJB de Lista de Requisitos
*
*************************************************************************************/

package com.netro.requisitos;

import com.netro.exception.*;
import netropology.utilerias.*;
import javax.ejb.EJBObject;
import javax.ejb.Remote;

@Remote
public interface IListaRequisitos {

    public void vactualizarDescReq1(String ecValorNuevo, String eiCveEPO) throws NafinException;
/*
    public void vactualizarDescReq3(String ecValorNuevo, String eiCveEPO, String eiPYME) throws NafinException;

    public void vactualizarDescReq4(String ecValorNuevo, String eiCvePYME, String esCveUsuario) throws NafinException;
*/
    public void vactualizarDescReq5(String ecValorNuevo, String eiCveEPO, String eiPYME) throws NafinException;
/*
    public void vactualizarDescReq6(String ecValorNuevo, String eiCveEPO, String eiPYME) throws NafinException;
*/
    public void vactualizarDescReq7(String ecValorNuevo, String eiCveEPO, String eiIF, String eiCtaBancaria, String eiPyme) throws NafinException;

    public void vactualizarDescReq8(String ecValorNuevo, String eiCveEPO, String eiPYME) throws NafinException;

    public void vactualizarAntReq2(String ecValorNuevo, String eiCveEPO, String eiPYME, String eiCveProducto) throws NafinException;

    public void vactualizarAntReq3(String ecValorNuevo, String eiCveEPO, String eiPYME, String eiCveProducto) throws NafinException;

    public void vactualizarAntReq7(String ecValorNuevo, String eiCveEPO, String eiPYME, String eiCveProducto) throws NafinException;

    public void vactualizarDisReq4(String ecValorNuevo, String eiCveEPO, String eiPYME, String eiCveProducto) throws NafinException;

    public abstract void vactualizarDisReq8(String ecValorNuevo, String eiCveEPO, String eiPYME) throws NafinException;				
				
    public void vactualizarDistClavesAltoR(String ic_epo,String ic_pyme,String valor) throws NafinException;				
				
}

