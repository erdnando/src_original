package com.netro.requisitos;

import com.netro.exception.NafinException;

import java.sql.ResultSet;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import netropology.utilerias.AccesoDB;

/*************************************************************************************
 *
 * Nombre de Clase o de Archivo: CListaRequisitosBean.java
 *
 * Versión: 		  1.0
 *
 * Fecha Creación: 11/febrero/2002
 *
 * Autor:          Ricardo Jimenez Vargas
 *
 * Fecha Ult. Modificación:
 *
 * Descripción de Clase: Clase que implementa los métodos para el EJB de Lista de Requisitos.
 *
 *************************************************************************************/
@Stateless(name = "ListaRequisitosEJB" , mappedName = "ListaRequisitosEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class CListaRequisitosBean implements IListaRequisitos {

	/*********************************************************************************************
	*
	*	    void vactualizarDescReq1()
	*
	*********************************************************************************************/
	public void vactualizarDescReq1(String ecValorNuevo, String eiCveEPO)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarDescReq1(E)");
		try {
			lodbConexion.conexionDB();
			lsCadenaSQL = "UPDATE comcat_epo SET cs_conv_nafin_epo = '" + ecValorNuevo + "', df_conv_nafin_epo = SYSDATE where ic_epo = " + eiCveEPO;
			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0001";
				throw new NafinException("REQ0001");    //Error al actualizar el Requisito 1 de Descuento Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarDescReq1(S)");
		}
	}


	/*********************************************************************************************
	*
	*	    Vector ovconsultarReq2()
	*
	********************************************************************************************
	public Vector ovconsultarReq2()
			throws NafinException{
		String lsCadenaSQL  = "";

		Vector lovRegistro = null;
		Vector lovDatosTabla = new Vector();

		ResultSet lrsSel = null;
		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::ovconsultarReq2(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "SELECT IC_EPO AS CLAVE,CG_RAZON_SOCIAL AS DESCRIPCION ";
			lsCadenaSQL += " FROM comcat_epo";
			lsCadenaSQL += " ORDER BY DESCRIPCION";

			lrsSel = lodbConexion.queryDB(lsCadenaSQL);

			if (lrsSel.next())
				do {
					lovRegistro = new Vector();

					lovRegistro.addElement(lrsSel.getString(1));
					lovRegistro.addElement(lrsSel.getString(2));

					lovDatosTabla.addElement(lovRegistro);

				} while (lrsSel.next());
			else
				throw new NafinException("PARM0010");

			return lovDatosTabla;
		} catch (NafinException Error){
			throw Error;
		} catch (Exception epError){
			throw new NafinException("SIST0001");
		} finally {
				lodbConexion.cierraConexionDB();
			System.out.println(" ListaRequisitosEJB::ovconsultarReq2(S)");
		}
	}

	*/
	/*********************************************************************************************
	*
	*	    void vactualizarDescReq3()
	*
	********************************************************************************************
	public void vactualizarDescReq3(String ecValorNuevo, String eiCveEPO, String eiPYME)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarDescReq3(E)");
		try {
			lodbConexion.conexionDB();
			lsCadenaSQL = "UPDATE comrel_pyme_epo SET cs_carta_bajo = '" + ecValorNuevo + "', df_carta_bajo = SYSDATE where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME;
			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0003";
				throw new NafinException("REQ0003");    //Error al actualizar el Requisito 3 de Descuento Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarDescReq3(S)");
		}
	}
	*/
	/*********************************************************************************************
	*
	*	    void vactualizarDescReq4()
	*
	********************************************************************************************
	public void vactualizarDescReq4(String ecValorNuevo, String eiCvePYME, String esCveUsuario)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarDescReq4(E)");
		try {
			lodbConexion.conexionDB();
			lsCadenaSQL = "UPDATE  SET cs_visitas = '" + ecValorNuevo + "', df_visitas = SYSDATE where ic_pyme = " + eiCvePYME + " and ic_usuario = '" + esCveUsuario + "'";
			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0004";
				throw new NafinException("REQ0004");    //Error al actualizar el Requisito 4 de Descuento Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarDescReq4(S)");
		}
	}
	*/
	/*********************************************************************************************
	*
	*	    void vactualizarDescReq5()
	*
	*********************************************************************************************/
	public void vactualizarDescReq5(String ecValorNuevo, String eiCveEPO, String eiPYME)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarDescReq5(E)");
		try {
			lodbConexion.conexionDB();
			lsCadenaSQL = "UPDATE comrel_pyme_epo SET cs_aceptacion = '" + ecValorNuevo + "', df_aceptacion = SYSDATE where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME;
			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0005";
				throw new NafinException("REQ0005");    //Error al actualizar el Requisito 5 de Descuento Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarDescReq5(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    void vactualizarDescReq6()
	*
	********************************************************************************************
	public void vactualizarDescReq6(String ecValorNuevo, String eiCveEPO, String eiPYME)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarDescReq6(E)");
		try {
			lodbConexion.conexionDB();
			lsCadenaSQL = "UPDATE comrel_pyme_epo SET cs_carta_alto = '" + ecValorNuevo + "', df_carta_alto = SYSDATE where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME;
			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0006";
				throw new NafinException("REQ0006");    //Error al actualizar el Requisito 6 de Descuento Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarDescReq6(S)");
		}
	}
	*/
	/*********************************************************************************************
	*
	*	    void vactualizarDescReq7()
	*
	*********************************************************************************************/
	public void vactualizarDescReq7(String ecValorNuevo, String eiCveEPO, String eiIF, String eiCtaBancaria, String eiPyme)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "",lsCadenaSQL1 = "", lsHabilitado = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarDescReq7(E)");
		try {
			if(ecValorNuevo.equals("S"))
				lsHabilitado = "H";
			else
				lsHabilitado = "R";
			lodbConexion.conexionDB();
			lsCadenaSQL = "UPDATE comrel_pyme_if SET cs_vobo_if = '" + ecValorNuevo + "', df_vobo_if = SYSDATE where ic_cuenta_bancaria = " + eiCtaBancaria + " and ic_epo = " + eiCveEPO + " and ic_if = " + eiIF;
			lsCadenaSQL1 = "UPDATE comrel_pyme_epo SET cs_aceptacion = '" + lsHabilitado + "', df_aceptacion = SYSDATE where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPyme;
			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0007";
				throw new NafinException("REQ0007");    //Error al actualizar el Requisito 7 de Descuento Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarDescReq7(S)");
		}
	}


	/*********************************************************************************************
	*
	*	    void vactualizarDescReq8()
	*
	*********************************************************************************************/
	public void vactualizarDescReq8(String ecValorNuevo, String eiCveEPO, String eiPYME)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";
		String lsCadenaSQL = "";

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarDescReq8(E)");

		try {

			/* ncm ya no se hace cambio de perfil
			if("H".equals(ecValorNuevo)) {
				strPerfilCambio = "ADMIN PYME";
			} else {
				strPerfilCambio = "ADMIN PYME BAJO";
			}*/

			// Modificado EGB 03/03/2004 FODA 002-2004
			lsCadenaSQL = "UPDATE comrel_pyme_epo SET cs_aceptacion = '" + ecValorNuevo + "' where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME;
			System.out.println("vactualizarDescReq8:  "+lsCadenaSQL);
			//lsCadenaSQL1 = "UPDATE seg _rel_usuario_perfil SET cc_perfil = '" + strPerfilCambio + "' where ic_usuario in (select ic_usuario from seg _usuario where ic_pyme = " + eiPYME + " and cs_tipo_afiliacion = 'A')";

			//System.out.println("**********inicio************");
			//System.out.println(utilusr.getUsuariosxAfiliado(eiPYME,"P"));

			/* ncm ya no se hace cambio de perfil
			List listaLoginUsuarios = utilusr.getUsuariosxAfiliado(eiPYME,"P");
			Iterator iteratorUsuarios = listaLoginUsuarios.iterator();

			while(iteratorUsuarios.hasNext()){
				String usuario=(String) iteratorUsuarios.next();
				System.out.println("usuario:"+usuario+"*");
				System.out.println("1 getPerfilUsuario:"+utilusr.getPerfilUsuario(usuario)+"*");
				utilusr.setPerfilUsuario(usuario, strPerfilCambio);
				System.out.println("2 getPerfilUsuario:"+utilusr.getPerfilUsuario(usuario)+"*");
			}*/


			try{
				lodbConexion.conexionDB();
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0008";
				throw new NafinException("REQ0008");    //Error al actualizar el Requisito 8 de Descuento Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarDescReq8(S)");
		}
	}


	/*********************************************************************************************
	*
	*	    void vactualizarAntReq2()
	*
	*********************************************************************************************/
	public void vactualizarAntReq2(String ecValorNuevo, String eiCveEPO, String eiPYME, String eiCveProducto)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";
		ResultSet lrsSel = null;

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarAntReq2(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "SELECT count(*) FROM comrel_pyme_epo_x_producto WHERE ic_epo = " + eiCveEPO+" and ic_pyme = " + eiPYME + " and ic_producto_nafin = " + eiCveProducto;
			lrsSel = lodbConexion.queryDB(lsCadenaSQL);
			if(lrsSel.next()){
				if(lrsSel.getInt(1) == 0)
					lsCadenaSQL = "INSERT INTO comrel_pyme_epo_x_producto (ic_epo,ic_pyme,ic_producto_nafin,cs_habilitado) VALUES (" + eiCveEPO + "," + eiPYME + "," + eiCveProducto + ",'" + ecValorNuevo + "')";
				else
					lsCadenaSQL = "UPDATE comrel_pyme_epo_x_producto SET cs_habilitado = '" + ecValorNuevo + "', df_aceptacion_pyme = SYSDATE where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME + " and ic_producto_nafin = " + eiCveProducto;
			}
			lrsSel.close();
			lodbConexion.cierraStatement();

			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0009";
				throw new NafinException("REQ0009");    //Error al actualizar el Requisito 2 de Anticipo Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarAntReq2(S)");
		}
	}


	/*********************************************************************************************
	*
	*	    void vactualizarAntReq3()
	*
	*********************************************************************************************/
	public void vactualizarAntReq3(String ecValorNuevo, String eiCveEPO, String eiPYME, String eiCveProducto)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";
		ResultSet lrsSel = null;

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarAntReq3(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "SELECT count(*) FROM comrel_pyme_epo_x_producto WHERE ic_epo = " + eiCveEPO+" and ic_pyme = " + eiPYME + " and ic_producto_nafin = " + eiCveProducto;
			lrsSel = lodbConexion.queryDB(lsCadenaSQL);
			if(lrsSel.next()){
				if(lrsSel.getInt(1) == 0)
					lsCadenaSQL = "INSERT INTO comrel_pyme_epo_x_producto (ic_epo,ic_pyme,ic_producto_nafin,cs_aceptacion_pyme) VALUES (" + eiCveEPO + "," + eiPYME + "," + eiCveProducto + ",'" + ecValorNuevo + "')";
				else
					lsCadenaSQL = "UPDATE comrel_pyme_epo_x_producto SET cs_aceptacion_pyme = '" + ecValorNuevo + "', df_aceptacion_pyme = SYSDATE where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME + " and ic_producto_nafin = " + eiCveProducto;
			}
			lrsSel.close();
			lodbConexion.cierraStatement();

			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0010";
				throw new NafinException("REQ0010");    //Error al actualizar el Requisito 3 de Anticipo Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarAntReq3(S)");
		}
	}


	/*********************************************************************************************
	*
	*	    void vactualizarDisReq4()
	*
	*********************************************************************************************/
	public void vactualizarDisReq4(String ecValorNuevo, String eiCveEPO, String eiPYME, String eiCveProducto)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";
		ResultSet lrsSel = null;

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarAntReq3(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "SELECT count(*) FROM comrel_pyme_epo_x_producto WHERE ic_epo = " + eiCveEPO+" and ic_pyme = " + eiPYME + " and ic_producto_nafin = " + eiCveProducto;
			lrsSel = lodbConexion.queryDB(lsCadenaSQL);
			if(lrsSel.next()){
				if(lrsSel.getInt(1) == 0)
					lsCadenaSQL = "INSERT INTO comrel_pyme_epo_x_producto (ic_epo,ic_pyme,ic_producto_nafin,cs_contrato_firmado) VALUES (" + eiCveEPO + "," + eiPYME + "," + eiCveProducto + ",'" + ecValorNuevo + "')";
				else
					lsCadenaSQL = "UPDATE comrel_pyme_epo_x_producto SET cs_contrato_firmado = '" + ecValorNuevo + "', df_aceptacion_pyme = SYSDATE where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME + " and ic_producto_nafin = " + eiCveProducto;
			}
			lrsSel.close();
			lodbConexion.cierraStatement();

			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0010";
				throw new NafinException("REQ0010");    //Error al actualizar el Requisito 3 de Anticipo Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarAntDis4(S)");
		}
	}


	/*********************************************************************************************
	*
	*	    void vactualizarAntReq7()
	*
	*********************************************************************************************/
	public void vactualizarAntReq7(String ecValorNuevo, String eiCveEPO, String eiPYME, String eiCveProducto)
		throws NafinException{
		boolean lbOK = true;

		String lsCodError = "OK";

		String lsCadenaSQL = "";
		ResultSet lrsSel = null;

		AccesoDB lodbConexion = new AccesoDB();

		System.out.println(" ListaRequisitosEJB::vactualizarAntReq7(E)");
		try {
			lodbConexion.conexionDB();

			lsCadenaSQL = "SELECT count(*) FROM comrel_pyme_epo_x_producto WHERE ic_epo = " + eiCveEPO+" and ic_pyme = " + eiPYME + " and ic_producto_nafin = " + eiCveProducto;
			lrsSel = lodbConexion.queryDB(lsCadenaSQL);
			if(lrsSel.next()){
				if(lrsSel.getInt(1) == 0)
					lsCadenaSQL = "INSERT INTO comrel_pyme_epo_x_producto (ic_epo,ic_pyme,ic_producto_nafin,cs_habilitado) VALUES (" + eiCveEPO + "," + eiPYME + "," + eiCveProducto + ",'" + ecValorNuevo + "')";
				else
					lsCadenaSQL = "UPDATE comrel_pyme_epo_x_producto SET cs_habilitado = '" + ecValorNuevo + "', df_aceptacion_pyme = SYSDATE where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME + " and ic_producto_nafin = " + eiCveProducto;
			}
			lrsSel.close();
			lodbConexion.cierraStatement();

			try {
				lodbConexion.ejecutaSQL(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQ0011";
				throw new NafinException("REQ0011");    //Error al actualizar el Requisito 7 de Anticipo Electrónico
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			throw new NafinException("SIST0001");
		} finally {
			lodbConexion.terminaTransaccion(lbOK);
			lodbConexion.cierraConexionDB();
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarAntReq7(S)");
		}
	}

	/*********************************************************************************************
	*
	*	    boolean vactualizarDisReq8()	Agregado por MPCS: 28/abr/03
	*
	*********************************************************************************************/

	public void vactualizarDisReq8(String ecValorNuevo, String eiCveEPO, String eiPYME)
		throws NafinException{
		boolean lbOK = true;
		String lsCodError = "OK";
		String qrySentencia="";
		String lsCadenaSQL = "";
		ResultSet lrsSel = null,rs=null;
		String EstatusFinal = "S"; //Estatus para permitir la operación
		String EstatusAnterior ="N"; //Estatus que deshabilita la operacion
		System.out.println(" ListaRequisitosEJB::vactualizarDisReq8(E)");
		AccesoDB 	con = null;
		try {
			con = new AccesoDB();
			con.conexionDB();
			/* Valida la situacion actual en la bandera */
			lsCadenaSQL = "select cs_habilitado from comrel_pyme_epo_x_producto  where ic_pyme = " + eiPYME + " and ic_epo =" + eiCveEPO+ "  and ic_producto_nafin = 4 ";
			System.out.println(lsCadenaSQL+"******");
			lrsSel = con.queryDB(lsCadenaSQL);
			if(lrsSel.next()){
				System.out.println("BD:"+lrsSel.getString("cs_habilitado")+" "+ecValorNuevo);
				if((lrsSel.getString("cs_habilitado")).equals("S")&&ecValorNuevo.equals("N"))
					lsCadenaSQL = "UPDATE comrel_pyme_epo_x_producto SET cs_habilitado = '"+EstatusAnterior+"' where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME + " and ic_producto_nafin = 4 ";
				else if (!((lrsSel.getString("cs_habilitado")).equals("S")) && ecValorNuevo.equals("S"))
				{   System.out.println("Habilita la bandera a "+ecValorNuevo+"...");
					qrySentencia=" select 1 "+
									" from comrel_pyme_epo_x_producto pep "+
									" where exists (select 1 "+
									" 	  		  from comrel_producto_epo pe "+
									" 			  where pe.ic_epo="+ eiCveEPO +" "+
									" 			  and   ic_producto_nafin=4 "+
									" 			  and   cs_habilitado='S') "+
									" and exists ( select 1 "+
									" 		   	 from comcat_pyme p "+
									" 			 where p.ic_pyme="+ eiPYME +" "+
									" 			 and   p.CS_TIPO_AFILIA_DISTRI='C') "+
									" and exists ( select 1 "+
									" 		   	 from com_linea_credito lc "+
									" 			 where lc.ic_epo = "+ eiCveEPO +"  "+
									" 			 and trunc(lc.df_vencimiento) >= trunc(SYSDATE) "+
									"  			 and lc.cg_tipo_solicitud in ('I','A','R')  "+
									" 			 and lc.ic_producto_nafin = 4) "+
									" and exists ( select 1 "+
									" 		   	 from com_tasa_if_pyme tp "+
									" 			 where tp.ic_pyme= "+ eiPYME +" "+
									" 			 and tp.ic_producto_nafin= 4   "+
									" 			 union all  "+
									" 			 select 1 "+
									" 			 from com_tasa_general tg "+
									" 			 where tg.ic_producto_nafin= 4) "+
									" and cs_aceptacion_pyme='S' "+
									" and cs_carta_alto='S'	"+
									" and ic_producto_nafin=4 "+
									" and cg_tipo_credito='C' "+
									" and ic_pyme="+ eiPYME +" "+
									" and ic_epo= "+ eiCveEPO +
									" union all "+
									" select 1 "+
									" from comrel_pyme_epo_x_producto pep "+
									" where exists (select 1 "+
									" 	  		  from comrel_producto_epo pe "+
									" 			  where pe.ic_epo="+ eiCveEPO +" "+
									" 			  and   ic_producto_nafin=4 "+
									" 			  and   cs_habilitado='S') "+
									" and exists ( select 1 "+
									" 		   	 from comcat_pyme p "+
									" 			 where p.ic_pyme="+ eiPYME +" "+
									" 			 and   p.CS_TIPO_AFILIA_DISTRI='C') "+
									" and exists ( select 1 "+
									" 		   	 from dis_linea_credito_dm lc "+
									" 			 where lc.ic_epo = "+ eiCveEPO +"  "+
									" 			 and trunc(lc.df_vencimiento) >= trunc(SYSDATE) "+
									"  			 and lc.cg_tipo_solicitud in ('I','A','R')  "+
									" 			 and lc.ic_producto_nafin = 4) "+
									" and exists ( select 1 "+
									" 		   	 from com_tasa_if_epo te "+
									" 			 where te.ic_epo= "+ eiCveEPO +" "+
									" 			 and te.ic_producto_nafin= 4) "+
									" and cs_aceptacion_pyme='S' "+
									" and cs_carta_alto='S'	"+
									" and ic_producto_nafin=4 "+
									" and cg_tipo_credito='D' "+
									" and ic_pyme="+ eiPYME +" "+
									" and ic_epo= "+ eiCveEPO ;
					try
					{ rs=con.queryDB(qrySentencia);
					  if  (rs.next()) //El registro cumplio con todas las condiciones
					  	lsCadenaSQL = "UPDATE comrel_pyme_epo_x_producto SET cs_habilitado = '"+EstatusFinal+"' , df_habilitado = SYSDATE where ic_epo = " + eiCveEPO + " and ic_pyme = " + eiPYME + " and ic_producto_nafin = 4 ";
				  	  else
				  	    throw new NafinException("REQU0100");
					} catch (NafinException e)
					{
						lbOK = false;
						throw e;
					} catch (Exception e)
					{   System.out.println(" Error al verificar los demas requisitos ");
						lbOK = false;
						throw e;
					}
				}
			} else
			{	throw new NafinException("REQU0100"); //No existe la relacion pyme-epo-x-producto
			}
			lrsSel.close();
			con.cierraStatement();
			try {
				if (!lsCadenaSQL.equals(""))
						con.ejecutaSQL(lsCadenaSQL);
				con.terminaTransaccion(true);
				System.out.println(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQU0101";
				throw new NafinException("REQU0101");    //Error al actualizar el Requisito 8 de Financiamiento a Distribuidores
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			System.out.println("ListaRequisitosEJB::VActualizaDisReq8 Exception "+epError);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				if( !lsCodError.equals("OK") )
					con.terminaTransaccion(false);
				else
					con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarDisReq8(S)");
		}
	} //Fin de vactualizarDisReq8

	public void vactualizarDistClavesAltoR(String ic_epo,String ic_pyme,String valor)
		throws NafinException{
		boolean lbOK = true;
		String lsCodError = "OK";
		String lsCadenaSQL = "";
		System.out.println(" ListaRequisitosEJB::vactualizarDistClavesAltoR(E)");
		AccesoDB 	con = null;
		try {
			con = new AccesoDB();
			con.conexionDB();
			lsCadenaSQL =
				" UPDATE comrel_pyme_epo_x_producto "   +
				" SET cs_carta_alto = 'N' "   +
				" WHERE ic_pyme = "+ic_pyme+" "   +
				" AND ic_epo = "+ic_epo+" "   +
				" AND ic_producto_nafin = 4"  ;

			try {
				if (!lsCadenaSQL.equals(""))
						con.ejecutaSQL(lsCadenaSQL);
				con.terminaTransaccion(true);
				System.out.println(lsCadenaSQL);
			}catch (Exception epError){
				lsCodError = "REQU0102";
				throw new NafinException("REQU0102");    //Error al actualizar el Requisito 8 de Financiamiento a Distribuidores
			}
		} catch (NafinException Error){
			lbOK = false;
			throw Error;
		} catch (Exception epError){
			lbOK = false;
			System.out.println("ListaRequisitosEJB::vactualizarDistClavesAltoR Exception "+epError);
			throw new NafinException("SIST0001");
		} finally {
			if(con.hayConexionAbierta()){
				if( !lsCodError.equals("OK") )
					con.terminaTransaccion(false);
				else
					con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
			if( !lsCodError.equals("OK") )
				throw new NafinException(lsCodError);
			System.out.println(" ListaRequisitosEJB::vactualizarDistClavesAltoR(S)");
		}
	} //Fin de vactualizarDistClavesAltoR



}// Fin de la Clase