package com.netro.parametrosgrales;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.naming.NamingException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Fecha;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

@Stateless(name = "ParametrosGralesEJB" , mappedName = "ParametrosGralesEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class ParametrosGralesBean implements ParametrosGrales {

	private final static Log log = ServiceLocator.getInstance().getLog(ParametrosGralesBean.class);

	//-------------------------------------------------------------------------------------------
	//	void ovactualizarProducto()
	//-------------------------------------------------------------------------------------------
	// Modificado 19/03/2003	--CARP
	public void ovactualizarProducto( 	String 	strCsAutomaticoSusceptible, /*String strCgTipoCobranza,*/
										/*String intInDiasProcesoCobro,*/		String intIcPeriodoMaxDisp,
										String intIcPeriodoMaxVig,			String intIcNumeroMaxAmort,
										/*String strIgClausulaVencimiento, 	String strIgTipoCalculoInteres,
										String intIcPeriodoCreditoMora,*/		String strCgRevisionTasaMora,
                                        /*String esBaseCalculo,*/				String cs_troya,
                                        String ig_numero_max_disp,			String ig_numero_min_amort,
                                        String strCsDetalle,				String lsdias_lib_linea,
                                        String lsnumeroamortizacion_exp,
                                        String lsperiodovigencia_exp,		String lsTipoPagoCapital,
                                        String lsTipoPagoCapital_exp,		String txtNumMaxDisp_exp,
                                        String lsdias_lib_linea_e, String lsnumeroamortizacion_emp,
                                        String lsnumeroamortizacion_e_emp, String lsTipoPagoCapital_emp,
                                        String lsTipoPagoCapital_e_emp, String txtNumMaxDisp_emp,
                                        String txtNumMaxDisp_e_emp)
        throws NafinException {

		AccesoDB		con 			= null;
		String			qrySentencia	= "";
		boolean			resultado		= true;

		try {
			con = new AccesoDB();
			con.conexionDB();

	        qrySentencia =	"Update comcat_producto_nafin " +
						"   Set cs_automatico_susceptible = '" + strCsAutomaticoSusceptible + "' ," +
					//	"   cg_tipo_cobranza = '" + strCgTipoCobranza + "', " +
					//	"   in_dias_proceso_cobro = " + intInDiasProcesoCobro + ", " +
						"   ic_periodo_max_disp = " + intIcPeriodoMaxDisp + ", " +
						"   ic_periodo_max_vig = " + intIcPeriodoMaxVig + ", " +
						"   ic_numero_max_amort = " + intIcNumeroMaxAmort + ", " +
					//	"   ig_clausula_vencimiento = " + strIgClausulaVencimiento + ", " +
					//	"   ig_tipo_calculo_interes = " + strIgTipoCalculoInteres + ", " +
					//	"   ic_periodo_credito_mora = " + intIcPeriodoCreditoMora + ", " +
						"   cg_revision_tasa_mora = '" + strCgRevisionTasaMora + "' " +
						//"  , cg_base_calculo  = '"+  esBaseCalculo +"'"+	//Agregado 19/03/2003	--CARP
						"   ,cs_troya = '" + cs_troya + "', ig_numero_max_disp = "+ig_numero_max_disp+" "+
						"	,ig_numero_min_amort = "+ig_numero_min_amort+" "+
						"	,cs_detalle_pyme = '"+strCsDetalle+"' "+ //strCsDetalle - Agregado 21/02/2006 -- NCM
						"	,ig_dias_lib_exp = "+lsdias_lib_linea+" "+ //FODA 11 -2006 SMJ
						"	,ig_dias_lib_emp = "+lsdias_lib_linea_e+" "+ //FODA 11 -2006 SMJ
						"	,ig_numero_max_amort_exp = "+lsnumeroamortizacion_exp+" "+
						"	,ig_numero_max_amort_exp_emp = "+lsnumeroamortizacion_emp+" "+
						//"	,ig_periodo_max_disp_exp = "+lsperiododisposicion_exp+" "+
						"	,ig_periodo_max_vig_exp = "+lsperiodovigencia_exp+" "+
						"	,ig_numero_max_amort_emp = "+lsnumeroamortizacion_e_emp+" "+

						"	,ic_tabla_amort= "+lsTipoPagoCapital+" "+
						"	,ic_tabla_amort_exp= "+lsTipoPagoCapital_exp+" "+

						"	,ic_tabla_amort_exp_emp = "+lsTipoPagoCapital_e_emp+" "+
						"	,ic_tabla_amort_emp = "+lsTipoPagoCapital_emp+" "+

						"	,ig_numero_max_disp_exp= "+txtNumMaxDisp_exp+" "+
						"	,ig_numero_max_disp_emp= "+txtNumMaxDisp_emp+" "+
						"	,ig_numero_max_disp_e_emp= "+txtNumMaxDisp_e_emp+" "+
						" Where ic_producto_nafin = 5 ";

			//System.out.print("ParametrosGralesBean :: ovactualizarProducto :: Query "+qrySentencia);

			con.ejecutaSQL(qrySentencia);

		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	} /*Fin del M�todo ovactualizarProducto*/

	//-------------------------------------------------------------------------------------------
	//	void ovinsertarProducto()
	//
	//-------------------------------------------------------------------------------------------
	// Modificado 19/03/2003	--CARP
	public void ovinsertarProducto( 	String 	strCsAutomaticoSusceptible, /*String strCgTipoCobranza,*/
										/*String intInDiasProcesoCobro,*/		String intIcPeriodoMaxDisp,
										String intIcPeriodoMaxVig,			String intIcNumeroMaxAmort,
										/*	String strIgClausulaVencimiento, String strIgTipoCalculoInteres,
										String intIcPeriodoCreditoMora,	*/	String strCgRevisionTasaMora,
                                        /*String esBaseCalculo,*/				String cs_troya,
                                        String ig_numero_max_disp,			String ig_numero_min_amort,
                                        String strCsDetalle,				String lsdias_lib_linea,
                                        String lsnumeroamortizacion_exp,
                                        String lsperiodovigencia_exp,		String lsTipoPagoCapital,
                                        String lsTipoPagoCapital_exp,		String txtNumMaxDisp_exp,
                                        String lsdias_lib_linea_e, String lsnumeroamortizacion_emp,
                                        String lsnumeroamortizacion_e_emp, String lsTipoPagoCapital_emp,
                                        String lsTipoPagoCapital_e_emp, String txtNumMaxDisp_emp,
                                        String txtNumMaxDisp_e_emp)
   throws NafinException {

		AccesoDB		con 			= null;
		String			qrySentencia	= "";
		boolean			resultado		= true;

		try {
			con = new AccesoDB();
			con.conexionDB();

			qrySentencia = " INSERT INTO comcat_producto_nafin  " +
							" ( " +
							" ic_producto_nafin, cs_automatico_susceptible, "+
			//				" ic_producto_nafin, cs_automatico_susceptible, cg_tipo_cobranza, in_dias_proceso_cobro, "+
							" ic_periodo_max_disp, ic_periodo_max_vig, ic_numero_max_amort, ig_clausula_vencimiento,  "+
							" ig_tipo_calculo_interes, ic_periodo_credito_mora, cg_revision_tasa_mora "+
							"  ,cs_troya,ig_numero_max_disp,ig_numero_min_amort  "+	//Agregado 19/03/2003	--CARP
//							"  , cg_base_calculo,cs_troya,ig_numero_max_disp,ig_numero_min_amort  "+	//Agregado 19/03/2003	--CARP
							" , cs_detalle_pyme,ig_dias_lib_exp,ig_numero_max_amort_exp,ig_periodo_max_vig_exp,ic_tabla_amort, "+
							" ic_tabla_amort_exp,ig_numero_max_disp_exp,ig_dias_lib_emp,ig_numero_max_amort_exp_emp,"+
							" ig_numero_max_amort_emp,ic_tabla_amort_exp_emp,ic_tabla_amort_emp,ig_numero_max_disp_emp,ig_numero_max_disp_e_emp) "+
							" VALUES "+
							" ( "+
							" 5, '" + strCsAutomaticoSusceptible + "' , '" +
			//				" 5, '" + strCsAutomaticoSusceptible + "' , '" + strCgTipoCobranza + "', " + intInDiasProcesoCobro + ", "+
							" " + intIcPeriodoMaxDisp + ", " + intIcPeriodoMaxVig + ", " + intIcNumeroMaxAmort + ", " +
			//				" " + intIcPeriodoMaxDisp + ", " + intIcPeriodoMaxVig + ", " + intIcNumeroMaxAmort + ", " + strIgClausulaVencimiento + ", "+
							" " + strCgRevisionTasaMora + "'  "+
			//				" " + strIgTipoCalculoInteres + ", " + intIcPeriodoCreditoMora + ", '" + strCgRevisionTasaMora + "'  "+
		    //     			"  , '"+ esBaseCalculo +"'"+	//Agregado 19/03/2003	--CARP
							" ,'"+cs_troya+"',"+ig_numero_max_disp+","+ig_numero_min_amort+",'"+strCsDetalle+"',"+ lsdias_lib_linea+","+ lsnumeroamortizacion_exp+","+  // strCsDetalle - Agregado 21/02/2006 -- NCM
							" " +lsperiodovigencia_exp+","+ lsTipoPagoCapital+","+ lsTipoPagoCapital_exp +"," + txtNumMaxDisp_exp + ", "+ lsdias_lib_linea_e +
							","+lsnumeroamortizacion_emp+","+lsnumeroamortizacion_e_emp+","+lsTipoPagoCapital_emp+","+lsTipoPagoCapital_e_emp+","+txtNumMaxDisp_emp+","+txtNumMaxDisp_e_emp+") ";

			System.out.print("ParametrosGralesBean :: ovinsertarProducto :: Query "+qrySentencia);

			con.ejecutaSQL(qrySentencia);

		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	} /*Fin del M�todo ovinsertarProducto*/


	//-------------------------------------------------------------------------------------------
	//	void ovinsertarMoneda()
	//
	//-------------------------------------------------------------------------------------------

	public void ovinsertarMoneda( 	int 	intIcProductoNafin, 	String strMoneda,
										String 	strFnMontoMaximo,		String strFnMontoMinimo,
										/*int intIcTasa,*/				String strCgRelMat,
										String strFnPuntos)
   throws NafinException
   {
		AccesoDB		con 			= null;
		String			qrySentencia	= "";
		boolean			resultado		= true;

		try {
			con = new AccesoDB();
			con.conexionDB();

						qrySentencia =	"Insert Into comrel_param_x_moneda " +
										" (ic_producto_nafin, ic_moneda, fn_monto_maximo, fn_monto_minimo, ";

						//if (intIcTasa==0){

							qrySentencia += " cs_tipo_calculo_mora, ic_tasa) ";
						//}else{
						//	qrySentencia += " cs_tipo_calculo_mora, ic_tasa, cg_rel_mat, fn_puntos) ";
						//}
						qrySentencia += 	" Values (" + intIcProductoNafin + "," +
										" " + strMoneda + "," +
										" " + strFnMontoMaximo + "," +
										" " + strFnMontoMinimo + ",";

					//	if (intIcTasa==0){
							qrySentencia += " 'S', null )";
					//	}else{
					//		qrySentencia += " 'N', " + intIcTasa + ", '" + strCgRelMat + "', " + strFnPuntos + ")";
					//	}

			System.out.print("ParametrosGralesBean :: ovinsertarMoneda :: Query "+qrySentencia);

			con.ejecutaSQL(qrySentencia);

		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	} /*Fin del M�todo */


	//-------------------------------------------------------------------------------------------
	//	void ovinsertarMoneda()
	//
	//-------------------------------------------------------------------------------------------

	public void ovactualizarMoneda( 		int 	intIcProductoNafin, 	String strMoneda,
										String 	strFnMontoMaximo,		String strFnMontoMinimo,
										/*int intIcTasa,*/					String strCgRelMat,
										String strFnPuntos)
   throws NafinException {

		AccesoDB		con 			= null;
		String			qrySentencia	= "";
		boolean			resultado		= true;

		try {
			con = new AccesoDB();
			con.conexionDB();

						qrySentencia =	" Update comrel_param_x_moneda "+
									" Set fn_monto_maximo = " + strFnMontoMaximo + ", "+
									" fn_monto_minimo =  " + strFnMontoMinimo + ", ";

						//if (intIcTasa==0){
							qrySentencia += " cs_tipo_calculo_mora = 'S', ic_tasa = null, cg_rel_mat = null, fn_puntos = null ";
						//}else{
						//	qrySentencia += " cs_tipo_calculo_mora = 'N',  ic_tasa = " + intIcTasa + " , ";
						//	qrySentencia += " cg_rel_mat = '" + strCgRelMat + "' , fn_puntos = " + strFnPuntos;
						//}
						qrySentencia += " Where ic_producto_nafin = " + intIcProductoNafin;
						qrySentencia += "	And ic_moneda = " + strMoneda +" ";

			System.out.print("ParametrosGralesBean :: ovactualizarMoneda :: Query "+qrySentencia);

			con.ejecutaSQL(qrySentencia);

		}catch(Exception e){
			resultado = false;
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(resultado);
				con.cierraConexionDB();
			}
		}
	} /*Fin del M�todo ovactualizarMoneda*/

/*>>>>>>>>>>>>>>>>Israel Herrera*/
 public String getAutoSuscep(String ic_epo)
  throws NafinException {
    String		autoSuscep		= "";
    String		qrySentencia	= "";
    AccesoDB	con        		= null;
    ResultSet	rs 				= null;
   	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
			"SELECT cs_automatico_susceptible "+
			"  FROM comrel_producto_epo "+
			" WHERE ic_producto_nafin = 5 "+
			"   AND ic_epo = "+ic_epo+" ";
//System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        if(rs.next())
			autoSuscep = (rs.getString(1)==null)?"":rs.getString(1);
	      con.cierraStatement();
	} catch(Exception e) {
      	System.out.println("Exception: Bean Parametros Generales"+e);
		throw new NafinException("SIST0001");
    } finally {
    	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
 	}
	return autoSuscep;
 }//getAutoSuscep

 public String getAutoSuscepPedidos(String ic_epo)
  throws NafinException {
    String		autoSuscep		= "";
    String		qrySentencia	= "";
    AccesoDB	con        		= null;
    ResultSet	rs 				= null;
   	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
			"SELECT cs_automatico_susceptible "+
			"  FROM comrel_producto_epo "+
			" WHERE ic_producto_nafin = 2 "+
			"   AND ic_epo = "+ic_epo+" ";
//System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
        if(rs.next())
			autoSuscep = (rs.getString(1)==null)?"":rs.getString(1);
	      con.cierraStatement();
	} catch(Exception e) {
      	System.out.println("Exception: Bean Parametros Generales"+e);
		throw new NafinException("SIST0001");
    } finally {
    	if(con.hayConexionAbierta())
        	con.cierraConexionDB();
 	}
	return autoSuscep;
 }//getAutoSuscepPedidos




 public void setAutoSuscep(String ic_producto_nafin, String ic_epo, String cs_automatico_susceptible)
  throws NafinException {
    String		qrySentencia	= "";
    AccesoDB	con        		= null;
    ResultSet	rs 				= null;
    int 		num				= 0;
	boolean flag = true;
   	try {
      	con = new AccesoDB();
        con.conexionDB();
        qrySentencia  =
			"SELECT count(1) "+
			"  FROM comrel_producto_epo "+
			" WHERE ic_producto_nafin = 5 "+
			"   AND ic_epo = "+ic_epo;
		System.out.println(qrySentencia);
        rs = con.queryDB(qrySentencia);
	    if(rs.next())
			num = rs.getInt(1);
		con.cierraStatement();
		if (num>0) {
	        qrySentencia  =
				"UPDATE comrel_producto_epo "+
				"   SET cs_automatico_susceptible = '"+cs_automatico_susceptible+"' "+
				" WHERE ic_producto_nafin = "+ic_producto_nafin+" "+
				"   AND ic_epo = "+ic_epo;
		}
		else {
	        qrySentencia  =
				"INSERT INTO comrel_producto_epo (ic_producto_nafin, ic_epo, cs_automatico_susceptible) "+
				"     VALUES("+ic_producto_nafin+", "+ic_epo+", '"+cs_automatico_susceptible+"')";
		}
		System.out.println(qrySentencia);
		con.ejecutaSQL(qrySentencia);
	} catch(Exception e){
		flag = false;
      	System.out.println("Exception: Bean Parametros Generales"+e);
		throw new NafinException("SIST0001");
	}
	finally	{
		con.terminaTransaccion(flag);
		if(con.hayConexionAbierta()) con.cierraConexionDB();
	}
 }//getAutoSuscep

	public Vector getParamGrales(String ic_producto_nafin,String ic_moneda)
  	 throws NafinException {
	    AccesoDB    con				= null;
    	ResultSet   rs				= null;
	    String      qrySentencia	= "",
	    			campos			= "",
	    			tablas			= "",
	    			condicion		= "";
	    Vector		vecColumnas		= new Vector();
	    Vector		vecFilas		= new Vector();
	    try{
			con = new AccesoDB();
			con.conexionDB();
			if (!"".equals(ic_moneda)){
				campos =
					"       ,pxm.cs_tipo_calculo_mora,   pxm.ic_tasa, cta.cd_nombre,"+
					"       pxm.cg_rel_mat, pxm.fn_puntos ";
				tablas =
					"       ,comrel_param_x_moneda pxm, comcat_tasa cta ";
				condicion =
					"   AND cpn.ic_producto_nafin = pxm.ic_producto_nafin (+) "+
					"   AND pxm.ic_moneda (+) = "+ic_moneda+" "+
					"	AND pxm.ic_tasa = cta.ic_tasa (+) ";
			}
			qrySentencia =
				"SELECT cpn.cg_base_calculo,         cpn.cg_tipo_cobranza, "+
				"       cpn.ic_periodo_max_disp,     cpn.ic_periodo_max_vig, "+
				"       cpn.ic_numero_max_amort,     cpn.ig_clausula_vencimiento, "+
				"       cpn.ig_tipo_calculo_interes, cpn.ic_periodo_credito_mora, "+
				"       cpn.ig_tipo_mora,            cpn.cg_revision_tasa_mora, "+
				"		cpn.ig_numero_max_disp"+campos+" "+
				"  FROM comcat_producto_nafin cpn "+tablas+" "+
				" WHERE cpn.ic_producto_nafin = 5 "+condicion+" ";
System.out.println(qrySentencia);
      		rs = con.queryDB(qrySentencia);
			while(rs.next()){
        		vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
				vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
				vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
				vecColumnas.addElement((rs.getString(4)==null)?"":rs.getString(4));
				vecColumnas.addElement((rs.getString(5)==null)?"":rs.getString(5));
				vecColumnas.addElement((rs.getString(6)==null)?"":rs.getString(6));
				vecColumnas.addElement((rs.getString(7)==null)?"":rs.getString(7));
				vecColumnas.addElement((rs.getString(8)==null)?"":rs.getString(8));
				vecColumnas.addElement((rs.getString(9)==null)?"":rs.getString(9));
				vecColumnas.addElement((rs.getString(10)==null)?"":rs.getString(10));
				vecColumnas.addElement((rs.getString(11)==null)?"":rs.getString(11));

				if (!"".equals(ic_moneda)){
					vecColumnas.addElement((rs.getString(12)==null)?"":rs.getString(12));
					vecColumnas.addElement((rs.getString(13)==null)?"":rs.getString(13));
					vecColumnas.addElement((rs.getString(14)==null)?"":rs.getString(14));
					vecColumnas.addElement((rs.getString(15)==null)?"":rs.getString(15));
					vecColumnas.addElement((rs.getString(16)==null)?"":rs.getString(16));
				}

				vecFilas.addElement(vecColumnas);
			}//while(rs.next()){
     		con.cierraStatement();
        }catch(Exception e){
      		System.out.println("Exception: Bean Parametros Generales"+e);
			throw new NafinException("SIST0001");
    	}finally{
      		if(con.hayConexionAbierta()){
				con.cierraConexionDB();
      		}
    	}
		return vecFilas;
	}//getParamGrales

	public Vector getParamGralesCredExp()
  	 throws NafinException {
	    AccesoDB	    	con		= null;
    	ResultSet   		rs		= null;
    	PreparedStatement 	ps		= null;
	    String      qrySentencia	= "";
		 Vector		vecColumnas		= new Vector();
	    Vector		vecFilas		= new Vector();
	    try{
			con = new AccesoDB();
			con.conexionDB();
			qrySentencia =
				"SELECT cpn.ig_dias_lib_exp, 		 cpn.ig_periodo_max_vig_exp, "+
				"		cpn.ig_numero_max_amort_exp, cpn.ig_numero_max_disp_exp, "+
				"		cpn.ic_tabla_amort_exp"+
				"  FROM comcat_producto_nafin cpn "+
				" WHERE cpn.ic_producto_nafin = 5 ";
			System.out.println(qrySentencia);
			ps = con.queryPrecompilado(qrySentencia);
			rs = ps.executeQuery();
			ps.clearParameters();
			while(rs.next()){
        		vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
				vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
				vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
				vecColumnas.addElement((rs.getString(4)==null)?"":rs.getString(4));
				vecColumnas.addElement((rs.getString(5)==null)?"":rs.getString(5));
				vecFilas.addElement(vecColumnas);
			}//while(rs.next()){
     		rs.close();ps.close();
        }catch(Exception e){
      		System.out.println("Exception: Bean Parametros Generales"+e);
			throw new NafinException("SIST0001");
    	}finally{
      		if(con.hayConexionAbierta()){
				con.cierraConexionDB();
      		}
    	}
		return vecFilas;
	}//getParamGrales

	public Vector getParamxIf(String ic_producto_nafin,String ic_if,String ic_moneda)
  	 throws NafinException {
	    AccesoDB    con				= null;
    	ResultSet   rs				= null;
	    String      qrySentencia	= "",
	    			campos			= "",
	    			tablas			= "",
	    			condicion		= "";
	    Vector		vecColumnas		= new Vector();
	    Vector		vecFilas		= new Vector();
	    try{
			con = new AccesoDB();
			con.conexionDB();
			if(!"".equals(ic_moneda)){
				campos =
					"		,pim.cs_tipo_calculo_mora, "+
					"       pim.ic_tasa, cta.cd_nombre, pim.cg_rel_mat, pim.fn_puntos ";
				tablas =
						" , comrel_param_if_x_moneda pim, comcat_tasa cta ";
				condicion =
					"   AND cpi.ic_producto_nafin = pim.ic_producto_nafin (+) "+
					"   AND cpi.ic_if = pim.ic_if (+) "+
					"   AND pim.ic_moneda (+) = "+ic_moneda+" "+
					"	AND pim.ic_tasa = cta.ic_tasa ";

			}
			qrySentencia =
				"SELECT cpi.cg_base_calculo, cpi.cg_tipo_cobranza, "+
				"       cpi.ic_periodo_max_disp, pl1.in_plazo_meses, cpi.ic_periodo_max_vig, pl2.in_plazo_meses, "+
				"       cpi.ic_numero_max_amort, pl3.in_plazo_meses, cpi.ig_clausula_vencimiento, "+
				"       cpi.ig_tipo_calculo_interes, cpi.cs_compartir_linea, "+
				"       cpi.ic_periodo_credito_mora, pl4.in_plazo_meses, cpi.ig_tipo_mora, "+
				"       cpi.cg_revision_tasa_mora,cpi.ig_numero_max_disp "+campos+
				"  FROM comrel_producto_if cpi , comcat_plazo pl1"+
				"		,comcat_plazo pl2, comcat_plazo pl3, comcat_plazo pl4"+tablas+
				" WHERE cpi.ic_producto_nafin = "+ic_producto_nafin+" "+
				"   AND cpi.ic_if = "+ic_if+" "+
				"   AND cpi.ic_periodo_max_disp = pl1.ic_plazo (+) "+
				"   AND cpi.ic_periodo_max_vig = pl2.ic_plazo (+) "+
				"   AND cpi.ic_numero_max_amort = pl3.ic_plazo (+) "+
				"   AND cpi.ic_periodo_credito_mora = pl4.ic_plazo (+) "+condicion;
System.out.println(qrySentencia);
      		rs = con.queryDB(qrySentencia);
			while(rs.next()){
        		vecColumnas = new Vector();
				vecColumnas.addElement((rs.getString(1)==null)?"":rs.getString(1));
				vecColumnas.addElement((rs.getString(2)==null)?"":rs.getString(2));
				vecColumnas.addElement((rs.getString(3)==null)?"":rs.getString(3));
				vecColumnas.addElement((rs.getString(4)==null)?"":rs.getString(4));
				vecColumnas.addElement((rs.getString(5)==null)?"":rs.getString(5));
				vecColumnas.addElement((rs.getString(6)==null)?"":rs.getString(6));
				vecColumnas.addElement((rs.getString(7)==null)?"":rs.getString(7));
				vecColumnas.addElement((rs.getString(8)==null)?"":rs.getString(8));
				vecColumnas.addElement((rs.getString(9)==null)?"":rs.getString(9));
				vecColumnas.addElement((rs.getString(10)==null)?"":rs.getString(10));
				vecColumnas.addElement((rs.getString(11)==null)?"":rs.getString(11));
				vecColumnas.addElement((rs.getString(12)==null)?"":rs.getString(12));
				vecColumnas.addElement((rs.getString(13)==null)?"":rs.getString(13));
				vecColumnas.addElement((rs.getString(14)==null)?"":rs.getString(14));
				vecColumnas.addElement((rs.getString(15)==null)?"":rs.getString(15));
				vecColumnas.addElement((rs.getString(16)==null)?"":rs.getString(16));
				if (!"".equals(ic_moneda)){
					vecColumnas.addElement((rs.getString(17)==null)?"":rs.getString(17));
					vecColumnas.addElement((rs.getString(18)==null)?"":rs.getString(18));
					vecColumnas.addElement((rs.getString(19)==null)?"":rs.getString(19));
					vecColumnas.addElement((rs.getString(20)==null)?"":rs.getString(20));
					vecColumnas.addElement((rs.getString(21)==null)?"":rs.getString(21));
				}
				vecColumnas.addElement((rs.getString("ig_numero_max_disp")==null)?"":rs.getString("ig_numero_max_disp"));
				vecFilas.addElement(vecColumnas);
			}
     		con.cierraStatement();
        }catch(Exception e){
      		System.out.println("Exception: Bean Parametros Generales"+e);
			throw new NafinException("SIST0001");
    	}finally{
      		if(con.hayConexionAbierta()){
				con.cierraConexionDB();
      		}
    	}
		return vecFilas;
	}//getParamxIf

	public void setParamxIf(String ic_producto_nafin,String ic_if,String ic_moneda,
							  String base_calculo,String tipo_cobranza,String periodo_max_disp,
							  String periodo_max_vig,String numero_max_amort,String clausula_vencimiento,
							  String tipo_calculo_interes,String compartir_linea,String periodo_credito_mora,
							  String tipo_mora,String ic_tasa,String rel_mat,String puntos,
							  String revision_tasa_mora, String ig_numero_max_disp)
	 throws NafinException{
		AccesoDB    con           	= null;
		String      qrySentencia 	= "";
		String      campos		 	= "";
    	ResultSet   rs				= null;
		boolean     ok 				= true;
		int 		num				= 0;
    	try {
      		con = new AccesoDB();
        	con.conexionDB();
        	if("".equals(ig_numero_max_disp.trim())){
        		ig_numero_max_disp = "NULL";
        	}
        	qrySentencia =
				"SELECT COUNT (*) "+
				"  FROM comrel_producto_if "+
				" WHERE ic_if = "+ic_if+" "+
				"   AND ic_producto_nafin = "+ic_producto_nafin+" ";
	        rs = con.queryDB(qrySentencia);
		    if(rs.next())
				num = rs.getInt(1);
			con.cierraStatement();
			base_calculo			= "".equals(base_calculo)?"NULL":"'"+base_calculo+"'";
			tipo_cobranza			= "".equals(tipo_cobranza)?"NULL":"'"+tipo_cobranza+"'";
			periodo_max_disp		= "".equals(periodo_max_disp)?"NULL":periodo_max_disp;
			periodo_max_vig			= "".equals(periodo_max_vig)?"NULL":periodo_max_vig;
			numero_max_amort		= "".equals(numero_max_amort)?"NULL":numero_max_amort;
			clausula_vencimiento	= "".equals(clausula_vencimiento)?"NULL":clausula_vencimiento;
			tipo_calculo_interes	= "".equals(tipo_calculo_interes)?"NULL":tipo_calculo_interes;
			compartir_linea			= "".equals(compartir_linea)?"NULL":"'"+compartir_linea+"'";
			periodo_credito_mora	= "".equals(periodo_credito_mora)?"NULL":periodo_credito_mora;
			tipo_mora				= "".equals(tipo_mora)?"NULL":tipo_mora;
			revision_tasa_mora		= "".equals(revision_tasa_mora)?"NULL":"'"+revision_tasa_mora+"'";
			ig_numero_max_disp		= "".equals(ig_numero_max_disp)?"NULL":ig_numero_max_disp;
			if (num>0) {
	        qrySentencia  =
				"UPDATE comrel_producto_if "+
				"   SET cg_base_calculo = "+base_calculo+" "+
				"       , cg_tipo_cobranza = "+tipo_cobranza+" "+
				"       , ic_periodo_max_disp = "+periodo_max_disp+" "+
				"       , ic_periodo_max_vig = "+periodo_max_vig+" "+
				"       , ic_numero_max_amort = "+numero_max_amort+" "+
				"       , ig_clausula_vencimiento = "+clausula_vencimiento+" "+
				"       , ig_tipo_calculo_interes = "+tipo_calculo_interes+" "+
				"       , cs_compartir_linea = "+compartir_linea+" "+
				"       , ic_periodo_credito_mora = "+periodo_credito_mora+" "+
				"       , ig_tipo_mora = "+tipo_mora+" "+
				"       , cg_revision_tasa_mora = "+revision_tasa_mora+" "+
				"       , ig_numero_max_disp = "+ig_numero_max_disp+" "+
				" WHERE ic_producto_nafin = "+ic_producto_nafin+" "+
				"   AND ic_if = "+ic_if;
			}
			else {
	    	    qrySentencia  =
					"INSERT INTO comrel_producto_if( "+
					"        cg_base_calculo,cg_tipo_cobranza, "+
					"        ic_periodo_max_disp,ic_periodo_max_vig, "+
					"        ic_numero_max_amort,ig_clausula_vencimiento, "+
					"        ig_tipo_calculo_interes,cs_compartir_linea, "+
					"        ic_periodo_credito_mora,ig_tipo_mora, "+
					"        cg_revision_tasa_mora,ic_producto_nafin,ic_if,ig_numero_max_disp) "+
					"     VALUES( "+
					"        "+base_calculo+","+tipo_cobranza+", "+
					"        "+periodo_max_disp+","+periodo_max_vig+", "+
					"        "+numero_max_amort+","+clausula_vencimiento+", "+
					"        "+tipo_calculo_interes+","+compartir_linea+", "+
					"        "+periodo_credito_mora+","+tipo_mora+", "+
					"        "+revision_tasa_mora+","+ic_producto_nafin+","+ic_if+","+ig_numero_max_disp+") ";
			}
			System.out.println(qrySentencia);
			con.ejecutaSQL(qrySentencia);
			if(!"".equals(ic_moneda)){
	        	qrySentencia =
					"SELECT COUNT (*) "+
					"  FROM comrel_param_if_x_moneda "+
					" WHERE ic_if = "+ic_if+" "+
					"   AND ic_producto_nafin = "+ic_producto_nafin+" "+
					"   AND ic_moneda = "+ic_moneda+" ";
		        rs = con.queryDB(qrySentencia);
			    if(rs.next())
					num = rs.getInt(1);
				con.cierraStatement();
				rel_mat	= "".equals(rel_mat)?"NULL":"'"+rel_mat+"'";
				puntos	= "".equals(puntos)?"NULL":puntos;
				ic_tasa = "".equals(ic_tasa)?"NULL":ic_tasa;
				if (num>0) {
					if(ic_tasa.equals("0")){
						campos =
							"   ,cs_tipo_calculo_mora = 'S', "+
							"   ic_tasa = null ";
					}
					else {
						campos =
							"   ,cs_tipo_calculo_mora = 'N', "+
							"   ic_tasa = "+ic_tasa+" ";
					}

			        qrySentencia  =
						"UPDATE comrel_param_if_x_moneda "+
						" SET   cg_rel_mat = "+rel_mat+", "+
						"       fn_puntos = "+puntos+" "+campos+
						" WHERE ic_if = "+ic_if+" "+
						"   AND ic_producto_nafin = "+ic_producto_nafin+" "+
						"   AND ic_moneda = "+ic_moneda+" ";
				}

				else {
		    	    qrySentencia  =
						"INSERT INTO comrel_param_if_x_moneda "+
						"            (ic_producto_nafin,ic_if,ic_moneda,cs_tipo_calculo_mora,ic_tasa";
					if(ic_tasa.equals("0"))
		    	    	qrySentencia  += ")";
					else
						qrySentencia  += ",cg_rel_mat, fn_puntos) ";
					qrySentencia +=	"     VALUES("+ic_producto_nafin+","+ic_if+","+ic_moneda;
					if(ic_tasa.equals("0"))
		    	    	qrySentencia  += ",'S',null)";
					else
						qrySentencia  += ",'N',"+ic_tasa+","+rel_mat+","+puntos+") ";
				}
				System.out.println(qrySentencia);
				con.ejecutaSQL(qrySentencia);
			}
      	} catch(Exception e){
			ok = false;
      		System.out.println("Exception: Bean Parametros Generales"+e);
			throw new NafinException("SIST0001");
		} finally {
      		if(con.hayConexionAbierta())
          		con.terminaTransaccion(ok);
       		con.cierraConexionDB();
      	}
	}//setParamxIf
/*<<<<<<<<<<<<<<<<<<Israel Herrera*/


/**
 * Actualiza el par�metro de cobranza autom�tica, para las Pymes
 * indicadas. Si el arreglo que contiene las claves de las pymes
 * es nulo, entonces significa que no hay nada seleccionado y por lo
 * tanto elimina toda la paremetrizacion.
 * @param strIF Clave del IF
 * @param strEPO Clave de la EPO
 * @param arrClavesPyme Arreglo de las claves de las pymes que manejan
 * 		cobranza autom�tica de credicadenas (Producto 5).
 */
public void setCobranzaAutomatica(String strIF, String strEPO,
		String[] arrClavesPyme) throws NafinException {
	AccesoDB con = new AccesoDB();
	boolean exito = true;

	int ic_if = 0;
	int ic_epo = 0;

	//**********************************Validaci�n de parametros:*****************************
	try {
		if (arrClavesPyme == null) {	//Si no hay claves de pyme, el arreglo se inicializa como vacio.
			arrClavesPyme = new String[0];
		}
		if (strIF == null || strEPO == null) {
			throw new Exception("Los parametros de EPO e IF no pueden ser nulos");
		}
		ic_if = Integer.parseInt(strIF);
		ic_epo = Integer.parseInt(strEPO);
	} catch(Exception e) {
		System.out.println("Error en los parametros recibidos. " + e.getMessage() +
				" strIF=" + strIF +
				" strEPO=" + strEPO +
				" arrClavesPyme=" + arrClavesPyme);
		throw new NafinException("SIST0001");
	}
	//***************************************************************************************

	try {
		con.conexionDB();
		// Se eliminan todos los registros
		String strSQL = " DELETE FROM comrel_cobranza_x_producto " +
				" WHERE ic_if = " + strIF +
				" AND ic_epo = " + strEPO +
				" AND ic_producto_nafin = 5 "; //5 = CREDICADENAS
		con.ejecutaSQL(strSQL);
		//Se insertan los que manejaran cobranza automatica de credicadenas:
		for (int i = 0; i < arrClavesPyme.length; i++) {
			strSQL = " INSERT INTO comrel_cobranza_x_producto(ic_if, ic_epo, ic_pyme, " +
					" ic_producto_nafin, cg_cobranza_automatica)" +
					" VALUES(" + strIF + "," + strEPO + "," + arrClavesPyme[i] +",5,'S')";
			con.ejecutaSQL(strSQL);
		}

	} catch (SQLException e) {
		exito = false;
		System.out.println(e.getMessage());
		throw new NafinException("SIST0001");
	} catch (NamingException e) {
		exito = false;
		System.out.println(e.getMessage());
		throw new NafinException("SIST0001");
	} finally {
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(exito);
			con.cierraConexionDB();
		}
	}

}


	public Vector getComboPlazos()
	throws NafinException{
		AccesoDB    con           	= null;
		String      qrySentencia 	= "";
    	ResultSet   rs				= null;
		Vector		vecFilas		= null;
		Vector		vecColumnas		= null;
    	try {
      		con = new AccesoDB();
        	con.conexionDB();
			qrySentencia =
				" SELECT   MIN (ic_plazo) IC_PLAZO , IN_PLAZO_MESES"   +
				"     FROM comcat_plazo"   +
				"    WHERE ic_producto_nafin IS NULL"   +
				"       OR ic_producto_nafin = 5"   +
				" GROUP BY in_plazo_meses"   +
				" ORDER BY 2"  ;
			rs = con.queryDB(qrySentencia);
			vecFilas = new Vector();
			while(rs.next()){
				vecColumnas = new Vector();
/*0*/			vecColumnas.add((rs.getString("IC_PLAZO")==null)?"":rs.getString("IC_PLAZO"));
/*1*/			vecColumnas.add((rs.getString("IN_PLAZO_MESES")==null)?"":rs.getString("IN_PLAZO_MESES"));
				vecFilas.add(vecColumnas);
			}
			con.cierraStatement();
      	} catch(Exception e){
      		System.out.println("Exception: getComboPlazo"+e);
			throw new NafinException("SIST0001");
		} finally {
      		if(con.hayConexionAbierta())
	       		con.cierraConexionDB();
      	}
		return vecFilas;
	}

	/*****************************************************************************************
	 *@author Salvador Saldivar Barajas
	 *@param ic_epo clave interna de la epo
	 *@param no_epo clave interna de la epo
	 *@param ne_epo numero nafin electronico de la epo
	 *@param nombreEpo nombre de la epo
	 *@param ic_secretaria clave interna del catalogo de secretarias
	 *@param ic_organismo clave interna del catalogo de tipos de organismos
	 *@return regresa una lista con los datos obtenidos de la consulta
	 ****************************************************************************************/
	//getEposProyectoPEF regresa los datos de la epo, de acuerdo al criterio de busqueda definido
	public List getEposProyectoPEF(String ic_epo, String no_epo, String ne_epo, String nombreEpo, String ic_secretaria, String ic_organismo)
	throws NafinException{
		AccesoDB con = null;
		String qry = "";
		String condiciones = "";
    	ResultSet rs = null;
		List lFilas = new ArrayList();
		List lColumnas = null;
    	try {
      		con = new AccesoDB();
        	con.conexionDB();

        	if(!ic_epo.equals(""))
        		condiciones += " AND e.ic_epo = " + ic_epo;

        	if(!no_epo.equals(""))
        		condiciones += "  AND e.ic_epo = " + no_epo;

        	if(!ne_epo.equals(""))
        		condiciones += " AND e.ic_epo = (SELECT ic_epo_pyme_if FROM comrel_nafin WHERE cg_tipo = 'E' AND ic_nafin_electronico = "+ne_epo+" )";

        	if(!nombreEpo.equals(""))
				condiciones += " AND e.cg_razon_social LIKE NVL (REPLACE (UPPER ('"+nombreEpo+"'), '*', '%'), '%')";

        	if(!ic_secretaria.equals(""))
        		condiciones += " AND e.ic_secretaria = " + ic_secretaria;

        	if(!ic_organismo.equals(""))
        		condiciones += " AND e.ic_tipo_organismo = " +ic_organismo;

			qry = " SELECT e.ic_epo as icepo, e.cg_razon_social as rsocial, s.ic_secretaria as secretaria, o.ic_tipo_organismo as organismo, e.ig_obra_publica_porc as obra_publica " +
				" FROM comcat_epo e, comcat_secretaria s, comcat_tipo_organismo o " +
				" WHERE s.ic_secretaria(+) = e.ic_secretaria " +
				" AND o.ic_tipo_organismo(+) = e.ic_tipo_organismo " + condiciones +
				" ORDER BY e.ic_epo";

			rs = con.queryDB(qry);

			while(rs.next()){
				lColumnas = new ArrayList();
				lColumnas.add((rs.getString("icepo")==null)?"":rs.getString("icepo"));
				lColumnas.add((rs.getString("rsocial")==null)?"":rs.getString("rsocial"));
				lColumnas.add((rs.getString("secretaria")==null)?"":rs.getString("secretaria"));
				lColumnas.add((rs.getString("organismo")==null)?"":rs.getString("organismo"));
				lColumnas.add((rs.getString("obra_publica")==null)?"":rs.getString("obra_publica"));
				lFilas.add(lColumnas);
			}
			con.cierraStatement();
      	} catch(Exception e){
      		System.out.println("Exception: getEposProyectoPEF::: "+e);
			throw new NafinException("SIST0001");
		} finally {
      		if(con.hayConexionAbierta())
	       		con.cierraConexionDB();
      	}
		return lFilas;
	}

	/*****************************************************************************************
	 *@author Salvador Saldivar Barajas
	 *@param ldatos, lista con los datos ic_epo, ic_secretaria, ic_tipo_organismo
	 *@return void
	 ****************************************************************************************/
	//ovactualizarEpos actualiza los datos secretaria y organismo de cada epo
	public void ovactualizarEpos(List ldatos) throws NafinException{
		AccesoDB con = null;
		String qry = "";
		String qryHistorial = "";
		String qryHistorialupdate = "";
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		List lregistros = null;
		boolean ok = true;
    	try {
      		con = new AccesoDB();
        	con.conexionDB();

			qry = "UPDATE comcat_epo set ic_secretaria = ?, ic_tipo_organismo = ?, ig_obra_publica_porc = ?, ig_anio_obra = ?  where ic_epo = ?";

			qryHistorialupdate = "update comhis_epo_pef "+
										" set ig_obra_publica_porc = ? "+
										" where ic_epo = ?  "+
										" and ig_anio = ? ";

			qryHistorial = " insert into comhis_epo_pef (ic_epo, ig_anio, ig_obra_publica_porc) "+
										 " values(?, to_number(to_char(SYSDATE,'YYYY')),?) ";

			ps = con.queryPrecompilado(qry);
			ps2 = con.queryPrecompilado(qryHistorial);

        	for(int a=0;a<ldatos.size();a++){
        		lregistros = new ArrayList();
        		lregistros = (List)ldatos.get(a);
        		if(lregistros.get(1).toString().equals(""))
        			ps.setNull(1,java.sql.Types.NUMERIC);
        		else
        			ps.setString(1,lregistros.get(1).toString());
        		if(lregistros.get(2).toString().equals(""))
        			ps.setNull(2,java.sql.Types.NUMERIC);
        		else
        			ps.setString(2,lregistros.get(2).toString());
						if(lregistros.get(3).toString().equals("")){
        			ps.setNull(3,java.sql.Types.NUMERIC);
							ps.setNull(4,java.sql.Types.NUMERIC);
        		}else{
        			ps.setString(3,lregistros.get(3).toString());
							ps.setString(4,lregistros.get(4).toString());

							ps2 = con.queryPrecompilado(qryHistorialupdate);
								ps2.setString(1,lregistros.get(3).toString());//se agraga valor para insertar Historial
								ps2.setString(2,lregistros.get(0).toString());//se agraga valor para insertar Historial
								ps2.setString(3,lregistros.get(4).toString());//se agraga valor para insertar Historial
							if(ps2.executeUpdate()==0)
							{
								System.out.println("no realiza update:::::");
								ps2 = con.queryPrecompilado(qryHistorial);
									ps2.setString(1,lregistros.get(0).toString());//se agraga valor para insertar Historial
									ps2.setString(2,lregistros.get(3).toString());//se agraga valor para insertar Historial
								ps2.execute();
							}
						}

						ps.setString(5,lregistros.get(0).toString());
				ps.execute();
        	}
        	ps.close();
					ps2.close();
					/*Isercion para historial de porcentajes de obra publica respecto a cada epo*/

      	} catch(Exception e){
      		ok = false;
      		System.out.println("Exception: ovactualizarEpos::: "+e);
			throw new NafinException("SIST0001");
		} finally {
      		if(con.hayConexionAbierta()){
      			con.terminaTransaccion(ok);
      			con.cierraConexionDB();
      		}
      	}
	}

	/**
	 * Consulta que obtiene historial del % de obra publica por epo
	 * @throws com.netro.exception.NafinException
	 * @return  List - registros encontrados
	 * @param anio- indica el anio en curso
	 * @param ic_epo- id de la epo a consultar
	 */
	public List consHistEpoPefObraPub(String ic_epo, String anio) throws NafinException
	{
		AccesoDB con = new AccesoDB();
		ResultSet rs;
		List colEpoPef = new ArrayList();
		String qryEpoPef = "";
		try
		{
			con.conexionDB();

			qryEpoPef = " select ig_anio, ig_obra_publica_porc "+
									" from comhis_epo_pef "+
									" where ic_epo =  "+ic_epo+
									" and ig_anio < "+anio;

			rs = con.queryDB(qryEpoPef);

			while(rs.next())
			{
				List regEpoPef = new ArrayList();
				regEpoPef.add(rs.getString(1));
				regEpoPef.add(rs.getString(2));

				colEpoPef.add(regEpoPef);
			}
			rs.close();
		}catch(Exception e)
		{
			e.printStackTrace();
			con.cierraConexionDB();
			throw new NafinException("SIST0001");
		}finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return colEpoPef;
	}

	/**
	 * Consulta que obtiene los anios paramatrizados en obra publica
	 * @throws com.netro.exception.NafinException
	 * @return  List - registros encontrados
	 */
	public List consAnioParamEpoPef() throws NafinException
	{
		AccesoDB con = new AccesoDB();
		ResultSet rs;
		List anioParamEpoPef = new ArrayList();
		String qryEpoPef = "";
		try
		{
			con.conexionDB();

			qryEpoPef = " select Distinct ig_anio from comhis_epo_pef "+
									" order by ig_anio desc ";

			rs = con.queryDB(qryEpoPef);

			while(rs.next())
			{
				anioParamEpoPef.add(rs.getString(1));
			}
			rs.close();
		}catch(Exception e)
		{
			e.printStackTrace();
			con.cierraConexionDB();
			throw new NafinException("SIST0001");
		}finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return anioParamEpoPef;
	}

	public List consEpoPefReporte(String anio, String mes) throws NafinException
	{
		AccesoDB con = new AccesoDB();
		ResultSet rs;
		List paramRep = new ArrayList();
		List colParamRep = new ArrayList();

		String qryEpoPef = "";
		String fecha = "01/"+mes+"/"+anio+"";
		try
		{
			con.conexionDB();

			qryEpoPef =

			" SELECT  ce.ic_epo,ce.CG_RAZON_SOCIAL, d.ic_moneda, SUM(d.fn_monto), round(((SUM( "+
					" (CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_descuento, 'YYYY')) > " +anio+  " THEN  to_number(to_char(last_day(to_date('"+fecha+"','DD/MM/YYYY')),'DD'))   "+
						" ELSE  "+
							" CASE WHEN TO_NUMBER(TO_CHAR(sa.df_v_descuento, 'MM')) > " +mes+  " THEN to_number(to_char(last_day(to_date('"+fecha+"','DD/MM/YYYY')),'DD'))   "+
							" ELSE "+
									" TO_NUMBER(TO_CHAR(sa.df_v_descuento, 'DD')) - 1  "+
						" END "+
					 " END  "+
					" - "+
					 " CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'YYYY')) <  "+anio+"   THEN 1 "+
						" ELSE "+
							" CASE WHEN TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'MM')) <  "+mes+"   THEN 1  "+
							" ELSE "+
									" TO_NUMBER(TO_CHAR(sa.df_fecha_solicitud, 'DD')) "+
						" END "+
					" END + 1 ) "+
			    " * d.fn_monto ) ) / to_number(to_char(last_day(to_date('"+fecha+"','DD/MM/YYYY')),'DD')))* decode(d.ic_moneda, 1, 1, 54, 10.75),2) AS saldo_promedio, "+
					" cep.IG_OBRA_PUBLICA_PORC, (SUM(d.fn_monto)*cep.IG_OBRA_PUBLICA_PORC)/100 "+
			" FROM com_documento d, "+
									" com_docto_seleccionado ds,  "+
									" com_solicitud sa, "+
									" comcat_producto_nafin cpn,  "+
									" comrel_if_epo_x_producto iexp,"+
									" comcat_epo ce, "+
									" comhis_epo_pef cep"+
						" WHERE  "+
									" sa.ic_documento = ds.ic_documento "+
									" AND ds.ic_documento = d.ic_documento  "+
									" AND ds.ic_if = iexp.ic_if "+
									" AND d.ic_epo = iexp.ic_epo  "+
									" AND cpn.ic_producto_nafin = iexp.ic_producto_nafin "+
									" AND d.ic_epo = ce.ic_epo "+
									" AND d.ic_epo = cep.ic_epo"+
									" AND cep.ig_anio = " +anio+
									" AND d.ic_moneda = 1 "+
									" AND iexp.ic_producto_nafin = 1 "+
									" AND sa.ic_estatus_solic in (3, 5) "+
									" AND sa.df_fecha_solicitud >= trunc(TO_DATE('"+fecha+"', 'DD/MM/YYYY'))   "+//modificacion para obtener unicamente los doctos operados del periodo
									" AND sa.df_fecha_solicitud < trunc(last_day(TO_DATE('"+fecha+"', 'DD/MM/YYYY'))+1)  "+
			" GROUP BY d.ic_moneda, ce.ic_epo, ce.CG_RAZON_SOCIAL, cep.IG_OBRA_PUBLICA_PORC";


			rs = con.queryDB(qryEpoPef);
			System.out.println("realiza consulta con exito::::");
			while(rs.next())
			{
				paramRep = new ArrayList();
				paramRep.add(rs.getString(1));//id epo
				paramRep.add(rs.getString(2));//razon social epo
				paramRep.add(rs.getString(3));//id moneda
				paramRep.add(rs.getString(4));//monto operado
				paramRep.add(rs.getString(5));//saldo promedio diario
				paramRep.add(rs.getString(6));//porcentaje obra publica
				paramRep.add(rs.getString(7));//monto correspondiente a obra publica

				colParamRep.add(paramRep);
			}
			rs.close();
		}catch(Exception e)
		{
			e.printStackTrace();
			con.cierraConexionDB();
			throw new NafinException("SIST0001");
		}finally
		{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return colParamRep;
	}

	/**
	 *
	 * Este metodo verifica si ya es posible consultar los siguientes Reportes Especiales:
	 *			1. Resumen de PyMES
	 *			2. Reporte Anal�tico de Documentos
	 *			3. Resumen de Operaci�n EPO
	 *			4. Avisos de Notificaci�n (IF).
	 * Solo se tiene considerado el producto 1: Descuento Electronico.
	 *
	 * @return <tt>HashMap</tt> con llave <tt>"PERMISO_CONSULTA"</tt> en <tt>true</tt> si ya se pueden
	 *         consultar los reportes. Adicionalmente se envia la hora en formato 24 hrs (la cual se puede acceder
	 *         mediante la llave <tt>"HORA_REPORTE"</tt>), a partir de la cual se pueden
	 *			  consultar los reportes.
	 *
	 * @since  F### - 2011 -- Mantenimiento N@E
	 * @author jshernandez
	 *
	 */
	public HashMap getPermisoConsultaReportesEPO()
		throws AppException{

		 log.info("getPermisoConsultaReportesEPO(E)");

		 AccesoDB 				con  					= new AccesoDB();
		 StringBuffer 			qrySentencia 		= new StringBuffer();
		 ResultSet 				rs 					= null;
		 PreparedStatement 	ps		 				= null;

		 HashMap					consulta				= new HashMap();
		 boolean					permisoConsulta	= true;

		 String horaReporte	= null;
		 String horaActual	= null;

		 try {

		 	con.conexionDB();

			// Consultar Hora de Acceso a los reportes especiales
			qrySentencia.append(
					"SELECT 														"  +
					"	CG_HORA_REPORTES_ESEPCIALES AS HORA_REPORTE  "  +
					"FROM 														"  +
					"	COMCAT_PRODUCTO_NAFIN 								"  +
					"WHERE 														"  +
					"	IC_PRODUCTO_NAFIN = ? 								" // 1: Descuento Electronico
			);

			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,1); // 1: Descuento Electronico
			rs = ps.executeQuery();

			if(rs != null && rs.next()){
				horaReporte = rs.getString("HORA_REPORTE");
			}
			horaReporte = horaReporte == null || horaReporte.trim().equals("")?"15:00":horaReporte;

			// Obtener la hora actual
			horaActual  = Fecha.getHoraActual("HH24:MI");

			// Comparar horas
			SimpleDateFormat 		dateformater 	= new SimpleDateFormat("HH:mm");
			java.util.Date 		dateReporte		= dateformater.parse(horaReporte);
			java.util.Date 		dateActual		= dateformater.parse(horaActual);

			if( dateReporte.compareTo(dateActual) > 0 ){
				permisoConsulta = false;
			}else{
				permisoConsulta = true;
			}

			// Enviar resultados de la validacion
			consulta.put("PERMISO_CONSULTA",	String.valueOf(permisoConsulta));
			consulta.put("HORA_REPORTE",		horaReporte);

		 } catch(Exception e) {
			log.error("getPermisoConsultaReportesEPO(Exception)");
			e.printStackTrace();
			throw new AppException("Error al consultar la Hora de acceso a lo consulta de Reportes Especiales.");
		 } finally {
		 	if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getPermisoConsultaReportesEPO(S)");
		 }
		 return consulta;

	}
	/**
	 * Este m�todo actualiza los d�as l�mite para notificaci�n y reactivaci�n
	 * para cesi�n de derechos
	 *
	 * @since 	Migraci�n EPO 2012
	 * @param 	porcentajeM Porcentaje de descuento M.N.
	 * @param 	porcentajeD Porcentaje de descuento en d�lares.
	 * @param 	operaFactVenc Opera Factoraje Vencido.
	 * @param 	operaFactDist Opera Factoraje Distribuido.
	 * @param 	diasNotif D�as l�mite para Notificaci�n (Cesi�n de derechos).
	 * @param 	diasNotifReact D�as l�mite para Reactivaci�n (Cesi�n de derechos).
	 * @param   claveEpo Clave de la EPO.
	 * @return 	HashMap Indicadores de actualizaci�n de par�metros.
	 *
	 */
	public boolean actualizaOtrosParametros(String porcentajeM, String porcentajeD, String operaFactVenc, String operaFactDist, String diasNotif, String diasNotifReact, String claveEpo){
		AccesoDB		con				=	new AccesoDB();
		StringBuffer qryCesion;
		StringBuffer qrySQLCesion;
		boolean hayExito = true;

		try{
			con.conectarDB();
			qryCesion = new StringBuffer();
			qryCesion.append(" SELECT COUNT (1)");
			qryCesion.append("   FROM comrel_producto_epo");
			qryCesion.append("  WHERE ic_producto_nafin = 9");
			qryCesion.append("    AND ic_epo = ");
			qryCesion.append(claveEpo);

			ResultSet rsExisteCesion = con.queryDB(qryCesion.toString());
			log.info("qryCesion = " + qryCesion.toString());
			int existeCesion = 0;
			if(rsExisteCesion.next())
				existeCesion = rsExisteCesion.getInt(1);
			rsExisteCesion.close();
			con.cierraStatement();

/************************* Cesion de derechos *****************************************/
        	qrySQLCesion = new StringBuffer();
			if(existeCesion>0) {
            if("".equals(diasNotif)){
					diasNotif = "''";
				}
            if("".equals(diasNotifReact)){
					diasNotifReact = "''";
				}
				qrySQLCesion.append(" UPDATE comrel_producto_epo");
            qrySQLCesion.append("    SET ig_dias_pnotificacion = ");
				qrySQLCesion.append(diasNotif);
				qrySQLCesion.append("        ,ig_dias_pnotificacion_reactiv = ");
				qrySQLCesion.append(diasNotifReact);
				qrySQLCesion.append("  WHERE ic_producto_nafin = 9 ");
				qrySQLCesion.append("    AND ic_epo = ");
				qrySQLCesion.append(claveEpo);
			}else{
				if(!diasNotif.equals("") || !diasNotifReact.equals("")){
					qrySQLCesion.append(" INSERT INTO comrel_producto_epo");
					qrySQLCesion.append("             (ic_producto_nafin, ic_epo, fn_aforo, fn_aforo_dl, cs_factoraje_vencido, cs_factoraje_distribuido, ig_dias_pnotificacion, ig_dias_pnotificacion_reactiv)");
					qrySQLCesion.append("      VALUES (9, ");// ?, ?, ?, ?, ?, ?, ?)");
					qrySQLCesion.append(claveEpo + ", ");
					qrySQLCesion.append(porcentajeM + ", ");
					qrySQLCesion.append(porcentajeD + ", ");
					qrySQLCesion.append("'" + operaFactVenc + "', '");
					qrySQLCesion.append(operaFactDist + "', '");
					qrySQLCesion.append(diasNotif + "', '");
					qrySQLCesion.append(diasNotifReact + "' )");
				}
			}
			try{
				if(qrySQLCesion.toString() != null && !qrySQLCesion.toString().equals("")){
					con.ejecutaSQL(qrySQLCesion.toString());
					log.info("qrySQLCesion = " + qrySQLCesion.toString());
				}//FODEA 037 - 2009 ACF
			}catch(SQLException errorSQL){
				hayExito = false;
				throw new AppException("Error al actualizar los datos");
			}
/*************************************************************************/
		}catch(Exception e){
			log.error("actualizaOtrosParametrosCesion(Error)",e);
			hayExito = false;
			throw new AppException("Error al actualizar");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(hayExito);
				con.cierraConexionDB();
				log.info("actualizaOtrosParametrosCesion");
			}
		}
		return hayExito;
	}
	/**
	 * Este m�todo da de alta o actualiza un documento y producto seleccionados.
	 *
	 * @since 	Migraci�n EPO 2012
	 * @param 	claveEpo Clave de la EPO.
	 * @param 	producto Producto selecionado.
	 * @return 	documento Documento seleccionado.
	 *
	 */
		public int actualizaDocumentoXProducto(String claveEpo, String producto, String documento){
		StringBuffer queryExisteDocto;
		StringBuffer qryAltaDocto;
		StringBuffer qryCambiaDocto;
		AccesoDB		con				=	new AccesoDB();
		int tipoAccion = 0;

		try{
			con.conexionDB();
			queryExisteDocto = new StringBuffer();

			int iExiste = 0;
			queryExisteDocto.append("SELECT count(*) FROM comrel_producto_docto");
			queryExisteDocto.append("	WHERE ic_epo = ");
			queryExisteDocto.append(claveEpo);
			queryExisteDocto.append(" AND ic_producto_nafin = ");
			queryExisteDocto.append(producto);
			ResultSet rsExisteDocto = con.queryDB(queryExisteDocto.toString());
			if(rsExisteDocto == null){
				tipoAccion = 0;
			}else{
				rsExisteDocto.next();
				iExiste = rsExisteDocto.getInt(1);
				log.info("queryExisteDocto = " + queryExisteDocto.toString());
			}
			if(iExiste == 0){
				qryAltaDocto = new StringBuffer();
				qryAltaDocto.append("INSERT INTO comrel_producto_docto(ic_epo, ic_producto_nafin, ic_clase_docto) " );
				qryAltaDocto.append("VALUES(");
				qryAltaDocto.append(claveEpo + ",");
				qryAltaDocto.append(producto + ",");
				qryAltaDocto.append(documento + ")");
				try{
					con.ejecutaSQL(qryAltaDocto.toString());
					con.terminaTransaccion(true);
					log.info("qryAltaDocto = " + qryAltaDocto.toString());
					tipoAccion = 1;
				}catch(Exception e){
					con.terminaTransaccion(false);
					con.cierraConexionDB();
					tipoAccion = 0;
				}
			}else{
				qryCambiaDocto = new StringBuffer();
				qryCambiaDocto.append("UPDATE comrel_producto_docto SET ic_clase_docto = ");
				qryCambiaDocto.append(documento);
				qryCambiaDocto.append(" WHERE ic_epo = ");
				qryCambiaDocto.append(claveEpo);
				qryCambiaDocto.append(" AND ic_producto_nafin = ");
				qryCambiaDocto.append(producto);

				try{
					con.ejecutaSQL(qryCambiaDocto.toString());
					con.terminaTransaccion(true);
					log.info("qryCambiaDocto = " + qryCambiaDocto.toString());
					tipoAccion = 2;
				}
				catch(Exception e){
					con.terminaTransaccion(false);
					con.cierraConexionDB();
					tipoAccion = 0;
				}
			}
		}catch(Exception e){
			log.error("actualizaDocumentoXProducto(Error)",e);
			tipoAccion = 0;
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return tipoAccion;
	}

	/**
	 * Este metodo actualiza la clasificacion de Epo. Administracion-Clasificacion-Individual
	 */
	public void actualizaClasificacionEpo(String ic_epo,String ic_calif, List listCalif){
		log.info("actualizaClasificacionEpo(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer strSQL;
		List varBind = new ArrayList();
		try{
			con.conexionDB();
			if(listCalif!=null && listCalif.size()>0){
				for(int x=0; x<listCalif.size();x++){
					varBind = new ArrayList();
					strSQL = new StringBuffer();
					String ic_pyme = (listCalif.get(x)).toString();
					strSQL.append(	" UPDATE comrel_pyme_epo "+
										" SET ic_calificacion= ? "+
										" WHERE ic_pyme= ? " +
										" AND ic_epo= ? ");
					varBind.add(ic_calif);
					varBind.add(ic_pyme);
					varBind.add(ic_epo);
					try{
						ps = con.queryPrecompilado(strSQL.toString(), varBind);
						ps.executeUpdate();
						ps.close();
						con.terminaTransaccion(true);
					}catch(Exception ex){
						con.terminaTransaccion(false);
					}
				}
			}
		} catch(Exception e) {
			log.info("actualizaClasificacionEpo(ERROR)");
			e.printStackTrace();
			throw new AppException();
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("actualizaClasificacionEpo(F)");
		}
	}

	/**
	 * Este metodo obtiene si existen registros de proveedores
	 */
	public String getConteoClasificacion(String ic_epo,String epo_pyme){
		log.info("getConteoClasificacion(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL;
		int noCampos = 0;
		String resultado="";
		try{
			con.conexionDB();
			strSQL = new StringBuffer();
			strSQL.append(	"SELECT COUNT(*) FROM comrel_pyme_epo WHERE ic_epo = ? "+
								" AND cg_pyme_epo_interno= ? ");
			ps = con.queryPrecompilado(strSQL.toString());
			ps.setString(1,ic_epo);
			ps.setString(2,epo_pyme);
			rs = ps.executeQuery();
			if (rs.next()){
				noCampos = rs.getInt(1);
			}
			resultado = Integer.toString(noCampos);
		} catch(Exception e) {
			log.info("getConteoClasificacion(ERROR)");
			e.printStackTrace();
			throw new AppException();
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getConteoClasificacion(F)");
		}
		return resultado;
	}

	/**
	 * Este metodo actualiza la clasificacion de Epo en base al archivo cargado
	 * Administracion-Clasificacion-Masiva
	 */
	public void actualizaClasificacionMasivaEpo(String ic_epo,String ic_calif, String epo_pyme){
		log.info("actualizaClasificacionMasivaEpo(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer strSQL;
		List varBind = new ArrayList();
		boolean transaccion = true;
		try{
			con.conexionDB();
			strSQL = new StringBuffer();
			strSQL.append(	" UPDATE comrel_pyme_epo "+
								" SET ic_calificacion= ? "+
								" WHERE ic_epo= ? "+
								" AND cg_pyme_epo_interno = ? ");
			varBind.add(ic_calif);
			varBind.add(ic_epo);
			varBind.add(epo_pyme);
			ps = con.queryPrecompilado(strSQL.toString(), varBind);
			ps.executeUpdate();
			ps.close();
		} catch(Exception e) {
			transaccion=false;
			log.info("actualizaClasificacionMasivaEpo(ERROR)");
			e.printStackTrace();
			throw new AppException();
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
			log.info("actualizaClasificacionMasivaEpo(F)");
		}
	}

	/**
	 * Obtiene los datos para crear campos dinamicos en comrel_visor
	 * @param ic_epo  La clave epo proveniente al seleccionar el tipo de referencia y producto
	 * @return tipo de datos Registro, que es una lista de objetos del query resultante
	 *
	 */
	public Registros getCamposAdicionalesEpo(String ic_epo, String ic_producto) {

		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append("SELECT ic_no_campo, ic_epo, INITCAP(cg_nombre_campo) AS nombre_campo, cg_tipo_dato, ig_longitud,cg_tipo_objeto, cs_obligatorio " +
							" FROM comrel_visor " +
							" WHERE ic_epo= ? "	+
							//" AND ic_no_campo BETWEEN ? AND ? "+
							" AND ic_producto_nafin = ? " +
							" ORDER BY ic_no_campo");
			List params = new ArrayList();
			params.add(new Integer(ic_epo));
			//params.add(new Integer(1));
			//params.add(new Integer(5));
			params.add(new Integer(ic_producto));
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;
		} catch (Exception e) {
			log.error("getCamposAdicionalesEpo(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los campos dinamicos de la EPO", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getCamposAdicionalesEpo(S) ::..");
		}
	}

	/**
	 * Obtiene los datos para crear campos dinamicos en comrel_visor_detalle
	 * @param ic_epo  La clave epo proveniente al seleccionar el tipo de referencia y producto
	 * @return tipo de datos Registro, que es una lista de objetos del query resultante
	 *
	 */
	public Registros getCamposAdicionalesDetalleEpo(String ic_epo, String ic_producto) {

		AccesoDB con = new AccesoDB();
		StringBuffer query;

		try {
			con.conexionDB();
			query = new StringBuffer();
			query.append("SELECT ic_epo, ic_no_campo, INITCAP(cg_nombre_campo) AS nombre_campo, cg_tipo_dato, ig_longitud " +
							" FROM comrel_visor_detalle " +
							" WHERE ic_epo= ? " +
							//" AND ic_no_campo BETWEEN ? AND ?" +
							" AND ic_producto_nafin=? ORDER BY ic_no_campo");
			List params = new ArrayList();
			params.add(new Integer(ic_epo));
			//params.add(new Integer(1));
			//params.add(new Integer(10));
			params.add(new Integer(ic_producto));
			Registros reg = con.consultarDB(query.toString(), params);

			return reg;
		} catch (Exception e) {
			log.error("getCamposAdicionalesDetalleEpo(Error) ::..");
			throw new AppException("Ocurrio un error al obtener los campos dinamicos de la EPO", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getCamposAdicionalesDetalleEpo(S) ::..");
		}
	}

	/**
	 * Actualiza o inserta campos din�micos en comrel_visor o comrel_visor_detalle
	 * @param ic_epo, Hastable datos (contiene la informacion referente a los campos adicionales)
	 * @return void
	 *
	 */
	public void actualizaCamposAdicionales(String ic_epo,Hashtable datos){
		log.info("actualizaCamposAdicionales(I)");
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer strSQL;
		String sel_tipo = "", sel_producto = "", tabla = "", val_nombre = "", val_tipo = "", val_longitud = "", val_objeto = "", val_numcampo = "", campos_agregar = "";
		String rad_oblig ="", operacion="",	folio = "",	hdn_bandera = "";
		int contador_fin = 0;
		List varBind = new ArrayList();
		boolean transaccion = true;
		campos_agregar =	(String)datos.get("campos_agregar");
		sel_tipo			=	(String)datos.get("_sel_tipo");
		sel_producto	=	(String)datos.get("_sel_producto");
		operacion		=	(String)datos.get("operacion");
		hdn_bandera		=	((String)datos.get("hdn_bandera"));

		try{
			con.conexionDB();
			if (sel_tipo.equals("1")){
				tabla = "comrel_visor";
				if(campos_agregar == null || campos_agregar.equals("")){
					contador_fin = 5;
				}else{
					contador_fin = Integer.parseInt(campos_agregar);
				}
			}else{
				tabla = "comrel_visor_detalle";
				if(campos_agregar == null || campos_agregar.equals("")){
					contador_fin = 10;
				}else{
					contador_fin = Integer.parseInt(campos_agregar);
				}
			}

			for (int x = 0; x < contador_fin; x++){
				String nom_campo = "TxtNombre" + String.valueOf(x);
				val_nombre = (String)datos.get(nom_campo);
				String nom_campo1 = "_sel_tipocampo" + String.valueOf(x);
				val_tipo = (String)datos.get(nom_campo1);
				String nom_campo2 = "TxtLongitud" + String.valueOf(x);
				val_longitud = (String)datos.get(nom_campo2);
				String nom_campo3 = "hdn_numcampo" + String.valueOf(x);
				val_numcampo = (String)datos.get(nom_campo3);
				String nom_campo5 = "rad_oblig" + String.valueOf(x);
				String radio = (String)datos.get(nom_campo5);
				rad_oblig = ((radio)==null)?"":radio;
				if(x==0 || x==1 || sel_producto.equals("9")){
					String nom_campo4 = "sel_tipobjeto" + String.valueOf(x);
					val_objeto = (String)datos.get(nom_campo4);
				}

				if((val_nombre != null && !val_nombre.equals("")) && ( val_longitud != null && !val_longitud.equals("") && !nom_campo1.equals("") )){
					strSQL = new StringBuffer();
					varBind = new ArrayList();
					if(hdn_bandera.equals("UPDATE")){
						strSQL.append(	" UPDATE " + tabla + " SET cg_nombre_campo = ?, ig_longitud = ?, cg_tipo_dato = ? ");
						varBind.add(val_nombre);
						varBind.add(val_longitud);
                                                varBind.add(val_tipo);
						if(sel_tipo.equals("1")){
								if(sel_producto.equals("1") && (x==0 || x==1) || sel_producto.equals("9")){
									strSQL.append( " , cg_tipo_objeto = ? ");
									varBind.add(val_objeto);

									if(sel_producto.equals("9")){
										strSQL.append(" , cs_obligatorio = ? ");
										varBind.add(rad_oblig);
									}
								}else{
									strSQL.append(" , cg_tipo_objeto = ? ");
									varBind.add("T");
								}
						}
						strSQL.append(" WHERE ic_epo= ? AND ic_no_campo = ? AND ic_producto_nafin = ? ");
						varBind.add(new Long(ic_epo));
						varBind.add(val_numcampo);
						varBind.add(sel_producto);

					}else{
						strSQL.append("SELECT NVL(MAX(ic_no_campo) + 1,1) FROM "  + tabla + " WHERE ic_epo = ? AND ic_producto_nafin = ? ");
						varBind.add(new Long(ic_epo));
						varBind.add(sel_producto);

						ps = con.queryPrecompilado(strSQL.toString(), varBind);
						rs = ps.executeQuery();
						strSQL = new StringBuffer();
						varBind = new ArrayList();
						if (rs.next()){
							folio = rs.getString(1);
							if(sel_tipo.equals("1") && sel_producto.equals("1") ){
								if(folio.equals("1") || folio.equals("2")){
									strSQL.append("INSERT INTO "+tabla+" (ic_no_campo,ic_epo,cg_nombre_campo,cg_tipo_dato,ig_longitud,ic_producto_nafin,cg_tipo_objeto) VALUES (?,?,?,?,?,?,?)");
									varBind.add(new Long(folio));
									varBind.add(new Long(ic_epo));
									varBind.add(val_nombre);
									varBind.add(val_tipo);
									varBind.add(val_longitud);
									varBind.add(sel_producto);
									varBind.add(val_objeto);
								}else{
									strSQL.append("INSERT INTO "+tabla+" (ic_no_campo,ic_epo,cg_nombre_campo,cg_tipo_dato,ig_longitud,ic_producto_nafin,cg_tipo_objeto) VALUES (?,?,?,?,?,?,?)");
									varBind.add(new Long(folio));
									varBind.add(new Long(ic_epo));
									varBind.add(val_nombre);
									varBind.add(val_tipo);
									varBind.add(val_longitud);
									varBind.add(sel_producto);
									varBind.add("T");
								}
							}else{
								strSQL.append("INSERT INTO "+tabla+" (ic_no_campo,ic_epo,cg_nombre_campo,cg_tipo_dato,ig_longitud,ic_producto_nafin) VALUES (?,?,?,?,?,?)");
								varBind.add(new Long(folio));
								varBind.add(new Long(ic_epo));
								varBind.add(val_nombre);
								varBind.add(val_tipo);
								varBind.add(val_longitud);
								varBind.add(sel_producto);
							}
						}
						if(rs!=null)rs.close();
						if(ps!=null)ps.close();
					}
					ps = con.queryPrecompilado(strSQL.toString(), varBind);
					ps.executeUpdate();
					if(ps!=null)ps.close();
				}
			}//for x



		} catch(Exception e) {
			transaccion=false;
			log.info("actualizaCamposAdicionales(ERROR)");
			e.printStackTrace();
			throw new AppException();
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(transaccion);
				con.cierraConexionDB();
			}
			log.info("actualizaCamposAdicionales(F)");
		}
	}


	/**
	 * Obtiene la parametrizacion para la consulta linea de Fondeo
	 * @param iNoCliente  intermediario financiero.
	 * @return tipo de parametrizacion segun el cliente
	 *
	 */
	public String getParametrizacionFondeo(String iNoCliente) {

		log.info("parametrizacion(E)");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		String  parametrizacion = "";

		try{
			con.conexionDB();

			query = new StringBuffer();
			query.append(" SELECT  NVL(cg_valor, '') CG_VALOR  ");
			query.append(" FROM com_parametrizacion_if  ");
			query.append(" WHERE cc_parametro_if ='TIPO_CONSULTA' ");
			query.append("  AND ic_if = "+iNoCliente+"");
			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();

			while(rs.next()){
				parametrizacion = rs.getString("CG_VALOR");
			}
			rs.close();
			ps.close();


			return parametrizacion;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error  parametrizacion", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("parametrizacion(S)");
		}
	}


/**
	 * Descripcion: M�todo que se encarga de realizar una consulta para obtener los encabezados
	 *              de las tablas en la pantalla de matriz Apis.
	 *
	 * @return Mapa con informaci�n de los nombres de encabezados derivados de la consulta.
 */

	public Map getEncabezadosMatrizApis() {
		log.info("getEncabezadosMatrizApis(E)");
		HashMap hmProductos = new HashMap();
		AccesoDB con = null;
		PreparedStatement ps = null;
		ResultSet rsAPI = null;

		try {
			con = new AccesoDB();
			con.conexionDB();
			String sQuery = "select cc_producto, cg_nombre "+
						" from comcat_productos_api "+
						" where cc_producto not in ('0F', '0C', '0E')"+
						" order by ig_orden ";
			ps = con.queryPrecompilado(sQuery);
			//ResultSet rs = ps.executeQuery();

			rsAPI = ps.executeQuery();
			while(rsAPI.next()) {
				hmProductos.put(rsAPI.getString("cc_producto"), rsAPI.getString("cg_nombre"));
			}
			log.info("getEncabezadosMatrizApis(S)");
			if(rsAPI != null)
				rsAPI.close();
			if (ps != null)
				ps.close();

			return hmProductos;

		}catch (Exception e) {
			log.error("Error en getEncabezadosMatrizApis: "+e);
			throw new AppException("Error en consulta para encabezados de Matriz Apis", e);
		} finally {

			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}


	} //fin metodo


/**
	 * Descripcion: M�todo que se encarga de realizar una consulta para obtener los datos del
	 * 				 grid en la pantalla de bloqueo de IF.
	 * @params ic_producto_nafin   Contiene la clave del producto.
	 * @params ic_if   Contiene la clave de las IF o en caso de ser vacio
	 * 					 indicar� todas las clavesen la consulta.
	 * @return List con informaci�n derivada de la consulta.
*/

	public List getCamposBloqueoIF(String ic_producto_nafin, String ic_if ){
		log.info("getCamposBloqueoIF(E)");
		List registros = new ArrayList();
		String qrySentencia = null;
		StringBuffer condicion = new StringBuffer();

		AccesoDB con = null;
		ResultSet rs = null;

		//***********Validaci�n de parametros:*****
		try {
			if (ic_producto_nafin == null || ic_if == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." + "\n" +
					" ic_producto_nafin =" + ic_producto_nafin+ "   ic_if ="+ ic_if );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//******************************************

		try{
			con = new AccesoDB();
			con.conexionDB();
			if(!"".equals(ic_if)) {
				condicion.append("	AND i.ic_if = ? ");
			}

			if (ic_producto_nafin.equals("0")) { //Credito Electr�nico

				qrySentencia =
						" SELECT " +
						" 	DISTINCT i.ic_if,  0 as ic_producto_nafin, " +
						" 	'Credito Electronico' as ic_nombre, " +
						" 	n.ic_nafin_electronico,  " +
						" 	i.cg_razon_social,  " +
						" 	bixp.cs_bloqueo, " +
						" 	TO_CHAR(bixp.df_modificacion,'dd/mm/yyyy hh24:mi') as df_modificacion,  " +
						" 	bixp.cg_causa_bloqueo " +
						" FROM com_base_op_credito boc, " +
						" 	comcat_if i, " +
						"  comrel_nafin n, " +
						" 	comrel_bloqueo_if_x_producto bixp " +
						" WHERE  " +
						"	boc.ic_if = i.ic_if " +
						" 	AND i.ic_if = n.ic_epo_pyme_if " +
						" 	AND i.cs_habilitado = 'S' " +
						" 	AND n.cg_tipo = 'I' " +
						" " + condicion + " " +
						" 	AND i.ic_if  =  bixp.ic_if(+) " +
						" 	AND bixp.ic_producto_nafin(+) = 0 " +
						" ORDER BY i.cg_razon_social ";
				}

				PreparedStatement ps = con.queryPrecompilado(qrySentencia);
				int cont = 0;

				if(!"".equals(ic_if)) {
					cont++;ps.setInt(cont, Integer.parseInt(ic_if));
				}

				rs = ps.executeQuery();
				int numRegistro = 0;
				HashMap hmDatos = null;

				while(rs.next()) {
					numRegistro++;
					hmDatos = new HashMap();

					hmDatos.put("claveProducto",rs.getString("ic_producto_nafin"));
					hmDatos.put("nombreProducto",rs.getString("ic_nombre"));
					hmDatos.put("claveIf",rs.getString("ic_if"));
					hmDatos.put("nombreIf",rs.getString("cg_razon_social"));
					hmDatos.put("nafinElectronico",rs.getString("ic_nafin_electronico"));
					hmDatos.put("fechaModificacion",(rs.getString("df_modificacion") == null)?"":rs.getString("df_modificacion"));
					hmDatos.put("causaBloqueo",(rs.getString("cg_causa_bloqueo") == null)?"":rs.getString("cg_causa_bloqueo"));
					hmDatos.put("estatusBloqueo",(rs.getString("cs_bloqueo") == null)?"":rs.getString("cs_bloqueo"));

					registros.add(hmDatos);
				}// fin while
				rs.close();
				ps.close();
			}catch(Exception e){
				log.error("Error en getCamposBloqueoIF: ",e);
				throw new AppException("Error en consulta para ventana Bloqueo de IF", e);
			} finally{

				if(con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}
			log.info("getCamposBloqueoIF(S)");
			return registros;
		}//fin metodo



/**
 * Realiza y registra los cambios de estatus de bloqueo.
 * Aquellos que no sufrieron cambios no se registran en la bitacora
 * @param DatoIF[] Contiene las claves de las IF.
 * @param DatoProducto[] Contiene las claves de los productos .
 * @param DatoEstatusAnterior[] Contiene la bandera que determina si fue bloqueado, desbloqueado o ninguno.
 * @param DatoEstatusActual[] Contiene la bandera que indica que se quiere realizar actualmente (Bloquear, desbloquear o nada).
 * @param DatoCausaBloqueo[] Contiene el texto de las causas del bloqueo.
 * @return Lista de los registros que realmente fueron modificados.
 */

	private List cambiarEstatusBloqueo(String[] DatoIF,String[] DatoProducto,
			String[] DatoEstatusAnterior,String[] DatoEstatusActual,String[] DatoCausaBloqueo,
			String loginUsuario, String numElementos ){

		log.info("cambiarEstatusBloqueo(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		List registrosModificados = new ArrayList();
		PreparedStatement ps = null;
		PreparedStatement psR = null;

		try {
			con.conexionDB();
			int num= Integer.parseInt(numElementos);

			for(int i=0;i<num;i++){
				String claveIf = DatoIF[i];
				String claveProducto = DatoProducto[i];
				String estatusBloqueoAnterior = DatoEstatusAnterior[i];
				String estatusBloqueoActual = DatoEstatusActual[i];
				String causaBloqueo = DatoCausaBloqueo[i];

				boolean realizarCambioBloqueo = false;
				String bloqueo = null;

				StringBuffer strSQL = null;
				StringBuffer strSQLBit = null;

				if (estatusBloqueoActual.equals("B")) { //Bloquear
					bloqueo = "B";
					//No bloqueada con anterioridad � Desbloqueada anteriormente
					if (estatusBloqueoAnterior.equals("")) {
						realizarCambioBloqueo = true;
						strSQL = new StringBuffer();

						strSQL.append(" INSERT INTO comrel_bloqueo_if_x_producto ");
						strSQL.append(" (IC_PRODUCTO_NAFIN, IC_IF, ");
						strSQL.append(" CG_CAUSA_BLOQUEO, DF_MODIFICACION, CS_BLOQUEO, IC_USUARIO) ");
						strSQL.append(" VALUES (?,?,?,sysdate,?,?) ");

						ps = con.queryPrecompilado(strSQL.toString());
						ps.setInt(1, Integer.parseInt(claveProducto));
						ps.setInt(2, Integer.parseInt(claveIf));
						ps.setString(3, causaBloqueo);
						ps.setString(4, "B");
						ps.setString(5, loginUsuario);
						log.debug(" Sentencia::: "+strSQL);

					}else if (estatusBloqueoAnterior.equals("D")) {
						realizarCambioBloqueo = true;
						strSQL = new StringBuffer();

						strSQL.append(" UPDATE comrel_bloqueo_if_x_producto ");
						strSQL.append(" SET CG_CAUSA_BLOQUEO = ? ," );
						strSQL.append(" DF_MODIFICACION = sysdate ," );
						strSQL.append(" CS_BLOQUEO = ? ," );
						strSQL.append(" IC_USUARIO = ? " );
						strSQL.append(" WHERE IC_PRODUCTO_NAFIN = ? ");
						strSQL.append(" AND IC_IF = ?");

						ps = con.queryPrecompilado(strSQL.toString());
						ps.setString(1, causaBloqueo);
						ps.setString(2, "B");
						ps.setString(3, loginUsuario);
						ps.setInt(4, Integer.parseInt(claveProducto));
						ps.setInt(5, Integer.parseInt(claveIf));
						log.info(" Sentencia::: "+strSQL);
					}

				} else if (estatusBloqueoActual.equals("")) {	//Desbloquear
					bloqueo = "D";
					//Solo si estaba bloqueada previamente se desbloquea
					if (estatusBloqueoAnterior.equals("B")) {
						realizarCambioBloqueo = true;
						strSQL = new StringBuffer();

						strSQL.append(" UPDATE comrel_bloqueo_if_x_producto ");
						strSQL.append(" SET CG_CAUSA_BLOQUEO = ? ," );
						strSQL.append(" DF_MODIFICACION = sysdate ," );
						strSQL.append(" CS_BLOQUEO = ? ," );
						strSQL.append(" IC_USUARIO = ? " );
						strSQL.append(" WHERE IC_PRODUCTO_NAFIN = ? ");
						strSQL.append(" AND IC_IF = ?");

						ps = con.queryPrecompilado(strSQL.toString());
						ps.setString(1, causaBloqueo);
						ps.setString(2, "D");
						ps.setString(3, loginUsuario);
						ps.setInt(4, Integer.parseInt(claveProducto));
						ps.setInt(5, Integer.parseInt(claveIf));
						log.debug(" Sentencia::: "+strSQL);
					}
				}

				if (realizarCambioBloqueo) {
					List registro = new ArrayList();
					registro.add(0, claveProducto);
					registro.add(1, claveIf);
					registrosModificados.add(registro);
					strSQLBit = new StringBuffer();

					strSQLBit.append(" INSERT INTO comhis_bloqueo_if_x_producto ");
					strSQLBit.append(" (IC_MODIFICACION, IC_PRODUCTO_NAFIN, IC_IF, ");
					strSQLBit.append(" CG_CAUSA_BLOQUEO, DF_MODIFICACION, CS_BLOQUEO, IC_USUARIO) " );
					strSQLBit.append(" VALUES ((select nvl(max(ic_modificacion),0)+1 from comhis_bloqueo_if_x_producto),");
					strSQLBit.append(" ?, ?, ?, sysdate, ?, ? )");

					psR = con.queryPrecompilado(strSQLBit.toString());
					psR.setInt(1, Integer.parseInt(claveProducto));
					psR.setInt(2, Integer.parseInt(claveIf));
					psR.setString(3, causaBloqueo);
					psR.setString(4, bloqueo);
					psR.setString(5, loginUsuario);
					log.debug(" Sentencia::: "+strSQLBit);

					ps.executeUpdate();
					psR.executeUpdate();

					ps.clearParameters();
					psR.clearParameters();

					if(ps != null)
					ps.close();

					if(psR != null)
					psR.close();
				}

			}//fin del for

			return registrosModificados;
		}catch(SQLException e) {
			exito = false;
			log.error("Error en cambiarEstatusBloqueo: ",e);
			throw new AppException("Error en sentencias para modificar datos de Bloqueo de IF", e);
		}catch(NamingException e) {
			exito = false;
			log.error("Error en cambiarEstatusBloqueo: ",e);
			throw new AppException("Error en cambiarEstatusBloqueo", e);
		} finally {
			log.info("cambiarEstatusBloqueo(S)");
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}


/**
 * Realiza y registra los cambios de estatus de bloqueo.
 *
 * @param DatoIF[] Contiene las claves de las IF.
 * @param DatoProducto[] Contiene las claves de los productos .
 * @param DatoEstatusAnterior[] Contiene la bandera que determina si fue bloqueado, desbloqueado o ninguno.
 * @param DatoEstatusActual[] Contiene la bandera que indica que se quiere realizar actualmente (Bloquear, desbloquear o nada).
 * @param DatoCausaBloqueo[] Contiene el texto de las causas del bloqueo.
 * @return Lista de los registros ya modificados.
 */


	public List getCamposBloqueoIfModificaciones(String[] DatoIF,String[] DatoProducto,
					String[] DatoEstatusAnterior,String[] DatoEstatusActual,String[] DatoCausaBloqueo, String iNoUsuario, String numElementos){

		log.info("getCamposBloqueoIfModificaciones(E)");
		//***********Validaci�n de parametros:************************************
		try {
			if (DatoIF == null || DatoProducto == null || DatoEstatusAnterior == null || DatoEstatusActual == null
				|| DatoCausaBloqueo == null || iNoUsuario == null || numElementos == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//************************************************************************

		List registroModificados = cambiarEstatusBloqueo(DatoIF,DatoProducto,DatoEstatusAnterior,
		DatoEstatusActual,DatoCausaBloqueo,iNoUsuario, numElementos);
		AccesoDB con = null;
		PreparedStatement ps = null;
		List registros = new ArrayList();
		Iterator it = registroModificados.iterator();


		StringBuffer condiciones = new StringBuffer();
		List conditions = new ArrayList();
		int numRegistrosModificados = 0;
		condiciones.append(" AND ( ");

		while (it.hasNext()) {
			numRegistrosModificados++;
			List registro = (List) it.next();
			String claveProducto = (String) registro.get(0);
			String claveIf = (String) registro.get(1);

			if (numRegistrosModificados > 1 ) {
				condiciones.append(" OR ");
			}
			condiciones.append(" (bixp.ic_if = ? AND bixp.ic_producto_nafin = ? )");
			conditions.add(claveIf);
			conditions.add(claveProducto);
		}
		condiciones.append(" )");

		String strSQL = null;

		strSQL =
			" SELECT  " +
			" 	CASE WHEN bixp.ic_producto_nafin = 0 THEN " +
			" 	'Credito Electronico' " +
			"	END as ic_nombre, " +
			" 	n.ic_nafin_electronico, " +
			" 	i.cg_razon_social, " +
			" 	bixp.cs_bloqueo,  " +
			" 	TO_CHAR(bixp.df_modificacion,'dd/mm/yyyy hh24:mi') as df_modificacion, " +
			" 	bixp.cg_causa_bloqueo  " +
			" FROM " +
			" 	comrel_bloqueo_if_x_producto bixp, " +
			" 	comrel_nafin n, comcat_if i " +
			" WHERE " +
			" 	bixp.ic_if = i.ic_if " +
			" 	AND bixp.ic_if = n.ic_epo_pyme_if " +
			" 	AND n.cg_tipo = 'I' " +
			condiciones +
			" ORDER BY i.cg_razon_social ";

		try{
			con = new AccesoDB();
			con.conexionDB();

			if (numRegistrosModificados > 0) {
				int p=0;
				int i=0;
				int control=0;
				ps = con.queryPrecompilado(strSQL);

				while(control < numRegistrosModificados)
				{
					p++;
					ps.setInt(p,Integer.parseInt(conditions.get(i).toString()));
					p++;
					i++;
					ps.setInt(p,Integer.parseInt(conditions.get(i).toString()));
					i++;
					control++;
				}

				ResultSet rs = ps.executeQuery();
				HashMap hmDatos = null;

				while(rs.next()) {
					//String nombreProducto = rs.getString("ic_nombre");
					//String nombreIf = rs.getString("cg_razon_social");
					//String nafinElectronico = rs.getString("ic_nafin_electronico");
					//String fechaModificacion = (rs.getString("df_modificacion") == null)?"":rs.getString("df_modificacion");
					//String causaBloqueo = (rs.getString("cg_causa_bloqueo") == null)?"":rs.getString("cg_causa_bloqueo");
					//String estatusBloqueo = (rs.getString("cs_bloqueo").equals("B"))?"Bloqueada":"Desbloqueada";

					hmDatos = new HashMap();
					hmDatos.put("nombreProducto",rs.getString("ic_nombre"));
					hmDatos.put("nombreIf",rs.getString("cg_razon_social"));
					hmDatos.put("nafinElectronico",rs.getString("ic_nafin_electronico"));
					hmDatos.put("fechaModificacion",rs.getString("df_modificacion"));
					hmDatos.put("causaBloqueo",rs.getString("cg_causa_bloqueo"));
					hmDatos.put("estatusBloqueo",(rs.getString("cs_bloqueo").equals("B"))?"Bloqueada":"Desbloqueada");

					registros.add(hmDatos);
				}

				rs.close();
				if(ps != null)
					ps.close();
				con.cierraStatement();
			}

		}catch(Exception e){
				log.error("Error en getCamposBloqueoIfModificaciones: ",e);
				throw new AppException("Error en getCamposBloqueoIfModificaciones ", e);
		} finally{
			if(con.hayConexionAbierta()) con.cierraConexionDB();
		}

		log.info("getCamposBloqueoIfModificaciones(S)");
		return registros;
	}



	public Map getMoneda(String claveMoneda) {
	//***********Validaci�n de parametros:************************************
		try {
			if (claveMoneda == null ) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos en getMoneda " + e.getMessage() );
		}
	//************************************************************************
		log.info("getMoneda(E)");
		HashMap hmProductos = new HashMap();
		AccesoDB con = null;
		PreparedStatement ps = null;
		ResultSet rsAPI = null;

		try {
			con = new AccesoDB();
			con.conexionDB();
			String sQuery = "SELECT cd_nombre, ic_moneda FROM comcat_moneda "+
					" WHERE ic_moneda = ? ";
			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, Integer.parseInt(claveMoneda));

			rsAPI = ps.executeQuery();
			while(rsAPI.next()) {
				hmProductos.put("CLAVEMONEDA", rsAPI.getString("ic_moneda"));
				hmProductos.put("NOMBREMONEDA", rsAPI.getString("cd_nombre"));
			}

			if(rsAPI != null)
				rsAPI.close();
			if (ps != null)
				ps.close();

			return hmProductos;

		}catch (Exception e) {
			log.error("Error en getMoneda: "+e);
			throw new AppException("Error en consulta para conseguir Moneda", e);
		} finally {
			log.info("getMoneda(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}//fin metodo


	public Map consultaValorMoneda(String txtDesde, String txtHasta, String cboMoneda){
		//***********Validaci�n de parametros:*****************************************
		try {
			if (txtDesde == null || txtHasta == null || cboMoneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." + "\n" +
					" txtDesde =" + txtDesde+ " txtHasta ="+ txtHasta+ " cboMoneda ="+ cboMoneda );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************
		log.info("consultaValorMoneda(E)");
		HashMap hmProductos = new HashMap();
		AccesoDB con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			con = new AccesoDB();
			con.conexionDB();

			String sQuery = "SELECT TC.fn_valor_compra AS ValorCompra, TC.fn_valor_venta AS ValorVenta"+
			" , TO_CHAR(TC.dc_fecha,'DD/MM/YYYY') AS Fecha "+
			" FROM comcat_moneda M, com_tipo_cambio TC "+
			" WHERE M.IC_MONEDA = TC.IC_MONEDA AND TC.dc_fecha "+
			" BETWEEN TO_Date( ? ,'DD/MM/YYYY')"+
			" AND TO_Date( ? ,'DD/MM/YYYY') "+
			" AND TC.ic_moneda = ? ORDER BY TC.dc_fecha";

			log.debug(sQuery);
			ps = con.queryPrecompilado(sQuery);
			ps.setString(1, txtDesde);
			ps.setString(2, txtHasta);
			ps.setInt(3, Integer.parseInt(cboMoneda));

			rs = ps.executeQuery();
			while(rs.next()) {
				hmProductos.put("VALORCOMPRA", rs.getString("ValorCompra"));
				hmProductos.put("	VALORVENTA", rs.getString("ValorVenta"));
				hmProductos.put("FECHA",rs.getString("Fecha"));
			}

			if(rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			return hmProductos;

		}catch (Exception e) {
			log.error("Error en consultaValorMoneda: "+e);
			throw new AppException("Error en consulta para conseguir el valro de la Moneda", e);
		} finally {
			log.info("consultaValorMoneda(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}//fin metodo




/** 
 * Obtiene el plazo m�nimo para la pantalla Captura Cadenas en Bases de Operacion.
 *
 * @param no_epo        Contiene la clave de la Epo.
 * @param ic_producto   Contiene las claves del producto .
 * @param tipoCartera   Contiene la clave del piso.
 * @param tipo_plazo    Contiene la clave del tipo de plazo.
 * @param ic_moneda     Contiene la clave de la moneda.
 * @return Map          Con el plazo m�nimo siguiente y �ltimo.
 */ 

	public Map enviaEPOCapturaCadenas (String no_epo, String ic_producto, String tipoCartera,  
											 String tipo_plazo, String ic_moneda, String tipoPago  ){
		//***********Validaci�n de parametros:*****************************************
		try {
			if (no_epo == null || ic_producto == null || tipoCartera == null
				 || tipo_plazo == null || ic_moneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************		
		log.info("enviaEPOCapturaCadenas(E)");		
		AccesoDB con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;	
		int ultimo = 0;
		String bo = "";
		String plazo = "";
		HashMap datos = new HashMap();
		
		try {
			con = new AccesoDB();
			con.conexionDB();
			
			String sQuery =  "select ig_plazo_maximo, ic_base_operacion from comrel_base_operacion "+
					"where ic_epo = ?" +
					" and ic_moneda = ?" + 
					" and ic_producto_nafin = ?" + 
					" and ig_tipo_cartera = ?" + 
					(ic_producto.equals("1") ||ic_producto.equals("5")?" and cs_tipo_plazo = '"+tipo_plazo+"'":"")+
					"  and IG_TIPO_PAGO = ?  "+  
					" order by ig_plazo_maximo desc";
			
			log.debug(sQuery);
			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, Integer.parseInt(no_epo));
			ps.setInt(2, Integer.parseInt(ic_moneda));
			ps.setInt(3, Integer.parseInt(ic_producto));
			ps.setInt(4, Integer.parseInt(tipoCartera));
			ps.setInt(5, Integer.parseInt(tipoPago) );  //Fodea 09-2015  //Financiamiento con intereses(1)   y Meses sin intereses (2)
			
			rs = ps.executeQuery();
			
			int p=0;
			if (rs.next()) {
				p = rs.getInt(1)+1;
				ultimo = rs.getInt(1);
				bo = rs.getString(2);
				plazo = ""+p;
			} else { plazo="1"; }
			
			if(rs != null)
				rs.close();
			if (ps != null)
				ps.close();
			
			datos.put("PLAZO", plazo);
			datos.put("ULTIMO",""+ultimo);
			
			return datos;	
		
		}catch (Exception e) {
			log.error("Error al conseguir plazo (enviaEPOCapturaCadenas): ", e);
			throw new AppException("Error en consulta de envia epo", e);
		} finally {
			log.info("enviaEPOCapturaCadenas(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


/**
 * Realiza la consulta y obtiene el valor de los campos del grid en la pantalla de Captura Cadenas .
 *
 * @param no_epo        Contiene la clave de la Epo.
 * @param ic_producto   Contiene las claves del producto .
 * @param tipoCartera   Contiene la clave del piso.
 * @param tipo_plazo    Contiene la clave del tipo de plazo.
 * @param ic_moneda     Contiene la clave de la moneda.
 * @return List         Con el resultado de la consulta.
 */

	public List getCamposCapturaCadenas(String no_epo, String ic_producto, String tipoCartera, 
											 String tipo_plazo, String ic_moneda, String tipoPago ){
			
		log.info("getCamposCapturaCadenas(E)");
		//***********Validaci�n de parametros:*****************************************
		try {
			if (no_epo == null || ic_producto == null || tipoCartera == null
				 || tipo_plazo == null || ic_moneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************

		AccesoDB con = null;
		PreparedStatement ps = null;
		List registros = new ArrayList();
		String strSQL = null;
	
		strSQL = 
					 " select BO.ic_base_operacion as baseoperacion, E.cg_razon_social as razonsocial, "+
					 " BO.ig_codigo_base as codigobase, BO.ig_plazo_minimo as plazominimo, "+
					 " BO.ig_plazo_maximo as plazomaximo, CBO.cg_descripcion as descripcion, "+
					" CPN.ic_nombre as nameProd"+
					", DECODE(BO.cs_recurso,'S','Pyme','N','Epo') as recurso "+
					", M.CD_NOMBRE as moneda, BO.cs_tipo_plazo as tipoplazo, BO.ig_tipo_cartera as tipocartera"+
					" ,DECODE(BO.ig_tipo_pago,'1','Financiamiento con intereses','2','Meses sin Intereses ') as tipoPago, "+ //F09-2015				
					" BO.ig_tipo_pago  as ig_tipo_pago "+//F09-2015
					
					" from comrel_base_operacion BO, comcat_base_operacion CBO, "+
					" comcat_epo E, comcat_producto_nafin CPN, comcat_moneda M "+
					" where BO.ig_codigo_base = CBO.ig_codigo_base "+
					" and BO.ic_epo = E.ic_epo "+
					" and BO.ic_producto_nafin = CPN.ic_producto_nafin"+
					" and BO.ic_moneda = M.ic_moneda"+
					" and M.ic_moneda = ?" + 
					" and BO.ic_moneda = ?" +
					" and BO.ic_epo = ?" + 
					" and BO.ic_producto_nafin = ?"+ 
					" and BO.ig_tipo_cartera = ?" + 
					(!tipoPago.equals("")?" and BO.ig_tipo_pago = '"+tipoPago+"'":"")+
					(ic_producto.equals("1") ||ic_producto.equals("5")?" and BO.cs_tipo_plazo = '"+tipo_plazo+"'":"")+
					" order by BO.ig_plazo_maximo";
		
		System.out.println(" strSQL ==  "+strSQL);  
		System.out.println(" tipoPago ==  "+tipoPago);  
		
		try{
			con = new AccesoDB();
			con.conexionDB();

			int p=0;
			int i=0;
			int control=0;
			ps = con.queryPrecompilado(strSQL);
			
			ps.setInt(1,Integer.parseInt(ic_moneda));
			ps.setInt(2,Integer.parseInt(ic_moneda));
			ps.setInt(3,Integer.parseInt(no_epo));
			ps.setInt(4,Integer.parseInt(ic_producto));
			ps.setInt(5,Integer.parseInt(tipoCartera));
			
			ResultSet rs = ps.executeQuery();
			HashMap hmDatos = null;
			
			while(rs.next()) {
				hmDatos = new HashMap();
				hmDatos.put("BASEOPERACION",rs.getString("baseoperacion"));
				hmDatos.put("RAZONSOCIAL",rs.getString("razonsocial"));
				hmDatos.put("CODIGOBASE",rs.getString("codigobase"));
				hmDatos.put("PLAZOMINIMO",rs.getString("plazominimo"));
				hmDatos.put("PLAZOMAXIMO",rs.getString("plazomaximo"));
				hmDatos.put("RAZONSOCIAL",rs.getString("razonsocial"));
				hmDatos.put("DESCRIPCION",rs.getString("descripcion"));
				hmDatos.put("NOMBRE",rs.getString("nameProd"));
				hmDatos.put("RECURSO",rs.getString("recurso"));					
				hmDatos.put("MONEDA",rs.getString("moneda"));	
				hmDatos.put("TIPOPLAZO",rs.getString("tipoplazo"));	
				hmDatos.put("TIPOCARTERA",rs.getString("tipocartera"));	
				hmDatos.put("TIPOPAGO",rs.getString("tipoPago")); 
				hmDatos.put("IG_TIPO_PAGO",rs.getString("ig_tipo_pago")); 	
				registros.add(hmDatos);
			}
		
			rs.close();
			if(ps != null)
				ps.close();
			con.cierraStatement();

		}catch(Exception e){
				log.error("Error en getCamposCapturaCadenas: ",e);
				throw new AppException("Error en getCamposCapturaCadenas ", e);
		} finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getCamposCapturaCadenas(S)");
		}
		return registros;
	}


/**
 * Inserta nuevos valores en la pantalla de Captura Cadenas .
 *
 * @param no_epo        Contiene la clave de la Epo.
 * @param ic_producto   Contiene la clave del producto .
 * @param tipoCartera   Contiene la clave del piso.
 * @param tipo_plazo    Contiene la clave del tipo de plazo.
 * @param ic_moneda     Contiene la clave de la moneda.
 * @param cod_operac    Contiene la clave del combo de c�digo de operaci�n.
 * @param plz_min       Contiene el plazo m�nimo .
 * @param plz_max       Contiene el plazo m�ximo .
 * @return List         Con el resultado de la consulta.
 */

	public String aceptarCapturaCadenas(String no_epo, String ic_producto, String tipoCartera, 
											 String tipo_plazo, String ic_moneda, String cod_operac, String plz_min,
											 String plz_max, String cod_operac_recurso, String tipoPago){
		
		//***********Validaci�n de parametros:*****************************************
		try {
			if (no_epo == null || ic_producto == null || tipoCartera == null
				 || tipo_plazo == null || ic_moneda == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************
		log.info("aceptarCapturaCadenas(E)");		
		AccesoDB con = new AccesoDB();
		boolean exito = false;
		PreparedStatement ps = null;
		String mensaje = null;
		StringBuffer strSQLBit = null;

		strSQLBit = new StringBuffer();

		strSQLBit.append( "insert into comrel_base_operacion (ic_base_operacion, ig_codigo_base, "); 
		strSQLBit.append(" ic_epo, ig_plazo_minimo, ig_plazo_maximo, ic_producto_nafin, ");
		strSQLBit.append(" cs_recurso, ic_moneda, cs_tipo_plazo, ig_tipo_cartera, ig_tipo_pago ) " );
		strSQLBit.append(" values(SEQ_COMREL_BASE_OPERACION.nextval,"+cod_operac+","+no_epo);
		strSQLBit.append(" ,"+plz_min+","+plz_max+","+ic_producto+",'"+cod_operac_recurso+"',"+ ic_moneda);
		strSQLBit.append(" ,"+(ic_producto.equals("1")||(ic_producto.equals("5"))?"'"+tipo_plazo+"'":(ic_producto.equals("8"))?"'F'":"null")+","+tipoCartera);
		strSQLBit.append(", "+tipoPago );//F09-2015
		 strSQLBit.append(" )" );
		
		try{
			con.conexionDB();
			ps = con.queryPrecompilado(strSQLBit.toString());
			log.debug(" Sentencia::: "+strSQLBit);
			
			ps.executeUpdate();

			mensaje = "La base de Operaci�n fue actualizada";
			exito = true;
			if(ps != null)
				ps.close();
		}catch(Exception e){
			exito = false;
			log.error("Error al actualizar datos",e);
			mensaje = "Error:" + e;
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();				
			}
		}	
		return mensaje;		
	}// fin metodo


/**
 * Elimina el rengl�n seleccionado en la pantalla de Captura Cadenas .
 * @param operacion        Contiene la clave de la operaci�n a borrar.
 * @return boolean         Con el resultado que determina si se realiz� o no la acci�n.
 */

	public boolean eliminarCapturaCadenas(String operacion){
		//***********Validaci�n de parametros:*****************************************
		try {
			if (operacion == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************
		log.info("eliminarCapturaCadenas(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = false;
		PreparedStatement ps = null;
		String mensaje = null;
		StringBuffer strSQLBit = null;

		strSQLBit = new StringBuffer();

		strSQLBit.append( "delete comrel_base_operacion where ic_base_operacion= ? ");

		try{
		con.conexionDB();
		ps = con.queryPrecompilado(strSQLBit.toString());
		ps.setInt(1, Integer.parseInt(operacion));
		log.debug(" Sentencia::: "+strSQLBit);
		ps.executeUpdate();
		ps.clearParameters();
		exito = true;
		mensaje = "La base de Operacion fue <BR> actualizada";
		if(ps != null)
			ps.close();
		}catch(Exception e){
			log.error(e);
			exito = false;
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("eliminarCapturaCadenas(S)");
		}
		return exito;
	}

/**
 * Modifica el rengl�n seleccionado en la pantalla de Captura Cadenas .
 * @param plz_max_modif               Contiene el nuevo plazo m�ximo.
 * @param cod_operac_recurso_modif    Contiene el codigo de operacion a modificar.
 * @param ic_base_operacion           Contiene la clave de la base de operacion.
 * @return boolean                    Con el resultado que determina si se realiz� o no la acci�n.
 */

	public boolean modificarCapturaCadenas(String plz_max_modif, String cod_operac_recurso_modif, String ic_base_operacion){
		//***********Validaci�n de parametros:*****************************************
		try {
			if (plz_max_modif == null || cod_operac_recurso_modif == null || ic_base_operacion == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************
		log.info("modificarCapturaCadenas(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = false;
		PreparedStatement ps = null;
		StringBuffer strSQLBit = null;

		strSQLBit = new StringBuffer();

		strSQLBit.append( "UPDATE comrel_base_operacion ");
		strSQLBit.append(" SET ig_plazo_maximo = ?");
		strSQLBit.append(" ,cs_recurso = ? " );
		strSQLBit.append(" WHERE ic_base_operacion= ? ");

		try{
		con.conexionDB();
		ps = con.queryPrecompilado(strSQLBit.toString());
		ps.setInt(1, Integer.parseInt(plz_max_modif));
		ps.setString(2, cod_operac_recurso_modif);
		ps.setInt(3, Integer.parseInt(ic_base_operacion));

		log.debug(" Sentencia::: "+strSQLBit);

		ps.executeUpdate();
		ps.clearParameters();

		exito = true;
		if(ps != null)
			ps.close();
		}catch(Exception e){
			log.error(e);
			exito = false;
		}finally{
		if(con.hayConexionAbierta())
		{
			con.terminaTransaccion(exito);
			con.cierraConexionDB();
		}
		log.info("modificarCapturaCadenas(S)");
		}
		return exito;
	}

/**
 * Obtiene el piso del combo de cartera en las pantalla de bases de operaci�n .
 * @param no_if    Contiene la clave de la IF
 * @return String  Con el n�mero de piso.
 */

	public String consigueCarteraCredito (String no_if){
		//***********Validaci�n de parametros:*****************************************
		try {
			if (no_if == null ) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************
		log.info(" consigueCarteraCredito(E)");
		AccesoDB con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String tipoCartera = "";

		try {
			con = new AccesoDB();
			con.conexionDB();

			String sQuery =  "select ig_tipo_piso from comcat_if where ic_if = ? ";

			log.debug(sQuery);
			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, Integer.parseInt(no_if));

			rs = ps.executeQuery();
			if (rs.next())
				tipoCartera = rs.getString(1);
			if(rs != null)
				rs.close();
			if (ps != null)
				ps.close();

			return tipoCartera;

		}catch (Exception e) {
			log.error("Error en consigueCarteraCredito : "+e);
			throw new AppException("Error en consulta de piso para credito", e);
		} finally {
			log.info("consigueCarteraCredito (S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}


/**
 * Devuelve el plazo minimo para la pantalla de Cr�dito electr�nico-captura en bases de Operaci�n.
 * @param no_if         Contiene la clave de la IF.
 * @param tipoTasa      Contiene la clave de la tasa.
 * @param tipoCred      Contiene la clave del cr�dito.
 * @param tipoAmort     Contiene la clave de amortizaci�n.
 * @param tipoCartera   Contiene la clave de cartera.
 * @param tipo_plazo    Contiene la clave del tipo de plazo.
 * @return Map          Con el plazo m�nimo.
 */

	public Map consiguePlazoCredito (String no_if, String tipoTasa, String tipoCred,
													String tipoAmort, String tipoCartera, String tipo_plazo){

		//***********Validaci�n de parametros:*****************************************
		try {
			if (no_if == null || tipoTasa == null || tipoCred == null
				 || tipoAmort == null ||tipoCartera == null || tipo_plazo == null	) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************
		log.info("consiguePlazoCredito (E)");
		AccesoDB con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		HashMap datos = new HashMap();

		int plazos = 0;
      boolean varf=false, varc=false, vare=false;
		String condicion = "";
		String tablas = " , comcat_tasa t, comcat_tipo_credito tc ";
		String condcred =	" where bo.ic_tasa = t.ic_tasa and bo.ic_tipo_credito = tc.ic_tipo_credito "+
					" and t.cs_creditoelec = 'S' and tc.cs_creditoelec = 'S' ";

		if(!no_if.equals("")){
			condicion += "AND bo.ic_if = "+no_if;
		}
	if (!tipoTasa.equals("")){
		condicion += " AND bo.ic_tasa = "+tipoTasa;
	}
	if (!tipoCred.equals("")){
		condicion += " AND bo.ic_tipo_credito = "+tipoCred;
	}
	if (!tipoAmort.equals("")){
		condicion += " AND bo.ic_tabla_amort = "+tipoAmort;
	}
	if (!tipoCartera.equals("")){
		condicion += " AND bo.ig_tipo_cartera = "+tipoCartera;
	}
	if (!tipo_plazo.equals("")){
		condicion += " AND bo.cs_tipo_plazo = '"+tipo_plazo+"'";
	}
	condicion+= " order by 1 desc";

	String	qryIF=  " select distinct cs_tipo_plazo " +
				" from com_base_op_credito bo" + tablas + condcred + condicion;

	log.debug(qryIF);

	try{
		con = new AccesoDB();
		con.conexionDB();

		ps = con.queryPrecompilado(qryIF);

		rs = ps.executeQuery();

		while (rs.next()){
			tipo_plazo=rs.getString(1);
			if (tipo_plazo.equals("F")){
				varf=true;
			}else if(tipo_plazo.equals("C")){
				varc=true;
			} else {
				vare=true;
			}
				plazos++;
		}

		datos.put("PLAZOS", ""+plazos);
		datos.put("VARF",""+varf);
		datos.put("VARC",""+varc);
		datos.put("VARE",""+vare);
		datos.put("TIPO",""+tipo_plazo);

		if(rs != null)
			rs.close();

		if (ps != null)
			ps.close();

	}catch (Exception e) {
			log.error("Error en consiguePlazoCredito : "+e);
			throw new AppException("Error en consulta de piso para credito", e);
	} finally {
		log.info("consiguePlazoCredito (S)");
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	}
	return datos;
}



/**
 * Elimina el rengl�n seleccionado en la pantalla de Captura Cr�dito Electr�nico .
 * @param operacion        Contiene la clave de la operaci�n a borrar.
 * @return boolean         Con el resultado que determina si se realiz� o no la acci�n.
 */

	public boolean eliminarCapturaCredito(String operacion){
//***********Validaci�n de parametros:*****************************************
		try {
			if (operacion == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
//********************************************************************************

		log.info("eliminarCapturaCadenas(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = false;
		PreparedStatement ps = null;
		StringBuffer strSQLBit = null;

		strSQLBit = new StringBuffer();

		strSQLBit.append("delete com_base_op_credito where ic_base_op_credito = ? ");

		try{
			con.conexionDB();
			ps = con.queryPrecompilado(strSQLBit.toString());
			ps.setInt(1, Integer.parseInt(operacion));
			log.debug(" Sentencia::: "+strSQLBit);

			ps.executeUpdate();

			ps.clearParameters();

			exito = true;
			if(ps != null)
				ps.close();
		}catch(Exception e){
			log.error(e);
			exito = false;
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		return exito;
	}


/**
 * Modifica el rengl�n seleccionado en la pantalla de Captura Cr�dito Electr�nico .
 * @param plzMaxMod               Contiene el nuevo plazo m�ximo.
 * @param codOpeMod    				 Contiene el codigo de operacion a modificar.
 * @param icBaseOperCred          Contiene la clave de la base de operacion.
 * @param cs_alta_automatica      Contiene la clave de la base de operacion.
 * @param cs_base_op_dinamica     Contiene la clave de la base de operacion.
 * @return boolean                Con el resultado que determina si se realiz� o no la acci�n.
 */
	public boolean modificarCapturaCredito(String plzMaxMod, String codOpeMod, String icBaseOpeCred,
														String cs_alta_automatica, String cs_base_op_dinamica){

		//***********Validaci�n de parametros:*****************************************
		try {
			if (plzMaxMod == null || codOpeMod == null || icBaseOpeCred == null
			|| cs_alta_automatica == null || cs_base_op_dinamica == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************
		log.info("modificarCapturaCredito(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = false;
		PreparedStatement ps = null;
		StringBuffer strSQLBit = null;

		strSQLBit = new StringBuffer();

		strSQLBit.append( "update com_base_op_credito set ig_plazo_maximo = ? "+
				" ,ig_codigo_base = "+(codOpeMod.equals("")?"null":codOpeMod)+
				" ,cs_alta_automatica_garantia = ? " +
				" ,cs_base_op_dinamica   = ? "  +
				//" ,cs_tipo_renta = '" + request.getParameter("cs_tipo_renta") + "'" +
				" where ic_base_op_credito = ? ");

		try{
			con.conexionDB();
			ps = con.queryPrecompilado(strSQLBit.toString());
			ps.setInt(1, Integer.parseInt(plzMaxMod));
			ps.setString(2,cs_alta_automatica);
			ps.setString(3,cs_base_op_dinamica);
			ps.setInt(4, Integer.parseInt(icBaseOpeCred));
			log.debug(" Sentencia::: "+strSQLBit);

			ps.executeUpdate();
			ps.clearParameters();
			exito = true;

			if(ps != null)
				ps.close();
		}catch(Exception e){
			log.error(e);
			exito = false;
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("modificarCapturaCredito(E)");
		}
		return exito;
	}



/**
 * Inserta nuevo valores en la pantalla de Cr�dito electr�nico-captura en bases de Operaci�n.
 * @param noif                    Contiene la clave de la IF.
 * @param tipoTasa                Contiene la clave de la tasa.
 * @param codOpe               	 Contiene la clave del c�digo de operaci�n.
 * @param tipoCred                Contiene la clave del cr�dito.
 * @param tipoAmort               Contiene la clave de amortizaci�n.
 * @param noEmisor                Contiene la clave del emisor.
 * @param plzMin                  Contiene el plazo m�nimo.
 * @param tipo_plazo              Contiene la clave del tipo de plazo.
 * @param plzMax                  Contiene el nuevo plazo m�ximo.
 * @param tipoRenta    				 Contiene la clave del tipo de renta.
 * @param tipoInteres    			 Contiene la clave del tipo de interes.
 * @param baseOperacionDinamica   Contiene la clave de la base de operacion din�mica.
 * @param altaAutomaticaGarantia  Contiene la clave de la base de operacion.
 * @param codProdBanc             Contiene la clave de la base de operacion.
 * @param periodicidad            Contiene la clave del combo de periodicidad.
 * @return boolean                Con el resultado que determina si se realiz� o no la acci�n.
 */

	public boolean aceptarCapturaCredito(String noIf, String tipoTasa, String codOpe, String tipoCred,
											 String tipoAmort, String noEmisor, String tipoCartera, String plzMin,
											 String plzMax, String tipo_plazo, String altaAutomaticaGarantia,
											 String codProdBanc, String periodicidad, String tipoInteres,
											 String tipoRenta, String baseOperacionDinamica){
		String tipoOperacion = "credito";
		log.info("aceptarCapturaCredito(E)");
		//***********Validaci�n de parametros:*****************************************
		try {
			if (noIf == null || tipoTasa == null || codOpe == null
				 || tipo_plazo == null || tipoAmort == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************

		AccesoDB con = new AccesoDB();
		boolean exito = false;
		PreparedStatement ps = null;
		PreparedStatement psAux = null;
		StringBuffer strSQLBit = null;
		ResultSet rs = null;

		strSQLBit = new StringBuffer();

		try{
		con.conexionDB();
		int icBaseOpCred = 0;
		String query = "select (NVL(max(ic_base_op_credito),0)+1) from com_base_op_credito";
		psAux = con.queryPrecompilado(query);
		rs = psAux.executeQuery();

		if(rs.next())
			icBaseOpCred = (rs.getInt(1)==0)?1:rs.getInt(1);

		strSQLBit.append( "insert into com_base_op_credito (ic_base_op_credito, ic_if, ic_tasa, "+
				"ig_codigo_base, ic_tipo_credito, ic_tabla_amort, ic_emisor, "+
				"ig_plazo_minimo, ig_plazo_maximo, cs_tipo_plazo, ig_tipo_cartera, cs_alta_automatica_garantia, "+
				"ig_producto_banco, ic_periodicidad, cg_tipo_interes "+
				((tipoOperacion.equals("equipamiento"))?", ic_producto_nafin ":"")+
				((!"".equals(tipoRenta))?", cs_tipo_renta ":"")+//MODFICACION F020-2011 FVR
				" , cs_base_op_dinamica " + // F014-2012. BASE DE OPERACION DINAMICA. BY JSHD
				" ) "+
				"values ("+icBaseOpCred+","+noIf+","+tipoTasa+","+(codOpe.equals("")?"null":codOpe)+","+
				tipoCred+","+tipoAmort+","+(noEmisor.equals("")?"null":noEmisor)+","+plzMin+","+
				plzMax+",'"+tipo_plazo+"',"+tipoCartera+",'" + altaAutomaticaGarantia +"'" +
				(codProdBanc.equals("")?", null":","+codProdBanc) +//se agrega campo para bases de operacion F004-2009 FVR
				(periodicidad.equals("")?", null":", "+periodicidad) +//se agrega campo para bases de operacion F004-2009 FVR
				(tipoInteres.equals("")?", null":", '"+tipoInteres+"'") +//se agrega campo para bases de operacion F004-2009 FVR
				((tipoOperacion.equals("equipamiento"))?" ,7 ":"")+
				((!"".equals(tipoRenta))?",'"+tipoRenta+"'":"")+//MODFICACION F020-2011 FVR
				",'"+baseOperacionDinamica+"'"+  // F014-2012. BASE DE OPERACION DINAMICA. BY JSHD
				")" );


		ps = con.queryPrecompilado(strSQLBit.toString());
		log.debug(" Sentencia::: "+strSQLBit);
		ps.executeUpdate();
		exito = true;
		if(ps != null)
			ps.close();

		if(psAux != null)
			psAux.close();

		if(rs != null)
			rs.close();

		}catch(Exception e){
			log.error("Error al actualizar datos",e);
			exito = false;
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
			log.info("aceptarCapturaCredito(S)");
		}
		return exito;
	}// fin metodo



/**
 * Devuelve el plazo minimo para la pantalla de Cr�dito electr�nico-captura en bases de Operaci�n.
 * @param noif                    Contiene la clave de la IF.
 * @param tipoTasa                Contiene la clave de la tasa.
 * @param tipoCred                Contiene la clave del cr�dito.
 * @param tipoAmort               Contiene la clave de amortizaci�n.
 * @param noEmisor                Contiene la clave del emisor.
 * @param plzMin                  Contiene el plazo m�nimo.
 * @param tipo_plazo              Contiene la clave del tipo de plazo.
 * @param plzMax                  Contiene el nuevo plazo m�ximo.
 * @param tipoRenta    				 Contiene la clave del tipo de renta..
 * @param periodicidad            Contiene la clave del combo de periodicidad.
 * @return Map                    Con el resultado del plazo m�nimo y operaci�n de captura.
 */

	public Map getPlazoyOperacionCapturaCredito (String noIf, String tipoTasa, String tipoCred,
											 String tipoAmort, String tipoCartera, String tipo_plazo,
											 String noEmisor, String periodicidad, String tipoRenta ){

		String tipoOperacion = "credito";
		//***********Validaci�n de parametros:*****************************************
		try {
			if (noIf == null || tipoTasa == null || tipoCred == null
				 || tipo_plazo == null || tipoAmort == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************
		log.info("getPlazoyOperacionCapturaCredito(E)");
		AccesoDB con = null;
		PreparedStatement ps = null;
		PreparedStatement ps2 = null;
		ResultSet rs = null;
		ResultSet rs2 = null;

		int plazo = 0;

		HashMap datos = new HashMap();

		try {
			con = new AccesoDB();
			con.conexionDB();

			String sQuery =  "select ig_plazo_maximo from com_base_op_credito  "+
					"where ic_if = ?" +
					" and ic_tasa  = ?" +
					" and ic_tipo_credito = ?" +
					" and ic_tabla_amort = ?" +
					" and ig_tipo_cartera = ?" +
					" and cs_tipo_plazo = ?" +
					(noEmisor.equals("")?" and ic_emisor is null":" and ic_emisor ="+noEmisor)+
					(periodicidad.equals("")?" and ic_periodicidad is null":" and ic_periodicidad ="+periodicidad)+//se agrega condicion por F004-2010 FVR
					(tipoRenta.equals("")?" and cs_tipo_renta is null":" and cs_tipo_renta = '"+tipoRenta+"'")+//se agrega condicion por F020-2011 FVR
					((tipoOperacion.equals("equipamiento"))?" and ic_producto_nafin = 7 ":"")+

				//" and ig_codigo_base ="+codOpe+
				" order by ig_plazo_maximo desc";

			log.debug(sQuery);
			ps = con.queryPrecompilado(sQuery);
			ps.setInt(1, Integer.parseInt(noIf));
			ps.setInt(2, Integer.parseInt(tipoTasa));
			ps.setInt(3, Integer.parseInt(tipoCred));
			ps.setInt(4, Integer.parseInt(tipoAmort));
			ps.setInt(5, Integer.parseInt(tipoCartera));
			ps.setString(6, tipo_plazo);

			rs = ps.executeQuery();

			if (rs.next())
			plazo = rs.getInt(1)+1;
		else
			plazo = 1;


			datos.put("PLAZO", ""+plazo);
			datos.put("ULTIMO",""+(plazo - 1));


			if( plazo > 1 ){
					String query2 =  "select ig_plazo_minimo, cs_base_op_dinamica as dinamica from com_base_op_credito  "+
					"where ic_if = ?" +
					" and ic_tasa  = ?" +
					" and ic_tipo_credito = ?" +
					" and ic_tabla_amort = ?" +
					" and ig_tipo_cartera = ?" +
					" and cs_tipo_plazo = ?" +
					(noEmisor.equals("")?" and ic_emisor is null":" and ic_emisor ="+noEmisor)+
					(periodicidad.equals("")?" and ic_periodicidad is null":" and ic_periodicidad ="+periodicidad)+//se agrega condicion por F004-2010 FVR
					(tipoRenta.equals("")?" and cs_tipo_renta is null":" and cs_tipo_renta = '"+tipoRenta+"'")+//se agrega condicion por F020-2011 FVR
					((tipoOperacion.equals("equipamiento"))?" and ic_producto_nafin = 7 ":"");

				log.debug(query2);
				ps2 = con.queryPrecompilado(query2);
				ps2.setInt(1, Integer.parseInt(noIf));
				ps2.setInt(2, Integer.parseInt(tipoTasa));
				ps2.setInt(3, Integer.parseInt(tipoCred));
				ps2.setInt(4, Integer.parseInt(tipoAmort));
				ps2.setInt(5, Integer.parseInt(tipoCartera));
				ps2.setString(6, tipo_plazo);

				rs2 = ps2.executeQuery();

				datos.put("BASEOPERACIONDINAMICA", "");

				if (rs2.next()){
					datos.put("BASEOPERACIONDINAMICA",rs2.getString("dinamica") );
				} else {
					datos.put("BASEOPERACIONDINAMICA","N");
				}
			}

			if(rs != null)
				rs.close();

			if(rs2 != null)
				rs2.close();

			if (ps != null)
				ps.close();

			if (ps2 != null)
				ps2.close();


			return datos;

		}catch (Exception e) {
			log.error("Error en getPlazoyOperacionCapturaCredito: ",e);
			throw new AppException("Error en consulta de plazo captura cr�dito", e);
		} finally {
			log.info("getPlazoyOperacionCapturaCredito(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}

/**
 * Obtiene los valores los campos en la pantalla de Cr�dito electr�nico-captura en bases de Operaci�n.
 * @param noif                    Contiene la clave de la IF.
 * @param tipoTasa                Contiene la clave de la tasa.
 * @param tipoCred                Contiene la clave del cr�dito.
 * @param tipoAmort               Contiene la clave de amortizaci�n.
 * @param noEmisor                Contiene la clave del emisor.
 * @param plzMin                  Contiene el plazo m�nimo.
 * @param tipo_plazo              Contiene la clave del tipo de plazo.
 * @param plzMax                  Contiene el nuevo plazo m�ximo.
 * @param tipoRenta    				 Contiene la clave del tipo de renta..
 * @param periodicidad            Contiene la clave del combo de periodicidad.
 * @return List                   Con el resultado de la consulta.
 */

	public List getCamposCapturaCredito(String noIf, String tipoTasa, String tipoCred,
											 String tipoAmort, String tipoCartera, String tipo_plazo,
											 String noEmisor, String periodicidad, String tipoRenta){

		log.info("getCamposCapturaCredito(E)");
		//***********Validaci�n de parametros:*****************************************
		try {
			if (noIf == null || tipoTasa == null || tipoCred == null
				 || tipo_plazo == null || tipoAmort == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
		} catch(Exception e) {
			log.error("Error en los parametros recibidos." );
			throw new AppException("Error en los parametros recibidos " + e.getMessage() );
		}
		//********************************************************************************

		String tipoOperacion = "credito";
		AccesoDB con = null;
		PreparedStatement ps = null;
		List registros = new ArrayList();

		String strSQL = null;

		strSQL =
		"select I.cg_razon_social as nomIF, T.cd_nombre as nomTasa, "+
						"TC.cd_descripcion as nomTipCred, TA.cd_descripcion as nomAmortiza, "+
						"BC.ig_plazo_minimo as plzMin, BC.ig_plazo_maximo as plzMax, "+
						"BC.ic_base_op_credito as icBaseOpeCred, BC.cs_tipo_plazo as plazo, BC.ig_tipo_cartera as cartera, "+
						"BO.cg_descripcion as nomBaseOper, BO.ig_codigo_base as CodigoBase "+
						(noEmisor.equals("")?", '' as nomEmisor ":", E.cd_descripcion as nomEmisor ")+
						" , BC.cs_alta_automatica_garantia as alta, BC.ig_producto_banco as producto_banco, "+
						" BC.ic_periodicidad as periodicidad, BC.cg_tipo_interes as tipo_interes "+//se agragn campos nuevos F004-2009 FVR
						(periodicidad.equals("")?", '' as nomPeriodicidad ":", CP.cd_descripcion as nomPeriodicidad ")+
						" , BC.cs_tipo_renta as tipo_renta "+//MODIFICACION F020-2011 FVR
						" , bc.cs_base_op_dinamica as basedinamica " + // F014 - 2012. BASES DE OPERACION DINAMICAS. BY JSHD
						//" , '/nafin/15basesoperce.jsp' as pantalla "+
						" from com_base_op_credito BC, comcat_if I, comcat_tasa T, "+
						" comcat_base_operacion BO, comcat_tipo_credito TC, comcat_tabla_amort TA "+
						(noEmisor.equals("")?"":", comcat_emisor E ")+
						(periodicidad.equals("")?"":", comcat_periodicidad CP ")+//SE AGREGA PRO F004-2010 FVR
						" where BC.ic_if = I.ic_if "+
						" and BC.ic_tasa = T.ic_tasa "+
						" and BC.ig_codigo_base = BO.ig_codigo_base(+) "+
						" and BC.ic_tipo_credito = TC.ic_tipo_credito "+
						" and BC.ic_tabla_amort = TA.ic_tabla_amort "+
						(noEmisor.equals("")?" and BC.ic_emisor is null ":" and BC.ic_emisor = E.ic_emisor ")+
						(periodicidad.equals("")?" and BC.ic_periodicidad is null ":" and BC.ic_periodicidad = CP.ic_periodicidad ")+//SE AGREGA PRO F004-2010 FVR
						" and BC.ic_if = ? "+
						" and BC.ic_tasa = ?"+
						" and BC.ic_tipo_credito = ?"+
						" and BC.ic_tabla_amort = ?"+
						" and BC.ig_tipo_cartera = ?"+
						" and BC.cs_tipo_plazo = ?"+
						//" AND BC.cs_alta_automatica_garantia = '" + altaAutomaticaGarantia + "' " +
						((tipoOperacion.equals("credito"))?" and T.cs_creditoelec = 'S' and TC.cs_creditoelec = 'S' ":"")+
						((tipoOperacion.equals("equipamiento"))?" and BC.ic_producto_nafin = 7 ":"")+
						(noEmisor.equals("")?" and BC.ic_emisor is null":" and BC.ic_emisor ="+noEmisor)+
						(periodicidad.equals("")?" and BC.ic_periodicidad is null ":" and BC.ic_periodicidad = "+periodicidad)+//SE AGREGA PRO F004-2010 FVR
						" order by BC.ic_base_op_credito";

		try{
			con = new AccesoDB();
			con.conexionDB();

			log.debug(strSQL);
			int i = 0;
			ps = con.queryPrecompilado(strSQL);


			ps.setInt(1, Integer.parseInt(noIf));
			ps.setInt(2, Integer.parseInt(tipoTasa));
			ps.setInt(3, Integer.parseInt(tipoCred));
			ps.setInt(4, Integer.parseInt(tipoAmort));
			ps.setInt(5, Integer.parseInt(tipoCartera));
			ps.setString(6, tipo_plazo);

			ResultSet rs = ps.executeQuery();
			HashMap hmDatos = null;

			while(rs.next()) {
				i++;
				hmDatos = new HashMap();
				hmDatos.put("NOMIF",rs.getString("nomIF"));
				hmDatos.put("NOMTASA",rs.getString("nomTasa"));
				hmDatos.put("NOMTIPCRED",rs.getString("nomTipCred"));
				hmDatos.put("NOMAMORTIZA",rs.getString("nomAmortiza"));
				hmDatos.put("PLZMIN",rs.getString("plzMin"));
				hmDatos.put("PLZMAX",rs.getString("plzMax"));
				hmDatos.put("ICBASEOPECRED",rs.getString("icBaseOpeCred"));
				hmDatos.put("PLAZO",rs.getString("plazo"));
				hmDatos.put("CARTERA",rs.getString("cartera"));
				hmDatos.put("NOMBASEOPER",rs.getString("nomBaseOper"));
				hmDatos.put("CODIGOBASE",rs.getString("CodigoBase"));
				hmDatos.put("NOMEMISOR",rs.getString("nomEmisor"));
				hmDatos.put("ALTA",rs.getString("alta"));
				hmDatos.put("PRODUCTOBANCO",rs.getString("producto_banco"));
				hmDatos.put("PERIODICIDAD",rs.getString("periodicidad"));
				hmDatos.put("TIPOINTERES",rs.getString("tipo_interes"));
				hmDatos.put("NOMPERIODICIDAD",rs.getString("nomPeriodicidad"));
				hmDatos.put("TIPORENTA",rs.getString("tipo_renta"));
				hmDatos.put("BASEDINAMICA",rs.getString("basedinamica"));
				hmDatos.put("TAM",""+i);

				registros.add(hmDatos);
			}

			rs.close();
			if(ps != null)
				ps.close();
			con.cierraStatement();

		}catch(Exception e){
				log.error("Error en getCamposCapturaCredito: ",e);
				throw new AppException("Error en consulta para obtener los campos de captura cr�dito ", e);
		} finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getCamposCapturaCredito(S)");
		}
		return registros;
	}

	/**
	 *
	 * Devuelve <tt>true</tt> si la bandera para verificar la codificaci�n de archivos
	 * cargados est� habilitada y <tt>false</tt> en caso contrario.
	 * <br>
	 * Se usa en pantallas espec�ficas del m�dulo de Descuento Electr�nico
	 * y de Distribuidores.
	 * <br>
	 * Para realizar la validaci�n se puede usar la siguiente clase: 
	 *   <tt>netropology.utilerias.codificacion.CodificacionArchivo</tt>
	 * <br>
	 *
	 * @throws AppException
	 *
	 * @return <tt>true</tt> para solicitar que se valide la codificaci�n del archivo cargado y 
	 *         <tt>false</tt> en caso contrario.
	 *
	 * @author jshernandez
	 * @since  F015 - 2015 -- DESC ELECT carga de doctos con caracteres especiales; 25/06/2015 09:01:30 p.m.
	 *
	 */
	public boolean validaCodificacionArchivoHabilitado()
		throws AppException {

		log.info("validaCodificacionArchivoHabilitado(E)");

		AccesoDB 				con 							= new AccesoDB();
		StringBuffer			query							= new StringBuffer();
		PreparedStatement		ps								= null;
		ResultSet				rs								= null;
		
		boolean					validar						= false;
 
		try{
			
			// Conectarse a la Base de Datos
			con.conexionDB();
 
			query.append( 
				"SELECT                   "  +
				"  DECODE( CS_VAL_CODIFICACION_ARCH, 'S', 'true', 'false') AS VALIDAR "  +
				"FROM                     "  +
				"	COM_PARAM_GRAL         "  +
				"WHERE                    "  +
				"	IC_PARAM_GRAL = 1      "
			);

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			
			if( rs.next() ){
				validar = "true".equals(rs.getString("VALIDAR"))?true:false;
			} 
			
		} catch(Exception e){
			
			log.error("validaCodificacionArchivoHabilitado(Exception)");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al revisar si se validar� la codificaci�n del(los) archivo(s). " + e);
			
		} finally {
			
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
			log.info("validaCodificacionArchivoHabilitado(S)");
			
		}
		
		return validar;
			
	}
	/**
		 * Metodo que verifica si existe la calificacion en el catalogo de calificaciones 
		 * @return 
		 * @param ic_calificacion es el id de la calificacion
		 */
		public boolean  getExisteClasificacion(String ic_calificacion  ){
			log.info("getExisteClasificacion (E)");
			AccesoDB       con            = new AccesoDB();
			StringBuffer   qrySentencia   = new StringBuffer();
			List           lVarBind       = new ArrayList();
			ResultSet         rs    =  null;
			PreparedStatement ps    =  null;
			boolean ban =true;
			String total = "";
			
			try   {
				
				con.conexionDB(); 
				
				qrySentencia = new StringBuffer();
				qrySentencia.append(" select * from COMCAT_CALIFICACION "+
										  " where IC_CALIFICACION = ? "); 

				lVarBind    = new ArrayList();
				lVarBind.add(ic_calificacion);         
						
				 
				log.debug("qrySentencia.toString() "+qrySentencia.toString());
				log.debug("varBind "+lVarBind);
				
				ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
				rs = ps.executeQuery();
				if(rs.next()){
					total = (rs.getString(1)==null)?"":rs.getString(1);   
					ban = true;
				}else{
					ban = false;
				}
				rs.close();
				ps.close();  
				

			} catch(Exception e) {     
				ban = false;
				log.info("getExisteClasificacion Exception "+e);
				throw new AppException("Error "+e);
			} finally {
				if(rs != null) { try { rs.close();}catch(Exception e){} }
				if(ps != null) { try { ps.close();}catch(Exception e){} }
				if(con.hayConexionAbierta())
					con.cierraConexionDB();
				log.info("getExisteClasificacion (S) ");
			}
			return ban;
		}

	 
	/**
	 * Metodo para verificar si ya existe parametrizada una base de operaciones
	 * con meses sin intereses
	 * @return 
	 * @param ic_epo
	 */
	public String  getvalidaMesesSinIntereses(String ic_epo  ){
		log.info("getvalidaMesesSinIntereses (E)");
		AccesoDB       con            = new AccesoDB();
		StringBuffer   qrySentencia   = new StringBuffer();
		List           lVarBind       = new ArrayList();
		ResultSet         rs    =  null;
		PreparedStatement ps    =  null;
		String total ="0";
		
		try   {
			
			con.conexionDB(); 
			
			qrySentencia = new StringBuffer();
			qrySentencia.append(" select count(*) as total   from comrel_base_operacion "+
									  " where  ic_epo = ? "+
									  " and ig_tipo_pago = ? "+
									  " and IC_PRODUCTO_NAFIN = ? "+
									  " and ic_moneda = ? "+
									  " and IG_TIPO_CARTERA = ?  "); 

			lVarBind    = new ArrayList();
			lVarBind.add(ic_epo);         
			lVarBind.add("2");         
			lVarBind.add("4");                  
			lVarBind.add("1");                  
			lVarBind.add("2");                  
			 
			log.debug("qrySentencia.toString() "+qrySentencia.toString());
			log.debug("varBind "+lVarBind);
			
			ps = con.queryPrecompilado(qrySentencia.toString(), lVarBind);
			rs = ps.executeQuery();
			if(rs.next()){
				total = (rs.getString(1)==null)?"":rs.getString(1);   
			}         
			rs.close();
			ps.close();  
			

		} catch(Exception e) {        
			log.info("getvalidaMesesSinIntereses Exception "+e);
			e.printStackTrace();
			throw new AppException("Error "+e);
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getvalidaMesesSinIntereses (S) ");
		}
		return total;
	}
	/**
	 * M�todo que nos ayuda a guardar la informacion capturada en los grid�'s
	 * @param IG_PARAM_CAMPO 
	 * @param CC_PRODUCTO
	 * @param IC_PARAMETRO
	 * @param CC_API
	 * @param IC_EPO
	 */
	 public boolean guardaParamInterfazDescuento(String IC_EPO,String CC_API,String IC_PARAMETRO,String CC_PRODUCTO,String IG_PARAM_CAMPO){
		AccesoDB		con 			=  new AccesoDB();;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List varBind = new ArrayList();
		String querySQL;
		boolean			exito		= true;
		String EXISTE_INFO = "";
		try {
			con.conexionDB();
			
			querySQL ="	SELECT DECODE(COUNT(*),0,'N','S' ) AS EXISTE_INFO " +
						 "	FROM COM_PARAM_INTERFASE_DESC	" +
						 "	WHERE IC_EPO = ? "+
						 "	AND   CC_API =?	"+
						 "	AND   IC_PARAMETRO =? "+
						 "	AND   CC_PRODUCTO =? ";
			varBind = new ArrayList();
			varBind.add(IC_EPO);
			varBind.add(CC_API);
			varBind.add(IC_PARAMETRO);
			varBind.add(CC_PRODUCTO);
						
			log.debug("querySQL -- "+querySQL);
			log.debug("varBind --  "+varBind);
			ps = con.queryPrecompilado(querySQL, varBind);
			rs = ps.executeQuery();
					
			if (rs.next()) {
				EXISTE_INFO = rs.getString("EXISTE_INFO") == null ? "" :rs.getString("EXISTE_INFO");
			}
			rs.close();
			ps.close();
			if (EXISTE_INFO.equals("S")) {
				querySQL = " UPDATE COM_PARAM_INTERFASE_DESC " +
						     " SET IG_PARAM_CAMPO = ? "+
							   "	WHERE IC_EPO = ? "+
								 "	AND   CC_API =?	"+
								 "	AND   IC_PARAMETRO =? "+
								 "	AND   CC_PRODUCTO =? ";
				varBind = new ArrayList();
				varBind.add(IG_PARAM_CAMPO);
				varBind.add(IC_EPO);
				varBind.add(CC_API);
				varBind.add(IC_PARAMETRO);
				varBind.add(CC_PRODUCTO);
				log.debug("querySQL -- "+querySQL);
				log.debug("varBind --  "+varBind);		   
				ps = con.queryPrecompilado(querySQL, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();
			}else{
				querySQL = "  INSERT INTO COM_PARAM_INTERFASE_DESC( IC_EPO,CC_API, IC_PARAMETRO , CC_PRODUCTO,IG_PARAM_CAMPO  ) " +
						     "  VALUES(?,?,?,?,?)";
				varBind = new ArrayList();
				varBind.add(IC_EPO);
				varBind.add(CC_API);
				varBind.add(IC_PARAMETRO);
				varBind.add(CC_PRODUCTO);
				varBind.add(IG_PARAM_CAMPO);
				log.debug("querySQL -- "+querySQL);
				log.debug("varBind --  "+varBind);		   
				ps = con.queryPrecompilado(querySQL, varBind);
				ps.executeUpdate();
				ps.clearParameters();
				ps.close();
			
			}
			
			
		}catch(Exception e){
			exito = false;
			throw new AppException("guardaParamInterfazDescuento(Error): Error al guardar la informaci�n", e);
		}finally{
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			   
			} catch (Exception t) {
				log.error("guardaParamInterfazDescuento():: Error al cerrar Recursos: " + t.getMessage());
			}
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
		return exito;
	 }
	
}// Fin de la Clase

