/*************************************************************************************
*
* Nombre de Clase o de Archivo: ParametrosGrales.java
*
* Versi�n: 		  1.0
*
* Fecha Creaci�n: Enero 2mil3
*
* Autor:          Ricardo Arzate Ramirez
*
*************************************************************************************/
/**************************************************************************
 *   
 * Modificado por: Cindy A. Ramos P�rez
 *
 * Fecha Modificaci�n: 19/03/2003
 *
 *  Agregar el par�metro de Base de C�lculo
 *
***************************************************************************/

package com.netro.parametrosgrales;

import com.netro.exception.NafinException;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.ejb.Remote;

import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
//import netropology.utilerias.Acuse;

@Remote
public interface ParametrosGrales {
	public void ovactualizarProducto( String 	strCsAutomaticoSusceptible,/* String strCgTipoCobranza,
										String 	intInDiasProcesoCobro,*/		String intIcPeriodoMaxDisp,
										String intIcPeriodoMaxVig,			String intIcNumeroMaxAmort,
										/*String strIgClausulaVencimiento, 	String strIgTipoCalculoInteres,
										String intIcPeriodoCreditoMora,	*/	String strCgRevisionTasaMora,
                                        /*String esBaseCalculo,*/				String cs_troya,
                                        String ig_numero_max_disp,			String ig_numero_min_amort,
                                        String strCsDetalle, 				String lsdias_lib_linea, 
                                        String lsnumeroamortizacion_exp,	//String lsperiododisposicion_exp,
                                        String lsperiodovigencia_exp,		String lsTipoPagoCapital,
                                        String lsTipoPagoCapital_exp,		String txtNumMaxDisp_exp,
                                        String lsdias_lib_linea_e, String lsnumeroamortizacion_emp,
                                        String lsnumeroamortizacion_e_emp, String lsTipoPagoCapital_emp,
                                        String lsTipoPagoCapital_e_emp, String txtNumMaxDisp_emp,
                                        String txtNumMaxDisp_e_emp) throws  NafinException;
										
								 
	public void ovinsertarProducto( 	String 	strCsAutomaticoSusceptible, /*String strCgTipoCobranza,
										String intInDiasProcesoCobro,*/		String intIcPeriodoMaxDisp,
										String intIcPeriodoMaxVig,			String intIcNumeroMaxAmort,
										/*String strIgClausulaVencimiento, 	String strIgTipoCalculoInteres,
										String intIcPeriodoCreditoMora,	*/	String strCgRevisionTasaMora,
                                        /*String esBaseCalculo,*/				String cs_troya,
                                        String ig_numero_max_disp,			String ig_numero_min_amort,
                                        String strCsDetalle, 				String lsdias_lib_linea, 
                                        String lsnumeroamortizacion_exp,	//String lsperiododisposicion_exp,
                                        String lsperiodovigencia_exp,		String lsTipoPagoCapital,
                                        String lsTipoPagoCapital_exp,		String txtNumMaxDisp_exp,
                                        String lsdias_lib_linea_e, String lsnumeroamortizacion_emp,
                                        String lsnumeroamortizacion_e_emp, String lsTipoPagoCapital_emp,
                                        String lsTipoPagoCapital_e_emp, String txtNumMaxDisp_emp,
                                        String txtNumMaxDisp_e_emp) throws  NafinException;

	public void ovactualizarMoneda(int 	intIcProductoNafin, 	String strMoneda,
										String 	strFnMontoMaximo,		String strFnMontoMinimo,
										/*int intIcTasa,*/				String strCgRelMat,
										String strFnPuntos) throws  NafinException;
										
	public void ovinsertarMoneda(int 	intIcProductoNafin, 	String strMoneda,
										String 	strFnMontoMaximo,		String strFnMontoMinimo,
										/*int intIcTasa,*/ String strCgRelMat,
										String strFnPuntos) throws  NafinException;
 	
	/*>>>>>>>>>>>>>>>>Israel Herrera*/
 	public String getAutoSuscep(String ic_epo) 
 		throws  NafinException;
	public String getAutoSuscepPedidos(String ic_epo)  	
		throws  NafinException;	
	public void setAutoSuscep(String ic_producto_nafin, String ic_epo, String cs_automatico_susceptible)
		throws  NafinException;
		
	public Vector getParamGrales(String ic_producto_nafin,String ic_moneda)
		throws  NafinException;	
		
	public Vector getParamGralesCredExp()
		throws  NafinException;	
			
	public Vector getParamxIf(String ic_producto_nafin,String ic_if,String ic_moneda) 		
		throws  NafinException;
	public void setParamxIf(String ic_producto_nafin,String ic_if,String ic_moneda,
							  String base_calculo,String tipo_cobranza,String periodo_max_disp,
							  String periodo_max_vig,String numero_max_amort,String clausula_vencimiento,
							  String tipo_calculo_interes,String compartir_linea,String periodo_credito_mora,
							  String tipo_mora,String ic_tasa,String rel_mat,String puntos,
							  String revision_tasa_mora, String ig_numero_max_disp)
		throws  NafinException;			
	public void setCobranzaAutomatica(String strIF, String strEPO, 
			String[] arrClavesPyme) throws NafinException;

	public Vector getComboPlazos()
		throws NafinException;
		
	public List getEposProyectoPEF(String ic_epo, String no_epo, String ne_epo, String nombreEpo, String ic_secretaria, String ic_organismo)
		throws NafinException;
		
	public void ovactualizarEpos(List ldatos) throws NafinException;
	
	/*consulta para obtener historico de % de obra publica*/
	public List consHistEpoPefObraPub(String ic_epo, String anio) throws NafinException;
	
	public List consAnioParamEpoPef() throws NafinException;
	
	public List consEpoPefReporte(String anio, String mes) throws NafinException;
	
	public HashMap getPermisoConsultaReportesEPO() throws  AppException;
	
	/*Migraci�n EPO Administraci�n->Parametrizaci�n->Otros Par�metros*/
	public boolean actualizaOtrosParametros(String porcentajeM, String porcentajeD, String operaFactVenc, String operaFactDist, String diasNotif, String diasNotifReact, String claveEpo);
	/*MIgraci�n EPO Administraci�n-ZParametrizaci�n->Documento por Producto*/
	public int actualizaDocumentoXProducto(String claveEpo, String producto, String documento);
	
	public void actualizaClasificacionEpo(String ic_epo,String ic_calif, List listCalif);
	public String getConteoClasificacion(String ic_epo,String epo_pyme);
	public void actualizaClasificacionMasivaEpo(String ic_epo,String ic_calif, String epo_pyme);
	public Registros getCamposAdicionalesEpo(String ic_epo, String ic_producto);
	public Registros getCamposAdicionalesDetalleEpo(String ic_epo, String ic_producto);
	public void actualizaCamposAdicionales(String ic_epo,Hashtable datos); 
	public String getParametrizacionFondeo(String iNoCliente);
	
	public Map getEncabezadosMatrizApis() throws  AppException;
	public List getCamposBloqueoIF(String ic_producto_nafin, String ic_if ) throws  AppException;
	public List getCamposBloqueoIfModificaciones(String[] DatoIF,String[] DatoProducto,
			String[] DatoEstatusAnterior,String[] DatoEstatusActual,String[] DatoCausaBloqueo,
			String iNoUsuario, String numElementos) throws  AppException;

	public Map getMoneda(String claveMoneda) throws  AppException;
	public Map consultaValorMoneda(String txtDesde, String txtHasta, String cboMoneda) throws  AppException; 

	public Map enviaEPOCapturaCadenas (String no_epo, String ic_producto, String tipoCartera, 
											 String tipo_plazo, String ic_moneda,  String tipoPago  ) throws  AppException;

	public List getCamposCapturaCadenas(String no_epo, String ic_producto, String tipoCartera, 
											 String tipo_plazo, String ic_moneda,  String tipoPago )throws  AppException;
											 
	public String aceptarCapturaCadenas(String no_epo, String ic_producto, String tipoCartera, 
											 String tipo_plazo, String ic_moneda, String cod_operac, String plz_min,
											 String plz_max, String cod_operac_recurso,  String tipoPago ) throws  AppException;										 

	public boolean modificarCapturaCadenas(String plz_max_modif, String cod_operac_recurso_modif,
											String ic_base_operacion) throws  AppException;	

	public boolean eliminarCapturaCadenas(String operacion) throws  AppException;
	
	public String consigueCarteraCredito (String no_if) throws  AppException;

	public Map consiguePlazoCredito (String no_if, String tipoTasa, String tipoCred,
													String tipoAmort, String tipoCartera, String tipo_plazo) throws  AppException;

	public boolean eliminarCapturaCredito(String operacion) throws  AppException;
	
	public boolean modificarCapturaCredito(String plzMaxMod, String codOpeMod, String icBaseOpeCred,
														String cs_alta_automatica, String cs_base_op_dinamica) throws  AppException;	

	
	public boolean aceptarCapturaCredito(String noIf, String tipoTasa, String codOpe, String tipoCred, 
											 String tipoAmort, String noEmisor, String tipoCartera, String plzMin,
											 String plzMax, String tipo_plazo, String altaAutomaticaGarantia,
											 String codProdBanc, String periodicidad, String tipoInteres, 
											 String tipoRenta, String baseOperacionDinamica) throws  AppException;	
	
	
	public Map getPlazoyOperacionCapturaCredito (String noIf, String tipoTasa, String tipoCred, 
											 String tipoAmort, String tipoCartera, String tipo_plazo,
											 String noEmisor, String periodicidad, String tipoRenta ) throws  AppException;

	public List getCamposCapturaCredito(String noIf, String tipoTasa, String tipoCred, 
											 String tipoAmort, String tipoCartera, String tipo_plazo,
											 String noEmisor, String periodicidad, String tipoRenta)throws  AppException;
	
	public boolean validaCodificacionArchivoHabilitado() throws AppException;
	public boolean  getExisteClasificacion(String ic_calificacion  ) throws AppException;
	public String  getvalidaMesesSinIntereses(String ic_epo  ); //F09-2015
	public boolean guardaParamInterfazDescuento(String IC_EPO,String CC_API,String IC_PARAMETRO,String CC_PRODUCTO,String IG_PARAM_CAMPO)throws  AppException; //F01-2016
 

}

