package com.netro.informacion;
import javax.ejb.EJBObject;
import java.util.*;

import javax.ejb.Remote;

import netropology.utilerias.*;

@Remote
public interface RepEspecialesProv {
	
	public String setGuardaClasificacion(String descripcion);
	public String setEliminarClasificacion(String noClasificacion);
	public String setModificarClasificacion(String noClasificacion, String descripcion );
	public ArrayList consultaCatClasificacion();
	public ArrayList llenaCombo1(String eposPEF, 	String eposGobMun, 	String eposPrivadas, 	String eposNC, 	String eposCredi, String sNoEposSelec );
	public ArrayList llenaCombo2(String sNoEposSelec , String clasificacion, String noSeleciona	);
	public String setInsertTemporal(String noProceso, List datosInsert);
	public List validaMeses(String sAnioIni, String sAnioFin, String sMesIni, String  sMesFin, String epos, String clasificacion);
	public String setGuardaSistema(String sAnioIni, String sAnioFin, String sMesIni, String  sMesFin, String epos, String clasificacion, String mesesqNo, String todo, String noProceso);
	public ArrayList llenaComboCom1(String federales,	 String otros, String sNoEposSelec );
	public ArrayList llenaComboCom2(String eposSelecc );
	public String setnoProceso();
	
	public List getCatalogoMeses();
	public List getCatalogoMeses(int mesActual);
	public List getCatalogoMesesHastaMesActual();
	public List getCatalogoMesesHastaUltimoMes(String tabla);
	public boolean existeRegistrosEnCompranet(int mes);
	public List getBusquedaRegistrosEnCompranet(int mesInicial, int mesFinal);
	public String getIdCargaCompranet();
	public void validaRegistroCompranet(int processID, int numeroLinea, RegistroTmpCompranet registro,ValidacionesEspeciales validacionesEspeciales);
	public void insertaEposNuevasEnCatalogoCompranet(int numeroProceso);
	public void borraEposNuevasOSinClasificacion();
	public void insertaPymesNuevasEnCatalogoCompranet(int numeroProceso);
	public void asignaNumeroEPOCatalogoCOMPRANET();
	public List getCatalogoEposCompranet();
	public List getCatalogoEposCompranet(String estatus);
	public void doSuprimeEstatusNuevoEposCompranet();
	public void updateNumeroEpoCatalogoCompranet(String icEpoCompranet, String numeroEpoNuevo);
	public void asignaIdEpoCompranet(int procesoID);
	public void asignaIdPymeCompranet(int procesoID);
	public void asignaClasificacionDependenciaEpoCompranet(int procesoID);
	public boolean hayErrorCargaCompranet(int numeroProceso);
	public void insertaRegistrosEnCruceCompranet(boolean reemplazarRegistros, int numeroProceso, int mesInicial, int mesFinal);
	public StringBuffer getMensajeErrorCargaCompranet(int numeroProceso);
	public String getCamposAdicionalesSiag();
	public StringBuffer getDescripcionCamposSiag();
	public void guardarCamposAdicionalesSiag(String camposAdicionales);
	public boolean existenRegistrosEnSiag(int mes);
	public List getBusquedaRegistrosEnSiag(int mesInicial, int mesFinal);
	public String getIdCargaSiag();
	public void validaRegistroSiag(int processID, int numeroLinea, RegistroTmpSiag registro, ValidacionesEspeciales validacionesEspeciales);
	public boolean hayErrorCargaSiag(int numeroProceso);
	public void insertaRegistrosEnCruceSiag(boolean reemplazarRegistros, int numeroProceso, int mesInicial, int mesFinal);
	public StringBuffer getMensajeErrorCargaSiag(int numeroProceso);
	public List getCatalogoEposCompranet(List tipoClasificacion,List listaEposSeleccionadas);
	public List getCatalogoEposSeleccionadasCompranet(List listaEposSeleccionadas);
	public List getCatalogoPortafolioSiag();
	public List getCatalogoClasificacionDependencia();
	public String getIdCruceCadenasCompranet();
	public void registraListaEposCompranetSeleccionadas(String idProcesoCruce,List listaEposSeleccionadas);
	public void cruzaProveedoresCadenasConCompranet(String idProceso, String fechaInicialCadenas, String fechaFinalCadenas, String clasificacionDependencia, String fechaInicialCompranet, String fechaFinalCompranet );
	public String getPrimerDiaDelMes(int anio,int mes);
	public String getUltimoDiaDelMes(int anio,int mes);
	public void registraPortafoliosSeleccionados(String idProcesoCruce,String[] listaPortafolios);
	public void cruzarRFCsUnicosConSIAG( String idProceso, String fechaInicialSiag, String fechaFinalSiag );
	public void insertarCruceEnRedinfSiag(String idProcesoCruce);
	public void borrarRegistrosTmpCargaCompranet(int numeroProceso, int numeroRegistro);
	public void borrarRegistrosTmpCargaSiag(int numeroProceso, int numeroRegistro);
	public String setGenerarArchivoCargaInfo(Map hm);
	public void insertaRegistrosTmpCompranet(String urlArchivo, int numeroProceso, FiltroFechaCompranet filtroFechas);
	public void insertaRegistrosTmpSiag(String urlArchivo, int numeroProceso, FiltroFechaSiag filtroFechas);
	//----------------
	public List getCatMesesCadenas();
	public List getCatMesesCompranet();
	public String getGenerarRFCunicosCadCompranet(Map hm);
	//--------------------------------
	public String getGenerarRFCconGarant(Map hm);
	public String getGenerarRFCconSinGarant(Map hm);
	//---------------------------------
	public String getGenerarRFCunicosCadCompSiag(Map hm);
	public String getDescargarRFCunicosConGarant(Map hm);
	public String getDescargarRFCunicosSinGarant(Map hm);
	
}