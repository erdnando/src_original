package com.netro.informacion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import java.sql.*;

import java.text.SimpleDateFormat;

import java.util.*;
import netropology.utilerias.*;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;


@Stateless(name = "RepEspecialesProvEJB" , mappedName = "RepEspecialesProvEJB")
@TransactionManagement(TransactionManagementType.BEAN)
public class RepEspecialesProvBean implements RepEspecialesProv  {

	//Variable para enviar mensajes al log.
	private final static Log 				log 			= ServiceLocator.getInstance().getLog(RepEspecialesProvBean.class);
	
	@Override
	public String setGuardaClasificacion(String descripcion)  throws AppException{
	
	 log.info("setGuardaClasificacion(E)");
	 
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean commit = true;
		String respuesta ="G";			
			
		try{
		
			con.conexionDB();
			query.append(" INSERT INTO COMCAT_DEPEN_CADENAS " );
			query.append(" (ic_dependencia, cg_descripcion )");
			query.append(" VALUES(SEQ_COMCAT_DEPEN_CADENAS.NEXTVAL,?) ");
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, descripcion);
			ps.executeUpdate();
			ps.close();
			
			return respuesta;
	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error al Guardar Clasificacion");
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("setGuardaClasificacion(S)");
		}
	}//END


	/**
	 * metodo que elimina los registros del catalogo de COMCAT_DEPEN_CADENAS
	 * siempre y cuando estoy no sean ya usados en otras tablas 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param noClasificacion
	 */
	@Override
	public String setEliminarClasificacion(String noClasificacion)  throws AppException{
	
	 log.info("setEliminarClasificacion(E)");
	 
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;		
		StringBuffer query = new StringBuffer();
		boolean commit = true;
		String respuesta ="";			
		String existeCa ="", existeCo ="";
		
		try{
			con.conexionDB();
			
			//valida si existe en cruce cadenas
			query = new StringBuffer();
			query.append(" select count(*) from  COMCAT_DEPEN_CADENAS  d, COM_CRUCE_CADENAS c " );
			query.append(" WHERE c.ic_dependencia = d.ic_dependencia " );
			query.append(" and  d.ic_dependencia = ? ");
	
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, Integer.parseInt(noClasificacion) );
			rs = ps.executeQuery();
				
			while(rs.next()){					
				existeCa = rs.getString(1);	
			}
			rs.close();
			ps.close();
			
			//valida si existe en compranet
			query = new StringBuffer();
			query.append(" select count(*) from  COMCAT_DEPEN_CADENAS  d, COM_CRUCE_COMPRANET c " );
			query.append(" WHERE c.IG_CLASIF_DEPENDENCIA = d.ic_dependencia " );
			query.append(" and  d.ic_dependencia = ? ");
	
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1, Integer.parseInt(noClasificacion) );
			rs = ps.executeQuery();
				
			while(rs.next()){					
				existeCo = rs.getString(1);	
			}
			rs.close();
			ps.close();
			
			
			if(existeCa.equals("0") &&  existeCo.equals("0")){	
				query = new StringBuffer();
				query.append(" DELETE  FROM COMCAT_DEPEN_CADENAS " );
				query.append(" WHERE ic_dependencia = ? ");
				ps = con.queryPrecompilado(query.toString());
				ps.setInt(1, Integer.parseInt(noClasificacion) );
				ps.executeUpdate();
				ps.close();
			
			respuesta =  "E";
			}else{
			respuesta =  "N";
			}
			
	 		
			return respuesta;
	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error al Eliminar Clasificacion");
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("setEliminarClasificacion(S)");
		}
	}//END


/**
	 *metodo que modifica los datos de catalogo de clasificacion 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param descripcion
	 * @param noClasificacion
	 */
	@Override
	public String setModificarClasificacion(String noClasificacion, String descripcion )  throws AppException{
	
	 log.info("setEliminarClasificacion(E)");
	 
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		StringBuffer query = new StringBuffer();
		boolean commit = true;
		String respuesta ="M";			
			
		try{
			con.conexionDB();
			
			query.append(" UPDATE COMCAT_DEPEN_CADENAS " );
			query.append(" SET  cg_descripcion  = ? " );
			query.append(" WHERE ic_dependencia = ? ");
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, descripcion);
			ps.setInt(2,Integer.parseInt(noClasificacion));
			ps.executeUpdate();
			ps.close();
			
			return respuesta;
	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error al Modifica Clasificacion");
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("setModificarClasificacion(S)");
		}
	}//END

	/**
	 * metodo que muestra los datos capturados en el
	 * catalogo de clasificacion
	 * @throws netropology.utilerias.AppException
	 * @return 
	 */
	@Override
	public ArrayList consultaCatClasificacion() throws AppException{
		log.info("consultaCatClasificacion(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList datos = new ArrayList();
		ArrayList catalogos = new ArrayList();		
		StringBuffer query = new StringBuffer();
		boolean commit = true;

		try{
			con.conexionDB();

			query.append(" SELECT ic_dependencia, cg_descripcion " );
			query.append(" FROM COMCAT_DEPEN_CADENAS " );
			query.append(" ORDER BY ic_dependencia ");

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			
			while(rs.next()){					
				datos = new ArrayList();
				datos.add(rs.getString("ic_dependencia")==null?"":rs.getString("ic_dependencia"));
				datos.add(rs.getString("cg_descripcion")==null?"":rs.getString("cg_descripcion"));						
				catalogos.add(datos);						
			}			
			rs.close();
			ps.close();

			//log.debug("catalogos "+catalogos);
			
			return catalogos;

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error al obtener el catalog de clasificacion de cadenas", e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("consultaCatClasificacion(S)");
		}
	}
	/**
	 * llena el primer combo de llenado de las Epos	 * 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param sNoEposSelec
	 * @param eposCredi
	 * @param eposNC
	 * @param eposPrivadas
	 * @param eposGobMun
	 * @param eposPEF
	 */
	@Override
	public ArrayList llenaCombo1(String eposPEF, 
		String eposGobMun, 
		String eposPrivadas,	
		String eposNC, 
		String eposCredi, 
		String sNoEposSelec ) throws AppException{
		boolean commit = true;
		
		log.info("llenCombo1(E)");
			
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList datos = new ArrayList();
		ArrayList catalogos = new ArrayList();
		StringBuffer strSQL = new StringBuffer();
		
		try{
			con.conexionDB();
					
		 if(!sNoEposSelec.equals("")){
				int tamanio2 =	sNoEposSelec.length();
				String valor =  sNoEposSelec.substring(tamanio2-1, tamanio2);
							
				if(valor.equals(",")){
					sNoEposSelec =sNoEposSelec.substring(0,tamanio2-1);
				}
			}
			
					
			strSQL.append(" SELECT e.ic_epo AS ic_epo, e.cg_razon_social AS cg_razon_social, COUNT(rpe.ic_pyme) AS pymes_afiliadas");
			strSQL.append(" FROM comcat_epo e, comrel_pyme_epo rpe");
			strSQL.append(" WHERE e.ic_epo = rpe.ic_epo");
			boolean entro = false;
		
			if (eposPEF.equals("S")) {
				strSQL.append("  AND ( e.ic_tipo_epo  = 1 ");
				entro = true;
			}
			if (eposGobMun.equals("S")) {
				strSQL.append(((entro)?" OR ": "  AND ( ") +" e.ic_tipo_epo = 2");
				entro = true;
			}
			if (eposPrivadas.equals("S")) {
				strSQL.append(((entro)?" OR ": " AND ( ") +" e.ic_tipo_epo = 3");
				entro = true;
			}
			if (eposNC.equals("S")) {
				strSQL.append(((entro)?" OR ": " AND ( ") +" e.ic_tipo_epo is null");
				entro = true;
			}
			strSQL.append((entro)?" ) ": " ");
			if (eposCredi.equals("S")) {
				strSQL.append(" AND e.ic_epo IN (SELECT ic_epo FROM comrel_producto_epo WHERE ic_producto_nafin = 5)");
			}
			if (!sNoEposSelec.trim().equals("") ) {
				strSQL.append(" AND e.ic_epo NOT IN ("+sNoEposSelec+")");
			}
			strSQL.append(" GROUP BY e.ic_epo, e.cg_razon_social");
			strSQL.append(" ORDER BY e.cg_razon_social ");
	
			log.debug(strSQL.toString());
	
			ps = con.queryPrecompilado(strSQL.toString());
			rs = ps.executeQuery();
	
			while(rs.next()){					
				datos = new ArrayList();
				datos.add(rs.getString("ic_epo")==null?"":rs.getString("ic_epo"));
				datos.add(rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social"));	
				datos.add(rs.getString("pymes_afiliadas")==null?"":rs.getString("pymes_afiliadas"));
				catalogos.add(datos);
			}
			rs.close();
			ps.close();
			
			return catalogos;
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error ", e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
				log.info("llenaCombo1 (S)");
		}
	}
	
	
	/**
	 * llena el segundo combo con las epos seleccionadas
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param sNoEposSelec
	 */
	@Override
	public ArrayList llenaCombo2(String sNoEposSelec, String clasificacion, String noSeleciona) throws AppException{
	
	log.info("llenaCombo2(E)");
		
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList datos = new ArrayList();
		ArrayList catalogos = new ArrayList();
		StringBuffer strSQL = new StringBuffer();
		StringBuffer strSQL2 = new StringBuffer();
		String ic_dependencia  ="";		
		StringBuffer eposP = new StringBuffer();		
		int tamanio  = 0;		
		String eposC  = "";
		String eposT = "";
		boolean commit = true;
		
		  if(!sNoEposSelec.equals("")){
				int tamanio2 =	sNoEposSelec.length();
				String valor =  sNoEposSelec.substring(tamanio2-1, tamanio2);					
				if(valor.equals(",")){
					sNoEposSelec =sNoEposSelec.substring(0,tamanio2-1);
				}
			}
		   
			 
			 
			if(!noSeleciona.equals("")){
				int tamanio3 =	noSeleciona.length();
				String valor =  noSeleciona.substring(tamanio3-1, tamanio3);				
				if(valor.equals(",")){
					noSeleciona =noSeleciona.substring(0,tamanio3-1);
				}
			}
		   
			 
		
		try{
			con.conexionDB();
			
			//cuando la tabla de COM_CRUCE_CADENAS tiene datos con la dependencia elegida
		//	if (!clasificacion.equals("")) {
			if (sNoEposSelec.equals("") &&  !clasificacion.equals("") ) {	
				strSQL = new StringBuffer();
				strSQL.append(" SELECT distinct  e.ic_epo AS ic_epo");
				strSQL.append(" FROM comcat_epo e,  COM_CRUCE_DEPEN_EPO c ");
				strSQL.append(" WHERE e.ic_epo = c.ic_epo");			
				strSQL.append(" AND  c.ic_dependencia = ?  ");	
			 
				log.debug(strSQL.toString());		
				
				ps = con.queryPrecompilado(strSQL.toString());
				ps.setInt(1, Integer.parseInt(clasificacion) );
				rs = ps.executeQuery();
					
				while(rs.next()){					
					eposP.append(rs.getString("ic_epo")==null?"":rs.getString("ic_epo")+",");		
				}
				rs.close();
				ps.close();
						
			}
			eposC  = eposP.toString();
					
			if(!eposC.equals("")){
				tamanio =	eposC.length()-1;
				eposT =eposC.substring(0,tamanio);			
			}
			 
			 if(!sNoEposSelec.equals("")){
				int tamanio2 =	sNoEposSelec.length();
				String valor =  sNoEposSelec.substring(tamanio2-1, tamanio2);
							
				if(valor.equals(",")){
					sNoEposSelec =sNoEposSelec.substring(0,tamanio2-1);
				}
			}
			
			log.debug(" sNoEposSelec" +sNoEposSelec );		
			log.debug(" noSeleciona " +noSeleciona );		
					
			if(!sNoEposSelec.equals("") ||  !eposT.equals("") ){	
				strSQL2 = new StringBuffer();
				strSQL2.append(" SELECT e.ic_epo AS ic_epo, e.cg_razon_social AS cg_razon_social, COUNT(rpe.ic_pyme) AS pymes_afiliadas");
				strSQL2.append(" FROM comcat_epo e, comrel_pyme_epo rpe ");
				strSQL2.append(" WHERE e.ic_epo = rpe.ic_epo");
				if(!sNoEposSelec.equals("") &&  !eposT.equals("") ){
					strSQL2.append(" AND e.ic_epo IN ("+sNoEposSelec+","+eposT+")");
				}else if(sNoEposSelec.equals("") &&  !eposT.equals("") ){
					strSQL2.append(" AND e.ic_epo IN ("+eposT+")");			
				}else if(!sNoEposSelec.equals("") &&  eposT.equals("") ){			
					strSQL2.append(" AND e.ic_epo IN ("+sNoEposSelec+")");	
				}
				 
				if (!noSeleciona.equals("")) {
				strSQL2.append(" AND e.ic_epo not IN ("+noSeleciona+") ");
				}
				 
				strSQL2.append(" GROUP BY e.ic_epo, e.cg_razon_social");
				strSQL2.append(" ORDER BY e.cg_razon_social ");
			
				log.debug("---------- "+strSQL2.toString());	
			
				ps = con.queryPrecompilado(strSQL2.toString());
				rs = ps.executeQuery();
						
				while(rs.next()){					
					datos = new ArrayList();
					datos.add(rs.getString("ic_epo")==null?"":rs.getString("ic_epo"));
					datos.add(rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social"));	
					datos.add(rs.getString("pymes_afiliadas")==null?"":rs.getString("pymes_afiliadas"));
					catalogos.add(datos);
				}
				rs.close();
				ps.close();
			
			}	
			return catalogos;

		}catch(Exception e){
				commit =false;
			e.printStackTrace();
			throw new AppException("Error :: ", e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("llenaCombo2(S)");
		}
	}
	
	
	@Override
	public String setnoProceso()  throws AppException{
	
	 log.info("setnoProceso(E)");
	 
		AccesoDB con = new AccesoDB();
		StringBuffer query = new StringBuffer();
		boolean commit = true;
		PreparedStatement ps = null;
		ResultSet rs = null;		
		String valor ="";	
		
		try{
		
			con.conexionDB();
				
			//obtenemos el numero maximo  de secuencia
			query = new StringBuffer();
			query.append("  select SEQ_COM_TMP_CRUCE_CADENAS.NEXTVAL as valor  from dual  " );
			ps = con.queryPrecompilado(query.toString());		
			rs = ps.executeQuery();
				
			while(rs.next()){					
				valor = rs.getString(1);	
			}
			rs.close();
			ps.close();
				
		
			return valor;
	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error setnoProceso");
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("setnoProceso(S)");
		}
	}//END
	
	
	/**
	 * inserta a tabla temporal
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param datosInsert
	 */
	@Override
	public String setInsertTemporal(String noProceso, List datosInsert)  throws AppException{
	
	 log.info("setInsertTemporal(E)");
	 
		AccesoDB con = new AccesoDB();
		StringBuffer query = new StringBuffer();
		boolean commit = true;
		PreparedStatement ps = null;
		ResultSet rs = null;		
			
		try{
		
			con.conexionDB();					
			String ic_epoN = "";
			StringBuffer eposNBorrar = new StringBuffer();
			String eposNBorrarS = "";
			String clasificacion  = "";
			//se realiza el insert en la 
			Iterator registros2 = datosInsert.iterator();
			Calendar cal = Calendar.getInstance();
			int sAnioIni = cal.get(Calendar.YEAR);

				for(int i=0; i< datosInsert.size();i++){
					List campos = (List)datosInsert.get(i);
							
					
				String ic_epo = (String) campos.get(0);
				String numPyme = (String) campos.get(1);
				String nomEstado = (String) campos.get(2);
				String cg_RFC = (String) campos.get(3);
				String sector = (String) campos.get(4);
				String monto = (String) campos.get(5);
				String mes = (String) campos.get(6);
				clasificacion = (String) campos.get(7);
				
				String nomes = mes.substring(0, 3);
				String nuMes ="", fecha  = "";
				
				//log.debug("num  Mes "+nomes);
				//log.debug("mes  "+mes);				
				
				if(nomes.equals("ENE")) {  nuMes ="1";   fecha ="01/01/";  }
				if(nomes.equals("FEB")) {  nuMes ="2";   fecha ="01/02/";  }
				if(nomes.equals("MAR")) {  nuMes ="3";   fecha ="01/03/";  }
				if(nomes.equals("ABR")) {  nuMes ="4";   fecha ="01/04/";  }
				if(nomes.equals("MAY")) {  nuMes ="5";   fecha ="01/05/";  }
				if(nomes.equals("JUN")) {  nuMes ="6";   fecha ="01/06/";  }
				if(nomes.equals("JUL")) {  nuMes ="7";   fecha ="01/07/";  }
				if(nomes.equals("AGO")) {  nuMes ="8";   fecha ="01/08/";  }
				if(nomes.equals("SEP")) {  nuMes ="9";   fecha ="01/09/";  }
				if(nomes.equals("OCT")) {  nuMes ="910"; fecha ="01/10/";  }
				if(nomes.equals("NOV")) {  nuMes ="911"; fecha ="01/11/"; }
				if(nomes.equals("DIC")) {  nuMes ="912"; fecha ="01/12/"; }
				
				//log.debug("nomes  "+nomes);
				fecha  += String.valueOf(sAnioIni); 
				//log.debug("fecha  "+fecha);
				 
				query = new StringBuffer();
				query.append(" INSERT INTO COM_TMP_CRUCE_CADENAS " );
				query.append(" (ic_cruce_cadenas ,ic_epo, ic_pyme, cg_estado, cg_RFC, cg_sector, fn_monto, cg_mes, ic_dependencia, cg_numes, df_fecha )");
				query.append(" VALUES(?,?,?,?,?,?,?,?,?,?, TO_DATE('"+fecha+"', 'DD/MM/YYYY')) ");
				
				//log.debug("query.toString()   "+query.toString());
			
				ps = con.queryPrecompilado(query.toString());
				ps.setInt(1, Integer.parseInt(noProceso) );				
				ps.setInt(2, Integer.parseInt(ic_epo) );
				ps.setInt(3, Integer.parseInt(numPyme) );
				ps.setString(4, nomEstado);
				ps.setString(5, cg_RFC);
				ps.setString(6, sector);
				ps.setString(7, monto);
				ps.setString(8, mes);
				ps.setInt(9, Integer.parseInt(clasificacion) );		
				ps.setString(10, nuMes);
				
				ps.executeUpdate();
				ps.close();
							
			} //while(registros2.hasNext()) {						
			
			return noProceso;
	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error setInsertTemporal");
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("setInsertTemporal(S)");
		}
	}//END
	
	

	/**
	 * metdo que valida los meses que ya estan capturados en la tabla de cruce de cadenas
	 * COM_CRUCE_CADENAS
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param epos
	 * @param sMesFin
	 * @param sMesIni
	 * @param sAnioFin
	 * @param sAnioIni
	 */
	@Override
	public List validaMeses(String sAnioIni, String sAnioFin, String sMesIni, String  sMesFin, String epos, String clasificacion) throws AppException{
		log.info("validaMeses(E)");
		
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;				
		StringBuffer strSQL = new StringBuffer();		
		StringBuffer mesesEpos = new StringBuffer();
		StringBuffer mesesEpos3 = new StringBuffer();
		String mesesP = "";
		String mesesE = "";
	  ArrayList mesesA = new ArrayList();
		boolean commit = true;
						
		try{
			con.conexionDB();
		
				strSQL = new StringBuffer();
				strSQL.append(" SELECT  MES.nombre||'-'||ANO.ic_anio as nomMesAnio");
				strSQL.append(" FROM (SELECT 1 AS ic_mes, 'ENE' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 2 AS ic_mes, 'FEB' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 3 AS ic_mes, 'MAR' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 4 AS ic_mes, 'ABR' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 5 AS ic_mes, 'MAY' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 6 AS ic_mes, 'JUN' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 7 AS ic_mes, 'JUL' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 8 AS ic_mes, 'AGO' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 9 AS ic_mes, 'SEP' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 10 AS ic_mes,'OCT' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 11 AS ic_mes,'NOV' AS nombre FROM dual UNION");
				strSQL.append(" SELECT 12 AS ic_mes,'DIC' AS nombre FROM dual) MES,");
				strSQL.append(" (SELECT "+sAnioIni+" AS ic_anio from dual UNION");
				strSQL.append(" SELECT "+sAnioFin+" AS ic_anio from dual) ANO");
				if(sAnioIni.equals(sAnioFin) ){
				strSQL.append(" WHERE (MES.ic_mes >= TO_NUMBER('"+sMesIni+"') AND ANO.ic_anio = "+sAnioIni+") AND (MES.ic_mes <= TO_NUMBER('"+sMesFin+"') AND ANO.ic_anio = "+sAnioFin+")");
				}else if(!sAnioIni.equals(sAnioFin) ){
				strSQL.append(" WHERE (MES.ic_mes >= TO_NUMBER('"+sMesIni+"') AND ANO.ic_anio = "+sAnioIni+") OR (MES.ic_mes <= TO_NUMBER('"+sMesFin+"') AND ANO.ic_anio = "+sAnioFin+")");
				}
				
				strSQL.append(" ORDER BY  nomMesAnio");
				
				//log.debug(strSQL.toString());
				
				ps = con.queryPrecompilado(strSQL.toString());
				rs = ps.executeQuery();
					
				while(rs.next()){					
					mesesEpos.append("'"+rs.getString("nomMesAnio")+"',");		
					int tamanio  =mesesEpos.length()-1;			
					mesesP = mesesEpos.substring(0, tamanio);		
				}
				rs.close();
				ps.close();
				
				//log.debug(" mesesP  "+ mesesP );	
		
				//-------------------------------------------------------------------
		
		 if(!epos.equals("")){
				int tamanio2 =	epos.length();
				String valor =  epos.substring(tamanio2-1, tamanio2);
							
				if(valor.equals(",")){
					epos =epos.substring(0,tamanio2-1);
				}
			}
			
				strSQL = new StringBuffer();
				StringBuffer mesesEpos2 = new StringBuffer();
				String meses = "", meses2 ="";
				strSQL.append(" SELECT distinct cg_mes  FROM COM_CRUCE_CADENAS ");		
				strSQL.append(" where ic_epo in( "+epos+") ");
				strSQL.append(" and  cg_mes IN ("+mesesP+") ");	
				if(!clasificacion.equals("")){
					strSQL.append(" and  ic_dependencia = "+clasificacion+" ");	
					}
				strSQL.append(" order by cg_mes ");

				log.debug(strSQL.toString());
				
				ps = con.queryPrecompilado(strSQL.toString());
				rs = ps.executeQuery();
					
				while(rs.next()){					
					mesesEpos2.append("'"+rs.getString("cg_mes")+"',");	
							
					int tamanio  =mesesEpos2.length()-1;			
					meses = mesesEpos2.substring(0, tamanio);	
					
					mesesEpos3.append(rs.getString("cg_mes")+",");		
					int tamanio2  =mesesEpos3.length()-1;			
					meses2 = mesesEpos3.substring(0, tamanio2);	
					 
				}
				rs.close();
				ps.close();
				
				mesesA.add(meses); //este para envieralo al metodo de setGuardaSistema
				mesesA.add(meses2); //este es para mostarlo en el comfirm
							
					
	   return mesesA;

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error obtener los meses que ya estan capturados", e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("validaMeses(S)");
		}
	}
	
	
	/**
	 * metodo que guarda los datos  a la tabla original de cruce de cadenas
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param clasificacion
	 * @param epos
	 * @param sMesFin
	 * @param sMesIni
	 * @param sAnioFin
	 * @param sAnioIni
	 */
	@Override
	public String setGuardaSistema(String sAnioIni, String sAnioFin, String sMesIni, String  sMesFin, String epos, String clasificacion, String mesesqNo, String todo, String noProceso) throws AppException{
	
		log.info("setGuardaSistema(E)");
	 
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean commit = true;
		String respuesta ="G";	
		StringBuffer meses = new StringBuffer();				
		StringBuffer query = new StringBuffer();
		String meses1 = "";
		//log.debug(" mesesqNo   " +mesesqNo);				
		try{
		
			con.conexionDB();
			
			query = new StringBuffer();
			query.append(" SELECT  MES.nombre||'-'||ANO.ic_anio as nomMesAnio");
			query.append(" FROM (SELECT 1 AS ic_mes, 'ENE' AS nombre FROM dual UNION");
			query.append(" SELECT 2 AS ic_mes, 'FEB' AS nombre FROM dual UNION");
			query.append(" SELECT 3 AS ic_mes, 'MAR' AS nombre FROM dual UNION");
			query.append(" SELECT 4 AS ic_mes, 'ABR' AS nombre FROM dual UNION");
			query.append(" SELECT 5 AS ic_mes, 'MAY' AS nombre FROM dual UNION");
			query.append(" SELECT 6 AS ic_mes, 'JUN' AS nombre FROM dual UNION");
			query.append(" SELECT 7 AS ic_mes, 'JUL' AS nombre FROM dual UNION");
			query.append(" SELECT 8 AS ic_mes, 'AGO' AS nombre FROM dual UNION");
			query.append(" SELECT 9 AS ic_mes, 'SEP' AS nombre FROM dual UNION");
			query.append(" SELECT 10 AS ic_mes,'OCT' AS nombre FROM dual UNION");
			query.append(" SELECT 11 AS ic_mes,'NOV' AS nombre FROM dual UNION");
			query.append(" SELECT 12 AS ic_mes,'DIC' AS nombre FROM dual) MES,");
			query.append(" (SELECT "+sAnioIni+" AS ic_anio from dual UNION");
			query.append(" SELECT "+sAnioFin+" AS ic_anio from dual) ANO");			
			if(sAnioIni.equals(sAnioFin) ){
					query.append(" WHERE (MES.ic_mes >= TO_NUMBER('"+sMesIni+"') AND ANO.ic_anio = "+sAnioIni+") AND (MES.ic_mes <= TO_NUMBER('"+sMesFin+"') AND ANO.ic_anio = "+sAnioFin+")");
			}else if(!sAnioIni.equals(sAnioFin) ){
					query.append(" WHERE (MES.ic_mes >= TO_NUMBER('"+sMesIni+"') AND ANO.ic_anio = "+sAnioIni+") OR (MES.ic_mes <= TO_NUMBER('"+sMesFin+"') AND ANO.ic_anio = "+sAnioFin+")");
			}
			query.append(" ORDER BY  nomMesAnio");
	
			log.debug("query.toString() Meses " +query.toString());
		 
			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();
			
			while(rs.next()){					
				meses.append("'"+rs.getString("nomMesAnio")+"',");		
				int tamanio  =meses.length()-1;			
				meses1 = meses.substring(0, tamanio);	
			}
			rs.close();
			ps.close();
		
				
			log.debug("todo " +todo );
			
			 if(!epos.equals("")){
				int tamanio2 =	epos.length();
				String valor =  epos.substring(tamanio2-1, tamanio2);
							
				if(valor.equals(",")){
					epos =epos.substring(0,tamanio2-1);
				}
			}
			
		
			if(todo.equals("N")){		
				//guarda los periodos seleccionados	 a Excepci�n  de los que ya existen 
				query = new StringBuffer();		
				query.append(" 	INSERT INTO COM_CRUCE_CADENAS  " );
				query.append(" 	 (ic_cruce_cadenas ,ic_epo, ic_pyme, cg_estado, cg_RFC, cg_sector, fn_monto, cg_mes, ic_dependencia, cg_numes, df_fecha ) " );
				query.append(" 	(Select SEQ_COM_CRUCE_CADENAS.NEXTVAL,  " );
				query.append(" 	ic_epo, ic_pyme, cg_estado, cg_RFC, cg_sector, fn_monto, cg_mes, ic_dependencia, cg_numes, df_fecha  " );
				query.append(" 	FROM  COM_TMP_CRUCE_CADENAS " );
				query.append(" 	where ic_epo in("+epos+",''"+") " );
				query.append(" 	and cg_mes in("+meses1+") " );
				if(!mesesqNo.toString().equals("")){
					query.append("  and cg_mes not in("+mesesqNo+") " );	
				}		
				query.append("  AND ic_dependencia = "+clasificacion );
				
				query.append("  AND ic_cruce_cadenas = "+noProceso+") " );
				
				
			}else if(todo.equals("S")){		 //cuando se remplazan todos los registros	
			
				query = new StringBuffer();	
				query.append(" DELETE  FROM COM_CRUCE_CADENAS " );
				query.append(" WHERE ic_dependencia in("+ clasificacion+") ");
				query.append(" AND ic_epo in("+epos+",''"+") " );
				if(!meses1.equals("")){
				query.append(" and cg_mes in("+meses1+") " );		
				}
			
				log.debug("query.toString() " +query.toString());
				
				ps = con.queryPrecompilado(query.toString());				
				ps.executeUpdate();
				ps.close();
			
				//guarda todos los periodos seleccionados 	
				query = new StringBuffer();		
				query.append(" 	INSERT INTO COM_CRUCE_CADENAS  " );
				query.append(" 	 (ic_cruce_cadenas ,ic_epo, ic_pyme, cg_estado, cg_RFC, cg_sector, fn_monto, cg_mes, ic_dependencia, cg_numes, df_fecha ) " );
				query.append(" 	(Select SEQ_COM_CRUCE_CADENAS.NEXTVAL,  " );
				query.append(" 	ic_epo, ic_pyme, cg_estado, cg_RFC, cg_sector, fn_monto, cg_mes, ic_dependencia, cg_numes, df_fecha  " );
				query.append(" 	FROM  COM_TMP_CRUCE_CADENAS " );
				query.append(" 	where ic_epo in("+epos+",''"+") " );
				if(!meses1.equals("")){
				query.append(" 	and cg_mes in("+meses1+") " );		
				}
				query.append("  AND ic_dependencia = "+clasificacion );
				query.append("  AND ic_cruce_cadenas = "+noProceso+") " );
			
		}
		
		log.debug("query.toString() " +query.toString());
		
		ps = con.queryPrecompilado(query.toString());	 
		ps.executeUpdate();
		ps.close();						
	
		return respuesta;
	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error setGuardaSistema");
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("setGuardaSistema(S)");
		}
	}//END

	/**
	 * llena el combo1 de la pantalla de consulta de compranet 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param otros
	 * @param federales
	 */
	@Override
	public ArrayList llenaComboCom1(String federales,	 String otros, String sNoEposSelec ) throws AppException{
	
	log.info("llenaComboCom1(E)");
		
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList datos = new ArrayList();
		ArrayList catalogos = new ArrayList();
		StringBuffer strSQL = new StringBuffer();
		String dependencias  ="";
		boolean commit = true;
		
		try{
			con.conexionDB();
 
			strSQL.append(" SELECT distinct e.IG_NUMERO_EPO AS ic_epo, e.cg_razon_social AS cg_razon_social ");;
			strSQL.append(" FROM comcat_epo_compranet e,  ");			
			strSQL.append(" com_cruce_compranet c ");
			strSQL.append(" WHERE e.ic_epo_compranet = c.ic_epo_compranet  ");
			
			if(!federales.equals("") && !otros.equals("")){			
				strSQL.append(" and c.IG_CLASIF_DEPENDENCIA in ( "+federales +", "+ otros+" )  ");
			}else if(!federales.equals("") ){
				strSQL.append(" and c.IG_CLASIF_DEPENDENCIA  ="+federales);
			}else if(!otros.equals("") ){				
				strSQL.append(" and c.IG_CLASIF_DEPENDENCIA =  "+otros);
			}	
			
			if (!sNoEposSelec.trim().equals("") ) {
				strSQL.append(" AND e.IG_NUMERO_EPO NOT IN ("+sNoEposSelec+")");
			}
			strSQL.append(" AND e.CS_NUEVO ='N' " );
			
			strSQL.append(" GROUP BY e.IG_NUMERO_EPO, e.cg_razon_social ");
			strSQL.append(" ORDER BY e.cg_razon_social  ");
			 
			log.debug(strSQL.toString());
	
			ps = con.queryPrecompilado(strSQL.toString());
		  rs = ps.executeQuery();
			
			
			String IG_NUMERO_EPO ="", IG_NUMERO_EPO2 ="";
			
			while(rs.next()){		
				IG_NUMERO_EPO = rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				if(!IG_NUMERO_EPO.equals(IG_NUMERO_EPO2)){
				datos = new ArrayList();
				datos.add(rs.getString("ic_epo")==null?"":rs.getString("ic_epo"));
				datos.add(rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social"));					
				catalogos.add(datos);				
			}
			IG_NUMERO_EPO2 =IG_NUMERO_EPO;
			}	
			rs.close();
			ps.close();
		
			return catalogos;

		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			throw new AppException("Error ", e);
		}finally{
				con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("llenaComboCom1 (S)");
		}
	}
	
/**
	 *llena el combo2 de la pantalla de consulta de compranet 
	 * @throws netropology.utilerias.AppException
	 * @return 
	 * @param eposSelecc
	 */
	@Override	
	public ArrayList llenaComboCom2(String eposSelecc ) throws AppException{
	
		log.info("llenaComboCom2(E)");
		
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList datos = new ArrayList();
		ArrayList catalogos = new ArrayList();
		StringBuffer strSQL = new StringBuffer();
		boolean commit = true;

		try{
			con.conexionDB();
	
			strSQL.append(" SELECT e.IG_NUMERO_EPO AS ic_epo, e.cg_razon_social AS cg_razon_social ");
			strSQL.append(" FROM comcat_epo_compranet e,  ");
			strSQL.append(" com_cruce_compranet c ");
			strSQL.append(" WHERE e.ic_epo_compranet = c.ic_epo_compranet  ");
			strSQL.append("and e.IG_NUMERO_EPO in ( "+eposSelecc+" )  ");
		  strSQL.append(" AND e.CS_NUEVO ='N' " );
			strSQL.append("GROUP BY e.IG_NUMERO_EPO, e.cg_razon_social ");
			strSQL.append("ORDER BY e.cg_razon_social  ");
								
			log.debug(strSQL.toString());
		
			ps = con.queryPrecompilado(strSQL.toString());		
			rs = ps.executeQuery();
		String IG_NUMERO_EPO ="", IG_NUMERO_EPO2 ="";
		
			while(rs.next()){		
				IG_NUMERO_EPO = rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
				
				if(!IG_NUMERO_EPO.equals(IG_NUMERO_EPO2)){				
					datos = new ArrayList();
					datos.add(rs.getString("ic_epo")==null?"":rs.getString("ic_epo"));
					datos.add(rs.getString("cg_razon_social")==null?"":rs.getString("cg_razon_social"));					
					catalogos.add(datos);
				}
					IG_NUMERO_EPO2 =IG_NUMERO_EPO;
			}
			rs.close();
			ps.close();

			return catalogos;

		}catch(Exception e){
				commit = false;
			e.printStackTrace();
			throw new AppException("Error ", e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("llenaComboCom2 (S)");
		}
	}
 
	/**
	 * Devuelve una lista con los nombres de los meses y sus claves.
	 * @return <tt>List</tt>
	 */
	@Override
	public List getCatalogoMeses(){
		return getCatalogoMeses(12);
	}
	
	/**
	 * Devuelve una lista con los nombres de los meses y sus claves, donde el ultimo mes de la lista
	 * es el que se indica por su clave en el parametro: <tt>mesActual</tt>
	 * @param mesActual Numero del Mes Actual
	 * @return <tt>List</tt>
	 */
	@Override
	public List getCatalogoMeses(int mesActual){
		
		log.info("getCatalogoMeses(E)");
		
		List 					lista 		= null;
		ElementoCatalogo 	elemento 	= null;
		
		lista 		= new ArrayList();
		if(mesActual == 0) return lista;
		// Enero
		elemento 	= new ElementoCatalogo();
		elemento.setClave("1");
		elemento.setDescripcion("Enero");
		lista.add(elemento);
		if(mesActual == 1) return lista;
		// Febrero
		elemento 	= new ElementoCatalogo();
		elemento.setClave("2");
		elemento.setDescripcion("Febrero");
		lista.add(elemento);
		if(mesActual == 2) return lista;
		// Marzo
		elemento 	= new ElementoCatalogo();
		elemento.setClave("3");
		elemento.setDescripcion("Marzo");
		lista.add(elemento);
		if(mesActual == 3) return lista;
		// Abril
		elemento 	= new ElementoCatalogo();
		elemento.setClave("4");
		elemento.setDescripcion("Abril");
		lista.add(elemento);
		if(mesActual == 4) return lista;
		// Mayo
		elemento 	= new ElementoCatalogo();
		elemento.setClave("5");
		elemento.setDescripcion("Mayo");
		lista.add(elemento);
		if(mesActual == 5) return lista;
		// Junio
		elemento 	= new ElementoCatalogo();
		elemento.setClave("6");
		elemento.setDescripcion("Junio");
		lista.add(elemento);
		if(mesActual == 6) return lista;
		// Julio
		elemento 	= new ElementoCatalogo();
		elemento.setClave("7");
		elemento.setDescripcion("Julio");
		lista.add(elemento);
		if(mesActual == 7) return lista;
		// Agosto
		elemento 	= new ElementoCatalogo();
		elemento.setClave("8");
		elemento.setDescripcion("Agosto");
		lista.add(elemento);
		if(mesActual == 8) return lista;
		// Septiembre
		elemento 	= new ElementoCatalogo();
		elemento.setClave("9");
		elemento.setDescripcion("Septiembre");
		lista.add(elemento);
		if(mesActual == 9) return lista;
		// Octubre
		elemento 	= new ElementoCatalogo();
		elemento.setClave("10");
		elemento.setDescripcion("Octubre");
		lista.add(elemento);
		if(mesActual == 10) return lista;
		// Noviembre
		elemento 	= new ElementoCatalogo();
		elemento.setClave("11");
		elemento.setDescripcion("Noviembre");
		lista.add(elemento);
		if(mesActual == 11) return lista;
		// Diciembre
		elemento 	= new ElementoCatalogo();
		elemento.setClave("12");
		elemento.setDescripcion("Diciembre");
		lista.add(elemento);
		if(mesActual == 12) return lista;
		
		log.info("getCatalogoMeses(S)");
		return lista;
	}
	
	/**
	 * Devuelve una lista con los nombres de los meses y sus claves, donde el ultimo mes de la lista
	 * es el que corresponde al mes actual.
	 * @return <tt>List</tt>
	 */
	@Override
	public List getCatalogoMesesHastaMesActual(){
		
		log.info("getCatalogoMeses(E)");
		int 		mesActual 	= Fecha.getMesActual(); 
		List 		lista 		= getCatalogoMeses(mesActual); 
		log.info("getCatalogoMeses(S)");
		
		return 	lista;
		
	}
	
	/**
	 * Devuelve el numero del ultimo mes para el que se encontraron registros en la tabla COM_CRUCE_SIAG
	 * @return <tt>int</tt> con el numero del mes. Devuelve cero si no se encontraron registros.
	 */
	private int getUltimoMesConDatosEnSiag()
		throws AppException{
		
		log.info("getUltimoMesConDatosEnSiag(E)");
		
		int 					numeroMes		= 0;	
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			con.conexionDB();
			
			int  		anioActual 			= Fecha.getAnioActual();
			String 	primerDiaDelAnio	= "01/01/"+String.valueOf(anioActual);
			
			querySentencia.append(
				"SELECT 														"  + 
				"  NVL(TO_CHAR(MAX(DF_FECHA_REGISTRO),'MM'),0) 	"  +
				"		AS NUMERO_MES 										"  +
 				"FROM 														"  + 
				"  COM_CRUCE_SIAG											"  +
				"WHERE 														"  +
				"	DF_FECHA_REGISTRO >= TO_DATE(?,'DD/MM/YYYY') "
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setString(1,primerDiaDelAnio);
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()){
				numeroMes = rs.getInt("NUMERO_MES");
			}
			
		}catch(Exception e){
			log.error("getUltimoMesConDatosEnSiag(Exception)");
			e.printStackTrace();
			throw new AppException("Error al consultar fecha de ultimo mes en com_cruce_siag.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getUltimoMesConDatosEnSiag(S)");	
		}
		
		return numeroMes;

	}
	
	/**
	 * Devuelve el numero del ultimo mes para el que se encontraron registros en la tabla COM_CRUCE_COMPRANET
	 * @return <tt>int</tt> con el numero del mes. Devuelve cero si no se encontraron registros.
	 */
	private int getUltimoMesConDatosEnCompranet()
		throws AppException{
		
		log.info("getUltimoMesConDatosEnCompranet(E)");
		
		int 					numeroMes		= 0;	
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			con.conexionDB();
			
			int  		anioActual 			= Fecha.getAnioActual();
			String 	primerDiaDelAnio	= "01/01/"+String.valueOf(anioActual);
			
			querySentencia.append(
				"SELECT 														"  + 
				"  NVL(TO_CHAR(MAX(DF_FECHA_EMISION),'MM'),0) 	"  +
				"		AS NUMERO_MES 										"  +
 				"FROM 														"  + 
				"  COM_CRUCE_COMPRANET									"  +
				"WHERE 														"  +
				"	DF_FECHA_EMISION >= TO_DATE(?,'DD/MM/YYYY')  "
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setString(1,primerDiaDelAnio);
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()){
				numeroMes = rs.getInt("NUMERO_MES");
			}
			
		}catch(Exception e){
			log.error("getUltimoMesConDatosEnCompranet(Exception)");
			e.printStackTrace();
			throw new AppException("Error al consultar fecha de ultimo mes en com_cruce_compranet.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getUltimoMesConDatosEnCompranet(S)");	
		}
		
		return numeroMes;

	}
	
	/**
	 * Devuelve el numero del ultimo mes para el que se encontraron registros en la tabla COM_CRUCE_COMPRANET
	 * @return <tt>int</tt> con el numero del mes. Devuelve cero si no se encontraron registros.
	 */
	private int getUltimoMesConDatosEnCadenas()
		throws AppException{
		
		log.info("getUltimoMesConDatosEnCadenas(E)");
		
		int 					numeroMes		= 0;	
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			con.conexionDB();
			
			int  		anioActual 			= Fecha.getAnioActual();
			String 	primerDiaDelAnio	= "01/01/"+String.valueOf(anioActual);
			
			querySentencia.append(
				"SELECT 											"  + 
				"  NVL(TO_CHAR(MAX(DF_FECHA),'MM'),0) 	"  +
				"		AS NUMERO_MES 							"  +
 				"FROM 											"  + 
				"  COM_CRUCE_CADENAS							"  +
				"WHERE 											"  +
				"	DF_FECHA >= TO_DATE(?,'DD/MM/YYYY') "
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setString(1,primerDiaDelAnio);
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()){
				numeroMes = rs.getInt("NUMERO_MES");
			}
			
		}catch(Exception e){
			log.error("getUltimoMesConDatosEnCadenas(Exception)");
			e.printStackTrace();
			throw new AppException("Error al consultar fecha de ultimo mes en com_cruce_compranet.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getUltimoMesConDatosEnCadenas(S)");	
		}
		
		return numeroMes;

	}
 
	/**
	 * Devuelve una lista con los nombres de los meses y sus claves, donde el ultimo mes de la lista
	 * corresponde al ultimo mes encontrado en la tabla: COM_CRUCE_SIAG o COM_CRUCE_COMPRANET
	 * @param tabla <tt>String</tt> con el nombre de la tabla donde se buscara el mes mas grande
	 *              para el que se encontraron registros.
	 * @return <tt>List</tt>
	 */
	@Override
	public List getCatalogoMesesHastaUltimoMes(String tabla)
		throws AppException{
		
		log.info("getCatalogoMeses(E)");
		
		int 		ultimoMes 	= 0;
		if("COM_CRUCE_SIAG".equals(tabla)){
			ultimoMes = getUltimoMesConDatosEnSiag();
		}else if("COM_CRUCE_COMPRANET".equals(tabla)){
			ultimoMes = getUltimoMesConDatosEnCompranet();
		}else if("COM_CRUCE_CADENAS".equals(tabla)){
			ultimoMes = getUltimoMesConDatosEnCadenas();
		}
		
		List 		lista 		= getCatalogoMeses(ultimoMes); 
		log.info("getCatalogoMeses(S)");
		
		return 	lista;
	}
	 
	/**
	* Revisa si hay registros en la tabla: COM_CRUCE_COMPRANET para el mes indicado
	* en el campo: <tt>mes</tt>
	* @param mes 	Numero del Mes
	* @return <tt>boolean</tt> con valor <tt>true</tt> si se encontraron registros.
	*			 <tt>false</tt> en caso contrario.
	*/
	@Override
	public boolean existeRegistrosEnCompranet(int mes)
		throws AppException{
			
		log.info("existeRegistrosEnCompranet(E)");
		boolean 				existeRegistro = false;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			String anioActual 		= String.valueOf(Fecha.getAnioActual());
			String fechaInicioMes 	= "01/"+(mes<10?"0":"")+String.valueOf(mes)+"/"+anioActual;
			String fechaFinMes		= Fecha.getUltimoDiaDelMes(fechaInicioMes,"dd/MM/yyyy");
			
			con.conexionDB();
			querySentencia.append(
				"SELECT 																"  + 
				"  DECODE(COUNT(1),0,'false','true') AS HAY_REGISTROS "  + 
				"FROM 																"  + 
				"  COM_CRUCE_COMPRANET 											"  +
				"WHERE 																"  + 
				"  ROWNUM = ? AND 												"  + // 1
				"  DF_FECHA_EMISION 		 >= TO_DATE(?,'DD/MM/YYYY') 	"  + // fechaInicioMes
				"  AND DF_FECHA_EMISION  <  TO_DATE(?,'DD/MM/YYYY')+1	"   // fechaFinMes
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, 	1 );
			ps.setString(2, fechaInicioMes);
			ps.setString(3, fechaFinMes);
			rs = ps.executeQuery();
			 
			if(rs != null && rs.next()){
				existeRegistro = "true".equals(rs.getString("HAY_REGISTROS"))?true:false;
			}
		}catch(Exception e){
			log.error("existeRegistrosEnCompranet(Exception)");
			log.error("existeRegistrosEnCompranet.mes = "+mes);
			e.printStackTrace();
			throw new AppException("Error al realizar busqueda en COMPRANET");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("existeRegistrosEnCompranet(S)");	
		}
		
		return existeRegistro;
	}
	
	/**
	 * Devuelve una lista con el resultado de la busqueda de registros para cada uno
	 * de los meses comprendidos a patir del mes: <tt>mesInicial</tt> y hasta el mes: 
	 * <tt>mesFinal</tt> para el a�o en curso.
	 *
	 * @param mesInicial Numero del mes inicial
	 * @param mesFinal Numero del mes final
	 * @return <tt>List</tt> con el resultado de las busquedas de los meses 
	 * especificados.
	 */
	@Override
	public List getBusquedaRegistrosEnCompranet(int mesInicial, int mesFinal)
		throws AppException{
		
		log.info("getBusquedaRegistrosEnCompranet(E)");
		List busqueda = new ArrayList();
    
		try {
			
			
			if(mesFinal < mesInicial){
				log.info("getBusquedaRegistrosEnCompranet(S)");
				return busqueda;
			}
			
			HashMap registro 			= null;
			boolean existeRegistro 	= false;
			for(int mes=mesInicial;mes<=mesFinal;mes++){
				
				registro 		= new HashMap();
				existeRegistro = existeRegistrosEnCompranet(mes);
				registro.put("MES",					String.valueOf(mes));
				registro.put("EXISTE_REGISTRO",	String.valueOf(existeRegistro));
				
				busqueda.add(registro);
			}
			
		}catch(Exception e){
			log.error("getBusquedaRegistrosEnCompranet(Exception)");
			log.error("getBusquedaRegistrosEnCompranet.mesInicial = "+mesInicial);
			log.error("getBusquedaRegistrosEnCompranet.mesFinal   = "+mesFinal);
			e.printStackTrace();
			throw new AppException("Error al buscar registros en COMPRANET");
		}
		log.info("getBusquedaRegistrosEnCompranet(S)");
		return busqueda;
	}
	
	/**
	 * Devuelve el ID del Proceso de Carga de la Base de Datos de Compranet
	 * @return <tt>String</tt> con el ID.
	 */
	@Override
	public String getIdCargaCompranet() 
		throws AppException{
		
		log.info("getIdCargaCompranet(E)");
		String idProceso = null;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			con.conexionDB();
			querySentencia.append(
				"SELECT SEQ_COMTMP_CRUCE_COMPRANET.NEXTVAL AS ID_PROCESO FROM DUAL"
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			rs = ps.executeQuery();
			 
			if(rs != null && rs.next()){
				idProceso = rs.getString("ID_PROCESO");
			}
			
		}catch(Exception e){
			log.error("getIdCargaCompranet(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener ID del Proceso de Carga de la Base de Datos de COMPRANET");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getIdCargaCompranet(S)");	
		}
		
		return idProceso;
	}
		
	/**
	 * Valida el registro cargado en la tabla COMTMP_CRUCE_COMPRANET tal que tenga el numero de proceso <tt>processID</tt> 
	 * y el numero de linea: <tt>numeroLinea</tt>
	 */
	@Override
	public void validaRegistroCompranet(int processID, int numeroLinea, RegistroTmpCompranet registro,ValidacionesEspeciales validacionesEspeciales) 
		throws AppException{

		//log.info("validaRegistroCompranet(E)");	
		
		// Obtener Registro	
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		ResultSet 			rs 				= null;
		PreparedStatement ps					= null;
		
		boolean				updateRegistro = false;
		
		try {
			
			con.conexionDB();
			
			// Borrar los datos anteriores
			registro.doReset();
			
			// Leer Datos del Registro
			query = new StringBuffer(); 		
			query.append(
				"SELECT                    " +
				"	CG_NOMBRE_EPO,        	" +
				"	CG_LICITACION,        	" +
				"	CG_UNIDAD,        		" +
				"	CG_TIPO_CONTRATACION,   " +
				"	CG_FECHA_EMISION,       " +
				"	CG_NUM_PARTIDA,        	" +
				"	CG_IMPORTE_SIN_IVA,     " +
				"	CG_RAZON_SOCIAL,        " +
				"	CG_RFC,        			" +
				"	CG_CLASIF_DEPENDENCIA   " +
		 		"FROM                      " +
				"	COMTMP_CRUCE_COMPRANET  " +
				"WHERE                     " +
				"	IC_PROCESO      = ? AND " +// processID
				"	IC_NUMERO_LINEA = ?		"  // Numero de Linea
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,processID);
			ps.setInt(2,numeroLinea);
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()){
 
				registro.setNombreEpo(						rs.getString("CG_NOMBRE_EPO")  			!= null ?rs.getString("CG_NOMBRE_EPO")				:"");
				registro.setLicitacion(						rs.getString("CG_LICITACION") 			!= null ?rs.getString("CG_LICITACION")				:"");
				registro.setUnidad(							rs.getString("CG_UNIDAD")  				!= null ?rs.getString("CG_UNIDAD")					:"");
				registro.setTipoContratacion(				rs.getString("CG_TIPO_CONTRATACION")	!= null ?rs.getString("CG_TIPO_CONTRATACION")	:"");
				registro.setFechaEmision(					rs.getString("CG_FECHA_EMISION")  		!= null ?rs.getString("CG_FECHA_EMISION")			:"");
				registro.setNumPartida(						rs.getString("CG_NUM_PARTIDA")  			!= null ?rs.getString("CG_NUM_PARTIDA")			:"");
				registro.setImporteSinIVA(					rs.getString("CG_IMPORTE_SIN_IVA")  	!= null ?rs.getString("CG_IMPORTE_SIN_IVA")		:"");
				registro.setRazonSocial(					rs.getString("CG_RAZON_SOCIAL")  		!= null ?rs.getString("CG_RAZON_SOCIAL")			:"");
				registro.setRfc(								rs.getString("CG_RFC")  					!= null ?rs.getString("CG_RFC")						:"");
				registro.setClasificacionDependencia(	rs.getString("CG_CLASIF_DEPENDENCIA")  != null ?rs.getString("CG_CLASIF_DEPENDENCIA")	:"");
 
			}
			rs.close();
			ps.close();
			
			// VALIDAR DATOS

			// 0. Verificar que la linea proporcionada no este vacia
			int numTotalCaracteres	=	0;
			
			int nombreEpoLength 						= registro.getNombreEpo().length();
			int licitacionLength 					= registro.getLicitacion().length();
			int unidadLength 							= registro.getUnidad().length();
			int tipoContratacionLength 			= registro.getTipoContratacion().length();
			int fechaEmisionLength 					= registro.getFechaEmision().length();
			int numPartidaLength 					= registro.getNumPartida().length();
			int importeSinIVALength 				= registro.getImporteSinIVA().length();
			int razonSocialLength 					= registro.getRazonSocial().length();
			int rfcLength 								= registro.getRfc().length();
			int clasificacionDependenciaLength 	= registro.getClasificacionDependencia().length();
 
			numTotalCaracteres += nombreEpoLength;
			numTotalCaracteres += licitacionLength;
			numTotalCaracteres += unidadLength;
			numTotalCaracteres += tipoContratacionLength;
			numTotalCaracteres += fechaEmisionLength;
			numTotalCaracteres += numPartidaLength;
			numTotalCaracteres += importeSinIVALength;
			numTotalCaracteres += razonSocialLength;
			numTotalCaracteres += rfcLength;
			numTotalCaracteres += clasificacionDependenciaLength;
			
			if(numTotalCaracteres == 0) {
				registro.addMensajeError("No hay datos");
			}else{ 
				// 1. Verificar que los campos del registro no excedan la logitud m�xima permitida
				if(nombreEpoLength						>	150) 	registro.addMensajeError("El Campo \"Nombre de EPO\" excede la logitud m�xima permitida");
				if(licitacionLength						>	16) 	registro.addMensajeError("El Campo \"Licitaci�n\" excede la logitud m�xima permitida");
				if(unidadLength							>	200) 	registro.addMensajeError("El Campo \"Unidad\" excede la logitud m�xima permitida");
				if(tipoContratacionLength				>	40) 	registro.addMensajeError("El Campo \"Tipo de Contrataci�n\" excede la logitud m�xima permitida");
				if(fechaEmisionLength					>	10) 	registro.addMensajeError("El Campo \"Fecha de Emisi�n Fallo\" excede la logitud m�xima permitida");
				if(numPartidaLength						>	5) 	registro.addMensajeError("El Campo \"No. de la Partida\" excede la logitud m�xima permitida");
				if(importeSinIVALength					>	18) 	registro.addMensajeError("El Campo \"Importe sin IVA\" excede la logitud m�xima permitida");
				if(razonSocialLength						>	200) 	registro.addMensajeError("El Campo \"Raz�n Social\" excede la logitud m�xima permitida");
				if(rfcLength								>	15) 	registro.addMensajeError("El Campo \"RFC\" excede la logitud m�xima permitida");
				if(clasificacionDependenciaLength	>	1) 	registro.addMensajeError("El Campo \"Clasificaci�n de la Dependencia\" excede la logitud m�xima permitida");
				// 2. Validar campos obligatorios
				if(nombreEpoLength						==	0) 	registro.addMensajeError("El Campo \"Nombre de EPO\" es requerido");
				if(licitacionLength						==	0) 	registro.addMensajeError("El Campo \"Licitaci�n\" es requerido");
				if(unidadLength							==	0) 	registro.addMensajeError("El Campo \"Unidad\" es requerido");
				if(tipoContratacionLength				==	0) 	registro.addMensajeError("El Campo \"Tipo de Contrataci�n\" es requerido");
				if(fechaEmisionLength					==	0) 	registro.addMensajeError("El Campo \"Fecha de Emisi�n Fallo\" es requerido");
				if(numPartidaLength						==	0) 	registro.addMensajeError("El Campo \"No. de la Partida\" es requerido");
				if(importeSinIVALength					==	0) 	registro.addMensajeError("El Campo \"Importe sin IVA\" es requerido");
				if(razonSocialLength						==	0) 	registro.addMensajeError("El Campo \"Raz�n Social\" es requerido");
				if(rfcLength								==	0) 	registro.addMensajeError("El Campo \"RFC\" es requerido");
				if(clasificacionDependenciaLength	==	0) 	registro.addMensajeError("El Campo \"Clasificaci�n de la Dependencia\" es requerido");
				
				// 3. Validar el Tipo de Dato
				// 3.1 Nombre de EPO								Texto
				// 3.2 Licitaci�n									Texto
				// 3.3 Unidad										Texto
				// 3.4 Tipo�de�Contrataci�n					Texto
				// 3.5 Fecha�de�Emision�Fallo					Fecha
				if (fechaEmisionLength	> 0 && fechaEmisionLength  <= 10 ){
					if(!Comunes.esFechaValida(registro.getFechaEmision().toString(), "dd/MM/yyyy")){	
						registro.addMensajeError("El Campo \"Fecha de Emisi�n Fallo\" no posee una fecha valida");
					}else if(!validacionesEspeciales.esAnioActual(registro.getFechaEmision())){
						registro.addMensajeError("El Campo \"Fecha de Emisi�n Fallo\" tiene una fecha que no corresponde al a�o actual");
					}
				}
				// 3.6 No.�de�la�Partida						Numero Entero
				if (numPartidaLength 	> 0 && numPartidaLength    <=  5 && !Comunes.esNumeroEnteroPositivo(registro.getNumPartida().toString())			) 	registro.addMensajeError("El campo: \"No. de la Partida\" no tiene un n�mero v�lido");
				// 3.7 Importe�sin�IVA							Numero Flotante con comas
				if (importeSinIVALength > 0 && importeSinIVALength <= 18 ){
					if(!Comunes.esDecimalPositivo(registro.getImporteSinIVA().toString())){
						registro.addMensajeError("El campo: \"Importe sin IVA\" no tiene un n�mero v�lido");
					}else if(Comunes.excedeCantidadDeEnterosYDecimales(registro.getImporteSinIVA().toString(), 15, 2)){
						registro.addMensajeError("El campo: \"Importe sin IVA\" excede el n�mero permitido de enteros y/o decimales");
					}
				}
				// 3.8 Raz�n�Social								Texto
				// 3.9 RFC											RFC
				if(rfcLength 				> 0 &&  rfcLength          <= 15 && !Comunes.validaRFC(registro.getRfc().toString(),(rfcLength == 15?'F':'M')	)) 	registro.addMensajeError("El campo: \"RFC\" tiene un formato incorrecto");
				// 3.10 Clasificaci�n�de�la�Dependencia	Numero Entero
				if( clasificacionDependenciaLength == 1 ){
					char claveDependencia 	= registro.getClasificacionDependencia().charAt(0);
					if( claveDependencia  != '1' && claveDependencia != '2' )																		registro.addMensajeError("El campo: \"Clasificaci�n de la Dependencia\" no tiene un n�mero v�lido");
				}
				
			}
			
			// Si hay errores, registrar el mensaje de error del registro
			if(registro.getHayError()){
				
				updateRegistro 			= true;
				query.delete(0,query.length()); 		
				query.append(
					"UPDATE                    "  +	
					"	COMTMP_CRUCE_COMPRANET  "  +
					"SET                       "  +
					"	CS_ERROR			= ?,		"  + // "S"
					"	CG_DESC_ERROR	= ?		"  + // registro.getMensajeError().toString()
					"WHERE                     "  +
					"	IC_PROCESO      = ? AND "  + // processID
					"	IC_NUMERO_LINEA = ?		"	  // numeroLinea
				);
				ps = con.queryPrecompilado(query.toString());
				ps.setString(1, 	"S"); // Validacion de Asignacion
				ps.setString(2, 	registro.getMensajeError().toString());
				ps.setInt(3,		processID);
				ps.setInt(4,		numeroLinea);
				ps.executeUpdate();
				ps.close();
				
			}
			
		}catch (Exception e){
			log.error("validaRegistroCompranet(Exception)");
			log.error("validaRegistroCompranet.processID   = <"+processID+">");	
			log.error("validaRegistroCompranet.numeroLinea = <"+numeroLinea+">");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al validar registro en la tabla: COMTMP_CRUCE_COMPRANET");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				if(updateRegistro) con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			//log.info("validaRegistroCompranet(E)");	
		}
	
		if( numeroLinea%350 == 0 ) System.gc();
    
      // System.out.println("memoria usada:" + (Runtime.getRuntime().totalMemory () -  Runtime.getRuntime().freeMemory())); // Debug info
	   	
	}
	
	/**
	 * Agrega al Catalogo de EPO's las EPO's nuevas que se encontraron en la carga con numero de proceso:<tt>numeroProceso</tt>
	 * @param numeroProceso <tt>int</tt> con el numero del proceso de carga.
	 */
	@Override
	public void insertaEposNuevasEnCatalogoCompranet(int numeroProceso)
		throws AppException{
		
		log.info("insertaEposNuevasEnCatalogoCompranet(E)");
		
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;
		
		try {
			
				con.conexionDB();
				
				query = new StringBuffer(); 		
				query.append(
					"INSERT INTO 				"  + 
					"  comcat_epo_compranet "  +
					"  ( 							"  +
					"	 ic_epo_compranet, 	"  + 
					"	 cg_razon_social 		"  +
					"  ) 							"  +
					"SELECT 						"  + 
					"  seq_comcat_epo_compranet.nextval, 			"  + 
					"  a.nombre_epo 			"  +
					"FROM 						"  +
					"  ( 							"  +
					"	 SELECT 					"  + 
					"		DISTINCT cg_nombre_epo AS nombre_epo 	"  +
					"	 FROM 									"  + 
					"		comtmp_cruce_compranet 			"  +
					"	 WHERE 									"  + 
					"		ic_proceso       = ? AND 		"  + // Numero de Proceso
					"		cg_nombre_epo IS NOT NULL AND "  +
					"		cs_error 		  = ? AND    	"  + // 'N'
					"		cg_nombre_epo NOT IN 			"  +
					"		( 										"  + 
					"		  SELECT cg_razon_social FROM comcat_epo_compranet "  +
					"		) 										"  +
					"  ) a "
				);
				ps = con.queryPrecompilado(query.toString());
				ps.setInt(1, 		numeroProceso); // Numero de Proceso
				ps.setString(2, 	"N");
				ps.executeUpdate();
				
				
		}catch(Exception e){
			log.error("insertaEposNuevasEnCatalogoCompranet(Exception)");
			log.error("insertaEposNuevasEnCatalogoCompranet.numeroProceso   = <"+numeroProceso+">");	
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al agregar EPOS nuevas al catalogo de COMCAT_EPO_COMPRANET");
		}finally{
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("insertaEposNuevasEnCatalogoCompranet(S)");
		}
 
	}
 
	/**
	 * Borra las Epos Nuevas de las que no fue completado su proceso de registro, es decir tienen el campo CS_NUEVO = 'S' o 
	 * IG_CLASIF_DEPENDENCIA = 0.
	 */
	@Override
	 public void borraEposNuevasOSinClasificacion()
		throws AppException{
		
		log.info("borraEposNuevasOSinClasificacion(E)");	
			
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;		
		
		try {
			
			con.conexionDB();
			
			query = new StringBuffer();	
			query.append(
				"DELETE FROM COMCAT_EPO_COMPRANET WHERE CS_NUEVO = ? OR IG_CLASIF_DEPENDENCIA = ? "
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,"S");
			ps.setInt(2,	0	);
			ps.executeUpdate();
			ps.close();
			
		}catch (Exception e){
			log.error("borraEposNuevasOSinClasificacion(Exception)");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al depurar el Catalogo de Epos Compranet.");
		}finally{
		 	if(ps != null) 	{ try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("borraEposNuevasOSinClasificacion(E)");	
		}
		
	}
	
	/**
	 * Agrega al Catalogo de PYMES COMPRANET, las PYMES nuevas que se encontraron en la carga con numero de proceso:<tt>numeroProceso</tt>
	 * @param numeroProceso <tt>int</tt> con el numero del proceso de carga.
	 */
	@Override
	public void insertaPymesNuevasEnCatalogoCompranet(int numeroProceso)
		throws AppException{
		
		log.info("insertaPymesNuevasEnCatalogoCompranet(E)");
		
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;
		
		try {
			
				con.conexionDB();
				
				query = new StringBuffer(); 		
				query.append(
					"INSERT INTO  					"  +
					"  COMCAT_PYME_COMPRANET 	"  +
					"  ( 								"  +
					"    IC_PYME_COMPRANET, 	"  +
					"    CG_RAZON_SOCIAL, 		"  +
					"    CG_RFC 					"  +
					"  ) 								"  +
					"SELECT  						"  +
					"  SEQ_COMCAT_PYME_COMPRANET.NEXTVAL, 	"  +
					"  A.CG_RAZON_SOCIAL, 						"  +
					"  A.CG_RFC 									"  +
					"FROM 											"  +
					"  ( 												"  +
					"    SELECT DISTINCT  						"  +
					"      CG_RAZON_SOCIAL, 					"  +
					"      CG_RFC 									"  +
					"    FROM  										"  +
					"      COMTMP_CRUCE_COMPRANET 			"  +
					"    WHERE  									"  +
					"      IC_PROCESO = ? 					"  +
					"      AND CG_RFC NOT  IN 					"  +
					"        ( 										"  +
					"          SELECT CG_RFC FROM COMCAT_PYME_COMPRANET 	"  +
					"        ) 															"  +
					"      AND CS_ERROR = 'N' 										"  +
					"  ) A 																"  
				);
				ps = con.queryPrecompilado(query.toString());
				ps.setInt(1, 	numeroProceso); // Numero de Proceso
				ps.executeUpdate();
				
				
		}catch(Exception e){
			log.error("insertaPymesNuevasEnCatalogoCompranet(Exception)");
			log.error("insertaPymesNuevasEnCatalogoCompranet.numeroProceso   = <"+numeroProceso+">");	
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al agregar PYMES nuevas al catalogo de COMCAT_PYME_COMPRANET");
		}finally{
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("insertaPymesNuevasEnCatalogoCompranet(S)");
		}
 
	}
	
	/**
	 * Asigna Numero de EPO a las EPOs Nuevas en COMCAT_EPO_COMPRANET
	 */
	@Override
	public void asignaNumeroEPOCatalogoCOMPRANET()
		throws AppException{

		log.info("asignaNumeroEPOCatalogoCOMPRANET(E)");	
		
		// Obtener Registro	
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		ResultSet 			rs 				= null;
		PreparedStatement ps					= null;
		PreparedStatement ps1				= null;
		
		boolean				updateRegistro = false;
		
		int numeroEpo 			= 0;
		int numeroEpoMaximo 	= 999999999;
		
		try {
			
			con.conexionDB();
 
			// Obtener Numero de EPO Maximo
			query = new StringBuffer(); 		
			query.append(
				"SELECT 								"  + 
				"	NVL(MAX(IG_NUMERO_EPO),0) 	"  + 
				"		AS NUMERO_EPO 				"  +
				"FROM 								"  +
				"	COMCAT_EPO_COMPRANET 		"  + 
				"WHERE 								"  +
				"	CS_NUEVO = ?					" // 'N' 
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,"N");
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()){
				numeroEpo = rs.getInt("NUMERO_EPO");
			}
			rs.close();
			ps.close();
			
			// Obtener las EPOs que sera actualizadas
			query.delete(0,query.length());
			query.append(
				"SELECT 						"  +
				"	IC_EPO_COMPRANET 		"  +
				"FROM 						"  +
				"	COMCAT_EPO_COMPRANET "  +
				"WHERE 						"  +
				"	CS_NUEVO = ?			" // 'S' 
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,"S");
			rs = ps.executeQuery();
			
			query.delete(0,query.length()); 		
			query.append(
					"UPDATE                    "  +	
					"	COMCAT_EPO_COMPRANET  	"  +
					"SET                       "  +
					"	IG_NUMERO_EPO		= ?	"  +  // nuevoNumeroEpo
					"WHERE                     "  +  
					"	IC_EPO_COMPRANET  = ? 	"     // icEpoCompranet
			);
			ps1 = con.queryPrecompilado(query.toString());
			
			int icEpoCompranet	= 0;
			int nuevoNumeroEpo 		= 0;
			while(rs != null && rs.next()){
				
				updateRegistro = true;
				
				icEpoCompranet = rs.getInt("IC_EPO_COMPRANET");
				nuevoNumeroEpo	= numeroEpo == numeroEpoMaximo?-1:++numeroEpo;
				
				ps1.clearParameters();
				ps1.setInt(1,nuevoNumeroEpo);
				ps1.setInt(2,icEpoCompranet);
				ps1.executeUpdate();
				
			}
			rs.close();
			ps.close();
			ps1.close();
 
		}catch (Exception e){
			log.error("asignaNumeroEPOCatalogoCOMPRANET(Exception)");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al Numero de EPO a las EPOS nuevas en el Catalogo de EPO de COMPRANET");
		}finally{
			if(rs != null) 	{ try { rs.close();}catch(Exception e){} }
		 	if(ps != null) 	{ try { ps.close();}catch(Exception e){} }
			if(ps1 != null) 	{ try { ps1.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				if(updateRegistro) con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("asignaNumeroEPOCatalogoCOMPRANET(E)");	
		}
		
	}
	
	/**
	 * Devuelve una lista con el catalogo de las EPOs de Compranet.
	 * @return <tt>List</tt> con el catalogo de las epos de compranet.
	 */
	@Override
	public List getCatalogoEposCompranet()
		throws AppException{
			return getCatalogoEposCompranet(null);
	}
	
	/**
	 * Devuelve una lista con el catalogo de las EPOs de Compranet.
	 * @param estatus Estatus de la Epo Registrada: "no_nuevos" (epos no nuevas y con clasificacion de dependencia), 
	 *                en cualquier otro caso, se traen ambas epos tanto la nuevas
	 *						como las anteriores.
	 * @return <tt>List</tt> con el catalogo de las epos de compranet.
	 */
	@Override
	public List getCatalogoEposCompranet(String estatus)
		throws AppException{
		
		log.info("getCatalogoEposCompranet(E)");
		
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;
		ResultSet 			rs 				= null;
		
		List					catalogo			= new ArrayList();
		
		try {
			
			con.conexionDB();
				
			query = new StringBuffer(); 		
			query.append(
				"SELECT 							"  + 
				"  IC_EPO_COMPRANET, 		"  +
				"  CG_RAZON_SOCIAL, 			"  +
				"  NVL(IG_NUMERO_EPO,-1) AS NUMERO_EPO, "  +
				"  CS_NUEVO 					"  +
				"FROM 							"  +
				"	COMCAT_EPO_COMPRANET 	"  +
				("no_nuevos".equals(estatus)?" WHERE CS_NUEVO = ? AND IG_CLASIF_DEPENDENCIA != ? ":"") + // CS_NUEVO = 'N',  IG_CLASIF_DEPENDENCIA != 0 
				"ORDER BY 						"  +
				"	CG_RAZON_SOCIAL			"
			);
			ps = con.queryPrecompilado(query.toString());
			if("no_nuevos".equals(estatus)){
				ps.setString(1,"N");
				ps.setInt(2,0);
			}
			rs = ps.executeQuery();
			
			String 				numeroEpo 			= null;
			String 				icEpoCompranet		= null;
			String 				nombreEpo			= null;
			String 				nuevo					= null;
			EpoCompranetBean 	epoCompranetBean 	= null;
			
			while(rs != null && rs.next()){
				
				numeroEpo 		= rs.getString("NUMERO_EPO");
				icEpoCompranet	= rs.getString("IC_EPO_COMPRANET");
				nombreEpo		= rs.getString("CG_RAZON_SOCIAL");
				nuevo				= rs.getString("CS_NUEVO");
				
				numeroEpo 		= numeroEpo 		== null?"":numeroEpo.trim();
				icEpoCompranet	= icEpoCompranet 	== null?"":icEpoCompranet.trim();
				nombreEpo		= nombreEpo 		== null?"":nombreEpo.trim();
				nuevo				= nuevo	 			== null?"":nuevo.trim();
			
				epoCompranetBean = new EpoCompranetBean();
			
				epoCompranetBean.setNumeroEpo(numeroEpo);
				epoCompranetBean.setIcEpoCompranet(icEpoCompranet);
				epoCompranetBean.setNombreEpo(nombreEpo);
				epoCompranetBean.setNuevo(nuevo);
				epoCompranetBean.setNumeroEpoOriginal(numeroEpo);
				
				catalogo.add(epoCompranetBean);
				
			}
			
				
		}catch(Exception e){
			log.error("getCatalogoEposCompranet(Exception)");	
			log.error("getCatalogoEposCompranet.estatus = <"+estatus+">");
			e.printStackTrace();
			catalogo = null;
			throw new AppException("Ocurrio un error al consultar el Catalogo de EPOS COMPRANET");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getCatalogoEposCompranet(S)");
		}
 
		catalogo = catalogo == null || catalogo.size() == 0?null:catalogo;
		
		return catalogo;
		
	}
	
	/**
	 * A todas las Epos nuevas que se encuentren en el catalogo COMCAT_EPO_COMPRANET les
	 * suprime su estatus nuevo.
	 */
	@Override
	public void doSuprimeEstatusNuevoEposCompranet()
		throws AppException{
		
		log.info("doSuprimeEstatusNuevoEposCompranet(E)");	
		
		// Obtener Registro	
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;		
		
		try {
			
			con.conexionDB();
			
			query = new StringBuffer();		
			query.append(
					"UPDATE                    	"  +	
					"	COMCAT_EPO_COMPRANET  		"  +
					"SET                       	"  +
					"	CS_NUEVO				= ?		"  +  // 'N'
					"WHERE                     	"  +  
					"	IG_NUMERO_EPO IS NOT NULL 	"    
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,"N");
			ps.executeUpdate();
			ps.close();
			
		}catch (Exception e){
			log.error("doSuprimeEstatusNuevoEposCompranet(Exception)");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al cambiar estatus de las EPOS nuevas en el Catalogo de EPO de COMPRANET");
		}finally{
		 	if(ps != null) 	{ try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("doSuprimeEstatusNuevoEposCompranet(E)");	
		}
		
	}
 
	/**
	 * Actualiza el Numero de Epo para la EPO cuyo id es: <tt>icEpoCompranet</tt>.
	 * @param icEpoCompranet Id de la EPO.
	 * @param numeroEpoNuevo Numero Numero EPO.
	 */
	@Override
	public void updateNumeroEpoCatalogoCompranet(String icEpoCompranet, String numeroEpoNuevo)
		throws AppException{
		
		log.info("updateNumeroEpoCatalogoCompranet(E)");	
		
		if(icEpoCompranet == null || icEpoCompranet.trim().equals("")) return;
		if(numeroEpoNuevo == null || numeroEpoNuevo.trim().equals("")) return;
		
		// Obtener Registro	
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;		
		
		try {
			
			con.conexionDB();
 
			query = new StringBuffer();		
			query.append(
					"UPDATE                    	"  +	
					"	COMCAT_EPO_COMPRANET  		"  +
					"SET                       	"  +
					"	IG_NUMERO_EPO			= ? 	"  +  // 'N'
					"WHERE                     	"  +  
					"	IC_EPO_COMPRANET 		= ?	"    
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,Integer.parseInt(numeroEpoNuevo));
			ps.setInt(2,Integer.parseInt(icEpoCompranet));
			ps.executeUpdate();
			ps.close();
			
		}catch (Exception e){
			log.error("updateNumeroEpoCatalogoCompranet(Exception)");
			log.error("updateNumeroEpoCatalogoCompranet.numeroEpoNuevo = <"+numeroEpoNuevo+">");
			log.error("updateNumeroEpoCatalogoCompranet.icEpoCompranet = <"+icEpoCompranet+">");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al actualizar el Numero Epo del Catalogo de EPO\\'s de COMPRANET");
		}finally{
		 	if(ps != null) 	{ try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("updateNumeroEpoCatalogoCompranet(E)");	
		}
		
	}
 
	/**
	 * Asigna ID a las EPOs que recien se han cargado en la tabla COMTMP_EPO_COMPRANET
	 */
	@Override
	public void asignaIdEpoCompranet(int procesoID) 
		throws AppException{
			
		log.info("asignaIdEpoCompranet(E)");
		
		AccesoDB 			con 	= new AccesoDB();
		ResultSet 			rs 	= null;
		StringBuffer 		query = null;
		CallableStatement cs 	= null;
		boolean				lbOk	= true;
 		
		try{
			 
			con.conexionDB();

			cs = con.ejecutaSP("SP_ASIGNA_ID_EPO_COMPRANET(?)");
			cs.setInt(1, procesoID);
			rs = cs.executeQuery();
 
		}catch(Exception e) {
			
			log.info("asignaIdEpoCompranet(Exception)");
			log.info("asignaIdEpoCompranet.procesoID = <"+procesoID+">");
			lbOk = false;
			e.printStackTrace();
			throw new AppException("Ocurri� un error al asignar los IDs correspondientes a las EPOS COMPRANET.");
			
		} finally {
			
			if(rs != null) try { rs.close(); }catch(Exception e){}
			if(cs != null) try { cs.close(); }catch(Exception e){}

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbOk);
				con.cierraConexionDB();
			}
			log.info("asignaIdEpoCompranet(S)");
		}

	}
	
	/**
	 * Asigna ID a las PYMES que recien se han cargado en la tabla COMTMP_EPO_COMPRANET
	 */
	@Override
	public void asignaIdPymeCompranet(int procesoID) 
		throws AppException{
			
		log.info("asignaIdPymeCompranet(E)");
		
		AccesoDB 			con 	= new AccesoDB();
		ResultSet 			rs 	= null;
		StringBuffer 		query = null;
		CallableStatement cs 	= null;
		boolean				lbOk	= true;
 		
		try{
			 
			con.conexionDB();

			cs = con.ejecutaSP("SP_ASIGNA_ID_PYME_COMPRANET(?)");
			cs.setInt(1, procesoID);
			rs = cs.executeQuery();
 
		}catch(Exception e) {
			
			log.info("asignaIdPymeCompranet(Exception)");
			log.info("asignaIdPymeCompranet.procesoID = <"+procesoID+">");
			lbOk = false;
			e.printStackTrace();
			throw new AppException("Ocurri� un error al asignar los IDs correspondientes a las PYMES COMPRANET.");
			
		} finally {
			
			if(rs != null) try { rs.close(); }catch(Exception e){}
			if(cs != null) try { cs.close(); }catch(Exception e){}

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbOk);
				con.cierraConexionDB();
			}
			log.info("asignaIdPymeCompranet(S)");
		}

	}
	
	
	/**
	 * Asigna/actualiza la clasificacion de la dependencia de las Epos Compranet
	 */
	@Override
	public void asignaClasificacionDependenciaEpoCompranet(int procesoID) 
		throws AppException{
			
		log.info("asignaClasificacionDependenciaEpoCompranet(E)");
		
		AccesoDB 			con 	= new AccesoDB();
		ResultSet 			rs 	= null;
		StringBuffer 		query = null;
		CallableStatement cs 	= null;
		boolean				lbOk	= true;
 		
		try{
			 
			con.conexionDB();

			cs = con.ejecutaSP("SP_ASIGNA_DEP_EPOS_COMPRANET(?)");
			cs.setInt(1, procesoID);
			rs = cs.executeQuery();
 
		}catch(Exception e) {
			
			log.info("asignaClasificacionDependenciaEpoCompranet(Exception)");
			log.info("asignaClasificacionDependenciaEpoCompranet.procesoID = <"+procesoID+">");
			lbOk = false;
			e.printStackTrace();
			throw new AppException("Ocurri� un error al asignar y/o actualizar la clasificacion de la dependencia de las EPOS COMPRANET.");
			
		} finally {
			
			if(rs != null) try { rs.close(); }catch(Exception e){}
			if(cs != null) try { cs.close(); }catch(Exception e){}

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbOk);
				con.cierraConexionDB();
			}
			log.info("asignaClasificacionDependenciaEpoCompranet(S)");
		}

	}
	
	@Override
	public boolean hayErrorCargaCompranet(int numeroProceso)
		throws AppException{
			
		log.info("hayErrorCargaCompranet(E)");
		boolean 				hayErrores = false;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
 
			con.conexionDB();
			querySentencia.append(
				"SELECT 											"  + 
				"  DECODE(COUNT(1),0,'false','true') 	"  +
				"							AS HAY_ERRORES  	"  +
				"FROM 											"  + 
				"  COMTMP_CRUCE_COMPRANET 					"  +
				"WHERE 											"  + 
				"  IC_PROCESO 		= ? 						"  + // numeroProceso
				"  AND CS_ERROR	= ?						"  + // 'S'
				"  AND ROWNUM     = ?						" // 1
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, 		numeroProceso);
			ps.setString(2, 	"S");
			ps.setInt(3, 	1 );
			rs = ps.executeQuery();
			 
			if(rs != null && rs.next()){
				hayErrores = "true".equals(rs.getString("HAY_ERRORES"))?true:false;
			}
			
		}catch(Exception e){
			log.error("hayErrorCargaCompranet(Exception)");
			log.error("hayErrorCargaCompranet.numeroProceso = "+numeroProceso);
			e.printStackTrace();
			throw new AppException("Error al realizar busqueda de registros con error");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("hayErrorCargaCompranet(S)");	
		}
		
		return hayErrores;
		
	}
	
	/**
	 * Inserta en COM_CRUCE_COMPRANET los registros cargados en COMTMP_CRUCE_COMPRANET, tal que el numero de 
	 * proceso de carga sea: <tt>numeroProceso</tt>. El parametro <tt>reemplazarRegistros</tt> sirve para
	 * indicar si se borrara o no la informacion previa y se reemplazara por una nueva. Solo se realiza la insercion
	 * si ninguno de los registros presento errores.
	 * @param reemplazarRegistros Indica si los registros anteriores se reemplazaran por los nuevos.
	 * @param numeroProceso Numero de Proceso de Carga que se utilizara para tomar los registros que seran
	 * @param mesInicial Mes Inicial a partir del cual se insertaran los registros
	 * @param mesFinal Ultimo mes para el que se insertaran los registros
	 * insertados.
	 */
	@Override
	public void insertaRegistrosEnCruceCompranet(boolean reemplazarRegistros, int numeroProceso, int mesInicial, int mesFinal)
		throws AppException{
		
		log.info("insertaRegistrosEnCruceCompranet(E)");
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = null;
		PreparedStatement ps		 			= null;
		
		boolean				lbOk				= true;
		
		try{
 
			con.conexionDB();
			
         querySentencia = new StringBuffer();
			if(reemplazarRegistros){
				
				String anioActual 		= String.valueOf(Fecha.getAnioActual());
				String fechaInicioMes 	= "01/"+(mesInicial<10?"0":"")+String.valueOf(mesInicial)+"/"+anioActual;
				String fechaFinMes		= "01/"+(mesFinal  <10?"0":"")+String.valueOf(mesFinal)  +"/"+anioActual;
				fechaFinMes					= Fecha.getUltimoDiaDelMes(fechaFinMes,"dd/MM/yyyy");
			
				querySentencia.append(
					"DELETE FROM 					"  +
					"	COM_CRUCE_COMPRANET 		"  +
					"WHERE 							"  +
					"  DF_FECHA_EMISION 		 >= TO_DATE(?,'DD/MM/YYYY') 	"  + // mes inicial
					"  AND DF_FECHA_EMISION  <  TO_DATE(?,'DD/MM/YYYY')+1	"   // mes final
				);
				
				ps = con.queryPrecompilado(querySentencia.toString());
				ps.setString(1, fechaInicioMes);
				ps.setString(2, fechaFinMes);
				ps.executeUpdate();
				ps.close();
				
			}
			
			querySentencia.delete(0,querySentencia.length());
			querySentencia.append(
				"INSERT INTO							"  + 
				"  COM_CRUCE_COMPRANET 				"  +
				"		( 									"  +
				"			IC_EPO_COMPRANET,			"  + //	NUMBER(9)   
				"			CG_LICITACION, 			"  + //	VARCHAR2(16)  
				"			CG_UNIDAD, 					"  + //	VARCHAR2(200)  
				"			CG_TIPO_CONTRATACION, 	"  + //	VARCHAR2(40)  
				"			DF_FECHA_EMISION,			"  + //	DATE  
				"			CG_NUM_PARTIDA,			"  + //	NUMBER(5) 
				"			FN_IMPORTE_SIN_IVA,		"  + //	NUMBER(17,2) 
				"			IC_PYME_COMPRANET,		"  + //	NUMBER(9) 
				"			IG_CLASIF_DEPENDENCIA	"  + //	NUMBER(2) 
				"		) 									"  +
				"SELECT 									"  +
				"		IC_EPO_COMPRANET, 			"  +
				"		CG_LICITACION,   	 			"  +
				"		CG_UNIDAD, 						"  +
				"		CG_TIPO_CONTRATACION,		"  +
				"		TO_DATE(CG_FECHA_EMISION,'DD/MM/YYYY'), 	"  +
				" 		TO_NUMBER(CG_NUM_PARTIDA), 					"  +
				"		TO_NUMBER(CG_IMPORTE_SIN_IVA), 				"  +
				"		IC_PYME_COMPRANET, 								"  +
				"		TO_NUMBER(CG_CLASIF_DEPENDENCIA) 			"  +
				"FROM 														"  + 
				"  COMTMP_CRUCE_COMPRANET 								"  +
				"WHERE 														"  + 
				"  IC_PROCESO = ? 										" 
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, 		numeroProceso);
			ps.executeUpdate();
			
		}catch(Exception e){
			log.error("insertaRegistrosEnCruceCompranet(Exception)");
			log.error("insertaRegistrosEnCruceCompranet.reemplazarRegistros 	= <"+reemplazarRegistros+">");
			log.error("insertaRegistrosEnCruceCompranet.numeroProceso 			= <"+numeroProceso+">");
			log.error("insertaRegistrosEnCruceCompranet.mesInicial 				= <"+mesInicial+">");
			log.error("insertaRegistrosEnCruceCompranet.mesFinal 					= <"+mesFinal+">");
			e.printStackTrace();
			lbOk = false;
			throw new AppException("Error al insertar registros en COM_CRUCE_COMPRANET.");
		}finally{
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
		 		con.terminaTransaccion(lbOk);
				con.cierraConexionDB();
			}
			log.info("insertaRegistrosEnCruceCompranet(S)");	
		}
		
	}
	
	/**
	 * Devuelve un <tt>StringBuffer</tt> con la lista de los mensajes de error
	 * que arrojo el proceso de validacion.
	 * @param numeroProceso Numero de Proceso de la Carga.
	 */
	@Override
	public StringBuffer getMensajeErrorCargaCompranet(int numeroProceso)
		throws AppException{
		
		log.info("getMensajeErrorCargaCompranet(E)");
		StringBuffer 		mensajeDeError = new StringBuffer(4096000);
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		int					NUMERO_MAXIMO_LINEAS	= 2000;
		int 					contadorLineasError	= 0;
		try{
 
			con.conexionDB();
			querySentencia.append(
				"SELECT 													"  + 
				"	IC_NUMERO_LINEA_REAL AS NUMERO_LINEA,		"  +
				"  CG_DESC_ERROR   		AS DESCRIPCION_ERROR	"  +
				"FROM 													"  + 
				"  COMTMP_CRUCE_COMPRANET 							"  +
				"WHERE 													"  + 
				"  IC_PROCESO 		= ? 								"  + // numeroProceso
				"  AND CS_ERROR	= ?								"  + // 'S'
				"ORDER BY IC_NUMERO_LINEA ASC						"
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, 		numeroProceso);
			ps.setString(2, 	"S");
			rs = ps.executeQuery();
			 
			String mensaje 		= null;
			String numeroLinea 	= null;
			while(rs != null && rs.next()){
				
				contadorLineasError++;
				
				if(contadorLineasError > NUMERO_MAXIMO_LINEAS){
					mensajeDeError.append("S�lo se muestran las primeras "+NUMERO_MAXIMO_LINEAS+" l�neas con error.\r\n");
					break;
				}
				
				// Agregar mensaje de error
				mensajeDeError.append("L�nea ");
				mensajeDeError.append(rs.getString("NUMERO_LINEA"));
				mensajeDeError.append(": ");
				mensajeDeError.append(rs.getString("DESCRIPCION_ERROR"));
				mensajeDeError.append("\r\n");
 
			}
			
		}catch(Exception e){
			log.error("getMensajeErrorCargaCompranet(Exception)");
			log.error("getMensajeErrorCargaCompranet.numeroProceso = "+numeroProceso);
			e.printStackTrace();
			throw new AppException("Error al consultar los registros con mensaje de error.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getMensajeErrorCargaCompranet(S)");	
		}
		
		return mensajeDeError;
		
	}
 
	/**
	 * Devuelve un <tt>String</tt> con la descripcion de los campos adicionales de la tabla COM_CRUCE_SIAG
	 */
	@Override
	public String getCamposAdicionalesSiag()
		throws AppException{
		
		log.info("getCamposAdicionalesSiag(E)");
		String 				camposAdicionales = null;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
 
			con.conexionDB();
			querySentencia.append(
				"SELECT 										"  + 
				"	CG_DESCRIPCION	AS DESCRIPCION		"  +
				"FROM 										"  + 
				"  COM_CAMPOS_ADIC_SIAG 				"  +
				"WHERE 										"  + 
				"  IC_NUMERO_LINEA = ? 					"   // 1 - Numero de Linea
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, 		1);
			rs = ps.executeQuery();
			 
			if(rs != null && rs.next()){
				camposAdicionales = rs.getString("DESCRIPCION");
			}
			
		}catch(Exception e){
			log.error("getCamposAdicionalesSiag(Exception)");
			e.printStackTrace();
			throw new AppException("Error al consultar la descripcion de los campos adicionales de la tabla COM_CRUCE_SIAG.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getCamposAdicionalesSiag(S)");	
		}
		
		return camposAdicionales;
		
	}
	
	/**
	 * Devuelve un <tt>StringBuffer</tt> con la descripcion de los Campos de SIAG
	 */
	@Override
	public StringBuffer getDescripcionCamposSiag()
		throws AppException{
		
		log.info("getDescripcionCamposSiag(E)");
		StringBuffer 		descripcion = new StringBuffer();
		
		try{

			descripcion.append(
				"		<table cellpadding=\"3\" cellspacing=\"0\" border=\"1\" bordercolor=\"#A5B8BF\" aling =\"center\"  style=\"width:455px;\">\n"  +
				"			<tr>\n"  +
				"				<td class=\"celda01\" align=\"center\" width=\"25px\" >No.&nbsp;de&nbsp;Campo</td>\n"  +
				"				<td class=\"celda01\" align=\"center\">Descripci&oacute;n</td>\n"  +
				"				<td class=\"celda01\" align=\"center\">Tipo&nbsp;de&nbsp;Dato</td>\n"  +
				"				<td class=\"celda01\" align=\"center\">Longitud</td>\n"  +
				"				<td class=\"celda01\" align=\"center\">Obligatorio</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >1</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Raz�n Social\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Texto</td>\n"  +
				"				<td class=\"formas\" align=\"center\">90</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >2</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					RFC\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Alfanum&eacute;rico</td>\n"  +
				"				<td class=\"formas\" align=\"center\">15</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >3</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Estado\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Texto</td>\n"  +
				"				<td class=\"formas\" align=\"center\">50</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >4</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Monto&nbsp;Actual&nbsp;del&nbsp;Cr&eacute;dito\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Num&eacute;rico</td>\n"  +
				"				<td class=\"formas\" align=\"center\">15,3</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >5</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Plazo\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Num&eacute;rico</td>\n"  +
				"				<td class=\"formas\" align=\"center\">3</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >6</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Intermediario\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Texto</td>\n"  +
				"				<td class=\"formas\" align=\"center\">90</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >7</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Estrato&nbsp;Inicial\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Num&eacute;rico</td>\n"  +
				"				<td class=\"formas\" align=\"center\">3</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >8</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Nuevo&nbsp;Estrato\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Num&eacute;rico</td>\n"  +
				"				<td class=\"formas\" align=\"center\">3</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >9</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Sector\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Num&eacute;rico</td>\n"  +
				"				<td class=\"formas\" align=\"center\">3</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >10</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Fecha&nbsp;de&nbsp;Registro\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Fecha</td>\n"  +
				"				<td class=\"formas\" align=\"center\">dd/mm/aaaa</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >11</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Descripci&oacute;n&nbsp;del&nbsp;Portafolio\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Texto</td>\n"  +
				"				<td class=\"formas\" align=\"center\">30</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >12</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Clave CONREC\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Num&eacute;rico</td>\n"  +
				"				<td class=\"formas\" align=\"center\">3</td>\n"  +
				"				<td class=\"formas\" align=\"center\">S&iacute;</td>\n"  +
				"			</tr>		\n"  +
				"			<tr>\n"  +
				"				<td class=\"formas\" align=\"center\" >13</td>\n"  +
				"				<td class=\"formas\" align=\"center\">\n"  +
				"					Campos Adicionales\n"  +
				"				</td>\n"  +
				"				<td class=\"formas\" align=\"left\">Texto</td>\n"  +
				"				<td class=\"formas\" align=\"center\">4000</td>\n"  +
				"				<td class=\"formas\" align=\"center\">No</td>\n"  +
				"			</tr>\n"
			);
			 
			String cabecera 	= "";
			cabecera 			= getCamposAdicionalesSiag();
			String campo 		= null;
			if(cabecera != null){
				String []array = cabecera.split(",");
				for(int i=0;i<array.length;i++){
					descripcion.append(
						"			<tr>\n"  +
						"				<td class=\"formas\" align=\"center\" >13."
					);
					descripcion.append(String.valueOf(i+1));
					descripcion.append(
						"</td>\n"  +
						"				<td class=\"formas\" align=\"center\">\n"  +
						"					"
					);
					campo = array[i];
					campo = campo == null || campo.trim().equals("")?"&nbsp;":campo;
					campo = campo.replaceAll("<","&lt;");
					campo = campo.replaceAll(">","&gt;");
					descripcion.append(campo);
					descripcion.append(
						"\n"  +
						"				</td>\n"  +
						"				<td class=\"formas\" align=\"left\">Texto</td>\n"  +
						"				<td class=\"formas\" align=\"center\">variable</td>\n"  +
						"				<td class=\"formas\" align=\"center\">No</td>\n"  +
						"			</tr>\n"
					);
				}
				
			}
			descripcion.append(
				"		</table>\n"  +
				"		<table  cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\" style=\"width:455px;\">\n"  +
				"			<tr>\n"  +
				"				<td align=\"right\" width=\"100%\">\n"  +
				"					<a href=\"JavaScript:ocultarTabla('Layout');\">\n"  +
				"						<img src=\"/nafin/00utils/gif/icerrar.gif\" border=\"0\" width=\"13\">\n"  +
				"					</a>\n"  +
				"				</td>\n"  +
				"			</tr>\n"  +
				"		</table>\n"
			);
			
		}catch(Exception e){
			log.error("getDescripcionCamposSiag(Exception)");
			e.printStackTrace();
			throw new AppException("Error al generar descripcion de Campos de la Carga Masiva de la Base de Datos de Compranet.");
		}finally{
			log.info("getDescripcionCamposSiag(S)");	
		}
		
		return descripcion;
		
	}
	
	/**
	 * Guarda la descripci�n de los Campos Adicionales
	 * @param camposAdicionales Cadena de Texto con la descripcion de los campos adicionales
	 */
	@Override
	public void guardarCamposAdicionalesSiag(String camposAdicionales)
		throws AppException{
			
		log.info("guardarCamposAdicionalesSiag(E)");
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		boolean 				lbOk				= true;
		PreparedStatement ps		 			= null;
		
		try{
 
			con.conexionDB();
			
			camposAdicionales = camposAdicionales == null?"":camposAdicionales.trim();
			
			querySentencia.append(
				"UPDATE 							"  +
				"  COM_CAMPOS_ADIC_SIAG 	"  +
				"SET 								"  +
				"	CG_DESCRIPCION	 = ?		"  + // descripcion de los campos adicionales
				"WHERE 							"  + 
				"  IC_NUMERO_LINEA = ? 		"    // 1 - Numero de Linea
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setString(1, 	 camposAdicionales);
			ps.setInt(2, 		1);
			ps.executeUpdate();
			 
		}catch(Exception e){
			log.error("guardarCamposAdicionalesSiag(Exception)");
			lbOk = false;
			e.printStackTrace();
			throw new AppException("Error al guardar la descripcion nueva de los campos adicionales de la tabla COM_CRUCE_SIAG.");
		}finally{
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOk);
				con.cierraConexionDB();
			}
			log.info("guardarCamposAdicionalesSiag(S)");	
		}
			
	}

	/**
	* Revisa si hay registros en la tabla: COM_CRUCE_SIAG para el mes indicado
	* en el campo: <tt>mes</tt>
	* @param mes 	Numero del Mes
	* @return <tt>boolean</tt> con valor <tt>true</tt> si se encontraron registros.
	*			 <tt>false</tt> en caso contrario.
	*/
	@Override
	public boolean existenRegistrosEnSiag(int mes)
		throws AppException{
			
		log.info("existenRegistrosEnSiag(E)");
		boolean 				existeRegistro = false;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			String anioActual 		= String.valueOf(Fecha.getAnioActual());
			String fechaInicioMes 	= "01/"+(mes<10?"0":"")+String.valueOf(mes)+"/"+anioActual;
			String fechaFinMes		= Fecha.getUltimoDiaDelMes(fechaInicioMes,"dd/MM/yyyy");
			
			con.conexionDB();
			querySentencia.append(
				"SELECT 																"  + 
				"  DECODE(COUNT(1),0,'false','true') AS HAY_REGISTROS "  + 
				"FROM 																"  + 
				"  COM_CRUCE_SIAG	 												"  +
				"WHERE 																"  + 
				"  ROWNUM = ? AND 												"  + // 1
				"  DF_FECHA_REGISTRO 	 >= TO_DATE(?,'DD/MM/YYYY') 	"  + // fechaInicioMes
				"  AND DF_FECHA_REGISTRO <  TO_DATE(?,'DD/MM/YYYY')+1	"   // fechaFinMes
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, 	1 );
			ps.setString(2, fechaInicioMes);
			ps.setString(3, fechaFinMes);
			rs = ps.executeQuery();
			 
			if(rs != null && rs.next()){
				existeRegistro = "true".equals(rs.getString("HAY_REGISTROS"))?true:false;
			}
			
		}catch(Exception e){
			log.error("existenRegistrosEnSiag(Exception)");
			log.error("existenRegistrosEnSiag.mes = "+mes);
			e.printStackTrace();
			throw new AppException("Error al realizar busqueda en SIAG");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("existenRegistrosEnSiag(S)");	
		}
		
		return existeRegistro;
	}
	
	/**
	 * Devuelve una lista con el resultado de la busqueda de registros para cada uno
	 * de los meses comprendidos a patir del mes: <tt>mesInicial</tt> y hasta el mes: 
	 * <tt>mesFinal</tt> para el a�o en curso.
	 *
	 * @param mesInicial Numero del mes inicial
	 * @param mesFinal Numero del mes final
	 * @return <tt>List</tt> con el resultado de las busquedas de los meses 
	 * especificados.
	 */
	@Override
	public List getBusquedaRegistrosEnSiag(int mesInicial, int mesFinal)
		throws AppException{
		
		log.info("getBusquedaRegistrosEnSiag(E)");
		List busqueda = new ArrayList();
    
		try {
			
			
			if(mesFinal < mesInicial){
				log.info("getBusquedaRegistrosEnSiag(S)");
				return busqueda;
			}
			
			HashMap registro 			= null;
			boolean existeRegistro 	= false;
			for(int mes=mesInicial;mes<=mesFinal;mes++){
				
				registro 		= new HashMap();
				existeRegistro = existenRegistrosEnSiag(mes);
				registro.put("MES",					String.valueOf(mes));
				registro.put("EXISTE_REGISTRO",	String.valueOf(existeRegistro));
				
				busqueda.add(registro);
			}
			
		}catch(Exception e){
			log.error("getBusquedaRegistrosEnSiag(Exception)");
			log.error("getBusquedaRegistrosEnSiag.mesInicial = "+mesInicial);
			log.error("getBusquedaRegistrosEnSiag.mesFinal   = "+mesFinal);
			e.printStackTrace();
			throw new AppException("Error al buscar registros en SIAG");
		}
		log.info("getBusquedaRegistrosEnSiag(S)");
		return busqueda;
	}

	/**
	 * Devuelve el ID del Proceso de Carga de la Base de Datos de Siag
	 * @return <tt>String</tt> con el ID.
	 */
	@Override
	public String getIdCargaSiag() 
		throws AppException{
		
		log.info("getIdCargaSiag(E)");
		String idProceso = null;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			con.conexionDB();
			querySentencia.append(
				"SELECT SEQ_COMTMP_CRUCE_SIAG.NEXTVAL AS ID_PROCESO FROM DUAL"
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			rs = ps.executeQuery();
			 
			if(rs != null && rs.next()){
				idProceso = rs.getString("ID_PROCESO");
			}
			
		}catch(Exception e){
			log.error("getIdCargaSiag(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener ID del Proceso de Carga de la Base de Datos de SIAG");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getIdCargaSiag(S)");	
		}
		
		return idProceso;
	}
	
	/**
	 * Valida el registro cargado en la tabla COMTMP_CRUCE_SIAG tal que tenga el numero de proceso <tt>processID</tt> 
	 * y el numero de linea: <tt>numeroLinea</tt>
	 */
	@Override
	public void validaRegistroSiag(int processID, int numeroLinea, RegistroTmpSiag registro,ValidacionesEspeciales validacionesEspeciales) 
		throws AppException{

		//log.info("validaRegistroSiag(E)");	
		
		// Obtener Registro	
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		ResultSet 			rs 				= null;
		PreparedStatement ps					= null;
		
		boolean				updateRegistro 							= false;
		boolean				camposAdicionalesExcedenLongitud		= false;
		try {
			
			con.conexionDB();
			
			// Borrar los datos anteriores
			registro.doReset();
			camposAdicionalesExcedenLongitud = false;
			
			// Leer Datos del Registro
			query = new StringBuffer(); 		
			query.append(
				"SELECT                    	"  +
				"	CG_RAZON_SOCIAL,				"  +
				"	CG_RFC,							"  +
				"	CG_ESTADO,						"  +
				"	CG_MONTO_ACTUAL_CRED,		"  +
				"	CG_PLAZO,						"  +
				"	CG_INTERNEDIARIOF,			"  +
				"	CG_ESTRATO_INI,				"  +
				"	CG_ESTRATO_NUEVO,				"  +
				"	CG_SECTOR,						"  +
				"	CG_FECHA_REGISTRO,			"  +
				"	CG_PORTAFOLIO,					"  +
				"	CG_CONREC_CLAVE,				"  +
				"	CG_CAMPOS_ADICIONALES,		"  +
				"	CS_ERROR_LONG_CADICIONALES	"  +
		 		"FROM                      	" 	+
				"	COMTMP_CRUCE_SIAG  			" 	+
				"WHERE                     	" 	+
				"	IC_PROCESO      = ? AND 	" 	+ // processID
				"	IC_NUMERO_LINEA = ?			"    // Numero de Linea
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,processID);
			ps.setInt(2,numeroLinea);
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()){
 
				registro.setRazonSocial(					rs.getString("CG_RAZON_SOCIAL") 			!= null?rs.getString("CG_RAZON_SOCIAL")		:"");
				registro.setRfc(								rs.getString("CG_RFC") 						!= null?rs.getString("CG_RFC")					:"");
				registro.setEstado(							rs.getString("CG_ESTADO") 					!= null?rs.getString("CG_ESTADO")				:"");
				registro.setMontoActualCredito(			rs.getString("CG_MONTO_ACTUAL_CRED") 	!= null?rs.getString("CG_MONTO_ACTUAL_CRED")	:"");
				registro.setPlazo(							rs.getString("CG_PLAZO") 					!= null?rs.getString("CG_PLAZO")					:"");
				registro.setIntermediario(					rs.getString("CG_INTERNEDIARIOF") 		!= null?rs.getString("CG_INTERNEDIARIOF")		:"");
				registro.setEstratoInicial(				rs.getString("CG_ESTRATO_INI") 			!= null?rs.getString("CG_ESTRATO_INI")			:"");
				registro.setNuevoEstrato(					rs.getString("CG_ESTRATO_NUEVO") 		!= null?rs.getString("CG_ESTRATO_NUEVO")		:"");
				registro.setSector(							rs.getString("CG_SECTOR") 					!= null?rs.getString("CG_SECTOR")				:"");
				registro.setFechaRegistro(					rs.getString("CG_FECHA_REGISTRO") 		!= null?rs.getString("CG_FECHA_REGISTRO")		:"");
				registro.setDescripcionPortafolio(		rs.getString("CG_PORTAFOLIO") 			!= null?rs.getString("CG_PORTAFOLIO")			:"");
				registro.setClaveCONREC(					rs.getString("CG_CONREC_CLAVE") 			!= null?rs.getString("CG_CONREC_CLAVE")		:"");
				registro.setCamposAdicionales(			rs.getString("CG_CAMPOS_ADICIONALES") 	!= null?rs.getString("CG_CAMPOS_ADICIONALES"):"");
				camposAdicionalesExcedenLongitud 		= "S".equals(rs.getString("CS_ERROR_LONG_CADICIONALES"))?true:false;
 
			}
			rs.close();
			ps.close();
			
			// VALIDAR DATOS
			// 0. Verificar que la linea proporcionada no este vacia
			int numTotalCaracteres					=	0;
			
			int razonSocialLength 					= registro.getRazonSocial().length();
			int rfcLength 								= registro.getRfc().length();
			int estadoLength 							= registro.getEstado().length();
			int montoActualCreditoLength 			= registro.getMontoActualCredito().length();
			int plazoLength 							= registro.getPlazo().length();
			int intermediarioLength 				= registro.getIntermediario().length();
			int estratoInicialLength 				= registro.getEstratoInicial().length();
			int nuevoEstratoLength 					= registro.getNuevoEstrato().length();
			int sectorLength 							= registro.getSector().length();
			int fechaRegistroLength 				= registro.getFechaRegistro().length();
			int descripcionPortafolioLength 		= registro.getDescripcionPortafolio().length();
			int claveCONRECLength 					= registro.getClaveCONREC().length();
			int camposAdicionalesLength 			= registro.getCamposAdicionales().length();
 
			numTotalCaracteres += razonSocialLength;
			numTotalCaracteres += rfcLength;
			numTotalCaracteres += estadoLength;
			numTotalCaracteres += montoActualCreditoLength;
			numTotalCaracteres += plazoLength;
			numTotalCaracteres += intermediarioLength;
			numTotalCaracteres += estratoInicialLength;
			numTotalCaracteres += nuevoEstratoLength;
			numTotalCaracteres += sectorLength;
			numTotalCaracteres += fechaRegistroLength;
			numTotalCaracteres += descripcionPortafolioLength;
			numTotalCaracteres += claveCONRECLength;
			numTotalCaracteres += camposAdicionalesLength;
			
			if(numTotalCaracteres == 0) {
				registro.addMensajeError("No hay datos");
			}else{ 
				// 1. Verificar que los campos del registro no excedan la logitud m�xima permitida
				if(razonSocialLength 					> 90) 	registro.addMensajeError("El Campo \"Raz�n Social\" excede la longitud m�xima permitida");
				if(rfcLength 								> 15) 	registro.addMensajeError("El Campo \"RFC\" excede la longitud m�xima permitida");
				if(estadoLength 							> 50) 	registro.addMensajeError("El Campo \"Estado\" excede la longitud m�xima permitida");
				if(montoActualCreditoLength 			> 19) 	registro.addMensajeError("El Campo \"Monto Actual del Cr�dito\" excede la longitud m�xima permitida");
				if(plazoLength 							> 3) 		registro.addMensajeError("El Campo \"Plazo\" excede la longitud m�xima permitida");
				if(intermediarioLength 					> 90) 	registro.addMensajeError("El Campo \"Intermediario\" excede la longitud m�xima permitida");
				if(estratoInicialLength 				> 3) 		registro.addMensajeError("El Campo \"Estrato Inicial\" excede la longitud m�xima permitida");
				if(nuevoEstratoLength 					> 3) 		registro.addMensajeError("El Campo \"Nuevo Estrato\" excede la longitud m�xima permitida");
				if(sectorLength 							> 3) 		registro.addMensajeError("El Campo \"Sector\" excede la longitud m�xima permitida");
				if(fechaRegistroLength 					> 10) 	registro.addMensajeError("El Campo \"Fecha de Registro\" excede la longitud m�xima permitida");
				if(descripcionPortafolioLength 		> 30) 	registro.addMensajeError("El Campo \"Descripci�n del Portafolio\" excede la longitud m�xima permitida");
				if(claveCONRECLength 					> 3) 		registro.addMensajeError("El Campo \"Clave CONREC\" excede la longitud m�xima permitida");
				if(camposAdicionalesExcedenLongitud		) 		registro.addMensajeError("El Campo \"Campos Adicionales\" excede la longitud m�xima permitida");
				// 2. Validar campos obligatorios
				if(razonSocialLength 					== 0) registro.addMensajeError("El Campo \"Raz�n Social\" es requerido");
				if(rfcLength 								== 0) registro.addMensajeError("El Campo \"RFC\" es requerido");
				if(estadoLength 							== 0) registro.addMensajeError("El Campo \"Estado\" es requerido");
				if(montoActualCreditoLength 			== 0) registro.addMensajeError("El Campo \"Monto Actual del Cr�dito\" es requerido");
				if(plazoLength 							== 0) registro.addMensajeError("El Campo \"Plazo\" es requerido");
				if(intermediarioLength 					== 0) registro.addMensajeError("El Campo \"Intermediario\" es requerido");
				if(estratoInicialLength 				== 0) registro.addMensajeError("El Campo \"Estrato Inicial\" es requerido");
				if(nuevoEstratoLength 					== 0) registro.addMensajeError("El Campo \"Nuevo Estrato\" es requerido");
				if(sectorLength 							== 0) registro.addMensajeError("El Campo \"Sector\" es requerido");
				if(fechaRegistroLength 					== 0) registro.addMensajeError("El Campo \"Fecha de Registro\" es requerido");
				if(descripcionPortafolioLength 		== 0) registro.addMensajeError("El Campo \"Descripci�n del Portafolio\" es requerido");
				if(claveCONRECLength 					== 0) registro.addMensajeError("El Campo \"Clave CONREC\" es requerido");
				//if(camposAdicionalesLength 			== 0) registro.addMensajeError("El Campo \"Campos Adicionales\" es requerido");
 
				// 3. Validar el Tipo de Dato
				// 3.1 	Raz�n Social  						Texto 		 
				// 3.2 	RFC  									Alfanum�rico
				/* 
				NOTA: Se suprimio la validacion del formato para que se pueda cargar cualquier tipo de RFCs
				if(rfcLength 				> 0 && rfcLength <= 15 && !validaRFCSiag(registro.getRfc().toString(),( (rfcLength == 15 || rfcLength == 12)?'F':'M'))){ 	
					registro.addMensajeError("El campo: \"RFC\" tiene un formato incorrecto");
				}
				*/
				// 3.3 	Estado  								Texto 		 
				// 3.4 	Monto Actual del Cr�dito  		Num�rico 15,3
				if (montoActualCreditoLength > 0 && montoActualCreditoLength <= 19 ){
					if(!Comunes.esDecimalPositivo(registro.getMontoActualCredito().toString())){
						registro.addMensajeError("El campo: \"Monto Actual del Cr�dito\" no tiene un n�mero v�lido");
					}else if(Comunes.excedeCantidadDeEnterosYDecimales(registro.getMontoActualCredito().toString(), 15, 3)){
						registro.addMensajeError("El campo: \"Monto Actual del Cr�dito\" excede el n�mero permitido de enteros y/o decimales");
					}
				}
				// 3.5 	Plazo  								Num�rico
				if (plazoLength > 0 && plazoLength <= 3 && !Comunes.esNumeroEnteroPositivo(registro.getPlazo().toString())){ 	
					registro.addMensajeError("El campo: \"Plazo\" no tiene un n�mero v�lido");
				}
				// 3.6 	Intermediario  					Texto  
				// 3.7 	Estrato Inicial  					Num�rico
				if (estratoInicialLength > 0 && estratoInicialLength <= 3 && !Comunes.esNumeroEnteroPositivo(registro.getEstratoInicial().toString())){ 	
					registro.addMensajeError("El campo: \"Estrato Inicial\" no tiene un n�mero v�lido");
				}
				// 3.8 	Nuevo Estrato  					Num�rico
				if (nuevoEstratoLength > 0 && nuevoEstratoLength <= 3 && !Comunes.esNumeroEnteroPositivo(registro.getNuevoEstrato().toString())){ 	
					registro.addMensajeError("El campo: \"Nuevo Estrato\" no tiene un n�mero v�lido");
				}
				// 3.9 	Sector  								Num�rico
				if (sectorLength > 0 && sectorLength <= 3 && !Comunes.esNumeroEnteroPositivo(registro.getSector().toString())){ 	
					registro.addMensajeError("El campo: \"Sector\" no tiene un n�mero v�lido");
				}
				// 3.10 	Fecha de Registro  				dd/mm/aaaa 
				if (fechaRegistroLength	>	0 && fechaRegistroLength <= 10  ){
					if(!Comunes.esFechaValida(registro.getFechaRegistro().toString(), "dd/MM/yyyy")){
						registro.addMensajeError("El Campo \"Fecha de Registro\" no posee una fecha valida");
					}else if(!validacionesEspeciales.esAnioActual(registro.getFechaRegistro())){
						registro.addMensajeError("El Campo \"Fecha de Registro\" tiene una fecha que no corresponde al a�o actual");
					}
				}
				// 3.11 	Descripci�n del Portafolio  	Texto  
				// 3.12 	Clave CONREC  						Num�rico 
				if (claveCONRECLength > 0 && claveCONRECLength <= 3 && !Comunes.esNumeroEnteroPositivo(registro.getClaveCONREC().toString())){ 	
					registro.addMensajeError("El campo: \"Clave CONREC\" no tiene un n�mero v�lido");
				}
				// 3.13 	Campos Adicionales  				Texto 
 
			}
			
			// Si hay errores, registrar el mensaje de error del registro
			if(registro.getHayError()){
				
				updateRegistro 			= true;
				query.delete(0,query.length()); 		
				query.append(
					"UPDATE                    "  +	
					"	COMTMP_CRUCE_SIAG 		"  +
					"SET                       "  +
					"	CS_ERROR			= ?,		"  + // "S"
					"	CG_DESC_ERROR	= ?		"  + // registro.getMensajeError().toString()
					"WHERE                     "  +
					"	IC_PROCESO      = ? AND "  + // processID
					"	IC_NUMERO_LINEA = ?		"	  // numeroLinea
				);
				ps = con.queryPrecompilado(query.toString());
				ps.setString(1, 	"S"); // Validacion de Asignacion
				ps.setString(2, 	registro.getMensajeError().toString());
				ps.setInt(3,		processID);
				ps.setInt(4,		numeroLinea);
				ps.executeUpdate();
				ps.close();
				
			}
			
		}catch (Exception e){
			log.error("validaRegistroCompranet(Exception)");
			log.error("validaRegistroCompranet.processID   = <"+processID+">");	
			log.error("validaRegistroCompranet.numeroLinea = <"+numeroLinea+">");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al validar registro en la tabla: COMTMP_CRUCE_COMPRANET");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				if(updateRegistro) con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			//log.info("validaRegistroCompranet(E)");	
		}
	
		if( numeroLinea%350 == 0 ) System.gc();
    
      // System.out.println("memoria usada:" + (Runtime.getRuntime().totalMemory () -  Runtime.getRuntime().freeMemory())); // Debug info
	   	
	}
	
	/**
	* Este metodo verifica si el RFC proporcionado en <code>strRFC</code> es valido, dependiendo
	* del tipo de persona indicada. Esta validacion tiene la peculiaridad de que acepta RFC's sin homoclave
	* como validos.
	* NOTA: Actualmente esta validacion ya no se utiliza.
	* <p>
	* Este metodo no sirve para validar RFC internacionales
	* <p>
	* @param  strRFC  		Cadena de texto con el RFC a verificar
	* @param  tipoPersona 	Tipo de persona para el cual se validara el RFC, puede tomar los siguientes
	*								valores: <code>'M'</code> para persona moral, <code>'F'</code> para persona fisica.	
	* @return boolean     	Regresa un valor de tipo <code>boolean</code>, siendo <code>true</code> si el 
	*								RFC proporcionado es valido o <code>false</code> en caso contrario
	*/
	private boolean validaRFCSiag(String strRFC, char tipoPersona){
		if( strRFC == null ) return false;
		
		// Extraer campos
		StringTokenizer 	stoken			= new StringTokenizer(strRFC,"-");
		String 				clvNombre 		= (stoken.hasMoreElements())?stoken.nextToken():"";
		String 				fecha	 			= (stoken.hasMoreElements())?stoken.nextToken():"";
		String 				homoclave		= (stoken.hasMoreElements())?stoken.nextToken():"";
		boolean 				hayHomoclave 	= (homoclave != null && homoclave.length() > 0)?true:false;
		// Verificar que los campos no esten vacios
		if(clvNombre.equals("")) 	return false;
		if(fecha.equals("")) 		return false;
		//if(homoclave.equals("")) 	return false;
		// Verificar que el tipo de los campos sea el correcto
		if (Comunes.esAlfabetico(clvNombre,"&0") 		== false)	return false; 
		if (Comunes.esFechaValida(fecha,"yyMMdd") 	== false)	return false; 
		if (hayHomoclave && Comunes.esAlfanumerico(homoclave) 		== false)   return false;
		// Comprobar que las longitudes de la clave del nombre, fecha y homoclave sean las correctas
		if(fecha.length()		!=6) return false;
		if(hayHomoclave && homoclave.length() > 3) return false;
		if (tipoPersona == 'M'){			
			if (clvNombre.length() != 3) return false;           
				return true;
      } else if (tipoPersona == 'F'){ 
			if (clvNombre.length() != 4) return false;
				return true;
		}
		
		return false;
	}
	
	/**
	 * Revisa si en el proceso de carga con id: <tt>numeroProceso</tt> se encontraron registros
	 * con error.
	 * @return <tt>true</tt> si se encontraron registros con error. <tt>false</tt> en caso 
	 * contrario.
	 */
	@Override
	public boolean hayErrorCargaSiag(int numeroProceso)
		throws AppException{
			
		log.info("hayErrorCargaSiag(E)");
		boolean 				hayErrores = false;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
 
			con.conexionDB();
			querySentencia.append(
				"SELECT 											"  + 
				"  DECODE(COUNT(1),0,'false','true') 	"  +
				"							AS HAY_ERRORES  	"  +
				"FROM 											"  + 
				"  COMTMP_CRUCE_SIAG 						"  +
				"WHERE 											"  + 
				"  IC_PROCESO 		= ? 						"  + // numeroProceso
				"  AND CS_ERROR	= ?						"  + // 'S'
				"  AND ROWNUM     = ?						" // 1
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, 		numeroProceso);
			ps.setString(2, 	"S");
			ps.setInt(3, 	1 );
			rs = ps.executeQuery();
			 
			if(rs != null && rs.next()){
				hayErrores = "true".equals(rs.getString("HAY_ERRORES"))?true:false;
			}
			
		}catch(Exception e){
			log.error("hayErrorCargaSiag(Exception)");
			log.error("hayErrorCargaSiag.numeroProceso = "+numeroProceso);
			e.printStackTrace();
			throw new AppException("Error al realizar busqueda de registros con error");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("hayErrorCargaSiag(S)");	
		}
		
		return hayErrores;
		
	}
	
	/**
	 * Inserta en COM_CRUCE_SIAG los registros cargados en COMTMP_CRUCE_SIAG, tal que el numero de 
	 * proceso de carga sea: <tt>numeroProceso</tt>. El parametro <tt>reemplazarRegistros</tt> sirve para
	 * indicar si se borrara o no la informacion previa y se reemplazara por una nueva. Solo se realiza la insercion
	 * si ninguno de los registros presento errores.
	 * @param reemplazarRegistros Indica si los registros anteriores se reemplazaran por los nuevos.
	 * @param numeroProceso Numero de Proceso de Carga que se utilizara para tomar los registros que seran
	 * @param mesInicial Mes Inicial a partir del cual se insertaran los registros
	 * @param mesFinal Ultimo mes para el que se insertaran los registros
	 * insertados.
	 */
	@Override
	public void insertaRegistrosEnCruceSiag(boolean reemplazarRegistros, int numeroProceso, int mesInicial, int mesFinal)
		throws AppException{
		
		log.info("insertaRegistrosEnCruceSiag(E)");
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = null;
		PreparedStatement ps		 			= null;
		
		boolean				lbOk				= true;
		
		try{
 
			con.conexionDB();
			
         querySentencia = new StringBuffer();
			if(reemplazarRegistros){
				
				String anioActual 		= String.valueOf(Fecha.getAnioActual());
				String fechaInicioMes 	= "01/"+(mesInicial<10?"0":"")+String.valueOf(mesInicial)+"/"+anioActual;
				String fechaFinMes		= "01/"+(mesFinal  <10?"0":"")+String.valueOf(mesFinal)  +"/"+anioActual;
				fechaFinMes					= Fecha.getUltimoDiaDelMes(fechaFinMes,"dd/MM/yyyy");
			
				querySentencia.append(
					"DELETE FROM 					"  +
					"	COM_CRUCE_SIAG 			"  +
					"WHERE 							"  +
					"  DF_FECHA_REGISTRO 	  >= TO_DATE(?,'DD/MM/YYYY') 	 "  + // mes inicial
					"  AND DF_FECHA_REGISTRO  <  TO_DATE(?,'DD/MM/YYYY')+1 "    // mes final
				);
				
				ps = con.queryPrecompilado(querySentencia.toString());
				ps.setString(1, fechaInicioMes);
				ps.setString(2, fechaFinMes);
				ps.executeUpdate();
				ps.close();
				
			}
			
			querySentencia.delete(0,querySentencia.length());
			querySentencia.append(
				"INSERT INTO							"  + 
				"  COM_CRUCE_SIAG 					"  +
				"		( 									"  +
				"			CG_RAZON_SOCIAL,			"  + // VARCHAR2(90)
				"			CG_RFC,						"  + // VARCHAR2(15)
				"			CG_ESTADO,					"  + // VARCHAR2(50)
				"			FN_MONTO_ACTUAL_CRED,	"  + // NUMBER(19,3)
				"			IN_PLAZO,					"  + // NUMBER(3,0)
				"			CG_INTERNEDIARIOF,		"  + // VARCHAR2(90)
				"			IN_ESTRATO_INI,			"  + // NUMBER(3,0)
				"			IN_ESTRATO_NUEVO,			"  + // NUMBER(3,0)
				"			IN_SECTOR,					"  + // NUMBER(3,0)
				"			DF_FECHA_REGISTRO,		"  + // DATE
				"			CG_PORTAFOLIO,				"  + // VARCHAR2(30)
				"			IN_CONREC_CLAVE,			"  + // NUMBER(3,0)
				"			CG_CAMPOS_ADICIONALES	"  + // VARCHAR2(4000)
				"		) 									"  +
				"SELECT 									"  +
				"		CG_RAZON_SOCIAL, 									"  +
				"		CG_RFC, 												"  +
				"		CG_ESTADO, 											"  +
				"		TO_NUMBER(CG_MONTO_ACTUAL_CRED), 			"  +
				"		TO_NUMBER(CG_PLAZO), 							"  +
				"		CG_INTERNEDIARIOF, 								"  +
				"		TO_NUMBER(CG_ESTRATO_INI), 					"  +
				"		TO_NUMBER(CG_ESTRATO_NUEVO), 					"  +
				"		TO_NUMBER(CG_SECTOR), 							"  +
				"		TO_DATE(CG_FECHA_REGISTRO,'DD/MM/YYYY'), 	"  +
				"		CG_PORTAFOLIO, 									"  +
				"		TO_NUMBER(CG_CONREC_CLAVE), 					"  +
				"		CG_CAMPOS_ADICIONALES 							"  +
				"FROM 														"  + 
				"  COMTMP_CRUCE_SIAG 									"  +
				"WHERE 														"  + 
				"  IC_PROCESO = ? 										" 
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, 		numeroProceso);
			ps.executeUpdate();
			
		}catch(Exception e){
			log.error("insertaRegistrosEnCruceSiag(Exception)");
			log.error("insertaRegistrosEnCruceSiag.reemplazarRegistros 	= <"+reemplazarRegistros+">");
			log.error("insertaRegistrosEnCruceSiag.numeroProceso 			= <"+numeroProceso+">");
			log.error("insertaRegistrosEnCruceSiag.mesInicial 				= <"+mesInicial+">");
			log.error("insertaRegistrosEnCruceSiag.mesFinal 				= <"+mesFinal+">");
			e.printStackTrace();
			lbOk = false;
			throw new AppException("Error al insertar registros en COM_CRUCE_SIAG.");
		}finally{
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
		 		con.terminaTransaccion(lbOk);
				con.cierraConexionDB();
			}
			log.info("insertaRegistrosEnCruceSiag(S)");	
		}
		
	}
	
	/**
	 * Devuelve un <tt>StringBuffer</tt> con la lista de los mensajes de error
	 * que arrojo el proceso de validacion.
	 * @param numeroProceso Numero de Proceso de la Carga.
	 */
	@Override
	public StringBuffer getMensajeErrorCargaSiag(int numeroProceso)
		throws AppException{
		
		log.info("getMensajeErrorCargaSiag(E)");
		StringBuffer 		mensajeDeError = new StringBuffer(4096000);
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		int					NUMERO_MAXIMO_LINEAS	= 2000;
		int 					contadorLineasError	= 0;
		try{
 
			con.conexionDB();
			querySentencia.append(
				"SELECT 													"  + 
				"	IC_NUMERO_LINEA_REAL AS NUMERO_LINEA,		"  +
				"  CG_DESC_ERROR   		AS DESCRIPCION_ERROR	"  +
				"FROM 													"  + 
				"  COMTMP_CRUCE_SIAG 								"  +
				"WHERE 													"  + 
				"  IC_PROCESO 		= ? 								"  + // numeroProceso
				"  AND CS_ERROR	= ?								"  + // 'S'
				"ORDER BY IC_NUMERO_LINEA ASC						"
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(1, 		numeroProceso);
			ps.setString(2, 	"S");
			rs = ps.executeQuery();
			 
			String mensaje 		= null;
			String numeroLinea 	= null;
			while(rs != null && rs.next()){
 
				contadorLineasError++;
				
				if(contadorLineasError > NUMERO_MAXIMO_LINEAS){
					mensajeDeError.append("S�lo se muestran las primeras "+NUMERO_MAXIMO_LINEAS+" l�neas con error.\r\n");
					break;
				}
				
				// Agregar mensaje de error
				mensajeDeError.append("L�nea ");
				mensajeDeError.append(rs.getString("NUMERO_LINEA"));
				mensajeDeError.append(": ");
				mensajeDeError.append(rs.getString("DESCRIPCION_ERROR"));
				mensajeDeError.append("\r\n");

			}
			
		}catch(Exception e){
			log.error("getMensajeErrorCargaSiag(Exception)");
			log.error("getMensajeErrorCargaSiag.numeroProceso = "+numeroProceso);
			e.printStackTrace();
			throw new AppException("Error al consultar los registros con mensaje de error.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getMensajeErrorCargaSiag(S)");	
		}
		
		return mensajeDeError;
		
	}
	
	/**
	 * Devuelve el Catalogo de Epos de Compranet bajo algun Criterio de Clasificacion
	 * @param tipoClasificacion <tt>ArrayList</tt> con los ids de las clasificaciones a consultar.
	 * @return Lista con el Catalogo de Epos
	 */
	@Override
	public List getCatalogoEposCompranet(List tipoClasificacion,List listaEposSeleccionadas)
		throws AppException{
		
		log.info("getCatalogoEposCompranet(E)");
		
		List 					lista 		= null;
 
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
 
		try{
			
			tipoClasificacion 		= tipoClasificacion 			== null?new ArrayList():tipoClasificacion;
			listaEposSeleccionadas	= listaEposSeleccionadas 	== null?new ArrayList():listaEposSeleccionadas;
			
			lista 		= new ArrayList();
			StringBuffer variablesBind = new StringBuffer();

			for(int i=0;i<tipoClasificacion.size();i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");	
			}
			
			StringBuffer variablesBindEpos = new StringBuffer();
			for(int i=0;i<listaEposSeleccionadas.size();i++){
				if(i>0) variablesBindEpos.append(",");
				variablesBindEpos.append("?");	
			}
			
			con.conexionDB();
			querySentencia.append(
				"SELECT  												"  +
				"  IG_NUMERO_EPO     AS CLAVE, 					"  +
				"  CG_RAZON_SOCIAL   AS DESCRIPCION 			"  +
				"FROM 													"  +
				 " COMCAT_EPO_COMPRANET 							"  + 
				"WHERE 													"  + 
				"  IG_CLASIF_DEPENDENCIA != ? 					"  +// 0: Sin Clasificacion 
				(tipoClasificacion.size() 			>0?" AND IG_CLASIF_DEPENDENCIA IN ("+variablesBind.toString()    +") ":"")+
				(listaEposSeleccionadas.size() 	>0?" AND IG_NUMERO_EPO     NOT IN ("+variablesBindEpos.toString()+") ":"")+
				"ORDER BY "   +
				"  CG_RAZON_SOCIAL ASC" 
			 );
  
			int indice = 1;
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(indice++, 		0	);
			for(int i=0;i<tipoClasificacion.size();i++){
				ps.setInt(indice++,Integer.parseInt((String)tipoClasificacion.get(i)));
			}
			for(int i=0;i<listaEposSeleccionadas.size();i++){
				ps.setInt(indice++,Integer.parseInt((String)listaEposSeleccionadas.get(i)));
			}
			rs = ps.executeQuery();
			 
			ElementoCatalogo 	elemento 		= null;
			String 				claveAnterior	= "";
			while(rs != null && rs.next()){
 
				if(claveAnterior.equals(rs.getString("CLAVE"))){
					continue;
				}else{
					claveAnterior = rs.getString("CLAVE");
				}
				
				elemento 						= new ElementoCatalogo();
				elemento.setClave(rs.getString("CLAVE"));
				elemento.setDescripcion(rs.getString("DESCRIPCION"));
				lista.add(elemento);				
			}
			
			lista =  lista.size() == 0?null:lista;
			
		}catch(Exception e){
			log.error("getCatalogoEposCompranet(Exception)");
			log.error("getCatalogoEposCompranet.tipoClasificacion = "+tipoClasificacion);
			e.printStackTrace();
			throw new AppException("Error al consultar el Catalogo de Epos Compranet.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getCatalogoEposCompranet(S)");	
		}
		
		return lista;
		
	} 
 
	/**
	 * Devuelve el Catalogo de Epos de Compranet (Seleccionadas por el usuario) ordenadas alfabeticamente.
	 * @param tipoClasificacion <tt>ArrayList</tt> con los ids de las clasificaciones a consultar.
	 * @return Lista con el Catalogo de Epos
	 */
	@Override
	public List getCatalogoEposSeleccionadasCompranet(List listaEposSeleccionadas)
		throws AppException{
		
		log.info("getCatalogoEposSeleccionadasCompranet(E)");
		
		if(listaEposSeleccionadas 	== null || listaEposSeleccionadas.size() == 0){
			log.info("getCatalogoEposSeleccionadasCompranet(S)");
			return null;
		}
			
		List 					lista 			= null;
 
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
 
		try{
 
			lista 		= new ArrayList();
			
			StringBuffer variablesBindEpos = new StringBuffer();
			for(int i=0;i<listaEposSeleccionadas.size();i++){
				if(i>0) variablesBindEpos.append(",");
				variablesBindEpos.append("?");	
			}
			
			con.conexionDB();
			querySentencia.append(
				"SELECT  												"  +
				"  IG_NUMERO_EPO     AS CLAVE, 					"  +
				"  CG_RAZON_SOCIAL   AS DESCRIPCION 			"  +
				"FROM 													"  +
				 " COMCAT_EPO_COMPRANET 							"  + 
				"WHERE 													"  + 
				"  IG_CLASIF_DEPENDENCIA != ? 					"  +// 0: Sin Clasificacion 
				(listaEposSeleccionadas.size() 	>0?" AND IG_NUMERO_EPO     IN ("+variablesBindEpos.toString()+") ":"")+
				"ORDER BY "   +
				"  CG_RAZON_SOCIAL ASC" 
			 );
  
			int indice = 1;
			ps = con.queryPrecompilado(querySentencia.toString());
			ps.setInt(indice++, 		0	);
			for(int i=0;i<listaEposSeleccionadas.size();i++){
				ps.setInt(indice++,Integer.parseInt((String)listaEposSeleccionadas.get(i)));
			}
			rs = ps.executeQuery();
			 
			ElementoCatalogo 	elemento 		= null;
			String 				claveAnterior	= "";
			while(rs != null && rs.next()){
 
				if(claveAnterior.equals(rs.getString("CLAVE"))){
					continue;
				}else{
					claveAnterior = rs.getString("CLAVE");
				}
				
				elemento 						= new ElementoCatalogo();
				elemento.setClave(rs.getString("CLAVE"));
				elemento.setDescripcion(rs.getString("DESCRIPCION"));
				lista.add(elemento);				
			}
			
			lista =  lista.size() == 0?null:lista;
			
		}catch(Exception e){
			log.error("getCatalogoEposSeleccionadasCompranet(Exception)");
			log.error("getCatalogoEposSeleccionadasCompranet.listaEposSeleccionadas = "+listaEposSeleccionadas);
			e.printStackTrace();
			throw new AppException("Error al consultar el Catalogo de Epos Compranet.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getCatalogoEposSeleccionadasCompranet(S)");	
		}
		
		return lista;
		
	} 
	
	/**
	 * Devuelve una lista con las claves del portafolio
	 * @return <tt>List</tt>
	 */
	@Override
	public List getCatalogoPortafolioSiag()
		throws AppException{
		
		log.info("getCatalogoPortafolioSiag(E)");
		
		List 					lista 		= new ArrayList();
		ElementoCatalogo 	elemento 	= null;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			con.conexionDB();
			querySentencia.append(
				"SELECT DISTINCT						"  + 
				"	CG_PORTAFOLIO AS PORTAFOLIO	"  +
				"FROM 									"  + 
				"  COM_CRUCE_SIAG 					"  +
				"ORDER BY CG_PORTAFOLIO ASC		"
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			rs = ps.executeQuery();
			
			String portafolio = null;
			while(rs != null && rs.next()){
 
				portafolio = rs.getString("PORTAFOLIO");
 
				if(portafolio != null && !portafolio.equals("")){
					elemento 	= new ElementoCatalogo();
					elemento.setClave(portafolio);
					elemento.setDescripcion(portafolio);
					lista.add(elemento);
				}
 
			}
			
		}catch(Exception e){
			log.error("getCatalogoPortafolioSiag(Exception)");
			e.printStackTrace();
			throw new AppException("Error al consultar las claves del portafolio.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getCatalogoPortafolioSiag(S)");	
		}
		
		return lista;

	}
	
	/**
	 * Devuelve una lista con las Claves de la Clasificacion de la Dependencia
	 * @return <tt>List</tt>
	 */
	public List getCatalogoClasificacionDependencia()
		throws AppException{
		
		log.info("getCatalogoClasificacionDependencia(E)");
		
		List 					lista 		= new ArrayList();
		ElementoCatalogo 	elemento 	= null;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			con.conexionDB();
			querySentencia.append(
				"SELECT 									"  + 
				"	IC_DEPENDENCIA AS CLAVE,		"  +
				"	CG_DESCRIPCION AS DESCRIPCION	"  +
				"FROM 									"  + 
				"  COMCAT_DEPEN_CADENAS 			"  +
				"ORDER BY CG_DESCRIPCION ASC		"
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			rs = ps.executeQuery();
			
			String clave			= null;
			String descripcion	= null;
			while(rs != null && rs.next()){
 
				clave = rs.getString("CLAVE");
				clave = clave == null?"":clave.trim();
 
				if(!"".equals(clave)){
					
					descripcion =  rs.getString("DESCRIPCION");
					descripcion =  descripcion == null || "".equals(descripcion.trim())?"SIN DESCRIPCION":descripcion.trim();
 
					elemento 	= new ElementoCatalogo();
					elemento.setClave(clave);
					elemento.setDescripcion(descripcion);
					lista.add(elemento);
				}
 
			}
			
		}catch(Exception e){
			log.error("getCatalogoClasificacionDependencia(Exception)");
			e.printStackTrace();
			throw new AppException("Error al consultar el Catalogo con la Clasificacion de la Dependencia.");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getCatalogoClasificacionDependencia(S)");	
		}
		
		return lista;

	}
	
	/**
	 * Devuelve el ID del Proceso de Cruce de Proveedores de CADENAS con COMPRANET
	 * @return <tt>String</tt> con el ID.
	 */
	@Override
	public String getIdCruceCadenasCompranet() 
		throws AppException{
		
		log.info("getIdCruceCadenasCompranet(E)");
		String idProceso = null;
		
		AccesoDB 			con  				= new AccesoDB();
		StringBuffer 		querySentencia = new StringBuffer();
		ResultSet 			rs 				= null;
		PreparedStatement ps		 			= null;
		
		try{
			
			con.conexionDB();
			querySentencia.append(
				"SELECT SEQ_COMTMP_CRUCE_CAD_COMPRANET.NEXTVAL AS ID_PROCESO FROM DUAL"
			 );
  
			ps = con.queryPrecompilado(querySentencia.toString());
			rs = ps.executeQuery();
			 
			if(rs != null && rs.next()){
				idProceso = rs.getString("ID_PROCESO");
			}
			
		}catch(Exception e){
			log.error("getIdCruceCadenasCompranet(Exception)");
			e.printStackTrace();
			throw new AppException("Error al obtener ID del Proceso de Cruce de Proveedores de Cadenas con Compranet");
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			log.info("getIdCruceCadenasCompranet(S)");	
		}
		
		return idProceso;
	}
 
	/**
	 * Registra en la tabla COMTMP_STACK_COMPRANET la lista de Numeros de EPOs que fueron seleccionadas por el usuario
	 * para realizar el cruce de proveedores entre CADENAS y COMPRANET.
	 * @param idProcesoCruce ID del Proceso de Cruce
	 * @param listaEposSeleccionadas <tt>List</tt> con los ids de las EPOs seleccionadas
	 */
	@Override
	public void registraListaEposCompranetSeleccionadas(String idProcesoCruce,List listaEposSeleccionadas)
		throws AppException{
		
		log.info("registraListaEposCompranetSeleccionadas(E)");	
		
		// Obtener Registro	
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;		
		
		try {
			
			con.conexionDB();
			int idProceso = Integer.parseInt(idProcesoCruce);
			
			// Borrar registros anteriores
			query = new StringBuffer();	
			query.append(
				"DELETE FROM COMTMP_STACK_COMPRANET WHERE IC_PROCESO = ? "
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,idProceso);
			ps.executeUpdate();
			ps.close();
			
			// Insertar Ids de las EPOs seleccionadas
			if(listaEposSeleccionadas != null && listaEposSeleccionadas.size() > 0 ){
				query = new StringBuffer();
				query.append(
					"INSERT INTO "  +
					"	COMTMP_STACK_COMPRANET "  +
					"		( "  +
					"			IC_PROCESO, "  +
					"			IG_NUMERO_EPO " +
					"		) " +
					"	VALUES "  +
					"		( "  +
					"			?, "  +
					"			? "  +
					"		)"   
				);
				ps = con.queryPrecompilado(query.toString());
				for(int i=0;i<listaEposSeleccionadas.size();i++){
					ps.clearParameters();
					ps.setInt(1,	idProceso);
					ps.setInt(2,	Integer.parseInt((String)listaEposSeleccionadas.get(i)));
					ps.executeUpdate();
				}
				ps.close();
			}
			
		}catch (Exception e){
			log.error("registraListaEposCompranetSeleccionadas(Exception)");
			log.error("registraListaEposCompranetSeleccionadas.idProcesoCruce 			= <"+idProcesoCruce+">");
			log.error("registraListaEposCompranetSeleccionadas.listaEposSeleccionadas 	= <"+listaEposSeleccionadas+">");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al registrar la lista de Numeros de EPOs que fueron seleccionadas por el usuario para realizar el cruce de proveedores entre CADENAS y COMPRANET");
		}finally{
		 	if(ps != null) 	{ try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("registraListaEposCompranetSeleccionadas(E)");	
		}
		
	}
	
	/**
	 * Este metodo manda a llamar un Procedimiento almacenado que realiza el cruce de proveedores de CADENAS con COMPRANET
	 * @param	idProceso						Id del Proceso de Cruce. 
	 * @param	fechaInicialCadenas			Fecha Inicial de COM_CRUCE_CADENAS. Formato DD/MM/YYYY.
	 * @param	fechaFinalCadenas				Fecha Final de COM_CRUCE_CADENAS. Formato DD/MM/YYYY.
	 * @param	clasificacionDependencia	Clasificacion de la Dependencia.
	 * @param	fechaInicialCompranet		Fecha Inicial de COM_CRUCE_COMPRANET. Formato DD/MM/YYYY.
	 * @param	fechaFinalCompranet			Fecha Final de COM_CRUCE_COMPRANET. Formato DD/MM/YYYY.
	 */
	@Override
	public void cruzaProveedoresCadenasConCompranet(
			String	idProceso,
			String	fechaInicialCadenas,
			String	fechaFinalCadenas,
			String 	clasificacionDependencia,
			String 	fechaInicialCompranet,
			String 	fechaFinalCompranet
		) 
		throws AppException{
			
		log.info("cruzaProveedoresCadenasConCompranet(E)");
		
		AccesoDB 			con 	= new AccesoDB();
		ResultSet 			rs 	= null;
		StringBuffer 		query = null;
		CallableStatement cs 	= null;
		boolean				lbOk	= true;
 		
		try{
			 
			con.conexionDB();

			cs = con.ejecutaSP("SP_CRUCE_CADENAS_COMPRANET(?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'),?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'))");
			cs.setInt(1, 		Integer.parseInt(idProceso)	);
			cs.setString(2, 	fechaInicialCadenas				);
			cs.setString(3, 	fechaFinalCadenas					);
			cs.setInt(4,		Integer.parseInt(clasificacionDependencia));
			cs.setString(5, 	fechaInicialCompranet			);
			cs.setString(6, 	fechaFinalCompranet				);
			rs = cs.executeQuery();
 
		}catch(Exception e) {
			
			log.info("cruzaProveedoresCadenasConCompranet(Exception)");
			log.info("cruzaProveedoresCadenasConCompranet.idProceso 						= <"+idProceso						+">");
			log.info("cruzaProveedoresCadenasConCompranet.fechaInicialCadenas 		= <"+fechaInicialCadenas		+">");
			log.info("cruzaProveedoresCadenasConCompranet.fechaFinalCadenas 			= <"+fechaFinalCadenas			+">");
			log.info("cruzaProveedoresCadenasConCompranet.clasificacionDependencia 	= <"+clasificacionDependencia	+">");
			log.info("cruzaProveedoresCadenasConCompranet.fechaInicialCompranet 		= <"+fechaInicialCompranet		+">");
			log.info("cruzaProveedoresCadenasConCompranet.fechaFinalCompranet 		= <"+fechaFinalCompranet		+">");
			lbOk = false;
			e.printStackTrace();
			throw new AppException("Ocurri� un error al realizar el cruce de proveedores de CADENAS con COMPRANET.");
			
		} finally {
			
			if(rs != null) try { rs.close(); }catch(Exception e){}
			if(cs != null) try { cs.close(); }catch(Exception e){}

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbOk);
				con.cierraConexionDB();
			}
			log.info("cruzaProveedoresCadenasConCompranet(S)");
		}

	}
	
	/** 
	 *  Devuelve una fecha con formato: DD/MM/YYYY correspondiente al primer dia del mes indicado en el
	 *  parametro: <tt>mes</tt> y para el a�o especificado en el parametro <tt>anio</tt>
	 *  @param anio <tt>int</tt> con el valor del A�o con el que se generara la fecha.
	 *  @param mes <tt>int</tt> con el valor del mes con el que se generara la fecha.
	 *  @return <tt>String</tt> con la fecha solicitada.
	 */
	@Override
	public String getPrimerDiaDelMes(int anio,int mes){
		return ("01/"+(mes<10?"0":"")+String.valueOf(mes)+"/"+String.valueOf(anio));
	}
	
	/** 
	 *  Devuelve una fecha con formato: DD/MM/YYYY correspondiente al ultimo dia del mes indicado en el
	 *  parametro: <tt>mes</tt> y para el a�o especificado en el parametro <tt>anio</tt>
	 *  @param anio <tt>int</tt> con el valor del A�o con el que se generara la fecha.
	 *  @param mes <tt>int</tt> con el valor del mes con el que se generara la fecha.
	 *  @return <tt>String</tt> con la fecha solicitada.
	 */
	@Override
	public String getUltimoDiaDelMes(int anio,int mes){
		String primerDiaDelMes = getPrimerDiaDelMes(anio,mes); 
		return Fecha.getUltimoDiaDelMes(primerDiaDelMes,"dd/MM/yyyy");
	}
 
	/**
	 * Registra en la tabla COMTMP_PORTAFOLIO_SIAG la lista de Portafolios que fueron seleccionados por el usuario.
	 * @param idProcesoCruce ID del Proceso de Cruce.
	 * @param listaPortafolios <tt>String[]</tt> con los ids de los portafolios seleccionados.
	 */
	@Override
	public void registraPortafoliosSeleccionados(String idProcesoCruce,String[] listaPortafolios)
		throws AppException{
		
		log.info("registraPortafoliosSeleccionados(E)");	
		
		// Obtener Registro	
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;		
		
		try {
			
			con.conexionDB();
			int idProceso = Integer.parseInt(idProcesoCruce);
			
			// Borrar registros anteriores
			query = new StringBuffer();	
			query.append(
				"DELETE FROM COMTMP_PORTAFOLIO_SIAG WHERE IC_PROCESO = ?"
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,idProceso);
			ps.executeUpdate();
			ps.close();
			
			// Insertar Ids de los Portafolios seleccionados
			if(listaPortafolios != null && listaPortafolios.length > 0 ){
				query = new StringBuffer();
				query.append(
					"INSERT INTO "  +
					"	COMTMP_PORTAFOLIO_SIAG "  +
					"		( "  +
					"			IC_PROCESO, "  +
					"			CG_PORTAFOLIO " +
					"		) " +
					"	VALUES "  +
					"		( "  +
					"			?, "  +
					"			? "  +
					"		)"   
				);
				ps = con.queryPrecompilado(query.toString());
				for(int i=0;i<listaPortafolios.length;i++){
					System.err.println("listaPortafolios[i]= " + listaPortafolios[i] );
					
					ps.clearParameters();
					ps.setInt(1,		idProceso);
					ps.setString(2,	listaPortafolios[i]);
					ps.executeUpdate();
				}
				ps.close();
			}
			
		}catch (Exception e){
			log.error("registraPortafoliosSeleccionados(Exception)");
			log.error("registraPortafoliosSeleccionados.idProcesoCruce 		= <"+idProcesoCruce+">");
			log.error("registraPortafoliosSeleccionados.listaPortafolios 	= <"+listaPortafolios+">");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al registrar la lista de Portafolios");
		}finally{
		 	if(ps != null) 	{ try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("registraPortafoliosSeleccionados(E)");	
		}
		
	}
 
	/**
	 * Este metodo manda a llamar un Procedimiento almacenado que realiza el cruce de los RFCs Unicos con los de SIAG
	 * @param	idProceso					Id del Proceso de Cruce. 
	 * @param	fechaInicialSiag			Fecha Inicial de COM_CRUCE_SIAG. Formato DD/MM/YYYY.
	 * @param	fechaFinalSiag				Fecha Final de COM_CRUCE_SIAG. Formato DD/MM/YYYY.
	 */
	@Override
	public void cruzarRFCsUnicosConSIAG(
			String	idProceso,
			String	fechaInicialSiag,
			String	fechaFinalSiag
		) 
		throws AppException{
			
		log.info("cruzarRFCsUnicosConSIAG(E)");
		
		AccesoDB 			con 	= new AccesoDB();
		ResultSet 			rs 	= null;
		StringBuffer 		query = null;
		CallableStatement cs 	= null;
		boolean				lbOk	= true;
 		
		try{
			 
			con.conexionDB();

			cs = con.ejecutaSP("SP_CRUCE_RFCS_UNICOS_SIAG(?,TO_DATE(?,'DD/MM/YYYY'),TO_DATE(?,'DD/MM/YYYY'))");
			cs.setInt(1, 		Integer.parseInt(idProceso)	);
			cs.setString(2, 	fechaInicialSiag				);
			cs.setString(3, 	fechaFinalSiag					);
			rs = cs.executeQuery();
 
		}catch(Exception e) {
			
			log.info("cruzarRFCsUnicosConSIAG(Exception)");
			log.info("cruzarRFCsUnicosConSIAG.idProceso 			= <"+idProceso				+">");
			log.info("cruzarRFCsUnicosConSIAG.fechaInicialSiag = <"+fechaInicialSiag	+">");
			log.info("cruzarRFCsUnicosConSIAG.fechaFinalSiag	= <"+fechaFinalSiag		+">");
			lbOk = false;
			e.printStackTrace();
			throw new AppException("Ocurri� un error al realizar el cruce de los RFCs Unicos con los de SIAG.");
			
		} finally {
			
			if(rs != null) try { rs.close(); }catch(Exception e){}
			if(cs != null) try { cs.close(); }catch(Exception e){}

			if(con.hayConexionAbierta()){
				con.terminaTransaccion(lbOk);
				con.cierraConexionDB();
			}
			log.info("cruzarRFCsUnicosConSIAG(S)");
		}

	}
	
	/**
	 * Inserta los proveedores resultantes del cruce de RFCs Unicos con SIAG en COM_REDINF_SIAG
	 * @param idProcesoCruce ID del Proceso de Cruce.
	 * @param listaPortafolios <tt>String[]</tt> con los ids de los portafolios seleccionados.
	 */
	@Override
	public void insertarCruceEnRedinfSiag(String idProcesoCruce)
		throws AppException{
		
		log.info("insertarCruceEnRedinfSiag(E)");	
		
		// Obtener Registro	
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;		
		
		try {
			
			con.conexionDB();
			int idProceso = Integer.parseInt(idProcesoCruce);
			
			// Borrar registros anteriores
			query = new StringBuffer();	
			query.append(
				"DELETE FROM COM_REDINF_SIAG"
			);
			ps = con.queryPrecompilado(query.toString());
			ps.executeUpdate();
			ps.close();
			
			// Insertar los RFC's resultantes del Cruce
			query = new StringBuffer();
			query.append(
					"INSERT INTO 							"  +
					"       COM_REDINF_SIAG 			"  +
					"       ( 								"  +  
					"               CG_RFC,          "  +   
					"               CG_RAZON_SOCIAL, "  +   
					"               CG_PORTAFOLIO 	"  +
					"       ) 								"  +  
					"  SELECT 								"  +
					"       DISTINCT 						"  +
					"               CG_RFC, 			"  +
					"               CG_RAZON_SOCIAL, "  +
					"               CG_PORTAFOLIO 	"  +
					"  FROM 									"  +
					"       COM_CRUCE_SIAG 				"  +
					"  WHERE 								"  +
					"       CG_RFC IN (SELECT CG_RFC FROM COMTMP_CRUCE_CADENAS_COMPRANET WHERE IC_PROCESO = ? AND CS_GARANTIAS = 'S' ) "    
				);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,	idProceso);
			ps.executeUpdate();
			ps.close();
			
			
		}catch (Exception e){
			log.error("insertarCruceEnRedinfSiag(Exception)");
			log.error("insertarCruceEnRedinfSiag.idProcesoCruce 		= <"+idProcesoCruce+">");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al insertar los registros");
		}finally{
		 	if(ps != null) 	{ try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("insertarCruceEnRedinfSiag(E)");	
		}
		
	}
	
	/**
	 * Borra todos los registros temporales cargados en la tabla COMTMP_CRUCE_COMPRANET
	 * @param numeroProceso  Id del Proceso de Carga
	 * @param numeroRegistro Numero de Registro a partir del cual se realizara el borrado.
	 */
	@Override
	public void borrarRegistrosTmpCargaCompranet(int numeroProceso, int numeroRegistro)
		throws AppException{
		
		log.info("borrarRegistrosTmpCargaCompranet(E)");	
			
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;		
		
		try {
			
			con.conexionDB();
			
			query = new StringBuffer();	
			query.append(
				"DELETE FROM COMTMP_CRUCE_COMPRANET WHERE IC_PROCESO = ? AND IC_NUMERO_LINEA >= ? "
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,numeroProceso);
			ps.setInt(2,numeroRegistro);
			ps.executeUpdate();
			ps.close();
			
		}catch (Exception e){
			log.error("borrarRegistrosTmpCargaCompranet(Exception)");
			log.error("borrarRegistrosTmpCargaCompranet.numeroProceso  = <"+numeroProceso+">");
			log.error("borrarRegistrosTmpCargaCompranet.numeroRegistro = <"+numeroRegistro+">");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al borrar los registros temporales de COMTMP_CRUCE_COMPRANET.");
		}finally{
		 	if(ps != null) 	{ try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("borrarRegistrosTmpCargaCompranet(E)");	
		}
		
	}
	
	/**
	 * Borra todos los registros temporales cargados en la tabla COMTMP_CRUCE_SIAG
	 * @param numeroProceso 	Id del Proceso de Carga
	 * @param numeroRegistro   Numero de Registro a partir del cual se realizara el borrado.
	 */
	@Override
	public void borrarRegistrosTmpCargaSiag(int numeroProceso, int numeroRegistro)
		throws AppException{
		
		log.info("borrarRegistrosTmpCargaSiag(E)");	
			
		AccesoDB 			con 				= new AccesoDB();
		boolean				lbOK				= true;
		StringBuffer 		query 			= null; 
		PreparedStatement ps					= null;		
		
		try {
			
			con.conexionDB();
			
			query = new StringBuffer();	
			query.append(
				"DELETE FROM COMTMP_CRUCE_SIAG WHERE IC_PROCESO = ? AND IC_NUMERO_LINEA >= ? "
			);
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,	numeroProceso);
			ps.setInt(2,	numeroRegistro);
			ps.executeUpdate();
			ps.close();
			
		}catch (Exception e){
			log.error("borrarRegistrosTmpCargaSiag(Exception)");
			log.error("borrarRegistrosTmpCargaSiag.numeroProceso   = <"+numeroProceso+">");
			log.error("borrarRegistrosTmpCargaSiag.numeroRegistro  = <"+numeroRegistro+">");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error al borrar los registros temporales de COMTMP_CRUCE_SIAG.");
		}finally{
		 	if(ps != null) 	{ try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.terminaTransaccion(lbOK);
				con.cierraConexionDB();
			}
			log.info("borrarRegistrosTmpCargaSiag(E)");	
		}
		
	}
	
	/**
	 * Genera archivo de Cadenas para la carga de informacion para Cadenas y guarda en la base de datos (cruce cadenas)
	 * @param hm
	 * @return
	 */
	public String setGenerarArchivoCargaInfo(Map hm){
		log.info("borrarRegistrosTmpCargaSiag(E)");	
			
		AccesoDB con = new AccesoDB();
		StringBuffer strSQL		= new StringBuffer();
		StringBuffer query		= null; 
		PreparedStatement ps	= null;		
		ResultSet rs			= null;
		File file				= null;
		FileOutputStream out	= null;
		BufferedWriter csv		= null;
		
		ArrayList datos			= new ArrayList();
		ArrayList datosInsert	= new ArrayList();
		boolean commit			= true;
		String nombreArchivo	= null;
		try {
			con.conexionDB();
			
			//recupero los datos enviados
			String clasificacion 	= (String)hm.get("clasificacion");
			String mesInicial 	= (String)hm.get("mesInicial");
			String mesFinal 	= (String)hm.get("mesFinal");
			String anioInicial 	= (String)hm.get("anioInicial");
			String anioFinal 	= (String)hm.get("anioFinal");
			String eposPEF		= (String)hm.get("eposPEF");
			String eposGobMun	= (String)hm.get("eposGobMun");
			String eposPrivadas	= (String)hm.get("eposPrivadas");
			String eposCredi	= (String)hm.get("eposCredi");
			String eposNC		= (String)hm.get("eposNC");
			String sNoEpoSelec	= (String)hm.get( "sNoEpoSelec");
			String periodosP	= (String)hm.get("periodosP");
			String noProceso	= (String)hm.get("noProceso");
			
			String 	strDirectorioPublicacion 	= (String)hm.get("directorio_publicacion");
			String 	strDirectorioPlantillas		= (String)hm.get("directorio_plantillas");
			String 	strDirectorioTemp 			= strDirectorioPublicacion + "00tmp/15cadenas/";
			CreaArchivo archivo					= null;
			
			
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".csv";
			out 				  = new FileOutputStream(strDirectorioTemp+nombreArchivo);
			csv 				  = new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
			ArrayList alMes = new ArrayList();
			ArrayList alAnio = new ArrayList();
			ArrayList datosmeses = new ArrayList();
			int i = 0;
			
			if(!sNoEpoSelec.equals("")){
				int tamanio2 =	sNoEpoSelec.length();
				String valor =  sNoEpoSelec.substring(tamanio2-1, tamanio2);
					log.debug("valor ----> ----- "+valor);
				if(valor.equals(",")){
					sNoEpoSelec =sNoEpoSelec.substring(0,tamanio2-1);
				}
			}
			
			VectorTokenizer vt = new VectorTokenizer(periodosP,",");
			Vector vecPeriodo = vt.getValuesVector();
			boolean bSeis = false;
			boolean bOcho = false;
			boolean bTodos = false;
			for (int p = 0; p < vecPeriodo.size(); p++) {
				if (vecPeriodo.get(p).toString().equals("6")) {  bSeis = true;		}
				if (vecPeriodo.get(p).toString().equals("8")) {  bOcho = true; 		}
				if (vecPeriodo.get(p).toString().equals("T")) {  bTodos = true; 	}
			}

			SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sdfAnio = new SimpleDateFormat("yyyy");
			SimpleDateFormat sdfMes = new SimpleDateFormat("MM");

			String sFechaIni="", sFechaFin ="";
			int mFinal = Integer.parseInt(mesFinal)+1; 
			
			sFechaIni = "01/"+mesInicial+"/"+anioInicial;
			sFechaFin =  "01/"+mFinal +"/"+anioFinal;

			//Inicia Encabezado
			csv.write(" EPO, PYME, ESTADO,RFC,SECTOR, ");

			//se obtie el nombre de los meses
			strSQL = new StringBuffer();
			strSQL.append(" SELECT MES.ic_mes AS mes, ANO.ic_anio AS anio, MES.nombre||'-'||ANO.ic_anio as nomMesAnio");
			strSQL.append(" FROM (SELECT 1 AS ic_mes, 'ENE' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 2 AS ic_mes, 'FEB' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 3 AS ic_mes, 'MAR' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 4 AS ic_mes, 'ABR' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 5 AS ic_mes, 'MAY' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 6 AS ic_mes, 'JUN' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 7 AS ic_mes, 'JUL' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 8 AS ic_mes, 'AGO' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 9 AS ic_mes, 'SEP' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 10 AS ic_mes,'OCT' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 11 AS ic_mes,'NOV' AS nombre FROM dual UNION");
			strSQL.append(" SELECT 12 AS ic_mes,'DIC' AS nombre FROM dual) MES,");
			strSQL.append(" (SELECT "+anioInicial+" AS ic_anio from dual UNION");
			strSQL.append(" SELECT "+anioFinal+" AS ic_anio from dual) ANO");
			if(anioInicial.equals(anioFinal) ){
				strSQL.append(" WHERE (MES.ic_mes >= TO_NUMBER('"+mesInicial+"') AND ANO.ic_anio = "+anioInicial+") AND (MES.ic_mes <= TO_NUMBER('"+mesFinal+"') AND ANO.ic_anio = "+anioFinal+")");
			}else if(!anioInicial.equals(anioFinal) ){
				strSQL.append(" WHERE (MES.ic_mes >= TO_NUMBER('"+mesInicial+"') AND ANO.ic_anio = "+anioInicial+") OR (MES.ic_mes <= TO_NUMBER('"+mesFinal+"') AND ANO.ic_anio = "+anioFinal+")");
			}
			strSQL.append(" ORDER BY ANO.ic_anio, MES.ic_mes");

			//log.debug("obtengo meses" + strSQL.toString());

			rs = con.queryDB(strSQL.toString());
			while (rs.next()) {
				alMes.add(i, rs.getString("mes"));
				alAnio.add(i, rs.getString("anio"));
				csv.write(rs.getString("nomMesAnio")+",");  // encabezado de meses
				datosmeses.add(rs.getString("nomMesAnio"));
				i++;
			}
			rs.close();
			con.cierraStatement();

			csv.write( "SUMA"+","); //encabezado de suma
			csv.write("\r\n");

			//inicia la consulta de los datos  a mostrar
			strSQL = new StringBuffer();
			strSQL.append(" SELECT /*+ use_nl( mto, p, e, dom, es, se, sub, ram)*/");
			strSQL.append(" e.ic_epo AS ic_epo,");
			strSQL.append(" e.cg_razon_social AS nomEpo,");
			strSQL.append(" p.ic_pyme AS numPyme,");
			strSQL.append(" REPLACE(p.cg_razon_social, ',', '') AS nomPyme,");
			strSQL.append(" es.cd_nombre AS nomEstado,");
			strSQL.append(" p.cg_rfc AS rfcPyme,");
			strSQL.append(" se.cd_nombre AS sector,");
			for (i = 0; i < alMes.size(); i++) {
				strSQL.append(" mto.mes" + (i + 1) + ",");
			}
			if(alMes.size()>6){
				strSQL.append(" CASE WHEN(INSTR((");
				for (i = 6; i < alMes.size(); i++) {
					strSQL.append(" DECODE(mto.mes"+ (i + 1) + ", 0, 0, 1) || '' ||");
				}
				strSQL.delete(strSQL.length() - 9, strSQL.length());
				strSQL.append("), '111111') <> 0) THEN 'SI' ELSE 'NO' END AS SeisConsec,");
			}
			strSQL.append(" CASE WHEN((");
			for( i = 0; i < alMes.size(); i++) {
				strSQL.append(" DECODE(mto.mes"+ (i + 1) + ", 0, 0, 1)+");
			}
			strSQL.delete(strSQL.length() - 1, strSQL.length());
			strSQL.append(") >= 8) THEN 'SI' ELSE 'NO' END AS OchoDeDoce");
			strSQL.append(" FROM comcat_pyme p,");
			strSQL.append(" comcat_epo e,");
			strSQL.append(" com_domicilio dom,");
			strSQL.append(" comcat_estado es,");
			strSQL.append(" comcat_sector_econ se,");
			strSQL.append(" comcat_subsector sub,");
			strSQL.append(" comcat_rama ram,");
			strSQL.append(" (");
			strSQL.append(" SELECT m.ic_epo,");
			strSQL.append(" m.ic_pyme,");
			strSQL.append(" m.credcad,");
			for (i = 0; i < alMes.size(); i++) {
				strSQL.append(" SUM(m.m" + (i + 1) + ") AS mes" + (i + 1) + ",");
			}
			strSQL.delete(strSQL.length() - 1, strSQL.length());
			strSQL.append(" FROM");
			strSQL.append(" (SELECT /*+ index(a in_com_acuse1_02_nuk) use_nl(a, d, pexp)*/");
			strSQL.append(" d.ic_epo,");
			strSQL.append(" d.ic_pyme,");
			strSQL.append(" DECODE(pexp.cs_habilitado, 'S', 'YA', 'H', 'YA', 'NO') AS credcad,");
			for (i = 0; i < alMes.size(); i++) {
				strSQL.append(" CASE WHEN (" + alMes.get(i).toString() + " = TO_NUMBER(TO_CHAR(D.df_alta,'MM'))) AND (" + alAnio.get(i).toString() + " = TO_NUMBER(TO_CHAR(D.df_alta,'YYYY'))) THEN SUM(D.fn_monto) ELSE 0 END AS m" + (i + 1) + ",");
			}
			strSQL.delete(strSQL.length() - 1, strSQL.length());
			strSQL.append(" FROM com_acuse1 a, com_documento D, comrel_pyme_epo_x_producto PEXP");
			strSQL.append(" WHERE a.cc_acuse = d.cc_acuse ");
			strSQL.append(" AND D.ic_epo = PEXP.ic_epo(+)");
			strSQL.append(" AND D.ic_pyme = PEXP.ic_pyme(+)");
			strSQL.append(" AND a.DF_FECHAHORA_CARGA >= TO_DATE('" + sFechaIni + "', 'DD/MM/YYYY')");
			strSQL.append(" AND a.DF_FECHAHORA_CARGA < TO_DATE('" + sFechaFin + "', 'DD/MM/YYYY')");
			strSQL.append(" AND D.ic_moneda = 1");
			strSQL.append(" AND D.ic_estatus_docto <> 1");
			strSQL.append(" AND D.ic_epo IN (" + sNoEpoSelec + ")");
			strSQL.append(" AND PEXP.ic_producto_nafin(+) = 5");
			strSQL.append(" GROUP BY d.ic_epo, d.ic_pyme, TO_NUMBER(TO_CHAR(D.df_alta,'YYYY')), TO_NUMBER(TO_CHAR(D.df_alta,'MM'))");
			strSQL.append(", DECODE (pexp.cs_habilitado, 'S', 'YA', 'H', 'YA', 'NO')) M");
			strSQL.append(" GROUP BY M.ic_epo, M.ic_pyme, m.credcad");
			strSQL.append(" ) MTO");
			strSQL.append(" WHERE E.ic_epo = mto.ic_epo");
			strSQL.append(" AND P.ic_pyme = mto.ic_pyme");
			strSQL.append(" AND P.ic_pyme = DOM.ic_pyme");
			strSQL.append(" AND p.ic_sector_econ = se.ic_sector_econ");
			strSQL.append(" AND p.ic_sector_econ = sub.ic_sector_econ");
			strSQL.append(" AND p.ic_subsector = sub.ic_subsector");
			strSQL.append(" AND p.ic_sector_econ = ram.ic_sector_econ");
			strSQL.append(" AND p.ic_subsector = ram.ic_subsector");
			strSQL.append(" AND p.ic_rama = ram.ic_rama");
			strSQL.append(" AND DOM.ic_estado = ES.ic_estado");

			if (!bTodos) {
				if (bSeis && bOcho) {
					strSQL.append(" AND ( ( INSTR( ( ");
					for (i = 6; i < alMes.size(); i++) {
						strSQL.append(" DECODE(mto.mes"+ (i + 1) + ", 0, 0, 1) || '' ||");
					}
					strSQL.delete(strSQL.length() - 9, strSQL.length());
					strSQL.append(" ), '111111') <> 0)");
					strSQL.append(" OR ( ");
					for (i = 0; i < alMes.size(); i++) {
						strSQL.append(" DECODE(mto.mes" + (i + 1) + ", 0, 0, 1)+");
					}
					strSQL.delete(strSQL.length() - 1, strSQL.length());
					strSQL.append(" >= 8) )");

				} else	if (bSeis) {
					strSQL.append(" AND INSTR( (");
					for (i = 6; i < alMes.size(); i++) {
						strSQL.append(" DECODE(mto.mes" + (i + 1) + ", 0, 0, 1) || '' ||");
					}
					strSQL.delete(strSQL.length() - 9, strSQL.length());
					strSQL.append(" ), '111111') <> 0 ");

				} else	if (bOcho ) {
					strSQL.append(" AND (");
					for (i = 0; i < alMes.size(); i++) {
						strSQL.append(" DECODE(mto.mes" + (i + 1) + ", 0, 0, 1)+");
					}
					strSQL.delete(strSQL.length() - 1, strSQL.length());
					strSQL.append(" ) >= 8 ");
				}
			}

			strSQL.append(" GROUP BY E.ic_epo,E.cg_razon_social, P.ic_pyme, ");
			strSQL.append(" mto.credcad,");
			strSQL.append(" P.in_numero_sirac, P.in_numero_sirac, P.cg_razon_social, ES.cd_nombre,");
			strSQL.append(" DOM.cn_cp, DOM.cg_telefono1, DOM.cg_telefono2, P.cg_rfc,");
			strSQL.append(" se.cd_nombre,");
			strSQL.append(" sub.cd_nombre,");
			strSQL.append(" ram.cd_nombre,");
			for (i = 0; i < alMes.size(); i++) {
				strSQL.append(" MTO.mes" + (i + 1) + ",");
			}

			strSQL.delete(strSQL.length() - 1, strSQL.length());
			strSQL.append("  order by e.ic_epo ");
			System.out.println("Genera Carga Cadenas : "+strSQL.toString());

			double suma  = 0;
			rs = con.queryDB(strSQL.toString());
			int hayRegistros = 0;

			while(rs.next()) {
				csv.write( rs.getString("nomEpo")==null?"":rs.getString("nomEpo").replace(',',' ') );
				csv.write(	","+(rs.getString("nomPyme")==null?"":rs.getString("nomPyme")) );
				csv.write(	","+(rs.getString("nomEstado")==null?"":rs.getString("nomEstado").replace(',',' ')) );
				csv.write(	","+(rs.getString("rfcPyme")==null?"":rs.getString("rfcPyme").replace('-',' ')) );
				csv.write(	","+(rs.getString("sector")==null?"":rs.getString("sector").replace(',',' ')) );
				csv.write(	","  );
				for(i=0; i<alMes.size(); i++){
					csv.write(rs.getString("mes"+(i+1))+",");
					String valor = rs.getString("mes"+(i+1));
					suma  += Double.parseDouble((String)valor);
					String meses =  (String)datosmeses.get(i);

					datos = new ArrayList();
					datos.add(rs.getString("ic_epo")==null?"":rs.getString("ic_epo"));
					datos.add(rs.getString("numPyme")==null?"":rs.getString("numPyme"));
					datos.add(rs.getString("nomEstado")==null?"":rs.getString("nomEstado"));
					datos.add(rs.getString("rfcPyme")==null?"":rs.getString("rfcPyme"));
					datos.add(rs.getString("sector")==null?"":rs.getString("sector"));
					datos.add(valor);
					datos.add(meses);
					datos.add(clasificacion);
					datosInsert.add(datos);
				}
					hayRegistros++;
					csv.write(suma+",");
					csv.write( "\r\n");
					suma = 0;
					hayRegistros++;

			}
			rs.close();
			con.cierraStatement();

			//CUANDO NO HAY REGISTROS
			if(hayRegistros==0){
				csv.write( " No se encontr� ning�n registro para los criter�os seleccionados. " );
				csv.write( "\r\n");
			}

			if(csv 	!= null )  	csv.close();

			//se genera el Archivo
			file = new File(strDirectorioTemp + nombreArchivo);
			//response.setHeader("Content-disposition","attachment; filename=" + "CargaCadenas.csv");
			
			//SI HAY REGISTROS GUARDA EN EL TABLA TEMPORAL DE COM_TMP_CRUCE_CADENAS
			if(hayRegistros >0){
				//Inserta  a la tabla temporal
				String guardar = setInsertTemporal(noProceso, datosInsert);

			}

			if(!sNoEpoSelec.equals("")){

				query = new StringBuffer();
				query.append(" delete  from  COM_CRUCE_DEPEN_EPO ");
				query.append(" where ic_dependencia  = ? ");
				ps = con.queryPrecompilado(query.toString());
				ps.setInt(1, Integer.parseInt(clasificacion) );
				ps.executeUpdate();
				ps.close();
				
				List sNoEpo = new VectorTokenizer(sNoEpoSelec, "\n").getValuesVector();
				for(int l = 0; l< sNoEpo.size(); l++) 	{

					if(sNoEpo.get(l)!=null){
						List Epos = new VectorTokenizer((String)sNoEpo.get(l), ",").getValuesVector();
							for(int e = 0; e < Epos.size(); e++) 	{
								String unaEpo   = (Epos.size()>= 1)?Epos.get(e)==null?"":((String)Epos.get(e)).trim():"";
									if(!unaEpo.equals("")){
									query = new StringBuffer();
									query.append(" INSERT INTO COM_CRUCE_DEPEN_EPO ");
									query.append(" (ic_dependencia,  IC_EPO) ");
									query.append(" VALUES(?,?)");
									log.debug(query.toString());

									ps = con.queryPrecompilado(query.toString());
									ps.setInt(1, Integer.parseInt(clasificacion) );
									ps.setInt(2, Integer.parseInt(unaEpo) );
									ps.executeUpdate();
									ps.close();										
								}
							}
					}
				}
			}
			
			return nombreArchivo;
		}catch(Exception e){
			commit = false;
			log.error("getStreamInfo(Exception)" +e );
			e.printStackTrace();
			throw new AppException("Error al generar el archivo de carga de informacion", e);
		}finally{
			if (rs 	!= null) try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null) try { ps.close(); 	} catch(SQLException e) {}
			if (csv 	!= null )try { csv.close();   } catch(Exception    e) {}
			
			if (con != null && con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("getStreamInfo(s)");
		}
	}
	
	/**
	 * Inserta los registros especificados por el usuario, en la tabla COMTMP_CRUCE_COMPRANET
	 * @param fileInputStream <tt>InputStream</tt> correspondiente al archivo especificado por el usuario
	 * @param numProceso Numero de proceso de carga
	 * @param filtroFechas Filtro de fechas, para filtrar registros que no correspondan al periodo indicado por el usuario
	 *                     o aquellos registros que tengan un numero de mes no valido.
	 */
	public void insertaRegistrosTmpCompranet(String urlArchivo, int numeroProceso, FiltroFechaCompranet filtroFechas)
		throws AppException {

		log.info("insertaRegistrosTmpCompranet(E)");

		AccesoDB 			con 				= new AccesoDB();
		StringBuffer		query 			= null;
		PreparedStatement ps 				= null;
		boolean 				lbOK 				= true;

		String 				lineaf 				= "";
		int 					numeroLinea 		= 1;
		int					numeroLineaReal	= 1;

		RegistroCSVCompranet registro = new RegistroCSVCompranet();

		StringBuffer nombreEpo 						= registro.getNombreEpo();
		StringBuffer licitacion 					= registro.getLicitacion();
		StringBuffer unidad 							= registro.getUnidad();
		StringBuffer tipoContratacion 			= registro.getTipoContratacion();
		StringBuffer fechaEmision 					= registro.getFechaEmision();
		StringBuffer numPartida 					= registro.getNumPartida();
		StringBuffer importeSinIVA 				= registro.getImporteSinIVA();
		StringBuffer razonSocial 					= registro.getRazonSocial();
		StringBuffer rfc 								= registro.getRfc();
		StringBuffer clasificacionDependencia 	= registro.getClasificacionDependencia();

		InputStream fileInputStream = null;
		
		try{
			
			con.conexionDB();
			File file = new File(urlArchivo);
			fileInputStream = new FileInputStream(file);

			// Insertar los datos en la tabla temporal COMTMP_CRUCE_COMPRANET
			query = new StringBuffer();
			query.append(
				"INSERT INTO 							"  +
				"	COMTMP_CRUCE_COMPRANET 			"  +
				"		( 									"  +
				"			IC_PROCESO, 				"  +
				"			IC_NUMERO_LINEA, 			"  +
				"			IC_NUMERO_LINEA_REAL, 	"  +
				"			CG_NOMBRE_EPO, 			"  +
				"			CG_LICITACION, 			"  +
				"			CG_UNIDAD, 					"  +
				"			CG_TIPO_CONTRATACION, 	"  +
				"			CG_FECHA_EMISION, 		"  +
				"			CG_NUM_PARTIDA, 			"  +
				"			CG_IMPORTE_SIN_IVA, 		"  +
				"			CG_RAZON_SOCIAL, 			"  +
				"			CG_RFC, 						"  +
				"			CG_CLASIF_DEPENDENCIA 	"  +
				"		) 									"  +
				"VALUES 									"  +
				"		( "  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			? "  +
				"		)	"
			);
			ps = con.queryPrecompilado(query.toString());

			// Insertar registros en COMTMP_CRUCE_COMPRANET
			CompranetCSV compranetCSV = new CompranetCSV();
			BufferedReader br	= new BufferedReader(new InputStreamReader(fileInputStream,"ISO-8859-1"));
			while((lineaf=br.readLine())!=null){

				compranetCSV.parse(lineaf,registro);

				// Suprimir espacios en blanco al inicio y al final de la cadena
				trim(nombreEpo);
				trim(licitacion);
				trim(unidad);
				trim(tipoContratacion);
				trim(fechaEmision);
				trim(numPartida);
				trim(importeSinIVA);
				trim(razonSocial);
				trim(rfc);
				trim(clasificacionDependencia);

				// Convertir RFC a Mayusculas
				toUpperCase(rfc);

				// Obtener los campos individuales de cada uno de los registros del archivo
				if(nombreEpo.length() 						> 150) 	nombreEpo.delete(151,nombreEpo.length());
				if(licitacion.length() 						> 16) 	licitacion.delete(17,licitacion.length());
				if(unidad.length() 							> 200) 	unidad.delete(201,unidad.length());
				if(tipoContratacion.length() 				> 40) 	tipoContratacion.delete(41,tipoContratacion.length());
				if(fechaEmision.length() 					> 10) 	fechaEmision.delete(11,fechaEmision.length());
				if(numPartida.length() 						> 5) 		numPartida.delete(6,numPartida.length());
				if(importeSinIVA.length() 					> 18) 	importeSinIVA.delete(19,importeSinIVA.length());
				if(razonSocial.length() 					> 200) 	razonSocial.delete(201,razonSocial.length());
				if(rfc.length() 								> 15) 	rfc.delete(16,rfc.length());
				if(clasificacionDependencia.length() 	> 1) 		clasificacionDependencia.delete(2,clasificacionDependencia.length());

				// Filtrar registros por fecha, para solo insertar aquellos que corresponden con el periodo indicado
				if(filtroFechas.insertarRegistro(fechaEmision)){

					compranetCSV.filtraCaracter(importeSinIVA,',');
					compranetCSV.agregaGuionesARFC(rfc);
					// Quitar comillas simples y dobles
					//strNumeroProveedor	=Comunes.quitaComitasSimplesyDobles(strNumeroProveedor);

					ps.clearParameters();
					ps.setInt(1,		numeroProceso);
					ps.setInt(2,		numeroLinea++);
					ps.setInt(3,		numeroLineaReal);
					ps.setString(4,	nombreEpo.toString());
					ps.setString(5,	licitacion.toString());
					ps.setString(6,	unidad.toString());
					ps.setString(7,	tipoContratacion.toString());
					ps.setString(8,	fechaEmision.toString());
					ps.setString(9,	numPartida.toString());
					ps.setString(10,	importeSinIVA.toString());
					ps.setString(11,	razonSocial.toString());
					ps.setString(12,	rfc.toString());
					ps.setString(13,	clasificacionDependencia.toString());
					ps.executeUpdate();

					//Realizar un COMMIT cada 100 registros
					if (numeroLinea % 100 == 0)
						con.terminaTransaccion(true);

				}

				// Calcular Numero Real de L�nea Siguiente
				numeroLineaReal++;

			}

		}catch (IOException ioe){
			log.error("insertaRegistrosTmpCompranet(Exception)");
			log.error("insertaRegistrosTmpCompranet.fileInputStream 	= <"+fileInputStream +">");
			log.error("insertaRegistrosTmpCompranet.numeroProceso		= <"+numeroProceso   +">");
			log.error("insertaRegistrosTmpCompranet.filtroFechas		= <"+filtroFechas    +">");
			log.error("insertaRegistrosTmpCompranet.numeroLineaReal 	= <"+numeroLineaReal	+">");
			log.error("insertaRegistrosTmpCompranet.lineaf          	= <"+lineaf				+">");
			lbOK = false;
			ioe.printStackTrace();
			throw new AppException("Ocurrio un error al realizar insertar registros procedentes de Compranet");
		}catch (Exception e){
			log.error("insertaRegistrosTmpCompranet(Exception)");
			log.error("insertaRegistrosTmpCompranet.fileInputStream 	= <"+fileInputStream +">");
			log.error("insertaRegistrosTmpCompranet.numeroProceso		= <"+numeroProceso   +">");
			log.error("insertaRegistrosTmpCompranet.filtroFechas		= <"+filtroFechas    +">");
			log.error("insertaRegistrosTmpCompranet.numeroLineaReal 	= <"+numeroLineaReal	+">");
			log.error("insertaRegistrosTmpCompranet.lineaf          	= <"+lineaf				+">");
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error en la insecion de registros procedentes de Compranet");
		}finally{
			if(ps != null) try { ps.close(); }catch(Exception e){}
			con.terminaTransaccion(lbOK);
				if(con.hayConexionAbierta()) con.cierraConexionDB();
			log.info("insertaRegistrosTmpCompranet(S)");
		}

	}
	
	/**
	 * Inserta los registros especificados por el usuario, en la tabla COMTMP_CRUCE_SIAG
	 * @param fileInputStream <tt>InputStream</tt> correspondiente al archivo especificado por el usuario
	 * @param numProceso Numero de proceso de carga
	 * @param filtroFechas Filtro de fechas, para filtrar registros que no correspondan al periodo indicado por el usuario
	 *                     o aquellos registros que tengan un numero de mes no valido.
	 */
	public void insertaRegistrosTmpSiag(String urlArchivo, int numeroProceso, FiltroFechaSiag filtroFechas)
		throws AppException {

		log.info("insertaRegistrosTmpSiag(E)");

		AccesoDB 			con 					= new AccesoDB();
		StringBuffer		query 				= null;
		PreparedStatement ps 					= null;
		boolean 				lbOK 					= true;

		String 				lineaf 				= "";
		int 					numeroLinea 		= 1;
		int					numeroLineaReal	= 1;

		RegistroCSVSiag registro = new RegistroCSVSiag();

		StringBuffer razonSocial 					= registro.getRazonSocial();
		StringBuffer rfc 								= registro.getRfc();
		StringBuffer estado 							= registro.getEstado();
		StringBuffer montoActualCredito 			= registro.getMontoActualCredito();
		StringBuffer plazo 							= registro.getPlazo();
		StringBuffer intermediario 				= registro.getIntermediario();
		StringBuffer estratoInicial 				= registro.getEstratoInicial();
		StringBuffer nuevoEstrato 					= registro.getNuevoEstrato();
		StringBuffer sector 							= registro.getSector();
		StringBuffer fechaRegistro 				= registro.getFechaRegistro();
		StringBuffer descripcionPortafolio 		= registro.getDescripcionPortafolio();
		StringBuffer claveCONREC 					= registro.getClaveCONREC();
		StringBuffer camposAdicionales 			= registro.getCamposAdicionales();
		
		InputStream fileInputStream = null;

		try{
			con.conexionDB();
			
			File file = new File(urlArchivo);
			fileInputStream = new FileInputStream(file);

			// Insertar los datos en la tabla temporal COMTMP_CRUCE_SIAG
			query = new StringBuffer();
			query.append(
				"INSERT INTO 								"  +
				"	COMTMP_CRUCE_SIAG 					"  +
				"		( 										"  +
				"			IC_PROCESO, 					"  +
				"			IC_NUMERO_LINEA, 				"  +
				"			IC_NUMERO_LINEA_REAL, 		"  +
				"			CG_RAZON_SOCIAL, 				"  +
				"			CG_RFC, 							"  +
				"			CG_ESTADO, 						"  +
				"			CG_MONTO_ACTUAL_CRED, 		"  +
				"			CG_PLAZO, 						"  +
				"			CG_INTERNEDIARIOF, 			"  +
				"			CG_ESTRATO_INI, 				"  +
				"			CG_ESTRATO_NUEVO, 			"  +
				"			CG_SECTOR, 						"  +
				"			CG_FECHA_REGISTRO, 			"  +
				"			CG_PORTAFOLIO, 				"  +
				"			CG_CONREC_CLAVE, 				"  +
				"			CG_CAMPOS_ADICIONALES, 		"  +
				"			CS_ERROR_LONG_CADICIONALES	"  +
				"		) 										"  +
				"VALUES 										"  +
				"		(    "  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			?,"  +
				"			? "  +
				"		)	  "
			);
			ps = con.queryPrecompilado(query.toString());

			// Insertar registros en COMTMP_CRUCE_SIAG
			boolean camposAdicionalesExcedenLongitud = false;
			SiagCSV siagCSV = new SiagCSV();
			BufferedReader br	= new BufferedReader(new InputStreamReader(fileInputStream,"ISO-8859-1"));
			while((lineaf=br.readLine())!=null){

				camposAdicionalesExcedenLongitud = false;
				siagCSV.parse(lineaf,registro);

				// Suprimir espacios en blanco al inicio y al final
				trim(razonSocial);
				trim(rfc);
				trim(estado);
				trim(montoActualCredito);
				trim(plazo);
				trim(intermediario);
				trim(estratoInicial);
				trim(nuevoEstrato);
				trim(sector);
				trim(fechaRegistro);
				trim(descripcionPortafolio);
				trim(claveCONREC);
				trim(camposAdicionales);

				// Convertir a mayusculas RFC proporcionado
				toUpperCase(rfc);

				// Obtener los campos individuales de cada uno de los registros del archivo
				if(razonSocial.length() 					> 90) 	razonSocial.delete(91,				razonSocial.length());
				if(rfc.length() 								> 15) 	rfc.delete(16,							rfc.length());
				if(estado.length() 							> 50) 	estado.delete(51,						estado.length());
				if(montoActualCredito.length() 			> 19) 	montoActualCredito.delete(20,		montoActualCredito.length());
				if(plazo.length() 							> 3) 		plazo.delete(4,						plazo.length());
				if(intermediario.length() 					> 90) 	intermediario.delete(91,			intermediario.length());
				if(estratoInicial.length() 				> 3) 		estratoInicial.delete(4,			estratoInicial.length());
				if(nuevoEstrato.length() 					> 3) 		nuevoEstrato.delete(4,				nuevoEstrato.length());
				if(sector.length() 							> 3) 		sector.delete(4,						sector.length());
				if(fechaRegistro.length() 					> 10) 	fechaRegistro.delete(11,			fechaRegistro.length());
				if(descripcionPortafolio.length() 		> 30) 	descripcionPortafolio.delete(31,	descripcionPortafolio.length());
				if(claveCONREC.length() 					> 3) 		claveCONREC.delete(4,				claveCONREC.length());
				if(camposAdicionales.length() 			> 4000) {camposAdicionales.delete(4000,	camposAdicionales.length()); camposAdicionalesExcedenLongitud = true; }

				// Filtrar registros por fecha, para solo insertar aquellos que corresponden con el periodo indicado
				if(filtroFechas.insertarRegistro(fechaRegistro)){

					siagCSV.filtraCaracter(montoActualCredito,',');
					siagCSV.agregaGuionesARFC(rfc);

					// Agregar filtro trim

					ps.clearParameters();
					ps.setInt(1,		numeroProceso											);
					ps.setInt(2,		numeroLinea++											);
					ps.setInt(3,		numeroLineaReal										);
					ps.setString(4,	razonSocial.toString()								);
					ps.setString(5,	rfc.toString()											);
					ps.setString(6,	estado.toString()										);
					ps.setString(7,	montoActualCredito.toString()						);
					ps.setString(8,	plazo.toString()										);
					ps.setString(9,	intermediario.toString()							);
					ps.setString(10,	estratoInicial.toString()							);
					ps.setString(11,	nuevoEstrato.toString()								);
					ps.setString(12,	sector.toString()										);
					ps.setString(13,	fechaRegistro.toString()							);
					ps.setString(14,	descripcionPortafolio.toString()					);
					ps.setString(15,	claveCONREC.toString()								);
					ps.setString(16,	camposAdicionales.toString()						);
					ps.setString(17,	(camposAdicionalesExcedenLongitud?"S":"N")	);
					ps.executeUpdate();

					//Realizar un COMMIT cada 100 registros
					if (numeroLinea % 100 == 0)
						con.terminaTransaccion(true);

				}

				// Calcular Numero Real de L�nea Siguiente
				numeroLineaReal++;

			}

		}catch (IOException ioe){
			log.error("insertaRegistrosTmpSiag(Exception)");
			log.error("insertaRegistrosTmpSiag.fileInputStream = <"+fileInputStream +">");
			log.error("insertaRegistrosTmpSiag.numeroProceso	= <"+numeroProceso   +">");
			log.error("insertaRegistrosTmpSiag.filtroFechas		= <"+filtroFechas    +">");
			log.error("insertaRegistrosTmpSiag.numeroLineaReal = <"+numeroLineaReal +">");
			log.error("insertaRegistrosTmpSiag.lineaf          = <"+lineaf          +">");
			lbOK = false;
			ioe.printStackTrace();
			throw new AppException("Ocurrio un error al realizar insertar registros procedentes de SIAG");
		}catch (Exception e){
			log.error("insertaRegistrosTmpSiag(Exception)");
			log.error("insertaRegistrosTmpSiag.fileInputStream = <"+fileInputStream +">");
			log.error("insertaRegistrosTmpSiag.numeroProceso	= <"+numeroProceso   +">");
			log.error("insertaRegistrosTmpSiag.filtroFechas		= <"+filtroFechas    +">");
			log.error("insertaRegistrosTmpSiag.numeroLineaReal = <"+numeroLineaReal+">");
			log.error("insertaRegistrosTmpSiag.lineaf          = <"+lineaf+">"			);
			lbOK = false;
			e.printStackTrace();
			throw new AppException("Ocurrio un error en la insecion de registros procedentes de SIAG");
		}finally{
			if(ps != null) try { ps.close(); }catch(Exception e){}
			con.terminaTransaccion(lbOK);
				if(con.hayConexionAbierta()) con.cierraConexionDB();
			log.info("insertaRegistrosTmpSiag(S)");
		}

	}
	
	/**
	 * genera archivo csv del cruce de informacion para CADENAS y COMPRANET
	 * @param hm
	 * @return
	 */
	public String getGenerarRFCunicosCadCompranet(Map hm){
		log.info("getGenerarRFCunicosCadCompranet(E)");
		AccesoDB con			= new AccesoDB();
		PreparedStatement ps 	= null;
		ResultSet rs 			= null;
		StringBuffer query 		= new StringBuffer();		
		File file				= null;
		
		String 	strDirectorioPublicacion 	= (String)hm.get("directorio_publicacion");
		String 	strDirectorioPlantillas 	= (String)hm.get("directorio_plantillas");
		String 	strDirectorioTemp 			= strDirectorioPublicacion + "00tmp/15cadenas/";		
		CreaArchivo archivo					= null;
		String nombreArchivo				= null;
		FileOutputStream out 				= null;
		BufferedWriter 	 csv 				= null;
		archivo 			= new CreaArchivo();
		nombreArchivo	= archivo.nombreArchivo()+".csv";
		
		int tamanio = 0;		
		String mesesC ="",  mesesCon ="";	
		String fechaInicialCom  ="", fechaFinalCom =""; 
		StringBuffer epoSel = new StringBuffer();
		String epoSele = "";	
		boolean commit = true;
		
		try{
			
			con.conexionDB();
			out  = new FileOutputStream(strDirectorioTemp+nombreArchivo);
			csv  = new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
			//se obtienen los datos seleccionados en el Jsp				
			
			String mesInicial 				= (String)hm.get("mesInicial");				
			String mesFinal 	= (String)hm.get("mesFinal");
			String clasificacion 	= (String)hm.get("clasificacion");				
			String federales 	= (String)hm.get("federales");
			String otros 	= (String)hm.get("otros");
			String mesInicialCom 	= (String)hm.get("mesInicialCom");
			String mesFinalCom 	= (String)hm.get("mesFinalCom");
			String seleciona 	= (String)hm.get("seleciona");
			String anioInicial	= (String)hm.get("anioInicial");
			String anioFinal		= (String)hm.get("anioFinal");
			
		
			//esto es para las fechas de Compranet
			fechaInicialCom = "01/"+mesInicialCom+"/"+anioInicial;				
			if(mesFinalCom.equals("2")){				
				fechaFinalCom =  "28/"+mesFinalCom+"/"+anioFinal;
			}else{
				fechaFinalCom =  "30/"+mesFinalCom+"/"+anioFinal;
			}
					
			//esto es para los meses de cadenas		
			
			if(!mesInicial.equals(mesFinal)){ 		
				tamanio= 	Integer.parseInt(mesFinal) - Integer.parseInt(mesInicial) +1;
			}else{ 	
				tamanio =	Integer.parseInt(mesFinal); 		
			}
			for (int e = Integer.parseInt(mesInicial); e <= tamanio; e++) {
				 if(e==1){ mesesC ="ENE";}
				 if(e==2){ mesesC ="FEB";}
				 if(e==3){ mesesC ="MAR";}
				 if(e==4){ mesesC ="ABR";}
				 if(e==5){ mesesC ="MAY";}
				 if(e==6){ mesesC ="JUN";}
				 if(e==7){ mesesC ="JUL";}
				 if(e==8){ mesesC ="AGO";}
				 if(e==9){ mesesC ="SEP";}
				 if(e==10){ mesesC ="OCT";}
				 if(e==11){ mesesC ="NOV";}
				 if(e==12){ mesesC ="DIC";}						 
					mesesCon +="'"+mesesC+"-"+anioFinal+"',";							
			}		

			log.debug("mesesCon "+mesesCon);
			log.debug("mesesCon "+mesesCon);
			
			int tamanioC =	mesesCon.length()-1;
			String mesesConT =mesesCon.substring(0,tamanioC);	
			
			log.debug("mesesConT "+mesesConT);
						
			//OBTENGO EL NUMERO DE EPOS COMPRANET 
			query = new StringBuffer();
			query.append(" select  IC_EPO_COMPRANET FROM  comcat_epo_compranet ");
			query.append(" where   IG_NUMERO_EPO  in( "+seleciona+")");	
			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();				
			while(rs.next()){					
					epoSel.append(rs.getString("IC_EPO_COMPRANET")==null?"":rs.getString("IC_EPO_COMPRANET")+",");		
			}
			rs.close();
			ps.close();		
			epoSele  = epoSel.toString();
			if(!epoSele.equals("")){
				int tamanio2 =	epoSele.length()-1;
				epoSele =epoSele.substring(0,tamanio2);			
			}
			log.debug("epoSele "+epoSele);
			
			//Inicia la consulta
			query = new StringBuffer();
			query.append(" SELECT distinct ");				
			query.append(" nvl(y.cg_razon_social, u.cg_razon_social) AS nomPyme,  ");
			query.append(" nvl(y.cg_rfc, u.cg_rfc) AS rfcPyme,  ");
			query.append(" u.montoCo as montoCo, ");
			query.append(" y.montoCa as montoCa, ");
			query.append(" u.compranet as origenCo, ");
			query.append(" y.cadenas as origenCa");
			query.append(" from (  ");
					
			//esta parte del query pertenece a COMPRANET 
			query.append(" 				SELECT  distinct p.cg_razon_social , ");
			query.append(" 								p.cg_rfc ,");
			query.append(" 								sum(FN_IMPORTE_SIN_IVA) as montoCo, ");
			query.append("								c.ic_pyme_compranet, ");
			query.append("								'COMPRANET'as compranet ");
			query.append(" 				FROM com_cruce_compranet c, ");
			query.append(" 						 comcat_epo_compranet e, ");
			query.append(" 						 COMCAT_PYME_COMPRANET p, ");
			query.append(" 						 COMCAT_DEPENDENCIA_COMPRANET d ");
			query.append(" 				WHERE c.ig_clasif_dependencia = d.ic_clasif_dependencia ");
			query.append(" 				AND e.ic_epo_compranet = c.ic_epo_compranet ");
			query.append(" 				AND p.ic_pyme_compranet = c.ic_pyme_compranet ");				
			query.append("   			and e.cs_nuevo = 'N' ");
			query.append("  			and e.ig_clasif_dependencia != 0 "); 
  
			if(federales !=null && otros !=null ) {
				query.append(" 			and d.ic_clasif_dependencia in( "+federales+", "+ otros+")");
			}else if(federales !=null && otros ==null ) {
				query.append(" 			and d.ic_clasif_dependencia = "+federales);
			}else if(federales ==null && otros !=null ) {
				query.append("			and d.ic_clasif_dependencia = "+otros);			
			}	
			query.append(" 			and c.IC_EPO_COMPRANET in( "+epoSele+")");			
			query.append(" 				AND df_fecha_emision >= to_date('"+fechaInicialCom+"','dd/mm/yyyy') ");
			query.append(" 				AND df_fecha_emision <= to_date('"+fechaFinalCom+"','dd/mm/yyyy')+1 ");
			query.append(" 				GROUP BY p.cg_razon_social,   ");
			query.append(" 								 c.ic_pyme_compranet,  ");
			query.append(" 								 p.cg_rfc )  u,  ");
				
			//esta parte pertenece a CADENAS  					 
			query.append(" (SELECT  distinct  p.cg_razon_social, ");
			query.append(" 					c.ic_pyme, ");
			query.append(" 					c.cg_rfc, ");
			query.append("					'CADENAS'as cadenas , ");					
			query.append(" 	SUM (c.fn_monto)AS montoca "); 						 					 
			query.append(" 					FROM com_cruce_cadenas c, ");
			query.append(" 							 comcat_depen_cadenas d, ");
			query.append(" 							 comcat_epo e, ");
			query.append(" 							 comcat_pyme p   ");
			query.append(" 					WHERE c.ic_epo = e.ic_epo ");
			query.append(" 					AND c.ic_pyme = p.ic_pyme ");
			query.append(" 					AND c.ic_dependencia = d.ic_dependencia ");
			query.append(" 					AND c.ic_dependencia = "+clasificacion+" ");
			query.append(" 					AND c.cg_mes IN ("+mesesConT+" ) ");											
			query.append(" 					GROUP BY p.cg_razon_social,   ");
			query.append(" 									 c.ic_pyme,  ");
			query.append(" 									 c.cg_rfc  ");
			query.append(" 									  ) y    ");					
			query.append(" WHERE y.cg_rfc(+) = u.cg_rfc   ");	
			
			query.append(" union   ");	
		
			query.append(" SELECT distinct ");				
			query.append(" nvl(y.cg_razon_social, u.cg_razon_social) AS nomPyme,  ");
			query.append(" nvl(y.cg_rfc, u.cg_rfc) AS rfcPyme,  ");
			query.append(" u.montoCo as montoCo, ");
			query.append(" y.montoCa as montoCa, ");
			query.append(" u.compranet as origenCo, ");
			query.append(" y.cadenas as origenCa");
			query.append(" from (  ");
				
			//esta parte del query pertenece a COMPRANET 
			query.append(" 				SELECT  distinct p.cg_razon_social , ");
			query.append(" 								p.cg_rfc ,");
			query.append(" 								sum(FN_IMPORTE_SIN_IVA) as montoCo, ");
			query.append("								c.ic_pyme_compranet, ");
			query.append("								'COMPRANET'as compranet ");
			query.append(" 				FROM com_cruce_compranet c, ");
			query.append(" 						 comcat_epo_compranet e, ");
			query.append(" 						 COMCAT_PYME_COMPRANET p, ");
			query.append(" 						 COMCAT_DEPENDENCIA_COMPRANET d ");
			query.append(" 				WHERE c.ig_clasif_dependencia = d.ic_clasif_dependencia ");
			query.append(" 				AND e.ic_epo_compranet = c.ic_epo_compranet ");
			query.append(" 				AND p.ic_pyme_compranet = c.ic_pyme_compranet ");				
			query.append("   			and e.cs_nuevo = 'N' ");
			query.append("  			and e.ig_clasif_dependencia != 0 "); 

			if(federales !=null && otros !=null ) {
				query.append(" 			and d.ic_clasif_dependencia in( "+federales+", "+ otros+")");
			}else if(federales !=null && otros ==null ) {
				query.append(" 			and d.ic_clasif_dependencia = "+federales);
			}else if(federales ==null && otros !=null ) {
				query.append("			and d.ic_clasif_dependencia = "+otros);			
			}	
			query.append(" 			and c.IC_EPO_COMPRANET in( "+epoSele+")");			
			query.append(" 				AND df_fecha_emision >= to_date('"+fechaInicialCom+"','dd/mm/yyyy') ");
			query.append(" 				AND df_fecha_emision <= to_date('"+fechaFinalCom+"','dd/mm/yyyy')+1 ");
			query.append(" 				GROUP BY p.cg_razon_social,   ");
			query.append(" 								 c.ic_pyme_compranet,  ");
			query.append(" 								 p.cg_rfc )  u,  ");
					
			//esta parte pertenece a CADENAS  					 
			query.append(" (SELECT  distinct  p.cg_razon_social, ");
			query.append(" 					c.ic_pyme, ");
			query.append(" 					c.cg_rfc, ");
			query.append("					'CADENAS'as cadenas , ");					
			query.append(" 	SUM (c.fn_monto)AS montoca "); 						 					 
			query.append(" 					FROM com_cruce_cadenas c, ");
			query.append(" 							 comcat_depen_cadenas d, ");
			query.append(" 							 comcat_epo e, ");
			query.append(" 							 comcat_pyme p   ");
			query.append(" 					WHERE c.ic_epo = e.ic_epo ");
			query.append(" 					AND c.ic_pyme = p.ic_pyme ");
			query.append(" 					AND c.ic_dependencia = d.ic_dependencia ");
			query.append(" 					AND c.ic_dependencia = "+clasificacion+" ");
			query.append(" 					AND c.cg_mes IN ("+mesesConT+" ) ");											
			query.append(" 					GROUP BY p.cg_razon_social,   ");
			query.append(" 									 c.ic_pyme,  ");
			query.append(" 									 c.cg_rfc  ");
			query.append(" 									  ) y    ");					
			query.append(" WHERE y.cg_rfc = u.cg_rfc(+)   ");				
			
		
			log.debug("query consulta Cadena : " +query.toString());
	
			ps = con.queryPrecompilado(query.toString());				
			rs = ps.executeQuery();	
				
			// DEFINIR CABECERA
			csv.write("PYME , RFC, Importe, Origen ");
			csv.write("\r\n");
					
			long i = 0L;
			double suma  = 0;
			int hayRegsitros = 0;
			while (rs.next()) {
								
				String nombrePyme = (rs.getString("nomPyme")==null?"":rs.getString("nomPyme"));
				String rfcPyme = (rs.getString("rfcPyme")==null?"":rs.getString("rfcPyme").replace('-', ' '));
				String montoCo = rs.getString("montoCo")==null?"0":rs.getString("montoCo");
				String montoCa = rs.getString("montoCa")==null?"0":rs.getString("montoCa");
				suma = Double.parseDouble((String)montoCo) + Double.parseDouble((String)montoCa);
				String origenCo = rs.getString("origenCo")==null?"":rs.getString("origenCo");
				String origenCa = rs.getString("origenCa")==null?"":rs.getString("origenCa");
				String origen= "";
				if(!origenCo.equals("") && !origenCa.equals("")){
					origen ="AMBAS";
				}else if(!origenCo.equals("") && origenCa.equals("")){
					origen =" COMPRANET ";
				}else if(origenCo.equals("") && !origenCa.equals("")){
					origen ="CADENAS";
				}			
				if(!nombrePyme.equals("")){
					csv.write(nombrePyme.replace(',', ' ') );
					csv.write( ","+ rfcPyme.replace(',', ' '));
					csv.write( ","+suma);
					csv.write( ","+origen );
					csv.write("\r\n");
				}	
				if((i%500L) == 0L) csv.flush(); 
					i++;
					suma =0; //se reseta el valor de la suma de los valores
					hayRegsitros ++;
			}

			//CUANDO NO HAY REGISTROS
			if(hayRegsitros==0){
				csv.write( " No se encontr� ning�n registro para los criter�os seleccionados. " );						
				csv.write( "\r\n");							
			}				
			
			if(csv 	!= null )  csv.close();
			
			file = new File(strDirectorioTemp + nombreArchivo);					
			
		}catch(Exception e){
			commit = false;			
			e.printStackTrace();
			file = new File(strDirectorioPlantillas + "error.csv");
			nombreArchivo = "error.csv";
			log.error("getGenerarRFCunicosCadCompranet(Exception)"+e);
		}finally{
			if (rs != null) try { rs.close(); 	} catch(SQLException e) {}
			if (ps != null) try { ps.close(); 	} catch(SQLException e) {}
			if (csv != null ) try { csv.close();   } catch(Exception    e) {}
			con.terminaTransaccion(commit);
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("getGenerarRFCunicosCadCompranet(s)");
		}
		
		return nombreArchivo;
	}
	
	
	/**
	 * regresa  una lista de los campos con los meses  ingresados en  com_cruce_cadenas
	 * @return
	 */
	public List getCatMesesCadenas(){

		log.info("getCatMesesCadenas(E)");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList datos = new ArrayList();
		ArrayList coleccionElementos = new ArrayList();
		StringBuffer query = new StringBuffer();
		String  clave = "";
		String  descripcion = "";
		Calendar cal = Calendar.getInstance();
		int sAnioIni = cal.get(Calendar.YEAR);

		try{
			con.conexionDB();


			query.append(" select distinct SUBSTR (cg_mes, 1, 3) as mes, CG_NUMES" );
			query.append(" from com_cruce_cadenas ");
			query.append("  where  SUBSTR (cg_mes, 5, 7) ="+sAnioIni);
			query.append(" order by  CG_NUMES ");


			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();

			log.debug(query.toString());

			while(rs.next()){
				 if(rs.getString("mes").equals("ENE")) {  clave ="1"; descripcion = "Enero";    }
				 if(rs.getString("mes").equals("FEB")) {  clave ="2";  descripcion= "Febrero";  }
				 if(rs.getString("mes").equals("MAR")) {  clave ="3";  descripcion= "Marzo";  }
				 if(rs.getString("mes").equals("ABR")) {  clave ="4";  descripcion= "Abril";  }
				 if(rs.getString("mes").equals("MAY")) {  clave ="5";  descripcion= "Mayo";  }
				 if(rs.getString("mes").equals("JUN")) {  clave ="6";  descripcion= "Junio";  }
				 if(rs.getString("mes").equals("JUL")) {  clave ="7";  descripcion= "Julio";  }
				 if(rs.getString("mes").equals("AGO")) {  clave ="8";  descripcion= "Agosto";  }
				 if(rs.getString("mes").equals("SEP")) {  clave ="9";  descripcion= "Septiembre";  }
				 if(rs.getString("mes").equals("OCT")) {  clave ="10";  descripcion= "Octubre";   }
				 if(rs.getString("mes").equals("NOV")) {  clave ="11";  descripcion= "Noviembre";  }
				 if(rs.getString("mes").equals("DIC")) {  clave ="12";  descripcion= "Diciembre";  }

					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(clave);
					elementoCatalogo.setDescripcion(descripcion);
					coleccionElementos.add(elementoCatalogo);
				}
			rs.close();
			ps.close();


			return coleccionElementos;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error  getCatMesesCadenas", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getCatMesesCadenas(S)");
		}

	}


	/**
	 * regresa  una lista de los campos con los meses  ingresados en  COM_CRUCE_COMPRANET
	 * @return
	 */
	public List getCatMesesCompranet(){

		log.info("getCatMesesCompranet(E)");

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList datos = new ArrayList();
		ArrayList coleccionElementos = new ArrayList();
		StringBuffer query = new StringBuffer();
		String  clave = "";
		String  descripcion = "";
		Calendar cal = Calendar.getInstance();
		int sAnioIni = cal.get(Calendar.YEAR);

		try{
			con.conexionDB();

			query.append(" select distinct TO_CHAR(df_fecha_emision,'mm') as mes" );
			query.append(" from COM_CRUCE_COMPRANET ");
			query.append(" where TO_CHAR(df_fecha_emision,'yyyy')  = "+sAnioIni);
			query.append(" order by  mes ");

			ps = con.queryPrecompilado(query.toString());
			rs = ps.executeQuery();

			log.debug(query.toString());

			while(rs.next()){
				 if(rs.getString("mes").equals("01")) {  clave ="1"; descripcion = "Enero";    }
				 if(rs.getString("mes").equals("02")) {  clave ="2";  descripcion= "Febrero";  }
				 if(rs.getString("mes").equals("03")) {  clave ="3";  descripcion= "Marzo";  }
				 if(rs.getString("mes").equals("04")) {  clave ="4";  descripcion= "Abril";  }
				 if(rs.getString("mes").equals("05")) {  clave ="5";  descripcion= "Mayo";  }
				 if(rs.getString("mes").equals("06")) {  clave ="6";  descripcion= "Junio";  }
				 if(rs.getString("mes").equals("07")) {  clave ="7";  descripcion= "Julio";  }
				 if(rs.getString("mes").equals("08")) {  clave ="8";  descripcion= "Agosto";  }
				 if(rs.getString("mes").equals("09")) {  clave ="9";  descripcion= "Septiembre";  }
				 if(rs.getString("mes").equals("10")) {  clave ="10";  descripcion= "Octubre";   }
				 if(rs.getString("mes").equals("11")) {  clave ="11";  descripcion= "Noviembre";  }
				 if(rs.getString("mes").equals("12")) {  clave ="13";  descripcion= "Diciembre";  }

					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(clave);
					elementoCatalogo.setDescripcion(descripcion);
					coleccionElementos.add(elementoCatalogo);
				}
				rs.close();
				ps.close();


			return coleccionElementos;

		}catch(Exception e){
			e.printStackTrace();
			throw new AppException("Error  getCatMesesCompranet", e);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("getCatMesesCompranet(S)");
		}

	}
	
	
	public String getGenerarRFCconGarant(Map hm){
		log.info("getGenerarRFCconGarant(E)");
 
		AccesoDB 			con								= new AccesoDB();
    	PreparedStatement ps 								= null;
    	ResultSet 			rs 								= null;
		StringBuffer      query 							= new StringBuffer();
		
		File 					file 								= null;
		String 				strDirectorioPublicacion 	= (String)hm.get("directorioPublicacion");
		String 				strDirectorioPlantillas 	= (String)hm.get("directorioPlantillas");
		String 				strDirectorioTemp 			= strDirectorioPublicacion + "00tmp/15cadenas/";
 
		CreaArchivo 		archivo 							= null;
		String 				nombreArchivo					= null;
		FileOutputStream 	out 								= null;
		BufferedWriter 	csv 								= null;

		try{
			
				con.conexionDB();

				archivo 			= new CreaArchivo();
				//nombreArchivo	= archivo.nombreArchivo()+".csv";
				nombreArchivo	= "RFCsConGarantias.csv";
				out 				= new FileOutputStream(strDirectorioTemp+nombreArchivo);
				csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));

				// Obtener las fechas para restringir la busqueda
				int 			   anioActual 					= Fecha.getAnioActual();
				String 			fechaMesInicial 			= getPrimerDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesInicial")));
				String 			ultimoDiaMesFinal			= getUltimoDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesFinal")));
				String 			fechaMesInicialSiag 		= getPrimerDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesInicialSiag")));
				String 			ultimoDiaMesFinalSiag	= getUltimoDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesFinalSiag")));
				
				// Obtener la lista de Epos que restringiran la consulta
				String 			eposSeleccionadas 		= (String)hm.get("eposSeleccionadas");
				String[]			listaEposSeleccionadas	= null;
				if(eposSeleccionadas != null && !eposSeleccionadas.equals("")){
					listaEposSeleccionadas = eposSeleccionadas.split(",");
				}
				listaEposSeleccionadas = listaEposSeleccionadas == null?new String[0]:listaEposSeleccionadas;
				
				// Obtener las variables bind
				StringBuffer 	variablesBind				= new StringBuffer();
				for(int i=0;i<listaEposSeleccionadas.length;i++){
					if(i>0) variablesBind.append(",");
					variablesBind.append("?");
				}
				
				// Obtener lista de portafolios
				String[]		listaPortafolios 				= ((String)hm.get("portafolio")).split(",");
				
				// Obtener las variables bind para el portafolio
				StringBuffer 	variablesBindPortafolio		= new StringBuffer();
				for(int i=0;i<listaPortafolios.length;i++){
					if(i>0) variablesBindPortafolio.append(",");
					variablesBindPortafolio.append("?");
				}
				
				// Realizar la consulta
				query.append(
					"SELECT 																									"  +
					"  EPO_COMPRANET.CG_RAZON_SOCIAL AS NOMBRE_EPO, 											"  +
					"  COMPRANET.CG_LICITACION, 																		"  +
					"  COMPRANET.CG_UNIDAD, 																			"  +
					"  COMPRANET.CG_TIPO_CONTRATACION, 																"  +
					"	TO_CHAR(COMPRANET.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION,				"  +
					"  COMPRANET.CG_NUM_PARTIDA, 																		"  +
					"  COMPRANET.FN_IMPORTE_SIN_IVA, 																"  +
					"  PYME_COMPRANET.CG_RAZON_SOCIAL AS NOMBRE_PROVEEDOR, 									"  +
					"  PYME_COMPRANET.CG_RFC AS RFC_PROVEEDOR, 													"  +
					"  COMPRANET.IG_CLASIF_DEPENDENCIA 																"  +
					"FROM 																									"  + 
					"  COM_CRUCE_COMPRANET 		COMPRANET, 															"  + 
					"  COMCAT_PYME_COMPRANET 	PYME_COMPRANET, 													"  + 
					"  COMCAT_EPO_COMPRANET 	EPO_COMPRANET 														"  +
					"WHERE 																									"  +
					"  COMPRANET.IC_PYME_COMPRANET 		 = 	PYME_COMPRANET.IC_PYME_COMPRANET 		"  +
					"  AND COMPRANET.IC_EPO_COMPRANET 	 = 	EPO_COMPRANET.IC_EPO_COMPRANET 			"  +
					// Reestringir registros por el periodo indicado en la Fecha de Emision de Fallo
					"  AND COMPRANET.DF_FECHA_EMISION    >= 	TO_DATE(?,'DD/MM/YYYY') 					"  +
					"  AND COMPRANET.DF_FECHA_EMISION    < 	TO_DATE(?,'DD/MM/YYYY') +1 				"  +
					// Solo realizar cruce de informacion para las Epos Compranet Seleccionadas
					"  AND EPO_COMPRANET.IG_NUMERO_EPO 	 IN   ("+variablesBind.toString()+")      	"  +
					"  AND PYME_COMPRANET.CG_RFC IN (	                                          	"  +
					"		SELECT DISTINCT 																				"  +
					"			CG_RFC 																						"  +
					"		FROM 																								"  +
					"			COM_CRUCE_SIAG SIAG																		"  +
					"		WHERE																								"  +
					// Solo traer rfcs para el periodo indicado
					"  		SIAG.DF_FECHA_REGISTRO    	 >= TO_DATE(?,'DD/MM/YYYY') 						"  +
					"  		AND SIAG.DF_FECHA_REGISTRO  <  TO_DATE(?,'DD/MM/YYYY') +1 					"  +
					// Restring registros en base a su portafolio especificado
					"			AND SIAG.CG_PORTAFOLIO		IN ("+variablesBindPortafolio.toString()+")	"  +
					"  ) 	"  
				);
				
				query.append("ORDER BY COMPRANET.DF_FECHA_EMISION, NOMBRE_EPO, NOMBRE_PROVEEDOR ASC ");
				
				int indice = 1;
				ps = con.queryPrecompilado(query.toString());
				// Agregar periodo Compranet
				ps.setString(indice++, fechaMesInicial);
				ps.setString(indice++, ultimoDiaMesFinal);
				// Agregar lista de Epos Compranet con respecto a las cuales se realizara el cruce
				for(int i=0;i<listaEposSeleccionadas.length;i++){
					ps.setInt(indice++, Integer.parseInt(listaEposSeleccionadas[i]));
				}
				// Agregar periodo Siag
				ps.setString(indice++, fechaMesInicialSiag);
				ps.setString(indice++, ultimoDiaMesFinalSiag);
				// Agregar parametros correspondientes al portafolio
				for(int i=0;i<listaPortafolios.length;i++){
					System.err.println("listaPortafolios["+i+"] = "+ listaPortafolios[i]);
					ps.setString(indice++,listaPortafolios[i]);
				}
				rs = ps.executeQuery();
 
				// DEFINIR CABECERA
				csv.write("NOMBRE DE LA EPO, LICITACI�N, UNIDAD, TIPO DE CONTRATACI�N, FECHA DE EMISI�N FALLO, N�MERO DE LA PARTIDA, IMPORTE SIN IVA, NOMBRE DEL PROVEEDOR, RFC DEL PROVEEDOR, CLASIFICACI�N DE LA DEPENDENCIA\n");

				long i = 0L;
				while (rs != null && rs.next()) {
					
					csv.write("\"");csv.write(getCampo(rs,"NOMBRE_EPO"));								csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"CG_LICITACION"));							csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"CG_UNIDAD"));								csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"CG_TIPO_CONTRATACION"));				csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"FECHA_EMISION"));							csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"CG_NUM_PARTIDA"));						csv.write("\",");
					csv.write("\"");csv.write(getMontoConFormato(rs,"FN_IMPORTE_SIN_IVA"));		csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"NOMBRE_PROVEEDOR"));						csv.write("\",");
					csv.write("\"");csv.write(getRfcFiltrado(rs,"RFC_PROVEEDOR"));					csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"IG_CLASIF_DEPENDENCIA"));				csv.write("\",");
					csv.write("\n");
						
					if((i%500L) == 0L) { csv.flush(); i=0L; System.gc(); } 
					i++;
				}

				if(csv 	!= null ) csv.close();
				
				file = new File(strDirectorioTemp + nombreArchivo);
 
		}catch(Exception e){
			log.error("getGenerarRFCconGarant(Exception)");
			e.printStackTrace();
			file = new File(strDirectorioPlantillas + "error.csv");
		}finally{
			if (rs 	!= null)	try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null)	try { ps.close(); 	} catch(SQLException e) {}
			if (csv 	!= null)	try { csv.close();   } catch(Exception    e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("getGenerarRFCconGarant(s)");
		}
		
		return nombreArchivo;
	}
	
	public String getGenerarRFCconSinGarant(Map hm){
		log.info("getGenerarRFCconSinGarant(E)");
 
		AccesoDB 			con								= new AccesoDB();
    	PreparedStatement ps 								= null;
    	ResultSet 			rs 								= null;
		StringBuffer      query 							= new StringBuffer();
		
		File 					file 								= null;
		String 				strDirectorioPublicacion 	= (String)hm.get("directorioPublicacion");
		String 				strDirectorioPlantillas 	= (String)hm.get("directorioPlantillas");
		String 				strDirectorioTemp 			= strDirectorioPublicacion + "00tmp/15cadenas/";
 
		CreaArchivo 		archivo 							= null;
		String 				nombreArchivo					= null;
		FileOutputStream 	out 								= null;
		BufferedWriter 	csv 								= null;

		try{
			
				con.conexionDB();

				archivo 			= new CreaArchivo();
				//nombreArchivo	= archivo.nombreArchivo()+".csv";
				nombreArchivo	= "RFCsConSinGarantias.csv";
				out 				= new FileOutputStream(strDirectorioTemp+nombreArchivo);
				csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));

				// Obtener las fechas para restringir la busqueda
				int 			   anioActual 					= Fecha.getAnioActual();
				String 			fechaMesInicial 			= getPrimerDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesInicial")));
				String 			ultimoDiaMesFinal			= getUltimoDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesFinal")));
				String 			fechaMesInicialSiag 		= getPrimerDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesInicialSiag")));
				String 			ultimoDiaMesFinalSiag	= getUltimoDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesFinalSiag")));
				
				// Obtener la lista de Epos que restringiran la consulta
				String 			eposSeleccionadas 		= (String)hm.get("eposSeleccionadas");
				String[]			listaEposSeleccionadas	= null;
				if(eposSeleccionadas != null && !eposSeleccionadas.equals("")){
					listaEposSeleccionadas = eposSeleccionadas.split(",");
				}
				listaEposSeleccionadas = listaEposSeleccionadas == null?new String[0]:listaEposSeleccionadas;
				
				// Obtener las variables bind
				StringBuffer 	variablesBind				= new StringBuffer();
				for(int i=0;i<listaEposSeleccionadas.length;i++){
					if(i>0) variablesBind.append(",");
					variablesBind.append("?");
				}
				
				// Obtener lista de portafolios
				String[]		listaPortafolios 				= ((String)hm.get("portafolio")).split(",");
				
				// Obtener las variables bind para el portafolio
				StringBuffer 	variablesBindPortafolio		= new StringBuffer();
				for(int i=0;i<listaPortafolios.length;i++){
					if(i>0) variablesBindPortafolio.append(",");
					variablesBindPortafolio.append("?");
				}
				
				// Realizar la consulta
				query.append(
					"SELECT 																									"  +
					"  EPO_COMPRANET.CG_RAZON_SOCIAL AS NOMBRE_EPO, 											"  +
					"  COMPRANET.CG_LICITACION, 																		"  +
					"  COMPRANET.CG_UNIDAD, 																			"  +
					"  COMPRANET.CG_TIPO_CONTRATACION, 																"  +
					"	TO_CHAR(COMPRANET.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION,				"  +
					"  COMPRANET.CG_NUM_PARTIDA, 																		"  +
					"  COMPRANET.FN_IMPORTE_SIN_IVA, 																"  +
					"  PYME_COMPRANET.CG_RAZON_SOCIAL AS NOMBRE_PROVEEDOR, 									"  +
					"  PYME_COMPRANET.CG_RFC AS RFC_PROVEEDOR, 													"  +
					"  COMPRANET.IG_CLASIF_DEPENDENCIA, 															"  +
					"	COMPRANET.DF_FECHA_EMISION,																	"  +
					"  'Si' AS GARANTIAS 																				"  +
					"FROM 																									"  + 
					"  COM_CRUCE_COMPRANET 		COMPRANET, 															"  + 
					"  COMCAT_PYME_COMPRANET 	PYME_COMPRANET, 													"  + 
					"  COMCAT_EPO_COMPRANET 	EPO_COMPRANET 														"  +
					"WHERE 																									"  +
					"  COMPRANET.IC_PYME_COMPRANET 		 = 	PYME_COMPRANET.IC_PYME_COMPRANET 		"  +
					"  AND COMPRANET.IC_EPO_COMPRANET 	 = 	EPO_COMPRANET.IC_EPO_COMPRANET 			"  +
					// Reestringir registros por el periodo indicado en la Fecha de Emision de Fallo
					"  AND COMPRANET.DF_FECHA_EMISION    >= 	TO_DATE(?,'DD/MM/YYYY') 					"  +
					"  AND COMPRANET.DF_FECHA_EMISION    < 	TO_DATE(?,'DD/MM/YYYY') +1 				"  +
					// Solo realizar cruce de informacion para las Epos Compranet Seleccionadas
					"  AND EPO_COMPRANET.IG_NUMERO_EPO 	 IN   ("+variablesBind.toString()+")      	"  +
					"  AND PYME_COMPRANET.CG_RFC IN (	                                          	"  +
					"		SELECT DISTINCT 																				"  +
					"			CG_RFC 																						"  +
					"		FROM 																								"  +
					"			COM_CRUCE_SIAG SIAG																		"  +
					"		WHERE																								"  +
					// Solo traer rfcs para el periodo indicado
					"  		SIAG.DF_FECHA_REGISTRO    	 >= TO_DATE(?,'DD/MM/YYYY') 						"  +
					"  		AND SIAG.DF_FECHA_REGISTRO  <  TO_DATE(?,'DD/MM/YYYY') +1 					"  +
					// Restring registros en base a su portafolio especificado
					"			AND SIAG.CG_PORTAFOLIO		IN ("+variablesBindPortafolio.toString()+")	"  +
					"  ) 																										"  +
					"UNION ALL 																								"  +
					"SELECT 																									"  +
					"  EPO_COMPRANET.CG_RAZON_SOCIAL AS NOMBRE_EPO, 											"  +
					"  COMPRANET.CG_LICITACION, 																		"  +
					"  COMPRANET.CG_UNIDAD, 																			"  +
					"  COMPRANET.CG_TIPO_CONTRATACION, 																"  +
					"	TO_CHAR(COMPRANET.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION,				"  +
					"  COMPRANET.CG_NUM_PARTIDA, 																		"  +
					"  COMPRANET.FN_IMPORTE_SIN_IVA, 																"  +
					"  PYME_COMPRANET.CG_RAZON_SOCIAL AS NOMBRE_PROVEEDOR, 									"  +
					"  PYME_COMPRANET.CG_RFC AS RFC_PROVEEDOR, 													"  +
					"  COMPRANET.IG_CLASIF_DEPENDENCIA, 															"  +
					"	COMPRANET.DF_FECHA_EMISION,																	"  +
					"  'NO' AS GARANTIAS 																				"  +
					"FROM 																									"  + 
					"  COM_CRUCE_COMPRANET 		COMPRANET, 															"  + 
					"  COMCAT_PYME_COMPRANET 	PYME_COMPRANET, 													"  + 
					"  COMCAT_EPO_COMPRANET 	EPO_COMPRANET 														"  +
					"WHERE 																									"  +
					"  COMPRANET.IC_PYME_COMPRANET 		 = 	PYME_COMPRANET.IC_PYME_COMPRANET 		"  +
					"  AND COMPRANET.IC_EPO_COMPRANET 	 = 	EPO_COMPRANET.IC_EPO_COMPRANET 			"  +
					// Reestringir registros por el periodo indicado en la Fecha de Emision de Fallo
					"  AND COMPRANET.DF_FECHA_EMISION    >= 	TO_DATE(?,'DD/MM/YYYY') 					"  +
					"  AND COMPRANET.DF_FECHA_EMISION    < 	TO_DATE(?,'DD/MM/YYYY') +1 				"  +
					// Solo realizar cruce de informacion para las Epos Compranet Seleccionadas
					"  AND EPO_COMPRANET.IG_NUMERO_EPO 	 IN   ("+variablesBind.toString()+")      	"  +
					"  AND PYME_COMPRANET.CG_RFC NOT IN (	                                         	"  +
					"		SELECT DISTINCT 																				"  +
					"			CG_RFC 																						"  +
					"		FROM 																								"  +
					"			COM_CRUCE_SIAG SIAG																		"  +
					"		WHERE																								"  +
					// Solo traer rfcs para el periodo indicado
					"  		SIAG.DF_FECHA_REGISTRO    	 >= TO_DATE(?,'DD/MM/YYYY') 						"  +
					"  		AND SIAG.DF_FECHA_REGISTRO  <  TO_DATE(?,'DD/MM/YYYY') +1 					"  +
					"  ) 	"
				);
				
				query.append("ORDER BY DF_FECHA_EMISION, NOMBRE_EPO, NOMBRE_PROVEEDOR ASC ");
				
				int indice = 1;
				ps = con.queryPrecompilado(query.toString());
				// Agregar periodo Compranet
				ps.setString(indice++, fechaMesInicial);
				ps.setString(indice++, ultimoDiaMesFinal);
				// Agregar lista de Epos Compranet con respecto a las cuales se realizara el cruce
				for(int i=0;i<listaEposSeleccionadas.length;i++){
					ps.setInt(indice++, Integer.parseInt(listaEposSeleccionadas[i]));
				}
				// Agregar periodo Siag
				ps.setString(indice++, fechaMesInicialSiag);
				ps.setString(indice++, ultimoDiaMesFinalSiag);
				// Agregar parametros correspondientes al portafolio
				for(int i=0;i<listaPortafolios.length;i++){
					System.err.println("listaPortafolios["+i+"] = "+ listaPortafolios[i]);
					ps.setString(indice++,listaPortafolios[i]);
				}
				// Agregar periodo Compranet
				ps.setString(indice++, fechaMesInicial);
				ps.setString(indice++, ultimoDiaMesFinal);
				// Agregar lista de Epos Compranet con respecto a las cuales se realizara el cruce
				for(int i=0;i<listaEposSeleccionadas.length;i++){
					ps.setInt(indice++, Integer.parseInt(listaEposSeleccionadas[i]));
				}
				// Agregar periodo Siag
				ps.setString(indice++, fechaMesInicialSiag);
				ps.setString(indice++, ultimoDiaMesFinalSiag);
				rs = ps.executeQuery();
 
				// DEFINIR CABECERA
				csv.write("NOMBRE DE LA EPO, LICITACI�N, UNIDAD, TIPO DE CONTRATACI�N, FECHA DE EMISI�N FALLO, N�MERO DE LA PARTIDA, IMPORTE SIN IVA, NOMBRE DEL PROVEEDOR, RFC DEL PROVEEDOR, CLASIFICACI�N DE LA DEPENDENCIA, GARANT�AS\n");

				long i = 0L;
				while (rs != null && rs.next()) {
					
					csv.write("\"");csv.write(getCampo(rs,"NOMBRE_EPO"));								csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"CG_LICITACION"));							csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"CG_UNIDAD"));								csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"CG_TIPO_CONTRATACION"));				csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"FECHA_EMISION"));							csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"CG_NUM_PARTIDA"));						csv.write("\",");
					csv.write("\"");csv.write(getMontoConFormato(rs,"FN_IMPORTE_SIN_IVA"));		csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"NOMBRE_PROVEEDOR"));						csv.write("\",");
					csv.write("\"");csv.write(getRfcFiltrado(rs,"RFC_PROVEEDOR"));					csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"IG_CLASIF_DEPENDENCIA"));				csv.write("\",");
					csv.write("\"");csv.write(getCampo(rs,"GARANTIAS"));								csv.write("\",");
					csv.write("\n");
						
					if((i%500L) == 0L) { csv.flush(); i=0L; System.gc(); } 
					i++;
				}

				if(csv 	!= null ) csv.close();
				
				file = new File(strDirectorioTemp + nombreArchivo);
 
		}catch(Exception e){
			log.error("getGenerarRFCconSinGarant(Exception)");
			e.printStackTrace();
			file = new File(strDirectorioPlantillas + "error.csv");
		}finally{
			if (rs 	!= null)	try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null)	try { ps.close(); 	} catch(SQLException e) {}
			if (csv 	!= null)	try { csv.close();   } catch(Exception    e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("getGenerarRFCconSinGarant(s)");
		}
		
		return nombreArchivo;
	}
	
	/**
	 * Genera Archivo para descargar del Cruce de Informacion entre Cadenas-Compranet-SIAG
	 * (Opcion = Generar RFCs unicos)
	 * @param hm
	 * @return
	 */
	public String getGenerarRFCunicosCadCompSiag(Map hm){
		log.info("getStreamInfo(E)");
 
		AccesoDB 			con								= new AccesoDB();
    	PreparedStatement ps 								= null;
    	ResultSet 			rs 								= null;
		StringBuffer      query 							= new StringBuffer();
		
		File 					file 								= null;
		String 				strDirectorioPublicacion 	= (String)hm.get("directorioPublicacion");
		String 				strDirectorioPlantillas 	= (String)hm.get("directorioPlantillas");
		String 				strDirectorioTemp 			= strDirectorioPublicacion + "00tmp/15cadenas/";
 
		CreaArchivo 		archivo 							= null;
		String 				nombreArchivo					= null;
		FileOutputStream 	out 								= null;
		BufferedWriter 	csv 								= null;

		try{
			
					con.conexionDB();
 
					archivo 		= new CreaArchivo();
					nombreArchivo	= "RFCsUnicos.csv";
					out 			= new FileOutputStream(strDirectorioTemp+nombreArchivo);
					csv 			= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
 
					// Obtener las fechas para restringir la busqueda
					String idProcesoCruce	= (String)hm.get("idProcesoCruce");
 
					// Realizar la consulta
					query.append(
						"SELECT  														"  +
						"	DECODE(CRUCE.CS_ORIGEN,'T',PYME_COMPRANET.CG_RAZON_SOCIAL,PYME_CADENAS.CG_RAZON_SOCIAL) "  + 
						"							AS PYME, 							"  +
						"	CRUCE.CG_RFC 		AS RFC, 								"  +
						"	CRUCE.FN_IMPORTE 	AS IMPORTE,  						"  +
						"	DECODE(CRUCE.CS_ORIGEN,'T','COMPRANET','C','CADENAS','A','AMBAS') "  + 
						"							AS ORIGEN 							"  +
						"FROM  															"  +
						"	COMTMP_CRUCE_CADENAS_COMPRANET 	CRUCE , 			"  +
						"	COMCAT_PYME             			PYME_CADENAS, 	"  +
						"	COMCAT_PYME_COMPRANET   			PYME_COMPRANET "  +
						"WHERE  															"  +
						"	CRUCE.CG_RFC = PYME_CADENAS.CG_RFC(+) 		AND 	"  +
						"	CRUCE.CG_RFC = PYME_COMPRANET.CG_RFC(+) 	AND	"  +
						"	CRUCE.IC_PROCESO	= ?									"  +
						//"	CRUCE.CG_RFC != 'NULL' "  +
						"ORDER BY PYME													"
					);
 
					ps = con.queryPrecompilado(query.toString());
					ps.setInt(1,Integer.parseInt(idProcesoCruce));
					rs = ps.executeQuery();
	 
					// DEFINIR CABECERA
					csv.write("PYME,RFC,Importe,Origen\n");
	
					long i = 0L;
					while (rs != null && rs.next()) {
						
						csv.write("\"");csv.write(getCampo(rs,			"PYME"));				csv.write("\",");
						csv.write("\"");csv.write(getRfcFiltrado(rs,	"RFC"));					csv.write("\",");
						csv.write("\"");csv.write(getMontoConFormato(rs,"IMPORTE"));		csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"ORIGEN"));				csv.write("\",");
						csv.write("\n");
							
						if((i%500L) == 0L) { csv.flush(); i=0L; System.gc(); } 
						i++;
					}
	
					if(csv 	!= null ) csv.close();
					
					file = new File(strDirectorioTemp + nombreArchivo);
 
		}catch(Exception e){
			log.error("getGenerarRFCunicosCadCompSiag(Exception)");
			e.printStackTrace();
			file = new File(strDirectorioPlantillas + "error.csv");
		}finally{
			if (rs 	!= null)	try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null)	try { ps.close(); 	} catch(SQLException e) {}
			if (csv 	!= null)	try { csv.close();   } catch(Exception    e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB(); 
			log.info("getGenerarRFCunicosCadCompSiag(s)");
		}
		return nombreArchivo;
	}
	
	
	/**
	 * se obtiene archivo generado por el cruce de informacion de CADENAS-COMPRANET-SIAG (RFC's unicos con garantia)
	 * @param hm
	 * @return
	 */
	public String getDescargarRFCunicosConGarant(Map hm){
		
		log.info("getDescargarRFCunicosConGarant(E)");

		AccesoDB 			con								= new AccesoDB();
    	PreparedStatement ps 								= null;
    	ResultSet 			rs 								= null;
		StringBuffer      query 							= new StringBuffer();

		File 					file 								= null;
		String 				strDirectorioPublicacion 	= (String)hm.get("directorioPublicacion");
		String 				strDirectorioPlantillas 	= (String)hm.get("directorioPlantillas");
		String 				strDirectorioTemp 			= strDirectorioPublicacion + "00tmp/15cadenas/";

		CreaArchivo 		archivo 							= null;
		String 				nombreArchivo					= null;
		FileOutputStream 	out 								= null;
		BufferedWriter 	csv 								= null;

		try{

			con.conexionDB();

			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".csv";
			out 				= new FileOutputStream(strDirectorioTemp+nombreArchivo);
			csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));


			// Obtener las fechas para restringir la busqueda
			String idProcesoCruce		= (String)hm.get("idProcesoCruce");
			int 	 anioActual 			= Fecha.getAnioActual();
			String fechaInicialSiag		= getPrimerDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesInicialSiag")));
			String fechaFinalSiag		= getUltimoDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesFinalSiag")));

			// Realizar la consulta
			query.append(
				"SELECT 							" 	+
				"	CG_RAZON_SOCIAL, 			" 	+
				"	CG_RFC, 						" 	+
				"	CG_ESTADO, 					"	+
				"	FN_MONTO_ACTUAL_CRED, 	"  +
				"	IN_PLAZO, 					"  +
				"	CG_INTERNEDIARIOF, 		"  +
				"	IN_ESTRATO_INI, 			" 	+
				"	IN_ESTRATO_NUEVO, 		"  +
				"	IN_SECTOR, 					"  +
				"	TO_CHAR(DF_FECHA_REGISTRO,'DD/MM/YYYY') AS FECHA_REGISTRO, "  +
				"	CG_PORTAFOLIO, 			"  +
				"	IN_CONREC_CLAVE, 			"  +
				"	CG_CAMPOS_ADICIONALES 	"  +
				"FROM 							"  +
				"	COM_CRUCE_SIAG 			"  +
				"WHERE 							"  +
				"	DF_FECHA_REGISTRO >= TO_DATE(?,'DD/MM/YYYY') 	AND "  +
				"	DF_FECHA_REGISTRO < 	TO_DATE(?,'DD/MM/YYYY')+1 	AND "  +
				"	CG_PORTAFOLIO 		IN ( "  +
				"				SELECT CG_PORTAFOLIO FROM COMTMP_PORTAFOLIO_SIAG WHERE IC_PROCESO = ? "  +
				"	) AND "  +
				"	CG_RFC IN ( "  +
				"		SELECT CG_RFC FROM COMTMP_CRUCE_CADENAS_COMPRANET WHERE IC_PROCESO = ? AND CS_GARANTIAS = 'S' "  +
				"	) "  +
				"ORDER BY CG_RAZON_SOCIAL "
			);

			ps = con.queryPrecompilado(query.toString());
			ps.setString(1,fechaInicialSiag);
			ps.setString(2,fechaFinalSiag);
			ps.setInt(3,Integer.parseInt(idProcesoCruce));
			ps.setInt(4,Integer.parseInt(idProcesoCruce));
			rs = ps.executeQuery();

			String descripcionCamposAdicionales = getCamposAdicionalesSiag();
			descripcionCamposAdicionales = descripcionCamposAdicionales == null?"":descripcionCamposAdicionales;
			// DEFINIR CABECERA
			csv.write("Raz�n Social,RFC,Estado,Monto Actual del Cr�dito,Plazo,Intermediario,Estrato Inicial,Nuevo Estrato,Sector,Fecha de Registro,Descripci�n del Portafolio,Clave CONREC,"+descripcionCamposAdicionales+"\n");

			long i = 0L;
			while (rs != null && rs.next()) {

				csv.write("\"");csv.write(getCampo(rs,			"CG_RAZON_SOCIAL"));				csv.write("\",");
				csv.write("\"");csv.write(getRfcFiltrado(rs,	"CG_RFC"));							csv.write("\",");
				csv.write("\"");csv.write(getCampo(rs,			"CG_ESTADO"));						csv.write("\",");
				csv.write("\"");csv.write(getMontoConFormato(rs,"FN_MONTO_ACTUAL_CRED"));	csv.write("\",");
				csv.write("\"");csv.write(getCampo(rs,			"IN_PLAZO"));						csv.write("\",");
				csv.write("\"");csv.write(getCampo(rs,			"CG_INTERNEDIARIOF"));			csv.write("\",");
				csv.write("\"");csv.write(getCampo(rs,			"IN_ESTRATO_INI"));				csv.write("\",");
				csv.write("\"");csv.write(getCampo(rs,			"IN_ESTRATO_NUEVO"));			csv.write("\",");
				csv.write("\"");csv.write(getCampo(rs,			"IN_SECTOR"));						csv.write("\",");
				csv.write("\"");csv.write(getCampo(rs,			"FECHA_REGISTRO"));				csv.write("\",");
				csv.write("\"");csv.write(getCampo(rs,			"CG_PORTAFOLIO"));				csv.write("\",");
				csv.write("\"");csv.write(getCampo(rs,			"IN_CONREC_CLAVE"));				csv.write("\",");
				csv.write(getCampo(rs, "CG_CAMPOS_ADICIONALES"));
				csv.write("\n");

				if((i%500L) == 0L) { csv.flush(); i=0L; System.gc(); }
				i++;
			}

			if(csv 	!= null ) csv.close();

			file = new File(strDirectorioTemp + nombreArchivo);

		}catch(Exception e){
			log.error("getDescargarRFCunicosConGarant(Exception)");
			e.printStackTrace();
			file = new File(strDirectorioPlantillas + "error.csv");
		}finally{
			if (rs 	!= null)	try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null)	try { ps.close(); 	} catch(SQLException e) {}
			if (csv 	!= null)	try { csv.close();   } catch(Exception    e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB();
			log.info("getDescargarRFCunicosConGarant(s)");
		}
		
		return nombreArchivo;
	}
	
	/**
	 * se obtiene archivo generado por el cruce de informacion de 
	 * CADENAS-COMPRANET-SIAG (RFC's unicos con/sin garantia)
	 * @param hm
	 * @return
	 */
	public String getDescargarRFCunicosSinGarant(Map hm){
		log.info("getDescargarRFCunicosSinGarant(E)");

		AccesoDB 			con								= new AccesoDB();
    	PreparedStatement ps 								= null;
    	ResultSet 			rs 								= null;
		StringBuffer      query 							= new StringBuffer();

		File 					file 								= null;
		String 				strDirectorioPublicacion 	= (String)hm.get("directorioPublicacion");
		String 				strDirectorioPlantillas 	= (String)hm.get("directorioPlantillas");
		String 				strDirectorioTemp 			= strDirectorioPublicacion + "00tmp/15cadenas/";

		CreaArchivo 		archivo 							= null;
		String 				nombreArchivo					= null;
		FileOutputStream 	out 								= null;
		BufferedWriter 	csv 								= null;

		try{

					con.conexionDB();

					archivo 			= new CreaArchivo();
					nombreArchivo	= archivo.nombreArchivo()+".csv";
					out 				= new FileOutputStream(strDirectorioTemp+nombreArchivo);
					csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));

					// Obtener las fechas para restringir la busqueda
					String idProcesoCruce		= (String)hm.get("idProcesoCruce");
					int 	 anioActual 			= Fecha.getAnioActual();
					String fechaInicialSiag		= getPrimerDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesInicialSiag")));
					String fechaFinalSiag		= getUltimoDiaDelMes(anioActual,Integer.parseInt((String)hm.get("mesFinalSiag")));

					// Realizar la consulta
					query.append(
						"SELECT  						"  +
						"	CG_RAZON_SOCIAL,  		"  +
						"	CG_RFC, 						"  +
						"	CG_ESTADO,  				"  +
						"	FN_MONTO_ACTUAL_CRED,  	"  +
						"	IN_PLAZO,  					"  +
						"	CG_INTERNEDIARIOF, 		"  +
						"	IN_ESTRATO_INI, 			"  +
						"	IN_ESTRATO_NUEVO, 		"  +
						"	IN_SECTOR, 					"  +
						"	TO_CHAR(DF_FECHA_REGISTRO,'DD/MM/YYYY') AS FECHA_REGISTRO, "  +
						"	CG_PORTAFOLIO, 			"  +
						"	IN_CONREC_CLAVE, 			"  +
						"	'Si' AS GARANTIAS, 		"  +
						"	CG_CAMPOS_ADICIONALES 	"  +
						"FROM 							"  +
						"	COM_CRUCE_SIAG 			"  +
						"WHERE  							"  +
						"	DF_FECHA_REGISTRO >= TO_DATE(?,'DD/MM/YYYY') 	AND "  +
						"	DF_FECHA_REGISTRO < 	TO_DATE(?,'DD/MM/YYYY')+1 	AND "  +
						"	CG_PORTAFOLIO IN ( SELECT CG_PORTAFOLIO FROM COMTMP_PORTAFOLIO_SIAG WHERE IC_PROCESO = ? ) AND 		"  +
						"	CG_RFC IN ( 				"  +
						"		SELECT CG_RFC FROM COMTMP_CRUCE_CADENAS_COMPRANET WHERE IC_PROCESO = ? AND CS_GARANTIAS = 'S' 	"  +
						"	) 								"  +
						"UNION ALL 						"  +
						"SELECT  		 				"  +
						"	DECODE(CRUCE.CS_ORIGEN,'T',PYME_COMPRANET.CG_RAZON_SOCIAL,PYME_CADENAS.CG_RAZON_SOCIAL) AS CG_RAZON_SOCIAL, 	 "  +
						"	CRUCE.CG_RFC, 		 		"  +
						"	NULL, 						"  +
						"	TO_NUMBER(NULL), 			"  +
						"	TO_NUMBER(NULL),			"  +
						"	NULL, 						"  +
						"	TO_NUMBER(NULL),			"  +
						"	TO_NUMBER(NULL), 			"  +
						"	TO_NUMBER(NULL), 			"  +
						"	NULL AS FECHA_REGISTRO, "  +
						"	NULL, 						"  +
						"	TO_NUMBER(NULL), 			"  +
						"	'NO' AS GARANTIAS,  		"  +
						"	NULL 	 						"  +
						"FROM  			 				"  +
						"	COMTMP_CRUCE_CADENAS_COMPRANET 	CRUCE , 			 		"  +
						"	COMCAT_PYME             			PYME_CADENAS, 	 		"  +
						"	COMCAT_PYME_COMPRANET   			PYME_COMPRANET  		"  +
						"WHERE  			 						"  +
						"	CRUCE.CG_RFC 		= PYME_CADENAS.CG_RFC(+) 		AND  	"  +
						"	CRUCE.CG_RFC 		= PYME_COMPRANET.CG_RFC(+) 	AND 	"  +
						"	CRUCE.IC_PROCESO	= ?	AND 	"  +
						"	CS_GARANTIAS 		= 'N'	 		"  +
						"ORDER BY CG_RAZON_SOCIAL "
					);

					ps = con.queryPrecompilado(query.toString());
					ps.setString(1,fechaInicialSiag);
					ps.setString(2,fechaFinalSiag);
					ps.setInt(3,Integer.parseInt(idProcesoCruce));
					ps.setInt(4,Integer.parseInt(idProcesoCruce));
					ps.setInt(5,Integer.parseInt(idProcesoCruce));
					rs = ps.executeQuery();

					String descripcionCamposAdicionales = getCamposAdicionalesSiag();
					descripcionCamposAdicionales = descripcionCamposAdicionales == null?"":descripcionCamposAdicionales;
					// DEFINIR CABECERA
					csv.write("Raz�n Social,RFC,Estado,Monto Actual del Cr�dito,Plazo,Intermediario,Estrato Inicial,Nuevo Estrato,Sector,Fecha de Registro,Descripci�n del Portafolio,Clave CONREC,Garantias,"+descripcionCamposAdicionales+"\n");

					long i = 0L;
					while (rs != null && rs.next()) {

						csv.write("\"");csv.write(getCampo(rs,			"CG_RAZON_SOCIAL"));				csv.write("\",");
						csv.write("\"");csv.write(getRfcFiltrado(rs,	"CG_RFC"));							csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"CG_ESTADO"));						csv.write("\",");
						csv.write("\"");csv.write(getMontoConFormato(rs,"FN_MONTO_ACTUAL_CRED"));	csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"IN_PLAZO"));						csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"CG_INTERNEDIARIOF"));			csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"IN_ESTRATO_INI"));				csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"IN_ESTRATO_NUEVO"));			csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"IN_SECTOR"));						csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"FECHA_REGISTRO"));				csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"CG_PORTAFOLIO"));				csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"IN_CONREC_CLAVE"));				csv.write("\",");
						csv.write("\"");csv.write(getCampo(rs,			"GARANTIAS"));						csv.write("\",");
						csv.write(getCampo(rs, "CG_CAMPOS_ADICIONALES"));
						csv.write("\n");

						if((i%500L) == 0L) { csv.flush(); i=0L; System.gc(); }
						i++;
					}

					if(csv 	!= null ) csv.close();

					file = new File(strDirectorioTemp + nombreArchivo);

		}catch(Exception e){
			log.error("getDescargarRFCunicosSinGarant(Exception)");
			e.printStackTrace();
			file = new File(strDirectorioPlantillas + "error.csv");
		}finally{
			if (rs 	!= null)	try { rs.close(); 	} catch(SQLException e) {}
			if (ps 	!= null)	try { ps.close(); 	} catch(SQLException e) {}
			if (csv 	!= null)	try { csv.close();   } catch(Exception    e) {}
			if (con 	!= null && con.hayConexionAbierta()) 	con.cierraConexionDB();
			log.info("getDescargarRFCunicosSinGarant(s)");
		}
		return nombreArchivo;
	}
	
	private String getCampo(ResultSet registros,String nombreCampo){
 
		String contenidoCampo = null;
		try {
			contenidoCampo = registros.getString(nombreCampo);
		}catch(Exception e){}
		contenidoCampo = contenidoCampo == null?"":contenidoCampo;		
	
		return contenidoCampo.replaceAll("\\\"","\"\"");
		
	}
	
	private String getRfcFiltrado(ResultSet registros,String nombreCampo){
 
		String contenidoCampo = null;
		try {
			contenidoCampo = registros.getString(nombreCampo);
		}catch(Exception e){}
		contenidoCampo = contenidoCampo == null?"":contenidoCampo;
		
		return contenidoCampo.replaceAll("-","");
		
	}
	
	private String getMontoConFormato(ResultSet registros,String nombreCampo){
 
		String contenidoCampo = null;
		String tmp				 = null;
		
		try {
			contenidoCampo = registros.getString(nombreCampo);
		}catch(Exception e){}		
		contenidoCampo = contenidoCampo == null?"":contenidoCampo;
		
		if( contenidoCampo.equals("") ) return contenidoCampo;
		
		tmp = contenidoCampo;
		try {	
			contenidoCampo = Comunes.formatoDecimal((Object)contenidoCampo, 2, true);
		}catch(Exception e){
			contenidoCampo = tmp;
		}
		return contenidoCampo;
		
	}
	
	/**
	 * Este metodo suprime los espacios vacios al inicio y al final de la
	 * cadena de caracteres almacenada en el <tt>buffer</tt>
	 * @param buffer Con la cadena de caracteres a filtrar
	 */
	private void trim(StringBuffer buffer){

		if(buffer != null && buffer.length() > 0){

			// Filtrar caracteres al inicio
			for(int i=0;i<buffer.length();i++){
				char c = buffer.charAt(i);
				// Si el caracter es un espacio en blanco
				if(Character.isWhitespace(c)){
					buffer.deleteCharAt(i);
					// Anular incremento
					i--;
				}else{
					break;
				}
			}
			// Filtrar caracteres al final
			for(int i=buffer.length()-1;i>=0;i--){
				char c = buffer.charAt(i);
				// Si el caracter es un espacio en blaco
				if(Character.isWhitespace(c)){
					buffer.deleteCharAt(i);
				}else{
					break;
				}
			}
		}

	}

	/**
	 * Convierte a mayusculas el contenido del <tt>buffer</tt>.
	 * @param buffer <tt>StringBuffer</tt> con los caracteres a convertir.
	 */
	private void toUpperCase(StringBuffer buffer){
		if(buffer != null && buffer.length() > 0){
			for(int i=0;i<buffer.length();i++){
				char c = buffer.charAt(i);
				c = Character.toUpperCase(c);
				buffer.setCharAt(i,c);
			}
		}
	}
	
	public void ejbCreate() {
	}

	public void ejbActivate() {
	}

	public void ejbPassivate() {
	}

	public void ejbRemove() {
	}

	public void setSessionContext(SessionContext ctx) {
	}
	
	
	
}