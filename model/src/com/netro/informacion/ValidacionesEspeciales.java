package com.netro.informacion;
 
import netropology.utilerias.*;

public class ValidacionesEspeciales 
{
   private StringBuffer anioTmp     = null;
   private StringBuffer	anioActual	= null;
   
   public ValidacionesEspeciales(){
      anioTmp     = new StringBuffer(256);
      anioActual  = new StringBuffer(String.valueOf(Fecha.getAnioActual()));
   }
   
   /**
    * Valida si el a�o proporcionado en el parametro <tt>fecha</tt> corresponde al
    * a�o actual. Para realizar esta validacion se debe proporcionar una fecha valida.
    * @param fecha <tt>StringBuffer</tt> con la fecha a validar, la cual debe tener el formato: dd/MM/yyyy
    * @return <tt>boolean</tt> con valor <tt>true</tt> si la fecha corresponde al a�o actual.<tt>false</tt> en 
    *          caso  contrario.
    */
   public boolean esAnioActual(StringBuffer fecha){
   
      boolean esFechaConAnioActual = false;
      
      if(fecha == null ){
         return false;
      }
      
      StringBuffer anio = getNumeroDelAnio(fecha);
      if(anio.length() == 0){
         return false;
      }
      
      esFechaConAnioActual       = true;
      char digitoAnioActual      = 0;
      char digitoAnioValidacion  = 0;
      
      for(int i=0;i<anioActual.length();i++){
         digitoAnioActual = anioActual.charAt(i);
         if(anio.length() > i){
            digitoAnioValidacion = anio.charAt(i);
         }else{
            esFechaConAnioActual = false;
            break;
         }
         
         if(digitoAnioActual != digitoAnioValidacion){
            esFechaConAnioActual = false;
            break;
         }
      }
      
      if(esFechaConAnioActual && (anio.length() > anioActual.length())){
         esFechaConAnioActual = false;
      }
      
      return esFechaConAnioActual;
      
   }
   
   /**
	 * Devuelve un StringBuffer con los caracteres del Numero de A�o
    * @param fecha Fecha de la que se extraera el a�o, la cual debe tener el 
    *              formato: dd/MM/yyyy
    * @return <tt>StringBuffer</tt> con los numeros correspondientes al a�o.            
	 */
	public StringBuffer getNumeroDelAnio(StringBuffer fecha){
   
      int numBarrasEncontradas = 0;
      int ctaCaracteres        = 0;
      anioTmp.delete(0,anioTmp.length());
      for(int i=0;i<fecha.length();i++){
         char caracter = fecha.charAt(i);
         if(caracter == '/'){ 
            ++numBarrasEncontradas;
            continue;
         }
         
         if(numBarrasEncontradas < 2 ){
            continue;
         }
         anioTmp.append(caracter);
         ctaCaracteres++;
         if(ctaCaracteres == 5) break;
      }
      return anioTmp;
      
   }
   
}