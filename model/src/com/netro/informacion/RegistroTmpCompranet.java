package com.netro.informacion;
 
public class RegistroTmpCompranet {
  
  /**
   * NOTA: Esta clase esta pensada para ahorrar memoria y hacer mas agil la validacion de registros.
   */
 
	private static final int ERROR_BUFFER_SIZE		 	= 1536;
	
	private StringBuffer	nombreEpo 						   = null;		
	private StringBuffer	licitacion 						   = null;
	private StringBuffer	unidad							   = null;
	private StringBuffer	tipoContratacion 				  	= null;
	private StringBuffer	fechaEmision 					   = null;
	private StringBuffer	numPartida						   = null;
	private StringBuffer importeSinIVA 						= null;
	private StringBuffer razonSocial							= null;
	private StringBuffer rfc								   = null;
	private StringBuffer	clasificacionDependencia 		= null;
	private StringBuffer	mensajeError 						= null;
   private boolean		hayError								= false;
	
	public RegistroTmpCompranet(){
		
      nombreEpo                 	= new StringBuffer(91);								
      licitacion						= new StringBuffer(17);								
      unidad							= new StringBuffer(91);								
      tipoContratacion				= new StringBuffer(41);								
      fechaEmision					= new StringBuffer(11);								
      numPartida						= new StringBuffer(4);								
      importeSinIVA					= new StringBuffer(19);								
      razonSocial						= new StringBuffer(101);								
      rfc								= new StringBuffer(16);								
      clasificacionDependencia  	= new StringBuffer(3);
		mensajeError					= new StringBuffer(ERROR_BUFFER_SIZE);
		hayError							= false;
		
	}
  
	public void doReset(){
		
		nombreEpo.delete(0,nombreEpo.length());
		licitacion.delete(0,licitacion.length());
		unidad.delete(0,unidad.length());
		tipoContratacion.delete(0,tipoContratacion.length());
		fechaEmision.delete(0,fechaEmision.length());
		numPartida.delete(0,numPartida.length());
		importeSinIVA.delete(0,importeSinIVA.length());
		razonSocial.delete(0,razonSocial.length());
		rfc.delete(0,rfc.length());
		clasificacionDependencia.delete(0,clasificacionDependencia.length());
		mensajeError.delete(0,mensajeError.length());
		hayError	= false;
	}
	

	public StringBuffer getNombreEpo(){
		return nombreEpo;
	}
 
	public void setNombreEpo(String nombreEpo){
		this.nombreEpo.delete(0,this.nombreEpo.length());
		this.nombreEpo.append(nombreEpo);
	}
 
	public StringBuffer getLicitacion(){
		return licitacion;
	}
 
	public void setLicitacion(String licitacion){
		this.licitacion.delete(0,this.licitacion.length());
		this.licitacion.append(licitacion);
	}
 
	public StringBuffer getUnidad(){
		return unidad;
	}
 
	public void setUnidad(String unidad){
		this.unidad.delete(0,this.unidad.length());
		this.unidad.append(unidad);
	}
 
	public StringBuffer getTipoContratacion(){
		return tipoContratacion;
	}
 
	public void setTipoContratacion(String tipoContratacion){
		this.tipoContratacion.delete(0,this.tipoContratacion.length());
		this.tipoContratacion.append(tipoContratacion);
	}
 
	public StringBuffer getFechaEmision(){
		return fechaEmision;
	}
 
	public void setFechaEmision(String fechaEmision){
		this.fechaEmision.delete(0,this.fechaEmision.length());
		this.fechaEmision.append(fechaEmision);
	}
 
	public StringBuffer getNumPartida(){
		return numPartida;
	}
 
	public void setNumPartida(String numPartida){
		this.numPartida.delete(0,this.numPartida.length());
		this.numPartida.append(numPartida);
	}
 
	public StringBuffer getImporteSinIVA(){
		return importeSinIVA;
	}
 
	public void setImporteSinIVA(String importeSinIVA){
		this.importeSinIVA.delete(0,this.importeSinIVA.length());
		this.importeSinIVA.append(importeSinIVA);
	}
 
	public StringBuffer getRazonSocial(){
		return razonSocial;
	}
 
	public void setRazonSocial(String razonSocial){
		this.razonSocial.delete(0,this.razonSocial.length());
		this.razonSocial.append(razonSocial);
	}
 
	public StringBuffer getRfc(){
		return rfc;
	}
 
	public void setRfc(String rfc){
		this.rfc.delete(0,this.rfc.length());
		this.rfc.append(rfc);
	}
 
	public StringBuffer getClasificacionDependencia(){
		return clasificacionDependencia;
	}
 
	public void setClasificacionDependencia(String clasificacionDependencia){
		this.clasificacionDependencia.delete(0,this.clasificacionDependencia.length());
		this.clasificacionDependencia.append(clasificacionDependencia);
	}

	public StringBuffer getMensajeError(){
		
		this.mensajeError.append(".");
		if( mensajeError.length() > ERROR_BUFFER_SIZE ){
			this.mensajeError.delete(ERROR_BUFFER_SIZE-3,this.mensajeError.length());
			this.mensajeError.append("...");	
		}
		return this.mensajeError;
	}

	public void setMensajeError(String mensajeError){
		this.mensajeError.delete(0,this.mensajeError.length());
		hayError = true;
		this.mensajeError.append(mensajeError);
	}

 	public void addMensajeError(String mensajeError){
		
		if(hayError){ 
			this.mensajeError.append(", ");
		}
		
		this.mensajeError.append(mensajeError);
		
		if(hayError == false){
			hayError = true;
		}
		
	}  
 
	public boolean getHayError(){
		return hayError;
	}
 
	public void setHayError(boolean hayError){
		
		if(this.hayError && !hayError){
			this.mensajeError.delete(0,this.mensajeError.length());
		}
		this.hayError = hayError;
		
	}

}