package com.netro.informacion;
  
import java.io.Serializable;

public class EpoCompranetBean implements Serializable {
	
   private static final long serialVersionUID = 20110126L;
   
   private String numeroEpo; 
	private String numeroEpoOriginal;
   private String icEpoCompranet;
   private String nombreEpo;
   private String nuevo;	
 
	public String getNumeroEpo(){
		return numeroEpo;
	}
 
	public void setNumeroEpo(String numeroEpo){
		this.numeroEpo = numeroEpo;
	}
 
	public String getIcEpoCompranet(){
		return icEpoCompranet;
	}

	public void setIcEpoCompranet(String icEpoCompranet){
		this.icEpoCompranet = icEpoCompranet;
	}
 
	public String getNombreEpo(){
		return nombreEpo;
	}
 
	public void setNombreEpo(String nombreEpo){
		this.nombreEpo = nombreEpo;
	}
 
	public String getNuevo(){
		return nuevo;
	}
 
	public void setNuevo(String nuevo){
		this.nuevo = nuevo;
	}
 
	public String getNumeroEpoOriginal(){
		return numeroEpoOriginal;
	}
 
	public void setNumeroEpoOriginal(String numeroEpoOriginal){
		this.numeroEpoOriginal = numeroEpoOriginal;
	}
 
}