package com.netro.informacion;
 
public class RegistroTmpSiag {
  
  /**
   * NOTA: Esta clase esta pensada para ahorrar memoria y hacer mas agil la validacion de registros.
   */
 
	private static final int ERROR_BUFFER_SIZE	= 2560;
	
	private StringBuffer razonSocial        		= null;
	private StringBuffer rfc                		= null; 
	private StringBuffer estado             		= null; 
	private StringBuffer montoActualCredito 		= null;
	private StringBuffer plazo              		= null; 
	private StringBuffer intermediario      		= null;
	private StringBuffer estratoInicial     		= null;
	private StringBuffer nuevoEstrato       		= null; 
	private StringBuffer sector             		= null; 
	private StringBuffer fechaRegistro           = null;
	private StringBuffer descripcionPortafolio   = null;
	private StringBuffer claveCONREC 				= null;
	private StringBuffer camposAdicionales 		= null;
	
	private StringBuffer	mensajeError 				= null;
   private boolean		hayError						= false;
	
	public RegistroTmpSiag(){

		razonSocial 					= new StringBuffer(91);
		rfc 								= new StringBuffer(16);
		estado 							= new StringBuffer(51);
		montoActualCredito 			= new StringBuffer(19);
		plazo 							= new StringBuffer(4);
		intermediario 					= new StringBuffer(91);
		estratoInicial 				= new StringBuffer(4);
		nuevoEstrato 					= new StringBuffer(4);
		sector 							= new StringBuffer(4);
		fechaRegistro 					= new StringBuffer(11);
		descripcionPortafolio 		= new StringBuffer(31);
		claveCONREC 					= new StringBuffer(4);
		camposAdicionales 			= new StringBuffer(4000);

		mensajeError					= new StringBuffer(ERROR_BUFFER_SIZE);
		hayError							= false;
		
	}
  
	public void doReset(){
		
		razonSocial.delete(0,				razonSocial.length());
		rfc.delete(0,							rfc.length());
		estado.delete(0,						estado.length());
		montoActualCredito.delete(0,		montoActualCredito.length());
		plazo.delete(0,						plazo.length());
		intermediario.delete(0,				intermediario.length());
		estratoInicial.delete(0,			estratoInicial.length());
		nuevoEstrato.delete(0,				nuevoEstrato.length());
		sector.delete(0,						sector.length());
		fechaRegistro.delete(0,				fechaRegistro.length());
		descripcionPortafolio.delete(0,	descripcionPortafolio.length());
		claveCONREC.delete(0,				claveCONREC.length());
		camposAdicionales.delete(0,		camposAdicionales.length());
 
		mensajeError.delete(0,mensajeError.length());
		hayError	= false;
		
	}
 
	public StringBuffer getRazonSocial(){
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial){
		this.razonSocial.delete(0,this.razonSocial.length());
		this.razonSocial.append(razonSocial);
	}

	public StringBuffer getRfc(){
		return rfc;
	}

	public void setRfc(String rfc){
		this.rfc.delete(0,this.rfc.length());
		this.rfc.append(rfc);
	}

	public StringBuffer getEstado(){
		return estado;
	}

	public void setEstado(String estado){
		this.estado.delete(0,this.estado.length());
		this.estado.append(estado);
	}

	public StringBuffer getMontoActualCredito(){
		return montoActualCredito;
	}

	public void setMontoActualCredito(String montoActualCredito){
		this.montoActualCredito.delete(0,this.montoActualCredito.length());
		this.montoActualCredito.append(montoActualCredito);
	}

	public StringBuffer getPlazo(){
		return plazo;
	}

	public void setPlazo(String plazo){
		this.plazo.delete(0,this.plazo.length());
		this.plazo.append(plazo);
	}

	public StringBuffer getIntermediario(){
		return intermediario;
	}

	public void setIntermediario(String intermediario){
		this.intermediario.delete(0,this.intermediario.length());
		this.intermediario.append(intermediario);
	}

	public StringBuffer getEstratoInicial(){
		return estratoInicial;
	}

	public void setEstratoInicial(String estratoInicial){
		this.estratoInicial.delete(0,this.estratoInicial.length());
		this.estratoInicial.append(estratoInicial);
	}

	public StringBuffer getNuevoEstrato(){
		return nuevoEstrato;
	}

	public void setNuevoEstrato(String nuevoEstrato){
		this.nuevoEstrato.delete(0,this.nuevoEstrato.length());
		this.nuevoEstrato.append(nuevoEstrato);
	}

	public StringBuffer getSector(){
		return sector;
	}

	public void setSector(String sector){
		this.sector.delete(0,this.sector.length());
		this.sector.append(sector);
	}

	public StringBuffer getFechaRegistro(){
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro){
		this.fechaRegistro.delete(0,this.fechaRegistro.length());
		this.fechaRegistro.append(fechaRegistro);
	}

	public StringBuffer getDescripcionPortafolio(){
		return descripcionPortafolio;
	}

	public void setDescripcionPortafolio(String descripcionPortafolio){
		this.descripcionPortafolio.delete(0,this.descripcionPortafolio.length());
		this.descripcionPortafolio.append(descripcionPortafolio);
	}

	public StringBuffer getClaveCONREC(){
		return claveCONREC;
	}

	public void setClaveCONREC(String claveCONREC){
		this.claveCONREC.delete(0,this.claveCONREC.length());
		this.claveCONREC.append(claveCONREC);
	}

	public StringBuffer getCamposAdicionales(){
		return camposAdicionales;
	}

	public void setCamposAdicionales(String camposAdicionales){
		this.camposAdicionales.delete(0,this.camposAdicionales.length());
		this.camposAdicionales.append(camposAdicionales);
	}
	
	
	public StringBuffer getMensajeError(){
		
		this.mensajeError.append(".");
		if( mensajeError.length() > ERROR_BUFFER_SIZE ){
			this.mensajeError.delete(ERROR_BUFFER_SIZE-3,this.mensajeError.length());
			this.mensajeError.append("...");	
		}
		return this.mensajeError;
	}

	public void setMensajeError(String mensajeError){
		this.mensajeError.delete(0,this.mensajeError.length());
		hayError = true;
		this.mensajeError.append(mensajeError);
	}

 	public void addMensajeError(String mensajeError){
		
		if(hayError){ 
			this.mensajeError.append(", ");
		}
		
		this.mensajeError.append(mensajeError);
		
		if(hayError == false){
			hayError = true;
		}
		
	}  
 
	
	public boolean getHayError(){
		return hayError;
	}
 
	public void setHayError(boolean hayError){
		
		if(this.hayError && !hayError){
			this.mensajeError.delete(0,this.mensajeError.length());
		}
		this.hayError = hayError;
		
	}

}