package com.netro.informacion;
 
public class RegistroCSVCompranet 
{
	private static final int BUFFER_SIZE				= 512;
	
	private StringBuffer nombreEpo						= null;
	private StringBuffer licitacion						= null;
	private StringBuffer unidad							= null;
	private StringBuffer tipoContratacion				= null;
	private StringBuffer fechaEmision					= null;
	private StringBuffer numPartida						= null;
	private StringBuffer importeSinIVA					= null;
	private StringBuffer razonSocial						= null;
	private StringBuffer rfc								= null;
	private StringBuffer clasificacionDependencia	= null;
	private int				fieldIndex						= 0;
		
	public RegistroCSVCompranet(){
	
		this.nombreEpo 					= new StringBuffer(BUFFER_SIZE);
		this.licitacion					= new StringBuffer(BUFFER_SIZE);
		this.unidad							= new StringBuffer(BUFFER_SIZE);
		this.tipoContratacion			= new StringBuffer(BUFFER_SIZE);
		this.fechaEmision					= new StringBuffer(BUFFER_SIZE);
		this.numPartida					= new StringBuffer(BUFFER_SIZE);
		this.importeSinIVA				= new StringBuffer(BUFFER_SIZE);
		this.razonSocial					= new StringBuffer(BUFFER_SIZE);
		this.rfc								= new StringBuffer(BUFFER_SIZE);
		this.clasificacionDependencia	= new StringBuffer(BUFFER_SIZE);	
		
		this.fieldIndex					= 0;
	}
	
	public void doReset(){
		
		this.fieldIndex					= 0;
		
		this.nombreEpo.delete(0,nombreEpo.length());
		this.licitacion.delete(0,licitacion.length());
		this.unidad.delete(0,unidad.length());
		this.tipoContratacion.delete(0,tipoContratacion.length());
		this.fechaEmision.delete(0,fechaEmision.length());
		this.numPartida.delete(0,numPartida.length());
		this.importeSinIVA.delete(0,importeSinIVA.length());
		this.razonSocial.delete(0,razonSocial.length());
		this.rfc.delete(0,rfc.length());
		this.clasificacionDependencia.delete(0,clasificacionDependencia.length());
		
	}
	
	public StringBuffer getNombreEpo() {
		return nombreEpo;
	}

	public void setNombreEpo(StringBuffer nombreEpo) {
		this.nombreEpo = nombreEpo;
	}

	public StringBuffer getLicitacion() {
		return licitacion;
	}

	public void setLicitacion(StringBuffer licitacion) {
		this.licitacion = licitacion;
	}

	public StringBuffer getUnidad() {
		return unidad;
	}

	public void setUnidad(StringBuffer unidad) {
		this.unidad = unidad;
	}

	public StringBuffer getTipoContratacion() {
		return tipoContratacion;
	}

	public void setTipoContratacion(StringBuffer tipoContratacion) {
		this.tipoContratacion = tipoContratacion;
	}

	public StringBuffer getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(StringBuffer fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public StringBuffer getNumPartida() {
		return numPartida;
	}

	public void setNumPartida(StringBuffer numPartida) {
		this.numPartida = numPartida;
	}

	public StringBuffer getImporteSinIVA() {
		return importeSinIVA;
	}

	public void setImporteSinIVA(StringBuffer importeSinIVA) {
		this.importeSinIVA = importeSinIVA;
	}

	public StringBuffer getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(StringBuffer razonSocial) {
		this.razonSocial = razonSocial;
	}

	public StringBuffer getRfc() {
		return rfc;
	}

	public void setRfc(StringBuffer rfc) {
		this.rfc = rfc;
	}

	public StringBuffer getClasificacionDependencia() {
		return clasificacionDependencia;
	}

	public void setClasificacionDependencia(StringBuffer clasificacionDependencia) {
		this.clasificacionDependencia = clasificacionDependencia;
	}
	
	public StringBuffer getField(){
		
		StringBuffer buffer = null;
		
		switch(fieldIndex){
			case 0: buffer = getNombreEpo(); 					this.fieldIndex++; break;
			case 1: buffer = getLicitacion(); 					this.fieldIndex++; break;
			case 2: buffer = getUnidad(); 						this.fieldIndex++; break;
			case 3: buffer = getTipoContratacion();			this.fieldIndex++; break;
			case 4: buffer = getFechaEmision(); 				this.fieldIndex++; break;
			case 5: buffer = getNumPartida(); 					this.fieldIndex++; break;
			case 6: buffer = getImporteSinIVA(); 				this.fieldIndex++; break;
			case 7: buffer = getRazonSocial(); 					this.fieldIndex++; break;
			case 8: buffer = getRfc(); 							this.fieldIndex++; break;
			case 9: buffer = getClasificacionDependencia(); this.fieldIndex++; break;	
			default:
				buffer = null;
		}
		
		return buffer;
		
	}
	
}