package com.netro.informacion;
 
import java.io.Serializable;
import com.netro.exception.*;
import netropology.utilerias.*;
import java.sql.*;
import java.text.*;
import java.math.*;

/**
 *
 * Clase que se encarga de invocar al metodo validaDatosCargaSiag del
 * EJB de RepEspecialesProv, actualizando el porcentaje de avance en el
 * atributo: percent
 *
 * @author  Jesus Salim Hernandez David
 * @version 1.1
 *
 */

public class ValidaDatosSiagThread implements Runnable, Serializable {
	
	private static final long serialVersionUID = 20110202L;
	 
   private int 							numberOfRegisters;
   private float							percent;
   private boolean 						started;
   private boolean 						running;
	private int 							processID;
	private int 							processedRegisters;
   private RepEspecialesProvBean 	reportes;
	private boolean						error;
	private RegistroTmpSiag				registroTemporal;
	private ValidacionesEspeciales	validacionesEspeciales;
	private int								errorRecordsCount; 
	
   public ValidaDatosSiagThread() {
        numberOfRegisters 	= 0;
        percent 		   	= 0;
        started 			   = false;
        running 			   = false;
	     processID 	   	= 0;
		  processedRegisters = 0;
		  reportes				= new RepEspecialesProvBean();
		  error					= false;
		  errorRecordsCount	= 0;
   }
	 
   protected void validateRegisters() {
		
	   try {
			
			reportes.validaRegistroSiag(processID,processedRegisters+1,registroTemporal,validacionesEspeciales);					
			processedRegisters++;
			percent = (processedRegisters/(float)numberOfRegisters)*100;
			
			if(registroTemporal.getHayError()){
				errorRecordsCount++;
			}
			
			if(errorRecordsCount == 2001){
				// Borrar los registros que ya no pudieron ser validados porque se alcanzo el limite de errores
				reportes.borrarRegistrosTmpCargaSiag(processID,processedRegisters+1);
				// Detener el thread
				setRunning(false);
				numberOfRegisters = processedRegisters;
				percent				= 100F;
			}
			
		} catch(Exception e){
			setRunning(false);
			numberOfRegisters = processedRegisters ;
			percent				= 100F;
			setError(true);
			e.printStackTrace();
			System.out.println("Error en ValidaDatosSiagThread::validateRegisters al extraer numero de registro de la base de datos");
			// terminar el proceso si falla la funcion de validacion para algun registro
		}
		
   }

   public synchronized float getPercent() {
       return percent;
   }
	
	public synchronized int getNumberOfRegisters() {
       return numberOfRegisters;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		return (processedRegisters < numberOfRegisters?false:true);
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
	public synchronized void setProcessID(int processID) {
       this.processID = processID;
   }
	
	public synchronized void setError(boolean error) {
       this.error = error;
   }
		
	public synchronized boolean hasError() {
       return error;
   }
	
	public synchronized int getProcessID() {
       return processID;
   }

	public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }

   public void run() {
		 
		// Obtener numero de registros
		AccesoDB 			con 	= new AccesoDB();
		boolean				lbOK	= true;
		StringBuffer 		query = null; 
		ResultSet 			rs 	= null;
		PreparedStatement ps		= null;
		
		try {
			
			con.conexionDB();
			
			query = new StringBuffer(); 		
			query.append(
				"SELECT                    " +
				"	MAX(IC_NUMERO_LINEA)    " +
		 		"FROM                      " +
				"	COMTMP_CRUCE_SIAG  		" +
				"WHERE                     " +
				"	IC_PROCESO  = ?         " // processID
			);
			
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,processID);
			rs = ps.executeQuery();
			
			if(rs != null && rs.next()){	
				numberOfRegisters = rs.getInt(1);
			}
			
		}catch (Exception e){
			System.out.println("Error en ValidaDatosSiagThread::run() al extraer de la base de datos el numero de registros a procesar");
			lbOK = false;
			e.printStackTrace();
			setError(true);
			numberOfRegisters = 0;
		}finally{
			if(rs != null) { try { rs.close();}catch(Exception e){} }
		 	if(ps != null) { try { ps.close();}catch(Exception e){} }
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		// Verificar que no se hayan devuelto datos vacios
		if ( numberOfRegisters == 0){
			numberOfRegisters  	= 0;
			processedRegisters 	= 0;
			percent					= 100F;
			lbOK 						= false;
		}
		
		// Realizar la Validacion de los datos
      try {
			
			if(lbOK == false){ 
				setRunning(false);
			}else{
				setRunning(true);
			}
			
			registroTemporal 			= new RegistroTmpSiag();
			validacionesEspeciales 	= new ValidacionesEspeciales();
			
			while (isRunning() && !isCompleted()){
					validateRegisters();
					// Debug info 
					if( (processedRegisters % 350) == 0){
						System.out.println("Validating Registers");
						System.out.println("numberOfRegisters:	" + numberOfRegisters);
						System.out.println("percent:    			" + percent);
						System.out.println("started:       		" + started);
						System.out.println("running:       		" + running);
						System.out.println("completed:     		" + isCompleted());
						System.out.println("processID: 			" + processID);
						System.out.println("processedRegisters:" + processedRegisters );
					}
			}
			
			// Debug info
			System.out.println("Validating Registers");
			System.out.println("numberOfRegisters:	" + numberOfRegisters);
			System.out.println("percent:    			" + percent);
			System.out.println("started:       		" + started);
			System.out.println("running:       		" + running);
			System.out.println("completed:     		" + isCompleted());
			System.out.println("processID: 			" + processID);
			System.out.println("processedRegisters:" + processedRegisters );
		
      } finally {
			setRunning(false);
      }

    }

}