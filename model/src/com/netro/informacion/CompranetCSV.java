package com.netro.informacion;
 
import org.apache.commons.logging.Log;
import netropology.utilerias.*;

public class CompranetCSV{

	private final static Log log = ServiceLocator.getInstance().getLog(CompranetCSV.class);
	
	private StringBuffer	field	= null;
	private StringBuffer in		= new StringBuffer(512);
	
   public void parse(String registro, RegistroCSVCompranet registroCSV){
		
		//log.info("parse(E)");
 
		boolean			quotedStringFound				= false;
		char 				antepenultimateChar 			= 0;
		char				penultimateChar				= 0;
		char 				character 						= 0;
		boolean 			insideQuotedString 			= false;
		int 				doubleQuotesCount				= 0;
		boolean 			firstCharIsNotDoubleQuote 	= false;
		
		registroCSV.doReset();
		field 							= registroCSV.getField();
		
		for(int i=0;i<registro.length();i++){
			
			character = registro.charAt(i);
			
			if(character == '"' && insideQuotedString){
				doubleQuotesCount++;
			}
			
			if(i==0 && character != '"'){
				firstCharIsNotDoubleQuote = true;
			}
			
			if(firstCharIsNotDoubleQuote){
				if(character == ',' ){
					//System.out.println("field1 = <"+simplifyField(field)+">, field1 = <"+field+">");
					simplifyField();
					field 							= registroCSV.getField();
					if(field == null) 			break;
					antepenultimateChar 			= 0;
					penultimateChar 				= 0;
					doubleQuotesCount   			= 0;
					quotedStringFound   			= false;
					firstCharIsNotDoubleQuote 	= false;
				}else{
					field.append(character);
				}
			}else if(quotedStringFound){
				if(character == ',' ){
					//System.out.println("field6 = <"+simplifyField(field)+">, field6 = <"+field+">");
					simplifyField();
					field 							= registroCSV.getField();
					if(field == null) 			break;
					antepenultimateChar 			= 0;
					penultimateChar 				= 0;
					doubleQuotesCount   			= 0;
					quotedStringFound   			= false;
					firstCharIsNotDoubleQuote 	= false;
				}else{
					field.append(character);
				}
			}else if( character == '"' && !insideQuotedString ){
					field.append(character);
					insideQuotedString 			= true;
					antepenultimateChar 			= 0;
					penultimateChar 				= 0;
					doubleQuotesCount   			= 0;
			}else if(antepenultimateChar == '"' && penultimateChar == '"' && character == ',' && insideQuotedString){
				
				if((doubleQuotesCount%2) == 1){
					//System.out.println("field5 = <"+simplifyField(field)+">, field5 = <"+field+">");
					simplifyField();
					field 							= registroCSV.getField();
					if(field == null) 			break;
					antepenultimateChar 			= 0;
					penultimateChar 				= 0;
					doubleQuotesCount   			= 0;
					quotedStringFound   			= false;
					insideQuotedString 			= false;
					firstCharIsNotDoubleQuote 	= false;
				}else{
					field.append(character);
					antepenultimateChar = penultimateChar;
					penultimateChar 	= character;
				}
				
			}else if( penultimateChar == '"' && character != '"' && insideQuotedString){
				
				if( (doubleQuotesCount%2) == 1 && character != ','){
					antepenultimateChar 			= 0;
					penultimateChar 				= 0;
					field.append(character);
					insideQuotedString 			= false;
					quotedStringFound 			= true;
				}else if( (doubleQuotesCount%2) == 1 && character == ',' ){
					//System.out.println("field2 = <"+simplifyField(field)+">, field2 = <"+field+">");
					simplifyField();
					field 							= registroCSV.getField();
					if(field == null) 			break;
					antepenultimateChar 			= 0;
					penultimateChar 				= 0;
					doubleQuotesCount   			= 0;
					quotedStringFound   			= false;
					insideQuotedString 			= false;
					firstCharIsNotDoubleQuote 	= false;
				}else{
					antepenultimateChar = penultimateChar;
					penultimateChar 	= character;
					field.append(character);
				}
				
			}else if(character == ',' && !insideQuotedString){
				//System.out.println("field3 = <"+simplifyField(field)+">, field3 = <"+field+">");
				simplifyField();
				field 							= registroCSV.getField();
				if(field == null) 			break;
				antepenultimateChar 			= 0;
				penultimateChar 				= 0;
				quotedStringFound   			= false;
				doubleQuotesCount   			= 0;
				firstCharIsNotDoubleQuote 	= false;
			}else{
				field.append(character);
				if(insideQuotedString){
					antepenultimateChar 	= penultimateChar;
					penultimateChar 		= character;
				}
			}
			
	
		}
		//System.out.println(" field4 = <"+field+">");
		simplifyField();
		//field 							= registroCSV.getField();
		
		//log.info("parse(S)");
		
	}

	public void simplifyField(){
		
		//log.info("simplifyField(E)");
		
		if( field == null || field.length() == 0) return;
		if( in == null ) return;
		
		in.delete(0,in.length());
		for(int i=0;i<field.length();i++){
			in.append(field.charAt(i));
		}
		
		if( in.length() == 0) return;
		
		StringBuffer resultado = field;
		resultado.delete(0,resultado.length());
		
		char 		previousChar					= 0;
		boolean	quotedStringFound				= false;
		
		char 		character 						= 0;
		boolean 	insideQuotedString 			= false;
		int 		doubleQuotesCount				= 0;
		boolean 	firstCharIsDoubleQuote 		= false;
		boolean 	firstCharIsNotDoubleQuote 	= false;
		
		for(int i=0;i<in.length();i++){
			
			character = in.charAt(i);
			
			if(character == '"' && insideQuotedString ){
				++doubleQuotesCount;
			}
			
			if(i == 0 && character != '"'){
				firstCharIsNotDoubleQuote = true;
			}
			
			if(firstCharIsNotDoubleQuote){
				resultado.append(character);
				continue;
			}
			
			if( i == 0 && character == '"'){
				
				firstCharIsDoubleQuote 	= true;
				previousChar 			= 0;
				insideQuotedString 		= true;

			}else if(i == (in.length()-1) && character == '"' && insideQuotedString){
				if((doubleQuotesCount%2) == 1){
					previousChar = character;
					if(!firstCharIsDoubleQuote) {
						resultado.append(character);
					}
				}else{
					resultado.append(character);
				}
			}else if(quotedStringFound){
				previousChar = character;
				resultado.append(character);
			}else if( character == '"' && !insideQuotedString){
				
				previousChar 			= 0;
				insideQuotedString 		= true;
				resultado.append(character);
				
			}else if(previousChar == '"' && character == '"' && insideQuotedString && ((doubleQuotesCount%2) == 0) ){
				
					previousChar 		= character;
				
			}else if(previousChar == '"' && character != '"' && insideQuotedString){
				
				if((doubleQuotesCount%2) == 1){
					
					previousChar 			= 0;
					insideQuotedString 	= false;
					quotedStringFound		= true;
					
					if(firstCharIsDoubleQuote) {
						resultado.deleteCharAt(resultado.length()-1);
					}
					resultado.append(character);
				}else{
					previousChar = character;
					resultado.append(character);
				}
				
			}else{
				
				previousChar 			= character;
				resultado.append(character);
				
			}
			
		}
		
		//log.info("simplifyField(S)");
		
	}
	
	public void filtraCaracter(StringBuffer campo,char caracter){
		
		if( campo == null || campo.length() == 0) return;
		if( in == null ) return;
		
		in.delete(0,in.length());
		for(int i=0;i<campo.length();i++){
			in.append(campo.charAt(i));
		}
		
		if(in.length() == 0) return;
		campo.delete(0,campo.length());
		
		for(int i=0;i<in.length();i++){
			if(caracter != in.charAt(i))	campo.append(in.charAt(i));
		}
		
	}
	
	public void agregaGuionesARFC(StringBuffer campo){
		
		if(campo == null || campo.length() == 0) return;
		
		char tipoPersona = campo.length()==13?'F':(campo.length()==12?'M':(char)0);
		
		if(tipoPersona == 'F'){
			campo.insert(4,  '-');
			campo.insert(11, '-');
		}else if(tipoPersona == 'M'){
			campo.insert(3,  '-');
			campo.insert(10, '-');
		}
		
	}

}