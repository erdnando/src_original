package com.netro.informacion;

import netropology.utilerias.*;
import java.util.*;
import javax.naming.Context;

public class FiltroFechaCompranet{

   private HashMap      permisoMes     = null;
   private StringBuffer mesTmp         = null;

   public FiltroFechaCompranet(int mesInicial, int mesFinal, boolean reemplazarRegistros)
      throws Exception{

      // 1. Inicializar los Buffers
      mesTmp      = new StringBuffer(256);

      // 2. Construir lista de meses para los cuales esta permitido cargar informacion

      // Inicializar HashMap de Permisos
      permisoMes  = new HashMap();
      for(int i=1;i<=12;i++){
         permisoMes.put(String.valueOf(i),"false");
         if(i<10){
            permisoMes.put("0"+String.valueOf(i),"false");
         }
      }

      // Si se selecciono la opcion de reemplazar registros, reemplazar todos los meses indicados en el rango
      if(reemplazarRegistros){
         for(int i=mesInicial;i<=mesFinal;i++){
            permisoMes.put(String.valueOf(i),"true");
            if(i<10){
               permisoMes.put("0"+String.valueOf(i),"true");
            }
         }
      }else{
      // Respetar los registros que ya tienen informacion, por lo que solo se dara permiso a los que no tienen datos

         // Instanciacion del EJB de Reportes Especiales
		 RepEspecialesProv repEspecialesProv = ServiceLocator.getInstance().lookup("RepEspecialesProvEJB", RepEspecialesProv.class);

         // Obtener Catalogo de Meses
         List     lista 	      = repEspecialesProv.getBusquedaRegistrosEnCompranet(mesInicial, mesFinal);

         // Agregar permitoso
         HashMap  registro       = null;
         boolean  existenDatos   = false;
         int      numeroMes      = 0;
         for(int i=0;i<lista.size();i++){

            registro       = (HashMap) lista.get(i);
            existenDatos   = "true".equals((String)registro.get("EXISTE_REGISTRO"))?true:false;
            numeroMes      = Integer.parseInt((String)registro.get("MES"));

            if(!existenDatos){
               permisoMes.put(String.valueOf(numeroMes),"true");
               if(numeroMes<10){
                  permisoMes.put("0"+String.valueOf(numeroMes),"true");
               }
            }

         }

      }

   }

   public boolean insertarRegistro(StringBuffer fecha){

      boolean        insertar 		= false;

      StringBuffer   bufferMes      = null;

      int            mes            = 0;

      try{

         bufferMes   = getNumeroDelMes(fecha);
         insertar   	= "true".equals((String) permisoMes.get(bufferMes.toString()))?true:false;

         // Si el mes tiene un numero valido pero esta fueran del rango de meses, insertarlo para que
         // se valide como una fecha con error
         if(!insertar){
            mes = Integer.parseInt(bufferMes.toString());
            if(mes < 0 || mes > 12) insertar = true;
         }

      }catch(Exception e){
         // La fecha tiene errores, permitir su carga para que pueda ser validada y su error reportado
         return true;
      }

      return insertar;

   }

   public StringBuffer getNumeroDelMes(StringBuffer fecha){
      int numBarrasEncontradas   = 0;
      mesTmp.delete(0,mesTmp.length());
      int ctaCaracteres          = 0;
      for(int i=0;i<fecha.length();i++){
         char caracter = fecha.charAt(i);
         if(caracter == '/'){
            ++numBarrasEncontradas;
            if(numBarrasEncontradas == 2)
            {
               break;
            }else{
               continue;
            }
         }
         if(numBarrasEncontradas == 1){
            mesTmp.append(caracter);
            ctaCaracteres++;
            if(ctaCaracteres==3) break;
         }
      }
      return mesTmp;
   }

}