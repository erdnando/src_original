package com.netro.informacion;
 
public class RegistroCSVSiag{
	
	private static final int BUFFER_SIZE			= 512;
 
	private StringBuffer razonSocial        		= null;
	private StringBuffer rfc                		= null; 
	private StringBuffer estado             		= null; 
	private StringBuffer montoActualCredito 		= null;
	private StringBuffer plazo              		= null; 
	private StringBuffer intermediario      		= null;
	private StringBuffer estratoInicial     		= null;
	private StringBuffer nuevoEstrato       		= null; 
	private StringBuffer sector             		= null; 
	private StringBuffer fechaRegistro           = null;
	private StringBuffer descripcionPortafolio   = null;
	private StringBuffer claveCONREC 				= null;
	private StringBuffer camposAdicionales 		= null;
 
	private int				fieldIndex					= 0;
		
	public RegistroCSVSiag(){
 
		this.razonSocial 					= new StringBuffer(BUFFER_SIZE);
		this.rfc 							= new StringBuffer(BUFFER_SIZE);
		this.estado 						= new StringBuffer(BUFFER_SIZE);
		this.montoActualCredito 		= new StringBuffer(BUFFER_SIZE);
		this.plazo 							= new StringBuffer(BUFFER_SIZE);
		this.intermediario 				= new StringBuffer(BUFFER_SIZE);
		this.estratoInicial 				= new StringBuffer(BUFFER_SIZE);
		this.nuevoEstrato 				= new StringBuffer(BUFFER_SIZE);
		this.sector 						= new StringBuffer(BUFFER_SIZE);
		this.fechaRegistro 				= new StringBuffer(BUFFER_SIZE);
		this.descripcionPortafolio 	= new StringBuffer(BUFFER_SIZE);
		this.claveCONREC 					= new StringBuffer(BUFFER_SIZE);
		this.camposAdicionales 			= new StringBuffer(10*BUFFER_SIZE);
 
		this.fieldIndex					= 0;
		
	}
	
	public void doReset(){
		
		this.fieldIndex					= 0;
		
		this.razonSocial.delete(0,razonSocial.length());
		this.rfc.delete(0,rfc.length());
		this.estado.delete(0,estado.length());
		this.montoActualCredito.delete(0,montoActualCredito.length());
		this.plazo.delete(0,plazo.length());
		this.intermediario.delete(0,intermediario.length());
		this.estratoInicial.delete(0,estratoInicial.length());
		this.nuevoEstrato.delete(0,nuevoEstrato.length());
		this.sector.delete(0,sector.length());
		this.fechaRegistro.delete(0,fechaRegistro.length());
		this.descripcionPortafolio.delete(0,descripcionPortafolio.length());
		this.claveCONREC.delete(0,claveCONREC.length());
		this.camposAdicionales.delete(0,camposAdicionales.length());

	}
	
	public StringBuffer getRazonSocial(){
		return razonSocial;
	}
	
	public void setRazonSocial(StringBuffer razonSocial) {
		this.razonSocial = razonSocial;
	}
	
	public StringBuffer getRfc() {
		return rfc;
	}
	
	public void setRfc(StringBuffer rfc) {
		this.rfc = rfc;
	}
	
	public StringBuffer getEstado() {
		return estado;
	}
	
	public void setEstado(StringBuffer estado) {
		this.estado = estado;
	}
	
	public StringBuffer getMontoActualCredito() {
		return montoActualCredito;
	}
	
	public void setMontoActualCredito(StringBuffer montoActualCredito) {
		this.montoActualCredito = montoActualCredito;
	}
	
	public StringBuffer getPlazo() {
		return plazo;
	}
	
	public void setPlazo(StringBuffer plazo) {
		this.plazo = plazo;
	}
	
	public StringBuffer getIntermediario() {
		return intermediario;
	}
	
	public void setIntermediario(StringBuffer intermediario) {
		this.intermediario = intermediario;
	}
	
	public StringBuffer getEstratoInicial() {
		return estratoInicial;
	}
	
	public void setEstratoInicial(StringBuffer estratoInicial) {
		this.estratoInicial = estratoInicial;
	}
	
	public StringBuffer getNuevoEstrato() {
		return nuevoEstrato;
	}
	
	public void setNuevoEstrato(StringBuffer nuevoEstrato) {
		this.nuevoEstrato = nuevoEstrato;
	}
	
	public StringBuffer getSector() {
		return sector;
	}
	
	public void setSector(StringBuffer sector) {
		this.sector = sector;
	}
	
	public StringBuffer getFechaRegistro() {
		return fechaRegistro;
	}
	
	public void setFechaRegistro(StringBuffer fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	public StringBuffer getDescripcionPortafolio() {
		return descripcionPortafolio;
	}
	
	public void setDescripcionPortafolio(StringBuffer descripcionPortafolio) {
		this.descripcionPortafolio = descripcionPortafolio;
	}
	
	public StringBuffer getClaveCONREC() {
		return claveCONREC;
	}
	
	public void setClaveCONREC(StringBuffer claveCONREC) {
		this.claveCONREC = claveCONREC;
	}
	
	public StringBuffer getCamposAdicionales() {
		return camposAdicionales;
	}
	
	public void setCamposAdicionales(StringBuffer camposAdicionales) {
		this.camposAdicionales = camposAdicionales;
	}
	
	public int getFieldIndex(){
		return fieldIndex;
	}
	
	public StringBuffer getField(){
		
		StringBuffer buffer = null;
		
		switch(fieldIndex){
			case  0: buffer = getRazonSocial(); 				this.fieldIndex++; break;
			case  1: buffer = getRfc(); 							this.fieldIndex++; break;
			case  2: buffer = getEstado(); 						this.fieldIndex++; break;
			case  3: buffer = getMontoActualCredito(); 		this.fieldIndex++; break;
			case  4: buffer = getPlazo(); 						this.fieldIndex++; break;
			case  5: buffer = getIntermediario(); 				this.fieldIndex++; break;
			case  6: buffer = getEstratoInicial(); 			this.fieldIndex++; break;
			case  7: buffer = getNuevoEstrato(); 				this.fieldIndex++; break;
			case  8: buffer = getSector(); 						this.fieldIndex++; break;
			case  9: buffer = getFechaRegistro(); 				this.fieldIndex++; break;
			case 10: buffer = getDescripcionPortafolio(); 	this.fieldIndex++; break;
			case 11: buffer = getClaveCONREC(); 				this.fieldIndex++; break;
			case 12: buffer = getCamposAdicionales(); 		this.fieldIndex++; break;
			default:
				buffer = null;
		}
		
		return buffer;
		
	}
	
}