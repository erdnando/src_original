package com.netro.garantias;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

import java.math.BigDecimal;

public class AuxiliarConciliacionAutomatica {
		
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(AuxiliarConciliacionAutomatica.class);
	
	/**
	 * Método que se encarga de transmitir la carga del portafolio IF de la Conciliación Automática de Garantías. 
	 * Actualiza el estatus de la situacion de la conciliación a: 3 ( Carga del Portafolio Del IF ).
	 *
	 * Este m&eacute;todo se ocupa en las siguientes pantallas:
	 *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - CARGA DE ARCHIVO
	 *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - CARGA DE ARCHIVO
	 *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - CARGA DE ARCHIVO
	 *
	 * @throws AppException
	 *
	 *	@param directorioTemporal  <tt>String</tt> con la ruta del directorio donde se subio el archivo a transmitir.
	 *	@param loginUsuario <tt>String</tt> con el login del usuario que realiza la carga.
	 *	@param nombreArchivo <tt>String</tt> con el nombre el nombre del archivo a "transmitir".
	 *	@param acuseFirmaPki <tt>String</tt> con el numero resultante del firmado digital de la transmición del portafolio IF.
	 *	@param numeroRegistros <tt>String</tt> con el n&uacute;mero de Registros cargados por el IF.
	 *	@param montoCarga <tt>String</tt> con el monto de la carga que especificó el IF. 
	 *	@param claveMesConciliar <tt>String</tt> con el n&uacute;mero del Mes a Conciliar. Formato AAAAMM.
	 * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
	 * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
	 * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliaci&oacute;n. Para ver los tipos de conciliaci&oacute;n 
	 *                              usar tabla GTICAT_TIPO_CONCILIACION.
	 * 
	 * @return <tt>int</tt> con la clave de la situaci&oacute;n de la conciliaci&oacute;n. <tt>0</tt> en caso de que el registro de conciliaci&oacute;n
	 * automática no exista.
	 *
	 * @author jshernandez
	 * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 29/10/2013 06:12:11 p.m.
	 *
	 */
	public static ResultadosGar transmitirCargaPortafolioIfConciliacionAutomatica(
			String directorioTemporal,
			String loginUsuario,
			String nombreArchivo,
			String acuseFirmaPki,
			String numeroRegistros,
			String montoCarga,
			String claveMesConciliar,
			String claveFiso,
			String claveIfSiag,
			String claveTipoConciliacion 
		) throws AppException {
		
			log.info("transmitirCargaPortafolioIfConciliacionAutomatica(E)");
			
			AccesoDB 			con 							= new AccesoDB();
			StringBuffer		sentence						= new StringBuffer();
			PreparedStatement ps 							= null;
			ResultSet			rs								= null;
			boolean 				exitoOperacion 			= true;
	 
			ResultadosGar 		retorno 						= new ResultadosGar();
			
			String 				fechaCarga					= null;
			String 				horaCarga					= null;
			
			String 			 	rutaArchivoZip 			= null;
			File 				 	archivoZip 					= null;
			FileInputStream 	archivoZipInputstream	= null;
			
			try {
				
				// Prepar archivo a subir
				rutaArchivoZip 			= directorioTemporal + loginUsuario + "." + nombreArchivo;
				archivoZip 					= new File(rutaArchivoZip);
				archivoZipInputstream 	= new FileInputStream(archivoZip);
				
				// Conectarse a la Base de Datos
				con.conexionDB();
							
				// Subir archivo y actualizar estatus registros
				sentence.setLength(0);
				sentence.append(
					"UPDATE                                       "  +
					"   GTI_CONC_ARCHIVO_PORTAFOLIO               "  +
					"SET                                          "  +
					"   BI_ARCHIVO_PORTAFOLIO_IF       = ?,       "  +
					"   IG_PKI_PORTAFOLIO_IF           = ?,       "  +
					"   IN_TOT_REGISTROS_PORTAFOLIO_IF = ?,       "  +
					"   FN_MONTO_CARGA                 = ?,       "  +
					"   CS_ORIGEN_PORTAFOLIO           = 'F',     "  + // F = "FRONT"
					"   IC_SITUACION_CONCILIACION      = 3,       "  + // CLAVE 3,	"CARGA DEL PORTAFOLIO DEL IF"
					"   CG_USUARIO_ACTUALIZA           = ?,       "  +
					"   DF_FECHA_ACTUALIZA             = SYSDATE, "  +
					"   CG_USUARIO_CARGA               = ?,       "  +
					"   DF_FECHA_CARGA                 = SYSDATE  "  +
					"WHERE                                        "  +
					"   IC_MES_CONCILIAR               = ? AND    "  +
					"   IC_FISO                        = ? AND    "  +
					"   IC_IF_SIAG                     = ? AND    "  +
					"   IC_TIPO_CONCILIACION           = ?        "
				);
				
				ps = con.queryPrecompilado(sentence.toString());
				ps.setBinaryStream(1, 	archivoZipInputstream, (int)archivoZip.length() );
				ps.setBigDecimal(2, 		new BigDecimal(acuseFirmaPki)							);
				ps.setBigDecimal(3, 		new BigDecimal(numeroRegistros)						);
				ps.setBigDecimal(4, 		new BigDecimal(montoCarga)								);
				ps.setString(5, 			loginUsuario												);
				ps.setString(6,			loginUsuario												);
				ps.setInt(7,  				Integer.parseInt(claveMesConciliar)					); 
				ps.setLong(8, 				Long.parseLong(claveFiso)								); 
				ps.setInt(9,  				Integer.parseInt(claveIfSiag)							); 
				ps.setInt(10, 				Integer.parseInt(claveTipoConciliacion)			);
			
				ps.executeUpdate();
				ps.close();
				
				// Consultar fecha y hora de carga
				sentence.setLength(0);
				sentence.append(
					"SELECT                                                  "  +
					"   TO_CHAR(DF_FECHA_CARGA,'DD/MM/YYYY') AS FECHA_CARGA, "  +
					"   TO_CHAR(DF_FECHA_CARGA,'HH:MI')      AS HORA_CARGA   "  +
					"FROM                                                    "  +
					"   GTI_CONC_ARCHIVO_PORTAFOLIO                          "  +
					"WHERE                                                   "  +
					"   IC_MES_CONCILIAR               = ? AND               "  +
					"   IC_FISO                        = ? AND               "  +
					"   IC_IF_SIAG                     = ? AND               "  +
					"   IC_TIPO_CONCILIACION           = ?                   "
				);
				ps = con.queryPrecompilado(sentence.toString());
				ps.setInt(1,  			Integer.parseInt(claveMesConciliar)		); 
				ps.setLong(2, 			Long.parseLong(claveFiso)					); 
				ps.setInt(3,  			Integer.parseInt(claveIfSiag)				); 
				ps.setInt(4,  			Integer.parseInt(claveTipoConciliacion));

				rs = ps.executeQuery();
				if(rs.next()){
					fechaCarga = (rs.getString("FECHA_CARGA")		== null)?"":rs.getString("FECHA_CARGA");
					horaCarga  = (rs.getString("HORA_CARGA")		== null)?"":rs.getString("HORA_CARGA");
				}
				
				// Regresar resultado de la carga
				retorno.setFolio(acuseFirmaPki);
				retorno.setFecha(fechaCarga);
				retorno.setHora(horaCarga);
					
			} catch(Exception e) {
				
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica(Exception)");				
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.directorioTemporal    = <" + directorioTemporal    + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.loginUsuario          = <" + loginUsuario          + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.nombreArchivo         = <" + nombreArchivo         + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.acuseFirmaPki         = <" + acuseFirmaPki         + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.numeroRegistros       = <" + numeroRegistros       + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.montoCarga            = <" + montoCarga            + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.claveMesConciliar     = <" + claveMesConciliar     + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.claveFiso             = <" + claveFiso             + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.claveIfSiag           = <" + claveIfSiag           + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.claveTipoConciliacion = <" + claveTipoConciliacion + ">");
				log.error("transmitirCargaPortafolioIfConciliacionAutomatica.rutaArchivoZip        = <" + rutaArchivoZip        + ">");
				e.printStackTrace();
				
				exitoOperacion = false;
				
				throw new AppException("Ocurrió un error al transmitir la carga del portafolio if: " + e.getMessage() + ". La operación ha sido cancelada.");
				
			} finally {
				
				if( archivoZipInputstream 	!= null	) { try { archivoZipInputstream.close(); 	} catch(Exception e) {} }
				
				if( rs      					!= null 	) { try { rs.close();      					} catch(Exception e) {} }
				if( ps      					!= null 	) { try { ps.close();      					} catch(Exception e) {} }

				if (con.hayConexionAbierta()) {
					con.terminaTransaccion(exitoOperacion);
					con.cierraConexionDB();
				}
				
				log.info("transmitirCargaPortafolioIfConciliacionAutomatica(S)");
				
			}
	
			return retorno;
		
	}
	
}