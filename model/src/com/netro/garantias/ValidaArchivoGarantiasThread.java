package com.netro.garantias;

import com.netro.zip.ComunesZIP;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;

import java.math.BigDecimal;

import java.util.List;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.logging.Log;

/**
 *
 * Clase que se encarga validar archivo de texto de Conciliacion de Garantias, para ello hace uso de una clase
 * de tipo <tt>com.netro.garantias.FileValidator</tt> que a su vez invoca un m�todo desde el EJB de Garantias. Reporta
 * el porcentaje de avance de la validacion en el atributo: <tt>percent</tt>.
 *
 * Este thread se dise�o para usar Validadores espec�ficos de archivo: <tt>com.netro.garantias.FileValidator</tt> y
 * espec�ficamente para las siguientes validaciones:
 *    1. Conciliacion de Saldos
 *    2. Recuperaci�n de Garant�as Pagadas
 *    3. Comisiones
 *
 * @since  F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 08/10/2013 12:04:52 p.m.
 * @author jshernandez
 *
 */

public class ValidaArchivoGarantiasThread implements Runnable, Serializable {
	
	private static final long serialVersionUID = 201310080L;
	
	private  static final int  NUMERO_MAXIMO_CLAVES_REPETIDAS = 500;
	
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(ValidaArchivoGarantiasThread.class);
	
   private volatile int 		numberOfRegisters;
   private volatile float		percent;
   private volatile int 	 	processedRegisters;
   
   private volatile boolean 	running;
   private volatile boolean 	error;
   private volatile boolean 	requestStop;
   
   private boolean 				started;
	private String					processID;
	private int						errorRecordsCount; 
	private StringBuffer			mensajes;
	 
	private ResultadosGar		resultadosGar;
	private String 				claveIF;
	private FileValidator		fileValidator;
	private String  				directorioTemporal;
	private String 				login;
	private String 				rutaArchivoZIP;
	private String 				rutaArchivoTXT;
	private GarantiasBean   	garantias;
	private int 					totalRegistros;
	private BigDecimal 			sumatoriaMontos;

   public ValidaArchivoGarantiasThread() {
   	
        	numberOfRegisters 	= 0;
        	percent 		   		= 0;
        	started 			   	= false;
        	running 			   	= false;
	     	processID 	   		= null;
		  	processedRegisters 	= 0;
		  	error						= false;
		  	errorRecordsCount		= 0;
       	mensajes   				= new StringBuffer(256); // Crear un buffer donde se guardar�n los mensajes de error
		  
			resultadosGar 			= null;
			claveIF 					= null;
			fileValidator 			= null;
			directorioTemporal 	= null;
			login 					= null;
			rutaArchivoZIP 		= null;
			rutaArchivoTXT 		= null;
			garantias				= null;
	
			requestStop 			= false;
   }
	 
   protected void validateRegisters(BufferedReader br) {
		
	   try {

			fileValidator.validaRegistro(this.processedRegisters+1,br);					
			processedRegisters++;
			percent = (processedRegisters/(float)numberOfRegisters)*100;
			
			if( fileValidator.getHayError() ){
				errorRecordsCount++;
			}
			
			if( errorRecordsCount > 1000 ){
				// Detener el thread
				setRunning(false);
				numberOfRegisters = processedRegisters;
				percent				= 100F;
			}
			
		} catch(Exception e) {
			
			setRunning(false);
			numberOfRegisters = processedRegisters ;
			percent				= 100F;
			setError(true);
			this.mensajes.setLength(0);
			this.mensajes.append("Ocurri� un error inesperado al validar registro "+ ( this.processedRegisters -1 )+": " + e.getMessage() + "; se aborta el proceso.");
			
			log.error("validateRegisters(Exception): Ocurri� un error inesperado al validar registro "+ ( this.processedRegisters -1 )+": " + e.getMessage() + "; se aborta el proceso.");
			log.error("validateRegisters.numberOfRegisters  = <" + numberOfRegisters  + ">");
			log.error("validateRegisters.percent            = <" + percent            + ">");
			log.error("validateRegisters.started            = <" + started            + ">");
			log.error("validateRegisters.running            = <" + running            + ">");
			log.error("validateRegisters.processID          = <" + processID          + ">");
			log.error("validateRegisters.processedRegisters = <" + processedRegisters + ">");
			log.error("validateRegisters.error              = <" + error              + ">");
			log.error("validateRegisters.errorRecordsCount  = <" + errorRecordsCount  + ">");
			
			log.error("validateRegisters.resultadosGar      = <" + resultadosGar      + ">");
			log.error("validateRegisters.claveIF            = <" + claveIF            + ">");
			log.error("validateRegisters.fileValidator      = <" + fileValidator      + ">");
			log.error("validateRegisters.directorioTemporal = <" + directorioTemporal + ">");
			log.error("validateRegisters.login              = <" + login              + ">");
			log.error("validateRegisters.rutaArchivoZIP     = <" + rutaArchivoZIP     + ">");
			log.error("validateRegisters.rutaArchivoTXT     = <" + rutaArchivoTXT     + ">");
			log.error("validateRegisters.garantias          = <" + garantias          + ">");
			e.printStackTrace();
			// terminar el proceso si falla la funcion de validacion para algun registro
			
		}
		
   }

   public synchronized int getNumberOfRegisters() {
       return numberOfRegisters;
   }
   
   public synchronized float getPercent() {
       return percent;
   }

   public synchronized boolean isStarted() {
       return started;
   }

   public synchronized boolean isCompleted() {
		//return((processedRegisters == 0 && numberOfRegisters == 0)?false:(processedRegisters == numberOfRegisters));
		return (processedRegisters < numberOfRegisters?false:true);
   }

   public synchronized boolean isRunning() {
       return running;
   }

   public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
   }
	 
   public synchronized String getProcessID() {
       return processID;
   }
   
	public synchronized void setProcessID(String processID) {
       this.processID = processID;
   }

   public synchronized int getProcessedRegisters() {
       return(processedRegisters);
   }

   public synchronized boolean hasError() {
       return error;
   }
   
	public synchronized void setError(boolean error) {
       this.error = error;
   }
		
   // errorRecordsCount

	public synchronized ResultadosGar getResultadosGar() {
		return resultadosGar;
	}

	public void setResultadosGar(ResultadosGar resultadosGar) {
		this.resultadosGar = resultadosGar;
	}

	public synchronized String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public synchronized FileValidator getFileValidator() {
		return fileValidator;
	}

	public void setFileValidator(FileValidator fileValidator) {
		this.fileValidator = fileValidator;
	}

	public synchronized String getDirectorioTemporal() {
		return directorioTemporal;
	}

	public void setDirectorioTemporal(String directorioTemporal) {
		this.directorioTemporal = directorioTemporal;
	}

	public synchronized String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public synchronized String getRutaArchivoZIP() {
		return rutaArchivoZIP;
	}

	public void setRutaArchivoZIP(String rutaArchivoZIP) {
		this.rutaArchivoZIP = rutaArchivoZIP;
	}

	public String getRutaArchivoTXT() {
		return rutaArchivoTXT;
	}

	public void setRutaArchivoTXT(String rutaArchivoTXT) {
		this.rutaArchivoTXT = rutaArchivoTXT;
	}

	public GarantiasBean getGarantiasBean() {
		return this.garantias;
	}

	public void setGarantiasBean(GarantiasBean garantias) {
		this.garantias = garantias;
	}

	public int getTotalRegistros() {
		return this.totalRegistros;
	}

	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}

	public BigDecimal getSumatoriaMontos() {
		return this.sumatoriaMontos;
	}

	public void setSumatoriaMontos(BigDecimal sumatoriaMontos) {
		this.sumatoriaMontos = sumatoriaMontos;
	}

	public void initialize(){
	
		log.info("initialize(E)");
		
		BufferedReader 	br 	= null;
		String 				linea = null;
		
		this.resultadosGar 		= new ResultadosGar(ResultadosGar.CARGA_MASIVA);
		
		// Configurar Propiedades Adicionales del File Validator
   	try {
   		
   		// Asignar processID
   		String calculatedProcessID = login  + "-" + System.currentTimeMillis() + "-" + (int)( Math.random() * 1000 );
   		this.setProcessID( calculatedProcessID );
 
   		// Obtener Reglas de Validacion en Base al Programa al cual pertenece el IF
   		ReglasPrograma reglasValidacionParametros = garantias.getReglasValidacionParametrosPorPrograma(this.getClaveIF());
			this.resultadosGar.setReglasValidacionParametros(reglasValidacionParametros);
			
			// Debido a que la informacion no se almacena en ninguna tabla temporal, el id del proceso carece de uso
			resultadosGar.setIcProceso(-1);
			
   		// Asignar clave IF SIAG
   		int 			claveIfSiag  = garantias.getClaveIfSiag(this.getClaveIF());

   		// Descomprimir archivo
			String archivoTXT = descomprimeArchivo( this.getDirectorioTemporal(), this.getLogin(), this.getRutaArchivoZIP() );

			// Validar que el archivo ZIP contenga un archivo TXT
			if( archivoTXT == null ){
				setError(true);
				this.mensajes.setLength(0);
				this.mensajes.append("El archivo comprimido viene vac�o.");
			// Validar que la extension del archivo cargado sea zip
			} else if( !archivoTXT.matches("(?i)^.*\\.txt$") ){
				setError(true);
				this.mensajes.setLength(0);
				this.mensajes.append("El archivo comprimido no es txt.");
			} else {
						
				// Determinar la ruta completa del archivo TXT
				this.setRutaArchivoTXT( directorioTemporal + login + "." + archivoTXT );
		
				// Obtener numero de registros			
				br = new BufferedReader(new InputStreamReader(new FileInputStream( this.getRutaArchivoTXT() ),"ISO-8859-1"));
				while( (linea = br.readLine()) != null) {
					++this.numberOfRegisters;
				}	
	
				// Obtener la logitud de la Clave del Financiamiento
				int longitudClaveFinanciamiento = garantias.operaProgramaSHFCreditoHipotecario(claveIF)?30:20;
				
				// Definir el n�mero m�ximo de lineas de error a mostrar
				this.resultadosGar.setNumeroMaximoErrores(1000);
				this.resultadosGar.setUsaConteoErrorEnMetodoSetErrorLinea(true);
				
				// CONFIGURAR FILE VALIDATOR
				
				// Asignar processID
				this.fileValidator.setProcessID( calculatedProcessID );
				
				// Asignar Objeto que guardar� los resultados
				this.fileValidator.setResultadosGar( this.getResultadosGar() );
				
				// Asignar clave IF SIAG
				this.fileValidator.setClaveIfSiag(claveIfSiag);
				
				// Asignar EJB de Garantias
				this.fileValidator.setGarantiasBean( this.getGarantiasBean() );
				
				// Asignar la longitud de la Clave del Financiamiento
				this.fileValidator.setLongitudClaveFinanciamiento( longitudClaveFinanciamiento );
				
				// Configurar las Cifras de Control
				this.fileValidator.setTotalRegistros(this.getTotalRegistros());
				this.fileValidator.setSumatoriaMontos(this.getSumatoriaMontos());
				
				// Determinar si el archivo ZIP, dentro de su estructura, posee al menos un directorio.
				boolean contieneDirectorio = ComunesZIP.contieneDirectorio( this.getRutaArchivoZIP() );
				this.fileValidator.setArchivoZIPContieneDirectorio( contieneDirectorio );
				
				// Determinar si el archivo ZIP, posee m�s de un archivo comprimido.
				boolean contieneMasDeUnArchivo = ComunesZIP.contieneMasDeUnArchivo( this.getRutaArchivoZIP() );
				this.fileValidator.setArchivoZIPContieneMasDeUnArchivo( contieneMasDeUnArchivo );
				
				// Inicializar lista de Garant�as repetidas
				List clavesFinanciamientoRepetidas = this.fileValidator.getClavesFinanciamientoRepetidas( this.getRutaArchivoTXT(), this.numberOfRegisters, NUMERO_MAXIMO_CLAVES_REPETIDAS + 1 );
				if( clavesFinanciamientoRepetidas != null && clavesFinanciamientoRepetidas.size() > NUMERO_MAXIMO_CLAVES_REPETIDAS ){
					this.setError(true);
					this.mensajes.setLength(0);
					this.mensajes.append("Se encontraron mas de " + NUMERO_MAXIMO_CLAVES_REPETIDAS + " Claves de Financiamiento repetidas. Se cancela la operaci�n.");
				} else {
					this.fileValidator.setClavesFinanciamientoRepetidas(clavesFinanciamientoRepetidas);
				}
			
			}
			
   	} catch (Exception e) {
			
   		log.error("initialize(Exception): Fall� la inicializaci�n de par�metros en el Thread de validaci�n: "+e.getMessage());
			log.error("initialize.numberOfRegisters  		= <" + numberOfRegisters  + ">");
			log.error("initialize.percent            		= <" + percent            + ">");
			log.error("initialize.started            		= <" + started            + ">");
			log.error("initialize.running            		= <" + running            + ">");
			log.error("initialize.processID          		= <" + processID          + ">");
			log.error("initialize.processedRegisters 		= <" + processedRegisters + ">");
			log.error("initialize.error              		= <" + error              + ">");
			log.error("initialize.errorRecordsCount  		= <" + errorRecordsCount  + ">");
			
			log.error("initialize.resultadosGar      		= <" + resultadosGar      + ">");
			log.error("initialize.claveIF            		= <" + claveIF            + ">");
			log.error("initialize.fileValidator      		= <" + fileValidator      + ">");
			log.error("initialize.directorioTemporal 		= <" + directorioTemporal + ">");
			log.error("initialize.login              		= <" + login              + ">");
			log.error("initialize.rutaArchivoZIP     		= <" + rutaArchivoZIP     + ">");
			log.error("initialize.rutaArchivoTXT     		= <" + rutaArchivoTXT     + ">");
			log.error("validateRegisters.garantias   		= <" + garantias          + ">");
			log.error("validateRegisters.totalRegistros	= <" + totalRegistros     + ">");
			log.error("validateRegisters.sumatoriaMontos	= <" + sumatoriaMontos.toPlainString()   + ">");
			e.printStackTrace();
			
			setError(true);
			this.mensajes.setLength(0);
			this.mensajes.append("Fall� la inicializaci�n de par�metros en el Thread de validaci�n: "+e.getMessage());
			this.numberOfRegisters = 0;
			
			
		} finally {
			
			if( br != null ) { try { br.close(); }catch(Exception e){} } 
				
		}
				
	}
	
   public void run() {

		boolean lbOK = true;

		// Verificar que no se hayan devuelto datos vacios
		if ( numberOfRegisters == 0 ){
			numberOfRegisters  	= 0;
			processedRegisters 	= 0;
			percent					= 100F;
			lbOK 						= false;
		}
		
		// Obtener Buffered Reader con el Archivo Cargado
		BufferedReader br = null;
		try {
			
			br = lbOK?new BufferedReader(new InputStreamReader(new FileInputStream( this.getRutaArchivoTXT() ),"ISO-8859-1")):null;
			
		} catch(Exception e) {
			
			if( br != null) { try { br.close(); br=null; }catch(Exception e2){} }
			lbOK 					= false;
			setRunning(false);
			numberOfRegisters = processedRegisters ;
			percent				= 100F;
			setError(true);
			this.getResultadosGar().appendErrores("Ocurri� un error al abrir archivo TXT: "+e.getMessage());
			
			log.error("run: Error en ValidaArchivoGarantiasThread::run al extraer numero de registro de la base de datos");
			log.error("run.numberOfRegisters  = <" + numberOfRegisters  + ">");
			log.error("run.percent            = <" + percent            + ">");
			log.error("run.started            = <" + started            + ">");
			log.error("run.running            = <" + running            + ">");
			log.error("run.processID          = <" + processID          + ">");
			log.error("run.processedRegisters = <" + processedRegisters + ">");
			log.error("run.error              = <" + error              + ">");
			log.error("run.errorRecordsCount  = <" + errorRecordsCount  + ">");
			
			log.error("run.resultadosGar      = <" + resultadosGar      + ">");
			log.error("run.claveIF            = <" + claveIF            + ">");
			log.error("run.fileValidator      = <" + fileValidator      + ">");
			log.error("run.directorioTemporal = <" + directorioTemporal + ">");
			log.error("run.login              = <" + login              + ">");
			log.error("run.rutaArchivoZIP     = <" + rutaArchivoZIP     + ">");
			log.error("run.rutaArchivoTXT     = <" + rutaArchivoTXT     + ">");
			log.error("run.garantias          = <" + garantias          + ">");
			log.error("run.totalRegistros     = <" + totalRegistros     + ">");
			log.error("run.sumatoriaMontos    = <" + sumatoriaMontos.toPlainString()    + ">");
			e.printStackTrace();
			
		}

		// Realizar la Validacion de los datos
      try {
			
			if(lbOK == false){ 
				setRunning(false);
			}else{
				setRunning(true);
			}

			while (isRunning() && !isCompleted()){
				
				// Revisar si se le ha dado la orden al thread de detenerse
			 	if(  this.getRequestStop() ){
					break;
				}
				
				validateRegisters(br);
				// Debug info 
				if( (processedRegisters % 350) == 0){
					log.debug("run: Validating Registers");
					log.debug("run.numberOfRegisters:	" + numberOfRegisters);
					log.debug("run.percent:    			" + percent);
					log.debug("run.started:       		" + started);
					log.debug("run.running:       		" + running);
					log.debug("run.completed:     		" + isCompleted());
					log.debug("run.processID: 			   " + processID);
					log.debug("run.processedRegisters:  " + processedRegisters );
				}
			}
			
			// VALIDAR CIFRAS DE CONTROL
			// .......................................................................................
			this.fileValidator.validaCifrasControl();
				
			// VALIDAR ARCHIVO ZIP
			// .......................................................................................
			this.fileValidator.validaArchivoZIP();
			
			// Debug info
			log.debug("run: Validating Registers");
			log.debug("run.numberOfRegisters:	" + numberOfRegisters);
			log.debug("run.percent:    			" + percent);
			log.debug("run.started:       		" + started);
			log.debug("run.running:       		" + running);
			log.debug("run.completed:     		" + isCompleted());
			log.debug("run.processID: 			   " + processID);
			log.debug("run.processedRegisters:  " + processedRegisters );

      } finally {
      	if( br != null) { try { br.close(); }catch(Exception e){} }
			setRunning(false);
      }

    }
	 
	private String descomprimeArchivo( String directorioTemporal, String login, String archivoZIP ) {
			
		log.info("descomprimeArchivo(E)");
			
		String 				descomprimido 	= null;
		
		InputStream 		inStream 		= null;
		ZipArchiveInputStream 	zipInStream 	= null;
		ZipArchiveEntry 			zipEntry 		= null;
		
		byte 					arrByte[] 		= new byte[4096];
		OutputStream 		outstream 		= null;
		
		try {
			
			inStream 		= new FileInputStream(archivoZIP);
			zipInStream 	= new ZipArchiveInputStream(inStream);
			
			while ( (zipEntry = zipInStream.getNextZipEntry()) != null ) {
			
				if(zipEntry.isDirectory()) {
					continue; /*Ignora Directorios*/
				}
			
				String 	rutaArchivo = null;
				rutaArchivo 		 	= zipEntry.getName();
				
				// Obtener extension del archivo
				String 	extension	= "";
				int 		indice 		= rutaArchivo.lastIndexOf(".");
				if( indice > 0 ){
					extension = rutaArchivo.substring(indice);	
				}

				// Generar Nombre del Archivo
				descomprimido 	= Comunes.cadenaAleatoria(16) + extension;
				login		  		= login == null || login.matches("\\s*")?"":login + ".";
				outstream     	= new BufferedOutputStream( new FileOutputStream(directorioTemporal + login + descomprimido ) );
				
				int i = 0;
				while((i = zipInStream.read(arrByte, 0, arrByte.length)) != -1) {
					outstream.write(arrByte, 0, i);
					outstream.flush();
				}
				if( outstream  != null ) {
					outstream.close();
					outstream = null;
				}
				break;
			}
		} catch(Exception e){
			
			log.error("descomprimeArchivo(Exception)");
			log.error("descomprimeArchivo.directorioTemporal = <" + directorioTemporal + ">");
			log.error("descomprimeArchivo.login              = <" + login              + ">");
			log.error("descomprimeArchivo.archivoZIP         = <" + archivoZIP         + ">"); 
			
			e.printStackTrace();
			
			throw new AppException("No se pudo descomprimir el archivo: " + e.getMessage() );
			
		} finally {
			
			if( outstream   != null ){
				try { 
					outstream.close();
				} catch(Exception e){
					log.error("descomprimeArchivo(Exception)", e);
				}
			}
			if( zipInStream != null ){
				try {
					zipInStream.close();
				} catch(Exception e){
					log.error("descomprimeArchivo(Exception)", e);
				}
			}
			log.info("descomprimeArchivo(S)");
		}
		return descomprimido;
	}
	
	public String getMensajes(){
		return this.mensajes.toString();
	}
	
	public synchronized void setRequestStop(boolean requestStop){
		if( !this.running && !this.isCompleted() ){
			setRunning(false);
			 // setCompleted(true);
			numberOfRegisters = processedRegisters;
			percent = 100F;
		}
		this.requestStop = requestStop;
	}
	
	public synchronized boolean getRequestStop(){
		return this.requestStop;
	}

}