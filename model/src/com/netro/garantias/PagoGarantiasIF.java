package com.netro.garantias;
import com.netro.exception.NafinException;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;

public class PagoGarantiasIF implements java.io.Serializable
{
	private String claveIF;
	private String claveUsuario;
	
	private String pkcs7;
	private String textoFirmado;
	private String serialCertificado;

//	private String rfcAcreditado;
	private String claveGarantia;
//	private String claveMoneda;
//	private String fechaVencimiento;
//	private String importeCapitalVencido;
//	private String importeIntereses;
//	private String porcentajeParticipacion;
//	private String importePago;
	private String fechaPrimerInclumplimiento;  //FODEA 017 - 2009 ACF
	private String causasIncumplimiento;
	private String nombreFuncionarioIF;
	private String telefonoFuncionario;
	private String extensionFuncionario;
	private String mailFuncionario;
	private String localizacionSupervision;
	private String ciudadSupervision;
	private String estadoSupervision;
//	private String nombreBeneficiario;
	private String proceso;
  private String fechaSegundoInclumplimiento; // FODEA 017 - 2009 ACF
  private String periodicidadPago;  // FODEA 017 - 2009 ACF

	public PagoGarantiasIF()
	{
	}


	public void setClaveGarantia(String claveGarantia)
	{
		this.claveGarantia = claveGarantia;
	}


	public String getClaveGarantia()
	{
		return claveGarantia;
	}
// FODEA 017 - 2009 ACF (I)
	public void setFechaPrimerInclumplimiento(String fechaPrimerInclumplimiento)
	{
		this.fechaPrimerInclumplimiento = fechaPrimerInclumplimiento;
	}


	public String getFechaPrimerInclumplimiento()
	{
		return fechaPrimerInclumplimiento;
	}
// FODEA 017 - 2009 ACF (I)

	public void setCausasIncumplimiento(String causasIncumplimiento)
	{
		this.causasIncumplimiento = causasIncumplimiento;
	}


	public String getCausasIncumplimiento()
	{
		return causasIncumplimiento;
	}


	public void setNombreFuncionarioIF(String nombreFuncionarioIF)
	{
		this.nombreFuncionarioIF = nombreFuncionarioIF;
	}


	public String getNombreFuncionarioIF()
	{
		return nombreFuncionarioIF;
	}


	public void setTelefonoFuncionario(String telefonoFuncionario)
	{
		this.telefonoFuncionario = telefonoFuncionario;
	}


	public String getTelefonoFuncionario()
	{
		return telefonoFuncionario;
	}


	public void setExtensionFuncionario(String extensionFuncionario)
	{
		this.extensionFuncionario = extensionFuncionario;
	}


	public String getExtensionFuncionario()
	{
		return extensionFuncionario;
	}


	public void setMailFuncionario(String mailFuncionario)
	{
		this.mailFuncionario = mailFuncionario;
	}


	public String getMailFuncionario()
	{
		return mailFuncionario;
	}


	public void setLocalizacionSupervision(String localizacionSupervision)
	{
		this.localizacionSupervision = localizacionSupervision;
	}


	public String getLocalizacionSupervision()
	{
		return localizacionSupervision;
	}


	public void setCiudadSupervision(String ciudadSupervision)
	{
		this.ciudadSupervision = ciudadSupervision;
	}


	public String getCiudadSupervision()
	{
		return ciudadSupervision;
	}


	public void setEstadoSupervision(String estadoSupervision)
	{
		this.estadoSupervision = estadoSupervision;
	}


	public String getEstadoSupervision()
	{
		return estadoSupervision;
	}


	public void setProceso(String proceso)
	{
		this.proceso = proceso;
	}


	public String getProceso()
	{
		return proceso;
	}
	
	public String toString() {
		String cadena = "";
		cadena = "[" +
				"claveIF=" + claveIF + "\n" +
				"claveUsuario=" + claveUsuario + "\n" +
				"pkcs7=" + pkcs7 + "\n" +
				"textoFirmado=" + textoFirmado + "\n" +
				"serialCertificado=" + serialCertificado + "\n" +
				"claveGarantia=" + claveGarantia + "\n" +
				"fechaPrimerInclumplimiento=" + fechaPrimerInclumplimiento + "\n" + //FODEA 017 - 2009 ACF
				"causasIncumplimiento=" + causasIncumplimiento + "\n" +
				"nombreFuncionarioIF=" + nombreFuncionarioIF + "\n" +
				"telefonoFuncionario=" + telefonoFuncionario + "\n" +
				"extensionFuncionario=" + extensionFuncionario + "\n" +
				"mailFuncionario=" + mailFuncionario + "\n" +
				"localizacionSupervision=" + localizacionSupervision + "\n" +
				"ciudadSupervision=" + ciudadSupervision + "\n" +
				"estadoSupervision=" + estadoSupervision + "\n" +
				"proceso=" + proceso + "\n" +
        "fechaSegundoInclumplimiento=" + fechaSegundoInclumplimiento + "\n" + // FODEA 017 - 2009 ACF
        "periodicidadPago=" + periodicidadPago + "\n" + // FODEA 017 - 2009 ACF
				"]";
		
		return cadena;
   }


	public void setClaveIF(String claveIF)
	{
		this.claveIF = claveIF;
	}


	public String getClaveIF()
	{
		return claveIF;
	}


	public void setClaveUsuario(String claveUsuario)
	{
		this.claveUsuario = claveUsuario;
	}


	public String getClaveUsuario()
	{
		return claveUsuario;
	}


	public void setPkcs7(String pkcs7)
	{
		this.pkcs7 = pkcs7;
	}


	public String getPkcs7()
	{
		return pkcs7;
	}


	public void setTextoFirmado(String textoFirmado)
	{
		this.textoFirmado = textoFirmado;
	}


	public String getTextoFirmado()
	{
		return textoFirmado;
	}


	public void setSerialCertificado(String serialCertificado)
	{
		this.serialCertificado = serialCertificado;
	}


	public String getSerialCertificado()
	{
		return serialCertificado;
	}
	
  // FODEA 017 - 2009 ACF (I)
	public String getFechaSegundoInclumplimiento()
	{
		return fechaSegundoInclumplimiento;
	}

	public void setFechaSegundoInclumplimiento(String fechaSegundoInclumplimiento)
	{
		this.fechaSegundoInclumplimiento = fechaSegundoInclumplimiento;
	}
  
	public String getPeriodicidadPago()
	{
		return periodicidadPago;
	}

	public void setPeriodicidadPago(String periodicidadPago)
	{
		this.periodicidadPago = periodicidadPago;
	}
  // FODEA 017 - 2009 ACF (F)
	
	



	/**
	 * Determina si ya existe una clave de garantia duplicada.
	 * Para Pago de garantias (tipoOperacion=3) se considera que ya 
	 * esta registrado si no est� en estatus Aceptado (5) o (8) Rechazado, es
	 * decir cuando aun no ha sido revisada y procesada por el BACK no se puede
	 * volver a registrar el numero de garantia.
	 * 
	 * @return cadena con el Folio en caso de que exista o cadena 
	 * 		vacia de lo contrario
	 * @throws com.netro.exception.NafinException
	 */
	public String existeGarantia() throws NafinException  {
		System.out.println("PagoGarantiasIF::existeGarantia(E)");
		AccesoDB con = new AccesoDB();
		boolean exito = false;

		try {
			con.conexionDB();
			String qrySentencia = 
					" SELECT sp.ic_folio " +
					" FROM gti_solicitud_pago sp, " +
					" 	gti_estatus_solic es, comcat_if i " +
					" WHERE sp.ic_folio = es.ic_folio " +
					" 	AND sp.ic_if_siag = es.ic_if_siag "+
					" 	AND sp.ic_if_siag = i.ic_if_siag " +
					" 	AND es.ic_situacion not in(5,8) "+
					" 	AND i.ic_if = ? "+
					" 	AND sp.cc_garantia = ? ";

			List varBind = new ArrayList();
			varBind.add(new Integer(claveIF));
			varBind.add(this.claveGarantia);

			Registros reg = con.consultarDB(qrySentencia, varBind);
			
			String folio = "";
			if(reg.next()) {
				folio = (reg.getString("ic_folio") == null )?"":reg.getString("ic_folio");
			}
			return folio;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			System.out.println("PagoGarantiasIF::existeGarantia(E)");
		}
	}

}