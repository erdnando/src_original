package com.netro.garantias;

public class AcusePagoGarantia implements java.io.Serializable {

	private String folioSolicitud;
	private String claveGarantia;
	private String fechaCarga;
	private String horaCarga;
	private String fechaValor;

	public AcusePagoGarantia()
	{
	}

	public String getFolioSolicitud()
	{
		return folioSolicitud;
	}

	public void setFolioSolicitud(String folioSolicitud)
	{
		this.folioSolicitud = folioSolicitud;
	}

	public String getClaveGarantia()
	{
		return claveGarantia;
	}

	public void setClaveGarantia(String claveGarantia)
	{
		this.claveGarantia = claveGarantia;
	}

	public String getFechaCarga()
	{
		return fechaCarga;
	}

	public void setFechaCarga(String fechaCarga)
	{
		this.fechaCarga = fechaCarga;
	}

	public String getHoraCarga()
	{
		return horaCarga;
	}

	public void setHoraCarga(String horaCarga)
	{
		this.horaCarga = horaCarga;
	}
	
	public void setFechaValor(String fechaValor)
	{
		this.fechaValor = fechaValor;
	}

	public String getFechaValor()
	{
		return fechaValor;
	}

	
}