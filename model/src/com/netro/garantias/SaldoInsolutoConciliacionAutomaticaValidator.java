package com.netro.garantias;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.AccesoDB;
import org.apache.commons.logging.Log;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import java.math.BigDecimal;

public class SaldoInsolutoConciliacionAutomaticaValidator implements FileValidator {
		
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(SaldoInsolutoConciliacionAutomaticaValidator.class);

	public int getTotalRegistros() {
		return this.totalRegistros;
	}

	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
	public void totalRegistrosAdd(int n) {
		this.totalRegistros += n;
	}

	public BigDecimal getSumatoriaMontos() {
		return this.sumatoriaMontos;
	}

	public void setSumatoriaMontos(BigDecimal sumatoriaMontos) {
		this.sumatoriaMontos = sumatoriaMontos;
	}
	
	public void sumatoriaMontosAdd(String n) {
		this.sumatoriaMontos = this.sumatoriaMontos.add(new BigDecimal(n));
	}

	// Metodos correspondientes a la interfase
	
	public void validaRegistro( int processedRegisters, BufferedReader br)
		throws Exception {
		parse(br);
		this.garantias.validarAltaPortafolioIfSaldosInsolutos( processedRegisters, this );
	}
	
	public void validaCifrasControl(){
		this.garantias.validarCifrasControlPortafolioIfSaldosInsolutos( this );
	}

	public void validaArchivoZIP(){
		this.garantias.validarArchivoZIPConciliacionAutomatica( this );
	}
	
	public void 			setProcessID(String processID){
		this.processID 	 = processID;
	}
	
	public String 			getProcessID(){
		return this.processID;
	}
	
	public void 			setResultadosGar(ResultadosGar resultadosGar){
		this.resultadosGar = resultadosGar;
	}
	
	public ResultadosGar getResultadosGar(){
		return this.resultadosGar;
	}
	
	public void 			setClaveIfSiag(int claveIfSiag) {
		this.claveIfSiag = claveIfSiag;
	}
	
	public int 				getClaveIfSiag() {
		return this.claveIfSiag;
	}

	public void 			setGarantiasBean( GarantiasBean garantias ){
		this.garantias = garantias;
	}
	
	public GarantiasBean getGarantiasBean(){
		return this.garantias;
	}
	
	public void 			setLongitudClaveFinanciamiento(int longitudClaveFinanciamiento ){
		this.longitudClaveFinanciamiento = longitudClaveFinanciamiento;
	}
	
	public int 				getLongitudClaveFinanciamiento(){
		return longitudClaveFinanciamiento;
	}
	
	// Metodos con variables especificas de la validacion

	public String getClaveMesConciliar() {
		return claveMesConciliar;
	}

	public void setClaveMesConciliar(String claveMesConciliar) {
		this.claveMesConciliar = claveMesConciliar;
	}
	
	public boolean getArchivoZIPContieneDirectorio() {
		return archivoZIPContieneDirectorio;
	}

	public void setArchivoZIPContieneDirectorio(boolean archivoZIPContieneDirectorio) {
		this.archivoZIPContieneDirectorio = archivoZIPContieneDirectorio;
	}

	public boolean getArchivoZIPContieneMasDeUnArchivo() {
		return archivoZIPContieneMasDeUnArchivo;
	}

	public void setArchivoZIPContieneMasDeUnArchivo(boolean archivoZIPContieneMasDeUnArchivo) {
		this.archivoZIPContieneMasDeUnArchivo = archivoZIPContieneMasDeUnArchivo;
	}

	public List getClavesFinanciamientoRepetidas(){
		return clavesFinanciamientoRepetidas;
	}

	public void setClavesFinanciamientoRepetidas(List clavesFinanciamientoRepetidas) {
		this.clavesFinanciamientoRepetidas = clavesFinanciamientoRepetidas;
	}

	public List getClavesFinanciamientoRepetidas( String rutaArchivoTXT, int numeroTotalRegistros, int numeroMaximoRepetidos )
		throws Exception {
		
		BufferedReader 	br 									= null;
		AccesoDB				con									= new AccesoDB();
		StringBuffer		sentence 							= new StringBuffer(48);
		PreparedStatement	ps										= null;
		ResultSet			rs										= null;
		boolean				exito									= true;
		long					icProceso							= 0L;
		List 					clavesFinanciamientoRepetidas	= new ArrayList();
		try {
			
			// Conectarse a la Base de Datos
			con.conexionDB();
			
			// Obtener ID de la Operacion
			sentence.setLength(0);
			sentence.append(
				"SELECT SEQ_GTIAUX_CARGA_GARANTIAS.NEXTVAL AS IC_PROCESO FROM DUAL"
			);
			ps = con.queryPrecompilado(sentence.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				icProceso = rs.getLong("IC_PROCESO");
			} else{
				throw new AppException("No se pudo obtener el ID en la b�squeda de Claves de Financiamiento repetidas.");	
			}
			rs.close();
			ps.close();
			
			// Abrir Archivo TXT para extraer las claves de la garant�a
			br = new BufferedReader(new InputStreamReader(new FileInputStream( rutaArchivoTXT ),"ISO-8859-1"));
			
			// Insertar garant�as en base de datos
			int 		registrosProcesados 				= 0;
			int 		longitudClaveFinanciamiento	= getLongitudClaveFinanciamiento();
			String 	claveFinanciamiento				= null;
			// Preparar validaci�n de l�nea vac�a
			Pattern  lineaVacia							= Pattern.compile("^\\s*$");
			// Preparar Sentencia para insertar registro en la Base de Datos
			sentence.setLength(0);
			sentence.append(
				"INSERT INTO GTIAUX_CARGA_GARANTIAS ( IC_PROCESO, IC_REGISTRO, CC_GARANTIA ) VALUES ( ?, ?, ? ) "
			);
			ps = con.queryPrecompilado(sentence.toString());
			int cuentaInserciones						= 0;
			int numeroRegistro							= 0;
			while( registrosProcesados < numeroTotalRegistros ){
				
				// Parsear l�nea
				parse(br);
				registrosProcesados++;
				
				// Obtener Clave de Financiamiento
				claveFinanciamiento = getClaveFinanciamiento().toString();
				
				// Validar que la Clave de Financiamiento sea elegible para ser insertada en la Base de Datos
				if( lineaVacia.matcher(claveFinanciamiento).matches() ){
					continue;
				} else if( claveFinanciamiento.length() > longitudClaveFinanciamiento ){
					continue;
				} 

				// Insertar Clave de Financiamiento en la Base de Datos
				ps.clearParameters();
				ps.setLong(1,   icProceso          	);
				ps.setInt(2,	 numeroRegistro		);
				ps.setString(3, claveFinanciamiento );
				ps.executeUpdate();
				
				// Actualizar contador de registros insertados
				numeroRegistro++;
				// Actualizar conteo de registros insertados
				cuentaInserciones++;
				
				// Dar Commit cada 3500 registros
				if( cuentaInserciones >= 50000 ){
					con.terminaTransaccion(true);
					cuentaInserciones = 0;
				}
				
			}
			con.terminaTransaccion(true);
			ps.close();
			
			// Extraer Lista de Garant�as repetidas
			sentence.setLength(0);
			sentence.append(
				"SELECT                             "  +
				"   CC_GARANTIA AS CLAVE_FINANCIAMIENTO  "  +
				"FROM                               "  +
				"   (                               "  +
				"      SELECT                       "  +
				"         CC_GARANTIA,              "  +
				"         COUNT(1) AS REPETICIONES  "  +
				"      FROM                         "  +
				"         GTIAUX_CARGA_GARANTIAS    "  +
				"      WHERE                        "  +
				"         IC_PROCESO = ?            "  +
				"      GROUP BY                     "  +
				"         CC_GARANTIA               "  +
				"    ) GARANTIAS_REPETIDAS          "  +
				"WHERE                              "  +
				"   REPETICIONES > 1 AND            "  +
				"   ROWNUM       < ?                "  +
				"ORDER BY CC_GARANTIA					"
			);
			ps = con.queryPrecompilado(sentence.toString());
			ps.setLong(1, icProceso							);
			ps.setInt( 2, numeroMaximoRepetidos + 1	);
			rs = ps.executeQuery();
			while(rs.next()){
				claveFinanciamiento = (rs.getString("CLAVE_FINANCIAMIENTO") == null)?"":rs.getString("CLAVE_FINANCIAMIENTO");
				clavesFinanciamientoRepetidas.add(claveFinanciamiento);
			}

			// Como no se encontr� ninguna clave de garant�a, regresar NULL
			if( clavesFinanciamientoRepetidas.size() == 0 ){
				clavesFinanciamientoRepetidas = null;	
			}
				
		} catch(Exception e) {

			exito = false;
			
			log.error("getClavesFinanciamientoRepetidas: Error al extraer las Claves de Financiamiento Repetidas.");
			log.error("getClavesFinanciamientoRepetidas.rutaArchivoTXT        = <" + rutaArchivoTXT        + ">");
			log.error("getClavesFinanciamientoRepetidas.numeroTotalRegistros  = <" + numeroTotalRegistros  + ">");
			log.error("getClavesFinanciamientoRepetidas.numeroMaximoRepetidos = <" + numeroMaximoRepetidos + ">");
			e.printStackTrace();
			
			throw e;
			
		} finally {
						
			if( br != null ) { try { br.close(); br=null; } catch(Exception e2){} }
			
			if( rs != null ) { try { rs.close();          } catch(Exception e2){} }
		 	if( ps != null ) { try { ps.close();          } catch(Exception e2){} }
		 	
		 	if(con.hayConexionAbierta()) {
		 		con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

			log.info("getClavesFinanciamientoRepetidas(S)");
			
		}
			
		return clavesFinanciamientoRepetidas;
		
	}
	
	// Metodos correspondientes a los campos del archivo
	private StringBuffer 	claveIntermediarioFinancieroSIAG = null;
	private StringBuffer 	claveFinanciamiento 					= null;
	private StringBuffer 	nombreAcreditado 						= null;
	private StringBuffer 	rfc 										= null;
	private StringBuffer 	saldoInsolutoFinMes 					= null;
	private StringBuffer 	fechaDisposicion 						= null;
	private StringBuffer 	fechaVencimiento 						= null;
	private StringBuffer 	estatusGarantia 						= null;
	private StringBuffer 	moneda 									= null;
	private StringBuffer 	porcentajeGarantizado 				= null;
	private StringBuffer 	tipoCredito 							= null;
	private StringBuffer		mesConciliar 							= null;
	private StringBuffer 	campoExtra 								= null;
	
	// Variables Asociadas a los Totales
	private int 				totalRegistros							= 0;
	private BigDecimal		sumatoriaMontos						= null;
	
	// Variables Auxiliares
	private int					fieldIndex								= 0;
	private int					fieldSize								= 0;
	private int					acumulaAnterior						= -1;
	private int					lastChar									= -1;
	private boolean			hayError									= false;
	
	// Parametro de la Validacion
	private String 			processID								= null;
	private ResultadosGar 	resultadosGar							= null;
	private int 				claveIfSiag 							= -1;
	private GarantiasBean 	garantias 								= null;
	private int 				longitudClaveFinanciamiento 		= -1;
	
	// Parametros Especificos de la Validacion
	private String 			claveMesConciliar						= null;
	private boolean			archivoZIPContieneDirectorio		= false;
	private boolean 			archivoZIPContieneMasDeUnArchivo	= false;
	private List 				clavesFinanciamientoRepetidas		= null;
		
	public SaldoInsolutoConciliacionAutomaticaValidator(){
		
		this.claveIntermediarioFinancieroSIAG	= new StringBuffer(6);
		this.claveFinanciamiento					= new StringBuffer(31);
		this.nombreAcreditado						= new StringBuffer(81);
		this.rfc											= new StringBuffer(14);
		this.saldoInsolutoFinMes					= new StringBuffer(19);
		this.fechaDisposicion						= new StringBuffer(9);
		this.fechaVencimiento						= new StringBuffer(9);
		this.estatusGarantia							= new StringBuffer(4);
		this.moneda										= new StringBuffer(8);
		this.porcentajeGarantizado					= new StringBuffer(7);
		this.tipoCredito								= new StringBuffer(4);
		this.mesConciliar								= new StringBuffer(7);
		this.campoExtra 								= new StringBuffer(1);
		
		this.totalRegistros							= 0;
		this.sumatoriaMontos							= new BigDecimal("0");
	
		this.fieldIndex								= 0;
		this.fieldSize									= 0;
		this.acumulaAnterior							= -1;
		this.lastChar									= -1;
		this.hayError									= false;
		
		this.processID									= null;
		this.resultadosGar							= null;
		this.claveIfSiag 								= -1;
		this.garantias 								= null;
		this.longitudClaveFinanciamiento			= -1;		
		
	}
	
	public void doReset(){

		this.claveIntermediarioFinancieroSIAG.setLength(0);
		this.claveFinanciamiento.setLength(0);
		this.nombreAcreditado.setLength(0);
		this.rfc.setLength(0);
		this.saldoInsolutoFinMes.setLength(0);
		this.fechaDisposicion.setLength(0);
		this.fechaVencimiento.setLength(0);
		this.estatusGarantia.setLength(0);
		this.moneda.setLength(0);
		this.porcentajeGarantizado.setLength(0);
		this.tipoCredito.setLength(0);
		this.mesConciliar.setLength(0);
		this.campoExtra.setLength(0);
		
		//this.totalRegistros					= 0;                   // No se puede resetear porque es global a todos los registros
		//this.sumatoriaMontos					= new BigDecimal("0"); // No se puede resetear porque es global a todos los registros
		
		this.fieldIndex =  0;
		this.fieldSize  =  0;
		// this.acumulaAnterior // Nota: Este campo no se puede resetear
		this.lastChar   = -1;
		this.hayError   = false;

	}

	// CAMPOS  ASOCIADOS AL REGISTRO
	public StringBuffer getClaveIntermediarioFinancieroSIAG() {
		return claveIntermediarioFinancieroSIAG;
	}

	public void setClaveIntermediarioFinancieroSIAG(StringBuffer claveIntermediarioFinancieroSIAG) {
		this.claveIntermediarioFinancieroSIAG = claveIntermediarioFinancieroSIAG;
	}

	public StringBuffer getClaveFinanciamiento() {
		return claveFinanciamiento;
	}

	public void setClaveFinanciamiento(StringBuffer claveFinanciamiento) {
		this.claveFinanciamiento = claveFinanciamiento;
	}

	public StringBuffer getNombreAcreditado() {
		return nombreAcreditado;
	}

	public void setNombreAcreditado(StringBuffer nombreAcreditado) {
		this.nombreAcreditado = nombreAcreditado;
	}

	public StringBuffer getRfc() {
		return rfc;
	}

	public void setRfc(StringBuffer rfc) {
		this.rfc = rfc;
	}

	public StringBuffer getSaldoInsolutoFinMes() {
		return saldoInsolutoFinMes;
	}

	public void setSaldoInsolutoFinMes(StringBuffer saldoInsolutoFinMes) {
		this.saldoInsolutoFinMes = saldoInsolutoFinMes;
	}

	public StringBuffer getFechaDisposicion() {
		return fechaDisposicion;
	}

	public void setFechaDisposicion(StringBuffer fechaDisposicion) {
		this.fechaDisposicion = fechaDisposicion;
	}

	public StringBuffer getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(StringBuffer fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public StringBuffer getEstatusGarantia() {
		return estatusGarantia;
	}

	public void setEstatusGarantia(StringBuffer estatusGarantia) {
		this.estatusGarantia = estatusGarantia;
	}

	public StringBuffer getMoneda() {
		return moneda;
	}

	public void setMoneda(StringBuffer moneda) {
		this.moneda = moneda;
	}

	public StringBuffer getPorcentajeGarantizado() {
		return porcentajeGarantizado;
	}

	public void setPorcentajeGarantizado(StringBuffer porcentajeGarantizado) {
		this.porcentajeGarantizado = porcentajeGarantizado;
	}

	public StringBuffer getTipoCredito() {
		return tipoCredito;
	}

	public void setTipoCredito(StringBuffer tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

	public StringBuffer getMesConciliar() {
		return mesConciliar;
	}

	public void setMesConciliar(StringBuffer mesConciliar) {
		this.mesConciliar = mesConciliar;
	}

	public StringBuffer getCampoExtra() {
		return campoExtra;
	}

	public void setCampoExtra(StringBuffer campoExtra) {
		this.campoExtra = campoExtra;
	}

	
	public StringBuffer getField(){
		
		StringBuffer buffer = null;
		
		switch(fieldIndex){
			case  0: buffer = getClaveIntermediarioFinancieroSIAG(); this.fieldSize =  6; this.fieldIndex++; break;
			case  1: buffer = getClaveFinanciamiento(); 					this.fieldSize = 31; this.fieldIndex++; break;
			case  2: buffer = getNombreAcreditado(); 						this.fieldSize = 81; this.fieldIndex++; break;
			case  3: buffer = getRfc(); 										this.fieldSize = 14; this.fieldIndex++; break;
			case  4: buffer = getSaldoInsolutoFinMes(); 					this.fieldSize = 19; this.fieldIndex++; break;
			case  5: buffer = getFechaDisposicion(); 						this.fieldSize =  9; this.fieldIndex++; break;
			case  6: buffer = getFechaVencimiento(); 						this.fieldSize =  9; this.fieldIndex++; break;
			case  7: buffer = getEstatusGarantia(); 						this.fieldSize =  4; this.fieldIndex++; break;
			case  8: buffer = getMoneda(); 									this.fieldSize =  8; this.fieldIndex++; break;
			case  9: buffer = getPorcentajeGarantizado(); 				this.fieldSize =  7; this.fieldIndex++; break;
			case 10: buffer = getTipoCredito(); 							this.fieldSize =  7; this.fieldIndex++; break;
			case 11: buffer = getMesConciliar(); 							this.fieldSize =  7; this.fieldIndex++; break;	
			case 12: buffer = getCampoExtra();								this.fieldSize =  1; this.fieldIndex++; break;
			default:
				buffer          = null;
				this.fieldSize  = -1;
				this.fieldIndex = -1;
		}
		
		return buffer;
		
	}
	
	public int getFieldSize(){
		return this.fieldSize;
	}
	
	public int getLastChar(){
		return this.lastChar;
	}

	public boolean getHayError() {
		return this.hayError;
	}

	public void setHayError(boolean hayError) {
		this.hayError = hayError;
	}
	
	public void parse(BufferedReader br)
		throws Exception {
		
		doReset();
		
		int 		 	 c 					= -1;
		StringBuffer field 				= getField();
		int			 bufferSize			= getFieldSize();
		int			 caracteres 		= 0;
		boolean      carriageReturn 	= false;		
		do {

			// LEER SIGUIENTE CARACTER
			do {
				
				// Leer siguiente caracter
				if( this.acumulaAnterior != -1 ){
					c = this.acumulaAnterior;
					this.acumulaAnterior = -1;
				} else {
					c = br.read();
				}
				
				// Considerar el caracter carriage return, que ven�a s�lo, como caracter de fin de l�nea
				if( carriageReturn && c != '\n' ){
					this.acumulaAnterior = c;
					carriageReturn = false;
					c = -1;
				}
			
				// Guardar memoria del �ltimo caracter v�lido leido
				if( 
					 ( 
						caracteres >= bufferSize 
							|| 
						field == null 
					  ) 
					&& 
					   c != '\r' && c != '\n' && c != -1 
				){
					this.lastChar = c; 
				}
				
			} while (
				(
					( caracteres >= bufferSize  && c != '@' ) 
						|| 
					( field      == null                     )
				)
					&& 
				c != '\r' && c != '\n' && c != -1 
			);
			
			// ASIGNAR CARACTER LEIDO
			switch(c){
				case  '@': // Pasar al siguiente campo
					field 	  		= getField();
					bufferSize 		= getFieldSize();
					// Resetear Conteo
					caracteres 		= field == null?-1:0;
					this.lastChar	= (int) '@'; // Guardar memoria del ultimo caracter valido leido
					break;
				case '\r':
					carriageReturn = true;
					break;
				case '\n':
					c = -1; // Se encontr� fin de l�nea
					break;
				case   -1:
					break;
				default  :
					field.append((char)c);
					++caracteres;
					this.lastChar	= c; // Guardar memoria del ultimo caracter valido leido
			}

		} while( c != -1 );
		
	}
	
}


