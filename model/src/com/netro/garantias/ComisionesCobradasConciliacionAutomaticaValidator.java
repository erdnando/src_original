package com.netro.garantias;

import java.io.BufferedReader;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.AccesoDB;
import org.apache.commons.logging.Log;
import java.util.List;

import java.math.BigDecimal;

public class ComisionesCobradasConciliacionAutomaticaValidator implements FileValidator {
		
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(ComisionesCobradasConciliacionAutomaticaValidator.class);

	public int getTotalRegistros() {
		return this.totalRegistros;
	}

	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	
	public void totalRegistrosAdd(int n) {
		this.totalRegistros += n;
	}

	public BigDecimal getSumatoriaMontos() {
		return this.sumatoriaMontos;
	}

	public void setSumatoriaMontos(BigDecimal sumatoriaMontos) {
		this.sumatoriaMontos = sumatoriaMontos;
	}
	
	public void sumatoriaMontosAdd(String n) {
		this.sumatoriaMontos = this.sumatoriaMontos.add(new BigDecimal(n));
	}

	// Metodos correspondientes a la interfase
	
	public void validaRegistro( int processedRegisters, BufferedReader br)
		throws Exception {
		parse(br);
		this.garantias.validarAltaPortafolioIfComisionesCobradas( processedRegisters, this );
	}
	
	public void validaCifrasControl(){
		this.garantias.validarCifrasControlPortafolioIfComisionesCobradas( this );
	}

	public void validaArchivoZIP(){
		this.garantias.validarArchivoZIPConciliacionAutomatica( this );
	}
	
	public void 			setProcessID(String processID){
		this.processID 	 = processID;
	}
	
	public String 			getProcessID(){
		return this.processID;
	}
	
	public void 			setResultadosGar(ResultadosGar resultadosGar){
		this.resultadosGar = resultadosGar;
	}
	
	public ResultadosGar getResultadosGar(){
		return this.resultadosGar;
	}
	
	public void 			setClaveIfSiag(int claveIfSiag) {
		this.claveIfSiag = claveIfSiag;
	}
	
	public int 				getClaveIfSiag() {
		return this.claveIfSiag;
	}

	public void 			setGarantiasBean( GarantiasBean garantias ){
		this.garantias = garantias;
	}
	
	public GarantiasBean getGarantiasBean(){
		return this.garantias;
	}
	
	public void 			setLongitudClaveFinanciamiento(int longitudClaveFinanciamiento ){
		this.longitudClaveFinanciamiento = longitudClaveFinanciamiento;
	}
	
	public int 				getLongitudClaveFinanciamiento(){
		return longitudClaveFinanciamiento;
	}
		
	// Metodos con variables especificas de la validacion

	public String getClaveMesConciliar() {
		return claveMesConciliar;
	}

	public void setClaveMesConciliar(String claveMesConciliar) {
		this.claveMesConciliar = claveMesConciliar;
	}

	public boolean getArchivoZIPContieneDirectorio() {
		return archivoZIPContieneDirectorio;
	}

	public void setArchivoZIPContieneDirectorio(boolean archivoZIPContieneDirectorio) {
		this.archivoZIPContieneDirectorio = archivoZIPContieneDirectorio;
	}

	public boolean getArchivoZIPContieneMasDeUnArchivo() {
		return archivoZIPContieneMasDeUnArchivo;
	}

	public void setArchivoZIPContieneMasDeUnArchivo(boolean archivoZIPContieneMasDeUnArchivo) {
		this.archivoZIPContieneMasDeUnArchivo = archivoZIPContieneMasDeUnArchivo;
	}
	
	public List getClavesFinanciamientoRepetidas(){
		return null;
	}

	public void setClavesFinanciamientoRepetidas(List clavesFinanciamientoRepetidas) {}
	
	public List getClavesFinanciamientoRepetidas( String rutaArchivoTXT, int numeroTotalRegistros, int numeroMaximoRepetidos )
		throws Exception {
		return null;
	}
		
	// Metodos correspondientes a los campos del archivo
	private StringBuffer 	claveIntermediarioFinancieroSIAG = null;
	private StringBuffer 	moneda 									= null;
	private StringBuffer 	montoComisionMensual 				= null;
	private StringBuffer 	montoComisionAniversario 			= null;
	private StringBuffer 	montoComisionSubasta 				= null;
	private StringBuffer 	montoComisionExtemporanea 			= null;
	private StringBuffer		mesConciliar 							= null;
	private StringBuffer 	campoExtra 								= null;
	
	// Variables Asociadas a los Totales
	private int 				totalRegistros							= 0;
	private BigDecimal		sumatoriaMontos						= null;
	
	// Variables Auxiliares
	private int					fieldIndex								= 0;
	private int					fieldSize								= 0;
	private int					acumulaAnterior						= -1;
	private int					lastChar									= -1;
	private boolean			hayError									= false;
	
	// Parametro de la Validacion
	private String 			processID								= null;
	private ResultadosGar 	resultadosGar							= null;
	private int 				claveIfSiag 							= -1;
	private GarantiasBean 	garantias 								= null;
	private int 				longitudClaveFinanciamiento 		= -1;

	// Parametros Especificos de la Validacion
	private String 			claveMesConciliar						= null;
	private boolean			archivoZIPContieneDirectorio		= false;
	private boolean 			archivoZIPContieneMasDeUnArchivo	= false;
	
	public ComisionesCobradasConciliacionAutomaticaValidator(){
		
		this.claveIntermediarioFinancieroSIAG	= new StringBuffer(6);
		this.moneda										= new StringBuffer(8);
		this.montoComisionMensual					= new StringBuffer(19);
		this.montoComisionAniversario				= new StringBuffer(19);
		this.montoComisionSubasta					= new StringBuffer(19);
		this.montoComisionExtemporanea			= new StringBuffer(19);
		this.mesConciliar								= new StringBuffer(7);
		this.campoExtra 								= new StringBuffer(1);
		
		this.totalRegistros							= 0;
		this.sumatoriaMontos							= new BigDecimal("0");
	
		this.fieldIndex								= 0;
		this.fieldSize									= 0;
		this.acumulaAnterior							= -1;
		this.lastChar									= -1;
		this.hayError									= false;
		
		this.processID									= null;
		this.resultadosGar							= null;
		this.claveIfSiag 								= -1;
		this.garantias 								= null;
		this.longitudClaveFinanciamiento			= -1;		
		
	}
	
	public void doReset(){

		this.claveIntermediarioFinancieroSIAG.setLength(0);
		this.moneda.setLength(0);
		this.montoComisionMensual.setLength(0);
		this.montoComisionAniversario.setLength(0);
		this.montoComisionSubasta.setLength(0);
		this.montoComisionExtemporanea.setLength(0);
		this.mesConciliar.setLength(0);
		this.campoExtra.setLength(0);
		
		//this.totalRegistros					= 0;                   // No se puede resetear porque es global a todos los registros
		//this.sumatoriaMontos					= new BigDecimal("0"); // No se puede resetear porque es global a todos los registros
		
		this.fieldIndex =  0;
		this.fieldSize  =  0;
		// this.acumulaAnterior // Nota: Este campo no se puede resetear
		this.lastChar   = -1;
		this.hayError   = false;

	}

	// CAMPOS  ASOCIADOS AL REGISTRO
	public StringBuffer getClaveIntermediarioFinancieroSIAG() {
		return claveIntermediarioFinancieroSIAG;
	}

	public void setClaveIntermediarioFinancieroSIAG(StringBuffer claveIntermediarioFinancieroSIAG) {
		this.claveIntermediarioFinancieroSIAG = claveIntermediarioFinancieroSIAG;
	}

	public StringBuffer getMoneda() {
		return moneda;
	}

	public void setMoneda(StringBuffer moneda) {
		this.moneda = moneda;
	}
	
	public StringBuffer getMontoComisionMensual() {
		return montoComisionMensual;
	}

	public void setMontoComisionMensual(StringBuffer montoComisionMensual) {
		this.montoComisionMensual = montoComisionMensual;
	}

	public StringBuffer getMontoComisionAniversario() {
		return montoComisionAniversario;
	}

	public void setMontoComisionAniversario(StringBuffer montoComisionAniversario) {
		this.montoComisionAniversario = montoComisionAniversario;
	}

	public StringBuffer getMontoComisionSubasta() {
		return montoComisionSubasta;
	}

	public void setMontoComisionSubasta(StringBuffer montoComisionSubasta) {
		this.montoComisionSubasta = montoComisionSubasta;
	}

	public StringBuffer getMontoComisionExtemporanea() {
		return montoComisionExtemporanea;
	}

	public void setMontoComisionExtemporanea(StringBuffer montoComisionExtemporanea) {
		this.montoComisionExtemporanea = montoComisionExtemporanea;
	}

	public StringBuffer getMesConciliar() {
		return mesConciliar;
	}

	public void setMesConciliar(StringBuffer mesConciliar) {
		this.mesConciliar = mesConciliar;
	}

	public StringBuffer getCampoExtra() {
		return campoExtra;
	}

	public void setCampoExtra(StringBuffer campoExtra) {
		this.campoExtra = campoExtra;
	}

	
	public StringBuffer getField(){
		
		StringBuffer buffer = null;
		
		switch(fieldIndex){
			case  0: buffer = getClaveIntermediarioFinancieroSIAG(); this.fieldSize =  6; this.fieldIndex++; break;
			case  1: buffer = getMoneda(); 									this.fieldSize =  8; this.fieldIndex++; break;
			case  2: buffer = getMontoComisionMensual(); 				this.fieldSize = 19; this.fieldIndex++; break;
			case  3: buffer = getMontoComisionAniversario(); 			this.fieldSize = 19; this.fieldIndex++; break;
			case  4: buffer = getMontoComisionSubasta(); 				this.fieldSize = 19; this.fieldIndex++; break;
			case  5: buffer = getMontoComisionExtemporanea(); 			this.fieldSize = 19; this.fieldIndex++; break;
			case  6: buffer = getMesConciliar(); 							this.fieldSize =  7; this.fieldIndex++; break;	
			case  7: buffer = getCampoExtra();								this.fieldSize =  1; this.fieldIndex++; break;
			default:
				buffer          = null;
				this.fieldSize  = -1;
				this.fieldIndex = -1;
		}
		
		return buffer;
		
	}
	
	public int getFieldSize(){
		return this.fieldSize;
	}
	
	public int getLastChar(){
		return this.lastChar;
	}

	public boolean getHayError() {
		return this.hayError;
	}

	public void setHayError(boolean hayError) {
		this.hayError = hayError;
	}
	
	public void parse(BufferedReader br)
		throws Exception {
		
		doReset();
		
		int 		 	 c 					= -1;
		StringBuffer field 				= getField();
		int			 bufferSize			= getFieldSize();
		int			 caracteres 		= 0;
		boolean      carriageReturn 	= false;		
		do {

			// LEER SIGUIENTE CARACTER
			do {
				
				// Leer siguiente caracter
				if( this.acumulaAnterior != -1 ){
					c = this.acumulaAnterior;
					this.acumulaAnterior = -1;
				} else {
					c = br.read();
				}
				
				// Considerar el caracter carriage return, que ven�a s�lo, como caracter de fin de l�nea
				if( carriageReturn && c != '\n' ){
					this.acumulaAnterior = c;
					carriageReturn = false;
					c = -1;
				}
			
				// Guardar memoria del �ltimo caracter v�lido leido
				if( 
					 ( 
						caracteres >= bufferSize 
							|| 
						field == null 
					  ) 
					&& 
					   c != '\r' && c != '\n' && c != -1 
				){
					this.lastChar = c; 
				}
				
			} while (
				(
					( caracteres >= bufferSize  && c != '@' ) 
						|| 
					( field      == null                     )
				)
					&& 
				c != '\r' && c != '\n' && c != -1 
			);
			
			// ASIGNAR CARACTER LEIDO
			switch(c){
				case  '@': // Pasar al siguiente campo
					field 	  		= getField();
					bufferSize 		= getFieldSize();
					// Resetear Conteo
					caracteres 		= field == null?-1:0;
					this.lastChar	= (int) '@'; // Guardar memoria del ultimo caracter valido leido
					break;
				case '\r':
					carriageReturn = true;
					break;
				case '\n':
					c = -1; // Se encontr� fin de l�nea
					break;
				case   -1:
					break;
				default  :
					field.append((char)c);
					++caracteres;
					this.lastChar	= c; // Guardar memoria del ultimo caracter valido leido
			}

		} while( c != -1 );
		
	}
	
}


