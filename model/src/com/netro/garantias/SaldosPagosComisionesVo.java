package com.netro.garantias;

import java.util.Date;

public class SaldosPagosComisionesVo {
    private int linea;
    private String claveFinanciamiento;
    private Date FechaInicialPeriodoSaldoInsoluto;
    private Date FechaFinalPeriodoSaldoInsoluto;
    private boolean cancelacionLiquidacion;
    
    public SaldosPagosComisionesVo() {
        super();
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public int getLinea() {
        return linea;
    }

    public void setClaveFinanciamiento(String claveFinanciamiento) {
        this.claveFinanciamiento = claveFinanciamiento;
    }

    public String getClaveFinanciamiento() {
        return claveFinanciamiento;
    }

    public void setFechaInicialPeriodoSaldoInsoluto(Date FechaInicialPeriodoSaldoInsoluto) {
        this.FechaInicialPeriodoSaldoInsoluto = FechaInicialPeriodoSaldoInsoluto;
    }

    public Date getFechaInicialPeriodoSaldoInsoluto() {
        return FechaInicialPeriodoSaldoInsoluto;
    }

    public void setFechaFinalPeriodoSaldoInsoluto(Date FechaFinalPeriodoSaldoInsoluto) {
        this.FechaFinalPeriodoSaldoInsoluto = FechaFinalPeriodoSaldoInsoluto;
    }

    public Date getFechaFinalPeriodoSaldoInsoluto() {
        return FechaFinalPeriodoSaldoInsoluto;
    }

    public void setCancelacionLiquidacion(boolean cancelacionLiquidacion) {
        this.cancelacionLiquidacion = cancelacionLiquidacion;
    }

    public boolean getCancelacionLiquidacion() {
        return cancelacionLiquidacion;
    }
}
