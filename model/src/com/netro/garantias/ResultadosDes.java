package com.netro.garantias;

import java.math.*;
import java.io.*;
import java.util.*;

public class ResultadosDes implements Serializable {

	private StringBuffer	errores = new StringBuffer("");
	private StringBuffer	correctos = new StringBuffer("");
	private StringBuffer	cifras	= new StringBuffer("");
	private boolean			errorLinea = false;
	private int 			num_reg_ok		= 0;
	private int				num_reg_err		= 0;
	private int				num_reg_err_bis	= 0;	
	private int				num_err_cifras	= 0;	
	private BigDecimal		sum_reg_ok	= new BigDecimal(0);
	private BigDecimal		sum_reg_err	= new BigDecimal(0);
	private BigDecimal		sum_tot_pag	= new BigDecimal(0);	
	private int				ic_proceso = 0;
	private String			fecha	= "";
	private String			hora	= "";
	private String			folio	= "";
	private List			clavesFinan = null;
	//para calif cartera
	private BigDecimal		sum_montos_cub_ok	= new BigDecimal(0);
	private BigDecimal		sum_montos_exp_ok	= new BigDecimal(0);
	private BigDecimal		sum_montos_cub_err	= new BigDecimal(0);
	private BigDecimal		sum_montos_exp_err	= new BigDecimal(0);
	private String fechaValor;
		


	public void addSumMontosCubErr(double valor){
		this.sum_montos_cub_err = this.sum_montos_cub_err.add(new BigDecimal(valor));
	}
	public void addSumMontosExpErr(double valor){
		this.sum_montos_exp_err = this.sum_montos_exp_err.add(new BigDecimal(valor));
	}
	public void addSumMontosCubOk(double valor){
		this.sum_montos_cub_ok = this.sum_montos_cub_ok.add(new BigDecimal(valor));
	}
	public void addSumMontosExpOk(double valor){
		this.sum_montos_exp_ok = this.sum_montos_exp_ok.add(new BigDecimal(valor));
	}

	public void addSumTotPag(double valor){
		this.sum_tot_pag = this.sum_tot_pag.add(new BigDecimal(valor));
	}
	public void addSumTotPag(BigDecimal valor){
		this.sum_tot_pag = this.sum_tot_pag.add(valor);
	}	
	public void addSumRegErr(double valor){
		this.sum_reg_err = this.sum_reg_err.add(new BigDecimal(valor));
	}	
	public void addSumRegErr(BigDecimal valor){
		this.sum_reg_err = this.sum_reg_err.add(valor);
	}
	public void addSumRegOk(double valor){
		this.sum_reg_ok = this.sum_reg_ok.add(new BigDecimal(valor));
	}
	public void addSumRegOk(BigDecimal valor){
		this.sum_reg_ok = this.sum_reg_ok.add(valor);
	}
	public void appendErrores(String valor){
		if(num_reg_err_bis<500){
			errores.append(valor);
		}else if(num_reg_err_bis==500){
			errores.append("\n\n\nFavor de revisar el archivo, contiene demasiados errores");
		}
		num_reg_err_bis++;
	}
	public void appendCorrectos(String valor){
		correctos.append(valor);
	}
	public void appendCifras(String valor){
		if(num_err_cifras < 200){
			cifras.append(valor);
		}else if(num_err_cifras==200){
			cifras.append("\n\n\nEl archivo contiene demasiados registros repetidos, favor de revisarlo.");
		}
		num_err_cifras++;
	}
	public void incNumRegOk(){
		this.num_reg_ok ++;
	}
	public void incNumRegErr(){
		this.num_reg_err ++;
	}	
	public void setClavesFinan(List clavesF){
		this.clavesFinan = clavesF;
	}
	public void setErrorLinea(boolean valor){
		this.errorLinea = valor;
	}
	public void setFecha(String valor){
		this.fecha = valor;
	}
	public void setFolio(String valor){
		this.folio = valor;
	}	
	public void setHora(String valor){
		this.hora = valor;
	}	
	public void setIcProceso(int valor){
		this.ic_proceso = valor;
	}	
	public List	getClavesFinan(){
		return this.clavesFinan;
	}
	public String getCorrectos(){
		return this.correctos.toString();
	}
	public String getCifras(){
		return this.cifras.toString();
	}
	public String getErrores(){
		return this.errores.toString();
	}
	public boolean getErrorLinea(){
		return this.errorLinea;
	}
	public String getFecha(){
		return this.fecha;
	}
	public String getFolio(){
		return this.folio;
	}
	public String getHora(){
		return this.hora;
	}	
	public int getIcProceso(){
		return this.ic_proceso;
	}
	public int	getNumRegOk(){
		return this.num_reg_ok;
	}
	public int	getNumRegErr(){
		return this.num_reg_err;
	}
	public double getSumRegOk(){
		return this.sum_reg_ok.doubleValue();
	}
	public double getSumRegErr(){
		return this.sum_reg_err.doubleValue();
	}
	public double getSumTotPag(){
		return this.sum_tot_pag.doubleValue();
	}

	public double getSumMontosCubErr(){
		return this.sum_montos_cub_err.doubleValue();
	}
	public double getSumMontosExpErr(){
		return this.sum_montos_exp_err.doubleValue();
	}
	public double getSumMontosCubOk(){
		return this.sum_montos_cub_ok.doubleValue();
	}
	public double getSumMontosExpOk(){
		return this.sum_montos_exp_ok.doubleValue();
	}	

	public String getFechaValor() {
		return fechaValor;
	}

	public void setFechaValor(String fechaValor) {
		this.fechaValor = fechaValor;
	}
	
	
}// Fin de la clase
