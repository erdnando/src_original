package com.netro.garantias;

import java.io.BufferedReader;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;
import java.util.List;

import java.math.BigDecimal;

public interface FileValidator  {

	void 				validaRegistro( int processedRegisters, BufferedReader br) throws Exception;
	void 				validaCifrasControl();
	void 				validaArchivoZIP();
	void 				setProcessID(String processID); 
	String 			getProcessID(); 	
	void 				setResultadosGar(ResultadosGar resultadosGar);
	ResultadosGar 	getResultadosGar();
	void 				setClaveIfSiag(int claveIfSiag);
	int 				getClaveIfSiag();
	void 				setGarantiasBean( GarantiasBean garantias );
	GarantiasBean 	getGarantiasBean();
	void 				setLongitudClaveFinanciamiento(int longitudClaveFinanciamiento );
	int 				getLongitudClaveFinanciamiento();
	boolean 			getHayError();
	void 				setHayError(boolean hayError);
	int 				getTotalRegistros();
	void 				setTotalRegistros(int totalRegistros);
	BigDecimal 		getSumatoriaMontos();
	void 				setSumatoriaMontos(BigDecimal sumatoriaMontos);
	void				setArchivoZIPContieneDirectorio(boolean archivoZIPContieneDirectorio);
	boolean			getArchivoZIPContieneDirectorio();
	void 				setArchivoZIPContieneMasDeUnArchivo(boolean archivoZIPContieneMasDeUnArchivo);
	boolean			getArchivoZIPContieneMasDeUnArchivo();
	void 				setClavesFinanciamientoRepetidas(List clavesFinanciamientoRepetidas);
	List 				getClavesFinanciamientoRepetidas();
	List				getClavesFinanciamientoRepetidas( String rutaArchivoTXT, int numberOfRegisters, int numeroMaximoRepetidos ) throws Exception;
	
}