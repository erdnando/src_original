package com.netro.garantias;

public class ParamAltaGarantiasAut implements java.io.Serializable {

	private String solicitaFondeoNafin = "";
	private String solicitaParticipRiesgoNAFIN = "";
	private String porcParticipacionSolicitado = "";
	private String propositoProyecto = "";
	private String claveTipoFinanciamiento = "";
	private String claveDeGarantia = "";
	private String montoDeGarantia = "";
	private String claceDeConceptoRecurso = "";
	private String porcentajeNacional = "";
	private String porcentajeImportacion = "";
	private String porcProdDestMercadoInterno = "";
	private String porcProdDestMercadoExterno = "";
	private String formaAmortizacion = "";
	private String numMesesGraciaFinan = "";
	private String claveFuncionarioFacultadoIF = "";
	private String tipoAutorizacion = "";
	private String tipoPrograma = "";
	private String tipoTasa = "";
	private String plazoNumDias = "";
	private String calificacionInicial = "";
	private String antiguedadClienteMeses = "";
	private String moneda = "";
	private String entidad = "";
	
	public ParamAltaGarantiasAut()
	{
	}
	/*---------------SETTER AND GETTER-----------------*/
	public String getSolicitaFondeoNafin(){ return solicitaFondeoNafin; }
	public void setSolicitaFondeoNafin(String solicitaFondeoNafin){ this.solicitaFondeoNafin = solicitaFondeoNafin;}
	
	public String getSolicitaParticipRiesgoNAFIN(){ return solicitaParticipRiesgoNAFIN; }
	public void setSolicitaParticipRiesgoNAFIN(String solicitaParticipRiesgoNAFIN){this.solicitaParticipRiesgoNAFIN = solicitaParticipRiesgoNAFIN;}
	
	public String getPorcParticipacionSolicitado(){ return porcParticipacionSolicitado; }
	public void setPorcParticipacionSolicitado(String porcParticipacionSolicitado){this.porcParticipacionSolicitado = porcParticipacionSolicitado;}
	
	public String getPropositoProyecto(){ return propositoProyecto; }
	public void setPropositoProyecto(String propositoProyecto){this.propositoProyecto = propositoProyecto;}
	
	public String getClaveTipoFinanciamiento(){ return claveTipoFinanciamiento; }
	public void setClaveTipoFinanciamiento(String claveTipoFinanciamiento){this.claveTipoFinanciamiento = claveTipoFinanciamiento;}
	
	public String getClaveDeGarantia(){ return claveDeGarantia; }
	public void setClaveDeGarantia(String claveDeGarantia){this.claveDeGarantia = claveDeGarantia;}
	
	public String getMontoDeGarantia(){ return montoDeGarantia; }
	public void setMontoDeGarantia(String montoDeGarantia){this.montoDeGarantia = montoDeGarantia;}
	
	public String getClaceDeConceptoRecurso(){ return claceDeConceptoRecurso; }
	public void setClaceDeConceptoRecurso(String claceDeConceptoRecurso){this.claceDeConceptoRecurso = claceDeConceptoRecurso;}
	
	public String getPorcentajeNacional(){ return porcentajeNacional; }
	public void setPorcentajeNacional(String porcentajeNacional){this.porcentajeNacional = porcentajeNacional;}
	
	public String getPorcentajeImportacion(){ return porcentajeImportacion; }
	public void setPorcentajeImportacion(String porcentajeImportacion){this.porcentajeImportacion = porcentajeImportacion;}
	
	public String getPorcProdDestMercadoInterno(){ return porcProdDestMercadoInterno; }
	public void setPorcProdDestMercadoInterno(String porcProdDestMercadoInterno){this.porcProdDestMercadoInterno = porcProdDestMercadoInterno;}
	
	public String getPorcProdDestMercadoExterno(){ return porcProdDestMercadoExterno; }
	public void setPorcProdDestMercadoExterno(String porcProdDestMercadoExterno){this.porcProdDestMercadoExterno = porcProdDestMercadoExterno;}
	
	public String getFormaAmortizacion(){ return formaAmortizacion; }
	public void setFormaAmortizacion(String formaAmortizacion){this.formaAmortizacion = formaAmortizacion;}
	
	public String getNumMesesGraciaFinan(){ return numMesesGraciaFinan; }
	public void setNumMesesGraciaFinan(String numMesesGraciaFinan){this.numMesesGraciaFinan = numMesesGraciaFinan;}
	
	public String getClaveFuncionarioFacultadoIF(){ return claveFuncionarioFacultadoIF; }
	public void setClaveFuncionarioFacultadoIF(String claveFuncionarioFacultadoIF){this.claveFuncionarioFacultadoIF = claveFuncionarioFacultadoIF;}
	
	public String getTipoAutorizacion(){ return tipoAutorizacion; }
	public void setTipoAutorizacion(String tipoAutorizacion){this.tipoAutorizacion = tipoAutorizacion;}
	
	public String getTipoPrograma(){ return tipoPrograma; }
	public void setTipoPrograma(String tipoPrograma){this.tipoPrograma = tipoPrograma;}
	
	public String getTipoTasa(){ return tipoTasa; }	
	public void setTipoTasa(String tipoTasa){this.tipoTasa = tipoTasa;}
	
	public String getPlazoNumDias(){ return plazoNumDias; }
	public void setPlazoNumDias(String plazoNumDias){this.plazoNumDias = plazoNumDias;}
	
	public String getCalificacionInicial(){ return calificacionInicial; }
	public void setCalificacionInicial(String calificacionInicial){this.calificacionInicial = calificacionInicial;}
	
	public String getAntiguedadClienteMeses(){ return antiguedadClienteMeses; }
	public void setAntiguedadClienteMeses(String antiguedadClienteMeses){this.antiguedadClienteMeses = antiguedadClienteMeses;}
	
	public String getMoneda(){ return moneda; }
	public void setMoneda(String moneda){this.moneda = moneda;}


	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}


	public String getEntidad() {
		return entidad;
	}

	
}